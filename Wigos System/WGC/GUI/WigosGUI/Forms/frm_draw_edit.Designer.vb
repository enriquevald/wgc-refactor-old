<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_draw_edit
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_draw_edit))
    Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
    Me.uc_entry_draw_numbers_max = New GUI_Controls.uc_entry_field()
    Me.uc_entry_draw_numbers_initial = New GUI_Controls.uc_entry_field()
    Me.ef_last_number = New GUI_Controls.uc_entry_field()
    Me.lbl_last_number = New System.Windows.Forms.Label()
    Me.gb_draws_numeration = New System.Windows.Forms.GroupBox()
    Me.lbl_total_numbers_count = New System.Windows.Forms.Label()
    Me.chk_bingo_format = New System.Windows.Forms.CheckBox()
    Me.Uc_text_field1 = New GUI_Controls.uc_text_field()
    Me.dtp_start = New GUI_Controls.uc_date_picker()
    Me.dtp_end = New GUI_Controls.uc_date_picker()
    Me.cmb_status = New GUI_Controls.uc_combo()
    Me.lbl_status = New System.Windows.Forms.Label()
    Me.ef_draw_name = New GUI_Controls.uc_entry_field()
    Me.ef_number_price = New GUI_Controls.uc_entry_field()
    Me.cmb_draw_type = New GUI_Controls.uc_combo()
    Me.gb_detail = New System.Windows.Forms.GroupBox()
    Me.lbl_price_ratio = New System.Windows.Forms.Label()
    Me.gb_points_by_level = New System.Windows.Forms.GroupBox()
    Me.lbl_info_points_level = New System.Windows.Forms.Label()
    Me.chk_points_level4 = New System.Windows.Forms.CheckBox()
    Me.chk_points_level3 = New System.Windows.Forms.CheckBox()
    Me.chk_points_level2 = New System.Windows.Forms.CheckBox()
    Me.chk_points_level1 = New System.Windows.Forms.CheckBox()
    Me.ef_points_level4 = New GUI_Controls.uc_entry_field()
    Me.ef_points_level1 = New GUI_Controls.uc_entry_field()
    Me.ef_points_level3 = New GUI_Controls.uc_entry_field()
    Me.ef_points_level2 = New GUI_Controls.uc_entry_field()
    Me.tb_winner = New System.Windows.Forms.TabPage()
    Me.btn_voucher = New GUI_Controls.uc_button()
    Me.ef_draw_ticket = New GUI_Controls.uc_entry_field()
    Me.ef_account = New GUI_Controls.uc_entry_field()
    Me.ef_voucher = New GUI_Controls.uc_entry_field()
    Me.ef_winner_name = New GUI_Controls.uc_entry_field()
    Me.ef_winning_number = New GUI_Controls.uc_entry_field()
    Me.btn_check_winner = New GUI_Controls.uc_button()
    Me.tb_layout = New System.Windows.Forms.TabPage()
    Me.tab_voucher = New System.Windows.Forms.TabControl()
    Me.TabPage6 = New System.Windows.Forms.TabPage()
    Me.wb_voucher = New System.Windows.Forms.WebBrowser()
    Me.TabPage1 = New System.Windows.Forms.TabPage()
    Me.ef_header = New System.Windows.Forms.TextBox()
    Me.TabPage2 = New System.Windows.Forms.TabPage()
    Me.ef_footer = New System.Windows.Forms.TextBox()
    Me.TabPage3 = New System.Windows.Forms.TabPage()
    Me.ef_detail1 = New System.Windows.Forms.TextBox()
    Me.TabPage4 = New System.Windows.Forms.TabPage()
    Me.ef_detail2 = New System.Windows.Forms.TextBox()
    Me.TabPage5 = New System.Windows.Forms.TabPage()
    Me.ef_detail3 = New System.Windows.Forms.TextBox()
    Me.tab_Extra = New System.Windows.Forms.TabControl()
    Me.tb_offer = New System.Windows.Forms.TabPage()
    Me.lbl_msg_order_offers = New GUI_Controls.uc_text_field()
    Me.lbl_not_apply = New GUI_Controls.uc_text_field()
    Me.btn_offer_remove = New GUI_Controls.uc_button()
    Me.gb_new_offer = New System.Windows.Forms.GroupBox()
    Me.ef_offer_n = New GUI_Controls.uc_entry_field()
    Me.Panel1 = New System.Windows.Forms.Panel()
    Me.btn_reset = New GUI_Controls.uc_button()
    Me.chk_by_gender_offer = New System.Windows.Forms.CheckBox()
    Me.opt_gender_male_offer = New System.Windows.Forms.RadioButton()
    Me.opt_gender_female_offer = New System.Windows.Forms.RadioButton()
    Me.lblTo = New System.Windows.Forms.Label()
    Me.lblFrom = New System.Windows.Forms.Label()
    Me.btn_check_all_days = New GUI_Controls.uc_button()
    Me.chk_offer_sunday = New System.Windows.Forms.CheckBox()
    Me.chk_offer_saturday = New System.Windows.Forms.CheckBox()
    Me.btn_offer_add = New GUI_Controls.uc_button()
    Me.chk_offer_friday = New System.Windows.Forms.CheckBox()
    Me.chk_offer_thursday = New System.Windows.Forms.CheckBox()
    Me.chk_offer_wednesday = New System.Windows.Forms.CheckBox()
    Me.chk_offer_tuesday = New System.Windows.Forms.CheckBox()
    Me.chk_offer_monday = New System.Windows.Forms.CheckBox()
    Me.ef_offer_m = New GUI_Controls.uc_entry_field()
    Me.opt_offer_weekday = New System.Windows.Forms.RadioButton()
    Me.opt_offer_single_day = New System.Windows.Forms.RadioButton()
    Me.dtp_offer_day = New GUI_Controls.uc_date_picker()
    Me.dtp_time_to = New GUI_Controls.uc_date_picker()
    Me.dtp_time_from = New GUI_Controls.uc_date_picker()
    Me.dg_offers = New GUI_Controls.uc_grid()
    Me.gb_max_number_day_player = New System.Windows.Forms.GroupBox()
    Me.chk_limit_per_operation = New System.Windows.Forms.CheckBox()
    Me.ef_limit_per_operation = New GUI_Controls.uc_entry_field()
    Me.chk_limit_to = New System.Windows.Forms.CheckBox()
    Me.ef_limit = New GUI_Controls.uc_entry_field()
    Me.ef_limit_per_voucher = New GUI_Controls.uc_entry_field()
    Me.opt_gender_male = New System.Windows.Forms.RadioButton()
    Me.opt_gender_female = New System.Windows.Forms.RadioButton()
    Me.chk_by_gender = New System.Windows.Forms.CheckBox()
    Me.tab_filters = New System.Windows.Forms.TabControl()
    Me.tab_gender = New System.Windows.Forms.TabPage()
    Me.opt_gender_male_draw = New System.Windows.Forms.RadioButton()
    Me.opt_gender_female_draw = New System.Windows.Forms.RadioButton()
    Me.chk_by_gender_draw = New System.Windows.Forms.CheckBox()
    Me.tab_level = New System.Windows.Forms.TabPage()
    Me.chk_only_vip = New System.Windows.Forms.CheckBox()
    Me.chk_level_anonymous = New System.Windows.Forms.CheckBox()
    Me.chk_level_04 = New System.Windows.Forms.CheckBox()
    Me.chk_level_03 = New System.Windows.Forms.CheckBox()
    Me.chk_level_02 = New System.Windows.Forms.CheckBox()
    Me.chk_level_01 = New System.Windows.Forms.CheckBox()
    Me.chk_by_level = New System.Windows.Forms.CheckBox()
    Me.uc_provider_filter1 = New GUI_Controls.uc_provider_filter()
    Me.img_icon = New GUI_Controls.uc_image()
    Me.img_image = New GUI_Controls.uc_image()
    Me.lbl_image = New System.Windows.Forms.Label()
    Me.lbl_icon = New System.Windows.Forms.Label()
    Me.lbl_icon_optimun_size = New System.Windows.Forms.Label()
    Me.lbl_image_optimun_size = New System.Windows.Forms.Label()
    Me.chk_visible_on_promobox = New System.Windows.Forms.CheckBox()
    Me.ef_text_on_promobox = New GUI_Controls.uc_entry_field()
    Me.chk_award_on_promobox = New System.Windows.Forms.CheckBox()
    Me.tab_misc = New System.Windows.Forms.TabControl()
    Me.tab_cashin_conditions = New System.Windows.Forms.TabPage()
    Me.chk_min_spent = New System.Windows.Forms.CheckBox()
    Me.chk_first_cash_in_not_constrained = New System.Windows.Forms.CheckBox()
    Me.ef_min_cash_in = New GUI_Controls.uc_entry_field()
    Me.chk_min_played = New System.Windows.Forms.CheckBox()
    Me.ef_min_spent = New GUI_Controls.uc_entry_field()
    Me.lbl_first_cash_in_constrained_2 = New GUI_Controls.uc_text_field()
    Me.ef_min_played = New GUI_Controls.uc_entry_field()
    Me.lbl_cash_in_conditions_2 = New GUI_Controls.uc_text_field()
    Me.lbl_cash_in_conditions_1 = New GUI_Controls.uc_text_field()
    Me.lbl_first_cash_in_constrained_1 = New GUI_Controls.uc_text_field()
    Me.tab_promobox = New System.Windows.Forms.TabPage()
    Me.panel_data.SuspendLayout()
    Me.gb_draws_numeration.SuspendLayout()
    Me.gb_detail.SuspendLayout()
    Me.gb_points_by_level.SuspendLayout()
    Me.tb_winner.SuspendLayout()
    Me.tb_layout.SuspendLayout()
    Me.tab_voucher.SuspendLayout()
    Me.TabPage6.SuspendLayout()
    Me.TabPage1.SuspendLayout()
    Me.TabPage2.SuspendLayout()
    Me.TabPage3.SuspendLayout()
    Me.TabPage4.SuspendLayout()
    Me.TabPage5.SuspendLayout()
    Me.tab_Extra.SuspendLayout()
    Me.tb_offer.SuspendLayout()
    Me.gb_new_offer.SuspendLayout()
    Me.Panel1.SuspendLayout()
    Me.gb_max_number_day_player.SuspendLayout()
    Me.tab_filters.SuspendLayout()
    Me.tab_gender.SuspendLayout()
    Me.tab_level.SuspendLayout()
    Me.tab_misc.SuspendLayout()
    Me.tab_cashin_conditions.SuspendLayout()
    Me.tab_promobox.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.uc_provider_filter1)
    Me.panel_data.Controls.Add(Me.tab_misc)
    Me.panel_data.Controls.Add(Me.lbl_image_optimun_size)
    Me.panel_data.Controls.Add(Me.tab_Extra)
    Me.panel_data.Controls.Add(Me.lbl_image)
    Me.panel_data.Controls.Add(Me.lbl_icon)
    Me.panel_data.Controls.Add(Me.img_icon)
    Me.panel_data.Controls.Add(Me.img_image)
    Me.panel_data.Controls.Add(Me.gb_draws_numeration)
    Me.panel_data.Controls.Add(Me.tab_filters)
    Me.panel_data.Controls.Add(Me.gb_max_number_day_player)
    Me.panel_data.Controls.Add(Me.gb_detail)
    Me.panel_data.Controls.Add(Me.lbl_icon_optimun_size)
    Me.panel_data.Location = New System.Drawing.Point(7, 4)
    Me.panel_data.Size = New System.Drawing.Size(987, 795)
    '
    'Timer1
    '
    Me.Timer1.Interval = 1000
    '
    'uc_entry_draw_numbers_max
    '
    Me.uc_entry_draw_numbers_max.DoubleValue = 0.0R
    Me.uc_entry_draw_numbers_max.IntegerValue = 0
    Me.uc_entry_draw_numbers_max.IsReadOnly = False
    Me.uc_entry_draw_numbers_max.Location = New System.Drawing.Point(9, 44)
    Me.uc_entry_draw_numbers_max.Name = "uc_entry_draw_numbers_max"
    Me.uc_entry_draw_numbers_max.PlaceHolder = Nothing
    Me.uc_entry_draw_numbers_max.ShortcutsEnabled = True
    Me.uc_entry_draw_numbers_max.Size = New System.Drawing.Size(208, 24)
    Me.uc_entry_draw_numbers_max.SufixText = "Sufix Text"
    Me.uc_entry_draw_numbers_max.SufixTextVisible = True
    Me.uc_entry_draw_numbers_max.TabIndex = 1
    Me.uc_entry_draw_numbers_max.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_entry_draw_numbers_max.TextValue = ""
    Me.uc_entry_draw_numbers_max.TextWidth = 90
    Me.uc_entry_draw_numbers_max.Value = ""
    Me.uc_entry_draw_numbers_max.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_entry_draw_numbers_initial
    '
    Me.uc_entry_draw_numbers_initial.DoubleValue = 0.0R
    Me.uc_entry_draw_numbers_initial.IntegerValue = 0
    Me.uc_entry_draw_numbers_initial.IsReadOnly = False
    Me.uc_entry_draw_numbers_initial.Location = New System.Drawing.Point(9, 17)
    Me.uc_entry_draw_numbers_initial.Name = "uc_entry_draw_numbers_initial"
    Me.uc_entry_draw_numbers_initial.PlaceHolder = Nothing
    Me.uc_entry_draw_numbers_initial.ShortcutsEnabled = True
    Me.uc_entry_draw_numbers_initial.Size = New System.Drawing.Size(208, 24)
    Me.uc_entry_draw_numbers_initial.SufixText = "Sufix Text"
    Me.uc_entry_draw_numbers_initial.SufixTextVisible = True
    Me.uc_entry_draw_numbers_initial.TabIndex = 0
    Me.uc_entry_draw_numbers_initial.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_entry_draw_numbers_initial.TextValue = ""
    Me.uc_entry_draw_numbers_initial.TextWidth = 90
    Me.uc_entry_draw_numbers_initial.Value = ""
    Me.uc_entry_draw_numbers_initial.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_last_number
    '
    Me.ef_last_number.DoubleValue = 0.0R
    Me.ef_last_number.Font = New System.Drawing.Font("Verdana", 13.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_last_number.IntegerValue = 0
    Me.ef_last_number.IsReadOnly = True
    Me.ef_last_number.Location = New System.Drawing.Point(99, 71)
    Me.ef_last_number.Name = "ef_last_number"
    Me.ef_last_number.PlaceHolder = Nothing
    Me.ef_last_number.ShortcutsEnabled = True
    Me.ef_last_number.Size = New System.Drawing.Size(118, 33)
    Me.ef_last_number.SufixText = "Sufix Text"
    Me.ef_last_number.SufixTextVisible = True
    Me.ef_last_number.TabIndex = 3
    Me.ef_last_number.TabStop = False
    Me.ef_last_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    Me.ef_last_number.TextValue = ""
    Me.ef_last_number.TextWidth = 0
    Me.ef_last_number.Value = ""
    Me.ef_last_number.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_last_number
    '
    Me.lbl_last_number.AutoSize = True
    Me.lbl_last_number.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_last_number.Location = New System.Drawing.Point(7, 81)
    Me.lbl_last_number.Name = "lbl_last_number"
    Me.lbl_last_number.Size = New System.Drawing.Size(82, 13)
    Me.lbl_last_number.TabIndex = 2
    Me.lbl_last_number.Text = "xLastNumber"
    Me.lbl_last_number.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'gb_draws_numeration
    '
    Me.gb_draws_numeration.Controls.Add(Me.lbl_total_numbers_count)
    Me.gb_draws_numeration.Controls.Add(Me.chk_bingo_format)
    Me.gb_draws_numeration.Controls.Add(Me.lbl_last_number)
    Me.gb_draws_numeration.Controls.Add(Me.ef_last_number)
    Me.gb_draws_numeration.Controls.Add(Me.uc_entry_draw_numbers_initial)
    Me.gb_draws_numeration.Controls.Add(Me.uc_entry_draw_numbers_max)
    Me.gb_draws_numeration.Location = New System.Drawing.Point(393, 6)
    Me.gb_draws_numeration.Name = "gb_draws_numeration"
    Me.gb_draws_numeration.Size = New System.Drawing.Size(229, 190)
    Me.gb_draws_numeration.TabIndex = 3
    Me.gb_draws_numeration.TabStop = False
    Me.gb_draws_numeration.Text = "xNumeration"
    '
    'lbl_total_numbers_count
    '
    Me.lbl_total_numbers_count.Font = New System.Drawing.Font("Courier New", 8.25!)
    Me.lbl_total_numbers_count.ForeColor = System.Drawing.Color.Navy
    Me.lbl_total_numbers_count.Location = New System.Drawing.Point(6, 107)
    Me.lbl_total_numbers_count.Name = "lbl_total_numbers_count"
    Me.lbl_total_numbers_count.Size = New System.Drawing.Size(217, 34)
    Me.lbl_total_numbers_count.TabIndex = 4
    Me.lbl_total_numbers_count.Text = "xDraft Numbers"
    Me.lbl_total_numbers_count.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'chk_bingo_format
    '
    Me.chk_bingo_format.AutoSize = True
    Me.chk_bingo_format.Location = New System.Drawing.Point(85, 158)
    Me.chk_bingo_format.Name = "chk_bingo_format"
    Me.chk_bingo_format.Size = New System.Drawing.Size(109, 17)
    Me.chk_bingo_format.TabIndex = 5
    Me.chk_bingo_format.Text = "xBingo Format"
    Me.chk_bingo_format.UseVisualStyleBackColor = True
    '
    'Uc_text_field1
    '
    Me.Uc_text_field1.AutoSize = True
    Me.Uc_text_field1.IsReadOnly = True
    Me.Uc_text_field1.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.Uc_text_field1.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.Uc_text_field1.Location = New System.Drawing.Point(6, 78)
    Me.Uc_text_field1.Name = "Uc_text_field1"
    Me.Uc_text_field1.Size = New System.Drawing.Size(328, 24)
    Me.Uc_text_field1.SufixText = "Sufix Text"
    Me.Uc_text_field1.SufixTextVisible = True
    Me.Uc_text_field1.TabIndex = 10
    Me.Uc_text_field1.TabStop = False
    Me.Uc_text_field1.TextVisible = False
    Me.Uc_text_field1.TextWidth = 0
    '
    'dtp_start
    '
    Me.dtp_start.Checked = True
    Me.dtp_start.IsReadOnly = False
    Me.dtp_start.Location = New System.Drawing.Point(27, 102)
    Me.dtp_start.Name = "dtp_start"
    Me.dtp_start.ShowCheckBox = False
    Me.dtp_start.ShowUpDown = False
    Me.dtp_start.Size = New System.Drawing.Size(238, 24)
    Me.dtp_start.SufixText = "Sufix Text"
    Me.dtp_start.SufixTextVisible = True
    Me.dtp_start.TabIndex = 4
    Me.dtp_start.Value = New Date(2010, 6, 9, 16, 32, 52, 911)
    '
    'dtp_end
    '
    Me.dtp_end.Checked = True
    Me.dtp_end.IsReadOnly = False
    Me.dtp_end.Location = New System.Drawing.Point(27, 130)
    Me.dtp_end.Name = "dtp_end"
    Me.dtp_end.ShowCheckBox = False
    Me.dtp_end.ShowUpDown = False
    Me.dtp_end.Size = New System.Drawing.Size(238, 24)
    Me.dtp_end.SufixText = "Sufix Text"
    Me.dtp_end.SufixTextVisible = True
    Me.dtp_end.TabIndex = 5
    Me.dtp_end.Value = New Date(2010, 6, 9, 16, 32, 52, 911)
    '
    'cmb_status
    '
    Me.cmb_status.AllowUnlistedValues = False
    Me.cmb_status.AutoCompleteMode = False
    Me.cmb_status.IsReadOnly = False
    Me.cmb_status.Location = New System.Drawing.Point(106, 74)
    Me.cmb_status.Name = "cmb_status"
    Me.cmb_status.SelectedIndex = -1
    Me.cmb_status.Size = New System.Drawing.Size(159, 24)
    Me.cmb_status.SufixText = "Sufix Text"
    Me.cmb_status.SufixTextVisible = True
    Me.cmb_status.TabIndex = 3
    Me.cmb_status.TextCombo = Nothing
    Me.cmb_status.TextVisible = False
    Me.cmb_status.TextWidth = 0
    '
    'lbl_status
    '
    Me.lbl_status.Location = New System.Drawing.Point(9, 77)
    Me.lbl_status.Name = "lbl_status"
    Me.lbl_status.Size = New System.Drawing.Size(96, 19)
    Me.lbl_status.TabIndex = 2
    Me.lbl_status.Text = "xStatus"
    Me.lbl_status.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'ef_draw_name
    '
    Me.ef_draw_name.DoubleValue = 0.0R
    Me.ef_draw_name.IntegerValue = 0
    Me.ef_draw_name.IsReadOnly = False
    Me.ef_draw_name.Location = New System.Drawing.Point(27, 18)
    Me.ef_draw_name.Name = "ef_draw_name"
    Me.ef_draw_name.PlaceHolder = Nothing
    Me.ef_draw_name.ShortcutsEnabled = True
    Me.ef_draw_name.Size = New System.Drawing.Size(307, 24)
    Me.ef_draw_name.SufixText = "Sufix Text"
    Me.ef_draw_name.SufixTextVisible = True
    Me.ef_draw_name.TabIndex = 0
    Me.ef_draw_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_draw_name.TextValue = ""
    Me.ef_draw_name.Value = ""
    Me.ef_draw_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_number_price
    '
    Me.ef_number_price.DoubleValue = 0.0R
    Me.ef_number_price.IntegerValue = 0
    Me.ef_number_price.IsReadOnly = False
    Me.ef_number_price.Location = New System.Drawing.Point(48, 158)
    Me.ef_number_price.Name = "ef_number_price"
    Me.ef_number_price.PlaceHolder = Nothing
    Me.ef_number_price.ShortcutsEnabled = True
    Me.ef_number_price.Size = New System.Drawing.Size(286, 24)
    Me.ef_number_price.SufixText = "xPlayed"
    Me.ef_number_price.SufixTextVisible = True
    Me.ef_number_price.SufixTextWidth = 70
    Me.ef_number_price.TabIndex = 6
    Me.ef_number_price.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_number_price.TextValue = ""
    Me.ef_number_price.TextWidth = 110
    Me.ef_number_price.Value = ""
    Me.ef_number_price.ValueForeColor = System.Drawing.Color.Blue
    '
    'cmb_draw_type
    '
    Me.cmb_draw_type.AllowUnlistedValues = False
    Me.cmb_draw_type.AutoCompleteMode = False
    Me.cmb_draw_type.IsReadOnly = False
    Me.cmb_draw_type.Location = New System.Drawing.Point(16, 47)
    Me.cmb_draw_type.Name = "cmb_draw_type"
    Me.cmb_draw_type.SelectedIndex = -1
    Me.cmb_draw_type.Size = New System.Drawing.Size(320, 24)
    Me.cmb_draw_type.SufixText = "Sufix Text"
    Me.cmb_draw_type.SufixTextVisible = True
    Me.cmb_draw_type.TabIndex = 1
    Me.cmb_draw_type.TextCombo = Nothing
    Me.cmb_draw_type.TextWidth = 90
    '
    'gb_detail
    '
    Me.gb_detail.Controls.Add(Me.lbl_price_ratio)
    Me.gb_detail.Controls.Add(Me.gb_points_by_level)
    Me.gb_detail.Controls.Add(Me.cmb_draw_type)
    Me.gb_detail.Controls.Add(Me.ef_number_price)
    Me.gb_detail.Controls.Add(Me.ef_draw_name)
    Me.gb_detail.Controls.Add(Me.lbl_status)
    Me.gb_detail.Controls.Add(Me.cmb_status)
    Me.gb_detail.Controls.Add(Me.dtp_end)
    Me.gb_detail.Controls.Add(Me.dtp_start)
    Me.gb_detail.Controls.Add(Me.Uc_text_field1)
    Me.gb_detail.Location = New System.Drawing.Point(1, 6)
    Me.gb_detail.Name = "gb_detail"
    Me.gb_detail.Size = New System.Drawing.Size(386, 379)
    Me.gb_detail.TabIndex = 0
    Me.gb_detail.TabStop = False
    Me.gb_detail.Text = "xDetail"
    '
    'lbl_price_ratio
    '
    Me.lbl_price_ratio.Font = New System.Drawing.Font("Courier New", 8.25!)
    Me.lbl_price_ratio.ForeColor = System.Drawing.Color.Navy
    Me.lbl_price_ratio.Location = New System.Drawing.Point(5, 185)
    Me.lbl_price_ratio.Name = "lbl_price_ratio"
    Me.lbl_price_ratio.Size = New System.Drawing.Size(374, 29)
    Me.lbl_price_ratio.TabIndex = 7
    Me.lbl_price_ratio.Text = "xNumber Price"
    '
    'gb_points_by_level
    '
    Me.gb_points_by_level.Controls.Add(Me.lbl_info_points_level)
    Me.gb_points_by_level.Controls.Add(Me.chk_points_level4)
    Me.gb_points_by_level.Controls.Add(Me.chk_points_level3)
    Me.gb_points_by_level.Controls.Add(Me.chk_points_level2)
    Me.gb_points_by_level.Controls.Add(Me.chk_points_level1)
    Me.gb_points_by_level.Controls.Add(Me.ef_points_level4)
    Me.gb_points_by_level.Controls.Add(Me.ef_points_level1)
    Me.gb_points_by_level.Controls.Add(Me.ef_points_level3)
    Me.gb_points_by_level.Controls.Add(Me.ef_points_level2)
    Me.gb_points_by_level.Location = New System.Drawing.Point(6, 217)
    Me.gb_points_by_level.Name = "gb_points_by_level"
    Me.gb_points_by_level.Size = New System.Drawing.Size(374, 159)
    Me.gb_points_by_level.TabIndex = 8
    Me.gb_points_by_level.TabStop = False
    Me.gb_points_by_level.Text = "GroupBox1"
    '
    'lbl_info_points_level
    '
    Me.lbl_info_points_level.Font = New System.Drawing.Font("Courier New", 8.25!)
    Me.lbl_info_points_level.ForeColor = System.Drawing.Color.Navy
    Me.lbl_info_points_level.Location = New System.Drawing.Point(6, 114)
    Me.lbl_info_points_level.Name = "lbl_info_points_level"
    Me.lbl_info_points_level.Size = New System.Drawing.Size(362, 40)
    Me.lbl_info_points_level.TabIndex = 8
    Me.lbl_info_points_level.Text = "xDescription points"
    '
    'chk_points_level4
    '
    Me.chk_points_level4.AutoSize = True
    Me.chk_points_level4.Location = New System.Drawing.Point(59, 95)
    Me.chk_points_level4.Name = "chk_points_level4"
    Me.chk_points_level4.Size = New System.Drawing.Size(70, 17)
    Me.chk_points_level4.TabIndex = 6
    Me.chk_points_level4.Text = "xLevel4"
    Me.chk_points_level4.UseVisualStyleBackColor = True
    '
    'chk_points_level3
    '
    Me.chk_points_level3.AutoSize = True
    Me.chk_points_level3.Location = New System.Drawing.Point(59, 70)
    Me.chk_points_level3.Name = "chk_points_level3"
    Me.chk_points_level3.Size = New System.Drawing.Size(70, 17)
    Me.chk_points_level3.TabIndex = 4
    Me.chk_points_level3.Text = "xLevel3"
    Me.chk_points_level3.UseVisualStyleBackColor = True
    '
    'chk_points_level2
    '
    Me.chk_points_level2.AutoSize = True
    Me.chk_points_level2.Location = New System.Drawing.Point(59, 45)
    Me.chk_points_level2.Name = "chk_points_level2"
    Me.chk_points_level2.Size = New System.Drawing.Size(70, 17)
    Me.chk_points_level2.TabIndex = 2
    Me.chk_points_level2.Text = "xLevel2"
    Me.chk_points_level2.UseVisualStyleBackColor = True
    '
    'chk_points_level1
    '
    Me.chk_points_level1.AutoSize = True
    Me.chk_points_level1.Location = New System.Drawing.Point(59, 20)
    Me.chk_points_level1.Name = "chk_points_level1"
    Me.chk_points_level1.Size = New System.Drawing.Size(70, 17)
    Me.chk_points_level1.TabIndex = 0
    Me.chk_points_level1.Text = "xLevel1"
    Me.chk_points_level1.UseVisualStyleBackColor = True
    '
    'ef_points_level4
    '
    Me.ef_points_level4.DoubleValue = 0.0R
    Me.ef_points_level4.IntegerValue = 0
    Me.ef_points_level4.IsReadOnly = False
    Me.ef_points_level4.Location = New System.Drawing.Point(168, 90)
    Me.ef_points_level4.Name = "ef_points_level4"
    Me.ef_points_level4.PlaceHolder = Nothing
    Me.ef_points_level4.ShortcutsEnabled = True
    Me.ef_points_level4.Size = New System.Drawing.Size(148, 24)
    Me.ef_points_level4.SufixText = "xPoints"
    Me.ef_points_level4.SufixTextVisible = True
    Me.ef_points_level4.SufixTextWidth = 60
    Me.ef_points_level4.TabIndex = 7
    Me.ef_points_level4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_points_level4.TextValue = ""
    Me.ef_points_level4.TextWidth = 0
    Me.ef_points_level4.Value = ""
    Me.ef_points_level4.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_points_level1
    '
    Me.ef_points_level1.DoubleValue = 0.0R
    Me.ef_points_level1.IntegerValue = 0
    Me.ef_points_level1.IsReadOnly = False
    Me.ef_points_level1.Location = New System.Drawing.Point(168, 15)
    Me.ef_points_level1.Name = "ef_points_level1"
    Me.ef_points_level1.PlaceHolder = Nothing
    Me.ef_points_level1.ShortcutsEnabled = True
    Me.ef_points_level1.Size = New System.Drawing.Size(148, 24)
    Me.ef_points_level1.SufixText = "xPoints"
    Me.ef_points_level1.SufixTextVisible = True
    Me.ef_points_level1.SufixTextWidth = 60
    Me.ef_points_level1.TabIndex = 1
    Me.ef_points_level1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_points_level1.TextValue = ""
    Me.ef_points_level1.TextWidth = 0
    Me.ef_points_level1.Value = ""
    Me.ef_points_level1.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_points_level3
    '
    Me.ef_points_level3.DoubleValue = 0.0R
    Me.ef_points_level3.IntegerValue = 0
    Me.ef_points_level3.IsReadOnly = False
    Me.ef_points_level3.Location = New System.Drawing.Point(168, 65)
    Me.ef_points_level3.Name = "ef_points_level3"
    Me.ef_points_level3.PlaceHolder = Nothing
    Me.ef_points_level3.ShortcutsEnabled = True
    Me.ef_points_level3.Size = New System.Drawing.Size(148, 24)
    Me.ef_points_level3.SufixText = "xPoints"
    Me.ef_points_level3.SufixTextVisible = True
    Me.ef_points_level3.SufixTextWidth = 60
    Me.ef_points_level3.TabIndex = 5
    Me.ef_points_level3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_points_level3.TextValue = ""
    Me.ef_points_level3.TextWidth = 0
    Me.ef_points_level3.Value = ""
    Me.ef_points_level3.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_points_level2
    '
    Me.ef_points_level2.DoubleValue = 0.0R
    Me.ef_points_level2.IntegerValue = 0
    Me.ef_points_level2.IsReadOnly = False
    Me.ef_points_level2.Location = New System.Drawing.Point(168, 40)
    Me.ef_points_level2.Name = "ef_points_level2"
    Me.ef_points_level2.PlaceHolder = Nothing
    Me.ef_points_level2.ShortcutsEnabled = True
    Me.ef_points_level2.Size = New System.Drawing.Size(148, 24)
    Me.ef_points_level2.SufixText = "xPoints"
    Me.ef_points_level2.SufixTextVisible = True
    Me.ef_points_level2.SufixTextWidth = 60
    Me.ef_points_level2.TabIndex = 3
    Me.ef_points_level2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_points_level2.TextValue = ""
    Me.ef_points_level2.TextWidth = 0
    Me.ef_points_level2.Value = ""
    Me.ef_points_level2.ValueForeColor = System.Drawing.Color.Blue
    '
    'tb_winner
    '
    Me.tb_winner.Controls.Add(Me.btn_voucher)
    Me.tb_winner.Controls.Add(Me.ef_draw_ticket)
    Me.tb_winner.Controls.Add(Me.ef_account)
    Me.tb_winner.Controls.Add(Me.ef_voucher)
    Me.tb_winner.Controls.Add(Me.ef_winner_name)
    Me.tb_winner.Controls.Add(Me.ef_winning_number)
    Me.tb_winner.Controls.Add(Me.btn_check_winner)
    Me.tb_winner.Location = New System.Drawing.Point(4, 22)
    Me.tb_winner.Name = "tb_winner"
    Me.tb_winner.Size = New System.Drawing.Size(582, 405)
    Me.tb_winner.TabIndex = 2
    Me.tb_winner.Text = "Winner"
    Me.tb_winner.UseVisualStyleBackColor = True
    '
    'btn_voucher
    '
    Me.btn_voucher.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_voucher.Location = New System.Drawing.Point(370, 143)
    Me.btn_voucher.Name = "btn_voucher"
    Me.btn_voucher.Size = New System.Drawing.Size(90, 30)
    Me.btn_voucher.TabIndex = 6
    Me.btn_voucher.ToolTipped = False
    Me.btn_voucher.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'ef_draw_ticket
    '
    Me.ef_draw_ticket.DoubleValue = 0.0R
    Me.ef_draw_ticket.IntegerValue = 0
    Me.ef_draw_ticket.IsReadOnly = False
    Me.ef_draw_ticket.Location = New System.Drawing.Point(65, 150)
    Me.ef_draw_ticket.Name = "ef_draw_ticket"
    Me.ef_draw_ticket.PlaceHolder = Nothing
    Me.ef_draw_ticket.ShortcutsEnabled = True
    Me.ef_draw_ticket.Size = New System.Drawing.Size(299, 24)
    Me.ef_draw_ticket.SufixText = "Sufix Text"
    Me.ef_draw_ticket.TabIndex = 5
    Me.ef_draw_ticket.TabStop = False
    Me.ef_draw_ticket.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_draw_ticket.TextValue = ""
    Me.ef_draw_ticket.TextVisible = False
    Me.ef_draw_ticket.TextWidth = 137
    Me.ef_draw_ticket.Value = ""
    Me.ef_draw_ticket.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_account
    '
    Me.ef_account.DoubleValue = 0.0R
    Me.ef_account.IntegerValue = 0
    Me.ef_account.IsReadOnly = False
    Me.ef_account.Location = New System.Drawing.Point(122, 120)
    Me.ef_account.Name = "ef_account"
    Me.ef_account.PlaceHolder = Nothing
    Me.ef_account.ShortcutsEnabled = True
    Me.ef_account.Size = New System.Drawing.Size(242, 24)
    Me.ef_account.SufixText = "Sufix Text"
    Me.ef_account.TabIndex = 4
    Me.ef_account.TabStop = False
    Me.ef_account.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_account.TextValue = ""
    Me.ef_account.TextVisible = False
    Me.ef_account.Value = ""
    Me.ef_account.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_voucher
    '
    Me.ef_voucher.DoubleValue = 0.0R
    Me.ef_voucher.IntegerValue = 0
    Me.ef_voucher.IsReadOnly = False
    Me.ef_voucher.Location = New System.Drawing.Point(122, 90)
    Me.ef_voucher.Name = "ef_voucher"
    Me.ef_voucher.PlaceHolder = Nothing
    Me.ef_voucher.ShortcutsEnabled = True
    Me.ef_voucher.Size = New System.Drawing.Size(242, 24)
    Me.ef_voucher.SufixText = "Sufix Text"
    Me.ef_voucher.TabIndex = 3
    Me.ef_voucher.TabStop = False
    Me.ef_voucher.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_voucher.TextValue = ""
    Me.ef_voucher.TextVisible = False
    Me.ef_voucher.Value = ""
    Me.ef_voucher.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_winner_name
    '
    Me.ef_winner_name.DoubleValue = 0.0R
    Me.ef_winner_name.IntegerValue = 0
    Me.ef_winner_name.IsReadOnly = False
    Me.ef_winner_name.Location = New System.Drawing.Point(122, 60)
    Me.ef_winner_name.Name = "ef_winner_name"
    Me.ef_winner_name.PlaceHolder = Nothing
    Me.ef_winner_name.ShortcutsEnabled = True
    Me.ef_winner_name.Size = New System.Drawing.Size(242, 24)
    Me.ef_winner_name.SufixText = "Sufix Text"
    Me.ef_winner_name.TabIndex = 2
    Me.ef_winner_name.TabStop = False
    Me.ef_winner_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_winner_name.TextValue = ""
    Me.ef_winner_name.TextVisible = False
    Me.ef_winner_name.Value = ""
    Me.ef_winner_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_winning_number
    '
    Me.ef_winning_number.DoubleValue = 0.0R
    Me.ef_winning_number.IntegerValue = 0
    Me.ef_winning_number.IsReadOnly = False
    Me.ef_winning_number.Location = New System.Drawing.Point(122, 23)
    Me.ef_winning_number.Name = "ef_winning_number"
    Me.ef_winning_number.PlaceHolder = Nothing
    Me.ef_winning_number.ShortcutsEnabled = True
    Me.ef_winning_number.Size = New System.Drawing.Size(242, 24)
    Me.ef_winning_number.SufixText = "Sufix Text"
    Me.ef_winning_number.TabIndex = 0
    Me.ef_winning_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_winning_number.TextValue = ""
    Me.ef_winning_number.TextVisible = False
    Me.ef_winning_number.Value = ""
    Me.ef_winning_number.ValueForeColor = System.Drawing.Color.Blue
    '
    'btn_check_winner
    '
    Me.btn_check_winner.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_check_winner.Location = New System.Drawing.Point(370, 19)
    Me.btn_check_winner.Name = "btn_check_winner"
    Me.btn_check_winner.Size = New System.Drawing.Size(90, 30)
    Me.btn_check_winner.TabIndex = 1
    Me.btn_check_winner.ToolTipped = False
    Me.btn_check_winner.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'tb_layout
    '
    Me.tb_layout.Controls.Add(Me.tab_voucher)
    Me.tb_layout.Location = New System.Drawing.Point(4, 22)
    Me.tb_layout.Name = "tb_layout"
    Me.tb_layout.Padding = New System.Windows.Forms.Padding(3)
    Me.tb_layout.Size = New System.Drawing.Size(582, 405)
    Me.tb_layout.TabIndex = 1
    Me.tb_layout.Text = "Ticket layout"
    Me.tb_layout.UseVisualStyleBackColor = True
    '
    'tab_voucher
    '
    Me.tab_voucher.Controls.Add(Me.TabPage6)
    Me.tab_voucher.Controls.Add(Me.TabPage1)
    Me.tab_voucher.Controls.Add(Me.TabPage2)
    Me.tab_voucher.Controls.Add(Me.TabPage3)
    Me.tab_voucher.Controls.Add(Me.TabPage4)
    Me.tab_voucher.Controls.Add(Me.TabPage5)
    Me.tab_voucher.Location = New System.Drawing.Point(57, 9)
    Me.tab_voucher.Name = "tab_voucher"
    Me.tab_voucher.SelectedIndex = 0
    Me.tab_voucher.Size = New System.Drawing.Size(464, 384)
    Me.tab_voucher.TabIndex = 10
    '
    'TabPage6
    '
    Me.TabPage6.Controls.Add(Me.wb_voucher)
    Me.TabPage6.Location = New System.Drawing.Point(4, 22)
    Me.TabPage6.Name = "TabPage6"
    Me.TabPage6.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage6.Size = New System.Drawing.Size(456, 358)
    Me.TabPage6.TabIndex = 5
    Me.TabPage6.Text = "TabPage6"
    Me.TabPage6.UseVisualStyleBackColor = True
    '
    'wb_voucher
    '
    Me.wb_voucher.Dock = System.Windows.Forms.DockStyle.Fill
    Me.wb_voucher.Location = New System.Drawing.Point(3, 3)
    Me.wb_voucher.MinimumSize = New System.Drawing.Size(23, 20)
    Me.wb_voucher.Name = "wb_voucher"
    Me.wb_voucher.Size = New System.Drawing.Size(450, 352)
    Me.wb_voucher.TabIndex = 0
    '
    'TabPage1
    '
    Me.TabPage1.Controls.Add(Me.ef_header)
    Me.TabPage1.Location = New System.Drawing.Point(4, 22)
    Me.TabPage1.Name = "TabPage1"
    Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage1.Size = New System.Drawing.Size(456, 358)
    Me.TabPage1.TabIndex = 0
    Me.TabPage1.Text = "TabPage1"
    Me.TabPage1.UseVisualStyleBackColor = True
    '
    'ef_header
    '
    Me.ef_header.Dock = System.Windows.Forms.DockStyle.Fill
    Me.ef_header.Location = New System.Drawing.Point(3, 3)
    Me.ef_header.Multiline = True
    Me.ef_header.Name = "ef_header"
    Me.ef_header.Size = New System.Drawing.Size(450, 352)
    Me.ef_header.TabIndex = 0
    '
    'TabPage2
    '
    Me.TabPage2.Controls.Add(Me.ef_footer)
    Me.TabPage2.Location = New System.Drawing.Point(4, 22)
    Me.TabPage2.Name = "TabPage2"
    Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage2.Size = New System.Drawing.Size(456, 358)
    Me.TabPage2.TabIndex = 1
    Me.TabPage2.Text = "TabPage2"
    Me.TabPage2.UseVisualStyleBackColor = True
    '
    'ef_footer
    '
    Me.ef_footer.Dock = System.Windows.Forms.DockStyle.Fill
    Me.ef_footer.Location = New System.Drawing.Point(3, 3)
    Me.ef_footer.Multiline = True
    Me.ef_footer.Name = "ef_footer"
    Me.ef_footer.Size = New System.Drawing.Size(450, 352)
    Me.ef_footer.TabIndex = 1
    '
    'TabPage3
    '
    Me.TabPage3.Controls.Add(Me.ef_detail1)
    Me.TabPage3.Location = New System.Drawing.Point(4, 22)
    Me.TabPage3.Name = "TabPage3"
    Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage3.Size = New System.Drawing.Size(456, 358)
    Me.TabPage3.TabIndex = 2
    Me.TabPage3.Text = "TabPage3"
    Me.TabPage3.UseVisualStyleBackColor = True
    '
    'ef_detail1
    '
    Me.ef_detail1.Dock = System.Windows.Forms.DockStyle.Fill
    Me.ef_detail1.Location = New System.Drawing.Point(3, 3)
    Me.ef_detail1.Multiline = True
    Me.ef_detail1.Name = "ef_detail1"
    Me.ef_detail1.Size = New System.Drawing.Size(450, 352)
    Me.ef_detail1.TabIndex = 1
    '
    'TabPage4
    '
    Me.TabPage4.Controls.Add(Me.ef_detail2)
    Me.TabPage4.Location = New System.Drawing.Point(4, 22)
    Me.TabPage4.Name = "TabPage4"
    Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage4.Size = New System.Drawing.Size(456, 358)
    Me.TabPage4.TabIndex = 3
    Me.TabPage4.Text = "TabPage4"
    Me.TabPage4.UseVisualStyleBackColor = True
    '
    'ef_detail2
    '
    Me.ef_detail2.Dock = System.Windows.Forms.DockStyle.Fill
    Me.ef_detail2.Location = New System.Drawing.Point(3, 3)
    Me.ef_detail2.Multiline = True
    Me.ef_detail2.Name = "ef_detail2"
    Me.ef_detail2.Size = New System.Drawing.Size(450, 352)
    Me.ef_detail2.TabIndex = 1
    '
    'TabPage5
    '
    Me.TabPage5.Controls.Add(Me.ef_detail3)
    Me.TabPage5.Location = New System.Drawing.Point(4, 22)
    Me.TabPage5.Name = "TabPage5"
    Me.TabPage5.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage5.Size = New System.Drawing.Size(456, 358)
    Me.TabPage5.TabIndex = 4
    Me.TabPage5.Text = "TabPage5"
    Me.TabPage5.UseVisualStyleBackColor = True
    '
    'ef_detail3
    '
    Me.ef_detail3.Dock = System.Windows.Forms.DockStyle.Fill
    Me.ef_detail3.Location = New System.Drawing.Point(3, 3)
    Me.ef_detail3.Multiline = True
    Me.ef_detail3.Name = "ef_detail3"
    Me.ef_detail3.Size = New System.Drawing.Size(450, 352)
    Me.ef_detail3.TabIndex = 1
    '
    'tab_Extra
    '
    Me.tab_Extra.Controls.Add(Me.tb_offer)
    Me.tab_Extra.Controls.Add(Me.tb_layout)
    Me.tab_Extra.Controls.Add(Me.tb_winner)
    Me.tab_Extra.Location = New System.Drawing.Point(394, 364)
    Me.tab_Extra.Name = "tab_Extra"
    Me.tab_Extra.SelectedIndex = 0
    Me.tab_Extra.Size = New System.Drawing.Size(590, 431)
    Me.tab_Extra.TabIndex = 11
    '
    'tb_offer
    '
    Me.tb_offer.Controls.Add(Me.lbl_msg_order_offers)
    Me.tb_offer.Controls.Add(Me.lbl_not_apply)
    Me.tb_offer.Controls.Add(Me.btn_offer_remove)
    Me.tb_offer.Controls.Add(Me.gb_new_offer)
    Me.tb_offer.Controls.Add(Me.dg_offers)
    Me.tb_offer.Location = New System.Drawing.Point(4, 22)
    Me.tb_offer.Name = "tb_offer"
    Me.tb_offer.Size = New System.Drawing.Size(582, 405)
    Me.tb_offer.TabIndex = 3
    Me.tb_offer.Text = "Offer"
    Me.tb_offer.UseVisualStyleBackColor = True
    '
    'lbl_msg_order_offers
    '
    Me.lbl_msg_order_offers.AutoSize = True
    Me.lbl_msg_order_offers.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_msg_order_offers.IsReadOnly = True
    Me.lbl_msg_order_offers.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_msg_order_offers.LabelForeColor = System.Drawing.Color.Navy
    Me.lbl_msg_order_offers.Location = New System.Drawing.Point(5, 146)
    Me.lbl_msg_order_offers.Name = "lbl_msg_order_offers"
    Me.lbl_msg_order_offers.Size = New System.Drawing.Size(396, 24)
    Me.lbl_msg_order_offers.SufixText = "Sufix Text"
    Me.lbl_msg_order_offers.SufixTextVisible = True
    Me.lbl_msg_order_offers.TabIndex = 2
    Me.lbl_msg_order_offers.TabStop = False
    Me.lbl_msg_order_offers.TextVisible = False
    Me.lbl_msg_order_offers.TextWidth = 0
    Me.lbl_msg_order_offers.Value = "xThe latest applicable offer is the one applied."
    '
    'lbl_not_apply
    '
    Me.lbl_not_apply.AutoSize = True
    Me.lbl_not_apply.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_not_apply.IsReadOnly = True
    Me.lbl_not_apply.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_not_apply.LabelForeColor = System.Drawing.Color.Navy
    Me.lbl_not_apply.Location = New System.Drawing.Point(5, 168)
    Me.lbl_not_apply.Name = "lbl_not_apply"
    Me.lbl_not_apply.Size = New System.Drawing.Size(461, 24)
    Me.lbl_not_apply.SufixText = "Sufix Text"
    Me.lbl_not_apply.SufixTextVisible = True
    Me.lbl_not_apply.TabIndex = 3
    Me.lbl_not_apply.TabStop = False
    Me.lbl_not_apply.TextVisible = False
    Me.lbl_not_apply.TextWidth = 0
    Me.lbl_not_apply.Value = "xOffers don't apply..."
    '
    'btn_offer_remove
    '
    Me.btn_offer_remove.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_offer_remove.Location = New System.Drawing.Point(403, 147)
    Me.btn_offer_remove.Name = "btn_offer_remove"
    Me.btn_offer_remove.Size = New System.Drawing.Size(63, 21)
    Me.btn_offer_remove.TabIndex = 1
    Me.btn_offer_remove.ToolTipped = False
    Me.btn_offer_remove.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.SMALL
    '
    'gb_new_offer
    '
    Me.gb_new_offer.Controls.Add(Me.ef_offer_n)
    Me.gb_new_offer.Controls.Add(Me.Panel1)
    Me.gb_new_offer.Controls.Add(Me.lblTo)
    Me.gb_new_offer.Controls.Add(Me.lblFrom)
    Me.gb_new_offer.Controls.Add(Me.btn_check_all_days)
    Me.gb_new_offer.Controls.Add(Me.chk_offer_sunday)
    Me.gb_new_offer.Controls.Add(Me.chk_offer_saturday)
    Me.gb_new_offer.Controls.Add(Me.btn_offer_add)
    Me.gb_new_offer.Controls.Add(Me.chk_offer_friday)
    Me.gb_new_offer.Controls.Add(Me.chk_offer_thursday)
    Me.gb_new_offer.Controls.Add(Me.chk_offer_wednesday)
    Me.gb_new_offer.Controls.Add(Me.chk_offer_tuesday)
    Me.gb_new_offer.Controls.Add(Me.chk_offer_monday)
    Me.gb_new_offer.Controls.Add(Me.ef_offer_m)
    Me.gb_new_offer.Controls.Add(Me.opt_offer_weekday)
    Me.gb_new_offer.Controls.Add(Me.opt_offer_single_day)
    Me.gb_new_offer.Controls.Add(Me.dtp_offer_day)
    Me.gb_new_offer.Controls.Add(Me.dtp_time_to)
    Me.gb_new_offer.Controls.Add(Me.dtp_time_from)
    Me.gb_new_offer.Location = New System.Drawing.Point(6, 193)
    Me.gb_new_offer.Name = "gb_new_offer"
    Me.gb_new_offer.Size = New System.Drawing.Size(573, 196)
    Me.gb_new_offer.TabIndex = 4
    Me.gb_new_offer.TabStop = False
    Me.gb_new_offer.Text = "xNewOffer"
    '
    'ef_offer_n
    '
    Me.ef_offer_n.DoubleValue = 0.0R
    Me.ef_offer_n.IntegerValue = 0
    Me.ef_offer_n.IsReadOnly = False
    Me.ef_offer_n.Location = New System.Drawing.Point(17, 43)
    Me.ef_offer_n.Name = "ef_offer_n"
    Me.ef_offer_n.PlaceHolder = Nothing
    Me.ef_offer_n.ShortcutsEnabled = True
    Me.ef_offer_n.Size = New System.Drawing.Size(40, 24)
    Me.ef_offer_n.SufixText = "Sufix Text"
    Me.ef_offer_n.SufixTextVisible = True
    Me.ef_offer_n.TabIndex = 0
    Me.ef_offer_n.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_offer_n.TextValue = ""
    Me.ef_offer_n.TextVisible = False
    Me.ef_offer_n.TextWidth = 0
    Me.ef_offer_n.Value = ""
    Me.ef_offer_n.ValueForeColor = System.Drawing.Color.Blue
    '
    'Panel1
    '
    Me.Panel1.Controls.Add(Me.btn_reset)
    Me.Panel1.Controls.Add(Me.chk_by_gender_offer)
    Me.Panel1.Controls.Add(Me.opt_gender_male_offer)
    Me.Panel1.Controls.Add(Me.opt_gender_female_offer)
    Me.Panel1.Location = New System.Drawing.Point(137, 16)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(416, 73)
    Me.Panel1.TabIndex = 2
    '
    'btn_reset
    '
    Me.btn_reset.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_reset.Location = New System.Drawing.Point(343, 5)
    Me.btn_reset.Name = "btn_reset"
    Me.btn_reset.Size = New System.Drawing.Size(63, 21)
    Me.btn_reset.TabIndex = 3
    Me.btn_reset.ToolTipped = False
    Me.btn_reset.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.SMALL
    '
    'chk_by_gender_offer
    '
    Me.chk_by_gender_offer.AutoSize = True
    Me.chk_by_gender_offer.Location = New System.Drawing.Point(16, 5)
    Me.chk_by_gender_offer.Name = "chk_by_gender_offer"
    Me.chk_by_gender_offer.Size = New System.Drawing.Size(68, 17)
    Me.chk_by_gender_offer.TabIndex = 0
    Me.chk_by_gender_offer.Text = "xActive"
    Me.chk_by_gender_offer.UseVisualStyleBackColor = True
    '
    'opt_gender_male_offer
    '
    Me.opt_gender_male_offer.Location = New System.Drawing.Point(64, 28)
    Me.opt_gender_male_offer.Name = "opt_gender_male_offer"
    Me.opt_gender_male_offer.Size = New System.Drawing.Size(187, 17)
    Me.opt_gender_male_offer.TabIndex = 1
    Me.opt_gender_male_offer.Text = "xApplies to Men Only"
    '
    'opt_gender_female_offer
    '
    Me.opt_gender_female_offer.Location = New System.Drawing.Point(64, 50)
    Me.opt_gender_female_offer.Name = "opt_gender_female_offer"
    Me.opt_gender_female_offer.Size = New System.Drawing.Size(194, 17)
    Me.opt_gender_female_offer.TabIndex = 2
    Me.opt_gender_female_offer.Text = "xApplies to Women Only"
    '
    'lblTo
    '
    Me.lblTo.Location = New System.Drawing.Point(271, 169)
    Me.lblTo.Name = "lblTo"
    Me.lblTo.Size = New System.Drawing.Size(43, 19)
    Me.lblTo.TabIndex = 15
    Me.lblTo.Text = "xTo"
    Me.lblTo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lblFrom
    '
    Me.lblFrom.Location = New System.Drawing.Point(121, 169)
    Me.lblFrom.Name = "lblFrom"
    Me.lblFrom.Size = New System.Drawing.Size(43, 19)
    Me.lblFrom.TabIndex = 13
    Me.lblFrom.Text = "xFrom"
    Me.lblFrom.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'btn_check_all_days
    '
    Me.btn_check_all_days.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_check_all_days.Location = New System.Drawing.Point(480, 104)
    Me.btn_check_all_days.Name = "btn_check_all_days"
    Me.btn_check_all_days.Size = New System.Drawing.Size(63, 21)
    Me.btn_check_all_days.TabIndex = 10
    Me.btn_check_all_days.ToolTipped = False
    Me.btn_check_all_days.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.SMALL
    '
    'chk_offer_sunday
    '
    Me.chk_offer_sunday.CheckAlign = System.Drawing.ContentAlignment.BottomCenter
    Me.chk_offer_sunday.Location = New System.Drawing.Point(373, 91)
    Me.chk_offer_sunday.Name = "chk_offer_sunday"
    Me.chk_offer_sunday.Size = New System.Drawing.Size(36, 32)
    Me.chk_offer_sunday.TabIndex = 9
    Me.chk_offer_sunday.Text = "SU"
    Me.chk_offer_sunday.TextAlign = System.Drawing.ContentAlignment.BottomCenter
    Me.chk_offer_sunday.UseVisualStyleBackColor = True
    '
    'chk_offer_saturday
    '
    Me.chk_offer_saturday.CheckAlign = System.Drawing.ContentAlignment.BottomCenter
    Me.chk_offer_saturday.Location = New System.Drawing.Point(343, 91)
    Me.chk_offer_saturday.Name = "chk_offer_saturday"
    Me.chk_offer_saturday.Size = New System.Drawing.Size(32, 32)
    Me.chk_offer_saturday.TabIndex = 8
    Me.chk_offer_saturday.Text = "SA"
    Me.chk_offer_saturday.TextAlign = System.Drawing.ContentAlignment.BottomCenter
    Me.chk_offer_saturday.UseVisualStyleBackColor = True
    '
    'btn_offer_add
    '
    Me.btn_offer_add.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_offer_add.Location = New System.Drawing.Point(480, 166)
    Me.btn_offer_add.Name = "btn_offer_add"
    Me.btn_offer_add.Size = New System.Drawing.Size(63, 21)
    Me.btn_offer_add.TabIndex = 17
    Me.btn_offer_add.ToolTipped = False
    Me.btn_offer_add.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.SMALL
    '
    'chk_offer_friday
    '
    Me.chk_offer_friday.CheckAlign = System.Drawing.ContentAlignment.BottomCenter
    Me.chk_offer_friday.Location = New System.Drawing.Point(313, 91)
    Me.chk_offer_friday.Name = "chk_offer_friday"
    Me.chk_offer_friday.Size = New System.Drawing.Size(32, 32)
    Me.chk_offer_friday.TabIndex = 7
    Me.chk_offer_friday.Text = "FR"
    Me.chk_offer_friday.TextAlign = System.Drawing.ContentAlignment.BottomCenter
    Me.chk_offer_friday.UseVisualStyleBackColor = True
    '
    'chk_offer_thursday
    '
    Me.chk_offer_thursday.CheckAlign = System.Drawing.ContentAlignment.BottomCenter
    Me.chk_offer_thursday.Location = New System.Drawing.Point(283, 91)
    Me.chk_offer_thursday.Name = "chk_offer_thursday"
    Me.chk_offer_thursday.Size = New System.Drawing.Size(32, 32)
    Me.chk_offer_thursday.TabIndex = 6
    Me.chk_offer_thursday.Text = "TH"
    Me.chk_offer_thursday.TextAlign = System.Drawing.ContentAlignment.BottomCenter
    Me.chk_offer_thursday.UseVisualStyleBackColor = True
    '
    'chk_offer_wednesday
    '
    Me.chk_offer_wednesday.CheckAlign = System.Drawing.ContentAlignment.BottomCenter
    Me.chk_offer_wednesday.Location = New System.Drawing.Point(253, 91)
    Me.chk_offer_wednesday.Name = "chk_offer_wednesday"
    Me.chk_offer_wednesday.Size = New System.Drawing.Size(32, 32)
    Me.chk_offer_wednesday.TabIndex = 5
    Me.chk_offer_wednesday.Text = "WE"
    Me.chk_offer_wednesday.TextAlign = System.Drawing.ContentAlignment.BottomCenter
    Me.chk_offer_wednesday.UseVisualStyleBackColor = True
    '
    'chk_offer_tuesday
    '
    Me.chk_offer_tuesday.CheckAlign = System.Drawing.ContentAlignment.BottomCenter
    Me.chk_offer_tuesday.Location = New System.Drawing.Point(223, 91)
    Me.chk_offer_tuesday.Name = "chk_offer_tuesday"
    Me.chk_offer_tuesday.Size = New System.Drawing.Size(32, 32)
    Me.chk_offer_tuesday.TabIndex = 4
    Me.chk_offer_tuesday.Text = "TU"
    Me.chk_offer_tuesday.TextAlign = System.Drawing.ContentAlignment.BottomCenter
    Me.chk_offer_tuesday.UseVisualStyleBackColor = True
    '
    'chk_offer_monday
    '
    Me.chk_offer_monday.CheckAlign = System.Drawing.ContentAlignment.BottomCenter
    Me.chk_offer_monday.Location = New System.Drawing.Point(193, 91)
    Me.chk_offer_monday.Name = "chk_offer_monday"
    Me.chk_offer_monday.Size = New System.Drawing.Size(32, 32)
    Me.chk_offer_monday.TabIndex = 3
    Me.chk_offer_monday.Text = "MO"
    Me.chk_offer_monday.TextAlign = System.Drawing.ContentAlignment.BottomCenter
    Me.chk_offer_monday.UseVisualStyleBackColor = True
    '
    'ef_offer_m
    '
    Me.ef_offer_m.DoubleValue = 0.0R
    Me.ef_offer_m.IntegerValue = 0
    Me.ef_offer_m.IsReadOnly = False
    Me.ef_offer_m.Location = New System.Drawing.Point(45, 43)
    Me.ef_offer_m.Name = "ef_offer_m"
    Me.ef_offer_m.PlaceHolder = Nothing
    Me.ef_offer_m.ShortcutsEnabled = True
    Me.ef_offer_m.Size = New System.Drawing.Size(79, 24)
    Me.ef_offer_m.SufixText = "Sufix Text"
    Me.ef_offer_m.SufixTextVisible = True
    Me.ef_offer_m.TabIndex = 1
    Me.ef_offer_m.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_offer_m.TextValue = ""
    Me.ef_offer_m.TextVisible = False
    Me.ef_offer_m.TextWidth = 35
    Me.ef_offer_m.Value = ""
    Me.ef_offer_m.ValueForeColor = System.Drawing.Color.Blue
    '
    'opt_offer_weekday
    '
    Me.opt_offer_weekday.AutoSize = True
    Me.opt_offer_weekday.Checked = True
    Me.opt_offer_weekday.Location = New System.Drawing.Point(46, 105)
    Me.opt_offer_weekday.Name = "opt_offer_weekday"
    Me.opt_offer_weekday.Size = New System.Drawing.Size(86, 17)
    Me.opt_offer_weekday.TabIndex = 2
    Me.opt_offer_weekday.TabStop = True
    Me.opt_offer_weekday.Text = "xWeekDay"
    Me.opt_offer_weekday.UseVisualStyleBackColor = True
    '
    'opt_offer_single_day
    '
    Me.opt_offer_single_day.AutoSize = True
    Me.opt_offer_single_day.Location = New System.Drawing.Point(46, 135)
    Me.opt_offer_single_day.Name = "opt_offer_single_day"
    Me.opt_offer_single_day.Size = New System.Drawing.Size(90, 17)
    Me.opt_offer_single_day.TabIndex = 11
    Me.opt_offer_single_day.Text = "xSingleDay"
    Me.opt_offer_single_day.UseVisualStyleBackColor = True
    '
    'dtp_offer_day
    '
    Me.dtp_offer_day.Checked = True
    Me.dtp_offer_day.Enabled = False
    Me.dtp_offer_day.IsReadOnly = False
    Me.dtp_offer_day.Location = New System.Drawing.Point(242, 135)
    Me.dtp_offer_day.Name = "dtp_offer_day"
    Me.dtp_offer_day.ShowCheckBox = False
    Me.dtp_offer_day.ShowUpDown = False
    Me.dtp_offer_day.Size = New System.Drawing.Size(99, 24)
    Me.dtp_offer_day.SufixText = "Sufix Text"
    Me.dtp_offer_day.SufixTextVisible = True
    Me.dtp_offer_day.TabIndex = 12
    Me.dtp_offer_day.TextVisible = False
    Me.dtp_offer_day.TextWidth = 0
    Me.dtp_offer_day.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_time_to
    '
    Me.dtp_time_to.Checked = True
    Me.dtp_time_to.IsReadOnly = False
    Me.dtp_time_to.Location = New System.Drawing.Point(321, 166)
    Me.dtp_time_to.Name = "dtp_time_to"
    Me.dtp_time_to.ShowCheckBox = False
    Me.dtp_time_to.ShowUpDown = False
    Me.dtp_time_to.Size = New System.Drawing.Size(98, 24)
    Me.dtp_time_to.SufixText = "Sufix Text"
    Me.dtp_time_to.SufixTextVisible = True
    Me.dtp_time_to.TabIndex = 16
    Me.dtp_time_to.TextVisible = False
    Me.dtp_time_to.TextWidth = 0
    Me.dtp_time_to.Value = New Date(2010, 5, 12, 0, 0, 0, 0)
    '
    'dtp_time_from
    '
    Me.dtp_time_from.Checked = True
    Me.dtp_time_from.IsReadOnly = False
    Me.dtp_time_from.Location = New System.Drawing.Point(168, 166)
    Me.dtp_time_from.Name = "dtp_time_from"
    Me.dtp_time_from.ShowCheckBox = False
    Me.dtp_time_from.ShowUpDown = False
    Me.dtp_time_from.Size = New System.Drawing.Size(102, 24)
    Me.dtp_time_from.SufixText = "Sufix Text"
    Me.dtp_time_from.SufixTextVisible = True
    Me.dtp_time_from.TabIndex = 14
    Me.dtp_time_from.TextVisible = False
    Me.dtp_time_from.TextWidth = 0
    Me.dtp_time_from.Value = New Date(2010, 5, 12, 0, 0, 0, 0)
    '
    'dg_offers
    '
    Me.dg_offers.BackColor = System.Drawing.SystemColors.Control
    Me.dg_offers.CurrentCol = -1
    Me.dg_offers.CurrentRow = -1
    Me.dg_offers.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_offers.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_offers.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_offers.Location = New System.Drawing.Point(3, 9)
    Me.dg_offers.Name = "dg_offers"
    Me.dg_offers.PanelRightVisible = False
    Me.dg_offers.Redraw = True
    Me.dg_offers.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_offers.Size = New System.Drawing.Size(463, 135)
    Me.dg_offers.Sortable = False
    Me.dg_offers.SortAscending = True
    Me.dg_offers.SortByCol = 0
    Me.dg_offers.TabIndex = 0
    Me.dg_offers.ToolTipped = True
    Me.dg_offers.TopRow = -2
    Me.dg_offers.WordWrap = False
    '
    'gb_max_number_day_player
    '
    Me.gb_max_number_day_player.Controls.Add(Me.chk_limit_per_operation)
    Me.gb_max_number_day_player.Controls.Add(Me.ef_limit_per_operation)
    Me.gb_max_number_day_player.Controls.Add(Me.chk_limit_to)
    Me.gb_max_number_day_player.Controls.Add(Me.ef_limit)
    Me.gb_max_number_day_player.Controls.Add(Me.ef_limit_per_voucher)
    Me.gb_max_number_day_player.Location = New System.Drawing.Point(393, 243)
    Me.gb_max_number_day_player.Name = "gb_max_number_day_player"
    Me.gb_max_number_day_player.Size = New System.Drawing.Size(590, 101)
    Me.gb_max_number_day_player.TabIndex = 10
    Me.gb_max_number_day_player.TabStop = False
    Me.gb_max_number_day_player.Text = "xMaxNumberDayPlayer"
    '
    'chk_limit_per_operation
    '
    Me.chk_limit_per_operation.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_limit_per_operation.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_limit_per_operation.Location = New System.Drawing.Point(294, 25)
    Me.chk_limit_per_operation.Name = "chk_limit_per_operation"
    Me.chk_limit_per_operation.Size = New System.Drawing.Size(15, 24)
    Me.chk_limit_per_operation.TabIndex = 3
    Me.chk_limit_per_operation.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
    Me.chk_limit_per_operation.UseVisualStyleBackColor = True
    '
    'ef_limit_per_operation
    '
    Me.ef_limit_per_operation.DoubleValue = 0.0R
    Me.ef_limit_per_operation.IntegerValue = 0
    Me.ef_limit_per_operation.IsReadOnly = False
    Me.ef_limit_per_operation.Location = New System.Drawing.Point(315, 24)
    Me.ef_limit_per_operation.Name = "ef_limit_per_operation"
    Me.ef_limit_per_operation.PlaceHolder = Nothing
    Me.ef_limit_per_operation.ShortcutsEnabled = True
    Me.ef_limit_per_operation.Size = New System.Drawing.Size(232, 24)
    Me.ef_limit_per_operation.SufixText = "Sufix Text"
    Me.ef_limit_per_operation.SufixTextVisible = True
    Me.ef_limit_per_operation.TabIndex = 4
    Me.ef_limit_per_operation.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_limit_per_operation.TextValue = ""
    Me.ef_limit_per_operation.TextWidth = 120
    Me.ef_limit_per_operation.Value = ""
    Me.ef_limit_per_operation.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_limit_to
    '
    Me.chk_limit_to.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_limit_to.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_limit_to.Location = New System.Drawing.Point(6, 25)
    Me.chk_limit_to.Name = "chk_limit_to"
    Me.chk_limit_to.Size = New System.Drawing.Size(15, 24)
    Me.chk_limit_to.TabIndex = 0
    Me.chk_limit_to.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
    Me.chk_limit_to.UseVisualStyleBackColor = True
    '
    'ef_limit
    '
    Me.ef_limit.DoubleValue = 0.0R
    Me.ef_limit.IntegerValue = 0
    Me.ef_limit.IsReadOnly = False
    Me.ef_limit.Location = New System.Drawing.Point(27, 25)
    Me.ef_limit.Name = "ef_limit"
    Me.ef_limit.PlaceHolder = Nothing
    Me.ef_limit.ShortcutsEnabled = True
    Me.ef_limit.Size = New System.Drawing.Size(232, 24)
    Me.ef_limit.SufixText = "Sufix Text"
    Me.ef_limit.SufixTextVisible = True
    Me.ef_limit.TabIndex = 1
    Me.ef_limit.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_limit.TextValue = ""
    Me.ef_limit.TextWidth = 120
    Me.ef_limit.Value = ""
    Me.ef_limit.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_limit_per_voucher
    '
    Me.ef_limit_per_voucher.DoubleValue = 0.0R
    Me.ef_limit_per_voucher.IntegerValue = 0
    Me.ef_limit_per_voucher.IsReadOnly = False
    Me.ef_limit_per_voucher.Location = New System.Drawing.Point(27, 52)
    Me.ef_limit_per_voucher.Name = "ef_limit_per_voucher"
    Me.ef_limit_per_voucher.PlaceHolder = Nothing
    Me.ef_limit_per_voucher.ShortcutsEnabled = True
    Me.ef_limit_per_voucher.Size = New System.Drawing.Size(232, 24)
    Me.ef_limit_per_voucher.SufixText = "Sufix Text"
    Me.ef_limit_per_voucher.SufixTextVisible = True
    Me.ef_limit_per_voucher.TabIndex = 2
    Me.ef_limit_per_voucher.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_limit_per_voucher.TextValue = ""
    Me.ef_limit_per_voucher.TextWidth = 120
    Me.ef_limit_per_voucher.Value = ""
    Me.ef_limit_per_voucher.ValueForeColor = System.Drawing.Color.Blue
    '
    'opt_gender_male
    '
    Me.opt_gender_male.Location = New System.Drawing.Point(26, 29)
    Me.opt_gender_male.Name = "opt_gender_male"
    Me.opt_gender_male.Size = New System.Drawing.Size(314, 17)
    Me.opt_gender_male.TabIndex = 1
    Me.opt_gender_male.Text = "xApplies to Men Only"
    '
    'opt_gender_female
    '
    Me.opt_gender_female.Location = New System.Drawing.Point(26, 52)
    Me.opt_gender_female.Name = "opt_gender_female"
    Me.opt_gender_female.Size = New System.Drawing.Size(314, 17)
    Me.opt_gender_female.TabIndex = 2
    Me.opt_gender_female.Text = "xApplies to Women Only"
    '
    'chk_by_gender
    '
    Me.chk_by_gender.AutoSize = True
    Me.chk_by_gender.Location = New System.Drawing.Point(6, 6)
    Me.chk_by_gender.Name = "chk_by_gender"
    Me.chk_by_gender.Size = New System.Drawing.Size(61, 17)
    Me.chk_by_gender.TabIndex = 0
    Me.chk_by_gender.Text = "xActive"
    Me.chk_by_gender.UseVisualStyleBackColor = True
    '
    'tab_filters
    '
    Me.tab_filters.Controls.Add(Me.tab_gender)
    Me.tab_filters.Controls.Add(Me.tab_level)
    Me.tab_filters.Location = New System.Drawing.Point(0, 445)
    Me.tab_filters.Name = "tab_filters"
    Me.tab_filters.SelectedIndex = 0
    Me.tab_filters.Size = New System.Drawing.Size(387, 143)
    Me.tab_filters.TabIndex = 1
    '
    'tab_gender
    '
    Me.tab_gender.Controls.Add(Me.opt_gender_male_draw)
    Me.tab_gender.Controls.Add(Me.opt_gender_female_draw)
    Me.tab_gender.Controls.Add(Me.chk_by_gender_draw)
    Me.tab_gender.Location = New System.Drawing.Point(4, 22)
    Me.tab_gender.Name = "tab_gender"
    Me.tab_gender.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_gender.Size = New System.Drawing.Size(379, 117)
    Me.tab_gender.TabIndex = 0
    Me.tab_gender.Text = "xBy Gender"
    Me.tab_gender.UseVisualStyleBackColor = True
    '
    'opt_gender_male_draw
    '
    Me.opt_gender_male_draw.Location = New System.Drawing.Point(42, 29)
    Me.opt_gender_male_draw.Name = "opt_gender_male_draw"
    Me.opt_gender_male_draw.Size = New System.Drawing.Size(314, 17)
    Me.opt_gender_male_draw.TabIndex = 1
    Me.opt_gender_male_draw.Text = "xApplies to Men Only"
    '
    'opt_gender_female_draw
    '
    Me.opt_gender_female_draw.Location = New System.Drawing.Point(42, 52)
    Me.opt_gender_female_draw.Name = "opt_gender_female_draw"
    Me.opt_gender_female_draw.Size = New System.Drawing.Size(314, 17)
    Me.opt_gender_female_draw.TabIndex = 2
    Me.opt_gender_female_draw.Text = "xApplies to Women Only"
    '
    'chk_by_gender_draw
    '
    Me.chk_by_gender_draw.AutoSize = True
    Me.chk_by_gender_draw.Location = New System.Drawing.Point(17, 6)
    Me.chk_by_gender_draw.Name = "chk_by_gender_draw"
    Me.chk_by_gender_draw.Size = New System.Drawing.Size(68, 17)
    Me.chk_by_gender_draw.TabIndex = 0
    Me.chk_by_gender_draw.Text = "xActive"
    Me.chk_by_gender_draw.UseVisualStyleBackColor = True
    '
    'tab_level
    '
    Me.tab_level.Controls.Add(Me.chk_only_vip)
    Me.tab_level.Controls.Add(Me.chk_level_anonymous)
    Me.tab_level.Controls.Add(Me.chk_level_04)
    Me.tab_level.Controls.Add(Me.chk_level_03)
    Me.tab_level.Controls.Add(Me.chk_level_02)
    Me.tab_level.Controls.Add(Me.chk_level_01)
    Me.tab_level.Controls.Add(Me.chk_by_level)
    Me.tab_level.Location = New System.Drawing.Point(4, 22)
    Me.tab_level.Name = "tab_level"
    Me.tab_level.Size = New System.Drawing.Size(379, 117)
    Me.tab_level.TabIndex = 3
    Me.tab_level.Text = "xBy Level"
    Me.tab_level.UseVisualStyleBackColor = True
    '
    'chk_only_vip
    '
    Me.chk_only_vip.AutoSize = True
    Me.chk_only_vip.Location = New System.Drawing.Point(6, 94)
    Me.chk_only_vip.Name = "chk_only_vip"
    Me.chk_only_vip.Size = New System.Drawing.Size(83, 17)
    Me.chk_only_vip.TabIndex = 21
    Me.chk_only_vip.Text = "xOnlyVips"
    Me.chk_only_vip.UseVisualStyleBackColor = True
    '
    'chk_level_anonymous
    '
    Me.chk_level_anonymous.AutoSize = True
    Me.chk_level_anonymous.Location = New System.Drawing.Point(26, 29)
    Me.chk_level_anonymous.Name = "chk_level_anonymous"
    Me.chk_level_anonymous.Size = New System.Drawing.Size(100, 17)
    Me.chk_level_anonymous.TabIndex = 1
    Me.chk_level_anonymous.Text = "xAnonymous"
    Me.chk_level_anonymous.UseVisualStyleBackColor = True
    '
    'chk_level_04
    '
    Me.chk_level_04.AutoSize = True
    Me.chk_level_04.Location = New System.Drawing.Point(217, 75)
    Me.chk_level_04.Name = "chk_level_04"
    Me.chk_level_04.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_04.TabIndex = 5
    Me.chk_level_04.Text = "xLevel 04"
    Me.chk_level_04.UseVisualStyleBackColor = True
    '
    'chk_level_03
    '
    Me.chk_level_03.AutoSize = True
    Me.chk_level_03.Location = New System.Drawing.Point(217, 52)
    Me.chk_level_03.Name = "chk_level_03"
    Me.chk_level_03.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_03.TabIndex = 4
    Me.chk_level_03.Text = "xLevel 03"
    Me.chk_level_03.UseVisualStyleBackColor = True
    '
    'chk_level_02
    '
    Me.chk_level_02.AutoSize = True
    Me.chk_level_02.Location = New System.Drawing.Point(26, 75)
    Me.chk_level_02.Name = "chk_level_02"
    Me.chk_level_02.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_02.TabIndex = 3
    Me.chk_level_02.Text = "xLevel 02"
    Me.chk_level_02.UseVisualStyleBackColor = True
    '
    'chk_level_01
    '
    Me.chk_level_01.AutoSize = True
    Me.chk_level_01.Location = New System.Drawing.Point(26, 52)
    Me.chk_level_01.Name = "chk_level_01"
    Me.chk_level_01.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_01.TabIndex = 2
    Me.chk_level_01.Text = "xLevel 01"
    Me.chk_level_01.UseVisualStyleBackColor = True
    '
    'chk_by_level
    '
    Me.chk_by_level.AutoSize = True
    Me.chk_by_level.Location = New System.Drawing.Point(6, 6)
    Me.chk_by_level.Name = "chk_by_level"
    Me.chk_by_level.Size = New System.Drawing.Size(68, 17)
    Me.chk_by_level.TabIndex = 0
    Me.chk_by_level.Text = "xActive"
    Me.chk_by_level.UseVisualStyleBackColor = True
    '
    'uc_provider_filter1
    '
    Me.uc_provider_filter1.AllowCollapse = True
    Me.uc_provider_filter1.Collapsed = True
    Me.uc_provider_filter1.Location = New System.Drawing.Point(54, 391)
    Me.uc_provider_filter1.Name = "uc_provider_filter1"
    Me.uc_provider_filter1.Size = New System.Drawing.Size(283, 48)
    Me.uc_provider_filter1.TabIndex = 1
    '
    'img_icon
    '
    Me.img_icon.AutoSize = True
    Me.img_icon.ButtonDeleteEnabled = True
    Me.img_icon.FreeResize = False
    Me.img_icon.Image = Nothing
    Me.img_icon.ImageInfoFont = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.img_icon.ImageLayout = System.Windows.Forms.ImageLayout.Zoom
    Me.img_icon.ImageName = Nothing
    Me.img_icon.Location = New System.Drawing.Point(626, 66)
    Me.img_icon.Margin = New System.Windows.Forms.Padding(0)
    Me.img_icon.Name = "img_icon"
    Me.img_icon.Size = New System.Drawing.Size(152, 102)
    Me.img_icon.TabIndex = 6
    Me.img_icon.Transparent = False
    '
    'img_image
    '
    Me.img_image.AutoSize = True
    Me.img_image.ButtonDeleteEnabled = True
    Me.img_image.FreeResize = False
    Me.img_image.Image = Nothing
    Me.img_image.ImageInfoFont = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.img_image.ImageLayout = System.Windows.Forms.ImageLayout.Zoom
    Me.img_image.ImageName = Nothing
    Me.img_image.Location = New System.Drawing.Point(779, 35)
    Me.img_image.Margin = New System.Windows.Forms.Padding(0)
    Me.img_image.Name = "img_image"
    Me.img_image.Size = New System.Drawing.Size(204, 194)
    Me.img_image.TabIndex = 9
    Me.img_image.Transparent = False
    '
    'lbl_image
    '
    Me.lbl_image.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.lbl_image.ForeColor = System.Drawing.Color.Black
    Me.lbl_image.Location = New System.Drawing.Point(780, 5)
    Me.lbl_image.Name = "lbl_image"
    Me.lbl_image.Size = New System.Drawing.Size(203, 15)
    Me.lbl_image.TabIndex = 7
    Me.lbl_image.Text = "xImage"
    Me.lbl_image.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_icon
    '
    Me.lbl_icon.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.lbl_icon.ForeColor = System.Drawing.Color.Black
    Me.lbl_icon.Location = New System.Drawing.Point(624, 34)
    Me.lbl_icon.Name = "lbl_icon"
    Me.lbl_icon.Size = New System.Drawing.Size(152, 15)
    Me.lbl_icon.TabIndex = 4
    Me.lbl_icon.Text = "xIcon"
    Me.lbl_icon.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_icon_optimun_size
    '
    Me.lbl_icon_optimun_size.Font = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_icon_optimun_size.Location = New System.Drawing.Point(610, 45)
    Me.lbl_icon_optimun_size.Name = "lbl_icon_optimun_size"
    Me.lbl_icon_optimun_size.Size = New System.Drawing.Size(179, 18)
    Me.lbl_icon_optimun_size.TabIndex = 5
    Me.lbl_icon_optimun_size.Text = "xOptimunSize"
    Me.lbl_icon_optimun_size.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_image_optimun_size
    '
    Me.lbl_image_optimun_size.Font = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_image_optimun_size.Location = New System.Drawing.Point(780, 17)
    Me.lbl_image_optimun_size.Name = "lbl_image_optimun_size"
    Me.lbl_image_optimun_size.Size = New System.Drawing.Size(203, 18)
    Me.lbl_image_optimun_size.TabIndex = 8
    Me.lbl_image_optimun_size.Text = "xOptimunSize"
    Me.lbl_image_optimun_size.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'chk_visible_on_promobox
    '
    Me.chk_visible_on_promobox.AutoSize = True
    Me.chk_visible_on_promobox.Location = New System.Drawing.Point(17, 24)
    Me.chk_visible_on_promobox.Name = "chk_visible_on_promobox"
    Me.chk_visible_on_promobox.Size = New System.Drawing.Size(145, 17)
    Me.chk_visible_on_promobox.TabIndex = 4
    Me.chk_visible_on_promobox.Text = "xVisibleOnPromoBox"
    Me.chk_visible_on_promobox.UseVisualStyleBackColor = True
    '
    'ef_text_on_promobox
    '
    Me.ef_text_on_promobox.DoubleValue = 0.0R
    Me.ef_text_on_promobox.IntegerValue = 0
    Me.ef_text_on_promobox.IsReadOnly = False
    Me.ef_text_on_promobox.Location = New System.Drawing.Point(10, 87)
    Me.ef_text_on_promobox.Name = "ef_text_on_promobox"
    Me.ef_text_on_promobox.PlaceHolder = Nothing
    Me.ef_text_on_promobox.ShortcutsEnabled = True
    Me.ef_text_on_promobox.Size = New System.Drawing.Size(339, 24)
    Me.ef_text_on_promobox.SufixText = "Sufix Text"
    Me.ef_text_on_promobox.TabIndex = 6
    Me.ef_text_on_promobox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_text_on_promobox.TextValue = ""
    Me.ef_text_on_promobox.TextVisible = False
    Me.ef_text_on_promobox.TextWidth = 100
    Me.ef_text_on_promobox.Value = ""
    Me.ef_text_on_promobox.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_award_on_promobox
    '
    Me.chk_award_on_promobox.AutoSize = True
    Me.chk_award_on_promobox.Location = New System.Drawing.Point(17, 54)
    Me.chk_award_on_promobox.Name = "chk_award_on_promobox"
    Me.chk_award_on_promobox.Size = New System.Drawing.Size(144, 17)
    Me.chk_award_on_promobox.TabIndex = 5
    Me.chk_award_on_promobox.Text = "xAwardOnPromoBox"
    Me.chk_award_on_promobox.UseVisualStyleBackColor = True
    '
    'tab_misc
    '
    Me.tab_misc.Controls.Add(Me.tab_cashin_conditions)
    Me.tab_misc.Controls.Add(Me.tab_promobox)
    Me.tab_misc.Location = New System.Drawing.Point(0, 590)
    Me.tab_misc.Name = "tab_misc"
    Me.tab_misc.SelectedIndex = 0
    Me.tab_misc.Size = New System.Drawing.Size(382, 197)
    Me.tab_misc.TabIndex = 2
    '
    'tab_cashin_conditions
    '
    Me.tab_cashin_conditions.Controls.Add(Me.chk_min_spent)
    Me.tab_cashin_conditions.Controls.Add(Me.chk_first_cash_in_not_constrained)
    Me.tab_cashin_conditions.Controls.Add(Me.ef_min_cash_in)
    Me.tab_cashin_conditions.Controls.Add(Me.chk_min_played)
    Me.tab_cashin_conditions.Controls.Add(Me.ef_min_spent)
    Me.tab_cashin_conditions.Controls.Add(Me.lbl_first_cash_in_constrained_2)
    Me.tab_cashin_conditions.Controls.Add(Me.ef_min_played)
    Me.tab_cashin_conditions.Controls.Add(Me.lbl_cash_in_conditions_2)
    Me.tab_cashin_conditions.Controls.Add(Me.lbl_cash_in_conditions_1)
    Me.tab_cashin_conditions.Controls.Add(Me.lbl_first_cash_in_constrained_1)
    Me.tab_cashin_conditions.Location = New System.Drawing.Point(4, 22)
    Me.tab_cashin_conditions.Name = "tab_cashin_conditions"
    Me.tab_cashin_conditions.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_cashin_conditions.Size = New System.Drawing.Size(374, 171)
    Me.tab_cashin_conditions.TabIndex = 0
    Me.tab_cashin_conditions.Text = "xCashInConditions"
    Me.tab_cashin_conditions.UseVisualStyleBackColor = True
    '
    'chk_min_spent
    '
    Me.chk_min_spent.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_min_spent.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_min_spent.Location = New System.Drawing.Point(12, 27)
    Me.chk_min_spent.Name = "chk_min_spent"
    Me.chk_min_spent.Size = New System.Drawing.Size(15, 24)
    Me.chk_min_spent.TabIndex = 2
    Me.chk_min_spent.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
    Me.chk_min_spent.UseVisualStyleBackColor = True
    '
    'chk_first_cash_in_not_constrained
    '
    Me.chk_first_cash_in_not_constrained.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_first_cash_in_not_constrained.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_first_cash_in_not_constrained.Location = New System.Drawing.Point(12, 81)
    Me.chk_first_cash_in_not_constrained.Name = "chk_first_cash_in_not_constrained"
    Me.chk_first_cash_in_not_constrained.Size = New System.Drawing.Size(15, 24)
    Me.chk_first_cash_in_not_constrained.TabIndex = 6
    Me.chk_first_cash_in_not_constrained.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
    Me.chk_first_cash_in_not_constrained.UseVisualStyleBackColor = True
    '
    'ef_min_cash_in
    '
    Me.ef_min_cash_in.DoubleValue = 0.0R
    Me.ef_min_cash_in.IntegerValue = 0
    Me.ef_min_cash_in.IsReadOnly = False
    Me.ef_min_cash_in.Location = New System.Drawing.Point(49, 4)
    Me.ef_min_cash_in.Name = "ef_min_cash_in"
    Me.ef_min_cash_in.PlaceHolder = Nothing
    Me.ef_min_cash_in.ShortcutsEnabled = True
    Me.ef_min_cash_in.Size = New System.Drawing.Size(214, 24)
    Me.ef_min_cash_in.SufixText = "Sufix Text"
    Me.ef_min_cash_in.SufixTextVisible = True
    Me.ef_min_cash_in.TabIndex = 1
    Me.ef_min_cash_in.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_min_cash_in.TextValue = ""
    Me.ef_min_cash_in.TextVisible = False
    Me.ef_min_cash_in.TextWidth = 100
    Me.ef_min_cash_in.Value = ""
    Me.ef_min_cash_in.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_min_played
    '
    Me.chk_min_played.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_min_played.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_min_played.Location = New System.Drawing.Point(12, 51)
    Me.chk_min_played.Name = "chk_min_played"
    Me.chk_min_played.Size = New System.Drawing.Size(15, 24)
    Me.chk_min_played.TabIndex = 4
    Me.chk_min_played.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
    Me.chk_min_played.UseVisualStyleBackColor = True
    '
    'ef_min_spent
    '
    Me.ef_min_spent.DoubleValue = 0.0R
    Me.ef_min_spent.IntegerValue = 0
    Me.ef_min_spent.IsReadOnly = False
    Me.ef_min_spent.Location = New System.Drawing.Point(49, 28)
    Me.ef_min_spent.Name = "ef_min_spent"
    Me.ef_min_spent.PlaceHolder = Nothing
    Me.ef_min_spent.ShortcutsEnabled = True
    Me.ef_min_spent.Size = New System.Drawing.Size(234, 24)
    Me.ef_min_spent.SufixText = "%"
    Me.ef_min_spent.SufixTextVisible = True
    Me.ef_min_spent.SufixTextWidth = 20
    Me.ef_min_spent.TabIndex = 3
    Me.ef_min_spent.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_min_spent.TextValue = ""
    Me.ef_min_spent.TextVisible = False
    Me.ef_min_spent.TextWidth = 100
    Me.ef_min_spent.Value = ""
    Me.ef_min_spent.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_first_cash_in_constrained_2
    '
    Me.lbl_first_cash_in_constrained_2.AutoSize = True
    Me.lbl_first_cash_in_constrained_2.IsReadOnly = True
    Me.lbl_first_cash_in_constrained_2.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_first_cash_in_constrained_2.LabelForeColor = System.Drawing.SystemColors.ControlText
    Me.lbl_first_cash_in_constrained_2.Location = New System.Drawing.Point(29, 98)
    Me.lbl_first_cash_in_constrained_2.Name = "lbl_first_cash_in_constrained_2"
    Me.lbl_first_cash_in_constrained_2.Size = New System.Drawing.Size(345, 24)
    Me.lbl_first_cash_in_constrained_2.SufixText = "Sufix Text"
    Me.lbl_first_cash_in_constrained_2.SufixTextVisible = True
    Me.lbl_first_cash_in_constrained_2.TabIndex = 8
    Me.lbl_first_cash_in_constrained_2.TabStop = False
    Me.lbl_first_cash_in_constrained_2.TextVisible = False
    Me.lbl_first_cash_in_constrained_2.TextWidth = 0
    Me.lbl_first_cash_in_constrained_2.Value = "xregardless of the minimun played and the minimun spent"
    '
    'ef_min_played
    '
    Me.ef_min_played.DoubleValue = 0.0R
    Me.ef_min_played.IntegerValue = 0
    Me.ef_min_played.IsReadOnly = False
    Me.ef_min_played.Location = New System.Drawing.Point(49, 52)
    Me.ef_min_played.Name = "ef_min_played"
    Me.ef_min_played.PlaceHolder = Nothing
    Me.ef_min_played.ShortcutsEnabled = True
    Me.ef_min_played.Size = New System.Drawing.Size(234, 24)
    Me.ef_min_played.SufixText = "%"
    Me.ef_min_played.SufixTextVisible = True
    Me.ef_min_played.SufixTextWidth = 20
    Me.ef_min_played.TabIndex = 5
    Me.ef_min_played.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_min_played.TextValue = ""
    Me.ef_min_played.TextVisible = False
    Me.ef_min_played.TextWidth = 100
    Me.ef_min_played.Value = ""
    Me.ef_min_played.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_cash_in_conditions_2
    '
    Me.lbl_cash_in_conditions_2.AutoSize = True
    Me.lbl_cash_in_conditions_2.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_cash_in_conditions_2.IsReadOnly = True
    Me.lbl_cash_in_conditions_2.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_cash_in_conditions_2.LabelForeColor = System.Drawing.Color.Navy
    Me.lbl_cash_in_conditions_2.Location = New System.Drawing.Point(19, 141)
    Me.lbl_cash_in_conditions_2.Name = "lbl_cash_in_conditions_2"
    Me.lbl_cash_in_conditions_2.Size = New System.Drawing.Size(355, 24)
    Me.lbl_cash_in_conditions_2.SufixText = "Sufix Text"
    Me.lbl_cash_in_conditions_2.SufixTextVisible = True
    Me.lbl_cash_in_conditions_2.TabIndex = 0
    Me.lbl_cash_in_conditions_2.TabStop = False
    Me.lbl_cash_in_conditions_2.TextVisible = False
    Me.lbl_cash_in_conditions_2.TextWidth = 0
    Me.lbl_cash_in_conditions_2.Value = "xeither --- spent or --- played"
    '
    'lbl_cash_in_conditions_1
    '
    Me.lbl_cash_in_conditions_1.AutoSize = True
    Me.lbl_cash_in_conditions_1.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_cash_in_conditions_1.IsReadOnly = True
    Me.lbl_cash_in_conditions_1.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_cash_in_conditions_1.LabelForeColor = System.Drawing.Color.Navy
    Me.lbl_cash_in_conditions_1.Location = New System.Drawing.Point(19, 120)
    Me.lbl_cash_in_conditions_1.Name = "lbl_cash_in_conditions_1"
    Me.lbl_cash_in_conditions_1.Size = New System.Drawing.Size(355, 24)
    Me.lbl_cash_in_conditions_1.SufixText = "Sufix Text"
    Me.lbl_cash_in_conditions_1.SufixTextVisible = True
    Me.lbl_cash_in_conditions_1.TabIndex = 9
    Me.lbl_cash_in_conditions_1.TabStop = False
    Me.lbl_cash_in_conditions_1.TextVisible = False
    Me.lbl_cash_in_conditions_1.TextWidth = 0
    Me.lbl_cash_in_conditions_1.Value = "xYou get one number if cash in of --- and,"
    '
    'lbl_first_cash_in_constrained_1
    '
    Me.lbl_first_cash_in_constrained_1.AutoSize = True
    Me.lbl_first_cash_in_constrained_1.IsReadOnly = True
    Me.lbl_first_cash_in_constrained_1.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_first_cash_in_constrained_1.LabelForeColor = System.Drawing.SystemColors.ControlText
    Me.lbl_first_cash_in_constrained_1.Location = New System.Drawing.Point(31, 79)
    Me.lbl_first_cash_in_constrained_1.Name = "lbl_first_cash_in_constrained_1"
    Me.lbl_first_cash_in_constrained_1.Size = New System.Drawing.Size(351, 24)
    Me.lbl_first_cash_in_constrained_1.SufixText = "Sufix Text"
    Me.lbl_first_cash_in_constrained_1.SufixTextVisible = True
    Me.lbl_first_cash_in_constrained_1.TabIndex = 7
    Me.lbl_first_cash_in_constrained_1.TabStop = False
    Me.lbl_first_cash_in_constrained_1.TextVisible = False
    Me.lbl_first_cash_in_constrained_1.TextWidth = 0
    Me.lbl_first_cash_in_constrained_1.Value = "xDeliver draw numbers in the first recharge"
    '
    'tab_promobox
    '
    Me.tab_promobox.Controls.Add(Me.chk_visible_on_promobox)
    Me.tab_promobox.Controls.Add(Me.chk_award_on_promobox)
    Me.tab_promobox.Controls.Add(Me.ef_text_on_promobox)
    Me.tab_promobox.Location = New System.Drawing.Point(4, 22)
    Me.tab_promobox.Name = "tab_promobox"
    Me.tab_promobox.Size = New System.Drawing.Size(374, 171)
    Me.tab_promobox.TabIndex = 3
    Me.tab_promobox.Text = "xPromoBOX"
    Me.tab_promobox.UseVisualStyleBackColor = True
    '
    'frm_draw_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1092, 806)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
    Me.Name = "frm_draw_edit"
    Me.Text = "frm_draw_edit"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.gb_draws_numeration.ResumeLayout(False)
    Me.gb_draws_numeration.PerformLayout()
    Me.gb_detail.ResumeLayout(False)
    Me.gb_detail.PerformLayout()
    Me.gb_points_by_level.ResumeLayout(False)
    Me.gb_points_by_level.PerformLayout()
    Me.tb_winner.ResumeLayout(False)
    Me.tb_layout.ResumeLayout(False)
    Me.tab_voucher.ResumeLayout(False)
    Me.TabPage6.ResumeLayout(False)
    Me.TabPage1.ResumeLayout(False)
    Me.TabPage1.PerformLayout()
    Me.TabPage2.ResumeLayout(False)
    Me.TabPage2.PerformLayout()
    Me.TabPage3.ResumeLayout(False)
    Me.TabPage3.PerformLayout()
    Me.TabPage4.ResumeLayout(False)
    Me.TabPage4.PerformLayout()
    Me.TabPage5.ResumeLayout(False)
    Me.TabPage5.PerformLayout()
    Me.tab_Extra.ResumeLayout(False)
    Me.tb_offer.ResumeLayout(False)
    Me.tb_offer.PerformLayout()
    Me.gb_new_offer.ResumeLayout(False)
    Me.gb_new_offer.PerformLayout()
    Me.Panel1.ResumeLayout(False)
    Me.Panel1.PerformLayout()
    Me.gb_max_number_day_player.ResumeLayout(False)
    Me.tab_filters.ResumeLayout(False)
    Me.tab_gender.ResumeLayout(False)
    Me.tab_gender.PerformLayout()
    Me.tab_level.ResumeLayout(False)
    Me.tab_level.PerformLayout()
    Me.tab_misc.ResumeLayout(False)
    Me.tab_cashin_conditions.ResumeLayout(False)
    Me.tab_cashin_conditions.PerformLayout()
    Me.tab_promobox.ResumeLayout(False)
    Me.tab_promobox.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents Timer1 As System.Windows.Forms.Timer
  Friend WithEvents tab_Extra As System.Windows.Forms.TabControl
  Friend WithEvents tb_layout As System.Windows.Forms.TabPage
  Friend WithEvents tab_voucher As System.Windows.Forms.TabControl
  Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
  Friend WithEvents ef_header As System.Windows.Forms.TextBox
  Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
  Friend WithEvents ef_footer As System.Windows.Forms.TextBox
  Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
  Friend WithEvents ef_detail1 As System.Windows.Forms.TextBox
  Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
  Friend WithEvents ef_detail2 As System.Windows.Forms.TextBox
  Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
  Friend WithEvents ef_detail3 As System.Windows.Forms.TextBox
  Friend WithEvents tb_winner As System.Windows.Forms.TabPage
  Friend WithEvents btn_voucher As GUI_Controls.uc_button
  Friend WithEvents ef_draw_ticket As GUI_Controls.uc_entry_field
  Friend WithEvents ef_account As GUI_Controls.uc_entry_field
  Friend WithEvents ef_voucher As GUI_Controls.uc_entry_field
  Friend WithEvents ef_winner_name As GUI_Controls.uc_entry_field
  Friend WithEvents ef_winning_number As GUI_Controls.uc_entry_field
  Friend WithEvents btn_check_winner As GUI_Controls.uc_button
  Friend WithEvents gb_detail As System.Windows.Forms.GroupBox
  Friend WithEvents cmb_draw_type As GUI_Controls.uc_combo
  Friend WithEvents ef_number_price As GUI_Controls.uc_entry_field
  Friend WithEvents ef_draw_name As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_status As System.Windows.Forms.Label
  Friend WithEvents cmb_status As GUI_Controls.uc_combo
  Friend WithEvents dtp_end As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_start As GUI_Controls.uc_date_picker
  Friend WithEvents Uc_text_field1 As GUI_Controls.uc_text_field
  Friend WithEvents gb_draws_numeration As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_last_number As System.Windows.Forms.Label
  Friend WithEvents ef_last_number As GUI_Controls.uc_entry_field
  Friend WithEvents uc_entry_draw_numbers_initial As GUI_Controls.uc_entry_field
  Friend WithEvents uc_entry_draw_numbers_max As GUI_Controls.uc_entry_field
  Friend WithEvents tb_offer As System.Windows.Forms.TabPage
  Friend WithEvents dg_offers As GUI_Controls.uc_grid
  Friend WithEvents gb_new_offer As System.Windows.Forms.GroupBox
  Friend WithEvents chk_offer_sunday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_offer_saturday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_offer_friday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_offer_thursday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_offer_wednesday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_offer_tuesday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_offer_monday As System.Windows.Forms.CheckBox
  Friend WithEvents ef_offer_m As GUI_Controls.uc_entry_field
  Friend WithEvents ef_offer_n As GUI_Controls.uc_entry_field
  Friend WithEvents opt_offer_weekday As System.Windows.Forms.RadioButton
  Friend WithEvents opt_offer_single_day As System.Windows.Forms.RadioButton
  Friend WithEvents dtp_offer_day As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_time_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_time_from As GUI_Controls.uc_date_picker
  Friend WithEvents btn_offer_remove As GUI_Controls.uc_button
  Friend WithEvents btn_offer_add As GUI_Controls.uc_button
  Friend WithEvents btn_check_all_days As GUI_Controls.uc_button
  Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
  Friend WithEvents wb_voucher As System.Windows.Forms.WebBrowser
  Friend WithEvents lbl_not_apply As GUI_Controls.uc_text_field
  Friend WithEvents lblTo As System.Windows.Forms.Label
  Friend WithEvents lblFrom As System.Windows.Forms.Label
  Friend WithEvents gb_max_number_day_player As System.Windows.Forms.GroupBox
  Friend WithEvents ef_limit As GUI_Controls.uc_entry_field
  Friend WithEvents ef_limit_per_voucher As GUI_Controls.uc_entry_field
  Friend WithEvents chk_limit_to As System.Windows.Forms.CheckBox
  Friend WithEvents opt_gender_male As System.Windows.Forms.RadioButton
  Friend WithEvents opt_gender_female As System.Windows.Forms.RadioButton
  Friend WithEvents chk_by_gender As System.Windows.Forms.CheckBox
  Friend WithEvents tab_filters As System.Windows.Forms.TabControl
  Friend WithEvents tab_gender As System.Windows.Forms.TabPage
  Friend WithEvents opt_gender_male_draw As System.Windows.Forms.RadioButton
  Friend WithEvents opt_gender_female_draw As System.Windows.Forms.RadioButton
  Friend WithEvents chk_by_gender_draw As System.Windows.Forms.CheckBox
  Friend WithEvents opt_gender_male_offer As System.Windows.Forms.RadioButton
  Friend WithEvents opt_gender_female_offer As System.Windows.Forms.RadioButton
  Friend WithEvents chk_by_gender_offer As System.Windows.Forms.CheckBox
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
  Friend WithEvents lbl_msg_order_offers As GUI_Controls.uc_text_field
  Friend WithEvents btn_reset As GUI_Controls.uc_button
  Friend WithEvents ef_limit_per_operation As GUI_Controls.uc_entry_field
  Friend WithEvents chk_limit_per_operation As System.Windows.Forms.CheckBox
  Friend WithEvents chk_bingo_format As System.Windows.Forms.CheckBox
  Friend WithEvents uc_provider_filter1 As GUI_Controls.uc_provider_filter
  Friend WithEvents img_icon As GUI_Controls.uc_image
  Friend WithEvents img_image As GUI_Controls.uc_image
  Friend WithEvents lbl_image As System.Windows.Forms.Label
  Friend WithEvents lbl_icon As System.Windows.Forms.Label
  Friend WithEvents lbl_icon_optimun_size As System.Windows.Forms.Label
  Friend WithEvents lbl_image_optimun_size As System.Windows.Forms.Label
  Friend WithEvents tab_level As System.Windows.Forms.TabPage
  Friend WithEvents chk_level_anonymous As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_04 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_03 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_02 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_01 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_by_level As System.Windows.Forms.CheckBox
  Friend WithEvents gb_points_by_level As System.Windows.Forms.GroupBox
  Friend WithEvents chk_points_level4 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_points_level3 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_points_level2 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_points_level1 As System.Windows.Forms.CheckBox
  Friend WithEvents ef_points_level4 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_points_level1 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_points_level3 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_points_level2 As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_info_points_level As System.Windows.Forms.Label
  Friend WithEvents lbl_total_numbers_count As System.Windows.Forms.Label
  Friend WithEvents lbl_price_ratio As System.Windows.Forms.Label
  Friend WithEvents chk_visible_on_promobox As System.Windows.Forms.CheckBox
  Friend WithEvents chk_only_vip As System.Windows.Forms.CheckBox
  Friend WithEvents ef_text_on_promobox As GUI_Controls.uc_entry_field
  Friend WithEvents chk_award_on_promobox As System.Windows.Forms.CheckBox
  Friend WithEvents tab_misc As System.Windows.Forms.TabControl
  Friend WithEvents tab_cashin_conditions As System.Windows.Forms.TabPage
  Friend WithEvents chk_min_spent As System.Windows.Forms.CheckBox
  Friend WithEvents chk_first_cash_in_not_constrained As System.Windows.Forms.CheckBox
  Friend WithEvents ef_min_cash_in As GUI_Controls.uc_entry_field
  Friend WithEvents chk_min_played As System.Windows.Forms.CheckBox
  Friend WithEvents ef_min_spent As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_first_cash_in_constrained_2 As GUI_Controls.uc_text_field
  Friend WithEvents ef_min_played As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_cash_in_conditions_2 As GUI_Controls.uc_text_field
  Friend WithEvents lbl_cash_in_conditions_1 As GUI_Controls.uc_text_field
  Friend WithEvents lbl_first_cash_in_constrained_1 As GUI_Controls.uc_text_field
  Friend WithEvents tab_promobox As System.Windows.Forms.TabPage
End Class
