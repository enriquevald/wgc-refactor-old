'-------------------------------------------------------------------
' Copyright � 2011 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_site_edit.vb
' DESCRIPTION:   Site edition form
' AUTHOR:        Agust� Poch
' CREATION DATE: 07-FEB-2011
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 07-FEB-2011  APB    Initial version.
'
'--------------------------------------------------------------------

Option Explicit On 

#Region " Imports "

Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE

#End Region ' Imports

Public Class frm_site_edit
  Inherits frm_base_edit

#Region " Constants "

  Private Const LEN_SITE_ID As Integer = 6
  Private Const LEN_SITE_NAME As Integer = 30
  Private Const LEN_OPERATOR As Integer = 30

#End Region

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents ef_site_name As GUI_Controls.uc_entry_field
  Friend WithEvents ef_operator As GUI_Controls.uc_entry_field
  Friend WithEvents btn_select_operator As GUI_Controls.uc_button
  Friend WithEvents ef_site_id As GUI_Controls.uc_entry_field
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.ef_site_id = New GUI_Controls.uc_entry_field
    Me.ef_site_name = New GUI_Controls.uc_entry_field
    Me.ef_operator = New GUI_Controls.uc_entry_field
    Me.btn_select_operator = New GUI_Controls.uc_button
    Me.panel_data.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.btn_select_operator)
    Me.panel_data.Controls.Add(Me.ef_operator)
    Me.panel_data.Controls.Add(Me.ef_site_name)
    Me.panel_data.Controls.Add(Me.ef_site_id)
    Me.panel_data.Size = New System.Drawing.Size(485, 131)
    '
    'ef_site_id
    '
    Me.ef_site_id.DoubleValue = 0
    Me.ef_site_id.IntegerValue = 0
    Me.ef_site_id.IsReadOnly = False
    Me.ef_site_id.Location = New System.Drawing.Point(64, 16)
    Me.ef_site_id.Name = "ef_site_id"
    Me.ef_site_id.OnlyUpperCase = True
    Me.ef_site_id.Size = New System.Drawing.Size(160, 24)
    Me.ef_site_id.SufixText = "Sufix Text"
    Me.ef_site_id.SufixTextVisible = True
    Me.ef_site_id.TabIndex = 1
    Me.ef_site_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_site_id.TextValue = ""
    Me.ef_site_id.Value = ""
    '
    'ef_site_name
    '
    Me.ef_site_name.DoubleValue = 0
    Me.ef_site_name.IntegerValue = 0
    Me.ef_site_name.IsReadOnly = False
    Me.ef_site_name.Location = New System.Drawing.Point(24, 48)
    Me.ef_site_name.Name = "ef_site_name"
    Me.ef_site_name.Size = New System.Drawing.Size(395, 24)
    Me.ef_site_name.SufixText = "Sufix Text"
    Me.ef_site_name.SufixTextVisible = True
    Me.ef_site_name.TabIndex = 2
    Me.ef_site_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_site_name.TextValue = ""
    Me.ef_site_name.TextWidth = 120
    Me.ef_site_name.Value = ""
    '
    'ef_operator
    '
    Me.ef_operator.DoubleValue = 0
    Me.ef_operator.IntegerValue = 0
    Me.ef_operator.IsReadOnly = True
    Me.ef_operator.Location = New System.Drawing.Point(64, 86)
    Me.ef_operator.Name = "ef_operator"
    Me.ef_operator.Size = New System.Drawing.Size(352, 24)
    Me.ef_operator.SufixText = "Sufix Text"
    Me.ef_operator.SufixTextVisible = True
    Me.ef_operator.TabIndex = 3
    Me.ef_operator.TabStop = False
    Me.ef_operator.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_operator.TextValue = ""
    Me.ef_operator.Value = ""
    '
    'btn_select_operator
    '
    Me.btn_select_operator.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_select_operator.Location = New System.Drawing.Point(422, 86)
    Me.btn_select_operator.Name = "btn_select_operator"
    Me.btn_select_operator.Size = New System.Drawing.Size(30, 29)
    Me.btn_select_operator.TabIndex = 3
    Me.btn_select_operator.ToolTipped = False
    Me.btn_select_operator.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.SELECTION
    '
    'frm_site_edit
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(587, 139)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_site_edit"
    Me.Text = "#5272 "
    Me.panel_data.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Members "

  Private m_input_site_id As String
  Private m_initial_operator_id As Integer = 0
  Private m_selected_operator_id As Integer = -1
  Private DeleteOperation As Boolean

#End Region ' Members

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_SITE_EDIT

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    ' Add any initialization after the MyBase.GUI_InitControls() call
    ' Hide the delete button if ScreenMode is New
    If Me.ScreenMode = frm_base_edit.ENUM_SCREEN_MODE.MODE_NEW Then
      GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
    Else
      GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = True
    End If

    Me.Text = GLB_NLS_GUI_CONFIGURATION.GetString(353)

    ef_site_id.Text = GLB_NLS_GUI_CONFIGURATION.GetString(209)
    ef_site_name.Text = GLB_NLS_GUI_CONFIGURATION.GetString(210)
    ef_operator.Text = GLB_NLS_GUI_CONFIGURATION.GetString(381)

    ' filters
    Call ef_site_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, LEN_SITE_ID)
    Call ef_site_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_SITE_NAME)
    Call ef_operator.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_OPERATOR)

    If Me.ScreenMode = frm_base_edit.ENUM_SCREEN_MODE.MODE_EDIT Then
      ef_site_id.IsReadOnly = True
    Else
      ef_site_id.IsReadOnly = False
    End If

  End Sub ' GUI_InitControls

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim site_item As CLASS_SITE

    site_item = DbReadObject

    If site_item.SiteId > 0 Then
      ef_site_id.Value = site_item.SiteId
    End If
    ef_site_name.Value = site_item.SiteName

    ' Load defined operator name
    If (site_item.OperatorId <> -1) Then
      ef_operator.Value = CLASS_SITE_OPERATOR.GetOperatorNameForId(site_item.OperatorId, SqlCtx)
    End If

    ' Protect against non-existent items
    If ef_operator.Value <> "" Then
      m_selected_operator_id = site_item.OperatorId
    End If

  End Sub ' GUI_SetScreenData

  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Dim nls_param1 As String

    ' blank field
    If ef_site_id.Value = "" Then
      nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(209)
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , nls_param1)
      Call ef_site_id.Focus()

      Return False
    End If

    ' Zero value
    If ef_site_id.Value = 0 Then
      nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(209)
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(125), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , nls_param1)
      Call ef_site_id.Focus()

      Return False
    End If

    ' blank field
    If ef_site_name.Value = "" Then
      nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(210)
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , nls_param1)
      Call ef_site_name.Focus()

      Return False
    End If

    ' blank field
    ' Do not get value from screen because entry field only holds the name and not the id.
    ' Every time the entry field is changed, member m_selected_operator is updated
    If Not m_selected_operator_id > 0 Then
      nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(381)
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , nls_param1)
      Call btn_select_operator.Focus()

      Return False
    End If

    Return True

  End Function ' GUI_IsScreenDataOk

  Protected Overrides Sub GUI_GetScreenData()

    Dim site_item As CLASS_SITE

    site_item = DbEditedObject

    site_item.SiteId = ef_site_id.Value
    site_item.SiteName = ef_site_name.Value

    ' Do not get value from screen because entry field only holds the name and not the id.
    ' Every time the entry field is changed, member m_selected_operator is updated
    site_item.OperatorId = m_selected_operator_id

    site_item.OperatorName = ef_operator.Value

  End Sub ' GUI_GetScreenData

  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Dim site_class As CLASS_SITE
    Dim nls_param1 As String
    Dim nls_param2 As String
    Dim rc As mdl_NLS.ENUM_MB_RESULT

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_SITE
        site_class = DbEditedObject
        site_class.SiteId = 0
        site_class.OperatorId = m_initial_operator_id
        DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          site_class = Me.DbEditedObject
          nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(380)
          nls_param2 = m_input_site_id
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(103), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1, _
                          nls_param2)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          site_class = Me.DbEditedObject
          nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(380)
          nls_param2 = site_class.SiteId
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(114), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1, _
                          nls_param2)
          Call ef_site_id.Focus()
        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          site_class = Me.DbEditedObject
          nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(380)
          nls_param2 = site_class.SiteId
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(105), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1, _
                          nls_param2)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          site_class = Me.DbEditedObject
          nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(380)
          nls_param2 = site_class.SiteName
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(107), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1, _
                          nls_param2)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE
        DeleteOperation = False
        site_class = Me.DbEditedObject
        nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(380)
        nls_param2 = site_class.SiteName
        rc = NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(112), _
                        ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        ENUM_MB_BTN.MB_BTN_YES_NO, _
                        ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
                        nls_param1, _
                        nls_param2)
        If rc = ENUM_MB_RESULT.MB_RESULT_YES Then
          DeleteOperation = True
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_DELETE
        If DeleteOperation Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        Else
          DbStatus = ENUM_STATUS.STATUS_ERROR
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK And DeleteOperation Then
          site_class = Me.DbEditedObject
          nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(380)
          nls_param2 = site_class.SiteName
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(116), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1, _
                          nls_param2)
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)

    Dim super_center_flag As String

    ' Protect against access when not running in SuperCenter Mode
    super_center_flag = GetSuperCenterData("Enabled")
    If super_center_flag = "1" Then
      AndPerm.Write = True
      AndPerm.Delete = True
      AndPerm.Read = True
    Else
      AndPerm.Write = False
      AndPerm.Delete = False
      AndPerm.Read = False
    End If

  End Sub ' GUI_Permissions

  Protected Overrides Sub GUI_SetInitialFocus()
    If Me.ScreenMode = frm_base_edit.ENUM_SCREEN_MODE.MODE_NEW Then
      Me.ActiveControl = Me.ef_site_id
    Else
      Me.ActiveControl = Me.ef_site_name
    End If
  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Init form in new mode
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overloads Sub ShowNewItem(Optional ByVal OperatorId As Integer = CLASS_SITE_OPERATOR.INVALID_OPERATOR_ID)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    DbObjectId = Nothing
    m_initial_operator_id = OperatorId
    DbStatus = ENUM_STATUS.STATUS_OK

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowNewItem

  ' PURPOSE: Init form in edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '       - SiteId
  '       - SiteName
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overloads Sub ShowEditItem(ByVal SiteId As Integer, ByVal SiteName As String)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Me.DbObjectId = SiteId
    Me.m_input_site_id = SiteName

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If
  End Sub ' ShowEditItem

#End Region ' Overrides

#Region " Events "

  ' PURPOSE: Launch operator selection form and get a valid operator id
  '
  '  PARAMS:
  '     - INPUT:
  '           - sender
  '           - event args
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Sub btn_select_operator_ClickEvent() Handles btn_select_operator.ClickEvent

    Dim selected_operators() As Integer
    Dim frm_p As frm_sites_operators_sel
    Dim ctx As Integer

    Try

      frm_p = New frm_sites_operators_sel
      selected_operators = frm_p.ShowForSelect(frm_sites_operators_sel.ENUM_RESULT_TYPE.RT_OPERATOR)
      If Not IsNothing(selected_operators) Then
        If selected_operators.Length > 0 Then
          ' Get operator name from database
          ef_operator.Value = CLASS_SITE_OPERATOR.GetOperatorNameForId(selected_operators(0), ctx)
          ' Save ID into a member to get it (not the name) when GetScreenData function is called
          m_selected_operator_id = selected_operators(0)
        End If
      End If

    Finally

    End Try

  End Sub ' btn_select_operator_ClickEvent

#End Region ' Events

End Class
