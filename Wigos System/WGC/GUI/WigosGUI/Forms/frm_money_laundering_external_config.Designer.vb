<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_money_laundering_external_config
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.lbl_minwage_desc = New GUI_Controls.uc_text_field
    Me.ef_minimum_wage = New GUI_Controls.uc_entry_field
    Me.lbl_info = New System.Windows.Forms.Label
    Me.ef_aml_name = New GUI_Controls.uc_entry_field
    Me.chk_active_aml = New System.Windows.Forms.CheckBox
    Me.gb_limit = New System.Windows.Forms.GroupBox
    Me.gb_prize_ident_limit = New System.Windows.Forms.GroupBox
    Me.ef_prize_ident_limit = New GUI_Controls.uc_entry_field
    Me.lbl_prize_ident_absolute = New GUI_Controls.uc_text_field
    Me.lbl_prize_ident_limit = New System.Windows.Forms.Label
    Me.lbl_prize_ident_msg = New System.Windows.Forms.Label
    Me.tb_prize_ident_msg = New System.Windows.Forms.TextBox
    Me.gb_rchg_ident_limit = New System.Windows.Forms.GroupBox
    Me.lbl_rchg_ident_absolute = New GUI_Controls.uc_text_field
    Me.tb_rchg_ident_msg = New System.Windows.Forms.TextBox
    Me.lbl_rchg_ident_msg = New System.Windows.Forms.Label
    Me.ef_rchg_ident_limit = New GUI_Controls.uc_entry_field
    Me.lbl_rchg_ident_limit = New System.Windows.Forms.Label
    Me.ef_aml_expiration_days = New GUI_Controls.uc_entry_field
    Me.lbl_days = New System.Windows.Forms.Label
    Me.panel_data.SuspendLayout()
    Me.gb_limit.SuspendLayout()
    Me.gb_prize_ident_limit.SuspendLayout()
    Me.gb_rchg_ident_limit.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.lbl_days)
    Me.panel_data.Controls.Add(Me.ef_aml_expiration_days)
    Me.panel_data.Controls.Add(Me.lbl_info)
    Me.panel_data.Controls.Add(Me.ef_aml_name)
    Me.panel_data.Controls.Add(Me.chk_active_aml)
    Me.panel_data.Controls.Add(Me.gb_limit)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(516, 508)
    '
    'lbl_minwage_desc
    '
    Me.lbl_minwage_desc.IsReadOnly = True
    Me.lbl_minwage_desc.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_minwage_desc.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_minwage_desc.Location = New System.Drawing.Point(6, 16)
    Me.lbl_minwage_desc.Name = "lbl_minwage_desc"
    Me.lbl_minwage_desc.Size = New System.Drawing.Size(461, 25)
    Me.lbl_minwage_desc.SufixText = "Sufix Text"
    Me.lbl_minwage_desc.SufixTextVisible = True
    Me.lbl_minwage_desc.TabIndex = 0
    Me.lbl_minwage_desc.TabStop = False
    Me.lbl_minwage_desc.TextVisible = False
    Me.lbl_minwage_desc.TextWidth = 0
    Me.lbl_minwage_desc.Value = "xDesc"
    '
    'ef_minimum_wage
    '
    Me.ef_minimum_wage.DoubleValue = 0
    Me.ef_minimum_wage.ForeColor = System.Drawing.SystemColors.ControlText
    Me.ef_minimum_wage.IntegerValue = 0
    Me.ef_minimum_wage.IsReadOnly = False
    Me.ef_minimum_wage.Location = New System.Drawing.Point(3, 45)
    Me.ef_minimum_wage.Name = "ef_minimum_wage"
    Me.ef_minimum_wage.PlaceHolder = Nothing
    Me.ef_minimum_wage.Size = New System.Drawing.Size(183, 24)
    Me.ef_minimum_wage.SufixText = "Sufix Text"
    Me.ef_minimum_wage.SufixTextVisible = True
    Me.ef_minimum_wage.TabIndex = 0
    Me.ef_minimum_wage.Tag = "4"
    Me.ef_minimum_wage.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_minimum_wage.TextValue = ""
    Me.ef_minimum_wage.TextVisible = False
    Me.ef_minimum_wage.TextWidth = 100
    Me.ef_minimum_wage.Value = ""
    Me.ef_minimum_wage.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_info
    '
    Me.lbl_info.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.lbl_info.ForeColor = System.Drawing.Color.Navy
    Me.lbl_info.Location = New System.Drawing.Point(7, 86)
    Me.lbl_info.Name = "lbl_info"
    Me.lbl_info.RightToLeft = System.Windows.Forms.RightToLeft.Yes
    Me.lbl_info.Size = New System.Drawing.Size(470, 17)
    Me.lbl_info.TabIndex = 0
    Me.lbl_info.Text = "xMasterProfiles"
    Me.lbl_info.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.lbl_info.Visible = False
    '
    'ef_aml_name
    '
    Me.ef_aml_name.DoubleValue = 0
    Me.ef_aml_name.IntegerValue = 0
    Me.ef_aml_name.IsReadOnly = False
    Me.ef_aml_name.Location = New System.Drawing.Point(21, 26)
    Me.ef_aml_name.Name = "ef_aml_name"
    Me.ef_aml_name.PlaceHolder = Nothing
    Me.ef_aml_name.Size = New System.Drawing.Size(256, 24)
    Me.ef_aml_name.SufixText = "Sufix Text"
    Me.ef_aml_name.SufixTextVisible = True
    Me.ef_aml_name.TabIndex = 1
    Me.ef_aml_name.Tag = "2"
    Me.ef_aml_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_aml_name.TextValue = ""
    Me.ef_aml_name.TextWidth = 50
    Me.ef_aml_name.Value = ""
    Me.ef_aml_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_active_aml
    '
    Me.chk_active_aml.AutoSize = True
    Me.chk_active_aml.Location = New System.Drawing.Point(11, 3)
    Me.chk_active_aml.Name = "chk_active_aml"
    Me.chk_active_aml.Size = New System.Drawing.Size(190, 17)
    Me.chk_active_aml.TabIndex = 0
    Me.chk_active_aml.Tag = "1"
    Me.chk_active_aml.Text = "xActiveAntiMoneyLaundering"
    Me.chk_active_aml.UseVisualStyleBackColor = True
    '
    'gb_limit
    '
    Me.gb_limit.Controls.Add(Me.gb_prize_ident_limit)
    Me.gb_limit.Controls.Add(Me.gb_rchg_ident_limit)
    Me.gb_limit.Controls.Add(Me.lbl_minwage_desc)
    Me.gb_limit.Controls.Add(Me.ef_minimum_wage)
    Me.gb_limit.Location = New System.Drawing.Point(7, 113)
    Me.gb_limit.Name = "gb_limit"
    Me.gb_limit.Size = New System.Drawing.Size(493, 394)
    Me.gb_limit.TabIndex = 3
    Me.gb_limit.TabStop = False
    Me.gb_limit.Text = "xLimit"
    '
    'gb_prize_ident_limit
    '
    Me.gb_prize_ident_limit.Controls.Add(Me.ef_prize_ident_limit)
    Me.gb_prize_ident_limit.Controls.Add(Me.lbl_prize_ident_absolute)
    Me.gb_prize_ident_limit.Controls.Add(Me.lbl_prize_ident_limit)
    Me.gb_prize_ident_limit.Controls.Add(Me.lbl_prize_ident_msg)
    Me.gb_prize_ident_limit.Controls.Add(Me.tb_prize_ident_msg)
    Me.gb_prize_ident_limit.Location = New System.Drawing.Point(52, 234)
    Me.gb_prize_ident_limit.Name = "gb_prize_ident_limit"
    Me.gb_prize_ident_limit.Size = New System.Drawing.Size(390, 138)
    Me.gb_prize_ident_limit.TabIndex = 3
    Me.gb_prize_ident_limit.TabStop = False
    '
    'ef_prize_ident_limit
    '
    Me.ef_prize_ident_limit.DoubleValue = 0
    Me.ef_prize_ident_limit.IntegerValue = 0
    Me.ef_prize_ident_limit.IsReadOnly = False
    Me.ef_prize_ident_limit.Location = New System.Drawing.Point(120, 30)
    Me.ef_prize_ident_limit.Name = "ef_prize_ident_limit"
    Me.ef_prize_ident_limit.PlaceHolder = Nothing
    Me.ef_prize_ident_limit.Size = New System.Drawing.Size(101, 24)
    Me.ef_prize_ident_limit.SufixText = "Sufix Text"
    Me.ef_prize_ident_limit.SufixTextVisible = True
    Me.ef_prize_ident_limit.TabIndex = 1
    Me.ef_prize_ident_limit.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_prize_ident_limit.TextValue = ""
    Me.ef_prize_ident_limit.TextVisible = False
    Me.ef_prize_ident_limit.TextWidth = 0
    Me.ef_prize_ident_limit.Value = ""
    Me.ef_prize_ident_limit.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_prize_ident_absolute
    '
    Me.lbl_prize_ident_absolute.IsReadOnly = True
    Me.lbl_prize_ident_absolute.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_prize_ident_absolute.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_prize_ident_absolute.Location = New System.Drawing.Point(227, 29)
    Me.lbl_prize_ident_absolute.Name = "lbl_prize_ident_absolute"
    Me.lbl_prize_ident_absolute.Size = New System.Drawing.Size(153, 24)
    Me.lbl_prize_ident_absolute.SufixText = "Sufix Text"
    Me.lbl_prize_ident_absolute.SufixTextVisible = True
    Me.lbl_prize_ident_absolute.TabIndex = 0
    Me.lbl_prize_ident_absolute.TabStop = False
    Me.lbl_prize_ident_absolute.TextVisible = False
    Me.lbl_prize_ident_absolute.TextWidth = 0
    Me.lbl_prize_ident_absolute.Value = "$0.00"
    '
    'lbl_prize_ident_limit
    '
    Me.lbl_prize_ident_limit.Location = New System.Drawing.Point(16, 36)
    Me.lbl_prize_ident_limit.Name = "lbl_prize_ident_limit"
    Me.lbl_prize_ident_limit.Size = New System.Drawing.Size(101, 13)
    Me.lbl_prize_ident_limit.TabIndex = 0
    Me.lbl_prize_ident_limit.Text = "xIdentifLimit"
    Me.lbl_prize_ident_limit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_prize_ident_msg
    '
    Me.lbl_prize_ident_msg.Location = New System.Drawing.Point(56, 65)
    Me.lbl_prize_ident_msg.Name = "lbl_prize_ident_msg"
    Me.lbl_prize_ident_msg.Size = New System.Drawing.Size(63, 13)
    Me.lbl_prize_ident_msg.TabIndex = 0
    Me.lbl_prize_ident_msg.Text = "xMessage"
    Me.lbl_prize_ident_msg.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'tb_prize_ident_msg
    '
    Me.tb_prize_ident_msg.AcceptsReturn = True
    Me.tb_prize_ident_msg.Location = New System.Drawing.Point(122, 60)
    Me.tb_prize_ident_msg.MaxLength = 256
    Me.tb_prize_ident_msg.Multiline = True
    Me.tb_prize_ident_msg.Name = "tb_prize_ident_msg"
    Me.tb_prize_ident_msg.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.tb_prize_ident_msg.Size = New System.Drawing.Size(258, 50)
    Me.tb_prize_ident_msg.TabIndex = 2
    '
    'gb_rchg_ident_limit
    '
    Me.gb_rchg_ident_limit.Controls.Add(Me.lbl_rchg_ident_absolute)
    Me.gb_rchg_ident_limit.Controls.Add(Me.tb_rchg_ident_msg)
    Me.gb_rchg_ident_limit.Controls.Add(Me.lbl_rchg_ident_msg)
    Me.gb_rchg_ident_limit.Controls.Add(Me.ef_rchg_ident_limit)
    Me.gb_rchg_ident_limit.Controls.Add(Me.lbl_rchg_ident_limit)
    Me.gb_rchg_ident_limit.Location = New System.Drawing.Point(52, 90)
    Me.gb_rchg_ident_limit.Name = "gb_rchg_ident_limit"
    Me.gb_rchg_ident_limit.Size = New System.Drawing.Size(390, 138)
    Me.gb_rchg_ident_limit.TabIndex = 2
    Me.gb_rchg_ident_limit.TabStop = False
    '
    'lbl_rchg_ident_absolute
    '
    Me.lbl_rchg_ident_absolute.IsReadOnly = True
    Me.lbl_rchg_ident_absolute.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_rchg_ident_absolute.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_rchg_ident_absolute.Location = New System.Drawing.Point(227, 29)
    Me.lbl_rchg_ident_absolute.Name = "lbl_rchg_ident_absolute"
    Me.lbl_rchg_ident_absolute.Size = New System.Drawing.Size(153, 24)
    Me.lbl_rchg_ident_absolute.SufixText = "Sufix Text"
    Me.lbl_rchg_ident_absolute.SufixTextVisible = True
    Me.lbl_rchg_ident_absolute.TabIndex = 0
    Me.lbl_rchg_ident_absolute.TabStop = False
    Me.lbl_rchg_ident_absolute.TextVisible = False
    Me.lbl_rchg_ident_absolute.TextWidth = 0
    Me.lbl_rchg_ident_absolute.Value = "$0.00"
    '
    'tb_rchg_ident_msg
    '
    Me.tb_rchg_ident_msg.AcceptsReturn = True
    Me.tb_rchg_ident_msg.Location = New System.Drawing.Point(122, 60)
    Me.tb_rchg_ident_msg.MaxLength = 256
    Me.tb_rchg_ident_msg.Multiline = True
    Me.tb_rchg_ident_msg.Name = "tb_rchg_ident_msg"
    Me.tb_rchg_ident_msg.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.tb_rchg_ident_msg.Size = New System.Drawing.Size(258, 50)
    Me.tb_rchg_ident_msg.TabIndex = 2
    '
    'lbl_rchg_ident_msg
    '
    Me.lbl_rchg_ident_msg.Location = New System.Drawing.Point(56, 65)
    Me.lbl_rchg_ident_msg.Name = "lbl_rchg_ident_msg"
    Me.lbl_rchg_ident_msg.Size = New System.Drawing.Size(63, 13)
    Me.lbl_rchg_ident_msg.TabIndex = 0
    Me.lbl_rchg_ident_msg.Text = "xMessage"
    Me.lbl_rchg_ident_msg.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'ef_rchg_ident_limit
    '
    Me.ef_rchg_ident_limit.DoubleValue = 0
    Me.ef_rchg_ident_limit.IntegerValue = 0
    Me.ef_rchg_ident_limit.IsReadOnly = False
    Me.ef_rchg_ident_limit.Location = New System.Drawing.Point(120, 30)
    Me.ef_rchg_ident_limit.Name = "ef_rchg_ident_limit"
    Me.ef_rchg_ident_limit.PlaceHolder = Nothing
    Me.ef_rchg_ident_limit.Size = New System.Drawing.Size(101, 24)
    Me.ef_rchg_ident_limit.SufixText = "Sufix Text"
    Me.ef_rchg_ident_limit.SufixTextVisible = True
    Me.ef_rchg_ident_limit.TabIndex = 1
    Me.ef_rchg_ident_limit.Tag = "5"
    Me.ef_rchg_ident_limit.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_rchg_ident_limit.TextValue = ""
    Me.ef_rchg_ident_limit.TextVisible = False
    Me.ef_rchg_ident_limit.TextWidth = 0
    Me.ef_rchg_ident_limit.Value = ""
    Me.ef_rchg_ident_limit.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_rchg_ident_limit
    '
    Me.lbl_rchg_ident_limit.Location = New System.Drawing.Point(16, 36)
    Me.lbl_rchg_ident_limit.Name = "lbl_rchg_ident_limit"
    Me.lbl_rchg_ident_limit.Size = New System.Drawing.Size(101, 13)
    Me.lbl_rchg_ident_limit.TabIndex = 1
    Me.lbl_rchg_ident_limit.Text = "xIdentifLimit"
    Me.lbl_rchg_ident_limit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'ef_aml_expiration_days
    '
    Me.ef_aml_expiration_days.DoubleValue = 0
    Me.ef_aml_expiration_days.IntegerValue = 0
    Me.ef_aml_expiration_days.IsReadOnly = False
    Me.ef_aml_expiration_days.Location = New System.Drawing.Point(21, 50)
    Me.ef_aml_expiration_days.Name = "ef_aml_expiration_days"
    Me.ef_aml_expiration_days.PlaceHolder = Nothing
    Me.ef_aml_expiration_days.Size = New System.Drawing.Size(256, 24)
    Me.ef_aml_expiration_days.SufixText = "Sufix Text"
    Me.ef_aml_expiration_days.SufixTextVisible = True
    Me.ef_aml_expiration_days.TabIndex = 2
    Me.ef_aml_expiration_days.Tag = "2"
    Me.ef_aml_expiration_days.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_aml_expiration_days.TextValue = ""
    Me.ef_aml_expiration_days.TextWidth = 200
    Me.ef_aml_expiration_days.Value = ""
    Me.ef_aml_expiration_days.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_days
    '
    Me.lbl_days.Location = New System.Drawing.Point(283, 50)
    Me.lbl_days.Name = "lbl_days"
    Me.lbl_days.Size = New System.Drawing.Size(60, 24)
    Me.lbl_days.TabIndex = 21
    Me.lbl_days.Text = "xdays"
    Me.lbl_days.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'frm_money_laundering_external_config
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(620, 519)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_money_laundering_external_config"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_money_laundering_external_config.vb"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.gb_limit.ResumeLayout(False)
    Me.gb_prize_ident_limit.ResumeLayout(False)
    Me.gb_prize_ident_limit.PerformLayout()
    Me.gb_rchg_ident_limit.ResumeLayout(False)
    Me.gb_rchg_ident_limit.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents lbl_info As System.Windows.Forms.Label
  Friend WithEvents ef_aml_name As GUI_Controls.uc_entry_field
  Friend WithEvents chk_active_aml As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_minwage_desc As GUI_Controls.uc_text_field
  Friend WithEvents ef_minimum_wage As GUI_Controls.uc_entry_field
  Friend WithEvents gb_limit As System.Windows.Forms.GroupBox
  Friend WithEvents gb_prize_ident_limit As System.Windows.Forms.GroupBox
  Friend WithEvents ef_prize_ident_limit As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_prize_ident_absolute As GUI_Controls.uc_text_field
  Friend WithEvents lbl_prize_ident_limit As System.Windows.Forms.Label
  Friend WithEvents lbl_prize_ident_msg As System.Windows.Forms.Label
  Friend WithEvents tb_prize_ident_msg As System.Windows.Forms.TextBox
  Friend WithEvents gb_rchg_ident_limit As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_rchg_ident_absolute As GUI_Controls.uc_text_field
  Friend WithEvents tb_rchg_ident_msg As System.Windows.Forms.TextBox
  Friend WithEvents lbl_rchg_ident_msg As System.Windows.Forms.Label
  Friend WithEvents ef_rchg_ident_limit As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_rchg_ident_limit As System.Windows.Forms.Label
  Friend WithEvents ef_aml_expiration_days As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_days As System.Windows.Forms.Label
End Class
