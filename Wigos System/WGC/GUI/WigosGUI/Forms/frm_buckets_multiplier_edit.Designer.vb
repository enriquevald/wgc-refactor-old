<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_buckets_multiplier_edit
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_buckets_multiplier_edit))
    Me.cmb_buckets = New GUI_Controls.uc_combo()
    Me.uc_schedule = New GUI_Controls.uc_schedule()
    Me.chk_enabled = New System.Windows.Forms.CheckBox()
    Me.uc_terminals_group_filter = New GUI_Controls.uc_terminals_group_filter()
    Me.uc_site_select = New GUI_Controls.uc_sites_sel()
    Me.ef_description = New GUI_Controls.uc_entry_field()
    Me.gb_bucket = New System.Windows.Forms.GroupBox()
    Me.ef_multiplier = New GUI_Controls.uc_entry_field()
    Me.gp_apply_levels = New System.Windows.Forms.GroupBox()
    Me.dg_apply = New GUI_Controls.uc_grid()
    Me.panel_data.SuspendLayout()
    Me.gb_bucket.SuspendLayout()
    Me.gp_apply_levels.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gp_apply_levels)
    Me.panel_data.Controls.Add(Me.gb_bucket)
    Me.panel_data.Controls.Add(Me.uc_site_select)
    Me.panel_data.Controls.Add(Me.uc_terminals_group_filter)
    Me.panel_data.Controls.Add(Me.uc_schedule)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(443, 691)
    '
    'cmb_buckets
    '
    Me.cmb_buckets.AllowUnlistedValues = False
    Me.cmb_buckets.AutoCompleteMode = False
    Me.cmb_buckets.IsReadOnly = False
    Me.cmb_buckets.Location = New System.Drawing.Point(6, 15)
    Me.cmb_buckets.Name = "cmb_buckets"
    Me.cmb_buckets.SelectedIndex = -1
    Me.cmb_buckets.Size = New System.Drawing.Size(268, 24)
    Me.cmb_buckets.SufixText = "Sufix Text"
    Me.cmb_buckets.SufixTextVisible = True
    Me.cmb_buckets.TabIndex = 200
    Me.cmb_buckets.TextCombo = Nothing
    Me.cmb_buckets.TextWidth = 87
    '
    'uc_schedule
    '
    Me.uc_schedule.CheckedDateTo = True
    Me.uc_schedule.DateFrom = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_schedule.DateTo = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_schedule.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_schedule.Location = New System.Drawing.Point(4, 286)
    Me.uc_schedule.Name = "uc_schedule"
    Me.uc_schedule.SecondTime = False
    Me.uc_schedule.SecondTimeFrom = 0
    Me.uc_schedule.SecondTimeTo = 0
    Me.uc_schedule.ShowCheckBoxDateTo = False
    Me.uc_schedule.Size = New System.Drawing.Size(410, 239)
    Me.uc_schedule.TabIndex = 206
    Me.uc_schedule.TimeFrom = 0
    Me.uc_schedule.TimeTo = 0
    Me.uc_schedule.Weekday = 127
    '
    'chk_enabled
    '
    Me.chk_enabled.AutoSize = True
    Me.chk_enabled.Location = New System.Drawing.Point(303, 72)
    Me.chk_enabled.Name = "chk_enabled"
    Me.chk_enabled.Size = New System.Drawing.Size(78, 17)
    Me.chk_enabled.TabIndex = 203
    Me.chk_enabled.Text = "xEnabled"
    Me.chk_enabled.UseVisualStyleBackColor = True
    '
    'uc_terminals_group_filter
    '
    Me.uc_terminals_group_filter.ForbiddenGroups = CType(resources.GetObject("uc_terminals_group_filter.ForbiddenGroups"), Microsoft.VisualBasic.Collection)
    Me.uc_terminals_group_filter.HeightOnExpanded = 300
    Me.uc_terminals_group_filter.Location = New System.Drawing.Point(9, 230)
    Me.uc_terminals_group_filter.Name = "uc_terminals_group_filter"
    Me.uc_terminals_group_filter.SelectedIndex = 0
    Me.uc_terminals_group_filter.SetDisplayMode = GUI_Controls.uc_terminals_group_filter.DisplayMode.CollapsedWithButtons
    Me.uc_terminals_group_filter.SetInitMode = GUI_Controls.uc_terminals_group_filter.InitMode.NormalMode
    Me.uc_terminals_group_filter.ShowGroupBoxLine = True
    Me.uc_terminals_group_filter.ShowGroups = Nothing
    Me.uc_terminals_group_filter.ShowNodeAll = True
    Me.uc_terminals_group_filter.Size = New System.Drawing.Size(399, 54)
    Me.uc_terminals_group_filter.TabIndex = 205
    Me.uc_terminals_group_filter.ThreeStateCheckMode = True
    '
    'uc_site_select
    '
    Me.uc_site_select.Location = New System.Drawing.Point(6, 521)
    Me.uc_site_select.MultiSelect = True
    Me.uc_site_select.Name = "uc_site_select"
    Me.uc_site_select.ShowIsoCode = False
    Me.uc_site_select.ShowMultisiteRow = True
    Me.uc_site_select.Size = New System.Drawing.Size(334, 168)
    Me.uc_site_select.TabIndex = 207
    '
    'ef_description
    '
    Me.ef_description.DoubleValue = 0.0R
    Me.ef_description.IntegerValue = 0
    Me.ef_description.IsReadOnly = False
    Me.ef_description.Location = New System.Drawing.Point(13, 42)
    Me.ef_description.Name = "ef_description"
    Me.ef_description.PlaceHolder = Nothing
    Me.ef_description.Size = New System.Drawing.Size(262, 24)
    Me.ef_description.SufixText = "Sufix Text"
    Me.ef_description.SufixTextVisible = True
    Me.ef_description.TabIndex = 201
    Me.ef_description.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_description.TextValue = ""
    Me.ef_description.Value = ""
    Me.ef_description.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_bucket
    '
    Me.gb_bucket.Controls.Add(Me.ef_multiplier)
    Me.gb_bucket.Controls.Add(Me.ef_description)
    Me.gb_bucket.Controls.Add(Me.chk_enabled)
    Me.gb_bucket.Controls.Add(Me.cmb_buckets)
    Me.gb_bucket.Location = New System.Drawing.Point(9, 3)
    Me.gb_bucket.Name = "gb_bucket"
    Me.gb_bucket.Size = New System.Drawing.Size(399, 99)
    Me.gb_bucket.TabIndex = 20
    Me.gb_bucket.TabStop = False
    Me.gb_bucket.Text = "xMessage"
    '
    'ef_multiplier
    '
    Me.ef_multiplier.DoubleValue = 0.0R
    Me.ef_multiplier.IntegerValue = 0
    Me.ef_multiplier.IsReadOnly = False
    Me.ef_multiplier.Location = New System.Drawing.Point(13, 68)
    Me.ef_multiplier.Name = "ef_multiplier"
    Me.ef_multiplier.PlaceHolder = Nothing
    Me.ef_multiplier.Size = New System.Drawing.Size(139, 24)
    Me.ef_multiplier.SufixText = "Sufix Text"
    Me.ef_multiplier.SufixTextVisible = True
    Me.ef_multiplier.TabIndex = 202
    Me.ef_multiplier.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_multiplier.TextValue = ""
    Me.ef_multiplier.Value = ""
    Me.ef_multiplier.ValueForeColor = System.Drawing.Color.Blue
    '
    'gp_apply_levels
    '
    Me.gp_apply_levels.Controls.Add(Me.dg_apply)
    Me.gp_apply_levels.Location = New System.Drawing.Point(9, 108)
    Me.gp_apply_levels.Name = "gp_apply_levels"
    Me.gp_apply_levels.Size = New System.Drawing.Size(399, 115)
    Me.gp_apply_levels.TabIndex = 207
    Me.gp_apply_levels.TabStop = False
    Me.gp_apply_levels.Text = "#apply_levels"
    '
    'dg_apply
    '
    Me.dg_apply.CurrentCol = -1
    Me.dg_apply.CurrentRow = -1
    Me.dg_apply.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_apply.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_apply.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_apply.Location = New System.Drawing.Point(7, 21)
    Me.dg_apply.Name = "dg_apply"
    Me.dg_apply.PanelRightVisible = True
    Me.dg_apply.Redraw = True
    Me.dg_apply.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_apply.Size = New System.Drawing.Size(200, 84)
    Me.dg_apply.Sortable = False
    Me.dg_apply.SortAscending = True
    Me.dg_apply.SortByCol = 0
    Me.dg_apply.TabIndex = 204
    Me.dg_apply.ToolTipped = True
    Me.dg_apply.TopRow = -2
    Me.dg_apply.WordWrap = False
    '
    'frm_buckets_multiplier_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(547, 698)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_buckets_multiplier_edit"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_buckets_multiplier_edit"
    Me.panel_data.ResumeLayout(False)
    Me.gb_bucket.ResumeLayout(False)
    Me.gb_bucket.PerformLayout()
    Me.gp_apply_levels.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents cmb_buckets As GUI_Controls.uc_combo
  Friend WithEvents uc_schedule As GUI_Controls.uc_schedule
  Friend WithEvents chk_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents uc_terminals_group_filter As GUI_Controls.uc_terminals_group_filter
  Friend WithEvents uc_site_select As GUI_Controls.uc_sites_sel
  Friend WithEvents ef_description As GUI_Controls.uc_entry_field
  Friend WithEvents gb_bucket As System.Windows.Forms.GroupBox
  Friend WithEvents ef_multiplier As GUI_Controls.uc_entry_field
  Friend WithEvents gp_apply_levels As System.Windows.Forms.GroupBox
  Friend WithEvents dg_apply As GUI_Controls.uc_grid
End Class
