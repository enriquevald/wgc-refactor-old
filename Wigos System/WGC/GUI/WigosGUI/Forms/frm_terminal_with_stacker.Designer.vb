<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_terminal_with_stacker
    Inherits GUI_Controls.frm_base_sel_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.ef_last_hours = New GUI_Controls.uc_entry_field()
    Me.opt_with_activity = New System.Windows.Forms.RadioButton()
    Me.opt_without_activity = New System.Windows.Forms.RadioButton()
    Me.uc_provider_filter = New GUI_Controls.uc_provider()
    Me.ef_stacker_id = New GUI_Controls.uc_entry_field()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.ef_stacker_id)
    Me.panel_filter.Controls.Add(Me.uc_provider_filter)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Controls.Add(Me.chk_terminal_location)
    Me.panel_filter.Size = New System.Drawing.Size(1126, 185)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_terminal_location, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_provider_filter, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_stacker_id, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 189)
    Me.panel_data.Size = New System.Drawing.Size(1126, 389)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1120, 27)
    Me.pn_separator_line.TabIndex = 0
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1120, 5)
    '
    'chk_terminal_location
    '
    Me.chk_terminal_location.AutoSize = True
    Me.chk_terminal_location.Location = New System.Drawing.Point(6, 162)
    Me.chk_terminal_location.Name = "chk_terminal_location"
    Me.chk_terminal_location.Size = New System.Drawing.Size(130, 17)
    Me.chk_terminal_location.TabIndex = 2
    Me.chk_terminal_location.Text = "xTerminalLocation"
    Me.chk_terminal_location.UseVisualStyleBackColor = True
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.ef_last_hours)
    Me.gb_date.Controls.Add(Me.opt_with_activity)
    Me.gb_date.Controls.Add(Me.opt_without_activity)
    Me.gb_date.Location = New System.Drawing.Point(7, 7)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(214, 101)
    Me.gb_date.TabIndex = 0
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'ef_last_hours
    '
    Me.ef_last_hours.DoubleValue = 0.0R
    Me.ef_last_hours.IntegerValue = 0
    Me.ef_last_hours.IsReadOnly = False
    Me.ef_last_hours.Location = New System.Drawing.Point(7, 44)
    Me.ef_last_hours.Name = "ef_last_hours"
    Me.ef_last_hours.PlaceHolder = Nothing
    Me.ef_last_hours.Size = New System.Drawing.Size(200, 24)
    Me.ef_last_hours.SufixText = "xHours"
    Me.ef_last_hours.SufixTextVisible = True
    Me.ef_last_hours.SufixTextWidth = 45
    Me.ef_last_hours.TabIndex = 1
    Me.ef_last_hours.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_last_hours.TextValue = ""
    Me.ef_last_hours.TextWidth = 120
    Me.ef_last_hours.Value = ""
    Me.ef_last_hours.ValueForeColor = System.Drawing.Color.Blue
    '
    'opt_with_activity
    '
    Me.opt_with_activity.AutoSize = True
    Me.opt_with_activity.Checked = True
    Me.opt_with_activity.Location = New System.Drawing.Point(6, 20)
    Me.opt_with_activity.Name = "opt_with_activity"
    Me.opt_with_activity.Size = New System.Drawing.Size(99, 17)
    Me.opt_with_activity.TabIndex = 0
    Me.opt_with_activity.TabStop = True
    Me.opt_with_activity.Text = "xWithActivity"
    Me.opt_with_activity.UseVisualStyleBackColor = True
    '
    'opt_without_activity
    '
    Me.opt_without_activity.AutoSize = True
    Me.opt_without_activity.Location = New System.Drawing.Point(6, 74)
    Me.opt_without_activity.Name = "opt_without_activity"
    Me.opt_without_activity.Size = New System.Drawing.Size(117, 17)
    Me.opt_without_activity.TabIndex = 2
    Me.opt_without_activity.Text = "xWithoutActivity"
    Me.opt_without_activity.UseVisualStyleBackColor = True
    '
    'uc_provider_filter
    '
    Me.uc_provider_filter.FilterByCurrency = False
    Me.uc_provider_filter.Location = New System.Drawing.Point(228, 3)
    Me.uc_provider_filter.Name = "uc_provider_filter"
    Me.uc_provider_filter.Size = New System.Drawing.Size(325, 179)
    Me.uc_provider_filter.TabIndex = 3
    '
    'ef_stacker_id
    '
    Me.ef_stacker_id.DoubleValue = 0.0R
    Me.ef_stacker_id.IntegerValue = 0
    Me.ef_stacker_id.IsReadOnly = False
    Me.ef_stacker_id.Location = New System.Drawing.Point(14, 114)
    Me.ef_stacker_id.Name = "ef_stacker_id"
    Me.ef_stacker_id.PlaceHolder = Nothing
    Me.ef_stacker_id.Size = New System.Drawing.Size(200, 24)
    Me.ef_stacker_id.SufixText = "Sufix Text"
    Me.ef_stacker_id.SufixTextVisible = True
    Me.ef_stacker_id.TabIndex = 1
    Me.ef_stacker_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_stacker_id.TextValue = ""
    Me.ef_stacker_id.Value = ""
    Me.ef_stacker_id.ValueForeColor = System.Drawing.Color.Blue
    '
    'frm_terminal_with_stacker
    '
    Me.ClientSize = New System.Drawing.Size(1134, 582)
    Me.Name = "frm_terminal_with_stacker"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gb_date.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents uc_provider_filter As GUI_Controls.uc_provider
  Friend WithEvents opt_with_activity As System.Windows.Forms.RadioButton
  Friend WithEvents opt_without_activity As System.Windows.Forms.RadioButton
  Friend WithEvents ef_stacker_id As GUI_Controls.uc_entry_field
  Friend WithEvents ef_last_hours As GUI_Controls.uc_entry_field

End Class
