'-------------------------------------------------------------------
' Copyright � 2002-2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_terminals_edit_status.vb
'
' DESCRIPTION:   Change status dialog
'
' AUTHOR:        Miquel Beltran
'
' CREATION DATE: 05-NOV-2010
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 05-NOV-2010  MBF    First version
'-------------------------------------------------------------------
Option Strict On
Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Windows.Forms

Public Class frm_terminals_edit_status
  Inherits GUI_Controls.frm_base

#Region " Form Variables "
 
#End Region

#Region " Overrides "

  ' PURPOSE: Initializes the window title and all the
  '          entry fields
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  '          
  ' NOTES
  ' 
  Protected Overrides Sub GUI_InitControls()

    MyBase.GUI_InitControls()

    'Add any initialization after the MyBase.GUI_InitControls() call

    ' Init the buttons
    btn_ok.Text = GLB_NLS_GUI_CONTROLS.GetString(1)
    btn_cancel.Text = GLB_NLS_GUI_CONTROLS.GetString(2)


  End Sub ' GUI_InitControls



  ' PURPOSE: Called to be set the initial focus.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetInitialFocus()

    ' ef_password.Focus()

  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Set form permissions.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_Permissions(ByRef Perm As GUI_Controls.CLASS_GUI_USER.TYPE_PERMISSIONS)
    'Perm.Read = True
    'Perm.Write = False
    'Perm.Delete = False
    'Perm.Execute = False

  End Sub ' GUI_Permissions

  ' PURPOSE: GUI form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    ' TODO MBF 05-NOV-2010

    ' Me.FormId = ENUM_COMMON_FORM.COMMON_FORM_LOGIN

  End Sub ' GUI_SetFormId

#End Region

#Region " Public Functions "

#End Region

#Region " Private functions "

#End Region

End Class