'-------------------------------------------------------------------
' Copyright © 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_player_tracking_chips_report
' DESCRIPTION:   Player tracking chips sales and purchases report
' AUTHOR:        Didac Campanals
' CREATION DATE: 04-SEP-2014
'
' REVISION HISTORY:
'
' Date         Author   Description
' -----------  ------   -----------------------------------------------
' 04-SEP-2014  DCS      First Release
'----------------------------------------------------------------------
Option Explicit On
Option Strict Off

#Region " Imports "

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient

#End Region

Public Class frm_player_tracking_chips_report
  Inherits GUI_Controls.frm_base_sel

#Region " Constants "

  ' SQL Columnds
  Private Const SQL_COLUMN_SESSION_ID As Integer = 0
  Private Const SQL_COLUMN_GAMING_TABLE_TYPE As Integer = 1
  Private Const SQL_COLUMN_GAMING_TABLE_NAME As Integer = 2
  Private Const SQL_COLUMN_GAMING_TABLE_DATE As Integer = 3
  Private Const SQL_COLUMN_CHIPS_IN As Integer = 4
  Private Const SQL_COLUMN_CHIPS_OUT As Integer = 5
  Private Const SQL_COLUMN_TOTAL_BUY_IN As Integer = 6

  ' Grid Columns
  Private Const GRID_COLUMNS As Integer = 8
  Private Const GRID_HEADER_ROWS As Integer = 1

  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_SESSION_ID As Integer = 1
  Private Const GRID_COLUMN_GAMING_TABLE_TYPE As Integer = 2
  Private Const GRID_COLUMN_GAMING_TABLE_NAME As Integer = 3
  Private Const GRID_COLUMN_GAMING_TABLE_DATE As Integer = 4
  Private Const GRID_COLUMN_CHIPS_IN As Integer = 5
  Private Const GRID_COLUMN_CHIPS_OUT As Integer = 6
  Private Const GRID_COLUMN_TOTAL_BUY_IN As Integer = 7

  ' Grid Columns widths
  Private Const WIDTH_GRID_COLUMN_INDEX As Integer = 200
  Private Const WIDTH_GRID_COLUMN_SESSION_ID As Integer = 0
  Private Const WIDTH_GRID_COLUMN_GAMING_TABLE_TYPE As Integer = 2500
  Private Const WIDTH_GRID_COLUMN_GAMING_TABLE_NAME As Integer = 3500
  Private Const WIDTH_GRID_COLUMN_GAMING_TABLE_DATE As Integer = 1700
  Private Const WIDTH_GRID_COLUMN_CHIPS_IN As Integer = 1900
  Private Const WIDTH_GRID_COLUMN_CHIPS_OUT As Integer = 1900
  Private Const WIDTH_GRID_COLUMN_TOTAL_BUY_IN As Integer = 1900


#End Region

#Region " Enums "

  Private Enum GAMING_TABLE_STATUS
    GT_STATUS_ALL = -1
    GT_STATUS_DISABLED = 0
    GT_STATUS_ENABLED = 1
  End Enum

#End Region

#Region " Members "

  ' Filter variables
  Private m_date_from As String
  Private m_date_to As String
  Private m_gaming_table_status As GAMING_TABLE_STATUS
  Private m_table_types_list As String
  Private m_area_id As String
  Private m_bank_id As String
  Private m_area_name As String
  Private m_bank_name As String

  ' For subtotals
  Private m_subtotal_name As String
  Private m_subtotal_type As String
  Private m_subtotal_chips_in As Decimal
  Private m_subtotal_chips_out As Decimal
  Private m_subtotal_total_buy_in As Decimal

  ' For totals
  Private m_total_chips_in As Decimal
  Private m_total_chips_out As Decimal
  Private m_total_total_buy_in As Decimal

#End Region

#Region " OVERRIDES "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS: 
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    Dim _operations As String
    Dim _status As String

    _status = String.Empty
    _operations = String.Empty

    ' Dates filter checks
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & Space(1) & GLB_NLS_GUI_AUDITOR.GetString(257), _
                        m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & Space(1) & GLB_NLS_GUI_AUDITOR.GetString(258), _
                        m_date_to)

    ' Space
    PrintData.SetFilter(String.Empty, String.Empty, True)

    ' Status
    Select Case m_gaming_table_status
      Case GAMING_TABLE_STATUS.GT_STATUS_ALL
        _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1807)
      Case GAMING_TABLE_STATUS.GT_STATUS_DISABLED
        _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(247)
      Case GAMING_TABLE_STATUS.GT_STATUS_ENABLED
        _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(246)
    End Select
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(262), _status)

    ' Gaming table type
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(3409), m_table_types_list)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)

    ' Area & Bank
    PrintData.SetFilter(GLB_NLS_GUI_CONFIGURATION.GetString(435), m_area_name)
    PrintData.SetFilter(GLB_NLS_GUI_CONFIGURATION.GetString(430), m_bank_name)

    '
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4581), IIf(chk_grouped_by_type.Checked, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)))

    ' Space
    PrintData.SetFilter(String.Empty, String.Empty, True)
    PrintData.SetFilter(String.Empty, String.Empty, True)
    PrintData.SetFilter(String.Empty, String.Empty, True)

    ' Width from header filter
    PrintData.Settings.FilterHeaderWidth(1) = 1600
    PrintData.Settings.FilterHeaderWidth(2) = 1000
    PrintData.Settings.FilterHeaderWidth(3) = 1400

  End Sub

  ' PURPOSE: Print Totals and subtotals
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - 
  Protected Overrides Sub GUI_AfterLastRow()
    ' Draw Sub Total
    Call DrawRow(False)

    ' Draw totals
    Call DrawRow(True)

  End Sub

  ' PURPOSE : Checks out the DB row before adding it to the grid
  '              and process counters (& totals rows)
  '
  '  PARAMS :
  '     - INPUT :
  '           - DataRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the data row can be added) or False (the data row can not be added)
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _chips_in As Decimal
    Dim _chips_out As Decimal
    Dim _total_buy_in As Decimal

    _chips_in = DbRow.Value(SQL_COLUMN_CHIPS_IN)
    _chips_out = DbRow.Value(SQL_COLUMN_CHIPS_OUT)
    _total_buy_in = DbRow.Value(SQL_COLUMN_TOTAL_BUY_IN)

    If String.IsNullOrEmpty(m_subtotal_name) Then
      m_subtotal_name = DbRow.Value(SQL_COLUMN_GAMING_TABLE_NAME)
      m_subtotal_type = DbRow.Value(SQL_COLUMN_GAMING_TABLE_TYPE)
    End If

    If Not String.Equals(m_subtotal_name, DbRow.Value(SQL_COLUMN_GAMING_TABLE_NAME)) Then
      DrawRow(False)
      m_subtotal_name = DbRow.Value(SQL_COLUMN_GAMING_TABLE_NAME)
      m_subtotal_type = DbRow.Value(SQL_COLUMN_GAMING_TABLE_TYPE)

      m_subtotal_chips_in = 0
      m_subtotal_chips_out = 0
      m_subtotal_total_buy_in = 0
    End If

    m_subtotal_chips_in += _chips_in
    m_subtotal_chips_out += _chips_out
    m_subtotal_total_buy_in += _total_buy_in

    ' Acum Total
    m_total_chips_in += _chips_in
    m_total_chips_out += _chips_out
    m_total_total_buy_in += _total_buy_in

    Return True
  End Function

  ' PURPOSE: Hide columns depending selected mode
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()
    m_subtotal_chips_in = 0
    m_subtotal_chips_out = 0
    m_subtotal_total_buy_in = 0

    m_subtotal_name = ""
    m_subtotal_type = ""

    m_total_chips_in = 0
    m_total_chips_out = 0
    m_total_total_buy_in = 0
  End Sub ' GUI_AfterLastRow

  ' PURPOSE : Set member values for filters
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  Protected Overrides Sub GUI_ReportUpdateFilters()
    Dim all_checked As Boolean

    m_date_from = ""
    m_date_to = ""

    ' Dates filter checks
    If Me.dtp_from.Checked Then
      m_date_from = GUI_FormatDate(dtp_from.Value, _
                    ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Me.dtp_to.Checked Then
      m_date_to = GUI_FormatDate(dtp_to.Value, _
                  ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Gaming table status
    If rb_status_all.Checked Then
      m_gaming_table_status = GAMING_TABLE_STATUS.GT_STATUS_ALL
    ElseIf rb_status_disabled.Checked Then
      m_gaming_table_status = GAMING_TABLE_STATUS.GT_STATUS_DISABLED
    ElseIf rb_status_enabled.Checked Then
      m_gaming_table_status = GAMING_TABLE_STATUS.GT_STATUS_ENABLED
    End If

    ' Include cashiers
    'm_show_tables_only = chb_show_tables_only.Checked

    ' Selected table types
    all_checked = True
    If uc_checked_list_table_type.SelectedIndexes.Length <> uc_checked_list_table_type.Count() Then
      all_checked = False
    End If

    If all_checked = True Then
      m_table_types_list = GLB_NLS_GUI_AUDITOR.GetString(263)
    Else
      m_table_types_list = uc_checked_list_table_type.SelectedValuesList()
    End If

    ' Location filters update
    m_area_name = String.Empty
    m_bank_name = String.Empty
    If Me.cmb_area.SelectedIndex > 0 Then
      m_area_name = Me.cmb_area.TextValue
      m_area_id = Me.cmb_area.Value
      m_bank_name = Me.cmb_bank.TextValue
      m_bank_id = Me.cmb_bank.Value
    Else
      m_area_id = String.Empty
      m_bank_id = String.Empty
    End If

  End Sub

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _chips_in As Decimal
    Dim _chips_out As Decimal
    Dim _total_buy_in As Decimal

    If chk_grouped_by_type.Checked Then

      Return False
    End If

    _chips_in = 0
    _chips_out = 0
    _total_buy_in = 0

    With Me.Grid

      .Cell(RowIndex, GRID_COLUMN_INDEX).Value = ""

      ' Session Id
      .Cell(RowIndex, GRID_COLUMN_SESSION_ID).Value = DbRow.Value(SQL_COLUMN_SESSION_ID)

      ' Table name
      .Cell(RowIndex, GRID_COLUMN_GAMING_TABLE_NAME).Value = DbRow.Value(SQL_COLUMN_GAMING_TABLE_NAME)

      'Date
      .Cell(RowIndex, GRID_COLUMN_GAMING_TABLE_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_GAMING_TABLE_DATE), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)


      ' Table type name
      .Cell(RowIndex, GRID_COLUMN_GAMING_TABLE_TYPE).Value = DbRow.Value(SQL_COLUMN_GAMING_TABLE_TYPE)
      'If .Cell(RowIndex, GRID_COLUMN_GAMING_TABLE_TYPE).Value = "---CASHIER---" Then
      '  .Cell(RowIndex, GRID_COLUMN_GAMING_TABLE_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(491) ' Cashier
      'End If

      ' CHIPS_IN 
      _chips_in = DbRow.Value(SQL_COLUMN_CHIPS_IN)
      .Cell(RowIndex, GRID_COLUMN_CHIPS_IN).Value = GUI_FormatNumber(_chips_in, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      ' CHIPS_OUT
      _chips_out = DbRow.Value(SQL_COLUMN_CHIPS_OUT)
      .Cell(RowIndex, GRID_COLUMN_CHIPS_OUT).Value = GUI_FormatNumber(_chips_out, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      ' BUY_IN 
      _total_buy_in = DbRow.Value(SQL_COLUMN_TOTAL_BUY_IN)
      .Cell(RowIndex, GRID_COLUMN_TOTAL_BUY_IN).Value = GUI_FormatNumber(_total_buy_in, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    End With

    Return True
  End Function

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    With Me.Grid

      .IsSortable = False
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = WIDTH_GRID_COLUMN_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Session Id
      .Column(GRID_COLUMN_SESSION_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_SESSION_ID).Width = WIDTH_GRID_COLUMN_SESSION_ID
      .Column(GRID_COLUMN_SESSION_ID).HighLightWhenSelected = False
      .Column(GRID_COLUMN_SESSION_ID).IsColumnPrintable = False

      ' Table Name
      .Column(GRID_COLUMN_GAMING_TABLE_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3419) ' Name
      .Column(GRID_COLUMN_GAMING_TABLE_NAME).Width = WIDTH_GRID_COLUMN_GAMING_TABLE_NAME

      ' Date
      .Column(GRID_COLUMN_GAMING_TABLE_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4452) ' Opening date
      .Column(GRID_COLUMN_GAMING_TABLE_DATE).Width = WIDTH_GRID_COLUMN_GAMING_TABLE_DATE

      ' Table Type
      .Column(GRID_COLUMN_GAMING_TABLE_TYPE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4544) ' Type
      .Column(GRID_COLUMN_GAMING_TABLE_TYPE).Width = WIDTH_GRID_COLUMN_GAMING_TABLE_TYPE

      ' Chips in 
      .Column(GRID_COLUMN_CHIPS_IN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5287) ' Chips IN
      .Column(GRID_COLUMN_CHIPS_IN).Width = WIDTH_GRID_COLUMN_CHIPS_IN

      ' Chips Out
      .Column(GRID_COLUMN_CHIPS_OUT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5288) ' Chips OUT
      .Column(GRID_COLUMN_CHIPS_OUT).Width = WIDTH_GRID_COLUMN_CHIPS_OUT

      ' Buy in 
      .Column(GRID_COLUMN_TOTAL_BUY_IN).Header(0).Text = WSI.Common.GeneralParam.GetString("GamingTable.PlayerTracking", "PlayerTracking.SellChips.Text", GLB_NLS_GUI_PLAYER_TRACKING.GetString(5492)) ' Buy-IN
      .Column(GRID_COLUMN_TOTAL_BUY_IN).Width = WIDTH_GRID_COLUMN_TOTAL_BUY_IN
    End With
  End Sub

  ' PURPOSE: Executes the query with a command
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '     - 
  Protected Overrides Sub GUI_ExecuteQueryCustom()
    Dim _sql_cmd As SqlCommand
    Dim _table As DataTable
    Dim _row As DataRow

    Call GUI_StyleSheet()

    _sql_cmd = New SqlCommand("GT_Chips_Operations_Player_Tracking")
    _sql_cmd.CommandType = CommandType.StoredProcedure

    _sql_cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = IIf(Not Me.dtp_from.Checked, System.DBNull.Value, m_date_from)
    _sql_cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = IIf(Not Me.dtp_to.Checked, System.DBNull.Value, m_date_to)
    _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = m_gaming_table_status
    _sql_cmd.Parameters.Add("@pArea", SqlDbType.VarChar).Value = m_area_id
    _sql_cmd.Parameters.Add("@pBank", SqlDbType.VarChar).Value = m_bank_id
    _sql_cmd.Parameters.Add("@pValidTypes", SqlDbType.VarChar).Value = uc_checked_list_table_type.SelectedIndexesList()

    _table = GUI_GetTableUsingCommand(_sql_cmd, Integer.MaxValue)

    Call GUI_BeforeFirstRow()

    Dim db_row As CLASS_DB_ROW
    Dim count As Integer
    Dim idx_row As Integer

    ' Fill the grid only if there is data
    If _table.Rows.Count > 0 Then

      For Each _row In _table.Rows

        Try
          db_row = New CLASS_DB_ROW(_row)

          If GUI_CheckOutRowBeforeAdd(db_row) Then
            ' Add the db row to the grid
            Me.Grid.AddRow()
            count = count + 1
            idx_row = Me.Grid.NumRows - 1

            If Not GUI_SetupRow(idx_row, db_row) Then
              ' The row can not be added
              Me.Grid.DeleteRowFast(idx_row)
              count = count - 1
            End If
          End If

        Catch ex As OutOfMemoryException
          Throw ex

        Catch exception As Exception
          Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
          Call Trace.WriteLine(exception.ToString())
          Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                                "frm_chips_operations_report", _
                                "GUI_SetupRow", _
                                exception.Message)
          Exit For
        End Try

        db_row = Nothing

        If Not GUI_DoEvents(Me.Grid) Then
          Exit Sub
        End If

      Next

      Call GUI_AfterLastRow()
    End If

  End Sub

  ' PURPOSE: Set the query method to use
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '     - ENUM_QUERY_TYPE
  Protected Overrides Function GUI_GetQueryType() As GUI_Controls.frm_base_sel.ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM
  End Function

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    MyBase.GUI_InitControls()

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Text = GLB_NLS_GUI_INVOICING.GetString(2)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Enabled = False

    ' Set form Name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5393)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_INFO).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    ' Date
    Me.gb_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4452) 'Opening date
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' TODO: Set NLS for all controls

    ' -- Location --
    '   · Area
    Me.gb_area_island.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1166)                                ' 1166 "Ubicación"
    Me.cmb_area.Text = GLB_NLS_GUI_CONFIGURATION.GetString(435)                                         ' 435  "Area"
    '   · Zone
    Me.ef_smoking.Text = GLB_NLS_GUI_CONFIGURATION.GetString(430)                                       ' 430  "Zona"
    Me.ef_smoking.IsReadOnly = True
    Me.ef_smoking.TabStop = False
    '   · Bank
    Me.cmb_bank.Text = GLB_NLS_GUI_CONFIGURATION.GetString(431)                                         ' 431  "Isla"

    ' Estado
    Me.gb_tables_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(262)
    Me.rb_status_all.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1807)
    Me.rb_status_disabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(247)
    Me.rb_status_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(246)

    ' Agrupado
    Me.chk_grouped_by_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4581)

    ' Fill operators
    Me.uc_checked_list_table_type.GroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3409)
    Call Me.uc_checked_list_table_type.ColumnWidth(uc_checked_list.GRID_COLUMN_DESC, 4250)
    Call FillTableTypeFilterGrid()

    ' Fill combos
    Call AreaComboFill()
    Call BankComboFill()

    ' Add Handles
    Call AddHandles()

    Call SetDefaultValues()

    Call GUI_StyleSheet()
  End Sub


  ' PURPOSE: Call to child window to show details for the selected row
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ShowSelectedItem()
    Dim _frm As frm_table_player_movements
    Dim _session_id As Int64
    Dim _session_name As String

    If Me.Grid.SelectedRows().Length = 1 Then

      If Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_SESSION_ID).Value = "" Then
        Return
      Else
        _session_id = Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_SESSION_ID).Value
        _session_name = Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_GAMING_TABLE_NAME).Value
      End If

      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      ' Create instance
      _frm = New frm_table_player_movements()
      _frm.ShowMovementsChips(_session_id, _session_name, Me.MdiParent)

      Windows.Forms.Cursor.Current = Cursors.Default

    End If

  End Sub ' GUI_ShowSelectedItem

  ' PURPOSE: Process button actions in order to branch to a child screen
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)
    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        Call GUI_ShowSelectedItem()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

    If ButtonId <> ENUM_BUTTON.BUTTON_CANCEL Then
      GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Enabled = CBool(Me.Grid.NumRows > 0) And Not Me.chk_grouped_by_type.Checked
    End If

  End Sub ' GUI_ButtonClick


  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()

  End Sub ' GUI_FilterReset

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_PLAYER_TRACKING_CHIPS_REPORT

    Call MyBase.GUI_SetFormId()
  End Sub

  Protected Overrides Sub Finalize()
    MyBase.Finalize()
  End Sub ' Finalize

#End Region

#Region " Private "

  ' PURPOSE: Show results to screen drawing the total counter data in a new row with the specified color
  '
  '  PARAMS:
  '     - INPUT:
  '          - LastTotal: Indicates the kind of total
  '     - OUTPUT:
  '
  ' RETURNS:
  '   - True
  Private Function DrawRow(ByVal IsTotal As Boolean) As Boolean
    Dim _idx_row As Integer

    ' Add the totals row & data
    With Me.Grid
      .AddRow()
      _idx_row = Me.Grid.NumRows - 1

      .Cell(_idx_row, GRID_COLUMN_INDEX).Value = String.Empty

      .Cell(_idx_row, GRID_COLUMN_SESSION_ID).Value = String.Empty

      .Cell(_idx_row, GRID_COLUMN_GAMING_TABLE_TYPE).Value = String.Empty

      If IsTotal Then
        .Cell(_idx_row, GRID_COLUMN_GAMING_TABLE_NAME).Value = GLB_NLS_GUI_INVOICING.GetString(205)

        .Cell(_idx_row, GRID_COLUMN_CHIPS_IN).Value = GUI_FormatCurrency(m_total_chips_in, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        .Cell(_idx_row, GRID_COLUMN_CHIPS_OUT).Value = GUI_FormatCurrency(m_total_chips_out, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        .Cell(_idx_row, GRID_COLUMN_TOTAL_BUY_IN).Value = GUI_FormatCurrency(m_total_total_buy_in, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)


        .Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
      Else

        .Cell(_idx_row, GRID_COLUMN_GAMING_TABLE_NAME).Value = GLB_NLS_GUI_INVOICING.GetString(295) + " " + m_subtotal_name + ":"

        If chk_grouped_by_type.Checked Then
          .Cell(_idx_row, GRID_COLUMN_GAMING_TABLE_TYPE).Value = m_subtotal_type

          .Cell(_idx_row, GRID_COLUMN_GAMING_TABLE_NAME).Value = m_subtotal_name
        End If

        .Cell(_idx_row, GRID_COLUMN_CHIPS_IN).Value = GUI_FormatCurrency(m_subtotal_chips_in, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        .Cell(_idx_row, GRID_COLUMN_CHIPS_OUT).Value = GUI_FormatCurrency(m_subtotal_chips_out, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        .Cell(_idx_row, GRID_COLUMN_TOTAL_BUY_IN).Value = GUI_FormatCurrency(m_subtotal_total_buy_in, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        .Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
      End If
    End With

    Return True
  End Function ' DrawRow


  Private Sub SetDefaultValues()
    Dim _current_date As DateTime
    Dim _closing_time As Integer
    Dim _initial_time As Date

    _current_date = WSI.Common.WGDB.Now

    _closing_time = GetDefaultClosingTime()
    _initial_time = New DateTime(_current_date.Year, _current_date.Month, _current_date.Day, _closing_time, 0, 0)

    Me.dtp_from.Value = _initial_time
    Me.dtp_from.Checked = True

    Me.dtp_to.Value = _initial_time.AddDays(1)
    Me.dtp_to.Checked = False

    Call Me.uc_checked_list_table_type.SetDefaultValue(True)

    ' Location
    cmb_area.SelectedIndex = 0
    cmb_bank.SelectedIndex = 0
    ef_smoking.Value = String.Empty

    Me.rb_status_all.Checked = True

  End Sub

  ' PURPOSE: Fill the filter Grid with data
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub FillTableTypeFilterGrid()
    Dim _data_table_tables_type As DataTable

    Call Me.uc_checked_list_table_type.Clear()
    Call Me.uc_checked_list_table_type.ReDraw(False)

    _data_table_tables_type = GamingTablesType_DBRead()
    Me.uc_checked_list_table_type.Add(_data_table_tables_type, "ID", "NAME")

    If Me.uc_checked_list_table_type.Count() > 0 Then
      Call Me.uc_checked_list_table_type.CurrentRow(0)
    End If

    Call Me.uc_checked_list_table_type.ReDraw(True)

  End Sub 'FillTableTypeFilterGrid

  ' PURPOSE: Read the data from gaming tables types
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - DataTable: collected data
  Private Function GamingTablesType_DBRead() As DataTable
    Dim _str_sql As String
    Dim _data_table_tables_type As DataTable

    _str_sql = "SELECT   gtt_gaming_table_type_id AS ID" & _
              "        , GTT_NAME  AS NAME" & _
              "   FROM   GAMING_TABLES_TYPES " & _
              "  WHERE   GTT_ENABLED =  " & 1 & ""

    _data_table_tables_type = GUI_GetTableUsingSQL(_str_sql, 5000)

    Return _data_table_tables_type
  End Function

  ' PURPOSE: Area selection
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub cmb_area_ValueChangedEvent()

    'Zone (smoking - no smoking)
    ef_smoking.Value = GetZoneDescription(cmb_area.Value)

    'Banks
    Call BankComboFill()

  End Sub

  ' PURPOSE: Returns zone description
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  '
  Private Function GetZoneDescription(ByVal AreaId As Integer) As String
    Dim _table As DataTable
    Dim _returnValue As String

    _table = GUI_GetTableUsingSQL("SELECT AR_SMOKING FROM AREAS WHERE AR_AREA_ID = " & AreaId.ToString(), Integer.MaxValue)

    _returnValue = GLB_NLS_GUI_CONFIGURATION.GetString(437)

    If Not IsNothing(_table) Then
      If _table.Rows.Count() > 0 Then
        If _table.Rows.Item(0).Item("AR_SMOKING") Then
          _returnValue = GLB_NLS_GUI_CONFIGURATION.GetString(436)
        End If
      End If
    End If

    Return _returnValue

  End Function

  ' PURPOSE: Init areas combo box
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub AreaComboFill()
    Dim _table As DataTable

    _table = GUI_GetTableUsingSQL("SELECT AR_AREA_ID, AR_NAME FROM AREAS INNER JOIN BANKS ON BK_AREA_ID = AR_AREA_ID GROUP BY AR_AREA_ID, AR_NAME ORDER BY AR_NAME", Integer.MaxValue)

    Me.cmb_area.Clear()
    Me.cmb_area.Add(_table)

  End Sub     ' AreaComboFill

  ' PURPOSE: Init BANKS combo box
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub BankComboFill()
    Dim _table As DataTable

    _table = GUI_GetTableUsingSQL("SELECT BK_BANK_ID, BK_NAME FROM BANKS WHERE BK_AREA_ID = " & cmb_area.Value & " GROUP BY BK_BANK_ID, BK_NAME ORDER BY BK_NAME", Integer.MaxValue)

    Me.cmb_bank.Clear()
    Me.cmb_bank.Add(_table)

  End Sub     ' BankComboFill

  ' PURPOSE: Add Handles
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub AddHandles()

    AddHandler cmb_area.ValueChangedEvent, AddressOf cmb_area_ValueChangedEvent

  End Sub ' AddHandles

#End Region

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

End Class