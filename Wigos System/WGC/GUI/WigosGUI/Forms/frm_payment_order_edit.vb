'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_payment_order_edit
' DESCRIPTION:   Payment orders edition form
' AUTHOR:        Luis Ernesto Mesa  
' CREATION DATE: 17-JAN-2013
' 
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 17-JAN-2013  LEM    Initial version
' 30-JAN-2013  LEM    Fixed bug #556: Button Edit/Cancel appear disabled with write permissions
' 14-FEB-2013  JCM    Fixed bug: Verify changes although user no enter Edit Mode on Form
'                     Fixed bug: No release temp files and memory when user exit from form
' 14-FEB-2013  JCM    Moved DeleTempFiles to Misc, and Fixed Bug: infinite loop when app from third lock files
' 15-FEB-2013  JCM    Tab Stop Disabled from optional fields when are not required
' 18-FEB-2013  JCM    Added function DiscardChanges used on Cancel and Exit
' 24-APR-2013  DMR    Fixed Bug #745: Don't allow payment order edit if disabled
' 11-JUN-2013  NMR    Preventing the focus losing when showing details
' 04-DEC-2013  LJM	  Changes to use the standard behaviour on ButtonClick events
' 13-MAR-2015  FOS    TASK 624: GUI: Modify Form
' 18-MAY-2015  FOS    Fixed Bug #2363: If a previous PDF document , you have to show if the option is disabled generate pdf
' 28-NOV-2016  ETP    Fixed Bug 21063: If you print pdf document without openning the preview shows and error.
'--------------------------------------------------------------------

#Region " Imports "

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonMisc
Imports System.IO
Imports System.Drawing.Printing
Imports WSI.Common

#End Region

Public Class frm_payment_order_edit
  Inherits frm_base_edit

#Region " Members "

  Dim m_order As CLASS_PAYMENT_ORDER
  Dim m_screen_data_ok As Boolean
  Dim m_old_temp_folders() As String
  Dim m_edition_mode As Boolean
  Dim m_wb_order As WebBrowser
  Dim m_bank_accounts As DataTable
  Dim m_bank As CLASS_BANK_ACCOUNT
  Dim m_gp_auth1_required As Boolean
  Dim m_gp_auth1_title As String
  Dim m_gp_auth1_name As String
  Dim m_gp_auth2_required As Boolean
  Dim m_gp_auth2_title As String
  Dim m_gp_auth2_name As String

#End Region   ' Members

#Region " Constants "

  Const BUTTON_PRINT = ENUM_BUTTON.BUTTON_CUSTOM_0
  Const BUTTON_EDIT_CANCEL = ENUM_BUTTON.BUTTON_DELETE

#End Region ' Constants

#Region " Overrides "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_PAYMENT_ORDER_EDIT
    Call MyBase.GUI_SetFormId()

  End Sub              ' GUI_SetFormId

  ' PURPOSE : Form controls initialization.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS  :
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1580)
    Me.CloseOnOkClick = False

    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)  'EXIT
    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Text = GLB_NLS_GUI_CONTROLS.GetString(31)      'APPLY
    Me.GUI_Button(BUTTON_PRINT).Text = GLB_NLS_GUI_CONTROLS.GetString(5)                'PRINT
    Me.GUI_Button(BUTTON_EDIT_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(25)         'EDIT 
    Me.GUI_Button(BUTTON_PRINT).Visible = True

    Call InitControlsText()
    Call InitControlsFormat()
    Call LoadGeneralParams()
    Call LoadComboAccounts()
    Call SetControlsEdit(False)
    Call VisibleData()

    If m_order.DocumentId <= 0 Then
      Call HideControls()
    End If

  End Sub        ' GUI_InitControls

  ' PURPOSE : Database overridable operations. 
  '           Define specific DB operation for this form.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_PAYMENT_ORDER()
        m_order = DbEditedObject

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ

        If DbStatus = CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND Then
          NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(119), ENUM_MB_TYPE.MB_TYPE_ERROR)
        Else
          Call RefreshTabPDF(False)
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub        ' GUI_DB_Operation

  ' PURPOSE : Validate the data presented on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '         TRUE: if everything goes ok
  '         FALSE: if fails
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Dim _controls_to_check As RequiredData
    Dim _data_empty As ENUM_CARDDATA_FIELD
    Dim _label_control As String
    Dim _focus_control As Control

    'check names
    _controls_to_check = New RequiredData(PLAYER_DATA_TO_CHECK.ACCOUNT)
    _label_control = ""
    _focus_control = Nothing

    _controls_to_check.Add(ENUM_CARDDATA_FIELD.NAME3, ef_player_name3.TextValue)
    _controls_to_check.Add(ENUM_CARDDATA_FIELD.NAME4, ef_player_name4.TextValue)
    _controls_to_check.Add(ENUM_CARDDATA_FIELD.NAME1, ef_player_name1.TextValue)
    _controls_to_check.Add(ENUM_CARDDATA_FIELD.NAME2, ef_player_name2.TextValue)

    If _controls_to_check.IsAnyEmpty(False, _data_empty) Then

      Select Case _data_empty
        Case ENUM_CARDDATA_FIELD.NAME1
          _focus_control = ef_player_name1
        Case ENUM_CARDDATA_FIELD.NAME2
          _focus_control = ef_player_name2
        Case ENUM_CARDDATA_FIELD.NAME3
          _focus_control = ef_player_name3
        Case ENUM_CARDDATA_FIELD.NAME4
          _focus_control = ef_player_name4
      End Select

      If Not _focus_control Is Nothing Then
        Call _focus_control.Focus()
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_ERROR, , , _focus_control.Text)
      End If

      m_screen_data_ok = False

      Return False

    End If

    ' check documents
    If String.IsNullOrEmpty(cb_player_doc_type1.TextValue) Then
      Call cb_player_doc_type1.Focus()
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_ERROR, , , cb_player_doc_type1.Text)
      m_screen_data_ok = False
      Return False
    End If

    If cb_player_doc_type2.Visible And String.IsNullOrEmpty(cb_player_doc_type2.TextValue) Then
      Call cb_player_doc_type2.Focus()
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_ERROR, , , cb_player_doc_type2.Text)
      m_screen_data_ok = False
      Return False
    End If

    If String.IsNullOrEmpty(ef_player_doc_id1.TextValue) Then
      Call ef_player_doc_id1.Focus()
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_ERROR, , , ef_player_doc_id1.Text)
      m_screen_data_ok = False
      Return False
    End If

    If cb_player_doc_type2.Visible And String.IsNullOrEmpty(ef_player_doc_id2.TextValue) Then
      Call ef_player_doc_id2.Focus()
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_ERROR, , , ef_player_doc_id2.Text)
      m_screen_data_ok = False
      Return False
    End If

    If Not ef_title1.IsReadOnly And String.IsNullOrEmpty(ef_title1.TextValue) Then
      Call ef_title1.Focus()
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_ERROR, , , ef_title1.Text)
      m_screen_data_ok = False
      Return False
    End If

    If Not ef_name1.IsReadOnly And String.IsNullOrEmpty(ef_name1.TextValue) Then
      Call ef_name1.Focus()
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_ERROR, , , ef_name1.Text)
      m_screen_data_ok = False
      Return False
    End If

    If Not ef_title2.IsReadOnly And String.IsNullOrEmpty(ef_title2.TextValue) Then
      Call ef_title2.Focus()
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_ERROR, , , ef_title2.Text)
      m_screen_data_ok = False
      Return False
    End If

    If Not ef_name2.IsReadOnly And String.IsNullOrEmpty(ef_name2.TextValue) Then
      Call ef_name2.Focus()
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_ERROR, , , ef_name2.Text)
      m_screen_data_ok = False
      Return False
    End If

    If Me.cb_player_doc_type1.TextValue = Me.cb_player_doc_type2.TextValue Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1585), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
      m_screen_data_ok = False
      Return False
    End If

    m_screen_data_ok = ValidateFormat()

    Return m_screen_data_ok

  End Function ' GUI_IsScreenDataOk


  'PURPOSE: Executed just before closing form
  '         
  ' PARAMS:
  '    - INPUT :
  '
  '    - OUTPUT :
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)

    If m_edition_mode Then
      If DiscardChanges(ENUM_BUTTON.BUTTON_CANCEL) Then
        CloseCanceled = False
      Else
        CloseCanceled = True
      End If
    End If

    If Not CloseCanceled Then
      While tab_order_viewer.TabCount > 0
        tab_order_viewer.TabPages.RemoveAt(0)
      End While
      Call Me.m_wb_order.Dispose()
      Call DeleteTempFiles(m_order.TempFolder)
    End If

  End Sub

  'PURPOSE: Executed when User Click Cancel or Exit
  '         Disacrd Changes If User agree undo Changes
  '
  ' PARAMS:
  '    - INPUT :  Button Id wich call the function, usefull for NLS
  '
  '    - OUTPUT :
  '       TRUE:   User  agreee  discard changes  or  No Changes
  '      FALSE:   User disagree discard changes
  ' RETURNS:
  '
  Protected Function DiscardChanges(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON) As Boolean
    Dim _nls_id As Integer

    ' Update EditedObject with Screen Data
    Call GUI_GetScreenData()

    'If no changes, exit
    If DbReadObject.AuditorData.IsEqual(DbEditedObject.AuditorData) Then

      Return True
    End If

    Select Case ButtonId
      Case BUTTON_EDIT_CANCEL    'EDIT OR CANCEL
        _nls_id = GLB_NLS_GUI_CONTROLS.Id(156)
      Case ENUM_BUTTON.BUTTON_CANCEL
        ' Exit 
        _nls_id = GLB_NLS_GUI_CONTROLS.Id(101)
      Case Else
        _nls_id = GLB_NLS_GUI_CONTROLS.Id(101)
    End Select

    ' Request User Agree Discard Changes
    If NLS_MsgBox(_nls_id, _
                  ENUM_MB_TYPE.MB_TYPE_WARNING, _
                  ENUM_MB_BTN.MB_BTN_YES_NO, _
                  ENUM_MB_DEF_BTN.MB_DEF_BTN_2) _
                  = ENUM_MB_RESULT.MB_RESULT_YES Then

      ' Undo Changes
      DbEditedObject = DbReadObject.Duplicate()
      m_order = DbEditedObject

      ' Useful when exit if Authorization Changed in GP 
      '  and necessary when Cancel
      Call GUI_SetScreenData(0)

      Return True
    End If

    Return False
  End Function ' DiscardChanges

  ' PURPOSE: Process clicks on data grid (double-clicks) and select button
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)

    Select Case ButtonId

      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        Call MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_PRINT)

      Case ENUM_BUTTON.BUTTON_OK
        If m_edition_mode And CheckAllowChanges() Then
          m_screen_data_ok = False
          Call MyBase.GUI_ButtonClick(ButtonId)
          If m_screen_data_ok Then
            Call m_order.UpdateDocument()
            Call SetControlsEdit(False)
            Call RefreshTabPDF(True)
          End If
        End If

      Case BUTTON_EDIT_CANCEL
        If m_edition_mode Then
          ' CANCEL
          If DiscardChanges(BUTTON_EDIT_CANCEL) Then
            Call SetControlsEdit(False)
            Call RefreshTabPDF(True)
          End If

        Else
          ' EDIT 
          If CheckAllowChanges() Then
            While tab_order_viewer.TabCount > 1
              Call tab_order_viewer.TabPages.RemoveAt(1)
            End While
            Call SetAuthorizationData()
            Call SetControlsEdit(True)
          End If
        End If

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)

    End Select

  End Sub         ' GUI_ButtonClick

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    Dim _doc_type As Int32
    Dim _str_doc_type As String

    Me.ef_order_date.TextValue = GUI_FormatDate(m_order.OrderDate, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.ef_player_name1.TextValue = m_order.PlayerName1
    Me.ef_player_name2.TextValue = m_order.PlayerName2
    Me.ef_player_name3.TextValue = m_order.PlayerName3
    Me.ef_player_name4.TextValue = m_order.PlayerName4

    If Int32.TryParse(m_order.PlayerDocType1, _doc_type) Then
      _str_doc_type = IdentificationTypes.DocIdTypeString(_doc_type)
    Else
      _str_doc_type = m_order.PlayerDocType1
    End If
    SetComboText(cb_player_doc_type1, _str_doc_type)

    Me.ef_player_doc_type1.TextValue = _str_doc_type
    If Int32.TryParse(m_order.PlayerDocType2, _doc_type) Then
      _str_doc_type = IdentificationTypes.DocIdTypeString(_doc_type)
    Else
      _str_doc_type = m_order.PlayerDocType2
    End If
    SetComboText(cb_player_doc_type2, _str_doc_type)

    Me.ef_player_doc_type2.TextValue = _str_doc_type
    Me.ef_player_doc_id1.TextValue = m_order.PlayerDocId1
    Me.ef_player_doc_id2.TextValue = m_order.PlayerDocId2
    Me.ef_title1.TextValue = m_order.AuthorizationTitle1
    Me.ef_title2.TextValue = m_order.AuthorizationTitle2
    Me.ef_name1.TextValue = m_order.AuthorizationName1
    Me.ef_name2.TextValue = m_order.AuthorizationName2

    Call LoadComboAccounts()

    Me.ef_bank_name.TextValue = m_order.BankName
    Me.ef_payment_type.TextValue = m_order.PaymentType
    Me.ef_efective_days.TextValue = m_order.EffectiveDays
    Me.dtp_application_date.Value = m_order.ApplicationDate
    Me.ef_customer_number.TextValue = m_order.BankCustomerNumber
    Me.ef_account_number.TextValue = m_order.BankAccountNumber
    Me.ef_return_balance.TextValue = GUI_FormatCurrency(m_order.ReturnBalance, 2)
    Me.ef_prize.TextValue = GUI_FormatCurrency(m_order.Prize, 2)
    Me.ef_witholding_tax1.TextValue = GUI_FormatCurrency(m_order.WitholdingTax1, 2)
    Me.ef_witholding_tax2.TextValue = GUI_FormatCurrency(m_order.WitholdingTax2, 2)
    Me.ef_witholding_tax3.TextValue = GUI_FormatCurrency(m_order.WitholdingTax3, 2)
    Me.ef_full_payment.TextValue = GUI_FormatCurrency(m_order.FullPayment, 2)
    Me.ef_cash_payment.TextValue = GUI_FormatCurrency(m_order.CashPayment, 2)
    Me.ef_check_payment.TextValue = GUI_FormatCurrency(m_order.CheckPayment, 2)

  End Sub       ' GUI_SetScreenData

  ' PURPOSE : Get data from the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_GetScreenData()
    m_order = DbEditedObject

    If m_edition_mode Then
      Me.ef_player_doc_type1.TextValue = Me.cb_player_doc_type1.TextValue
      Me.ef_player_doc_type2.TextValue = Me.cb_player_doc_type2.TextValue
    End If

    m_order.PlayerName1 = Me.ef_player_name1.TextValue
    m_order.PlayerName2 = Me.ef_player_name2.TextValue
    m_order.PlayerName3 = Me.ef_player_name3.TextValue
    m_order.PlayerName4 = Me.ef_player_name4.TextValue

    If Me.cb_player_doc_type1.SelectedIndex >= 0 Then
      m_order.PlayerDocType1 = Me.cb_player_doc_type1.Value(Me.cb_player_doc_type1.SelectedIndex).ToString()
    End If

    If Me.cb_player_doc_type2.SelectedIndex >= 0 Then
      m_order.PlayerDocType2 = Me.cb_player_doc_type2.Value(Me.cb_player_doc_type2.SelectedIndex).ToString()
    End If

    m_order.PlayerDocId1 = Me.ef_player_doc_id1.TextValue
    m_order.PlayerDocId2 = Me.ef_player_doc_id2.TextValue

    m_order.AuthorizationTitle1 = Me.ef_title1.TextValue
    m_order.AuthorizationTitle2 = Me.ef_title2.TextValue
    m_order.AuthorizationName1 = Me.ef_name1.TextValue
    m_order.AuthorizationName2 = Me.ef_name2.TextValue

    If cb_bank_sel.SelectedIndex >= 0 Then
      m_order.BankAccountId = cb_bank_sel.Value(cb_bank_sel.SelectedIndex)
    End If

    m_order.BankName = Me.ef_bank_name.TextValue
    m_order.PaymentType = Me.ef_payment_type.TextValue
    m_order.EffectiveDays = Me.ef_efective_days.TextValue
    m_order.ApplicationDate = Me.dtp_application_date.Value
    m_order.BankCustomerNumber = Me.ef_customer_number.TextValue
    m_order.BankAccountNumber = Me.ef_account_number.TextValue

  End Sub       ' GUI_GetScreenData

#End Region ' Overrides

#Region " Privates "

  ' PURPOSE: Sets the controls when user are editing or are reading the form.
  '         
  ' PARAMS:
  '    - INPUT:   ShowEnable = True    -> Set User Controls When Users Pressed Edit  Button
  '               ShowEnable = False   -> Set User Controls When Users Pressed Apply/Cancel Button
  '    - OUTPUT:
  '
  ' RETURNS :
  Private Sub SetControlsEdit(ByVal ShowEnable As Boolean)
    Dim _row_ref As Int32

    Me.SuspendLayout()

    Me.tab_order_viewer.SelectTab(0)

    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = ShowEnable
    Me.GUI_Button(BUTTON_PRINT).Enabled = Not ShowEnable
    Me.GUI_Button(BUTTON_EDIT_CANCEL).Enabled = Me.Permissions.Write

    m_edition_mode = ShowEnable
    ef_order_date.IsReadOnly = True
    ef_player_name1.IsReadOnly = Not ShowEnable
    ef_player_name2.IsReadOnly = Not ShowEnable
    ef_player_name3.IsReadOnly = Not ShowEnable
    ef_player_name4.IsReadOnly = Not ShowEnable
    cb_bank_sel.Visible = ShowEnable
    dtp_application_date.IsReadOnly = Not ShowEnable
    ef_player_doc_id1.IsReadOnly = Not ShowEnable
    cb_player_doc_type1.IsReadOnly = Not ShowEnable
    cb_player_doc_type2.IsReadOnly = cb_player_doc_type2.Count() <= 0 Or Not ShowEnable

    If cb_player_doc_type2.Count() <= 0 Then
      ef_player_doc_id2.TextValue = ""
      ef_player_doc_id2.IsReadOnly = True
      ef_player_doc_id2.TabStop = False
      cb_player_doc_type2.TabStop = False
    Else
      ef_player_doc_id2.IsReadOnly = Not ShowEnable
      ef_player_doc_id2.TabStop = True
      cb_player_doc_type2.TabStop = True
    End If

    ef_bank_name.IsReadOnly = True
    ef_payment_type.IsReadOnly = True
    ef_efective_days.IsReadOnly = True
    ef_customer_number.IsReadOnly = True
    ef_account_number.IsReadOnly = True
    ef_return_balance.IsReadOnly = True
    ef_prize.IsReadOnly = True
    ef_witholding_tax1.IsReadOnly = True
    ef_witholding_tax2.IsReadOnly = True
    ef_witholding_tax3.IsReadOnly = True
    ef_full_payment.IsReadOnly = True
    ef_cash_payment.IsReadOnly = True
    ef_check_payment.IsReadOnly = True
    ef_player_doc_type1.IsReadOnly = True
    ef_player_doc_type2.IsReadOnly = True

    If Not m_gp_auth1_required Or Not ShowEnable Then
      ef_title1.IsReadOnly = True
      ef_name1.IsReadOnly = True
      ef_title1.TabStop = False
      ef_name1.TabStop = False
    Else
      ef_title1.IsReadOnly = False
      ef_name1.IsReadOnly = False
      ef_title1.TabStop = True
      ef_name1.TabStop = True
    End If

    If Not m_gp_auth2_required Or Not ShowEnable Then
      ef_title2.IsReadOnly = True
      ef_name2.IsReadOnly = True
      ef_title2.TabStop = False
      ef_name2.TabStop = False
    Else
      ef_title2.IsReadOnly = False
      ef_name2.IsReadOnly = False
      ef_title2.TabStop = True
      ef_name2.TabStop = True
    End If

    ef_player_doc_type1.Visible = False
    ef_player_doc_type2.Visible = False
    cb_player_doc_type1.Visible = False
    cb_player_doc_type2.Visible = False

    _row_ref = TableLayoutPanel1.GetCellPosition(ef_player_name2).Row

    If ShowEnable Then
      TableLayoutPanel1.SetCellPosition(ef_player_doc_type1, New TableLayoutPanelCellPosition(1, 0))
      TableLayoutPanel1.SetCellPosition(ef_player_doc_type2, New TableLayoutPanelCellPosition(1, 1))
      TableLayoutPanel1.SetCellPosition(cb_player_doc_type1, New TableLayoutPanelCellPosition(0, _row_ref + 1))
      TableLayoutPanel1.SetCellPosition(cb_player_doc_type2, New TableLayoutPanelCellPosition(0, _row_ref + 2))
    Else
      TableLayoutPanel1.SetCellPosition(cb_player_doc_type1, New TableLayoutPanelCellPosition(1, 0))
      TableLayoutPanel1.SetCellPosition(cb_player_doc_type2, New TableLayoutPanelCellPosition(1, 1))
      TableLayoutPanel1.SetCellPosition(ef_player_doc_type1, New TableLayoutPanelCellPosition(0, _row_ref + 1))
      TableLayoutPanel1.SetCellPosition(ef_player_doc_type2, New TableLayoutPanelCellPosition(0, _row_ref + 2))
    End If

    ef_player_doc_type1.Visible = Not ShowEnable
    ef_player_doc_type2.Visible = Not ShowEnable And ef_player_doc_id2.Visible
    cb_player_doc_type1.Visible = ShowEnable
    cb_player_doc_type2.Visible = ShowEnable And ef_player_doc_id2.Visible

    If ShowEnable Then
      Me.GUI_Button(BUTTON_EDIT_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(2)
      Call GUI_Button(ENUM_BUTTON.BUTTON_OK).Focus()
    Else
      Me.GUI_Button(BUTTON_EDIT_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(25)
      Call GUI_Button(BUTTON_EDIT_CANCEL).Focus()
    End If

    Me.ResumeLayout()

  End Sub           ' SetControlsEdit

  Private Sub VisibleData()

    Dim _visible_data As VisibleData

    _visible_data = New VisibleData()

    _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.NAME3, New Control() {ef_player_name3})
    _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.NAME4, New Control() {ef_player_name4})
    _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.NAME1, New Control() {ef_player_name1})
    _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.NAME2, New Control() {ef_player_name2})

    _visible_data.HideControls()

    If ef_player_name1.Visible And Not ef_player_name2.Visible Then
      ef_player_name1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5099)
    End If

  End Sub

  ' PURPOSE: Hide form controls when GP GenerateDocument is False
  '          
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  '     
  Private Sub HideControls()
    gb_player_info.Visible = False
    gb_bank_info.Visible = False
    ef_title1.Visible = False
    ef_name1.Visible = False
    ef_title2.Visible = False
    ef_name2.Visible = False

    Me.GUI_Button(BUTTON_PRINT).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = False

    Call RenderForm()
  End Sub 'HideControls

  ' PURPOSE: Render Form when GP Generate Document is False
  '          
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  '     
  Private Sub RenderForm()
    gb_room_info.Size = New Size(150, gb_room_info.Size.Height + 24)
    ef_order_date.Location = New Point(ef_return_balance.Location.X, ef_return_balance.Location.Y)
    ef_return_balance.Location = New Point(ef_prize.Location.X, ef_prize.Location.Y)
    ef_prize.Location = New Point(ef_witholding_tax1.Location.X, ef_witholding_tax1.Location.Y)
    ef_witholding_tax1.Location = New Point(ef_witholding_tax2.Location.X, ef_witholding_tax2.Location.Y)
    ef_witholding_tax2.Location = New Point(ef_witholding_tax3.Location.X, ef_witholding_tax3.Location.Y)
    ef_witholding_tax3.Location = New Point(ef_full_payment.Location.X, ef_full_payment.Location.Y)
    ef_full_payment.Location = New Point(ef_cash_payment.Location.X, ef_cash_payment.Location.Y)
    ef_cash_payment.Location = New Point(ef_check_payment.Location.X, ef_check_payment.Location.Y)
    ef_check_payment.Location = New Point(ef_cash_payment.Location.X, ef_cash_payment.Location.Y + 24)

    Me.Height = 325
    Me.Width = 525
  End Sub 'RenderForm


  ' PURPOSE: If the operation date don't exceed from allow time to change
  '          Check also user_permissions
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  '     - TRUE: if the change date is greater than Now and the user has write permission
  '     - FALSE: if not, the previous condition
  Private Function CheckAllowChanges() As Boolean

    Dim _allow_edit As Boolean
    Dim _allow_minutes As Integer
    Dim _op_date As Date

    _allow_edit = GeneralParam.GetBoolean("PaymentOrder", "Enabled")

    If Not _allow_edit Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1909), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    If Not Integer.TryParse(WSI.Common.Misc.ReadGeneralParams("PaymentOrder", "EditableMinutes"), _allow_minutes) Then
      _allow_minutes = 0
    End If

    _op_date = m_order.OrderDate.AddMinutes(_allow_minutes)

    If _op_date > WSI.Common.WGDB.Now Then

      Return Me.Permissions.Write     'The modifications is allowed, but depends of Permissions.Write.
    Else
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1581), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

  End Function    ' CheckAllowChanges

  ' PURPOSE: Prints the order
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  Protected Overrides Sub GUI_PrintResultList()

    Dim _files() As String
    Dim _printer_name As String
    Dim _print_params As GhostScriptParameter

    _files = m_order.TempFiles
    _printer_name = Printer.GetPrinterName(2)

    If Not String.IsNullOrEmpty(_printer_name) And _files.Length > 0 Then

      For Each _file As String In _files
        _print_params = New WSI.Common.GhostScriptParameter(_file, 2, _printer_name)

        If _file Is Nothing Or Not WSI.Common.GSPrint.Print(_print_params) Then
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(632), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        Else
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(633), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO)
        End If
      Next

    End If

  End Sub                ' PrintOrder

  ' PURPOSE: sets the values of authorizers
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS : None
  Private Sub SetAuthorizationData()

    ef_title1.TextValue = ""
    ef_name1.TextValue = ""
    ef_title2.TextValue = ""
    ef_name2.TextValue = ""

    If m_gp_auth1_required Then
      ef_title1.TextValue = m_gp_auth1_title
      ef_name1.TextValue = m_gp_auth1_name
    End If

    If m_gp_auth2_required Then
      ef_title2.TextValue = m_gp_auth2_title
      ef_name2.TextValue = m_gp_auth2_name
    End If

  End Sub      ' SetAuthorizationData

  ' PURPOSE: Load the datas from general params
  '          
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  '        None
  Private Sub LoadGeneralParams()

    Dim _types As String
    Dim _auth1 As String
    Dim _auth2 As String
    Dim _dt_doc_types1 As DataTable
    Dim _dt_doc_types2 As DataTable

    _auth1 = ""
    _auth2 = ""
    m_gp_auth1_title = ""
    m_gp_auth1_name = ""
    m_gp_auth2_title = ""
    m_gp_auth2_name = ""
    _types = ""

    _dt_doc_types1 = New DataTable()
    _dt_doc_types2 = New DataTable()

    Try

      'Doc Types
      IdentificationTypes.GetIdentificationTypesPaymentOrders(_dt_doc_types1, _dt_doc_types2)

      If _dt_doc_types1.Rows.Count > 0 Then
        cb_player_doc_type1.Add(_dt_doc_types1, 0, 1)
        cb_player_doc_type1.SelectedIndex = -1
      End If

      If _dt_doc_types2.Rows.Count > 0 Then
        cb_player_doc_type2.Add(_dt_doc_types2, 0, 1)
        cb_player_doc_type2.SelectedIndex = -1
      Else
        cb_player_doc_type2.Visible = False
        ef_player_doc_id2.Visible = False
      End If

      'authorization data  
      _auth1 = WSI.Common.Misc.ReadGeneralParams("PaymentOrder", "Authorization1.Required")
      _auth2 = WSI.Common.Misc.ReadGeneralParams("PaymentOrder", "Authorization2.Required")

      m_gp_auth1_required = (_auth1 = "1")
      m_gp_auth2_required = (_auth2 = "1")

      If m_gp_auth1_required Then
        m_gp_auth1_title = WSI.Common.Misc.ReadGeneralParams("PaymentOrder", "Authorization1.DefaultTitle")
        m_gp_auth1_name = WSI.Common.Misc.ReadGeneralParams("PaymentOrder", "Authorization1.DefaultName")
      End If

      If m_gp_auth2_required Then
        m_gp_auth2_title = WSI.Common.Misc.ReadGeneralParams("PaymentOrder", "Authorization2.DefaultTitle")
        m_gp_auth2_name = WSI.Common.Misc.ReadGeneralParams("PaymentOrder", "Authorization2.DefaultName")
      End If

    Catch ex As Exception

    End Try


  End Sub         ' LoadGeneralParams

  ' PURPOSE: Load the the combo with the bank account alias
  '          
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  '        None
  Private Sub LoadComboAccounts()

    cb_bank_sel.Clear()
    cb_bank_sel.Add(m_bank_accounts)
    cb_bank_sel.SelectedIndex = -1

  End Sub         ' LoadComboAccounts

  ' PURPOSE: If the text supplied exists in the combo, it is selected
  '          
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  '        None
  Private Sub SetComboText(ByVal combo As uc_combo, ByVal text As String)

    Dim _idf As Integer

    For _idf = 0 To combo.Count() - 1

      If combo.TextValue(_idf) = text Then
        combo.SelectedIndex = _idf

        Return
      End If

    Next

  End Sub              ' SetComboText

  ' PURPOSE: Sets the text to the controls
  '          
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  '        None
  Private Sub InitControlsText()

    Me.tab_order_viewer.TabPages.Item(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1624)

    gb_player_info.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1598)
    gb_bank_info.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1599)
    gb_room_info.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1600)

    Me.ef_order_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1597)
    Me.ef_title1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1605, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1613))
    Me.ef_title2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1606, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1613))
    Me.ef_name1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1605, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1614))
    Me.ef_name2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1606, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1614))
    Me.ef_player_name1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1575)
    Me.ef_player_name2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1576)
    Me.ef_player_name3.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1628)
    Me.ef_player_name4.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5087)
    Me.cb_player_doc_type1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1589)
    Me.ef_player_doc_id1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1565)
    Me.cb_player_doc_type2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1590)
    Me.ef_player_doc_id2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1566)

    Me.cb_bank_sel.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1561)
    Me.ef_bank_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1554)
    Me.ef_payment_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1595)
    Me.ef_efective_days.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1556)
    Me.dtp_application_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1596)
    Me.ef_customer_number.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1557)
    Me.ef_account_number.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1558)

    Me.ef_return_balance.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1593)
    Me.ef_prize.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1592)

    Me.ef_witholding_tax1.Text = WSI.Common.Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.1.Name")
    Me.ef_witholding_tax2.Text = WSI.Common.Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.2.Name")
    Me.ef_witholding_tax3.Text = WSI.Common.Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.3.Name")

    Me.ef_full_payment.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1573)
    Me.ef_cash_payment.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1570)
    Me.ef_check_payment.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1569)

    Me.ef_player_doc_type1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1589)
    Me.ef_player_doc_type2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1590)

  End Sub          ' InitControlsText

  ' PURPOSE: Initialize the controls format
  '          
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  '        None
  Private Sub InitControlsFormat()

    Me.ef_title1.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, 50)
    Me.ef_title2.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, 50)
    Me.ef_name1.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_BASE, 50)
    Me.ef_name2.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_BASE, 50)
    Me.ef_player_name1.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_BASE, 50)
    Me.ef_player_name2.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_BASE, 50)
    Me.ef_player_name3.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_BASE, 50)
    Me.ef_player_name4.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_BASE, 50)
    Me.ef_player_doc_id1.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, 20)
    Me.ef_player_doc_id2.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, 20)

    Me.ef_bank_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    Me.ef_customer_number.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    Me.ef_account_number.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    Me.ef_efective_days.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    Me.ef_return_balance.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9, 2)
    Me.ef_prize.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9, 2)
    Me.ef_witholding_tax1.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9, 2)
    Me.ef_witholding_tax2.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9, 2)
    Me.ef_witholding_tax3.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9, 2)
    Me.ef_full_payment.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9, 2)
    Me.ef_cash_payment.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9, 2)
    Me.ef_check_payment.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9, 2)

    Me.cb_bank_sel.Visible = False

    Me.dtp_application_date.SetRange(m_order.OrderDate, DateAdd(DateInterval.Year, 1, Date.Today)) 'Min: OrderDate   Max: Today + 1 year

  End Sub        ' InitControlsFormat

  ' PURPOSE: Load the bank accounts calling the DB_Read method
  '          
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  '        None
  Private Sub LoadBankAccount()

    Dim _sel_id As Integer

    _sel_id = cb_bank_sel.Value(cb_bank_sel.SelectedIndex)

    If m_bank.DB_Read(_sel_id, 0) = ENUM_STATUS.STATUS_OK Then
      ef_bank_name.TextValue = m_bank.BankName
      ef_efective_days.TextValue = m_bank.EfectiveDays
      ef_customer_number.TextValue = m_bank.CustomerNumber
      ef_account_number.TextValue = m_bank.AccountNumber
      ef_payment_type.TextValue = m_bank.PaymentType
    End If

  End Sub           ' LoadBankAccount

  ' PURPOSE: Searches the bank account id supplied in the combo.
  '          
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  '        the bank account index
  '        -1 if it does not find the id supplied
  Private Function BankAccountIdToIndex(ByVal Id As Integer) As Integer
    Dim _idf As Integer

    For _idf = 0 To cb_bank_sel.Count() - 1
      If cb_bank_sel.Value(_idf) = Id Then
        Return _idf
      End If
    Next

    Return -1
  End Function ' BankAccountIdToIndex

  ' PURPOSE: Refresh & Regenerates de tabs and pdfs
  '         
  ' PARAMS:
  '    - INPUT: ShowPagePDF = True -> PDF control is created
  '             ShowPagePDF = False -> PDF control is not created; used only when windows is showed at first time
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  Private Sub RefreshTabPDF(ByVal ShowPagePDF As Boolean)
    Dim _old_temp_folder As String

    _old_temp_folder = ""
    If Not m_order.TempFolder Is Nothing Then
      _old_temp_folder = m_order.TempFolder
    End If

    If m_order.LoadDocument() Then

      Call InitTabControl()

      If ShowPagePDF Then
        Call ShowTabPagePDF(0)
      End If

      'Del If previous docs generated
      If Not _old_temp_folder = "" Then
        DeleteTempFiles(_old_temp_folder)
      End If

    End If

  End Sub             ' RefreshTabPDF

  ' PURPOSE: Gets the tab page activated for the user and shows its corresponding PDF on the web browser
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  Private Sub ShowTabPagePDF(ByVal DocIndex As Integer)

    Dim _document_path As String

    Me.m_wb_order.Dispose()                  'If document tab change, we must release the previous buffer memory, ifnot will have errors deleting temp files
    Me.m_wb_order = New WebBrowser()

    Try
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      Me.GUI_Button(BUTTON_PRINT).Enabled = False

      'set the webbrowser to a blank page
      Call m_wb_order.Navigate("about:blank")

      If DocIndex < m_order.DocumentList.Count Then

        'If there is a envidence related to the current tab...
        If Not m_order.DocumentList(DocIndex) Is Nothing Then
          '...we look for its temp PDF file...
          _document_path = GetTempPDF(DocIndex)

          If _document_path IsNot Nothing Then
            '...and the tell the webbrowser to show it
            Call Me.m_wb_order.Navigate(_document_path & "#toolbar=0&statusbar=0&navpanes=0&view=FitH")

            Me.GUI_Button(BUTTON_PRINT).Enabled = True
          End If
        End If
      End If

      'Finally, let's place the webbrowser control on the tab selected by user
      Me.m_wb_order.Parent = Me.tab_order_viewer.TabPages(DocIndex + 1)
      Me.m_wb_order.Dock = DockStyle.Fill

    Catch ex As Exception

      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "", _
                            "", _
                            ex.Message)

    Finally
      Windows.Forms.Cursor.Current = Cursors.Default

    End Try
  End Sub            ' ShowTabPagePDF

  ' PURPOSE: Saves a stream to a temp file to load it into the webbrowser control
  '         
  ' PARAMS:
  '    - INPUT:
  '       StreamPDF
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  Private Function GetTempPDF(ByVal IdxPDF As Integer) As String
    Dim _temp_file As String

    Try
      'And empty file name means that this evidence haven't be saved to a temp file previously
      If m_order.TempFolder = "" Then

        'let's make a random and expected "unique" filename to save the evidence to a temp file
        m_order.TempFolder = Path.GetRandomFileName()
        m_order.TempFolder = m_order.TempFolder.Replace(".", "")
        m_order.TempFolder = Path.GetTempPath & m_order.TempFolder

        'ensure that the file doesn't exist
        While Directory.Exists(m_order.TempFolder)

          'if exists, let's make another random and expected "unique" filename
          m_order.TempFolder = Path.GetRandomFileName()
          m_order.TempFolder = m_order.TempFolder.Replace(".", "")
          m_order.TempFolder = Path.GetTempPath & m_order.TempFolder

        End While

        Directory.CreateDirectory(m_order.TempFolder)
      End If

      'loop up for a previously saved PDF
      _temp_file = m_order.TempFiles(IdxPDF)

      If _temp_file Is Nothing _
        Or Not File.Exists(_temp_file) Then

        'if there is not a previously saved PDF, let's genereate it
        _temp_file = m_order.TempFolder & Path.DirectorySeparatorChar & m_order.DocumentList(IdxPDF).Name
        Call WriteStreamToTempFolder(_temp_file, m_order.DocumentList(IdxPDF).Content)
        m_order.TempFiles(IdxPDF) = _temp_file

      End If

      Return _temp_file

    Catch ex As Exception
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "", _
                            "", _
                            ex.Message)

      Return Nothing

    End Try

  End Function           ' GetTempPDF

  ' PURPOSE: Save the file stored previously on the DB to a temp file
  '         
  ' PARAMS:
  '    - INPUT:
  '       FilePath
  '        Buffer
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  Private Sub WriteStreamToTempFolder(ByVal FilePath As String, ByVal Buffer() As Byte)
    Dim _file_stream As FileStream

    _file_stream = New FileStream(FilePath, FileMode.CreateNew, FileAccess.Write, FileShare.Read)

    _file_stream.Write(Buffer, 0, Buffer.Length)

    _file_stream.Close()

  End Sub   ' WriteStreamToTempFolder

  ' PURPOSE: Initilizes the tab control where the documents will be shown and shows the first one
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  Private Sub InitTabControl()

    Try

      While tab_order_viewer.TabCount > 1
        Me.tab_order_viewer.TabPages.RemoveAt(1)
      End While

      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      m_order.TempFolder = ""
      m_order.TempFiles = New String(m_order.DocumentList.Count - 1) {}

      GetTempPDF(0)

      If m_order.DocumentId > 0 Then
        Me.tab_order_viewer.TabPages.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1587))
      End If

    Catch ex As Exception

      NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id((123)), _
                 ENUM_MB_TYPE.MB_TYPE_ERROR, _
                 ENUM_MB_BTN.MB_BTN_OK)

      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "", _
                            "", _
                            ex.Message)

      Call Me.Close()

    Finally
      Windows.Forms.Cursor.Current = Cursors.Default

    End Try

  End Sub            ' InitTabControl


#End Region  ' Privates

#Region " Publics "

  Public Function ValidateFormat() As Boolean

    If Not ef_player_name3.ValidateFormat() Then
      Call ef_player_name3.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
            mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.ef_player_name3.Text)
      Return False
    End If

    If Not ef_player_name4.ValidateFormat() Then
      Call ef_player_name4.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
            mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.ef_player_name4.Text)
      Return False
    End If

    If Not String.IsNullOrEmpty(ef_player_name2.TextValue) And Not ef_player_name2.ValidateFormat() Then
      Call ef_player_name2.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
            mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.ef_player_name2.Text)
      Return False
    End If

    If Not ef_player_name1.ValidateFormat() Then
      Call ef_player_name1.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
            mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.ef_player_name1.Text)
      Return False
    End If

    If Not ef_player_doc_id1.ValidateFormat() Then
      Call ef_player_doc_id1.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
            mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.ef_player_doc_id1.Text)
      Return False
    End If

    If Not String.IsNullOrEmpty(ef_player_doc_id2.TextValue) And Not ef_player_doc_id2.ValidateFormat() Then
      Call ef_player_doc_id2.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
            mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.ef_player_doc_id2.Text)
      Return False
    End If

    If Not ef_title1.IsReadOnly And Not ef_title1.ValidateFormat() Then
      Call ef_title1.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
            mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.ef_title1.Text)
      Return False
    End If

    If Not ef_name1.IsReadOnly And Not ef_name1.ValidateFormat() Then
      Call ef_name1.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
            mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.ef_name1.Text)
      Return False
    End If

    If Not ef_title2.IsReadOnly And Not ef_title2.ValidateFormat() Then
      Call ef_title2.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
            mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.ef_title2.Text)
      Return False
    End If

    If Not ef_name2.IsReadOnly And Not ef_name2.ValidateFormat() Then
      Call ef_name2.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
            mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.ef_name2.Text)
      Return False
    End If

    Return True

  End Function

  ' PURPOSE: Display the form
  '         
  ' PARAMS:
  '    - INPUT:
  '       TempFiles
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  Public Overloads Sub ShowNewItem()

  End Sub ' ShowNewItem

  ' PURPOSE: Create a new instance
  '         
  ' PARAMS:
  '    - INPUT:
  '       TempFiles
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  Public Sub New()

    InitializeComponent()

    m_wb_order = New WebBrowser()
    m_bank_accounts = CLASS_BANK_ACCOUNT.FillComboAccounts()
    m_bank = New CLASS_BANK_ACCOUNT()

  End Sub                   ' New

#End Region   ' Publics

#Region " Events "

  Private Sub cb_bank_sel_ValueChangedEvent() Handles cb_bank_sel.ValueChangedEvent

    If cb_bank_sel.SelectedIndex >= 0 And m_edition_mode = True Then
      Call LoadBankAccount()
    End If

  End Sub

  Private Sub tab_order_viewer_Selected(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TabControlEventArgs) Handles tab_order_viewer.Selected

    If tab_order_viewer.SelectedIndex > 0 Then                  'Only show Web Browser (pdf viewer) when the selected tab is not the 0 (data entry)
      Call ShowTabPagePDF(tab_order_viewer.SelectedIndex - 1)
    End If

  End Sub

#End Region    ' Events

End Class
