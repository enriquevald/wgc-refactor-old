﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_new_section_schema_edit
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_new_section_schema_edit))
    Me.lbl_template = New System.Windows.Forms.Label()
    Me.uc_template = New GUI_Controls.uc_combo()
    Me.lbl_layout = New System.Windows.Forms.Label()
    Me.uc_layout = New GUI_Controls.uc_combo()
    Me.chk_visible = New System.Windows.Forms.CheckBox()
    Me.lbl_section_parent = New System.Windows.Forms.Label()
    Me.lbl_Order = New System.Windows.Forms.Label()
    Me.lbl_name = New System.Windows.Forms.Label()
    Me.ef_order = New GUI_Controls.uc_entry_field()
    Me.uc_section_parent = New GUI_Controls.uc_combo()
    Me.txt_name = New GUI_Controls.uc_entry_field()
    Me.gb_superior_level = New System.Windows.Forms.GroupBox()
    Me.lbl_icon_image = New System.Windows.Forms.Label()
    Me.gb_image_reference_1 = New System.Windows.Forms.GroupBox()
    Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
    Me.pb_image_reference_superior = New System.Windows.Forms.PictureBox()
    Me.uc_image_icon = New GUI_Controls.uc_image()
    Me.lbl_abstract = New System.Windows.Forms.Label()
    Me.lbl_button_text = New System.Windows.Forms.Label()
    Me.txt_abstract = New System.Windows.Forms.TextBox()
    Me.lbl_subtitle = New System.Windows.Forms.Label()
    Me.lbl_title = New System.Windows.Forms.Label()
    Me.txt_button_text = New GUI_Controls.uc_entry_field()
    Me.txt_main_subtitle = New GUI_Controls.uc_entry_field()
    Me.txt_main_title = New GUI_Controls.uc_entry_field()
    Me.gb_items = New System.Windows.Forms.GroupBox()
    Me.gb_image_reference_3 = New System.Windows.Forms.GroupBox()
    Me.LinkLabel3 = New System.Windows.Forms.LinkLabel()
    Me.pb_image_reference_items = New System.Windows.Forms.PictureBox()
    Me.lbl_layout_item = New System.Windows.Forms.Label()
    Me.uc_layout_item = New GUI_Controls.uc_combo()
    Me.gb_detail = New System.Windows.Forms.GroupBox()
    Me.lbl_description = New System.Windows.Forms.Label()
    Me.txt_description = New System.Windows.Forms.TextBox()
    Me.lbl_back_image = New System.Windows.Forms.Label()
    Me.gb_image_reference_2 = New System.Windows.Forms.GroupBox()
    Me.LinkLabel2 = New System.Windows.Forms.LinkLabel()
    Me.pb_image_reference_detail = New System.Windows.Forms.PictureBox()
    Me.lbl_headline_text = New System.Windows.Forms.Label()
    Me.lbl_main_subtitle = New System.Windows.Forms.Label()
    Me.lbl_main_title = New System.Windows.Forms.Label()
    Me.uc_image_background_detail = New GUI_Controls.uc_image()
    Me.txt_headline_text = New GUI_Controls.uc_entry_field()
    Me.txt_subtitle = New GUI_Controls.uc_entry_field()
    Me.txt_title = New GUI_Controls.uc_entry_field()
    Me.lbl_type = New System.Windows.Forms.Label()
    Me.lbl_content = New System.Windows.Forms.Label()
    Me.gb_childrens = New System.Windows.Forms.GroupBox()
    Me.btn_delete_child = New System.Windows.Forms.Button()
    Me.btn_add_child = New System.Windows.Forms.Button()
    Me.uc_grid_childrens = New GUI_Controls.uc_grid()
    Me.panel_data.SuspendLayout()
    Me.gb_superior_level.SuspendLayout()
    Me.gb_image_reference_1.SuspendLayout()
    CType(Me.pb_image_reference_superior, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.gb_items.SuspendLayout()
    Me.gb_image_reference_3.SuspendLayout()
    CType(Me.pb_image_reference_items, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.gb_detail.SuspendLayout()
    Me.gb_image_reference_2.SuspendLayout()
    CType(Me.pb_image_reference_detail, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.gb_childrens.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_childrens)
    Me.panel_data.Controls.Add(Me.lbl_content)
    Me.panel_data.Controls.Add(Me.lbl_type)
    Me.panel_data.Controls.Add(Me.gb_superior_level)
    Me.panel_data.Controls.Add(Me.gb_items)
    Me.panel_data.Controls.Add(Me.gb_detail)
    Me.panel_data.Controls.Add(Me.lbl_template)
    Me.panel_data.Controls.Add(Me.uc_template)
    Me.panel_data.Controls.Add(Me.lbl_layout)
    Me.panel_data.Controls.Add(Me.uc_layout)
    Me.panel_data.Controls.Add(Me.chk_visible)
    Me.panel_data.Controls.Add(Me.lbl_section_parent)
    Me.panel_data.Controls.Add(Me.lbl_Order)
    Me.panel_data.Controls.Add(Me.lbl_name)
    Me.panel_data.Controls.Add(Me.ef_order)
    Me.panel_data.Controls.Add(Me.uc_section_parent)
    Me.panel_data.Controls.Add(Me.txt_name)
    Me.panel_data.Location = New System.Drawing.Point(3, 3)
    Me.panel_data.Margin = New System.Windows.Forms.Padding(2)
    Me.panel_data.Size = New System.Drawing.Size(1128, 782)
    '
    'lbl_template
    '
    Me.lbl_template.AutoSize = True
    Me.lbl_template.Location = New System.Drawing.Point(368, 45)
    Me.lbl_template.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
    Me.lbl_template.Name = "lbl_template"
    Me.lbl_template.Size = New System.Drawing.Size(66, 13)
    Me.lbl_template.TabIndex = 50
    Me.lbl_template.Text = "xTemplate"
    '
    'uc_template
    '
    Me.uc_template.AllowUnlistedValues = False
    Me.uc_template.AutoCompleteMode = False
    Me.uc_template.IsReadOnly = False
    Me.uc_template.Location = New System.Drawing.Point(517, 38)
    Me.uc_template.Margin = New System.Windows.Forms.Padding(2)
    Me.uc_template.Name = "uc_template"
    Me.uc_template.SelectedIndex = -1
    Me.uc_template.Size = New System.Drawing.Size(242, 24)
    Me.uc_template.SufixText = "Sufix Text"
    Me.uc_template.SufixTextVisible = True
    Me.uc_template.TabIndex = 49
    Me.uc_template.TextCombo = Nothing
    Me.uc_template.TextWidth = 0
    '
    'lbl_layout
    '
    Me.lbl_layout.AutoSize = True
    Me.lbl_layout.Location = New System.Drawing.Point(368, 72)
    Me.lbl_layout.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
    Me.lbl_layout.Name = "lbl_layout"
    Me.lbl_layout.Size = New System.Drawing.Size(52, 13)
    Me.lbl_layout.TabIndex = 48
    Me.lbl_layout.Text = "xLayout"
    '
    'uc_layout
    '
    Me.uc_layout.AllowUnlistedValues = False
    Me.uc_layout.AutoCompleteMode = False
    Me.uc_layout.IsReadOnly = False
    Me.uc_layout.Location = New System.Drawing.Point(517, 65)
    Me.uc_layout.Margin = New System.Windows.Forms.Padding(2)
    Me.uc_layout.Name = "uc_layout"
    Me.uc_layout.SelectedIndex = -1
    Me.uc_layout.Size = New System.Drawing.Size(242, 24)
    Me.uc_layout.SufixText = "Sufix Text"
    Me.uc_layout.SufixTextVisible = True
    Me.uc_layout.TabIndex = 47
    Me.uc_layout.TextCombo = Nothing
    Me.uc_layout.TextWidth = 0
    '
    'chk_visible
    '
    Me.chk_visible.AutoSize = True
    Me.chk_visible.Location = New System.Drawing.Point(258, 72)
    Me.chk_visible.Margin = New System.Windows.Forms.Padding(2)
    Me.chk_visible.Name = "chk_visible"
    Me.chk_visible.Size = New System.Drawing.Size(63, 17)
    Me.chk_visible.TabIndex = 46
    Me.chk_visible.Text = "Visible"
    Me.chk_visible.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_visible.UseVisualStyleBackColor = True
    '
    'lbl_section_parent
    '
    Me.lbl_section_parent.AutoSize = True
    Me.lbl_section_parent.Location = New System.Drawing.Point(9, 45)
    Me.lbl_section_parent.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
    Me.lbl_section_parent.Name = "lbl_section_parent"
    Me.lbl_section_parent.Size = New System.Drawing.Size(51, 13)
    Me.lbl_section_parent.TabIndex = 45
    Me.lbl_section_parent.Text = "xParent"
    '
    'lbl_Order
    '
    Me.lbl_Order.AutoSize = True
    Me.lbl_Order.Location = New System.Drawing.Point(9, 72)
    Me.lbl_Order.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
    Me.lbl_Order.Name = "lbl_Order"
    Me.lbl_Order.Size = New System.Drawing.Size(47, 13)
    Me.lbl_Order.TabIndex = 44
    Me.lbl_Order.Text = "xOrder"
    '
    'lbl_name
    '
    Me.lbl_name.AutoSize = True
    Me.lbl_name.Location = New System.Drawing.Point(368, 18)
    Me.lbl_name.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
    Me.lbl_name.Name = "lbl_name"
    Me.lbl_name.Size = New System.Drawing.Size(47, 13)
    Me.lbl_name.TabIndex = 43
    Me.lbl_name.Text = "xName"
    '
    'ef_order
    '
    Me.ef_order.DoubleValue = 0.0R
    Me.ef_order.IntegerValue = 0
    Me.ef_order.IsReadOnly = False
    Me.ef_order.Location = New System.Drawing.Point(103, 66)
    Me.ef_order.Margin = New System.Windows.Forms.Padding(2)
    Me.ef_order.Name = "ef_order"
    Me.ef_order.PlaceHolder = Nothing
    Me.ef_order.Size = New System.Drawing.Size(43, 24)
    Me.ef_order.SufixText = "Sufix Text"
    Me.ef_order.SufixTextVisible = True
    Me.ef_order.TabIndex = 42
    Me.ef_order.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_order.TextValue = "0"
    Me.ef_order.TextWidth = 0
    Me.ef_order.Value = "0"
    Me.ef_order.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_section_parent
    '
    Me.uc_section_parent.AllowUnlistedValues = False
    Me.uc_section_parent.AutoCompleteMode = False
    Me.uc_section_parent.IsReadOnly = False
    Me.uc_section_parent.Location = New System.Drawing.Point(103, 37)
    Me.uc_section_parent.Margin = New System.Windows.Forms.Padding(2)
    Me.uc_section_parent.Name = "uc_section_parent"
    Me.uc_section_parent.SelectedIndex = -1
    Me.uc_section_parent.Size = New System.Drawing.Size(215, 24)
    Me.uc_section_parent.SufixText = "Sufix Text"
    Me.uc_section_parent.SufixTextVisible = True
    Me.uc_section_parent.TabIndex = 41
    Me.uc_section_parent.TextCombo = Nothing
    Me.uc_section_parent.TextWidth = 0
    '
    'txt_name
    '
    Me.txt_name.DoubleValue = 0.0R
    Me.txt_name.IntegerValue = 0
    Me.txt_name.IsReadOnly = False
    Me.txt_name.Location = New System.Drawing.Point(517, 11)
    Me.txt_name.Margin = New System.Windows.Forms.Padding(0)
    Me.txt_name.Name = "txt_name"
    Me.txt_name.PlaceHolder = Nothing
    Me.txt_name.Size = New System.Drawing.Size(242, 24)
    Me.txt_name.SufixText = "Sufix Text"
    Me.txt_name.SufixTextVisible = True
    Me.txt_name.TabIndex = 40
    Me.txt_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.txt_name.TextValue = ""
    Me.txt_name.TextWidth = 0
    Me.txt_name.Value = ""
    Me.txt_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_superior_level
    '
    Me.gb_superior_level.Controls.Add(Me.lbl_icon_image)
    Me.gb_superior_level.Controls.Add(Me.gb_image_reference_1)
    Me.gb_superior_level.Controls.Add(Me.uc_image_icon)
    Me.gb_superior_level.Controls.Add(Me.lbl_abstract)
    Me.gb_superior_level.Controls.Add(Me.lbl_button_text)
    Me.gb_superior_level.Controls.Add(Me.txt_abstract)
    Me.gb_superior_level.Controls.Add(Me.lbl_subtitle)
    Me.gb_superior_level.Controls.Add(Me.lbl_title)
    Me.gb_superior_level.Controls.Add(Me.txt_button_text)
    Me.gb_superior_level.Controls.Add(Me.txt_main_subtitle)
    Me.gb_superior_level.Controls.Add(Me.txt_main_title)
    Me.gb_superior_level.Location = New System.Drawing.Point(4, 104)
    Me.gb_superior_level.Margin = New System.Windows.Forms.Padding(2)
    Me.gb_superior_level.Name = "gb_superior_level"
    Me.gb_superior_level.Padding = New System.Windows.Forms.Padding(2)
    Me.gb_superior_level.Size = New System.Drawing.Size(449, 373)
    Me.gb_superior_level.TabIndex = 53
    Me.gb_superior_level.TabStop = False
    Me.gb_superior_level.Text = "xSuperior_Label"
    '
    'lbl_icon_image
    '
    Me.lbl_icon_image.AutoSize = True
    Me.lbl_icon_image.Location = New System.Drawing.Point(7, 181)
    Me.lbl_icon_image.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
    Me.lbl_icon_image.Name = "lbl_icon_image"
    Me.lbl_icon_image.Size = New System.Drawing.Size(75, 13)
    Me.lbl_icon_image.TabIndex = 45
    Me.lbl_icon_image.Text = "Encabezado"
    '
    'gb_image_reference_1
    '
    Me.gb_image_reference_1.Controls.Add(Me.LinkLabel1)
    Me.gb_image_reference_1.Controls.Add(Me.pb_image_reference_superior)
    Me.gb_image_reference_1.Location = New System.Drawing.Point(320, 13)
    Me.gb_image_reference_1.Name = "gb_image_reference_1"
    Me.gb_image_reference_1.Size = New System.Drawing.Size(125, 216)
    Me.gb_image_reference_1.TabIndex = 39
    Me.gb_image_reference_1.TabStop = False
    Me.gb_image_reference_1.Text = "Imagen de guia"
    '
    'LinkLabel1
    '
    Me.LinkLabel1.AutoSize = True
    Me.LinkLabel1.Location = New System.Drawing.Point(36, 193)
    Me.LinkLabel1.Name = "LinkLabel1"
    Me.LinkLabel1.Size = New System.Drawing.Size(51, 13)
    Me.LinkLabel1.TabIndex = 27
    Me.LinkLabel1.TabStop = True
    Me.LinkLabel1.Text = "Ampliar"
    '
    'pb_image_reference_superior
    '
    Me.pb_image_reference_superior.Location = New System.Drawing.Point(11, 19)
    Me.pb_image_reference_superior.Margin = New System.Windows.Forms.Padding(2)
    Me.pb_image_reference_superior.Name = "pb_image_reference_superior"
    Me.pb_image_reference_superior.Size = New System.Drawing.Size(103, 155)
    Me.pb_image_reference_superior.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
    Me.pb_image_reference_superior.TabIndex = 26
    Me.pb_image_reference_superior.TabStop = False
    '
    'uc_image_icon
    '
    Me.uc_image_icon.AutoSize = True
    Me.uc_image_icon.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_image_icon.ButtonDeleteEnabled = True
    Me.uc_image_icon.FreeResize = False
    Me.uc_image_icon.Image = Nothing
    Me.uc_image_icon.ImageInfoFont = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_image_icon.ImageLayout = System.Windows.Forms.ImageLayout.Zoom
    Me.uc_image_icon.ImageName = Nothing
    Me.uc_image_icon.Location = New System.Drawing.Point(113, 166)
    Me.uc_image_icon.Margin = New System.Windows.Forms.Padding(0)
    Me.uc_image_icon.Name = "uc_image_icon"
    Me.uc_image_icon.Size = New System.Drawing.Size(203, 194)
    Me.uc_image_icon.TabIndex = 40
    Me.uc_image_icon.Transparent = False
    '
    'lbl_abstract
    '
    Me.lbl_abstract.AutoSize = True
    Me.lbl_abstract.Location = New System.Drawing.Point(7, 100)
    Me.lbl_abstract.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
    Me.lbl_abstract.Name = "lbl_abstract"
    Me.lbl_abstract.Size = New System.Drawing.Size(78, 13)
    Me.lbl_abstract.TabIndex = 38
    Me.lbl_abstract.Text = "xDescription"
    '
    'lbl_button_text
    '
    Me.lbl_button_text.AutoSize = True
    Me.lbl_button_text.Location = New System.Drawing.Point(7, 72)
    Me.lbl_button_text.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
    Me.lbl_button_text.Name = "lbl_button_text"
    Me.lbl_button_text.Size = New System.Drawing.Size(75, 13)
    Me.lbl_button_text.TabIndex = 34
    Me.lbl_button_text.Text = "xButtonText"
    '
    'txt_abstract
    '
    Me.txt_abstract.Location = New System.Drawing.Point(113, 97)
    Me.txt_abstract.Margin = New System.Windows.Forms.Padding(2)
    Me.txt_abstract.MaxLength = 200
    Me.txt_abstract.Multiline = True
    Me.txt_abstract.Name = "txt_abstract"
    Me.txt_abstract.Size = New System.Drawing.Size(201, 59)
    Me.txt_abstract.TabIndex = 37
    '
    'lbl_subtitle
    '
    Me.lbl_subtitle.AutoSize = True
    Me.lbl_subtitle.Location = New System.Drawing.Point(7, 48)
    Me.lbl_subtitle.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
    Me.lbl_subtitle.Name = "lbl_subtitle"
    Me.lbl_subtitle.Size = New System.Drawing.Size(57, 13)
    Me.lbl_subtitle.TabIndex = 36
    Me.lbl_subtitle.Text = "Subtitulo"
    '
    'lbl_title
    '
    Me.lbl_title.AutoSize = True
    Me.lbl_title.Location = New System.Drawing.Point(7, 25)
    Me.lbl_title.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
    Me.lbl_title.Name = "lbl_title"
    Me.lbl_title.Size = New System.Drawing.Size(38, 13)
    Me.lbl_title.TabIndex = 35
    Me.lbl_title.Text = "Titulo"
    '
    'txt_button_text
    '
    Me.txt_button_text.DoubleValue = 0.0R
    Me.txt_button_text.IntegerValue = 0
    Me.txt_button_text.IsReadOnly = False
    Me.txt_button_text.Location = New System.Drawing.Point(111, 68)
    Me.txt_button_text.Margin = New System.Windows.Forms.Padding(2)
    Me.txt_button_text.Name = "txt_button_text"
    Me.txt_button_text.PlaceHolder = Nothing
    Me.txt_button_text.Size = New System.Drawing.Size(205, 24)
    Me.txt_button_text.SufixText = "Sufix Text"
    Me.txt_button_text.SufixTextVisible = True
    Me.txt_button_text.TabIndex = 33
    Me.txt_button_text.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.txt_button_text.TextValue = ""
    Me.txt_button_text.TextWidth = 0
    Me.txt_button_text.Value = ""
    Me.txt_button_text.ValueForeColor = System.Drawing.Color.Blue
    '
    'txt_main_subtitle
    '
    Me.txt_main_subtitle.DoubleValue = 0.0R
    Me.txt_main_subtitle.IntegerValue = 0
    Me.txt_main_subtitle.IsReadOnly = False
    Me.txt_main_subtitle.Location = New System.Drawing.Point(111, 43)
    Me.txt_main_subtitle.Margin = New System.Windows.Forms.Padding(2)
    Me.txt_main_subtitle.Name = "txt_main_subtitle"
    Me.txt_main_subtitle.PlaceHolder = Nothing
    Me.txt_main_subtitle.Size = New System.Drawing.Size(205, 24)
    Me.txt_main_subtitle.SufixText = "Sufix Text"
    Me.txt_main_subtitle.SufixTextVisible = True
    Me.txt_main_subtitle.TabIndex = 32
    Me.txt_main_subtitle.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.txt_main_subtitle.TextValue = ""
    Me.txt_main_subtitle.TextWidth = 0
    Me.txt_main_subtitle.Value = ""
    Me.txt_main_subtitle.ValueForeColor = System.Drawing.Color.Blue
    '
    'txt_main_title
    '
    Me.txt_main_title.DoubleValue = 0.0R
    Me.txt_main_title.IntegerValue = 0
    Me.txt_main_title.IsReadOnly = False
    Me.txt_main_title.Location = New System.Drawing.Point(111, 18)
    Me.txt_main_title.Margin = New System.Windows.Forms.Padding(2)
    Me.txt_main_title.Name = "txt_main_title"
    Me.txt_main_title.PlaceHolder = Nothing
    Me.txt_main_title.Size = New System.Drawing.Size(205, 24)
    Me.txt_main_title.SufixText = "Sufix Text"
    Me.txt_main_title.SufixTextVisible = True
    Me.txt_main_title.TabIndex = 31
    Me.txt_main_title.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.txt_main_title.TextValue = ""
    Me.txt_main_title.TextWidth = 0
    Me.txt_main_title.Value = ""
    Me.txt_main_title.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_items
    '
    Me.gb_items.Controls.Add(Me.gb_image_reference_3)
    Me.gb_items.Controls.Add(Me.lbl_layout_item)
    Me.gb_items.Controls.Add(Me.uc_layout_item)
    Me.gb_items.Location = New System.Drawing.Point(926, 104)
    Me.gb_items.Margin = New System.Windows.Forms.Padding(2)
    Me.gb_items.Name = "gb_items"
    Me.gb_items.Padding = New System.Windows.Forms.Padding(2)
    Me.gb_items.Size = New System.Drawing.Size(195, 373)
    Me.gb_items.TabIndex = 52
    Me.gb_items.TabStop = False
    Me.gb_items.Text = "xItems"
    '
    'gb_image_reference_3
    '
    Me.gb_image_reference_3.Controls.Add(Me.LinkLabel3)
    Me.gb_image_reference_3.Controls.Add(Me.pb_image_reference_items)
    Me.gb_image_reference_3.Location = New System.Drawing.Point(35, 68)
    Me.gb_image_reference_3.Name = "gb_image_reference_3"
    Me.gb_image_reference_3.Size = New System.Drawing.Size(125, 216)
    Me.gb_image_reference_3.TabIndex = 44
    Me.gb_image_reference_3.TabStop = False
    Me.gb_image_reference_3.Text = "Imagen de guia"
    '
    'LinkLabel3
    '
    Me.LinkLabel3.AutoSize = True
    Me.LinkLabel3.Location = New System.Drawing.Point(36, 188)
    Me.LinkLabel3.Name = "LinkLabel3"
    Me.LinkLabel3.Size = New System.Drawing.Size(51, 13)
    Me.LinkLabel3.TabIndex = 44
    Me.LinkLabel3.TabStop = True
    Me.LinkLabel3.Text = "Ampliar"
    '
    'pb_image_reference_items
    '
    Me.pb_image_reference_items.Location = New System.Drawing.Point(11, 19)
    Me.pb_image_reference_items.Margin = New System.Windows.Forms.Padding(2)
    Me.pb_image_reference_items.Name = "pb_image_reference_items"
    Me.pb_image_reference_items.Size = New System.Drawing.Size(103, 155)
    Me.pb_image_reference_items.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
    Me.pb_image_reference_items.TabIndex = 43
    Me.pb_image_reference_items.TabStop = False
    '
    'lbl_layout_item
    '
    Me.lbl_layout_item.AutoSize = True
    Me.lbl_layout_item.Location = New System.Drawing.Point(5, 26)
    Me.lbl_layout_item.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
    Me.lbl_layout_item.Name = "lbl_layout_item"
    Me.lbl_layout_item.Size = New System.Drawing.Size(51, 13)
    Me.lbl_layout_item.TabIndex = 41
    Me.lbl_layout_item.Text = "Plantilla"
    '
    'uc_layout_item
    '
    Me.uc_layout_item.AllowUnlistedValues = False
    Me.uc_layout_item.AutoCompleteMode = False
    Me.uc_layout_item.IsReadOnly = False
    Me.uc_layout_item.Location = New System.Drawing.Point(54, 19)
    Me.uc_layout_item.Margin = New System.Windows.Forms.Padding(2)
    Me.uc_layout_item.Name = "uc_layout_item"
    Me.uc_layout_item.SelectedIndex = -1
    Me.uc_layout_item.Size = New System.Drawing.Size(137, 25)
    Me.uc_layout_item.SufixText = "Sufix Text"
    Me.uc_layout_item.SufixTextVisible = True
    Me.uc_layout_item.TabIndex = 40
    Me.uc_layout_item.TextCombo = Nothing
    Me.uc_layout_item.TextWidth = 0
    '
    'gb_detail
    '
    Me.gb_detail.Controls.Add(Me.lbl_description)
    Me.gb_detail.Controls.Add(Me.txt_description)
    Me.gb_detail.Controls.Add(Me.lbl_back_image)
    Me.gb_detail.Controls.Add(Me.gb_image_reference_2)
    Me.gb_detail.Controls.Add(Me.lbl_headline_text)
    Me.gb_detail.Controls.Add(Me.lbl_main_subtitle)
    Me.gb_detail.Controls.Add(Me.lbl_main_title)
    Me.gb_detail.Controls.Add(Me.uc_image_background_detail)
    Me.gb_detail.Controls.Add(Me.txt_headline_text)
    Me.gb_detail.Controls.Add(Me.txt_subtitle)
    Me.gb_detail.Controls.Add(Me.txt_title)
    Me.gb_detail.Location = New System.Drawing.Point(462, 104)
    Me.gb_detail.Margin = New System.Windows.Forms.Padding(2)
    Me.gb_detail.Name = "gb_detail"
    Me.gb_detail.Padding = New System.Windows.Forms.Padding(2)
    Me.gb_detail.Size = New System.Drawing.Size(460, 373)
    Me.gb_detail.TabIndex = 51
    Me.gb_detail.TabStop = False
    Me.gb_detail.Text = "xDetails"
    '
    'lbl_description
    '
    Me.lbl_description.AutoSize = True
    Me.lbl_description.Location = New System.Drawing.Point(7, 100)
    Me.lbl_description.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
    Me.lbl_description.Name = "lbl_description"
    Me.lbl_description.Size = New System.Drawing.Size(89, 13)
    Me.lbl_description.TabIndex = 47
    Me.lbl_description.Text = "lbl_description"
    '
    'txt_description
    '
    Me.txt_description.Location = New System.Drawing.Point(123, 97)
    Me.txt_description.Margin = New System.Windows.Forms.Padding(2)
    Me.txt_description.MaxLength = 200
    Me.txt_description.Multiline = True
    Me.txt_description.Name = "txt_description"
    Me.txt_description.Size = New System.Drawing.Size(202, 59)
    Me.txt_description.TabIndex = 46
    '
    'lbl_back_image
    '
    Me.lbl_back_image.AutoSize = True
    Me.lbl_back_image.Location = New System.Drawing.Point(7, 181)
    Me.lbl_back_image.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
    Me.lbl_back_image.Name = "lbl_back_image"
    Me.lbl_back_image.Size = New System.Drawing.Size(114, 13)
    Me.lbl_back_image.TabIndex = 44
    Me.lbl_back_image.Text = "Background image"
    '
    'gb_image_reference_2
    '
    Me.gb_image_reference_2.Controls.Add(Me.LinkLabel2)
    Me.gb_image_reference_2.Controls.Add(Me.pb_image_reference_detail)
    Me.gb_image_reference_2.Location = New System.Drawing.Point(330, 13)
    Me.gb_image_reference_2.Name = "gb_image_reference_2"
    Me.gb_image_reference_2.Size = New System.Drawing.Size(125, 216)
    Me.gb_image_reference_2.TabIndex = 43
    Me.gb_image_reference_2.TabStop = False
    Me.gb_image_reference_2.Text = "Imagen de guia"
    '
    'LinkLabel2
    '
    Me.LinkLabel2.AutoSize = True
    Me.LinkLabel2.Location = New System.Drawing.Point(37, 192)
    Me.LinkLabel2.Name = "LinkLabel2"
    Me.LinkLabel2.Size = New System.Drawing.Size(51, 13)
    Me.LinkLabel2.TabIndex = 28
    Me.LinkLabel2.TabStop = True
    Me.LinkLabel2.Text = "Ampliar"
    '
    'pb_image_reference_detail
    '
    Me.pb_image_reference_detail.Location = New System.Drawing.Point(11, 19)
    Me.pb_image_reference_detail.Margin = New System.Windows.Forms.Padding(2)
    Me.pb_image_reference_detail.Name = "pb_image_reference_detail"
    Me.pb_image_reference_detail.Size = New System.Drawing.Size(103, 155)
    Me.pb_image_reference_detail.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
    Me.pb_image_reference_detail.TabIndex = 27
    Me.pb_image_reference_detail.TabStop = False
    '
    'lbl_headline_text
    '
    Me.lbl_headline_text.AutoSize = True
    Me.lbl_headline_text.Location = New System.Drawing.Point(7, 72)
    Me.lbl_headline_text.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
    Me.lbl_headline_text.Name = "lbl_headline_text"
    Me.lbl_headline_text.Size = New System.Drawing.Size(75, 13)
    Me.lbl_headline_text.TabIndex = 42
    Me.lbl_headline_text.Text = "Encabezado"
    '
    'lbl_main_subtitle
    '
    Me.lbl_main_subtitle.AutoSize = True
    Me.lbl_main_subtitle.Location = New System.Drawing.Point(7, 48)
    Me.lbl_main_subtitle.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
    Me.lbl_main_subtitle.Name = "lbl_main_subtitle"
    Me.lbl_main_subtitle.Size = New System.Drawing.Size(99, 13)
    Me.lbl_main_subtitle.TabIndex = 41
    Me.lbl_main_subtitle.Text = "Subtitulo detalle"
    '
    'lbl_main_title
    '
    Me.lbl_main_title.AutoSize = True
    Me.lbl_main_title.Location = New System.Drawing.Point(7, 24)
    Me.lbl_main_title.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
    Me.lbl_main_title.Name = "lbl_main_title"
    Me.lbl_main_title.Size = New System.Drawing.Size(80, 13)
    Me.lbl_main_title.TabIndex = 40
    Me.lbl_main_title.Text = "Titulo detalle"
    '
    'uc_image_background_detail
    '
    Me.uc_image_background_detail.AutoSize = True
    Me.uc_image_background_detail.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_image_background_detail.ButtonDeleteEnabled = True
    Me.uc_image_background_detail.FreeResize = False
    Me.uc_image_background_detail.Image = Nothing
    Me.uc_image_background_detail.ImageInfoFont = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_image_background_detail.ImageLayout = System.Windows.Forms.ImageLayout.Zoom
    Me.uc_image_background_detail.ImageName = Nothing
    Me.uc_image_background_detail.Location = New System.Drawing.Point(123, 166)
    Me.uc_image_background_detail.Margin = New System.Windows.Forms.Padding(0)
    Me.uc_image_background_detail.Name = "uc_image_background_detail"
    Me.uc_image_background_detail.Size = New System.Drawing.Size(203, 194)
    Me.uc_image_background_detail.TabIndex = 39
    Me.uc_image_background_detail.Transparent = False
    '
    'txt_headline_text
    '
    Me.txt_headline_text.DoubleValue = 0.0R
    Me.txt_headline_text.IntegerValue = 0
    Me.txt_headline_text.IsReadOnly = False
    Me.txt_headline_text.Location = New System.Drawing.Point(121, 68)
    Me.txt_headline_text.Margin = New System.Windows.Forms.Padding(2)
    Me.txt_headline_text.Name = "txt_headline_text"
    Me.txt_headline_text.PlaceHolder = Nothing
    Me.txt_headline_text.Size = New System.Drawing.Size(206, 24)
    Me.txt_headline_text.SufixText = "Sufix Text"
    Me.txt_headline_text.SufixTextVisible = True
    Me.txt_headline_text.TabIndex = 38
    Me.txt_headline_text.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.txt_headline_text.TextValue = ""
    Me.txt_headline_text.TextWidth = 0
    Me.txt_headline_text.Value = ""
    Me.txt_headline_text.ValueForeColor = System.Drawing.Color.Blue
    '
    'txt_subtitle
    '
    Me.txt_subtitle.DoubleValue = 0.0R
    Me.txt_subtitle.IntegerValue = 0
    Me.txt_subtitle.IsReadOnly = False
    Me.txt_subtitle.Location = New System.Drawing.Point(121, 43)
    Me.txt_subtitle.Margin = New System.Windows.Forms.Padding(2)
    Me.txt_subtitle.Name = "txt_subtitle"
    Me.txt_subtitle.PlaceHolder = Nothing
    Me.txt_subtitle.Size = New System.Drawing.Size(206, 24)
    Me.txt_subtitle.SufixText = "Sufix Text"
    Me.txt_subtitle.SufixTextVisible = True
    Me.txt_subtitle.TabIndex = 37
    Me.txt_subtitle.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.txt_subtitle.TextValue = ""
    Me.txt_subtitle.TextWidth = 0
    Me.txt_subtitle.Value = ""
    Me.txt_subtitle.ValueForeColor = System.Drawing.Color.Blue
    '
    'txt_title
    '
    Me.txt_title.DoubleValue = 0.0R
    Me.txt_title.IntegerValue = 0
    Me.txt_title.IsReadOnly = False
    Me.txt_title.Location = New System.Drawing.Point(121, 18)
    Me.txt_title.Margin = New System.Windows.Forms.Padding(2)
    Me.txt_title.Name = "txt_title"
    Me.txt_title.PlaceHolder = Nothing
    Me.txt_title.Size = New System.Drawing.Size(206, 24)
    Me.txt_title.SufixText = "Sufix Text"
    Me.txt_title.SufixTextVisible = True
    Me.txt_title.TabIndex = 36
    Me.txt_title.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.txt_title.TextValue = ""
    Me.txt_title.TextWidth = 0
    Me.txt_title.Value = ""
    Me.txt_title.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_type
    '
    Me.lbl_type.AutoSize = True
    Me.lbl_type.Location = New System.Drawing.Point(9, 18)
    Me.lbl_type.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
    Me.lbl_type.Name = "lbl_type"
    Me.lbl_type.Size = New System.Drawing.Size(51, 13)
    Me.lbl_type.TabIndex = 54
    Me.lbl_type.Text = "xParent"
    '
    'lbl_content
    '
    Me.lbl_content.AutoSize = True
    Me.lbl_content.Location = New System.Drawing.Point(106, 18)
    Me.lbl_content.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
    Me.lbl_content.Name = "lbl_content"
    Me.lbl_content.Size = New System.Drawing.Size(51, 13)
    Me.lbl_content.TabIndex = 55
    Me.lbl_content.Text = "xParent"
    '
    'gb_childrens
    '
    Me.gb_childrens.Controls.Add(Me.btn_delete_child)
    Me.gb_childrens.Controls.Add(Me.btn_add_child)
    Me.gb_childrens.Controls.Add(Me.uc_grid_childrens)
    Me.gb_childrens.Location = New System.Drawing.Point(5, 478)
    Me.gb_childrens.Name = "gb_childrens"
    Me.gb_childrens.Size = New System.Drawing.Size(1116, 196)
    Me.gb_childrens.TabIndex = 56
    Me.gb_childrens.TabStop = False
    Me.gb_childrens.Text = "GroupBox1"
    '
    'btn_delete_child
    '
    Me.btn_delete_child.Location = New System.Drawing.Point(862, 158)
    Me.btn_delete_child.Name = "btn_delete_child"
    Me.btn_delete_child.Size = New System.Drawing.Size(88, 23)
    Me.btn_delete_child.TabIndex = 2
    Me.btn_delete_child.Text = "xDelete"
    Me.btn_delete_child.UseVisualStyleBackColor = True
    '
    'btn_add_child
    '
    Me.btn_add_child.Location = New System.Drawing.Point(956, 158)
    Me.btn_add_child.Name = "btn_add_child"
    Me.btn_add_child.Size = New System.Drawing.Size(88, 23)
    Me.btn_add_child.TabIndex = 1
    Me.btn_add_child.Text = "xAdd"
    Me.btn_add_child.UseVisualStyleBackColor = True
    '
    'uc_grid_childrens
    '
    Me.uc_grid_childrens.CurrentCol = -1
    Me.uc_grid_childrens.CurrentRow = -1
    Me.uc_grid_childrens.EditableCellBackColor = System.Drawing.Color.Empty
    Me.uc_grid_childrens.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.uc_grid_childrens.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.uc_grid_childrens.Location = New System.Drawing.Point(10, 20)
    Me.uc_grid_childrens.Name = "uc_grid_childrens"
    Me.uc_grid_childrens.PanelRightVisible = True
    Me.uc_grid_childrens.Redraw = True
    Me.uc_grid_childrens.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.uc_grid_childrens.Size = New System.Drawing.Size(1100, 133)
    Me.uc_grid_childrens.Sortable = False
    Me.uc_grid_childrens.SortAscending = True
    Me.uc_grid_childrens.SortByCol = 0
    Me.uc_grid_childrens.TabIndex = 0
    Me.uc_grid_childrens.ToolTipped = True
    Me.uc_grid_childrens.TopRow = -2
    Me.uc_grid_childrens.WordWrap = False
    '
    'frm_new_section_schema_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1309, 691)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Margin = New System.Windows.Forms.Padding(2)
    Me.Name = "frm_new_section_schema_edit"
    Me.Padding = New System.Windows.Forms.Padding(3)
    Me.Text = "fm_new_section_schema_edit"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.gb_superior_level.ResumeLayout(False)
    Me.gb_superior_level.PerformLayout()
    Me.gb_image_reference_1.ResumeLayout(False)
    Me.gb_image_reference_1.PerformLayout()
    CType(Me.pb_image_reference_superior, System.ComponentModel.ISupportInitialize).EndInit()
    Me.gb_items.ResumeLayout(False)
    Me.gb_items.PerformLayout()
    Me.gb_image_reference_3.ResumeLayout(False)
    Me.gb_image_reference_3.PerformLayout()
    CType(Me.pb_image_reference_items, System.ComponentModel.ISupportInitialize).EndInit()
    Me.gb_detail.ResumeLayout(False)
    Me.gb_detail.PerformLayout()
    Me.gb_image_reference_2.ResumeLayout(False)
    Me.gb_image_reference_2.PerformLayout()
    CType(Me.pb_image_reference_detail, System.ComponentModel.ISupportInitialize).EndInit()
    Me.gb_childrens.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents lbl_template As System.Windows.Forms.Label
  Friend WithEvents uc_template As GUI_Controls.uc_combo
  Friend WithEvents lbl_layout As System.Windows.Forms.Label
  Friend WithEvents uc_layout As GUI_Controls.uc_combo
  Friend WithEvents chk_visible As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_section_parent As System.Windows.Forms.Label
  Friend WithEvents lbl_Order As System.Windows.Forms.Label
  Friend WithEvents lbl_name As System.Windows.Forms.Label
  Friend WithEvents ef_order As GUI_Controls.uc_entry_field
  Friend WithEvents uc_section_parent As GUI_Controls.uc_combo
  Friend WithEvents txt_name As GUI_Controls.uc_entry_field
  Friend WithEvents gb_superior_level As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_abstract As System.Windows.Forms.Label
  Friend WithEvents lbl_button_text As System.Windows.Forms.Label
  Friend WithEvents txt_abstract As System.Windows.Forms.TextBox
  Friend WithEvents lbl_subtitle As System.Windows.Forms.Label
  Friend WithEvents lbl_title As System.Windows.Forms.Label
  Friend WithEvents txt_button_text As GUI_Controls.uc_entry_field
  Friend WithEvents txt_main_subtitle As GUI_Controls.uc_entry_field
  Friend WithEvents txt_main_title As GUI_Controls.uc_entry_field
  Friend WithEvents pb_image_reference_superior As System.Windows.Forms.PictureBox
  Friend WithEvents gb_items As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_layout_item As System.Windows.Forms.Label
  Friend WithEvents uc_layout_item As GUI_Controls.uc_combo
  Friend WithEvents gb_detail As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_headline_text As System.Windows.Forms.Label
  Friend WithEvents lbl_main_subtitle As System.Windows.Forms.Label
  Friend WithEvents lbl_main_title As System.Windows.Forms.Label
  Friend WithEvents uc_image_background_detail As GUI_Controls.uc_image
  Friend WithEvents txt_headline_text As GUI_Controls.uc_entry_field
  Friend WithEvents txt_subtitle As GUI_Controls.uc_entry_field
  Friend WithEvents txt_title As GUI_Controls.uc_entry_field
  Friend WithEvents pb_image_reference_detail As System.Windows.Forms.PictureBox
  Friend WithEvents pb_image_reference_items As System.Windows.Forms.PictureBox
  Friend WithEvents uc_image_icon As GUI_Controls.uc_image
  Friend WithEvents gb_image_reference_1 As System.Windows.Forms.GroupBox
  Friend WithEvents gb_image_reference_3 As System.Windows.Forms.GroupBox
  Friend WithEvents gb_image_reference_2 As System.Windows.Forms.GroupBox
  Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
  Friend WithEvents LinkLabel3 As System.Windows.Forms.LinkLabel
  Friend WithEvents LinkLabel2 As System.Windows.Forms.LinkLabel
  Friend WithEvents lbl_type As System.Windows.Forms.Label
  Friend WithEvents lbl_content As System.Windows.Forms.Label
  Friend WithEvents lbl_icon_image As System.Windows.Forms.Label
  Friend WithEvents lbl_back_image As System.Windows.Forms.Label
  Friend WithEvents lbl_description As System.Windows.Forms.Label
  Friend WithEvents txt_description As System.Windows.Forms.TextBox
  Friend WithEvents gb_childrens As System.Windows.Forms.GroupBox
  Friend WithEvents uc_grid_childrens As GUI_Controls.uc_grid
  Friend WithEvents btn_delete_child As System.Windows.Forms.Button
  Friend WithEvents btn_add_child As System.Windows.Forms.Button
End Class
