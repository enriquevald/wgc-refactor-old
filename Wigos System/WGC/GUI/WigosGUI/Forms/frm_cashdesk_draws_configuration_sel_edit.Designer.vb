<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_cashdesk_draws_configuration_sel_edit
  Inherits GUI_Controls.frm_base_sel_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Size = New System.Drawing.Size(590, 92)
    '
    'panel_data
    '
    Me.panel_data.Size = New System.Drawing.Size(590, 373)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(584, 27)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(584, 5)
    '
    'frm_cashdesk_draws_configuration_sel_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(598, 473)
    Me.Name = "frm_cashdesk_draws_configuration_sel_edit"
    Me.Text = "frm_cashdesk_draws_configuration_sel_edit"
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
End Class
