<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_gift_edit
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_available = New System.Windows.Forms.GroupBox
    Me.opt_status_not_available = New System.Windows.Forms.RadioButton
    Me.opt_status_available = New System.Windows.Forms.RadioButton
    Me.ef_gift_name = New GUI_Controls.uc_entry_field
    Me.gb_type = New System.Windows.Forms.GroupBox
    Me.ef_redeem_credit = New GUI_Controls.uc_entry_field
    Me.opt_type_redeem_credit = New System.Windows.Forms.RadioButton
    Me.opt_type_services = New System.Windows.Forms.RadioButton
    Me.ef_current_stock = New GUI_Controls.uc_entry_field
    Me.ef_nrc = New GUI_Controls.uc_entry_field
    Me.opt_type_nrc = New System.Windows.Forms.RadioButton
    Me.opt_type_object = New System.Windows.Forms.RadioButton
    Me.txt_description = New System.Windows.Forms.TextBox
    Me.TabControl1 = New System.Windows.Forms.TabControl
    Me.tab_icon = New System.Windows.Forms.TabPage
    Me.lbl_icon_optimun_size = New System.Windows.Forms.Label
    Me.img_icon_gift = New GUI_Controls.uc_image
    Me.tab_image = New System.Windows.Forms.TabPage
    Me.lbl_image_optimun_size = New System.Windows.Forms.Label
    Me.img_image_gift = New GUI_Controls.uc_image
    Me.TabControl2 = New System.Windows.Forms.TabControl
    Me.tab_by_customer = New System.Windows.Forms.TabPage
    Me.ef_monthly_limit = New GUI_Controls.uc_entry_field
    Me.lbl_monthly_limit = New GUI_Controls.uc_text_field
    Me.ef_daily_limit = New GUI_Controls.uc_entry_field
    Me.lbl_daily_limit = New GUI_Controls.uc_text_field
    Me.tab_by_promotion = New System.Windows.Forms.TabPage
    Me.ef_gift_limit_month = New GUI_Controls.uc_entry_field
    Me.lbl_promotion_monthly_limit = New GUI_Controls.uc_text_field
    Me.ef_gift_limit_day = New GUI_Controls.uc_entry_field
    Me.lbl_promotion_daily_limit = New GUI_Controls.uc_text_field
    Me.lbl_belongs_to_points_to_credit = New GUI_Controls.uc_text_field
    Me.chk_redeem_on_promobox = New System.Windows.Forms.CheckBox
    Me.gb_points = New System.Windows.Forms.GroupBox
    Me.chk_only_vip = New System.Windows.Forms.CheckBox
    Me.lbl_info_points_level = New System.Windows.Forms.Label
    Me.chk_points_level4 = New System.Windows.Forms.CheckBox
    Me.chk_points_level3 = New System.Windows.Forms.CheckBox
    Me.chk_points_level2 = New System.Windows.Forms.CheckBox
    Me.chk_points_level1 = New System.Windows.Forms.CheckBox
    Me.ef_points_level4 = New GUI_Controls.uc_entry_field
    Me.ef_points_level1 = New GUI_Controls.uc_entry_field
    Me.ef_points_level3 = New GUI_Controls.uc_entry_field
    Me.ef_points_level2 = New GUI_Controls.uc_entry_field
    Me.ef_text_on_promobox = New GUI_Controls.uc_entry_field
    Me.TabControl3 = New System.Windows.Forms.TabControl
    Me.tab_description = New System.Windows.Forms.TabPage
    Me.tab_promobox = New System.Windows.Forms.TabPage
    Me.panel_data.SuspendLayout()
    Me.gb_available.SuspendLayout()
    Me.gb_type.SuspendLayout()
    Me.TabControl1.SuspendLayout()
    Me.tab_icon.SuspendLayout()
    Me.tab_image.SuspendLayout()
    Me.TabControl2.SuspendLayout()
    Me.tab_by_customer.SuspendLayout()
    Me.tab_by_promotion.SuspendLayout()
    Me.gb_points.SuspendLayout()
    Me.TabControl3.SuspendLayout()
    Me.tab_description.SuspendLayout()
    Me.tab_promobox.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.panel_data.Controls.Add(Me.TabControl3)
    Me.panel_data.Controls.Add(Me.gb_points)
    Me.panel_data.Controls.Add(Me.lbl_belongs_to_points_to_credit)
    Me.panel_data.Controls.Add(Me.TabControl2)
    Me.panel_data.Controls.Add(Me.TabControl1)
    Me.panel_data.Controls.Add(Me.gb_type)
    Me.panel_data.Controls.Add(Me.ef_gift_name)
    Me.panel_data.Controls.Add(Me.gb_available)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(923, 563)
    '
    'gb_available
    '
    Me.gb_available.Controls.Add(Me.opt_status_not_available)
    Me.gb_available.Controls.Add(Me.opt_status_available)
    Me.gb_available.Location = New System.Drawing.Point(15, 51)
    Me.gb_available.Name = "gb_available"
    Me.gb_available.Size = New System.Drawing.Size(136, 46)
    Me.gb_available.TabIndex = 1
    Me.gb_available.TabStop = False
    Me.gb_available.Text = "xAvailable"
    '
    'opt_status_not_available
    '
    Me.opt_status_not_available.Location = New System.Drawing.Point(80, 21)
    Me.opt_status_not_available.Name = "opt_status_not_available"
    Me.opt_status_not_available.Size = New System.Drawing.Size(50, 16)
    Me.opt_status_not_available.TabIndex = 1
    Me.opt_status_not_available.Text = "xNo"
    '
    'opt_status_available
    '
    Me.opt_status_available.Location = New System.Drawing.Point(17, 21)
    Me.opt_status_available.Name = "opt_status_available"
    Me.opt_status_available.Size = New System.Drawing.Size(52, 16)
    Me.opt_status_available.TabIndex = 0
    Me.opt_status_available.TabStop = True
    Me.opt_status_available.Text = "xYes"
    '
    'ef_gift_name
    '
    Me.ef_gift_name.DoubleValue = 0
    Me.ef_gift_name.IntegerValue = 0
    Me.ef_gift_name.IsReadOnly = False
    Me.ef_gift_name.Location = New System.Drawing.Point(3, 20)
    Me.ef_gift_name.Name = "ef_gift_name"
    Me.ef_gift_name.Size = New System.Drawing.Size(485, 25)
    Me.ef_gift_name.SufixText = "Sufix Text"
    Me.ef_gift_name.SufixTextVisible = True
    Me.ef_gift_name.TabIndex = 0
    Me.ef_gift_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_gift_name.TextValue = ""
    Me.ef_gift_name.TextWidth = 87
    Me.ef_gift_name.Value = ""
    Me.ef_gift_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_type
    '
    Me.gb_type.Controls.Add(Me.ef_redeem_credit)
    Me.gb_type.Controls.Add(Me.opt_type_redeem_credit)
    Me.gb_type.Controls.Add(Me.opt_type_services)
    Me.gb_type.Controls.Add(Me.ef_current_stock)
    Me.gb_type.Controls.Add(Me.ef_nrc)
    Me.gb_type.Controls.Add(Me.opt_type_nrc)
    Me.gb_type.Controls.Add(Me.opt_type_object)
    Me.gb_type.Location = New System.Drawing.Point(15, 272)
    Me.gb_type.Name = "gb_type"
    Me.gb_type.Size = New System.Drawing.Size(463, 130)
    Me.gb_type.TabIndex = 5
    Me.gb_type.TabStop = False
    Me.gb_type.Text = "xType"
    '
    'ef_redeem_credit
    '
    Me.ef_redeem_credit.DoubleValue = 0
    Me.ef_redeem_credit.IntegerValue = 0
    Me.ef_redeem_credit.IsReadOnly = False
    Me.ef_redeem_credit.Location = New System.Drawing.Point(353, 74)
    Me.ef_redeem_credit.Name = "ef_redeem_credit"
    Me.ef_redeem_credit.Size = New System.Drawing.Size(98, 24)
    Me.ef_redeem_credit.SufixText = "Sufix Text"
    Me.ef_redeem_credit.SufixTextVisible = True
    Me.ef_redeem_credit.TabIndex = 6
    Me.ef_redeem_credit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_redeem_credit.TextValue = ""
    Me.ef_redeem_credit.TextVisible = False
    Me.ef_redeem_credit.TextWidth = 0
    Me.ef_redeem_credit.Value = ""
    Me.ef_redeem_credit.ValueForeColor = System.Drawing.Color.Blue
    '
    'opt_type_redeem_credit
    '
    Me.opt_type_redeem_credit.Location = New System.Drawing.Point(13, 77)
    Me.opt_type_redeem_credit.Name = "opt_type_redeem_credit"
    Me.opt_type_redeem_credit.Size = New System.Drawing.Size(183, 21)
    Me.opt_type_redeem_credit.TabIndex = 2
    Me.opt_type_redeem_credit.Text = "xRedeemable Credits"
    '
    'opt_type_services
    '
    Me.opt_type_services.Location = New System.Drawing.Point(13, 102)
    Me.opt_type_services.Name = "opt_type_services"
    Me.opt_type_services.Size = New System.Drawing.Size(183, 21)
    Me.opt_type_services.TabIndex = 3
    Me.opt_type_services.Text = "xServices"
    '
    'ef_current_stock
    '
    Me.ef_current_stock.DoubleValue = 0
    Me.ef_current_stock.IntegerValue = 0
    Me.ef_current_stock.IsReadOnly = False
    Me.ef_current_stock.Location = New System.Drawing.Point(273, 20)
    Me.ef_current_stock.Name = "ef_current_stock"
    Me.ef_current_stock.Size = New System.Drawing.Size(178, 24)
    Me.ef_current_stock.SufixText = "Sufix Text"
    Me.ef_current_stock.SufixTextVisible = True
    Me.ef_current_stock.TabIndex = 4
    Me.ef_current_stock.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_current_stock.TextValue = ""
    Me.ef_current_stock.Value = ""
    Me.ef_current_stock.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_nrc
    '
    Me.ef_nrc.DoubleValue = 0
    Me.ef_nrc.IntegerValue = 0
    Me.ef_nrc.IsReadOnly = False
    Me.ef_nrc.Location = New System.Drawing.Point(353, 47)
    Me.ef_nrc.Name = "ef_nrc"
    Me.ef_nrc.Size = New System.Drawing.Size(98, 24)
    Me.ef_nrc.SufixText = "Sufix Text"
    Me.ef_nrc.SufixTextVisible = True
    Me.ef_nrc.TabIndex = 5
    Me.ef_nrc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_nrc.TextValue = ""
    Me.ef_nrc.TextVisible = False
    Me.ef_nrc.TextWidth = 0
    Me.ef_nrc.Value = ""
    Me.ef_nrc.ValueForeColor = System.Drawing.Color.Blue
    '
    'opt_type_nrc
    '
    Me.opt_type_nrc.Location = New System.Drawing.Point(13, 50)
    Me.opt_type_nrc.Name = "opt_type_nrc"
    Me.opt_type_nrc.Size = New System.Drawing.Size(183, 21)
    Me.opt_type_nrc.TabIndex = 1
    Me.opt_type_nrc.Text = "xNot Redeemable Credits"
    '
    'opt_type_object
    '
    Me.opt_type_object.Location = New System.Drawing.Point(13, 23)
    Me.opt_type_object.Name = "opt_type_object"
    Me.opt_type_object.Size = New System.Drawing.Size(183, 21)
    Me.opt_type_object.TabIndex = 0
    Me.opt_type_object.TabStop = True
    Me.opt_type_object.Text = "xObject"
    '
    'txt_description
    '
    Me.txt_description.Location = New System.Drawing.Point(3, 3)
    Me.txt_description.MaxLength = 200
    Me.txt_description.Multiline = True
    Me.txt_description.Name = "txt_description"
    Me.txt_description.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.txt_description.Size = New System.Drawing.Size(449, 113)
    Me.txt_description.TabIndex = 7
    '
    'TabControl1
    '
    Me.TabControl1.Controls.Add(Me.tab_icon)
    Me.TabControl1.Controls.Add(Me.tab_image)
    Me.TabControl1.Location = New System.Drawing.Point(490, 6)
    Me.TabControl1.Name = "TabControl1"
    Me.TabControl1.SelectedIndex = 0
    Me.TabControl1.Size = New System.Drawing.Size(426, 407)
    Me.TabControl1.TabIndex = 8
    '
    'tab_icon
    '
    Me.tab_icon.Controls.Add(Me.lbl_icon_optimun_size)
    Me.tab_icon.Controls.Add(Me.img_icon_gift)
    Me.tab_icon.Location = New System.Drawing.Point(4, 22)
    Me.tab_icon.Name = "tab_icon"
    Me.tab_icon.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_icon.Size = New System.Drawing.Size(418, 381)
    Me.tab_icon.TabIndex = 0
    Me.tab_icon.Text = "xIcon"
    Me.tab_icon.UseVisualStyleBackColor = True
    '
    'lbl_icon_optimun_size
    '
    Me.lbl_icon_optimun_size.Font = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_icon_optimun_size.Location = New System.Drawing.Point(9, 3)
    Me.lbl_icon_optimun_size.Name = "lbl_icon_optimun_size"
    Me.lbl_icon_optimun_size.Size = New System.Drawing.Size(205, 18)
    Me.lbl_icon_optimun_size.TabIndex = 0
    Me.lbl_icon_optimun_size.Text = "xOptimunSize"
    '
    'img_icon_gift
    '
    Me.img_icon_gift.AutoSize = True
    Me.img_icon_gift.ButtonDeleteEnabled = True
    Me.img_icon_gift.FreeResize = False
    Me.img_icon_gift.Image = Nothing
    Me.img_icon_gift.ImageInfoFont = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.img_icon_gift.ImageLayout = System.Windows.Forms.ImageLayout.Center
    Me.img_icon_gift.ImageName = Nothing
    Me.img_icon_gift.Location = New System.Drawing.Point(111, 70)
    Me.img_icon_gift.Margin = New System.Windows.Forms.Padding(0)
    Me.img_icon_gift.Name = "img_icon_gift"
    Me.img_icon_gift.Size = New System.Drawing.Size(203, 191)
    Me.img_icon_gift.TabIndex = 1
    '
    'tab_image
    '
    Me.tab_image.Controls.Add(Me.lbl_image_optimun_size)
    Me.tab_image.Controls.Add(Me.img_image_gift)
    Me.tab_image.Location = New System.Drawing.Point(4, 22)
    Me.tab_image.Name = "tab_image"
    Me.tab_image.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_image.Size = New System.Drawing.Size(418, 381)
    Me.tab_image.TabIndex = 1
    Me.tab_image.Text = "xImage"
    Me.tab_image.UseVisualStyleBackColor = True
    '
    'lbl_image_optimun_size
    '
    Me.lbl_image_optimun_size.Font = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_image_optimun_size.Location = New System.Drawing.Point(9, 3)
    Me.lbl_image_optimun_size.Name = "lbl_image_optimun_size"
    Me.lbl_image_optimun_size.Size = New System.Drawing.Size(403, 13)
    Me.lbl_image_optimun_size.TabIndex = 26
    Me.lbl_image_optimun_size.Text = "xOptimunSize"
    '
    'img_image_gift
    '
    Me.img_image_gift.AutoSize = True
    Me.img_image_gift.ButtonDeleteEnabled = True
    Me.img_image_gift.FreeResize = False
    Me.img_image_gift.Image = Nothing
    Me.img_image_gift.ImageInfoFont = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.img_image_gift.ImageLayout = System.Windows.Forms.ImageLayout.Zoom
    Me.img_image_gift.ImageName = Nothing
    Me.img_image_gift.Location = New System.Drawing.Point(9, 20)
    Me.img_image_gift.Margin = New System.Windows.Forms.Padding(0)
    Me.img_image_gift.Name = "img_image_gift"
    Me.img_image_gift.Size = New System.Drawing.Size(403, 360)
    Me.img_image_gift.TabIndex = 9
    '
    'TabControl2
    '
    Me.TabControl2.Controls.Add(Me.tab_by_customer)
    Me.TabControl2.Controls.Add(Me.tab_by_promotion)
    Me.TabControl2.Location = New System.Drawing.Point(490, 416)
    Me.TabControl2.Name = "TabControl2"
    Me.TabControl2.SelectedIndex = 0
    Me.TabControl2.Size = New System.Drawing.Size(422, 137)
    Me.TabControl2.TabIndex = 9
    '
    'tab_by_customer
    '
    Me.tab_by_customer.Controls.Add(Me.ef_monthly_limit)
    Me.tab_by_customer.Controls.Add(Me.lbl_monthly_limit)
    Me.tab_by_customer.Controls.Add(Me.ef_daily_limit)
    Me.tab_by_customer.Controls.Add(Me.lbl_daily_limit)
    Me.tab_by_customer.Location = New System.Drawing.Point(4, 22)
    Me.tab_by_customer.Name = "tab_by_customer"
    Me.tab_by_customer.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_by_customer.Size = New System.Drawing.Size(414, 111)
    Me.tab_by_customer.TabIndex = 0
    Me.tab_by_customer.Text = "xLimitCustomer"
    Me.tab_by_customer.UseVisualStyleBackColor = True
    '
    'ef_monthly_limit
    '
    Me.ef_monthly_limit.DoubleValue = 0
    Me.ef_monthly_limit.IntegerValue = 0
    Me.ef_monthly_limit.IsReadOnly = False
    Me.ef_monthly_limit.Location = New System.Drawing.Point(9, 57)
    Me.ef_monthly_limit.Name = "ef_monthly_limit"
    Me.ef_monthly_limit.Size = New System.Drawing.Size(200, 24)
    Me.ef_monthly_limit.SufixText = "Sufix Text"
    Me.ef_monthly_limit.SufixTextVisible = True
    Me.ef_monthly_limit.TabIndex = 2
    Me.ef_monthly_limit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_monthly_limit.TextValue = "0"
    Me.ef_monthly_limit.TextVisible = False
    Me.ef_monthly_limit.TextWidth = 110
    Me.ef_monthly_limit.Value = "0"
    Me.ef_monthly_limit.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_monthly_limit
    '
    Me.lbl_monthly_limit.AutoSize = True
    Me.lbl_monthly_limit.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_monthly_limit.IsReadOnly = True
    Me.lbl_monthly_limit.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_monthly_limit.LabelForeColor = System.Drawing.Color.Navy
    Me.lbl_monthly_limit.Location = New System.Drawing.Point(8, 81)
    Me.lbl_monthly_limit.Name = "lbl_monthly_limit"
    Me.lbl_monthly_limit.Size = New System.Drawing.Size(429, 24)
    Me.lbl_monthly_limit.SufixText = "Sufix Text"
    Me.lbl_monthly_limit.SufixTextVisible = True
    Me.lbl_monthly_limit.TabIndex = 3
    Me.lbl_monthly_limit.TabStop = False
    Me.lbl_monthly_limit.TextVisible = False
    Me.lbl_monthly_limit.TextWidth = 0
    Me.lbl_monthly_limit.Value = "x[] Maximum per customer, per month"
    '
    'ef_daily_limit
    '
    Me.ef_daily_limit.DoubleValue = 0
    Me.ef_daily_limit.IntegerValue = 0
    Me.ef_daily_limit.IsReadOnly = False
    Me.ef_daily_limit.Location = New System.Drawing.Point(9, 6)
    Me.ef_daily_limit.Name = "ef_daily_limit"
    Me.ef_daily_limit.Size = New System.Drawing.Size(200, 24)
    Me.ef_daily_limit.SufixText = "Sufix Text"
    Me.ef_daily_limit.SufixTextVisible = True
    Me.ef_daily_limit.TabIndex = 0
    Me.ef_daily_limit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_daily_limit.TextValue = "0"
    Me.ef_daily_limit.TextVisible = False
    Me.ef_daily_limit.TextWidth = 110
    Me.ef_daily_limit.Value = "0"
    Me.ef_daily_limit.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_daily_limit
    '
    Me.lbl_daily_limit.AutoSize = True
    Me.lbl_daily_limit.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_daily_limit.ForeColor = System.Drawing.Color.Navy
    Me.lbl_daily_limit.IsReadOnly = True
    Me.lbl_daily_limit.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_daily_limit.LabelForeColor = System.Drawing.Color.Navy
    Me.lbl_daily_limit.Location = New System.Drawing.Point(9, 31)
    Me.lbl_daily_limit.Name = "lbl_daily_limit"
    Me.lbl_daily_limit.Size = New System.Drawing.Size(428, 24)
    Me.lbl_daily_limit.SufixText = "Sufix Text"
    Me.lbl_daily_limit.SufixTextVisible = True
    Me.lbl_daily_limit.TabIndex = 1
    Me.lbl_daily_limit.TabStop = False
    Me.lbl_daily_limit.TextVisible = False
    Me.lbl_daily_limit.TextWidth = 0
    Me.lbl_daily_limit.Value = "x[] Maximum per customer, per day"
    '
    'tab_by_promotion
    '
    Me.tab_by_promotion.Controls.Add(Me.ef_gift_limit_month)
    Me.tab_by_promotion.Controls.Add(Me.lbl_promotion_monthly_limit)
    Me.tab_by_promotion.Controls.Add(Me.ef_gift_limit_day)
    Me.tab_by_promotion.Controls.Add(Me.lbl_promotion_daily_limit)
    Me.tab_by_promotion.Location = New System.Drawing.Point(4, 22)
    Me.tab_by_promotion.Name = "tab_by_promotion"
    Me.tab_by_promotion.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_by_promotion.Size = New System.Drawing.Size(414, 111)
    Me.tab_by_promotion.TabIndex = 2
    Me.tab_by_promotion.Text = "xLimitPromotion"
    Me.tab_by_promotion.UseVisualStyleBackColor = True
    '
    'ef_gift_limit_month
    '
    Me.ef_gift_limit_month.DoubleValue = 0
    Me.ef_gift_limit_month.IntegerValue = 0
    Me.ef_gift_limit_month.IsReadOnly = False
    Me.ef_gift_limit_month.Location = New System.Drawing.Point(9, 57)
    Me.ef_gift_limit_month.Name = "ef_gift_limit_month"
    Me.ef_gift_limit_month.Size = New System.Drawing.Size(200, 24)
    Me.ef_gift_limit_month.SufixText = "Sufix Text"
    Me.ef_gift_limit_month.TabIndex = 2
    Me.ef_gift_limit_month.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_gift_limit_month.TextValue = "0"
    Me.ef_gift_limit_month.TextVisible = False
    Me.ef_gift_limit_month.TextWidth = 110
    Me.ef_gift_limit_month.Value = "0"
    Me.ef_gift_limit_month.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_promotion_monthly_limit
    '
    Me.lbl_promotion_monthly_limit.AutoSize = True
    Me.lbl_promotion_monthly_limit.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_promotion_monthly_limit.IsReadOnly = True
    Me.lbl_promotion_monthly_limit.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_promotion_monthly_limit.LabelForeColor = System.Drawing.Color.Navy
    Me.lbl_promotion_monthly_limit.Location = New System.Drawing.Point(8, 81)
    Me.lbl_promotion_monthly_limit.Name = "lbl_promotion_monthly_limit"
    Me.lbl_promotion_monthly_limit.Size = New System.Drawing.Size(429, 24)
    Me.lbl_promotion_monthly_limit.SufixText = "Sufix Text"
    Me.lbl_promotion_monthly_limit.TabIndex = 3
    Me.lbl_promotion_monthly_limit.TabStop = False
    Me.lbl_promotion_monthly_limit.TextVisible = False
    Me.lbl_promotion_monthly_limit.TextWidth = 0
    Me.lbl_promotion_monthly_limit.Value = "x[] Maximum per promotion, per month"
    '
    'ef_gift_limit_day
    '
    Me.ef_gift_limit_day.DoubleValue = 0
    Me.ef_gift_limit_day.IntegerValue = 0
    Me.ef_gift_limit_day.IsReadOnly = False
    Me.ef_gift_limit_day.Location = New System.Drawing.Point(9, 6)
    Me.ef_gift_limit_day.Name = "ef_gift_limit_day"
    Me.ef_gift_limit_day.Size = New System.Drawing.Size(200, 24)
    Me.ef_gift_limit_day.SufixText = "Sufix Text"
    Me.ef_gift_limit_day.TabIndex = 0
    Me.ef_gift_limit_day.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_gift_limit_day.TextValue = "0"
    Me.ef_gift_limit_day.TextVisible = False
    Me.ef_gift_limit_day.TextWidth = 110
    Me.ef_gift_limit_day.Value = "0"
    Me.ef_gift_limit_day.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_promotion_daily_limit
    '
    Me.lbl_promotion_daily_limit.AutoSize = True
    Me.lbl_promotion_daily_limit.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_promotion_daily_limit.IsReadOnly = True
    Me.lbl_promotion_daily_limit.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_promotion_daily_limit.LabelForeColor = System.Drawing.Color.Navy
    Me.lbl_promotion_daily_limit.Location = New System.Drawing.Point(9, 31)
    Me.lbl_promotion_daily_limit.Name = "lbl_promotion_daily_limit"
    Me.lbl_promotion_daily_limit.Size = New System.Drawing.Size(428, 24)
    Me.lbl_promotion_daily_limit.SufixText = "Sufix Text"
    Me.lbl_promotion_daily_limit.TabIndex = 1
    Me.lbl_promotion_daily_limit.TabStop = False
    Me.lbl_promotion_daily_limit.TextVisible = False
    Me.lbl_promotion_daily_limit.TextWidth = 0
    Me.lbl_promotion_daily_limit.Value = "x[] Maximum per promotion, per day"
    '
    'lbl_belongs_to_points_to_credit
    '
    Me.lbl_belongs_to_points_to_credit.IsReadOnly = True
    Me.lbl_belongs_to_points_to_credit.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_belongs_to_points_to_credit.LabelForeColor = System.Drawing.Color.Blue
    Me.lbl_belongs_to_points_to_credit.Location = New System.Drawing.Point(15, 62)
    Me.lbl_belongs_to_points_to_credit.Name = "lbl_belongs_to_points_to_credit"
    Me.lbl_belongs_to_points_to_credit.Size = New System.Drawing.Size(469, 24)
    Me.lbl_belongs_to_points_to_credit.SufixText = "Sufix Text"
    Me.lbl_belongs_to_points_to_credit.SufixTextVisible = True
    Me.lbl_belongs_to_points_to_credit.TabIndex = 11
    Me.lbl_belongs_to_points_to_credit.TextWidth = 0
    Me.lbl_belongs_to_points_to_credit.Value = """xBelongs To 'Points To Credit'"
    '
    'chk_redeem_on_promobox
    '
    Me.chk_redeem_on_promobox.AutoSize = True
    Me.chk_redeem_on_promobox.Location = New System.Drawing.Point(47, 27)
    Me.chk_redeem_on_promobox.Name = "chk_redeem_on_promobox"
    Me.chk_redeem_on_promobox.Size = New System.Drawing.Size(155, 17)
    Me.chk_redeem_on_promobox.TabIndex = 2
    Me.chk_redeem_on_promobox.Text = "xRedeemOnPromoBox"
    Me.chk_redeem_on_promobox.UseVisualStyleBackColor = True
    '
    'gb_points
    '
    Me.gb_points.Controls.Add(Me.chk_only_vip)
    Me.gb_points.Controls.Add(Me.lbl_info_points_level)
    Me.gb_points.Controls.Add(Me.chk_points_level4)
    Me.gb_points.Controls.Add(Me.chk_points_level3)
    Me.gb_points.Controls.Add(Me.chk_points_level2)
    Me.gb_points.Controls.Add(Me.chk_points_level1)
    Me.gb_points.Controls.Add(Me.ef_points_level4)
    Me.gb_points.Controls.Add(Me.ef_points_level1)
    Me.gb_points.Controls.Add(Me.ef_points_level3)
    Me.gb_points.Controls.Add(Me.ef_points_level2)
    Me.gb_points.Location = New System.Drawing.Point(15, 103)
    Me.gb_points.Name = "gb_points"
    Me.gb_points.Size = New System.Drawing.Size(463, 162)
    Me.gb_points.TabIndex = 4
    Me.gb_points.TabStop = False
    Me.gb_points.Text = "xPointsLevel"
    '
    'chk_only_vip
    '
    Me.chk_only_vip.AutoSize = True
    Me.chk_only_vip.Location = New System.Drawing.Point(13, 133)
    Me.chk_only_vip.Name = "chk_only_vip"
    Me.chk_only_vip.Size = New System.Drawing.Size(83, 17)
    Me.chk_only_vip.TabIndex = 10
    Me.chk_only_vip.Text = "xOnlyVips"
    Me.chk_only_vip.UseVisualStyleBackColor = True
    '
    'lbl_info_points_level
    '
    Me.lbl_info_points_level.Font = New System.Drawing.Font("Courier New", 8.25!)
    Me.lbl_info_points_level.ForeColor = System.Drawing.Color.Navy
    Me.lbl_info_points_level.Location = New System.Drawing.Point(13, 94)
    Me.lbl_info_points_level.Name = "lbl_info_points_level"
    Me.lbl_info_points_level.Size = New System.Drawing.Size(439, 35)
    Me.lbl_info_points_level.TabIndex = 8
    Me.lbl_info_points_level.Text = "xDescription of group box"
    '
    'chk_points_level4
    '
    Me.chk_points_level4.AutoSize = True
    Me.chk_points_level4.Location = New System.Drawing.Point(245, 61)
    Me.chk_points_level4.Name = "chk_points_level4"
    Me.chk_points_level4.Size = New System.Drawing.Size(70, 17)
    Me.chk_points_level4.TabIndex = 6
    Me.chk_points_level4.Text = "xLevel4"
    Me.chk_points_level4.UseVisualStyleBackColor = True
    '
    'chk_points_level3
    '
    Me.chk_points_level3.AutoSize = True
    Me.chk_points_level3.Location = New System.Drawing.Point(245, 30)
    Me.chk_points_level3.Name = "chk_points_level3"
    Me.chk_points_level3.Size = New System.Drawing.Size(70, 17)
    Me.chk_points_level3.TabIndex = 4
    Me.chk_points_level3.Text = "xLevel3"
    Me.chk_points_level3.UseVisualStyleBackColor = True
    '
    'chk_points_level2
    '
    Me.chk_points_level2.AutoSize = True
    Me.chk_points_level2.Location = New System.Drawing.Point(13, 61)
    Me.chk_points_level2.Name = "chk_points_level2"
    Me.chk_points_level2.Size = New System.Drawing.Size(70, 17)
    Me.chk_points_level2.TabIndex = 2
    Me.chk_points_level2.Text = "xLevel2"
    Me.chk_points_level2.UseVisualStyleBackColor = True
    '
    'chk_points_level1
    '
    Me.chk_points_level1.AutoSize = True
    Me.chk_points_level1.Location = New System.Drawing.Point(13, 30)
    Me.chk_points_level1.Name = "chk_points_level1"
    Me.chk_points_level1.Size = New System.Drawing.Size(70, 17)
    Me.chk_points_level1.TabIndex = 0
    Me.chk_points_level1.Text = "xLevel1"
    Me.chk_points_level1.UseVisualStyleBackColor = True
    '
    'ef_points_level4
    '
    Me.ef_points_level4.DoubleValue = 0
    Me.ef_points_level4.IntegerValue = 0
    Me.ef_points_level4.IsReadOnly = False
    Me.ef_points_level4.Location = New System.Drawing.Point(354, 56)
    Me.ef_points_level4.Name = "ef_points_level4"
    Me.ef_points_level4.Size = New System.Drawing.Size(98, 24)
    Me.ef_points_level4.SufixText = "Sufix Text"
    Me.ef_points_level4.SufixTextVisible = True
    Me.ef_points_level4.TabIndex = 7
    Me.ef_points_level4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_points_level4.TextValue = ""
    Me.ef_points_level4.TextWidth = 0
    Me.ef_points_level4.Value = ""
    Me.ef_points_level4.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_points_level1
    '
    Me.ef_points_level1.DoubleValue = 0
    Me.ef_points_level1.IntegerValue = 0
    Me.ef_points_level1.IsReadOnly = False
    Me.ef_points_level1.Location = New System.Drawing.Point(122, 25)
    Me.ef_points_level1.Name = "ef_points_level1"
    Me.ef_points_level1.Size = New System.Drawing.Size(98, 24)
    Me.ef_points_level1.SufixText = "Sufix Text"
    Me.ef_points_level1.SufixTextVisible = True
    Me.ef_points_level1.TabIndex = 1
    Me.ef_points_level1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_points_level1.TextValue = ""
    Me.ef_points_level1.TextWidth = 0
    Me.ef_points_level1.Value = ""
    Me.ef_points_level1.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_points_level3
    '
    Me.ef_points_level3.DoubleValue = 0
    Me.ef_points_level3.IntegerValue = 0
    Me.ef_points_level3.IsReadOnly = False
    Me.ef_points_level3.Location = New System.Drawing.Point(354, 25)
    Me.ef_points_level3.Name = "ef_points_level3"
    Me.ef_points_level3.Size = New System.Drawing.Size(98, 24)
    Me.ef_points_level3.SufixText = "Sufix Text"
    Me.ef_points_level3.SufixTextVisible = True
    Me.ef_points_level3.TabIndex = 5
    Me.ef_points_level3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_points_level3.TextValue = ""
    Me.ef_points_level3.TextWidth = 0
    Me.ef_points_level3.Value = ""
    Me.ef_points_level3.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_points_level2
    '
    Me.ef_points_level2.DoubleValue = 0
    Me.ef_points_level2.IntegerValue = 0
    Me.ef_points_level2.IsReadOnly = False
    Me.ef_points_level2.Location = New System.Drawing.Point(122, 56)
    Me.ef_points_level2.Name = "ef_points_level2"
    Me.ef_points_level2.Size = New System.Drawing.Size(98, 24)
    Me.ef_points_level2.SufixText = "Sufix Text"
    Me.ef_points_level2.SufixTextVisible = True
    Me.ef_points_level2.TabIndex = 3
    Me.ef_points_level2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_points_level2.TextValue = ""
    Me.ef_points_level2.TextWidth = 0
    Me.ef_points_level2.Value = ""
    Me.ef_points_level2.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_text_on_promobox
    '
    Me.ef_text_on_promobox.DoubleValue = 0
    Me.ef_text_on_promobox.IntegerValue = 0
    Me.ef_text_on_promobox.IsReadOnly = False
    Me.ef_text_on_promobox.Location = New System.Drawing.Point(43, 58)
    Me.ef_text_on_promobox.Name = "ef_text_on_promobox"
    Me.ef_text_on_promobox.Size = New System.Drawing.Size(364, 24)
    Me.ef_text_on_promobox.SufixText = "Sufix Text"
    Me.ef_text_on_promobox.TabIndex = 3
    Me.ef_text_on_promobox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_text_on_promobox.TextValue = ""
    Me.ef_text_on_promobox.TextVisible = False
    Me.ef_text_on_promobox.TextWidth = 100
    Me.ef_text_on_promobox.Value = ""
    Me.ef_text_on_promobox.ValueForeColor = System.Drawing.Color.Blue
    '
    'TabControl3
    '
    Me.TabControl3.Controls.Add(Me.tab_description)
    Me.TabControl3.Controls.Add(Me.tab_promobox)
    Me.TabControl3.Location = New System.Drawing.Point(15, 408)
    Me.TabControl3.Name = "TabControl3"
    Me.TabControl3.SelectedIndex = 0
    Me.TabControl3.Size = New System.Drawing.Size(463, 145)
    Me.TabControl3.TabIndex = 12
    '
    'tab_description
    '
    Me.tab_description.Controls.Add(Me.txt_description)
    Me.tab_description.Location = New System.Drawing.Point(4, 22)
    Me.tab_description.Name = "tab_description"
    Me.tab_description.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_description.Size = New System.Drawing.Size(455, 119)
    Me.tab_description.TabIndex = 0
    Me.tab_description.Text = "xDescription"
    Me.tab_description.UseVisualStyleBackColor = True
    '
    'tab_promobox
    '
    Me.tab_promobox.Controls.Add(Me.chk_redeem_on_promobox)
    Me.tab_promobox.Controls.Add(Me.ef_text_on_promobox)
    Me.tab_promobox.Location = New System.Drawing.Point(4, 22)
    Me.tab_promobox.Name = "tab_promobox"
    Me.tab_promobox.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_promobox.Size = New System.Drawing.Size(455, 119)
    Me.tab_promobox.TabIndex = 2
    Me.tab_promobox.Text = "xPromoBOX"
    Me.tab_promobox.UseVisualStyleBackColor = True
    '
    'frm_gift_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1046, 571)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_gift_edit"
    Me.Text = "frm_gift_edit"
    Me.panel_data.ResumeLayout(False)
    Me.gb_available.ResumeLayout(False)
    Me.gb_type.ResumeLayout(False)
    Me.TabControl1.ResumeLayout(False)
    Me.tab_icon.ResumeLayout(False)
    Me.tab_icon.PerformLayout()
    Me.tab_image.ResumeLayout(False)
    Me.tab_image.PerformLayout()
    Me.TabControl2.ResumeLayout(False)
    Me.tab_by_customer.ResumeLayout(False)
    Me.tab_by_customer.PerformLayout()
    Me.tab_by_promotion.ResumeLayout(False)
    Me.tab_by_promotion.PerformLayout()
    Me.gb_points.ResumeLayout(False)
    Me.gb_points.PerformLayout()
    Me.TabControl3.ResumeLayout(False)
    Me.tab_description.ResumeLayout(False)
    Me.tab_description.PerformLayout()
    Me.tab_promobox.ResumeLayout(False)
    Me.tab_promobox.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_available As System.Windows.Forms.GroupBox
  Friend WithEvents opt_status_not_available As System.Windows.Forms.RadioButton
  Friend WithEvents opt_status_available As System.Windows.Forms.RadioButton
  Friend WithEvents ef_gift_name As GUI_Controls.uc_entry_field
  Friend WithEvents gb_type As System.Windows.Forms.GroupBox
  Friend WithEvents opt_type_nrc As System.Windows.Forms.RadioButton
  Friend WithEvents opt_type_object As System.Windows.Forms.RadioButton
  Friend WithEvents ef_nrc As GUI_Controls.uc_entry_field
  Friend WithEvents ef_current_stock As GUI_Controls.uc_entry_field
  Friend WithEvents opt_type_services As System.Windows.Forms.RadioButton
  Friend WithEvents opt_type_redeem_credit As System.Windows.Forms.RadioButton
  Friend WithEvents ef_redeem_credit As GUI_Controls.uc_entry_field
  Friend WithEvents txt_description As System.Windows.Forms.TextBox
  Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
  Friend WithEvents tab_icon As System.Windows.Forms.TabPage
  Friend WithEvents tab_image As System.Windows.Forms.TabPage
  Friend WithEvents img_image_gift As GUI_Controls.uc_image
  Friend WithEvents img_icon_gift As GUI_Controls.uc_image
  Friend WithEvents lbl_icon_optimun_size As System.Windows.Forms.Label
  Friend WithEvents lbl_image_optimun_size As System.Windows.Forms.Label
  Friend WithEvents TabControl2 As System.Windows.Forms.TabControl
  Friend WithEvents tab_by_customer As System.Windows.Forms.TabPage
  Friend WithEvents ef_monthly_limit As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_monthly_limit As GUI_Controls.uc_text_field
  Friend WithEvents ef_daily_limit As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_daily_limit As GUI_Controls.uc_text_field
  Friend WithEvents tab_by_promotion As System.Windows.Forms.TabPage
  Friend WithEvents ef_gift_limit_month As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_promotion_monthly_limit As GUI_Controls.uc_text_field
  Friend WithEvents ef_gift_limit_day As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_promotion_daily_limit As GUI_Controls.uc_text_field
  Friend WithEvents lbl_belongs_to_points_to_credit As GUI_Controls.uc_text_field
  Friend WithEvents chk_redeem_on_promobox As System.Windows.Forms.CheckBox
  Friend WithEvents gb_points As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_info_points_level As System.Windows.Forms.Label
  Friend WithEvents chk_points_level4 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_points_level3 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_points_level2 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_points_level1 As System.Windows.Forms.CheckBox
  Friend WithEvents ef_points_level4 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_points_level1 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_points_level3 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_points_level2 As GUI_Controls.uc_entry_field
  Friend WithEvents chk_only_vip As System.Windows.Forms.CheckBox
  Friend WithEvents TabControl3 As System.Windows.Forms.TabControl
  Friend WithEvents tab_description As System.Windows.Forms.TabPage
  Friend WithEvents tab_promobox As System.Windows.Forms.TabPage
  Friend WithEvents ef_text_on_promobox As GUI_Controls.uc_entry_field
End Class
