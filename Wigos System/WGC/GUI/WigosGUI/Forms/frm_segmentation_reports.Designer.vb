<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_segmentation_reports
  Inherits GUI_Controls.frm_base_print

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.tab_segmentation = New System.Windows.Forms.TabControl
    Me.dtp_from = New GUI_Controls.uc_date_picker
    Me.dtp_to = New GUI_Controls.uc_date_picker
    Me.panel_grids.SuspendLayout()
    Me.panel_filter.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_grids
    '
    Me.panel_grids.Controls.Add(Me.tab_segmentation)
    Me.panel_grids.Location = New System.Drawing.Point(5, 107)
    Me.panel_grids.Size = New System.Drawing.Size(1254, 611)
    Me.panel_grids.Controls.SetChildIndex(Me.panel_buttons, 0)
    Me.panel_grids.Controls.SetChildIndex(Me.tab_segmentation, 0)
    '
    'panel_buttons
    '
    Me.panel_buttons.Location = New System.Drawing.Point(1163, 0)
    Me.panel_buttons.Size = New System.Drawing.Size(91, 611)
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.dtp_to)
    Me.panel_filter.Controls.Add(Me.dtp_from)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1254, 80)
    Me.panel_filter.Controls.SetChildIndex(Me.dtp_from, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.dtp_to, 0)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Location = New System.Drawing.Point(5, 84)
    Me.pn_separator_line.Size = New System.Drawing.Size(1254, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1254, 4)
    '
    'tab_segmentation
    '
    Me.tab_segmentation.Dock = System.Windows.Forms.DockStyle.Fill
    Me.tab_segmentation.Location = New System.Drawing.Point(0, 0)
    Me.tab_segmentation.Name = "tab_segmentation"
    Me.tab_segmentation.SelectedIndex = 0
    Me.tab_segmentation.Size = New System.Drawing.Size(1163, 611)
    Me.tab_segmentation.TabIndex = 10
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.Enabled = False
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(40, 13)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = False
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(235, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 11
    Me.dtp_from.Value = New Date(2013, 2, 20, 0, 0, 0, 0)
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(40, 38)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = False
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(235, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 12
    Me.dtp_to.Value = New Date(2013, 2, 20, 0, 0, 0, 0)
    '
    'frm_segmentation_reports
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1264, 722)
    Me.Name = "frm_segmentation_reports"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_segmentation_reports"
    Me.panel_grids.ResumeLayout(False)
    Me.panel_filter.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents tab_segmentation As System.Windows.Forms.TabControl
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
End Class
