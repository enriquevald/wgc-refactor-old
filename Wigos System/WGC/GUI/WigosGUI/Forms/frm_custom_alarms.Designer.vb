<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_custom_alarms
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_custom_alarms))
    Me.gb_payout = New System.Windows.Forms.GroupBox()
    Me.Label1 = New System.Windows.Forms.Label()
    Me.lbl_federal_tax_on_won_suffix = New System.Windows.Forms.Label()
    Me.lbl_payout_pus_text = New System.Windows.Forms.Label()
    Me.lbl_payout_limit_upper = New System.Windows.Forms.Label()
    Me.lbl_payout_limit_lower = New System.Windows.Forms.Label()
    Me.lbl_payout_example = New System.Windows.Forms.Label()
    Me.ef_payout_limit_upper = New GUI_Controls.uc_entry_field()
    Me.ef_payout_limit_lower = New GUI_Controls.uc_entry_field()
    Me.cb_payout_pus_unit = New GUI_Controls.uc_combo()
    Me.ef_payout_pus_value = New GUI_Controls.uc_entry_field()
    Me.gb_jackpot = New System.Windows.Forms.GroupBox()
    Me.lbl_jackpot_pus_text = New System.Windows.Forms.Label()
    Me.lbl_jackpot_example = New System.Windows.Forms.Label()
    Me.ef_jackpot_quantity_value = New GUI_Controls.uc_entry_field()
    Me.chk_jackpot_quantity = New System.Windows.Forms.CheckBox()
    Me.ef_jackpot_amount_value = New GUI_Controls.uc_entry_field()
    Me.chk_jackpot_amount = New System.Windows.Forms.CheckBox()
    Me.cb_jackpot_pus_unit = New GUI_Controls.uc_combo()
    Me.ef_jackpot_pus_value = New GUI_Controls.uc_entry_field()
    Me.gb_canceled_credits = New System.Windows.Forms.GroupBox()
    Me.lbl_cc_pus_text = New System.Windows.Forms.Label()
    Me.lbl_canceled_credits_example = New System.Windows.Forms.Label()
    Me.ef_canceled_credits_amount_value = New GUI_Controls.uc_entry_field()
    Me.chk_canceled_credits_amount = New System.Windows.Forms.CheckBox()
    Me.ef_canceled_credits_quantity_value = New GUI_Controls.uc_entry_field()
    Me.chk_canceled_credits_quantity = New System.Windows.Forms.CheckBox()
    Me.cb_canceled_credits_pus_unit = New GUI_Controls.uc_combo()
    Me.ef_canceled_credits_pus_value = New GUI_Controls.uc_entry_field()
    Me.CheckBox11 = New System.Windows.Forms.CheckBox()
    Me.gb_counterfeits = New System.Windows.Forms.GroupBox()
    Me.chk_counterfeits_enabled = New System.Windows.Forms.CheckBox()
    Me.lbl_counterfeits_quantity_value = New System.Windows.Forms.Label()
    Me.lbl_counterfeits_pus_text = New System.Windows.Forms.Label()
    Me.lbl_counterfeits_example = New System.Windows.Forms.Label()
    Me.ef_counterfeits_quantity_value = New GUI_Controls.uc_entry_field()
    Me.cb_counterfeits_pus_unit = New GUI_Controls.uc_combo()
    Me.ef_counterfeits_pus_value = New GUI_Controls.uc_entry_field()
    Me.chk_payout_enabled = New System.Windows.Forms.CheckBox()
    Me.chk_jackpot_enabled = New System.Windows.Forms.CheckBox()
    Me.chk_canceled_credits_enabled = New System.Windows.Forms.CheckBox()
    Me.chk_won_enabled = New System.Windows.Forms.CheckBox()
    Me.gb_won = New System.Windows.Forms.GroupBox()
    Me.lbl_won_pus_text = New System.Windows.Forms.Label()
    Me.cb_won_pus_unit = New GUI_Controls.uc_combo()
    Me.ef_won_pus_value = New GUI_Controls.uc_entry_field()
    Me.lbl_won_example = New System.Windows.Forms.Label()
    Me.ef_won_amount_value = New GUI_Controls.uc_entry_field()
    Me.chk_won_amount = New System.Windows.Forms.CheckBox()
    Me.ef_won_quantity_value = New GUI_Controls.uc_entry_field()
    Me.chk_won_quantity = New System.Windows.Forms.CheckBox()
    Me.chk_played_enabled = New System.Windows.Forms.CheckBox()
    Me.gb_played = New System.Windows.Forms.GroupBox()
    Me.lbl_played_pus_text = New System.Windows.Forms.Label()
    Me.cb_played_pus_unit = New GUI_Controls.uc_combo()
    Me.ef_played_pus_value = New GUI_Controls.uc_entry_field()
    Me.lbl_played_example = New System.Windows.Forms.Label()
    Me.ef_played_amount_value = New GUI_Controls.uc_entry_field()
    Me.chk_played_amount = New System.Windows.Forms.CheckBox()
    Me.ef_played_quantity_value = New GUI_Controls.uc_entry_field()
    Me.chk_played_quantity = New System.Windows.Forms.CheckBox()
    Me.gb_terminal_activity = New System.Windows.Forms.GroupBox()
    Me.lbl_terminal_activity_pus_text = New System.Windows.Forms.Label()
    Me.lbl_terminal_activity_example = New System.Windows.Forms.Label()
    Me.cb_terminal_activity_pus_unit = New GUI_Controls.uc_combo()
    Me.ef_terminal_activity_pus_value = New GUI_Controls.uc_entry_field()
    Me.chk_terminal_activity_enabled = New System.Windows.Forms.CheckBox()
    Me.gb_highroller = New System.Windows.Forms.GroupBox()
    Me.lbl_highroller_pus_text = New System.Windows.Forms.Label()
    Me.lbl_highroller_example = New System.Windows.Forms.Label()
    Me.chk_highroller_betavg = New System.Windows.Forms.CheckBox()
    Me.cb_highroller_pus_unit = New GUI_Controls.uc_combo()
    Me.ef_highroller_pus_value = New GUI_Controls.uc_entry_field()
    Me.chk_highroller_include_personal = New System.Windows.Forms.CheckBox()
    Me.chk_highroller_coinin = New System.Windows.Forms.CheckBox()
    Me.ef_highroller_coinin = New GUI_Controls.uc_entry_field()
    Me.ef_highroller_bet_average = New GUI_Controls.uc_entry_field()
    Me.chk_highroller_enabled = New System.Windows.Forms.CheckBox()
    Me.panel_data.SuspendLayout()
    Me.gb_payout.SuspendLayout()
    Me.gb_jackpot.SuspendLayout()
    Me.gb_canceled_credits.SuspendLayout()
    Me.gb_counterfeits.SuspendLayout()
    Me.gb_won.SuspendLayout()
    Me.gb_played.SuspendLayout()
    Me.gb_terminal_activity.SuspendLayout()
    Me.gb_highroller.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.chk_highroller_enabled)
    Me.panel_data.Controls.Add(Me.gb_highroller)
    Me.panel_data.Controls.Add(Me.chk_terminal_activity_enabled)
    Me.panel_data.Controls.Add(Me.gb_terminal_activity)
    Me.panel_data.Controls.Add(Me.chk_won_enabled)
    Me.panel_data.Controls.Add(Me.gb_won)
    Me.panel_data.Controls.Add(Me.chk_played_enabled)
    Me.panel_data.Controls.Add(Me.gb_played)
    Me.panel_data.Controls.Add(Me.chk_canceled_credits_enabled)
    Me.panel_data.Controls.Add(Me.chk_jackpot_enabled)
    Me.panel_data.Controls.Add(Me.chk_payout_enabled)
    Me.panel_data.Controls.Add(Me.gb_counterfeits)
    Me.panel_data.Controls.Add(Me.gb_canceled_credits)
    Me.panel_data.Controls.Add(Me.gb_jackpot)
    Me.panel_data.Controls.Add(Me.gb_payout)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(791, 626)
    '
    'gb_payout
    '
    Me.gb_payout.Controls.Add(Me.Label1)
    Me.gb_payout.Controls.Add(Me.lbl_federal_tax_on_won_suffix)
    Me.gb_payout.Controls.Add(Me.lbl_payout_pus_text)
    Me.gb_payout.Controls.Add(Me.lbl_payout_limit_upper)
    Me.gb_payout.Controls.Add(Me.lbl_payout_limit_lower)
    Me.gb_payout.Controls.Add(Me.lbl_payout_example)
    Me.gb_payout.Controls.Add(Me.ef_payout_limit_upper)
    Me.gb_payout.Controls.Add(Me.ef_payout_limit_lower)
    Me.gb_payout.Controls.Add(Me.cb_payout_pus_unit)
    Me.gb_payout.Controls.Add(Me.ef_payout_pus_value)
    Me.gb_payout.Location = New System.Drawing.Point(16, 348)
    Me.gb_payout.Name = "gb_payout"
    Me.gb_payout.Size = New System.Drawing.Size(375, 167)
    Me.gb_payout.TabIndex = 4
    Me.gb_payout.TabStop = False
    '
    'Label1
    '
    Me.Label1.Location = New System.Drawing.Point(287, 71)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(15, 24)
    Me.Label1.TabIndex = 19
    Me.Label1.Text = "%"
    Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_federal_tax_on_won_suffix
    '
    Me.lbl_federal_tax_on_won_suffix.Location = New System.Drawing.Point(287, 45)
    Me.lbl_federal_tax_on_won_suffix.Name = "lbl_federal_tax_on_won_suffix"
    Me.lbl_federal_tax_on_won_suffix.Size = New System.Drawing.Size(15, 24)
    Me.lbl_federal_tax_on_won_suffix.TabIndex = 18
    Me.lbl_federal_tax_on_won_suffix.Text = "%"
    Me.lbl_federal_tax_on_won_suffix.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_payout_pus_text
    '
    Me.lbl_payout_pus_text.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_payout_pus_text.ForeColor = System.Drawing.Color.Black
    Me.lbl_payout_pus_text.Location = New System.Drawing.Point(36, 23)
    Me.lbl_payout_pus_text.Name = "lbl_payout_pus_text"
    Me.lbl_payout_pus_text.Size = New System.Drawing.Size(124, 16)
    Me.lbl_payout_pus_text.TabIndex = 4
    Me.lbl_payout_pus_text.Text = "xjackpot_pus_text"
    '
    'lbl_payout_limit_upper
    '
    Me.lbl_payout_limit_upper.AutoSize = True
    Me.lbl_payout_limit_upper.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_payout_limit_upper.ForeColor = System.Drawing.Color.Black
    Me.lbl_payout_limit_upper.Location = New System.Drawing.Point(36, 77)
    Me.lbl_payout_limit_upper.Name = "lbl_payout_limit_upper"
    Me.lbl_payout_limit_upper.Size = New System.Drawing.Size(124, 13)
    Me.lbl_payout_limit_upper.TabIndex = 6
    Me.lbl_payout_limit_upper.Text = "xpayout_limit_upper"
    '
    'lbl_payout_limit_lower
    '
    Me.lbl_payout_limit_lower.AutoSize = True
    Me.lbl_payout_limit_lower.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_payout_limit_lower.ForeColor = System.Drawing.Color.Black
    Me.lbl_payout_limit_lower.Location = New System.Drawing.Point(36, 50)
    Me.lbl_payout_limit_lower.Name = "lbl_payout_limit_lower"
    Me.lbl_payout_limit_lower.Size = New System.Drawing.Size(122, 13)
    Me.lbl_payout_limit_lower.TabIndex = 5
    Me.lbl_payout_limit_lower.Text = "xpayout_limit_lower"
    '
    'lbl_payout_example
    '
    Me.lbl_payout_example.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_payout_example.ForeColor = System.Drawing.Color.DarkBlue
    Me.lbl_payout_example.Location = New System.Drawing.Point(6, 103)
    Me.lbl_payout_example.Name = "lbl_payout_example"
    Me.lbl_payout_example.Size = New System.Drawing.Size(363, 56)
    Me.lbl_payout_example.TabIndex = 7
    Me.lbl_payout_example.Text = resources.GetString("lbl_payout_example.Text")
    '
    'ef_payout_limit_upper
    '
    Me.ef_payout_limit_upper.DoubleValue = 0.0R
    Me.ef_payout_limit_upper.IntegerValue = 0
    Me.ef_payout_limit_upper.IsReadOnly = False
    Me.ef_payout_limit_upper.Location = New System.Drawing.Point(9, 71)
    Me.ef_payout_limit_upper.Name = "ef_payout_limit_upper"
    Me.ef_payout_limit_upper.PlaceHolder = Nothing
    Me.ef_payout_limit_upper.Size = New System.Drawing.Size(276, 24)
    Me.ef_payout_limit_upper.SufixText = "Sufix Text"
    Me.ef_payout_limit_upper.SufixTextVisible = True
    Me.ef_payout_limit_upper.TabIndex = 3
    Me.ef_payout_limit_upper.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_payout_limit_upper.TextValue = "0"
    Me.ef_payout_limit_upper.TextWidth = 163
    Me.ef_payout_limit_upper.Value = "0"
    Me.ef_payout_limit_upper.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_payout_limit_lower
    '
    Me.ef_payout_limit_lower.DoubleValue = 0.0R
    Me.ef_payout_limit_lower.IntegerValue = 0
    Me.ef_payout_limit_lower.IsReadOnly = False
    Me.ef_payout_limit_lower.Location = New System.Drawing.Point(9, 44)
    Me.ef_payout_limit_lower.Name = "ef_payout_limit_lower"
    Me.ef_payout_limit_lower.PlaceHolder = Nothing
    Me.ef_payout_limit_lower.Size = New System.Drawing.Size(276, 24)
    Me.ef_payout_limit_lower.SufixText = "56745"
    Me.ef_payout_limit_lower.SufixTextVisible = True
    Me.ef_payout_limit_lower.TabIndex = 2
    Me.ef_payout_limit_lower.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_payout_limit_lower.TextValue = "0"
    Me.ef_payout_limit_lower.TextWidth = 163
    Me.ef_payout_limit_lower.Value = "0"
    Me.ef_payout_limit_lower.ValueForeColor = System.Drawing.Color.Blue
    '
    'cb_payout_pus_unit
    '
    Me.cb_payout_pus_unit.AllowUnlistedValues = False
    Me.cb_payout_pus_unit.AutoCompleteMode = False
    Me.cb_payout_pus_unit.IsReadOnly = False
    Me.cb_payout_pus_unit.Location = New System.Drawing.Point(287, 18)
    Me.cb_payout_pus_unit.Name = "cb_payout_pus_unit"
    Me.cb_payout_pus_unit.SelectedIndex = -1
    Me.cb_payout_pus_unit.Size = New System.Drawing.Size(80, 24)
    Me.cb_payout_pus_unit.SufixText = "Sufix Text"
    Me.cb_payout_pus_unit.SufixTextVisible = True
    Me.cb_payout_pus_unit.TabIndex = 1
    Me.cb_payout_pus_unit.TextCombo = Nothing
    Me.cb_payout_pus_unit.TextVisible = False
    Me.cb_payout_pus_unit.TextWidth = 0
    '
    'ef_payout_pus_value
    '
    Me.ef_payout_pus_value.DoubleValue = 0.0R
    Me.ef_payout_pus_value.IntegerValue = 0
    Me.ef_payout_pus_value.IsReadOnly = False
    Me.ef_payout_pus_value.Location = New System.Drawing.Point(10, 18)
    Me.ef_payout_pus_value.Name = "ef_payout_pus_value"
    Me.ef_payout_pus_value.PlaceHolder = Nothing
    Me.ef_payout_pus_value.Size = New System.Drawing.Size(275, 24)
    Me.ef_payout_pus_value.SufixText = "Sufix Text"
    Me.ef_payout_pus_value.SufixTextVisible = True
    Me.ef_payout_pus_value.TabIndex = 0
    Me.ef_payout_pus_value.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_payout_pus_value.TextValue = "0"
    Me.ef_payout_pus_value.TextWidth = 163
    Me.ef_payout_pus_value.Value = "0"
    Me.ef_payout_pus_value.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_jackpot
    '
    Me.gb_jackpot.Controls.Add(Me.lbl_jackpot_pus_text)
    Me.gb_jackpot.Controls.Add(Me.lbl_jackpot_example)
    Me.gb_jackpot.Controls.Add(Me.ef_jackpot_quantity_value)
    Me.gb_jackpot.Controls.Add(Me.chk_jackpot_quantity)
    Me.gb_jackpot.Controls.Add(Me.ef_jackpot_amount_value)
    Me.gb_jackpot.Controls.Add(Me.chk_jackpot_amount)
    Me.gb_jackpot.Controls.Add(Me.cb_jackpot_pus_unit)
    Me.gb_jackpot.Controls.Add(Me.ef_jackpot_pus_value)
    Me.gb_jackpot.Location = New System.Drawing.Point(16, 173)
    Me.gb_jackpot.Name = "gb_jackpot"
    Me.gb_jackpot.Size = New System.Drawing.Size(375, 165)
    Me.gb_jackpot.TabIndex = 2
    Me.gb_jackpot.TabStop = False
    '
    'lbl_jackpot_pus_text
    '
    Me.lbl_jackpot_pus_text.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_jackpot_pus_text.ForeColor = System.Drawing.Color.Black
    Me.lbl_jackpot_pus_text.Location = New System.Drawing.Point(36, 23)
    Me.lbl_jackpot_pus_text.Name = "lbl_jackpot_pus_text"
    Me.lbl_jackpot_pus_text.Size = New System.Drawing.Size(124, 16)
    Me.lbl_jackpot_pus_text.TabIndex = 4
    Me.lbl_jackpot_pus_text.Text = "xjackpot_pus_text"
    '
    'lbl_jackpot_example
    '
    Me.lbl_jackpot_example.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_jackpot_example.ForeColor = System.Drawing.Color.DarkBlue
    Me.lbl_jackpot_example.Location = New System.Drawing.Point(7, 98)
    Me.lbl_jackpot_example.Name = "lbl_jackpot_example"
    Me.lbl_jackpot_example.Size = New System.Drawing.Size(363, 59)
    Me.lbl_jackpot_example.TabIndex = 7
    Me.lbl_jackpot_example.Text = resources.GetString("lbl_jackpot_example.Text")
    '
    'ef_jackpot_quantity_value
    '
    Me.ef_jackpot_quantity_value.DoubleValue = 0.0R
    Me.ef_jackpot_quantity_value.IntegerValue = 0
    Me.ef_jackpot_quantity_value.IsReadOnly = False
    Me.ef_jackpot_quantity_value.Location = New System.Drawing.Point(173, 44)
    Me.ef_jackpot_quantity_value.Name = "ef_jackpot_quantity_value"
    Me.ef_jackpot_quantity_value.PlaceHolder = Nothing
    Me.ef_jackpot_quantity_value.Size = New System.Drawing.Size(112, 24)
    Me.ef_jackpot_quantity_value.SufixText = "Sufix Text"
    Me.ef_jackpot_quantity_value.SufixTextVisible = True
    Me.ef_jackpot_quantity_value.TabIndex = 3
    Me.ef_jackpot_quantity_value.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_jackpot_quantity_value.TextValue = "0"
    Me.ef_jackpot_quantity_value.TextWidth = 0
    Me.ef_jackpot_quantity_value.Value = "0"
    Me.ef_jackpot_quantity_value.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_jackpot_quantity
    '
    Me.chk_jackpot_quantity.AutoSize = True
    Me.chk_jackpot_quantity.Location = New System.Drawing.Point(20, 47)
    Me.chk_jackpot_quantity.Name = "chk_jackpot_quantity"
    Me.chk_jackpot_quantity.Size = New System.Drawing.Size(155, 17)
    Me.chk_jackpot_quantity.TabIndex = 2
    Me.chk_jackpot_quantity.Text = "xchk_jackpot_quantity"
    Me.chk_jackpot_quantity.UseVisualStyleBackColor = True
    '
    'ef_jackpot_amount_value
    '
    Me.ef_jackpot_amount_value.DoubleValue = 0.0R
    Me.ef_jackpot_amount_value.IntegerValue = 0
    Me.ef_jackpot_amount_value.IsReadOnly = False
    Me.ef_jackpot_amount_value.Location = New System.Drawing.Point(173, 69)
    Me.ef_jackpot_amount_value.Name = "ef_jackpot_amount_value"
    Me.ef_jackpot_amount_value.PlaceHolder = Nothing
    Me.ef_jackpot_amount_value.Size = New System.Drawing.Size(112, 24)
    Me.ef_jackpot_amount_value.SufixText = "Sufix Text"
    Me.ef_jackpot_amount_value.SufixTextVisible = True
    Me.ef_jackpot_amount_value.TabIndex = 5
    Me.ef_jackpot_amount_value.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_jackpot_amount_value.TextValue = "0"
    Me.ef_jackpot_amount_value.TextWidth = 0
    Me.ef_jackpot_amount_value.Value = "0"
    Me.ef_jackpot_amount_value.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_jackpot_amount
    '
    Me.chk_jackpot_amount.AutoSize = True
    Me.chk_jackpot_amount.Location = New System.Drawing.Point(20, 72)
    Me.chk_jackpot_amount.Name = "chk_jackpot_amount"
    Me.chk_jackpot_amount.Size = New System.Drawing.Size(152, 17)
    Me.chk_jackpot_amount.TabIndex = 4
    Me.chk_jackpot_amount.Text = "xchk_jackpot_amount"
    Me.chk_jackpot_amount.UseVisualStyleBackColor = True
    '
    'cb_jackpot_pus_unit
    '
    Me.cb_jackpot_pus_unit.AllowUnlistedValues = False
    Me.cb_jackpot_pus_unit.AutoCompleteMode = False
    Me.cb_jackpot_pus_unit.IsReadOnly = False
    Me.cb_jackpot_pus_unit.Location = New System.Drawing.Point(287, 18)
    Me.cb_jackpot_pus_unit.Name = "cb_jackpot_pus_unit"
    Me.cb_jackpot_pus_unit.SelectedIndex = -1
    Me.cb_jackpot_pus_unit.Size = New System.Drawing.Size(80, 24)
    Me.cb_jackpot_pus_unit.SufixText = "Sufix Text"
    Me.cb_jackpot_pus_unit.SufixTextVisible = True
    Me.cb_jackpot_pus_unit.TabIndex = 1
    Me.cb_jackpot_pus_unit.TextCombo = Nothing
    Me.cb_jackpot_pus_unit.TextWidth = 0
    '
    'ef_jackpot_pus_value
    '
    Me.ef_jackpot_pus_value.DoubleValue = 0.0R
    Me.ef_jackpot_pus_value.IntegerValue = 0
    Me.ef_jackpot_pus_value.IsReadOnly = False
    Me.ef_jackpot_pus_value.Location = New System.Drawing.Point(10, 18)
    Me.ef_jackpot_pus_value.Name = "ef_jackpot_pus_value"
    Me.ef_jackpot_pus_value.PlaceHolder = Nothing
    Me.ef_jackpot_pus_value.Size = New System.Drawing.Size(275, 24)
    Me.ef_jackpot_pus_value.SufixText = "Sufix Text"
    Me.ef_jackpot_pus_value.SufixTextVisible = True
    Me.ef_jackpot_pus_value.TabIndex = 0
    Me.ef_jackpot_pus_value.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_jackpot_pus_value.TextValue = "0"
    Me.ef_jackpot_pus_value.TextWidth = 163
    Me.ef_jackpot_pus_value.Value = "0"
    Me.ef_jackpot_pus_value.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_canceled_credits
    '
    Me.gb_canceled_credits.Controls.Add(Me.lbl_cc_pus_text)
    Me.gb_canceled_credits.Controls.Add(Me.lbl_canceled_credits_example)
    Me.gb_canceled_credits.Controls.Add(Me.ef_canceled_credits_amount_value)
    Me.gb_canceled_credits.Controls.Add(Me.chk_canceled_credits_amount)
    Me.gb_canceled_credits.Controls.Add(Me.ef_canceled_credits_quantity_value)
    Me.gb_canceled_credits.Controls.Add(Me.chk_canceled_credits_quantity)
    Me.gb_canceled_credits.Controls.Add(Me.cb_canceled_credits_pus_unit)
    Me.gb_canceled_credits.Controls.Add(Me.ef_canceled_credits_pus_value)
    Me.gb_canceled_credits.Controls.Add(Me.CheckBox11)
    Me.gb_canceled_credits.Location = New System.Drawing.Point(402, 162)
    Me.gb_canceled_credits.Name = "gb_canceled_credits"
    Me.gb_canceled_credits.Size = New System.Drawing.Size(375, 157)
    Me.gb_canceled_credits.TabIndex = 3
    Me.gb_canceled_credits.TabStop = False
    '
    'lbl_cc_pus_text
    '
    Me.lbl_cc_pus_text.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_cc_pus_text.ForeColor = System.Drawing.Color.Black
    Me.lbl_cc_pus_text.Location = New System.Drawing.Point(36, 19)
    Me.lbl_cc_pus_text.Name = "lbl_cc_pus_text"
    Me.lbl_cc_pus_text.Size = New System.Drawing.Size(124, 16)
    Me.lbl_cc_pus_text.TabIndex = 5
    Me.lbl_cc_pus_text.Text = "xcc_pus_text"
    '
    'lbl_canceled_credits_example
    '
    Me.lbl_canceled_credits_example.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_canceled_credits_example.ForeColor = System.Drawing.Color.DarkBlue
    Me.lbl_canceled_credits_example.Location = New System.Drawing.Point(5, 94)
    Me.lbl_canceled_credits_example.Name = "lbl_canceled_credits_example"
    Me.lbl_canceled_credits_example.Size = New System.Drawing.Size(361, 56)
    Me.lbl_canceled_credits_example.TabIndex = 8
    Me.lbl_canceled_credits_example.Text = resources.GetString("lbl_canceled_credits_example.Text")
    '
    'ef_canceled_credits_amount_value
    '
    Me.ef_canceled_credits_amount_value.DoubleValue = 0.0R
    Me.ef_canceled_credits_amount_value.IntegerValue = 0
    Me.ef_canceled_credits_amount_value.IsReadOnly = False
    Me.ef_canceled_credits_amount_value.Location = New System.Drawing.Point(173, 65)
    Me.ef_canceled_credits_amount_value.Name = "ef_canceled_credits_amount_value"
    Me.ef_canceled_credits_amount_value.PlaceHolder = Nothing
    Me.ef_canceled_credits_amount_value.Size = New System.Drawing.Size(113, 24)
    Me.ef_canceled_credits_amount_value.SufixText = "Sufix Text"
    Me.ef_canceled_credits_amount_value.SufixTextVisible = True
    Me.ef_canceled_credits_amount_value.TabIndex = 5
    Me.ef_canceled_credits_amount_value.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_canceled_credits_amount_value.TextValue = "0"
    Me.ef_canceled_credits_amount_value.TextWidth = 0
    Me.ef_canceled_credits_amount_value.Value = "0"
    Me.ef_canceled_credits_amount_value.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_canceled_credits_amount
    '
    Me.chk_canceled_credits_amount.AutoSize = True
    Me.chk_canceled_credits_amount.Location = New System.Drawing.Point(20, 68)
    Me.chk_canceled_credits_amount.Name = "chk_canceled_credits_amount"
    Me.chk_canceled_credits_amount.Size = New System.Drawing.Size(205, 17)
    Me.chk_canceled_credits_amount.TabIndex = 4
    Me.chk_canceled_credits_amount.Text = "xchk_canceled_credits_amount"
    Me.chk_canceled_credits_amount.UseVisualStyleBackColor = True
    '
    'ef_canceled_credits_quantity_value
    '
    Me.ef_canceled_credits_quantity_value.DoubleValue = 0.0R
    Me.ef_canceled_credits_quantity_value.IntegerValue = 0
    Me.ef_canceled_credits_quantity_value.IsReadOnly = False
    Me.ef_canceled_credits_quantity_value.Location = New System.Drawing.Point(173, 40)
    Me.ef_canceled_credits_quantity_value.Name = "ef_canceled_credits_quantity_value"
    Me.ef_canceled_credits_quantity_value.PlaceHolder = Nothing
    Me.ef_canceled_credits_quantity_value.Size = New System.Drawing.Size(113, 24)
    Me.ef_canceled_credits_quantity_value.SufixText = "Sufix Text"
    Me.ef_canceled_credits_quantity_value.SufixTextVisible = True
    Me.ef_canceled_credits_quantity_value.TabIndex = 3
    Me.ef_canceled_credits_quantity_value.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_canceled_credits_quantity_value.TextValue = "0"
    Me.ef_canceled_credits_quantity_value.TextWidth = 0
    Me.ef_canceled_credits_quantity_value.Value = "0"
    Me.ef_canceled_credits_quantity_value.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_canceled_credits_quantity
    '
    Me.chk_canceled_credits_quantity.Location = New System.Drawing.Point(20, 43)
    Me.chk_canceled_credits_quantity.Name = "chk_canceled_credits_quantity"
    Me.chk_canceled_credits_quantity.Size = New System.Drawing.Size(221, 17)
    Me.chk_canceled_credits_quantity.TabIndex = 2
    Me.chk_canceled_credits_quantity.Text = "xchk_canceled_credits_quantity"
    Me.chk_canceled_credits_quantity.UseVisualStyleBackColor = True
    '
    'cb_canceled_credits_pus_unit
    '
    Me.cb_canceled_credits_pus_unit.AllowUnlistedValues = False
    Me.cb_canceled_credits_pus_unit.AutoCompleteMode = False
    Me.cb_canceled_credits_pus_unit.IsReadOnly = False
    Me.cb_canceled_credits_pus_unit.Location = New System.Drawing.Point(287, 14)
    Me.cb_canceled_credits_pus_unit.Name = "cb_canceled_credits_pus_unit"
    Me.cb_canceled_credits_pus_unit.SelectedIndex = -1
    Me.cb_canceled_credits_pus_unit.Size = New System.Drawing.Size(80, 24)
    Me.cb_canceled_credits_pus_unit.SufixText = "Sufix Text"
    Me.cb_canceled_credits_pus_unit.SufixTextVisible = True
    Me.cb_canceled_credits_pus_unit.TabIndex = 1
    Me.cb_canceled_credits_pus_unit.TextCombo = Nothing
    Me.cb_canceled_credits_pus_unit.TextWidth = 0
    '
    'ef_canceled_credits_pus_value
    '
    Me.ef_canceled_credits_pus_value.DoubleValue = 0.0R
    Me.ef_canceled_credits_pus_value.IntegerValue = 0
    Me.ef_canceled_credits_pus_value.IsReadOnly = False
    Me.ef_canceled_credits_pus_value.Location = New System.Drawing.Point(10, 14)
    Me.ef_canceled_credits_pus_value.Name = "ef_canceled_credits_pus_value"
    Me.ef_canceled_credits_pus_value.PlaceHolder = Nothing
    Me.ef_canceled_credits_pus_value.Size = New System.Drawing.Size(276, 24)
    Me.ef_canceled_credits_pus_value.SufixText = "Sufix Text"
    Me.ef_canceled_credits_pus_value.SufixTextVisible = True
    Me.ef_canceled_credits_pus_value.TabIndex = 0
    Me.ef_canceled_credits_pus_value.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_canceled_credits_pus_value.TextValue = "0"
    Me.ef_canceled_credits_pus_value.TextWidth = 163
    Me.ef_canceled_credits_pus_value.Value = "0"
    Me.ef_canceled_credits_pus_value.ValueForeColor = System.Drawing.Color.Blue
    '
    'CheckBox11
    '
    Me.CheckBox11.AutoSize = True
    Me.CheckBox11.Location = New System.Drawing.Point(10, -33)
    Me.CheckBox11.Name = "CheckBox11"
    Me.CheckBox11.Size = New System.Drawing.Size(127, 17)
    Me.CheckBox11.TabIndex = 4
    Me.CheckBox11.TabStop = False
    Me.CheckBox11.Text = "xCanceledCredits"
    Me.CheckBox11.UseVisualStyleBackColor = True
    '
    'gb_counterfeits
    '
    Me.gb_counterfeits.Controls.Add(Me.chk_counterfeits_enabled)
    Me.gb_counterfeits.Controls.Add(Me.lbl_counterfeits_quantity_value)
    Me.gb_counterfeits.Controls.Add(Me.lbl_counterfeits_pus_text)
    Me.gb_counterfeits.Controls.Add(Me.lbl_counterfeits_example)
    Me.gb_counterfeits.Controls.Add(Me.ef_counterfeits_quantity_value)
    Me.gb_counterfeits.Controls.Add(Me.cb_counterfeits_pus_unit)
    Me.gb_counterfeits.Controls.Add(Me.ef_counterfeits_pus_value)
    Me.gb_counterfeits.Location = New System.Drawing.Point(402, 326)
    Me.gb_counterfeits.Name = "gb_counterfeits"
    Me.gb_counterfeits.Size = New System.Drawing.Size(375, 112)
    Me.gb_counterfeits.TabIndex = 5
    Me.gb_counterfeits.TabStop = False
    '
    'chk_counterfeits_enabled
    '
    Me.chk_counterfeits_enabled.AutoSize = True
    Me.chk_counterfeits_enabled.Location = New System.Drawing.Point(8, -1)
    Me.chk_counterfeits_enabled.Name = "chk_counterfeits_enabled"
    Me.chk_counterfeits_enabled.Size = New System.Drawing.Size(179, 17)
    Me.chk_counterfeits_enabled.TabIndex = 5
    Me.chk_counterfeits_enabled.Text = "xchk_counterfeits_enabled"
    Me.chk_counterfeits_enabled.UseVisualStyleBackColor = True
    '
    'lbl_counterfeits_quantity_value
    '
    Me.lbl_counterfeits_quantity_value.AutoSize = True
    Me.lbl_counterfeits_quantity_value.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_counterfeits_quantity_value.ForeColor = System.Drawing.Color.Black
    Me.lbl_counterfeits_quantity_value.Location = New System.Drawing.Point(36, 44)
    Me.lbl_counterfeits_quantity_value.Name = "lbl_counterfeits_quantity_value"
    Me.lbl_counterfeits_quantity_value.Size = New System.Drawing.Size(172, 13)
    Me.lbl_counterfeits_quantity_value.TabIndex = 4
    Me.lbl_counterfeits_quantity_value.Text = "xcounterfeits_quantity_value"
    '
    'lbl_counterfeits_pus_text
    '
    Me.lbl_counterfeits_pus_text.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_counterfeits_pus_text.ForeColor = System.Drawing.Color.Black
    Me.lbl_counterfeits_pus_text.Location = New System.Drawing.Point(36, 19)
    Me.lbl_counterfeits_pus_text.Name = "lbl_counterfeits_pus_text"
    Me.lbl_counterfeits_pus_text.Size = New System.Drawing.Size(124, 16)
    Me.lbl_counterfeits_pus_text.TabIndex = 3
    Me.lbl_counterfeits_pus_text.Text = "xcounterfeits_pus_value"
    '
    'lbl_counterfeits_example
    '
    Me.lbl_counterfeits_example.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_counterfeits_example.ForeColor = System.Drawing.Color.DarkBlue
    Me.lbl_counterfeits_example.Location = New System.Drawing.Point(5, 64)
    Me.lbl_counterfeits_example.Name = "lbl_counterfeits_example"
    Me.lbl_counterfeits_example.Size = New System.Drawing.Size(365, 42)
    Me.lbl_counterfeits_example.TabIndex = 5
    Me.lbl_counterfeits_example.Text = resources.GetString("lbl_counterfeits_example.Text")
    '
    'ef_counterfeits_quantity_value
    '
    Me.ef_counterfeits_quantity_value.DoubleValue = 0.0R
    Me.ef_counterfeits_quantity_value.IntegerValue = 0
    Me.ef_counterfeits_quantity_value.IsReadOnly = False
    Me.ef_counterfeits_quantity_value.Location = New System.Drawing.Point(10, 38)
    Me.ef_counterfeits_quantity_value.Name = "ef_counterfeits_quantity_value"
    Me.ef_counterfeits_quantity_value.PlaceHolder = Nothing
    Me.ef_counterfeits_quantity_value.Size = New System.Drawing.Size(277, 24)
    Me.ef_counterfeits_quantity_value.SufixText = "Sufix Text"
    Me.ef_counterfeits_quantity_value.SufixTextVisible = True
    Me.ef_counterfeits_quantity_value.TabIndex = 2
    Me.ef_counterfeits_quantity_value.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_counterfeits_quantity_value.TextValue = "0"
    Me.ef_counterfeits_quantity_value.TextWidth = 163
    Me.ef_counterfeits_quantity_value.Value = "0"
    Me.ef_counterfeits_quantity_value.ValueForeColor = System.Drawing.Color.Blue
    '
    'cb_counterfeits_pus_unit
    '
    Me.cb_counterfeits_pus_unit.AllowUnlistedValues = False
    Me.cb_counterfeits_pus_unit.AutoCompleteMode = False
    Me.cb_counterfeits_pus_unit.IsReadOnly = False
    Me.cb_counterfeits_pus_unit.Location = New System.Drawing.Point(287, 14)
    Me.cb_counterfeits_pus_unit.Name = "cb_counterfeits_pus_unit"
    Me.cb_counterfeits_pus_unit.SelectedIndex = -1
    Me.cb_counterfeits_pus_unit.Size = New System.Drawing.Size(80, 24)
    Me.cb_counterfeits_pus_unit.SufixText = "Sufix Text"
    Me.cb_counterfeits_pus_unit.SufixTextVisible = True
    Me.cb_counterfeits_pus_unit.TabIndex = 1
    Me.cb_counterfeits_pus_unit.TextCombo = Nothing
    Me.cb_counterfeits_pus_unit.TextWidth = 0
    '
    'ef_counterfeits_pus_value
    '
    Me.ef_counterfeits_pus_value.DoubleValue = 0.0R
    Me.ef_counterfeits_pus_value.IntegerValue = 0
    Me.ef_counterfeits_pus_value.IsReadOnly = False
    Me.ef_counterfeits_pus_value.Location = New System.Drawing.Point(10, 14)
    Me.ef_counterfeits_pus_value.Name = "ef_counterfeits_pus_value"
    Me.ef_counterfeits_pus_value.PlaceHolder = Nothing
    Me.ef_counterfeits_pus_value.Size = New System.Drawing.Size(276, 24)
    Me.ef_counterfeits_pus_value.SufixText = "Sufix Text"
    Me.ef_counterfeits_pus_value.SufixTextVisible = True
    Me.ef_counterfeits_pus_value.TabIndex = 0
    Me.ef_counterfeits_pus_value.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_counterfeits_pus_value.TextValue = "0"
    Me.ef_counterfeits_pus_value.TextWidth = 163
    Me.ef_counterfeits_pus_value.Value = "0"
    Me.ef_counterfeits_pus_value.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_payout_enabled
    '
    Me.chk_payout_enabled.AutoSize = True
    Me.chk_payout_enabled.Location = New System.Drawing.Point(25, 346)
    Me.chk_payout_enabled.Name = "chk_payout_enabled"
    Me.chk_payout_enabled.Size = New System.Drawing.Size(151, 17)
    Me.chk_payout_enabled.TabIndex = 4
    Me.chk_payout_enabled.Text = "xchk_payout_enabled"
    Me.chk_payout_enabled.UseVisualStyleBackColor = True
    '
    'chk_jackpot_enabled
    '
    Me.chk_jackpot_enabled.AutoSize = True
    Me.chk_jackpot_enabled.Location = New System.Drawing.Point(25, 171)
    Me.chk_jackpot_enabled.Name = "chk_jackpot_enabled"
    Me.chk_jackpot_enabled.Size = New System.Drawing.Size(154, 17)
    Me.chk_jackpot_enabled.TabIndex = 2
    Me.chk_jackpot_enabled.Text = "xchk_jackpot_enabled"
    Me.chk_jackpot_enabled.UseVisualStyleBackColor = True
    '
    'chk_canceled_credits_enabled
    '
    Me.chk_canceled_credits_enabled.AutoSize = True
    Me.chk_canceled_credits_enabled.Location = New System.Drawing.Point(410, 160)
    Me.chk_canceled_credits_enabled.Name = "chk_canceled_credits_enabled"
    Me.chk_canceled_credits_enabled.Size = New System.Drawing.Size(127, 17)
    Me.chk_canceled_credits_enabled.TabIndex = 3
    Me.chk_canceled_credits_enabled.Text = "xCanceledCredits"
    Me.chk_canceled_credits_enabled.UseVisualStyleBackColor = True
    '
    'chk_won_enabled
    '
    Me.chk_won_enabled.AutoSize = True
    Me.chk_won_enabled.Location = New System.Drawing.Point(410, 13)
    Me.chk_won_enabled.Name = "chk_won_enabled"
    Me.chk_won_enabled.Size = New System.Drawing.Size(135, 17)
    Me.chk_won_enabled.TabIndex = 1
    Me.chk_won_enabled.Text = "xchk_won_enabled"
    Me.chk_won_enabled.UseVisualStyleBackColor = True
    '
    'gb_won
    '
    Me.gb_won.Controls.Add(Me.lbl_won_pus_text)
    Me.gb_won.Controls.Add(Me.cb_won_pus_unit)
    Me.gb_won.Controls.Add(Me.ef_won_pus_value)
    Me.gb_won.Controls.Add(Me.lbl_won_example)
    Me.gb_won.Controls.Add(Me.ef_won_amount_value)
    Me.gb_won.Controls.Add(Me.chk_won_amount)
    Me.gb_won.Controls.Add(Me.ef_won_quantity_value)
    Me.gb_won.Controls.Add(Me.chk_won_quantity)
    Me.gb_won.Location = New System.Drawing.Point(401, 15)
    Me.gb_won.Name = "gb_won"
    Me.gb_won.Size = New System.Drawing.Size(375, 142)
    Me.gb_won.TabIndex = 1
    Me.gb_won.TabStop = False
    '
    'lbl_won_pus_text
    '
    Me.lbl_won_pus_text.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_won_pus_text.ForeColor = System.Drawing.Color.Black
    Me.lbl_won_pus_text.Location = New System.Drawing.Point(36, 19)
    Me.lbl_won_pus_text.Name = "lbl_won_pus_text"
    Me.lbl_won_pus_text.Size = New System.Drawing.Size(124, 16)
    Me.lbl_won_pus_text.TabIndex = 4
    Me.lbl_won_pus_text.Text = "xwon_pus_text"
    '
    'cb_won_pus_unit
    '
    Me.cb_won_pus_unit.AllowUnlistedValues = False
    Me.cb_won_pus_unit.AutoCompleteMode = False
    Me.cb_won_pus_unit.IsReadOnly = False
    Me.cb_won_pus_unit.Location = New System.Drawing.Point(287, 14)
    Me.cb_won_pus_unit.Name = "cb_won_pus_unit"
    Me.cb_won_pus_unit.SelectedIndex = -1
    Me.cb_won_pus_unit.Size = New System.Drawing.Size(80, 24)
    Me.cb_won_pus_unit.SufixText = "Sufix Text"
    Me.cb_won_pus_unit.SufixTextVisible = True
    Me.cb_won_pus_unit.TabIndex = 1
    Me.cb_won_pus_unit.TextCombo = Nothing
    Me.cb_won_pus_unit.TextWidth = 0
    '
    'ef_won_pus_value
    '
    Me.ef_won_pus_value.DoubleValue = 0.0R
    Me.ef_won_pus_value.IntegerValue = 0
    Me.ef_won_pus_value.IsReadOnly = False
    Me.ef_won_pus_value.Location = New System.Drawing.Point(10, 14)
    Me.ef_won_pus_value.Name = "ef_won_pus_value"
    Me.ef_won_pus_value.PlaceHolder = Nothing
    Me.ef_won_pus_value.Size = New System.Drawing.Size(276, 24)
    Me.ef_won_pus_value.SufixText = "Sufix Text"
    Me.ef_won_pus_value.SufixTextVisible = True
    Me.ef_won_pus_value.TabIndex = 0
    Me.ef_won_pus_value.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_won_pus_value.TextValue = "0"
    Me.ef_won_pus_value.TextWidth = 163
    Me.ef_won_pus_value.Value = "0"
    Me.ef_won_pus_value.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_won_example
    '
    Me.lbl_won_example.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_won_example.ForeColor = System.Drawing.Color.DarkBlue
    Me.lbl_won_example.Location = New System.Drawing.Point(5, 91)
    Me.lbl_won_example.Name = "lbl_won_example"
    Me.lbl_won_example.Size = New System.Drawing.Size(361, 42)
    Me.lbl_won_example.TabIndex = 7
    Me.lbl_won_example.Text = resources.GetString("lbl_won_example.Text")
    '
    'ef_won_amount_value
    '
    Me.ef_won_amount_value.DoubleValue = 0.0R
    Me.ef_won_amount_value.IntegerValue = 0
    Me.ef_won_amount_value.IsReadOnly = False
    Me.ef_won_amount_value.Location = New System.Drawing.Point(173, 66)
    Me.ef_won_amount_value.Name = "ef_won_amount_value"
    Me.ef_won_amount_value.PlaceHolder = Nothing
    Me.ef_won_amount_value.Size = New System.Drawing.Size(113, 24)
    Me.ef_won_amount_value.SufixText = "Sufix Text"
    Me.ef_won_amount_value.SufixTextVisible = True
    Me.ef_won_amount_value.TabIndex = 5
    Me.ef_won_amount_value.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_won_amount_value.TextValue = "0"
    Me.ef_won_amount_value.TextWidth = 0
    Me.ef_won_amount_value.Value = "0"
    Me.ef_won_amount_value.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_won_amount
    '
    Me.chk_won_amount.AutoSize = True
    Me.chk_won_amount.Location = New System.Drawing.Point(20, 69)
    Me.chk_won_amount.Name = "chk_won_amount"
    Me.chk_won_amount.Size = New System.Drawing.Size(133, 17)
    Me.chk_won_amount.TabIndex = 4
    Me.chk_won_amount.Text = "xchk_won_amount"
    Me.chk_won_amount.UseVisualStyleBackColor = True
    '
    'ef_won_quantity_value
    '
    Me.ef_won_quantity_value.DoubleValue = 0.0R
    Me.ef_won_quantity_value.IntegerValue = 0
    Me.ef_won_quantity_value.IsReadOnly = False
    Me.ef_won_quantity_value.Location = New System.Drawing.Point(173, 40)
    Me.ef_won_quantity_value.Name = "ef_won_quantity_value"
    Me.ef_won_quantity_value.PlaceHolder = Nothing
    Me.ef_won_quantity_value.Size = New System.Drawing.Size(113, 24)
    Me.ef_won_quantity_value.SufixText = "Sufix Text"
    Me.ef_won_quantity_value.SufixTextVisible = True
    Me.ef_won_quantity_value.TabIndex = 3
    Me.ef_won_quantity_value.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_won_quantity_value.TextValue = "0"
    Me.ef_won_quantity_value.TextWidth = 0
    Me.ef_won_quantity_value.Value = "0"
    Me.ef_won_quantity_value.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_won_quantity
    '
    Me.chk_won_quantity.AutoSize = True
    Me.chk_won_quantity.Location = New System.Drawing.Point(20, 43)
    Me.chk_won_quantity.Name = "chk_won_quantity"
    Me.chk_won_quantity.Size = New System.Drawing.Size(136, 17)
    Me.chk_won_quantity.TabIndex = 2
    Me.chk_won_quantity.Text = "xchk_won_quantity"
    Me.chk_won_quantity.UseVisualStyleBackColor = True
    '
    'chk_played_enabled
    '
    Me.chk_played_enabled.AutoSize = True
    Me.chk_played_enabled.Location = New System.Drawing.Point(25, 13)
    Me.chk_played_enabled.Name = "chk_played_enabled"
    Me.chk_played_enabled.Size = New System.Drawing.Size(150, 17)
    Me.chk_played_enabled.TabIndex = 0
    Me.chk_played_enabled.Text = "xchk_played_enabled"
    Me.chk_played_enabled.UseVisualStyleBackColor = True
    '
    'gb_played
    '
    Me.gb_played.Controls.Add(Me.lbl_played_pus_text)
    Me.gb_played.Controls.Add(Me.cb_played_pus_unit)
    Me.gb_played.Controls.Add(Me.ef_played_pus_value)
    Me.gb_played.Controls.Add(Me.lbl_played_example)
    Me.gb_played.Controls.Add(Me.ef_played_amount_value)
    Me.gb_played.Controls.Add(Me.chk_played_amount)
    Me.gb_played.Controls.Add(Me.ef_played_quantity_value)
    Me.gb_played.Controls.Add(Me.chk_played_quantity)
    Me.gb_played.Location = New System.Drawing.Point(16, 15)
    Me.gb_played.Name = "gb_played"
    Me.gb_played.Size = New System.Drawing.Size(375, 150)
    Me.gb_played.TabIndex = 0
    Me.gb_played.TabStop = False
    '
    'lbl_played_pus_text
    '
    Me.lbl_played_pus_text.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_played_pus_text.ForeColor = System.Drawing.Color.Black
    Me.lbl_played_pus_text.Location = New System.Drawing.Point(36, 23)
    Me.lbl_played_pus_text.Name = "lbl_played_pus_text"
    Me.lbl_played_pus_text.Size = New System.Drawing.Size(124, 16)
    Me.lbl_played_pus_text.TabIndex = 4
    Me.lbl_played_pus_text.Text = "xplayed_pus_text"
    '
    'cb_played_pus_unit
    '
    Me.cb_played_pus_unit.AllowUnlistedValues = False
    Me.cb_played_pus_unit.AutoCompleteMode = False
    Me.cb_played_pus_unit.IsReadOnly = False
    Me.cb_played_pus_unit.Location = New System.Drawing.Point(287, 18)
    Me.cb_played_pus_unit.Name = "cb_played_pus_unit"
    Me.cb_played_pus_unit.SelectedIndex = -1
    Me.cb_played_pus_unit.Size = New System.Drawing.Size(80, 24)
    Me.cb_played_pus_unit.SufixText = "Sufix Text"
    Me.cb_played_pus_unit.SufixTextVisible = True
    Me.cb_played_pus_unit.TabIndex = 1
    Me.cb_played_pus_unit.TextCombo = Nothing
    Me.cb_played_pus_unit.TextVisible = False
    Me.cb_played_pus_unit.TextWidth = 0
    '
    'ef_played_pus_value
    '
    Me.ef_played_pus_value.DoubleValue = 0.0R
    Me.ef_played_pus_value.IntegerValue = 0
    Me.ef_played_pus_value.IsReadOnly = False
    Me.ef_played_pus_value.Location = New System.Drawing.Point(10, 18)
    Me.ef_played_pus_value.Name = "ef_played_pus_value"
    Me.ef_played_pus_value.PlaceHolder = Nothing
    Me.ef_played_pus_value.Size = New System.Drawing.Size(275, 24)
    Me.ef_played_pus_value.SufixText = "Sufix Text"
    Me.ef_played_pus_value.SufixTextVisible = True
    Me.ef_played_pus_value.TabIndex = 0
    Me.ef_played_pus_value.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_played_pus_value.TextValue = "0"
    Me.ef_played_pus_value.TextWidth = 163
    Me.ef_played_pus_value.Value = "0"
    Me.ef_played_pus_value.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_played_example
    '
    Me.lbl_played_example.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_played_example.ForeColor = System.Drawing.Color.DarkBlue
    Me.lbl_played_example.Location = New System.Drawing.Point(6, 100)
    Me.lbl_played_example.Name = "lbl_played_example"
    Me.lbl_played_example.Size = New System.Drawing.Size(360, 42)
    Me.lbl_played_example.TabIndex = 7
    Me.lbl_played_example.Text = resources.GetString("lbl_played_example.Text")
    '
    'ef_played_amount_value
    '
    Me.ef_played_amount_value.DoubleValue = 0.0R
    Me.ef_played_amount_value.IntegerValue = 0
    Me.ef_played_amount_value.IsReadOnly = False
    Me.ef_played_amount_value.Location = New System.Drawing.Point(173, 70)
    Me.ef_played_amount_value.Name = "ef_played_amount_value"
    Me.ef_played_amount_value.PlaceHolder = Nothing
    Me.ef_played_amount_value.Size = New System.Drawing.Size(112, 24)
    Me.ef_played_amount_value.SufixText = "Sufix Text"
    Me.ef_played_amount_value.SufixTextVisible = True
    Me.ef_played_amount_value.TabIndex = 5
    Me.ef_played_amount_value.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_played_amount_value.TextValue = "0"
    Me.ef_played_amount_value.TextWidth = 0
    Me.ef_played_amount_value.Value = "0"
    Me.ef_played_amount_value.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_played_amount
    '
    Me.chk_played_amount.AutoSize = True
    Me.chk_played_amount.Location = New System.Drawing.Point(20, 73)
    Me.chk_played_amount.Name = "chk_played_amount"
    Me.chk_played_amount.Size = New System.Drawing.Size(141, 17)
    Me.chk_played_amount.TabIndex = 4
    Me.chk_played_amount.Text = "chk_played_amount"
    Me.chk_played_amount.UseVisualStyleBackColor = True
    '
    'ef_played_quantity_value
    '
    Me.ef_played_quantity_value.DoubleValue = 0.0R
    Me.ef_played_quantity_value.IntegerValue = 0
    Me.ef_played_quantity_value.IsReadOnly = False
    Me.ef_played_quantity_value.Location = New System.Drawing.Point(173, 44)
    Me.ef_played_quantity_value.Name = "ef_played_quantity_value"
    Me.ef_played_quantity_value.PlaceHolder = Nothing
    Me.ef_played_quantity_value.Size = New System.Drawing.Size(112, 24)
    Me.ef_played_quantity_value.SufixText = "Sufix Text"
    Me.ef_played_quantity_value.SufixTextVisible = True
    Me.ef_played_quantity_value.TabIndex = 3
    Me.ef_played_quantity_value.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_played_quantity_value.TextValue = "0"
    Me.ef_played_quantity_value.TextWidth = 0
    Me.ef_played_quantity_value.Value = "0"
    Me.ef_played_quantity_value.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_played_quantity
    '
    Me.chk_played_quantity.AutoSize = True
    Me.chk_played_quantity.Location = New System.Drawing.Point(20, 47)
    Me.chk_played_quantity.Name = "chk_played_quantity"
    Me.chk_played_quantity.Size = New System.Drawing.Size(151, 17)
    Me.chk_played_quantity.TabIndex = 2
    Me.chk_played_quantity.Text = "xchk_played_quantity"
    Me.chk_played_quantity.UseVisualStyleBackColor = True
    '
    'gb_terminal_activity
    '
    Me.gb_terminal_activity.Controls.Add(Me.lbl_terminal_activity_pus_text)
    Me.gb_terminal_activity.Controls.Add(Me.lbl_terminal_activity_example)
    Me.gb_terminal_activity.Controls.Add(Me.cb_terminal_activity_pus_unit)
    Me.gb_terminal_activity.Controls.Add(Me.ef_terminal_activity_pus_value)
    Me.gb_terminal_activity.Location = New System.Drawing.Point(16, 524)
    Me.gb_terminal_activity.Name = "gb_terminal_activity"
    Me.gb_terminal_activity.Size = New System.Drawing.Size(375, 93)
    Me.gb_terminal_activity.TabIndex = 7
    Me.gb_terminal_activity.TabStop = False
    '
    'lbl_terminal_activity_pus_text
    '
    Me.lbl_terminal_activity_pus_text.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_terminal_activity_pus_text.ForeColor = System.Drawing.Color.Black
    Me.lbl_terminal_activity_pus_text.Location = New System.Drawing.Point(36, 23)
    Me.lbl_terminal_activity_pus_text.Name = "lbl_terminal_activity_pus_text"
    Me.lbl_terminal_activity_pus_text.Size = New System.Drawing.Size(124, 16)
    Me.lbl_terminal_activity_pus_text.TabIndex = 3
    Me.lbl_terminal_activity_pus_text.Text = "xlbl_terminal_activity_pus_text"
    '
    'lbl_terminal_activity_example
    '
    Me.lbl_terminal_activity_example.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_terminal_activity_example.ForeColor = System.Drawing.Color.DarkBlue
    Me.lbl_terminal_activity_example.Location = New System.Drawing.Point(5, 48)
    Me.lbl_terminal_activity_example.Name = "lbl_terminal_activity_example"
    Me.lbl_terminal_activity_example.Size = New System.Drawing.Size(365, 42)
    Me.lbl_terminal_activity_example.TabIndex = 5
    Me.lbl_terminal_activity_example.Text = resources.GetString("lbl_terminal_activity_example.Text")
    '
    'cb_terminal_activity_pus_unit
    '
    Me.cb_terminal_activity_pus_unit.AllowUnlistedValues = False
    Me.cb_terminal_activity_pus_unit.AutoCompleteMode = False
    Me.cb_terminal_activity_pus_unit.IsReadOnly = False
    Me.cb_terminal_activity_pus_unit.Location = New System.Drawing.Point(287, 18)
    Me.cb_terminal_activity_pus_unit.Name = "cb_terminal_activity_pus_unit"
    Me.cb_terminal_activity_pus_unit.SelectedIndex = -1
    Me.cb_terminal_activity_pus_unit.Size = New System.Drawing.Size(80, 24)
    Me.cb_terminal_activity_pus_unit.SufixText = "Sufix Text"
    Me.cb_terminal_activity_pus_unit.SufixTextVisible = True
    Me.cb_terminal_activity_pus_unit.TabIndex = 1
    Me.cb_terminal_activity_pus_unit.TextCombo = Nothing
    Me.cb_terminal_activity_pus_unit.TextWidth = 0
    '
    'ef_terminal_activity_pus_value
    '
    Me.ef_terminal_activity_pus_value.DoubleValue = 0.0R
    Me.ef_terminal_activity_pus_value.IntegerValue = 0
    Me.ef_terminal_activity_pus_value.IsReadOnly = False
    Me.ef_terminal_activity_pus_value.Location = New System.Drawing.Point(10, 18)
    Me.ef_terminal_activity_pus_value.Name = "ef_terminal_activity_pus_value"
    Me.ef_terminal_activity_pus_value.PlaceHolder = Nothing
    Me.ef_terminal_activity_pus_value.Size = New System.Drawing.Size(276, 24)
    Me.ef_terminal_activity_pus_value.SufixText = "Sufix Text"
    Me.ef_terminal_activity_pus_value.SufixTextVisible = True
    Me.ef_terminal_activity_pus_value.TabIndex = 0
    Me.ef_terminal_activity_pus_value.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_terminal_activity_pus_value.TextValue = "0"
    Me.ef_terminal_activity_pus_value.TextWidth = 163
    Me.ef_terminal_activity_pus_value.Value = "0"
    Me.ef_terminal_activity_pus_value.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_terminal_activity_enabled
    '
    Me.chk_terminal_activity_enabled.AutoSize = True
    Me.chk_terminal_activity_enabled.Location = New System.Drawing.Point(23, 521)
    Me.chk_terminal_activity_enabled.Name = "chk_terminal_activity_enabled"
    Me.chk_terminal_activity_enabled.Size = New System.Drawing.Size(207, 17)
    Me.chk_terminal_activity_enabled.TabIndex = 8
    Me.chk_terminal_activity_enabled.Text = "xchk_terminal_activity_enabled"
    Me.chk_terminal_activity_enabled.UseVisualStyleBackColor = True
    '
    'gb_highroller
    '
    Me.gb_highroller.Controls.Add(Me.lbl_highroller_pus_text)
    Me.gb_highroller.Controls.Add(Me.lbl_highroller_example)
    Me.gb_highroller.Controls.Add(Me.chk_highroller_betavg)
    Me.gb_highroller.Controls.Add(Me.cb_highroller_pus_unit)
    Me.gb_highroller.Controls.Add(Me.ef_highroller_pus_value)
    Me.gb_highroller.Controls.Add(Me.chk_highroller_include_personal)
    Me.gb_highroller.Controls.Add(Me.chk_highroller_coinin)
    Me.gb_highroller.Controls.Add(Me.ef_highroller_coinin)
    Me.gb_highroller.Controls.Add(Me.ef_highroller_bet_average)
    Me.gb_highroller.Location = New System.Drawing.Point(402, 443)
    Me.gb_highroller.Name = "gb_highroller"
    Me.gb_highroller.Size = New System.Drawing.Size(375, 174)
    Me.gb_highroller.TabIndex = 7
    Me.gb_highroller.TabStop = False
    '
    'lbl_highroller_pus_text
    '
    Me.lbl_highroller_pus_text.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_highroller_pus_text.ForeColor = System.Drawing.Color.Black
    Me.lbl_highroller_pus_text.Location = New System.Drawing.Point(24, 20)
    Me.lbl_highroller_pus_text.Name = "lbl_highroller_pus_text"
    Me.lbl_highroller_pus_text.Size = New System.Drawing.Size(124, 16)
    Me.lbl_highroller_pus_text.TabIndex = 3
    Me.lbl_highroller_pus_text.Text = "xhighroller_pus_text"
    '
    'lbl_highroller_example
    '
    Me.lbl_highroller_example.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_highroller_example.ForeColor = System.Drawing.Color.DarkBlue
    Me.lbl_highroller_example.Location = New System.Drawing.Point(5, 114)
    Me.lbl_highroller_example.Name = "lbl_highroller_example"
    Me.lbl_highroller_example.Size = New System.Drawing.Size(365, 57)
    Me.lbl_highroller_example.TabIndex = 5
    Me.lbl_highroller_example.Text = resources.GetString("lbl_highroller_example.Text")
    '
    'chk_highroller_betavg
    '
    Me.chk_highroller_betavg.AutoSize = True
    Me.chk_highroller_betavg.Location = New System.Drawing.Point(11, 41)
    Me.chk_highroller_betavg.Name = "chk_highroller_betavg"
    Me.chk_highroller_betavg.Size = New System.Drawing.Size(160, 17)
    Me.chk_highroller_betavg.TabIndex = 2
    Me.chk_highroller_betavg.Text = "xchk_highroller_betavg"
    Me.chk_highroller_betavg.UseVisualStyleBackColor = True
    '
    'cb_highroller_pus_unit
    '
    Me.cb_highroller_pus_unit.AllowUnlistedValues = False
    Me.cb_highroller_pus_unit.AutoCompleteMode = False
    Me.cb_highroller_pus_unit.IsReadOnly = False
    Me.cb_highroller_pus_unit.Location = New System.Drawing.Point(287, 14)
    Me.cb_highroller_pus_unit.Name = "cb_highroller_pus_unit"
    Me.cb_highroller_pus_unit.SelectedIndex = -1
    Me.cb_highroller_pus_unit.Size = New System.Drawing.Size(80, 24)
    Me.cb_highroller_pus_unit.SufixText = "Sufix Text"
    Me.cb_highroller_pus_unit.SufixTextVisible = True
    Me.cb_highroller_pus_unit.TabIndex = 1
    Me.cb_highroller_pus_unit.TextCombo = Nothing
    Me.cb_highroller_pus_unit.TextWidth = 0
    '
    'ef_highroller_pus_value
    '
    Me.ef_highroller_pus_value.DoubleValue = 0.0R
    Me.ef_highroller_pus_value.IntegerValue = 0
    Me.ef_highroller_pus_value.IsReadOnly = False
    Me.ef_highroller_pus_value.Location = New System.Drawing.Point(10, 14)
    Me.ef_highroller_pus_value.Name = "ef_highroller_pus_value"
    Me.ef_highroller_pus_value.PlaceHolder = Nothing
    Me.ef_highroller_pus_value.Size = New System.Drawing.Size(276, 24)
    Me.ef_highroller_pus_value.SufixText = "Sufix Text"
    Me.ef_highroller_pus_value.SufixTextVisible = True
    Me.ef_highroller_pus_value.TabIndex = 0
    Me.ef_highroller_pus_value.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_highroller_pus_value.TextValue = "0"
    Me.ef_highroller_pus_value.TextWidth = 163
    Me.ef_highroller_pus_value.Value = "0"
    Me.ef_highroller_pus_value.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_highroller_include_personal
    '
    Me.chk_highroller_include_personal.AutoSize = True
    Me.chk_highroller_include_personal.Location = New System.Drawing.Point(11, 90)
    Me.chk_highroller_include_personal.Name = "chk_highroller_include_personal"
    Me.chk_highroller_include_personal.Size = New System.Drawing.Size(224, 17)
    Me.chk_highroller_include_personal.TabIndex = 4
    Me.chk_highroller_include_personal.Text = "xchk_highroller__include_personal"
    Me.chk_highroller_include_personal.UseVisualStyleBackColor = True
    '
    'chk_highroller_coinin
    '
    Me.chk_highroller_coinin.AutoSize = True
    Me.chk_highroller_coinin.Location = New System.Drawing.Point(11, 66)
    Me.chk_highroller_coinin.Name = "chk_highroller_coinin"
    Me.chk_highroller_coinin.Size = New System.Drawing.Size(154, 17)
    Me.chk_highroller_coinin.TabIndex = 4
    Me.chk_highroller_coinin.Text = "xchk_highroller_coinin"
    Me.chk_highroller_coinin.UseVisualStyleBackColor = True
    '
    'ef_highroller_coinin
    '
    Me.ef_highroller_coinin.DoubleValue = 0.0R
    Me.ef_highroller_coinin.IntegerValue = 0
    Me.ef_highroller_coinin.IsReadOnly = False
    Me.ef_highroller_coinin.Location = New System.Drawing.Point(174, 62)
    Me.ef_highroller_coinin.Name = "ef_highroller_coinin"
    Me.ef_highroller_coinin.PlaceHolder = Nothing
    Me.ef_highroller_coinin.Size = New System.Drawing.Size(112, 24)
    Me.ef_highroller_coinin.SufixText = "56745"
    Me.ef_highroller_coinin.SufixTextVisible = True
    Me.ef_highroller_coinin.TabIndex = 2
    Me.ef_highroller_coinin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_highroller_coinin.TextValue = "0"
    Me.ef_highroller_coinin.TextWidth = 0
    Me.ef_highroller_coinin.Value = "0"
    Me.ef_highroller_coinin.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_highroller_bet_average
    '
    Me.ef_highroller_bet_average.DoubleValue = 0.0R
    Me.ef_highroller_bet_average.IntegerValue = 0
    Me.ef_highroller_bet_average.IsReadOnly = False
    Me.ef_highroller_bet_average.Location = New System.Drawing.Point(174, 38)
    Me.ef_highroller_bet_average.Name = "ef_highroller_bet_average"
    Me.ef_highroller_bet_average.PlaceHolder = Nothing
    Me.ef_highroller_bet_average.Size = New System.Drawing.Size(112, 24)
    Me.ef_highroller_bet_average.SufixText = "56745"
    Me.ef_highroller_bet_average.SufixTextVisible = True
    Me.ef_highroller_bet_average.TabIndex = 2
    Me.ef_highroller_bet_average.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_highroller_bet_average.TextValue = "0"
    Me.ef_highroller_bet_average.TextWidth = 0
    Me.ef_highroller_bet_average.Value = "0"
    Me.ef_highroller_bet_average.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_highroller_enabled
    '
    Me.chk_highroller_enabled.AutoSize = True
    Me.chk_highroller_enabled.Location = New System.Drawing.Point(410, 441)
    Me.chk_highroller_enabled.Name = "chk_highroller_enabled"
    Me.chk_highroller_enabled.Size = New System.Drawing.Size(166, 17)
    Me.chk_highroller_enabled.TabIndex = 8
    Me.chk_highroller_enabled.Text = "xchk_highroller_enabled"
    Me.chk_highroller_enabled.UseVisualStyleBackColor = True
    '
    'frm_custom_alarms
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(895, 637)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_custom_alarms"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_custom_alarms"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.gb_payout.ResumeLayout(False)
    Me.gb_payout.PerformLayout()
    Me.gb_jackpot.ResumeLayout(False)
    Me.gb_jackpot.PerformLayout()
    Me.gb_canceled_credits.ResumeLayout(False)
    Me.gb_canceled_credits.PerformLayout()
    Me.gb_counterfeits.ResumeLayout(False)
    Me.gb_counterfeits.PerformLayout()
    Me.gb_won.ResumeLayout(False)
    Me.gb_won.PerformLayout()
    Me.gb_played.ResumeLayout(False)
    Me.gb_played.PerformLayout()
    Me.gb_terminal_activity.ResumeLayout(False)
    Me.gb_highroller.ResumeLayout(False)
    Me.gb_highroller.PerformLayout()
    Me.ResumeLayout(False)

End Sub
  Friend WithEvents gb_payout As System.Windows.Forms.GroupBox
  Friend WithEvents ef_payout_limit_upper As GUI_Controls.uc_entry_field
  Friend WithEvents ef_payout_limit_lower As GUI_Controls.uc_entry_field
  Friend WithEvents cb_payout_pus_unit As GUI_Controls.uc_combo
  Friend WithEvents ef_payout_pus_value As GUI_Controls.uc_entry_field
  Friend WithEvents gb_counterfeits As System.Windows.Forms.GroupBox
  Friend WithEvents gb_canceled_credits As System.Windows.Forms.GroupBox
  Friend WithEvents gb_jackpot As System.Windows.Forms.GroupBox
  Friend WithEvents ef_counterfeits_quantity_value As GUI_Controls.uc_entry_field
  Friend WithEvents cb_counterfeits_pus_unit As GUI_Controls.uc_combo
  Friend WithEvents ef_counterfeits_pus_value As GUI_Controls.uc_entry_field
  Friend WithEvents chk_jackpot_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents chk_payout_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents CheckBox11 As System.Windows.Forms.CheckBox
  Friend WithEvents cb_jackpot_pus_unit As GUI_Controls.uc_combo
  Friend WithEvents ef_jackpot_pus_value As GUI_Controls.uc_entry_field
  Friend WithEvents ef_canceled_credits_amount_value As GUI_Controls.uc_entry_field
  Friend WithEvents chk_canceled_credits_amount As System.Windows.Forms.CheckBox
  Friend WithEvents ef_canceled_credits_quantity_value As GUI_Controls.uc_entry_field
  Friend WithEvents chk_canceled_credits_quantity As System.Windows.Forms.CheckBox
  Friend WithEvents cb_canceled_credits_pus_unit As GUI_Controls.uc_combo
  Friend WithEvents ef_canceled_credits_pus_value As GUI_Controls.uc_entry_field
  Friend WithEvents ef_jackpot_amount_value As GUI_Controls.uc_entry_field
  Friend WithEvents chk_jackpot_amount As System.Windows.Forms.CheckBox
  Friend WithEvents ef_jackpot_quantity_value As GUI_Controls.uc_entry_field
  Friend WithEvents chk_jackpot_quantity As System.Windows.Forms.CheckBox
  Friend WithEvents chk_canceled_credits_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents chk_counterfeits_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_counterfeits_example As System.Windows.Forms.Label
  Friend WithEvents lbl_canceled_credits_example As System.Windows.Forms.Label
  Friend WithEvents lbl_jackpot_example As System.Windows.Forms.Label
  Friend WithEvents lbl_payout_example As System.Windows.Forms.Label
  Friend WithEvents chk_won_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents gb_won As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_won_example As System.Windows.Forms.Label
  Friend WithEvents ef_won_amount_value As GUI_Controls.uc_entry_field
  Friend WithEvents chk_won_amount As System.Windows.Forms.CheckBox
  Friend WithEvents ef_won_quantity_value As GUI_Controls.uc_entry_field
  Friend WithEvents chk_won_quantity As System.Windows.Forms.CheckBox
  Friend WithEvents chk_played_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents gb_played As System.Windows.Forms.GroupBox
  Friend WithEvents cb_played_pus_unit As GUI_Controls.uc_combo
  Friend WithEvents ef_played_pus_value As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_played_example As System.Windows.Forms.Label
  Friend WithEvents ef_played_amount_value As GUI_Controls.uc_entry_field
  Friend WithEvents chk_played_amount As System.Windows.Forms.CheckBox
  Friend WithEvents ef_played_quantity_value As GUI_Controls.uc_entry_field
  Friend WithEvents chk_played_quantity As System.Windows.Forms.CheckBox
  Friend WithEvents cb_won_pus_unit As GUI_Controls.uc_combo
  Friend WithEvents ef_won_pus_value As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_won_pus_text As System.Windows.Forms.Label
  Friend WithEvents lbl_played_pus_text As System.Windows.Forms.Label
  Friend WithEvents lbl_cc_pus_text As System.Windows.Forms.Label
  Friend WithEvents lbl_jackpot_pus_text As System.Windows.Forms.Label
  Friend WithEvents lbl_counterfeits_pus_text As System.Windows.Forms.Label
  Friend WithEvents lbl_counterfeits_quantity_value As System.Windows.Forms.Label
  Friend WithEvents lbl_payout_limit_upper As System.Windows.Forms.Label
  Friend WithEvents lbl_payout_limit_lower As System.Windows.Forms.Label
  Friend WithEvents lbl_payout_pus_text As System.Windows.Forms.Label
  Friend WithEvents Label1 As System.Windows.Forms.Label
  Friend WithEvents lbl_federal_tax_on_won_suffix As System.Windows.Forms.Label
  Friend WithEvents chk_terminal_activity_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents gb_terminal_activity As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_terminal_activity_pus_text As System.Windows.Forms.Label
  Friend WithEvents lbl_terminal_activity_example As System.Windows.Forms.Label
  Friend WithEvents cb_terminal_activity_pus_unit As GUI_Controls.uc_combo
  Friend WithEvents ef_terminal_activity_pus_value As GUI_Controls.uc_entry_field
  Friend WithEvents gb_highroller As System.Windows.Forms.GroupBox
  Friend WithEvents chk_highroller_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_highroller_example As System.Windows.Forms.Label
  Friend WithEvents ef_highroller_bet_average As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_highroller_pus_text As System.Windows.Forms.Label
  Friend WithEvents cb_highroller_pus_unit As GUI_Controls.uc_combo
  Friend WithEvents ef_highroller_pus_value As GUI_Controls.uc_entry_field
  Friend WithEvents chk_highroller_betavg As System.Windows.Forms.CheckBox
  Friend WithEvents chk_highroller_coinin As System.Windows.Forms.CheckBox
  Friend WithEvents ef_highroller_coinin As GUI_Controls.uc_entry_field
  Friend WithEvents chk_highroller_include_personal As System.Windows.Forms.CheckBox
End Class
