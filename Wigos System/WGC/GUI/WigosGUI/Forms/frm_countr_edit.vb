﻿'-------------------------------------------------------------------
' Copyright © 2016 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_countr_edit.vb
' DESCRIPTION:   Edit CountR Kiosks
' AUTHOR:        Yineth Nuñez
' CREATION DATE: 23-MAR-2016
'
' REVISION HISTORY:
'
' Date         Author  Description
' -----------  ------  -----------------------------------------------
' 23-MAR-2016   YNM     Initial version
' 04-JUL-2016   AMF     Bug 15090:CountR: No se graba la modificación de área/isla después de haber sido creado un kiosko
' 11-JUL-2016   JBP     Bug 15315:CountR: no se puede cerrar una sesión de caja de countr teniendo el kiosco deshabilitado
' 14-JUL-2016   JCA     Bug 15608:CountR: error en el log del Gui al crear kiosco
' 11-AGO-2016   FJC     Bug 16687:CountR: no se puede crear kiosco de redención
' 15-DEC-2016   AMF     Bug 21657:WigosGUI: Excepción no controlada en la ventana "Kiosko de redención"
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonMisc
Imports System.Text
Imports WSI.Common
Imports WSI.Common.CountR

Public Class frm_countr_edit
  Inherits frm_base_edit

#Region "Constants"

  ' Fields length
  Private Const MAX_CODE_LEN As Integer = 3
  Private Const MAX_NAME_LEN As Integer = 50
  Private Const MAX_DESC_LEN As Integer = 250
  Private Const MAX_IP_LEN As Integer = 15

#End Region ' Constants

#Region "Members"

  Protected m_countr As CLASS_COUNTR
  Private m_national_currency As String

#End Region ' Members

#Region "Members"

  Public Property CountR As CLASS_COUNTR
    Get
      Return Me.m_countr
    End Get
    Set(value As CLASS_COUNTR)
      Me.m_countr = value
    End Set
  End Property

  Public Property NationalCurrency As String
    Get
      Return Me.m_national_currency
    End Get
    Set(value As String)
      Me.m_national_currency = value
    End Set
  End Property

#End Region ' Members

#Region "Privates"

  ''' <summary>
  ''' Add Handles
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub AddHandles()

    AddHandler cmb_area.ValueChangedEvent, AddressOf cmb_area_ValueChangedEvent

  End Sub ' AddHandles

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub AreaComboFill()
    Dim _table As DataTable

    _table = GUI_GetTableUsingSQL("SELECT AR_AREA_ID, AR_NAME FROM AREAS INNER JOIN BANKS ON BK_AREA_ID = AR_AREA_ID GROUP BY AR_AREA_ID, AR_NAME ORDER BY AR_NAME", Integer.MaxValue)

    Me.cmb_area.Clear()
    Me.cmb_area.Add(_table)

  End Sub ' AreaComboFill

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub BankComboFill()
    Dim _table As DataTable

    _table = GUI_GetTableUsingSQL("SELECT BK_BANK_ID, BK_NAME FROM BANKS WHERE BK_AREA_ID = " & cmb_area.Value & " GROUP BY BK_BANK_ID, BK_NAME ORDER BY BK_NAME", Integer.MaxValue)

    Me.cmb_bank.Clear()
    Me.cmb_bank.Add(_table)

  End Sub ' BankComboFill

  ''' <summary>
  ''' Returns zone description
  ''' </summary>
  ''' <param name="AreaId"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetZoneDescription(ByVal AreaId As Integer) As String
    Dim _table As DataTable
    Dim _returnValue As String

    _table = GUI_GetTableUsingSQL("SELECT AR_SMOKING FROM AREAS WHERE AR_AREA_ID = " & AreaId.ToString(), Integer.MaxValue)

    _returnValue = GLB_NLS_GUI_CONFIGURATION.GetString(437)

    If Not IsNothing(_table) Then
      If _table.Rows.Count() > 0 Then
        If _table.Rows.Item(0).Item("AR_SMOKING") Then
          _returnValue = GLB_NLS_GUI_CONFIGURATION.GetString(436)
        End If
      End If
    End If

    Return _returnValue

  End Function ' GetZoneDescription

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub CurrencyComboFill()
    Dim table As DataTable
    Dim idx As Integer
    table = Nothing

    WSI.Common.CurrencyExchange.GetFloorDualCurrenciesForCombo(table)
    m_national_currency = table.Rows(0)("CE_CURRENCY_ISO_CODE")

    For idx = 0 To table.Rows.Count - 1
      Me.cmb_currency.Add(table.Rows.Item(idx).Item("CE_CURRENCY_ISO_CODE"))
    Next

  End Sub ' CurrencyComboFill

#End Region ' Privates

#Region "Overrides"

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_COUNTR_EDIT

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <param name="DbOperation"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        Me.DbEditedObject = New CLASS_COUNTR
        Me.CountR = DbEditedObject

        Me.DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          If Me.DbStatus = ENUM_STATUS.STATUS_NOT_FOUND Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7249), ENUM_MB_TYPE.MB_TYPE_ERROR)
          Else
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7248), ENUM_MB_TYPE.MB_TYPE_ERROR)
          End If

        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          If Me.DbStatus = ENUM_STATUS.STATUS_DEPENDENCIES Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7254), ENUM_MB_TYPE.MB_TYPE_ERROR)
          ElseIf DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7259), ENUM_MB_TYPE.MB_TYPE_ERROR)
          ElseIf Me.DbStatus = ENUM_STATUS.STATUS_ALREADY_EXISTS Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7259), ENUM_MB_TYPE.MB_TYPE_WARNING)
          Else
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7250), ENUM_MB_TYPE.MB_TYPE_ERROR)
          End If
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE
        Dim _read As CLASS_COUNTR = DbReadObject
        Dim _edit As CLASS_COUNTR = DbEditedObject
        If (_read.Enabled <> _edit.Enabled) Then
          Me.CountR.RetirementUserId = IIf(Not m_countr.Enabled, IIf(Me.CountR.RetirementDate = DateTime.MinValue, GLB_CurrentUser.Id, Me.CountR.RetirementUserId), 0)
          Me.CountR.RetirementUserName = IIf(Not m_countr.Enabled, IIf(Me.CountR.RetirementDate = DateTime.MinValue, GLB_CurrentUser.Name, Me.CountR.RetirementUserName), String.Empty)
          Me.CountR.RetirementDate = IIf(Not m_countr.Enabled, IIf(Me.CountR.RetirementDate = DateTime.MinValue, WGDB.Now, Me.CountR.RetirementDate), DateTime.MinValue)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          If Me.DbStatus = ENUM_STATUS.STATUS_DEPENDENCIES Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7254), ENUM_MB_TYPE.MB_TYPE_ERROR)
          ElseIf Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7259), ENUM_MB_TYPE.MB_TYPE_ERROR)
          ElseIf Me.DbStatus = ENUM_STATUS.STATUS_NOT_FOUND Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7249), ENUM_MB_TYPE.MB_TYPE_ERROR)
          ElseIf Me.DbStatus = ENUM_STATUS.STATUS_ALREADY_EXISTS Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7259), ENUM_MB_TYPE.MB_TYPE_WARNING)
          Else
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7250), ENUM_MB_TYPE.MB_TYPE_ERROR)
          End If
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)
    End Select

  End Sub ' GUI_DB_Operation

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <param name="CountRId"></param>
  ''' <remarks></remarks>
  Public Overloads Sub ShowEditItem(ByVal CountRId As Int64)
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
    Me.DbObjectId = CountRId

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)

    If Me.DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If Me.DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowEditItem

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <param name="CountRCopiedId"></param>
  ''' <remarks></remarks>
  Public Overloads Sub ShowNewItem(Optional ByVal CountRCopiedId As Long = 0)
    Dim _countr As CLASS_COUNTR

    If Me.CountR Is Nothing Then
      Me.CountR = New CLASS_COUNTR()
    End If

    Me.CountR.Id = CountRCopiedId

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    DbObjectId = Nothing
    DbStatus = ENUM_STATUS.STATUS_OK

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)

    If CountRCopiedId > 0 Then
      _countr = DbEditedObject
      DbObjectId = CountRCopiedId
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
      DbObjectId = Nothing

      Call _countr.ResetIdValues()
    End If

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowNewItem

  ''' <summary>
  ''' Key press event
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    Select Case e.KeyChar
      Case Chr(Keys.Enter)

        If Me.txt_description.ContainsFocus Then
          Return True
        End If

        Return False
    End Select

  End Function ' GUI_KeyPress

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Dim nls_param As String
    Dim _exist_opened_session As Boolean

    ' Empty Code
    If String.IsNullOrEmpty(Me.txt_code.Value) Then
      ' 3441 "El campo 'Código' es obligatorio"
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3441), _
                                ENUM_MB_TYPE.MB_TYPE_ERROR, _
                                ENUM_MB_BTN.MB_BTN_OK)
      Call Me.txt_code.Focus()

      Return False
    End If

    ' Too long Code
    If Me.txt_code.Value.Length > MAX_CODE_LEN Then
      ' 3442 "El campo 'Código' debe tener un máximo de %1 carácteres."
      nls_param = MAX_CODE_LEN.ToString()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3442), _
                                ENUM_MB_TYPE.MB_TYPE_ERROR, _
                                ENUM_MB_BTN.MB_BTN_OK, _
                                ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                                nls_param)
      Call Me.txt_code.Focus()

      Return False
    End If

    ' Empty Name
    If String.IsNullOrEmpty(Me.txt_name.Value) Then
      ' 3443 "El campo 'Nombre' es obligatorio"
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3443), _
                                ENUM_MB_TYPE.MB_TYPE_ERROR, _
                                ENUM_MB_BTN.MB_BTN_OK)
      Call Me.txt_name.Focus()

      Return False
    End If

    ' Too long Name
    If Me.txt_name.Value.Length > MAX_NAME_LEN Then
      ' 3442 "El campo 'Nombre' debe tener un máximo de %1 carácteres."
      nls_param = MAX_NAME_LEN.ToString()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3444), _
                                ENUM_MB_TYPE.MB_TYPE_ERROR, _
                                ENUM_MB_BTN.MB_BTN_OK, _
                                nls_param)
      Call Me.txt_name.Focus()

      Return False
    End If

    ' Too long Description
    If Me.txt_description.Text.Length > MAX_DESC_LEN Then
      ' 3448 "El campo 'Descripción' debe tener un máximo de %1 carácteres."
      nls_param = MAX_DESC_LEN.ToString()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3448), _
                                ENUM_MB_TYPE.MB_TYPE_ERROR, _
                                ENUM_MB_BTN.MB_BTN_OK, _
                                nls_param)
      Call Me.txt_description.Focus()

      Return False
    End If


    'IpAddress
    If Me.txt_ip_address.Text.Length > MAX_IP_LEN Then
      ' 3448 "El campo 'Ip' debe tener un máximo de %1 carácteres."
      nls_param = MAX_IP_LEN.ToString()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3448), _
                                ENUM_MB_TYPE.MB_TYPE_ERROR, _
                                ENUM_MB_BTN.MB_BTN_OK, _
                                nls_param)
      Call Me.txt_description.Focus()

      Return False
    End If

    ' Table Speed Slow is higher than 0
    If GUI_ParseNumber(Me.txt_max_payment.Value) > 0 And GUI_ParseNumber(Me.txt_max_payment.Value) <= GUI_ParseNumber(Me.txt_min_payment.Value) Then
      ' "The '%1' field cannot be greater than %2."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7271), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.txt_min_payment.Text, Me.txt_max_payment.Text)
      Call Me.txt_min_payment.Focus()

      Return False
    End If

    ' Check existing opened session
    If Me.CountR.Id > 0 Then
      _exist_opened_session = CountRBusinessLogic.ExistOpenCountRSession(m_countr.Id, m_countr.Enabled)
    End If

    ' Without opened session
    If Not Me.chk_enabled.Checked And _exist_opened_session Then
      ' 7401 "El Kiosko de Redención tiene una sesión abierta y no puede ser deshabilitado."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7401), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)
      Call Me.chk_enabled.Focus()

      Return False
    End If

    Return True

  End Function ' GUI_IsScreenDataOk

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_GetScreenData()

    m_countr = Me.DbEditedObject

    ' Code
    m_countr.Code = GUI_ParseNumber(Me.txt_code.Value)

    ' Name
    m_countr.Name = Me.txt_name.Value 'IIf(String.IsNullOrEmpty(txt_name.Value), Nothing, txt_name.Value)

    'Provider
    m_countr.Provider = Me.txt_provider.Value

    'IpAdress
    m_countr.IpAddress = Me.txt_ip_address.Value

    ' Show Log
    m_countr.ShowLog = Me.chk_show_log.Checked

    ' Create Ticket
    m_countr.CreateTicket = Me.chk_create_ticket.Checked

    ' Enabled
    m_countr.Enabled = Me.chk_enabled.Checked

    ' Description
    m_countr.Description = Me.txt_description.Text

    ' Area
    m_countr.AreaId = Me.cmb_area.Value
    m_countr.AreaName = cmb_area.TextValue

    ' Bank 
    m_countr.BankId = Me.cmb_bank.Value
    m_countr.BankName = cmb_bank.TextValue

    ' FloorId
    m_countr.FloorId = Me.ef_floor_id.Value

    ' IsoCode
    m_countr.IsoCode = Me.cmb_currency.TextValue

    ' Max Payment
    m_countr.MaxPayment = GUI_ParseCurrency(Me.txt_max_payment.Value)

    ' Min Payment
    m_countr.MinPayment = GUI_ParseCurrency(Me.txt_min_payment.Value)

  End Sub ' GUI_GetScreenData

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _read_item As CLASS_COUNTR

    _read_item = DbReadObject

    ' Code
    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT Then
      txt_code.Value = _read_item.Code.ToString()
    End If

    ' Name
    txt_name.Value = _read_item.Name

    'Provider
    txt_provider.Value = _read_item.Provider

    ' Show Log
    chk_show_log.Checked = _read_item.ShowLog

    ' Create Ticket
    chk_create_ticket.Checked = _read_item.CreateTicket

    ' Enabled
    chk_enabled.Checked = _read_item.Enabled

    ' Description
    txt_description.Text = _read_item.Description

    ' Area
    cmb_area.Value = _read_item.AreaId

    ef_smoking.Value = IIf(_read_item.Zone, GLB_NLS_GUI_CONFIGURATION.GetString(437), GLB_NLS_GUI_CONFIGURATION.GetString(436))

    ' Bank 
    cmb_bank.Value = _read_item.BankId

    ' FloorId
    ef_floor_id.Value = _read_item.FloorId

    ' IsoCode
    If WSI.Common.Misc.IsFloorDualCurrencyEnabled() And Not String.IsNullOrEmpty(_read_item.IsoCode) Then
      Me.cmb_currency.TextValue = _read_item.IsoCode
    Else
      Me.cmb_currency.TextValue = m_national_currency
    End If

    ' Max Payment
    txt_max_payment.Value = _read_item.MaxPayment

    ' Min Payment
    txt_min_payment.Value = _read_item.MinPayment

    txt_retirement_date.Value = IIf(_read_item.RetirementDate = DateTime.MinValue, AUDIT_NONE_STRING, _read_item.RetirementDate.ToString())

    txt_retirement_user.Value = IIf(Me.CountR.RetirementDate = DateTime.MinValue, AUDIT_NONE_STRING, Me.CountR.RetirementUserName)

    'Last connection
    If Me.CountR.Id > 0 Or _read_item.LastConnection = DateTime.MinValue Then
      txt_last_connection.Value = AUDIT_NONE_STRING
    Else
      txt_last_connection.Value = Me.CountR.LastConnection
    End If

    'Status
    txt_status.Value = IIf(Me.CountR.Status = 1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7299), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7300))

    'Ip address
    Me.txt_ip_address.Value = _read_item.IpAddress

  End Sub ' GUI_SetScreenData

  ''' <summary>
  ''' Initializes resources for controls
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_InitControls()

    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    ' CountR
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7225)                         ' 7225 "Edición de CountR"

    'Controls:
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_DELETE).Visible = False

    Me.gb_countr.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7226)

    ' CountR Code
    Me.txt_code.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7227)                ' 7227 "Código"
    Me.txt_code.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, MAX_CODE_LEN, 0)

    ' CountR Name
    Me.txt_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7228)                ' 7228 "Nombre"
    Me.txt_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, MAX_NAME_LEN, 0)

    ' Area
    Me.gb_area_island.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7229)          ' 7229 "Ubicación"
    Me.cmb_area.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7230)                ' 7230 "Area"

    ' Zone
    Me.ef_smoking.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7231)              ' 7231 "Zona"
    Me.ef_smoking.Value = AUDIT_NONE_STRING
    Me.ef_smoking.IsReadOnly = True
    Me.ef_smoking.TabStop = False

    ' Bank
    Me.cmb_bank.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7232)                ' 7232 "Isla"

    ' Floor
    Me.ef_floor_id.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7233)             ' 7233 "Id en planta"
    Me.ef_floor_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 6, 0)

    ' Show Log
    Me.chk_show_log.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7325)            ' 7325 "Habilitar log"

    ' Create Ticket
    Me.chk_create_ticket.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7331)       ' 7331 "Habilitar log"

    ' Enabled
    Me.chk_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7234)            ' 7234 "Habilitada"

    ' Description
    Me.lbl_description.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7235)        ' 7235 "Descripción"
    Me.txt_description.MaxLength = MAX_DESC_LEN

    ' Provider
    Me.txt_provider.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7236)           ' 7236 "Proveedor"
    Me.txt_provider.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, MAX_NAME_LEN, 0)

    Me.cmb_currency.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7237)           ' 7237 "Moneda"

    Call AreaComboFill()
    Call BankComboFill()
    Call CurrencyComboFill()

    Me.gb_payment.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7238)             ' 7238 Configuración de Pago
    Me.txt_max_payment.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7239)        ' 7239 Max Pago
    Me.txt_max_payment.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)
    Me.txt_min_payment.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7240)        ' 7240 Min Pago
    Me.txt_min_payment.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)

    Me.gb_retired.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7241)             ' 7241 Info de Retiro
    Me.txt_retirement_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7242)    ' 7242 Fecha de retiro
    Me.txt_retirement_date.Value = AUDIT_NONE_STRING
    Me.txt_retirement_date.IsReadOnly = True
    Me.txt_retirement_date.TabStop = False


    Me.txt_retirement_user.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7243)    ' 7243 fecha de usuario
    Me.txt_retirement_user.Value = AUDIT_NONE_STRING
    Me.txt_retirement_user.IsReadOnly = True
    Me.txt_retirement_user.TabStop = False


    'Last connection
    Me.txt_last_connection.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7297) '7297 Última conexión
    Me.txt_last_connection.Value = AUDIT_NONE_STRING
    Me.txt_last_connection.IsReadOnly = True
    Me.txt_last_connection.TabStop = False

    'Status
    Me.txt_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6868) '6868 Estado
    Me.txt_status.Value = AUDIT_NONE_STRING
    Me.txt_status.IsReadOnly = True
    Me.txt_status.TabStop = False

    'Ip Address
    Me.txt_ip_address.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1897) '1897 Dirección ip
    Me.txt_ip_address.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_IP_ADDRESS, 15)

    AddHandles()

  End Sub ' GUI_InitControls

#End Region ' Overrides

#Region "Events"

  ''' <summary>
  ''' Area selection
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub cmb_area_ValueChangedEvent()

    'Zone (smoking - no smoking)
    ef_smoking.Value = GetZoneDescription(cmb_area.Value)

    'Banks
    Call BankComboFill()

  End Sub ' cmb_area_ValueChangedEvent

#End Region ' Events 

End Class
