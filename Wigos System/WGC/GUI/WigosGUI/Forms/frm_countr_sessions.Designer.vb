﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_countr_sessions
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.opt_opening_session = New System.Windows.Forms.RadioButton()
    Me.opt_movement_session = New System.Windows.Forms.RadioButton()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.fra_session_state = New System.Windows.Forms.GroupBox()
    Me.chk_session_closed = New System.Windows.Forms.CheckBox()
    Me.chk_session_open = New System.Windows.Forms.CheckBox()
    Me.gb_enabled = New System.Windows.Forms.GroupBox()
    Me.chk_no = New System.Windows.Forms.CheckBox()
    Me.chk_yes = New System.Windows.Forms.CheckBox()
    Me.gb_area_island = New System.Windows.Forms.GroupBox()
    Me.ef_floor_id = New GUI_Controls.uc_entry_field()
    Me.cmb_area = New GUI_Controls.uc_combo()
    Me.cmb_bank = New GUI_Controls.uc_combo()
    Me.ef_smoking = New GUI_Controls.uc_entry_field()
    Me.fra_terminal = New System.Windows.Forms.GroupBox()
    Me.ef_terminal_code = New GUI_Controls.uc_entry_field()
    Me.ef_terminal_name = New GUI_Controls.uc_entry_field()
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.fra_session_state.SuspendLayout()
    Me.gb_enabled.SuspendLayout()
    Me.gb_area_island.SuspendLayout()
    Me.fra_terminal.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.chk_terminal_location)
    Me.panel_filter.Controls.Add(Me.fra_terminal)
    Me.panel_filter.Controls.Add(Me.gb_enabled)
    Me.panel_filter.Controls.Add(Me.gb_area_island)
    Me.panel_filter.Controls.Add(Me.fra_session_state)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Size = New System.Drawing.Size(1381, 166)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.fra_session_state, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_area_island, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_enabled, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.fra_terminal, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_terminal_location, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 170)
    Me.panel_data.Size = New System.Drawing.Size(1381, 541)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1375, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1375, 4)
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.opt_opening_session)
    Me.gb_date.Controls.Add(Me.opt_movement_session)
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(288, 5)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(237, 130)
    Me.gb_date.TabIndex = 11
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'opt_opening_session
    '
    Me.opt_opening_session.Location = New System.Drawing.Point(18, 14)
    Me.opt_opening_session.Name = "opt_opening_session"
    Me.opt_opening_session.Size = New System.Drawing.Size(210, 24)
    Me.opt_opening_session.TabIndex = 6
    Me.opt_opening_session.Text = "xSearchByOpeningDate"
    '
    'opt_movement_session
    '
    Me.opt_movement_session.Location = New System.Drawing.Point(18, 40)
    Me.opt_movement_session.Name = "opt_movement_session"
    Me.opt_movement_session.Size = New System.Drawing.Size(210, 24)
    Me.opt_movement_session.TabIndex = 7
    Me.opt_movement_session.Text = "xSearchByMovementsDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 100)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(222, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.Cursor = System.Windows.Forms.Cursors.Default
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 70)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(222, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 8
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'fra_session_state
    '
    Me.fra_session_state.Controls.Add(Me.chk_session_closed)
    Me.fra_session_state.Controls.Add(Me.chk_session_open)
    Me.fra_session_state.Location = New System.Drawing.Point(150, 95)
    Me.fra_session_state.Name = "fra_session_state"
    Me.fra_session_state.Size = New System.Drawing.Size(132, 68)
    Me.fra_session_state.TabIndex = 12
    Me.fra_session_state.TabStop = False
    Me.fra_session_state.Text = "xState"
    '
    'chk_session_closed
    '
    Me.chk_session_closed.Location = New System.Drawing.Point(18, 46)
    Me.chk_session_closed.Name = "chk_session_closed"
    Me.chk_session_closed.Size = New System.Drawing.Size(101, 16)
    Me.chk_session_closed.TabIndex = 5
    Me.chk_session_closed.Text = "xClosed"
    '
    'chk_session_open
    '
    Me.chk_session_open.Location = New System.Drawing.Point(18, 23)
    Me.chk_session_open.Name = "chk_session_open"
    Me.chk_session_open.Size = New System.Drawing.Size(101, 17)
    Me.chk_session_open.TabIndex = 4
    Me.chk_session_open.Text = "xOpen"
    '
    'gb_enabled
    '
    Me.gb_enabled.Controls.Add(Me.chk_no)
    Me.gb_enabled.Controls.Add(Me.chk_yes)
    Me.gb_enabled.Location = New System.Drawing.Point(6, 95)
    Me.gb_enabled.Name = "gb_enabled"
    Me.gb_enabled.Size = New System.Drawing.Size(131, 68)
    Me.gb_enabled.TabIndex = 13
    Me.gb_enabled.TabStop = False
    Me.gb_enabled.Text = "xEnabled"
    '
    'chk_no
    '
    Me.chk_no.AutoSize = True
    Me.chk_no.Location = New System.Drawing.Point(11, 47)
    Me.chk_no.Name = "chk_no"
    Me.chk_no.Size = New System.Drawing.Size(48, 17)
    Me.chk_no.TabIndex = 3
    Me.chk_no.Text = "xNo"
    Me.chk_no.UseVisualStyleBackColor = True
    '
    'chk_yes
    '
    Me.chk_yes.AutoSize = True
    Me.chk_yes.Location = New System.Drawing.Point(11, 24)
    Me.chk_yes.Name = "chk_yes"
    Me.chk_yes.Size = New System.Drawing.Size(53, 17)
    Me.chk_yes.TabIndex = 2
    Me.chk_yes.Text = "xYes"
    Me.chk_yes.UseVisualStyleBackColor = True
    '
    'gb_area_island
    '
    Me.gb_area_island.Controls.Add(Me.ef_floor_id)
    Me.gb_area_island.Controls.Add(Me.cmb_area)
    Me.gb_area_island.Controls.Add(Me.cmb_bank)
    Me.gb_area_island.Controls.Add(Me.ef_smoking)
    Me.gb_area_island.Location = New System.Drawing.Point(531, 4)
    Me.gb_area_island.Name = "gb_area_island"
    Me.gb_area_island.Size = New System.Drawing.Size(351, 159)
    Me.gb_area_island.TabIndex = 14
    Me.gb_area_island.TabStop = False
    Me.gb_area_island.Text = "xLocation"
    '
    'ef_floor_id
    '
    Me.ef_floor_id.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.ef_floor_id.DoubleValue = 0.0R
    Me.ef_floor_id.IntegerValue = 0
    Me.ef_floor_id.IsReadOnly = False
    Me.ef_floor_id.Location = New System.Drawing.Point(11, 115)
    Me.ef_floor_id.Name = "ef_floor_id"
    Me.ef_floor_id.PlaceHolder = Nothing
    Me.ef_floor_id.Size = New System.Drawing.Size(327, 24)
    Me.ef_floor_id.SufixText = "Sufix Text"
    Me.ef_floor_id.SufixTextVisible = True
    Me.ef_floor_id.TabIndex = 13
    Me.ef_floor_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_floor_id.TextValue = ""
    Me.ef_floor_id.TextWidth = 120
    Me.ef_floor_id.Value = ""
    Me.ef_floor_id.ValueForeColor = System.Drawing.Color.Blue
    '
    'cmb_area
    '
    Me.cmb_area.AllowUnlistedValues = False
    Me.cmb_area.AutoCompleteMode = False
    Me.cmb_area.IsReadOnly = False
    Me.cmb_area.Location = New System.Drawing.Point(11, 20)
    Me.cmb_area.Name = "cmb_area"
    Me.cmb_area.SelectedIndex = -1
    Me.cmb_area.Size = New System.Drawing.Size(327, 24)
    Me.cmb_area.SufixText = "Sufix Text"
    Me.cmb_area.SufixTextVisible = True
    Me.cmb_area.TabIndex = 10
    Me.cmb_area.TextCombo = Nothing
    Me.cmb_area.TextWidth = 120
    '
    'cmb_bank
    '
    Me.cmb_bank.AllowUnlistedValues = False
    Me.cmb_bank.AutoCompleteMode = False
    Me.cmb_bank.IsReadOnly = False
    Me.cmb_bank.Location = New System.Drawing.Point(11, 85)
    Me.cmb_bank.Name = "cmb_bank"
    Me.cmb_bank.SelectedIndex = -1
    Me.cmb_bank.Size = New System.Drawing.Size(327, 24)
    Me.cmb_bank.SufixText = "Sufix Text"
    Me.cmb_bank.SufixTextVisible = True
    Me.cmb_bank.TabIndex = 12
    Me.cmb_bank.TextCombo = Nothing
    Me.cmb_bank.TextWidth = 120
    '
    'ef_smoking
    '
    Me.ef_smoking.DoubleValue = 0.0R
    Me.ef_smoking.IntegerValue = 0
    Me.ef_smoking.IsReadOnly = False
    Me.ef_smoking.Location = New System.Drawing.Point(11, 51)
    Me.ef_smoking.Name = "ef_smoking"
    Me.ef_smoking.PlaceHolder = Nothing
    Me.ef_smoking.Size = New System.Drawing.Size(327, 24)
    Me.ef_smoking.SufixText = "Sufix Text"
    Me.ef_smoking.SufixTextVisible = True
    Me.ef_smoking.TabIndex = 11
    Me.ef_smoking.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_smoking.TextValue = ""
    Me.ef_smoking.TextWidth = 120
    Me.ef_smoking.Value = ""
    Me.ef_smoking.ValueForeColor = System.Drawing.Color.Blue
    '
    'fra_terminal
    '
    Me.fra_terminal.Controls.Add(Me.ef_terminal_code)
    Me.fra_terminal.Controls.Add(Me.ef_terminal_name)
    Me.fra_terminal.Location = New System.Drawing.Point(6, 6)
    Me.fra_terminal.Name = "fra_terminal"
    Me.fra_terminal.Size = New System.Drawing.Size(276, 83)
    Me.fra_terminal.TabIndex = 16
    Me.fra_terminal.TabStop = False
    Me.fra_terminal.Text = "xTerminal"
    '
    'ef_terminal_code
    '
    Me.ef_terminal_code.DoubleValue = 0.0R
    Me.ef_terminal_code.IntegerValue = 0
    Me.ef_terminal_code.IsReadOnly = False
    Me.ef_terminal_code.Location = New System.Drawing.Point(16, 49)
    Me.ef_terminal_code.Name = "ef_terminal_code"
    Me.ef_terminal_code.PlaceHolder = Nothing
    Me.ef_terminal_code.Size = New System.Drawing.Size(232, 25)
    Me.ef_terminal_code.SufixText = "Sufix Text"
    Me.ef_terminal_code.SufixTextVisible = True
    Me.ef_terminal_code.TabIndex = 1
    Me.ef_terminal_code.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_terminal_code.TextValue = ""
    Me.ef_terminal_code.TextWidth = 70
    Me.ef_terminal_code.Value = ""
    Me.ef_terminal_code.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_terminal_name
    '
    Me.ef_terminal_name.DoubleValue = 0.0R
    Me.ef_terminal_name.IntegerValue = 0
    Me.ef_terminal_name.IsReadOnly = False
    Me.ef_terminal_name.Location = New System.Drawing.Point(16, 18)
    Me.ef_terminal_name.Name = "ef_terminal_name"
    Me.ef_terminal_name.PlaceHolder = Nothing
    Me.ef_terminal_name.Size = New System.Drawing.Size(232, 25)
    Me.ef_terminal_name.SufixText = "Sufix Text"
    Me.ef_terminal_name.SufixTextVisible = True
    Me.ef_terminal_name.TabIndex = 0
    Me.ef_terminal_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_terminal_name.TextValue = ""
    Me.ef_terminal_name.TextWidth = 70
    Me.ef_terminal_name.Value = ""
    Me.ef_terminal_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_terminal_location
    '
    Me.chk_terminal_location.Location = New System.Drawing.Point(288, 143)
    Me.chk_terminal_location.Name = "chk_terminal_location"
    Me.chk_terminal_location.Size = New System.Drawing.Size(199, 16)
    Me.chk_terminal_location.TabIndex = 17
    Me.chk_terminal_location.Text = "xShow terminals location"
    '
    'frm_countr_sessions
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1389, 715)
    Me.Name = "frm_countr_sessions"
    Me.Text = "frm_countr_sessions"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.fra_session_state.ResumeLayout(False)
    Me.gb_enabled.ResumeLayout(False)
    Me.gb_enabled.PerformLayout()
    Me.gb_area_island.ResumeLayout(False)
    Me.fra_terminal.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents opt_opening_session As System.Windows.Forms.RadioButton
  Friend WithEvents opt_movement_session As System.Windows.Forms.RadioButton
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents fra_session_state As System.Windows.Forms.GroupBox
  Friend WithEvents chk_session_closed As System.Windows.Forms.CheckBox
  Friend WithEvents chk_session_open As System.Windows.Forms.CheckBox
  Friend WithEvents gb_enabled As System.Windows.Forms.GroupBox
  Friend WithEvents chk_no As System.Windows.Forms.CheckBox
  Friend WithEvents chk_yes As System.Windows.Forms.CheckBox
  Friend WithEvents gb_area_island As System.Windows.Forms.GroupBox
  Friend WithEvents cmb_area As GUI_Controls.uc_combo
  Friend WithEvents cmb_bank As GUI_Controls.uc_combo
  Friend WithEvents ef_smoking As GUI_Controls.uc_entry_field
  Friend WithEvents ef_floor_id As GUI_Controls.uc_entry_field
  Friend WithEvents fra_terminal As System.Windows.Forms.GroupBox
  Friend WithEvents ef_terminal_code As GUI_Controls.uc_entry_field
  Friend WithEvents ef_terminal_name As GUI_Controls.uc_entry_field
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox
End Class
