<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_YesNo_web_browser
  Inherits GUI_Controls.frm_base

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
    Me.wb_browser = New System.Windows.Forms.WebBrowser
    Me.lbl_message = New System.Windows.Forms.Label
    Me.btn_ok = New GUI_Controls.uc_button
    Me.btn_cancel = New GUI_Controls.uc_button
    Me.SplitContainer1.Panel1.SuspendLayout()
    Me.SplitContainer1.Panel2.SuspendLayout()
    Me.SplitContainer1.SuspendLayout()
    Me.SuspendLayout()
    '
    'SplitContainer1
    '
    Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
    Me.SplitContainer1.Location = New System.Drawing.Point(5, 4)
    Me.SplitContainer1.Name = "SplitContainer1"
    Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
    '
    'SplitContainer1.Panel1
    '
    Me.SplitContainer1.Panel1.Controls.Add(Me.wb_browser)
    '
    'SplitContainer1.Panel2
    '
    Me.SplitContainer1.Panel2.Controls.Add(Me.lbl_message)
    Me.SplitContainer1.Panel2.Controls.Add(Me.btn_ok)
    Me.SplitContainer1.Panel2.Controls.Add(Me.btn_cancel)
    Me.SplitContainer1.Size = New System.Drawing.Size(562, 381)
    Me.SplitContainer1.SplitterDistance = 220
    Me.SplitContainer1.TabIndex = 0
    '
    'wb_browser
    '
    Me.wb_browser.Dock = System.Windows.Forms.DockStyle.Fill
    Me.wb_browser.Location = New System.Drawing.Point(0, 0)
    Me.wb_browser.MinimumSize = New System.Drawing.Size(23, 20)
    Me.wb_browser.Name = "wb_browser"
    Me.wb_browser.Size = New System.Drawing.Size(562, 220)
    Me.wb_browser.TabIndex = 0
    '
    'lbl_message
    '
    Me.lbl_message.AutoSize = True
    Me.lbl_message.Location = New System.Drawing.Point(44, 14)
    Me.lbl_message.MaximumSize = New System.Drawing.Size(448, 88)
    Me.lbl_message.Name = "lbl_message"
    Me.lbl_message.Size = New System.Drawing.Size(0, 13)
    Me.lbl_message.TabIndex = 2
    '
    'btn_ok
    '
    Me.btn_ok.DialogResult = System.Windows.Forms.DialogResult.Yes
    Me.btn_ok.Location = New System.Drawing.Point(173, 115)
    Me.btn_ok.Name = "btn_ok"
    Me.btn_ok.Size = New System.Drawing.Size(90, 30)
    Me.btn_ok.TabIndex = 1
    Me.btn_ok.ToolTipped = False
    Me.btn_ok.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_cancel
    '
    Me.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.No
    Me.btn_cancel.Location = New System.Drawing.Point(286, 115)
    Me.btn_cancel.Name = "btn_cancel"
    Me.btn_cancel.Size = New System.Drawing.Size(90, 30)
    Me.btn_cancel.TabIndex = 0
    Me.btn_cancel.ToolTipped = False
    Me.btn_cancel.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'frm_YesNo_web_browser
    '
    Me.AcceptButton = Me.btn_cancel
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.CancelButton = Me.btn_cancel
    Me.ClientSize = New System.Drawing.Size(572, 389)
    Me.Controls.Add(Me.SplitContainer1)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Name = "frm_YesNo_web_browser"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.ShowInTaskbar = False
    Me.Text = "frm_dialog_web_browser"
    Me.SplitContainer1.Panel1.ResumeLayout(False)
    Me.SplitContainer1.Panel2.ResumeLayout(False)
    Me.SplitContainer1.Panel2.PerformLayout()
    Me.SplitContainer1.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
  Friend WithEvents wb_browser As System.Windows.Forms.WebBrowser
  Friend WithEvents btn_cancel As GUI_Controls.uc_button
  Friend WithEvents btn_ok As GUI_Controls.uc_button
  Friend WithEvents lbl_message As System.Windows.Forms.Label
End Class
