<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_cashdesk_draws_configuration_edit
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_cashdesk_draws_configuration_edit))
    Me.chk_enable_draws = New System.Windows.Forms.CheckBox()
    Me.chk_show_draws_cd = New System.Windows.Forms.CheckBox()
    Me.chk_print_voucher_winner = New System.Windows.Forms.CheckBox()
    Me.chk_ask_participation = New System.Windows.Forms.CheckBox()
    Me.gb_cashier = New System.Windows.Forms.GroupBox()
    Me.ef_voucher_name_loser = New GUI_Controls.uc_entry_field()
    Me.chk_print_voucher_loser = New System.Windows.Forms.CheckBox()
    Me.ef_voucher_name_winner = New GUI_Controls.uc_entry_field()
    Me.ef_extracted_balls = New GUI_Controls.uc_entry_field()
    Me.ef_total_balls = New GUI_Controls.uc_entry_field()
    Me.gb_ball_config = New System.Windows.Forms.GroupBox()
    Me.ef_balls_participant = New GUI_Controls.uc_entry_field()
    Me.pnl_enable_draws = New System.Windows.Forms.Panel()
    Me.ef_participation_max_price = New GUI_Controls.uc_entry_field()
    Me.cmb_prize_mode = New GUI_Controls.uc_combo()
    Me.ef_participation_price = New GUI_Controls.uc_entry_field()
    Me.gb_prize_probability = New System.Windows.Forms.GroupBox()
    Me.ef_winners = New GUI_Controls.uc_entry_field()
    Me.ef_participants = New GUI_Controls.uc_entry_field()
    Me.gb_prizes = New System.Windows.Forms.GroupBox()
    Me.lbl_sum = New System.Windows.Forms.Label()
    Me.ef_probability_5 = New GUI_Controls.uc_entry_field()
    Me.ef_probability_4 = New GUI_Controls.uc_entry_field()
    Me.ef_probability_3 = New GUI_Controls.uc_entry_field()
    Me.ef_probability_2 = New GUI_Controls.uc_entry_field()
    Me.ef_probability_1 = New GUI_Controls.uc_entry_field()
    Me.lbl_probability = New System.Windows.Forms.Label()
    Me.lbl_tax_provisions_remarks = New System.Windows.Forms.Label()
    Me.ef_tax_provisions_loser = New GUI_Controls.uc_entry_field()
    Me.ef_tax_provisions_4 = New GUI_Controls.uc_entry_field()
    Me.ef_tax_provisions_3 = New GUI_Controls.uc_entry_field()
    Me.ef_tax_provisions_2 = New GUI_Controls.uc_entry_field()
    Me.ef_tax_provisions_1 = New GUI_Controls.uc_entry_field()
    Me.lbl_tax_provisions = New System.Windows.Forms.Label()
    Me.cmb_loser_credit_type = New GUI_Controls.uc_combo()
    Me.cmb_prize4_credit_type = New GUI_Controls.uc_combo()
    Me.cmb_prize3_credit_type = New GUI_Controls.uc_combo()
    Me.cmb_prize2_credit_type = New GUI_Controls.uc_combo()
    Me.cmb_prize1_credit_type = New GUI_Controls.uc_combo()
    Me.lbl_cpromo_explanation = New System.Windows.Forms.Label()
    Me.lbl_prize_explanation = New System.Windows.Forms.Label()
    Me.lbl_prizes_nonredemable = New System.Windows.Forms.Label()
    Me.lbl_prizes_example = New System.Windows.Forms.Label()
    Me.chk_enable_loser = New System.Windows.Forms.CheckBox()
    Me.chk_enable_winner_4 = New System.Windows.Forms.CheckBox()
    Me.lbl_example_loser = New System.Windows.Forms.Label()
    Me.chk_enable_winner_3 = New System.Windows.Forms.CheckBox()
    Me.lbl_loser = New System.Windows.Forms.Label()
    Me.ef_access_perc_loser = New GUI_Controls.uc_entry_field()
    Me.chk_enable_winner_2 = New System.Windows.Forms.CheckBox()
    Me.ef_cprize_percentage = New GUI_Controls.uc_entry_field()
    Me.chk_enable_winner_1 = New System.Windows.Forms.CheckBox()
    Me.ef_cprize_fixed = New GUI_Controls.uc_entry_field()
    Me.lbl_winner_prize4 = New System.Windows.Forms.Label()
    Me.lbl_example_winner_4 = New System.Windows.Forms.Label()
    Me.ef_access_perc_winner_4 = New GUI_Controls.uc_entry_field()
    Me.ef_perc_4 = New GUI_Controls.uc_entry_field()
    Me.lbl_example_winner_3 = New System.Windows.Forms.Label()
    Me.ef_fixed_4 = New GUI_Controls.uc_entry_field()
    Me.lbl_winner_prize3 = New System.Windows.Forms.Label()
    Me.ef_access_perc_winner_3 = New GUI_Controls.uc_entry_field()
    Me.ef_perc_3 = New GUI_Controls.uc_entry_field()
    Me.lbl_winner_prize2 = New System.Windows.Forms.Label()
    Me.ef_fixed_3 = New GUI_Controls.uc_entry_field()
    Me.lbl_example_winner_2 = New System.Windows.Forms.Label()
    Me.ef_access_perc_winner_2 = New GUI_Controls.uc_entry_field()
    Me.ef_perc_2 = New GUI_Controls.uc_entry_field()
    Me.lbl_example_winner_1 = New System.Windows.Forms.Label()
    Me.ef_fixed_2 = New GUI_Controls.uc_entry_field()
    Me.ef_access_perc_winner_1 = New GUI_Controls.uc_entry_field()
    Me.lbl_winner_prize1 = New System.Windows.Forms.Label()
    Me.ef_perc_1 = New GUI_Controls.uc_entry_field()
    Me.ef_fixed_1 = New GUI_Controls.uc_entry_field()
    Me.lbl_prizes_category = New System.Windows.Forms.Label()
    Me.lbl_prizes_enable = New System.Windows.Forms.Label()
    Me.lbl_prizes_fixed = New System.Windows.Forms.Label()
    Me.lbl_prizes_percentage = New System.Windows.Forms.Label()
    Me.lbl_prizes_access_perc = New System.Windows.Forms.Label()
    Me.gb_draw_connection = New System.Windows.Forms.GroupBox()
    Me.panel_webservice = New System.Windows.Forms.Panel()
    Me.rb_opt_webservice = New System.Windows.Forms.RadioButton()
    Me.lbl_center_address_1 = New System.Windows.Forms.Label()
    Me.panel_not_connected = New System.Windows.Forms.Panel()
    Me.rb_recharge_no_red = New System.Windows.Forms.RadioButton()
    Me.lbl_not_connected = New System.Windows.Forms.Label()
    Me.rb_recharge_red = New System.Windows.Forms.RadioButton()
    Me.rb_dont_recharge = New System.Windows.Forms.RadioButton()
    Me.lbl_center_address_2 = New System.Windows.Forms.Label()
    Me.ef_center_address_2 = New GUI_Controls.uc_entry_field()
    Me.ef_center_address_1 = New GUI_Controls.uc_entry_field()
    Me.rb_opt_ext_db = New System.Windows.Forms.RadioButton()
    Me.rb_opt_Local_DB = New System.Windows.Forms.RadioButton()
    Me.gb_ext_db_connection_string = New System.Windows.Forms.GroupBox()
    Me.ef_extdb_server_2 = New GUI_Controls.uc_entry_field()
    Me.ef_extdb_pwd = New GUI_Controls.uc_entry_field()
    Me.ef_extdb_server_1 = New GUI_Controls.uc_entry_field()
    Me.ef_extdb_user = New GUI_Controls.uc_entry_field()
    Me.ef_extdb_database = New GUI_Controls.uc_entry_field()
    Me.ef_terminal_game_timeout = New GUI_Controls.uc_entry_field()
    Me.ef_terminal_game_url = New GUI_Controls.uc_entry_field()
    Me.gb_terminal = New System.Windows.Forms.GroupBox()
    Me.chk_allow_withdraw_pending = New System.Windows.Forms.CheckBox()
    Me.chk_allow_cancel_recharge = New System.Windows.Forms.CheckBox()
    Me.ef_terminal_game_draws = New GUI_Controls.uc_entry_field()
    Me.chk_player_can_cancel_draw = New System.Windows.Forms.CheckBox()
    Me.chk_show_buy_dialog = New System.Windows.Forms.CheckBox()
    Me.ef_terminal_game_name = New GUI_Controls.uc_entry_field()
    Me.panel_data.SuspendLayout()
    Me.gb_cashier.SuspendLayout()
    Me.gb_ball_config.SuspendLayout()
    Me.pnl_enable_draws.SuspendLayout()
    Me.gb_prize_probability.SuspendLayout()
    Me.gb_prizes.SuspendLayout()
    Me.gb_draw_connection.SuspendLayout()
    Me.panel_webservice.SuspendLayout()
    Me.panel_not_connected.SuspendLayout()
    Me.gb_ext_db_connection_string.SuspendLayout()
    Me.gb_terminal.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_terminal)
    Me.panel_data.Controls.Add(Me.pnl_enable_draws)
    Me.panel_data.Controls.Add(Me.chk_enable_draws)
    Me.panel_data.Size = New System.Drawing.Size(968, 530)
    '
    'chk_enable_draws
    '
    Me.chk_enable_draws.AutoSize = True
    Me.chk_enable_draws.Location = New System.Drawing.Point(27, 4)
    Me.chk_enable_draws.Name = "chk_enable_draws"
    Me.chk_enable_draws.Size = New System.Drawing.Size(107, 17)
    Me.chk_enable_draws.TabIndex = 0
    Me.chk_enable_draws.Text = "xEnableDraws"
    Me.chk_enable_draws.UseVisualStyleBackColor = True
    '
    'chk_show_draws_cd
    '
    Me.chk_show_draws_cd.AutoSize = True
    Me.chk_show_draws_cd.Location = New System.Drawing.Point(17, 17)
    Me.chk_show_draws_cd.Name = "chk_show_draws_cd"
    Me.chk_show_draws_cd.Size = New System.Drawing.Size(100, 17)
    Me.chk_show_draws_cd.TabIndex = 2
    Me.chk_show_draws_cd.Text = "xShowDraws"
    Me.chk_show_draws_cd.UseVisualStyleBackColor = True
    '
    'chk_print_voucher_winner
    '
    Me.chk_print_voucher_winner.AutoSize = True
    Me.chk_print_voucher_winner.Location = New System.Drawing.Point(17, 63)
    Me.chk_print_voucher_winner.Name = "chk_print_voucher_winner"
    Me.chk_print_voucher_winner.Size = New System.Drawing.Size(145, 17)
    Me.chk_print_voucher_winner.TabIndex = 2
    Me.chk_print_voucher_winner.Text = "xPrintVoucherWinner"
    Me.chk_print_voucher_winner.UseVisualStyleBackColor = True
    '
    'chk_ask_participation
    '
    Me.chk_ask_participation.AutoSize = True
    Me.chk_ask_participation.Location = New System.Drawing.Point(17, 40)
    Me.chk_ask_participation.Name = "chk_ask_participation"
    Me.chk_ask_participation.Size = New System.Drawing.Size(124, 17)
    Me.chk_ask_participation.TabIndex = 1
    Me.chk_ask_participation.Text = "xAskParticipation"
    Me.chk_ask_participation.UseVisualStyleBackColor = True
    '
    'gb_cashier
    '
    Me.gb_cashier.Controls.Add(Me.ef_voucher_name_loser)
    Me.gb_cashier.Controls.Add(Me.chk_print_voucher_loser)
    Me.gb_cashier.Controls.Add(Me.ef_voucher_name_winner)
    Me.gb_cashier.Controls.Add(Me.chk_ask_participation)
    Me.gb_cashier.Controls.Add(Me.chk_print_voucher_winner)
    Me.gb_cashier.Controls.Add(Me.chk_show_draws_cd)
    Me.gb_cashier.Location = New System.Drawing.Point(13, 3)
    Me.gb_cashier.Name = "gb_cashier"
    Me.gb_cashier.Size = New System.Drawing.Size(400, 182)
    Me.gb_cashier.TabIndex = 0
    Me.gb_cashier.TabStop = False
    Me.gb_cashier.Text = "xCashier"
    '
    'ef_voucher_name_loser
    '
    Me.ef_voucher_name_loser.DoubleValue = 0.0R
    Me.ef_voucher_name_loser.IntegerValue = 0
    Me.ef_voucher_name_loser.IsReadOnly = False
    Me.ef_voucher_name_loser.Location = New System.Drawing.Point(17, 141)
    Me.ef_voucher_name_loser.Name = "ef_voucher_name_loser"
    Me.ef_voucher_name_loser.PlaceHolder = Nothing
    Me.ef_voucher_name_loser.ShortcutsEnabled = True
    Me.ef_voucher_name_loser.Size = New System.Drawing.Size(361, 24)
    Me.ef_voucher_name_loser.SufixText = "Sufix Text"
    Me.ef_voucher_name_loser.SufixTextVisible = True
    Me.ef_voucher_name_loser.TabIndex = 0
    Me.ef_voucher_name_loser.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_voucher_name_loser.TextValue = ""
    Me.ef_voucher_name_loser.TextWidth = 200
    Me.ef_voucher_name_loser.Value = ""
    Me.ef_voucher_name_loser.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_print_voucher_loser
    '
    Me.chk_print_voucher_loser.AutoSize = True
    Me.chk_print_voucher_loser.Location = New System.Drawing.Point(17, 118)
    Me.chk_print_voucher_loser.Name = "chk_print_voucher_loser"
    Me.chk_print_voucher_loser.Size = New System.Drawing.Size(136, 17)
    Me.chk_print_voucher_loser.TabIndex = 4
    Me.chk_print_voucher_loser.Text = "xPrintVoucherLoser"
    Me.chk_print_voucher_loser.UseVisualStyleBackColor = True
    '
    'ef_voucher_name_winner
    '
    Me.ef_voucher_name_winner.DoubleValue = 0.0R
    Me.ef_voucher_name_winner.IntegerValue = 0
    Me.ef_voucher_name_winner.IsReadOnly = False
    Me.ef_voucher_name_winner.Location = New System.Drawing.Point(17, 86)
    Me.ef_voucher_name_winner.Name = "ef_voucher_name_winner"
    Me.ef_voucher_name_winner.PlaceHolder = Nothing
    Me.ef_voucher_name_winner.ShortcutsEnabled = True
    Me.ef_voucher_name_winner.Size = New System.Drawing.Size(361, 24)
    Me.ef_voucher_name_winner.SufixText = "Sufix Text"
    Me.ef_voucher_name_winner.SufixTextVisible = True
    Me.ef_voucher_name_winner.TabIndex = 3
    Me.ef_voucher_name_winner.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_voucher_name_winner.TextValue = ""
    Me.ef_voucher_name_winner.TextWidth = 200
    Me.ef_voucher_name_winner.Value = ""
    Me.ef_voucher_name_winner.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_extracted_balls
    '
    Me.ef_extracted_balls.DoubleValue = 0.0R
    Me.ef_extracted_balls.IntegerValue = 0
    Me.ef_extracted_balls.IsReadOnly = False
    Me.ef_extracted_balls.Location = New System.Drawing.Point(160, 47)
    Me.ef_extracted_balls.Name = "ef_extracted_balls"
    Me.ef_extracted_balls.PlaceHolder = Nothing
    Me.ef_extracted_balls.ShortcutsEnabled = True
    Me.ef_extracted_balls.Size = New System.Drawing.Size(348, 24)
    Me.ef_extracted_balls.SufixText = "Sufix Text"
    Me.ef_extracted_balls.SufixTextVisible = True
    Me.ef_extracted_balls.TabIndex = 1
    Me.ef_extracted_balls.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_extracted_balls.TextValue = ""
    Me.ef_extracted_balls.TextWidth = 300
    Me.ef_extracted_balls.Value = ""
    Me.ef_extracted_balls.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_total_balls
    '
    Me.ef_total_balls.DoubleValue = 0.0R
    Me.ef_total_balls.IntegerValue = 0
    Me.ef_total_balls.IsReadOnly = False
    Me.ef_total_balls.Location = New System.Drawing.Point(160, 17)
    Me.ef_total_balls.Name = "ef_total_balls"
    Me.ef_total_balls.PlaceHolder = Nothing
    Me.ef_total_balls.ShortcutsEnabled = True
    Me.ef_total_balls.Size = New System.Drawing.Size(348, 24)
    Me.ef_total_balls.SufixText = "Sufix Text"
    Me.ef_total_balls.SufixTextVisible = True
    Me.ef_total_balls.TabIndex = 0
    Me.ef_total_balls.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_total_balls.TextValue = ""
    Me.ef_total_balls.TextWidth = 300
    Me.ef_total_balls.Value = ""
    Me.ef_total_balls.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_ball_config
    '
    Me.gb_ball_config.Controls.Add(Me.ef_balls_participant)
    Me.gb_ball_config.Controls.Add(Me.ef_total_balls)
    Me.gb_ball_config.Controls.Add(Me.ef_extracted_balls)
    Me.gb_ball_config.Location = New System.Drawing.Point(433, 0)
    Me.gb_ball_config.Name = "gb_ball_config"
    Me.gb_ball_config.Size = New System.Drawing.Size(517, 105)
    Me.gb_ball_config.TabIndex = 4
    Me.gb_ball_config.TabStop = False
    Me.gb_ball_config.Text = "xBallConfig"
    '
    'ef_balls_participant
    '
    Me.ef_balls_participant.DoubleValue = 0.0R
    Me.ef_balls_participant.IntegerValue = 0
    Me.ef_balls_participant.IsReadOnly = False
    Me.ef_balls_participant.Location = New System.Drawing.Point(160, 75)
    Me.ef_balls_participant.Name = "ef_balls_participant"
    Me.ef_balls_participant.PlaceHolder = Nothing
    Me.ef_balls_participant.ShortcutsEnabled = True
    Me.ef_balls_participant.Size = New System.Drawing.Size(348, 24)
    Me.ef_balls_participant.SufixText = "Sufix Text"
    Me.ef_balls_participant.SufixTextVisible = True
    Me.ef_balls_participant.TabIndex = 2
    Me.ef_balls_participant.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_balls_participant.TextValue = ""
    Me.ef_balls_participant.TextWidth = 300
    Me.ef_balls_participant.Value = ""
    Me.ef_balls_participant.ValueForeColor = System.Drawing.Color.Blue
    '
    'pnl_enable_draws
    '
    Me.pnl_enable_draws.Controls.Add(Me.ef_participation_max_price)
    Me.pnl_enable_draws.Controls.Add(Me.cmb_prize_mode)
    Me.pnl_enable_draws.Controls.Add(Me.ef_participation_price)
    Me.pnl_enable_draws.Controls.Add(Me.gb_prize_probability)
    Me.pnl_enable_draws.Controls.Add(Me.gb_prizes)
    Me.pnl_enable_draws.Controls.Add(Me.gb_ball_config)
    Me.pnl_enable_draws.Controls.Add(Me.gb_draw_connection)
    Me.pnl_enable_draws.Controls.Add(Me.gb_cashier)
    Me.pnl_enable_draws.Controls.Add(Me.gb_ext_db_connection_string)
    Me.pnl_enable_draws.Location = New System.Drawing.Point(6, 27)
    Me.pnl_enable_draws.Name = "pnl_enable_draws"
    Me.pnl_enable_draws.Size = New System.Drawing.Size(959, 502)
    Me.pnl_enable_draws.TabIndex = 1
    '
    'ef_participation_max_price
    '
    Me.ef_participation_max_price.DoubleValue = 0.0R
    Me.ef_participation_max_price.IntegerValue = 0
    Me.ef_participation_max_price.IsReadOnly = False
    Me.ef_participation_max_price.Location = New System.Drawing.Point(423, 153)
    Me.ef_participation_max_price.Name = "ef_participation_max_price"
    Me.ef_participation_max_price.PlaceHolder = Nothing
    Me.ef_participation_max_price.ShortcutsEnabled = True
    Me.ef_participation_max_price.Size = New System.Drawing.Size(208, 24)
    Me.ef_participation_max_price.SufixText = "Sufix Text"
    Me.ef_participation_max_price.SufixTextVisible = True
    Me.ef_participation_max_price.TabIndex = 7
    Me.ef_participation_max_price.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_participation_max_price.TextValue = ""
    Me.ef_participation_max_price.TextWidth = 130
    Me.ef_participation_max_price.Value = ""
    Me.ef_participation_max_price.ValueForeColor = System.Drawing.Color.Blue
    Me.ef_participation_max_price.Visible = False
    '
    'cmb_prize_mode
    '
    Me.cmb_prize_mode.AllowUnlistedValues = False
    Me.cmb_prize_mode.AutoCompleteMode = False
    Me.cmb_prize_mode.IsReadOnly = False
    Me.cmb_prize_mode.Location = New System.Drawing.Point(650, 121)
    Me.cmb_prize_mode.Name = "cmb_prize_mode"
    Me.cmb_prize_mode.SelectedIndex = -1
    Me.cmb_prize_mode.Size = New System.Drawing.Size(300, 24)
    Me.cmb_prize_mode.SufixText = "Sufix Text"
    Me.cmb_prize_mode.SufixTextVisible = True
    Me.cmb_prize_mode.TabIndex = 5
    Me.cmb_prize_mode.TextCombo = Nothing
    Me.cmb_prize_mode.TextWidth = 160
    '
    'ef_participation_price
    '
    Me.ef_participation_price.DoubleValue = 0.0R
    Me.ef_participation_price.IntegerValue = 0
    Me.ef_participation_price.IsReadOnly = False
    Me.ef_participation_price.Location = New System.Drawing.Point(732, 152)
    Me.ef_participation_price.Name = "ef_participation_price"
    Me.ef_participation_price.PlaceHolder = Nothing
    Me.ef_participation_price.ShortcutsEnabled = True
    Me.ef_participation_price.Size = New System.Drawing.Size(218, 24)
    Me.ef_participation_price.SufixText = "Sufix Text"
    Me.ef_participation_price.SufixTextVisible = True
    Me.ef_participation_price.TabIndex = 6
    Me.ef_participation_price.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_participation_price.TextValue = ""
    Me.ef_participation_price.TextWidth = 160
    Me.ef_participation_price.Value = ""
    Me.ef_participation_price.ValueForeColor = System.Drawing.Color.Blue
    Me.ef_participation_price.Visible = False
    '
    'gb_prize_probability
    '
    Me.gb_prize_probability.Controls.Add(Me.ef_winners)
    Me.gb_prize_probability.Controls.Add(Me.ef_participants)
    Me.gb_prize_probability.Location = New System.Drawing.Point(3, 237)
    Me.gb_prize_probability.Name = "gb_prize_probability"
    Me.gb_prize_probability.Size = New System.Drawing.Size(10, 10)
    Me.gb_prize_probability.TabIndex = 12
    Me.gb_prize_probability.TabStop = False
    Me.gb_prize_probability.Text = "xPrizeProbability"
    Me.gb_prize_probability.Visible = False
    '
    'ef_winners
    '
    Me.ef_winners.DoubleValue = 0.0R
    Me.ef_winners.IntegerValue = 0
    Me.ef_winners.IsReadOnly = False
    Me.ef_winners.Location = New System.Drawing.Point(149, 45)
    Me.ef_winners.Name = "ef_winners"
    Me.ef_winners.PlaceHolder = Nothing
    Me.ef_winners.ShortcutsEnabled = True
    Me.ef_winners.Size = New System.Drawing.Size(228, 24)
    Me.ef_winners.SufixText = "Sufix Text"
    Me.ef_winners.SufixTextVisible = True
    Me.ef_winners.TabIndex = 1
    Me.ef_winners.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_winners.TextValue = ""
    Me.ef_winners.TextWidth = 180
    Me.ef_winners.Value = ""
    Me.ef_winners.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_participants
    '
    Me.ef_participants.DoubleValue = 0.0R
    Me.ef_participants.IntegerValue = 0
    Me.ef_participants.IsReadOnly = False
    Me.ef_participants.Location = New System.Drawing.Point(149, 14)
    Me.ef_participants.Name = "ef_participants"
    Me.ef_participants.PlaceHolder = Nothing
    Me.ef_participants.ShortcutsEnabled = True
    Me.ef_participants.Size = New System.Drawing.Size(228, 24)
    Me.ef_participants.SufixText = "Sufix Text"
    Me.ef_participants.SufixTextVisible = True
    Me.ef_participants.TabIndex = 0
    Me.ef_participants.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_participants.TextValue = ""
    Me.ef_participants.TextWidth = 180
    Me.ef_participants.Value = ""
    Me.ef_participants.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_prizes
    '
    Me.gb_prizes.Controls.Add(Me.lbl_sum)
    Me.gb_prizes.Controls.Add(Me.ef_probability_5)
    Me.gb_prizes.Controls.Add(Me.ef_probability_4)
    Me.gb_prizes.Controls.Add(Me.ef_probability_3)
    Me.gb_prizes.Controls.Add(Me.ef_probability_2)
    Me.gb_prizes.Controls.Add(Me.ef_probability_1)
    Me.gb_prizes.Controls.Add(Me.lbl_probability)
    Me.gb_prizes.Controls.Add(Me.lbl_tax_provisions_remarks)
    Me.gb_prizes.Controls.Add(Me.ef_tax_provisions_loser)
    Me.gb_prizes.Controls.Add(Me.ef_tax_provisions_4)
    Me.gb_prizes.Controls.Add(Me.ef_tax_provisions_3)
    Me.gb_prizes.Controls.Add(Me.ef_tax_provisions_2)
    Me.gb_prizes.Controls.Add(Me.ef_tax_provisions_1)
    Me.gb_prizes.Controls.Add(Me.lbl_tax_provisions)
    Me.gb_prizes.Controls.Add(Me.cmb_loser_credit_type)
    Me.gb_prizes.Controls.Add(Me.cmb_prize4_credit_type)
    Me.gb_prizes.Controls.Add(Me.cmb_prize3_credit_type)
    Me.gb_prizes.Controls.Add(Me.cmb_prize2_credit_type)
    Me.gb_prizes.Controls.Add(Me.cmb_prize1_credit_type)
    Me.gb_prizes.Controls.Add(Me.lbl_cpromo_explanation)
    Me.gb_prizes.Controls.Add(Me.lbl_prize_explanation)
    Me.gb_prizes.Controls.Add(Me.lbl_prizes_nonredemable)
    Me.gb_prizes.Controls.Add(Me.lbl_prizes_example)
    Me.gb_prizes.Controls.Add(Me.chk_enable_loser)
    Me.gb_prizes.Controls.Add(Me.chk_enable_winner_4)
    Me.gb_prizes.Controls.Add(Me.lbl_example_loser)
    Me.gb_prizes.Controls.Add(Me.chk_enable_winner_3)
    Me.gb_prizes.Controls.Add(Me.lbl_loser)
    Me.gb_prizes.Controls.Add(Me.ef_access_perc_loser)
    Me.gb_prizes.Controls.Add(Me.chk_enable_winner_2)
    Me.gb_prizes.Controls.Add(Me.ef_cprize_percentage)
    Me.gb_prizes.Controls.Add(Me.chk_enable_winner_1)
    Me.gb_prizes.Controls.Add(Me.ef_cprize_fixed)
    Me.gb_prizes.Controls.Add(Me.lbl_winner_prize4)
    Me.gb_prizes.Controls.Add(Me.lbl_example_winner_4)
    Me.gb_prizes.Controls.Add(Me.ef_access_perc_winner_4)
    Me.gb_prizes.Controls.Add(Me.ef_perc_4)
    Me.gb_prizes.Controls.Add(Me.lbl_example_winner_3)
    Me.gb_prizes.Controls.Add(Me.ef_fixed_4)
    Me.gb_prizes.Controls.Add(Me.lbl_winner_prize3)
    Me.gb_prizes.Controls.Add(Me.ef_access_perc_winner_3)
    Me.gb_prizes.Controls.Add(Me.ef_perc_3)
    Me.gb_prizes.Controls.Add(Me.lbl_winner_prize2)
    Me.gb_prizes.Controls.Add(Me.ef_fixed_3)
    Me.gb_prizes.Controls.Add(Me.lbl_example_winner_2)
    Me.gb_prizes.Controls.Add(Me.ef_access_perc_winner_2)
    Me.gb_prizes.Controls.Add(Me.ef_perc_2)
    Me.gb_prizes.Controls.Add(Me.lbl_example_winner_1)
    Me.gb_prizes.Controls.Add(Me.ef_fixed_2)
    Me.gb_prizes.Controls.Add(Me.ef_access_perc_winner_1)
    Me.gb_prizes.Controls.Add(Me.lbl_winner_prize1)
    Me.gb_prizes.Controls.Add(Me.ef_perc_1)
    Me.gb_prizes.Controls.Add(Me.ef_fixed_1)
    Me.gb_prizes.Controls.Add(Me.lbl_prizes_category)
    Me.gb_prizes.Controls.Add(Me.lbl_prizes_enable)
    Me.gb_prizes.Controls.Add(Me.lbl_prizes_fixed)
    Me.gb_prizes.Controls.Add(Me.lbl_prizes_percentage)
    Me.gb_prizes.Controls.Add(Me.lbl_prizes_access_perc)
    Me.gb_prizes.Location = New System.Drawing.Point(14, 211)
    Me.gb_prizes.Name = "gb_prizes"
    Me.gb_prizes.Size = New System.Drawing.Size(936, 280)
    Me.gb_prizes.TabIndex = 8
    Me.gb_prizes.TabStop = False
    Me.gb_prizes.Text = "xPrizes"
    '
    'lbl_sum
    '
    Me.lbl_sum.AutoSize = True
    Me.lbl_sum.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_sum.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_sum.Location = New System.Drawing.Point(869, 196)
    Me.lbl_sum.MaximumSize = New System.Drawing.Size(880, 0)
    Me.lbl_sum.Name = "lbl_sum"
    Me.lbl_sum.Size = New System.Drawing.Size(35, 14)
    Me.lbl_sum.TabIndex = 52
    Me.lbl_sum.Text = "xSum"
    '
    'ef_probability_5
    '
    Me.ef_probability_5.DoubleValue = 0.0R
    Me.ef_probability_5.IntegerValue = 0
    Me.ef_probability_5.IsReadOnly = False
    Me.ef_probability_5.Location = New System.Drawing.Point(846, 165)
    Me.ef_probability_5.Name = "ef_probability_5"
    Me.ef_probability_5.PlaceHolder = Nothing
    Me.ef_probability_5.ShortcutsEnabled = True
    Me.ef_probability_5.Size = New System.Drawing.Size(79, 24)
    Me.ef_probability_5.SufixText = "Sufix Text"
    Me.ef_probability_5.SufixTextVisible = True
    Me.ef_probability_5.SufixTextWidth = 14
    Me.ef_probability_5.TabIndex = 24
    Me.ef_probability_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_probability_5.TextValue = ""
    Me.ef_probability_5.TextWidth = 0
    Me.ef_probability_5.Value = ""
    Me.ef_probability_5.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_probability_4
    '
    Me.ef_probability_4.DoubleValue = 0.0R
    Me.ef_probability_4.IntegerValue = 0
    Me.ef_probability_4.IsReadOnly = False
    Me.ef_probability_4.Location = New System.Drawing.Point(846, 135)
    Me.ef_probability_4.Name = "ef_probability_4"
    Me.ef_probability_4.PlaceHolder = Nothing
    Me.ef_probability_4.ShortcutsEnabled = True
    Me.ef_probability_4.Size = New System.Drawing.Size(79, 24)
    Me.ef_probability_4.SufixText = "Sufix Text"
    Me.ef_probability_4.SufixTextVisible = True
    Me.ef_probability_4.SufixTextWidth = 14
    Me.ef_probability_4.TabIndex = 19
    Me.ef_probability_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_probability_4.TextValue = ""
    Me.ef_probability_4.TextWidth = 0
    Me.ef_probability_4.Value = ""
    Me.ef_probability_4.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_probability_3
    '
    Me.ef_probability_3.DoubleValue = 0.0R
    Me.ef_probability_3.IntegerValue = 0
    Me.ef_probability_3.IsReadOnly = False
    Me.ef_probability_3.Location = New System.Drawing.Point(846, 105)
    Me.ef_probability_3.Name = "ef_probability_3"
    Me.ef_probability_3.PlaceHolder = Nothing
    Me.ef_probability_3.ShortcutsEnabled = True
    Me.ef_probability_3.Size = New System.Drawing.Size(79, 24)
    Me.ef_probability_3.SufixText = "Sufix Text"
    Me.ef_probability_3.SufixTextVisible = True
    Me.ef_probability_3.SufixTextWidth = 14
    Me.ef_probability_3.TabIndex = 14
    Me.ef_probability_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_probability_3.TextValue = ""
    Me.ef_probability_3.TextWidth = 0
    Me.ef_probability_3.Value = ""
    Me.ef_probability_3.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_probability_2
    '
    Me.ef_probability_2.DoubleValue = 0.0R
    Me.ef_probability_2.IntegerValue = 0
    Me.ef_probability_2.IsReadOnly = False
    Me.ef_probability_2.Location = New System.Drawing.Point(846, 75)
    Me.ef_probability_2.Name = "ef_probability_2"
    Me.ef_probability_2.PlaceHolder = Nothing
    Me.ef_probability_2.ShortcutsEnabled = True
    Me.ef_probability_2.Size = New System.Drawing.Size(79, 24)
    Me.ef_probability_2.SufixText = "Sufix Text"
    Me.ef_probability_2.SufixTextVisible = True
    Me.ef_probability_2.SufixTextWidth = 14
    Me.ef_probability_2.TabIndex = 9
    Me.ef_probability_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_probability_2.TextValue = ""
    Me.ef_probability_2.TextWidth = 0
    Me.ef_probability_2.Value = ""
    Me.ef_probability_2.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_probability_1
    '
    Me.ef_probability_1.DoubleValue = 0.0R
    Me.ef_probability_1.IntegerValue = 0
    Me.ef_probability_1.IsReadOnly = False
    Me.ef_probability_1.Location = New System.Drawing.Point(846, 45)
    Me.ef_probability_1.Name = "ef_probability_1"
    Me.ef_probability_1.PlaceHolder = Nothing
    Me.ef_probability_1.ShortcutsEnabled = True
    Me.ef_probability_1.Size = New System.Drawing.Size(79, 24)
    Me.ef_probability_1.SufixText = "Sufix Text"
    Me.ef_probability_1.SufixTextVisible = True
    Me.ef_probability_1.SufixTextWidth = 14
    Me.ef_probability_1.TabIndex = 4
    Me.ef_probability_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_probability_1.TextValue = ""
    Me.ef_probability_1.TextWidth = 0
    Me.ef_probability_1.Value = ""
    Me.ef_probability_1.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_probability
    '
    Me.lbl_probability.AutoSize = True
    Me.lbl_probability.Location = New System.Drawing.Point(843, 18)
    Me.lbl_probability.MaximumSize = New System.Drawing.Size(110, 0)
    Me.lbl_probability.Name = "lbl_probability"
    Me.lbl_probability.Size = New System.Drawing.Size(74, 13)
    Me.lbl_probability.TabIndex = 51
    Me.lbl_probability.Text = "xProbability"
    '
    'lbl_tax_provisions_remarks
    '
    Me.lbl_tax_provisions_remarks.AutoSize = True
    Me.lbl_tax_provisions_remarks.Font = New System.Drawing.Font("Courier New", 8.25!)
    Me.lbl_tax_provisions_remarks.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_tax_provisions_remarks.Location = New System.Drawing.Point(20, 251)
    Me.lbl_tax_provisions_remarks.MaximumSize = New System.Drawing.Size(880, 0)
    Me.lbl_tax_provisions_remarks.Name = "lbl_tax_provisions_remarks"
    Me.lbl_tax_provisions_remarks.Size = New System.Drawing.Size(175, 14)
    Me.lbl_tax_provisions_remarks.TabIndex = 50
    Me.lbl_tax_provisions_remarks.Text = "x Tax Provisions Remarks"
    '
    'ef_tax_provisions_loser
    '
    Me.ef_tax_provisions_loser.DoubleValue = 0.0R
    Me.ef_tax_provisions_loser.IntegerValue = 0
    Me.ef_tax_provisions_loser.IsReadOnly = False
    Me.ef_tax_provisions_loser.Location = New System.Drawing.Point(625, 165)
    Me.ef_tax_provisions_loser.Name = "ef_tax_provisions_loser"
    Me.ef_tax_provisions_loser.PlaceHolder = Nothing
    Me.ef_tax_provisions_loser.ShortcutsEnabled = True
    Me.ef_tax_provisions_loser.Size = New System.Drawing.Size(79, 24)
    Me.ef_tax_provisions_loser.SufixText = "Sufix Text"
    Me.ef_tax_provisions_loser.SufixTextVisible = True
    Me.ef_tax_provisions_loser.SufixTextWidth = 14
    Me.ef_tax_provisions_loser.TabIndex = 48
    Me.ef_tax_provisions_loser.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_tax_provisions_loser.TextValue = ""
    Me.ef_tax_provisions_loser.TextWidth = 0
    Me.ef_tax_provisions_loser.Value = ""
    Me.ef_tax_provisions_loser.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_tax_provisions_4
    '
    Me.ef_tax_provisions_4.DoubleValue = 0.0R
    Me.ef_tax_provisions_4.IntegerValue = 0
    Me.ef_tax_provisions_4.IsReadOnly = False
    Me.ef_tax_provisions_4.Location = New System.Drawing.Point(625, 135)
    Me.ef_tax_provisions_4.Name = "ef_tax_provisions_4"
    Me.ef_tax_provisions_4.PlaceHolder = Nothing
    Me.ef_tax_provisions_4.ShortcutsEnabled = True
    Me.ef_tax_provisions_4.Size = New System.Drawing.Size(79, 24)
    Me.ef_tax_provisions_4.SufixText = "Sufix Text"
    Me.ef_tax_provisions_4.SufixTextVisible = True
    Me.ef_tax_provisions_4.SufixTextWidth = 14
    Me.ef_tax_provisions_4.TabIndex = 47
    Me.ef_tax_provisions_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_tax_provisions_4.TextValue = ""
    Me.ef_tax_provisions_4.TextWidth = 0
    Me.ef_tax_provisions_4.Value = ""
    Me.ef_tax_provisions_4.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_tax_provisions_3
    '
    Me.ef_tax_provisions_3.DoubleValue = 0.0R
    Me.ef_tax_provisions_3.IntegerValue = 0
    Me.ef_tax_provisions_3.IsReadOnly = False
    Me.ef_tax_provisions_3.Location = New System.Drawing.Point(625, 105)
    Me.ef_tax_provisions_3.Name = "ef_tax_provisions_3"
    Me.ef_tax_provisions_3.PlaceHolder = Nothing
    Me.ef_tax_provisions_3.ShortcutsEnabled = True
    Me.ef_tax_provisions_3.Size = New System.Drawing.Size(79, 24)
    Me.ef_tax_provisions_3.SufixText = "Sufix Text"
    Me.ef_tax_provisions_3.SufixTextVisible = True
    Me.ef_tax_provisions_3.SufixTextWidth = 14
    Me.ef_tax_provisions_3.TabIndex = 46
    Me.ef_tax_provisions_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_tax_provisions_3.TextValue = ""
    Me.ef_tax_provisions_3.TextWidth = 0
    Me.ef_tax_provisions_3.Value = ""
    Me.ef_tax_provisions_3.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_tax_provisions_2
    '
    Me.ef_tax_provisions_2.DoubleValue = 0.0R
    Me.ef_tax_provisions_2.IntegerValue = 0
    Me.ef_tax_provisions_2.IsReadOnly = False
    Me.ef_tax_provisions_2.Location = New System.Drawing.Point(625, 75)
    Me.ef_tax_provisions_2.Name = "ef_tax_provisions_2"
    Me.ef_tax_provisions_2.PlaceHolder = Nothing
    Me.ef_tax_provisions_2.ShortcutsEnabled = True
    Me.ef_tax_provisions_2.Size = New System.Drawing.Size(79, 24)
    Me.ef_tax_provisions_2.SufixText = "Sufix Text"
    Me.ef_tax_provisions_2.SufixTextVisible = True
    Me.ef_tax_provisions_2.SufixTextWidth = 14
    Me.ef_tax_provisions_2.TabIndex = 45
    Me.ef_tax_provisions_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_tax_provisions_2.TextValue = ""
    Me.ef_tax_provisions_2.TextWidth = 0
    Me.ef_tax_provisions_2.Value = ""
    Me.ef_tax_provisions_2.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_tax_provisions_1
    '
    Me.ef_tax_provisions_1.DoubleValue = 0.0R
    Me.ef_tax_provisions_1.IntegerValue = 0
    Me.ef_tax_provisions_1.IsReadOnly = False
    Me.ef_tax_provisions_1.Location = New System.Drawing.Point(625, 45)
    Me.ef_tax_provisions_1.Name = "ef_tax_provisions_1"
    Me.ef_tax_provisions_1.PlaceHolder = Nothing
    Me.ef_tax_provisions_1.ShortcutsEnabled = True
    Me.ef_tax_provisions_1.Size = New System.Drawing.Size(79, 24)
    Me.ef_tax_provisions_1.SufixText = "Sufix Text"
    Me.ef_tax_provisions_1.SufixTextVisible = True
    Me.ef_tax_provisions_1.SufixTextWidth = 14
    Me.ef_tax_provisions_1.TabIndex = 44
    Me.ef_tax_provisions_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_tax_provisions_1.TextValue = ""
    Me.ef_tax_provisions_1.TextWidth = 0
    Me.ef_tax_provisions_1.Value = ""
    Me.ef_tax_provisions_1.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_tax_provisions
    '
    Me.lbl_tax_provisions.AutoSize = True
    Me.lbl_tax_provisions.Location = New System.Drawing.Point(623, 18)
    Me.lbl_tax_provisions.MaximumSize = New System.Drawing.Size(100, 0)
    Me.lbl_tax_provisions.Name = "lbl_tax_provisions"
    Me.lbl_tax_provisions.Size = New System.Drawing.Size(96, 13)
    Me.lbl_tax_provisions.TabIndex = 49
    Me.lbl_tax_provisions.Text = "xTax provisions"
    '
    'cmb_loser_credit_type
    '
    Me.cmb_loser_credit_type.AllowUnlistedValues = False
    Me.cmb_loser_credit_type.AutoCompleteMode = False
    Me.cmb_loser_credit_type.IsReadOnly = False
    Me.cmb_loser_credit_type.Location = New System.Drawing.Point(223, 167)
    Me.cmb_loser_credit_type.Name = "cmb_loser_credit_type"
    Me.cmb_loser_credit_type.SelectedIndex = -1
    Me.cmb_loser_credit_type.Size = New System.Drawing.Size(128, 24)
    Me.cmb_loser_credit_type.SufixText = "Sufix Text"
    Me.cmb_loser_credit_type.SufixTextVisible = True
    Me.cmb_loser_credit_type.TabIndex = 21
    Me.cmb_loser_credit_type.TextCombo = Nothing
    Me.cmb_loser_credit_type.TextVisible = False
    Me.cmb_loser_credit_type.TextWidth = 0
    '
    'cmb_prize4_credit_type
    '
    Me.cmb_prize4_credit_type.AllowUnlistedValues = False
    Me.cmb_prize4_credit_type.AutoCompleteMode = False
    Me.cmb_prize4_credit_type.IsReadOnly = False
    Me.cmb_prize4_credit_type.Location = New System.Drawing.Point(223, 136)
    Me.cmb_prize4_credit_type.Name = "cmb_prize4_credit_type"
    Me.cmb_prize4_credit_type.SelectedIndex = -1
    Me.cmb_prize4_credit_type.Size = New System.Drawing.Size(128, 24)
    Me.cmb_prize4_credit_type.SufixText = "Sufix Text"
    Me.cmb_prize4_credit_type.SufixTextVisible = True
    Me.cmb_prize4_credit_type.TabIndex = 16
    Me.cmb_prize4_credit_type.TextCombo = Nothing
    Me.cmb_prize4_credit_type.TextVisible = False
    Me.cmb_prize4_credit_type.TextWidth = 0
    '
    'cmb_prize3_credit_type
    '
    Me.cmb_prize3_credit_type.AllowUnlistedValues = False
    Me.cmb_prize3_credit_type.AutoCompleteMode = False
    Me.cmb_prize3_credit_type.IsReadOnly = False
    Me.cmb_prize3_credit_type.Location = New System.Drawing.Point(223, 106)
    Me.cmb_prize3_credit_type.Name = "cmb_prize3_credit_type"
    Me.cmb_prize3_credit_type.SelectedIndex = -1
    Me.cmb_prize3_credit_type.Size = New System.Drawing.Size(128, 24)
    Me.cmb_prize3_credit_type.SufixText = "Sufix Text"
    Me.cmb_prize3_credit_type.SufixTextVisible = True
    Me.cmb_prize3_credit_type.TabIndex = 11
    Me.cmb_prize3_credit_type.TextCombo = Nothing
    Me.cmb_prize3_credit_type.TextVisible = False
    Me.cmb_prize3_credit_type.TextWidth = 0
    '
    'cmb_prize2_credit_type
    '
    Me.cmb_prize2_credit_type.AllowUnlistedValues = False
    Me.cmb_prize2_credit_type.AutoCompleteMode = False
    Me.cmb_prize2_credit_type.IsReadOnly = False
    Me.cmb_prize2_credit_type.Location = New System.Drawing.Point(223, 76)
    Me.cmb_prize2_credit_type.Name = "cmb_prize2_credit_type"
    Me.cmb_prize2_credit_type.SelectedIndex = -1
    Me.cmb_prize2_credit_type.Size = New System.Drawing.Size(128, 24)
    Me.cmb_prize2_credit_type.SufixText = "Sufix Text"
    Me.cmb_prize2_credit_type.SufixTextVisible = True
    Me.cmb_prize2_credit_type.TabIndex = 6
    Me.cmb_prize2_credit_type.TextCombo = Nothing
    Me.cmb_prize2_credit_type.TextVisible = False
    Me.cmb_prize2_credit_type.TextWidth = 0
    '
    'cmb_prize1_credit_type
    '
    Me.cmb_prize1_credit_type.AllowUnlistedValues = False
    Me.cmb_prize1_credit_type.AutoCompleteMode = False
    Me.cmb_prize1_credit_type.IsReadOnly = False
    Me.cmb_prize1_credit_type.Location = New System.Drawing.Point(223, 45)
    Me.cmb_prize1_credit_type.Name = "cmb_prize1_credit_type"
    Me.cmb_prize1_credit_type.SelectedIndex = -1
    Me.cmb_prize1_credit_type.Size = New System.Drawing.Size(128, 24)
    Me.cmb_prize1_credit_type.SufixText = "Sufix Text"
    Me.cmb_prize1_credit_type.SufixTextVisible = True
    Me.cmb_prize1_credit_type.TabIndex = 1
    Me.cmb_prize1_credit_type.TextCombo = Nothing
    Me.cmb_prize1_credit_type.TextVisible = False
    Me.cmb_prize1_credit_type.TextWidth = 0
    '
    'lbl_cpromo_explanation
    '
    Me.lbl_cpromo_explanation.AutoSize = True
    Me.lbl_cpromo_explanation.Font = New System.Drawing.Font("Courier New", 8.25!)
    Me.lbl_cpromo_explanation.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_cpromo_explanation.Location = New System.Drawing.Point(20, 230)
    Me.lbl_cpromo_explanation.MaximumSize = New System.Drawing.Size(880, 0)
    Me.lbl_cpromo_explanation.Name = "lbl_cpromo_explanation"
    Me.lbl_cpromo_explanation.Size = New System.Drawing.Size(133, 14)
    Me.lbl_cpromo_explanation.TabIndex = 43
    Me.lbl_cpromo_explanation.Text = "xCpromoExplanation"
    '
    'lbl_prize_explanation
    '
    Me.lbl_prize_explanation.AutoSize = True
    Me.lbl_prize_explanation.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_prize_explanation.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_prize_explanation.Location = New System.Drawing.Point(20, 210)
    Me.lbl_prize_explanation.MaximumSize = New System.Drawing.Size(880, 0)
    Me.lbl_prize_explanation.Name = "lbl_prize_explanation"
    Me.lbl_prize_explanation.Size = New System.Drawing.Size(126, 14)
    Me.lbl_prize_explanation.TabIndex = 42
    Me.lbl_prize_explanation.Text = "xPrizeExplanation"
    '
    'lbl_prizes_nonredemable
    '
    Me.lbl_prizes_nonredemable.AutoSize = True
    Me.lbl_prizes_nonredemable.Location = New System.Drawing.Point(221, 18)
    Me.lbl_prizes_nonredemable.MaximumSize = New System.Drawing.Size(100, 0)
    Me.lbl_prizes_nonredemable.Name = "lbl_prizes_nonredemable"
    Me.lbl_prizes_nonredemable.Size = New System.Drawing.Size(100, 26)
    Me.lbl_prizes_nonredemable.TabIndex = 27
    Me.lbl_prizes_nonredemable.Text = "xNonRedeemable"
    '
    'lbl_prizes_example
    '
    Me.lbl_prizes_example.AutoSize = True
    Me.lbl_prizes_example.Location = New System.Drawing.Point(726, 18)
    Me.lbl_prizes_example.MaximumSize = New System.Drawing.Size(110, 0)
    Me.lbl_prizes_example.Name = "lbl_prizes_example"
    Me.lbl_prizes_example.Size = New System.Drawing.Size(97, 13)
    Me.lbl_prizes_example.TabIndex = 36
    Me.lbl_prizes_example.Text = "xPrizesExample"
    '
    'chk_enable_loser
    '
    Me.chk_enable_loser.AutoSize = True
    Me.chk_enable_loser.Location = New System.Drawing.Point(183, 172)
    Me.chk_enable_loser.Name = "chk_enable_loser"
    Me.chk_enable_loser.Size = New System.Drawing.Size(15, 14)
    Me.chk_enable_loser.TabIndex = 20
    Me.chk_enable_loser.UseVisualStyleBackColor = True
    '
    'chk_enable_winner_4
    '
    Me.chk_enable_winner_4.AutoSize = True
    Me.chk_enable_winner_4.Location = New System.Drawing.Point(183, 142)
    Me.chk_enable_winner_4.Name = "chk_enable_winner_4"
    Me.chk_enable_winner_4.Size = New System.Drawing.Size(15, 14)
    Me.chk_enable_winner_4.TabIndex = 15
    Me.chk_enable_winner_4.UseVisualStyleBackColor = True
    '
    'lbl_example_loser
    '
    Me.lbl_example_loser.AutoSize = True
    Me.lbl_example_loser.Location = New System.Drawing.Point(726, 170)
    Me.lbl_example_loser.Name = "lbl_example_loser"
    Me.lbl_example_loser.Size = New System.Drawing.Size(94, 13)
    Me.lbl_example_loser.TabIndex = 41
    Me.lbl_example_loser.Text = "xExampleLoser"
    '
    'chk_enable_winner_3
    '
    Me.chk_enable_winner_3.AutoSize = True
    Me.chk_enable_winner_3.Location = New System.Drawing.Point(183, 112)
    Me.chk_enable_winner_3.Name = "chk_enable_winner_3"
    Me.chk_enable_winner_3.Size = New System.Drawing.Size(15, 14)
    Me.chk_enable_winner_3.TabIndex = 10
    Me.chk_enable_winner_3.UseVisualStyleBackColor = True
    '
    'lbl_loser
    '
    Me.lbl_loser.AutoSize = True
    Me.lbl_loser.Location = New System.Drawing.Point(21, 172)
    Me.lbl_loser.MaximumSize = New System.Drawing.Size(150, 0)
    Me.lbl_loser.Name = "lbl_loser"
    Me.lbl_loser.Size = New System.Drawing.Size(45, 13)
    Me.lbl_loser.TabIndex = 25
    Me.lbl_loser.Text = "xLoser"
    '
    'ef_access_perc_loser
    '
    Me.ef_access_perc_loser.DoubleValue = 0.0R
    Me.ef_access_perc_loser.IntegerValue = 0
    Me.ef_access_perc_loser.IsReadOnly = False
    Me.ef_access_perc_loser.Location = New System.Drawing.Point(357, 165)
    Me.ef_access_perc_loser.Name = "ef_access_perc_loser"
    Me.ef_access_perc_loser.PlaceHolder = Nothing
    Me.ef_access_perc_loser.ShortcutsEnabled = True
    Me.ef_access_perc_loser.Size = New System.Drawing.Size(79, 24)
    Me.ef_access_perc_loser.SufixText = "Sufix Text"
    Me.ef_access_perc_loser.SufixTextVisible = True
    Me.ef_access_perc_loser.SufixTextWidth = 14
    Me.ef_access_perc_loser.TabIndex = 33
    Me.ef_access_perc_loser.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_access_perc_loser.TextValue = ""
    Me.ef_access_perc_loser.TextWidth = 0
    Me.ef_access_perc_loser.Value = ""
    Me.ef_access_perc_loser.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_enable_winner_2
    '
    Me.chk_enable_winner_2.AutoSize = True
    Me.chk_enable_winner_2.Location = New System.Drawing.Point(183, 82)
    Me.chk_enable_winner_2.Name = "chk_enable_winner_2"
    Me.chk_enable_winner_2.Size = New System.Drawing.Size(15, 14)
    Me.chk_enable_winner_2.TabIndex = 5
    Me.chk_enable_winner_2.UseVisualStyleBackColor = True
    '
    'ef_cprize_percentage
    '
    Me.ef_cprize_percentage.DoubleValue = 0.0R
    Me.ef_cprize_percentage.IntegerValue = 0
    Me.ef_cprize_percentage.IsReadOnly = False
    Me.ef_cprize_percentage.Location = New System.Drawing.Point(442, 165)
    Me.ef_cprize_percentage.Name = "ef_cprize_percentage"
    Me.ef_cprize_percentage.PlaceHolder = Nothing
    Me.ef_cprize_percentage.ShortcutsEnabled = True
    Me.ef_cprize_percentage.Size = New System.Drawing.Size(79, 24)
    Me.ef_cprize_percentage.SufixText = "Sufix Text"
    Me.ef_cprize_percentage.SufixTextVisible = True
    Me.ef_cprize_percentage.SufixTextWidth = 14
    Me.ef_cprize_percentage.TabIndex = 22
    Me.ef_cprize_percentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_cprize_percentage.TextValue = ""
    Me.ef_cprize_percentage.TextWidth = 0
    Me.ef_cprize_percentage.Value = ""
    Me.ef_cprize_percentage.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_enable_winner_1
    '
    Me.chk_enable_winner_1.AutoSize = True
    Me.chk_enable_winner_1.Location = New System.Drawing.Point(183, 52)
    Me.chk_enable_winner_1.Name = "chk_enable_winner_1"
    Me.chk_enable_winner_1.Size = New System.Drawing.Size(15, 14)
    Me.chk_enable_winner_1.TabIndex = 0
    Me.chk_enable_winner_1.UseVisualStyleBackColor = True
    '
    'ef_cprize_fixed
    '
    Me.ef_cprize_fixed.DoubleValue = 0.0R
    Me.ef_cprize_fixed.IntegerValue = 0
    Me.ef_cprize_fixed.IsReadOnly = False
    Me.ef_cprize_fixed.Location = New System.Drawing.Point(527, 165)
    Me.ef_cprize_fixed.Name = "ef_cprize_fixed"
    Me.ef_cprize_fixed.PlaceHolder = Nothing
    Me.ef_cprize_fixed.ShortcutsEnabled = True
    Me.ef_cprize_fixed.Size = New System.Drawing.Size(90, 24)
    Me.ef_cprize_fixed.SufixText = "Sufix Text"
    Me.ef_cprize_fixed.SufixTextVisible = True
    Me.ef_cprize_fixed.TabIndex = 23
    Me.ef_cprize_fixed.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_cprize_fixed.TextValue = ""
    Me.ef_cprize_fixed.TextWidth = 0
    Me.ef_cprize_fixed.Value = ""
    Me.ef_cprize_fixed.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_winner_prize4
    '
    Me.lbl_winner_prize4.AutoSize = True
    Me.lbl_winner_prize4.Location = New System.Drawing.Point(21, 142)
    Me.lbl_winner_prize4.Name = "lbl_winner_prize4"
    Me.lbl_winner_prize4.Size = New System.Drawing.Size(89, 13)
    Me.lbl_winner_prize4.TabIndex = 24
    Me.lbl_winner_prize4.Text = "xWinnerPrize4"
    '
    'lbl_example_winner_4
    '
    Me.lbl_example_winner_4.AutoSize = True
    Me.lbl_example_winner_4.Location = New System.Drawing.Point(726, 140)
    Me.lbl_example_winner_4.Name = "lbl_example_winner_4"
    Me.lbl_example_winner_4.Size = New System.Drawing.Size(110, 13)
    Me.lbl_example_winner_4.TabIndex = 40
    Me.lbl_example_winner_4.Text = "xExampleWinner4"
    '
    'ef_access_perc_winner_4
    '
    Me.ef_access_perc_winner_4.DoubleValue = 0.0R
    Me.ef_access_perc_winner_4.IntegerValue = 0
    Me.ef_access_perc_winner_4.IsReadOnly = False
    Me.ef_access_perc_winner_4.Location = New System.Drawing.Point(357, 135)
    Me.ef_access_perc_winner_4.Name = "ef_access_perc_winner_4"
    Me.ef_access_perc_winner_4.PlaceHolder = Nothing
    Me.ef_access_perc_winner_4.ShortcutsEnabled = True
    Me.ef_access_perc_winner_4.Size = New System.Drawing.Size(79, 24)
    Me.ef_access_perc_winner_4.SufixText = "Sufix Text"
    Me.ef_access_perc_winner_4.SufixTextVisible = True
    Me.ef_access_perc_winner_4.SufixTextWidth = 14
    Me.ef_access_perc_winner_4.TabIndex = 32
    Me.ef_access_perc_winner_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_access_perc_winner_4.TextValue = ""
    Me.ef_access_perc_winner_4.TextWidth = 0
    Me.ef_access_perc_winner_4.Value = ""
    Me.ef_access_perc_winner_4.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_perc_4
    '
    Me.ef_perc_4.DoubleValue = 0.0R
    Me.ef_perc_4.IntegerValue = 0
    Me.ef_perc_4.IsReadOnly = False
    Me.ef_perc_4.Location = New System.Drawing.Point(442, 135)
    Me.ef_perc_4.Name = "ef_perc_4"
    Me.ef_perc_4.PlaceHolder = Nothing
    Me.ef_perc_4.ShortcutsEnabled = True
    Me.ef_perc_4.Size = New System.Drawing.Size(79, 24)
    Me.ef_perc_4.SufixText = "Sufix Text"
    Me.ef_perc_4.SufixTextVisible = True
    Me.ef_perc_4.SufixTextWidth = 14
    Me.ef_perc_4.TabIndex = 17
    Me.ef_perc_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_perc_4.TextValue = ""
    Me.ef_perc_4.TextWidth = 0
    Me.ef_perc_4.Value = ""
    Me.ef_perc_4.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_example_winner_3
    '
    Me.lbl_example_winner_3.AutoSize = True
    Me.lbl_example_winner_3.Location = New System.Drawing.Point(726, 110)
    Me.lbl_example_winner_3.Name = "lbl_example_winner_3"
    Me.lbl_example_winner_3.Size = New System.Drawing.Size(110, 13)
    Me.lbl_example_winner_3.TabIndex = 39
    Me.lbl_example_winner_3.Text = "xExampleWinner3"
    '
    'ef_fixed_4
    '
    Me.ef_fixed_4.DoubleValue = 0.0R
    Me.ef_fixed_4.IntegerValue = 0
    Me.ef_fixed_4.IsReadOnly = False
    Me.ef_fixed_4.Location = New System.Drawing.Point(527, 135)
    Me.ef_fixed_4.Name = "ef_fixed_4"
    Me.ef_fixed_4.PlaceHolder = Nothing
    Me.ef_fixed_4.ShortcutsEnabled = True
    Me.ef_fixed_4.Size = New System.Drawing.Size(90, 24)
    Me.ef_fixed_4.SufixText = "Sufix Text"
    Me.ef_fixed_4.SufixTextVisible = True
    Me.ef_fixed_4.TabIndex = 18
    Me.ef_fixed_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_fixed_4.TextValue = ""
    Me.ef_fixed_4.TextWidth = 0
    Me.ef_fixed_4.Value = ""
    Me.ef_fixed_4.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_winner_prize3
    '
    Me.lbl_winner_prize3.AutoSize = True
    Me.lbl_winner_prize3.Location = New System.Drawing.Point(21, 112)
    Me.lbl_winner_prize3.Name = "lbl_winner_prize3"
    Me.lbl_winner_prize3.Size = New System.Drawing.Size(89, 13)
    Me.lbl_winner_prize3.TabIndex = 23
    Me.lbl_winner_prize3.Text = "xWinnerPrize3"
    '
    'ef_access_perc_winner_3
    '
    Me.ef_access_perc_winner_3.DoubleValue = 0.0R
    Me.ef_access_perc_winner_3.IntegerValue = 0
    Me.ef_access_perc_winner_3.IsReadOnly = False
    Me.ef_access_perc_winner_3.Location = New System.Drawing.Point(357, 105)
    Me.ef_access_perc_winner_3.Name = "ef_access_perc_winner_3"
    Me.ef_access_perc_winner_3.PlaceHolder = Nothing
    Me.ef_access_perc_winner_3.ShortcutsEnabled = True
    Me.ef_access_perc_winner_3.Size = New System.Drawing.Size(79, 24)
    Me.ef_access_perc_winner_3.SufixText = "Sufix Text"
    Me.ef_access_perc_winner_3.SufixTextVisible = True
    Me.ef_access_perc_winner_3.SufixTextWidth = 14
    Me.ef_access_perc_winner_3.TabIndex = 31
    Me.ef_access_perc_winner_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_access_perc_winner_3.TextValue = ""
    Me.ef_access_perc_winner_3.TextWidth = 0
    Me.ef_access_perc_winner_3.Value = ""
    Me.ef_access_perc_winner_3.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_perc_3
    '
    Me.ef_perc_3.DoubleValue = 0.0R
    Me.ef_perc_3.IntegerValue = 0
    Me.ef_perc_3.IsReadOnly = False
    Me.ef_perc_3.Location = New System.Drawing.Point(442, 105)
    Me.ef_perc_3.Name = "ef_perc_3"
    Me.ef_perc_3.PlaceHolder = Nothing
    Me.ef_perc_3.ShortcutsEnabled = True
    Me.ef_perc_3.Size = New System.Drawing.Size(79, 24)
    Me.ef_perc_3.SufixText = "Sufix Text"
    Me.ef_perc_3.SufixTextVisible = True
    Me.ef_perc_3.SufixTextWidth = 14
    Me.ef_perc_3.TabIndex = 12
    Me.ef_perc_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_perc_3.TextValue = ""
    Me.ef_perc_3.TextWidth = 0
    Me.ef_perc_3.Value = ""
    Me.ef_perc_3.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_winner_prize2
    '
    Me.lbl_winner_prize2.AutoSize = True
    Me.lbl_winner_prize2.Location = New System.Drawing.Point(21, 82)
    Me.lbl_winner_prize2.Name = "lbl_winner_prize2"
    Me.lbl_winner_prize2.Size = New System.Drawing.Size(89, 13)
    Me.lbl_winner_prize2.TabIndex = 22
    Me.lbl_winner_prize2.Text = "xWinnerPrize2"
    '
    'ef_fixed_3
    '
    Me.ef_fixed_3.DoubleValue = 0.0R
    Me.ef_fixed_3.IntegerValue = 0
    Me.ef_fixed_3.IsReadOnly = False
    Me.ef_fixed_3.Location = New System.Drawing.Point(527, 105)
    Me.ef_fixed_3.Name = "ef_fixed_3"
    Me.ef_fixed_3.PlaceHolder = Nothing
    Me.ef_fixed_3.ShortcutsEnabled = True
    Me.ef_fixed_3.Size = New System.Drawing.Size(90, 24)
    Me.ef_fixed_3.SufixText = "Sufix Text"
    Me.ef_fixed_3.SufixTextVisible = True
    Me.ef_fixed_3.TabIndex = 13
    Me.ef_fixed_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_fixed_3.TextValue = ""
    Me.ef_fixed_3.TextWidth = 0
    Me.ef_fixed_3.Value = ""
    Me.ef_fixed_3.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_example_winner_2
    '
    Me.lbl_example_winner_2.AutoSize = True
    Me.lbl_example_winner_2.Location = New System.Drawing.Point(726, 81)
    Me.lbl_example_winner_2.Name = "lbl_example_winner_2"
    Me.lbl_example_winner_2.Size = New System.Drawing.Size(110, 13)
    Me.lbl_example_winner_2.TabIndex = 38
    Me.lbl_example_winner_2.Text = "xExampleWinner2"
    '
    'ef_access_perc_winner_2
    '
    Me.ef_access_perc_winner_2.DoubleValue = 0.0R
    Me.ef_access_perc_winner_2.IntegerValue = 0
    Me.ef_access_perc_winner_2.IsReadOnly = False
    Me.ef_access_perc_winner_2.Location = New System.Drawing.Point(357, 75)
    Me.ef_access_perc_winner_2.Name = "ef_access_perc_winner_2"
    Me.ef_access_perc_winner_2.PlaceHolder = Nothing
    Me.ef_access_perc_winner_2.ShortcutsEnabled = True
    Me.ef_access_perc_winner_2.Size = New System.Drawing.Size(79, 24)
    Me.ef_access_perc_winner_2.SufixText = "Sufix Text"
    Me.ef_access_perc_winner_2.SufixTextVisible = True
    Me.ef_access_perc_winner_2.SufixTextWidth = 14
    Me.ef_access_perc_winner_2.TabIndex = 30
    Me.ef_access_perc_winner_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_access_perc_winner_2.TextValue = ""
    Me.ef_access_perc_winner_2.TextWidth = 0
    Me.ef_access_perc_winner_2.Value = ""
    Me.ef_access_perc_winner_2.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_perc_2
    '
    Me.ef_perc_2.DoubleValue = 0.0R
    Me.ef_perc_2.IntegerValue = 0
    Me.ef_perc_2.IsReadOnly = False
    Me.ef_perc_2.Location = New System.Drawing.Point(442, 75)
    Me.ef_perc_2.Name = "ef_perc_2"
    Me.ef_perc_2.PlaceHolder = Nothing
    Me.ef_perc_2.ShortcutsEnabled = True
    Me.ef_perc_2.Size = New System.Drawing.Size(79, 24)
    Me.ef_perc_2.SufixText = "Sufix Text"
    Me.ef_perc_2.SufixTextVisible = True
    Me.ef_perc_2.SufixTextWidth = 14
    Me.ef_perc_2.TabIndex = 7
    Me.ef_perc_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_perc_2.TextValue = ""
    Me.ef_perc_2.TextWidth = 0
    Me.ef_perc_2.Value = ""
    Me.ef_perc_2.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_example_winner_1
    '
    Me.lbl_example_winner_1.AutoSize = True
    Me.lbl_example_winner_1.Location = New System.Drawing.Point(726, 50)
    Me.lbl_example_winner_1.Name = "lbl_example_winner_1"
    Me.lbl_example_winner_1.Size = New System.Drawing.Size(110, 13)
    Me.lbl_example_winner_1.TabIndex = 37
    Me.lbl_example_winner_1.Text = "xExampleWinner1"
    '
    'ef_fixed_2
    '
    Me.ef_fixed_2.DoubleValue = 0.0R
    Me.ef_fixed_2.IntegerValue = 0
    Me.ef_fixed_2.IsReadOnly = False
    Me.ef_fixed_2.Location = New System.Drawing.Point(527, 75)
    Me.ef_fixed_2.Name = "ef_fixed_2"
    Me.ef_fixed_2.PlaceHolder = Nothing
    Me.ef_fixed_2.ShortcutsEnabled = True
    Me.ef_fixed_2.Size = New System.Drawing.Size(90, 24)
    Me.ef_fixed_2.SufixText = "Sufix Text"
    Me.ef_fixed_2.SufixTextVisible = True
    Me.ef_fixed_2.TabIndex = 8
    Me.ef_fixed_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_fixed_2.TextValue = ""
    Me.ef_fixed_2.TextWidth = 0
    Me.ef_fixed_2.Value = ""
    Me.ef_fixed_2.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_access_perc_winner_1
    '
    Me.ef_access_perc_winner_1.DoubleValue = 0.0R
    Me.ef_access_perc_winner_1.IntegerValue = 0
    Me.ef_access_perc_winner_1.IsReadOnly = False
    Me.ef_access_perc_winner_1.Location = New System.Drawing.Point(357, 45)
    Me.ef_access_perc_winner_1.Name = "ef_access_perc_winner_1"
    Me.ef_access_perc_winner_1.PlaceHolder = Nothing
    Me.ef_access_perc_winner_1.ShortcutsEnabled = True
    Me.ef_access_perc_winner_1.Size = New System.Drawing.Size(79, 24)
    Me.ef_access_perc_winner_1.SufixText = "Sufix Text"
    Me.ef_access_perc_winner_1.SufixTextVisible = True
    Me.ef_access_perc_winner_1.SufixTextWidth = 14
    Me.ef_access_perc_winner_1.TabIndex = 29
    Me.ef_access_perc_winner_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_access_perc_winner_1.TextValue = ""
    Me.ef_access_perc_winner_1.TextWidth = 0
    Me.ef_access_perc_winner_1.Value = ""
    Me.ef_access_perc_winner_1.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_winner_prize1
    '
    Me.lbl_winner_prize1.AutoSize = True
    Me.lbl_winner_prize1.Location = New System.Drawing.Point(21, 52)
    Me.lbl_winner_prize1.Name = "lbl_winner_prize1"
    Me.lbl_winner_prize1.Size = New System.Drawing.Size(89, 13)
    Me.lbl_winner_prize1.TabIndex = 21
    Me.lbl_winner_prize1.Text = "xWinnerPrize1"
    '
    'ef_perc_1
    '
    Me.ef_perc_1.DoubleValue = 0.0R
    Me.ef_perc_1.IntegerValue = 0
    Me.ef_perc_1.IsReadOnly = False
    Me.ef_perc_1.Location = New System.Drawing.Point(442, 45)
    Me.ef_perc_1.Name = "ef_perc_1"
    Me.ef_perc_1.PlaceHolder = Nothing
    Me.ef_perc_1.ShortcutsEnabled = True
    Me.ef_perc_1.Size = New System.Drawing.Size(79, 24)
    Me.ef_perc_1.SufixText = "Sufix Text"
    Me.ef_perc_1.SufixTextVisible = True
    Me.ef_perc_1.SufixTextWidth = 14
    Me.ef_perc_1.TabIndex = 2
    Me.ef_perc_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_perc_1.TextValue = ""
    Me.ef_perc_1.TextWidth = 0
    Me.ef_perc_1.Value = ""
    Me.ef_perc_1.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_fixed_1
    '
    Me.ef_fixed_1.DoubleValue = 0.0R
    Me.ef_fixed_1.IntegerValue = 0
    Me.ef_fixed_1.IsReadOnly = False
    Me.ef_fixed_1.Location = New System.Drawing.Point(527, 45)
    Me.ef_fixed_1.Name = "ef_fixed_1"
    Me.ef_fixed_1.PlaceHolder = Nothing
    Me.ef_fixed_1.ShortcutsEnabled = True
    Me.ef_fixed_1.Size = New System.Drawing.Size(90, 24)
    Me.ef_fixed_1.SufixText = "Sufix Text"
    Me.ef_fixed_1.SufixTextVisible = True
    Me.ef_fixed_1.TabIndex = 3
    Me.ef_fixed_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_fixed_1.TextValue = ""
    Me.ef_fixed_1.TextWidth = 0
    Me.ef_fixed_1.Value = ""
    Me.ef_fixed_1.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_prizes_category
    '
    Me.lbl_prizes_category.AutoSize = True
    Me.lbl_prizes_category.Location = New System.Drawing.Point(14, 18)
    Me.lbl_prizes_category.Name = "lbl_prizes_category"
    Me.lbl_prizes_category.Size = New System.Drawing.Size(67, 13)
    Me.lbl_prizes_category.TabIndex = 20
    Me.lbl_prizes_category.Text = "xCategory"
    '
    'lbl_prizes_enable
    '
    Me.lbl_prizes_enable.AutoSize = True
    Me.lbl_prizes_enable.Location = New System.Drawing.Point(165, 18)
    Me.lbl_prizes_enable.MaximumSize = New System.Drawing.Size(100, 0)
    Me.lbl_prizes_enable.Name = "lbl_prizes_enable"
    Me.lbl_prizes_enable.Size = New System.Drawing.Size(86, 13)
    Me.lbl_prizes_enable.TabIndex = 26
    Me.lbl_prizes_enable.Text = "xPrizesEnable"
    '
    'lbl_prizes_fixed
    '
    Me.lbl_prizes_fixed.AutoSize = True
    Me.lbl_prizes_fixed.Location = New System.Drawing.Point(539, 18)
    Me.lbl_prizes_fixed.Name = "lbl_prizes_fixed"
    Me.lbl_prizes_fixed.Size = New System.Drawing.Size(78, 13)
    Me.lbl_prizes_fixed.TabIndex = 35
    Me.lbl_prizes_fixed.Text = "xPrizesFixed"
    '
    'lbl_prizes_percentage
    '
    Me.lbl_prizes_percentage.AutoSize = True
    Me.lbl_prizes_percentage.Location = New System.Drawing.Point(440, 18)
    Me.lbl_prizes_percentage.MaximumSize = New System.Drawing.Size(100, 0)
    Me.lbl_prizes_percentage.Name = "lbl_prizes_percentage"
    Me.lbl_prizes_percentage.Size = New System.Drawing.Size(73, 13)
    Me.lbl_prizes_percentage.TabIndex = 34
    Me.lbl_prizes_percentage.Text = "xPrizesPerc"
    '
    'lbl_prizes_access_perc
    '
    Me.lbl_prizes_access_perc.AutoEllipsis = True
    Me.lbl_prizes_access_perc.AutoSize = True
    Me.lbl_prizes_access_perc.Location = New System.Drawing.Point(355, 18)
    Me.lbl_prizes_access_perc.MaximumSize = New System.Drawing.Size(80, 30)
    Me.lbl_prizes_access_perc.Name = "lbl_prizes_access_perc"
    Me.lbl_prizes_access_perc.Size = New System.Drawing.Size(75, 26)
    Me.lbl_prizes_access_perc.TabIndex = 28
    Me.lbl_prizes_access_perc.Text = "xPrizesAccessPerc"
    '
    'gb_draw_connection
    '
    Me.gb_draw_connection.Controls.Add(Me.panel_webservice)
    Me.gb_draw_connection.Controls.Add(Me.rb_opt_ext_db)
    Me.gb_draw_connection.Controls.Add(Me.rb_opt_Local_DB)
    Me.gb_draw_connection.Location = New System.Drawing.Point(3, 211)
    Me.gb_draw_connection.Name = "gb_draw_connection"
    Me.gb_draw_connection.Size = New System.Drawing.Size(10, 10)
    Me.gb_draw_connection.TabIndex = 11
    Me.gb_draw_connection.TabStop = False
    Me.gb_draw_connection.Text = "xDrawConnection"
    Me.gb_draw_connection.Visible = False
    '
    'panel_webservice
    '
    Me.panel_webservice.Controls.Add(Me.rb_opt_webservice)
    Me.panel_webservice.Controls.Add(Me.lbl_center_address_1)
    Me.panel_webservice.Controls.Add(Me.panel_not_connected)
    Me.panel_webservice.Controls.Add(Me.lbl_center_address_2)
    Me.panel_webservice.Controls.Add(Me.ef_center_address_2)
    Me.panel_webservice.Controls.Add(Me.ef_center_address_1)
    Me.panel_webservice.Location = New System.Drawing.Point(27, 279)
    Me.panel_webservice.Name = "panel_webservice"
    Me.panel_webservice.Size = New System.Drawing.Size(492, 185)
    Me.panel_webservice.TabIndex = 1
    '
    'rb_opt_webservice
    '
    Me.rb_opt_webservice.AutoSize = True
    Me.rb_opt_webservice.Location = New System.Drawing.Point(8, 3)
    Me.rb_opt_webservice.Name = "rb_opt_webservice"
    Me.rb_opt_webservice.Size = New System.Drawing.Size(99, 17)
    Me.rb_opt_webservice.TabIndex = 4
    Me.rb_opt_webservice.TabStop = True
    Me.rb_opt_webservice.Text = "xWebService"
    Me.rb_opt_webservice.UseVisualStyleBackColor = True
    '
    'lbl_center_address_1
    '
    Me.lbl_center_address_1.AutoSize = True
    Me.lbl_center_address_1.Location = New System.Drawing.Point(39, 23)
    Me.lbl_center_address_1.Name = "lbl_center_address_1"
    Me.lbl_center_address_1.Size = New System.Drawing.Size(106, 13)
    Me.lbl_center_address_1.TabIndex = 1
    Me.lbl_center_address_1.Text = "xCenterAddress1"
    '
    'panel_not_connected
    '
    Me.panel_not_connected.Controls.Add(Me.rb_recharge_no_red)
    Me.panel_not_connected.Controls.Add(Me.lbl_not_connected)
    Me.panel_not_connected.Controls.Add(Me.rb_recharge_red)
    Me.panel_not_connected.Controls.Add(Me.rb_dont_recharge)
    Me.panel_not_connected.Location = New System.Drawing.Point(34, 112)
    Me.panel_not_connected.Name = "panel_not_connected"
    Me.panel_not_connected.Size = New System.Drawing.Size(447, 66)
    Me.panel_not_connected.TabIndex = 6
    '
    'rb_recharge_no_red
    '
    Me.rb_recharge_no_red.AutoSize = True
    Me.rb_recharge_no_red.Location = New System.Drawing.Point(19, 69)
    Me.rb_recharge_no_red.Name = "rb_recharge_no_red"
    Me.rb_recharge_no_red.Size = New System.Drawing.Size(123, 17)
    Me.rb_recharge_no_red.TabIndex = 10
    Me.rb_recharge_no_red.TabStop = True
    Me.rb_recharge_no_red.Text = "xRechargeNoRed"
    Me.rb_recharge_no_red.UseVisualStyleBackColor = True
    '
    'lbl_not_connected
    '
    Me.lbl_not_connected.AutoSize = True
    Me.lbl_not_connected.Location = New System.Drawing.Point(5, 7)
    Me.lbl_not_connected.Name = "lbl_not_connected"
    Me.lbl_not_connected.Size = New System.Drawing.Size(118, 13)
    Me.lbl_not_connected.TabIndex = 9
    Me.lbl_not_connected.Text = "xNotConnectedText"
    '
    'rb_recharge_red
    '
    Me.rb_recharge_red.AutoSize = True
    Me.rb_recharge_red.Location = New System.Drawing.Point(19, 46)
    Me.rb_recharge_red.Name = "rb_recharge_red"
    Me.rb_recharge_red.Size = New System.Drawing.Size(108, 17)
    Me.rb_recharge_red.TabIndex = 1
    Me.rb_recharge_red.TabStop = True
    Me.rb_recharge_red.Text = "xRechargeRed"
    Me.rb_recharge_red.UseVisualStyleBackColor = True
    '
    'rb_dont_recharge
    '
    Me.rb_dont_recharge.AutoSize = True
    Me.rb_dont_recharge.Location = New System.Drawing.Point(19, 23)
    Me.rb_dont_recharge.Name = "rb_dont_recharge"
    Me.rb_dont_recharge.Size = New System.Drawing.Size(113, 17)
    Me.rb_dont_recharge.TabIndex = 0
    Me.rb_dont_recharge.TabStop = True
    Me.rb_dont_recharge.Text = "xDontRecharge"
    Me.rb_dont_recharge.UseVisualStyleBackColor = True
    '
    'lbl_center_address_2
    '
    Me.lbl_center_address_2.AutoSize = True
    Me.lbl_center_address_2.Location = New System.Drawing.Point(39, 66)
    Me.lbl_center_address_2.Name = "lbl_center_address_2"
    Me.lbl_center_address_2.Size = New System.Drawing.Size(106, 13)
    Me.lbl_center_address_2.TabIndex = 7
    Me.lbl_center_address_2.Text = "xCenterAddress2"
    '
    'ef_center_address_2
    '
    Me.ef_center_address_2.DoubleValue = 0.0R
    Me.ef_center_address_2.IntegerValue = 0
    Me.ef_center_address_2.IsReadOnly = False
    Me.ef_center_address_2.Location = New System.Drawing.Point(42, 82)
    Me.ef_center_address_2.Name = "ef_center_address_2"
    Me.ef_center_address_2.PlaceHolder = Nothing
    Me.ef_center_address_2.ShortcutsEnabled = True
    Me.ef_center_address_2.Size = New System.Drawing.Size(439, 24)
    Me.ef_center_address_2.SufixText = "Sufix Text"
    Me.ef_center_address_2.SufixTextVisible = True
    Me.ef_center_address_2.TabIndex = 8
    Me.ef_center_address_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_center_address_2.TextValue = ""
    Me.ef_center_address_2.TextVisible = False
    Me.ef_center_address_2.TextWidth = 0
    Me.ef_center_address_2.Value = ""
    Me.ef_center_address_2.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_center_address_1
    '
    Me.ef_center_address_1.DoubleValue = 0.0R
    Me.ef_center_address_1.IntegerValue = 0
    Me.ef_center_address_1.IsReadOnly = False
    Me.ef_center_address_1.Location = New System.Drawing.Point(42, 39)
    Me.ef_center_address_1.Name = "ef_center_address_1"
    Me.ef_center_address_1.PlaceHolder = Nothing
    Me.ef_center_address_1.ShortcutsEnabled = True
    Me.ef_center_address_1.Size = New System.Drawing.Size(439, 24)
    Me.ef_center_address_1.SufixText = "Sufix Text"
    Me.ef_center_address_1.SufixTextVisible = True
    Me.ef_center_address_1.TabIndex = 6
    Me.ef_center_address_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_center_address_1.TextValue = ""
    Me.ef_center_address_1.TextVisible = False
    Me.ef_center_address_1.TextWidth = 0
    Me.ef_center_address_1.Value = ""
    Me.ef_center_address_1.ValueForeColor = System.Drawing.Color.Blue
    '
    'rb_opt_ext_db
    '
    Me.rb_opt_ext_db.AutoSize = True
    Me.rb_opt_ext_db.Location = New System.Drawing.Point(17, 41)
    Me.rb_opt_ext_db.Name = "rb_opt_ext_db"
    Me.rb_opt_ext_db.Size = New System.Drawing.Size(95, 17)
    Me.rb_opt_ext_db.TabIndex = 1
    Me.rb_opt_ext_db.TabStop = True
    Me.rb_opt_ext_db.Text = "xExternalDb"
    Me.rb_opt_ext_db.UseVisualStyleBackColor = True
    '
    'rb_opt_Local_DB
    '
    Me.rb_opt_Local_DB.AutoSize = True
    Me.rb_opt_Local_DB.Location = New System.Drawing.Point(17, 18)
    Me.rb_opt_Local_DB.Name = "rb_opt_Local_DB"
    Me.rb_opt_Local_DB.Size = New System.Drawing.Size(78, 17)
    Me.rb_opt_Local_DB.TabIndex = 0
    Me.rb_opt_Local_DB.TabStop = True
    Me.rb_opt_Local_DB.Text = "xLocalDB"
    Me.rb_opt_Local_DB.UseVisualStyleBackColor = True
    '
    'gb_ext_db_connection_string
    '
    Me.gb_ext_db_connection_string.Controls.Add(Me.ef_extdb_server_2)
    Me.gb_ext_db_connection_string.Controls.Add(Me.ef_extdb_pwd)
    Me.gb_ext_db_connection_string.Controls.Add(Me.ef_extdb_server_1)
    Me.gb_ext_db_connection_string.Controls.Add(Me.ef_extdb_user)
    Me.gb_ext_db_connection_string.Controls.Add(Me.ef_extdb_database)
    Me.gb_ext_db_connection_string.Location = New System.Drawing.Point(3, 190)
    Me.gb_ext_db_connection_string.Name = "gb_ext_db_connection_string"
    Me.gb_ext_db_connection_string.Size = New System.Drawing.Size(10, 10)
    Me.gb_ext_db_connection_string.TabIndex = 10
    Me.gb_ext_db_connection_string.TabStop = False
    Me.gb_ext_db_connection_string.Text = "xExtDbConnectionString"
    Me.gb_ext_db_connection_string.Visible = False
    '
    'ef_extdb_server_2
    '
    Me.ef_extdb_server_2.DoubleValue = 0.0R
    Me.ef_extdb_server_2.IntegerValue = 0
    Me.ef_extdb_server_2.IsReadOnly = False
    Me.ef_extdb_server_2.Location = New System.Drawing.Point(6, 60)
    Me.ef_extdb_server_2.Name = "ef_extdb_server_2"
    Me.ef_extdb_server_2.PlaceHolder = Nothing
    Me.ef_extdb_server_2.ShortcutsEnabled = True
    Me.ef_extdb_server_2.Size = New System.Drawing.Size(432, 24)
    Me.ef_extdb_server_2.SufixText = "Sufix Text"
    Me.ef_extdb_server_2.SufixTextVisible = True
    Me.ef_extdb_server_2.TabIndex = 1
    Me.ef_extdb_server_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_extdb_server_2.TextValue = ""
    Me.ef_extdb_server_2.TextWidth = 140
    Me.ef_extdb_server_2.Value = ""
    Me.ef_extdb_server_2.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_extdb_pwd
    '
    Me.ef_extdb_pwd.DoubleValue = 0.0R
    Me.ef_extdb_pwd.IntegerValue = 0
    Me.ef_extdb_pwd.IsReadOnly = False
    Me.ef_extdb_pwd.Location = New System.Drawing.Point(6, 177)
    Me.ef_extdb_pwd.Name = "ef_extdb_pwd"
    Me.ef_extdb_pwd.PlaceHolder = Nothing
    Me.ef_extdb_pwd.ShortcutsEnabled = True
    Me.ef_extdb_pwd.Size = New System.Drawing.Size(432, 24)
    Me.ef_extdb_pwd.SufixText = "Sufix Text"
    Me.ef_extdb_pwd.SufixTextVisible = True
    Me.ef_extdb_pwd.TabIndex = 4
    Me.ef_extdb_pwd.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_extdb_pwd.TextValue = ""
    Me.ef_extdb_pwd.TextWidth = 140
    Me.ef_extdb_pwd.Value = ""
    Me.ef_extdb_pwd.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_extdb_server_1
    '
    Me.ef_extdb_server_1.DoubleValue = 0.0R
    Me.ef_extdb_server_1.IntegerValue = 0
    Me.ef_extdb_server_1.IsReadOnly = False
    Me.ef_extdb_server_1.Location = New System.Drawing.Point(6, 21)
    Me.ef_extdb_server_1.Name = "ef_extdb_server_1"
    Me.ef_extdb_server_1.PlaceHolder = Nothing
    Me.ef_extdb_server_1.ShortcutsEnabled = True
    Me.ef_extdb_server_1.Size = New System.Drawing.Size(432, 24)
    Me.ef_extdb_server_1.SufixText = "Sufix Text"
    Me.ef_extdb_server_1.SufixTextVisible = True
    Me.ef_extdb_server_1.TabIndex = 4
    Me.ef_extdb_server_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_extdb_server_1.TextValue = ""
    Me.ef_extdb_server_1.TextWidth = 140
    Me.ef_extdb_server_1.Value = ""
    Me.ef_extdb_server_1.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_extdb_user
    '
    Me.ef_extdb_user.DoubleValue = 0.0R
    Me.ef_extdb_user.IntegerValue = 0
    Me.ef_extdb_user.IsReadOnly = False
    Me.ef_extdb_user.Location = New System.Drawing.Point(6, 138)
    Me.ef_extdb_user.Name = "ef_extdb_user"
    Me.ef_extdb_user.PlaceHolder = Nothing
    Me.ef_extdb_user.ShortcutsEnabled = True
    Me.ef_extdb_user.Size = New System.Drawing.Size(432, 24)
    Me.ef_extdb_user.SufixText = "Sufix Text"
    Me.ef_extdb_user.SufixTextVisible = True
    Me.ef_extdb_user.TabIndex = 3
    Me.ef_extdb_user.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_extdb_user.TextValue = ""
    Me.ef_extdb_user.TextWidth = 140
    Me.ef_extdb_user.Value = ""
    Me.ef_extdb_user.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_extdb_database
    '
    Me.ef_extdb_database.DoubleValue = 0.0R
    Me.ef_extdb_database.IntegerValue = 0
    Me.ef_extdb_database.IsReadOnly = False
    Me.ef_extdb_database.Location = New System.Drawing.Point(6, 99)
    Me.ef_extdb_database.Name = "ef_extdb_database"
    Me.ef_extdb_database.PlaceHolder = Nothing
    Me.ef_extdb_database.ShortcutsEnabled = True
    Me.ef_extdb_database.Size = New System.Drawing.Size(432, 24)
    Me.ef_extdb_database.SufixText = "Sufix Text"
    Me.ef_extdb_database.SufixTextVisible = True
    Me.ef_extdb_database.TabIndex = 2
    Me.ef_extdb_database.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_extdb_database.TextValue = ""
    Me.ef_extdb_database.TextWidth = 140
    Me.ef_extdb_database.Value = ""
    Me.ef_extdb_database.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_terminal_game_timeout
    '
    Me.ef_terminal_game_timeout.DoubleValue = 0.0R
    Me.ef_terminal_game_timeout.IntegerValue = 0
    Me.ef_terminal_game_timeout.IsReadOnly = False
    Me.ef_terminal_game_timeout.Location = New System.Drawing.Point(1, 92)
    Me.ef_terminal_game_timeout.Name = "ef_terminal_game_timeout"
    Me.ef_terminal_game_timeout.PlaceHolder = Nothing
    Me.ef_terminal_game_timeout.ShortcutsEnabled = True
    Me.ef_terminal_game_timeout.Size = New System.Drawing.Size(290, 24)
    Me.ef_terminal_game_timeout.SufixText = "Sufix Text"
    Me.ef_terminal_game_timeout.SufixTextVisible = True
    Me.ef_terminal_game_timeout.SufixTextWidth = 80
    Me.ef_terminal_game_timeout.TabIndex = 2
    Me.ef_terminal_game_timeout.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_terminal_game_timeout.TextValue = ""
    Me.ef_terminal_game_timeout.TextWidth = 120
    Me.ef_terminal_game_timeout.Value = ""
    Me.ef_terminal_game_timeout.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_terminal_game_url
    '
    Me.ef_terminal_game_url.DoubleValue = 0.0R
    Me.ef_terminal_game_url.IntegerValue = 0
    Me.ef_terminal_game_url.IsReadOnly = False
    Me.ef_terminal_game_url.Location = New System.Drawing.Point(1, 57)
    Me.ef_terminal_game_url.Name = "ef_terminal_game_url"
    Me.ef_terminal_game_url.PlaceHolder = Nothing
    Me.ef_terminal_game_url.ShortcutsEnabled = True
    Me.ef_terminal_game_url.Size = New System.Drawing.Size(373, 24)
    Me.ef_terminal_game_url.SufixText = "Sufix Text"
    Me.ef_terminal_game_url.SufixTextVisible = True
    Me.ef_terminal_game_url.TabIndex = 1
    Me.ef_terminal_game_url.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_terminal_game_url.TextValue = ""
    Me.ef_terminal_game_url.TextWidth = 120
    Me.ef_terminal_game_url.Value = ""
    Me.ef_terminal_game_url.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_terminal
    '
    Me.gb_terminal.Controls.Add(Me.chk_allow_withdraw_pending)
    Me.gb_terminal.Controls.Add(Me.chk_allow_cancel_recharge)
    Me.gb_terminal.Controls.Add(Me.ef_terminal_game_draws)
    Me.gb_terminal.Controls.Add(Me.chk_player_can_cancel_draw)
    Me.gb_terminal.Controls.Add(Me.chk_show_buy_dialog)
    Me.gb_terminal.Controls.Add(Me.ef_terminal_game_timeout)
    Me.gb_terminal.Controls.Add(Me.ef_terminal_game_name)
    Me.gb_terminal.Controls.Add(Me.ef_terminal_game_url)
    Me.gb_terminal.Location = New System.Drawing.Point(20, 27)
    Me.gb_terminal.Name = "gb_terminal"
    Me.gb_terminal.Size = New System.Drawing.Size(413, 205)
    Me.gb_terminal.TabIndex = 1
    Me.gb_terminal.TabStop = False
    Me.gb_terminal.Text = "xTerminal"
    Me.gb_terminal.Visible = False
    '
    'chk_allow_withdraw_pending
    '
    Me.chk_allow_withdraw_pending.AutoSize = True
    Me.chk_allow_withdraw_pending.Location = New System.Drawing.Point(238, 175)
    Me.chk_allow_withdraw_pending.Name = "chk_allow_withdraw_pending"
    Me.chk_allow_withdraw_pending.Size = New System.Drawing.Size(161, 17)
    Me.chk_allow_withdraw_pending.TabIndex = 7
    Me.chk_allow_withdraw_pending.Text = "xAllowWithdrawPending"
    Me.chk_allow_withdraw_pending.UseVisualStyleBackColor = True
    '
    'chk_allow_cancel_recharge
    '
    Me.chk_allow_cancel_recharge.AutoSize = True
    Me.chk_allow_cancel_recharge.Location = New System.Drawing.Point(238, 151)
    Me.chk_allow_cancel_recharge.Name = "chk_allow_cancel_recharge"
    Me.chk_allow_cancel_recharge.Size = New System.Drawing.Size(156, 17)
    Me.chk_allow_cancel_recharge.TabIndex = 6
    Me.chk_allow_cancel_recharge.Text = "xAllowCancelRecharge"
    Me.chk_allow_cancel_recharge.UseVisualStyleBackColor = True
    '
    'ef_terminal_game_draws
    '
    Me.ef_terminal_game_draws.DoubleValue = 0.0R
    Me.ef_terminal_game_draws.IntegerValue = 0
    Me.ef_terminal_game_draws.IsReadOnly = False
    Me.ef_terminal_game_draws.Location = New System.Drawing.Point(1, 119)
    Me.ef_terminal_game_draws.Name = "ef_terminal_game_draws"
    Me.ef_terminal_game_draws.PlaceHolder = Nothing
    Me.ef_terminal_game_draws.ShortcutsEnabled = True
    Me.ef_terminal_game_draws.Size = New System.Drawing.Size(290, 24)
    Me.ef_terminal_game_draws.SufixText = "Sufix Text"
    Me.ef_terminal_game_draws.SufixTextVisible = True
    Me.ef_terminal_game_draws.SufixTextWidth = 80
    Me.ef_terminal_game_draws.TabIndex = 3
    Me.ef_terminal_game_draws.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_terminal_game_draws.TextValue = ""
    Me.ef_terminal_game_draws.TextWidth = 120
    Me.ef_terminal_game_draws.Value = ""
    Me.ef_terminal_game_draws.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_player_can_cancel_draw
    '
    Me.chk_player_can_cancel_draw.AutoSize = True
    Me.chk_player_can_cancel_draw.Location = New System.Drawing.Point(5, 174)
    Me.chk_player_can_cancel_draw.Name = "chk_player_can_cancel_draw"
    Me.chk_player_can_cancel_draw.Size = New System.Drawing.Size(152, 17)
    Me.chk_player_can_cancel_draw.TabIndex = 5
    Me.chk_player_can_cancel_draw.Text = "xThePlayerCanCancel"
    Me.chk_player_can_cancel_draw.UseVisualStyleBackColor = True
    '
    'chk_show_buy_dialog
    '
    Me.chk_show_buy_dialog.AutoSize = True
    Me.chk_show_buy_dialog.Location = New System.Drawing.Point(5, 152)
    Me.chk_show_buy_dialog.Name = "chk_show_buy_dialog"
    Me.chk_show_buy_dialog.Size = New System.Drawing.Size(122, 17)
    Me.chk_show_buy_dialog.TabIndex = 4
    Me.chk_show_buy_dialog.Text = "xShowBuyDialog"
    Me.chk_show_buy_dialog.UseVisualStyleBackColor = True
    '
    'ef_terminal_game_name
    '
    Me.ef_terminal_game_name.DoubleValue = 0.0R
    Me.ef_terminal_game_name.IntegerValue = 0
    Me.ef_terminal_game_name.IsReadOnly = False
    Me.ef_terminal_game_name.Location = New System.Drawing.Point(1, 24)
    Me.ef_terminal_game_name.Name = "ef_terminal_game_name"
    Me.ef_terminal_game_name.PlaceHolder = Nothing
    Me.ef_terminal_game_name.ShortcutsEnabled = True
    Me.ef_terminal_game_name.Size = New System.Drawing.Size(373, 24)
    Me.ef_terminal_game_name.SufixText = "Sufix Text"
    Me.ef_terminal_game_name.SufixTextVisible = True
    Me.ef_terminal_game_name.TabIndex = 0
    Me.ef_terminal_game_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_terminal_game_name.TextValue = ""
    Me.ef_terminal_game_name.TextWidth = 120
    Me.ef_terminal_game_name.Value = ""
    Me.ef_terminal_game_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'frm_cashdesk_draws_configuration_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1062, 541)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
    Me.Name = "frm_cashdesk_draws_configuration_edit"
    Me.Text = "frm_cashdesk_draws_configuration_edit"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.gb_cashier.ResumeLayout(False)
    Me.gb_cashier.PerformLayout()
    Me.gb_ball_config.ResumeLayout(False)
    Me.pnl_enable_draws.ResumeLayout(False)
    Me.gb_prize_probability.ResumeLayout(False)
    Me.gb_prizes.ResumeLayout(False)
    Me.gb_prizes.PerformLayout()
    Me.gb_draw_connection.ResumeLayout(False)
    Me.gb_draw_connection.PerformLayout()
    Me.panel_webservice.ResumeLayout(False)
    Me.panel_webservice.PerformLayout()
    Me.panel_not_connected.ResumeLayout(False)
    Me.panel_not_connected.PerformLayout()
    Me.gb_ext_db_connection_string.ResumeLayout(False)
    Me.gb_terminal.ResumeLayout(False)
    Me.gb_terminal.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents chk_enable_draws As System.Windows.Forms.CheckBox
  Friend WithEvents gb_ball_config As System.Windows.Forms.GroupBox
  Friend WithEvents ef_total_balls As GUI_Controls.uc_entry_field
  Friend WithEvents ef_extracted_balls As GUI_Controls.uc_entry_field
  Friend WithEvents gb_cashier As System.Windows.Forms.GroupBox
  Friend WithEvents chk_ask_participation As System.Windows.Forms.CheckBox
  Friend WithEvents chk_print_voucher_winner As System.Windows.Forms.CheckBox
  Friend WithEvents chk_show_draws_cd As System.Windows.Forms.CheckBox
  Friend WithEvents pnl_enable_draws As System.Windows.Forms.Panel
  Friend WithEvents ef_balls_participant As GUI_Controls.uc_entry_field
  Friend WithEvents gb_prizes As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_example_loser As System.Windows.Forms.Label
  Friend WithEvents lbl_loser As System.Windows.Forms.Label
  Friend WithEvents ef_access_perc_loser As GUI_Controls.uc_entry_field
  Friend WithEvents ef_cprize_percentage As GUI_Controls.uc_entry_field
  Friend WithEvents ef_cprize_fixed As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_prizes_access_perc As System.Windows.Forms.Label
  Friend WithEvents lbl_prizes_percentage As System.Windows.Forms.Label
  Friend WithEvents lbl_prizes_fixed As System.Windows.Forms.Label
  Friend WithEvents lbl_prizes_enable As System.Windows.Forms.Label
  Friend WithEvents lbl_prizes_category As System.Windows.Forms.Label
  Friend WithEvents chk_enable_loser As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_prizes_example As System.Windows.Forms.Label
  Friend WithEvents chk_enable_winner_4 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_enable_winner_3 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_enable_winner_2 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_enable_winner_1 As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_winner_prize4 As System.Windows.Forms.Label
  Friend WithEvents lbl_example_winner_4 As System.Windows.Forms.Label
  Friend WithEvents ef_access_perc_winner_4 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_perc_4 As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_example_winner_3 As System.Windows.Forms.Label
  Friend WithEvents ef_fixed_4 As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_winner_prize3 As System.Windows.Forms.Label
  Friend WithEvents ef_access_perc_winner_3 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_perc_3 As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_winner_prize2 As System.Windows.Forms.Label
  Friend WithEvents ef_fixed_3 As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_example_winner_2 As System.Windows.Forms.Label
  Friend WithEvents ef_access_perc_winner_2 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_perc_2 As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_example_winner_1 As System.Windows.Forms.Label
  Friend WithEvents ef_fixed_2 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_access_perc_winner_1 As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_winner_prize1 As System.Windows.Forms.Label
  Friend WithEvents ef_perc_1 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_fixed_1 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_voucher_name_winner As GUI_Controls.uc_entry_field
  Friend WithEvents ef_voucher_name_loser As GUI_Controls.uc_entry_field
  Friend WithEvents chk_print_voucher_loser As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_prizes_nonredemable As System.Windows.Forms.Label
  Friend WithEvents lbl_cpromo_explanation As System.Windows.Forms.Label
  Friend WithEvents lbl_prize_explanation As System.Windows.Forms.Label
  Friend WithEvents cmb_prize_mode As GUI_Controls.uc_combo
  Friend WithEvents cmb_prize4_credit_type As GUI_Controls.uc_combo
  Friend WithEvents cmb_prize3_credit_type As GUI_Controls.uc_combo
  Friend WithEvents cmb_prize2_credit_type As GUI_Controls.uc_combo
  Friend WithEvents cmb_prize1_credit_type As GUI_Controls.uc_combo
  Friend WithEvents cmb_loser_credit_type As GUI_Controls.uc_combo
  Friend WithEvents ef_tax_provisions_loser As GUI_Controls.uc_entry_field
  Friend WithEvents ef_tax_provisions_4 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_tax_provisions_3 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_tax_provisions_2 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_tax_provisions_1 As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_tax_provisions As System.Windows.Forms.Label
  Friend WithEvents lbl_tax_provisions_remarks As System.Windows.Forms.Label
  Friend WithEvents ef_participation_price As GUI_Controls.uc_entry_field
  Friend WithEvents gb_prize_probability As System.Windows.Forms.GroupBox
  Friend WithEvents ef_winners As GUI_Controls.uc_entry_field
  Friend WithEvents ef_participants As GUI_Controls.uc_entry_field
  Friend WithEvents gb_draw_connection As System.Windows.Forms.GroupBox
  Friend WithEvents panel_webservice As System.Windows.Forms.Panel
  Friend WithEvents rb_opt_webservice As System.Windows.Forms.RadioButton
  Friend WithEvents lbl_center_address_1 As System.Windows.Forms.Label
  Friend WithEvents panel_not_connected As System.Windows.Forms.Panel
  Friend WithEvents rb_recharge_no_red As System.Windows.Forms.RadioButton
  Friend WithEvents lbl_not_connected As System.Windows.Forms.Label
  Friend WithEvents rb_recharge_red As System.Windows.Forms.RadioButton
  Friend WithEvents rb_dont_recharge As System.Windows.Forms.RadioButton
  Friend WithEvents lbl_center_address_2 As System.Windows.Forms.Label
  Friend WithEvents ef_center_address_2 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_center_address_1 As GUI_Controls.uc_entry_field
  Friend WithEvents rb_opt_ext_db As System.Windows.Forms.RadioButton
  Friend WithEvents rb_opt_Local_DB As System.Windows.Forms.RadioButton
  Friend WithEvents gb_ext_db_connection_string As System.Windows.Forms.GroupBox
  Friend WithEvents ef_extdb_server_2 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_extdb_pwd As GUI_Controls.uc_entry_field
  Friend WithEvents ef_extdb_server_1 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_extdb_user As GUI_Controls.uc_entry_field
  Friend WithEvents ef_extdb_database As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_probability As System.Windows.Forms.Label
  Friend WithEvents ef_probability_5 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_probability_4 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_probability_3 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_probability_2 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_probability_1 As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_sum As System.Windows.Forms.Label
  Friend WithEvents ef_participation_max_price As GUI_Controls.uc_entry_field
  Friend WithEvents ef_terminal_game_url As GUI_Controls.uc_entry_field
  Friend WithEvents ef_terminal_game_timeout As GUI_Controls.uc_entry_field
  Friend WithEvents gb_terminal As System.Windows.Forms.GroupBox
  Friend WithEvents ef_terminal_game_name As GUI_Controls.uc_entry_field
  Friend WithEvents chk_player_can_cancel_draw As System.Windows.Forms.CheckBox
  Friend WithEvents chk_show_buy_dialog As System.Windows.Forms.CheckBox
  Friend WithEvents ef_terminal_game_draws As GUI_Controls.uc_entry_field
  Friend WithEvents chk_allow_withdraw_pending As System.Windows.Forms.CheckBox
  Friend WithEvents chk_allow_cancel_recharge As System.Windows.Forms.CheckBox
End Class
