'-------------------------------------------------------------------
' Copyright � 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_multisite_multicurrency.vb
'
' DESCRIPTION : MultiSite currency rate edition
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 22-APR-2015  ANM    Initial version.
'--------------------------------------------------------------------
Option Strict Off
Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE
Imports GUI_CommonOperations.CLASS_BASE
Imports WSI.Common
Imports System.Globalization
Imports System.Threading.Thread

Public Class frm_multisite_multicurrency
  Inherits frm_base_edit

#Region " Constants "

  ' Grid Columns
  Private Const GRID_COLUMNS As Integer = 5
  Private Const GRID_HEADER_ROWS As Integer = 1

  Private Const GRID_COLUMN_CURRENCY As Integer = 0
  Private Const GRID_COLUMN_CODE As Integer = 1
  Private Const GRID_COLUMN_CODE_VIEW As Integer = 2
  Private Const GRID_COLUMN_CONVERSION As Integer = 3
  Private Const GRID_COLUMN_NATIONAL_CURRENCY As Integer = 4

  Private Const MAX_NUMBER_EXCHANGE As Decimal = 99999999.999999985
#End Region 'Constants

#Region " Members "

  Private m_main_currency As String

#End Region ' Members

#Region "Overrides"

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_MULTI_CURRENCY

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    ' Initialize Form Controls

    ' - Form
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6239)

    ef_main_currency.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT)
    ef_main_currency.IsReadOnly = True
    ef_main_currency.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1057)
    dtp_day.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    dtp_day.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(268)
    dtp_day.SetRange(New Date(2007, 1, 1), WGDB.Now)

    ' Configure Data Grid
    Call GUI_StyleView()

    ' Enable / Disable controls
    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = False
    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Visible = False
    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Enabled = True

    uc_search.Text = GLB_NLS_GUI_CONTROLS.GetString(8)
    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Text = GLB_NLS_GUI_CONTROLS.GetString(13)
    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)

    'Disable Close Windows in Button OK
    CloseOnOkClick = False

  End Sub 'GUI_InitControls

  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Dim _idx As Integer

    For _idx = 0 To dg_coins.NumRows - 1
      'if it's empty
      If dg_coins.Cell(_idx, GRID_COLUMN_CONVERSION).Value <> String.Empty Then
        '(don't control if isn't numeric because it isn't possible)

        'if it's 0
        If dg_coins.Cell(_idx, GRID_COLUMN_CONVERSION).Value = 0 Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6281), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , dg_coins.Cell(_idx, GRID_COLUMN_CODE).Value)

          Return False
        Else
          'if it's negative
          If dg_coins.Cell(_idx, GRID_COLUMN_CONVERSION).Value < 0 Then
            dg_coins.Cell(_idx, GRID_COLUMN_CONVERSION).Value = ""

            Return False
          End If
          'if it's larger than allowed
          If dg_coins.Cell(_idx, GRID_COLUMN_CONVERSION).Value > MAX_NUMBER_EXCHANGE Then
            dg_coins.Cell(_idx, GRID_COLUMN_CONVERSION).Value = ""

            Return False
          End If
        End If
      Else
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6280), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , dg_coins.Cell(_idx, GRID_COLUMN_CODE).Value)

        Return False
      End If

    Next

    Return True

  End Function ' GUI_IsScreenDataOk

  Protected Overrides Sub GUI_GetScreenData()
    Dim _idx As Integer
    Dim _currency_inst As CLASS_MULTI_CURRENCY.TYPE_MULTI_CURRENCY_INSTANCE
    Dim _multi_currency As CLASS_MULTI_CURRENCY

    _multi_currency = Me.DbEditedObject
    _multi_currency.Currencies.Clear()
    For _idx = 0 To dg_coins.NumRows - 1
      _currency_inst = New CLASS_MULTI_CURRENCY.TYPE_MULTI_CURRENCY_INSTANCE

      _currency_inst.iso_code = dg_coins.Cell(_idx, GRID_COLUMN_CODE).Value
      _currency_inst.iso_code_view = dg_coins.Cell(_idx, GRID_COLUMN_CODE_VIEW).Value

      If String.IsNullOrEmpty(dg_coins.Cell(_idx, GRID_COLUMN_CONVERSION).Value) OrElse Not IsNumeric(dg_coins.Cell(_idx, GRID_COLUMN_CONVERSION).Value) Then
        _currency_inst.change = 0
      Else
        _currency_inst.change = dg_coins.Cell(_idx, GRID_COLUMN_CONVERSION).Value
      End If

      _multi_currency.Currencies.Add(_currency_inst)
    Next

  End Sub ' GUI_GetScreenData

  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    Dim _multi_currency As CLASS_MULTI_CURRENCY

    _multi_currency = Me.DbReadObject

    ef_main_currency.Value = m_main_currency & " - " & _multi_currency.NationalCurrency

    dtp_day.Value = _multi_currency.WorkingDay
    lbl_day_note.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(268) & ": " & GUI_FormatDate(dtp_day.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    Call SetGrid(_multi_currency)

  End Sub ' GUI_SetScreenData

  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)
    Dim _multi_currency As CLASS_MULTI_CURRENCY

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_MULTI_CURRENCY
        _multi_currency = DbEditedObject
        m_main_currency = CurrencyMultisite.GetMainCurrency()
        _multi_currency.NationalIsoCurrency = m_main_currency

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        Else
          _multi_currency = Me.DbEditedObject
          GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Visible = _multi_currency.Currencies.Count <> 0
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE
        _multi_currency = Me.DbEditedObject
        Me.DbStatus = ENUM_STATUS.STATUS_OK

      Case ENUM_DB_OPERATION.DB_OPERATION_UPDATE
        If Me.DbStatus = ENUM_STATUS.STATUS_OK Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK AndAlso Me.DbStatus <> ENUM_STATUS.STATUS_NOT_SUPPORTED Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE
        If Me.DbEditedObject IsNot Nothing Then
          _multi_currency = Me.DbEditedObject
        End If
        Call MyBase.GUI_DB_Operation(DbOperation)
      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)
    End Select

  End Sub ' GUI_DB_Operation

  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean
    ' The keypress event is done in uc_grid control
    If dg_coins.ContainsFocus Then

      Return dg_coins.KeyPressed(sender, e)
    End If

    Return MyBase.GUI_KeyPress(sender, e)

  End Function ' GUI_KeyPress

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_OK
        If Me.dtp_day.ContainsFocus Then
          uc_search_ClickEvent()
        Else
          ' Assign to ERROR, so the message will be only shown when OK.
          DbStatus = ENUM_STATUS.STATUS_ERROR
          MyBase.GUI_ButtonClick(ButtonId)
          If DbStatus = ENUM_STATUS.STATUS_OK Then
            Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(108), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO)
          End If
        End If
      Case Else
        MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

#End Region

#Region "Private Functions"

  Private Sub InitializeAuditorData()
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(False)
    End If

  End Sub ' InitializeAuditorData

  ' PURPOSE: Configure Grid columns
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - none
  '
  Private Sub GUI_StyleView()

    With dg_coins

      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, True)

      ' Grid Columns

      'Currency
      .Column(GRID_COLUMN_CURRENCY).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_CURRENCY).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1069)
      .Column(GRID_COLUMN_CURRENCY).Width = 1970
      .Column(GRID_COLUMN_CURRENCY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'ISO Code (invisible)
      .Column(GRID_COLUMN_CODE).Header.Text = String.Empty
      .Column(GRID_COLUMN_CODE).Width = 0

      'ISO Code (visible)
      .Column(GRID_COLUMN_CODE_VIEW).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_CODE_VIEW).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1057)
      .Column(GRID_COLUMN_CODE_VIEW).Width = 850
      .Column(GRID_COLUMN_CODE_VIEW).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'Conversion
      .Column(GRID_COLUMN_CONVERSION).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_CONVERSION).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2144)
      .Column(GRID_COLUMN_CONVERSION).Width = 1300
      .Column(GRID_COLUMN_CONVERSION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_CONVERSION).Editable = True
      .Column(GRID_COLUMN_CONVERSION).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_CONVERSION).EditionControl.EntryField.IsReadOnly = False
      .Column(GRID_COLUMN_CONVERSION).EditionControl.EntryField.SetFilter(FORMAT_NUMBER, 17, 8)   '16 + comma

      'National currency
      .Column(GRID_COLUMN_NATIONAL_CURRENCY).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_NATIONAL_CURRENCY).Header.Text = ""
      .Column(GRID_COLUMN_NATIONAL_CURRENCY).Width = 550
      .Column(GRID_COLUMN_NATIONAL_CURRENCY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With

  End Sub ' GUI_StyleView

  Private Sub SetGrid(ByVal MultiCurrency As CLASS_MULTI_CURRENCY)
    Dim _idx As Integer

    dg_coins.Clear()
    For Each currency As CLASS_MULTI_CURRENCY.TYPE_MULTI_CURRENCY_INSTANCE In MultiCurrency.Currencies
      dg_coins.AddRow()
      dg_coins.Cell(_idx, GRID_COLUMN_CURRENCY).Value = currency.description
      dg_coins.Cell(_idx, GRID_COLUMN_CODE).Value = currency.iso_code
      dg_coins.Cell(_idx, GRID_COLUMN_CODE_VIEW).Value = currency.iso_code_view
      If DbStatus <> ENUM_STATUS.STATUS_ERROR Then
        If currency.change <> 0 Then
          dg_coins.Cell(_idx, GRID_COLUMN_CONVERSION).Value = GUI_FormatCurrency(currency.change, 8, , False, , True)
        End If
      End If
      dg_coins.Cell(_idx, GRID_COLUMN_NATIONAL_CURRENCY).Value = m_main_currency
      _idx += 1
    Next

  End Sub ' SetGrid

#End Region ' Private Functions

#Region "Public Functions"

#End Region ' Public Functions

#Region "Events"

  Private Sub uc_search_ClickEvent() Handles uc_search.ClickEvent
    Dim _multi_currency As CLASS_MULTI_CURRENCY
    Dim _print_data As GUI_Reports.CLASS_PRINT_DATA

    'take a look if there is data to save
    Call GUI_GetScreenData()
    If Not DbReadObject.AuditorData.IsEqual(DbEditedObject.AuditorData) Then
      If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(122), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then

        Exit Sub
      End If
    End If

    'manual auditor. Because the uc_search button is not common
    _print_data = New GUI_Reports.CLASS_PRINT_DATA
    _print_data.SetFilter(dtp_day.Text, GUI_FormatDate(dtp_day.Value))
    Call MyBase.AuditFormClick(ENUM_BUTTON.BUTTON_FILTER_APPLY, _print_data)

    'update info to auditor data
    _multi_currency = Me.DbEditedObject
    _multi_currency.WorkingDay = dtp_day.Value
    lbl_day_note.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(268) & ": " & GUI_FormatDate(dtp_day.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)

    Call InitializeAuditorData()
    Call SetGrid(_multi_currency)

  End Sub ' uc_search_ClickEvent

#End Region ' Events

End Class