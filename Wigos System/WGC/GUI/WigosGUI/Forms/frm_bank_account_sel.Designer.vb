<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_bank_account_sel
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.ef_bank_name = New GUI_Controls.uc_entry_field
    Me.ef_account_info = New GUI_Controls.uc_entry_field
    Me.ef_customer_number = New GUI_Controls.uc_entry_field
    Me.ef_account_number = New GUI_Controls.uc_entry_field
    Me.gb_filers = New System.Windows.Forms.GroupBox
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_filers.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_filers)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(891, 95)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_filers, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 99)
    Me.panel_data.Size = New System.Drawing.Size(891, 447)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(885, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(885, 4)
    '
    'ef_bank_name
    '
    Me.ef_bank_name.DoubleValue = 0
    Me.ef_bank_name.IntegerValue = 0
    Me.ef_bank_name.IsReadOnly = False
    Me.ef_bank_name.Location = New System.Drawing.Point(49, 16)
    Me.ef_bank_name.Name = "ef_bank_name"
    Me.ef_bank_name.Size = New System.Drawing.Size(260, 24)
    Me.ef_bank_name.SufixText = "Sufix Text"
    Me.ef_bank_name.SufixTextVisible = True
    Me.ef_bank_name.TabIndex = 11
    Me.ef_bank_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_bank_name.TextValue = ""
    Me.ef_bank_name.TextWidth = 120
    Me.ef_bank_name.Value = ""
    '
    'ef_account_info
    '
    Me.ef_account_info.DoubleValue = 0
    Me.ef_account_info.IntegerValue = 0
    Me.ef_account_info.IsReadOnly = False
    Me.ef_account_info.Location = New System.Drawing.Point(8, 53)
    Me.ef_account_info.Name = "ef_account_info"
    Me.ef_account_info.Size = New System.Drawing.Size(300, 24)
    Me.ef_account_info.SufixText = "Sufix Text"
    Me.ef_account_info.SufixTextVisible = True
    Me.ef_account_info.TabIndex = 12
    Me.ef_account_info.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_account_info.TextValue = ""
    Me.ef_account_info.TextWidth = 160
    Me.ef_account_info.Value = ""
    '
    'ef_customer_number
    '
    Me.ef_customer_number.DoubleValue = 0
    Me.ef_customer_number.IntegerValue = 0
    Me.ef_customer_number.IsReadOnly = False
    Me.ef_customer_number.Location = New System.Drawing.Point(327, 16)
    Me.ef_customer_number.Name = "ef_customer_number"
    Me.ef_customer_number.Size = New System.Drawing.Size(260, 24)
    Me.ef_customer_number.SufixText = "Sufix Text"
    Me.ef_customer_number.SufixTextVisible = True
    Me.ef_customer_number.TabIndex = 13
    Me.ef_customer_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_customer_number.TextValue = ""
    Me.ef_customer_number.TextWidth = 120
    Me.ef_customer_number.Value = ""
    '
    'ef_account_number
    '
    Me.ef_account_number.DoubleValue = 0
    Me.ef_account_number.IntegerValue = 0
    Me.ef_account_number.IsReadOnly = False
    Me.ef_account_number.Location = New System.Drawing.Point(327, 53)
    Me.ef_account_number.Name = "ef_account_number"
    Me.ef_account_number.Size = New System.Drawing.Size(260, 24)
    Me.ef_account_number.SufixText = "Sufix Text"
    Me.ef_account_number.SufixTextVisible = True
    Me.ef_account_number.TabIndex = 14
    Me.ef_account_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_account_number.TextValue = ""
    Me.ef_account_number.TextWidth = 120
    Me.ef_account_number.Value = ""
    '
    'gb_filers
    '
    Me.gb_filers.Controls.Add(Me.ef_account_number)
    Me.gb_filers.Controls.Add(Me.ef_bank_name)
    Me.gb_filers.Controls.Add(Me.ef_account_info)
    Me.gb_filers.Controls.Add(Me.ef_customer_number)
    Me.gb_filers.Location = New System.Drawing.Point(90, 4)
    Me.gb_filers.Name = "gb_filers"
    Me.gb_filers.Size = New System.Drawing.Size(604, 88)
    Me.gb_filers.TabIndex = 15
    Me.gb_filers.TabStop = False
    '
    'frm_bank_account_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(901, 550)
    Me.Name = "frm_bank_account_sel"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_bank_account_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_filers.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_bank_name As GUI_Controls.uc_entry_field
  Friend WithEvents ef_account_info As GUI_Controls.uc_entry_field
  Friend WithEvents ef_customer_number As GUI_Controls.uc_entry_field
  Friend WithEvents ef_account_number As GUI_Controls.uc_entry_field
  Friend WithEvents gb_filers As System.Windows.Forms.GroupBox
End Class
