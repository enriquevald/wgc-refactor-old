'-------------------------------------------------------------------
' Copyright � 2007 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_cashier_movements
' DESCRIPTION:   This screen allows to view cashier movements:
'                           - between two dates 
'                           - for one or all cashiers
'                           - for one or all users
'                           - for one or all movements types
'                           - for one or all cashier sessions
' AUTHOR:        Agust� Poch
' CREATION DATE: 31-AUG-2007
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 31-AUG-2007  APB    Initial version
' 15-JUN-2010  RCI    Added totals by movement type and movement 'Draw Ticket Print'
' 22-NOV-2011  JMM    Added new movements (PIN Change & PIN Generation)
' 17-FEB-2012  RCI    Added new movement (CashIn CoverCoupon)
' 29-FEB-2012  JMM    On MB CashIn, insert the related movements to NR & NR2 into CASHIER_MOVEMENTS
' 24-APR-2012  JCM    Added new Movement (Card Recycle)
' 08-MAR-2012  SSC    Added new Movements for Note Acceptor
' 22-MAY-2012  ACC    Add code and functions for Acceptors funcionality. Change NLS of acceptors movements.
' 13-JUN-2012  SSC    Added new movements for Gifts/Points
' 06-JUL-2012  DDM    Added movement the Promo. Redeemable in the type filter
' 11-JUL-2012  JAB    Added new movements type.
' 31-JUL-2012  DDM    Added movement Promo. Per Points and Cancel Promo. Per points.
' 18-AUG-2012  XCD    Added show unsubscribed to filter users 
' 27-SEP-2012  DDM    Added new movement REQUEST_TO_VOUCHER_REPRINT
' 12-NOV-2012  RRB    Exclude MB and NA movements when filtering by user
' 25-FEB-2013  RRB    Added CASHIER_TERMINALS in query.
' 16-APR-2013  DMR    Changing cashier name for its alias
' 22-APR-2013  SMN    Fixed Bug #730: In print - Incorrect format of Movement Type filter
' 09-MAY-2013  RBG    Added details in "Type" column on promotion points rows.
' 09-MAY-2013  ACM    Added account block/unblock movements.
' 14-JUN-2013  JML    Fixed Bug #853: Error displaying cashier movements
' 22-JUL-2013  LEM    Overloaded ShowForEdit to filter by date.
' 26-JUL-2013  ICS    Added Currency Exchange movements
' 08-AUG-2013  DLL    Added details in "Type" column on Fill in, Fill out, Open Session and Close Session in Currency Exchange
' 27-AUG-2013  FBA    Added control for new movement Recharge for points
' 27-AUG-2013  NMR    Added control for TITO's movement points
' 28-AUG-2013  NMR    Added Cashier and Account TITO movements
' 30-AUG-2013  ACM    Fixed Bug WIG-164 Currency code printed twice
' 03-SEP-2013  JBC    Fixed Bug WIG-177 Credit Card movements bad formated
' 10-SEP-2013  SMN & DHA    Added movements types for Cash Desk Draw
' 10-SEP-2013  ICS    Added Check Recharges movements
' 13-SEP-2013  NMR    Setting NLS for new Cashier movement
' 17-SEP-2013  JBP    Added Cashier movements
' 18-SEP-2013  NMR    Added new TITO cashier movement
' 20-SEP-2013  ICS    Added new movements: Check Filler Out, Cash Closing with Check and Bank card
' 26-SEP-2013  NMR    Adding new TITO movement types
' 01-OCT-2013  ICS    Fixed Bug WIG-239 The closing cash movements of bank card and Check are always zero
' 04-OCT-2013  DRV    Added Header check status "Checked Disabled" when some items are selected
' 16-OCT-2013  ICS    Added Undo Operation suport
' 28-NOV-2013  MMG    Fixed bug WIG-377 (Modified FORM_DB_MIN_VERSION value according to the data base version which were added the last columns)
' 28-NOV-2013  JCA    Added functionality for Cage
' 03-DEC-2013  JCA    Added Cage Movements
' 17-DEC-2013  RMS    In TITO Mode, virtual accounts id should be hidden
' 20-DEC-2013  LJM    New chips functionality added.
' 20-DEC-2013  LJM & RMS    Changed the query, some movements where not shown
' 20-DEC-2013  LEM    Hide Company B info from filter movement types if not enabled
' 02-JAN-2014  DLL    Added new movement CASH_IN_TAX and grouped movement description
' 22-JAN-2014  JPJ    Added new movements TITO expired
' 31-JAN-2014  JPJ    Added new movements Handpay and jackpot
' 03-FEB-2014  DHA    Fixed Bug WIGOSTITO-1030: If TITO mode change movement "MB Deposit" to "Stacker collection"
' 05-FEB-2014  DHA    Added new movements Handpay and jackpot redeem
' 20-FEB-2014  JPJ    Defect 656 Tito tickets shouldn't have total and units must be set correctly.
' 13-MAR-2014  SMN    Incorrect declaration of variable. Modified: Short -> Integer
' 19-MAR-2014  DLL    Fixed bug WIG-749: Total chips movements are not showed
' 16-APR-2014  LEM    Fixed Bug WIG-835: Wrong description for Company B movements
' 25-APR-2014  DHA & JML Rename/Added TITO Cashier/Account Movements
' 08-JUL-2014  AMF    Personal Card Replacement
' 15-SEP-2014  AMF    Added new movement Concepts Input - Output
' 18-SEP-2014  DLL    Added new movement Cash Advance
' 22-SEP-2014  SMN    Added "(Undone)" to Concept movements
' 08-OCT-2014  DHA    Added new movement, advice if a recharge is going to refund without plays
' 13-NOV-2014  LRS    Add currency type column
' 18-NOV-2014  OPC    Added account filter
' 24-NOV-2014 MPO & OPC  LOGRAND CON02: Added IVA calculated (when needed)
' 02-DEC-2014  OPC    Fixed Bug WIG-1792 and WIG-1784
' 09-DIC-2014  OPC    Fixed Bug WIG-1816: base tax negative when undo last operation.
' 10-DEC-2014  DRV    Decimal currency formatting: DUT 201411 - Tito Chile - Currency formatting without decimals
' 15-DEC-2014  JBC, DLL, JPJ    New Company B commission movements 
' 23-DEC-2014  MPO    WIG-1888: It isn't necesarry gui_users table. 
' 16-FEB-2015  FOS    Defect WIG-1804: Hide BM Movements when mode is TITO.
' 16-MAR-2015  YNM    Fixed Bug WIG-2138: Check activated on Cashier Movements
' 01-JUL-2015  FOS    New movements create and replace card
' 02-JUL-2015  JCA & FJC Fixed Bug 2525
' 14-JUL-2015  DCS    Fixed Bug WIG-2563: Add expired TITO movements
' 08-OCT-2015  FOS    Rename NLS in report columns "Efectivo" 
' 20-NOV-2015 FAV     PBI 6048: Report to Promobox movements about print promotions not redeemable
' 03-DIC-2015 FAV     PBI 6048: Report to Promobox movements about print promotions redeemable
' 28-JAN-2016 DHA    Product Backlog Item 8351:Floor Dual Currency: Movimientos de tickets TITO en M.N.
' 03-FEB-2016 ETP    FIXED BUG 8941 NLS And bucket movement are incorrect
' 09-FEB-2016  FAV    PBI 9148: void machine ticket
' 24-FEB-2016  RAB    Bug 9928:Movimientos de caja: etiqueta incorrecta al validar un t�cket TITO en estado pendiente de cancelaci�n
' 16-FEB-2016 LA      PBI 7496: New Movements (RE)
' 02-MAR-2016  DHA    Product Backlog Item 10085: Winpot - added Tax Provisions
' 03-MAR-2016  FAV    Fixed Bug 10254: Added TITO_VALIDATE_TICKET movement in the type filter
' 14-MAR-2016  EOR    Product Backlog Item 10235:TITA: Reporte de tickets
' 19-APR-2016  RGR    Product Backlog Item 11297: Codere - Tax Custody: Reports and cash movements and account
' 27-APR-2016  RGR    Product Backlog Item 11298: Codere - Tax Custody: Modify reports GUI Income and Return
' 03-MAR-2016 FAV     Fixed Bug 10254: Added TITO_VALIDATE_TICKET movement in the type filter
' 21-MAR-2016 JRC     PBI: 1792 Multiple Buckets: Desactivar Acumulaci�n de Buckets si SPACE is enabled
' 29-MAR-2016 ETP     Fixed Bug Bug 10994: GamingHall: added new movements.
' 08-APR-2016 DHA     Fixed Bug 11593: Floor Dual Currency - change movements filter
' 28-APR-2016 FGB     Product Backlog Item 9758: Tables (Phase 1): Review table reports
' 12-MAY-2016 ETP     Product Backlog Item 12652: Added new movements
' 12-MAY-2016 ETP     Fixed Bug 8067: new card and card substitucion not added in company B
' 24-MAY-2016 DHA     Product Backlog Item 9848: drop box gaming tables
' 23-JUN-2016 RAB     PBI 14486: GamingTables (Phase 1): Review compatibility (last cashier movements)
' 30-JUN-2016 ETP     Fixed Bug 9360: Movimientos de caja: no se informa de la cantidad al anular un t�cket TITO emitido por un terminal de juego
' 18-JUL-2016 DHA     Product Backlog Item 15068: Multicurrency sales/purchase, added movements
' 27-JUL-2016  PDM    Product Backlog Item 15448:Visitas / Recepci�n: MEJORAS - Cajero & GUI - Nuevo motivo de bloqueo
' 28-JUN-2016 DHA     Product Backlog Item 15903: Gaming tables (Fase 5): Drop box collection on Cashier
' 08-AUG-2016 FGB     Product Backlog Item 15614: TPV Televisa: Undo last operation on bank card
' 22-SEP-2016 EOR     PBI 17468: Televisa - Service Charge: Cashier Movements
' 27-SEP-2016 RAB     Bug 17875: TPV Televisa: Shortages of balance to cancel a credit card operation. Problems with the price of the player card
' 22-SEP-2016 EOR     PBI 17468: Televisa - Service Charge: Cashier Movements
' 27-SEP-2016 DHA     Fixed Bug 17847: added gaming profit for check and bank card
' 29-SEP-2016 RGR     PBI 17447: Televisa - Draw Cash (Cash prize) - Recharge Operation
' 05-OCT-2016  RGR    PBI 17964: Televisa - Draw Cash (Cash prize) - Configuration
' 05-OCT-2016 EOR     PBI 17963: Televisa - Service Charge: Modifications Voucher/Report/Settings
' 20-OCT-2016 PDM     Fixed Bug 19399:Cajero: No se muestran los cambios solicitados por Televisa en los "Movimientos de caja"
' 07-NOV-2016 ATB     Bug 20147:Televisa: Cambios solicitados por Televisa para "Movimientos en caja"
' 12-DEC-2016 AMF     Bug 5446:�ltimos movimientos del cajero y de Wigos GUI: registros alternados en la primera recarga y posterior anulaci�n
' 08-FEB-2017 CCG     Bug 24248:WigosGUI: Movimientos de caja, columnas mal justificadas
' 14-ENE-2017 DPC     Bug 24589:Pago de tarjeta: se tiene en cuenta el impuesto de la empresa B
' 23-FEB-2017 DPC     Bug 25029:Movimientos de caja: registro no identificado de la anulaci�n del adelanto de efectivo con tarjeta bancaria
' 13-MAR-2017 FGB     PBI 25735: Third TAX - Movement reports
' 22-MAR-2017 AMF     WIGOS-109: CreditLine - Get a Marker
' 19-JUN-2017 RAB     PBI 28000: WIGOS-2732 - Rounding - Movements
' 06-JUL-2017 RAB     PBI 28422: WIGOS-2702 Rounding - Consume the remaining amount
' 01-AUG-2017 JBM     Bug Wigos-4010  - Cancelled closing retirements are not subtracted from the total in "Cash movements" for Gambling Tables
' 02-AUG-2017 MS      PBI 29118: WIGOS-4165: WigosUI report an error message when the user try to see the movements of a closed gambling table session
' 04-AUG-2017 DPC     Bug 29190:[WIGOS-4229] Error message when the user try to see the movements of a reopened gambling table session
' 18-AUG-2017 AMF     Bug 29374:[WIGOS-3821] Missing selection criteria for exporting data in Cash Movements screen
' 21-FEB-2018 RGR     Bug 31635: WIGOS-3785 Incorrect warning message when Several is selected in selection criterion
' 01-JUN-2018 LTC     Bug 32865: WIGOS-12250 [Ticket #14556] Fallo � Movimiento de caja � Totales - Retiro en cierre Total Versi�n V03.08.0001 
' 20-JUN-2018 AGS     Bug 33223: WIGOS-13078 Gambling tables: Close cash session unbalanced error "There has been an error. Please try again" is displayed in gambling table with integrated cashier when there are credit card and check operations.
' 26-JUN-2018 DMT     PBI 33339: WIGOS-13132 Quantity of Color chips in Cash movements is always 1, no matter how many chips are sent/received.
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports WSI.Common

Public Class frm_cashier_movements
  Inherits frm_base_sel

  Public Structure TYPE_DG_FILTER_ELEMENT
    Dim code As Integer
    Dim description As String
    Dim elem_type As Integer
  End Structure

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents opt_one_cashier As System.Windows.Forms.RadioButton
  Friend WithEvents opt_all_cashiers As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_cashier As GUI_Controls.uc_combo
  Friend WithEvents gb_user As System.Windows.Forms.GroupBox
  Friend WithEvents opt_one_user As System.Windows.Forms.RadioButton
  Friend WithEvents opt_all_users As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_user As GUI_Controls.uc_combo
  Friend WithEvents gb_type As System.Windows.Forms.GroupBox
  Friend WithEvents opt_all_types As System.Windows.Forms.RadioButton
  Friend WithEvents opt_several_types As System.Windows.Forms.RadioButton
  Friend WithEvents dg_filter As GUI_Controls.uc_grid
  Friend WithEvents tf_cm_cash_desk_session As GUI_Controls.uc_text_field
  Friend WithEvents chk_show_all As System.Windows.Forms.CheckBox
  Friend WithEvents Uc_account_sel1 As GUI_Controls.uc_account_sel
  Friend WithEvents chk_not_show_system_sessions As System.Windows.Forms.CheckBox
  Friend WithEvents gb_cashier As System.Windows.Forms.GroupBox

  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_cashier_movements))
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.gb_cashier = New System.Windows.Forms.GroupBox()
    Me.opt_one_cashier = New System.Windows.Forms.RadioButton()
    Me.opt_all_cashiers = New System.Windows.Forms.RadioButton()
    Me.cmb_cashier = New GUI_Controls.uc_combo()
    Me.gb_user = New System.Windows.Forms.GroupBox()
    Me.chk_show_all = New System.Windows.Forms.CheckBox()
    Me.opt_one_user = New System.Windows.Forms.RadioButton()
    Me.opt_all_users = New System.Windows.Forms.RadioButton()
    Me.cmb_user = New GUI_Controls.uc_combo()
    Me.gb_type = New System.Windows.Forms.GroupBox()
    Me.opt_all_types = New System.Windows.Forms.RadioButton()
    Me.opt_several_types = New System.Windows.Forms.RadioButton()
    Me.dg_filter = New GUI_Controls.uc_grid()
    Me.tf_cm_cash_desk_session = New GUI_Controls.uc_text_field()
    Me.Uc_account_sel1 = New GUI_Controls.uc_account_sel()
    Me.chk_not_show_system_sessions = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_cashier.SuspendLayout()
    Me.gb_user.SuspendLayout()
    Me.gb_type.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.chk_not_show_system_sessions)
    Me.panel_filter.Controls.Add(Me.Uc_account_sel1)
    Me.panel_filter.Controls.Add(Me.gb_user)
    Me.panel_filter.Controls.Add(Me.tf_cm_cash_desk_session)
    Me.panel_filter.Controls.Add(Me.gb_type)
    Me.panel_filter.Controls.Add(Me.gb_cashier)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Size = New System.Drawing.Size(1205, 261)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_cashier, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.tf_cm_cash_desk_session, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_user, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.Uc_account_sel1, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_not_show_system_sessions, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 265)
    Me.panel_data.Size = New System.Drawing.Size(1205, 380)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1199, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1199, 4)
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(8, 8)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(272, 80)
    Me.gb_date.TabIndex = 0
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(25, 45)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(240, 25)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(25, 17)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(240, 25)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'gb_cashier
    '
    Me.gb_cashier.Controls.Add(Me.opt_one_cashier)
    Me.gb_cashier.Controls.Add(Me.opt_all_cashiers)
    Me.gb_cashier.Controls.Add(Me.cmb_cashier)
    Me.gb_cashier.Location = New System.Drawing.Point(8, 96)
    Me.gb_cashier.Name = "gb_cashier"
    Me.gb_cashier.Size = New System.Drawing.Size(272, 105)
    Me.gb_cashier.TabIndex = 1
    Me.gb_cashier.TabStop = False
    Me.gb_cashier.Text = "xCashier"
    '
    'opt_one_cashier
    '
    Me.opt_one_cashier.Location = New System.Drawing.Point(8, 31)
    Me.opt_one_cashier.Name = "opt_one_cashier"
    Me.opt_one_cashier.Size = New System.Drawing.Size(72, 24)
    Me.opt_one_cashier.TabIndex = 0
    Me.opt_one_cashier.Text = "xOne"
    '
    'opt_all_cashiers
    '
    Me.opt_all_cashiers.Location = New System.Drawing.Point(8, 58)
    Me.opt_all_cashiers.Name = "opt_all_cashiers"
    Me.opt_all_cashiers.Size = New System.Drawing.Size(64, 24)
    Me.opt_all_cashiers.TabIndex = 1
    Me.opt_all_cashiers.Text = "xAll"
    '
    'cmb_cashier
    '
    Me.cmb_cashier.AllowUnlistedValues = False
    Me.cmb_cashier.AutoCompleteMode = False
    Me.cmb_cashier.IsReadOnly = False
    Me.cmb_cashier.Location = New System.Drawing.Point(80, 31)
    Me.cmb_cashier.Name = "cmb_cashier"
    Me.cmb_cashier.SelectedIndex = -1
    Me.cmb_cashier.Size = New System.Drawing.Size(184, 24)
    Me.cmb_cashier.SufixText = "Sufix Text"
    Me.cmb_cashier.SufixTextVisible = True
    Me.cmb_cashier.TabIndex = 2
    Me.cmb_cashier.TextCombo = Nothing
    Me.cmb_cashier.TextVisible = False
    Me.cmb_cashier.TextWidth = 0
    '
    'gb_user
    '
    Me.gb_user.Controls.Add(Me.chk_show_all)
    Me.gb_user.Controls.Add(Me.opt_one_user)
    Me.gb_user.Controls.Add(Me.opt_all_users)
    Me.gb_user.Controls.Add(Me.cmb_user)
    Me.gb_user.Location = New System.Drawing.Point(288, 8)
    Me.gb_user.Name = "gb_user"
    Me.gb_user.Size = New System.Drawing.Size(305, 80)
    Me.gb_user.TabIndex = 2
    Me.gb_user.TabStop = False
    Me.gb_user.Text = "xUser"
    '
    'chk_show_all
    '
    Me.chk_show_all.AutoSize = True
    Me.chk_show_all.Location = New System.Drawing.Point(83, 50)
    Me.chk_show_all.Name = "chk_show_all"
    Me.chk_show_all.Size = New System.Drawing.Size(102, 17)
    Me.chk_show_all.TabIndex = 3
    Me.chk_show_all.Text = "chk_show_all"
    Me.chk_show_all.UseVisualStyleBackColor = True
    '
    'opt_one_user
    '
    Me.opt_one_user.Location = New System.Drawing.Point(8, 18)
    Me.opt_one_user.Name = "opt_one_user"
    Me.opt_one_user.Size = New System.Drawing.Size(72, 24)
    Me.opt_one_user.TabIndex = 0
    Me.opt_one_user.Text = "xOne"
    '
    'opt_all_users
    '
    Me.opt_all_users.Location = New System.Drawing.Point(8, 45)
    Me.opt_all_users.Name = "opt_all_users"
    Me.opt_all_users.Size = New System.Drawing.Size(64, 24)
    Me.opt_all_users.TabIndex = 1
    Me.opt_all_users.Text = "xAll"
    '
    'cmb_user
    '
    Me.cmb_user.AllowUnlistedValues = False
    Me.cmb_user.AutoCompleteMode = False
    Me.cmb_user.IsReadOnly = False
    Me.cmb_user.Location = New System.Drawing.Point(80, 18)
    Me.cmb_user.Name = "cmb_user"
    Me.cmb_user.SelectedIndex = -1
    Me.cmb_user.Size = New System.Drawing.Size(184, 24)
    Me.cmb_user.SufixText = "Sufix Text"
    Me.cmb_user.SufixTextVisible = True
    Me.cmb_user.TabIndex = 2
    Me.cmb_user.TextCombo = Nothing
    Me.cmb_user.TextVisible = False
    Me.cmb_user.TextWidth = 0
    '
    'gb_type
    '
    Me.gb_type.Controls.Add(Me.opt_all_types)
    Me.gb_type.Controls.Add(Me.opt_several_types)
    Me.gb_type.Controls.Add(Me.dg_filter)
    Me.gb_type.Location = New System.Drawing.Point(602, 8)
    Me.gb_type.Name = "gb_type"
    Me.gb_type.Size = New System.Drawing.Size(415, 193)
    Me.gb_type.TabIndex = 4
    Me.gb_type.TabStop = False
    Me.gb_type.Text = "xMov. Type"
    '
    'opt_all_types
    '
    Me.opt_all_types.Location = New System.Drawing.Point(8, 45)
    Me.opt_all_types.Name = "opt_all_types"
    Me.opt_all_types.Size = New System.Drawing.Size(64, 24)
    Me.opt_all_types.TabIndex = 1
    Me.opt_all_types.Text = "xAll"
    '
    'opt_several_types
    '
    Me.opt_several_types.Location = New System.Drawing.Point(8, 17)
    Me.opt_several_types.Name = "opt_several_types"
    Me.opt_several_types.Size = New System.Drawing.Size(72, 24)
    Me.opt_several_types.TabIndex = 0
    Me.opt_several_types.Text = "xSeveral"
    '
    'dg_filter
    '
    Me.dg_filter.CurrentCol = -1
    Me.dg_filter.CurrentRow = -1
    Me.dg_filter.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_filter.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_filter.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_filter.Location = New System.Drawing.Point(86, 16)
    Me.dg_filter.Name = "dg_filter"
    Me.dg_filter.PanelRightVisible = False
    Me.dg_filter.Redraw = True
    Me.dg_filter.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_filter.Size = New System.Drawing.Size(323, 148)
    Me.dg_filter.Sortable = False
    Me.dg_filter.SortAscending = True
    Me.dg_filter.SortByCol = 0
    Me.dg_filter.TabIndex = 2
    Me.dg_filter.ToolTipped = True
    Me.dg_filter.TopRow = -2
    Me.dg_filter.WordWrap = False
    '
    'tf_cm_cash_desk_session
    '
    Me.tf_cm_cash_desk_session.IsReadOnly = True
    Me.tf_cm_cash_desk_session.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_cm_cash_desk_session.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_cm_cash_desk_session.Location = New System.Drawing.Point(0, 236)
    Me.tf_cm_cash_desk_session.Name = "tf_cm_cash_desk_session"
    Me.tf_cm_cash_desk_session.Size = New System.Drawing.Size(534, 24)
    Me.tf_cm_cash_desk_session.SufixText = "Sufix Text"
    Me.tf_cm_cash_desk_session.SufixTextVisible = True
    Me.tf_cm_cash_desk_session.TabIndex = 124
    Me.tf_cm_cash_desk_session.TabStop = False
    Me.tf_cm_cash_desk_session.TextWidth = 60
    Me.tf_cm_cash_desk_session.Value = "xCash Desk Session"
    '
    'Uc_account_sel1
    '
    Me.Uc_account_sel1.Account = ""
    Me.Uc_account_sel1.AccountText = ""
    Me.Uc_account_sel1.Anon = False
    Me.Uc_account_sel1.AutoSize = True
    Me.Uc_account_sel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.Uc_account_sel1.BirthDate = New Date(CType(0, Long))
    Me.Uc_account_sel1.DisabledHolder = False
    Me.Uc_account_sel1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Uc_account_sel1.Holder = ""
    Me.Uc_account_sel1.Location = New System.Drawing.Point(286, 93)
    Me.Uc_account_sel1.MassiveSearchNumbers = ""
    Me.Uc_account_sel1.MassiveSearchNumbersToEdit = ""
    Me.Uc_account_sel1.MassiveSearchType = 0
    Me.Uc_account_sel1.Name = "Uc_account_sel1"
    Me.Uc_account_sel1.SearchTrackDataAsInternal = True
    Me.Uc_account_sel1.ShowMassiveSearch = False
    Me.Uc_account_sel1.ShowVipClients = False
    Me.Uc_account_sel1.Size = New System.Drawing.Size(307, 108)
    Me.Uc_account_sel1.TabIndex = 3
    Me.Uc_account_sel1.Telephone = ""
    Me.Uc_account_sel1.TrackData = ""
    Me.Uc_account_sel1.Vip = False
    Me.Uc_account_sel1.WeddingDate = New Date(CType(0, Long))
    '
    'chk_not_show_system_sessions
    '
    Me.chk_not_show_system_sessions.AutoSize = True
    Me.chk_not_show_system_sessions.Location = New System.Drawing.Point(14, 213)
    Me.chk_not_show_system_sessions.Name = "chk_not_show_system_sessions"
    Me.chk_not_show_system_sessions.Size = New System.Drawing.Size(176, 17)
    Me.chk_not_show_system_sessions.TabIndex = 5
    Me.chk_not_show_system_sessions.Text = "xNotShowSystemSessions"
    Me.chk_not_show_system_sessions.UseVisualStyleBackColor = True
    '
    'frm_cashier_movements
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(1213, 649)
    Me.Name = "frm_cashier_movements"
    Me.Text = "frm_cashier_movements"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gb_cashier.ResumeLayout(False)
    Me.gb_user.ResumeLayout(False)
    Me.gb_user.PerformLayout()
    Me.gb_type.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Constants "

  ' DB Columns
  Private Const SQL_COLUMN_SESSION_ID As Integer = 0
  Private Const SQL_COLUMN_SESSION_NAME As Integer = 1
  Private Const SQL_COLUMN_DATE As Integer = 2
  Private Const SQL_COLUMN_CASHIER_ID As Integer = 3
  Private Const SQL_COLUMN_CASHIER_NAME As Integer = 4
  Private Const SQL_COLUMN_USER_ID As Integer = 5
  Private Const SQL_COLUMN_USER_NAME As Integer = 6
  Private Const SQL_COLUMN_TYPE As Integer = 7
  Private Const SQL_COLUMN_ACCOUNT_ID As Integer = 8
  Private Const SQL_COLUMN_INIT_BALANCE As Integer = 9
  Private Const SQL_COLUMN_SUB_AMOUNT As Integer = 10
  Private Const SQL_COLUMN_ADD_AMOUNT As Integer = 11
  Private Const SQL_COLUMN_FINAL_BALANCE As Integer = 12
  Private Const SQL_COLUMN_DETAILS As Integer = 13
  Private Const SQL_COLUMN_CURRENCY_ISO_CODE As Integer = 14
  Private Const SQL_COLUMN_AUX_AMOUNT As Integer = 15
  Private Const SQL_COLUMN_UNDO_STATUS As Integer = 16
  Private Const SQL_COLUMN_DENOMINATION As Integer = 17
  Private Const SQL_COLUMN_ACCOUNT_TYPE As Integer = 18
  Private Const SQL_COLUMN_CURRENCY_TYPE As Integer = 19
  Private Const SQL_COLUMN_CHIP_ID As Integer = 20

  ' Grid Columns
  Private Const GRID_COLUMN_SESSION_ID As Integer = 0
  Private Const GRID_COLUMN_SESSION_NAME As Integer = 1
  Private Const GRID_COLUMN_DATE As Integer = 2
  Private Const GRID_COLUMN_CASHIER_ID As Integer = 3
  Private Const GRID_COLUMN_CASHIER_NAME As Integer = 4
  Private Const GRID_COLUMN_USER_ID As Integer = 5
  Private Const GRID_COLUMN_USER_NAME As Integer = 6
  Private Const GRID_COLUMN_TYPE_ID As Integer = 7
  Private Const GRID_COLUMN_TYPE_NAME As Integer = 8
  Private Const GRID_COLUMN_ACCOUNT_ID As Integer = 9
  Private Const GRID_COLUMN_ISO_CODE As Integer = 10
  Private Const GRID_COLUMN_DENOMINATION As Integer = 11
  Private Const GRID_COLUMN_CURRENCY_TYPE As Integer = 12
  Private Const GRID_COLUMN_UNITS As Integer = 13
  Private Const GRID_COLUMN_TOTAL As Integer = 14
  Private Const GRID_COLUMN_SUB_AMOUNT As Integer = 15
  Private Const GRID_COLUMN_ADD_AMOUNT As Integer = 16

  Private Const GRID_COLUMNS As Integer = 17
  Private Const GRID_HEADER_ROWS As Integer = 1

  ' Mov. types data grid
  Private Const GRID_2_COLUMN_CODE As Integer = 0
  Private Const GRID_2_COLUMN_CHECKED As Integer = 1
  Private Const GRID_2_COLUMN_DESC As Integer = 2
  Private Const GRID_2_COLUMN_ROW_TYPE As Integer = 3

  Private Const GRID_2_COLUMNS As Integer = 4
  Private Const GRID_2_HEADER_ROWS As Integer = 1

  ' Width
  Private Const GRID_WIDTH_DATE As Integer = 2000
  Private Const GRID_WIDTH_CASHIER As Integer = 1585
  Private Const GRID_WIDTH_TYPE_NAME As Integer = 2800 'EOR 22-MAR-2016 Expands With Column
  Private Const GRID_WIDTH_AMOUNT_SHORT As Integer = 1100
  Private Const GRID_WIDTH_AMOUNT_LONG As Integer = 1400
  Private Const GRID_WIDTH_USER As Integer = 1050
  Private Const GRID_WIDTH_SESSION_NAME As Integer = 2500
  Private Const GRID_WIDTH_ACCOUNT_ID As Integer = 1000
  Private Const GRID_WIDTH_ISO_CODE As Integer = 625
  Private Const GRID_WIDTH_DENOMINATION As Integer = 1620
  Private Const GRID_WIDTH_CURRENCY_TYPE As Integer = 1620
  Private Const GRID_WIDTH_UNITS As Integer = 1050
  Private Const GRID_WIDTH_TOTAL As Integer = 1635

  Private Const GRID_WIDTH_CURRENCY_EXCHANGE As Integer = 2150 '2450

  Private Const GRID_2_ROW_TYPE_HEADER = 0
  Private Const GRID_2_ROW_TYPE_DATA = 1

  ' Left padding for data rows in the mov. types data grid
  Private Const GRID_2_TAB As String = "     "

  Private Const FORM_DB_MIN_VERSION As Short = 160

#End Region ' Constants

#Region " Members "

  ' Used to avoid data-changed recursive calls when re-painting a data grid
  Private m_refreshing_grid As Boolean

  ' For report filters 
  Private m_cashiers As String
  Private m_users As String
  Private m_date_from As String
  Private m_date_to As String
  Private m_terminal_location As String

  'Private m_session As String
  Private m_types As String
  Private bl_sessions As Boolean = False
  Private m_session_id As Integer
  Private m_session_name As String
  ' LEM 19-JUL-2013: Initial dates movements filter as DATE
  Private m_init_date_from As Date
  Private m_init_date_to As Date

  Private m_totals_data_table As DataTable = Nothing

  Private m_split_a_name_str As String
  Private m_split_b_name_str As String

  Private m_prize_taxes_1_name As String
  Private m_prize_taxes_2_name As String
  Private m_prize_taxes_3_name As String

  Private m_cash_in_tax_name As String

  ' DHA 02-MAR-2016 TAx Provisions
  Private m_tax_provisions_name As String

  Private m_cash_in_tax_also_apply_to_split_b As Boolean
  Private m_split_b_tax_name As String

  Private m_system_user_id As Int32
  Private m_system_promobox_user_id As Int32

  ' DLL 31-JAN-2014: When chips
  Private m_only_show_chips_movements As Boolean

  Private m_national_currency_code As String

  Private m_is_tito_mode As Boolean

  ' OPC 11/11/2014
  Private m_account As String
  Private m_track_data As String
  Private m_holder As String

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CASHIER_MOVS

    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_InitControls()
    Dim _user_id As Int32
    Dim _user_name As String

    Call MyBase.GUI_InitControls()

    m_is_tito_mode = TITO.Utils.IsTitoMode()

    Me.Text = GLB_NLS_GUI_INVOICING.GetString(217)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(1)

    ' Date
    Me.gb_date.Text = GLB_NLS_GUI_INVOICING.GetString(201)
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Terminals 
    Me.gb_cashier.Text = GLB_NLS_GUI_INVOICING.GetString(209)
    Me.opt_one_cashier.Text = GLB_NLS_GUI_INVOICING.GetString(218)
    Me.opt_all_cashiers.Text = GLB_NLS_GUI_INVOICING.GetString(219)

    ' Users 
    Me.gb_user.Text = GLB_NLS_GUI_INVOICING.GetString(220)
    Me.opt_one_user.Text = GLB_NLS_GUI_INVOICING.GetString(218)
    Me.opt_all_users.Text = GLB_NLS_GUI_INVOICING.GetString(219)
    Me.chk_show_all.Text = GLB_NLS_GUI_CONFIGURATION.GetString(90)

    ' Mov. Types grid
    Me.gb_type.Text = GLB_NLS_GUI_INVOICING.GetString(211)
    Me.opt_several_types.Text = GLB_NLS_GUI_INVOICING.GetString(223)
    Me.opt_all_types.Text = GLB_NLS_GUI_INVOICING.GetString(224)

    m_national_currency_code = WSI.Common.CurrencyExchange.GetNationalCurrency()

    ' Grid
    Call GUI_StyleSheet()

    ' Mov. types grid
    Call GUI_StyleSheetMovementTypes()

    m_split_b_tax_name = GeneralParam.GetString("Cashier", "Split.B.Tax.Name")

    m_split_a_name_str = GetCashierData("Split.A.Name")
    m_split_b_name_str = GetCashierData("Split.B.Name")
    If String.IsNullOrEmpty(m_split_b_name_str) Then
      m_split_b_name_str = "[" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(361) & " B: " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(363) & "]"
    End If

    m_prize_taxes_1_name = GetCashierData("Tax.OnPrize.1.Name")
    m_prize_taxes_2_name = GetCashierData("Tax.OnPrize.2.Name")
    m_prize_taxes_3_name = GetCashierData("Tax.OnPrize.3.Name")

    m_cash_in_tax_name = GeneralParam.GetString("Cashier", "CashInTax.Name", GLB_NLS_GUI_PLAYER_TRACKING.GetString(4562))
    m_cash_in_tax_also_apply_to_split_b = GeneralParam.GetBoolean("Cashier", "CashInTax.AlsoApplyToSplitB")

    ' DHA 02-MAR-2016 TAx Provisions
    m_tax_provisions_name = GeneralParam.GetString("Cashier.TaxProvisions", "Voucher.Title", GLB_NLS_GUI_PLAYER_TRACKING.GetString(7151))

    ' Set filter default values
    Call SetDefaultValues()

    ' Set combo with Cashier Terminals
    Call SetComboCashierAlias(Me.cmb_cashier)

    Me.cmb_cashier.Enabled = False

    ' XCD 16-Aug-2012 Set combo with Users
    Call SetCombo(Me.cmb_user, "   SELECT GU_USER_ID, GU_USERNAME" _
                             & "     FROM GUI_USERS" _
                             & "    WHERE GU_BLOCK_REASON = " & WSI.Common.GUI_USER_BLOCK_REASON.NONE _
                             & "      AND GU_USER_TYPE    = " & WSI.Common.GU_USER_TYPE.USER _
                             & " ORDER BY GU_USERNAME")

    Me.cmb_user.Enabled = False
    Me.chk_show_all.Enabled = False

    ' TITO Mode
    If WSI.Common.TITO.Utils.IsTitoMode() Then
      Uc_account_sel1.InitExtendedQuery(True)
    End If

    ' Session Name
    Me.tf_cm_cash_desk_session.Text = GLB_NLS_GUI_INVOICING.GetString(208)
    If m_only_show_chips_movements Then
      Me.tf_cm_cash_desk_session.Text = ""
      Me.tf_cm_cash_desk_session.Value = ""
      Me.tf_cm_cash_desk_session.Visible = False
    End If

    ' Check show system movements
    Me.chk_not_show_system_sessions.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5714)

    _user_id = 0
    _user_name = ""
    m_system_user_id = -1
    m_system_promobox_user_id = -1
    Using _db_trx As DB_TRX = New DB_TRX()
      If Cashier.GetSystemUser(GU_USER_TYPE.SYS_ACCEPTOR, _db_trx.SqlTransaction, _user_id, _user_name) Then
        m_system_user_id = _user_id
      End If
      If Cashier.GetSystemUser(GU_USER_TYPE.SYS_PROMOBOX, _db_trx.SqlTransaction, _user_id, _user_name) Then
        m_system_promobox_user_id = _user_id
      End If
    End Using
  End Sub 'GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub 'GUI_FilterReset

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()

    Call InitTotalsDataTable()

  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()

    Dim _all_rows() As DataRow
    Dim _row As DataRow
    Dim _idx_row As Integer
    Dim _first_time As Boolean
    Dim _currency_code As String
    Dim _str_type_name As String
    Dim _str_sub_value As String
    Dim _str_add_value As String
    Dim _sub_amount As Decimal
    Dim _add_amount As Decimal

    ' Currency ISO code
    If Me.Grid Is Nothing OrElse Me.Grid.NumRows = 0 Then
      Exit Sub
    End If

    _first_time = True
    _all_rows = m_totals_data_table.Select("", "MOV_TYPE_NAME")

    ' Show total records
    For Each _row In _all_rows
      Me.Grid.AddRow()
      _idx_row = Me.Grid.NumRows - 1

      If _first_time Then
        ' Label - TOTAL:
        Me.Grid.Cell(_idx_row, GRID_COLUMN_SESSION_NAME).Value = GLB_NLS_GUI_INVOICING.GetString(205)
        _first_time = False
      End If

      _currency_code = String.Empty
      _str_sub_value = String.Empty
      _str_add_value = String.Empty
      _sub_amount = 0
      _add_amount = 0

      If Not _row.IsNull("ISO_CODE") Then
        _currency_code = _row.Item("ISO_CODE")
      End If

      If Not _row.IsNull("TOTAL_SUB_AMOUNT") Then
        _sub_amount = _row.Item("TOTAL_SUB_AMOUNT")
      End If

      If Not _row.IsNull("TOTAL_ADD_AMOUNT") Then
        _add_amount = _row.Item("TOTAL_ADD_AMOUNT")
      End If

      _str_type_name = _row.Item("MOV_TYPE_NAME")
      If _row.Item("MOV_TYPE") = WSI.Common.CASHIER_MOVEMENT.MB_CASH_IN Then
        If m_is_tito_mode Then
          _str_type_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4586)
        Else
          _str_type_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(730)
        End If
      End If

      'Iso_Code
      'XMS 22/11/2013 Modify set with GetCurrencyCodeToGrid, if the movement code hasn' t any ISO_CODE assign = "".
      If String.IsNullOrEmpty(_currency_code) OrElse (_currency_code = m_national_currency_code) Then
        Me.Grid.Cell(_idx_row, GRID_COLUMN_ISO_CODE).Value = GetCurrencyCodeToGrid(_row.Item("MOV_TYPE"), m_national_currency_code, _add_amount)
      Else
        Me.Grid.Cell(_idx_row, GRID_COLUMN_ISO_CODE).Value = GetCurrencyCodeToGrid(_row.Item("MOV_TYPE"), _currency_code, _add_amount)
      End If

      ' Subtract amount
      Select Case _row.Item("MOV_TYPE")
        Case CASHIER_MOVEMENT.REQUEST_TO_VOUCHER_REPRINT
          _str_sub_value = ""

        Case CASHIER_MOVEMENT.PROMOTION_POINT, CASHIER_MOVEMENT.CANCEL_PROMOTION_POINT
          _str_sub_value = GUI_FormatNumber(_sub_amount)

        Case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT1, _
             CASHIER_MOVEMENT.COMPANY_B_CURRENCY_EXCHANGE_SPLIT1
          _str_sub_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509) _
                             & ": " _
                             & Currency.Format(Math.Round(_sub_amount, 2), CurrencyExchange.GetNationalCurrency())
          ' 15-DEC-2014  JBC, DLL, JPJ    New Company B commission movements 
        Case CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1, _
             CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1_UNDO, _
             CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT1, _
             CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT1, _
             CASHIER_MOVEMENT.CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1, _
             CASHIER_MOVEMENT.CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1, _
             CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT1, _
             CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1, _
             CASHIER_MOVEMENT.COMPANY_B_CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1
          _str_sub_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509) _
                             & ": " _
                             & GUI_FormatCurrency(_sub_amount, 2)

        Case CASHIER_MOVEMENT.WIN_LOSS_STATEMENT_REQUEST
          _str_sub_value = ""

          ' DHA 27-JAN-2016: dual currency
        Case CASHIER_MOVEMENT.TITO_TICKETS_PLAYED_EXCHANGE_DUAL_CURRENCY
          _str_sub_value = Currency.Format(Math.Round(_sub_amount, 2), CurrencyExchange.GetNationalCurrency() _
                             & " = " _
                             & Currency.Format(Math.Round(_row.Item("TOTAL_CURRENCY"), 2), _currency_code))

        Case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_NATIONAL_CURRENCY

          Me.Grid.Cell(_idx_row, GRID_COLUMN_SUB_AMOUNT).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509) & ": " & GUI_FormatCurrency(_row.Item("TOTAL_SUB_AMOUNT"), 2)

        Case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_FOREIGN_CURRENCY

          Me.Grid.Cell(_idx_row, GRID_COLUMN_SUB_AMOUNT).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509) & ": " & GUI_FormatCurrency(_row.Item("TOTAL_SUB_AMOUNT"), 2)

        Case WSI.Common.MovementType.MULTIPLE_BUCKETS_EndSession To WSI.Common.MovementType.MULTIPLE_BUCKETS_Manual_Set_Last ' [1100-1599]

          If GetBucketFormatedAmount(_row.Item("MOV_TYPE")) = Buckets.BucketUnits.Points Then
            _str_sub_value = GUI_FormatNumber(_sub_amount)
          Else
            _str_sub_value = GUI_FormatCurrency(_sub_amount)
          End If

        Case Else
          If String.IsNullOrEmpty(_currency_code) OrElse (_currency_code = m_national_currency_code) Then
            _str_sub_value = GUI_FormatCurrency(_sub_amount, 2)
          Else
            _str_sub_value = GUI_FormatNumber(_sub_amount, 2)
          End If

      End Select

      ' Add amount
      Select Case _row.Item("MOV_TYPE")
        Case CASHIER_MOVEMENT.PROMOTION_POINT, CASHIER_MOVEMENT.CANCEL_PROMOTION_POINT
          _str_add_value = GUI_FormatNumber(_add_amount)

        Case CASHIER_MOVEMENT.REQUEST_TO_VOUCHER_REPRINT
          _str_add_value = GUI_FormatNumber(_add_amount, 0)

        Case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT1, _
             CASHIER_MOVEMENT.COMPANY_B_CURRENCY_EXCHANGE_SPLIT1
          _str_add_value = Currency.Format(Math.Round(_row.Item("TOTAL_CURRENCY"), 2), _currency_code) _
                             & " = " _
                             & Currency.Format(Math.Round(_add_amount, 2), CurrencyExchange.GetNationalCurrency())

          ' DHA 27-JAN-2016: dual currency
        Case CASHIER_MOVEMENT.TITO_TICKETS_CREATE_EXCHANGE_DUAL_CURRENCY, _
             CASHIER_MOVEMENT.BILL_IN_EXCHANGE_DUAL_CURRENCY
          _str_add_value = Currency.Format(Math.Round(_row.Item("TOTAL_CURRENCY"), 2), _currency_code) _
                             & " = " _
                             & Currency.Format(Math.Round(_add_amount, 2), CurrencyExchange.GetNationalCurrency())

        Case CASHIER_MOVEMENT.WIN_LOSS_STATEMENT_REQUEST
          _str_add_value = ""

        Case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_NATIONAL_CURRENCY

          Me.Grid.Cell(_idx_row, GRID_COLUMN_ADD_AMOUNT).Value = Currency.Format(Math.Round(_row.Item("TOTAL_CURRENCY"), 2), _row.Item("ISO_CODE")) _
                                                                   & " = " _
                                                                   & Currency.Format(Math.Round(_row.Item("TOTAL_ADD_AMOUNT"), 2), CurrencyExchange.GetNationalCurrency())

        Case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_FOREIGN_CURRENCY

          Me.Grid.Cell(_idx_row, GRID_COLUMN_ADD_AMOUNT).Value = Currency.Format(Math.Round(_row.Item("TOTAL_ADD_AMOUNT"), 2), CurrencyExchange.GetNationalCurrency()) _
                                                                   & " = " _
                                                                   & Currency.Format(Math.Round(_row.Item("TOTAL_CURRENCY"), 2), _row.Item("ISO_CODE"))


        Case WSI.Common.MovementType.MULTIPLE_BUCKETS_EndSession To WSI.Common.MovementType.MULTIPLE_BUCKETS_Manual_Set_Last ' [1100-1599]

          If GetBucketFormatedAmount(_row.Item("MOV_TYPE")) = Buckets.BucketUnits.Points Then
            _str_add_value = GUI_FormatNumber(_add_amount)
          Else
            _str_add_value = GUI_FormatCurrency(_add_amount)
          End If

        Case Else
          If String.IsNullOrEmpty(_currency_code) OrElse (_currency_code = m_national_currency_code) Then
            _str_add_value = GUI_FormatCurrency(_add_amount, 2, , , _currency_code)
          Else
            _str_add_value = GUI_FormatNumber(_add_amount, 2)
          End If

      End Select

      'If Color CHIPS then it is an integer value, without decimals
      If (_currency_code = WSI.Common.Cage.CHIPS_COLOR) Then
        If (Not String.IsNullOrEmpty(_str_sub_value)) Then
          _str_sub_value = GUI_FormatNumber(_sub_amount, 0)
        End If

        If (Not String.IsNullOrEmpty(_str_add_value)) Then
          _str_add_value = GUI_FormatNumber(_add_amount, 0)
        End If
      End If

      'Cell values
      Me.Grid.Cell(_idx_row, GRID_COLUMN_TYPE_NAME).Value = _str_type_name
      Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_ID).Value = GUI_FormatNumber(_row.Item("COUNT"), 0)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_SUB_AMOUNT).Value = _str_sub_value
      Me.Grid.Cell(_idx_row, GRID_COLUMN_ADD_AMOUNT).Value = _str_add_value
      'BackColor 
      Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

    Next

  End Sub ' GUI_AfterLastRow

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Date selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    'Check Types
    Call CheckMovTypeFilter()

    'Account Filter
    If Not Uc_account_sel1.ValidateFormat() Then
      Return False
    End If

    Return True
  End Function 'GUI_FilterCheck

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database

  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim str_sql As String

    ' Get Select and from
    str_sql = "SELECT " & _
                  "CM_SESSION_ID," & _
                  "CS_NAME," & _
                  "CM_DATE," & _
                  "CM_CASHIER_ID," & _
                  "CM_CASHIER_NAME," & _
                  "CM_USER_ID," & _
                  "CM_USER_NAME," & _
                  "CM_TYPE," & _
                  "CM_ACCOUNT_ID," & _
                  "CM_INITIAL_BALANCE," & _
                  "CM_SUB_AMOUNT," & _
                  "CM_ADD_AMOUNT," & _
                  "CM_FINAL_BALANCE, " & _
                  "CM_DETAILS, " & _
                  "CM_CURRENCY_ISO_CODE, " & _
                  "CM_AUX_AMOUNT, " & _
                  "CM_UNDO_STATUS, " & _
                  "ISNULL(CM_CURRENCY_DENOMINATION, 0), " & _
                  "AC_TYPE, " & _
                  "CM_CAGE_CURRENCY_TYPE, " & _
                  "CM_CHIP_ID " & _
              " FROM CASHIER_MOVEMENTS "
    str_sql &= "INNER JOIN CASHIER_SESSIONS  ON CM_SESSION_ID = CS_SESSION_ID "

    ' Filter User
    If Me.opt_one_user.Checked Then
      str_sql &= "INNER JOIN GUI_USERS GU ON CM_USER_ID = GU_USER_ID "
      str_sql &= "INNER JOIN CASHIER_TERMINALS ON CM_CASHIER_ID = CT_CASHIER_ID "
    End If

    str_sql &= " LEFT JOIN ACCOUNTS         ON CM_ACCOUNT_ID = AC_ACCOUNT_ID "

    'FBA 27-AUG-2013  Do not show Exchange movements for company 'B'
    str_sql = str_sql & " WHERE CM_TYPE NOT IN (" & WSI.Common.CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT2 & ", " _
                                                  & WSI.Common.CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT2 & ", " _
                                                  & WSI.Common.CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT2_UNDO & ", " _
                                                  & WSI.Common.CASHIER_MOVEMENT.CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT2 & ", " _
                                                  & WSI.Common.CASHIER_MOVEMENT.CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT2 & ", " _
                                                  & WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT2 & ", " _
                                                  & WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT2 & ", " _
                                                  & WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT2 & ", " _
                                                  & WSI.Common.CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT2 & ", " _
                                                  & WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CURRENCY_EXCHANGE_SPLIT2 & ", " _
                                                  & WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT2 & ", " _
                                                  & WSI.Common.CASHIER_MOVEMENT.CASINO_CHIPS_EXCHANGE_SPLIT2 & ", " _
                                                  & WSI.Common.CASHIER_MOVEMENT.CAGE_CASHIER_COLLECT_DROPBOX_TERMINAL & ", " _
                                                  & WSI.Common.CASHIER_MOVEMENT.CAGE_CASHIER_REFILL_HOPPER_TERMINAL & ")"

    ' MBF 02-DEC-2009 - "Limit extension movement" should not be shown
    ' JCM 08-AUG-2012 - "Limit extension movement MANUALS " should be shown
    'str_sql = str_sql & " AND CM_TYPE <> " & MOV_TYPE_DB_CARD_MB_MANUAL_CHANGE_LIMIT & " "

    str_sql = str_sql & " AND (CM_TYPE <= " & WSI.Common.CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_IN & _
                        "  OR  CM_TYPE = " & WSI.Common.CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_OUT & " ) "

    str_sql = str_sql & GetSqlWhere()
    str_sql = str_sql & " ORDER BY CM_MOVEMENT_ID DESC, CM_DATE DESC "

    Return str_sql

  End Function 'GUI_FilterGetSqlQuery

  ' PURPOSE : return "" to the movements not used cash.
  '
  '  PARAMS :
  '     - INPUT :
  '           - MovType, CurrentCode
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '           - CurrentCode
  '
  Private Function GetCurrencyCodeToGrid(ByVal MovType As Integer, ByVal CurrencyCode As String, ByVal AddAmount As Decimal) As String

    Select Case MovType
      Case WSI.Common.CASHIER_MOVEMENT.CARD_CREATION _
         , WSI.Common.CASHIER_MOVEMENT.CARD_PERSONALIZATION _
         , WSI.Common.CASHIER_MOVEMENT.ACCOUNT_PIN_CHANGE _
         , WSI.Common.CASHIER_MOVEMENT.ACCOUNT_PIN_RANDOM _
         , WSI.Common.CASHIER_MOVEMENT.CARD_RECYCLED _
         , WSI.Common.CASHIER_MOVEMENT.ACCOUNT_BLOCKED _
         , WSI.Common.CASHIER_MOVEMENT.ACCOUNT_UNBLOCKED _
         , WSI.Common.CASHIER_MOVEMENT.CASH_PENDING_CLOSING _
         , WSI.Common.CASHIER_MOVEMENT.DRAW_TICKET_PRINT _
         , WSI.Common.CASHIER_MOVEMENT.WIN_LOSS_STATEMENT_REQUEST
        CurrencyCode = ""

      Case WSI.Common.CASHIER_MOVEMENT.PARTICIPATION_IN_CASHDESK_DRAW
        If (AddAmount = 0) Then
          CurrencyCode = ""
        End If
    End Select

    'Don't show X02 -> Show 'Color'
    If (CurrencyCode = WSI.Common.Cage.CHIPS_COLOR) Then
      CurrencyCode = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7286)
    End If

    Return CurrencyCode
  End Function

  ''' <summary>
  ''' Returns the ChipSet description and the Currency Type suffix to concatenate to the movement description
  ''' </summary>
  ''' <param name="ChipSetId"></param>
  ''' <param name="CurrencyTypeText"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetChipsetAndCurrencyTypeSuffix(ByVal ChipSetId As Integer, ByVal CurrencyTypeText As String) As String
    Dim _str_type_name As String
    Dim _chipset_desc As String

    _str_type_name = String.Empty
    _chipset_desc = String.Empty

    'Search description from ChipSet 
    If (ChipSetId <> 0) Then
      _chipset_desc = GetChipSetDescription(ChipSetId)
    End If

    'Append ChipSet description
    If Not String.IsNullOrEmpty(_chipset_desc) Then
      _str_type_name &= " - (" & _chipset_desc & ")"
    End If

    'Append Currency Type
    If Not String.IsNullOrEmpty(CurrencyTypeText) Then
      _str_type_name &= " - " & CurrencyTypeText
    End If

    Return _str_type_name
  End Function

  ' PURPOSE : Translate the provided movement type to its description string
  '
  '  PARAMS :
  '     - INPUT :
  '           - MovType
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '           - Movement type description
  '
  Private Function TranslateMovementType(ByVal MovType As Integer, Optional ByVal CurrencyCode As String = "", Optional ByVal Denomination As Decimal = 0, Optional ByVal UserId As Int32 = 0, Optional ByVal CurrencyExchangeType As CurrencyExchangeType = 0) As String
    Dim _str_type As String
    Dim _str_chips_type As String
    Dim _str_currency As String
    Dim _str_denomination As String
    Dim _buckets_forms As BucketsForm
    Dim _name As String
    Dim _str_chip_type As String

    _str_type = ""
    _str_chips_type = ""
    _str_currency = ""
    _str_denomination = ""
    _str_chip_type = ""

    _buckets_forms = New BucketsForm()
    _name = ""

    ' Movement type names
    Select Case MovType
      Case WSI.Common.CASHIER_MOVEMENT.OPEN_SESSION
        If String.IsNullOrEmpty(CurrencyCode) Then
          _str_type = GLB_NLS_GUI_INVOICING.GetString(26)
        Else
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2498) & _str_currency
        End If
        _str_type &= " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1340)

      Case WSI.Common.CASHIER_MOVEMENT.CLOSE_SESSION
        If String.IsNullOrEmpty(CurrencyCode) Then
          ' LTC 01-JUN-2018
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2499)
        Else
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2499) & _str_currency
        End If
        _str_type &= " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1340)

        ' FGB 21-JAN-2017: Session reopen
      Case CASHIER_MOVEMENT.REOPEN_CASHIER
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7881)

        ' DHA 10-OCT-2014: added new movement (158) if there is cash short on closing cashier session
      Case WSI.Common.CASHIER_MOVEMENT.CASH_CLOSING_SHORT_DEPRECATED
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6626)

        ' DHA 10-OCT-2014: added new movement (159) if there is cash over on closing cashier session
      Case WSI.Common.CASHIER_MOVEMENT.CASH_CLOSING_OVER_DEPRECATED
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6627)

        ' JML 03-AUG-2015: ADD new movements for cash short/over on closing cashier session
      Case WSI.Common.CASHIER_MOVEMENT.CASH_CLOSING_SHORT
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2499) & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5669)
      Case WSI.Common.CASHIER_MOVEMENT.CASH_CLOSING_OVER
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2499) & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5670)

      Case WSI.Common.CASHIER_MOVEMENT.FILLER_IN
        If String.IsNullOrEmpty(CurrencyCode) Then
          _str_type = GLB_NLS_GUI_INVOICING.GetString(28) & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1340)
        Else
          _str_type = GLB_NLS_GUI_INVOICING.GetString(28) & " - " & _str_currency & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1340)
        End If

      Case WSI.Common.CASHIER_MOVEMENT.FILLER_OUT
        If Denomination = -CurrencyExchangeType.CARD Then
          If String.IsNullOrEmpty(CurrencyCode) Then
            _str_type = GLB_NLS_GUI_INVOICING.GetString(29) & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7446)
          Else
            _str_type = GLB_NLS_GUI_INVOICING.GetString(29) & " - " & _str_currency & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7446)
          End If
        Else
          If String.IsNullOrEmpty(CurrencyCode) Then
            _str_type = GLB_NLS_GUI_INVOICING.GetString(29) & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1340)
          Else
            _str_type = GLB_NLS_GUI_INVOICING.GetString(29) & " - " & _str_currency & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1340)
          End If
        End If
      Case WSI.Common.CASHIER_MOVEMENT.FILLER_IN_CLOSING_STOCK
        If String.IsNullOrEmpty(CurrencyCode) Then
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7369) & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1340)
        Else
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7369) & " - " & _str_currency & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1340)
        End If

      Case WSI.Common.CASHIER_MOVEMENT.FILLER_OUT_CLOSING_STOCK
        If String.IsNullOrEmpty(CurrencyCode) Then
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7370) & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1340)
        Else
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7370) & " - " & _str_currency & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1340)
        End If

      Case WSI.Common.CASHIER_MOVEMENT.CASH_IN
        _str_type = IIf(WSI.Common.Misc.IsVoucherModeTV(), WSI.Common.Resource.String("STR_FRM_LAST_MOVEMENTS_REQUEST"), GLB_NLS_GUI_INVOICING.GetString(30))

      Case WSI.Common.CASHIER_MOVEMENT.CASH_OUT
        _str_type = GLB_NLS_GUI_INVOICING.GetString(489)

      Case WSI.Common.CASHIER_MOVEMENT.TAX_ON_PRIZE1
        _str_type = m_prize_taxes_1_name

      Case WSI.Common.CASHIER_MOVEMENT.TAX_ON_PRIZE2
        _str_type = m_prize_taxes_2_name

      Case WSI.Common.CASHIER_MOVEMENT.TAX_ON_PRIZE3
        _str_type = m_prize_taxes_3_name

      Case WSI.Common.CASHIER_MOVEMENT.PRIZES
        _str_type = IIf(WSI.Common.Misc.IsVoucherModeTV(), WSI.Common.Resource.String("STR_PRIZES_GROSS"), WSI.Common.Resource.String("STR_UC_BANK_PRIZES"))

      Case WSI.Common.CASHIER_MOVEMENT.PRIZE_DRAW_GROSS_OTHER

        If WSI.Common.Misc.IsVoucherModeTV Then
          _str_type = WSI.Common.Resource.String("STR_RAFFLE_PRICE_PAYMENT")
        Else
          _str_type = WSI.Common.Resource.String("STR_PRIZES_GROSS_OTHERS")
        End If

      Case WSI.Common.CASHIER_MOVEMENT.PROMOTION_NOT_REDEEMABLE
        _str_type = GLB_NLS_GUI_INVOICING.GetString(50)

      Case WSI.Common.CASHIER_MOVEMENT.PROMOTION_REDEEMABLE
        _str_type = GLB_NLS_GUI_INVOICING.GetString(100)

      Case WSI.Common.CASHIER_MOVEMENT.CASH_IN_COVER_COUPON
        _str_type = GLB_NLS_GUI_INVOICING.GetString(38)

      Case WSI.Common.CASHIER_MOVEMENT.CANCEL_NOT_REDEEMABLE
        _str_type = GLB_NLS_GUI_INVOICING.GetString(53)

        ' DCS 15-JUL-2015: add movements for pay player card
      Case WSI.Common.CASHIER_MOVEMENT.CARD_DEPOSIT_IN, _
           WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN
        _str_type = GLB_NLS_GUI_INVOICING.GetString(51)

      Case WSI.Common.CASHIER_MOVEMENT.CARD_DEPOSIT_OUT, _
           WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_OUT
        _str_type = GLB_NLS_GUI_INVOICING.GetString(52)

      Case WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT, _
           WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT
        _str_type = GLB_NLS_GUI_INVOICING.GetString(70)

      Case WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_FOR_FREE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5079)

      Case WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_IN_POINTS
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5080)

      Case WSI.Common.CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CHECK, _
           WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CHECK
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6513)

      Case WSI.Common.CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CURRENCY_EXCHANGE, _
           WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CURRENCY_EXCHANGE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6514)

      Case WSI.Common.CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_CREDIT, _
           WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_CREDIT
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6515)

      Case WSI.Common.CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_DEBIT, _
           WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_DEBIT
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6516)

      Case WSI.Common.CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_GENERIC, _
           WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_GENERIC, _
           WSI.Common.CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_UNDO, _
           WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_UNDO
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6517)

      Case WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CHECK, _
           WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CHECK
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6508)

      Case WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_CREDIT, _
           WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_CREDIT
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6510)

      Case WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_DEBIT, _
           WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_DEBIT
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6511)

      Case WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_GENERIC, _
           WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_GENERIC
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6512)

      Case WSI.Common.CASHIER_MOVEMENT.DRAW_TICKET_PRINT
        _str_type = GLB_NLS_GUI_INVOICING.GetString(71)

      Case WSI.Common.CASHIER_MOVEMENT.POINTS_TO_DRAW_TICKET_PRINT
        _str_type = GLB_NLS_GUI_INVOICING.GetString(83)

      Case WSI.Common.CASHIER_MOVEMENT.PRIZE_COUPON
        _str_type = GLB_NLS_GUI_CONFIGURATION.GetString(337)

      Case WSI.Common.CASHIER_MOVEMENT.HANDPAY
        _str_type = GLB_NLS_GUI_CONFIGURATION.GetString(364)

      Case WSI.Common.CASHIER_MOVEMENT.HANDPAY_CANCELLATION
        _str_type = GLB_NLS_GUI_CONFIGURATION.GetString(365)

      Case WSI.Common.CASHIER_MOVEMENT.MANUAL_HANDPAY
        _str_type = GLB_NLS_GUI_CONFIGURATION.GetString(366)

      Case WSI.Common.CASHIER_MOVEMENT.MANUAL_HANDPAY_CANCELLATION
        _str_type = GLB_NLS_GUI_CONFIGURATION.GetString(367)

      Case WSI.Common.CASHIER_MOVEMENT.CASH_IN_SPLIT1
        _str_type = m_split_a_name_str

      Case WSI.Common.CASHIER_MOVEMENT.CASH_IN_SPLIT2
        _str_type = m_split_b_name_str

      Case WSI.Common.CASHIER_MOVEMENT.DEV_SPLIT1
        _str_type = GLB_NLS_GUI_INVOICING.GetString(31) & " - " & m_split_a_name_str

      Case WSI.Common.CASHIER_MOVEMENT.DEV_SPLIT2
        _str_type = GLB_NLS_GUI_INVOICING.GetString(31) & " - " & m_split_b_name_str

      Case WSI.Common.CASHIER_MOVEMENT.CANCEL_SPLIT1
        _str_type = GLB_NLS_GUI_INVOICING.GetString(80) & " - " & m_split_a_name_str

      Case WSI.Common.CASHIER_MOVEMENT.CANCEL_SPLIT2
        _str_type = GLB_NLS_GUI_INVOICING.GetString(80) & " - " & m_split_b_name_str

      Case WSI.Common.CASHIER_MOVEMENT.HANDPAY_VOIDED
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1834) & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5592)

      Case WSI.Common.CASHIER_MOVEMENT.HANDPAY_AUTHORIZED
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1834) & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5593)

      Case WSI.Common.CASHIER_MOVEMENT.HANDPAY_UNAUTHORIZED
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1834) & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5594)

      Case WSI.Common.CASHIER_MOVEMENT.HANDPAY_VOIDED_UNDONE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1834) & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5600)

        ' Mobile Bank 
      Case WSI.Common.CASHIER_MOVEMENT.MB_MANUAL_CHANGE_LIMIT
        _str_type = GLB_NLS_GUI_INVOICING.GetString(54)

      Case WSI.Common.CASHIER_MOVEMENT.MB_CASH_IN
        If m_is_tito_mode Then
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4586)
        Else
          If UserId = m_system_user_id OrElse UserId = m_system_promobox_user_id Then
            _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(577)
          Else
            _str_type = GLB_NLS_GUI_INVOICING.GetString(82)
          End If
        End If

      Case WSI.Common.CASHIER_MOVEMENT.MB_CASH_IN_SPLIT1
        _str_type = GLB_NLS_GUI_INVOICING.GetString(55) & " - " & m_split_a_name_str

      Case WSI.Common.CASHIER_MOVEMENT.MB_CASH_IN_SPLIT2
        _str_type = GLB_NLS_GUI_INVOICING.GetString(55) & " - " & m_split_b_name_str

      Case WSI.Common.CASHIER_MOVEMENT.PROMO_CREDITS
        _str_type = GLB_NLS_GUI_INVOICING.GetString(64)

      Case WSI.Common.CASHIER_MOVEMENT.PROMO_TO_REDEEMABLE
        _str_type = GLB_NLS_GUI_INVOICING.GetString(65)

      Case WSI.Common.CASHIER_MOVEMENT.PROMO_EXPIRED
        _str_type = GLB_NLS_GUI_INVOICING.GetString(66)

      Case WSI.Common.CASHIER_MOVEMENT.PROMO_CANCEL
        _str_type = GLB_NLS_GUI_INVOICING.GetString(67)

      Case WSI.Common.CASHIER_MOVEMENT.PROMO_START_SESSION
        _str_type = GLB_NLS_GUI_INVOICING.GetString(68)

      Case WSI.Common.CASHIER_MOVEMENT.PROMO_END_SESSION
        _str_type = GLB_NLS_GUI_INVOICING.GetString(69)

      Case WSI.Common.CASHIER_MOVEMENT.CARD_CREATION
        _str_type = GLB_NLS_GUI_INVOICING.GetString(90)

      Case WSI.Common.CASHIER_MOVEMENT.CARD_PERSONALIZATION
        _str_type = GLB_NLS_GUI_INVOICING.GetString(91)

      Case WSI.Common.CASHIER_MOVEMENT.ACCOUNT_PIN_CHANGE
        _str_type = GLB_NLS_GUI_INVOICING.GetString(93)

      Case WSI.Common.CASHIER_MOVEMENT.ACCOUNT_PIN_RANDOM
        _str_type = GLB_NLS_GUI_INVOICING.GetString(94)

      Case WSI.Common.CASHIER_MOVEMENT.MB_CASH_IN_NOT_REDEEMABLE
        _str_type = GLB_NLS_GUI_INVOICING.GetString(55) & " - " & GLB_NLS_GUI_INVOICING.GetString(50)

      Case WSI.Common.CASHIER_MOVEMENT.MB_CASH_IN_COVER_COUPON
        _str_type = GLB_NLS_GUI_INVOICING.GetString(55) & " - " & GLB_NLS_GUI_INVOICING.GetString(38)

      Case WSI.Common.CASHIER_MOVEMENT.MB_PRIZE_COUPON
        _str_type = GLB_NLS_GUI_INVOICING.GetString(55) & " - " & GLB_NLS_GUI_CONFIGURATION.GetString(337)
        'JCM 24-APR-2012

      Case WSI.Common.CASHIER_MOVEMENT.CARD_RECYCLED
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(654)

        'SSC 08-MAR-2012
      Case WSI.Common.CASHIER_MOVEMENT.NA_CASH_IN_SPLIT1
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6614) & " - " & m_split_a_name_str

      Case WSI.Common.CASHIER_MOVEMENT.NA_CASH_IN_SPLIT2
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6614) & " - " & m_split_b_name_str

      Case WSI.Common.CASHIER_MOVEMENT.NA_PRIZE_COUPON
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(576) & " - " & GLB_NLS_GUI_CONFIGURATION.GetString(337)

      Case WSI.Common.CASHIER_MOVEMENT.NA_CASH_IN_NOT_REDEEMABLE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(576) & " - " & GLB_NLS_GUI_INVOICING.GetString(50)

      Case WSI.Common.CASHIER_MOVEMENT.NA_CASH_IN_COVER_COUPON
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(576) & " - " & GLB_NLS_GUI_INVOICING.GetString(38)

        'SSC 13-JUN-2012: Gift Request
      Case WSI.Common.CASHIER_MOVEMENT.POINTS_TO_NOT_REDEEMABLE
        _str_type = GLB_NLS_GUI_INVOICING.GetString(78)

      Case WSI.Common.CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE
        _str_type = GLB_NLS_GUI_INVOICING.GetString(88)

      Case WSI.Common.CASHIER_MOVEMENT.CANCEL_PROMOTION_REDEEMABLE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1120)

      Case WSI.Common.CASHIER_MOVEMENT.REDEEMABLE_DEV_EXPIRED
        _str_type = GLB_NLS_GUI_INVOICING.GetString(477)

        'Reception nls strings'
      Case WSI.Common.CASHIER_MOVEMENT.CUSTOMER_ENTRANCE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7267)

      Case WSI.Common.CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_CARD
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7264)

      Case WSI.Common.CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_CHECK
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7268)

      Case WSI.Common.CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_CREDIT_CARD
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7266)

      Case WSI.Common.CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_DEBIT_CARD
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7265)

      Case WSI.Common.CASHIER_MOVEMENT.REDEEMABLE_PRIZE_EXPIRED
        _str_type = GLB_NLS_GUI_INVOICING.GetString(89)

      Case WSI.Common.CASHIER_MOVEMENT.NOT_REDEEMABLE_EXPIRED
        _str_type = GLB_NLS_GUI_INVOICING.GetString(478)

      Case WSI.Common.CASHIER_MOVEMENT.PROMOTION_POINT
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1121)

      Case WSI.Common.CASHIER_MOVEMENT.CANCEL_PROMOTION_POINT
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1122)

      Case WSI.Common.CASHIER_MOVEMENT.TAX_RETURNING_ON_PRIZE_COUPON
        _str_type = GLB_NLS_GUI_INVOICING.GetString(483)

        ' RRB 13-SEP-2012
      Case WSI.Common.CASHIER_MOVEMENT.DECIMAL_ROUNDING
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1126)

      Case WSI.Common.CASHIER_MOVEMENT.SERVICE_CHARGE, _
           WSI.Common.CASHIER_MOVEMENT.SERVICE_CHARGE_ON_COMPANY_A
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1127)

      Case WSI.Common.CASHIER_MOVEMENT.SERVICE_CHARGE_GAMING_TABLE, _
         WSI.Common.CASHIER_MOVEMENT.SERVICE_CHARGE_GAMING_TABLE_ON_COMPANY_A
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8774)

      Case WSI.Common.CASHIER_MOVEMENT.MB_CASH_LOST
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1320)

        'DDM 27-SEP-2012: Important!! 
        ' There is space reserved for cashier movements in GLB_NLS_GUI_PLAYER_TRACKING From 1120 to 1150.. 
      Case WSI.Common.CASHIER_MOVEMENT.REQUEST_TO_VOUCHER_REPRINT
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1128)

      Case WSI.Common.CASHIER_MOVEMENT.CHECK_PAYMENT
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1130)

        ' QMP 02-MAY-2013: Cashier session pending closing
      Case WSI.Common.CASHIER_MOVEMENT.CASH_PENDING_CLOSING
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1926)

        ' ACM 09-MAY-2013: Cashier movement - Blocked/Unblocked accounts
      Case WSI.Common.CASHIER_MOVEMENT.ACCOUNT_BLOCKED
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1133)
        'XMS 22/11/2013 set currency = ""
        CurrencyCode = ""

      Case WSI.Common.CASHIER_MOVEMENT.ACCOUNT_UNBLOCKED
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1134)
        'XMS 22/11/2013 set currency = ""
        CurrencyCode = ""

      Case WSI.Common.CASHIER_MOVEMENT.ACCOUNT_ADD_BLACKLIST
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7452)
        'XMS 22/11/2013 set currency = ""
        CurrencyCode = ""

      Case WSI.Common.CASHIER_MOVEMENT.ACCOUNT_REMOVE_BLACKLIST
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7453)
        'XMS 22/11/2013 set currency = ""
        CurrencyCode = ""

        ' ICS 26-JUL-2013: Cashier movements - Currency Exchange
      Case WSI.Common.CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT1, _
           WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CURRENCY_EXCHANGE_SPLIT1
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2219) & _str_currency

        ' 15-DEC-2014  JBC, DLL, JPJ    New Company B commission movements 
      Case WSI.Common.CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1, _
           WSI.Common.CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1_UNDO, _
           WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT1
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2538)

      Case WSI.Common.CASHIER_MOVEMENT.CASINO_CHIPS_EXCHANGE_SPLIT1
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2221)

      Case WSI.Common.CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT1, _
           WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT1
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2591)

        ' FBA 27-AUG-2013: Cashier movements - Recharge for points - Company A & B
      Case WSI.Common.CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_SPLIT1
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2510) & " - " & m_split_a_name_str

      Case WSI.Common.CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_SPLIT2
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2510) & " - " & m_split_b_name_str

        ' DHA 29-MAY-2014: New movement (143) Cashier movement for Points to redeemable as cashin prize
      Case WSI.Common.CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_PRIZE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5009)

        ' NMR 27-AUG-2013: TITO's cashier and account movements

      Case WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PRINTED_CASHABLE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2545)

      Case WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PRINTED_PROMO_REDEEMABLE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2655)

      Case WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PRINTED_PROMO_NOT_REDEEMABLE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4571)

        ' FAV 03-DIC-2015
      Case WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_PROMOBOX_PRINTED_PROMO_REDEEMABLE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6963)

        ' FAV 20-NOV-2015
      Case WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_PROMOBOX_PRINTED_PROMO_NOT_REDEEMABLE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6952)

      Case WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PRINTED_CASHABLE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4880)

      Case WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_COUNTR_PRINTED_CASHABLE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7289)

      Case WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PRINTED_PROMO_NOT_REDEEMABLE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4881)

      Case WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PAID_CASHABLE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2634)

      Case WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PAID_PROMO_REDEEMABLE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4882)

      Case WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PLAYED_CASHABLE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2841)

      Case WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PLAYED_PROMO_REDEEMABLE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4883)

      Case WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PLAYED_PROMO_NOT_REDEEMABLE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4884)

      Case WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PAID_HANDPAY
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4598)

      Case WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PAID_JACKPOT
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4599)

      Case WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_REISSUE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2997)

      Case WSI.Common.CASHIER_MOVEMENT.PARTICIPATION_IN_CASHDESK_DRAW
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2570)
        'XMS 22/11/2013 set currency = ""
        CurrencyCode = ""

      Case WSI.Common.CASHIER_MOVEMENT.PRIZE_IN_KIND1_GROSS
        _str_type = WSI.Common.Misc.ReadGeneralParams("CashDesk.Draw", "AccountingMode.KindOfNameMovement")

      Case WSI.Common.CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX1
        _str_type = String.Format("{0} - {1}", WSI.Common.Misc.ReadGeneralParams("CashDesk.Draw", "AccountingMode.KindOfNameMovement"), m_prize_taxes_1_name)

      Case WSI.Common.CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX2
        _str_type = String.Format("{0} - {1}", WSI.Common.Misc.ReadGeneralParams("CashDesk.Draw", "AccountingMode.KindOfNameMovement"), m_prize_taxes_2_name)

      Case WSI.Common.CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX3
        _str_type = String.Format("{0} - {1}", WSI.Common.Misc.ReadGeneralParams("CashDesk.Draw", "AccountingMode.KindOfNameMovement"), m_prize_taxes_3_name)

      Case WSI.Common.CASHIER_MOVEMENT.PRIZE_IN_KIND2_GROSS
        _str_type = WSI.Common.Misc.ReadGeneralParams("CashDesk.Draw", "AccountingMode.PromoNameMovement")

      Case WSI.Common.CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX1
        _str_type = String.Format("{0} - {1}", WSI.Common.Misc.ReadGeneralParams("CashDesk.Draw", "AccountingMode.PromoNameMovement"), m_prize_taxes_1_name)

      Case WSI.Common.CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX2
        _str_type = String.Format("{0} - {1}", WSI.Common.Misc.ReadGeneralParams("CashDesk.Draw", "AccountingMode.PromoNameMovement"), m_prize_taxes_2_name)

      Case WSI.Common.CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX3
        _str_type = String.Format("{0} - {1}", WSI.Common.Misc.ReadGeneralParams("CashDesk.Draw", "AccountingMode.PromoNameMovement"), m_prize_taxes_3_name)

        ' LA 16-FEB-2016 PBI 7496: New Movements (RE)
      Case WSI.Common.CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_GROSS
        _str_type = WSI.Common.Misc.ReadGeneralParams("CashDesk.Draw", "AccountingMode.RE.KindOfNameMovement")

      Case WSI.Common.CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX1
        _str_type = String.Format("{0} - {1}", WSI.Common.Misc.ReadGeneralParams("CashDesk.Draw", "AccountingMode.RE.KindOfNameMovement"), m_prize_taxes_1_name)

      Case WSI.Common.CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX2
        _str_type = String.Format("{0} - {1}", WSI.Common.Misc.ReadGeneralParams("CashDesk.Draw", "AccountingMode.RE.KindOfNameMovement"), m_prize_taxes_2_name)

      Case WSI.Common.CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX3
        _str_type = String.Format("{0} - {1}", WSI.Common.Misc.ReadGeneralParams("CashDesk.Draw", "AccountingMode.RE.KindOfNameMovement"), m_prize_taxes_3_name)

      Case WSI.Common.CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_GROSS
        _str_type = WSI.Common.Misc.ReadGeneralParams("CashDesk.Draw", "AccountingMode.RE.PromoNameMovement")

      Case WSI.Common.CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX1
        _str_type = String.Format("{0} - {1}", WSI.Common.Misc.ReadGeneralParams("CashDesk.Draw", "AccountingMode.RE.PromoNameMovement"), m_prize_taxes_1_name)

      Case WSI.Common.CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX2
        _str_type = String.Format("{0} - {1}", WSI.Common.Misc.ReadGeneralParams("CashDesk.Draw", "AccountingMode.RE.PromoNameMovement"), m_prize_taxes_2_name)

      Case WSI.Common.CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX3
        _str_type = String.Format("{0} - {1}", WSI.Common.Misc.ReadGeneralParams("CashDesk.Draw", "AccountingMode.RE.PromoNameMovement"), m_prize_taxes_3_name)

        'LA 26-FEB-2016 
      Case WSI.Common.CASHIER_MOVEMENT.PROMOTION_REDEEMABLE_FEDERAL_TAX
        _str_type = String.Format("{0} - {1}", GLB_NLS_GUI_INVOICING.GetString(358), WSI.Common.Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.1.Name"))

      Case WSI.Common.CASHIER_MOVEMENT.PROMOTION_REDEEMABLE_STATE_TAX
        _str_type = String.Format("{0} - {1}", GLB_NLS_GUI_INVOICING.GetString(358), WSI.Common.Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.2.Name"))

        ' ICS 17-SEP-2013: New Currency Exchange movements
      Case WSI.Common.CASHIER_MOVEMENT.CLOSE_SESSION_BANK_CARD
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2630)

      Case WSI.Common.CASHIER_MOVEMENT.CLOSE_SESSION_CHECK
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2631)

      Case WSI.Common.CASHIER_MOVEMENT.FILLER_OUT_CHECK
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2632)

      Case WSI.Common.CASHIER_MOVEMENT.CAGE_OPEN_SESSION '= 200
        If String.IsNullOrEmpty(CurrencyCode) Then
          _str_type = GLB_NLS_GUI_INVOICING.GetString(26) & _str_denomination
        Else
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2498) & _str_currency '& str_denomination & str_currency
        End If
        _str_type &= " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(474)


      Case WSI.Common.CASHIER_MOVEMENT.CAGE_CLOSE_SESSION '= 201
        If String.IsNullOrEmpty(CurrencyCode) Then
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2499) & _str_denomination
        Else
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2499) & _str_currency '& str_denomination & str_currency
        End If
        _str_type &= " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(474)

      Case WSI.Common.CASHIER_MOVEMENT.CAGE_FILLER_IN '= 202
        If String.IsNullOrEmpty(CurrencyCode) Then
          _str_type = GLB_NLS_GUI_INVOICING.GetString(28) & _str_denomination
        Else
          _str_type = GLB_NLS_GUI_INVOICING.GetString(28) & _str_currency '& str_denomination & str_currency
        End If
        _str_type &= " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(474)

      Case WSI.Common.CASHIER_MOVEMENT.CAGE_FILLER_OUT '= 203
        If String.IsNullOrEmpty(CurrencyCode) Then
          _str_type = GLB_NLS_GUI_INVOICING.GetString(29) & _str_denomination
        Else
          _str_type = GLB_NLS_GUI_INVOICING.GetString(29) & _str_currency '& str_denomination & str_currency
        End If
        _str_type &= " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(474)

        ' LJM 13-DEC-2013 For Chip movements
      Case WSI.Common.CASHIER_MOVEMENT.CHIPS_PURCHASE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1867)

      Case WSI.Common.CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL, _
           WSI.Common.CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL_EXCHANGE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1869)

      Case WSI.Common.CASHIER_MOVEMENT.CHIPS_SALE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1868)

      Case WSI.Common.CASHIER_MOVEMENT.CHIPS_SALE_TOTAL, _
           WSI.Common.CASHIER_MOVEMENT.CHIPS_SALE_TOTAL_EXCHANGE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1870)

      Case WSI.Common.CASHIER_MOVEMENT.CHIPS_SALE_REMAINING_AMOUNT
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8416)

      Case WSI.Common.CASHIER_MOVEMENT.CHIPS_SALE_CONSUMED_REMAINING_AMOUNT
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8474)

      Case WSI.Common.CASHIER_MOVEMENT.CHIPS_SALE_DEVOLUTION_FOR_TITO
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1871)

        ' LJM

        ' DLL 31-DEC-2013 Tax deposit
      Case WSI.Common.CASHIER_MOVEMENT.CASH_IN_TAX_SPLIT1
        If m_cash_in_tax_also_apply_to_split_b Then
          _str_type = m_cash_in_tax_name & " - " & m_split_a_name_str
        Else
          _str_type = m_cash_in_tax_name
        End If

      Case WSI.Common.CASHIER_MOVEMENT.CASH_IN_TAX_SPLIT2
        _str_type = m_cash_in_tax_name & " - " & m_split_b_name_str

      Case WSI.Common.CASHIER_MOVEMENT.TAX_CUSTODY
        _str_type = WSI.Common.Misc.TaxCustodyName()

      Case WSI.Common.CASHIER_MOVEMENT.TAX_RETURNING_CUSTODY
        _str_type = WSI.Common.Misc.TaxCustodyRefundName()

        ' DHA 02-MAR-2016 TAx Provisions
      Case WSI.Common.CASHIER_MOVEMENT.TAX_PROVISIONS
        _str_type = m_tax_provisions_name

        ' DLL 15-JAN-2014 Change chips
      Case WSI.Common.CASHIER_MOVEMENT.CHIPS_CHANGE_IN
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4462)

      Case WSI.Common.CASHIER_MOVEMENT.CHIPS_CHANGE_OUT
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4463)

      Case WSI.Common.CASHIER_MOVEMENT.GAMING_TABLE_GAME_NETWIN
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4465)

      Case WSI.Common.CASHIER_MOVEMENT.GAMING_TABLE_GAME_NETWIN_CLOSE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4606)

        ' DHA 13-MAY-2016
      Case WSI.Common.CASHIER_MOVEMENT.CAGE_CASHIER_COLLECT_DROPBOX_GAMINGTABLE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7328) ' Drop box removal from the gambling table

        ' DHA 26-JUL-2016
      Case WSI.Common.CASHIER_MOVEMENT.CAGE_CASHIER_COLLECT_DROPBOX_GAMINGTABLE_DETAIL
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7454) ' Drop box removal from the gambling table - detail

        ' RMS 21-JAN-2014
      Case WSI.Common.CASHIER_MOVEMENT.TRANSFER_CREDIT_IN
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4476)

      Case WSI.Common.CASHIER_MOVEMENT.TRANSFER_CREDIT_OUT
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4475)

        ' JPJ 22-JAN-2014
      Case WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_EXPIRED_PROMO_REDEEMABLE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4538)

      Case WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_EXPIRED_PROMO_NOT_REDEEMABLE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4539)

      Case WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_EXPIRED_REDEEMABLE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4540)

        ' DCS 14-JUL-2015: Add expired TITO movements
      Case WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_CASHIER_EXPIRED_PAID_CASHABLE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6569)

      Case WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_CASHIER_EXPIRED_PAID_PROMO_REDEEMABLE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6570)

      Case WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_CASHIER_EXPIRED_PAID_JACKPOT
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6571)

      Case WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_CASHIER_EXPIRED_PAID_HANDPAY
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6572)

        ' JPJ 31-JAN-2014
      Case WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PRINTED_HANDPAY
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4583)

      Case WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PRINTED_JACKPOT
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4584)

        ' RMS 10-FEB-2014
      Case WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_CREATE_CASHABLE_OFFLINE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4608)

      Case WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_CREATE_PLAYABLE_OFFLINE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4609)

        'ICS & DHA 12-FEB-2014
      Case WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_CASHABLE_REDEEMED_OFFLINE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4702)

      Case WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_PLAYABLE_REDEEMED_OFFLINE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4701)

        'HBB 28-MAY-2014
      Case WSI.Common.CASHIER_MOVEMENT.CHIPS_SALE_WITH_CASH_IN
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5007)

      Case WSI.Common.CASHIER_MOVEMENT.CHIPS_PURCHASE_WITH_CASH_OUT
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5008)

        'DRV 15-JUL-2014: Chips Sale Register new movements
        'Case WSI.Common.CASHIER_MOVEMENT.CHIPS_SALE_REGISTER_CASH_IN
        '  str_type = GLB_NLS_GUI_INVOICING.GetString(30) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5093)

        'Case WSI.Common.CASHIER_MOVEMENT.CHIPS_SALE_REGISTER_CASH_IN_SPLIT1
        '  str_type = m_split_a_name_str & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5093)

        'Case WSI.Common.CASHIER_MOVEMENT.CHIPS_SALE_REGISTER_CASH_IN_SPLIT2
        '  str_type = m_split_b_name_str & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5093)

        'Case WSI.Common.CASHIER_MOVEMENT.CHIPS_SALE_REGISTER_CASH_IN_TAX_SPLIT1
        '  If m_cash_in_tax_also_apply_to_split_b Then
        '    str_type = m_cash_in_tax_name & " - " & m_split_a_name_str & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5093)
        '  Else
        '    str_type = m_cash_in_tax_name & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5093)
        '  End If

        'Case WSI.Common.CASHIER_MOVEMENT.CHIPS_SALE_REGISTER_CASH_IN_TAX_SPLIT2
        '  str_type = m_cash_in_tax_name & " - " & m_split_b_name_str & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5093)

      Case WSI.Common.CASHIER_MOVEMENT.CHIPS_SALE_REGISTER_TOTAL
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5093)

        ' 15-DEC-2014  JBC, DLL, JPJ    New Company B commission movements 
      Case WSI.Common.CASHIER_MOVEMENT.CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1, _
           WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5291)

      Case WSI.Common.CASHIER_MOVEMENT.CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1, _
           WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5290)

      Case WSI.Common.CASHIER_MOVEMENT.CASH_ADVANCE_CHECK, _
           WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CHECK
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5458)

      Case WSI.Common.CASHIER_MOVEMENT.CASH_ADVANCE_CREDIT_BANK_CARD, _
           WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CREDIT_BANK_CARD, _
           WSI.Common.CASHIER_MOVEMENT.CASH_ADVANCE_CREDIT_BANK_CARD_UNDO
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5456)

      Case WSI.Common.CASHIER_MOVEMENT.CASH_ADVANCE_DEBIT_BANK_CARD, _
           WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_DEBIT_BANK_CARD, _
           WSI.Common.CASHIER_MOVEMENT.CASH_ADVANCE_DEBIT_BANK_CARD_UNDO
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5457)

      Case WSI.Common.CASHIER_MOVEMENT.CASH_ADVANCE_GENERIC_BANK_CARD, _
           WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_GENERIC_BANK_CARD, _
           WSI.Common.CASHIER_MOVEMENT.CASH_ADVANCE_GENERIC_BANK_CARD_UNDO
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5455)

      Case WSI.Common.CASHIER_MOVEMENT.CASH_ADVANCE_CURRENCY_EXCHANGE, _
           WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CURRENCY_EXCHANGE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5459)

      Case WSI.Common.CASHIER_MOVEMENT.CASH_ADVANCE_CASH
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5460)

      Case WSI.Common.CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_IN
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5446)

      Case WSI.Common.CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_OUT
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5447)

        ' DHA 08-OCT-2014: New movement (157) advice if a recharge is going to refund without plays
      Case WSI.Common.CASHIER_MOVEMENT.NOT_PLAYED_RECHARGE_REFUNDED
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5623)

        ' DHA 10-OCT-2014: added new movement (158) if there is cash short on closing cashier session
      Case WSI.Common.CASHIER_MOVEMENT.CASH_CLOSING_SHORT_DEPRECATED
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6626)

        ' DHA 10-OCT-2014: added new movement (159) if there is cash over on closing cashier session
      Case WSI.Common.CASHIER_MOVEMENT.CASH_CLOSING_OVER_DEPRECATED
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6627)

        ' JML 03-AUG-2015: ADD new movements for cash short/over on closing cashier session
      Case WSI.Common.CASHIER_MOVEMENT.CASH_CLOSING_SHORT
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2499) & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5669)
      Case WSI.Common.CASHIER_MOVEMENT.CASH_CLOSING_OVER
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2499) & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5670)

        ' OPC 27-OCT-2014: Added new movement (510)
      Case WSI.Common.CASHIER_MOVEMENT.HANDPAY_AUTHORIZED
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1834) & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5593)

        ' OPC 3-NOV-2014: Added new movement(10) for mobile bank
      Case WSI.Common.CASHIER_MOVEMENT.MB_EXCESS
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5685)

      Case WSI.Common.CASHIER_MOVEMENT.HANDPAY_WITHHOLDING
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5800)

        'FBA 27-AUG-2013 default movement ID - Movement (XX)

      Case WSI.Common.CASHIER_MOVEMENT.WIN_LOSS_STATEMENT_REQUEST
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6253)

      Case WSI.Common.CASHIER_MOVEMENT.CASHIER_REFILL_HOPPER_TERMINAL, _
           WSI.Common.CASHIER_MOVEMENT.CASHIER_REFILL_HOPPER_TERMINAL_CAGE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7055)

      Case WSI.Common.CASHIER_MOVEMENT.CASHIER_COLLECT_DROPBOX_TERMINAL, _
           WSI.Common.CASHIER_MOVEMENT.CASHIER_COLLECT_DROPBOX_TERMINAL_CAGE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7054)

        ' DHA 27-JAN-2016: new movement for dual currency when ticket is played
      Case WSI.Common.CASHIER_MOVEMENT.BILL_IN_EXCHANGE_DUAL_CURRENCY
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7079) & _str_currency

      Case WSI.Common.CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_NATIONAL_CURRENCY
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7318)

      Case WSI.Common.CASHIER_MOVEMENT.CURRENCY_EXCHANGE_AMOUNT_OUT
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7319)

      Case WSI.Common.CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_FOREIGN_CURRENCY
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7320)

      Case WSI.Common.CASHIER_MOVEMENT.CURRENCY_EXCHANGE_AMOUNT_IN
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7321)

      Case WSI.Common.CASHIER_MOVEMENT.CURRENCY_EXCHANGE_AMOUNT_IN_CHANGE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7322)

      Case WSI.Common.CASHIER_MOVEMENT.MULTIPLE_BUCKETS_END_SESSION To WSI.Common.CASHIER_MOVEMENT.MULTIPLE_BUCKETS_END_SESSION_LAST ' [1100-1199]
        If (_buckets_forms.GetBucketName(CType(MovType, CASHIER_MOVEMENT), _name)) Then
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7075, _name)
        Else
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2504, MovType.ToString) ' Default value if error.
        End If

      Case WSI.Common.CASHIER_MOVEMENT.MULTIPLE_BUCKETS_EXPIRED To WSI.Common.CASHIER_MOVEMENT.MULTIPLE_BUCKETS_EXPIRED_LAST ' [1200-1299]
        If (_buckets_forms.GetBucketName(CType(MovType, CASHIER_MOVEMENT), _name)) Then
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7074, _name)
        Else
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2504, MovType.ToString) ' Default value if error.
        End If

      Case WSI.Common.CASHIER_MOVEMENT.MULTIPLE_BUCKETS_MANUAL_ADD To WSI.Common.CASHIER_MOVEMENT.MULTIPLE_BUCKETS_MANUAL_ADD_LAST ' [1300-1399]
        If (_buckets_forms.GetBucketName(CType(MovType, CASHIER_MOVEMENT), _name)) Then
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7065, _name)
        Else
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2504, MovType.ToString) ' Default value if error.
        End If

      Case WSI.Common.CASHIER_MOVEMENT.MULTIPLE_BUCKETS_MANUAL_SUB To WSI.Common.CASHIER_MOVEMENT.MULTIPLE_BUCKETS_MANUAL_SUB_LAST ' [1400-1499]
        If (_buckets_forms.GetBucketName(CType(MovType, CASHIER_MOVEMENT), _name)) Then
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7066, _name)
        Else
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2504, MovType.ToString) ' Default value if error.
        End If

      Case WSI.Common.CASHIER_MOVEMENT.MULTIPLE_BUCKETS_MANUAL_SET To WSI.Common.CASHIER_MOVEMENT.MULTIPLE_BUCKETS_MANUAL_SET_LAST ' [1400-1499]
        If (_buckets_forms.GetBucketName(CType(MovType, CASHIER_MOVEMENT), _name)) Then
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7076, _name)
        Else
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2504, MovType.ToString) ' Default value if error.
        End If

      Case WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_CASHIER_VOID_CASHABLE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7106)

      Case WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_CASHIER_VOID_PROMO_REDEEMABLE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7134)

      Case WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_CASHIER_VOID_PROMO_NOT_REDEEMABLE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7135)

      Case WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_MACHINE_VOID_CASHABLE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7136)

      Case WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_MACHINE_VOID_PROMO_NOT_REDEEMABLE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7137)

        ' DHA 27-JAN-2016: new movement for dual currency when ticket is created
      Case WSI.Common.CASHIER_MOVEMENT.TITO_TICKETS_CREATE_EXCHANGE_DUAL_CURRENCY
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7077) & _str_currency

        ' DHA 27-JAN-2016: new movement for dual currency when ticket is played
      Case WSI.Common.CASHIER_MOVEMENT.TITO_TICKETS_PLAYED_EXCHANGE_DUAL_CURRENCY
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7078) & _str_currency

        ' RAB 24-FEB-2016
      Case WSI.Common.CASHIER_MOVEMENT.TITO_VALIDATE_TICKET
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4716)

        'EOR 22-MAR-2016
      Case WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_CASHIER_SWAP_FOR_CHIPS
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7148)

      Case WSI.Common.CASHIER_MOVEMENT.CASHIER_CHANGE_STACKER_TERMINAL
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7223)

      Case WSI.Common.CASHIER_MOVEMENT.TERMINAL_REFILL_HOPPER
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7224)

        ' LA 12-JUL-2016
      Case WSI.Common.CASHIER_MOVEMENT.CARD_ASSOCIATE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7403)

      Case CASHIER_MOVEMENT.CREDIT_LINE_MARKER
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7970)

      Case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7971)

      Case CASHIER_MOVEMENT.CREDIT_LINE_MARKER_CARD_REPLACEMENT
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7989)

      Case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CARD_EXCHANGE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7990)

      Case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CREDIT_CARD_EXCHANGE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7991)

      Case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_DEBIT_CARD_EXCHANGE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7992)

      Case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CHECK_EXCHANGE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7993)

      Case Else
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2504, MovType.ToString)

    End Select

    Return _str_type
  End Function ' TranslateMovementType

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _mov_type As Integer
    Dim _mov_type_tmp As Integer
    Dim _str_type As String
    Dim _total_row As DataRow
    Dim _currency_code As String
    Dim _national_currency_code As String
    Dim _denomination As Decimal
    Dim _must_show_total As Boolean
    Dim _sub_amount As Decimal
    Dim _add_amount As Decimal
    Dim _total_amount As Decimal
    Dim _str_total As String
    Dim _str_sub_amount As String
    Dim _str_add_amount As String
    Dim _units As Int32
    Dim _str_type_name As String
    Dim _show_currency_symbol As Boolean
    Dim _show_currency_type As String
    Dim _cage_currency_type As Integer
    Dim _chip_id As Integer
    Dim _chipset_id As Integer
    Dim _str_suffix As String

    _str_type = String.Empty
    _currency_code = String.Empty
    _national_currency_code = CurrencyExchange.GetNationalCurrency()

    ' Add/Sub amount
    _str_total = String.Empty
    _str_sub_amount = String.Empty
    _str_add_amount = String.Empty
    _sub_amount = 0
    _add_amount = 0
    _currency_code = String.Empty
    _show_currency_type = String.Empty
    _cage_currency_type = 0
    _chip_id = 0
    _chipset_id = 0

    If Not DbRow.IsNull(SQL_COLUMN_SUB_AMOUNT) Then
      _sub_amount = DbRow.Value(SQL_COLUMN_SUB_AMOUNT)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_ADD_AMOUNT) Then
      _add_amount = DbRow.Value(SQL_COLUMN_ADD_AMOUNT)
    End If

    ' Currency ISO code
    If Not DbRow.IsNull(SQL_COLUMN_CURRENCY_ISO_CODE) Then
      _currency_code = DbRow.Value(SQL_COLUMN_CURRENCY_ISO_CODE)
    End If

    ' CAGE Currency Type
    If Not DbRow.IsNull(SQL_COLUMN_CURRENCY_TYPE) Then
      _cage_currency_type = DbRow.Value(SQL_COLUMN_CURRENCY_TYPE)
    End If

    ' Backward compatibility with X01
    If (_currency_code = WSI.Common.Cage.CHIPS_ISO_CODE) Then
      _currency_code = m_national_currency_code
      _cage_currency_type = WSI.Common.FeatureChips.ChipType.RE
    End If

    ' Chip ID -> Search its ChipSet description
    If Not DbRow.IsNull(SQL_COLUMN_CHIP_ID) Then
      _chip_id = DbRow.Value(SQL_COLUMN_CHIP_ID)
    End If

    _show_currency_symbol = (String.IsNullOrEmpty(_currency_code) OrElse _currency_code = _national_currency_code)

    _str_sub_amount = GUI_FormatCurrency(_sub_amount, 2, , _show_currency_symbol, _currency_code)
    _str_add_amount = GUI_FormatCurrency(_add_amount, 2, , _show_currency_symbol, _currency_code)

    ' Session Id
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SESSION_ID).Value = DbRow.Value(SQL_COLUMN_SESSION_ID)

    ' Session Name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SESSION_NAME).Value = DbRow.Value(SQL_COLUMN_SESSION_NAME)

    ' Date
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATE), _
                                                                                ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Cashier Id
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CASHIER_ID).Value = DbRow.Value(SQL_COLUMN_CASHIER_ID)

    ' Cashier Name
    If Not DbRow.IsNull(SQL_COLUMN_CASHIER_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CASHIER_NAME).Value = Computer.Alias(DbRow.Value(SQL_COLUMN_CASHIER_NAME))
    End If

    ' User Id
    Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_ID).Value = DbRow.Value(SQL_COLUMN_USER_ID)

    ' User Name
    If Not DbRow.IsNull(SQL_COLUMN_USER_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_NAME).Value = DbRow.Value(SQL_COLUMN_USER_NAME)
    End If

    ' Movement type
    _mov_type = DbRow.Value(SQL_COLUMN_TYPE)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE_ID).Value = _mov_type
    _mov_type_tmp = _mov_type

    ' Denomination
    _denomination = DbRow.Value(SQL_COLUMN_DENOMINATION)

    ' Movement type names
    _str_type_name = TranslateMovementType(_mov_type, _currency_code, _denomination)

    ' Update movement type name if movement operation is undone
    If Not DbRow.IsNull(SQL_COLUMN_UNDO_STATUS) AndAlso DbRow.Value(SQL_COLUMN_UNDO_STATUS) = UndoStatus.UndoOperation Then
      If _mov_type <> WSI.Common.CASHIER_MOVEMENT.HANDPAY_CANCELLATION And _mov_type <> CASHIER_MOVEMENT.MANUAL_HANDPAY_CANCELLATION Then
        _str_type_name = "(" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2786) & ") " & _str_type_name
      End If
    ElseIf _mov_type = WSI.Common.CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_IN AndAlso _add_amount < 0 Then
      _str_type_name = "(" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2786) & ") " & _str_type_name
    ElseIf _mov_type = WSI.Common.CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_OUT AndAlso _sub_amount < 0 Then
      _str_type_name = "(" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2786) & ") " & _str_type_name
    End If

    'Iso_Code
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ISO_CODE).Value = GetCurrencyCodeToGrid(_mov_type, _
                                                                               IIf(String.IsNullOrEmpty(_currency_code) _
                                                                                , _national_currency_code _
                                                                                , _currency_code), _add_amount)

    ' Details
    If Not DbRow.IsNull(SQL_COLUMN_DETAILS) AndAlso Not String.IsNullOrEmpty(DbRow.Value(SQL_COLUMN_DETAILS)) Then
      Select Case _mov_type
        Case CASHIER_MOVEMENT.TITO_TICKET_REISSUE _
           , CASHIER_MOVEMENT.PROMOTION_NOT_REDEEMABLE _
           , CASHIER_MOVEMENT.PROMOTION_REDEEMABLE _
           , CASHIER_MOVEMENT.PROMOTION_POINT
          ' ATB 04-NOV-2016 Bug 20147:Televisa: Cambios solicitados por Televisa para "Movimientos en caja"
          If WSI.Common.Misc.IsVoucherModeTV And DbRow.Value(SQL_COLUMN_DETAILS).ToString().ToLower() = GLB_NLS_GUI_INVOICING.GetString(512).ToLower() Then
            _str_type_name = DbRow.Value(SQL_COLUMN_DETAILS)
          Else
            _str_type_name &= " (" & DbRow.Value(SQL_COLUMN_DETAILS) & ")"
          End If
        Case Else
      End Select
    End If

    ' Account Id
    If Not DbRow.IsNull(SQL_COLUMN_ACCOUNT_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_ID)
    End If

    'JCA Cuando es movimiento de B�veda le cambi� el mov_type, al final le volvemos a poner el mismo
    Select Case _mov_type
      Case CASHIER_MOVEMENT.CAGE_OPEN_SESSION
        _mov_type = CASHIER_MOVEMENT.OPEN_SESSION
        If _currency_code = _national_currency_code Then
          _currency_code = ""
        End If

      Case CASHIER_MOVEMENT.CAGE_CLOSE_SESSION
        _mov_type = CASHIER_MOVEMENT.CLOSE_SESSION
        If _currency_code = _national_currency_code Then
          _currency_code = ""
        End If

      Case CASHIER_MOVEMENT.CAGE_FILLER_IN
        _mov_type = CASHIER_MOVEMENT.FILLER_IN
        If _currency_code = _national_currency_code Then
          _currency_code = ""
        End If

      Case CASHIER_MOVEMENT.CAGE_FILLER_OUT
        _mov_type = CASHIER_MOVEMENT.FILLER_OUT
        If _currency_code = _national_currency_code Then
          _currency_code = ""
        End If

    End Select

    If Not DbRow.IsNull(SQL_COLUMN_SUB_AMOUNT) Then
      Select Case _mov_type
        Case CASHIER_MOVEMENT.REQUEST_TO_VOUCHER_REPRINT
          _str_sub_amount = ""
          _str_add_amount = GUI_FormatNumber(_add_amount, 0)

        Case CASHIER_MOVEMENT.PROMOTION_POINT _
           , CASHIER_MOVEMENT.CANCEL_PROMOTION_POINT
          _str_sub_amount = GUI_FormatNumber(_sub_amount)
          _str_add_amount = GUI_FormatNumber(_add_amount)

        Case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT1, _
             CASHIER_MOVEMENT.COMPANY_B_CURRENCY_EXCHANGE_SPLIT1
          _str_sub_amount = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509) & ": " & Currency.Format(Math.Round(_sub_amount, 2), _national_currency_code)
          _str_add_amount = Currency.Format(Math.Round(DbRow.Value(SQL_COLUMN_INIT_BALANCE), 2), _currency_code) _
                            & " = " _
                            & Currency.Format(Math.Round(_add_amount, 2), _national_currency_code)

          '15-12-2014 JPJ New Company B commission movements 
        Case CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1 _
           , CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1_UNDO _
           , CASHIER_MOVEMENT.CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1 _
           , CASHIER_MOVEMENT.CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1 _
           , CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT1 _
           , CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1 _
           , CASHIER_MOVEMENT.COMPANY_B_CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1 _
           , CASHIER_MOVEMENT.CASH_ADVANCE_GENERIC_BANK_CARD _
           , CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_GENERIC_BANK_CARD _
           , CASHIER_MOVEMENT.CASH_ADVANCE_CREDIT_BANK_CARD _
           , CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CREDIT_BANK_CARD _
           , CASHIER_MOVEMENT.CASH_ADVANCE_DEBIT_BANK_CARD _
           , CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_DEBIT_BANK_CARD _
           , CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT1 _
           , CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT1 _
           , CASHIER_MOVEMENT.CASH_ADVANCE_CHECK _
           , CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CHECK
          _str_sub_amount = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509) & ": " & _str_sub_amount

        Case CASHIER_MOVEMENT.PARTICIPATION_IN_CASHDESK_DRAW
          If (_add_amount = 0) Then
            _str_sub_amount = String.Empty
            _str_add_amount = String.Empty
          Else
            _str_sub_amount = GUI_FormatCurrency(_sub_amount, 2, , _show_currency_symbol, _currency_code)
            _str_add_amount = GUI_FormatCurrency(_add_amount, 2, , _show_currency_symbol, _currency_code)
          End If

        Case CASHIER_MOVEMENT.CLOSE_SESSION _
           , CASHIER_MOVEMENT.CLOSE_SESSION_BANK_CARD _
           , CASHIER_MOVEMENT.CLOSE_SESSION_CHECK

          _sub_amount = DbRow.Value(SQL_COLUMN_FINAL_BALANCE)

          ' the following if adds the minus symbol to the balance amount in the gaming table close override
          If Not DbRow.IsNull(SQL_COLUMN_UNDO_STATUS) AndAlso DbRow.Value(SQL_COLUMN_UNDO_STATUS) = UndoStatus.UndoOperation AndAlso Not _sub_amount.ToString().Contains("-") AndAlso Not _sub_amount = 0.0 Then
            _str_sub_amount = _sub_amount.ToString().Insert(0, "-")
            _sub_amount = Decimal.Parse(_str_sub_amount)
          End If

          _str_sub_amount = GUI_FormatCurrency(_sub_amount, 2, , _show_currency_symbol, _currency_code)

        Case CASHIER_MOVEMENT.NOT_PLAYED_RECHARGE_REFUNDED
          _str_add_amount = ""

        Case CASHIER_MOVEMENT.WIN_LOSS_STATEMENT_REQUEST _
           , CASHIER_MOVEMENT.REOPEN_CASHIER
          _str_sub_amount = ""
          _str_add_amount = ""

          ' DHA 27-JAN-2016: new movement for dual currency when ticket is created
        Case CASHIER_MOVEMENT.TITO_TICKETS_CREATE_EXCHANGE_DUAL_CURRENCY
          _str_sub_amount = 0
          _str_add_amount = Currency.Format(Math.Round(DbRow.Value(SQL_COLUMN_INIT_BALANCE), 2), _currency_code) _
                            & " = " _
                            & Currency.Format(Math.Round(_add_amount, 2), _national_currency_code)

          ' DHA 27-JAN-2016: new movement for dual currency when ticket is played
        Case CASHIER_MOVEMENT.TITO_TICKETS_PLAYED_EXCHANGE_DUAL_CURRENCY
          _str_sub_amount = Currency.Format(Math.Round(_sub_amount, 2), _national_currency_code) _
                            & " = " _
                            & Currency.Format(Math.Round(DbRow.Value(SQL_COLUMN_INIT_BALANCE), 2), _currency_code)
          _str_add_amount = 0

          ' DHA 27-JAN-2016: new movement for dual currency when bill in
        Case CASHIER_MOVEMENT.BILL_IN_EXCHANGE_DUAL_CURRENCY
          _str_sub_amount = 0
          _str_add_amount = Currency.Format(Math.Round(DbRow.Value(SQL_COLUMN_INIT_BALANCE), 2), _currency_code) _
                            & " = " _
                            & Currency.Format(Math.Round(_add_amount, 2), _national_currency_code)

        Case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_NATIONAL_CURRENCY
          _str_sub_amount = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509) & ": " & Currency.Format(Math.Round(_sub_amount, 2), _national_currency_code)
          _str_add_amount = Currency.Format(Math.Round(DbRow.Value(SQL_COLUMN_INIT_BALANCE), 2), _currency_code) _
                            & " = " _
                            & Currency.Format(Math.Round(_add_amount, 2), _national_currency_code)

        Case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_FOREIGN_CURRENCY
          _str_sub_amount = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509) & ": " & Currency.Format(Math.Round(_sub_amount, 2), _national_currency_code)
          _str_add_amount = Currency.Format(Math.Round(_add_amount, 2), _national_currency_code) _
                            & " = " _
                            & Currency.Format(Math.Round(DbRow.Value(SQL_COLUMN_INIT_BALANCE), 2), _currency_code)

        Case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_AMOUNT_OUT
          _str_add_amount = _str_sub_amount
          _str_sub_amount = ""

        Case CASHIER_MOVEMENT.CHIPS_SALE_TOTAL_EXCHANGE
          _str_sub_amount = Currency.Format(Math.Round(DbRow.Value(SQL_COLUMN_INIT_BALANCE), 2), _currency_code) _
                            & " = " _
                            & Currency.Format(Math.Round(_sub_amount, 2), _national_currency_code)
          _str_add_amount = 0

        Case CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL_EXCHANGE
          _str_sub_amount = 0
          _str_add_amount = Currency.Format(Math.Round(DbRow.Value(SQL_COLUMN_INIT_BALANCE), 2), _currency_code) _
                            & " = " _
                            & Currency.Format(Math.Round(_add_amount, 2), _national_currency_code)

        Case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_VOID_CASHABLE _
           , CASHIER_MOVEMENT.TITO_TICKET_CASHIER_VOID_CASHABLE _
           , CASHIER_MOVEMENT.TITO_TICKET_CASHIER_VOID_PROMO_REDEEMABLE _
           , CASHIER_MOVEMENT.TITO_TICKET_CASHIER_VOID_PROMO_NOT_REDEEMABLE _
           , CASHIER_MOVEMENT.TITO_TICKET_MACHINE_VOID_CASHABLE _
           , CASHIER_MOVEMENT.TITO_TICKET_MACHINE_VOID_PROMO_NOT_REDEEMABLE

          _add_amount = -_add_amount
          _str_add_amount = GUI_FormatCurrency(_add_amount, 2, , _show_currency_symbol, _currency_code)

        Case CASHIER_MOVEMENT.MULTIPLE_BUCKETS_END_SESSION To CASHIER_MOVEMENT.MULTIPLE_BUCKETS_MANUAL_SET_LAST

          If GetBucketFormatedAmount(_mov_type) = Buckets.BucketUnits.Points Then
            _str_sub_amount = GUI_FormatNumber(_sub_amount)
            _str_add_amount = GUI_FormatNumber(_add_amount)
          End If

        Case Else
          '_str_sub_amount
          '_str_add_amount

      End Select
    End If

    If Not DbRow.IsNull(SQL_COLUMN_AUX_AMOUNT) Then
      Select Case _mov_type

        ' OPC 24-NOV-2014: IVA: xxx, where xxx is the _str_sub_amount
        Case WSI.Common.CASHIER_MOVEMENT.MB_CASH_IN_SPLIT2 _
           , WSI.Common.CASHIER_MOVEMENT.CASH_IN_SPLIT2 _
           , WSI.Common.CASHIER_MOVEMENT.SERVICE_CHARGE _
           , WSI.Common.CASHIER_MOVEMENT.SERVICE_CHARGE_ON_COMPANY_A _
           , WSI.Common.CASHIER_MOVEMENT.SERVICE_CHARGE_GAMING_TABLE _
           , WSI.Common.CASHIER_MOVEMENT.SERVICE_CHARGE_GAMING_TABLE_ON_COMPANY_A _
           , WSI.Common.CASHIER_MOVEMENT.NA_CASH_IN_SPLIT2 _
           , WSI.Common.CASHIER_MOVEMENT.CARD_DEPOSIT_IN _
           , WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT _
           , WSI.Common.CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CHECK _
           , WSI.Common.CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CURRENCY_EXCHANGE _
           , WSI.Common.CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_CREDIT _
           , WSI.Common.CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_DEBIT _
           , WSI.Common.CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_GENERIC _
           , WSI.Common.CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_UNDO _
           , WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CHECK _
           , WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_CREDIT _
           , WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_DEBIT _
           , WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_GENERIC _
           , WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CHECK _
           , WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CURRENCY_EXCHANGE _
           , WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_CREDIT _
           , WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_DEBIT _
           , WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_GENERIC _
           , WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CHECK _
           , WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_CREDIT _
           , WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_DEBIT _
           , WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_GENERIC _
           , WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_UNDO _
           , WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN _
           , WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT

          Dim _aux_amount As Decimal = DbRow.Value(SQL_COLUMN_AUX_AMOUNT)

          _str_sub_amount = String.Empty
          ''commission
          If _sub_amount <> 0.0 Then
            _str_sub_amount += GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509) & ": " & GUI_FormatCurrency(_sub_amount, 2, , _show_currency_symbol)
          End If
          '' tax
          If _aux_amount <> 0.0 Then
            If _str_sub_amount <> String.Empty Then
              _str_sub_amount += ", "
            End If
            _str_sub_amount += m_split_b_tax_name & ": " & GUI_FormatCurrency(_aux_amount, 2, , _show_currency_symbol)
          End If

      End Select
    End If

    'JCA Cuando era movimiento de B�veda se le cambi� el mov_type, ahora le volvemos a poner el mismo
    _mov_type = _mov_type_tmp

    _must_show_total = True

    ' JML 30-JUL-2015
    If _mov_type = CASHIER_MOVEMENT.CASH_CLOSING_SHORT Or _mov_type = CASHIER_MOVEMENT.CASH_CLOSING_OVER Or _
      _mov_type = CASHIER_MOVEMENT.GAMING_TABLE_GAME_NETWIN Or _mov_type = CASHIER_MOVEMENT.GAMING_TABLE_GAME_NETWIN_CLOSE Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DENOMINATION).Value = Cage.GetDenomination(_denomination, _currency_code)
    End If

    ' LJM Also if it is a chips movement
    If (IsAChipMovement(_mov_type)) Then
      'XMS rellenamos los valores en las tres columans a�adidas para b�veda

      _units = 0

      _must_show_total = (_mov_type = CASHIER_MOVEMENT.CHIPS_CHANGE_IN OrElse _mov_type = CASHIER_MOVEMENT.CHIPS_CHANGE_OUT)

      If _sub_amount <> 0 Then
        _total_amount = _sub_amount
      ElseIf _add_amount <> 0 Then
        _total_amount = _add_amount
      End If

      Me.Grid.Cell(RowIndex, GRID_COLUMN_DENOMINATION).Value = Cage.GetDenomination(_denomination, _currency_code)

      If (_denomination > 0 _
          And _currency_code <> Cage.CHIPS_ISO_CODE _
          And (Not FeatureChips.IsCageCurrencyTypeChips(CType(_cage_currency_type, CageCurrencyType)))) Then
        If _cage_currency_type = CageCurrencyType.Bill Then
          _show_currency_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5660)   'Bill
        ElseIf _cage_currency_type = CageCurrencyType.Coin Then
          _show_currency_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5661)   'Coin
        End If
      End If

      If _total_amount <> 0 Then
        If _denomination > 0 Then
          _units = Decimal.ToInt32(Decimal.Divide(_total_amount, _denomination))
        Else
          Select Case _denomination

            Case Cage.TICKETS_CODE
              _units = _total_amount
              _total_amount = 0

            Case Cage.COINS_CODE
              _units = 0

            Case Else
              If _currency_code = WSI.Common.Cage.CHIPS_COLOR Then
                _units = _total_amount
              Else
                _units = 1
              End If

          End Select
        End If
      End If

      If _units <> 0 Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_UNITS).Value = GUI_FormatNumber(_units, 0)
      End If

      If _total_amount <> 0 Then
        _str_total = GUI_FormatCurrency(_total_amount, 2, , _show_currency_symbol, _currency_code)
      End If

      _str_sub_amount = ""
      _str_add_amount = ""
    End If

    ' Search currency type description
    If String.IsNullOrEmpty(_show_currency_type) Then
      _show_currency_type = GetChipTypeDescription(_cage_currency_type, Nothing)
    End If

    ' Search ChipSet id and description
    If (_chip_id <> 0) Then
      _chipset_id = GetChipSetIDFromChip(_chip_id) 'Get the ChipSet from the Chip Id
    End If

    ' Append the ChipSet description and the Currency Type to the movement description
    _str_suffix = GetChipsetAndCurrencyTypeSuffix(_chipset_id, _show_currency_type)
    _str_type_name &= _str_suffix

    If (_currency_code = WSI.Common.Cage.CHIPS_COLOR) Then
      'If Color CHIPS then it is an integer value, without decimals
      If (Not String.IsNullOrEmpty(_str_total)) Then
        _str_total = GUI_FormatNumber(_total_amount, 0)
      End If

      If (Not String.IsNullOrEmpty(_str_sub_amount)) Then
        _str_sub_amount = GUI_FormatNumber(_sub_amount, 0)
      End If

      If (Not String.IsNullOrEmpty(_str_add_amount)) Then
        _str_add_amount = GUI_FormatNumber(_add_amount, 0)
      End If
    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE_NAME).Value = _str_type_name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL).Value = _str_total
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SUB_AMOUNT).Value = _str_sub_amount
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ADD_AMOUNT).Value = _str_add_amount

    ' Currency Type: Bill, Coin, Valor Chips, Color Chips, ...
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CURRENCY_TYPE).Value = _show_currency_type

    'EOR 22-SEP-2016
    If _mov_type = WSI.Common.CASHIER_MOVEMENT.SERVICE_CHARGE_ON_COMPANY_A OrElse _
      _mov_type = WSI.Common.CASHIER_MOVEMENT.SERVICE_CHARGE_GAMING_TABLE OrElse _
      _mov_type = WSI.Common.CASHIER_MOVEMENT.SERVICE_CHARGE_GAMING_TABLE_ON_COMPANY_A Then

      _mov_type = WSI.Common.CASHIER_MOVEMENT.SERVICE_CHARGE
    End If

    'Accumulate in totals
    If _must_show_total Then

      _total_row = GetRowFromTotalsByType(_mov_type, _currency_code, _str_suffix)

      _total_row.Item("TOTAL_SUB_AMOUNT") = _total_row.Item("TOTAL_SUB_AMOUNT") + _sub_amount
      _total_row.Item("TOTAL_ADD_AMOUNT") = _total_row.Item("TOTAL_ADD_AMOUNT") + _add_amount
      _total_row.Item("TOTAL_CURRENCY") = _total_row.Item("TOTAL_CURRENCY") + DbRow.Value(SQL_COLUMN_INIT_BALANCE)
      _total_row.Item("COUNT") = _total_row.Item("COUNT") + 1

    End If

    Return True

  End Function ' GUI_SetupRow

  Private Function IsAChipMovement(MovementType As Integer) As Boolean
    Return ((MovementType >= CASHIER_MOVEMENT.CAGE_OPEN_SESSION AndAlso MovementType <= CASHIER_MOVEMENT.CAGE_LAST_MOVEMENT) _
                OrElse (MovementType = CASHIER_MOVEMENT.CHIPS_SALE OrElse MovementType = CASHIER_MOVEMENT.CHIPS_PURCHASE _
                OrElse MovementType = CASHIER_MOVEMENT.CHIPS_CHANGE_IN OrElse MovementType = CASHIER_MOVEMENT.CHIPS_CHANGE_OUT))
  End Function

  Public Function GetBucketFormatedAmount(CashierMovement As CASHIER_MOVEMENT) As Buckets.BucketUnits
    Dim _bucket_form As BucketsForm
    Dim _type As Buckets.BucketType

    _bucket_form = New BucketsForm()

    _type = Buckets.BucketId.RankingLevelPoints

    _bucket_form.GetBucketType(CashierMovement, _type)

    Return Buckets.GetBucketUnits(_type)

  End Function

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.dtp_from
  End Sub 'GUI_SetInitialFocus

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(209), m_cashiers)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(220), m_users)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5714), m_terminal_location)

    'PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(221), m_session)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(211), m_types)
    If bl_sessions Then
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(208) & " ", m_session_name)
    Else
      PrintData.SetFilter("", "", True)
    End If

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), m_account)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_track_data)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), m_holder)

    ' With
    PrintData.FilterHeaderWidth(1) = 1000
    PrintData.FilterValueWidth(1) = 1600
    PrintData.FilterHeaderWidth(2) = 800
    PrintData.FilterValueWidth(2) = 2500
    PrintData.FilterHeaderWidth(3) = 500
    PrintData.FilterValueWidth(3) = 8750

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    ' Prevent date/time column from being squeezed too much and drop the time part to the following line
    PrintData.Settings.Columns(GRID_COLUMN_DATE).IsWidthFixed = True
    PrintData.Settings.Columns(GRID_COLUMN_SESSION_NAME).IsWidthFixed = True
    'PrintData.Settings.Colums(GRID_COLUMN_CARD_TRACK).IsWidthFixed = True

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Params.Title = GLB_NLS_GUI_INVOICING.GetString(217)

    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_cashiers = ""
    m_users = ""
    m_date_from = ""
    m_date_to = ""
    'm_session = ""
    m_types = ""
    m_account = String.Empty
    m_track_data = String.Empty
    m_holder = String.Empty
    m_terminal_location = String.Empty

    'Date 
    If Me.dtp_from.Checked Then
      m_date_from = GUI_FormatDate(dtp_from.Value, _
                    ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Me.dtp_to.Checked Then
      m_date_to = GUI_FormatDate(dtp_to.Value, _
                  ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Cashiers
    If Me.opt_all_cashiers.Checked Then
      m_cashiers = Me.opt_all_cashiers.Text
    Else
      m_cashiers = Me.cmb_cashier.TextValue
    End If

    If Me.chk_not_show_system_sessions.Checked Then
      m_terminal_location = GLB_NLS_GUI_PLAYER_TRACKING.GetString(698)
    Else
      m_terminal_location = GLB_NLS_GUI_PLAYER_TRACKING.GetString(699)
    End If

    ' Users
    If Me.opt_all_users.Checked Then
      m_users = Me.opt_all_users.Text
    Else
      m_users = Me.cmb_user.TextValue
    End If

    ' Movement Types
    If Me.opt_all_types.Checked Then
      m_types = Me.opt_all_types.Text
    Else
      m_types = GetTypeListSelected()
    End If

    ' Account
    m_account = Me.Uc_account_sel1.Account()
    m_track_data = Me.Uc_account_sel1.TrackData()
    m_holder = Me.Uc_account_sel1.Holder()

  End Sub

#End Region ' GUI Reports

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    bl_sessions = False
    m_init_date_from = Nothing
    m_init_date_to = Nothing
    m_only_show_chips_movements = False

    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub


  ' PURPOSE: Opens dialog with default settings for edit mode with filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - SessionId:
  '           - SessionName:
  '           - DateFrom: Movements date from
  '           - DateTo: Movements date to
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window, _
                         ByVal SessionId As Integer, _
                         ByVal SessionName As String, _
                         Optional ByVal DateFrom As Date = Nothing, _
                         Optional ByVal DateTo As Date = Nothing)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING

    bl_sessions = True
    m_session_id = SessionId
    m_session_name = SessionName

    m_init_date_from = DateFrom
    m_init_date_to = DateTo
    m_only_show_chips_movements = False

    Me.MdiParent = MdiParent
    Me.Display(False)

    ' For Show window before filter apply 
    Call Application.DoEvents()
    System.Threading.Thread.Sleep(100)
    Call Application.DoEvents()

    Me.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)


  End Sub

  ' PURPOSE: Opens dialog with default settings for edit mode with filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - SessionId:
  '           - SessionName:
  '           - DateFrom: Movements date from
  '           - DateTo: Movements date to
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window, _
                         ByVal SessionId As Integer)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING

    bl_sessions = True
    m_session_id = SessionId
    m_session_name = ""

    Me.bl_sessions = True

    tf_cm_cash_desk_session.Text = ""

    m_init_date_from = Nothing
    m_init_date_to = Nothing
    m_only_show_chips_movements = True

    gb_type.Visible = False

    Me.MdiParent = MdiParent
    Me.Display(False)

    ' For Show window before filter apply 
    Call Application.DoEvents()
    System.Threading.Thread.Sleep(100)
    Call Application.DoEvents()

    Me.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)

  End Sub

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      ' Session Id
      .Column(GRID_COLUMN_SESSION_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_SESSION_ID).Width = 0
      .Column(GRID_COLUMN_SESSION_ID).IsColumnPrintable = False

      ' Session Name
      .Column(GRID_COLUMN_SESSION_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(208)
      .Column(GRID_COLUMN_SESSION_NAME).Width = GRID_WIDTH_SESSION_NAME
      .Column(GRID_COLUMN_SESSION_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_SESSION_NAME).Header(0).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Date
      .Column(GRID_COLUMN_DATE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(201)
      .Column(GRID_COLUMN_DATE).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_DATE).Header(0).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Cashier Id
      .Column(GRID_COLUMN_CASHIER_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_CASHIER_ID).Width = 0
      .Column(GRID_COLUMN_CASHIER_ID).IsColumnPrintable = False

      ' Cashier Name
      .Column(GRID_COLUMN_CASHIER_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(209)
      .Column(GRID_COLUMN_CASHIER_NAME).Width = GRID_WIDTH_CASHIER
      .Column(GRID_COLUMN_CASHIER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_CASHIER_NAME).Header(0).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' User Id
      .Column(GRID_COLUMN_USER_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_USER_ID).Width = 0
      .Column(GRID_COLUMN_USER_ID).IsColumnPrintable = False

      ' User Name
      .Column(GRID_COLUMN_USER_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(220)
      .Column(GRID_COLUMN_USER_NAME).Width = GRID_WIDTH_USER
      .Column(GRID_COLUMN_USER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_USER_NAME).Header(0).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Movement Type Id
      .Column(GRID_COLUMN_TYPE_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_TYPE_ID).Width = 0
      .Column(GRID_COLUMN_TYPE_ID).IsColumnPrintable = False

      ' Movement Type Name
      .Column(GRID_COLUMN_TYPE_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3391)
      .Column(GRID_COLUMN_TYPE_NAME).Width = GRID_WIDTH_TYPE_NAME
      .Column(GRID_COLUMN_TYPE_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_TYPE_NAME).Header(0).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Account Id
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_ACCOUNT_ID).Width = GRID_WIDTH_ACCOUNT_ID
      .Column(GRID_COLUMN_ACCOUNT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(0).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' ISO_CODE
      .Column(GRID_COLUMN_ISO_CODE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2430) ' "Divisa"
      .Column(GRID_COLUMN_ISO_CODE).Width = GRID_WIDTH_ISO_CODE
      .Column(GRID_COLUMN_ISO_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_ISO_CODE).Header(0).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      Dim _is_cage_active As Boolean
      _is_cage_active = Cage.IsCageEnabled

      ' DENOMINATION
      .Column(GRID_COLUMN_DENOMINATION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2270)  '"Denominaci�n"
      If _is_cage_active Then
        .Column(GRID_COLUMN_DENOMINATION).Width = GRID_WIDTH_DENOMINATION
      Else
        .Column(GRID_COLUMN_DENOMINATION).Width = 0
      End If

      .Column(GRID_COLUMN_DENOMINATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_DENOMINATION).Header(0).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      'CURRENCY TYPES
      .Column(GRID_COLUMN_CURRENCY_TYPE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5678)
      .Column(GRID_COLUMN_CURRENCY_TYPE).Width = GRID_WIDTH_CURRENCY_TYPE
      .Column(GRID_COLUMN_CURRENCY_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_CURRENCY_TYPE).Header(0).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' UNITS 
      .Column(GRID_COLUMN_UNITS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3302)  '"Qty."
      If _is_cage_active Then
        .Column(GRID_COLUMN_UNITS).Width = GRID_WIDTH_UNITS
      Else
        .Column(GRID_COLUMN_UNITS).Width = 0
      End If
      .Column(GRID_COLUMN_UNITS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_UNITS).Header(0).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' TOTAL
      .Column(GRID_COLUMN_TOTAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2271)  '"Total"
      .Column(GRID_COLUMN_TOTAL).Width = GRID_WIDTH_CURRENCY_EXCHANGE 'GRID_WIDTH_AMOUNT_SHORT
      .Column(GRID_COLUMN_TOTAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_TOTAL).Header(0).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Sub amount
      .Column(GRID_COLUMN_SUB_AMOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(214)
      .Column(GRID_COLUMN_SUB_AMOUNT).Width = GRID_WIDTH_CURRENCY_EXCHANGE 'GRID_WIDTH_AMOUNT_SHORT
      .Column(GRID_COLUMN_SUB_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_SUB_AMOUNT).Header(0).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Add amount
      .Column(GRID_COLUMN_ADD_AMOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(215)
      .Column(GRID_COLUMN_ADD_AMOUNT).Width = GRID_WIDTH_CURRENCY_EXCHANGE 'GRID_WIDTH_AMOUNT_SHORT
      .Column(GRID_COLUMN_ADD_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_ADD_AMOUNT).Header(0).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

  End Sub 'GUI_StyleSheet

  ' PURPOSE: Obtain the list of elements that will populate the 
  '         movement types filter grid.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '     - List of elements to be added to the grid
  Private Function GetMovTypesFilterGridData() As List(Of TYPE_DG_FILTER_ELEMENT)

    ' Dimensioned this way to avoid getting an extra element: VB uses the constant to set the upper value, 
    ' not the number of elements
    Dim _dg_filter_rows_list As List(Of TYPE_DG_FILTER_ELEMENT)
    Dim _row As TYPE_DG_FILTER_ELEMENT
    Dim _is_cage_enabled As Boolean
    Dim _split_data As New WSI.Common.TYPE_SPLITS

    _dg_filter_rows_list = New List(Of TYPE_DG_FILTER_ELEMENT)

    _is_cage_enabled = Cage.IsCageEnabled

    _split_data.enabled_b = False
    WSI.Common.Split.ReadSplitParameters(_split_data)

    ' * CASHIER *
    ' Header row - Cashier
    _row.code = 0
    _row.description = GLB_NLS_GUI_INVOICING.GetString(225)
    _row.elem_type = GRID_2_ROW_TYPE_HEADER
    _dg_filter_rows_list.Add(_row)

    If Not m_only_show_chips_movements Then
      _row.code = WSI.Common.CASHIER_MOVEMENT.OPEN_SESSION
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      If _is_cage_enabled Then
        _row.code = WSI.Common.CASHIER_MOVEMENT.CAGE_OPEN_SESSION
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)
      End If

      _row.code = WSI.Common.CASHIER_MOVEMENT.CLOSE_SESSION
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      If _is_cage_enabled Then
        _row.code = WSI.Common.CASHIER_MOVEMENT.CAGE_CLOSE_SESSION
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)
      End If

      ' DHA 10-OCT-2014: added new movement (158) if there is cash short on closing cashier session
      _row.code = WSI.Common.CASHIER_MOVEMENT.CASH_CLOSING_SHORT
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      ' DHA 10-OCT-2014: added new movement (159) if there is cash over on closing cashier session
      _row.code = WSI.Common.CASHIER_MOVEMENT.CASH_CLOSING_OVER
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      ' QMP 02-MAY-2013: Cashier session pending closing
      _row.code = WSI.Common.CASHIER_MOVEMENT.CASH_PENDING_CLOSING
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.FILLER_IN
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      If _is_cage_enabled Then
        _row.code = WSI.Common.CASHIER_MOVEMENT.CAGE_FILLER_IN
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)
      End If

      _row.code = WSI.Common.CASHIER_MOVEMENT.FILLER_OUT
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      If _is_cage_enabled Then
        _row.code = WSI.Common.CASHIER_MOVEMENT.CAGE_FILLER_OUT
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)
      End If

      ' DHA 17-06-2016
      If GamingTableBusinessLogic.IsEnabledGTPlayerTracking() Then
        _row.code = WSI.Common.CASHIER_MOVEMENT.FILLER_IN_CLOSING_STOCK
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        _row.code = WSI.Common.CASHIER_MOVEMENT.FILLER_OUT_CLOSING_STOCK
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)
      End If

      ' MBF 02-DEC-2009 - Limit extension movement should not be shown
      ' JCM 08-AUG-2012 - "Limit extension movement MANUALS " should be shown
      'Mobile(Bank)
      _row.code = WSI.Common.CASHIER_MOVEMENT.MB_MANUAL_CHANGE_LIMIT
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.MB_CASH_IN
      If m_is_tito_mode Then
        _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4586)
      Else
        _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(730)
      End If
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.MB_CASH_LOST
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      ' OPC 3-NOV-2014
      _row.code = WSI.Common.CASHIER_MOVEMENT.MB_EXCESS
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.REQUEST_TO_VOUCHER_REPRINT
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CHECK_PAYMENT
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      '' NMR 28-AUG-2013 Cashier TITO movements
      If m_is_tito_mode Then
        _row.code = WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PRINTED_CASHABLE
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        _row.code = WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PRINTED_PROMO_REDEEMABLE
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        _row.code = WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PRINTED_PROMO_NOT_REDEEMABLE
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        ' FAV 03-DIC-2015
        _row.code = WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_PROMOBOX_PRINTED_PROMO_REDEEMABLE
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        ' FAV 20-NOV-2015
        _row.code = WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_PROMOBOX_PRINTED_PROMO_NOT_REDEEMABLE
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        _row.code = WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PRINTED_CASHABLE
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        _row.code = WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PRINTED_PROMO_NOT_REDEEMABLE
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        _row.code = WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PAID_CASHABLE
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        _row.code = WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PAID_PROMO_REDEEMABLE
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        _row.code = WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PLAYED_CASHABLE
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        _row.code = WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PLAYED_PROMO_REDEEMABLE
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        _row.code = WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PLAYED_PROMO_NOT_REDEEMABLE
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        _row.code = WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PAID_HANDPAY
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        _row.code = WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PAID_JACKPOT
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        _row.code = WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_REISSUE
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        ' JPJ 22-JAN-2014
        _row.code = WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_EXPIRED_REDEEMABLE
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        _row.code = WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_EXPIRED_PROMO_REDEEMABLE
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        _row.code = WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_EXPIRED_PROMO_NOT_REDEEMABLE
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        ' DCS 14-JUL-2015: Add expired TITO movements  
        _row.code = WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_CASHIER_EXPIRED_PAID_CASHABLE
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        _row.code = WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_CASHIER_EXPIRED_PAID_PROMO_REDEEMABLE
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        _row.code = WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_CASHIER_EXPIRED_PAID_JACKPOT
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        _row.code = WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_CASHIER_EXPIRED_PAID_HANDPAY
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        _row.code = WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PRINTED_HANDPAY
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        _row.code = WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PRINTED_JACKPOT
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        _row.code = WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_CREATE_CASHABLE_OFFLINE
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        _row.code = WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_CREATE_PLAYABLE_OFFLINE
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        _row.code = WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_CASHABLE_REDEEMED_OFFLINE
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        _row.code = WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_PLAYABLE_REDEEMED_OFFLINE
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        _row.code = WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_CASHIER_VOID_CASHABLE
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        _row.code = WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_CASHIER_VOID_PROMO_REDEEMABLE
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        _row.code = WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_CASHIER_VOID_PROMO_NOT_REDEEMABLE
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        _row.code = WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_MACHINE_VOID_CASHABLE
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        _row.code = WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_MACHINE_VOID_PROMO_NOT_REDEEMABLE
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        _row.code = WSI.Common.CASHIER_MOVEMENT.TITO_VALIDATE_TICKET
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        ' DHA 27-JAN-2016: added new movements for dual currency
        _row.code = WSI.Common.CASHIER_MOVEMENT.TITO_TICKETS_CREATE_EXCHANGE_DUAL_CURRENCY
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        _row.code = WSI.Common.CASHIER_MOVEMENT.TITO_TICKETS_PLAYED_EXCHANGE_DUAL_CURRENCY
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        'EOR 14-MAR-2016
        If GeneralParam.GetInt32("TITA", "Enabled") <> 0 Then
          _row.code = WSI.Common.CASHIER_MOVEMENT.TITO_TICKET_CASHIER_SWAP_FOR_CHIPS
          _row.description = TranslateMovementType(_row.code)
          _row.elem_type = GRID_2_ROW_TYPE_DATA
          _dg_filter_rows_list.Add(_row)
        End If

      End If

      ' ICS 17-SEP-2013: New movement for Closing Cash with Bank Card
      _row.code = WSI.Common.CASHIER_MOVEMENT.CLOSE_SESSION_BANK_CARD
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      ' ICS 17-SEP-2013: New movement for Closing Cash with Checks
      _row.code = WSI.Common.CASHIER_MOVEMENT.CLOSE_SESSION_CHECK
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      ' ICS 17-SEP-2013: New movement for Filler Out with Checks
      _row.code = WSI.Common.CASHIER_MOVEMENT.FILLER_OUT_CHECK
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      ' RCI & DRV 08-JUL-2014: For now, don't use special movements for Integrated chip operations (Palacio Shortcut).
      '                        USe them if the AC_INITIAL_CASH_IN_FOR_CHIPS is enabled in the future.
      '
      'HBB 29-MAY-2014
      '_row.code = WSI.Common.CASHIER_MOVEMENT.CHIPS_SALE_WITH_CASH_IN
      '_row.description = TranslateMovementType(_row.code)
      '_row.elem_type = GRID_2_ROW_TYPE_DATA
      '_dg_filter_rows_list.Add(_row)

      '_row.code = WSI.Common.CASHIER_MOVEMENT.CHIPS_PURCHASE_WITH_CASH_OUT
      '_row.description = TranslateMovementType(_row.code)
      '_row.elem_type = GRID_2_ROW_TYPE_DATA
      '_dg_filter_rows_list.Add(_row)

    End If

    If GamingTableBusinessLogic.IsGamingTablesEnabled Then
      _row.code = WSI.Common.CASHIER_MOVEMENT.CHIPS_CHANGE_IN
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CHIPS_CHANGE_OUT
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.GAMING_TABLE_GAME_NETWIN
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.GAMING_TABLE_GAME_NETWIN_CLOSE
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      ' DHA 13-MAY-2016
      _row.code = WSI.Common.CASHIER_MOVEMENT.CAGE_CASHIER_COLLECT_DROPBOX_GAMINGTABLE
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      ' DHA 26-JUL-2016
      _row.code = WSI.Common.CASHIER_MOVEMENT.CAGE_CASHIER_COLLECT_DROPBOX_GAMINGTABLE_DETAIL
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

    End If

    ' AMF 15-SEP-2014
    _row.code = WSI.Common.CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_IN
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_filter_rows_list.Add(_row)

    _row.code = WSI.Common.CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_OUT
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_filter_rows_list.Add(_row)

    ' FJC 29-ARP-2015 
    ' WinLossSatetement Request
    _row.code = WSI.Common.CASHIER_MOVEMENT.WIN_LOSS_STATEMENT_REQUEST
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_filter_rows_list.Add(_row)

    'ETP Add multicurrency movements to type filter.
    _row.code = WSI.Common.CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_NATIONAL_CURRENCY
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_filter_rows_list.Add(_row)

    _row.code = WSI.Common.CASHIER_MOVEMENT.CURRENCY_EXCHANGE_AMOUNT_OUT
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_filter_rows_list.Add(_row)

    _row.code = WSI.Common.CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_FOREIGN_CURRENCY
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_filter_rows_list.Add(_row)

    _row.code = WSI.Common.CASHIER_MOVEMENT.CURRENCY_EXCHANGE_AMOUNT_IN
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_filter_rows_list.Add(_row)

    _row.code = WSI.Common.CASHIER_MOVEMENT.CURRENCY_EXCHANGE_AMOUNT_IN_CHANGE
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_filter_rows_list.Add(_row)

    ' * CARD *
    ' Header row - Card
    _row.code = 0
    _row.description = GLB_NLS_GUI_INVOICING.GetString(226)
    _row.elem_type = GRID_2_ROW_TYPE_HEADER
    _dg_filter_rows_list.Add(_row)

    If Not m_only_show_chips_movements Then
      _row.code = WSI.Common.CASHIER_MOVEMENT.CASH_IN
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CASH_IN_SPLIT1
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      If GeneralParam.GetBoolean("Cashier", "CashInTax.Enabled") Then
        _row.code = WSI.Common.CASHIER_MOVEMENT.CASH_IN_TAX_SPLIT1
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)
      End If

      If GeneralParam.GetBoolean("Cashier", "CashInTax.AlsoApplyToSplitB", False) Then
        _row.code = WSI.Common.CASHIER_MOVEMENT.CASH_IN_TAX_SPLIT2
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)
      End If

      If WSI.Common.Misc.IsTaxCustodyEnabled() Then
        _row.code = WSI.Common.CASHIER_MOVEMENT.TAX_CUSTODY
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)
      End If

      If WSI.Common.Misc.IsTaxCustodyEnabled() Then
        _row.code = WSI.Common.CASHIER_MOVEMENT.TAX_RETURNING_CUSTODY
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)
      End If

      ' DHA 02-MAR-2016 TAx Provisions
      If WSI.Common.Misc.IsTaxProvisionsEnabled() Then
        _row.code = WSI.Common.CASHIER_MOVEMENT.TAX_PROVISIONS
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)
      End If

      If _split_data.enabled_b Then
        _row.code = WSI.Common.CASHIER_MOVEMENT.CASH_IN_SPLIT2
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)
      End If

      _row.code = WSI.Common.CASHIER_MOVEMENT.PRIZE_COUPON
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CASH_OUT
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.DEV_SPLIT1
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      If _split_data.enabled_b Then
        _row.code = WSI.Common.CASHIER_MOVEMENT.DEV_SPLIT2
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)
      End If

      _row.code = WSI.Common.CASHIER_MOVEMENT.CANCEL_SPLIT1
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      If _split_data.enabled_b Then
        _row.code = WSI.Common.CASHIER_MOVEMENT.CANCEL_SPLIT2
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)
      End If

      _row.code = WSI.Common.CASHIER_MOVEMENT.TAX_ON_PRIZE1
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.TAX_ON_PRIZE2
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.TAX_ON_PRIZE3
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.PRIZES
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      If (WSI.Common.Misc.IsVoucherModeTV()) Then
        _row.code = WSI.Common.CASHIER_MOVEMENT.PRIZE_DRAW_GROSS_OTHER
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)
      End If

      _row.code = WSI.Common.CASHIER_MOVEMENT.PROMOTION_NOT_REDEEMABLE
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      'DDM 06-JUL-2012
      _row.code = WSI.Common.CASHIER_MOVEMENT.PROMOTION_REDEEMABLE
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      'DDM 31-JUL-2012
      _row.code = WSI.Common.CASHIER_MOVEMENT.PROMOTION_POINT
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CANCEL_PROMOTION_POINT
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.REDEEMABLE_DEV_EXPIRED
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.REDEEMABLE_PRIZE_EXPIRED
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.NOT_REDEEMABLE_EXPIRED
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CASH_IN_COVER_COUPON
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CANCEL_NOT_REDEEMABLE
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CANCEL_PROMOTION_REDEEMABLE
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CARD_DEPOSIT_IN
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CARD_DEPOSIT_OUT
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_FOR_FREE
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CHECK
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_CREDIT
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_DEBIT
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_GENERIC
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      '_row.code = WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_IN_POINTS
      '_row.description = TranslateMovementType(_row.code)
      '_row.elem_type = GRID_2_ROW_TYPE_DATA
      '_dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.DRAW_TICKET_PRINT
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.POINTS_TO_DRAW_TICKET_PRINT
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.HANDPAY
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.HANDPAY_CANCELLATION
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.MANUAL_HANDPAY
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.MANUAL_HANDPAY_CANCELLATION
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      ' OPC 02-DEC-2014
      _row.code = WSI.Common.CASHIER_MOVEMENT.HANDPAY_VOIDED
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.HANDPAY_VOIDED_UNDONE
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.HANDPAY_AUTHORIZED
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.HANDPAY_UNAUTHORIZED
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.HANDPAY_WITHHOLDING
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.PROMO_CREDITS
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.PROMO_TO_REDEEMABLE
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.PROMO_EXPIRED
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.PROMO_CANCEL
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.PROMO_START_SESSION
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.PROMO_END_SESSION
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CARD_CREATION
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CHECK
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CURRENCY_EXCHANGE
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_CREDIT
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_DEBIT
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_GENERIC
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CARD_PERSONALIZATION
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.ACCOUNT_PIN_CHANGE
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.ACCOUNT_PIN_RANDOM
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CARD_RECYCLED
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      ' ACM 09-MAY-2013: Cashier movements - Blocked/Unblocked accounts
      _row.code = WSI.Common.CASHIER_MOVEMENT.ACCOUNT_BLOCKED
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.ACCOUNT_UNBLOCKED
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      'FOS 16-FEB-2015
      If Not m_is_tito_mode Then
        _row.code = WSI.Common.CASHIER_MOVEMENT.MB_CASH_IN_SPLIT1
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)
      End If

      If _split_data.enabled_b Then
        _row.code = WSI.Common.CASHIER_MOVEMENT.MB_CASH_IN_SPLIT2
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)
      End If

      If Not m_is_tito_mode Then
        _row.code = WSI.Common.CASHIER_MOVEMENT.MB_CASH_IN_NOT_REDEEMABLE
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        _row.code = WSI.Common.CASHIER_MOVEMENT.MB_CASH_IN_COVER_COUPON
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)

        _row.code = WSI.Common.CASHIER_MOVEMENT.MB_PRIZE_COUPON
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)
      End If

      'SSC 08-MAR-2012
      _row.code = WSI.Common.CASHIER_MOVEMENT.NA_CASH_IN_SPLIT1
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      If _split_data.enabled_b Then
        _row.code = WSI.Common.CASHIER_MOVEMENT.NA_CASH_IN_SPLIT2
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)
      End If

      _row.code = WSI.Common.CASHIER_MOVEMENT.NA_CASH_IN_NOT_REDEEMABLE
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.NA_CASH_IN_COVER_COUPON
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.NA_PRIZE_COUPON
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      'SSC 13-JUN-2012: Gift Request
      _row.code = WSI.Common.CASHIER_MOVEMENT.POINTS_TO_NOT_REDEEMABLE
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.TAX_RETURNING_ON_PRIZE_COUPON
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.DECIMAL_ROUNDING
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.SERVICE_CHARGE
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT1
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT1
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      ' FBA 26-AUG-2013: New movement (85) Recharge for points
      _row.code = WSI.Common.CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_SPLIT1
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      If _split_data.enabled_b Then
        ' FBA 26-AUG-2013: New movement (86) Recharge for points - split 2
        _row.code = WSI.Common.CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_SPLIT2
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)
      End If

      ' DHA 29-MAY-2014: New movement (143) Cashier movement for Points to redeemable as cashin prize
      _row.code = WSI.Common.CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_PRIZE
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      ' SMN 06-SEP-2013: New movement (91) Cashier movement for Participation in draw
      _row.code = WSI.Common.CASHIER_MOVEMENT.PARTICIPATION_IN_CASHDESK_DRAW
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      ' DHA 10-SEP-2013: New movement (94) Cashier movement for Prize in kind1 gross
      _row.code = WSI.Common.CASHIER_MOVEMENT.PRIZE_IN_KIND1_GROSS
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      ' DHA 10-SEP-2013: New movement (95) Cashier movement for Prize in kind1 federal tax
      _row.code = WSI.Common.CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX1
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      ' DHA 10-SEP-2013: New movement (96) Cashier movement for Prize in kind1 state tax
      _row.code = WSI.Common.CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX2
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      ' JBP 17-SEP-2013: New movement (100) Cashier movement for Prize in kind2 gross
      _row.code = WSI.Common.CASHIER_MOVEMENT.PRIZE_IN_KIND2_GROSS
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      ' JBP 17-SEP-2013: New movement (101) Cashier movement for Prize in kind2 federal tax
      _row.code = WSI.Common.CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX1
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      ' JBP 17-SEP-2013: New movement (102) Cashier movement for Prize in kind2 state tax
      _row.code = WSI.Common.CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX2
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      ' LA 24-FEB-2016: New movement (566) Cashier movement for Prize in kind1 RE gross
      _row.code = WSI.Common.CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_GROSS
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      ' LA 24-FEB-2016: New movement (568) Cashier movement for Prize in kind1 RE federal tax
      _row.code = WSI.Common.CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX1
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      ' LA 24-FEB-2016: New movement (569) Cashier movement for Prize in kind1 RE state tax
      _row.code = WSI.Common.CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX2
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      ' LA 24-FEB-2016:: New movement (567) Cashier movement for Prize in kind2 RE gross
      _row.code = WSI.Common.CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_GROSS
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      ' LA 24-FEB-2016: New movement (570) Cashier movement for Prize in kind2 RE federal tax
      _row.code = WSI.Common.CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX1
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      ' LA 24-FEB-2016: New movement (571) Cashier movement for Prize in kind2 RE state tax
      _row.code = WSI.Common.CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX2
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      ' LA 26-FEB-2016: New movement (1600) Cashier movement for Promotion RE federal tax
      _row.code = WSI.Common.CASHIER_MOVEMENT.PROMOTION_REDEEMABLE_FEDERAL_TAX
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      ' LA 26-FEB-2016: New movement (1601) Cashier movement for Promotion RE state tax
      _row.code = WSI.Common.CASHIER_MOVEMENT.PROMOTION_REDEEMABLE_STATE_TAX
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      ' DLL 16-SEP-2014: New movement (102) Cashier movement for Prize in kind2 state tax
      _row.code = WSI.Common.CASHIER_MOVEMENT.CASH_ADVANCE_CASH
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CASH_ADVANCE_CHECK
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CASH_ADVANCE_CREDIT_BANK_CARD
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CASH_ADVANCE_DEBIT_BANK_CARD
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CASH_ADVANCE_GENERIC_BANK_CARD
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CASH_ADVANCE_CURRENCY_EXCHANGE
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)
    End If

    ' LJM 13-DEC-2013 Chips movements
    ' ACM 07-JAN-2013 Add chips movements only when gaming tables are enabled.
    If GamingTableBusinessLogic.IsGamingTablesEnabled Then
      _row.code = WSI.Common.CASHIER_MOVEMENT.CHIPS_PURCHASE
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CHIPS_SALE
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CHIPS_SALE_TOTAL
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CHIPS_SALE_REMAINING_AMOUNT
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CHIPS_SALE_CONSUMED_REMAINING_AMOUNT
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CHIPS_SALE_DEVOLUTION_FOR_TITO
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      If GeneralParam.GetBoolean("GamingTables", "Cashier.RegisterChipSaleAtTableEnabled") Then

        _row.code = WSI.Common.CASHIER_MOVEMENT.CHIPS_SALE_REGISTER_TOTAL
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        _dg_filter_rows_list.Add(_row)
      End If
    End If

    If Not m_only_show_chips_movements Then
      ' RMS 21-JAN-2014
      _row.code = WSI.Common.CASHIER_MOVEMENT.TRANSFER_CREDIT_IN
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.TRANSFER_CREDIT_OUT
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_filter_rows_list.Add(_row)
    End If

    ' DHA 08-OCT-2014: New movement (157) advice if a recharge is going to refund without plays
    _row.code = WSI.Common.CASHIER_MOVEMENT.NOT_PLAYED_RECHARGE_REFUNDED
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_filter_rows_list.Add(_row)

    ' DHA 27-JAN-2016: added new movements for dual currency
    _row.code = WSI.Common.CASHIER_MOVEMENT.BILL_IN_EXCHANGE_DUAL_CURRENCY
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_filter_rows_list.Add(_row)


    '28/01/2016 BUCKETS: 
    If (GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode", 0) = 0) Then 'Buckets SPACE	
      MovTypesFilterGridAddBuckets(_dg_filter_rows_list)
    End If

    ' AMF 22-MAR-2017: Credit Lines
    MovTypesFilterGridAddCreditLine(_dg_filter_rows_list)

    _dg_filter_rows_list = MovTypeFilterGridAddReception(_dg_filter_rows_list)

    Return _dg_filter_rows_list

  End Function

  Private Function MovTypeFilterGridAddReception(DgFilterRows As List(Of TYPE_DG_FILTER_ELEMENT)) As List(Of TYPE_DG_FILTER_ELEMENT)
    Dim _row_header As TYPE_DG_FILTER_ELEMENT
    Dim _row As TYPE_DG_FILTER_ELEMENT

    If WSI.Common.Misc.IsReceptionEnabled() Then
      _row_header.code = -1
      _row_header.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7036)
      _row_header.elem_type = GRID_2_ROW_TYPE_HEADER
      DgFilterRows.Add(_row_header)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CUSTOMER_ENTRANCE
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_CARD
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_CHECK
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

      _row.code = WSI.Common.CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_CREDIT_CARD
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)


      _row.code = WSI.Common.CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_DEBIT_CARD
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)
    End If

    Return DgFilterRows

  End Function
  ' GetMovTypesFilterGridData


  ' PURPOSE: Add Buckets movement types filter grid.
  '
  '  PARAMS:
  '     - INPUT: DgFilterRows As List(Of TYPE_DG_FILTER_ELEMENT)
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '     - 
  Private Sub MovTypesFilterGridAddBuckets(ByRef DgFilterRows As List(Of TYPE_DG_FILTER_ELEMENT))
    Dim _row_header As TYPE_DG_FILTER_ELEMENT
    Dim _row As TYPE_DG_FILTER_ELEMENT

    Dim _id As Integer
    Dim _buckets_forms As BucketsForm

    _buckets_forms = New BucketsForm()
    ' DOING

    Dim _buckets_id As List(Of Long)
    _buckets_id = New List(Of Long)

    _buckets_forms.GetBucketsID(_buckets_id)

    If (_buckets_id.Count > 0) Then

      _row_header.code = -1
      _row_header.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6968)
      _row_header.elem_type = GRID_2_ROW_TYPE_HEADER
      DgFilterRows.Add(_row_header)

      ' buckets
      For Each _id In _buckets_id
        _row.code = CASHIER_MOVEMENT.MULTIPLE_BUCKETS_END_SESSION + _id
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        DgFilterRows.Add(_row)

        _row.code = CASHIER_MOVEMENT.MULTIPLE_BUCKETS_EXPIRED + _id
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        DgFilterRows.Add(_row)

        _row.code = CASHIER_MOVEMENT.MULTIPLE_BUCKETS_MANUAL_ADD + _id
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        DgFilterRows.Add(_row)

        _row.code = CASHIER_MOVEMENT.MULTIPLE_BUCKETS_MANUAL_SUB + _id
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        DgFilterRows.Add(_row)

        _row.code = CASHIER_MOVEMENT.MULTIPLE_BUCKETS_MANUAL_SET + _id
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        DgFilterRows.Add(_row)
      Next
    End If
  End Sub

  ''' <summary>
  ''' Add Credit line movements to filter
  ''' </summary>
  ''' <param name="DgFilterRows"></param>
  ''' <remarks></remarks>
  Private Sub MovTypesFilterGridAddCreditLine(ByRef DgFilterRows As List(Of TYPE_DG_FILTER_ELEMENT))
    Dim _row As TYPE_DG_FILTER_ELEMENT

    If GeneralParam.GetBoolean("CreditLine", "Enabled", False) Then

      _row.code = -1
      _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7947)
      _row.elem_type = GRID_2_ROW_TYPE_HEADER
      DgFilterRows.Add(_row)

      _row.code = CASHIER_MOVEMENT.CREDIT_LINE_MARKER
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

      _row.code = CASHIER_MOVEMENT.CREDIT_LINE_MARKER_CARD_REPLACEMENT
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

      _row.code = CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

      _row.code = CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CARD_EXCHANGE
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

      _row.code = CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CREDIT_CARD_EXCHANGE
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

      _row.code = CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_DEBIT_CARD_EXCHANGE
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

      _row.code = CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CHECK_EXCHANGE
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

    End If

  End Sub ' MovTypesFilterGridAddCreditLine

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub SetDefaultValues()

    Dim _closing_time As Integer
    Dim _initial_time As Date
    Dim _now As Date

    _now = WGDB.Now

    _closing_time = GetDefaultClosingTime()
    _initial_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)

    If _initial_time > _now Then
      _initial_time = _initial_time.AddDays(-1)
    End If

    If Me.bl_sessions Then
      Me.tf_cm_cash_desk_session.Visible = True
      Me.tf_cm_cash_desk_session.Value = m_session_name

      If m_init_date_from <> Nothing Then
        Me.dtp_from.Value = m_init_date_from
        Me.dtp_from.Checked = True
      Else
        Me.dtp_from.Value = _initial_time
        Me.dtp_from.Checked = False
      End If

      If m_init_date_to <> Nothing Then
        Me.dtp_to.Value = m_init_date_to
        Me.dtp_to.Checked = True
      Else
        Me.dtp_to.Value = _initial_time.AddDays(1)
        Me.dtp_to.Checked = False
      End If
    Else
      Me.dtp_from.Value = _initial_time
      Me.dtp_from.Checked = True ' SLE 11-MAY-2010
      Me.dtp_to.Value = _initial_time.AddDays(1)
      Me.dtp_to.Checked = False
      Me.tf_cm_cash_desk_session.Visible = False
      Me.tf_cm_cash_desk_session.Value = ""
    End If

    Me.opt_all_cashiers.Checked = True
    Me.cmb_cashier.Enabled = False

    Me.opt_all_users.Checked = True
    Me.cmb_user.Enabled = False
    Me.chk_show_all.Checked = False
    Me.chk_show_all.Enabled = False

    ' Account filter
    Me.Uc_account_sel1.Clear()

    ' System CheckBox
    If (tf_cm_cash_desk_session.Value <> "") Then
      Me.chk_not_show_system_sessions.Checked = False
      Me.chk_not_show_system_sessions.Enabled = False
    Else
      Me.chk_not_show_system_sessions.Checked = False
      Me.chk_not_show_system_sessions.Enabled = True
    End If

    ' Movement Types data grid
    Call InitMovementTypesFilter()

  End Sub 'SetDefaultValues

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the main SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Function GetSqlWhere() As String
    Dim str_where As String = ""

    ' Filter Dates
    If Me.dtp_from.Checked = True Then
      str_where = str_where & " AND (CM_DATE >= " & GUI_FormatDateDB(dtp_from.Value) & ") "
    End If

    If Me.dtp_to.Checked = True Then
      str_where = str_where & " AND (CM_DATE < " & GUI_FormatDateDB(dtp_to.Value) & ") "
    End If

    ' Filter Cashier
    If Me.opt_one_cashier.Checked = True Then
      str_where = str_where & " AND (CM_CASHIER_ID = " & Me.cmb_cashier.Value & ") "
    End If

    ' Filter User
    If Me.opt_one_user.Checked = True Then
      str_where = str_where & " AND (CM_USER_ID = " & Me.cmb_user.Value & ") "
      ' RRB 12-NOV-2012: Exclude MB and NA movements
      str_where = str_where & " AND (CM_TYPE NOT IN ( " & WSI.Common.CASHIER_MOVEMENT.MB_CASH_IN_SPLIT1 & _
                                                   ", " & WSI.Common.CASHIER_MOVEMENT.MB_CASH_IN_SPLIT2 & _
                                                   ", " & WSI.Common.CASHIER_MOVEMENT.MB_CASH_IN_NOT_REDEEMABLE & _
                                                   ", " & WSI.Common.CASHIER_MOVEMENT.MB_CASH_IN_COVER_COUPON & _
                                                   ", " & WSI.Common.CASHIER_MOVEMENT.MB_PRIZE_COUPON & _
                                                   ", " & WSI.Common.CASHIER_MOVEMENT.NA_CASH_IN_SPLIT1 & _
                                                   ", " & WSI.Common.CASHIER_MOVEMENT.NA_CASH_IN_SPLIT2 & _
                                                   ", " & WSI.Common.CASHIER_MOVEMENT.NA_CASH_IN_NOT_REDEEMABLE & _
                                                   ", " & WSI.Common.CASHIER_MOVEMENT.NA_CASH_IN_COVER_COUPON & _
                                                   ", " & WSI.Common.CASHIER_MOVEMENT.NA_PRIZE_COUPON & ")) "
    End If

    ' Filter Session Id
    If Me.bl_sessions = True Then
      str_where = str_where & " AND (CM_SESSION_ID = " & m_session_id & ") "
    End If

    ' Filter Account
    If Not String.IsNullOrEmpty(Uc_account_sel1.GetFilterSQL()) Then
      str_where = str_where & " AND " & Uc_account_sel1.GetFilterSQL()
    End If

    ' Checkbox System
    If chk_not_show_system_sessions.Checked Then
      str_where = str_where & " AND CS_USER_ID NOT IN (SELECT   GU_USER_ID " _
                                                    & "  FROM   GUI_USERS " _
                                                    & " WHERE   GU_USER_TYPE IN (" & WSI.Common.Misc.SystemUsersListListToString() & "))"
    End If

    ' Filter Movement type
    str_where = str_where & GetSqlWhereMovType("CM_TYPE")

    Return str_where
  End Function ' GetSqlWhere

#Region " Movement Types "

  ' PURPOSE: Initialize the movement types filter grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub InitMovementTypesFilter()

    Me.opt_all_types.Checked = True
    Me.dg_filter.Enabled = True

    ' Reset dg selections
    Call FillMovTypesFilterGrid(GetMovTypesFilterGridData())

  End Sub ' InitMovementTypesFilter

  ' PURPOSE: Define the layout of the movement types grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub GUI_StyleSheetMovementTypes()
    With Me.dg_filter
      Call .Init(GRID_2_COLUMNS, GRID_2_HEADER_ROWS, True)
      .Counter(0).Visible = False
      .Sortable = False

      ' Hidden columns
      .Column(GRID_2_COLUMN_CODE).Header.Text = ""
      .Column(GRID_2_COLUMN_CODE).WidthFixed = 0

      ' GRID_COL_CHECKBOX
      .Column(GRID_2_COLUMN_CHECKED).Header.Text = ""
      .Column(GRID_2_COLUMN_CHECKED).WidthFixed = 400
      .Column(GRID_2_COLUMN_CHECKED).Fixed = True
      .Column(GRID_2_COLUMN_CHECKED).Editable = True
      .Column(GRID_2_COLUMN_CHECKED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX

      ' GRID_COL_DESCRIPTION
      .Column(GRID_2_COLUMN_DESC).Header.Text = ""
      .Column(GRID_2_COLUMN_DESC).WidthFixed = 4105
      .Column(GRID_2_COLUMN_DESC).Fixed = True
      .Column(GRID_2_COLUMN_DESC).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' GRID_COL_TYPE
      .Column(GRID_2_COLUMN_ROW_TYPE).Header.Text = ""
      .Column(GRID_2_COLUMN_ROW_TYPE).WidthFixed = 0
    End With

  End Sub 'GUI_StyleSheetMovementTypes

  ' PURPOSE: Add a new data row the data grid filter
  '
  '  PARAMS:
  '     - INPUT:
  '           - TypeCode: numeric identifier
  '           - TypeDesc: textual description
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub AddOneRowData(ByVal TypeCode As Integer, ByVal TypeDesc As String)
    Dim prev_redraw As Boolean

    prev_redraw = Me.dg_filter.Redraw
    Me.dg_filter.Redraw = False

    dg_filter.AddRow()

    Me.dg_filter.Redraw = False

    dg_filter.Cell(dg_filter.NumRows - 1, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    If m_only_show_chips_movements Then
      dg_filter.Cell(dg_filter.NumRows - 1, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
    End If

    dg_filter.Cell(dg_filter.NumRows - 1, GRID_2_COLUMN_CODE).Value = TypeCode
    dg_filter.Cell(dg_filter.NumRows - 1, GRID_2_COLUMN_DESC).Value = GRID_2_TAB & TypeDesc
    dg_filter.Cell(dg_filter.NumRows - 1, GRID_2_COLUMN_ROW_TYPE).Value = GRID_2_ROW_TYPE_DATA

    Me.dg_filter.Row(dg_filter.NumRows - 1).Height = 0

    Me.dg_filter.Redraw = prev_redraw
  End Sub ' AddOneRowData

  ' PURPOSE: Add a new header row the data grid filter
  '
  '  PARAMS:
  '     - INPUT:
  '           - HeaderMsg: textual description
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub AddOneRowHeader(ByVal HeaderMsg As String)

    dg_filter.AddRow()

    dg_filter.Cell(dg_filter.NumRows - 1, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    dg_filter.Cell(dg_filter.NumRows - 1, GRID_2_COLUMN_DESC).Value = HeaderMsg
    dg_filter.Cell(dg_filter.NumRows - 1, GRID_2_COLUMN_ROW_TYPE).Value = GRID_2_ROW_TYPE_HEADER

    Call dg_filter.Row(dg_filter.NumRows - 1).SetSignalAllowExpand(GRID_2_COLUMN_DESC)
  End Sub ' AddOneRowHeader

  ' PURPOSE: Populate the movement types data grid filter.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Private Sub FillMovTypesFilterGrid(ByVal DgList As List(Of TYPE_DG_FILTER_ELEMENT))

    Me.dg_filter.Clear()
    Me.dg_filter.Redraw = False

    ' Get the filters array and insert it into the grid.

    For Each _element As TYPE_DG_FILTER_ELEMENT In DgList
      If (_element.elem_type = GRID_2_ROW_TYPE_DATA) Then
        Call AddOneRowData(_element.code, _element.description)
      Else
        If (_element.elem_type = GRID_2_ROW_TYPE_HEADER) Then
          Call AddOneRowHeader(_element.description)
        End If
      End If
    Next

    Me.dg_filter.Redraw = True

  End Sub ' FillMovTypesFilterGrid

  ' PURPOSE: Indicate if a given row in the filter data grid is a header or a data row
  '
  '  PARAMS:
  '     - INPUT:
  '           - RowIdx
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - True: specified row is a header row
  '     - False: specified row is a data row

  Private Function RowIsHeader(ByVal RowIdx As Integer) As Boolean

    If Me.dg_filter.Cell(RowIdx, GRID_2_COLUMN_ROW_TYPE).Value = GRID_2_ROW_TYPE_HEADER Then

      Return True
    Else

      Return False
    End If

  End Function ' RowIsHeader

  ' PURPOSE: Update the checkbox state for all row dependants (for header rows)
  '
  '  PARAMS:
  '     - INPUT:
  '           - FirstRow
  '           - ValueChecked
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub ChangeCheckofRows(ByVal FirstRow As Integer, ByVal ValueChecked As String)
    Dim idx As Integer

    For idx = FirstRow + 1 To dg_filter.NumRows - 1
      If Me.dg_filter.Cell(idx, GRID_2_COLUMN_ROW_TYPE).Value = GRID_2_ROW_TYPE_HEADER Then

        Exit Sub
      End If

      Me.dg_filter.Cell(idx, GRID_2_COLUMN_CHECKED).Value = ValueChecked

    Next

  End Sub ' ChangeCheckofRows

  ' PURPOSE: Update the check status of a header row when one of its dependant rows has been unchecked
  '
  '  PARAMS:
  '     - INPUT:
  '           - FirstRow
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub TryToUncheckHeader(ByVal FirstRow As Integer)
    Dim _idx As Integer
    Dim _idx_header As Integer
    Dim _idx_bottom_header As Integer
    Dim _checked As Boolean

    _idx_bottom_header = -1
    _idx_header = -1
    ' Go look for the header from current row upwards
    For _idx = FirstRow - 1 To 0 Step -1
      If RowIsHeader(_idx) And _idx_header = -1 Then
        _idx_header = _idx
      End If

      If Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED And _idx_header = -1 Then
        ' One or more upper rows are already unchecked
        _checked = True
      End If
    Next

    ' Go look for the bottom header from current row downwards
    For _idx = FirstRow + 1 To Me.dg_filter.NumRows - 1
      If RowIsHeader(_idx) And _idx_bottom_header = -1 Then
        _idx_bottom_header = _idx
      End If
      If _idx_bottom_header = -1 And Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        _checked = True
      End If
    Next

    'update de check status of the header row when some or all the dependant rows has been checked
    If Not _checked Then
      Me.dg_filter.Cell(_idx_header, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    Else
      Me.dg_filter.Cell(_idx_header, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
    End If

  End Sub ' TryToUncheckHeader
  ' PURPOSE: Update the check status of a header row when one of its dependant rows has been checked
  '
  '  PARAMS:
  '     - INPUT:
  '           - FirstRow
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub TryToCheckHeader(ByVal FirstRow As Integer)
    Dim _idx As Integer
    Dim _idx_header As Integer
    Dim _idx_bottom_header As Integer
    Dim _is_unchecked As Boolean

    _is_unchecked = False
    _idx_header = -1
    _idx_bottom_header = -1

    ' Go look for the header from current row upwards usually controls if any item is unchecked
    For _idx = FirstRow - 1 To 0 Step -1
      If RowIsHeader(_idx) And _idx_header = -1 Then
        _idx_header = _idx
      End If
      If _idx_header = -1 And Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED Then
        _is_unchecked = True
      End If
    Next

    ' Go look for the bottom header from current row downwards
    For _idx = FirstRow + 1 To Me.dg_filter.NumRows - 1
      If RowIsHeader(_idx) And _idx_bottom_header = -1 Then
        _idx_bottom_header = _idx
      End If
      If _idx_bottom_header = -1 And Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED Then
        _is_unchecked = True
      End If
    Next

    ' Updates the header check status if all rows are checked
    If _is_unchecked = False Then
      Me.dg_filter.Cell(_idx_header, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
    Else
      Me.dg_filter.Cell(_idx_header, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
    End If
  End Sub ' TryToCheckHeader

  ''' <summary>
  ''' Add movement type, if constains Added value. Used when we want to add a MovType only if another exists.
  ''' </summary>
  ''' <param name="ListMovementTypes"></param>
  ''' <param name="Added"></param>
  ''' <param name="ToAdd"></param>
  ''' <remarks></remarks>
  Private Sub AddMovementType(ByRef ListMovementTypes As List(Of CASHIER_MOVEMENT), Added As CASHIER_MOVEMENT, ToAdd As CASHIER_MOVEMENT)

    If ListMovementTypes.Contains(Added) Then
      ListMovementTypes.Add(ToAdd)
    End If
  End Sub

  ''' <summary>
  ''' Add New Company B commission movements types 
  ''' </summary>
  ''' <param name="ListMovementTypes"></param>
  ''' <param name="Added"></param>
  ''' <param name="ToAdd"></param>
  ''' <remarks></remarks>
  Private Sub AddCompanyBTypes(ByRef ListMovementTypes As List(Of CASHIER_MOVEMENT), Added As CASHIER_MOVEMENT, ToAdd As CASHIER_MOVEMENT)

    AddMovementType(ListMovementTypes, Added, ToAdd)

  End Sub

  ' PURPOSE: Get the list of the items selected in the filter data grid
  '          Format list to build query: X, X, X
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String contining id's of the selected rows
  Private Function GetTypeIdListSelected() As String
    Dim _idx_row As Integer
    Dim _list_mov_types As List(Of CASHIER_MOVEMENT)
    Dim _list_mov_aux As List(Of String)

    _list_mov_types = New List(Of CASHIER_MOVEMENT)
    _list_mov_aux = New List(Of String)

    ' Add checked filters
    For _idx_row = 0 To Me.dg_filter.NumRows - 1
      If (dg_filter.Cell(_idx_row, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED _
        And dg_filter.Cell(_idx_row, GRID_2_COLUMN_ROW_TYPE).Value = CStr(GRID_2_ROW_TYPE_DATA)) Then

        _list_mov_types.Add(dg_filter.Cell(_idx_row, GRID_2_COLUMN_CODE).Value)

      End If
    Next

    ' 15-DEC-2014  JBC, DLL, JPJ    New Company B commission movements  
    AddCompanyBTypes(_list_mov_types, CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1, CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT1)
    AddCompanyBTypes(_list_mov_types, CASHIER_MOVEMENT.CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1, CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1)
    AddCompanyBTypes(_list_mov_types, CASHIER_MOVEMENT.CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1, CASHIER_MOVEMENT.COMPANY_B_CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1)
    AddCompanyBTypes(_list_mov_types, CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT1, CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT1)
    AddCompanyBTypes(_list_mov_types, CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT1, CASHIER_MOVEMENT.COMPANY_B_CURRENCY_EXCHANGE_SPLIT1)
    AddCompanyBTypes(_list_mov_types, CASHIER_MOVEMENT.CASH_ADVANCE_CHECK, CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CHECK)
    AddCompanyBTypes(_list_mov_types, CASHIER_MOVEMENT.CASH_ADVANCE_CREDIT_BANK_CARD, CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CREDIT_BANK_CARD)
    AddCompanyBTypes(_list_mov_types, CASHIER_MOVEMENT.CASH_ADVANCE_DEBIT_BANK_CARD, CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_DEBIT_BANK_CARD)
    AddCompanyBTypes(_list_mov_types, CASHIER_MOVEMENT.CASH_ADVANCE_GENERIC_BANK_CARD, CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_GENERIC_BANK_CARD)

    AddCompanyBTypes(_list_mov_types, CASHIER_MOVEMENT.CARD_DEPOSIT_IN, CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN)
    AddCompanyBTypes(_list_mov_types, CASHIER_MOVEMENT.CARD_DEPOSIT_OUT, CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_OUT)
    AddCompanyBTypes(_list_mov_types, CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CHECK, CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CHECK)
    AddCompanyBTypes(_list_mov_types, CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CURRENCY_EXCHANGE, CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CURRENCY_EXCHANGE)
    AddCompanyBTypes(_list_mov_types, CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_CREDIT, CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_CREDIT)
    AddCompanyBTypes(_list_mov_types, CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_DEBIT, CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_DEBIT)
    AddCompanyBTypes(_list_mov_types, CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_GENERIC, CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_GENERIC)
    AddCompanyBTypes(_list_mov_types, CASHIER_MOVEMENT.CARD_REPLACEMENT, CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT)
    AddCompanyBTypes(_list_mov_types, CASHIER_MOVEMENT.CARD_REPLACEMENT_CHECK, CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CHECK)
    AddCompanyBTypes(_list_mov_types, CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_CREDIT, CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_CREDIT)
    AddCompanyBTypes(_list_mov_types, CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_DEBIT, CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_DEBIT)
    AddCompanyBTypes(_list_mov_types, CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_GENERIC, CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_GENERIC)

    ' DHA add chips sale/purchase exchange movements
    AddMovementType(_list_mov_types, CASHIER_MOVEMENT.CHIPS_SALE_TOTAL, CASHIER_MOVEMENT.CHIPS_SALE_TOTAL_EXCHANGE)

    AddMovementType(_list_mov_types, CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL, CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL_EXCHANGE)

    ' 08-AUG-2016 FGB Add Card Exchange Undo movement
    AddMovementType(_list_mov_types, CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1, CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1_UNDO)

    ' EOR 22-SEP-2016
    AddCompanyBTypes(_list_mov_types, CASHIER_MOVEMENT.SERVICE_CHARGE, CASHIER_MOVEMENT.SERVICE_CHARGE_ON_COMPANY_A)

    AddCompanyBTypes(_list_mov_types, CASHIER_MOVEMENT.SERVICE_CHARGE, CASHIER_MOVEMENT.SERVICE_CHARGE_GAMING_TABLE)
    AddCompanyBTypes(_list_mov_types, CASHIER_MOVEMENT.SERVICE_CHARGE, CASHIER_MOVEMENT.SERVICE_CHARGE_GAMING_TABLE_ON_COMPANY_A)

    ' Convert from List(Of CASHIER_MOVEMENT) to List(Of String)
    _list_mov_aux = _list_mov_types.ConvertAll(Of String)(Function(i As Integer) i.ToString())

    ' Return string concatenated
    Return String.Join(", ", _list_mov_aux.ToArray())

  End Function 'GetTypeIdListSelected

  ' PURPOSE: Get selected items text descriptions from the filter data grid for report purposes
  '          
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Function GetTypeListSelected() As String
    Dim _idx_row As Integer
    Dim _list_types As List(Of String)

    _list_types = New List(Of String)

    For _idx_row = 0 To Me.dg_filter.NumRows - 1

      If dg_filter.Cell(_idx_row, GRID_2_COLUMN_ROW_TYPE).Value = CStr(GRID_2_ROW_TYPE_DATA) Then

        If dg_filter.Cell(_idx_row, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED _
         And dg_filter.Cell(_idx_row, GRID_2_COLUMN_CODE).Value.Length > 0 Then
          'If filter checked then add movement type description
          _list_types.Add(LTrim(dg_filter.Cell(_idx_row, GRID_2_COLUMN_DESC).Value))

        End If

      End If
    Next

    '08-AUG-2016 FGB Comment next lines, because they do nothing, it is searching for CODES and the list contains DESCRIPTIONS
    ' 15-DEC-2014  JBC, DLL, JPJ    New Company B commission movements 
    'If _list_types.Contains(WSI.Common.CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1) Then
    '  _list_types.Add(WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT1)
    'End If

    'If _list_types.Contains(WSI.Common.CASHIER_MOVEMENT.CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1) Then
    '  _list_types.Add(WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1)
    'End If

    'If _list_types.Contains(WSI.Common.CASHIER_MOVEMENT.CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1) Then
    '  _list_types.Add(WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1)
    'End If

    'If _list_types.Contains(WSI.Common.CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT1) Then
    '  _list_types.Add(WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT1)
    'End If

    'If _list_types.Contains(WSI.Common.CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT1) Then
    '  _list_types.Add(WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CURRENCY_EXCHANGE_SPLIT1)
    'End If

    'If _list_types.Contains(WSI.Common.CASHIER_MOVEMENT.CASH_ADVANCE_CHECK) Then
    '  _list_types.Add(WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CHECK)
    'End If

    'If _list_types.Contains(WSI.Common.CASHIER_MOVEMENT.CASH_ADVANCE_CREDIT_BANK_CARD) Then
    '  _list_types.Add(WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CREDIT_BANK_CARD)
    'End If

    'If _list_types.Contains(WSI.Common.CASHIER_MOVEMENT.CASH_ADVANCE_DEBIT_BANK_CARD) Then
    '  _list_types.Add(WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_DEBIT_BANK_CARD)
    'End If

    'If _list_types.Contains(WSI.Common.CASHIER_MOVEMENT.CASH_ADVANCE_GENERIC_BANK_CARD) Then
    '  _list_types.Add(WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_GENERIC_BANK_CARD)
    'End If

    '' DHA 27-JAN-2016: added new movements for dual currency
    'If _list_types.Contains(WSI.Common.CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT1) Then
    '  _list_types.Add(WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CURRENCY_EXCHANGE_SPLIT1)
    'End If

    Return String.Join(", ", _list_types.ToArray())

  End Function 'GetTypeListSelected

  ' PURPOSE: Check consistency of the data grid filter values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - True: filter values are consistent
  '     - False: filter values are not consistent

  Private Sub CheckMovTypeFilter()

    If Me.opt_several_types.Checked Then
      If GetTypeIdListSelected() = "" Then

        Me.opt_several_types.Checked = False
        Me.opt_all_types.Checked = True

      End If
    End If

  End Sub ' CheckMovTypeFilter

  ' PURPOSE: Build search component to add to the 'where' clause of the 'select' statement
  '          according to the values selected in the data grid filter
  '
  '  PARAMS:
  '     - INPUT:
  '           - DBField: database field name to use in the search condition
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Search condition to add

  Private Function GetSqlWhereMovType(ByVal DBField As String) As String
    Dim str_where As String
    Dim types_list As String

    str_where = String.Empty

    If m_only_show_chips_movements Then
      Me.opt_several_types.Checked = True
    End If

    ' Operation Code Type Filter
    If Me.opt_several_types.Checked = True Then
      types_list = GetTypeIdListSelected()

      If types_list <> "" Then
        str_where = " AND (" & DBField & " IN (" & types_list & "))"
      End If
    End If

    Return str_where
  End Function ' GetSqlWhereMovType

#End Region ' Movement Types

  ''' <summary>
  ''' Returns the Movtype to totalize
  ''' </summary>
  ''' <param name="MovType"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetFinalMovType(ByVal MovType As Integer) As Integer
    Dim _final_mov_type As Integer

    _final_mov_type = MovType

    ' CURRENCY_CARD_EXCHANGE_SPLIT1_UNDO and CURRENCY_CARD_EXCHANGE_SPLIT1 go to the same total
    If (MovType = CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1_UNDO) Then
      _final_mov_type = CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1
    End If

    Return _final_mov_type
  End Function

  ' PURPOSE : Obtain the row from DataTable m_totals_data_table according to MovType.
  '           It it doesn't exist, add a new row for the MovType.
  '
  '  PARAMS :
  '     - INPUT :
  '           - MovType
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '           - A DataRow for the MovType totals.
  '
  Private Function GetRowFromTotalsByType(ByVal MovType As Integer, ByVal CurrencyCode As String, ByVal SuffixText As String) As DataRow
    Dim _final_mov_type As Integer
    Dim _selected_rows() As DataRow
    Dim _row_new_type As DataRow
    Dim _filter As String
    Dim _str_type As String

    If String.IsNullOrEmpty(CurrencyCode) Then
      CurrencyCode = m_national_currency_code
    End If

    ' Get the movtype to totalize
    _final_mov_type = GetFinalMovType(MovType)

    _filter = "MOV_TYPE = " & _final_mov_type & " AND ISO_CODE = '" & CurrencyCode & "' AND MOV_SUFFIX_TEXT = '" & SuffixText & "'"
    ' As column MOV_TYPE is Unique, if MovType exists, only one row is selected.
    _selected_rows = m_totals_data_table.Select(_filter)

    ' Total doesn't exist. Add it to the data table.
    If _selected_rows.Length = 0 Then
      _str_type = TranslateMovementType(MovType, CurrencyCode)
      _str_type &= SuffixText

      _row_new_type = m_totals_data_table.NewRow()
      _row_new_type.Item("MOV_TYPE") = _final_mov_type
      _row_new_type.Item("ISO_CODE") = CurrencyCode
      _row_new_type.Item("MOV_SUFFIX_TEXT") = SuffixText
      _row_new_type.Item("MOV_TYPE_NAME") = _str_type
      _row_new_type.Item("COUNT") = 0
      _row_new_type.Item("TOTAL_SUB_AMOUNT") = 0
      _row_new_type.Item("TOTAL_ADD_AMOUNT") = 0
      _row_new_type.Item("TOTAL_CURRENCY") = 0
      m_totals_data_table.Rows.Add(_row_new_type)
    Else
      _row_new_type = _selected_rows(0)
    End If

    Return _row_new_type
  End Function ' GetRowFromTotalsByType

  'Return the Chips Type description for the Chips Operation
  Private Function GetChipTypeDescription(ByVal CurrencyExchangeType As CurrencyExchangeType, ByVal CurrencyCode As String) As String
    Dim _str_chips_type As String

    _str_chips_type = WSI.Common.FeatureChips.GetChipTypeDescription(CurrencyExchangeType, CurrencyCode)

    Return _str_chips_type
  End Function

  'Return the ChipSet ID for the Chips Operation
  Private Function GetChipSetIDFromChip(ByVal ChipId As Integer) As Integer
    Dim _chip_set_id As Integer
    Dim _chip_set As WSI.Common.FeatureChips.ChipsSets.ChipSet

    'Get the ChipSet from the ChipId
    _chip_set = WSI.Common.FeatureChips.ChipsSets.GetChipsSetByChip(ChipId)
    _chip_set_id = 0

    'Get the ChipSet description/name
    If (Not _chip_set Is Nothing) Then
      _chip_set_id = _chip_set.ChipSetId
    End If

    Return _chip_set_id
  End Function

  'Return the ChipSet description for a ChipSet
  Private Function GetChipSetDescription(ByVal ChipSetId As Integer) As String
    Dim _chipset_desc As String
    Dim _chip_set As WSI.Common.FeatureChips.ChipsSets.ChipSet

    'Get the ChipSet from the Id
    _chip_set = WSI.Common.FeatureChips.ChipsSets.GetSet(ChipSetId)
    _chipset_desc = String.Empty

    'Get the ChipSet description/name
    If (Not _chip_set Is Nothing) Then
      _chipset_desc = _chip_set.Name
    End If

    Return _chipset_desc
  End Function

  ' PURPOSE : Create the data table for the totals by movement type.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  Private Sub InitTotalsDataTable()
    Dim column As DataColumn

    If m_totals_data_table IsNot Nothing Then
      m_totals_data_table.Dispose()
      m_totals_data_table = Nothing
    End If

    m_totals_data_table = New DataTable("TOTALS_BY_MOV_TYPE")

    column = New DataColumn()
    column.DataType = System.Type.GetType("System.Int32")
    column.ColumnName = "MOV_TYPE"
    column.ReadOnly = False
    m_totals_data_table.Columns.Add(column)

    column = New DataColumn()
    column.DataType = System.Type.GetType("System.String")
    column.ColumnName = "ISO_CODE"
    column.ReadOnly = False
    m_totals_data_table.Columns.Add(column)

    column = New DataColumn()
    column.DataType = System.Type.GetType("System.String")
    column.ColumnName = "MOV_SUFFIX_TEXT"
    column.ReadOnly = False
    m_totals_data_table.Columns.Add(column)

    column = New DataColumn()
    column.DataType = System.Type.GetType("System.String")
    column.ColumnName = "MOV_TYPE_NAME"
    column.ReadOnly = False
    m_totals_data_table.Columns.Add(column)

    column = New DataColumn()
    column.DataType = System.Type.GetType("System.Int32")
    column.ColumnName = "COUNT"
    column.ReadOnly = False
    m_totals_data_table.Columns.Add(column)

    column = New DataColumn()
    column.DataType = System.Type.GetType("System.Decimal")
    column.ColumnName = "TOTAL_SUB_AMOUNT"
    column.ReadOnly = False
    m_totals_data_table.Columns.Add(column)

    column = New DataColumn()
    column.DataType = System.Type.GetType("System.Decimal")
    column.ColumnName = "TOTAL_ADD_AMOUNT"
    column.ReadOnly = False
    m_totals_data_table.Columns.Add(column)

    column = New DataColumn()
    column.DataType = System.Type.GetType("System.Decimal")
    column.ColumnName = "TOTAL_CURRENCY"
    column.ReadOnly = False
    m_totals_data_table.Columns.Add(column)

  End Sub ' InitTotalsDataTable

#End Region ' Private Functions

#Region " Events "

  Private Sub opt_several_cashiers_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_one_cashier.Click
    Me.cmb_cashier.Enabled = True
  End Sub ' opt_several_cashiers_Click

  Private Sub opt_all_cashiers_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_all_cashiers.Click
    Me.cmb_cashier.Enabled = False
  End Sub ' opt_all_cashiers_Click

  Private Sub opt_several_users_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_one_user.Click
    Me.cmb_user.Enabled = True
    Me.chk_show_all.Enabled = True
  End Sub ' opt_several_users_Click

  Private Sub opt_all_users_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_all_users.Click
    Me.cmb_user.Enabled = False
    Me.chk_show_all.Enabled = False
  End Sub ' opt_all_users_Click

  Private Sub dg_types_DataSelectedEvent() Handles dg_filter.DataSelectedEvent
    Dim _idx As Integer
    Dim _last_row As Integer
    Dim _row_is_header As Boolean

    If Me.opt_several_types.Checked = False Then
      Me.opt_several_types.Checked = True
    End If

    With Me.dg_filter
      _row_is_header = RowIsHeader(.CurrentRow)

      If _row_is_header And .CurrentCol <> GRID_2_COLUMN_CHECKED Then
        _last_row = .CurrentRow
        For _idx = .CurrentRow + 1 To .NumRows - 1
          If RowIsHeader(_idx) Then

            Exit For
          End If
          _last_row = _idx
        Next

        Me.dg_filter.CollapseExpand(.CurrentRow, _last_row, GRID_2_COLUMN_DESC)

      End If
    End With
  End Sub ' dg_types_DataSelectedEvent

  Private Sub dg_types_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer) Handles dg_filter.CellDataChangedEvent
    Dim current_row As Integer

    ' Prevent recursive calls
    If m_refreshing_grid = True Then

      Exit Sub
    End If

    ' Update radio buttons accordingly
    If Me.opt_several_types.Checked = False Then
      Me.opt_several_types.Checked = True
    End If

    With Me.dg_filter

      current_row = .CurrentRow
      m_refreshing_grid = True

      If RowIsHeader(.CurrentRow) Then
        Call ChangeCheckofRows(.CurrentRow, .Cell(.CurrentRow, GRID_2_COLUMN_CHECKED).Value)
      Else
        If .Cell(.CurrentRow, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED Then
          Call TryToUncheckHeader(.CurrentRow)
        Else
          Call TryToCheckHeader(.CurrentRow)
        End If
      End If

      m_refreshing_grid = False
      .CurrentRow = current_row

    End With
  End Sub ' dg_types_CellDataChangedEvent

  Private Sub opt_all_types_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_all_types.Click
    Dim _idx As Integer

    For _idx = 0 To dg_filter.NumRows - 1
      Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    Next

  End Sub ' opt_all_types_Click

  ' XCD 16-Aug-2012 Fill combo users
  Private Sub chk_show_all_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_show_all.CheckedChanged
    If chk_show_all.Checked Then
      Call SetCombo(Me.cmb_user, "SELECT GU_USER_ID, GU_USERNAME FROM GUI_USERS WHERE GU_USER_TYPE = " & WSI.Common.GU_USER_TYPE.USER & " ORDER BY GU_USERNAME")
    Else
      Call SetCombo(Me.cmb_user, "SELECT GU_USER_ID, GU_USERNAME FROM GUI_USERS WHERE GU_BLOCK_REASON = " & WSI.Common.GUI_USER_BLOCK_REASON.NONE & " AND GU_USER_TYPE = " & WSI.Common.GU_USER_TYPE.USER & " ORDER BY GU_USERNAME")
    End If
  End Sub 'chk_show_all

#End Region ' Events

End Class
