<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_player_edit
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.gb_player_info = New System.Windows.Forms.GroupBox()
    Me.lbl_blocked = New System.Windows.Forms.Label()
    Me.ef_birth_date = New GUI_Controls.uc_entry_field()
    Me.ef_age = New GUI_Controls.uc_entry_field()
    Me.ef_gender = New GUI_Controls.uc_entry_field()
    Me.ef_document = New GUI_Controls.uc_entry_field()
    Me.ef_account = New GUI_Controls.uc_entry_field()
    Me.uc_player_data = New WigosGUI.uc_player_edit()
    Me.panel_data.SuspendLayout()
    Me.gb_player_info.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_player_info)
    Me.panel_data.Controls.Add(Me.uc_player_data)
    Me.panel_data.Size = New System.Drawing.Size(640, 486)
    '
    'gb_player_info
    '
    Me.gb_player_info.Controls.Add(Me.lbl_blocked)
    Me.gb_player_info.Controls.Add(Me.ef_birth_date)
    Me.gb_player_info.Controls.Add(Me.ef_age)
    Me.gb_player_info.Controls.Add(Me.ef_gender)
    Me.gb_player_info.Controls.Add(Me.ef_document)
    Me.gb_player_info.Controls.Add(Me.ef_account)
    Me.gb_player_info.Location = New System.Drawing.Point(3, 3)
    Me.gb_player_info.Name = "gb_player_info"
    Me.gb_player_info.Size = New System.Drawing.Size(634, 112)
    Me.gb_player_info.TabIndex = 1
    Me.gb_player_info.TabStop = False
    '
    'lbl_blocked
    '
    Me.lbl_blocked.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_blocked.Location = New System.Drawing.Point(341, 52)
    Me.lbl_blocked.Name = "lbl_blocked"
    Me.lbl_blocked.Size = New System.Drawing.Size(287, 19)
    Me.lbl_blocked.TabIndex = 5
    Me.lbl_blocked.Text = "xBlocked"
    Me.lbl_blocked.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'ef_birth_date
    '
    Me.ef_birth_date.DoubleValue = 0.0R
    Me.ef_birth_date.IntegerValue = 0
    Me.ef_birth_date.IsReadOnly = True
    Me.ef_birth_date.Location = New System.Drawing.Point(341, 76)
    Me.ef_birth_date.Name = "ef_birth_date"
    Me.ef_birth_date.PlaceHolder = Nothing
    Me.ef_birth_date.Size = New System.Drawing.Size(287, 24)
    Me.ef_birth_date.SufixText = "Sufix Text"
    Me.ef_birth_date.SufixTextVisible = True
    Me.ef_birth_date.TabIndex = 4
    Me.ef_birth_date.TabStop = False
    Me.ef_birth_date.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_birth_date.TextValue = ""
    Me.ef_birth_date.TextWidth = 120
    Me.ef_birth_date.Value = ""
    Me.ef_birth_date.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_age
    '
    Me.ef_age.DoubleValue = 0.0R
    Me.ef_age.IntegerValue = 0
    Me.ef_age.IsReadOnly = True
    Me.ef_age.Location = New System.Drawing.Point(237, 77)
    Me.ef_age.Name = "ef_age"
    Me.ef_age.PlaceHolder = Nothing
    Me.ef_age.Size = New System.Drawing.Size(81, 24)
    Me.ef_age.SufixText = "Sufix Text"
    Me.ef_age.SufixTextVisible = True
    Me.ef_age.TabIndex = 3
    Me.ef_age.TabStop = False
    Me.ef_age.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_age.TextValue = ""
    Me.ef_age.TextWidth = 45
    Me.ef_age.Value = ""
    Me.ef_age.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_gender
    '
    Me.ef_gender.DoubleValue = 0.0R
    Me.ef_gender.IntegerValue = 0
    Me.ef_gender.IsReadOnly = True
    Me.ef_gender.Location = New System.Drawing.Point(6, 77)
    Me.ef_gender.Name = "ef_gender"
    Me.ef_gender.PlaceHolder = Nothing
    Me.ef_gender.Size = New System.Drawing.Size(225, 24)
    Me.ef_gender.SufixText = "Sufix Text"
    Me.ef_gender.SufixTextVisible = True
    Me.ef_gender.TabIndex = 2
    Me.ef_gender.TabStop = False
    Me.ef_gender.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_gender.TextValue = ""
    Me.ef_gender.TextWidth = 140
    Me.ef_gender.Value = ""
    Me.ef_gender.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_document
    '
    Me.ef_document.DoubleValue = 0.0R
    Me.ef_document.IntegerValue = 0
    Me.ef_document.IsReadOnly = True
    Me.ef_document.Location = New System.Drawing.Point(6, 47)
    Me.ef_document.Name = "ef_document"
    Me.ef_document.PlaceHolder = Nothing
    Me.ef_document.Size = New System.Drawing.Size(312, 24)
    Me.ef_document.SufixText = "Sufix Text"
    Me.ef_document.SufixTextVisible = True
    Me.ef_document.TabIndex = 1
    Me.ef_document.TabStop = False
    Me.ef_document.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_document.TextValue = ""
    Me.ef_document.TextWidth = 140
    Me.ef_document.Value = ""
    Me.ef_document.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_account
    '
    Me.ef_account.DoubleValue = 0.0R
    Me.ef_account.IntegerValue = 0
    Me.ef_account.IsReadOnly = True
    Me.ef_account.Location = New System.Drawing.Point(6, 17)
    Me.ef_account.Name = "ef_account"
    Me.ef_account.PlaceHolder = Nothing
    Me.ef_account.Size = New System.Drawing.Size(622, 24)
    Me.ef_account.SufixText = "Sufix Text"
    Me.ef_account.SufixTextVisible = True
    Me.ef_account.TabIndex = 0
    Me.ef_account.TabStop = False
    Me.ef_account.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_account.TextValue = ""
    Me.ef_account.TextWidth = 140
    Me.ef_account.Value = ""
    Me.ef_account.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_player_data
    '
    Me.uc_player_data.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.uc_player_data.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_player_data.Location = New System.Drawing.Point(3, 121)
    Me.uc_player_data.Name = "uc_player_data"
    Me.uc_player_data.Size = New System.Drawing.Size(636, 364)
    Me.uc_player_data.TabIndex = 0
    '
    'frm_player_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(742, 495)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_player_edit"
    Me.Text = "frm_player_edit"
    Me.panel_data.ResumeLayout(False)
    Me.gb_player_info.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_player_data As WigosGUI.uc_player_edit
  Friend WithEvents gb_player_info As System.Windows.Forms.GroupBox
  Friend WithEvents ef_account As GUI_Controls.uc_entry_field
  Friend WithEvents ef_birth_date As GUI_Controls.uc_entry_field
  Friend WithEvents ef_age As GUI_Controls.uc_entry_field
  Friend WithEvents ef_gender As GUI_Controls.uc_entry_field
  Friend WithEvents ef_document As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_blocked As System.Windows.Forms.Label
End Class
