'-------------------------------------------------------------------
' Copyright � 2009 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_cage_stock.vb
'
' DESCRIPTION:   Show full details for a Cage stock 
'
' AUTHOR:        Ferran Ortner
'
' CREATION DATE: 29-MAR-2016
'
' REVISION HISTORY:
'
' Date         Author     Description
' -----------  ---------  ---------------------------------------------------
' 29-MAR-2016  FOS        Initial Release.
' 14-APR-2016  FOS        Add columns color and picture 
' 25-APR-2016  FOS        Grouped by chips_set_id
' 03-MAY-2016  FOS        Show all tabs in excel
' 24-MAY-2016  RAB        PBI 11755: Tables (Phase 1): Upgrading to a new version of chips. Backward compatibility
' 09-JUN-2016  RAB        PBI 11755: Tables (Phase 1): Correction errors as a result of the backward compatibility of the chips group
' 27-JUN-2016  FOS        Product Backlog Item 14493: Gaming tables (Fase 4): Cage
' 16-NOV-2016  JMV        Bug 20573: WigosGUI error in cage stock
' 14-MAR-2017  JML        Bug 25738:Stock de b�veda: Error en los totalizadores
'--------------------------------------------------------------------
Option Strict Off
Option Explicit On

Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_Controls.frm_base_sel
Imports GUI_Reports
Imports WSI.Common
Imports System.Text

Public Class frm_cage_stock
  Inherits frm_base_print

#Region " Constants "
  '' Sql Columns
  Private Const SQL_COLUMN_ISO_CODE_STOCK As Int32 = 0
  Private Const SQL_COLUMN_DENOMINATION_STOCK As Int32 = 1
  Private Const SQL_COLUMN_CURRENCY_TYPE_STOCK As Int32 = 2
  Private Const SQL_COLUMN_COLOR As Int32 = 3
  Private Const SQL_COLUMN_GROUP_NAME As Int32 = 4
  Private Const SQL_COLUMN_CHIP_NAME As Int32 = 5
  Private Const SQL_COLUMN_PICTURE As Int32 = 6
  Private Const SQL_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK As Int32 = 9
  Private Const SQL_COLUMN_QUANTITY_STOCK As Int32 = 10
  Private Const SQL_COLUMN_TOTAL_STOCK As Int32 = 11
  '' Grid Columns
  Private Const GRID_COLUMN_INDEX_STOCK As Int32 = 0
  Private Const GRID_COLUMN_ISO_CODE_STOCK As Int32 = 1
  Private Const GRID_COLUMN_CURRENCY_TYPE_STOCK As Int32 = 2
  Private Const GRID_COLUMN_GROUP_NAME As Int32 = 3
  Private Const GRID_COLUMN_COLOR As Int32 = 4
  Private Const GRID_COLUMN_CHIP_NAME As Int32 = 5
  Private Const GRID_COLUMN_PICTURE As Int32 = 6
  Private Const GRID_COLUMN_DENOMINATION_STOCK As Int32 = 7
  Private Const GRID_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK As Int32 = 8
  Private Const GRID_COLUMN_QUANTITY_STOCK As Int32 = 9
  Private Const GRID_COLUMN_TOTAL_STOCK As Int32 = 10
  '' Grid Width
  Private Const WIDTH_GRID_COLUMN_INDEX_STOCK As Int32 = 0
  Private WIDTH_GRID_COLUMN_ISO_CODE_STOCK As Int32 = 800
  Private Const WIDTH_GRID_COLUMN_DENOMINATION_STOCK As Int32 = 0
  Private WIDTH_GRID_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK As Int32 = 1600
  Private WIDTH_GRID_COLUMN_CURRENCY_TYPE_STOCK As Int32 = 1700
  Private WIDTH_GRID_COLUMN_QUANTITY_STOCK As Int32 = 1200
  Private WIDTH_GRID_COLUMN_TOTAL_STOCK As Int32 = 1800
  Private Const WIDTH_GRID_COLUMN_COLOR As Int32 = 300
  Private Const GRID_COLUMNS_STOCK As Int32 = 11
  Private Const GRID_HEADER_ROWS_STOCK As Int32 = 1
  '' Totals grid columns
  Private Const TB_COLUMN_ISO_CODE_STOCK As Int32 = 0
  Private Const TB_COLUMN_DENOMINATION_STOCK As Int32 = 1
  Private Const TB_COLUMN_CURRENCY_TYPE As Int32 = 2
  Private Const TB_COLUMN_GRID_TYPE As Int32 = 3
  Private Const TB_COLUMN_QUANTITY_STOCK As Int32 = 4
  Private Const TB_COLUMN_TOTAL_STOCK As Int32 = 5
  '' Form Values
  Private Const FORM_WIDTH_STOCK As Int32 = 1005
  Private Const FORM_HEIGHT_STOCK As Int32 = 548
  Private Const FORM_GRID_WIDTH As Int32 = 867
  Private Const FORM_GRID_HEIGHT As Int32 = 440
  Private Const FORM_TAB_HEIGHT As Int32 = 468
  Private Const MAX_RECORDS_LOAD As Integer = Integer.MaxValue
  Private Const MAX_RECORDS_FIRST_LOAD As Integer = 50

#End Region

#Region " Members "
  Private m_session_detail As cls_cage_session_detail
  Private m_dt_totals As DataTable
  Private m_rw_current_total_stock As DataRow
  Private m_rw_current_total_stock_cash As DataRow
  Private m_rw_current_total_stock_chips As DataRow
  Private m_theoric_stock As CLASS_CAGE_CONTROL
  '' To custom query
  Private m_grid_overflow As Boolean
  Private m_grid_refresh_step As Integer
  Private m_grid_refresh_pending_rows As Integer
  Private m_show_only_stock As Boolean = False
  Private m_first_time As Boolean = True
  Private m_cage_show_denominations As Integer = 1 ' = Show all denominations
  Private m_grid_cash As uc_grid
  Private m_grid_chips As uc_grid
  Private m_print_datetime As Date
#End Region

#Region " Builder "

  Public Sub New()
    MyBase.New()

    Me.FormId = ENUM_FORM.FORM_CAGE_STOCK

    Call MyBase.GUI_SetFormId()

    InitializeComponent()
  End Sub

#End Region

#Region " Public Properties "

  Public Property SessionDetail() As cls_cage_session_detail
    Get
      Return m_session_detail
    End Get
    Set(ByVal value As cls_cage_session_detail)
      m_session_detail = value
    End Set
  End Property ' SessionDetail

#End Region   ' Public Properties

#Region " Publics "

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window, ByVal Item As Object, Optional ByVal ShowOnlyStock As Boolean = False)

    Me.SessionDetail = Item
    Me.MdiParent = MdiParent
    Me.m_show_only_stock = ShowOnlyStock
    If ShowOnlyStock Then
      Me.Width = FORM_WIDTH_STOCK
    End If
    Me.Height = FORM_HEIGHT_STOCK
    Me.Display(False)

    Me.MaximumSize = New Size(FORM_WIDTH_STOCK, FORM_HEIGHT_STOCK)
    Me.MinimumSize = New Size(FORM_WIDTH_STOCK, FORM_HEIGHT_STOCK)

    Call GUI_ExecuteQueryCustom()

  End Sub

#End Region

#Region " Private "

  ' PURPOSE: Set the labels' information 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub SetLabelsText()

    If m_session_detail.m_session_id <> -1 Then
      m_session_detail.m_session_name = m_session_detail.GetSessionName()
    Else
      m_session_detail.m_session_name = AUDIT_NONE_STRING
    End If

  End Sub ' SetLabelsText

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub GUI_StyleSheet()
    Dim _offset_with As Integer = WIDTH_GRID_COLUMN_QUANTITY_STOCK / 3

    Me.m_cage_show_denominations = GeneralParam.GetInt32("Cage", "ShowDenominations", 1)

    GUI_StyleSheetCash()
    GUI_StyleSheetChips()

  End Sub ' GUI_StyleSheet
  ' PURPOSE: Define layout ofCash grid columns 
  '
  '  PARAMS:
  '     - INPUT:
  '
  ' RETURNS:
  Private Sub GUI_StyleSheetCash()
    Dim _offset_with As Integer = WIDTH_GRID_COLUMN_QUANTITY_STOCK / 3

    m_grid_cash = New uc_grid()
    m_grid_cash.Width = FORM_GRID_WIDTH
    m_grid_cash.Height = FORM_GRID_HEIGHT
    tb_cash.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1570)
    tb_cash.Height = FORM_TAB_HEIGHT
    m_grid_cash.TabStop = False

    m_grid_cash.WordWrap = True

    With m_grid_cash

      If Me.m_cage_show_denominations = Cage.ShowDenominationsMode.HideAllDenominations Then
        WIDTH_GRID_COLUMN_QUANTITY_STOCK = 0
        WIDTH_GRID_COLUMN_ISO_CODE_STOCK += _offset_with
        WIDTH_GRID_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK += _offset_with
        WIDTH_GRID_COLUMN_TOTAL_STOCK += _offset_with
      End If

      Call .Init(GRID_COLUMNS_STOCK, GRID_HEADER_ROWS_STOCK)

      ' Index
      .Column(GRID_COLUMN_INDEX_STOCK).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX_STOCK).Width = WIDTH_GRID_COLUMN_INDEX_STOCK
      .Column(GRID_COLUMN_INDEX_STOCK).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX_STOCK).IsColumnPrintable = False
      .Column(GRID_COLUMN_INDEX_STOCK).HighLightWhenSelected = False
      ' Iso Code
      .Column(GRID_COLUMN_ISO_CODE_STOCK).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4356)  ' Divisa
      .Column(GRID_COLUMN_ISO_CODE_STOCK).Width = WIDTH_GRID_COLUMN_ISO_CODE_STOCK
      .Column(GRID_COLUMN_ISO_CODE_STOCK).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_ISO_CODE_STOCK).IsMerged = True
      .Column(GRID_COLUMN_ISO_CODE_STOCK).HighLightWhenSelected = False
      ' Currency type
      .Column(GRID_COLUMN_CURRENCY_TYPE_STOCK).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5678) ' Tipo
      .Column(GRID_COLUMN_CURRENCY_TYPE_STOCK).Width = WIDTH_GRID_COLUMN_CURRENCY_TYPE_STOCK
      .Column(GRID_COLUMN_CURRENCY_TYPE_STOCK).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_CURRENCY_TYPE_STOCK).HighLightWhenSelected = False
      ' Group name
      .Column(GRID_COLUMN_GROUP_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(367) ' Group name
      .Column(GRID_COLUMN_GROUP_NAME).Width = 0
      .Column(GRID_COLUMN_GROUP_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_GROUP_NAME).HighLightWhenSelected = False
      ' Color
      .Column(GRID_COLUMN_COLOR).Header(0).Text = ""  ' Color
      .Column(GRID_COLUMN_COLOR).Width = 0
      .Column(GRID_COLUMN_COLOR).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_COLOR).HighLightWhenSelected = False
      ' Chip name
      .Column(GRID_COLUMN_CHIP_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(367) ' Chip name
      .Column(GRID_COLUMN_CHIP_NAME).Width = 0
      .Column(GRID_COLUMN_CHIP_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_CHIP_NAME).HighLightWhenSelected = False
      ' Picture
      .Column(GRID_COLUMN_PICTURE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6924) ' Picture
      .Column(GRID_COLUMN_PICTURE).Width = 0
      .Column(GRID_COLUMN_PICTURE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_PICTURE).HighLightWhenSelected = False
      ' Denomination [number]
      .Column(GRID_COLUMN_DENOMINATION_STOCK).Header(0).Text = " "  ' Denominaciones
      .Column(GRID_COLUMN_DENOMINATION_STOCK).Width = WIDTH_GRID_COLUMN_DENOMINATION_STOCK
      .Column(GRID_COLUMN_DENOMINATION_STOCK).IsMerged = True
      .Column(GRID_COLUMN_DENOMINATION_STOCK).IsColumnPrintable = False
      .Column(GRID_COLUMN_DENOMINATION_STOCK).HighLightWhenSelected = False
      ' Denomination [number or text]
      .Column(GRID_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4357)  ' Denominaciones
      .Column(GRID_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK).Width = WIDTH_GRID_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK
      .Column(GRID_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK).HighLightWhenSelected = False
      ' Quantity
      .Column(GRID_COLUMN_QUANTITY_STOCK).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2265)    ' Cantidad
      .Column(GRID_COLUMN_QUANTITY_STOCK).Width = WIDTH_GRID_COLUMN_QUANTITY_STOCK
      .Column(GRID_COLUMN_QUANTITY_STOCK).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_QUANTITY_STOCK).HighLightWhenSelected = False
      ' Total
      .Column(GRID_COLUMN_TOTAL_STOCK).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1340) ' Total
      .Column(GRID_COLUMN_TOTAL_STOCK).Width = WIDTH_GRID_COLUMN_TOTAL_STOCK
      .Column(GRID_COLUMN_TOTAL_STOCK).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_TOTAL_STOCK).HighLightWhenSelected = False
    End With
    m_grid_cash.IsSortable = False
    Me.tb_cash.Controls.Add(m_grid_cash)

  End Sub ' GUI_StyleSheetCash
  ' PURPOSE: Define layout ofCash grid columns 
  '
  '  PARAMS:
  '     - INPUT:
  '
  ' RETURNS:
  Private Sub GUI_StyleSheetChips()
    Dim _offset_with As Integer = WIDTH_GRID_COLUMN_QUANTITY_STOCK / 3

    m_grid_chips = New uc_grid()
    m_grid_chips.Width = FORM_GRID_WIDTH
    m_grid_chips.Height = FORM_GRID_HEIGHT
    tb_chips.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2941)
    tb_chips.Height = FORM_TAB_HEIGHT
    m_grid_chips.TabStop = False

    m_grid_chips.WordWrap = True

    With m_grid_chips

      If Me.m_cage_show_denominations = Cage.ShowDenominationsMode.HideAllDenominations Then
        WIDTH_GRID_COLUMN_QUANTITY_STOCK = 0
        WIDTH_GRID_COLUMN_ISO_CODE_STOCK += _offset_with
        WIDTH_GRID_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK += _offset_with
        WIDTH_GRID_COLUMN_TOTAL_STOCK += _offset_with
      End If

      Call .Init(GRID_COLUMNS_STOCK, GRID_HEADER_ROWS_STOCK)

      ' Index
      .Column(GRID_COLUMN_INDEX_STOCK).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX_STOCK).Width = WIDTH_GRID_COLUMN_INDEX_STOCK
      .Column(GRID_COLUMN_INDEX_STOCK).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX_STOCK).IsColumnPrintable = False
      .Column(GRID_COLUMN_INDEX_STOCK).HighLightWhenSelected = False
      ' Iso Code
      .Column(GRID_COLUMN_ISO_CODE_STOCK).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4356)  ' Divisa
      .Column(GRID_COLUMN_ISO_CODE_STOCK).Width = WIDTH_GRID_COLUMN_ISO_CODE_STOCK
      .Column(GRID_COLUMN_ISO_CODE_STOCK).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_ISO_CODE_STOCK).IsMerged = True
      .Column(GRID_COLUMN_ISO_CODE_STOCK).HighLightWhenSelected = False
      ' Currency type
      .Column(GRID_COLUMN_CURRENCY_TYPE_STOCK).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5678) ' Tipo
      .Column(GRID_COLUMN_CURRENCY_TYPE_STOCK).Width = WIDTH_GRID_COLUMN_CURRENCY_TYPE_STOCK
      .Column(GRID_COLUMN_CURRENCY_TYPE_STOCK).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_CURRENCY_TYPE_STOCK).HighLightWhenSelected = False
      .Column(GRID_COLUMN_CURRENCY_TYPE_STOCK).IsMerged = True
      ' Group
      .Column(GRID_COLUMN_GROUP_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2420) ' Group
      .Column(GRID_COLUMN_GROUP_NAME).Width = WIDTH_GRID_COLUMN_CURRENCY_TYPE_STOCK
      .Column(GRID_COLUMN_GROUP_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT_CENTER
      .Column(GRID_COLUMN_GROUP_NAME).HighLightWhenSelected = False
      .Column(GRID_COLUMN_GROUP_NAME).IsMerged = True

      ' Color
      .Column(GRID_COLUMN_COLOR).Header(0).Text = "" ' Color
      .Column(GRID_COLUMN_COLOR).Width = WIDTH_GRID_COLUMN_COLOR
      .Column(GRID_COLUMN_COLOR).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_COLOR).HighLightWhenSelected = False

      ' Chip name
      .Column(GRID_COLUMN_CHIP_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(367) ' Name
      .Column(GRID_COLUMN_CHIP_NAME).Width = WIDTH_GRID_COLUMN_TOTAL_STOCK
      .Column(GRID_COLUMN_CHIP_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_CHIP_NAME).HighLightWhenSelected = False
      ' Picture
      .Column(GRID_COLUMN_PICTURE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6924) ' Picture
      .Column(GRID_COLUMN_PICTURE).Width = WIDTH_GRID_COLUMN_TOTAL_STOCK
      .Column(GRID_COLUMN_PICTURE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_PICTURE).HighLightWhenSelected = False
      ' Denomination [number]
      .Column(GRID_COLUMN_DENOMINATION_STOCK).Header(0).Text = " "  ' Denominaciones
      .Column(GRID_COLUMN_DENOMINATION_STOCK).Width = WIDTH_GRID_COLUMN_DENOMINATION_STOCK
      .Column(GRID_COLUMN_DENOMINATION_STOCK).IsMerged = True
      .Column(GRID_COLUMN_DENOMINATION_STOCK).IsColumnPrintable = False
      .Column(GRID_COLUMN_DENOMINATION_STOCK).HighLightWhenSelected = False
      ' Denomination [number or text]
      .Column(GRID_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4357)  ' Denominaciones
      .Column(GRID_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK).Width = WIDTH_GRID_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK
      .Column(GRID_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK).HighLightWhenSelected = False
      ' Quantity
      .Column(GRID_COLUMN_QUANTITY_STOCK).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2265)    ' Cantidad
      .Column(GRID_COLUMN_QUANTITY_STOCK).Width = WIDTH_GRID_COLUMN_QUANTITY_STOCK
      .Column(GRID_COLUMN_QUANTITY_STOCK).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_QUANTITY_STOCK).HighLightWhenSelected = False
      ' Total
      .Column(GRID_COLUMN_TOTAL_STOCK).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1340) ' Total
      .Column(GRID_COLUMN_TOTAL_STOCK).Width = WIDTH_GRID_COLUMN_TOTAL_STOCK
      .Column(GRID_COLUMN_TOTAL_STOCK).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_TOTAL_STOCK).HighLightWhenSelected = False
    End With
    m_grid_chips.IsSortable = False
    Me.tb_chips.Controls.Add(m_grid_chips)

  End Sub ' GUI_StyleSheetChips

  ' PURPOSE: Initialize totals' table
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  'Private Sub CreateTotalsTable(ByVal FrmMode As FORM_MODE)
  Private Sub CreateTotalsTable()
    Dim _col As DataColumn

    m_dt_totals = New DataTable()

    _col = m_dt_totals.Columns.Add()
    _col.DataType = System.Type.GetType("System.String")
    _col.AllowDBNull = True
    _col.Caption = "TB_COLUMN_ISO_CODE_STOCK"
    _col.ColumnName = "TB_COLUMN_ISO_CODE_STOCK"

    _col = m_dt_totals.Columns.Add()
    _col.DataType = System.Type.GetType("System.Decimal")
    _col.AllowDBNull = True
    _col.Caption = "TB_COLUMN_DENOMINATION_STOCK"
    _col.ColumnName = "TB_COLUMN_DENOMINATION_STOCK"

    _col = m_dt_totals.Columns.Add()
    _col.DataType = System.Type.GetType("System.String")
    _col.AllowDBNull = True
    _col.Caption = "TB_COLUMN_CURRENCY_TYPE"
    _col.ColumnName = "TB_COLUMN_CURRENCY_TYPE"

    _col = m_dt_totals.Columns.Add()
    _col.DataType = System.Type.GetType("System.String")
    _col.AllowDBNull = True
    _col.Caption = "TB_COLUMN_GRID_TYPE"
    _col.ColumnName = "TB_COLUMN_GRID_TYPE"

    _col = m_dt_totals.Columns.Add()
    _col.DataType = System.Type.GetType("System.Decimal")
    _col.AllowDBNull = True
    _col.Caption = "TB_COLUMN_QUANTITY_STOCK"
    _col.ColumnName = "TB_COLUMN_QUANTITY_STOCK"

    _col = m_dt_totals.Columns.Add()
    _col.DataType = System.Type.GetType("System.Decimal")
    _col.AllowDBNull = True
    _col.Caption = "TB_COLUMN_TOTAL_STOCK"
    _col.ColumnName = "TB_COLUMN_TOTAL_STOCK"

  End Sub ' CreateTotalsTable

  ' PURPOSE: Refresh stock grids
  '
  '  PARAMS:
  '     - INPUT:
  '
  ' RETURNS:
  '
  Private Sub CheckRefreshGrid()

    CheckRefreshGridChips()
    CheckRefreshGridCash()

  End Sub   ' CheckRefreshGrid

  ' PURPOSE: Refresh chips grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  ' RETURNS:
  '
  Private Sub CheckRefreshGridChips()

    If m_grid_refresh_pending_rows > m_grid_refresh_step Then

      m_grid_chips.Redraw = True
      Call Application.DoEvents()

      ' DoEvents run each second.
      If Not GUI_DoEvents(m_grid_chips) Then
        Exit Sub
      End If

      Windows.Forms.Cursor.Current = Cursors.WaitCursor
      m_grid_chips.Redraw = False

      m_grid_refresh_pending_rows = 0

    End If

  End Sub   ' CheckRefreshGridChips

  ' PURPOSE: Refresh cash grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  ' RETURNS:
  '
  Private Sub CheckRefreshGridCash()

    If m_grid_refresh_pending_rows > m_grid_refresh_step Then

      m_grid_cash.Redraw = True
      Call Application.DoEvents()

      ' DoEvents run each second.
      If Not GUI_DoEvents(m_grid_chips) Then
        Exit Sub
      End If

      Windows.Forms.Cursor.Current = Cursors.WaitCursor
      m_grid_cash.Redraw = False

      m_grid_refresh_pending_rows = 0

    End If

  End Sub   ' CheckRefreshGridCash

  ' PURPOSE: Insert a totals' row
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub InsertTotalsChips(ByVal iso_code As String, ByRef grid_chips As uc_grid, ByVal idx_row As Integer, Optional ByVal add_new_row As Boolean = False)
    Dim _row As DataRow


    If add_new_row Then
      m_dt_totals.Rows.Add(m_rw_current_total_stock)
      grid_chips.AddRow()
      idx_row = grid_chips.NumRows - 1
    End If

    For Each _row In m_dt_totals.Select

      grid_chips.Cell(idx_row, GRID_COLUMN_CURRENCY_TYPE_STOCK).Value = GLB_NLS_GUI_INVOICING.GetString(205)

      If _row.IsNull(TB_COLUMN_QUANTITY_STOCK) OrElse _row.Item(TB_COLUMN_QUANTITY_STOCK) = 0 Then
        grid_chips.Cell(idx_row, GRID_COLUMN_QUANTITY_STOCK).Value = ""
      Else
        grid_chips.Cell(idx_row, GRID_COLUMN_QUANTITY_STOCK).Value = SessionDetail.FormatAmount(_row.Item(TB_COLUMN_QUANTITY_STOCK), "-")
      End If
      If String.IsNullOrEmpty(_row.Item(SQL_COLUMN_CURRENCY_TYPE_STOCK).ToString) OrElse _row.Item(SQL_COLUMN_CURRENCY_TYPE_STOCK).ToString = CageCurrencyType.ChipsColor Then
        grid_chips.Cell(idx_row, GRID_COLUMN_TOTAL_STOCK).Value = ""
      Else
        grid_chips.Cell(idx_row, GRID_COLUMN_TOTAL_STOCK).Value = SessionDetail.FormatAmount(_row.Item(TB_COLUMN_TOTAL_STOCK), iso_code)
      End If

      grid_chips.Row(idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00) ' Total:
    Next

  End Sub ' InsertTotalsChips

  ' PURPOSE: Insert a totals' row
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub InsertTotalsCash(ByVal iso_code As String, ByRef grid_cash As uc_grid, ByVal idx_row As Integer, Optional ByVal add_new_row As Boolean = False)
    Dim _row As DataRow

    If add_new_row Then
      m_dt_totals.Rows.Add(m_rw_current_total_stock)
      grid_cash.AddRow()
      idx_row = grid_cash.NumRows - 1
    End If

    For Each _row In m_dt_totals.Select
      If (Not _row.IsNull(0)) Then
        grid_cash.Cell(idx_row, GRID_COLUMN_CURRENCY_TYPE_STOCK).Value = _row.Item(TB_COLUMN_GRID_TYPE)
        If _row.IsNull(TB_COLUMN_QUANTITY_STOCK) OrElse _row.Item(TB_COLUMN_QUANTITY_STOCK) = 0 Then
          grid_cash.Cell(idx_row, GRID_COLUMN_QUANTITY_STOCK).Value = ""
        Else
          grid_cash.Cell(idx_row, GRID_COLUMN_QUANTITY_STOCK).Value = SessionDetail.FormatAmount(_row.Item(TB_COLUMN_QUANTITY_STOCK), "-")
        End If
        grid_cash.Cell(idx_row, GRID_COLUMN_TOTAL_STOCK).Value = SessionDetail.FormatAmount(_row.Item(TB_COLUMN_TOTAL_STOCK), iso_code)

        grid_cash.Row(idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00) ' Total:
      End If
    Next

  End Sub ' InsertTotalsCash

  ' PURPOSE: Show cage stock
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Function ShowCageStock(ByVal CageStock As DataTable) As Boolean

    If m_grid_overflow Then
      Return False
    End If

    m_rw_current_total_stock = m_dt_totals.NewRow()

    '-- Fill Cash grid
    FillCashGrid(CageStock)

    '-- Fill Chips grid
    FillChipsGrid(CageStock)

    If Me.IsDisposed Then
      Return False
    End If

    Return True

  End Function ' ShowCageStock

  ' PURPOSE : Get Data And Fill chips grid
  '
  '  PARAMS :
  '     - INPUT : CageStock - datatable with all stock values 
  '
  ' RETURNS :
  Private Sub FillCashGrid(ByVal CageStock As DataTable)
    Dim _currencies_accepted As String
    Dim _sb As StringBuilder

    _sb = New StringBuilder()
    _currencies_accepted = "'" & GeneralParam.GetString("RegionalOptions", "CurrenciesAccepted").Replace(";", "', '") & "'"
    _currencies_accepted = _currencies_accepted.Replace(", 'X01'", "")

    _sb.Append(" TYPE < 1000")
    _sb.Append(" AND ((ISO_CODE IN (" & _currencies_accepted & ") AND ((QUANTITY > 0) OR (TOTAL > 0) OR (DENOMINATION = " & Cage.COINS_CODE & ")" & "))")
    _sb.Append(" OR (QUANTITY > 0 OR TOTAL > 0 )")
    _sb.Append(" AND  NOT (ISO_CODE = '" & Cage.CHIPS_ISO_CODE & "' OR DENOMINATION = 1))")
    _sb.Append(" AND  NOT (QUANTITY IS NULL OR TOTAL IS NULL )")

    Call FillGridValues(_sb.ToString(), CageStock, m_grid_cash)
    m_grid_cash.AutoScroll = True
    m_grid_cash.PanelRightVisible = False

  End Sub ' FillCashGrid

  ' PURPOSE : Get Data And Fill chips grid
  '
  '  PARAMS :
  '     - INPUT : CageStock - datatable with all stock values 
  '
  ' RETURNS :
  Private Sub FillChipsGrid(ByVal CageStock As DataTable)
    Dim _str_query As String

    _str_query = String.Empty
    _str_query = "TYPE > 1000 "

    Call CreateTotalsTable()
    m_rw_current_total_stock = m_dt_totals.NewRow()
    Call FillGridValues(_str_query, CageStock, m_grid_chips)
    m_grid_chips.AutoScroll = True
    m_grid_chips.PanelRightVisible = False

  End Sub ' FillChipsGrid


  ' PURPOSE : Get Data And Fill grids
  '
  '  PARAMS :
  '     - INPUT : CageStock - datatable with all stock values 
  '               str_query - Query to get values
  '               uc_grid - grid to fill
  ' RETURNS :
  Private Sub FillGridValues(ByVal str_query As String, ByVal CageStock As DataTable, ByVal uc_grid As uc_grid)
    Dim _dr As DataRow
    Dim _idx_row As Int32
    Dim _dec_quantity_stock As Decimal
    Dim _dec_total_stock As Decimal
    Dim _str_denomination As String
    Dim _str_iso_code As String
    Dim _currency_type_actual As Integer
    Dim _currency_type As Integer
    Dim _is_old_total_system As Boolean
    Dim _is_currency_chip As Integer
    Dim _insert_total_row As Boolean
    Dim _chip_value_color As Color
    Dim _is_first_time As Boolean

    _str_iso_code = String.Empty
    _str_denomination = String.Empty
    _insert_total_row = False
    _is_first_time = True

    For Each _dr In CageStock.Select(str_query, "ISO_CODE ASC, TYPE ASC, CHIP_SET_ID ASC, DENOMINATION DESC, CHIP_ID ASC")
      Call CheckRefreshGrid()

      If m_grid_overflow Or Me.IsDisposed Then
        Exit Sub
      End If

      If _dr.Item(SQL_COLUMN_DENOMINATION_STOCK) > 0 _
        Or (_dr.Item(SQL_COLUMN_DENOMINATION_STOCK) = Cage.COINS_CODE AndAlso _dr.Item(SQL_COLUMN_ISO_CODE_STOCK) <> Cage.CHIPS_ISO_CODE) _
        Or _dr.Item(SQL_COLUMN_DENOMINATION_STOCK) = Cage.CHECK_CODE _
        Or _dr.Item(SQL_COLUMN_DENOMINATION_STOCK) = Cage.BANK_CARD_CODE _
        Or _dr.Item(SQL_COLUMN_ISO_CODE_STOCK) = Cage.CHIPS_COLOR Then

        uc_grid.AddRow()

        _idx_row = uc_grid.NumRows - 1

        If _idx_row = 0 Then
          _currency_type_actual = _dr.Item(SQL_COLUMN_CURRENCY_TYPE_STOCK)
        End If

        If Not _is_first_time Then
          If uc_grid.Equals(m_grid_cash) AndAlso _str_iso_code <> Nothing AndAlso _str_iso_code <> _dr.Item(SQL_COLUMN_ISO_CODE_STOCK) Then
            m_dt_totals.Rows.Add(m_rw_current_total_stock)
            InsertTotalsCash(_str_iso_code, m_grid_cash, _idx_row)
            _currency_type_actual = _dr.Item(SQL_COLUMN_CURRENCY_TYPE_STOCK)
            _insert_total_row = True
            _is_first_time = True
          Else
            If uc_grid.Equals(m_grid_chips) AndAlso _dr.Item(SQL_COLUMN_CURRENCY_TYPE_STOCK) > 0 AndAlso _str_iso_code <> Nothing _
              AndAlso (_dr.Item(SQL_COLUMN_CURRENCY_TYPE_STOCK) <> _currency_type_actual Or _str_iso_code <> _dr.Item(SQL_COLUMN_ISO_CODE_STOCK)) Then
              m_dt_totals.Rows.Add(m_rw_current_total_stock)
              InsertTotalsChips(_str_iso_code, m_grid_chips, _idx_row)
              _currency_type_actual = _dr.Item(SQL_COLUMN_CURRENCY_TYPE_STOCK)
              _insert_total_row = True
              _is_first_time = True
            End If
          End If

        End If

        If _insert_total_row Then
          uc_grid.AddRow()
          _idx_row = uc_grid.NumRows - 1

        End If

        ' IsoCode
        If Not _dr.IsNull(SQL_COLUMN_ISO_CODE_STOCK) Then
          _str_iso_code = _dr.Item(SQL_COLUMN_ISO_CODE_STOCK)
          If _str_iso_code = Cage.CHIPS_COLOR Then
            uc_grid.Cell(_idx_row, GRID_COLUMN_ISO_CODE_STOCK).Value = GetCurrencyType(_dr.Item(SQL_COLUMN_CURRENCY_TYPE_STOCK).ToString())
          Else

            uc_grid.Cell(_idx_row, GRID_COLUMN_ISO_CODE_STOCK).Value = _dr.Item(SQL_COLUMN_ISO_CODE_STOCK)
          End If
        Else
          uc_grid.Cell(_idx_row, GRID_COLUMN_ISO_CODE_STOCK).Value = ""
        End If

        ' Color
        If Not _dr.IsNull(SQL_COLUMN_COLOR) Then
          _chip_value_color = Color.FromArgb(_dr.Item(SQL_COLUMN_COLOR))
          uc_grid.Cell(_idx_row, GRID_COLUMN_COLOR).BackColor = _chip_value_color
        End If

        ' Group
        If Not _dr.IsNull(SQL_COLUMN_GROUP_NAME) And FeatureChips.IsChipsType(CType(_currency_type_actual, CageCurrencyType)) Then
          uc_grid.Cell(_idx_row, GRID_COLUMN_GROUP_NAME).Value = _dr.Item(SQL_COLUMN_GROUP_NAME)
        End If

        ' Chip name
        If Not _dr.IsNull(SQL_COLUMN_CHIP_NAME) Then
          uc_grid.Cell(_idx_row, SQL_COLUMN_CHIP_NAME).Value = _dr.Item(SQL_COLUMN_CHIP_NAME)
        End If

        ' Picture
        If Not _dr.IsNull(SQL_COLUMN_PICTURE) And FeatureChips.IsChipsType(CType(_currency_type_actual, CageCurrencyType)) Then
          uc_grid.Cell(_idx_row, GRID_COLUMN_PICTURE).Value = _dr.Item(SQL_COLUMN_PICTURE)
        End If

        ' Denomination [number]
        If Not _dr.IsNull(SQL_COLUMN_DENOMINATION_STOCK) Then
          uc_grid.Cell(_idx_row, GRID_COLUMN_DENOMINATION_STOCK).Value = _dr.Item(SQL_COLUMN_DENOMINATION_STOCK)
        Else
          uc_grid.Cell(_idx_row, GRID_COLUMN_DENOMINATION_STOCK).Value = ""
        End If
        _str_denomination = uc_grid.Cell(_idx_row, GRID_COLUMN_DENOMINATION_STOCK).Value

        ' Denomination [number or text]
        If Not _dr.IsNull(SQL_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK) Then
          If IsNumeric(_dr.Item(SQL_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK).ToString()) Then
            uc_grid.Cell(_idx_row, GRID_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK).Value = SessionDetail.FormatAmount(_dr.Item(SQL_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK), "")
            ' Currency type
            If _str_iso_code <> Cage.CHIPS_ISO_CODE Then
              _currency_type_actual = _dr.Item(SQL_COLUMN_CURRENCY_TYPE_STOCK)
              uc_grid.Cell(_idx_row, GRID_COLUMN_CURRENCY_TYPE_STOCK).Value = GetCurrencyType(_dr.Item(SQL_COLUMN_CURRENCY_TYPE_STOCK).ToString())
            End If
          Else
            uc_grid.Cell(_idx_row, GRID_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK).Value = _dr.Item(SQL_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK)
          End If
        Else
          uc_grid.Cell(_idx_row, GRID_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK).Value = ""
        End If
        ' Quantity
        If Not _dr.IsNull(SQL_COLUMN_QUANTITY_STOCK) Then
          If (_dr.Item(SQL_COLUMN_DENOMINATION_STOCK) > 0 OrElse _str_denomination = Cage.TICKETS_CODE) Then
            uc_grid.Cell(_idx_row, GRID_COLUMN_QUANTITY_STOCK).Value = SessionDetail.FormatAmount(_dr.Item(SQL_COLUMN_QUANTITY_STOCK), "-")
          Else ' bank cards ,checks and tickets
            If _dr.Item(SQL_COLUMN_TOTAL_STOCK) > 0 AndAlso _str_denomination <> Cage.COINS_CODE Then
              uc_grid.Cell(_idx_row, GRID_COLUMN_QUANTITY_STOCK).Value = 1
            End If
          End If
        Else
          uc_grid.Cell(_idx_row, GRID_COLUMN_QUANTITY_STOCK).Value = ""
        End If


        ' Total Stock
        If uc_grid.Cell(_idx_row, GRID_COLUMN_DENOMINATION_STOCK).Value <> Cage.TICKETS_CODE Then
          If Not _dr.IsNull(SQL_COLUMN_TOTAL_STOCK) Then
            uc_grid.Cell(_idx_row, GRID_COLUMN_TOTAL_STOCK).Value = SessionDetail.FormatAmount(_dr.Item(SQL_COLUMN_TOTAL_STOCK), _str_iso_code)
          Else
            uc_grid.Cell(_idx_row, GRID_COLUMN_TOTAL_STOCK).Value = SessionDetail.FormatAmount(0, _str_iso_code)
          End If
        Else
          uc_grid.Cell(_idx_row, GRID_COLUMN_TOTAL_STOCK).Value = ""
        End If

        'Exception color chips 
        If _currency_type_actual = CageCurrencyType.ChipsColor Then
          If IsDBNull(_dr.Item(SQL_COLUMN_TOTAL_STOCK)) Then
            uc_grid.Cell(_idx_row, GRID_COLUMN_QUANTITY_STOCK).Value = SessionDetail.FormatAmount(0, "-")
          Else
            uc_grid.Cell(_idx_row, GRID_COLUMN_QUANTITY_STOCK).Value = SessionDetail.FormatAmount(_dr.Item(SQL_COLUMN_TOTAL_STOCK), "-")
          End If

          uc_grid.Cell(_idx_row, GRID_COLUMN_TOTAL_STOCK).Value = SessionDetail.FormatAmount(0, "-")
          uc_grid.Cell(_idx_row, GRID_COLUMN_DENOMINATION_STOCK).Value = ""
          uc_grid.Cell(_idx_row, GRID_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK).Value = ""
        End If

        _is_old_total_system = True
        _insert_total_row = False

        If (_currency_type <> _currency_type_actual AndAlso Not m_rw_current_total_stock(TB_COLUMN_ISO_CODE_STOCK).Equals(_str_iso_code)) OrElse _is_first_time Then
          _is_old_total_system = False
          _is_currency_chip = _currency_type
          _currency_type = _currency_type_actual
          _dec_quantity_stock = 0
          _dec_total_stock = 0
          m_rw_current_total_stock = m_dt_totals.NewRow()
        Else
          _is_old_total_system = True
        End If

        ' Update Stock totals table
        ' First time
        If m_rw_current_total_stock(TB_COLUMN_ISO_CODE_STOCK).Equals(DBNull.Value) AndAlso _is_first_time Then
          ' Total
          m_rw_current_total_stock.Item(TB_COLUMN_ISO_CODE_STOCK) = _dr.Item(SQL_COLUMN_ISO_CODE_STOCK)
          m_rw_current_total_stock.Item(TB_COLUMN_DENOMINATION_STOCK) = _dr.Item(SQL_COLUMN_DENOMINATION_STOCK)
          m_rw_current_total_stock.Item(TB_COLUMN_CURRENCY_TYPE) = _dr.Item(SQL_COLUMN_CURRENCY_TYPE_STOCK)
          m_rw_current_total_stock.Item(TB_COLUMN_GRID_TYPE) = GLB_NLS_GUI_INVOICING.GetString(205) ' Total:
          Decimal.TryParse(_dr.Item(SQL_COLUMN_QUANTITY_STOCK).ToString(), _dec_quantity_stock)
          Decimal.TryParse(_dr.Item(SQL_COLUMN_TOTAL_STOCK).ToString(), _dec_total_stock)
          If _str_denomination <> Cage.COINS_CODE Then
            m_rw_current_total_stock.Item(TB_COLUMN_QUANTITY_STOCK) = _dec_quantity_stock
          End If
          m_rw_current_total_stock.Item(TB_COLUMN_TOTAL_STOCK) = _dec_total_stock
          _is_first_time = False

        Else
          ' Update
          If _dr.Item(SQL_COLUMN_ISO_CODE_STOCK) = m_rw_current_total_stock(TB_COLUMN_ISO_CODE_STOCK) AndAlso _is_old_total_system Then
            Decimal.TryParse(_dr.Item(SQL_COLUMN_QUANTITY_STOCK).ToString(), _dec_quantity_stock)
            Decimal.TryParse(_dr.Item(SQL_COLUMN_TOTAL_STOCK).ToString(), _dec_total_stock)
            If _str_denomination <> Cage.COINS_CODE Then
              m_rw_current_total_stock.Item(TB_COLUMN_QUANTITY_STOCK) += _dec_quantity_stock
            End If
            m_rw_current_total_stock.Item(TB_COLUMN_TOTAL_STOCK) += _dec_total_stock

          End If
          ' Currency change [Add & New total]
          If _dr.Item(SQL_COLUMN_ISO_CODE_STOCK) <> m_rw_current_total_stock(TB_COLUMN_ISO_CODE_STOCK) OrElse Not _is_old_total_system Then
            m_dt_totals.Rows.Add(m_rw_current_total_stock)
            m_rw_current_total_stock = m_dt_totals.NewRow()
            m_rw_current_total_stock.Item(TB_COLUMN_ISO_CODE_STOCK) = _dr.Item(SQL_COLUMN_ISO_CODE_STOCK)
            m_rw_current_total_stock.Item(TB_COLUMN_DENOMINATION_STOCK) = _dr.Item(SQL_COLUMN_DENOMINATION_STOCK)
            m_rw_current_total_stock.Item(TB_COLUMN_CURRENCY_TYPE) = _dr.Item(SQL_COLUMN_CURRENCY_TYPE_STOCK)
            m_rw_current_total_stock.Item(TB_COLUMN_GRID_TYPE) = GLB_NLS_GUI_INVOICING.GetString(205) ' Total:
            Decimal.TryParse(_dr.Item(SQL_COLUMN_QUANTITY_STOCK).ToString(), _dec_quantity_stock)
            Decimal.TryParse(_dr.Item(SQL_COLUMN_TOTAL_STOCK).ToString(), _dec_total_stock)
            m_rw_current_total_stock.Item(TB_COLUMN_QUANTITY_STOCK) = _dec_quantity_stock
            m_rw_current_total_stock.Item(TB_COLUMN_TOTAL_STOCK) = _dec_total_stock

          End If
        End If
      End If
    Next

    If uc_grid.Equals(m_grid_cash) Then
      InsertTotalsCash(_str_iso_code, m_grid_cash, m_grid_cash.NumRows - 1, True)
      m_rw_current_total_stock_cash = m_rw_current_total_stock
    Else
      InsertTotalsChips(_str_iso_code, m_grid_chips, m_grid_chips.NumRows - 1, True)
      m_rw_current_total_stock_chips = m_rw_current_total_stock
    End If

  End Sub ' FillGridValues

  ' PURPOSE : Define the ExecuteQuery customized
  '
  '  PARAMS :
  '     - INPUT :
  ' RETURNS :
  Protected Sub GUI_ExecuteQueryCustom()

    Call CreateTotalsTable()

    Call m_theoric_stock.GetGlobalCageStock(True, False, True, True)

    Try

      ' Grid load and refresh
      m_grid_overflow = False
      m_grid_refresh_pending_rows = 0
      m_grid_refresh_step = Math.Max(MAX_RECORDS_FIRST_LOAD, MAX_RECORDS_LOAD / 20)
      m_grid_refresh_step = 10

      ' Refresh labels before executing SQL Query.
      Call Application.DoEvents()

      If m_theoric_stock.TableCurrencies().Rows.Count = 0 Then
        Return
      End If

      If Not ShowCageStock(m_theoric_stock.TableCurrencies()) Then

        If Me.IsDisposed Then

          Return
        End If

        ' 123 "Se ha producido un error al acceder a la base de datos."
        Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

        Return
      End If

    Catch _ex As Exception
      ' An error has occurred in the query execution
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Trace.WriteLine(_ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "Cage Stock", _
                            "GUI_ExecuteQueryCustom", _
                            _ex.Message)

    Finally
      ' Check if disposed.
      If Not Me.IsDisposed Then
        m_grid_cash.Redraw = True
        m_grid_chips.Redraw = True
      End If

      Call Application.DoEvents()

    End Try

  End Sub ' GUI_ExecuteQueryCustom

  ' PURPOSE : Previsualize  grids selected to print
  '
  '  PARAMS :
  '
  ' RETURNS :
  Private Sub PrintScreen()
    Dim _print_data As GUI_Reports.CLASS_PRINT_DATA
    Dim _prev_cursor As Windows.Forms.Cursor

    _prev_cursor = Me.Cursor
    Me.Cursor = Cursors.WaitCursor

    Try
      ' Save DateTime for Excel Print
      m_print_datetime = frm_base_sel.GUI_GetPrintDateTime()

      _print_data = New GUI_Reports.CLASS_PRINT_DATA()
      Call GUI_ReportFilter(_print_data)

      If tb_options.SelectedIndex = 0 Then
        frm_base_sel.GUI_GenerateExcel(m_grid_cash, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4561), m_print_datetime, _print_data, True)
      Else
        frm_base_sel.GUI_GenerateExcel(m_grid_chips, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4561), m_print_datetime, _print_data, True)
      End If

    Finally
      Me.Cursor = _prev_cursor
    End Try

  End Sub ' PrintScreen

  ' PURPOSE : Return NLS' to show on grid
  '
  '  PARAMS :
  '
  ' RETURNS :
  Private Function GetCurrencyType(ByVal currencyType) As String

    Dim _str As String

    _str = String.Empty

    Select Case currencyType
      Case CageCurrencyType.Coin
        _str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6734) 'moneda
      Case CageCurrencyType.Bill
        _str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2294) '"Billetes"
      Case CageCurrencyType.ChipsColor
        _str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1291) '"Color"
      Case CageCurrencyType.ChipsNoRedimible
        _str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224) '"No Redimibles"
      Case CageCurrencyType.ChipsRedimible
        _str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(250) '"Redimible"
      Case Else
        _str = String.Empty
    End Select

    Return _str

  End Function ' GetCurrencyType
#End Region

#Region " Overrides "

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    'Me.FormId = m_form_id
    'Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4561)

    '- Buttons
    ' Disable unused buttons: Delete button is not needed
    Me.GUI_Button(ENUM_BUTTON.BUTTON_FILTER_APPLY).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4348)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_FILTER_APPLY).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_FILTER_APPLY).TabStop = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_FILTER_RESET).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Enabled = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Enabled = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(27) ' Excel
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(1) ' Salir

    Me.panel_filter.Visible = False
    Me.pn_line.Visible = False

    m_theoric_stock = New CLASS_CAGE_CONTROL()

    Call SetLabelsText()
    Call GUI_StyleSheet()
    Call GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)

  End Sub ' GUI_InitControls

  ' PURPOSE : Actions to doing when button is clicked
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base.ENUM_BUTTON)

    If Me.m_show_only_stock And Me.m_first_time Then
      ButtonId = ENUM_BUTTON.BUTTON_CUSTOM_1
      Me.m_first_time = False
    End If

    Select Case ButtonId

      Case ENUM_BUTTON.BUTTON_CUSTOM_1 ' Stock de B�veda
      Case ENUM_BUTTON.BUTTON_FILTER_APPLY
        Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4561) '"Stock de b�veda"
        Call GUI_StyleSheet()
        Call GUI_ExecuteQueryCustom()

      Case ENUM_BUTTON.BUTTON_CANCEL
        MyBase.GUI_ButtonClick(ButtonId)

      Case ENUM_BUTTON.BUTTON_PRINT
        Call PrintScreen()

      Case Else
        MyBase.GUI_ButtonClick(ButtonId)

    End Select
  End Sub ' GUI_ButtonClick

  ' PURPOSE: Operations to be performed to generate the excel file
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ExcelResultList()

    Dim _print_data As GUI_Reports.CLASS_PRINT_DATA
    Dim _prev_cursor As Windows.Forms.Cursor
    Dim _report_layout As ExcelReportLayout
    Dim _sheet_layout As ExcelReportLayout.SheetLayout
    Dim _uc_grid_def As ExcelReportLayout.SheetLayout.UcGridDefinition

    ' Save DateTime for Excel Print
    m_print_datetime = frm_base_sel.GUI_GetPrintDateTime()

    _prev_cursor = Me.Cursor
    Cursor = Cursors.WaitCursor

    Try
      _print_data = New GUI_Reports.CLASS_PRINT_DATA()
      Call GUI_ReportFilter(_print_data)
      _report_layout = New ExcelReportLayout()

      With _report_layout
        .PrintData = _print_data
        .PrintDateTime = m_print_datetime
        For Each _tab_page As TabPage In Me.tb_options.TabPages
          _sheet_layout = New ExcelReportLayout.SheetLayout()
          _sheet_layout.Title = Me.FormTitle
          _sheet_layout.Name = _tab_page.Text

          _uc_grid_def = New ExcelReportLayout.SheetLayout.UcGridDefinition()
          _uc_grid_def.UcGrid = _tab_page.Controls(0)
          _uc_grid_def.Title = _tab_page.Text
          _sheet_layout.ListOfUcGrids.Add(_uc_grid_def)
          .ListOfSheets.Add(_sheet_layout)
        Next
      End With

      _report_layout.GUI_GenerateExcel()

    Finally
      Cursor = _prev_cursor
    End Try
  End Sub ' GUI_ExcelResultList
#End Region

End Class