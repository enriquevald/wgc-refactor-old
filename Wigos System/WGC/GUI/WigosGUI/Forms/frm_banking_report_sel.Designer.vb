﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_banking_report_sel
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.gb_card_type = New System.Windows.Forms.GroupBox()
    Me.chk_debit = New System.Windows.Forms.CheckBox()
    Me.chk_credit = New System.Windows.Forms.CheckBox()
    Me.chk_national = New System.Windows.Forms.CheckBox()
    Me.chk_linternational = New System.Windows.Forms.CheckBox()
    Me.gb_status = New System.Windows.Forms.GroupBox()
    Me.chk_canceled = New System.Windows.Forms.CheckBox()
    Me.chk_Declined = New System.Windows.Forms.CheckBox()
    Me.chk_NoResponse = New System.Windows.Forms.CheckBox()
    Me.chk_PendingWigos = New System.Windows.Forms.CheckBox()
    Me.chk_Initialized = New System.Windows.Forms.CheckBox()
    Me.chk_approved = New System.Windows.Forms.CheckBox()
    Me.chk_denied = New System.Windows.Forms.CheckBox()
    Me.chk_error = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_card_type.SuspendLayout()
    Me.gb_status.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_status)
    Me.panel_filter.Controls.Add(Me.gb_card_type)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Size = New System.Drawing.Size(1076, 103)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_card_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_status, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 107)
    Me.panel_data.Size = New System.Drawing.Size(1076, 450)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1070, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1070, 4)
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(3, 3)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(272, 100)
    Me.gb_date.TabIndex = 11
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 45)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(258, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 17)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(258, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'gb_card_type
    '
    Me.gb_card_type.Controls.Add(Me.chk_debit)
    Me.gb_card_type.Controls.Add(Me.chk_credit)
    Me.gb_card_type.Controls.Add(Me.chk_national)
    Me.gb_card_type.Controls.Add(Me.chk_linternational)
    Me.gb_card_type.Location = New System.Drawing.Point(520, 3)
    Me.gb_card_type.Name = "gb_card_type"
    Me.gb_card_type.Size = New System.Drawing.Size(124, 100)
    Me.gb_card_type.TabIndex = 12
    Me.gb_card_type.TabStop = False
    Me.gb_card_type.Text = "xCardType"
    Me.gb_card_type.Visible = False
    '
    'chk_debit
    '
    Me.chk_debit.AutoSize = True
    Me.chk_debit.Location = New System.Drawing.Point(11, 19)
    Me.chk_debit.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_debit.Name = "chk_debit"
    Me.chk_debit.Size = New System.Drawing.Size(63, 17)
    Me.chk_debit.TabIndex = 0
    Me.chk_debit.Text = "xDebit"
    Me.chk_debit.UseVisualStyleBackColor = True
    '
    'chk_credit
    '
    Me.chk_credit.AutoSize = True
    Me.chk_credit.Location = New System.Drawing.Point(11, 38)
    Me.chk_credit.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_credit.Name = "chk_credit"
    Me.chk_credit.Size = New System.Drawing.Size(68, 17)
    Me.chk_credit.TabIndex = 1
    Me.chk_credit.Text = "xCredit"
    Me.chk_credit.UseVisualStyleBackColor = True
    '
    'chk_national
    '
    Me.chk_national.AutoSize = True
    Me.chk_national.Location = New System.Drawing.Point(11, 57)
    Me.chk_national.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_national.Name = "chk_national"
    Me.chk_national.Size = New System.Drawing.Size(79, 17)
    Me.chk_national.TabIndex = 2
    Me.chk_national.Text = "xNational"
    Me.chk_national.UseVisualStyleBackColor = True
    '
    'chk_linternational
    '
    Me.chk_linternational.AutoSize = True
    Me.chk_linternational.Location = New System.Drawing.Point(11, 76)
    Me.chk_linternational.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_linternational.Name = "chk_linternational"
    Me.chk_linternational.Size = New System.Drawing.Size(106, 17)
    Me.chk_linternational.TabIndex = 3
    Me.chk_linternational.Text = "xInternational"
    Me.chk_linternational.UseVisualStyleBackColor = True
    '
    'gb_status
    '
    Me.gb_status.Controls.Add(Me.chk_canceled)
    Me.gb_status.Controls.Add(Me.chk_Declined)
    Me.gb_status.Controls.Add(Me.chk_NoResponse)
    Me.gb_status.Controls.Add(Me.chk_PendingWigos)
    Me.gb_status.Controls.Add(Me.chk_Initialized)
    Me.gb_status.Controls.Add(Me.chk_approved)
    Me.gb_status.Controls.Add(Me.chk_denied)
    Me.gb_status.Controls.Add(Me.chk_error)
    Me.gb_status.Location = New System.Drawing.Point(281, 3)
    Me.gb_status.Name = "gb_status"
    Me.gb_status.Size = New System.Drawing.Size(233, 100)
    Me.gb_status.TabIndex = 13
    Me.gb_status.TabStop = False
    Me.gb_status.Text = "xStatus"
    '
    'chk_canceled
    '
    Me.chk_canceled.AutoSize = True
    Me.chk_canceled.Location = New System.Drawing.Point(11, 76)
    Me.chk_canceled.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_canceled.Name = "chk_canceled"
    Me.chk_canceled.Size = New System.Drawing.Size(86, 17)
    Me.chk_canceled.TabIndex = 3
    Me.chk_canceled.Text = "xCanceled"
    Me.chk_canceled.UseVisualStyleBackColor = True
    '
    'chk_Declined
    '
    Me.chk_Declined.AutoSize = True
    Me.chk_Declined.Location = New System.Drawing.Point(105, 76)
    Me.chk_Declined.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_Declined.Name = "chk_Declined"
    Me.chk_Declined.Size = New System.Drawing.Size(82, 17)
    Me.chk_Declined.TabIndex = 0
    Me.chk_Declined.Text = "xDeclined"
    Me.chk_Declined.UseVisualStyleBackColor = True
    '
    'chk_NoResponse
    '
    Me.chk_NoResponse.AutoSize = True
    Me.chk_NoResponse.Location = New System.Drawing.Point(105, 57)
    Me.chk_NoResponse.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_NoResponse.Name = "chk_NoResponse"
    Me.chk_NoResponse.Size = New System.Drawing.Size(103, 17)
    Me.chk_NoResponse.TabIndex = 0
    Me.chk_NoResponse.Text = "xNoResponse"
    Me.chk_NoResponse.UseVisualStyleBackColor = True
    '
    'chk_PendingWigos
    '
    Me.chk_PendingWigos.AutoSize = True
    Me.chk_PendingWigos.Location = New System.Drawing.Point(105, 38)
    Me.chk_PendingWigos.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_PendingWigos.Name = "chk_PendingWigos"
    Me.chk_PendingWigos.Size = New System.Drawing.Size(112, 17)
    Me.chk_PendingWigos.TabIndex = 0
    Me.chk_PendingWigos.Text = "xPendingWigos"
    Me.chk_PendingWigos.UseVisualStyleBackColor = True
    '
    'chk_Initialized
    '
    Me.chk_Initialized.AutoSize = True
    Me.chk_Initialized.Location = New System.Drawing.Point(105, 20)
    Me.chk_Initialized.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_Initialized.Name = "chk_Initialized"
    Me.chk_Initialized.Size = New System.Drawing.Size(88, 17)
    Me.chk_Initialized.TabIndex = 0
    Me.chk_Initialized.Text = "xInitialized"
    Me.chk_Initialized.UseVisualStyleBackColor = True
    '
    'chk_approved
    '
    Me.chk_approved.AutoSize = True
    Me.chk_approved.Location = New System.Drawing.Point(11, 19)
    Me.chk_approved.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_approved.Name = "chk_approved"
    Me.chk_approved.Size = New System.Drawing.Size(88, 17)
    Me.chk_approved.TabIndex = 0
    Me.chk_approved.Text = "xApproved"
    Me.chk_approved.UseVisualStyleBackColor = True
    '
    'chk_denied
    '
    Me.chk_denied.AutoSize = True
    Me.chk_denied.Location = New System.Drawing.Point(11, 38)
    Me.chk_denied.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_denied.Name = "chk_denied"
    Me.chk_denied.Size = New System.Drawing.Size(73, 17)
    Me.chk_denied.TabIndex = 1
    Me.chk_denied.Text = "xDenied"
    Me.chk_denied.UseVisualStyleBackColor = True
    '
    'chk_error
    '
    Me.chk_error.AutoSize = True
    Me.chk_error.Location = New System.Drawing.Point(11, 57)
    Me.chk_error.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_error.Name = "chk_error"
    Me.chk_error.Size = New System.Drawing.Size(62, 17)
    Me.chk_error.TabIndex = 2
    Me.chk_error.Text = "xError"
    Me.chk_error.UseVisualStyleBackColor = True
    '
    'frm_banking_report_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1084, 561)
    Me.Name = "frm_banking_report_sel"
    Me.Text = "frm_banking_report_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gb_card_type.ResumeLayout(False)
    Me.gb_card_type.PerformLayout()
    Me.gb_status.ResumeLayout(False)
    Me.gb_status.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents chk_approved As System.Windows.Forms.CheckBox
  Friend WithEvents chk_denied As System.Windows.Forms.CheckBox
  Friend WithEvents chk_error As System.Windows.Forms.CheckBox
  Friend WithEvents gb_card_type As System.Windows.Forms.GroupBox
  Friend WithEvents chk_debit As System.Windows.Forms.CheckBox
  Friend WithEvents chk_credit As System.Windows.Forms.CheckBox
  Friend WithEvents chk_national As System.Windows.Forms.CheckBox
  Friend WithEvents chk_linternational As System.Windows.Forms.CheckBox
  Friend WithEvents chk_canceled As System.Windows.Forms.CheckBox
  Friend WithEvents chk_Declined As System.Windows.Forms.CheckBox
  Friend WithEvents chk_NoResponse As System.Windows.Forms.CheckBox
  Friend WithEvents chk_PendingWigos As System.Windows.Forms.CheckBox
  Friend WithEvents chk_Initialized As System.Windows.Forms.CheckBox
End Class
