'-------------------------------------------------------------------
' Copyright � 2011 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_alarms
' DESCRIPTION:   This screen allows to view the history of all alarms
'                and also monitorize the last alarms in the system
' AUTHOR:        Raul Cervera
' CREATION DATE: 15-JUL-2011
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 15-JUL-2011  RCI    Initial version
'--------------------------------------------------------------------
Option Explicit On 
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data

Public Class frm_alarms
  Inherits frm_base_sel

  Public Structure TYPE_ALARM
    Dim code As Integer
    Dim name_nls_id As Integer
  End Structure

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents gb_alarms As System.Windows.Forms.GroupBox
  Friend WithEvents dg_alarms As GUI_Controls.uc_grid
  Friend WithEvents gb_severity As System.Windows.Forms.GroupBox
  Friend WithEvents cb_error As System.Windows.Forms.CheckBox
  Friend WithEvents cb_warning As System.Windows.Forms.CheckBox
  Friend WithEvents cb_ok As System.Windows.Forms.CheckBox
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents btn_uncheck_all As System.Windows.Forms.Button
  Friend WithEvents gb_mode As System.Windows.Forms.GroupBox
  Friend WithEvents opt_history_mode As System.Windows.Forms.RadioButton
  Friend WithEvents opt_monitor_mode As System.Windows.Forms.RadioButton
  Friend WithEvents lbl_last_alarms As System.Windows.Forms.Label
  Friend WithEvents btn_check_all As System.Windows.Forms.Button
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.gb_date = New System.Windows.Forms.GroupBox
    Me.dtp_to = New GUI_Controls.uc_date_picker
    Me.dtp_from = New GUI_Controls.uc_date_picker
    Me.gb_alarms = New System.Windows.Forms.GroupBox
    Me.btn_uncheck_all = New System.Windows.Forms.Button
    Me.btn_check_all = New System.Windows.Forms.Button
    Me.dg_alarms = New GUI_Controls.uc_grid
    Me.gb_severity = New System.Windows.Forms.GroupBox
    Me.cb_error = New System.Windows.Forms.CheckBox
    Me.cb_warning = New System.Windows.Forms.CheckBox
    Me.cb_ok = New System.Windows.Forms.CheckBox
    Me.uc_pr_list = New GUI_Controls.uc_provider
    Me.gb_mode = New System.Windows.Forms.GroupBox
    Me.opt_monitor_mode = New System.Windows.Forms.RadioButton
    Me.opt_history_mode = New System.Windows.Forms.RadioButton
    Me.lbl_last_alarms = New System.Windows.Forms.Label
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_alarms.SuspendLayout()
    Me.gb_severity.SuspendLayout()
    Me.gb_mode.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.lbl_last_alarms)
    Me.panel_filter.Controls.Add(Me.gb_mode)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Controls.Add(Me.gb_severity)
    Me.panel_filter.Controls.Add(Me.gb_alarms)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Size = New System.Drawing.Size(1099, 195)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_alarms, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_severity, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_mode, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_last_alarms, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 199)
    Me.panel_data.Size = New System.Drawing.Size(1099, 519)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1093, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1093, 4)
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(8, 6)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(272, 72)
    Me.gb_date.TabIndex = 1
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 37)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(230, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2003, 5, 20, 10, 37, 59, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 13)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(230, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2003, 5, 20, 0, 0, 0, 0)
    '
    'gb_alarms
    '
    Me.gb_alarms.Controls.Add(Me.btn_uncheck_all)
    Me.gb_alarms.Controls.Add(Me.btn_check_all)
    Me.gb_alarms.Controls.Add(Me.dg_alarms)
    Me.gb_alarms.Location = New System.Drawing.Point(623, 6)
    Me.gb_alarms.Name = "gb_alarms"
    Me.gb_alarms.Size = New System.Drawing.Size(323, 181)
    Me.gb_alarms.TabIndex = 5
    Me.gb_alarms.TabStop = False
    Me.gb_alarms.Text = "xAlarms"
    '
    'btn_uncheck_all
    '
    Me.btn_uncheck_all.Location = New System.Drawing.Point(101, 149)
    Me.btn_uncheck_all.Name = "btn_uncheck_all"
    Me.btn_uncheck_all.Size = New System.Drawing.Size(89, 29)
    Me.btn_uncheck_all.TabIndex = 1
    Me.btn_uncheck_all.Text = "xUncheckAll"
    Me.btn_uncheck_all.UseVisualStyleBackColor = True
    '
    'btn_check_all
    '
    Me.btn_check_all.Location = New System.Drawing.Point(6, 149)
    Me.btn_check_all.Name = "btn_check_all"
    Me.btn_check_all.Size = New System.Drawing.Size(89, 29)
    Me.btn_check_all.TabIndex = 0
    Me.btn_check_all.Text = "xCheckAll"
    Me.btn_check_all.UseVisualStyleBackColor = True
    '
    'dg_alarms
    '
    Me.dg_alarms.CurrentCol = -1
    Me.dg_alarms.CurrentRow = -1
    Me.dg_alarms.Location = New System.Drawing.Point(6, 14)
    Me.dg_alarms.Name = "dg_alarms"
    Me.dg_alarms.PanelRightVisible = False
    Me.dg_alarms.Redraw = True
    Me.dg_alarms.Size = New System.Drawing.Size(311, 135)
    Me.dg_alarms.Sortable = False
    Me.dg_alarms.SortAscending = True
    Me.dg_alarms.SortByCol = 0
    Me.dg_alarms.TabIndex = 2
    Me.dg_alarms.ToolTipped = True
    Me.dg_alarms.TopRow = -2
    '
    'gb_severity
    '
    Me.gb_severity.Controls.Add(Me.cb_error)
    Me.gb_severity.Controls.Add(Me.cb_warning)
    Me.gb_severity.Controls.Add(Me.cb_ok)
    Me.gb_severity.Location = New System.Drawing.Point(140, 98)
    Me.gb_severity.Name = "gb_severity"
    Me.gb_severity.Size = New System.Drawing.Size(140, 89)
    Me.gb_severity.TabIndex = 3
    Me.gb_severity.TabStop = False
    Me.gb_severity.Text = "xSeverity"
    '
    'cb_error
    '
    Me.cb_error.AutoSize = True
    Me.cb_error.Location = New System.Drawing.Point(6, 65)
    Me.cb_error.Name = "cb_error"
    Me.cb_error.Size = New System.Drawing.Size(75, 17)
    Me.cb_error.TabIndex = 2
    Me.cb_error.Text = "cb_error"
    Me.cb_error.UseVisualStyleBackColor = True
    '
    'cb_warning
    '
    Me.cb_warning.AutoSize = True
    Me.cb_warning.Location = New System.Drawing.Point(6, 42)
    Me.cb_warning.Name = "cb_warning"
    Me.cb_warning.Size = New System.Drawing.Size(91, 17)
    Me.cb_warning.TabIndex = 1
    Me.cb_warning.Text = "cb_warning"
    Me.cb_warning.UseVisualStyleBackColor = True
    '
    'cb_ok
    '
    Me.cb_ok.AutoSize = True
    Me.cb_ok.Location = New System.Drawing.Point(6, 20)
    Me.cb_ok.Name = "cb_ok"
    Me.cb_ok.Size = New System.Drawing.Size(60, 17)
    Me.cb_ok.TabIndex = 0
    Me.cb_ok.Text = "cb_ok"
    Me.cb_ok.UseVisualStyleBackColor = True
    '
    'uc_pr_list
    '
    Me.uc_pr_list.Location = New System.Drawing.Point(286, 3)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(338, 188)
    Me.uc_pr_list.TabIndex = 4
    '
    'gb_mode
    '
    Me.gb_mode.Controls.Add(Me.opt_history_mode)
    Me.gb_mode.Controls.Add(Me.opt_monitor_mode)
    Me.gb_mode.Location = New System.Drawing.Point(8, 98)
    Me.gb_mode.Name = "gb_mode"
    Me.gb_mode.Size = New System.Drawing.Size(126, 89)
    Me.gb_mode.TabIndex = 11
    Me.gb_mode.TabStop = False
    Me.gb_mode.Text = "xMode"
    '
    'opt_monitor_mode
    '
    Me.opt_monitor_mode.AutoSize = True
    Me.opt_monitor_mode.Location = New System.Drawing.Point(6, 19)
    Me.opt_monitor_mode.Name = "opt_monitor_mode"
    Me.opt_monitor_mode.Size = New System.Drawing.Size(74, 17)
    Me.opt_monitor_mode.TabIndex = 0
    Me.opt_monitor_mode.TabStop = True
    Me.opt_monitor_mode.Text = "xMonitor"
    Me.opt_monitor_mode.UseVisualStyleBackColor = True
    '
    'opt_history_mode
    '
    Me.opt_history_mode.AutoSize = True
    Me.opt_history_mode.Location = New System.Drawing.Point(6, 41)
    Me.opt_history_mode.Name = "opt_history_mode"
    Me.opt_history_mode.Size = New System.Drawing.Size(74, 17)
    Me.opt_history_mode.TabIndex = 1
    Me.opt_history_mode.TabStop = True
    Me.opt_history_mode.Text = "xHistoric"
    Me.opt_history_mode.UseVisualStyleBackColor = True
    '
    'lbl_last_alarms
    '
    Me.lbl_last_alarms.AutoSize = True
    Me.lbl_last_alarms.Location = New System.Drawing.Point(35, 82)
    Me.lbl_last_alarms.Name = "lbl_last_alarms"
    Me.lbl_last_alarms.Size = New System.Drawing.Size(80, 13)
    Me.lbl_last_alarms.TabIndex = 12
    Me.lbl_last_alarms.Text = "xLast alarms"
    '
    'frm_alarms
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(1107, 722)
    Me.Name = "frm_alarms"
    Me.Text = "frm_alarms"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gb_alarms.ResumeLayout(False)
    Me.gb_severity.ResumeLayout(False)
    Me.gb_severity.PerformLayout()
    Me.gb_mode.ResumeLayout(False)
    Me.gb_mode.PerformLayout()
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Constants "

  ' event type
  Const WCP_EVENT_TYPE_DEVICE_STATUS As Integer = 1
  Const WCP_EVENT_TYPE_OPERATION As Integer = 2

  ' priority
  Const COMMON_PRIORITY_INFO = 0
  Const COMMON_PRIORITY_WARNING = 1
  Const COMMON_PRIORITY_ERROR = 2

  ' Device Codes 
  Const COMMON_DEVICE_CODE_NO_DEVICE = 0
  Const COMMON_DEVICE_CODE_PRINTER = 1
  Const COMMON_DEVICE_CODE_NOTE_ACCEPTOR = 2
  Const COMMON_DEVICE_CODE_SMARTCARD_READER = 3
  Const COMMON_DEVICE_CODE_CUSTOMER_DISPLAY = 4
  Const COMMON_DEVICE_CODE_COIN_ACCEPTOR = 5
  Const COMMON_DEVICE_CODE_UPPER_DISPLAY = 6
  Const COMMON_DEVICE_CODE_BAR_CODE_READER = 7
  Const COMMON_DEVICE_CODE_INSTRUSION = 8
  Const COMMON_DEVICE_CODE_UPS = 9
  Const COMMON_DEVICE_CODE_MODULE_IO = 15
  ' SLE: Devices Codes Added or Removed 18/11/09
  'Const COMMON_DEVICE_CODE_COMMUNICATIONS = 8
  'Const COMMON_DEVICE_CODE_LKAS_SOFTWARE_VERSION = 10
  'Const COMMON_DEVICE_CODE_LKT_SOFTWARE_VERSION = 11
  'Const COMMON_DEVICE_CODE_FLASH_1 = 12
  'Const COMMON_DEVICE_CODE_FLASH_2 = 13
  'Const COMMON_DEVICE_CODE_KIOSK = 14
  ' Const COMMON_DEVICE_CODE_AGENCY_CONNECTION = 16

  Const COMMON_DEVICE_CODE_DISPLAY_MAIN = 17
  Const COMMON_DEVICE_CODE_DISPLAY_TOP = 18
  Const COMMON_DEVICE_CODE_TOUCH_PAD = 19
  Const COMMON_DEVICE_CODE_KEYBOARD = 20

  Const COMMON_DEVICE_CODE_UNICASH_KIT_LCD = 21
  Const COMMON_DEVICE_CODE_UNICASH_KIT_KEYPAD = 22
  Const COMMON_DEVICE_CODE_UNICASH_KIT_CARD_READER = 23
  Const COMMON_DEVICE_CODE_UNICASH_KIT_BOARD = 24

  Const COMMON_DEVICE_STATUS_UNKNOWN = 0
  Const COMMON_DEVICE_STATUS_OK = 1
  Const COMMON_DEVICE_STATUS_ERROR = 2
  Const COMMON_DEVICE_STATUS_NOT_INSTALLED = 100

  ' Play status
  Const COMMON_PLAY_STATUS_OK = 0
  Const COMMON_PLAY_STATUS_ERROR_GET_RNG = 1

  ' Printer status
  Const COMMON_DEVICE_STATUS_PRINTER_OFF = 3
  Const COMMON_DEVICE_STATUS_PRINTER_READY = 4
  Const COMMON_DEVICE_STATUS_PRINTER_NOT_READY = 5
  Const COMMON_DEVICE_STATUS_PRINTER_PAPER_OUT = 6
  Const COMMON_DEVICE_STATUS_PRINTER_PAPER_LOW = 7
  Const COMMON_DEVICE_STATUS_PRINTER_OFFLINE = 8

  ' Note acceptor status
  Const COMMON_DEVICE_STATUS_NOTE_ACCEPTOR_JAM = 3
  Const COMMON_DEVICE_STATUS_NOTE_ACCEPTOR_SN_ERROR = 4
  Const COMMON_DEVICE_STATUS_NOTE_ACCEPTOR_STACKER_FULL = 5

  ' Ups status
  Const COMMON_DEVICE_STATUS_UPS_NO_AC = 3
  Const COMMON_DEVICE_STATUS_UPS_BATTERY_LOW = 4
  Const COMMON_DEVICE_STATUS_UPS_OVERLOAD = 5
  Const COMMON_DEVICE_STATUS_UPS_OFF = 6
  Const COMMON_DEVICE_STATUS_UPS_BATTERY_FAIL = 7

  ' Smartcard status
  Const COMMON_DEVICE_STATUS_SMARTCARD_REMOVED = 3
  Const COMMON_DEVICE_STATUS_SMARTCARD_INSERTED = 4

  '' SLE: Devices Status Removed 18/11/09
  '' software status
  'Const COMMON_DEVICE_STATUS_VERSION_ERROR = 3
  'Const COMMON_DEVICE_STATUS_DL_ERROR = 4

  '' Kiosk status
  'Const COMMON_DEVICE_STATUS_KIOSK_ACTIVE = 3
  'Const COMMON_DEVICE_STATUS_KIOSK_NOT_ACTIVE = 4

  '' Agency connection
  'Const COMMON_DEVICE_STATUS_CONNECTED = 3
  'Const COMMON_DEVICE_STATUS_NOT_CONNECTED = 4

  ' Coin acceptor status
  Const COMMON_DEVICE_STATUS_COIN_ACCEPTOR_JAM = 3
  Const COMMON_DEVICE_STATUS_COIN_ACCEPTOR_SN_ERROR = 4
  Const COMMON_DEVICE_STATUS_COIN_ACCEPTOR_STACKER_FULL = 5

  Const COMMON_KIOSK_DOOR_1 = 1
  Const COMMON_KIOSK_DOOR_2 = 2
  Const COMMON_KIOSK_DOOR_3 = 3
  Const COMMON_KIOSK_DOOR_4 = 4
  Const COMMON_KIOSK_DOOR_5 = 5

  ' Operations
  Const COMMON_OPERATION_CODE_NO_OPERATION = 0
  Const COMMON_OPERATION_CODE_START = 1
  Const COMMON_OPERATION_CODE_SHUTDOWN = 2
  Const COMMON_OPERATION_CODE_RESTART = 3
  Const COMMON_OPERATION_CODE_TUNE_SCREEN = 4
  Const COMMON_OPERATION_CODE_LAUNCH_EXPLORER = 5
  Const COMMON_OPERATION_CODE_ASSIGN_KIOSK = 6
  Const COMMON_OPERATION_CODE_DEASSIGN_KIOSK = 7
  Const COMMON_OPERATION_CODE_UNLOCK_DOOR = 8
  Const COMMON_OPERATION_CODE_LOGIN = 9
  Const COMMON_OPERATION_CODE_LOGOUT = 10
  Const COMMON_OPERATION_CODE_DISPLAY_SETTINGS = 11
  Const COMMON_OPERATION_CODE_DOOR_OPENED = 12
  Const COMMON_OPERATION_CODE_DOOR_CLOSED = 13
  Const COMMON_OPERATION_CODE_BLOCK_KIOSK = 14
  Const COMMON_OPERATION_CODE_UNBLOCK_KIOSK = 15
  Const COMMON_OPERATION_CODE_JACKPOT_WON = 16
  Const COMMON_OPERATION_CODE_KIOSK_BLOCKED_HIGH_PRIZE = 17
  Const COMMON_OPERATION_CODE_CALL_ATTENDANT = 18
  Const COMMON_OPERATION_CODE_TEST_MODE_ENTER = 19
  Const COMMON_OPERATION_CODE_TEST_MODE_LEAVE = 20
  Const COMMON_OPERATION_CODE_LOGIN_ERROR = 21
  Const COMMON_OPERATION_CODE_ATTENDANT_MENU_ENTER = 22
  Const COMMON_OPERATION_CODE_ATTENDANT_MENU_EXIT = 23
  Const COMMON_OPERATION_CODE_OPERATOR_MENU_ENTER = 24
  Const COMMON_OPERATION_CODE_OPERATOR_MENU_EXIT = 25
  Const COMMON_OPERATION_CODE_CONFIG_SETTINGS_MODIFIED = 26
  Const COMMON_OPERATION_CODE_HANDPAY_REQUESTED = 27
  Const COMMON_OPERATION_CODE_HANDPAY_RESET = 28
  Const COMMON_OPERATION_CODE_CARD_ABANDONED = 29
  Const COMMON_OPERATION_CODE_SECURITY_CASHOUT = 30
  Const COMMON_OPERATION_CODE_TERMINAL_STATUS_CHANGE = 31

  Private Const SQL_COLUMN_EVENT_ID As Integer = 0
  Private Const SQL_COLUMN_REPORTED As Integer = 1
  Private Const SQL_COLUMN_TERMINAL_ID As Integer = 2
  Private Const SQL_COLUMN_DATETIME As Integer = 3
  Private Const SQL_COLUMN_EVENT_TYPE As Integer = 4
  Private Const SQL_COLUMN_EVENT_DATA As Integer = 5
  Private Const SQL_COLUMN_EVENT_DEVICE_CODE As Integer = 6
  Private Const SQL_COLUMN_EVENT_STATUS As Integer = 7
  Private Const SQL_COLUMN_EVENT_PRIORITY As Integer = 8
  Private Const SQL_COLUMN_OPERATION_CODE As Integer = 9
  Private Const SQL_COLUMN_OPERATION_DATA As Integer = 10

  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_DATETIME As Integer = 1
  Private Const GRID_COLUMN_TERMINAL_ID As Integer = 2
  Private Const GRID_COLUMN_PRIORITY As Integer = 3
  Private Const GRID_COLUMN_DEVICE As Integer = 4
  Private Const GRID_COLUMN_STATUS As Integer = 5

  Private Const GRID_COLUMNS As Integer = 6
  Private Const GRID_HEADER_ROWS As Integer = 1

  Private Const GRID_2_COLUMN_CODE As Integer = 0
  Private Const GRID_2_COLUMN_CHECKED As Integer = 1
  Private Const GRID_2_COLUMN_DESC As Integer = 2
  Private Const GRID_2_COLUMN_TYPE As Integer = 3
  Private Const GRID_2_COLUMNS As Integer = 4
  Private Const GRID_2_HEADER_ROWS As Integer = 0
  Private Const GRID_2_TAB As String = "    "

  Private Const TYPE_OPERATION As Integer = 1
  Private Const TYPE_DEVICE As Integer = 2

  Private Const DEVICE_AGENCY As Integer = 1
  Private Const DEVICE_KIOSK As Integer = 2
  Private Const DEVICE_BOTH As Integer = DEVICE_AGENCY + DEVICE_KIOSK

  Private Const MAX_KIOSK_LEN As Integer = 3

  'Device Status/priority
  Private Const DEVICE_PRIORITY_OK As Integer = 0
  Private Const DEVICE_PRIORITY_WARNING As Integer = 1
  Private Const DEVICE_PRIORITY_ERROR As Integer = 2

  'Counters
  Private Const COUNTER_DEVICE_ERROR As Integer = 1
  Private Const COUNTER_DEVICE_WARNING As Integer = 2
  Private Const COUNTER_DEVICE_OK As Integer = 3
  Private Const COUNTER_OPERATIONS As Integer = 4

  Private Const COLOR_DEVICE_OK_BACK = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_DEVICE_OK_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_DEVICE_ERROR_BACK = ENUM_GUI_COLOR.GUI_COLOR_RED_02
  Private Const COLOR_DEVICE_ERROR_FORE = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_DEVICE_WARNING_BACK = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
  Private Const COLOR_DEVICE_WARNING_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_OPERATION_BACK = ENUM_GUI_COLOR.GUI_COLOR_GREY_01
  Private Const COLOR_OPERATION_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00

  Private Const MAX_FILTER_REPORT = 4

#End Region ' Constants

#Region " Members "

  Private m_refreshing_grid As Boolean

  ' For report filters 
  Private m_terminals As String
  Private m_date_from As String
  Private m_date_to As String
  Private m_severity As String
  Private m_code As String

#End Region ' Members

#Region " OVERRIDES "

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_EVENTS_HISTORY

    ' TJG 01-FEB-2005
    ' Set the form icon from the embedded resource (ICO file must be built in the project as "Embedded Resource")
    ' Call could be made as GetManifestResourceStream("GUI_Auditor.GUI_Auditor.ico"))

    '------------------------------------------------
    ' XVV 13/04/2007
    ' Translate tu BASE Form
    ' Me.Icon = New Icon(System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream(Me.GetType(), "WigosGUI.ico"))
    '------------------------------------------------

    '------------------------------------------------
    'XVV 13/04/2007
    'Call Base Form proc
    Call MyBase.GUI_SetFormId()
    '------------------------------------------------

  End Sub ' GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_AUDITOR.GetString(267)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_AUDITOR.GetString(12)

    ' Date
    Me.gb_date.Text = GLB_NLS_GUI_ALARMS.GetString(443)
    Me.dtp_from.Text = GLB_NLS_GUI_AUDITOR.GetString(257)
    Me.dtp_to.Text = GLB_NLS_GUI_AUDITOR.GetString(258)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Operation
    Me.gb_alarms.Text = GLB_NLS_GUI_ALARMS.GetString(445)

    ' Providers - Terminals
    Dim _terminal_types() As Integer = Nothing
    ReDim Preserve _terminal_types(0 To 6)
    _terminal_types(0) = WSI.Common.TerminalTypes.WIN
    _terminal_types(1) = WSI.Common.TerminalTypes.T3GS
    _terminal_types(2) = WSI.Common.TerminalTypes.SAS_HOST
    _terminal_types(3) = WSI.Common.TerminalTypes.SITE_JACKPOT
    _terminal_types(4) = WSI.Common.TerminalTypes.MOBILE_BANK
    _terminal_types(5) = WSI.Common.TerminalTypes.MOBILE_BANK_IMB
    _terminal_types(6) = WSI.Common.TerminalTypes.ISTATS
    Call Me.uc_pr_list.Init(_terminal_types)

    'Severity
    Me.gb_severity.Text = GLB_NLS_GUI_ALARMS.GetString(448)
    Me.cb_ok.Text = GLB_NLS_GUI_ALARMS.GetString(356)
    Me.cb_warning.Text = GLB_NLS_GUI_ALARMS.GetString(358)
    Me.cb_error.Text = GLB_NLS_GUI_ALARMS.GetString(357)

    btn_check_all.Text = GLB_NLS_GUI_ALARMS.GetString(452)
    btn_uncheck_all.Text = GLB_NLS_GUI_ALARMS.GetString(453)

    Call GUI_StyleSheetDeviceOperation()

    Call GUI_StyleSheet()

    FillFilterGrid()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Events  selection
    If (GetEventIdListSelected() = "" And GetSeverityIdListSelected() = "") Then
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(104), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
      Me.dg_alarms.CurrentRow = 0

      'XVV 16/04/2007
      'Change Me.XXX for frm_events_history, compiler generate a warning
      Me.dg_alarms.CurrentCol = GRID_2_COLUMN_CHECKED
      Me.dg_alarms.Focus()
      Return False
    End If

    Return True
  End Function ' GUI_FilterCheck

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim str_sql As String

    ' Get Select and from
    str_sql = "SELECT  EH_EVENT_ID " & _
                   " , EH_REPORTED " & _
                   " , TE_NAME " & _
                   " , EH_DATETIME " & _
                   " , EH_EVENT_TYPE " & _
                   " , EH_EVENT_DATA " & _
                   " , EH_DEVICE_CODE " & _
                   " , EH_DEVICE_STATUS " & _
                   " , EH_DEVICE_PRIORITY " & _
                   " , EH_OPERATION_CODE " & _
                   " , EH_OPERATION_DATA " & _
                   " FROM EVENT_HISTORY " & _
                      "INNER JOIN TERMINALS " & _
                          "ON EH_TERMINAL_ID=TE_TERMINAL_ID"

    str_sql = str_sql & GetSqlWhere()

    str_sql = str_sql & " ORDER BY EH_DATETIME DESC, EH_EVENT_ID DESC"

    Return str_sql

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    ' Reported
    'XVV 16/04/2007
    'Change Me.XXX for frm_events_history, compiler generate a warning
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATETIME), _
                                                                        ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                        ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    ' Terminal
    'XVV 16/04/2007
    'Change Me.XXX for frm_events_history, compiler generate a warning
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_ID).Value = DbRow.Value(SQL_COLUMN_TERMINAL_ID)

    ' Event type
    'XVV 16/04/2007
    'Change Me.XXX for frm_events_history, compiler generate a warning

    Select Case DbRow.Value(SQL_COLUMN_EVENT_TYPE)

      Case WCP_EVENT_TYPE_DEVICE_STATUS
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DEVICE).Value = DecodeDevice(DbRow.Value(SQL_COLUMN_EVENT_DEVICE_CODE), DbRow.Value(SQL_COLUMN_EVENT_STATUS), 1)
        If Not IsDBNull(DbRow.Value(SQL_COLUMN_EVENT_PRIORITY)) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_PRIORITY).Value = DecodePriority(DbRow.Value(SQL_COLUMN_EVENT_PRIORITY))
        End If
        Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = DecodeDevice(DbRow.Value(SQL_COLUMN_EVENT_DEVICE_CODE), DbRow.Value(SQL_COLUMN_EVENT_STATUS), 2)

      Case WCP_EVENT_TYPE_OPERATION
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DEVICE).Value = DecodeOperationCode(DbRow.Value(SQL_COLUMN_OPERATION_CODE), DbRow.Value(SQL_COLUMN_OPERATION_DATA))
    End Select

    Return True

  End Function ' GUI_SetupRow

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.dtp_from
  End Sub ' GUI_SetInitialFocus

#Region " GUI Reports "

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(261) & " " & GLB_NLS_GUI_AUDITOR.GetString(257), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(261) & " " & GLB_NLS_GUI_AUDITOR.GetString(258), m_date_to)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_terminals)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)

    PrintData.SetFilter(GLB_NLS_GUI_ALARMS.GetString(445), m_code)

    PrintData.FilterValueWidth(1) = 3000
    PrintData.FilterValueWidth(2) = 1000
    PrintData.FilterValueWidth(3) = 5000
  End Sub ' GUI_ReportFilter

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    PrintData.Settings.Columns(0).Width = 2000
    PrintData.Settings.Columns(0).IsWidthFixed = True
    PrintData.Settings.Columns(1).Width = 2300
    PrintData.Settings.Columns(1).IsWidthFixed = True
    PrintData.Settings.Columns(2).Width = 2000
    PrintData.Settings.Columns(2).IsWidthFixed = True
    PrintData.Settings.Columns(3).Width = 5000

    Call MyBase.GUI_ReportParams(PrintData)

    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  Protected Overrides Sub GUI_ReportUpdateFilters()

    Dim no_one As Boolean
    Dim all As Boolean
    Dim counter As Integer

    m_terminals = ""
    m_date_from = ""
    m_date_to = ""
    m_severity = ""

    ' Date 
    If Me.dtp_from.Checked Then
      m_date_from = GUI_FormatDate(Me.dtp_from.Value, _
                                   ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Me.dtp_to.Checked Then
      m_date_to = GUI_FormatDate(Me.dtp_to.Value, _
                                 ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    no_one = True
    all = True
    If cb_ok.Checked Then
      m_severity = m_severity & Me.cb_ok.Text & ", "
      no_one = False
    Else
      all = False
    End If

    If cb_warning.Checked Then
      m_severity = m_severity & Me.cb_warning.Text & ", "
      no_one = False
    Else
      all = False
    End If

    If cb_error.Checked Then
      m_severity = m_severity & Me.cb_error.Text & ", "
      no_one = False
    Else
      all = False
    End If

    If m_severity.Length <> 0 Then
      m_severity = Strings.Left(m_severity, Len(m_severity) - 2)
    End If

    If no_one Or all Then
      m_severity = GLB_NLS_GUI_ALARMS.GetString(277)
    End If

    ' Providers - Terminals
    If Me.uc_pr_list.opt_all_types.Checked Then
      m_terminals = Me.uc_pr_list.opt_all_types.Text
    ElseIf Me.uc_pr_list.opt_several_types.Checked Then
      m_terminals = Me.uc_pr_list.opt_several_types.Text
    Else
      m_terminals = Me.uc_pr_list.cmb_terminal.TextValue
    End If

    Dim idx_rows As Integer
    Dim all_checked As Boolean

    all_checked = True

    For idx_rows = 0 To dg_alarms.NumRows - 1
      If Not dg_alarms.Cell(idx_rows, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then

        all_checked = False
        Exit For

      End If
    Next

    If all_checked = True Then
      m_code = GLB_NLS_GUI_AUDITOR.GetString(263)
    Else
      m_code = ""
      For idx_rows = 0 To dg_alarms.NumRows - 1
        If dg_alarms.Cell(idx_rows, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
          m_code = m_code & dg_alarms.Cell(idx_rows, GRID_2_COLUMN_DESC).Value & "; "

          counter = counter + 1
          If counter > MAX_FILTER_REPORT Then
            m_code = GLB_NLS_GUI_INVOICING.GetString(223)
            Exit For
          End If
        End If
      Next

    End If


  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region  ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      .Counter(COUNTER_DEVICE_OK).Visible = False
      .Counter(COUNTER_DEVICE_OK).BackColor = GetColor(COLOR_DEVICE_OK_BACK)

      .Counter(COUNTER_DEVICE_ERROR).Visible = False
      .Counter(COUNTER_DEVICE_ERROR).BackColor = GetColor(COLOR_DEVICE_ERROR_BACK)
      .Counter(COUNTER_DEVICE_ERROR).ForeColor = GetColor(COLOR_DEVICE_ERROR_FORE)

      .Counter(COUNTER_DEVICE_WARNING).Visible = False
      .Counter(COUNTER_DEVICE_WARNING).BackColor = GetColor(COLOR_DEVICE_WARNING_BACK)

      .Counter(COUNTER_OPERATIONS).Visible = False
      .Counter(COUNTER_OPERATIONS).BackColor = GetColor(COLOR_OPERATION_BACK)

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      '  Date Report
      'XVV 16/04/2007
      'Change Me.XXX for frm_events_history, compiler generate a warning
      '------------------------------------------------------------------------------------------------
      .Column(GRID_COLUMN_DATETIME).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(443)
      .Column(GRID_COLUMN_DATETIME).Width = 2100
      .Column(GRID_COLUMN_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      '  Terminal
      .Column(GRID_COLUMN_TERMINAL_ID).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(442)
      .Column(GRID_COLUMN_TERMINAL_ID).Width = 2500
      .Column(GRID_COLUMN_TERMINAL_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Severity
      .Column(GRID_COLUMN_PRIORITY).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(416)
      .Column(GRID_COLUMN_PRIORITY).Width = 1000
      .Column(GRID_COLUMN_PRIORITY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Device
      .Column(GRID_COLUMN_DEVICE).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(444)
      .Column(GRID_COLUMN_DEVICE).Width = 5700
      .Column(GRID_COLUMN_DEVICE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Status
      .Column(GRID_COLUMN_STATUS).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(447)
      .Column(GRID_COLUMN_STATUS).Width = 2000
      .Column(GRID_COLUMN_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT


      '----------------------------------------------------------------------------------------------------

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Me.dtp_from.Value = New DateTime(Now.Year, Now.Month, Now.Day, 0, 0, 0)

    Me.dtp_from.Checked = True

    Me.dtp_to.Value = Me.dtp_from.Value.AddDays(1)
    Me.dtp_to.Checked = False

    Call Me.uc_pr_list.SetDefaultValues()

    cb_ok.Checked = True
    cb_warning.Checked = True
    cb_error.Checked = True

    Call btn_check_all_Click(Nothing, Nothing)

  End Sub ' SetDefaultValues

  ' PURPOSE: Get Sql WHERE to build SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String

    Dim str_where As String = ""

    ' Filter Dates
    If Me.dtp_from.Checked = True Then
      str_where = str_where & " AND (EH_DATETIME >= " & GUI_FormatDateDB(dtp_from.Value) & ") "
    End If

    If Me.dtp_to.Checked = True Then
      str_where = str_where & " AND (EH_DATETIME < " & GUI_FormatDateDB(dtp_to.Value) & ") "
    End If

    ' Filter Providers - Terminals
    If Me.uc_pr_list.opt_one_terminal.Checked Then
      str_where = str_where & " AND TE_TERMINAL_ID = " & Me.uc_pr_list.GetProviderIdListSelected()
    Else
      str_where = str_where & " AND TE_TERMINAL_ID IN " & Me.uc_pr_list.GetProviderIdListSelected()
    End If

    If str_where <> "" Then
      str_where = Strings.Right(str_where, Len(str_where) - 5)
      str_where = " WHERE " & str_where
    End If

    Return str_where

  End Function ' GetSqlWhere

  Private Function DecodeDevice(ByVal DeviceNameId As Integer, ByVal DeviceStatusId As Integer, ByVal ReturnedValue As Integer) As String

    Dim DeviceName As String
    Dim DeviceStatus As String
    Dim Result As String

    DeviceName = ""
    DeviceStatus = ""

    Select Case DeviceNameId

      Case COMMON_DEVICE_CODE_PRINTER
        DeviceName = GLB_NLS_GUI_ALARMS.GetString(295)
        Select Case DeviceStatusId
          Case COMMON_DEVICE_STATUS_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
          Case COMMON_DEVICE_STATUS_PRINTER_OFF
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
          Case COMMON_DEVICE_STATUS_PRINTER_OFFLINE
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(306)
          Case COMMON_DEVICE_STATUS_PRINTER_NOT_READY
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(307)
          Case COMMON_DEVICE_STATUS_PRINTER_READY
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(321)
          Case COMMON_DEVICE_STATUS_PRINTER_PAPER_OUT
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(322)
          Case COMMON_DEVICE_STATUS_PRINTER_PAPER_LOW
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(323)
          Case COMMON_DEVICE_STATUS_UNKNOWN
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
          Case COMMON_DEVICE_STATUS_NOT_INSTALLED
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
          Case COMMON_DEVICE_STATUS_OK
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        End Select

      Case COMMON_DEVICE_CODE_NOTE_ACCEPTOR
        DeviceName = GLB_NLS_GUI_ALARMS.GetString(296)
        Select Case DeviceStatusId
          Case COMMON_DEVICE_STATUS_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
          Case COMMON_DEVICE_STATUS_NOTE_ACCEPTOR_SN_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(308)
          Case COMMON_DEVICE_STATUS_NOTE_ACCEPTOR_JAM
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(324)
          Case COMMON_DEVICE_STATUS_NOTE_ACCEPTOR_STACKER_FULL
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(325)
          Case COMMON_DEVICE_STATUS_UNKNOWN
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
          Case COMMON_DEVICE_STATUS_NOT_INSTALLED
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
          Case COMMON_DEVICE_STATUS_OK
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        End Select

      Case COMMON_DEVICE_CODE_SMARTCARD_READER
        DeviceName = GLB_NLS_GUI_ALARMS.GetString(297)
        Select Case DeviceStatusId
          Case COMMON_DEVICE_STATUS_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
          Case COMMON_DEVICE_STATUS_UNKNOWN
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
          Case COMMON_DEVICE_STATUS_NOT_INSTALLED
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
          Case COMMON_DEVICE_STATUS_OK
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        End Select

      Case COMMON_DEVICE_CODE_CUSTOMER_DISPLAY
        DeviceName = GLB_NLS_GUI_ALARMS.GetString(300)
        Select Case DeviceStatusId
          Case COMMON_DEVICE_STATUS_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
          Case COMMON_DEVICE_STATUS_UNKNOWN
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
          Case COMMON_DEVICE_STATUS_NOT_INSTALLED
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
          Case COMMON_DEVICE_STATUS_OK
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        End Select

      Case COMMON_DEVICE_CODE_UPPER_DISPLAY
        DeviceName = GLB_NLS_GUI_ALARMS.GetString(302)
        Select Case DeviceStatusId
          Case COMMON_DEVICE_STATUS_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
          Case COMMON_DEVICE_STATUS_UNKNOWN
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
          Case COMMON_DEVICE_STATUS_NOT_INSTALLED
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
          Case COMMON_DEVICE_STATUS_OK
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        End Select

      Case COMMON_DEVICE_CODE_BAR_CODE_READER
        DeviceName = GLB_NLS_GUI_ALARMS.GetString(298)
        Select Case DeviceStatusId
          Case COMMON_DEVICE_STATUS_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
          Case COMMON_DEVICE_STATUS_UNKNOWN
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
          Case COMMON_DEVICE_STATUS_NOT_INSTALLED
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
          Case COMMON_DEVICE_STATUS_OK
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        End Select

      Case COMMON_DEVICE_CODE_INSTRUSION
        DeviceName = GLB_NLS_GUI_ALARMS.GetString(374) ' Intrusion
        Select Case DeviceStatusId
          Case COMMON_DEVICE_STATUS_OK
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320) ' OK
          Case Else
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(375) ' Detected
        End Select


      Case COMMON_DEVICE_CODE_UPS
        DeviceName = GLB_NLS_GUI_ALARMS.GetString(304)
        Select Case DeviceStatusId
          Case COMMON_DEVICE_STATUS_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
          Case COMMON_DEVICE_STATUS_UPS_NO_AC
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(309)
          Case COMMON_DEVICE_STATUS_UPS_BATTERY_LOW
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(328)
          Case COMMON_DEVICE_STATUS_UPS_OVERLOAD
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(327)
          Case COMMON_DEVICE_STATUS_UPS_OFF
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(329)
          Case COMMON_DEVICE_STATUS_UPS_BATTERY_FAIL
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(330)
          Case COMMON_DEVICE_STATUS_UNKNOWN
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
          Case COMMON_DEVICE_STATUS_NOT_INSTALLED
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
          Case COMMON_DEVICE_STATUS_OK
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        End Select

        'Case COMMON_DEVICE_CODE_LKAS_SOFTWARE_VERSION
        '  DeviceName = GLB_NLS_GUI_ALARMS.GetString(312)
        '  Select Case DeviceStatusId
        '    Case COMMON_DEVICE_STATUS_ERROR
        '      DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
        '    Case COMMON_DEVICE_STATUS_UNKNOWN
        '      DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
        '    Case COMMON_DEVICE_STATUS_NOT_INSTALLED
        '      DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
        '    Case COMMON_DEVICE_STATUS_OK
        '      DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        '  End Select

        'Case COMMON_DEVICE_CODE_LKT_SOFTWARE_VERSION
        '  DeviceName = GLB_NLS_GUI_ALARMS.GetString(311)
        '  Select Case DeviceStatusId
        '    Case COMMON_DEVICE_STATUS_ERROR
        '      DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
        '    Case COMMON_DEVICE_STATUS_UNKNOWN
        '      DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
        '    Case COMMON_DEVICE_STATUS_NOT_INSTALLED
        '      DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
        '    Case COMMON_DEVICE_STATUS_OK
        '      DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        '  End Select

        'Case COMMON_DEVICE_CODE_FLASH_1
        '  DeviceName = GLB_NLS_GUI_ALARMS.GetString(315)
        '  Select Case DeviceStatusId
        '    Case COMMON_DEVICE_STATUS_ERROR
        '      DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
        '    Case COMMON_DEVICE_STATUS_UNKNOWN
        '      DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
        '    Case COMMON_DEVICE_STATUS_OK
        '      DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        '  End Select

        'Case COMMON_DEVICE_CODE_FLASH_2
        '  DeviceName = GLB_NLS_GUI_ALARMS.GetString(316)
        '  Select Case DeviceStatusId
        '    Case COMMON_DEVICE_STATUS_ERROR
        '      DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
        '    Case COMMON_DEVICE_STATUS_UNKNOWN
        '      DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
        '    Case COMMON_DEVICE_STATUS_OK
        '      DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        '  End Select

        'Case COMMON_DEVICE_CODE_KIOSK
        '  DeviceName = GLB_NLS_GUI_ALARMS.GetString(341)
        '  Select Case DeviceStatusId
        '    Case COMMON_DEVICE_STATUS_UNKNOWN
        '      DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
        'Case COMMON_DEVICE_STATUS_KIOSK_ACTIVE
        '  DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(333)
        'Case COMMON_DEVICE_STATUS_KIOSK_NOT_ACTIVE
        '  DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(334)
        'End Select

        'Case COMMON_DEVICE_CODE_AGENCY_CONNECTION
        '  DeviceName = GLB_NLS_GUI_ALARMS.GetString(343)
        '  Select Case DeviceStatusId
        '    Case COMMON_DEVICE_STATUS_CONNECTED
        '      DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(344)
        '    Case COMMON_DEVICE_STATUS_NOT_CONNECTED
        '      DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(345)
        '  End Select

      Case COMMON_DEVICE_CODE_MODULE_IO
        DeviceName = GLB_NLS_GUI_ALARMS.GetString(348)
        Select Case DeviceStatusId
          Case COMMON_DEVICE_STATUS_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
          Case COMMON_DEVICE_STATUS_UNKNOWN
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
          Case COMMON_DEVICE_STATUS_NOT_INSTALLED
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
          Case COMMON_DEVICE_STATUS_OK
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        End Select

      Case COMMON_DEVICE_CODE_COIN_ACCEPTOR
        DeviceName = GLB_NLS_GUI_ALARMS.GetString(350)
        Select Case DeviceStatusId
          Case COMMON_DEVICE_STATUS_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
          Case COMMON_DEVICE_STATUS_COIN_ACCEPTOR_SN_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(308)
          Case COMMON_DEVICE_STATUS_COIN_ACCEPTOR_JAM
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(324)
          Case COMMON_DEVICE_STATUS_COIN_ACCEPTOR_STACKER_FULL
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(325)
          Case COMMON_DEVICE_STATUS_UNKNOWN
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
          Case COMMON_DEVICE_STATUS_NOT_INSTALLED
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
          Case COMMON_DEVICE_STATUS_OK
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        End Select

      Case COMMON_DEVICE_CODE_UNICASH_KIT_LCD
        DeviceName = GLB_NLS_GUI_ALARMS.GetString(370)
        Select Case DeviceStatusId
          Case COMMON_DEVICE_STATUS_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
          Case COMMON_DEVICE_STATUS_UNKNOWN
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
          Case COMMON_DEVICE_STATUS_NOT_INSTALLED
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
          Case COMMON_DEVICE_STATUS_OK
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        End Select

      Case COMMON_DEVICE_CODE_UNICASH_KIT_KEYPAD
        DeviceName = GLB_NLS_GUI_ALARMS.GetString(371)
        Select Case DeviceStatusId
          Case COMMON_DEVICE_STATUS_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
          Case COMMON_DEVICE_STATUS_UNKNOWN
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
          Case COMMON_DEVICE_STATUS_NOT_INSTALLED
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
          Case COMMON_DEVICE_STATUS_OK
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        End Select

      Case COMMON_DEVICE_CODE_UNICASH_KIT_CARD_READER
        DeviceName = GLB_NLS_GUI_ALARMS.GetString(372)
        Select Case DeviceStatusId
          Case COMMON_DEVICE_STATUS_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
          Case COMMON_DEVICE_STATUS_UNKNOWN
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
          Case COMMON_DEVICE_STATUS_NOT_INSTALLED
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
          Case COMMON_DEVICE_STATUS_OK
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        End Select

      Case COMMON_DEVICE_CODE_UNICASH_KIT_BOARD
        DeviceName = GLB_NLS_GUI_ALARMS.GetString(373)
        Select Case DeviceStatusId
          Case COMMON_DEVICE_STATUS_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
          Case COMMON_DEVICE_STATUS_UNKNOWN
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
          Case COMMON_DEVICE_STATUS_NOT_INSTALLED
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
          Case COMMON_DEVICE_STATUS_OK
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        End Select

      Case COMMON_DEVICE_CODE_DISPLAY_MAIN
        DeviceName = GLB_NLS_GUI_ALARMS.GetString(379)
        Select Case DeviceStatusId
          Case COMMON_DEVICE_STATUS_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
          Case COMMON_DEVICE_STATUS_UNKNOWN
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
          Case COMMON_DEVICE_STATUS_NOT_INSTALLED
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
          Case COMMON_DEVICE_STATUS_OK
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        End Select

      Case COMMON_DEVICE_CODE_DISPLAY_TOP
        DeviceName = GLB_NLS_GUI_ALARMS.GetString(380)
        Select Case DeviceStatusId
          Case COMMON_DEVICE_STATUS_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
          Case COMMON_DEVICE_STATUS_UNKNOWN
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
          Case COMMON_DEVICE_STATUS_NOT_INSTALLED
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
          Case COMMON_DEVICE_STATUS_OK
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        End Select

      Case COMMON_DEVICE_CODE_TOUCH_PAD
        DeviceName = GLB_NLS_GUI_ALARMS.GetString(381)
        Select Case DeviceStatusId
          Case COMMON_DEVICE_STATUS_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
          Case COMMON_DEVICE_STATUS_UNKNOWN
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
          Case COMMON_DEVICE_STATUS_NOT_INSTALLED
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
          Case COMMON_DEVICE_STATUS_OK
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        End Select

      Case COMMON_DEVICE_CODE_KEYBOARD
        DeviceName = GLB_NLS_GUI_ALARMS.GetString(382)
        Select Case DeviceStatusId
          Case COMMON_DEVICE_STATUS_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
          Case COMMON_DEVICE_STATUS_UNKNOWN
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
          Case COMMON_DEVICE_STATUS_NOT_INSTALLED
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
          Case COMMON_DEVICE_STATUS_OK
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        End Select

    End Select
    Result = ""
    If ReturnedValue = 1 Then
      Result = DeviceName
    End If
    If ReturnedValue = 2 Then
      Result = DeviceStatus
    End If

    Return Result
  End Function ' DecodeDevice


  Private Function DecodePriority(ByVal Id As Integer) As String

    Dim result As String

    result = ""
    Select Case Id
      Case COMMON_PRIORITY_INFO
        result = GLB_NLS_GUI_ALARMS.GetString(356)
      Case COMMON_PRIORITY_ERROR
        result = GLB_NLS_GUI_ALARMS.GetString(357)
      Case COMMON_PRIORITY_WARNING
        result = GLB_NLS_GUI_ALARMS.GetString(358)
    End Select

    Return result
  End Function ' DecodePriority

  Private Function DecodeOperationCode(ByVal Id As Integer, ByVal Data As Decimal) As String

    Dim result As String
    Dim aux As String

    result = ""
    Select Case Id
      Case COMMON_OPERATION_CODE_START
        result = GLB_NLS_GUI_ALARMS.GetString(331)
      Case COMMON_OPERATION_CODE_SHUTDOWN
        result = GLB_NLS_GUI_ALARMS.GetString(332)
      Case COMMON_OPERATION_CODE_RESTART
        result = GLB_NLS_GUI_ALARMS.GetString(336)
      Case COMMON_OPERATION_CODE_LAUNCH_EXPLORER
        result = GLB_NLS_GUI_ALARMS.GetString(337)
      Case COMMON_OPERATION_CODE_TUNE_SCREEN
        result = GLB_NLS_GUI_ALARMS.GetString(338)
      Case COMMON_OPERATION_CODE_ASSIGN_KIOSK
        result = GLB_NLS_GUI_ALARMS.GetString(339, GUI_FormatNumber(Data, 0))
      Case COMMON_OPERATION_CODE_DEASSIGN_KIOSK
        result = GLB_NLS_GUI_ALARMS.GetString(340, GUI_FormatNumber(Data, 0))
      Case COMMON_OPERATION_CODE_UNLOCK_DOOR
        result = GLB_NLS_GUI_ALARMS.GetString(335)
      Case COMMON_OPERATION_CODE_LOGIN
        result = GLB_NLS_GUI_ALARMS.GetString(346, GUI_FormatNumber(Data, 0))
      Case COMMON_OPERATION_CODE_LOGOUT
        result = GLB_NLS_GUI_ALARMS.GetString(347, GUI_FormatNumber(Data, 0))
      Case COMMON_OPERATION_CODE_DISPLAY_SETTINGS
        result = GLB_NLS_GUI_ALARMS.GetString(349)
      Case COMMON_OPERATION_CODE_DOOR_OPENED
        result = GLB_NLS_GUI_ALARMS.GetString(352, GUI_FormatNumber(Data, 0))
      Case COMMON_OPERATION_CODE_DOOR_CLOSED
        result = GLB_NLS_GUI_ALARMS.GetString(353, GUI_FormatNumber(Data, 0))
      Case COMMON_OPERATION_CODE_BLOCK_KIOSK
        result = GLB_NLS_GUI_ALARMS.GetString(354)
      Case COMMON_OPERATION_CODE_UNBLOCK_KIOSK
        result = GLB_NLS_GUI_ALARMS.GetString(355)
      Case COMMON_OPERATION_CODE_JACKPOT_WON
        result = GLB_NLS_GUI_ALARMS.GetString(359, GUI_FormatCurrency(Data))
      Case COMMON_OPERATION_CODE_KIOSK_BLOCKED_HIGH_PRIZE
        result = GLB_NLS_GUI_ALARMS.GetString(360, GUI_FormatCurrency(Data))
      Case COMMON_OPERATION_CODE_CALL_ATTENDANT
        result = GLB_NLS_GUI_ALARMS.GetString(361)
      Case COMMON_OPERATION_CODE_TEST_MODE_ENTER
        result = GLB_NLS_GUI_ALARMS.GetString(362)
      Case COMMON_OPERATION_CODE_TEST_MODE_LEAVE
        result = GLB_NLS_GUI_ALARMS.GetString(363)
      Case COMMON_OPERATION_CODE_LOGIN_ERROR
        result = GLB_NLS_GUI_ALARMS.GetString(364)
      Case COMMON_OPERATION_CODE_ATTENDANT_MENU_ENTER
        result = GLB_NLS_GUI_ALARMS.GetString(365)
      Case COMMON_OPERATION_CODE_ATTENDANT_MENU_EXIT
        result = GLB_NLS_GUI_ALARMS.GetString(366)
      Case COMMON_OPERATION_CODE_OPERATOR_MENU_ENTER
        result = GLB_NLS_GUI_ALARMS.GetString(367)
      Case COMMON_OPERATION_CODE_OPERATOR_MENU_EXIT
        result = GLB_NLS_GUI_ALARMS.GetString(368)
      Case COMMON_OPERATION_CODE_CONFIG_SETTINGS_MODIFIED
        result = GLB_NLS_GUI_ALARMS.GetString(369)
      Case COMMON_OPERATION_CODE_HANDPAY_REQUESTED
        aux = GLB_NLS_GUI_ALARMS.GetString(378, GUI_FormatCurrency(Data / 100))
        result = GLB_NLS_GUI_ALARMS.GetString(376, aux)
      Case COMMON_OPERATION_CODE_HANDPAY_RESET
        aux = GLB_NLS_GUI_ALARMS.GetString(378, GUI_FormatCurrency(Data / 100))
        result = GLB_NLS_GUI_ALARMS.GetString(377, aux)
      Case COMMON_OPERATION_CODE_CARD_ABANDONED
        aux = GLB_NLS_GUI_ALARMS.GetString(385, GUI_FormatNumber(Data, 0))
        result = GLB_NLS_GUI_ALARMS.GetString(383, aux)
      Case COMMON_OPERATION_CODE_SECURITY_CASHOUT
        aux = GLB_NLS_GUI_ALARMS.GetString(378, GUI_FormatCurrency(Data / 100))
        result = GLB_NLS_GUI_ALARMS.GetString(384, aux)
      Case COMMON_OPERATION_CODE_TERMINAL_STATUS_CHANGE
        aux = GetTerminalStatusString(((GUI_FormatNumber(Data, 0) / 100) - 1) Mod 3) + " -> " + GetTerminalStatusString((GUI_FormatNumber(Data, 0) Mod 100) Mod 3)
        result = GLB_NLS_GUI_ALARMS.GetString(386, aux)
    End Select

    Return result
  End Function ' DecodeOperationCode

  Private Function GetTerminalStatusString(ByVal Code As Integer) As String
    Select Case Code
      Case WSI.Common.TerminalStatus.ACTIVE
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(397)

      Case WSI.Common.TerminalStatus.OUT_OF_SERVICE
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(398)

      Case WSI.Common.TerminalStatus.RETIRED
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(399)

      Case Else
        Return "Unknown"

    End Select
  End Function

#Region " Grid Event "

  ' PURPOSE: Define all Grid Event Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheetDeviceOperation()
    With Me.dg_alarms
      Call .Init(GRID_2_COLUMNS, GRID_2_HEADER_ROWS, True)
      .Counter(0).Visible = False
      .Sortable = False

      ' Hidden colums
      .Column(GRID_2_COLUMN_CODE).Header.Text = ""
      .Column(GRID_2_COLUMN_CODE).WidthFixed = 0

      ' GRID_COL_CHECKBOX
      .Column(GRID_2_COLUMN_CHECKED).Header.Text = ""
      .Column(GRID_2_COLUMN_CHECKED).WidthFixed = 400
      .Column(GRID_2_COLUMN_CHECKED).Fixed = True
      .Column(GRID_2_COLUMN_CHECKED).Editable = True
      .Column(GRID_2_COLUMN_CHECKED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX

      ' GRID_COL_DESCRIPTION
      .Column(GRID_2_COLUMN_DESC).Header.Text = ""
      .Column(GRID_2_COLUMN_DESC).WidthFixed = 3900
      .Column(GRID_2_COLUMN_DESC).Fixed = True
      .Column(GRID_2_COLUMN_DESC).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' GRID_COL_TYPE
      .Column(GRID_2_COLUMN_TYPE).Header.Text = ""
      .Column(GRID_2_COLUMN_TYPE).WidthFixed = 0
    End With

  End Sub 'GUI_StyleSheetType

  ' PURPOSE: Get envents
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Events TYPE_EVENT()
  Public Function GetEventsFilters() As TYPE_ALARM()
    Dim events(30) As TYPE_ALARM
    Dim ev As TYPE_ALARM

    ev.code = COMMON_OPERATION_CODE_START
    ev.name_nls_id = 331
    events(0) = ev
    ev.code = COMMON_OPERATION_CODE_SHUTDOWN
    ev.name_nls_id = 332
    events(1) = ev
    ev.code = COMMON_OPERATION_CODE_RESTART
    ev.name_nls_id = 336
    events(2) = ev
    ev.code = COMMON_OPERATION_CODE_LAUNCH_EXPLORER
    ev.name_nls_id = 337
    events(3) = ev
    ev.code = COMMON_OPERATION_CODE_TUNE_SCREEN
    ev.name_nls_id = 338
    events(4) = ev
    ev.code = COMMON_OPERATION_CODE_ASSIGN_KIOSK
    ev.name_nls_id = 339
    events(5) = ev
    ev.code = COMMON_OPERATION_CODE_DEASSIGN_KIOSK
    ev.name_nls_id = 340
    events(6) = ev
    ev.code = COMMON_OPERATION_CODE_UNLOCK_DOOR
    ev.name_nls_id = 335
    events(7) = ev
    ev.code = COMMON_OPERATION_CODE_LOGIN
    ev.name_nls_id = 346
    events(8) = ev
    ev.code = COMMON_OPERATION_CODE_LOGOUT
    ev.name_nls_id = 347
    events(9) = ev
    ev.code = COMMON_OPERATION_CODE_DISPLAY_SETTINGS
    ev.name_nls_id = 349
    events(10) = ev
    ev.code = COMMON_OPERATION_CODE_DOOR_OPENED
    ev.name_nls_id = 352
    events(11) = ev
    ev.code = COMMON_OPERATION_CODE_DOOR_CLOSED
    ev.name_nls_id = 353
    events(12) = ev
    ev.code = COMMON_OPERATION_CODE_BLOCK_KIOSK
    ev.name_nls_id = 354
    events(13) = ev
    ev.code = COMMON_OPERATION_CODE_UNBLOCK_KIOSK
    ev.name_nls_id = 355
    events(14) = ev
    ev.code = COMMON_OPERATION_CODE_JACKPOT_WON
    ev.name_nls_id = 359
    events(15) = ev
    ev.code = COMMON_OPERATION_CODE_KIOSK_BLOCKED_HIGH_PRIZE
    ev.name_nls_id = 360
    events(16) = ev
    ev.code = COMMON_OPERATION_CODE_CALL_ATTENDANT
    ev.name_nls_id = 361
    events(17) = ev
    ev.code = COMMON_OPERATION_CODE_TEST_MODE_ENTER
    ev.name_nls_id = 362
    events(18) = ev
    ev.code = COMMON_OPERATION_CODE_TEST_MODE_LEAVE
    ev.name_nls_id = 363
    events(19) = ev
    ev.code = COMMON_OPERATION_CODE_LOGIN_ERROR
    ev.name_nls_id = 364
    events(20) = ev
    ev.code = COMMON_OPERATION_CODE_ATTENDANT_MENU_ENTER
    ev.name_nls_id = 365
    events(21) = ev
    ev.code = COMMON_OPERATION_CODE_ATTENDANT_MENU_EXIT
    ev.name_nls_id = 366
    events(22) = ev
    ev.code = COMMON_OPERATION_CODE_OPERATOR_MENU_ENTER
    ev.name_nls_id = 367
    events(23) = ev
    ev.code = COMMON_OPERATION_CODE_OPERATOR_MENU_EXIT
    ev.name_nls_id = 368
    events(24) = ev
    ev.code = COMMON_OPERATION_CODE_CONFIG_SETTINGS_MODIFIED
    ev.name_nls_id = 369
    events(25) = ev
    ev.code = COMMON_OPERATION_CODE_HANDPAY_REQUESTED
    ev.name_nls_id = 376
    events(26) = ev
    ev.code = COMMON_OPERATION_CODE_HANDPAY_RESET
    ev.name_nls_id = 377
    events(27) = ev
    ev.code = COMMON_OPERATION_CODE_CARD_ABANDONED
    ev.name_nls_id = 383
    events(28) = ev
    ev.code = COMMON_OPERATION_CODE_SECURITY_CASHOUT
    ev.name_nls_id = 384
    events(29) = ev
    ev.code = COMMON_OPERATION_CODE_TERMINAL_STATUS_CHANGE
    ev.name_nls_id = 386
    events(30) = ev

    Return events

  End Function


  ' PURPOSE: Fill the filter Grid with data
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub FillFilterGrid()
    Dim events() As TYPE_ALARM
    Dim idx_event As Integer

    events = GetEventsFilters()

    Me.dg_alarms.Clear()
    Me.dg_alarms.Redraw = False

    idx_event = 0
    For Each ev As TYPE_ALARM In events
      Call AddOneFilterRowDevice(ev.code, WCP_EVENT_TYPE_OPERATION, GLB_NLS_GUI_ALARMS.GetString(ev.name_nls_id))
      idx_event += 1
    Next

    If Me.dg_alarms.NumRows > 0 Then
      Me.dg_alarms.CurrentRow = 0
    End If

    Me.dg_alarms.Redraw = True

  End Sub

  Private Function GetTypeName(ByVal EventType As Integer) As String
    Dim type_name As String
    type_name = ""
    Select Case EventType
      Case WCP_EVENT_TYPE_DEVICE_STATUS
        type_name = GLB_NLS_GUI_ALARMS.GetString(416)
      Case WCP_EVENT_TYPE_OPERATION
        type_name = GLB_NLS_GUI_ALARMS.GetString(445)
    End Select
    Return type_name
  End Function

  ' PURPOSE: Add a Row of a event to the Event grid according to 
  '          the given code
  '
  '  PARAMS:
  '     - INPUT:
  '           - Code 
  '   
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub AddOneFilterRowDevice(ByVal EventCode As Integer, ByVal EventType As Integer, ByVal EventName As String)
    Dim prev_redraw As Boolean

    prev_redraw = Me.dg_alarms.Redraw
    Me.dg_alarms.Redraw = False

    dg_alarms.AddRow()

    Me.dg_alarms.Redraw = False

    'XVV 16/04/2007
    'Change Me.XXX for frm_events_history, compiler generate a warning
    '-----------------------------------------------------------------------------------------------------
    dg_alarms.Cell(dg_alarms.NumRows - 1, GRID_2_COLUMN_TYPE).Value = EventType
    dg_alarms.Cell(dg_alarms.NumRows - 1, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
    dg_alarms.Cell(dg_alarms.NumRows - 1, GRID_2_COLUMN_CODE).Value = EventCode
    dg_alarms.Cell(dg_alarms.NumRows - 1, GRID_2_COLUMN_DESC).Value = GRID_2_TAB & EventName
    '-----------------------------------------------------------------------------------------------------

    Me.dg_alarms.Redraw = prev_redraw
  End Sub


  ' PURPOSE: Change checked value to all subrows (rows not headers)
  '
  '  PARAMS:
  '     - INPUT:
  '           - FirstRow
  '           - ValueChecked
  '
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ChangeCheckofRows(ByVal FirstRow As Integer, ByVal ValueChecked As String)
    Dim idx As Integer
    Dim type_id_str As String

    For idx = FirstRow + 1 To dg_alarms.NumRows - 1
      type_id_str = Me.dg_alarms.Cell(idx, GRID_2_COLUMN_CODE).Value
      If type_id_str.Length = 0 Then

        Exit Sub
      End If

      Me.dg_alarms.Cell(idx, GRID_2_COLUMN_CHECKED).Value = ValueChecked

    Next

  End Sub

  ' PURPOSE: Uncheck all subrows (rows not headers)
  '
  '  PARAMS:
  '     - INPUT:
  '           - FirstRow
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub TryToUncheckHeader(ByVal FirstRow As Integer)
    Dim idx As Integer

    For idx = FirstRow - 1 To 0 Step -1

      If Me.dg_alarms.Cell(idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED Then

        Exit Sub
      End If
    Next
  End Sub


  ' PURPOSE: Get Event Id list for selected operation
  '          Format list to build query: X, X, X
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Function GetEventIdListSelected() As String
    Dim idx_row As Integer
    Dim list_type As String
    Dim no_one As Boolean
    no_one = True

    list_type = ""


    For idx_row = 0 To Me.dg_alarms.NumRows - 1

      'XVV 16/04/2007
      'Change Me.XXX for frm_events_history, compiler generate a warning
      '------------------------------------------------------------------------------------------------
      If dg_alarms.Cell(idx_row, GRID_2_COLUMN_TYPE).Value = WCP_EVENT_TYPE_OPERATION Then
        If dg_alarms.Cell(idx_row, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED And _
           dg_alarms.Cell(idx_row, GRID_2_COLUMN_CODE).Value.Length > 0 Then
          If list_type.Length = 0 Then
            list_type = dg_alarms.Cell(idx_row, GRID_2_COLUMN_CODE).Value
            no_one = False
          Else
            list_type = list_type & ", " & dg_alarms.Cell(idx_row, GRID_2_COLUMN_CODE).Value
            no_one = False
          End If
        End If
      End If
      '------------------------------------------------------------------------------------------------
    Next

    If no_one Then
      For idx_row = 0 To Me.dg_alarms.NumRows - 1
        If list_type.Length = 0 Then
          list_type = dg_alarms.Cell(idx_row, GRID_2_COLUMN_CODE).Value
        Else
          list_type = list_type & ", " & dg_alarms.Cell(idx_row, GRID_2_COLUMN_CODE).Value
        End If
      Next
    End If

    Return list_type
  End Function 'GetEventIdListSelected



  ' PURPOSE: Get Event Id list for selected severity
  '          Format list to build query: X, X, X
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Function GetSeverityIdListSelected() As String
    Dim list_type As String
    Dim no_one As Boolean
    no_one = True

    list_type = ""

    If cb_ok.Checked Then
      If list_type.Length = 0 Then
        list_type = COMMON_PRIORITY_INFO
        no_one = False
      Else
        list_type = list_type & ", " & COMMON_PRIORITY_INFO
        no_one = False
      End If
    End If

    If cb_warning.Checked Then
      If list_type.Length = 0 Then
        list_type = COMMON_PRIORITY_WARNING
        no_one = False
      Else
        list_type = list_type & ", " & COMMON_PRIORITY_WARNING
        no_one = False
      End If
    End If

    If cb_error.Checked Then
      If list_type.Length = 0 Then
        list_type = COMMON_PRIORITY_ERROR
        no_one = False
      Else
        list_type = list_type & ", " & COMMON_PRIORITY_ERROR
        no_one = False
      End If
    End If

    If no_one Then
      list_type = COMMON_PRIORITY_INFO
      list_type = list_type & ", " & COMMON_PRIORITY_WARNING
      list_type = list_type & ", " & COMMON_PRIORITY_ERROR
    End If

    Return list_type
  End Function 'GetEventIdListSelected


#End Region ' Grid Event

#End Region  ' Private Functions

#Region " Events "

  Private Sub dg_devices_DataSelectedEvent() Handles dg_alarms.DataSelectedEvent

    Dim idx As Integer
    Dim last_row As Integer

    'XVV 16/04/2007
    'Unnused variable, disabled declaration because compiler generate a warning
    'Dim row_is_header As Boolean

    With Me.dg_alarms

      'row_is_header = RowIsHeader(.CurrentRow)

      'If row_is_header And .CurrentCol <> GRID_2_COLUMN_CHECKED Then
      If .CurrentCol <> GRID_2_COLUMN_CHECKED Then
        last_row = .CurrentRow
        For idx = .CurrentRow + 1 To .NumRows - 1
          'If RowIsHeader(idx) Then

          '  Exit For
          'End If
          last_row = idx
        Next

        'Me.dg_devices.CollapseExpand(.CurrentRow, last_row, GRID_2_COLUMN_DESC)

      End If
    End With
  End Sub ' dg_devices_DataSelectedEvent

  Private Sub dg_devices_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer) Handles dg_alarms.CellDataChangedEvent
    Dim current_row As Integer

    If m_refreshing_grid = True Then

      Exit Sub
    End If

    With Me.dg_alarms

      current_row = .CurrentRow
      m_refreshing_grid = True


      'If RowIsHeader(.CurrentRow) Then
      '  Call ChangeCheckofRows(.CurrentRow, _
      '                           .Cell(.CurrentRow, GRID_2_COLUMN_CHECKED).Value)
      'Else
      If .Cell(.CurrentRow, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED Then
        Call TryToUncheckHeader(.CurrentRow)
      End If
      'End If

      m_refreshing_grid = False
      .CurrentRow = current_row

    End With
  End Sub ' dg_devices_CellDataChangedEvent

  Private Sub btn_check_all_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_check_all.Click
    Dim idx_rows As Integer
    For idx_rows = 0 To dg_alarms.NumRows - 1
      dg_alarms.Cell(idx_rows, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
    Next
  End Sub

  Private Sub btn_uncheck_all_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_uncheck_all.Click
    Dim idx_rows As Integer
    For idx_rows = 0 To dg_alarms.NumRows - 1
      dg_alarms.Cell(idx_rows, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    Next
  End Sub

#End Region ' Events

End Class
