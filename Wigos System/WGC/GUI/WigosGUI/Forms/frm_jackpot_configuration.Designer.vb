<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_jackpot_configuration
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.gb_gen_values = New System.Windows.Forms.GroupBox
    Me.btn_load_last_month_played = New GUI_Controls.uc_button
    Me.ef_last_month_played = New GUI_Controls.uc_entry_field
    Me.ef_contribution = New GUI_Controls.uc_entry_field
    Me.gb_jackpot_flag = New System.Windows.Forms.GroupBox
    Me.opt_enabled = New System.Windows.Forms.RadioButton
    Me.opt_disabled = New System.Windows.Forms.RadioButton
    Me.gb_wrk_days = New System.Windows.Forms.GroupBox
    Me.dtp_working_from = New GUI_Controls.uc_date_picker
    Me.dtp_working_to = New GUI_Controls.uc_date_picker
    Me.chk_sunday = New System.Windows.Forms.CheckBox
    Me.chk_saturday = New System.Windows.Forms.CheckBox
    Me.chk_friday = New System.Windows.Forms.CheckBox
    Me.chk_thursday = New System.Windows.Forms.CheckBox
    Me.chk_wednesday = New System.Windows.Forms.CheckBox
    Me.chk_tuesday = New System.Windows.Forms.CheckBox
    Me.chk_monday = New System.Windows.Forms.CheckBox
    Me.dg_jackpot_list = New GUI_Controls.uc_grid
    Me.gb_parameters = New System.Windows.Forms.GroupBox
    Me.lbl_min_jackpot_block = New System.Windows.Forms.Label
    Me.cmb_min_jack_block = New GUI_Controls.uc_combo
    Me.lbl_recent_interval = New System.Windows.Forms.Label
    Me.cmb_recent_interval = New GUI_Controls.uc_combo
    Me.lbl_animation_interval = New System.Windows.Forms.Label
    Me.cmb_anim_interval = New GUI_Controls.uc_combo
    Me.lbl_block_interval = New System.Windows.Forms.Label
    Me.lbl_block_mode = New System.Windows.Forms.Label
    Me.cmb_block_interval = New GUI_Controls.uc_combo
    Me.cmb_block_mode = New GUI_Controls.uc_combo
    Me.panel_data.SuspendLayout()
    Me.gb_gen_values.SuspendLayout()
    Me.gb_jackpot_flag.SuspendLayout()
    Me.gb_wrk_days.SuspendLayout()
    Me.gb_parameters.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_parameters)
    Me.panel_data.Controls.Add(Me.gb_wrk_days)
    Me.panel_data.Controls.Add(Me.dg_jackpot_list)
    Me.panel_data.Controls.Add(Me.gb_gen_values)
    Me.panel_data.Controls.Add(Me.gb_jackpot_flag)
    Me.panel_data.Size = New System.Drawing.Size(946, 362)
    '
    'gb_gen_values
    '
    Me.gb_gen_values.Controls.Add(Me.btn_load_last_month_played)
    Me.gb_gen_values.Controls.Add(Me.ef_last_month_played)
    Me.gb_gen_values.Controls.Add(Me.ef_contribution)
    Me.gb_gen_values.Location = New System.Drawing.Point(153, 3)
    Me.gb_gen_values.Name = "gb_gen_values"
    Me.gb_gen_values.Size = New System.Drawing.Size(786, 86)
    Me.gb_gen_values.TabIndex = 8
    Me.gb_gen_values.TabStop = False
    Me.gb_gen_values.Text = "xPercentages"
    '
    'btn_load_last_month_played
    '
    Me.btn_load_last_month_played.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_load_last_month_played.Location = New System.Drawing.Point(292, 47)
    Me.btn_load_last_month_played.Name = "btn_load_last_month_played"
    Me.btn_load_last_month_played.Size = New System.Drawing.Size(90, 30)
    Me.btn_load_last_month_played.TabIndex = 6
    Me.btn_load_last_month_played.ToolTipped = False
    Me.btn_load_last_month_played.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'ef_last_month_played
    '
    Me.ef_last_month_played.DoubleValue = 0
    Me.ef_last_month_played.IntegerValue = 0
    Me.ef_last_month_played.IsReadOnly = False
    Me.ef_last_month_played.Location = New System.Drawing.Point(6, 50)
    Me.ef_last_month_played.Name = "ef_last_month_played"
    Me.ef_last_month_played.Size = New System.Drawing.Size(268, 24)
    Me.ef_last_month_played.SufixText = "Sufix Text"
    Me.ef_last_month_played.SufixTextVisible = True
    Me.ef_last_month_played.TabIndex = 5
    Me.ef_last_month_played.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_last_month_played.TextValue = ""
    Me.ef_last_month_played.TextWidth = 160
    Me.ef_last_month_played.Value = ""
    '
    'ef_contribution
    '
    Me.ef_contribution.DoubleValue = 0
    Me.ef_contribution.IntegerValue = 0
    Me.ef_contribution.IsReadOnly = False
    Me.ef_contribution.Location = New System.Drawing.Point(6, 20)
    Me.ef_contribution.Name = "ef_contribution"
    Me.ef_contribution.Size = New System.Drawing.Size(242, 24)
    Me.ef_contribution.SufixText = "%"
    Me.ef_contribution.SufixTextVisible = True
    Me.ef_contribution.SufixTextWidth = 20
    Me.ef_contribution.TabIndex = 4
    Me.ef_contribution.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_contribution.TextValue = ""
    Me.ef_contribution.TextWidth = 160
    Me.ef_contribution.Value = ""
    '
    'gb_jackpot_flag
    '
    Me.gb_jackpot_flag.Controls.Add(Me.opt_enabled)
    Me.gb_jackpot_flag.Controls.Add(Me.opt_disabled)
    Me.gb_jackpot_flag.Location = New System.Drawing.Point(3, 3)
    Me.gb_jackpot_flag.Name = "gb_jackpot_flag"
    Me.gb_jackpot_flag.Size = New System.Drawing.Size(144, 86)
    Me.gb_jackpot_flag.TabIndex = 7
    Me.gb_jackpot_flag.TabStop = False
    Me.gb_jackpot_flag.Text = "XEstado"
    '
    'opt_enabled
    '
    Me.opt_enabled.Location = New System.Drawing.Point(16, 22)
    Me.opt_enabled.Name = "opt_enabled"
    Me.opt_enabled.Size = New System.Drawing.Size(88, 16)
    Me.opt_enabled.TabIndex = 0
    Me.opt_enabled.Text = "xEnabled"
    '
    'opt_disabled
    '
    Me.opt_disabled.Location = New System.Drawing.Point(16, 48)
    Me.opt_disabled.Name = "opt_disabled"
    Me.opt_disabled.Size = New System.Drawing.Size(112, 16)
    Me.opt_disabled.TabIndex = 1
    Me.opt_disabled.Text = "xDisabled"
    '
    'gb_wrk_days
    '
    Me.gb_wrk_days.Controls.Add(Me.dtp_working_from)
    Me.gb_wrk_days.Controls.Add(Me.dtp_working_to)
    Me.gb_wrk_days.Controls.Add(Me.chk_sunday)
    Me.gb_wrk_days.Controls.Add(Me.chk_saturday)
    Me.gb_wrk_days.Controls.Add(Me.chk_friday)
    Me.gb_wrk_days.Controls.Add(Me.chk_thursday)
    Me.gb_wrk_days.Controls.Add(Me.chk_wednesday)
    Me.gb_wrk_days.Controls.Add(Me.chk_tuesday)
    Me.gb_wrk_days.Controls.Add(Me.chk_monday)
    Me.gb_wrk_days.Enabled = False
    Me.gb_wrk_days.Location = New System.Drawing.Point(3, 95)
    Me.gb_wrk_days.Name = "gb_wrk_days"
    Me.gb_wrk_days.Size = New System.Drawing.Size(144, 262)
    Me.gb_wrk_days.TabIndex = 9
    Me.gb_wrk_days.TabStop = False
    Me.gb_wrk_days.Text = "xWorking Days"
    '
    'dtp_working_from
    '
    Me.dtp_working_from.Checked = True
    Me.dtp_working_from.IsReadOnly = False
    Me.dtp_working_from.Location = New System.Drawing.Point(6, 22)
    Me.dtp_working_from.Name = "dtp_working_from"
    Me.dtp_working_from.ShowCheckBox = False
    Me.dtp_working_from.ShowUpDown = False
    Me.dtp_working_from.Size = New System.Drawing.Size(112, 24)
    Me.dtp_working_from.SufixText = "Sufix Text"
    Me.dtp_working_from.SufixTextVisible = True
    Me.dtp_working_from.TabIndex = 15
    Me.dtp_working_from.TextWidth = 50
    Me.dtp_working_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_working_to
    '
    Me.dtp_working_to.Checked = True
    Me.dtp_working_to.IsReadOnly = False
    Me.dtp_working_to.Location = New System.Drawing.Point(6, 46)
    Me.dtp_working_to.Name = "dtp_working_to"
    Me.dtp_working_to.ShowCheckBox = False
    Me.dtp_working_to.ShowUpDown = False
    Me.dtp_working_to.Size = New System.Drawing.Size(112, 24)
    Me.dtp_working_to.SufixText = "Sufix Text"
    Me.dtp_working_to.SufixTextVisible = True
    Me.dtp_working_to.TabIndex = 16
    Me.dtp_working_to.TextWidth = 50
    Me.dtp_working_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'chk_sunday
    '
    Me.chk_sunday.AutoSize = True
    Me.chk_sunday.Location = New System.Drawing.Point(16, 229)
    Me.chk_sunday.Name = "chk_sunday"
    Me.chk_sunday.Size = New System.Drawing.Size(76, 17)
    Me.chk_sunday.TabIndex = 6
    Me.chk_sunday.Text = "xSunday"
    Me.chk_sunday.UseVisualStyleBackColor = True
    '
    'chk_saturday
    '
    Me.chk_saturday.AutoSize = True
    Me.chk_saturday.Location = New System.Drawing.Point(15, 206)
    Me.chk_saturday.Name = "chk_saturday"
    Me.chk_saturday.Size = New System.Drawing.Size(85, 17)
    Me.chk_saturday.TabIndex = 5
    Me.chk_saturday.Text = "xSaturday"
    Me.chk_saturday.UseVisualStyleBackColor = True
    '
    'chk_friday
    '
    Me.chk_friday.AutoSize = True
    Me.chk_friday.Location = New System.Drawing.Point(15, 183)
    Me.chk_friday.Name = "chk_friday"
    Me.chk_friday.Size = New System.Drawing.Size(68, 17)
    Me.chk_friday.TabIndex = 4
    Me.chk_friday.Text = "xFriday"
    Me.chk_friday.UseVisualStyleBackColor = True
    '
    'chk_thursday
    '
    Me.chk_thursday.AutoSize = True
    Me.chk_thursday.Location = New System.Drawing.Point(15, 160)
    Me.chk_thursday.Name = "chk_thursday"
    Me.chk_thursday.Size = New System.Drawing.Size(86, 17)
    Me.chk_thursday.TabIndex = 3
    Me.chk_thursday.Text = "xThursday"
    Me.chk_thursday.UseVisualStyleBackColor = True
    '
    'chk_wednesday
    '
    Me.chk_wednesday.AutoSize = True
    Me.chk_wednesday.Location = New System.Drawing.Point(15, 137)
    Me.chk_wednesday.Name = "chk_wednesday"
    Me.chk_wednesday.Size = New System.Drawing.Size(99, 17)
    Me.chk_wednesday.TabIndex = 2
    Me.chk_wednesday.Text = "xWednesday"
    Me.chk_wednesday.UseVisualStyleBackColor = True
    '
    'chk_tuesday
    '
    Me.chk_tuesday.AutoSize = True
    Me.chk_tuesday.Location = New System.Drawing.Point(15, 114)
    Me.chk_tuesday.Name = "chk_tuesday"
    Me.chk_tuesday.Size = New System.Drawing.Size(81, 17)
    Me.chk_tuesday.TabIndex = 1
    Me.chk_tuesday.Text = "xTuesday"
    Me.chk_tuesday.UseVisualStyleBackColor = True
    '
    'chk_monday
    '
    Me.chk_monday.AutoSize = True
    Me.chk_monday.Location = New System.Drawing.Point(15, 91)
    Me.chk_monday.Name = "chk_monday"
    Me.chk_monday.Size = New System.Drawing.Size(77, 17)
    Me.chk_monday.TabIndex = 0
    Me.chk_monday.Text = "xMonday"
    Me.chk_monday.UseVisualStyleBackColor = True
    '
    'dg_jackpot_list
    '
    Me.dg_jackpot_list.CurrentCol = -1
    Me.dg_jackpot_list.CurrentRow = -1
    Me.dg_jackpot_list.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_jackpot_list.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_jackpot_list.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_jackpot_list.Location = New System.Drawing.Point(153, 99)
    Me.dg_jackpot_list.Name = "dg_jackpot_list"
    Me.dg_jackpot_list.PanelRightVisible = False
    Me.dg_jackpot_list.Redraw = True
    Me.dg_jackpot_list.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_jackpot_list.Size = New System.Drawing.Size(786, 71)
    Me.dg_jackpot_list.Sortable = False
    Me.dg_jackpot_list.SortAscending = True
    Me.dg_jackpot_list.SortByCol = 0
    Me.dg_jackpot_list.TabIndex = 10
    Me.dg_jackpot_list.ToolTipped = True
    Me.dg_jackpot_list.TopRow = -2
    '
    'gb_parameters
    '
    Me.gb_parameters.Controls.Add(Me.lbl_min_jackpot_block)
    Me.gb_parameters.Controls.Add(Me.cmb_min_jack_block)
    Me.gb_parameters.Controls.Add(Me.lbl_recent_interval)
    Me.gb_parameters.Controls.Add(Me.cmb_recent_interval)
    Me.gb_parameters.Controls.Add(Me.lbl_animation_interval)
    Me.gb_parameters.Controls.Add(Me.cmb_anim_interval)
    Me.gb_parameters.Controls.Add(Me.lbl_block_interval)
    Me.gb_parameters.Controls.Add(Me.lbl_block_mode)
    Me.gb_parameters.Controls.Add(Me.cmb_block_interval)
    Me.gb_parameters.Controls.Add(Me.cmb_block_mode)
    Me.gb_parameters.Location = New System.Drawing.Point(153, 176)
    Me.gb_parameters.Name = "gb_parameters"
    Me.gb_parameters.Size = New System.Drawing.Size(786, 181)
    Me.gb_parameters.TabIndex = 11
    Me.gb_parameters.TabStop = False
    Me.gb_parameters.Text = "xJackpot Award Parameters"
    '
    'lbl_min_jackpot_block
    '
    Me.lbl_min_jackpot_block.AutoSize = True
    Me.lbl_min_jackpot_block.Location = New System.Drawing.Point(6, 72)
    Me.lbl_min_jackpot_block.Name = "lbl_min_jackpot_block"
    Me.lbl_min_jackpot_block.Size = New System.Drawing.Size(194, 13)
    Me.lbl_min_jackpot_block.TabIndex = 4
    Me.lbl_min_jackpot_block.Text = "xMinimum Jackpot Block amount"
    Me.lbl_min_jackpot_block.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'cmb_min_jack_block
    '
    Me.cmb_min_jack_block.AllowUnlistedValues = False
    Me.cmb_min_jack_block.AutoCompleteMode = False
    Me.cmb_min_jack_block.IsReadOnly = False
    Me.cmb_min_jack_block.Location = New System.Drawing.Point(376, 66)
    Me.cmb_min_jack_block.Name = "cmb_min_jack_block"
    Me.cmb_min_jack_block.SelectedIndex = -1
    Me.cmb_min_jack_block.Size = New System.Drawing.Size(167, 24)
    Me.cmb_min_jack_block.SufixText = "Sufix Text"
    Me.cmb_min_jack_block.SufixTextVisible = True
    Me.cmb_min_jack_block.TabIndex = 5
    Me.cmb_min_jack_block.TextVisible = False
    Me.cmb_min_jack_block.TextWidth = 0
    '
    'lbl_recent_interval
    '
    Me.lbl_recent_interval.AutoSize = True
    Me.lbl_recent_interval.Location = New System.Drawing.Point(6, 152)
    Me.lbl_recent_interval.Name = "lbl_recent_interval"
    Me.lbl_recent_interval.Size = New System.Drawing.Size(96, 13)
    Me.lbl_recent_interval.TabIndex = 8
    Me.lbl_recent_interval.Text = "xRecentinterval"
    Me.lbl_recent_interval.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'cmb_recent_interval
    '
    Me.cmb_recent_interval.AllowUnlistedValues = False
    Me.cmb_recent_interval.AutoCompleteMode = False
    Me.cmb_recent_interval.IsReadOnly = False
    Me.cmb_recent_interval.Location = New System.Drawing.Point(376, 146)
    Me.cmb_recent_interval.Name = "cmb_recent_interval"
    Me.cmb_recent_interval.SelectedIndex = -1
    Me.cmb_recent_interval.Size = New System.Drawing.Size(167, 24)
    Me.cmb_recent_interval.SufixText = "Sufix Text"
    Me.cmb_recent_interval.SufixTextVisible = True
    Me.cmb_recent_interval.TabIndex = 9
    Me.cmb_recent_interval.TextVisible = False
    Me.cmb_recent_interval.TextWidth = 0
    '
    'lbl_animation_interval
    '
    Me.lbl_animation_interval.AutoSize = True
    Me.lbl_animation_interval.Location = New System.Drawing.Point(6, 112)
    Me.lbl_animation_interval.Name = "lbl_animation_interval"
    Me.lbl_animation_interval.Size = New System.Drawing.Size(116, 13)
    Me.lbl_animation_interval.TabIndex = 6
    Me.lbl_animation_interval.Text = "xAnimationInterval"
    Me.lbl_animation_interval.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'cmb_anim_interval
    '
    Me.cmb_anim_interval.AllowUnlistedValues = False
    Me.cmb_anim_interval.AutoCompleteMode = False
    Me.cmb_anim_interval.IsReadOnly = False
    Me.cmb_anim_interval.Location = New System.Drawing.Point(376, 106)
    Me.cmb_anim_interval.Name = "cmb_anim_interval"
    Me.cmb_anim_interval.SelectedIndex = -1
    Me.cmb_anim_interval.Size = New System.Drawing.Size(167, 24)
    Me.cmb_anim_interval.SufixText = "Sufix Text"
    Me.cmb_anim_interval.SufixTextVisible = True
    Me.cmb_anim_interval.TabIndex = 7
    Me.cmb_anim_interval.TextVisible = False
    Me.cmb_anim_interval.TextWidth = 0
    '
    'lbl_block_interval
    '
    Me.lbl_block_interval.AutoSize = True
    Me.lbl_block_interval.Location = New System.Drawing.Point(543, 32)
    Me.lbl_block_interval.Name = "lbl_block_interval"
    Me.lbl_block_interval.Size = New System.Drawing.Size(42, 13)
    Me.lbl_block_interval.TabIndex = 2
    Me.lbl_block_interval.Text = "xInter"
    Me.lbl_block_interval.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_block_mode
    '
    Me.lbl_block_mode.AutoSize = True
    Me.lbl_block_mode.Location = New System.Drawing.Point(6, 32)
    Me.lbl_block_mode.Name = "lbl_block_mode"
    Me.lbl_block_mode.Size = New System.Drawing.Size(75, 13)
    Me.lbl_block_mode.TabIndex = 0
    Me.lbl_block_mode.Text = "xBlockMode"
    Me.lbl_block_mode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'cmb_block_interval
    '
    Me.cmb_block_interval.AllowUnlistedValues = False
    Me.cmb_block_interval.AutoCompleteMode = False
    Me.cmb_block_interval.IsReadOnly = False
    Me.cmb_block_interval.Location = New System.Drawing.Point(610, 26)
    Me.cmb_block_interval.Name = "cmb_block_interval"
    Me.cmb_block_interval.SelectedIndex = -1
    Me.cmb_block_interval.Size = New System.Drawing.Size(133, 24)
    Me.cmb_block_interval.SufixText = "Sufix Text"
    Me.cmb_block_interval.SufixTextVisible = True
    Me.cmb_block_interval.TabIndex = 3
    Me.cmb_block_interval.TextVisible = False
    Me.cmb_block_interval.TextWidth = 0
    '
    'cmb_block_mode
    '
    Me.cmb_block_mode.AllowUnlistedValues = False
    Me.cmb_block_mode.AutoCompleteMode = False
    Me.cmb_block_mode.IsReadOnly = False
    Me.cmb_block_mode.Location = New System.Drawing.Point(375, 26)
    Me.cmb_block_mode.Name = "cmb_block_mode"
    Me.cmb_block_mode.SelectedIndex = -1
    Me.cmb_block_mode.Size = New System.Drawing.Size(167, 24)
    Me.cmb_block_mode.SufixText = "Sufix Text"
    Me.cmb_block_mode.SufixTextVisible = True
    Me.cmb_block_mode.TabIndex = 1
    Me.cmb_block_mode.TextVisible = False
    Me.cmb_block_mode.TextWidth = 0
    '
    'frm_jackpot_configuration
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1048, 371)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_jackpot_configuration"
    Me.Text = "frm_jackpot_configuration"
    Me.panel_data.ResumeLayout(False)
    Me.gb_gen_values.ResumeLayout(False)
    Me.gb_jackpot_flag.ResumeLayout(False)
    Me.gb_wrk_days.ResumeLayout(False)
    Me.gb_wrk_days.PerformLayout()
    Me.gb_parameters.ResumeLayout(False)
    Me.gb_parameters.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_gen_values As System.Windows.Forms.GroupBox
  Friend WithEvents ef_contribution As GUI_Controls.uc_entry_field
  Friend WithEvents gb_jackpot_flag As System.Windows.Forms.GroupBox
  Friend WithEvents opt_enabled As System.Windows.Forms.RadioButton
  Friend WithEvents opt_disabled As System.Windows.Forms.RadioButton
  Friend WithEvents gb_wrk_days As System.Windows.Forms.GroupBox
  Friend WithEvents chk_monday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_saturday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_friday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_thursday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_wednesday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_tuesday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_sunday As System.Windows.Forms.CheckBox
  Friend WithEvents dg_jackpot_list As GUI_Controls.uc_grid
  Friend WithEvents dtp_working_from As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_working_to As GUI_Controls.uc_date_picker
  Friend WithEvents gb_parameters As System.Windows.Forms.GroupBox
  Friend WithEvents cmb_block_interval As GUI_Controls.uc_combo
  Friend WithEvents cmb_block_mode As GUI_Controls.uc_combo
  Friend WithEvents cmb_recent_interval As GUI_Controls.uc_combo
  Friend WithEvents cmb_anim_interval As GUI_Controls.uc_combo
  Friend WithEvents lbl_recent_interval As System.Windows.Forms.Label
  Friend WithEvents lbl_animation_interval As System.Windows.Forms.Label
  Friend WithEvents lbl_block_interval As System.Windows.Forms.Label
  Friend WithEvents lbl_block_mode As System.Windows.Forms.Label
  Friend WithEvents lbl_min_jackpot_block As System.Windows.Forms.Label
  Friend WithEvents cmb_min_jack_block As GUI_Controls.uc_combo
  Friend WithEvents btn_load_last_month_played As GUI_Controls.uc_button
  Friend WithEvents ef_last_month_played As GUI_Controls.uc_entry_field
End Class
