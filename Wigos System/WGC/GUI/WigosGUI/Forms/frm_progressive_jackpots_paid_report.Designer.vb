<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_progressive_jackpots_paid_report
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.uc_checked_list_progressive_catalog = New GUI_Controls.uc_checked_list()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.uc_pr_list = New GUI_Controls.uc_provider()
    Me.gp_progressive = New System.Windows.Forms.GroupBox()
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox()
    Me.chk_view_detail = New System.Windows.Forms.CheckBox()
    Me.chk_group_progressive = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gp_progressive.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gp_progressive)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Controls.Add(Me.uc_checked_list_progressive_catalog)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1097, 188)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_checked_list_progressive_catalog, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gp_progressive, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 192)
    Me.panel_data.Size = New System.Drawing.Size(1097, 378)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1091, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1091, 4)
    '
    'uc_checked_list_progressive_catalog
    '
    Me.uc_checked_list_progressive_catalog.GroupBoxText = "xCheckedList"
    Me.uc_checked_list_progressive_catalog.Location = New System.Drawing.Point(263, 6)
    Me.uc_checked_list_progressive_catalog.m_resize_width = 352
    Me.uc_checked_list_progressive_catalog.multiChoice = True
    Me.uc_checked_list_progressive_catalog.Name = "uc_checked_list_progressive_catalog"
    Me.uc_checked_list_progressive_catalog.SelectedIndexes = New Integer(-1) {}
    Me.uc_checked_list_progressive_catalog.SelectedIndexesList = ""
    Me.uc_checked_list_progressive_catalog.SelectedIndexesListLevel2 = ""
    Me.uc_checked_list_progressive_catalog.SelectedValuesArray = New String(-1) {}
    Me.uc_checked_list_progressive_catalog.SelectedValuesList = ""
    Me.uc_checked_list_progressive_catalog.SetLevels = 2
    Me.uc_checked_list_progressive_catalog.Size = New System.Drawing.Size(352, 174)
    Me.uc_checked_list_progressive_catalog.TabIndex = 2
    Me.uc_checked_list_progressive_catalog.ValuesArray = New String(-1) {}
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(6, 6)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(251, 81)
    Me.gb_date.TabIndex = 0
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xEndingDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 50)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(240, 25)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2010, 5, 10, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 20)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(240, 25)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2010, 5, 10, 0, 0, 0, 0)
    '
    'uc_pr_list
    '
    Me.uc_pr_list.Location = New System.Drawing.Point(618, 3)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(367, 179)
    Me.uc_pr_list.TabIndex = 3
    Me.uc_pr_list.TerminalListHasValues = False
    '
    'gp_progressive
    '
    Me.gp_progressive.Controls.Add(Me.chk_terminal_location)
    Me.gp_progressive.Controls.Add(Me.chk_view_detail)
    Me.gp_progressive.Controls.Add(Me.chk_group_progressive)
    Me.gp_progressive.Location = New System.Drawing.Point(7, 90)
    Me.gp_progressive.Name = "gp_progressive"
    Me.gp_progressive.Size = New System.Drawing.Size(250, 90)
    Me.gp_progressive.TabIndex = 1
    Me.gp_progressive.TabStop = False
    '
    'chk_terminal_location
    '
    Me.chk_terminal_location.Location = New System.Drawing.Point(14, 60)
    Me.chk_terminal_location.Name = "chk_terminal_location"
    Me.chk_terminal_location.Size = New System.Drawing.Size(212, 21)
    Me.chk_terminal_location.TabIndex = 2
    Me.chk_terminal_location.Text = "xShow terminals location"
    '
    'chk_view_detail
    '
    Me.chk_view_detail.AutoSize = True
    Me.chk_view_detail.Location = New System.Drawing.Point(25, 37)
    Me.chk_view_detail.Name = "chk_view_detail"
    Me.chk_view_detail.Size = New System.Drawing.Size(99, 17)
    Me.chk_view_detail.TabIndex = 1
    Me.chk_view_detail.Text = "xViewDetails"
    Me.chk_view_detail.UseVisualStyleBackColor = True
    '
    'chk_group_progressive
    '
    Me.chk_group_progressive.AutoSize = True
    Me.chk_group_progressive.Location = New System.Drawing.Point(14, 14)
    Me.chk_group_progressive.Name = "chk_group_progressive"
    Me.chk_group_progressive.Size = New System.Drawing.Size(150, 17)
    Me.chk_group_progressive.TabIndex = 0
    Me.chk_group_progressive.Text = "xGroupByProgressive"
    Me.chk_group_progressive.UseVisualStyleBackColor = True
    '
    'frm_progressive_jackpots_paid_report
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1107, 574)
    Me.Name = "frm_progressive_jackpots_paid_report"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_progressive_jackpots_paid_report"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gp_progressive.ResumeLayout(False)
    Me.gp_progressive.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_checked_list_progressive_catalog As GUI_Controls.uc_checked_list
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents gp_progressive As System.Windows.Forms.GroupBox
  Friend WithEvents chk_view_detail As System.Windows.Forms.CheckBox
  Friend WithEvents chk_group_progressive As System.Windows.Forms.CheckBox
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox
End Class
