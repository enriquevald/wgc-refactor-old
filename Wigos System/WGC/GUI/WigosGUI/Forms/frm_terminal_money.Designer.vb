<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_terminal_money
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.uc_pr_list = New GUI_Controls.uc_provider
    Me.opt_all_notes = New System.Windows.Forms.RadioButton
    Me.opt_by_note = New System.Windows.Forms.RadioButton
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector
    Me.gb_Notes = New System.Windows.Forms.GroupBox
    Me.opt_no_collected = New System.Windows.Forms.RadioButton
    Me.gb_group = New System.Windows.Forms.GroupBox
    Me.opt_grouped_terminal = New System.Windows.Forms.RadioButton
    Me.opt_grouped_terminal_notes = New System.Windows.Forms.RadioButton
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_Notes.SuspendLayout()
    Me.gb_group.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.chk_terminal_location)
    Me.panel_filter.Controls.Add(Me.gb_group)
    Me.panel_filter.Controls.Add(Me.uc_dsl)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Controls.Add(Me.gb_Notes)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1124, 190)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_Notes, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dsl, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_group, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_terminal_location, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 194)
    Me.panel_data.Size = New System.Drawing.Size(1124, 449)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1118, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1118, 4)
    '
    'uc_pr_list
    '
    Me.uc_pr_list.Location = New System.Drawing.Point(295, 4)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(390, 191)
    Me.uc_pr_list.TabIndex = 2
    '
    'opt_all_notes
    '
    Me.opt_all_notes.Enabled = False
    Me.opt_all_notes.Location = New System.Drawing.Point(6, 49)
    Me.opt_all_notes.Name = "opt_all_notes"
    Me.opt_all_notes.Size = New System.Drawing.Size(256, 24)
    Me.opt_all_notes.TabIndex = 1
    Me.opt_all_notes.TabStop = True
    Me.opt_all_notes.Text = "xAllNotes"
    '
    'opt_by_note
    '
    Me.opt_by_note.Enabled = False
    Me.opt_by_note.Location = New System.Drawing.Point(6, 79)
    Me.opt_by_note.Name = "opt_by_note"
    Me.opt_by_note.Size = New System.Drawing.Size(256, 24)
    Me.opt_by_note.TabIndex = 2
    Me.opt_by_note.Text = "xDetailNotes"
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.ClosingTimeEnabled = True
    Me.uc_dsl.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.Location = New System.Drawing.Point(18, 7)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.ShowBorder = True
    Me.uc_dsl.Size = New System.Drawing.Size(271, 82)
    Me.uc_dsl.TabIndex = 0
    Me.uc_dsl.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    '
    'gb_Notes
    '
    Me.gb_Notes.Controls.Add(Me.opt_no_collected)
    Me.gb_Notes.Controls.Add(Me.opt_all_notes)
    Me.gb_Notes.Controls.Add(Me.opt_by_note)
    Me.gb_Notes.Enabled = False
    Me.gb_Notes.Location = New System.Drawing.Point(709, 57)
    Me.gb_Notes.Name = "gb_Notes"
    Me.gb_Notes.Size = New System.Drawing.Size(271, 113)
    Me.gb_Notes.TabIndex = 4
    Me.gb_Notes.TabStop = False
    Me.gb_Notes.Text = "xNotes"
    '
    'opt_no_collected
    '
    Me.opt_no_collected.Checked = True
    Me.opt_no_collected.Enabled = False
    Me.opt_no_collected.Location = New System.Drawing.Point(6, 21)
    Me.opt_no_collected.Name = "opt_no_collected"
    Me.opt_no_collected.Size = New System.Drawing.Size(256, 24)
    Me.opt_no_collected.TabIndex = 0
    Me.opt_no_collected.TabStop = True
    Me.opt_no_collected.Text = "xNoCollected"
    '
    'gb_group
    '
    Me.gb_group.Controls.Add(Me.opt_grouped_terminal)
    Me.gb_group.Controls.Add(Me.opt_grouped_terminal_notes)
    Me.gb_group.Location = New System.Drawing.Point(18, 105)
    Me.gb_group.Name = "gb_group"
    Me.gb_group.Size = New System.Drawing.Size(271, 83)
    Me.gb_group.TabIndex = 1
    Me.gb_group.TabStop = False
    Me.gb_group.Text = "xNotes"
    '
    'opt_grouped_terminal
    '
    Me.opt_grouped_terminal.Checked = True
    Me.opt_grouped_terminal.Location = New System.Drawing.Point(12, 21)
    Me.opt_grouped_terminal.Name = "opt_grouped_terminal"
    Me.opt_grouped_terminal.Size = New System.Drawing.Size(243, 24)
    Me.opt_grouped_terminal.TabIndex = 0
    Me.opt_grouped_terminal.TabStop = True
    Me.opt_grouped_terminal.Text = "xTerminal"
    '
    'opt_grouped_terminal_notes
    '
    Me.opt_grouped_terminal_notes.Location = New System.Drawing.Point(12, 49)
    Me.opt_grouped_terminal_notes.Name = "opt_grouped_terminal_notes"
    Me.opt_grouped_terminal_notes.Size = New System.Drawing.Size(243, 24)
    Me.opt_grouped_terminal_notes.TabIndex = 1
    Me.opt_grouped_terminal_notes.TabStop = True
    Me.opt_grouped_terminal_notes.Text = "xTerminalNotes"
    '
    'chk_terminal_location
    '
    Me.chk_terminal_location.Location = New System.Drawing.Point(673, 24)
    Me.chk_terminal_location.Name = "chk_terminal_location"
    Me.chk_terminal_location.Size = New System.Drawing.Size(247, 17)
    Me.chk_terminal_location.TabIndex = 3
    Me.chk_terminal_location.Text = "xShow terminals location"
    '
    'frm_terminal_money
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1134, 647)
    Me.Name = "frm_terminal_money"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_terminal_money"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_Notes.ResumeLayout(False)
    Me.gb_group.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents opt_all_notes As System.Windows.Forms.RadioButton
  Friend WithEvents opt_by_note As System.Windows.Forms.RadioButton
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector
  Friend WithEvents gb_Notes As System.Windows.Forms.GroupBox
  Friend WithEvents opt_no_collected As System.Windows.Forms.RadioButton
  Friend WithEvents gb_group As System.Windows.Forms.GroupBox
  Friend WithEvents opt_grouped_terminal As System.Windows.Forms.RadioButton
  Friend WithEvents opt_grouped_terminal_notes As System.Windows.Forms.RadioButton
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox
End Class
