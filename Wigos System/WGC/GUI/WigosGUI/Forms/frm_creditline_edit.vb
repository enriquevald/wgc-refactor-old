﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_creditline_edit.vb
' DESCRIPTION:   Create and edit credit lines
' AUTHOR:        David Perelló
' CREATION DATE: 13-MAR-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 13-MAR-2017  DPC     Initial version
' 24-JUL-2017  DPC     WIGOS-3933: Not possible to have 2 signers in Credit lines
' 25-JUL-2017  DPC     WIGOS-3930: Wrong chars allowed in IBAN field - Credit line edition screen
' -------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports WSI.Common.Promotion
Imports WSI.Common.Buckets
Imports WSI.Common
Imports GUI_CommonMisc
Imports System.Xml
Imports System.Text

Public Class frm_creditline_edit

  Inherits frm_base_edit

#Region " Members "

  Private m_dt_signers As DataTable
  Private m_credit_line_read As CLASS_CREDITLINE
  Private m_credit_line_edit As CLASS_CREDITLINE
  Private m_signers_deleted As Boolean
  Private m_is_center As Boolean

#End Region ' Members

#Region " Constants "

  Private Const GRID_COLUMNS As Integer = 2
  Private Const GRID_HEADER_ROWS As Integer = 1

  ' DATAGRID Width
  Private Const GRID_WIDTH_NAME As Integer = 3625
  Private Const GRID_WIDTH_DATE As Integer = 1500

#End Region ' Constants

#Region " Overrides "

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_SetFormId()
    m_is_center = GeneralParam.GetBoolean("MultiSite", "IsCenter", False)

    If (m_is_center) Then
      Me.FormId = ENUM_FORM.FORM_CREDITLINE_SEL
    Else
      Me.FormId = ENUM_FORM.FORM_CREDITLINE_EDIT
    End If

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8088)

    Me.ef_credit_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7948) '  Credit limit
    Call Me.ef_credit_limit.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 10)

    Me.ef_tto.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7949) '  TTO
    Call Me.ef_tto.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 10)

    Me.ef_spent.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2555) '  Spent
    Call Me.ef_spent.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 10)
    Me.ef_spent.IsReadOnly = True

    Me.ef_iban.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7952) '  IBAN
    Call Me.ef_iban.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_ALPHA_NUMERIC, 34)

    Me.cb_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1090) '  Status

    Me.ef_creation_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7985) '  Date Created
    Me.ef_creation_date.Value = GUI_FormatDate(GUI_GetDate(), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)
    Me.ef_creation_date.IsReadOnly = True

    Me.dtp_expiration.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7950) '  Expiration date
    Me.dtp_expiration.Value = GUI_GetDate().AddDays(1).AddHours(GetDefaultClosingTime())
    Me.dtp_expiration.IsReadOnly = False
    Me.dtp_expiration.Checked = False

    lbl_signers.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7951) '  Remaining approvers

    bt_open_account_summary.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1237) '  Accounts

    Call Me.ef_id_account.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER)
    Me.ef_id_account.IsReadOnly = True
    Call Me.ef_holder_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_ALPHA_NUMERIC)
    Me.ef_holder_name.IsReadOnly = True

    gb_account.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1564) ' Account
    gb_settings.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(474) ' Details
    gb_signers.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7953) ' Signatories

    bt_sign.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7954) ' Sign

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_DELETE).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_PRINT).Visible = False

    m_dt_signers = New DataTable

    m_dt_signers.Columns.Add("IdSigner", GetType(String))
    m_dt_signers.Columns.Add("NameSigner", GetType(String))
    m_dt_signers.Columns.Add("Date", GetType(String))

    If m_credit_line_edit Is Nothing Then
      m_credit_line_edit = DbEditedObject
    End If

    If m_credit_line_read Is Nothing Then
      m_credit_line_read = DbReadObject
    End If

    GUI_StyleSheetProviders()

  End Sub ' GUI_InitControls

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _dic_status As Dictionary(Of Integer, String)

    If m_credit_line_read.AccountId <> 0 Then
      ef_id_account.Value = m_credit_line_read.AccountId
      bt_open_account_summary.Enabled = False
    End If

    If m_credit_line_read.HolderName <> String.Empty Then
      ef_holder_name.Value = m_credit_line_read.HolderName
    End If

    cb_status.Clear()

    If ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT Then
      ef_credit_limit.Value = m_credit_line_read.LimitAmount
      ef_tto.Value = m_credit_line_read.TTOAmount
      ef_spent.Value = m_credit_line_read.SpentAmount
      ef_iban.Value = m_credit_line_read.Iban
      ef_creation_date.Value = GUI_FormatDate(m_credit_line_read.Creation, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)
      dtp_expiration.Value = IIf(IsDBNull(m_credit_line_read.Expired), WGDB.Now, m_credit_line_read.Expired)
      dtp_expiration.Checked = m_credit_line_read.HasExpired
      _dic_status = m_credit_line_read.getListStatus(m_credit_line_read.Status)
      cb_status.Add(_dic_status)
      cb_status.Value = m_credit_line_read.Status
    Else
      ef_spent.Value = 0
      _dic_status = m_credit_line_read.getListStatus(CreditLines.StatusDefinition.None)
      cb_status.Add(_dic_status)
      cb_status.Enabled = False
    End If

    AddSignerFromXML(m_credit_line_read.Signers)
    LoadGrid()

    EnableControls(Not m_is_center)

  End Sub ' GUI_SetScreenData

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_GetScreenData()

    m_credit_line_edit.AccountId = Int64.Parse(IIf(ef_id_account.Value = String.Empty, 0, ef_id_account.Value))
    m_credit_line_edit.HolderName = IIf(ef_holder_name.Value = String.Empty, 0, ef_holder_name.Value)
    m_credit_line_edit.LimitAmount = Decimal.Parse(IIf(ef_credit_limit.Value = String.Empty, 0, ef_credit_limit.Value))
    m_credit_line_edit.TTOAmount = Decimal.Parse(IIf(ef_tto.Value = String.Empty, 0, ef_tto.Value))
    m_credit_line_edit.SpentAmount = ef_spent.Value
    m_credit_line_edit.Status = CalculateNextStatus(cb_status.Value)

    m_credit_line_edit.Iban = ef_iban.Value

    If ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
      m_credit_line_edit.Creation = WGDB.Now
    End If

    m_credit_line_edit.Update = Nothing
    m_credit_line_edit.HasExpired = dtp_expiration.Checked
    m_credit_line_edit.Expired = IIf(dtp_expiration.Checked, dtp_expiration.Value, GUI_GetDate().AddHours(GetDefaultClosingTime()))

    m_credit_line_edit.Expired = New DateTime(m_credit_line_edit.Expired.Year, m_credit_line_edit.Expired.Month, m_credit_line_edit.Expired.Day, GetDefaultClosingTime(), 0, 0)

    m_credit_line_edit.LastPayback = Nothing
    m_credit_line_edit.Signers = GetXmlSigners()

    m_credit_line_edit.CreditLineRead = m_credit_line_read

  End Sub ' GUI_GetScreenData

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <param name="DbOperation"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Dim ctx As Integer

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        Me.DbEditedObject = New CLASS_CREDITLINE
        Me.DbStatus = ENUM_STATUS.STATUS_OK

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ

        If Me.DbStatus = ENUM_STATUS.STATUS_NOT_FOUND Then
          Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW
          Me.DbStatus = ENUM_STATUS.STATUS_OK
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_INSERT
        DbStatus = DbEditedObject.DB_Insert(ctx)

      Case ENUM_DB_OPERATION.DB_OPERATION_UPDATE
        DbStatus = DbEditedObject.DB_Update(ctx)

      Case ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE
        DbReadObject = DbEditedObject.Duplicate()

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)
    End Select

  End Sub ' GUI_DB_Operation

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <param name="ButtonId"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Call MyBase.GUI_ButtonClick(ButtonId)

  End Sub 'GUI_ButtonClick

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Dim _msg_signers_deleted As String
    Dim _msg_automatic_change_status As String
    Dim _limit As Decimal

    m_signers_deleted = False
    _msg_automatic_change_status = False

    If String.IsNullOrEmpty(ef_id_account.Value.Trim()) Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1202))
      bt_open_account_summary.Focus()

      Return False
    End If

    If dtp_expiration.Checked AndAlso Not IsExpirationDateCorrect() Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7979), ENUM_MB_TYPE.MB_TYPE_WARNING, , , WGDB.Now.ToShortDateString)
      bt_open_account_summary.Focus()

      Return False
    End If

    Decimal.TryParse(ef_credit_limit.Value.Trim(), _limit)

    If String.IsNullOrEmpty(ef_credit_limit.Value.Trim()) Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_credit_limit.Text)
      ef_credit_limit.Focus()

      Return False
    ElseIf _limit = 0 Then
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7976), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_NO Then
        Return False
      End If
    End If

    m_credit_line_edit.Status = CalculateNextStatus(cb_status.Value)

    If m_credit_line_read.Status <> CreditLines.StatusDefinition.PendingApproval AndAlso m_credit_line_edit.Status = CreditLines.StatusDefinition.PendingApproval OrElse ThereHaveBeenChanges() Then
      If m_dt_signers.Rows.Count > 0 Then
        m_signers_deleted = True
      End If
    End If

    _msg_automatic_change_status = IIf(m_credit_line_edit.Status <> cb_status.Value, String.Format(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7974), CreditLines.CreditLine.StatusToString(m_credit_line_edit.Status)), String.Empty)
    _msg_signers_deleted = IIf(m_signers_deleted, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7973), String.Empty)

    If _msg_automatic_change_status <> String.Empty OrElse _msg_signers_deleted <> String.Empty Then
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7972), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, , _msg_automatic_change_status, _msg_signers_deleted) = ENUM_MB_RESULT.MB_RESULT_NO Then
        Return False
      Else
        If m_signers_deleted Then
          m_dt_signers.Rows.Clear()
        End If
      End If
    End If

    If Not String.IsNullOrEmpty(ef_iban.Value) AndAlso Not CheckIBAN(ef_iban.Value) Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_iban.Text)
      ef_credit_limit.Focus()

      Return False
    End If

    Return True

  End Function ' GUI_IsScreenDataOk

#End Region ' Overrides

#Region " Events "

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub bt_open_account_summary_Click(sender As Object, e As EventArgs) Handles bt_open_account_summary.Click

    Dim _frm As frm_account_summary
    Dim _account As cls_account

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_account_summary()

    _frm.is_selection_mode = True
    _frm.ShowDialog(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

    _account = New cls_account
    _account.holder_name = _frm.account.holder_name
    _account.id_account = _frm.account.id_account

    If Not _account.id_account Is Nothing Then
      If m_credit_line_read.GetCreditLine(_account) <> ENUM_STATUS.STATUS_NOT_FOUND Then
        If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7975), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_YES Then
          Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
          Call GUI_SetScreenData(0)
        End If
      Else
        ef_id_account.Value = _frm.account.id_account
        ef_holder_name.Value = _frm.account.holder_name
      End If
    End If

  End Sub ' bt_open_account_summary_Click

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub bt_sign_Click(sender As Object, e As EventArgs) Handles bt_sign.Click
    AddSginer(GLB_CurrentUser.Name, GLB_CurrentUser.FullName, Now.ToShortDateString)
    LoadGrid()
  End Sub ' bt_sign_Click

#End Region ' Events

#Region " Private Methods "

  ''' <summary>
  ''' Returns the XML of signers
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetXmlSigners() As String

    Dim _root_node As XmlElement
    Dim _xml_doc As XmlDocument

    Try

      _xml_doc = New XmlDocument

      _root_node = _xml_doc.CreateElement("signers")
      _xml_doc.AppendChild(_root_node)

      For Each _row_signer As DataRow In m_dt_signers.Rows
        SetChildNode(_xml_doc, _row_signer)
      Next

      Return _xml_doc.OuterXml

    Catch _ex As Exception
      Log.Error(_ex.Message())

      Return String.Empty
    End Try

  End Function ' GetXmlSigners

  ''' <summary>
  ''' Add child signer to XML signers
  ''' </summary>
  ''' <param name="xml_signers"></param>
  ''' <param name="row_signer"></param>
  ''' <remarks></remarks>
  Private Sub SetChildNode(ByRef XmlSigners As XmlDocument, ByRef RowSigner As DataRow)

    Dim _root_node As XmlElement
    Dim _parent_node As XmlElement
    Dim _name_node As XmlElement
    Dim _date_node As XmlElement
    Dim _id_node As XmlElement
    Dim _name_text As XmlText
    Dim _date_text As XmlText
    Dim _id_text As XmlText

    Try

      _root_node = XmlSigners.SelectSingleNode("//signers")
      _parent_node = XmlSigners.CreateElement("signer")

      XmlSigners.DocumentElement.AppendChild(_parent_node)

      _name_node = XmlSigners.CreateElement("Name")
      _date_node = XmlSigners.CreateElement("Date")
      _id_node = XmlSigners.CreateElement("ID")
      _name_text = XmlSigners.CreateTextNode(RowSigner("NameSigner"))
      _date_text = XmlSigners.CreateTextNode(RowSigner("Date"))
      _id_text = XmlSigners.CreateTextNode(RowSigner("IdSigner"))

      _parent_node.AppendChild(_name_node)
      _parent_node.AppendChild(_date_node)
      _parent_node.AppendChild(_id_node)

      _name_node.AppendChild(_name_text)
      _date_node.AppendChild(_date_text)
      _id_node.AppendChild(_id_text)

    Catch _ex As Exception
      Log.Error(_ex.Message())
    End Try

  End Sub ' SetChildNode

  ''' <summary>
  ''' Add signer from XML to datatable signers
  ''' </summary>
  ''' <param name="xml_signers"></param>
  ''' <remarks></remarks>
  Private Sub AddSignerFromXML(ByVal XmlSigners As String)

    Dim _xml_doc As XmlDocument

    If XmlSigners <> String.Empty Then

      _xml_doc = New XmlDocument

      _xml_doc.LoadXml(XmlSigners)

      For Each node_signer As XmlNode In _xml_doc.SelectNodes("//signers/signer")
        AddSginer(node_signer.ChildNodes(2).InnerText, _
                  node_signer.ChildNodes(0).InnerText, _
                  node_signer.ChildNodes(1).InnerText)
      Next

    End If

  End Sub ' AddSignerFromXML

  ''' <summary>
  ''' Add signer from XML to datatable signers
  ''' </summary>
  ''' <param name="id_signer"></param>
  ''' <param name="name_signer"></param>
  ''' <param name="date_sign"></param>
  ''' <remarks></remarks>
  Private Sub AddSginer(ByVal IdSigner As String, ByVal NameSigner As String, ByVal DateSign As String)

    Dim _row_signer As DataRow

    _row_signer = m_dt_signers.NewRow

    _row_signer("IdSigner") = IdSigner
    _row_signer("NameSigner") = NameSigner
    _row_signer("Date") = DateSign
    m_dt_signers.Rows.Add(_row_signer)

    bt_sign.Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_CREDITLINE_SIGNER).Read AndAlso _
                 CurrentUser.Permissions(ENUM_FORM.FORM_CREDITLINE_SIGNER).Write AndAlso _
                 Not (m_dt_signers.Select(String.Format("IdSigner = '{0}'", GLB_CurrentUser.Name)).Length > 0) AndAlso _
                 cb_status.Value = CreditLines.StatusDefinition.PendingApproval


  End Sub ' AddSginer

  ''' <summary>
  ''' Load grid signers
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub LoadGrid()

    Dim _idx_row As Integer

    Try

      dg_signers.Redraw = False
      dg_signers.Clear()
      dg_signers.Redraw = True

      dg_signers.Redraw = False

      For Each _provider As DataRow In m_dt_signers.Rows
        dg_signers.AddRow()
        _idx_row = dg_signers.NumRows - 1

        dg_signers.Cell(_idx_row, 0).Value = _provider("NameSigner")
        dg_signers.Cell(_idx_row, 1).Value = _provider("Date")

      Next

      lbl_count_signers.Text = String.Format("{0} / {1}", m_dt_signers.Rows.Count, CreditLines.CreditLine.GetNumSigners())

    Finally
      dg_signers.Redraw = True
    End Try

  End Sub ' LoadGrid

  ''' <summary>
  ''' Set style to grid
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GUI_StyleSheetProviders()

    With Me.dg_signers

      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, True)

      ' Provider Name
      .Column(0).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(253)
      .Column(0).Width = GRID_WIDTH_NAME
      .Column(0).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      .Column(1).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(268)
      .Column(1).Width = GRID_WIDTH_DATE
      .Column(1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    End With

  End Sub ' GUI_StyleSheetProviders

  ''' <summary>
  ''' Returns the next status from workflow
  ''' </summary>
  ''' <param name="Status"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CalculateNextStatus(ByVal Status As CreditLines.StatusDefinition) As CreditLines.StatusDefinition

    Dim _next_status As CreditLines.StatusDefinition

    _next_status = Status

    If (m_is_center) Then
      Return _next_status
    End If

    If ThereHaveBeenChanges() Then
      _next_status = CreditLines.StatusDefinition.PendingApproval

      If m_dt_signers.Rows.Count > 0 Then
        m_signers_deleted = True
        m_dt_signers.Rows.Clear()
      End If
    Else
      If Status = CreditLines.StatusDefinition.PendingApproval Then
        If m_credit_line_read.Status = CreditLines.StatusDefinition.PendingApproval AndAlso m_dt_signers.Rows.Count = CreditLines.CreditLine.GetNumSigners Then
          _next_status = CreditLines.StatusDefinition.Approved
        End If
      End If
    End If

    Return _next_status

  End Function ' CalculateNextStatus

  ''' <summary>
  ''' Returns if some field have been changed
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ThereHaveBeenChanges() As Boolean

    Dim _tto As Decimal
    Dim _limit As Decimal

    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT Then

      Decimal.TryParse(ef_credit_limit.Value, _limit)

      If m_credit_line_read.LimitAmount <> _limit Then
        Return True
      End If

      Decimal.TryParse(ef_tto.Value, _tto)

      If m_credit_line_read.TTOAmount <> _tto Then
        Return True
      End If

      If m_credit_line_read.Iban <> ef_iban.Value Then
        Return True
      End If

      If m_credit_line_read.Expired <> dtp_expiration.Value Then
        Return True
      End If

      If m_credit_line_read.HasExpired <> dtp_expiration.Checked Then
        Return True
      End If

    End If

    Return False

  End Function ' ThereHaveBeenChanges

  ''' <summary>
  ''' Check expiration date
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function IsExpirationDateCorrect() As Boolean

    Dim _date_today As Date
    Dim _date_expiration As Date

    Try

      _date_today = Date.ParseExact(WGDB.Now.ToShortDateString, "dd/MM/yyyy", Nothing)
      _date_expiration = Date.ParseExact(dtp_expiration.Value.ToShortDateString, "dd/MM/yyyy", Nothing)

      Return _date_expiration > _date_today OrElse m_credit_line_edit.Status = CreditLines.StatusDefinition.Expired

    Catch _ex As Exception
      Log.Error(_ex.Message())
    End Try

    Return False

  End Function ' IsExpirationDateCorrect

  ''' <summary>
  ''' Enable/Disable controls
  ''' </summary>
  ''' <param name="Enable"></param>
  ''' <remarks></remarks>
  Private Sub EnableControls(ByVal Enable As Boolean)

    bt_open_account_summary.Enabled = (Enable AndAlso (Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW))
    bt_sign.Enabled = Enable AndAlso
                  (CurrentUser.Permissions(ENUM_FORM.FORM_CREDITLINE_SIGNER).Read AndAlso _
                  CurrentUser.Permissions(ENUM_FORM.FORM_CREDITLINE_SIGNER).Write AndAlso _
                  Not (m_dt_signers.Select(String.Format("IdSigner = '{0}'", GLB_CurrentUser.Name)).Length > 0) AndAlso _
                  cb_status.Value = CreditLines.StatusDefinition.PendingApproval)

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_OK).Enabled = Enable

    ef_credit_limit.IsReadOnly = Not Enable
    ef_tto.IsReadOnly = Not Enable
    ef_iban.IsReadOnly = Not Enable

    cb_status.IsReadOnly = Not Enable
    dtp_expiration.IsReadOnly = Not Enable

  End Sub ' EnableControls

  ''' <summary>
  ''' Validate IBAN
  ''' </summary>
  ''' <param name="BankAccount"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CheckIBAN(ByVal BankAccount As String) As Boolean

    Dim _v As Integer
    Dim _bank As String
    Dim _checksum As Integer
    Dim _ascii_shift As Integer
    Dim _sb As New StringBuilder
    Dim _check_sum_string As String

    Try

      BankAccount = BankAccount.ToUpper()

      If String.IsNullOrEmpty(BankAccount) Then
        Return False
      ElseIf RegularExpressions.Regex.IsMatch(BankAccount, "^[A-Z0-9]") Then

        BankAccount = BankAccount.Replace(" ", String.Empty)

        _bank = BankAccount.Substring(4, BankAccount.Length - 4) + BankAccount.Substring(0, 4)
        _ascii_shift = 55

        For Each c As Char In _bank

          If Char.IsLetter(c) Then
            _v = Convert.ToInt32(c) - _ascii_shift
          Else
            _v = Integer.Parse(c.ToString())
          End If
          _sb.Append(_v)
        Next

        _check_sum_string = _sb.ToString()
        _checksum = Integer.Parse(_check_sum_string.Substring(0, 1))

        For i As Integer = 1 To _check_sum_string.Length - 1
          Dim v As Integer = Integer.Parse(_check_sum_string.Substring(i, 1))
          _checksum *= 10
          _checksum += v
          _checksum = _checksum Mod 97
        Next

        Return _checksum = 1
      Else

        Return False
      End If

    Catch _ex As Exception
      Log.Error(_ex.Message())
    End Try

    Return False

  End Function ' CheckIBAN

#End Region ' Private Methods

End Class