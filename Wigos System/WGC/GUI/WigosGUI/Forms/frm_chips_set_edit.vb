'-------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_chips_set_edit.vb
'
' DESCRIPTION : Window for edit an group of chips
'               In this window can add and edit chips to the selected group,
'               edit name group and enabled or disabled the chips gorup.
'
' AUTHOR:        David Lasdiez
'
' CREATION DATE: 01-JUL-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 01-JUL-2014  DLL    Initial version   
' 10-JUL-2014  DCS    Add Radio Button and edit filters
'                     Add validations when button accept is pressed
'                     Allow delete items
'                     Change Formats and controls in the data grid view
' 09-FEB-2016  RGR    Task 5778: Group chips: Design changes in Group chips
' 16-FEB-2016  EOR    Task 4951: Change Name Window to Chips Edit
'                     Filter Change Name to Group
'                     Delete Number of Field Color
' 22-FEB-2016  RGR    Product Backlog Item 9339:Sprint Review 18 Winions - Mex
' 01-APR-2016  EOR    Product Backlog Item 10936:Mesas (Fase 1): Ajustes en diferentes pantallas de fichas
' 09-JUN-2016  RAB    PBI 11755: Tables (Phase 1): Correction errors as a result of the backward compatibility of the chips group
' 29-JUN-2016  FOS    Product Backlog Item 13403: Tables: GUI modifications (Phase 1)
' 30-AGO-2016  FOS    Fixed Bug 16575: When introduce a new denomination  don't show the system currency.
'--------------------------------------------------------------------

Option Strict Off
Option Explicit On

#Region "Imports"
Imports GUI_Controls
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls.uc_grid
Imports GUI_Controls.uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE
Imports WSI.Common
Imports System.Text
#End Region ' Imports

Public Class frm_chips_set_edit
  Inherits frm_base_edit

#Region " Constants "

  ' Fields length
  Public Const MAX_NAME_LEN As Integer = 50
  Public Const MAX_ISO_CODE_LEN As Integer = 3

  ' Grid Columns
  Private Const GRID_COLUMNS_CUR As Integer = 12
  Private Const GRID_HEADER_ROWS_CUR As Integer = 1

  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_ID As Integer = 1
  Private Const GRID_COLUMN_DENOMINATION As Integer = 2
  Private Const GRID_COLUMN_DENOMINATION_VALUE As Integer = 3
  Private Const GRID_COLUMN_ID_COLOR As Integer = 4
  Private Const GRID_COLUMN_COL_COLOR As Integer = 5
  Private Const GRID_COLUMN_NAME As Integer = 6
  Private Const GRID_COLUMN_DRAWING As Integer = 7
  Private Const GRID_COLUMN_ISO_CODE As Integer = 8
  Private Const GRID_COLUMN_ALLOWED As Integer = 9
  Private Const GRID_COLUMN_CHIP_TYPE As Integer = 10
  Private Const GRID_COLUMN_ID_AUX As Integer = 11

  ' Width Columns
  Private Const GRID_WIDTH_INDEX As Integer = 200
  Private Const GRID_WIDTH_ID As Integer = 0
  Private Const GRID_WIDTH_DENOMINATION As Integer = 1700
  Private Const GRID_WIDTH_DENOMINATION_VALUE As Integer = 0
  Private Const GRID_WIDTH_COLOR As Integer = 700
  Private Const GRID_WIDTH_NAME As Integer = 2500
  Private Const GRID_WIDTH_DRAWING As Integer = 2500
  Private Const GRID_WIDTH_ISO_CODE As Integer = 0
  Private Const GRID_WIDTH_ALLOWED As Integer = 1000
  Private Const GRID_WIDTH_CHIP_TYPE As Integer = 0
  Private Const GRID_WIDTH_ID_AUX As Integer = 0

#End Region ' Constants

#Region " Members "
  Private m_chips_sets As CLS_CHIPS_SET
  Private m_list_chip_types As IDictionary(Of FeatureChips.ChipType, String)
  Private m_iso_code As String
  Private m_id_max As Long
  Private m_num_rows_add_sub As Long
  Private m_multiple_currency As Boolean
  Private m_max_num_rows As Integer
#End Region ' Members

#Region " Constructors"
  Public Sub New()

    InitializeComponent()
    Me.m_list_chip_types = Me.GetChipTypes()

    'EOR 30-03-2016 Is Multiple Currency
    Me.m_multiple_currency = WSI.Common.Misc.IsMultiCurrencyExchangeEnabled()
    Me.m_max_num_rows = WSI.Common.GeneralParam.GetInt32("FeatureChips", "MaxChipsForSet", 20)

  End Sub
#End Region ' Constructors

#Region " Overrides"

  ' PURPOSE: Set FormId.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CHIPS_SETS_EDIT

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    ' Chips set
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5061)

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1291)
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True

    ' Name
    'EOR Change Name to Group
    Me.ef_chips_sets_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4807)
    Me.ef_chips_sets_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, MAX_NAME_LEN, 0)

    ' Enabled
    Me.chk_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4400)

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_DELETE).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_DELETE).Enabled = False

    ' Button Add / Delete rows to grid
    Me.btn_add.Enabled = Me.Permissions.Write
    Me.btn_del.Enabled = Me.Permissions.Write
    Me.btn_add.Text = GLB_NLS_GUI_CONTROLS.GetString(12)
    Me.btn_del.Text = GLB_NLS_GUI_CONTROLS.GetString(11)

    'EOR Initial Color White
    cd_button_color.FullOpen = True
    cd_button_color.Color = Color.White
    AddHandler dg_chips.RowSelectedEvent, AddressOf dgChips_RowSelect
    AddHandler dg_chips.CellDataChangedEvent, AddressOf dgChips_CellDataChange

    Call Me.GUI_StyleSheet()

    ' ChipType Combo
    Me.cmb_chip_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5062)
    Me.ChipTypeComboFill()

    'EOR 30-MAR-2016 Text Combo Iso Code And Fill
    Me.cmb_iso_code.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5270)

    m_num_rows_add_sub = 0
    m_chips_sets.GetLastChipId(m_id_max)
  End Sub ' GUI_InitControls

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    Me.m_chips_sets = Me.DbReadObject

    If Me.m_chips_sets.ChipsSetsId = 0 Then
      Me.ScreenMode = frm_base_edit.ENUM_SCREEN_MODE.MODE_NEW
    End If

    If Me.ScreenMode = frm_base_edit.ENUM_SCREEN_MODE.MODE_NEW Then
      Me.chk_enabled.Checked = True
    Else
      'EOR Only Name Group
      Me.ef_chips_sets_name.Value = m_chips_sets.Name

      ' Enabled
      Me.chk_enabled.Checked = m_chips_sets.Enabled

      'EOR 30-MAR-2016 Iso Code
      Me.m_iso_code = m_chips_sets.IsoCode
      Me.cmb_iso_code.TextValue = m_chips_sets.IsoCode

      'EOR 30-MAR-2016 ChipType
      Me.cmb_chip_type.Value = m_chips_sets.ChipType
      Me.cmb_chip_type.IsReadOnly = True
    End If

    ' Hide column
    CustomColumnsHideGrid(cmb_chip_type.Value)

    ' Grid
    Me.SetChipsDefinitions()
  End Sub ' GUI_SetScreenData

  ' PURPOSE: Verifies the data to save.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS
  Private Function IsDataSuccess() As Boolean
    Dim _gaming_tables_assignes As List(Of String)

    _gaming_tables_assignes = New List(Of String)

    ' Check title Set Chips
    If String.IsNullOrEmpty(Me.ef_chips_sets_name.Value.Trim()) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(4807)) ' "Field '%1' must not be left blank." param %1-> "Name"
      Return False
    End If


    ' EOR 01-APR-2016 Check Chip Set not exists
    If m_chips_sets.ExistsChipsSets(Me.m_chips_sets.ChipsSetsId, Me.ef_chips_sets_name.Value, m_chips_sets.IsoCode, cmb_chip_type.Value) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(163), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , Me.ef_chips_sets_name.Value)
      Return False
    End If

    ' EOR 01-APR-2016 Check Chip Type Maximun 
    If m_chips_sets.IsValidCreateChipSetType(cmb_chip_type.Value) = False _
      And ScreenMode <> ENUM_SCREEN_MODE.MODE_EDIT Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6929), ENUM_MB_TYPE.MB_TYPE_INFO, , , cmb_chip_type.TextValue, FeatureChips.NumMaxGroupsByChipType(cmb_chip_type.Value))
      Return False
    End If

    ' Check if Chip Sets assigned in gambling table
    If m_chips_sets.IsAssignedIn_GamingTable(m_chips_sets.ChipsSetsId, _gaming_tables_assignes) <> ENUM_STATUS.STATUS_OK AndAlso Not chk_enabled.Checked Then
      Dim _separator As String
      _separator = vbCrLf & " - "
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5096), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , _separator & String.Join(_separator, _gaming_tables_assignes.ToArray()))
      Return False
    End If

    ' Check at least one row in grid
    If Me.dg_chips.NumRows = 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2597), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)
      Return False
    End If

    If m_chips_sets.ChipType <> FeatureChips.ChipType.COLOR Then
      ' Check if Denomination is empty
      If Me.IsEqualValueInGrid(GRID_COLUMN_DENOMINATION_VALUE, String.Empty) Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1060))
        Return False
      End If

      ' Check Not duplicate denomination
      If Me.IsRepeatValueInGrid(GRID_COLUMN_DENOMINATION_VALUE) Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2796), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1060))
        Return False
      End If

      ' Check if Denomination is zero
      If Me.IsEqualValueInGrid(GRID_COLUMN_DENOMINATION_VALUE, "0") Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5067), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , )
        Return False
      End If

      ' Check if Denomination is numeric
      If Not Me.IsNumericValueInGrid(GRID_COLUMN_DENOMINATION_VALUE) Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6927), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , )
        Return False
      End If
    End If

    ' Check if Color is Empty
    If Me.IsEqualValueInGrid(GRID_COLUMN_ID_COLOR, String.Empty) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1291))
      Return False
    End If

    ' Check Not duplicate colors
    If Me.IsRepeatValueInGrid(GRID_COLUMN_ID_COLOR) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2796), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1291))
      Return False
    End If

    If m_chips_sets.ChipType = FeatureChips.ChipType.COLOR Then
      ' Check if Name is Empty
      If Me.IsEqualValueInGrid(GRID_COLUMN_NAME, String.Empty) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(4979))
        Return False
      End If

      ' Check Not duplicate Name
      If Me.IsRepeatValueInGrid(GRID_COLUMN_NAME) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2796), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(4979))
        Return False
      End If
    End If

    Return True

  End Function

  ' PURPOSE: Valid if the elements are numeric column grid.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS
  Private Function IsNumericValueInGrid(ByVal column_number) As Boolean
    Dim _idx_i As Integer
    Dim _isNumeric As Boolean

    _isNumeric = True
    For _idx_i = 0 To Me.dg_chips.NumRows - 1
      If Not IsNumeric(Me.dg_chips.Cell(_idx_i, column_number).Value) Then
        _isNumeric = False
        Exit For

      End If
    Next

    Return _isNumeric

  End Function

  ' PURPOSE: Validates if there are equal elements in the grid column.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS
  Private Function IsEqualValueInGrid(ByVal column_number As Integer, ByVal value As String) As Boolean
    Dim _idx_i As Integer
    Dim _isEqual As Boolean

    _isEqual = False
    For _idx_i = 0 To Me.dg_chips.NumRows - 1
      If Me.dg_chips.Cell(_idx_i, column_number).Value.Trim() = value Then
        _isEqual = True
        Exit For

      End If
    Next

    Return _isEqual

  End Function

  ' PURPOSE: Validates if there are repeated elements in the column of the grid.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS
  Private Function IsRepeatValueInGrid(ByVal column_number As Integer) As Boolean
    Dim _idx_i As Integer
    Dim _idx_j As Integer
    Dim _isRepeat As Boolean
    Dim _value1 As String
    Dim _value2 As String

    _isRepeat = False
    For _idx_i = 0 To Me.dg_chips.NumRows - 1
      _value1 = Me.dg_chips.Cell(_idx_i, column_number).Value
      For _idx_j = _idx_i + 1 To Me.dg_chips.NumRows - 1
        _value2 = Me.dg_chips.Cell(_idx_j, column_number).Value
        If _value1 = _value2 Then
          _isRepeat = True
          Exit For

        End If
      Next
      If _isRepeat Then
        Exit For

      End If
    Next

    Return _isRepeat

  End Function

  ' PURPOSE : Check if screen data is ok before saving
  '
  '  PARAMS :
  '     -  INPUT :
  '     - OUTPUT :
  '               - True / False
  '
  ' RETURNS :
  '
  '   NOTES :
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Dim _success As Boolean

    _success = Me.IsDataSuccess()

    If _success And Not Me.chk_enabled.Checked Then
      If Not (NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5098), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING,
                    mdl_NLS.ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_YES) Then
        _success = False
      End If
    End If

    Return _success

  End Function ' GUI_IsScreenDataOk

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_GetScreenData()
    Dim _chip As DataRow
    Dim _row_index As Integer

    m_chips_sets = Me.DbEditedObject

    ' Name
    'EOR Only Name Group
    m_chips_sets.Name = Me.ef_chips_sets_name.Value

    ' Enabled
    m_chips_sets.Enabled = Me.chk_enabled.Checked

    ' ChipType
    m_chips_sets.ChipType = Me.cmb_chip_type.Value

    'Iso Code
    If Me.cmb_iso_code.TextValue Is Nothing Then
      m_chips_sets.IsoCode = "X02"
    Else
      m_chips_sets.IsoCode = IIf(Me.cmb_iso_code.TextValue.Equals("--"), Cage.CHIPS_COLOR, Me.cmb_iso_code.TextValue)
    End If


    ' Grid Info
    For _row_index = 0 To Me.dg_chips.NumRows - 1

      _chip = Me.GetDataRow(_row_index)

      If _chip(CLS_CHIPS_SET.SQL_COLUMN_ALLOWED) <> GridValueToBool(Me.dg_chips.Cell(_row_index, GRID_COLUMN_ALLOWED).Value) Then
        _chip(CLS_CHIPS_SET.SQL_COLUMN_ALLOWED) = GridValueToBool(Me.dg_chips.Cell(_row_index, GRID_COLUMN_ALLOWED).Value)
      End If

      If _chip(CLS_CHIPS_SET.SQL_COLUMN_CHIP_TYPE) <> Me.dg_chips.Cell(_row_index, GRID_COLUMN_CHIP_TYPE).Value Then
        _chip(CLS_CHIPS_SET.SQL_COLUMN_CHIP_TYPE) = Me.dg_chips.Cell(_row_index, GRID_COLUMN_CHIP_TYPE).Value
      End If

      If _chip(CLS_CHIPS_SET.SQL_COLUMN_COLOR) <> Me.dg_chips.Cell(_row_index, GRID_COLUMN_ID_COLOR).Value Then
        _chip(CLS_CHIPS_SET.SQL_COLUMN_COLOR) = Me.dg_chips.Cell(_row_index, GRID_COLUMN_ID_COLOR).Value
      End If

      If _chip(CLS_CHIPS_SET.SQL_COLUMN_DENOMINATION) <> Me.dg_chips.Cell(_row_index, GRID_COLUMN_DENOMINATION_VALUE).Value Then
        _chip(CLS_CHIPS_SET.SQL_COLUMN_DENOMINATION) = Me.dg_chips.Cell(_row_index, GRID_COLUMN_DENOMINATION_VALUE).Value
      End If

      If (IsDBNull(_chip(CLS_CHIPS_SET.SQL_COLUMN_DRAWING))) Then
        _chip(CLS_CHIPS_SET.SQL_COLUMN_DRAWING) = ""
      End If

      If _chip(CLS_CHIPS_SET.SQL_COLUMN_DRAWING) <> Me.dg_chips.Cell(_row_index, GRID_COLUMN_DRAWING).Value Then
        _chip(CLS_CHIPS_SET.SQL_COLUMN_DRAWING) = Me.dg_chips.Cell(_row_index, GRID_COLUMN_DRAWING).Value
      End If

      If _chip(CLS_CHIPS_SET.SQL_COLUMN_ISO_CODE) <> Me.m_chips_sets.IsoCode Then
        _chip(CLS_CHIPS_SET.SQL_COLUMN_ISO_CODE) = Me.m_chips_sets.IsoCode
      End If

      If (IsDBNull(_chip(CLS_CHIPS_SET.SQL_COLUMN_NAME))) Then
        _chip(CLS_CHIPS_SET.SQL_COLUMN_NAME) = ""
      End If

      If _chip(CLS_CHIPS_SET.SQL_COLUMN_NAME) <> Me.dg_chips.Cell(_row_index, GRID_COLUMN_NAME).Value Then
        _chip(CLS_CHIPS_SET.SQL_COLUMN_NAME) = Me.dg_chips.Cell(_row_index, GRID_COLUMN_NAME).Value
      End If

    Next
  End Sub ' GUI_GetScreenData

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLS_CHIPS_SET
        m_chips_sets = DbEditedObject

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE: Button overridable operations. 
  '          Define actions for each button are pressed.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)
    Dim _valor As Integer
    Dim _bytes As Byte()

    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0

        If Me.dg_chips.NumRows <= 0 Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6926), ENUM_MB_TYPE.MB_TYPE_WARNING)
          Return

        End If

        If Me.dg_chips.SelectedRows.Length <= 0 Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6926), ENUM_MB_TYPE.MB_TYPE_WARNING)
          Return

        End If

        If Not IsAllowEditableChip(Me.dg_chips.Cell(dg_chips.SelectedRows(0), GRID_COLUMN_ID).Value) Then
          Return

        End If

        _valor = Me.dg_chips.Cell(dg_chips.SelectedRows(0), GRID_COLUMN_ID_COLOR).Value
        _bytes = BitConverter.GetBytes(_valor)

        If _bytes.Length = 4 Then
          If _valor <> -1 Then
            cd_button_color.Color = System.Drawing.Color.FromArgb(_bytes(3), _bytes(2), _bytes(1), _bytes(0))
          Else
            cd_button_color.Color = Color.Gray
          End If
        Else
          cd_button_color.Color = Color.Gray
        End If

        If cd_button_color.ShowDialog() = Windows.Forms.DialogResult.OK Then
          ProcessColor(cd_button_color.Color)
          Me.dg_chips.Cell(Me.dg_chips.SelectedRows(0), GRID_COLUMN_ID_COLOR).Value = cd_button_color.Color.ToArgb()
          'EOR 16-FEB-2016 BackColor Form Color
          Me.dg_chips.Cell(Me.dg_chips.SelectedRows(0), GRID_COLUMN_COL_COLOR).BackColor = cd_button_color.Color
        End If
        'END COLOR

      Case frm_base_sel.ENUM_BUTTON.BUTTON_OK
        'ACCEPT

        Call MyBase.GUI_ButtonClick(ButtonId)


      Case frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL
        MyBase.GUI_ButtonClick(ButtonId)

    End Select

  End Sub  ' GUI_ButtonClick

#End Region ' Overrides

#Region " Private Functions "

  ' PURPOSE: Define style for chips grid 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  Private Sub GUI_StyleSheet()

    With Me.dg_chips
      Call .Init(GRID_COLUMNS_CUR, GRID_HEADER_ROWS_CUR, True)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Index
      .Column(GRID_COLUMN_INDEX).Header.Text = " "
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' ID Empty
      .Column(GRID_COLUMN_ID).Header.Text = " "
      .Column(GRID_COLUMN_ID).Width = GRID_WIDTH_ID
      .Column(GRID_COLUMN_ID).HighLightWhenSelected = False
      .Column(GRID_COLUMN_ID).IsColumnPrintable = False

      ' Denomination
      .Column(GRID_COLUMN_DENOMINATION).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4357)
      .Column(GRID_COLUMN_DENOMINATION).Width = GRID_WIDTH_DENOMINATION
      .Column(GRID_COLUMN_DENOMINATION).Editable = True
      .Column(GRID_COLUMN_DENOMINATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_DENOMINATION).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_DENOMINATION).EditionControl.EntryField.SetFilter(FORMAT_NUMBER, 7, 2)
      .Column(GRID_COLUMN_DENOMINATION).ColDataType = CLASS_COL_DATA.ENUM_COLDATATYPE.FLEX_DT_CURRENCY

      ' Denomination Value
      .Column(GRID_COLUMN_DENOMINATION_VALUE).Header.Text = " "
      .Column(GRID_COLUMN_DENOMINATION_VALUE).Width = GRID_WIDTH_DENOMINATION_VALUE
      .Column(GRID_COLUMN_DENOMINATION_VALUE).HighLightWhenSelected = False
      .Column(GRID_COLUMN_DENOMINATION_VALUE).IsColumnPrintable = False

      ' Chips ID Color
      .Column(GRID_COLUMN_ID_COLOR).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1291)
      .Column(GRID_COLUMN_ID_COLOR).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_ID_COLOR).Editable = False
      .Column(GRID_COLUMN_ID_COLOR).Width = 0

      ' 'EOR 16-FEB-2016 Chips Col Color
      .Column(GRID_COLUMN_COL_COLOR).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1291)
      .Column(GRID_COLUMN_COL_COLOR).Width = GRID_WIDTH_COLOR
      .Column(GRID_COLUMN_COL_COLOR).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_COL_COLOR).Editable = False

      ' Chips Name
      .Column(GRID_COLUMN_NAME).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4979)
      .Column(GRID_COLUMN_NAME).Width = GRID_WIDTH_NAME
      .Column(GRID_COLUMN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_NAME).Editable = True
      .Column(GRID_COLUMN_NAME).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_NAME).EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50, 0)

      ' Chips Drawing
      .Column(GRID_COLUMN_DRAWING).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6924)
      .Column(GRID_COLUMN_DRAWING).Width = GRID_WIDTH_DRAWING
      .Column(GRID_COLUMN_DRAWING).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_DRAWING).Editable = True
      .Column(GRID_COLUMN_DRAWING).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_DRAWING).EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50, 0)

      ' Iso Code
      .Column(GRID_COLUMN_ISO_CODE).Header.Text = ""
      .Column(GRID_COLUMN_ISO_CODE).Width = GRID_WIDTH_ISO_CODE
      .Column(GRID_COLUMN_ISO_CODE).HighLightWhenSelected = False
      .Column(GRID_COLUMN_ISO_CODE).IsColumnPrintable = False

      ' Allowed
      .Column(GRID_COLUMN_ALLOWED).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4400)
      .Column(GRID_COLUMN_ALLOWED).Width = GRID_WIDTH_ALLOWED
      .Column(GRID_COLUMN_ALLOWED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_ALLOWED).IsMerged = False
      .Column(GRID_COLUMN_ALLOWED).Editable = True
      .Column(GRID_COLUMN_ALLOWED).EditionControl.Type = CONTROL_TYPE_CHECK_BOX

      ' Chip Type
      .Column(GRID_COLUMN_CHIP_TYPE).Header.Text = ""
      .Column(GRID_COLUMN_CHIP_TYPE).Width = GRID_WIDTH_CHIP_TYPE
      .Column(GRID_COLUMN_CHIP_TYPE).HighLightWhenSelected = False
      .Column(GRID_COLUMN_CHIP_TYPE).IsColumnPrintable = False

      ' ID aux
      .Column(GRID_COLUMN_ID_AUX).Header.Text = " "
      .Column(GRID_COLUMN_ID_AUX).Width = GRID_WIDTH_ID_AUX
      .Column(GRID_COLUMN_ID_AUX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_ID_AUX).IsColumnPrintable = False

    End With
  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set chips grid with information in chips datatable
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  Private Sub SetChipsDefinitions()
    Dim _idx_row As Integer

    For Each _row As DataRow In Me.m_chips_sets.ChipsDefinition.Rows
      _idx_row = dg_chips.AddRow()

      If Not _row(CLS_CHIPS_SET.SQL_COLUMN_ID) Is DBNull.Value Then
        dg_chips.Cell(_idx_row, GRID_COLUMN_ID).Value = _row(CLS_CHIPS_SET.SQL_COLUMN_ID)
      End If

      If Not _row(CLS_CHIPS_SET.SQL_COLUMN_DENOMINATION) Is DBNull.Value Then
        dg_chips.Cell(_idx_row, GRID_COLUMN_DENOMINATION).Value = GUI_FormatNumber(_row(CLS_CHIPS_SET.SQL_COLUMN_DENOMINATION), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, False)
        dg_chips.Cell(_idx_row, GRID_COLUMN_DENOMINATION_VALUE).Value = Double.Parse(_row(CLS_CHIPS_SET.SQL_COLUMN_DENOMINATION))
      Else
        dg_chips.Cell(_idx_row, GRID_COLUMN_DENOMINATION).Value = GUI_FormatNumber(0, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, False)
        dg_chips.Cell(_idx_row, GRID_COLUMN_DENOMINATION_VALUE).Value = 0
      End If

      If Not _row(CLS_CHIPS_SET.SQL_COLUMN_COLOR) Is DBNull.Value Then
        dg_chips.Cell(_idx_row, GRID_COLUMN_ID_COLOR).Value = _row(CLS_CHIPS_SET.SQL_COLUMN_COLOR)
        'EOR 16-FEB-2016 Paint Cell
        dg_chips.Cell(_idx_row, GRID_COLUMN_COL_COLOR).BackColor = Color.FromArgb(_row(CLS_CHIPS_SET.SQL_COLUMN_COLOR))
      Else
        dg_chips.Cell(_idx_row, GRID_COLUMN_ID_COLOR).Value = Color.White.ToArgb()
        'EOR 16-FEB-2016 Paint Cell
        dg_chips.Cell(_idx_row, GRID_COLUMN_COL_COLOR).BackColor = Color.White
      End If

      If Not _row(CLS_CHIPS_SET.SQL_COLUMN_NAME) Is DBNull.Value Then
        dg_chips.Cell(_idx_row, GRID_COLUMN_NAME).Value = _row(CLS_CHIPS_SET.SQL_COLUMN_NAME)
      End If

      If Not _row(CLS_CHIPS_SET.SQL_COLUMN_DRAWING) Is DBNull.Value Then
        dg_chips.Cell(_idx_row, GRID_COLUMN_DRAWING).Value = _row(CLS_CHIPS_SET.SQL_COLUMN_DRAWING)
      End If

      If Not _row(CLS_CHIPS_SET.SQL_COLUMN_ISO_CODE) Is DBNull.Value Then
        dg_chips.Cell(_idx_row, GRID_COLUMN_ISO_CODE).Value = _row(CLS_CHIPS_SET.SQL_COLUMN_ISO_CODE)
      End If

      dg_chips.Cell(_idx_row, GRID_COLUMN_ALLOWED).Value = uc_grid.GRID_CHK_UNCHECKED
      If Not _row(CLS_CHIPS_SET.SQL_COLUMN_ALLOWED) Is DBNull.Value AndAlso Boolean.Parse(_row(CLS_CHIPS_SET.SQL_COLUMN_ALLOWED)) Then
        dg_chips.Cell(_idx_row, GRID_COLUMN_ALLOWED).Value = uc_grid.GRID_CHK_CHECKED
      End If

      If Not _row(CLS_CHIPS_SET.SQL_COLUMN_CHIP_TYPE) Is DBNull.Value Then
        dg_chips.Cell(_idx_row, GRID_COLUMN_CHIP_TYPE).Value = _row(CLS_CHIPS_SET.SQL_COLUMN_CHIP_TYPE)
      End If

      If Not _row(CLS_CHIPS_SET.SQL_COLUMN_ID_AUX) Is DBNull.Value Then
        dg_chips.Cell(_idx_row, GRID_COLUMN_ID_AUX).Value = _row(CLS_CHIPS_SET.SQL_COLUMN_ID_AUX)
      End If
    Next

    dg_chips.Redraw = True
    If dg_chips.NumRows > 0 Then
      dg_chips.IsSelected(0) = True
    End If

    Call InformNumberRows()

  End Sub ' SetChipsDefinitions

  ' PURPOSE : Trick to set cell backround color to Black
  '
  '  PARAMS :
  '     - INPUT : Color
  '     - OUTPUT : Color
  '
  ' RETURNS : 
  '
  Private Sub ProcessColor(ByRef CageColor)

    If (CageColor.A = Color.Black.A And _
        CageColor.R = Color.Black.R And _
        CageColor.G = Color.Black.G And _
        CageColor.B = Color.Black.B) Then

      CageColor = Color.FromArgb(CageColor.A, CageColor.R + 1, CageColor.G, CageColor.B)

    End If

  End Sub ' ProcessColor

  Private Sub InformNumberRows()

    lbl_max_chips_for_set.Text = Me.m_chips_sets.ChipsDefinition.Rows.Count.ToString() & " / " & m_max_num_rows.ToString()

    If (Me.m_chips_sets.ChipsDefinition.Rows.Count > m_max_num_rows) Then
      lbl_max_chips_for_set.ForeColor = Color.Red
    End If

  End Sub

  ' PURPOSE: Verify that the color not exist.
  '
  '  PARAMS:
  '     - INPUT:
  '           - color select
  '     - OUTPUT:
  '           - true or false. if the color exist in grid.
  '
  ' RETURNS:
  '     - None
  Private Function Exist_Color(ByVal color As Integer, ByVal row As Integer) As Boolean
    Dim _return As Boolean

    _return = False

    ' If color <> -1 Then 'white
    For _idx_row As Integer = 0 To Me.dg_chips.NumRows - 1


      If row <> _idx_row Then

        _return = True
        Exit For

      End If



    Next

    '  End If

    Return _return

  End Function ' Exist_Color

  ' PURPOSE:  Cast from uc_grid checked to boolean
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GridValueToBool(ByVal enabled As String) As Boolean

    If (enabled.Equals(uc_grid.GRID_CHK_CHECKED) Or enabled.Equals(uc_grid.GRID_CHK_CHECKED_DISABLED)) Then
      Return True

    Else
      Return False

    End If

  End Function ' GridValueToBool

  ' PURPOSE: Insert a new row in grid and in BD
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function InsertRow() As Integer

    Dim _idx_row As Integer
    Dim _str_new_chip(8) As String

    _idx_row = dg_chips.AddRow()
    m_id_max = m_id_max + m_num_rows_add_sub
    Me.dg_chips.Cell(_idx_row, GRID_COLUMN_ID).Value = -1
    Me.dg_chips.Cell(_idx_row, GRID_COLUMN_DENOMINATION).Value = GUI_FormatCurrency(0, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, False)
    Me.dg_chips.Cell(_idx_row, GRID_COLUMN_DENOMINATION_VALUE).Value = 0
    Me.dg_chips.Cell(_idx_row, GRID_COLUMN_ID_COLOR).Value = Color.White.ToArgb()
    'EOR 16-FEB-2016 Paint Cell
    Me.dg_chips.Cell(_idx_row, GRID_COLUMN_COL_COLOR).BackColor = Color.White
    Me.dg_chips.Cell(_idx_row, GRID_COLUMN_NAME).Value = String.Empty
    Me.dg_chips.Cell(_idx_row, GRID_COLUMN_DRAWING).Value = String.Empty
    Me.dg_chips.Cell(_idx_row, GRID_COLUMN_ISO_CODE).Value = Me.m_chips_sets.IsoCode
    Me.dg_chips.Cell(_idx_row, GRID_COLUMN_ALLOWED).Value = uc_grid.GRID_CHK_CHECKED
    Me.dg_chips.Cell(_idx_row, GRID_COLUMN_CHIP_TYPE).Value = Me.cmb_chip_type.Value
    Me.dg_chips.Cell(_idx_row, GRID_COLUMN_ID_AUX).Value = Me.m_id_max + 1

    _str_new_chip(CLS_CHIPS_SET.SQL_COLUMN_DENOMINATION) = 0
    _str_new_chip(CLS_CHIPS_SET.SQL_COLUMN_COLOR) = Color.White.ToArgb()
    _str_new_chip(CLS_CHIPS_SET.SQL_COLUMN_NAME) = String.Empty
    _str_new_chip(CLS_CHIPS_SET.SQL_COLUMN_DRAWING) = String.Empty
    _str_new_chip(CLS_CHIPS_SET.SQL_COLUMN_ISO_CODE) = Me.m_chips_sets.IsoCode
    _str_new_chip(CLS_CHIPS_SET.SQL_COLUMN_ALLOWED) = True
    _str_new_chip(CLS_CHIPS_SET.SQL_COLUMN_CHIP_TYPE) = Me.cmb_chip_type.Value
    _str_new_chip(CLS_CHIPS_SET.SQL_COLUMN_ID_AUX) = Me.m_id_max + 1

    m_chips_sets = Me.DbEditedObject
    m_chips_sets.ChipsDefinition.Rows.Add(_str_new_chip)

    cd_button_color.Color = Color.White
    m_num_rows_add_sub = m_num_rows_add_sub + 1

    Call InformNumberRows()

    Return _idx_row

  End Function ' InsertRow

  ' PURPOSE: Check pending movements 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - Bolean
  '
  ' RETURNS:
  '     - Boolean. True: there is  no movements. False: there is  movements.
  Private Function IsAllowEditableChip(ByVal _chip_id As Integer) As Boolean

    Dim _rc_result As ENUM_CONFIGURATION_RC
    Dim _bool_return As Boolean

    _rc_result = m_chips_sets.NotPendingMovements_Chips(_chip_id)

    Select Case _rc_result

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        _bool_return = False
        Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(138), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK) 'An error occurred while accessing the database

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DEPENDENCIES
        _bool_return = False
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5066), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK) ' Chips with pending movements cannot be disabled

      Case ENUM_STATUS.STATUS_OK
        _bool_return = True

      Case Else
        _bool_return = False
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(138), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK) 'An error occurred while accessing the database

    End Select

    Return _bool_return

  End Function ' IsAllowEditableChip

  ' PURPOSE: Delete a new row in grid and in BD
  '
  '  PARAMS:
  '     - INPUT:
  '           - _idx_row As Integer 
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub DeleteRow(ByVal _idx_row As Integer)
    Dim _dr_delete_chip As DataRow

    m_chips_sets = Me.DbEditedObject

    _dr_delete_chip = Me.GetDataRow(_idx_row)
    _dr_delete_chip.Delete()

    Me.dg_chips.IsSelected(_idx_row) = False
    Me.dg_chips.DeleteRow(_idx_row)

    Call InformNumberRows()

  End Sub ' DeleteRow

  ' PURPOSE: Get DataRow
  '
  '  PARAMS:
  '     - INPUT:
  '           - index As Integer 
  '     - OUTPUT:
  '           - DataRow
  '
  ' RETURNS:
  '     - None
  Private Function GetDataRow(ByVal index As Integer) As DataRow
    Dim _row As DataRow

    If Me.dg_chips.Cell(index, GRID_COLUMN_ID).Value = -1 Then

      _row = Me.GetDataRow(Me.dg_chips.Cell(index, GRID_COLUMN_ID_AUX).Value, "ch_chip_id_aux")
    Else

      _row = Me.GetDataRow(Me.dg_chips.Cell(index, GRID_COLUMN_ID).Value, "ch_chip_id")
    End If

    Return _row
  End Function ' GetDataRow

  ' PURPOSE: Get DataRow
  '
  '  PARAMS:
  '     - INPUT:
  '           - id As long 
  '           - field As string
  '     - OUTPUT:
  '           - DataRow
  '
  ' RETURNS:
  '     - None
  Private Function GetDataRow(ByVal id As Long, ByVal field As String) As DataRow
    Dim _row As DataRow

    _row = m_chips_sets.ChipsDefinition.Select(String.Format("{0} = {1}", field, id))(0)

    Return _row
  End Function 'GetDataRow

  ' PURPOSE:  Check row is null
  '
  '  PARAMS:
  '     - INPUT:
  '           - _idx_row AS Integer
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Boolean
  Private Function IsNullRow(ByVal _idx_row As Integer) As Boolean
    If m_chips_sets.ChipType = WSI.Common.Cage.CHIPS_VALUE Then
      'If String.IsNullOrEmpty(dg_chips.Cell(_idx_row, GRID_COLUMN_DENOMINATION_VALUE).Value) Or dg_chips.Cell(_idx_row, GRID_COLUMN_DENOMINATION_VALUE).Value = 0 Then
      '  Return True
      'End If
    End If
  End Function ' IsNullRow

  ' PURPOSE: Get Int of String with Currency values. 
  '
  '  PARAMS:
  '     - INPUT:
  '           - Value
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Integer. the clean Import.
  Private Function GetSinCurrency(ByVal Valor As String) As Integer
    Dim _idx As Int16

    For _idx = 0 To Valor.Length - 1
      If Not IsNumeric(Valor.Substring(_idx, 1)) Then
        Valor = Valor.Remove(_idx, 1)
        _idx = _idx - 1
      Else
        Exit For

      End If
    Next

    Return Valor

  End Function ' GetSinCurrency

  ' PURPOSE: Fill currencies combo with allowed currency
  '
  '  PARAMS:
  '     - INPUT:
  '           - Value
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '
  Private Sub IsoCodeComboFill(ByVal ChipTypeSelect As FeatureChips.ChipType)
    cmb_iso_code.Clear()

    If ChipTypeSelect <> FeatureChips.ChipType.COLOR Then
      Dim _national_currency As New WSI.Common.CurrencyExchange
      Dim _allowed_currency As New List(Of WSI.Common.CurrencyExchange)
      Dim _search_currency_type As New List(Of WSI.Common.CurrencyExchangeType)
      Dim _currency_accepted As String
      Dim _currency_array As String()

      _search_currency_type.Add(WSI.Common.CurrencyExchangeType.CURRENCY)
      WSI.Common.CurrencyExchange.GetAllowedCurrencies(True, _search_currency_type, False, _national_currency, _allowed_currency)

      _currency_accepted = GeneralParam.GetString("RegionalOptions", "CurrenciesAccepted")

      If m_multiple_currency Then

        Dim _str_one_word As String = String.Empty

        _currency_array = _currency_accepted.Split(";")

        For Each _str_one_word In _currency_array
          Me.cmb_iso_code.Add(_str_one_word)
        Next
      Else
        Me.cmb_iso_code.Add(_national_currency.CurrencyCode)
      End If
    Else
      Me.cmb_iso_code.Add("--")
      Me.cmb_iso_code.SelectedIndex = 0
    End If

    If Me.m_chips_sets.ChipsSetsId <> 0 Then
      Me.cmb_iso_code.TextValue = m_chips_sets.IsoCode
      Me.cmb_iso_code.IsReadOnly = True
    Else
      Me.cmb_iso_code.IsReadOnly = Not m_multiple_currency
    End If
  End Sub ' CurrencyComboFill

  ' PURPOSE: Fill combo chip types
  '
  '  PARAMS:
  '     - INPUT:
  '           - Value
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '
  Private Sub ChipTypeComboFill()
    For Each _item As FeatureChips.ChipType In Me.m_list_chip_types.Keys
      If FeatureChips.IsChipsTypeEnabledGP(_item) Then
        Me.cmb_chip_type.Add(_item, m_list_chip_types.Item(_item))
      End If
    Next
  End Sub ' ChipTypeComboFill

  ' PURPOSE: Get chip types
  '
  '  PARAMS:
  '     - INPUT:
  '           - Value
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '
  Private Function GetChipTypes() As IDictionary(Of FeatureChips.ChipType, String)


    Return CLS_CHIPS_SET.GetNamesChipType()

  End Function ' GetChipTypes

  ' PURPOSE: Custom Columns Grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '
  Private Sub CustomColumnsHideGrid(ByVal TypeChip As FeatureChips.ChipType)
    If TypeChip = FeatureChips.ChipType.COLOR Then
      With Me.dg_chips
        .Column(GRID_COLUMN_DENOMINATION).Header.Text = String.Empty
        .Column(GRID_COLUMN_DENOMINATION).Width = 1
        .Column(GRID_COLUMN_DENOMINATION).IsColumnPrintable = False
        .Column(GRID_COLUMN_DENOMINATION).HighLightWhenSelected = False
        .Column(GRID_COLUMN_NAME).Width = GRID_WIDTH_NAME + GRID_WIDTH_DENOMINATION
      End With
    Else
      With Me.dg_chips
        .Column(GRID_COLUMN_DENOMINATION).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5555)
        .Column(GRID_COLUMN_DENOMINATION).Width = GRID_WIDTH_DENOMINATION
        .Column(GRID_COLUMN_DENOMINATION).IsColumnPrintable = True
        .Column(GRID_COLUMN_DENOMINATION).HighLightWhenSelected = True
        .Column(GRID_COLUMN_NAME).Width = GRID_WIDTH_NAME
      End With
    End If
  End Sub

#End Region ' Private Functions

#Region " Events"

  ' PURPOSE: Handler to control Click on add buton
  '          - Add new row and focused
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub btn_add_ClickEvent() Handles btn_add.ClickEvent
    Dim _idx_row As Integer

    ' Check Permissions
    If Not Permissions.Write Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Return
    End If

    ' Check Max chips
    If Me.dg_chips.NumRows >= m_max_num_rows Then
      btn_add.Enabled = False
      Return
    End If

    _idx_row = Me.InsertRow()

    ' Focus on Row added
    dg_chips.ClearSelection()
    dg_chips.IsSelected(_idx_row) = True
    dg_chips.Redraw = True
    dg_chips.TopRow = _idx_row

  End Sub ' btn_add_ClickEvent

  ' PURPOSE: Handler to control Click on delete buton
  '          - If not pending movements delete the row and select previous row
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub btn_del_ClickEvent() Handles btn_del.ClickEvent
    Dim _idx_row As Integer
    Dim _select_row() As Integer

    _select_row = Me.dg_chips.SelectedRows()

    If _select_row Is Nothing Then
      Return
    End If

    If _select_row.Length = 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6926), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Return
    End If

    ' Check Permissions
    If Not Permissions.Delete Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Return
    End If

    _idx_row = _select_row(0)

    ' Check are pending movements for this chip
    If Not IsAllowEditableChip(Me.dg_chips.Cell(_idx_row, GRID_COLUMN_ID).Value) Then
      Return
    End If

    Me.DeleteRow(_idx_row)
    m_num_rows_add_sub = m_num_rows_add_sub - 1

    ' Select previous Row
    If Me.dg_chips.NumRows > 0 Then
      Me.dg_chips.ClearSelection()
    End If

    ' Check Max chips
    If Me.dg_chips.NumRows < m_max_num_rows Then
      btn_add.Enabled = True
    End If

  End Sub ' btn_del_ClickEvent

  ' PURPOSE: Handler to control cells changes  in grid Chips
  '           - Save Denomination value in hide column and apply format in visible denomination column  
  '
  '  PARAMS:
  '     - INPUT:
  '           - Index Row
  '           - Index Col
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub dgChips_CellDataChange(ByVal Row As Integer, ByVal Column As Integer) Handles dg_chips.CellDataChangedEvent

    If dg_chips Is Nothing OrElse dg_chips.NumRows <= 0 Then
      Exit Sub
    End If

    If Me.dg_chips.NumRows > 0 Then
      dg_chips.Cell(Row, GRID_COLUMN_DENOMINATION_VALUE).Value =
      GUI_ParseCurrency(IIf(String.IsNullOrEmpty(dg_chips.Cell(Row, GRID_COLUMN_DENOMINATION).Value), 0, dg_chips.Cell(Row, GRID_COLUMN_DENOMINATION).Value))
    End If

  End Sub ' dgChips_CellDataChange

  ' PURPOSE: Handler to control selected row in the grid
  '          - Unpaint column color to view the real color
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub dgChips_RowSelect(ByVal SelectedRow As Integer) Handles dg_chips.RowSelectedEvent
    If dg_chips Is Nothing OrElse dg_chips.NumRows <= 0 Then
      Exit Sub
    End If

    Try

      If SelectedRow >= 0 And dg_chips.NumRows >= 0 Then

        dg_chips.Redraw = False
        dg_chips.Redraw = True
        dg_chips.Column(GRID_COLUMN_COL_COLOR).HighLightWhenSelected = False

        If Me.dg_chips.Cell(SelectedRow, GRID_COLUMN_ID).Value > 0 Then
          Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False
          btn_del.Enabled = False
        Else
          Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = True
          btn_del.Enabled = True
        End If

      End If
    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)

    End Try
  End Sub ' dgChips_RowSelect

  ' PURPOSE: Handler to control key pressed intro in the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean
    Select Case e.KeyChar
      Case Chr(Keys.Enter)

        If dg_chips.ContainsFocus Then
          dg_chips.KeyPressed(sender, e)
        End If

        Return True

      Case Chr(Keys.Escape)

        Return MyBase.GUI_KeyPress(sender, e)

      Case Else
    End Select

    ' The keypress event is done in uc_grid control

  End Function ' GUI_KeyPress

  ' PURPOSE: Handler to control change value in currency combo
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub cmb_chip_type_ValueChangedEvent() Handles cmb_chip_type.ValueChangedEvent
    Dim _idx_row As Integer
    _idx_row = 0

    For _idx_row = 0 To dg_chips.NumRows - 1
      Me.m_chips_sets.ChipsDefinition.Rows(_idx_row).Item(CLS_CHIPS_SET.SQL_COLUMN_ISO_CODE) = Me.cmb_chip_type.TextValue
      Me.dg_chips.Cell(_idx_row, GRID_COLUMN_CHIP_TYPE).Value = Me.cmb_chip_type.Value
    Next

    'EOR 01-APR-2016
    IsoCodeComboFill(cmb_chip_type.Value)
    CustomColumnsHideGrid(cmb_chip_type.Value)
  End Sub ' cmb_currency_ValueChangedEvent

  Private Sub dg_chips_BeforeStartEditionEvent(Row As Integer, Column As Integer, ByRef EditionCanceled As Boolean) Handles dg_chips.BeforeStartEditionEvent

    If (Column = GRID_COLUMN_DENOMINATION Or Column = GRID_COLUMN_ID_COLOR) And Me.dg_chips.Cell(Row, GRID_COLUMN_ID).Value > 0 Then
      EditionCanceled = True
    End If

  End Sub


#End Region ' Events
End Class