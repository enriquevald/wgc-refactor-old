﻿'-------------------------------------------------------------------
' Copyright © 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_gamegateway_terminals_edit.vb
'
' DESCRIPTION:   GameGateway terminals selection configuration.
'
' AUTHOR:        Jorge Concheyro
'
' CREATION DATE: 13-OCT-2015
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 13-OCT-2015  JRC    Initial version.
' 22-OCT-2015  AVZ    Change InTouchGames to GameGateway
'--------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports System.Data.OleDb
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Math
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports WSI.Common
Imports System.Collections
Imports System.Globalization

Public Class frm_gamegateway_terminals_edit
  Inherits frm_base_edit

#Region " Constants "

#End Region 'Constants

#Region " Members "
  Private m_GameGateway_cfg As cls_gamegateway_terminals
#End Region ' Members

#Region "Overrides"

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Public Overloads Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
    Me.MdiParent = MdiParent
    Me.DbObjectId = Nothing

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(False)
    End If

  End Sub ' ShowEditItem


  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_GAMEGATEWAY_TERMINALS

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()
    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()


    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6835)

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_DELETE).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_DELETE).Enabled = False

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_OK).Visible = Me.Permissions.Write
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_OK).Enabled = Me.Permissions.Write

    Me.chk_gp_gamegateway_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6836)


    Me.chk_gp_gamegateway_enabled.Enabled = Me.Permissions.Write






  End Sub 'GUI_InitControls

  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Return True
  End Function 'GUI_IsScreenDataOk

  Protected Overrides Sub GUI_GetScreenData()
    Dim _GameGateway_cfg As cls_gamegateway_terminals
    _GameGateway_cfg = Me.DbEditedObject
    Me.dg_providers.GetTerminalsFromControl(_GameGateway_cfg.TerminalList)
    _GameGateway_cfg.GameGateway_EnabledGP = Me.chk_gp_gamegateway_enabled.Checked

  End Sub 'GUI_GetScreenData

  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    Dim _GameGateway_cfg As cls_gamegateway_terminals
    _GameGateway_cfg = DbReadObject
    Me.dg_providers.SetTerminalList(_GameGateway_cfg.TerminalList)
    Me.chk_gp_gamegateway_enabled.Checked = _GameGateway_cfg.GameGateway_EnabledGP
    Me.dg_providers.Enabled = _GameGateway_cfg.GameGateway_EnabledGP
  End Sub 'GUI_SetScreenData


  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)
    Dim GameGateway_cfg As cls_gamegateway_terminals

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New cls_gamegateway_terminals
        GameGateway_cfg = DbEditedObject


      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6837), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6838), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6838), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE
        GameGateway_cfg = Me.DbEditedObject

      Case ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE
        If Me.DbEditedObject IsNot Nothing Then
          GameGateway_cfg = Me.DbEditedObject
        End If
        Call MyBase.GUI_DB_Operation(DbOperation)
      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    Return MyBase.GUI_KeyPress(sender, e)

  End Function ' GUI_KeyPress

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    If ButtonId = ENUM_BUTTON.BUTTON_OK Then

      ' Assign to ERROR, so the message will be only shown when OK.

      DbStatus = ENUM_STATUS.STATUS_ERROR

      MyBase.GUI_ButtonClick(ButtonId)

      If DbStatus = ENUM_STATUS.STATUS_OK Then

        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(108), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO)
      End If

    Else
      MyBase.GUI_ButtonClick(ButtonId)
    End If


  End Sub ' GUI_ButtonClick


#End Region 'Overrides

#Region " Events "

  Private Sub chk_gp_gamegateway_enabled_CheckedChanged(sender As Object, e As EventArgs) Handles chk_gp_gamegateway_enabled.CheckedChanged
    dg_providers.Enabled = chk_gp_gamegateway_enabled.Checked And Me.Permissions.Write
  End Sub

#End Region ' Events

End Class