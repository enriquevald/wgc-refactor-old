'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_play_session_close.vb
' DESCRIPTION:   Play session close
' AUTHOR:        Marcos Mohedano
' CREATION DATE: 20-NOV-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 20-NOV-2013  MMG    Initial version.
'--------------------------------------------------------------------

Option Explicit On

#Region " Imports "

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common

#End Region

Public Class frm_play_session_close
  Inherits frm_base_edit

#Region " Constants "

#End Region 'Constants

#Region " Members "
  Dim m_account_holder_name As String
  Dim m_account_id As Integer
  Dim m_closing_reason As String

#End Region ' Members

#Region "Properties"
  Public ReadOnly Property ClosingReason() As String
    Get
      Return m_closing_reason
    End Get

  End Property

#End Region

#Region " Publics "

  ' PURPOSE: Constructor. 
  '
  '  PARAMS:
  '     - INPUT:
  '           - Account Id of the play session which will be closed
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call    
    ' m_account_id = AccountId

  End Sub

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal AccountId As Integer, ByVal HolderName As String)
    m_account_id = AccountId
    m_account_holder_name = HolderName

    Me.Display(True)

  End Sub ' ShowForEdit

#End Region

#Region " Overrides "

  ' PURPOSE: Process clicks on data grid (double-clicks) and select button
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Me.DialogResult = Windows.Forms.DialogResult.OK
    m_closing_reason = Me.txt_reason.Text.Trim()

    If ButtonId <> frm_base_edit.ENUM_BUTTON.BUTTON_OK Then
      Me.DialogResult = Windows.Forms.DialogResult.Cancel
      m_closing_reason = ""

    End If

    Me.Close()

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_FORM.FORM_PLAY_SESSION_CLOSE
  End Sub 'GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    Dim _header As String

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    ' Set account header
    _header = m_account_id.ToString

    If Not String.IsNullOrEmpty(m_account_holder_name) Then
      _header = _header & " - " & m_account_holder_name
    End If

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2993)
    Me.lbl_account_header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2994, _header)
    Me.lbl_close_session_description.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2995)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False

    'Customize button OK size
    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Type = uc_button.ENUM_BUTTON_TYPE.USER
    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Size = New Size(Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Width, Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Height + 10)

    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2996) 'Close session
    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False

  End Sub 'GUI_InitControls


  ' PURPOSE: Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.txt_reason

  End Sub ' GUI_SetInitialFocus

#End Region

#Region " Private Functions "

#End Region

#Region " Events "

  ' PURPOSE: Ignore [ENTER] key when editing comments
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    If e.KeyChar = Chr(Keys.Enter) And Me.txt_reason.ContainsFocus Then
      Return True
    End If

    Return MyBase.GUI_KeyPress(sender, e)

  End Function ' GUI_KeyPress

#End Region 'Events

End Class
