﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
'   MODULE NAME : frm_customer_dashboard_report_sel
'   DESCRIPTION : Customer dashboard Report
'        AUTHOR : Ervin Olvera
' CREATION DATE : 01-SEP-2017
'
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 01-SEP-2017  EOR    Initial version.
' 13-SEP-2017  EOR    Bug 29753: WIGOS-5068 Activity last six months - The screen is not applying any type of data validation
' 13-SEP-2017  EOR    Bug 29756: WIGOS-5070 Activity last six months - When the user do double click over some of the results the screen is autoclosed
' 14-SEP-2017  EOR    Bug 29773: WIGOS-5069 Activity last six months - The printed or excel file is displaying all the filters,included the not selected
'                     Bug 29776: WIGOS-5147 Activity last six months report - shows amount zero for "Bet" column
'                     Bug 29778: WIGOS-5137 Activity last six months - Specify the unit for the sections of "Visits","Games" and "Bet"
' 18-SEP-2017  EOR    Bug 29777: WIGOS-5071 Activity last six months - When the user appy some of the filter of the section "Games" the button search is disabled
' 03-NOV-2017  EOR    Bug 30507: WIGOS-6124 Small errors in Last 6 months activity report
' 16-NOV-2017  XGJ    WIGOS-6611: Activity last six months - When the user apply some of the filter of the section "Games" the button search is disabled
' 07-MAY-2018  AGS    Bug 32572: WIGOS-5326 Activity last six months - Time out message when the user try to search
' 28-MAY-2018  AGS    Bug 32799: WIGOS-12151 [Ticket #14464] Fallo- Reporte Actividad últimos 6 meses – Filtro tipo de juego - Versión V03.08.0001
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

#Region "Imports"
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports WSI.Common
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports System.Xml
Imports System.Globalization
#End Region ' Imports

Public Class frm_customer_dashboard_report_sel
  Inherits frm_base_sel

#Region " Structures"

  Private Structure ControlsGamesType
    Public Index As Int32
    Public GameType As Int32
    Public GameName As String
    Public ControlCheckBox As CheckBox
    Public ControlEntryFieldFrom As uc_entry_field
    Public ControlEntryFieldTo As uc_entry_field
  End Structure

#End Region

#Region " Constants"
  'Grid Setup
  Private Const GRID_HEADER_ROWS As Integer = 2
  Private Const GRID_COLUMNS As Integer = 27

  ' Grid Columns
  Private Const GRID_COLUMN_ID As Integer = 0
  Private Const GRID_COLUMN_ACCOUNT_ID As Integer = 1
  Private Const GRID_COLUMN_CARD As Integer = 2
  Private Const GRID_COLUMN_HOLDER_NAME As Integer = 3
  Private Const GRID_COLUMN_GENDER As Integer = 4
  Private Const GRID_COLUMN_ADDRESS As Integer = 5
  Private Const GRID_COLUMN_AGE As Integer = 6
  Private Const GRID_COLUMN_BIRTHDAY As Integer = 7
  Private Const GRID_COLUMN_AGE_RANGE As Integer = 8
  Private Const GRID_COLUMN_LEVEL As Integer = 9
  Private Const GRID_COLUMN_CREATED As Integer = 10
  Private Const GRID_COLUMN_LAST_RANGE As Integer = 11
  Private Const GRID_COLUMN_LAST_ACTIVITY As Integer = 12
  Private Const GRID_COLUMN_TOTAL As Integer = 13
  Private Const GRID_COLUMN_MONTH_01 As Integer = 14
  Private Const GRID_COLUMN_MONTH_02 As Integer = 15
  Private Const GRID_COLUMN_MONTH_03 As Integer = 16
  Private Const GRID_COLUMN_MONTH_04 As Integer = 17
  Private Const GRID_COLUMN_MONTH_05 As Integer = 18
  Private Const GRID_COLUMN_MONTH_06 As Integer = 19
  Private Const GRID_COLUMN_DAY_01 As Integer = 20
  Private Const GRID_COLUMN_DAY_02 As Integer = 21
  Private Const GRID_COLUMN_DAY_03 As Integer = 22
  Private Const GRID_COLUMN_DAY_04 As Integer = 23
  Private Const GRID_COLUMN_DAY_05 As Integer = 24
  Private Const GRID_COLUMN_DAY_06 As Integer = 25
  Private Const GRID_COLUMN_DAY_07 As Integer = 26
  Private Const GRID_COLUMN_INITIAL_FIXED_GAMES As Integer = 27

  'SQL Columns
  Private Const SQL_COLUMN_ACCOUNT_ID As Integer = 0
  Private Const SQL_COLUMN_CARD As Integer = 1
  Private Const SQL_COLUMN_HOLDER_NAME As Integer = 2
  Private Const SQL_COLUMN_GENDER As Integer = 3
  Private Const SQL_COLUMN_ADDRESS As Integer = 4
  Private Const SQL_COLUMN_AGE As Integer = 5
  Private Const SQL_COLUMN_MONTH_BIRTHDAY As Integer = 6
  Private Const SQL_COLUMN_DAY_BIRTHDAY As Integer = 7
  Private Const SQL_COLUMN_BIRTHDAY As Integer = 8
  Private Const SQL_COLUMN_AGE_RANGE As Integer = 9
  Private Const SQL_COLUMN_LEVEL As Integer = 10
  Private Const SQL_COLUMN_CREATED As Integer = 11
  Private Const SQL_COLUMN_LAST_RANGE As Integer = 12
  Private Const SQL_COLUMN_LAST_ACTIVITY As Integer = 13
  Private Const SQL_COLUMN_AC_TYPE As Integer = 14
  Private Const SQL_COLUMN_TOTAL As Integer = 15
  Private Const SQL_COLUMN_MONTH_01 As Integer = 16
  Private Const SQL_COLUMN_MONTH_02 As Integer = 17
  Private Const SQL_COLUMN_MONTH_03 As Integer = 18
  Private Const SQL_COLUMN_MONTH_04 As Integer = 19
  Private Const SQL_COLUMN_MONTH_05 As Integer = 20
  Private Const SQL_COLUMN_MONTH_06 As Integer = 21
  Private Const SQL_COLUMN_MONTH_07 As Integer = 22
  Private Const SQL_COLUMN_MONTH_08 As Integer = 23
  Private Const SQL_COLUMN_MONTH_09 As Integer = 24
  Private Const SQL_COLUMN_MONTH_10 As Integer = 25
  Private Const SQL_COLUMN_MONTH_11 As Integer = 26
  Private Const SQL_COLUMN_MONTH_12 As Integer = 27
  Private Const SQL_COLUMN_DAY_01 As Integer = 28
  Private Const SQL_COLUMN_DAY_02 As Integer = 29
  Private Const SQL_COLUMN_DAY_03 As Integer = 30
  Private Const SQL_COLUMN_DAY_04 As Integer = 31
  Private Const SQL_COLUMN_DAY_05 As Integer = 32
  Private Const SQL_COLUMN_DAY_06 As Integer = 33
  Private Const SQL_COLUMN_DAY_07 As Integer = 34
  Private Const SQL_COLUMN_INITIAL_FIXED_GAMES As Integer = 35

  ' Width Columns
  Private Const GRID_WIDTH_ID As Integer = 200
  Private Const GRID_WIDTH_DATE As Integer = 1650
  Private Const GRID_WIDTH_DATE_LONG As Integer = 2200
  Private Const GRID_WIDTH_ACCOUNT As Integer = 1150
  Private Const GRID_WIDTH_CARD_HOLDER_NAME As Integer = 2500
  Private Const GRID_WIDTH_CARD_TRACK_DATA As Integer = 2300
  Private Const GRID_WIDTH_HOLDER_GENDER As Integer = 1150
  Private Const GRID_WIDTH_ADDRESS As Integer = 2500
  Private Const GRID_WIDTH_AGE As Integer = 550
  Private Const GRID_WIDTH_BIRTHDAY As Integer = 1150
  Private Const GRID_WIDTH_RANGE As Integer = 1150
  Private Const GRID_WIDTH_AGE_RANGE As Integer = 1700
  Private Const GRID_WIDTH_LEVEL As Integer = 1500
  Private Const GRID_WIDTH_LAST_RANGE As Integer = 1150
  Private Const GRID_WIDTH_NUMBER As Integer = 1150
  Private Const GRID_WIDTH_AMOUNT As Integer = 1700

#End Region

#Region " Members"

  Private m_data_table As DataTable
  Private m_old_month As Date
  Private m_id_language As Int16
  Private m_current_month As Date
  Private m_cultureinfo As CultureInfo
  Private m_list_controls As IList(Of ControlsGamesType)

  Private m_account As String
  Private m_track_data As String
  Private m_holder As String
  Private m_gender As String
  Private m_level As String
  Private m_range_age As String
  Private m_birthdays As String
  Private m_date_creation As String
  Private m_visits As String
  Private m_months As String
  Private m_days As String
  Private m_game_type As String
  Private m_bets As String
#End Region

#Region " Public Functions"

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)
  End Sub ' ShowForEdit

#End Region

#Region " Overrides"

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    Call MyBase.GUI_InitControls()

    m_current_month = WGDB.Now
    m_current_month = New Date(m_current_month.Year, m_current_month.Month, 1, WSI.Common.Misc.TodayOpening().Hour, WSI.Common.Misc.TodayOpening().Minute, WSI.Common.Misc.TodayOpening().Second)

    m_old_month = WGDB.Now.AddMonths(-5)
    m_old_month = New Date(m_old_month.Year, m_old_month.Month, 1, WSI.Common.Misc.TodayOpening().Hour, WSI.Common.Misc.TodayOpening().Minute, WSI.Common.Misc.TodayOpening().Second)


    If GeneralParam.GetString("WigosGUI", "Language") = "es" Then
      m_cultureinfo = New System.Globalization.CultureInfo("es-ES")
      m_id_language = 10
    ElseIf GeneralParam.GetString("WigosGUI", "Language") = "fr" Then
      m_cultureinfo = New System.Globalization.CultureInfo("fr-FR")
      m_id_language = 12
    Else
      m_cultureinfo = New System.Globalization.CultureInfo("en-US")
      m_id_language = 9
    End If

    'Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    'Set form Name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8605)

    ' In TITO Mode set account filter not extended to hide virtual accounts
    If WSI.Common.TITO.Utils.IsTitoMode() Then
      Me.uc_account_search.InitExtendedQuery(False)
    End If

    ' Gender
    Me.gb_gender.Text = GLB_NLS_GUI_INVOICING.GetString(403)
    Me.chk_gender_male.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(521)
    Me.chk_gender_female.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(522)
    Me.chk_gender_unknown.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7773)

    ' Level
    Me.gb_level.Text = GLB_NLS_GUI_INVOICING.GetString(381)
    Me.chk_level_01.Text = GetCashierPlayerTrackingData("Level01.Name")
    If Me.chk_level_01.Text = "" Then
      Me.chk_level_01.Text = GLB_NLS_GUI_CONFIGURATION.GetString(289)
    End If
    Me.chk_level_02.Text = GetCashierPlayerTrackingData("Level02.Name")
    If Me.chk_level_02.Text = "" Then
      Me.chk_level_02.Text = GLB_NLS_GUI_CONFIGURATION.GetString(290)
    End If
    Me.chk_level_03.Text = GetCashierPlayerTrackingData("Level03.Name")
    If Me.chk_level_03.Text = "" Then
      Me.chk_level_03.Text = GLB_NLS_GUI_CONFIGURATION.GetString(291)
    End If
    Me.chk_level_04.Text = GetCashierPlayerTrackingData("Level04.Name")
    If Me.chk_level_04.Text = "" Then
      Me.chk_level_04.Text = GLB_NLS_GUI_CONFIGURATION.GetString(332)
    End If

    ' Creation Date
    Me.gb_date.Text = GLB_NLS_GUI_INVOICING.GetString(468)
    Me.dtp_from_creation.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_to_creation.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_from_creation.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    Me.dtp_to_creation.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    Me.dtp_from_creation.SetRange(New Date(2002, 1, 1), WGDB.Now)
    Me.dtp_to_creation.SetRange(New Date(2002, 1, 1), WGDB.Now)

    ' Age Range
    Me.chk_range_age.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8432)
    Me.opt_range.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2705)
    Me.opt_range_custom.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8602)
    Me.cmb_age_range.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8432)
    Me.ef_age_from.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2706)
    Me.ef_age_from.ShortcutsEnabled = False
    Me.ef_age_to.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2707)
    Me.ef_age_to.ShortcutsEnabled = False
    Me.ef_age_from.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_age_to.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Call ComboAgeRangeFill()

    'Birthday
    Me.chk_birthday.Text = GLB_NLS_GUI_INVOICING.GetString(405)
    Me.cmb_birthday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7113)
    Me.ef_birthday_day.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(706)
    Me.ef_birthday_day.ShortcutsEnabled = False
    Me.ef_birthday_day.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Call ComboMonthFill()

    'Visit
    Me.chk_visit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(542)
    Me.opt_last_visit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(543)
    Me.cmb_visit.Text = ""
    Call ComboVisitFill()
    Me.opt_last_visit_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2175)
    Me.dtp_from_visit.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_to_visit.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_from_visit.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    Me.dtp_to_visit.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    Me.dtp_from_visit.SetRange(m_old_month, WGDB.Now)
    Me.dtp_to_visit.SetRange(m_old_month, WGDB.Now)

    'Visit x Months
    Me.gb_visit_month.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8603)
    Me.lbl_description_month.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8688)
    Me.chk_visit_month_06.Text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(m_cultureinfo.DateTimeFormat.GetMonthName(m_current_month.Month))
    Me.chk_visit_month_05.Text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(m_cultureinfo.DateTimeFormat.GetMonthName(m_current_month.AddMonths(-1).Month))
    Me.chk_visit_month_04.Text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(m_cultureinfo.DateTimeFormat.GetMonthName(m_current_month.AddMonths(-2).Month))
    Me.chk_visit_month_03.Text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(m_cultureinfo.DateTimeFormat.GetMonthName(m_current_month.AddMonths(-3).Month))
    Me.chk_visit_month_02.Text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(m_cultureinfo.DateTimeFormat.GetMonthName(m_current_month.AddMonths(-4).Month))
    Me.chk_visit_month_01.Text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(m_cultureinfo.DateTimeFormat.GetMonthName(m_current_month.AddMonths(-5).Month))
    Me.ef_month_from_01.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_month_from_02.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_month_from_03.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_month_from_04.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_month_from_05.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_month_from_06.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_month_to_01.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_month_to_02.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_month_to_03.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_month_to_04.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_month_to_05.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_month_to_06.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_month_from_01.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.ef_month_from_02.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.ef_month_from_03.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.ef_month_from_04.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.ef_month_from_05.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.ef_month_from_06.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.ef_month_to_01.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.ef_month_to_02.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.ef_month_to_03.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.ef_month_to_04.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.ef_month_to_05.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.ef_month_to_06.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.ef_month_from_01.ShortcutsEnabled = False
    Me.ef_month_from_02.ShortcutsEnabled = False
    Me.ef_month_from_03.ShortcutsEnabled = False
    Me.ef_month_from_04.ShortcutsEnabled = False
    Me.ef_month_from_05.ShortcutsEnabled = False
    Me.ef_month_from_06.ShortcutsEnabled = False
    Me.ef_month_to_01.ShortcutsEnabled = False
    Me.ef_month_to_02.ShortcutsEnabled = False
    Me.ef_month_to_03.ShortcutsEnabled = False
    Me.ef_month_to_04.ShortcutsEnabled = False
    Me.ef_month_to_05.ShortcutsEnabled = False
    Me.ef_month_to_06.ShortcutsEnabled = False

    'Visit Week
    Me.gb_visit_week.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8604)
    Me.lbl_description_week.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8689)
    Me.chk_day_sunday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(307)
    Me.chk_day_monday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(301)
    Me.chk_day_thursday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(302)
    Me.chk_day_wednesday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(303)
    Me.chk_day_thuesday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(304)
    Me.chk_day_friday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(305)
    Me.chk_day_saturday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(306)
    Me.ef_from_sunday.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.ef_from_monday.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.ef_from_thursday.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.ef_from_wednesday.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.ef_from_thuesday.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.ef_from_friday.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.ef_from_saturday.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.ef_to_sunday.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.ef_to_monday.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.ef_to_thursday.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.ef_to_wednesday.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.ef_to_thuesday.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.ef_to_friday.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.ef_to_saturday.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.ef_from_sunday.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_from_monday.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_from_thursday.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_from_wednesday.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_from_thuesday.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_from_friday.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_from_saturday.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_to_sunday.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_to_monday.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_to_thursday.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_to_wednesday.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_to_thuesday.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_to_friday.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_to_saturday.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_from_sunday.ShortcutsEnabled = False
    Me.ef_from_monday.ShortcutsEnabled = False
    Me.ef_from_thursday.ShortcutsEnabled = False
    Me.ef_from_wednesday.ShortcutsEnabled = False
    Me.ef_from_thuesday.ShortcutsEnabled = False
    Me.ef_from_friday.ShortcutsEnabled = False
    Me.ef_from_saturday.ShortcutsEnabled = False
    Me.ef_to_sunday.ShortcutsEnabled = False
    Me.ef_to_monday.ShortcutsEnabled = False
    Me.ef_to_thursday.ShortcutsEnabled = False
    Me.ef_to_wednesday.ShortcutsEnabled = False
    Me.ef_to_thuesday.ShortcutsEnabled = False
    Me.ef_to_friday.ShortcutsEnabled = False
    Me.ef_to_saturday.ShortcutsEnabled = False

    Me.gb_games.Text = GLB_NLS_GUI_CONFIGURATION.GetString(427)
    Me.lbl_description_game.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8690)
    Call CreateControlsGames()

    Me.chk_bet.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5857)
    Me.lbl_description_bet.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8691)
    Me.ef_bet_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.ef_bet_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.ef_bet_from.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9, 2)
    Me.ef_bet_to.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9, 2)
    Me.ef_bet_from.ShortcutsEnabled = False
    Me.ef_bet_to.ShortcutsEnabled = False

    Call GUI_StyleSheet()

    Call SetDefaultValues()
  End Sub 'GUI_InitControls

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_FORM.FORM_LAST_SIX_MONTHS_REPORT
    Call MyBase.GUI_SetFormId()
  End Sub 'GUI_SetFormId

  ' PURPOSE: Call function to reset Filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub 'GUI_FilterReset

  ' PURPOSE: Process button actions in order to branch to a child screen
  '
  '  PARAMS:
  '     - INPUT:
  '           - ButtonId
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)
    MyBase.GUI_ButtonClick(ButtonId)
  End Sub ' GUI_ButtonClick

  Protected Overrides Function GUI_GetQueryType() As frm_base_sel.ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM
  End Function

  ' PURPOSE : Sets the values of a row after show in datagrid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _gender As String
    Dim _level As String
    Dim _count_visit As Integer
    Dim _index As Integer

    Call MyBase.GUI_SetupRow(RowIndex, DbRow)

    Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID).Value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_ACCOUNT_ID)), "", DbRow.Value(SQL_COLUMN_ACCOUNT_ID))
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD).Value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_CARD)), "", DbRow.Value(SQL_COLUMN_CARD))
    Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_NAME).Value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_HOLDER_NAME)), "", DbRow.Value(SQL_COLUMN_HOLDER_NAME))

    Select Case IIf(IsDBNull(DbRow.Value(SQL_COLUMN_GENDER)), 0, DbRow.Value(SQL_COLUMN_GENDER))
      Case 1
        _gender = GLB_NLS_GUI_INVOICING.GetString(413)
      Case 2
        _gender = GLB_NLS_GUI_INVOICING.GetString(414)
      Case Else
        _gender = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6897)
    End Select
    Me.Grid.Cell(RowIndex, GRID_COLUMN_GENDER).Value = _gender

    Me.Grid.Cell(RowIndex, GRID_COLUMN_ADDRESS).Value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_ADDRESS)), "", DbRow.Value(SQL_COLUMN_ADDRESS))

    If DbRow.Value(SQL_COLUMN_BIRTHDAY) Is DBNull.Value Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_BIRTHDAY).Value = ""
      Me.Grid.Cell(RowIndex, GRID_COLUMN_AGE_RANGE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6897)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_AGE).Value = "A"
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_BIRTHDAY).Value = CDate(DbRow.Value(SQL_COLUMN_BIRTHDAY)).ToString("dd-MMM")
      Me.Grid.Cell(RowIndex, GRID_COLUMN_AGE_RANGE).Value = DbRow.Value(SQL_COLUMN_AGE_RANGE)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_AGE).Value = DbRow.Value(SQL_COLUMN_AGE)
    End If


    Select Case IIf(IsDBNull(DbRow.Value(SQL_COLUMN_LEVEL)), 1, DbRow.Value(SQL_COLUMN_LEVEL))
      Case 1
        _level = chk_level_01.Text
      Case 2
        _level = chk_level_02.Text
      Case 3
        _level = chk_level_03.Text
      Case 4
        _level = chk_level_04.Text
      Case Else
        _level = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6897)
    End Select
    Me.Grid.Cell(RowIndex, GRID_COLUMN_LEVEL).Value = _level

    ' XGJ 16-NOV-2017
    If (IsDBNull(DbRow.Value(SQL_COLUMN_CREATED))) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CREATED).Value = ""
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CREATED).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_CREATED), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_LAST_RANGE).Value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_LAST_RANGE)), "", DbRow.Value(SQL_COLUMN_LAST_RANGE) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7310))

    If (IsDBNull(DbRow.Value(SQL_COLUMN_LAST_ACTIVITY))) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_LAST_ACTIVITY).Value = ""
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_LAST_ACTIVITY).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_LAST_ACTIVITY), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL).Value = DbRow.Value(SQL_COLUMN_TOTAL)

    For _i As Integer = 0 To 5
      _count_visit = 0

      Select Case m_old_month.AddMonths(_i).Month
        Case 1
          _count_visit = DbRow.Value(SQL_COLUMN_MONTH_01)
        Case 2
          _count_visit = DbRow.Value(SQL_COLUMN_MONTH_02)
        Case 3
          _count_visit = DbRow.Value(SQL_COLUMN_MONTH_03)
        Case 4
          _count_visit = DbRow.Value(SQL_COLUMN_MONTH_04)
        Case 5
          _count_visit = DbRow.Value(SQL_COLUMN_MONTH_05)
        Case 6
          _count_visit = DbRow.Value(SQL_COLUMN_MONTH_06)
        Case 7
          _count_visit = DbRow.Value(SQL_COLUMN_MONTH_07)
        Case 8
          _count_visit = DbRow.Value(SQL_COLUMN_MONTH_08)
        Case 9
          _count_visit = DbRow.Value(SQL_COLUMN_MONTH_09)
        Case 10
          _count_visit = DbRow.Value(SQL_COLUMN_MONTH_10)
        Case 11
          _count_visit = DbRow.Value(SQL_COLUMN_MONTH_11)
        Case 12
          _count_visit = DbRow.Value(SQL_COLUMN_MONTH_12)
      End Select

      Select Case _i
        Case 0
          Me.Grid.Cell(RowIndex, GRID_COLUMN_MONTH_01).Value = _count_visit
        Case 1
          Me.Grid.Cell(RowIndex, GRID_COLUMN_MONTH_02).Value = _count_visit
        Case 2
          Me.Grid.Cell(RowIndex, GRID_COLUMN_MONTH_03).Value = _count_visit
        Case 3
          Me.Grid.Cell(RowIndex, GRID_COLUMN_MONTH_04).Value = _count_visit
        Case 4
          Me.Grid.Cell(RowIndex, GRID_COLUMN_MONTH_05).Value = _count_visit
        Case 5
          Me.Grid.Cell(RowIndex, GRID_COLUMN_MONTH_06).Value = _count_visit
      End Select
    Next


    Me.Grid.Cell(RowIndex, GRID_COLUMN_DAY_01).Value = DbRow.Value(SQL_COLUMN_DAY_01)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DAY_02).Value = DbRow.Value(SQL_COLUMN_DAY_02)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DAY_03).Value = DbRow.Value(SQL_COLUMN_DAY_03)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DAY_04).Value = DbRow.Value(SQL_COLUMN_DAY_04)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DAY_05).Value = DbRow.Value(SQL_COLUMN_DAY_05)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DAY_06).Value = DbRow.Value(SQL_COLUMN_DAY_06)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DAY_07).Value = DbRow.Value(SQL_COLUMN_DAY_07)

    _index = 0
    For Each _game As ControlsGamesType In m_list_controls
      Me.Grid.Cell(RowIndex, GRID_COLUMN_INITIAL_FIXED_GAMES + _index).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_INITIAL_FIXED_GAMES + _index), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      _index += 1
    Next

    Me.Grid.Cell(RowIndex, GRID_COLUMN_INITIAL_FIXED_GAMES + m_list_controls.Count).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_INITIAL_FIXED_GAMES + m_list_controls.Count), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    Return True
  End Function 'GUI_SetupRow

  ' PURPOSE : Sets initial focus
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS:
  '     - none       
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.uc_account_search
  End Sub 'GUI_SetInitialFocus

  ' PURPOSE: Print totalizator row in the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    Call SetRowTotalGrid()
  End Sub ' GUI_AfterLastRow

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()
    m_account = "---"
    m_track_data = "---"
    m_holder = "---"
    m_gender = "---"
    m_level = "---"
    m_range_age = "---"
    m_birthdays = "---"
    m_date_creation = "---"
    m_visits = "---"
    m_months = "---"
    m_days = "---"
    m_game_type = "---"
    m_bets = "---"

    m_account = Me.uc_account_search.Account()
    m_track_data = Me.uc_account_search.TrackData()
    m_holder = Me.uc_account_search.Holder()

    If chk_gender_male.Checked Then
      m_gender += ", " & chk_gender_male.Text
    End If
    If chk_gender_female.Checked Then
      m_gender += ", " & chk_gender_female.Text
    End If
    If chk_gender_unknown.Checked Then
      m_gender += ", " & chk_gender_unknown.Text
    End If
    If m_gender <> "---" Then
      m_gender = Mid(m_gender, 3, m_gender.Length)
    End If

    If chk_level_01.Checked Then
      m_level += ", " & chk_level_01.Text
    End If
    If chk_level_02.Checked Then
      m_level += ", " & chk_level_02.Text
    End If
    If chk_level_03.Checked Then
      m_level += ", " & chk_level_04.Text
    End If
    If chk_level_04.Checked Then
      m_level += ", " & chk_level_04.Text
    End If
    If m_level <> "---" Then
      m_level = Mid(m_level, 3, m_level.Length)
    End If

    If chk_range_age.Checked Then
      If opt_range.Checked Then
        m_range_age = cmb_age_range.TextValue
      End If
      If opt_range_custom.Checked Then
        m_range_age = GLB_NLS_GUI_INVOICING.GetString(202) & ": " & IIf(String.IsNullOrEmpty(ef_age_from.TextValue), "---", ef_age_from.TextValue)
        m_range_age += " " & GLB_NLS_GUI_INVOICING.GetString(203) & ": " & IIf(String.IsNullOrEmpty(ef_age_to.TextValue), "---", ef_age_to.TextValue)
      End If
    End If

    If chk_birthday.Checked Then
      m_birthdays = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7188) & ": " & IIf(String.IsNullOrEmpty(ef_birthday_day.TextValue), "---", ef_birthday_day.TextValue) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7113) & ": " & cmb_birthday.TextValue
    End If

    If dtp_from_creation.Checked Then
      m_date_creation += " " & GLB_NLS_GUI_INVOICING.GetString(202) & ": " & GUI_FormatDate(dtp_from_creation.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    End If
    If dtp_to_creation.Checked Then
      m_date_creation += " " & GLB_NLS_GUI_INVOICING.GetString(203) & ": " & GUI_FormatDate(dtp_to_creation.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    End If
    If m_date_creation <> "---" Then
      m_date_creation = Mid(m_date_creation, 4, m_date_creation.Length)
    End If

    If chk_visit.Checked Then
      If opt_last_visit.Checked Then
        m_visits = cmb_visit.TextValue
      End If
      If opt_last_visit_date.Checked Then
        m_visits = GLB_NLS_GUI_INVOICING.GetString(202) & ": " & GUI_FormatDate(dtp_from_visit.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE) & " " & GLB_NLS_GUI_INVOICING.GetString(203) & ": " & GUI_FormatDate(dtp_to_visit.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
      End If
    End If

    If chk_visit_month_01.Checked Then
      m_months += " " & chk_visit_month_01.Text & " "
      m_months += " " & GLB_NLS_GUI_INVOICING.GetString(202) & ": " & IIf(String.IsNullOrEmpty(ef_month_from_01.TextValue), "---", ef_month_from_01.TextValue)
      m_months += " " & GLB_NLS_GUI_INVOICING.GetString(203) & ": " & IIf(String.IsNullOrEmpty(ef_month_to_01.TextValue), "---", ef_month_to_01.TextValue)
    End If
    If chk_visit_month_02.Checked Then
      m_months += " " & chk_visit_month_02.Text & " "
      m_months += " " & GLB_NLS_GUI_INVOICING.GetString(202) & ": " & IIf(String.IsNullOrEmpty(ef_month_from_02.TextValue), "---", ef_month_from_02.TextValue)
      m_months += " " & GLB_NLS_GUI_INVOICING.GetString(203) & ": " & IIf(String.IsNullOrEmpty(ef_month_to_02.TextValue), "---", ef_month_to_02.TextValue)
    End If
    If chk_visit_month_03.Checked Then
      m_months += " " & chk_visit_month_03.Text & " "
      m_months += " " & GLB_NLS_GUI_INVOICING.GetString(202) & ": " & IIf(String.IsNullOrEmpty(ef_month_from_03.TextValue), "---", ef_month_from_03.TextValue)
      m_months += " " & GLB_NLS_GUI_INVOICING.GetString(203) & ": " & IIf(String.IsNullOrEmpty(ef_month_to_03.TextValue), "---", ef_month_to_03.TextValue)
    End If
    If chk_visit_month_04.Checked Then
      m_months += " " & chk_visit_month_04.Text & " "
      m_months += " " & GLB_NLS_GUI_INVOICING.GetString(202) & ": " & IIf(String.IsNullOrEmpty(ef_month_from_04.TextValue), "---", ef_month_from_04.TextValue)
      m_months += " " & GLB_NLS_GUI_INVOICING.GetString(203) & ": " & IIf(String.IsNullOrEmpty(ef_month_to_04.TextValue), "---", ef_month_to_04.TextValue)
    End If
    If chk_visit_month_05.Checked Then
      m_months += " " & chk_visit_month_05.Text & " "
      m_months += " " & GLB_NLS_GUI_INVOICING.GetString(202) & ": " & IIf(String.IsNullOrEmpty(ef_month_from_05.TextValue), "---", ef_month_from_05.TextValue)
      m_months += " " & GLB_NLS_GUI_INVOICING.GetString(203) & ": " & IIf(String.IsNullOrEmpty(ef_month_to_05.TextValue), "---", ef_month_to_05.TextValue)
    End If
    If chk_visit_month_06.Checked Then
      m_months += " " & chk_visit_month_06.Text & " "
      m_months += " " & GLB_NLS_GUI_INVOICING.GetString(202) & ": " & IIf(String.IsNullOrEmpty(ef_month_from_06.TextValue), "---", ef_month_from_06.TextValue)
      m_months += " " & GLB_NLS_GUI_INVOICING.GetString(203) & ": " & IIf(String.IsNullOrEmpty(ef_month_to_06.TextValue), "---", ef_month_to_06.TextValue)
    End If
    If m_months <> "---" Then
      m_months = Mid(m_months, 4, m_months.Length)
    End If

    If chk_day_sunday.Checked Then
      m_days += " " & chk_day_sunday.Text
      m_days += " " & GLB_NLS_GUI_INVOICING.GetString(202) & ": " & IIf(String.IsNullOrEmpty(ef_from_sunday.TextValue), "---", ef_from_sunday.TextValue)
      m_days += " " & GLB_NLS_GUI_INVOICING.GetString(203) & ": " & IIf(String.IsNullOrEmpty(ef_to_sunday.TextValue), "---", ef_to_sunday.TextValue)
    End If
    If chk_day_monday.Checked Then
      m_days += " " & chk_day_monday.Text
      m_days += " " & GLB_NLS_GUI_INVOICING.GetString(202) & ": " & IIf(String.IsNullOrEmpty(ef_from_monday.TextValue), "---", ef_from_monday.TextValue)
      m_days += " " & GLB_NLS_GUI_INVOICING.GetString(203) & ": " & IIf(String.IsNullOrEmpty(ef_to_monday.TextValue), "---", ef_to_monday.TextValue)
    End If
    If chk_day_thuesday.Checked Then
      m_days += " " & chk_day_thuesday.Text
      m_days += " " & GLB_NLS_GUI_INVOICING.GetString(202) & ": " & IIf(String.IsNullOrEmpty(ef_from_thuesday.TextValue), "---", ef_from_thuesday.TextValue)
      m_days += " " & GLB_NLS_GUI_INVOICING.GetString(203) & ": " & IIf(String.IsNullOrEmpty(ef_to_thuesday.TextValue), "---", ef_to_thuesday.TextValue)
    End If
    If chk_day_wednesday.Checked Then
      m_days += " " & chk_day_wednesday.Text
      m_days += " " & GLB_NLS_GUI_INVOICING.GetString(202) & ": " & IIf(String.IsNullOrEmpty(ef_from_wednesday.TextValue), "---", ef_from_wednesday.TextValue)
      m_days += " " & GLB_NLS_GUI_INVOICING.GetString(203) & ": " & IIf(String.IsNullOrEmpty(ef_to_wednesday.TextValue), "---", ef_to_wednesday.TextValue)
    End If
    If chk_day_thursday.Checked Then
      m_days += " " & chk_day_thursday.Text
      m_days += " " & GLB_NLS_GUI_INVOICING.GetString(202) & ": " & IIf(String.IsNullOrEmpty(ef_from_thursday.TextValue), "---", ef_from_thursday.TextValue)
      m_days += " " & GLB_NLS_GUI_INVOICING.GetString(203) & ": " & IIf(String.IsNullOrEmpty(ef_to_thursday.TextValue), "---", ef_to_thursday.TextValue)
    End If
    If chk_day_friday.Checked Then
      m_days += " " & chk_day_friday.Text
      m_days += " " & GLB_NLS_GUI_INVOICING.GetString(202) & ": " & IIf(String.IsNullOrEmpty(ef_from_friday.TextValue), "---", ef_from_friday.TextValue)
      m_days += " " & GLB_NLS_GUI_INVOICING.GetString(203) & ": " & IIf(String.IsNullOrEmpty(ef_to_friday.TextValue), "---", ef_to_friday.TextValue)
    End If
    If chk_day_sunday.Checked Then
      m_days += " " & chk_day_sunday.Text
      m_days += " " & GLB_NLS_GUI_INVOICING.GetString(202) & ": " & IIf(String.IsNullOrEmpty(ef_from_sunday.TextValue), "---", ef_from_sunday.TextValue)
      m_days += " " & GLB_NLS_GUI_INVOICING.GetString(203) & ": " & IIf(String.IsNullOrEmpty(ef_to_sunday.TextValue), "---", ef_to_sunday.TextValue)
    End If
    If m_days <> "---" Then
      m_days = Mid(m_days, 4, m_days.Length)
    End If

    For Each _game As ControlsGamesType In m_list_controls
      If _game.ControlCheckBox.Checked Then
        m_game_type += " " & _game.GameName
        m_game_type += " " & GLB_NLS_GUI_INVOICING.GetString(202) & ": " & IIf(String.IsNullOrEmpty(_game.ControlEntryFieldFrom.TextValue), "---", _game.ControlEntryFieldFrom.TextValue)
        m_game_type += " " & GLB_NLS_GUI_INVOICING.GetString(203) & ": " & IIf(String.IsNullOrEmpty(_game.ControlEntryFieldTo.TextValue), "---", _game.ControlEntryFieldTo.TextValue)
      End If
    Next
    If m_game_type <> "---" Then
      m_game_type = Mid(m_game_type, 4, m_game_type.Length)
    End If

    If chk_bet.Checked Then
      m_bets += " " & GLB_NLS_GUI_INVOICING.GetString(202) & ": " & IIf(String.IsNullOrEmpty(ef_bet_from.TextValue), "---", ef_bet_from.TextValue)
      m_bets += " " & GLB_NLS_GUI_INVOICING.GetString(203) & ": " & IIf(String.IsNullOrEmpty(ef_bet_to.TextValue), "---", ef_bet_to.TextValue)
    End If
    If m_bets <> "---" Then
      m_bets = Mid(m_bets, 4, m_bets.Length)
    End If

  End Sub 'GUI_ReportUpdateFilters

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), m_account)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_track_data)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), m_holder)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(403), m_gender)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(381), m_level)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(8432), m_range_age)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(405), m_birthdays)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(468), m_date_creation)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(543), m_visits)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(8603), m_months)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(8604), m_days)
    PrintData.SetFilter(GLB_NLS_GUI_CONFIGURATION.GetString(427), m_game_type)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5857), m_bets)
  End Sub ' GUI_ReportFilter

  Protected Overrides Function GUI_FilterCheck() As Boolean
    If Me.dtp_from_creation.Checked And Me.dtp_to_creation.Checked Then
      If Me.dtp_from_creation.Value > Me.dtp_to_creation.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8680), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Me.dtp_to_creation.Focus()
        Return False
      End If
    End If

    If chk_range_age.Checked And opt_range_custom.Checked Then
      If Val(IIf(String.IsNullOrEmpty(ef_age_from.TextValue), 0, ef_age_from.TextValue)) > Val(IIf(String.IsNullOrEmpty(ef_age_to.TextValue), 100, ef_age_to.TextValue)) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8681), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Me.ef_age_to.Focus()
        Return False
      End If
    End If

    If chk_visit.Checked And opt_last_visit_date.Checked Then
      If Me.dtp_from_visit.Value > Me.dtp_to_visit.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8682), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Me.dtp_to_visit.Focus()
        Return False
      End If
    End If

    If chk_visit_month_01.Checked Then
      If Val(IIf(String.IsNullOrEmpty(ef_month_from_01.TextValue), 0, ef_month_from_01.TextValue)) > Val(IIf(String.IsNullOrEmpty(ef_month_to_01.TextValue), 100, ef_month_to_01.TextValue)) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8684), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, chk_visit_month_01.Text)
        Me.ef_month_to_01.Focus()
        Return False
      End If
    End If

    If chk_visit_month_02.Checked Then
      If Val(IIf(String.IsNullOrEmpty(ef_month_from_02.TextValue), 0, ef_month_from_02.TextValue)) > Val(IIf(String.IsNullOrEmpty(ef_month_to_02.TextValue), 100, ef_month_to_02.TextValue)) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8684), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, chk_visit_month_02.Text)
        Me.ef_month_to_02.Focus()
        Return False
      End If
    End If

    If chk_visit_month_03.Checked Then
      If Val(IIf(String.IsNullOrEmpty(ef_month_from_03.TextValue), 0, ef_month_from_03.TextValue)) > Val(IIf(String.IsNullOrEmpty(ef_month_to_03.TextValue), 100, ef_month_to_03.TextValue)) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8684), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, chk_visit_month_03.Text)
        Me.ef_month_to_03.Focus()
        Return False
      End If
    End If

    If chk_visit_month_04.Checked Then
      If Val(IIf(String.IsNullOrEmpty(ef_month_from_04.TextValue), 0, ef_month_from_04.TextValue)) > Val(IIf(String.IsNullOrEmpty(ef_month_to_04.TextValue), 100, ef_month_to_04.TextValue)) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8684), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, chk_visit_month_04.Text)
        Me.ef_month_to_04.Focus()
        Return False
      End If
    End If

    If chk_visit_month_05.Checked Then
      If Val(IIf(String.IsNullOrEmpty(ef_month_from_05.TextValue), 0, ef_month_from_05.TextValue)) > Val(IIf(String.IsNullOrEmpty(ef_month_to_05.TextValue), 100, ef_month_to_05.TextValue)) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8684), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, chk_visit_month_05.Text)
        Me.ef_month_to_05.Focus()
        Return False
      End If
    End If

    If chk_visit_month_06.Checked Then
      If Val(IIf(String.IsNullOrEmpty(ef_month_from_06.TextValue), 0, ef_month_from_06.TextValue)) > Val(IIf(String.IsNullOrEmpty(ef_month_to_06.TextValue), 100, ef_month_to_06.TextValue)) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8684), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, chk_visit_month_06.Text)
        Me.ef_month_to_06.Focus()
        Return False
      End If
    End If

    If chk_day_sunday.Checked Then
      If Val(IIf(String.IsNullOrEmpty(ef_from_sunday.TextValue), 1, ef_from_sunday.TextValue)) > Val(IIf(String.IsNullOrEmpty(ef_to_sunday.TextValue), 100, ef_to_sunday.TextValue)) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8685), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, chk_day_sunday.Text)
        Me.ef_to_sunday.Focus()
        Return False
      End If
    End If

    If chk_day_monday.Checked Then
      If Val(IIf(String.IsNullOrEmpty(ef_from_monday.TextValue), 1, ef_from_monday.TextValue)) > Val(IIf(String.IsNullOrEmpty(ef_to_monday.TextValue), 100, ef_to_monday.TextValue)) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8685), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, chk_day_monday.Text)
        Me.ef_to_monday.Focus()
        Return False
      End If
    End If

    If chk_day_thuesday.Checked Then
      If Val(IIf(String.IsNullOrEmpty(ef_from_thuesday.TextValue), 1, ef_from_thuesday.TextValue)) > Val(IIf(String.IsNullOrEmpty(ef_to_thuesday.TextValue), 100, ef_to_thuesday.TextValue)) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8685), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, chk_day_thuesday.Text)
        Me.ef_to_thuesday.Focus()
        Return False
      End If
    End If

    If chk_day_wednesday.Checked Then
      If Val(IIf(String.IsNullOrEmpty(ef_from_wednesday.TextValue), 1, ef_from_wednesday.TextValue)) > Val(IIf(String.IsNullOrEmpty(ef_to_wednesday.TextValue), 100, ef_to_wednesday.TextValue)) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8685), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, chk_day_wednesday.Text)
        Me.ef_to_wednesday.Focus()
        Return False
      End If
    End If

    If chk_day_thursday.Checked Then
      If Val(IIf(String.IsNullOrEmpty(ef_from_thursday.TextValue), 1, ef_from_thursday.TextValue)) > Val(IIf(String.IsNullOrEmpty(ef_to_thursday.TextValue), 100, ef_to_thursday.TextValue)) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8685), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, chk_day_thursday.Text)
        Me.ef_to_thursday.Focus()
        Return False
      End If
    End If

    If chk_day_friday.Checked Then
      If Val(IIf(String.IsNullOrEmpty(ef_from_friday.TextValue), 1, ef_from_friday.TextValue)) > Val(IIf(String.IsNullOrEmpty(ef_to_friday.TextValue), 100, ef_to_friday.TextValue)) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8685), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, chk_day_friday.Text)
        Me.ef_to_friday.Focus()
        Return False
      End If
    End If

    If chk_day_saturday.Checked Then
      If Val(IIf(String.IsNullOrEmpty(ef_from_saturday.TextValue), 1, ef_from_saturday.TextValue)) > Val(IIf(String.IsNullOrEmpty(ef_to_saturday.TextValue), 100, ef_to_saturday.TextValue)) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8685), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, chk_day_saturday.Text)
        Me.ef_to_saturday.Focus()
        Return False
      End If
    End If

    For Each _game As ControlsGamesType In m_list_controls
      If _game.ControlCheckBox.Checked Then
        If Val(IIf(String.IsNullOrEmpty(_game.ControlEntryFieldFrom.TextValue), 1, _game.ControlEntryFieldFrom.TextValue)) > Val(IIf(String.IsNullOrEmpty(_game.ControlEntryFieldTo.TextValue), 1000000000, _game.ControlEntryFieldTo.TextValue)) Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8686), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _game.GameName)
          _game.ControlEntryFieldTo.Focus()
          Return False
        End If
      End If
    Next

    If chk_bet.Checked Then
      If Val(IIf(String.IsNullOrEmpty(ef_bet_from.TextValue), 1, ef_bet_from.TextValue)) > Val(IIf(String.IsNullOrEmpty(ef_bet_to.TextValue), 1000000000, ef_bet_to.TextValue)) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8687), ENUM_MB_TYPE.MB_TYPE_WARNING)
        _ef_bet_to.Focus()
        Return False
      End If
    End If

    Return True
  End Function

  Protected Overrides Sub GUI_ExecuteQueryCustom()
    Dim _sql_command As SqlCommand = New SqlCommand()
    Dim db_row As CLASS_DB_ROW
    Dim _query_as_datatable As DataTable
    Dim _row As DataRow
    Dim _rows As DataRow()
    Dim idx_row As Integer

    _sql_command.CommandText = GetSqlQuery()
    _sql_command.CommandType = CommandType.Text
    _sql_command.Parameters.Add("@pDateInitial", SqlDbType.DateTime).Value = m_old_month
    _sql_command.Parameters.Add("@pDateFinal", SqlDbType.DateTime).Value = WSI.Common.Misc.TodayOpening().AddDays(1)

    m_data_table = New DataTable

    _query_as_datatable = GUI_GetTableUsingCommand(_sql_command, Integer.MaxValue)

    m_data_table = _query_as_datatable.Clone()

    If _query_as_datatable.Rows.Count > 0 Then

      _rows = ApplyFilters(_query_as_datatable)

      For Each _row In _rows
        Try
          db_row = New CLASS_DB_ROW(_row)

          Me.Grid.AddRow()
          idx_row = Me.Grid.NumRows - 1

          If Not GUI_SetupRow(idx_row, db_row) Then
            Me.Grid.DeleteRowFast(idx_row)
          Else
            m_data_table.ImportRow(_row)
          End If
        Catch ex As OutOfMemoryException
          Throw
        Catch exception As Exception
          Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
          Call Trace.WriteLine(exception.ToString())
          Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                                "frm_activity_tables", _
                                "GUI_SetupRow", _
                                exception.Message)
          Exit For
        End Try

        db_row = Nothing

        If Not GUI_DoEvents(Me.Grid) Then
          Exit Sub
        End If

      Next

      If _rows.Length > 0 Then
        Call GUI_AfterLastRow()
      End If
    End If
  End Sub

  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(ExcelData)

    ExcelData.SetColumnFormat(6, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TRACKDATA)

  End Sub ' GUI_ReportParams

#End Region

#Region " Private Functions "

  Private Sub ComboMonthFill()
    Dim _i As Integer

    Me.cmb_birthday.Add(_i, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353))

    For Each _month As String In m_cultureinfo.DateTimeFormat.MonthNames
      If Not String.IsNullOrEmpty(_month) Then
        _i += 1
        Me.cmb_birthday.Add(_i, CultureInfo.CurrentCulture.TextInfo.ToTitleCase(_month))
      End If
    Next

  End Sub

  Private Sub ComboAgeRangeFill()
    Me.cmb_age_range.Add(1, "18-24")
    Me.cmb_age_range.Add(2, "25-34")
    Me.cmb_age_range.Add(3, "35-44")
    Me.cmb_age_range.Add(4, "45-54")
    Me.cmb_age_range.Add(5, "55-64")
    Me.cmb_age_range.Add(6, "65-74")
    Me.cmb_age_range.Add(7, "75-84")
    Me.cmb_age_range.Add(8, "+ 85")
  End Sub

  Private Sub ComboVisitFill()
    Me.cmb_visit.Add(1, "< 30 " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7310))
    Me.cmb_visit.Add(2, "< 60 " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7310))
    Me.cmb_visit.Add(3, "< 90 " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7310))
    Me.cmb_visit.Add(4, "< 120 " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7310))
    Me.cmb_visit.Add(5, "< 150 " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7310))
    Me.cmb_visit.Add(6, "< 180 " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7310))
  End Sub

  Private Sub CreateControlsGames()
    Dim _sql As StringBuilder
    Dim _x_check As Integer
    Dim _y_check As Integer
    Dim _x_ef_from As Integer
    Dim _y_ef_from As Integer
    Dim _x_ef_to As Integer
    Dim _y_ef_to As Integer
    Dim table As DataTable
    Dim _ctrl_check_box As CheckBox
    Dim _ctrl_uc_entry_field As uc_entry_field
    Dim _location As Point
    Dim _structure As ControlsGamesType
    Dim _index As Integer
    Dim _tab_index As Integer


    _sql = New StringBuilder()
    m_list_controls = New List(Of ControlsGamesType)

    _sql.AppendLine(" SELECT DISTINCT GT_GAME_TYPE, GT_NAME ")
    _sql.AppendLine(" FROM GAME_TYPES  ")
    _sql.AppendLine(" WHERE GT_LANGUAGE_ID = " & m_id_language.ToString())
    _sql.AppendLine(" ORDER BY GT_NAME ")

    table = GUI_GetTableUsingSQL(_sql.ToString(), Integer.MaxValue)

    If table Is Nothing OrElse table.Rows.Count = 0 Then
      gb_games.Visible = False
    Else
      _x_check = 10
      _y_check = 8

      _x_ef_from = 86
      _y_ef_from = 3

      _x_ef_to = 220
      _y_ef_to = 3

      _index = 0
      _tab_index = 69

      For Each _dr As DataRow In table.Rows
        _structure = New ControlsGamesType()

        _structure.Index = _index
        _structure.GameType = CInt(_dr(0))
        _structure.GameName = _dr(1).ToString()

        _tab_index += 1
        _location = New Point(_x_check, _y_check)
        _ctrl_check_box = New CheckBox()
        _ctrl_check_box.Name = "chk_game_" & _index.ToString()
        _ctrl_check_box.TabIndex = _tab_index
        _ctrl_check_box.UseVisualStyleBackColor = True
        _ctrl_check_box.Location = _location
        _ctrl_check_box.Size = New Size(116, 17)
        _ctrl_check_box.Text = _dr(1).ToString()
        _ctrl_check_box.Checked = True
        AddHandler _ctrl_check_box.CheckedChanged, AddressOf chk_games_CheckedChanged
        pnl_games.Controls.Add(_ctrl_check_box)
        _structure.ControlCheckBox = _ctrl_check_box

        _tab_index += 1
        _location = New Point(_x_ef_from, _y_ef_from)
        _ctrl_uc_entry_field = New uc_entry_field()
        _ctrl_uc_entry_field.Name = "ef_game_from_" & _index.ToString()
        _ctrl_uc_entry_field.IsReadOnly = False
        _ctrl_uc_entry_field.SufixText = "Sufix Text"
        _ctrl_uc_entry_field.SufixTextVisible = True
        _ctrl_uc_entry_field.TabIndex = _tab_index
        _ctrl_uc_entry_field.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        _ctrl_uc_entry_field.Location = _location
        _ctrl_uc_entry_field.Size = New Size(179, 25)
        _ctrl_uc_entry_field.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9, 2)
        _ctrl_uc_entry_field.Text = GLB_NLS_GUI_INVOICING.GetString(202)
        _ctrl_uc_entry_field.ShortcutsEnabled = False
        pnl_games.Controls.Add(_ctrl_uc_entry_field)
        _structure.ControlEntryFieldFrom = _ctrl_uc_entry_field

        _tab_index += 1
        _location = New Point(_x_ef_to, _y_ef_to)
        _ctrl_uc_entry_field = New uc_entry_field()
        _ctrl_uc_entry_field.Name = "ef_game_to_" & _index.ToString()
        _ctrl_uc_entry_field.IsReadOnly = False
        _ctrl_uc_entry_field.SufixText = "Sufix Text"
        _ctrl_uc_entry_field.SufixTextVisible = True
        _ctrl_uc_entry_field.TabIndex = _tab_index
        _ctrl_uc_entry_field.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        _ctrl_uc_entry_field.Location = _location
        _ctrl_uc_entry_field.Size = New System.Drawing.Size(179, 25)
        _ctrl_uc_entry_field.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9, 2)
        _ctrl_uc_entry_field.Text = GLB_NLS_GUI_INVOICING.GetString(203)
        _ctrl_uc_entry_field.ShortcutsEnabled = False
        pnl_games.Controls.Add(_ctrl_uc_entry_field)
        _structure.ControlEntryFieldTo = _ctrl_uc_entry_field

        _y_check += 24
        _y_ef_from += 24
        _y_ef_to += 24

        m_list_controls.Insert(_index, _structure)
        _index += 1
      Next
    End If
  End Sub

  Private Sub SetDefaultValues()
    uc_account_search.Clear()

    chk_gender_male.Checked = False
    chk_gender_female.Checked = False
    chk_gender_unknown.Checked = False

    chk_level_01.Checked = False
    chk_level_02.Checked = False
    chk_level_03.Checked = False
    chk_level_04.Checked = False

    dtp_from_creation.Value = WSI.Common.Misc.TodayOpening().AddDays(-1)
    dtp_from_creation.Checked = False
    dtp_to_creation.Value = WSI.Common.Misc.TodayOpening()
    dtp_to_creation.Checked = False

    chk_birthday.Checked = False
    ef_birthday_day.TextValue = ""
    cmb_birthday.SelectedIndex = 0

    opt_range.Checked = True
    cmb_age_range.SelectedIndex = 0
    ef_age_from.TextValue = ""
    ef_age_to.TextValue = ""
    chk_range_age.Checked = False

    cmb_visit.SelectedIndex = 0
    opt_last_visit.Checked = True
    dtp_from_visit.Value = WSI.Common.Misc.TodayOpening().AddDays(-1)
    dtp_to_visit.Value = WSI.Common.Misc.TodayOpening()
    chk_visit.Checked = False

    ef_month_from_01.TextValue = ""
    ef_month_from_02.TextValue = ""
    ef_month_from_03.TextValue = ""
    ef_month_from_04.TextValue = ""
    ef_month_from_05.TextValue = ""
    ef_month_from_06.TextValue = ""
    ef_month_to_01.TextValue = ""
    ef_month_to_02.TextValue = ""
    ef_month_to_03.TextValue = ""
    ef_month_to_04.TextValue = ""
    ef_month_to_05.TextValue = ""
    ef_month_to_06.TextValue = ""
    chk_visit_month_01.Checked = False
    chk_visit_month_02.Checked = False
    chk_visit_month_03.Checked = False
    chk_visit_month_04.Checked = False
    chk_visit_month_05.Checked = False
    chk_visit_month_06.Checked = False
    ef_from_sunday.TextValue = ""
    ef_to_sunday.TextValue = ""
    ef_from_monday.TextValue = ""
    ef_to_monday.TextValue = ""
    ef_from_thursday.TextValue = ""
    ef_to_thursday.TextValue = ""
    ef_from_wednesday.TextValue = ""
    ef_to_wednesday.TextValue = ""
    ef_from_thuesday.TextValue = ""
    ef_to_thuesday.TextValue = ""
    ef_from_friday.TextValue = ""
    ef_to_friday.TextValue = ""
    ef_from_saturday.TextValue = ""
    ef_to_saturday.TextValue = ""
    chk_day_sunday.Checked = False
    chk_day_monday.Checked = False
    chk_day_thursday.Checked = False
    chk_day_wednesday.Checked = False
    chk_day_thuesday.Checked = False
    chk_day_friday.Checked = False
    chk_day_saturday.Checked = False

    For Each _control_structure As ControlsGamesType In m_list_controls
      _control_structure.ControlEntryFieldFrom.TextValue = ""
      _control_structure.ControlEntryFieldTo.TextValue = ""
      _control_structure.ControlCheckBox.Checked = False
    Next

    chk_bet.Checked = False
    ef_bet_from.TextValue = ""
    ef_bet_to.TextValue = ""
  End Sub

  Private Sub GUI_StyleSheet()
    With Me.Grid
      ' Initialize Grid
      Call .Init(GRID_COLUMNS + m_list_controls.Count + 1, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' GRID INDEX
      .Column(GRID_COLUMN_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_ID).Width = GRID_WIDTH_ID
      .Column(GRID_COLUMN_ID).HighLightWhenSelected = False
      .Column(GRID_COLUMN_ID).IsColumnPrintable = False

      ' ACCOUNTS
      ' ACCOUNT
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COLUMN_ACCOUNT_ID).Width = GRID_WIDTH_ACCOUNT
      .Column(GRID_COLUMN_ACCOUNT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' CARD
      .Column(GRID_COLUMN_CARD).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_CARD).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(212)
      .Column(GRID_COLUMN_CARD).Width = GRID_WIDTH_CARD_TRACK_DATA
      .Column(GRID_COLUMN_CARD).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' NAME
      .Column(GRID_COLUMN_HOLDER_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_HOLDER_NAME).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(235)
      .Column(GRID_COLUMN_HOLDER_NAME).Width = GRID_WIDTH_CARD_HOLDER_NAME
      .Column(GRID_COLUMN_HOLDER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' GENDER
      .Column(GRID_COLUMN_GENDER).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_GENDER).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(403)
      .Column(GRID_COLUMN_GENDER).Width = GRID_WIDTH_HOLDER_GENDER
      .Column(GRID_COLUMN_GENDER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' ADDRESS
      .Column(GRID_COLUMN_ADDRESS).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_ADDRESS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7629)
      .Column(GRID_COLUMN_ADDRESS).Width = GRID_WIDTH_ADDRESS
      .Column(GRID_COLUMN_ADDRESS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' AGE
      .Column(GRID_COLUMN_AGE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_AGE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8432)
      .Column(GRID_COLUMN_AGE).Width = GRID_WIDTH_AGE
      .Column(GRID_COLUMN_AGE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' BIRTHDAY
      .Column(GRID_COLUMN_BIRTHDAY).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_BIRTHDAY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1651)
      .Column(GRID_COLUMN_BIRTHDAY).Width = GRID_WIDTH_BIRTHDAY
      .Column(GRID_COLUMN_BIRTHDAY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' AGE_RANGE
      .Column(GRID_COLUMN_AGE_RANGE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_AGE_RANGE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2705)
      .Column(GRID_COLUMN_AGE_RANGE).Width = GRID_WIDTH_AGE_RANGE
      .Column(GRID_COLUMN_AGE_RANGE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' LEVEL
      .Column(GRID_COLUMN_LEVEL).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_LEVEL).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(381)
      .Column(GRID_COLUMN_LEVEL).Width = GRID_WIDTH_LEVEL
      .Column(GRID_COLUMN_LEVEL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' CREATED
      .Column(GRID_COLUMN_CREATED).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_CREATED).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(468)
      .Column(GRID_COLUMN_CREATED).Width = GRID_WIDTH_DATE_LONG
      .Column(GRID_COLUMN_CREATED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' VISITS
      ' LAST_RANGE
      .Column(GRID_COLUMN_LAST_RANGE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(542)
      .Column(GRID_COLUMN_LAST_RANGE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5101)
      .Column(GRID_COLUMN_LAST_RANGE).Width = GRID_WIDTH_LAST_RANGE
      .Column(GRID_COLUMN_LAST_RANGE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' LAST_ACTIVITY
      .Column(GRID_COLUMN_LAST_ACTIVITY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(542)
      .Column(GRID_COLUMN_LAST_ACTIVITY).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(467)
      .Column(GRID_COLUMN_LAST_ACTIVITY).Width = GRID_WIDTH_DATE_LONG
      .Column(GRID_COLUMN_LAST_ACTIVITY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' TOTAL
      .Column(GRID_COLUMN_TOTAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(542)
      .Column(GRID_COLUMN_TOTAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7096)
      .Column(GRID_COLUMN_TOTAL).Width = GRID_WIDTH_NUMBER
      .Column(GRID_COLUMN_TOTAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      'MONTH
      ' MONTH_01
      .Column(GRID_COLUMN_MONTH_01).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8603)
      .Column(GRID_COLUMN_MONTH_01).Header(1).Text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(m_cultureinfo.DateTimeFormat.GetMonthName(m_current_month.AddMonths(-5).Month))
      .Column(GRID_COLUMN_MONTH_01).Width = GRID_WIDTH_NUMBER
      .Column(GRID_COLUMN_MONTH_01).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' MONTH_02
      .Column(GRID_COLUMN_MONTH_02).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8603)
      .Column(GRID_COLUMN_MONTH_02).Header(1).Text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(m_cultureinfo.DateTimeFormat.GetMonthName(m_current_month.AddMonths(-4).Month))
      .Column(GRID_COLUMN_MONTH_02).Width = GRID_WIDTH_NUMBER
      .Column(GRID_COLUMN_MONTH_02).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' MONTH_03
      .Column(GRID_COLUMN_MONTH_03).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8603)
      .Column(GRID_COLUMN_MONTH_03).Header(1).Text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(m_cultureinfo.DateTimeFormat.GetMonthName(m_current_month.AddMonths(-3).Month))
      .Column(GRID_COLUMN_MONTH_03).Width = GRID_WIDTH_NUMBER
      .Column(GRID_COLUMN_MONTH_03).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' MONTH_04
      .Column(GRID_COLUMN_MONTH_04).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8603)
      .Column(GRID_COLUMN_MONTH_04).Header(1).Text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(m_cultureinfo.DateTimeFormat.GetMonthName(m_current_month.AddMonths(-2).Month))
      .Column(GRID_COLUMN_MONTH_04).Width = GRID_WIDTH_NUMBER
      .Column(GRID_COLUMN_MONTH_04).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' MONTH_05
      .Column(GRID_COLUMN_MONTH_05).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8603)
      .Column(GRID_COLUMN_MONTH_05).Header(1).Text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(m_cultureinfo.DateTimeFormat.GetMonthName(m_current_month.AddMonths(-1).Month))
      .Column(GRID_COLUMN_MONTH_05).Width = GRID_WIDTH_NUMBER
      .Column(GRID_COLUMN_MONTH_05).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' MONTH_06
      .Column(GRID_COLUMN_MONTH_06).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8603)
      .Column(GRID_COLUMN_MONTH_06).Header(1).Text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(m_cultureinfo.DateTimeFormat.GetMonthName(m_current_month.Month))
      .Column(GRID_COLUMN_MONTH_06).Width = GRID_WIDTH_NUMBER
      .Column(GRID_COLUMN_MONTH_06).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      'WEEK
      ' DAY_01
      .Column(GRID_COLUMN_DAY_01).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8604)
      .Column(GRID_COLUMN_DAY_01).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(307)
      .Column(GRID_COLUMN_DAY_01).Width = GRID_WIDTH_NUMBER
      .Column(GRID_COLUMN_DAY_01).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' DAY_02
      .Column(GRID_COLUMN_DAY_02).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8604)
      .Column(GRID_COLUMN_DAY_02).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(301)
      .Column(GRID_COLUMN_DAY_02).Width = GRID_WIDTH_NUMBER
      .Column(GRID_COLUMN_DAY_02).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' DAY_03
      .Column(GRID_COLUMN_DAY_03).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8604)
      .Column(GRID_COLUMN_DAY_03).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(302)
      .Column(GRID_COLUMN_DAY_03).Width = GRID_WIDTH_NUMBER
      .Column(GRID_COLUMN_DAY_03).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' DAY_04
      .Column(GRID_COLUMN_DAY_04).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8604)
      .Column(GRID_COLUMN_DAY_04).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(303)
      .Column(GRID_COLUMN_DAY_04).Width = GRID_WIDTH_NUMBER
      .Column(GRID_COLUMN_DAY_04).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' DAY_05
      .Column(GRID_COLUMN_DAY_05).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8604)
      .Column(GRID_COLUMN_DAY_05).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(304)
      .Column(GRID_COLUMN_DAY_05).Width = GRID_WIDTH_NUMBER
      .Column(GRID_COLUMN_DAY_05).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' DAY_06
      .Column(GRID_COLUMN_DAY_06).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8604)
      .Column(GRID_COLUMN_DAY_06).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(305)
      .Column(GRID_COLUMN_DAY_06).Width = GRID_WIDTH_NUMBER
      .Column(GRID_COLUMN_DAY_06).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' DAY_07
      .Column(GRID_COLUMN_DAY_07).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8604)
      .Column(GRID_COLUMN_DAY_07).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(306)
      .Column(GRID_COLUMN_DAY_07).Width = GRID_WIDTH_NUMBER
      .Column(GRID_COLUMN_DAY_07).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      'GAMES
      For _i As Int32 = 0 To m_list_controls.Count - 1
        .Column(GRID_COLUMN_INITIAL_FIXED_GAMES + _i).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1923)

        For Each _game As ControlsGamesType In m_list_controls
          If _game.Index = _i Then
            .Column(GRID_COLUMN_INITIAL_FIXED_GAMES + _i).Header(1).Text = _game.GameName
            Exit For
          End If
        Next
        .Column(GRID_COLUMN_INITIAL_FIXED_GAMES + _i).Width = GRID_WIDTH_AMOUNT
        .Column(GRID_COLUMN_INITIAL_FIXED_GAMES + _i).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      Next

      'BET
      .Column(GRID_COLUMN_INITIAL_FIXED_GAMES + m_list_controls.Count).Header(0).Text = ""
      .Column(GRID_COLUMN_INITIAL_FIXED_GAMES + m_list_controls.Count).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5857)
      .Column(GRID_COLUMN_INITIAL_FIXED_GAMES + m_list_controls.Count).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_INITIAL_FIXED_GAMES + m_list_controls.Count).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With
  End Sub

  Private Function GetSqlQuery() As String
    Dim _sb As StringBuilder

    _sb = New StringBuilder()

    _sb.AppendLine("DECLARE @closing_hour    int                                                           ")
    _sb.AppendLine("DECLARE @closing_minutes int                                                           ")
    _sb.AppendLine("SET @closing_hour = ISNULL((SELECT   gp_key_value                                      ")
    _sb.AppendLine("                              FROM   general_params                                    ")
    _sb.AppendLine("                             WHERE   gp_group_key   = 'WigosGUI'                       ")
    _sb.AppendLine("                               AND   gp_subject_key = 'ClosingTime'), 6)               ")
    _sb.AppendLine("SET @closing_minutes = ISNULL((SELECT   gp_key_value                                   ")
    _sb.AppendLine("                                 FROM   general_params                                 ")
    _sb.AppendLine("                                WHERE   gp_group_key   = 'WigosGUI'                    ")
    _sb.AppendLine("                                  AND   gp_subject_key = 'ClosingTimeMinutes'), 0)     ")

    _sb.AppendLine("SELECT   PS_ACCOUNT_ID                                                                                                                               ")
    _sb.AppendLine("       , SUM(PS_PLAYED_AMOUNT) AS PS_PLAYED_AMOUNT                                                                                                   ")
    _sb.AppendLine("       , PS_TERMINAL_ID                                                                                                                              ")
    _sb.AppendLine("       , CASE WHEN DATEADD(MINUTE, @closing_minutes, DATEADD(HOUR, @closing_hour, DATEADD(DAY, DATEDIFF(day, 0, PS_STARTED), 0))) > PS_STARTED       ")
    _sb.AppendLine("              THEN DATEADD(DAY, -1, DATEADD(MINUTE, @closing_minutes, DATEADD(HOUR, @closing_hour, DATEADD(DAY, DATEDIFF(day, 0, PS_STARTED), 0))))  ")
    _sb.AppendLine("              ELSE DATEADD(MINUTE, @closing_minutes, DATEADD(HOUR, @closing_hour, DATEADD(DAY, DATEDIFF(day, 0, PS_STARTED), 0)))                    ")
    _sb.AppendLine("         END AS DATE_STARTED                                                                                                                         ")
    _sb.AppendLine("  INTO   #PLAY_SESSIONS                                                                                                                              ")
    _sb.AppendLine("  FROM   PLAY_SESSIONS                                                                                                                               ")
    _sb.AppendLine(" WHERE   PS_STARTED >= @pDateInitial                                                                                                                 ")
    _sb.AppendLine("   AND   PS_STARTED < @pDateFinal                                                                                                                    ")
    _sb.AppendLine(" GROUP   BY PS_ACCOUNT_ID                                                                                                                            ")
    _sb.AppendLine("       , PS_TERMINAL_ID                                                                                                                              ")
    _sb.AppendLine("       , CASE WHEN DATEADD(MINUTE, @closing_minutes, DATEADD(HOUR, @closing_hour, DATEADD(DAY, DATEDIFF(day, 0, PS_STARTED), 0))) > PS_STARTED       ")
    _sb.AppendLine("              THEN DATEADD(DAY, -1, DATEADD(MINUTE, @closing_minutes, DATEADD(HOUR, @closing_hour, DATEADD(DAY, DATEDIFF(day, 0, PS_STARTED), 0))))  ")
    _sb.AppendLine("              ELSE DATEADD(MINUTE, @closing_minutes, DATEADD(HOUR, @closing_hour, DATEADD(DAY, DATEDIFF(day, 0, PS_STARTED), 0)))                    ")
    _sb.AppendLine("         END                                                                                                                                         ")

    _sb.AppendLine("SELECT DISTINCT AC_ACCOUNT_ID                                                                                                                                                                     ")
    _sb.AppendLine("      , dbo.TrackDataToExternal(AC_TRACK_DATA) AC_TRACK_DATA                                                                                                                                                    ")
    _sb.AppendLine("      , UPPER(AC_HOLDER_NAME) AC_HOLDER_NAME                                                                                                                                                                    ")
    _sb.AppendLine("      , CASE AC_HOLDER_GENDER WHEN 1 THEN 1 WHEN 2 THEN 2 ELSE 3 END AC_HOLDER_GENDER                                                                                                                           ")
    _sb.AppendLine("      , ISNULL(AC_HOLDER_ADDRESS_01,'') + ' ' + ISNULL(AC_HOLDER_ADDRESS_02,'') + ' ' + ISNULL(AC_HOLDER_ADDRESS_03,'') + ' ' + ISNULL(AC_HOLDER_CITY,'') ADDRESS                                               ")
    _sb.AppendLine("      , DATEDIFF(YEAR, AC_HOLDER_BIRTH_DATE,GETDATE()) - (CASE WHEN DATEADD(YY,DATEDIFF(YEAR, AC_HOLDER_BIRTH_DATE, GETDATE()), AC_HOLDER_BIRTH_DATE) > GETDATE() THEN 1 ELSE 0 END) AGE                        ")
    _sb.AppendLine("      , ISNULL(MONTH(AC_HOLDER_BIRTH_DATE), 0) AC_BIRTHDAY_MONTH                                                                                                                                                ")
    _sb.AppendLine("      , ISNULL(DAY(AC_HOLDER_BIRTH_DATE), 0) AC_BIRTHDAY_DAY                                                                                                                                                    ")
    _sb.AppendLine("      , AC_HOLDER_BIRTH_DATE                                                                                                                                                                                    ")
    _sb.AppendLine("      , CASE WHEN DATEDIFF(YEAR, AC_HOLDER_BIRTH_DATE,GETDATE()) - (CASE WHEN DATEADD(YY,DATEDIFF(YEAR, AC_HOLDER_BIRTH_DATE, GETDATE()), AC_HOLDER_BIRTH_DATE) > GETDATE() THEN 1 ELSE 0 END) < 18 THEN '< 18' ")
    _sb.AppendLine("        WHEN DATEDIFF(YEAR, AC_HOLDER_BIRTH_DATE,GETDATE()) - (CASE WHEN DATEADD(YY,DATEDIFF(YEAR, AC_HOLDER_BIRTH_DATE, GETDATE()), AC_HOLDER_BIRTH_DATE) > GETDATE() THEN 1 ELSE 0 END) >= 18 AND             ")
    _sb.AppendLine("        DATEDIFF(YEAR, AC_HOLDER_BIRTH_DATE,GETDATE()) - (CASE WHEN DATEADD(YY,DATEDIFF(YEAR, AC_HOLDER_BIRTH_DATE, GETDATE()), AC_HOLDER_BIRTH_DATE) > GETDATE() THEN 1 ELSE 0 END) <= 24 THEN '18-24'         ")
    _sb.AppendLine("        WHEN DATEDIFF(YEAR, AC_HOLDER_BIRTH_DATE,GETDATE()) - (CASE WHEN DATEADD(YY,DATEDIFF(YEAR, AC_HOLDER_BIRTH_DATE, GETDATE()), AC_HOLDER_BIRTH_DATE) > GETDATE() THEN 1 ELSE 0 END) >= 25 AND             ")
    _sb.AppendLine("        DATEDIFF(YEAR, AC_HOLDER_BIRTH_DATE,GETDATE()) - (CASE WHEN DATEADD(YY,DATEDIFF(YEAR, AC_HOLDER_BIRTH_DATE, GETDATE()), AC_HOLDER_BIRTH_DATE) > GETDATE() THEN 1 ELSE 0 END) <= 34 THEN '25-34'         ")
    _sb.AppendLine("        WHEN DATEDIFF(YEAR, AC_HOLDER_BIRTH_DATE,GETDATE()) - (CASE WHEN DATEADD(YY,DATEDIFF(YEAR, AC_HOLDER_BIRTH_DATE, GETDATE()), AC_HOLDER_BIRTH_DATE) > GETDATE() THEN 1 ELSE 0 END) >= 35 AND             ")
    _sb.AppendLine("        DATEDIFF(YEAR, AC_HOLDER_BIRTH_DATE,GETDATE()) - (CASE WHEN DATEADD(YY,DATEDIFF(YEAR, AC_HOLDER_BIRTH_DATE, GETDATE()), AC_HOLDER_BIRTH_DATE) > GETDATE() THEN 1 ELSE 0 END) <= 44 THEN '35-44'         ")
    _sb.AppendLine("        WHEN DATEDIFF(YEAR, AC_HOLDER_BIRTH_DATE,GETDATE()) - (CASE WHEN DATEADD(YY,DATEDIFF(YEAR, AC_HOLDER_BIRTH_DATE, GETDATE()), AC_HOLDER_BIRTH_DATE) > GETDATE() THEN 1 ELSE 0 END) >= 45 AND             ")
    _sb.AppendLine("        DATEDIFF(YEAR, AC_HOLDER_BIRTH_DATE,GETDATE()) - (CASE WHEN DATEADD(YY,DATEDIFF(YEAR, AC_HOLDER_BIRTH_DATE, GETDATE()), AC_HOLDER_BIRTH_DATE) > GETDATE() THEN 1 ELSE 0 END) <= 54 THEN '45-54'         ")
    _sb.AppendLine("        WHEN DATEDIFF(YEAR, AC_HOLDER_BIRTH_DATE,GETDATE()) - (CASE WHEN DATEADD(YY,DATEDIFF(YEAR, AC_HOLDER_BIRTH_DATE, GETDATE()), AC_HOLDER_BIRTH_DATE) > GETDATE() THEN 1 ELSE 0 END) >= 55 AND             ")
    _sb.AppendLine("        DATEDIFF(YEAR, AC_HOLDER_BIRTH_DATE,GETDATE()) - (CASE WHEN DATEADD(YY,DATEDIFF(YEAR, AC_HOLDER_BIRTH_DATE, GETDATE()), AC_HOLDER_BIRTH_DATE) > GETDATE() THEN 1 ELSE 0 END) <= 64 THEN '55-64'         ")
    _sb.AppendLine("        WHEN DATEDIFF(YEAR, AC_HOLDER_BIRTH_DATE,GETDATE()) - (CASE WHEN DATEADD(YY,DATEDIFF(YEAR, AC_HOLDER_BIRTH_DATE, GETDATE()), AC_HOLDER_BIRTH_DATE) > GETDATE() THEN 1 ELSE 0 END) >= 65 AND             ")
    _sb.AppendLine("        DATEDIFF(YEAR, AC_HOLDER_BIRTH_DATE,GETDATE()) - (CASE WHEN DATEADD(YY,DATEDIFF(YEAR, AC_HOLDER_BIRTH_DATE, GETDATE()), AC_HOLDER_BIRTH_DATE) > GETDATE() THEN 1 ELSE 0 END) <= 74 THEN '65-74'         ")
    _sb.AppendLine("        WHEN DATEDIFF(YEAR, AC_HOLDER_BIRTH_DATE,GETDATE()) - (CASE WHEN DATEADD(YY,DATEDIFF(YEAR, AC_HOLDER_BIRTH_DATE, GETDATE()), AC_HOLDER_BIRTH_DATE) > GETDATE() THEN 1 ELSE 0 END) >= 75 AND             ")
    _sb.AppendLine("        DATEDIFF(YEAR, AC_HOLDER_BIRTH_DATE,GETDATE()) - (CASE WHEN DATEADD(YY,DATEDIFF(YEAR, AC_HOLDER_BIRTH_DATE, GETDATE()), AC_HOLDER_BIRTH_DATE) > GETDATE() THEN 1 ELSE 0 END) <= 84 THEN '75-84'         ")
    _sb.AppendLine("        WHEN DATEDIFF(YEAR, AC_HOLDER_BIRTH_DATE,GETDATE()) - (CASE WHEN DATEADD(YY,DATEDIFF(YEAR, AC_HOLDER_BIRTH_DATE, GETDATE()), AC_HOLDER_BIRTH_DATE) > GETDATE() THEN 1 ELSE 0 END) >= 85 THEN '> 85'     ")
    _sb.AppendLine("        END AGE_RANGE                                                                                                                                                                                           ")
    _sb.AppendLine("      , AC_HOLDER_LEVEL                                                                                                                                                                                         ")
    _sb.AppendLine("      , AC_CREATED                                                                                                                                                                                              ")
    _sb.AppendLine("      , CASE WHEN DATEDIFF(DAY, AC_LAST_ACTIVITY, GETDATE()) <= 30 THEN '< 30'                                                                                                                                  ")
    _sb.AppendLine("        WHEN DATEDIFF(DAY, AC_LAST_ACTIVITY, GETDATE()) <= 60 THEN '< 60'                                                                                                                                       ")
    _sb.AppendLine("        WHEN DATEDIFF(DAY, AC_LAST_ACTIVITY, GETDATE()) <= 90 THEN '< 90'                                                                                                                                       ")
    _sb.AppendLine("        WHEN DATEDIFF(DAY, AC_LAST_ACTIVITY, GETDATE()) <= 120 THEN '< 120'                                                                                                                                     ")
    _sb.AppendLine("        WHEN DATEDIFF(DAY, AC_LAST_ACTIVITY, GETDATE()) <= 150 THEN '< 150'                                                                                                                                     ")
    _sb.AppendLine("        WHEN DATEDIFF(DAY, AC_LAST_ACTIVITY, GETDATE()) <= 180 THEN '< 180'                                                                                                                                     ")
    _sb.AppendLine("        ELSE '> 180' END LAST                                                                                                                                                                                   ")
    _sb.AppendLine("      , AC_LAST_ACTIVITY                                                                                                                                                                                        ")
    _sb.AppendLine("      , AC_TYPE                                                                                                                                                                                                 ")
    _sb.AppendLine("INTO   #ACCOUNT_INFO                                                                                                                                                                                            ")
    _sb.AppendLine("FROM   ACCOUNTS                                                                                                                                                                                                 ")
    _sb.AppendLine("WHERE EXISTS(SELECT 1 FROM #PLAY_SESSIONS WHERE AC_ACCOUNT_ID = PS_ACCOUNT_ID)                                                                                                                                  ")

    _sb.AppendLine("SELECT  TB.ACCOUNT_ID                                                   ")
    _sb.AppendLine("      , SUM(CASE TB.NUMBER_MONTH WHEN 1 THEN 1 ELSE 0 END) +            ")
    _sb.AppendLine("        SUM(CASE TB.NUMBER_MONTH WHEN 2 THEN 1 ELSE 0 END) +            ")
    _sb.AppendLine("        SUM(CASE TB.NUMBER_MONTH WHEN 3 THEN 1 ELSE 0 END) +            ")
    _sb.AppendLine("        SUM(CASE TB.NUMBER_MONTH WHEN 4 THEN 1 ELSE 0 END) +            ")
    _sb.AppendLine("        SUM(CASE TB.NUMBER_MONTH WHEN 5 THEN 1 ELSE 0 END) +            ")
    _sb.AppendLine("        SUM(CASE TB.NUMBER_MONTH WHEN 6 THEN 1 ELSE 0 END) +            ")
    _sb.AppendLine("        SUM(CASE TB.NUMBER_MONTH WHEN 7 THEN 1 ELSE 0 END) +            ")
    _sb.AppendLine("        SUM(CASE TB.NUMBER_MONTH WHEN 8 THEN 1 ELSE 0 END) +            ")
    _sb.AppendLine("        SUM(CASE TB.NUMBER_MONTH WHEN 9 THEN 1 ELSE 0 END) +            ")
    _sb.AppendLine("        SUM(CASE TB.NUMBER_MONTH WHEN 10 THEN 1 ELSE 0 END) +           ")
    _sb.AppendLine("        SUM(CASE TB.NUMBER_MONTH WHEN 11 THEN 1 ELSE 0 END) +           ")
    _sb.AppendLine("        SUM(CASE TB.NUMBER_MONTH WHEN 12 THEN 1 ELSE 0 END) TOTAL       ")
    _sb.AppendLine("      , SUM(CASE TB.NUMBER_MONTH WHEN 1 THEN 1 ELSE 0 END) MONTH_1      ")
    _sb.AppendLine("      , SUM(CASE TB.NUMBER_MONTH WHEN 2 THEN 1 ELSE 0 END) MONTH_2      ")
    _sb.AppendLine("      , SUM(CASE TB.NUMBER_MONTH WHEN 3 THEN 1 ELSE 0 END) MONTH_3      ")
    _sb.AppendLine("      , SUM(CASE TB.NUMBER_MONTH WHEN 4 THEN 1 ELSE 0 END) MONTH_4      ")
    _sb.AppendLine("      , SUM(CASE TB.NUMBER_MONTH WHEN 5 THEN 1 ELSE 0 END) MONTH_5      ")
    _sb.AppendLine("      , SUM(CASE TB.NUMBER_MONTH WHEN 6 THEN 1 ELSE 0 END) MONTH_6      ")
    _sb.AppendLine("      , SUM(CASE TB.NUMBER_MONTH WHEN 7 THEN 1 ELSE 0 END) MONTH_7      ")
    _sb.AppendLine("      , SUM(CASE TB.NUMBER_MONTH WHEN 8 THEN 1 ELSE 0 END) MONTH_8      ")
    _sb.AppendLine("      , SUM(CASE TB.NUMBER_MONTH WHEN 9 THEN 1 ELSE 0 END) MONTH_9      ")
    _sb.AppendLine("      , SUM(CASE TB.NUMBER_MONTH WHEN 10 THEN 1 ELSE 0 END) MONTH_10    ")
    _sb.AppendLine("      , SUM(CASE TB.NUMBER_MONTH WHEN 11 THEN 1 ELSE 0 END) MONTH_11    ")
    _sb.AppendLine("      , SUM(CASE TB.NUMBER_MONTH WHEN 12 THEN 1 ELSE 0 END) MONTH_12    ")
    _sb.AppendLine("INTO   #ACCOUNT_MONTH                                                   ")
    _sb.AppendLine("FROM ( SELECT  DISTINCT PS_ACCOUNT_ID ACCOUNT_ID                        ")
    _sb.AppendLine("             , MONTH(DATE_STARTED) NUMBER_MONTH                         ")
    _sb.AppendLine("        FROM   #PLAY_SESSIONS                                           ")
    _sb.AppendLine("      ) TB                                                              ")
    _sb.AppendLine("GROUP BY TB.ACCOUNT_ID                                                  ")

    _sb.AppendLine("SELECT  TB.ACCOUNT_ID                                                   ")
    _sb.AppendLine("      , SUM(CASE TB.NUMBER_DAY WHEN 1 THEN 1 ELSE 0 END) SUNDAY         ")
    _sb.AppendLine("      , SUM(CASE TB.NUMBER_DAY WHEN 2 THEN 1 ELSE 0 END) MONDAY         ")
    _sb.AppendLine("      , SUM(CASE TB.NUMBER_DAY WHEN 3 THEN 1 ELSE 0 END) TUESDAY        ")
    _sb.AppendLine("      , SUM(CASE TB.NUMBER_DAY WHEN 4 THEN 1 ELSE 0 END) WEDNESDAY      ")
    _sb.AppendLine("      , SUM(CASE TB.NUMBER_DAY WHEN 5 THEN 1 ELSE 0 END) THURSDAY       ")
    _sb.AppendLine("      , SUM(CASE TB.NUMBER_DAY WHEN 6 THEN 1 ELSE 0 END) FRIDAY         ")
    _sb.AppendLine("      , SUM(CASE TB.NUMBER_DAY WHEN 7 THEN 1 ELSE 0 END) SATURDAY       ")
    _sb.AppendLine(" INTO   #ACCOUNT_DAY                                                    ")
    _sb.AppendLine(" FROM ( SELECT   DISTINCT PS_ACCOUNT_ID ACCOUNT_ID                      ")
    _sb.AppendLine("               , DATEPART(dw, DATE_STARTED) NUMBER_DAY                  ")
    _sb.AppendLine("          FROM   #PLAY_SESSIONS                                         ")
    _sb.AppendLine("       ) TB                                                             ")
    _sb.AppendLine("GROUP BY TB.ACCOUNT_ID                                                  ")

    _sb.AppendLine("SELECT  *                                                               ")
    _sb.AppendLine("INTO #ACCOUNT_GAMES                                                     ")
    _sb.AppendLine("FROM  ( SELECT  PS_ACCOUNT_ID ACCOUNT_ID                                ")

    For Each _game As ControlsGamesType In m_list_controls
      _sb.AppendLine("              , SUM (CASE WHEN TE_GAME_TYPE = " & _game.GameType.ToString() & " THEN PS_PLAYED_AMOUNT ELSE 0 END) TD_GAME_TYPE_PLAYED_" & _game.GameType.ToString() & " ")
    Next

    _sb.AppendLine("              , SUM(PS_PLAYED_AMOUNT) BET                               ")
    _sb.AppendLine("         FROM   #PLAY_SESSIONS                                          ")
    _sb.AppendLine("    LEFT JOIN   TERMINALS ON  PS_TERMINAL_ID = TE_TERMINAL_ID           ")
    _sb.AppendLine("     GROUP BY   PS_ACCOUNT_ID                                           ")
    _sb.AppendLine("      )TB                                                               ")

    _sb.AppendLine("SELECT  #ACCOUNT_INFO.*                                                 ")
    _sb.AppendLine("      , #ACCOUNT_MONTH.TOTAL                                            ")
    _sb.AppendLine("      , #ACCOUNT_MONTH.MONTH_1                                          ")
    _sb.AppendLine("      , #ACCOUNT_MONTH.MONTH_2                                          ")
    _sb.AppendLine("      , #ACCOUNT_MONTH.MONTH_3                                          ")
    _sb.AppendLine("      , #ACCOUNT_MONTH.MONTH_4                                          ")
    _sb.AppendLine("      , #ACCOUNT_MONTH.MONTH_5                                          ")
    _sb.AppendLine("      , #ACCOUNT_MONTH.MONTH_6                                          ")
    _sb.AppendLine("      , #ACCOUNT_MONTH.MONTH_7                                          ")
    _sb.AppendLine("      , #ACCOUNT_MONTH.MONTH_8                                          ")
    _sb.AppendLine("      , #ACCOUNT_MONTH.MONTH_9                                          ")
    _sb.AppendLine("      , #ACCOUNT_MONTH.MONTH_10                                         ")
    _sb.AppendLine("      , #ACCOUNT_MONTH.MONTH_11                                         ")
    _sb.AppendLine("      , #ACCOUNT_MONTH.MONTH_12                                         ")
    _sb.AppendLine("      , #ACCOUNT_DAY.SUNDAY                                             ")
    _sb.AppendLine("      , #ACCOUNT_DAY.MONDAY                                             ")
    _sb.AppendLine("      , #ACCOUNT_DAY.TUESDAY                                            ")
    _sb.AppendLine("      , #ACCOUNT_DAY.WEDNESDAY                                          ")
    _sb.AppendLine("      , #ACCOUNT_DAY.THURSDAY                                           ")
    _sb.AppendLine("      , #ACCOUNT_DAY.FRIDAY                                             ")
    _sb.AppendLine("      , #ACCOUNT_DAY.SATURDAY                                           ")

    For Each _game As ControlsGamesType In m_list_controls
      _sb.AppendLine("      , #ACCOUNT_GAMES.TD_GAME_TYPE_PLAYED_" & _game.GameType.ToString() & "  / #ACCOUNT_MONTH.TOTAL  TD_GAME_TYPE_PLAYED_" & _game.GameType.ToString() & " ")
    Next

    _sb.AppendLine("      , #ACCOUNT_GAMES.BET  / #ACCOUNT_MONTH.TOTAL  BET                              ")
    _sb.AppendLine("FROM #ACCOUNT_INFO                                                                   ")
    _sb.AppendLine("LEFT JOIN #ACCOUNT_MONTH ON #ACCOUNT_MONTH.ACCOUNT_ID = #ACCOUNT_INFO.AC_ACCOUNT_ID  ")
    _sb.AppendLine("LEFT JOIN #ACCOUNT_DAY ON #ACCOUNT_DAY.ACCOUNT_ID = #ACCOUNT_INFO.AC_ACCOUNT_ID      ")
    _sb.AppendLine("LEFT JOIN #ACCOUNT_GAMES ON #ACCOUNT_GAMES.ACCOUNT_ID = #ACCOUNT_INFO.AC_ACCOUNT_ID  ")
    _sb.AppendLine("ORDER BY #ACCOUNT_INFO.AC_ACCOUNT_ID                                                 ")

    _sb.AppendLine("DROP TABLE #ACCOUNT_INFO    ")
    _sb.AppendLine("DROP TABLE #ACCOUNT_MONTH   ")
    _sb.AppendLine("DROP TABLE #ACCOUNT_DAY     ")
    _sb.AppendLine("DROP TABLE #ACCOUNT_GAMES   ")
    _sb.AppendLine("DROP TABLE #PLAY_SESSIONS   ")


    Return _sb.ToString()
  End Function

  Private Function ApplyFilters(ByVal _query_as_datatable As DataTable) As DataRow()
    Dim _filter As String
    Dim _filter_visit As String
    Dim _aux_filter As String
    Dim _visit As Integer
    Dim _amount As Double
    Dim _filter_amount As String
    Dim _aux_game As Double
    Dim _aux_bet As Double

    _filter = ""


    If Not String.IsNullOrEmpty(uc_account_search.GetFilterSQL()) Then
      _filter += uc_account_search.GetFilterSQL()
      _filter = " AND " & _filter.Replace("UPPER", "")
    End If

    _aux_filter = ""
    If chk_gender_male.Checked Then
      _aux_filter += ", 1"
    End If
    If chk_gender_female.Checked Then
      _aux_filter += ", 2"
    End If
    If chk_gender_unknown.Checked Then
      _aux_filter += ", 3"
    End If
    If Not String.IsNullOrEmpty(_aux_filter) Then
      _filter += " AND AC_HOLDER_GENDER IN (" & _aux_filter.Substring(2, _aux_filter.Length - 2) & ")"
    End If

    _aux_filter = ""
    If chk_level_01.Checked Then
      _aux_filter += ", 1"
    End If
    If chk_level_02.Checked Then
      _aux_filter += ", 2"
    End If
    If chk_level_03.Checked Then
      _aux_filter += ", 3"
    End If
    If chk_level_04.Checked Then
      _aux_filter += ", 4"
    End If
    If Not String.IsNullOrEmpty(_aux_filter) Then
      _filter += " AND AC_HOLDER_LEVEL IN (" & _aux_filter.Substring(2, _aux_filter.Length - 2) & ")"
    End If

    If chk_birthday.Checked Then
      If ef_birthday_day.TextValue <> "" Then
        _filter += " AND AC_BIRTHDAY_DAY = " & ef_birthday_day.TextValue
      End If
      If cmb_birthday.SelectedIndex <> 0 Then
        _filter += " AND AC_BIRTHDAY_MONTH = " & cmb_birthday.SelectedIndex.ToString()
      End If
    End If

    If chk_range_age.Checked Then
      If opt_range.Checked Then
        _filter += " AND AGE_RANGE = '" & cmb_age_range.TextValue().Trim() & "'"
      End If
      If opt_range_custom.Checked Then
        If Not String.IsNullOrEmpty(ef_age_from.TextValue) Then
          _filter += " AND AGE >= " & ef_age_from.TextValue().Trim()
        End If

        If Not String.IsNullOrEmpty(ef_age_to.TextValue) Then
          _filter += " AND AGE <= " & ef_age_to.TextValue().Trim()
        End If
      End If
    End If

    If dtp_from_creation.Checked Then
      _filter += " AND AC_CREATED >= #" & dtp_from_creation.Value.ToString("MM/dd/yyyy HH:mm:ss") & "#"
    End If
    If dtp_to_creation.Checked Then
      _filter += " AND AC_CREATED <= #" & dtp_to_creation.Value.ToString("MM/dd/yyyy HH:mm:ss") & "#"
    End If

    If chk_visit.Checked Then
      If opt_last_visit.Checked Then
        _filter += " AND LAST = '" & cmb_visit.TextValue().Trim().Replace(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7310), "").Trim() & "'"
      End If
      If opt_last_visit_date.Checked Then
        _filter += " AND AC_LAST_ACTIVITY >= #" & dtp_from_visit.Value.ToString("MM/dd/yyyy HH:mm:ss") & "#"
        _filter += " AND AC_LAST_ACTIVITY <= #" & dtp_to_visit.Value.ToString("MM/dd/yyyy HH:mm:ss") & "#"
      End If
    End If

    'Months
    _filter_visit = ""
    _aux_filter = ""
    _visit = 1
    If chk_visit_month_01.Checked Then
      If Not String.IsNullOrEmpty(ef_month_from_01.TextValue) Then
        _visit = ef_month_from_01.TextValue
      End If
      _aux_filter += " AND MONTH_" & m_old_month.Month.ToString() & " >= " & _visit.ToString()

      If Not String.IsNullOrEmpty(ef_month_to_01.TextValue) Then
        _aux_filter += " AND MONTH_" & m_old_month.Month.ToString() & " <= " & ef_month_to_01.TextValue()
      End If

      _filter_visit += " OR (" & Mid(_aux_filter, 5, _aux_filter.Length) & ")"
    End If

    _aux_filter = ""
    _visit = 1
    If chk_visit_month_02.Checked Then
      If Not String.IsNullOrEmpty(ef_month_from_02.TextValue) Then
        _visit = ef_month_from_02.TextValue
      End If
      _aux_filter += " AND MONTH_" & m_old_month.AddMonths(1).Month.ToString() & " >= " & _visit.ToString()

      If Not String.IsNullOrEmpty(ef_month_to_02.TextValue) Then
        _aux_filter += " AND MONTH_" & m_old_month.AddMonths(1).Month.ToString() & " <= " & ef_month_to_02.TextValue()
      End If

      _filter_visit += " OR (" & Mid(_aux_filter, 5, _aux_filter.Length) & ")"
    End If

    _aux_filter = ""
    _visit = 1
    If chk_visit_month_03.Checked Then
      If Not String.IsNullOrEmpty(ef_month_from_03.TextValue) Then
        _visit = ef_month_from_03.TextValue
      End If
      _aux_filter += " AND MONTH_" & m_old_month.AddMonths(2).Month.ToString() & " >= " & _visit.ToString()

      If Not String.IsNullOrEmpty(ef_month_to_03.TextValue) Then
        _aux_filter += " AND MONTH_" & m_old_month.AddMonths(2).Month.ToString() & " <= " & ef_month_to_03.TextValue()
      End If

      _filter_visit += " OR (" & Mid(_aux_filter, 5, _aux_filter.Length) & ")"
    End If

    _aux_filter = ""
    _visit = 1
    If chk_visit_month_04.Checked Then
      If Not String.IsNullOrEmpty(ef_month_from_04.TextValue) Then
        _visit = ef_month_from_04.TextValue
      End If
      _aux_filter += " AND MONTH_" & m_old_month.AddMonths(3).Month.ToString() & " >= " & _visit.ToString()

      If Not String.IsNullOrEmpty(ef_month_to_04.TextValue) Then
        _aux_filter += " AND MONTH_" & m_old_month.AddMonths(3).Month.ToString() & " <= " & ef_month_to_04.TextValue()
      End If

      _filter_visit += " OR (" & Mid(_aux_filter, 5, _aux_filter.Length) & ")"
    End If

    _aux_filter = ""
    _visit = 1
    If chk_visit_month_05.Checked Then
      If Not String.IsNullOrEmpty(ef_month_from_05.TextValue) Then
        _visit = ef_month_from_05.TextValue
      End If
      _aux_filter += " AND MONTH_" & m_old_month.AddMonths(4).Month.ToString() & " >= " & _visit.ToString()

      If Not String.IsNullOrEmpty(ef_month_to_05.TextValue) Then
        _aux_filter += " AND MONTH_" & m_old_month.AddMonths(4).Month.ToString() & " <= " & ef_month_to_05.TextValue()
      End If

      _filter_visit += " OR (" & Mid(_aux_filter, 5, _aux_filter.Length) & ")"
    End If

    _aux_filter = ""
    _visit = 1
    If chk_visit_month_06.Checked Then
      If Not String.IsNullOrEmpty(ef_month_from_06.TextValue) Then
        _visit = ef_month_from_06.TextValue
      End If
      _aux_filter += " AND MONTH_" & m_old_month.AddMonths(5).Month.ToString() & " >= " & _visit.ToString()

      If Not String.IsNullOrEmpty(ef_month_to_06.TextValue) Then
        _aux_filter += " AND MONTH_" & m_old_month.AddMonths(5).Month.ToString() & " <= " & ef_month_to_06.TextValue()
      End If

      _filter_visit += " OR (" & Mid(_aux_filter, 5, _aux_filter.Length) & ")"
    End If

    If Not String.IsNullOrEmpty(_filter_visit) Then
      _filter += " AND  (" & Mid(_filter_visit, 4, _filter_visit.Length) & ") "
    End If


    'Days
    _filter_visit = ""
    _aux_filter = ""
    _visit = 1
    If chk_day_sunday.Checked Then
      If Not String.IsNullOrEmpty(ef_from_sunday.TextValue) Then
        _visit = ef_from_sunday.TextValue
      End If
      _aux_filter += " AND SUNDAY >= " & _visit.ToString()

      If Not String.IsNullOrEmpty(ef_to_sunday.TextValue) Then
        _aux_filter += " AND SUNDAY <= " & ef_to_sunday.TextValue()
      End If

      _filter_visit += " OR (" & Mid(_aux_filter, 5, _aux_filter.Length) & ")"
    End If

    _aux_filter = ""
    _visit = 1
    If chk_day_monday.Checked Then
      If Not String.IsNullOrEmpty(ef_from_monday.TextValue) Then
        _visit = ef_from_monday.TextValue
      End If
      _aux_filter += " AND MONDAY >= " & _visit.ToString()

      If Not String.IsNullOrEmpty(ef_to_monday.TextValue) Then
        _aux_filter += " AND MONDAY <= " & ef_to_monday.TextValue()
      End If

      _filter_visit += " OR (" & Mid(_aux_filter, 5, _aux_filter.Length) & ")"
    End If

    _aux_filter = ""
    _visit = 1
    If chk_day_thuesday.Checked Then
      If Not String.IsNullOrEmpty(ef_from_thuesday.TextValue) Then
        _visit = ef_from_thuesday.TextValue
      End If
      _aux_filter += " AND TUESDAY >= " & _visit.ToString()

      If Not String.IsNullOrEmpty(ef_to_thuesday.TextValue) Then
        _aux_filter += " AND TUESDAY <= " & ef_to_thuesday.TextValue()
      End If

      _filter_visit += " OR (" & Mid(_aux_filter, 5, _aux_filter.Length) & ")"
    End If

    _aux_filter = ""
    _visit = 1
    If chk_day_wednesday.Checked Then
      If Not String.IsNullOrEmpty(ef_from_wednesday.TextValue) Then
        _visit = ef_from_wednesday.TextValue
      End If
      _aux_filter += " AND WEDNESDAY >= " & _visit.ToString()

      If Not String.IsNullOrEmpty(ef_to_wednesday.TextValue) Then
        _aux_filter += " AND WEDNESDAY <= " & ef_to_wednesday.TextValue()
      End If

      _filter_visit += " OR (" & Mid(_aux_filter, 5, _aux_filter.Length) & ")"
    End If

    _aux_filter = ""
    _visit = 1
    If chk_day_thursday.Checked Then
      If Not String.IsNullOrEmpty(ef_from_thursday.TextValue) Then
        _visit = ef_from_thursday.TextValue
      End If
      _aux_filter += " AND THURSDAY >= " & _visit.ToString()

      If Not String.IsNullOrEmpty(ef_to_thursday.TextValue) Then
        _aux_filter += " AND THURSDAY <= " & ef_to_thursday.TextValue()
      End If

      _filter_visit += " OR (" & Mid(_aux_filter, 5, _aux_filter.Length) & ")"
    End If

    _aux_filter = ""
    _visit = 1
    If chk_day_friday.Checked Then
      If Not String.IsNullOrEmpty(ef_from_friday.TextValue) Then
        _visit = ef_from_friday.TextValue
      End If
      _aux_filter += " AND FRIDAY >= " & _visit.ToString()

      If Not String.IsNullOrEmpty(ef_to_friday.TextValue) Then
        _aux_filter += " AND FRIDAY <= " & ef_to_friday.TextValue()
      End If

      _filter_visit += " OR (" & Mid(_aux_filter, 5, _aux_filter.Length) & ")"
    End If

    _aux_filter = ""
    _visit = 1
    If chk_day_saturday.Checked Then
      If Not String.IsNullOrEmpty(ef_from_saturday.TextValue) Then
        _visit = ef_from_saturday.TextValue
      End If
      _aux_filter += " AND SATURDAY >= " & _visit.ToString()

      If Not String.IsNullOrEmpty(ef_to_saturday.TextValue) Then
        _aux_filter += " AND SATURDAY <= " & ef_to_saturday.TextValue()
      End If

      _filter_visit += " OR (" & Mid(_aux_filter, 5, _aux_filter.Length) & ")"
    End If

    If Not String.IsNullOrEmpty(_filter_visit) Then
      _filter += " AND  (" & Mid(_filter_visit, 4, _filter_visit.Length) & ") "
    End If

    _filter_amount = ""
    For Each _game As ControlsGamesType In m_list_controls
      _aux_filter = ""
      _aux_game = 0.0
      _amount = 0.01

      ' XGJ 16-NOV-2017
      If _game.ControlCheckBox.Checked Then
        If Not String.IsNullOrEmpty(_game.ControlEntryFieldFrom.TextValue) Then
          _amount = _game.ControlEntryFieldFrom.TextValue
        End If
        _aux_filter += " AND TD_GAME_TYPE_PLAYED_" & _game.GameType.ToString() & " >= " & GUI_LocalNumberToDBNumber(_amount.ToString())

        If Not String.IsNullOrEmpty(_game.ControlEntryFieldTo.TextValue) Then
          _aux_game = _game.ControlEntryFieldTo.TextValue
          _aux_filter += " AND TD_GAME_TYPE_PLAYED_" & _game.GameType.ToString() & " <= " & GUI_LocalNumberToDBNumber(_aux_game)
        End If

        _filter_amount += " OR (" & Mid(_aux_filter, 5, _aux_filter.Length) & ")"
      End If
    Next

    If Not String.IsNullOrEmpty(_filter_amount) Then
      _filter += " AND  (" & Mid(_filter_amount, 4, _filter_amount.Length) & ") "
    End If

    ' XGJ 16-NOV-2017
    If chk_bet.Checked Then
      If Not String.IsNullOrEmpty(ef_bet_from.TextValue) Then
        _aux_bet = ef_bet_from.TextValue
        _filter += " AND BET >= " & GUI_LocalNumberToDBNumber(_aux_bet.ToString())
      End If

      If Not String.IsNullOrEmpty(ef_bet_to.TextValue) Then
        _aux_bet = ef_bet_to.TextValue
        _filter += " AND BET <= " & GUI_LocalNumberToDBNumber(_aux_bet.ToString())
      End If
    End If

    _filter = Mid(_filter, 5, _filter.Length)

    If String.IsNullOrEmpty(_filter) Then
      Return _query_as_datatable.Select()
    Else
      Return _query_as_datatable.Select(_filter, "AC_ACCOUNT_ID")
    End If

  End Function

  Private Sub SetRowTotalGrid()
    Dim _idx_row As Integer
    Dim _count_visit As Integer
    Dim _index As Integer

    Me.Grid.AddRow()

    _idx_row = Me.Grid.NumRows - 1

    'Label - TOTAL
    Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_ID).Value = GLB_NLS_GUI_INVOICING.GetString(205)

    'No. Visits
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TOTAL).Value = m_data_table.Compute("SUM(TOTAL)", "")

    'Month Visits
    For _i As Integer = 0 To 5
      _count_visit = m_data_table.Compute("SUM(MONTH_" & m_old_month.AddMonths(_i).Month.ToString() & ")", "")

      Select Case _i
        Case 0
          Me.Grid.Cell(_idx_row, GRID_COLUMN_MONTH_01).Value = _count_visit
        Case 1
          Me.Grid.Cell(_idx_row, GRID_COLUMN_MONTH_02).Value = _count_visit
        Case 2
          Me.Grid.Cell(_idx_row, GRID_COLUMN_MONTH_03).Value = _count_visit
        Case 3
          Me.Grid.Cell(_idx_row, GRID_COLUMN_MONTH_04).Value = _count_visit
        Case 4
          Me.Grid.Cell(_idx_row, GRID_COLUMN_MONTH_05).Value = _count_visit
        Case 5
          Me.Grid.Cell(_idx_row, GRID_COLUMN_MONTH_06).Value = _count_visit
      End Select
    Next

    'Week Visits
    Me.Grid.Cell(_idx_row, GRID_COLUMN_DAY_01).Value = m_data_table.Compute("SUM(SUNDAY)", "")
    Me.Grid.Cell(_idx_row, GRID_COLUMN_DAY_02).Value = m_data_table.Compute("SUM(MONDAY)", "")
    Me.Grid.Cell(_idx_row, GRID_COLUMN_DAY_03).Value = m_data_table.Compute("SUM(TUESDAY)", "")
    Me.Grid.Cell(_idx_row, GRID_COLUMN_DAY_04).Value = m_data_table.Compute("SUM(WEDNESDAY)", "")
    Me.Grid.Cell(_idx_row, GRID_COLUMN_DAY_05).Value = m_data_table.Compute("SUM(THURSDAY)", "")
    Me.Grid.Cell(_idx_row, GRID_COLUMN_DAY_06).Value = m_data_table.Compute("SUM(FRIDAY)", "")
    Me.Grid.Cell(_idx_row, GRID_COLUMN_DAY_07).Value = m_data_table.Compute("SUM(SATURDAY)", "")

    _index = 0
    For Each _game As ControlsGamesType In m_list_controls
      Me.Grid.Cell(_idx_row, GRID_COLUMN_INITIAL_FIXED_GAMES + _index).Value = GUI_FormatCurrency(m_data_table.Compute("SUM(TD_GAME_TYPE_PLAYED_" & _game.GameType.ToString() & ")", ""), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      _index += 1
    Next

    Me.Grid.Cell(_idx_row, GRID_COLUMN_INITIAL_FIXED_GAMES + m_list_controls.Count).Value = GUI_FormatCurrency(m_data_table.Compute("SUM(BET)", ""), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Color Row
    Me.Grid.Row(_idx_row).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
  End Sub

#End Region

#Region " Events"

  Private Sub opt_last_visit_date_CheckedChanged(sender As Object, e As EventArgs) Handles opt_last_visit_date.CheckedChanged
    Me.dtp_from_visit.Enabled = IIf(opt_last_visit_date.Checked, True, False)
    Me.dtp_to_visit.Enabled = IIf(opt_last_visit_date.Checked, True, False)
  End Sub

  Private Sub opt_last_visit_CheckedChanged(sender As Object, e As EventArgs) Handles opt_last_visit.CheckedChanged
    Me.cmb_visit.Enabled = IIf(opt_last_visit.Checked, True, False)
  End Sub

  Private Sub chk_visit_CheckedChanged(sender As Object, e As EventArgs) Handles chk_visit.CheckedChanged
    Me.pnl_visit.Enabled = IIf(chk_visit.Checked, True, False)

    If Me.opt_last_visit.Checked Then
      Me.opt_last_visit.Checked = True
    Else
      Me.opt_last_visit_date.Checked = True
    End If
  End Sub

  Private Sub chk_visit_month_CheckedChanged(sender As Object, e As EventArgs) Handles chk_visit_month_01.CheckedChanged, _
                                                                                      chk_visit_month_02.CheckedChanged, _
                                                                                      chk_visit_month_03.CheckedChanged, _
                                                                                      chk_visit_month_04.CheckedChanged, _
                                                                                      chk_visit_month_05.CheckedChanged, _
                                                                                      chk_visit_month_06.CheckedChanged

    ef_month_from_01.Enabled = IIf(chk_visit_month_01.Checked, True, False)
    ef_month_to_01.Enabled = IIf(chk_visit_month_01.Checked, True, False)

    ef_month_from_02.Enabled = IIf(chk_visit_month_02.Checked, True, False)
    ef_month_to_02.Enabled = IIf(chk_visit_month_02.Checked, True, False)

    ef_month_from_03.Enabled = IIf(chk_visit_month_03.Checked, True, False)
    ef_month_to_03.Enabled = IIf(chk_visit_month_03.Checked, True, False)

    ef_month_from_04.Enabled = IIf(chk_visit_month_04.Checked, True, False)
    ef_month_to_04.Enabled = IIf(chk_visit_month_04.Checked, True, False)

    ef_month_from_05.Enabled = IIf(chk_visit_month_05.Checked, True, False)
    ef_month_to_05.Enabled = IIf(chk_visit_month_05.Checked, True, False)

    ef_month_from_06.Enabled = IIf(chk_visit_month_06.Checked, True, False)
    ef_month_to_06.Enabled = IIf(chk_visit_month_06.Checked, True, False)

  End Sub

  Private Sub opt_range_CheckedChanged(sender As Object, e As EventArgs) Handles opt_range.CheckedChanged
    cmb_age_range.Enabled = IIf(opt_range.Checked, True, False)
  End Sub

  Private Sub opt_range_custom_CheckedChanged(sender As Object, e As EventArgs) Handles opt_range_custom.CheckedChanged
    ef_age_from.Enabled = IIf(opt_range_custom.Checked, True, False)
    ef_age_to.Enabled = IIf(opt_range_custom.Checked, True, False)
  End Sub

  Private Sub chk_range_age_CheckedChanged(sender As Object, e As EventArgs) Handles chk_range_age.CheckedChanged
    Me.pnl_age_range.Enabled = IIf(chk_range_age.Checked, True, False)

    If Me.opt_range.Checked Then
      Me.opt_range.Checked = True
    Else
      Me.opt_range_custom.Checked = True
    End If
  End Sub

  Private Sub chk_day_CheckedChanged(sender As Object, e As EventArgs) Handles chk_day_sunday.CheckedChanged, _
                                                                              chk_day_monday.CheckedChanged, _
                                                                              chk_day_thursday.CheckedChanged, _
                                                                              chk_day_wednesday.CheckedChanged, _
                                                                              chk_day_thuesday.CheckedChanged, _
                                                                              chk_day_friday.CheckedChanged, _
                                                                              chk_day_saturday.CheckedChanged
    ef_from_sunday.Enabled = IIf(chk_day_sunday.Checked, True, False)
    ef_to_sunday.Enabled = IIf(chk_day_sunday.Checked, True, False)

    ef_from_monday.Enabled = IIf(chk_day_monday.Checked, True, False)
    ef_to_monday.Enabled = IIf(chk_day_monday.Checked, True, False)

    ef_from_thursday.Enabled = IIf(chk_day_thursday.Checked, True, False)
    ef_to_thursday.Enabled = IIf(chk_day_thursday.Checked, True, False)

    ef_from_wednesday.Enabled = IIf(chk_day_wednesday.Checked, True, False)
    ef_to_wednesday.Enabled = IIf(chk_day_wednesday.Checked, True, False)

    ef_from_thuesday.Enabled = IIf(chk_day_thuesday.Checked, True, False)
    ef_to_thuesday.Enabled = IIf(chk_day_thuesday.Checked, True, False)

    ef_from_friday.Enabled = IIf(chk_day_friday.Checked, True, False)
    ef_to_friday.Enabled = IIf(chk_day_friday.Checked, True, False)

    ef_from_saturday.Enabled = IIf(chk_day_saturday.Checked, True, False)
    ef_to_saturday.Enabled = IIf(chk_day_saturday.Checked, True, False)

  End Sub

  Private Sub chk_games_CheckedChanged(sender As Object, e As EventArgs)
    Dim _name As String
    Dim _check As Boolean

    _name = CType(sender, CheckBox).Name
    _check = CType(sender, CheckBox).Checked

    For Each _game As ControlsGamesType In m_list_controls
      If _game.ControlCheckBox.Name = _name Then
        _game.ControlEntryFieldFrom.Enabled = IIf(_check, True, False)
        _game.ControlEntryFieldTo.Enabled = IIf(_check, True, False)

        Exit For
      End If
    Next

  End Sub

  Private Sub chk_bet_CheckedChanged(sender As Object, e As EventArgs) Handles chk_bet.CheckedChanged
    Me.pnl_bet.Enabled = IIf(chk_bet.Checked, True, False)
  End Sub

#End Region

  Private Sub chk_birthday_CheckedChanged(sender As Object, e As EventArgs) Handles chk_birthday.CheckedChanged
    Me.pnl_birthday.Enabled = IIf(chk_birthday.Checked, True, False)
  End Sub

End Class