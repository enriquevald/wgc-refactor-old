<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_screen_selector
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.lbl_title = New System.Windows.Forms.Label
    Me.flp_list = New System.Windows.Forms.FlowLayoutPanel
    Me.pnl_line = New System.Windows.Forms.Panel
    Me.panel_data.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.panel_data.AutoSize = True
    Me.panel_data.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.panel_data.BackColor = System.Drawing.SystemColors.Info
    Me.panel_data.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.panel_data.Controls.Add(Me.pnl_line)
    Me.panel_data.Controls.Add(Me.flp_list)
    Me.panel_data.Controls.Add(Me.lbl_title)
    Me.panel_data.Dock = System.Windows.Forms.DockStyle.Fill
    Me.panel_data.Location = New System.Drawing.Point(0, 0)
    Me.panel_data.Size = New System.Drawing.Size(543, 231)
    '
    'lbl_title
    '
    Me.lbl_title.AutoSize = True
    Me.lbl_title.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_title.Location = New System.Drawing.Point(9, 3)
    Me.lbl_title.Name = "lbl_title"
    Me.lbl_title.Size = New System.Drawing.Size(44, 13)
    Me.lbl_title.TabIndex = 0
    Me.lbl_title.Text = "xTitle"
    '
    'flp_list
    '
    Me.flp_list.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.flp_list.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
    Me.flp_list.Location = New System.Drawing.Point(6, 33)
    Me.flp_list.Name = "flp_list"
    Me.flp_list.Size = New System.Drawing.Size(528, 186)
    Me.flp_list.TabIndex = 1
    '
    'pnl_line
    '
    Me.pnl_line.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.pnl_line.Location = New System.Drawing.Point(9, 20)
    Me.pnl_line.Name = "pnl_line"
    Me.pnl_line.Size = New System.Drawing.Size(528, 4)
    Me.pnl_line.TabIndex = 2
    '
    'frm_screen_selector
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.AutoSize = True
    Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.BackColor = System.Drawing.SystemColors.ControlDark
    Me.ClientSize = New System.Drawing.Size(631, 231)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
    Me.Name = "frm_screen_selector"
    Me.Padding = New System.Windows.Forms.Padding(0)
    Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
    'ATB 19-10-2016
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7680)
    Me.TopMost = True
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents flp_list As System.Windows.Forms.FlowLayoutPanel
  Friend WithEvents lbl_title As System.Windows.Forms.Label
  Protected WithEvents pnl_line As System.Windows.Forms.Panel
End Class
