'-------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_progressive_provision_report.vb
'
' DESCRIPTION : Progressive Jackpots Provissioning screen
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 08-AUG-2014  JMM    Initial version.
' 19-AUG-2014  FJC    Development version.
' 06-OCT-2014  FJC    Fixed Bug 1409: Totalizador: est� est� en otro color en los informes de progresivos
' 06-OCT-2014  FJC    Align Left Column Name Level 
' 08-OCT-2014  FJC    Add column User (User who has created the provision)
' 29-OCT-2014  SGB    Fixed Bug WIG-1595: Add excel filter Dates, group & details.
' 26-JAN-2015  FJC    Fixed Bug WIG-1965: Remove intersection filter dates.
' 29-JAN-2015  SGB    Fixed Bug WIG-1979: Add cutoff time in 'date to'.
' 30-FEB-2015  SGB    Fixed Bug WIG-1990: Add excel hours in filter Dates.
'--------------------------------------------------------------------
Option Strict Off
Option Explicit On

Imports System.Data.OleDb
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports GUI_CommonOperations.CLASS_BASE
Imports WSI.Common

Public Class frm_progressive_provision_report
  Inherits frm_base_sel

#Region " Constants "

  ' Grid configuration
  Private Const GRID_HEADER_ROWS As Integer = 2
  Private GRID_COLUMNS_PROGRESSIVE As Integer
  Private TERMINAL_DATA_COLUMNS As Int32

  ' Hidden
  Private GRID_COLUMN_PROGRESSIVE_ID As Integer = 0
  Private GRID_COLUMN_PROGRESSIVE_PROVISION_ID As Integer = 1
  Private GRID_COLUMN_PROGRESSIVE_TERMINAL_ID As Integer = 2

  ' Showed
  Private GRID_COLUMN_PROGRESSIVE_CREATION_DATE As Integer = 3
  Private GRID_COLUMN_PROGRESSIVE_HOUR_FROM As Integer = 4
  Private GRID_COLUMN_PROGRESSIVE_HOUR_TO As Integer = 5

  Private GRID_COLUMN_PROGRESSIVE_GUI_USER As Integer = 6

  Private GRID_COLUMN_PROGRESSIVE_STATUS As Integer = 7
  Private GRID_COLUMN_PROGRESSIVE_NAME As Integer = 8
  Private GRID_COLUMN_PROGRESSIVE_AMOUNT As Integer = 9
  Private GRID_INIT_TERMINAL_DATA As Integer
  Private GRID_COLUMN_PROGRESSIVE_TERMINAL_AMOUNT As Integer
  Private GRID_COLUMN_PROGRESSIVE_LEVEL_NAME As Integer
  Private GRID_COLUMN_PROGRESSIVE_LEVEL_AMOUNT As Integer
  Private GRID_COLUMN_PROGRESSIVE_CAGE_AMOUNT As Integer

  'SQL Query Columns
  Private Const SQL_COL_PROGRESSIVE_ID As Integer = 0
  Private Const SQL_COL_PROGRESSIVE_NAME As Integer = 1
  Private Const SQL_COL_PROGRESSIVE_AMOUNT As Integer = 2
  Private Const SQL_COL_PROGRESSIVE_CAGE_AMOUNT As Integer = 3
  Private Const SQL_COL_PROGRESSIVE_PROVISION_ID As Integer = 4
  Private Const SQL_COL_PROGRESSIVE_CREATION_DATE As Integer = 5
  Private Const SQL_COL_PROGRESSIVE_PROVISION_STATUS As Integer = 6
  Private Const SQL_COL_PROGRESSIVE_HOUR_FROM As Integer = 7
  Private Const SQL_COL_PROGRESSIVE_HOUR_TO As Integer = 8
  Private Const SQL_COL_PROGRESSIVE_GUI_USER As Integer = 9

  Private Const SQL_COL_PROGRESSIVE_TERMINAL_ID As Integer = 10
  Private Const SQL_COL_PROGRESSIVE_PROVIDER As Integer = 11
  Private Const SQL_COL_PROGRESSIVE_TERMINAL As Integer = 12
  Private Const SQL_COL_PROGRESSIVE_LEVEL_NAME As Integer = 13

  ' Grid Columns Width

  '   group by Progressive
  Private Const GRID_WIDTH_PROGRESSIVE_GROUP_PROGRESSIVE As Integer = 4000
  Private Const GRID_WIDTH_PROVIDER_GROUP_PROGRESSIVE As Integer = 2700
  Private Const GRID_WIDTH_TERMINAL_GROUP_PROGRESSIVE As Integer = 3000
  Private Const GRID_WIDTH_AMOUNT_GROUP_PROGRESSIVE As Integer = 2000
  Private Const GRID_WIDTH_AMOUNT_CAGE As Integer = 1900

  '   group by Provision
  Private Const GRID_WIDTH_PROGRESSIVE_GROUP_PROVISION As Integer = 2500
  Private Const GRID_WIDTH_PROVIDER_GROUP_PROVISION As Integer = 1900
  Private Const GRID_WIDTH_TERMINAL_GROUP_PROVISION As Integer = 2300
  Private Const GRID_WIDTH_CREATION_DATE_PROVISION As Integer = 2000
  Private Const GRID_WIDTH_AMOUNT_GROUP_PROVISION As Integer = 1600
  Private Const GRID_WIDTH_HOUR_FROM As Integer = 2000
  Private Const GRID_WIDTH_HOUR_TO As Integer = 2000
  Private Const GRID_WIDTH_GUI_USER As Integer = 1725
  Private Const GRID_WIDTH_STATUS_PROVISION As Integer = 400

#End Region 'Constants

#Region " Enums "

  Private Enum ENUM_SUBTOTAL_TYPE
    PROGRESIVE = 0
    PROVISION = 1
    TOTAL = 2
  End Enum ' ENUM_SUBTOTAL_TYPE

#End Region ' Enums

#Region " Members "

  'SUBTOTALS
  Private m_total_amount_progressive As Decimal
  Private m_total_amount_provision As Decimal
  Private m_total_amount As Decimal
  Private m_total_cage_amount_progressive As Decimal
  Private m_total_cage_amount As Decimal
  Private m_cage_amount As Decimal

  'ID's
  Private progressive_id As Long = -1
  Private provision_id As Long = -1
  Private terminal_id As Long = -1

  'OTHERS
  Private progressive_name As String
  Private m_terminal_report_type As ReportType = ReportType.Provider

  ' Report filters
  Public m_progressive_group As String
  Public m_progressive_date_type As String
  Public m_progressive_date_from As String
  Public m_progressive_date_to As String
  Public m_progressive_detail As String
  Public m_progressive_catalog As String
  Public m_prevision As String

#End Region ' Members

#Region " Overrides "

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_PROVISIONS_REPORT

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    Dim _dt_progressives As DataTable = Nothing

    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    ' Initialize Form Controls

    ' - Form
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5206) ' 5206 "Informe de Provisiones"

    ' - Buttons
    GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(27)
    GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)
    GUI_Button(ENUM_BUTTON.BUTTON_FILTER_APPLY).Text = GLB_NLS_GUI_CONTROLS.GetString(8)

    GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Enabled = False
    GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Enabled = False
    GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Enabled = False
    GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False

    ' Date 
    Me.gb_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5683)                                                                             ' "Fechas"
    Me.opt_provision_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5512)                          'Provision
    Me.opt_period_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5513)                             'Periodo Provisionado
    Me.dtp_from.ShowCheckBox = False
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)                                           ' "Desde"
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)                                             ' "Hasta"
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Progressives Check List
    Me.uc_checked_list_progressive_catalog.GroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5217) ' "Progresivo"
    Me.uc_checked_list_progressive_catalog.ColumnWidth(1, 150)
    If Progressives.GetProgressives(_dt_progressives) Then
      Me.uc_checked_list_progressive_catalog.Add(_dt_progressives)
    End If
    Me.uc_checked_list_progressive_catalog.ResizeGrid()
    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)

    ' Option Group (Group By)
    Me.gb_group.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2651)                'Group by 
    Me.opt_group_progressive.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5217)   'Progressive
    Me.opt_group_provision.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5245)     'Provision

    ' Option Group (Detail)
    Me.gb_detail.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(474)                'Detail
    Me.opt_detail_total.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1340)        'Total
    Me.opt_detail_level.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5281)        'Level
    Me.opt_detail_terminal.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5364)     'Terminal

    ' Show Canceled Provisions
    Me.chk_showcanceledprovisions.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5535) 'Show canceled provisions

    ' Set filter default values
    Call SetDefaultValues()

    ' Grid
    Call GUI_StyleSheet()

  End Sub ' GUI_InitControls  

  ' PURPOSE: Manage buttons pressed.
  '
  '  PARAMS:
  '     - INPUT:
  '         - ButtonId: Id. of the button clicked.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)
    Dim _frm As frm_progressive_provision
    Dim _provision_id As Int64

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_SELECT

        Try
          Windows.Forms.Cursor.Current = Cursors.WaitCursor

          _frm = New frm_progressive_provision()

          If Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_PROGRESSIVE_PROVISION_ID).Value <> String.Empty Then
            _provision_id = Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_PROGRESSIVE_PROVISION_ID).Value
            _frm.ShowForEdit(_provision_id, Nothing)
          End If

        Finally
          Windows.Forms.Cursor.Current = Cursors.Default

        End Try

      Case Else

        MyBase.GUI_ButtonClick(ButtonId)

        If ButtonId = ENUM_BUTTON.BUTTON_FILTER_APPLY Then
          GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Enabled = (Not Me.opt_group_progressive.Checked AndAlso _
                                                           Me.Grid.NumRows > 0)
        End If

    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Enable button in selected row
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_RowSelectedEvent(ByVal SelectedRow As Integer)
    GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Enabled = False
    If SelectedRow >= 0 AndAlso Me.Grid.NumRows > 0 Then
      If Me.Grid.Cell(SelectedRow, GRID_COLUMN_PROGRESSIVE_PROVISION_ID).Value <> String.Empty Then
        GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Enabled = True
      End If
    End If
  End Sub

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  '
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _str_sql As System.Text.StringBuilder

    Dim a As New CLASS_GUI_USER
    Dim b As New CLASS_USER

    _str_sql = New System.Text.StringBuilder()

    _str_sql.AppendLine(" SELECT                                                                                  ")
    _str_sql.AppendLine("          P.PGS_PROGRESSIVE_ID                                                           ")
    _str_sql.AppendLine("        , P.PGS_NAME                                                                     ")
    _str_sql.AppendLine("        , SUB_GROUP.AMOUNT                                                               ")
    Select Case True
      Case Me.opt_group_progressive.Checked
        'NOTHING
        _str_sql.AppendLine("        , SUB_GROUP2.CAGE_AMOUNT                                                     ")
        _str_sql.AppendLine("        , NULL                                                                       ")
        _str_sql.AppendLine("        , NULL                                                                       ")
        _str_sql.AppendLine("        , NULL                                                                       ")
        _str_sql.AppendLine("        , NULL                                                                       ")
        _str_sql.AppendLine("        , NULL                                                                       ")
        _str_sql.AppendLine("        , NULL                                                                       ")

      Case Me.opt_group_provision.Checked
        _str_sql.AppendLine("        , PP.PGP_CAGE_AMOUNT                                                         ")
        _str_sql.AppendLine("        , PP.PGP_PROVISION_ID                                                        ")
        _str_sql.AppendLine("        , PP.PGP_CREATED                                                             ")
        _str_sql.AppendLine("        , PP.PGP_STATUS                                                              ")
        _str_sql.AppendLine("        , PP.PGP_HOUR_FROM                                                           ")
        _str_sql.AppendLine("        , PP.PGP_HOUR_TO                                                             ")
        _str_sql.AppendLine("        , G.GU_USERNAME                                                              ")

    End Select

    Select Case True
      Case Me.opt_detail_total.Checked
        'NOTHING
        _str_sql.AppendLine("        , NULL                                                                       ")
        _str_sql.AppendLine("        , NULL                                                                       ")
        _str_sql.AppendLine("        , NULL                                                                       ")
        _str_sql.AppendLine("        , NULL                                                                       ")

      Case Me.opt_detail_level.Checked
        _str_sql.AppendLine("        , NULL                                                                       ")
        _str_sql.AppendLine("        , NULL                                                                       ")
        _str_sql.AppendLine("        , NULL                                                                       ")
        _str_sql.AppendLine("        , PPL.PGL_NAME                                                               ")
      Case Me.opt_detail_terminal.Checked
        _str_sql.AppendLine("        , T.TE_TERMINAL_ID                                                           ")
        _str_sql.AppendLine("        , PV.PV_NAME                                                                 ")
        _str_sql.AppendLine("        , T.TE_NAME                                                                  ")
        _str_sql.AppendLine("        , NULL                                                                       ")
    End Select
    _str_sql.AppendLine("    FROM                                                                                 ")
    _str_sql.AppendLine("       (                                                                                 ")
    _str_sql.AppendLine("         SELECT                                                                          ")

    Select Case True
      Case Me.opt_group_progressive.Checked
        _str_sql.AppendLine("               PP_GROUP.PGP_PROGRESSIVE_ID                                           ")

        If Me.opt_detail_level.Checked Then
          _str_sql.AppendLine("         ,   PPL_GROUP.PPL_LEVEL_ID                                                ")
        End If
      Case Me.opt_group_provision.Checked
        Select Case True
          Case Me.opt_detail_total.Checked OrElse _
               Me.opt_detail_terminal.Checked
            _str_sql.AppendLine("           PP_GROUP.PGP_PROVISION_ID                                             ")
          Case Me.opt_detail_level.Checked
            _str_sql.AppendLine("           PP_GROUP.PGP_PROGRESSIVE_ID                                           ")
            _str_sql.AppendLine("         , PP_GROUP.PGP_PROVISION_ID                                             ")
            _str_sql.AppendLine("         , PPL_GROUP.PPL_LEVEL_ID                                                ")
        End Select
    End Select


    Select Case True
      Case Me.opt_detail_total.Checked OrElse _
           Me.opt_detail_level.Checked
        _str_sql.AppendLine("             , SUM(PPL_GROUP.PPL_AMOUNT) AS AMOUNT                                   ")
        _str_sql.AppendLine("       FROM                                                                          ")
        _str_sql.AppendLine("               PROGRESSIVES_PROVISIONS AS PP_GROUP                                   ")
        _str_sql.AppendLine(" INNER JOIN	  PROGRESSIVES_PROVISIONS_LEVELS AS PPL_GROUP                           ")
        _str_sql.AppendLine("					ON	  PPL_GROUP.PPL_PROVISION_ID = PP_GROUP.PGP_PROVISION_ID                ")

      Case Me.opt_detail_terminal.Checked
        _str_sql.AppendLine("             , T_GROUP.TE_TERMINAL_ID                                                ")
        _str_sql.AppendLine("             , SUM(PPT_GROUP.PPT_AMOUNT) AS AMOUNT                                   ")
        _str_sql.AppendLine("       FROM                                                                          ")
        _str_sql.AppendLine("  PROVIDERS AS PV_GROUP                                                              ")
        _str_sql.AppendLine(" INNER JOIN    TERMINALS T_GROUP                                                     ")
        _str_sql.AppendLine("         ON    PV_GROUP.PV_ID = T_GROUP.TE_PROV_ID                                   ")
        _str_sql.AppendLine(" INNER JOIN    PROGRESSIVES_PROVISIONS_TERMINALS PPT_GROUP                           ")
        _str_sql.AppendLine("         ON    T_GROUP.TE_TERMINAL_ID = PPT_GROUP.PPT_TERMINAL_ID                    ")
        _str_sql.AppendLine(" INNER JOIN    PROGRESSIVES_PROVISIONS PP_GROUP                                      ")
        _str_sql.AppendLine("         ON    PPT_GROUP.PPT_PROVISION_ID = PP_GROUP.PGP_PROVISION_ID                ")
    End Select

    ' Filter
    _str_sql.AppendLine("         WHERE                                                                           ")
    _str_sql.AppendLine(GetSqlWhereFilter())

    _str_sql.AppendLine("       GROUP BY                                                                          ")
    Select Case True
      Case Me.opt_group_progressive.Checked
        _str_sql.AppendLine("               PP_GROUP.PGP_PROGRESSIVE_ID                                           ")

        'LEVEL ONLY
        If Me.opt_detail_level.Checked Then
          _str_sql.AppendLine("          ,  PPL_GROUP.PPL_LEVEL_ID                                                ")
        End If

      Case Me.opt_group_provision.Checked
        Select Case True

          'TERMINAL & TOTAL
          Case Me.opt_detail_terminal.Checked OrElse Me.opt_detail_total.Checked
            _str_sql.AppendLine("           PP_GROUP.PGP_PROVISION_ID                                             ")

            'LEVEL
          Case Me.opt_detail_level.Checked
            _str_sql.AppendLine("           PP_GROUP.PGP_PROGRESSIVE_ID                                           ")
            _str_sql.AppendLine("        ,  PP_GROUP.PGP_PROVISION_ID                                             ")
            _str_sql.AppendLine("        ,  PPL_GROUP.PPL_LEVEL_ID                                                ")
        End Select

    End Select
    If Me.opt_detail_terminal.Checked Then
      _str_sql.AppendLine("             ,   T_GROUP.TE_TERMINAL_ID                                                ")
    End If

    _str_sql.AppendLine("   ) AS SUB_GROUP                                                                        ")

    If Me.opt_detail_terminal.Checked Then
      _str_sql.AppendLine("   INNER JOIN    TERMINALS T                                                           ")
      _str_sql.AppendLine("           ON    SUB_GROUP.TE_TERMINAL_ID = T.TE_TERMINAL_ID                           ")
      _str_sql.AppendLine("   INNER JOIN    PROVIDERS PV                                                          ")
      _str_sql.AppendLine("           ON    T.TE_PROV_ID = PV.PV_ID                                               ")

    End If

    Select Case True
      Case Me.opt_group_progressive.Checked
        _str_sql.AppendLine(" INNER JOIN 	PROGRESSIVES AS P                                                       ")
        _str_sql.AppendLine("         ON	P.PGS_PROGRESSIVE_ID = SUB_GROUP.PGP_PROGRESSIVE_ID                     ")
        _str_sql.AppendLine(" INNER JOIN                                                                          ")
        _str_sql.AppendLine("          (                                                                          ")
        _str_sql.AppendLine("                 SELECT                                                              ")
        _str_sql.AppendLine("                         PP_GROUP.PGP_PROGRESSIVE_ID                                 ")
        _str_sql.AppendLine("                       , SUM(PP_GROUP.PGP_CAGE_AMOUNT) AS CAGE_AMOUNT                ")
        _str_sql.AppendLine("                   FROM                                                              ")
        _str_sql.AppendLine("PROGRESSIVES_PROVISIONS PP_GROUP                                                     ")

        ' Filter
        _str_sql.AppendLine("                  WHERE                                                              ")
        _str_sql.AppendLine(GetSqlWhereFilter())

        _str_sql.AppendLine("               GROUP BY PP_GROUP.PGP_PROGRESSIVE_ID                                  ")
        _str_sql.AppendLine("   ) AS SUB_GROUP2                                                                   ")
        _str_sql.AppendLine("         ON P.PGS_PROGRESSIVE_ID = SUB_GROUP2.PGP_PROGRESSIVE_ID                     ")


      Case Me.opt_group_provision.Checked
        _str_sql.AppendLine(" INNER JOIN	PROGRESSIVES_PROVISIONS PP                                              ")
        _str_sql.AppendLine("		      ON	PP.PGP_PROVISION_ID = SUB_GROUP.PGP_PROVISION_ID                        ")
        _str_sql.AppendLine(" INNER JOIN	PROGRESSIVES P                                                          ")
        _str_sql.AppendLine("		      ON	P.PGS_PROGRESSIVE_ID = PP.PGP_PROGRESSIVE_ID                            ")
        _str_sql.AppendLine("	LEFT JOIN GUI_USERS G                                                               ")
        _str_sql.AppendLine("	        ON  G.GU_USER_ID = PP.PGP_GUI_USER_ID                                       ")

    End Select

    If Me.opt_detail_level.Checked Then
      _str_sql.AppendLine(" INNER JOIN		PROGRESSIVES_LEVELS AS PPL                                              ")
      _str_sql.AppendLine("         ON		PPL.PGL_LEVEL_ID = SUB_GROUP.PPL_LEVEL_ID                               ")
      _str_sql.AppendLine("        AND		PPL.PGL_PROGRESSIVE_ID = SUB_GROUP.PGP_PROGRESSIVE_ID                   ")
    End If

    _str_sql.AppendLine("     ORDER BY                                                                            ")
    Select Case True
      Case Me.opt_group_progressive.Checked
        _str_sql.AppendLine("             P.PGS_PROGRESSIVE_ID                                                    ")
      Case Me.opt_group_provision.Checked
        _str_sql.AppendLine("             P.PGS_PROGRESSIVE_ID                                                    ")
        _str_sql.AppendLine("          ,  PP.PGP_PROVISION_ID                                                     ")
        If Me.opt_detail_terminal.Checked Then
          _str_sql.AppendLine("        ,  T.TE_TERMINAL_ID                                                        ")
        End If
    End Select

    Return _str_sql.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _terminal_data As List(Of Object)

    ' Progressive Id
    If Not DbRow.IsNull(SQL_COL_PROGRESSIVE_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROGRESSIVE_ID).Value = DbRow.Value(SQL_COL_PROGRESSIVE_ID)
    End If

    ' Provision Id
    If Not DbRow.IsNull(SQL_COL_PROGRESSIVE_PROVISION_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROGRESSIVE_PROVISION_ID).Value = DbRow.Value(SQL_COL_PROGRESSIVE_PROVISION_ID)
    End If

    ' Terminal Id
    If Not DbRow.IsNull(SQL_COL_PROGRESSIVE_TERMINAL_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROGRESSIVE_TERMINAL_ID).Value = DbRow.Value(SQL_COL_PROGRESSIVE_TERMINAL_ID)
    End If

    ' Provision progressive Name
    If Not DbRow.IsNull(SQL_COL_PROGRESSIVE_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROGRESSIVE_NAME).Value = DbRow.Value(SQL_COL_PROGRESSIVE_NAME)
    End If

    ' Provision progressive Creation Date
    If Not DbRow.IsNull(SQL_COL_PROGRESSIVE_CREATION_DATE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROGRESSIVE_CREATION_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COL_PROGRESSIVE_CREATION_DATE), , _
                                                                            ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Provision progressive Hour From
    If Not DbRow.IsNull(SQL_COL_PROGRESSIVE_HOUR_FROM) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROGRESSIVE_HOUR_FROM).Value = GUI_FormatDate(DbRow.Value(SQL_COL_PROGRESSIVE_HOUR_FROM), , _
                                                                        ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Provision progressive Hour To
    If Not DbRow.IsNull(SQL_COL_PROGRESSIVE_HOUR_TO) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROGRESSIVE_HOUR_TO).Value = GUI_FormatDate(DbRow.Value(SQL_COL_PROGRESSIVE_HOUR_TO), , _
                                                                      ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' User who has created the provision 
    If Not DbRow.IsNull(SQL_COL_PROGRESSIVE_GUI_USER) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROGRESSIVE_GUI_USER).Value = DbRow.Value(SQL_COL_PROGRESSIVE_GUI_USER)

    End If

    ' Provision status
    If Not DbRow.IsNull(SQL_COL_PROGRESSIVE_PROVISION_STATUS) Then
      Select Case DbRow.Value(SQL_COL_PROGRESSIVE_PROVISION_STATUS)
        Case WSI.Common.Progressives.PROGRESSIVE_PROVISION_STATUS.ACTIVE
          Me.Grid.Cell(RowIndex, GRID_COLUMN_PROGRESSIVE_STATUS).Value = ""
        Case WSI.Common.Progressives.PROGRESSIVE_PROVISION_STATUS.CANCELED
          Me.Grid.Cell(RowIndex, GRID_COLUMN_PROGRESSIVE_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5536)
      End Select
    End If

    ' Terminal Report
    If Me.opt_detail_terminal.Checked Then
      If Not DbRow.IsNull(SQL_COL_PROGRESSIVE_TERMINAL_ID) Then
        _terminal_data = TerminalReport.GetReportDataList(DbRow.Value(SQL_COL_PROGRESSIVE_TERMINAL_ID), m_terminal_report_type)
        For _idx As Int32 = 0 To _terminal_data.Count - 1
          If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value Then
            Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
          End If
        Next
      End If
    End If

    ' Level Name
    If Not DbRow.IsNull(SQL_COL_PROGRESSIVE_LEVEL_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROGRESSIVE_LEVEL_NAME).Value = DbRow.Value(SQL_COL_PROGRESSIVE_LEVEL_NAME)
    End If

    ' Amount: Terminal|Level|Progressive
    If Not DbRow.IsNull(SQL_COL_PROGRESSIVE_AMOUNT) Then
      Select Case True
        Case Me.opt_detail_terminal.Checked
          Me.Grid.Cell(RowIndex, GRID_COLUMN_PROGRESSIVE_TERMINAL_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COL_PROGRESSIVE_AMOUNT), _
                  ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Case Me.opt_detail_level.Checked
          Me.Grid.Cell(RowIndex, GRID_COLUMN_PROGRESSIVE_LEVEL_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COL_PROGRESSIVE_AMOUNT), _
                  ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Case Me.opt_detail_total.Checked
          Me.Grid.Cell(RowIndex, GRID_COLUMN_PROGRESSIVE_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COL_PROGRESSIVE_AMOUNT), _
                  ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End Select
    End If

    ' Cage Amount
    Select Case True
      Case Me.opt_detail_total.Checked
        If Not DbRow.IsNull(SQL_COL_PROGRESSIVE_CAGE_AMOUNT) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_PROGRESSIVE_CAGE_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COL_PROGRESSIVE_CAGE_AMOUNT), _
                      ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If
    End Select

    Return True
  End Function ' GUI_SetupRow

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5683), m_progressive_date_type)                                      ' Catalog
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_AUDITOR.GetString(257), m_progressive_date_from) ' Period date from
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_AUDITOR.GetString(258), m_progressive_date_to)   ' Period date to
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5682), m_prevision)                                                  ' Options
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2651), m_progressive_group)                                          ' Group by
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(474), m_progressive_detail)                                          ' Detail
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5217), m_progressive_catalog)                                        ' Catalog

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportUpdateFilters()

    Dim _all_checked As Boolean
    m_progressive_date_type = ""
    m_progressive_date_from = ""
    m_progressive_date_to = ""
    m_progressive_group = ""
    m_progressive_detail = ""
    m_progressive_catalog = ""
    m_prevision = ""

    ' type date
    m_progressive_date_type = IIf(opt_period_date.Checked, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5513), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5512))

    ' Period date
    If dtp_to.Checked Then
      m_progressive_date_to = GUI_FormatDate(dtp_to.Value, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If
    m_progressive_date_from = GUI_FormatDate(dtp_from.Value, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Previsions
    m_prevision = IIf(chk_showcanceledprovisions.Checked, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5535), AUDIT_NONE_STRING)

    ' Progressive group
    m_progressive_group = IIf(opt_group_progressive.Checked, _
                              GLB_NLS_GUI_PLAYER_TRACKING.GetString(5217), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5245))

    'Detail
    If opt_detail_total.Checked Then
      m_progressive_detail = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1340)
    ElseIf opt_detail_level.Checked Then
      m_progressive_detail = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5281)
    ElseIf opt_detail_terminal.Checked Then
      m_progressive_detail = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5364)
    End If

    ' Progressive catalog
    _all_checked = True

    If uc_checked_list_progressive_catalog.SelectedIndexes.Length <> uc_checked_list_progressive_catalog.Count() Then
      _all_checked = False
    End If

    If _all_checked = True Then
      m_progressive_catalog = GLB_NLS_GUI_AUDITOR.GetString(263)
    Else
      m_progressive_catalog = uc_checked_list_progressive_catalog.SelectedValuesList()
    End If

  End Sub ' GUI_ReportUpdateFilters

  ' PURPOSE : Checks out the values of a db row before adding it to the grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the db row should be added to the grid
  '     - False: the db row should NOT be added to the grid
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As CLASS_DB_ROW) As Boolean

    If Not Me.opt_group_progressive.Checked OrElse Not Me.opt_detail_total.Checked Then

      'GROUP BY Progressive
      If Me.opt_group_progressive.Checked OrElse _
          (Me.opt_detail_total.Checked AndAlso _
           Me.opt_group_provision.Checked) Then

        If progressive_id <> DbRow.Value(SQL_COL_PROGRESSIVE_ID) AndAlso _
           progressive_id <> -1 Then

          Call DrawSubTotal(ENUM_SUBTOTAL_TYPE.PROGRESIVE)

          'Amount
          m_total_amount = m_total_amount + m_total_amount_progressive

          'Cage Amount
          Select Case True
            Case Me.opt_group_progressive.Checked
              m_total_cage_amount = m_total_cage_amount + m_cage_amount
            Case Me.opt_group_provision.Checked
              m_total_cage_amount = m_total_cage_amount + m_total_cage_amount_progressive
          End Select

          Call ResetCountersProgressive()
        End If

      Else

        'GROUP BY Provision

        If provision_id <> DbRow.Value(SQL_COL_PROGRESSIVE_PROVISION_ID) AndAlso _
           provision_id <> -1 Then

          Call DrawSubTotal(ENUM_SUBTOTAL_TYPE.PROVISION)
          Call ResetCountersProvision()

        End If
        If progressive_id <> DbRow.Value(SQL_COL_PROGRESSIVE_ID) AndAlso _
          progressive_id <> -1 Then

          Call DrawSubTotal(ENUM_SUBTOTAL_TYPE.PROGRESIVE)
          m_total_amount = m_total_amount + m_total_amount_progressive                'Amount

          'Cage Amount
          Select Case True
            Case Me.opt_group_progressive.Checked
              m_total_cage_amount = m_total_cage_amount + m_cage_amount
            Case Me.opt_group_provision.Checked
              m_total_cage_amount = m_total_cage_amount + m_total_cage_amount_progressive
          End Select

          Call ResetCountersProgressive()

        End If
      End If
    End If

    'Amount 
    m_total_amount_progressive = m_total_amount_progressive + DbRow.Value(SQL_COL_PROGRESSIVE_AMOUNT)
    m_total_amount_provision = m_total_amount_provision + DbRow.Value(SQL_COL_PROGRESSIVE_AMOUNT)

    'Cage Amount 
    m_cage_amount = DbRow.Value(SQL_COL_PROGRESSIVE_CAGE_AMOUNT)
    If Me.opt_group_provision.Checked AndAlso _
       (Me.opt_detail_level.Checked OrElse _
        Me.opt_detail_terminal.Checked) Then

      If provision_id <> DbRow.Value(SQL_COL_PROGRESSIVE_PROVISION_ID) OrElse _
         provision_id = -1 Then
        m_total_cage_amount_progressive = m_total_cage_amount_progressive + m_cage_amount
      End If
    Else
      m_total_cage_amount_progressive = m_total_cage_amount_progressive + m_cage_amount

    End If

    'Progressive Name
    progressive_name = DbRow.Value(SQL_COL_PROGRESSIVE_NAME)

    If Not DbRow.IsNull(SQL_COL_PROGRESSIVE_ID) Then
      progressive_id = DbRow.Value(SQL_COL_PROGRESSIVE_ID)
    End If

    If Not DbRow.IsNull(SQL_COL_PROGRESSIVE_PROVISION_ID) Then
      provision_id = DbRow.Value(SQL_COL_PROGRESSIVE_PROVISION_ID)
    End If

    Return True
  End Function ' GUI_CheckOutRowBeforeAdd

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()

    If Me.opt_group_provision.Checked AndAlso Me.Grid.NumRows > 0 AndAlso _
       (Me.opt_detail_level.Checked OrElse _
        Me.opt_detail_terminal.Checked) Then

      Call DrawSubTotal(ENUM_SUBTOTAL_TYPE.PROVISION)

    End If

    If Me.Grid.NumRows > 0 AndAlso Not (Me.opt_group_progressive.Checked AndAlso Me.opt_detail_total.Checked) Then
      Call DrawSubTotal(ENUM_SUBTOTAL_TYPE.PROGRESIVE)

    End If

    'Amount 
    m_total_amount = m_total_amount + m_total_amount_progressive

    'Cage Amount 
    If Me.opt_group_progressive.Checked AndAlso _
      (Me.opt_detail_level.Checked OrElse _
      Me.opt_detail_terminal.Checked) Then
      m_total_cage_amount = m_total_cage_amount + m_cage_amount
    Else
      m_total_cage_amount = m_total_cage_amount + m_total_cage_amount_progressive
    End If

    Call DrawSubTotal(ENUM_SUBTOTAL_TYPE.TOTAL)

  End Sub ' GUI_AfterLastRow

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE : Activated when a row of the grid is double clicked or selected, so it can be edited
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - None
  '
  '   NOTES :
  '
  Protected Overrides Sub GUI_EditSelectedItem()

    Dim idx_row As Short
    Dim item_id As Integer = 0
    Dim frm_progressive_provision As frm_progressive_provision

    ' Search the first row selected
    For idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(idx_row).IsSelected Then
        Exit For
      End If
    Next

    If idx_row = Me.Grid.NumRows Then
      Return
    End If

    'TODO: Call to the form edit provision

    ' Get the selected row's type
    If Me.Grid.Cell(idx_row, GRID_COLUMN_PROGRESSIVE_PROVISION_ID).Value <> String.Empty Then
      item_id = Me.Grid.Cell(idx_row, GRID_COLUMN_PROGRESSIVE_PROVISION_ID).Value

      frm_progressive_provision = New frm_progressive_provision

      'Call frm_progressive_provision.ShowEditItem(item_id, item_name)
      Call frm_progressive_provision.ShowDialog()
      frm_progressive_provision = Nothing
    Else
      Return
    End If

    Call Me.Grid.Focus()
  End Sub ' GUI_EditSelectedItem

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()

    progressive_id = -1
    provision_id = -1

    Call ResetCountersProgressive()
    Call ResetCountersProvision()
    Call ResetTotalCounters()
    Call GUI_StyleSheet()

  End Sub ' GUI_BeforeFirstRow

#End Region 'Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    Dim _terminal_columns As List(Of ColumnSettings)

    Call GridColumnReIndex()

    With Me.Grid
      .Clear()

      Call .Init(GRID_COLUMNS_PROGRESSIVE, GRID_HEADER_ROWS)

      ' Progressive ID
      .Column(GRID_COLUMN_PROGRESSIVE_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_PROGRESSIVE_ID).Header(1).Text = " ID "
      .Column(GRID_COLUMN_PROGRESSIVE_ID).Width = 0

      ' Provision ID
      .Column(GRID_COLUMN_PROGRESSIVE_PROVISION_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_PROGRESSIVE_PROVISION_ID).Header(1).Text = " ID "
      .Column(GRID_COLUMN_PROGRESSIVE_PROVISION_ID).Width = 0

      ' Terminal ID
      .Column(GRID_COLUMN_PROGRESSIVE_TERMINAL_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_PROGRESSIVE_TERMINAL_ID).Header(1).Text = " ID "
      .Column(GRID_COLUMN_PROGRESSIVE_TERMINAL_ID).Width = 0

      ' Provision progressive Creation Date
      If Not Me.opt_group_provision.Checked Then
        .Column(GRID_COLUMN_PROGRESSIVE_CREATION_DATE).Width = 0
        .Column(GRID_COLUMN_PROGRESSIVE_HOUR_FROM).Width = 0
        .Column(GRID_COLUMN_PROGRESSIVE_HOUR_TO).Width = 0
        .Column(GRID_COLUMN_PROGRESSIVE_GUI_USER).Width = 0
        .Column(GRID_COLUMN_PROGRESSIVE_STATUS).Width = 0
      Else

        'Creation Date
        .Column(GRID_COLUMN_PROGRESSIVE_CREATION_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5245)  'Provisi�n 
        .Column(GRID_COLUMN_PROGRESSIVE_CREATION_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3306)  'Creation Date
        .Column(GRID_COLUMN_PROGRESSIVE_CREATION_DATE).Width = GRID_WIDTH_CREATION_DATE_PROVISION
        .Column(GRID_COLUMN_PROGRESSIVE_CREATION_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

        'Provisioned Period (Hour From)
        .Column(GRID_COLUMN_PROGRESSIVE_HOUR_FROM).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5245)  'Provisi�n 
        .Column(GRID_COLUMN_PROGRESSIVE_HOUR_FROM).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202)
        .Column(GRID_COLUMN_PROGRESSIVE_HOUR_FROM).Width = GRID_WIDTH_HOUR_FROM
        .Column(GRID_COLUMN_PROGRESSIVE_HOUR_FROM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

        'Provisioned Period (Hour To)
        .Column(GRID_COLUMN_PROGRESSIVE_HOUR_TO).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5245)  'Provisi�n 
        .Column(GRID_COLUMN_PROGRESSIVE_HOUR_TO).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203)
        .Column(GRID_COLUMN_PROGRESSIVE_HOUR_TO).Width = GRID_WIDTH_HOUR_TO
        .Column(GRID_COLUMN_PROGRESSIVE_HOUR_TO).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

        'User who has provisioned 
        .Column(GRID_COLUMN_PROGRESSIVE_GUI_USER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5245)          'Provisi�n 
        .Column(GRID_COLUMN_PROGRESSIVE_GUI_USER).Header(1).Text = GLB_NLS_GUI_CONTROLS.GetString(260)                  'User
        .Column(GRID_COLUMN_PROGRESSIVE_GUI_USER).Width = GRID_WIDTH_GUI_USER
        .Column(GRID_COLUMN_PROGRESSIVE_GUI_USER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

        'Provision status
        .Column(GRID_COLUMN_PROGRESSIVE_STATUS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5245)        'Provisi�n 
        .Column(GRID_COLUMN_PROGRESSIVE_STATUS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5536) & "."  'Canceled Provision
        .Column(GRID_COLUMN_PROGRESSIVE_STATUS).Width = GRID_WIDTH_STATUS_PROVISION
        .Column(GRID_COLUMN_PROGRESSIVE_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      End If

      ' Provision progressive Name
      .Column(GRID_COLUMN_PROGRESSIVE_NAME).Header(0).Text = ""
      .Column(GRID_COLUMN_PROGRESSIVE_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5217)  'Progressive
      .Column(GRID_COLUMN_PROGRESSIVE_NAME).Width = IIf(Me.opt_group_progressive.Checked = True, GRID_WIDTH_PROGRESSIVE_GROUP_PROGRESSIVE, GRID_WIDTH_PROGRESSIVE_GROUP_PROVISION)
      .Column(GRID_COLUMN_PROGRESSIVE_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      '  Terminal Report
      _terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)
      For _idx As Int32 = 0 To _terminal_columns.Count - 1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(232)
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = _terminal_columns(_idx).Header1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = IIf(Me.opt_detail_terminal.Checked, _terminal_columns(_idx).Width, 0)
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = _terminal_columns(_idx).Alignment
      Next

      'Level Name
      If Not Me.opt_detail_level.Checked Then
        .Column(GRID_COLUMN_PROGRESSIVE_LEVEL_NAME).Width = 0
      Else
        .Column(GRID_COLUMN_PROGRESSIVE_LEVEL_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5281)    'Level 
        .Column(GRID_COLUMN_PROGRESSIVE_LEVEL_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3419)    'Nombre
        .Column(GRID_COLUMN_PROGRESSIVE_LEVEL_NAME).Width = IIf(Me.opt_group_progressive.Checked = True, GRID_WIDTH_AMOUNT_GROUP_PROGRESSIVE, GRID_WIDTH_AMOUNT_GROUP_PROVISION)
        .Column(GRID_COLUMN_PROGRESSIVE_LEVEL_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      End If

      ' Amount (Progressive|Terminal|Level)
      Select Case True
        Case Me.opt_detail_total.Checked
          .Column(GRID_COLUMN_PROGRESSIVE_TERMINAL_AMOUNT).Width = 0
          .Column(GRID_COLUMN_PROGRESSIVE_LEVEL_AMOUNT).Width = 0
          .Column(GRID_COLUMN_PROGRESSIVE_AMOUNT).Header(0).Text = ""
          .Column(GRID_COLUMN_PROGRESSIVE_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5214) 'Amount
          .Column(GRID_COLUMN_PROGRESSIVE_AMOUNT).Width = IIf(Me.opt_group_progressive.Checked = True, GRID_WIDTH_AMOUNT_GROUP_PROGRESSIVE, GRID_WIDTH_AMOUNT_GROUP_PROVISION)
          .Column(GRID_COLUMN_PROGRESSIVE_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        Case Me.opt_detail_terminal.Checked
          .Column(GRID_COLUMN_PROGRESSIVE_AMOUNT).Width = 0
          .Column(GRID_COLUMN_PROGRESSIVE_LEVEL_AMOUNT).Width = 0
          .Column(GRID_COLUMN_PROGRESSIVE_TERMINAL_AMOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(232) 'Terminal
          .Column(GRID_COLUMN_PROGRESSIVE_TERMINAL_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5214) 'Amount
          .Column(GRID_COLUMN_PROGRESSIVE_TERMINAL_AMOUNT).Width = IIf(Me.opt_group_progressive.Checked = True, GRID_WIDTH_AMOUNT_GROUP_PROGRESSIVE, GRID_WIDTH_AMOUNT_GROUP_PROVISION)
          .Column(GRID_COLUMN_PROGRESSIVE_TERMINAL_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        Case Me.opt_detail_level.Checked
          .Column(GRID_COLUMN_PROGRESSIVE_AMOUNT).Width = 0
          .Column(GRID_COLUMN_PROGRESSIVE_TERMINAL_AMOUNT).Width = 0
          .Column(GRID_COLUMN_PROGRESSIVE_LEVEL_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(443)   'Nivel
          .Column(GRID_COLUMN_PROGRESSIVE_LEVEL_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5214)  'Amount
          .Column(GRID_COLUMN_PROGRESSIVE_LEVEL_AMOUNT).Width = IIf(Me.opt_group_progressive.Checked = True, GRID_WIDTH_AMOUNT_GROUP_PROGRESSIVE, GRID_WIDTH_AMOUNT_GROUP_PROVISION)
          .Column(GRID_COLUMN_PROGRESSIVE_LEVEL_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      End Select

      ' Cage Amount 
      If Cage.IsCageEnabled() Then
        .Column(GRID_COLUMN_PROGRESSIVE_CAGE_AMOUNT).Header(0).Text = ""
        .Column(GRID_COLUMN_PROGRESSIVE_CAGE_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5515)       'Cage Amount
        .Column(GRID_COLUMN_PROGRESSIVE_CAGE_AMOUNT).Width = GRID_WIDTH_AMOUNT_CAGE
        .Column(GRID_COLUMN_PROGRESSIVE_CAGE_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      Else
        .Column(GRID_COLUMN_PROGRESSIVE_CAGE_AMOUNT).Width = 0
      End If

      .Sortable = False
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    ' Dates
    Me.opt_provision_date.Checked = True
    Me.dtp_from.Value = WSI.Common.Misc.TodayOpening().AddDays(-1)
    Me.dtp_to.Value = New DateTime(Now.Year, Now.Month, Now.Day, WSI.Common.Misc.TodayOpening().Hour, WSI.Common.Misc.TodayOpening().Minute, WSI.Common.Misc.TodayOpening().Second)
    Me.dtp_to.Checked = False

    ' Filter Group & Detail
    Me.opt_group_progressive.Checked = True
    Me.opt_detail_total.Checked = True

    'Filter Progressives
    Me.uc_checked_list_progressive_catalog.btn_check_all.PerformClick()
    Me.chk_terminal_location.Checked = False
    Me.chk_terminal_location.Enabled = False
    Me.chk_showcanceledprovisions.Checked = False

  End Sub ' SetDefaultValues

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the
  '          main SQL Query.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhereFilter() As String

    Dim _str_where As System.Text.StringBuilder

    _str_where = New System.Text.StringBuilder()

    ' Filter Dates
    If Me.gb_date.Enabled Then
      If Me.opt_period_date.Checked = True Then
        If Me.dtp_to.Checked = True Then
          _str_where.AppendLine(" PP_GROUP.PGP_HOUR_FROM    < " & GUI_FormatDateDB(dtp_to.Value) & " ")
          _str_where.AppendLine(" AND PP_GROUP.PGP_HOUR_TO  > " & GUI_FormatDateDB(dtp_from.Value) & " ")
        Else
          _str_where.AppendLine(" (PP_GROUP.PGP_HOUR_FROM   > " & GUI_FormatDateDB(dtp_from.Value) & " ")
          _str_where.AppendLine(" OR PP_GROUP.PGP_HOUR_TO   > " & GUI_FormatDateDB(dtp_from.Value) & " )")
        End If
      Else
        If Me.dtp_to.Checked = True Then
          _str_where.AppendLine(" PP_GROUP.PGP_CREATED      >= " & GUI_FormatDateDB(dtp_from.Value) & " ")
          _str_where.AppendLine(" AND PP_GROUP.PGP_CREATED  <= " & GUI_FormatDateDB(dtp_to.Value) & " ")
        Else
          _str_where.AppendLine(" PP_GROUP.PGP_CREATED >= " & GUI_FormatDateDB(dtp_from.Value) & " ")
        End If
      End If
    End If

    ' Filter Progressives
    If Me.uc_checked_list_progressive_catalog.SelectedIndexesList <> "" Then
      _str_where.AppendLine(" AND PP_GROUP.PGP_PROGRESSIVE_ID IN(" & Me.uc_checked_list_progressive_catalog.SelectedIndexesList & ") ")
    End If

    ''
    If chk_showcanceledprovisions.Checked Then
      _str_where.AppendLine(" AND PP_GROUP.PGP_STATUS IN(0,1) ")
    Else
      _str_where.AppendLine(" AND PP_GROUP.PGP_STATUS = 0 ")
    End If

    Return _str_where.ToString()

  End Function ' GetSqlWhereFilter

  ' PURPOSE: Write Subtotals Amounts in the Grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub DrawSubTotal(ByVal type_subtotal As ENUM_SUBTOTAL_TYPE)
    Dim _idx_row As Integer
    Dim _str_total As String = String.Empty
    Dim _provision_id As Long = -1
    Dim _amount As Decimal
    Dim _cage_amount As Decimal
    Dim _color As ENUM_GUI_COLOR

    Select Case type_subtotal
      Case ENUM_SUBTOTAL_TYPE.PROGRESIVE
        _str_total = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1046) & " " & progressive_name & ":" '"SubTotal <progressive name>: " 
        _amount = m_total_amount_progressive                'Amount

        Select Case True                                    'Cage Amount
          Case Me.opt_group_progressive.Checked
            _cage_amount = m_cage_amount
          Case Me.opt_group_provision.Checked
            _cage_amount = m_total_cage_amount_progressive
        End Select
        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01

      Case ENUM_SUBTOTAL_TYPE.PROVISION
        _str_total = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1046) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5245) & ":" '"SubTotal Provision: " 
        _provision_id = provision_id
        _amount = m_total_amount_provision                  'Amount

        Select Case True                                    'Cage Amount
          Case Me.opt_group_progressive.Checked
            _cage_amount = m_cage_amount
          Case Me.opt_group_provision.Checked
            Select Case True
              Case Me.opt_detail_level.Checked, _
                   Me.opt_detail_terminal.Checked
                _cage_amount = m_cage_amount
              Case Me.opt_detail_total.Checked
                _cage_amount = m_total_cage_amount_progressive
            End Select
        End Select

        ''_color = ENUM_GUI_COLOR.GUI_COLOR_OCHRE_00
        _color = ENUM_GUI_COLOR.GUI_COLOR_GREY_01

      Case ENUM_SUBTOTAL_TYPE.TOTAL
        _str_total = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2838).ToUpper & ":"  '"TOTAL" 
        _amount = m_total_amount
        _cage_amount = m_total_cage_amount
        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
    End Select

    Me.Grid.AddRow()
    _idx_row = Me.Grid.NumRows - 1

    If Me.opt_group_provision.Checked AndAlso _provision_id <> -1 Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_PROGRESSIVE_PROVISION_ID).Value = _provision_id
    End If

    ' Set Text: 'Total Progressive <n> or SubTotal Provision <n>
    Select Case True
      Case Me.opt_group_progressive.Checked
        Me.Grid.Cell(_idx_row, GRID_COLUMN_PROGRESSIVE_NAME).Value = _str_total
      Case Me.opt_group_provision.Checked
        Me.Grid.Cell(_idx_row, GRID_COLUMN_PROGRESSIVE_CREATION_DATE).Value = _str_total

    End Select

    'Amount <n> in grid column <n>
    Select Case True
      Case Me.opt_detail_total.Checked
        Me.Grid.Cell(_idx_row, GRID_COLUMN_PROGRESSIVE_AMOUNT).Value = GUI_FormatCurrency(_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      Case Me.opt_detail_level.Checked
        Me.Grid.Cell(_idx_row, GRID_COLUMN_PROGRESSIVE_LEVEL_AMOUNT).Value = GUI_FormatCurrency(_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      Case Me.opt_detail_terminal.Checked
        Me.Grid.Cell(_idx_row, GRID_COLUMN_PROGRESSIVE_TERMINAL_AMOUNT).Value = GUI_FormatCurrency(_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    End Select

    'Cage Amount
    Me.Grid.Cell(_idx_row, GRID_COLUMN_PROGRESSIVE_CAGE_AMOUNT).Value = GUI_FormatCurrency(_cage_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    Me.Grid.Row(_idx_row).BackColor = GetColor(_color)

  End Sub ' DrawSubTotal

  ' PURPOSE: Reset Counters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ResetCountersProgressive()
    m_total_amount_progressive = 0
    m_total_cage_amount_progressive = 0
  End Sub 'ResetCountersProgressive

  ' PURPOSE: Reset Counters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ResetCountersProvision()
    m_total_amount_provision = 0
  End Sub 'ResetCountersProvision

  ' PURPOSE: Reset Counters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ResetTotalCounters()
    m_total_amount = 0
    m_total_cage_amount = 0
  End Sub 'ResetTotalCounters

  ' PURPOSE: Set Addittional info when show detail terminal (Area, Bank, etc.)
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GridColumnReIndex()
    TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(m_terminal_report_type)

    GRID_COLUMN_PROGRESSIVE_ID = 0
    GRID_COLUMN_PROGRESSIVE_PROVISION_ID = 1
    GRID_COLUMN_PROGRESSIVE_TERMINAL_ID = 2
    GRID_COLUMN_PROGRESSIVE_CREATION_DATE = 3
    GRID_COLUMN_PROGRESSIVE_HOUR_FROM = 4
    GRID_COLUMN_PROGRESSIVE_HOUR_TO = 5

    GRID_COLUMN_PROGRESSIVE_GUI_USER = 6


    GRID_COLUMN_PROGRESSIVE_STATUS = 7
    GRID_COLUMN_PROGRESSIVE_NAME = 8
    GRID_COLUMN_PROGRESSIVE_AMOUNT = 9
    GRID_INIT_TERMINAL_DATA = 10

    GRID_COLUMN_PROGRESSIVE_TERMINAL_AMOUNT = TERMINAL_DATA_COLUMNS + 10
    GRID_COLUMN_PROGRESSIVE_LEVEL_NAME = TERMINAL_DATA_COLUMNS + 11
    GRID_COLUMN_PROGRESSIVE_LEVEL_AMOUNT = TERMINAL_DATA_COLUMNS + 12
    GRID_COLUMN_PROGRESSIVE_CAGE_AMOUNT = TERMINAL_DATA_COLUMNS + 13

    GRID_COLUMNS_PROGRESSIVE = TERMINAL_DATA_COLUMNS + 14

  End Sub

#End Region 'Private Functions

#Region " Events "

  Private Sub chk_terminal_location_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminal_location.CheckedChanged
    If chk_terminal_location.Checked Then
      m_terminal_report_type = ReportType.Provider + ReportType.Location
    Else
      m_terminal_report_type = ReportType.Provider
    End If

  End Sub

  Private Sub opt_detail_terminal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_detail_terminal.CheckedChanged
    chk_terminal_location.Enabled = Me.opt_detail_terminal.Checked
  End Sub
#End Region

End Class