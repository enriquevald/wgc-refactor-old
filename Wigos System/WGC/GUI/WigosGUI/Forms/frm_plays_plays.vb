'-------------------------------------------------------------------
' Copyright � 2007 Win Systems Ltd. 
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_plays_plays
' DESCRIPTION:   This screen allows to view the plays between:
'                           - two dates 
'                           - for all terminals or one terminal
'                   
' AUTHOR:        Merc� Torrelles Minguella
' CREATION DATE: 13-APR-2007
'
' REVISION HISTORY:
'
' Date         Author     Description
' -----------  ------     -----------------------------------------------
' 25-APR-2007  MTM        Initial version
' 04-JUN-2012  JAB        Eliminate top clause of sql query
' 12-JUN-2012  JAB        Fixed bug in sql query (GUI_FilterGetSqlQuery, GetSqlWhere)
' 12-FEB-2013  JMM        uc_account_sel.ValidateFormat added on FilterCheck
' 29-APR-2013  DMR        Fixed bug #752: Correct Final Balance Values marked as mismatched
' 25-MAY-2013  JCA        Change to use new table providers_games
' 30-JUL-2013  JCA        Fixed bug WIG-88: Not shows the plays because incorrect use of PLAYS_V view.
' 25-NOV-2013  QMP & RMS  Updated SQL Query and ShowForEdit()
' 12-DEC-2013  QMP        Fixed bug 486: 3GS plays are out of balance
' 23-DEC-2013  AMF        Virtual account don't show track data
' 27-JUN-2014  LEM & RCI    When card is recycled, the trackdata is shown empty and must be CardNumber.INVALID_TRACKDATA.
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports WSI.Common
Imports System.Data.SqlClient
Imports System.Text

Public Class frm_plays_plays
  Inherits frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents lb_session_id As System.Windows.Forms.Label
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.gb_date = New System.Windows.Forms.GroupBox
    Me.dtp_to = New GUI_Controls.uc_date_picker
    Me.dtp_from = New GUI_Controls.uc_date_picker
    Me.lb_session_id = New System.Windows.Forms.Label
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.lb_session_id)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Size = New System.Drawing.Size(1175, 190)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lb_session_id, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 194)
    Me.panel_data.Size = New System.Drawing.Size(1175, 514)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1169, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1169, 4)
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(6, 6)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(237, 72)
    Me.gb_date.TabIndex = 1
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 38)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(222, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 14)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(222, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'lb_session_id
    '
    Me.lb_session_id.AutoSize = True
    Me.lb_session_id.Location = New System.Drawing.Point(34, 93)
    Me.lb_session_id.Name = "lb_session_id"
    Me.lb_session_id.Size = New System.Drawing.Size(0, 13)
    Me.lb_session_id.TabIndex = 6
    '
    'frm_plays_plays
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(1183, 712)
    Me.Name = "frm_plays_plays"
    Me.Text = "frm_plays_plays"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Constants "

  Private Const SQL_COLUMN_DATETIME As Integer = 0
  Private Const SQL_COLUMN_TERMINAL As Integer = 1
  Private Const SQL_COLUMN_GAME As Integer = 2
  Private Const SQL_COLUMN_CARD_NUMBER As Integer = 3
  Private Const SQL_COLUMN_ACCOUNT_TYPE As Integer = 4
  Private Const SQL_COLUMN_CARD_TRACK_DATA As Integer = 5
  Private Const SQL_COLUMN_PLAYED_AMOUNT As Integer = 6
  Private Const SQL_COLUMN_WON_AMOUNT As Integer = 7
  Private Const SQL_COLUMN_INITIAL_BALANCE As Integer = 8
  Private Const SQL_COLUMN_FINAL_BALANCE As Integer = 9
  Private Const SQL_COLUMN_CARD_PLAYER As Integer = 10

  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_DATETIME As Integer = 1
  Private Const GRID_COLUMN_TERMINAL As Integer = 2
  Private Const GRID_COLUMN_GAME As Integer = 3
  Private Const GRID_COLUMN_CARD_NUMBER As Integer = 4
  Private Const GRID_COLUMN_ACCOUNT_TYPE As Integer = 5
  Private Const GRID_COLUMN_CARD_TRACK_DATA As Integer = 6
  Private Const GRID_COLUMN_PLAYED_AMOUNT As Integer = 7
  Private Const GRID_COLUMN_WON_AMOUNT As Integer = 8
  Private Const GRID_COLUMN_INITIAL_BALANCE As Integer = 9
  Private Const GRID_COLUMN_FINAL_BALANCE As Integer = 10

  Private Const GRID_COLUMNS As Integer = 11
  Private Const GRID_HEADER_ROWS As Integer = 1

  ' Stand Alone mode
  Private Const GRID_COLUMN_CARD_PLAYER_SA As Integer = 7
  Private Const GRID_COLUMN_PLAYED_AMOUNT_SA As Integer = 8
  Private Const GRID_COLUMN_WON_AMOUNT_SA As Integer = 9

  Private Const GRID_COLUMNS_SA As Integer = 10

  'Width
  Private Const GRID_WIDTH_DATE As Integer = 2190
  Private Const GRID_WIDTH_AMOUNT As Integer = 1475
  Private Const GRID_WIDTH_COUNT As Integer = 1300
  Private Const GRID_WIDTH_TERMINAL As Integer = 2000
  Private Const GRID_WIDTH_STATUS As Integer = 1200

  ' For 3GS
  Private Const SQL_COLUMN_A3GS_EXECUTION As Integer = 0
  Private Const SQL_COLUMN_A3GS_MACHINE_NUMBER As Integer = 1
  Private Const SQL_COLUMN_A3GS_PROCEDURE As Integer = 2
  Private Const SQL_COLUMN_A3GS_ACCOUNT_ID As Integer = 3
  Private Const SQL_COLUMN_A3GS_VENDOR_ID As Integer = 4
  Private Const SQL_COLUMN_A3GS_SERIAL_NUMBER As Integer = 5
  Private Const SQL_COLUMN_A3GS_SESSION_ID As Integer = 6
  Private Const SQL_COLUMN_A3GS_BALANCE As Integer = 7
  Private Const SQL_COLUMN_A3GS_STATUS_CODE As Integer = 8
  Private Const SQL_COLUMN_A3GS_INPUT As Integer = 9
  Private Const SQL_COLUMN_A3GS_OUTPUT As Integer = 10

  Private Const GRID_COLUMN_A3GS_EXECUTION As Integer = 1
  Private Const GRID_COLUMN_A3GS_PROCEDURE As Integer = 2
  Private Const GRID_COLUMN_A3GS_STATUS As Integer = 3
  Private Const GRID_COLUMN_A3GS_INITIAL_BALANCE As Integer = 4
  Private Const GRID_COLUMN_A3GS_INPUT_PLAYED_AMOUNT As Integer = 5
  Private Const GRID_COLUMN_A3GS_INPUT_WON_AMOUNT As Integer = 6
  Private Const GRID_COLUMN_A3GS_INPUT_FINAL_BALANCE As Integer = 7
  Private Const GRID_COLUMN_A3GS_INPUT_FINAL_BALANCE_CALCULATE As Integer = 8
  Private Const GRID_COLUMN_A3GS_INPUT_PLAYED_PLAYS As Integer = 9
  Private Const GRID_COLUMN_A3GS_INPUT_WON_PLAYS As Integer = 10
  Private Const GRID_COLUMN_A3GS_STATUS_TEXT As Integer = 11

  Private Const GRID_COLUMNS_A3GS As Integer = 12
  Private Const GRID_HEADER_ROWS_A3GS As Integer = 2
  ' End for 3GS

#End Region ' Constants

  Private Structure TYPE_DATA_AUDIT
    Dim initial_balance As Double
    Dim played_amount As Double
    Dim won_amount As Double
    Dim final_balance As Double
    Dim final_balance_calculate As Double
    Dim played_plays As Integer
    Dim won_plays As Integer
    Dim status_text As String
  End Structure ' TYPE_DATA_AUDIT

#Region " Members "

  Private m_refreshing_grid As Boolean

  ' For report filters 
  Private m_date_from As String
  Private m_date_to As String

  Private m_session_id As Integer
  Private m_session_date As String
  Private m_play_stand_alone As Boolean

  ' For amount grid
  Dim total_played_amount As Double
  Dim total_won_amount As Double

  ' For 3GS display
  Dim m_terminal_type As String
  Dim m_initial_balance As Double

  Dim m_parse_data As TYPE_DATA_AUDIT

#End Region ' Members

#Region " OVERRIDES "

  Protected Overrides Function GUI_MaxRows() As Integer
    Return 10000
  End Function ' GUI_MaxRows

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_PLAYS_PLAYS

    ' TJG 01-FEB-2005
    ' Set the form icon from the embedded resource (ICO file must be built in the project as "Embedded Resource")
    ' Call could be made as GetManifestResourceStream("GUI_Auditor.GUI_Auditor.ico"))
    'Me.Icon = New Icon(System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream(Me.GetType(), "WigosGUI.ico"))

    '------------------------------------------------
    'XVV 13/04/2007
    'Call Base Form proc
    Call MyBase.GUI_SetFormId()
    '------------------------------------------------

  End Sub ' GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_STATISTICS.GetString(354)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    ' Date
    Me.gb_date.Text = GLB_NLS_GUI_ALARMS.GetString(441)
    Me.dtp_from.Text = GLB_NLS_GUI_AUDITOR.GetString(257)
    Me.dtp_to.Text = GLB_NLS_GUI_AUDITOR.GetString(258)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    If m_session_id <> 0 Then
      Me.lb_session_id.Text = GLB_NLS_GUI_STATISTICS.GetString(412) & " " & m_session_date
    End If

    ' Check Play.StandAlone active
    Me.m_play_stand_alone = GeneralParam.GetBoolean("Play", "StandAlone")

    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

    ' 5 minutes timeout
    Call GUI_SetQueryTimeout(300)

  End Sub ' GUI_InitControls

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  Protected Overrides Sub GUI_BeforeFirstRow()

    total_played_amount = 0
    total_won_amount = 0

  End Sub ' GUI_BeforeFirsRow

  Protected Overrides Sub GUI_AfterLastRow()

    If (m_terminal_type = WSI.Common.TerminalTypes.T3GS) Then
      Exit Sub
    End If

    Me.Grid.AddRow()

    Dim idx_row As Integer = Me.Grid.NumRows - 1
    Dim ini_balance As Double = 0
    Dim fin_balance As Double = 0

    ' Dates
    Me.Grid.Cell(idx_row, GRID_COLUMN_DATETIME).Value = GLB_NLS_GUI_INVOICING.GetString(205)

    ' Terminal
    Me.Grid.Cell(idx_row, GRID_COLUMN_TERMINAL).Value = ""

    If (m_play_stand_alone) Then

      ' Played Amount
      Me.Grid.Cell(idx_row, GRID_COLUMN_PLAYED_AMOUNT_SA).Value = GUI_FormatCurrency(total_played_amount, _
                                                                                  ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      ' Won Amount
      Me.Grid.Cell(idx_row, GRID_COLUMN_WON_AMOUNT_SA).Value = GUI_FormatCurrency(total_won_amount, _
                                                                                  ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    Else

      ' Played Amount
      Me.Grid.Cell(idx_row, GRID_COLUMN_PLAYED_AMOUNT).Value = GUI_FormatCurrency(total_played_amount, _
                                                                                  ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      ' Won Amount
      Me.Grid.Cell(idx_row, GRID_COLUMN_WON_AMOUNT).Value = GUI_FormatCurrency(total_won_amount, _
                                                                                  ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    End If

    Me.Grid.Row(idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

  End Sub ' GUI_AfterLastRow

  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim str_sql As String
    Dim _sb As StringBuilder

    _sb = New StringBuilder()

    If m_terminal_type <> "" Then
      str_sql = "SELECT   A3GS_EXECUTION " & _
          "       , A3GS_MACHINE_NUMBER " & _
          "       , REPLACE(A3GS_PROCEDURE, 'zsp_Session', '') AS A3GS_PROCEDURE " & _
          "       , A3GS_ACCOUNT_ID " & _
          "       , A3GS_VENDOR_ID " & _
          "       , ISNULL(A3GS_SERIAL_NUMBER, '') " & _
          "       , A3GS_SESSION_ID " & _
          "       , A3GS_BALANCE " & _
          "       , A3GS_STATUS_CODE " & _
          "       , ISNULL(A3GS_INPUT, '') " & _
          "       , ISNULL(A3GS_OUTPUT, '') " & _
          "  FROM   AUDIT_3GS WITH( INDEX(IX_audit_3gs_session_id_3gs_id)) " & _
          " WHERE   A3GS_SESSION_ID = '" & m_session_id & "' " & _
          " ORDER BY A3GS_EXECUTION DESC "
    Else
      _sb.AppendLine("SELECT   PL_DATETIME        ")
      _sb.AppendLine("       , TE_NAME            ")
      _sb.AppendLine("       , GM_NAME            ")
      _sb.AppendLine("       , AC_ACCOUNT_ID      ")
      _sb.AppendLine("       , AC_TYPE            ")
      _sb.AppendLine("       , AC_TRACK_DATA      ")
      _sb.AppendLine("       , PL_PLAYED_AMOUNT   ")
      _sb.AppendLine("       , PL_WON_AMOUNT      ")
      _sb.AppendLine("       , PL_INITIAL_BALANCE ")
      _sb.AppendLine("       , PL_FINAL_BALANCE   ")
      _sb.AppendLine("       , AC_HOLDER_NAME     ")
      _sb.AppendLine("  FROM ( SELECT             ")
      _sb.AppendLine(GetSqlWhere())
      _sb.AppendLine(" ) X LEFT OUTER JOIN TERMINALS AS T ON PL_TERMINAL_ID = T.TE_TERMINAL_ID ")
      _sb.AppendLine(" ORDER BY PL_DATETIME DESC ")
      str_sql = _sb.ToString()
    End If

    Return str_sql

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _played_amount As Double
    Dim _won_amount As Double
    Dim _initial_balance As Double
    Dim _final_balance As Double
    Dim _track_data As String
    Dim _account_type As AccountType

    _played_amount = 0
    _won_amount = 0
    _initial_balance = 0
    _final_balance = 0

    If (m_terminal_type <> WSI.Common.TerminalTypes.T3GS) Then


      ' Datetime
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATETIME), _
                                                                          ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                          ENUM_FORMAT_TIME.FORMAT_HHMMSS)

      ' Terminal
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL).Value = DbRow.Value(SQL_COLUMN_TERMINAL)

      ' Game
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME).Value = DbRow.Value(SQL_COLUMN_GAME)

      ' Card number
      If Not DbRow.IsNull(SQL_COLUMN_CARD_NUMBER) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_NUMBER).Value = DbRow.Value(SQL_COLUMN_CARD_NUMBER)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_NUMBER).Value = GLB_NLS_GUI_STATISTICS.GetString(314)
      End If

      ' Account Type
      _account_type = CType(DbRow.Value(SQL_COLUMN_ACCOUNT_TYPE), AccountType)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_TYPE).Value = _account_type

      ' Card Track Data
      _track_data = ""
      If Not DbRow.IsNull(SQL_COLUMN_CARD_TRACK_DATA) Then
        CardNumber.VisibleTrackData(_track_data, DbRow.Value(SQL_COLUMN_CARD_TRACK_DATA), MAGNETIC_CARD_TYPES.CARD_TYPE_PLAYER, _account_type)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_TRACK_DATA).Value = _track_data
      End If

      If (m_play_stand_alone) Then

        ' Player
        If Not DbRow.IsNull(SQL_COLUMN_CARD_PLAYER) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_PLAYER_SA).Value = DbRow.Value(SQL_COLUMN_CARD_PLAYER)
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_PLAYER_SA).Value = ""
        End If

        ' PlayedAmount
        _played_amount = DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT)

        Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED_AMOUNT_SA).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT), _
                                                                                     ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        total_played_amount += DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT)

        ' WonAmount
        _won_amount = DbRow.Value(SQL_COLUMN_WON_AMOUNT)

        Me.Grid.Cell(RowIndex, GRID_COLUMN_WON_AMOUNT_SA).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_WON_AMOUNT), _
                                                                                  ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        total_won_amount += DbRow.Value(SQL_COLUMN_WON_AMOUNT)

        Return True
      End If

      ' PlayedAmount
      _played_amount = DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT)

      Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT), _
                                                                                   ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      total_played_amount += DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT)

      ' WonAmount
      _won_amount = DbRow.Value(SQL_COLUMN_WON_AMOUNT)

      Me.Grid.Cell(RowIndex, GRID_COLUMN_WON_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_WON_AMOUNT), _
                                                                                ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      total_won_amount += DbRow.Value(SQL_COLUMN_WON_AMOUNT)

      ' Initial Balance
      _initial_balance = DbRow.Value(SQL_COLUMN_INITIAL_BALANCE)

      Me.Grid.Cell(RowIndex, GRID_COLUMN_INITIAL_BALANCE).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_INITIAL_BALANCE), _
                                                                                     ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      ' Final Balance
      _final_balance = DbRow.Value(SQL_COLUMN_FINAL_BALANCE)

      Me.Grid.Cell(RowIndex, GRID_COLUMN_FINAL_BALANCE).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_FINAL_BALANCE), _
                                                                                   ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      If Math.Round((_played_amount - _won_amount), 2) <> Math.Round((_initial_balance - _final_balance), 2) Then
        Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_01)
      End If

    Else

      ParseAuditData(DbRow.Value(GRID_COLUMN_A3GS_PROCEDURE), DbRow.Value(SQL_COLUMN_A3GS_INPUT), DbRow.Value(SQL_COLUMN_A3GS_OUTPUT))

      ' GRID_COLUMN_A3GS_EXECUTION
      Me.Grid.Cell(RowIndex, GRID_COLUMN_A3GS_EXECUTION).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_A3GS_EXECUTION), _
                                                                          ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                          ENUM_FORMAT_TIME.FORMAT_HHMMSS)

      ' GRID_COLUMN_A3GS_PROCEDURE
      Me.Grid.Cell(RowIndex, GRID_COLUMN_A3GS_PROCEDURE).Value = DbRow.Value(SQL_COLUMN_A3GS_PROCEDURE)

      ' GRID_COLUMN_A3GS_STATUS
      Me.Grid.Cell(RowIndex, GRID_COLUMN_A3GS_STATUS).Value = DbRow.Value(SQL_COLUMN_A3GS_STATUS_CODE)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_A3GS_STATUS_TEXT).Value = m_parse_data.status_text
      '      GUI_SetToolTipText(RowIndex, GRID_COLUMN_A3GS_STATUS, m_parse_data.status_text)

      ' GRID_COLUMN_A3GS_INITIAL_BALANCE
      Me.Grid.Cell(RowIndex, GRID_COLUMN_A3GS_INITIAL_BALANCE).Value = GUI_FormatCurrency(m_parse_data.initial_balance, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, True)

      ' GRID_COLUMN_A3GS_INPUT_PLAYED_AMOUNT
      Me.Grid.Cell(RowIndex, GRID_COLUMN_A3GS_INPUT_PLAYED_AMOUNT).Value = GUI_FormatCurrency(m_parse_data.played_amount, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, True)

      ' GRID_COLUMN_A3GS_INPUT_WON_AMOUNT
      Me.Grid.Cell(RowIndex, GRID_COLUMN_A3GS_INPUT_WON_AMOUNT).Value = GUI_FormatCurrency(m_parse_data.won_amount, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, True)

      ' GRID_COLUMN_A3GS_INPUT_FINAL_BALANCE
      Me.Grid.Cell(RowIndex, GRID_COLUMN_A3GS_INPUT_FINAL_BALANCE).Value = GUI_FormatCurrency(m_parse_data.final_balance, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, True)

      ' GRID_COLUMN_A3GS_INPUT_FINAL_BALANCE_calculate
      Me.Grid.Cell(RowIndex, GRID_COLUMN_A3GS_INPUT_FINAL_BALANCE_CALCULATE).Value = GUI_FormatCurrency(m_parse_data.final_balance_calculate, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, True)

      ' GRID_COLUMN_A3GS_INPUT_PLAYED_PLAYS
      Me.Grid.Cell(RowIndex, GRID_COLUMN_A3GS_INPUT_PLAYED_PLAYS).Value = GUI_FormatNumber(m_parse_data.played_plays, 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      ' GRID_COLUMN_A3GS_INPUT_WON_PLAYS
      Me.Grid.Cell(RowIndex, GRID_COLUMN_A3GS_INPUT_WON_PLAYS).Value = GUI_FormatNumber(m_parse_data.won_plays, 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      If m_parse_data.final_balance <> Math.Round(m_parse_data.final_balance_calculate, 2, MidpointRounding.AwayFromZero) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_A3GS_INPUT_FINAL_BALANCE).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
      End If

    End If


    Return True

  End Function ' GUI_SetupRow

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.dtp_from
  End Sub 'GUI_SetInitialFocus

  ' PURPOSE: Set the tool tip text for a given row and column
  '
  '  PARAMS:
  '     - INPUT:
  '           - RowIndex As Integer
  '           - ColIndex As Integer
  '     - OUTPUT:
  '           - ToolTipTxt As String
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_SetToolTipText(ByVal RowIndex As Integer, _
                                             ByVal ColIndex As Integer, _
                                             ByRef ToolTipTxt As String)

    If ColIndex = GRID_COLUMN_A3GS_STATUS Then
      ToolTipTxt = Me.Grid.Cell(RowIndex, GRID_COLUMN_A3GS_STATUS_TEXT).Value
    End If

  End Sub ' GUI_SetToolTipText

#Region " GUI Reports "

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(261) & " " & GLB_NLS_GUI_AUDITOR.GetString(257), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(261) & " " & GLB_NLS_GUI_AUDITOR.GetString(258), m_date_to)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(412), m_session_date)

    PrintData.FilterValueWidth(1) = 3000
    PrintData.FilterValueWidth(2) = 3000

  End Sub ' GUI_ReportFilter

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)

    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_date_from = ""
    m_date_to = ""

    ' Date 
    If Me.dtp_from.Checked Then
      m_date_from = GUI_FormatDate(dtp_from.Value, _
                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Me.dtp_to.Checked Then
      m_date_to = GUI_FormatDate(dtp_to.Value, _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region  ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window, _
                         ByVal SessionId As Integer, _
                         ByVal TerminalType As String, _
                         ByVal SessionDate As String, _
                         Optional ByVal InitialBalance As String = "")

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING

    Me.m_session_id = SessionId
    Me.m_session_date = SessionDate

    If TerminalType <> "" AndAlso TerminalType = WSI.Common.TerminalTypes.T3GS Then
      Me.m_terminal_type = TerminalType
      m_initial_balance = InitialBalance
    End If

    Me.MdiParent = MdiParent
    Me.Display(False)

    ' For Show window before filter apply 
    Call Application.DoEvents()
    System.Threading.Thread.Sleep(100)
    Call Application.DoEvents()

    Me.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    With Me.Grid

      If (m_play_stand_alone And m_terminal_type <> WSI.Common.TerminalTypes.T3GS) Then
        Call .Init(GRID_COLUMNS_SA, GRID_HEADER_ROWS)
      ElseIf (m_terminal_type = WSI.Common.TerminalTypes.T3GS) Then
        Call .Init(GRID_COLUMNS_A3GS, GRID_HEADER_ROWS_A3GS)
      Else
        Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      End If

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      If (m_terminal_type <> WSI.Common.TerminalTypes.T3GS) Then

        '  Datetime
        .Column(GRID_COLUMN_DATETIME).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(410)
        .Column(GRID_COLUMN_DATETIME).Width = GRID_WIDTH_DATE
        .Column(GRID_COLUMN_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

        '  Terminal
        .Column(GRID_COLUMN_TERMINAL).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(383)
        .Column(GRID_COLUMN_TERMINAL).Width = GRID_WIDTH_TERMINAL
        .Column(GRID_COLUMN_TERMINAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

        '  Game
        .Column(GRID_COLUMN_GAME).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(411)
        .Column(GRID_COLUMN_GAME).Width = GRID_WIDTH_COUNT
        .Column(GRID_COLUMN_GAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

        '  Card Number
        .Column(GRID_COLUMN_CARD_NUMBER).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
        .Column(GRID_COLUMN_CARD_NUMBER).Width = 1000
        .Column(GRID_COLUMN_CARD_NUMBER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

        ' Account type
        .Column(GRID_COLUMN_ACCOUNT_TYPE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
        .Column(GRID_COLUMN_ACCOUNT_TYPE).Width = 0

        '  Track Data Card
        .Column(GRID_COLUMN_CARD_TRACK_DATA).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(212)
        .Column(GRID_COLUMN_CARD_TRACK_DATA).Width = 2300
        .Column(GRID_COLUMN_CARD_TRACK_DATA).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

        If (m_play_stand_alone) Then

          '  Card Number
          .Column(GRID_COLUMN_CARD_PLAYER_SA).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(210)
          .Column(GRID_COLUMN_CARD_PLAYER_SA).Width = GRID_WIDTH_AMOUNT * 2
          .Column(GRID_COLUMN_CARD_PLAYER_SA).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

          '  Played Amount
          .Column(GRID_COLUMN_PLAYED_AMOUNT_SA).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(385)
          .Column(GRID_COLUMN_PLAYED_AMOUNT_SA).Width = GRID_WIDTH_AMOUNT
          .Column(GRID_COLUMN_PLAYED_AMOUNT_SA).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          ' Won Amount
          .Column(GRID_COLUMN_WON_AMOUNT_SA).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(386)
          .Column(GRID_COLUMN_WON_AMOUNT_SA).Width = GRID_WIDTH_AMOUNT
          .Column(GRID_COLUMN_WON_AMOUNT_SA).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          Return
        End If

        '  Played Amount
        .Column(GRID_COLUMN_PLAYED_AMOUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(385)
        .Column(GRID_COLUMN_PLAYED_AMOUNT).Width = GRID_WIDTH_AMOUNT
        .Column(GRID_COLUMN_PLAYED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ' Won Amount
        .Column(GRID_COLUMN_WON_AMOUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(386)
        .Column(GRID_COLUMN_WON_AMOUNT).Width = GRID_WIDTH_AMOUNT
        .Column(GRID_COLUMN_WON_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        '  Initial Balance
        .Column(GRID_COLUMN_INITIAL_BALANCE).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(384)
        .Column(GRID_COLUMN_INITIAL_BALANCE).Width = GRID_WIDTH_AMOUNT
        .Column(GRID_COLUMN_INITIAL_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ' Final Balance
        .Column(GRID_COLUMN_FINAL_BALANCE).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(389)
        .Column(GRID_COLUMN_FINAL_BALANCE).Width = GRID_WIDTH_AMOUNT
        .Column(GRID_COLUMN_FINAL_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      Else

        'GRID_COLUMN_A3GS_EXECUTION
        .Column(GRID_COLUMN_A3GS_EXECUTION).Header(0).Text = ""
        .Column(GRID_COLUMN_A3GS_EXECUTION).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(410)
        .Column(GRID_COLUMN_A3GS_EXECUTION).Width = GRID_WIDTH_DATE
        .Column(GRID_COLUMN_A3GS_EXECUTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

        'GRID_COLUMN_A3GS_PROCEDURE
        .Column(GRID_COLUMN_A3GS_PROCEDURE).Header(0).Text = ""
        .Column(GRID_COLUMN_A3GS_PROCEDURE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1111)
        .Column(GRID_COLUMN_A3GS_PROCEDURE).Width = GRID_WIDTH_STATUS
        .Column(GRID_COLUMN_A3GS_PROCEDURE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

        'GRID_COLUMN_A3GS_STATUS
        .Column(GRID_COLUMN_A3GS_STATUS).Header(0).Text = ""
        .Column(GRID_COLUMN_A3GS_STATUS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1112)
        .Column(GRID_COLUMN_A3GS_STATUS).Width = GRID_WIDTH_STATUS
        .Column(GRID_COLUMN_A3GS_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

        'GRID_COLUMN_A3GS_INITIAL_BALANCE
        .Column(GRID_COLUMN_A3GS_INITIAL_BALANCE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(240)
        .Column(GRID_COLUMN_A3GS_INITIAL_BALANCE).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(384)
        .Column(GRID_COLUMN_A3GS_INITIAL_BALANCE).Width = GRID_WIDTH_AMOUNT
        .Column(GRID_COLUMN_A3GS_INITIAL_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        'GRID_COLUMN_A3GS_INPUT_PLAYED_AMOUNT
        .Column(GRID_COLUMN_A3GS_INPUT_PLAYED_AMOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(240)
        .Column(GRID_COLUMN_A3GS_INPUT_PLAYED_AMOUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(385)
        .Column(GRID_COLUMN_A3GS_INPUT_PLAYED_AMOUNT).Width = GRID_WIDTH_AMOUNT
        .Column(GRID_COLUMN_A3GS_INPUT_PLAYED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        'GRID_COLUMN_A3GS_INPUT_WON_AMOUNT
        .Column(GRID_COLUMN_A3GS_INPUT_WON_AMOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(240)
        .Column(GRID_COLUMN_A3GS_INPUT_WON_AMOUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(386)
        .Column(GRID_COLUMN_A3GS_INPUT_WON_AMOUNT).Width = GRID_WIDTH_AMOUNT
        .Column(GRID_COLUMN_A3GS_INPUT_WON_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        'GRID_COLUMN_A3GS_INPUT_FINAL_BALANCE
        .Column(GRID_COLUMN_A3GS_INPUT_FINAL_BALANCE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(240)
        .Column(GRID_COLUMN_A3GS_INPUT_FINAL_BALANCE).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(389)
        .Column(GRID_COLUMN_A3GS_INPUT_FINAL_BALANCE).Width = GRID_WIDTH_AMOUNT
        .Column(GRID_COLUMN_A3GS_INPUT_FINAL_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        'GRID_COLUMN_A3GS_INPUT_FINAL_BALANCE_CALCULATE
        .Column(GRID_COLUMN_A3GS_INPUT_FINAL_BALANCE_CALCULATE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(240)
        .Column(GRID_COLUMN_A3GS_INPUT_FINAL_BALANCE_CALCULATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1109)
        .Column(GRID_COLUMN_A3GS_INPUT_FINAL_BALANCE_CALCULATE).Width = GRID_WIDTH_AMOUNT
        .Column(GRID_COLUMN_A3GS_INPUT_FINAL_BALANCE_CALCULATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        'GRID_COLUMN_A3GS_INPUT_PLAYED_PLAYS
        .Column(GRID_COLUMN_A3GS_INPUT_PLAYED_PLAYS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1110)
        .Column(GRID_COLUMN_A3GS_INPUT_PLAYED_PLAYS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1114)
        .Column(GRID_COLUMN_A3GS_INPUT_PLAYED_PLAYS).Width = GRID_WIDTH_COUNT
        .Column(GRID_COLUMN_A3GS_INPUT_PLAYED_PLAYS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        'GRID_COLUMN_A3GS_INPUT_WON_PLAYS
        .Column(GRID_COLUMN_A3GS_INPUT_WON_PLAYS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1110)
        .Column(GRID_COLUMN_A3GS_INPUT_WON_PLAYS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1115)
        .Column(GRID_COLUMN_A3GS_INPUT_WON_PLAYS).Width = GRID_WIDTH_COUNT
        .Column(GRID_COLUMN_A3GS_INPUT_WON_PLAYS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        'GRID_COLUMN_A3GS_STATUS_TEXT
        .Column(GRID_COLUMN_A3GS_STATUS_TEXT).Header(0).Text = ""
        .Column(GRID_COLUMN_A3GS_STATUS_TEXT).Header(1).Text = ""
        .Column(GRID_COLUMN_A3GS_STATUS_TEXT).Width = 0

      End If

    End With

  End Sub 'GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Dim filter_date_nothing As Date

    Me.dtp_from.Value = New DateTime(Now.Year, Now.Month, Now.Day, 0, 0, 0)

    Me.dtp_from.Checked = False
    Me.panel_filter.Size = New System.Drawing.Size(1175, 113)

    Me.dtp_to.Value = Me.dtp_from.Value.AddDays(1)
    Me.dtp_to.Checked = False

    filter_date_nothing = Nothing

  End Sub ' SetDefaultValues

  ' PURPOSE: Get Sql WHERE to buil SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String

    Dim str_where As String = ""
    Dim where_added As Boolean
    Dim _sb As StringBuilder

    _sb = New StringBuilder()

    where_added = False

    _sb.AppendLine(" PL_DATETIME,                                       ")
    _sb.AppendLine(" PL_TERMINAL_ID,                                    ")
    _sb.AppendLine(" ISNULL(GM_NAME,'UNKNOWN') AS GM_NAME,              ")
    _sb.AppendLine(" AC_ACCOUNT_ID,                                     ")
    _sb.AppendLine(" AC_TYPE,                                           ")
    _sb.AppendLine(" AC_TRACK_DATA,                                     ")
    _sb.AppendLine(" PL_PLAYED_AMOUNT,                                  ")
    _sb.AppendLine(" PL_WON_AMOUNT,                                     ")
    _sb.AppendLine(" PL_INITIAL_BALANCE,                                ")
    _sb.AppendLine(" PL_FINAL_BALANCE,                                  ")
    _sb.AppendLine(" AC_HOLDER_NAME                                     ")
    _sb.AppendLine(" FROM PLAYS WITH( INDEX(IX_plays)) INNER JOIN       ")

    _sb.AppendLine(" TERMINAL_GAME_TRANSLATION TGT                      ")
    _sb.AppendLine(" ON TGT.TGT_TERMINAL_ID = PLAYS.PL_TERMINAL_ID AND TGT.TGT_SOURCE_GAME_ID = PLAYS.PL_GAME_ID")
    _sb.AppendLine(" LEFT OUTER JOIN GAMES_V2 GV                        ")
    _sb.AppendLine(" ON GV.GM_GAME_ID = TGT.TGT_TRANSLATED_GAME_ID      ")

    _sb.AppendLine(" LEFT OUTER JOIN                                    ")
    _sb.AppendLine(" ACCOUNTS ON PL_ACCOUNT_ID = AC_ACCOUNT_ID          ")

    str_where = _sb.ToString()

    ' Filter Dates
    If Me.dtp_from.Checked = True Then

      If where_added = False Then
        str_where = str_where & " WHERE "
        where_added = True
      Else
        str_where = str_where & " AND "
      End If

      str_where = str_where & " (PL_DATETIME >= " & GUI_FormatDateDB(dtp_from.Value) & ") "
    End If

    If Me.dtp_to.Checked = True Then

      If where_added = False Then
        str_where = str_where & " WHERE "
        where_added = True
      Else
        str_where = str_where & " AND "
      End If

      str_where = str_where & " (PL_DATETIME < " & GUI_FormatDateDB(dtp_to.Value) & ") "
    End If

    If where_added = False Then
      str_where = str_where & " WHERE "
      where_added = True
    Else
      str_where = str_where & " AND "
    End If

    str_where = str_where & " PL_PLAY_SESSION_ID = " & m_session_id

    Return str_where
  End Function ' GetSqlWhere

  ' PURPOSE: Parse values from audit_3gs.a3gs_input y audit_3gs.a3gs_output
  '
  '  PARAMS:
  '     - INPUT:
  '           - a3gs_procedure
  '           - a3gs_input
  '           - a3gs_output
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ParseAuditData(ByVal Procedure As String, ByVal InputData As String, ByVal OutputData As String)

    Dim _input_data_parse() As String
    Dim _output_data_parse() As String
    Dim _sb As StringBuilder

    Try
      m_parse_data.initial_balance = m_initial_balance
      m_parse_data.played_amount = 0
      m_parse_data.won_amount = 0
      m_parse_data.final_balance = m_initial_balance
      m_parse_data.final_balance_calculate = m_initial_balance
      m_parse_data.played_plays = 0
      m_parse_data.won_plays = 0
      m_parse_data.status_text = ""

      _output_data_parse = OutputData.Split(";")

      _sb = New StringBuilder()

      _sb.AppendLine(GetValue(_output_data_parse(0)) & " - " & GetValue(_output_data_parse(1)))
      _sb.AppendLine()

      For Each _elem As String In _output_data_parse
        _sb.AppendLine(_elem)
      Next

      m_parse_data.status_text = _sb.ToString()

      If Procedure = "Start" Then
        Exit Sub
      End If
      ' @AmountPlayed=300.00;@AmountWon=60.00;@GamesPlayed=15;@GamesWon=4;@CreditBalance=9160.00; I-J+G
      '0  @AccountID=09739858831667586800;
      '1  @VendorId=WSI;
      '2  @SerialNumber=RAUL SERIAL 001;
      '3  @MachineNumber=4012345;
      '4  @SessionId=647585;
      '5  @AmountPlayed=100.00;
      '6  @AmountWon=0.00;
      '7  @GamesPlayed=5;
      '8  @GamesWon=0;
      '9  @CreditBalance=9400.00;
      '10  @CurrentJackpot=0.00


      _input_data_parse = InputData.Split(";")

      m_parse_data.played_amount = GetValue(_input_data_parse(5))
      m_parse_data.won_amount = GetValue(_input_data_parse(6))
      m_parse_data.played_plays = GetValue(_input_data_parse(7))
      m_parse_data.won_plays = GetValue(_input_data_parse(8))
      m_parse_data.final_balance = GetValue(_input_data_parse(9))

      m_parse_data.final_balance_calculate = m_parse_data.initial_balance - m_parse_data.played_amount + m_parse_data.won_amount

    Catch
    End Try

  End Sub ' ParseInputAuditData

  Private Function GetValue(ByVal Element As String) As String
    Try
      Return Element.Split("=")(1)
    Catch
      Return ""
    End Try
  End Function ' GetValue

#End Region  ' Private Functions

#Region "Events"

#End Region ' Events

End Class
