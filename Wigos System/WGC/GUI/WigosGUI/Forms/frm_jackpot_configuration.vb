'-------------------------------------------------------------------
' Copyright � 2008 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_jackpot_configuration.vb
'
' DESCRIPTION:   Jackpot configuration form
'
' AUTHOR:        Armando Alva
'
' CREATION DATE: 25-AUG-2008
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 25-AUG-2008  AAV    Initial Draft.
' 02-SEP-2008  AAV    Initial Release.
' 05-FEB-2013  ICS    Fixed Bug #562: Exception in GUI_IsScreenDataOk
'                     when ef_contribution is empty.
' 11-JUL-2014  JMM    Weeks days sorted depending on system's first day of week
' 21-NOV-2016  GMV    Product Backlog Item 19930:Jackpots - GUI - Show on WinUp setting
' 27-JUL-2017  JBM    Fixed Defect #Wigos-4020 Unhandled exception configuring Jackpot WIN when fields Minimum, Maximum or Average are empty.
' 09-AUG-2017  DPC    WIGOS-4335: Jackpot WIN: it is not possible to define the amounts for the jackpot
'--------------------------------------------------------------------
Option Strict Off
Option Explicit On

Imports System.Data.OleDb
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Math
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE

Public Class frm_jackpot_configuration
  Inherits frm_base_edit


#Region " Constants "

  'Match with constant in CommonData.dll
  Private AUDIT_NAME_JACKPOT_PARAMETERS As Integer = 17

  ' Grid Columns
  Private Const GRID_COLUMNS As Integer = 9

  Private Const GRID_HEADER_ROWS As Integer = 1
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_NAME As Integer = 1
  Private Const GRID_COLUMN_CONTRIBUTION As Integer = 2
  Private Const GRID_COLUMN_MIN_BET_AMOUNT As Integer = 3
  Private Const GRID_COLUMN_MIN_AMOUNT As Integer = 4
  Private Const GRID_COLUMN_MAX_AMOUNT As Integer = 5
  Private Const GRID_COLUMN_DESIRED_AMOUNT As Integer = 6
  Private Const GRID_COLUMN_FREQUENCY As Integer = 7
  Private Const GRID_COLUMN_SHOW_ON_WINUP As Integer = 8

  Private Const MAX_JACKPOT_LOCK_ID As Integer = 1000000

#End Region 'Constants

#Region " Members "
  Private m_winUp_enabled As Boolean                 'indicates wether WinUP module is installed
#End Region ' Members

#Region "Overrides"

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CLASS_II_JACKPOT_CONFIGURATION

    ' Set the form icon from the embedded resource (ICO file must be built in the project as "Embedded Resource")
    ' Call could be made as GetManifestResourceStream("GUI_JackpotManager.GUI_JackpotManager.ico"))
    ' Me.Icon = New Icon(System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream(Me.GetType(), "GUI_JackpotManager.ico"))

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    Dim _week_days_chk_array(6) As Windows.Forms.CheckBox

    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    ' Initialize Form Controls

    ' - Form
    Me.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(231)

    '- Buttons

    ' - Labels
    '   - Jackpot system 
    gb_jackpot_flag.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(252)
    opt_enabled.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(218)
    opt_disabled.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(250)

    '   - Contribution
    gb_gen_values.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(488)
    ef_contribution.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(279)
    ef_last_month_played.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(338)
    btn_load_last_month_played.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(345)

    '   - Awarding
    gb_wrk_days.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(296)
    dtp_working_from.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(204)
    dtp_working_to.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(205)

    '     - Working Days
    chk_monday.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(289)
    chk_tuesday.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(290)
    chk_wednesday.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(291)
    chk_thursday.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(292)
    chk_friday.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(293)
    chk_saturday.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(294)
    chk_sunday.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(295)

    _week_days_chk_array(0) = chk_monday
    _week_days_chk_array(1) = chk_tuesday
    _week_days_chk_array(2) = chk_wednesday
    _week_days_chk_array(3) = chk_thursday
    _week_days_chk_array(4) = chk_friday
    _week_days_chk_array(5) = chk_saturday
    _week_days_chk_array(6) = chk_sunday

    GUI_Controls.Misc.SortCheckBoxesByFirstDayOfWeek(_week_days_chk_array)

    ' Configure Data Grid
    Me.m_winUp_enabled = WSI.Common.GeneralParam.GetBoolean("Features", "WinUP")

    ' Configure Data Grid
    Call GUI_StyleView()

    ' Enable / Disable controls
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = False
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Visible = False
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Enabled = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Enabled = True

    ' Set date picker obj properties
    Me.dtp_working_from.SetFormat(ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_working_from.ShowUpDown = True
    Me.dtp_working_to.SetFormat(ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_working_to.ShowUpDown = True

    ' Format entry fields
    Me.ef_contribution.SetFilter(FORMAT_NUMBER, 5, 2)
    Me.ef_last_month_played.SetFilter(FORMAT_MONEY, 10)

    ' Titles
    Me.cmb_block_mode.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(317)
    Me.cmb_block_interval.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(318)
    ' 319 "Duraci�n de la animaci�n del Jackpot"
    Me.cmb_anim_interval.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(319)
    ' 320 "Todos los terminales anuncian el �ltimo Jackpot durante"
    Me.cmb_recent_interval.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(320)
    ' 320 "Todos los terminales anuncian el �ltimo Jackpot durante"
    Me.cmb_min_jack_block.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(320)

    Me.lbl_block_mode.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(317)
    Me.lbl_block_interval.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(318)
    ' 319 "Duraci�n de la animaci�n del Jackpot"
    Me.lbl_animation_interval.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(319)
    ' 320 "Todos los terminales anuncian el �ltimo Jackpot durante"
    Me.lbl_recent_interval.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(320)
    Me.lbl_min_jackpot_block.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(326)

    Me.gb_parameters.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(323)

    ' There are 3 possible blocks Modes, None, Time and Key.
    Me.cmb_block_mode.Add(0, GLB_NLS_GUI_JACKPOT_MGR.GetString(306))
    Me.cmb_block_mode.Add(1, GLB_NLS_GUI_JACKPOT_MGR.GetString(307))
    Me.cmb_block_mode.Add(2, GLB_NLS_GUI_JACKPOT_MGR.GetString(308))

    'Block interval
    ' The database returns the data in seconds, so the identifier is expressed in seconds.
    Me.cmb_block_interval.Add(5 * 60, "5 " + GLB_NLS_GUI_JACKPOT_MGR.GetString(312))      ' 312 minutes
    Me.cmb_block_interval.Add(10 * 60, "10 " + GLB_NLS_GUI_JACKPOT_MGR.GetString(312))    ' 312 minutes
    Me.cmb_block_interval.Add(15 * 60, "15 " + GLB_NLS_GUI_JACKPOT_MGR.GetString(312))    ' 312 minutes
    Me.cmb_block_interval.Add(1 * 60 * 60, "1 " + GLB_NLS_GUI_JACKPOT_MGR.GetString(313)) ' 313 hour
    Me.cmb_block_interval.Add(2 * 60 * 60, "2 " + GLB_NLS_GUI_JACKPOT_MGR.GetString(314)) ' 314 hours

    'Animation interval
    ' The database returns the data in seconds, so the identifier is expressed in seconds.
    Me.cmb_anim_interval.Add(15, "15 " + GLB_NLS_GUI_JACKPOT_MGR.GetString(310))          ' 310 seconds
    Me.cmb_anim_interval.Add(30, "30 " + GLB_NLS_GUI_JACKPOT_MGR.GetString(310))          ' 310 seconds
    Me.cmb_anim_interval.Add(1 * 60, "1 " + GLB_NLS_GUI_JACKPOT_MGR.GetString(311))       ' 311 minute
    Me.cmb_anim_interval.Add(2 * 60, "2 " + GLB_NLS_GUI_JACKPOT_MGR.GetString(312))       ' 312 minutes

    'Minimum amount to lock 
    ' The database returns the data in seconds, so the identifier is expressed in seconds.
    Me.cmb_min_jack_block.Add(0, GUI_FormatCurrency(0, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, True).ToString())
    Me.cmb_min_jack_block.Add(500, GUI_FormatCurrency(500, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, True).ToString())
    Me.cmb_min_jack_block.Add(1000, GUI_FormatCurrency(1000, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, True).ToString())
    Me.cmb_min_jack_block.Add(2000, GUI_FormatCurrency(2000, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, True).ToString())
    Me.cmb_min_jack_block.Add(5000, GUI_FormatCurrency(5000, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, True).ToString())
    Me.cmb_min_jack_block.Add(10000, GUI_FormatCurrency(10000, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, True).ToString())
    Me.cmb_min_jack_block.Add(20000, GUI_FormatCurrency(20000, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, True).ToString())
    Me.cmb_min_jack_block.Add(50000, GUI_FormatCurrency(50000, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, True).ToString())

    'Recent jackpot interval
    ' The database returns the data in seconds, so the identifier is expressed in seconds.
    Me.cmb_recent_interval.Add(1 * 60 * 60, "1 " + GLB_NLS_GUI_JACKPOT_MGR.GetString(313))        ' 313 hour
    Me.cmb_recent_interval.Add(2 * 60 * 60, "2 " + GLB_NLS_GUI_JACKPOT_MGR.GetString(314))        ' 314 hours
    Me.cmb_recent_interval.Add(1 * 24 * 60 * 60, "1 " + GLB_NLS_GUI_JACKPOT_MGR.GetString(315))   ' 315 day
    Me.cmb_recent_interval.Add(2 * 24 * 60 * 60, "2 " + GLB_NLS_GUI_JACKPOT_MGR.GetString(316))   ' 316 days
    Me.cmb_recent_interval.Add(7 * 24 * 60 * 60, "1 " + GLB_NLS_GUI_JACKPOT_MGR.GetString(324))   ' 324 week
    Me.cmb_recent_interval.Add(14 * 24 * 60 * 60, "2 " + GLB_NLS_GUI_JACKPOT_MGR.GetString(325))  ' 325 week

  End Sub 'GUI_InitControls

  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Dim idx As Integer
    Dim hour_min As Integer
    Dim minute_min As Integer
    Dim hour_max As Integer
    Dim minute_max As Integer
    Dim nls_param1 As String
    Dim min_amount As Double
    Dim max_amount As Double
    Dim desired_amount As Double
    Dim acc_contribution As Double
    Dim warning As Boolean
    Dim warning_str As String

    warning_str = ""

    ' Working Start / End times
    hour_min = dtp_working_from.Value.Hour
    minute_min = dtp_working_from.Value.Minute + hour_min * 60
    hour_max = dtp_working_to.Value.Hour
    minute_max = dtp_working_to.Value.Minute + hour_max * 60
    minute_min = minute_max - minute_min

    If minute_min < MIN_DIFF_MINUTES Then
      nls_param1 = CStr(MIN_DIFF_MINUTES \ 60)
      Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(107), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , nls_param1)
      Call Me.dtp_working_from.Focus()

      Return False
    End If

    ' Check if ef_contribution is empty
    If String.IsNullOrEmpty(ef_contribution.Value) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , ef_contribution.Text)
      Call Me.ef_contribution.Focus()

      Return False
    End If

    If GUI_FormatNumber(ef_contribution.Value) > 100 Then
      Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Me.ef_contribution.Focus()

      Return False
    End If

    ' Check consistence between parameters set for every jackpot instance
    For idx = 0 To Me.dg_jackpot_list.NumRows - 1

      min_amount = GUI_ParseCurrency(Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_MIN_AMOUNT).Value)
      max_amount = GUI_ParseCurrency(Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_MAX_AMOUNT).Value)
      desired_amount = GUI_ParseCurrency(Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_DESIRED_AMOUNT).Value)

      If max_amount <= min_amount Then
        Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(121), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_INDEX).Value & "-" & Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_NAME).Value)
        Me.dg_jackpot_list.CurrentCol = GRID_COLUMN_MAX_AMOUNT
        Me.dg_jackpot_list.CurrentRow = idx
        Call Me.dg_jackpot_list.Focus()

        Return False
      End If

      If min_amount >= max_amount Then
        Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(122), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_INDEX).Value & "-" & Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_NAME).Value)
        Me.dg_jackpot_list.CurrentCol = GRID_COLUMN_MIN_AMOUNT
        Me.dg_jackpot_list.CurrentRow = idx
        Call Me.dg_jackpot_list.Focus()

        Return False
      End If

      If desired_amount <= min_amount Then
        Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_INDEX).Value & "-" & Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_NAME).Value)
        Me.dg_jackpot_list.CurrentCol = GRID_COLUMN_DESIRED_AMOUNT
        Me.dg_jackpot_list.CurrentRow = idx
        Call Me.dg_jackpot_list.Focus()

        Return False
      End If

      If desired_amount >= max_amount Then
        Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_INDEX).Value & "-" & Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_NAME).Value)
        Me.dg_jackpot_list.CurrentCol = GRID_COLUMN_DESIRED_AMOUNT
        Me.dg_jackpot_list.CurrentRow = idx
        Call Me.dg_jackpot_list.Focus()

        Return False
      End If

      If Not min_amount <= desired_amount / 2 Then
        ' Issue warning
        'Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(129), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_INDEX).Value & "-" & Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_NAME).Value)
        warning = True
        warning_str += "- " & NLS_GetString(GLB_NLS_GUI_JACKPOT_MGR.Id(129), Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_INDEX).Value & "-" & Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_NAME).Value) & Environment.NewLine & Environment.NewLine

      End If

      If Not desired_amount <= max_amount / 2 Then
        ' Issue warning
        'Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(130), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_INDEX).Value & "-" & Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_NAME).Value)
        warning_str += "- " & NLS_GetString(GLB_NLS_GUI_JACKPOT_MGR.Id(130), Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_INDEX).Value & "-" & Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_NAME).Value) & Environment.NewLine & Environment.NewLine
        warning = True
      End If

    Next

    ' Check accumulated contribution: must be 100
    acc_contribution = 0
    For idx = 0 To dg_jackpot_list.NumRows - 1
      acc_contribution += dg_jackpot_list.Cell(idx, GRID_COLUMN_CONTRIBUTION).Value
    Next
    If acc_contribution <> 100 Then
      Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(102), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Me.dg_jackpot_list.CurrentCol = GRID_COLUMN_CONTRIBUTION
      Me.dg_jackpot_list.CurrentRow = 0
      Call Me.dg_jackpot_list.Focus()

      Return False
    End If

    ' Check for overlapping values (check for maximums)
    idx = dg_jackpot_list.NumRows - 1
    While idx >= 1

      'Max. amount
      If CInt(dg_jackpot_list.Cell(idx, GRID_COLUMN_MAX_AMOUNT).Value) >= CInt(dg_jackpot_list.Cell(idx - 1, GRID_COLUMN_MIN_AMOUNT).Value) Then
        Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(128), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_INDEX).Value & "-" & Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_NAME).Value)
        Me.dg_jackpot_list.CurrentCol = GRID_COLUMN_MAX_AMOUNT
        Me.dg_jackpot_list.CurrentRow = idx
        Call Me.dg_jackpot_list.Focus()

        Return False
      End If

      idx -= 1

    End While

    If (warning = True) Then

    End If

    If (warning) Then
      warning_str += Environment.NewLine & Environment.NewLine & NLS_GetString(GLB_NLS_GUI_JACKPOT_MGR.Id(489))
      MsgBox(warning_str, MsgBoxStyle.OkOnly)

      Return False
    End If

    Return True

  End Function ' GUI_IsScreenDataOk

  Protected Overrides Sub GUI_GetScreenData()

    Dim jp_params_edit As CLASS_JACKPOT_PARAMS
    Dim jp_params_read As CLASS_JACKPOT_PARAMS
    Dim temp_date As Date
    Dim second As Integer
    Dim idx As Integer

    jp_params_edit = Me.DbEditedObject
    jp_params_read = Me.DbReadObject

    jp_params_edit.Enabled = Me.opt_enabled.Checked

    temp_date = Me.dtp_working_from.Value
    second = Format_HhMmSsToSecond(temp_date)
    jp_params_edit.WrkStart = second

    temp_date = Me.dtp_working_to.Value
    second = Format_HhMmSsToSecond(temp_date)
    jp_params_edit.WrkEnd = second

    jp_params_edit.ContributionPct = GUI_ParseNumber(Me.ef_contribution.Value)

    jp_params_edit.JackpotInstances.Clear()

    jp_params_edit.WorkingDays = GetWorkingDays()

    For idx = 0 To Me.dg_jackpot_list.NumRows - 1
      Dim jackpot_inst As New CLASS_JACKPOT_PARAMS.TYPE_JACKPOT_INSTANCE

      jackpot_inst.index = GUI_ParseNumber(Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_INDEX).Value)
      jackpot_inst.name = Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_NAME).Value
      jackpot_inst.contribution_pct = GUI_ParseNumber(Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_CONTRIBUTION).Value)
      jackpot_inst.min_bet_amount = GUI_ParseCurrency(Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_MIN_BET_AMOUNT).Value)
      jackpot_inst.min_amount = GUI_ParseCurrency(Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_MIN_AMOUNT).Value)
      jackpot_inst.max_amount = GUI_ParseCurrency(Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_MAX_AMOUNT).Value)
      jackpot_inst.average = GUI_ParseCurrency(Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_DESIRED_AMOUNT).Value)
      jackpot_inst.show_on_winup = GridValuetoBool(Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_SHOW_ON_WINUP).Value)

      jp_params_edit.JackpotInstances.Add(jackpot_inst)
    Next

    jp_params_edit.BlockMode = cmb_block_mode.Value
    jp_params_edit.BlockInterval = cmb_block_interval.Value
    jp_params_edit.AnimInterval = cmb_anim_interval.Value
    jp_params_edit.RecentInterval = cmb_recent_interval.Value
    jp_params_edit.MinAwardAmount = cmb_min_jack_block.Value

  End Sub ' GUI_GetScreenData

  Protected Overrides Sub GUI_ButtonClick(ButtonId As frm_base.ENUM_BUTTON)
    MyBase.GUI_ButtonClick(ButtonId)
  End Sub

  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    Dim jackpot_parameters As CLASS_JACKPOT_PARAMS
    Dim second As Integer
    Dim minute As Integer
    Dim hour As Integer
    Dim day As Integer
    Dim date_time As Date

    date_time = GUI_GetDateTime()
    jackpot_parameters = Me.DbReadObject

    ' Enabled / Disabled
    If jackpot_parameters.Enabled Then
      opt_enabled.Checked = True
      opt_disabled.Checked = False
    Else
      opt_enabled.Checked = False
      opt_disabled.Checked = True
    End If

    ' Working Values
    Call Format_SecondsToDdHhMmSs(jackpot_parameters.WrkStart, day, hour, minute, second)
    Me.dtp_working_from.Value = New DateTime(date_time.Year, date_time.Month, date_time.Day, hour, minute, second)

    Call Format_SecondsToDdHhMmSs(jackpot_parameters.WrkEnd, day, hour, minute, second)
    Me.dtp_working_to.Value = New DateTime(date_time.Year, date_time.Month, date_time.Day, hour, minute, second)

    ' Contribution
    Me.ef_contribution.Value = jackpot_parameters.ContributionPct

    ' Set Jackpot List
    Call SetJackpotList(jackpot_parameters)

    ' Working Days
    Call SetWorkingDays(jackpot_parameters.WorkingDays)

    Me.cmb_block_mode.Value = jackpot_parameters.BlockMode
    Me.cmb_block_interval.Value = jackpot_parameters.BlockInterval
    Me.cmb_anim_interval.Value = jackpot_parameters.AnimInterval
    Me.cmb_recent_interval.Value = jackpot_parameters.RecentInterval
    Me.cmb_min_jack_block.Value = jackpot_parameters.MinAwardAmount

    Select Case cmb_block_mode.Value
      Case 0
        cmb_block_interval.Visible = False
        lbl_block_interval.Visible = False
        lbl_min_jackpot_block.Visible = False
        cmb_min_jack_block.Visible = False

      Case 1
        cmb_block_interval.Visible = True
        lbl_block_interval.Visible = True
        lbl_min_jackpot_block.Visible = True
        cmb_min_jack_block.Visible = True

      Case 2
        cmb_block_interval.Visible = False
        lbl_block_interval.Visible = False
        lbl_min_jackpot_block.Visible = True
        cmb_min_jack_block.Visible = True

    End Select

  End Sub ' GUI_SetScreenData

  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)

  End Sub  'GUI_Permissions

  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)
    Dim jackpot_parameters As CLASS_JACKPOT_PARAMS

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_JACKPOT_PARAMS
        jackpot_parameters = DbEditedObject
        jackpot_parameters.Enabled = False

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        Else
          ' Call Me.DB_CheckAllCounters() DO NOTHING
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)
    End Select

  End Sub ' GUI_DB_Operation

  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    ' The keypress event is done in uc_grid control
    If Me.dg_jackpot_list.ContainsFocus Then
      Return Me.dg_jackpot_list.KeyPressed(sender, e)
    End If

    Return MyBase.GUI_KeyPress(sender, e)

  End Function ' GUI_KeyPress

#End Region

#Region "Private Functions"

  ' PURPOSE: Set jackpot active pct list
  '
  '  PARAMS:
  '     - INPUT:
  '           - JackpotParams
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - none
  '
  Private Sub SetJackpotList(ByVal JackpotParams As CLASS_JACKPOT_PARAMS)

    Dim idx As Integer
    Dim last_month_played As Decimal

    Decimal.TryParse(ef_last_month_played.Value, last_month_played)

    Call Me.dg_jackpot_list.Clear()
    idx = 0

    For Each jackpot_instance As CLASS_JACKPOT_PARAMS.TYPE_JACKPOT_INSTANCE In JackpotParams.JackpotInstances

      Me.dg_jackpot_list.AddRow()

      Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_NAME).Value = jackpot_instance.name
      Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_CONTRIBUTION).Value = GUI_FormatNumber(jackpot_instance.contribution_pct, 2)
      Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_MIN_BET_AMOUNT).Value = GUI_FormatCurrency(jackpot_instance.min_bet_amount)
      Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_MIN_AMOUNT).Value = GUI_FormatCurrency(jackpot_instance.min_amount)
      Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_MAX_AMOUNT).Value = GUI_FormatCurrency(jackpot_instance.max_amount)
      Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_DESIRED_AMOUNT).Value = GUI_FormatCurrency(jackpot_instance.average)
      Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_INDEX).Value = CStr(jackpot_instance.index)
      Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_SHOW_ON_WINUP).Value = BoolToGridValue(jackpot_instance.show_on_winup)

      If String.IsNullOrEmpty(ef_last_month_played.Value) Then
        Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_FREQUENCY).Value = ""
      Else
        Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_FREQUENCY).Value = CalculateFrequency(last_month_played, _
                                                                                       JackpotParams.ContributionPct, _
                                                                                       jackpot_instance.contribution_pct, _
                                                                                       jackpot_instance.average)
      End If

      idx += 1
    Next

  End Sub ' SetJackpotList

  ' PURPOSE: Configure Grid columns
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - none
  '
  Private Sub GUI_StyleView()
    With Me.dg_jackpot_list

      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, True)

      .Button(BUTTON_TYPE_ADD).Visible = False
      .Button(BUTTON_TYPE_DELETE).Visible = False

      ' Grid Columns

      'Index
      .Column(GRID_COLUMN_INDEX).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_INDEX).Header.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(288)
      .Column(GRID_COLUMN_INDEX).Width = 800
      .Column(GRID_COLUMN_INDEX).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'Name
      .Column(GRID_COLUMN_NAME).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_NAME).Header.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(282)
      .Column(GRID_COLUMN_NAME).Width = 1700
      .Column(GRID_COLUMN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_NAME).Editable = True
      .Column(GRID_COLUMN_NAME).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_NAME).EditionControl.EntryField.IsReadOnly = False
      .Column(GRID_COLUMN_NAME).EditionControl.EntryField.SetFilter(FORMAT_TEXT, 20)

      'Contribution %
      .Column(GRID_COLUMN_CONTRIBUTION).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_CONTRIBUTION).Header.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(283)
      .Column(GRID_COLUMN_CONTRIBUTION).Width = 1500
      .Column(GRID_COLUMN_CONTRIBUTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_CONTRIBUTION).Editable = True
      .Column(GRID_COLUMN_CONTRIBUTION).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_CONTRIBUTION).EditionControl.EntryField.IsReadOnly = False
      .Column(GRID_COLUMN_CONTRIBUTION).EditionControl.EntryField.SetFilter(FORMAT_NUMBER, 5, 2)

      'Min Bet Amount
      .Column(GRID_COLUMN_MIN_BET_AMOUNT).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_MIN_BET_AMOUNT).Header.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(284)
      .Column(GRID_COLUMN_MIN_BET_AMOUNT).Width = 1500
      .Column(GRID_COLUMN_MIN_BET_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_MIN_BET_AMOUNT).Editable = True
      .Column(GRID_COLUMN_MIN_BET_AMOUNT).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_MIN_BET_AMOUNT).EditionControl.EntryField.IsReadOnly = False
      .Column(GRID_COLUMN_MIN_BET_AMOUNT).EditionControl.EntryField.SetFilter(FORMAT_MONEY, MAX_AMOUNT_DIGITS, 2)

      'Min. Amount
      .Column(GRID_COLUMN_MIN_AMOUNT).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_MIN_AMOUNT).Header.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(285)
      .Column(GRID_COLUMN_MIN_AMOUNT).Width = 1500
      .Column(GRID_COLUMN_MIN_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_MIN_AMOUNT).Editable = True
      .Column(GRID_COLUMN_MIN_AMOUNT).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_MIN_AMOUNT).EditionControl.EntryField.IsReadOnly = False
      .Column(GRID_COLUMN_MIN_AMOUNT).EditionControl.EntryField.SetFilter(FORMAT_MONEY, MAX_AMOUNT_DIGITS, 2)

      'Max. Amount
      .Column(GRID_COLUMN_MAX_AMOUNT).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_MAX_AMOUNT).Header.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(286)
      .Column(GRID_COLUMN_MAX_AMOUNT).Width = 1500
      .Column(GRID_COLUMN_MAX_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_MAX_AMOUNT).Editable = True
      .Column(GRID_COLUMN_MAX_AMOUNT).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_MAX_AMOUNT).EditionControl.EntryField.IsReadOnly = False
      .Column(GRID_COLUMN_MAX_AMOUNT).EditionControl.EntryField.SetFilter(FORMAT_MONEY, MAX_AMOUNT_DIGITS, 2)

      'Average Amount
      .Column(GRID_COLUMN_DESIRED_AMOUNT).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_DESIRED_AMOUNT).Header.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(287)
      .Column(GRID_COLUMN_DESIRED_AMOUNT).Width = 1500
      .Column(GRID_COLUMN_DESIRED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_DESIRED_AMOUNT).Editable = True
      .Column(GRID_COLUMN_DESIRED_AMOUNT).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_DESIRED_AMOUNT).EditionControl.EntryField.IsReadOnly = False
      .Column(GRID_COLUMN_DESIRED_AMOUNT).EditionControl.EntryField.SetFilter(FORMAT_MONEY, MAX_AMOUNT_DIGITS, 2)

      'Fequency
      .Column(GRID_COLUMN_FREQUENCY).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_FREQUENCY).Header.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(339)
      .Column(GRID_COLUMN_FREQUENCY).Width = 1700
      .Column(GRID_COLUMN_FREQUENCY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_FREQUENCY).Editable = False

      'Show on WinUP
      .Column(GRID_COLUMN_SHOW_ON_WINUP).Header.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(499) '"Visible On WinUP"
      .Column(GRID_COLUMN_SHOW_ON_WINUP).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_SHOW_ON_WINUP).Width = IIf(Me.m_winUp_enabled, 1000, 0)
      .Column(GRID_COLUMN_SHOW_ON_WINUP).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_SHOW_ON_WINUP).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
      .Column(GRID_COLUMN_SHOW_ON_WINUP).Editable = True

    End With

  End Sub ' GUI_StyleView

  Private Sub SetWorkingDays(ByVal Days As Integer)
    Dim str_binary As String
    Dim bit_array As Char()

    str_binary = Convert.ToString(Days, 2)
    str_binary = New String("0", 7 - str_binary.Length) + str_binary

    bit_array = str_binary.ToCharArray()

    If (bit_array(6) = "0") Then Me.chk_sunday.Checked = False Else Me.chk_sunday.Checked = True ' Sunday
    If (bit_array(5) = "0") Then Me.chk_monday.Checked = False Else Me.chk_monday.Checked = True ' Monday 
    If (bit_array(4) = "0") Then Me.chk_tuesday.Checked = False Else Me.chk_tuesday.Checked = True ' Tuesday
    If (bit_array(3) = "0") Then Me.chk_wednesday.Checked = False Else Me.chk_wednesday.Checked = True ' Wednesday
    If (bit_array(2) = "0") Then Me.chk_thursday.Checked = False Else Me.chk_thursday.Checked = True ' Thursday
    If (bit_array(1) = "0") Then Me.chk_friday.Checked = False Else Me.chk_friday.Checked = True ' Friday
    If (bit_array(0) = "0") Then Me.chk_saturday.Checked = False Else Me.chk_saturday.Checked = True ' Saturday


  End Sub

  Private Function GetWorkingDays() As Integer
    Dim bit_array As Char()

    bit_array = New String("0000000").ToCharArray()

    bit_array(6) = Convert.ToInt32(Me.chk_sunday.Checked).ToString()  ' Sunday
    bit_array(5) = Convert.ToInt32(Me.chk_monday.Checked).ToString()  ' Monday 
    bit_array(4) = Convert.ToInt32(Me.chk_tuesday.Checked).ToString()  ' Tuesday
    bit_array(3) = Convert.ToInt32(Me.chk_wednesday.Checked).ToString()  ' Wednesday
    bit_array(2) = Convert.ToInt32(Me.chk_thursday.Checked).ToString()  ' Thursday
    bit_array(1) = Convert.ToInt32(Me.chk_friday.Checked).ToString()  ' Friday
    bit_array(0) = Convert.ToInt32(Me.chk_saturday.Checked).ToString()  ' Saturday

    Return Convert.ToInt32(bit_array, 2)

  End Function

  ' PURPOSE:  Cast from boolean to uc_grid checked
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function BoolToGridValue(ByVal enabled As Boolean) As String
    If (enabled) Then
      Return uc_grid.GRID_CHK_CHECKED
    Else
      Return uc_grid.GRID_CHK_UNCHECKED
    End If
  End Function 'BoolToGridValue


  ' PURPOSE:  Cast from  uc_grid checked to boolean
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GridValuetoBool(ByVal enabled As String) As Boolean
    If enabled = uc_grid.GRID_CHK_CHECKED Or enabled = uc_grid.GRID_CHK_CHECKED_DISABLED Then
      Return True
    Else
      Return False
    End If
  End Function 'GridValuetoBool

#End Region

#Region "Public Functions"

  ' PURPOSE: Init form in edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - mdiparent
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Overloads Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
    Me.MdiParent = MdiParent
    Me.DbObjectId = Nothing

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(False)
    End If

  End Sub ' ShowEditItem

#End Region

#Region "Events"

  Private Sub dg_jackpot_list_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer)
    ' DO NOTHING
  End Sub  ' dg_jackpot_list_CellDataChangedEvent

  Private Sub UpdateFrequency()
    Dim jackpot_parameters As CLASS_JACKPOT_PARAMS
    jackpot_parameters = Me.DbReadObject

    Dim idx As Integer
    Dim contribution_pct As Double
    Dim instance_contribution_pct As Double
    Dim instance_average As Double
    Dim last_month_played As Decimal

    Decimal.TryParse(ef_last_month_played.Value, last_month_played)

    idx = 0
    For Each jackpot_instance As CLASS_JACKPOT_PARAMS.TYPE_JACKPOT_INSTANCE In jackpot_parameters.JackpotInstances
      Double.TryParse(ef_contribution.Value, contribution_pct)
      Double.TryParse(Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_CONTRIBUTION).Value, instance_contribution_pct)
      instance_average = GUI_ParseCurrency(Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_DESIRED_AMOUNT).Value)

      If String.IsNullOrEmpty(ef_last_month_played.Value) Then
        Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_FREQUENCY).Value = ""
      Else
        Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_FREQUENCY).Value = CalculateFrequency(last_month_played, _
                                                                                       contribution_pct, _
                                                                                       instance_contribution_pct, _
                                                                                       instance_average)
      End If

      idx += 1
    Next

  End Sub ' UpdateFrequency

  Private Sub ef_contribution_EntryFieldValueChanged() Handles ef_contribution.EntryFieldValueChanged
    UpdateFrequency()
  End Sub

  Private Sub ef_last_month_played_EntryFieldValueChanged() Handles ef_last_month_played.EntryFieldValueChanged
    UpdateFrequency()
  End Sub

  Private Sub dg_jackpot_list_CellDataChangedEvent1(ByVal Row As Integer, ByVal Column As Integer) Handles dg_jackpot_list.CellDataChangedEvent
    UpdateFrequency()
  End Sub

  Private Sub btn_load_last_month_played_ClickEvent() Handles btn_load_last_month_played.ClickEvent
    Dim prev_value As String

    prev_value = ef_last_month_played.Value
    ef_last_month_played.Value = GUI_FormatCurrency(CalculateLastMonthPlayed(False, True))

    If prev_value <> ef_last_month_played.Value Then
      UpdateFrequency()
    End If
  End Sub

  Private Sub cmb_block_mode_ValueChangedEvent() Handles cmb_block_mode.ValueChangedEvent

    Select Case cmb_block_mode.Value
      Case 0
        cmb_block_interval.Visible = False
        lbl_block_interval.Visible = False
        lbl_min_jackpot_block.Visible = False
        cmb_min_jack_block.Visible = False

      Case 1
        cmb_block_interval.Visible = True
        lbl_block_interval.Visible = True
        lbl_min_jackpot_block.Visible = True
        cmb_min_jack_block.Visible = True

      Case 2
        cmb_block_interval.Visible = False
        lbl_block_interval.Visible = False
        lbl_min_jackpot_block.Visible = True
        cmb_min_jack_block.Visible = True

    End Select

  End Sub

#End Region

End Class