<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_providers_sel
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.ef_providers_games_name = New GUI_Controls.uc_entry_field()
    Me.chk_3gs = New System.Windows.Forms.CheckBox()
    Me.chk_games = New System.Windows.Forms.CheckBox()
    Me.lbl_edition = New System.Windows.Forms.Label()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.lbl_edition)
    Me.panel_filter.Controls.Add(Me.chk_games)
    Me.panel_filter.Controls.Add(Me.chk_3gs)
    Me.panel_filter.Controls.Add(Me.ef_providers_games_name)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(912, 105)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_providers_games_name, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_3gs, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_games, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_edition, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 109)
    Me.panel_data.Size = New System.Drawing.Size(912, 369)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(906, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(906, 4)
    '
    'ef_providers_games_name
    '
    Me.ef_providers_games_name.AccessibleRole = System.Windows.Forms.AccessibleRole.IpAddress
    Me.ef_providers_games_name.DoubleValue = 0.0R
    Me.ef_providers_games_name.IntegerValue = 0
    Me.ef_providers_games_name.IsReadOnly = False
    Me.ef_providers_games_name.Location = New System.Drawing.Point(6, 32)
    Me.ef_providers_games_name.Name = "ef_providers_games_name"
    Me.ef_providers_games_name.PlaceHolder = Nothing
    Me.ef_providers_games_name.Size = New System.Drawing.Size(232, 24)
    Me.ef_providers_games_name.SufixText = "Sufix Text"
    Me.ef_providers_games_name.SufixTextVisible = True
    Me.ef_providers_games_name.TabIndex = 0
    Me.ef_providers_games_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_providers_games_name.TextValue = ""
    Me.ef_providers_games_name.TextWidth = 70
    Me.ef_providers_games_name.Value = ""
    Me.ef_providers_games_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_3gs
    '
    Me.chk_3gs.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_3gs.Location = New System.Drawing.Point(-1, 73)
    Me.chk_3gs.Name = "chk_3gs"
    Me.chk_3gs.Size = New System.Drawing.Size(93, 17)
    Me.chk_3gs.TabIndex = 1
    Me.chk_3gs.Text = "x3gs"
    Me.chk_3gs.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_3gs.UseVisualStyleBackColor = True
    '
    'chk_games
    '
    Me.chk_games.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_games.Location = New System.Drawing.Point(118, 72)
    Me.chk_games.Name = "chk_games"
    Me.chk_games.Size = New System.Drawing.Size(120, 17)
    Me.chk_games.TabIndex = 2
    Me.chk_games.Text = "xGames"
    Me.chk_games.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_games.UseVisualStyleBackColor = True
    '
    'lbl_edition
    '
    Me.lbl_edition.AutoSize = True
    Me.lbl_edition.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_edition.Location = New System.Drawing.Point(303, 39)
    Me.lbl_edition.Name = "lbl_edition"
    Me.lbl_edition.Size = New System.Drawing.Size(146, 13)
    Me.lbl_edition.TabIndex = 125
    Me.lbl_edition.Text = "Edici�n s�lo en el centro"
    Me.lbl_edition.Visible = False
    '
    'frm_providers_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(922, 482)
    Me.Name = "frm_providers_sel"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_providers_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_providers_games_name As GUI_Controls.uc_entry_field
  Friend WithEvents chk_games As System.Windows.Forms.CheckBox
  Friend WithEvents chk_3gs As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_edition As System.Windows.Forms.Label
End Class
