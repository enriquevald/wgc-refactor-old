<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_skipped_meters_report
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.uc_pr_list = New GUI_Controls.uc_provider()
    Me.uc_checked_list_meters = New GUI_Controls.uc_checked_list()
    Me.gb_mode = New System.Windows.Forms.GroupBox()
    Me.opt_separate = New System.Windows.Forms.RadioButton()
    Me.opt_grouped = New System.Windows.Forms.RadioButton()
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_mode.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.chk_terminal_location)
    Me.panel_filter.Controls.Add(Me.gb_mode)
    Me.panel_filter.Controls.Add(Me.uc_checked_list_meters)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Size = New System.Drawing.Size(1240, 201)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_checked_list_meters, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_mode, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_terminal_location, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 205)
    Me.panel_data.Size = New System.Drawing.Size(1240, 553)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1234, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1234, 4)
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(7, 8)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(290, 89)
    Me.gb_date.TabIndex = 0
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xInitDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(42, 50)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(240, 25)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2010, 5, 10, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(42, 21)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(240, 25)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2010, 5, 10, 0, 0, 0, 0)
    '
    'uc_pr_list
    '
    Me.uc_pr_list.Location = New System.Drawing.Point(303, 6)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(393, 189)
    Me.uc_pr_list.TabIndex = 3
    Me.uc_pr_list.TerminalListHasValues = False
    '
    'uc_checked_list_meters
    '
    Me.uc_checked_list_meters.GroupBoxText = "xCheckedList"
    Me.uc_checked_list_meters.Location = New System.Drawing.Point(640, 9)
    Me.uc_checked_list_meters.m_resize_width = 351
    Me.uc_checked_list_meters.multiChoice = True
    Me.uc_checked_list_meters.Name = "uc_checked_list_meters"
    Me.uc_checked_list_meters.SelectedIndexes = New Integer(-1) {}
    Me.uc_checked_list_meters.SelectedIndexesList = ""
    Me.uc_checked_list_meters.SelectedIndexesListLevel2 = ""
    Me.uc_checked_list_meters.SelectedValuesArray = New String(-1) {}
    Me.uc_checked_list_meters.SelectedValuesList = ""
    Me.uc_checked_list_meters.SetLevels = 2
    Me.uc_checked_list_meters.Size = New System.Drawing.Size(351, 173)
    Me.uc_checked_list_meters.TabIndex = 4
    Me.uc_checked_list_meters.ValuesArray = New String(-1) {}
    '
    'gb_mode
    '
    Me.gb_mode.Controls.Add(Me.opt_separate)
    Me.gb_mode.Controls.Add(Me.opt_grouped)
    Me.gb_mode.Location = New System.Drawing.Point(6, 127)
    Me.gb_mode.Name = "gb_mode"
    Me.gb_mode.Size = New System.Drawing.Size(290, 89)
    Me.gb_mode.TabIndex = 2
    Me.gb_mode.TabStop = False
    Me.gb_mode.Text = "xMode"
    Me.gb_mode.Visible = False
    '
    'opt_separate
    '
    Me.opt_separate.Location = New System.Drawing.Point(6, 50)
    Me.opt_separate.Name = "opt_separate"
    Me.opt_separate.Size = New System.Drawing.Size(238, 24)
    Me.opt_separate.TabIndex = 1
    Me.opt_separate.Text = "xSeparate"
    '
    'opt_grouped
    '
    Me.opt_grouped.Location = New System.Drawing.Point(6, 20)
    Me.opt_grouped.Name = "opt_grouped"
    Me.opt_grouped.Size = New System.Drawing.Size(238, 24)
    Me.opt_grouped.TabIndex = 0
    Me.opt_grouped.Text = "xGrouped"
    '
    'chk_terminal_location
    '
    Me.chk_terminal_location.Location = New System.Drawing.Point(20, 107)
    Me.chk_terminal_location.Name = "chk_terminal_location"
    Me.chk_terminal_location.Size = New System.Drawing.Size(247, 17)
    Me.chk_terminal_location.TabIndex = 1
    Me.chk_terminal_location.Text = "xShow terminals location"
    '
    'frm_skipped_meters_report
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1248, 762)
    Me.Name = "frm_skipped_meters_report"
    Me.Text = "frm_skipped_meters_report"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gb_mode.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents uc_checked_list_meters As GUI_Controls.uc_checked_list
  Friend WithEvents gb_mode As System.Windows.Forms.GroupBox
  Public WithEvents opt_separate As System.Windows.Forms.RadioButton
  Public WithEvents opt_grouped As System.Windows.Forms.RadioButton
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox
End Class
