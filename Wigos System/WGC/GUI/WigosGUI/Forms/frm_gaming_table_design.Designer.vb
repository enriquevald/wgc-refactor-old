<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_gaming_table_design
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.gb_gaming_tables_to_apply = New System.Windows.Forms.GroupBox
    Me.dg_gaming_tables = New GUI_Controls.uc_grid
    Me.gb_configuration = New System.Windows.Forms.GroupBox
    Me.lbl_properties = New System.Windows.Forms.Label
    Me.pg_properties = New System.Windows.Forms.PropertyGrid
    Me.ef_num_seats = New GUI_Controls.uc_entry_field
    Me.gb_design = New System.Windows.Forms.GroupBox
    Me.panel_table_design = New WSI.Common.GamingTablePanel
    Me.panel_data.SuspendLayout()
    Me.gb_gaming_tables_to_apply.SuspendLayout()
    Me.gb_configuration.SuspendLayout()
    Me.gb_design.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.BackColor = System.Drawing.SystemColors.Control
    Me.panel_data.Controls.Add(Me.gb_gaming_tables_to_apply)
    Me.panel_data.Controls.Add(Me.gb_configuration)
    Me.panel_data.Controls.Add(Me.gb_design)
    Me.panel_data.Size = New System.Drawing.Size(1184, 451)
    '
    'gb_gaming_tables_to_apply
    '
    Me.gb_gaming_tables_to_apply.Controls.Add(Me.dg_gaming_tables)
    Me.gb_gaming_tables_to_apply.Location = New System.Drawing.Point(888, 258)
    Me.gb_gaming_tables_to_apply.Name = "gb_gaming_tables_to_apply"
    Me.gb_gaming_tables_to_apply.Size = New System.Drawing.Size(296, 189)
    Me.gb_gaming_tables_to_apply.TabIndex = 0
    Me.gb_gaming_tables_to_apply.TabStop = False
    Me.gb_gaming_tables_to_apply.Text = "xGamingTablesToApply"
    '
    'dg_gaming_tables
    '
    Me.dg_gaming_tables.CurrentCol = -1
    Me.dg_gaming_tables.CurrentRow = -1
    Me.dg_gaming_tables.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_gaming_tables.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_gaming_tables.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_gaming_tables.Location = New System.Drawing.Point(6, 19)
    Me.dg_gaming_tables.Name = "dg_gaming_tables"
    Me.dg_gaming_tables.PanelRightVisible = False
    Me.dg_gaming_tables.Redraw = True
    Me.dg_gaming_tables.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_gaming_tables.Size = New System.Drawing.Size(284, 164)
    Me.dg_gaming_tables.Sortable = False
    Me.dg_gaming_tables.SortAscending = True
    Me.dg_gaming_tables.SortByCol = 0
    Me.dg_gaming_tables.TabIndex = 14
    Me.dg_gaming_tables.ToolTipped = True
    Me.dg_gaming_tables.TopRow = -2
    '
    'gb_configuration
    '
    Me.gb_configuration.Controls.Add(Me.lbl_properties)
    Me.gb_configuration.Controls.Add(Me.pg_properties)
    Me.gb_configuration.Controls.Add(Me.ef_num_seats)
    Me.gb_configuration.Location = New System.Drawing.Point(888, 4)
    Me.gb_configuration.Name = "gb_configuration"
    Me.gb_configuration.Size = New System.Drawing.Size(296, 249)
    Me.gb_configuration.TabIndex = 0
    Me.gb_configuration.TabStop = False
    Me.gb_configuration.Text = "xGamblingTableName"
    '
    'lbl_properties
    '
    Me.lbl_properties.AutoSize = True
    Me.lbl_properties.Location = New System.Drawing.Point(6, 41)
    Me.lbl_properties.Name = "lbl_properties"
    Me.lbl_properties.Size = New System.Drawing.Size(72, 13)
    Me.lbl_properties.TabIndex = 15
    Me.lbl_properties.Text = "xProperties"
    '
    'pg_properties
    '
    Me.pg_properties.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.pg_properties.HelpVisible = False
    Me.pg_properties.Location = New System.Drawing.Point(6, 59)
    Me.pg_properties.Name = "pg_properties"
    Me.pg_properties.PropertySort = System.Windows.Forms.PropertySort.Categorized
    Me.pg_properties.Size = New System.Drawing.Size(284, 181)
    Me.pg_properties.TabIndex = 0
    Me.pg_properties.ToolbarVisible = False
    '
    'ef_num_seats
    '
    Me.ef_num_seats.DoubleValue = 0
    Me.ef_num_seats.IntegerValue = 0
    Me.ef_num_seats.IsReadOnly = False
    Me.ef_num_seats.Location = New System.Drawing.Point(8, 15)
    Me.ef_num_seats.Name = "ef_num_seats"
    Me.ef_num_seats.Size = New System.Drawing.Size(198, 24)
    Me.ef_num_seats.SufixText = "Sufix Text"
    Me.ef_num_seats.SufixTextVisible = True
    Me.ef_num_seats.TabIndex = 4
    Me.ef_num_seats.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_num_seats.TextValue = ""
    Me.ef_num_seats.TextWidth = 150
    Me.ef_num_seats.Value = ""
    Me.ef_num_seats.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_design
    '
    Me.gb_design.Controls.Add(Me.panel_table_design)
    Me.gb_design.Location = New System.Drawing.Point(4, 4)
    Me.gb_design.Name = "gb_design"
    Me.gb_design.Size = New System.Drawing.Size(875, 443)
    Me.gb_design.TabIndex = 18
    Me.gb_design.TabStop = False
    Me.gb_design.Text = "xDesign"
    '
    'panel_table_design
    '
    Me.panel_table_design.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.panel_table_design.AutoScroll = True
    Me.panel_table_design.BackColor = System.Drawing.SystemColors.Control
    Me.panel_table_design.BackgroundPicture = Nothing
    Me.panel_table_design.CanMoveSeats = True
    Me.panel_table_design.DoubleBufferred = True
    Me.panel_table_design.Location = New System.Drawing.Point(6, 25)
    Me.panel_table_design.ModeDesign = True
    Me.panel_table_design.Name = "panel_table_design"
    Me.panel_table_design.Size = New System.Drawing.Size(863, 404)
    Me.panel_table_design.TabIndex = 1
    '
    'frm_gaming_table_design
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1286, 462)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_gaming_table_design"
    Me.Text = "frm_gaming_table_design"
    Me.panel_data.ResumeLayout(False)
    Me.gb_gaming_tables_to_apply.ResumeLayout(False)
    Me.gb_configuration.ResumeLayout(False)
    Me.gb_configuration.PerformLayout()
    Me.gb_design.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents pg_properties As System.Windows.Forms.PropertyGrid
  Friend WithEvents panel_table_design As WSI.Common.GamingTablePanel
  Friend WithEvents ef_num_seats As GUI_Controls.uc_entry_field
  Friend WithEvents dg_gaming_tables As GUI_Controls.uc_grid
  Friend WithEvents lbl_properties As System.Windows.Forms.Label
  Friend WithEvents gb_design As System.Windows.Forms.GroupBox
  Friend WithEvents gb_configuration As System.Windows.Forms.GroupBox
  Friend WithEvents gb_gaming_tables_to_apply As System.Windows.Forms.GroupBox
End Class
