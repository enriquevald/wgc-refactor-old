<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_meters_history_edit
  Inherits GUI_Controls.frm_base_print

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_meters_history_edit))
    Me.tc_meters = New System.Windows.Forms.TabControl()
    Me.tp_sel = New System.Windows.Forms.TabPage()
    Me.dg_meters_data = New GUI_Controls.uc_grid()
    Me.tp_edit = New System.Windows.Forms.TabPage()
    Me.ef_initial_meter_current = New GUI_Controls.uc_entry_field()
    Me.lbl_initial = New System.Windows.Forms.Label()
    Me.ef_final_meter_current = New GUI_Controls.uc_entry_field()
    Me.ef_initial_meter_new = New GUI_Controls.uc_entry_field()
    Me.ef_final_meter_new = New GUI_Controls.uc_entry_field()
    Me.lbl_delta = New System.Windows.Forms.Label()
    Me.lbl_final = New System.Windows.Forms.Label()
    Me.ef_delta_calculated_current = New GUI_Controls.uc_entry_field()
    Me.txt_rollover_new = New System.Windows.Forms.NumericUpDown()
    Me.ef_delta_calculated_new = New GUI_Controls.uc_entry_field()
    Me.lbl_rollover_num = New System.Windows.Forms.Label()
    Me.lbl_delta_calculated = New System.Windows.Forms.Label()
    Me.txt_rollover_current = New System.Windows.Forms.NumericUpDown()
    Me.ef_delta_system_current = New GUI_Controls.uc_entry_field()
    Me.ef_delta_system_new = New GUI_Controls.uc_entry_field()
    Me.lbl_delta_system = New System.Windows.Forms.Label()
    Me.lbl_new_register = New System.Windows.Forms.Label()
    Me.lbl_system_delta_adjustment = New System.Windows.Forms.Label()
    Me.lbl_max_increment = New System.Windows.Forms.Label()
    Me.cmb_meters = New GUI_Controls.uc_combo()
    Me.lbl_max_value = New System.Windows.Forms.Label()
    Me.cmb_reason = New GUI_Controls.uc_combo()
    Me.btn_reset = New GUI_Controls.uc_button()
    Me.lbl_history_meters_changes = New System.Windows.Forms.Label()
    Me.dg_history_meters = New GUI_Controls.uc_grid()
    Me.lbl_description = New System.Windows.Forms.Label()
    Me.txt_remark = New System.Windows.Forms.TextBox()
    Me.uc_provider = New GUI_Controls.uc_provider()
    Me.dtp_working_day = New GUI_Controls.uc_date_picker()
    Me.cmb_meters_group = New GUI_Controls.uc_combo()
    Me.lbl_working_day = New System.Windows.Forms.Label()
    Me.lbl_meter_group = New System.Windows.Forms.Label()
    Me.lbl_terminal = New System.Windows.Forms.Label()
    Me.uc_site_select = New GUI_Controls.uc_sites_sel()
    Me.lbl_site = New System.Windows.Forms.Label()
    Me.panel_grids.SuspendLayout()
    Me.panel_filter.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.tc_meters.SuspendLayout()
    Me.tp_sel.SuspendLayout()
    Me.tp_edit.SuspendLayout()
    CType(Me.txt_rollover_new, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.txt_rollover_current, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'panel_grids
    '
    Me.panel_grids.Controls.Add(Me.lbl_site)
    Me.panel_grids.Controls.Add(Me.lbl_terminal)
    Me.panel_grids.Controls.Add(Me.lbl_meter_group)
    Me.panel_grids.Controls.Add(Me.tc_meters)
    Me.panel_grids.Controls.Add(Me.lbl_working_day)
    Me.panel_grids.Location = New System.Drawing.Point(5, 224)
    Me.panel_grids.Size = New System.Drawing.Size(1079, 481)
    Me.panel_grids.TabIndex = 2
    Me.panel_grids.Controls.SetChildIndex(Me.lbl_working_day, 0)
    Me.panel_grids.Controls.SetChildIndex(Me.tc_meters, 0)
    Me.panel_grids.Controls.SetChildIndex(Me.panel_buttons, 0)
    Me.panel_grids.Controls.SetChildIndex(Me.lbl_meter_group, 0)
    Me.panel_grids.Controls.SetChildIndex(Me.lbl_terminal, 0)
    Me.panel_grids.Controls.SetChildIndex(Me.lbl_site, 0)
    '
    'panel_buttons
    '
    Me.panel_buttons.Location = New System.Drawing.Point(991, 0)
    Me.panel_buttons.Size = New System.Drawing.Size(88, 481)
    Me.panel_buttons.TabIndex = 1
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.uc_provider)
    Me.panel_filter.Controls.Add(Me.uc_site_select)
    Me.panel_filter.Controls.Add(Me.cmb_meters_group)
    Me.panel_filter.Controls.Add(Me.dtp_working_day)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1079, 203)
    Me.panel_filter.Controls.SetChildIndex(Me.dtp_working_day, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.cmb_meters_group, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_site_select, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_provider, 0)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Location = New System.Drawing.Point(5, 207)
    Me.pn_separator_line.Size = New System.Drawing.Size(1079, 17)
    Me.pn_separator_line.TabIndex = 1
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1079, 4)
    '
    'tc_meters
    '
    Me.tc_meters.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.tc_meters.Controls.Add(Me.tp_sel)
    Me.tc_meters.Controls.Add(Me.tp_edit)
    Me.tc_meters.Location = New System.Drawing.Point(0, 24)
    Me.tc_meters.Name = "tc_meters"
    Me.tc_meters.SelectedIndex = 0
    Me.tc_meters.Size = New System.Drawing.Size(985, 448)
    Me.tc_meters.TabIndex = 0
    '
    'tp_sel
    '
    Me.tp_sel.Controls.Add(Me.dg_meters_data)
    Me.tp_sel.Location = New System.Drawing.Point(4, 22)
    Me.tp_sel.Name = "tp_sel"
    Me.tp_sel.Padding = New System.Windows.Forms.Padding(3)
    Me.tp_sel.Size = New System.Drawing.Size(977, 422)
    Me.tp_sel.TabIndex = 0
    Me.tp_sel.Text = "xSelection"
    Me.tp_sel.UseVisualStyleBackColor = True
    '
    'dg_meters_data
    '
    Me.dg_meters_data.CurrentCol = -1
    Me.dg_meters_data.CurrentRow = -1
    Me.dg_meters_data.Dock = System.Windows.Forms.DockStyle.Fill
    Me.dg_meters_data.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_meters_data.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_meters_data.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_meters_data.Location = New System.Drawing.Point(3, 3)
    Me.dg_meters_data.Name = "dg_meters_data"
    Me.dg_meters_data.PanelRightVisible = False
    Me.dg_meters_data.Redraw = True
    Me.dg_meters_data.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    Me.dg_meters_data.Size = New System.Drawing.Size(971, 416)
    Me.dg_meters_data.Sortable = False
    Me.dg_meters_data.SortAscending = True
    Me.dg_meters_data.SortByCol = 0
    Me.dg_meters_data.TabIndex = 0
    Me.dg_meters_data.ToolTipped = True
    Me.dg_meters_data.TopRow = -2
    Me.dg_meters_data.WordWrap = False
    '
    'tp_edit
    '
    Me.tp_edit.Controls.Add(Me.ef_initial_meter_current)
    Me.tp_edit.Controls.Add(Me.lbl_initial)
    Me.tp_edit.Controls.Add(Me.ef_final_meter_current)
    Me.tp_edit.Controls.Add(Me.ef_initial_meter_new)
    Me.tp_edit.Controls.Add(Me.ef_final_meter_new)
    Me.tp_edit.Controls.Add(Me.lbl_delta)
    Me.tp_edit.Controls.Add(Me.lbl_final)
    Me.tp_edit.Controls.Add(Me.ef_delta_calculated_current)
    Me.tp_edit.Controls.Add(Me.txt_rollover_new)
    Me.tp_edit.Controls.Add(Me.ef_delta_calculated_new)
    Me.tp_edit.Controls.Add(Me.lbl_rollover_num)
    Me.tp_edit.Controls.Add(Me.lbl_delta_calculated)
    Me.tp_edit.Controls.Add(Me.txt_rollover_current)
    Me.tp_edit.Controls.Add(Me.ef_delta_system_current)
    Me.tp_edit.Controls.Add(Me.ef_delta_system_new)
    Me.tp_edit.Controls.Add(Me.lbl_delta_system)
    Me.tp_edit.Controls.Add(Me.lbl_new_register)
    Me.tp_edit.Controls.Add(Me.lbl_system_delta_adjustment)
    Me.tp_edit.Controls.Add(Me.lbl_max_increment)
    Me.tp_edit.Controls.Add(Me.cmb_meters)
    Me.tp_edit.Controls.Add(Me.lbl_max_value)
    Me.tp_edit.Controls.Add(Me.cmb_reason)
    Me.tp_edit.Controls.Add(Me.btn_reset)
    Me.tp_edit.Controls.Add(Me.lbl_history_meters_changes)
    Me.tp_edit.Controls.Add(Me.dg_history_meters)
    Me.tp_edit.Controls.Add(Me.lbl_description)
    Me.tp_edit.Controls.Add(Me.txt_remark)
    Me.tp_edit.Location = New System.Drawing.Point(4, 22)
    Me.tp_edit.Name = "tp_edit"
    Me.tp_edit.Padding = New System.Windows.Forms.Padding(3)
    Me.tp_edit.Size = New System.Drawing.Size(278, 142)
    Me.tp_edit.TabIndex = 1
    Me.tp_edit.Text = "xEdition"
    Me.tp_edit.UseVisualStyleBackColor = True
    '
    'ef_initial_meter_current
    '
    Me.ef_initial_meter_current.DoubleValue = 0.0R
    Me.ef_initial_meter_current.IntegerValue = 0
    Me.ef_initial_meter_current.IsReadOnly = False
    Me.ef_initial_meter_current.Location = New System.Drawing.Point(5, 94)
    Me.ef_initial_meter_current.Name = "ef_initial_meter_current"
    Me.ef_initial_meter_current.PlaceHolder = Nothing
    Me.ef_initial_meter_current.Size = New System.Drawing.Size(245, 24)
    Me.ef_initial_meter_current.SufixText = "Sufix Text"
    Me.ef_initial_meter_current.TabIndex = 11
    Me.ef_initial_meter_current.TabStop = False
    Me.ef_initial_meter_current.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_initial_meter_current.TextValue = ""
    Me.ef_initial_meter_current.TextVisible = False
    Me.ef_initial_meter_current.Value = ""
    Me.ef_initial_meter_current.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_initial
    '
    Me.lbl_initial.Location = New System.Drawing.Point(85, 78)
    Me.lbl_initial.Name = "lbl_initial"
    Me.lbl_initial.Size = New System.Drawing.Size(165, 13)
    Me.lbl_initial.TabIndex = 4
    Me.lbl_initial.Text = "xInitial"
    Me.lbl_initial.TextAlign = System.Drawing.ContentAlignment.TopCenter
    '
    'ef_final_meter_current
    '
    Me.ef_final_meter_current.DoubleValue = 0.0R
    Me.ef_final_meter_current.IntegerValue = 0
    Me.ef_final_meter_current.IsReadOnly = False
    Me.ef_final_meter_current.Location = New System.Drawing.Point(256, 94)
    Me.ef_final_meter_current.Name = "ef_final_meter_current"
    Me.ef_final_meter_current.PlaceHolder = Nothing
    Me.ef_final_meter_current.Size = New System.Drawing.Size(165, 24)
    Me.ef_final_meter_current.SufixText = "Sufix Text"
    Me.ef_final_meter_current.TabIndex = 12
    Me.ef_final_meter_current.TabStop = False
    Me.ef_final_meter_current.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_final_meter_current.TextValue = ""
    Me.ef_final_meter_current.TextVisible = False
    Me.ef_final_meter_current.TextWidth = 0
    Me.ef_final_meter_current.Value = ""
    Me.ef_final_meter_current.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_initial_meter_new
    '
    Me.ef_initial_meter_new.BackColor = System.Drawing.Color.Transparent
    Me.ef_initial_meter_new.DoubleValue = 0.0R
    Me.ef_initial_meter_new.IntegerValue = 0
    Me.ef_initial_meter_new.IsReadOnly = False
    Me.ef_initial_meter_new.Location = New System.Drawing.Point(5, 124)
    Me.ef_initial_meter_new.Name = "ef_initial_meter_new"
    Me.ef_initial_meter_new.PlaceHolder = Nothing
    Me.ef_initial_meter_new.Size = New System.Drawing.Size(245, 24)
    Me.ef_initial_meter_new.SufixText = "Sufix Text"
    Me.ef_initial_meter_new.TabIndex = 17
    Me.ef_initial_meter_new.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_initial_meter_new.TextValue = ""
    Me.ef_initial_meter_new.TextVisible = False
    Me.ef_initial_meter_new.Value = ""
    Me.ef_initial_meter_new.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_final_meter_new
    '
    Me.ef_final_meter_new.DoubleValue = 0.0R
    Me.ef_final_meter_new.IntegerValue = 0
    Me.ef_final_meter_new.IsReadOnly = False
    Me.ef_final_meter_new.Location = New System.Drawing.Point(256, 124)
    Me.ef_final_meter_new.Name = "ef_final_meter_new"
    Me.ef_final_meter_new.PlaceHolder = Nothing
    Me.ef_final_meter_new.Size = New System.Drawing.Size(165, 24)
    Me.ef_final_meter_new.SufixText = "Sufix Text"
    Me.ef_final_meter_new.TabIndex = 18
    Me.ef_final_meter_new.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_final_meter_new.TextValue = ""
    Me.ef_final_meter_new.TextVisible = False
    Me.ef_final_meter_new.TextWidth = 0
    Me.ef_final_meter_new.Value = ""
    Me.ef_final_meter_new.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_delta
    '
    Me.lbl_delta.Location = New System.Drawing.Point(533, 60)
    Me.lbl_delta.Name = "lbl_delta"
    Me.lbl_delta.Size = New System.Drawing.Size(336, 13)
    Me.lbl_delta.TabIndex = 7
    Me.lbl_delta.Text = "xDelta"
    Me.lbl_delta.TextAlign = System.Drawing.ContentAlignment.TopCenter
    '
    'lbl_final
    '
    Me.lbl_final.Location = New System.Drawing.Point(256, 78)
    Me.lbl_final.Name = "lbl_final"
    Me.lbl_final.Size = New System.Drawing.Size(165, 13)
    Me.lbl_final.TabIndex = 5
    Me.lbl_final.Text = "xFinal"
    Me.lbl_final.TextAlign = System.Drawing.ContentAlignment.TopCenter
    '
    'ef_delta_calculated_current
    '
    Me.ef_delta_calculated_current.DoubleValue = 0.0R
    Me.ef_delta_calculated_current.IntegerValue = 0
    Me.ef_delta_calculated_current.IsReadOnly = False
    Me.ef_delta_calculated_current.Location = New System.Drawing.Point(533, 94)
    Me.ef_delta_calculated_current.Name = "ef_delta_calculated_current"
    Me.ef_delta_calculated_current.PlaceHolder = Nothing
    Me.ef_delta_calculated_current.Size = New System.Drawing.Size(165, 24)
    Me.ef_delta_calculated_current.SufixText = "Sufix Text"
    Me.ef_delta_calculated_current.TabIndex = 14
    Me.ef_delta_calculated_current.TabStop = False
    Me.ef_delta_calculated_current.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_delta_calculated_current.TextValue = ""
    Me.ef_delta_calculated_current.TextVisible = False
    Me.ef_delta_calculated_current.TextWidth = 0
    Me.ef_delta_calculated_current.Value = ""
    Me.ef_delta_calculated_current.ValueForeColor = System.Drawing.Color.Blue
    '
    'txt_rollover_new
    '
    Me.txt_rollover_new.Location = New System.Drawing.Point(447, 126)
    Me.txt_rollover_new.Maximum = New Decimal(New Integer() {9, 0, 0, 0})
    Me.txt_rollover_new.Name = "txt_rollover_new"
    Me.txt_rollover_new.Size = New System.Drawing.Size(60, 21)
    Me.txt_rollover_new.TabIndex = 19
    '
    'ef_delta_calculated_new
    '
    Me.ef_delta_calculated_new.DoubleValue = 0.0R
    Me.ef_delta_calculated_new.IntegerValue = 0
    Me.ef_delta_calculated_new.IsReadOnly = False
    Me.ef_delta_calculated_new.Location = New System.Drawing.Point(533, 124)
    Me.ef_delta_calculated_new.Name = "ef_delta_calculated_new"
    Me.ef_delta_calculated_new.PlaceHolder = Nothing
    Me.ef_delta_calculated_new.Size = New System.Drawing.Size(165, 24)
    Me.ef_delta_calculated_new.SufixText = "Sufix Text"
    Me.ef_delta_calculated_new.TabIndex = 20
    Me.ef_delta_calculated_new.TabStop = False
    Me.ef_delta_calculated_new.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_delta_calculated_new.TextValue = ""
    Me.ef_delta_calculated_new.TextVisible = False
    Me.ef_delta_calculated_new.TextWidth = 0
    Me.ef_delta_calculated_new.Value = ""
    Me.ef_delta_calculated_new.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_rollover_num
    '
    Me.lbl_rollover_num.Location = New System.Drawing.Point(427, 78)
    Me.lbl_rollover_num.Name = "lbl_rollover_num"
    Me.lbl_rollover_num.Size = New System.Drawing.Size(100, 13)
    Me.lbl_rollover_num.TabIndex = 6
    Me.lbl_rollover_num.Text = "xRolloverNum"
    Me.lbl_rollover_num.TextAlign = System.Drawing.ContentAlignment.TopCenter
    '
    'lbl_delta_calculated
    '
    Me.lbl_delta_calculated.Location = New System.Drawing.Point(533, 78)
    Me.lbl_delta_calculated.Name = "lbl_delta_calculated"
    Me.lbl_delta_calculated.Size = New System.Drawing.Size(165, 13)
    Me.lbl_delta_calculated.TabIndex = 8
    Me.lbl_delta_calculated.Text = "xCalculated"
    Me.lbl_delta_calculated.TextAlign = System.Drawing.ContentAlignment.TopCenter
    '
    'txt_rollover_current
    '
    Me.txt_rollover_current.Location = New System.Drawing.Point(447, 96)
    Me.txt_rollover_current.Maximum = New Decimal(New Integer() {9, 0, 0, 0})
    Me.txt_rollover_current.Name = "txt_rollover_current"
    Me.txt_rollover_current.Size = New System.Drawing.Size(60, 21)
    Me.txt_rollover_current.TabIndex = 13
    '
    'ef_delta_system_current
    '
    Me.ef_delta_system_current.DoubleValue = 0.0R
    Me.ef_delta_system_current.IntegerValue = 0
    Me.ef_delta_system_current.IsReadOnly = False
    Me.ef_delta_system_current.Location = New System.Drawing.Point(704, 94)
    Me.ef_delta_system_current.Name = "ef_delta_system_current"
    Me.ef_delta_system_current.PlaceHolder = Nothing
    Me.ef_delta_system_current.Size = New System.Drawing.Size(165, 24)
    Me.ef_delta_system_current.SufixText = "Sufix Text"
    Me.ef_delta_system_current.TabIndex = 15
    Me.ef_delta_system_current.TabStop = False
    Me.ef_delta_system_current.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_delta_system_current.TextValue = ""
    Me.ef_delta_system_current.TextVisible = False
    Me.ef_delta_system_current.TextWidth = 0
    Me.ef_delta_system_current.Value = ""
    Me.ef_delta_system_current.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_delta_system_new
    '
    Me.ef_delta_system_new.DoubleValue = 0.0R
    Me.ef_delta_system_new.IntegerValue = 0
    Me.ef_delta_system_new.IsReadOnly = False
    Me.ef_delta_system_new.Location = New System.Drawing.Point(704, 124)
    Me.ef_delta_system_new.Name = "ef_delta_system_new"
    Me.ef_delta_system_new.PlaceHolder = Nothing
    Me.ef_delta_system_new.Size = New System.Drawing.Size(165, 24)
    Me.ef_delta_system_new.SufixText = "Sufix Text"
    Me.ef_delta_system_new.TabIndex = 21
    Me.ef_delta_system_new.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_delta_system_new.TextValue = ""
    Me.ef_delta_system_new.TextVisible = False
    Me.ef_delta_system_new.TextWidth = 0
    Me.ef_delta_system_new.Value = ""
    Me.ef_delta_system_new.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_delta_system
    '
    Me.lbl_delta_system.Location = New System.Drawing.Point(704, 78)
    Me.lbl_delta_system.Name = "lbl_delta_system"
    Me.lbl_delta_system.Size = New System.Drawing.Size(165, 13)
    Me.lbl_delta_system.TabIndex = 9
    Me.lbl_delta_system.Text = "xSystem"
    Me.lbl_delta_system.TextAlign = System.Drawing.ContentAlignment.TopCenter
    '
    'lbl_new_register
    '
    Me.lbl_new_register.AutoSize = True
    Me.lbl_new_register.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_new_register.Location = New System.Drawing.Point(85, 36)
    Me.lbl_new_register.Name = "lbl_new_register"
    Me.lbl_new_register.Size = New System.Drawing.Size(96, 13)
    Me.lbl_new_register.TabIndex = 2
    Me.lbl_new_register.Text = "xNewRegister"
    '
    'lbl_system_delta_adjustment
    '
    Me.lbl_system_delta_adjustment.AutoSize = True
    Me.lbl_system_delta_adjustment.Location = New System.Drawing.Point(6, 198)
    Me.lbl_system_delta_adjustment.Name = "lbl_system_delta_adjustment"
    Me.lbl_system_delta_adjustment.Size = New System.Drawing.Size(122, 13)
    Me.lbl_system_delta_adjustment.TabIndex = 25
    Me.lbl_system_delta_adjustment.Text = "xSystemAdjustment"
    '
    'lbl_max_increment
    '
    Me.lbl_max_increment.AutoSize = True
    Me.lbl_max_increment.Location = New System.Drawing.Point(533, 32)
    Me.lbl_max_increment.Name = "lbl_max_increment"
    Me.lbl_max_increment.Size = New System.Drawing.Size(96, 13)
    Me.lbl_max_increment.TabIndex = 3
    Me.lbl_max_increment.Text = "xMaxIncrement"
    '
    'cmb_meters
    '
    Me.cmb_meters.AllowUnlistedValues = False
    Me.cmb_meters.AutoCompleteMode = False
    Me.cmb_meters.IsReadOnly = False
    Me.cmb_meters.Location = New System.Drawing.Point(5, 9)
    Me.cmb_meters.Name = "cmb_meters"
    Me.cmb_meters.SelectedIndex = -1
    Me.cmb_meters.Size = New System.Drawing.Size(416, 24)
    Me.cmb_meters.SufixText = "Sufix Text"
    Me.cmb_meters.TabIndex = 0
    Me.cmb_meters.TextCombo = Nothing
    Me.cmb_meters.TextVisible = False
    '
    'lbl_max_value
    '
    Me.lbl_max_value.AutoSize = True
    Me.lbl_max_value.Location = New System.Drawing.Point(533, 5)
    Me.lbl_max_value.Name = "lbl_max_value"
    Me.lbl_max_value.Size = New System.Drawing.Size(68, 13)
    Me.lbl_max_value.TabIndex = 1
    Me.lbl_max_value.Text = "xMaxValue"
    '
    'cmb_reason
    '
    Me.cmb_reason.AllowUnlistedValues = False
    Me.cmb_reason.AutoCompleteMode = False
    Me.cmb_reason.IsReadOnly = False
    Me.cmb_reason.Location = New System.Drawing.Point(21, 160)
    Me.cmb_reason.Name = "cmb_reason"
    Me.cmb_reason.SelectedIndex = -1
    Me.cmb_reason.Size = New System.Drawing.Size(229, 24)
    Me.cmb_reason.SufixText = "Sufix Text"
    Me.cmb_reason.TabIndex = 22
    Me.cmb_reason.TextCombo = Nothing
    Me.cmb_reason.TextVisible = False
    Me.cmb_reason.TextWidth = 65
    '
    'btn_reset
    '
    Me.btn_reset.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_reset.Location = New System.Drawing.Point(875, 91)
    Me.btn_reset.Name = "btn_reset"
    Me.btn_reset.Size = New System.Drawing.Size(90, 30)
    Me.btn_reset.TabIndex = 27
    Me.btn_reset.ToolTipped = False
    Me.btn_reset.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'lbl_history_meters_changes
    '
    Me.lbl_history_meters_changes.AutoSize = True
    Me.lbl_history_meters_changes.Location = New System.Drawing.Point(6, 234)
    Me.lbl_history_meters_changes.Name = "lbl_history_meters_changes"
    Me.lbl_history_meters_changes.Size = New System.Drawing.Size(107, 13)
    Me.lbl_history_meters_changes.TabIndex = 26
    Me.lbl_history_meters_changes.Text = "xChanges history"
    Me.lbl_history_meters_changes.TextAlign = System.Drawing.ContentAlignment.TopCenter
    '
    'dg_history_meters
    '
    Me.dg_history_meters.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.dg_history_meters.CurrentCol = -1
    Me.dg_history_meters.CurrentRow = -1
    Me.dg_history_meters.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_history_meters.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_history_meters.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_history_meters.Location = New System.Drawing.Point(3, 254)
    Me.dg_history_meters.Name = "dg_history_meters"
    Me.dg_history_meters.PanelRightVisible = False
    Me.dg_history_meters.Redraw = True
    Me.dg_history_meters.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    Me.dg_history_meters.Size = New System.Drawing.Size(4466, 1565)
    Me.dg_history_meters.Sortable = False
    Me.dg_history_meters.SortAscending = True
    Me.dg_history_meters.SortByCol = 0
    Me.dg_history_meters.TabIndex = 28
    Me.dg_history_meters.ToolTipped = True
    Me.dg_history_meters.TopRow = -2
    Me.dg_history_meters.WordWrap = False
    '
    'lbl_description
    '
    Me.lbl_description.Location = New System.Drawing.Point(325, 166)
    Me.lbl_description.Name = "lbl_description"
    Me.lbl_description.Size = New System.Drawing.Size(96, 13)
    Me.lbl_description.TabIndex = 23
    Me.lbl_description.Text = "xDescription"
    Me.lbl_description.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'txt_remark
    '
    Me.txt_remark.Location = New System.Drawing.Point(447, 160)
    Me.txt_remark.Multiline = True
    Me.txt_remark.Name = "txt_remark"
    Me.txt_remark.Size = New System.Drawing.Size(422, 51)
    Me.txt_remark.TabIndex = 24
    '
    'uc_provider
    '
    Me.uc_provider.FilterByCurrency = False
    Me.uc_provider.Location = New System.Drawing.Point(658, 6)
    Me.uc_provider.Name = "uc_provider"
    Me.uc_provider.Size = New System.Drawing.Size(325, 179)
    Me.uc_provider.TabIndex = 2
    Me.uc_provider.TerminalListHasValues = False
    '
    'dtp_working_day
    '
    Me.dtp_working_day.Checked = True
    Me.dtp_working_day.IsReadOnly = False
    Me.dtp_working_day.Location = New System.Drawing.Point(6, 23)
    Me.dtp_working_day.Name = "dtp_working_day"
    Me.dtp_working_day.ShowCheckBox = False
    Me.dtp_working_day.ShowUpDown = False
    Me.dtp_working_day.Size = New System.Drawing.Size(250, 24)
    Me.dtp_working_day.SufixText = "Sufix Text"
    Me.dtp_working_day.SufixTextVisible = True
    Me.dtp_working_day.TabIndex = 0
    Me.dtp_working_day.TextWidth = 125
    Me.dtp_working_day.Value = New Date(2010, 5, 10, 0, 0, 0, 0)
    '
    'cmb_meters_group
    '
    Me.cmb_meters_group.AllowUnlistedValues = False
    Me.cmb_meters_group.AutoCompleteMode = False
    Me.cmb_meters_group.IsReadOnly = False
    Me.cmb_meters_group.Location = New System.Drawing.Point(6, 54)
    Me.cmb_meters_group.Name = "cmb_meters_group"
    Me.cmb_meters_group.SelectedIndex = -1
    Me.cmb_meters_group.Size = New System.Drawing.Size(329, 24)
    Me.cmb_meters_group.SufixText = "Sufix Text"
    Me.cmb_meters_group.SufixTextVisible = True
    Me.cmb_meters_group.TabIndex = 1
    Me.cmb_meters_group.TextCombo = Nothing
    Me.cmb_meters_group.TextWidth = 125
    '
    'lbl_working_day
    '
    Me.lbl_working_day.AutoSize = True
    Me.lbl_working_day.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_working_day.Location = New System.Drawing.Point(4, 6)
    Me.lbl_working_day.Name = "lbl_working_day"
    Me.lbl_working_day.Size = New System.Drawing.Size(166, 13)
    Me.lbl_working_day.TabIndex = 1
    Me.lbl_working_day.Text = "xWorkingDay: DD/MM/YYYY"
    '
    'lbl_meter_group
    '
    Me.lbl_meter_group.AutoSize = True
    Me.lbl_meter_group.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_meter_group.Location = New System.Drawing.Point(242, 6)
    Me.lbl_meter_group.Name = "lbl_meter_group"
    Me.lbl_meter_group.Size = New System.Drawing.Size(242, 13)
    Me.lbl_meter_group.TabIndex = 2
    Me.lbl_meter_group.Text = "xMeterGroup: ZZZZZZZZZZZZZZZZZZZ"
    '
    'lbl_terminal
    '
    Me.lbl_terminal.AutoSize = True
    Me.lbl_terminal.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_terminal.Location = New System.Drawing.Point(522, 6)
    Me.lbl_terminal.Name = "lbl_terminal"
    Me.lbl_terminal.Size = New System.Drawing.Size(224, 13)
    Me.lbl_terminal.TabIndex = 3
    Me.lbl_terminal.Text = "xTerminal: ZZZZZZZZZZZZZZZZZZZ"
    '
    'uc_site_select
    '
    Me.uc_site_select.Location = New System.Drawing.Point(353, 7)
    Me.uc_site_select.MultiSelect = True
    Me.uc_site_select.Name = "uc_site_select"
    Me.uc_site_select.ShowIsoCode = False
    Me.uc_site_select.ShowMultisiteRow = True
    Me.uc_site_select.Size = New System.Drawing.Size(334, 161)
    Me.uc_site_select.TabIndex = 11
    '
    'lbl_site
    '
    Me.lbl_site.AutoSize = True
    Me.lbl_site.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_site.Location = New System.Drawing.Point(761, 6)
    Me.lbl_site.Name = "lbl_site"
    Me.lbl_site.Size = New System.Drawing.Size(197, 13)
    Me.lbl_site.TabIndex = 4
    Me.lbl_site.Text = "xSite: ZZZZZZZZZZZZZZZZZZZ"
    '
    'frm_meters_history_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1089, 709)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_meters_history_edit"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_meters_history_edit"
    Me.panel_grids.ResumeLayout(False)
    Me.panel_grids.PerformLayout()
    Me.panel_filter.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.tc_meters.ResumeLayout(False)
    Me.tp_sel.ResumeLayout(False)
    Me.tp_edit.ResumeLayout(False)
    Me.tp_edit.PerformLayout()
    CType(Me.txt_rollover_new, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.txt_rollover_current, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents tc_meters As System.Windows.Forms.TabControl
  Friend WithEvents tp_sel As System.Windows.Forms.TabPage
  Friend WithEvents tp_edit As System.Windows.Forms.TabPage
  Friend WithEvents uc_provider As GUI_Controls.uc_provider
  Friend WithEvents dg_meters_data As GUI_Controls.uc_grid
  Friend WithEvents dtp_working_day As GUI_Controls.uc_date_picker
  Friend WithEvents cmb_meters_group As GUI_Controls.uc_combo
  Friend WithEvents lbl_description As System.Windows.Forms.Label
  Friend WithEvents txt_remark As System.Windows.Forms.TextBox
  Friend WithEvents btn_reset As GUI_Controls.uc_button
  Friend WithEvents dg_history_meters As GUI_Controls.uc_grid
  Friend WithEvents lbl_history_meters_changes As System.Windows.Forms.Label
  Friend WithEvents cmb_reason As GUI_Controls.uc_combo
  Friend WithEvents cmb_meters As GUI_Controls.uc_combo
  Friend WithEvents lbl_max_increment As System.Windows.Forms.Label
  Friend WithEvents lbl_max_value As System.Windows.Forms.Label
  Friend WithEvents lbl_working_day As System.Windows.Forms.Label
  Friend WithEvents lbl_meter_group As System.Windows.Forms.Label
  Friend WithEvents lbl_terminal As System.Windows.Forms.Label
  Friend WithEvents lbl_system_delta_adjustment As System.Windows.Forms.Label
  Friend WithEvents lbl_new_register As System.Windows.Forms.Label
  Friend WithEvents ef_initial_meter_current As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_initial As System.Windows.Forms.Label
  Friend WithEvents ef_final_meter_current As GUI_Controls.uc_entry_field
  Friend WithEvents ef_initial_meter_new As GUI_Controls.uc_entry_field
  Friend WithEvents ef_final_meter_new As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_delta As System.Windows.Forms.Label
  Friend WithEvents lbl_final As System.Windows.Forms.Label
  Friend WithEvents ef_delta_calculated_current As GUI_Controls.uc_entry_field
  Friend WithEvents txt_rollover_new As System.Windows.Forms.NumericUpDown
  Friend WithEvents ef_delta_calculated_new As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_rollover_num As System.Windows.Forms.Label
  Friend WithEvents lbl_delta_calculated As System.Windows.Forms.Label
  Friend WithEvents txt_rollover_current As System.Windows.Forms.NumericUpDown
  Friend WithEvents ef_delta_system_current As GUI_Controls.uc_entry_field
  Friend WithEvents ef_delta_system_new As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_delta_system As System.Windows.Forms.Label
  Friend WithEvents uc_site_select As GUI_Controls.uc_sites_sel
  Friend WithEvents lbl_site As System.Windows.Forms.Label
End Class
