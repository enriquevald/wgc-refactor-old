'-------------------------------------------------------------------
' Copyright � 2007-2012 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_customer_notices_priority_edit
' DESCRIPTION:   New or Edit customer notices priority item
' AUTHOR:        Francis Gretz
' CREATION DATE: 04-SEP-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 04-SEP-2017  FGB    Initial version.
' 28-MAR-2018  AGS    Bug 32086:WIGOS-9568 Customer notices - Priority missing mandatory field
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports WSI.Common

Public Class frm_customer_notices_priority_edit
  Inherits frm_base_edit

#Region " Constants "
  'Fields length
  Private Const MAX_PRIORITY_NAME_LEN As Integer = 50
#End Region ' Constants

#Region " Members "
  Private m_input_priority_name As String
  Private m_delete_operation As Boolean
#End Region ' Members

#Region " Overrides "
  ''' <summary>
  ''' Initializes the form id.
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CUSTOMER_NOTICES_PRIORITY_EDIT

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ''' <summary>
  ''' Form controls initialization.
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_InitControls()
    Dim _options_values As CustomerNotices.CustomerNoticesOptionsDict

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    ' Set form Name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8614)                         ' 8614 "Priority Editor"

    chk_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8621)                ' 8621 "Habilitado"
    ef_priority_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8623)           ' 8623 "Nombre"

    ef_priority_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, MAX_PRIORITY_NAME_LEN)

    ' Delete button is only available when editing, not while creating
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_DELETE).Visible = (Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_DELETE).Enabled = (Me.Permissions.Write)

    ' Initialize grid option
    dg_options.Redraw = False

    Try
      _options_values = New CustomerNotices.CustomerNoticesOptionsDict()
      _options_values.SetOption(CustomerNotices.CustomerNoticesPriorityOptions.ShowInCashier, False)
      _options_values.SetOption(CustomerNotices.CustomerNoticesPriorityOptions.ShowInReception, False)
      _options_values.SetOption(CustomerNotices.CustomerNoticesPriorityOptions.ShowInGamingTables, False)

      mdl_custom_notices.FillGridOptions(dg_options, _options_values, False)
    Finally
      dg_options.Redraw = True
    End Try
  End Sub ' GUI_InitControls

  ''' <summary>
  ''' Validate the data presented on the screen.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    ' The following values must be entered
    '   - Name : Mandator

    ' Empty Name
    If ef_priority_name.Value.Length = 0 Then
      ' 123 "Debe introducir un valor."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_priority_name.Text)
      Call ef_priority_name.Focus()

      Return False
    End If


    Try
      Dim _options_values = New CustomerNotices.CustomerNoticesOptionsDict()
      mdl_custom_notices.GetValuesFromGrid(dg_options, _options_values)
      If Not _options_values.Dict.ContainsValue(True) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4678), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(8624))
        Call dg_options.Focus()
        Return False
      End If
    Finally
    End Try
    Return True
  End Function ' GUI_IsScreenDataOk

  ''' <summary>
  ''' Set initial data on the screen.
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    Dim _options_values As CustomerNotices.CustomerNoticesOptionsDict
    Dim _priority As CLASS_CUSTOMER_NOTICES_PRIORITY

    _priority = DbReadObject

    Me.ef_priority_name.Value = _priority.PriorityName
    Me.chk_enabled.Checked = _priority.Enabled

    dg_options.Redraw = False

    Try
      _options_values = New CustomerNotices.CustomerNoticesOptionsDict()
      _options_values.SetOption(CustomerNotices.CustomerNoticesPriorityOptions.ShowInCashier, _priority.ShowInCashier)
      _options_values.SetOption(CustomerNotices.CustomerNoticesPriorityOptions.ShowInReception, _priority.ShowInReception)
      _options_values.SetOption(CustomerNotices.CustomerNoticesPriorityOptions.ShowInGamingTables, _priority.ShowInGamingTables)

      mdl_custom_notices.FillGridOptions(dg_options, _options_values, True)
    Finally
      dg_options.Redraw = True
    End Try
  End Sub ' GUI_SetScreenData

  ''' <summary>
  ''' Get data from the screen.
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_GetScreenData()
    Dim _options_values As CustomerNotices.CustomerNoticesOptionsDict
    Dim _priority As CLASS_CUSTOMER_NOTICES_PRIORITY

    _priority = Me.DbEditedObject

    _priority.PriorityName = ef_priority_name.Value
    _priority.Enabled = chk_enabled.Checked

    'Set flag bit by bit
    _priority.Flags = 0

    'Get options checked in grid
    _options_values = New CustomerNotices.CustomerNoticesOptionsDict()
    mdl_custom_notices.GetValuesFromGrid(dg_options, _options_values)

    _priority.ShowInCashier = _options_values.GetOption(CustomerNotices.CustomerNoticesPriorityOptions.ShowInCashier)
    _priority.ShowInReception = _options_values.GetOption(CustomerNotices.CustomerNoticesPriorityOptions.ShowInReception)
    _priority.ShowInGamingTables = _options_values.GetOption(CustomerNotices.CustomerNoticesPriorityOptions.ShowInGamingTables)
  End Sub ' GUI_GetScreenData

  ''' <summary>
  ''' Database overridable operations. 
  '           Define specific DB operation for this form.
  ''' </summary>
  ''' <param name="DbOperation"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)
    Dim _priority As CLASS_CUSTOMER_NOTICES_PRIORITY
    Dim _aux_nls As String
    Dim _rc As mdl_NLS.ENUM_MB_RESULT
    Dim _is_used_in_notices As Boolean

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        Me.DbEditedObject = New CLASS_CUSTOMER_NOTICES_PRIORITY
        _priority = Me.DbEditedObject
        _priority.PriorityId = 0

        DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          ' 8615 "Se ha producido un error al leer la prioridad %1."
          _priority = Me.DbEditedObject
          _aux_nls = m_input_priority_name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8615), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          ' 8616 "Ya existe una prioridad con el nombre %1."
          _priority = Me.DbEditedObject
          _aux_nls = _priority.PriorityName
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8616), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
          Call ef_priority_name.Focus()
        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          ' 8617 "Se ha producido un error al a�adir la prioridad %1."
          _priority = Me.DbEditedObject
          _aux_nls = _priority.PriorityName
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8617), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          ' 8616 "Ya existe un area con el nombre %1."
          _priority = Me.DbEditedObject
          _aux_nls = _priority.PriorityName
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8616), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
          Call ef_priority_name.Focus()

        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          ' 8618 "Se ha producido un error al modificar la prioridad %1."
          _priority = Me.DbEditedObject
          _aux_nls = _priority.PriorityName
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8618), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE
        m_delete_operation = False
        _is_used_in_notices = False
        _priority = Me.DbEditedObject
        _aux_nls = _priority.PriorityName

        If (Not _priority.DB_CanDeletePriority()) Then
          ' 8632 "No se puede borrar la prioridad %1 porque tiene avisos de clientes asociados"
          _rc = NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8632), _
                                   ENUM_MB_TYPE.MB_TYPE_WARNING, _
                                   ENUM_MB_BTN.MB_BTN_OK, _
                                   ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
                                  _aux_nls)
          _is_used_in_notices = True
        End If

        ' 8620 "�Est� seguro que quiere borrar la prioridad %1?"
        If Not _is_used_in_notices Then
          _rc = NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8620), _
                           ENUM_MB_TYPE.MB_TYPE_WARNING, _
                           ENUM_MB_BTN.MB_BTN_YES_NO, _
                           ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
                          _aux_nls)

          If _rc = ENUM_MB_RESULT.MB_RESULT_YES Then
            m_delete_operation = True
          End If
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK And m_delete_operation Then
          ' 8619 "Se ha producido un error al borrar la prioridad %1."
          _priority = Me.DbEditedObject
          _aux_nls = _priority.PriorityName
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8619), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_DELETE
        If m_delete_operation Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        Else
          DbStatus = ENUM_STATUS.STATUS_ERROR
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE : Manage permissions.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)
  End Sub ' GUI_Permissions

  ' PURPOSE : Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.ef_priority_name

  End Sub ' GUI_SetInitialFocus

  ' PURPOSE : Init form in new mode
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '
  Public Overloads Sub ShowNewItem(ByVal AreaId As Integer)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    DbObjectId = Nothing
    DbStatus = ENUM_STATUS.STATUS_OK

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowNewItem

  ' PURPOSE : Init form in edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '       - UserId
  '       - Username
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Public Overloads Sub ShowEditItem(ByVal PriorityId As Integer)
    Dim _priority As CLASS_CUSTOMER_NOTICES_PRIORITY

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Me.DbObjectId = PriorityId

    'Get priority name
    _priority = New CLASS_CUSTOMER_NOTICES_PRIORITY
    Me.m_input_priority_name = _priority.GetPriorityName(PriorityId)

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If
  End Sub ' ShowEditItem

#End Region ' Overrides

#Region " Private functions "


#End Region 'Private functions

#Region " Events "


#End Region

End Class