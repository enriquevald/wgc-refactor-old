'-------------------------------------------------------------------
' Copyright � 2007-2012 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_terminal_money
' DESCRIPTION:   List terminal-face value quantities.
' AUTHOR:        Susana Sams�
' CREATION DATE: 03-FEB-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 03-FEB-2012  SSC    Initial version.
' 16-MAY-2012  DDM    Modifications: - The gb_notes control and columns the "Pending to be collected" are NOT visible.
'                                    - Change size of the form.
'                                    - The closing time is NOT visible .
'                                    - Change combo for radiobuttons.
'                                    - Lost button is NOT visible.
'                                    - Counters from subtotal are NOT visibles.
' 22-MAY-2012  ACC    Add code and functions for Acceptors funcionality. Change header columns.
' 07-MAY-2014  DRV    Fixed Bug WIG-902: subtotal row must be yellow
' 03-SEP-2014  LEM    Added functionality to show terminal location data.
' 02-FEB-2015  ANM    Increased form's width due there is a new column in grid
' 27-MAR-2015  ANM    Calling GetProviderIdListSelected always returns a query
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports WSI.Common

Public Class frm_terminal_money
  Inherits frm_base_sel

#Region " Constants "
  Private TERMINAL_DATA_COLUMNS As Int32

  ' Grid Columns
  Private GRID_COLUMN_SENTINEL As Integer
  Private GRID_INIT_TERMINAL_DATA As Integer
  Private GRID_COLUMN_PROVIDER As Integer
  Private GRID_COLUMN_TERMINAL_NAME As Integer
  Private GRID_COLUMN_TERMINAL_ID As Integer
  Private GRID_COLUMN_AMOUNT As Integer
  Private GRID_COLUMN_NUM_REPORTED As Integer
  Private GRID_COLUMN_SUM_REPORTED As Integer
  Private GRID_COLUMN_NUM_TRANSF As Integer
  Private GRID_COLUMN_SUM_TRANSF As Integer
  Private GRID_COLUMN_NUM_COLLECT As Integer
  Private GRID_COLUMN_SUM_COLLECT As Integer

  Private Const GRID_HEADER_ROWS As Integer = 2
  Private GRID_COLUMNS As Integer

  Private Const GRID_WIDTH_PROVIDER As Integer = 1800
  Private Const GRID_WIDTH_TERMINAL As Integer = 2300
  Private Const GRID_WIDTH_AMOUNT As Integer = 1600

  ' Sql Columns
  Private Const SQL_COLUMN_PROVIDER As Integer = 0
  Private Const SQL_COLUMN_TERMINAL_ID As Integer = 1
  Private Const SQL_COLUMN_TERMINAL_NAME As Integer = 2
  Private Const SQL_COLUMN_AMOUNT As Integer = 3
  Private Const SQL_COLUMN_NUM_REPORTED As Integer = 4
  Private Const SQL_COLUMN_SUM_REPORTED As Integer = 5
  Private Const SQL_COLUMN_NUM_TRANSF As Integer = 6
  Private Const SQL_COLUMN_SUM_TRANSF As Integer = 7
  Private Const SQL_COLUMN_NUM_COLLECT As Integer = 8
  Private Const SQL_COLUMN_SUM_COLLECT As Integer = 9

  ' Counter
  Private Const COUNTER_OK As Integer = 1
  Private Const COUNTER_DIFF As Integer = 2
#End Region ' Constants

#Region " Structures "


#End Region ' Structures

#Region " Enums "

  Private Enum ENUM_REPORT_TYPE
    BY_TERMINAL = 0 ' by terminal
    BY_FACE = 1     ' by terminal and notes
  End Enum

#End Region ' Enums

#Region " Members "

  'REPORT FILTER
  Private m_date_from As String
  Private m_date_to As String
  Private m_terminal As String
  Private m_type_note As String
  Private m_report_type As String

  Private m_change_prov As Boolean

  'TOTAL
  Dim m_num_reported As Decimal
  Dim m_sum_reported As Decimal
  Dim m_num_transf As Decimal
  Dim m_sum_transf As Decimal
  Dim m_num_collect As Decimal
  Dim m_sum_collect As Decimal

  'SUBTOTAL
  Dim m_ant_terminal As String
  Dim m_num_reported_subtotal As Decimal
  Dim m_sum_reported_subtotal As Decimal
  Dim m_num_transf_subtotal As Decimal
  Dim m_sum_transf_subtotal As Decimal
  Dim m_num_collect_subtotal As Decimal
  Dim m_sum_collect_subtotal As Decimal

  Dim m_table_money_lost As DataTable

  Private m_terminal_report_type As ReportType = ReportType.Provider
  Private m_refresh_grid As Boolean = False

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_TERMINAL_MONEY

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Terminal Money
    Me.Text = GLB_NLS_GUI_INVOICING.GetString(448)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(3)

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(672)
    ' DDM 17-MAY-2012: Button Lost not visible.
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False

    ' Date time filter
    Me.uc_dsl.Init(GLB_NLS_GUI_INVOICING.Id(201), True, False)
    ' DDM 16-MAY-2012: The closing time is not edit
    Me.uc_dsl.ClosingTimeEnabled = False

    ' Notes
    Me.gb_Notes.Text = GLB_NLS_GUI_INVOICING.GetString(37)
    Me.opt_all_notes.Text = GLB_NLS_GUI_INVOICING.GetString(272)
    Me.opt_by_note.Text = GLB_NLS_GUI_INVOICING.GetString(446)
    Me.opt_no_collected.Text = GLB_NLS_GUI_INVOICING.GetString(34)

    ' DDM 16-MAY-2012: The control gb_Notes is not visible
    Me.gb_Notes.Visible = False

    ' Providers - Terminals
    Call Me.uc_pr_list.Init(WSI.Common.Misc.AcceptorTerminalTypeList)

    ' DDM 17-MAY-2012: Changed the combo report type, is NOT visible. Now we using radiobuttons   
    'Group
    Me.gb_group.Text = GLB_NLS_GUI_INVOICING.GetString(377)
    Me.opt_grouped_terminal.Text = GLB_NLS_GUI_INVOICING.GetString(441)
    Me.opt_grouped_terminal_notes.Text = GLB_NLS_GUI_INVOICING.GetString(442)

    ' Events
    AddHandler opt_no_collected.CheckedChanged, AddressOf collected_CheckedChanged

    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)

    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()

    If WSI.Common.TITO.Utils.IsTitoMode Then

    End If

  End Sub ' GUI_FilterReset  

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  '
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.uc_dsl

  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  '
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.opt_no_collected.Checked = False And Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate > Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck  

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  '
  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _sql_str As String
    Dim _having As Boolean = False

    _sql_str = GetStringSQL()

    If Me.opt_no_collected.Checked Then
      m_table_money_lost = GUI_GetTableUsingSQL(_sql_str, 5000, )
    End If

    Return _sql_str

  End Function ' GUI_FilterGetSqlQuery 

  Private Function GetStringSQL() As String

    Dim _str_sql As System.Text.StringBuilder
    _str_sql = New System.Text.StringBuilder()

    _str_sql.AppendLine(" DECLARE @STARTED AS DATETIME ")
    _str_sql.AppendLine(" DECLARE @FINISHED AS DATETIME ")

    _str_sql.AppendLine(" SET @STARTED = " & GUI_FormatDateDB(uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime)))
    _str_sql.AppendLine(" SET @FINISHED = " & GUI_FormatDateDB(ToDate()))

    _str_sql.AppendLine(" SELECT   TE_PROVIDER_ID           ")
    _str_sql.AppendLine(" 	     , TE_TERMINAL_ID           ")
    _str_sql.AppendLine("        , TE_NAME                  ")
    _str_sql.AppendLine(" 	     , MIN(TM_AMOUNT) AS AMOUNT ")

    _str_sql.AppendLine(" 	     , SUM(CASE WHEN TM_REPORTED IS NULL THEN 0 ELSE 1 END) AS NUM_REPORTED               ")
    _str_sql.AppendLine(" 	     , SUM(CASE WHEN TM_REPORTED IS NULL THEN 0 ELSE TM_AMOUNT END) AS SUM_REPORTED       ")
    _str_sql.AppendLine(" 	     , SUM(CASE WHEN TM_TRANSFERRED IS NULL THEN 0 ELSE 1 END) AS NUM_TRANSFERRED         ")
    _str_sql.AppendLine(" 	     , SUM(CASE WHEN TM_TRANSFERRED IS NULL THEN 0 ELSE TM_AMOUNT END) AS SUM_TRANSFERRED ")
    _str_sql.AppendLine(" 	     , SUM(CASE WHEN TM_COLLECTION_ID IS NULL THEN 0 ELSE 1 END) AS NUM_COLLECTED         ")
    _str_sql.AppendLine(" 	     , SUM(CASE WHEN TM_COLLECTION_ID IS NULL THEN 0 ELSE TM_AMOUNT END) AS SUM_COLLECTED ")

    _str_sql.AppendLine("   FROM                            ")
    _str_sql.AppendLine("  	       TERMINAL_MONEY INNER JOIN TERMINALS ON TM_TERMINAL_ID = TE_TERMINAL_ID             ")
    _str_sql.AppendLine("  WHERE   TE_TERMINAL_ID IN " & Me.uc_pr_list.GetProviderIdListSelected())

    If Me.opt_no_collected.Checked Then
      _str_sql.AppendLine("  AND   TM_COLLECTION_ID IS NULL ")
      _str_sql.AppendLine("  AND   TM_INTO_ACCEPTOR = 0 ")
    Else
      If Me.uc_dsl.Enabled Then
        If Me.uc_dsl.ToDateSelected Then
          _str_sql.AppendLine("  AND TM_REPORTED < @FINISHED ")
        End If

        If Me.uc_dsl.FromDateSelected Then
          _str_sql.AppendLine("  AND TM_REPORTED >= @STARTED ")
        End If
      End If
    End If

    _str_sql.AppendLine("  GROUP BY   TE_TERMINAL_ID, TE_PROVIDER_ID, TE_NAME ")
    ' DDM 17-MAY-2012: Change combo for radiobuttons
    If opt_grouped_terminal_notes.Checked Then
      _str_sql.AppendLine(" 	      , TM_AMOUNT  ")
    End If

    If opt_by_note.Checked Then
      _str_sql.AppendLine("  HAVING ")
      _str_sql.AppendLine("           SUM(CASE WHEN TM_REPORTED IS NULL THEN 0 ELSE TM_AMOUNT END) <>	")
      _str_sql.AppendLine("           SUM(CASE WHEN TM_TRANSFERRED IS NULL THEN 0 ELSE TM_AMOUNT END) ")
    End If

    _str_sql.AppendLine("   ORDER BY  TE_TERMINAL_ID ")

    Return _str_sql.ToString()

  End Function

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  '
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _diff_transf_values As Decimal
    Dim _num_transf_diff_values As Decimal
    Dim _diff_collect_values As Decimal
    Dim _num_collect_diff_values As Decimal
    Dim _terminal_data As List(Of Object)

    ' DDM 17-MAY-2012: Change combo for radiobuttons
    If opt_grouped_terminal_notes.Checked And (RowIndex <> 0) And (m_ant_terminal <> DbRow.Value(SQL_COLUMN_TERMINAL_ID)) Then
      Call TerminalSubtotalRow(RowIndex)
      Me.Grid.AddRow()
      RowIndex += 1
    End If

    m_ant_terminal = DbRow.Value(SQL_COLUMN_TERMINAL_ID)

    ' Terminal Report
    _terminal_data = TerminalReport.GetReportDataList(DbRow.Value(SQL_COLUMN_TERMINAL_ID), m_terminal_report_type)
    For _idx As Int32 = 0 To _terminal_data.Count - 1
      If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value Then
        Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
      End If
    Next

    Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_ID).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_TERMINAL_ID), 0)

    ' DDM 17-MAY-2012: Change combo for radiobuttons
    If opt_grouped_terminal_notes.Checked Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_AMOUNT), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' Reported
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_REPORTED).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_NUM_REPORTED), 0)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_REPORTED).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_SUM_REPORTED), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Transferred
    _num_transf_diff_values = DbRow.Value(SQL_COLUMN_NUM_REPORTED) - DbRow.Value(SQL_COLUMN_NUM_TRANSF)
    _diff_transf_values = DbRow.Value(SQL_COLUMN_SUM_REPORTED) - DbRow.Value(SQL_COLUMN_SUM_TRANSF)

    Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_TRANSF).Value = GUI_FormatNumber(_num_transf_diff_values, 0)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_TRANSF).Value = GUI_FormatCurrency(_diff_transf_values, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    If IsMisMatchTransfer(DbRow) Then
      ' DDM 17-MAY-2012: Change combo for radiobuttons
      If opt_grouped_terminal.Checked Then
        If opt_all_notes.Checked Or opt_no_collected.Checked Then
          ' DDM 17-MAY-2012: We have removed orange color in cells
          'Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_TRANSF).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
          'Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_TRANSF).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_ORANGE_00)
          'Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_TRANSF).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
          'Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_TRANSF).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_ORANGE_00)
          'Me.Grid.Cell(RowIndex, GRID_COLUMN_SENTINEL).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_ORANGE_00)
        End If
        Me.Grid.Counter(COUNTER_DIFF).Value += 1
      End If
    Else
      ' DDM 17-MAY-2012: Change combo for radiobuttons
      If opt_grouped_terminal.Checked Then
        Me.Grid.Counter(COUNTER_OK).Value += 1
      End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_TRANSF).Value = ""
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_TRANSF).Value = ""
    End If

    ' Collected
    _num_collect_diff_values = DbRow.Value(SQL_COLUMN_NUM_REPORTED) - DbRow.Value(SQL_COLUMN_NUM_COLLECT)
    _diff_collect_values = DbRow.Value(SQL_COLUMN_SUM_REPORTED) - DbRow.Value(SQL_COLUMN_SUM_COLLECT)

    Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_COLLECT).Value = GUI_FormatNumber(_num_collect_diff_values, 0)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_COLLECT).Value = GUI_FormatCurrency(_diff_collect_values, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'TO DO: pending to be collected
    If IsMisMatchCollect(DbRow) Then
      '  If cmb_report_type.Value = ENUM_REPORT_TYPE.BY_TERMINAL Then
      '    If opt_all_notes.Checked Or opt_no_collected.Checked Then
      '      Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_COLLECT).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
      '      Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_COLLECT).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_ORANGE_00)
      '      Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_COLLECT).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
      '      Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_COLLECT).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_ORANGE_00)
      '      Me.Grid.Cell(RowIndex, GRID_COLUMN_SENTINEL).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_ORANGE_00)
      '    End If
      '    Me.Grid.Counter(COUNTER_DIFF).Value += 1
      '  End If
    Else
      '  If cmb_report_type.Value = ENUM_REPORT_TYPE.BY_TERMINAL Then
      '    Me.Grid.Counter(COUNTER_OK).Value += 1
      '  End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_COLLECT).Value = ""
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_COLLECT).Value = ""
    End If

    'Totals
    m_num_reported += DbRow.Value(SQL_COLUMN_NUM_REPORTED)
    m_sum_reported += DbRow.Value(SQL_COLUMN_SUM_REPORTED)
    m_num_transf += _num_transf_diff_values
    m_sum_transf += _diff_transf_values
    m_num_collect += _num_collect_diff_values
    m_sum_collect += _diff_collect_values

    'Subtotal by terminal (If terminal changes set to 0)
    m_num_reported_subtotal += DbRow.Value(SQL_COLUMN_NUM_REPORTED)
    m_sum_reported_subtotal += DbRow.Value(SQL_COLUMN_SUM_REPORTED)
    m_num_transf_subtotal += _num_transf_diff_values
    m_sum_transf_subtotal += _diff_transf_values
    m_num_collect_subtotal += _num_collect_diff_values
    m_sum_collect_subtotal += _diff_collect_values

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()

    'Call GUI_StyleSheet()

    m_num_reported = 0
    m_sum_reported = 0
    m_num_transf = 0
    m_sum_transf = 0
    m_num_collect = 0
    m_sum_collect = 0

    m_num_reported_subtotal = 0
    m_sum_reported_subtotal = 0
    m_num_transf_subtotal = 0
    m_sum_transf_subtotal = 0
    m_num_collect_subtotal = 0
    m_sum_collect_subtotal = 0

    If m_refresh_grid Then
      Call GUI_StyleSheet()
    End If

    m_refresh_grid = False

  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    Dim _row_index As Integer

    ' DDM 17-MAY-2012: Change combo for radiobuttons
    If opt_grouped_terminal_notes.Checked Then
      If Grid.NumRows > 0 Then
        Me.Grid.AddRow()
        _row_index = Me.Grid.NumRows - 1
        Call TerminalSubtotalRow(_row_index)
      End If
    End If

    Me.Grid.AddRow()
    _row_index = Me.Grid.NumRows - 1

    Me.Grid.Cell(_row_index, GRID_COLUMN_PROVIDER).Value = GLB_NLS_GUI_STATISTICS.GetString(203)

    Me.Grid.Cell(_row_index, GRID_COLUMN_NUM_REPORTED).Value = GUI_FormatNumber(m_num_reported, 0)
    Me.Grid.Cell(_row_index, GRID_COLUMN_SUM_REPORTED).Value = GUI_FormatCurrency(m_sum_reported, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(_row_index, GRID_COLUMN_NUM_TRANSF).Value = GUI_FormatNumber(m_num_transf, 0)
    Me.Grid.Cell(_row_index, GRID_COLUMN_SUM_TRANSF).Value = GUI_FormatCurrency(m_sum_transf, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(_row_index, GRID_COLUMN_NUM_COLLECT).Value = GUI_FormatNumber(m_num_collect, 0)
    Me.Grid.Cell(_row_index, GRID_COLUMN_SUM_COLLECT).Value = GUI_FormatCurrency(m_sum_collect, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'If color is assigned to all file, it loses the color when it's deselected
    Me.Grid.Row(_row_index).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

    If Math.Abs(m_sum_transf) > 0 Then
      Me.Grid.Cell(_row_index, GRID_COLUMN_NUM_TRANSF).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
      Me.Grid.Cell(_row_index, GRID_COLUMN_NUM_TRANSF).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_RED_00)
      Me.Grid.Cell(_row_index, GRID_COLUMN_SUM_TRANSF).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
      Me.Grid.Cell(_row_index, GRID_COLUMN_SUM_TRANSF).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_RED_00)
    End If

    'TO DO: pending to be collected
    'If Math.Abs(m_sum_collect) > 0 Then
    '  Me.Grid.Cell(_row_index, GRID_COLUMN_NUM_COLLECT).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
    '  Me.Grid.Cell(_row_index, GRID_COLUMN_NUM_COLLECT).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_RED_00)
    '  Me.Grid.Cell(_row_index, GRID_COLUMN_SUM_COLLECT).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
    '  Me.Grid.Cell(_row_index, GRID_COLUMN_SUM_COLLECT).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_RED_00)
    'End If


  End Sub 'GUI_AfterLastRow

  ' PURPOSE: Process clicks on data grid (double-clicks) and select button
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        MarkNotesAsLost()
        Return

      Case ENUM_BUTTON.BUTTON_SELECT
        Return

      Case ENUM_BUTTON.BUTTON_FILTER_APPLY
        ' DDM 17-MAY-2012: Change combo for radiobuttons
        If Me.opt_no_collected.Checked And opt_grouped_terminal_notes.Checked Then
          Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = True
        Else
          Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False
        End If

        Call MyBase.GUI_ButtonClick(ButtonId)
        Me.Grid.Counter(0).Value = Me.Grid.Counter(COUNTER_DIFF).Value + Me.Grid.Counter(COUNTER_OK).Value

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)

    End Select
  End Sub ' GUI_ButtonClick

#Region " GUI Reports "

  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    With Me.Grid

    End With

    Call MyBase.GUI_ReportParams(ExcelData)

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    With Me.Grid

    End With

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    ' DDM 16-MAY-2012 m_type_note is NOT visible.
    'PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(37), m_type_note)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_terminal)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(433), m_report_type)

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportUpdateFilters()

    Dim _date As Date

    m_date_from = ""
    m_date_to = ""
    m_terminal = ""
    m_type_note = ""
    m_report_type = ""

    ' Providers - Terminals
    m_terminal = Me.uc_pr_list.GetTerminalReportText()

    ' All notes, pending to be transferred notes or pending to be collected notes
    If opt_all_notes.Checked = True Then
      m_type_note = GLB_NLS_GUI_INVOICING.GetString(272)
    ElseIf opt_by_note.Checked = True Then
      m_type_note = GLB_NLS_GUI_INVOICING.GetString(446)
    ElseIf opt_no_collected.Checked = True Then
      m_type_note = GLB_NLS_GUI_INVOICING.GetString(34)
    Else
    End If


    ' Grouped
    ' DDM 17-MAY-2012: Change combo for radiobuttons
    If opt_grouped_terminal.Checked Then
      m_report_type = GLB_NLS_GUI_INVOICING.GetString(441)
    Else
      m_report_type = GLB_NLS_GUI_INVOICING.GetString(442)
    End If

    ' No collected 
    If opt_no_collected.Checked = False Then
      ' Date
      _date = uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime)
      m_date_from = GUI_FormatDate(_date, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

      If uc_dsl.ToDateSelected Then
        _date = ToDate()
        m_date_to = GUI_FormatDate(_date, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
      End If
    Else
    End If


  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region

#Region " Private Functions"

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub GUI_StyleSheet()
    Dim _terminal_columns As List(Of ColumnSettings)

    Call GridColumnReIndex()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, False)

      .Sortable = True
      ' DDM 17-MAY-2012: We have Removed orange color in cells

      ' SENTINEL
      .Column(GRID_COLUMN_SENTINEL).Header(0).Text = ""
      .Column(GRID_COLUMN_SENTINEL).Header(1).Text = ""
      .Column(GRID_COLUMN_SENTINEL).Width = 200
      .Column(GRID_COLUMN_SENTINEL).HighLightWhenSelected = False
      .Column(GRID_COLUMN_SENTINEL).IsColumnPrintable = False

      '  Terminal Report
      _terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)
      For _idx As Int32 = 0 To _terminal_columns.Count - 1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = " "
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = _terminal_columns(_idx).Header0
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = _terminal_columns(_idx).Width
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = _terminal_columns(_idx).Alignment
        If _terminal_columns(_idx).Column = ReportColumn.Provider Then
          GRID_COLUMN_PROVIDER = GRID_INIT_TERMINAL_DATA + _idx
        End If
        If _terminal_columns(_idx).Column = ReportColumn.Terminal Then
          GRID_COLUMN_TERMINAL_NAME = GRID_INIT_TERMINAL_DATA + _idx
        End If
      Next

      ' TERMINAL ID
      .Column(GRID_COLUMN_TERMINAL_ID).Header(0).Text = " "
      .Column(GRID_COLUMN_TERMINAL_ID).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(300)
      .Column(GRID_COLUMN_TERMINAL_ID).Width = 0
      .Column(GRID_COLUMN_TERMINAL_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' AMOUNT
      .Column(GRID_COLUMN_AMOUNT).Header(0).Text = " "
      .Column(GRID_COLUMN_AMOUNT).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(443)
      .Column(GRID_COLUMN_AMOUNT).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' REPORTED QUANTITY
      .Column(GRID_COLUMN_NUM_REPORTED).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(445)
      .Column(GRID_COLUMN_NUM_REPORTED).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(317)
      .Column(GRID_COLUMN_NUM_REPORTED).Width = 1600
      .Column(GRID_COLUMN_NUM_REPORTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' REPORTED VALUE
      .Column(GRID_COLUMN_SUM_REPORTED).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(445)
      .Column(GRID_COLUMN_SUM_REPORTED).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(488)
      .Column(GRID_COLUMN_SUM_REPORTED).Width = 1900
      .Column(GRID_COLUMN_SUM_REPORTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' TRANSFERRED QUANTITY
      .Column(GRID_COLUMN_NUM_TRANSF).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(446)
      .Column(GRID_COLUMN_NUM_TRANSF).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(317)
      'RRR 21-11-2013: Not shown in TITO mode
      If WSI.Common.TITO.Utils.IsTitoMode Then
        .Column(GRID_COLUMN_NUM_TRANSF).Width = 0
      Else
        .Column(GRID_COLUMN_NUM_TRANSF).Width = 1400
      End If
      .Column(GRID_COLUMN_NUM_TRANSF).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' TRANSFERRED VALUE
      .Column(GRID_COLUMN_SUM_TRANSF).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(446)
      .Column(GRID_COLUMN_SUM_TRANSF).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(488)
      'RRR 21-11-2013: Not shown in TITO mode
      If WSI.Common.TITO.Utils.IsTitoMode Then
        .Column(GRID_COLUMN_SUM_TRANSF).Width = 0
      Else
        .Column(GRID_COLUMN_SUM_TRANSF).Width = 1900
      End If
      .Column(GRID_COLUMN_SUM_TRANSF).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' DDM 16-MAY-2012: The columns COLLECTED QUANTITY and COLLECTED VALUE are NOT visible 
      ' COLLECTED QUANTITY
      .Column(GRID_COLUMN_NUM_COLLECT).Header(0).Text = "" 'GLB_NLS_GUI_INVOICING.GetString(34)
      .Column(GRID_COLUMN_NUM_COLLECT).Header(1).Text = "" ' GLB_NLS_GUI_JACKPOT_MGR.GetString(353)
      .Column(GRID_COLUMN_NUM_COLLECT).Width = 0 '1400
      .Column(GRID_COLUMN_NUM_COLLECT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' COLLECTED VALUE
      .Column(GRID_COLUMN_SUM_COLLECT).Header(0).Text = "" 'GLB_NLS_GUI_INVOICING.GetString(34)
      .Column(GRID_COLUMN_SUM_COLLECT).Header(1).Text = "" 'GLB_NLS_GUI_INVOICING.GetString(447)
      .Column(GRID_COLUMN_SUM_COLLECT).Width = 0 '1900
      .Column(GRID_COLUMN_SUM_COLLECT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub SetDefaultValues()

    Dim _closing_time As Integer
    Dim _inital_time As Date

    ' Providers - Terminals
    Call Me.uc_pr_list.SetDefaultValues()

    ' DDM 17-MAY-2012: The date is Today Opening
    _inital_time = WSI.Common.Misc.TodayOpening()
    _closing_time = _inital_time.Hour

    _inital_time = _inital_time.Date
    Me.uc_dsl.FromDate = _inital_time
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.ToDate = _inital_time.AddDays(1)
    Me.uc_dsl.ToDateSelected = True

    Me.uc_dsl.ClosingTime = _closing_time

    Me.opt_grouped_terminal.Checked = False
    Me.opt_grouped_terminal_notes.Checked = True

    ' DDM 16-MAY-2012: Option by defect opt_all_notes
    'Me.opt_no_collected.Checked = True
    Me.opt_all_notes.Checked = True

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False

    Me.chk_terminal_location.Checked = False

  End Sub ' SetDefaultValues  

  ' PURPOSE: match values reported - transferred
  '
  '  PARAMS:
  '     - INPUT:
  '           - DbRow
  '     - OUTPUT:
  '           - True: value accepted <> value transferred
  '           - False: value accepted = value transferred
  '
  ' RETURNS:
  '     - None
  '
  Private Function IsMisMatchTransfer(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    If (GUI_FormatNumber(DbRow.Value(SQL_COLUMN_NUM_REPORTED), 0) - GUI_FormatNumber(DbRow.Value(SQL_COLUMN_NUM_TRANSF), 0)) <> 0 Then
      Return True
    End If

    Return False

  End Function 'IsMisMatch

  ' PURPOSE: match values reported - collected
  '
  '  PARAMS:
  '     - INPUT:
  '           - DbRow
  '     - OUTPUT:
  '           - True: value accepted <> value collected
  '           - False: value accepted = value collected
  '
  ' RETURNS:
  '     - None
  '
  Private Function IsMisMatchCollect(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    If (GUI_FormatNumber(DbRow.Value(SQL_COLUMN_NUM_REPORTED), 0) - GUI_FormatNumber(DbRow.Value(SQL_COLUMN_NUM_COLLECT), 0)) <> 0 Then
      Return True
    End If

    Return False

  End Function 'IsMisMatch

  ' PURPOSE: Format to Date
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Function ToDate() As Date

    If uc_dsl.ToDateSelected Then
      Return New Date(uc_dsl.ToDate.Year, uc_dsl.ToDate.Month, uc_dsl.ToDate.Day, uc_dsl.ClosingTime, 0, 0)
    Else
      Return New Date(Today.Year, Today.Month, Today.Day, uc_dsl.ClosingTime, 0, 0).AddDays(1)
    End If

  End Function 'ToDate

  ' PURPOSE: Results for each Terminal in a SubtotalRow
  '
  '  PARAMS:
  '     - INPUT:
  '           - DbRow
  '     - OUTPUT:
  '           - True: value accepted <> value transferred
  '           - False: value accepted = value transferred
  '
  ' RETURNS:
  '     - None
  '
  Private Sub TerminalSubtotalRow(ByVal RowIndex)

    Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_NAME).Value = GLB_NLS_GUI_INVOICING.GetString(375)

    Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_REPORTED).Value = GUI_FormatNumber(m_num_reported_subtotal, 0)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_REPORTED).Value = GUI_FormatCurrency(m_sum_reported_subtotal, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_TRANSF).Value = GUI_FormatNumber(m_num_transf_subtotal, 0)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_TRANSF).Value = GUI_FormatCurrency(m_sum_transf_subtotal, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_COLLECT).Value = GUI_FormatNumber(m_num_collect_subtotal, 0)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_COLLECT).Value = GUI_FormatCurrency(m_sum_collect_subtotal, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Color for cell instead all file, because when the file was unselected losed the color.
    Me.Grid.Row(RowIndex).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

    ' Pending transfer
    If Math.Abs(m_sum_transf_subtotal) > 0 Then
      If opt_all_notes.Checked Or opt_no_collected.Checked Then
        ' DDM 17-MAY-2012: We have Removed orange color in cells
        'Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_TRANSF).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
        'Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_TRANSF).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_ORANGE_00)
        'Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_TRANSF).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
        'Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_TRANSF).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_ORANGE_00)
        'Me.Grid.Cell(RowIndex, GRID_COLUMN_SENTINEL).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_ORANGE_00)
      End If
      Me.Grid.Counter(COUNTER_DIFF).Value += 1
    Else
      Me.Grid.Counter(COUNTER_OK).Value += 1
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_TRANSF).Value = ""
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_TRANSF).Value = ""
    End If

    ' Pending Collect
    If Math.Abs(m_sum_collect_subtotal) > 0 Then
      '  If opt_all_notes.Checked Or opt_no_collected.Checked Then
      '    Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_COLLECT).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
      '    Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_COLLECT).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_ORANGE_00)
      '    Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_COLLECT).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
      '    Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_COLLECT).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_ORANGE_00)
      '    Me.Grid.Cell(RowIndex, GRID_COLUMN_SENTINEL).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_ORANGE_00)
      '  End If
      '  Me.Grid.Counter(COUNTER_DIFF).Value += 1
    Else
      'Me.Grid.Counter(COUNTER_OK).Value += 1
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_COLLECT).Value = ""
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_COLLECT).Value = ""
    End If

    'Subtotal by terminal (If terminal changes, set to 0)
    m_num_reported_subtotal = 0
    m_sum_reported_subtotal = 0
    m_num_transf_subtotal = 0
    m_sum_transf_subtotal = 0
    m_num_collect_subtotal = 0
    m_sum_collect_subtotal = 0

  End Sub 'TerminalSubtotalRow

  ' PURPOSE: Mark selected notes pending to collect as money lost
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub MarkNotesAsLost()

    Dim _terminal_id As Int64
    Dim _amount As Decimal
    Dim _rows_grid As DataRow()

    Dim _current_table_money_lost As DataTable
    Dim _table_edit As DataTable
    Dim _row As DataRow

    Dim _frm_edit As frm_money_lost
    Dim _subtotal_selected As Boolean
    Dim _already_updated_as_lost As Boolean

    _subtotal_selected = False
    _already_updated_as_lost = False

    ' If CurrentRow = -1, there is no row selected.
    If Me.Grid.CurrentRow = -1 Then
      Return
    End If

    ' No nominal selected
    If Me.Grid.SelectedRows().Length = 0 Then
      ' 590 "Debe seleccionar al menos un nominal."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(590), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

      Return
    End If

    ' Get selected terminals
    _table_edit = New DataTable()
    _table_edit.Columns.Add("TERMINAL_ID", Type.GetType("System.Int64"))
    _table_edit.Columns.Add("TERMINAL_NAME", Type.GetType("System.String"))
    _table_edit.Columns.Add("AMOUNT", Type.GetType("System.String"))
    _table_edit.Columns.Add("NUM_REPORTED", Type.GetType("System.Int32"))

    'Registers updated
    _current_table_money_lost = GUI_GetTableUsingSQL(GetStringSQL, 5000, )

    For Each _selected_row As Integer In Me.Grid.SelectedRows()
      ' If it's a subtotal row
      If IsDBNull(Me.Grid.Cell(_selected_row, GRID_COLUMN_TERMINAL_ID).Value) Or Me.Grid.Cell(_selected_row, GRID_COLUMN_TERMINAL_ID).Value = "" Then
        _subtotal_selected = True
        Continue For
      End If

      _terminal_id = Me.Grid.Cell(_selected_row, GRID_COLUMN_TERMINAL_ID).Value

      _amount = GUI_ParseCurrency(Me.Grid.Cell(_selected_row, GRID_COLUMN_AMOUNT).Value)
      _rows_grid = m_table_money_lost.Select("TE_TERMINAL_ID = " & _terminal_id & " AND AMOUNT = " & _amount)

      'Check if register has already been modified as lost 
      If _current_table_money_lost.Select("TE_TERMINAL_ID = " & _terminal_id & " AND AMOUNT = " & _amount).Length = 0 Then
        _already_updated_as_lost = True
        Continue For
      Else
        _rows_grid = _current_table_money_lost.Select("TE_TERMINAL_ID = " & _terminal_id & " AND AMOUNT = " & _amount)
      End If

      If _rows_grid.Length > 0 Then
        _row = _table_edit.NewRow()
        _row("TERMINAL_ID") = _terminal_id
        _row("TERMINAL_NAME") = _rows_grid(0)("TE_NAME")
        _row("AMOUNT") = GUI_FormatCurrency(_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        _row("NUM_REPORTED") = _rows_grid(0)("NUM_REPORTED")
        _table_edit.Rows.Add(_row)
      End If
    Next

    If _table_edit.Rows.Count > 0 Then
      _frm_edit = New frm_money_lost
      Call _frm_edit.ShowEditItem(_table_edit)
      _frm_edit = Nothing

    Else
      If _subtotal_selected Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(590), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

      End If

      If _already_updated_as_lost Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(683), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

      End If
    End If

  End Sub ' MarkNotesAsLost

  Private Sub GridColumnReIndex()
    TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(m_terminal_report_type)

    GRID_COLUMN_SENTINEL = 0
    GRID_INIT_TERMINAL_DATA = 1
    GRID_COLUMN_PROVIDER = GRID_INIT_TERMINAL_DATA
    GRID_COLUMN_TERMINAL_NAME = GRID_INIT_TERMINAL_DATA + 1
    GRID_COLUMN_TERMINAL_ID = TERMINAL_DATA_COLUMNS + 1
    GRID_COLUMN_AMOUNT = TERMINAL_DATA_COLUMNS + 2
    GRID_COLUMN_NUM_REPORTED = TERMINAL_DATA_COLUMNS + 3
    GRID_COLUMN_SUM_REPORTED = TERMINAL_DATA_COLUMNS + 4
    GRID_COLUMN_NUM_TRANSF = TERMINAL_DATA_COLUMNS + 5
    GRID_COLUMN_SUM_TRANSF = TERMINAL_DATA_COLUMNS + 6
    GRID_COLUMN_NUM_COLLECT = TERMINAL_DATA_COLUMNS + 7
    GRID_COLUMN_SUM_COLLECT = TERMINAL_DATA_COLUMNS + 8

    GRID_COLUMNS = TERMINAL_DATA_COLUMNS + 9
  End Sub

#End Region ' Private 

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region

#Region " Events "

  ' PURPOSE: if check Pending (chk_no_collected) is checked, Dates control is disabled
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '          - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub collected_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    If Me.opt_no_collected.Checked = True Then
      Me.uc_dsl.Enabled = False
    Else
      Me.uc_dsl.Enabled = True
    End If

  End Sub 'collected_CheckedChanged

  ' PURPOSE: if opt_grouped_terminal is checked, opt_grouped_terminal_notes is not checked
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '          - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub opt_grouped_terminal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_grouped_terminal.CheckedChanged

    If Me.opt_grouped_terminal.Checked Then
      Me.opt_grouped_terminal_notes.Checked = False
    End If

  End Sub

  ' PURPOSE: if opt_grouped_terminal_notes is checked, opt_grouped_terminal is not checked
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '          - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub opt_grouped_terminal_notes_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_grouped_terminal_notes.CheckedChanged

    If Me.opt_grouped_terminal_notes.Checked Then
      Me.opt_grouped_terminal.Checked = False
    End If

  End Sub

  Private Sub chk_terminal_location_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminal_location.CheckedChanged
    If chk_terminal_location.Checked Then
      m_terminal_report_type = ReportType.Provider + ReportType.Location
    Else
      m_terminal_report_type = ReportType.Provider
    End If

    m_refresh_grid = True
  End Sub

#End Region ' Events

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.

  End Sub

End Class ' frm_terminal_money
