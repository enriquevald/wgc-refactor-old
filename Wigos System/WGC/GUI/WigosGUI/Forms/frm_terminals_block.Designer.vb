﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_terminals_block
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_terminals_block))
    Me.gb_block_automatic = New System.Windows.Forms.GroupBox()
    Me.lbl_last_start_trading = New System.Windows.Forms.Label()
    Me.cmb_start_trading = New GUI_Controls.uc_combo()
    Me.ef_start_trading = New GUI_Controls.uc_entry_field()
    Me.lbl_minutes_unblock = New System.Windows.Forms.Label()
    Me.lbl_last_end_trading = New System.Windows.Forms.Label()
    Me.cmb_end_trading = New GUI_Controls.uc_combo()
    Me.ef_end_trading = New GUI_Controls.uc_entry_field()
    Me.lbl_minutes_block = New System.Windows.Forms.Label()
    Me.chk_block = New System.Windows.Forms.CheckBox()
    Me.gb_block_exceptions = New System.Windows.Forms.GroupBox()
    Me.btn_rmv_unblock_all = New System.Windows.Forms.Button()
    Me.btn_rmv_block_all = New System.Windows.Forms.Button()
    Me.tgf_terminals = New GUI_Controls.uc_terminals_group_filter()
    Me.btn_rmv_unblock = New System.Windows.Forms.Button()
    Me.btn_add_unblock = New System.Windows.Forms.Button()
    Me.btn_rmv_block = New System.Windows.Forms.Button()
    Me.btn_add_block = New System.Windows.Forms.Button()
    Me.gb_terminals_ever_block = New System.Windows.Forms.GroupBox()
    Me.dg_terminals_ever_block = New GUI_Controls.uc_grid()
    Me.lbl_info = New System.Windows.Forms.Label()
    Me.gb_terminals_ever_unblock = New System.Windows.Forms.GroupBox()
    Me.dg_terminals_ever_unblock = New GUI_Controls.uc_grid()
    Me.gb_block_manual = New System.Windows.Forms.GroupBox()
    Me.btn_unblock_all_terminals = New System.Windows.Forms.Button()
    Me.btn_block_all_terminals = New System.Windows.Forms.Button()
    Me.tf_terminals_unblock = New GUI_Controls.uc_text_field()
    Me.tf_terminals_block = New GUI_Controls.uc_text_field()
    Me.tf_terminals_total = New GUI_Controls.uc_text_field()
    Me.tf_terminal_session = New GUI_Controls.uc_text_field()
    Me.tmr_terminals_count = New System.Windows.Forms.Timer(Me.components)
    Me.gb_current_status = New System.Windows.Forms.GroupBox()
    Me.tt_terminal_count = New System.Windows.Forms.ToolTip(Me.components)
    Me.panel_data.SuspendLayout()
    Me.gb_block_automatic.SuspendLayout()
    Me.gb_block_exceptions.SuspendLayout()
    Me.gb_terminals_ever_block.SuspendLayout()
    Me.gb_terminals_ever_unblock.SuspendLayout()
    Me.gb_block_manual.SuspendLayout()
    Me.gb_current_status.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_current_status)
    Me.panel_data.Controls.Add(Me.gb_block_manual)
    Me.panel_data.Controls.Add(Me.gb_block_exceptions)
    Me.panel_data.Controls.Add(Me.gb_block_automatic)
    Me.panel_data.Size = New System.Drawing.Size(1211, 607)
    '
    'gb_block_automatic
    '
    Me.gb_block_automatic.Controls.Add(Me.lbl_last_start_trading)
    Me.gb_block_automatic.Controls.Add(Me.cmb_start_trading)
    Me.gb_block_automatic.Controls.Add(Me.ef_start_trading)
    Me.gb_block_automatic.Controls.Add(Me.lbl_minutes_unblock)
    Me.gb_block_automatic.Controls.Add(Me.lbl_last_end_trading)
    Me.gb_block_automatic.Controls.Add(Me.cmb_end_trading)
    Me.gb_block_automatic.Controls.Add(Me.ef_end_trading)
    Me.gb_block_automatic.Controls.Add(Me.lbl_minutes_block)
    Me.gb_block_automatic.Controls.Add(Me.chk_block)
    Me.gb_block_automatic.Location = New System.Drawing.Point(194, 3)
    Me.gb_block_automatic.Name = "gb_block_automatic"
    Me.gb_block_automatic.Size = New System.Drawing.Size(467, 112)
    Me.gb_block_automatic.TabIndex = 0
    Me.gb_block_automatic.TabStop = False
    Me.gb_block_automatic.Text = "xBlockAutomatic"
    '
    'lbl_last_start_trading
    '
    Me.lbl_last_start_trading.AutoSize = True
    Me.lbl_last_start_trading.Location = New System.Drawing.Point(299, 79)
    Me.lbl_last_start_trading.Name = "lbl_last_start_trading"
    Me.lbl_last_start_trading.Size = New System.Drawing.Size(49, 13)
    Me.lbl_last_start_trading.TabIndex = 8
    Me.lbl_last_start_trading.Text = "xto_init"
    '
    'cmb_start_trading
    '
    Me.cmb_start_trading.AllowUnlistedValues = False
    Me.cmb_start_trading.AutoCompleteMode = False
    Me.cmb_start_trading.IsReadOnly = False
    Me.cmb_start_trading.Location = New System.Drawing.Point(202, 73)
    Me.cmb_start_trading.Name = "cmb_start_trading"
    Me.cmb_start_trading.SelectedIndex = -1
    Me.cmb_start_trading.Size = New System.Drawing.Size(90, 24)
    Me.cmb_start_trading.SufixText = "Sufix Text"
    Me.cmb_start_trading.SufixTextVisible = True
    Me.cmb_start_trading.TabIndex = 4
    Me.cmb_start_trading.TextCombo = Nothing
    Me.cmb_start_trading.TextVisible = False
    Me.cmb_start_trading.TextWidth = 0
    '
    'ef_start_trading
    '
    Me.ef_start_trading.DoubleValue = 0.0R
    Me.ef_start_trading.IntegerValue = 0
    Me.ef_start_trading.IsReadOnly = False
    Me.ef_start_trading.Location = New System.Drawing.Point(6, 73)
    Me.ef_start_trading.Name = "ef_start_trading"
    Me.ef_start_trading.PlaceHolder = Nothing
    Me.ef_start_trading.Size = New System.Drawing.Size(133, 24)
    Me.ef_start_trading.SufixText = "Sufix Text"
    Me.ef_start_trading.SufixTextVisible = True
    Me.ef_start_trading.TabIndex = 3
    Me.ef_start_trading.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_start_trading.TextValue = "0"
    Me.ef_start_trading.Value = "0"
    Me.ef_start_trading.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_minutes_unblock
    '
    Me.lbl_minutes_unblock.AutoSize = True
    Me.lbl_minutes_unblock.Location = New System.Drawing.Point(142, 79)
    Me.lbl_minutes_unblock.Name = "lbl_minutes_unblock"
    Me.lbl_minutes_unblock.Size = New System.Drawing.Size(59, 13)
    Me.lbl_minutes_unblock.TabIndex = 6
    Me.lbl_minutes_unblock.Text = "xminutes"
    '
    'lbl_last_end_trading
    '
    Me.lbl_last_end_trading.AutoSize = True
    Me.lbl_last_end_trading.Location = New System.Drawing.Point(299, 53)
    Me.lbl_last_end_trading.Name = "lbl_last_end_trading"
    Me.lbl_last_end_trading.Size = New System.Drawing.Size(56, 13)
    Me.lbl_last_end_trading.TabIndex = 4
    Me.lbl_last_end_trading.Text = "xto_final"
    '
    'cmb_end_trading
    '
    Me.cmb_end_trading.AllowUnlistedValues = False
    Me.cmb_end_trading.AutoCompleteMode = False
    Me.cmb_end_trading.IsReadOnly = False
    Me.cmb_end_trading.Location = New System.Drawing.Point(202, 47)
    Me.cmb_end_trading.Name = "cmb_end_trading"
    Me.cmb_end_trading.SelectedIndex = -1
    Me.cmb_end_trading.Size = New System.Drawing.Size(90, 24)
    Me.cmb_end_trading.SufixText = "Sufix Text"
    Me.cmb_end_trading.SufixTextVisible = True
    Me.cmb_end_trading.TabIndex = 2
    Me.cmb_end_trading.TextCombo = Nothing
    Me.cmb_end_trading.TextVisible = False
    Me.cmb_end_trading.TextWidth = 0
    '
    'ef_end_trading
    '
    Me.ef_end_trading.DoubleValue = 0.0R
    Me.ef_end_trading.IntegerValue = 0
    Me.ef_end_trading.IsReadOnly = False
    Me.ef_end_trading.Location = New System.Drawing.Point(6, 47)
    Me.ef_end_trading.Name = "ef_end_trading"
    Me.ef_end_trading.PlaceHolder = Nothing
    Me.ef_end_trading.Size = New System.Drawing.Size(133, 24)
    Me.ef_end_trading.SufixText = "Sufix Text"
    Me.ef_end_trading.SufixTextVisible = True
    Me.ef_end_trading.TabIndex = 1
    Me.ef_end_trading.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_end_trading.TextValue = "0"
    Me.ef_end_trading.Value = "0"
    Me.ef_end_trading.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_minutes_block
    '
    Me.lbl_minutes_block.AutoSize = True
    Me.lbl_minutes_block.Location = New System.Drawing.Point(142, 53)
    Me.lbl_minutes_block.Name = "lbl_minutes_block"
    Me.lbl_minutes_block.Size = New System.Drawing.Size(59, 13)
    Me.lbl_minutes_block.TabIndex = 2
    Me.lbl_minutes_block.Text = "xminutes"
    '
    'chk_block
    '
    Me.chk_block.AutoSize = True
    Me.chk_block.Location = New System.Drawing.Point(6, 25)
    Me.chk_block.Name = "chk_block"
    Me.chk_block.Size = New System.Drawing.Size(168, 17)
    Me.chk_block.TabIndex = 0
    Me.chk_block.Text = "xBlock/Unblock_terminal"
    Me.chk_block.UseVisualStyleBackColor = True
    '
    'gb_block_exceptions
    '
    Me.gb_block_exceptions.Controls.Add(Me.btn_rmv_unblock_all)
    Me.gb_block_exceptions.Controls.Add(Me.btn_rmv_block_all)
    Me.gb_block_exceptions.Controls.Add(Me.tgf_terminals)
    Me.gb_block_exceptions.Controls.Add(Me.btn_rmv_unblock)
    Me.gb_block_exceptions.Controls.Add(Me.btn_add_unblock)
    Me.gb_block_exceptions.Controls.Add(Me.btn_rmv_block)
    Me.gb_block_exceptions.Controls.Add(Me.btn_add_block)
    Me.gb_block_exceptions.Controls.Add(Me.gb_terminals_ever_block)
    Me.gb_block_exceptions.Controls.Add(Me.lbl_info)
    Me.gb_block_exceptions.Controls.Add(Me.gb_terminals_ever_unblock)
    Me.gb_block_exceptions.Location = New System.Drawing.Point(4, 121)
    Me.gb_block_exceptions.Name = "gb_block_exceptions"
    Me.gb_block_exceptions.Size = New System.Drawing.Size(1198, 484)
    Me.gb_block_exceptions.TabIndex = 2
    Me.gb_block_exceptions.TabStop = False
    Me.gb_block_exceptions.Text = "xBlockException"
    '
    'btn_rmv_unblock_all
    '
    Me.btn_rmv_unblock_all.Font = New System.Drawing.Font("Agency FB", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btn_rmv_unblock_all.Location = New System.Drawing.Point(293, 399)
    Me.btn_rmv_unblock_all.Name = "btn_rmv_unblock_all"
    Me.btn_rmv_unblock_all.Size = New System.Drawing.Size(45, 45)
    Me.btn_rmv_unblock_all.TabIndex = 6
    Me.btn_rmv_unblock_all.Text = "<<"
    Me.btn_rmv_unblock_all.UseVisualStyleBackColor = True
    '
    'btn_rmv_block_all
    '
    Me.btn_rmv_block_all.Font = New System.Drawing.Font("Agency FB", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btn_rmv_block_all.Location = New System.Drawing.Point(293, 188)
    Me.btn_rmv_block_all.Name = "btn_rmv_block_all"
    Me.btn_rmv_block_all.Size = New System.Drawing.Size(45, 45)
    Me.btn_rmv_block_all.TabIndex = 3
    Me.btn_rmv_block_all.Text = "<<"
    Me.btn_rmv_block_all.UseVisualStyleBackColor = True
    '
    'tgf_terminals
    '
    Me.tgf_terminals.ForbiddenGroups = CType(resources.GetObject("tgf_terminals.ForbiddenGroups"), Microsoft.VisualBasic.Collection)
    Me.tgf_terminals.HeightOnExpanded = 422
    Me.tgf_terminals.Location = New System.Drawing.Point(12, 52)
    Me.tgf_terminals.MinimumSize = New System.Drawing.Size(250, 422)
    Me.tgf_terminals.Name = "tgf_terminals"
    Me.tgf_terminals.SelectedIndex = 0
    Me.tgf_terminals.SetDisplayMode = GUI_Controls.uc_terminals_group_filter.DisplayMode.NotCollapsedWithoutButtons
    Me.tgf_terminals.SetInitMode = GUI_Controls.uc_terminals_group_filter.InitMode.NormalMode
    Me.tgf_terminals.ShowGroupBoxLine = True
    Me.tgf_terminals.ShowGroups = Nothing
    Me.tgf_terminals.ShowNodeAll = False
    Me.tgf_terminals.Size = New System.Drawing.Size(275, 422)
    Me.tgf_terminals.TabIndex = 0
    Me.tgf_terminals.ThreeStateCheckMode = False
    '
    'btn_rmv_unblock
    '
    Me.btn_rmv_unblock.Font = New System.Drawing.Font("Agency FB", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btn_rmv_unblock.Location = New System.Drawing.Point(293, 352)
    Me.btn_rmv_unblock.Name = "btn_rmv_unblock"
    Me.btn_rmv_unblock.Size = New System.Drawing.Size(45, 45)
    Me.btn_rmv_unblock.TabIndex = 5
    Me.btn_rmv_unblock.Text = "<"
    Me.btn_rmv_unblock.UseVisualStyleBackColor = True
    '
    'btn_add_unblock
    '
    Me.btn_add_unblock.Font = New System.Drawing.Font("Agency FB", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btn_add_unblock.Location = New System.Drawing.Point(293, 305)
    Me.btn_add_unblock.Name = "btn_add_unblock"
    Me.btn_add_unblock.Size = New System.Drawing.Size(45, 45)
    Me.btn_add_unblock.TabIndex = 4
    Me.btn_add_unblock.Text = ">"
    Me.btn_add_unblock.UseVisualStyleBackColor = True
    '
    'btn_rmv_block
    '
    Me.btn_rmv_block.Font = New System.Drawing.Font("Agency FB", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btn_rmv_block.Location = New System.Drawing.Point(293, 141)
    Me.btn_rmv_block.Name = "btn_rmv_block"
    Me.btn_rmv_block.Size = New System.Drawing.Size(45, 45)
    Me.btn_rmv_block.TabIndex = 2
    Me.btn_rmv_block.Text = "<"
    Me.btn_rmv_block.UseVisualStyleBackColor = True
    '
    'btn_add_block
    '
    Me.btn_add_block.Font = New System.Drawing.Font("Agency FB", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btn_add_block.Location = New System.Drawing.Point(293, 94)
    Me.btn_add_block.Name = "btn_add_block"
    Me.btn_add_block.Size = New System.Drawing.Size(45, 45)
    Me.btn_add_block.TabIndex = 1
    Me.btn_add_block.Text = ">"
    Me.btn_add_block.UseVisualStyleBackColor = True
    '
    'gb_terminals_ever_block
    '
    Me.gb_terminals_ever_block.Controls.Add(Me.dg_terminals_ever_block)
    Me.gb_terminals_ever_block.Location = New System.Drawing.Point(344, 52)
    Me.gb_terminals_ever_block.Name = "gb_terminals_ever_block"
    Me.gb_terminals_ever_block.Size = New System.Drawing.Size(844, 211)
    Me.gb_terminals_ever_block.TabIndex = 7
    Me.gb_terminals_ever_block.TabStop = False
    Me.gb_terminals_ever_block.Text = "xTerminalsEverBlock"
    '
    'dg_terminals_ever_block
    '
    Me.dg_terminals_ever_block.CurrentCol = -1
    Me.dg_terminals_ever_block.CurrentRow = -1
    Me.dg_terminals_ever_block.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_terminals_ever_block.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_terminals_ever_block.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_terminals_ever_block.Location = New System.Drawing.Point(6, 20)
    Me.dg_terminals_ever_block.Name = "dg_terminals_ever_block"
    Me.dg_terminals_ever_block.PanelRightVisible = False
    Me.dg_terminals_ever_block.Redraw = True
    Me.dg_terminals_ever_block.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_terminals_ever_block.Size = New System.Drawing.Size(820, 180)
    Me.dg_terminals_ever_block.Sortable = False
    Me.dg_terminals_ever_block.SortAscending = True
    Me.dg_terminals_ever_block.SortByCol = 0
    Me.dg_terminals_ever_block.TabIndex = 0
    Me.dg_terminals_ever_block.ToolTipped = True
    Me.dg_terminals_ever_block.TopRow = -2
    Me.dg_terminals_ever_block.WordWrap = False
    '
    'lbl_info
    '
    Me.lbl_info.Location = New System.Drawing.Point(8, 23)
    Me.lbl_info.Name = "lbl_info"
    Me.lbl_info.Size = New System.Drawing.Size(809, 19)
    Me.lbl_info.TabIndex = 9
    Me.lbl_info.Text = "xInfo"
    '
    'gb_terminals_ever_unblock
    '
    Me.gb_terminals_ever_unblock.Controls.Add(Me.dg_terminals_ever_unblock)
    Me.gb_terminals_ever_unblock.Location = New System.Drawing.Point(344, 263)
    Me.gb_terminals_ever_unblock.Name = "gb_terminals_ever_unblock"
    Me.gb_terminals_ever_unblock.Size = New System.Drawing.Size(844, 211)
    Me.gb_terminals_ever_unblock.TabIndex = 8
    Me.gb_terminals_ever_unblock.TabStop = False
    Me.gb_terminals_ever_unblock.Text = "xTerminalsEverUnblock"
    '
    'dg_terminals_ever_unblock
    '
    Me.dg_terminals_ever_unblock.CurrentCol = -1
    Me.dg_terminals_ever_unblock.CurrentRow = -1
    Me.dg_terminals_ever_unblock.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_terminals_ever_unblock.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_terminals_ever_unblock.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_terminals_ever_unblock.Location = New System.Drawing.Point(6, 20)
    Me.dg_terminals_ever_unblock.Name = "dg_terminals_ever_unblock"
    Me.dg_terminals_ever_unblock.PanelRightVisible = False
    Me.dg_terminals_ever_unblock.Redraw = True
    Me.dg_terminals_ever_unblock.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_terminals_ever_unblock.Size = New System.Drawing.Size(820, 180)
    Me.dg_terminals_ever_unblock.Sortable = False
    Me.dg_terminals_ever_unblock.SortAscending = True
    Me.dg_terminals_ever_unblock.SortByCol = 0
    Me.dg_terminals_ever_unblock.TabIndex = 0
    Me.dg_terminals_ever_unblock.ToolTipped = True
    Me.dg_terminals_ever_unblock.TopRow = -2
    Me.dg_terminals_ever_unblock.WordWrap = False
    '
    'gb_block_manual
    '
    Me.gb_block_manual.Controls.Add(Me.btn_unblock_all_terminals)
    Me.gb_block_manual.Controls.Add(Me.btn_block_all_terminals)
    Me.gb_block_manual.Location = New System.Drawing.Point(667, 3)
    Me.gb_block_manual.Name = "gb_block_manual"
    Me.gb_block_manual.Size = New System.Drawing.Size(211, 67)
    Me.gb_block_manual.TabIndex = 1
    Me.gb_block_manual.TabStop = False
    Me.gb_block_manual.Text = "xBlockManual"
    '
    'btn_unblock_all_terminals
    '
    Me.btn_unblock_all_terminals.Location = New System.Drawing.Point(111, 20)
    Me.btn_unblock_all_terminals.Name = "btn_unblock_all_terminals"
    Me.btn_unblock_all_terminals.Size = New System.Drawing.Size(90, 40)
    Me.btn_unblock_all_terminals.TabIndex = 3
    Me.btn_unblock_all_terminals.Text = "xUnblockAll"
    Me.btn_unblock_all_terminals.UseVisualStyleBackColor = True
    '
    'btn_block_all_terminals
    '
    Me.btn_block_all_terminals.Location = New System.Drawing.Point(10, 20)
    Me.btn_block_all_terminals.Name = "btn_block_all_terminals"
    Me.btn_block_all_terminals.Size = New System.Drawing.Size(90, 40)
    Me.btn_block_all_terminals.TabIndex = 2
    Me.btn_block_all_terminals.Text = "xBlockAll"
    Me.btn_block_all_terminals.UseVisualStyleBackColor = True
    '
    'tf_terminals_unblock
    '
    Me.tf_terminals_unblock.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.tf_terminals_unblock.IsReadOnly = True
    Me.tf_terminals_unblock.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_terminals_unblock.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_terminals_unblock.Location = New System.Drawing.Point(24, 35)
    Me.tf_terminals_unblock.Name = "tf_terminals_unblock"
    Me.tf_terminals_unblock.Size = New System.Drawing.Size(150, 24)
    Me.tf_terminals_unblock.SufixText = "Sufix Text"
    Me.tf_terminals_unblock.SufixTextVisible = True
    Me.tf_terminals_unblock.TabIndex = 4
    Me.tf_terminals_unblock.TextWidth = 125
    '
    'tf_terminals_block
    '
    Me.tf_terminals_block.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.tf_terminals_block.IsReadOnly = True
    Me.tf_terminals_block.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_terminals_block.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_terminals_block.Location = New System.Drawing.Point(24, 14)
    Me.tf_terminals_block.Name = "tf_terminals_block"
    Me.tf_terminals_block.Size = New System.Drawing.Size(150, 24)
    Me.tf_terminals_block.SufixText = "Sufix Text"
    Me.tf_terminals_block.SufixTextVisible = True
    Me.tf_terminals_block.TabIndex = 2
    Me.tf_terminals_block.TextWidth = 125
    '
    'tf_terminals_total
    '
    Me.tf_terminals_total.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.tf_terminals_total.IsReadOnly = True
    Me.tf_terminals_total.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_terminals_total.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_terminals_total.Location = New System.Drawing.Point(24, 56)
    Me.tf_terminals_total.Name = "tf_terminals_total"
    Me.tf_terminals_total.Size = New System.Drawing.Size(150, 24)
    Me.tf_terminals_total.SufixText = "Sufix Text"
    Me.tf_terminals_total.SufixTextVisible = True
    Me.tf_terminals_total.TabIndex = 0
    Me.tf_terminals_total.TextWidth = 125
    '
    'tf_terminal_session
    '
    Me.tf_terminal_session.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.tf_terminal_session.IsReadOnly = True
    Me.tf_terminal_session.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_terminal_session.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_terminal_session.Location = New System.Drawing.Point(24, 85)
    Me.tf_terminal_session.Name = "tf_terminal_session"
    Me.tf_terminal_session.Size = New System.Drawing.Size(150, 24)
    Me.tf_terminal_session.SufixText = "Sufix Text"
    Me.tf_terminal_session.SufixTextVisible = True
    Me.tf_terminal_session.TabIndex = 1
    Me.tf_terminal_session.TextWidth = 125
    '
    'tmr_terminals_count
    '
    Me.tmr_terminals_count.Enabled = True
    Me.tmr_terminals_count.Interval = 10000
    '
    'gb_current_status
    '
    Me.gb_current_status.Controls.Add(Me.tf_terminals_unblock)
    Me.gb_current_status.Controls.Add(Me.tf_terminals_block)
    Me.gb_current_status.Controls.Add(Me.tf_terminals_total)
    Me.gb_current_status.Controls.Add(Me.tf_terminal_session)
    Me.gb_current_status.Location = New System.Drawing.Point(4, 3)
    Me.gb_current_status.Name = "gb_current_status"
    Me.gb_current_status.Size = New System.Drawing.Size(184, 112)
    Me.gb_current_status.TabIndex = 3
    Me.gb_current_status.TabStop = False
    Me.gb_current_status.Text = "xCurrentStatus"
    '
    'tt_terminal_count
    '
    Me.tt_terminal_count.ShowAlways = True
    '
    'frm_terminals_block
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1313, 615)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_terminals_block"
    Me.Text = "frm_terminals_block"
    Me.panel_data.ResumeLayout(False)
    Me.gb_block_automatic.ResumeLayout(False)
    Me.gb_block_automatic.PerformLayout()
    Me.gb_block_exceptions.ResumeLayout(False)
    Me.gb_terminals_ever_block.ResumeLayout(False)
    Me.gb_terminals_ever_unblock.ResumeLayout(False)
    Me.gb_block_manual.ResumeLayout(False)
    Me.gb_current_status.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_block_automatic As System.Windows.Forms.GroupBox
  Friend WithEvents chk_block As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_last_end_trading As System.Windows.Forms.Label
  Friend WithEvents cmb_end_trading As GUI_Controls.uc_combo
  Friend WithEvents ef_end_trading As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_minutes_block As System.Windows.Forms.Label
  Friend WithEvents lbl_last_start_trading As System.Windows.Forms.Label
  Friend WithEvents cmb_start_trading As GUI_Controls.uc_combo
  Friend WithEvents ef_start_trading As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_minutes_unblock As System.Windows.Forms.Label
  Friend WithEvents gb_block_exceptions As System.Windows.Forms.GroupBox
  Friend WithEvents gb_terminals_ever_unblock As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_info As System.Windows.Forms.Label
  Friend WithEvents gb_terminals_ever_block As System.Windows.Forms.GroupBox
  Friend WithEvents dg_terminals_ever_block As GUI_Controls.uc_grid
  Friend WithEvents dg_terminals_ever_unblock As GUI_Controls.uc_grid
  Friend WithEvents gb_block_manual As System.Windows.Forms.GroupBox
  Friend WithEvents tf_terminal_session As GUI_Controls.uc_text_field
  Friend WithEvents tf_terminals_unblock As GUI_Controls.uc_text_field
  Friend WithEvents tf_terminals_block As GUI_Controls.uc_text_field
  Friend WithEvents tf_terminals_total As GUI_Controls.uc_text_field
  Friend WithEvents gb_current_status As System.Windows.Forms.GroupBox
  Friend WithEvents tmr_terminals_count As System.Windows.Forms.Timer
  Friend WithEvents btn_unblock_all_terminals As System.Windows.Forms.Button
  Friend WithEvents btn_block_all_terminals As System.Windows.Forms.Button
  Friend WithEvents btn_rmv_unblock As System.Windows.Forms.Button
  Friend WithEvents btn_add_unblock As System.Windows.Forms.Button
  Friend WithEvents btn_rmv_block As System.Windows.Forms.Button
  Friend WithEvents btn_add_block As System.Windows.Forms.Button
  Friend WithEvents tt_terminal_count As System.Windows.Forms.ToolTip
  Friend WithEvents tgf_terminals As GUI_Controls.uc_terminals_group_filter
  Friend WithEvents btn_rmv_unblock_all As System.Windows.Forms.Button
  Friend WithEvents btn_rmv_block_all As System.Windows.Forms.Button
End Class
