<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_ads_configuration
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.dg_fields = New GUI_Controls.uc_grid
    Me.tab_config = New System.Windows.Forms.TabControl
    Me.tab_fields = New System.Windows.Forms.TabPage
    Me.lbl_read_only_text = New System.Windows.Forms.Label
    Me.lbl_read_only = New System.Windows.Forms.Label
    Me.tab_gifts = New System.Windows.Forms.TabPage
    Me.chk_split_gifts = New System.Windows.Forms.CheckBox
    Me.gb_gifts_split = New System.Windows.Forms.GroupBox
    Me.lbl_split_caption = New System.Windows.Forms.Label
    Me.lbl_category_button = New System.Windows.Forms.Label
    Me.lbl_category_name = New System.Windows.Forms.Label
    Me.ef_category_1_short = New GUI_Controls.uc_entry_field
    Me.ef_category_3_short = New GUI_Controls.uc_entry_field
    Me.ef_category_2_short = New GUI_Controls.uc_entry_field
    Me.ef_category_1_name = New GUI_Controls.uc_entry_field
    Me.ef_category_3_name = New GUI_Controls.uc_entry_field
    Me.ef_category_2_name = New GUI_Controls.uc_entry_field
    Me.gb_next_gifts = New System.Windows.Forms.GroupBox
    Me.lbl_percentage = New System.Windows.Forms.Label
    Me.lbl_demo = New System.Windows.Forms.Label
    Me.lbl_min_2_show = New System.Windows.Forms.Label
    Me.ef_min_gifts = New GUI_Controls.uc_entry_field
    Me.chk_Percent = New System.Windows.Forms.CheckBox
    Me.lbl_max_2_show = New System.Windows.Forms.Label
    Me.ef_percent = New GUI_Controls.uc_entry_field
    Me.ef_unit = New GUI_Controls.uc_entry_field
    Me.chk_Max = New System.Windows.Forms.CheckBox
    Me.tab_functionalities = New System.Windows.Forms.TabPage
    Me.gb_voucher = New System.Windows.Forms.GroupBox
    Me.tab_voucher = New System.Windows.Forms.TabControl
    Me.tab_voucher_header = New System.Windows.Forms.TabPage
    Me.txt_voucher_header = New System.Windows.Forms.TextBox
    Me.tab_voucher_footer = New System.Windows.Forms.TabPage
    Me.txt_voucher_footer = New System.Windows.Forms.TextBox
    Me.gb_schedule = New System.Windows.Forms.GroupBox
    Me.lbl_schedule_help = New System.Windows.Forms.Label
    Me.dtp_time_from = New GUI_Controls.uc_date_picker
    Me.dtp_time_to = New GUI_Controls.uc_date_picker
    Me.lb_functionality_description = New System.Windows.Forms.Label
    Me.dg_functionalities = New GUI_Controls.uc_grid
    Me.tab_Image = New System.Windows.Forms.TabPage
    Me.lb_img_description = New System.Windows.Forms.Label
    Me.img_preview = New GUI_Controls.uc_image
    Me.dg_images = New GUI_Controls.uc_grid
    Me.tab_messages = New System.Windows.Forms.TabPage
    Me.lbl_message_help = New System.Windows.Forms.Label
    Me.dg_messages = New GUI_Controls.uc_grid
    Me.panel_data.SuspendLayout()
    Me.tab_config.SuspendLayout()
    Me.tab_fields.SuspendLayout()
    Me.tab_gifts.SuspendLayout()
    Me.gb_gifts_split.SuspendLayout()
    Me.gb_next_gifts.SuspendLayout()
    Me.tab_functionalities.SuspendLayout()
    Me.gb_voucher.SuspendLayout()
    Me.tab_voucher.SuspendLayout()
    Me.tab_voucher_header.SuspendLayout()
    Me.tab_voucher_footer.SuspendLayout()
    Me.gb_schedule.SuspendLayout()
    Me.tab_Image.SuspendLayout()
    Me.tab_messages.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.tab_config)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(741, 443)
    '
    'dg_fields
    '
    Me.dg_fields.CurrentCol = -1
    Me.dg_fields.CurrentRow = -1
    Me.dg_fields.Location = New System.Drawing.Point(6, 6)
    Me.dg_fields.Name = "dg_fields"
    Me.dg_fields.PanelRightVisible = False
    Me.dg_fields.Redraw = True
    Me.dg_fields.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_fields.Size = New System.Drawing.Size(712, 380)
    Me.dg_fields.Sortable = False
    Me.dg_fields.SortAscending = True
    Me.dg_fields.SortByCol = 0
    Me.dg_fields.TabIndex = 0
    Me.dg_fields.ToolTipped = True
    Me.dg_fields.TopRow = -2
    '
    'tab_config
    '
    Me.tab_config.Controls.Add(Me.tab_fields)
    Me.tab_config.Controls.Add(Me.tab_gifts)
    Me.tab_config.Controls.Add(Me.tab_functionalities)
    Me.tab_config.Controls.Add(Me.tab_Image)
    Me.tab_config.Controls.Add(Me.tab_messages)
    Me.tab_config.Location = New System.Drawing.Point(3, 0)
    Me.tab_config.Name = "tab_config"
    Me.tab_config.SelectedIndex = 0
    Me.tab_config.Size = New System.Drawing.Size(732, 440)
    Me.tab_config.TabIndex = 2
    '
    'tab_fields
    '
    Me.tab_fields.Controls.Add(Me.lbl_read_only_text)
    Me.tab_fields.Controls.Add(Me.lbl_read_only)
    Me.tab_fields.Controls.Add(Me.dg_fields)
    Me.tab_fields.Location = New System.Drawing.Point(4, 22)
    Me.tab_fields.Name = "tab_fields"
    Me.tab_fields.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_fields.Size = New System.Drawing.Size(724, 414)
    Me.tab_fields.TabIndex = 0
    Me.tab_fields.Text = "xFields"
    Me.tab_fields.UseVisualStyleBackColor = True
    '
    'lbl_read_only_text
    '
    Me.lbl_read_only_text.AutoSize = True
    Me.lbl_read_only_text.Location = New System.Drawing.Point(28, 390)
    Me.lbl_read_only_text.Name = "lbl_read_only_text"
    Me.lbl_read_only_text.Size = New System.Drawing.Size(69, 13)
    Me.lbl_read_only_text.TabIndex = 115
    Me.lbl_read_only_text.Text = "xReadOnly"
    '
    'lbl_read_only
    '
    Me.lbl_read_only.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_read_only.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_read_only.Location = New System.Drawing.Point(6, 389)
    Me.lbl_read_only.Name = "lbl_read_only"
    Me.lbl_read_only.Size = New System.Drawing.Size(16, 16)
    Me.lbl_read_only.TabIndex = 114
    '
    'tab_gifts
    '
    Me.tab_gifts.Controls.Add(Me.chk_split_gifts)
    Me.tab_gifts.Controls.Add(Me.gb_gifts_split)
    Me.tab_gifts.Controls.Add(Me.gb_next_gifts)
    Me.tab_gifts.Location = New System.Drawing.Point(4, 22)
    Me.tab_gifts.Name = "tab_gifts"
    Me.tab_gifts.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_gifts.Size = New System.Drawing.Size(724, 414)
    Me.tab_gifts.TabIndex = 1
    Me.tab_gifts.Text = "xGifts"
    Me.tab_gifts.UseVisualStyleBackColor = True
    '
    'chk_split_gifts
    '
    Me.chk_split_gifts.AutoSize = True
    Me.chk_split_gifts.Location = New System.Drawing.Point(16, 214)
    Me.chk_split_gifts.Name = "chk_split_gifts"
    Me.chk_split_gifts.Size = New System.Drawing.Size(84, 17)
    Me.chk_split_gifts.TabIndex = 1
    Me.chk_split_gifts.Text = "xSplitGifts"
    Me.chk_split_gifts.UseVisualStyleBackColor = True
    '
    'gb_gifts_split
    '
    Me.gb_gifts_split.Controls.Add(Me.lbl_split_caption)
    Me.gb_gifts_split.Controls.Add(Me.lbl_category_button)
    Me.gb_gifts_split.Controls.Add(Me.lbl_category_name)
    Me.gb_gifts_split.Controls.Add(Me.ef_category_1_short)
    Me.gb_gifts_split.Controls.Add(Me.ef_category_3_short)
    Me.gb_gifts_split.Controls.Add(Me.ef_category_2_short)
    Me.gb_gifts_split.Controls.Add(Me.ef_category_1_name)
    Me.gb_gifts_split.Controls.Add(Me.ef_category_3_name)
    Me.gb_gifts_split.Controls.Add(Me.ef_category_2_name)
    Me.gb_gifts_split.Location = New System.Drawing.Point(6, 214)
    Me.gb_gifts_split.Name = "gb_gifts_split"
    Me.gb_gifts_split.Size = New System.Drawing.Size(711, 193)
    Me.gb_gifts_split.TabIndex = 2
    Me.gb_gifts_split.TabStop = False
    '
    'lbl_split_caption
    '
    Me.lbl_split_caption.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_split_caption.ForeColor = System.Drawing.Color.Navy
    Me.lbl_split_caption.Location = New System.Drawing.Point(20, 133)
    Me.lbl_split_caption.Name = "lbl_split_caption"
    Me.lbl_split_caption.Size = New System.Drawing.Size(682, 53)
    Me.lbl_split_caption.TabIndex = 9
    Me.lbl_split_caption.Text = "xSplitCategoriesCaption"
    '
    'lbl_category_button
    '
    Me.lbl_category_button.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_category_button.ForeColor = System.Drawing.SystemColors.ControlText
    Me.lbl_category_button.Location = New System.Drawing.Point(460, 18)
    Me.lbl_category_button.Name = "lbl_category_button"
    Me.lbl_category_button.Size = New System.Drawing.Size(152, 14)
    Me.lbl_category_button.TabIndex = 10
    Me.lbl_category_button.Text = "xCategoryButton"
    Me.lbl_category_button.TextAlign = System.Drawing.ContentAlignment.TopCenter
    '
    'lbl_category_name
    '
    Me.lbl_category_name.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_category_name.ForeColor = System.Drawing.SystemColors.ControlText
    Me.lbl_category_name.Location = New System.Drawing.Point(133, 18)
    Me.lbl_category_name.Name = "lbl_category_name"
    Me.lbl_category_name.Size = New System.Drawing.Size(287, 14)
    Me.lbl_category_name.TabIndex = 9
    Me.lbl_category_name.Text = "xCategoryName"
    Me.lbl_category_name.TextAlign = System.Drawing.ContentAlignment.TopCenter
    '
    'ef_category_1_short
    '
    Me.ef_category_1_short.DoubleValue = 0
    Me.ef_category_1_short.IntegerValue = 0
    Me.ef_category_1_short.IsReadOnly = False
    Me.ef_category_1_short.Location = New System.Drawing.Point(460, 35)
    Me.ef_category_1_short.Name = "ef_category_1_short"
    Me.ef_category_1_short.Size = New System.Drawing.Size(152, 24)
    Me.ef_category_1_short.SufixText = "Sufix Text"
    Me.ef_category_1_short.SufixTextVisible = True
    Me.ef_category_1_short.TabIndex = 1
    Me.ef_category_1_short.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_category_1_short.TextValue = ""
    Me.ef_category_1_short.TextWidth = 0
    Me.ef_category_1_short.Value = ""
    '
    'ef_category_3_short
    '
    Me.ef_category_3_short.DoubleValue = 0
    Me.ef_category_3_short.IntegerValue = 0
    Me.ef_category_3_short.IsReadOnly = False
    Me.ef_category_3_short.Location = New System.Drawing.Point(460, 95)
    Me.ef_category_3_short.Name = "ef_category_3_short"
    Me.ef_category_3_short.Size = New System.Drawing.Size(152, 24)
    Me.ef_category_3_short.SufixText = "Sufix Text"
    Me.ef_category_3_short.SufixTextVisible = True
    Me.ef_category_3_short.TabIndex = 5
    Me.ef_category_3_short.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_category_3_short.TextValue = ""
    Me.ef_category_3_short.TextWidth = 0
    Me.ef_category_3_short.Value = ""
    '
    'ef_category_2_short
    '
    Me.ef_category_2_short.DoubleValue = 0
    Me.ef_category_2_short.IntegerValue = 0
    Me.ef_category_2_short.IsReadOnly = False
    Me.ef_category_2_short.Location = New System.Drawing.Point(460, 65)
    Me.ef_category_2_short.Name = "ef_category_2_short"
    Me.ef_category_2_short.Size = New System.Drawing.Size(152, 24)
    Me.ef_category_2_short.SufixText = "Sufix Text"
    Me.ef_category_2_short.SufixTextVisible = True
    Me.ef_category_2_short.TabIndex = 3
    Me.ef_category_2_short.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_category_2_short.TextValue = ""
    Me.ef_category_2_short.TextWidth = 0
    Me.ef_category_2_short.Value = ""
    '
    'ef_category_1_name
    '
    Me.ef_category_1_name.DoubleValue = 0
    Me.ef_category_1_name.IntegerValue = 0
    Me.ef_category_1_name.IsReadOnly = False
    Me.ef_category_1_name.Location = New System.Drawing.Point(20, 35)
    Me.ef_category_1_name.Name = "ef_category_1_name"
    Me.ef_category_1_name.Size = New System.Drawing.Size(402, 24)
    Me.ef_category_1_name.SufixText = "Sufix Text"
    Me.ef_category_1_name.SufixTextVisible = True
    Me.ef_category_1_name.TabIndex = 0
    Me.ef_category_1_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_category_1_name.TextValue = ""
    Me.ef_category_1_name.TextWidth = 110
    Me.ef_category_1_name.Value = ""
    '
    'ef_category_3_name
    '
    Me.ef_category_3_name.DoubleValue = 0
    Me.ef_category_3_name.IntegerValue = 0
    Me.ef_category_3_name.IsReadOnly = False
    Me.ef_category_3_name.Location = New System.Drawing.Point(20, 95)
    Me.ef_category_3_name.Name = "ef_category_3_name"
    Me.ef_category_3_name.Size = New System.Drawing.Size(402, 24)
    Me.ef_category_3_name.SufixText = "Sufix Text"
    Me.ef_category_3_name.SufixTextVisible = True
    Me.ef_category_3_name.TabIndex = 4
    Me.ef_category_3_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_category_3_name.TextValue = ""
    Me.ef_category_3_name.TextWidth = 110
    Me.ef_category_3_name.Value = ""
    '
    'ef_category_2_name
    '
    Me.ef_category_2_name.DoubleValue = 0
    Me.ef_category_2_name.IntegerValue = 0
    Me.ef_category_2_name.IsReadOnly = False
    Me.ef_category_2_name.Location = New System.Drawing.Point(20, 65)
    Me.ef_category_2_name.Name = "ef_category_2_name"
    Me.ef_category_2_name.Size = New System.Drawing.Size(402, 24)
    Me.ef_category_2_name.SufixText = "Sufix Text"
    Me.ef_category_2_name.SufixTextVisible = True
    Me.ef_category_2_name.TabIndex = 2
    Me.ef_category_2_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_category_2_name.TextValue = ""
    Me.ef_category_2_name.TextWidth = 110
    Me.ef_category_2_name.Value = ""
    '
    'gb_next_gifts
    '
    Me.gb_next_gifts.Controls.Add(Me.lbl_percentage)
    Me.gb_next_gifts.Controls.Add(Me.lbl_demo)
    Me.gb_next_gifts.Controls.Add(Me.lbl_min_2_show)
    Me.gb_next_gifts.Controls.Add(Me.ef_min_gifts)
    Me.gb_next_gifts.Controls.Add(Me.chk_Percent)
    Me.gb_next_gifts.Controls.Add(Me.lbl_max_2_show)
    Me.gb_next_gifts.Controls.Add(Me.ef_percent)
    Me.gb_next_gifts.Controls.Add(Me.chk_Max)
    Me.gb_next_gifts.Controls.Add(Me.ef_unit)
    Me.gb_next_gifts.Location = New System.Drawing.Point(6, 6)
    Me.gb_next_gifts.Name = "gb_next_gifts"
    Me.gb_next_gifts.Size = New System.Drawing.Size(712, 194)
    Me.gb_next_gifts.TabIndex = 0
    Me.gb_next_gifts.TabStop = False
    Me.gb_next_gifts.Text = "xNextGifts"
    '
    'lbl_percentage
    '
    Me.lbl_percentage.AutoSize = True
    Me.lbl_percentage.Location = New System.Drawing.Point(53, 23)
    Me.lbl_percentage.Name = "lbl_percentage"
    Me.lbl_percentage.Size = New System.Drawing.Size(78, 13)
    Me.lbl_percentage.TabIndex = 0
    Me.lbl_percentage.Text = "xPercentage"
    '
    'lbl_demo
    '
    Me.lbl_demo.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_demo.ForeColor = System.Drawing.Color.Navy
    Me.lbl_demo.Location = New System.Drawing.Point(283, 60)
    Me.lbl_demo.Name = "lbl_demo"
    Me.lbl_demo.Size = New System.Drawing.Size(263, 101)
    Me.lbl_demo.TabIndex = 8
    Me.lbl_demo.Text = "xSAMPLE"
    '
    'lbl_min_2_show
    '
    Me.lbl_min_2_show.AutoSize = True
    Me.lbl_min_2_show.Location = New System.Drawing.Point(53, 83)
    Me.lbl_min_2_show.Name = "lbl_min_2_show"
    Me.lbl_min_2_show.Size = New System.Drawing.Size(137, 13)
    Me.lbl_min_2_show.TabIndex = 3
    Me.lbl_min_2_show.Text = "xMinNumItemsToShow"
    '
    'ef_min_gifts
    '
    Me.ef_min_gifts.DoubleValue = 0
    Me.ef_min_gifts.IntegerValue = 0
    Me.ef_min_gifts.IsReadOnly = False
    Me.ef_min_gifts.Location = New System.Drawing.Point(22, 99)
    Me.ef_min_gifts.Name = "ef_min_gifts"
    Me.ef_min_gifts.Size = New System.Drawing.Size(186, 24)
    Me.ef_min_gifts.SufixText = "Sufix Text"
    Me.ef_min_gifts.SufixTextVisible = True
    Me.ef_min_gifts.TabIndex = 4
    Me.ef_min_gifts.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_min_gifts.TextValue = "10%"
    Me.ef_min_gifts.TextVisible = False
    Me.ef_min_gifts.Value = "10%"
    '
    'chk_Percent
    '
    Me.chk_Percent.AutoSize = True
    Me.chk_Percent.Location = New System.Drawing.Point(79, 44)
    Me.chk_Percent.Name = "chk_Percent"
    Me.chk_Percent.Size = New System.Drawing.Size(15, 14)
    Me.chk_Percent.TabIndex = 2
    Me.chk_Percent.UseVisualStyleBackColor = True
    '
    'lbl_max_2_show
    '
    Me.lbl_max_2_show.AutoSize = True
    Me.lbl_max_2_show.Location = New System.Drawing.Point(53, 144)
    Me.lbl_max_2_show.Name = "lbl_max_2_show"
    Me.lbl_max_2_show.Size = New System.Drawing.Size(141, 13)
    Me.lbl_max_2_show.TabIndex = 5
    Me.lbl_max_2_show.Text = "xMaxNumItemsToShow"
    '
    'ef_percent
    '
    Me.ef_percent.DoubleValue = 0
    Me.ef_percent.Enabled = False
    Me.ef_percent.IntegerValue = 0
    Me.ef_percent.IsReadOnly = False
    Me.ef_percent.Location = New System.Drawing.Point(22, 39)
    Me.ef_percent.Name = "ef_percent"
    Me.ef_percent.Size = New System.Drawing.Size(186, 24)
    Me.ef_percent.SufixText = "%"
    Me.ef_percent.SufixTextVisible = True
    Me.ef_percent.SufixTextWidth = 20
    Me.ef_percent.TabIndex = 1
    Me.ef_percent.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_percent.TextValue = "10%"
    Me.ef_percent.TextVisible = False
    Me.ef_percent.Value = "10%"
    '
    'ef_unit
    '
    Me.ef_unit.DoubleValue = 0
    Me.ef_unit.Enabled = False
    Me.ef_unit.IntegerValue = 0
    Me.ef_unit.IsReadOnly = False
    Me.ef_unit.Location = New System.Drawing.Point(22, 160)
    Me.ef_unit.Name = "ef_unit"
    Me.ef_unit.Size = New System.Drawing.Size(186, 24)
    Me.ef_unit.SufixText = "Sufix Text"
    Me.ef_unit.SufixTextVisible = True
    Me.ef_unit.TabIndex = 6
    Me.ef_unit.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_unit.TextValue = "5"
    Me.ef_unit.TextVisible = False
    Me.ef_unit.Value = "5"
    '
    'chk_Max
    '
    Me.chk_Max.AutoSize = True
    Me.chk_Max.Location = New System.Drawing.Point(79, 166)
    Me.chk_Max.Name = "chk_Max"
    Me.chk_Max.Size = New System.Drawing.Size(15, 14)
    Me.chk_Max.TabIndex = 7
    Me.chk_Max.UseVisualStyleBackColor = True
    '
    'tab_functionalities
    '
    Me.tab_functionalities.Controls.Add(Me.gb_voucher)
    Me.tab_functionalities.Controls.Add(Me.gb_schedule)
    Me.tab_functionalities.Controls.Add(Me.lb_functionality_description)
    Me.tab_functionalities.Controls.Add(Me.dg_functionalities)
    Me.tab_functionalities.Location = New System.Drawing.Point(4, 22)
    Me.tab_functionalities.Name = "tab_functionalities"
    Me.tab_functionalities.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_functionalities.Size = New System.Drawing.Size(724, 414)
    Me.tab_functionalities.TabIndex = 2
    Me.tab_functionalities.Text = "xFunctionalities"
    Me.tab_functionalities.UseVisualStyleBackColor = True
    '
    'gb_voucher
    '
    Me.gb_voucher.Controls.Add(Me.tab_voucher)
    Me.gb_voucher.Location = New System.Drawing.Point(320, 244)
    Me.gb_voucher.Name = "gb_voucher"
    Me.gb_voucher.Size = New System.Drawing.Size(398, 159)
    Me.gb_voucher.TabIndex = 11
    Me.gb_voucher.TabStop = False
    Me.gb_voucher.Text = "xVoucher"
    '
    'tab_voucher
    '
    Me.tab_voucher.Controls.Add(Me.tab_voucher_header)
    Me.tab_voucher.Controls.Add(Me.tab_voucher_footer)
    Me.tab_voucher.Location = New System.Drawing.Point(10, 19)
    Me.tab_voucher.Name = "tab_voucher"
    Me.tab_voucher.SelectedIndex = 0
    Me.tab_voucher.Size = New System.Drawing.Size(380, 134)
    Me.tab_voucher.TabIndex = 10
    '
    'tab_voucher_header
    '
    Me.tab_voucher_header.Controls.Add(Me.txt_voucher_header)
    Me.tab_voucher_header.Location = New System.Drawing.Point(4, 22)
    Me.tab_voucher_header.Name = "tab_voucher_header"
    Me.tab_voucher_header.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_voucher_header.Size = New System.Drawing.Size(372, 108)
    Me.tab_voucher_header.TabIndex = 0
    Me.tab_voucher_header.Text = "xHeader"
    Me.tab_voucher_header.UseVisualStyleBackColor = True
    '
    'txt_voucher_header
    '
    Me.txt_voucher_header.Dock = System.Windows.Forms.DockStyle.Fill
    Me.txt_voucher_header.Location = New System.Drawing.Point(3, 3)
    Me.txt_voucher_header.MaxLength = 500
    Me.txt_voucher_header.Multiline = True
    Me.txt_voucher_header.Name = "txt_voucher_header"
    Me.txt_voucher_header.Size = New System.Drawing.Size(366, 102)
    Me.txt_voucher_header.TabIndex = 0
    '
    'tab_voucher_footer
    '
    Me.tab_voucher_footer.Controls.Add(Me.txt_voucher_footer)
    Me.tab_voucher_footer.Location = New System.Drawing.Point(4, 22)
    Me.tab_voucher_footer.Name = "tab_voucher_footer"
    Me.tab_voucher_footer.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_voucher_footer.Size = New System.Drawing.Size(372, 108)
    Me.tab_voucher_footer.TabIndex = 1
    Me.tab_voucher_footer.Text = "xFooter"
    Me.tab_voucher_footer.UseVisualStyleBackColor = True
    '
    'txt_voucher_footer
    '
    Me.txt_voucher_footer.Dock = System.Windows.Forms.DockStyle.Fill
    Me.txt_voucher_footer.Location = New System.Drawing.Point(3, 3)
    Me.txt_voucher_footer.MaxLength = 500
    Me.txt_voucher_footer.Multiline = True
    Me.txt_voucher_footer.Name = "txt_voucher_footer"
    Me.txt_voucher_footer.Size = New System.Drawing.Size(366, 102)
    Me.txt_voucher_footer.TabIndex = 0
    '
    'gb_schedule
    '
    Me.gb_schedule.Controls.Add(Me.lbl_schedule_help)
    Me.gb_schedule.Controls.Add(Me.dtp_time_from)
    Me.gb_schedule.Controls.Add(Me.dtp_time_to)
    Me.gb_schedule.Location = New System.Drawing.Point(320, 137)
    Me.gb_schedule.Name = "gb_schedule"
    Me.gb_schedule.Size = New System.Drawing.Size(398, 100)
    Me.gb_schedule.TabIndex = 9
    Me.gb_schedule.TabStop = False
    Me.gb_schedule.Text = "xSchedule"
    '
    'lbl_schedule_help
    '
    Me.lbl_schedule_help.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_schedule_help.ForeColor = System.Drawing.Color.Navy
    Me.lbl_schedule_help.Location = New System.Drawing.Point(15, 53)
    Me.lbl_schedule_help.Name = "lbl_schedule_help"
    Me.lbl_schedule_help.Size = New System.Drawing.Size(368, 41)
    Me.lbl_schedule_help.TabIndex = 10
    Me.lbl_schedule_help.Text = "xScheduleHelp"
    '
    'dtp_time_from
    '
    Me.dtp_time_from.Checked = True
    Me.dtp_time_from.IsReadOnly = False
    Me.dtp_time_from.Location = New System.Drawing.Point(45, 21)
    Me.dtp_time_from.Name = "dtp_time_from"
    Me.dtp_time_from.ShowCheckBox = False
    Me.dtp_time_from.ShowUpDown = False
    Me.dtp_time_from.Size = New System.Drawing.Size(130, 24)
    Me.dtp_time_from.SufixText = "Sufix Text"
    Me.dtp_time_from.TabIndex = 7
    Me.dtp_time_from.TextVisible = False
    Me.dtp_time_from.TextWidth = 58
    Me.dtp_time_from.Value = New Date(2010, 5, 12, 0, 0, 0, 0)
    '
    'dtp_time_to
    '
    Me.dtp_time_to.Checked = True
    Me.dtp_time_to.IsReadOnly = False
    Me.dtp_time_to.Location = New System.Drawing.Point(235, 21)
    Me.dtp_time_to.Name = "dtp_time_to"
    Me.dtp_time_to.ShowCheckBox = False
    Me.dtp_time_to.ShowUpDown = False
    Me.dtp_time_to.Size = New System.Drawing.Size(120, 24)
    Me.dtp_time_to.SufixText = "Sufix Text"
    Me.dtp_time_to.TabIndex = 8
    Me.dtp_time_to.TextVisible = False
    Me.dtp_time_to.TextWidth = 48
    Me.dtp_time_to.Value = New Date(2010, 5, 12, 0, 0, 0, 0)
    '
    'lb_functionality_description
    '
    Me.lb_functionality_description.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lb_functionality_description.ForeColor = System.Drawing.Color.Navy
    Me.lb_functionality_description.Location = New System.Drawing.Point(317, 10)
    Me.lb_functionality_description.Name = "lb_functionality_description"
    Me.lb_functionality_description.Size = New System.Drawing.Size(401, 124)
    Me.lb_functionality_description.TabIndex = 6
    Me.lb_functionality_description.Text = "xSAMPLE"
    '
    'dg_functionalities
    '
    Me.dg_functionalities.CurrentCol = -1
    Me.dg_functionalities.CurrentRow = -1
    Me.dg_functionalities.Location = New System.Drawing.Point(6, 6)
    Me.dg_functionalities.Name = "dg_functionalities"
    Me.dg_functionalities.PanelRightVisible = False
    Me.dg_functionalities.Redraw = True
    Me.dg_functionalities.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_functionalities.Size = New System.Drawing.Size(300, 397)
    Me.dg_functionalities.Sortable = False
    Me.dg_functionalities.SortAscending = True
    Me.dg_functionalities.SortByCol = 0
    Me.dg_functionalities.TabIndex = 1
    Me.dg_functionalities.ToolTipped = True
    Me.dg_functionalities.TopRow = -2
    '
    'tab_Image
    '
    Me.tab_Image.Controls.Add(Me.lb_img_description)
    Me.tab_Image.Controls.Add(Me.img_preview)
    Me.tab_Image.Controls.Add(Me.dg_images)
    Me.tab_Image.Location = New System.Drawing.Point(4, 22)
    Me.tab_Image.Name = "tab_Image"
    Me.tab_Image.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_Image.Size = New System.Drawing.Size(724, 414)
    Me.tab_Image.TabIndex = 3
    Me.tab_Image.Text = "xImage"
    Me.tab_Image.UseVisualStyleBackColor = True
    '
    'lb_img_description
    '
    Me.lb_img_description.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lb_img_description.ForeColor = System.Drawing.Color.Navy
    Me.lb_img_description.Location = New System.Drawing.Point(317, 10)
    Me.lb_img_description.Name = "lb_img_description"
    Me.lb_img_description.Size = New System.Drawing.Size(401, 43)
    Me.lb_img_description.TabIndex = 7
    Me.lb_img_description.Text = "xSAMPLE"
    '
    'img_preview
    '
    Me.img_preview.AutoSize = True
    Me.img_preview.ButtonDeleteEnabled = True
    Me.img_preview.FreeResize = False
    Me.img_preview.Image = Nothing
    Me.img_preview.ImageInfoFont = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.img_preview.ImageLayout = System.Windows.Forms.ImageLayout.None
    Me.img_preview.Location = New System.Drawing.Point(314, 52)
    Me.img_preview.Margin = New System.Windows.Forms.Padding(0)
    Me.img_preview.Name = "img_preview"
    Me.img_preview.Size = New System.Drawing.Size(404, 359)
    Me.img_preview.TabIndex = 3
    '
    'dg_images
    '
    Me.dg_images.CurrentCol = -1
    Me.dg_images.CurrentRow = -1
    Me.dg_images.Location = New System.Drawing.Point(6, 6)
    Me.dg_images.Name = "dg_images"
    Me.dg_images.PanelRightVisible = False
    Me.dg_images.Redraw = False
    Me.dg_images.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_images.Size = New System.Drawing.Size(300, 397)
    Me.dg_images.Sortable = False
    Me.dg_images.SortAscending = True
    Me.dg_images.SortByCol = 0
    Me.dg_images.TabIndex = 4
    Me.dg_images.ToolTipped = True
    Me.dg_images.TopRow = -2
    '
    'tab_messages
    '
    Me.tab_messages.Controls.Add(Me.lbl_message_help)
    Me.tab_messages.Controls.Add(Me.dg_messages)
    Me.tab_messages.Location = New System.Drawing.Point(4, 22)
    Me.tab_messages.Name = "tab_messages"
    Me.tab_messages.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_messages.Size = New System.Drawing.Size(724, 414)
    Me.tab_messages.TabIndex = 4
    Me.tab_messages.Text = "xMessages"
    Me.tab_messages.UseVisualStyleBackColor = True
    '
    'lbl_message_help
    '
    Me.lbl_message_help.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_message_help.ForeColor = System.Drawing.Color.Navy
    Me.lbl_message_help.Location = New System.Drawing.Point(506, 17)
    Me.lbl_message_help.Name = "lbl_message_help"
    Me.lbl_message_help.Size = New System.Drawing.Size(212, 391)
    Me.lbl_message_help.TabIndex = 7
    Me.lbl_message_help.Text = "xMessageHelp"
    '
    'dg_messages
    '
    Me.dg_messages.CurrentCol = -1
    Me.dg_messages.CurrentRow = -1
    Me.dg_messages.Location = New System.Drawing.Point(6, 17)
    Me.dg_messages.Name = "dg_messages"
    Me.dg_messages.PanelRightVisible = False
    Me.dg_messages.Redraw = True
    Me.dg_messages.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_messages.Size = New System.Drawing.Size(494, 380)
    Me.dg_messages.Sortable = False
    Me.dg_messages.SortAscending = True
    Me.dg_messages.SortByCol = 0
    Me.dg_messages.TabIndex = 1
    Me.dg_messages.ToolTipped = True
    Me.dg_messages.TopRow = -2
    '
    'frm_ads_configuration
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(844, 454)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_ads_configuration"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "ads_configuration"
    Me.panel_data.ResumeLayout(False)
    Me.tab_config.ResumeLayout(False)
    Me.tab_fields.ResumeLayout(False)
    Me.tab_fields.PerformLayout()
    Me.tab_gifts.ResumeLayout(False)
    Me.tab_gifts.PerformLayout()
    Me.gb_gifts_split.ResumeLayout(False)
    Me.gb_next_gifts.ResumeLayout(False)
    Me.gb_next_gifts.PerformLayout()
    Me.tab_functionalities.ResumeLayout(False)
    Me.gb_voucher.ResumeLayout(False)
    Me.tab_voucher.ResumeLayout(False)
    Me.tab_voucher_header.ResumeLayout(False)
    Me.tab_voucher_header.PerformLayout()
    Me.tab_voucher_footer.ResumeLayout(False)
    Me.tab_voucher_footer.PerformLayout()
    Me.gb_schedule.ResumeLayout(False)
    Me.tab_Image.ResumeLayout(False)
    Me.tab_Image.PerformLayout()
    Me.tab_messages.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents dg_fields As GUI_Controls.uc_grid
  Friend WithEvents tab_config As System.Windows.Forms.TabControl
  Friend WithEvents tab_fields As System.Windows.Forms.TabPage
  Friend WithEvents tab_gifts As System.Windows.Forms.TabPage
  Friend WithEvents img_preview As GUI_Controls.uc_image
  Friend WithEvents dg_images As GUI_Controls.uc_grid
  Friend WithEvents ef_unit As GUI_Controls.uc_entry_field
  Friend WithEvents ef_percent As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_min_2_show As System.Windows.Forms.Label
  Friend WithEvents ef_min_gifts As GUI_Controls.uc_entry_field
  Friend WithEvents tab_functionalities As System.Windows.Forms.TabPage
  Friend WithEvents tab_Image As System.Windows.Forms.TabPage
  Friend WithEvents dg_functionalities As GUI_Controls.uc_grid
  Friend WithEvents lbl_max_2_show As System.Windows.Forms.Label
  Friend WithEvents chk_Percent As System.Windows.Forms.CheckBox
  Friend WithEvents chk_Max As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_demo As System.Windows.Forms.Label
  Friend WithEvents lbl_percentage As System.Windows.Forms.Label
  Friend WithEvents lb_functionality_description As System.Windows.Forms.Label
  Friend WithEvents lb_img_description As System.Windows.Forms.Label
  Friend WithEvents lbl_read_only_text As System.Windows.Forms.Label
  Friend WithEvents lbl_read_only As System.Windows.Forms.Label
  Friend WithEvents tab_messages As System.Windows.Forms.TabPage
  Friend WithEvents dg_messages As GUI_Controls.uc_grid
  Friend WithEvents lbl_message_help As System.Windows.Forms.Label
  Friend WithEvents dtp_time_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_time_from As GUI_Controls.uc_date_picker
  Friend WithEvents gb_schedule As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_schedule_help As System.Windows.Forms.Label
  Friend WithEvents tab_voucher As System.Windows.Forms.TabControl
  Friend WithEvents tab_voucher_header As System.Windows.Forms.TabPage
  Friend WithEvents txt_voucher_header As System.Windows.Forms.TextBox
  Friend WithEvents tab_voucher_footer As System.Windows.Forms.TabPage
  Friend WithEvents txt_voucher_footer As System.Windows.Forms.TextBox
  Friend WithEvents gb_voucher As System.Windows.Forms.GroupBox
  Friend WithEvents gb_next_gifts As System.Windows.Forms.GroupBox
  Friend WithEvents chk_split_gifts As System.Windows.Forms.CheckBox
  Friend WithEvents ef_category_1_name As GUI_Controls.uc_entry_field
  Friend WithEvents ef_category_3_name As GUI_Controls.uc_entry_field
  Friend WithEvents ef_category_2_name As GUI_Controls.uc_entry_field
  Friend WithEvents gb_gifts_split As System.Windows.Forms.GroupBox
  Friend WithEvents ef_category_1_short As GUI_Controls.uc_entry_field
  Friend WithEvents ef_category_3_short As GUI_Controls.uc_entry_field
  Friend WithEvents ef_category_2_short As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_category_button As System.Windows.Forms.Label
  Friend WithEvents lbl_category_name As System.Windows.Forms.Label
  Friend WithEvents lbl_split_caption As System.Windows.Forms.Label
End Class
