﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_junkets_representatives_edit
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.gb_comments = New System.Windows.Forms.GroupBox()
    Me.txt_comments = New System.Windows.Forms.TextBox()
    Me.chk_status = New System.Windows.Forms.CheckBox()
    Me.ef_deposit_amount = New GUI_Controls.uc_entry_field()
    Me.ef_creation_date = New GUI_Controls.uc_entry_field()
    Me.ef_name = New GUI_Controls.uc_entry_field()
    Me.ef_code = New GUI_Controls.uc_entry_field()
    Me.chk_internal_employee = New System.Windows.Forms.CheckBox()
    Me.panel_data.SuspendLayout()
    Me.gb_comments.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.chk_internal_employee)
    Me.panel_data.Controls.Add(Me.gb_comments)
    Me.panel_data.Controls.Add(Me.chk_status)
    Me.panel_data.Controls.Add(Me.ef_deposit_amount)
    Me.panel_data.Controls.Add(Me.ef_creation_date)
    Me.panel_data.Controls.Add(Me.ef_name)
    Me.panel_data.Controls.Add(Me.ef_code)
    Me.panel_data.Location = New System.Drawing.Point(8, 7)
    Me.panel_data.Size = New System.Drawing.Size(546, 210)
    '
    'gb_comments
    '
    Me.gb_comments.Controls.Add(Me.txt_comments)
    Me.gb_comments.Location = New System.Drawing.Point(3, 75)
    Me.gb_comments.Name = "gb_comments"
    Me.gb_comments.Size = New System.Drawing.Size(389, 119)
    Me.gb_comments.TabIndex = 4
    Me.gb_comments.TabStop = False
    Me.gb_comments.Text = "xComments"
    '
    'txt_comments
    '
    Me.txt_comments.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.txt_comments.Location = New System.Drawing.Point(16, 25)
    Me.txt_comments.Multiline = True
    Me.txt_comments.Name = "txt_comments"
    Me.txt_comments.Size = New System.Drawing.Size(355, 88)
    Me.txt_comments.TabIndex = 0
    '
    'chk_status
    '
    Me.chk_status.AutoSize = True
    Me.chk_status.Location = New System.Drawing.Point(399, 100)
    Me.chk_status.Name = "chk_status"
    Me.chk_status.Size = New System.Drawing.Size(69, 17)
    Me.chk_status.TabIndex = 5
    Me.chk_status.Text = "xStatus"
    Me.chk_status.UseVisualStyleBackColor = True
    '
    'ef_deposit_amount
    '
    Me.ef_deposit_amount.DoubleValue = 0.0R
    Me.ef_deposit_amount.IntegerValue = 0
    Me.ef_deposit_amount.IsReadOnly = False
    Me.ef_deposit_amount.Location = New System.Drawing.Point(287, 13)
    Me.ef_deposit_amount.Name = "ef_deposit_amount"
    Me.ef_deposit_amount.PlaceHolder = Nothing
    Me.ef_deposit_amount.Size = New System.Drawing.Size(228, 24)
    Me.ef_deposit_amount.SufixText = "Sufix Text"
    Me.ef_deposit_amount.SufixTextVisible = True
    Me.ef_deposit_amount.TabIndex = 2
    Me.ef_deposit_amount.TabStop = False
    Me.ef_deposit_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_deposit_amount.TextValue = ""
    Me.ef_deposit_amount.TextWidth = 110
    Me.ef_deposit_amount.Value = ""
    Me.ef_deposit_amount.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_creation_date
    '
    Me.ef_creation_date.DoubleValue = 0.0R
    Me.ef_creation_date.IntegerValue = 0
    Me.ef_creation_date.IsReadOnly = False
    Me.ef_creation_date.Location = New System.Drawing.Point(287, 44)
    Me.ef_creation_date.Name = "ef_creation_date"
    Me.ef_creation_date.PlaceHolder = Nothing
    Me.ef_creation_date.Size = New System.Drawing.Size(228, 24)
    Me.ef_creation_date.SufixText = "Sufix Text"
    Me.ef_creation_date.SufixTextVisible = True
    Me.ef_creation_date.TabIndex = 3
    Me.ef_creation_date.TabStop = False
    Me.ef_creation_date.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_creation_date.TextValue = ""
    Me.ef_creation_date.TextWidth = 110
    Me.ef_creation_date.Value = ""
    Me.ef_creation_date.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_name
    '
    Me.ef_name.DoubleValue = 0.0R
    Me.ef_name.IntegerValue = 0
    Me.ef_name.IsReadOnly = False
    Me.ef_name.Location = New System.Drawing.Point(3, 44)
    Me.ef_name.Name = "ef_name"
    Me.ef_name.PlaceHolder = Nothing
    Me.ef_name.Size = New System.Drawing.Size(278, 24)
    Me.ef_name.SufixText = "Sufix Text"
    Me.ef_name.SufixTextVisible = True
    Me.ef_name.TabIndex = 1
    Me.ef_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_name.TextValue = ""
    Me.ef_name.TextWidth = 50
    Me.ef_name.Value = ""
    Me.ef_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_code
    '
    Me.ef_code.DoubleValue = 0.0R
    Me.ef_code.IntegerValue = 0
    Me.ef_code.IsReadOnly = False
    Me.ef_code.Location = New System.Drawing.Point(3, 13)
    Me.ef_code.Name = "ef_code"
    Me.ef_code.PlaceHolder = Nothing
    Me.ef_code.Size = New System.Drawing.Size(278, 24)
    Me.ef_code.SufixText = "Sufix Text"
    Me.ef_code.SufixTextVisible = True
    Me.ef_code.TabIndex = 0
    Me.ef_code.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_code.TextValue = ""
    Me.ef_code.TextWidth = 50
    Me.ef_code.Value = ""
    Me.ef_code.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_internal_employee
    '
    Me.chk_internal_employee.AutoSize = True
    Me.chk_internal_employee.Location = New System.Drawing.Point(398, 123)
    Me.chk_internal_employee.Name = "chk_internal_employee"
    Me.chk_internal_employee.Size = New System.Drawing.Size(134, 17)
    Me.chk_internal_employee.TabIndex = 6
    Me.chk_internal_employee.Text = "xInternalEmployee"
    Me.chk_internal_employee.UseVisualStyleBackColor = True
    '
    'frm_junkets_representatives_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(668, 221)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
    Me.Name = "frm_junkets_representatives_edit"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_junkets_representatives_edit"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.gb_comments.ResumeLayout(False)
    Me.gb_comments.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_comments As System.Windows.Forms.GroupBox
  Friend WithEvents txt_comments As System.Windows.Forms.TextBox
  Friend WithEvents chk_status As System.Windows.Forms.CheckBox
  Friend WithEvents ef_deposit_amount As GUI_Controls.uc_entry_field
  Friend WithEvents ef_creation_date As GUI_Controls.uc_entry_field
  Friend WithEvents ef_name As GUI_Controls.uc_entry_field
  Friend WithEvents ef_code As GUI_Controls.uc_entry_field
  Friend WithEvents chk_internal_employee As System.Windows.Forms.CheckBox
End Class
