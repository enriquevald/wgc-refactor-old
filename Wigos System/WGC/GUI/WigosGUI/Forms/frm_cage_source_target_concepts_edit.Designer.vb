<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_cage_source_target_concepts_edit
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.ef_Name = New GUI_Controls.uc_entry_field
    Me.ef_UnitPrice = New GUI_Controls.uc_entry_field
    Me.lbl_description = New System.Windows.Forms.Label
    Me.txt_Description = New System.Windows.Forms.TextBox
    Me.gb_Status = New System.Windows.Forms.GroupBox
    Me.rb_disabled = New System.Windows.Forms.RadioButton
    Me.rb_enabled = New System.Windows.Forms.RadioButton
    Me.chk_IsProvision = New System.Windows.Forms.CheckBox
    Me.chk_ShowInReport = New System.Windows.Forms.CheckBox
    Me.tf_Type = New GUI_Controls.uc_text_field
    Me.panel_data.SuspendLayout()
    Me.gb_Status.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.tf_Type)
    Me.panel_data.Controls.Add(Me.chk_ShowInReport)
    Me.panel_data.Controls.Add(Me.chk_IsProvision)
    Me.panel_data.Controls.Add(Me.gb_Status)
    Me.panel_data.Controls.Add(Me.txt_Description)
    Me.panel_data.Controls.Add(Me.lbl_description)
    Me.panel_data.Controls.Add(Me.ef_UnitPrice)
    Me.panel_data.Controls.Add(Me.ef_Name)
    Me.panel_data.Size = New System.Drawing.Size(322, 268)
    '
    'ef_Name
    '
    Me.ef_Name.DoubleValue = 0
    Me.ef_Name.IntegerValue = 0
    Me.ef_Name.IsReadOnly = False
    Me.ef_Name.Location = New System.Drawing.Point(16, 7)
    Me.ef_Name.Name = "ef_Name"
    Me.ef_Name.Size = New System.Drawing.Size(300, 24)
    Me.ef_Name.SufixText = "Sufix Text"
    Me.ef_Name.SufixTextVisible = True
    Me.ef_Name.TabIndex = 0
    Me.ef_Name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_Name.TextValue = ""
    Me.ef_Name.Value = ""
    Me.ef_Name.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_UnitPrice
    '
    Me.ef_UnitPrice.DoubleValue = 0
    Me.ef_UnitPrice.IntegerValue = 0
    Me.ef_UnitPrice.IsReadOnly = False
    Me.ef_UnitPrice.Location = New System.Drawing.Point(-4, 119)
    Me.ef_UnitPrice.Name = "ef_UnitPrice"
    Me.ef_UnitPrice.Size = New System.Drawing.Size(209, 24)
    Me.ef_UnitPrice.SufixText = "Sufix Text"
    Me.ef_UnitPrice.SufixTextVisible = True
    Me.ef_UnitPrice.TabIndex = 3
    Me.ef_UnitPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_UnitPrice.TextValue = ""
    Me.ef_UnitPrice.TextWidth = 100
    Me.ef_UnitPrice.Value = ""
    Me.ef_UnitPrice.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_description
    '
    Me.lbl_description.AutoSize = True
    Me.lbl_description.Location = New System.Drawing.Point(22, 40)
    Me.lbl_description.Name = "lbl_description"
    Me.lbl_description.Size = New System.Drawing.Size(80, 13)
    Me.lbl_description.TabIndex = 1
    Me.lbl_description.Text = "xDescripcion"
    Me.lbl_description.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'txt_Description
    '
    Me.txt_Description.Location = New System.Drawing.Point(98, 37)
    Me.txt_Description.Multiline = True
    Me.txt_Description.Name = "txt_Description"
    Me.txt_Description.Size = New System.Drawing.Size(216, 76)
    Me.txt_Description.TabIndex = 2
    '
    'gb_Status
    '
    Me.gb_Status.Controls.Add(Me.rb_disabled)
    Me.gb_Status.Controls.Add(Me.rb_enabled)
    Me.gb_Status.Location = New System.Drawing.Point(16, 187)
    Me.gb_Status.Name = "gb_Status"
    Me.gb_Status.Size = New System.Drawing.Size(139, 70)
    Me.gb_Status.TabIndex = 5
    Me.gb_Status.TabStop = False
    Me.gb_Status.Text = "xEstado"
    '
    'rb_disabled
    '
    Me.rb_disabled.AutoSize = True
    Me.rb_disabled.Location = New System.Drawing.Point(6, 43)
    Me.rb_disabled.Name = "rb_disabled"
    Me.rb_disabled.Size = New System.Drawing.Size(109, 17)
    Me.rb_disabled.TabIndex = 1
    Me.rb_disabled.TabStop = True
    Me.rb_disabled.Text = "xDeshabilitada"
    Me.rb_disabled.UseVisualStyleBackColor = True
    '
    'rb_enabled
    '
    Me.rb_enabled.AutoSize = True
    Me.rb_enabled.Location = New System.Drawing.Point(6, 20)
    Me.rb_enabled.Name = "rb_enabled"
    Me.rb_enabled.Size = New System.Drawing.Size(88, 17)
    Me.rb_enabled.TabIndex = 0
    Me.rb_enabled.TabStop = True
    Me.rb_enabled.Text = "xHabilitada"
    Me.rb_enabled.UseVisualStyleBackColor = True
    '
    'chk_IsProvision
    '
    Me.chk_IsProvision.AutoSize = True
    Me.chk_IsProvision.Location = New System.Drawing.Point(173, 207)
    Me.chk_IsProvision.Name = "chk_IsProvision"
    Me.chk_IsProvision.Size = New System.Drawing.Size(126, 17)
    Me.chk_IsProvision.TabIndex = 6
    Me.chk_IsProvision.Text = "xEs Provisionable"
    Me.chk_IsProvision.UseVisualStyleBackColor = True
    '
    'chk_ShowInReport
    '
    Me.chk_ShowInReport.AutoSize = True
    Me.chk_ShowInReport.Location = New System.Drawing.Point(173, 230)
    Me.chk_ShowInReport.Name = "chk_ShowInReport"
    Me.chk_ShowInReport.Size = New System.Drawing.Size(143, 17)
    Me.chk_ShowInReport.TabIndex = 7
    Me.chk_ShowInReport.Text = "xMostrar en Reporte"
    Me.chk_ShowInReport.UseVisualStyleBackColor = True
    '
    'tf_Type
    '
    Me.tf_Type.IsReadOnly = True
    Me.tf_Type.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_Type.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_Type.Location = New System.Drawing.Point(16, 150)
    Me.tf_Type.Name = "tf_Type"
    Me.tf_Type.Size = New System.Drawing.Size(298, 24)
    Me.tf_Type.SufixText = "Sufix Text"
    Me.tf_Type.SufixTextVisible = True
    Me.tf_Type.TabIndex = 4
    '
    'frm_cage_source_target_concepts_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(424, 279)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_cage_source_target_concepts_edit"
    Me.Text = "frm_cage_source_target_concepts_edit"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.gb_Status.ResumeLayout(False)
    Me.gb_Status.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_Name As GUI_Controls.uc_entry_field
  Friend WithEvents ef_UnitPrice As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_description As System.Windows.Forms.Label
  Friend WithEvents txt_Description As System.Windows.Forms.TextBox
  Friend WithEvents gb_Status As System.Windows.Forms.GroupBox
  Friend WithEvents rb_disabled As System.Windows.Forms.RadioButton
  Friend WithEvents rb_enabled As System.Windows.Forms.RadioButton
  Friend WithEvents chk_ShowInReport As System.Windows.Forms.CheckBox
  Friend WithEvents chk_IsProvision As System.Windows.Forms.CheckBox
  Friend WithEvents tf_Type As GUI_Controls.uc_text_field
End Class
