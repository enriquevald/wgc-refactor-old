'-------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_progressive_jackpots_paid_report.vb
'
' DESCRIPTION : Progressive Jackpots Paid Report
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 19-AUG-2014  FJC    Initial version.
' 01-OCT-2014  FJC    Add New JackPot Type: HANDPAY_TYPE.MANUAL_JACKPOT
' 06-OCT-2014  FJC    Fixed Bug 1413: Informe de Jackpots progresivos pagados: error al buscar marcando "Agrupado por progresivo"
' 06-OCT-2014  FJC    Fixed Bug 1409: Totalizador: est� est� en otro color en los informes de progresivos
' 29-OCT-2014  SGB    Fixed Bug WIG-1595: Add excel filter Dates, terminals & filter terminals.
' 10-FEB-2015  FJC    WIG-2003. 
' 26-MAR-2015  ANM    Calling GetProviderIdListSelected always returns a query
' 30-AUG-2017  DPC    [WIGOS-4294]: When paying a Progressive Jackpot in Cashier and no holds are applied, that is displayed with amount zero in Provisioned jackpots paid report.
'--------------------------------------------------------------------
Option Strict Off
Option Explicit On

Imports System.Data.OleDb
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports GUI_CommonOperations.CLASS_BASE
Imports WSI.Common

Public Class frm_progressive_jackpots_paid_report
  Inherits frm_base_sel

#Region " Constants "

  ' Grid configuration
  Private Const GRID_HEADER_ROWS As Integer = 2
  Private TERMINAL_DATA_COLUMNS As Int32
  Private GRID_COLUMNS_PROGRESSIVE As Integer
  Private Const GRID_COLUMNS_GROUP_PROGRESSIVE As Integer = 3

  ' Hidden
  Private Const GRID_COLUMN_ID As Integer = 0

  ' Showed
  Private Const GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_PAID_DATE As Integer = 1
  Private Const GRID_COLUMN_DETAIL_PROGRESSIVE_NAME As Integer = 2

  ' Terminal Detail
  Private GRID_INIT_TERMINAL_DATA As Integer
  Private GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_AMOUNT As Integer
  Private GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_DATE As Integer

  Private GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_LEVEL As Integer
  Private GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_LEVEL_NAME As Integer

  'Rest
  Private Const GRID_COLUMN_GROUP_PROGRESSIVE_NAME As Integer = 1
  Private Const GRID_COLUMN_GROUP_PROGRESSIVE_JACKPOT_AMOUNT As Integer = 2

  'SQL Query Columns
  Private Const SQL_COL_PROGRESSIVE_ID As Integer = 0
  Private Const SQL_COL_PROGRESSIVE_HP_TYPE As Integer = 1
  Private Const SQL_COL_PROGRESSIVE_NAME As Integer = 2
  Private Const SQL_COL_PROGRESSIVE_JACKPOT_AMOUNT As Integer = 3
  Private Const SQL_COL_PROGRESSIVE_TERMINAL_ID As Integer = 4
  Private Const SQL_COL_PROGRESSIVE_PROVIDER As Integer = 5
  Private Const SQL_COL_PROGRESSIVE_TERMINAL As Integer = 6
  Private Const SQL_COL_PROGRESSIVE_JACKPOT_DATE As Integer = 7
  Private Const SQL_COL_PROGRESSIVE_JACKPOT_PAID_DATE As Integer = 8
  Private Const SQL_COL_PROGRESSIVE_JACKPOT_LEVEL As Integer = 9
  Private Const SQL_COL_PROGRESSIVE_JACKPOT_LEVEL_NAME As Integer = 10

  ' Grid Columns Width
  Private Const GRID_WIDTH_PROGRESSIVE_NAME As Integer = 3300
  Private Const GRID_WIDTH_PROGRESSIVE_JACKPOT_DATE As Integer = 2500
  Private Const GRID_WIDTH_PROGRESSIVE_PROVIDER As Integer = 2700
  Private Const GRID_WIDTH_PROGRESSIVE_TERMINAL As Integer = 3100
  Private Const GRID_WIDTH_PROGRESSIVE_JACKPOT_AMOUNT As Integer = 2300
  Private Const GRID_WIDTH_PROGRESSIVE_JACKPOT_LEVEL As Integer = 800
  Private Const GRID_WIDTH_PROGRESSIVE_JACKPOT_LEVEL_NAME As Integer = 2000

#End Region ' Constants

#Region " Enums "

  Private Enum ENUM_SUBTOTAL_TYPE
    PROGRESIVE = 0
    TOTAL = 1
  End Enum ' ENUM_SUBTOTAL_TYPE

#End Region ' Enums

#Region " Members "

  'SUBTOTALS
  Private m_total_progressive As Decimal
  Private m_total As Decimal

  'ID's
  Private progressive_id As Long = -1
  Private terminal_id As Long = -1

  'OTHERS
  Private progressive_name As String
  Private m_terminal_report_type As ReportType = ReportType.Provider

  ' Report filters
  Public m_progressive_terminals As String
  Public m_progressive_date_from As String
  Public m_progressive_date_to As String
  Public m_progressive_group As String
  Public m_progressive_catalog As String


#End Region ' Members

#Region " Overrides "

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_PROGRESSIVE_PAID_REPORT

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    Dim _dt_progressives As DataTable = Nothing

    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    ' Initialize Form Controls

    ' - Form
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5207) ' 5207 "Informe de jackpots progresivos pagados"

    ' - Buttons
    GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(27)
    GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)
    GUI_Button(ENUM_BUTTON.BUTTON_FILTER_APPLY).Text = GLB_NLS_GUI_CONTROLS.GetString(8)

    GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Enabled = False
    GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Enabled = False
    GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False

    ' Date 
    Me.gb_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5514)   'Fecha de Pago
    Me.dtp_from.ShowCheckBox = False
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)         ' 202 "Desde"
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)           ' 203 "Hasta"
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Progressives Check List
    Me.uc_checked_list_progressive_catalog.GroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5217) '"Progresivo"
    Me.uc_checked_list_progressive_catalog.ColumnWidth(1, 150)
    If Progressives.GetProgressives(_dt_progressives) Then
      Me.uc_checked_list_progressive_catalog.Add(_dt_progressives)
    End If
    Me.uc_checked_list_progressive_catalog.ResizeGrid()

    'Filter
    gp_progressive.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5682)
    chk_group_progressive.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(781) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5217) 'Group by Progressive
    Me.chk_view_detail.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1361)
    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)

    ' Providers - Terminals
    Call Me.uc_pr_list.Init(WSI.Common.Misc.GamingTerminalTypeList())

    ' Grid
    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls  

  ' PURPOSE: Manage buttons pressed.
  '
  '  PARAMS:
  '     - INPUT:
  '         - ButtonId: Id. of the button clicked.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    MyBase.GUI_ButtonClick(ButtonId)

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  '
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _str_sql As System.Text.StringBuilder

    _str_sql = New System.Text.StringBuilder()

    _str_sql.AppendLine("    SELECT                                                   ")

    If Me.chk_group_progressive.Checked AndAlso Not Me.chk_view_detail.Checked Then
      _str_sql.AppendLine("             HP_GROUP.HP_PROGRESSIVE_ID                    ")
      _str_sql.AppendLine("             , NULL                                        ")
      _str_sql.AppendLine("             , PGS_NAME                                    ")
      _str_sql.AppendLine("             , HP_GROUP.AMOUNTSUM                          ")
    Else
      _str_sql.AppendLine("               H.HP_PROGRESSIVE_ID                         ")
      _str_sql.AppendLine("             , H.HP_TYPE                                   ")
      _str_sql.AppendLine("             , PGS.PGS_NAME                                ")
      If WSI.Common.TITO.Utils.IsTitoMode Then
        _str_sql.AppendLine("             , CASE WHEN (H.HP_TAX_BASE_AMOUNT IS NULL OR H.HP_TAX_BASE_AMOUNT = 0) THEN H.HP_AMOUNT ELSE H.HP_TAX_BASE_AMOUNT END AS HP_AMOUNT ")
      Else
        _str_sql.AppendLine("             , H.HP_AMOUNT                                 ")
      End If
      _str_sql.AppendLine("             , T.TE_TERMINAL_ID                            ")
      _str_sql.AppendLine("             , P.PV_NAME                                   ")
      _str_sql.AppendLine("             , T.TE_NAME                                   ")
      _str_sql.AppendLine("             , H.HP_DATETIME                               ")
      _str_sql.AppendLine("             , AM.AM_DATETIME                              ")
      _str_sql.AppendLine("             , H.HP_LEVEL                                  ")
      _str_sql.AppendLine("             , PL.PGL_NAME                                 ")

    End If

    If Me.chk_group_progressive.Checked AndAlso Not Me.chk_view_detail.Checked Then
      _str_sql.AppendLine("       FROM (                                                          ")
      _str_sql.AppendLine("             SELECT    HP_PROGRESSIVE_ID                               ")
      If WSI.Common.TITO.Utils.IsTitoMode Then
        _str_sql.AppendLine("                     , SUM(CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN HP_AMOUNT ELSE HP_TAX_BASE_AMOUNT END) AS AMOUNTSUM ")
      Else
        _str_sql.AppendLine("                     , SUM(HP_AMOUNT) AS AMOUNTSUM                   ")
      End If
      _str_sql.AppendLine("               FROM    HANDPAYS                                        ")
      _str_sql.AppendLine("		       LEFT JOIN	  ACCOUNT_MOVEMENTS                               ")
      _str_sql.AppendLine("                 ON    AM_MOVEMENT_ID = HP_MOVEMENT_ID                 ")
      _str_sql.AppendLine(GetSqlWhereFilter())
      _str_sql.AppendLine("           GROUP BY    HP_PROGRESSIVE_ID                               ")
      _str_sql.AppendLine("            ) AS HP_GROUP                                              ")
      _str_sql.AppendLine("         INNER JOIN	  PROGRESSIVES	                                  ")
      _str_sql.AppendLine("                 ON    PGS_PROGRESSIVE_ID = HP_GROUP.HP_PROGRESSIVE_ID ")
    Else
      _str_sql.AppendLine("       FROM    HANDPAYS H                                  ")
      _str_sql.AppendLine("  LEFT JOIN	  ACCOUNT_MOVEMENTS AM")
      _str_sql.AppendLine("         ON    AM.AM_MOVEMENT_id = HP_MOVEMENT_ID ")
      _str_sql.AppendLine(" INNER JOIN    PROGRESSIVES PGS ")
      _str_sql.AppendLine("         ON    H.HP_PROGRESSIVE_ID = PGS.PGS_PROGRESSIVE_ID")
      _str_sql.AppendLine(" INNER JOIN    TERMINALS T ")
      _str_sql.AppendLine("         ON    T.TE_TERMINAL_ID = H.HP_TERMINAL_ID ")
      _str_sql.AppendLine(" INNER JOIN	  PROVIDERS P ")
      _str_sql.AppendLine("         ON    T.TE_PROV_ID = P.PV_ID ")
      _str_sql.AppendLine("  INNER JOIN	   PROGRESSIVES_LEVELS PL ")
      _str_sql.AppendLine("         ON    H.HP_LEVEL = PL.PGL_LEVEL_ID AND H.HP_PROGRESSIVE_ID = PL.PGL_PROGRESSIVE_ID ")
      _str_sql.AppendLine(GetSqlWhereFilter())

    End If


    If Not Me.chk_group_progressive.Checked Then
      _str_sql.AppendLine("ORDER BY	     H.HP_DATETIME DESC")
    Else
      If Me.chk_view_detail.Checked Then
        _str_sql.AppendLine("ORDER BY	     HP_PROGRESSIVE_ID, H.HP_DATETIME DESC ")
      Else
        _str_sql.AppendLine("ORDER BY	     HP_GROUP.HP_PROGRESSIVE_ID ")
      End If
    End If

    Return _str_sql.ToString
  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _terminal_data As List(Of Object)

    ' Progressive Id
    If Not DbRow.IsNull(SQL_COL_PROGRESSIVE_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ID).Value = DbRow.Value(SQL_COL_PROGRESSIVE_ID)
    End If

    If Me.chk_group_progressive.Checked AndAlso Not Me.chk_view_detail.Checked Then
      ' Progressive Name
      If Not DbRow.IsNull(SQL_COL_PROGRESSIVE_NAME) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_GROUP_PROGRESSIVE_NAME).Value = DbRow.Value(SQL_COL_PROGRESSIVE_NAME)
      End If

      ' Amount
      If Not DbRow.IsNull(SQL_COL_PROGRESSIVE_JACKPOT_AMOUNT) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_GROUP_PROGRESSIVE_JACKPOT_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COL_PROGRESSIVE_JACKPOT_AMOUNT), _
          ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_GROUP_PROGRESSIVE_JACKPOT_AMOUNT).Value = ""
      End If

    Else

      ' JackPot Paid Date
      If Not DbRow.IsNull(SQL_COL_PROGRESSIVE_HP_TYPE) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_PAID_DATE).Value = ""

        Select Case DbRow.Value(SQL_COL_PROGRESSIVE_HP_TYPE)
          Case WSI.Common.HANDPAY_TYPE.JACKPOT, _
               WSI.Common.HANDPAY_TYPE.MANUAL_JACKPOT

            If Not DbRow.IsNull(SQL_COL_PROGRESSIVE_JACKPOT_PAID_DATE) Then
              Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_PAID_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COL_PROGRESSIVE_JACKPOT_PAID_DATE), , ENUM_FORMAT_TIME.FORMAT_HHMMSS)
            End If

          Case WSI.Common.HANDPAY_TYPE.SPECIAL_PROGRESSIVE
            If Not DbRow.IsNull(SQL_COL_PROGRESSIVE_JACKPOT_DATE) Then
              Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_PAID_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COL_PROGRESSIVE_JACKPOT_DATE), , ENUM_FORMAT_TIME.FORMAT_HHMMSS)
            End If

        End Select
      End If

      ' Progressive Name
      If Not DbRow.IsNull(SQL_COL_PROGRESSIVE_NAME) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_PROGRESSIVE_NAME).Value = DbRow.Value(SQL_COL_PROGRESSIVE_NAME)
      End If

      ' Terminal Report
      If Me.chk_view_detail.Checked OrElse _
         Not Me.chk_group_progressive.Checked Then

        If Not DbRow.IsNull(SQL_COL_PROGRESSIVE_TERMINAL_ID) Then
          _terminal_data = TerminalReport.GetReportDataList(DbRow.Value(SQL_COL_PROGRESSIVE_TERMINAL_ID), m_terminal_report_type)

          For _idx As Int32 = 0 To _terminal_data.Count - 1
            If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value Then
              Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
            End If
          Next
        End If
      End If

      ' Amount
      If Not DbRow.IsNull(SQL_COL_PROGRESSIVE_JACKPOT_AMOUNT) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COL_PROGRESSIVE_JACKPOT_AMOUNT), _
          ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_AMOUNT).Value = ""
      End If

      ' JackPot Date (Fecha de Premio)
      If Not DbRow.IsNull(SQL_COL_PROGRESSIVE_JACKPOT_DATE) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COL_PROGRESSIVE_JACKPOT_DATE), , ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_DATE).Value = ""
      End If

      ' JackPot level num 
      If Not DbRow.IsNull(SQL_COL_PROGRESSIVE_JACKPOT_LEVEL) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_LEVEL).Value = GUI_FormatNumber(DbRow.Value(SQL_COL_PROGRESSIVE_JACKPOT_LEVEL), 0)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_LEVEL).Value = ""
      End If

      ' JackPot level name
      If Not DbRow.IsNull(SQL_COL_PROGRESSIVE_JACKPOT_LEVEL_NAME) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_LEVEL_NAME).Value = DbRow.Value(SQL_COL_PROGRESSIVE_JACKPOT_LEVEL_NAME)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_LEVEL_NAME).Value = ""
      End If
    End If

    Return True
  End Function ' GUI_SetupRow


  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5514) & " " & GLB_NLS_GUI_AUDITOR.GetString(257), m_progressive_date_from) ' Period date from
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5514) & " " & GLB_NLS_GUI_AUDITOR.GetString(258), m_progressive_date_to)   ' Period date to
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5682), m_progressive_group)                                                ' Fitler group
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5217), m_progressive_catalog)                                              ' Catalog
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5220), m_progressive_terminals)                                            ' Detail

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportUpdateFilters()

    Dim _all_checked As Boolean
    m_progressive_date_from = ""
    m_progressive_date_to = ""
    m_progressive_group = ""
    m_progressive_catalog = ""
    m_progressive_terminals = ""

    ' Period date
    If dtp_to.Checked Then
      m_progressive_date_to = GUI_FormatDate(dtp_to.Value, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    End If
    m_progressive_date_from = GUI_FormatDate(dtp_from.Value, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)

    ' Group filter
    m_progressive_group = IIf(chk_group_progressive.Checked, _
                          GLB_NLS_GUI_PLAYER_TRACKING.GetString(781) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5217), AUDIT_NONE_STRING) 'Progressive group / None


    ' Progressive catalog
    _all_checked = True

    If uc_checked_list_progressive_catalog.SelectedIndexes.Length <> uc_checked_list_progressive_catalog.Count() Then
      _all_checked = False
    End If

    If _all_checked = True Then
      m_progressive_catalog = GLB_NLS_GUI_AUDITOR.GetString(263)
    Else
      m_progressive_catalog = uc_checked_list_progressive_catalog.SelectedValuesList()
    End If

    ' Progressive terminals
    m_progressive_terminals = Me.uc_pr_list.GetTerminalReportText()

  End Sub ' GUI_ReportUpdateFilters

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()
    progressive_id = -1

    Call ResetCountersProgressive()
    Call ResetTotalCounters()

    Call GUI_StyleSheet()
  End Sub ' GUI_BeforeFirstRow

  ' PURPOSE : Checks out the values of a db row before adding it to the grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the db row should be added to the grid
  '     - False: the db row should NOT be added to the grid
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As CLASS_DB_ROW) As Boolean

    If Me.chk_group_progressive.Checked AndAlso _
       Me.chk_view_detail.Checked Then

      If progressive_id <> DbRow.Value(SQL_COL_PROGRESSIVE_ID) AndAlso _
        progressive_id <> -1 Then

        Call DrawSubTotal(ENUM_SUBTOTAL_TYPE.PROGRESIVE)
        m_total = m_total + m_total_progressive
        Call ResetCountersProgressive()
      End If
    End If

    m_total_progressive = m_total_progressive + DbRow.Value(SQL_COL_PROGRESSIVE_JACKPOT_AMOUNT)
    progressive_name = DbRow.Value(SQL_COL_PROGRESSIVE_NAME)

    If Not DbRow.IsNull(SQL_COL_PROGRESSIVE_ID) Then
      progressive_id = DbRow.Value(SQL_COL_PROGRESSIVE_ID)
    End If

    Return True
  End Function ' GUI_CheckOutRowBeforeAdd

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    If Me.Grid.NumRows > 0 AndAlso _
         Me.chk_group_progressive.Checked AndAlso _
         Me.chk_view_detail.Checked Then
      Call DrawSubTotal(ENUM_SUBTOTAL_TYPE.PROGRESIVE)
    End If

    m_total = m_total + m_total_progressive
    Call DrawSubTotal(ENUM_SUBTOTAL_TYPE.TOTAL)
  End Sub ' GUI_AfterLastRow

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    ' Dates
    Me.dtp_from.Value = WSI.Common.Misc.TodayOpening().AddDays(-1)
    Me.dtp_to.Value = New DateTime(Now.Year, Now.Month, Now.Day, 0, 0, 0)
    Me.dtp_from.Checked = True
    Me.dtp_to.Checked = False

    'Progressives
    Me.uc_checked_list_progressive_catalog.btn_check_all.PerformClick()

    'Terminals
    Me.uc_pr_list.SetDefaultValues()

    'Detail / Group
    Me.chk_group_progressive.Checked = False
    Me.chk_view_detail.Checked = False
    Me.chk_view_detail.Enabled = False

    Me.chk_terminal_location.Checked = False
    Me.chk_terminal_location.Enabled = True
  End Sub ' SetDefaultValues

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    Dim _terminal_columns As List(Of ColumnSettings)

    Call GridColumnReIndex()

    With Me.Grid

      .Clear()

      If Not Me.chk_group_progressive.Checked OrElse _
         (Me.chk_group_progressive.Checked AndAlso Me.chk_view_detail.Checked) Then
        Call .Init(GRID_COLUMNS_PROGRESSIVE, GRID_HEADER_ROWS)
      Else
        Call .Init(GRID_COLUMNS_GROUP_PROGRESSIVE, GRID_HEADER_ROWS)
      End If

      ' ID
      .Column(GRID_COLUMN_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_ID).Header(1).Text = " ID "
      .Column(GRID_COLUMN_ID).Width = 0

      If Not Me.chk_group_progressive.Checked OrElse _
         (Me.chk_group_progressive.Checked AndAlso Me.chk_view_detail.Checked) Then

        ' JackPot Paid Date
        .Column(GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_PAID_DATE).Header(0).Text = ""
        .Column(GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_PAID_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5514)  'Fecha de Pago
        .Column(GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_PAID_DATE).Width = GRID_WIDTH_PROGRESSIVE_JACKPOT_DATE
        .Column(GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_PAID_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

        ' Progressive Name
        .Column(GRID_COLUMN_DETAIL_PROGRESSIVE_NAME).Header(0).Text = ""
        .Column(GRID_COLUMN_DETAIL_PROGRESSIVE_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5217) 'Progressive
        .Column(GRID_COLUMN_DETAIL_PROGRESSIVE_NAME).Width = GRID_WIDTH_PROGRESSIVE_NAME
        .Column(GRID_COLUMN_DETAIL_PROGRESSIVE_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

        '  Terminal Report
        _terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)
        For _idx As Int32 = 0 To _terminal_columns.Count - 1
          .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(232)
          .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = _terminal_columns(_idx).Header1
          .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = _terminal_columns(_idx).Width
          .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = _terminal_columns(_idx).Alignment
        Next

        ' Paid Amount
        .Column(GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_AMOUNT).Header(0).Text = ""
        .Column(GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5214) 'Amount
        .Column(GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_AMOUNT).Width = GRID_WIDTH_PROGRESSIVE_JACKPOT_AMOUNT
        .Column(GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ' JackPot Date
        .Column(GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_DATE).Header(0).Text = ""
        .Column(GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_DATE).Header(1).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(203) 'Date awarded (fecha del premio)
        .Column(GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_DATE).Width = GRID_WIDTH_PROGRESSIVE_JACKPOT_DATE
        .Column(GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

        ' JackPot level
        .Column(GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_LEVEL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(443) 'level 
        .Column(GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_LEVEL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(443) 'level 
        .Column(GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_LEVEL).Width = GRID_WIDTH_PROGRESSIVE_JACKPOT_LEVEL
        .Column(GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_LEVEL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

        ' JackPot level name
        .Column(GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_LEVEL_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(443) 'level 
        .Column(GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_LEVEL_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(253) 'level name 
        .Column(GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_LEVEL_NAME).Width = GRID_WIDTH_PROGRESSIVE_JACKPOT_LEVEL_NAME
        .Column(GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_LEVEL_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      Else

        ' Progressive Name
        .Column(GRID_COLUMN_GROUP_PROGRESSIVE_NAME).Header(0).Text = ""
        .Column(GRID_COLUMN_GROUP_PROGRESSIVE_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5217) 'Progressive
        .Column(GRID_COLUMN_GROUP_PROGRESSIVE_NAME).Width = GRID_WIDTH_PROGRESSIVE_NAME
        .Column(GRID_COLUMN_GROUP_PROGRESSIVE_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

        ' Paid Amount
        .Column(GRID_COLUMN_GROUP_PROGRESSIVE_JACKPOT_AMOUNT).Header(0).Text = ""
        .Column(GRID_COLUMN_GROUP_PROGRESSIVE_JACKPOT_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5214) 'Amount
        .Column(GRID_COLUMN_GROUP_PROGRESSIVE_JACKPOT_AMOUNT).Width = GRID_WIDTH_PROGRESSIVE_JACKPOT_AMOUNT
        .Column(GRID_COLUMN_GROUP_PROGRESSIVE_JACKPOT_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      End If

      .Sortable = False
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the
  '          main SQL Query.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhereFilter() As String

    Dim _str_where As System.Text.StringBuilder

    _str_where = New System.Text.StringBuilder()

    ' Common conditions

    ' JacPot HandPay
    _str_where.AppendLine(" WHERE                                                                                                 ")
    _str_where.AppendLine(" (                                                                                                     ")
    _str_where.AppendLine("   (                                                                                                   ")
    _str_where.AppendLine("     (HP_LEVEL BETWEEN CONVERT(VARBINARY(8),1) AND CONVERT(VARBINARY(8),20))                           ")
    _str_where.AppendLine("     AND HP_TYPE IN (                                                                                  ")
    _str_where.AppendLine(WSI.Common.HANDPAY_TYPE.JACKPOT)
    _str_where.AppendLine("                    ,                                                                                  ")
    _str_where.AppendLine(WSI.Common.HANDPAY_TYPE.MANUAL_JACKPOT)
    _str_where.AppendLine("                    )                                                                                  ")
    _str_where.AppendLine("     AND HP_MOVEMENT_ID IS NOT NULL                                                                    ")
    _str_where.AppendLine("     AND HP_PLAY_SESSION_ID IS NOT NULL                                                                ")
    _str_where.AppendLine("     AND HP_PROGRESSIVE_ID IS NOT NULL                                                                 ")

    ' Filter Dates
    If Me.gb_date.Enabled Then
      _str_where.AppendLine("   AND AM_DATETIME >= " & GUI_FormatDateDB(dtp_from.Value))
      If Me.dtp_to.Checked = True Then
        _str_where.AppendLine(" AND AM_DATETIME <= " & GUI_FormatDateDB(dtp_to.Value))
      End If
    End If
    _str_where.AppendLine("   )")

    _str_where.AppendLine(" OR ")

    '   Manual Progressive HandPay
    _str_where.AppendLine("   (                                                                                                   ")
    _str_where.AppendLine("     HP_TYPE = " & WSI.Common.HANDPAY_TYPE.SPECIAL_PROGRESSIVE)
    _str_where.AppendLine("     AND HP_PROGRESSIVE_ID IS NOT NULL                                                                 ")
    ' Filter Dates
    If Me.gb_date.Enabled Then
      _str_where.AppendLine("   AND HP_DATETIME >= " & GUI_FormatDateDB(dtp_from.Value))
      If Me.dtp_to.Checked = True Then
        _str_where.AppendLine(" AND HP_DATETIME <= " & GUI_FormatDateDB(dtp_to.Value))
      End If
    End If
    _str_where.AppendLine("   )")
    _str_where.AppendLine(" )")


    ' Filter Progressives
    If Me.uc_checked_list_progressive_catalog.SelectedIndexesList <> "" Then
      _str_where.AppendLine(" AND HP_PROGRESSIVE_ID IN(" & Me.uc_checked_list_progressive_catalog.SelectedIndexesList & ")        ")
    End If

    'Filter Terminal
    _str_where.AppendLine(" AND HP_TERMINAL_ID IN " & Me.uc_pr_list.GetProviderIdListSelected())

    Return _str_where.ToString()

  End Function ' GetSqlWhereFilter

  ' PURPOSE: Write Subtotals Amounts in the Grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub DrawSubTotal(ByVal type_subtotal As ENUM_SUBTOTAL_TYPE)
    Dim _idx_row As Integer
    Dim _str_total As String = String.Empty
    Dim _provision_id As Long = -1
    Dim _amount As Decimal
    Dim _color As ENUM_GUI_COLOR
    Dim _col_Text As Integer
    Dim _col_Total As Integer

    Select Case type_subtotal
      Case ENUM_SUBTOTAL_TYPE.PROGRESIVE
        _str_total = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1046) & " " & progressive_name & ":" '"SubTotal <progressive name>: " 
        _amount = m_total_progressive
        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01
      Case ENUM_SUBTOTAL_TYPE.TOTAL
        _str_total = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2838).ToUpper & ":"  '"TOTAL" 
        _amount = m_total
        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
    End Select

    If Me.chk_group_progressive.Checked = False OrElse _
      (Me.chk_group_progressive.Checked And Me.chk_view_detail.Checked) Then
      _col_Text = GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_PAID_DATE
      _col_Total = GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_AMOUNT
    Else
      _col_Text = GRID_COLUMN_GROUP_PROGRESSIVE_NAME
      _col_Total = GRID_COLUMN_GROUP_PROGRESSIVE_JACKPOT_AMOUNT
    End If
    Me.Grid.AddRow()
    _idx_row = Me.Grid.NumRows - 1

    Me.Grid.Cell(_idx_row, _col_Text).Value = _str_total
    Me.Grid.Cell(_idx_row, _col_Total).Value = GUI_FormatCurrency(_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Row(_idx_row).BackColor = GetColor(_color)

  End Sub ' DrawSubTotal

  ' PURPOSE: Reset Counters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ResetCountersProgressive()
    m_total_progressive = 0
  End Sub 'ResetCountersProgressive

  ' PURPOSE: Reset Counters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ResetTotalCounters()
    m_total = 0
  End Sub 'ResetTotalCounters

  ' PURPOSE: Set Addittional info when show detail terminal (Area, Bank, etc.)
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GridColumnReIndex()
    TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(m_terminal_report_type)

    GRID_INIT_TERMINAL_DATA = 3
    GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_AMOUNT = TERMINAL_DATA_COLUMNS + 3
    GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_DATE = TERMINAL_DATA_COLUMNS + 4
    GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_LEVEL = TERMINAL_DATA_COLUMNS + 5
    GRID_COLUMN_DETAIL_PROGRESSIVE_JACKPOT_LEVEL_NAME = TERMINAL_DATA_COLUMNS + 6

    GRID_COLUMNS_PROGRESSIVE = TERMINAL_DATA_COLUMNS + 7

  End Sub

#End Region ' Private Functions

#Region " Region Events "

  Private Sub chk_terminal_location_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminal_location.CheckedChanged
    If chk_terminal_location.Checked Then
      m_terminal_report_type = ReportType.Provider + ReportType.Location
    Else
      m_terminal_report_type = ReportType.Provider
    End If

  End Sub ' chk_terminal_location_CheckedChanged

  Private Sub chk_group_progressive_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_group_progressive.CheckedChanged
    Me.chk_view_detail.Checked = False
    Me.chk_view_detail.Enabled = Me.chk_group_progressive.Checked

    Me.chk_terminal_location.Enabled = Not Me.chk_group_progressive.Checked
  End Sub ' chk_group_progressive_CheckedChanged

  Private Sub chk_view_detail_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_view_detail.CheckedChanged
    Me.chk_terminal_location.Enabled = Me.chk_view_detail.Checked
  End Sub ' chk_view_detail_CheckedChanged


#End Region ' Events

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.

  End Sub

End Class