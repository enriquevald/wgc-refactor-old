<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_users_assignation
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_users_assignation))
    Me.ef_area_name = New GUI_Controls.uc_entry_field()
    Me.ef_zone_name = New GUI_Controls.uc_entry_field()
    Me.ef_bank_name = New GUI_Controls.uc_entry_field()
    Me.pn_separator_line = New System.Windows.Forms.Panel()
    Me.lb_all_runners = New System.Windows.Forms.ListBox()
    Me.lb_assigned_runners = New System.Windows.Forms.ListBox()
    Me.btn_add_selected = New System.Windows.Forms.Button()
    Me.btn_delete_selected = New System.Windows.Forms.Button()
    Me.btn_add_all = New System.Windows.Forms.Button()
    Me.btn_delete_all = New System.Windows.Forms.Button()
    Me.panel_data.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.panel_data.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.panel_data.Controls.Add(Me.btn_delete_all)
    Me.panel_data.Controls.Add(Me.btn_add_all)
    Me.panel_data.Controls.Add(Me.btn_delete_selected)
    Me.panel_data.Controls.Add(Me.btn_add_selected)
    Me.panel_data.Controls.Add(Me.lb_assigned_runners)
    Me.panel_data.Controls.Add(Me.lb_all_runners)
    Me.panel_data.Controls.Add(Me.pn_separator_line)
    Me.panel_data.Controls.Add(Me.ef_bank_name)
    Me.panel_data.Controls.Add(Me.ef_zone_name)
    Me.panel_data.Controls.Add(Me.ef_area_name)
    Me.panel_data.Dock = System.Windows.Forms.DockStyle.Top
    Me.panel_data.Size = New System.Drawing.Size(576, 483)
    '
    'ef_area_name
    '
    Me.ef_area_name.DoubleValue = 0.0R
    Me.ef_area_name.IntegerValue = 0
    Me.ef_area_name.IsReadOnly = False
    Me.ef_area_name.Location = New System.Drawing.Point(15, 14)
    Me.ef_area_name.Name = "ef_area_name"
    Me.ef_area_name.PlaceHolder = Nothing
    Me.ef_area_name.ShortcutsEnabled = True
    Me.ef_area_name.Size = New System.Drawing.Size(200, 24)
    Me.ef_area_name.SufixText = "Sufix Text"
    Me.ef_area_name.SufixTextVisible = True
    Me.ef_area_name.TabIndex = 0
    Me.ef_area_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_area_name.TextValue = ""
    Me.ef_area_name.Value = ""
    Me.ef_area_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_zone_name
    '
    Me.ef_zone_name.DoubleValue = 0.0R
    Me.ef_zone_name.IntegerValue = 0
    Me.ef_zone_name.IsReadOnly = False
    Me.ef_zone_name.Location = New System.Drawing.Point(15, 44)
    Me.ef_zone_name.Name = "ef_zone_name"
    Me.ef_zone_name.PlaceHolder = Nothing
    Me.ef_zone_name.ShortcutsEnabled = True
    Me.ef_zone_name.Size = New System.Drawing.Size(200, 24)
    Me.ef_zone_name.SufixText = "Sufix Text"
    Me.ef_zone_name.SufixTextVisible = True
    Me.ef_zone_name.TabIndex = 1
    Me.ef_zone_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_zone_name.TextValue = ""
    Me.ef_zone_name.Value = ""
    Me.ef_zone_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_bank_name
    '
    Me.ef_bank_name.DoubleValue = 0.0R
    Me.ef_bank_name.IntegerValue = 0
    Me.ef_bank_name.IsReadOnly = False
    Me.ef_bank_name.Location = New System.Drawing.Point(333, 14)
    Me.ef_bank_name.Name = "ef_bank_name"
    Me.ef_bank_name.PlaceHolder = Nothing
    Me.ef_bank_name.ShortcutsEnabled = True
    Me.ef_bank_name.Size = New System.Drawing.Size(200, 24)
    Me.ef_bank_name.SufixText = "Sufix Text"
    Me.ef_bank_name.SufixTextVisible = True
    Me.ef_bank_name.TabIndex = 2
    Me.ef_bank_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_bank_name.TextValue = ""
    Me.ef_bank_name.Value = ""
    Me.ef_bank_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Location = New System.Drawing.Point(3, 74)
    Me.pn_separator_line.Name = "pn_separator_line"
    Me.pn_separator_line.Size = New System.Drawing.Size(565, 10)
    Me.pn_separator_line.TabIndex = 3
    '
    'lb_all_runners
    '
    Me.lb_all_runners.FormattingEnabled = True
    Me.lb_all_runners.Location = New System.Drawing.Point(15, 100)
    Me.lb_all_runners.Name = "lb_all_runners"
    Me.lb_all_runners.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
    Me.lb_all_runners.Size = New System.Drawing.Size(209, 355)
    Me.lb_all_runners.TabIndex = 4
    '
    'lb_assigned_runners
    '
    Me.lb_assigned_runners.FormattingEnabled = True
    Me.lb_assigned_runners.Location = New System.Drawing.Point(324, 96)
    Me.lb_assigned_runners.Name = "lb_assigned_runners"
    Me.lb_assigned_runners.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
    Me.lb_assigned_runners.Size = New System.Drawing.Size(209, 355)
    Me.lb_assigned_runners.TabIndex = 5
    '
    'btn_add_selected
    '
    Me.btn_add_selected.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btn_add_selected.Location = New System.Drawing.Point(251, 167)
    Me.btn_add_selected.Name = "btn_add_selected"
    Me.btn_add_selected.Size = New System.Drawing.Size(45, 38)
    Me.btn_add_selected.TabIndex = 6
    Me.btn_add_selected.Text = ">"
    Me.btn_add_selected.UseVisualStyleBackColor = True
    '
    'btn_delete_selected
    '
    Me.btn_delete_selected.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btn_delete_selected.Location = New System.Drawing.Point(251, 211)
    Me.btn_delete_selected.Name = "btn_delete_selected"
    Me.btn_delete_selected.Size = New System.Drawing.Size(45, 38)
    Me.btn_delete_selected.TabIndex = 7
    Me.btn_delete_selected.Text = "<"
    Me.btn_delete_selected.UseVisualStyleBackColor = True
    '
    'btn_add_all
    '
    Me.btn_add_all.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btn_add_all.Location = New System.Drawing.Point(251, 299)
    Me.btn_add_all.Name = "btn_add_all"
    Me.btn_add_all.Size = New System.Drawing.Size(45, 38)
    Me.btn_add_all.TabIndex = 8
    Me.btn_add_all.Text = ">>"
    Me.btn_add_all.UseVisualStyleBackColor = True
    '
    'btn_delete_all
    '
    Me.btn_delete_all.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btn_delete_all.Location = New System.Drawing.Point(251, 343)
    Me.btn_delete_all.Name = "btn_delete_all"
    Me.btn_delete_all.Size = New System.Drawing.Size(45, 38)
    Me.btn_delete_all.TabIndex = 9
    Me.btn_delete_all.Text = "<<"
    Me.btn_delete_all.UseVisualStyleBackColor = True
    '
    'frm_users_assignation
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(672, 491)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
    Me.Name = "frm_users_assignation"
    Me.Text = "frm_users_assignation"
    Me.panel_data.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_bank_name As GUI_Controls.uc_entry_field
  Friend WithEvents ef_zone_name As GUI_Controls.uc_entry_field
  Friend WithEvents ef_area_name As GUI_Controls.uc_entry_field
  Friend WithEvents pn_separator_line As System.Windows.Forms.Panel
  Friend WithEvents lb_assigned_runners As System.Windows.Forms.ListBox
  Friend WithEvents lb_all_runners As System.Windows.Forms.ListBox
  Friend WithEvents btn_delete_all As System.Windows.Forms.Button
  Friend WithEvents btn_add_all As System.Windows.Forms.Button
  Friend WithEvents btn_delete_selected As System.Windows.Forms.Button
  Friend WithEvents btn_add_selected As System.Windows.Forms.Button
End Class
