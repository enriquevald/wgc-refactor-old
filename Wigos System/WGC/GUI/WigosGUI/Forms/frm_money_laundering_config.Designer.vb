<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_money_laundering_config
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.lbl_minwage_desc = New GUI_Controls.uc_text_field
    Me.tc_limits = New System.Windows.Forms.TabControl
    Me.tp_recharge = New System.Windows.Forms.TabPage
    Me.chk_rchg_maximum_limit = New System.Windows.Forms.CheckBox
    Me.gb_rchg_maximum_limit = New System.Windows.Forms.GroupBox
    Me.tb_rchg_maximum_msg = New System.Windows.Forms.TextBox
    Me.lbl_rchg_maximum_absolute = New GUI_Controls.uc_text_field
    Me.ef_rchg_maximum_limit = New GUI_Controls.uc_entry_field
    Me.lbl_rchg_maximum_limit = New System.Windows.Forms.Label
    Me.lbl_rchg_maximum_msg = New System.Windows.Forms.Label
    Me.gb_rchg_report_limit = New System.Windows.Forms.GroupBox
    Me.tb_rchg_report_msg = New System.Windows.Forms.TextBox
    Me.lbl_rchg_exceed_report_limit = New System.Windows.Forms.Label
    Me.lbl_rchg_report_absolute = New GUI_Controls.uc_text_field
    Me.cmb_exceed_recharge = New GUI_Controls.uc_combo
    Me.ef_rchg_report_limit = New GUI_Controls.uc_entry_field
    Me.chk_rchg_report_limit_message = New System.Windows.Forms.CheckBox
    Me.lbl_rchg_report_limit = New System.Windows.Forms.Label
    Me.lbl_rchg_report_msg = New System.Windows.Forms.Label
    Me.gb_rchg_ident_limit = New System.Windows.Forms.GroupBox
    Me.lbl_rchg_ident_absolute = New GUI_Controls.uc_text_field
    Me.tb_rchg_ident_msg = New System.Windows.Forms.TextBox
    Me.lbl_rchg_ident_msg = New System.Windows.Forms.Label
    Me.ef_rchg_ident_limit = New GUI_Controls.uc_entry_field
    Me.lbl_rchg_ident_limit = New System.Windows.Forms.Label
    Me.chk_rchg_report_warning = New System.Windows.Forms.CheckBox
    Me.chk_rchg_ident_warning = New System.Windows.Forms.CheckBox
    Me.gb_rchg_report_warning = New System.Windows.Forms.GroupBox
    Me.chk_report_not_allow_mb = New System.Windows.Forms.CheckBox
    Me.lbl_rchg_report_warning_msg = New System.Windows.Forms.Label
    Me.ef_rchg_report_warning_limit = New GUI_Controls.uc_entry_field
    Me.tb_rchg_report_warning_msg = New System.Windows.Forms.TextBox
    Me.lbl_rchg_report_warning_absolute = New GUI_Controls.uc_text_field
    Me.lbl_rchg_report_warning_limit = New System.Windows.Forms.Label
    Me.gb_rchg_ident_warning = New System.Windows.Forms.GroupBox
    Me.chk_ident_not_allow_mb = New System.Windows.Forms.CheckBox
    Me.lbl_rchg_ident_warning_msg = New System.Windows.Forms.Label
    Me.ef_rchg_ident_warning_limit = New GUI_Controls.uc_entry_field
    Me.tb_rchg_ident_warning_msg = New System.Windows.Forms.TextBox
    Me.lbl_rchg_ident_warning_absolute = New GUI_Controls.uc_text_field
    Me.lbl_rchg_ident_warning_limit = New System.Windows.Forms.Label
    Me.tp_prize = New System.Windows.Forms.TabPage
    Me.chk_prize_ident_warning = New System.Windows.Forms.CheckBox
    Me.chk_prize_maximum_limit = New System.Windows.Forms.CheckBox
    Me.chk_prize_report_warning = New System.Windows.Forms.CheckBox
    Me.gb_prize_maximum_limit = New System.Windows.Forms.GroupBox
    Me.tb_prize_maximum_msg = New System.Windows.Forms.TextBox
    Me.lbl_prize_maximum_absolute = New GUI_Controls.uc_text_field
    Me.ef_prize_maximum_limit = New GUI_Controls.uc_entry_field
    Me.lbl_prize_maxim_limit = New System.Windows.Forms.Label
    Me.lbl_prize_maximum_msg = New System.Windows.Forms.Label
    Me.gb_prize_report_limit = New System.Windows.Forms.GroupBox
    Me.tb_prize_report_msg = New System.Windows.Forms.TextBox
    Me.lbl_prize_exceed_report_limit = New System.Windows.Forms.Label
    Me.lbl_prize_report_absolute = New GUI_Controls.uc_text_field
    Me.cmb_exceed_prize = New GUI_Controls.uc_combo
    Me.ef_prize_report_limit = New GUI_Controls.uc_entry_field
    Me.chk_prize_report_limit_message = New System.Windows.Forms.CheckBox
    Me.lbl_prize_report_limit = New System.Windows.Forms.Label
    Me.lbl_prize_report_msg = New System.Windows.Forms.Label
    Me.gb_prize_ident_limit = New System.Windows.Forms.GroupBox
    Me.ef_prize_ident_limit = New GUI_Controls.uc_entry_field
    Me.lbl_prize_ident_absolute = New GUI_Controls.uc_text_field
    Me.lbl_prize_ident_limit = New System.Windows.Forms.Label
    Me.lbl_prize_ident_msg = New System.Windows.Forms.Label
    Me.tb_prize_ident_msg = New System.Windows.Forms.TextBox
    Me.gb_prize_report_warning = New System.Windows.Forms.GroupBox
    Me.lbl_prize_report_warning_msg = New System.Windows.Forms.Label
    Me.ef_prize_report_warning_limit = New GUI_Controls.uc_entry_field
    Me.tb_prize_report_warning_msg = New System.Windows.Forms.TextBox
    Me.lbl_prize_report_warning_absolute = New GUI_Controls.uc_text_field
    Me.lbl_prize_report_warning_limit = New System.Windows.Forms.Label
    Me.gb_prize_ident_warning = New System.Windows.Forms.GroupBox
    Me.lbl_prize_ident_warning_msg = New System.Windows.Forms.Label
    Me.ef_prize_ident_warning_limit = New GUI_Controls.uc_entry_field
    Me.tb_prize_ident_warning_msg = New System.Windows.Forms.TextBox
    Me.lbl_prize_ident_warning_absolute = New GUI_Controls.uc_text_field
    Me.lbl_prize_ident_warning_limit = New System.Windows.Forms.Label
    Me.ef_minimum_wage = New GUI_Controls.uc_entry_field
    Me.lbl_info = New System.Windows.Forms.Label
    Me.ef_aml_name = New GUI_Controls.uc_entry_field
    Me.chk_active_aml = New System.Windows.Forms.CheckBox
    Me.gb_limit = New System.Windows.Forms.GroupBox
    Me.chk_has_beneficiary = New System.Windows.Forms.CheckBox
    Me.panel_data.SuspendLayout()
    Me.tc_limits.SuspendLayout()
    Me.tp_recharge.SuspendLayout()
    Me.gb_rchg_maximum_limit.SuspendLayout()
    Me.gb_rchg_report_limit.SuspendLayout()
    Me.gb_rchg_ident_limit.SuspendLayout()
    Me.gb_rchg_report_warning.SuspendLayout()
    Me.gb_rchg_ident_warning.SuspendLayout()
    Me.tp_prize.SuspendLayout()
    Me.gb_prize_maximum_limit.SuspendLayout()
    Me.gb_prize_report_limit.SuspendLayout()
    Me.gb_prize_ident_limit.SuspendLayout()
    Me.gb_prize_report_warning.SuspendLayout()
    Me.gb_prize_ident_warning.SuspendLayout()
    Me.gb_limit.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.chk_has_beneficiary)
    Me.panel_data.Controls.Add(Me.lbl_info)
    Me.panel_data.Controls.Add(Me.ef_aml_name)
    Me.panel_data.Controls.Add(Me.chk_active_aml)
    Me.panel_data.Controls.Add(Me.gb_limit)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(840, 636)
    '
    'lbl_minwage_desc
    '
    Me.lbl_minwage_desc.IsReadOnly = True
    Me.lbl_minwage_desc.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_minwage_desc.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_minwage_desc.Location = New System.Drawing.Point(6, 16)
    Me.lbl_minwage_desc.Name = "lbl_minwage_desc"
    Me.lbl_minwage_desc.Size = New System.Drawing.Size(795, 24)
    Me.lbl_minwage_desc.SufixText = "Sufix Text"
    Me.lbl_minwage_desc.SufixTextVisible = True
    Me.lbl_minwage_desc.TabIndex = 0
    Me.lbl_minwage_desc.TabStop = False
    Me.lbl_minwage_desc.TextVisible = False
    Me.lbl_minwage_desc.TextWidth = 0
    Me.lbl_minwage_desc.Value = "xDesc"
    '
    'tc_limits
    '
    Me.tc_limits.Controls.Add(Me.tp_recharge)
    Me.tc_limits.Controls.Add(Me.tp_prize)
    Me.tc_limits.Location = New System.Drawing.Point(4, 73)
    Me.tc_limits.Name = "tc_limits"
    Me.tc_limits.SelectedIndex = 0
    Me.tc_limits.Size = New System.Drawing.Size(814, 470)
    Me.tc_limits.TabIndex = 1
    '
    'tp_recharge
    '
    Me.tp_recharge.Controls.Add(Me.chk_rchg_maximum_limit)
    Me.tp_recharge.Controls.Add(Me.gb_rchg_maximum_limit)
    Me.tp_recharge.Controls.Add(Me.gb_rchg_report_limit)
    Me.tp_recharge.Controls.Add(Me.gb_rchg_ident_limit)
    Me.tp_recharge.Controls.Add(Me.chk_rchg_report_warning)
    Me.tp_recharge.Controls.Add(Me.chk_rchg_ident_warning)
    Me.tp_recharge.Controls.Add(Me.gb_rchg_report_warning)
    Me.tp_recharge.Controls.Add(Me.gb_rchg_ident_warning)
    Me.tp_recharge.Location = New System.Drawing.Point(4, 22)
    Me.tp_recharge.Name = "tp_recharge"
    Me.tp_recharge.Padding = New System.Windows.Forms.Padding(3)
    Me.tp_recharge.Size = New System.Drawing.Size(806, 444)
    Me.tp_recharge.TabIndex = 0
    Me.tp_recharge.Text = "xRecharge"
    Me.tp_recharge.UseVisualStyleBackColor = True
    '
    'chk_rchg_maximum_limit
    '
    Me.chk_rchg_maximum_limit.AutoSize = True
    Me.chk_rchg_maximum_limit.Location = New System.Drawing.Point(18, 314)
    Me.chk_rchg_maximum_limit.Name = "chk_rchg_maximum_limit"
    Me.chk_rchg_maximum_limit.Size = New System.Drawing.Size(160, 17)
    Me.chk_rchg_maximum_limit.TabIndex = 6
    Me.chk_rchg_maximum_limit.Text = "xActivatemaximumlimit"
    Me.chk_rchg_maximum_limit.TextAlign = System.Drawing.ContentAlignment.TopCenter
    Me.chk_rchg_maximum_limit.UseVisualStyleBackColor = True
    '
    'gb_rchg_maximum_limit
    '
    Me.gb_rchg_maximum_limit.Controls.Add(Me.tb_rchg_maximum_msg)
    Me.gb_rchg_maximum_limit.Controls.Add(Me.lbl_rchg_maximum_absolute)
    Me.gb_rchg_maximum_limit.Controls.Add(Me.ef_rchg_maximum_limit)
    Me.gb_rchg_maximum_limit.Controls.Add(Me.lbl_rchg_maximum_limit)
    Me.gb_rchg_maximum_limit.Controls.Add(Me.lbl_rchg_maximum_msg)
    Me.gb_rchg_maximum_limit.Location = New System.Drawing.Point(6, 313)
    Me.gb_rchg_maximum_limit.Name = "gb_rchg_maximum_limit"
    Me.gb_rchg_maximum_limit.Size = New System.Drawing.Size(390, 127)
    Me.gb_rchg_maximum_limit.TabIndex = 6
    Me.gb_rchg_maximum_limit.TabStop = False
    '
    'tb_rchg_maximum_msg
    '
    Me.tb_rchg_maximum_msg.AcceptsReturn = True
    Me.tb_rchg_maximum_msg.Location = New System.Drawing.Point(122, 58)
    Me.tb_rchg_maximum_msg.MaxLength = 256
    Me.tb_rchg_maximum_msg.Multiline = True
    Me.tb_rchg_maximum_msg.Name = "tb_rchg_maximum_msg"
    Me.tb_rchg_maximum_msg.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.tb_rchg_maximum_msg.Size = New System.Drawing.Size(258, 50)
    Me.tb_rchg_maximum_msg.TabIndex = 2
    '
    'lbl_rchg_maximum_absolute
    '
    Me.lbl_rchg_maximum_absolute.IsReadOnly = True
    Me.lbl_rchg_maximum_absolute.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_rchg_maximum_absolute.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_rchg_maximum_absolute.Location = New System.Drawing.Point(228, 30)
    Me.lbl_rchg_maximum_absolute.Name = "lbl_rchg_maximum_absolute"
    Me.lbl_rchg_maximum_absolute.Size = New System.Drawing.Size(151, 24)
    Me.lbl_rchg_maximum_absolute.SufixText = "Sufix Text"
    Me.lbl_rchg_maximum_absolute.TabIndex = 0
    Me.lbl_rchg_maximum_absolute.TextVisible = False
    Me.lbl_rchg_maximum_absolute.TextWidth = 0
    Me.lbl_rchg_maximum_absolute.Value = "$0.00"
    '
    'ef_rchg_maximum_limit
    '
    Me.ef_rchg_maximum_limit.DoubleValue = 0
    Me.ef_rchg_maximum_limit.IntegerValue = 0
    Me.ef_rchg_maximum_limit.IsReadOnly = False
    Me.ef_rchg_maximum_limit.Location = New System.Drawing.Point(120, 30)
    Me.ef_rchg_maximum_limit.Name = "ef_rchg_maximum_limit"
    Me.ef_rchg_maximum_limit.Size = New System.Drawing.Size(101, 24)
    Me.ef_rchg_maximum_limit.SufixText = "Sufix Text"
    Me.ef_rchg_maximum_limit.TabIndex = 1
    Me.ef_rchg_maximum_limit.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_rchg_maximum_limit.TextValue = ""
    Me.ef_rchg_maximum_limit.TextVisible = False
    Me.ef_rchg_maximum_limit.TextWidth = 0
    Me.ef_rchg_maximum_limit.Value = ""
    '
    'lbl_rchg_maximum_limit
    '
    Me.lbl_rchg_maximum_limit.Location = New System.Drawing.Point(17, 36)
    Me.lbl_rchg_maximum_limit.Name = "lbl_rchg_maximum_limit"
    Me.lbl_rchg_maximum_limit.Size = New System.Drawing.Size(101, 13)
    Me.lbl_rchg_maximum_limit.TabIndex = 0
    Me.lbl_rchg_maximum_limit.Text = "xMaxLimit"
    Me.lbl_rchg_maximum_limit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_rchg_maximum_msg
    '
    Me.lbl_rchg_maximum_msg.Location = New System.Drawing.Point(17, 63)
    Me.lbl_rchg_maximum_msg.Name = "lbl_rchg_maximum_msg"
    Me.lbl_rchg_maximum_msg.Size = New System.Drawing.Size(101, 13)
    Me.lbl_rchg_maximum_msg.TabIndex = 0
    Me.lbl_rchg_maximum_msg.Text = "xMessage"
    Me.lbl_rchg_maximum_msg.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'gb_rchg_report_limit
    '
    Me.gb_rchg_report_limit.Controls.Add(Me.tb_rchg_report_msg)
    Me.gb_rchg_report_limit.Controls.Add(Me.lbl_rchg_exceed_report_limit)
    Me.gb_rchg_report_limit.Controls.Add(Me.lbl_rchg_report_absolute)
    Me.gb_rchg_report_limit.Controls.Add(Me.cmb_exceed_recharge)
    Me.gb_rchg_report_limit.Controls.Add(Me.ef_rchg_report_limit)
    Me.gb_rchg_report_limit.Controls.Add(Me.chk_rchg_report_limit_message)
    Me.gb_rchg_report_limit.Controls.Add(Me.lbl_rchg_report_limit)
    Me.gb_rchg_report_limit.Controls.Add(Me.lbl_rchg_report_msg)
    Me.gb_rchg_report_limit.Location = New System.Drawing.Point(6, 146)
    Me.gb_rchg_report_limit.Name = "gb_rchg_report_limit"
    Me.gb_rchg_report_limit.Size = New System.Drawing.Size(390, 164)
    Me.gb_rchg_report_limit.TabIndex = 3
    Me.gb_rchg_report_limit.TabStop = False
    '
    'tb_rchg_report_msg
    '
    Me.tb_rchg_report_msg.AcceptsReturn = True
    Me.tb_rchg_report_msg.Location = New System.Drawing.Point(122, 55)
    Me.tb_rchg_report_msg.MaxLength = 256
    Me.tb_rchg_report_msg.Multiline = True
    Me.tb_rchg_report_msg.Name = "tb_rchg_report_msg"
    Me.tb_rchg_report_msg.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.tb_rchg_report_msg.Size = New System.Drawing.Size(258, 50)
    Me.tb_rchg_report_msg.TabIndex = 2
    '
    'lbl_rchg_exceed_report_limit
    '
    Me.lbl_rchg_exceed_report_limit.Location = New System.Drawing.Point(2, 137)
    Me.lbl_rchg_exceed_report_limit.Name = "lbl_rchg_exceed_report_limit"
    Me.lbl_rchg_exceed_report_limit.Size = New System.Drawing.Size(115, 18)
    Me.lbl_rchg_exceed_report_limit.TabIndex = 0
    Me.lbl_rchg_exceed_report_limit.Text = "xReportLimit"
    Me.lbl_rchg_exceed_report_limit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_rchg_report_absolute
    '
    Me.lbl_rchg_report_absolute.IsReadOnly = True
    Me.lbl_rchg_report_absolute.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_rchg_report_absolute.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_rchg_report_absolute.Location = New System.Drawing.Point(228, 26)
    Me.lbl_rchg_report_absolute.Name = "lbl_rchg_report_absolute"
    Me.lbl_rchg_report_absolute.Size = New System.Drawing.Size(151, 24)
    Me.lbl_rchg_report_absolute.SufixText = "Sufix Text"
    Me.lbl_rchg_report_absolute.TabIndex = 0
    Me.lbl_rchg_report_absolute.TabStop = False
    Me.lbl_rchg_report_absolute.TextVisible = False
    Me.lbl_rchg_report_absolute.TextWidth = 0
    Me.lbl_rchg_report_absolute.Value = "$0.00"
    '
    'cmb_exceed_recharge
    '
    Me.cmb_exceed_recharge.AllowUnlistedValues = False
    Me.cmb_exceed_recharge.AutoCompleteMode = False
    Me.cmb_exceed_recharge.IsReadOnly = False
    Me.cmb_exceed_recharge.Location = New System.Drawing.Point(120, 134)
    Me.cmb_exceed_recharge.Name = "cmb_exceed_recharge"
    Me.cmb_exceed_recharge.SelectedIndex = -1
    Me.cmb_exceed_recharge.Size = New System.Drawing.Size(136, 24)
    Me.cmb_exceed_recharge.SufixText = "Sufix Text"
    Me.cmb_exceed_recharge.TabIndex = 4
    Me.cmb_exceed_recharge.TextVisible = False
    Me.cmb_exceed_recharge.TextWidth = 0
    '
    'ef_rchg_report_limit
    '
    Me.ef_rchg_report_limit.DoubleValue = 0
    Me.ef_rchg_report_limit.IntegerValue = 0
    Me.ef_rchg_report_limit.IsReadOnly = False
    Me.ef_rchg_report_limit.Location = New System.Drawing.Point(120, 27)
    Me.ef_rchg_report_limit.Name = "ef_rchg_report_limit"
    Me.ef_rchg_report_limit.Size = New System.Drawing.Size(101, 24)
    Me.ef_rchg_report_limit.SufixText = "Sufix Text"
    Me.ef_rchg_report_limit.TabIndex = 1
    Me.ef_rchg_report_limit.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_rchg_report_limit.TextValue = ""
    Me.ef_rchg_report_limit.TextVisible = False
    Me.ef_rchg_report_limit.TextWidth = 0
    Me.ef_rchg_report_limit.Value = ""
    '
    'chk_rchg_report_limit_message
    '
    Me.chk_rchg_report_limit_message.AutoSize = True
    Me.chk_rchg_report_limit_message.Location = New System.Drawing.Point(120, 111)
    Me.chk_rchg_report_limit_message.Name = "chk_rchg_report_limit_message"
    Me.chk_rchg_report_limit_message.Size = New System.Drawing.Size(178, 17)
    Me.chk_rchg_report_limit_message.TabIndex = 3
    Me.chk_rchg_report_limit_message.Text = "xShowReportLimitMessage"
    Me.chk_rchg_report_limit_message.TextAlign = System.Drawing.ContentAlignment.TopCenter
    Me.chk_rchg_report_limit_message.UseVisualStyleBackColor = True
    '
    'lbl_rchg_report_limit
    '
    Me.lbl_rchg_report_limit.Location = New System.Drawing.Point(17, 32)
    Me.lbl_rchg_report_limit.Name = "lbl_rchg_report_limit"
    Me.lbl_rchg_report_limit.Size = New System.Drawing.Size(101, 13)
    Me.lbl_rchg_report_limit.TabIndex = 0
    Me.lbl_rchg_report_limit.Text = "xReportLimit"
    Me.lbl_rchg_report_limit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_rchg_report_msg
    '
    Me.lbl_rchg_report_msg.Location = New System.Drawing.Point(17, 58)
    Me.lbl_rchg_report_msg.Name = "lbl_rchg_report_msg"
    Me.lbl_rchg_report_msg.Size = New System.Drawing.Size(101, 13)
    Me.lbl_rchg_report_msg.TabIndex = 0
    Me.lbl_rchg_report_msg.Text = "xMessage"
    Me.lbl_rchg_report_msg.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'gb_rchg_ident_limit
    '
    Me.gb_rchg_ident_limit.Controls.Add(Me.lbl_rchg_ident_absolute)
    Me.gb_rchg_ident_limit.Controls.Add(Me.tb_rchg_ident_msg)
    Me.gb_rchg_ident_limit.Controls.Add(Me.lbl_rchg_ident_msg)
    Me.gb_rchg_ident_limit.Controls.Add(Me.ef_rchg_ident_limit)
    Me.gb_rchg_ident_limit.Controls.Add(Me.lbl_rchg_ident_limit)
    Me.gb_rchg_ident_limit.Location = New System.Drawing.Point(6, 4)
    Me.gb_rchg_ident_limit.Name = "gb_rchg_ident_limit"
    Me.gb_rchg_ident_limit.Size = New System.Drawing.Size(390, 138)
    Me.gb_rchg_ident_limit.TabIndex = 1
    Me.gb_rchg_ident_limit.TabStop = False
    '
    'lbl_rchg_ident_absolute
    '
    Me.lbl_rchg_ident_absolute.IsReadOnly = True
    Me.lbl_rchg_ident_absolute.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_rchg_ident_absolute.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_rchg_ident_absolute.Location = New System.Drawing.Point(227, 29)
    Me.lbl_rchg_ident_absolute.Name = "lbl_rchg_ident_absolute"
    Me.lbl_rchg_ident_absolute.Size = New System.Drawing.Size(153, 24)
    Me.lbl_rchg_ident_absolute.SufixText = "Sufix Text"
    Me.lbl_rchg_ident_absolute.TabIndex = 0
    Me.lbl_rchg_ident_absolute.TabStop = False
    Me.lbl_rchg_ident_absolute.TextVisible = False
    Me.lbl_rchg_ident_absolute.TextWidth = 0
    Me.lbl_rchg_ident_absolute.Value = "$0.00"
    '
    'tb_rchg_ident_msg
    '
    Me.tb_rchg_ident_msg.AcceptsReturn = True
    Me.tb_rchg_ident_msg.Location = New System.Drawing.Point(122, 60)
    Me.tb_rchg_ident_msg.MaxLength = 256
    Me.tb_rchg_ident_msg.Multiline = True
    Me.tb_rchg_ident_msg.Name = "tb_rchg_ident_msg"
    Me.tb_rchg_ident_msg.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.tb_rchg_ident_msg.Size = New System.Drawing.Size(258, 50)
    Me.tb_rchg_ident_msg.TabIndex = 2
    '
    'lbl_rchg_ident_msg
    '
    Me.lbl_rchg_ident_msg.Location = New System.Drawing.Point(56, 65)
    Me.lbl_rchg_ident_msg.Name = "lbl_rchg_ident_msg"
    Me.lbl_rchg_ident_msg.Size = New System.Drawing.Size(63, 13)
    Me.lbl_rchg_ident_msg.TabIndex = 0
    Me.lbl_rchg_ident_msg.Text = "xMessage"
    Me.lbl_rchg_ident_msg.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'ef_rchg_ident_limit
    '
    Me.ef_rchg_ident_limit.DoubleValue = 0
    Me.ef_rchg_ident_limit.IntegerValue = 0
    Me.ef_rchg_ident_limit.IsReadOnly = False
    Me.ef_rchg_ident_limit.Location = New System.Drawing.Point(120, 30)
    Me.ef_rchg_ident_limit.Name = "ef_rchg_ident_limit"
    Me.ef_rchg_ident_limit.Size = New System.Drawing.Size(101, 24)
    Me.ef_rchg_ident_limit.SufixText = "Sufix Text"
    Me.ef_rchg_ident_limit.TabIndex = 1
    Me.ef_rchg_ident_limit.Tag = "5"
    Me.ef_rchg_ident_limit.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_rchg_ident_limit.TextValue = ""
    Me.ef_rchg_ident_limit.TextVisible = False
    Me.ef_rchg_ident_limit.TextWidth = 0
    Me.ef_rchg_ident_limit.Value = ""
    '
    'lbl_rchg_ident_limit
    '
    Me.lbl_rchg_ident_limit.Location = New System.Drawing.Point(16, 36)
    Me.lbl_rchg_ident_limit.Name = "lbl_rchg_ident_limit"
    Me.lbl_rchg_ident_limit.Size = New System.Drawing.Size(101, 13)
    Me.lbl_rchg_ident_limit.TabIndex = 1
    Me.lbl_rchg_ident_limit.Text = "xIdentifLimit"
    Me.lbl_rchg_ident_limit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'chk_rchg_report_warning
    '
    Me.chk_rchg_report_warning.AutoSize = True
    Me.chk_rchg_report_warning.Location = New System.Drawing.Point(415, 144)
    Me.chk_rchg_report_warning.Name = "chk_rchg_report_warning"
    Me.chk_rchg_report_warning.Size = New System.Drawing.Size(126, 17)
    Me.chk_rchg_report_warning.TabIndex = 4
    Me.chk_rchg_report_warning.Text = "xActivateWarning"
    Me.chk_rchg_report_warning.TextAlign = System.Drawing.ContentAlignment.TopCenter
    Me.chk_rchg_report_warning.UseVisualStyleBackColor = True
    '
    'chk_rchg_ident_warning
    '
    Me.chk_rchg_ident_warning.AutoSize = True
    Me.chk_rchg_ident_warning.Location = New System.Drawing.Point(415, 4)
    Me.chk_rchg_ident_warning.Name = "chk_rchg_ident_warning"
    Me.chk_rchg_ident_warning.Size = New System.Drawing.Size(126, 17)
    Me.chk_rchg_ident_warning.TabIndex = 2
    Me.chk_rchg_ident_warning.Text = "xActivateWarning"
    Me.chk_rchg_ident_warning.TextAlign = System.Drawing.ContentAlignment.TopCenter
    Me.chk_rchg_ident_warning.UseVisualStyleBackColor = True
    '
    'gb_rchg_report_warning
    '
    Me.gb_rchg_report_warning.Controls.Add(Me.chk_report_not_allow_mb)
    Me.gb_rchg_report_warning.Controls.Add(Me.lbl_rchg_report_warning_msg)
    Me.gb_rchg_report_warning.Controls.Add(Me.ef_rchg_report_warning_limit)
    Me.gb_rchg_report_warning.Controls.Add(Me.tb_rchg_report_warning_msg)
    Me.gb_rchg_report_warning.Controls.Add(Me.lbl_rchg_report_warning_absolute)
    Me.gb_rchg_report_warning.Controls.Add(Me.lbl_rchg_report_warning_limit)
    Me.gb_rchg_report_warning.Location = New System.Drawing.Point(406, 146)
    Me.gb_rchg_report_warning.Name = "gb_rchg_report_warning"
    Me.gb_rchg_report_warning.Size = New System.Drawing.Size(390, 164)
    Me.gb_rchg_report_warning.TabIndex = 5
    Me.gb_rchg_report_warning.TabStop = False
    '
    'chk_report_not_allow_mb
    '
    Me.chk_report_not_allow_mb.AutoSize = True
    Me.chk_report_not_allow_mb.Location = New System.Drawing.Point(9, 111)
    Me.chk_report_not_allow_mb.Name = "chk_report_not_allow_mb"
    Me.chk_report_not_allow_mb.Size = New System.Drawing.Size(153, 17)
    Me.chk_report_not_allow_mb.TabIndex = 3
    Me.chk_report_not_allow_mb.Text = "xNotAllowMBRecharge"
    Me.chk_report_not_allow_mb.TextAlign = System.Drawing.ContentAlignment.TopCenter
    Me.chk_report_not_allow_mb.UseVisualStyleBackColor = True
    '
    'lbl_rchg_report_warning_msg
    '
    Me.lbl_rchg_report_warning_msg.Location = New System.Drawing.Point(55, 60)
    Me.lbl_rchg_report_warning_msg.Name = "lbl_rchg_report_warning_msg"
    Me.lbl_rchg_report_warning_msg.Size = New System.Drawing.Size(63, 13)
    Me.lbl_rchg_report_warning_msg.TabIndex = 0
    Me.lbl_rchg_report_warning_msg.Text = "xMessage"
    Me.lbl_rchg_report_warning_msg.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'ef_rchg_report_warning_limit
    '
    Me.ef_rchg_report_warning_limit.DoubleValue = 0
    Me.ef_rchg_report_warning_limit.IntegerValue = 0
    Me.ef_rchg_report_warning_limit.IsReadOnly = False
    Me.ef_rchg_report_warning_limit.Location = New System.Drawing.Point(120, 26)
    Me.ef_rchg_report_warning_limit.Name = "ef_rchg_report_warning_limit"
    Me.ef_rchg_report_warning_limit.Size = New System.Drawing.Size(101, 24)
    Me.ef_rchg_report_warning_limit.SufixText = "Sufix Text"
    Me.ef_rchg_report_warning_limit.TabIndex = 1
    Me.ef_rchg_report_warning_limit.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_rchg_report_warning_limit.TextValue = ""
    Me.ef_rchg_report_warning_limit.TextVisible = False
    Me.ef_rchg_report_warning_limit.TextWidth = 0
    Me.ef_rchg_report_warning_limit.Value = ""
    '
    'tb_rchg_report_warning_msg
    '
    Me.tb_rchg_report_warning_msg.AcceptsReturn = True
    Me.tb_rchg_report_warning_msg.Location = New System.Drawing.Point(122, 55)
    Me.tb_rchg_report_warning_msg.MaxLength = 256
    Me.tb_rchg_report_warning_msg.Multiline = True
    Me.tb_rchg_report_warning_msg.Name = "tb_rchg_report_warning_msg"
    Me.tb_rchg_report_warning_msg.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.tb_rchg_report_warning_msg.Size = New System.Drawing.Size(258, 50)
    Me.tb_rchg_report_warning_msg.TabIndex = 2
    '
    'lbl_rchg_report_warning_absolute
    '
    Me.lbl_rchg_report_warning_absolute.IsReadOnly = True
    Me.lbl_rchg_report_warning_absolute.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_rchg_report_warning_absolute.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_rchg_report_warning_absolute.Location = New System.Drawing.Point(233, 26)
    Me.lbl_rchg_report_warning_absolute.Name = "lbl_rchg_report_warning_absolute"
    Me.lbl_rchg_report_warning_absolute.Size = New System.Drawing.Size(147, 24)
    Me.lbl_rchg_report_warning_absolute.SufixText = "Sufix Text"
    Me.lbl_rchg_report_warning_absolute.TabIndex = 0
    Me.lbl_rchg_report_warning_absolute.TabStop = False
    Me.lbl_rchg_report_warning_absolute.TextVisible = False
    Me.lbl_rchg_report_warning_absolute.TextWidth = 0
    Me.lbl_rchg_report_warning_absolute.Value = "$0.00"
    '
    'lbl_rchg_report_warning_limit
    '
    Me.lbl_rchg_report_warning_limit.Location = New System.Drawing.Point(17, 32)
    Me.lbl_rchg_report_warning_limit.Name = "lbl_rchg_report_warning_limit"
    Me.lbl_rchg_report_warning_limit.Size = New System.Drawing.Size(101, 13)
    Me.lbl_rchg_report_warning_limit.TabIndex = 0
    Me.lbl_rchg_report_warning_limit.Text = "xWarningLimit"
    Me.lbl_rchg_report_warning_limit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'gb_rchg_ident_warning
    '
    Me.gb_rchg_ident_warning.Controls.Add(Me.chk_ident_not_allow_mb)
    Me.gb_rchg_ident_warning.Controls.Add(Me.lbl_rchg_ident_warning_msg)
    Me.gb_rchg_ident_warning.Controls.Add(Me.ef_rchg_ident_warning_limit)
    Me.gb_rchg_ident_warning.Controls.Add(Me.tb_rchg_ident_warning_msg)
    Me.gb_rchg_ident_warning.Controls.Add(Me.lbl_rchg_ident_warning_absolute)
    Me.gb_rchg_ident_warning.Controls.Add(Me.lbl_rchg_ident_warning_limit)
    Me.gb_rchg_ident_warning.Location = New System.Drawing.Point(406, 4)
    Me.gb_rchg_ident_warning.Name = "gb_rchg_ident_warning"
    Me.gb_rchg_ident_warning.Size = New System.Drawing.Size(390, 138)
    Me.gb_rchg_ident_warning.TabIndex = 2
    Me.gb_rchg_ident_warning.TabStop = False
    '
    'chk_ident_not_allow_mb
    '
    Me.chk_ident_not_allow_mb.AutoSize = True
    Me.chk_ident_not_allow_mb.Location = New System.Drawing.Point(9, 115)
    Me.chk_ident_not_allow_mb.Name = "chk_ident_not_allow_mb"
    Me.chk_ident_not_allow_mb.Size = New System.Drawing.Size(153, 17)
    Me.chk_ident_not_allow_mb.TabIndex = 3
    Me.chk_ident_not_allow_mb.Text = "xNotAllowMBRecharge"
    Me.chk_ident_not_allow_mb.TextAlign = System.Drawing.ContentAlignment.TopCenter
    Me.chk_ident_not_allow_mb.UseVisualStyleBackColor = True
    '
    'lbl_rchg_ident_warning_msg
    '
    Me.lbl_rchg_ident_warning_msg.Location = New System.Drawing.Point(54, 65)
    Me.lbl_rchg_ident_warning_msg.Name = "lbl_rchg_ident_warning_msg"
    Me.lbl_rchg_ident_warning_msg.Size = New System.Drawing.Size(63, 13)
    Me.lbl_rchg_ident_warning_msg.TabIndex = 0
    Me.lbl_rchg_ident_warning_msg.Text = "xMessage"
    Me.lbl_rchg_ident_warning_msg.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'ef_rchg_ident_warning_limit
    '
    Me.ef_rchg_ident_warning_limit.DoubleValue = 0
    Me.ef_rchg_ident_warning_limit.IntegerValue = 0
    Me.ef_rchg_ident_warning_limit.IsReadOnly = False
    Me.ef_rchg_ident_warning_limit.Location = New System.Drawing.Point(120, 30)
    Me.ef_rchg_ident_warning_limit.Name = "ef_rchg_ident_warning_limit"
    Me.ef_rchg_ident_warning_limit.Size = New System.Drawing.Size(101, 24)
    Me.ef_rchg_ident_warning_limit.SufixText = "Sufix Text"
    Me.ef_rchg_ident_warning_limit.TabIndex = 1
    Me.ef_rchg_ident_warning_limit.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_rchg_ident_warning_limit.TextValue = ""
    Me.ef_rchg_ident_warning_limit.TextVisible = False
    Me.ef_rchg_ident_warning_limit.TextWidth = 0
    Me.ef_rchg_ident_warning_limit.Value = ""
    '
    'tb_rchg_ident_warning_msg
    '
    Me.tb_rchg_ident_warning_msg.AcceptsReturn = True
    Me.tb_rchg_ident_warning_msg.Location = New System.Drawing.Point(122, 60)
    Me.tb_rchg_ident_warning_msg.MaxLength = 256
    Me.tb_rchg_ident_warning_msg.Multiline = True
    Me.tb_rchg_ident_warning_msg.Name = "tb_rchg_ident_warning_msg"
    Me.tb_rchg_ident_warning_msg.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.tb_rchg_ident_warning_msg.Size = New System.Drawing.Size(258, 50)
    Me.tb_rchg_ident_warning_msg.TabIndex = 2
    '
    'lbl_rchg_ident_warning_absolute
    '
    Me.lbl_rchg_ident_warning_absolute.IsReadOnly = True
    Me.lbl_rchg_ident_warning_absolute.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_rchg_ident_warning_absolute.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_rchg_ident_warning_absolute.Location = New System.Drawing.Point(230, 30)
    Me.lbl_rchg_ident_warning_absolute.Name = "lbl_rchg_ident_warning_absolute"
    Me.lbl_rchg_ident_warning_absolute.Size = New System.Drawing.Size(151, 24)
    Me.lbl_rchg_ident_warning_absolute.SufixText = "Sufix Text"
    Me.lbl_rchg_ident_warning_absolute.TabIndex = 0
    Me.lbl_rchg_ident_warning_absolute.TabStop = False
    Me.lbl_rchg_ident_warning_absolute.TextVisible = False
    Me.lbl_rchg_ident_warning_absolute.TextWidth = 0
    Me.lbl_rchg_ident_warning_absolute.Value = "$0.00"
    '
    'lbl_rchg_ident_warning_limit
    '
    Me.lbl_rchg_ident_warning_limit.Location = New System.Drawing.Point(16, 36)
    Me.lbl_rchg_ident_warning_limit.Name = "lbl_rchg_ident_warning_limit"
    Me.lbl_rchg_ident_warning_limit.Size = New System.Drawing.Size(101, 13)
    Me.lbl_rchg_ident_warning_limit.TabIndex = 0
    Me.lbl_rchg_ident_warning_limit.Text = "xWarningLimit"
    Me.lbl_rchg_ident_warning_limit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'tp_prize
    '
    Me.tp_prize.Controls.Add(Me.chk_prize_ident_warning)
    Me.tp_prize.Controls.Add(Me.chk_prize_maximum_limit)
    Me.tp_prize.Controls.Add(Me.chk_prize_report_warning)
    Me.tp_prize.Controls.Add(Me.gb_prize_maximum_limit)
    Me.tp_prize.Controls.Add(Me.gb_prize_report_limit)
    Me.tp_prize.Controls.Add(Me.gb_prize_ident_limit)
    Me.tp_prize.Controls.Add(Me.gb_prize_report_warning)
    Me.tp_prize.Controls.Add(Me.gb_prize_ident_warning)
    Me.tp_prize.Location = New System.Drawing.Point(4, 22)
    Me.tp_prize.Name = "tp_prize"
    Me.tp_prize.Padding = New System.Windows.Forms.Padding(3)
    Me.tp_prize.Size = New System.Drawing.Size(806, 444)
    Me.tp_prize.TabIndex = 1
    Me.tp_prize.Text = "xPrize"
    Me.tp_prize.UseVisualStyleBackColor = True
    '
    'chk_prize_ident_warning
    '
    Me.chk_prize_ident_warning.AutoSize = True
    Me.chk_prize_ident_warning.Location = New System.Drawing.Point(415, 4)
    Me.chk_prize_ident_warning.Name = "chk_prize_ident_warning"
    Me.chk_prize_ident_warning.Size = New System.Drawing.Size(126, 17)
    Me.chk_prize_ident_warning.TabIndex = 1
    Me.chk_prize_ident_warning.Text = "xActivateWarning"
    Me.chk_prize_ident_warning.TextAlign = System.Drawing.ContentAlignment.TopCenter
    Me.chk_prize_ident_warning.UseVisualStyleBackColor = True
    '
    'chk_prize_maximum_limit
    '
    Me.chk_prize_maximum_limit.AutoSize = True
    Me.chk_prize_maximum_limit.Location = New System.Drawing.Point(18, 314)
    Me.chk_prize_maximum_limit.Name = "chk_prize_maximum_limit"
    Me.chk_prize_maximum_limit.Size = New System.Drawing.Size(160, 17)
    Me.chk_prize_maximum_limit.TabIndex = 6
    Me.chk_prize_maximum_limit.Text = "xActivatemaximumlimit"
    Me.chk_prize_maximum_limit.TextAlign = System.Drawing.ContentAlignment.TopCenter
    Me.chk_prize_maximum_limit.UseVisualStyleBackColor = True
    '
    'chk_prize_report_warning
    '
    Me.chk_prize_report_warning.AutoSize = True
    Me.chk_prize_report_warning.Location = New System.Drawing.Point(415, 144)
    Me.chk_prize_report_warning.Name = "chk_prize_report_warning"
    Me.chk_prize_report_warning.Size = New System.Drawing.Size(126, 17)
    Me.chk_prize_report_warning.TabIndex = 4
    Me.chk_prize_report_warning.Text = "xActivateWarning"
    Me.chk_prize_report_warning.TextAlign = System.Drawing.ContentAlignment.TopCenter
    Me.chk_prize_report_warning.UseVisualStyleBackColor = True
    '
    'gb_prize_maximum_limit
    '
    Me.gb_prize_maximum_limit.Controls.Add(Me.tb_prize_maximum_msg)
    Me.gb_prize_maximum_limit.Controls.Add(Me.lbl_prize_maximum_absolute)
    Me.gb_prize_maximum_limit.Controls.Add(Me.ef_prize_maximum_limit)
    Me.gb_prize_maximum_limit.Controls.Add(Me.lbl_prize_maxim_limit)
    Me.gb_prize_maximum_limit.Controls.Add(Me.lbl_prize_maximum_msg)
    Me.gb_prize_maximum_limit.Location = New System.Drawing.Point(6, 313)
    Me.gb_prize_maximum_limit.Name = "gb_prize_maximum_limit"
    Me.gb_prize_maximum_limit.Size = New System.Drawing.Size(390, 127)
    Me.gb_prize_maximum_limit.TabIndex = 7
    Me.gb_prize_maximum_limit.TabStop = False
    '
    'tb_prize_maximum_msg
    '
    Me.tb_prize_maximum_msg.AcceptsReturn = True
    Me.tb_prize_maximum_msg.Location = New System.Drawing.Point(122, 58)
    Me.tb_prize_maximum_msg.MaxLength = 256
    Me.tb_prize_maximum_msg.Multiline = True
    Me.tb_prize_maximum_msg.Name = "tb_prize_maximum_msg"
    Me.tb_prize_maximum_msg.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.tb_prize_maximum_msg.Size = New System.Drawing.Size(258, 50)
    Me.tb_prize_maximum_msg.TabIndex = 2
    '
    'lbl_prize_maximum_absolute
    '
    Me.lbl_prize_maximum_absolute.IsReadOnly = True
    Me.lbl_prize_maximum_absolute.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_prize_maximum_absolute.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_prize_maximum_absolute.Location = New System.Drawing.Point(228, 30)
    Me.lbl_prize_maximum_absolute.Name = "lbl_prize_maximum_absolute"
    Me.lbl_prize_maximum_absolute.Size = New System.Drawing.Size(151, 24)
    Me.lbl_prize_maximum_absolute.SufixText = "Sufix Text"
    Me.lbl_prize_maximum_absolute.SufixTextVisible = True
    Me.lbl_prize_maximum_absolute.TabIndex = 0
    Me.lbl_prize_maximum_absolute.TabStop = False
    Me.lbl_prize_maximum_absolute.TextVisible = False
    Me.lbl_prize_maximum_absolute.TextWidth = 0
    Me.lbl_prize_maximum_absolute.Value = "$0.00"
    '
    'ef_prize_maximum_limit
    '
    Me.ef_prize_maximum_limit.DoubleValue = 0
    Me.ef_prize_maximum_limit.IntegerValue = 0
    Me.ef_prize_maximum_limit.IsReadOnly = False
    Me.ef_prize_maximum_limit.Location = New System.Drawing.Point(120, 30)
    Me.ef_prize_maximum_limit.Name = "ef_prize_maximum_limit"
    Me.ef_prize_maximum_limit.Size = New System.Drawing.Size(101, 24)
    Me.ef_prize_maximum_limit.SufixText = "Sufix Text"
    Me.ef_prize_maximum_limit.SufixTextVisible = True
    Me.ef_prize_maximum_limit.TabIndex = 1
    Me.ef_prize_maximum_limit.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_prize_maximum_limit.TextValue = ""
    Me.ef_prize_maximum_limit.TextVisible = False
    Me.ef_prize_maximum_limit.TextWidth = 0
    Me.ef_prize_maximum_limit.Value = ""
    '
    'lbl_prize_maxim_limit
    '
    Me.lbl_prize_maxim_limit.Location = New System.Drawing.Point(17, 36)
    Me.lbl_prize_maxim_limit.Name = "lbl_prize_maxim_limit"
    Me.lbl_prize_maxim_limit.Size = New System.Drawing.Size(101, 13)
    Me.lbl_prize_maxim_limit.TabIndex = 0
    Me.lbl_prize_maxim_limit.Text = "xMaxLimit"
    Me.lbl_prize_maxim_limit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_prize_maximum_msg
    '
    Me.lbl_prize_maximum_msg.Location = New System.Drawing.Point(17, 63)
    Me.lbl_prize_maximum_msg.Name = "lbl_prize_maximum_msg"
    Me.lbl_prize_maximum_msg.Size = New System.Drawing.Size(101, 13)
    Me.lbl_prize_maximum_msg.TabIndex = 0
    Me.lbl_prize_maximum_msg.Text = "xMessage"
    Me.lbl_prize_maximum_msg.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'gb_prize_report_limit
    '
    Me.gb_prize_report_limit.Controls.Add(Me.tb_prize_report_msg)
    Me.gb_prize_report_limit.Controls.Add(Me.lbl_prize_exceed_report_limit)
    Me.gb_prize_report_limit.Controls.Add(Me.lbl_prize_report_absolute)
    Me.gb_prize_report_limit.Controls.Add(Me.cmb_exceed_prize)
    Me.gb_prize_report_limit.Controls.Add(Me.ef_prize_report_limit)
    Me.gb_prize_report_limit.Controls.Add(Me.chk_prize_report_limit_message)
    Me.gb_prize_report_limit.Controls.Add(Me.lbl_prize_report_limit)
    Me.gb_prize_report_limit.Controls.Add(Me.lbl_prize_report_msg)
    Me.gb_prize_report_limit.Location = New System.Drawing.Point(6, 146)
    Me.gb_prize_report_limit.Name = "gb_prize_report_limit"
    Me.gb_prize_report_limit.Size = New System.Drawing.Size(390, 164)
    Me.gb_prize_report_limit.TabIndex = 3
    Me.gb_prize_report_limit.TabStop = False
    '
    'tb_prize_report_msg
    '
    Me.tb_prize_report_msg.AcceptsReturn = True
    Me.tb_prize_report_msg.Location = New System.Drawing.Point(122, 55)
    Me.tb_prize_report_msg.MaxLength = 256
    Me.tb_prize_report_msg.Multiline = True
    Me.tb_prize_report_msg.Name = "tb_prize_report_msg"
    Me.tb_prize_report_msg.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.tb_prize_report_msg.Size = New System.Drawing.Size(258, 50)
    Me.tb_prize_report_msg.TabIndex = 2
    '
    'lbl_prize_exceed_report_limit
    '
    Me.lbl_prize_exceed_report_limit.Location = New System.Drawing.Point(2, 137)
    Me.lbl_prize_exceed_report_limit.Name = "lbl_prize_exceed_report_limit"
    Me.lbl_prize_exceed_report_limit.Size = New System.Drawing.Size(115, 18)
    Me.lbl_prize_exceed_report_limit.TabIndex = 0
    Me.lbl_prize_exceed_report_limit.Text = "xReportLimit"
    Me.lbl_prize_exceed_report_limit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_prize_report_absolute
    '
    Me.lbl_prize_report_absolute.IsReadOnly = True
    Me.lbl_prize_report_absolute.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_prize_report_absolute.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_prize_report_absolute.Location = New System.Drawing.Point(228, 26)
    Me.lbl_prize_report_absolute.Name = "lbl_prize_report_absolute"
    Me.lbl_prize_report_absolute.Size = New System.Drawing.Size(151, 24)
    Me.lbl_prize_report_absolute.SufixText = "Sufix Text"
    Me.lbl_prize_report_absolute.SufixTextVisible = True
    Me.lbl_prize_report_absolute.TabIndex = 0
    Me.lbl_prize_report_absolute.TabStop = False
    Me.lbl_prize_report_absolute.TextVisible = False
    Me.lbl_prize_report_absolute.TextWidth = 0
    Me.lbl_prize_report_absolute.Value = "$0.00"
    '
    'cmb_exceed_prize
    '
    Me.cmb_exceed_prize.AllowUnlistedValues = False
    Me.cmb_exceed_prize.AutoCompleteMode = False
    Me.cmb_exceed_prize.IsReadOnly = False
    Me.cmb_exceed_prize.Location = New System.Drawing.Point(120, 134)
    Me.cmb_exceed_prize.Name = "cmb_exceed_prize"
    Me.cmb_exceed_prize.SelectedIndex = -1
    Me.cmb_exceed_prize.Size = New System.Drawing.Size(136, 24)
    Me.cmb_exceed_prize.SufixText = "Sufix Text"
    Me.cmb_exceed_prize.SufixTextVisible = True
    Me.cmb_exceed_prize.TabIndex = 4
    Me.cmb_exceed_prize.TextVisible = False
    Me.cmb_exceed_prize.TextWidth = 0
    '
    'ef_prize_report_limit
    '
    Me.ef_prize_report_limit.DoubleValue = 0
    Me.ef_prize_report_limit.IntegerValue = 0
    Me.ef_prize_report_limit.IsReadOnly = False
    Me.ef_prize_report_limit.Location = New System.Drawing.Point(120, 27)
    Me.ef_prize_report_limit.Name = "ef_prize_report_limit"
    Me.ef_prize_report_limit.Size = New System.Drawing.Size(101, 24)
    Me.ef_prize_report_limit.SufixText = "Sufix Text"
    Me.ef_prize_report_limit.SufixTextVisible = True
    Me.ef_prize_report_limit.TabIndex = 1
    Me.ef_prize_report_limit.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_prize_report_limit.TextValue = ""
    Me.ef_prize_report_limit.TextVisible = False
    Me.ef_prize_report_limit.TextWidth = 0
    Me.ef_prize_report_limit.Value = ""
    '
    'chk_prize_report_limit_message
    '
    Me.chk_prize_report_limit_message.AutoSize = True
    Me.chk_prize_report_limit_message.Location = New System.Drawing.Point(120, 111)
    Me.chk_prize_report_limit_message.Name = "chk_prize_report_limit_message"
    Me.chk_prize_report_limit_message.Size = New System.Drawing.Size(178, 17)
    Me.chk_prize_report_limit_message.TabIndex = 3
    Me.chk_prize_report_limit_message.Text = "xShowReportLimitMessage"
    Me.chk_prize_report_limit_message.TextAlign = System.Drawing.ContentAlignment.TopCenter
    Me.chk_prize_report_limit_message.UseVisualStyleBackColor = True
    '
    'lbl_prize_report_limit
    '
    Me.lbl_prize_report_limit.Location = New System.Drawing.Point(17, 32)
    Me.lbl_prize_report_limit.Name = "lbl_prize_report_limit"
    Me.lbl_prize_report_limit.Size = New System.Drawing.Size(101, 13)
    Me.lbl_prize_report_limit.TabIndex = 0
    Me.lbl_prize_report_limit.Text = "xReportLimit"
    Me.lbl_prize_report_limit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_prize_report_msg
    '
    Me.lbl_prize_report_msg.Location = New System.Drawing.Point(17, 58)
    Me.lbl_prize_report_msg.Name = "lbl_prize_report_msg"
    Me.lbl_prize_report_msg.Size = New System.Drawing.Size(101, 13)
    Me.lbl_prize_report_msg.TabIndex = 0
    Me.lbl_prize_report_msg.Text = "xMessage"
    Me.lbl_prize_report_msg.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'gb_prize_ident_limit
    '
    Me.gb_prize_ident_limit.Controls.Add(Me.ef_prize_ident_limit)
    Me.gb_prize_ident_limit.Controls.Add(Me.lbl_prize_ident_absolute)
    Me.gb_prize_ident_limit.Controls.Add(Me.lbl_prize_ident_limit)
    Me.gb_prize_ident_limit.Controls.Add(Me.lbl_prize_ident_msg)
    Me.gb_prize_ident_limit.Controls.Add(Me.tb_prize_ident_msg)
    Me.gb_prize_ident_limit.Location = New System.Drawing.Point(6, 4)
    Me.gb_prize_ident_limit.Name = "gb_prize_ident_limit"
    Me.gb_prize_ident_limit.Size = New System.Drawing.Size(390, 138)
    Me.gb_prize_ident_limit.TabIndex = 0
    Me.gb_prize_ident_limit.TabStop = False
    '
    'ef_prize_ident_limit
    '
    Me.ef_prize_ident_limit.DoubleValue = 0
    Me.ef_prize_ident_limit.IntegerValue = 0
    Me.ef_prize_ident_limit.IsReadOnly = False
    Me.ef_prize_ident_limit.Location = New System.Drawing.Point(120, 30)
    Me.ef_prize_ident_limit.Name = "ef_prize_ident_limit"
    Me.ef_prize_ident_limit.Size = New System.Drawing.Size(101, 24)
    Me.ef_prize_ident_limit.SufixText = "Sufix Text"
    Me.ef_prize_ident_limit.SufixTextVisible = True
    Me.ef_prize_ident_limit.TabIndex = 1
    Me.ef_prize_ident_limit.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_prize_ident_limit.TextValue = ""
    Me.ef_prize_ident_limit.TextVisible = False
    Me.ef_prize_ident_limit.TextWidth = 0
    Me.ef_prize_ident_limit.Value = ""
    '
    'lbl_prize_ident_absolute
    '
    Me.lbl_prize_ident_absolute.IsReadOnly = True
    Me.lbl_prize_ident_absolute.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_prize_ident_absolute.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_prize_ident_absolute.Location = New System.Drawing.Point(227, 29)
    Me.lbl_prize_ident_absolute.Name = "lbl_prize_ident_absolute"
    Me.lbl_prize_ident_absolute.Size = New System.Drawing.Size(153, 24)
    Me.lbl_prize_ident_absolute.SufixText = "Sufix Text"
    Me.lbl_prize_ident_absolute.SufixTextVisible = True
    Me.lbl_prize_ident_absolute.TabIndex = 0
    Me.lbl_prize_ident_absolute.TabStop = False
    Me.lbl_prize_ident_absolute.TextVisible = False
    Me.lbl_prize_ident_absolute.TextWidth = 0
    Me.lbl_prize_ident_absolute.Value = "$0.00"
    '
    'lbl_prize_ident_limit
    '
    Me.lbl_prize_ident_limit.Location = New System.Drawing.Point(16, 36)
    Me.lbl_prize_ident_limit.Name = "lbl_prize_ident_limit"
    Me.lbl_prize_ident_limit.Size = New System.Drawing.Size(101, 13)
    Me.lbl_prize_ident_limit.TabIndex = 0
    Me.lbl_prize_ident_limit.Text = "xIdentifLimit"
    Me.lbl_prize_ident_limit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_prize_ident_msg
    '
    Me.lbl_prize_ident_msg.Location = New System.Drawing.Point(56, 65)
    Me.lbl_prize_ident_msg.Name = "lbl_prize_ident_msg"
    Me.lbl_prize_ident_msg.Size = New System.Drawing.Size(63, 13)
    Me.lbl_prize_ident_msg.TabIndex = 0
    Me.lbl_prize_ident_msg.Text = "xMessage"
    Me.lbl_prize_ident_msg.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'tb_prize_ident_msg
    '
    Me.tb_prize_ident_msg.AcceptsReturn = True
    Me.tb_prize_ident_msg.Location = New System.Drawing.Point(122, 60)
    Me.tb_prize_ident_msg.MaxLength = 256
    Me.tb_prize_ident_msg.Multiline = True
    Me.tb_prize_ident_msg.Name = "tb_prize_ident_msg"
    Me.tb_prize_ident_msg.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.tb_prize_ident_msg.Size = New System.Drawing.Size(258, 50)
    Me.tb_prize_ident_msg.TabIndex = 2
    '
    'gb_prize_report_warning
    '
    Me.gb_prize_report_warning.Controls.Add(Me.lbl_prize_report_warning_msg)
    Me.gb_prize_report_warning.Controls.Add(Me.ef_prize_report_warning_limit)
    Me.gb_prize_report_warning.Controls.Add(Me.tb_prize_report_warning_msg)
    Me.gb_prize_report_warning.Controls.Add(Me.lbl_prize_report_warning_absolute)
    Me.gb_prize_report_warning.Controls.Add(Me.lbl_prize_report_warning_limit)
    Me.gb_prize_report_warning.Location = New System.Drawing.Point(406, 146)
    Me.gb_prize_report_warning.Name = "gb_prize_report_warning"
    Me.gb_prize_report_warning.Size = New System.Drawing.Size(390, 164)
    Me.gb_prize_report_warning.TabIndex = 5
    Me.gb_prize_report_warning.TabStop = False
    '
    'lbl_prize_report_warning_msg
    '
    Me.lbl_prize_report_warning_msg.Location = New System.Drawing.Point(55, 60)
    Me.lbl_prize_report_warning_msg.Name = "lbl_prize_report_warning_msg"
    Me.lbl_prize_report_warning_msg.Size = New System.Drawing.Size(63, 13)
    Me.lbl_prize_report_warning_msg.TabIndex = 0
    Me.lbl_prize_report_warning_msg.Text = "xMessage"
    Me.lbl_prize_report_warning_msg.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'ef_prize_report_warning_limit
    '
    Me.ef_prize_report_warning_limit.DoubleValue = 0
    Me.ef_prize_report_warning_limit.IntegerValue = 0
    Me.ef_prize_report_warning_limit.IsReadOnly = False
    Me.ef_prize_report_warning_limit.Location = New System.Drawing.Point(120, 26)
    Me.ef_prize_report_warning_limit.Name = "ef_prize_report_warning_limit"
    Me.ef_prize_report_warning_limit.Size = New System.Drawing.Size(101, 24)
    Me.ef_prize_report_warning_limit.SufixText = "Sufix Text"
    Me.ef_prize_report_warning_limit.SufixTextVisible = True
    Me.ef_prize_report_warning_limit.TabIndex = 1
    Me.ef_prize_report_warning_limit.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_prize_report_warning_limit.TextValue = ""
    Me.ef_prize_report_warning_limit.TextVisible = False
    Me.ef_prize_report_warning_limit.TextWidth = 0
    Me.ef_prize_report_warning_limit.Value = ""
    '
    'tb_prize_report_warning_msg
    '
    Me.tb_prize_report_warning_msg.AcceptsReturn = True
    Me.tb_prize_report_warning_msg.Location = New System.Drawing.Point(122, 55)
    Me.tb_prize_report_warning_msg.MaxLength = 256
    Me.tb_prize_report_warning_msg.Multiline = True
    Me.tb_prize_report_warning_msg.Name = "tb_prize_report_warning_msg"
    Me.tb_prize_report_warning_msg.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.tb_prize_report_warning_msg.Size = New System.Drawing.Size(258, 50)
    Me.tb_prize_report_warning_msg.TabIndex = 2
    '
    'lbl_prize_report_warning_absolute
    '
    Me.lbl_prize_report_warning_absolute.IsReadOnly = True
    Me.lbl_prize_report_warning_absolute.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_prize_report_warning_absolute.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_prize_report_warning_absolute.Location = New System.Drawing.Point(233, 26)
    Me.lbl_prize_report_warning_absolute.Name = "lbl_prize_report_warning_absolute"
    Me.lbl_prize_report_warning_absolute.Size = New System.Drawing.Size(147, 24)
    Me.lbl_prize_report_warning_absolute.SufixText = "Sufix Text"
    Me.lbl_prize_report_warning_absolute.SufixTextVisible = True
    Me.lbl_prize_report_warning_absolute.TabIndex = 0
    Me.lbl_prize_report_warning_absolute.TabStop = False
    Me.lbl_prize_report_warning_absolute.TextVisible = False
    Me.lbl_prize_report_warning_absolute.TextWidth = 0
    Me.lbl_prize_report_warning_absolute.Value = "$0.00"
    '
    'lbl_prize_report_warning_limit
    '
    Me.lbl_prize_report_warning_limit.Location = New System.Drawing.Point(17, 32)
    Me.lbl_prize_report_warning_limit.Name = "lbl_prize_report_warning_limit"
    Me.lbl_prize_report_warning_limit.Size = New System.Drawing.Size(101, 13)
    Me.lbl_prize_report_warning_limit.TabIndex = 0
    Me.lbl_prize_report_warning_limit.Text = "xWarningLimit"
    Me.lbl_prize_report_warning_limit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'gb_prize_ident_warning
    '
    Me.gb_prize_ident_warning.Controls.Add(Me.lbl_prize_ident_warning_msg)
    Me.gb_prize_ident_warning.Controls.Add(Me.ef_prize_ident_warning_limit)
    Me.gb_prize_ident_warning.Controls.Add(Me.tb_prize_ident_warning_msg)
    Me.gb_prize_ident_warning.Controls.Add(Me.lbl_prize_ident_warning_absolute)
    Me.gb_prize_ident_warning.Controls.Add(Me.lbl_prize_ident_warning_limit)
    Me.gb_prize_ident_warning.Location = New System.Drawing.Point(406, 4)
    Me.gb_prize_ident_warning.Name = "gb_prize_ident_warning"
    Me.gb_prize_ident_warning.Size = New System.Drawing.Size(390, 138)
    Me.gb_prize_ident_warning.TabIndex = 2
    Me.gb_prize_ident_warning.TabStop = False
    '
    'lbl_prize_ident_warning_msg
    '
    Me.lbl_prize_ident_warning_msg.Location = New System.Drawing.Point(54, 65)
    Me.lbl_prize_ident_warning_msg.Name = "lbl_prize_ident_warning_msg"
    Me.lbl_prize_ident_warning_msg.Size = New System.Drawing.Size(63, 13)
    Me.lbl_prize_ident_warning_msg.TabIndex = 0
    Me.lbl_prize_ident_warning_msg.Text = "xMessage"
    Me.lbl_prize_ident_warning_msg.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'ef_prize_ident_warning_limit
    '
    Me.ef_prize_ident_warning_limit.DoubleValue = 0
    Me.ef_prize_ident_warning_limit.IntegerValue = 0
    Me.ef_prize_ident_warning_limit.IsReadOnly = False
    Me.ef_prize_ident_warning_limit.Location = New System.Drawing.Point(120, 30)
    Me.ef_prize_ident_warning_limit.Name = "ef_prize_ident_warning_limit"
    Me.ef_prize_ident_warning_limit.Size = New System.Drawing.Size(101, 24)
    Me.ef_prize_ident_warning_limit.SufixText = "Sufix Text"
    Me.ef_prize_ident_warning_limit.SufixTextVisible = True
    Me.ef_prize_ident_warning_limit.TabIndex = 1
    Me.ef_prize_ident_warning_limit.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_prize_ident_warning_limit.TextValue = ""
    Me.ef_prize_ident_warning_limit.TextVisible = False
    Me.ef_prize_ident_warning_limit.TextWidth = 0
    Me.ef_prize_ident_warning_limit.Value = ""
    '
    'tb_prize_ident_warning_msg
    '
    Me.tb_prize_ident_warning_msg.AcceptsReturn = True
    Me.tb_prize_ident_warning_msg.Location = New System.Drawing.Point(122, 60)
    Me.tb_prize_ident_warning_msg.MaxLength = 256
    Me.tb_prize_ident_warning_msg.Multiline = True
    Me.tb_prize_ident_warning_msg.Name = "tb_prize_ident_warning_msg"
    Me.tb_prize_ident_warning_msg.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.tb_prize_ident_warning_msg.Size = New System.Drawing.Size(258, 50)
    Me.tb_prize_ident_warning_msg.TabIndex = 2
    '
    'lbl_prize_ident_warning_absolute
    '
    Me.lbl_prize_ident_warning_absolute.IsReadOnly = True
    Me.lbl_prize_ident_warning_absolute.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_prize_ident_warning_absolute.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_prize_ident_warning_absolute.Location = New System.Drawing.Point(230, 30)
    Me.lbl_prize_ident_warning_absolute.Name = "lbl_prize_ident_warning_absolute"
    Me.lbl_prize_ident_warning_absolute.Size = New System.Drawing.Size(151, 24)
    Me.lbl_prize_ident_warning_absolute.SufixText = "Sufix Text"
    Me.lbl_prize_ident_warning_absolute.SufixTextVisible = True
    Me.lbl_prize_ident_warning_absolute.TabIndex = 0
    Me.lbl_prize_ident_warning_absolute.TabStop = False
    Me.lbl_prize_ident_warning_absolute.TextVisible = False
    Me.lbl_prize_ident_warning_absolute.TextWidth = 0
    Me.lbl_prize_ident_warning_absolute.Value = "$0.00"
    '
    'lbl_prize_ident_warning_limit
    '
    Me.lbl_prize_ident_warning_limit.Location = New System.Drawing.Point(16, 36)
    Me.lbl_prize_ident_warning_limit.Name = "lbl_prize_ident_warning_limit"
    Me.lbl_prize_ident_warning_limit.Size = New System.Drawing.Size(101, 13)
    Me.lbl_prize_ident_warning_limit.TabIndex = 0
    Me.lbl_prize_ident_warning_limit.Text = "xWarningLimit"
    Me.lbl_prize_ident_warning_limit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'ef_minimum_wage
    '
    Me.ef_minimum_wage.DoubleValue = 0
    Me.ef_minimum_wage.ForeColor = System.Drawing.SystemColors.ControlText
    Me.ef_minimum_wage.IntegerValue = 0
    Me.ef_minimum_wage.IsReadOnly = False
    Me.ef_minimum_wage.Location = New System.Drawing.Point(3, 45)
    Me.ef_minimum_wage.Name = "ef_minimum_wage"
    Me.ef_minimum_wage.Size = New System.Drawing.Size(183, 24)
    Me.ef_minimum_wage.SufixText = "Sufix Text"
    Me.ef_minimum_wage.SufixTextVisible = True
    Me.ef_minimum_wage.TabIndex = 0
    Me.ef_minimum_wage.Tag = "4"
    Me.ef_minimum_wage.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_minimum_wage.TextValue = ""
    Me.ef_minimum_wage.TextVisible = False
    Me.ef_minimum_wage.TextWidth = 100
    Me.ef_minimum_wage.Value = ""
    '
    'lbl_info
    '
    Me.lbl_info.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.lbl_info.ForeColor = System.Drawing.Color.Navy
    Me.lbl_info.Location = New System.Drawing.Point(317, 57)
    Me.lbl_info.Name = "lbl_info"
    Me.lbl_info.RightToLeft = System.Windows.Forms.RightToLeft.Yes
    Me.lbl_info.Size = New System.Drawing.Size(515, 19)
    Me.lbl_info.TabIndex = 0
    Me.lbl_info.Text = "xMasterProfiles"
    Me.lbl_info.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.lbl_info.Visible = False
    '
    'ef_aml_name
    '
    Me.ef_aml_name.DoubleValue = 0
    Me.ef_aml_name.IntegerValue = 0
    Me.ef_aml_name.IsReadOnly = False
    Me.ef_aml_name.Location = New System.Drawing.Point(25, 26)
    Me.ef_aml_name.Name = "ef_aml_name"
    Me.ef_aml_name.Size = New System.Drawing.Size(256, 24)
    Me.ef_aml_name.SufixText = "Sufix Text"
    Me.ef_aml_name.SufixTextVisible = True
    Me.ef_aml_name.TabIndex = 1
    Me.ef_aml_name.Tag = "2"
    Me.ef_aml_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_aml_name.TextValue = ""
    Me.ef_aml_name.TextWidth = 50
    Me.ef_aml_name.Value = ""
    '
    'chk_active_aml
    '
    Me.chk_active_aml.AutoSize = True
    Me.chk_active_aml.Location = New System.Drawing.Point(11, 3)
    Me.chk_active_aml.Name = "chk_active_aml"
    Me.chk_active_aml.Size = New System.Drawing.Size(190, 17)
    Me.chk_active_aml.TabIndex = 0
    Me.chk_active_aml.Tag = "1"
    Me.chk_active_aml.Text = "xActiveAntiMoneyLaundering"
    Me.chk_active_aml.UseVisualStyleBackColor = True
    '
    'gb_limit
    '
    Me.gb_limit.Controls.Add(Me.lbl_minwage_desc)
    Me.gb_limit.Controls.Add(Me.tc_limits)
    Me.gb_limit.Controls.Add(Me.ef_minimum_wage)
    Me.gb_limit.Location = New System.Drawing.Point(7, 80)
    Me.gb_limit.Name = "gb_limit"
    Me.gb_limit.Size = New System.Drawing.Size(822, 549)
    Me.gb_limit.TabIndex = 3
    Me.gb_limit.TabStop = False
    Me.gb_limit.Text = "xLimit"
    '
    'chk_has_beneficiary
    '
    Me.chk_has_beneficiary.AutoSize = True
    Me.chk_has_beneficiary.Location = New System.Drawing.Point(11, 56)
    Me.chk_has_beneficiary.Name = "chk_has_beneficiary"
    Me.chk_has_beneficiary.Size = New System.Drawing.Size(118, 17)
    Me.chk_has_beneficiary.TabIndex = 2
    Me.chk_has_beneficiary.Tag = "3"
    Me.chk_has_beneficiary.Text = "xHasBeneficiary"
    Me.chk_has_beneficiary.TextAlign = System.Drawing.ContentAlignment.TopCenter
    Me.chk_has_beneficiary.UseVisualStyleBackColor = True
    '
    'frm_money_laundering_config
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(944, 647)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_money_laundering_config"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_account_fields_configuration"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.tc_limits.ResumeLayout(False)
    Me.tp_recharge.ResumeLayout(False)
    Me.tp_recharge.PerformLayout()
    Me.gb_rchg_maximum_limit.ResumeLayout(False)
    Me.gb_rchg_maximum_limit.PerformLayout()
    Me.gb_rchg_report_limit.ResumeLayout(False)
    Me.gb_rchg_report_limit.PerformLayout()
    Me.gb_rchg_ident_limit.ResumeLayout(False)
    Me.gb_rchg_ident_limit.PerformLayout()
    Me.gb_rchg_report_warning.ResumeLayout(False)
    Me.gb_rchg_report_warning.PerformLayout()
    Me.gb_rchg_ident_warning.ResumeLayout(False)
    Me.gb_rchg_ident_warning.PerformLayout()
    Me.tp_prize.ResumeLayout(False)
    Me.tp_prize.PerformLayout()
    Me.gb_prize_maximum_limit.ResumeLayout(False)
    Me.gb_prize_maximum_limit.PerformLayout()
    Me.gb_prize_report_limit.ResumeLayout(False)
    Me.gb_prize_report_limit.PerformLayout()
    Me.gb_prize_ident_limit.ResumeLayout(False)
    Me.gb_prize_ident_limit.PerformLayout()
    Me.gb_prize_report_warning.ResumeLayout(False)
    Me.gb_prize_report_warning.PerformLayout()
    Me.gb_prize_ident_warning.ResumeLayout(False)
    Me.gb_prize_ident_warning.PerformLayout()
    Me.gb_limit.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents lbl_info As System.Windows.Forms.Label
  Friend WithEvents ef_aml_name As GUI_Controls.uc_entry_field
  Friend WithEvents chk_active_aml As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_minwage_desc As GUI_Controls.uc_text_field
  Friend WithEvents tc_limits As System.Windows.Forms.TabControl
  Friend WithEvents tp_recharge As System.Windows.Forms.TabPage
  Friend WithEvents gb_rchg_maximum_limit As System.Windows.Forms.GroupBox
  Friend WithEvents tb_rchg_maximum_msg As System.Windows.Forms.TextBox
  Friend WithEvents lbl_rchg_maximum_absolute As GUI_Controls.uc_text_field
  Friend WithEvents ef_rchg_maximum_limit As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_rchg_maximum_limit As System.Windows.Forms.Label
  Friend WithEvents lbl_rchg_maximum_msg As System.Windows.Forms.Label
  Friend WithEvents gb_rchg_report_limit As System.Windows.Forms.GroupBox
  Friend WithEvents tb_rchg_report_msg As System.Windows.Forms.TextBox
  Friend WithEvents lbl_rchg_exceed_report_limit As System.Windows.Forms.Label
  Friend WithEvents lbl_rchg_report_absolute As GUI_Controls.uc_text_field
  Friend WithEvents cmb_exceed_recharge As GUI_Controls.uc_combo
  Friend WithEvents ef_rchg_report_limit As GUI_Controls.uc_entry_field
  Friend WithEvents chk_rchg_report_limit_message As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_rchg_report_limit As System.Windows.Forms.Label
  Friend WithEvents lbl_rchg_report_msg As System.Windows.Forms.Label
  Friend WithEvents gb_rchg_ident_limit As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_rchg_ident_absolute As GUI_Controls.uc_text_field
  Friend WithEvents tb_rchg_ident_msg As System.Windows.Forms.TextBox
  Friend WithEvents lbl_rchg_ident_msg As System.Windows.Forms.Label
  Friend WithEvents ef_rchg_ident_limit As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_rchg_ident_limit As System.Windows.Forms.Label
  Friend WithEvents chk_rchg_report_warning As System.Windows.Forms.CheckBox
  Friend WithEvents chk_rchg_ident_warning As System.Windows.Forms.CheckBox
  Friend WithEvents gb_rchg_report_warning As System.Windows.Forms.GroupBox
  Friend WithEvents chk_report_not_allow_mb As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_rchg_report_warning_msg As System.Windows.Forms.Label
  Friend WithEvents ef_rchg_report_warning_limit As GUI_Controls.uc_entry_field
  Friend WithEvents tb_rchg_report_warning_msg As System.Windows.Forms.TextBox
  Friend WithEvents lbl_rchg_report_warning_absolute As GUI_Controls.uc_text_field
  Friend WithEvents lbl_rchg_report_warning_limit As System.Windows.Forms.Label
  Friend WithEvents gb_rchg_ident_warning As System.Windows.Forms.GroupBox
  Friend WithEvents chk_ident_not_allow_mb As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_rchg_ident_warning_msg As System.Windows.Forms.Label
  Friend WithEvents ef_rchg_ident_warning_limit As GUI_Controls.uc_entry_field
  Friend WithEvents tb_rchg_ident_warning_msg As System.Windows.Forms.TextBox
  Friend WithEvents lbl_rchg_ident_warning_absolute As GUI_Controls.uc_text_field
  Friend WithEvents lbl_rchg_ident_warning_limit As System.Windows.Forms.Label
  Friend WithEvents tp_prize As System.Windows.Forms.TabPage
  Friend WithEvents gb_prize_maximum_limit As System.Windows.Forms.GroupBox
  Friend WithEvents tb_prize_maximum_msg As System.Windows.Forms.TextBox
  Friend WithEvents lbl_prize_maximum_absolute As GUI_Controls.uc_text_field
  Friend WithEvents ef_prize_maximum_limit As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_prize_maxim_limit As System.Windows.Forms.Label
  Friend WithEvents lbl_prize_maximum_msg As System.Windows.Forms.Label
  Friend WithEvents gb_prize_report_limit As System.Windows.Forms.GroupBox
  Friend WithEvents tb_prize_report_msg As System.Windows.Forms.TextBox
  Friend WithEvents lbl_prize_exceed_report_limit As System.Windows.Forms.Label
  Friend WithEvents lbl_prize_report_absolute As GUI_Controls.uc_text_field
  Friend WithEvents cmb_exceed_prize As GUI_Controls.uc_combo
  Friend WithEvents ef_prize_report_limit As GUI_Controls.uc_entry_field
  Friend WithEvents chk_prize_report_limit_message As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_prize_report_limit As System.Windows.Forms.Label
  Friend WithEvents lbl_prize_report_msg As System.Windows.Forms.Label
  Friend WithEvents gb_prize_ident_limit As System.Windows.Forms.GroupBox
  Friend WithEvents ef_prize_ident_limit As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_prize_ident_absolute As GUI_Controls.uc_text_field
  Friend WithEvents lbl_prize_ident_limit As System.Windows.Forms.Label
  Friend WithEvents lbl_prize_ident_msg As System.Windows.Forms.Label
  Friend WithEvents tb_prize_ident_msg As System.Windows.Forms.TextBox
  Friend WithEvents gb_prize_report_warning As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_prize_report_warning_msg As System.Windows.Forms.Label
  Friend WithEvents ef_prize_report_warning_limit As GUI_Controls.uc_entry_field
  Friend WithEvents tb_prize_report_warning_msg As System.Windows.Forms.TextBox
  Friend WithEvents lbl_prize_report_warning_absolute As GUI_Controls.uc_text_field
  Friend WithEvents lbl_prize_report_warning_limit As System.Windows.Forms.Label
  Friend WithEvents gb_prize_ident_warning As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_prize_ident_warning_msg As System.Windows.Forms.Label
  Friend WithEvents ef_prize_ident_warning_limit As GUI_Controls.uc_entry_field
  Friend WithEvents tb_prize_ident_warning_msg As System.Windows.Forms.TextBox
  Friend WithEvents lbl_prize_ident_warning_absolute As GUI_Controls.uc_text_field
  Friend WithEvents lbl_prize_ident_warning_limit As System.Windows.Forms.Label
  Friend WithEvents ef_minimum_wage As GUI_Controls.uc_entry_field
  Friend WithEvents chk_prize_report_warning As System.Windows.Forms.CheckBox
  Friend WithEvents chk_rchg_maximum_limit As System.Windows.Forms.CheckBox
  Friend WithEvents chk_prize_maximum_limit As System.Windows.Forms.CheckBox
  Friend WithEvents chk_prize_ident_warning As System.Windows.Forms.CheckBox
  Friend WithEvents gb_limit As System.Windows.Forms.GroupBox
  Friend WithEvents chk_has_beneficiary As System.Windows.Forms.CheckBox
End Class
