<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_cage_safe_keeping_edit
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.ef_name = New GUI_Controls.uc_entry_field()
    Me.ef_middle_name = New GUI_Controls.uc_entry_field()
    Me.ef_surname_2 = New GUI_Controls.uc_entry_field()
    Me.ef_surname_1 = New GUI_Controls.uc_entry_field()
    Me.cmb_document_type = New GUI_Controls.uc_combo()
    Me.ef_document_id = New GUI_Controls.uc_entry_field()
    Me.ef_balance = New GUI_Controls.uc_entry_field()
    Me.chk_block_reason = New System.Windows.Forms.CheckBox()
    Me.chk_max_balance = New System.Windows.Forms.CheckBox()
    Me.ef_max_balance = New GUI_Controls.uc_entry_field()
    Me.lbl_blocked = New System.Windows.Forms.Label()
    Me.panel_data.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.ef_name)
    Me.panel_data.Controls.Add(Me.chk_max_balance)
    Me.panel_data.Controls.Add(Me.ef_balance)
    Me.panel_data.Controls.Add(Me.ef_surname_1)
    Me.panel_data.Controls.Add(Me.ef_max_balance)
    Me.panel_data.Controls.Add(Me.chk_block_reason)
    Me.panel_data.Controls.Add(Me.ef_middle_name)
    Me.panel_data.Controls.Add(Me.lbl_blocked)
    Me.panel_data.Controls.Add(Me.ef_surname_2)
    Me.panel_data.Controls.Add(Me.cmb_document_type)
    Me.panel_data.Controls.Add(Me.ef_document_id)
    Me.panel_data.Size = New System.Drawing.Size(473, 320)
    '
    'ef_name
    '
    Me.ef_name.DoubleValue = 0.0R
    Me.ef_name.IntegerValue = 0
    Me.ef_name.IsReadOnly = False
    Me.ef_name.Location = New System.Drawing.Point(12, 12)
    Me.ef_name.Name = "ef_name"
    Me.ef_name.OnlyUpperCase = True
    Me.ef_name.PlaceHolder = Nothing
    Me.ef_name.Size = New System.Drawing.Size(450, 24)
    Me.ef_name.SufixText = "Sufix Text"
    Me.ef_name.SufixTextVisible = True
    Me.ef_name.TabIndex = 0
    Me.ef_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_name.TextValue = ""
    Me.ef_name.TextWidth = 120
    Me.ef_name.Value = ""
    Me.ef_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_middle_name
    '
    Me.ef_middle_name.DoubleValue = 0.0R
    Me.ef_middle_name.IntegerValue = 0
    Me.ef_middle_name.IsReadOnly = False
    Me.ef_middle_name.Location = New System.Drawing.Point(12, 42)
    Me.ef_middle_name.Name = "ef_middle_name"
    Me.ef_middle_name.OnlyUpperCase = True
    Me.ef_middle_name.PlaceHolder = Nothing
    Me.ef_middle_name.Size = New System.Drawing.Size(450, 24)
    Me.ef_middle_name.SufixText = "Sufix Text"
    Me.ef_middle_name.SufixTextVisible = True
    Me.ef_middle_name.TabIndex = 1
    Me.ef_middle_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_middle_name.TextValue = ""
    Me.ef_middle_name.TextWidth = 120
    Me.ef_middle_name.Value = ""
    Me.ef_middle_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_surname_2
    '
    Me.ef_surname_2.DoubleValue = 0.0R
    Me.ef_surname_2.IntegerValue = 0
    Me.ef_surname_2.IsReadOnly = False
    Me.ef_surname_2.Location = New System.Drawing.Point(12, 102)
    Me.ef_surname_2.Name = "ef_surname_2"
    Me.ef_surname_2.OnlyUpperCase = True
    Me.ef_surname_2.PlaceHolder = Nothing
    Me.ef_surname_2.Size = New System.Drawing.Size(450, 24)
    Me.ef_surname_2.SufixText = "Sufix Text"
    Me.ef_surname_2.SufixTextVisible = True
    Me.ef_surname_2.TabIndex = 3
    Me.ef_surname_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_surname_2.TextValue = ""
    Me.ef_surname_2.TextWidth = 120
    Me.ef_surname_2.Value = ""
    Me.ef_surname_2.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_surname_1
    '
    Me.ef_surname_1.DoubleValue = 0.0R
    Me.ef_surname_1.IntegerValue = 0
    Me.ef_surname_1.IsReadOnly = False
    Me.ef_surname_1.Location = New System.Drawing.Point(12, 72)
    Me.ef_surname_1.Name = "ef_surname_1"
    Me.ef_surname_1.OnlyUpperCase = True
    Me.ef_surname_1.PlaceHolder = Nothing
    Me.ef_surname_1.Size = New System.Drawing.Size(450, 24)
    Me.ef_surname_1.SufixText = "Sufix Text"
    Me.ef_surname_1.SufixTextVisible = True
    Me.ef_surname_1.TabIndex = 2
    Me.ef_surname_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_surname_1.TextValue = ""
    Me.ef_surname_1.TextWidth = 120
    Me.ef_surname_1.Value = ""
    Me.ef_surname_1.ValueForeColor = System.Drawing.Color.Blue
    '
    'cmb_document_type
    '
    Me.cmb_document_type.AllowUnlistedValues = False
    Me.cmb_document_type.AutoCompleteMode = False
    Me.cmb_document_type.IsReadOnly = False
    Me.cmb_document_type.Location = New System.Drawing.Point(11, 132)
    Me.cmb_document_type.Name = "cmb_document_type"
    Me.cmb_document_type.SelectedIndex = -1
    Me.cmb_document_type.Size = New System.Drawing.Size(300, 24)
    Me.cmb_document_type.SufixText = "Sufix Text"
    Me.cmb_document_type.SufixTextVisible = True
    Me.cmb_document_type.TabIndex = 4
    Me.cmb_document_type.TextWidth = 120
    '
    'ef_document_id
    '
    Me.ef_document_id.DoubleValue = 0.0R
    Me.ef_document_id.IntegerValue = 0
    Me.ef_document_id.IsReadOnly = False
    Me.ef_document_id.Location = New System.Drawing.Point(12, 162)
    Me.ef_document_id.Name = "ef_document_id"
    Me.ef_document_id.PlaceHolder = Nothing
    Me.ef_document_id.Size = New System.Drawing.Size(299, 25)
    Me.ef_document_id.SufixText = "Sufix Text"
    Me.ef_document_id.SufixTextVisible = True
    Me.ef_document_id.TabIndex = 5
    Me.ef_document_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_document_id.TextValue = ""
    Me.ef_document_id.TextWidth = 120
    Me.ef_document_id.Value = ""
    Me.ef_document_id.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_balance
    '
    Me.ef_balance.DoubleValue = 0.0R
    Me.ef_balance.IntegerValue = 0
    Me.ef_balance.IsReadOnly = False
    Me.ef_balance.Location = New System.Drawing.Point(12, 191)
    Me.ef_balance.Name = "ef_balance"
    Me.ef_balance.PlaceHolder = Nothing
    Me.ef_balance.Size = New System.Drawing.Size(299, 25)
    Me.ef_balance.SufixText = "Sufix Text"
    Me.ef_balance.SufixTextVisible = True
    Me.ef_balance.TabIndex = 6
    Me.ef_balance.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_balance.TextValue = ""
    Me.ef_balance.TextWidth = 120
    Me.ef_balance.Value = ""
    Me.ef_balance.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_block_reason
    '
    Me.chk_block_reason.AutoSize = True
    Me.chk_block_reason.Location = New System.Drawing.Point(12, 247)
    Me.chk_block_reason.Name = "chk_block_reason"
    Me.chk_block_reason.Size = New System.Drawing.Size(79, 17)
    Me.chk_block_reason.TabIndex = 9
    Me.chk_block_reason.Text = "xBloqued"
    Me.chk_block_reason.UseVisualStyleBackColor = True
    '
    'chk_max_balance
    '
    Me.chk_max_balance.AutoSize = True
    Me.chk_max_balance.Location = New System.Drawing.Point(12, 222)
    Me.chk_max_balance.Name = "chk_max_balance"
    Me.chk_max_balance.Size = New System.Drawing.Size(109, 17)
    Me.chk_max_balance.TabIndex = 7
    Me.chk_max_balance.Text = "xMax. Balance"
    Me.chk_max_balance.UseVisualStyleBackColor = True
    '
    'ef_max_balance
    '
    Me.ef_max_balance.DoubleValue = 0.0R
    Me.ef_max_balance.IntegerValue = 0
    Me.ef_max_balance.IsReadOnly = False
    Me.ef_max_balance.Location = New System.Drawing.Point(132, 220)
    Me.ef_max_balance.Name = "ef_max_balance"
    Me.ef_max_balance.PlaceHolder = Nothing
    Me.ef_max_balance.Size = New System.Drawing.Size(179, 25)
    Me.ef_max_balance.SufixText = "Sufix Text"
    Me.ef_max_balance.SufixTextVisible = True
    Me.ef_max_balance.TabIndex = 8
    Me.ef_max_balance.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_max_balance.TextValue = ""
    Me.ef_max_balance.TextWidth = 0
    Me.ef_max_balance.Value = ""
    Me.ef_max_balance.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_blocked
    '
    Me.lbl_blocked.BackColor = System.Drawing.Color.Red
    Me.lbl_blocked.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_blocked.ForeColor = System.Drawing.Color.White
    Me.lbl_blocked.Location = New System.Drawing.Point(143, 277)
    Me.lbl_blocked.Name = "lbl_blocked"
    Me.lbl_blocked.Size = New System.Drawing.Size(190, 23)
    Me.lbl_blocked.TabIndex = 13
    Me.lbl_blocked.Text = "BLOQUEADO"
    Me.lbl_blocked.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'frm_cage_safe_keeping_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(575, 329)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_cage_safe_keeping_edit"
    Me.Text = "frm_cage_safe_keeping_edit"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents cmb_document_type As GUI_Controls.uc_combo
  Friend WithEvents chk_block_reason As System.Windows.Forms.CheckBox
  Friend WithEvents chk_max_balance As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_blocked As System.Windows.Forms.Label
  Friend WithEvents ef_surname_2 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_surname_1 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_middle_name As GUI_Controls.uc_entry_field
  Friend WithEvents ef_name As GUI_Controls.uc_entry_field
  Friend WithEvents ef_document_id As GUI_Controls.uc_entry_field
  Friend WithEvents ef_balance As GUI_Controls.uc_entry_field
  Friend WithEvents ef_max_balance As GUI_Controls.uc_entry_field
End Class
