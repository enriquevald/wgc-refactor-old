'-------------------------------------------------------------------
' Copyright � 2007-2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_play_sessions_to_revise
' DESCRIPTION:   Windows form for revise points of play sessions
' AUTHOR:        Alberto Marcos
' CREATION DATE: 04-OCT-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 04-OCT-2013  AMF    Initial version
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports WSI.Common
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data


Public Class frm_play_sessions_to_revise
  Inherits GUI_Controls.frm_base_edit

#Region " Constant "

  Private Const GRID_COLUMNS_ACCOUNTS As Integer = 5
  Private Const GRID_HEADER_ROWS_ACCOUNTS As Integer = 1
  Private Const GRID_COLUMNS_SESSIONS As Integer = 7
  Private Const GRID_HEADER_ROWS_SESSIONS As Integer = 2

  Private Const GRID_COLUMN_ACCT_NUMBER As Integer = 0
  Private Const GRID_COLUMN_HOLDER_NAME As Integer = 1
  Private Const GRID_COLUMN_COMPUTED_POINTS_ACCT As Integer = 2
  Private Const GRID_COLUMN_AWARDED_POINTS_ACCT As Integer = 3
  Private Const GRID_COLUMN_TOTAL_PLAY_SESSIONS As Integer = 4

  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_PROVIDER_NAME As Integer = 1
  Private Const GRID_COLUMN_TERMINAL_NAME As Integer = 2
  Private Const GRID_COLUMN_AWARDED_POINTS_STATUS As Integer = 3
  Private Const GRID_COLUMN_COMPUTED_POINTS As Integer = 4
  Private Const GRID_COLUMN_AWARDED_POINTS As Integer = 5
  Private Const GRID_COLUMN_PLAY_SESSION_ID As Integer = 6

  Private Const SQL_COLUMN_PLAY_SESSION_ID As Integer = 0
  Private Const SQL_COLUMN_ACCOUNT_ID As Integer = 1
  Private Const SQL_COLUMN_PROVIDER_NAME As Integer = 2
  Private Const SQL_COLUMN_TERMINAL_NAME As Integer = 3
  Private Const SQL_COLUMN_COMPUTED_POINTS As Integer = 4
  Private Const SQL_COLUMN_AWARDED_POINTS As Integer = 5
  Private Const SQL_COLUMN_AWARDED_POINTS_STATUS As Integer = 6

  Private Const SQL_COLUMN_ACCT_NUMBER As Integer = 0
  Private Const SQL_COLUMN_HOLDER_NAME As Integer = 1
  Private Const SQL_COLUMN_COMPUTED_POINTS_ACCT As Integer = 2
  Private Const SQL_COLUMN_AWARDED_POINTS_ACCT As Integer = 3
  Private Const SQL_COLUMN_TOTAL_PLAY_SESSIONS As Integer = 4

#End Region

#Region " Members "
  Private m_point_awarded_changed As Double

#End Region

#Region " Overrides "

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_PLAY_SESSIONS_TO_REVISE

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Check for screen data.
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  '
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Return True

  End Function ' GUI_IsScreenDataOk

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_GetScreenData()
    Dim _play As CLASS_PLAY_SESSIONS_TO_REVISE
    Dim _idx_row As Integer
    Dim _idx_row_dt As Integer
    Dim _status As Integer

    _play = DbEditedObject
    _idx_row_dt = 0

    For _idx_row = 0 To dg_list_play_sessions.NumRows() - 1

      ' Play Session Selected
      While dg_list_play_sessions.Cell(_idx_row, GRID_COLUMN_PLAY_SESSION_ID).Value <> _
            _play.DtPlaySessions.Rows(_idx_row_dt).Item(SQL_COLUMN_PLAY_SESSION_ID)
        _idx_row_dt += 1
      End While

      ' Evaluate modified rows
      If dg_list_play_sessions.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS).Value <> _
         _play.DtPlaySessions.Rows(_idx_row_dt).Item(SQL_COLUMN_AWARDED_POINTS) Then

        ' Updated row
        _play.DtPlaySessions.Rows(_idx_row_dt).Item(SQL_COLUMN_AWARDED_POINTS) = dg_list_play_sessions.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS).Value

        ' Status
        Select Case dg_list_play_sessions.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS_STATUS).Value
          Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(2753)
            _status = AwardPointsStatus.Default
          Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(2754)
            _status = AwardPointsStatus.CalculatedAndReward
          Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(2755)
            _status = AwardPointsStatus.Pending
          Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(2756)
            _status = AwardPointsStatus.Manual
          Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(2757)
            _status = AwardPointsStatus.ManualCalculated
          Case Else
            _status = AwardPointsStatus.Default
        End Select

        _play.DtPlaySessions.Rows(_idx_row_dt).Item(SQL_COLUMN_AWARDED_POINTS_STATUS) = _status
      End If

      _idx_row_dt += 1

    Next

  End Sub ' GUI_GetScreenData

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    Dim _play As CLASS_PLAY_SESSIONS_TO_REVISE
    Dim _idx As Integer

    _play = DbReadObject

    ' Accounts
    _idx = 0
    For Each _row As DataRow In _play.DtAccounts.Rows
      Me.dg_list_accounts.AddRow()
      Me.dg_list_accounts.Cell(_idx, GRID_COLUMN_ACCT_NUMBER).Value = _row(SQL_COLUMN_ACCT_NUMBER)
      Me.dg_list_accounts.Cell(_idx, GRID_COLUMN_HOLDER_NAME).Value = _row(SQL_COLUMN_HOLDER_NAME)
      Me.dg_list_accounts.Cell(_idx, GRID_COLUMN_COMPUTED_POINTS_ACCT).Value = GUI_FormatNumber(_row(SQL_COLUMN_COMPUTED_POINTS_ACCT))
      Me.dg_list_accounts.Cell(_idx, GRID_COLUMN_AWARDED_POINTS_ACCT).Value = GUI_FormatNumber(_row(SQL_COLUMN_AWARDED_POINTS_ACCT))
      Me.dg_list_accounts.Cell(_idx, GRID_COLUMN_TOTAL_PLAY_SESSIONS).Value = GUI_FormatNumber(_row(SQL_COLUMN_TOTAL_PLAY_SESSIONS))

      _idx += 1
    Next

    Call dg_list_accounts.SelectFirstRow(True)
    Call dg_list_accounts_RowSelectedEvent(0)

    ' Add handler to event after fill data
    AddHandler dg_list_accounts.RowSelectedEvent, AddressOf dg_list_accounts_RowSelectedEvent

  End Sub ' GUI_SetScreenData

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2690)

    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False

    Me.ef_points_accounts.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(350)
    Me.ef_points_accounts.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 5, 2)

    Me.btn_apply_accounts.Text = GLB_NLS_GUI_CONTROLS.GetString(31)

    m_point_awarded_changed = 0

    Call GUI_StyleSheet()

  End Sub ' GUI_InitControls

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_PLAY_SESSIONS_TO_REVISE
        DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2690), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2690), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_DELETE

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)
    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE: Key Press Control 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: True / False
  '
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    Select Case e.KeyChar
      Case Chr(Keys.Enter)
        If dg_list_play_sessions.ContainsFocus() Then
          dg_list_play_sessions.KeyPressed(sender, e)

          Return True
        End If

        If dg_list_accounts.ContainsFocus() Then
          dg_list_accounts.KeyPressed(sender, e)

          Return True
        End If

        If ef_points_accounts.ContainsFocus() Then
          Call btn_apply_accounts_ClickEvent()

          Return True
        End If

        Return False

      Case Chr(Keys.Escape)

      Case Else
        Return MyBase.GUI_KeyPress(sender, e)

    End Select

  End Function ' GUI_KeyPress

#End Region

#Region " Private Functions "

  ' PURPOSE: Define all Main Grid Columns
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()

    ' Grid Accounts
    With Me.dg_list_accounts
      Call .Init(GRID_COLUMNS_ACCOUNTS, GRID_HEADER_ROWS_ACCOUNTS, True)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Account number
      .Column(GRID_COLUMN_ACCT_NUMBER).Header.Text = GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COLUMN_ACCT_NUMBER).Width = 1850
      .Column(GRID_COLUMN_ACCT_NUMBER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Holder Name
      .Column(GRID_COLUMN_HOLDER_NAME).Header.Text = GLB_NLS_GUI_INVOICING.GetString(235)
      .Column(GRID_COLUMN_HOLDER_NAME).Width = 3510
      .Column(GRID_COLUMN_HOLDER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Computed Points
      .Column(GRID_COLUMN_COMPUTED_POINTS_ACCT).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2683)
      .Column(GRID_COLUMN_COMPUTED_POINTS_ACCT).Width = 1100
      .Column(GRID_COLUMN_COMPUTED_POINTS_ACCT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Awarded Points
      .Column(GRID_COLUMN_AWARDED_POINTS_ACCT).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2684)
      .Column(GRID_COLUMN_AWARDED_POINTS_ACCT).Width = 1100
      .Column(GRID_COLUMN_AWARDED_POINTS_ACCT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_AWARDED_POINTS_ACCT).Editable = True
      .Column(GRID_COLUMN_AWARDED_POINTS_ACCT).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_AWARDED_POINTS_ACCT).EditionControl.EntryField.SetFilter(FORMAT_NUMBER, 6, 2)

      ' Total Play Sessions
      .Column(GRID_COLUMN_TOTAL_PLAY_SESSIONS).Header.Text = " "
      .Column(GRID_COLUMN_TOTAL_PLAY_SESSIONS).Width = 0

    End With

    With Me.dg_list_play_sessions
      Call .Init(GRID_COLUMNS_SESSIONS, GRID_HEADER_ROWS_SESSIONS, True)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Provider Id
      .Column(GRID_COLUMN_PROVIDER_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(232)
      .Column(GRID_COLUMN_PROVIDER_NAME).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(315)
      .Column(GRID_COLUMN_PROVIDER_NAME).Width = 1750
      .Column(GRID_COLUMN_PROVIDER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Terminal Id
      .Column(GRID_COLUMN_TERMINAL_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(232)
      .Column(GRID_COLUMN_TERMINAL_NAME).Header(1).Text = GLB_NLS_GUI_CONTROLS.GetString(323)
      .Column(GRID_COLUMN_TERMINAL_NAME).Width = 1610
      .Column(GRID_COLUMN_TERMINAL_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Awarded Points Status
      .Column(GRID_COLUMN_AWARDED_POINTS_STATUS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(350)
      .Column(GRID_COLUMN_AWARDED_POINTS_STATUS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(262)
      .Column(GRID_COLUMN_AWARDED_POINTS_STATUS).Width = 1800
      .Column(GRID_COLUMN_AWARDED_POINTS_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Computed Points
      .Column(GRID_COLUMN_COMPUTED_POINTS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(350)
      .Column(GRID_COLUMN_COMPUTED_POINTS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2683)
      .Column(GRID_COLUMN_COMPUTED_POINTS).Width = 1100
      .Column(GRID_COLUMN_COMPUTED_POINTS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Awarded Points
      .Column(GRID_COLUMN_AWARDED_POINTS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(350)
      .Column(GRID_COLUMN_AWARDED_POINTS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2684)
      .Column(GRID_COLUMN_AWARDED_POINTS).Width = 1100
      .Column(GRID_COLUMN_AWARDED_POINTS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_AWARDED_POINTS).Editable = True
      .Column(GRID_COLUMN_AWARDED_POINTS).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_AWARDED_POINTS).EditionControl.EntryField.SetFilter(FORMAT_NUMBER, 6, 2)

      ' Play Session Id
      .Column(GRID_COLUMN_PLAY_SESSION_ID).Header(0).Text = " "
      .Column(GRID_COLUMN_PLAY_SESSION_ID).Header(1).Text = " "
      .Column(GRID_COLUMN_PLAY_SESSION_ID).Width = 0
    End With

  End Sub ' GUI_StyleSheet

  Private Sub dg_list_play_sessions_BeforeStartEditionEvent(ByVal Row As Integer, ByVal Column As Integer, ByRef EditionCanceled As Boolean) Handles dg_list_play_sessions.BeforeStartEditionEvent

    If Column = GRID_COLUMN_AWARDED_POINTS Then
      m_point_awarded_changed = GUI_ParseNumber(Me.dg_list_play_sessions.Cell(Row, Column).Value)
    End If

  End Sub ' dg_list_play_sessions_BeforeStartEditionEvent

  Private Sub dg_list_play_sessions_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer) Handles dg_list_play_sessions.CellDataChangedEvent

    Call DgListPlaySessionsCellDataChangedEvent(Row, Column, True)

  End Sub ' dg_list_play_sessions_CellDataChangedEvent

  Private Sub DgListPlaySessionsCellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer, Optional ByVal Recalc As Boolean = True)
    Dim _points As Double

    If Column = GRID_COLUMN_AWARDED_POINTS Then
      If Me.dg_list_play_sessions.Cell(Row, Column).Value = 0 Then
        Me.dg_list_play_sessions.Cell(Row, GRID_COLUMN_AWARDED_POINTS_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2755)
        Me.dg_list_play_sessions.Row(Row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLUE_01)
      ElseIf Me.dg_list_play_sessions.Cell(Row, Column).Value = Me.dg_list_play_sessions.Cell(Row, GRID_COLUMN_COMPUTED_POINTS).Value Then
        Me.dg_list_play_sessions.Cell(Row, GRID_COLUMN_AWARDED_POINTS_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2757)
        Me.dg_list_play_sessions.Row(Row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
      Else
        Me.dg_list_play_sessions.Cell(Row, GRID_COLUMN_AWARDED_POINTS_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2756)
        Me.dg_list_play_sessions.Row(Row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
      End If

      If Recalc Then
        _points = GUI_ParseNumber(Me.dg_list_accounts.Cell(dg_list_accounts.SelectedRows(0), GRID_COLUMN_AWARDED_POINTS_ACCT).Value) + _
                  GUI_ParseNumber(Me.dg_list_play_sessions.Cell(Row, Column).Value) - _
                  m_point_awarded_changed

        m_point_awarded_changed = 0

        Me.dg_list_accounts.Cell(dg_list_accounts.SelectedRows(0), GRID_COLUMN_AWARDED_POINTS_ACCT).Value = _
                                 GUI_FormatNumber(_points, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End If

    End If

  End Sub ' DgListPlaySessionsCellDataChangedEvent

  Private Sub dg_list_accounts_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer) Handles dg_list_accounts.CellDataChangedEvent
    Dim _idx_row As Integer
    Dim _points As Double

    If Column = GRID_COLUMN_AWARDED_POINTS_ACCT Then
      For _idx_row = 0 To dg_list_play_sessions.NumRows() - 1
        If Me.dg_list_accounts.Cell(Row, Column).Value = Me.dg_list_accounts.Cell(Row, GRID_COLUMN_COMPUTED_POINTS_ACCT).Value Then
          _points = dg_list_play_sessions.Cell(_idx_row, GRID_COLUMN_COMPUTED_POINTS).Value
        Else
          _points = Me.dg_list_accounts.Cell(Row, Column).Value / dg_list_play_sessions.NumRows
        End If
        dg_list_play_sessions.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS).Value = _
                                   GUI_FormatNumber(_points, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        Call DgListPlaySessionsCellDataChangedEvent(_idx_row, GRID_COLUMN_AWARDED_POINTS, False)
      Next
    End If

  End Sub ' dg_list_accounts_CellDataChangedEvent

  Private Sub btn_apply_accounts_ClickEvent() Handles btn_apply_accounts.ClickEvent
    Dim _play As CLASS_PLAY_SESSIONS_TO_REVISE
    Dim _idx_row As Integer

    _play = DbEditedObject

    If ef_points_accounts.Value <> vbNullString Then
      For _idx_row = 0 To dg_list_accounts.NumRows() - 1
        Me.dg_list_accounts.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS_ACCT).Value = ef_points_accounts.Value

        If _idx_row = dg_list_accounts.SelectedRows(0) Then
          Call dg_list_accounts_CellDataChangedEvent(_idx_row, GRID_COLUMN_AWARDED_POINTS_ACCT)
        Else
          For Each _row As DataRow In _play.DtPlaySessions.Rows
            If _row(SQL_COLUMN_ACCOUNT_ID) = Me.dg_list_accounts.Cell(_idx_row, GRID_COLUMN_ACCT_NUMBER).Value Then
              If Me.dg_list_accounts.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS_ACCT).Value = _
              Me.dg_list_accounts.Cell(_idx_row, GRID_COLUMN_COMPUTED_POINTS_ACCT).Value Then
                _row(SQL_COLUMN_AWARDED_POINTS) = _row(SQL_COLUMN_COMPUTED_POINTS)
              Else
                _row(SQL_COLUMN_AWARDED_POINTS) = ef_points_accounts.Value / Me.dg_list_accounts.Cell(_idx_row, GRID_COLUMN_TOTAL_PLAY_SESSIONS).Value
              End If

              If _row(SQL_COLUMN_AWARDED_POINTS) = 0 Then
                _row(SQL_COLUMN_AWARDED_POINTS_STATUS) = AwardPointsStatus.Pending
              ElseIf _row(SQL_COLUMN_AWARDED_POINTS) = _row(SQL_COLUMN_COMPUTED_POINTS) Then
                _row(SQL_COLUMN_AWARDED_POINTS_STATUS) = AwardPointsStatus.ManualCalculated
              Else
                _row(SQL_COLUMN_AWARDED_POINTS_STATUS) = AwardPointsStatus.Manual
              End If
            End If
          Next
        End If

      Next

      ef_points_accounts.Value = vbNullString
    End If

  End Sub ' btn_apply_accounts_ClickEvent

  Private Sub dg_list_accounts_RowSelectedEvent(ByVal SelectedRow As System.Int32)
    Dim _play As CLASS_PLAY_SESSIONS_TO_REVISE
    Dim _idx As Integer
    Dim _str_status As String = ""
    Dim _rows_play_sessions As DataRow()

    Call GUI_GetScreenData()

    _play = DbEditedObject

    _rows_play_sessions = _play.DtPlaySessions.Select("PS_ACCOUNT_ID = " & dg_list_accounts.Cell(SelectedRow, GRID_COLUMN_ACCT_NUMBER).Value)

    Me.dg_list_play_sessions.Clear()

    ' Play Sessions
    _idx = 0
    For Each _row As DataRow In _rows_play_sessions
      Me.dg_list_play_sessions.AddRow()
      Me.dg_list_play_sessions.Cell(_idx, GRID_COLUMN_PROVIDER_NAME).Value = _row(SQL_COLUMN_PROVIDER_NAME)
      Me.dg_list_play_sessions.Cell(_idx, GRID_COLUMN_TERMINAL_NAME).Value = _row(SQL_COLUMN_TERMINAL_NAME)
      ' Status
      Select Case _row(SQL_COLUMN_AWARDED_POINTS_STATUS)
        Case AwardPointsStatus.Default
          _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2753)
        Case AwardPointsStatus.CalculatedAndReward
          _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2754)
        Case AwardPointsStatus.Pending
          _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2755)
          Me.dg_list_play_sessions.Row(_idx).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLUE_01)
        Case AwardPointsStatus.Manual
          _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2756)
        Case AwardPointsStatus.ManualCalculated
          _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2757)
      End Select
      Me.dg_list_play_sessions.Cell(_idx, GRID_COLUMN_AWARDED_POINTS_STATUS).Value = _str_status
      Me.dg_list_play_sessions.Cell(_idx, GRID_COLUMN_COMPUTED_POINTS).Value = GUI_FormatNumber(_row(SQL_COLUMN_COMPUTED_POINTS))
      Me.dg_list_play_sessions.Cell(_idx, GRID_COLUMN_AWARDED_POINTS).Value = GUI_FormatNumber(_row(SQL_COLUMN_AWARDED_POINTS))
      Me.dg_list_play_sessions.Cell(_idx, GRID_COLUMN_PLAY_SESSION_ID).Value = _row(SQL_COLUMN_PLAY_SESSION_ID)

      _idx += 1
    Next

  End Sub ' dg_list_accounts_RowSelectedEvent

#End Region

End Class
