﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_bucket_edit
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.chk_expiration_date = New System.Windows.Forms.CheckBox()
    Me.ef_expiration_days = New GUI_Controls.uc_entry_field()
    Me.gb_expiration = New System.Windows.Forms.GroupBox()
    Me.chk_expiration_days = New System.Windows.Forms.CheckBox()
    Me.dtp_expiration_date = New System.Windows.Forms.DateTimePicker()
    Me.chk_enabled = New System.Windows.Forms.CheckBox()
    Me.chk_lcd = New System.Windows.Forms.CheckBox()
    Me.chk_cashier = New System.Windows.Forms.CheckBox()
    Me.chk_promobox = New System.Windows.Forms.CheckBox()
    Me.gb_visible = New System.Windows.Forms.GroupBox()
    Me.dg_Coef_Global = New GUI_Controls.uc_grid()
    Me.rbLevelBased = New System.Windows.Forms.RadioButton()
    Me.rbGlobal = New System.Windows.Forms.RadioButton()
    Me.dg_Coef_Level = New GUI_Controls.uc_grid()
    Me.ef_bucket_name = New GUI_Controls.uc_entry_field()
    Me.cb_CalculoCoefK = New GUI_Controls.uc_combo()
    Me.lblType = New System.Windows.Forms.Label()
    Me.panel_data.SuspendLayout()
    Me.gb_expiration.SuspendLayout()
    Me.gb_visible.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.chk_lcd)
    Me.panel_data.Controls.Add(Me.lblType)
    Me.panel_data.Controls.Add(Me.chk_promobox)
    Me.panel_data.Controls.Add(Me.cb_CalculoCoefK)
    Me.panel_data.Controls.Add(Me.dg_Coef_Global)
    Me.panel_data.Controls.Add(Me.ef_bucket_name)
    Me.panel_data.Controls.Add(Me.rbLevelBased)
    Me.panel_data.Controls.Add(Me.dg_Coef_Level)
    Me.panel_data.Controls.Add(Me.rbGlobal)
    Me.panel_data.Controls.Add(Me.gb_visible)
    Me.panel_data.Controls.Add(Me.chk_enabled)
    Me.panel_data.Controls.Add(Me.gb_expiration)
    Me.panel_data.Size = New System.Drawing.Size(1009, 320)
    '
    'chk_expiration_date
    '
    Me.chk_expiration_date.AutoSize = True
    Me.chk_expiration_date.Location = New System.Drawing.Point(6, 62)
    Me.chk_expiration_date.Name = "chk_expiration_date"
    Me.chk_expiration_date.Size = New System.Drawing.Size(75, 17)
    Me.chk_expiration_date.TabIndex = 28
    Me.chk_expiration_date.Text = "xTo date"
    Me.chk_expiration_date.UseVisualStyleBackColor = True
    '
    'ef_expiration_days
    '
    Me.ef_expiration_days.DoubleValue = 0.0R
    Me.ef_expiration_days.IntegerValue = 0
    Me.ef_expiration_days.IsReadOnly = False
    Me.ef_expiration_days.Location = New System.Drawing.Point(91, 19)
    Me.ef_expiration_days.Name = "ef_expiration_days"
    Me.ef_expiration_days.PlaceHolder = Nothing
    Me.ef_expiration_days.Size = New System.Drawing.Size(184, 24)
    Me.ef_expiration_days.SufixText = "xdays without activity"
    Me.ef_expiration_days.SufixTextVisible = True
    Me.ef_expiration_days.SufixTextWidth = 150
    Me.ef_expiration_days.TabIndex = 27
    Me.ef_expiration_days.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_expiration_days.TextValue = ""
    Me.ef_expiration_days.TextVisible = False
    Me.ef_expiration_days.TextWidth = 0
    Me.ef_expiration_days.Value = ""
    Me.ef_expiration_days.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_expiration
    '
    Me.gb_expiration.Controls.Add(Me.chk_expiration_days)
    Me.gb_expiration.Controls.Add(Me.dtp_expiration_date)
    Me.gb_expiration.Controls.Add(Me.ef_expiration_days)
    Me.gb_expiration.Controls.Add(Me.chk_expiration_date)
    Me.gb_expiration.Location = New System.Drawing.Point(7, 85)
    Me.gb_expiration.Name = "gb_expiration"
    Me.gb_expiration.Size = New System.Drawing.Size(286, 108)
    Me.gb_expiration.TabIndex = 35
    Me.gb_expiration.TabStop = False
    Me.gb_expiration.Text = "xExpiration bucket from client"
    '
    'chk_expiration_days
    '
    Me.chk_expiration_days.AutoSize = True
    Me.chk_expiration_days.Location = New System.Drawing.Point(6, 27)
    Me.chk_expiration_days.Name = "chk_expiration_days"
    Me.chk_expiration_days.Size = New System.Drawing.Size(75, 17)
    Me.chk_expiration_days.TabIndex = 52
    Me.chk_expiration_days.Text = "xTo date"
    Me.chk_expiration_days.UseVisualStyleBackColor = True
    '
    'dtp_expiration_date
    '
    Me.dtp_expiration_date.CustomFormat = "dd MMMM"
    Me.dtp_expiration_date.Format = System.Windows.Forms.DateTimePickerFormat.Custom
    Me.dtp_expiration_date.Location = New System.Drawing.Point(91, 57)
    Me.dtp_expiration_date.Name = "dtp_expiration_date"
    Me.dtp_expiration_date.Size = New System.Drawing.Size(120, 21)
    Me.dtp_expiration_date.TabIndex = 51
    '
    'chk_enabled
    '
    Me.chk_enabled.AutoSize = True
    Me.chk_enabled.Location = New System.Drawing.Point(7, 12)
    Me.chk_enabled.Name = "chk_enabled"
    Me.chk_enabled.Size = New System.Drawing.Size(78, 17)
    Me.chk_enabled.TabIndex = 36
    Me.chk_enabled.Text = "xEnabled"
    Me.chk_enabled.UseVisualStyleBackColor = True
    '
    'chk_lcd
    '
    Me.chk_lcd.AutoSize = True
    Me.chk_lcd.Location = New System.Drawing.Point(13, 261)
    Me.chk_lcd.Name = "chk_lcd"
    Me.chk_lcd.Size = New System.Drawing.Size(57, 17)
    Me.chk_lcd.TabIndex = 43
    Me.chk_lcd.Text = "xLCD"
    Me.chk_lcd.UseVisualStyleBackColor = True
    Me.chk_lcd.Visible = False
    '
    'chk_cashier
    '
    Me.chk_cashier.AutoSize = True
    Me.chk_cashier.Location = New System.Drawing.Point(6, 21)
    Me.chk_cashier.Name = "chk_cashier"
    Me.chk_cashier.Size = New System.Drawing.Size(77, 17)
    Me.chk_cashier.TabIndex = 44
    Me.chk_cashier.Text = "xCashier"
    Me.chk_cashier.UseVisualStyleBackColor = True
    '
    'chk_promobox
    '
    Me.chk_promobox.AutoSize = True
    Me.chk_promobox.Location = New System.Drawing.Point(13, 291)
    Me.chk_promobox.Name = "chk_promobox"
    Me.chk_promobox.Size = New System.Drawing.Size(95, 17)
    Me.chk_promobox.TabIndex = 45
    Me.chk_promobox.Text = "xPromoBOX"
    Me.chk_promobox.UseVisualStyleBackColor = True
    Me.chk_promobox.Visible = False
    '
    'gb_visible
    '
    Me.gb_visible.Controls.Add(Me.chk_cashier)
    Me.gb_visible.Location = New System.Drawing.Point(7, 200)
    Me.gb_visible.Name = "gb_visible"
    Me.gb_visible.Size = New System.Drawing.Size(131, 48)
    Me.gb_visible.TabIndex = 46
    Me.gb_visible.TabStop = False
    Me.gb_visible.Text = "xVisible"
    '
    'dg_Coef_Global
    '
    Me.dg_Coef_Global.CurrentCol = -1
    Me.dg_Coef_Global.CurrentRow = -1
    Me.dg_Coef_Global.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_Coef_Global.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_Coef_Global.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_Coef_Global.Location = New System.Drawing.Point(299, 105)
    Me.dg_Coef_Global.Name = "dg_Coef_Global"
    Me.dg_Coef_Global.PanelRightVisible = False
    Me.dg_Coef_Global.Redraw = True
    Me.dg_Coef_Global.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    Me.dg_Coef_Global.Size = New System.Drawing.Size(702, 37)
    Me.dg_Coef_Global.Sortable = False
    Me.dg_Coef_Global.SortAscending = True
    Me.dg_Coef_Global.SortByCol = 0
    Me.dg_Coef_Global.TabIndex = 41
    Me.dg_Coef_Global.ToolTipped = True
    Me.dg_Coef_Global.TopRow = -2
    '
    'rbLevelBased
    '
    Me.rbLevelBased.AutoSize = True
    Me.rbLevelBased.Location = New System.Drawing.Point(299, 72)
    Me.rbLevelBased.Name = "rbLevelBased"
    Me.rbLevelBased.Size = New System.Drawing.Size(97, 17)
    Me.rbLevelBased.TabIndex = 38
    Me.rbLevelBased.Text = "xLevelBased"
    Me.rbLevelBased.UseVisualStyleBackColor = True
    '
    'rbGlobal
    '
    Me.rbGlobal.AutoSize = True
    Me.rbGlobal.Checked = True
    Me.rbGlobal.Location = New System.Drawing.Point(489, 72)
    Me.rbGlobal.Name = "rbGlobal"
    Me.rbGlobal.Size = New System.Drawing.Size(68, 17)
    Me.rbGlobal.TabIndex = 37
    Me.rbGlobal.TabStop = True
    Me.rbGlobal.Text = "xGlobal"
    Me.rbGlobal.UseVisualStyleBackColor = True
    '
    'dg_Coef_Level
    '
    Me.dg_Coef_Level.CurrentCol = -1
    Me.dg_Coef_Level.CurrentRow = -1
    Me.dg_Coef_Level.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_Coef_Level.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_Coef_Level.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_Coef_Level.Location = New System.Drawing.Point(299, 105)
    Me.dg_Coef_Level.Name = "dg_Coef_Level"
    Me.dg_Coef_Level.PanelRightVisible = False
    Me.dg_Coef_Level.Redraw = True
    Me.dg_Coef_Level.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    Me.dg_Coef_Level.Size = New System.Drawing.Size(702, 85)
    Me.dg_Coef_Level.Sortable = False
    Me.dg_Coef_Level.SortAscending = True
    Me.dg_Coef_Level.SortByCol = 0
    Me.dg_Coef_Level.TabIndex = 36
    Me.dg_Coef_Level.ToolTipped = True
    Me.dg_Coef_Level.TopRow = -2
    '
    'ef_bucket_name
    '
    Me.ef_bucket_name.DoubleValue = 0.0R
    Me.ef_bucket_name.IntegerValue = 0
    Me.ef_bucket_name.IsReadOnly = False
    Me.ef_bucket_name.Location = New System.Drawing.Point(7, 35)
    Me.ef_bucket_name.Name = "ef_bucket_name"
    Me.ef_bucket_name.PlaceHolder = Nothing
    Me.ef_bucket_name.Size = New System.Drawing.Size(399, 24)
    Me.ef_bucket_name.SufixText = "Sufix Text"
    Me.ef_bucket_name.SufixTextVisible = True
    Me.ef_bucket_name.TabIndex = 50
    Me.ef_bucket_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_bucket_name.TextValue = ""
    Me.ef_bucket_name.Value = ""
    Me.ef_bucket_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'cb_CalculoCoefK
    '
    Me.cb_CalculoCoefK.AllowUnlistedValues = False
    Me.cb_CalculoCoefK.AutoCompleteMode = False
    Me.cb_CalculoCoefK.IsReadOnly = False
    Me.cb_CalculoCoefK.Location = New System.Drawing.Point(447, 34)
    Me.cb_CalculoCoefK.Name = "cb_CalculoCoefK"
    Me.cb_CalculoCoefK.SelectedIndex = -1
    Me.cb_CalculoCoefK.Size = New System.Drawing.Size(250, 24)
    Me.cb_CalculoCoefK.SufixText = "Sufix Text"
    Me.cb_CalculoCoefK.SufixTextVisible = True
    Me.cb_CalculoCoefK.TabIndex = 51
    Me.cb_CalculoCoefK.TextCombo = Nothing
    Me.cb_CalculoCoefK.TextWidth = 120
    '
    'lblType
    '
    Me.lblType.AutoSize = True
    Me.lblType.Location = New System.Drawing.Point(146, 13)
    Me.lblType.Name = "lblType"
    Me.lblType.Size = New System.Drawing.Size(57, 13)
    Me.lblType.TabIndex = 52
    Me.lblType.Text = "xLblType"
    '
    'frm_bucket_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1111, 329)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_bucket_edit"
    Me.Text = "frm_bucket_edit"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.gb_expiration.ResumeLayout(False)
    Me.gb_expiration.PerformLayout()
    Me.gb_visible.ResumeLayout(False)
    Me.gb_visible.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents chk_expiration_date As System.Windows.Forms.CheckBox
  Friend WithEvents ef_expiration_days As GUI_Controls.uc_entry_field
  Friend WithEvents chk_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents gb_expiration As System.Windows.Forms.GroupBox
  Friend WithEvents gb_visible As System.Windows.Forms.GroupBox
  Friend WithEvents chk_lcd As System.Windows.Forms.CheckBox
  Friend WithEvents chk_promobox As System.Windows.Forms.CheckBox
  Friend WithEvents chk_cashier As System.Windows.Forms.CheckBox
  Friend WithEvents ef_bucket_name As GUI_Controls.uc_entry_field
  Friend WithEvents dtp_expiration_date As System.Windows.Forms.DateTimePicker
  Friend WithEvents chk_expiration_days As System.Windows.Forms.CheckBox
  Friend WithEvents rbLevelBased As System.Windows.Forms.RadioButton
  Friend WithEvents rbGlobal As System.Windows.Forms.RadioButton
  Friend WithEvents dg_Coef_Level As GUI_Controls.uc_grid
  Friend WithEvents dg_Coef_Global As GUI_Controls.uc_grid
  Friend WithEvents cb_CalculoCoefK As GUI_Controls.uc_combo
  Friend WithEvents lblType As System.Windows.Forms.Label
End Class
