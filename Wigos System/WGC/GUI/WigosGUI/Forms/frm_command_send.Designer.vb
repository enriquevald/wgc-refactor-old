<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_command_send
  Inherits GUI_Controls.frm_base

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Me.uc_pr_list = New GUI_Controls.uc_provider()
    Me.btn_restart = New GUI_Controls.uc_button()
    Me.btn_logger = New GUI_Controls.uc_button()
    Me.gd_commands = New GUI_Controls.uc_grid()
    Me.tmr_refresh = New System.Windows.Forms.Timer(Me.components)
    Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
    Me.Button2 = New System.Windows.Forms.Button()
    Me.Button1 = New System.Windows.Forms.Button()
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox()
    Me.panel_filter_buttons = New System.Windows.Forms.Panel()
    Me.btn_clear = New GUI_Controls.uc_button()
    Me.gb_status = New System.Windows.Forms.GroupBox()
    Me.lbl_pending_reply_text = New System.Windows.Forms.Label()
    Me.lbl_timeout_disconnected_text = New System.Windows.Forms.Label()
    Me.lbl_timeout_not_reply_text = New System.Windows.Forms.Label()
    Me.lbl_replied_error_text = New System.Windows.Forms.Label()
    Me.lbl_replied_ok_text = New System.Windows.Forms.Label()
    Me.lbl_pending_send_text = New System.Windows.Forms.Label()
    Me.lbl_timeout_disconnected = New System.Windows.Forms.Label()
    Me.lbl_timeout_not_reply = New System.Windows.Forms.Label()
    Me.lbl_replied_error = New System.Windows.Forms.Label()
    Me.lbl_replied_ok = New System.Windows.Forms.Label()
    Me.lbl_pending_reply = New System.Windows.Forms.Label()
    Me.lbl_pending_send = New System.Windows.Forms.Label()
    Me.pn_line = New System.Windows.Forms.Panel()
    Me.SplitContainer2 = New System.Windows.Forms.SplitContainer()
    Me.btn_select = New GUI_Controls.uc_button()
    Me.btn_exit = New GUI_Controls.uc_button()
    Me.SplitContainer1.Panel1.SuspendLayout()
    Me.SplitContainer1.Panel2.SuspendLayout()
    Me.SplitContainer1.SuspendLayout()
    Me.panel_filter_buttons.SuspendLayout()
    Me.gb_status.SuspendLayout()
    Me.SplitContainer2.Panel1.SuspendLayout()
    Me.SplitContainer2.Panel2.SuspendLayout()
    Me.SplitContainer2.SuspendLayout()
    Me.SuspendLayout()
    '
    'uc_pr_list
    '
    Me.uc_pr_list.FilterByCurrency = False
    Me.uc_pr_list.Location = New System.Drawing.Point(3, 3)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(338, 188)
    Me.uc_pr_list.TabIndex = 0
    '
    'btn_restart
    '
    Me.btn_restart.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_restart.Location = New System.Drawing.Point(347, 121)
    Me.btn_restart.Name = "btn_restart"
    Me.btn_restart.Size = New System.Drawing.Size(90, 30)
    Me.btn_restart.TabIndex = 2
    Me.btn_restart.ToolTipped = False
    Me.btn_restart.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_logger
    '
    Me.btn_logger.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_logger.Location = New System.Drawing.Point(347, 157)
    Me.btn_logger.Name = "btn_logger"
    Me.btn_logger.Size = New System.Drawing.Size(90, 30)
    Me.btn_logger.TabIndex = 3
    Me.btn_logger.ToolTipped = False
    Me.btn_logger.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'gd_commands
    '
    Me.gd_commands.CurrentCol = -1
    Me.gd_commands.CurrentRow = -1
    Me.gd_commands.Dock = System.Windows.Forms.DockStyle.Fill
    Me.gd_commands.EditableCellBackColor = System.Drawing.Color.Empty
    Me.gd_commands.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.gd_commands.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.gd_commands.Location = New System.Drawing.Point(0, 0)
    Me.gd_commands.Name = "gd_commands"
    Me.gd_commands.PanelRightVisible = True
    Me.gd_commands.Redraw = True
    Me.gd_commands.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.gd_commands.Size = New System.Drawing.Size(860, 513)
    Me.gd_commands.Sortable = False
    Me.gd_commands.SortAscending = True
    Me.gd_commands.SortByCol = 0
    Me.gd_commands.TabIndex = 0
    Me.gd_commands.ToolTipped = True
    Me.gd_commands.TopRow = -2
    '
    'tmr_refresh
    '
    '
    'SplitContainer1
    '
    Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
    Me.SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
    Me.SplitContainer1.Location = New System.Drawing.Point(4, 4)
    Me.SplitContainer1.Name = "SplitContainer1"
    Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
    '
    'SplitContainer1.Panel1
    '
    Me.SplitContainer1.Panel1.Controls.Add(Me.Button2)
    Me.SplitContainer1.Panel1.Controls.Add(Me.Button1)
    Me.SplitContainer1.Panel1.Controls.Add(Me.chk_terminal_location)
    Me.SplitContainer1.Panel1.Controls.Add(Me.panel_filter_buttons)
    Me.SplitContainer1.Panel1.Controls.Add(Me.gb_status)
    Me.SplitContainer1.Panel1.Controls.Add(Me.pn_line)
    Me.SplitContainer1.Panel1.Controls.Add(Me.uc_pr_list)
    Me.SplitContainer1.Panel1.Controls.Add(Me.btn_logger)
    Me.SplitContainer1.Panel1.Controls.Add(Me.btn_restart)
    '
    'SplitContainer1.Panel2
    '
    Me.SplitContainer1.Panel2.Controls.Add(Me.SplitContainer2)
    Me.SplitContainer1.Size = New System.Drawing.Size(956, 714)
    Me.SplitContainer1.SplitterDistance = 197
    Me.SplitContainer1.TabIndex = 4
    '
    'Button2
    '
    Me.Button2.Location = New System.Drawing.Point(773, 77)
    Me.Button2.Name = "Button2"
    Me.Button2.Size = New System.Drawing.Size(75, 56)
    Me.Button2.TabIndex = 13
    Me.Button2.Text = "WCP_Command"
    Me.Button2.UseVisualStyleBackColor = True
    Me.Button2.Visible = False
    '
    'Button1
    '
    Me.Button1.Location = New System.Drawing.Point(773, 3)
    Me.Button1.Name = "Button1"
    Me.Button1.Size = New System.Drawing.Size(75, 68)
    Me.Button1.TabIndex = 12
    Me.Button1.Text = "Subs Machine Partial Credit"
    Me.Button1.UseVisualStyleBackColor = True
    Me.Button1.Visible = False
    '
    'chk_terminal_location
    '
    Me.chk_terminal_location.Location = New System.Drawing.Point(354, 59)
    Me.chk_terminal_location.Name = "chk_terminal_location"
    Me.chk_terminal_location.Size = New System.Drawing.Size(211, 20)
    Me.chk_terminal_location.TabIndex = 1
    Me.chk_terminal_location.Text = "xShow terminals location"
    '
    'panel_filter_buttons
    '
    Me.panel_filter_buttons.Controls.Add(Me.btn_clear)
    Me.panel_filter_buttons.Dock = System.Windows.Forms.DockStyle.Right
    Me.panel_filter_buttons.Location = New System.Drawing.Point(864, 0)
    Me.panel_filter_buttons.Name = "panel_filter_buttons"
    Me.panel_filter_buttons.Size = New System.Drawing.Size(92, 193)
    Me.panel_filter_buttons.TabIndex = 11
    '
    'btn_clear
    '
    Me.btn_clear.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_clear.Location = New System.Drawing.Point(0, 157)
    Me.btn_clear.Margin = New System.Windows.Forms.Padding(10)
    Me.btn_clear.Name = "btn_clear"
    Me.btn_clear.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_clear.Size = New System.Drawing.Size(90, 30)
    Me.btn_clear.TabIndex = 4
    Me.btn_clear.ToolTipped = False
    Me.btn_clear.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'gb_status
    '
    Me.gb_status.Controls.Add(Me.lbl_pending_reply_text)
    Me.gb_status.Controls.Add(Me.lbl_timeout_disconnected_text)
    Me.gb_status.Controls.Add(Me.lbl_timeout_not_reply_text)
    Me.gb_status.Controls.Add(Me.lbl_replied_error_text)
    Me.gb_status.Controls.Add(Me.lbl_replied_ok_text)
    Me.gb_status.Controls.Add(Me.lbl_pending_send_text)
    Me.gb_status.Controls.Add(Me.lbl_timeout_disconnected)
    Me.gb_status.Controls.Add(Me.lbl_timeout_not_reply)
    Me.gb_status.Controls.Add(Me.lbl_replied_error)
    Me.gb_status.Controls.Add(Me.lbl_replied_ok)
    Me.gb_status.Controls.Add(Me.lbl_pending_reply)
    Me.gb_status.Controls.Add(Me.lbl_pending_send)
    Me.gb_status.Location = New System.Drawing.Point(565, 35)
    Me.gb_status.Name = "gb_status"
    Me.gb_status.Size = New System.Drawing.Size(181, 152)
    Me.gb_status.TabIndex = 4
    Me.gb_status.TabStop = False
    Me.gb_status.Text = "xStatus"
    '
    'lbl_pending_reply_text
    '
    Me.lbl_pending_reply_text.AutoSize = True
    Me.lbl_pending_reply_text.Location = New System.Drawing.Point(28, 41)
    Me.lbl_pending_reply_text.Name = "lbl_pending_reply_text"
    Me.lbl_pending_reply_text.Size = New System.Drawing.Size(91, 13)
    Me.lbl_pending_reply_text.TabIndex = 118
    Me.lbl_pending_reply_text.Text = "xPendingReply"
    '
    'lbl_timeout_disconnected_text
    '
    Me.lbl_timeout_disconnected_text.AutoSize = True
    Me.lbl_timeout_disconnected_text.Location = New System.Drawing.Point(28, 129)
    Me.lbl_timeout_disconnected_text.Name = "lbl_timeout_disconnected_text"
    Me.lbl_timeout_disconnected_text.Size = New System.Drawing.Size(136, 13)
    Me.lbl_timeout_disconnected_text.TabIndex = 117
    Me.lbl_timeout_disconnected_text.Text = "xTimeoutDisconnected"
    '
    'lbl_timeout_not_reply_text
    '
    Me.lbl_timeout_not_reply_text.AutoSize = True
    Me.lbl_timeout_not_reply_text.Location = New System.Drawing.Point(28, 107)
    Me.lbl_timeout_not_reply_text.Name = "lbl_timeout_not_reply_text"
    Me.lbl_timeout_not_reply_text.Size = New System.Drawing.Size(111, 13)
    Me.lbl_timeout_not_reply_text.TabIndex = 116
    Me.lbl_timeout_not_reply_text.Text = "xTimeoutNotReply"
    '
    'lbl_replied_error_text
    '
    Me.lbl_replied_error_text.AutoSize = True
    Me.lbl_replied_error_text.Location = New System.Drawing.Point(28, 85)
    Me.lbl_replied_error_text.Name = "lbl_replied_error_text"
    Me.lbl_replied_error_text.Size = New System.Drawing.Size(85, 13)
    Me.lbl_replied_error_text.TabIndex = 115
    Me.lbl_replied_error_text.Text = "xRepliedError"
    '
    'lbl_replied_ok_text
    '
    Me.lbl_replied_ok_text.AutoSize = True
    Me.lbl_replied_ok_text.Location = New System.Drawing.Point(28, 63)
    Me.lbl_replied_ok_text.Name = "lbl_replied_ok_text"
    Me.lbl_replied_ok_text.Size = New System.Drawing.Size(72, 13)
    Me.lbl_replied_ok_text.TabIndex = 114
    Me.lbl_replied_ok_text.Text = "xRepliedOk"
    '
    'lbl_pending_send_text
    '
    Me.lbl_pending_send_text.AutoSize = True
    Me.lbl_pending_send_text.Location = New System.Drawing.Point(28, 19)
    Me.lbl_pending_send_text.Name = "lbl_pending_send_text"
    Me.lbl_pending_send_text.Size = New System.Drawing.Size(88, 13)
    Me.lbl_pending_send_text.TabIndex = 113
    Me.lbl_pending_send_text.Text = "xPendingSend"
    '
    'lbl_timeout_disconnected
    '
    Me.lbl_timeout_disconnected.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_timeout_disconnected.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_timeout_disconnected.Location = New System.Drawing.Point(6, 128)
    Me.lbl_timeout_disconnected.Name = "lbl_timeout_disconnected"
    Me.lbl_timeout_disconnected.Size = New System.Drawing.Size(16, 16)
    Me.lbl_timeout_disconnected.TabIndex = 112
    '
    'lbl_timeout_not_reply
    '
    Me.lbl_timeout_not_reply.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_timeout_not_reply.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_timeout_not_reply.Location = New System.Drawing.Point(6, 106)
    Me.lbl_timeout_not_reply.Name = "lbl_timeout_not_reply"
    Me.lbl_timeout_not_reply.Size = New System.Drawing.Size(16, 16)
    Me.lbl_timeout_not_reply.TabIndex = 111
    '
    'lbl_replied_error
    '
    Me.lbl_replied_error.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_replied_error.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_replied_error.Location = New System.Drawing.Point(6, 84)
    Me.lbl_replied_error.Name = "lbl_replied_error"
    Me.lbl_replied_error.Size = New System.Drawing.Size(16, 16)
    Me.lbl_replied_error.TabIndex = 110
    '
    'lbl_replied_ok
    '
    Me.lbl_replied_ok.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_replied_ok.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_replied_ok.Location = New System.Drawing.Point(6, 62)
    Me.lbl_replied_ok.Name = "lbl_replied_ok"
    Me.lbl_replied_ok.Size = New System.Drawing.Size(16, 16)
    Me.lbl_replied_ok.TabIndex = 109
    '
    'lbl_pending_reply
    '
    Me.lbl_pending_reply.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_pending_reply.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_pending_reply.Location = New System.Drawing.Point(6, 40)
    Me.lbl_pending_reply.Name = "lbl_pending_reply"
    Me.lbl_pending_reply.Size = New System.Drawing.Size(16, 16)
    Me.lbl_pending_reply.TabIndex = 108
    '
    'lbl_pending_send
    '
    Me.lbl_pending_send.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_pending_send.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_pending_send.Location = New System.Drawing.Point(6, 18)
    Me.lbl_pending_send.Name = "lbl_pending_send"
    Me.lbl_pending_send.Size = New System.Drawing.Size(16, 16)
    Me.lbl_pending_send.TabIndex = 107
    '
    'pn_line
    '
    Me.pn_line.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.pn_line.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.pn_line.Location = New System.Drawing.Point(0, 193)
    Me.pn_line.Name = "pn_line"
    Me.pn_line.Size = New System.Drawing.Size(956, 4)
    Me.pn_line.TabIndex = 5
    '
    'SplitContainer2
    '
    Me.SplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill
    Me.SplitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel2
    Me.SplitContainer2.Location = New System.Drawing.Point(0, 0)
    Me.SplitContainer2.Name = "SplitContainer2"
    '
    'SplitContainer2.Panel1
    '
    Me.SplitContainer2.Panel1.Controls.Add(Me.gd_commands)
    '
    'SplitContainer2.Panel2
    '
    Me.SplitContainer2.Panel2.Controls.Add(Me.btn_select)
    Me.SplitContainer2.Panel2.Controls.Add(Me.btn_exit)
    Me.SplitContainer2.Size = New System.Drawing.Size(956, 513)
    Me.SplitContainer2.SplitterDistance = 860
    Me.SplitContainer2.TabIndex = 5
    '
    'btn_select
    '
    Me.btn_select.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btn_select.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.btn_select.Location = New System.Drawing.Point(0, 453)
    Me.btn_select.Name = "btn_select"
    Me.btn_select.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_select.Size = New System.Drawing.Size(90, 30)
    Me.btn_select.TabIndex = 1
    Me.btn_select.ToolTipped = False
    Me.btn_select.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_exit
    '
    Me.btn_exit.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btn_exit.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.btn_exit.Location = New System.Drawing.Point(0, 483)
    Me.btn_exit.Name = "btn_exit"
    Me.btn_exit.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_exit.Size = New System.Drawing.Size(90, 30)
    Me.btn_exit.TabIndex = 2
    Me.btn_exit.ToolTipped = False
    Me.btn_exit.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'frm_command_send
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.CancelButton = Me.btn_exit
    Me.ClientSize = New System.Drawing.Size(964, 722)
    Me.Controls.Add(Me.SplitContainer1)
    Me.Name = "frm_command_send"
    Me.Text = "frm_command_send"
    Me.SplitContainer1.Panel1.ResumeLayout(False)
    Me.SplitContainer1.Panel2.ResumeLayout(False)
    Me.SplitContainer1.ResumeLayout(False)
    Me.panel_filter_buttons.ResumeLayout(False)
    Me.gb_status.ResumeLayout(False)
    Me.gb_status.PerformLayout()
    Me.SplitContainer2.Panel1.ResumeLayout(False)
    Me.SplitContainer2.Panel2.ResumeLayout(False)
    Me.SplitContainer2.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents btn_restart As GUI_Controls.uc_button
  Friend WithEvents btn_logger As GUI_Controls.uc_button
  Friend WithEvents gd_commands As GUI_Controls.uc_grid
  Friend WithEvents tmr_refresh As System.Windows.Forms.Timer
  Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
  Friend WithEvents SplitContainer2 As System.Windows.Forms.SplitContainer
  Friend WithEvents btn_exit As GUI_Controls.uc_button
  Protected WithEvents pn_line As System.Windows.Forms.Panel
  Friend WithEvents btn_select As GUI_Controls.uc_button
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_timeout_disconnected As System.Windows.Forms.Label
  Friend WithEvents lbl_timeout_not_reply As System.Windows.Forms.Label
  Friend WithEvents lbl_replied_error As System.Windows.Forms.Label
  Friend WithEvents lbl_replied_ok As System.Windows.Forms.Label
  Friend WithEvents lbl_pending_reply As System.Windows.Forms.Label
  Friend WithEvents lbl_pending_send As System.Windows.Forms.Label
  Friend WithEvents lbl_pending_reply_text As System.Windows.Forms.Label
  Friend WithEvents lbl_timeout_disconnected_text As System.Windows.Forms.Label
  Friend WithEvents lbl_timeout_not_reply_text As System.Windows.Forms.Label
  Friend WithEvents lbl_replied_error_text As System.Windows.Forms.Label
  Friend WithEvents lbl_replied_ok_text As System.Windows.Forms.Label
  Friend WithEvents lbl_pending_send_text As System.Windows.Forms.Label
  Private WithEvents panel_filter_buttons As System.Windows.Forms.Panel
  Friend WithEvents btn_clear As GUI_Controls.uc_button
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox
  Friend WithEvents Button1 As System.Windows.Forms.Button
  Friend WithEvents Button2 As System.Windows.Forms.Button
End Class
