﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_creditline_sel
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing AndAlso components IsNot Nothing Then
      components.Dispose()
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.uc_account_sel1 = New GUI_Controls.uc_account_sel()
    Me.uc_creditline_status = New GUI_Controls.uc_checked_list()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.uc_dtp_from = New GUI_Controls.uc_date_picker()
    Me.gb_debt = New System.Windows.Forms.GroupBox()
    Me.ef_greater_than = New GUI_Controls.uc_entry_field()
    Me.chkDebtGreaterThan = New System.Windows.Forms.CheckBox()
    Me.chkWithoutDebt = New System.Windows.Forms.CheckBox()
    Me.chkWithDebt = New System.Windows.Forms.CheckBox()
    Me.uc_sites_sel = New GUI_Controls.uc_sites_sel()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_debt.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_debt)
    Me.panel_filter.Controls.Add(Me.uc_sites_sel)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Controls.Add(Me.uc_creditline_status)
    Me.panel_filter.Controls.Add(Me.uc_account_sel1)
    Me.panel_filter.Size = New System.Drawing.Size(1190, 171)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account_sel1, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_creditline_status, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_sites_sel, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_debt, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 175)
    Me.panel_data.Size = New System.Drawing.Size(1190, 499)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1184, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1184, 4)
    '
    'uc_account_sel1
    '
    Me.uc_account_sel1.Account = ""
    Me.uc_account_sel1.AccountText = ""
    Me.uc_account_sel1.Anon = False
    Me.uc_account_sel1.AutoSize = True
    Me.uc_account_sel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_account_sel1.BirthDate = New Date(CType(0, Long))
    Me.uc_account_sel1.DisabledHolder = False
    Me.uc_account_sel1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_account_sel1.Holder = ""
    Me.uc_account_sel1.Location = New System.Drawing.Point(6, 6)
    Me.uc_account_sel1.MassiveSearchNumbers = ""
    Me.uc_account_sel1.MassiveSearchNumbersToEdit = ""
    Me.uc_account_sel1.MassiveSearchType = 0
    Me.uc_account_sel1.Name = "uc_account_sel1"
    Me.uc_account_sel1.SearchTrackDataAsInternal = True
    Me.uc_account_sel1.ShowMassiveSearch = False
    Me.uc_account_sel1.ShowVipClients = True
    Me.uc_account_sel1.Size = New System.Drawing.Size(307, 134)
    Me.uc_account_sel1.TabIndex = 0
    Me.uc_account_sel1.Telephone = ""
    Me.uc_account_sel1.TrackData = ""
    Me.uc_account_sel1.Vip = False
    Me.uc_account_sel1.WeddingDate = New Date(CType(0, Long))
    '
    'uc_creditline_status
    '
    Me.uc_creditline_status.GroupBoxText = "xStatusList"
    Me.uc_creditline_status.Location = New System.Drawing.Point(319, 9)
    Me.uc_creditline_status.m_resize_width = 322
    Me.uc_creditline_status.multiChoice = True
    Me.uc_creditline_status.Name = "uc_creditline_status"
    Me.uc_creditline_status.SelectedIndexes = New Integer(-1) {}
    Me.uc_creditline_status.SelectedIndexesList = ""
    Me.uc_creditline_status.SelectedIndexesListLevel2 = ""
    Me.uc_creditline_status.SelectedValuesArray = New String(-1) {}
    Me.uc_creditline_status.SelectedValuesList = ""
    Me.uc_creditline_status.SelectedValuesListLevel2 = ""
    Me.uc_creditline_status.SetLevels = 2
    Me.uc_creditline_status.Size = New System.Drawing.Size(322, 144)
    Me.uc_creditline_status.TabIndex = 1
    Me.uc_creditline_status.ValuesArray = New String(-1) {}
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.uc_dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(647, 9)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(322, 75)
    Me.gb_date.TabIndex = 2
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xExpiryDate"
    '
    'uc_dtp_from
    '
    Me.uc_dtp_from.Checked = True
    Me.uc_dtp_from.IsReadOnly = False
    Me.uc_dtp_from.Location = New System.Drawing.Point(30, 27)
    Me.uc_dtp_from.Name = "uc_dtp_from"
    Me.uc_dtp_from.ShowCheckBox = True
    Me.uc_dtp_from.ShowUpDown = False
    Me.uc_dtp_from.Size = New System.Drawing.Size(232, 24)
    Me.uc_dtp_from.SufixText = "Sufix Text"
    Me.uc_dtp_from.SufixTextVisible = True
    Me.uc_dtp_from.TabIndex = 4
    Me.uc_dtp_from.TextWidth = 50
    Me.uc_dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'gb_debt
    '
    Me.gb_debt.Controls.Add(Me.ef_greater_than)
    Me.gb_debt.Controls.Add(Me.chkDebtGreaterThan)
    Me.gb_debt.Controls.Add(Me.chkWithoutDebt)
    Me.gb_debt.Controls.Add(Me.chkWithDebt)
    Me.gb_debt.Location = New System.Drawing.Point(647, 90)
    Me.gb_debt.Name = "gb_debt"
    Me.gb_debt.Size = New System.Drawing.Size(322, 75)
    Me.gb_debt.TabIndex = 3
    Me.gb_debt.TabStop = False
    Me.gb_debt.Text = "xDebt"
    '
    'ef_greater_than
    '
    Me.ef_greater_than.DoubleValue = 0.0R
    Me.ef_greater_than.Enabled = False
    Me.ef_greater_than.IntegerValue = 0
    Me.ef_greater_than.IsReadOnly = False
    Me.ef_greater_than.Location = New System.Drawing.Point(218, 16)
    Me.ef_greater_than.Name = "ef_greater_than"
    Me.ef_greater_than.PlaceHolder = Nothing
    Me.ef_greater_than.Size = New System.Drawing.Size(98, 24)
    Me.ef_greater_than.SufixText = "Sufix Text"
    Me.ef_greater_than.SufixTextVisible = True
    Me.ef_greater_than.TabIndex = 3
    Me.ef_greater_than.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_greater_than.TextValue = ""
    Me.ef_greater_than.TextWidth = 0
    Me.ef_greater_than.Value = ""
    Me.ef_greater_than.ValueForeColor = System.Drawing.Color.Blue
    '
    'chkDebtGreaterThan
    '
    Me.chkDebtGreaterThan.AutoSize = True
    Me.chkDebtGreaterThan.Enabled = False
    Me.chkDebtGreaterThan.Location = New System.Drawing.Point(103, 20)
    Me.chkDebtGreaterThan.Name = "chkDebtGreaterThan"
    Me.chkDebtGreaterThan.Size = New System.Drawing.Size(118, 17)
    Me.chkDebtGreaterThan.TabIndex = 2
    Me.chkDebtGreaterThan.Text = "chkGreaterThan"
    Me.chkDebtGreaterThan.UseVisualStyleBackColor = True
    '
    'chkWithoutDebt
    '
    Me.chkWithoutDebt.AutoSize = True
    Me.chkWithoutDebt.Location = New System.Drawing.Point(6, 43)
    Me.chkWithoutDebt.Name = "chkWithoutDebt"
    Me.chkWithoutDebt.Size = New System.Drawing.Size(103, 17)
    Me.chkWithoutDebt.TabIndex = 1
    Me.chkWithoutDebt.Text = "xWithoutDebt"
    Me.chkWithoutDebt.UseVisualStyleBackColor = True
    '
    'chkWithDebt
    '
    Me.chkWithDebt.AutoSize = True
    Me.chkWithDebt.Location = New System.Drawing.Point(6, 20)
    Me.chkWithDebt.Name = "chkWithDebt"
    Me.chkWithDebt.Size = New System.Drawing.Size(85, 17)
    Me.chkWithDebt.TabIndex = 0
    Me.chkWithDebt.Text = "xWithDebt"
    Me.chkWithDebt.UseVisualStyleBackColor = True
    '
    'uc_sites_sel
    '
    Me.uc_sites_sel.Location = New System.Drawing.Point(987, 6)
    Me.uc_sites_sel.MultiSelect = True
    Me.uc_sites_sel.Name = "uc_sites_sel"
    Me.uc_sites_sel.ShowIsoCode = False
    Me.uc_sites_sel.ShowMultisiteRow = True
    Me.uc_sites_sel.Size = New System.Drawing.Size(293, 166)
    Me.uc_sites_sel.TabIndex = 4
    Me.uc_sites_sel.Visible = False
    '
    'frm_creditline_sel
    '
    Me.ClientSize = New System.Drawing.Size(1198, 678)
    Me.Name = "frm_creditline_sel"
    Me.Text = "frm_creditline_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gb_debt.ResumeLayout(False)
    Me.gb_debt.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_account_sel1 As GUI_Controls.uc_account_sel
  Friend WithEvents uc_creditline_status As GUI_Controls.uc_checked_list
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents uc_dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents gb_debt As System.Windows.Forms.GroupBox
  Friend WithEvents chkDebtGreaterThan As System.Windows.Forms.CheckBox
  Friend WithEvents chkWithoutDebt As System.Windows.Forms.CheckBox
  Friend WithEvents chkWithDebt As System.Windows.Forms.CheckBox
  Friend WithEvents ef_greater_than As GUI_Controls.uc_entry_field
  Friend WithEvents uc_sites_sel As GUI_Controls.uc_sites_sel

End Class
