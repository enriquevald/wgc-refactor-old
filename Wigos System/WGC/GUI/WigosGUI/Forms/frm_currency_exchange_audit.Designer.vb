<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_currency_exchange_audit
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.fra_date = New System.Windows.Forms.GroupBox()
    Me.dt_date_to = New GUI_Controls.uc_date_picker()
    Me.dt_date_from = New GUI_Controls.uc_date_picker()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.fra_date.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.fra_date)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(995, 101)
    Me.panel_filter.Controls.SetChildIndex(Me.fra_date, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 105)
    Me.panel_data.Size = New System.Drawing.Size(995, 446)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(989, 19)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(989, 4)
    '
    'fra_date
    '
    Me.fra_date.Controls.Add(Me.dt_date_to)
    Me.fra_date.Controls.Add(Me.dt_date_from)
    Me.fra_date.Location = New System.Drawing.Point(21, 6)
    Me.fra_date.Name = "fra_date"
    Me.fra_date.Size = New System.Drawing.Size(240, 80)
    Me.fra_date.TabIndex = 0
    Me.fra_date.TabStop = False
    Me.fra_date.Text = "fra_date"
    '
    'dt_date_to
    '
    Me.dt_date_to.Checked = True
    Me.dt_date_to.IsReadOnly = False
    Me.dt_date_to.Location = New System.Drawing.Point(2, 48)
    Me.dt_date_to.Name = "dt_date_to"
    Me.dt_date_to.ShowCheckBox = True
    Me.dt_date_to.ShowUpDown = False
    Me.dt_date_to.Size = New System.Drawing.Size(230, 25)
    Me.dt_date_to.SufixText = "Sufix Text"
    Me.dt_date_to.SufixTextVisible = True
    Me.dt_date_to.TabIndex = 1
    Me.dt_date_to.TextWidth = 50
    Me.dt_date_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dt_date_from
    '
    Me.dt_date_from.Checked = True
    Me.dt_date_from.IsReadOnly = False
    Me.dt_date_from.Location = New System.Drawing.Point(1, 17)
    Me.dt_date_from.Name = "dt_date_from"
    Me.dt_date_from.ShowCheckBox = False
    Me.dt_date_from.ShowUpDown = False
    Me.dt_date_from.Size = New System.Drawing.Size(230, 25)
    Me.dt_date_from.SufixText = "Sufix Text"
    Me.dt_date_from.SufixTextVisible = True
    Me.dt_date_from.TabIndex = 0
    Me.dt_date_from.TextWidth = 50
    Me.dt_date_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'frm_currency_exchange_audit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1005, 555)
    Me.Name = "frm_currency_exchange_audit"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_currency_exchange_audit"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.fra_date.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents fra_date As System.Windows.Forms.GroupBox
  Friend WithEvents dt_date_to As GUI_Controls.uc_date_picker
  Friend WithEvents dt_date_from As GUI_Controls.uc_date_picker
End Class
