'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_account_flags.vb
' DESCRIPTION:   Account Flags selection form
' AUTHOR:        Xavier Cots
' CREATION DATE: 13-SEP-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 13-SEP-2012  XCD    Initial version.
' 17-SEP-2012  ANG    Expand Initial version.
' 12-FEB-2013  JMM    uc_account_sel.ValidateFormat added on FilterCheck.
' 24-APR-2013  SMN    Hide Player Tracking data if exists an External Loyalty.
' 10-JUN-2013  DHA    Fixed Bug #836: added colored flag column in Excel.
' 03-JUL-2013  QMP    Added alignment to flag filter name column.
' 21-AUG-2013  JMM    Fixed Defect WIG-141: Several filters errors.
' 16-SEP-2013  CCG    Added account massive search in the query.
' 26-SEP-2013  CCG    Added Multitype massive search.
' 23-OCT-2013  CCG    Added GroupBy and Subtotal functions to the grid results.
' 24-OCT-2013  CCG    Fixed Bug WIG-309: Some errors related with GroupBy and Subtotal.
' 28-OCT-2013  CCG    Fixed Bug WIG-325 and WIG-326: Unhandled exception and total column as 0.
' 28-OCT-2013  JAB    Modified function "GUI_GetSqlCommand". Used by promotion edit form.
' 29-OCT-2013  JAB    To make the form modal.
' 07-NOV-2013  DLL    Fixed Defect WIG-395: filter errors when filter in promotions
' 16-DEC-2013  RMS    In TITO Mode account id of virtual accounts are hidden
' 14-MAR-2014  DHA    Fixed Bug #WIG-707: Error opening screen from promotions edit
' 14-MAR-2014  DHA    Fixed Bug #WIG-736: Wrong values in columns 'Active', 'Expired' and 'Cancelled'
' 06-MAY-2014  JBP    Added Vip client filter at reports.
' 27-JUN-2014  LEM & RCI    When card is recycled, the trackdata is shown empty and must be CardNumber.INVALID_TRACKDATA.
' 28-JAN-2015  DCS    In TITO mode anonymous accounts don't displayed
' 21-APR-2017  ETP    WIGOS-706: Junkets - Technical story - Refactor FlagType Enum to common
' 18-JUL-2017  RGR    Fixed Bug 28830:WIGOS-3720 Flags - Database error when the user proceed with a "Search" in the screen "Reporte de banderas por cuenta"
' 10-AUG-2017  AMF    Bug 29303:[WIGOS-4083] Flag report shows the internal flag ID
'--------------------------------------------------------------------

Option Strict Off
Option Explicit On

#Region " Imports "

Imports System.Data.OleDb
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Text
Imports System.Data.SqlClient
Imports WSI.Common


#End Region

Public Class frm_account_flags
  Inherits GUI_Controls.frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub
  Friend WithEvents uc_account_sel1 As GUI_Controls.uc_account_sel
  Friend WithEvents gb_flag As System.Windows.Forms.GroupBox
  Friend WithEvents dg_select_flag As GUI_Controls.uc_grid
  Friend WithEvents gbGroup As System.Windows.Forms.GroupBox
  Friend WithEvents gbStatus As System.Windows.Forms.GroupBox
  Friend WithEvents opt_AccountFlag As System.Windows.Forms.RadioButton
  Friend WithEvents opt_FlagAccount As System.Windows.Forms.RadioButton
  Friend WithEvents chk_status_cancelled As System.Windows.Forms.CheckBox
  Friend WithEvents chk_status_expired As System.Windows.Forms.CheckBox
  Friend WithEvents chk_status_active As System.Windows.Forms.CheckBox
  Friend WithEvents gb_level As System.Windows.Forms.GroupBox
  Friend WithEvents chk_level_04 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_03 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_02 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_01 As System.Windows.Forms.CheckBox
  Friend WithEvents uc_assigned_date As GUI_Controls.uc_date_picker
  Friend WithEvents btn_select_none_flags As System.Windows.Forms.Button
  Friend WithEvents btn_select_all_flags As System.Windows.Forms.Button
  Friend WithEvents chk_only_anonymous As System.Windows.Forms.CheckBox
  Friend WithEvents chk_show_all_accounts As System.Windows.Forms.CheckBox
  Friend WithEvents gb_assigned_date As System.Windows.Forms.GroupBox
  Friend WithEvents uc_assigned_date_until As GUI_Controls.uc_date_picker
  Friend WithEvents opt_flag_not_include_any As System.Windows.Forms.RadioButton
  Friend WithEvents opt_flag_include_any As System.Windows.Forms.RadioButton
  Friend WithEvents chk_ShowSubtotalByAccountFlag As System.Windows.Forms.CheckBox
  Friend WithEvents chk_GroupByFlag As System.Windows.Forms.CheckBox
  Friend WithEvents gb_type As System.Windows.Forms.GroupBox
  Friend WithEvents chk_automatic As System.Windows.Forms.CheckBox
  Friend WithEvents chk_manual As System.Windows.Forms.CheckBox

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.uc_account_sel1 = New GUI_Controls.uc_account_sel()
    Me.gb_flag = New System.Windows.Forms.GroupBox()
    Me.opt_flag_not_include_any = New System.Windows.Forms.RadioButton()
    Me.opt_flag_include_any = New System.Windows.Forms.RadioButton()
    Me.btn_select_none_flags = New System.Windows.Forms.Button()
    Me.btn_select_all_flags = New System.Windows.Forms.Button()
    Me.dg_select_flag = New GUI_Controls.uc_grid()
    Me.gbStatus = New System.Windows.Forms.GroupBox()
    Me.chk_status_cancelled = New System.Windows.Forms.CheckBox()
    Me.chk_status_expired = New System.Windows.Forms.CheckBox()
    Me.chk_status_active = New System.Windows.Forms.CheckBox()
    Me.gbGroup = New System.Windows.Forms.GroupBox()
    Me.chk_show_all_accounts = New System.Windows.Forms.CheckBox()
    Me.opt_AccountFlag = New System.Windows.Forms.RadioButton()
    Me.opt_FlagAccount = New System.Windows.Forms.RadioButton()
    Me.gb_level = New System.Windows.Forms.GroupBox()
    Me.chk_level_04 = New System.Windows.Forms.CheckBox()
    Me.chk_level_03 = New System.Windows.Forms.CheckBox()
    Me.chk_level_02 = New System.Windows.Forms.CheckBox()
    Me.chk_level_01 = New System.Windows.Forms.CheckBox()
    Me.uc_assigned_date = New GUI_Controls.uc_date_picker()
    Me.chk_only_anonymous = New System.Windows.Forms.CheckBox()
    Me.gb_assigned_date = New System.Windows.Forms.GroupBox()
    Me.uc_assigned_date_until = New GUI_Controls.uc_date_picker()
    Me.chk_ShowSubtotalByAccountFlag = New System.Windows.Forms.CheckBox()
    Me.chk_GroupByFlag = New System.Windows.Forms.CheckBox()
    Me.gb_type = New System.Windows.Forms.GroupBox()
    Me.chk_automatic = New System.Windows.Forms.CheckBox()
    Me.chk_manual = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_flag.SuspendLayout()
    Me.gbStatus.SuspendLayout()
    Me.gbGroup.SuspendLayout()
    Me.gb_level.SuspendLayout()
    Me.gb_assigned_date.SuspendLayout()
    Me.gb_type.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_type)
    Me.panel_filter.Controls.Add(Me.chk_GroupByFlag)
    Me.panel_filter.Controls.Add(Me.chk_ShowSubtotalByAccountFlag)
    Me.panel_filter.Controls.Add(Me.gb_assigned_date)
    Me.panel_filter.Controls.Add(Me.chk_only_anonymous)
    Me.panel_filter.Controls.Add(Me.gb_level)
    Me.panel_filter.Controls.Add(Me.gbGroup)
    Me.panel_filter.Controls.Add(Me.gbStatus)
    Me.panel_filter.Controls.Add(Me.gb_flag)
    Me.panel_filter.Controls.Add(Me.uc_account_sel1)
    Me.panel_filter.Size = New System.Drawing.Size(1229, 218)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account_sel1, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_flag, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gbStatus, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gbGroup, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_level, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_only_anonymous, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_assigned_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_ShowSubtotalByAccountFlag, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_GroupByFlag, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_type, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 222)
    Me.panel_data.Size = New System.Drawing.Size(1229, 496)
    Me.panel_data.TabIndex = 3
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1223, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1223, 4)
    '
    'uc_account_sel1
    '
    Me.uc_account_sel1.Account = ""
    Me.uc_account_sel1.AccountText = ""
    Me.uc_account_sel1.Anon = False
    Me.uc_account_sel1.AutoSize = True
    Me.uc_account_sel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_account_sel1.BirthDate = New Date(CType(0, Long))
    Me.uc_account_sel1.DisabledHolder = False
    Me.uc_account_sel1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_account_sel1.Holder = ""
    Me.uc_account_sel1.Location = New System.Drawing.Point(646, 3)
    Me.uc_account_sel1.MassiveSearchNumbers = ""
    Me.uc_account_sel1.MassiveSearchNumbersToEdit = ""
    Me.uc_account_sel1.MassiveSearchType = 0
    Me.uc_account_sel1.Name = "uc_account_sel1"
    Me.uc_account_sel1.SearchTrackDataAsInternal = True
    Me.uc_account_sel1.ShowMassiveSearch = False
    Me.uc_account_sel1.ShowVipClients = True
    Me.uc_account_sel1.Size = New System.Drawing.Size(307, 134)
    Me.uc_account_sel1.TabIndex = 4
    Me.uc_account_sel1.Telephone = ""
    Me.uc_account_sel1.TrackData = ""
    Me.uc_account_sel1.Vip = False
    Me.uc_account_sel1.WeddingDate = New Date(CType(0, Long))
    '
    'gb_flag
    '
    Me.gb_flag.Controls.Add(Me.opt_flag_not_include_any)
    Me.gb_flag.Controls.Add(Me.opt_flag_include_any)
    Me.gb_flag.Controls.Add(Me.btn_select_none_flags)
    Me.gb_flag.Controls.Add(Me.btn_select_all_flags)
    Me.gb_flag.Controls.Add(Me.dg_select_flag)
    Me.gb_flag.Location = New System.Drawing.Point(377, 6)
    Me.gb_flag.Name = "gb_flag"
    Me.gb_flag.Size = New System.Drawing.Size(267, 157)
    Me.gb_flag.TabIndex = 3
    Me.gb_flag.TabStop = False
    Me.gb_flag.Text = "#17286"
    '
    'opt_flag_not_include_any
    '
    Me.opt_flag_not_include_any.Location = New System.Drawing.Point(114, 15)
    Me.opt_flag_not_include_any.Name = "opt_flag_not_include_any"
    Me.opt_flag_not_include_any.Size = New System.Drawing.Size(133, 17)
    Me.opt_flag_not_include_any.TabIndex = 1
    Me.opt_flag_not_include_any.Text = "xNo incluya ninguna"
    Me.opt_flag_not_include_any.UseVisualStyleBackColor = True
    '
    'opt_flag_include_any
    '
    Me.opt_flag_include_any.Checked = True
    Me.opt_flag_include_any.Location = New System.Drawing.Point(8, 15)
    Me.opt_flag_include_any.Name = "opt_flag_include_any"
    Me.opt_flag_include_any.Size = New System.Drawing.Size(115, 17)
    Me.opt_flag_include_any.TabIndex = 0
    Me.opt_flag_include_any.TabStop = True
    Me.opt_flag_include_any.Text = "xIncluya alguna"
    Me.opt_flag_include_any.UseVisualStyleBackColor = True
    '
    'btn_select_none_flags
    '
    Me.btn_select_none_flags.Location = New System.Drawing.Point(135, 121)
    Me.btn_select_none_flags.Name = "btn_select_none_flags"
    Me.btn_select_none_flags.Size = New System.Drawing.Size(125, 30)
    Me.btn_select_none_flags.TabIndex = 4
    Me.btn_select_none_flags.Text = "btn_select_none"
    Me.btn_select_none_flags.UseVisualStyleBackColor = True
    '
    'btn_select_all_flags
    '
    Me.btn_select_all_flags.Location = New System.Drawing.Point(6, 121)
    Me.btn_select_all_flags.Name = "btn_select_all_flags"
    Me.btn_select_all_flags.Size = New System.Drawing.Size(125, 30)
    Me.btn_select_all_flags.TabIndex = 3
    Me.btn_select_all_flags.Text = "xSelectAll"
    Me.btn_select_all_flags.UseVisualStyleBackColor = True
    '
    'dg_select_flag
    '
    Me.dg_select_flag.CurrentCol = -1
    Me.dg_select_flag.CurrentRow = -1
    Me.dg_select_flag.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_select_flag.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_select_flag.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_select_flag.Location = New System.Drawing.Point(6, 36)
    Me.dg_select_flag.Name = "dg_select_flag"
    Me.dg_select_flag.PanelRightVisible = False
    Me.dg_select_flag.Redraw = True
    Me.dg_select_flag.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_select_flag.Size = New System.Drawing.Size(255, 84)
    Me.dg_select_flag.Sortable = False
    Me.dg_select_flag.SortAscending = True
    Me.dg_select_flag.SortByCol = 0
    Me.dg_select_flag.TabIndex = 2
    Me.dg_select_flag.ToolTipped = True
    Me.dg_select_flag.TopRow = -2
    Me.dg_select_flag.WordWrap = False
    '
    'gbStatus
    '
    Me.gbStatus.Controls.Add(Me.chk_status_cancelled)
    Me.gbStatus.Controls.Add(Me.chk_status_expired)
    Me.gbStatus.Controls.Add(Me.chk_status_active)
    Me.gbStatus.Location = New System.Drawing.Point(6, 6)
    Me.gbStatus.Name = "gbStatus"
    Me.gbStatus.Size = New System.Drawing.Size(118, 88)
    Me.gbStatus.TabIndex = 0
    Me.gbStatus.TabStop = False
    Me.gbStatus.Text = "xEstado"
    '
    'chk_status_cancelled
    '
    Me.chk_status_cancelled.AutoSize = True
    Me.chk_status_cancelled.Location = New System.Drawing.Point(11, 62)
    Me.chk_status_cancelled.Name = "chk_status_cancelled"
    Me.chk_status_cancelled.Size = New System.Drawing.Size(99, 17)
    Me.chk_status_cancelled.TabIndex = 2
    Me.chk_status_cancelled.Text = "xCanceladas"
    Me.chk_status_cancelled.UseVisualStyleBackColor = True
    '
    'chk_status_expired
    '
    Me.chk_status_expired.AutoSize = True
    Me.chk_status_expired.Location = New System.Drawing.Point(11, 41)
    Me.chk_status_expired.Name = "chk_status_expired"
    Me.chk_status_expired.Size = New System.Drawing.Size(89, 17)
    Me.chk_status_expired.TabIndex = 1
    Me.chk_status_expired.Text = "xExpiradas"
    Me.chk_status_expired.UseVisualStyleBackColor = True
    '
    'chk_status_active
    '
    Me.chk_status_active.AutoSize = True
    Me.chk_status_active.Location = New System.Drawing.Point(11, 20)
    Me.chk_status_active.Name = "chk_status_active"
    Me.chk_status_active.Size = New System.Drawing.Size(74, 17)
    Me.chk_status_active.TabIndex = 0
    Me.chk_status_active.Text = "xActivas"
    Me.chk_status_active.UseVisualStyleBackColor = True
    '
    'gbGroup
    '
    Me.gbGroup.Controls.Add(Me.chk_show_all_accounts)
    Me.gbGroup.Controls.Add(Me.opt_AccountFlag)
    Me.gbGroup.Controls.Add(Me.opt_FlagAccount)
    Me.gbGroup.Location = New System.Drawing.Point(6, 95)
    Me.gbGroup.Name = "gbGroup"
    Me.gbGroup.Size = New System.Drawing.Size(365, 68)
    Me.gbGroup.TabIndex = 2
    Me.gbGroup.TabStop = False
    Me.gbGroup.Text = "xAgrupaci�n"
    '
    'chk_show_all_accounts
    '
    Me.chk_show_all_accounts.AutoSize = True
    Me.chk_show_all_accounts.Location = New System.Drawing.Point(9, 41)
    Me.chk_show_all_accounts.Name = "chk_show_all_accounts"
    Me.chk_show_all_accounts.Size = New System.Drawing.Size(179, 17)
    Me.chk_show_all_accounts.TabIndex = 1
    Me.chk_show_all_accounts.Text = "xMostrar todas las cuentas"
    Me.chk_show_all_accounts.UseVisualStyleBackColor = True
    '
    'opt_AccountFlag
    '
    Me.opt_AccountFlag.AutoSize = True
    Me.opt_AccountFlag.Location = New System.Drawing.Point(10, 18)
    Me.opt_AccountFlag.Name = "opt_AccountFlag"
    Me.opt_AccountFlag.Size = New System.Drawing.Size(113, 17)
    Me.opt_AccountFlag.TabIndex = 0
    Me.opt_AccountFlag.TabStop = True
    Me.opt_AccountFlag.Text = "xAccount'n'Flag"
    Me.opt_AccountFlag.UseVisualStyleBackColor = True
    '
    'opt_FlagAccount
    '
    Me.opt_FlagAccount.AutoSize = True
    Me.opt_FlagAccount.Location = New System.Drawing.Point(175, 18)
    Me.opt_FlagAccount.Name = "opt_FlagAccount"
    Me.opt_FlagAccount.Size = New System.Drawing.Size(112, 17)
    Me.opt_FlagAccount.TabIndex = 2
    Me.opt_FlagAccount.TabStop = True
    Me.opt_FlagAccount.Text = "xFlag'n'Account"
    Me.opt_FlagAccount.UseVisualStyleBackColor = True
    '
    'gb_level
    '
    Me.gb_level.Controls.Add(Me.chk_level_04)
    Me.gb_level.Controls.Add(Me.chk_level_03)
    Me.gb_level.Controls.Add(Me.chk_level_02)
    Me.gb_level.Controls.Add(Me.chk_level_01)
    Me.gb_level.Location = New System.Drawing.Point(958, 6)
    Me.gb_level.Name = "gb_level"
    Me.gb_level.Size = New System.Drawing.Size(124, 100)
    Me.gb_level.TabIndex = 6
    Me.gb_level.TabStop = False
    Me.gb_level.Text = "xLevel"
    '
    'chk_level_04
    '
    Me.chk_level_04.AutoSize = True
    Me.chk_level_04.Location = New System.Drawing.Point(11, 19)
    Me.chk_level_04.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_level_04.Name = "chk_level_04"
    Me.chk_level_04.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_04.TabIndex = 0
    Me.chk_level_04.Text = "xLevel 04"
    Me.chk_level_04.UseVisualStyleBackColor = True
    '
    'chk_level_03
    '
    Me.chk_level_03.AutoSize = True
    Me.chk_level_03.Location = New System.Drawing.Point(11, 36)
    Me.chk_level_03.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_level_03.Name = "chk_level_03"
    Me.chk_level_03.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_03.TabIndex = 1
    Me.chk_level_03.Text = "xLevel 03"
    Me.chk_level_03.UseVisualStyleBackColor = True
    '
    'chk_level_02
    '
    Me.chk_level_02.AutoSize = True
    Me.chk_level_02.Location = New System.Drawing.Point(11, 54)
    Me.chk_level_02.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_level_02.Name = "chk_level_02"
    Me.chk_level_02.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_02.TabIndex = 2
    Me.chk_level_02.Text = "xLevel 02"
    Me.chk_level_02.UseVisualStyleBackColor = True
    '
    'chk_level_01
    '
    Me.chk_level_01.AutoSize = True
    Me.chk_level_01.Location = New System.Drawing.Point(11, 71)
    Me.chk_level_01.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_level_01.Name = "chk_level_01"
    Me.chk_level_01.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_01.TabIndex = 3
    Me.chk_level_01.Text = "xLevel 01"
    Me.chk_level_01.UseVisualStyleBackColor = True
    '
    'uc_assigned_date
    '
    Me.uc_assigned_date.Checked = True
    Me.uc_assigned_date.IsReadOnly = False
    Me.uc_assigned_date.Location = New System.Drawing.Point(6, 23)
    Me.uc_assigned_date.Name = "uc_assigned_date"
    Me.uc_assigned_date.ShowCheckBox = True
    Me.uc_assigned_date.ShowUpDown = False
    Me.uc_assigned_date.Size = New System.Drawing.Size(230, 24)
    Me.uc_assigned_date.SufixText = "Sufix Text"
    Me.uc_assigned_date.SufixTextVisible = True
    Me.uc_assigned_date.TabIndex = 0
    Me.uc_assigned_date.TextWidth = 40
    Me.uc_assigned_date.Value = New Date(2012, 9, 20, 16, 39, 0, 93)
    '
    'chk_only_anonymous
    '
    Me.chk_only_anonymous.AutoSize = True
    Me.chk_only_anonymous.Location = New System.Drawing.Point(969, 109)
    Me.chk_only_anonymous.Name = "chk_only_anonymous"
    Me.chk_only_anonymous.Size = New System.Drawing.Size(129, 17)
    Me.chk_only_anonymous.TabIndex = 7
    Me.chk_only_anonymous.Text = "xOnly anonymous"
    Me.chk_only_anonymous.UseVisualStyleBackColor = True
    '
    'gb_assigned_date
    '
    Me.gb_assigned_date.Controls.Add(Me.uc_assigned_date_until)
    Me.gb_assigned_date.Controls.Add(Me.uc_assigned_date)
    Me.gb_assigned_date.Location = New System.Drawing.Point(130, 6)
    Me.gb_assigned_date.Name = "gb_assigned_date"
    Me.gb_assigned_date.Size = New System.Drawing.Size(241, 88)
    Me.gb_assigned_date.TabIndex = 1
    Me.gb_assigned_date.TabStop = False
    Me.gb_assigned_date.Text = "xFecha asignacion"
    '
    'uc_assigned_date_until
    '
    Me.uc_assigned_date_until.Checked = True
    Me.uc_assigned_date_until.IsReadOnly = False
    Me.uc_assigned_date_until.Location = New System.Drawing.Point(6, 51)
    Me.uc_assigned_date_until.Name = "uc_assigned_date_until"
    Me.uc_assigned_date_until.ShowCheckBox = True
    Me.uc_assigned_date_until.ShowUpDown = False
    Me.uc_assigned_date_until.Size = New System.Drawing.Size(230, 24)
    Me.uc_assigned_date_until.SufixText = "Sufix Text"
    Me.uc_assigned_date_until.SufixTextVisible = True
    Me.uc_assigned_date_until.TabIndex = 1
    Me.uc_assigned_date_until.TextWidth = 40
    Me.uc_assigned_date_until.Value = New Date(2012, 9, 20, 16, 39, 0, 93)
    '
    'chk_ShowSubtotalByAccountFlag
    '
    Me.chk_ShowSubtotalByAccountFlag.AutoSize = True
    Me.chk_ShowSubtotalByAccountFlag.Location = New System.Drawing.Point(15, 195)
    Me.chk_ShowSubtotalByAccountFlag.Name = "chk_ShowSubtotalByAccountFlag"
    Me.chk_ShowSubtotalByAccountFlag.Size = New System.Drawing.Size(80, 17)
    Me.chk_ShowSubtotalByAccountFlag.TabIndex = 9
    Me.chk_ShowSubtotalByAccountFlag.Text = "xSubtotal"
    Me.chk_ShowSubtotalByAccountFlag.UseVisualStyleBackColor = True
    '
    'chk_GroupByFlag
    '
    Me.chk_GroupByFlag.AutoSize = True
    Me.chk_GroupByFlag.Location = New System.Drawing.Point(15, 172)
    Me.chk_GroupByFlag.Name = "chk_GroupByFlag"
    Me.chk_GroupByFlag.Size = New System.Drawing.Size(88, 17)
    Me.chk_GroupByFlag.TabIndex = 8
    Me.chk_GroupByFlag.Text = "xAgrupado"
    Me.chk_GroupByFlag.UseVisualStyleBackColor = True
    '
    'gb_type
    '
    Me.gb_type.Controls.Add(Me.chk_automatic)
    Me.gb_type.Controls.Add(Me.chk_manual)
    Me.gb_type.Location = New System.Drawing.Point(649, 136)
    Me.gb_type.Name = "gb_type"
    Me.gb_type.Size = New System.Drawing.Size(304, 66)
    Me.gb_type.TabIndex = 5
    Me.gb_type.TabStop = False
    Me.gb_type.Text = "xType"
    '
    'chk_automatic
    '
    Me.chk_automatic.AutoSize = True
    Me.chk_automatic.Location = New System.Drawing.Point(11, 41)
    Me.chk_automatic.Name = "chk_automatic"
    Me.chk_automatic.Size = New System.Drawing.Size(90, 17)
    Me.chk_automatic.TabIndex = 1
    Me.chk_automatic.Text = "xAutomatic"
    Me.chk_automatic.UseVisualStyleBackColor = True
    '
    'chk_manual
    '
    Me.chk_manual.AutoSize = True
    Me.chk_manual.Location = New System.Drawing.Point(11, 18)
    Me.chk_manual.Name = "chk_manual"
    Me.chk_manual.Size = New System.Drawing.Size(73, 17)
    Me.chk_manual.TabIndex = 0
    Me.chk_manual.Text = "xManual"
    Me.chk_manual.UseVisualStyleBackColor = True
    '
    'frm_account_flags
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(1237, 722)
    Me.Name = "frm_account_flags"
    Me.ShowInTaskbar = False
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_flag.ResumeLayout(False)
    Me.gbStatus.ResumeLayout(False)
    Me.gbStatus.PerformLayout()
    Me.gbGroup.ResumeLayout(False)
    Me.gbGroup.PerformLayout()
    Me.gb_level.ResumeLayout(False)
    Me.gb_level.PerformLayout()
    Me.gb_assigned_date.ResumeLayout(False)
    Me.gb_type.ResumeLayout(False)
    Me.gb_type.PerformLayout()
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Constants "

  ' Grid Rows

  Private Const MODULE_NAME As String = "FRM_ACCOUNT_FLAGS"

  Private Const GRID_COLUMN_AF_UNIQUE_ID As Integer = 0
  Private GRID_COLUMN_ACCOUNT_ID As Integer = 1
  Private GRID_COLUMN_TRACK_DATA As Integer = 2
  Private GRID_COLUMN_HOLDER_NAME As Integer = 3
  Private GRID_COLUMN_HOLDER_LEVEL As Integer = 4
  Private GRID_COLUMN_FLAG_COLOR As Integer = 5
  Private GRID_COLUMN_FLAG_NAME As Integer = 6
  Private GRID_COLUMN_FLAG_TYPE As Integer = 7
  Private Const GRID_COLUMN_VALID_FROM As Integer = 8
  Private Const GRID_COLUMN_VALID_TO As Integer = 9
  Private Const GRID_COLUMN_FLAG_STATUS As Integer = 10
  Private Const GRID_COLUMN_AC_ADDED_BY As Integer = 11
  Private Const GRID_COLUMN_PM_ADDED_BY As Integer = 12
  Private Const GRID_COLUMN_ADDED_DATETIME As Integer = 13
  Private Const GRID_COLUMN_FLAG_ID As Integer = 14
  Private Const GRID_COLUMN_CANCELLED_DATE_TIME As Integer = 15
  Private Const GRID_COLUMN_CANCELLED_USER_ID As Integer = 16
  Private GRID_COLUMN_FLAG_AMOUNT As Integer = 7
  Private GRID_COLUMN_FLAG_ACTIVE As Integer = 8
  Private GRID_COLUMN_FLAG_EXPIRED As Integer = 9
  Private GRID_COLUMN_FLAG_CANCELLED As Integer = 10


  Private Const SQL_COLUMN_AF_UNIQUE_ID As Integer = 0
  Private SQL_COLUMN_ACCOUNT_ID As Integer = 1
  Private SQL_COLUMN_TRACK_DATA As Integer = 2
  Private SQL_COLUMN_HOLDER_NAME As Integer = 3
  Private SQL_COLUMN_HOLDER_LEVEL As Integer = 4
  Private SQL_COLUMN_FLAG_COLOR As Integer = 5
  Private SQL_COLUMN_FLAG_NAME As Integer = 6
  Private Const SQL_COLUMN_VALID_FROM As Integer = 7
  Private Const SQL_COLUMN_VALID_TO As Integer = 8
  Private Const SQL_COLUMN_FLAG_STATUS As Integer = 9
  Private Const SQL_COLUMN_AC_ADDED_BY As Integer = 10
  Private Const SQL_COLUMN_PM_ADDED_BY As Integer = 11
  Private Const SQL_COLUMN_ADDED_DATETIME As Integer = 12
  Private SQL_COLUMN_FLAG_ID As Integer = 13
  Private Const SQL_COLUMN_CANCELLED_DATE_TIME As Integer = 14
  Private Const SQL_COLUMN_CANCELLED_USER_ID As Integer = 15
  Private SQL_COLUMN_FLAG_AMOUNT As Integer = 7

  Private SQL_COLUMN_ACCOUNT_TYPE As Integer = 8

  Private SQL_COLUMN_FLAG_ACTIVE As Integer = 9
  Private SQL_COLUMN_FLAG_EXPIRED As Integer = 10
  Private SQL_COLUMN_FLAG_CANCELLED As Integer = 11
  Private SQL_COLUMN_FLAG_TYPE As Integer = 17

  Private Const GRID_COLUMNS As Integer = 17
  Private Const GRID_HEADER_ROWS As Integer = 2

  ' SELECT FLAG GRID
  Private Const GRID_SELECT_FLAG_IDX As Integer = 0
  Private Const GRID_SELECT_FLAG_CHECKED As Integer = 1
  Private Const GRID_SELECT_FLAG_COLOR As Integer = 2
  Private Const GRID_SELECT_FLAG_NAME As Integer = 3
  Private Const GRID_SELECT_FLAG_COUNT As Integer = 4


  Private Const GRID_SELECT_FLAG_COLUMNS As Integer = 4
  Private Const GRID_SELECT_FLAG_HEADER_ROWS As Integer = 0

#End Region

#Region " Members "

  ' For reports
  Private m_account As String
  Private m_track_data As String
  Private m_holder As String
  Private m_holder_is_vip As String
  Private m_flag As String
  Private m_type As String
  Private m_order As String
  Private m_status As String
  Private m_assigned_date As String
  Private m_assigned_date_until As String
  Private m_level As String
  Private m_only_anonymous_accounts As Boolean
  Private m_show_all_accounts As String
  Private m_flags_to_include As String
  Private m_idx_rows_actived As Integer
  Private m_accounts_names_cache As DataTable
  Private m_flags_names_cache As DataTable
  Private m_cache_flags As DataTable

  Private m_number_of_checks As Integer

  Private m_previous_account_id As String
  Private m_prev_flag As String

  Private m_subtotal_actived_flags As Integer
  Private m_subtotal_canceled_flags As Integer
  Private m_subtotal_expired_flags As Integer

  Private m_subtotal_flag_actived_flags As Integer
  Private m_subtotal_flag_canceled_flags As Integer
  Private m_subtotal_flag_expired_flags As Integer
  Private m_procedence_promotions As Boolean = False
  Private m_first_call_from_prmotion As Boolean
  Private m_table_flag_procedence As DataTable

  Private m_is_tito_mode As Boolean

#End Region

#Region " Properties "

#End Region

#Region " Overridable GUI function "

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_ACCOUNT_FLAGS
    Call MyBase.GUI_SetFormId()

  End Sub

  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    ' FORM CAPTION
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1259)

    m_is_tito_mode = WSI.Common.TITO.Utils.IsTitoMode()

    ' BUTTONS

    ' Awards flag button

    With Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1)
      .Visible = True
      .Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1324) '"Otorgar Bandera"
      .Type = uc_button.ENUM_BUTTON_TYPE.USER
      .Size = New System.Drawing.Size(90, 45)
      GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = False
    End With

    With Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0)
      .Visible = True
      .Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1325) '"Cancelar Bandera"
      .Type = uc_button.ENUM_BUTTON_TYPE.USER
      .Size = New System.Drawing.Size(90, 45)
      .Enabled = False
    End With

    ' Cancel flag Button

    ' Cancel button
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)

    ' Excel button
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_EXCEL).Visible = True

    ' Select button
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Enabled = Me.Permissions.Write

    ' New Button
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False

    'Print button
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_PRINT).Visible = False

    ' STATUS
    Me.gbStatus.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(262) ' Status
    Me.chk_status_active.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1314)    ' ACTIVA
    Me.chk_status_expired.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1315)   ' EXPIRADA
    Me.chk_status_cancelled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1331) ' CANCELADA

    ' ORDER BY RADIO BUTTONS
    Me.gbGroup.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(792)    ' Ordenado
    Me.opt_FlagAccount.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1310)
    Me.opt_AccountFlag.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1311)

    Me.chk_show_all_accounts.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1367) ' Exclue users withou flag



    ' ASSIGNATION DATE
    Me.gb_assigned_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1307)

    Me.uc_assigned_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(297) ' desde
    Me.uc_assigned_date.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    Me.uc_assigned_date_until.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(298) ' hasta
    Me.uc_assigned_date_until.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)


    ' FLAG
    Me.gb_flag.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286) ' Bandera
    Me.btn_select_all_flags.Text = GLB_NLS_GUI_ALARMS.GetString(452)
    Me.btn_select_none_flags.Text = GLB_NLS_GUI_ALARMS.GetString(453)
    Me.opt_flag_include_any.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1376) ' Include any
    Me.opt_flag_not_include_any.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1377)  ' Not inclue any

    'LEVEL
    gb_level.Text = GLB_NLS_GUI_INVOICING.GetString(381)
    Me.chk_only_anonymous.Text = GLB_NLS_GUI_CONFIGURATION.GetString(66)       ' Only anoymous accounts
    If m_is_tito_mode Then
      Me.chk_only_anonymous.Visible = False
    End If

    chk_level_01.Text = GetCashierPlayerTrackingData("Level01.Name")
    If chk_level_01.Text = "" Then
      chk_level_01.Text = GLB_NLS_GUI_CONFIGURATION.GetString(289)
    End If
    chk_level_02.Text = GetCashierPlayerTrackingData("Level02.Name")
    If chk_level_02.Text = "" Then
      chk_level_02.Text = GLB_NLS_GUI_CONFIGURATION.GetString(290)
    End If
    chk_level_03.Text = GetCashierPlayerTrackingData("Level03.Name")
    If chk_level_03.Text = "" Then
      chk_level_03.Text = GLB_NLS_GUI_CONFIGURATION.GetString(291)
    End If
    chk_level_04.Text = GetCashierPlayerTrackingData("Level04.Name")
    If chk_level_04.Text = "" Then
      chk_level_04.Text = GLB_NLS_GUI_CONFIGURATION.GetString(332)
    End If

    Me.chk_manual.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2756) ' Manual
    Me.chk_automatic.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8020) ' Automatic

    Me.gb_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(254) ' Type

    'CCG Show Massive Search
    Me.uc_account_sel1.ShowMassiveSearch = True


    Me.m_accounts_names_cache = New DataTable()
    Me.m_flags_names_cache = New DataTable()

    Me.chk_GroupByFlag.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2815)
    Me.chk_ShowSubtotalByAccountFlag.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2816)

    Me.m_number_of_checks = 0
    Me.m_previous_account_id = -1
    Me.m_prev_flag = String.Empty
    Me.m_subtotal_actived_flags = 0
    Me.m_subtotal_canceled_flags = 0
    Me.m_subtotal_expired_flags = 0
    Me.m_subtotal_flag_actived_flags = 0
    Me.m_subtotal_flag_canceled_flags = 0
    Me.m_subtotal_flag_expired_flags = 0

    Call GUI_StyleView()
    Call InitializeGridSelectFlags()

    Call Me.SetDefaultValues()

    If m_procedence_promotions Then
      Me.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)
      m_first_call_from_prmotion = True
    End If



  End Sub

  Protected Overrides Function GUI_FilterCheck() As Boolean
    Dim _idx As Integer
    Dim _has_values As Boolean

    ' No status selected
    If (Not chk_status_active.Checked AndAlso Not chk_status_cancelled.Checked AndAlso Not chk_status_expired.Checked) Then

      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1372), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False

    End If

    ' Date from greater than date until
    If (Me.uc_assigned_date.Checked AndAlso Me.uc_assigned_date_until.Value < Me.uc_assigned_date.Value) Then

      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1365), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    ' Check fields on uc_account_sel are ok
    If Not Me.uc_account_sel1.ValidateFormat() Then
      Return False
    End If

    _has_values = False
    For _idx = 0 To Me.dg_select_flag.NumRows - 1
      If (Me.dg_select_flag.Cell(_idx, GRID_SELECT_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED) Then
        _has_values = True
        Exit For
      End If
    Next

    If Not _has_values Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1333), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Return False
    End If

    ' Any data is ok in filter fields
    Return True
  End Function 'GUI_FilterCheck

  Protected Overrides Sub GUI_FilterReset()
    SetDefaultValues()
  End Sub 'GUI_FilterReset

  ' PURPOSE : Checks out the DB row before adding it to the grid
  '              and process counters
  '
  '  PARAMS :
  '     - INPUT :
  '           - DataRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the data row can be added) or False (the data row can not be added)
  '
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As CLASS_DB_ROW) As Boolean
    Dim _idx_row As Integer
    Dim _temp_account_id As Integer
    Dim _is_subtotal_flag As Boolean

    _idx_row = Me.Grid.NumRows
    _temp_account_id = -1
    _is_subtotal_flag = False

    If Me.chk_ShowSubtotalByAccountFlag.Checked Then

      If Not DbRow.Value(SQL_COLUMN_ACCOUNT_ID) Is System.DBNull.Value Then
        _temp_account_id = DbRow.Value(SQL_COLUMN_ACCOUNT_ID)
      End If

      'If Not Me.opt_FlagAccount.Checked Then
      'Subtotal By Flag
      If _idx_row = 0 Then
        If Not DbRow.Value(SQL_COLUMN_FLAG_NAME) Is System.DBNull.Value Then
          m_prev_flag = DbRow.Value(SQL_COLUMN_FLAG_NAME)
        Else
          m_prev_flag = "---"
        End If
        If Not DbRow.Value(SQL_COLUMN_FLAG_STATUS) Is System.DBNull.Value Then
          Call Me.CheckSubtotalFlag(DbRow.Value(SQL_COLUMN_FLAG_STATUS))
        End If
      Else
        If Not Me.opt_FlagAccount.Checked Then ' Order by account and flag
          If Not Me.m_prev_flag.Equals(DbRow.Value(SQL_COLUMN_FLAG_NAME)) _
              Or Not Me.m_previous_account_id = _temp_account_id Then ' Diferent Flag or Holder

            Call Me.AddSubtotalFlagRow()

            If Not DbRow.Value(SQL_COLUMN_FLAG_STATUS) Is System.DBNull.Value Then
              Call Me.CheckSubtotalFlag(DbRow.Value(SQL_COLUMN_FLAG_STATUS))
            End If

          Else ' Calculate subtotal

            Call Me.CheckSubtotalFlag(DbRow.Value(SQL_COLUMN_FLAG_STATUS))

          End If
        Else ' Order By flag and account
          If Not Me.m_prev_flag.Equals(DbRow.Value(SQL_COLUMN_FLAG_NAME)) Then ' Diferent Flag or Holder

            _is_subtotal_flag = True

          Else ' Calculate subtotal

            Call Me.CheckSubtotalFlag(DbRow.Value(SQL_COLUMN_FLAG_STATUS))

          End If
        End If

        If Not DbRow.Value(SQL_COLUMN_FLAG_NAME) Is System.DBNull.Value Then
          m_prev_flag = DbRow.Value(SQL_COLUMN_FLAG_NAME)
        Else
          Me.m_prev_flag = String.Empty
        End If

        End If
        'Else

        'End If

        'Subtotal By Account
        If _idx_row = 0 Then
          Me.m_previous_account_id = _temp_account_id
          If Not DbRow.Value(SQL_COLUMN_FLAG_STATUS) Is System.DBNull.Value Then
            Call Me.CheckSubtotal(DbRow.Value(SQL_COLUMN_FLAG_STATUS))
          End If
        Else
        If Not Me.m_previous_account_id = _temp_account_id Then

          Call Me.AddSubtotalRow()

          If Not DbRow.Value(SQL_COLUMN_FLAG_STATUS) Is System.DBNull.Value Then
            Call Me.CheckSubtotal(DbRow.Value(SQL_COLUMN_FLAG_STATUS))
          End If

        Else ' Calculate subtotal

          Call Me.CheckSubtotal(DbRow.Value(SQL_COLUMN_FLAG_STATUS))

        End If

        If Not DbRow.Value(SQL_COLUMN_ACCOUNT_ID) Is System.DBNull.Value Then
          Me.m_previous_account_id = DbRow.Value(SQL_COLUMN_ACCOUNT_ID)
        Else
          Me.m_previous_account_id = -1
        End If
      End If
    End If

    If _is_subtotal_flag Then
      _is_subtotal_flag = False
      Call Me.AddSubtotalFlagRow()

      If Not DbRow.Value(SQL_COLUMN_FLAG_STATUS) Is System.DBNull.Value Then
        Call Me.CheckSubtotalFlag(DbRow.Value(SQL_COLUMN_FLAG_STATUS))
      End If
    End If

    Return True
  End Function ' GUI_CheckOutRowBeforeAdd

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                       ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _track_data As String
    Dim _flag_color As Color
    Dim _status As ACCOUNT_FLAG_STATUS
    Dim _str_status As String

    Call MyBase.GUI_SetupRow(RowIndex, DbRow)

    ' AF_UNIQUE_ID is null when user doesn't has assigned flag

    If (Not DbRow.IsNull(SQL_COLUMN_AF_UNIQUE_ID)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_AF_UNIQUE_ID).Value = DbRow.Value(SQL_COLUMN_AF_UNIQUE_ID)

      ' Trick to hide value in grid;)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_AF_UNIQUE_ID).ForeColor = Color.White

    End If

    ' Account id
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_ID).ToString

    ' Card Track Data
    _track_data = ""
    If Not DbRow.IsNull(SQL_COLUMN_TRACK_DATA) Then
      CardNumber.VisibleTrackData(_track_data, DbRow.Value(SQL_COLUMN_TRACK_DATA), MAGNETIC_CARD_TYPES.CARD_TYPE_PLAYER, _
                                  CType(DbRow.Value(SQL_COLUMN_ACCOUNT_TYPE), AccountType))
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TRACK_DATA).Value = _track_data
    End If

    ' Holder Name
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_NAME).Value = DbRow.Value(SQL_COLUMN_HOLDER_NAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_NAME).Value = AUDIT_NONE_STRING
    End If

    ' Player Level
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_LEVEL) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_LEVEL).Value = DbRow.Value(SQL_COLUMN_HOLDER_LEVEL)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_LEVEL).Value = AUDIT_NONE_STRING
    End If


    ' Flag Color
    If Not DbRow.IsNull(SQL_COLUMN_FLAG_COLOR) Then

      If Not (DbRow.Value(SQL_COLUMN_FLAG_COLOR).Equals(Color.Empty.ToArgb())) Then

        _flag_color = Color.FromArgb(Convert.ToInt32(DbRow.Value(SQL_COLUMN_FLAG_COLOR)))

        ' Trick that allow set cell backcolor as Black
        Call ProcessColor(_flag_color)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_FLAG_COLOR).BackColor = _flag_color
        Me.Grid.Cell(RowIndex, GRID_COLUMN_FLAG_COLOR).ForeColor = _flag_color
      End If

      Me.Grid.Cell(RowIndex, GRID_COLUMN_FLAG_COLOR).Value = String.Empty ' Ninguno
    Else
      '' NOT COLOR
      Me.Grid.Cell(RowIndex, GRID_COLUMN_FLAG_COLOR).Value = String.Empty ' Ninguno
    End If


    'Flag Name
    If Not DbRow.IsNull(SQL_COLUMN_FLAG_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_FLAG_NAME).Value = DbRow.Value(SQL_COLUMN_FLAG_NAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_FLAG_NAME).Value = AUDIT_NONE_STRING
    End If

    'Flag type
    If Not DbRow.IsNull(SQL_COLUMN_FLAG_TYPE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_FLAG_TYPE).Value = IIf(DbRow.Value(SQL_COLUMN_FLAG_TYPE).ToString = AccountFlag.FlagTypes.Manual, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2756), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8020))
    End If

    If Not Me.chk_GroupByFlag.Checked Then
      ' Valid FROM
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_VALID_FROM)) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_VALID_FROM).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_VALID_FROM), _
                                                                                             ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                             ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_VALID_FROM).Value = AUDIT_NONE_STRING
      End If

      ' Valid TO 
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_VALID_TO)) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_VALID_TO).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_VALID_TO), _
                                                                                             ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                             ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_VALID_TO).Value = AUDIT_NONE_STRING
      End If


      ' Flag Status

      If Not IsDBNull(DbRow.Value(SQL_COLUMN_FLAG_STATUS)) Then

        _status = DbRow.Value(SQL_COLUMN_FLAG_STATUS)
        _str_status = String.Empty

        Select Case _status
          Case ACCOUNT_FLAG_STATUS.ACTIVE
            _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1314)
            m_idx_rows_actived += 1
          Case ACCOUNT_FLAG_STATUS.CANCELLED
            _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1331)
          Case ACCOUNT_FLAG_STATUS.EXPIRED
            _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1315)
          Case ACCOUNT_FLAG_STATUS.NO_ACTIVE
            _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1330)
        End Select

        Me.Grid.Cell(RowIndex, GRID_COLUMN_FLAG_STATUS).Value = _str_status
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_FLAG_STATUS).Value = AUDIT_NONE_STRING
      End If

      ' Added By Account

      If Not IsDBNull(DbRow.Value(SQL_COLUMN_AC_ADDED_BY)) Then

        Me.Grid.Cell(RowIndex, GRID_COLUMN_AC_ADDED_BY).Value = DbRow.Value(SQL_COLUMN_AC_ADDED_BY).ToString()
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_AC_ADDED_BY).Value = AUDIT_NONE_STRING
      End If


      ' Added By Promotion

      If Not IsDBNull(DbRow.Value(GRID_COLUMN_PM_ADDED_BY)) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PM_ADDED_BY).Value = DbRow.Value(SQL_COLUMN_PM_ADDED_BY).ToString()
      Else

        Me.Grid.Cell(RowIndex, GRID_COLUMN_PM_ADDED_BY).Value = AUDIT_NONE_STRING
      End If


      ' Added DateTime
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_ADDED_DATETIME)) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_ADDED_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_ADDED_DATETIME), _
                                                                                             ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                             ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_ADDED_DATETIME).Value = AUDIT_NONE_STRING
      End If


      ' Flag Id

      If Not IsDBNull(DbRow.Value(SQL_COLUMN_FLAG_ID)) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_FLAG_ID).Value = DbRow.Value(SQL_COLUMN_FLAG_ID)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_FLAG_ID).Value = AUDIT_NONE_STRING
      End If


      ' Cancelled datetime
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_CANCELLED_DATE_TIME)) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CANCELLED_DATE_TIME).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_CANCELLED_DATE_TIME), _
                                                                                             ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                             ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CANCELLED_DATE_TIME).Value = AUDIT_NONE_STRING
      End If

      ' Cancelled user
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_CANCELLED_USER_ID)) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CANCELLED_USER_ID).Value = DbRow.Value(SQL_COLUMN_CANCELLED_USER_ID)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CANCELLED_USER_ID).Value = AUDIT_NONE_STRING
      End If
    End If

    'Amount
    If Me.chk_GroupByFlag.Checked Then

      If Not IsDBNull(DbRow.Value(SQL_COLUMN_FLAG_AMOUNT)) Then
        If Not DbRow.Value(SQL_COLUMN_FLAG_ID) Is System.DBNull.Value Then ' Check if this account have a flag
          If m_procedence_promotions Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_FLAG_AMOUNT).Value = DbRow.Value(SQL_COLUMN_FLAG_ACTIVE)
          Else
            Me.Grid.Cell(RowIndex, GRID_COLUMN_FLAG_AMOUNT).Value = DbRow.Value(SQL_COLUMN_FLAG_AMOUNT)
          End If
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_FLAG_AMOUNT).Value = 0
        End If
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_FLAG_AMOUNT).Value = AUDIT_NONE_STRING
      End If

      'Status flag
      If Me.m_number_of_checks >= 2 Then ' Only when there are more than one check checked
        If Me.chk_status_active.Checked Then
          If Not IsDBNull(DbRow.Value(SQL_COLUMN_FLAG_ACTIVE)) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_FLAG_ACTIVE).Value = DbRow.Value(SQL_COLUMN_FLAG_ACTIVE)
          Else
            Me.Grid.Cell(RowIndex, GRID_COLUMN_FLAG_ACTIVE).Value = AUDIT_NONE_STRING
          End If
        End If

        If Me.chk_status_expired.Checked Then
          If Not IsDBNull(DbRow.Value(SQL_COLUMN_FLAG_EXPIRED)) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_FLAG_EXPIRED).Value = DbRow.Value(SQL_COLUMN_FLAG_EXPIRED)
          Else
            Me.Grid.Cell(RowIndex, GRID_COLUMN_FLAG_EXPIRED).Value = AUDIT_NONE_STRING
          End If
        End If

        If Me.chk_status_cancelled.Checked Then
          If Not IsDBNull(DbRow.Value(SQL_COLUMN_FLAG_CANCELLED)) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_FLAG_CANCELLED).Value = DbRow.Value(SQL_COLUMN_FLAG_CANCELLED)
          Else
            Me.Grid.Cell(RowIndex, GRID_COLUMN_FLAG_CANCELLED).Value = AUDIT_NONE_STRING
          End If
        End If
      End If

    End If

    Return True
  End Function


  Protected Overrides Sub GUI_BeforeFirstRow()
    m_idx_rows_actived = 0
  End Sub
  Protected Overrides Sub GUI_AfterLastRow()

    Dim _cancel_flag_button As uc_button
    Dim _assign_flag_button As uc_button

    _cancel_flag_button = GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0)
    _assign_flag_button = GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1)

    _cancel_flag_button.Enabled = (m_idx_rows_actived > 0 AndAlso Me.Permissions.Delete)
    _assign_flag_button.Enabled = (Me.Grid.NumRows <> 0) AndAlso Me.Permissions.Write

    If Not Me.opt_FlagAccount.Checked Then
      Call Me.AddSubtotalFlagRow()
      Call Me.AddSubtotalRow()
    Else
      Call Me.AddSubtotalRow()
      Call Me.AddSubtotalFlagRow()
    End If

    

    m_subtotal_flag_actived_flags = 0
    m_subtotal_flag_canceled_flags = 0
    m_subtotal_flag_expired_flags = 0
    m_subtotal_actived_flags = 0
    m_subtotal_canceled_flags = 0
    m_subtotal_expired_flags = 0

  End Sub

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Dim _confirmation_result As ENUM_MB_RESULT
    Dim _selected_rows() As Int32
    Dim _processed_flags As Int32
    Dim _selected_flags_ids() As Int32
    Dim _selected_account_ids() As Int64
    Dim _idx As Int32
    Dim _id_sure_award_flag As Int32

    _selected_rows = Me.Grid.SelectedRows

    Select Case ButtonId

      Case ENUM_BUTTON.BUTTON_CUSTOM_0 ' UNASSIGN

        ReDim _selected_flags_ids(1)

        If (_selected_rows Is Nothing OrElse _selected_rows.Length < 1) Then

          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1364), ENUM_MB_TYPE.MB_TYPE_WARNING)

          Return
        End If

        Call CancelAccountFlag(_selected_rows, Grid)

        Call FillFlags(Me.dg_select_flag)

      Case ENUM_BUTTON.BUTTON_CUSTOM_1 ' ASSIGN

        If (_selected_rows Is Nothing OrElse _selected_rows.Length < 1) Then
          'MsgBox("xSelecciona almenos una cuenta ")
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1368), ENUM_MB_TYPE.MB_TYPE_WARNING)

          Return
        End If

        'Check if is only one row selected and is a subtotal row
        If _selected_rows.Length = 1 Then
          If Me.opt_AccountFlag.Checked Then
            If Grid.Cell(_selected_rows(0), GRID_COLUMN_HOLDER_NAME).Value.Contains(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1046)) Then
              'MsgBox("xSelecciona almenos una cuenta ")
              Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1368), ENUM_MB_TYPE.MB_TYPE_WARNING)

              Return
            End If
          ElseIf Me.opt_FlagAccount.Checked Then
            If Grid.Cell(_selected_rows(0), GRID_COLUMN_ACCOUNT_ID).Value.Contains(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1046)) Then
              'MsgBox("xSelecciona almenos una cuenta ")
              Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1368), ENUM_MB_TYPE.MB_TYPE_WARNING)

              Return
            End If
          End If
        End If


        ' Get flag Ids
        _selected_flags_ids = GetIdsFromSelectFormFlags()

        If (_selected_flags_ids Is Nothing OrElse _selected_flags_ids.Length < 1) Then
          Return
        End If


        _id_sure_award_flag = IIf(_selected_flags_ids.Length = 1 _
                              , GLB_NLS_GUI_PLAYER_TRACKING.Id(1354) _
                              , GLB_NLS_GUI_PLAYER_TRACKING.Id(1419))

        ' Are you sure to Assign flag/s?
        _confirmation_result = NLS_MsgBox(_id_sure_award_flag, _
                            ENUM_MB_TYPE.MB_TYPE_INFO, _
                            ENUM_MB_BTN.MB_BTN_YES_NO)

        If (_confirmation_result <> ENUM_MB_RESULT.MB_RESULT_YES) Then
          Return
        End If

        ' Get user Ids
        ReDim _selected_account_ids(_selected_rows.Length() - 1)
        _idx = 0
        For Each _idx_row As Int32 In _selected_rows
          ' Check if the selected rows are accounts, not subtotals rows
          If Me.opt_AccountFlag.Checked Then
            If Not Grid.Cell(_idx_row, GRID_COLUMN_HOLDER_NAME).Value.Contains(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1046)) Then
              _selected_account_ids(_idx) = Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_ID).Value
              _idx = _idx + 1
            End If
          ElseIf Me.opt_FlagAccount.Checked Then
            If Not Grid.Cell(_idx_row, GRID_COLUMN_TRACK_DATA).Value.Contains(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1046)) Then
              _selected_account_ids(_idx) = Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_ID).Value
              _idx = _idx + 1
            End If
          End If
        Next

        ' Sort Ids to assign process
        System.Array.Sort(_selected_account_ids)

        _processed_flags = AssignAccountsFlags(_selected_account_ids _
                                , _selected_flags_ids)

        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1380), _
                         ENUM_MB_TYPE.MB_TYPE_INFO, _
                         ENUM_MB_BTN.MB_BTN_OK, _
                         ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                         _processed_flags)

        Call FillFlags(Me.dg_select_flag)

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select




  End Sub 'GUI_ButtonClick


  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_COMMAND
  End Function

  Protected Overrides Function GUI_GetSqlCommand() As SqlCommand


    Dim _idx_flags_list As StringBuilder
    Dim _str_bld_af As StringBuilder
    Dim _str_sql As StringBuilder
    Dim _account_filter As String
    Dim _selected_account_level As List(Of Int32)
    Dim _idx As Int32
    Dim _show_only_anonymous_accounts As Boolean
    Dim _sql_cmd As SqlCommand
    Dim _list_flag_status As List(Of ACCOUNT_FLAG_STATUS)
    Dim _inner_type As String
    Dim _show_all_accounts As Boolean
    Dim _tmp_str As String

    _sql_cmd = New SqlCommand()
    _str_sql = New StringBuilder()
    _idx_flags_list = New StringBuilder()
    _list_flag_status = New List(Of ACCOUNT_FLAG_STATUS)
    _selected_account_level = New List(Of Int32)
    _show_only_anonymous_accounts = False
    _show_all_accounts = False
    _account_filter = ""

    _show_all_accounts = Me.chk_show_all_accounts.Enabled AndAlso Me.chk_show_all_accounts.Checked

    ' CCG Create and insert account massive search data
    If Not String.IsNullOrEmpty(Me.uc_account_sel1.MassiveSearchNumbers) Then
      _str_sql.AppendLine(Me.uc_account_sel1.CreateAndInsertAccountData())
    End If

    _str_sql.AppendLine("SELECT   AF.AF_UNIQUE_ID                                                                                      ")
    _str_sql.AppendLine("       , A.AC_ACCOUNT_ID                                                                                      ")
    _str_sql.AppendLine("       , A.AC_TRACK_DATA															                                                         ")
    _str_sql.AppendLine("       , A.AC_HOLDER_NAME  -- USER ID								                                                         ")
    _str_sql.AppendLine("       , (  SELECT   GP_KEY_VALUE 																	            		                           ")
    _str_sql.AppendLine("              FROM   GENERAL_PARAMS 																            		                           ")
    _str_sql.AppendLine("             WHERE   GP_GROUP_KEY   = 'PlayerTracking' 							            		                         ")
    _str_sql.AppendLine("               AND   GP_SUBJECT_KEY = 'Level' + RIGHT('0' + CAST(AC_HOLDER_LEVEL AS NVARCHAR), 2) + '.Name'	 ")
    _str_sql.AppendLine("       ) AS   AC_LEVEL																					                                               ")
    _str_sql.AppendLine("       , FL_COLOR                                                                                             ")
    _str_sql.AppendLine("       , FL_NAME                                                                                              ")
    _str_sql.AppendLine("       , AF_VALID_FROM																                                                         ")
    _str_sql.AppendLine("       , AF_VALID_TO                                                                                          ")
    _str_sql.AppendLine("       , AF_STATUS																		                                                         ")

    If Me.chk_GroupByFlag.Checked Then
      _str_sql.AppendLine("       , CASE WHEN AF.AF_STATUS = 0 THEN 1 ELSE 0 END AS ACTIVE                                             ")
      _str_sql.AppendLine("       , CASE WHEN AF.AF_STATUS = 2 THEN 1 ELSE 0 END AS EXPIRED																		         ")
      _str_sql.AppendLine("       , CASE WHEN AF.AF_STATUS = 3 THEN 1 ELSE 0 END AS CANCELLED																		       ")
    End If

    _str_sql.AppendLine("       , G.GU_USERNAME                                         						                                   ")
    _str_sql.AppendLine("       , PM_NAME				    -- Promo Name												                                               ")
    _str_sql.AppendLine("       , AF_ADDED_DATETIME                                                                                    ")
    _str_sql.AppendLine("       , AF_FLAG_ID																		                                                       ")
    _str_sql.AppendLine("       , AF_CANCELLED_DATETIME                                                                                ")
    _str_sql.AppendLine("       , GC.GU_USERNAME  AS CANCELLER -- Canceller user	                                                     ")

    _str_sql.AppendLine("       , A.AC_TYPE                                                                                            ")
    _str_sql.AppendLine("       , F.FL_TYPE                                                                                            ")

    If Me.chk_GroupByFlag.Checked Then
      _str_sql.AppendLine(" INTO #TEMP_TOTAL                                                                                           ")
    End If

    _str_sql.AppendLine("  FROM   ACCOUNTS A                                                                                           ")


    _inner_type = IIf(_show_all_accounts, "LEFT   OUTER JOIN", "INNER JOIN")

    _str_bld_af = New StringBuilder()

    _str_bld_af.AppendLine("SELECT   AF_UNIQUE_ID              ")
    _str_bld_af.AppendLine("       , AF_ACCOUNT_ID             ")
    _str_bld_af.AppendLine("       , AF_FLAG_ID                ")
    _str_bld_af.AppendLine("       , AF_STATUS                 ")
    _str_bld_af.AppendLine("       , AF_VALID_FROM             ")
    _str_bld_af.AppendLine("       , AF_VALID_TO               ")
    _str_bld_af.AppendLine("       , AF_ADDED_DATETIME         ")
    _str_bld_af.AppendLine("       , AF_ADDED_BY_USER_ID       ")
    _str_bld_af.AppendLine("       , AF_ADDED_BY_PROMOTION_ID  ")
    _str_bld_af.AppendLine("       , AF_CANCELLED_DATETIME     ")
    _str_bld_af.AppendLine("       , AF_CANCELLED_USER_ID      ")
    _str_bld_af.AppendLine("  FROM   ACCOUNT_FLAGS             ")
    _str_bld_af.AppendLine(" WHERE   1=1                       ")
    _str_bld_af.AppendLine(GetSqlStatusFilter())
    _str_bld_af.AppendLine(GetSqlAssignedDateFilter())
    _str_bld_af.AppendLine(GetSqlFlagFilter())


    _str_sql.AppendFormat("{0} ({1}) AF ON AF.AF_ACCOUNT_ID = A.AC_ACCOUNT_ID " _
                          , _inner_type _
                          , _str_bld_af.ToString() _
                          ).AppendLine()

    _str_sql.AppendLine("LEFT   OUTER JOIN PROMOTIONS P ON AF.AF_ADDED_BY_PROMOTION_ID = P.PM_PROMOTION_ID                             ")
    _str_sql.AppendLine("LEFT	  OUTER JOIN GUI_USERS G ON AF.AF_ADDED_BY_USER_ID = G.GU_USER_ID                                        ")
    _str_sql.AppendLine("LEFT	  OUTER JOIN GUI_USERS GC ON AF.AF_CANCELLED_USER_ID = GC.GU_USER_ID                                     ")
    _str_sql.AppendLine("LEFT   OUTER JOIN FLAGS F ON F.FL_FLAG_ID = AF.AF_FLAG_ID                                                     ")

    ' CCG Create Inner Join for account massive search
    If Not String.IsNullOrEmpty(Me.uc_account_sel1.MassiveSearchNumbers) Then
      If Me.uc_account_sel1.MassiveSearchType = GUI_Controls.uc_account_sel.ENUM_MASSIVE_SEARCH_TYPE.ID_ACCOUNT Then
        _str_sql.AppendLine(Me.uc_account_sel1.GetInnerJoinAccountMassiveSearch("A.AC_ACCOUNT_ID"))
      Else
        _str_sql.AppendLine(Me.uc_account_sel1.GetInnerJoinAccountMassiveSearch("A.AC_TRACK_DATA"))
      End If

    End If

    _str_sql.AppendLine(" WHERE   AC_TYPE = @pAccountType                                                                              ")

    ' Type
    If chk_manual.Checked() And Not chk_automatic.Checked() Then
      _str_sql.AppendLine(" AND F.FL_TYPE = " & AccountFlag.FlagTypes.Manual)
    End If

    If chk_automatic.Checked() And Not chk_manual.Checked() Then
      _str_sql.AppendLine(" AND F.FL_TYPE = " & AccountFlag.FlagTypes.Automatic)
    End If

    ' Get user filter
    _account_filter = Me.uc_account_sel1.GetFilterSQL()

    If (_account_filter <> String.Empty) Then

      'Litle Trick ;)
      _account_filter = _account_filter.Replace("AC_ACCOUNT_ID", "A.AC_ACCOUNT_ID")
      _account_filter = _account_filter.Replace("AC_TRACK_DATA", "A.AC_TRACK_DATA")
      _account_filter = _account_filter.Replace("AC_HOLDER_NAME", "A.AC_HOLDER_NAME")

      _str_sql.AppendLine(" AND " & _account_filter)

    End If

    If Not Me.uc_account_sel1.DisabledHolder Then

      ' Get account type filter
      _show_only_anonymous_accounts = Me.chk_only_anonymous.Checked
      If (_show_only_anonymous_accounts) Then
        _str_sql.Append(" AND AC_HOLDER_LEVEL = 0 ")
      End If

      ' Gets level filter
      If (Me.gb_level.Enabled) Then

        For Each _ctr As Control In Me.gb_level.Controls

          If TypeOf _ctr Is CheckBox AndAlso CType(_ctr, CheckBox).Checked Then

            Select Case _ctr.Text
              Case Me.chk_level_01.Text
                _selected_account_level.Add(1)
              Case Me.chk_level_02.Text
                _selected_account_level.Add(2)
              Case Me.chk_level_03.Text
                _selected_account_level.Add(3)
              Case Me.chk_level_04.Text
                _selected_account_level.Add(4)
            End Select
          End If
        Next

        If (_selected_account_level.Count > 0) Then
          _str_sql.Append("AND   AC_HOLDER_LEVEL IN (")
          For _idx = 0 To _selected_account_level.Count - 1
            _str_sql.Append(_selected_account_level(_idx))
            If (_idx < _selected_account_level.Count - 1) Then
              _str_sql.Append(", ")
            End If
          Next
          _str_sql.Append(") ")
        End If

      End If

    End If


    If m_procedence_promotions Then
      _str_sql.AppendLine("  SELECT   AC_ACCOUNT_ID                                                                         ")
      _str_sql.AppendLine("         , AC_TRACK_DATA                                                                         ")
      _str_sql.AppendLine("         , AC_HOLDER_NAME                                                                        ")
      _str_sql.AppendLine("         , AC_LEVEL                                                                              ")

      _tmp_str = ""
      For _idx = 0 To Me.dg_select_flag.NumRows - 1
        If (Me.dg_select_flag.Cell(_idx, GRID_SELECT_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED) Then
          _tmp_str = ",["
          _tmp_str += Me.dg_select_flag.Cell(_idx, GRID_SELECT_FLAG_IDX).Value.ToString()
          _tmp_str += "] AS '"
          _tmp_str += Me.dg_select_flag.Cell(_idx, GRID_SELECT_FLAG_IDX).Value.ToString()
          _tmp_str += "'"

          _str_sql.AppendLine(_tmp_str)
        End If
      Next

      _str_sql.AppendLine("     INTO   #TEMP_TOTAL2                                                                          ")
      _str_sql.AppendLine("     FROM (                                                                                       ")
      _str_sql.AppendLine("   SELECT   AC_ACCOUNT_ID                                                                         ")
      _str_sql.AppendLine("          , AC_TRACK_DATA                                                                         ")
      _str_sql.AppendLine("          , AC_HOLDER_NAME                                                                        ")
      _str_sql.AppendLine("          , AC_LEVEL                                                                              ")
      _str_sql.AppendLine("          , AF_FLAG_ID                                                                            ")
      _str_sql.AppendLine("          , AC_TYPE                                                                               ")
      _str_sql.AppendLine("          , SUM(ISNULL(ACTIVE,0)) AS ACTIVE                                                       ")
      _str_sql.AppendLine("    FROM    #TEMP_TOTAL                                                                           ")
      _str_sql.AppendLine("GROUP BY   AC_ACCOUNT_ID                                                                          ")
      _str_sql.AppendLine("          , AF_FLAG_ID                                                                            ")
      _str_sql.AppendLine("          , AC_TRACK_DATA                                                                         ")
      _str_sql.AppendLine("          , AC_HOLDER_NAME                                                                        ")
      _str_sql.AppendLine("          , AC_LEVEL                                                                              ")
      _str_sql.AppendLine("          , AF_FLAG_ID --,FL_COLOR,FL_NAME                                                        ")
      _str_sql.AppendLine("          , AC_TYPE                                                                               ")
      _str_sql.AppendLine("          ) AS BYFLAG                                                                             ")
      _str_sql.AppendLine("          PIVOT(SUM(ACTIVE) FOR AF_FLAG_ID IN (                                                   ")

      _tmp_str = ""
      For _idx = 0 To Me.dg_select_flag.NumRows - 1
        If (Me.dg_select_flag.Cell(_idx, GRID_SELECT_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED) Then
          If Not String.IsNullOrEmpty(_tmp_str) Then
            _tmp_str += ", "
          End If
          _tmp_str += "[" & Me.dg_select_flag.Cell(_idx, GRID_SELECT_FLAG_IDX).Value.ToString() & "]"
        End If
      Next

      If Not String.IsNullOrEmpty(_tmp_str) Then
        _str_sql.AppendLine(_tmp_str)
      End If

      _str_sql.AppendLine("          )) AS pvt                                                                              ")
      _str_sql.AppendLine("  WHERE                                                                                          ")

      _tmp_str = ""
      For _idx = 0 To Me.dg_select_flag.NumRows - 1
        If (Me.dg_select_flag.Cell(_idx, GRID_SELECT_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED) Then
          If Not String.IsNullOrEmpty(_tmp_str) Then
            _tmp_str += " AND "
          End If
          _tmp_str += "ISNULL([" & Me.dg_select_flag.Cell(_idx, GRID_SELECT_FLAG_IDX).Value.ToString() & "],0)"
          _tmp_str += " >= "
          ' TODO: cambiar por GRID_SELECT_FLAG_AMOUNT cuando este la parte de xavi
          _tmp_str += Me.dg_select_flag.Cell(_idx, GRID_SELECT_FLAG_COUNT).Value.ToString()
          '_tmp_str += Me.dg_select_flag.Cell(_idx, GRID_SELECT_FLAG_IDX).Value.ToString()
        End If
      Next

      If Not String.IsNullOrEmpty(_tmp_str) Then
        _str_sql.AppendLine(_tmp_str)
      End If

      _str_sql.AppendLine("     SELECT   #TEMP_TOTAL.AC_ACCOUNT_ID                                                         ")
      _str_sql.AppendLine("            , #TEMP_TOTAL.AC_TRACK_DATA                                                         ")
      _str_sql.AppendLine("            , #TEMP_TOTAL.AC_HOLDER_NAME                                                        ")
      _str_sql.AppendLine("            , #TEMP_TOTAL.AC_LEVEL                                                              ")
      _str_sql.AppendLine("            , AF_FLAG_ID                                                                        ")
      _str_sql.AppendLine("            , FL_COLOR                                                                          ")
      _str_sql.AppendLine("            , FL_NAME                                                                           ")
      _str_sql.AppendLine("            , COUNT(*) AS FL_TOTAL                                                              ")
      _str_sql.AppendLine("            , AC_TYPE                                                                           ")
      _str_sql.AppendLine("            , SUM(ACTIVE) AS ACTIVE                                                             ")
      _str_sql.AppendLine("            , FL_TYPE                                                                           ")
      _str_sql.AppendLine("       FROM   #TEMP_TOTAL                                                                       ")
      _str_sql.AppendLine(" INNER JOIN   #TEMP_TOTAL2 ON #TEMP_TOTAL.AC_ACCOUNT_ID = #TEMP_TOTAL2.AC_ACCOUNT_ID            ")
      _str_sql.AppendLine("   GROUP BY	 #TEMP_TOTAL.AC_ACCOUNT_ID                                                         ")
      _str_sql.AppendLine("            , #TEMP_TOTAL.AC_TRACK_DATA                                                         ")
      _str_sql.AppendLine("            , #TEMP_TOTAL.AC_HOLDER_NAME                                                        ")
      _str_sql.AppendLine("            , #TEMP_TOTAL.AC_LEVEL, AF_FLAG_ID                                                  ")
      _str_sql.AppendLine("            , FL_COLOR,FL_NAME                                                                  ")
      _str_sql.AppendLine("            , AC_TYPE                                                                           ")
      _str_sql.AppendLine("            , FL_TYPE                                                                           ")

    Else
      If Me.chk_GroupByFlag.Checked Then
        _str_sql.AppendLine("SELECT *                                                                                       ")
        _str_sql.AppendLine("FROM                                                                                           ")
        _str_sql.AppendLine("(SELECT    AC_ACCOUNT_ID                                                                       ")
        _str_sql.AppendLine("          ,AC_TRACK_DATA                                                                       ")
        _str_sql.AppendLine("          ,AC_HOLDER_NAME                                                                      ")
        _str_sql.AppendLine("          ,AC_LEVEL                                                                            ")
        _str_sql.AppendLine("          ,AF_FLAG_ID                                                                          ")
        _str_sql.AppendLine("          ,FL_COLOR                                                                            ")
        _str_sql.AppendLine("          ,FL_NAME                                                                             ")
        _str_sql.AppendLine("          ,COUNT(*) AS FL_TOTAL                                                                ")

        _str_sql.AppendLine("          ,AC_TYPE                                                                             ")

        If Me.chk_status_active.Checked Then
          _str_sql.AppendLine("         ,SUM(ACTIVE) AS ACTIVE                                                              ")
        End If

        If Me.chk_status_expired.Checked Then
          _str_sql.AppendLine("   ,SUM(EXPIRED) AS EXPIRED                                                                  ")
        End If

        If Me.chk_status_cancelled.Checked Then
          _str_sql.AppendLine("   ,SUM(CANCELLED) AS CANCELLED                                                              ")
        End If

        _str_sql.AppendLine("            , FL_TYPE                                                                           ")

        _str_sql.AppendLine("FROM #TEMP_TOTAL                                                                               ")
        _str_sql.AppendLine("GROUP BY AC_ACCOUNT_ID,AC_TRACK_DATA,AC_HOLDER_NAME,AC_LEVEL,AF_FLAG_ID,FL_COLOR,FL_NAME,AC_TYPE,FL_TYPE ")
        _str_sql.AppendLine(") AS BYFLAG                                                                                    ")
      End If

    End If

    If m_procedence_promotions Then
      If (Me.opt_AccountFlag.Checked) Then
        _str_sql.AppendLine("   ORDER BY   #TEMP_TOTAL.AC_LEVEL, #TEMP_TOTAL.AC_ACCOUNT_ID, FL_NAME                         ")
      ElseIf (Me.opt_FlagAccount.Checked) Then
        _str_sql.AppendLine("   ORDER BY   FL_NAME, #TEMP_TOTAL.AC_LEVEL, #TEMP_TOTAL.AC_ACCOUNT_ID                         ")
      End If
    Else
      If (Me.opt_AccountFlag.Checked) Then
        _str_sql.AppendLine("ORDER BY AC_LEVEL,AC_ACCOUNT_ID,FL_NAME")
      ElseIf (Me.opt_FlagAccount.Checked) Then
        _str_sql.AppendLine("ORDER BY FL_NAME,AC_LEVEL,AC_ACCOUNT_ID")
      End If
    End If

    ' CCG Delete temporary table for account massive search
    If Not String.IsNullOrEmpty(Me.uc_account_sel1.MassiveSearchNumbers) Then
      _str_sql.AppendLine(Me.uc_account_sel1.DropTableAccountMassiveSearch())
    End If

    If Me.chk_GroupByFlag.Checked Then
      _str_sql.AppendLine(" DROP TABLE #TEMP_TOTAL")
    End If

    If m_procedence_promotions Then
      _str_sql.AppendLine(" DROP TABLE #TEMP_TOTAL2")
    End If

    _sql_cmd.CommandText = _str_sql.ToString()

    _sql_cmd.Parameters.Add("@pAccountType", SqlDbType.Int).Value = AccountType.ACCOUNT_WIN

    If (Me.uc_assigned_date.Checked) Then
      _sql_cmd.Parameters.Add("@pAddedDay", SqlDbType.DateTime).Value = Me.uc_assigned_date.Value
    End If

    If (Me.uc_assigned_date_until.Checked) Then
      _sql_cmd.Parameters.Add("@pAddedDayUntil", SqlDbType.DateTime).Value = Me.uc_assigned_date_until.Value
    End If

    Call GUI_StyleView()

    Return _sql_cmd

  End Function

  Private Function GetSqlFlagFilter() As String

    Dim _str_sql As StringBuilder
    Dim _selected_flags_id As List(Of Int32)
    Dim _flags_in_list As Boolean
    Dim _idx As Int32
    Dim _idx_flags_list As StringBuilder

    _str_sql = New StringBuilder()
    _selected_flags_id = New List(Of Int32)
    _idx_flags_list = New StringBuilder()


    _selected_flags_id = GetSelectedFlagsId()
    _flags_in_list = opt_flag_include_any.Checked

    If (_selected_flags_id.Count > 0) Then
      For _idx = 0 To _selected_flags_id.Count - 1
        _idx_flags_list.Append(_selected_flags_id(_idx))
        If (_idx < (_selected_flags_id.Count - 1)) Then
          _idx_flags_list.Append(",  ")
        End If
      Next
    End If

    If _idx_flags_list.ToString() <> String.Empty Then

      If (_flags_in_list) Then
        _str_sql.AppendFormat(" AND ( AF_FLAG_ID IN ( {0} )", _idx_flags_list.ToString())
      Else
        _str_sql.AppendFormat(" AND ( AF_FLAG_ID NOT IN ( {0} )", _idx_flags_list.ToString())
      End If

      'If (_show_all_accounts) Then
      '  _str_sql.AppendLine(" OR AF_FLAG_ID IS NULL ")
      'End If

      _str_sql.AppendLine(" ) ")
    ElseIf (Not Me.opt_flag_not_include_any.Checked) Then
      _str_sql.AppendLine(" AND AF_FLAG_ID IS NULL ")
    End If


    Return _str_sql.ToString()

  End Function

  Private Function GetSqlAssignedDateFilter() As String
    ' Assigned Date Filter
    Dim _str_sql As StringBuilder
    _str_sql = New StringBuilder()

    If (Me.uc_assigned_date.Checked) Then
      _str_sql.AppendLine("AND  AF_ADDED_DATETIME >= @pAddedDay                 ")
    End If

    If (Me.uc_assigned_date_until.Checked) Then
      _str_sql.AppendLine(" AND  AF_ADDED_DATETIME <= @pAddedDayUntil")
    End If

    Return _str_sql.ToString()

  End Function
  Private Function GetSqlStatusFilter() As String
    ' Gets Status Filter

    Dim _list_flag_status As List(Of ACCOUNT_FLAG_STATUS)
    Dim _str_sql As StringBuilder
    Dim _idx As Int32

    _str_sql = New StringBuilder()

    _list_flag_status = New List(Of ACCOUNT_FLAG_STATUS)

    If (Me.gbStatus.Enabled) Then

      For Each _ctr As Control In Me.gbStatus.Controls

        If TypeOf _ctr Is CheckBox AndAlso CType(_ctr, CheckBox).Checked Then

          Select Case _ctr.Text
            Case Me.chk_status_active.Text
              _list_flag_status.Add(ACCOUNT_FLAG_STATUS.ACTIVE)
            Case Me.chk_status_cancelled.Text
              _list_flag_status.Add(ACCOUNT_FLAG_STATUS.CANCELLED)
            Case Me.chk_status_expired.Text
              _list_flag_status.Add(ACCOUNT_FLAG_STATUS.EXPIRED)
          End Select
        End If
      Next

      If (_list_flag_status.Count > 0) Then
        '_str_sql.Append("AND (  AF_STATUS IN (")
        _str_sql.Append("AND  AF_STATUS IN (")
        For _idx = 0 To _list_flag_status.Count - 1
          _str_sql.Append(_list_flag_status(_idx))
          If (_idx < _list_flag_status.Count - 1) Then
            _str_sql.Append(", ")
          End If
        Next
        _str_sql.Append(") ")


        'If (_show_all_accounts) Then
        '  _str_sql.AppendLine(" OR AF_STATUS IS NULL")
        'End If
        '_str_sql.Append(") ")

      End If

    End If

    Return _str_sql.ToString()

  End Function

#Region " GUI Reports "

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    Dim _value As String

    ' ORDER FlagAccount / AccountFlag
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(792), m_order)    ' Orden

    ' FLAG
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1090), m_status)  ' Estado

    ' Flag ( Include any , not include any )
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1418), m_flags_to_include)

    ' Flag/s Name/s
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286), m_flag)    ' Bandera

    m_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353) ' All

    If chk_manual.Checked() And Not chk_automatic.Checked() Then
      m_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2756) ' Manual
    End If

    If chk_automatic.Checked() And Not chk_manual.Checked() Then
      m_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8020) ' Automatic
    End If

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(254), m_type)    ' Tipo bandera

    ' ACCOUNT
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), m_account)      ' Cuenta
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_track_data)   ' Tarjeta
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), m_holder)       ' Titular
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4802), m_holder_is_vip)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(381), m_level)        ' Nivel

    ' Fecha Asignaci�n desde
    If (m_assigned_date <> String.Empty) Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1307, GLB_NLS_GUI_PLAYER_TRACKING.GetString(297)), m_assigned_date)

    Else
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1307, GLB_NLS_GUI_PLAYER_TRACKING.GetString(297)), String.Empty)
    End If

    If (m_assigned_date_until <> String.Empty) Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1307, GLB_NLS_GUI_PLAYER_TRACKING.GetString(298)), m_assigned_date_until)

    Else
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1307, GLB_NLS_GUI_PLAYER_TRACKING.GetString(298)), String.Empty)
    End If

    ' Show all accounts
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1367), m_show_all_accounts)

    'Only anonymous accounts
    _value = IIf(m_only_anonymous_accounts, GLB_NLS_GUI_PLAYER_TRACKING.GetString(698), GLB_NLS_GUI_PLAYER_TRACKING.GetString(699))
    If Not m_is_tito_mode Then
      PrintData.SetFilter(GLB_NLS_GUI_CONFIGURATION.GetString(66), _value)
    End If

    ' Options: GroupBy and ShowSubtotal
    If Me.chk_GroupByFlag.Checked Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1277), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2815)) ' Agrupado por bandera
    ElseIf Me.chk_ShowSubtotalByAccountFlag.Checked Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1277), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2816)) ' Mostrar subtotal por cuenta y bandera
    Else
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1277), String.Empty)
    End If

  End Sub

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)
    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Params.Title = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1259)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_PORTRAIT

  End Sub

  Protected Overrides Sub GUI_ReportUpdateFilters()


    m_status = String.Empty
    m_flag = String.Empty
    m_account = String.Empty
    m_track_data = String.Empty
    m_holder_is_vip = String.Empty
    m_holder = String.Empty
    m_order = String.Empty
    m_level = String.Empty
    m_assigned_date = String.Empty
    m_assigned_date_until = String.Empty
    m_show_all_accounts = String.Empty
    m_flags_to_include = String.Empty

    ' Estado
    If (Me.gbStatus.Enabled) Then
      m_status = GetCaptionsFromSelectedCheckedBoxes(Me.gbStatus)
    End If

    If (m_status = String.Empty) Then
      m_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353) ' ALL
    End If

    ' Bandera
    m_flag = GetCaptionFromSelectedRadioButton(Me.gb_flag)
    m_flag = GetSelectedFlagsTitles()


    ' Cuenta
    m_account = Me.uc_account_sel1.Account()
    m_track_data = Me.uc_account_sel1.TrackData()
    m_holder = Me.uc_account_sel1.Holder()
    m_holder_is_vip = Me.uc_account_sel1.HolderIsVip

    ' Order By
    If (Me.gbGroup.Enabled) Then
      m_order = GetCaptionFromSelectedRadioButton(Me.gbGroup)
    End If

    ' Nivel
    If (Me.gb_level.Enabled) Then
      m_level = GetCaptionsFromSelectedCheckedBoxes(Me.gb_level)
    End If

    If (m_level = String.Empty) Then
      m_level = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353) ' ALL
    End If

    ' Assignation date ( from )
    If (Me.uc_assigned_date.Checked) Then
      m_assigned_date = GUI_FormatDate(Me.uc_assigned_date.Value _
                                      , ENUM_FORMAT_DATE.FORMAT_DATE_SHORT _
                                      , ENUM_FORMAT_TIME.FORMAT_HHMM)

    Else
      m_assigned_date = String.Empty
    End If

    ' Assignation date ( until ) 
    If (Me.uc_assigned_date_until.Checked) Then
      m_assigned_date_until = GUI_FormatDate(Me.uc_assigned_date_until.Value _
                                      , ENUM_FORMAT_DATE.FORMAT_DATE_SHORT _
                                      , ENUM_FORMAT_TIME.FORMAT_HHMM)

    Else
      m_assigned_date_until = String.Empty
    End If

    ' Excluir usuarios sin bandera
    If (Me.chk_show_all_accounts.Enabled) Then

      If (Not (Me.chk_show_all_accounts.Enabled AndAlso Not Me.chk_show_all_accounts.Checked)) Then
        ' Si
        m_show_all_accounts = GLB_NLS_GUI_PLAYER_TRACKING.GetString(698)
      Else
        ' No
        m_show_all_accounts = GLB_NLS_GUI_PLAYER_TRACKING.GetString(699)
      End If
    End If


    If (Me.opt_flag_include_any.Checked) Then
      m_flags_to_include = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1376)
    ElseIf (Me.opt_flag_not_include_any.Checked) Then
      m_flags_to_include = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1377)
    End If


    ' S�lo cuentas an�nimas
    Me.m_only_anonymous_accounts = Me.chk_only_anonymous.Checked

  End Sub

#End Region ' GUI Reports


#End Region

#Region " Private Functions "

  Private Function AssignAccountsFlags(ByRef AccountId() As Int64 _
                                     , ByRef FlagId() As Int32) As Int32

    Dim _data As DataTable
    Dim _current_date As Date

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _str_holder_name As String
    Dim _str_flag_name As String
    Dim _last_account_id As Int64

    ' Init values
    _last_account_id = Int64.MinValue
    _data = AccountFlag.CreateAccountFlagsTable()

    ' Fill datatable
    For Each _account As Int64 In AccountId
      If _last_account_id <> _account _
          And _account <> 0 Then

        _last_account_id = _account
        _current_date = GUI_GetDateTime()

        ' Add all flags selected
        For Each _flag_id As Int32 In FlagId
          AccountFlag.AddFlagToAccount(_last_account_id _
                                      , _flag_id _
                                      , ACCOUNT_FLAG_STATUS.ACTIVE _
                                      , CurrentUser.Id _
                                      , -1 _
                                      , _data _
                                      , False)

        Next
      End If
    Next

    ' Insert data in database and audit changes
    If (AccountFlag.InsertAccountFlags(_data)) Then
      ''' AUDIT CHANGES
      _last_account_id = Int64.MinValue

      ' Get Holder Name in cache to improve performance
      Call FillAccountsNamesCache()

      ' Get Flag Name in cache to improve performance
      Call FillFlagNamesCache()

      For Each _account As Int64 In AccountId
        If _last_account_id <> _account _
            And _account <> 0 Then

          _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_ACCOUNT)
          _last_account_id = _account
          _str_holder_name = GetHolderName(_last_account_id)

          ' Accounts | Flag Assignation to Account %id - %Name
          _auditor_data.SetName(GLB_NLS_GUI_STATISTICS.Id(207) _
                          , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1381, _last_account_id.ToString(), _str_holder_name))

          For Each _flag_id As Int64 In FlagId
            _str_flag_name = GetFlagName(_flag_id)   ' Flag = flag_name
            _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1286), _str_flag_name) ' Flag = flag_name
          Next

          _auditor_data.Notify(GLB_CurrentUser.GuiId, _
                 GLB_CurrentUser.Id, _
                 GLB_CurrentUser.Name, _
                 CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, _
                 0)

          _auditor_data = Nothing
        End If
      Next
    Else
      Return 0 ' No changes in database
    End If

    Return _data.Rows.Count()

  End Function


  Private Sub CancelAccountFlag(ByRef _selected_rows() As Int32 _
                                    , ByRef Grid As uc_grid)

    Dim _tb_account_flags_input As DataTable
    Dim _tb_account_flags_output As DataTable
    Dim _account_flag_row As DataRow
    Dim _tb_view_sorted As DataView
    Dim _confirmation_result As ENUM_MB_RESULT
    Dim _id_sure_cancel_flag As Int32
    Dim _auditor_data As CLASS_AUDITOR_DATA

    Dim _str_holder_name As String
    Dim _str_flag_name As String
    Dim _last_account_id As Int64

    Dim _idx_rows_cancelled As Int64



    _tb_account_flags_input = AccountFlag.CreateAccountFlagsTable()
    _tb_account_flags_output = AccountFlag.CreateAccountFlagsTable()
    _str_holder_name = String.Empty
    _str_flag_name = String.Empty
    _idx_rows_cancelled = 0

    For Each _idx_row As Int32 In _selected_rows

      If Not String.IsNullOrEmpty(Grid.Cell(_idx_row, GRID_COLUMN_AF_UNIQUE_ID).Value) _
        AndAlso GetFlagStatusFromDescription(Grid.Cell(_idx_row, GRID_COLUMN_FLAG_STATUS).Value) = ACCOUNT_FLAG_STATUS.ACTIVE Then

        ' Current data is valid to cancel a flag 
        _account_flag_row = _tb_account_flags_input.NewRow()
        _account_flag_row(AccountFlag.SQL_COLUMN_AF_UNIQUE_ID) = Grid.Cell(_idx_row, GRID_COLUMN_AF_UNIQUE_ID).Value
        _account_flag_row(AccountFlag.SQL_COLUMN_AF_ACCOUNT_ID) = Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_ID).Value
        _account_flag_row(AccountFlag.SQL_COLUMN_AF_FLAG_ID) = Grid.Cell(_idx_row, GRID_COLUMN_FLAG_ID).Value
        _account_flag_row(AccountFlag.SQL_COLUMN_AF_STATUS) = GetFlagStatusFromDescription(Grid.Cell(_idx_row, GRID_COLUMN_FLAG_STATUS).Value)
        _tb_account_flags_input.Rows.Add(_account_flag_row)
      End If
    Next

    If _tb_account_flags_input.Rows.Count = 0 Then

      'MsgBox("xDebe seleccionar banderas v�lidas!")
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1423), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Exit Sub

    End If
    ' Set Status to Canceled to selected flags
    _id_sure_cancel_flag = IIf(_tb_account_flags_input.Rows.Count = 1 _
                              , GLB_NLS_GUI_PLAYER_TRACKING.Id(1355) _
                              , GLB_NLS_GUI_PLAYER_TRACKING.Id(1420))

    ' Are you sure to CANCELL flag/s?
    _confirmation_result = NLS_MsgBox(_id_sure_cancel_flag _
                                      , ENUM_MB_TYPE.MB_TYPE_INFO _
                                      , ENUM_MB_BTN.MB_BTN_YES_NO _
                                     , ENUM_MB_DEF_BTN.MB_DEF_BTN_1 _
                                     , _tb_account_flags_input.Rows.Count.ToString())

    If (_confirmation_result <> ENUM_MB_RESULT.MB_RESULT_YES) Then
      Exit Sub
    End If


    _idx_rows_cancelled += AccountFlag.ChangeStatusFlags(_tb_account_flags_input _
                                                        , ACCOUNT_FLAG_STATUS.CANCELLED _
                                                        , CurrentUser.Id _
                                                        , _tb_account_flags_output)

    _tb_account_flags_input.Dispose()

    ' Get output table and sort to audit changes
    _tb_view_sorted = _tb_account_flags_output.DefaultView
    _tb_view_sorted.Sort = "AF_ACCOUNT_ID"
    _tb_account_flags_output.Dispose()

    ' Save sorted table
    _tb_account_flags_output = _tb_view_sorted.ToTable()
    _tb_view_sorted.Dispose()

    ''' AUDIT CHANGES
    _last_account_id = Int64.MinValue
    _auditor_data = Nothing
    For Each _changed_row As DataRow In _tb_account_flags_output.Rows
      If _last_account_id <> _changed_row(AccountFlag.SQL_COLUMN_AF_ACCOUNT_ID) Then
        If _last_account_id <> Int64.MinValue Then
          ' Send last change and clean _auditor_data
          _auditor_data.Notify(GLB_CurrentUser.GuiId, _
               GLB_CurrentUser.Id, _
               GLB_CurrentUser.Name, _
               CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, _
               0)
          _auditor_data = Nothing
        End If
        ' Set new entry to auditor
        _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_ACCOUNT)

        _last_account_id = _changed_row(AccountFlag.SQL_COLUMN_AF_ACCOUNT_ID)
        _str_holder_name = GetHolderName(_last_account_id)

        ' Accounts | Flag Assignation to Account %id - %Name
        _auditor_data.SetName(GLB_NLS_GUI_STATISTICS.Id(207) _
                        , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1382, _last_account_id.ToString(), _str_holder_name))


        'Add flag
        _str_flag_name = GetFlagName(_changed_row(AccountFlag.SQL_COLUMN_AF_FLAG_ID))
        _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1286), _str_flag_name) ' Flag = flag_name
      Else
        ' Add flag
        _str_flag_name = GetFlagName(_changed_row(AccountFlag.SQL_COLUMN_AF_FLAG_ID))
        _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1286), _str_flag_name) ' Flag = flag_name
      End If
    Next
    If _last_account_id <> Int64.MinValue Then
      ' Send last change and clean _auditor_data
      _auditor_data.Notify(GLB_CurrentUser.GuiId, _
           GLB_CurrentUser.Id, _
           GLB_CurrentUser.Name, _
           CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, _
           0)
      _auditor_data = Nothing
    End If




    If (_idx_rows_cancelled = _selected_rows.Length) Then

      If (_idx_rows_cancelled = 1) Then

        'Call MsgBox(String.Format("xSe han cancelado la bandera seleccionada", _selected_rows.Length))
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1426) _
                            , ENUM_MB_TYPE.MB_TYPE_INFO _
                            , ENUM_MB_BTN.MB_BTN_OK _
                            , ENUM_MB_DEF_BTN.MB_DEF_BTN_1 _
                            , _selected_rows.Length)
      Else

        'Call MsgBox(String.Format("xSe han cancelado las {0} banderas seleccionadas", _selected_rows.Length))
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1424) _
                            , ENUM_MB_TYPE.MB_TYPE_INFO _
                            , ENUM_MB_BTN.MB_BTN_OK _
                            , ENUM_MB_DEF_BTN.MB_DEF_BTN_1 _
                            , _selected_rows.Length)
      End If



    Else


      If (_idx_rows_cancelled = 1) Then
        'Call MsgBox(String.Format("xSe han cancelado la bandera seleccionada", _selected_rows.Length))
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1426) _
                            , ENUM_MB_TYPE.MB_TYPE_INFO _
                            , ENUM_MB_BTN.MB_BTN_OK _
                            , ENUM_MB_DEF_BTN.MB_DEF_BTN_1 _
                            , _selected_rows.Length)
      Else
        'Call MsgBox(String.Format("xSe han cancelado {0} de las {1} banderas seleccionadas", _idx_rows_cancelled, _tb_account_flags_output.Rows.Count))
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1425) _
                        , ENUM_MB_TYPE.MB_TYPE_INFO _
                        , ENUM_MB_BTN.MB_BTN_OK _
                        , ENUM_MB_DEF_BTN.MB_DEF_BTN_1 _
                        , _idx_rows_cancelled _
                        , _selected_rows.Length)


      End If
    End If



  End Sub


  Private Sub SetDefaultValues()

    ' Status
    chk_status_active.Checked = True
    chk_status_cancelled.Checked = False
    chk_status_expired.Checked = False

    ' Order
    Me.opt_AccountFlag.Checked = True
    Me.chk_show_all_accounts.Checked = False

    ' Assignation dates
    uc_assigned_date.Value = GUI_GetDate().AddHours(GetDefaultClosingTime())
    uc_assigned_date.Checked = False
    uc_assigned_date_until.Value = GUI_GetDate().AddDays(1).AddHours(GetDefaultClosingTime())
    uc_assigned_date_until.Checked = False

    ' Flag
    Me.dg_select_flag.Clear()
    FillFlags(Me.dg_select_flag)
    handle_select_flags_Click(Me.btn_select_all_flags, Nothing)
    Me.opt_flag_include_any.Checked = True

    ' Account
    uc_account_sel1.Clear()

    ' Account Level
    If Not m_procedence_promotions Or m_first_call_from_prmotion Then
      Me.chk_level_01.Checked = False
      Me.chk_level_02.Checked = False
      Me.chk_level_03.Checked = False
      Me.chk_level_04.Checked = False
      Me.chk_only_anonymous.Checked = False
    End If


    If m_procedence_promotions Then
      Me.gbStatus.Enabled = False
      Me.chk_GroupByFlag.Checked = True
      Me.chk_ShowSubtotalByAccountFlag.Checked = False
      Me.chk_GroupByFlag.Enabled = False
      Me.chk_ShowSubtotalByAccountFlag.Enabled = False
      Me.chk_show_all_accounts.Enabled = False

    Else
      Me.chk_GroupByFlag.Checked = False
      Me.chk_ShowSubtotalByAccountFlag.Checked = True
    End If


  End Sub

  Private Sub GUI_StyleView()
    Dim _count_columns As Integer

    _count_columns = 0
    Call Me.Grid.Init(GRID_COLUMNS, GRID_HEADER_ROWS)

    SQL_COLUMN_ACCOUNT_TYPE = 8
    SQL_COLUMN_FLAG_TYPE = 10

    If Me.opt_AccountFlag.Checked Or Not Me.gbGroup.Enabled Then
      GRID_COLUMN_HOLDER_LEVEL = 1
      GRID_COLUMN_ACCOUNT_ID = 2
      GRID_COLUMN_TRACK_DATA = 3
      GRID_COLUMN_HOLDER_NAME = 4
      GRID_COLUMN_FLAG_COLOR = 5
      GRID_COLUMN_FLAG_NAME = 6

    ElseIf (Me.opt_FlagAccount.Checked) Then
      GRID_COLUMN_FLAG_COLOR = 1
      GRID_COLUMN_FLAG_NAME = 2
      GRID_COLUMN_HOLDER_LEVEL = 3
      GRID_COLUMN_ACCOUNT_ID = 4
      GRID_COLUMN_TRACK_DATA = 5
      GRID_COLUMN_HOLDER_NAME = 6
    End If

    If Me.chk_GroupByFlag.Checked Then
      SQL_COLUMN_ACCOUNT_ID = 0
      SQL_COLUMN_TRACK_DATA = 1
      SQL_COLUMN_HOLDER_NAME = 2
      SQL_COLUMN_HOLDER_LEVEL = 3
      SQL_COLUMN_FLAG_ID = 4
      SQL_COLUMN_FLAG_COLOR = 5
      SQL_COLUMN_FLAG_NAME = 6

      If Me.m_number_of_checks >= 2 Then ' Check the status checkboxes and build the Grid/Sql
        If Me.chk_status_active.Checked _
            And Me.chk_status_cancelled.Checked _
            And Me.chk_status_expired.Checked Then ' X Active, X Expired, X Canceled

          GRID_COLUMN_FLAG_ACTIVE = 7
          GRID_COLUMN_FLAG_EXPIRED = 8
          GRID_COLUMN_FLAG_CANCELLED = 9
          GRID_COLUMN_FLAG_AMOUNT = 10

          SQL_COLUMN_FLAG_ACTIVE = 9
          SQL_COLUMN_FLAG_EXPIRED = 10
          SQL_COLUMN_FLAG_CANCELLED = 11

          _count_columns = 11

        ElseIf Me.chk_status_active.Checked _
              And Me.chk_status_cancelled.Checked _
              And Not Me.chk_status_expired.Checked Then ' X Active, - Expired, X Canceled


          GRID_COLUMN_FLAG_ACTIVE = 7
          GRID_COLUMN_FLAG_CANCELLED = 8
          GRID_COLUMN_FLAG_AMOUNT = 9

          SQL_COLUMN_FLAG_ACTIVE = 9
          SQL_COLUMN_FLAG_CANCELLED = 10

          _count_columns = 10

        ElseIf Me.chk_status_active.Checked _
              And Not Me.chk_status_cancelled.Checked _
              And Me.chk_status_expired.Checked Then ' X Active, X Expired, - Canceled

          GRID_COLUMN_FLAG_ACTIVE = 7
          GRID_COLUMN_FLAG_EXPIRED = 8
          GRID_COLUMN_FLAG_AMOUNT = 9

          SQL_COLUMN_FLAG_ACTIVE = 9
          SQL_COLUMN_FLAG_EXPIRED = 10

          _count_columns = 10

        ElseIf Not Me.chk_status_active.Checked _
              And Me.chk_status_cancelled.Checked _
              And Me.chk_status_expired.Checked Then ' - Active, X Expired, X Canceled

          GRID_COLUMN_FLAG_EXPIRED = 7
          GRID_COLUMN_FLAG_CANCELLED = 8
          GRID_COLUMN_FLAG_AMOUNT = 9

          SQL_COLUMN_FLAG_EXPIRED = 9
          SQL_COLUMN_FLAG_CANCELLED = 10

          _count_columns = 10

        End If
      Else
        GRID_COLUMN_FLAG_AMOUNT = 7
        _count_columns = 8
      End If

      Call Me.Grid.Init(_count_columns, GRID_HEADER_ROWS)

    Else
      SQL_COLUMN_ACCOUNT_ID = 1
      SQL_COLUMN_TRACK_DATA = 2
      SQL_COLUMN_HOLDER_NAME = 3
      SQL_COLUMN_HOLDER_LEVEL = 4
      SQL_COLUMN_FLAG_ID = 13
      SQL_COLUMN_FLAG_COLOR = 5
      SQL_COLUMN_FLAG_NAME = 6
      SQL_COLUMN_ACCOUNT_TYPE = 16
      SQL_COLUMN_FLAG_TYPE = 17
    End If

    ' AF_UNIQUE_ID
    With Grid.Column(GRID_COLUMN_AF_UNIQUE_ID)
      .Header(0).Text = "  "
      .Header(1).Text = "   "
      .Width = 200
      .HighLightWhenSelected = False
      .IsColumnPrintable = False
      .Mapping = SQL_COLUMN_AF_UNIQUE_ID
    End With
    'End If

    ' ACCOUNT_ID
    With Grid.Column(GRID_COLUMN_ACCOUNT_ID)
      .Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230) ' Cuenta
      .Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(231)
      .Width = 1150
      .Mapping = SQL_COLUMN_ACCOUNT_ID
      .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    End With

    ' TRACK_DATA
    With Grid.Column(GRID_COLUMN_TRACK_DATA)
      .Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(212)
      .Width = 2300
      .Mapping = SQL_COLUMN_TRACK_DATA
      .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
    End With

    ' HOLDER_NAME
    With Grid.Column(GRID_COLUMN_HOLDER_NAME)
      .Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(235)
      .Width = 2500
      .Mapping = SQL_COLUMN_HOLDER_NAME
      .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    End With

    ' HOLDER LEVEL
    With Me.Grid.Column(GRID_COLUMN_HOLDER_LEVEL)
      .Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(381)
      .Width = 1200
      .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Mapping = SQL_COLUMN_HOLDER_LEVEL
    End With


    ' FLAG_COLOR
    With Me.Grid.Column(GRID_COLUMN_FLAG_COLOR)
      .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286)
      .Header(1).Text = "  "
      .Width = 200
      .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .HighLightWhenSelected = False
      .Mapping = SQL_COLUMN_FLAG_COLOR
      .IsMerged = True
    End With

    ' FLAG_NAME
    With Me.Grid.Column(GRID_COLUMN_FLAG_NAME)
      .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286) ' Bandera
      .Header(1).Text = GLB_NLS_GUI_CONFIGURATION.GetString(210)    ' Name
      .Width = 2500
      .Mapping = SQL_COLUMN_FLAG_NAME
      .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    End With

    ' FLAG_TYPE
    With Me.Grid.Column(GRID_COLUMN_FLAG_TYPE)
      .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286) ' Bandera
      .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(254)  ' Type
      .Width = 1200
      .Mapping = SQL_COLUMN_FLAG_TYPE
      .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    End With

    If Not Me.chk_GroupByFlag.Checked Then

      ' VALID FROM
      With Me.Grid.Column(GRID_COLUMN_VALID_FROM)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1326) '"Validez"
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1328) ' Valid From
        .Width = 2000
        .Mapping = SQL_COLUMN_VALID_FROM
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      End With

      ' VALID TO    
      With Me.Grid.Column(GRID_COLUMN_VALID_TO)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1326) '"Validez"
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1329) ' V�lida Hasta
        .Width = 2000
        .Mapping = SQL_COLUMN_VALID_TO
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      End With

      ' FLAG STATUS
      With Me.Grid.Column(GRID_COLUMN_FLAG_STATUS)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1326) '"Validez"
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(262) ' ESTADO
        .Width = 1400
        .Mapping = SQL_COLUMN_FLAG_STATUS
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      End With

      ' HOLDER_NAME ( ADDER )
      With Me.Grid.Column(GRID_COLUMN_AC_ADDED_BY)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1327) ' Asignacion
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1308)   ' Usuario Alta      TO DO : Buscar algo mejor ;) 
        .Width = 2000
        .Mapping = SQL_COLUMN_AC_ADDED_BY
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      End With

      ' PROMO NAME ( ADDER )
      With Me.Grid.Column(GRID_COLUMN_PM_ADDED_BY)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1327) ' Asignacion
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1309)   ' Promoci�n Alta    TO DO : Buscar algo mejor ;)
        .Width = 2000
        .Mapping = SQL_COLUMN_PM_ADDED_BY
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      End With

      ' ADDED DATETIME
      With Me.Grid.Column(GRID_COLUMN_ADDED_DATETIME)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1327) ' Asignacion
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1307) ' Fecha de asignaci�n
        .Width = 2000
        .Mapping = SQL_COLUMN_ADDED_DATETIME
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      End With

      ' FLAG ID
      With Grid.Column(GRID_COLUMN_FLAG_ID)
        .Width = 0
        .Header(0).Text = String.Empty
        .Header(1).Text = String.Empty
        .Mapping = SQL_COLUMN_FLAG_ID
        .HighLightWhenSelected = False
        .IsColumnPrintable = False
      End With

      ' CANCELATION DATE
      With Grid.Column(GRID_COLUMN_CANCELLED_DATE_TIME)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1373) ' Cancelaci�n
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1374) ' Cancelation date
        .Width = 2000
        .Mapping = SQL_COLUMN_CANCELLED_DATE_TIME
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      End With

      ' CANCELLER
      With Grid.Column(GRID_COLUMN_CANCELLED_USER_ID)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1373) ' Cancelaci�n
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1308) ' Canceller
        .Width = 2000
        .Mapping = SQL_COLUMN_CANCELLED_USER_ID
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      End With
    Else ' GroupByFlag Checked
      ' AMOUNT
      With Grid.Column(GRID_COLUMN_FLAG_AMOUNT)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286)
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1340) ' Total
        .Width = 1000
        .Mapping = SQL_COLUMN_FLAG_AMOUNT
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      End With

      If Me.m_number_of_checks >= 2 Then ' Only if there are more than one status checked
        If Me.chk_status_active.Checked Then
          ' ACTIVE
          With Grid.Column(GRID_COLUMN_FLAG_ACTIVE)
            .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286)
            .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2817) ' Activas
            .Width = 1000
            .Mapping = SQL_COLUMN_FLAG_ACTIVE
            .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
          End With
        End If

        If Me.chk_status_expired.Checked Then
          ' EXPIRED
          With Grid.Column(GRID_COLUMN_FLAG_EXPIRED)
            .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286)
            .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2819) ' Expiradas
            .Width = 1000
            .Mapping = SQL_COLUMN_FLAG_EXPIRED
            .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
          End With
        End If

        If Me.chk_status_cancelled.Checked Then
          ' CANCELLED
          With Grid.Column(GRID_COLUMN_FLAG_CANCELLED)
            .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286)
            .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2818) ' Canceladas
            .Width = 1200
            .Mapping = SQL_COLUMN_FLAG_CANCELLED
            .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
          End With
        End If

        ' Set the grid width depending on the number of columns
        Grid.Column(GRID_COLUMN_HOLDER_NAME).Width = 3900
        Grid.Column(GRID_COLUMN_FLAG_NAME).Width = 3065

        If Me.chk_status_cancelled.Checked Then
          Grid.Column(GRID_COLUMN_FLAG_NAME).Width = 2865
        End If

        If Me.m_number_of_checks = 3 Then
          Grid.Column(GRID_COLUMN_HOLDER_NAME).Width = 3250
          Grid.Column(GRID_COLUMN_FLAG_NAME).Width = 2500
        End If

      Else
        Grid.Column(GRID_COLUMN_HOLDER_NAME).Width = 5000
        Grid.Column(GRID_COLUMN_FLAG_NAME).Width = 3965
      End If
    End If

    ' Merge by selected order
    If (Me.opt_AccountFlag.Checked) Then

      Grid.Column(GRID_COLUMN_ACCOUNT_ID).IsMerged = True
      Grid.Column(GRID_COLUMN_TRACK_DATA).IsMerged = True
      Grid.Column(GRID_COLUMN_HOLDER_NAME).IsMerged = True
      Grid.Column(GRID_COLUMN_HOLDER_LEVEL).IsMerged = True

    ElseIf Me.opt_FlagAccount.Checked Then

      Grid.Column(GRID_COLUMN_FLAG_NAME).IsMerged = True
      Grid.Column(GRID_COLUMN_FLAG_COLOR).IsMerged = True
      Grid.Column(GRID_COLUMN_ACCOUNT_ID).IsMerged = False
      Grid.Column(GRID_COLUMN_TRACK_DATA).IsMerged = False

    End If
  End Sub 'GUI_StyleView

  Private Sub InitializeGridSelectFlags()

    If Not m_procedence_promotions Then
      Call Me.dg_select_flag.Init(GRID_SELECT_FLAG_COLUMNS, GRID_SELECT_FLAG_HEADER_ROWS, True)
    Else
      Call Me.dg_select_flag.Init(GRID_SELECT_FLAG_COLUMNS + 1, GRID_SELECT_FLAG_HEADER_ROWS, True)
    End If

    ' FLAG_ID ( NOT VISIBLE )
    With Me.dg_select_flag.Column(GRID_SELECT_FLAG_IDX)
      .Width = 0
      .IsColumnPrintable = False
    End With

    ' FLAG COLOR
    With Me.dg_select_flag.Column(GRID_SELECT_FLAG_COLOR)
      .Width = 200
      .HighLightWhenSelected = False
    End With

    ' FLAG CHECKED
    With Me.dg_select_flag.Column(GRID_SELECT_FLAG_CHECKED)
      .Header.Text = ""
      .WidthFixed = 300
      .Fixed = True
      .Editable = True
      .EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
    End With

    ' COLUMN_FLAG_NAME
    With Me.dg_select_flag.Column(GRID_SELECT_FLAG_NAME)
      If Not m_procedence_promotions Then
        .Width = 3000
      Else
        .Width = 2400
      End If
      .Editable = False
      .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    End With

    ' COLUMN_FLAG_COUNT 
    If m_procedence_promotions Then
      With Me.dg_select_flag.Column(GRID_SELECT_FLAG_COUNT)
        .Width = 600
        .Editable = False
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      End With
    End If
  End Sub

  ' PURPOSE : Ger Flag List
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS : DataTable with FlagId and FlagName
  Private Function DB_GetFlagsData() As DataTable

    Dim _table As DataTable
    Dim _str_bld As StringBuilder
    _str_bld = New StringBuilder()

    _str_bld.AppendLine("SELECT   FL_FLAG_ID ")
    _str_bld.AppendLine("       , FL_COLOR   ")
    _str_bld.AppendLine("       , FL_NAME    ")
    _str_bld.AppendLine("  FROM   FLAGS      ")
    _str_bld.AppendLine(" ORDER   BY FL_NAME ")

    _table = GUI_GetTableUsingSQL(_str_bld.ToString(), Int32.MaxValue)

    Return _table

  End Function 'GetFlagsData()

  ' PURPOSE : Fill Grid with Flag Data
  '
  '  PARAMS :
  '     - INPUT : Grid to Fill
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub FillFlags(ByRef Grid As uc_grid)

    Dim _table As DataTable
    Dim _idx As Integer
    Dim _flag_color As Color
    Dim _flag_id As Integer
    Dim _has_previous_data As Boolean
    Dim _previous_unselected_flags As List(Of Int64)

    _previous_unselected_flags = New List(Of Int64)

    If Not m_procedence_promotions Then
      _table = CLASS_FLAG.GetAllFlags(True)
    Else
      _table = m_table_flag_procedence
    End If

    ' Get previous unselected flags to keep flag selection
    If (Me.dg_select_flag.NumRows > 0) Then
      For _idx = 0 To dg_select_flag.NumRows - 1
        If Me.dg_select_flag.Cell(_idx, GRID_SELECT_FLAG_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED Then
          _flag_id = dg_select_flag.Cell(_idx, GRID_SELECT_FLAG_IDX).Value
          _previous_unselected_flags.Add(_flag_id)
        End If
      Next
      _has_previous_data = True
    End If

    Me.dg_select_flag.Clear()

    'Dim _tem_id As Integer = 9990
    For _idx = 0 To _table.Rows.Count - 1

      Call dg_select_flag.AddRow()

      _flag_id = _table.Rows.Item(_idx).Item("FL_FLAG_ID")
      _flag_color = Color.FromArgb(_table.Rows.Item(_idx).Item("FL_COLOR"))

      Me.dg_select_flag.Cell(_idx, GRID_SELECT_FLAG_IDX).Value = _flag_id
      '_tem_id = _tem_id + 1

      ' Trick that allow set cell backcolor as Black
      Call ProcessColor(_flag_color)
      Me.dg_select_flag.Cell(_idx, GRID_SELECT_FLAG_COLOR).BackColor = _flag_color
      Me.dg_select_flag.Cell(_idx, GRID_SELECT_FLAG_COLOR).Value = String.Empty
      Me.dg_select_flag.Cell(_idx, GRID_SELECT_FLAG_NAME).Value = _table.Rows.Item(_idx).Item("FL_NAME").ToString()

      'Count Required
      If m_procedence_promotions Then
        Me.dg_select_flag.Cell(_idx, GRID_SELECT_FLAG_COUNT).Value = _table.Rows.Item(_idx).Item("Counter").ToString()
      End If
      If _has_previous_data Then
        Me.dg_select_flag.Cell(_idx, GRID_SELECT_FLAG_CHECKED).Value = IIf(_previous_unselected_flags.Contains(_flag_id), _
                                                                              uc_grid.GRID_CHK_UNCHECKED, _
                                                                              uc_grid.GRID_CHK_CHECKED)
      Else
        Me.dg_select_flag.Cell(_idx, GRID_SELECT_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
      End If

    Next
  End Sub 'FillFlags()

  ' PURPOSE : Open Form Flag Selection and allow to user to select a Flag
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS : Selected FlagId or -1 if no flag selected

  Private Sub ShowDataTable(ByRef Data As DataTable)

    Dim _form As Form
    Dim _grid As DataGridView

    _form = New Form()

    _form.Width = 800
    _form.Height = 600
    _grid = New DataGridView
    _grid.Location = New Point(0, 0)
    _grid.Width = _form.ClientRectangle.Width
    _grid.Height = _form.ClientRectangle.Height
    _grid.AutoGenerateColumns = True
    _grid.DataSource = Data
    _form.Controls.Add(_grid)

    Call _form.ShowDialog()

  End Sub
  Private Function GetIdsFromSelectFormFlags() As Int32()

    Dim _frm_f As frm_flags_sel
    Dim _flag_id() As Integer

    _frm_f = New frm_flags_sel
    _flag_id = _frm_f.ShowForSelect()

    Return _flag_id

  End Function ' SelectFlag()

  Private Function GetSelectedFlagsTitles(Optional ByRef Separator As String = ",") As String

    Dim _idx As Int32
    Dim _str_bld As StringBuilder
    Dim _str_out As String
    Dim _flag_count As Integer

    _str_bld = New StringBuilder
    _flag_count = 0

    For _idx = 0 To (Me.dg_select_flag.NumRows - 1)
      If (Me.dg_select_flag.Cell(_idx, GRID_SELECT_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED) Then
        _str_bld.Append(Me.dg_select_flag.Cell(_idx, GRID_SELECT_FLAG_NAME).Value & ",")
        _flag_count += 1
      End If
    Next

    ' All or flag list
    _str_out = IIf(_flag_count = Me.dg_select_flag.NumRows, GLB_NLS_GUI_PLAYER_TRACKING.GetString(287), _str_bld.ToString())


    If (_str_out.Length > 0 AndAlso _str_out.EndsWith(",")) Then

      _str_out = _str_out.Substring(0, _str_out.Length - 1)

    End If


    Return _str_out


  End Function

  Private Function GetSelectedFlagsId() As List(Of Int32)

    Dim _list As List(Of Int32)
    Dim _idx As Int32

    _list = New List(Of Int32)

    For _idx = 0 To Me.dg_select_flag.NumRows - 1

      If (Me.dg_select_flag.Cell(_idx, GRID_SELECT_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED) Then
        _list.Add(Me.dg_select_flag.Cell(_idx, GRID_SELECT_FLAG_IDX).Value)
      End If

    Next


    Return _list

  End Function

  Private Function GetCaptionsFromSelectedCheckedBoxes(ByRef ControlContainer As Control) As String

    Dim _str_out As String
    Dim _str_bld As StringBuilder
    Dim _idx As Integer
    Dim _idx_checked As Integer
    _str_bld = New StringBuilder()
    _idx = 0
    _idx_checked = 0

    For Each _ctr As Control In ControlContainer.Controls
      If TypeOf _ctr Is CheckBox Then
        If CType(_ctr, CheckBox).Checked Then

          _str_bld.AppendFormat("{0}, ", _ctr.Text)
          _idx_checked += 1
        End If
        _idx += 1
      End If
    Next

    _str_out = IIf(_idx_checked <> _idx, _str_bld.ToString(), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353))

    If (_str_out.Length - 2 > 0 AndAlso _str_out.EndsWith(", ")) Then
      _str_out = _str_out.Substring(0, _str_out.Length - 2)
    End If

    Return _str_out

  End Function

  Private Function GetCaptionFromSelectedRadioButton(ByRef GroupBox As GroupBox)

    Dim _rb As RadioButton

    _rb = GetSelectedRadioButton(GroupBox)

    If _rb Is Nothing Then
      Return String.Empty
    Else
      Return _rb.Text
    End If

  End Function

  Private Function GetSelectedRadioButton(ByRef ControlContainer As Control) As RadioButton

    Dim _selected_radio_button As RadioButton
    _selected_radio_button = Nothing

    For Each _ctr As Control In ControlContainer.Controls
      If TypeOf _ctr Is RadioButton Then
        If CType(_ctr, RadioButton).Checked Then
          _selected_radio_button = CType(_ctr, RadioButton)
          Exit For
        End If
      End If
    Next

    Return _selected_radio_button
  End Function

  ' PURPOSE : Trick to set cell backround color to Black
  '
  '  PARAMS :
  '     - INPUT : Color
  '     - OUTPUT : Color
  '
  ' RETURNS : 
  Private Sub ProcessColor(ByRef FlagColor)


    If (FlagColor.A = Color.Black.A And _
    FlagColor.R = Color.Black.R And _
    FlagColor.G = Color.Black.G And _
    FlagColor.B = Color.Black.B) Then
      FlagColor = Color.FromArgb(FlagColor.A, FlagColor.R, FlagColor.G, FlagColor.B + 1)
    End If

  End Sub

  Private Function GetFlagStatusFromDescription(ByVal Description As String) As ACCOUNT_FLAG_STATUS

    Dim _status As ACCOUNT_FLAG_STATUS

    Select Case Description
      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(1314) ' Active
        _status = ACCOUNT_FLAG_STATUS.ACTIVE
      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(1331) ' Cancelled
        _status = ACCOUNT_FLAG_STATUS.CANCELLED
      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(1315) ' Expired
        _status = ACCOUNT_FLAG_STATUS.EXPIRED
      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(1330) ' No active
        _status = ACCOUNT_FLAG_STATUS.NO_ACTIVE
      Case Else
        _status = Nothing
    End Select

    Return _status

  End Function

  Private Function GetHolderName(ByVal AccountId As Int64) As String


    Dim _found_row As DataRow


    If (m_accounts_names_cache.Rows.Count = 0) Then
      Call FillAccountsNamesCache()
    End If

    _found_row = m_accounts_names_cache.Rows.Find(AccountId)


    If Not _found_row Is Nothing Then
      Return _found_row(1)
    End If
    Return String.Empty

  End Function

  Private Function GetFlagName(ByVal FlagId As Int64) As String


    Dim _found_row As DataRow

    If (m_flags_names_cache.Rows.Count = 0) Then
      Call FillFlagNamesCache()
    End If

    _found_row = m_flags_names_cache.Rows.Find(FlagId)


    If Not _found_row Is Nothing Then
      Return _found_row(1)
    End If
    Return String.Empty
  End Function

  Private Sub FillFlagNamesCache()

    Dim _str_sql As StringBuilder
    _str_sql = New StringBuilder()


    _str_sql.AppendLine("SELECT   FL_FLAG_ID")
    _str_sql.AppendLine("	      , FL_NAME   ")
    _str_sql.AppendLine("  FROM   FLAGS     ")


    Me.m_flags_names_cache = GUI_GetTableUsingSQL(_str_sql.ToString(), 9999)

    Me.m_flags_names_cache.PrimaryKey = New DataColumn() {Me.m_flags_names_cache.Columns("FL_FLAG_ID")}


  End Sub
  Private Sub FillAccountsNamesCache()

    Dim _str_sql As StringBuilder
    _str_sql = New StringBuilder()


    _str_sql.AppendLine("SELECT   AC_ACCOUNT_ID   ")
    _str_sql.AppendLine("	      , AC_HOLDER_NAME  ")
    _str_sql.AppendLine("  FROM   ACCOUNTS        ")
    _str_sql.AppendLine(" WHERE   AC_HOLDER_NAME IS NOT NULL")


    Me.m_accounts_names_cache = GUI_GetTableUsingSQL(_str_sql.ToString(), 9999)

    Me.m_accounts_names_cache.PrimaryKey = New DataColumn() {Me.m_accounts_names_cache.Columns("AC_ACCOUNT_ID")}



  End Sub
  ' PURPOSE: Add row with the subtotal count by flag
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub AddSubtotalFlagRow()
    Dim _idx_row As Integer = Me.Grid.NumRows

    If Me.chk_ShowSubtotalByAccountFlag.Checked Then
      If m_subtotal_flag_actived_flags <> 0 _
                Or m_subtotal_flag_canceled_flags <> 0 _
                Or m_subtotal_flag_expired_flags <> 0 Then

        Me.Grid.AddRow()

        If Not Me.opt_FlagAccount.Checked Then
          ' Label - TOTAL
          Me.Grid.Cell(_idx_row, 4).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1280, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2821))
          Me.Grid.Cell(_idx_row, 4).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
        Else
          Me.Grid.Cell(_idx_row, 5).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1280, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2821))
          Me.Grid.Cell(_idx_row, 5).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
        End If

        If Me.chk_status_active.Checked Then
          ' Total - Activos
          Me.Grid.Cell(_idx_row, 6).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2817, m_subtotal_flag_actived_flags.ToString)
        End If

        If Me.chk_status_expired.Checked Then
          ' Total - Expirados
          Me.Grid.Cell(_idx_row, 7).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2819, m_subtotal_flag_expired_flags.ToString)
        End If

        If Me.chk_status_cancelled.Checked Then
          ' Total - Cancelados
          Me.Grid.Cell(_idx_row, 8).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2818, m_subtotal_flag_canceled_flags.ToString)
        End If

        If Not Me.opt_FlagAccount.Checked Then
          ' Color Row
          Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_01)
        Else
          Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
        End If

        m_subtotal_flag_actived_flags = 0
        m_subtotal_flag_canceled_flags = 0
        m_subtotal_flag_expired_flags = 0
      End If
    End If
  End Sub ' AddSubtotalFlagRow

  ' PURPOSE: Add row with the subtotal count by account
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub AddSubtotalRow()
    Dim _idx_row As Integer = Me.Grid.NumRows

    If Me.chk_ShowSubtotalByAccountFlag.Checked Then
      If m_subtotal_actived_flags <> 0 _
          Or m_subtotal_canceled_flags <> 0 _
          Or m_subtotal_expired_flags <> 0 Then

        Me.Grid.AddRow()

        If Not Me.opt_FlagAccount.Checked Then
          ' Label - TOTAL
          Me.Grid.Cell(_idx_row, 4).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1046)
          Me.Grid.Cell(_idx_row, 4).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
        Else
          Me.Grid.Cell(_idx_row, 5).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1046)
          Me.Grid.Cell(_idx_row, 5).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
        End If

        If Me.chk_status_active.Checked Then
          ' Total - Activos
          Me.Grid.Cell(_idx_row, 6).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2817, m_subtotal_actived_flags.ToString)
        End If

        If Me.chk_status_expired.Checked Then
          ' Total - Expirados
          Me.Grid.Cell(_idx_row, 7).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2819, m_subtotal_expired_flags.ToString)
        End If

        If Me.chk_status_cancelled.Checked Then
          ' Total - Cancelados
          Me.Grid.Cell(_idx_row, 8).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2818, m_subtotal_canceled_flags.ToString)
        End If

        If Not Me.opt_FlagAccount.Checked Then
          ' Color Row
          Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
        Else
          Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_01)
        End If

        m_subtotal_actived_flags = 0
        m_subtotal_canceled_flags = 0
        m_subtotal_expired_flags = 0
      End If
    End If
  End Sub ' AddSubtotalRow

  ' PURPOSE: Update the count of subtotal by flag
  '
  '  PARAMS:
  '     - INPUT:
  '             flag_status
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub CheckSubtotalFlag(ByVal flag_status As Integer)

    If Me.chk_ShowSubtotalByAccountFlag.Checked Then
      Select Case flag_status
        Case ACCOUNT_FLAG_STATUS.ACTIVE
          m_subtotal_flag_actived_flags += 1
        Case ACCOUNT_FLAG_STATUS.CANCELLED
          m_subtotal_flag_canceled_flags += 1
        Case ACCOUNT_FLAG_STATUS.EXPIRED
          m_subtotal_flag_expired_flags += 1
      End Select
    End If
  End Sub ' CheckSubtotalFlag

  ' PURPOSE: Update the count of subtotal by account
  '
  '  PARAMS:
  '     - INPUT:
  '             flag_status
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub CheckSubtotal(ByVal flag_status As Integer)
    If Me.chk_ShowSubtotalByAccountFlag.Checked Then
      Select Case flag_status
        Case ACCOUNT_FLAG_STATUS.ACTIVE
          m_subtotal_actived_flags += 1
        Case ACCOUNT_FLAG_STATUS.CANCELLED
          m_subtotal_canceled_flags += 1
        Case ACCOUNT_FLAG_STATUS.EXPIRED
          m_subtotal_expired_flags += 1
      End Select
    End If
  End Sub ' CheckSubtotal

  ' PURPOSE: Update the count of Checks that are checked
  '
  '  PARAMS:
  '     - INPUT:
  '             Checked
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub CountChecksChecked(ByVal Checked As Boolean)
    If Checked Then
      Me.m_number_of_checks += 1
    Else
      If Me.m_number_of_checks > 0 Then
        Me.m_number_of_checks -= 1
      End If
    End If
  End Sub ' CountChecksChecked

  ' PURPOSE : Declare DateTable Flags.
  '
  '  PARAMS :
  '     - INPUT : 
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Sub DeclareDateTableFlags()
    m_table_flag_procedence = New DataTable

    m_table_flag_procedence.Columns.Add("ID", GetType(Integer))
    m_table_flag_procedence.Columns.Add("FL_FLAG_ID", GetType(String))
    m_table_flag_procedence.Columns.Add("Counter", GetType(Integer))
    m_table_flag_procedence.Columns.Add("FL_NAME", GetType(String))
    m_table_flag_procedence.Columns.Add("FL_COLOR", GetType(String))

  End Sub

#End Region

#Region " Public Functions "

  ' PURPOSE : Opens dialog with default settings for edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window, Optional ByVal _procedence_promotions As Boolean = False)

    m_procedence_promotions = _procedence_promotions
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent

    If _procedence_promotions Then
      Me.Display(True)
    Else
      Me.Display(False)
    End If

  End Sub ' ShowForEdit
  ' PURPOSE : Add new Row in Datatable
  '
  '  PARAMS :
  '     - INPUT : -idx, Flag_idx, Counter, Name, Color
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Public Sub FlagsAccountAdd(ByVal _idx As Integer, _
                              ByVal Flag_idx As String, _
                              ByVal Counter As Integer, _
                              ByVal Name As String, _
                              ByVal Color As Integer)


    Try

      m_table_flag_procedence.Rows.Add(_idx, Flag_idx, Counter, Name, Color)

    Catch ex As Exception
    End Try

  End Sub

  ' PURPOSE : Set Values at Level.
  '
  '  PARAMS :
  '     - INPUT : level1, level2, level3, level4
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Sub FlagsAccountLevelsAdd()
    Call FlagsAccountLevelsAdd(False, False, False, False, False)
  End Sub
  Public Sub FlagsAccountLevelsAdd(ByVal chk_level_01 As Boolean, _
                                      ByVal chk_level_02 As Boolean, _
                                      ByVal chk_level_03 As Boolean, _
                                      ByVal chk_level_04 As Boolean, _
                                      ByVal chk_only_anonymous As Boolean)

    Me.chk_level_04.Checked = chk_level_04
    Me.chk_level_03.Checked = chk_level_03
    Me.chk_level_02.Checked = chk_level_02
    Me.chk_level_01.Checked = chk_level_01
    Me.chk_only_anonymous.Checked = chk_only_anonymous

    DeclareDateTableFlags()

  End Sub

#End Region

#Region " Events "

  Private Sub handle_select_flags_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
              Handles btn_select_all_flags.Click, btn_select_none_flags.Click

    Dim _button As Button
    Dim _mark As String
    Dim idx_rows As Integer

    _button = CType(sender, Button)

    _mark = IIf(_button.Text = Me.btn_select_all_flags.Text, _
                uc_grid.GRID_CHK_CHECKED, _
                uc_grid.GRID_CHK_UNCHECKED)

    For idx_rows = 0 To Me.dg_select_flag.NumRows - 1
      Me.dg_select_flag.Cell(idx_rows, GRID_SELECT_FLAG_CHECKED).Value = _mark
    Next

  End Sub

  Private Sub chk_only_anonymous_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_only_anonymous.CheckedChanged

    Dim _check As CheckBox
    _check = CType(sender, CheckBox)


    Me.gb_level.Enabled = Not _check.Checked

  End Sub

  Private Sub opt_AccountFlag_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) _
            Handles opt_AccountFlag.CheckedChanged
    If Not m_procedence_promotions Then
      Me.chk_show_all_accounts.Enabled = opt_AccountFlag.Checked
    End If
  End Sub

  Private Sub uc_account_sel1_OneAccountFilterChanged() Handles uc_account_sel1.OneAccountFilterChanged
    Me.gb_level.Enabled = Not uc_account_sel1.DisabledHolder
    Me.chk_only_anonymous.Enabled = Not uc_account_sel1.DisabledHolder
  End Sub

  Private Sub chk_GroupByFlag_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_GroupByFlag.CheckedChanged
    If Me.chk_GroupByFlag.Checked _
        And Me.chk_ShowSubtotalByAccountFlag.Checked Then

      Me.chk_ShowSubtotalByAccountFlag.Checked = False

    End If
  End Sub

  Private Sub chk_ShowSubtotalByAccountFlag_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_ShowSubtotalByAccountFlag.CheckedChanged
    If Me.chk_ShowSubtotalByAccountFlag.Checked _
        And Me.chk_GroupByFlag.Checked Then

      Me.chk_GroupByFlag.Checked = False

    End If
  End Sub

  Private Sub chk_status_active_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk_status_active.CheckedChanged
    Call Me.CountChecksChecked(Me.chk_status_active.Checked)
  End Sub

  Private Sub chk_status_cancelled_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk_status_cancelled.CheckedChanged
    Call Me.CountChecksChecked(Me.chk_status_cancelled.Checked)
  End Sub

  Private Sub chk_status_expired_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk_status_expired.CheckedChanged
    Call Me.CountChecksChecked(Me.chk_status_expired.Checked)
  End Sub

#End Region

End Class
