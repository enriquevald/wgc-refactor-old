﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_loading
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.lb_message = New System.Windows.Forms.Label()
    Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
    Me.pb_gif = New System.Windows.Forms.PictureBox()
    Me.TableLayoutPanel1.SuspendLayout()
    CType(Me.pb_gif, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'lb_message
    '
    Me.lb_message.Anchor = System.Windows.Forms.AnchorStyles.None
    Me.lb_message.AutoSize = True
    Me.lb_message.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lb_message.Location = New System.Drawing.Point(85, 143)
    Me.lb_message.Name = "lb_message"
    Me.lb_message.Size = New System.Drawing.Size(57, 16)
    Me.lb_message.TabIndex = 1
    Me.lb_message.Text = "Loading"
    '
    'TableLayoutPanel1
    '
    Me.TableLayoutPanel1.BackColor = System.Drawing.SystemColors.Control
    Me.TableLayoutPanel1.ColumnCount = 1
    Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
    Me.TableLayoutPanel1.Controls.Add(Me.lb_message, 0, 1)
    Me.TableLayoutPanel1.Controls.Add(Me.pb_gif, 0, 0)
    Me.TableLayoutPanel1.Location = New System.Drawing.Point(2, 2)
    Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
    Me.TableLayoutPanel1.RowCount = 2
    Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80.0!))
    Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
    Me.TableLayoutPanel1.Size = New System.Drawing.Size(228, 168)
    Me.TableLayoutPanel1.TabIndex = 3
    '
    'pb_gif
    '
    Me.pb_gif.Anchor = System.Windows.Forms.AnchorStyles.None
    Me.pb_gif.Image = Global.WigosGUI.My.Resources.Resources.loading
    Me.pb_gif.Location = New System.Drawing.Point(77, 37)
    Me.pb_gif.Name = "pb_gif"
    Me.pb_gif.Size = New System.Drawing.Size(74, 59)
    Me.pb_gif.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
    Me.pb_gif.TabIndex = 1
    Me.pb_gif.TabStop = False
    '
    'frm_loading
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.BackColor = System.Drawing.SystemColors.ControlText
    Me.ClientSize = New System.Drawing.Size(232, 172)
    Me.Controls.Add(Me.TableLayoutPanel1)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
    Me.Name = "frm_loading"
    Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
    Me.Text = "frm_loading"
    Me.TopMost = True
    Me.TableLayoutPanel1.ResumeLayout(False)
    Me.TableLayoutPanel1.PerformLayout()
    CType(Me.pb_gif, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents lb_message As System.Windows.Forms.Label
  Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
  Friend WithEvents pb_gif As System.Windows.Forms.PictureBox
End Class
