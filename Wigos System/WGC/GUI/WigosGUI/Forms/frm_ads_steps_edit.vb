
'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_ads_steps_edit
' DESCRIPTION:   Advertisement edit
' AUTHOR:        Artur Nebot
' CREATION DATE: 16-MAY-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 16-MAY-2012  ANG    Initial version
' 29-JAN-2013  ANG    Fix Bug #537 & Set description max length
' 03-JAN-2017  DMT    PBI21626:Correcciones en el GUI
' 06-SEP-2017  RGR    Bug 29615:WIGOS-4217 WigosGUI is asking about save changes in the screen "Edicion de campa�a publicitaria" when the user didn't any
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Public Class frm_ads_steps_edit
  Inherits frm_base_edit

#Region " Constants "

  Protected Const IMG_MAX_WIDTH As Integer = 1280
  Protected Const IMG_MAX_HEIGH As Integer = 1024

  Protected Const CAMPAIGN_DEFAULT_DURATION As Integer = 15 ' In days 
#End Region

#Region " Members "

  Private m_input_ads_name As String
  Private m_delete_operation As Boolean
  Private m_winup_settings As CLASS_WINUP_SETTINGS
  Private m_section_promotions_id As Integer = 0
  Private m_section_events_id As Integer = 0

#End Region

#Region " Overrides "

  ' PURPOSE : Initializes the form id.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_ADS_STEPS_EDIT

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId
  Public Class ucComboBox
    Public Sub New(ByVal NewValue As Integer, ByVal NewDescription As String)
      Value = NewValue
      Description = NewDescription
    End Sub
    Public Value As Integer
    Public Description As String
    Public Overrides Function ToString() As String
      Return Description
    End Function
  End Class

  ' PURPOSE : Form controls initialization.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS  :
  Protected Overrides Sub GUI_InitControls()
    Call MyBase.GUI_InitControls()

    Dim _show_delete_button As Boolean
    _show_delete_button = False
    Dim _imageMaxSize As Size
    ' Set filters to entry fields
    Me.ef_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 100)

    ' Set Label values
    'Me.ef_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(693) ' Label name
    Me.lbl_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(693) ' Label name
    Me.lbl_winup_titulo.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7939) ' Label title
    Me.lbl_description.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(688) ' Label description
    Me.ef_description.MaxLength = 500 ' Set field max length with database column size

    'Me.img_image.Init(uc_image.MAXIMUN_RESOLUTION.x1280X1024, GLB_NLS_GUI_PLAYER_TRACKING.GetString(LABEL_IMAGE_CAPTION, "1280", "1024"))
    Me.img_image.Init(uc_image.MAXIMUN_RESOLUTION.x1280X1024)
    Me.lbl_optimum_size.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(733, "1280", "1024") ' Image Caption

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(694) ' Label form


    Me.chk_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(532)

    Me.lbl_abstract.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7936)
    Me.lbl_list_description.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7935)
    Me.ef_order.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7449)
    Me.ef_order.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.tab_app_images.TabPages.Item(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7450) 'imagen de lista
    Me.tab_app_images.TabPages.Item(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7451) 'imagen de detalle

    Me.TabContent.TabPages.Item(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7482)
    Me.TabContent.TabPages.Item(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7937)
    Me.TabContent.TabPages.Item(2).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7931)
    Me.chk_promobox.Checked = True
    Me.chk_promobox.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7932)
    Me.chk_winup.Checked = True
    Me.chk_winup.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7933)

    Me.lbl_valid_from.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(295)
    Me.lbl_valid_to.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(296)

    Me.ef_name.TextVisible = True
    Me.ef_order.TextVisible = True
    Me.lbl_section.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7934)
    If WSI.Common.GeneralParam.GetBoolean("Features", "WinUP") Then
      Me.m_winup_settings = New CLASS_WINUP_SETTINGS
      _imageMaxSize = Me.m_winup_settings.GetImageMaxSize()
      Me.Uc_imageList.Init(_imageMaxSize.Height, _imageMaxSize.Width)
      Me.Uc_imageDetail.Init(_imageMaxSize.Height, _imageMaxSize.Width)
      Me.AppFields.Parent = Me.TabContent
      Me.chk_winup.Visible = True
    Else
      Me.AppFields.Parent = Nothing
      Me.chk_winup.Visible = False
      Me.chk_winup.Checked = False
    End If

    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
      _show_delete_button = False

      'SDS 27-07-2016 add fields for app mobile PBI 15957
      Me.ef_order.Value = 0 'by default

      Dim _lista As New List(Of ucComboBox)
      For Each target As Integer In System.Enum.GetValues(GetType(WSI.Common.ADS_TARGET))
        Dim _list As New ucComboBox(target, TargetName(target))
        _lista.Add(_list)
      Next
      'cmb_target.ValueMember = "Value "
      'cmb_target.DisplayMember = "Description"
      'cmb_target.DataSource = _lista

    ElseIf Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT Then
      _show_delete_button = True
    End If

    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = _show_delete_button

    Me.img_image.ButtonDeleteEnabled = False

    'SDS 27-08-2016 PBI 17065 html description
    tab_html.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7567)
    tab_preview.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7568)
    chk_add_style.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7569)
    btn_insert_tag.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7570)
    cmbTag.Items.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7571)) 'titulo 1
    cmbTag.Items.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7572)) 'titulo 2
    cmbTag.Items.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7573)) 'titulo 3
    cmbTag.Items.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7574)) 'div
    cmbTag.Items.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7575)) 'span
    cmbTag.Items.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7576)) 'list
    cmbTag.Items.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7577)) 'bold
    cmbTag.Items.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7578)) 'italic
    cmbTag.Items.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7579)) 'underline
    cmbTag.Items.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7580)) 'paragraph


  End Sub

  ' PURPOSE : Set initial data on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _ads As CLASS_ADS_STEPS

    _ads = DbReadObject

    Me.ef_name.Value = _ads.Name
    Me.ef_description.Text = _ads.Description
    Me.chk_promobox.Checked = _ads.PromoboxEnabled

    If _ads.WinupValidFrom.ToShortTimeString() = dtp_from.GetMinDate().ToShortTimeString() Then
      Me.dtp_from.Value = GUI_GetDateTime()
    End If

    If _ads.WinupValidTo.ToShortTimeString() = dtp_to.GetMinDate().ToShortTimeString() Then
      Me.dtp_to.Value = GUI_GetDateTime()
    End If

    Me.Uc_schedule1.DateFrom = _ads.InitalDate
    Me.Uc_schedule1.DateTo = _ads.FinalDate
    Me.txt_winup_titulo.Text = _ads.WinupTitle
    Me.Uc_schedule1.Weekday = _ads.Weekday
    Me.Uc_schedule1.TimeFrom = _ads.TimeFrom
    Me.Uc_schedule1.TimeTo = _ads.TimeTo

    If _ads.HasSecondTime Then
      Me.Uc_schedule1.SecondTime = True
      Me.Uc_schedule1.SecondTimeFrom = _ads.SecondTimeFrom
      Me.Uc_schedule1.SecondTimeTo = _ads.SecondTimeTo
    End If


    Me.chk_enabled.Checked = _ads.Enabled

    If Not IsNothing(_ads.Image) Then
      Me.img_image.Image = _ads.Image
    Else
      Me.img_image.Image = Nothing
    End If

    'SDS 27-07-2016 add fields for app mobile PBI 15957
    If WSI.Common.GeneralParam.GetBoolean("Features", "WinUP") Then
      Me.txt_winup_titulo.Text = _ads.WinupTitle
      Me.chk_winup.Checked = _ads.WinupEnabled
      Me.dtp_from.Value = _ads.WinupValidFrom
      Me.dtp_to.Value = _ads.WinupValidTo
      Me.ef_list_description.Text = _ads.WinupListDescription

      CargaComboSchema()
      cmb_Section.Value = _ads.TargetSchemaId  

      Me.txt_abstract.Text = _ads.Abstract
      Me.ef_order.Value = _ads.Order

      If Not IsNothing(_ads.ImageDetail) Then
        Me.Uc_imageDetail.Image = _ads.ImageDetail
      Else
        Me.Uc_imageDetail.Image = Nothing
      End If

      If Not IsNothing(_ads.ImageList) Then
        Me.Uc_imageList.Image = _ads.ImageList
      Else
        Me.Uc_imageList.Image = Nothing
      End If
    End If
  End Sub ' GUI_SetScreenData

  ' PURPOSE : Get data from the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_GetScreenData()

    Dim _ads As CLASS_ADS_STEPS

    _ads = DbEditedObject

    _ads.Name = Me.ef_name.Value
    _ads.Description = Me.ef_description.Text.Trim()

    _ads.InitalDate = Me.Uc_schedule1.DateFrom
    _ads.FinalDate = Me.Uc_schedule1.DateTo
    _ads.Enabled = Me.chk_enabled.Checked

    _ads.Weekday = Me.Uc_schedule1.Weekday

    _ads.TimeFrom = Me.Uc_schedule1.TimeFrom
    _ads.TimeTo = Me.Uc_schedule1.TimeTo

    If Not Me.img_image.Image Is Nothing Then
      _ads.Image = Me.img_image.Image
    Else
      _ads.Image = Nothing
    End If

    If (Me.Uc_schedule1.SecondTime) Then
      _ads.SecondTimeFrom = Me.Uc_schedule1.SecondTimeFrom
      _ads.SecondTimeTo = Me.Uc_schedule1.SecondTimeTo
    Else
      _ads.SecondTimeFrom = Nothing
      _ads.SecondTimeTo = Nothing
    End If

    _ads.PromoboxEnabled = Me.chk_promobox.Checked



    'SDS 27-07-2016 add fields for app mobile PBI 15957
    If WSI.Common.GeneralParam.GetBoolean("Features", "WinUP") Then
      _ads.WinupTitle = Me.txt_winup_titulo.Text
      _ads.WinupEnabled = Me.chk_winup.Checked
      _ads.WinupValidFrom = Me.dtp_from.Value
      _ads.WinupValidTo = Me.dtp_to.Value
      _ads.WinupListDescription = Me.ef_list_description.Text

      If cmb_Section.Value = m_section_promotions_id Then
        _ads.Target = WSI.Common.ADS_TARGET.Promotions
      End If
      If cmb_Section.Value = m_section_events_id Then
        _ads.Target = WSI.Common.ADS_TARGET.Events
      End If

      _ads.TargetSchemaId = cmb_Section.Value  
      _ads.Abstract = Me.txt_abstract.Text.Replace(Chr(34), Chr(39))
      _ads.Order = Me.ef_order.Value

      If Not Me.Uc_imageDetail.Image Is Nothing Then
        _ads.ImageDetail = Me.Uc_imageDetail.Image
      Else
        _ads.ImageDetail = Nothing
      End If

      If Not Me.Uc_imageList.Image Is Nothing Then
        _ads.ImageList = Me.Uc_imageList.Image
      Else
        _ads.ImageList = Nothing
      End If
    End If

  End Sub ' GUI_GetScreenData

  ' PURPOSE : Validate the data presented on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    If String.IsNullOrEmpty(Me.ef_name.Value) Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_name.Text)
      Call ef_name.Focus()

      Return False

    End If

    If Me.chk_promobox.Checked Then

      If (Me.Uc_schedule1.DateTo < Me.Uc_schedule1.DateFrom) Then
        ' Error interval , last value less than first
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(732) _
        , ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(268), GLB_NLS_GUI_PLAYER_TRACKING.GetString(708))

        Return False
      End If

      If (Me.dtp_to.Value < Me.dtp_from.Value) Then
        ' Error interval , last value less than first
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(732) _
        , ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(268), GLB_NLS_GUI_PLAYER_TRACKING.GetString(708))

        Return False
      End If

      If (Me.img_image.Image Is Nothing) Then
        ' Must have picture
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1055), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Return False
      End If
    End If

    If Me.chk_winup.Checked Then


      If String.IsNullOrEmpty(Me.txt_winup_titulo.Text) Then

        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(7939))
        Call ef_name.Focus()

        Return False

      End If

      If (Me.Uc_imageList.Image Is Nothing) Then
        ' Must have picture
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8287), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Return False
      End If

      If (Me.Uc_imageDetail.Image Is Nothing) Then
        ' Must have picture
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8288), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Return False
      End If

      If (Me.Uc_imageList.Image IsNot Nothing) Then
        Dim _image_weight = Me.Uc_imageList.GetImageKBWeight()
        Dim _image_maxsize = Me.m_winup_settings.GetImageMaxWeightKB()

        If (_image_weight > 0) And (_image_weight > _image_maxsize) Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7802), ENUM_MB_TYPE.MB_TYPE_WARNING, , , " " & _image_maxsize & " KB", , )
          Return False
        End If
      End If
      If (Me.Uc_imageDetail.Image IsNot Nothing) Then
        Dim _image_weight = Me.Uc_imageDetail.GetImageKBWeight()
        Dim _image_maxsize = Me.m_winup_settings.GetImageMaxWeightKB()

        If (_image_weight > 0) And (_image_weight > _image_maxsize) Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7802), ENUM_MB_TYPE.MB_TYPE_WARNING, , , " " & _image_maxsize & " KB", , )
          Return False
        End If
      End If
    End If

    Return True

  End Function ' GUI_IsScreenDataOk

  ' PURPOSE : Init form in new mode
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '
  Public Overloads Sub ShowNewItem()

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    DbObjectId = Nothing
    DbStatus = ENUM_STATUS.STATUS_OK

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowNewItem

  ' PURPOSE : Init form in edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '       - UserId
  '       - Username
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Overloads Sub ShowEditItem(ByVal AdsId As Integer, ByVal AdsName As String)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Me.DbObjectId = AdsId

    Me.m_input_ads_name = AdsName

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If
  End Sub ' ShowEditItem

  ' PURPOSE : Database overridable operations. 
  '           Define specific DB operation for this form.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Dim _ads As CLASS_ADS_STEPS
    Dim _rc As Integer
    Dim _campaign_name As String

    Select Case DbOperation

      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        Me.DbEditedObject = New CLASS_ADS_STEPS
        _ads = Me.DbEditedObject

        With _ads
          .AdsStepId = 0
          .Enabled = True
          .InitalDate = WSI.Common.Misc.TodayOpening()
          .FinalDate = .InitalDate.AddDays(CAMPAIGN_DEFAULT_DURATION)
          .TimeFrom = 0
          .Weekday = Me.Uc_schedule1.AllWeekDaysSelected()              ' Select all days by default
        End With
        Me.DbStatus = ENUM_STATUS.STATUS_OK

      Case ENUM_DB_OPERATION.DB_OPERATION_INSERT
        Call MyBase.GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_INSERT)

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        Dim _msg As Integer

        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          _campaign_name = GetEditedCampaignName()
          _msg = IIf(Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY, 713, 716) ' Alredy exist , Error Addd
          Call ShowErrorMessage(_msg, _campaign_name)


        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_UPDATE
        Call MyBase.GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_UPDATE)

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        _campaign_name = GetEditedCampaignName()
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call ShowErrorMessage(714, _campaign_name) ' Error modify
        End If
      Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE

        _campaign_name = GetEditedCampaignName()
        m_delete_operation = False

        ' Delete confirmation
        _rc = NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(718),
                                 ENUM_MB_TYPE.MB_TYPE_WARNING,
                                 ENUM_MB_BTN.MB_BTN_YES_NO,
                                 ENUM_MB_DEF_BTN.MB_DEF_BTN_2,
                                _campaign_name)
        If _rc = ENUM_MB_RESULT.MB_RESULT_YES Then
          m_delete_operation = True
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_DELETE
        If m_delete_operation Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        Else
          DbStatus = ENUM_STATUS.STATUS_ERROR
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE

        If Me.DbStatus <> ENUM_STATUS.STATUS_OK And m_delete_operation Then
          _campaign_name = GetEditedCampaignName()
          Call ShowErrorMessage(717, _campaign_name)    ' ERROR DELETE

        End If


      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)

    Call MyBase.GUI_ButtonClick(ButtonId)

  End Sub

  ' PURPOSE : Get string of play session
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - String: 
  '
  Private Function TargetName(ByVal Target As WSI.Common.ADS_TARGET) As String

    Dim _str_target As String

    _str_target = ""

    Select Case Target
      Case WSI.Common.ADS_TARGET.Other
        _str_target = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7455) ' other
      Case WSI.Common.ADS_TARGET.Dining
        _str_target = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7456) ' dining
      Case WSI.Common.ADS_TARGET.Events
        _str_target = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7458) ' events
      Case WSI.Common.ADS_TARGET.RecentWinners
        _str_target = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7457) ' recesntwinners
      Case WSI.Common.ADS_TARGET.Promotions
        _str_target = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7581) ' promotions
      Case Else
        _str_target = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7455) ' other
    End Select


    Return _str_target

  End Function

  ' PURPOSE : Override default behavior when user press key
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    If Me.ef_description.Focused Then
      Return True
    Else
      Return False
    End If

    '
  End Function


#End Region

#Region " Private "

  Private Function GetEditedCampaignName() As String
    Return DirectCast(Me.DbEditedObject, CLASS_ADS_STEPS).Name
  End Function

  ' PURPOSE : Show error message
  '
  '  PARAMS :
  '     - INPUT : Integer represents NLS string , String
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub ShowErrorMessage(ByVal Nls_message As Integer, ByVal Field As String)

    Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(Nls_message),
                                           ENUM_MB_TYPE.MB_TYPE_ERROR,
                                           ENUM_MB_BTN.MB_BTN_OK,
                                           ENUM_MB_DEF_BTN.MB_DEF_BTN_1,
                                           Field)

  End Sub
#End Region

#Region " Events "


  Private Sub img_image_ImageSelected(ByRef [Continue] As Boolean, ByVal ImageSelected As System.Drawing.Image)

    ' Image Oversize
    If ImageSelected.Width > IMG_MAX_WIDTH Or ImageSelected.Height > IMG_MAX_HEIGH Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(833) _
            , ENUM_MB_TYPE.MB_TYPE_WARNING)

      [Continue] = False
    End If

  End Sub



#End Region

  Private Sub frm_ads_steps_edit_Load(sender As Object, e As EventArgs) Handles MyBase.Load

  End Sub


  Private Sub cmbTag_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTag.SelectedIndexChanged
    chk_add_style.Checked = False
  End Sub

  Private Sub btn_insert_tag_Click(sender As Object, e As EventArgs) Handles btn_insert_tag.Click

    Select Case cmbTag.Text
      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(7571)
        AddControl("H1")

      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(7572)
        AddControl("H2")
      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(7573)
        AddControl("H3")
      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(7577)
        AddControl("B")
      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(7578)
        AddControl("I")

      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(7579)
        AddControl("U")
      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(7576)
        txt_abstract.SelectionColor = Color.DarkGreen
        If chk_add_style.Checked = True Then
          txt_abstract.AppendText(vbCrLf + "<UL style= ''>")
          txt_abstract.AppendText(vbCrLf + "<LI style= ''>")
          txt_abstract.SelectionColor = Color.Black
          txt_abstract.AppendText("item1")
          txt_abstract.SelectionColor = Color.DarkGreen
          txt_abstract.AppendText("</LI>")
          txt_abstract.AppendText(vbCrLf + "</UL>")
        Else
          txt_abstract.AppendText(vbCrLf + "<UL>")
          txt_abstract.AppendText(vbCrLf + "<LI>")
          txt_abstract.SelectionColor = Color.Black
          txt_abstract.AppendText("item2")
          txt_abstract.SelectionColor = Color.DarkGreen
          txt_abstract.AppendText("</LI>")
          txt_abstract.AppendText(vbCrLf + "</UL>")
        End If


      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(7574)
        AddControl("DIV")
      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(7575)
        AddControl("SPAN")
      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(7580)
        AddControl("P")
    End Select
  End Sub
  'SDS 27-08-2016 PBI 17065 html description
  Private Sub AddControl(control As String)
    txt_abstract.SelectionColor = Color.DarkGreen
    If chk_add_style.Checked = True Then
      txt_abstract.AppendText(vbCrLf + "<" + control + " style= '' " + ">")
    Else
      txt_abstract.AppendText(vbCrLf + "<" + control + ">")
    End If
    txt_abstract.SelectionColor = Color.Black
    txt_abstract.AppendText("texto")
    txt_abstract.SelectionColor = Color.DarkGreen
    txt_abstract.AppendText("</" + control + ">")
  End Sub

  Private Sub tab_control_HTML_SelectedIndexChanged(sender As Object, e As EventArgs) Handles tab_control_HTML.SelectedIndexChanged
    Dim current As TabPage = TryCast(sender, TabControl).SelectedTab
    If current.Name = "tab_preview" Then

      web_browser_html.DocumentText = vbCrLf + txt_abstract.Text
    End If
  End Sub



  Private Sub CargaComboSchema()
    Dim _url As String
    Dim _response, _responseSanitized As String
    Dim _data As DataTable
    Dim _jarray As New JArray
    _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/sectionsschema/getSchemaByTemplate/3"
    Dim _api As New CLS_API(_url)

    Try

      cmb_Section.Clear()
      cmb_Section.Enabled = True

      _response = _api.getData()
      _responseSanitized = _api.sanitizeData(_response)
      _data = JsonConvert.DeserializeObject(Of DataTable)(_responseSanitized.ToString())
      cmb_Section.Add(_data, 0, 2)
    Catch ex As Exception
    End Try
  End Sub
End Class
