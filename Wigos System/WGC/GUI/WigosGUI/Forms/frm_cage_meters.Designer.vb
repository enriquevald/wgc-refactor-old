<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_cage_meters
  Inherits GUI_Controls.frm_base_print

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.dg_cage_meters = New GUI_Controls.uc_grid()
    Me.gb_origins_destinations = New System.Windows.Forms.GroupBox()
    Me.bt_uncheck_all_sources = New System.Windows.Forms.Button()
    Me.bt_check_all_sources = New System.Windows.Forms.Button()
    Me.dg_source_target = New GUI_Controls.uc_grid()
    Me.gb_concepts = New System.Windows.Forms.GroupBox()
    Me.bt_uncheck_all_concepts = New System.Windows.Forms.Button()
    Me.bt_check_all_concepts = New System.Windows.Forms.Button()
    Me.dg_concepts = New GUI_Controls.uc_grid()
    Me.panel_grids.SuspendLayout()
    Me.panel_filter.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_origins_destinations.SuspendLayout()
    Me.gb_concepts.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_grids
    '
    Me.panel_grids.Controls.Add(Me.dg_cage_meters)
    Me.panel_grids.Location = New System.Drawing.Point(4, 258)
    Me.panel_grids.Size = New System.Drawing.Size(976, 282)
    Me.panel_grids.Controls.SetChildIndex(Me.dg_cage_meters, 0)
    Me.panel_grids.Controls.SetChildIndex(Me.panel_buttons, 0)
    '
    'panel_buttons
    '
    Me.panel_buttons.Location = New System.Drawing.Point(888, 0)
    Me.panel_buttons.Size = New System.Drawing.Size(88, 282)
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_concepts)
    Me.panel_filter.Controls.Add(Me.gb_origins_destinations)
    Me.panel_filter.Size = New System.Drawing.Size(976, 231)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_origins_destinations, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_concepts, 0)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Location = New System.Drawing.Point(4, 235)
    Me.pn_separator_line.Size = New System.Drawing.Size(976, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(976, 4)
    '
    'dg_cage_meters
    '
    Me.dg_cage_meters.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.dg_cage_meters.CurrentCol = -1
    Me.dg_cage_meters.CurrentRow = -1
    Me.dg_cage_meters.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_cage_meters.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_cage_meters.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_cage_meters.Location = New System.Drawing.Point(3, 3)
    Me.dg_cage_meters.Name = "dg_cage_meters"
    Me.dg_cage_meters.PanelRightVisible = True
    Me.dg_cage_meters.Redraw = True
    Me.dg_cage_meters.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_cage_meters.Size = New System.Drawing.Size(879, 270)
    Me.dg_cage_meters.Sortable = False
    Me.dg_cage_meters.SortAscending = True
    Me.dg_cage_meters.SortByCol = 0
    Me.dg_cage_meters.TabIndex = 9
    Me.dg_cage_meters.ToolTipped = True
    Me.dg_cage_meters.TopRow = -2
    Me.dg_cage_meters.WordWrap = False
    '
    'gb_origins_destinations
    '
    Me.gb_origins_destinations.Controls.Add(Me.bt_uncheck_all_sources)
    Me.gb_origins_destinations.Controls.Add(Me.bt_check_all_sources)
    Me.gb_origins_destinations.Controls.Add(Me.dg_source_target)
    Me.gb_origins_destinations.Location = New System.Drawing.Point(6, 10)
    Me.gb_origins_destinations.Name = "gb_origins_destinations"
    Me.gb_origins_destinations.Size = New System.Drawing.Size(285, 217)
    Me.gb_origins_destinations.TabIndex = 11
    Me.gb_origins_destinations.TabStop = False
    Me.gb_origins_destinations.Text = "xstackers"
    '
    'bt_uncheck_all_sources
    '
    Me.bt_uncheck_all_sources.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.bt_uncheck_all_sources.Location = New System.Drawing.Point(116, 177)
    Me.bt_uncheck_all_sources.Name = "bt_uncheck_all_sources"
    Me.bt_uncheck_all_sources.Size = New System.Drawing.Size(115, 29)
    Me.bt_uncheck_all_sources.TabIndex = 2
    Me.bt_uncheck_all_sources.Text = "xDesmarcar todas"
    Me.bt_uncheck_all_sources.UseVisualStyleBackColor = True
    '
    'bt_check_all_sources
    '
    Me.bt_check_all_sources.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.bt_check_all_sources.Location = New System.Drawing.Point(10, 177)
    Me.bt_check_all_sources.Name = "bt_check_all_sources"
    Me.bt_check_all_sources.Size = New System.Drawing.Size(100, 29)
    Me.bt_check_all_sources.TabIndex = 1
    Me.bt_check_all_sources.Text = "xMarcar Todas"
    Me.bt_check_all_sources.UseVisualStyleBackColor = True
    '
    'dg_source_target
    '
    Me.dg_source_target.CurrentCol = -1
    Me.dg_source_target.CurrentRow = -1
    Me.dg_source_target.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_source_target.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_source_target.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_source_target.Location = New System.Drawing.Point(8, 20)
    Me.dg_source_target.Name = "dg_source_target"
    Me.dg_source_target.PanelRightVisible = True
    Me.dg_source_target.Redraw = True
    Me.dg_source_target.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_source_target.Size = New System.Drawing.Size(271, 151)
    Me.dg_source_target.Sortable = False
    Me.dg_source_target.SortAscending = True
    Me.dg_source_target.SortByCol = 0
    Me.dg_source_target.TabIndex = 0
    Me.dg_source_target.ToolTipped = True
    Me.dg_source_target.TopRow = -2
    Me.dg_source_target.WordWrap = False
    '
    'gb_concepts
    '
    Me.gb_concepts.Controls.Add(Me.bt_uncheck_all_concepts)
    Me.gb_concepts.Controls.Add(Me.bt_check_all_concepts)
    Me.gb_concepts.Controls.Add(Me.dg_concepts)
    Me.gb_concepts.Location = New System.Drawing.Point(297, 10)
    Me.gb_concepts.Name = "gb_concepts"
    Me.gb_concepts.Size = New System.Drawing.Size(285, 217)
    Me.gb_concepts.TabIndex = 12
    Me.gb_concepts.TabStop = False
    Me.gb_concepts.Text = "xstackers"
    '
    'bt_uncheck_all_concepts
    '
    Me.bt_uncheck_all_concepts.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.bt_uncheck_all_concepts.Location = New System.Drawing.Point(118, 177)
    Me.bt_uncheck_all_concepts.Name = "bt_uncheck_all_concepts"
    Me.bt_uncheck_all_concepts.Size = New System.Drawing.Size(115, 29)
    Me.bt_uncheck_all_concepts.TabIndex = 2
    Me.bt_uncheck_all_concepts.Text = "xDesmarcar todas"
    Me.bt_uncheck_all_concepts.UseVisualStyleBackColor = True
    '
    'bt_check_all_concepts
    '
    Me.bt_check_all_concepts.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.bt_check_all_concepts.Location = New System.Drawing.Point(8, 177)
    Me.bt_check_all_concepts.Name = "bt_check_all_concepts"
    Me.bt_check_all_concepts.Size = New System.Drawing.Size(100, 29)
    Me.bt_check_all_concepts.TabIndex = 1
    Me.bt_check_all_concepts.Text = "xMarcar Todas"
    Me.bt_check_all_concepts.UseVisualStyleBackColor = True
    '
    'dg_concepts
    '
    Me.dg_concepts.CurrentCol = -1
    Me.dg_concepts.CurrentRow = -1
    Me.dg_concepts.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_concepts.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_concepts.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_concepts.Location = New System.Drawing.Point(8, 20)
    Me.dg_concepts.Name = "dg_concepts"
    Me.dg_concepts.PanelRightVisible = True
    Me.dg_concepts.Redraw = True
    Me.dg_concepts.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_concepts.Size = New System.Drawing.Size(271, 151)
    Me.dg_concepts.Sortable = False
    Me.dg_concepts.SortAscending = True
    Me.dg_concepts.SortByCol = 0
    Me.dg_concepts.TabIndex = 0
    Me.dg_concepts.ToolTipped = True
    Me.dg_concepts.TopRow = -2
    Me.dg_concepts.WordWrap = False
    '
    'frm_cage_meters
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(984, 544)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.MaximizeBox = False
    Me.MaximumSize = New System.Drawing.Size(1000, 583)
    Me.MinimumSize = New System.Drawing.Size(750, 583)
    Me.Name = "frm_cage_meters"
    Me.Text = "frm_cage_meters"
    Me.panel_grids.ResumeLayout(False)
    Me.panel_filter.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_origins_destinations.ResumeLayout(False)
    Me.gb_concepts.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents dg_cage_meters As GUI_Controls.uc_grid
  Friend WithEvents gb_concepts As System.Windows.Forms.GroupBox
  Friend WithEvents bt_uncheck_all_concepts As System.Windows.Forms.Button
  Friend WithEvents bt_check_all_concepts As System.Windows.Forms.Button
  Friend WithEvents dg_concepts As GUI_Controls.uc_grid
  Friend WithEvents gb_origins_destinations As System.Windows.Forms.GroupBox
  Friend WithEvents bt_uncheck_all_sources As System.Windows.Forms.Button
  Friend WithEvents bt_check_all_sources As System.Windows.Forms.Button
  Friend WithEvents dg_source_target As GUI_Controls.uc_grid
End Class
