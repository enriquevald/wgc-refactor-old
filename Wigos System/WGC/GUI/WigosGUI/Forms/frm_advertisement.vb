'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_advertisement
' DESCRIPTION:   Advertisement
' AUTHOR:        Artur Nebot
' CREATION DATE: 09-MAY-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 09-MAY-2012  ANG    Initial version
' 30-JAN-2013  ANG    Fix bug #547
' 12-FEB-2013  ANG    Fix bug #576
'                     Reallocate GUI controls
' 11-MAR-2013  ICS    Changed condition in GUI_ReportFilter() to prevent 
'                     that header be incorrect when the selection changes
' 15-APR-2013  LEM    Fixed Bug #712: Error updating Name filter for Print or Excel.
' 25-APR-2013  RBG    #746 It makes no sense to work in this form with multiselection
' 22-MAY-2013  ACM    Fixed Bug #794: Filter any occurrence in description. Not just the first characters
' 05-JUN-2013  DHA    Fixed Bug #819: Modifications in the query for filters with character '%'
' 31-JUL-2017  MS     WIGOS-4092: Marketing - The Advertising campaing report is not displaying the campaing created without check the Promobox checkbox
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports System.Data.SqlClient


Public Class frm_advertisement
  Inherits GUI_Controls.frm_base_sel


#Region "Members"

  Private m_campaign_custom_filter_controls As New List(Of Control)
  Private m_filt_campaign As String
  Private m_filt_campaign_date As String
  Private m_filt_campaign_search_filter As String
  Private m_filt_date_from As String
  Private m_filt_date_to As String
  Private m_filt_enabled As Boolean
  Private m_filt_disabled As Boolean
  Private m_default_target As WSI.Common.ADS_TARGET

  Public Class ucComboBox
    Public Sub New(ByVal NewValue As Integer, ByVal NewDescription As String)
      Value = NewValue
      Description = NewDescription
    End Sub
    Public Value As Integer
    Public Description As String
    Public Overrides Function ToString() As String
      Return Description
    End Function
  End Class
#End Region

#Region " CONSTANT "


  Private Const GRID_COLUMNS As Integer = 9
  Private Const GRID_HEADER_ROWS As Integer = 2
  Private Const GRID_WIDTH_DATE As Integer = 2000
  Private Const GRID_WIDTH_NAME As Integer = 2000
  Private Const GRID_WIDTH_DESCRIPTION As Integer = 5000
  Private Const GRID_WIDTH_ENABLED As Integer = 1000
  Private Const GRID_WIDTH_WINUP_DATE_FROM As Integer = 2000
  Private Const GRID_WIDTH_WINUP_DATE_TO As Integer = 2000

  Private Const GRID_COL_INDEX As Integer = 0
  Private Const GRID_COL_ID As Integer = 1
  Private Const GRID_COL_NAME As Integer = 2
  Private Const GRID_COL_DESCRIPTION As Integer = 3
  Private Const GRID_COL_ENABLED As Integer = 4
  Private Const GRID_COL_START_DATE As Integer = 5
  Private Const GRID_COL_END_DATE As Integer = 6
  Private Const GRID_COL_WINUP_DATE_FROM As Integer = 7
  Private Const GRID_COL_WINUP_DATE_TO As Integer = 8


  Private Const SQL_COL_ID As Integer = 0
  Private Const SQL_COL_NAME As Integer = 1
  Private Const SQL_COL_DESCRIPTION As Integer = 2
  Private Const SQL_COL_START_DATE As Integer = 3
  Private Const SQL_COL_END_DATE As Integer = 4
  Private Const SQL_COL_ENABLED As Integer = 5
  Private Const SQL_COL_PROMOBOX_ENABLED As Integer = 6
  Private Const SQL_COL_WINUP_ENABLED As Integer = 7
  Private Const SQL_COL_WINUP_DATE_FROM As Integer = 8
  Private Const SQL_COL_WINUP_DATE_TO As Integer = 9

#End Region

#Region "Constructor"

  Public Sub New()
    Me.New(WSI.Common.ADS_TARGET.Other)
  End Sub

  Public Sub New(ByVal DefaultTarget As WSI.Common.ADS_TARGET)
    m_default_target = DefaultTarget
    Me.InitializeComponent()
  End Sub

#End Region

#Region " OVERRIDES "


  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean


    Dim _start_date As String
    Dim _end_date As String
    Dim _winup_date_from As String
    Dim _winup_date_to As String
    Dim _enabled As String


    _start_date = GUI_FormatDate(DbRow.Value(SQL_COL_START_DATE), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    _end_date = GUI_FormatDate(DbRow.Value(SQL_COL_END_DATE), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    
    Me.Grid.Cell(RowIndex, GRID_COL_ID).Value = DbRow.Value(SQL_COL_ID).ToString()
    Me.Grid.Cell(RowIndex, GRID_COL_NAME).Value = DbRow.Value(SQL_COL_NAME).ToString()
    Me.Grid.Cell(RowIndex, GRID_COL_DESCRIPTION).Value = DbRow.Value(SQL_COL_DESCRIPTION).ToString
    If CType(DbRow.Value(SQL_COL_PROMOBOX_ENABLED), Boolean) Then
      Me.Grid.Cell(RowIndex, GRID_COL_START_DATE).Value = _start_date
      Me.Grid.Cell(RowIndex, GRID_COL_END_DATE).Value = _end_date
    Else
      Me.Grid.Cell(RowIndex, GRID_COL_START_DATE).Value = String.Empty
      Me.Grid.Cell(RowIndex, GRID_COL_END_DATE).Value = String.Empty
    End If
    

    If CType(DbRow.Value(SQL_COL_ENABLED), Boolean) Then
      _enabled = GLB_NLS_GUI_PLAYER_TRACKING.GetString(698)
    Else
      _enabled = GLB_NLS_GUI_PLAYER_TRACKING.GetString(699) ' NO
    End If




    Me.Grid.Cell(RowIndex, GRID_COL_ENABLED).Value = _enabled

    If WSI.Common.GeneralParam.GetBoolean("Features", "WinUP", False) Then
      If CType(DbRow.Value(SQL_COL_WINUP_ENABLED), Boolean) Then
        If Not IsDBNull(DbRow.Value(SQL_COL_WINUP_DATE_FROM)) Then
          _winup_date_from = GUI_FormatDate(DbRow.Value(SQL_COL_WINUP_DATE_FROM), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
          Me.Grid.Cell(RowIndex, GRID_COL_WINUP_DATE_FROM).Value = _winup_date_from
        End If

        If Not IsDBNull(DbRow.Value(SQL_COL_WINUP_DATE_TO)) Then
          _winup_date_to = GUI_FormatDate(DbRow.Value(SQL_COL_WINUP_DATE_TO), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
          Me.Grid.Cell(RowIndex, GRID_COL_WINUP_DATE_TO).Value = _winup_date_to
        End If
      End If
    End If
    Return True


  End Function

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_ADVERTISEMENT

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId


  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()


    ' Advertisement
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(950)  ' Label form caption

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(3)

    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.ef_campaign_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 100)


    Me.dtp_now.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_past.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_future.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    Me.dtp_now.Enabled = False
    Me.dtp_past.Enabled = False
    Me.dtp_future.Enabled = False

    ' INITIALIZE LABEL CAPTIONS

    Me.gb_campaign.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(692)  ' Label campaign
    Me.ef_campaign_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(693) ' Label name
    Me.ef_campaign_name.Enabled = True

    Me.gb_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(708)
    Me.dtp_from.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(695) ' From
    Me.dtp_to.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(696) ' To

    Me.gb_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(697) ' Enabled
    Me.chk_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(698)
    Me.chk_disabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(699) ' NO

    Me.opt_custom.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(701) ' Custom filter
    Me.opt_now.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(703) ' NOW
    Me.opt_future.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(704) ' FUTURE
    Me.opt_past.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(705) ' PAST

    Me.m_campaign_custom_filter_controls.Add(Me.gb_date)
    Me.m_campaign_custom_filter_controls.Add(Me.gb_enabled)

    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()


  End Sub ' GUI_InitControls




  ' PURPOSE: Get the defined Query Type
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_QUERY_TYPE
  '

  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_COMMAND
  End Function ' GUI_GetQueryType

  ' PURPOSE: Build an SQL Command query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL Command query ready to send to the database

  Protected Overrides Function GUI_GetSqlCommand() As SqlCommand

    Dim _sql_command As SqlCommand
    Dim _campaign_name As String
    Dim _initial_date As Nullable(Of Date)
    Dim _final_date As Nullable(Of Date)
    Dim _current_date As Nullable(Of Date)

    If Me.opt_now.Checked Then
      _current_date = Me.dtp_now.Value
    ElseIf Me.opt_future.Checked Then
      _current_date = Me.dtp_future.Value
    ElseIf Me.opt_past.Checked Then
      _current_date = Me.dtp_past.Value
    Else
      _current_date = Nothing
    End If

    _initial_date = IIf(dtp_from.Enabled, dtp_from.Value, Nothing)
    _final_date = IIf(dtp_to.Enabled, dtp_to.Value, Nothing)
    _campaign_name = Me.ef_campaign_name.Value

    Dim _str_bld As System.Text.StringBuilder
    _str_bld = New System.Text.StringBuilder()

    _str_bld.AppendLine("SELECT  ")
    _str_bld.AppendLine(" AD_ID, ")
    _str_bld.AppendLine(" AD_NAME, ")
    _str_bld.AppendLine(" AD_DESCRIPTION, ")
    _str_bld.AppendLine(" AD_FROM, ")
    _str_bld.AppendLine(" AD_TO, ")
    _str_bld.AppendLine(" AD_ENABLED, ")
    _str_bld.AppendLine(" AD_PROMOBOX_ENABLED ")

    If WSI.Common.GeneralParam.GetBoolean("Features", "WinUP", False) Then
      _str_bld.AppendLine(" ,AD_WINUP_ENABLED ")
      _str_bld.AppendLine(" ,AD_WINUP_VALID_FROM ")
      _str_bld.AppendLine(" ,AD_WINUP_VALID_TO ")
    End If
    _str_bld.AppendLine(" FROM WKT_ADS ")

    _str_bld.AppendLine(GetSqlWhere(_campaign_name, _initial_date, _final_date))

    _sql_command = New SqlCommand(_str_bld.ToString())

    If Me.opt_custom.Checked And _initial_date.HasValue Then
      _sql_command.Parameters.Add("@pStartDate", SqlDbType.DateTime).Value = _initial_date.Value
    End If

    If Me.opt_custom.Checked And _initial_date.HasValue Then
      _sql_command.Parameters.Add("@pEndDate", SqlDbType.DateTime).Value = _final_date.Value
    End If

    If Not String.IsNullOrEmpty(_campaign_name) Then
      _sql_command.Parameters.Add("@pNameFilter", SqlDbType.NVarChar).Value = _campaign_name.Replace("%", "\%")
    End If

    If _current_date.HasValue Then
      _sql_command.Parameters.Add("@pCurrentDate", SqlDbType.DateTime).Value = _current_date.Value
    End If

    Return _sql_command

  End Function

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset  


  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted

  Protected Overrides Function GUI_FilterCheck() As Boolean
    ' Date selection 
    If Me.opt_custom.Checked And Me.dtp_from.Value > Me.dtp_to.Value Then
      Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call Me.dtp_to.Focus()

      Return False
    End If


    Return True
  End Function ' GUI_FilterCheck  


  ' PURPOSE : Activated when a row of the grid is double clicked or selected, so it can be edited
  '
  '  PARAMS :
  '     - INPUT :
  '               
  '     - OUTPUT :
  '          
  ' RETURNS :

  Protected Overrides Sub GUI_EditSelectedItem()

    Dim _idx_row As Int32
    Dim _adv_id As Integer
    Dim _adv_name As String
    Dim _frm_edit As frm_ads_steps_edit 'GUI_Controls.frm_base_edit
    Dim _rows_to_move() As Integer

    _rows_to_move = Me.Grid.SelectedRows()

    ' ICS 18-DEC-2012: Check if there is a row selected
    If _rows_to_move Is Nothing Then
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(130), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return
    End If

    _idx_row = _rows_to_move(0)
    _adv_id = Me.Grid.Cell(_idx_row, GRID_COL_ID).Value
    _adv_name = Me.Grid.Cell(_idx_row, GRID_COL_NAME).Value

    _frm_edit = New frm_ads_steps_edit

    Call _frm_edit.ShowEditItem(_adv_id, _adv_name)

    _frm_edit = Nothing

    Call Me.Grid.Focus()

  End Sub ' GUI_EditSelectedItem


  ' PURPOSE : Process button actions in order to branch to a child screen
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_NEW
        Call ShowNewAdsForm()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        Call GUI_EditSelectedItem()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

#Region " GUI Reports "

  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    With Me.Grid

    End With

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE



  End Sub ' GUI_ReportParams

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    Dim _enabled As String = ""

    'Name filter
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(693), m_filt_campaign_search_filter) ' Label Name

    ' Show Initial and Final date or Now/Past/Future
    If Not String.IsNullOrEmpty(m_filt_campaign) Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(731, m_filt_campaign), m_filt_campaign_date)

    Else
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(708) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(695), m_filt_date_from) ' Initial date
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(708) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(696), m_filt_date_to) ' Final date

      ' Show search filter
      If (m_filt_enabled Xor m_filt_disabled) Or (m_filt_enabled And m_filt_disabled) Then

        If m_filt_enabled Then
          _enabled &= GLB_NLS_GUI_PLAYER_TRACKING.GetString(698)
        End If
        If m_filt_disabled Then
          If Not String.IsNullOrEmpty(_enabled) Then _enabled &= ", "
          _enabled &= GLB_NLS_GUI_PLAYER_TRACKING.GetString(699) ' NO
        End If

        PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(711), _enabled) ' Enabled 2


      End If

    End If



    PrintData.FilterHeaderWidth(1) = 1500
    PrintData.FilterValueWidth(1) = 3000


  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()

    ' Name
    m_filt_campaign_search_filter = ef_campaign_name.Value

    If Not Me.opt_custom.Checked Then
      If opt_future.Checked Then
        m_filt_campaign = GLB_NLS_GUI_PLAYER_TRACKING.GetString(704) ' FUTURE
        m_filt_campaign_date = GUI_FormatDate(Me.dtp_future.Value, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
      ElseIf opt_now.Checked Then
        m_filt_campaign = GLB_NLS_GUI_PLAYER_TRACKING.GetString(703) ' NOW
        m_filt_campaign_date = GUI_FormatDate(Me.dtp_now.Value, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
      ElseIf opt_past.Checked Then
        m_filt_campaign = GLB_NLS_GUI_PLAYER_TRACKING.GetString(705) ' PAST
        m_filt_campaign_date = GUI_FormatDate(Me.dtp_past.Value, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
      End If
      m_filt_date_from = ""
      m_filt_date_to = ""
    Else

      ' Date
      m_filt_date_from = GUI_FormatDate(Me.dtp_from.Value, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
      m_filt_date_to = GUI_FormatDate(Me.dtp_to.Value, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
      m_filt_campaign = ""
      m_filt_campaign_date = ""

      ' Enable / Disabled
      m_filt_disabled = chk_disabled.Checked
      m_filt_enabled = chk_enabled.Checked
    End If


  End Sub ' GUI_ReportUpdateFilters

#End Region

#End Region

#Region " Private Functions "

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub GUI_StyleSheet()
    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
      .Sortable = False


      With .Column(GRID_COL_INDEX)
        .Header(0).Text = " "
        .Header(1).Text = " "
        .Width = 200
        .HighLightWhenSelected = False
        .IsColumnPrintable = False
      End With

      ' ID
      With .Column(GRID_COL_ID)
        .Width = 0
      End With
      'NAME
      With .Column(GRID_COL_NAME)
        .Header(0).Text = " "
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(693) ' Label Name
        .Width = GRID_WIDTH_NAME
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      End With

      'DESCRIPTION
      With .Column(GRID_COL_DESCRIPTION)
        .Header(0).Text = " "
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(707) ' Header description
        .Width = GRID_WIDTH_DESCRIPTION
      End With

      ' ENABLED
      With .Column(GRID_COL_ENABLED)
        .Header(0).Text = " "
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(697) ' Enabled
        .Width = GRID_WIDTH_ENABLED
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      End With

      ' START DATE
      With .Column(GRID_COL_START_DATE)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7932)
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(708)
        .Width = GRID_WIDTH_DATE
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      End With

      ' END DATE
      With .Column(GRID_COL_END_DATE)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7932)
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(709) ' Final date
        .Width = GRID_WIDTH_DATE
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      End With

      If WSI.Common.GeneralParam.GetBoolean("Features", "WinUP", False) Then
        ' WINUP DATE FROM
        With .Column(GRID_COL_WINUP_DATE_FROM)
          .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7933)
          .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(708) ' WINUP DATE FROM
          .Width = GRID_WIDTH_WINUP_DATE_FROM
          .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
        End With
        ' WINUP DATE TO
        With .Column(GRID_COL_WINUP_DATE_TO)
          .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7933)
          .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(709) ' Final date
          .Width = GRID_WIDTH_WINUP_DATE_TO
          .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
        End With
      Else
        With .Column(GRID_COL_WINUP_DATE_FROM)
          .Header(0).Text = ""
          .Header(1).Text = "" ' WINUP DATE FROM
          .Width = 0
          .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
        End With
        ' WINUP DATE TO
        With .Column(GRID_COL_WINUP_DATE_TO)
          .Header(0).Text = ""
          .Header(1).Text = "" ' Final date
          .Width = 0
          .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
        End With
      End If

    End With
  End Sub

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub SetDefaultValues()


    Dim _current_date As Date
    Dim _initial_date As Date
    Dim _final_date As Date

    _current_date = WSI.Common.WGDB.Now

    _initial_date = New Date(_current_date.Year, _current_date.Month, 1)
    _final_date = _initial_date.AddMonths(1)

    Me.dtp_from.Value = _initial_date
    Me.dtp_to.Value = _final_date

    Me.dtp_now.Value = _current_date
    Me.dtp_past.Value = _current_date
    Me.dtp_future.Value = _current_date

    Me.ef_campaign_name.Value = ""

    Me.chk_enabled.Checked = True

    Me.opt_now.Checked = True
    Me.opt_campaign_Checkedchanged(Me.opt_now, New System.EventArgs())


  End Sub


  ' PURPOSE: Get SQL WHERE 
  '     
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String

  Private Function GetSqlWhere(ByVal _campaign_name As String, ByVal _initial_date As Nullable(Of Date), _
                                ByVal _final_date As Nullable(Of Date)) As String

    Dim _str_winup_disabled_condition As String = ""
    Dim _str_bld As System.Text.StringBuilder
    _str_bld = New System.Text.StringBuilder

    _str_bld.AppendLine(" WHERE 1=1 ")

    If Not WSI.Common.GeneralParam.GetBoolean("Features", "WinUP", False) Then
      _str_winup_disabled_condition = " 1=2 AND "
    End If
    If Not Me.opt_custom.Checked Then
      If opt_future.Checked Then
        _str_bld.AppendLine(" AND ((" & _str_winup_disabled_condition & " AD_WINUP_VALID_FROM > @pCurrentDate) OR (AD_FROM > @pCurrentDate))")
      ElseIf opt_now.Checked Then
        _str_bld.AppendLine(" AND ((" & _str_winup_disabled_condition & " AD_WINUP_VALID_FROM <= @pCurrentDate AND AD_WINUP_VALID_TO >= @pCurrentDate) OR (AD_FROM <= @pCurrentDate AND AD_TO >= @pCurrentDate))")
      ElseIf opt_past.Checked Then
        _str_bld.AppendLine(" AND ((" & _str_winup_disabled_condition & " AD_WINUP_VALID_TO < @pCurrentDate) OR (AD_TO < @pCurrentDate))")
      End If
    Else
      If _initial_date.HasValue Then
        _str_bld.AppendLine(" AND ((" & _str_winup_disabled_condition & " AD_WINUP_VALID_FROM >= @pStartDate) OR (AD_FROM >= @pStartDate))")
      End If

      If _final_date.HasValue Then
        _str_bld.AppendLine("	AND ((" & _str_winup_disabled_condition & " AD_WINUP_VALID_FROM <= @pEndDate) OR (AD_FROM <= @pEndDate))")
      End If
    End If
  

    If Not String.IsNullOrEmpty(_campaign_name) Then
      _str_bld.AppendLine(" AND AD_NAME like '%'+@pNameFilter+'%' ESCAPE '\'")

    End If

    If Not Me.opt_custom.Checked Then
      _str_bld.AppendLine(" AND AD_ENABLED= 1")
    Else
      If (chk_enabled.Checked Xor chk_disabled.Checked) Then
        If chk_enabled.Checked Then
          _str_bld.AppendLine(" AND AD_ENABLED= 1")
        ElseIf chk_disabled.Checked Then
          _str_bld.AppendLine(" AND AD_ENABLED= 0")
        End If

      End If
    End If


    Return _str_bld.ToString()


  End Function

  Private Sub ShowNewAdsForm()

    Dim frm_edit As frm_ads_steps_edit

    frm_edit = New frm_ads_steps_edit
    Call frm_edit.ShowNewItem()

    frm_edit = Nothing

    Call Me.Grid.Focus()

  End Sub ' ShowNewAdsForm

  Private Function TargetName(ByVal Target As WSI.Common.ADS_TARGET) As String

    Dim _str_target As String

    _str_target = ""

    Select Case Target
      Case WSI.Common.ADS_TARGET.Other
        _str_target = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7455) ' other
      Case WSI.Common.ADS_TARGET.Dining
        _str_target = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7456) ' dining
      Case WSI.Common.ADS_TARGET.Events
        _str_target = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7458) ' events
      Case WSI.Common.ADS_TARGET.RecentWinners
        _str_target = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7457) ' recesntwinners
      Case WSI.Common.ADS_TARGET.Promotions
        _str_target = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7581) ' promotions
      Case WSI.Common.ADS_TARGET.CasinoOffers
        _str_target = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7584) ' casinooffer

      Case Else
        _str_target = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7455) ' other
    End Select


    Return _str_target

  End Function

#End Region

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION
    Me.MdiParent = MdiParent

    Me.Display(False)

  End Sub ' ShowForEdit

#End Region

#Region " Events "


  Private Sub opt_campaign_Checkedchanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_past.CheckedChanged, opt_now.CheckedChanged, opt_future.CheckedChanged, opt_custom.CheckedChanged

    Dim _rb As RadioButton
    _rb = CType(sender, RadioButton)

    Me.dtp_past.Enabled = Not _rb.Checked
    Me.dtp_future.Enabled = Not _rb.Checked
    Me.dtp_now.Enabled = Not _rb.Checked

    Select Case _rb.Name
      Case Me.opt_future.Name
        Me.dtp_future.Enabled = _rb.Checked
      Case Me.opt_now.Name
        Me.dtp_now.Enabled = _rb.Checked
      Case Me.opt_past.Name
        Me.dtp_past.Enabled = _rb.Checked
      Case Me.opt_custom.Name
        For Each _opt_item As Control In m_campaign_custom_filter_controls
          _opt_item.Enabled = _rb.Checked
        Next
    End Select



  End Sub

#End Region



End Class