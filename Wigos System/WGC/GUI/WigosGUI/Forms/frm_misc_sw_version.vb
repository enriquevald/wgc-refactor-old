'-------------------------------------------------------------------
' Copyright � 2009 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_misc_sw_version.vb
' DESCRIPTION:   Monitor for miscellaneous software versions
' AUTHOR:        Agust� Poch
' CREATION DATE: 07-APR-2009
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 07-APR-2009  APB    Initial Draft.
' 17-APR-2013  DMR    Changing cashier name for its alias
' 16-JUL-2013  JCA    Add PSA_Client
' 15-JAN-2015  SGB    Fixed Bug WIG-1931: Change format of column app version in excel
'--------------------------------------------------------------------
Option Strict Off
Option Explicit On

Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_Controls.uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE
Imports GUI_CommonMisc
Imports WSI.Common
Imports System.Text
Imports System.Data.SqlClient

Public Class frm_misc_sw_version
  Inherits frm_base

#Region "Constants"

  'Excel format column
  Private Const EXCEL_COLUMN_APP_VERSION = 2

  ' Grid Columns
  Private Const GRID_COLUMNS As Integer = 9
  Private Const GRID_HEADER_ROWS As Integer = 1

  Private Const GRID_COLUMN_SENTINEL = 0
  Private Const GRID_COLUMN_APP_NAME = 1
  Private Const GRID_COLUMN_APP_VERSION = 2
  Private Const GRID_COLUMN_APP_COMPUTER = 3
  Private Const GRID_COLUMN_IP_ADDRESS = 4
  Private Const GRID_COLUMN_OS_USERNAME = 5
  Private Const GRID_COLUMN_APP_USERNAME = 6
  Private Const GRID_COLUMN_APP_LAST_ACCESS = 7
  Private Const GRID_COLUMN_UPDATE_STATUS = 8

  Private Const SQL_COLUMN_APP_NAME = 0
  Private Const SQL_COLUMN_APP_VERSION = 1
  Private Const SQL_COLUMN_APP_COMPUTER = 2
  Private Const SQL_COLUMN_IP_ADDRESS = 3
  Private Const SQL_COLUMN_OS_USERNAME = 4
  Private Const SQL_COLUMN_APP_USERNAME = 5
  Private Const SQL_COLUMN_APP_LAST_ACCESS = 6

  ' Grid counters
  Private Const COUNTER_APP_UP_TO_DATE As Integer = 1
  Private Const COUNTER_APP_OUTDATED As Integer = 2

  ' Timer interval to refresh services status (in milliseconds)
  Private Const REFRESH_TIMEOUT = 5000

  Private Const APP_TYPE_CASHIER = "CASHIER"
  Private Const APP_TYPE_GUI = "WigosGUI"
  Private Const APP_TYPE_MULTISITE_GUI = "WigosMultiSiteGUI"
  Private Const APP_TYPE_CASH_DESK_DRAW_GUI = "WigosDrawGUI"
  Private Const APP_TYPE_SERVICE_DRAW = "" 'reserved for draw service
  Private Const APP_TYPE_SERVICE_WCP = "WCP_Service"
  Private Const APP_TYPE_SERVICE_WC2 = "WC2_Service"
  Private Const APP_TYPE_SERVICE_WXP = "WXP_Service"
  Private Const APP_TYPE_SERVICE_WWP_SERVER = "WWP_CenterService"
  Private Const APP_TYPE_SERVICE_WWP_SITE_SERVICE = "WWP_SiteService"
  Private Const APP_TYPE_SERVICE_PSA_CLIENT_SERVICE = "PSA_Client"
  Private Const APP_TYPE_SERVICE_SMARTFLOOR_DATA_SERVICE = "DATAService"

#End Region

#Region "Enums"
  Private Enum ENUM_UPDATE_STATUS
    STATUS_OUTDATED = 0
    STATUS_TO_DATE = 1
  End Enum

  Private Enum ENUM_APP_TYPE
    APP_TYPE_CASHIER = 0
    APP_TYPE_KIOSK = 1
    APP_TYPE_GUI = 2
    APP_TYPE_SERVICE = 3
    APP_TYPE_MULTISITE_GUI = 4
    APP_TYPE_CASHDESK_DRAWS_GUI = 5
  End Enum

#End Region ' Enums

#Region "members"
  Private m_application As String
  Private m_print_data As GUI_Reports.CLASS_PRINT_DATA
  Private m_excel_data As GUI_Reports.CLASS_EXCEL_DATA
  Private m_print_datetime As Date
#End Region

#Region "Overrides"

  ' PURPOSE: Set the internal Form Id
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_MISC_SW_BY_BUILD

    'Call Base Form proc
    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Initialize all form controls
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     

  Protected Overrides Sub GUI_InitControls()

    Dim fore_color As Color
    Dim back_color As Color

    Call MyBase.GUI_InitControls()

    ' Form Title
    Me.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(239)

    Me.btn_exit.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(2)
    Me.btn_clean.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5208)
    Me.btn_excel.Text = GLB_NLS_GUI_CONTROLS.GetString(27)
    Me.btn_print.Text = GLB_NLS_GUI_CONTROLS.GetString(5)

    ' Terminal Type
    Me.gb_app_type.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(240)
    Me.opt_all_types.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(209)
    Me.opt_services.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(241)
    Me.opt_wigos_gui.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(242)
    Me.opt_win_cashier.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(243)
    Me.opt_all_types.Checked = True

    If GLB_GuiMode = ENUM_GUI.MULTISITE_GUI Then
      Me.opt_win_cashier.Visible = False
      Me.opt_wigos_gui.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(256)
    End If

    If GLB_GuiMode = ENUM_GUI.CASHDESK_DRAWS_GUI Then
      Me.opt_win_cashier.Visible = False
      Me.opt_wigos_gui.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(339)
    End If

    Me.gb_status.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(219)
    Me.tf_to_date.Value = GLB_NLS_GUI_SW_DOWNLOAD.GetString(220)
    GetUpdateStatusColor(ENUM_UPDATE_STATUS.STATUS_TO_DATE, _
                          fore_color, _
                          back_color)
    lbl_running.BackColor = back_color
    lbl_running.ForeColor = fore_color

    Me.tf_outdated.Value = GLB_NLS_GUI_SW_DOWNLOAD.GetString(221)
    GetUpdateStatusColor(ENUM_UPDATE_STATUS.STATUS_OUTDATED, _
                          fore_color, _
                          back_color)
    lbl_standby.BackColor = back_color
    lbl_standby.ForeColor = fore_color

    Me.tf_latest_gui_build.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(222)
    Me.tf_latest_gui_build.Value = GLB_NLS_GUI_SW_DOWNLOAD.GetString(224)

    Me.tf_latest_cashier_build.Visible = False
    If GLB_GuiMode = ENUM_GUI.WIGOS_GUI Then
      Me.tf_latest_cashier_build.Visible = True
      Me.tf_latest_cashier_build.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(222)
      Me.tf_latest_cashier_build.Value = GLB_NLS_GUI_SW_DOWNLOAD.GetString(224)
    End If

    Me.Timer1.Interval = REFRESH_TIMEOUT

    Me.btn_clean.Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_MISC_SW_BY_BUILD).Delete

    GUI_StyleSheet()
    RefreshGrid()

    Me.Timer1.Start()

  End Sub ' GUI_InitControls

#End Region ' Overrides

#Region "Private Functions"

  ' PURPOSE: Refresh data in the grid with fresh info from database
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     

  Private Sub RefreshGrid()

    Dim data_table As DataTable
    Dim idx_rows As Integer
    Dim row_index As Integer
    Dim data_row As DataRow
    Dim fore_color As Color
    Dim back_color As Color
    Dim num_updated As Integer
    Dim num_outdated As Integer

    Try

      Call RefreshVersion()
      Call GUI_ReportUpdateFilters()

      ' Data Grid
      data_table = GUI_GetTableUsingSQL(GetSqlQuery(), 5000)
      If IsNothing(data_table) Then
        Exit Sub
      End If

      num_updated = 0
      num_outdated = 0

      If Me.dg_list.NumRows > data_table.Rows.Count Then
        ' Done bottom-up to avoid exception arising from FlexGrid
        For idx_rows = Me.dg_list.NumRows - 1 To data_table.Rows.Count Step -1
          Me.dg_list.DeleteRowFast(idx_rows)
        Next
      End If

      If Me.dg_list.NumRows < data_table.Rows.Count Then
        For idx_rows = Me.dg_list.NumRows To data_table.Rows.Count - 1
          Me.dg_list.AddRow()
        Next
      End If

      row_index = 0
      For Each data_row In data_table.Rows

        Me.dg_list.Cell(row_index, GRID_COLUMN_APP_NAME).Value = data_row.Item(SQL_COLUMN_APP_NAME)

        If (Not IsDBNull(data_row.Item(SQL_COLUMN_APP_VERSION))) Then
          Me.dg_list.Cell(row_index, GRID_COLUMN_APP_VERSION).Value = data_row.Item(SQL_COLUMN_APP_VERSION)
        Else
          Me.dg_list.Cell(row_index, GRID_COLUMN_APP_VERSION).Value = ""
        End If

        If (Not IsDBNull(data_row.Item(SQL_COLUMN_APP_COMPUTER))) Then
          If (Me.dg_list.Cell(row_index, GRID_COLUMN_APP_NAME).Value = APP_TYPE_CASHIER) Then
            Me.dg_list.Cell(row_index, GRID_COLUMN_APP_COMPUTER).Value = Computer.Alias(data_row.Item(SQL_COLUMN_APP_COMPUTER))
          Else
            Me.dg_list.Cell(row_index, GRID_COLUMN_APP_COMPUTER).Value = data_row.Item(SQL_COLUMN_APP_COMPUTER)
          End If
        Else
          Me.dg_list.Cell(row_index, GRID_COLUMN_APP_COMPUTER).Value = ""
        End If

        If (Not IsDBNull(data_row.Item(SQL_COLUMN_IP_ADDRESS))) Then
          Me.dg_list.Cell(row_index, GRID_COLUMN_IP_ADDRESS).Value = data_row.Item(SQL_COLUMN_IP_ADDRESS)
        Else
          Me.dg_list.Cell(row_index, GRID_COLUMN_IP_ADDRESS).Value = ""
        End If

        If (Not IsDBNull(data_row.Item(SQL_COLUMN_OS_USERNAME))) Then
          Me.dg_list.Cell(row_index, GRID_COLUMN_OS_USERNAME).Value = data_row.Item(SQL_COLUMN_OS_USERNAME)
        Else
          Me.dg_list.Cell(row_index, GRID_COLUMN_OS_USERNAME).Value = ""
        End If

        If (Not IsDBNull(data_row.Item(SQL_COLUMN_APP_USERNAME))) Then
          Me.dg_list.Cell(row_index, GRID_COLUMN_APP_USERNAME).Value = data_row.Item(SQL_COLUMN_APP_USERNAME)
        Else
          Me.dg_list.Cell(row_index, GRID_COLUMN_APP_USERNAME).Value = ""
        End If

        If (Not IsDBNull(data_row.Item(SQL_COLUMN_APP_LAST_ACCESS))) Then
          Me.dg_list.Cell(row_index, GRID_COLUMN_APP_LAST_ACCESS).Value = GUI_FormatDate(data_row.Item(SQL_COLUMN_APP_LAST_ACCESS), _
                                                                                                       ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                                       ENUM_FORMAT_TIME.FORMAT_HHMMSS)
        Else
          Me.dg_list.Cell(row_index, GRID_COLUMN_APP_LAST_ACCESS).Value = ""
        End If

        Select Case Me.dg_list.Cell(row_index, GRID_COLUMN_APP_NAME).Value

          Case "CASHIER"
            If Me.dg_list.Cell(row_index, GRID_COLUMN_APP_VERSION).Value = Me.tf_latest_cashier_build.Value Then
              Me.dg_list.Cell(row_index, GRID_COLUMN_UPDATE_STATUS).Value = ENUM_UPDATE_STATUS.STATUS_TO_DATE
            Else
              Me.dg_list.Cell(row_index, GRID_COLUMN_UPDATE_STATUS).Value = ENUM_UPDATE_STATUS.STATUS_OUTDATED
            End If

          Case "WigosGUI"
            If Me.dg_list.Cell(row_index, GRID_COLUMN_APP_VERSION).Value = Me.tf_latest_gui_build.Value Then
              Me.dg_list.Cell(row_index, GRID_COLUMN_UPDATE_STATUS).Value = ENUM_UPDATE_STATUS.STATUS_TO_DATE
            Else
              Me.dg_list.Cell(row_index, GRID_COLUMN_UPDATE_STATUS).Value = ENUM_UPDATE_STATUS.STATUS_OUTDATED
            End If

          Case "WigosMultiSiteGUI"
            If Me.dg_list.Cell(row_index, GRID_COLUMN_APP_VERSION).Value = Me.tf_latest_gui_build.Value Then
              Me.dg_list.Cell(row_index, GRID_COLUMN_UPDATE_STATUS).Value = ENUM_UPDATE_STATUS.STATUS_TO_DATE
            Else
              Me.dg_list.Cell(row_index, GRID_COLUMN_UPDATE_STATUS).Value = ENUM_UPDATE_STATUS.STATUS_OUTDATED
            End If

          Case "WigosDrawGUI"
            If Me.dg_list.Cell(row_index, GRID_COLUMN_APP_VERSION).Value = Me.tf_latest_gui_build.Value Then
              Me.dg_list.Cell(row_index, GRID_COLUMN_UPDATE_STATUS).Value = ENUM_UPDATE_STATUS.STATUS_TO_DATE
            Else
              Me.dg_list.Cell(row_index, GRID_COLUMN_UPDATE_STATUS).Value = ENUM_UPDATE_STATUS.STATUS_OUTDATED
            End If

          Case Else
            Me.dg_list.Cell(row_index, GRID_COLUMN_UPDATE_STATUS).Value = ENUM_UPDATE_STATUS.STATUS_TO_DATE

        End Select

        GetUpdateStatusColor(Me.dg_list.Cell(row_index, GRID_COLUMN_UPDATE_STATUS).Value, fore_color, back_color)
        Me.dg_list.Row(row_index).ForeColor = fore_color
        Me.dg_list.Row(row_index).BackColor = back_color

        ' Update counters
        If Me.dg_list.Cell(row_index, GRID_COLUMN_UPDATE_STATUS).Value = ENUM_UPDATE_STATUS.STATUS_TO_DATE Then
          num_updated += 1
        Else
          num_outdated += 1
        End If

        row_index += 1
      Next

      Me.dg_list.Counter(COUNTER_APP_UP_TO_DATE).Value = num_updated
      Me.dg_list.Counter(COUNTER_APP_OUTDATED).Value = num_outdated

    Catch ex As Exception

    End Try

  End Sub ' RefreshGrid

  ' PURPOSE: Build the SQL query to obtain the relevant data from database
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     

  Private Function GetCurrentSwBuild(ByVal SwType As ENUM_APP_TYPE, ByRef CurrentClient As Integer, ByRef CurrentBuild As Integer) As Boolean

    Dim _sql_str As StringBuilder
    Dim data_table As DataTable
    Dim app_type As Integer = -1

    Select Case SwType
      Case ENUM_APP_TYPE.APP_TYPE_CASHIER
        app_type = 0

      Case ENUM_APP_TYPE.APP_TYPE_KIOSK
        ' Nothing to do

      Case ENUM_APP_TYPE.APP_TYPE_GUI
        app_type = 2

      Case ENUM_APP_TYPE.APP_TYPE_SERVICE
        ' Nothing to do

      Case ENUM_APP_TYPE.APP_TYPE_MULTISITE_GUI
        app_type = 12

      Case ENUM_APP_TYPE.APP_TYPE_CASHDESK_DRAWS_GUI
        app_type = 15

      Case Else
        ' Nothing to do
    End Select

    If app_type = -1 Then
      ' Incorrect application type
      Return False
    End If

    _sql_str = New StringBuilder()
    _sql_str.AppendLine("  SELECT TOP 1 tsv_client_id, tsv_build_id ")
    _sql_str.AppendLine("    FROM terminal_software_versions ")
    _sql_str.AppendFormat(" WHERE tsv_terminal_type = {0} ", app_type)
    _sql_str.AppendLine("ORDER BY tsv_insertion_date DESC")

    data_table = GUI_GetTableUsingSQL(_sql_str.ToString(), 10)

    If (data_table.Rows.Count = 0) OrElse IsNothing(data_table) Then

      Return False
    End If

    CurrentClient = data_table.Rows(0).Item(0)
    CurrentBuild = data_table.Rows(0).Item(1)

    Return True

  End Function ' GetCurrentSwBuild

  ' PURPOSE: Build the SQL query to obtain the relevant data from database
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     

  Private Function GetSqlQuery() As String

    Dim _sql_str As StringBuilder

    _sql_str = New StringBuilder()
    _sql_str.AppendLine("   SELECT   APP_NAME ")
    _sql_str.AppendLine("          , APP_VERSION ")
    _sql_str.AppendLine("          , APP_MACHINE ")
    _sql_str.AppendLine("          , APP_IP_ADDRESS ")
    _sql_str.AppendLine("          , APP_OS_USERNAME ")
    _sql_str.AppendLine("          , APP_LOGIN_NAME ")
    _sql_str.AppendLine("          , APP_LAST_ACCESS ")
    _sql_str.AppendLine("     FROM   APPLICATIONS ")
    _sql_str.AppendLine(GetSqlWhere())
    _sql_str.AppendLine(" ORDER BY   APP_NAME, APP_VERSION ")

    Return _sql_str.ToString()

  End Function ' GetSqlQuery

  ' PURPOSE: Build the SQL where condition 
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     

  Private Function GetSqlWhere() As String

    Dim sql_where As String = ""

    If Me.opt_all_types.Checked Then
      sql_where = ""
    End If

    If Me.opt_services.Checked Then
      If GLB_GuiMode = ENUM_GUI.MULTISITE_GUI Then
        sql_where = " WHERE (app_name LIKE '%" & APP_TYPE_SERVICE_WWP_SERVER & "') "
      ElseIf GLB_GuiMode = ENUM_GUI.CASHDESK_DRAWS_GUI Then
        sql_where = " WHERE (app_name LIKE '%" & APP_TYPE_SERVICE_DRAW & "') "
      Else
        sql_where = " WHERE (app_name LIKE '%" & APP_TYPE_SERVICE_WCP & "') or (app_name LIKE '%" & APP_TYPE_SERVICE_WC2 & "') or (app_name LIKE '%" & APP_TYPE_SERVICE_WWP_SITE_SERVICE & "') or (app_name LIKE '%" & APP_TYPE_SERVICE_WXP & "') " & _
                    "or (app_name LIKE '%" & APP_TYPE_SERVICE_PSA_CLIENT_SERVICE & "') or (app_name LIKE '%" & APP_TYPE_SERVICE_SMARTFLOOR_DATA_SERVICE & "')"
      End If
    End If

    If Me.opt_win_cashier.Checked Then
      sql_where = " WHERE (app_name LIKE '%" & APP_TYPE_CASHIER & "') "
    End If

    If Me.opt_wigos_gui.Checked Then
      If GLB_GuiMode = ENUM_GUI.MULTISITE_GUI Then
        sql_where = " WHERE (app_name LIKE '%" & APP_TYPE_MULTISITE_GUI & "') "
      ElseIf GLB_GuiMode = ENUM_GUI.CASHDESK_DRAWS_GUI Then
        sql_where = " WHERE (app_name LIKE '%" & APP_TYPE_CASH_DESK_DRAW_GUI & "') "
      Else
        sql_where = " WHERE (app_name LIKE '%" & APP_TYPE_GUI & "') "
      End If
    End If

    Return sql_where
  End Function ' GetSqlWhere

  ' PURPOSE : Formats the version as CC.BBB (Client.Build)
  '
  '  PARAMS:
  '     - INPUT:
  '         - BuildNumber 
  '
  '     - OUTPUT:
  '
  ' RETURNS : Version 

  Private Function FormatBuild(ByVal ClientNumber As Integer, ByVal BuildNumber As Integer) As String

    Return ClientNumber.ToString("00") + "." + BuildNumber.ToString("000")

  End Function ' FormatBuild

  ' PURPOSE: Format appearance of the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     

  Private Sub GUI_StyleSheet()

    Dim fore_color As Color
    Dim back_color As Color

    With Me.dg_list
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, False)

      .Button(BUTTON_TYPE_ADD).Visible = False
      .Button(BUTTON_TYPE_DELETE).Visible = False

      .Sortable = True

      GetUpdateStatusColor(ENUM_UPDATE_STATUS.STATUS_TO_DATE, fore_color, back_color)
      .Counter(COUNTER_APP_UP_TO_DATE).Visible = True
      .Counter(COUNTER_APP_UP_TO_DATE).BackColor = back_color
      .Counter(COUNTER_APP_UP_TO_DATE).ForeColor = fore_color

      GetUpdateStatusColor(ENUM_UPDATE_STATUS.STATUS_OUTDATED, fore_color, back_color)
      .Counter(COUNTER_APP_OUTDATED).Visible = True
      .Counter(COUNTER_APP_OUTDATED).BackColor = back_color
      .Counter(COUNTER_APP_OUTDATED).ForeColor = fore_color

      ' Sentinel
      .Column(GRID_COLUMN_SENTINEL).Header.Text = ""
      .Column(GRID_COLUMN_SENTINEL).Width = 200
      .Column(GRID_COLUMN_SENTINEL).HighLightWhenSelected = False

      ' Application
      .Column(GRID_COLUMN_APP_NAME).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_APP_NAME).Header.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(240)
      .Column(GRID_COLUMN_APP_NAME).Width = 1900
      .Column(GRID_COLUMN_APP_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_APP_NAME).HighLightWhenSelected = False

      ' Build String
      .Column(GRID_COLUMN_APP_VERSION).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_APP_VERSION).Header.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(217)
      .Column(GRID_COLUMN_APP_VERSION).Width = 1000
      .Column(GRID_COLUMN_APP_VERSION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_APP_VERSION).HighLightWhenSelected = False

      ' Computer
      .Column(GRID_COLUMN_APP_COMPUTER).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_APP_COMPUTER).Header.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(246)
      .Column(GRID_COLUMN_APP_COMPUTER).Width = 2300
      .Column(GRID_COLUMN_APP_COMPUTER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_APP_COMPUTER).HighLightWhenSelected = False

      ' IP Address
      .Column(GRID_COLUMN_IP_ADDRESS).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_IP_ADDRESS).Header.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(248)
      .Column(GRID_COLUMN_IP_ADDRESS).Width = 1800
      .Column(GRID_COLUMN_IP_ADDRESS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_IP_ADDRESS).HighLightWhenSelected = False

      ' OS Username
      .Column(GRID_COLUMN_OS_USERNAME).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_OS_USERNAME).Header.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(244)
      .Column(GRID_COLUMN_OS_USERNAME).Width = 1600
      .Column(GRID_COLUMN_OS_USERNAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_OS_USERNAME).HighLightWhenSelected = False

      ' App Username
      .Column(GRID_COLUMN_APP_USERNAME).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_APP_USERNAME).Header.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(245)
      .Column(GRID_COLUMN_APP_USERNAME).Width = 1600
      .Column(GRID_COLUMN_APP_USERNAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_APP_USERNAME).HighLightWhenSelected = False

      ' Last Access
      .Column(GRID_COLUMN_APP_LAST_ACCESS).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_APP_LAST_ACCESS).Header.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(247)
      .Column(GRID_COLUMN_APP_LAST_ACCESS).Width = 2400
      .Column(GRID_COLUMN_APP_LAST_ACCESS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_APP_LAST_ACCESS).HighLightWhenSelected = False

      ' Status Code
      .Column(GRID_COLUMN_UPDATE_STATUS).Header.Text = ""
      .Column(GRID_COLUMN_UPDATE_STATUS).Width = 0

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Get color by SERVICE status
  '
  '  PARAMS:
  '     - INPUT:
  '           - Status:  ENUM_SERVICE_STATUS
  '     - OUTPUT:
  '           - ForeColor: Foreground color
  '           - BackColor: Background color
  '
  ' RETURNS:
  '     - 
  Private Sub GetUpdateStatusColor(ByVal Status As ENUM_UPDATE_STATUS, _
                                   ByRef ForeColor As Color, _
                                   ByRef BackColor As Color)
    Select Case Status

      Case ENUM_UPDATE_STATUS.STATUS_TO_DATE
        ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
        BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)

      Case ENUM_UPDATE_STATUS.STATUS_OUTDATED
        ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
        BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_RED_02)

      Case Else
        ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
        BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_GREY_00)

    End Select

  End Sub ' GetUpdateStatusColor


  Private Sub RefreshVersion()
    Dim _current_client As Integer
    Dim _current_build As Integer
    Dim _rc As Boolean

    If GLB_GuiMode = ENUM_GUI.MULTISITE_GUI Then

      ' Current SW Build for Multisite
      _rc = GetCurrentSwBuild(ENUM_APP_TYPE.APP_TYPE_MULTISITE_GUI, _current_client, _current_build)
      If (Not _rc) Then
        Me.tf_latest_gui_build.Value = GLB_NLS_GUI_SW_DOWNLOAD.GetString(224)
        _current_build = -1
      Else
        Me.tf_latest_gui_build.Value = FormatBuild(_current_client, _current_build)
      End If
    ElseIf GLB_GuiMode = ENUM_GUI.CASHDESK_DRAWS_GUI Then

      ' Current SW Build for CashDesk Draw
      _rc = GetCurrentSwBuild(ENUM_APP_TYPE.APP_TYPE_CASHDESK_DRAWS_GUI, _current_client, _current_build)
      If (Not _rc) Then
        Me.tf_latest_gui_build.Value = GLB_NLS_GUI_SW_DOWNLOAD.GetString(224)
        _current_build = -1
      Else
        Me.tf_latest_gui_build.Value = FormatBuild(_current_client, _current_build)
      End If

    Else

      ' Current SW Build for Cashier
      _rc = GetCurrentSwBuild(ENUM_APP_TYPE.APP_TYPE_CASHIER, _current_client, _current_build)
      If (Not _rc) Then
        Me.tf_latest_cashier_build.Value = GLB_NLS_GUI_SW_DOWNLOAD.GetString(224)
        _current_build = -1
      Else
        Me.tf_latest_cashier_build.Value = FormatBuild(_current_client, _current_build)
      End If

      ' Current SW Build for Wigos GUI
      _rc = GetCurrentSwBuild(ENUM_APP_TYPE.APP_TYPE_GUI, _current_client, _current_build)
      If (Not _rc) Then
        Me.tf_latest_gui_build.Value = GLB_NLS_GUI_SW_DOWNLOAD.GetString(224)
        _current_build = -1
      Else
        Me.tf_latest_gui_build.Value = FormatBuild(_current_client, _current_build)
      End If

    End If

  End Sub


  Private Function DeleteAllApplicationRows() As Boolean
    Dim _sql_str As StringBuilder

    _sql_str = New StringBuilder()
    _sql_str.AppendLine(" DELETE FROM APPLICATIONS ")

    Using _trx As New DB_TRX()
      Try
        Using _sql_cmd As New SqlCommand(_sql_str.ToString(), _trx.SqlTransaction.Connection, _trx.SqlTransaction)
          _sql_cmd.ExecuteNonQuery()
          _trx.Commit()
        End Using

      Catch ex As Exception
        Log.Exception(ex)
        _trx.Rollback()

        Return False
      End Try
    End Using

    Return True

  End Function

  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    m_print_datetime = GUI_GetDateTime()
    Call frm_base_sel.GUI_ReportParamsX(Me.dg_list, ExcelData, FormTitle, m_print_datetime)
  End Sub

#End Region ' Private Functions

#Region "Public Functions"

  ' PURPOSE: Entry point for the screen
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    Me.MdiParent = MdiParent

    Call Me.Display(False)

  End Sub

  Private Sub GUI_ExcelOrPrintResultList(Optional ByVal ToPrint As Boolean = False, Optional ByVal WithPreview As Boolean = False)

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    Try

      m_excel_data = New GUI_Reports.CLASS_EXCEL_DATA(ToPrint, WithPreview)

      If Not m_excel_data.Cancelled Then
        Call GUI_ReportParams(m_excel_data)
        Call GUI_ReportFilter(m_excel_data)
        Call GUI_ReportData(m_excel_data)
        Call GUI_PrintReport(m_excel_data)
      End If

    Catch ex As Exception

      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(146), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            GLB_ApplicationName, _
                            "GUI_ExcelResultList()", _
                            "", _
                            "", _
                            "", _
                           "Error in Excel data export.")
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            GLB_ApplicationName, _
                            "Exception", _
                            "", _
                            "", _
                            "", _
                            ex.Message)
    Finally
      If m_excel_data Is Nothing Then
      Else
        m_excel_data.Dispose()
        m_excel_data = Nothing
      End If
      Windows.Forms.Cursor.Current = Cursors.Default
    End Try

  End Sub

  ' PURPOSE: Get reports from print information and update report filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub GUI_ReportFilter(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA)

    m_print_data = New GUI_Reports.CLASS_PRINT_DATA

    Call GUI_ReportFilter(m_print_data)

    ''Insert filter data
    ExcelData.UpdateReportFilters(m_print_data.Filter.ItemArray)

    ExcelData.SetColumnFormat(EXCEL_COLUMN_APP_VERSION, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.NUMERIC, 3)

  End Sub

  ' PURPOSE: Overridable routine to initialize report filters.
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData As GUI_Reports.CLASS_PRINT_DATA
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(gb_app_type.Text, m_application)

    PrintData.FilterValueWidth(1) = 3000
    PrintData.FilterValueWidth(2) = 3000

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Get report data from grid and inserts it intro Excel document
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub GUI_ReportData(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA)

    Call frm_base_sel.GUI_ReportDataX(Me.dg_list, ExcelData)

  End Sub ' GUI_ReportData

  ' PURPOSE: Show Excel document
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub GUI_PrintReport(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA)
    Call ExcelData.Preview()
  End Sub ' GUI_PrintReport

  ' PURPOSE: Update report filters.
  '
  '  PARAMS:
  '     - INPUT:
  '           - MyDataGrid As DataGrid
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_ReportUpdateFilters()
    m_application = ""

    If opt_services.Checked Then
      m_application = opt_services.Text
    ElseIf opt_wigos_gui.Checked Then
      m_application = opt_wigos_gui.Text
    ElseIf opt_win_cashier.Checked Then
      m_application = opt_win_cashier.Text
    Else
      m_application = opt_all_types.Text
    End If

  End Sub

#End Region ' Public Functions

#Region "Events"

  ' PURPOSE: Execute proper actions when user presses the 'Exit' button
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     

  Private Sub btn_cancel_Click() Handles btn_exit.ClickEvent
    Me.Close()
  End Sub

  ' PURPOSE: Execute proper actions when programmed timer elapses
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     

  Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
    Me.RefreshGrid()
  End Sub


  Private Sub btn_clean_ClickEvent() Handles btn_clean.ClickEvent
    Call DeleteAllApplicationRows()
    Call RefreshGrid()
  End Sub

  Private Sub btn_excel_ClickEvent() Handles btn_excel.ClickEvent
    Call GUI_ExcelOrPrintResultList()
  End Sub

#End Region ' Events


  Private Sub btn_print_ClickEvent() Handles btn_print.ClickEvent
    Call GUI_ExcelOrPrintResultList(True, True)

  End Sub
End Class ' frm_misc_sw_version