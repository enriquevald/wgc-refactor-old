'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_web_browser
' DESCRIPTION:   Open a html document
' AUTHOR:        Raul Cervera
' CREATION DATE: 11-JUN-2010
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 11-JUN-2010  RCI    Initial version
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_Controls

Public Class frm_web_browser
  Inherits frm_base

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' - Form title
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4364)

    ' - Buttons
    btn_close.Text = GLB_NLS_GUI_CONTROLS.GetString(1)

  End Sub ' GUI_InitControls

End Class ' frm_web_browser
