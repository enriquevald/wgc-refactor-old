'-------------------------------------------------------------------
' Copyright © 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_collection_file
' DESCRIPTION:   Collection file
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 06-MAY-2015  AMF    Initial version.
' 10-MAY-2015  SGB    BUG 2049: No show msg_box if don't selected any collection.
' 02-JUL-2015  FJC    Fixed Bug WIG-2530.
' 20-JUL-2015  FJC    Fixed Bug WIG-2511.
' 15-DIC-2016  XGJ    Bug 13685: Recaudación desde fichero: Error al recaudar un fichero sin ticket
'--------------------------------------------------------------------
Option Explicit On

#Region " Imports "

Imports GUI_Controls
Imports WSI.Common
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports WSI.Common.CollectionImport

#End Region ' Imports

Public Class frm_collection_file
  Inherits frm_base_edit

#Region " Members "

  Private m_parameters As SofCountData.SOFT_COUNT_PARAMETERS
  Private m_soft_count_data As CollectionImport.SofCountData
  Private m_cage_cls As CLASS_CAGE_CONTROL

#End Region ' Members

#Region " Overrides "

  ' PURPOSE: Opens dialog with default settings for new mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - SOFT_COUNT_PARAMETERS
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overloads Sub ShowNewItem(ByVal Parameters As SofCountData.SOFT_COUNT_PARAMETERS)

    m_parameters = Parameters

    GUI_SetFormId()

    Call Me.Display(True)

  End Sub ' ShowEditItem

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_COLLECTION_FILE_COLLECT

    Me.m_cage_cls = New CLASS_CAGE_CONTROL()

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    MyBase.GUI_InitControls()

    ' Title
    If m_parameters.CollectionMode = COLLECTION_MODE.MODE_COLLECT Then
      Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6348)
    Else
      Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6354)
    End If

    ' Buttons
    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = m_parameters.CollectionMode = COLLECTION_MODE.MODE_COLLECT
    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_COLLECTION_FILE_IMPORT).Write
    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Text = IIf(m_parameters.CollectionMode = COLLECTION_MODE.MODE_COLLECT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2904), GLB_NLS_GUI_PLAYER_TRACKING.GetString(4679))
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(1)

    ' Class
    m_soft_count_data = New CollectionImport.SofCountData(m_parameters.Xml, m_parameters.SoftCountId)
    m_soft_count_data.CollectionMode = m_parameters.CollectionMode

    ' Dates
    Me.cmb_cage_sessions.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3388)
    Me.ef_working_day.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6314)
    Me.ef_cage_session.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3388)
    Me.m_cage_cls.SetComboCageSessions(Me.cmb_cage_sessions, False)
    Call Me.SetDefaultCageSession()
    Me.ef_working_day.IsReadOnly = True
    Me.ef_cage_session.IsReadOnly = True

    ' Totals
    Me.gb_totals.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2306)
    Me.ef_total_bills.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2266)
    Me.ef_total_bills.IsReadOnly = True
    Me.ef_total_tickets.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1825)
    Me.ef_total_tickets.IsReadOnly = True

    ' Collection Status
    Me.gb_collection_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6349)
    Me.gb_collection_status.Enabled = m_parameters.CollectionMode = COLLECTION_MODE.MODE_COLLECT

    Me.chk_balanced.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6351)
    Me.chk_balanced.Checked = True

    Me.chk_unbalanced.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6350)
    Me.chk_unbalanced.Checked = True

    Me.chk_only_system.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6352)
    Me.chk_only_system.Checked = m_parameters.CollectionMode = COLLECTION_MODE.MODE_COLLECT

    Me.chk_only_file.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6353)
    Me.chk_only_file.Checked = m_parameters.CollectionMode = COLLECTION_MODE.MODE_COLLECT

    ' Collections
    Me.gb_collections.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3397)

    ' Detail
    Me.gb_detail.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(474)

  End Sub 'GUI_InitControls

  ' PURPOSE: Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.cmb_cage_sessions

  End Sub ' GUI_SetInitialFocus

  ' PURPOSE : Set initial data on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '         - SqlCtx
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    ' Dates
    Me.ef_working_day.TextValue = m_parameters.SessionDay.ToShortDateString()

    ' Collections
    Me.dg_collection_sel.DataCollections = m_soft_count_data.MergedCollectionSoftCount
    Me.dg_collection_sel.MachineIdType = m_soft_count_data.MachineIdType
    Me.dg_collection_sel.CollectionMode = m_parameters.CollectionMode
    Me.dg_collection_sel.SetDataGridCollections()

    'XGJ 15-DIC-2016
    If (Me.dg_collection_sel.DataCollections.Rows.Count = 0) Then
      Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = False
    Else
      Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = True
    End If


      ' Mode cancel
      If m_parameters.CollectionMode = COLLECTION_MODE.MODE_CANCEL Then
        Me.cmb_cage_sessions.Value = m_parameters.CageSessionId
        Me.ef_cage_session.TextValue = m_parameters.CageSessionName
        Me.cmb_cage_sessions.Visible = False
      Else
        Me.ef_cage_session.Visible = False
      End If

      ' Handler
      AddHandler chk_balanced.CheckedChanged, AddressOf CheckedChanged
      AddHandler chk_unbalanced.CheckedChanged, AddressOf CheckedChanged
      AddHandler chk_only_system.CheckedChanged, AddressOf CheckedChanged
      AddHandler chk_only_file.CheckedChanged, AddressOf CheckedChanged

  End Sub ' GUI_SetScreenData

  ' PURPOSE: Process clicks on select button
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Select Case ButtonId

      Case ENUM_BUTTON.BUTTON_OK

        If GUI_IsScreenDataOk() Then

          Select Case m_parameters.CollectionMode

            Case COLLECTION_MODE.MODE_CANCEL
              If CancelCollections() Then
                Call Me.Close()
              End If

            Case COLLECTION_MODE.MODE_COLLECT
              If CollectCollections() Then
                Call Me.Close()
              End If

          End Select

        End If

      Case ENUM_BUTTON.BUTTON_CANCEL

        If m_parameters.CollectionMode = COLLECTION_MODE.MODE_COLLECT Then
          If HaveChanges() Then
            If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then
              Return
            End If
          End If
        End If

        MyBase.GUI_ButtonClick(ButtonId)

      Case ENUM_BUTTON.BUTTON_DELETE

        If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6315), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_YES Then
          If m_soft_count_data.DeleteSoftCount(m_parameters.SoftCountId, CurrentUser.Id) Then

            Call AuditDelete()
            Call Me.Close()
          Else
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6316), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
          End If
        End If

      Case Else
        MyBase.GUI_ButtonClick(ButtonId)

    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Keypress 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    Select Case e.KeyChar
      Case Chr(Keys.Enter), Chr(Keys.Escape)

        If Me.dg_collection_sel.ContainsFocus Then
          Me.dg_collection_sel.CollectionKeyPress(sender, e)

          Return True
        End If

        Return False

      Case Else
        Return MyBase.GUI_KeyPress(sender, e)

    End Select

  End Function ' GUI_KeyPress

  ' PURPOSE : Validate the data presented on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - None       
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    If String.IsNullOrEmpty(Me.cmb_cage_sessions.TextValue) AndAlso m_parameters.CollectionMode = COLLECTION_MODE.MODE_COLLECT Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4554), ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.cmb_cage_sessions.Text) ' Select a cash cage session
      Me.cmb_cage_sessions.Focus()

      Return False
    End If

    If m_soft_count_data.MergedCollectionSoftCount.Select("CHECKED = 1").Length <= 0 Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6488), ENUM_MB_TYPE.MB_TYPE_WARNING, , , , ) ' No selected collection
      Me.cmb_cage_sessions.Focus()

      Return False

    End If

    Return True

  End Function ' GUI_IsScreenDataOk

#End Region ' Overrides

#Region " Private Functions "

  ' PURPOSE : Cancel collections
  '
  '  PARAMS :
  '     - INPUT : 
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - True or False
  '
  Private Function CancelCollections() As Boolean

    Dim _cashier_session_id As Long
    Dim _money_collections As New List(Of Int64)
    Dim _canceled_money_collections As New List(Of Int64)

    Try

      If Me.dg_collection_sel.GetDataGridCollections(_money_collections) Then
        If _money_collections.Count > 0 Then
          If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6355), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _money_collections.Count) = ENUM_MB_RESULT.MB_RESULT_YES Then
            For Each _mc_id As Int64 In _money_collections
              If m_soft_count_data.GetCashierSessionId(_mc_id, _cashier_session_id) Then
                If Me.m_cage_cls.CancelMovementExternalCall(_mc_id, _cashier_session_id) = ENUM_DB_RC.DB_RC_OK Then
                  _canceled_money_collections.Add(_mc_id)
                End If
              End If
            Next

            If Not AuditCollect(_canceled_money_collections, COLLECTION_MODE.MODE_CANCEL, CurrentUser.Name, cmb_cage_sessions.Value(cmb_cage_sessions.SelectedIndex)) Then
              'TODO: FALTA NLS_MSGBOX
              Return False

            End If

            m_soft_count_data.CancelSoftCount(m_parameters.SoftCountId, m_soft_count_data.SoftCountObject)

            Return True
          End If
        End If
      End If

    Catch _ex As Exception

      Call Trace.WriteLine(_ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, "frm_collection_file", "CancelCollections", _ex.Message)
    End Try

    Return False

  End Function ' CancelCollections

  ' PURPOSE : Collect collections
  '
  '  PARAMS :
  '     - INPUT : 
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - True or False
  '
  Private Function CollectCollections() As Boolean

    Dim _money_collections As New List(Of Int64)
    Dim _collections As New DataTable
    Dim _denominations As New DataTable
    Dim _error As Int16
    Try

      If Me.dg_collection_sel.GetDataGridCollections(_money_collections) Then

        If _money_collections.Count = 0 Then
          Return False
        End If
        If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6368), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _money_collections.Count, Me.dg_collection_sel.CountIdOk) = ENUM_MB_RESULT.MB_RESULT_YES Then
          'If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6368), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2, Me.dg_collection_sel.CountIdOk) = ENUM_MB_RESULT.MB_RESULT_YES Then

          Using _trx As New DB_TRX
            If Me.m_soft_count_data.CollectSoftCounts(_money_collections, CurrentUser.Id, CurrentUser.Name, cmb_cage_sessions.Value(cmb_cage_sessions.SelectedIndex), _trx) Then
              'Audit
              If AuditCollect(_money_collections, COLLECTION_MODE.MODE_COLLECT, CurrentUser.FullName, cmb_cage_sessions.TextValue) Then
                _trx.Commit()
                _error = 0
              Else
                _error = 2
              End If
            Else
              _error = 1
            End If
          End Using
        Else
          Return False
        End If
      End If

        Select Case _error
          Case 0
            NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3034), ENUM_MB_TYPE.MB_TYPE_INFO, ENUM_MB_BTN.MB_BTN_OK) ' Collection Success
          Case 1
            NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3035), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' Collection Error
            Return False
          Case 2
            NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3035), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' Auditing Error
            Return False
          Case Else
        End Select

    Catch _ex As Exception

      Call Trace.WriteLine(_ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, "frm_collection_file", "CollectCollections", _ex.Message)
    End Try

    Return True

  End Function ' CollectCollections

  ' PURPOSE : Creates audit register with Delete Operation
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - True or False
  '
  Private Function AuditDeleteSoftCountRegister(ByVal strSoftCountName As String, _
                                                ByVal strSoftCountDataImport As String) As Boolean
    Dim _auditor As CLASS_AUDITOR_DATA
    Dim _str_descritpion As String

    _auditor = New CLASS_AUDITOR_DATA(AUDIT_CODE_USER_ACTIVITY)
    _auditor.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(6294), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6311) & " - " & _
                                                           GLB_NLS_GUI_PLAYER_TRACKING.GetString(6294))

    _str_descritpion = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6303) & ": " & strSoftCountName & " " & _
                       GLB_NLS_GUI_PLAYER_TRACKING.GetString(6312) & ": " & strSoftCountDataImport & " "
    ''                   GLB_NLS_GUI_PLAYER_TRACKING.GetString(6198) & ": " & IIf(IsDeposit, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6196), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6197))

    _auditor.SetField(0, Nothing, _str_descritpion, CLASS_AUDITOR_DATA.ENUM_FIELD_TYPE.FIELD_NO_DATA)
    _auditor.Notify(CurrentUser.GuiId, CurrentUser.Id, CurrentUser.Name, CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, 0)
  End Function


  ' PURPOSE : Audit Collect Collection 
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - True or False
  '
  Private Function AuditCollect(ByVal MoneyCollections As List(Of Int64), _
                                      ByVal Mode As COLLECTION_MODE, _
                                      ByVal UserFullName As String, _
                                      ByVal CageSession As String)

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _data_entry As SofCountData.SOFT_COUNT_DATA_DETAIL
    Dim _dt_detail_for_collection As DataTable
    Dim _dr_collection() As DataRow
    Dim _denomination As String
    Dim _value As String
    Dim _expected_value As String
    Dim _currency_type As String
    Dim _iso_code As String
    Dim _nls_id As Integer
    Dim _stacker_terminal As String
    Dim _terminal_name As String
    Dim _init_audit_data As String

    Dim _total_tickets As Integer
    Dim _total_bills As Integer
    Dim _total_bills_amount As Decimal
    Dim _total_delivered As Integer
    Dim _stacker_id As Long


    Try


      ' Initialize
      _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_CAGE)
      _data_entry = New SofCountData.SOFT_COUNT_DATA_DETAIL
      _dt_detail_for_collection = New DataTable
      _dr_collection = Nothing
      _denomination = String.Empty
      _value = String.Empty
      _expected_value = String.Empty
      _currency_type = String.Empty
      _stacker_terminal = String.Empty
      _stacker_id = 0
      _nls_id = 0
      _iso_code = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")
      _terminal_name = String.Empty
      _init_audit_data = String.Empty

      'Initialize Totals
      _total_tickets = 0
      _total_delivered = 0
      _total_bills = 0
      _total_bills_amount = 0

      ' INIT Audit data
      _init_audit_data = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6383) & " (" & m_parameters.SoftCountName & ")"
      If Mode = COLLECTION_MODE.MODE_CANCEL Then
        _init_audit_data &= " (" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2908) & ")"
      End If
      _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(6383), _init_audit_data)

      '1. READ SOFTCOUNT COLLECT
      ' SoftCount Name
      _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(253), IIf(String.IsNullOrEmpty(m_parameters.SoftCountName), AUDIT_NONE_STRING, m_parameters.SoftCountName))

      ' SoftCount Status            
      If Mode = COLLECTION_MODE.MODE_COLLECT Then
        _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(262), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2264))  'Collected
      ElseIf Mode = COLLECTION_MODE.MODE_CANCEL Then
        _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(262), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2908))  'Cancelled

        'User who cancelled collection
        _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4613), UserFullName)

      End If

      ' User Name
      _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2896), UserFullName)

      ' SoftCount Collect DateTime (Pending.)
      _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6313), WGDB.Now.ToString)

      ' SoftCount Cage session                        
      _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(3388), CageSession)  ' Cage session 

      ' SoftCount Operation         
      _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6198), GLB_NLS_GUI_PLAYER_TRACKING.GetString(4946))

      '2. READ SOFTCOUNT COLLECT for COLLECT

      For Each _collection_id As Int64 In MoneyCollections

        'For Each _dr As DataRow In Me.m_soft_count_data.MergedCollectionSoftCount.Rows
        _dr_collection = Me.m_soft_count_data.MergedCollectionSoftCount.Select(SofCountData.SQL_COLUMN_COLLECTION_ID & " = " & _collection_id)

        ''If _dr(SofCountData.SQL_COLUMN_CHECKED) = "1" Then
        If _dr_collection.Length > 0 Then

          Me.m_soft_count_data.GetSoftCountOneTerminalBaseNameFloorId(_dr_collection(0)(SofCountData.SQL_COLUMN_TERMINAL_ID), _terminal_name, String.Empty)

          _stacker_id = IIf(_dr_collection(0).IsNull(SofCountData.SQL_COLUMN_STACKER_ID), 0, _dr_collection(0)(SofCountData.SQL_COLUMN_STACKER_ID))
          If _stacker_id > 0 Then

            ' Recaudacion de Terminal XXX con Stacker ID XXX
            _stacker_terminal = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6382, _stacker_id.ToString())
            _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6381), _terminal_name & " " & _stacker_terminal)
          Else
            ' Recaudacion de Terminal XXX
            _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6381), _terminal_name)
          End If

          ' Terminal Name 
          Me.m_soft_count_data.GetSoftCountOneTerminalBaseNameFloorId(_dr_collection(0)(SofCountData.SQL_COLUMN_TERMINAL_ID), _terminal_name, String.Empty)
          _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5730), _terminal_name)

          ' Floor Id (Pending Solo si esta lleno)
          If _dr_collection(0)(SofCountData.SQL_COLUMN_FLOOR_ID).ToString <> String.Empty Then
            _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5200), _dr_collection(0)(SofCountData.SQL_COLUMN_FLOOR_ID))
          End If

          '2.1. READ DETAIL FROM ONE SOFTCOUNT 
          _data_entry.StackerId = _stacker_id
          _data_entry.TerminalId = _dr_collection(0)(SofCountData.SQL_COLUMN_TERMINAL_ID)
          _data_entry.CollectionId = _dr_collection(0)(SofCountData.SQL_COLUMN_COLLECTION_ID)
          _total_tickets = 0
          _total_delivered = 0
          _total_bills = 0
          _total_bills_amount = 0
          If (Me.m_soft_count_data.GetSoftCountCollectionsDetail(_data_entry, True, _dt_detail_for_collection)) Then

            For Each _dr_detail As DataRow In _dt_detail_for_collection.Rows
              _nls_id = 0
              Select Case _dr_detail(SofCountData.SQL_COLUMN_TYPE)
                Case WSI.Common.CollectionImport.TYPE_ROW_TICKET_BILL.BILL

                  ' BILLS
                  _denomination = GUI_FormatNumber(_dr_detail(SofCountData.SQL_COLUMN_DENOMINATION_NUMERIC), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
                  _value = GUI_FormatNumber(IIf(IsDBNull(_dr_detail(SofCountData.SQL_COLUMN_COLLECTED_BILLS_QUANTITY)), 0, _dr_detail(SofCountData.SQL_COLUMN_COLLECTED_BILLS_QUANTITY)), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
                  _total_bills += _value
                  _total_bills_amount += GUI_FormatNumber(IIf(IsDBNull(_dr_detail(SofCountData.SQL_COLUMN_COLLECTED_BILLS_TOTAL)), 0, _dr_detail(SofCountData.SQL_COLUMN_COLLECTED_BILLS_TOTAL)), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
                  _expected_value = GUI_FormatNumber(IIf(IsDBNull(_dr_detail(SofCountData.SQL_COLUMN_EXPECTED_BILLS_QUANTITY)), 0, _dr_detail(SofCountData.SQL_COLUMN_EXPECTED_BILLS_QUANTITY)), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
                  _currency_type = CLASS_CAGE_CONTROL.GetCurrencyTypeDescription(CageCurrencyType.Bill)
                  _nls_id = 5674

                  _value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1984, _value, _expected_value)

                  If _nls_id = 5674 Then
                    _auditor_data.SetField(0, _value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_id, _iso_code, _denomination, _currency_type))
                  Else
                    _auditor_data.SetField(0, _value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_id, _iso_code, _denomination))
                  End If

                Case WSI.Common.CollectionImport.TYPE_ROW_TICKET_BILL.TOTAL_TICKET

                  'TOTAL TICKETS
                  _total_tickets = GUI_FormatNumber(_dr_detail(SofCountData.SQL_COLUMN_COLLECTED_BILLS_QUANTITY), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)

              End Select

            Next
          End If

          'Tickets Collected
          _auditor_data.SetField(0, _total_tickets.ToString, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2842)) ' Tickets recaudados

          'Num of Bills
          _auditor_data.SetField(0, _total_bills.ToString(), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2784)) ' Cantidad de billetes

          'Total Delivered
          _auditor_data.SetField(0, GUI_FormatCurrency(_total_bills_amount.ToString()), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2783)) ' Total delivered

        End If

      Next

      _auditor_data.Notify(CurrentUser.GuiId, _
                           CurrentUser.Id, _
                           CurrentUser.Name, _
                           CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, _
                           0)

      Return True
    Catch ex As Exception

      Log.Exception(ex)
    End Try

    Return False
  End Function

  ' PURPOSE : Set default case session
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub SetDefaultCageSession()

    If Me.cmb_cage_sessions.Count = 1 Then
      Me.cmb_cage_sessions.SelectedIndex = 0
      Me.cmb_cage_sessions.IsReadOnly = True
      Me.ef_cage_session.TextValue = Me.cmb_cage_sessions.TextValue
      Me.cmb_cage_sessions.Visible = False
    End If

  End Sub 'SetDefaultCageSession

  ' PURPOSE : check if have changes
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - True or False
  '
  Private Function HaveChanges()
    Dim _id_list As New List(Of Int64)

    Me.dg_collection_sel.GetDataGridCollections(_id_list)

    If (Me.dg_collection_sel.CountIdLoad <> _id_list.Count) OrElse _
       (Me.cmb_cage_sessions.SelectedIndex <> -1 AndAlso Me.cmb_cage_sessions.Visible) OrElse _
        Not Me.chk_balanced.Checked OrElse _
        Not Me.chk_unbalanced.Checked OrElse _
        Not Me.chk_only_system.Checked OrElse _
        Not Me.chk_only_file.Checked OrElse _
        Me.dg_collection_sel.HaveChanges Then

      Return True
    End If

    Return False
  End Function ' HaveChanges


  ' PURPOSE : Audit delete action
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - True or False
  '
  Private Sub AuditDelete()

    Dim _auditor_data As CLASS_AUDITOR_DATA

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_CAGE)

    ' Audit data
    _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(6294), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2617))

    '     - name import
    _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6297), IIf(String.IsNullOrEmpty(m_parameters.SoftCountName), AUDIT_NONE_STRING, m_parameters.SoftCountName))

    _auditor_data.Notify(CurrentUser.GuiId, _
                         CurrentUser.Id, _
                         CurrentUser.Name, _
                         CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.DELETE, _
                         0)
  End Sub
#End Region ' Private Functions

#Region " Events "

  ' PURPOSE : Collection status change
  '
  '  PARAMS :
  '     - INPUT :
  '         - sender
  '         - EventArgs
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    Me.dg_collection_sel.Completed = chk_balanced.Checked
    Me.dg_collection_sel.Unbalanced = chk_unbalanced.Checked
    Me.dg_collection_sel.Pending = chk_only_system.Checked
    Me.dg_collection_sel.Collected = chk_only_file.Checked

    Me.dg_collection_sel.SetDataGridCollections()

  End Sub ' CheckedChanged

  ' PURPOSE : Collection sel row selected event
  '
  '  PARAMS :
  '     - INPUT :
  '         - SelectedRow
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub dg_collection_sel_RowSelectedEvent(ByVal SelectedRow As Integer) Handles dg_collection_sel.RowSelectedEvent

    Dim _detail As CollectionImport.SofCountData.SOFT_COUNT_DATA_DETAIL
    Dim _dt As DataTable

    _dt = New DataTable()
    _detail = New CollectionImport.SofCountData.SOFT_COUNT_DATA_DETAIL

    If Me.dg_collection_sel.GetSearchDetail(SelectedRow, _detail) Then
      m_soft_count_data.GetSoftCountCollectionsDetail(_detail, False, _dt)
    End If
    Me.dg_collection_detail.DataCollectionDetail = _dt
    Me.dg_collection_detail.SetDataGridCollectionTotal()
    Me.dg_collection_detail.SetDataGridCollectionTickets()

    Call Me.ChangeTotals()

  End Sub ' dg_collection_sel_RowSelectedEvent

  ' PURPOSE : Recalculate total bills and tickets
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub ChangeTotals() Handles dg_collection_sel.ChangeTotals

    Me.ef_total_bills.TextValue = GUI_FormatCurrency(Me.dg_collection_sel.TotalBills)
    Me.ef_total_tickets.TextValue = GUI_FormatCurrency(Me.dg_collection_sel.TotalTickets)

  End Sub ' ChangeTotals

  ' PURPOSE : Cell data change event
  '
  '  PARAMS:
  '     - INPUT:
  '         - Row
  '         - Column
  '     - OUTPUT:
  '
  ' RETURNS :
  '
  Private Sub dg_collection_sel_CellDataChangeEvent(ByVal Row As Integer, ByVal Column As Integer) Handles dg_collection_sel.CollectionDataChangedEvent

    Dim _result As New SOFTCOUNT_EDIT_RESULT
    Dim _dt_changes As New DataTable

    If Me.dg_collection_sel.RaiseCellDataChangedEvent Then
      _result = m_soft_count_data.EditSoftCountMergedRows(Me.dg_collection_sel.NewValue, Me.dg_collection_sel.ImportIndex, _dt_changes)

      Select Case _result
        Case SOFTCOUNT_EDIT_RESULT.EDIT_STATUS_OK
          Me.dg_collection_sel.DataCollections = m_soft_count_data.MergedCollectionSoftCount
          Me.dg_collection_sel.SetChangeDataGridCollections(_dt_changes)
          Me.dg_collection_sel_RowSelectedEvent(Row)

        Case SOFTCOUNT_EDIT_RESULT.EDIT_STATUS_ID_NOT_EXISTS
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6369), ENUM_MB_TYPE.MB_TYPE_ERROR)
          Me.dg_collection_sel.LoadOldValue(Row, Column)

        Case SOFTCOUNT_EDIT_RESULT.EDIT_STATUS_ERROR
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3035), ENUM_MB_TYPE.MB_TYPE_ERROR)
          Me.dg_collection_sel.LoadOldValue(Row, Column)

      End Select

    Else
      Me.dg_collection_sel.LoadOldValue(Row, Column)
    End If

  End Sub ' dg_collection_sel_CellDataChangeEvent

#End Region

End Class