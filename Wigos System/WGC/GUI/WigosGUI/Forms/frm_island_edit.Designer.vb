<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_island_edit
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.ef_bank = New GUI_Controls.uc_entry_field
    Me.cmb_area = New GUI_Controls.uc_combo
    Me.ef_smoking = New GUI_Controls.uc_entry_field
    Me.panel_data.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.cmb_area)
    Me.panel_data.Controls.Add(Me.ef_smoking)
    Me.panel_data.Controls.Add(Me.ef_bank)
    Me.panel_data.Size = New System.Drawing.Size(362, 152)
    '
    'ef_bank
    '
    Me.ef_bank.DoubleValue = 0
    Me.ef_bank.IntegerValue = 0
    Me.ef_bank.IsReadOnly = False
    Me.ef_bank.Location = New System.Drawing.Point(28, 97)
    Me.ef_bank.Name = "ef_bank"
    Me.ef_bank.Size = New System.Drawing.Size(192, 24)
    Me.ef_bank.SufixText = "Sufix Text"
    Me.ef_bank.SufixTextVisible = True
    Me.ef_bank.TabIndex = 2
    Me.ef_bank.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_bank.TextValue = ""
    Me.ef_bank.TextWidth = 87
    Me.ef_bank.Value = ""
    '
    'cmb_area
    '
    Me.cmb_area.IsReadOnly = False
    Me.cmb_area.Location = New System.Drawing.Point(3, 21)
    Me.cmb_area.Name = "cmb_area"
    Me.cmb_area.SelectedIndex = -1
    Me.cmb_area.Size = New System.Drawing.Size(318, 24)
    Me.cmb_area.SufixText = "Sufix Text"
    Me.cmb_area.SufixTextVisible = True
    Me.cmb_area.TabIndex = 0
    Me.cmb_area.TextWidth = 110
    '
    'ef_smoking
    '
    Me.ef_smoking.DoubleValue = 0
    Me.ef_smoking.IntegerValue = 0
    Me.ef_smoking.IsReadOnly = False
    Me.ef_smoking.Location = New System.Drawing.Point(5, 61)
    Me.ef_smoking.Name = "ef_smoking"
    Me.ef_smoking.Size = New System.Drawing.Size(316, 24)
    Me.ef_smoking.SufixText = "Sufix Text"
    Me.ef_smoking.SufixTextVisible = True
    Me.ef_smoking.TabIndex = 1
    Me.ef_smoking.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_smoking.TextValue = ""
    Me.ef_smoking.TextWidth = 110
    Me.ef_smoking.Value = ""
    '
    'frm_island_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(464, 161)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_island_edit"
    Me.Text = "frm_island_edit"
    Me.panel_data.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_bank As GUI_Controls.uc_entry_field
  Friend WithEvents cmb_area As GUI_Controls.uc_combo
  Friend WithEvents ef_smoking As GUI_Controls.uc_entry_field
End Class
