﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_customer_entrance_prices_edit
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_customer_entrances = New System.Windows.Forms.GroupBox()
    Me.lblLegend = New System.Windows.Forms.Label()
    Me.btn_add = New GUI_Controls.uc_button()
    Me.btn_del = New GUI_Controls.uc_button()
    Me.dg_entrance_prices = New GUI_Controls.uc_grid()
    Me.panel_data.SuspendLayout()
    Me.gb_customer_entrances.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_customer_entrances)
    Me.panel_data.Size = New System.Drawing.Size(594, 328)
    '
    'gb_customer_entrances
    '
    Me.gb_customer_entrances.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.gb_customer_entrances.Controls.Add(Me.lblLegend)
    Me.gb_customer_entrances.Controls.Add(Me.btn_add)
    Me.gb_customer_entrances.Controls.Add(Me.btn_del)
    Me.gb_customer_entrances.Controls.Add(Me.dg_entrance_prices)
    Me.gb_customer_entrances.Location = New System.Drawing.Point(9, 9)
    Me.gb_customer_entrances.Name = "gb_customer_entrances"
    Me.gb_customer_entrances.Size = New System.Drawing.Size(581, 314)
    Me.gb_customer_entrances.TabIndex = 1
    Me.gb_customer_entrances.TabStop = False
    Me.gb_customer_entrances.Text = "xEntrancePrices"
    '
    'lblLegend
    '
    Me.lblLegend.AutoSize = True
    Me.lblLegend.Location = New System.Drawing.Point(6, 283)
    Me.lblLegend.Name = "lblLegend"
    Me.lblLegend.Size = New System.Drawing.Size(12, 13)
    Me.lblLegend.TabIndex = 6
    Me.lblLegend.Text = "-"
    '
    'btn_add
    '
    Me.btn_add.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btn_add.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_add.Location = New System.Drawing.Point(6, 253)
    Me.btn_add.Name = "btn_add"
    Me.btn_add.Size = New System.Drawing.Size(50, 21)
    Me.btn_add.TabIndex = 3
    Me.btn_add.ToolTipped = False
    Me.btn_add.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.ADD_DELETE
    '
    'btn_del
    '
    Me.btn_del.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btn_del.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_del.Location = New System.Drawing.Point(521, 253)
    Me.btn_del.Name = "btn_del"
    Me.btn_del.Size = New System.Drawing.Size(50, 21)
    Me.btn_del.TabIndex = 4
    Me.btn_del.ToolTipped = False
    Me.btn_del.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.ADD_DELETE
    '
    'dg_entrance_prices
    '
    Me.dg_entrance_prices.CurrentCol = -1
    Me.dg_entrance_prices.CurrentRow = -1
    Me.dg_entrance_prices.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_entrance_prices.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_entrance_prices.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_entrance_prices.Location = New System.Drawing.Point(7, 20)
    Me.dg_entrance_prices.Name = "dg_entrance_prices"
    Me.dg_entrance_prices.PanelRightVisible = False
    Me.dg_entrance_prices.Redraw = True
    Me.dg_entrance_prices.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    Me.dg_entrance_prices.Size = New System.Drawing.Size(570, 227)
    Me.dg_entrance_prices.Sortable = False
    Me.dg_entrance_prices.SortAscending = True
    Me.dg_entrance_prices.SortByCol = 0
    Me.dg_entrance_prices.TabIndex = 2
    Me.dg_entrance_prices.ToolTipped = True
    Me.dg_entrance_prices.TopRow = -2
    '
    'frm_customer_entrance_prices_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(696, 339)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_customer_entrance_prices_edit"
    Me.Text = "frm_customer_entrance_ticket_sel"
    Me.panel_data.ResumeLayout(False)
    Me.gb_customer_entrances.ResumeLayout(False)
    Me.gb_customer_entrances.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_customer_entrances As System.Windows.Forms.GroupBox
  Friend WithEvents btn_add As GUI_Controls.uc_button
  Friend WithEvents btn_del As GUI_Controls.uc_button
  Friend WithEvents dg_entrance_prices As GUI_Controls.uc_grid
  Friend WithEvents lblLegend As System.Windows.Forms.Label
End Class
