﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_player_tracking_global_drop_hourly
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.chb_only_with_activity = New System.Windows.Forms.CheckBox()
    Me.uc_multicurrency = New GUI_Controls.uc_multi_currency_site_sel()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.uc_checked_list_groups = New GUI_Controls.uc_checked_list()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.uc_checked_list_groups)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Controls.Add(Me.uc_multicurrency)
    Me.panel_filter.Controls.Add(Me.chb_only_with_activity)
    Me.panel_filter.Size = New System.Drawing.Size(973, 175)
    Me.panel_filter.Controls.SetChildIndex(Me.chb_only_with_activity, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_multicurrency, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_checked_list_groups, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 179)
    Me.panel_data.Size = New System.Drawing.Size(973, 348)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(967, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(967, 4)
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 45)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(222, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 5
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 17)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(222, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 4
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'chb_only_with_activity
    '
    Me.chb_only_with_activity.AutoSize = True
    Me.chb_only_with_activity.Location = New System.Drawing.Point(12, 98)
    Me.chb_only_with_activity.Name = "chb_only_with_activity"
    Me.chb_only_with_activity.Size = New System.Drawing.Size(126, 17)
    Me.chb_only_with_activity.TabIndex = 11
    Me.chb_only_with_activity.Text = "xOnlyWithActivity"
    Me.chb_only_with_activity.UseVisualStyleBackColor = True
    '
    'uc_multicurrency
    '
    Me.uc_multicurrency.GroupBoxText = Nothing
    Me.uc_multicurrency.HeightControl = 0
    Me.uc_multicurrency.Location = New System.Drawing.Point(670, 14)
    Me.uc_multicurrency.Name = "uc_multicurrency"
    Me.uc_multicurrency.OpenModeControl = GUI_Controls.uc_multi_currency_site_sel.OPEN_MODE.OnlyCurrency
    Me.uc_multicurrency.SelectedCurrency = ""
    Me.uc_multicurrency.SelectedOption = GUI_Controls.uc_multi_currency_site_sel.MULTICURRENCY_OPTION.TotalToIsoCode
    Me.uc_multicurrency.Size = New System.Drawing.Size(206, 78)
    Me.uc_multicurrency.TabIndex = 12
    Me.uc_multicurrency.WidthControl = 0
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Location = New System.Drawing.Point(6, 14)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(242, 78)
    Me.gb_date.TabIndex = 13
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'uc_checked_list_groups
    '
    Me.uc_checked_list_groups.GroupBoxText = "xCheckedList"
    Me.uc_checked_list_groups.Location = New System.Drawing.Point(260, 14)
    Me.uc_checked_list_groups.m_resize_width = 404
    Me.uc_checked_list_groups.multiChoice = True
    Me.uc_checked_list_groups.Name = "uc_checked_list_groups"
    Me.uc_checked_list_groups.SelectedIndexes = New Integer(-1) {}
    Me.uc_checked_list_groups.SelectedIndexesList = ""
    Me.uc_checked_list_groups.SelectedIndexesListLevel2 = ""
    Me.uc_checked_list_groups.SelectedValuesArray = New String(-1) {}
    Me.uc_checked_list_groups.SelectedValuesList = ""
    Me.uc_checked_list_groups.SelectedValuesListLevel2 = ""
    Me.uc_checked_list_groups.SetLevels = 2
    Me.uc_checked_list_groups.Size = New System.Drawing.Size(404, 153)
    Me.uc_checked_list_groups.TabIndex = 14
    Me.uc_checked_list_groups.ValuesArray = New String(-1) {}
    '
    'frm_player_tracking_global_drop_hourly
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(981, 531)
    Me.Name = "frm_player_tracking_global_drop_hourly"
    Me.Text = "frm_player_tracking_global_drop_hourly"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents chb_only_with_activity As System.Windows.Forms.CheckBox
  Friend WithEvents uc_multicurrency As GUI_Controls.uc_multi_currency_site_sel
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents uc_checked_list_groups As GUI_Controls.uc_checked_list
End Class
