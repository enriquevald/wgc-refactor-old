'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_mailing_parameters
' DESCRIPTION:   Edit Mailing Parameters
' AUTHOR:        Raul Cervera
' CREATION DATE: 04-JAN-2011
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 04-JAN-2011  RCI    Initial version
' 08-JAN-2013  HBB    used the method CheckEmailAddress Moved to WSI.Common.Mailing
' 12-FEB-2013  JMM    Replaced call to WSI.Common.Mailing.CheckEmailAddress with WSI.Common.ValidateFormat.Email
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT

Public Class frm_mailing_parameters
  Inherits frm_base_edit

#Region " Constants "

  Private Const MAILING_SERVICE_NAME As String = "WC2_Service"

#End Region ' Constants

#Region " Members "

  Private m_default_button As IButtonControl

#End Region ' Members

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_MAILING_PROGRAMMING_EDIT

    Call MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_STATISTICS.GetString(437)

    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Enabled = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = True

    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Text = GLB_NLS_GUI_CONTROLS.GetString(13)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)

    chk_mailing_enabled.Text = GLB_NLS_GUI_CONFIGURATION.GetString(396)
    chk_smtp_enabled.Text = GLB_NLS_GUI_CONFIGURATION.GetString(397)

    gb_authorized_server_list.Text = GLB_NLS_GUI_CONFIGURATION.GetString(412)
    gb_smtp_config.Text = GLB_NLS_GUI_CONFIGURATION.GetString(398)
    ef_smtp_server.Text = GLB_NLS_GUI_CONFIGURATION.GetString(400)
    ef_smtp_port.Text = GLB_NLS_GUI_CONFIGURATION.GetString(401)
    cmb_smtp_secured.Text = GLB_NLS_GUI_CONFIGURATION.GetString(402)

    gb_credentials.Text = GLB_NLS_GUI_CONFIGURATION.GetString(399)
    ef_smtp_username.Text = GLB_NLS_GUI_CONFIGURATION.GetString(403)
    ef_smtp_password.Text = GLB_NLS_GUI_CONFIGURATION.GetString(404)
    ef_smtp_domain.Text = GLB_NLS_GUI_CONFIGURATION.GetString(405)
    ef_smtp_email_from.Text = GLB_NLS_GUI_CONFIGURATION.GetString(406)

    ef_smtp_server.SetFilter(FORMAT_TEXT, 80)
    ef_smtp_port.SetFilter(FORMAT_NUMBER, 5)
    ef_smtp_username.SetFilter(FORMAT_TEXT, 50)
    ef_smtp_password.SetFilter(FORMAT_TEXT, 50)
    ef_smtp_password.Password = True
    ef_smtp_domain.SetFilter(FORMAT_TEXT, 50)
    ef_smtp_email_from.SetFilter(FORMAT_TEXT, 50)
    ef_email_test_to.SetFilter(FORMAT_TEXT, 50)

    cmb_smtp_secured.Add(1, GLB_NLS_GUI_CONFIGURATION.GetString(264))
    cmb_smtp_secured.Add(0, GLB_NLS_GUI_CONFIGURATION.GetString(265))

    gb_email_test.Text = GLB_NLS_GUI_CONFIGURATION.GetString(409)
    ef_email_test_to.Text = GLB_NLS_GUI_CONFIGURATION.GetString(410)
    btn_email_test.Text = GLB_NLS_GUI_CONFIGURATION.GetString(411)

    SetAuthorizedServerList(New String() {})

    'Disable Close Windows in Button OK
    CloseOnOkClick = False

  End Sub ' GUI_InitControls

  ' PURPOSE: Validate the data presented on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Dim authorized_server_list As String

    If Not chk_smtp_enabled.Checked Then

      Return True
    End If

    ef_smtp_server.Value = ef_smtp_server.Value.Trim()
    ef_smtp_port.Value = ef_smtp_port.Value.Trim()
    ef_smtp_username.Value = ef_smtp_username.Value.Trim()
    ef_smtp_email_from.Value = ef_smtp_email_from.Value.Trim()

    If ef_smtp_server.Value = "" Then
      Call ef_smtp_server.Focus()
      ' 219 "Field '%1' is mandatory"
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , ef_smtp_server.Text)

      Return False
    End If

    If ef_smtp_port.Value = "" Then
      Call ef_smtp_port.Focus()
      ' 219 "Field '%1' is mandatory"
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , ef_smtp_port.Text)

      Return False
    End If

    If ef_smtp_username.Value = "" Then
      ' 219 "Field '%1' is mandatory"
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , ef_smtp_username.Text)
      Call ef_smtp_username.Focus()

      Return False
    End If

    If ef_smtp_email_from.Value = "" Then
      ' 219 "Field '%1' is mandatory"
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , ef_smtp_email_from.Text)
      Call ef_smtp_email_from.Focus()

      Return False
    End If

    If Not WSI.Common.ValidateFormat.Email(ef_smtp_email_from.Value) Then
      ' 114 "Invalid email: %1"
      Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(114), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_smtp_email_from.Value)
      Call ef_smtp_email_from.Focus()

      Return False
    End If

    authorized_server_list = GetAuthorizedServerList()
    If String.IsNullOrEmpty(authorized_server_list) Then
      If NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(196), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then
        Return False
      End If
    End If

    Return True

  End Function ' GUI_IsScreenDataOk

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_GetScreenData()

    Dim mailing_params As CLASS_MAILING_PARAMETERS

    mailing_params = DbEditedObject

    mailing_params.AuthorizedServerList = GetAuthorizedServerList()
    mailing_params.MailingEnabled = chk_mailing_enabled.Checked
    mailing_params.SmtpEnabled = chk_smtp_enabled.Checked
    mailing_params.SmtpServer = ef_smtp_server.Value
    mailing_params.SmtpPort = GUI_ParseNumber(ef_smtp_port.Value)
    mailing_params.SmtpSecured = cmb_smtp_secured.Value
    mailing_params.SmtpUsername = ef_smtp_username.Value
    mailing_params.SmtpPassword = ef_smtp_password.Value
    mailing_params.SmtpDomain = ef_smtp_domain.Value
    mailing_params.SmtpEmailFrom = ef_smtp_email_from.Value

  End Sub ' GUI_GetScreenData

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim mailing_params As CLASS_MAILING_PARAMETERS
    Dim machines() As String

    mailing_params = DbReadObject

    machines = mailing_params.AuthorizedServerList.Split(",")
    SetAuthorizedServerList(machines)

    chk_mailing_enabled.Checked = mailing_params.MailingEnabled
    chk_smtp_enabled.Checked = mailing_params.SmtpEnabled
    ef_smtp_server.Value = mailing_params.SmtpServer
    ef_smtp_port.Value = mailing_params.SmtpPort
    cmb_smtp_secured.Value = IIf(mailing_params.SmtpSecured, "1", "0")
    ef_smtp_username.Value = mailing_params.SmtpUsername
    ef_smtp_password.Value = mailing_params.SmtpPassword
    ef_smtp_domain.Value = mailing_params.SmtpDomain
    ef_smtp_email_from.Value = mailing_params.SmtpEmailFrom

  End Sub ' GUI_SetScreenData

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Dim mailing_params As CLASS_MAILING_PARAMETERS

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_MAILING_PARAMETERS
        mailing_params = DbEditedObject

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)
    End Select


  End Sub ' GUI_DB_Operation

  ' PURPOSE: Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.chk_mailing_enabled

  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Process button actions in order to branch to a child screen
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    If ButtonId = ENUM_BUTTON.BUTTON_OK Then

      ' Assign to ERROR, so the message will be only shown when OK.
      DbStatus = ENUM_STATUS.STATUS_ERROR
      MyBase.GUI_ButtonClick(ButtonId)

      If DbStatus = ENUM_STATUS.STATUS_OK Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(108), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO)
      End If

    Else
      MyBase.GUI_ButtonClick(ButtonId)
    End If

  End Sub ' GUI_ButtonClick

#End Region ' Overrides

#Region " Private "

  ' PURPOSE: Send Test Email.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - none
  '
  Private Sub SendTestEmail()

    Dim mailing_params As WSI.Common.TYPE_MAILING_PARAMETERS
    Dim error_msg As String

    ' Check the data on the screen
    If Not GUI_IsScreenDataOk() Then
      Exit Sub
    End If

    ef_email_test_to.Value = ef_email_test_to.Value.Trim()

    If ef_email_test_to.Value = "" Then
      Call ef_email_test_to.Focus()
      ' 219 "Field '%1' is mandatory"
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , ef_email_test_to.Text)

      Exit Sub
    End If

    If Not WSI.Common.ValidateFormat.Email(ef_email_test_to.Value) Then
      ' 114 "Invalid email: %1"
      Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(114), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_email_test_to.Value)
      Call ef_email_test_to.Focus()

      Exit Sub
    End If

    mailing_params.authorized_server_list = ""
    mailing_params.enabled = True
    mailing_params.smtp_enabled = True
    mailing_params.smtp_server = ef_smtp_server.Value
    mailing_params.smtp_port = GUI_ParseNumber(ef_smtp_port.Value)
    mailing_params.smtp_secured = cmb_smtp_secured.Value
    mailing_params.smtp_username = ef_smtp_username.Value
    mailing_params.smtp_password = ef_smtp_password.Value
    mailing_params.smtp_domain = ef_smtp_domain.Value
    mailing_params.smtp_email_from = ef_smtp_email_from.Value

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    Try
      WSI.Common.Mailing.SendTestEmail(mailing_params, ef_email_test_to.Value)

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(835), ENUM_MB_TYPE.MB_TYPE_INFO)

      'MessageBox.Show("Mensaje de prueba enviado correctamente.", "Correo de Prueba", _
      'MessageBoxButtons.OK, MessageBoxIcon.Information)

    Catch ex As Exception
      error_msg = ex.Message
      While ex.InnerException IsNot Nothing
        error_msg = error_msg & vbCrLf & ex.InnerException.Message
        ex = ex.InnerException
      End While

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(836), ENUM_MB_TYPE.MB_TYPE_ERROR, , , error_msg)

      'MessageBox.Show(error_msg, "Correo de Prueba", MessageBoxButtons.OK, MessageBoxIcon.Error)
    Finally
      Windows.Forms.Cursor.Current = Cursors.Default
    End Try

  End Sub ' SendTestEmail

  ' PURPOSE: Set Authorized Server List. Mark checked the SelectedServers.
  '
  '  PARAMS:
  '     - INPUT:
  '           - SelectedServers() As String
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - none
  '
  Private Sub SetAuthorizedServerList(ByVal SelectedServers() As String)
    Dim str_sql As String
    Dim data_table As DataTable
    Dim machine As String
    Dim found As Boolean

    str_sql = "SELECT   APP_MACHINE " & _
              "  FROM   APPLICATIONS " & _
              " WHERE   APP_NAME = '" + MAILING_SERVICE_NAME + "' " & _
              "ORDER BY APP_MACHINE "

    data_table = GUI_GetTableUsingSQL(str_sql, 20)

    clb_authorized_server_list.Items.Clear()

    For Each row_machine As DataRow In data_table.Rows
      found = False
      machine = row_machine(0)
      For Each server As String In SelectedServers
        If machine = server Then
          found = True
          Exit For
        End If
      Next
      clb_authorized_server_list.Items.Add(machine, found)
    Next

  End Sub ' SetAuthorizedServerList

  ' PURPOSE: Get Authorized Server List.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - String
  '
  Private Function GetAuthorizedServerList() As String
    Dim server_list As String
    Dim idx_item As Integer

    server_list = ""

    For idx_item = 0 To clb_authorized_server_list.Items.Count - 1
      If clb_authorized_server_list.GetItemChecked(idx_item) Then
        server_list = server_list & "," & clb_authorized_server_list.Items.Item(idx_item)
      End If
    Next
    If server_list.Length > 0 Then
      server_list = server_list.Substring(1)
    End If

    Return server_list
  End Function ' GetAuthorizedServerList

#End Region ' Private

#Region "Public Functions"

  ' PURPOSE: Init form in edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Overloads Sub ShowForEdit()

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Events "

  Private Sub btn_email_test_ClickEvent() Handles btn_email_test.ClickEvent
    SendTestEmail()
  End Sub ' btn_email_test_ClickEvent

  Private Sub ef_email_test_to_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ef_email_test_to.Enter
    m_default_button = Me.AcceptButton
    Me.AcceptButton = Me.btn_email_test
  End Sub ' ef_email_test_to_Enter

  Private Sub ef_email_test_to_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ef_email_test_to.Leave
    Me.AcceptButton = m_default_button
  End Sub ' ef_email_test_to_Leave

#End Region ' Events

End Class ' frm_mailing_programming_edit
