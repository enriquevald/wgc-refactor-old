<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_alarm_inquiry_source
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.cb_all_alarms = New System.Windows.Forms.CheckBox
    Me.ef_alarm_id = New GUI_Controls.uc_entry_field
    Me.lbl_alarms_that_belong_to_pattern = New GUI_Controls.uc_text_field
    Me.lbl_alarm_generate_by_pattern = New GUI_Controls.uc_text_field
    Me.lbl_alarms_that_belong_to_pattern_color = New System.Windows.Forms.Label
    Me.lbl_alarm_generate_by_pattern_color = New System.Windows.Forms.Label
    Me.lbl_date_text = New GUI_Controls.uc_text_field
    Me.lbl_date = New System.Windows.Forms.Label
    Me.lbl_pattern_name = New System.Windows.Forms.Label
    Me.lbl_pattern_name_text = New GUI_Controls.uc_text_field
    Me.lbl_alarm_name = New System.Windows.Forms.Label
    Me.lbl_alarm_name_text = New GUI_Controls.uc_text_field
    Me.lbl_alarm_description = New System.Windows.Forms.Label
    Me.lbl_alarm_description_text = New GUI_Controls.uc_text_field
    Me.lbl_pattern_description = New System.Windows.Forms.Label
    Me.lbl_pattern_description_text = New GUI_Controls.uc_text_field
    Me.lbl_terminal = New System.Windows.Forms.Label
    Me.lbl_terminal_text = New GUI_Controls.uc_text_field
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.lbl_alarm_description)
    Me.panel_filter.Controls.Add(Me.lbl_alarm_description_text)
    Me.panel_filter.Controls.Add(Me.lbl_pattern_description)
    Me.panel_filter.Controls.Add(Me.lbl_pattern_description_text)
    Me.panel_filter.Controls.Add(Me.lbl_terminal)
    Me.panel_filter.Controls.Add(Me.lbl_terminal_text)
    Me.panel_filter.Controls.Add(Me.lbl_alarm_name)
    Me.panel_filter.Controls.Add(Me.lbl_alarm_name_text)
    Me.panel_filter.Controls.Add(Me.lbl_pattern_name)
    Me.panel_filter.Controls.Add(Me.lbl_pattern_name_text)
    Me.panel_filter.Controls.Add(Me.lbl_date)
    Me.panel_filter.Controls.Add(Me.lbl_date_text)
    Me.panel_filter.Controls.Add(Me.lbl_alarm_generate_by_pattern_color)
    Me.panel_filter.Controls.Add(Me.lbl_alarms_that_belong_to_pattern_color)
    Me.panel_filter.Controls.Add(Me.lbl_alarm_generate_by_pattern)
    Me.panel_filter.Controls.Add(Me.lbl_alarms_that_belong_to_pattern)
    Me.panel_filter.Controls.Add(Me.cb_all_alarms)
    Me.panel_filter.Controls.Add(Me.ef_alarm_id)
    Me.panel_filter.Size = New System.Drawing.Size(1240, 233)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_alarm_id, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.cb_all_alarms, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_alarms_that_belong_to_pattern, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_alarm_generate_by_pattern, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_alarms_that_belong_to_pattern_color, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_alarm_generate_by_pattern_color, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_date_text, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_pattern_name_text, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_pattern_name, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_alarm_name_text, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_alarm_name, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_terminal_text, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_terminal, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_pattern_description_text, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_pattern_description, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_alarm_description_text, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_alarm_description, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 237)
    Me.panel_data.Size = New System.Drawing.Size(1240, 421)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1234, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1234, 4)
    '
    'cb_all_alarms
    '
    Me.cb_all_alarms.AutoSize = True
    Me.cb_all_alarms.Location = New System.Drawing.Point(715, 206)
    Me.cb_all_alarms.Name = "cb_all_alarms"
    Me.cb_all_alarms.Size = New System.Drawing.Size(183, 17)
    Me.cb_all_alarms.TabIndex = 11
    Me.cb_all_alarms.Text = "xViewAllAlarmsInThePeriod"
    Me.cb_all_alarms.UseVisualStyleBackColor = True
    '
    'ef_alarm_id
    '
    Me.ef_alarm_id.DoubleValue = 0
    Me.ef_alarm_id.IntegerValue = 0
    Me.ef_alarm_id.IsReadOnly = False
    Me.ef_alarm_id.Location = New System.Drawing.Point(993, 3)
    Me.ef_alarm_id.Name = "ef_alarm_id"
    Me.ef_alarm_id.Size = New System.Drawing.Size(150, 24)
    Me.ef_alarm_id.SufixText = "Sufix Text"
    Me.ef_alarm_id.SufixTextVisible = True
    Me.ef_alarm_id.TabIndex = 13
    Me.ef_alarm_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_alarm_id.TextValue = ""
    Me.ef_alarm_id.TextWidth = 50
    Me.ef_alarm_id.Value = ""
    Me.ef_alarm_id.Visible = False
    '
    'lbl_alarms_that_belong_to_pattern
    '
    Me.lbl_alarms_that_belong_to_pattern.AutoSize = True
    Me.lbl_alarms_that_belong_to_pattern.IsReadOnly = True
    Me.lbl_alarms_that_belong_to_pattern.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_alarms_that_belong_to_pattern.LabelForeColor = System.Drawing.SystemColors.WindowText
    Me.lbl_alarms_that_belong_to_pattern.Location = New System.Drawing.Point(34, 202)
    Me.lbl_alarms_that_belong_to_pattern.Name = "lbl_alarms_that_belong_to_pattern"
    Me.lbl_alarms_that_belong_to_pattern.Size = New System.Drawing.Size(216, 24)
    Me.lbl_alarms_that_belong_to_pattern.SufixText = "Sufix Text"
    Me.lbl_alarms_that_belong_to_pattern.SufixTextVisible = True
    Me.lbl_alarms_that_belong_to_pattern.TabIndex = 21
    Me.lbl_alarms_that_belong_to_pattern.TabStop = False
    Me.lbl_alarms_that_belong_to_pattern.TextWidth = 0
    Me.lbl_alarms_that_belong_to_pattern.Value = "x Alarms that belong to pattern"
    '
    'lbl_alarm_generate_by_pattern
    '
    Me.lbl_alarm_generate_by_pattern.AutoSize = True
    Me.lbl_alarm_generate_by_pattern.IsReadOnly = True
    Me.lbl_alarm_generate_by_pattern.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_alarm_generate_by_pattern.LabelForeColor = System.Drawing.SystemColors.WindowText
    Me.lbl_alarm_generate_by_pattern.Location = New System.Drawing.Point(391, 202)
    Me.lbl_alarm_generate_by_pattern.Name = "lbl_alarm_generate_by_pattern"
    Me.lbl_alarm_generate_by_pattern.Size = New System.Drawing.Size(216, 24)
    Me.lbl_alarm_generate_by_pattern.SufixText = "Sufix Text"
    Me.lbl_alarm_generate_by_pattern.SufixTextVisible = True
    Me.lbl_alarm_generate_by_pattern.TabIndex = 22
    Me.lbl_alarm_generate_by_pattern.TabStop = False
    Me.lbl_alarm_generate_by_pattern.TextWidth = 0
    Me.lbl_alarm_generate_by_pattern.Value = "x Alarm generate by pattern"
    '
    'lbl_alarms_that_belong_to_pattern_color
    '
    Me.lbl_alarms_that_belong_to_pattern_color.AutoSize = True
    Me.lbl_alarms_that_belong_to_pattern_color.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_alarms_that_belong_to_pattern_color.Location = New System.Drawing.Point(13, 207)
    Me.lbl_alarms_that_belong_to_pattern_color.MaximumSize = New System.Drawing.Size(15, 15)
    Me.lbl_alarms_that_belong_to_pattern_color.MinimumSize = New System.Drawing.Size(15, 15)
    Me.lbl_alarms_that_belong_to_pattern_color.Name = "lbl_alarms_that_belong_to_pattern_color"
    Me.lbl_alarms_that_belong_to_pattern_color.Size = New System.Drawing.Size(15, 15)
    Me.lbl_alarms_that_belong_to_pattern_color.TabIndex = 23
    Me.lbl_alarms_that_belong_to_pattern_color.Text = "B"
    '
    'lbl_alarm_generate_by_pattern_color
    '
    Me.lbl_alarm_generate_by_pattern_color.AutoSize = True
    Me.lbl_alarm_generate_by_pattern_color.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_alarm_generate_by_pattern_color.Location = New System.Drawing.Point(370, 207)
    Me.lbl_alarm_generate_by_pattern_color.MaximumSize = New System.Drawing.Size(15, 15)
    Me.lbl_alarm_generate_by_pattern_color.MinimumSize = New System.Drawing.Size(15, 15)
    Me.lbl_alarm_generate_by_pattern_color.Name = "lbl_alarm_generate_by_pattern_color"
    Me.lbl_alarm_generate_by_pattern_color.Size = New System.Drawing.Size(15, 15)
    Me.lbl_alarm_generate_by_pattern_color.TabIndex = 24
    Me.lbl_alarm_generate_by_pattern_color.Text = "G"
    '
    'lbl_date_text
    '
    Me.lbl_date_text.AutoSize = True
    Me.lbl_date_text.IsReadOnly = True
    Me.lbl_date_text.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_RIGTH
    Me.lbl_date_text.LabelForeColor = System.Drawing.SystemColors.WindowText
    Me.lbl_date_text.Location = New System.Drawing.Point(6, 3)
    Me.lbl_date_text.Name = "lbl_date_text"
    Me.lbl_date_text.Size = New System.Drawing.Size(185, 25)
    Me.lbl_date_text.SufixText = "Sufix Text"
    Me.lbl_date_text.SufixTextVisible = True
    Me.lbl_date_text.TabIndex = 25
    Me.lbl_date_text.TabStop = False
    Me.lbl_date_text.TextWidth = 0
    Me.lbl_date_text.Value = "x Date"
    '
    'lbl_date
    '
    Me.lbl_date.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_date.Location = New System.Drawing.Point(197, 8)
    Me.lbl_date.Name = "lbl_date"
    Me.lbl_date.Size = New System.Drawing.Size(132, 20)
    Me.lbl_date.TabIndex = 26
    Me.lbl_date.Text = "x 99/99/9999"
    '
    'lbl_pattern_name
    '
    Me.lbl_pattern_name.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_pattern_name.Location = New System.Drawing.Point(197, 34)
    Me.lbl_pattern_name.Name = "lbl_pattern_name"
    Me.lbl_pattern_name.Size = New System.Drawing.Size(884, 20)
    Me.lbl_pattern_name.TabIndex = 28
    Me.lbl_pattern_name.Text = "x pattern_name"
    '
    'lbl_pattern_name_text
    '
    Me.lbl_pattern_name_text.AutoSize = True
    Me.lbl_pattern_name_text.IsReadOnly = True
    Me.lbl_pattern_name_text.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_RIGTH
    Me.lbl_pattern_name_text.LabelForeColor = System.Drawing.SystemColors.WindowText
    Me.lbl_pattern_name_text.Location = New System.Drawing.Point(6, 28)
    Me.lbl_pattern_name_text.Name = "lbl_pattern_name_text"
    Me.lbl_pattern_name_text.Size = New System.Drawing.Size(185, 25)
    Me.lbl_pattern_name_text.SufixText = "Sufix Text"
    Me.lbl_pattern_name_text.SufixTextVisible = True
    Me.lbl_pattern_name_text.TabIndex = 27
    Me.lbl_pattern_name_text.TabStop = False
    Me.lbl_pattern_name_text.TextWidth = 0
    Me.lbl_pattern_name_text.Value = "x pattern name text"
    '
    'lbl_alarm_name
    '
    Me.lbl_alarm_name.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_alarm_name.Location = New System.Drawing.Point(197, 119)
    Me.lbl_alarm_name.Name = "lbl_alarm_name"
    Me.lbl_alarm_name.Size = New System.Drawing.Size(884, 20)
    Me.lbl_alarm_name.TabIndex = 30
    Me.lbl_alarm_name.Text = "x alarm_name"
    '
    'lbl_alarm_name_text
    '
    Me.lbl_alarm_name_text.AutoSize = True
    Me.lbl_alarm_name_text.IsReadOnly = True
    Me.lbl_alarm_name_text.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_RIGTH
    Me.lbl_alarm_name_text.LabelForeColor = System.Drawing.SystemColors.WindowText
    Me.lbl_alarm_name_text.Location = New System.Drawing.Point(6, 114)
    Me.lbl_alarm_name_text.Name = "lbl_alarm_name_text"
    Me.lbl_alarm_name_text.Size = New System.Drawing.Size(185, 25)
    Me.lbl_alarm_name_text.SufixText = "Sufix Text"
    Me.lbl_alarm_name_text.SufixTextVisible = True
    Me.lbl_alarm_name_text.TabIndex = 29
    Me.lbl_alarm_name_text.TabStop = False
    Me.lbl_alarm_name_text.TextWidth = 0
    Me.lbl_alarm_name_text.Value = "x alarm name text"
    '
    'lbl_alarm_description
    '
    Me.lbl_alarm_description.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_alarm_description.Location = New System.Drawing.Point(197, 143)
    Me.lbl_alarm_description.Name = "lbl_alarm_description"
    Me.lbl_alarm_description.Size = New System.Drawing.Size(884, 60)
    Me.lbl_alarm_description.TabIndex = 36
    Me.lbl_alarm_description.Text = "x alarm description"
    '
    'lbl_alarm_description_text
    '
    Me.lbl_alarm_description_text.AutoSize = True
    Me.lbl_alarm_description_text.IsReadOnly = True
    Me.lbl_alarm_description_text.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_RIGTH
    Me.lbl_alarm_description_text.LabelForeColor = System.Drawing.SystemColors.WindowText
    Me.lbl_alarm_description_text.Location = New System.Drawing.Point(6, 138)
    Me.lbl_alarm_description_text.Name = "lbl_alarm_description_text"
    Me.lbl_alarm_description_text.Size = New System.Drawing.Size(185, 25)
    Me.lbl_alarm_description_text.SufixText = "Sufix Text"
    Me.lbl_alarm_description_text.SufixTextVisible = True
    Me.lbl_alarm_description_text.TabIndex = 35
    Me.lbl_alarm_description_text.TabStop = False
    Me.lbl_alarm_description_text.TextWidth = 0
    Me.lbl_alarm_description_text.Value = "x alarm description txt"
    '
    'lbl_pattern_description
    '
    Me.lbl_pattern_description.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_pattern_description.Location = New System.Drawing.Point(197, 56)
    Me.lbl_pattern_description.Name = "lbl_pattern_description"
    Me.lbl_pattern_description.Size = New System.Drawing.Size(884, 60)
    Me.lbl_pattern_description.TabIndex = 34
    Me.lbl_pattern_description.Text = "x pattern description"
    '
    'lbl_pattern_description_text
    '
    Me.lbl_pattern_description_text.AutoSize = True
    Me.lbl_pattern_description_text.IsReadOnly = True
    Me.lbl_pattern_description_text.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_RIGTH
    Me.lbl_pattern_description_text.LabelForeColor = System.Drawing.SystemColors.WindowText
    Me.lbl_pattern_description_text.Location = New System.Drawing.Point(6, 51)
    Me.lbl_pattern_description_text.Name = "lbl_pattern_description_text"
    Me.lbl_pattern_description_text.Size = New System.Drawing.Size(185, 25)
    Me.lbl_pattern_description_text.SufixText = "Sufix Text"
    Me.lbl_pattern_description_text.SufixTextVisible = True
    Me.lbl_pattern_description_text.TabIndex = 33
    Me.lbl_pattern_description_text.TabStop = False
    Me.lbl_pattern_description_text.TextWidth = 0
    Me.lbl_pattern_description_text.Value = "x pattern description t"
    '
    'lbl_terminal
    '
    Me.lbl_terminal.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_terminal.Location = New System.Drawing.Point(514, 8)
    Me.lbl_terminal.Name = "lbl_terminal"
    Me.lbl_terminal.Size = New System.Drawing.Size(473, 24)
    Me.lbl_terminal.TabIndex = 32
    Me.lbl_terminal.Text = "lbl_terminal"
    '
    'lbl_terminal_text
    '
    Me.lbl_terminal_text.AutoSize = True
    Me.lbl_terminal_text.IsReadOnly = True
    Me.lbl_terminal_text.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_RIGTH
    Me.lbl_terminal_text.LabelForeColor = System.Drawing.SystemColors.WindowText
    Me.lbl_terminal_text.Location = New System.Drawing.Point(355, 3)
    Me.lbl_terminal_text.Name = "lbl_terminal_text"
    Me.lbl_terminal_text.Size = New System.Drawing.Size(153, 25)
    Me.lbl_terminal_text.SufixText = "Sufix Text"
    Me.lbl_terminal_text.SufixTextVisible = True
    Me.lbl_terminal_text.TabIndex = 31
    Me.lbl_terminal_text.TabStop = False
    Me.lbl_terminal_text.TextWidth = 0
    Me.lbl_terminal_text.Value = "x Terminal text"
    '
    'frm_alarm_inquiry_source
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1248, 662)
    Me.Name = "frm_alarm_inquiry_source"
    Me.Text = "frm_alarm_inquiry_source"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents cb_all_alarms As System.Windows.Forms.CheckBox
  Friend WithEvents ef_alarm_id As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_alarm_generate_by_pattern As GUI_Controls.uc_text_field
  Friend WithEvents lbl_alarms_that_belong_to_pattern As GUI_Controls.uc_text_field
  Friend WithEvents lbl_alarms_that_belong_to_pattern_color As System.Windows.Forms.Label
  Friend WithEvents lbl_alarm_generate_by_pattern_color As System.Windows.Forms.Label
  Friend WithEvents lbl_date As System.Windows.Forms.Label
  Friend WithEvents lbl_date_text As GUI_Controls.uc_text_field
  Friend WithEvents lbl_pattern_name As System.Windows.Forms.Label
  Friend WithEvents lbl_pattern_name_text As GUI_Controls.uc_text_field
  Friend WithEvents lbl_alarm_description As System.Windows.Forms.Label
  Friend WithEvents lbl_alarm_description_text As GUI_Controls.uc_text_field
  Friend WithEvents lbl_pattern_description As System.Windows.Forms.Label
  Friend WithEvents lbl_pattern_description_text As GUI_Controls.uc_text_field
  Friend WithEvents lbl_terminal As System.Windows.Forms.Label
  Friend WithEvents lbl_terminal_text As GUI_Controls.uc_text_field
  Friend WithEvents lbl_alarm_name As System.Windows.Forms.Label
  Friend WithEvents lbl_alarm_name_text As GUI_Controls.uc_text_field
End Class
