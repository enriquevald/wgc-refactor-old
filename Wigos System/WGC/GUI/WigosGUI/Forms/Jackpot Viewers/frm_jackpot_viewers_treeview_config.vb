﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_jackpot_viewers_treeview_config
' DESCRIPTION:   Jackpot Viewers New Treview form
' AUTHOR:        Carlos Cáceres González
' CREATION DATE: 16-MAY-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 16-MAY-2017  CCG    Initial version
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonMisc
Imports System.Text
Imports WSI.Common
Imports WSI.Common.Jackpot

Public Class frm_jackpot_viewers_treeview_config
  Inherits frm_treeview_edit

#Region "Constants"

#End Region ' Constants

#Region "Members"

  Private m_jackpot_viewers As CLASS_JACKPOT_VIEWER
  Private m_new_jackpot_viewer_label As String

#End Region ' Members

#Region "Propeties"

  Public Property JackpotViewers As CLASS_JACKPOT_VIEWER
    Get
      Return Me.m_jackpot_viewers
    End Get
    Set(value As CLASS_JACKPOT_VIEWER)
      Me.m_jackpot_viewers = value
    End Set
  End Property

  Public Property NewJackpotViewerLabel As String
    Get
      Return Me.m_new_jackpot_viewer_label
    End Get
    Set(value As String)
      Me.m_new_jackpot_viewer_label = value
    End Set
  End Property

#End Region ' Members

#Region "Overrides"

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_JACKPOT_VIEWER_TREEVIEW_EDIT

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <param name="DbOperation"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        Me.DbStatus = ENUM_STATUS.STATUS_OK

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          If Me.DbStatus = ENUM_STATUS.STATUS_NOT_FOUND Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8034), ENUM_MB_TYPE.MB_TYPE_ERROR) ' "Jackpot viewer was not found." 
          Else
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8035), ENUM_MB_TYPE.MB_TYPE_ERROR) ' "An error occurred while reading Jackpot viewer data." 
          End If

        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8300), ENUM_MB_TYPE.MB_TYPE_ERROR) ' "An error occurred while creating the Jackpot." 
        Else
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8301), ENUM_MB_TYPE.MB_TYPE_INFO, ENUM_MB_BTN.MB_BTN_OK) ' "The jackpot viewer has been saved properly."
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE

        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          If Me.DbStatus = ENUM_STATUS.STATUS_DEPENDENCIES Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8302), ENUM_MB_TYPE.MB_TYPE_ERROR) ' "The Jackpot viewer cannot be disabled because it has actually in use." 
          ElseIf Me.DbStatus = ENUM_STATUS.STATUS_NOT_FOUND Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8303), ENUM_MB_TYPE.MB_TYPE_ERROR) ' "Jackpot viewer was not found."
          Else
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8304), ENUM_MB_TYPE.MB_TYPE_ERROR) ' "An error occurred while creating the Jackpot."
          End If
        Else
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8301), ENUM_MB_TYPE.MB_TYPE_INFO, ENUM_MB_BTN.MB_BTN_OK) ' "The jackpot viewer has been saved properly."
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)
    End Select

  End Sub ' GUI_DB_Operation

  ''' <summary>
  ''' Key press event
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean
    Return False
  End Function ' GUI_KeyPress

  Protected Overrides Function GUI_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) As Boolean

    If Me.NodeInEdition Is Nothing Then
      Return True
    End If

    Select Case (Me.NodeInEdition.Tag)

      Case EditSelectionItemType.JACKPOT_VIEWER_CONFIG
        KeyUp_JackpotViewerConfig(sender, e)

      Case Else

    End Select

    Return True

  End Function ' GUI_KeyUp

  ''' <summary>
  ''' Check screen data
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    If Me.NodeInEdition Is Nothing Then
      Return True
    End If

    Select Case (Me.NodeInEdition.Tag)

      Case EditSelectionItemType.JACKPOT_VIEWER_CONFIG
        Return Me.IsScreenDataOk_JackpotViewerConfig()

      Case EditSelectionItemType.JACKPOT_VIEWER_TERMINAL
        Return Me.IsScreenDataOk_JackpotViewerTerminals()

    End Select

    Return False

  End Function ' GUI_IsScreenDataOk

  ''' <summary>
  ''' Get screen data from specific control
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_GetScreenData()

    If Me.DbEditedObject Is Nothing Then
      Return
    End If

    Me.JackpotViewers = Me.DbEditedObject

    If Me.NodeInEdition Is Nothing Then
      Return
    End If

    Select Case (Me.NodeInEdition.Tag)

      Case EditSelectionItemType.JACKPOT_VIEWER_CONFIG
        GetScreenData_JackpotViewerConfig()

      Case EditSelectionItemType.JACKPOT_VIEWER_TERMINAL
        GetScreenData_JackpotViewerConfigTerminals()

    End Select

    ' Set last modification (Always Update Jackpot Main Class LastUpdate)
    Me.JackpotViewers.Selected.LastModification = WGDB.Now

  End Sub ' GUI_GetScreenData

  ''' <summary>
  ''' Set screen data base method
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    MyBase.GUI_SetScreenData(SqlCtx)

  End Sub ' GUI_SetScreenData

  ''' <summary>
  ''' Initializes resources for controls
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_InitControls()

    Dim _flags As DataTable
    _flags = New DataTable()

    Me.CloseOnOkClick = False
    Me.DbEditedObject = New CLASS_JACKPOT_VIEWER()
    Me.JackpotViewers = Me.DbEditedObject

    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8380)

    ' Bind TreeView     
    Me.InitializeJackpotViewerTreeView()
    Me.BindTreeNodeToEdit()

    ' Controls:
    Me.GUI_Button(frm_treeview_edit.ENUM_BUTTON.BUTTON_NEW).Visible = False

  End Sub ' GUI_InitControls

  ''' <summary>
  ''' Handle Form buttons clicks
  ''' </summary>
  ''' <param name="ButtonId"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_OK
        ' Save without close form
        MyBase.CloseOnOkClick = False
        Call MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_OK)
        MyBase.CloseOnOkClick = True

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick


  ''' <summary>
  ''' Initialize treeview control
  ''' </summary>
  ''' <param name="RootDescription"></param>
  ''' <remarks></remarks>
  Public Overrides Sub InitTreeView(Optional ByVal RootDescription As String = uc_treeview.DEFAULT_TREEVIEW_ROOT_DESCRIPTION)

    Me.TreeView.ItemList = Me.JackpotViewers.List.ToTreeViewItemList()

    TreeView.IconPerNode = False

    If TreeView.IconPerNode Then
      TreeView.ImageList = New ImageList()

      ' TODO JBP: Made generic. Actualmente se hace aquí porque en el GUI_Controls no tenemos acceso al fichero de recursos. Esto debería estar en el uc_treeview. Buscar solución. 
      TreeView.ImageList.Images.Add(CLASS_GUI_RESOURCES.GetImage("node_enabled"))
      TreeView.ImageList.Images.Add(CLASS_GUI_RESOURCES.GetImage("node_disabled"))
      TreeView.ImageList.Images.Add(CLASS_GUI_RESOURCES.GetImage("node_warning"))
    End If

    MyBase.InitTreeView(RootDescription)

  End Sub

  ''' <summary>
  ''' Discard jackpot treeview changes
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub DiscardTreeViewChanges(ByRef TreeNode As TreeNode)

    MyBase.DiscardTreeViewChanges(TreeNode)

    ' Only for jackpot treeview item
    If Me.TreeView.SelectedNode Is Nothing Then
      Return
    End If

    ' Restore original name (Jackpot name node = monitor)
    If Me.TreeView.SelectedNode.Tag = EditSelectionItemType.JACKPOT_VIEWER_MONITOR Then
      Me.SetSelectedNodeDescription(CType(DbReadObject, CLASS_JACKPOT_VIEWER).Selected.Name, True)
    End If

  End Sub

  ''' <summary>
  ''' Manage selected TreeNode item
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Public Overrides Sub TreeView_AfterSelect(sender As Object, e As TreeViewEventArgs)

    MyBase.TreeView_AfterSelect(sender, e)

    If Me.TreeView.IsNewNode() Then
      Me.JackpotViewers.Selected = New JackpotViewer()
    Else
      Me.SetSelectedJackpot()
    End If

  End Sub ' TreeView_AfterSelect

  ''' <summary>
  ''' Get object form treenode
  ''' </summary>
  ''' <param name="TreeNode"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function GetObjectFromTreeNode(ByVal TreeNode As TreeNode) As Object

    ' Check TreeNode item id
    If GUI_ParseNumber(TreeNode.Name) <= 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8087), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)

      Return Nothing
    End If

    Return Me.JackpotViewers.List.GetJackpotViewerById(CInt(TreeNode.Name))

  End Function

  Public Overrides Sub TreeView_NodeMouseClick(sender As Object, e As TreeNodeMouseClickEventArgs)

    MyBase.TreeView_NodeMouseClick(sender, e)

  End Sub

  ''' <summary>
  ''' Set show new item mode
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_ShowNewItem()

    ' Sets the screen mode
    MyBase.GUI_ShowNewItem()

  End Sub

  ''' <summary>
  ''' Set show edit item mode
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_ShowEditItem(ByVal ObjectId As Object)
    ' Sets the screen mode
    MyBase.GUI_ShowEditItem(ObjectId)

    Me.JackpotViewers = Me.DbEditedObject
  End Sub

  ''' <summary>
  ''' Get specific context menu
  ''' </summary>
  ''' <param name="ObjectId"></param>
  ''' <param name="Type"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function TreeView_GetContextMenu(ByVal JackpotId As String, ByVal Type As EditSelectionItemType) As ContextMenu

    ' Get context menu
    Select Case Type

      Case EditSelectionItemType.JACKPOT_VIEWER_CONFIG ' Jackpot Viewer
        'Return Me.CreateRootContextMenu()

      Case EditSelectionItemType.NONE ' Root
        Return Me.CreateRootContextMenu()

    End Select

    Return Nothing

  End Function


#End Region ' Overrides

#Region "Public Functions"

  Public Sub MenuItem_OnClick(sender As Object, e As EventArgs)
    Dim _menu_item As MenuItem

    _menu_item = CType(sender, MenuItem)

    If Not _menu_item Is Nothing Then
      Call OnClickEvent(_menu_item)
    End If

  End Sub

#End Region

#Region "Private Functions"

  ''' <summary>
  ''' Manage click on context menu
  ''' </summary>
  ''' <param name="TreeNodeMenuItem"></param>
  ''' <remarks></remarks>
  Private Sub OnClickEvent(ByVal TreeNodeMenuItem As MenuItem)
    Dim _action As ContextMenuAction

    _action = CType(TreeNodeMenuItem.Tag, ContextMenuAction)

    ' Must be force RootNode Selection if clicked in anyone action for check diferences in current item.
    '   - Only for not new nodes.
    If Not Me.TreeView.IsNewNode() Then
      Me.TreeView.SelectedNode = Me.TreeView.RootNode
    End If

    Select Case _action

      Case ContextMenuAction.ROOT_COLLAPSE_ALL
        Call Me.OnclickEvent_TreeViewCollapseAll()

      Case ContextMenuAction.ROOT_EXPAND_ALL
        Call Me.OnclickEvent_TreeViewExpandAll()

      Case ContextMenuAction.ROOT_REFRESH
        Call Me.OnclickEvent_TreeViewRefresh()

      Case ContextMenuAction.JACKPOT_NEW
        Call Me.OnclickEvent_JackpotViewerNew()

    End Select

  End Sub ' OnClickEvent

  ''' <summary>
  ''' Add new jackpot node 
  ''' </summary>
  ''' <param name="JackpotId"></param>
  ''' <remarks></remarks>
  'Private Sub AddNewJackpotViewerNode(ByVal JackpotViewerId As Integer, ByVal IsClon As Boolean)
  Private Sub AddNewJackpotViewerNode(ByVal JackpotViewerId As Integer)
    ' Set New Jackpot label for replace in case Name Textbox is empty
    'Me.NewJackpotViewerLabel = IIf(IsClon, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8368), GLB_NLS_GUI_JACKPOT_MGR.GetString(597))
    Me.NewJackpotViewerLabel = GLB_NLS_GUI_JACKPOT_MGR.GetString(597)

    Me.TreeView.AddNewNode(JackpotViewerId.ToString(), Me.NewJackpotViewerLabel, EditSelectionItemType.JACKPOT_VIEWER_CONFIG)
  End Sub

  ''' <summary>
  ''' Create root context menu
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CreateRootContextMenu() As ContextMenu
    Dim _root_contextmenu As ContextMenu

    _root_contextmenu = New ContextMenu()
    _root_contextmenu.Name = Me.TreeView.GetRootName()
    _root_contextmenu.MenuItems.Add(CreateMenuItem(ContextMenuAction.JACKPOT_NEW, GLB_NLS_GUI_JACKPOT_MGR.GetString(597), AddressOf MenuItem_OnClick))
    _root_contextmenu.MenuItems.Add(CreateMenuItem(ContextMenuAction.NONE, "-", Nothing))
    _root_contextmenu.MenuItems.Add(CreateMenuItem(ContextMenuAction.ROOT_COLLAPSE_ALL, GLB_NLS_GUI_JACKPOT_MGR.GetString(527), AddressOf MenuItem_OnClick))
    _root_contextmenu.MenuItems.Add(CreateMenuItem(ContextMenuAction.ROOT_EXPAND_ALL, GLB_NLS_GUI_JACKPOT_MGR.GetString(528), AddressOf MenuItem_OnClick))
    _root_contextmenu.MenuItems.Add(CreateMenuItem(ContextMenuAction.ROOT_REFRESH, GLB_NLS_GUI_JACKPOT_MGR.GetString(529), AddressOf MenuItem_OnClick))

    Return _root_contextmenu

  End Function

  ''' <summary>
  ''' Create menu item for context menu
  ''' </summary>
  ''' <param name="Tag"></param>
  ''' <param name="Text"></param>
  ''' <param name="MenuItem_OnClick"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CreateMenuItem(Tag As ContextMenuAction, Text As String, MenuItem_OnClick As EventHandler) As MenuItem
    Dim _menu_item As MenuItem

    _menu_item = New MenuItem(Text, MenuItem_OnClick)
    _menu_item.Tag = Tag

    Return _menu_item
  End Function

  ''' <summary>
  ''' Initialize Jackpot Viewers tree view
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub InitializeJackpotViewerTreeView()
    Me.InitTreeView(GLB_NLS_GUI_PLAYER_TRACKING.GetString(8065))
  End Sub

  ''' <summary>
  ''' Get Jackpot config screen data
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GetScreenData_JackpotViewerConfig()

    Me.JackpotViewers.Selected = CType(Me.CurrentUcEdit, uc_jackpot_viewer_config).GetJackpotViewerConfig()

  End Sub ' GetScreenData_JackpotConfig

  ''' <summary>
  ''' Get Jackpot config screen data
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GetScreenData_JackpotViewerConfigTerminals()

    Me.JackpotViewers.Selected = CType(Me.CurrentUcEdit, uc_jackpot_viewer_terminal_selection).GetJackpotViewerConfigTerminal()

  End Sub ' GetScreenData_JackpotConfig

  ''' <summary>
  ''' Check if is jackpot viewer config screen data ok 
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function IsScreenDataOk_JackpotViewerConfig() As Boolean
    Return CType(Me.CurrentUcEdit, uc_jackpot_viewer_config).IsScreenDataOk()
  End Function ' IsScreenDataOk_JackpotViewerConfig

  ''' <summary>
  ''' Check if is jackpot viewer config terminals screen data ok 
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function IsScreenDataOk_JackpotViewerTerminals() As Boolean
    Return CType(Me.CurrentUcEdit, uc_jackpot_viewer_terminal_selection).IsScreenDataOk()
  End Function ' IsScreenDataOk_JackpotViewerTerminals

  ''' <summary>
  ''' Get Jackpot config screen data
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub KeyUp_JackpotViewerConfig(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)

    If CType(Me.CurrentUcEdit, uc_jackpot_viewer_config).ef_viewer_name.ContainsFocus Then
      Me.NodeInEdition.Text = CType(Me.CurrentUcEdit, uc_jackpot_viewer_config).ef_viewer_name.Value

      If (String.IsNullOrEmpty(Me.NodeInEdition.Text)) Then
        Me.NodeInEdition.Text = Me.NewJackpotViewerLabel
      End If

      Me.TreeView.Refresh()
    End If

  End Sub ' KeyUp_JackpotViewerConfig

#Region " OnclickEvents "

  ''' <summary>
  ''' Expand all TreeView
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub OnclickEvent_TreeViewExpandAll()
    If Not Me.TreeView.IsNewNode() Then
      Me.TreeView.ExpandAll()
    End If
  End Sub

  ''' <summary>
  ''' Collapse all TreeView
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub OnclickEvent_TreeViewCollapseAll()
    If Not Me.TreeView.IsNewNode() Then
      Me.TreeView.CollapseAll()
    End If
  End Sub

  ''' <summary>
  ''' Expand all TreeView
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub OnclickEvent_TreeViewRefresh()
    Me.TreeView.Refresh()
  End Sub


  ''' <summary>
  ''' Create new Jackpot
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub OnclickEvent_JackpotViewerNew()

    ' If exist new node in edition...
    If Me.TreeView.IsNewNode() Then
      Return
    End If

    ' Add new TreeView node
    Me.AddNewTreeViewNode(uc_treeview.DEFAULT_TREEVIEW_NEW_ITEM_ID)

  End Sub ' OnclickEvent_JackpotNew

#End Region

  ''' <summary>
  ''' Set selected Jackpot
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SetSelectedJackpot()

    Dim _jackpot_id As Integer

    If Me.TreeView_CurrentNodeIsCompatible() AndAlso Not Me.TreeView.SelectedNode Is Nothing AndAlso Me.TreeView.SelectedNode.Name <> Me.TreeView.GetRootName() Then
      _jackpot_id = CInt(Me.TreeView.SelectedNode.Name)

      If _jackpot_id > 0 Then
        Me.JackpotViewers.Selected = Me.JackpotViewers.List.GetJackpotViewerById(_jackpot_id)
      End If
    End If

  End Sub ' SetSelectedJackpot


  ''' <summary>
  ''' Add new TreeView node 
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub AddNewTreeViewNode(ByVal JackpotViewerId As Integer)

    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW
    Me.AddNewJackpotViewerNode(JackpotViewerId)

    ' If jackpot Id Not exist in list of Jackpots Viewer: New Jackpot Viewer.
    ' If jackpot Id exist in list of Jackpots Viewer: New cloned Jackpot Viewer.
    Me.BindTreeNodeToEdit(EditSelectionItemType.JACKPOT_VIEWER_CONFIG, Me.JackpotViewers.List.GetJackpotViewerById(JackpotViewerId))

  End Sub ' AddNewTreeViewNode

  ''' <summary>
  ''' Current node is compatible
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function TreeView_CurrentNodeIsCompatible() As Boolean

    ' Check if node in edition is compatible with JackpotsViewer
    Return (Me.NodeInEdition.Tag = EditSelectionItemType.JACKPOT_VIEWER_CONFIG Or _
            Me.NodeInEdition.Tag = EditSelectionItemType.JACKPOT_VIEWER_MONITOR Or _
            Me.NodeInEdition.Tag = EditSelectionItemType.JACKPOT_VIEWER_TERMINAL)

  End Function ' TreeView_CurrentNodeIsCompatible

#End Region

End Class