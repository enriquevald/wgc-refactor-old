'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_handpays
' DESCRIPTION:   Hand Pays History
' AUTHOR:        Raul Cervera
' CREATION DATE: 25-JUN-2010
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 25-JUN-2010  RCI    Initial version
' 22-NOV-2011  RCI    Add percentage values only for total rows
' 04-JUN-2012  JAB    Eliminate top clause of sql query
' 16-DEC-2013  RMS    In TITO Mode virtual account id should be hidden
' 19-FEB-2014  AMF    Fixed Bug WIG-647: Control minimum version
' 28-MAR-2014  ICS    Fixed Bug WIGOSTITO-1177: Incongruence of data in the GUI for the expiration of tickets
' 04-APR-2014  AMF    Added new information from Long Poll 1B
' 03-SEP-2014  QMP    Standardized Cashless and TITO status NLS
' 08-SEP-2014  FJC    HandPays: 'SPECIAL_PROGRESSIVE' should not appear on Grid
' 03-SEP-2014  LEM    Added functionality to show terminal location data.
' 30-SEP-2014  OPC    Fixed Bug WIG-1358: Added column type manual and subtype and modified the type filter.
' 22-OCT-2014  FJC    Add and modify Filter/s; New Radio button for subtotals
'                       � Add handpay types in the actual filter
'                       � Add Filter status handpay control (new filter)
'                       � Add Radiobutton to choose subtotal style (by status; by handpay-type)
'                       � Etc.
' 23-OCT-2014  FJC    Delete ColumnHeader from Status Control
' 05-FEB-2015  FJC    Fixed Bug WIG-2010.
' 10-FEB-2015  FJC    � Fixed bug WIG-1942: HandPays Screen has duplicated handpays.
'                     � WIG-2003: Show disaggregated amount information with retention base.
' 20-FEB-2015  ANM    Fixed bug WIG-2080: Indicates undone in grid if do
' 26-MAR-2015  ANM    Calling GetProviderIdListSelected always returns a query
' 24-APR-2015  DLL    Fixed bug WIG-2242: filter error.
' 08-OCT-2015  CPC    Added columns Reason and Comment, Item 3710, Task 4810
' 14-JAN-2016  JPJ    Bug 8429: TITOTaxWaiver GP is not taken into account
' 25-JAN-2016  FGB    Bug 5482:Pantalla "Hist�rico pagos manuales": Errores varios
'                       . El bot�n "Reset" no aplicaba al grupo Pagado por.
' 29-JAN-2016  GDA    Se modifica query de selecci�n para que la relaci�n
'                     entre Handpays y Account_operation sea directa
'                     a trav�s de hp_operation_id
' 11-FEB-2016  FAV    Fixed Bug 9269: Error in records returned when select 'Expired' status.
' 11-FEB-2016  FAV    Fixed Bug 9269: Error in records returned when select 'Expired' status.
' 04-MAR-2016  RGR    Task 10297:7402:SAS20: Hanpay manual input Disputes: Add comments.
' 18-OCT-2016  RGR    Bug 12561: GUI - History Payments Manuals - error in the filter type
' 18-OCT-2016  RGR    Bug 12561: GUI - History Payments Manuals - error in the filter type
' 16-JAN-2016  JMM    PBI 19744: Pago autom�tico de handpays
' 07-DEC-2017  FGB    PBI 31058: WIGOS-6802: AGG - Handpay report (multisite adaption)
' 15-GEN-2018  XGJ    The grid must can be sorted is the AGG feature is enabled
' 22-JAN-2018  LTC    PBI 31301:WIGOS-7865 AGG - Handpay form add filter "payment greater than" 
' 21-FEB-2018  RGR    Bug 31635: WIGOS-3785 Incorrect warning message when Several is selected in selection criterion
' 05-JUL-2018  FOS    Bug 33508:WIGOS-13431 [Ticket #15449] error informe de pagos manuales
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports WSI.Common
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Text
Imports System.Xml

Public Class frm_handpays
  Inherits frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If

    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector
  Friend WithEvents chk_total_day As System.Windows.Forms.CheckBox
  Friend WithEvents chk_details As System.Windows.Forms.CheckBox
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents opt_all_status As System.Windows.Forms.RadioButton
  Friend WithEvents opt_several_status As System.Windows.Forms.RadioButton
  Friend WithEvents dg_filter_status As GUI_Controls.uc_grid
  Friend WithEvents gb_subtotal As System.Windows.Forms.GroupBox
  Friend WithEvents gb_details As System.Windows.Forms.GroupBox
  Friend WithEvents opt_subtotal_type As System.Windows.Forms.RadioButton
  Friend WithEvents opt_subtotal_status As System.Windows.Forms.RadioButton
  Friend WithEvents uc_checked_list_handtypes As GUI_Controls.uc_checked_list
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents gb_user As System.Windows.Forms.GroupBox
  Friend WithEvents chk_show_all As System.Windows.Forms.CheckBox
  Friend WithEvents opt_one_user As System.Windows.Forms.RadioButton
  Friend WithEvents opt_all_users As System.Windows.Forms.RadioButton
  Friend WithEvents uc_site_select As GUI_Controls.uc_sites_sel
  Friend WithEvents gb_mb_payment As System.Windows.Forms.GroupBox
  Friend WithEvents ef_mb_amount_limit As GUI_Controls.uc_entry_field
  Friend WithEvents cmb_user As GUI_Controls.uc_combo

  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_handpays))
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector()
    Me.uc_pr_list = New GUI_Controls.uc_provider()
    Me.chk_total_day = New System.Windows.Forms.CheckBox()
    Me.chk_details = New System.Windows.Forms.CheckBox()
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox()
    Me.gb_status = New System.Windows.Forms.GroupBox()
    Me.opt_all_status = New System.Windows.Forms.RadioButton()
    Me.opt_several_status = New System.Windows.Forms.RadioButton()
    Me.dg_filter_status = New GUI_Controls.uc_grid()
    Me.gb_subtotal = New System.Windows.Forms.GroupBox()
    Me.opt_subtotal_type = New System.Windows.Forms.RadioButton()
    Me.opt_subtotal_status = New System.Windows.Forms.RadioButton()
    Me.gb_details = New System.Windows.Forms.GroupBox()
    Me.uc_checked_list_handtypes = New GUI_Controls.uc_checked_list()
    Me.gb_user = New System.Windows.Forms.GroupBox()
    Me.chk_show_all = New System.Windows.Forms.CheckBox()
    Me.opt_one_user = New System.Windows.Forms.RadioButton()
    Me.opt_all_users = New System.Windows.Forms.RadioButton()
    Me.cmb_user = New GUI_Controls.uc_combo()
    Me.uc_site_select = New GUI_Controls.uc_sites_sel()
    Me.ef_mb_amount_limit = New GUI_Controls.uc_entry_field()
    Me.gb_mb_payment = New System.Windows.Forms.GroupBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_status.SuspendLayout()
    Me.gb_subtotal.SuspendLayout()
    Me.gb_details.SuspendLayout()
    Me.gb_user.SuspendLayout()
    Me.gb_mb_payment.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_mb_payment)
    Me.panel_filter.Controls.Add(Me.uc_site_select)
    Me.panel_filter.Controls.Add(Me.uc_checked_list_handtypes)
    Me.panel_filter.Controls.Add(Me.gb_details)
    Me.panel_filter.Controls.Add(Me.gb_subtotal)
    Me.panel_filter.Controls.Add(Me.gb_status)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Controls.Add(Me.uc_dsl)
    Me.panel_filter.Controls.Add(Me.gb_user)
    Me.panel_filter.Size = New System.Drawing.Size(1179, 269)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_user, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dsl, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_status, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_subtotal, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_details, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_checked_list_handtypes, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_site_select, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_mb_payment, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 273)
    Me.panel_data.Size = New System.Drawing.Size(1179, 436)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1173, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1173, 4)
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.ClosingTimeEnabled = True
    Me.uc_dsl.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.FromDateText = "#5339"
    Me.uc_dsl.Location = New System.Drawing.Point(7, 8)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.ShowBorder = True
    Me.uc_dsl.Size = New System.Drawing.Size(247, 106)
    Me.uc_dsl.TabIndex = 0
    Me.uc_dsl.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    Me.uc_dsl.ToDateText = "#5340"
    '
    'uc_pr_list
    '
    Me.uc_pr_list.Location = New System.Drawing.Point(260, 5)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(325, 188)
    Me.uc_pr_list.TabIndex = 2
    Me.uc_pr_list.TerminalListHasValues = False
    '
    'chk_total_day
    '
    Me.chk_total_day.AutoSize = True
    Me.chk_total_day.Location = New System.Drawing.Point(7, 38)
    Me.chk_total_day.Name = "chk_total_day"
    Me.chk_total_day.Size = New System.Drawing.Size(89, 17)
    Me.chk_total_day.TabIndex = 1
    Me.chk_total_day.Text = "xDailyTotal"
    Me.chk_total_day.UseVisualStyleBackColor = True
    '
    'chk_details
    '
    Me.chk_details.AutoSize = True
    Me.chk_details.Location = New System.Drawing.Point(7, 19)
    Me.chk_details.Name = "chk_details"
    Me.chk_details.Size = New System.Drawing.Size(72, 17)
    Me.chk_details.TabIndex = 0
    Me.chk_details.Text = "xDetails"
    Me.chk_details.UseVisualStyleBackColor = True
    '
    'chk_terminal_location
    '
    Me.chk_terminal_location.Location = New System.Drawing.Point(7, 57)
    Me.chk_terminal_location.Name = "chk_terminal_location"
    Me.chk_terminal_location.Size = New System.Drawing.Size(204, 17)
    Me.chk_terminal_location.TabIndex = 2
    Me.chk_terminal_location.Text = "xShow terminals location"
    '
    'gb_status
    '
    Me.gb_status.Controls.Add(Me.opt_all_status)
    Me.gb_status.Controls.Add(Me.opt_several_status)
    Me.gb_status.Controls.Add(Me.dg_filter_status)
    Me.gb_status.Location = New System.Drawing.Point(876, 8)
    Me.gb_status.Name = "gb_status"
    Me.gb_status.Size = New System.Drawing.Size(206, 108)
    Me.gb_status.TabIndex = 5
    Me.gb_status.TabStop = False
    Me.gb_status.Text = "xMov. Status"
    '
    'opt_all_status
    '
    Me.opt_all_status.AutoSize = True
    Me.opt_all_status.Location = New System.Drawing.Point(8, 42)
    Me.opt_all_status.Name = "opt_all_status"
    Me.opt_all_status.Size = New System.Drawing.Size(46, 17)
    Me.opt_all_status.TabIndex = 2
    Me.opt_all_status.Text = "xAll"
    '
    'opt_several_status
    '
    Me.opt_several_status.AutoSize = True
    Me.opt_several_status.Location = New System.Drawing.Point(8, 17)
    Me.opt_several_status.Name = "opt_several_status"
    Me.opt_several_status.Size = New System.Drawing.Size(76, 17)
    Me.opt_several_status.TabIndex = 0
    Me.opt_several_status.Text = "xSeveral"
    '
    'dg_filter_status
    '
    Me.dg_filter_status.CurrentCol = -1
    Me.dg_filter_status.CurrentRow = -1
    Me.dg_filter_status.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_filter_status.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_filter_status.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_filter_status.Location = New System.Drawing.Point(73, 16)
    Me.dg_filter_status.Name = "dg_filter_status"
    Me.dg_filter_status.PanelRightVisible = False
    Me.dg_filter_status.Redraw = True
    Me.dg_filter_status.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_filter_status.Size = New System.Drawing.Size(128, 84)
    Me.dg_filter_status.Sortable = False
    Me.dg_filter_status.SortAscending = True
    Me.dg_filter_status.SortByCol = 0
    Me.dg_filter_status.TabIndex = 1
    Me.dg_filter_status.ToolTipped = True
    Me.dg_filter_status.TopRow = -2
    Me.dg_filter_status.WordWrap = False
    '
    'gb_subtotal
    '
    Me.gb_subtotal.Controls.Add(Me.opt_subtotal_type)
    Me.gb_subtotal.Controls.Add(Me.opt_subtotal_status)
    Me.gb_subtotal.Location = New System.Drawing.Point(876, 122)
    Me.gb_subtotal.Name = "gb_subtotal"
    Me.gb_subtotal.Size = New System.Drawing.Size(206, 88)
    Me.gb_subtotal.TabIndex = 6
    Me.gb_subtotal.TabStop = False
    Me.gb_subtotal.Text = "x_GroupBy"
    '
    'opt_subtotal_type
    '
    Me.opt_subtotal_type.AutoSize = True
    Me.opt_subtotal_type.Location = New System.Drawing.Point(26, 56)
    Me.opt_subtotal_type.Name = "opt_subtotal_type"
    Me.opt_subtotal_type.Size = New System.Drawing.Size(66, 17)
    Me.opt_subtotal_type.TabIndex = 1
    Me.opt_subtotal_type.TabStop = True
    Me.opt_subtotal_type.Text = "x_Type"
    Me.opt_subtotal_type.UseVisualStyleBackColor = True
    '
    'opt_subtotal_status
    '
    Me.opt_subtotal_status.AutoSize = True
    Me.opt_subtotal_status.Location = New System.Drawing.Point(26, 28)
    Me.opt_subtotal_status.Name = "opt_subtotal_status"
    Me.opt_subtotal_status.Size = New System.Drawing.Size(75, 17)
    Me.opt_subtotal_status.TabIndex = 0
    Me.opt_subtotal_status.TabStop = True
    Me.opt_subtotal_status.Text = "x_Status"
    Me.opt_subtotal_status.UseVisualStyleBackColor = True
    '
    'gb_details
    '
    Me.gb_details.Controls.Add(Me.chk_details)
    Me.gb_details.Controls.Add(Me.chk_total_day)
    Me.gb_details.Controls.Add(Me.chk_terminal_location)
    Me.gb_details.Location = New System.Drawing.Point(7, 122)
    Me.gb_details.Name = "gb_details"
    Me.gb_details.Size = New System.Drawing.Size(247, 143)
    Me.gb_details.TabIndex = 1
    Me.gb_details.TabStop = False
    Me.gb_details.Text = "xOptions"
    '
    'uc_checked_list_handtypes
    '
    Me.uc_checked_list_handtypes.GroupBoxText = "xCheckedList"
    Me.uc_checked_list_handtypes.Location = New System.Drawing.Point(592, 8)
    Me.uc_checked_list_handtypes.m_resize_width = 278
    Me.uc_checked_list_handtypes.multiChoice = True
    Me.uc_checked_list_handtypes.Name = "uc_checked_list_handtypes"
    Me.uc_checked_list_handtypes.SelectedIndexes = New Integer(-1) {}
    Me.uc_checked_list_handtypes.SelectedIndexesList = ""
    Me.uc_checked_list_handtypes.SelectedIndexesListLevel2 = ""
    Me.uc_checked_list_handtypes.SelectedValuesArray = New String(-1) {}
    Me.uc_checked_list_handtypes.SelectedValuesList = ""
    Me.uc_checked_list_handtypes.SelectedValuesListLevel2 = ""
    Me.uc_checked_list_handtypes.SetLevels = 2
    Me.uc_checked_list_handtypes.Size = New System.Drawing.Size(278, 202)
    Me.uc_checked_list_handtypes.TabIndex = 4
    Me.uc_checked_list_handtypes.ValuesArray = New String(-1) {}
    '
    'gb_user
    '
    Me.gb_user.Controls.Add(Me.chk_show_all)
    Me.gb_user.Controls.Add(Me.opt_one_user)
    Me.gb_user.Controls.Add(Me.opt_all_users)
    Me.gb_user.Controls.Add(Me.cmb_user)
    Me.gb_user.Location = New System.Drawing.Point(263, 187)
    Me.gb_user.Name = "gb_user"
    Me.gb_user.Size = New System.Drawing.Size(272, 78)
    Me.gb_user.TabIndex = 3
    Me.gb_user.TabStop = False
    Me.gb_user.Text = "xUser"
    '
    'chk_show_all
    '
    Me.chk_show_all.AutoSize = True
    Me.chk_show_all.Location = New System.Drawing.Point(81, 45)
    Me.chk_show_all.Name = "chk_show_all"
    Me.chk_show_all.Size = New System.Drawing.Size(102, 17)
    Me.chk_show_all.TabIndex = 3
    Me.chk_show_all.Text = "chk_show_all"
    Me.chk_show_all.UseVisualStyleBackColor = True
    '
    'opt_one_user
    '
    Me.opt_one_user.Location = New System.Drawing.Point(8, 14)
    Me.opt_one_user.Name = "opt_one_user"
    Me.opt_one_user.Size = New System.Drawing.Size(72, 24)
    Me.opt_one_user.TabIndex = 0
    Me.opt_one_user.Text = "xOne"
    '
    'opt_all_users
    '
    Me.opt_all_users.Location = New System.Drawing.Point(8, 40)
    Me.opt_all_users.Name = "opt_all_users"
    Me.opt_all_users.Size = New System.Drawing.Size(64, 24)
    Me.opt_all_users.TabIndex = 2
    Me.opt_all_users.Text = "xAll"
    '
    'cmb_user
    '
    Me.cmb_user.AllowUnlistedValues = False
    Me.cmb_user.AutoCompleteMode = False
    Me.cmb_user.IsReadOnly = False
    Me.cmb_user.Location = New System.Drawing.Point(78, 14)
    Me.cmb_user.Name = "cmb_user"
    Me.cmb_user.SelectedIndex = -1
    Me.cmb_user.Size = New System.Drawing.Size(184, 24)
    Me.cmb_user.SufixText = "Sufix Text"
    Me.cmb_user.SufixTextVisible = True
    Me.cmb_user.TabIndex = 1
    Me.cmb_user.TextCombo = Nothing
    Me.cmb_user.TextVisible = False
    Me.cmb_user.TextWidth = 0
    '
    'uc_site_select
    '
    Me.uc_site_select.Location = New System.Drawing.Point(8, 209)
    Me.uc_site_select.MultiSelect = False
    Me.uc_site_select.Name = "uc_site_select"
    Me.uc_site_select.ShowIsoCode = False
    Me.uc_site_select.ShowMultisiteRow = False
    Me.uc_site_select.Size = New System.Drawing.Size(247, 137)
    Me.uc_site_select.TabIndex = 11
    '
    'ef_mb_amount_limit
    '
    Me.ef_mb_amount_limit.DoubleValue = 0.0R
    Me.ef_mb_amount_limit.IntegerValue = 0
    Me.ef_mb_amount_limit.IsReadOnly = False
    Me.ef_mb_amount_limit.Location = New System.Drawing.Point(7, 15)
    Me.ef_mb_amount_limit.Name = "ef_mb_amount_limit"
    Me.ef_mb_amount_limit.PlaceHolder = Nothing
    Me.ef_mb_amount_limit.ShortcutsEnabled = True
    Me.ef_mb_amount_limit.Size = New System.Drawing.Size(265, 25)
    Me.ef_mb_amount_limit.SufixText = "Sufix Text"
    Me.ef_mb_amount_limit.SufixTextVisible = True
    Me.ef_mb_amount_limit.TabIndex = 3
    Me.ef_mb_amount_limit.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_mb_amount_limit.TextValue = ""
    Me.ef_mb_amount_limit.TextWidth = 130
    Me.ef_mb_amount_limit.Value = ""
    Me.ef_mb_amount_limit.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_mb_payment
    '
    Me.gb_mb_payment.Controls.Add(Me.ef_mb_amount_limit)
    Me.gb_mb_payment.Location = New System.Drawing.Point(592, 216)
    Me.gb_mb_payment.Name = "gb_mb_payment"
    Me.gb_mb_payment.Size = New System.Drawing.Size(278, 49)
    Me.gb_mb_payment.TabIndex = 12
    Me.gb_mb_payment.TabStop = False
    Me.gb_mb_payment.Text = "GroupBox1"
    '
    'frm_handpays
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(1187, 713)
    Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
    Me.Name = "frm_handpays"
    Me.Text = "frm_handpays"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_status.ResumeLayout(False)
    Me.gb_status.PerformLayout()
    Me.gb_subtotal.ResumeLayout(False)
    Me.gb_subtotal.PerformLayout()
    Me.gb_details.ResumeLayout(False)
    Me.gb_details.PerformLayout()
    Me.gb_user.ResumeLayout(False)
    Me.gb_user.PerformLayout()
    Me.gb_mb_payment.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Enums "

  Private Enum ENUM_GRID_ROW_STATUS
    DETAILS = 1
    '
    DAILY_PAID_TOTAL = 2
    DAILY_PENDING_TOTAL = 3
    DAILY_EXPIRED_TOTAL = 4

    DAILY_AUTHORIZED_TOTAL = 5
    DAILY_VOIDED_TOTAL = 6
    DAILY_TOTAL = 7
    '
    PAID_TOTAL = 8
    PENDING_TOTAL = 9
    EXPIRED_TOTAL = 10
    '
    AUTHORIZED_TOTAL = 11
    VOIDED_TOTAL = 12
    '
    TOTAL = 13
  End Enum   ' ENUM_GRID_ROW_STATUS

  Private Enum ENUM_GRID_ROW_TYPE
    DETAILS = 1
    DAILY_CANCELLED_CREDITS_TOTAL = 2             'Canceled Credits
    DAILY_ORPHAN_CREDITS_TOTAL = 3                'Orphan Credits
    DAILY_JACKPOT_TOTAL = 4                       'JackPot
    DAILY_JACKPOT_SITE_TOTAL = 5                  'Site JackPot
    DAILY_JACKPOT_PROGRESSIVE_TOTAL = 6           'JackPot Progressive
    DAILY_JACKPOT_MAJOR_PRIZE_TOTAL = 7           'JackPot Not Progressive (Premio Mayor)

    DAILY_MANUAL_TOTAL = 8                        'Manual
    DAILY_MANUAL_CANCELLED_CREDITS_TOTAL = 9      'MANUAL Canceled credits
    DAILY_MANUAL_JACKPOT_TOTAL = 10               'MANUAL Jackpot
    DAILY_MANUAL_JACKPOT_PROGRESSIVE_TOTAL = 11   'MANUAL Progressive Jackpot
    DAILY_MANUAL_JACKPOT_MAJOR_PRIZE_TOTAL = 12   'MANUAL JackPot MAJOR_PRIZE
    DAILY_MANUAL_OTHERS_TOTAL = 13                'MANUAL Others
    DAILY_TOTAL = 14
    '
    CANCELLED_CREDITS_TOTAL = 15                  'Canceled Credits
    ORPHAN_CREDITS_TOTAL = 16                     'Orphan Credits
    JACKPOT_TOTAL = 17                            'JackPot
    JACKPOT_SITE_TOTAL = 18                       'Site JackPot
    JACKPOT_PROGRESSIVE_TOTAL = 19                'JackPot Progressive
    JACKPOT_MAJOR_PRIZE_TOTAL = 20                'JackPot MAJOR_PRIZE
    '
    MANUAL_TOTAL = 21                             'Manual
    MANUAL_CANCELLED_CREDITS_TOTAL = 22           'MANUAL Canceled credits
    MANUAL_JACKPOT_TOTAL = 23                     'MANUAL Jackpot
    MANUAL_JACKPOT_PROGRESSIVE_TOTAL = 24         'MANUAL Progressive Jackpot
    MANUAL_JACKPOT_MAJOR_PRIZE_TOTAL = 25         'MANUAL JackPot MAJOR_PRIZE
    MANUAL_OTHERS_TOTAL = 26                      'MANUAL Others

    TOTAL = 27
  End Enum     ' ENUM_GRID_ROW_TYPE

  Public Enum ENUM_HP_LEVEL_JACKPOT
    HP_JACKPOT_LEVEL_NOTHING = 0
    HP_JACKPOT_PROGRESSIVE_LEVEL_START = 1
    HP_JACKPOT_PROGRESSIVE_LEVEL_END = 32
    HP_JACKPOT_MAJOR_PRIZE = 64
  End Enum  ' ENUM_HP_LEVEL_JACKPOT

  Public Enum ENUM_HP_JACKPOT_TRANSLATE
    HP_JACKPOT = 0
    HP_JACKPOT_PROGRESSIVE = 10000
    HP_JACKPOT_MAJOR_PRIZE = 10001
  End Enum  ' ENUM_HP_JACKPOT_PROGRESSIVE_TRANSLATE

#End Region ' Enums

#Region " Constants "
  Private TERMINAL_DATA_COLUMNS As Int32

  ' DB Columns
  Private Const SQL_COLUMN_SITE_ID As Integer = 0
  Private Const SQL_COLUMN_DATETIME As Integer = 1
  Private Const SQL_COLUMN_PROVIDER As Integer = 2
  Private Const SQL_COLUMN_TERMINAL As Integer = 3
  Private Const SQL_COLUMN_TYPE As Integer = 4
  Private Const SQL_COLUMN_AMOUNT As Integer = 5

  Private Const SQL_COLUMN_TAX_BASE_AMOUNT As Integer = 6  ' Base
  Private Const SQL_COLUMN_REST_AMOUNT As Integer = 7      ' Resto
  Private Const SQL_COLUMN_TAX_AMOUNT As Integer = 8       ' Retenciones
  Private Const SQL_COLUMN_PAID_AMOUNT As Integer = 9      ' Pagado

  Private Const SQL_COLUMN_PROVIDER_ACTUAL As Integer = 10
  Private Const SQL_COLUMN_TERMINAL_ACTUAL As Integer = 11
  Private Const SQL_COLUMN_CARD_ACCOUNT As Integer = 12
  Private Const SQL_COLUMN_CARD_HOLDER As Integer = 13
  Private Const SQL_COLUMN_CASHIER_NAME As Integer = 14
  Private Const SQL_COLUMN_HP_EXPIRED As Integer = 15
  Private Const SQL_COLUMN_ACCOUNT_TYPE As Integer = 16   'field AC_TYPE, not used
  Private Const SQL_COLUMN_LONG_POLL_1B_DATA As Integer = 17
  Private Const SQL_COLUMN_TERMINAL_ID As Integer = 18
  Private Const SQL_COLUMN_STATUS As Integer = 19
  Private Const SQL_COLUMN_LEVEL As Integer = 20
  Private Const SQL_COLUMN_ACCOUNT_MOVS_TYPE As Integer = 21

  Private Const SQL_COLUMN_ACCOUNT_OPS_REASON As Integer = 22
  Private Const SQL_COLUMN_ACCOUNT_OPS_COMMENT As Integer = 23
  Private Const SQL_COLUMN_AMT0 As Integer = 24
  Private Const SQL_COLUMN_CUR0 As Integer = 25

  Private Const SQL_COLUMN_ACCOUNT_HP_COMMENT As Integer = 26

  ' Grid Columns
  Private GRID_COLUMN_INDEX As Integer
  Private GRID_COLUMN_SITE_ID As Integer
  Private GRID_COLUMN_DATETIME As Integer
  Private GRID_INIT_TERMINAL_DATA As Integer
  Private GRID_COLUMN_PROVIDER As Integer
  Private GRID_COLUMN_TERMINAL As Integer
  Private GRID_COLUMN_AMT0 As Integer
  Private GRID_COLUMN_AMOUNT As Integer
  Private GRID_COLUMN_PERCENT As Integer

  Private GRID_COLUMN_TAX_BASE_AMOUNT As Integer
  Private GRID_COLUMN_REST_AMOUNT As Integer
  Private GRID_COLUMN_TAX_AMOUNT As Integer
  Private GRID_COLUMN_PAID_AMOUNT As Integer

  Private GRID_COLUMN_TYPE As Integer
  Private GRID_COLUMN_GROUP As Integer
  Private GRID_COLUMN_LEVEL As Integer
  Private GRID_COLUMN_STATUS As Integer
  '  Necessary for ToolTip Text
  Private GRID_COLUMN_PROVIDER_ACTUAL As Integer
  Private GRID_COLUMN_TERMINAL_ACTUAL As Integer
  Private GRID_COLUMN_ROW_TYPE As Integer

  Private GRID_COLUMN_REASON As Integer
  Private GRID_COLUMN_COMMENT As Integer

  Private GRID_COLUMN_HP_COMMENT As Integer

  Private GRID_COLUMNS As Integer
  Private Const GRID_HEADER_ROWS As Integer = 2
  Private Const GRID_2_TAB As String = "    "

  ' Width
  Private Const GRID_WIDTH_INDEX As Integer = 200
  Private Const GRID_WIDTH_SITE_ID As Integer = 850
  Private Const GRID_WIDTH_DATE As Integer = 2000
  Private Const GRID_WIDTH_PROVIDER_STATUS As Integer = 2000  'Not used
  Private Const GRID_WIDTH_PROVIDER_TYPE As Integer = 3500    'Only subtotal by type
  Private Const GRID_WIDTH_TERMINAL As Integer = 2400
  Private Const GRID_WIDTH_AMOUNT_LONG As Integer = 1400
  Private Const GRID_WIDTH_STATUS As Integer = 5150
  Private Const GRID_WIDTH_TYPE As Integer = 2800
  Private Const GRID_WIDTH_GROUP As Integer = 700
  Private Const GRID_WIDTH_LEVEL As Integer = 700
  Private Const GRID_WIDTH_PERCENT As Integer = 850

  Private Const GRID_WIDTH_INVISIBLE As Integer = 0

  Private Const GRID_WIDTH_COLUMN_FILTER_DESCRIPTION_STATUS As Integer = 1425
  Private Const GRID_WIDTH_COLUMN_FILTER_DESCRIPTION_TYPE As Integer = 3225

  Private Const GRID_WIDTH_REASON As Integer = 2800
  Private Const GRID_WIDTH_COMMENT As Integer = 5150

  Private Const GRID_WIDTH_HP_COMMENT As Integer = 5150

  ' Types data grid
  Private Const GRID_2_COLUMN_CODE As Integer = 0
  Private Const GRID_2_COLUMN_CHECKED As Integer = 1
  Private Const GRID_2_COLUMN_DESC As Integer = 2
  Private Const GRID_2_COLUMN_CODE_HANDPAY_STATUS As Integer = 3        'Hidden
  Private Const GRID_2_COLUMN_CODE_HANDPAY_MASK_STATUS As Integer = 4   'Hidden

  Private Const GRID_2_COLUMNS As Integer = 5
  Private Const GRID_2_HEADER_ROWS As Integer = 0

  Private Const FORM_DB_MIN_VERSION As Short = 201

  Private Const HANDPAYS_NODE = "Handpay"
  Private Const PROGRESSIVE_GROUP_NODE = "ProgressiveGroup"
  Private Const LEVEL_NODE = "Level"

  ' Excel Columns
  Private Const EXCEL_COLUMN_GROUP As Integer = 6
  Private Const EXCEL_COLUMN_LEVEL As Integer = 7

#End Region ' Constants

#Region " Structures "

  Private Structure TYPE_TOTAL_COUNTERS_STATUS
    Dim day_status As Date

    'Paid
    Dim paid_amount As Decimal
    Dim paid_base_amount As Decimal
    Dim paid_rest_amount As Decimal
    Dim paid_retention_amount As Decimal
    Dim paid_paid_amount As Decimal
    Dim paid_count As Integer

    'Pending
    Dim pending_amount As Decimal
    Dim pending_base_amount As Decimal
    Dim pending_rest_amount As Decimal
    Dim pending_retention_amount As Decimal
    Dim pending_paid_amount As Decimal
    Dim pending_count As Integer

    'Expired
    Dim expired_amount As Decimal
    Dim expired_base_amount As Decimal
    Dim expired_rest_amount As Decimal
    Dim expired_retention_amount As Decimal
    Dim expired_paid_amount As Decimal
    Dim expired_count As Integer

    'Authorized
    Dim authorized_amount As Decimal
    Dim authorized_base_amount As Decimal
    Dim authorized_rest_amount As Decimal
    Dim authorized_retention_amount As Decimal
    Dim authorized_paid_amount As Decimal
    Dim authorized_count As Integer

    'Voided (anulado)
    Dim voided_amount As Decimal
    Dim voided_base_amount As Decimal
    Dim voided_rest_amount As Decimal
    Dim voided_retention_amount As Decimal
    Dim voided_paid_amount As Decimal
    Dim voided_count As Integer

    'Totals
    Dim total_amount_status As Decimal
    Dim total_count_status As Integer
    Dim total_base_amount_status As Decimal
    Dim total_rest_amount_status As Decimal
    Dim total_retention_amount_status As Decimal
    Dim total_paid_amount_status As Decimal

  End Structure

  Private Structure TYPE_TOTAL_COUNTERS_TYPE
    Dim day_type As Date

    '*** NOT MANUAL
    ' Cancelled Credits
    Dim cancelled_credits_amount As Decimal
    Dim cancelled_credits_base_amount As Decimal
    Dim cancelled_credits_rest_amount As Decimal
    Dim cancelled_credits_retention_amount As Decimal
    Dim cancelled_credits_paid_amount As Decimal
    Dim cancelled_credits_count As Integer

    ' JackPot
    Dim jackpot_amount As Decimal
    Dim jackpot_base_amount As Decimal
    Dim jackpot_rest_amount As Decimal
    Dim jackpot_retention_amount As Decimal
    Dim jackpot_paid_amount As Decimal
    Dim jackpot_count As Integer

    ' Progressive JackPot
    Dim jackpot_progressive_amount As Decimal
    Dim jackpot_progressive_base_amount As Decimal
    Dim jackpot_progressive_rest_amount As Decimal
    Dim jackpot_progressive_retention_amount As Decimal
    Dim jackpot_progressive_paid_amount As Decimal
    Dim jackpot_progressive_count As Integer

    ' Site Jackpot
    Dim site_jackpot_amount As Decimal
    Dim site_jackpot_base_amount As Decimal
    Dim site_jackpot_rest_amount As Decimal
    Dim site_jackpot_retention_amount As Decimal
    Dim site_jackpot_paid_amount As Decimal
    Dim site_jackpot_count As Integer

    ' Orphan Credits
    Dim orphan_credits_amount As Decimal
    Dim orphan_credits_base_amount As Decimal
    Dim orphan_credits_rest_amount As Decimal
    Dim orphan_credits_retention_amount As Decimal
    Dim orphan_credits_paid_amount As Decimal
    Dim orphan_credits_count As Integer

    ' JackPot Top Win
    Dim jackpot_major_prize_amount As Decimal
    Dim jackpot_major_prize_base_amount As Decimal
    Dim jackpot_major_prize_rest_amount As Decimal
    Dim jackpot_major_prize_retention_amount As Decimal
    Dim jackpot_major_prize_paid_amount As Decimal
    Dim jackpot_major_prize_count As Decimal

    '*** MANUAL
    ' Manual
    Dim manual_amount As Decimal
    Dim manual_base_amount As Decimal
    Dim manual_rest_amount As Decimal
    Dim manual_retention_amount As Decimal
    Dim manual_paid_amount As Decimal
    Dim manual_count As Integer

    ' Manual Progressive JackPot
    Dim manual_progressive_jackpot_amount As Decimal
    Dim manual_progressive_jackpot_base_amount As Decimal
    Dim manual_progressive_jackpot_rest_amount As Decimal
    Dim manual_progressive_jackpot_retention_amount As Decimal
    Dim manual_progressive_jackpot_paid_amount As Decimal
    Dim manual_progressive_jackpot_count As Integer

    'Manual
    Dim manual_jackpot_amount As Decimal
    Dim manual_jackpot_base_amount As Decimal
    Dim manual_jackpot_rest_amount As Decimal
    Dim manual_jackpot_retention_amount As Decimal
    Dim manual_jackpot_paid_amount As Decimal
    Dim manual_jackpot_count As Integer

    ' Manual JackPot Top Win
    Dim manual_jackpot_major_prize_amount As Decimal
    Dim manual_jackpot_major_prize_base_amount As Decimal
    Dim manual_jackpot_major_prize_rest_amount As Decimal
    Dim manual_jackpot_major_prize_retention_amount As Decimal
    Dim manual_jackpot_major_prize_paid_amount As Decimal
    Dim manual_jackpot_major_prize_count As Decimal

    'Manual Cancelled Credits
    Dim manual_cancelled_credits_amount As Decimal
    Dim manual_cancelled_credits_base_amount As Decimal
    Dim manual_cancelled_credits_rest_amount As Decimal
    Dim manual_cancelled_credits_retention_amount As Decimal
    Dim manual_cancelled_credits_paid_amount As Decimal
    Dim manual_cancelled_credits_count As Integer

    'Manual Others
    Dim manual_others_amount As Decimal
    Dim manual_others_base_amount As Decimal
    Dim manual_others_rest_amount As Decimal
    Dim manual_others_retention_amount As Decimal
    Dim manual_others_paid_amount As Decimal
    Dim manual_others_count As Integer

    'Total
    Dim total_amount_type As Decimal
    Dim total_base_amount_type As Decimal
    Dim total_rest_amount_type As Decimal
    Dim total_retention_amount_type As Decimal
    Dim total_paid_amount_type As Decimal
    Dim total_count_type As Integer
  End Structure

  Public Structure TYPE_DG_FILTER_ELEMENT
    Dim code As String                                        'ID or ID's
    Dim code_handpay_status As WSI.Common.HANDPAY_STATUS      'HandPay_status
    Dim code_mask_status As WSI.Common.HANDPAY_STATUS         'Mask_Status
    Dim description As String                                 'Description
    Dim checked_by_default As Boolean                         'Checked by default
  End Structure

#End Region ' Structures

#Region " Members "
  ' For report filters 
  Private m_date_from As String
  Private m_date_to As String
  Private m_terminals As String
  Private m_show_details As String
  Private m_show_totals As String
  Private m_show_terminal_location As String
  Private m_types As String
  Private m_status As String
  Private m_paid_by As String
  Private m_subtotal As String
  Private m_sites As String

  ' For totals
  Private m_first_time_check_day As Boolean
  Private m_daily_total_status As TYPE_TOTAL_COUNTERS_STATUS
  Private m_global_total_status As TYPE_TOTAL_COUNTERS_STATUS
  Private m_daily_total_type As TYPE_TOTAL_COUNTERS_TYPE
  Private m_global_total_type As TYPE_TOTAL_COUNTERS_TYPE
  Private m_terminal_report_type As ReportType = ReportType.Provider
  Private m_time_period As Integer = 0

  ' For searchs
  Private m_id_status_find As HANDPAY_STATUS = HANDPAY_STATUS.PENDING
  Private m_id_type_range_hp_level_find_start As ENUM_HP_LEVEL_JACKPOT = ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_LEVEL_NOTHING
  Private m_id_type_range_hp_level_find_end As ENUM_HP_LEVEL_JACKPOT = ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_LEVEL_NOTHING

  ' Is Tito Mode
  Private m_is_tito_mode As Boolean

  ' Is multisite center
  Private m_is_multisite_center As Boolean
#End Region ' Members

#Region " Overrides "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_HANDPAYS

    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)

    Call MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Is Tito Mode
    m_is_tito_mode = WSI.Common.TITO.Utils.IsTitoMode

    ' Is multisite center
    m_is_multisite_center = WSI.Common.GeneralParam.GetBoolean("MultiSite", "IsCenter", False)

    Me.Text = GLB_NLS_GUI_AUDITOR.GetString(371)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    ' Date time filter
    Me.uc_dsl.Init(GLB_NLS_GUI_INVOICING.Id(201))

    ' Game Providers - Terminals
    Dim _terminal_types() As Integer = Nothing
    ReDim Preserve _terminal_types(0 To 3)
    _terminal_types(0) = WSI.Common.TerminalTypes.WIN
    _terminal_types(1) = WSI.Common.TerminalTypes.T3GS
    _terminal_types(2) = WSI.Common.TerminalTypes.SAS_HOST
    _terminal_types(3) = WSI.Common.TerminalTypes.SITE
    Me.uc_pr_list.FilterByCurrency = False

    Call Me.uc_pr_list.Init(_terminal_types, , , m_is_multisite_center)

    ' Sites
    Me.uc_site_select.Visible = m_is_multisite_center
    If (uc_site_select.Visible) Then
      ' Sites
      Me.uc_site_select.ShowMultisiteRow = False
      Me.uc_site_select.MultiSelect = True
      Me.uc_site_select.Init()
      Me.uc_site_select.SetDefaultValues(False)
      Me.uc_site_select.Location = uc_pr_list.Location 'Set sites on the same spot that terminals
    End If

    Me.chk_details.Text = GLB_NLS_GUI_CONTROLS.GetString(321)
    Me.chk_total_day.Text = GLB_NLS_GUI_INVOICING.GetString(278)

    Me.gb_status.Text = GLB_NLS_GUI_INVOICING.GetString(237)
    Me.opt_several_status.Text = GLB_NLS_GUI_INVOICING.GetString(223)
    Me.opt_all_status.Text = GLB_NLS_GUI_INVOICING.GetString(224)

    Me.gb_details.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5361)          'Show
    Me.gb_subtotal.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1046)         'Subtotal

    Me.opt_subtotal_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5635) 'By status
    Me.opt_subtotal_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5636)   'By type

    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)

    ' Mov. types grid 
    Me.uc_checked_list_handtypes.GroupBoxText = GLB_NLS_GUI_INVOICING.GetString(314)
    Me.uc_checked_list_handtypes.SetLevels = 2
    Me.uc_checked_list_handtypes.btn_check_all.Width = 115
    Me.uc_checked_list_handtypes.btn_uncheck_all.Width = 115
    Me.uc_checked_list_handtypes.ColumnWidth(3, 1000)

    ' Set the content for user combo
    Call SetCombo(Me.cmb_user, GetSqlUsers())

    ' Users 
    Me.gb_user.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5651)
    Me.opt_one_user.Text = GLB_NLS_GUI_INVOICING.GetString(218)
    Me.opt_all_users.Text = GLB_NLS_GUI_INVOICING.GetString(219)
    Me.chk_show_all.Text = GLB_NLS_GUI_CONFIGURATION.GetString(90)
    Me.opt_all_users.Checked = True

    ' If multisite move controls
    Call InitControlsMultisite()

    ' Style Grid
    Call GUI_StyleSheet()

    ' Mov. types status
    Call GUI_StyleSheetFilter(dg_filter_status, GRID_WIDTH_COLUMN_FILTER_DESCRIPTION_STATUS)

    ' LTC 22-JAN-2018
    ' Payment
    Me.gb_mb_payment.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1578)
    Me.ef_mb_amount_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8989)
    Me.ef_mb_amount_limit.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9)

    ' Set filter default values
    Call SetDefaultValues()
  End Sub ' GUI_InitControls

  ''' <summary>
  ''' Initialize control sizes and position for multisite
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub InitControlsMultisite()
    If (Not m_is_multisite_center) Then
      Return
    End If

    uc_site_select.Location = New Point(gb_details.Location.X, gb_details.Location.Y + 2)
    uc_site_select.Size = gb_details.Size
    uc_site_select.TabIndex = 1
    uc_site_select.ResizeControlsToCurrentSize()

    gb_details.Location = gb_user.Location
    gb_details.Size = New System.Drawing.Size((uc_pr_list.Size.Width - 5), gb_user.Size.Height)
    gb_details.TabIndex = 3

    gb_user.Visible = False
    gb_user.Enabled = gb_user.Visible

    gb_status.Visible = False
    gb_status.Enabled = gb_status.Visible

    gb_subtotal.Visible = False
    gb_subtotal.Enabled = gb_subtotal.Visible
    opt_subtotal_type.Checked = True  'Totals by type (status is not shown)
  End Sub

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()

    m_first_time_check_day = True

    If Me.opt_subtotal_status.Checked Then
      ResetTotalCountersStatus(m_global_total_status, Nothing)
      ResetTotalCountersStatus(m_daily_total_status, Nothing)
    Else
      ResetTotalCountersType(m_global_total_type, Nothing)
      ResetTotalCountersType(m_daily_total_type, Nothing)
    End If

    Call GUI_StyleSheet()

  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    Dim _daily_day As Date

    'ORDER
    'Cancelled Credits
    'Abandonded credits
    'Jackpot
    'JackPot Progressive 
    'Site Jackpot
    'Manual
    'Manual Canceled Credits
    'Manual JackPot 
    'Manual JackPot Progressive 
    'Manual Manual (Manual Others)

    If Me.opt_subtotal_status.Checked Then
      _daily_day = m_daily_total_status.day_status
    Else
      _daily_day = m_daily_total_type.day_type
    End If

    If _daily_day <> Nothing Then
      If Me.chk_total_day.Checked Then
        If Me.opt_subtotal_type.Checked Then
          DrawTotalRowType(ENUM_GRID_ROW_TYPE.DAILY_CANCELLED_CREDITS_TOTAL)
          DrawTotalRowType(ENUM_GRID_ROW_TYPE.DAILY_ORPHAN_CREDITS_TOTAL)
          DrawTotalRowType(ENUM_GRID_ROW_TYPE.DAILY_JACKPOT_TOTAL)
          DrawTotalRowType(ENUM_GRID_ROW_TYPE.DAILY_JACKPOT_PROGRESSIVE_TOTAL)
          DrawTotalRowType(ENUM_GRID_ROW_TYPE.DAILY_JACKPOT_MAJOR_PRIZE_TOTAL)
          DrawTotalRowType(ENUM_GRID_ROW_TYPE.DAILY_JACKPOT_SITE_TOTAL)

          DrawTotalRowType(ENUM_GRID_ROW_TYPE.DAILY_MANUAL_CANCELLED_CREDITS_TOTAL)
          DrawTotalRowType(ENUM_GRID_ROW_TYPE.DAILY_MANUAL_JACKPOT_TOTAL)
          DrawTotalRowType(ENUM_GRID_ROW_TYPE.DAILY_MANUAL_JACKPOT_PROGRESSIVE_TOTAL)
          DrawTotalRowType(ENUM_GRID_ROW_TYPE.DAILY_MANUAL_JACKPOT_MAJOR_PRIZE_TOTAL)
          DrawTotalRowType(ENUM_GRID_ROW_TYPE.DAILY_MANUAL_TOTAL)
          DrawTotalRowType(ENUM_GRID_ROW_TYPE.DAILY_MANUAL_OTHERS_TOTAL)
          '
          DrawTotalRowType(ENUM_GRID_ROW_TYPE.DAILY_TOTAL)
        Else
          ' Draw last daily total row.
          DrawTotalRowStatus(ENUM_GRID_ROW_STATUS.DAILY_VOIDED_TOTAL)
          DrawTotalRowStatus(ENUM_GRID_ROW_STATUS.DAILY_PAID_TOTAL)
          DrawTotalRowStatus(ENUM_GRID_ROW_STATUS.DAILY_AUTHORIZED_TOTAL)
          DrawTotalRowStatus(ENUM_GRID_ROW_STATUS.DAILY_EXPIRED_TOTAL)
          DrawTotalRowStatus(ENUM_GRID_ROW_STATUS.DAILY_PENDING_TOTAL)
          '
          DrawTotalRowStatus(ENUM_GRID_ROW_STATUS.DAILY_TOTAL)
        End If
      End If
    End If

    If Me.opt_subtotal_type.Checked Then
      DrawTotalRowType(ENUM_GRID_ROW_TYPE.CANCELLED_CREDITS_TOTAL)
      DrawTotalRowType(ENUM_GRID_ROW_TYPE.ORPHAN_CREDITS_TOTAL)
      DrawTotalRowType(ENUM_GRID_ROW_TYPE.JACKPOT_TOTAL)
      DrawTotalRowType(ENUM_GRID_ROW_TYPE.JACKPOT_PROGRESSIVE_TOTAL)
      DrawTotalRowType(ENUM_GRID_ROW_TYPE.JACKPOT_MAJOR_PRIZE_TOTAL)
      DrawTotalRowType(ENUM_GRID_ROW_TYPE.JACKPOT_SITE_TOTAL)

      DrawTotalRowType(ENUM_GRID_ROW_TYPE.MANUAL_CANCELLED_CREDITS_TOTAL)
      DrawTotalRowType(ENUM_GRID_ROW_TYPE.MANUAL_JACKPOT_TOTAL)
      DrawTotalRowType(ENUM_GRID_ROW_TYPE.MANUAL_JACKPOT_PROGRESSIVE_TOTAL)
      DrawTotalRowType(ENUM_GRID_ROW_TYPE.MANUAL_JACKPOT_MAJOR_PRIZE_TOTAL)
      DrawTotalRowType(ENUM_GRID_ROW_TYPE.MANUAL_TOTAL)
      DrawTotalRowType(ENUM_GRID_ROW_TYPE.MANUAL_OTHERS_TOTAL)
      '
      DrawTotalRowType(ENUM_GRID_ROW_TYPE.TOTAL)
    Else
      DrawTotalRowStatus(ENUM_GRID_ROW_STATUS.VOIDED_TOTAL)
      DrawTotalRowStatus(ENUM_GRID_ROW_STATUS.PAID_TOTAL)
      DrawTotalRowStatus(ENUM_GRID_ROW_STATUS.AUTHORIZED_TOTAL)
      DrawTotalRowStatus(ENUM_GRID_ROW_STATUS.EXPIRED_TOTAL)
      DrawTotalRowStatus(ENUM_GRID_ROW_STATUS.PENDING_TOTAL)
      '
      DrawTotalRowStatus(ENUM_GRID_ROW_STATUS.TOTAL)
    End If

  End Sub ' GUI_AfterLastRow

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate > Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    End If

    'Check Types
    Call Me.CheckStatusFilter()
   
    ' If Multisite then selected site needed
    If m_is_multisite_center Then
      If Not uc_site_select.FilterCheckSites() Then
        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _sb As StringBuilder
    Dim _nat_curr As String

    _sb = New StringBuilder()
    _nat_curr = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode", "   ")

    'RXM 05-OCT-2012
    m_time_period = WSI.Common.GeneralParam.GetInt32("Cashier.HandPays", "TimePeriod", 60, 5, 2500)

    If (Not m_is_multisite_center) Then
      _sb.AppendLine("   SELECT   0  as  HP_SITE_ID ")
      _sb.AppendLine("          , HP_DATETIME ")
      _sb.AppendLine("          , HP_TE_PROVIDER_ID ")
      _sb.AppendLine("          , HP_TE_NAME ")
      _sb.AppendLine("          , HP_TYPE ")
      _sb.AppendLine("          , HP_AMOUNT ")
      _sb.AppendLine("          , (CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) ")
      _sb.AppendLine("                    THEN HP_AMOUNT                  ")
      _sb.AppendLine("                    ELSE HP_TAX_BASE_AMOUNT         ")
      _sb.AppendLine("             END) AS BASE ")
      _sb.AppendLine("          , (CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) ")
      _sb.AppendLine("                    THEN 0                          ")
      _sb.AppendLine("                    ELSE ISNULL(HP_AMOUNT, 0) - ISNULL(HP_TAX_BASE_AMOUNT, 0) ")
      _sb.AppendLine("             END) AS REMAINDER")
      _sb.AppendLine("          , ISNULL(HP_TAX_AMOUNT, 0)                              AS TAXES")
      _sb.AppendLine("          , ISNULL(HP_AMOUNT, 0) - ISNULL(HP_TAX_AMOUNT, 0)       AS PAID_AMOUNT ")
      _sb.AppendLine("          , TE_PROVIDER_ID ")
      _sb.AppendLine("          , TE_NAME ")
      _sb.AppendLine("          , AC_ACCOUNT_ID ")
      _sb.AppendLine("          , AC_HOLDER_NAME ")
      _sb.AppendLine("          , AM_CASHIER_NAME ")
      _sb.AppendLine("          , (CASE ")
      _sb.AppendLine("               WHEN (TI_STATUS IS NOT NULL AND TI_STATUS = " & TITO_TICKET_STATUS.EXPIRED & ") THEN 1  ")
      _sb.AppendLine("               WHEN (TI_STATUS IS NULL AND HP_STATUS_CALCULATED = " & HANDPAY_STATUS.EXPIRED & ") THEN 1  ")
      _sb.AppendLine("               WHEN (TI_STATUS IS NULL AND DATEADD (MINUTE, " & m_time_period & ", HP_DATETIME) < GETDATE()) THEN 1 ")
      _sb.AppendLine("               ELSE 0 ")
      _sb.AppendLine("             END) AS HP_EXPIRED")
      'AC_TYPE --> not used
      _sb.AppendLine("          , AC_TYPE ")
      _sb.AppendLine("          , HP_LONG_POLL_1B_DATA ")
      _sb.AppendLine("          , HP_TERMINAL_ID ")
      _sb.AppendLine("          , HP_STATUS ")
      _sb.AppendLine("          , HP_LEVEL ")
      _sb.AppendLine("          , AM_TYPE ")
      _sb.AppendLine("          , CAI_NAME AS REASON ")
      _sb.AppendLine("          , AO_COMMENT ")
      _sb.AppendLine("          , ISNULL(HP_AMT0, HP_AMOUNT) AS HP_AMT0 ")
      _sb.AppendLine("          , ISNULL(HP_CUR0, '" & _nat_curr & "') as HP_CUR0 ")
      _sb.AppendLine("          , AO_COMMENT_HANDPAY ")
      _sb.AppendLine("     FROM   HANDPAYS ")
      _sb.AppendLine("LEFT JOIN   TERMINALS           ON  TE_TERMINAL_ID = HP_TERMINAL_ID ")
      _sb.AppendLine("LEFT JOIN   ACCOUNT_MOVEMENTS   ON  AM_MOVEMENT_ID = HP_MOVEMENT_ID ")
      _sb.AppendLine("LEFT JOIN   ACCOUNTS            ON  AC_ACCOUNT_ID = AM_ACCOUNT_ID ")
      _sb.AppendLine("LEFT JOIN   ACCOUNT_OPERATIONS  ON  AO_OPERATION_ID = HP_OPERATION_ID ")
      _sb.AppendLine("LEFT JOIN   TICKETS             ON  TI_TICKET_ID = HP_TICKET_ID ")
      _sb.AppendLine("LEFT JOIN   CATALOG_ITEMS       ON  CAI_ID = AO_REASON_ID ")
    Else
      _sb.AppendLine("   SELECT   HP_SITE_ID ")
      _sb.AppendLine("          , HP_DATETIME ")
      _sb.AppendLine("          , HP_TE_PROVIDER_ID ")
      _sb.AppendLine("          , HP_TE_NAME ")
      _sb.AppendLine("          , HP_TYPE ")
      _sb.AppendLine("          , HP_AMOUNT ")
      _sb.AppendLine("          , (CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) ")
      _sb.AppendLine("                    THEN HP_AMOUNT                  ")
      _sb.AppendLine("                    ELSE HP_TAX_BASE_AMOUNT         ")
      _sb.AppendLine("             END) AS BASE ")
      _sb.AppendLine("          , (CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) ")
      _sb.AppendLine("                    THEN 0                          ")
      _sb.AppendLine("                    ELSE ISNULL(HP_AMOUNT, 0) - ISNULL(HP_TAX_BASE_AMOUNT, 0) ")
      _sb.AppendLine("             END) AS REMAINDER")
      _sb.AppendLine("          , ISNULL(HP_TAX_AMOUNT, 0)                              AS TAXES")
      _sb.AppendLine("          , ISNULL(HP_AMOUNT, 0) - ISNULL(HP_TAX_AMOUNT, 0)       AS PAID_AMOUNT ")
      _sb.AppendLine("          , TE_PROVIDER_ID ")
      _sb.AppendLine("          , TE_NAME ")
      _sb.AppendLine("          , AC_ACCOUNT_ID ")
      _sb.AppendLine("          , AC_HOLDER_NAME ")
      _sb.AppendLine("          , AM_CASHIER_NAME ")
      _sb.AppendLine("          , NULL  AS  HP_EXPIRED ")
      _sb.AppendLine("          , AC_TYPE ")
      _sb.AppendLine("          , HP_LONG_POLL_1B_DATA ")
      _sb.AppendLine("          , HP_TERMINAL_ID ")
      _sb.AppendLine("          , HP_STATUS ")
      _sb.AppendLine("          , HP_LEVEL ")
      _sb.AppendLine("          , AM_TYPE ")
      _sb.AppendLine("          , NULL  AS  REASON ")
      _sb.AppendLine("          , NULL  AS  AO_COMMENT ")
      _sb.AppendLine("          , ISNULL(HP_AMT0, HP_AMOUNT) AS HP_AMT0 ")
      _sb.AppendLine("          , ISNULL(HP_CUR0, '" & _nat_curr & "') as HP_CUR0 ")
      _sb.AppendLine("          , NULL  AS  AO_COMMENT_HANDPAY ")
      _sb.AppendLine("     FROM   HANDPAYS ")
      _sb.AppendLine("LEFT JOIN   TERMINALS           ON  TE_TERMINAL_ID = HP_TERMINAL_ID   AND TE_SITE_ID = HP_SITE_ID ")
      _sb.AppendLine("LEFT JOIN   ACCOUNT_MOVEMENTS   ON  AM_MOVEMENT_ID = HP_MOVEMENT_ID   AND AM_SITE_ID = HP_SITE_ID ")
      _sb.AppendLine("LEFT JOIN   ACCOUNTS            ON  AC_ACCOUNT_ID = AM_ACCOUNT_ID ")
    End If

    _sb.AppendLine(GetSqlWhere())
    _sb.AppendLine(" ORDER BY   HP_DATETIME ASC ")

    Return _sb.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Checks out the values of a db row before adding it to the grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the db row should be added to the grid
  '     - False: the db row should NOT be added to the grid
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As CLASS_DB_ROW) As Boolean

    Dim _current_day As Date
    Dim _status As HANDPAY_STATUS
    Dim _type As HANDPAY_TYPE
    Dim _hp_level As Integer
    Dim _is_progresive As Boolean
    Dim _daily_day As Date

    _status = HANDPAY_STATUS.PENDING
    _type = HANDPAY_TYPE.CANCELLED_CREDITS
    _hp_level = ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_LEVEL_NOTHING
    _is_progresive = False

    _current_day = GetClosingTime(DbRow.Value(SQL_COLUMN_DATETIME))

    If m_first_time_check_day Then
      m_first_time_check_day = False

      If Me.opt_subtotal_status.Checked Then
        ResetTotalCountersStatus(m_daily_total_status, _current_day)
      Else
        ResetTotalCountersType(m_daily_total_type, _current_day)
      End If
    End If

    If Me.opt_subtotal_status.Checked Then
      _daily_day = m_daily_total_status.day_status
    Else
      _daily_day = m_daily_total_type.day_type
    End If

    ' Actual detailed row, change the Day for the daily totals.
    If Not _daily_day.Equals(_current_day) Then
      If Me.chk_total_day.Checked Then
        If Me.opt_subtotal_type.Checked Then
          ' We have to add daily total row before current details row.
          DrawTotalRowType(ENUM_GRID_ROW_TYPE.DAILY_CANCELLED_CREDITS_TOTAL)
          DrawTotalRowType(ENUM_GRID_ROW_TYPE.DAILY_ORPHAN_CREDITS_TOTAL)
          DrawTotalRowType(ENUM_GRID_ROW_TYPE.DAILY_JACKPOT_TOTAL)
          DrawTotalRowType(ENUM_GRID_ROW_TYPE.DAILY_JACKPOT_PROGRESSIVE_TOTAL)
          DrawTotalRowType(ENUM_GRID_ROW_TYPE.DAILY_JACKPOT_MAJOR_PRIZE_TOTAL)
          DrawTotalRowType(ENUM_GRID_ROW_TYPE.DAILY_JACKPOT_SITE_TOTAL)

          DrawTotalRowType(ENUM_GRID_ROW_TYPE.DAILY_MANUAL_CANCELLED_CREDITS_TOTAL)
          DrawTotalRowType(ENUM_GRID_ROW_TYPE.DAILY_MANUAL_JACKPOT_TOTAL)
          DrawTotalRowType(ENUM_GRID_ROW_TYPE.DAILY_MANUAL_JACKPOT_PROGRESSIVE_TOTAL)
          DrawTotalRowType(ENUM_GRID_ROW_TYPE.DAILY_MANUAL_JACKPOT_MAJOR_PRIZE_TOTAL)
          DrawTotalRowType(ENUM_GRID_ROW_TYPE.DAILY_MANUAL_TOTAL)
          DrawTotalRowType(ENUM_GRID_ROW_TYPE.DAILY_MANUAL_OTHERS_TOTAL)

          DrawTotalRowType(ENUM_GRID_ROW_TYPE.DAILY_TOTAL)
        Else
          ' We have to add daily total row before current details row.
          DrawTotalRowStatus(ENUM_GRID_ROW_STATUS.DAILY_VOIDED_TOTAL)
          DrawTotalRowStatus(ENUM_GRID_ROW_STATUS.DAILY_PAID_TOTAL)
          DrawTotalRowStatus(ENUM_GRID_ROW_STATUS.DAILY_AUTHORIZED_TOTAL)
          DrawTotalRowStatus(ENUM_GRID_ROW_STATUS.DAILY_EXPIRED_TOTAL)
          DrawTotalRowStatus(ENUM_GRID_ROW_STATUS.DAILY_PENDING_TOTAL)

          DrawTotalRowStatus(ENUM_GRID_ROW_STATUS.DAILY_TOTAL)
        End If
      End If

      ' It's a new day... Reset daily totals.
      If Me.opt_subtotal_status.Checked Then
        ResetTotalCountersStatus(m_daily_total_status, _current_day)
      Else
        ResetTotalCountersType(m_daily_total_type, _current_day)
      End If
    End If

    '   Status
    If (Not DbRow.IsNull(SQL_COLUMN_STATUS)) Then
      _status = GetHandPayStatus(DbRow.Value(SQL_COLUMN_STATUS), GetRowExpiredValue(DbRow))
    End If

    '   Type
    If Not DbRow.IsNull(SQL_COLUMN_TYPE) Then
      _type = DbRow.Value(SQL_COLUMN_TYPE)

      If Not DbRow.IsNull(SQL_COLUMN_LEVEL) Then
        _hp_level = DbRow.Value(SQL_COLUMN_LEVEL)

      End If
    End If

    ' Accumulate the totals
    If Me.opt_subtotal_type.Checked Then
      '   By Type
      Call UpdateTotalsType(m_daily_total_type, _
                            _type, _
                            _hp_level, _
                            DbRow.Value(SQL_COLUMN_AMOUNT), _
                            DbRow.Value(SQL_COLUMN_TAX_BASE_AMOUNT), _
                            DbRow.Value(SQL_COLUMN_REST_AMOUNT), _
                            DbRow.Value(SQL_COLUMN_TAX_AMOUNT), _
                            DbRow.Value(SQL_COLUMN_PAID_AMOUNT))

      Call UpdateTotalsType(m_global_total_type, _
                            _type, _
                            _hp_level, _
                            DbRow.Value(SQL_COLUMN_AMOUNT), _
                            DbRow.Value(SQL_COLUMN_TAX_BASE_AMOUNT), _
                            DbRow.Value(SQL_COLUMN_REST_AMOUNT), _
                            DbRow.Value(SQL_COLUMN_TAX_AMOUNT), _
                            DbRow.Value(SQL_COLUMN_PAID_AMOUNT))
    Else
      '   By Status
      Call UpdateTotalsStatus(m_daily_total_status, _
                              _status, _
                              DbRow.Value(SQL_COLUMN_AMOUNT), _
                              DbRow.Value(SQL_COLUMN_TAX_BASE_AMOUNT), _
                              DbRow.Value(SQL_COLUMN_REST_AMOUNT), _
                              DbRow.Value(SQL_COLUMN_TAX_AMOUNT), _
                              DbRow.Value(SQL_COLUMN_PAID_AMOUNT))

      Call UpdateTotalsStatus(m_global_total_status, _
                              _status, _
                              DbRow.Value(SQL_COLUMN_AMOUNT), _
                              DbRow.Value(SQL_COLUMN_TAX_BASE_AMOUNT), _
                              DbRow.Value(SQL_COLUMN_REST_AMOUNT), _
                              DbRow.Value(SQL_COLUMN_TAX_AMOUNT), _
                              DbRow.Value(SQL_COLUMN_PAID_AMOUNT))
    End If

    ' Row must be added to the grid only if details are required
    Return Me.chk_details.Checked

  End Function ' GUI_CheckOutRowBeforeAdd

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _str_cashier As String
    Dim _str_account As String
    Dim _str_paid_cashier_account As String
    Dim _status As HANDPAY_STATUS
    Dim _aux_text As String
    Dim _str_reader As System.IO.StringReader
    Dim _xml_reader As XmlReader
    Dim _group As Byte
    Dim _level As Byte
    Dim _hp_level As Integer
    Dim _terminal_data As List(Of Object)

    _str_account = String.Empty

    ' Row columns
    '   - Site Id
    '   - DateTime
    '   - Provider
    '   - Terminal
    '   - Type
    '   - Amount
    '   - Status
    '   - Current Provider 
    '   - Current Terminal

    ' Site id
    If Not DbRow.IsNull(SQL_COLUMN_SITE_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SITE_ID).Value = String.Format("{0:000}", DbRow.Value(SQL_COLUMN_SITE_ID))
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SITE_ID).Value = ""
    End If

    ' DateTime
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATETIME), , _
                                                                        ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Terminal Report
    If ((m_is_multisite_center) AndAlso (Not DbRow.IsNull(SQL_COLUMN_SITE_ID))) Then
      _terminal_data = TerminalReport.GetReportDataList(DbRow.Value(SQL_COLUMN_SITE_ID), DbRow.Value(SQL_COLUMN_TERMINAL_ID), m_terminal_report_type)
    Else
      _terminal_data = TerminalReport.GetReportDataList(DbRow.Value(SQL_COLUMN_TERMINAL_ID), m_terminal_report_type)
    End If

    For _idx As Int32 = 0 To _terminal_data.Count - 1
      If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value Then
        Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
      End If
    Next

    ' Level
    _hp_level = 0

    If Not DbRow.IsNull(SQL_COLUMN_LEVEL) Then
      _hp_level = DbRow.Value(SQL_COLUMN_LEVEL)
      _group = 0

      ' Group
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GROUP).Value = Hex(_group).ToString().PadLeft(2, "0")

      ' Level
      Me.Grid.Cell(RowIndex, GRID_COLUMN_LEVEL).Value = Hex(_hp_level).ToString().PadLeft(2, "0")

    ElseIf Not DbRow.IsNull(SQL_COLUMN_LONG_POLL_1B_DATA) AndAlso _
           Not String.IsNullOrEmpty(DbRow.Value(SQL_COLUMN_LONG_POLL_1B_DATA)) Then

      'LoadXML from HP_LONG_POLL_1B_DATA
      _aux_text = DbRow.Value(SQL_COLUMN_LONG_POLL_1B_DATA)
      _str_reader = New System.IO.StringReader(_aux_text)
      _xml_reader = XmlReader.Create(_str_reader)
      LoadXml(_xml_reader, _group, _level)

      _hp_level = _level

      ' - Group
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GROUP).Value = Hex(_group).ToString().PadLeft(2, "0")

      ' - Level
      Me.Grid.Cell(RowIndex, GRID_COLUMN_LEVEL).Value = Hex(_hp_level).ToString().PadLeft(2, "0")
    End If

    ' Type
    If Not DbRow.IsNull(SQL_COLUMN_TYPE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE).Value = WSI.Common.Handpays.HandpayTypeString(DbRow.Value(SQL_COLUMN_TYPE), _hp_level)

      If Handpays.StatusActivated(DbRow.Value(SQL_COLUMN_STATUS), HANDPAY_STATUS.AUTOMATICALLY_PAID) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7806) '7806 "Pago Manual Autom�tico"
      End If

      If DbRow.Value(SQL_COLUMN_TYPE) >= 1000 Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE).Value = GLB_NLS_GUI_INVOICING.GetString(330) & " - " & Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE).Value
      End If

      ' indicates undone if do
      If Not DbRow.IsNull(SQL_COLUMN_ACCOUNT_MOVS_TYPE) AndAlso DbRow.Value(SQL_COLUMN_ACCOUNT_MOVS_TYPE) = WSI.Common.MovementType.ManualHandpayCancellation Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE).Value = "(" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2786) & ") " & Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE).Value
      End If
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE).Value = ""
    End If

    ' Amt0
    Me.Grid.Cell(RowIndex, GRID_COLUMN_AMT0).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_AMT0), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & " " & DbRow.Value(SQL_COLUMN_CUR0)

    ' Amount
    Me.Grid.Cell(RowIndex, GRID_COLUMN_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_AMOUNT), _
                                                                          ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    ' Percentage
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PERCENT).Value = ""

    If m_is_tito_mode Then
      ' Tax Base Amount  
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TAX_BASE_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TAX_BASE_AMOUNT), _
                                                                                     ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      ' Rest Amount 
      Me.Grid.Cell(RowIndex, GRID_COLUMN_REST_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_REST_AMOUNT), _
                                                                                     ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      ' Tax Amount
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TAX_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TAX_AMOUNT), _
                                                                                     ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      ' Paid Amount
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PAID_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PAID_AMOUNT), _
                                                                                     ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' Status
    _str_paid_cashier_account = ""
    If Handpays.StatusActivated(DbRow.Value(SQL_COLUMN_STATUS), HANDPAY_STATUS.AUTOMATICALLY_PAID) Then
      _str_paid_cashier_account = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7807, DbRow.Value(SQL_COLUMN_CARD_ACCOUNT)) '7807 "Transferred to the account %1"
    End If

    If Handpays.StatusActivated(DbRow.Value(SQL_COLUMN_STATUS), HANDPAY_STATUS.PAID) Then
      If DbRow.IsNull(SQL_COLUMN_CASHIER_NAME) Then
        _str_cashier = "---"
      ElseIf DbRow.Value(SQL_COLUMN_CASHIER_NAME) = "" Then
        _str_cashier = "---"
      Else
        _str_cashier = DbRow.Value(SQL_COLUMN_CASHIER_NAME)
      End If

      ' Not virtual account
      If DbRow.IsNull(SQL_COLUMN_CARD_HOLDER) Then
        If Not DbRow.IsNull(SQL_COLUMN_CARD_ACCOUNT) Then
          _str_account = DbRow.Value(SQL_COLUMN_CARD_ACCOUNT)
        End If
      ElseIf DbRow.Value(SQL_COLUMN_CARD_HOLDER) = "" Then
        _str_account = DbRow.Value(SQL_COLUMN_CARD_ACCOUNT)
      Else
        _str_account = DbRow.Value(SQL_COLUMN_CARD_ACCOUNT) & ", " & DbRow.Value(SQL_COLUMN_CARD_HOLDER)
      End If

      If Me.m_is_tito_mode Then
        _str_paid_cashier_account = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5359, _str_cashier)      ' Paid by %2
      Else
        _str_paid_cashier_account = GLB_NLS_GUI_AUDITOR.GetString(374, _str_account, _str_cashier) ' Paid to account %1, by %2
      End If
    End If

    If (Not DbRow.IsNull(SQL_COLUMN_STATUS)) Then
      ' Orden obligatorio MP 15/10/2014
      '   Anulado
      '   Pagado
      '   Expirado (Caducado)
      '   Autorizado
      '   Pendiente
      _status = GetHandPayStatus(DbRow.Value(SQL_COLUMN_STATUS), GetRowExpiredValue(DbRow))

      Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = IIf(_status = HANDPAY_STATUS.PAID Or _status = HANDPAY_STATUS.AUTOMATICALLY_PAID, _str_paid_cashier_account, _
                                                                                            Handpays.HandpayStatusString(_status))
    End If

    ' Current Provider 
    If Not DbRow.IsNull(SQL_COLUMN_PROVIDER_ACTUAL) Then
      If String.Equals(DbRow.Value(SQL_COLUMN_PROVIDER), DbRow.Value(SQL_COLUMN_PROVIDER_ACTUAL)) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROVIDER_ACTUAL).Value = ""
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROVIDER_ACTUAL).Value = DbRow.Value(SQL_COLUMN_PROVIDER_ACTUAL)
      End If
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROVIDER_ACTUAL).Value = ""
    End If

    ' Current Terminal 
    If Not DbRow.IsNull(SQL_COLUMN_TERMINAL_ACTUAL) Then
      If String.Equals(DbRow.Value(SQL_COLUMN_TERMINAL), DbRow.Value(SQL_COLUMN_TERMINAL_ACTUAL)) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_ACTUAL).Value = ""
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_ACTUAL).Value = DbRow.Value(SQL_COLUMN_TERMINAL_ACTUAL)
      End If
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_ACTUAL).Value = ""
    End If

    ' If some of the two fields is not empty, put values in both.
    If Me.Grid.Cell(RowIndex, GRID_COLUMN_PROVIDER_ACTUAL).Value <> "" _
    Or Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_ACTUAL).Value <> "" Then

      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROVIDER_ACTUAL).Value = DbRow.Value(SQL_COLUMN_PROVIDER_ACTUAL)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_ACTUAL).Value = DbRow.Value(SQL_COLUMN_TERMINAL_ACTUAL)
    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_ROW_TYPE).Value = ENUM_GRID_ROW_STATUS.DETAILS

    If Tax.EnableTitoTaxWaiver(True) Then
      If DbRow.IsNull(SQL_COLUMN_ACCOUNT_OPS_REASON) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_REASON).Value = ""
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_REASON).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_OPS_REASON)
      End If

      If DbRow.IsNull(SQL_COLUMN_ACCOUNT_OPS_COMMENT) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMENT).Value = ""
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMENT).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_OPS_COMMENT)
      End If
    End If

    If DbRow.IsNull(SQL_COLUMN_ACCOUNT_HP_COMMENT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HP_COMMENT).Value = ""
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HP_COMMENT).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_HP_COMMENT)
    End If

    Return True
  End Function ' GUI_SetupRow

  ''' <summary>
  ''' Get expired value from row
  ''' </summary>
  ''' <param name="DbRow"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetRowExpiredValue(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Integer
    If (DbRow.IsNull(SQL_COLUMN_HP_EXPIRED)) Then
      Return 0
    End If

    Return DbRow.Value(SQL_COLUMN_HP_EXPIRED)
  End Function

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.uc_dsl
  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Set the tool tip text for a given row and column
  '
  '  PARAMS:
  '     - INPUT:
  '           - RowIndex As Integer
  '           - ColIndex As Integer
  '     - OUTPUT:
  '           - ToolTipTxt As String
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_SetToolTipText(ByVal RowIndex As Integer, ByVal ColIndex As Integer, _
                                             ByRef ToolTipTxt As String)

    Dim _str_datetime As String
    Dim _str_provider As String
    Dim _str_terminal As String
    Dim _str_type As String
    Dim _str_amount As String
    Dim _str_status As String
    Dim _str_provider_actual As String
    Dim _str_terminal_actual As String

    ' If Row is Header, exit.
    If RowIndex < 0 Then
      Exit Sub
    End If

    ' Only show Special ToolTip if Row Type is DETAILS.
    If Me.Grid.Cell(RowIndex, GRID_COLUMN_ROW_TYPE).Value <> ENUM_GRID_ROW_STATUS.DETAILS Then
      Exit Sub
    End If

    _str_datetime = Me.Grid.Cell(RowIndex, GRID_COLUMN_DATETIME).Value
    _str_provider = Me.Grid.Cell(RowIndex, GRID_COLUMN_PROVIDER).Value
    _str_terminal = Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL).Value
    _str_type = Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE).Value
    _str_amount = Me.Grid.Cell(RowIndex, GRID_COLUMN_AMOUNT).Value
    _str_status = Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value

    _str_provider_actual = Me.Grid.Cell(RowIndex, GRID_COLUMN_PROVIDER_ACTUAL).Value
    _str_terminal_actual = Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_ACTUAL).Value

    ToolTipTxt = GLB_NLS_GUI_AUDITOR.GetString(121, _str_datetime, _str_provider, _str_terminal, _str_amount) & _
                 ChrW(10).ToString() & _str_status
    If _str_provider_actual <> "" Then
      ToolTipTxt = ToolTipTxt & GLB_NLS_GUI_AUDITOR.GetString(122, _str_provider_actual, _str_terminal_actual)
    End If

  End Sub ' GUI_SetToolTipText

#Region " GUI Reports "

  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(ExcelData)

    ' Set specific column formats.
    'ExcelData.SetColumnFormat(EXCEL_COLUMN_GROUP, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)
    'ExcelData.SetColumnFormat(EXCEL_COLUMN_LEVEL, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1927), m_sites)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(251), m_terminals)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(211), m_types)

    If (Not m_is_multisite_center) Then
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(237), m_status)
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1046), m_subtotal)
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5651), m_paid_by)
    End If

    PrintData.SetFilter(chk_details.Text, m_show_details)
    PrintData.SetFilter(chk_total_day.Text, m_show_totals)
    PrintData.SetFilter(chk_terminal_location.Text, m_show_terminal_location)
  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()
    Dim _list_handpay_status As List(Of TYPE_DG_FILTER_ELEMENT)

    m_date_from = ""
    m_date_to = ""
    m_terminals = ""
    m_types = ""
    m_status = ""
    m_paid_by = ""
    m_sites = ""

    ' From Date
    If Me.uc_dsl.FromDateSelected Then
      m_date_from = GUI_FormatDate(Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime), _
                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    ' To Date
    If Me.uc_dsl.ToDateSelected Then
      m_date_to = GUI_FormatDate(Me.uc_dsl.ToDate.Date.AddHours(Me.uc_dsl.ClosingTime), _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    ' Providers - Terminals
    m_terminals = Me.uc_pr_list.GetTerminalReportText()

    ' Sites
    If (m_is_multisite_center) Then
      m_sites = uc_site_select.GetSitesIdListSelectedToPrint()
    End If

    ' Show Details
    If Me.chk_details.Checked = True Then
      m_show_details = GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_show_details = GLB_NLS_GUI_INVOICING.GetString(480)
    End If

    ' Show Totals
    If Me.chk_total_day.Checked = True Then
      m_show_totals = GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_show_totals = GLB_NLS_GUI_INVOICING.GetString(480)
    End If

    'Show Terminal Location
    If Me.chk_terminal_location.Checked Then
      m_show_terminal_location = GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_show_terminal_location = GLB_NLS_GUI_INVOICING.GetString(480)
    End If

    ' Paid by
    If (opt_all_users.Checked) Then
      m_paid_by = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353)
    Else
      If String.IsNullOrEmpty(cmb_user.Text) Then
        m_paid_by = String.Empty
      Else
        m_paid_by = cmb_user.TextValue
      End If
    End If

    ' HandPay Status
    If Me.opt_all_status.Checked Then
      m_status = Me.opt_all_status.Text
    Else
      _list_handpay_status = Me.GetStatusIdListSelected(Me.dg_filter_status)
      For Each item As TYPE_DG_FILTER_ELEMENT In _list_handpay_status
        m_status = m_status & IIf(m_status <> String.Empty, ",", String.Empty) & item.description.ToString
      Next
    End If

    ' HandPay Types 
    If Me.uc_checked_list_handtypes.SelectedIndexes.Length <= 0 OrElse _
       Me.uc_checked_list_handtypes.Count = Me.uc_checked_list_handtypes.SelectedIndexes.Length Then

      m_types = GLB_NLS_GUI_INVOICING.GetString(224)
    Else
      m_types = Me.uc_checked_list_handtypes.SelectedValuesList
    End If

    'SubTotal
    If Me.opt_subtotal_type.Checked Then
      m_subtotal = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5636)    'By type
    Else
      m_subtotal = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5635)    'By status
    End If
  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region  ' OVERRIDES

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    Dim _terminal_columns As List(Of ColumnSettings)
    Dim _report_Column As WSI.Common.ReportColumn

    Call GridColumnReIndex()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Site
      .Column(GRID_COLUMN_SITE_ID).Header(0).Text = " "
      .Column(GRID_COLUMN_SITE_ID).Header(1).Text = IIf(m_is_multisite_center, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1927), String.Empty) ' SiteId
      .Column(GRID_COLUMN_SITE_ID).Width = IIf(m_is_multisite_center, GRID_WIDTH_SITE_ID, GRID_WIDTH_INVISIBLE)
      .Column(GRID_COLUMN_SITE_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Date
      .Column(GRID_COLUMN_DATETIME).Header(0).Text = ""
      .Column(GRID_COLUMN_DATETIME).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(201)
      .Column(GRID_COLUMN_DATETIME).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '  Terminal Report
      _terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)
      For _idx As Int32 = 0 To _terminal_columns.Count - 1
        _report_Column = _terminal_columns(_idx).Column
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(232)
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = _terminal_columns(_idx).Header1

        If _report_Column = ReportColumn.Provider AndAlso _
           Me.opt_subtotal_type.Checked Then

          .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = GRID_WIDTH_PROVIDER_TYPE
        Else
          .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = _terminal_columns(_idx).Width
        End If

        .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = _terminal_columns(_idx).Alignment
        If _terminal_columns(_idx).Column = ReportColumn.Provider Then
          GRID_COLUMN_PROVIDER = GRID_INIT_TERMINAL_DATA + _idx
        End If

        If _terminal_columns(_idx).Column = ReportColumn.Terminal Then
          GRID_COLUMN_TERMINAL = GRID_INIT_TERMINAL_DATA + _idx
        End If
      Next

      ' Amt0
      .Column(GRID_COLUMN_AMT0).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(159)
      .Column(GRID_COLUMN_AMT0).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7104) 'Created amount
      .Column(GRID_COLUMN_AMT0).Width = GRID_WIDTH_AMOUNT_LONG
      .Column(GRID_COLUMN_AMT0).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Amount
      .Column(GRID_COLUMN_AMOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(159)
      If m_is_tito_mode Then
        .Column(GRID_COLUMN_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5885) 'Total
      Else
        .Column(GRID_COLUMN_AMOUNT).Header(1).Text = GLB_NLS_GUI_AUDITOR.GetString(372)
      End If

      .Column(GRID_COLUMN_AMOUNT).Width = GRID_WIDTH_AMOUNT_LONG
      .Column(GRID_COLUMN_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Percentage
      .Column(GRID_COLUMN_PERCENT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(159)
      .Column(GRID_COLUMN_PERCENT).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(160)
      .Column(GRID_COLUMN_PERCENT).Width = GRID_WIDTH_PERCENT
      .Column(GRID_COLUMN_PERCENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'TITO MODE
      ' Tax Base Amount  
      If TITO.Utils.IsTitoMode Then
        .Column(GRID_COLUMN_TAX_BASE_AMOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(159)
        .Column(GRID_COLUMN_TAX_BASE_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5886)  'Base
        .Column(GRID_COLUMN_TAX_BASE_AMOUNT).Width = GRID_WIDTH_AMOUNT_LONG
        .Column(GRID_COLUMN_TAX_BASE_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ' Rest Amount  
        .Column(GRID_COLUMN_REST_AMOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(159)
        .Column(GRID_COLUMN_REST_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5887)      'Resto
        .Column(GRID_COLUMN_REST_AMOUNT).Width = GRID_WIDTH_AMOUNT_LONG
        .Column(GRID_COLUMN_REST_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ' Tax Amount  
        .Column(GRID_COLUMN_TAX_AMOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(159)
        .Column(GRID_COLUMN_TAX_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5888)       'Retenciones
        .Column(GRID_COLUMN_TAX_AMOUNT).Width = GRID_WIDTH_AMOUNT_LONG
        .Column(GRID_COLUMN_TAX_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ' Paid Amount
        .Column(GRID_COLUMN_PAID_AMOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(159)
        .Column(GRID_COLUMN_PAID_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5889)      'Pagado
        .Column(GRID_COLUMN_PAID_AMOUNT).Width = GRID_WIDTH_AMOUNT_LONG
        .Column(GRID_COLUMN_PAID_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      Else
        .Column(GRID_COLUMN_TAX_BASE_AMOUNT).Width = GRID_WIDTH_INVISIBLE
        .Column(GRID_COLUMN_REST_AMOUNT).Width = GRID_WIDTH_INVISIBLE
        .Column(GRID_COLUMN_TAX_AMOUNT).Width = GRID_WIDTH_INVISIBLE
        .Column(GRID_COLUMN_PAID_AMOUNT).Width = GRID_WIDTH_INVISIBLE
      End If

      ' Type
      .Column(GRID_COLUMN_TYPE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(314)
      .Column(GRID_COLUMN_TYPE).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(314)
      .Column(GRID_COLUMN_TYPE).Width = GRID_WIDTH_TYPE
      .Column(GRID_COLUMN_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Group
      .Column(GRID_COLUMN_GROUP).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(314)
      .Column(GRID_COLUMN_GROUP).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4807)
      .Column(GRID_COLUMN_GROUP).Width = GRID_WIDTH_GROUP
      .Column(GRID_COLUMN_GROUP).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Level
      .Column(GRID_COLUMN_LEVEL).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(314)
      .Column(GRID_COLUMN_LEVEL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4808)
      .Column(GRID_COLUMN_LEVEL).Width = GRID_WIDTH_LEVEL
      .Column(GRID_COLUMN_LEVEL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Status
      .Column(GRID_COLUMN_STATUS).Header(0).Text = ""
      .Column(GRID_COLUMN_STATUS).Header(1).Text = GLB_NLS_GUI_CONTROLS.GetString(261)
      .Column(GRID_COLUMN_STATUS).Width = IIf(m_is_multisite_center, GRID_WIDTH_INVISIBLE, GRID_WIDTH_STATUS)
      .Column(GRID_COLUMN_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Necessary for ToolTip Text
      ' Provider Actual
      .Column(GRID_COLUMN_PROVIDER_ACTUAL).Width = GRID_WIDTH_INVISIBLE
      .Column(GRID_COLUMN_PROVIDER_ACTUAL).Header(0).Text = ""
      .Column(GRID_COLUMN_PROVIDER_ACTUAL).Header(1).Text = ""

      ' Terminal Actual
      .Column(GRID_COLUMN_TERMINAL_ACTUAL).Width = GRID_WIDTH_INVISIBLE
      .Column(GRID_COLUMN_TERMINAL_ACTUAL).Header(0).Text = ""
      .Column(GRID_COLUMN_TERMINAL_ACTUAL).Header(1).Text = ""

      ' Row Type
      .Column(GRID_COLUMN_ROW_TYPE).Width = GRID_WIDTH_INVISIBLE
      .Column(GRID_COLUMN_ROW_TYPE).Header(0).Text = ""
      .Column(GRID_COLUMN_ROW_TYPE).Header(1).Text = ""

      ' Comment
      .Column(GRID_COLUMN_HP_COMMENT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1578)
      .Column(GRID_COLUMN_HP_COMMENT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6785)
      .Column(GRID_COLUMN_HP_COMMENT).Width = IIf(m_is_multisite_center, GRID_WIDTH_INVISIBLE, GRID_WIDTH_COMMENT)
      .Column(GRID_COLUMN_HP_COMMENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      If ((Tax.EnableTitoTaxWaiver(True)) AndAlso (Not m_is_multisite_center)) Then
        .Column(GRID_COLUMN_REASON).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7160)
        .Column(GRID_COLUMN_REASON).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6784)
        .Column(GRID_COLUMN_REASON).Width = GRID_WIDTH_REASON
        .Column(GRID_COLUMN_REASON).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

        .Column(GRID_COLUMN_COMMENT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7160)
        .Column(GRID_COLUMN_COMMENT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6785)
        .Column(GRID_COLUMN_COMMENT).Width = GRID_WIDTH_COMMENT
        .Column(GRID_COLUMN_COMMENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      Else
        .Column(GRID_COLUMN_REASON).Width = GRID_WIDTH_INVISIBLE
        .Column(GRID_COLUMN_COMMENT).Width = GRID_WIDTH_INVISIBLE
      End If

      ' XGJ 15-GEN-2018 The grid must can be sorted is the AGG feature is enabled
      If (WSI.Common.Misc.IsAGGEnabled()) Then
        .Sortable = True
      Else
        .Sortable = False
      End If

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Dim _closing_time As Integer
    Dim _final_time As Date
    Dim _now As Date

    _now = WGDB.Now

    _closing_time = GetDefaultClosingTime()
    _final_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)
    If _final_time > _now Then
      _final_time = _final_time.AddDays(-1)
    End If

    _final_time = _final_time.Date
    Me.uc_dsl.ToDate = _final_time
    Me.uc_dsl.ToDateSelected = True
    Me.uc_dsl.FromDate = _final_time.AddDays(-1)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.ClosingTime = _closing_time

    Me.chk_details.Checked = True
    Me.chk_total_day.Checked = False

    Me.opt_subtotal_status.Checked = Not m_is_multisite_center
    Me.opt_subtotal_type.Checked = m_is_multisite_center

    ' Terminals
    Call Me.uc_pr_list.SetDefaultValues()

    ' Sites
    If (m_is_multisite_center) Then
      Dim _dt_sites As DataTable = Me.uc_site_select.GetSitesDataTable()
      If (Not _dt_sites Is Nothing) AndAlso (_dt_sites.Rows.Count > 0) Then
        Me.uc_site_select.SetDefaultValues(False) 'Clear all sites checked
        Me.uc_site_select.SetSitesIdListSelected(_dt_sites.Rows(0).Item(SQL_COLUMN_SITE_ID)) 'Select first site
      End If
    End If

    Me.chk_terminal_location.Checked = False

    ' Pagado por
    Me.opt_all_users.Checked = True
    Me.chk_show_all.Checked = False
    Call SwitchCheckShowAllUsers()

    ' LTC 22-JAN-2018
    ' Pago
    Me.ef_mb_amount_limit.Enabled = True
    Me.ef_mb_amount_limit.TextValue = GUI_FormatCurrency(0)

    ' Movement Types data grid
    Call InitTypesFilter()

    ' Catalog Filter (set Default Values)
    Call Me.uc_checked_list_handtypes.CollapseAll()
    Call Me.uc_checked_list_handtypes.SetDefaultValue(True)
  End Sub ' SetDefaultValues

  ' PURPOSE: Return the query for the selected user.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlUserFilter() As String
    Dim _sb As StringBuilder

    _sb = New StringBuilder()
    _sb.AppendLine(" AND AM_CASHIER_NAME LIKE '" & cmb_user.TextValue.ToString() & "@%'")

    Return _sb.ToString()
  End Function ' GetSqlUserFilter

  ' PURPOSE: Return the query for get all users.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlUsers() As String
    Dim _sb As StringBuilder

    _sb = New StringBuilder()
    _sb.AppendLine("SELECT   GU_USER_ID")
    _sb.AppendLine("       , GU_USERNAME")
    _sb.AppendLine("  FROM   GUI_USERS")
    _sb.AppendLine(" WHERE   GU_USER_TYPE IN ( " & GU_USER_TYPE.USER & ", " & GU_USER_TYPE.SYS_SYSTEM & " ) ")

    If Not chk_show_all.Checked Then
      _sb.AppendLine(" AND   GU_BLOCK_REASON = '" & WSI.Common.GUI_USER_BLOCK_REASON.NONE & "'")
    End If

    _sb.AppendLine("ORDER BY GU_USERNAME")

    Return _sb.ToString()
  End Function ' GetSqlUsers

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the
  '          main SQL Query.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String

    Dim _str_where As String = ""

    ' Filter Dates
    _str_where = Me.uc_dsl.GetSqlFilterCondition("HP_DATETIME")
    If _str_where.Length > 0 Then
      _str_where = " AND " & _str_where
    End If

    ' Filter Providers - Terminals
    _str_where = _str_where & " AND HP_TERMINAL_ID IN " & Me.uc_pr_list.GetProviderIdListSelected()

    ' Filter sites
    If (m_is_multisite_center) Then
      Dim _str_filter_site As String
      _str_filter_site = uc_site_select.GetSitesIdListSelected()

      If (_str_filter_site <> String.Empty) Then
        _str_where = _str_where & " AND HP_SITE_ID IN (" & _str_filter_site & ")"
      End If
    End If

    'HANDPAY_TYPE.SPECIAL_PROGRESSIVE should not appear on screen
    _str_where = _str_where & " AND HP_TYPE <> " & WSI.Common.HANDPAY_TYPE.SPECIAL_PROGRESSIVE & " "

    ' Filter Movement type
    _str_where = _str_where & GetSqlWhereType("HP_TYPE", "HP_LEVEL", "HP_PAYMENT_MODE")

    'Filter Status movement 
    _str_where = _str_where & GetSqlWhereStatus("HP_STATUS")

    ' User filter
    If opt_one_user.Checked Then
      _str_where = _str_where & GetSqlUserFilter()
    End If

    ' LTC 22-JAN-2018
    ' Limit payment
    If (GUI_ParseCurrency(ef_mb_amount_limit.TextValue) > 0) Then
      _str_where = _str_where & " AND HP_AMOUNT > " & GUI_ParseCurrency(ef_mb_amount_limit.TextValue)
    End If

    If _str_where.Length > 0 Then
      _str_where = Strings.Right(_str_where, Len(_str_where) - 5)
      _str_where = " WHERE " & _str_where
    End If

    Return _str_where
  End Function ' GetSqlWhere

  ' PURPOSE : Obtain the Closing Time based on RowDate.
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowDate As Date
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '           - Date in ClosingTime format.
  '
  Private Function GetClosingTime(ByVal RowDate As Date) As Date
    Dim _closing_time As Date

    _closing_time = New DateTime(RowDate.Year, RowDate.Month, RowDate.Day, Me.uc_dsl.ClosingTime, 0, 0)
    If RowDate.Hour < Me.uc_dsl.ClosingTime Then
      _closing_time = _closing_time.AddDays(-1)
    End If

    Return _closing_time
  End Function ' GetClosingTime

  ' PURPOSE : Add and draw a total row.
  '
  '  PARAMS :
  '     - INPUT : 
  '           - Type As ENUM_GRID_ROW_STATUS
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub DrawTotalRowStatus(ByVal Type As ENUM_GRID_ROW_STATUS)
    Dim _idx_row As Integer
    Dim _str_total As String
    Dim _str_total_descrip As String
    Dim _amount As Decimal
    Dim _base_amount As Decimal
    Dim _rest_amount As Decimal
    Dim _retention_amount As Decimal
    Dim _paid_amount As Decimal
    Dim _percent As Decimal

    Dim _count As Integer
    Dim _daily As Boolean
    Dim _color As ENUM_GUI_COLOR
    Dim _status_handpay As HANDPAY_STATUS
    Dim _add_row As Boolean
    Dim _list_of_status As List(Of TYPE_DG_FILTER_ELEMENT)

    _add_row = False
    _str_total = String.Empty
    _str_total_descrip = String.Empty

    Select Case Type

      ' **Daily Totals
      Case ENUM_GRID_ROW_STATUS.DAILY_PAID_TOTAL
        ' Paid
        _str_total = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5557) & " " & _
                     WSI.Common.Handpays.HandpayStatusString(HANDPAY_STATUS.PAID) & ":"
        _amount = m_daily_total_status.paid_amount
        _count = m_daily_total_status.paid_count

        If Me.m_is_tito_mode Then
          _base_amount = m_daily_total_status.paid_base_amount
          _rest_amount = m_daily_total_status.paid_rest_amount
          _retention_amount = m_daily_total_status.paid_retention_amount
          _paid_amount = m_daily_total_status.paid_paid_amount
        End If

        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01
        _daily = True
        _status_handpay = HANDPAY_STATUS.PAID

      Case ENUM_GRID_ROW_STATUS.DAILY_PENDING_TOTAL
        ' Pending
        _str_total = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5557) & " " & _
                     WSI.Common.Handpays.HandpayStatusString(HANDPAY_STATUS.PENDING) & ":"
        _amount = m_daily_total_status.pending_amount
        _count = m_daily_total_status.pending_count

        If Me.m_is_tito_mode Then
          _base_amount = m_daily_total_status.pending_base_amount
          _rest_amount = m_daily_total_status.pending_rest_amount
          _retention_amount = m_daily_total_status.pending_retention_amount
          _paid_amount = m_daily_total_status.pending_paid_amount
        End If

        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01
        _daily = True
        _status_handpay = HANDPAY_STATUS.PENDING

      Case ENUM_GRID_ROW_STATUS.DAILY_EXPIRED_TOTAL
        ' Expired (Caducado)
        _str_total = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5557) & " " & _
                     WSI.Common.Handpays.HandpayStatusString(HANDPAY_STATUS.EXPIRED) & ":"
        _amount = m_daily_total_status.expired_amount
        _count = m_daily_total_status.expired_count

        If Me.m_is_tito_mode Then
          _base_amount = m_daily_total_status.expired_base_amount
          _rest_amount = m_daily_total_status.expired_rest_amount
          _retention_amount = m_daily_total_status.expired_retention_amount
          _paid_amount = m_daily_total_status.expired_paid_amount
        End If

        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01
        _daily = True
        _status_handpay = HANDPAY_STATUS.EXPIRED

      Case ENUM_GRID_ROW_STATUS.DAILY_AUTHORIZED_TOTAL
        ' Authorized
        _str_total = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5557) & " " & _
                     WSI.Common.Handpays.HandpayStatusString(HANDPAY_STATUS.AUTHORIZED) & ":"
        _amount = m_daily_total_status.authorized_amount
        _count = m_daily_total_status.authorized_count

        If Me.m_is_tito_mode Then
          _base_amount = m_daily_total_status.authorized_base_amount
          _rest_amount = m_daily_total_status.authorized_rest_amount
          _retention_amount = m_daily_total_status.authorized_retention_amount
          _paid_amount = m_daily_total_status.authorized_paid_amount
        End If

        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01
        _daily = True
        _status_handpay = HANDPAY_STATUS.AUTHORIZED

      Case ENUM_GRID_ROW_STATUS.DAILY_VOIDED_TOTAL
        ' Voided (Anulado)
        _str_total = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5557) & " " & _
                     WSI.Common.Handpays.HandpayStatusString(HANDPAY_STATUS.VOIDED) & ":"
        _amount = m_daily_total_status.voided_amount
        _count = m_daily_total_status.voided_count

        If Me.m_is_tito_mode Then
          _base_amount = m_daily_total_status.voided_base_amount
          _rest_amount = m_daily_total_status.voided_rest_amount
          _retention_amount = m_daily_total_status.voided_retention_amount
          _paid_amount = m_daily_total_status.voided_paid_amount
        End If

        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01
        _daily = True
        _status_handpay = HANDPAY_STATUS.VOIDED

      Case ENUM_GRID_ROW_STATUS.DAILY_TOTAL
        'Total
        _str_total = GLB_NLS_GUI_INVOICING.GetString(205)
        _amount = m_daily_total_status.total_amount_status
        _count = m_daily_total_status.total_count_status

        If Me.m_is_tito_mode Then
          _base_amount = m_daily_total_status.total_base_amount_status
          _rest_amount = m_daily_total_status.total_rest_amount_status
          _retention_amount = m_daily_total_status.total_retention_amount_status
          _paid_amount = m_daily_total_status.total_paid_amount_status
        End If

        _color = ENUM_GUI_COLOR.GUI_COLOR_OCHRE_00
        _daily = True

        ' **Global Totals
      Case ENUM_GRID_ROW_STATUS.PAID_TOTAL
        'Paid
        _str_total = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5557) & " " & _
                     WSI.Common.Handpays.HandpayStatusString(HANDPAY_STATUS.PAID) & ":"
        _amount = m_global_total_status.paid_amount
        _count = m_global_total_status.paid_count

        If Me.m_is_tito_mode Then
          _base_amount = m_global_total_status.paid_base_amount
          _rest_amount = m_global_total_status.paid_rest_amount
          _retention_amount = m_global_total_status.paid_retention_amount
          _paid_amount = m_global_total_status.paid_paid_amount
        End If

        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
        _daily = False
        _status_handpay = HANDPAY_STATUS.PAID

      Case ENUM_GRID_ROW_STATUS.PENDING_TOTAL
        ' Pending
        _str_total = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5557) & " " & _
                     WSI.Common.Handpays.HandpayStatusString(HANDPAY_STATUS.PENDING) & ":"
        _amount = m_global_total_status.pending_amount
        _count = m_global_total_status.pending_count

        If Me.m_is_tito_mode Then
          _base_amount = m_global_total_status.pending_base_amount
          _rest_amount = m_global_total_status.pending_rest_amount
          _retention_amount = m_global_total_status.pending_retention_amount
          _paid_amount = m_global_total_status.pending_paid_amount
        End If

        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
        _daily = False
        _status_handpay = HANDPAY_STATUS.PENDING

      Case ENUM_GRID_ROW_STATUS.EXPIRED_TOTAL
        ' Expired (Caducado)
        _str_total = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5557) & " " & _
                     WSI.Common.Handpays.HandpayStatusString(HANDPAY_STATUS.EXPIRED) & ":"
        _amount = m_global_total_status.expired_amount
        _count = m_global_total_status.expired_count
        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00

        If Me.m_is_tito_mode Then
          _base_amount = m_global_total_status.expired_base_amount
          _rest_amount = m_global_total_status.expired_rest_amount
          _retention_amount = m_global_total_status.expired_retention_amount
          _paid_amount = m_global_total_status.expired_paid_amount
        End If

        _daily = False
        _status_handpay = HANDPAY_STATUS.EXPIRED

      Case ENUM_GRID_ROW_STATUS.AUTHORIZED_TOTAL
        ' Authorized
        _str_total = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5557) & " " & _
                     WSI.Common.Handpays.HandpayStatusString(HANDPAY_STATUS.AUTHORIZED) & ":"
        _amount = m_global_total_status.authorized_amount
        _count = m_global_total_status.authorized_count

        If Me.m_is_tito_mode Then
          _base_amount = m_global_total_status.authorized_base_amount
          _rest_amount = m_global_total_status.authorized_rest_amount
          _retention_amount = m_global_total_status.authorized_retention_amount
          _paid_amount = m_global_total_status.authorized_paid_amount
        End If

        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
        _daily = False
        _status_handpay = HANDPAY_STATUS.AUTHORIZED

      Case ENUM_GRID_ROW_STATUS.VOIDED_TOTAL
        ' Voided (Anulado)
        _str_total = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5557) & " " & _
                     WSI.Common.Handpays.HandpayStatusString(HANDPAY_STATUS.VOIDED) & ":"
        _amount = m_global_total_status.voided_amount
        _count = m_global_total_status.voided_count

        If Me.m_is_tito_mode Then
          _base_amount = m_global_total_status.voided_base_amount
          _rest_amount = m_global_total_status.voided_rest_amount
          _retention_amount = m_global_total_status.voided_retention_amount
          _paid_amount = m_global_total_status.voided_paid_amount
        End If

        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
        _daily = False
        _status_handpay = HANDPAY_STATUS.VOIDED

      Case ENUM_GRID_ROW_STATUS.TOTAL
        _str_total = GLB_NLS_GUI_INVOICING.GetString(205)
        _amount = m_global_total_status.total_amount_status
        _count = m_global_total_status.total_count_status

        If Me.m_is_tito_mode Then
          _base_amount = m_global_total_status.total_base_amount_status
          _rest_amount = m_global_total_status.total_rest_amount_status
          _retention_amount = m_global_total_status.total_retention_amount_status
          _paid_amount = m_global_total_status.total_paid_amount_status
        End If

        _color = ENUM_GUI_COLOR.GUI_COLOR_ORANGE_01
        _daily = False

    End Select

    ' RCI 22-NOV-2011: Add percentage column only in total rows.
    _percent = -1
    Select Case Type
      Case ENUM_GRID_ROW_STATUS.DAILY_TOTAL, ENUM_GRID_ROW_STATUS.TOTAL
        ' Do nothing

      Case ENUM_GRID_ROW_STATUS.DAILY_PAID_TOTAL, _
           ENUM_GRID_ROW_STATUS.DAILY_PENDING_TOTAL, _
           ENUM_GRID_ROW_STATUS.DAILY_EXPIRED_TOTAL, _
           ENUM_GRID_ROW_STATUS.DAILY_AUTHORIZED_TOTAL, _
           ENUM_GRID_ROW_STATUS.DAILY_VOIDED_TOTAL

        If m_daily_total_status.total_amount_status > 0 Then
          _percent = Math.Round(100 * (_amount / m_daily_total_status.total_amount_status), 2, MidpointRounding.AwayFromZero)
        End If

      Case ENUM_GRID_ROW_STATUS.PAID_TOTAL, _
           ENUM_GRID_ROW_STATUS.PENDING_TOTAL, _
           ENUM_GRID_ROW_STATUS.EXPIRED_TOTAL, _
           ENUM_GRID_ROW_STATUS.AUTHORIZED_TOTAL, _
           ENUM_GRID_ROW_STATUS.VOIDED_TOTAL

        If m_global_total_status.total_amount_status > 0 Then
          _percent = Math.Round(100 * (_amount / m_global_total_status.total_amount_status), 2, MidpointRounding.AwayFromZero)
        End If
    End Select

    Select Case Type
      Case ENUM_GRID_ROW_STATUS.DAILY_TOTAL, ENUM_GRID_ROW_STATUS.TOTAL
        _add_row = True

      Case Else
        _list_of_status = GetStatusIdListSelected(dg_filter_status)
        m_id_status_find = _status_handpay

        If Me.opt_all_status.Checked OrElse _
           _list_of_status.FindAll(AddressOf FindIdStatusHandPay).Count > 0 Then

          _add_row = True
        End If
    End Select

    If _add_row Then
      Me.Grid.AddRow()
      _idx_row = Me.Grid.NumRows - 1

      If _daily Then
        Me.Grid.Cell(_idx_row, GRID_COLUMN_DATETIME).Value = GLB_NLS_GUI_CONFIGURATION.GetString(495) & " " & _
                                                             GUI_FormatDate(m_daily_total_status.day_status, , _
                                                             ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
      End If

      Me.Grid.Cell(_idx_row, GRID_COLUMN_PROVIDER).Value = _str_total_descrip & _str_total
      Me.Grid.Cell(_idx_row, GRID_COLUMN_TERMINAL).Value = GUI_FormatNumber(_count, 0)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_AMOUNT).Value = GUI_FormatCurrency(_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_PERCENT).Value = IIf(_percent = -1, "", GUI_FormatNumber(_percent, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%")

      If Me.m_is_tito_mode Then
        Me.Grid.Cell(_idx_row, GRID_COLUMN_TAX_BASE_AMOUNT).Value = GUI_FormatCurrency(_base_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Me.Grid.Cell(_idx_row, GRID_COLUMN_REST_AMOUNT).Value = GUI_FormatCurrency(_rest_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Me.Grid.Cell(_idx_row, GRID_COLUMN_TAX_AMOUNT).Value = GUI_FormatCurrency(_retention_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Me.Grid.Cell(_idx_row, GRID_COLUMN_PAID_AMOUNT).Value = GUI_FormatCurrency(_paid_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End If

      Me.Grid.Row(_idx_row).BackColor = GetColor(_color)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_ROW_TYPE).Value = ENUM_GRID_ROW_STATUS.TOTAL
    End If
  End Sub ' DrawTotalRowStatus

  ' PURPOSE : Add and draw a total row.
  '
  '  PARAMS :
  '     - INPUT : 
  '           - Type As ENUM_GRID_ROW_TYPE
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub DrawTotalRowType(ByVal Type As ENUM_GRID_ROW_TYPE)
    Dim _idx_row As Integer
    Dim _str_total As String
    Dim _amount As Decimal
    Dim _base_amount As Decimal
    Dim _rest_amount As Decimal
    Dim _retention_amount As Decimal
    Dim _paid_amount As Decimal
    Dim _percent As Decimal
    Dim _count As Integer
    Dim _daily As Boolean
    Dim _color As ENUM_GUI_COLOR
    Dim _type_handpay As HANDPAY_TYPE
    Dim _add_row As Boolean
    Dim _str_total_descrip As String

    _str_total_descrip = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5557) & " "

    'Get Range progressive (1-32)

    _add_row = False
    _str_total = String.Empty

    Select Case Type
      ' **Daily Totals
      Case ENUM_GRID_ROW_TYPE.DAILY_CANCELLED_CREDITS_TOTAL
        ' Cancelled Credits
        _str_total = WSI.Common.Handpays.HandpayTypeString(HANDPAY_TYPE.CANCELLED_CREDITS, _
                                                           ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_LEVEL_NOTHING) & ":"
        _amount = m_daily_total_type.cancelled_credits_amount
        _count = m_daily_total_type.cancelled_credits_count

        If Me.m_is_tito_mode Then
          _base_amount = m_daily_total_type.cancelled_credits_base_amount
          _rest_amount = m_daily_total_type.cancelled_credits_rest_amount
          _retention_amount = m_daily_total_type.cancelled_credits_retention_amount
          _paid_amount = m_daily_total_type.cancelled_credits_paid_amount
        End If

        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01
        _daily = True
        _type_handpay = HANDPAY_TYPE.CANCELLED_CREDITS

      Case ENUM_GRID_ROW_TYPE.DAILY_JACKPOT_TOTAL
        ' Jackpot
        _str_total = WSI.Common.Handpays.HandpayTypeString(HANDPAY_TYPE.JACKPOT, _
                                                           ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_LEVEL_NOTHING) & ":"
        _amount = m_daily_total_type.jackpot_amount
        _count = m_daily_total_type.jackpot_count

        If Me.m_is_tito_mode Then
          _base_amount = m_daily_total_type.jackpot_base_amount
          _rest_amount = m_daily_total_type.jackpot_rest_amount
          _retention_amount = m_daily_total_type.jackpot_retention_amount
          _paid_amount = m_daily_total_type.jackpot_paid_amount
        End If

        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01
        _daily = True
        _type_handpay = HANDPAY_TYPE.JACKPOT + ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT

      Case ENUM_GRID_ROW_TYPE.DAILY_JACKPOT_PROGRESSIVE_TOTAL
        'Progressive Jackpot
        _str_total = WSI.Common.Handpays.HandpayTypeString(HANDPAY_TYPE.JACKPOT, _
                                                           ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_PROGRESSIVE_LEVEL_START) & ":"
        _amount = m_daily_total_type.jackpot_progressive_amount
        _count = m_daily_total_type.jackpot_progressive_count

        If Me.m_is_tito_mode Then
          _base_amount = m_daily_total_type.jackpot_progressive_base_amount
          _rest_amount = m_daily_total_type.jackpot_progressive_rest_amount
          _retention_amount = m_daily_total_type.jackpot_progressive_retention_amount
          _paid_amount = m_daily_total_type.jackpot_progressive_paid_amount
        End If

        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01
        _daily = True
        _type_handpay = HANDPAY_TYPE.JACKPOT + ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT_PROGRESSIVE

      Case ENUM_GRID_ROW_TYPE.DAILY_JACKPOT_MAJOR_PRIZE_TOTAL
        ' JackPot Top Win
        _str_total = WSI.Common.Handpays.HandpayTypeString(HANDPAY_TYPE.JACKPOT, _
                                                           ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_MAJOR_PRIZE) & ":"
        _amount = m_daily_total_type.jackpot_major_prize_amount
        _count = m_daily_total_type.jackpot_major_prize_count

        If Me.m_is_tito_mode Then
          _base_amount = m_daily_total_type.jackpot_major_prize_base_amount
          _rest_amount = m_daily_total_type.jackpot_major_prize_rest_amount
          _retention_amount = m_daily_total_type.jackpot_major_prize_retention_amount
          _paid_amount = m_daily_total_type.jackpot_major_prize_paid_amount
        End If

        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01
        _daily = True
        _type_handpay = HANDPAY_TYPE.JACKPOT + ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT_MAJOR_PRIZE

      Case ENUM_GRID_ROW_TYPE.DAILY_JACKPOT_SITE_TOTAL
        ' Site JackPot
        _str_total = WSI.Common.Handpays.HandpayTypeString(HANDPAY_TYPE.SITE_JACKPOT, _
                                                           ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_LEVEL_NOTHING) & ":"
        _amount = m_daily_total_type.site_jackpot_amount
        _count = m_daily_total_type.site_jackpot_count

        If Me.m_is_tito_mode Then
          _base_amount = m_daily_total_type.site_jackpot_base_amount
          _rest_amount = m_daily_total_type.site_jackpot_rest_amount
          _retention_amount = m_daily_total_type.site_jackpot_retention_amount
          _paid_amount = m_daily_total_type.site_jackpot_paid_amount
        End If

        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01
        _daily = True
        _type_handpay = HANDPAY_TYPE.SITE_JACKPOT

      Case ENUM_GRID_ROW_TYPE.DAILY_ORPHAN_CREDITS_TOTAL
        ' Orphan Credits (Cr�ditos abandonados)
        _str_total = WSI.Common.Handpays.HandpayTypeString(HANDPAY_TYPE.ORPHAN_CREDITS, _
                                                           ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_LEVEL_NOTHING) & ":"
        _amount = m_daily_total_type.orphan_credits_amount
        _count = m_daily_total_type.orphan_credits_count

        If Me.m_is_tito_mode Then
          _base_amount = m_daily_total_type.orphan_credits_base_amount
          _rest_amount = m_daily_total_type.orphan_credits_rest_amount
          _retention_amount = m_daily_total_type.orphan_credits_retention_amount
          _paid_amount = m_daily_total_type.orphan_credits_paid_amount
        End If

        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01
        _daily = True
        _type_handpay = HANDPAY_TYPE.ORPHAN_CREDITS

      Case ENUM_GRID_ROW_TYPE.DAILY_MANUAL_TOTAL
        ' Manual
        _str_total = WSI.Common.Handpays.HandpayTypeString(HANDPAY_TYPE.MANUAL, _
                                                           ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_LEVEL_NOTHING) & ":"
        _amount = m_daily_total_type.manual_amount
        _count = m_daily_total_type.manual_count

        If Me.m_is_tito_mode Then
          _base_amount = m_daily_total_type.manual_base_amount
          _rest_amount = m_daily_total_type.manual_rest_amount
          _retention_amount = m_daily_total_type.manual_retention_amount
          _paid_amount = m_daily_total_type.manual_paid_amount
        End If

        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01
        _daily = True
        _type_handpay = HANDPAY_TYPE.MANUAL

      Case ENUM_GRID_ROW_TYPE.DAILY_MANUAL_JACKPOT_TOTAL
        ' Manual JackPot
        _str_total = GLB_NLS_GUI_INVOICING.GetString(330) & " - " & _
                     WSI.Common.Handpays.HandpayTypeString(HANDPAY_TYPE.MANUAL_JACKPOT, _
                                                           ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_LEVEL_NOTHING) & ":"
        _amount = m_daily_total_type.manual_jackpot_amount
        _count = m_daily_total_type.manual_jackpot_count

        If Me.m_is_tito_mode Then
          _base_amount = m_daily_total_type.manual_jackpot_base_amount
          _rest_amount = m_daily_total_type.manual_jackpot_rest_amount
          _retention_amount = m_daily_total_type.manual_jackpot_retention_amount
          _paid_amount = m_daily_total_type.manual_jackpot_paid_amount
        End If

        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01
        _daily = True
        _type_handpay = HANDPAY_TYPE.MANUAL_JACKPOT + ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT

      Case ENUM_GRID_ROW_TYPE.DAILY_MANUAL_JACKPOT_PROGRESSIVE_TOTAL
        ' Manual Progressive JackPot
        _str_total = GLB_NLS_GUI_INVOICING.GetString(330) & " - " & _
                     WSI.Common.Handpays.HandpayTypeString(HANDPAY_TYPE.MANUAL_JACKPOT, _
                                                           ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_PROGRESSIVE_LEVEL_START) & ":"
        _amount = m_daily_total_type.manual_progressive_jackpot_amount
        _count = m_daily_total_type.manual_progressive_jackpot_count

        If Me.m_is_tito_mode Then
          _base_amount = m_daily_total_type.manual_progressive_jackpot_base_amount
          _rest_amount = m_daily_total_type.manual_progressive_jackpot_rest_amount
          _retention_amount = m_daily_total_type.manual_progressive_jackpot_retention_amount
          _paid_amount = m_daily_total_type.manual_progressive_jackpot_paid_amount
        End If

        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01
        _daily = True
        _type_handpay = HANDPAY_TYPE.MANUAL_JACKPOT + ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT_PROGRESSIVE

      Case ENUM_GRID_ROW_TYPE.DAILY_MANUAL_JACKPOT_MAJOR_PRIZE_TOTAL
        ' Manual JackPot Top Win
        _str_total = GLB_NLS_GUI_INVOICING.GetString(330) & " - " & _
                     WSI.Common.Handpays.HandpayTypeString(HANDPAY_TYPE.MANUAL_JACKPOT, _
                                                           ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_MAJOR_PRIZE) & ":"
        _amount = m_daily_total_type.manual_jackpot_major_prize_amount
        _count = m_daily_total_type.manual_jackpot_major_prize_count

        If Me.m_is_tito_mode Then
          _base_amount = m_daily_total_type.manual_jackpot_major_prize_base_amount
          _rest_amount = m_daily_total_type.manual_jackpot_major_prize_rest_amount
          _retention_amount = m_daily_total_type.manual_jackpot_major_prize_retention_amount
          _paid_amount = m_daily_total_type.manual_jackpot_major_prize_paid_amount
        End If

        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01
        _daily = True
        _type_handpay = HANDPAY_TYPE.MANUAL_JACKPOT + ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT_MAJOR_PRIZE

      Case ENUM_GRID_ROW_TYPE.DAILY_MANUAL_CANCELLED_CREDITS_TOTAL
        ' Manual Cancelled Credits
        _str_total = GLB_NLS_GUI_INVOICING.GetString(330) & " - " & _
                     WSI.Common.Handpays.HandpayTypeString(HANDPAY_TYPE.MANUAL_CANCELLED_CREDITS, _
                                                           ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_LEVEL_NOTHING) & ":"
        _amount = m_daily_total_type.manual_cancelled_credits_amount
        _count = m_daily_total_type.manual_cancelled_credits_count

        If Me.m_is_tito_mode Then
          _base_amount = m_daily_total_type.manual_cancelled_credits_base_amount
          _rest_amount = m_daily_total_type.manual_cancelled_credits_rest_amount
          _retention_amount = m_daily_total_type.manual_cancelled_credits_retention_amount
          _paid_amount = m_daily_total_type.manual_cancelled_credits_paid_amount
        End If

        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01
        _daily = True
        _type_handpay = HANDPAY_TYPE.MANUAL_CANCELLED_CREDITS

      Case ENUM_GRID_ROW_TYPE.DAILY_MANUAL_OTHERS_TOTAL
        'Manual Others (Manual - Manual)
        _str_total = GLB_NLS_GUI_INVOICING.GetString(330) & " - " & _
                     WSI.Common.Handpays.HandpayTypeString(HANDPAY_TYPE.MANUAL_OTHERS, _
                                                           ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_LEVEL_NOTHING) & ":"
        _amount = m_daily_total_type.manual_others_amount
        _count = m_daily_total_type.manual_others_count

        If Me.m_is_tito_mode Then
          _base_amount = m_daily_total_type.manual_others_base_amount
          _rest_amount = m_daily_total_type.manual_others_rest_amount
          _retention_amount = m_daily_total_type.manual_others_retention_amount
          _paid_amount = m_daily_total_type.manual_others_paid_amount
        End If

        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01
        _daily = True
        _type_handpay = HANDPAY_TYPE.MANUAL_OTHERS

      Case ENUM_GRID_ROW_TYPE.DAILY_TOTAL
        _str_total_descrip = String.Empty
        _str_total = GLB_NLS_GUI_INVOICING.GetString(205)
        _amount = m_daily_total_type.total_amount_type
        _count = m_daily_total_type.total_count_type

        If Me.m_is_tito_mode Then
          _base_amount = m_daily_total_type.total_base_amount_type
          _rest_amount = m_daily_total_type.total_rest_amount_type
          _retention_amount = m_daily_total_type.total_retention_amount_type
          _paid_amount = m_daily_total_type.total_paid_amount_type
        End If

        _color = ENUM_GUI_COLOR.GUI_COLOR_OCHRE_00
        _daily = True

        ' **Global Totals
      Case ENUM_GRID_ROW_TYPE.CANCELLED_CREDITS_TOTAL
        ' Cancelled Credits
        _str_total = WSI.Common.Handpays.HandpayTypeString(HANDPAY_TYPE.CANCELLED_CREDITS, _
                                                           ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_LEVEL_NOTHING) & ":"
        _amount = m_global_total_type.cancelled_credits_amount
        _count = m_global_total_type.cancelled_credits_count

        If Me.m_is_tito_mode Then
          _base_amount = m_global_total_type.cancelled_credits_base_amount
          _rest_amount = m_global_total_type.cancelled_credits_rest_amount
          _retention_amount = m_global_total_type.cancelled_credits_retention_amount
          _paid_amount = m_global_total_type.cancelled_credits_paid_amount
        End If

        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
        _daily = False
        _type_handpay = HANDPAY_TYPE.CANCELLED_CREDITS

      Case ENUM_GRID_ROW_TYPE.JACKPOT_TOTAL
        ' JackPot
        _str_total = WSI.Common.Handpays.HandpayTypeString(HANDPAY_TYPE.JACKPOT, _
                                                           ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_LEVEL_NOTHING) & ":"
        _amount = m_global_total_type.jackpot_amount
        _count = m_global_total_type.jackpot_count

        If Me.m_is_tito_mode Then
          _base_amount = m_global_total_type.jackpot_base_amount
          _rest_amount = m_global_total_type.jackpot_rest_amount
          _retention_amount = m_global_total_type.jackpot_retention_amount
          _paid_amount = m_global_total_type.jackpot_paid_amount
        End If

        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
        _daily = False
        _type_handpay = HANDPAY_TYPE.JACKPOT + ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT

      Case ENUM_GRID_ROW_TYPE.JACKPOT_PROGRESSIVE_TOTAL
        'Progressive JackPot
        _str_total = WSI.Common.Handpays.HandpayTypeString(HANDPAY_TYPE.JACKPOT, _
                                                           ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_PROGRESSIVE_LEVEL_START) & ":"
        _amount = m_global_total_type.jackpot_progressive_amount
        _count = m_global_total_type.jackpot_progressive_count

        If Me.m_is_tito_mode Then
          _base_amount = m_global_total_type.jackpot_progressive_base_amount
          _rest_amount = m_global_total_type.jackpot_progressive_rest_amount
          _retention_amount = m_global_total_type.jackpot_progressive_retention_amount
          _paid_amount = m_global_total_type.jackpot_progressive_paid_amount
        End If

        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
        _daily = False
        _type_handpay = HANDPAY_TYPE.JACKPOT + ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT_PROGRESSIVE

      Case ENUM_GRID_ROW_TYPE.JACKPOT_MAJOR_PRIZE_TOTAL
        ' Jackpot Top Win
        _str_total = WSI.Common.Handpays.HandpayTypeString(HANDPAY_TYPE.MANUAL_JACKPOT, _
                                                           ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_MAJOR_PRIZE) & ":"

        _amount = m_global_total_type.jackpot_major_prize_amount
        _count = m_global_total_type.jackpot_major_prize_count

        If Me.m_is_tito_mode Then
          _base_amount = m_global_total_type.jackpot_major_prize_base_amount
          _rest_amount = m_global_total_type.jackpot_major_prize_rest_amount
          _retention_amount = m_global_total_type.jackpot_major_prize_retention_amount
          _paid_amount = m_global_total_type.jackpot_major_prize_paid_amount
        End If

        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
        _daily = False
        _type_handpay = HANDPAY_TYPE.JACKPOT + ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT_MAJOR_PRIZE

      Case ENUM_GRID_ROW_TYPE.JACKPOT_SITE_TOTAL
        ' Site JackPot
        _str_total = WSI.Common.Handpays.HandpayTypeString(HANDPAY_TYPE.SITE_JACKPOT, _
                                                           ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_LEVEL_NOTHING) & ":"
        _amount = m_global_total_type.site_jackpot_amount
        _count = m_global_total_type.site_jackpot_count

        If Me.m_is_tito_mode Then
          _base_amount = m_global_total_type.site_jackpot_base_amount
          _rest_amount = m_global_total_type.site_jackpot_rest_amount
          _retention_amount = m_global_total_type.site_jackpot_retention_amount
          _paid_amount = m_global_total_type.site_jackpot_paid_amount
        End If

        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
        _daily = False
        _type_handpay = HANDPAY_TYPE.SITE_JACKPOT

      Case ENUM_GRID_ROW_TYPE.ORPHAN_CREDITS_TOTAL
        ' Orphan Credits (Cr�ditos abandonados)
        _str_total = WSI.Common.Handpays.HandpayTypeString(HANDPAY_TYPE.ORPHAN_CREDITS, _
                                                           ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_LEVEL_NOTHING) & ":"
        _amount = m_global_total_type.orphan_credits_amount
        _count = m_global_total_type.orphan_credits_count

        If Me.m_is_tito_mode Then
          _base_amount = m_global_total_type.orphan_credits_base_amount
          _rest_amount = m_global_total_type.orphan_credits_rest_amount
          _retention_amount = m_global_total_type.orphan_credits_retention_amount
          _paid_amount = m_global_total_type.site_jackpot_paid_amount
        End If

        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
        _daily = False
        _type_handpay = HANDPAY_TYPE.ORPHAN_CREDITS

      Case ENUM_GRID_ROW_TYPE.MANUAL_TOTAL
        ' Manual
        _str_total = WSI.Common.Handpays.HandpayTypeString(HANDPAY_TYPE.MANUAL, _
                                                           ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_LEVEL_NOTHING) & ":"
        _amount = m_global_total_type.manual_amount
        _count = m_global_total_type.manual_count

        If Me.m_is_tito_mode Then
          _base_amount = m_global_total_type.manual_base_amount
          _rest_amount = m_global_total_type.manual_rest_amount
          _retention_amount = m_global_total_type.manual_retention_amount
          _paid_amount = m_global_total_type.manual_paid_amount
        End If

        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
        _daily = False
        _type_handpay = HANDPAY_TYPE.MANUAL

      Case ENUM_GRID_ROW_TYPE.MANUAL_JACKPOT_TOTAL
        ' Manual JackPot
        _str_total = GLB_NLS_GUI_INVOICING.GetString(330) & " - " & _
                     WSI.Common.Handpays.HandpayTypeString(HANDPAY_TYPE.MANUAL_JACKPOT, _
                                                           ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_LEVEL_NOTHING) & ":"
        _amount = m_global_total_type.manual_jackpot_amount
        _count = m_global_total_type.manual_jackpot_count

        If Me.m_is_tito_mode Then
          _base_amount = m_global_total_type.manual_jackpot_base_amount
          _rest_amount = m_global_total_type.manual_jackpot_rest_amount
          _retention_amount = m_global_total_type.manual_jackpot_retention_amount
          _paid_amount = m_global_total_type.manual_jackpot_paid_amount
        End If

        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
        _daily = False
        _type_handpay = HANDPAY_TYPE.MANUAL_JACKPOT + ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT

      Case ENUM_GRID_ROW_TYPE.MANUAL_JACKPOT_PROGRESSIVE_TOTAL
        ' Manual Progressive JackPot
        _str_total = GLB_NLS_GUI_INVOICING.GetString(330) & " - " & _
                     WSI.Common.Handpays.HandpayTypeString(HANDPAY_TYPE.JACKPOT, _
                                                           ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_PROGRESSIVE_LEVEL_START) & ":"
        _amount = m_global_total_type.manual_progressive_jackpot_amount
        _count = m_global_total_type.manual_progressive_jackpot_count

        If Me.m_is_tito_mode Then
          _base_amount = m_global_total_type.manual_progressive_jackpot_base_amount
          _rest_amount = m_global_total_type.manual_progressive_jackpot_rest_amount
          _retention_amount = m_global_total_type.manual_progressive_jackpot_retention_amount
          _paid_amount = m_global_total_type.manual_progressive_jackpot_paid_amount
        End If

        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
        _daily = False
        _type_handpay = HANDPAY_TYPE.MANUAL_JACKPOT + ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT_PROGRESSIVE

      Case ENUM_GRID_ROW_TYPE.MANUAL_JACKPOT_MAJOR_PRIZE_TOTAL
        ' Manual Jackpot Top Win 
        _str_total = GLB_NLS_GUI_INVOICING.GetString(330) & " - " & _
                     WSI.Common.Handpays.HandpayTypeString(HANDPAY_TYPE.JACKPOT, _
                                                           ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_MAJOR_PRIZE) & ":"
        _amount = m_global_total_type.manual_jackpot_major_prize_amount
        _count = m_global_total_type.manual_jackpot_major_prize_count

        If Me.m_is_tito_mode Then
          _base_amount = m_global_total_type.manual_jackpot_major_prize_base_amount
          _rest_amount = m_global_total_type.manual_jackpot_major_prize_rest_amount
          _retention_amount = m_global_total_type.manual_jackpot_major_prize_retention_amount
          _paid_amount = m_global_total_type.manual_jackpot_major_prize_paid_amount
        End If

        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
        _daily = False
        _type_handpay = HANDPAY_TYPE.MANUAL_JACKPOT + ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT_MAJOR_PRIZE

      Case ENUM_GRID_ROW_TYPE.MANUAL_CANCELLED_CREDITS_TOTAL
        ' Manual Cancelled Credits
        _str_total = GLB_NLS_GUI_INVOICING.GetString(330) & " - " & _
                     WSI.Common.Handpays.HandpayTypeString(HANDPAY_TYPE.MANUAL_CANCELLED_CREDITS, _
                                                           ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_LEVEL_NOTHING) & ":"
        _amount = m_global_total_type.manual_cancelled_credits_amount
        _count = m_global_total_type.manual_cancelled_credits_count

        If Me.m_is_tito_mode Then
          _base_amount = m_global_total_type.manual_cancelled_credits_base_amount
          _rest_amount = m_global_total_type.manual_cancelled_credits_rest_amount
          _retention_amount = m_global_total_type.manual_cancelled_credits_retention_amount
          _paid_amount = m_global_total_type.manual_cancelled_credits_paid_amount
        End If

        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
        _daily = False
        _type_handpay = HANDPAY_TYPE.MANUAL_CANCELLED_CREDITS

      Case ENUM_GRID_ROW_TYPE.MANUAL_OTHERS_TOTAL
        ' Manual Others (Manual - Manual)
        _str_total = GLB_NLS_GUI_INVOICING.GetString(330) & " - " & _
                     WSI.Common.Handpays.HandpayTypeString(HANDPAY_TYPE.MANUAL_OTHERS, _
                                                           ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_LEVEL_NOTHING) & ":"
        _amount = m_global_total_type.manual_others_amount
        _count = m_global_total_type.manual_others_count

        If Me.m_is_tito_mode Then
          _base_amount = m_global_total_type.manual_others_base_amount
          _rest_amount = m_global_total_type.manual_others_rest_amount
          _retention_amount = m_global_total_type.manual_others_retention_amount
          _paid_amount = m_global_total_type.manual_others_paid_amount
        End If

        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
        _daily = False
        _type_handpay = HANDPAY_TYPE.MANUAL_OTHERS

      Case ENUM_GRID_ROW_TYPE.TOTAL
        _str_total_descrip = String.Empty
        _str_total = GLB_NLS_GUI_INVOICING.GetString(205)
        _amount = m_global_total_type.total_amount_type
        _count = m_global_total_type.total_count_type

        If Me.m_is_tito_mode Then
          _base_amount = m_global_total_type.total_base_amount_type
          _rest_amount = m_global_total_type.total_rest_amount_type
          _retention_amount = m_global_total_type.total_retention_amount_type
          _paid_amount = m_global_total_type.total_paid_amount_type
        End If

        _color = ENUM_GUI_COLOR.GUI_COLOR_ORANGE_01
        _daily = False

    End Select

    ' RCI 22-NOV-2011: Add percentage column only in total rows.
    _percent = -1
    Select Case Type
      Case ENUM_GRID_ROW_TYPE.DAILY_TOTAL, ENUM_GRID_ROW_TYPE.TOTAL
        ' Do nothing

      Case ENUM_GRID_ROW_TYPE.DAILY_CANCELLED_CREDITS_TOTAL, _
           ENUM_GRID_ROW_TYPE.DAILY_JACKPOT_TOTAL, _
           ENUM_GRID_ROW_TYPE.DAILY_MANUAL_CANCELLED_CREDITS_TOTAL, _
           ENUM_GRID_ROW_TYPE.DAILY_MANUAL_JACKPOT_TOTAL, _
           ENUM_GRID_ROW_TYPE.DAILY_MANUAL_OTHERS_TOTAL, _
           ENUM_GRID_ROW_TYPE.DAILY_MANUAL_JACKPOT_PROGRESSIVE_TOTAL, _
           ENUM_GRID_ROW_TYPE.DAILY_MANUAL_JACKPOT_MAJOR_PRIZE_TOTAL, _
           ENUM_GRID_ROW_TYPE.DAILY_MANUAL_TOTAL, _
           ENUM_GRID_ROW_TYPE.DAILY_JACKPOT_TOTAL, _
           ENUM_GRID_ROW_TYPE.DAILY_ORPHAN_CREDITS_TOTAL, _
           ENUM_GRID_ROW_TYPE.DAILY_JACKPOT_PROGRESSIVE_TOTAL, _
           ENUM_GRID_ROW_TYPE.DAILY_JACKPOT_MAJOR_PRIZE_TOTAL, _
           ENUM_GRID_ROW_TYPE.DAILY_JACKPOT_SITE_TOTAL

        If m_daily_total_type.total_amount_type > 0 Then
          _percent = Math.Round(100 * (_amount / m_daily_total_type.total_amount_type), 2, MidpointRounding.AwayFromZero)
        End If

      Case ENUM_GRID_ROW_TYPE.CANCELLED_CREDITS_TOTAL, _
           ENUM_GRID_ROW_TYPE.JACKPOT_TOTAL, _
           ENUM_GRID_ROW_TYPE.MANUAL_CANCELLED_CREDITS_TOTAL, _
           ENUM_GRID_ROW_TYPE.MANUAL_JACKPOT_TOTAL, _
           ENUM_GRID_ROW_TYPE.MANUAL_OTHERS_TOTAL, _
           ENUM_GRID_ROW_TYPE.MANUAL_JACKPOT_PROGRESSIVE_TOTAL, _
           ENUM_GRID_ROW_TYPE.MANUAL_JACKPOT_MAJOR_PRIZE_TOTAL, _
           ENUM_GRID_ROW_TYPE.MANUAL_TOTAL, _
           ENUM_GRID_ROW_TYPE.ORPHAN_CREDITS_TOTAL, _
           ENUM_GRID_ROW_TYPE.JACKPOT_PROGRESSIVE_TOTAL, _
           ENUM_GRID_ROW_TYPE.JACKPOT_MAJOR_PRIZE_TOTAL, _
           ENUM_GRID_ROW_TYPE.JACKPOT_SITE_TOTAL

        If m_global_total_type.total_amount_type > 0 Then
          _percent = Math.Round(100 * (_amount / m_global_total_type.total_amount_type), 2, MidpointRounding.AwayFromZero)
        End If
    End Select

    Select Case Type
      Case ENUM_GRID_ROW_TYPE.DAILY_TOTAL, ENUM_GRID_ROW_TYPE.TOTAL
        _add_row = True

      Case Else
        If Me.uc_checked_list_handtypes.SelectedIndexes.Length <= 0 OrElse _
           FindIdTypeHandPay(_type_handpay, Me.uc_checked_list_handtypes.SelectedIndexes) OrElse _
              (FindIdTypeHandPay(WSI.Common.HANDPAY_TYPE.MANUAL_OTHERS, Me.uc_checked_list_handtypes.SelectedIndexes) AndAlso _
              _type_handpay = HANDPAY_TYPE.MANUAL) Then

          _add_row = True
        End If
    End Select

    If _add_row Then
      Me.Grid.AddRow()
      _idx_row = Me.Grid.NumRows - 1

      If _daily Then
        Me.Grid.Cell(_idx_row, GRID_COLUMN_DATETIME).Value = GLB_NLS_GUI_CONFIGURATION.GetString(495) & " " & _
                                                             GUI_FormatDate(m_daily_total_type.day_type, , _
                                                             ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
      End If

      Me.Grid.Cell(_idx_row, GRID_COLUMN_PROVIDER).Value = _str_total_descrip & _str_total
      Me.Grid.Cell(_idx_row, GRID_COLUMN_TERMINAL).Value = GUI_FormatNumber(_count, 0)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_AMOUNT).Value = GUI_FormatCurrency(_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_PERCENT).Value = IIf(_percent = -1, "", GUI_FormatNumber(_percent, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%")

      If Me.m_is_tito_mode Then
        Me.Grid.Cell(_idx_row, GRID_COLUMN_TAX_BASE_AMOUNT).Value = GUI_FormatCurrency(_base_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Me.Grid.Cell(_idx_row, GRID_COLUMN_REST_AMOUNT).Value = GUI_FormatCurrency(_rest_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Me.Grid.Cell(_idx_row, GRID_COLUMN_TAX_AMOUNT).Value = GUI_FormatCurrency(_retention_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Me.Grid.Cell(_idx_row, GRID_COLUMN_PAID_AMOUNT).Value = GUI_FormatCurrency(_paid_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End If

      Me.Grid.Row(_idx_row).BackColor = GetColor(_color)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_ROW_TYPE).Value = ENUM_GRID_ROW_STATUS.TOTAL
    End If
  End Sub ' DrawTotalRowType

  ' PURPOSE : Reset global variable m_daily_total to Day ActualDay and counters to 0.
  '
  '  PARAMS :
  '     - INPUT : 
  '           - Total As TYPE_TOTAL_COUNTERS_STATUS
  '           - ActualDay As Date
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub ResetTotalCountersStatus(ByRef Total As TYPE_TOTAL_COUNTERS_STATUS, ByVal ActualDay As Date)
    Total.day_status = ActualDay

    'Paid
    Total.paid_amount = 0
    Total.paid_base_amount = 0
    Total.paid_rest_amount = 0
    Total.paid_retention_amount = 0
    Total.paid_paid_amount = 0
    Total.paid_count = 0

    'Pending
    Total.pending_amount = 0
    Total.pending_base_amount = 0
    Total.pending_rest_amount = 0
    Total.pending_retention_amount = 0
    Total.pending_paid_amount = 0
    Total.pending_count = 0

    'Expired
    Total.expired_amount = 0
    Total.expired_base_amount = 0
    Total.expired_rest_amount = 0
    Total.expired_retention_amount = 0
    Total.expired_paid_amount = 0
    Total.expired_count = 0

    'Authorized
    Total.authorized_amount = 0
    Total.authorized_base_amount = 0
    Total.authorized_rest_amount = 0
    Total.authorized_retention_amount = 0
    Total.authorized_paid_amount = 0
    Total.authorized_count = 0

    'Voided
    Total.voided_amount = 0
    Total.voided_base_amount = 0
    Total.voided_rest_amount = 0
    Total.voided_retention_amount = 0
    Total.voided_paid_amount = 0
    Total.voided_count = 0

    'Total
    Total.total_amount_status = 0
    Total.total_base_amount_status = 0
    Total.total_rest_amount_status = 0
    Total.total_retention_amount_status = 0
    Total.total_paid_amount_status = 0
    Total.total_count_status = 0

  End Sub ' ResetTotalCountersStatus

  ' PURPOSE : Reset global variable m_daily_total to Day ActualDay and counters to 0.
  '
  '  PARAMS :
  '     - INPUT : 
  '           - Total As TYPE_TOTAL_COUNTERS_TYPE
  '           - ActualDay As Date
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub ResetTotalCountersType(ByRef Total As TYPE_TOTAL_COUNTERS_TYPE, ByVal ActualDay As Date)
    Total.day_type = ActualDay

    ' Cancelled Credits
    Total.cancelled_credits_amount = 0
    Total.cancelled_credits_base_amount = 0
    Total.cancelled_credits_rest_amount = 0
    Total.cancelled_credits_retention_amount = 0
    Total.cancelled_credits_paid_amount = 0
    Total.cancelled_credits_count = 0

    ' Jackpot
    Total.jackpot_amount = 0
    Total.jackpot_base_amount = 0
    Total.jackpot_rest_amount = 0
    Total.jackpot_retention_amount = 0
    Total.jackpot_paid_amount = 0
    Total.jackpot_count = 0

    ' Orphan Credits
    Total.orphan_credits_amount = 0
    Total.orphan_credits_base_amount = 0
    Total.orphan_credits_rest_amount = 0
    Total.orphan_credits_retention_amount = 0
    Total.orphan_credits_paid_amount = 0
    Total.orphan_credits_count = 0

    ' Progressive JackPot
    Total.jackpot_progressive_amount = 0
    Total.jackpot_progressive_base_amount = 0
    Total.jackpot_progressive_rest_amount = 0
    Total.jackpot_progressive_retention_amount = 0
    Total.jackpot_progressive_paid_amount = 0
    Total.jackpot_progressive_count = 0

    ' JackPot Top Win
    Total.jackpot_major_prize_amount = 0
    Total.jackpot_major_prize_base_amount = 0
    Total.jackpot_major_prize_rest_amount = 0
    Total.jackpot_major_prize_retention_amount = 0
    Total.jackpot_major_prize_paid_amount = 0
    Total.jackpot_major_prize_count = 0

    ' Manual
    Total.manual_amount = 0
    Total.manual_base_amount = 0
    Total.manual_rest_amount = 0
    Total.manual_retention_amount = 0
    Total.manual_paid_amount = 0
    Total.manual_count = 0

    ' Manual Cancelled Credits
    Total.manual_cancelled_credits_amount = 0
    Total.manual_cancelled_credits_base_amount = 0
    Total.manual_cancelled_credits_rest_amount = 0
    Total.manual_cancelled_credits_retention_amount = 0
    Total.manual_cancelled_credits_paid_amount = 0
    Total.manual_cancelled_credits_count = 0

    ' Manual JackPot
    Total.manual_jackpot_amount = 0
    Total.manual_jackpot_base_amount = 0
    Total.manual_jackpot_rest_amount = 0
    Total.manual_jackpot_retention_amount = 0
    Total.manual_jackpot_paid_amount = 0
    Total.manual_jackpot_count = 0

    ' Manual Others
    Total.manual_others_amount = 0
    Total.manual_others_base_amount = 0
    Total.manual_others_rest_amount = 0
    Total.manual_others_retention_amount = 0
    Total.manual_others_paid_amount = 0
    Total.manual_others_count = 0

    ' Manual Progressive JackPot
    Total.manual_progressive_jackpot_amount = 0
    Total.manual_progressive_jackpot_base_amount = 0
    Total.manual_progressive_jackpot_rest_amount = 0
    Total.manual_progressive_jackpot_retention_amount = 0
    Total.manual_progressive_jackpot_paid_amount = 0
    Total.manual_progressive_jackpot_count = 0

    ' Manual JackPot Top Win
    Total.manual_jackpot_major_prize_amount = 0
    Total.manual_jackpot_major_prize_base_amount = 0
    Total.manual_jackpot_major_prize_rest_amount = 0
    Total.manual_jackpot_major_prize_retention_amount = 0
    Total.manual_jackpot_major_prize_paid_amount = 0
    Total.manual_jackpot_major_prize_count = 0

    ' Site JackPot
    Total.site_jackpot_amount = 0
    Total.site_jackpot_base_amount = 0
    Total.site_jackpot_rest_amount = 0
    Total.site_jackpot_retention_amount = 0
    Total.site_jackpot_paid_amount = 0
    Total.site_jackpot_count = 0

    ' Totals
    Total.total_amount_type = 0
    Total.total_base_amount_type = 0
    Total.total_rest_amount_type = 0
    Total.total_retention_amount_type = 0
    Total.total_paid_amount_type = 0
    Total.total_count_type = 0

  End Sub ' ResetTotalCountersType

  ' PURPOSE : Update totals depending of Handpay status (pending, paid, etc.)
  '
  '  PARAMS :
  '     - INPUT :
  '           - Total As TYPE_TOTAL_COUNTERS
  '           - Amount As Decimal
  '           - Status As HANDPAY_STATUS
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub UpdateTotalsType(ByRef Total As TYPE_TOTAL_COUNTERS_TYPE, _
                               ByVal Type As HANDPAY_TYPE, _
                               ByVal HpLevel As Integer, _
                               ByVal Amount As Decimal, _
                               ByVal BaseAmount As Decimal, _
                               ByVal RestAmount As Decimal, _
                               ByVal RetentionAmount As Decimal, _
                               ByVal PaidAmount As Decimal)

    Select Case Type
      Case HANDPAY_TYPE.CANCELLED_CREDITS
        'Canceled Credits
        Total.cancelled_credits_amount += Amount
        Total.cancelled_credits_count += 1

        If Me.m_is_tito_mode Then
          Total.cancelled_credits_base_amount += BaseAmount
          Total.cancelled_credits_rest_amount += RestAmount
          Total.cancelled_credits_retention_amount += RetentionAmount
          Total.cancelled_credits_paid_amount += PaidAmount
        End If

      Case HANDPAY_TYPE.JACKPOT
        If HpLevel >= ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_PROGRESSIVE_LEVEL_START AndAlso _
           HpLevel <= ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_PROGRESSIVE_LEVEL_END Then

          ' Progresssive Jackpot
          Total.jackpot_progressive_amount += Amount
          Total.jackpot_progressive_count += 1

          If Me.m_is_tito_mode Then
            Total.jackpot_progressive_base_amount += BaseAmount
            Total.jackpot_progressive_rest_amount += RestAmount
            Total.jackpot_progressive_retention_amount += RetentionAmount
            Total.jackpot_progressive_paid_amount += PaidAmount
          End If

        ElseIf HpLevel = ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_MAJOR_PRIZE Then
          ' Jackpot Top Win
          Total.jackpot_major_prize_amount += Amount
          Total.jackpot_major_prize_count += 1

          If Me.m_is_tito_mode Then
            Total.jackpot_major_prize_base_amount += BaseAmount
            Total.jackpot_major_prize_rest_amount += RestAmount
            Total.jackpot_major_prize_retention_amount += RetentionAmount
            Total.jackpot_major_prize_paid_amount += PaidAmount
          End If

        Else
          ' Jackpot
          Total.jackpot_amount += Amount
          Total.jackpot_count += 1

          If Me.m_is_tito_mode Then

            Total.jackpot_base_amount += BaseAmount
            Total.jackpot_rest_amount += RestAmount
            Total.jackpot_retention_amount += RetentionAmount
            Total.jackpot_paid_amount += PaidAmount
          End If
        End If

      Case HANDPAY_TYPE.SITE_JACKPOT
        'Site Jackpot
        Total.site_jackpot_amount += Amount
        Total.site_jackpot_count += 1

        If Me.m_is_tito_mode Then
          Total.site_jackpot_base_amount += BaseAmount
          Total.site_jackpot_rest_amount += RestAmount
          Total.site_jackpot_retention_amount += RetentionAmount
          Total.site_jackpot_paid_amount += PaidAmount
        End If

      Case HANDPAY_TYPE.ORPHAN_CREDITS
        'Orphan credits
        Total.orphan_credits_amount += Amount
        Total.orphan_credits_count += 1

        If Me.m_is_tito_mode Then
          Total.orphan_credits_base_amount += BaseAmount
          Total.orphan_credits_rest_amount += RestAmount
          Total.orphan_credits_retention_amount += RetentionAmount
          Total.orphan_credits_paid_amount += PaidAmount
        End If

      Case HANDPAY_TYPE.MANUAL
        'Manual
        Total.manual_amount += Amount
        Total.manual_count += 1

        If Me.m_is_tito_mode Then
          Total.manual_base_amount += BaseAmount
          Total.manual_rest_amount += RestAmount
          Total.manual_retention_amount += RetentionAmount
          Total.manual_paid_amount += PaidAmount
        End If

      Case HANDPAY_TYPE.MANUAL_JACKPOT
        If HpLevel >= ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_PROGRESSIVE_LEVEL_START AndAlso _
           HpLevel <= ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_PROGRESSIVE_LEVEL_END Then

          ' Manual Progressive Jackpot
          Total.manual_progressive_jackpot_amount += Amount
          Total.manual_progressive_jackpot_count += 1

          If Me.m_is_tito_mode Then
            Total.manual_progressive_jackpot_base_amount += BaseAmount
            Total.manual_progressive_jackpot_rest_amount += RestAmount
            Total.manual_progressive_jackpot_retention_amount += RetentionAmount
            Total.manual_progressive_jackpot_paid_amount += PaidAmount
          End If

        ElseIf HpLevel = ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_MAJOR_PRIZE Then
          ' Manual Jackpot Top Win
          Total.manual_jackpot_major_prize_amount += Amount
          Total.manual_jackpot_major_prize_count += 1

          If Me.m_is_tito_mode Then
            Total.manual_jackpot_major_prize_base_amount += BaseAmount
            Total.manual_jackpot_major_prize_rest_amount += RestAmount
            Total.manual_jackpot_major_prize_retention_amount += RetentionAmount
            Total.manual_jackpot_major_prize_paid_amount += PaidAmount
          End If

        Else
          ' Manual Jackpot 
          Total.manual_jackpot_amount += Amount
          Total.manual_jackpot_count += 1

          If Me.m_is_tito_mode Then
            Total.manual_jackpot_base_amount += BaseAmount
            Total.manual_jackpot_rest_amount += RestAmount
            Total.manual_jackpot_retention_amount += RetentionAmount
            Total.manual_jackpot_paid_amount += PaidAmount
          End If
        End If

      Case HANDPAY_TYPE.MANUAL_CANCELLED_CREDITS
        'Manual Canceled Credits
        Total.manual_cancelled_credits_amount += Amount
        Total.manual_cancelled_credits_count += 1

        If Me.m_is_tito_mode Then
          Total.manual_cancelled_credits_base_amount += BaseAmount
          Total.manual_cancelled_credits_rest_amount += RestAmount
          Total.manual_cancelled_credits_retention_amount += RetentionAmount
          Total.manual_cancelled_credits_paid_amount += PaidAmount
        End If

      Case HANDPAY_TYPE.MANUAL_OTHERS
        'Manual Others 
        Total.manual_others_amount += Amount
        Total.manual_others_count += 1

        If Me.m_is_tito_mode Then
          Total.manual_others_base_amount += BaseAmount
          Total.manual_others_rest_amount += RestAmount
          Total.manual_others_retention_amount += RetentionAmount
          Total.manual_others_paid_amount += PaidAmount
        End If

    End Select

    Total.total_amount_type = Total.total_amount_type + Amount
    Total.total_count_type = Total.total_count_type + 1

    If Me.m_is_tito_mode Then
      Total.total_base_amount_type += BaseAmount
      Total.total_rest_amount_type += RestAmount
      Total.total_retention_amount_type += RetentionAmount
      Total.total_paid_amount_type += PaidAmount
    End If

  End Sub ' UpdateTotalsType

  ' PURPOSE : Update totals depending of Handpay type (jackpot, progressive jackpot, cancelled credits, etc.)
  '
  '  PARAMS :
  '     - INPUT :
  '           - Total As TYPE_TOTAL_COUNTERS
  '           - Amount As Decimal
  '           - Status As HANDPAY_TYPE
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub UpdateTotalsStatus(ByRef Total As TYPE_TOTAL_COUNTERS_STATUS, _
                                 ByVal Status As HANDPAY_TYPE, _
                                 ByVal Amount As Decimal, _
                                 ByVal BaseAmount As Decimal, _
                                 ByVal RestAmount As Decimal, _
                                 ByVal RetentionAmount As Decimal, _
                                 ByVal PaidAmount As Decimal)

    Select Case Status
      Case HANDPAY_STATUS.PAID
        'Paid
        Total.paid_amount = Total.paid_amount + Amount
        Total.paid_count = Total.paid_count + 1

        If Me.m_is_tito_mode Then
          Total.paid_base_amount += BaseAmount
          Total.paid_rest_amount += RestAmount
          Total.paid_retention_amount += RetentionAmount
          Total.paid_paid_amount += PaidAmount
        End If

      Case HANDPAY_STATUS.PENDING
        'Pending
        Total.pending_amount = Total.pending_amount + Amount
        Total.pending_count = Total.pending_count + 1

        If Me.m_is_tito_mode Then
          Total.pending_base_amount += BaseAmount
          Total.pending_rest_amount += RestAmount
          Total.pending_retention_amount += RetentionAmount
          Total.pending_paid_amount += PaidAmount
        End If

      Case HANDPAY_STATUS.EXPIRED
        'Expired (Caducado)
        Total.expired_amount = Total.expired_amount + Amount
        Total.expired_count = Total.expired_count + 1

        If Me.m_is_tito_mode Then
          Total.expired_base_amount += BaseAmount
          Total.expired_rest_amount += RestAmount
          Total.expired_retention_amount += RetentionAmount
          Total.expired_paid_amount += PaidAmount
        End If

      Case HANDPAY_STATUS.AUTHORIZED
        'Authorized
        Total.authorized_amount = Total.authorized_amount + Amount
        Total.authorized_count = Total.authorized_count + 1

        If Me.m_is_tito_mode Then
          Total.authorized_base_amount += BaseAmount
          Total.authorized_rest_amount += RestAmount
          Total.authorized_retention_amount += RetentionAmount
          Total.authorized_paid_amount += PaidAmount
        End If

      Case HANDPAY_STATUS.VOIDED
        'Voided (anulado)
        Total.voided_amount = Total.voided_amount + Amount
        Total.voided_count = Total.voided_count + 1

        If Me.m_is_tito_mode Then
          Total.voided_base_amount += BaseAmount
          Total.voided_rest_amount += RestAmount
          Total.voided_retention_amount += RetentionAmount
          Total.voided_paid_amount += PaidAmount
        End If

    End Select

    'Total
    Total.total_amount_status = Total.total_amount_status + Amount
    Total.total_count_status = Total.total_count_status + 1

    If Me.m_is_tito_mode Then
      Total.total_base_amount_status += BaseAmount
      Total.total_rest_amount_status += RestAmount
      Total.total_retention_amount_status += RetentionAmount
      Total.total_paid_amount_status += PaidAmount
    End If
  End Sub ' UpdateTotalsType

  ' PURPOSE: Obtain the list of elements that will populate the 
  '         status filter grid.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - List of elements to be added to the grid
  Private Function GetStatusFilterGridData() As List(Of TYPE_DG_FILTER_ELEMENT)
    ' Dimensioned this way to avoid getting an extra element: VB uses the constant to set the upper value, 
    ' not the number of elements
    Dim _dg_filter_rows_list As List(Of TYPE_DG_FILTER_ELEMENT)
    Dim _row As TYPE_DG_FILTER_ELEMENT

    _dg_filter_rows_list = New List(Of TYPE_DG_FILTER_ELEMENT)

    _row = InitializeDgFilterElement()

    ' Anulado
    ' Pagado
    ' Expirado 
    ' Autorizado
    ' Pendiente

    ' Voided (anulado)
    _row.code_handpay_status = WSI.Common.HANDPAY_STATUS.VOIDED
    _row.code_mask_status = WSI.Common.HANDPAY_STATUS.MASK_STATUS
    _row.description = WSI.Common.Handpays.HandpayStatusString(HANDPAY_STATUS.VOIDED)
    _row.checked_by_default = False
    _dg_filter_rows_list.Add(_row)

    ' Paid (pagado)
    _row.code_handpay_status = WSI.Common.HANDPAY_STATUS.PAID
    _row.code_mask_status = WSI.Common.HANDPAY_STATUS.MASK_STATUS
    _row.description = WSI.Common.Handpays.HandpayStatusString(HANDPAY_STATUS.PAID)
    _row.checked_by_default = False
    _dg_filter_rows_list.Add(_row)

    ' Authorized (autorizado)
    _row.code_handpay_status = WSI.Common.HANDPAY_STATUS.AUTHORIZED
    _row.code_mask_status = WSI.Common.HANDPAY_STATUS.MASK_AUTHORIZATION
    _row.description = WSI.Common.Handpays.HandpayStatusString(HANDPAY_STATUS.AUTHORIZED)
    _row.checked_by_default = False
    _dg_filter_rows_list.Add(_row)

    ' Expired (caducado)
    _row.code_handpay_status = WSI.Common.HANDPAY_STATUS.EXPIRED
    _row.code_mask_status = WSI.Common.HANDPAY_STATUS.MASK_STATUS
    _row.description = WSI.Common.Handpays.HandpayStatusString(HANDPAY_STATUS.EXPIRED)
    _row.checked_by_default = False
    _dg_filter_rows_list.Add(_row)

    ' Pending (pendiente)
    _row.code_handpay_status = WSI.Common.HANDPAY_STATUS.PENDING
    _row.code_mask_status = WSI.Common.HANDPAY_STATUS.MASK_STATUS
    _row.description = WSI.Common.Handpays.HandpayStatusString(HANDPAY_STATUS.PENDING)
    _row.checked_by_default = False
    _dg_filter_rows_list.Add(_row)

    Return _dg_filter_rows_list

  End Function ' GetStatusFilterGridData

  ' PURPOSE : Parse input string, in XML format, and fill internal properties
  '
  '  PARAMS :
  '      - INPUT :
  '        - Reader: loaded XML structure
  '
  '      - OUTPUT :
  '
  ' RETURNS :
  '
  Private Sub LoadXml(ByVal Reader As XmlReader, ByRef ProgressiveGroup As Byte, ByRef Level As Byte)
    Dim _doc As XmlDocument
    Dim _node As XmlNode

    _doc = New XmlDocument()
    _doc.Load(Reader)

    _node = _doc.GetElementsByTagName(HANDPAYS_NODE).Item(0)

    ProgressiveGroup = Byte.Parse(XML.GetAttributeValue(_node, PROGRESSIVE_GROUP_NODE))
    Level = Byte.Parse(XML.GetAttributeValue(_node, LEVEL_NODE))

  End Sub ' LoadXml

  Private Sub GridColumnReIndex()
    TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(m_terminal_report_type)

    GRID_COLUMN_INDEX = 0
    GRID_COLUMN_SITE_ID = 1
    GRID_COLUMN_DATETIME = 2
    GRID_INIT_TERMINAL_DATA = 3
    GRID_COLUMN_PROVIDER = GRID_INIT_TERMINAL_DATA
    GRID_COLUMN_TERMINAL = GRID_INIT_TERMINAL_DATA + 1
    GRID_COLUMN_AMT0 = TERMINAL_DATA_COLUMNS + 2
    GRID_COLUMN_AMOUNT = TERMINAL_DATA_COLUMNS + 3
    GRID_COLUMN_PERCENT = TERMINAL_DATA_COLUMNS + 4

    GRID_COLUMN_TAX_BASE_AMOUNT = TERMINAL_DATA_COLUMNS + 5
    GRID_COLUMN_REST_AMOUNT = TERMINAL_DATA_COLUMNS + 6
    GRID_COLUMN_TAX_AMOUNT = TERMINAL_DATA_COLUMNS + 7
    GRID_COLUMN_PAID_AMOUNT = TERMINAL_DATA_COLUMNS + 8

    GRID_COLUMN_TYPE = TERMINAL_DATA_COLUMNS + 9
    GRID_COLUMN_GROUP = TERMINAL_DATA_COLUMNS + 10
    GRID_COLUMN_LEVEL = TERMINAL_DATA_COLUMNS + 11
    GRID_COLUMN_STATUS = TERMINAL_DATA_COLUMNS + 12
    GRID_COLUMN_PROVIDER_ACTUAL = TERMINAL_DATA_COLUMNS + 13
    GRID_COLUMN_TERMINAL_ACTUAL = TERMINAL_DATA_COLUMNS + 14
    GRID_COLUMN_ROW_TYPE = TERMINAL_DATA_COLUMNS + 15

    GRID_COLUMN_HP_COMMENT = TERMINAL_DATA_COLUMNS + 16

    GRID_COLUMN_REASON = TERMINAL_DATA_COLUMNS + 17
    GRID_COLUMN_COMMENT = TERMINAL_DATA_COLUMNS + 18

    GRID_COLUMNS = TERMINAL_DATA_COLUMNS + 19
  End Sub

  ' PURPOSE : Check if element exists in the grid 'status-handpay'
  '
  '  PARAMS :
  '      - INPUT :
  '        - DgFilter : TYPE_DG_FILTER_ELEMENT
  '
  '      - OUTPUT :
  '
  ' RETURNS : Boolean (true if exists, false if not)
  '
  Private Function FindIdStatusHandPay(ByVal DgFilter As TYPE_DG_FILTER_ELEMENT) As Boolean
    Return (DgFilter.code_handpay_status = m_id_status_find)
  End Function ' FindIdStatusHandPay

  ' PURPOSE : Check if element exists in the grid 'type-handpay'
  '
  '  PARAMS :
  '      - INPUT :
  '        - DgFilter : TYPE_DG_FILTER_ELEMENT
  '
  '      - OUTPUT :
  '
  ' RETURNS : Boolean (true if exists, false if not)
  '
  Private Function FindIdTypeHandPay(ByVal HandPayId As Long, ByVal ArrayHandPayType() As Integer) As Boolean
    Dim _idx As Integer

    For _idx = 0 To ArrayHandPayType.Length - 1
      If ArrayHandPayType(_idx) = HandPayId Then

        Return True

      End If
    Next

    Return False

  End Function ' FindIdTypeHandPay

  ' PURPOSE : Fill New Control of handpay-Types (NOT USED TEMPORALLY)
  '
  '  PARAMS :
  '      - INPUT :
  '        - DgFilter : TYPE_DG_FILTER_ELEMENT
  '
  '      - OUTPUT :
  '
  ' RETURNS : Boolean (true if exists, false if not)
  '
  Public Sub FillTypesHandPayControl(ByRef HandPayTypeCheckedList As GUI_Controls.uc_checked_list)
    Dim _level As Integer
    Dim _code_group As Integer

    _code_group = -1

    Try
      ' Fill CheckedList control with groups
      Call HandPayTypeCheckedList.Clear()
      Call HandPayTypeCheckedList.ReDraw(False)

      '+CANCELED CREDITS / ORPHAN CREDITS
      _level = 2

      'Cancelled Credits
      HandPayTypeCheckedList.Add(_level, WSI.Common.HANDPAY_TYPE.CANCELLED_CREDITS, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5654))

      'Abandonded credits
      HandPayTypeCheckedList.Add(_level, WSI.Common.HANDPAY_TYPE.ORPHAN_CREDITS, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5655))

      _level = 1
      '+JACKPOT
      HandPayTypeCheckedList.Add(_level, _code_group, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5652))
      'Increase level
      _level += 1

      '   �Jackpot
      HandPayTypeCheckedList.Add(_level, WSI.Common.HANDPAY_TYPE.JACKPOT, _
                                         GLB_NLS_GUI_PLAYER_TRACKING.GetString(5652))
      '   �Progressive
      HandPayTypeCheckedList.Add(_level, WSI.Common.HANDPAY_TYPE.JACKPOT + ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT_PROGRESSIVE, _
                                         GLB_NLS_GUI_PLAYER_TRACKING.GetString(5656))
      '   �Major Prize
      HandPayTypeCheckedList.Add(_level, WSI.Common.HANDPAY_TYPE.JACKPOT + ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT_MAJOR_PRIZE, _
                                         GLB_NLS_GUI_PLAYER_TRACKING.GetString(5657))
      '   �Site Jackpot
      HandPayTypeCheckedList.Add(_level, WSI.Common.HANDPAY_TYPE.SITE_JACKPOT, _
                                         GLB_NLS_GUI_PLAYER_TRACKING.GetString(5658))

      '+MANUAL
      'Initialize level
      _level = 1

      HandPayTypeCheckedList.Add(_level, _code_group, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5653))

      'Increase level
      _level += 1

      '   �Canceled credits
      HandPayTypeCheckedList.Add(_level, WSI.Common.HANDPAY_TYPE.MANUAL_CANCELLED_CREDITS, _
                                         GLB_NLS_GUI_PLAYER_TRACKING.GetString(5654))
      '   +JACKPOT
      
      '     �Jackpot
      HandPayTypeCheckedList.Add(_level, WSI.Common.HANDPAY_TYPE.MANUAL_JACKPOT + ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT, _
                                         GLB_NLS_GUI_PLAYER_TRACKING.GetString(5652))

      '     �Progressive
      HandPayTypeCheckedList.Add(_level, WSI.Common.HANDPAY_TYPE.MANUAL_JACKPOT + ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT_PROGRESSIVE, _
                                         GLB_NLS_GUI_PLAYER_TRACKING.GetString(5652) + " " + GLB_NLS_GUI_PLAYER_TRACKING.GetString(5656))

      '     �Major Prize
      HandPayTypeCheckedList.Add(_level, WSI.Common.HANDPAY_TYPE.MANUAL_JACKPOT + ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT_MAJOR_PRIZE, _
                                         GLB_NLS_GUI_PLAYER_TRACKING.GetString(5652) + " " + GLB_NLS_GUI_PLAYER_TRACKING.GetString(5657))

      
      '   +OTHERS
      HandPayTypeCheckedList.Add(_level, WSI.Common.HANDPAY_TYPE.MANUAL_OTHERS, _
                                         GLB_NLS_GUI_PLAYER_TRACKING.GetString(5659))

      If HandPayTypeCheckedList.Count > 0 Then
        Call HandPayTypeCheckedList.CurrentRow(0)
        Call HandPayTypeCheckedList.ReDraw(True)
        Call HandPayTypeCheckedList.ResizeGrid()
      End If

    Catch ex As Exception

      Debug.WriteLine(ex.Message)

    End Try

  End Sub ' FillTypesHandPayControl

#End Region ' Private Functions

#Region " Handpay Type "

  ' PURPOSE: Initialize the types filter grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub InitTypesFilter()
    ' FILTER by HandPay Type 

    ' Reset dg selections
    Call FillTypesHandPayControl(Me.uc_checked_list_handtypes)

    ' FILTER by HandPay status
    Me.opt_all_status.Checked = True
    Me.dg_filter_status.Enabled = True

    ' Reset dg selections
    Call FillTypesFilterGrid(Me.dg_filter_status, GetStatusFilterGridData())

    Me.dg_filter_status.SortGrid(GRID_2_COLUMN_DESC)
    Me.dg_filter_status.Redraw = True
  End Sub ' InitTypesFilter

  ' PURPOSE: Define the layout of the types grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheetFilter(ByRef DgFilter As uc_grid, ByVal ColumnDescriptioWidth As Integer)
    With DgFilter
      Call .Init(GRID_2_COLUMNS, GRID_2_HEADER_ROWS, True)

      .Counter(0).Visible = False
      .Sortable = False

      ' Hidden columns
      .Column(GRID_2_COLUMN_CODE).Header.Text = ""
      .Column(GRID_2_COLUMN_CODE).WidthFixed = 0

      .Column(GRID_2_COLUMN_CODE_HANDPAY_STATUS).Header.Text = ""
      .Column(GRID_2_COLUMN_CODE_HANDPAY_STATUS).WidthFixed = 0

      .Column(GRID_2_COLUMN_CODE_HANDPAY_MASK_STATUS).Header.Text = ""
      .Column(GRID_2_COLUMN_CODE_HANDPAY_MASK_STATUS).WidthFixed = 0

      ' GRID_COL_CHECKBOX
      .Column(GRID_2_COLUMN_CHECKED).Header.Text = ""
      .Column(GRID_2_COLUMN_CHECKED).WidthFixed = 400
      .Column(GRID_2_COLUMN_CHECKED).Width = 400
      .Column(GRID_2_COLUMN_CHECKED).Fixed = True
      .Column(GRID_2_COLUMN_CHECKED).Editable = True
      .Column(GRID_2_COLUMN_CHECKED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX

      ' GRID_COL_DESCRIPTION
      .Column(GRID_2_COLUMN_DESC).Header.Text = ""
      .Column(GRID_2_COLUMN_DESC).Width = ColumnDescriptioWidth
      .Column(GRID_2_COLUMN_DESC).WidthFixed = ColumnDescriptioWidth
      .Column(GRID_2_COLUMN_DESC).Fixed = True
      .Column(GRID_2_COLUMN_DESC).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    End With

    LockColumnsResize()

  End Sub 'GUI_StyleSheetTypes

  ' PURPOSE: Lock the column resize
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub LockColumnsResize()

    For Each _control As Object In dg_filter_status.Controls
      If TypeOf (_control) Is AxVSFlex8.AxVSFlexGrid Then
        Dim _grid As AxVSFlex8.AxVSFlexGrid = CType(_control, AxVSFlex8.AxVSFlexGrid)

        If Not _grid Is Nothing Then
          _grid.AllowUserResizing = VSFlex8.AllowUserResizeSettings.flexResizeNone
        End If

        Exit For
      End If
    Next

  End Sub ' LockColumnsResize

  ' PURPOSE: Add a new data row the data grid filter
  '
  '  PARAMS:
  '     - INPUT:
  '           - TypeCode: numeric identifier
  '           - TypeDesc: textual description
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub AddOneRowData(ByRef DgGrid As uc_grid, _
                            ByVal TypeCode As String, _
                            ByVal TypeCodeHandPayStatus As WSI.Common.HANDPAY_STATUS, _
                            ByVal TypeCodeHandPayStatusMask As WSI.Common.HANDPAY_STATUS, _
                            ByVal TypeDesc As String, _
                            ByVal Checked As Boolean)

    Dim _prev_redraw As Boolean
    Dim _num_row As Integer

    _prev_redraw = DgGrid.Redraw
    DgGrid.Redraw = False

    _num_row = DgGrid.AddRow()

    DgGrid.Redraw = False

    DgGrid.Cell(_num_row, GRID_2_COLUMN_CHECKED).Value = IIf(Checked, uc_grid.GRID_CHK_CHECKED, uc_grid.GRID_CHK_UNCHECKED)
    DgGrid.Cell(_num_row, GRID_2_COLUMN_CODE).Value = TypeCode
    DgGrid.Cell(_num_row, GRID_2_COLUMN_CODE_HANDPAY_STATUS).Value = TypeCodeHandPayStatus
    DgGrid.Cell(_num_row, GRID_2_COLUMN_CODE_HANDPAY_MASK_STATUS).Value = TypeCodeHandPayStatusMask
    DgGrid.Cell(_num_row, GRID_2_COLUMN_DESC).Value = GRID_2_TAB & TypeDesc

    DgGrid.Redraw = _prev_redraw
  End Sub ' AddOneRowData

  ' PURPOSE: Populate the movement types data grid filter.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub FillTypesFilterGrid(ByRef DgGrid As uc_grid, ByVal DgList As List(Of TYPE_DG_FILTER_ELEMENT))
    Dim _an_option_checked As Boolean
    _an_option_checked = False

    DgGrid.Clear()

    DgGrid.Redraw = False

    Try
      ' Get the filters array and insert it into the grid.
      For Each _element As TYPE_DG_FILTER_ELEMENT In DgList
        _an_option_checked = _an_option_checked OrElse _element.checked_by_default

        Call AddOneRowData(DgGrid, _element.code, _
                           _element.code_handpay_status, _
                           _element.code_mask_status, _
                           _element.description, _
                           _element.checked_by_default)
      Next

      Me.opt_several_status.Checked = _an_option_checked
    Finally
      DgGrid.Redraw = True
    End Try
  End Sub ' FillTypesFilterGrid

  ' PURPOSE: Initialize variables of type: TYPE_DG_FILTER_ELEMENT
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - variable type TYPE_DG_FILTER_ELEMENT
  Private Function InitializeDgFilterElement() As TYPE_DG_FILTER_ELEMENT
    Dim DGFilter As TYPE_DG_FILTER_ELEMENT
    DGFilter.code = String.Empty
    DGFilter.code_handpay_status = HANDPAY_STATUS.PENDING
    DGFilter.code_mask_status = HANDPAY_STATUS.MASK_STATUS
    DGFilter.description = String.Empty

    Return DGFilter
  End Function ' InitializeDgFilterElement

  ' PURPOSE: Get the list of the items selected in the filter data grid
  '          Format list to build query: X, X, X
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String contining id's of the selected rows
  Private Function GetStatusIdListSelected(ByVal DgGrid As uc_grid) As List(Of TYPE_DG_FILTER_ELEMENT)
    Dim _idx_row As Integer

    Dim _list_type_level As New List(Of TYPE_DG_FILTER_ELEMENT)
    Dim _dg_filter As TYPE_DG_FILTER_ELEMENT

    _dg_filter = InitializeDgFilterElement()

    For _idx_row = 0 To DgGrid.NumRows - 1
      If DgGrid.Cell(_idx_row, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        _dg_filter.code_handpay_status = DgGrid.Cell(_idx_row, GRID_2_COLUMN_CODE_HANDPAY_STATUS).Value
        _dg_filter.code_mask_status = DgGrid.Cell(_idx_row, GRID_2_COLUMN_CODE_HANDPAY_MASK_STATUS).Value
        _dg_filter.description = DgGrid.Cell(_idx_row, GRID_2_COLUMN_DESC).Value

        _list_type_level.Add(_dg_filter)
      End If
    Next

    Return _list_type_level

  End Function 'GetStatusIdListSelected

  ' PURPOSE: Check consistency of the data grid filter values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - True: filter values are consistent
  '     - False: filter values are not consistent
  Private Sub CheckStatusFilter()
    Dim _collection_handtypes As List(Of TYPE_DG_FILTER_ELEMENT)
    _collection_handtypes = Nothing

    If Me.opt_several_status.Checked Then
      _collection_handtypes = GetStatusIdListSelected(Me.dg_filter_status)

      If _collection_handtypes.Count <= 0 Then        
        Me.opt_several_status.Checked = False
        Me.opt_all_status.Checked = True

      End If
    End If

  End Sub ' CheckTypeFilter

  ' PURPOSE: Build search component to add to the 'where' clause of the 'select' statement
  '          according to the values selected in the data grid filter
  '
  '  PARAMS:
  '     - INPUT:
  '           - DBField: database field name to use in the search condition
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Search condition to add
  Private Function GetSqlWhereType(ByVal DBField1 As String, ByVal DBField2 As String, ByVal DBField3 As String) As String
    Dim _str_where As String
    Dim _str_where_main As String
    Dim _selected_indexes As Integer()
    Dim _idx As Integer
    Dim _code_handpay As String

    _str_where = String.Empty
    _str_where_main = String.Empty

    ' Operation Code Type Filter
    _selected_indexes = Me.uc_checked_list_handtypes.SelectedIndexes

    If _selected_indexes.Length > 0 Then

      For _idx = 0 To _selected_indexes.Length - 1

        If _selected_indexes(_idx) > -1 Then
          _str_where = String.Empty
          If _str_where_main <> "" Then
            _str_where_main = _str_where_main & " OR "
          End If

          Select Case _selected_indexes(_idx)
            'JACKPOT
            Case WSI.Common.HANDPAY_TYPE.JACKPOT + ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT
              _code_handpay = _selected_indexes(_idx) - ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT

            Case WSI.Common.HANDPAY_TYPE.JACKPOT + ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT_PROGRESSIVE
              _code_handpay = _selected_indexes(_idx) - ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT_PROGRESSIVE

            Case WSI.Common.HANDPAY_TYPE.JACKPOT + ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT_MAJOR_PRIZE
              _code_handpay = _selected_indexes(_idx) - ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT_MAJOR_PRIZE

              'MANUAL
            Case WSI.Common.HANDPAY_TYPE.MANUAL_JACKPOT + ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT
              _code_handpay = _selected_indexes(_idx) - ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT

            Case WSI.Common.HANDPAY_TYPE.MANUAL_JACKPOT + ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT_PROGRESSIVE
              _code_handpay = _selected_indexes(_idx) - ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT_PROGRESSIVE

            Case WSI.Common.HANDPAY_TYPE.MANUAL_JACKPOT + ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT_MAJOR_PRIZE
              _code_handpay = _selected_indexes(_idx) - ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT_MAJOR_PRIZE

            Case WSI.Common.HANDPAY_TYPE.MANUAL_OTHERS
              _code_handpay = WSI.Common.HANDPAY_TYPE.MANUAL & "," & WSI.Common.HANDPAY_TYPE.MANUAL_OTHERS

            Case Else
              _code_handpay = _selected_indexes(_idx)
          End Select

          'HP_TYPE
          Select Case _selected_indexes(_idx)
            Case WSI.Common.HANDPAY_TYPE.MANUAL_OTHERS
              _str_where = _str_where & DBField1 & " IN (" & _code_handpay & ")"

            Case WSI.Common.HANDPAY_TYPE.SITE_JACKPOT
              _str_where = _str_where & " (" & DBField1 & " = " & _code_handpay & _
                           " AND " & DBField3 & " <> " & WSI.Common.HANDPAY_PAYMENT_MODE.BONUSING & ")"

            Case Else
              _str_where = _str_where & DBField1 & " = " & _code_handpay
          End Select

          'HP_LEVEL
          Select Case _selected_indexes(_idx)
            Case (WSI.Common.HANDPAY_TYPE.JACKPOT + ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT), _
                 (WSI.Common.HANDPAY_TYPE.MANUAL_JACKPOT + ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT)

              'Level NOT Between 1-32; NOT = 64; OR IS NULL
              _str_where = _str_where & " AND ( ( (" & DBField2 & " < " & ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_PROGRESSIVE_LEVEL_START & _
                                        " OR       " & DBField2 & " > " & ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_PROGRESSIVE_LEVEL_END & ")" & _
                                        " AND      " & DBField2 & " <> " & ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_MAJOR_PRIZE & ")" & _
                                        " OR      (" & DBField2 & " IS NULL) )"

            Case (WSI.Common.HANDPAY_TYPE.JACKPOT + ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT_PROGRESSIVE), _
                 (WSI.Common.HANDPAY_TYPE.MANUAL_JACKPOT + ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT_PROGRESSIVE)

              'Progressive: Level Between 1-32
              _str_where = _str_where & " AND " & DBField2 & _
                                        " BETWEEN " & ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_PROGRESSIVE_LEVEL_START & _
                                        " AND " & ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_PROGRESSIVE_LEVEL_END

            Case (WSI.Common.HANDPAY_TYPE.JACKPOT + ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT_MAJOR_PRIZE), _
                 (WSI.Common.HANDPAY_TYPE.MANUAL_JACKPOT + ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT_MAJOR_PRIZE)

              'Mayor Award: Level = 64
              _str_where = _str_where & " AND " & DBField2 & _
                                        " = " & ENUM_HP_LEVEL_JACKPOT.HP_JACKPOT_MAJOR_PRIZE
          End Select

          _str_where = "(" & _str_where & ")"
          _str_where_main = _str_where_main & _str_where
        End If
      Next

      If _str_where_main <> "" Then
        _str_where_main = " AND (" & _str_where_main & ")"
      End If
    End If

    Return _str_where_main
  End Function ' GetSqlWhereTypeNEW

  ' PURPOSE: Build search component to add to the 'where' clause of the 'select' statement
  '          according to the values selected in the data grid filter

  '  PARAMS:
  '     - INPUT:
  '           - DBField: database field name to use in the search condition
  '     - OUTPUT:
  '           - None

  ' RETURNS:
  '     - Search condition to add
  Private Function GetSqlWhereStatus(ByVal DBField As String) As String
    Dim _list_dg_filter_element As List(Of TYPE_DG_FILTER_ELEMENT)
    Dim _item_list As TYPE_DG_FILTER_ELEMENT
    Dim _str_where As String
    Dim _types_list_code As WSI.Common.HANDPAY_STATUS
    Dim _code_mask_status As WSI.Common.HANDPAY_STATUS

    '**MP & FJC 17/10/2014**
    'PENDIENTE:
    '	* Pendiente
    '	* No autorizado
    '	* No expirado
    'EXPIRADO:
    '	* Pendiente
    '	* Expirado
    'AUTORIZADO:
    '	* No expirado
    '	* No pendiente
    '	* Autorizado
    'PAGADO
    '	* Pagado
    'INVALIDADO
    '	* Invalidado

    _str_where = ""

    ' Operation Code Type Filter
    If Me.opt_several_status.Checked = True Then
      _list_dg_filter_element = GetStatusIdListSelected(Me.dg_filter_status)

      For Each _item_list In _list_dg_filter_element
        _types_list_code = _item_list.code_handpay_status
        _code_mask_status = _item_list.code_mask_status
        If _str_where <> "" Then _str_where = _str_where & " OR "

        Select Case _types_list_code
          Case WSI.Common.HANDPAY_STATUS.PENDING
            ' Pending
            ' Not authorized
            ' Not expired 
            _str_where = _str_where & "( (" & _
                                          "      NOT (TI_STATUS IS NOT NULL AND TI_STATUS = " & TITO_TICKET_STATUS.EXPIRED & ")                 " & _
                                          "      AND                                                                                            " & _
                                          "      NOT (TI_STATUS IS NULL AND DATEADD (MINUTE, " & m_time_period & ", HP_DATETIME) < GETDATE())   " & _
                                          "      AND                                                                                            " & _
                                          "      NOT " & "  (" & DBField & " & " & WSI.Common.HANDPAY_STATUS.MASK_AUTHORIZATION & " =           " & _
                                                                                   WSI.Common.HANDPAY_STATUS.AUTHORIZED & ") )                  " & _
                                          "      AND                                                                                            " & _
                                          "                 (" & DBField & " & " & Convert.ToInt32(_code_mask_status) & " =                     " & _
                                                                                   Convert.ToInt32(_types_list_code) & ")                       "
            _str_where = _str_where & " ) "

          Case WSI.Common.HANDPAY_STATUS.EXPIRED
            ' Expired
            _str_where = _str_where & "( (" & _
                                      "      (TI_STATUS IS NOT NULL AND TI_STATUS = " & TITO_TICKET_STATUS.EXPIRED & ") OR                      " & _
                                      "      ((TI_STATUS IS NULL AND DATEADD (MINUTE, " & m_time_period & ", HP_DATETIME) < GETDATE()))          " & _
                                      "      AND                                                                                                " & _
                                      "                     (" & DBField & " & " & Convert.ToInt32(WSI.Common.HANDPAY_STATUS.MASK_STATUS) & " = " & _
                                                                                   Convert.ToInt32(WSI.Common.HANDPAY_STATUS.EXPIRED) & "))      "
            _str_where = _str_where & " )                                                                                                       "

          Case WSI.Common.HANDPAY_STATUS.AUTHORIZED
            ' Not Expired
            ' Pending
            ' Authorized
            _str_where = _str_where & "( (" & _
                                      "      NOT (TI_STATUS IS NOT NULL AND TI_STATUS = " & TITO_TICKET_STATUS.EXPIRED & ") AND                 " & _
                                      "      NOT (TI_STATUS IS NULL AND DATEADD (MINUTE, " & m_time_period & ", HP_DATETIME) < GETDATE())       " & _
                                      "      AND                                                                                                " & _
                                      "      (" & DBField & " & " & WSI.Common.HANDPAY_STATUS.MASK_STATUS & " =                                 " & _
                                                                               WSI.Common.HANDPAY_STATUS.PENDING & "))                          " & _
                                      "      AND        (" & DBField & " & " & Convert.ToInt32(_code_mask_status) & " =                         " & _
                                                                               Convert.ToInt32(_types_list_code) & ")                           "
            _str_where = _str_where & " ) "

          Case Else
            ' Paid or Voided
            _str_where = _str_where & " (" & DBField & " & " & Convert.ToInt32(_code_mask_status) & " = " & Convert.ToInt32(_types_list_code) & ")"

        End Select
      Next

      _str_where = " AND (" & _str_where & " ) "
    End If

    Return _str_where
  End Function ' GetSqlWhereStatus

  ' PURPOSE: Return HandPay Status (enum)
  '  PARAMS:
  '     - INPUT:
  '           - HP_Status:  Determines hp_status
  '           - HP_Expired: Calculated field that indicates if handpay has expired
  '     - OUTPUT:
  '           - None
  ' RETURNS:
  '     - HandPay status enum
  Private Function GetHandPayStatus(ByVal HpStatus As Integer, ByVal HpExpired As Integer) As HANDPAY_STATUS
    If Handpays.StatusActivated(HpStatus, HANDPAY_STATUS.VOIDED) Then
      Return HANDPAY_STATUS.VOIDED
    ElseIf Handpays.StatusActivated(HpStatus, HANDPAY_STATUS.PAID) Then
      Return HANDPAY_STATUS.PAID
    ElseIf Handpays.StatusActivated(HpStatus, HANDPAY_STATUS.AUTOMATICALLY_PAID) Then
      Return HANDPAY_STATUS.AUTOMATICALLY_PAID
    ElseIf HpExpired = 1 Then
      Return HANDPAY_STATUS.EXPIRED
    Else
      If Handpays.StatusActivated(HpStatus, HANDPAY_STATUS.EXPIRED) Then
        Return HANDPAY_STATUS.EXPIRED
      ElseIf Handpays.StatusActivated(HpStatus, HANDPAY_STATUS.AUTHORIZED) Then
        Return HANDPAY_STATUS.AUTHORIZED
      ElseIf Handpays.StatusActivated(HpStatus, HANDPAY_STATUS.PENDING) Then
        Return HANDPAY_STATUS.PENDING
      End If
    End If
  End Function ' GetHandPayStatus

#End Region ' Handpay Types

#Region " Events"

  Private Sub dg_filter_status_DataSelectedEvent() Handles dg_filter_status.DataSelectedEvent
    If Me.opt_several_status.Checked = False Then
      Me.opt_several_status.Checked = True
    End If
  End Sub ' dg_filter_status_DataSelectedEvent

  Private Sub dg_filter_status_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer) Handles dg_filter_status.CellDataChangedEvent
    ' Update radio buttons accordingly
    'If Me.opt_one_user.Checked Then
    '  If Me.dg_filter_status.Cell(Row, GRID_2_COLUMN_CODE_HANDPAY_STATUS).Value = HANDPAY_STATUS.PAID Then
    '    Me.dg_filter_status.Cell(Row, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
    '  Else
    '    Select Case Me.dg_filter_status.Cell(Row, GRID_2_COLUMN_CHECKED).Value
    '      Case uc_grid.GRID_CHK_CHECKED
    '        Me.dg_filter_status.Cell(Row, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED_DISABLED

    '      Case uc_grid.GRID_CHK_UNCHECKED
    '        Me.dg_filter_status.Cell(Row, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
    '    End Select
    '  End If
    'End If

    If Me.opt_several_status.Checked = False Then
      Me.opt_several_status.Checked = True
    End If
  End Sub ' dg_filter_status_CellDataChangedEvent

  Private Sub opt_all_status_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_all_status.Click
    Dim _idx As Integer

    For _idx = 0 To dg_filter_status.NumRows - 1
      Me.dg_filter_status.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    Next

  End Sub ' opt_all_types_Click

  Private Sub chk_terminal_location_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminal_location.CheckedChanged
    If chk_terminal_location.Checked Then
      m_terminal_report_type = ReportType.Provider + ReportType.Location
    Else
      m_terminal_report_type = ReportType.Provider
    End If
  End Sub

  ' PURPOSE: Enables the checkbox chk_show_all only when we are filtering by an user.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SwitchCheckShowAllUsers()
    ' El checkbox chk_show_all solo se activara cuando filtremos por un solo usuario.
    chk_show_all.Enabled = opt_one_user.Checked
    If (Not opt_one_user.Checked) Then
      chk_show_all.Checked = False
    End If
  End Sub

  Private Sub opt_one_user_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_one_user.CheckedChanged
    If opt_one_user.Checked Then
      Dim _idx As Integer

      For _idx = 0 To dg_filter_status.NumRows - 1
        If HANDPAY_STATUS.PAID = Me.dg_filter_status.Cell(_idx, GRID_2_COLUMN_CODE_HANDPAY_STATUS).Value Then
          Me.dg_filter_status.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
        Else
          Me.dg_filter_status.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
        End If
      Next

      gb_status.Enabled = False
      Me.opt_several_status.Checked = True

      cmb_user.Enabled = True
    End If

    Call SwitchCheckShowAllUsers()
  End Sub ' opt_one_user_CheckedChanged

  Private Sub opt_all_users_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_all_users.CheckedChanged
    If opt_all_users.Checked Then

      Dim _idx As Integer

      For _idx = 0 To dg_filter_status.NumRows - 1
        Me.dg_filter_status.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
      Next

      gb_status.Enabled = True
      opt_all_status.Checked = True

      cmb_user.Enabled = False
    End If

    Call SwitchCheckShowAllUsers()
  End Sub ' opt_all_users_CheckedChanged

  Private Sub chk_show_all_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_show_all.CheckedChanged
    Call SetCombo(Me.cmb_user, GetSqlUsers())
  End Sub ' chk_show_all_CheckedChanged
#End Region ' Events

End Class ' frm_handpays
