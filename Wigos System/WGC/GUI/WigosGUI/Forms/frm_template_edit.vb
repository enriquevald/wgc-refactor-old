'-------------------------------------------------------------------
' Copyright � 2014 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_template_edit
' DESCRIPTION:   New or Edit template item
' AUTHOR:        Joan Marc Pepi�
' CREATION DATE: 18-JUN-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 18-JUN-2014  JPJ    Initial version.
' 24-OCT-2014  JBC    Fixed Bug WIG-1576: Now form uses correct permission.
' 19-NOV-2014  JPJ    Fixed Bug WIG-1710: Canceling the template name using the form's closing button.
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE

Public Class frm_template_edit
  Inherits frm_base_edit

#Region " Properties "

  Private m_template_name As String
  Private m_canceled As Boolean

  'Indicates the template name
  Public Property TemplateName() As String
    Get
      TemplateName = m_template_name
    End Get

    Set(ByVal value As String)
      m_template_name = value
    End Set
  End Property

  Public Property Canceled() As Boolean
    Get
      Canceled = m_canceled
    End Get

    Set(ByVal value As Boolean)
      m_template_name = value
    End Set
  End Property

#End Region ' Constants

#Region " Constants "
  Private Const LEN_TEMPLATE_NAME As Integer = 50
#End Region ' Constants

#Region " Members "

  Private m_input_template_name As String

#End Region ' Members

#Region " Overrides "

  ' PURPOSE : Initializes the form id.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CAGE_CONTROL

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE : Form controls initialization.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS  :
  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    ' Set form Name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5053) ' Template editor

    ' Delete button is only available whe editing, not while creating
    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False

    ef_template_name.Text = GLB_NLS_GUI_CONTROLS.GetString(323) ' Name

    ef_template_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_TEMPLATE_NAME)

    m_canceled = True

  End Sub ' GUI_InitControls

  ' PURPOSE : Validate the data presented on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    If ef_template_name.Value.Length = 0 Then
      ' 123 "Debe introducir un valor."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_template_name.Text)
      Call ef_template_name.Focus()

      Return False
    End If

    Return True

  End Function ' GUI_IsScreenDataOk

  ' PURPOSE : Set initial data on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

  End Sub ' GUI_SetScreenData

  ' PURPOSE : Get data from the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_GetScreenData()

    m_template_name = ef_template_name.Value

  End Sub ' GUI_GetScreenData

  ' PURPOSE: Manage buttons pressed.
  '
  '  PARAMS:
  '     - INPUT:
  '         - ButtonId: Id. of the button clicked.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CANCEL
        Me.Close()
      Case ENUM_BUTTON.BUTTON_OK
        MyBase.GUI_ButtonClick(ButtonId)
        m_canceled = False
      Case Else
        '
    End Select

  End Sub

  ' PURPOSE : Database overridable operations. 
  '           Define specific DB operation for this form.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    DbStatus = ENUM_STATUS.STATUS_OK

  End Sub ' GUI_DB_Operation

  ' PURPOSE : Database overridable operations. 
  '           Define specific DB operation for this form.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :

  ' GUI_DB_Operation

  ' PURPOSE : Manage permissions.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)
  End Sub ' GUI_Permissions

  ' PURPOSE : Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.ef_template_name

  End Sub ' GUI_SetInitialFocus

  ' PURPOSE : Init form in new mode
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '
  Public Overloads Sub ShowNewItem(ByVal AreaId As Integer)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    DbObjectId = Nothing
    DbStatus = ENUM_STATUS.STATUS_OK

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowNewItem

#End Region ' Overrides
End Class
