<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_terminals_edition
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_terminals_edition))
    Me.cmb_status = New GUI_Controls.uc_combo()
    Me.btn_ok = New GUI_Controls.uc_button()
    Me.btn_cancel = New GUI_Controls.uc_button()
    Me.ef_name = New GUI_Controls.uc_entry_field()
    Me.ef_external_id = New GUI_Controls.uc_entry_field()
    Me.ef_provider_id = New GUI_Controls.uc_entry_field()
    Me.ef_vendor_id = New GUI_Controls.uc_entry_field()
    Me.chk_blocked = New System.Windows.Forms.CheckBox()
    Me.ef_terminal_type_build = New GUI_Controls.uc_entry_field()
    Me.gb_dates = New System.Windows.Forms.GroupBox()
    Me.ef_replacement_date = New GUI_Controls.uc_entry_field()
    Me.ef_creation_date = New GUI_Controls.uc_entry_field()
    Me.lbl_blocked = New System.Windows.Forms.Label()
    Me.lbl_machine_blocked_reason = New System.Windows.Forms.Label()
    Me.chk_machine_blocked = New System.Windows.Forms.CheckBox()
    Me.ef_activation_date = New GUI_Controls.uc_entry_field()
    Me.ef_retirement_date = New GUI_Controls.uc_entry_field()
    Me.dtp_retirement_date = New GUI_Controls.uc_date_picker()
    Me.ef_retirement_requested = New GUI_Controls.uc_entry_field()
    Me.ef_program = New GUI_Controls.uc_entry_field()
    Me.ef_floor_id = New GUI_Controls.uc_entry_field()
    Me.cmb_area = New GUI_Controls.uc_combo()
    Me.cmb_bank = New GUI_Controls.uc_combo()
    Me.ef_smoking = New GUI_Controls.uc_entry_field()
    Me.gb_area_island = New System.Windows.Forms.GroupBox()
    Me.ef_position = New GUI_Controls.uc_entry_field()
    Me.ef_tax_registration = New GUI_Controls.uc_entry_field()
    Me.gb_SAS = New System.Windows.Forms.GroupBox()
    Me.ef_sas_host_id = New GUI_Controls.uc_entry_field()
    Me.gb_serial_number = New System.Windows.Forms.GroupBox()
    Me.btn_reset_serial_number = New GUI_Controls.uc_button()
    Me.ef_configured_serial_number = New GUI_Controls.uc_entry_field()
    Me.ef_reported_serial_number = New GUI_Controls.uc_entry_field()
    Me.ef_accounting_denomination = New GUI_Controls.uc_entry_field()
    Me.gb_asset_number = New System.Windows.Forms.GroupBox()
    Me.btn_reset_asset_number = New GUI_Controls.uc_button()
    Me.ef_configured_asset_number = New GUI_Controls.uc_entry_field()
    Me.ef_reported_asset_number = New GUI_Controls.uc_entry_field()
    Me.cmb_meter_delta = New GUI_Controls.uc_combo()
    Me.lbl_meter_delta = New System.Windows.Forms.Label()
    Me.uc_sas_flags = New GUI_Controls.uc_sas_flags()
    Me.ef_min_denomination = New GUI_Controls.uc_entry_field()
    Me.cmb_provider = New GUI_Controls.uc_combo()
    Me.gb_contract = New System.Windows.Forms.GroupBox()
    Me.cmb_contract_type = New GUI_Controls.uc_combo()
    Me.ef_order_number = New GUI_Controls.uc_entry_field()
    Me.ef_contract_id = New GUI_Controls.uc_entry_field()
    Me.gb_ticket_configuration = New System.Windows.Forms.GroupBox()
    Me.chk_allow_truncate = New System.Windows.Forms.CheckBox()
    Me.lbl_allow_truncate = New System.Windows.Forms.Label()
    Me.ef_min_ticket_in = New GUI_Controls.uc_entry_field()
    Me.lbl_ticket_out_info = New System.Windows.Forms.Label()
    Me.ef_max_ticket_out = New GUI_Controls.uc_entry_field()
    Me.ef_max_ticket_in = New GUI_Controls.uc_entry_field()
    Me.chk_allow_promotional_emission = New System.Windows.Forms.CheckBox()
    Me.chk_allow_cashable_emission = New System.Windows.Forms.CheckBox()
    Me.chk_allow_redemption = New System.Windows.Forms.CheckBox()
    Me.opt_default = New System.Windows.Forms.RadioButton()
    Me.opt_personalized = New System.Windows.Forms.RadioButton()
    Me.gb_signature_validation = New System.Windows.Forms.GroupBox()
    Me.lbl_authentication_status_detail = New System.Windows.Forms.Label()
    Me.lbl_authentication_signature = New System.Windows.Forms.Label()
    Me.lbl_authentication_seed = New System.Windows.Forms.Label()
    Me.lbl_authentication_method = New System.Windows.Forms.Label()
    Me.lbl_authentication_status = New System.Windows.Forms.Label()
    Me.lbl_authentication_last_checked = New System.Windows.Forms.Label()
    Me.ef_machine_id = New GUI_Controls.uc_entry_field()
    Me.ef_theoretical_payout = New GUI_Controls.uc_entry_field()
    Me.cmb_game_type = New GUI_Controls.uc_combo()
    Me.ef_cabinet_type = New GUI_Controls.uc_entry_field()
    Me.ef_jackpot_contribution_pct = New GUI_Controls.uc_entry_field()
    Me.ef_top_award = New GUI_Controls.uc_entry_field()
    Me.ef_max_bet = New GUI_Controls.uc_entry_field()
    Me.gb_denomination = New System.Windows.Forms.GroupBox()
    Me.chk_equity_percentage = New System.Windows.Forms.CheckBox()
    Me.cmb_currency = New GUI_Controls.uc_combo()
    Me.chk_coin_collection = New System.Windows.Forms.CheckBox()
    Me.ef_game_theme = New GUI_Controls.uc_entry_field()
    Me.opt_multi = New System.Windows.Forms.RadioButton()
    Me.opt_single = New System.Windows.Forms.RadioButton()
    Me.cmb_multi_denomination = New GUI_Controls.uc_combo()
    Me.ef_number_lines = New GUI_Controls.uc_entry_field()
    Me.cmb_single_denomination = New GUI_Controls.uc_combo()
    Me.ef_equity_percentage = New GUI_Controls.uc_entry_field()
    Me.gb_communication_type = New System.Windows.Forms.GroupBox()
    Me.cmb_smib_configuration = New GUI_Controls.uc_combo()
    Me.cmb_communication_type = New GUI_Controls.uc_combo()
    Me.ef_brand_code = New GUI_Controls.uc_entry_field()
    Me.ef_model = New GUI_Controls.uc_entry_field()
    Me.ef_manufacture_year = New GUI_Controls.uc_entry_field()
    Me.chk_met_homologated = New System.Windows.Forms.CheckBox()
    Me.ef_bet_code = New GUI_Controls.uc_entry_field()
    Me.gb_3GS = New System.Windows.Forms.GroupBox()
    Me.ef_reported_3gs_serial_number = New GUI_Controls.uc_entry_field()
    Me.uc_ds_manufacturing_date = New GUI_Controls.uc_date_select()
    Me.lbl_date_manufacturing = New System.Windows.Forms.Label()
    Me.panel_data.SuspendLayout()
    Me.gb_dates.SuspendLayout()
    Me.gb_area_island.SuspendLayout()
    Me.gb_SAS.SuspendLayout()
    Me.gb_serial_number.SuspendLayout()
    Me.gb_asset_number.SuspendLayout()
    Me.gb_contract.SuspendLayout()
    Me.gb_ticket_configuration.SuspendLayout()
    Me.gb_signature_validation.SuspendLayout()
    Me.gb_denomination.SuspendLayout()
    Me.gb_communication_type.SuspendLayout()
    Me.gb_3GS.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.lbl_date_manufacturing)
    Me.panel_data.Controls.Add(Me.gb_dates)
    Me.panel_data.Controls.Add(Me.uc_ds_manufacturing_date)
    Me.panel_data.Controls.Add(Me.gb_ticket_configuration)
    Me.panel_data.Controls.Add(Me.gb_denomination)
    Me.panel_data.Controls.Add(Me.gb_communication_type)
    Me.panel_data.Controls.Add(Me.ef_machine_id)
    Me.panel_data.Controls.Add(Me.gb_contract)
    Me.panel_data.Controls.Add(Me.ef_tax_registration)
    Me.panel_data.Controls.Add(Me.gb_area_island)
    Me.panel_data.Controls.Add(Me.ef_program)
    Me.panel_data.Controls.Add(Me.gb_signature_validation)
    Me.panel_data.Controls.Add(Me.ef_terminal_type_build)
    Me.panel_data.Controls.Add(Me.ef_vendor_id)
    Me.panel_data.Controls.Add(Me.ef_external_id)
    Me.panel_data.Controls.Add(Me.ef_name)
    Me.panel_data.Controls.Add(Me.cmb_provider)
    Me.panel_data.Controls.Add(Me.ef_provider_id)
    Me.panel_data.Controls.Add(Me.ef_bet_code)
    Me.panel_data.Controls.Add(Me.ef_manufacture_year)
    Me.panel_data.Controls.Add(Me.ef_model)
    Me.panel_data.Controls.Add(Me.ef_brand_code)
    Me.panel_data.Controls.Add(Me.chk_met_homologated)
    Me.panel_data.Controls.Add(Me.gb_SAS)
    Me.panel_data.Controls.Add(Me.gb_3GS)
    Me.panel_data.Size = New System.Drawing.Size(762, 1047)
    '
    'cmb_status
    '
    Me.cmb_status.AllowUnlistedValues = False
    Me.cmb_status.AutoCompleteMode = False
    Me.cmb_status.IsReadOnly = False
    Me.cmb_status.Location = New System.Drawing.Point(22, 113)
    Me.cmb_status.Name = "cmb_status"
    Me.cmb_status.SelectedIndex = -1
    Me.cmb_status.Size = New System.Drawing.Size(332, 24)
    Me.cmb_status.SufixText = "Sufix Text"
    Me.cmb_status.SufixTextVisible = True
    Me.cmb_status.TabIndex = 1
    Me.cmb_status.TextCombo = Nothing
    Me.cmb_status.TextWidth = 120
    '
    'btn_ok
    '
    Me.btn_ok.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_ok.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.btn_ok.Location = New System.Drawing.Point(0, 12)
    Me.btn_ok.Name = "btn_ok"
    Me.btn_ok.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_ok.Size = New System.Drawing.Size(90, 30)
    Me.btn_ok.TabIndex = 3
    Me.btn_ok.ToolTipped = False
    Me.btn_ok.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_cancel
    '
    Me.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btn_cancel.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.btn_cancel.Location = New System.Drawing.Point(0, 42)
    Me.btn_cancel.Name = "btn_cancel"
    Me.btn_cancel.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_cancel.Size = New System.Drawing.Size(90, 30)
    Me.btn_cancel.TabIndex = 4
    Me.btn_cancel.ToolTipped = False
    Me.btn_cancel.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'ef_name
    '
    Me.ef_name.DoubleValue = 0.0R
    Me.ef_name.IntegerValue = 0
    Me.ef_name.IsReadOnly = False
    Me.ef_name.Location = New System.Drawing.Point(19, 5)
    Me.ef_name.Name = "ef_name"
    Me.ef_name.PlaceHolder = Nothing
    Me.ef_name.ShortcutsEnabled = True
    Me.ef_name.Size = New System.Drawing.Size(354, 24)
    Me.ef_name.SufixText = "Sufix Text"
    Me.ef_name.SufixTextVisible = True
    Me.ef_name.TabIndex = 0
    Me.ef_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_name.TextValue = ""
    Me.ef_name.TextWidth = 110
    Me.ef_name.Value = ""
    Me.ef_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_external_id
    '
    Me.ef_external_id.DoubleValue = 0.0R
    Me.ef_external_id.IntegerValue = 0
    Me.ef_external_id.IsReadOnly = False
    Me.ef_external_id.Location = New System.Drawing.Point(9, 31)
    Me.ef_external_id.Name = "ef_external_id"
    Me.ef_external_id.PlaceHolder = Nothing
    Me.ef_external_id.ShortcutsEnabled = True
    Me.ef_external_id.Size = New System.Drawing.Size(364, 24)
    Me.ef_external_id.SufixText = "Sufix Text"
    Me.ef_external_id.SufixTextVisible = True
    Me.ef_external_id.TabIndex = 1
    Me.ef_external_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_external_id.TextValue = ""
    Me.ef_external_id.TextWidth = 120
    Me.ef_external_id.Value = ""
    Me.ef_external_id.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_provider_id
    '
    Me.ef_provider_id.DoubleValue = 0.0R
    Me.ef_provider_id.IntegerValue = 0
    Me.ef_provider_id.IsReadOnly = False
    Me.ef_provider_id.Location = New System.Drawing.Point(19, 60)
    Me.ef_provider_id.Name = "ef_provider_id"
    Me.ef_provider_id.PlaceHolder = Nothing
    Me.ef_provider_id.ShortcutsEnabled = True
    Me.ef_provider_id.Size = New System.Drawing.Size(354, 24)
    Me.ef_provider_id.SufixText = "Sufix Text"
    Me.ef_provider_id.SufixTextVisible = True
    Me.ef_provider_id.TabIndex = 3
    Me.ef_provider_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_provider_id.TextValue = ""
    Me.ef_provider_id.TextWidth = 110
    Me.ef_provider_id.Value = ""
    Me.ef_provider_id.ValueForeColor = System.Drawing.Color.Blue
    Me.ef_provider_id.Visible = False
    '
    'ef_vendor_id
    '
    Me.ef_vendor_id.AllowDrop = True
    Me.ef_vendor_id.DoubleValue = 0.0R
    Me.ef_vendor_id.IntegerValue = 0
    Me.ef_vendor_id.IsReadOnly = False
    Me.ef_vendor_id.Location = New System.Drawing.Point(19, 86)
    Me.ef_vendor_id.Name = "ef_vendor_id"
    Me.ef_vendor_id.PlaceHolder = Nothing
    Me.ef_vendor_id.ShortcutsEnabled = True
    Me.ef_vendor_id.Size = New System.Drawing.Size(354, 24)
    Me.ef_vendor_id.SufixText = "Sufix Text"
    Me.ef_vendor_id.SufixTextVisible = True
    Me.ef_vendor_id.TabIndex = 4
    Me.ef_vendor_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_vendor_id.TextValue = ""
    Me.ef_vendor_id.TextWidth = 110
    Me.ef_vendor_id.Value = ""
    Me.ef_vendor_id.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_blocked
    '
    Me.chk_blocked.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_blocked.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.chk_blocked.Location = New System.Drawing.Point(123, 203)
    Me.chk_blocked.Name = "chk_blocked"
    Me.chk_blocked.RightToLeft = System.Windows.Forms.RightToLeft.Yes
    Me.chk_blocked.Size = New System.Drawing.Size(191, 18)
    Me.chk_blocked.TabIndex = 5
    Me.chk_blocked.Text = "xBlocked"
    Me.chk_blocked.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'ef_terminal_type_build
    '
    Me.ef_terminal_type_build.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.ef_terminal_type_build.DoubleValue = 0.0R
    Me.ef_terminal_type_build.IntegerValue = 0
    Me.ef_terminal_type_build.IsReadOnly = False
    Me.ef_terminal_type_build.Location = New System.Drawing.Point(19, 115)
    Me.ef_terminal_type_build.Name = "ef_terminal_type_build"
    Me.ef_terminal_type_build.PlaceHolder = Nothing
    Me.ef_terminal_type_build.ShortcutsEnabled = True
    Me.ef_terminal_type_build.Size = New System.Drawing.Size(354, 24)
    Me.ef_terminal_type_build.SufixText = "Sufix Text"
    Me.ef_terminal_type_build.SufixTextVisible = True
    Me.ef_terminal_type_build.TabIndex = 5
    Me.ef_terminal_type_build.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_terminal_type_build.TextValue = ""
    Me.ef_terminal_type_build.TextWidth = 110
    Me.ef_terminal_type_build.Value = ""
    Me.ef_terminal_type_build.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_dates
    '
    Me.gb_dates.Controls.Add(Me.ef_replacement_date)
    Me.gb_dates.Controls.Add(Me.ef_creation_date)
    Me.gb_dates.Controls.Add(Me.lbl_blocked)
    Me.gb_dates.Controls.Add(Me.lbl_machine_blocked_reason)
    Me.gb_dates.Controls.Add(Me.chk_machine_blocked)
    Me.gb_dates.Controls.Add(Me.chk_blocked)
    Me.gb_dates.Controls.Add(Me.ef_activation_date)
    Me.gb_dates.Controls.Add(Me.ef_retirement_date)
    Me.gb_dates.Controls.Add(Me.dtp_retirement_date)
    Me.gb_dates.Controls.Add(Me.ef_retirement_requested)
    Me.gb_dates.Controls.Add(Me.cmb_status)
    Me.gb_dates.Location = New System.Drawing.Point(4, 489)
    Me.gb_dates.Name = "gb_dates"
    Me.gb_dates.Size = New System.Drawing.Size(375, 267)
    Me.gb_dates.TabIndex = 15
    Me.gb_dates.TabStop = False
    Me.gb_dates.Text = "xStatus"
    '
    'ef_replacement_date
    '
    Me.ef_replacement_date.DoubleValue = 0.0R
    Me.ef_replacement_date.IntegerValue = 0
    Me.ef_replacement_date.IsReadOnly = False
    Me.ef_replacement_date.Location = New System.Drawing.Point(22, 81)
    Me.ef_replacement_date.Name = "ef_replacement_date"
    Me.ef_replacement_date.PlaceHolder = Nothing
    Me.ef_replacement_date.ShortcutsEnabled = True
    Me.ef_replacement_date.Size = New System.Drawing.Size(332, 24)
    Me.ef_replacement_date.SufixText = "Sufix Text"
    Me.ef_replacement_date.SufixTextVisible = True
    Me.ef_replacement_date.TabIndex = 9
    Me.ef_replacement_date.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_replacement_date.TextValue = ""
    Me.ef_replacement_date.TextWidth = 120
    Me.ef_replacement_date.Value = ""
    Me.ef_replacement_date.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_creation_date
    '
    Me.ef_creation_date.DoubleValue = 0.0R
    Me.ef_creation_date.IntegerValue = 0
    Me.ef_creation_date.IsReadOnly = False
    Me.ef_creation_date.Location = New System.Drawing.Point(22, 20)
    Me.ef_creation_date.Name = "ef_creation_date"
    Me.ef_creation_date.PlaceHolder = Nothing
    Me.ef_creation_date.ShortcutsEnabled = True
    Me.ef_creation_date.Size = New System.Drawing.Size(332, 24)
    Me.ef_creation_date.SufixText = "Sufix Text"
    Me.ef_creation_date.SufixTextVisible = True
    Me.ef_creation_date.TabIndex = 10
    Me.ef_creation_date.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_creation_date.TextValue = ""
    Me.ef_creation_date.TextWidth = 120
    Me.ef_creation_date.Value = ""
    Me.ef_creation_date.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_blocked
    '
    Me.lbl_blocked.BackColor = System.Drawing.Color.Red
    Me.lbl_blocked.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_blocked.ForeColor = System.Drawing.Color.White
    Me.lbl_blocked.Location = New System.Drawing.Point(144, 228)
    Me.lbl_blocked.Name = "lbl_blocked"
    Me.lbl_blocked.Size = New System.Drawing.Size(190, 23)
    Me.lbl_blocked.TabIndex = 7
    Me.lbl_blocked.Text = "BLOQUEADO"
    Me.lbl_blocked.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.lbl_blocked.Visible = False
    '
    'lbl_machine_blocked_reason
    '
    Me.lbl_machine_blocked_reason.AutoSize = True
    Me.lbl_machine_blocked_reason.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_machine_blocked_reason.ForeColor = System.Drawing.Color.Navy
    Me.lbl_machine_blocked_reason.Location = New System.Drawing.Point(28, 242)
    Me.lbl_machine_blocked_reason.Name = "lbl_machine_blocked_reason"
    Me.lbl_machine_blocked_reason.Size = New System.Drawing.Size(154, 14)
    Me.lbl_machine_blocked_reason.TabIndex = 8
    Me.lbl_machine_blocked_reason.Text = "xMachineBlockedReason"
    Me.lbl_machine_blocked_reason.Visible = False
    '
    'chk_machine_blocked
    '
    Me.chk_machine_blocked.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_machine_blocked.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.chk_machine_blocked.Location = New System.Drawing.Point(123, 224)
    Me.chk_machine_blocked.Name = "chk_machine_blocked"
    Me.chk_machine_blocked.RightToLeft = System.Windows.Forms.RightToLeft.Yes
    Me.chk_machine_blocked.Size = New System.Drawing.Size(231, 18)
    Me.chk_machine_blocked.TabIndex = 6
    Me.chk_machine_blocked.Text = "xMachineBlocked"
    Me.chk_machine_blocked.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_machine_blocked.Visible = False
    '
    'ef_activation_date
    '
    Me.ef_activation_date.DoubleValue = 0.0R
    Me.ef_activation_date.IntegerValue = 0
    Me.ef_activation_date.IsReadOnly = False
    Me.ef_activation_date.Location = New System.Drawing.Point(22, 50)
    Me.ef_activation_date.Name = "ef_activation_date"
    Me.ef_activation_date.PlaceHolder = Nothing
    Me.ef_activation_date.ShortcutsEnabled = True
    Me.ef_activation_date.Size = New System.Drawing.Size(332, 24)
    Me.ef_activation_date.SufixText = "Sufix Text"
    Me.ef_activation_date.SufixTextVisible = True
    Me.ef_activation_date.TabIndex = 0
    Me.ef_activation_date.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_activation_date.TextValue = ""
    Me.ef_activation_date.TextWidth = 120
    Me.ef_activation_date.Value = ""
    Me.ef_activation_date.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_retirement_date
    '
    Me.ef_retirement_date.DoubleValue = 0.0R
    Me.ef_retirement_date.IntegerValue = 0
    Me.ef_retirement_date.IsReadOnly = False
    Me.ef_retirement_date.Location = New System.Drawing.Point(22, 145)
    Me.ef_retirement_date.Name = "ef_retirement_date"
    Me.ef_retirement_date.PlaceHolder = Nothing
    Me.ef_retirement_date.ShortcutsEnabled = True
    Me.ef_retirement_date.Size = New System.Drawing.Size(332, 24)
    Me.ef_retirement_date.SufixText = "Sufix Text"
    Me.ef_retirement_date.SufixTextVisible = True
    Me.ef_retirement_date.TabIndex = 2
    Me.ef_retirement_date.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_retirement_date.TextValue = ""
    Me.ef_retirement_date.TextWidth = 120
    Me.ef_retirement_date.Value = ""
    Me.ef_retirement_date.ValueForeColor = System.Drawing.Color.Blue
    '
    'dtp_retirement_date
    '
    Me.dtp_retirement_date.Checked = True
    Me.dtp_retirement_date.IsReadOnly = False
    Me.dtp_retirement_date.Location = New System.Drawing.Point(28, 145)
    Me.dtp_retirement_date.Name = "dtp_retirement_date"
    Me.dtp_retirement_date.ShowCheckBox = True
    Me.dtp_retirement_date.ShowUpDown = False
    Me.dtp_retirement_date.Size = New System.Drawing.Size(326, 24)
    Me.dtp_retirement_date.SufixText = "Sufix Text"
    Me.dtp_retirement_date.SufixTextVisible = True
    Me.dtp_retirement_date.TabIndex = 3
    Me.dtp_retirement_date.TextWidth = 114
    Me.dtp_retirement_date.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'ef_retirement_requested
    '
    Me.ef_retirement_requested.DoubleValue = 0.0R
    Me.ef_retirement_requested.IntegerValue = 0
    Me.ef_retirement_requested.IsReadOnly = False
    Me.ef_retirement_requested.Location = New System.Drawing.Point(22, 177)
    Me.ef_retirement_requested.Name = "ef_retirement_requested"
    Me.ef_retirement_requested.PlaceHolder = Nothing
    Me.ef_retirement_requested.ShortcutsEnabled = True
    Me.ef_retirement_requested.Size = New System.Drawing.Size(332, 24)
    Me.ef_retirement_requested.SufixText = "Sufix Text"
    Me.ef_retirement_requested.SufixTextVisible = True
    Me.ef_retirement_requested.TabIndex = 4
    Me.ef_retirement_requested.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_retirement_requested.TextValue = ""
    Me.ef_retirement_requested.TextWidth = 120
    Me.ef_retirement_requested.Value = ""
    Me.ef_retirement_requested.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_program
    '
    Me.ef_program.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.ef_program.DoubleValue = 0.0R
    Me.ef_program.IntegerValue = 0
    Me.ef_program.IsReadOnly = False
    Me.ef_program.Location = New System.Drawing.Point(19, 144)
    Me.ef_program.Name = "ef_program"
    Me.ef_program.PlaceHolder = Nothing
    Me.ef_program.ShortcutsEnabled = True
    Me.ef_program.Size = New System.Drawing.Size(354, 24)
    Me.ef_program.SufixText = "Sufix Text"
    Me.ef_program.SufixTextVisible = True
    Me.ef_program.TabIndex = 6
    Me.ef_program.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_program.TextValue = ""
    Me.ef_program.TextWidth = 110
    Me.ef_program.Value = ""
    Me.ef_program.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_floor_id
    '
    Me.ef_floor_id.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.ef_floor_id.DoubleValue = 0.0R
    Me.ef_floor_id.IntegerValue = 0
    Me.ef_floor_id.IsReadOnly = False
    Me.ef_floor_id.Location = New System.Drawing.Point(11, 130)
    Me.ef_floor_id.Name = "ef_floor_id"
    Me.ef_floor_id.PlaceHolder = Nothing
    Me.ef_floor_id.ShortcutsEnabled = True
    Me.ef_floor_id.Size = New System.Drawing.Size(327, 24)
    Me.ef_floor_id.SufixText = "Sufix Text"
    Me.ef_floor_id.SufixTextVisible = True
    Me.ef_floor_id.TabIndex = 4
    Me.ef_floor_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_floor_id.TextValue = ""
    Me.ef_floor_id.TextWidth = 120
    Me.ef_floor_id.Value = ""
    Me.ef_floor_id.ValueForeColor = System.Drawing.Color.Blue
    '
    'cmb_area
    '
    Me.cmb_area.AllowUnlistedValues = False
    Me.cmb_area.AutoCompleteMode = False
    Me.cmb_area.IsReadOnly = False
    Me.cmb_area.Location = New System.Drawing.Point(11, 12)
    Me.cmb_area.Name = "cmb_area"
    Me.cmb_area.SelectedIndex = -1
    Me.cmb_area.Size = New System.Drawing.Size(327, 24)
    Me.cmb_area.SufixText = "Sufix Text"
    Me.cmb_area.SufixTextVisible = True
    Me.cmb_area.TabIndex = 0
    Me.cmb_area.TextCombo = Nothing
    Me.cmb_area.TextWidth = 120
    '
    'cmb_bank
    '
    Me.cmb_bank.AllowUnlistedValues = False
    Me.cmb_bank.AutoCompleteMode = False
    Me.cmb_bank.IsReadOnly = False
    Me.cmb_bank.Location = New System.Drawing.Point(11, 70)
    Me.cmb_bank.Name = "cmb_bank"
    Me.cmb_bank.SelectedIndex = -1
    Me.cmb_bank.Size = New System.Drawing.Size(327, 24)
    Me.cmb_bank.SufixText = "Sufix Text"
    Me.cmb_bank.SufixTextVisible = True
    Me.cmb_bank.TabIndex = 2
    Me.cmb_bank.TextCombo = Nothing
    Me.cmb_bank.TextWidth = 120
    '
    'ef_smoking
    '
    Me.ef_smoking.DoubleValue = 0.0R
    Me.ef_smoking.IntegerValue = 0
    Me.ef_smoking.IsReadOnly = False
    Me.ef_smoking.Location = New System.Drawing.Point(11, 40)
    Me.ef_smoking.Name = "ef_smoking"
    Me.ef_smoking.PlaceHolder = Nothing
    Me.ef_smoking.ShortcutsEnabled = True
    Me.ef_smoking.Size = New System.Drawing.Size(327, 24)
    Me.ef_smoking.SufixText = "Sufix Text"
    Me.ef_smoking.SufixTextVisible = True
    Me.ef_smoking.TabIndex = 1
    Me.ef_smoking.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_smoking.TextValue = ""
    Me.ef_smoking.TextWidth = 120
    Me.ef_smoking.Value = ""
    Me.ef_smoking.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_area_island
    '
    Me.gb_area_island.Controls.Add(Me.ef_position)
    Me.gb_area_island.Controls.Add(Me.cmb_area)
    Me.gb_area_island.Controls.Add(Me.ef_floor_id)
    Me.gb_area_island.Controls.Add(Me.cmb_bank)
    Me.gb_area_island.Controls.Add(Me.ef_smoking)
    Me.gb_area_island.Location = New System.Drawing.Point(390, 5)
    Me.gb_area_island.Name = "gb_area_island"
    Me.gb_area_island.Size = New System.Drawing.Size(361, 160)
    Me.gb_area_island.TabIndex = 19
    Me.gb_area_island.TabStop = False
    Me.gb_area_island.Text = "xLocation"
    '
    'ef_position
    '
    Me.ef_position.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.ef_position.DoubleValue = 0.0R
    Me.ef_position.IntegerValue = 0
    Me.ef_position.IsReadOnly = False
    Me.ef_position.Location = New System.Drawing.Point(11, 100)
    Me.ef_position.Name = "ef_position"
    Me.ef_position.PlaceHolder = Nothing
    Me.ef_position.ShortcutsEnabled = True
    Me.ef_position.Size = New System.Drawing.Size(327, 24)
    Me.ef_position.SufixText = "Sufix Text"
    Me.ef_position.SufixTextVisible = True
    Me.ef_position.TabIndex = 3
    Me.ef_position.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_position.TextValue = ""
    Me.ef_position.TextWidth = 120
    Me.ef_position.Value = ""
    Me.ef_position.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_tax_registration
    '
    Me.ef_tax_registration.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.ef_tax_registration.DoubleValue = 0.0R
    Me.ef_tax_registration.IntegerValue = 0
    Me.ef_tax_registration.IsReadOnly = False
    Me.ef_tax_registration.Location = New System.Drawing.Point(19, 174)
    Me.ef_tax_registration.Name = "ef_tax_registration"
    Me.ef_tax_registration.PlaceHolder = Nothing
    Me.ef_tax_registration.ShortcutsEnabled = True
    Me.ef_tax_registration.Size = New System.Drawing.Size(354, 24)
    Me.ef_tax_registration.SufixText = "Sufix Text"
    Me.ef_tax_registration.SufixTextVisible = True
    Me.ef_tax_registration.TabIndex = 7
    Me.ef_tax_registration.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_tax_registration.TextValue = ""
    Me.ef_tax_registration.TextWidth = 110
    Me.ef_tax_registration.Value = ""
    Me.ef_tax_registration.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_SAS
    '
    Me.gb_SAS.Controls.Add(Me.ef_sas_host_id)
    Me.gb_SAS.Controls.Add(Me.gb_serial_number)
    Me.gb_SAS.Controls.Add(Me.ef_accounting_denomination)
    Me.gb_SAS.Controls.Add(Me.gb_asset_number)
    Me.gb_SAS.Controls.Add(Me.cmb_meter_delta)
    Me.gb_SAS.Controls.Add(Me.lbl_meter_delta)
    Me.gb_SAS.Controls.Add(Me.uc_sas_flags)
    Me.gb_SAS.Location = New System.Drawing.Point(390, 514)
    Me.gb_SAS.Name = "gb_SAS"
    Me.gb_SAS.Size = New System.Drawing.Size(361, 494)
    Me.gb_SAS.TabIndex = 21
    Me.gb_SAS.TabStop = False
    Me.gb_SAS.Text = "xSAS"
    '
    'ef_sas_host_id
    '
    Me.ef_sas_host_id.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.ef_sas_host_id.DoubleValue = 0.0R
    Me.ef_sas_host_id.IntegerValue = 0
    Me.ef_sas_host_id.IsReadOnly = False
    Me.ef_sas_host_id.Location = New System.Drawing.Point(16, 300)
    Me.ef_sas_host_id.Name = "ef_sas_host_id"
    Me.ef_sas_host_id.PlaceHolder = Nothing
    Me.ef_sas_host_id.ShortcutsEnabled = True
    Me.ef_sas_host_id.Size = New System.Drawing.Size(304, 24)
    Me.ef_sas_host_id.SufixText = "Sufix Text"
    Me.ef_sas_host_id.SufixTextVisible = True
    Me.ef_sas_host_id.TabIndex = 4
    Me.ef_sas_host_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_sas_host_id.TextValue = ""
    Me.ef_sas_host_id.TextWidth = 150
    Me.ef_sas_host_id.Value = ""
    Me.ef_sas_host_id.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_serial_number
    '
    Me.gb_serial_number.Controls.Add(Me.btn_reset_serial_number)
    Me.gb_serial_number.Controls.Add(Me.ef_configured_serial_number)
    Me.gb_serial_number.Controls.Add(Me.ef_reported_serial_number)
    Me.gb_serial_number.Location = New System.Drawing.Point(9, 334)
    Me.gb_serial_number.Name = "gb_serial_number"
    Me.gb_serial_number.Size = New System.Drawing.Size(341, 72)
    Me.gb_serial_number.TabIndex = 5
    Me.gb_serial_number.TabStop = False
    Me.gb_serial_number.Text = "xSerial Number"
    '
    'btn_reset_serial_number
    '
    Me.btn_reset_serial_number.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_reset_serial_number.Location = New System.Drawing.Point(275, 40)
    Me.btn_reset_serial_number.MaximumSize = New System.Drawing.Size(60, 25)
    Me.btn_reset_serial_number.Name = "btn_reset_serial_number"
    Me.btn_reset_serial_number.Size = New System.Drawing.Size(60, 25)
    Me.btn_reset_serial_number.TabIndex = 2
    Me.btn_reset_serial_number.ToolTipped = False
    Me.btn_reset_serial_number.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'ef_configured_serial_number
    '
    Me.ef_configured_serial_number.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.ef_configured_serial_number.DoubleValue = 0.0R
    Me.ef_configured_serial_number.IntegerValue = 0
    Me.ef_configured_serial_number.IsReadOnly = False
    Me.ef_configured_serial_number.Location = New System.Drawing.Point(12, 41)
    Me.ef_configured_serial_number.Name = "ef_configured_serial_number"
    Me.ef_configured_serial_number.PlaceHolder = Nothing
    Me.ef_configured_serial_number.ShortcutsEnabled = True
    Me.ef_configured_serial_number.Size = New System.Drawing.Size(262, 24)
    Me.ef_configured_serial_number.SufixText = "Sufix Text"
    Me.ef_configured_serial_number.SufixTextVisible = True
    Me.ef_configured_serial_number.TabIndex = 1
    Me.ef_configured_serial_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_configured_serial_number.TextValue = ""
    Me.ef_configured_serial_number.TextWidth = 75
    Me.ef_configured_serial_number.Value = ""
    Me.ef_configured_serial_number.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_reported_serial_number
    '
    Me.ef_reported_serial_number.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.ef_reported_serial_number.DoubleValue = 0.0R
    Me.ef_reported_serial_number.IntegerValue = 0
    Me.ef_reported_serial_number.IsReadOnly = False
    Me.ef_reported_serial_number.Location = New System.Drawing.Point(12, 18)
    Me.ef_reported_serial_number.Name = "ef_reported_serial_number"
    Me.ef_reported_serial_number.PlaceHolder = Nothing
    Me.ef_reported_serial_number.ShortcutsEnabled = True
    Me.ef_reported_serial_number.Size = New System.Drawing.Size(262, 24)
    Me.ef_reported_serial_number.SufixText = "Sufix Text"
    Me.ef_reported_serial_number.SufixTextVisible = True
    Me.ef_reported_serial_number.TabIndex = 0
    Me.ef_reported_serial_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_reported_serial_number.TextValue = ""
    Me.ef_reported_serial_number.TextWidth = 75
    Me.ef_reported_serial_number.Value = ""
    Me.ef_reported_serial_number.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_accounting_denomination
    '
    Me.ef_accounting_denomination.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.ef_accounting_denomination.DoubleValue = 0.0R
    Me.ef_accounting_denomination.IntegerValue = 0
    Me.ef_accounting_denomination.IsReadOnly = False
    Me.ef_accounting_denomination.Location = New System.Drawing.Point(16, 270)
    Me.ef_accounting_denomination.Name = "ef_accounting_denomination"
    Me.ef_accounting_denomination.PlaceHolder = Nothing
    Me.ef_accounting_denomination.ShortcutsEnabled = True
    Me.ef_accounting_denomination.Size = New System.Drawing.Size(304, 24)
    Me.ef_accounting_denomination.SufixText = "Sufix Text"
    Me.ef_accounting_denomination.SufixTextVisible = True
    Me.ef_accounting_denomination.TabIndex = 3
    Me.ef_accounting_denomination.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_accounting_denomination.TextValue = ""
    Me.ef_accounting_denomination.TextWidth = 150
    Me.ef_accounting_denomination.Value = ""
    Me.ef_accounting_denomination.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_asset_number
    '
    Me.gb_asset_number.Controls.Add(Me.btn_reset_asset_number)
    Me.gb_asset_number.Controls.Add(Me.ef_configured_asset_number)
    Me.gb_asset_number.Controls.Add(Me.ef_reported_asset_number)
    Me.gb_asset_number.Location = New System.Drawing.Point(9, 411)
    Me.gb_asset_number.Name = "gb_asset_number"
    Me.gb_asset_number.Size = New System.Drawing.Size(341, 72)
    Me.gb_asset_number.TabIndex = 6
    Me.gb_asset_number.TabStop = False
    Me.gb_asset_number.Text = "xAsset Number"
    '
    'btn_reset_asset_number
    '
    Me.btn_reset_asset_number.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_reset_asset_number.Location = New System.Drawing.Point(275, 41)
    Me.btn_reset_asset_number.MaximumSize = New System.Drawing.Size(60, 25)
    Me.btn_reset_asset_number.Name = "btn_reset_asset_number"
    Me.btn_reset_asset_number.Size = New System.Drawing.Size(60, 25)
    Me.btn_reset_asset_number.TabIndex = 2
    Me.btn_reset_asset_number.ToolTipped = False
    Me.btn_reset_asset_number.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'ef_configured_asset_number
    '
    Me.ef_configured_asset_number.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.ef_configured_asset_number.DoubleValue = 0.0R
    Me.ef_configured_asset_number.IntegerValue = 0
    Me.ef_configured_asset_number.IsReadOnly = False
    Me.ef_configured_asset_number.Location = New System.Drawing.Point(12, 41)
    Me.ef_configured_asset_number.Name = "ef_configured_asset_number"
    Me.ef_configured_asset_number.PlaceHolder = Nothing
    Me.ef_configured_asset_number.ShortcutsEnabled = True
    Me.ef_configured_asset_number.Size = New System.Drawing.Size(262, 24)
    Me.ef_configured_asset_number.SufixText = "Sufix Text"
    Me.ef_configured_asset_number.SufixTextVisible = True
    Me.ef_configured_asset_number.TabIndex = 1
    Me.ef_configured_asset_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_configured_asset_number.TextValue = ""
    Me.ef_configured_asset_number.TextWidth = 75
    Me.ef_configured_asset_number.Value = ""
    Me.ef_configured_asset_number.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_reported_asset_number
    '
    Me.ef_reported_asset_number.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.ef_reported_asset_number.DoubleValue = 0.0R
    Me.ef_reported_asset_number.IntegerValue = 0
    Me.ef_reported_asset_number.IsReadOnly = False
    Me.ef_reported_asset_number.Location = New System.Drawing.Point(12, 18)
    Me.ef_reported_asset_number.Name = "ef_reported_asset_number"
    Me.ef_reported_asset_number.PlaceHolder = Nothing
    Me.ef_reported_asset_number.ShortcutsEnabled = True
    Me.ef_reported_asset_number.Size = New System.Drawing.Size(262, 24)
    Me.ef_reported_asset_number.SufixText = "Sufix Text"
    Me.ef_reported_asset_number.SufixTextVisible = True
    Me.ef_reported_asset_number.TabIndex = 0
    Me.ef_reported_asset_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_reported_asset_number.TextValue = ""
    Me.ef_reported_asset_number.TextWidth = 75
    Me.ef_reported_asset_number.Value = ""
    Me.ef_reported_asset_number.ValueForeColor = System.Drawing.Color.Blue
    '
    'cmb_meter_delta
    '
    Me.cmb_meter_delta.AllowUnlistedValues = False
    Me.cmb_meter_delta.AutoCompleteMode = False
    Me.cmb_meter_delta.IsReadOnly = False
    Me.cmb_meter_delta.Location = New System.Drawing.Point(14, 31)
    Me.cmb_meter_delta.Name = "cmb_meter_delta"
    Me.cmb_meter_delta.SelectedIndex = -1
    Me.cmb_meter_delta.Size = New System.Drawing.Size(204, 24)
    Me.cmb_meter_delta.SufixText = "Sufix Text"
    Me.cmb_meter_delta.SufixTextVisible = True
    Me.cmb_meter_delta.TabIndex = 1
    Me.cmb_meter_delta.TextCombo = Nothing
    Me.cmb_meter_delta.TextWidth = 0
    '
    'lbl_meter_delta
    '
    Me.lbl_meter_delta.AutoSize = True
    Me.lbl_meter_delta.Location = New System.Drawing.Point(14, 15)
    Me.lbl_meter_delta.Name = "lbl_meter_delta"
    Me.lbl_meter_delta.Size = New System.Drawing.Size(76, 13)
    Me.lbl_meter_delta.TabIndex = 0
    Me.lbl_meter_delta.Text = "xMeterDelta"
    '
    'uc_sas_flags
    '
    Me.uc_sas_flags.AFT_although_lock_0 = False
    Me.uc_sas_flags.BCD4 = 4
    Me.uc_sas_flags.BCD5 = 5
    Me.uc_sas_flags.Cmd1BHandPays = WSI.Common.ENUM_SAS_FLAGS.SITE_DEFINED
    Me.uc_sas_flags.EnableDisableNoteAcceptor = WSI.Common.ENUM_SAS_FLAGS.SITE_DEFINED
    Me.uc_sas_flags.ExtendedMeters = WSI.Common.ENUM_SAS_FLAGS.SITE_DEFINED
    Me.uc_sas_flags.Ignore_no_bet_plays = False
    Me.uc_sas_flags.Location = New System.Drawing.Point(11, 56)
    Me.uc_sas_flags.LockEgmOnReserve = WSI.Common.ENUM_SAS_FLAGS.NOT_ACTIVATED
    Me.uc_sas_flags.Name = "uc_sas_flags"
    Me.uc_sas_flags.No_RTE = False
    Me.uc_sas_flags.ProgressiveMeters = WSI.Common.ENUM_SAS_FLAGS.SITE_DEFINED
    Me.uc_sas_flags.PromotionalCredits = WSI.Common.ENUM_SAS_FLAGS.SITE_DEFINED
    Me.uc_sas_flags.ReserveTerminal = WSI.Common.ENUM_SAS_FLAGS.SITE_DEFINED
    Me.uc_sas_flags.Single_byte = False
    Me.uc_sas_flags.Size = New System.Drawing.Size(345, 209)
    Me.uc_sas_flags.TabIndex = 2
    '
    'ef_min_denomination
    '
    Me.ef_min_denomination.DoubleValue = 0.0R
    Me.ef_min_denomination.IntegerValue = 0
    Me.ef_min_denomination.IsReadOnly = False
    Me.ef_min_denomination.Location = New System.Drawing.Point(5, 137)
    Me.ef_min_denomination.Name = "ef_min_denomination"
    Me.ef_min_denomination.PlaceHolder = Nothing
    Me.ef_min_denomination.ShortcutsEnabled = True
    Me.ef_min_denomination.Size = New System.Drawing.Size(267, 24)
    Me.ef_min_denomination.SufixText = "Sufix Text"
    Me.ef_min_denomination.SufixTextVisible = True
    Me.ef_min_denomination.TabIndex = 6
    Me.ef_min_denomination.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_min_denomination.TextValue = ""
    Me.ef_min_denomination.TextWidth = 175
    Me.ef_min_denomination.Value = ""
    Me.ef_min_denomination.ValueForeColor = System.Drawing.Color.Blue
    '
    'cmb_provider
    '
    Me.cmb_provider.AllowUnlistedValues = False
    Me.cmb_provider.AutoCompleteMode = False
    Me.cmb_provider.IsReadOnly = False
    Me.cmb_provider.Location = New System.Drawing.Point(10, 59)
    Me.cmb_provider.Name = "cmb_provider"
    Me.cmb_provider.SelectedIndex = -1
    Me.cmb_provider.Size = New System.Drawing.Size(363, 24)
    Me.cmb_provider.SufixText = "Sufix Text"
    Me.cmb_provider.SufixTextVisible = True
    Me.cmb_provider.TabIndex = 2
    Me.cmb_provider.TextCombo = Nothing
    Me.cmb_provider.TextWidth = 120
    '
    'gb_contract
    '
    Me.gb_contract.Controls.Add(Me.cmb_contract_type)
    Me.gb_contract.Controls.Add(Me.ef_order_number)
    Me.gb_contract.Controls.Add(Me.ef_contract_id)
    Me.gb_contract.Location = New System.Drawing.Point(4, 390)
    Me.gb_contract.Name = "gb_contract"
    Me.gb_contract.Size = New System.Drawing.Size(375, 100)
    Me.gb_contract.TabIndex = 14
    Me.gb_contract.TabStop = False
    Me.gb_contract.Text = "xContract"
    '
    'cmb_contract_type
    '
    Me.cmb_contract_type.AllowUnlistedValues = False
    Me.cmb_contract_type.AutoCompleteMode = False
    Me.cmb_contract_type.IsReadOnly = False
    Me.cmb_contract_type.Location = New System.Drawing.Point(6, 13)
    Me.cmb_contract_type.Name = "cmb_contract_type"
    Me.cmb_contract_type.SelectedIndex = -1
    Me.cmb_contract_type.Size = New System.Drawing.Size(348, 24)
    Me.cmb_contract_type.SufixText = "Sufix Text"
    Me.cmb_contract_type.SufixTextVisible = True
    Me.cmb_contract_type.TabIndex = 0
    Me.cmb_contract_type.TextCombo = Nothing
    Me.cmb_contract_type.TextWidth = 131
    '
    'ef_order_number
    '
    Me.ef_order_number.DoubleValue = 0.0R
    Me.ef_order_number.IntegerValue = 0
    Me.ef_order_number.IsReadOnly = False
    Me.ef_order_number.Location = New System.Drawing.Point(6, 69)
    Me.ef_order_number.Name = "ef_order_number"
    Me.ef_order_number.PlaceHolder = Nothing
    Me.ef_order_number.ShortcutsEnabled = True
    Me.ef_order_number.Size = New System.Drawing.Size(348, 24)
    Me.ef_order_number.SufixText = "Sufix Text"
    Me.ef_order_number.SufixTextVisible = True
    Me.ef_order_number.TabIndex = 2
    Me.ef_order_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_order_number.TextValue = ""
    Me.ef_order_number.TextWidth = 131
    Me.ef_order_number.Value = ""
    Me.ef_order_number.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_contract_id
    '
    Me.ef_contract_id.AllowDrop = True
    Me.ef_contract_id.DoubleValue = 0.0R
    Me.ef_contract_id.IntegerValue = 0
    Me.ef_contract_id.IsReadOnly = False
    Me.ef_contract_id.Location = New System.Drawing.Point(6, 41)
    Me.ef_contract_id.Name = "ef_contract_id"
    Me.ef_contract_id.PlaceHolder = Nothing
    Me.ef_contract_id.ShortcutsEnabled = True
    Me.ef_contract_id.Size = New System.Drawing.Size(348, 24)
    Me.ef_contract_id.SufixText = "Sufix Text"
    Me.ef_contract_id.SufixTextVisible = True
    Me.ef_contract_id.TabIndex = 1
    Me.ef_contract_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_contract_id.TextValue = ""
    Me.ef_contract_id.TextWidth = 131
    Me.ef_contract_id.Value = ""
    Me.ef_contract_id.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_ticket_configuration
    '
    Me.gb_ticket_configuration.Controls.Add(Me.chk_allow_truncate)
    Me.gb_ticket_configuration.Controls.Add(Me.lbl_allow_truncate)
    Me.gb_ticket_configuration.Controls.Add(Me.ef_min_ticket_in)
    Me.gb_ticket_configuration.Controls.Add(Me.lbl_ticket_out_info)
    Me.gb_ticket_configuration.Controls.Add(Me.ef_max_ticket_out)
    Me.gb_ticket_configuration.Controls.Add(Me.ef_max_ticket_in)
    Me.gb_ticket_configuration.Controls.Add(Me.chk_allow_promotional_emission)
    Me.gb_ticket_configuration.Controls.Add(Me.chk_allow_cashable_emission)
    Me.gb_ticket_configuration.Controls.Add(Me.chk_allow_redemption)
    Me.gb_ticket_configuration.Controls.Add(Me.opt_default)
    Me.gb_ticket_configuration.Controls.Add(Me.opt_personalized)
    Me.gb_ticket_configuration.Controls.Add(Me.ef_min_denomination)
    Me.gb_ticket_configuration.Location = New System.Drawing.Point(5, 707)
    Me.gb_ticket_configuration.Name = "gb_ticket_configuration"
    Me.gb_ticket_configuration.Size = New System.Drawing.Size(375, 320)
    Me.gb_ticket_configuration.TabIndex = 16
    Me.gb_ticket_configuration.TabStop = False
    Me.gb_ticket_configuration.Text = "xTicketConfig"
    '
    'chk_allow_truncate
    '
    Me.chk_allow_truncate.AutoSize = True
    Me.chk_allow_truncate.Location = New System.Drawing.Point(27, 117)
    Me.chk_allow_truncate.Name = "chk_allow_truncate"
    Me.chk_allow_truncate.Size = New System.Drawing.Size(112, 17)
    Me.chk_allow_truncate.TabIndex = 5
    Me.chk_allow_truncate.Text = "xAllowTruncate"
    Me.chk_allow_truncate.UseVisualStyleBackColor = True
    '
    'lbl_allow_truncate
    '
    Me.lbl_allow_truncate.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_allow_truncate.ForeColor = System.Drawing.Color.Navy
    Me.lbl_allow_truncate.Location = New System.Drawing.Point(28, 283)
    Me.lbl_allow_truncate.Name = "lbl_allow_truncate"
    Me.lbl_allow_truncate.Size = New System.Drawing.Size(341, 24)
    Me.lbl_allow_truncate.TabIndex = 9
    Me.lbl_allow_truncate.Text = "xInfo"
    '
    'ef_min_ticket_in
    '
    Me.ef_min_ticket_in.DoubleValue = 0.0R
    Me.ef_min_ticket_in.IntegerValue = 0
    Me.ef_min_ticket_in.IsReadOnly = False
    Me.ef_min_ticket_in.Location = New System.Drawing.Point(5, 203)
    Me.ef_min_ticket_in.Name = "ef_min_ticket_in"
    Me.ef_min_ticket_in.PlaceHolder = Nothing
    Me.ef_min_ticket_in.ShortcutsEnabled = True
    Me.ef_min_ticket_in.Size = New System.Drawing.Size(267, 24)
    Me.ef_min_ticket_in.SufixText = "Sufix Text"
    Me.ef_min_ticket_in.SufixTextVisible = True
    Me.ef_min_ticket_in.TabIndex = 8
    Me.ef_min_ticket_in.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_min_ticket_in.TextValue = ""
    Me.ef_min_ticket_in.TextWidth = 175
    Me.ef_min_ticket_in.Value = ""
    Me.ef_min_ticket_in.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_ticket_out_info
    '
    Me.lbl_ticket_out_info.AutoSize = True
    Me.lbl_ticket_out_info.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_ticket_out_info.ForeColor = System.Drawing.Color.Navy
    Me.lbl_ticket_out_info.Location = New System.Drawing.Point(28, 267)
    Me.lbl_ticket_out_info.Name = "lbl_ticket_out_info"
    Me.lbl_ticket_out_info.Size = New System.Drawing.Size(42, 14)
    Me.lbl_ticket_out_info.TabIndex = 7
    Me.lbl_ticket_out_info.Text = "xInfo"
    '
    'ef_max_ticket_out
    '
    Me.ef_max_ticket_out.DoubleValue = 0.0R
    Me.ef_max_ticket_out.IntegerValue = 0
    Me.ef_max_ticket_out.IsReadOnly = False
    Me.ef_max_ticket_out.Location = New System.Drawing.Point(5, 232)
    Me.ef_max_ticket_out.Name = "ef_max_ticket_out"
    Me.ef_max_ticket_out.PlaceHolder = Nothing
    Me.ef_max_ticket_out.ShortcutsEnabled = True
    Me.ef_max_ticket_out.Size = New System.Drawing.Size(267, 24)
    Me.ef_max_ticket_out.SufixText = "Sufix Text"
    Me.ef_max_ticket_out.SufixTextVisible = True
    Me.ef_max_ticket_out.TabIndex = 9
    Me.ef_max_ticket_out.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_max_ticket_out.TextValue = ""
    Me.ef_max_ticket_out.TextWidth = 175
    Me.ef_max_ticket_out.Value = ""
    Me.ef_max_ticket_out.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_max_ticket_in
    '
    Me.ef_max_ticket_in.DoubleValue = 0.0R
    Me.ef_max_ticket_in.IntegerValue = 0
    Me.ef_max_ticket_in.IsReadOnly = False
    Me.ef_max_ticket_in.Location = New System.Drawing.Point(5, 174)
    Me.ef_max_ticket_in.Name = "ef_max_ticket_in"
    Me.ef_max_ticket_in.PlaceHolder = Nothing
    Me.ef_max_ticket_in.ShortcutsEnabled = True
    Me.ef_max_ticket_in.Size = New System.Drawing.Size(267, 24)
    Me.ef_max_ticket_in.SufixText = "Sufix Text"
    Me.ef_max_ticket_in.SufixTextVisible = True
    Me.ef_max_ticket_in.TabIndex = 7
    Me.ef_max_ticket_in.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_max_ticket_in.TextValue = ""
    Me.ef_max_ticket_in.TextWidth = 175
    Me.ef_max_ticket_in.Value = ""
    Me.ef_max_ticket_in.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_allow_promotional_emission
    '
    Me.chk_allow_promotional_emission.AutoSize = True
    Me.chk_allow_promotional_emission.Location = New System.Drawing.Point(27, 92)
    Me.chk_allow_promotional_emission.Name = "chk_allow_promotional_emission"
    Me.chk_allow_promotional_emission.Size = New System.Drawing.Size(221, 17)
    Me.chk_allow_promotional_emission.TabIndex = 4
    Me.chk_allow_promotional_emission.Text = "xAllowPromotionalTicketsEmission"
    Me.chk_allow_promotional_emission.UseVisualStyleBackColor = True
    '
    'chk_allow_cashable_emission
    '
    Me.chk_allow_cashable_emission.AutoSize = True
    Me.chk_allow_cashable_emission.Location = New System.Drawing.Point(27, 68)
    Me.chk_allow_cashable_emission.Name = "chk_allow_cashable_emission"
    Me.chk_allow_cashable_emission.Size = New System.Drawing.Size(205, 17)
    Me.chk_allow_cashable_emission.TabIndex = 3
    Me.chk_allow_cashable_emission.Text = "xAllowCashableTicketsEmission"
    Me.chk_allow_cashable_emission.UseVisualStyleBackColor = True
    '
    'chk_allow_redemption
    '
    Me.chk_allow_redemption.AutoSize = True
    Me.chk_allow_redemption.Location = New System.Drawing.Point(27, 44)
    Me.chk_allow_redemption.Name = "chk_allow_redemption"
    Me.chk_allow_redemption.Size = New System.Drawing.Size(165, 17)
    Me.chk_allow_redemption.TabIndex = 2
    Me.chk_allow_redemption.Text = "xAllowTicketRedemption"
    Me.chk_allow_redemption.UseVisualStyleBackColor = True
    '
    'opt_default
    '
    Me.opt_default.AutoSize = True
    Me.opt_default.Location = New System.Drawing.Point(6, 18)
    Me.opt_default.Name = "opt_default"
    Me.opt_default.Size = New System.Drawing.Size(73, 17)
    Me.opt_default.TabIndex = 0
    Me.opt_default.TabStop = True
    Me.opt_default.Text = "xDefault"
    Me.opt_default.UseVisualStyleBackColor = True
    '
    'opt_personalized
    '
    Me.opt_personalized.AutoSize = True
    Me.opt_personalized.Location = New System.Drawing.Point(179, 18)
    Me.opt_personalized.Name = "opt_personalized"
    Me.opt_personalized.Size = New System.Drawing.Size(104, 17)
    Me.opt_personalized.TabIndex = 1
    Me.opt_personalized.TabStop = True
    Me.opt_personalized.Text = "xPersonalized"
    Me.opt_personalized.UseVisualStyleBackColor = True
    '
    'gb_signature_validation
    '
    Me.gb_signature_validation.Controls.Add(Me.lbl_authentication_status_detail)
    Me.gb_signature_validation.Controls.Add(Me.lbl_authentication_signature)
    Me.gb_signature_validation.Controls.Add(Me.lbl_authentication_seed)
    Me.gb_signature_validation.Controls.Add(Me.lbl_authentication_method)
    Me.gb_signature_validation.Controls.Add(Me.lbl_authentication_status)
    Me.gb_signature_validation.Controls.Add(Me.lbl_authentication_last_checked)
    Me.gb_signature_validation.Location = New System.Drawing.Point(12, 780)
    Me.gb_signature_validation.Name = "gb_signature_validation"
    Me.gb_signature_validation.Size = New System.Drawing.Size(375, 122)
    Me.gb_signature_validation.TabIndex = 16
    Me.gb_signature_validation.TabStop = False
    Me.gb_signature_validation.Text = "xSignatureValidation"
    Me.gb_signature_validation.Visible = False
    '
    'lbl_authentication_status_detail
    '
    Me.lbl_authentication_status_detail.AutoSize = True
    Me.lbl_authentication_status_detail.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_authentication_status_detail.ForeColor = System.Drawing.Color.Red
    Me.lbl_authentication_status_detail.Location = New System.Drawing.Point(69, 78)
    Me.lbl_authentication_status_detail.Name = "lbl_authentication_status_detail"
    Me.lbl_authentication_status_detail.Size = New System.Drawing.Size(196, 14)
    Me.lbl_authentication_status_detail.TabIndex = 4
    Me.lbl_authentication_status_detail.Text = "xAuthenticationStatusDetail"
    '
    'lbl_authentication_signature
    '
    Me.lbl_authentication_signature.AutoSize = True
    Me.lbl_authentication_signature.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_authentication_signature.ForeColor = System.Drawing.Color.Navy
    Me.lbl_authentication_signature.Location = New System.Drawing.Point(11, 59)
    Me.lbl_authentication_signature.Name = "lbl_authentication_signature"
    Me.lbl_authentication_signature.Size = New System.Drawing.Size(175, 14)
    Me.lbl_authentication_signature.TabIndex = 2
    Me.lbl_authentication_signature.Text = "xAuthenticationSignature"
    '
    'lbl_authentication_seed
    '
    Me.lbl_authentication_seed.AutoSize = True
    Me.lbl_authentication_seed.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_authentication_seed.ForeColor = System.Drawing.Color.Navy
    Me.lbl_authentication_seed.Location = New System.Drawing.Point(11, 40)
    Me.lbl_authentication_seed.Name = "lbl_authentication_seed"
    Me.lbl_authentication_seed.Size = New System.Drawing.Size(140, 14)
    Me.lbl_authentication_seed.TabIndex = 1
    Me.lbl_authentication_seed.Text = "xAuthenticationSeed"
    '
    'lbl_authentication_method
    '
    Me.lbl_authentication_method.AutoSize = True
    Me.lbl_authentication_method.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_authentication_method.ForeColor = System.Drawing.Color.Navy
    Me.lbl_authentication_method.Location = New System.Drawing.Point(11, 21)
    Me.lbl_authentication_method.Name = "lbl_authentication_method"
    Me.lbl_authentication_method.Size = New System.Drawing.Size(154, 14)
    Me.lbl_authentication_method.TabIndex = 0
    Me.lbl_authentication_method.Text = "xAuthenticationMethod"
    '
    'lbl_authentication_status
    '
    Me.lbl_authentication_status.AutoSize = True
    Me.lbl_authentication_status.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_authentication_status.ForeColor = System.Drawing.Color.Navy
    Me.lbl_authentication_status.Location = New System.Drawing.Point(11, 78)
    Me.lbl_authentication_status.Name = "lbl_authentication_status"
    Me.lbl_authentication_status.Size = New System.Drawing.Size(154, 14)
    Me.lbl_authentication_status.TabIndex = 3
    Me.lbl_authentication_status.Text = "xAuthenticationStatus"
    '
    'lbl_authentication_last_checked
    '
    Me.lbl_authentication_last_checked.AutoSize = True
    Me.lbl_authentication_last_checked.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_authentication_last_checked.ForeColor = System.Drawing.Color.Navy
    Me.lbl_authentication_last_checked.Location = New System.Drawing.Point(11, 97)
    Me.lbl_authentication_last_checked.Name = "lbl_authentication_last_checked"
    Me.lbl_authentication_last_checked.Size = New System.Drawing.Size(189, 14)
    Me.lbl_authentication_last_checked.TabIndex = 5
    Me.lbl_authentication_last_checked.Text = "xAuthenticationLastChecked"
    '
    'ef_machine_id
    '
    Me.ef_machine_id.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.ef_machine_id.DoubleValue = 0.0R
    Me.ef_machine_id.IntegerValue = 0
    Me.ef_machine_id.IsReadOnly = False
    Me.ef_machine_id.Location = New System.Drawing.Point(5, 233)
    Me.ef_machine_id.Name = "ef_machine_id"
    Me.ef_machine_id.PlaceHolder = Nothing
    Me.ef_machine_id.ShortcutsEnabled = True
    Me.ef_machine_id.Size = New System.Drawing.Size(368, 24)
    Me.ef_machine_id.SufixText = "Sufix Text"
    Me.ef_machine_id.SufixTextVisible = True
    Me.ef_machine_id.TabIndex = 8
    Me.ef_machine_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_machine_id.TextValue = ""
    Me.ef_machine_id.TextWidth = 124
    Me.ef_machine_id.Value = ""
    Me.ef_machine_id.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_theoretical_payout
    '
    Me.ef_theoretical_payout.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.ef_theoretical_payout.DoubleValue = 0.0R
    Me.ef_theoretical_payout.IntegerValue = 0
    Me.ef_theoretical_payout.IsReadOnly = False
    Me.ef_theoretical_payout.Location = New System.Drawing.Point(14, 136)
    Me.ef_theoretical_payout.Name = "ef_theoretical_payout"
    Me.ef_theoretical_payout.PlaceHolder = Nothing
    Me.ef_theoretical_payout.ShortcutsEnabled = True
    Me.ef_theoretical_payout.Size = New System.Drawing.Size(336, 24)
    Me.ef_theoretical_payout.SufixText = "Sufix Text"
    Me.ef_theoretical_payout.SufixTextVisible = True
    Me.ef_theoretical_payout.TabIndex = 4
    Me.ef_theoretical_payout.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_theoretical_payout.TextValue = ""
    Me.ef_theoretical_payout.TextWidth = 180
    Me.ef_theoretical_payout.Value = ""
    Me.ef_theoretical_payout.ValueForeColor = System.Drawing.Color.Blue
    '
    'cmb_game_type
    '
    Me.cmb_game_type.AllowUnlistedValues = False
    Me.cmb_game_type.AutoCompleteMode = False
    Me.cmb_game_type.IsReadOnly = False
    Me.cmb_game_type.Location = New System.Drawing.Point(84, 13)
    Me.cmb_game_type.Name = "cmb_game_type"
    Me.cmb_game_type.SelectedIndex = -1
    Me.cmb_game_type.Size = New System.Drawing.Size(266, 24)
    Me.cmb_game_type.SufixText = "Sufix Text"
    Me.cmb_game_type.SufixTextVisible = True
    Me.cmb_game_type.TabIndex = 0
    Me.cmb_game_type.TextCombo = Nothing
    Me.cmb_game_type.TextWidth = 110
    '
    'ef_cabinet_type
    '
    Me.ef_cabinet_type.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.ef_cabinet_type.DoubleValue = 0.0R
    Me.ef_cabinet_type.IntegerValue = 0
    Me.ef_cabinet_type.IsReadOnly = False
    Me.ef_cabinet_type.Location = New System.Drawing.Point(14, 43)
    Me.ef_cabinet_type.Name = "ef_cabinet_type"
    Me.ef_cabinet_type.PlaceHolder = Nothing
    Me.ef_cabinet_type.ShortcutsEnabled = True
    Me.ef_cabinet_type.Size = New System.Drawing.Size(336, 24)
    Me.ef_cabinet_type.SufixText = "Sufix Text"
    Me.ef_cabinet_type.SufixTextVisible = True
    Me.ef_cabinet_type.TabIndex = 1
    Me.ef_cabinet_type.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_cabinet_type.TextValue = ""
    Me.ef_cabinet_type.TextWidth = 180
    Me.ef_cabinet_type.Value = ""
    Me.ef_cabinet_type.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_jackpot_contribution_pct
    '
    Me.ef_jackpot_contribution_pct.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.ef_jackpot_contribution_pct.DoubleValue = 0.0R
    Me.ef_jackpot_contribution_pct.IntegerValue = 0
    Me.ef_jackpot_contribution_pct.IsReadOnly = False
    Me.ef_jackpot_contribution_pct.Location = New System.Drawing.Point(14, 167)
    Me.ef_jackpot_contribution_pct.Name = "ef_jackpot_contribution_pct"
    Me.ef_jackpot_contribution_pct.PlaceHolder = Nothing
    Me.ef_jackpot_contribution_pct.ShortcutsEnabled = True
    Me.ef_jackpot_contribution_pct.Size = New System.Drawing.Size(336, 24)
    Me.ef_jackpot_contribution_pct.SufixText = "Sufix Text"
    Me.ef_jackpot_contribution_pct.SufixTextVisible = True
    Me.ef_jackpot_contribution_pct.TabIndex = 5
    Me.ef_jackpot_contribution_pct.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_jackpot_contribution_pct.TextValue = ""
    Me.ef_jackpot_contribution_pct.TextWidth = 180
    Me.ef_jackpot_contribution_pct.Value = ""
    Me.ef_jackpot_contribution_pct.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_top_award
    '
    Me.ef_top_award.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.ef_top_award.DoubleValue = 0.0R
    Me.ef_top_award.IntegerValue = 0
    Me.ef_top_award.IsReadOnly = False
    Me.ef_top_award.Location = New System.Drawing.Point(14, 74)
    Me.ef_top_award.Name = "ef_top_award"
    Me.ef_top_award.PlaceHolder = Nothing
    Me.ef_top_award.ShortcutsEnabled = True
    Me.ef_top_award.Size = New System.Drawing.Size(336, 24)
    Me.ef_top_award.SufixText = "Sufix Text"
    Me.ef_top_award.SufixTextVisible = True
    Me.ef_top_award.TabIndex = 2
    Me.ef_top_award.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_top_award.TextValue = ""
    Me.ef_top_award.TextWidth = 180
    Me.ef_top_award.Value = ""
    Me.ef_top_award.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_max_bet
    '
    Me.ef_max_bet.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.ef_max_bet.DoubleValue = 0.0R
    Me.ef_max_bet.IntegerValue = 0
    Me.ef_max_bet.IsReadOnly = False
    Me.ef_max_bet.Location = New System.Drawing.Point(14, 105)
    Me.ef_max_bet.Name = "ef_max_bet"
    Me.ef_max_bet.PlaceHolder = Nothing
    Me.ef_max_bet.ShortcutsEnabled = True
    Me.ef_max_bet.Size = New System.Drawing.Size(336, 24)
    Me.ef_max_bet.SufixText = "Sufix Text"
    Me.ef_max_bet.SufixTextVisible = True
    Me.ef_max_bet.TabIndex = 3
    Me.ef_max_bet.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_max_bet.TextValue = ""
    Me.ef_max_bet.TextWidth = 180
    Me.ef_max_bet.Value = ""
    Me.ef_max_bet.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_denomination
    '
    Me.gb_denomination.Controls.Add(Me.chk_equity_percentage)
    Me.gb_denomination.Controls.Add(Me.cmb_currency)
    Me.gb_denomination.Controls.Add(Me.chk_coin_collection)
    Me.gb_denomination.Controls.Add(Me.ef_game_theme)
    Me.gb_denomination.Controls.Add(Me.opt_multi)
    Me.gb_denomination.Controls.Add(Me.opt_single)
    Me.gb_denomination.Controls.Add(Me.cmb_multi_denomination)
    Me.gb_denomination.Controls.Add(Me.ef_number_lines)
    Me.gb_denomination.Controls.Add(Me.cmb_single_denomination)
    Me.gb_denomination.Controls.Add(Me.ef_max_bet)
    Me.gb_denomination.Controls.Add(Me.ef_top_award)
    Me.gb_denomination.Controls.Add(Me.ef_jackpot_contribution_pct)
    Me.gb_denomination.Controls.Add(Me.ef_cabinet_type)
    Me.gb_denomination.Controls.Add(Me.cmb_game_type)
    Me.gb_denomination.Controls.Add(Me.ef_theoretical_payout)
    Me.gb_denomination.Controls.Add(Me.ef_equity_percentage)
    Me.gb_denomination.Location = New System.Drawing.Point(390, 167)
    Me.gb_denomination.Name = "gb_denomination"
    Me.gb_denomination.Size = New System.Drawing.Size(361, 341)
    Me.gb_denomination.TabIndex = 20
    Me.gb_denomination.TabStop = False
    Me.gb_denomination.Text = "xFeatures"
    '
    'chk_equity_percentage
    '
    Me.chk_equity_percentage.AutoSize = True
    Me.chk_equity_percentage.Location = New System.Drawing.Point(34, 354)
    Me.chk_equity_percentage.Name = "chk_equity_percentage"
    Me.chk_equity_percentage.Size = New System.Drawing.Size(15, 14)
    Me.chk_equity_percentage.TabIndex = 27
    Me.chk_equity_percentage.UseVisualStyleBackColor = True
    '
    'cmb_currency
    '
    Me.cmb_currency.AllowUnlistedValues = False
    Me.cmb_currency.AutoCompleteMode = False
    Me.cmb_currency.IsReadOnly = False
    Me.cmb_currency.Location = New System.Drawing.Point(84, 257)
    Me.cmb_currency.Name = "cmb_currency"
    Me.cmb_currency.SelectedIndex = -1
    Me.cmb_currency.Size = New System.Drawing.Size(266, 24)
    Me.cmb_currency.SufixText = "Sufix Text"
    Me.cmb_currency.SufixTextVisible = True
    Me.cmb_currency.TabIndex = 8
    Me.cmb_currency.TextCombo = Nothing
    Me.cmb_currency.TextWidth = 110
    '
    'chk_coin_collection
    '
    Me.chk_coin_collection.AutoSize = True
    Me.chk_coin_collection.Location = New System.Drawing.Point(14, 323)
    Me.chk_coin_collection.Name = "chk_coin_collection"
    Me.chk_coin_collection.Size = New System.Drawing.Size(251, 17)
    Me.chk_coin_collection.TabIndex = 13
    Me.chk_coin_collection.Text = "#Habilitar recaudaciones de monedas#"
    Me.chk_coin_collection.UseVisualStyleBackColor = True
    Me.chk_coin_collection.Visible = False
    '
    'ef_game_theme
    '
    Me.ef_game_theme.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.ef_game_theme.DoubleValue = 0.0R
    Me.ef_game_theme.IntegerValue = 0
    Me.ef_game_theme.IsReadOnly = False
    Me.ef_game_theme.Location = New System.Drawing.Point(14, 197)
    Me.ef_game_theme.Name = "ef_game_theme"
    Me.ef_game_theme.PlaceHolder = Nothing
    Me.ef_game_theme.ShortcutsEnabled = True
    Me.ef_game_theme.Size = New System.Drawing.Size(336, 24)
    Me.ef_game_theme.SufixText = "Sufix Text"
    Me.ef_game_theme.SufixTextVisible = True
    Me.ef_game_theme.TabIndex = 6
    Me.ef_game_theme.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_game_theme.TextValue = ""
    Me.ef_game_theme.TextWidth = 180
    Me.ef_game_theme.Value = ""
    Me.ef_game_theme.ValueForeColor = System.Drawing.Color.Blue
    '
    'opt_multi
    '
    Me.opt_multi.AutoSize = True
    Me.opt_multi.Location = New System.Drawing.Point(57, 324)
    Me.opt_multi.Name = "opt_multi"
    Me.opt_multi.Size = New System.Drawing.Size(135, 17)
    Me.opt_multi.TabIndex = 11
    Me.opt_multi.TabStop = True
    Me.opt_multi.Text = "xMultidenomination"
    Me.opt_multi.UseVisualStyleBackColor = True
    '
    'opt_single
    '
    Me.opt_single.AutoSize = True
    Me.opt_single.Location = New System.Drawing.Point(57, 294)
    Me.opt_single.Name = "opt_single"
    Me.opt_single.Size = New System.Drawing.Size(111, 17)
    Me.opt_single.TabIndex = 9
    Me.opt_single.TabStop = True
    Me.opt_single.Text = "xDenomination"
    Me.opt_single.UseVisualStyleBackColor = True
    '
    'cmb_multi_denomination
    '
    Me.cmb_multi_denomination.AllowUnlistedValues = False
    Me.cmb_multi_denomination.AutoCompleteMode = False
    Me.cmb_multi_denomination.Enabled = False
    Me.cmb_multi_denomination.IsReadOnly = False
    Me.cmb_multi_denomination.Location = New System.Drawing.Point(194, 316)
    Me.cmb_multi_denomination.Name = "cmb_multi_denomination"
    Me.cmb_multi_denomination.SelectedIndex = -1
    Me.cmb_multi_denomination.Size = New System.Drawing.Size(156, 24)
    Me.cmb_multi_denomination.SufixText = "Sufix Text"
    Me.cmb_multi_denomination.SufixTextVisible = True
    Me.cmb_multi_denomination.TabIndex = 12
    Me.cmb_multi_denomination.TextCombo = Nothing
    Me.cmb_multi_denomination.TextWidth = 0
    '
    'ef_number_lines
    '
    Me.ef_number_lines.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.ef_number_lines.DoubleValue = 0.0R
    Me.ef_number_lines.IntegerValue = 0
    Me.ef_number_lines.IsReadOnly = False
    Me.ef_number_lines.Location = New System.Drawing.Point(14, 227)
    Me.ef_number_lines.Name = "ef_number_lines"
    Me.ef_number_lines.PlaceHolder = Nothing
    Me.ef_number_lines.ShortcutsEnabled = True
    Me.ef_number_lines.Size = New System.Drawing.Size(336, 24)
    Me.ef_number_lines.SufixText = "Sufix Text"
    Me.ef_number_lines.SufixTextVisible = True
    Me.ef_number_lines.TabIndex = 7
    Me.ef_number_lines.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_number_lines.TextValue = ""
    Me.ef_number_lines.TextWidth = 180
    Me.ef_number_lines.Value = ""
    Me.ef_number_lines.ValueForeColor = System.Drawing.Color.Blue
    '
    'cmb_single_denomination
    '
    Me.cmb_single_denomination.AllowUnlistedValues = False
    Me.cmb_single_denomination.AutoCompleteMode = False
    Me.cmb_single_denomination.Enabled = False
    Me.cmb_single_denomination.IsReadOnly = False
    Me.cmb_single_denomination.Location = New System.Drawing.Point(194, 286)
    Me.cmb_single_denomination.Name = "cmb_single_denomination"
    Me.cmb_single_denomination.SelectedIndex = -1
    Me.cmb_single_denomination.Size = New System.Drawing.Size(156, 24)
    Me.cmb_single_denomination.SufixText = "Sufix Text"
    Me.cmb_single_denomination.SufixTextVisible = True
    Me.cmb_single_denomination.TabIndex = 10
    Me.cmb_single_denomination.TextCombo = Nothing
    Me.cmb_single_denomination.TextWidth = 0
    '
    'ef_equity_percentage
    '
    Me.ef_equity_percentage.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.ef_equity_percentage.DoubleValue = 0.0R
    Me.ef_equity_percentage.IntegerValue = 0
    Me.ef_equity_percentage.IsReadOnly = False
    Me.ef_equity_percentage.Location = New System.Drawing.Point(14, 349)
    Me.ef_equity_percentage.Name = "ef_equity_percentage"
    Me.ef_equity_percentage.PlaceHolder = Nothing
    Me.ef_equity_percentage.ShortcutsEnabled = True
    Me.ef_equity_percentage.Size = New System.Drawing.Size(336, 24)
    Me.ef_equity_percentage.SufixText = "Sufix Text"
    Me.ef_equity_percentage.SufixTextVisible = True
    Me.ef_equity_percentage.TabIndex = 26
    Me.ef_equity_percentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_equity_percentage.TextValue = ""
    Me.ef_equity_percentage.TextWidth = 180
    Me.ef_equity_percentage.Value = ""
    Me.ef_equity_percentage.ValueForeColor = System.Drawing.Color.Blue
    Me.ef_equity_percentage.Visible = False
    '
    'gb_communication_type
    '
    Me.gb_communication_type.Controls.Add(Me.cmb_smib_configuration)
    Me.gb_communication_type.Controls.Add(Me.cmb_communication_type)
    Me.gb_communication_type.Location = New System.Drawing.Point(13, 720)
    Me.gb_communication_type.Name = "gb_communication_type"
    Me.gb_communication_type.Size = New System.Drawing.Size(375, 81)
    Me.gb_communication_type.TabIndex = 17
    Me.gb_communication_type.TabStop = False
    Me.gb_communication_type.Text = "xCommunicationType"
    Me.gb_communication_type.Visible = False
    '
    'cmb_smib_configuration
    '
    Me.cmb_smib_configuration.AllowUnlistedValues = False
    Me.cmb_smib_configuration.AutoCompleteMode = False
    Me.cmb_smib_configuration.IsReadOnly = False
    Me.cmb_smib_configuration.Location = New System.Drawing.Point(6, 43)
    Me.cmb_smib_configuration.Name = "cmb_smib_configuration"
    Me.cmb_smib_configuration.SelectedIndex = -1
    Me.cmb_smib_configuration.Size = New System.Drawing.Size(348, 24)
    Me.cmb_smib_configuration.SufixText = "Sufix Text"
    Me.cmb_smib_configuration.SufixTextVisible = True
    Me.cmb_smib_configuration.TabIndex = 0
    Me.cmb_smib_configuration.TextCombo = Nothing
    Me.cmb_smib_configuration.TextWidth = 131
    '
    'cmb_communication_type
    '
    Me.cmb_communication_type.AllowUnlistedValues = False
    Me.cmb_communication_type.AutoCompleteMode = False
    Me.cmb_communication_type.IsReadOnly = False
    Me.cmb_communication_type.Location = New System.Drawing.Point(6, 13)
    Me.cmb_communication_type.Name = "cmb_communication_type"
    Me.cmb_communication_type.SelectedIndex = -1
    Me.cmb_communication_type.Size = New System.Drawing.Size(348, 24)
    Me.cmb_communication_type.SufixText = "Sufix Text"
    Me.cmb_communication_type.SufixTextVisible = True
    Me.cmb_communication_type.TabIndex = 0
    Me.cmb_communication_type.TextCombo = Nothing
    Me.cmb_communication_type.TextWidth = 131
    '
    'ef_brand_code
    '
    Me.ef_brand_code.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.ef_brand_code.DoubleValue = 0.0R
    Me.ef_brand_code.IntegerValue = 0
    Me.ef_brand_code.IsReadOnly = False
    Me.ef_brand_code.Location = New System.Drawing.Point(5, 262)
    Me.ef_brand_code.Name = "ef_brand_code"
    Me.ef_brand_code.PlaceHolder = Nothing
    Me.ef_brand_code.ShortcutsEnabled = True
    Me.ef_brand_code.Size = New System.Drawing.Size(368, 24)
    Me.ef_brand_code.SufixText = "Sufix Text"
    Me.ef_brand_code.SufixTextVisible = True
    Me.ef_brand_code.TabIndex = 9
    Me.ef_brand_code.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_brand_code.TextValue = ""
    Me.ef_brand_code.TextWidth = 124
    Me.ef_brand_code.Value = ""
    Me.ef_brand_code.ValueForeColor = System.Drawing.Color.Blue
    Me.ef_brand_code.Visible = False
    '
    'ef_model
    '
    Me.ef_model.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.ef_model.DoubleValue = 0.0R
    Me.ef_model.IntegerValue = 0
    Me.ef_model.IsReadOnly = False
    Me.ef_model.Location = New System.Drawing.Point(5, 290)
    Me.ef_model.Name = "ef_model"
    Me.ef_model.PlaceHolder = Nothing
    Me.ef_model.ShortcutsEnabled = True
    Me.ef_model.Size = New System.Drawing.Size(368, 24)
    Me.ef_model.SufixText = "Sufix Text"
    Me.ef_model.SufixTextVisible = True
    Me.ef_model.TabIndex = 10
    Me.ef_model.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_model.TextValue = ""
    Me.ef_model.TextWidth = 124
    Me.ef_model.Value = ""
    Me.ef_model.ValueForeColor = System.Drawing.Color.Blue
    Me.ef_model.Visible = False
    '
    'ef_manufacture_year
    '
    Me.ef_manufacture_year.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.ef_manufacture_year.DoubleValue = 0.0R
    Me.ef_manufacture_year.IntegerValue = 0
    Me.ef_manufacture_year.IsReadOnly = False
    Me.ef_manufacture_year.Location = New System.Drawing.Point(5, 318)
    Me.ef_manufacture_year.Name = "ef_manufacture_year"
    Me.ef_manufacture_year.PlaceHolder = Nothing
    Me.ef_manufacture_year.ShortcutsEnabled = True
    Me.ef_manufacture_year.Size = New System.Drawing.Size(368, 24)
    Me.ef_manufacture_year.SufixText = "Sufix Text"
    Me.ef_manufacture_year.SufixTextVisible = True
    Me.ef_manufacture_year.TabIndex = 11
    Me.ef_manufacture_year.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_manufacture_year.TextValue = ""
    Me.ef_manufacture_year.TextWidth = 124
    Me.ef_manufacture_year.Value = ""
    Me.ef_manufacture_year.ValueForeColor = System.Drawing.Color.Blue
    Me.ef_manufacture_year.Visible = False
    '
    'chk_met_homologated
    '
    Me.chk_met_homologated.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_met_homologated.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.chk_met_homologated.Location = New System.Drawing.Point(131, 376)
    Me.chk_met_homologated.Name = "chk_met_homologated"
    Me.chk_met_homologated.RightToLeft = System.Windows.Forms.RightToLeft.Yes
    Me.chk_met_homologated.Size = New System.Drawing.Size(191, 18)
    Me.chk_met_homologated.TabIndex = 13
    Me.chk_met_homologated.Text = "xMETHomologated"
    Me.chk_met_homologated.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_met_homologated.Visible = False
    '
    'ef_bet_code
    '
    Me.ef_bet_code.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.ef_bet_code.DoubleValue = 0.0R
    Me.ef_bet_code.IntegerValue = 0
    Me.ef_bet_code.IsReadOnly = False
    Me.ef_bet_code.Location = New System.Drawing.Point(5, 347)
    Me.ef_bet_code.Name = "ef_bet_code"
    Me.ef_bet_code.PlaceHolder = Nothing
    Me.ef_bet_code.ShortcutsEnabled = True
    Me.ef_bet_code.Size = New System.Drawing.Size(368, 24)
    Me.ef_bet_code.SufixText = "Sufix Text"
    Me.ef_bet_code.SufixTextVisible = True
    Me.ef_bet_code.TabIndex = 12
    Me.ef_bet_code.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_bet_code.TextValue = ""
    Me.ef_bet_code.TextWidth = 124
    Me.ef_bet_code.Value = ""
    Me.ef_bet_code.ValueForeColor = System.Drawing.Color.Blue
    Me.ef_bet_code.Visible = False
    '
    'gb_3GS
    '
    Me.gb_3GS.Controls.Add(Me.ef_reported_3gs_serial_number)
    Me.gb_3GS.Location = New System.Drawing.Point(10, 875)
    Me.gb_3GS.Name = "gb_3GS"
    Me.gb_3GS.Size = New System.Drawing.Size(361, 56)
    Me.gb_3GS.TabIndex = 22
    Me.gb_3GS.TabStop = False
    Me.gb_3GS.Text = "x3GS"
    '
    'ef_reported_3gs_serial_number
    '
    Me.ef_reported_3gs_serial_number.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.ef_reported_3gs_serial_number.DoubleValue = 0.0R
    Me.ef_reported_3gs_serial_number.IntegerValue = 0
    Me.ef_reported_3gs_serial_number.IsReadOnly = False
    Me.ef_reported_3gs_serial_number.Location = New System.Drawing.Point(26, 21)
    Me.ef_reported_3gs_serial_number.Name = "ef_reported_3gs_serial_number"
    Me.ef_reported_3gs_serial_number.PlaceHolder = Nothing
    Me.ef_reported_3gs_serial_number.ShortcutsEnabled = True
    Me.ef_reported_3gs_serial_number.Size = New System.Drawing.Size(327, 24)
    Me.ef_reported_3gs_serial_number.SufixText = "Sufix Text"
    Me.ef_reported_3gs_serial_number.SufixTextVisible = True
    Me.ef_reported_3gs_serial_number.TabIndex = 1
    Me.ef_reported_3gs_serial_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_reported_3gs_serial_number.TextValue = ""
    Me.ef_reported_3gs_serial_number.TextWidth = 120
    Me.ef_reported_3gs_serial_number.Value = ""
    Me.ef_reported_3gs_serial_number.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_ds_manufacturing_date
    '
    Me.uc_ds_manufacturing_date.Day = ""
    Me.uc_ds_manufacturing_date.Location = New System.Drawing.Point(46, 204)
    Me.uc_ds_manufacturing_date.Margin = New System.Windows.Forms.Padding(0)
    Me.uc_ds_manufacturing_date.Month = ""
    Me.uc_ds_manufacturing_date.Name = "uc_ds_manufacturing_date"
    Me.uc_ds_manufacturing_date.Size = New System.Drawing.Size(328, 25)
    Me.uc_ds_manufacturing_date.TabIndex = 23
    Me.uc_ds_manufacturing_date.Year = ""
    '
    'lbl_date_manufacturing
    '
    Me.lbl_date_manufacturing.Location = New System.Drawing.Point(3, 203)
    Me.lbl_date_manufacturing.Name = "lbl_date_manufacturing"
    Me.lbl_date_manufacturing.Size = New System.Drawing.Size(126, 24)
    Me.lbl_date_manufacturing.TabIndex = 24
    Me.lbl_date_manufacturing.Text = "Text"
    Me.lbl_date_manufacturing.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'frm_terminals_edition
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.AutoScroll = True
    Me.ClientSize = New System.Drawing.Size(865, 1053)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
    Me.Name = "frm_terminals_edition"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_terminals_edit_status"
    Me.panel_data.ResumeLayout(False)
    Me.gb_dates.ResumeLayout(False)
    Me.gb_dates.PerformLayout()
    Me.gb_area_island.ResumeLayout(False)
    Me.gb_SAS.ResumeLayout(False)
    Me.gb_SAS.PerformLayout()
    Me.gb_serial_number.ResumeLayout(False)
    Me.gb_asset_number.ResumeLayout(False)
    Me.gb_contract.ResumeLayout(False)
    Me.gb_ticket_configuration.ResumeLayout(False)
    Me.gb_ticket_configuration.PerformLayout()
    Me.gb_signature_validation.ResumeLayout(False)
    Me.gb_signature_validation.PerformLayout()
    Me.gb_denomination.ResumeLayout(False)
    Me.gb_denomination.PerformLayout()
    Me.gb_communication_type.ResumeLayout(False)
    Me.gb_3GS.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents cmb_status As GUI_Controls.uc_combo
  Private WithEvents btn_ok As GUI_Controls.uc_button
  Private WithEvents btn_cancel As GUI_Controls.uc_button
  Friend WithEvents ef_name As GUI_Controls.uc_entry_field
  Friend WithEvents ef_provider_id As GUI_Controls.uc_entry_field
  Friend WithEvents ef_external_id As GUI_Controls.uc_entry_field
  Friend WithEvents ef_vendor_id As GUI_Controls.uc_entry_field
  Friend WithEvents chk_blocked As System.Windows.Forms.CheckBox
  Friend WithEvents ef_terminal_type_build As GUI_Controls.uc_entry_field
  Friend WithEvents gb_dates As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_retirement_date As GUI_Controls.uc_date_picker
  Friend WithEvents ef_retirement_requested As GUI_Controls.uc_entry_field
  Friend WithEvents ef_program As GUI_Controls.uc_entry_field
  Friend WithEvents ef_floor_id As GUI_Controls.uc_entry_field
  Friend WithEvents ef_retirement_date As GUI_Controls.uc_entry_field
  Friend WithEvents ef_activation_date As GUI_Controls.uc_entry_field
  Friend WithEvents ef_smoking As GUI_Controls.uc_entry_field
  Friend WithEvents cmb_bank As GUI_Controls.uc_combo
  Friend WithEvents cmb_area As GUI_Controls.uc_combo
  Friend WithEvents gb_area_island As System.Windows.Forms.GroupBox
  Friend WithEvents ef_tax_registration As GUI_Controls.uc_entry_field
  Friend WithEvents gb_SAS As System.Windows.Forms.GroupBox
  Friend WithEvents ef_reported_serial_number As GUI_Controls.uc_entry_field
  Friend WithEvents cmb_provider As GUI_Controls.uc_combo
  Friend WithEvents gb_contract As System.Windows.Forms.GroupBox
  Friend WithEvents ef_contract_id As GUI_Controls.uc_entry_field
  Friend WithEvents cmb_contract_type As GUI_Controls.uc_combo
  Friend WithEvents ef_order_number As GUI_Controls.uc_entry_field
  Friend WithEvents gb_ticket_configuration As System.Windows.Forms.GroupBox
  Friend WithEvents chk_allow_redemption As System.Windows.Forms.CheckBox
  Friend WithEvents opt_default As System.Windows.Forms.RadioButton
  Friend WithEvents opt_personalized As System.Windows.Forms.RadioButton
  Friend WithEvents chk_allow_promotional_emission As System.Windows.Forms.CheckBox
  Friend WithEvents chk_allow_cashable_emission As System.Windows.Forms.CheckBox
  Friend WithEvents ef_max_ticket_out As GUI_Controls.uc_entry_field
  Friend WithEvents ef_max_ticket_in As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_ticket_out_info As System.Windows.Forms.Label
  Friend WithEvents uc_sas_flags As GUI_Controls.uc_sas_flags
  Friend WithEvents gb_signature_validation As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_authentication_signature As System.Windows.Forms.Label
  Friend WithEvents lbl_authentication_seed As System.Windows.Forms.Label
  Friend WithEvents lbl_authentication_method As System.Windows.Forms.Label
  Friend WithEvents lbl_authentication_status As System.Windows.Forms.Label
  Friend WithEvents lbl_authentication_last_checked As System.Windows.Forms.Label
  Friend WithEvents lbl_machine_blocked_reason As System.Windows.Forms.Label
  Friend WithEvents chk_machine_blocked As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_authentication_status_detail As System.Windows.Forms.Label
  Friend WithEvents ef_machine_id As GUI_Controls.uc_entry_field
  Friend WithEvents ef_position As GUI_Controls.uc_entry_field
  Friend WithEvents gb_denomination As System.Windows.Forms.GroupBox
  Friend WithEvents cmb_single_denomination As GUI_Controls.uc_combo
  Friend WithEvents ef_max_bet As GUI_Controls.uc_entry_field
  Friend WithEvents ef_top_award As GUI_Controls.uc_entry_field
  Friend WithEvents ef_jackpot_contribution_pct As GUI_Controls.uc_entry_field
  Friend WithEvents ef_cabinet_type As GUI_Controls.uc_entry_field
  Friend WithEvents cmb_game_type As GUI_Controls.uc_combo
  Friend WithEvents ef_theoretical_payout As GUI_Controls.uc_entry_field
  Friend WithEvents ef_number_lines As GUI_Controls.uc_entry_field
  Friend WithEvents cmb_multi_denomination As GUI_Controls.uc_combo
  Friend WithEvents opt_multi As System.Windows.Forms.RadioButton
  Friend WithEvents opt_single As System.Windows.Forms.RadioButton
  Friend WithEvents ef_game_theme As GUI_Controls.uc_entry_field
  Friend WithEvents gb_serial_number As System.Windows.Forms.GroupBox
  Friend WithEvents btn_reset_serial_number As GUI_Controls.uc_button
  Friend WithEvents ef_configured_serial_number As GUI_Controls.uc_entry_field
  Protected WithEvents cmb_meter_delta As GUI_Controls.uc_combo
  Friend WithEvents lbl_meter_delta As System.Windows.Forms.Label
  Friend WithEvents gb_asset_number As System.Windows.Forms.GroupBox
  Friend WithEvents btn_reset_asset_number As GUI_Controls.uc_button
  Friend WithEvents ef_configured_asset_number As GUI_Controls.uc_entry_field
  Friend WithEvents ef_reported_asset_number As GUI_Controls.uc_entry_field
  Friend WithEvents gb_communication_type As System.Windows.Forms.GroupBox
  Friend WithEvents cmb_communication_type As GUI_Controls.uc_combo
  Friend WithEvents cmb_smib_configuration As GUI_Controls.uc_combo
  Friend WithEvents lbl_blocked As System.Windows.Forms.Label
  Friend WithEvents ef_bet_code As GUI_Controls.uc_entry_field
  Friend WithEvents chk_coin_collection As System.Windows.Forms.CheckBox
  Friend WithEvents gb_3GS As System.Windows.Forms.GroupBox
  Friend WithEvents ef_reported_3gs_serial_number As GUI_Controls.uc_entry_field
  Friend WithEvents cmb_currency As GUI_Controls.uc_combo
  Friend WithEvents ef_accounting_denomination As GUI_Controls.uc_entry_field
  Friend WithEvents ef_equity_percentage As GUI_Controls.uc_entry_field
  Friend WithEvents ef_min_denomination As GUI_Controls.uc_entry_field
  Friend WithEvents ef_sas_host_id As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_allow_truncate As System.Windows.Forms.Label
  Friend WithEvents ef_min_ticket_in As GUI_Controls.uc_entry_field
  Friend WithEvents chk_allow_truncate As System.Windows.Forms.CheckBox
  Friend WithEvents chk_equity_percentage As System.Windows.Forms.CheckBox
  Friend WithEvents uc_ds_manufacturing_date As GUI_Controls.uc_date_select
  Friend WithEvents ef_replacement_date As GUI_Controls.uc_entry_field
  Friend WithEvents ef_creation_date As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_date_manufacturing As System.Windows.Forms.Label
End Class
