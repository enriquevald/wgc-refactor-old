<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_currency_configuration
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
    Me.ef_national_currency = New GUI_Controls.uc_entry_field()
    Me.tab_national_currency = New System.Windows.Forms.TabControl()
    Me.TabBankCard = New System.Windows.Forms.TabPage()
    Me.tab_bank_card_vouchers = New System.Windows.Forms.TabControl()
    Me.TabRecharge = New System.Windows.Forms.TabPage()
    Me.chk_bank_card_voucher = New System.Windows.Forms.CheckBox()
    Me.ef_bank_card_voucher_title = New GUI_Controls.uc_entry_field()
    Me.TabCashAdvance = New System.Windows.Forms.TabPage()
    Me.chk_bank_card_cash_advance_hide_voucher = New System.Windows.Forms.CheckBox()
    Me.ef_bank_card_cash_advance_voucher_title = New GUI_Controls.uc_entry_field()
    Me.TabCardComission = New System.Windows.Forms.TabPage()
    Me.uc_bank_card_commission = New GUI_Controls.uc_entry_field()
    Me.chk_bank_data_input = New System.Windows.Forms.CheckBox()
    Me.chk_card_specification = New System.Windows.Forms.CheckBox()
    Me.chk_credit_card = New System.Windows.Forms.CheckBox()
    Me.TabCheck = New System.Windows.Forms.TabPage()
    Me.tab_check_vouchers = New System.Windows.Forms.TabControl()
    Me.TabCheckRecharge = New System.Windows.Forms.TabPage()
    Me.chk_check_voucher = New System.Windows.Forms.CheckBox()
    Me.ef_check_voucher_title = New GUI_Controls.uc_entry_field()
    Me.TabCheckCardAdvance = New System.Windows.Forms.TabPage()
    Me.chk_check_cash_advance_hide_voucher = New System.Windows.Forms.CheckBox()
    Me.ef_check_cash_advance_voucher_title = New GUI_Controls.uc_entry_field()
    Me.TabCheckCommission = New System.Windows.Forms.TabPage()
    Me.uc_check_commission = New GUI_Controls.uc_entry_field()
    Me.chk_bank_check_data = New System.Windows.Forms.CheckBox()
    Me.chk_check = New System.Windows.Forms.CheckBox()
    Me.tab_currency_exchange_vouchers = New System.Windows.Forms.TabControl()
    Me.TabExchangeRecharge = New System.Windows.Forms.TabPage()
    Me.chk_foreign_currency_voucher = New System.Windows.Forms.CheckBox()
    Me.ef_foreign_currency_voucher_title = New GUI_Controls.uc_entry_field()
    Me.TabExchangeCommission = New System.Windows.Forms.TabPage()
    Me.uc_exchange_commission = New GUI_Controls.uc_entry_field()
    Me.gb_foreign_currency = New System.Windows.Forms.GroupBox()
    Me.lbl_order = New System.Windows.Forms.Label()
    Me.uc_button_right = New System.Windows.Forms.Button()
    Me.uc_button_left = New System.Windows.Forms.Button()
    Me.CurrencyTab = New System.Windows.Forms.TabControl()
    Me.Tab_Credit_Card_Type = New System.Windows.Forms.TabControl()
    Me.panel_data.SuspendLayout()
    Me.tab_national_currency.SuspendLayout()
    Me.TabBankCard.SuspendLayout()
    Me.tab_bank_card_vouchers.SuspendLayout()
    Me.TabRecharge.SuspendLayout()
    Me.TabCashAdvance.SuspendLayout()
    Me.TabCardComission.SuspendLayout()
    Me.TabCheck.SuspendLayout()
    Me.tab_check_vouchers.SuspendLayout()
    Me.TabCheckRecharge.SuspendLayout()
    Me.TabCheckCardAdvance.SuspendLayout()
    Me.TabCheckCommission.SuspendLayout()
    Me.tab_currency_exchange_vouchers.SuspendLayout()
    Me.TabExchangeRecharge.SuspendLayout()
    Me.TabExchangeCommission.SuspendLayout()
    Me.gb_foreign_currency.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_foreign_currency)
    Me.panel_data.Controls.Add(Me.tab_national_currency)
    Me.panel_data.Controls.Add(Me.ef_national_currency)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(603, 816)
    '
    'Timer1
    '
    Me.Timer1.Enabled = True
    '
    'ef_national_currency
    '
    Me.ef_national_currency.DoubleValue = 0.0R
    Me.ef_national_currency.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_national_currency.IntegerValue = 0
    Me.ef_national_currency.IsReadOnly = False
    Me.ef_national_currency.Location = New System.Drawing.Point(118, 12)
    Me.ef_national_currency.Name = "ef_national_currency"
    Me.ef_national_currency.PlaceHolder = Nothing
    Me.ef_national_currency.Size = New System.Drawing.Size(331, 25)
    Me.ef_national_currency.SufixText = "Sufix Text"
    Me.ef_national_currency.SufixTextVisible = True
    Me.ef_national_currency.TabIndex = 0
    Me.ef_national_currency.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_national_currency.TextValue = ""
    Me.ef_national_currency.TextWidth = 110
    Me.ef_national_currency.Value = ""
    Me.ef_national_currency.ValueForeColor = System.Drawing.Color.Blue
    '
    'tab_national_currency
    '
    Me.tab_national_currency.Controls.Add(Me.TabBankCard)
    Me.tab_national_currency.Controls.Add(Me.TabCheck)
    Me.tab_national_currency.Location = New System.Drawing.Point(13, 44)
    Me.tab_national_currency.Name = "tab_national_currency"
    Me.tab_national_currency.SelectedIndex = 0
    Me.tab_national_currency.Size = New System.Drawing.Size(567, 323)
    Me.tab_national_currency.TabIndex = 3
    '
    'TabBankCard
    '
    Me.TabBankCard.Controls.Add(Me.Tab_Credit_Card_Type)
    Me.TabBankCard.Controls.Add(Me.tab_bank_card_vouchers)
    Me.TabBankCard.Controls.Add(Me.chk_bank_data_input)
    Me.TabBankCard.Controls.Add(Me.chk_card_specification)
    Me.TabBankCard.Controls.Add(Me.chk_credit_card)
    Me.TabBankCard.Location = New System.Drawing.Point(4, 22)
    Me.TabBankCard.Name = "TabBankCard"
    Me.TabBankCard.Padding = New System.Windows.Forms.Padding(3)
    Me.TabBankCard.Size = New System.Drawing.Size(559, 297)
    Me.TabBankCard.TabIndex = 0
    Me.TabBankCard.Text = "xTabCreditCard"
    Me.TabBankCard.UseVisualStyleBackColor = True
    '
    'tab_bank_card_vouchers
    '
    Me.tab_bank_card_vouchers.Controls.Add(Me.TabRecharge)
    Me.tab_bank_card_vouchers.Controls.Add(Me.TabCashAdvance)
    Me.tab_bank_card_vouchers.Controls.Add(Me.TabCardComission)
    Me.tab_bank_card_vouchers.Location = New System.Drawing.Point(18, 225)
    Me.tab_bank_card_vouchers.Name = "tab_bank_card_vouchers"
    Me.tab_bank_card_vouchers.SelectedIndex = 0
    Me.tab_bank_card_vouchers.Size = New System.Drawing.Size(503, 69)
    Me.tab_bank_card_vouchers.TabIndex = 32
    '
    'TabRecharge
    '
    Me.TabRecharge.Controls.Add(Me.chk_bank_card_voucher)
    Me.TabRecharge.Controls.Add(Me.ef_bank_card_voucher_title)
    Me.TabRecharge.Location = New System.Drawing.Point(4, 22)
    Me.TabRecharge.Name = "TabRecharge"
    Me.TabRecharge.Padding = New System.Windows.Forms.Padding(3)
    Me.TabRecharge.Size = New System.Drawing.Size(495, 43)
    Me.TabRecharge.TabIndex = 0
    Me.TabRecharge.Text = "xTabRecharge"
    Me.TabRecharge.UseVisualStyleBackColor = True
    '
    'chk_bank_card_voucher
    '
    Me.chk_bank_card_voucher.AutoSize = True
    Me.chk_bank_card_voucher.Location = New System.Drawing.Point(18, 15)
    Me.chk_bank_card_voucher.Name = "chk_bank_card_voucher"
    Me.chk_bank_card_voucher.Size = New System.Drawing.Size(162, 17)
    Me.chk_bank_card_voucher.TabIndex = 26
    Me.chk_bank_card_voucher.Text = "xBankCardHideVoucher"
    Me.chk_bank_card_voucher.UseVisualStyleBackColor = True
    '
    'ef_bank_card_voucher_title
    '
    Me.ef_bank_card_voucher_title.DoubleValue = 0.0R
    Me.ef_bank_card_voucher_title.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_bank_card_voucher_title.IntegerValue = 0
    Me.ef_bank_card_voucher_title.IsReadOnly = False
    Me.ef_bank_card_voucher_title.Location = New System.Drawing.Point(133, 11)
    Me.ef_bank_card_voucher_title.Name = "ef_bank_card_voucher_title"
    Me.ef_bank_card_voucher_title.PlaceHolder = Nothing
    Me.ef_bank_card_voucher_title.Size = New System.Drawing.Size(331, 25)
    Me.ef_bank_card_voucher_title.SufixText = "Sufix Text"
    Me.ef_bank_card_voucher_title.SufixTextVisible = True
    Me.ef_bank_card_voucher_title.TabIndex = 27
    Me.ef_bank_card_voucher_title.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_bank_card_voucher_title.TextValue = ""
    Me.ef_bank_card_voucher_title.TextVisible = False
    Me.ef_bank_card_voucher_title.TextWidth = 100
    Me.ef_bank_card_voucher_title.Value = ""
    Me.ef_bank_card_voucher_title.ValueForeColor = System.Drawing.Color.Blue
    '
    'TabCashAdvance
    '
    Me.TabCashAdvance.Controls.Add(Me.chk_bank_card_cash_advance_hide_voucher)
    Me.TabCashAdvance.Controls.Add(Me.ef_bank_card_cash_advance_voucher_title)
    Me.TabCashAdvance.Location = New System.Drawing.Point(4, 22)
    Me.TabCashAdvance.Name = "TabCashAdvance"
    Me.TabCashAdvance.Padding = New System.Windows.Forms.Padding(3)
    Me.TabCashAdvance.Size = New System.Drawing.Size(495, 43)
    Me.TabCashAdvance.TabIndex = 1
    Me.TabCashAdvance.Text = "xTabCashAdvance"
    Me.TabCashAdvance.UseVisualStyleBackColor = True
    '
    'chk_bank_card_cash_advance_hide_voucher
    '
    Me.chk_bank_card_cash_advance_hide_voucher.AutoSize = True
    Me.chk_bank_card_cash_advance_hide_voucher.Location = New System.Drawing.Point(18, 15)
    Me.chk_bank_card_cash_advance_hide_voucher.Name = "chk_bank_card_cash_advance_hide_voucher"
    Me.chk_bank_card_cash_advance_hide_voucher.Size = New System.Drawing.Size(162, 17)
    Me.chk_bank_card_cash_advance_hide_voucher.TabIndex = 26
    Me.chk_bank_card_cash_advance_hide_voucher.Text = "xBankCardHideVoucher"
    Me.chk_bank_card_cash_advance_hide_voucher.UseVisualStyleBackColor = True
    '
    'ef_bank_card_cash_advance_voucher_title
    '
    Me.ef_bank_card_cash_advance_voucher_title.DoubleValue = 0.0R
    Me.ef_bank_card_cash_advance_voucher_title.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_bank_card_cash_advance_voucher_title.IntegerValue = 0
    Me.ef_bank_card_cash_advance_voucher_title.IsReadOnly = False
    Me.ef_bank_card_cash_advance_voucher_title.Location = New System.Drawing.Point(133, 11)
    Me.ef_bank_card_cash_advance_voucher_title.Name = "ef_bank_card_cash_advance_voucher_title"
    Me.ef_bank_card_cash_advance_voucher_title.PlaceHolder = Nothing
    Me.ef_bank_card_cash_advance_voucher_title.Size = New System.Drawing.Size(331, 25)
    Me.ef_bank_card_cash_advance_voucher_title.SufixText = "Sufix Text"
    Me.ef_bank_card_cash_advance_voucher_title.TabIndex = 27
    Me.ef_bank_card_cash_advance_voucher_title.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_bank_card_cash_advance_voucher_title.TextValue = ""
    Me.ef_bank_card_cash_advance_voucher_title.TextVisible = False
    Me.ef_bank_card_cash_advance_voucher_title.TextWidth = 100
    Me.ef_bank_card_cash_advance_voucher_title.Value = ""
    Me.ef_bank_card_cash_advance_voucher_title.ValueForeColor = System.Drawing.Color.Blue
    '
    'TabCardComission
    '
    Me.TabCardComission.Controls.Add(Me.uc_bank_card_commission)
    Me.TabCardComission.Location = New System.Drawing.Point(4, 22)
    Me.TabCardComission.Name = "TabCardComission"
    Me.TabCardComission.Padding = New System.Windows.Forms.Padding(3)
    Me.TabCardComission.Size = New System.Drawing.Size(495, 43)
    Me.TabCardComission.TabIndex = 2
    Me.TabCardComission.Text = "xTabCardComission"
    Me.TabCardComission.UseVisualStyleBackColor = True
    '
    'uc_bank_card_commission
    '
    Me.uc_bank_card_commission.DoubleValue = 0.0R
    Me.uc_bank_card_commission.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_bank_card_commission.IntegerValue = 0
    Me.uc_bank_card_commission.IsReadOnly = False
    Me.uc_bank_card_commission.Location = New System.Drawing.Point(8, 11)
    Me.uc_bank_card_commission.Name = "uc_bank_card_commission"
    Me.uc_bank_card_commission.PlaceHolder = Nothing
    Me.uc_bank_card_commission.Size = New System.Drawing.Size(456, 25)
    Me.uc_bank_card_commission.SufixText = "Sufix Text"
    Me.uc_bank_card_commission.TabIndex = 29
    Me.uc_bank_card_commission.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_bank_card_commission.TextValue = ""
    Me.uc_bank_card_commission.TextVisible = False
    Me.uc_bank_card_commission.TextWidth = 100
    Me.uc_bank_card_commission.Value = ""
    Me.uc_bank_card_commission.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_bank_data_input
    '
    Me.chk_bank_data_input.AutoSize = True
    Me.chk_bank_data_input.Location = New System.Drawing.Point(30, 49)
    Me.chk_bank_data_input.Name = "chk_bank_data_input"
    Me.chk_bank_data_input.Size = New System.Drawing.Size(119, 17)
    Me.chk_bank_data_input.TabIndex = 29
    Me.chk_bank_data_input.Text = "xBankDataInput"
    Me.chk_bank_data_input.UseVisualStyleBackColor = True
    '
    'chk_card_specification
    '
    Me.chk_card_specification.AutoSize = True
    Me.chk_card_specification.Location = New System.Drawing.Point(30, 27)
    Me.chk_card_specification.Name = "chk_card_specification"
    Me.chk_card_specification.Size = New System.Drawing.Size(168, 17)
    Me.chk_card_specification.TabIndex = 28
    Me.chk_card_specification.Text = "xCreditCardSpecification"
    Me.chk_card_specification.UseVisualStyleBackColor = True
    '
    'chk_credit_card
    '
    Me.chk_credit_card.AutoSize = True
    Me.chk_credit_card.Location = New System.Drawing.Point(14, 6)
    Me.chk_credit_card.Name = "chk_credit_card"
    Me.chk_credit_card.Size = New System.Drawing.Size(96, 17)
    Me.chk_credit_card.TabIndex = 14
    Me.chk_credit_card.Text = "xCreditCard"
    Me.chk_credit_card.UseVisualStyleBackColor = True
    '
    'TabCheck
    '
    Me.TabCheck.Controls.Add(Me.tab_check_vouchers)
    Me.TabCheck.Controls.Add(Me.chk_bank_check_data)
    Me.TabCheck.Controls.Add(Me.chk_check)
    Me.TabCheck.Location = New System.Drawing.Point(4, 22)
    Me.TabCheck.Name = "TabCheck"
    Me.TabCheck.Padding = New System.Windows.Forms.Padding(3)
    Me.TabCheck.Size = New System.Drawing.Size(559, 297)
    Me.TabCheck.TabIndex = 1
    Me.TabCheck.Text = "xTabCheck"
    Me.TabCheck.UseVisualStyleBackColor = True
    '
    'tab_check_vouchers
    '
    Me.tab_check_vouchers.Controls.Add(Me.TabCheckRecharge)
    Me.tab_check_vouchers.Controls.Add(Me.TabCheckCardAdvance)
    Me.tab_check_vouchers.Controls.Add(Me.TabCheckCommission)
    Me.tab_check_vouchers.Location = New System.Drawing.Point(18, 225)
    Me.tab_check_vouchers.Name = "tab_check_vouchers"
    Me.tab_check_vouchers.SelectedIndex = 0
    Me.tab_check_vouchers.Size = New System.Drawing.Size(503, 69)
    Me.tab_check_vouchers.TabIndex = 45
    '
    'TabCheckRecharge
    '
    Me.TabCheckRecharge.Controls.Add(Me.chk_check_voucher)
    Me.TabCheckRecharge.Controls.Add(Me.ef_check_voucher_title)
    Me.TabCheckRecharge.Location = New System.Drawing.Point(4, 22)
    Me.TabCheckRecharge.Name = "TabCheckRecharge"
    Me.TabCheckRecharge.Padding = New System.Windows.Forms.Padding(3)
    Me.TabCheckRecharge.Size = New System.Drawing.Size(495, 43)
    Me.TabCheckRecharge.TabIndex = 0
    Me.TabCheckRecharge.Text = "xTabCheckRecharge"
    Me.TabCheckRecharge.UseVisualStyleBackColor = True
    '
    'chk_check_voucher
    '
    Me.chk_check_voucher.AutoSize = True
    Me.chk_check_voucher.Location = New System.Drawing.Point(18, 15)
    Me.chk_check_voucher.Name = "chk_check_voucher"
    Me.chk_check_voucher.Size = New System.Drawing.Size(141, 17)
    Me.chk_check_voucher.TabIndex = 40
    Me.chk_check_voucher.Text = "xCheckHideVoucher"
    Me.chk_check_voucher.UseVisualStyleBackColor = True
    '
    'ef_check_voucher_title
    '
    Me.ef_check_voucher_title.DoubleValue = 0.0R
    Me.ef_check_voucher_title.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_check_voucher_title.IntegerValue = 0
    Me.ef_check_voucher_title.IsReadOnly = False
    Me.ef_check_voucher_title.Location = New System.Drawing.Point(133, 11)
    Me.ef_check_voucher_title.Name = "ef_check_voucher_title"
    Me.ef_check_voucher_title.PlaceHolder = Nothing
    Me.ef_check_voucher_title.Size = New System.Drawing.Size(331, 25)
    Me.ef_check_voucher_title.SufixText = "Sufix Text"
    Me.ef_check_voucher_title.TabIndex = 41
    Me.ef_check_voucher_title.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_check_voucher_title.TextValue = ""
    Me.ef_check_voucher_title.TextVisible = False
    Me.ef_check_voucher_title.TextWidth = 100
    Me.ef_check_voucher_title.Value = ""
    Me.ef_check_voucher_title.ValueForeColor = System.Drawing.Color.Blue
    '
    'TabCheckCardAdvance
    '
    Me.TabCheckCardAdvance.Controls.Add(Me.chk_check_cash_advance_hide_voucher)
    Me.TabCheckCardAdvance.Controls.Add(Me.ef_check_cash_advance_voucher_title)
    Me.TabCheckCardAdvance.Location = New System.Drawing.Point(4, 22)
    Me.TabCheckCardAdvance.Name = "TabCheckCardAdvance"
    Me.TabCheckCardAdvance.Padding = New System.Windows.Forms.Padding(3)
    Me.TabCheckCardAdvance.Size = New System.Drawing.Size(495, 43)
    Me.TabCheckCardAdvance.TabIndex = 1
    Me.TabCheckCardAdvance.Text = "xTabCheckCardAdvance"
    Me.TabCheckCardAdvance.UseVisualStyleBackColor = True
    '
    'chk_check_cash_advance_hide_voucher
    '
    Me.chk_check_cash_advance_hide_voucher.AutoSize = True
    Me.chk_check_cash_advance_hide_voucher.Location = New System.Drawing.Point(18, 15)
    Me.chk_check_cash_advance_hide_voucher.Name = "chk_check_cash_advance_hide_voucher"
    Me.chk_check_cash_advance_hide_voucher.Size = New System.Drawing.Size(162, 17)
    Me.chk_check_cash_advance_hide_voucher.TabIndex = 26
    Me.chk_check_cash_advance_hide_voucher.Text = "xBankCardHideVoucher"
    Me.chk_check_cash_advance_hide_voucher.UseVisualStyleBackColor = True
    '
    'ef_check_cash_advance_voucher_title
    '
    Me.ef_check_cash_advance_voucher_title.DoubleValue = 0.0R
    Me.ef_check_cash_advance_voucher_title.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_check_cash_advance_voucher_title.IntegerValue = 0
    Me.ef_check_cash_advance_voucher_title.IsReadOnly = False
    Me.ef_check_cash_advance_voucher_title.Location = New System.Drawing.Point(133, 11)
    Me.ef_check_cash_advance_voucher_title.Name = "ef_check_cash_advance_voucher_title"
    Me.ef_check_cash_advance_voucher_title.PlaceHolder = Nothing
    Me.ef_check_cash_advance_voucher_title.Size = New System.Drawing.Size(331, 25)
    Me.ef_check_cash_advance_voucher_title.SufixText = "Sufix Text"
    Me.ef_check_cash_advance_voucher_title.TabIndex = 27
    Me.ef_check_cash_advance_voucher_title.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_check_cash_advance_voucher_title.TextValue = ""
    Me.ef_check_cash_advance_voucher_title.TextVisible = False
    Me.ef_check_cash_advance_voucher_title.TextWidth = 100
    Me.ef_check_cash_advance_voucher_title.Value = ""
    Me.ef_check_cash_advance_voucher_title.ValueForeColor = System.Drawing.Color.Blue
    '
    'TabCheckCommission
    '
    Me.TabCheckCommission.Controls.Add(Me.uc_check_commission)
    Me.TabCheckCommission.Location = New System.Drawing.Point(4, 22)
    Me.TabCheckCommission.Name = "TabCheckCommission"
    Me.TabCheckCommission.Size = New System.Drawing.Size(495, 43)
    Me.TabCheckCommission.TabIndex = 2
    Me.TabCheckCommission.Text = "xTabCheckCommission"
    Me.TabCheckCommission.UseVisualStyleBackColor = True
    '
    'uc_check_commission
    '
    Me.uc_check_commission.DoubleValue = 0.0R
    Me.uc_check_commission.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_check_commission.IntegerValue = 0
    Me.uc_check_commission.IsReadOnly = False
    Me.uc_check_commission.Location = New System.Drawing.Point(10, 9)
    Me.uc_check_commission.Name = "uc_check_commission"
    Me.uc_check_commission.PlaceHolder = Nothing
    Me.uc_check_commission.Size = New System.Drawing.Size(454, 25)
    Me.uc_check_commission.SufixText = "Sufix Text"
    Me.uc_check_commission.TabIndex = 30
    Me.uc_check_commission.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_check_commission.TextValue = ""
    Me.uc_check_commission.TextVisible = False
    Me.uc_check_commission.TextWidth = 100
    Me.uc_check_commission.Value = ""
    Me.uc_check_commission.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_bank_check_data
    '
    Me.chk_bank_check_data.AutoSize = True
    Me.chk_bank_check_data.Location = New System.Drawing.Point(30, 27)
    Me.chk_bank_check_data.Name = "chk_bank_check_data"
    Me.chk_bank_check_data.Size = New System.Drawing.Size(119, 17)
    Me.chk_bank_check_data.TabIndex = 42
    Me.chk_bank_check_data.Text = "xBankDataInput"
    Me.chk_bank_check_data.UseVisualStyleBackColor = True
    '
    'chk_check
    '
    Me.chk_check.AutoSize = True
    Me.chk_check.Location = New System.Drawing.Point(14, 6)
    Me.chk_check.Name = "chk_check"
    Me.chk_check.Size = New System.Drawing.Size(69, 17)
    Me.chk_check.TabIndex = 28
    Me.chk_check.Text = "xCheck"
    Me.chk_check.UseVisualStyleBackColor = True
    '
    'tab_currency_exchange_vouchers
    '
    Me.tab_currency_exchange_vouchers.Controls.Add(Me.TabExchangeRecharge)
    Me.tab_currency_exchange_vouchers.Controls.Add(Me.TabExchangeCommission)
    Me.tab_currency_exchange_vouchers.Location = New System.Drawing.Point(22, 359)
    Me.tab_currency_exchange_vouchers.Name = "tab_currency_exchange_vouchers"
    Me.tab_currency_exchange_vouchers.SelectedIndex = 0
    Me.tab_currency_exchange_vouchers.Size = New System.Drawing.Size(503, 69)
    Me.tab_currency_exchange_vouchers.TabIndex = 19
    '
    'TabExchangeRecharge
    '
    Me.TabExchangeRecharge.Controls.Add(Me.chk_foreign_currency_voucher)
    Me.TabExchangeRecharge.Controls.Add(Me.ef_foreign_currency_voucher_title)
    Me.TabExchangeRecharge.Location = New System.Drawing.Point(4, 22)
    Me.TabExchangeRecharge.Name = "TabExchangeRecharge"
    Me.TabExchangeRecharge.Padding = New System.Windows.Forms.Padding(3)
    Me.TabExchangeRecharge.Size = New System.Drawing.Size(495, 43)
    Me.TabExchangeRecharge.TabIndex = 0
    Me.TabExchangeRecharge.Text = "xTabExchangeRecharge"
    Me.TabExchangeRecharge.UseVisualStyleBackColor = True
    '
    'chk_foreign_currency_voucher
    '
    Me.chk_foreign_currency_voucher.AutoSize = True
    Me.chk_foreign_currency_voucher.Location = New System.Drawing.Point(18, 14)
    Me.chk_foreign_currency_voucher.Name = "chk_foreign_currency_voucher"
    Me.chk_foreign_currency_voucher.Size = New System.Drawing.Size(200, 17)
    Me.chk_foreign_currency_voucher.TabIndex = 17
    Me.chk_foreign_currency_voucher.Text = "xForeignCurrencyHideVoucher"
    Me.chk_foreign_currency_voucher.UseVisualStyleBackColor = True
    '
    'ef_foreign_currency_voucher_title
    '
    Me.ef_foreign_currency_voucher_title.DoubleValue = 0.0R
    Me.ef_foreign_currency_voucher_title.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_foreign_currency_voucher_title.IntegerValue = 0
    Me.ef_foreign_currency_voucher_title.IsReadOnly = False
    Me.ef_foreign_currency_voucher_title.Location = New System.Drawing.Point(133, 10)
    Me.ef_foreign_currency_voucher_title.Name = "ef_foreign_currency_voucher_title"
    Me.ef_foreign_currency_voucher_title.PlaceHolder = Nothing
    Me.ef_foreign_currency_voucher_title.Size = New System.Drawing.Size(331, 25)
    Me.ef_foreign_currency_voucher_title.SufixText = "Sufix Text"
    Me.ef_foreign_currency_voucher_title.SufixTextVisible = True
    Me.ef_foreign_currency_voucher_title.TabIndex = 18
    Me.ef_foreign_currency_voucher_title.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_foreign_currency_voucher_title.TextValue = ""
    Me.ef_foreign_currency_voucher_title.TextVisible = False
    Me.ef_foreign_currency_voucher_title.TextWidth = 100
    Me.ef_foreign_currency_voucher_title.Value = ""
    Me.ef_foreign_currency_voucher_title.ValueForeColor = System.Drawing.Color.Blue
    '
    'TabExchangeCommission
    '
    Me.TabExchangeCommission.Controls.Add(Me.uc_exchange_commission)
    Me.TabExchangeCommission.Location = New System.Drawing.Point(4, 22)
    Me.TabExchangeCommission.Name = "TabExchangeCommission"
    Me.TabExchangeCommission.Padding = New System.Windows.Forms.Padding(3)
    Me.TabExchangeCommission.Size = New System.Drawing.Size(495, 43)
    Me.TabExchangeCommission.TabIndex = 1
    Me.TabExchangeCommission.Text = "xTabExchangeCommission"
    Me.TabExchangeCommission.UseVisualStyleBackColor = True
    '
    'uc_exchange_commission
    '
    Me.uc_exchange_commission.DoubleValue = 0.0R
    Me.uc_exchange_commission.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_exchange_commission.IntegerValue = 0
    Me.uc_exchange_commission.IsReadOnly = False
    Me.uc_exchange_commission.Location = New System.Drawing.Point(8, 10)
    Me.uc_exchange_commission.Name = "uc_exchange_commission"
    Me.uc_exchange_commission.PlaceHolder = Nothing
    Me.uc_exchange_commission.Size = New System.Drawing.Size(456, 25)
    Me.uc_exchange_commission.SufixText = "Sufix Text"
    Me.uc_exchange_commission.TabIndex = 31
    Me.uc_exchange_commission.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_exchange_commission.TextValue = ""
    Me.uc_exchange_commission.TextVisible = False
    Me.uc_exchange_commission.TextWidth = 100
    Me.uc_exchange_commission.Value = ""
    Me.uc_exchange_commission.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_foreign_currency
    '
    Me.gb_foreign_currency.Controls.Add(Me.lbl_order)
    Me.gb_foreign_currency.Controls.Add(Me.uc_button_right)
    Me.gb_foreign_currency.Controls.Add(Me.uc_button_left)
    Me.gb_foreign_currency.Controls.Add(Me.CurrencyTab)
    Me.gb_foreign_currency.Controls.Add(Me.tab_currency_exchange_vouchers)
    Me.gb_foreign_currency.Location = New System.Drawing.Point(13, 373)
    Me.gb_foreign_currency.Name = "gb_foreign_currency"
    Me.gb_foreign_currency.Size = New System.Drawing.Size(567, 438)
    Me.gb_foreign_currency.TabIndex = 2
    Me.gb_foreign_currency.TabStop = False
    Me.gb_foreign_currency.Text = "xForeignCurrency"
    '
    'lbl_order
    '
    Me.lbl_order.AutoSize = True
    Me.lbl_order.Location = New System.Drawing.Point(386, 16)
    Me.lbl_order.Name = "lbl_order"
    Me.lbl_order.Size = New System.Drawing.Size(44, 13)
    Me.lbl_order.TabIndex = 22
    Me.lbl_order.Text = "Label1"
    '
    'uc_button_right
    '
    Me.uc_button_right.Location = New System.Drawing.Point(502, 11)
    Me.uc_button_right.Name = "uc_button_right"
    Me.uc_button_right.Size = New System.Drawing.Size(59, 23)
    Me.uc_button_right.TabIndex = 21
    Me.uc_button_right.Text = "Button2"
    Me.uc_button_right.UseVisualStyleBackColor = True
    '
    'uc_button_left
    '
    Me.uc_button_left.Location = New System.Drawing.Point(438, 11)
    Me.uc_button_left.Name = "uc_button_left"
    Me.uc_button_left.Size = New System.Drawing.Size(59, 23)
    Me.uc_button_left.TabIndex = 20
    Me.uc_button_left.Text = "Button1"
    Me.uc_button_left.UseVisualStyleBackColor = True
    '
    'CurrencyTab
    '
    Me.CurrencyTab.Location = New System.Drawing.Point(6, 37)
    Me.CurrencyTab.Margin = New System.Windows.Forms.Padding(0)
    Me.CurrencyTab.Name = "CurrencyTab"
    Me.CurrencyTab.SelectedIndex = 0
    Me.CurrencyTab.Size = New System.Drawing.Size(555, 308)
    Me.CurrencyTab.TabIndex = 5
    '
    'Tab_Credit_Card_Type
    '
    Me.Tab_Credit_Card_Type.Location = New System.Drawing.Point(6, 27)
    Me.Tab_Credit_Card_Type.Name = "Tab_Credit_Card_Type"
    Me.Tab_Credit_Card_Type.SelectedIndex = 0
    Me.Tab_Credit_Card_Type.Size = New System.Drawing.Size(523, 192)
    Me.Tab_Credit_Card_Type.TabIndex = 35
    '
    'frm_currency_configuration
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(707, 827)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_currency_configuration"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_currency_configuration"
    Me.panel_data.ResumeLayout(False)
    Me.tab_national_currency.ResumeLayout(False)
    Me.TabBankCard.ResumeLayout(False)
    Me.TabBankCard.PerformLayout()
    Me.tab_bank_card_vouchers.ResumeLayout(False)
    Me.TabRecharge.ResumeLayout(False)
    Me.TabRecharge.PerformLayout()
    Me.TabCashAdvance.ResumeLayout(False)
    Me.TabCashAdvance.PerformLayout()
    Me.TabCardComission.ResumeLayout(False)
    Me.TabCheck.ResumeLayout(False)
    Me.TabCheck.PerformLayout()
    Me.tab_check_vouchers.ResumeLayout(False)
    Me.TabCheckRecharge.ResumeLayout(False)
    Me.TabCheckRecharge.PerformLayout()
    Me.TabCheckCardAdvance.ResumeLayout(False)
    Me.TabCheckCardAdvance.PerformLayout()
    Me.TabCheckCommission.ResumeLayout(False)
    Me.tab_currency_exchange_vouchers.ResumeLayout(False)
    Me.TabExchangeRecharge.ResumeLayout(False)
    Me.TabExchangeRecharge.PerformLayout()
    Me.TabExchangeCommission.ResumeLayout(False)
    Me.gb_foreign_currency.ResumeLayout(False)
    Me.gb_foreign_currency.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents Timer1 As System.Windows.Forms.Timer
  Friend WithEvents ef_national_currency As GUI_Controls.uc_entry_field
  Friend WithEvents tab_national_currency As System.Windows.Forms.TabControl
  Friend WithEvents TabBankCard As System.Windows.Forms.TabPage
  Friend WithEvents TabCheck As System.Windows.Forms.TabPage
  Friend WithEvents chk_bank_card_voucher As System.Windows.Forms.CheckBox
  Friend WithEvents ef_bank_card_voucher_title As GUI_Controls.uc_entry_field
  Friend WithEvents chk_credit_card As System.Windows.Forms.CheckBox
  Friend WithEvents chk_check_voucher As System.Windows.Forms.CheckBox
  Friend WithEvents ef_check_voucher_title As GUI_Controls.uc_entry_field
  Friend WithEvents chk_check As System.Windows.Forms.CheckBox
  Friend WithEvents chk_bank_data_input As System.Windows.Forms.CheckBox
  Friend WithEvents chk_card_specification As System.Windows.Forms.CheckBox
  Friend WithEvents chk_bank_check_data As System.Windows.Forms.CheckBox
  Friend WithEvents chk_bank_card_cash_advance_hide_voucher As System.Windows.Forms.CheckBox
  Friend WithEvents ef_bank_card_cash_advance_voucher_title As GUI_Controls.uc_entry_field
  Friend WithEvents chk_check_cash_advance_hide_voucher As System.Windows.Forms.CheckBox
  Friend WithEvents ef_check_cash_advance_voucher_title As GUI_Controls.uc_entry_field
  Friend WithEvents tab_bank_card_vouchers As System.Windows.Forms.TabControl
  Friend WithEvents TabRecharge As System.Windows.Forms.TabPage
  Friend WithEvents TabCashAdvance As System.Windows.Forms.TabPage
  Friend WithEvents TabCardComission As System.Windows.Forms.TabPage
  Friend WithEvents uc_bank_card_commission As GUI_Controls.uc_entry_field
  Friend WithEvents tab_check_vouchers As System.Windows.Forms.TabControl
  Friend WithEvents TabCheckRecharge As System.Windows.Forms.TabPage
  Friend WithEvents TabCheckCardAdvance As System.Windows.Forms.TabPage
  Friend WithEvents TabCheckCommission As System.Windows.Forms.TabPage
  Friend WithEvents uc_check_commission As GUI_Controls.uc_entry_field
  Friend WithEvents gb_foreign_currency As System.Windows.Forms.GroupBox
  Friend WithEvents tab_currency_exchange_vouchers As System.Windows.Forms.TabControl
  Friend WithEvents TabExchangeRecharge As System.Windows.Forms.TabPage
  Friend WithEvents chk_foreign_currency_voucher As System.Windows.Forms.CheckBox
  Friend WithEvents ef_foreign_currency_voucher_title As GUI_Controls.uc_entry_field
  Friend WithEvents TabExchangeCommission As System.Windows.Forms.TabPage
  Friend WithEvents uc_exchange_commission As GUI_Controls.uc_entry_field
  Friend WithEvents CurrencyTab As System.Windows.Forms.TabControl
  Friend WithEvents uc_button_left As System.Windows.Forms.Button
  Friend WithEvents uc_button_right As System.Windows.Forms.Button
  Friend WithEvents lbl_order As System.Windows.Forms.Label
  Friend WithEvents Tab_Credit_Card_Type As System.Windows.Forms.TabControl
End Class
