'-------------------------------------------------------------------
' Copyright � 2009 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_points_import.vb
'
' DESCRIPTION : Allows to import points from excel.
'
' AUTHOR:        Jaume Barn�s
' CREATION DATE: 17-SEP-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -------------------------------------------------------------------------------
' 17-SEP-2013  JBC    Initial version
' 02-OCT-2013  ACM    Allow negative points.
' 11-OCT-2013  ACM    Functionality changed with new user control: uc_data_import
' 12-DEC-2016  XGJ    Bug 17664:No se realiza la importaci�n de puntos a la cuenta de un cliente si el formato de la hoja de c�lculo no es correcto
'----------------------------------------------------------------------------------------------------

Option Explicit On
Option Strict Off

#Region " Imports "

Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.IO
Imports System.Threading
Imports System.Runtime.InteropServices
Imports WSI.Common

#End Region

Public Class frm_points_import
  Inherits frm_base_edit

#Region "Constants"

#End Region

#Region "Structures"

#End Region

#Region "Members"
  Private m_permission As CLASS_GUI_USER.TYPE_PERMISSIONS
  Private m_thread_import As Thread 'Main Thread.
  Private m_accounts_points As AccountImportPoints
  Private m_imported_data_table As DataTable
  Private m_state As WSI.Common.AccountsImport.IMPORT_STATES 'Import State.
#End Region

#Region "Delegates"
  Private Delegate Sub SetTextCallback(ByVal e As WSI.Common.AccountPointsImportEventArgs)
  Private Delegate Function ShowErrorLog(ByVal ErrorsDataTable As DataTable, ByVal Message As String, ByVal ButtonOKText As String, ByVal ButtonCancelText As String) As Boolean
  Private Delegate Sub SetStopButtonDelegate(ByVal Enabled As Boolean)
#End Region

#Region "Overrides"
  'PURPOSE: Form controls initialization
  '
  'PARAMS
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    'Form title
    Me.Text = GLB_NLS_GUI_CLASS_II.GetString(436)

    'Buttons

    'OK
    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Visible = False

    'STOP
    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Text = GLB_NLS_GUI_CLASS_II.GetString(427)

    'EXIT
    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CLASS_II.GetString(3)
    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = False

    'Formating Screen
    Me.gb_output.Text = GLB_NLS_GUI_CLASS_II.GetString(425)

    Me.tb_output.Enabled = True
    Me.tb_output.ReadOnly = True

    chk_negative_points.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(838) 'Allow import negative points
   
    'Load Permisions
    m_permission = CurrentUser.Permissions(ENUM_FORM.FORM_POINTS_IMPORT)

    'Load fields to import
    dti_excel_import.AddColumnsMapping(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2749), "System.Int64") 'Account Id
    dti_excel_import.AddColumnsMapping(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1885), "System.Decimal") 'Level points
    dti_excel_import.AddColumnsMapping(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1886), "System.Decimal") 'Change Points

  End Sub 'GUI_InitControls
  'PURPOSE: Form controls initialization
  '
  'PARAMS
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_DELETE
        StopPointsImport()
      Case Else
        MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub
  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_POINTS_IMPORT

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

#End Region

#Region "Public Functions"
  ' PURPOSE: Form entry point from menu selection.
  '
  '  PARAMS:
  '     - INPUT:
  '         - mdiparent
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overloads Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
    Me.MdiParent = MdiParent

    Call Me.Display(False)

  End Sub ' ShowEditItem
#End Region

#Region "Private Functions"
  'PURPOSE: Start the importing points threads.
  '
  'PARAMS
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  Private Sub StartPointsImport(ByRef ImportedDataTable As DataTable)

    m_imported_data_table = ImportedDataTable

    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = True
    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Enabled = False
    Me.dti_excel_import.Enabled = False
    Me.gb_output.Text = String.Empty
    Me.tb_output.Text = String.Empty
    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Focus()

    'Create thread
    m_thread_import = New Thread(AddressOf PointsImportThread)
    m_thread_import.Start() ' Launch import thread


  End Sub
  'PURPOSE: Start the importing points threads.
  '
  'PARAMS
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  Private Sub PointsImportThread()

    Dim _ds_accounts_points As DataSet
    Dim _ask_for_continue_callback As New ShowErrorLog(AddressOf ShowBrowserMessage)
    Dim _stop_callback As New SetStopButtonDelegate(AddressOf SetStopButton)
    Dim _message As String
    Dim _txt_ok As String
    Dim _txt_cancel As String

    _ds_accounts_points = New DataSet()
    m_accounts_points = New AccountImportPoints()

    AddHandler m_accounts_points.OnRowUpdated, AddressOf UpdateProcess 'Every time that launch the event, launch UpdateProcess

    'Read Imported DataTable and fill Dataset
    If (m_accounts_points.GetPoints(m_imported_data_table, chk_negative_points.Checked, _ds_accounts_points)) Then

      'Formating text
      _txt_ok = GLB_NLS_GUI_CONTROLS.GetString(3)
      _txt_cancel = GLB_NLS_GUI_CONTROLS.GetString(4)
      _message = String.Empty

      'If there's no errors and no rows to process, show other text.
      If Not _ds_accounts_points.Tables("AccountsPoints") Is Nothing And _ds_accounts_points.Tables("AccountsPoints").Rows.Count = 0 Then
        _message = GLB_NLS_GUI_CLASS_II.GetString(433)
        _txt_ok = String.Empty
        _txt_cancel = GLB_NLS_GUI_CONTROLS.GetString(1)
      End If

      'Launch form
      If (Me.Invoke(_ask_for_continue_callback, _ds_accounts_points.Tables("Errors"), _message, _txt_ok, _txt_cancel) = True) Then
        m_accounts_points.AddImportedPoints(_ds_accounts_points.Tables("AccountsPoints"))
        Call RegisterAudit()
      Else
        m_accounts_points.setLastEvent()
      End If

      Me.Invoke(_stop_callback, False)
    Else
      m_accounts_points.setErrorEvent()
      m_accounts_points.setLastEvent()
      Me.Invoke(_stop_callback, False)
    End If
  End Sub
  ' PURPOSE: handle m_accounts_import.OnRowUpdated event
  '
  '  PARAMS:
  '     - INPUT:
  '         - mdiparent
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub UpdateProcess(ByVal Sender As Object, ByVal UpdateEventArgs As WSI.Common.AccountPointsImportEventArgs)
    AccountsImportStep(UpdateEventArgs)
  End Sub


  ' PURPOSE: called from m_thread_import throw a delegate, set BUTTON_DELETE enabled/disabled
  '
  '  PARAMS:
  '     - INPUT:
  '         - mdiparent
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub SetStopButton(ByVal Enabled As Boolean)
    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = Enabled
    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Enabled = True
    Me.dti_excel_import.Enabled = True
  End Sub
  ' PURPOSE: called from m_thread_import throw a delegate, set output text and progressbar progress
  '
  '  PARAMS:
  '     - INPUT:
  '         - mdiparent
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub AccountsImportStep(ByVal AccountImportEventArgs As WSI.Common.AccountPointsImportEventArgs)

    Dim _callback As SetTextCallback
    Dim _progress As Double

    'If control is not accessible, launch another thread with the event info.
    If (Me.tb_output.InvokeRequired) Then
      _callback = New SetTextCallback(AddressOf AccountsImportStep)
      Me.Invoke(_callback, New Object() {AccountImportEventArgs})
    Else
      'Fill the interface info
      _progress = 0
      tb_output.AppendText(AccountImportEventArgs.Message)
      tb_output.SelectionStart = tb_output.TextLength
      tb_output.ScrollToCaret()

      Try
        'Fill ProgessBar
        If AccountImportEventArgs.Row_count > 0 Then
          _progress = ((CDbl(AccountImportEventArgs.Row_count) - CDbl(AccountImportEventArgs.Pending_rows)) / CDbl(AccountImportEventArgs.Row_count) * 100)
        Else
          _progress = 0
        End If
        Me.pgb_import_progress.Value = CInt(_progress)
      Catch ex As Exception
        _progress = 0
        Me.pgb_import_progress.Value = CInt(_progress)
      End Try

      'Update process state
      Me.m_state = AccountImportEventArgs.State

      'If we finished reading the excel document, enable Stop button.
      If (m_state = AccountsImport.IMPORT_STATES.DocumentReaded) Then
        GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = True
      End If
      If (m_state = AccountsImport.IMPORT_STATES.DBUpdated) Then
        GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = False
      End If

    End If
  End Sub 'AccountsImportStep
  ' PURPOSE: called from UpdateProcess. Update the info of the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '         - mdiparent
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Function ShowBrowserMessage(ByVal ErrorsDataTable As DataTable, ByVal Message As String, ByVal ButtonOKText As String, ByVal ButtonCancelText As String) As Boolean
    Dim _message_Text As String
    Dim _frm_dialog As frm_YesNo_web_browser
    Dim _str_errors As String

    _frm_dialog = New frm_YesNo_web_browser()
    _str_errors = String.Empty

    If Message.Trim() = String.Empty Then
      _message_Text = GLB_NLS_GUI_CLASS_II.GetString(430) '"�Continuar procesando las cuentas correctas?"
    Else
      _message_Text = Message
    End If

    For Each _row As DataRow In ErrorsDataTable.Rows
      _str_errors += GLB_NLS_GUI_CLASS_II.GetString(432) + _row(0).ToString() + ": " + _row(1).ToString().Replace("<", """").Replace(">", """") + "</br>" 'Fila
    Next

    If _str_errors.Trim() = String.Empty Then
      _str_errors = GLB_NLS_GUI_CLASS_II.GetString(416) '"No se han encontrado errores."
      _message_Text = GLB_NLS_GUI_CLASS_II.GetString(434)
      _frm_dialog.Init(_message_Text, GLB_NLS_GUI_CONTROLS.GetString(1), String.Empty)
    Else
      _frm_dialog.Init(_message_Text, ButtonOKText, ButtonCancelText) '"�Continuar procesando las cuentas correctas?"
    End If

    _frm_dialog.wb_browser.DocumentText = _str_errors
    Call _frm_dialog.ShowDialog()

    If (_frm_dialog.DialogResult = Windows.Forms.DialogResult.Yes) Then
      Return True
    Else
      Return False
    End If

  End Function
  ' PURPOSE: stop Import Thread
  '
  '  PARAMS:
  '     - INPUT:
  '         - mdiparent
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub StopPointsImport()

    If Not m_thread_import Is Nothing Then
      If (m_thread_import.ThreadState <> ThreadState.Stopped) Then
        m_thread_import.Abort()
        pgb_import_progress.Value = 0
        tb_output.Text += vbCrLf + GLB_NLS_GUI_CLASS_II.GetString(431)
        tb_output.SelectionStart = tb_output.TextLength
        tb_output.ScrollToCaret()
        GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = False
        GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Enabled = True
        Me.dti_excel_import.Enabled = True
      End If
    End If

  End Sub 'StopPointsImport

  ' PURPOSE: Audit import action
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub RegisterAudit()
    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _file_name As String

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_ACCOUNT)

    _auditor_data.SetName(GLB_NLS_GUI_CLASS_II.Id(415), GLB_NLS_GUI_CLASS_II.GetString(436))

    _file_name = System.IO.Path.GetFileName(dti_excel_import.Value)
    _auditor_data.SetField(GLB_NLS_GUI_CLASS_II.Id(550), _file_name)

    _auditor_data.Notify(GLB_CurrentUser.GuiId, _
                 GLB_CurrentUser.Id, _
                 GLB_CurrentUser.Name, _
                 CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, _
                 0)

  End Sub ' RegisterAudit

#End Region

#Region "Events"
  Private Sub dti_excel_import_FileImported(ByRef ImportedDataTable As System.Data.DataTable) Handles dti_excel_import.FileImported

    If ImportedDataTable.Rows.Count > 0 Then
      Call StartPointsImport(ImportedDataTable)
    Else
      ' XGJ 12-DEC-2016
      Me.tb_output.Text = GLB_NLS_GUI_CLASS_II.GetString(433) & vbNewLine & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7619)
    End If
  End Sub
#End Region


End Class