<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_statistics_providers
  Inherits GUI_Controls.frm_base

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.dg_list = New GUI_Controls.uc_grid()
    Me.uc_pr_list = New GUI_Controls.uc_provider()
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector()
    Me.lbl_d = New System.Windows.Forms.Label()
    Me.lbl_c = New System.Windows.Forms.Label()
    Me.lbl_a = New System.Windows.Forms.Label()
    Me.lbl_b = New System.Windows.Forms.Label()
    Me.panel_filter_buttons = New System.Windows.Forms.Panel()
    Me.btn_filter_reset = New GUI_Controls.uc_button()
    Me.btn_filter_apply = New GUI_Controls.uc_button()
    Me.Panel1 = New System.Windows.Forms.Panel()
    Me.btn_excel = New GUI_Controls.uc_button()
    Me.btn_print = New GUI_Controls.uc_button()
    Me.btn_select = New GUI_Controls.uc_button()
    Me.btn_exit = New GUI_Controls.uc_button()
    Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
    Me.tb_control = New System.Windows.Forms.TabControl()
    Me.TabPage1 = New System.Windows.Forms.TabPage()
    Me.SplitContainer2 = New System.Windows.Forms.SplitContainer()
    Me.chk_only_unbalance = New System.Windows.Forms.CheckBox()
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox()
    Me.opt_by_provider = New System.Windows.Forms.RadioButton()
    Me.opt_by_terminal = New System.Windows.Forms.RadioButton()
    Me.gb_labels = New System.Windows.Forms.GroupBox()
    Me.lbl_e = New System.Windows.Forms.Label()
    Me.TabPage2 = New System.Windows.Forms.TabPage()
    Me.SplitContainer3 = New System.Windows.Forms.SplitContainer()
    Me.lbl_note = New System.Windows.Forms.Label()
    Me.uc_dsl_cashier = New GUI_Controls.uc_daily_session_selector()
    Me.dg_list_cashier = New GUI_Controls.uc_grid()
    Me.SplitContainer4 = New System.Windows.Forms.SplitContainer()
    Me.panel_filter_buttons.SuspendLayout()
    Me.Panel1.SuspendLayout()
    Me.SplitContainer1.Panel1.SuspendLayout()
    Me.SplitContainer1.Panel2.SuspendLayout()
    Me.SplitContainer1.SuspendLayout()
    Me.tb_control.SuspendLayout()
    Me.TabPage1.SuspendLayout()
    Me.SplitContainer2.Panel1.SuspendLayout()
    Me.SplitContainer2.Panel2.SuspendLayout()
    Me.SplitContainer2.SuspendLayout()
    Me.gb_labels.SuspendLayout()
    Me.TabPage2.SuspendLayout()
    Me.SplitContainer3.Panel1.SuspendLayout()
    Me.SplitContainer3.Panel2.SuspendLayout()
    Me.SplitContainer3.SuspendLayout()
    Me.SplitContainer4.Panel1.SuspendLayout()
    Me.SplitContainer4.Panel2.SuspendLayout()
    Me.SplitContainer4.SuspendLayout()
    Me.SuspendLayout()
    '
    'dg_list
    '
    Me.dg_list.CurrentCol = -1
    Me.dg_list.CurrentRow = -1
    Me.dg_list.Dock = System.Windows.Forms.DockStyle.Fill
    Me.dg_list.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_list.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_list.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_list.Location = New System.Drawing.Point(0, 0)
    Me.dg_list.Name = "dg_list"
    Me.dg_list.PanelRightVisible = False
    Me.dg_list.Redraw = True
    Me.dg_list.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_list.Size = New System.Drawing.Size(1029, 420)
    Me.dg_list.Sortable = False
    Me.dg_list.SortAscending = True
    Me.dg_list.SortByCol = 0
    Me.dg_list.TabIndex = 0
    Me.dg_list.ToolTipped = True
    Me.dg_list.TopRow = -2
    Me.dg_list.WordWrap = False
    '
    'uc_pr_list
    '
    Me.uc_pr_list.Location = New System.Drawing.Point(267, 0)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(337, 193)
    Me.uc_pr_list.TabIndex = 5
    Me.uc_pr_list.TerminalListHasValues = False
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.ClosingTimeEnabled = False
    Me.uc_dsl.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.Location = New System.Drawing.Point(8, 6)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.ShowBorder = True
    Me.uc_dsl.Size = New System.Drawing.Size(251, 78)
    Me.uc_dsl.TabIndex = 0
    Me.uc_dsl.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    '
    'lbl_d
    '
    Me.lbl_d.AutoSize = True
    Me.lbl_d.Location = New System.Drawing.Point(6, 101)
    Me.lbl_d.Name = "lbl_d"
    Me.lbl_d.Size = New System.Drawing.Size(164, 13)
    Me.lbl_d.TabIndex = 3
    Me.lbl_d.Text = "D - Handpay not transfered"
    '
    'lbl_c
    '
    Me.lbl_c.AutoSize = True
    Me.lbl_c.Location = New System.Drawing.Point(6, 73)
    Me.lbl_c.Name = "lbl_c"
    Me.lbl_c.Size = New System.Drawing.Size(135, 13)
    Me.lbl_c.TabIndex = 2
    Me.lbl_c.Text = "C - Open Playsessions"
    '
    'lbl_a
    '
    Me.lbl_a.AutoSize = True
    Me.lbl_a.Location = New System.Drawing.Point(6, 17)
    Me.lbl_a.Name = "lbl_a"
    Me.lbl_a.Size = New System.Drawing.Size(123, 13)
    Me.lbl_a.TabIndex = 0
    Me.lbl_a.Text = "A - NetWin Statistics"
    '
    'lbl_b
    '
    Me.lbl_b.AutoSize = True
    Me.lbl_b.Location = New System.Drawing.Point(6, 45)
    Me.lbl_b.Name = "lbl_b"
    Me.lbl_b.Size = New System.Drawing.Size(126, 13)
    Me.lbl_b.TabIndex = 1
    Me.lbl_b.Text = "B - NetWin Providers"
    '
    'panel_filter_buttons
    '
    Me.panel_filter_buttons.Controls.Add(Me.btn_filter_reset)
    Me.panel_filter_buttons.Controls.Add(Me.btn_filter_apply)
    Me.panel_filter_buttons.Dock = System.Windows.Forms.DockStyle.Fill
    Me.panel_filter_buttons.Location = New System.Drawing.Point(0, 0)
    Me.panel_filter_buttons.Name = "panel_filter_buttons"
    Me.panel_filter_buttons.Size = New System.Drawing.Size(92, 219)
    Me.panel_filter_buttons.TabIndex = 125
    '
    'btn_filter_reset
    '
    Me.btn_filter_reset.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_filter_reset.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.btn_filter_reset.Location = New System.Drawing.Point(0, 159)
    Me.btn_filter_reset.Name = "btn_filter_reset"
    Me.btn_filter_reset.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_filter_reset.Size = New System.Drawing.Size(90, 30)
    Me.btn_filter_reset.TabIndex = 0
    Me.btn_filter_reset.ToolTipped = False
    Me.btn_filter_reset.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_filter_apply
    '
    Me.btn_filter_apply.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_filter_apply.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.btn_filter_apply.Location = New System.Drawing.Point(0, 189)
    Me.btn_filter_apply.Name = "btn_filter_apply"
    Me.btn_filter_apply.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_filter_apply.Size = New System.Drawing.Size(90, 30)
    Me.btn_filter_apply.TabIndex = 1
    Me.btn_filter_apply.ToolTipped = False
    Me.btn_filter_apply.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'Panel1
    '
    Me.Panel1.Controls.Add(Me.btn_excel)
    Me.Panel1.Controls.Add(Me.btn_print)
    Me.Panel1.Controls.Add(Me.btn_select)
    Me.Panel1.Controls.Add(Me.btn_exit)
    Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
    Me.Panel1.Location = New System.Drawing.Point(0, 0)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(92, 429)
    Me.Panel1.TabIndex = 126
    '
    'btn_excel
    '
    Me.btn_excel.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_excel.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.btn_excel.Location = New System.Drawing.Point(0, 309)
    Me.btn_excel.Name = "btn_excel"
    Me.btn_excel.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_excel.Size = New System.Drawing.Size(90, 30)
    Me.btn_excel.TabIndex = 4
    Me.btn_excel.ToolTipped = False
    Me.btn_excel.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_print
    '
    Me.btn_print.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_print.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.btn_print.Location = New System.Drawing.Point(0, 339)
    Me.btn_print.Name = "btn_print"
    Me.btn_print.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_print.Size = New System.Drawing.Size(90, 30)
    Me.btn_print.TabIndex = 3
    Me.btn_print.ToolTipped = False
    Me.btn_print.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_select
    '
    Me.btn_select.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_select.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.btn_select.Location = New System.Drawing.Point(0, 369)
    Me.btn_select.Name = "btn_select"
    Me.btn_select.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_select.Size = New System.Drawing.Size(90, 30)
    Me.btn_select.TabIndex = 2
    Me.btn_select.ToolTipped = False
    Me.btn_select.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_exit
    '
    Me.btn_exit.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btn_exit.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.btn_exit.Location = New System.Drawing.Point(0, 399)
    Me.btn_exit.Name = "btn_exit"
    Me.btn_exit.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_exit.Size = New System.Drawing.Size(90, 30)
    Me.btn_exit.TabIndex = 0
    Me.btn_exit.ToolTipped = False
    Me.btn_exit.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'SplitContainer1
    '
    Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
    Me.SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2
    Me.SplitContainer1.IsSplitterFixed = True
    Me.SplitContainer1.Location = New System.Drawing.Point(4, 4)
    Me.SplitContainer1.Name = "SplitContainer1"
    '
    'SplitContainer1.Panel1
    '
    Me.SplitContainer1.Panel1.Controls.Add(Me.tb_control)
    '
    'SplitContainer1.Panel2
    '
    Me.SplitContainer1.Panel2.Controls.Add(Me.SplitContainer4)
    Me.SplitContainer1.Size = New System.Drawing.Size(1139, 652)
    Me.SplitContainer1.SplitterDistance = 1043
    Me.SplitContainer1.TabIndex = 0
    '
    'tb_control
    '
    Me.tb_control.Controls.Add(Me.TabPage1)
    Me.tb_control.Controls.Add(Me.TabPage2)
    Me.tb_control.Dock = System.Windows.Forms.DockStyle.Fill
    Me.tb_control.Location = New System.Drawing.Point(0, 0)
    Me.tb_control.Name = "tb_control"
    Me.tb_control.SelectedIndex = 0
    Me.tb_control.Size = New System.Drawing.Size(1043, 652)
    Me.tb_control.TabIndex = 0
    '
    'TabPage1
    '
    Me.TabPage1.Controls.Add(Me.SplitContainer2)
    Me.TabPage1.Location = New System.Drawing.Point(4, 22)
    Me.TabPage1.Name = "TabPage1"
    Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage1.Size = New System.Drawing.Size(1035, 626)
    Me.TabPage1.TabIndex = 0
    Me.TabPage1.Text = "TabPage1"
    Me.TabPage1.UseVisualStyleBackColor = True
    '
    'SplitContainer2
    '
    Me.SplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill
    Me.SplitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
    Me.SplitContainer2.IsSplitterFixed = True
    Me.SplitContainer2.Location = New System.Drawing.Point(3, 3)
    Me.SplitContainer2.Name = "SplitContainer2"
    Me.SplitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal
    '
    'SplitContainer2.Panel1
    '
    Me.SplitContainer2.Panel1.Controls.Add(Me.chk_only_unbalance)
    Me.SplitContainer2.Panel1.Controls.Add(Me.uc_dsl)
    Me.SplitContainer2.Panel1.Controls.Add(Me.uc_pr_list)
    Me.SplitContainer2.Panel1.Controls.Add(Me.chk_terminal_location)
    Me.SplitContainer2.Panel1.Controls.Add(Me.opt_by_provider)
    Me.SplitContainer2.Panel1.Controls.Add(Me.opt_by_terminal)
    Me.SplitContainer2.Panel1.Controls.Add(Me.gb_labels)
    '
    'SplitContainer2.Panel2
    '
    Me.SplitContainer2.Panel2.Controls.Add(Me.dg_list)
    Me.SplitContainer2.Size = New System.Drawing.Size(1029, 620)
    Me.SplitContainer2.SplitterDistance = 196
    Me.SplitContainer2.TabIndex = 0
    '
    'chk_only_unbalance
    '
    Me.chk_only_unbalance.Location = New System.Drawing.Point(3, 174)
    Me.chk_only_unbalance.Name = "chk_only_unbalance"
    Me.chk_only_unbalance.Size = New System.Drawing.Size(235, 16)
    Me.chk_only_unbalance.TabIndex = 4
    Me.chk_only_unbalance.Text = "xOnlyUnbalance"
    '
    'chk_terminal_location
    '
    Me.chk_terminal_location.Location = New System.Drawing.Point(33, 120)
    Me.chk_terminal_location.Name = "chk_terminal_location"
    Me.chk_terminal_location.Size = New System.Drawing.Size(235, 16)
    Me.chk_terminal_location.TabIndex = 2
    Me.chk_terminal_location.Text = "xShow terminals location"
    '
    'opt_by_provider
    '
    Me.opt_by_provider.Location = New System.Drawing.Point(3, 144)
    Me.opt_by_provider.Name = "opt_by_provider"
    Me.opt_by_provider.Size = New System.Drawing.Size(256, 24)
    Me.opt_by_provider.TabIndex = 3
    Me.opt_by_provider.Text = "xDetailProviders"
    '
    'opt_by_terminal
    '
    Me.opt_by_terminal.Location = New System.Drawing.Point(3, 90)
    Me.opt_by_terminal.Name = "opt_by_terminal"
    Me.opt_by_terminal.Size = New System.Drawing.Size(256, 24)
    Me.opt_by_terminal.TabIndex = 1
    Me.opt_by_terminal.Text = "xDetailTerminals"
    '
    'gb_labels
    '
    Me.gb_labels.BackColor = System.Drawing.Color.Transparent
    Me.gb_labels.Controls.Add(Me.lbl_e)
    Me.gb_labels.Controls.Add(Me.lbl_a)
    Me.gb_labels.Controls.Add(Me.lbl_c)
    Me.gb_labels.Controls.Add(Me.lbl_b)
    Me.gb_labels.Controls.Add(Me.lbl_d)
    Me.gb_labels.Location = New System.Drawing.Point(599, 3)
    Me.gb_labels.Name = "gb_labels"
    Me.gb_labels.Size = New System.Drawing.Size(274, 150)
    Me.gb_labels.TabIndex = 6
    Me.gb_labels.TabStop = False
    Me.gb_labels.Text = "GroupBox1"
    '
    'lbl_e
    '
    Me.lbl_e.AutoSize = True
    Me.lbl_e.Location = New System.Drawing.Point(7, 126)
    Me.lbl_e.Name = "lbl_e"
    Me.lbl_e.Size = New System.Drawing.Size(123, 13)
    Me.lbl_e.TabIndex = 4
    Me.lbl_e.Text = "E - Handpay manual"
    '
    'TabPage2
    '
    Me.TabPage2.Controls.Add(Me.SplitContainer3)
    Me.TabPage2.Location = New System.Drawing.Point(4, 22)
    Me.TabPage2.Name = "TabPage2"
    Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage2.Size = New System.Drawing.Size(1035, 626)
    Me.TabPage2.TabIndex = 1
    Me.TabPage2.Text = "TabPage2"
    Me.TabPage2.UseVisualStyleBackColor = True
    '
    'SplitContainer3
    '
    Me.SplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill
    Me.SplitContainer3.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
    Me.SplitContainer3.IsSplitterFixed = True
    Me.SplitContainer3.Location = New System.Drawing.Point(3, 3)
    Me.SplitContainer3.Name = "SplitContainer3"
    Me.SplitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal
    '
    'SplitContainer3.Panel1
    '
    Me.SplitContainer3.Panel1.Controls.Add(Me.lbl_note)
    Me.SplitContainer3.Panel1.Controls.Add(Me.uc_dsl_cashier)
    '
    'SplitContainer3.Panel2
    '
    Me.SplitContainer3.Panel2.Controls.Add(Me.dg_list_cashier)
    Me.SplitContainer3.Size = New System.Drawing.Size(1029, 620)
    Me.SplitContainer3.SplitterDistance = 90
    Me.SplitContainer3.TabIndex = 3
    '
    'lbl_note
    '
    Me.lbl_note.AutoSize = True
    Me.lbl_note.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.lbl_note.Location = New System.Drawing.Point(266, 70)
    Me.lbl_note.Name = "lbl_note"
    Me.lbl_note.Size = New System.Drawing.Size(105, 13)
    Me.lbl_note.TabIndex = 17
    Me.lbl_note.Text = "x*: Remember..."
    '
    'uc_dsl_cashier
    '
    Me.uc_dsl_cashier.ClosingTime = 0
    Me.uc_dsl_cashier.ClosingTimeEnabled = False
    Me.uc_dsl_cashier.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl_cashier.FromDateSelected = True
    Me.uc_dsl_cashier.Location = New System.Drawing.Point(3, 3)
    Me.uc_dsl_cashier.Name = "uc_dsl_cashier"
    Me.uc_dsl_cashier.ShowBorder = False
    Me.uc_dsl_cashier.Size = New System.Drawing.Size(257, 83)
    Me.uc_dsl_cashier.TabIndex = 2
    Me.uc_dsl_cashier.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl_cashier.ToDateSelected = True
    '
    'dg_list_cashier
    '
    Me.dg_list_cashier.CurrentCol = -1
    Me.dg_list_cashier.CurrentRow = -1
    Me.dg_list_cashier.Dock = System.Windows.Forms.DockStyle.Fill
    Me.dg_list_cashier.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_list_cashier.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_list_cashier.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_list_cashier.Location = New System.Drawing.Point(0, 0)
    Me.dg_list_cashier.Name = "dg_list_cashier"
    Me.dg_list_cashier.PanelRightVisible = False
    Me.dg_list_cashier.Redraw = True
    Me.dg_list_cashier.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_list_cashier.Size = New System.Drawing.Size(1029, 526)
    Me.dg_list_cashier.Sortable = False
    Me.dg_list_cashier.SortAscending = True
    Me.dg_list_cashier.SortByCol = 0
    Me.dg_list_cashier.TabIndex = 2
    Me.dg_list_cashier.ToolTipped = True
    Me.dg_list_cashier.TopRow = -2
    Me.dg_list_cashier.WordWrap = False
    '
    'SplitContainer4
    '
    Me.SplitContainer4.Dock = System.Windows.Forms.DockStyle.Fill
    Me.SplitContainer4.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
    Me.SplitContainer4.IsSplitterFixed = True
    Me.SplitContainer4.Location = New System.Drawing.Point(0, 0)
    Me.SplitContainer4.Name = "SplitContainer4"
    Me.SplitContainer4.Orientation = System.Windows.Forms.Orientation.Horizontal
    '
    'SplitContainer4.Panel1
    '
    Me.SplitContainer4.Panel1.Controls.Add(Me.panel_filter_buttons)
    '
    'SplitContainer4.Panel2
    '
    Me.SplitContainer4.Panel2.Controls.Add(Me.Panel1)
    Me.SplitContainer4.Size = New System.Drawing.Size(92, 652)
    Me.SplitContainer4.SplitterDistance = 219
    Me.SplitContainer4.TabIndex = 127
    '
    'frm_statistics_providers
    '
    Me.CancelButton = Me.btn_exit
    Me.ClientSize = New System.Drawing.Size(1147, 660)
    Me.Controls.Add(Me.SplitContainer1)
    Me.KeyPreview = True
    Me.Name = "frm_statistics_providers"
    Me.Text = "xfrm_statistics_providers"
    Me.panel_filter_buttons.ResumeLayout(False)
    Me.Panel1.ResumeLayout(False)
    Me.SplitContainer1.Panel1.ResumeLayout(False)
    Me.SplitContainer1.Panel2.ResumeLayout(False)
    Me.SplitContainer1.ResumeLayout(False)
    Me.tb_control.ResumeLayout(False)
    Me.TabPage1.ResumeLayout(False)
    Me.SplitContainer2.Panel1.ResumeLayout(False)
    Me.SplitContainer2.Panel2.ResumeLayout(False)
    Me.SplitContainer2.ResumeLayout(False)
    Me.gb_labels.ResumeLayout(False)
    Me.gb_labels.PerformLayout()
    Me.TabPage2.ResumeLayout(False)
    Me.SplitContainer3.Panel1.ResumeLayout(False)
    Me.SplitContainer3.Panel1.PerformLayout()
    Me.SplitContainer3.Panel2.ResumeLayout(False)
    Me.SplitContainer3.ResumeLayout(False)
    Me.SplitContainer4.Panel1.ResumeLayout(False)
    Me.SplitContainer4.Panel2.ResumeLayout(False)
    Me.SplitContainer4.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

  Private Sub Grid_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dg_list.Load

  End Sub
  Friend WithEvents dg_list As GUI_Controls.uc_grid
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector
  Friend WithEvents lbl_d As System.Windows.Forms.Label
  Friend WithEvents lbl_c As System.Windows.Forms.Label
  Friend WithEvents lbl_a As System.Windows.Forms.Label
  Friend WithEvents lbl_b As System.Windows.Forms.Label
  Private WithEvents panel_filter_buttons As System.Windows.Forms.Panel
  Private WithEvents btn_filter_reset As GUI_Controls.uc_button
  Private WithEvents btn_filter_apply As GUI_Controls.uc_button
  Private WithEvents Panel1 As System.Windows.Forms.Panel
  Private WithEvents btn_exit As GUI_Controls.uc_button
  Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
  Friend WithEvents tb_control As System.Windows.Forms.TabControl
  Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
  Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
  Friend WithEvents dg_list_cashier As GUI_Controls.uc_grid
  Friend WithEvents gb_labels As System.Windows.Forms.GroupBox
  Friend WithEvents SplitContainer2 As System.Windows.Forms.SplitContainer
  Friend WithEvents SplitContainer3 As System.Windows.Forms.SplitContainer
  Friend WithEvents SplitContainer4 As System.Windows.Forms.SplitContainer
  Friend WithEvents uc_dsl_cashier As GUI_Controls.uc_daily_session_selector
  Friend WithEvents opt_by_provider As System.Windows.Forms.RadioButton
  Friend WithEvents opt_by_terminal As System.Windows.Forms.RadioButton
  Friend WithEvents lbl_e As System.Windows.Forms.Label
  Private WithEvents btn_excel As GUI_Controls.uc_button
  Private WithEvents btn_print As GUI_Controls.uc_button
  Private WithEvents btn_select As GUI_Controls.uc_button
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_note As System.Windows.Forms.Label
  Friend WithEvents chk_only_unbalance As System.Windows.Forms.CheckBox
End Class
