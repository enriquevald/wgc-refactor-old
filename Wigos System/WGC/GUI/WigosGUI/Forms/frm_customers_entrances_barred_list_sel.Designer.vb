﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_customers_entrances_barred_list_sel
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_customers_entrances_barred_list_sel))
    Me.grp_box_list_type = New System.Windows.Forms.GroupBox()
    Me.chk_list_type_imported = New System.Windows.Forms.CheckBox()
    Me.chk_list_type_internal = New System.Windows.Forms.CheckBox()
    Me.chk_entrance_denied = New System.Windows.Forms.CheckBox()
    Me.cmb_blacklist_type = New GUI_Controls.uc_combo()
    Me.uc_account_sel1 = New GUI_Controls.uc_account_sel()
    Me.chklst_Block_type = New GUI_Controls.uc_checked_list()
    Me.edtDocumentNumber = New System.Windows.Forms.TextBox()
    Me.lblDocNumber = New System.Windows.Forms.Label()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.grp_box_list_type.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.lblDocNumber)
    Me.panel_filter.Controls.Add(Me.edtDocumentNumber)
    Me.panel_filter.Controls.Add(Me.chklst_Block_type)
    Me.panel_filter.Controls.Add(Me.uc_account_sel1)
    Me.panel_filter.Controls.Add(Me.grp_box_list_type)
    Me.panel_filter.Size = New System.Drawing.Size(1135, 195)
    Me.panel_filter.TabIndex = 1
    Me.panel_filter.Controls.SetChildIndex(Me.grp_box_list_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account_sel1, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chklst_Block_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.edtDocumentNumber, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lblDocNumber, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 199)
    Me.panel_data.Size = New System.Drawing.Size(1135, 509)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1129, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1129, 4)
    '
    'grp_box_list_type
    '
    Me.grp_box_list_type.Controls.Add(Me.chk_list_type_imported)
    Me.grp_box_list_type.Controls.Add(Me.chk_list_type_internal)
    Me.grp_box_list_type.Controls.Add(Me.chk_entrance_denied)
    Me.grp_box_list_type.Controls.Add(Me.cmb_blacklist_type)
    Me.grp_box_list_type.Location = New System.Drawing.Point(6, 6)
    Me.grp_box_list_type.Name = "grp_box_list_type"
    Me.grp_box_list_type.Size = New System.Drawing.Size(373, 131)
    Me.grp_box_list_type.TabIndex = 11
    Me.grp_box_list_type.TabStop = False
    Me.grp_box_list_type.Text = "xListType"
    '
    'chk_list_type_imported
    '
    Me.chk_list_type_imported.AutoSize = True
    Me.chk_list_type_imported.Location = New System.Drawing.Point(15, 57)
    Me.chk_list_type_imported.Name = "chk_list_type_imported"
    Me.chk_list_type_imported.Size = New System.Drawing.Size(94, 17)
    Me.chk_list_type_imported.TabIndex = 12
    Me.chk_list_type_imported.Text = "xIMPORTED"
    Me.chk_list_type_imported.UseVisualStyleBackColor = True
    '
    'chk_list_type_internal
    '
    Me.chk_list_type_internal.AutoSize = True
    Me.chk_list_type_internal.Location = New System.Drawing.Point(15, 27)
    Me.chk_list_type_internal.Name = "chk_list_type_internal"
    Me.chk_list_type_internal.Size = New System.Drawing.Size(90, 17)
    Me.chk_list_type_internal.TabIndex = 11
    Me.chk_list_type_internal.Text = "xINTERNAL"
    Me.chk_list_type_internal.UseVisualStyleBackColor = True
    '
    'chk_entrance_denied
    '
    Me.chk_entrance_denied.AutoSize = True
    Me.chk_entrance_denied.Location = New System.Drawing.Point(141, 69)
    Me.chk_entrance_denied.Name = "chk_entrance_denied"
    Me.chk_entrance_denied.Size = New System.Drawing.Size(127, 17)
    Me.chk_entrance_denied.TabIndex = 15
    Me.chk_entrance_denied.Text = "xEntrance Denied"
    Me.chk_entrance_denied.UseVisualStyleBackColor = True
    '
    'cmb_blacklist_type
    '
    Me.cmb_blacklist_type.AllowUnlistedValues = False
    Me.cmb_blacklist_type.AutoCompleteMode = False
    Me.cmb_blacklist_type.IsReadOnly = False
    Me.cmb_blacklist_type.Location = New System.Drawing.Point(126, 95)
    Me.cmb_blacklist_type.Name = "cmb_blacklist_type"
    Me.cmb_blacklist_type.SelectedIndex = -1
    Me.cmb_blacklist_type.Size = New System.Drawing.Size(221, 24)
    Me.cmb_blacklist_type.SufixText = "Sufix Text"
    Me.cmb_blacklist_type.SufixTextVisible = True
    Me.cmb_blacklist_type.TabIndex = 10
    Me.cmb_blacklist_type.TextCombo = Nothing
    Me.cmb_blacklist_type.TextWidth = 40
    '
    'uc_account_sel1
    '
    Me.uc_account_sel1.Account = ""
    Me.uc_account_sel1.AccountText = ""
    Me.uc_account_sel1.Anon = False
    Me.uc_account_sel1.AutoSize = True
    Me.uc_account_sel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_account_sel1.BirthDate = New Date(CType(0, Long))
    Me.uc_account_sel1.DisabledHolder = False
    Me.uc_account_sel1.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.uc_account_sel1.Holder = ""
    Me.uc_account_sel1.Location = New System.Drawing.Point(733, 3)
    Me.uc_account_sel1.MassiveSearchNumbers = ""
    Me.uc_account_sel1.MassiveSearchNumbersToEdit = ""
    Me.uc_account_sel1.MassiveSearchType = 0
    Me.uc_account_sel1.Name = "uc_account_sel1"
    Me.uc_account_sel1.SearchTrackDataAsInternal = True
    Me.uc_account_sel1.ShowMassiveSearch = False
    Me.uc_account_sel1.ShowVipClients = False
    Me.uc_account_sel1.Size = New System.Drawing.Size(307, 108)
    Me.uc_account_sel1.TabIndex = 13
    Me.uc_account_sel1.Telephone = ""
    Me.uc_account_sel1.TrackData = ""
    Me.uc_account_sel1.Vip = False
    Me.uc_account_sel1.WeddingDate = New Date(CType(0, Long))
    '
    'chklst_Block_type
    '
    Me.chklst_Block_type.GroupBoxText = "xBlockType"
    Me.chklst_Block_type.Location = New System.Drawing.Point(400, 6)
    Me.chklst_Block_type.m_resize_width = 315
    Me.chklst_Block_type.multiChoice = True
    Me.chklst_Block_type.Name = "chklst_Block_type"
    Me.chklst_Block_type.SelectedIndexes = New Integer(-1) {}
    Me.chklst_Block_type.SelectedIndexesList = ""
    Me.chklst_Block_type.SelectedIndexesListLevel2 = ""
    Me.chklst_Block_type.SelectedValuesArray = New String(-1) {}
    Me.chklst_Block_type.SelectedValuesList = ""
    Me.chklst_Block_type.SelectedValuesListLevel2 = ""
    Me.chklst_Block_type.SetLevels = 2
    Me.chklst_Block_type.Size = New System.Drawing.Size(315, 179)
    Me.chklst_Block_type.TabIndex = 14
    Me.chklst_Block_type.ValuesArray = New String(-1) {}
    '
    'edtDocumentNumber
    '
    Me.edtDocumentNumber.Location = New System.Drawing.Point(157, 149)
    Me.edtDocumentNumber.Name = "edtDocumentNumber"
    Me.edtDocumentNumber.Size = New System.Drawing.Size(196, 21)
    Me.edtDocumentNumber.TabIndex = 16
    '
    'lblDocNumber
    '
    Me.lblDocNumber.AutoSize = True
    Me.lblDocNumber.Location = New System.Drawing.Point(20, 152)
    Me.lblDocNumber.Name = "lblDocNumber"
    Me.lblDocNumber.Size = New System.Drawing.Size(128, 13)
    Me.lblDocNumber.TabIndex = 17
    Me.lblDocNumber.Text = "xDOCUMENTNUMBER"
    '
    'frm_customers_entrances_barred_list_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1143, 712)
    Me.Name = "frm_customers_entrances_barred_list_sel"
    Me.Text = "frm_customers_entrances_barred_list_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.grp_box_list_type.ResumeLayout(False)
    Me.grp_box_list_type.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents grp_box_list_type As System.Windows.Forms.GroupBox
  Friend WithEvents chk_list_type_imported As System.Windows.Forms.CheckBox
  Friend WithEvents chk_list_type_internal As System.Windows.Forms.CheckBox
  Friend WithEvents cmb_blacklist_type As GUI_Controls.uc_combo
  Friend WithEvents chklst_Block_type As GUI_Controls.uc_checked_list
  Friend WithEvents uc_account_sel1 As GUI_Controls.uc_account_sel
  Friend WithEvents lblDocNumber As System.Windows.Forms.Label
  Friend WithEvents edtDocumentNumber As System.Windows.Forms.TextBox
  Friend WithEvents chk_entrance_denied As System.Windows.Forms.CheckBox
End Class
