﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_about_us_edit
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.lbl_content = New System.Windows.Forms.Label()
    Me.ef_title = New GUI_Controls.uc_entry_field()
    Me.tab_control_HTML = New System.Windows.Forms.TabControl()
    Me.tab_html = New System.Windows.Forms.TabPage()
    Me.chk_add_style = New System.Windows.Forms.CheckBox()
    Me.tb_content = New System.Windows.Forms.RichTextBox()
    Me.btn_insert_tag = New System.Windows.Forms.Button()
    Me.cmbTag = New System.Windows.Forms.ComboBox()
    Me.tab_preview = New System.Windows.Forms.TabPage()
    Me.web_browser_html = New System.Windows.Forms.WebBrowser()
    Me.panel_data.SuspendLayout()
    Me.tab_control_HTML.SuspendLayout()
    Me.tab_html.SuspendLayout()
    Me.tab_preview.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.tab_control_HTML)
    Me.panel_data.Controls.Add(Me.ef_title)
    Me.panel_data.Controls.Add(Me.lbl_content)
    Me.panel_data.Size = New System.Drawing.Size(525, 515)
    '
    'lbl_content
    '
    Me.lbl_content.Location = New System.Drawing.Point(0, 75)
    Me.lbl_content.Margin = New System.Windows.Forms.Padding(0)
    Me.lbl_content.Name = "lbl_content"
    Me.lbl_content.Size = New System.Drawing.Size(80, 57)
    Me.lbl_content.TabIndex = 14
    Me.lbl_content.Text = "xContent"
    Me.lbl_content.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'ef_title
    '
    Me.ef_title.DoubleValue = 0.0R
    Me.ef_title.IntegerValue = 0
    Me.ef_title.IsReadOnly = False
    Me.ef_title.Location = New System.Drawing.Point(4, 20)
    Me.ef_title.Name = "ef_title"
    Me.ef_title.PlaceHolder = Nothing
    Me.ef_title.Size = New System.Drawing.Size(441, 24)
    Me.ef_title.SufixText = "Sufix Text"
    Me.ef_title.SufixTextVisible = True
    Me.ef_title.TabIndex = 15
    Me.ef_title.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_title.TextValue = ""
    Me.ef_title.Value = ""
    Me.ef_title.ValueForeColor = System.Drawing.Color.Blue
    '
    'tab_control_HTML
    '
    Me.tab_control_HTML.Controls.Add(Me.tab_html)
    Me.tab_control_HTML.Controls.Add(Me.tab_preview)
    Me.tab_control_HTML.Location = New System.Drawing.Point(80, 69)
    Me.tab_control_HTML.Name = "tab_control_HTML"
    Me.tab_control_HTML.SelectedIndex = 0
    Me.tab_control_HTML.Size = New System.Drawing.Size(369, 381)
    Me.tab_control_HTML.TabIndex = 26
    '
    'tab_html
    '
    Me.tab_html.Controls.Add(Me.chk_add_style)
    Me.tab_html.Controls.Add(Me.tb_content)
    Me.tab_html.Controls.Add(Me.btn_insert_tag)
    Me.tab_html.Controls.Add(Me.cmbTag)
    Me.tab_html.Location = New System.Drawing.Point(4, 22)
    Me.tab_html.Name = "tab_html"
    Me.tab_html.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_html.Size = New System.Drawing.Size(361, 355)
    Me.tab_html.TabIndex = 0
    Me.tab_html.Text = "HTML"
    Me.tab_html.UseVisualStyleBackColor = True
    '
    'chk_add_style
    '
    Me.chk_add_style.AutoSize = True
    Me.chk_add_style.Location = New System.Drawing.Point(129, 10)
    Me.chk_add_style.Name = "chk_add_style"
    Me.chk_add_style.Size = New System.Drawing.Size(81, 17)
    Me.chk_add_style.TabIndex = 24
    Me.chk_add_style.Text = "Add Style"
    Me.chk_add_style.UseVisualStyleBackColor = True
    '
    'tb_content
    '
    Me.tb_content.Location = New System.Drawing.Point(6, 47)
    Me.tb_content.Name = "tb_content"
    Me.tb_content.Size = New System.Drawing.Size(349, 302)
    Me.tb_content.TabIndex = 19
    Me.tb_content.Text = ""
    '
    'btn_insert_tag
    '
    Me.btn_insert_tag.Location = New System.Drawing.Point(221, 6)
    Me.btn_insert_tag.Name = "btn_insert_tag"
    Me.btn_insert_tag.Size = New System.Drawing.Size(65, 23)
    Me.btn_insert_tag.TabIndex = 23
    Me.btn_insert_tag.Text = "insert"
    Me.btn_insert_tag.UseVisualStyleBackColor = True
    '
    'cmbTag
    '
    Me.cmbTag.FormattingEnabled = True
    Me.cmbTag.Location = New System.Drawing.Point(6, 6)
    Me.cmbTag.Name = "cmbTag"
    Me.cmbTag.Size = New System.Drawing.Size(105, 21)
    Me.cmbTag.TabIndex = 21
    '
    'tab_preview
    '
    Me.tab_preview.Controls.Add(Me.web_browser_html)
    Me.tab_preview.Location = New System.Drawing.Point(4, 22)
    Me.tab_preview.Name = "tab_preview"
    Me.tab_preview.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_preview.Size = New System.Drawing.Size(361, 355)
    Me.tab_preview.TabIndex = 2
    Me.tab_preview.Text = "Preview"
    Me.tab_preview.UseVisualStyleBackColor = True
    '
    'web_browser_html
    '
    Me.web_browser_html.Location = New System.Drawing.Point(11, 10)
    Me.web_browser_html.MinimumSize = New System.Drawing.Size(20, 20)
    Me.web_browser_html.Name = "web_browser_html"
    Me.web_browser_html.Size = New System.Drawing.Size(344, 244)
    Me.web_browser_html.TabIndex = 22
    '
    'frm_about_us_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(627, 524)
    Me.Name = "frm_about_us_edit"
    Me.Text = "Acerca de Nosotros"
    Me.panel_data.ResumeLayout(False)
    Me.tab_control_HTML.ResumeLayout(False)
    Me.tab_html.ResumeLayout(False)
    Me.tab_html.PerformLayout()
    Me.tab_preview.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents lbl_content As System.Windows.Forms.Label
  Friend WithEvents ef_title As GUI_Controls.uc_entry_field
  Friend WithEvents tab_control_HTML As System.Windows.Forms.TabControl
  Friend WithEvents tab_html As System.Windows.Forms.TabPage
  Friend WithEvents chk_add_style As System.Windows.Forms.CheckBox
  Friend WithEvents tb_content As System.Windows.Forms.RichTextBox
  Friend WithEvents btn_insert_tag As System.Windows.Forms.Button
  Friend WithEvents cmbTag As System.Windows.Forms.ComboBox
  Friend WithEvents tab_preview As System.Windows.Forms.TabPage
  Friend WithEvents web_browser_html As System.Windows.Forms.WebBrowser
End Class
