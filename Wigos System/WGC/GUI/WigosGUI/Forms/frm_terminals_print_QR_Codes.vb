﻿'-------------------------------------------------------------------
' Copyright © 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
'   MODULE NAME : frm_terminals_print_QR_Codes
'   DESCRIPTION : Labels with specific QR Codes are printed for selected terminals
'        AUTHOR : Jorge Concheyro
' CREATION DATE : 16-NOV-2015 
'
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 16-NOV-2015  JCC    Initial version. Product Backlog Item 5993 GUI - Creación e Impresión Códigos QR
' 08-AUG-2016  ETP    Fixed bug 16530 Add terminal to qr code generator.
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonMisc
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports WSI.Common
Imports WSI.Common.Groups
Imports GUI_Controls.uc_terminals_group_filter
Imports WSI.Labels
Imports System.Text
Imports System.Data.SqlClient


Public Class frm_terminals_print_QR_Codes
  Inherits frm_base_edit

#Region " Structures "

  Private Structure PARAMS_TO_ADD

    Dim item_group As ItemGroup
    Dim data_table_to_add As DataTable


  End Structure ' PARAMS_TO_ADD

#End Region ' Structures

#Region " Constants "

  Private Const GRID_COLUMNS As Integer = 8
  Private Const GRID_HEADER_ROWS As Integer = 1

  ' GRID TERMINALS CONFLICT
  Private Const GRID_COLUMN_SELECT As Integer = 0
  Private Const GRID_COLUMN_PROVIDER As Integer = 1
  Private Const GRID_COLUMN_NAME As Integer = 2
  Private Const GRID_COLUMN_FLOOR_ID As Integer = 3
  Private Const GRID_COLUMN_AREA As Integer = 4
  Private Const GRID_COLUMN_BANK_ID As Integer = 5
  Private Const GRID_COLUMN_POSITION As Integer = 6
  Private Const GRID_COLUMN_TERMINAL_ID As Integer = 7

  ' GRID TERMINALS CONFLICT WIDTH
  Private Const GRID_COLUMN_WIDTH_SELECT As Integer = 150
  Private Const GRID_COLUMN_WIDTH_PROVIDER As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_NAME As Integer = 2200
  Private Const GRID_COLUMN_WIDTH_FLOOR_ID As Integer = 1120
  Private Const GRID_COLUMN_WIDTH_BANK_ID As Integer = 1400
  Private Const GRID_COLUMN_WIDTH_AREA As Integer = 1900
  Private Const GRID_COLUMN_WIDTH_POSITION As Integer = 1000
  Private Const GRID_COLUMN_WIDTH_TERMINAL_ID As Integer = 0

  Private Const DATATABLE_TERMINAL_ID As Integer = 0
  Private Const DATATABLE_PROVIDER_NAME As Integer = 1
  Private Const DATATABLE_MACHINE_NAME As Integer = 2
  Private Const DATATABLE_FLOOR_ID As Integer = 3
  Private Const DATATABLE_AREA As Integer = 4
  Private Const DATATABLE_BANK As Integer = 5
  Private Const DATATABLE_POSITION As Integer = 6

#End Region 'Constants

#Region " Enums"

  Private Enum ENUM_RESULT_BLOCK
    OK = 0
    ERROR_AUDIT = 1
    ERROR_SETFLAG = 2

  End Enum ' ENUM_RESULT_BLOCK

#End Region ' Enums

#Region " Members "


  Private m_terminals As DataTable
  Friend WithEvents _plm As PrintLabelManager
  Private m_print_parameters As WSI.Common.PrintParameters
  Private m_printing As Boolean = False

#End Region 'Members

#Region " Overrides "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_SMARTFLOOR_TERMINALS_PRINT_QR_CODES

    'Call Base Form proc
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Database overridable operations.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)


    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
      Case ENUM_DB_OPERATION.DB_OPERATION_READ

      Case ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation




  ' PURPOSE: Initializes the window title and all the
  '          entry fields
  '          
  '    - INPUT: None
  '
  '    - OUTPUT: None
  '              
  ' RETURNS: None
  '          
  ' NOTES
  ' 
  Protected Overrides Sub GUI_InitControls()

    Dim _terminal_list As TerminalList

    Call MyBase.GUI_InitControls()

    ' Form
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6955) 'Imprimir QR-Codes

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_DELETE).Visible = False

    ' Print progress
    Me.gb_print_progress.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(649)  ' 649 "Progreso de impresión"
    Me.btn_pause_continue.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7)   ' 7   "Pausa"
    Me.btn_print_cancel.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6)     ' 6   "Cancelar"

    ' Groupbox exception

    Me.tgf_terminals.SetGroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6556) '"Máquinas"
    Me.tgf_terminals.ThreeStateCheckMode = False
    Me.tgf_terminals.Tree.CheckBoxes = False

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_OK).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_OK).Enabled = False


    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_PRINT).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_PRINT).Enabled = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_PRINT).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6957)  ' "Imprimir etiquetas"

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2) ' Exit

    ' Terminals list
    _terminal_list = New TerminalList
    _terminal_list.SetListType(TerminalList.TerminalListType.Listed)
    Me.tgf_terminals.SetTerminalList(_terminal_list)
    Me.tgf_terminals.Size = New System.Drawing.Size(275, 339)

    ' Stylesheet terminals exception
    Call GUI_StyleSheet(dg_terminals)

    ' Inizialize DataTables
    Call InizializeDataTableTerminalsToGrid(m_terminals)

    ' Print parameters
    Me.lbl_print_status.Text = ""
    Me.lblCountSelected.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6965, "0")
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_PRINT).Enabled = dg_terminals.NumRows > 0



  End Sub ' GUI_InitControls

  ' PURPOSE : Set initial data on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

  End Sub 'GUI_SetScreenData


  ' PURPOSE: Manage buttons pressed.
  '
  '  PARAMS:
  '     - INPUT:
  '         - ButtonId: Id. of the button clicked.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)
    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_PRINT
        PrintLabels()
      Case ENUM_BUTTON.BUTTON_CANCEL

        If m_printing Then

          If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(155), _
                  ENUM_MB_TYPE.MB_TYPE_WARNING, _
                  ENUM_MB_BTN.MB_BTN_YES_NO) <> ENUM_MB_RESULT.MB_RESULT_YES Then

            Return
          End If

          _plm.CancelRequested = True
        End If

        Call MyBase.GUI_ButtonClick(ButtonId)

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub ' GUI_ButtonClick

#End Region 'Overrides

#Region " Private Functions "

  ' PURPOSE: Returns the field to be used as AssetNumber
  '
  ' PARAMS:
  '       - TerminalID
  '
  ' RETURNS:
  '       - The selected field from Terminals table
  '
  ' NOTES:
  Private Function GetTerminalAssetNumber(ByRef TerminalID As Integer) As String
    ' from frm_cage_safe_add_movement
    Dim _sb As StringBuilder
    Dim _table As DataTable
    Dim _row As DataRow
    _sb = New StringBuilder
    _sb.AppendLine("    SELECT   TE_BASE_NAME  ")
    _sb.AppendLine("      FROM   TERMINALS ")
    _sb.AppendLine("     WHERE   TE_TERMINAL_ID = " + TerminalID.ToString)

    Try
      _table = GUI_GetTableUsingSQL(_sb.ToString(), Integer.MaxValue)
    Catch ex As Exception
      _table = New DataTable
    End Try
    _row = _table.Rows.Item(0)

    If IsDBNull(_row(0)) Then
      Return ""
    Else
      Return _row(0)
    End If

  End Function ' GetTerminalAssetNumber

  ' PURPOSE : Block / Unblock All Terminales from a site
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - True or False
  '


  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet(ByRef Dg As GUI_Controls.uc_grid)

    With Dg
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, Me.Permissions.Write)
      .Sortable = True


      ' Blank "select" column at the left of the grid
      .Column(GRID_COLUMN_SELECT).Header.Text = ""
      .Column(GRID_COLUMN_SELECT).Width = GRID_COLUMN_WIDTH_SELECT
      .Column(GRID_COLUMN_SELECT).IsColumnPrintable = False

      ' Provider
      .Column(GRID_COLUMN_PROVIDER).Header.Text = GLB_NLS_GUI_AUDITOR.GetString(341) ' Proveedor
      .Column(GRID_COLUMN_PROVIDER).Width = GRID_COLUMN_WIDTH_PROVIDER
      .Column(GRID_COLUMN_PROVIDER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Terminal name
      .Column(GRID_COLUMN_NAME).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6636) ' Máquina
      .Column(GRID_COLUMN_NAME).Width = GRID_COLUMN_WIDTH_NAME
      .Column(GRID_COLUMN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Floor ID
      .Column(GRID_COLUMN_FLOOR_ID).Header.Text = GLB_NLS_GUI_CONFIGURATION.GetString(432) ' Id de planta
      .Column(GRID_COLUMN_FLOOR_ID).Width = GRID_COLUMN_WIDTH_FLOOR_ID
      .Column(GRID_COLUMN_FLOOR_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Location area  
      .Column(GRID_COLUMN_AREA).Header.Text = GLB_NLS_GUI_CONFIGURATION.GetString(435) ' Area
      .Column(GRID_COLUMN_AREA).Width = GRID_COLUMN_WIDTH_AREA
      .Column(GRID_COLUMN_AREA).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Bank_id (Island)
      .Column(GRID_COLUMN_BANK_ID).Header.Text = GLB_NLS_GUI_CONFIGURATION.GetString(431) ' Isla
      .Column(GRID_COLUMN_BANK_ID).Width = GRID_COLUMN_WIDTH_BANK_ID
      .Column(GRID_COLUMN_BANK_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Position
      .Column(GRID_COLUMN_POSITION).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5222) ' Posición
      .Column(GRID_COLUMN_POSITION).Width = GRID_COLUMN_WIDTH_POSITION
      .Column(GRID_COLUMN_POSITION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Terminal Id
      .Column(GRID_COLUMN_TERMINAL_ID).Header.Text = ""
      .Column(GRID_COLUMN_TERMINAL_ID).Width = GRID_COLUMN_WIDTH_TERMINAL_ID

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Add a tree node to element's list
  '
  '    - INPUT:
  '         - ParamsToAdd (PARAMS_TO_ADD)
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub AddTreeNodeToGrid(ByRef ParamsToAdd As PARAMS_TO_ADD)

    Select Case ParamsToAdd.item_group.ElementType

      Case GROUP_NODE_ELEMENT.GROUP, _
           GROUP_NODE_ELEMENT.GROUPS, _
           GROUP_NODE_ELEMENT.PROVIDER, _
           GROUP_NODE_ELEMENT.PROVIDERS, _
           GROUP_NODE_ELEMENT.ZONE, _
           GROUP_NODE_ELEMENT.ZONES, _
           GROUP_NODE_ELEMENT.AREAS, _
           GROUP_NODE_ELEMENT.BANKS

        ' Calculate subelements of tree node root
        If Not ParamsToAdd.item_group.ElementExpanded Then
          Me.tgf_terminals.AddChildNodes(ParamsToAdd.item_group)
        End If

        ' Add all child elements to list
        For Each _node As ItemGroup In ParamsToAdd.item_group.Nodes
          ParamsToAdd.item_group = _node
          Call AddTreeNodeToGrid(ParamsToAdd)
        Next

      Case GROUP_NODE_ELEMENT.TERMINALS

        ' Add element to list
        Call AddNewElement(ParamsToAdd)

    End Select

  End Sub ' AddTreeNodeToList

  ' PURPOSE: Add new element
  '
  '  PARAMS:
  '     - INPUT:
  '         - ParamsToAdd (PARAMS_TO_ADD)
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub AddNewElement(ByRef ParamsToAdd As PARAMS_TO_ADD)


    ' Add Item in DataTable
    If ParamsToAdd.data_table_to_add.Select("TERMINAL_ID = " & ParamsToAdd.item_group.ElementTerminalId).Length = 0 Then
      Call NewRowToDataTable(ParamsToAdd.item_group.ElementTerminalId, ParamsToAdd.item_group.ElementName, ParamsToAdd.data_table_to_add)
    End If

  End Sub ' AddNewElement

  ' PURPOSE: Delete elements on grid.
  '
  '  PARAMS:
  '     - INPUT:
  '          - Grid to delete elements
  '          - DataTable to delete elements
  '          - Delete all boolean
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub DeleteElementsGrid(ByRef GridToDelete As uc_grid, ByRef DataTableTerminals As DataTable, ByVal DeleteAll As Boolean)

    Dim _selected_rows As Array
    Dim _idx_row As Integer
    Dim _idx As Integer

    If DeleteAll Then
      DataTableTerminals.Clear()
      GridToDelete.Clear()
    Else
      _selected_rows = GridToDelete.SelectedRows()
      If _selected_rows Is Nothing Then
        Return
      End If

      For _idx = _selected_rows.Length - 1 To 0 Step -1
        _idx_row = _selected_rows(_idx)

        If (_idx_row <= GridToDelete.NumRows - 1) Then
          If GridToDelete.Row(_idx_row).IsSelected = True OrElse DeleteAll Then
            DataTableTerminals.Rows.Remove(DataTableTerminals.Select("TERMINAL_ID = " & GridToDelete.Cell(_idx_row, GRID_COLUMN_TERMINAL_ID).Value)(0))
            GridToDelete.DeleteRow(_idx_row)
          End If
        End If
      Next
    End If
    lblCountSelected.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6965, GridToDelete.NumRows.ToString)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_PRINT).Enabled = dg_terminals.NumRows > 0

  End Sub ' DeleteElementsGrid

  ' PURPOSE: Refresh grid.
  '
  '  PARAMS:
  '     - INPUT:
  '          - Grid to add rows
  '          - DataTable origen
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub RefreshGrid(ByRef GridToAdd As uc_grid, ByRef DataTableTerminals As DataTable)

    Dim _view As DataView
    Dim _dt As DataTable

    GridToAdd.Redraw = False
    GridToAdd.Clear()

    _view = New DataView(DataTableTerminals)
    _dt = New DataTable()

    _view.Sort = "PROVIDER_NAME, MACHINE_NAME"
    _dt = _view.ToTable()

    For Each _row As DataRow In _dt.Rows
      GridToAdd.AddRow()

      GridToAdd.Cell(GridToAdd.NumRows - 1, GRID_COLUMN_PROVIDER).Value = _row.Item(DATATABLE_PROVIDER_NAME).ToString()
      GridToAdd.Cell(GridToAdd.NumRows - 1, GRID_COLUMN_NAME).Value = _row.Item(DATATABLE_MACHINE_NAME).ToString()
      GridToAdd.Cell(GridToAdd.NumRows - 1, GRID_COLUMN_FLOOR_ID).Value = _row.Item(DATATABLE_FLOOR_ID).ToString()
      GridToAdd.Cell(GridToAdd.NumRows - 1, GRID_COLUMN_AREA).Value = _row.Item(DATATABLE_AREA).ToString()
      GridToAdd.Cell(GridToAdd.NumRows - 1, GRID_COLUMN_BANK_ID).Value = _row.Item(DATATABLE_BANK).ToString()
      GridToAdd.Cell(GridToAdd.NumRows - 1, GRID_COLUMN_POSITION).Value = _row.Item(DATATABLE_POSITION).ToString()
      GridToAdd.Cell(GridToAdd.NumRows - 1, GRID_COLUMN_TERMINAL_ID).Value = _row.Item(DATATABLE_TERMINAL_ID).ToString()
    Next

    GridToAdd.Redraw = True
    lblCountSelected.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6965, GridToAdd.NumRows.ToString)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_PRINT).Enabled = dg_terminals.NumRows > 0

  End Sub ' RefreshGrid

  ' PURPOSE: Inizialize DataTables.
  '
  '  PARAMS:
  '     - INPUT:
  '          - DataTable to inizialize
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub InizializeDataTableTerminalsToGrid(ByRef DataTableTerminals As DataTable)

    DataTableTerminals = New DataTable

    DataTableTerminals.Columns.Add("TERMINAL_ID", Type.GetType("System.Int32"))
    DataTableTerminals.Columns.Add("PROVIDER_NAME", Type.GetType("System.String"))
    DataTableTerminals.Columns.Add("MACHINE_NAME", Type.GetType("System.String"))
    DataTableTerminals.Columns.Add("FLOOR_ID", Type.GetType("System.String"))
    DataTableTerminals.Columns.Add("AREA", Type.GetType("System.String"))
    DataTableTerminals.Columns.Add("BANK", Type.GetType("System.String"))
    DataTableTerminals.Columns.Add("POSITION", Type.GetType("System.String"))

  End Sub ' InizializeDataTableTerminalsToGrid

  ' PURPOSE: New row to DataTable.
  '
  '  PARAMS:
  '     - INPUT:
  '          - Element Id of terminal report
  '          - Element Name of terminal report
  '          - DataTable destination terminal
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub NewRowToDataTable(ByVal ElementId As Integer, ByVal ElementName As String, ByRef DataTableTerminals As DataTable)

    Dim _row As DataRow
    Dim _index As Integer
    Dim _report As List(Of Object)


    _row = DataTableTerminals.NewRow()
    _index = 1

    _report = TerminalReport.GetReportDataList(ElementId, ReportType.Full)

    _row.Item(DATATABLE_TERMINAL_ID) = ElementId  'Terminal ID
    _row.Item(DATATABLE_PROVIDER_NAME) = _report(0)  'Provider Name
    _row.Item(DATATABLE_MACHINE_NAME) = ElementName 'Terminal Name
    _row.Item(DATATABLE_FLOOR_ID) = _report(2) 'floor ID
    _row.Item(DATATABLE_AREA) = _report(4) 'area
    _row.Item(DATATABLE_BANK) = _report(5) 'bank
    _row.Item(DATATABLE_POSITION) = _report(6) 'position


    DataTableTerminals.Rows.Add(_row)

  End Sub ' NewRowToDataTable

  ' PURPOSE: Set DataTable with screen data.
  '
  '  PARAMS:
  '     - INPUT:
  '          - DataTable of origen terminals
  '          - DataTable of destination
  '          - Grid of destionation info
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub SetDataTableData(ByVal DataTableOrigen As DataTable, ByRef DataTableDestination As DataTable, ByRef GridDestination As uc_grid)

    If DataTableOrigen.Rows.Count > 0 Then

      For Each _row As DataRow In DataTableOrigen.Rows
        Call NewRowToDataTable(_row(DATATABLE_TERMINAL_ID), _row(DATATABLE_MACHINE_NAME), DataTableDestination)
      Next

      Call RefreshGrid(GridDestination, DataTableDestination)

    End If

  End Sub ' SetDataTableData

  ' PURPOSE: Get DataTable data to screen.
  '
  '  PARAMS:
  '     - INPUT:
  '          - DataTable of Terminals
  '          - Machine_flags
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '          - DataTable
  Private Function GetDataTableData(ByRef DataTableTerminals As DataTable, ByVal MachineFlag As TerminalStatusFlags.MACHINE_FLAGS) As DataTable

    Dim _dt As DataTable
    Dim _dr As DataRow

    _dt = New DataTable()
    _dt.Columns.Add("TS_TERMINAL_ID", Type.GetType("System.Int32"))
    _dt.Columns.Add("TS_MACHINE_FLAGS", Type.GetType("System.Int32"))
    _dt.Columns.Add("TE_NAME", Type.GetType("System.String"))

    Try

      For Each _terminal As DataRow In DataTableTerminals.Rows
        _dr = _dt.NewRow()
        _dr("TS_TERMINAL_ID") = _terminal.Item(DATATABLE_TERMINAL_ID)
        _dr("TS_MACHINE_FLAGS") = MachineFlag
        _dr("TE_NAME") = _terminal.Item(DATATABLE_MACHINE_NAME)
        _dt.Rows.Add(_dr)
      Next

      ' Fill DataTable with all terminals
      If m_terminals Is Nothing Then
        m_terminals = New DataTable()
      End If
      m_terminals.Merge(_dt)

    Catch _ex As Exception
      Debug.WriteLine(_ex.Message)
    End Try

    Return _dt

  End Function ' GetDataTableData

  Private Sub PrintProgress(sender As Object, e As EventArgs) Handles _plm.PrintProgress
    lbl_print_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6956, GUI_FormatNumber(_plm.CurrentPage, 0), GUI_FormatNumber(_plm.PageCount, 0))

    Dim tick0 As Integer
    tick0 = WSI.Common.Misc.GetTickCount()
    While WSI.Common.Misc.GetElapsedTicks(tick0) < 1000
      System.Threading.Thread.Sleep(10)
      Application.DoEvents()
    End While

  End Sub

  Private Sub PrintLabels()
    ' Create a print label manager
    _plm = PrintLabelManager.Create()

    _plm.GLB_CurrentUser_GuiId = GLB_CurrentUser.GuiId
    _plm.GLB_CurrentUser_Id = GLB_CurrentUser.Id
    _plm.GLB_CurrentUser_Name = GLB_CurrentUser.Name
    _plm.GLB_Computer_Name = Environment.MachineName


    Call FillPrintLabel(_plm)
    ' Print all labels 
    btn_pause_continue.Enabled = True
    btn_print_cancel.Enabled = True

    Try
      m_printing = True
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_PRINT).Enabled = False
      _plm.Print()
    Catch

    End Try
    m_printing = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_PRINT).Enabled = True


    btn_pause_continue.Enabled = False
    btn_print_cancel.Enabled = False
    lbl_print_status.Text = ""
    Me.btn_pause_continue.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7)   ' 7   "Pausa"

  End Sub ' PrintVoucher


  ' PURPOSE : normalizes the tags in case where written in other capitalizations i.e: AsSet, ASSET, AsseT
  '
  '  PARAMS:
  '     - INPUT:
  '         - Mask, Tag
  '     - OUTPUT:
  '         - 
  '
  ' RETURNS :
  '     
  Private Sub TagToUpper(ByRef Mask As String, Tag As String)

    Dim _startpos As Integer = Mask.ToUpper.IndexOf(Tag.ToUpper)
    If _startpos > -1 Then
      If _startpos > 1 Then
        Mask = Mask.Substring(1, Mask - 1) + Tag + Mask.Substring(_startpos + Tag.Length, Mask.Length - Tag.Length - _startpos)
      Else
        Mask = Tag + Mask.Substring(Tag.Length, Mask.Length - Tag.Length)
      End If
    End If

  End Sub

  ' PURPOSE : Adds a transfer order data into a PrintLabelManager object
  '
  '  PARAMS:
  '     - INPUT:
  '         - TransferOrder
  '     - OUTPUT:
  '         - Plm
  '
  ' RETURNS :
  '     
  Public Sub FillPrintLabel(ByRef Plm As PrintLabelManager)
    Dim _lbl As PrintLabel
    Dim _asset_number As String
    Dim _area As String
    Dim _bank As String
    Dim _position As String
    Dim _qr_code_mask As String
    Dim _qr_footer_mask As String

    Dim _qr_code_mask_gp As String
    Dim _qr_footer_mask_gp As String


    _qr_code_mask_gp = GeneralParam.GetString("Terminal", "QR", "")    ' http;//www.casino.com#{@QR}
    _qr_footer_mask_gp = GeneralParam.GetString("Terminal", "QR.Footer", "")   '  {@AssetNumber}

    ' Regularizes capitalization of the tags
    TagToUpper("{@QR}", _qr_code_mask_gp)
    TagToUpper("{@AssetNumber}", _qr_code_mask_gp)
    TagToUpper("{@Location}", _qr_code_mask_gp)
    TagToUpper("{@Bank}", _qr_code_mask_gp)
    TagToUpper("{@Position}", _qr_code_mask_gp)
    ' --
    TagToUpper("{@QR}", _qr_footer_mask_gp)
    TagToUpper("{@AssetNumber}", _qr_footer_mask_gp)
    TagToUpper("{@Location}", _qr_footer_mask_gp)
    TagToUpper("{@Bank}", _qr_footer_mask_gp)
    TagToUpper("{@Position}", _qr_footer_mask_gp)



    If m_terminals.Rows.Count > 0 Then
      For Each _row As DataRow In m_terminals.Rows
        ' Create a print label
        _asset_number = GetTerminalAssetNumber(_row(DATATABLE_TERMINAL_ID))
        _area = _row(DATATABLE_AREA)
        _bank = _row(DATATABLE_BANK)
        _position = IIf(_row(DATATABLE_POSITION).GetType.Name.ToString = "DBNull", "", _row(DATATABLE_POSITION))

        _lbl = Plm.CreatePrintLabel()

        _lbl.AssetNumber = _asset_number
        _lbl.Name = _row.Item(DATATABLE_MACHINE_NAME).ToString()
        _qr_code_mask = _qr_code_mask_gp
        _qr_footer_mask = _qr_footer_mask_gp

        ' replacing tags
        _qr_code_mask = _qr_code_mask.Replace("{@QR}", _asset_number)
        _qr_code_mask = _qr_code_mask.Replace("{@AssetNumber}", _asset_number)
        _qr_code_mask = _qr_code_mask.Replace("{@Location}", _area)
        _qr_code_mask = _qr_code_mask.Replace("{@Bank}", _bank)
        _qr_code_mask = _qr_code_mask.Replace("{@Position}", _position)

        _qr_footer_mask = _qr_footer_mask.Replace("{@QR}", _asset_number)
        _qr_footer_mask = _qr_footer_mask.Replace("{@AssetNumber}", _asset_number)
        _qr_footer_mask = _qr_footer_mask.Replace("{@Location}", _area)
        _qr_footer_mask = _qr_footer_mask.Replace("{@Bank}", _bank)
        _qr_footer_mask = _qr_footer_mask.Replace("{@Position}", _position)

        _lbl.QR_Code = _qr_code_mask
        _lbl.QR_Footer = _qr_footer_mask

        ' Add the label to the manager
        Plm.Add(_lbl)
      Next
    End If

  End Sub


#End Region 'Private functions

#Region " Events "



  Private Sub btn_add_block_Click(sender As Object, e As EventArgs) Handles btn_add_block.Click

    Dim _params As PARAMS_TO_ADD

    Try

      ' Add the selected tree node to element's list
      If Not tgf_terminals.Tree.SelectedNode Is Nothing Then

        _params.item_group = CType(tgf_terminals.Tree.SelectedNode, ItemGroup)
        _params.data_table_to_add = m_terminals

        Call AddTreeNodeToGrid(_params)
        Call RefreshGrid(Me.dg_terminals, _params.data_table_to_add)


      End If

    Catch _ex As Exception
      Debug.WriteLine(_ex.Message)
    End Try

  End Sub ' btn_add_block_Click

  Private Sub btn_rmv_Click(sender As Object, e As EventArgs) Handles btn_rmv.Click

    Try

      DeleteElementsGrid(dg_terminals, m_terminals, False)

    Catch _ex As Exception
      Debug.WriteLine(_ex.Message)
    End Try

  End Sub ' btn_rmv_block_Click

  Private Sub btn_rmv_all_Click(sender As Object, e As EventArgs) Handles btn_rmv_all.Click

    Try

      If dg_terminals.NumRows = 0 Then
        Return
      End If

      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(168), _
              ENUM_MB_TYPE.MB_TYPE_WARNING, _
              ENUM_MB_BTN.MB_BTN_YES_NO, _
              ENUM_MB_DEF_BTN.MB_DEF_BTN_2) <> ENUM_MB_RESULT.MB_RESULT_YES Then
        Return
      End If

      DeleteElementsGrid(dg_terminals, m_terminals, True)

    Catch _ex As Exception
      Debug.WriteLine(_ex.Message)
    End Try

  End Sub ' btn_rmv_block_all_Click

  Private Sub btn_print_cancel_ClickEvent() Handles btn_print_cancel.ClickEvent
    If (_plm IsNot Nothing) Then
      _plm.CancelRequested = True
      _plm.PauseRequested = False
      Me.btn_pause_continue.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7)   ' 7   "Pausa"
      Me.lbl_print_status.Text = ""
    End If
  End Sub


  Private Sub btn_pause_continue_ClickEvent() Handles btn_pause_continue.ClickEvent
    If (_plm IsNot Nothing) Then
      If Not (_plm.PauseRequested) Then
        _plm.PauseRequested = True
        Me.lbl_print_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(652)
        btn_pause_continue.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8) ' 8    Continuar
      Else
        _plm.PauseRequested = False
        Me.lbl_print_status.Text = ""
        Me.btn_pause_continue.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7)   ' 7   "Pausa"
      End If
    End If

  End Sub

#End Region ' Events

End Class ' frm_terminals_print_QR_Codes