<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_productivity_reports
  Inherits GUI_Controls.frm_base_print

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector()
    Me.uc_pr_list = New GUI_Controls.uc_provider()
    Me.tab_productivity = New System.Windows.Forms.TabControl()
    Me.ef_vendor_id = New GUI_Controls.uc_entry_field()
    Me.panel_grids.SuspendLayout()
    Me.panel_filter.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_grids
    '
    Me.panel_grids.Controls.Add(Me.tab_productivity)
    Me.panel_grids.Location = New System.Drawing.Point(5, 221)
    Me.panel_grids.Size = New System.Drawing.Size(1224, 497)
    Me.panel_grids.Controls.SetChildIndex(Me.panel_buttons, 0)
    Me.panel_grids.Controls.SetChildIndex(Me.tab_productivity, 0)
    '
    'panel_buttons
    '
    Me.panel_buttons.Location = New System.Drawing.Point(1133, 0)
    Me.panel_buttons.Size = New System.Drawing.Size(91, 497)
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.ef_vendor_id)
    Me.panel_filter.Controls.Add(Me.uc_dsl)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1224, 194)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dsl, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_vendor_id, 0)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Location = New System.Drawing.Point(5, 198)
    Me.pn_separator_line.Size = New System.Drawing.Size(1224, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1224, 4)
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.ClosingTimeEnabled = True
    Me.uc_dsl.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.Location = New System.Drawing.Point(7, 3)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.ShowBorder = True
    Me.uc_dsl.Size = New System.Drawing.Size(260, 82)
    Me.uc_dsl.TabIndex = 0
    Me.uc_dsl.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    '
    'uc_pr_list
    '
    Me.uc_pr_list.Location = New System.Drawing.Point(273, 0)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(387, 194)
    Me.uc_pr_list.TabIndex = 2
    Me.uc_pr_list.TerminalListHasValues = False
    '
    'tab_productivity
    '
    Me.tab_productivity.Dock = System.Windows.Forms.DockStyle.Fill
    Me.tab_productivity.Location = New System.Drawing.Point(0, 0)
    Me.tab_productivity.Name = "tab_productivity"
    Me.tab_productivity.SelectedIndex = 0
    Me.tab_productivity.Size = New System.Drawing.Size(1133, 497)
    Me.tab_productivity.TabIndex = 3
    '
    'ef_vendor_id
    '
    Me.ef_vendor_id.AllowDrop = True
    Me.ef_vendor_id.DoubleValue = 0.0R
    Me.ef_vendor_id.IntegerValue = 0
    Me.ef_vendor_id.IsReadOnly = False
    Me.ef_vendor_id.Location = New System.Drawing.Point(7, 107)
    Me.ef_vendor_id.Name = "ef_vendor_id"
    Me.ef_vendor_id.PlaceHolder = Nothing
    Me.ef_vendor_id.Size = New System.Drawing.Size(260, 24)
    Me.ef_vendor_id.SufixText = "Sufix Text"
    Me.ef_vendor_id.SufixTextVisible = True
    Me.ef_vendor_id.TabIndex = 1
    Me.ef_vendor_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_vendor_id.TextValue = ""
    Me.ef_vendor_id.TextWidth = 110
    Me.ef_vendor_id.Value = ""
    Me.ef_vendor_id.ValueForeColor = System.Drawing.Color.Blue
    '
    'frm_productivity_reports
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1234, 722)
    Me.Name = "frm_productivity_reports"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_productivity_reports"
    Me.panel_grids.ResumeLayout(False)
    Me.panel_filter.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents tab_productivity As System.Windows.Forms.TabControl
  Friend WithEvents ef_vendor_id As GUI_Controls.uc_entry_field
End Class
