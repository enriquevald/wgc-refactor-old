﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_table_players_report
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_table_players_report))
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector()
    Me.uc_checked_list_table_type = New GUI_Controls.uc_checked_list()
    Me.uc_account_select = New GUI_Controls.uc_account_sel()
    Me.gb_report = New System.Windows.Forms.GroupBox()
    Me.uc_entry_netwin = New GUI_Controls.uc_entry_field()
    Me.uc_entry_chips_in = New GUI_Controls.uc_entry_field()
    Me.uc_entry_buy_in = New GUI_Controls.uc_entry_field()
    Me.uc_entry_win = New GUI_Controls.uc_entry_field()
    Me.uc_entry_drop = New GUI_Controls.uc_entry_field()
    Me.uc_multicurrency = New GUI_Controls.uc_multi_currency_site_sel()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_report.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.uc_multicurrency)
    Me.panel_filter.Controls.Add(Me.uc_account_select)
    Me.panel_filter.Controls.Add(Me.uc_checked_list_table_type)
    Me.panel_filter.Controls.Add(Me.uc_dsl)
    Me.panel_filter.Size = New System.Drawing.Size(1214, 176)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dsl, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_checked_list_table_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account_select, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_multicurrency, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 180)
    Me.panel_data.Size = New System.Drawing.Size(1214, 430)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Controls.Add(Me.gb_report)
    Me.pn_separator_line.Size = New System.Drawing.Size(1208, 84)
    Me.pn_separator_line.Controls.SetChildIndex(Me.pn_line, 0)
    Me.pn_separator_line.Controls.SetChildIndex(Me.gb_report, 0)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1208, 4)
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.ClosingTimeEnabled = False
    Me.uc_dsl.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.FromDateText = "#5339"
    Me.uc_dsl.Location = New System.Drawing.Point(6, 0)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.ShowBorder = True
    Me.uc_dsl.Size = New System.Drawing.Size(254, 82)
    Me.uc_dsl.TabIndex = 11
    Me.uc_dsl.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    Me.uc_dsl.ToDateText = "#5340"
    '
    'uc_checked_list_table_type
    '
    Me.uc_checked_list_table_type.GroupBoxText = "xCheckedList"
    Me.uc_checked_list_table_type.Location = New System.Drawing.Point(265, 0)
    Me.uc_checked_list_table_type.m_resize_width = 374
    Me.uc_checked_list_table_type.multiChoice = True
    Me.uc_checked_list_table_type.Name = "uc_checked_list_table_type"
    Me.uc_checked_list_table_type.SelectedIndexes = New Integer(-1) {}
    Me.uc_checked_list_table_type.SelectedIndexesList = ""
    Me.uc_checked_list_table_type.SelectedIndexesListLevel2 = ""
    Me.uc_checked_list_table_type.SelectedValuesArray = New String(-1) {}
    Me.uc_checked_list_table_type.SelectedValuesList = ""
    Me.uc_checked_list_table_type.SelectedValuesListLevel2 = ""
    Me.uc_checked_list_table_type.SetLevels = 2
    Me.uc_checked_list_table_type.Size = New System.Drawing.Size(374, 173)
    Me.uc_checked_list_table_type.TabIndex = 12
    Me.uc_checked_list_table_type.ValuesArray = New String(-1) {}
    '
    'uc_account_select
    '
    Me.uc_account_select.Account = ""
    Me.uc_account_select.AccountText = ""
    Me.uc_account_select.Anon = False
    Me.uc_account_select.AutoSize = True
    Me.uc_account_select.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_account_select.BirthDate = New Date(CType(0, Long))
    Me.uc_account_select.DisabledHolder = False
    Me.uc_account_select.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_account_select.Holder = ""
    Me.uc_account_select.Location = New System.Drawing.Point(640, -3)
    Me.uc_account_select.MassiveSearchNumbers = ""
    Me.uc_account_select.MassiveSearchNumbersToEdit = ""
    Me.uc_account_select.MassiveSearchType = 0
    Me.uc_account_select.Name = "uc_account_select"
    Me.uc_account_select.SearchTrackDataAsInternal = True
    Me.uc_account_select.ShowMassiveSearch = False
    Me.uc_account_select.ShowVipClients = True
    Me.uc_account_select.Size = New System.Drawing.Size(307, 134)
    Me.uc_account_select.TabIndex = 13
    Me.uc_account_select.Telephone = ""
    Me.uc_account_select.TrackData = ""
    Me.uc_account_select.Vip = False
    Me.uc_account_select.WeddingDate = New Date(CType(0, Long))
    '
    'gb_report
    '
    Me.gb_report.Controls.Add(Me.uc_entry_netwin)
    Me.gb_report.Controls.Add(Me.uc_entry_chips_in)
    Me.gb_report.Controls.Add(Me.uc_entry_buy_in)
    Me.gb_report.Controls.Add(Me.uc_entry_win)
    Me.gb_report.Controls.Add(Me.uc_entry_drop)
    Me.gb_report.Location = New System.Drawing.Point(3, 18)
    Me.gb_report.Name = "gb_report"
    Me.gb_report.Size = New System.Drawing.Size(1041, 56)
    Me.gb_report.TabIndex = 1
    Me.gb_report.TabStop = False
    Me.gb_report.Text = "#8669"
    '
    'uc_entry_netwin
    '
    Me.uc_entry_netwin.DoubleValue = 0.0R
    Me.uc_entry_netwin.Enabled = False
    Me.uc_entry_netwin.IntegerValue = 0
    Me.uc_entry_netwin.IsReadOnly = False
    Me.uc_entry_netwin.Location = New System.Drawing.Point(830, 21)
    Me.uc_entry_netwin.Name = "uc_entry_netwin"
    Me.uc_entry_netwin.PlaceHolder = Nothing
    Me.uc_entry_netwin.ShortcutsEnabled = True
    Me.uc_entry_netwin.Size = New System.Drawing.Size(200, 24)
    Me.uc_entry_netwin.SufixText = "Sufix Text"
    Me.uc_entry_netwin.SufixTextVisible = True
    Me.uc_entry_netwin.TabIndex = 4
    Me.uc_entry_netwin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.uc_entry_netwin.TextValue = ""
    Me.uc_entry_netwin.Value = ""
    Me.uc_entry_netwin.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_entry_chips_in
    '
    Me.uc_entry_chips_in.DoubleValue = 0.0R
    Me.uc_entry_chips_in.Enabled = False
    Me.uc_entry_chips_in.IntegerValue = 0
    Me.uc_entry_chips_in.IsReadOnly = False
    Me.uc_entry_chips_in.Location = New System.Drawing.Point(624, 21)
    Me.uc_entry_chips_in.Name = "uc_entry_chips_in"
    Me.uc_entry_chips_in.PlaceHolder = Nothing
    Me.uc_entry_chips_in.ShortcutsEnabled = True
    Me.uc_entry_chips_in.Size = New System.Drawing.Size(200, 24)
    Me.uc_entry_chips_in.SufixText = "Sufix Text"
    Me.uc_entry_chips_in.SufixTextVisible = True
    Me.uc_entry_chips_in.TabIndex = 3
    Me.uc_entry_chips_in.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.uc_entry_chips_in.TextValue = ""
    Me.uc_entry_chips_in.Value = ""
    Me.uc_entry_chips_in.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_entry_buy_in
    '
    Me.uc_entry_buy_in.DoubleValue = 0.0R
    Me.uc_entry_buy_in.Enabled = False
    Me.uc_entry_buy_in.IntegerValue = 0
    Me.uc_entry_buy_in.IsReadOnly = False
    Me.uc_entry_buy_in.Location = New System.Drawing.Point(418, 20)
    Me.uc_entry_buy_in.Name = "uc_entry_buy_in"
    Me.uc_entry_buy_in.PlaceHolder = Nothing
    Me.uc_entry_buy_in.ShortcutsEnabled = True
    Me.uc_entry_buy_in.Size = New System.Drawing.Size(200, 24)
    Me.uc_entry_buy_in.SufixText = "Sufix Text"
    Me.uc_entry_buy_in.SufixTextVisible = True
    Me.uc_entry_buy_in.TabIndex = 2
    Me.uc_entry_buy_in.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.uc_entry_buy_in.TextValue = ""
    Me.uc_entry_buy_in.Value = ""
    Me.uc_entry_buy_in.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_entry_win
    '
    Me.uc_entry_win.DoubleValue = 0.0R
    Me.uc_entry_win.Enabled = False
    Me.uc_entry_win.IntegerValue = 0
    Me.uc_entry_win.IsReadOnly = False
    Me.uc_entry_win.Location = New System.Drawing.Point(212, 20)
    Me.uc_entry_win.Name = "uc_entry_win"
    Me.uc_entry_win.PlaceHolder = Nothing
    Me.uc_entry_win.ShortcutsEnabled = True
    Me.uc_entry_win.Size = New System.Drawing.Size(200, 24)
    Me.uc_entry_win.SufixText = "Sufix Text"
    Me.uc_entry_win.SufixTextVisible = True
    Me.uc_entry_win.TabIndex = 1
    Me.uc_entry_win.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.uc_entry_win.TextValue = ""
    Me.uc_entry_win.Value = ""
    Me.uc_entry_win.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_entry_drop
    '
    Me.uc_entry_drop.DoubleValue = 0.0R
    Me.uc_entry_drop.Enabled = False
    Me.uc_entry_drop.IntegerValue = 0
    Me.uc_entry_drop.IsReadOnly = False
    Me.uc_entry_drop.Location = New System.Drawing.Point(6, 20)
    Me.uc_entry_drop.Name = "uc_entry_drop"
    Me.uc_entry_drop.PlaceHolder = Nothing
    Me.uc_entry_drop.ShortcutsEnabled = True
    Me.uc_entry_drop.Size = New System.Drawing.Size(200, 24)
    Me.uc_entry_drop.SufixText = "Sufix Text"
    Me.uc_entry_drop.SufixTextVisible = True
    Me.uc_entry_drop.TabIndex = 0
    Me.uc_entry_drop.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.uc_entry_drop.TextValue = ""
    Me.uc_entry_drop.Value = ""
    Me.uc_entry_drop.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_multicurrency
    '
    Me.uc_multicurrency.GroupBoxText = Nothing
    Me.uc_multicurrency.HeightControl = 0
    Me.uc_multicurrency.Location = New System.Drawing.Point(6, 81)
    Me.uc_multicurrency.Name = "uc_multicurrency"
    Me.uc_multicurrency.OpenModeControl = GUI_Controls.uc_multi_currency_site_sel.OPEN_MODE.OnlyCurrency
    Me.uc_multicurrency.SelectedCurrency = ""
    Me.uc_multicurrency.SelectedOption = GUI_Controls.uc_multi_currency_site_sel.MULTICURRENCY_OPTION.TotalToIsoCode
    Me.uc_multicurrency.Size = New System.Drawing.Size(254, 77)
    Me.uc_multicurrency.TabIndex = 14
    Me.uc_multicurrency.WidthControl = 0
    '
    'frm_table_players_report
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1222, 614)
    Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
    Me.Name = "frm_table_players_report"
    Me.Text = "frm_table_players_report"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_report.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector
  Friend WithEvents uc_checked_list_table_type As GUI_Controls.uc_checked_list
  Friend WithEvents uc_account_select As GUI_Controls.uc_account_sel
  Friend WithEvents gb_report As System.Windows.Forms.GroupBox
  Friend WithEvents uc_entry_drop As GUI_Controls.uc_entry_field
  Friend WithEvents uc_entry_netwin As GUI_Controls.uc_entry_field
  Friend WithEvents uc_entry_chips_in As GUI_Controls.uc_entry_field
  Friend WithEvents uc_entry_buy_in As GUI_Controls.uc_entry_field
  Friend WithEvents uc_entry_win As GUI_Controls.uc_entry_field
  Friend WithEvents uc_multicurrency As GUI_Controls.uc_multi_currency_site_sel
End Class
