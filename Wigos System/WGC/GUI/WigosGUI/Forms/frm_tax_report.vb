'-------------------------------------------------------------------
' Copyright � 2011 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_tax_report
' DESCRIPTION:   Tax report
' AUTHOR:        Jaume Barn�s
' CREATION DATE: 26-NOV-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------   
' 26-NOV-2014  JBC    Initial version
' 26-NOV-2014  JBC    Fixed Bug WIG-1772: SQL 2005 compliant
' 28-NOV-2014  JPJ    Fixed Bug WIG-1776: Incorrect date format when printing the report
' 02-DEC-2014  JPJ    Fixed Bug WIG-1785: The columns regarding Card and CompanyB are swapped
' 02-DEC-2014  JPJ    Fixed Bug WIG-1799: Filter cashier movement types
' 02-DEC-2014  JPJ    Fixed Bug WIG-1794: Changed query type to execute a stored procedure Cashier_Movements_Per_Workingday
' 03-DEC-2014  JPJ    Fixed Bug WIG-1797: Default options
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common


Public Class frm_tax_report
  Inherits frm_base_sel

#Region " Constants "

  Private Const GRID_TOTAL_COLUMNS As Integer = 11
  Private Const GRID_HEADER_ROWS As Integer = 2

  ' DB Columns
  Private Const SQL_COLUMN_DATETIME As Integer = 0
  Private Const SQL_COLUMN_TOTAL_AMOUNT As Integer = 1
  Private Const SQL_COLUMN_TOTAL_TAX As Integer = 2
  Private Const SQL_COLUMN_TOTAL_TOTAL As Integer = 3

  Private Const SQL_COLUMN_CONCEPT_1_AMOUNT As Integer = 4
  Private Const SQL_COLUMN_CONCEPT_1_TAX As Integer = 5
  Private Const SQL_COLUMN_CONCEPT_1_TOTAL As Integer = 6

  Private Const SQL_COLUMN_CONCEPT_2_AMOUNT As Integer = 7
  Private Const SQL_COLUMN_CONCEPT_2_TAX As Integer = 8
  Private Const SQL_COLUMN_CONCEPT_2_TOTAL As Integer = 9

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0

  Private Const GRID_COLUMN_DATETIME As Integer = 1
  Private Const GRID_COLUMN_TOTAL_AMOUNT As Integer = 2
  Private Const GRID_COLUMN_TOTAL_TAX As Integer = 3
  Private Const GRID_COLUMN_TOTAL_TOTAL As Integer = 4

  Private Const GRID_COLUMN_CONCEPT_1_AMOUNT As Integer = 5
  Private Const GRID_COLUMN_CONCEPT_1_TAX As Integer = 6
  Private Const GRID_COLUMN_CONCEPT_1_TOTAL As Integer = 7

  Private Const GRID_COLUMN_CONCEPT_2_AMOUNT As Integer = 8
  Private Const GRID_COLUMN_CONCEPT_2_TAX As Integer = 9
  Private Const GRID_COLUMN_CONCEPT_2_TOTAL As Integer = 10

  Private Const TOTAL_AMOUNT As Integer = 0
  Private Const TOTAL_TAX As Integer = 1
  Private Const TOTAL_TOTAL As Integer = 2
  Private Const TOTAL_CONCEPT_1_AMOUNT As Integer = 3
  Private Const TOTAL_CONCEPT_1_TAX As Integer = 4
  Private Const TOTAL_CONCEPT_1_TOTAL As Integer = 5
  Private Const TOTAL_CONCEPT_2_AMOUNT As Integer = 6
  Private Const TOTAL_CONCEPT_2_TAX As Integer = 7
  Private Const TOTAL_CONCEPT_2_TOTAL As Integer = 8

  Private Const QUANTITY_OF_TOTAL_AMOUNTS As Byte = 9

#End Region ' Constants

#Region " Members "
  Private m_str_tax_name As String
  Private m_str_tax_b_name As String
  Private m_totals_list As List(Of Decimal)
  Private m_init_date_from As String
  Private m_init_date_to As String
  Private m_rows As String
#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_DAILY_TAX_SPLIT_B_SUMMARY

    Call MyBase.GUI_SetFormId()
  End Sub

  ' PURPOSE : Checks out the DB row before adding it to the grid
  '              and process counters (& totals rows)
  '
  '  PARAMS :
  '     - INPUT :
  '           - DataRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the data row can be added) or False (the data row can not be added)
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    ' If user wants to see all the rows there's no need to check anything else
    If chk_rows.Checked = False Then

      Return True
    End If

    'Checking empty rows
    If DbRow.Value(SQL_COLUMN_TOTAL_AMOUNT) = 0 AndAlso DbRow.Value(SQL_COLUMN_TOTAL_TAX) = 0 AndAlso _
       DbRow.Value(SQL_COLUMN_TOTAL_TOTAL) = 0 AndAlso DbRow.Value(SQL_COLUMN_CONCEPT_1_AMOUNT) = 0 AndAlso _
       DbRow.Value(SQL_COLUMN_CONCEPT_1_TAX) = 0 AndAlso DbRow.Value(SQL_COLUMN_CONCEPT_1_TOTAL) = 0 AndAlso _
       DbRow.Value(SQL_COLUMN_CONCEPT_2_AMOUNT) = 0 AndAlso DbRow.Value(SQL_COLUMN_CONCEPT_2_TAX) = 0 AndAlso _
       DbRow.Value(SQL_COLUMN_CONCEPT_2_TOTAL) = 0 Then

      Return False
    Else

      Return True
    End If


  End Function

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(3)

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_INFO).Visible = False

    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False

    ' Date time filter
    Me.uc_date.Init(GLB_NLS_GUI_INVOICING.Id(201), True)

    'GP
    Me.m_str_tax_name = GeneralParam.GetString("Cashier", "Split.B.Tax.Name")
    Me.m_str_tax_b_name = GeneralParam.GetString("Cashier", "Split.B.Name")

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5764, m_str_tax_name)

    ' Set filter default values
    Call SetDefaultValues()

    ' Grid
    Call GUI_StyleSheet()

    ' Checkbox rows
    Me.chk_rows.Text = GLB_NLS_GUI_STATISTICS.GetString(370)

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_FilterReset()
    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_FilterReset

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()
    Dim _idx_totals As Byte

    m_totals_list = New List(Of Decimal)

    For _idx_totals = 1 To QUANTITY_OF_TOTAL_AMOUNTS
      m_totals_list.Add(0)
    Next

  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    Dim _idx_row As Integer

    Me.Grid.AddRow()
    _idx_row = Me.Grid.NumRows - 1

    Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

    Me.Grid.Cell(_idx_row, GRID_COLUMN_TOTAL_AMOUNT).Value = GUI_FormatCurrency(m_totals_list(TOTAL_AMOUNT))
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TOTAL_TAX).Value = GUI_FormatCurrency(m_totals_list(TOTAL_TAX))
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TOTAL_TOTAL).Value = GUI_FormatCurrency(m_totals_list(TOTAL_TOTAL))
    Me.Grid.Cell(_idx_row, GRID_COLUMN_CONCEPT_1_AMOUNT).Value = GUI_FormatCurrency(m_totals_list(TOTAL_CONCEPT_1_AMOUNT))
    Me.Grid.Cell(_idx_row, GRID_COLUMN_CONCEPT_1_TAX).Value = GUI_FormatCurrency(m_totals_list(TOTAL_CONCEPT_1_TAX))
    Me.Grid.Cell(_idx_row, GRID_COLUMN_CONCEPT_1_TOTAL).Value = GUI_FormatCurrency(m_totals_list(TOTAL_CONCEPT_1_TOTAL))
    Me.Grid.Cell(_idx_row, GRID_COLUMN_CONCEPT_2_AMOUNT).Value = GUI_FormatCurrency(m_totals_list(TOTAL_CONCEPT_2_AMOUNT))
    Me.Grid.Cell(_idx_row, GRID_COLUMN_CONCEPT_2_TAX).Value = GUI_FormatCurrency(m_totals_list(TOTAL_CONCEPT_2_TAX))
    Me.Grid.Cell(_idx_row, GRID_COLUMN_CONCEPT_2_TOTAL).Value = GUI_FormatCurrency(m_totals_list(TOTAL_CONCEPT_2_TOTAL))

  End Sub ' GUI_AfterLastRow

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.uc_date.ToDateSelected Then
      If Me.uc_date.FromDate > Me.uc_date.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_date.Focus()

        Return False
      End If
    End If

    Return True
  End Function 'GUI_FilterCheck

  ' PURPOSE: Build an SQL Command query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL Command query ready to send to the database
  Protected Overrides Function GUI_GetSqlCommand() As SqlCommand
    Dim _sql_cmd As SqlCommand

    _sql_cmd = New SqlCommand("Tax_Report_Per_Day")
    _sql_cmd.CommandType = CommandType.StoredProcedure
    _sql_cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = uc_date.FromDate.AddHours(uc_date.ClosingTime)

    If Me.uc_date.ToDateSelected Then
      _sql_cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = uc_date.ToDate.AddHours(uc_date.ClosingTime)
    Else
      _sql_cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = DBNull.Value
    End If

    Return _sql_cmd

  End Function

  ' PURPOSE: Get the defined Query Type
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_QUERY_TYPE
  '
  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE

    Return ENUM_QUERY_TYPE.QUERY_COMMAND
  End Function ' GUI_GetQueryType

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_AUDITOR.GetString(257), m_init_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_AUDITOR.GetString(258), m_init_date_to)
    PrintData.SetFilter(chk_rows.Text, m_rows)

  End Sub

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportUpdateFilters()
    m_rows = ""
    m_init_date_from = ""
    m_init_date_to = ""

    If chk_rows.Checked = True Then
      m_rows = GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_rows = GLB_NLS_GUI_INVOICING.GetString(480)
    End If

    ' Init Date From
    m_init_date_from = GUI_FormatDate(Me.uc_date.FromDate.AddHours(Me.uc_date.ClosingTime), _
                                      ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    ' Init Date To
    If Me.uc_date.ToDateSelected Then
      m_init_date_to = GUI_FormatDate(Me.uc_date.ToDate.AddHours(Me.uc_date.ClosingTime), _
                                      ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

  End Sub

  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(ExcelData)

    ExcelData.SetColumnFormat(0, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.DATE)

  End Sub

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Call CalculateTotal(DbRow)

    ' Jornada
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_DATETIME)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATETIME), _
                                                                                      ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                      ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DATETIME).Value = String.Empty
    End If

    ' Importe Base
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_TOTAL_AMOUNT)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TOTAL_AMOUNT))
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_AMOUNT).Value = String.Empty
    End If

    ' Importe Base
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_TOTAL_TAX)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_TAX).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TOTAL_TAX))
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_TAX).Value = String.Empty
    End If

    ' Jornada
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_TOTAL_TOTAL)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_TOTAL).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TOTAL_TOTAL))
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_TOTAL).Value = String.Empty
    End If

    ' Importe Base
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_CONCEPT_1_AMOUNT)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CONCEPT_1_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_CONCEPT_1_AMOUNT))
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CONCEPT_1_AMOUNT).Value = String.Empty
    End If

    ' Jornada
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_CONCEPT_1_TAX)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CONCEPT_1_TAX).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_CONCEPT_1_TAX))
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CONCEPT_1_TAX).Value = String.Empty
    End If

    ' Importe Base
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_CONCEPT_1_TOTAL)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CONCEPT_1_TOTAL).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_CONCEPT_1_TOTAL))
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CONCEPT_1_TOTAL).Value = String.Empty
    End If

    ' Jornada
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_CONCEPT_2_AMOUNT)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CONCEPT_2_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_CONCEPT_2_AMOUNT))
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CONCEPT_2_AMOUNT).Value = String.Empty
    End If

    ' Importe Base
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_CONCEPT_2_TAX)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CONCEPT_2_TAX).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_CONCEPT_2_TAX))
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CONCEPT_2_TAX).Value = String.Empty
    End If

    ' Importe Base
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_CONCEPT_2_TOTAL)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CONCEPT_2_TOTAL).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_CONCEPT_2_TOTAL))
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CONCEPT_2_TOTAL).Value = String.Empty
    End If

    Return True
  End Function ' GUI_SetupRow

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()

    With Me.Grid

      Call .Init(GRID_TOTAL_COLUMNS, GRID_HEADER_ROWS)

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Header(1).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = 150
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' ""  
      .Column(GRID_COLUMN_DATETIME).Header(0).Text = ""
      ' Jornada
      .Column(GRID_COLUMN_DATETIME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5723) 'Jornada
      .Column(GRID_COLUMN_DATETIME).Width = 1400 'GRID_WIDTH_ACCOUNT_ID
      .Column(GRID_COLUMN_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Importe
      .Column(GRID_COLUMN_TOTAL_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2838)
      ' Base
      .Column(GRID_COLUMN_TOTAL_AMOUNT).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(354)
      .Column(GRID_COLUMN_TOTAL_AMOUNT).Width = 1100 'GRID_WIDTH_ACCOUNT_ID
      .Column(GRID_COLUMN_TOTAL_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Importe
      .Column(GRID_COLUMN_TOTAL_TAX).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2838)
      ' IVA
      .Column(GRID_COLUMN_TOTAL_TAX).Header(1).Text = m_str_tax_name
      .Column(GRID_COLUMN_TOTAL_TAX).Width = 1100 'GRID_WIDTH_ACCOUNT_ID
      .Column(GRID_COLUMN_TOTAL_TAX).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Importe
      .Column(GRID_COLUMN_TOTAL_TOTAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2838)
      ' Total
      .Column(GRID_COLUMN_TOTAL_TOTAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2838)
      .Column(GRID_COLUMN_TOTAL_TOTAL).Width = 1100 'GRID_WIDTH_ACCOUNT_ID
      .Column(GRID_COLUMN_TOTAL_TOTAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Tarjeta
      .Column(GRID_COLUMN_CONCEPT_1_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5767)
      ' Base
      .Column(GRID_COLUMN_CONCEPT_1_AMOUNT).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(354)
      .Column(GRID_COLUMN_CONCEPT_1_AMOUNT).Width = 1100 'GRID_WIDTH_ACCOUNT_ID
      .Column(GRID_COLUMN_CONCEPT_1_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Tarjeta
      .Column(GRID_COLUMN_CONCEPT_1_TAX).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5767)
      ' IVA
      .Column(GRID_COLUMN_CONCEPT_1_TAX).Header(1).Text = m_str_tax_name
      .Column(GRID_COLUMN_CONCEPT_1_TAX).Width = 1100 'GRID_WIDTH_ACCOUNT_ID
      .Column(GRID_COLUMN_CONCEPT_1_TAX).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Tarjeta
      .Column(GRID_COLUMN_CONCEPT_1_TOTAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5767)
      ' Total
      .Column(GRID_COLUMN_CONCEPT_1_TOTAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2838)
      .Column(GRID_COLUMN_CONCEPT_1_TOTAL).Width = 1100 'GRID_WIDTH_ACCOUNT_ID
      .Column(GRID_COLUMN_CONCEPT_1_TOTAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Concepto B
      .Column(GRID_COLUMN_CONCEPT_2_AMOUNT).Header(0).Text = m_str_tax_b_name
      ' Base
      .Column(GRID_COLUMN_CONCEPT_2_AMOUNT).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(354)
      .Column(GRID_COLUMN_CONCEPT_2_AMOUNT).Width = 1100 'GRID_WIDTH_ACCOUNT_ID
      .Column(GRID_COLUMN_CONCEPT_2_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Concepto B
      .Column(GRID_COLUMN_CONCEPT_2_TAX).Header(0).Text = m_str_tax_b_name
      ' IVA
      .Column(GRID_COLUMN_CONCEPT_2_TAX).Header(1).Text = m_str_tax_name
      .Column(GRID_COLUMN_CONCEPT_2_TAX).Width = 1100 'GRID_WIDTH_ACCOUNT_ID
      .Column(GRID_COLUMN_CONCEPT_2_TAX).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Concepto B
      .Column(GRID_COLUMN_CONCEPT_2_TOTAL).Header(0).Text = m_str_tax_b_name
      ' Total
      .Column(GRID_COLUMN_CONCEPT_2_TOTAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2838)
      .Column(GRID_COLUMN_CONCEPT_2_TOTAL).Width = 1100 'GRID_WIDTH_ACCOUNT_ID
      .Column(GRID_COLUMN_CONCEPT_2_TOTAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

  End Sub 'GUI_StyleSheet

#Region " GUI Reports "

#End Region ' GUI Reports

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()
    Dim _closing_time As Integer
    Dim _from_date As DateTime

    _closing_time = GetDefaultClosingTime()
    Me.uc_date.ClosingTime = _closing_time

    _from_date = New DateTime(Now.Year, Now.Month, 1)
    Me.uc_date.FromDate = _from_date.AddMonths(-1)
    Me.uc_date.ToDate = Me.uc_date.FromDate.AddMonths(1)
    Me.uc_date.ToDateSelected = True
    Me.chk_rows.Checked = False

  End Sub ' SetDefaultValues

  Private Sub CalculateTotal(ByVal DbRow As CLASS_DB_ROW)

    m_totals_list(TOTAL_AMOUNT) += DbRow.Value(SQL_COLUMN_TOTAL_AMOUNT)
    m_totals_list(TOTAL_TAX) += DbRow.Value(SQL_COLUMN_TOTAL_TAX)
    m_totals_list(TOTAL_TOTAL) += DbRow.Value(SQL_COLUMN_TOTAL_TOTAL)
    m_totals_list(TOTAL_CONCEPT_1_AMOUNT) += DbRow.Value(SQL_COLUMN_CONCEPT_1_AMOUNT)
    m_totals_list(TOTAL_CONCEPT_1_TAX) += DbRow.Value(SQL_COLUMN_CONCEPT_1_TAX)
    m_totals_list(TOTAL_CONCEPT_1_TOTAL) += DbRow.Value(SQL_COLUMN_CONCEPT_1_TOTAL)
    m_totals_list(TOTAL_CONCEPT_2_AMOUNT) += DbRow.Value(SQL_COLUMN_CONCEPT_2_AMOUNT)
    m_totals_list(TOTAL_CONCEPT_2_TAX) += DbRow.Value(SQL_COLUMN_CONCEPT_2_TAX)
    m_totals_list(TOTAL_CONCEPT_2_TOTAL) += DbRow.Value(SQL_COLUMN_CONCEPT_2_TOTAL)

  End Sub ' CalculateTotal

#End Region ' Private Functions

End Class