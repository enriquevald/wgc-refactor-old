'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_currency_exchange_audit.vb
' DESCRIPTION:   Interface for filter currency exchange audit information
' AUTHOR:        David Lasdiez
' CREATION DATE: 17-06-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 17-JUN-2013  DLL    Initial version
' 30-JUL-2013  JBC    Limited functions
' 10-APR-2014  DLL    Added two columns for old and new Decimals
' 07-MAY-2014  DRV    Fixed Bug WIG-901: changed the row order
' 15-OCT-2014  JPJ    Fixed Bug WIG-1501: The number of decimals is set to 2 (old and new exchange rate) when loading the report.
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
#Region " Imports "
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common
#End Region ' Imports


Public Class frm_currency_exchange_audit
  Inherits GUI_Controls.frm_base_sel

#Region "Constants"
  Private Const GRID_COLUMNS As Integer = 8
  Private Const GRID_HEADER_ROWS As Integer = 2

  ' DB Columns
  Private Const SQL_COLUMN_AUDIT_ID As Integer = 0
  Private Const SQL_COLUMN_TYPE As Integer = 1
  Private Const SQL_COLUMN_CURRENCY_ISO_CODE As Integer = 2
  Private Const SQL_COLUMN_CURRENCY_DESCRIPTION As Integer = 3
  Private Const SQL_COLUMN_DATETIME As Integer = 4
  Private Const SQL_COLUMN_OLD_CHANGE As Integer = 5
  Private Const SQL_COLUMN_NEW_CHANGE As Integer = 6
  Private Const SQL_COLUMN_OLD_DECIMALS As Integer = 7
  Private Const SQL_COLUMN_NEW_DECIMALS As Integer = 8

  'Grid Columns
  Private Const GRID_COLUMN_AUDIT_ID As Integer = 0
  Private Const GRID_COLUMN_CURRENCY_ISO_CODE As Integer = 1
  Private Const GRID_COLUMN_CURRENCY_DESCRIPTION As Integer = 2
  Private Const GRID_COLUMN_DATETIME As Integer = 3
  Private Const GRID_COLUMN_OLD_CHANGE As Integer = 4
  Private Const GRID_COLUMN_NEW_CHANGE As Integer = 5
  Private Const GRID_COLUMN_OLD_DECIMALS As Integer = 6
  Private Const GRID_COLUMN_NEW_DECIMALS As Integer = 7

  'Excel Columns
  Private Const EXCEL_COLUMN_OLD_CHANGE As Integer = 3
  Private Const EXCEL_COLUMN_NEW_CHANGE As Integer = 4

  ' DB Currencies Columns
  Private Const SQL_CURR_ISO_CODE As Integer = 0
  Private Const SQL_CURR_NAME As Integer = 1

  'Grid Currencies Columns
  Private Const GRID_CURR_ISO_CODE As Integer = 0
  Private Const GRID_CURR_CHECKED As Integer = 1
  Private Const GRID_CURR_NAME As Integer = 2

  Private Const GRID_CURR_COLUMNS As Integer = 3
  Private Const GRID_CURR_HEADER_ROWS As Integer = 0
  Private Const GRID_CURR_ISO_CODE_WIDTH As Integer = 2800

  Private Const GRID_COLUMN_AUDIT_ID_WIDTH = 200
  Private Const GRID_COLUMN_CURRENCY_ISO_CODE_WIDTH As Integer = 1500
  Private Const GRID_COLUMN_CURRENCY_DESCRIPTION_WIDTH As Integer = 3000
  Private Const GRID_COLUMN_DATETIME_WIDTH As Integer = 2000
  Private Const GRID_COLUMN_OLD_CHANGE_WIDTH As Integer = 1500
  Private Const GRID_COLUMN_NEW_CHANGE_WIDTH As Integer = 1500

  Private Const FORM_DB_MIN_VERSION As Short = 201

  Private Const NUMBER_OF_DECIMALS As Integer = 8

#End Region

#Region " Members "
  Private m_date_from As String
  Private m_date_to As String
#End Region

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CURRENCY_EXCHANGE_AUDIT

    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' - Form title
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2082)

    'Frame Date
    Me.fra_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(830)

    ' - Date
    Me.dt_date_from.Text = GLB_NLS_GUI_AUDITOR.GetString(257)
    Me.dt_date_to.Text = GLB_NLS_GUI_AUDITOR.GetString(258)
    Me.dt_date_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dt_date_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    ' - Buttons
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_CONTROLS.GetString(6)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False

    Call Me.Grid.Clear()

    ' Style main Grid
    Call Me.GUI_StyleSheet()

    'Default values
    Call Me.SetDefaultValues()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_FilterReset()

    Call Me.SetDefaultValues()

  End Sub ' GUI_FilterReset

  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE

    Return ENUM_QUERY_TYPE.QUERY_COMMAND

  End Function ' GUI_GetQueryType

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_GetSqlCommand() As SqlCommand

    Dim _sql_cmd As SqlCommand
    Dim _str_sql As StringBuilder

    _str_sql = New StringBuilder()
    _str_sql.AppendLine("  SELECT   CEA_AUDIT_ID ")
    _str_sql.AppendLine("         , CEA_TYPE     ")
    _str_sql.AppendLine("         , CEA_CURRENCY_ISO_CODE   ")
    _str_sql.AppendLine("         , ISNULL(CE_DESCRIPTION,'') AS CE_DESCRIPTION ")
    _str_sql.AppendLine("         , CEA_DATETIME   ")
    _str_sql.AppendLine("         , CEA_OLD_CHANGE ")
    _str_sql.AppendLine("         , CEA_NEW_CHANGE ")
    _str_sql.AppendLine("         , CEA_OLD_NUM_DECIMALS ")
    _str_sql.AppendLine("         , CEA_NEW_NUM_DECIMALS ")
    _str_sql.AppendLine("    FROM   CURRENCY_EXCHANGE_AUDIT ")
    _str_sql.AppendLine("         , CURRENCY_EXCHANGE  ")
    _str_sql.AppendLine("   WHERE   CEA_CURRENCY_ISO_CODE = CE_CURRENCY_ISO_CODE ")
    _str_sql.AppendLine("     AND   CEA_TYPE = CE_TYPE  ")

    _str_sql.Append(GetSqlWhere())
    _str_sql.AppendLine("ORDER BY   CEA_DATETIME DESC  ")

    _sql_cmd = New SqlCommand(_str_sql.ToString())

    Return _sql_cmd

  End Function ' GUI_GetSqlCommand

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_FILTER_APPLY
        Me.Grid.Update()
        Call MyBase.GUI_ButtonClick(ButtonId)
      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Date verification
    If Me.dt_date_to.Checked = True Then
      If Me.dt_date_from.Value > Me.dt_date_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dt_date_to.Focus()
        Return False
      End If
    End If

    Return True

  End Function ' GUI_FilterCheck

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Call MyBase.GUI_SetupRow(RowIndex, DbRow)


    ' ISO code
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CURRENCY_ISO_CODE).Value = DbRow.Value(SQL_COLUMN_CURRENCY_ISO_CODE)

    'ISO description
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CURRENCY_DESCRIPTION).Value = CurrencyExchange.GetDescription(DbRow.Value(SQL_COLUMN_TYPE), DbRow.Value(SQL_COLUMN_CURRENCY_ISO_CODE), DbRow.Value(SQL_COLUMN_CURRENCY_DESCRIPTION))

    ' Date
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATETIME), _
                  ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    ' Old Change
    Me.Grid.Cell(RowIndex, GRID_COLUMN_OLD_CHANGE).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_OLD_CHANGE), NUMBER_OF_DECIMALS)

    ' New Change
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NEW_CHANGE).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_NEW_CHANGE), NUMBER_OF_DECIMALS)

    ' Old Deciamls
    Me.Grid.Cell(RowIndex, GRID_COLUMN_OLD_DECIMALS).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_OLD_DECIMALS), 2)

    ' New Deciamls
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NEW_DECIMALS).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_NEW_DECIMALS), 2)

    Return True

  End Function ' GUI_SetupRow

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_AUDITOR.GetString(257), Me.m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), Me.m_date_to)

    PrintData.FilterValueWidth(1) = 3000

  End Sub ' GUI_ReportFilter


  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)

    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - ExcelData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(ExcelData)

    ' 15-OCT-2014 JPJ Fixed Bug WIG-1501: The number of decimals is set to 2 (old and new exchange rate) when loading the report.
    ExcelData.SetColumnFormat(EXCEL_COLUMN_OLD_CHANGE, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.NUMERIC, NUMBER_OF_DECIMALS) 'Old Change
    ExcelData.SetColumnFormat(EXCEL_COLUMN_NEW_CHANGE, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.NUMERIC, NUMBER_OF_DECIMALS) 'New Change

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportUpdateFilters()

    Me.m_date_from = ""
    Me.m_date_to = ""

    'Date 
    Me.m_date_from = GUI_FormatDate(Me.dt_date_from.Value, _
                     ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    If Me.dt_date_to.Checked Then
      Me.m_date_to = GUI_FormatDate(Me.dt_date_to.Value, _
                     ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

  End Sub 'GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region

#Region " Private Functions "

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, False)
      .Sortable = True
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' AUDIT_ID
      .Column(GRID_COLUMN_AUDIT_ID).Header(0).Text = " "
      .Column(GRID_COLUMN_AUDIT_ID).IsColumnSortable = False
      .Column(GRID_COLUMN_AUDIT_ID).Header(1).Text = " "
      .Column(GRID_COLUMN_AUDIT_ID).Width = GRID_COLUMN_AUDIT_ID_WIDTH
      .Column(GRID_COLUMN_AUDIT_ID).HighLightWhenSelected = False
      .Column(GRID_COLUMN_AUDIT_ID).IsColumnPrintable = False



      ' CURRENCY_ISO_CODE
      .Column(GRID_COLUMN_CURRENCY_ISO_CODE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2404)
      .Column(GRID_COLUMN_CURRENCY_ISO_CODE).Header(1).Text = GLB_NLS_GUI_AUDITOR.GetString(264)
      .Column(GRID_COLUMN_CURRENCY_ISO_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_CURRENCY_ISO_CODE).Width = GRID_COLUMN_CURRENCY_ISO_CODE_WIDTH

      ' CURRENCY DESCRIPTION
      .Column(GRID_COLUMN_CURRENCY_DESCRIPTION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2404)
      .Column(GRID_COLUMN_CURRENCY_DESCRIPTION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2142)
      .Column(GRID_COLUMN_CURRENCY_DESCRIPTION).Width = GRID_COLUMN_CURRENCY_DESCRIPTION_WIDTH
      .Column(GRID_COLUMN_CURRENCY_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' DATETIME
      .Column(GRID_COLUMN_DATETIME).Header(0).Text = " "
      .Column(GRID_COLUMN_DATETIME).Header(1).Text = GLB_NLS_GUI_AUDITOR.GetString(261)
      .Column(GRID_COLUMN_DATETIME).Width = GRID_COLUMN_DATETIME_WIDTH
      .Column(GRID_COLUMN_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' OLD CHANGE
      .Column(GRID_COLUMN_OLD_CHANGE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2434)
      .Column(GRID_COLUMN_OLD_CHANGE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2084)
      .Column(GRID_COLUMN_OLD_CHANGE).Width = GRID_COLUMN_OLD_CHANGE_WIDTH
      .Column(GRID_COLUMN_OLD_CHANGE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' NEW CHANGE
      .Column(GRID_COLUMN_NEW_CHANGE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2434)
      .Column(GRID_COLUMN_NEW_CHANGE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2085)
      .Column(GRID_COLUMN_NEW_CHANGE).Width = GRID_COLUMN_NEW_CHANGE_WIDTH
      .Column(GRID_COLUMN_NEW_CHANGE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' OLD CHANGE
      .Column(GRID_COLUMN_OLD_DECIMALS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2145)
      .Column(GRID_COLUMN_OLD_DECIMALS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2084)
      .Column(GRID_COLUMN_OLD_DECIMALS).Width = 1000
      .Column(GRID_COLUMN_OLD_DECIMALS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' NEW CHANGE
      .Column(GRID_COLUMN_NEW_DECIMALS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2145)
      .Column(GRID_COLUMN_NEW_DECIMALS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2085)
      .Column(GRID_COLUMN_NEW_DECIMALS).Width = 1000
      .Column(GRID_COLUMN_NEW_DECIMALS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Get Sql WHERE to build SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String

    Dim _str_where As String = ""
    Dim _currency_list As String = ""

    ' Filter Dates
    _str_where = " AND CEA_DATETIME >= " & GUI_FormatDateDB(Me.dt_date_from.Value) & " "

    If Me.dt_date_to.Checked = True Then
      _str_where = _str_where & " AND CEA_DATETIME <= " & GUI_FormatDateDB(Me.dt_date_to.Value) & " "
    End If

    ' Type CURRENCY
    _str_where = _str_where & " AND ( CEA_TYPE = " & CurrencyExchangeType.CURRENCY & " )"

    Return _str_where

  End Function ' GetSqlWhere

  ' PURPOSE: Put values to default
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()
    Dim _today As DateTime

    _today = WSI.Common.Misc.TodayOpening()

    'Making a Date YEAR-MONTH-01
    Me.dt_date_from.Value = New DateTime(_today.Year, _today.Month, 1, _today.Hour, 0, 0)
    Me.dt_date_to.Value = _today
    Me.dt_date_to.Checked = False

  End Sub 'SetDefaultValues

#End Region

#Region " Public Functions "

  ' PURPOSE : Opens dialog with default settings for edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '           - MdiParent
  '     - OUTPUT :
  '
  ' RETURNS :

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region 'Public Functions

#Region " Events"

#End Region

End Class