﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_report_profit_day_and_month
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_report_profit_day_and_month))
    Me.gb_init_date = New System.Windows.Forms.GroupBox()
    Me.lbl_to_hour = New System.Windows.Forms.Label()
    Me.lbl_from_hour = New System.Windows.Forms.Label()
    Me.dtp_init_to = New GUI_Controls.uc_date_picker()
    Me.dtp_init_from = New GUI_Controls.uc_date_picker()
    Me.uc_site_select = New GUI_Controls.uc_sites_sel()
    Me.ef_win_increment = New GUI_Controls.uc_entry_field()
    Me.chk_only_closed = New System.Windows.Forms.CheckBox()
    Me.pnl_filters = New System.Windows.Forms.Panel()
    Me.lbl_profits = New System.Windows.Forms.Label()
    Me.lbl_payouts = New System.Windows.Forms.Label()
    Me.lbl_payments = New System.Windows.Forms.Label()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_init_date.SuspendLayout()
    Me.pnl_filters.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.pnl_filters)
    Me.panel_filter.Controls.Add(Me.uc_site_select)
    Me.panel_filter.Size = New System.Drawing.Size(1027, 218)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_site_select, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.pnl_filters, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 222)
    Me.panel_data.Size = New System.Drawing.Size(1027, 344)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1021, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1021, 4)
    '
    'gb_init_date
    '
    Me.gb_init_date.Controls.Add(Me.lbl_to_hour)
    Me.gb_init_date.Controls.Add(Me.lbl_from_hour)
    Me.gb_init_date.Controls.Add(Me.dtp_init_to)
    Me.gb_init_date.Controls.Add(Me.dtp_init_from)
    Me.gb_init_date.Location = New System.Drawing.Point(7, 1)
    Me.gb_init_date.Name = "gb_init_date"
    Me.gb_init_date.Size = New System.Drawing.Size(240, 91)
    Me.gb_init_date.TabIndex = 0
    Me.gb_init_date.TabStop = False
    Me.gb_init_date.Text = "xDate"
    '
    'lbl_to_hour
    '
    Me.lbl_to_hour.AutoSize = True
    Me.lbl_to_hour.ForeColor = System.Drawing.Color.Blue
    Me.lbl_to_hour.Location = New System.Drawing.Point(180, 54)
    Me.lbl_to_hour.Name = "lbl_to_hour"
    Me.lbl_to_hour.Size = New System.Drawing.Size(40, 13)
    Me.lbl_to_hour.TabIndex = 15
    Me.lbl_to_hour.Text = "00:00"
    '
    'lbl_from_hour
    '
    Me.lbl_from_hour.AutoSize = True
    Me.lbl_from_hour.ForeColor = System.Drawing.Color.Blue
    Me.lbl_from_hour.Location = New System.Drawing.Point(180, 26)
    Me.lbl_from_hour.Name = "lbl_from_hour"
    Me.lbl_from_hour.Size = New System.Drawing.Size(40, 13)
    Me.lbl_from_hour.TabIndex = 14
    Me.lbl_from_hour.Text = "00:00"
    '
    'dtp_init_to
    '
    Me.dtp_init_to.Checked = True
    Me.dtp_init_to.IsReadOnly = False
    Me.dtp_init_to.Location = New System.Drawing.Point(6, 51)
    Me.dtp_init_to.Name = "dtp_init_to"
    Me.dtp_init_to.ShowCheckBox = False
    Me.dtp_init_to.ShowUpDown = False
    Me.dtp_init_to.Size = New System.Drawing.Size(162, 24)
    Me.dtp_init_to.SufixText = "Sufix Text"
    Me.dtp_init_to.SufixTextVisible = True
    Me.dtp_init_to.TabIndex = 1
    Me.dtp_init_to.TextWidth = 50
    Me.dtp_init_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_init_from
    '
    Me.dtp_init_from.Checked = True
    Me.dtp_init_from.IsReadOnly = False
    Me.dtp_init_from.Location = New System.Drawing.Point(6, 20)
    Me.dtp_init_from.Name = "dtp_init_from"
    Me.dtp_init_from.ShowCheckBox = False
    Me.dtp_init_from.ShowUpDown = False
    Me.dtp_init_from.Size = New System.Drawing.Size(162, 24)
    Me.dtp_init_from.SufixText = "Sufix Text"
    Me.dtp_init_from.SufixTextVisible = True
    Me.dtp_init_from.TabIndex = 0
    Me.dtp_init_from.TextWidth = 50
    Me.dtp_init_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'uc_site_select
    '
    Me.uc_site_select.Location = New System.Drawing.Point(17, 12)
    Me.uc_site_select.MultiSelect = True
    Me.uc_site_select.Name = "uc_site_select"
    Me.uc_site_select.ShowIsoCode = False
    Me.uc_site_select.ShowMultisiteRow = True
    Me.uc_site_select.Size = New System.Drawing.Size(287, 168)
    Me.uc_site_select.TabIndex = 0
    Me.uc_site_select.Visible = False
    '
    'ef_win_increment
    '
    Me.ef_win_increment.DoubleValue = 0.0R
    Me.ef_win_increment.IntegerValue = 0
    Me.ef_win_increment.IsReadOnly = False
    Me.ef_win_increment.Location = New System.Drawing.Point(254, 21)
    Me.ef_win_increment.Name = "ef_win_increment"
    Me.ef_win_increment.PlaceHolder = Nothing
    Me.ef_win_increment.ShortcutsEnabled = True
    Me.ef_win_increment.Size = New System.Drawing.Size(218, 24)
    Me.ef_win_increment.SufixText = "Sufix Text"
    Me.ef_win_increment.SufixTextVisible = True
    Me.ef_win_increment.TabIndex = 2
    Me.ef_win_increment.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_win_increment.TextValue = ""
    Me.ef_win_increment.TextWidth = 150
    Me.ef_win_increment.Value = ""
    Me.ef_win_increment.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_only_closed
    '
    Me.chk_only_closed.AutoSize = True
    Me.chk_only_closed.Location = New System.Drawing.Point(7, 98)
    Me.chk_only_closed.Name = "chk_only_closed"
    Me.chk_only_closed.Size = New System.Drawing.Size(98, 17)
    Me.chk_only_closed.TabIndex = 1
    Me.chk_only_closed.Text = "xOnlyClosed"
    Me.chk_only_closed.UseVisualStyleBackColor = True
    '
    'pnl_filters
    '
    Me.pnl_filters.Controls.Add(Me.lbl_profits)
    Me.pnl_filters.Controls.Add(Me.lbl_payouts)
    Me.pnl_filters.Controls.Add(Me.lbl_payments)
    Me.pnl_filters.Controls.Add(Me.chk_only_closed)
    Me.pnl_filters.Controls.Add(Me.gb_init_date)
    Me.pnl_filters.Controls.Add(Me.ef_win_increment)
    Me.pnl_filters.Location = New System.Drawing.Point(14, 15)
    Me.pnl_filters.Name = "pnl_filters"
    Me.pnl_filters.Size = New System.Drawing.Size(496, 197)
    Me.pnl_filters.TabIndex = 1
    '
    'lbl_profits
    '
    Me.lbl_profits.AutoSize = True
    Me.lbl_profits.ForeColor = System.Drawing.Color.Navy
    Me.lbl_profits.Location = New System.Drawing.Point(4, 154)
    Me.lbl_profits.Name = "lbl_profits"
    Me.lbl_profits.Size = New System.Drawing.Size(161, 13)
    Me.lbl_profits.TabIndex = 15
    Me.lbl_profits.Text = "xProfits = Bets - Payments"
    '
    'lbl_payouts
    '
    Me.lbl_payouts.AutoSize = True
    Me.lbl_payouts.ForeColor = System.Drawing.Color.Navy
    Me.lbl_payouts.Location = New System.Drawing.Point(4, 173)
    Me.lbl_payouts.Name = "lbl_payouts"
    Me.lbl_payouts.Size = New System.Drawing.Size(218, 13)
    Me.lbl_payouts.TabIndex = 14
    Me.lbl_payouts.Text = "xPayout = ( Payments / Bets ) * 100"
    '
    'lbl_payments
    '
    Me.lbl_payments.AutoSize = True
    Me.lbl_payments.ForeColor = System.Drawing.Color.Navy
    Me.lbl_payments.Location = New System.Drawing.Point(4, 135)
    Me.lbl_payments.Name = "lbl_payments"
    Me.lbl_payments.Size = New System.Drawing.Size(201, 13)
    Me.lbl_payments.TabIndex = 13
    Me.lbl_payments.Text = "xPayments = Coin out + Jackpots"
    '
    'frm_report_profit_day_and_month
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1035, 570)
    Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
    Me.Name = "frm_report_profit_day_and_month"
    Me.Text = "frm_report_profit_day_and_month"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_init_date.ResumeLayout(False)
    Me.gb_init_date.PerformLayout()
    Me.pnl_filters.ResumeLayout(False)
    Me.pnl_filters.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_init_date As System.Windows.Forms.GroupBox
  Protected WithEvents lbl_to_hour As System.Windows.Forms.Label
  Protected WithEvents lbl_from_hour As System.Windows.Forms.Label
  Friend WithEvents dtp_init_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_init_from As GUI_Controls.uc_date_picker
  Friend WithEvents uc_site_select As GUI_Controls.uc_sites_sel
  Friend WithEvents pnl_filters As System.Windows.Forms.Panel
  Friend WithEvents chk_only_closed As System.Windows.Forms.CheckBox
  Friend WithEvents ef_win_increment As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_profits As System.Windows.Forms.Label
  Friend WithEvents lbl_payouts As System.Windows.Forms.Label
  Friend WithEvents lbl_payments As System.Windows.Forms.Label
End Class
