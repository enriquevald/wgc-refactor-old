'-------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME :       frm_mobile_bank_edit.vb
'
' DESCRIPTION :       Window for edit a mobile bank
'
' AUTHOR:             Didac Campanals
'
' CREATION DATE:      03-NOV-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 03-NOV-2014  DCS    Initial version   
'--------------------------------------------------------------------

Option Strict Off
Option Explicit On

#Region "Imports"
Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_CommonOperations.CLASS_BASE
Imports WSI.Common
#End Region ' Imports

Public Class frm_mobile_bank_edit
  Inherits frm_base_edit

#Region " Constants "
  Private Const LEN_EMPLOYEE_CODE As Integer = 40

#End Region ' Constants

#Region " Members "
  Private m_mobile_bank As New cls_mobile_bank
#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_MOBILE_BANK_EDIT

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5688) '  "Edit mobile bank"

    ' Identifier
    Me.ef_mb_id.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2180)
    Me.ef_mb_id.IsReadOnly = True

    ' Name
    Me.ef_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(253) '  "Name"
    Me.ef_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_BASE, 50, 0)
    Me.ef_name.OnlyUpperCase = True

    ' Employee code
    Me.ef_employee_code.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4704)
    Me.ef_employee_code.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_EMPLOYEE_CODE)

    ' Locked / Unlocked
    Me.chk_disabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(558)

    ' PIN
    Me.ef_pin.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(747) '  "PIN"
    Me.ef_pin.Password = True
    Me.ef_pin.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, 4, 0)
    Me.ef_pin.TextAlign = HorizontalAlignment.Right

    ' Group Limits
    Me.gb_limits.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5689) ' 5689 - L�mites m�ximos de recarga

    ' Limit by recharge
    Me.ef_limit_recharge.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 12, 2)
    Me.chk_limit_recharge.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5690) ' 5690 "Maximum recharge"
    Me.chk_limit_recharge.Checked = False
    Me.ef_limit_recharge.Enabled = False

    ' Limit by number of recharge
    Me.ef_limit_num_recharges.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 6, 0)
    Me.chk_limit_num_recharges.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5691) ' 5691 - Number of recharges by session
    Me.chk_limit_num_recharges.Checked = False
    Me.ef_limit_num_recharges.Enabled = False

    ' L�mite by session
    Me.ef_limit_by_session.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 12, 2)
    Me.chk_limit_by_session.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5698) ' 5698 - Total recharging by session
    Me.chk_limit_by_session.Checked = False
    Me.ef_limit_by_session.Enabled = False

    AddHandler chk_limit_by_session.CheckedChanged, AddressOf checked_event
    AddHandler chk_limit_num_recharges.CheckedChanged, AddressOf checked_event
    AddHandler chk_limit_recharge.CheckedChanged, AddressOf checked_event

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Visible = False

  End Sub ' GUI_InitControls

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    m_mobile_bank = Me.DbReadObject

    ' Info Fields
    Me.ef_mb_id.Value = m_mobile_bank.ID

    RemoveHandler chk_limit_by_session.CheckedChanged, AddressOf checked_event
    RemoveHandler chk_limit_num_recharges.CheckedChanged, AddressOf checked_event
    RemoveHandler chk_limit_recharge.CheckedChanged, AddressOf checked_event

    ' Editable fields
    If Not String.IsNullOrEmpty(m_mobile_bank.NumberOfRechargesLimit) AndAlso GUI_FormatNumber(m_mobile_bank.NumberOfRechargesLimit, 0) > 0 Then
      Me.ef_limit_num_recharges.Value = GUI_FormatNumber(m_mobile_bank.NumberOfRechargesLimit, 0)
      Me.chk_limit_num_recharges.Checked = True
      Me.ef_limit_num_recharges.Enabled = True

    End If

    If Not m_mobile_bank.RechargeLimit.SqlMoney.IsNull AndAlso GUI_FormatCurrency(m_mobile_bank.RechargeLimit, 2) > 0 Then
      Me.ef_limit_recharge.Value = GUI_FormatCurrency(m_mobile_bank.RechargeLimit, 2)
      Me.ef_limit_recharge.Enabled = True
      Me.chk_limit_recharge.Checked = True
    End If

    If Not m_mobile_bank.TotalLimit.SqlMoney.IsNull AndAlso GUI_FormatCurrency(m_mobile_bank.TotalLimit, 2) > 0 Then
      Me.ef_limit_by_session.Value = GUI_FormatCurrency(m_mobile_bank.TotalLimit, 2)
      Me.ef_limit_by_session.Enabled = True
      Me.chk_limit_by_session.Checked = True
    End If

    Me.ef_name.Value = m_mobile_bank.HolderName
    Me.ef_pin.Value = m_mobile_bank.Pin
    Me.ef_employee_code.Value = m_mobile_bank.EmployeeCode
    Me.chk_disabled.Checked = m_mobile_bank.Blocked

    AddHandler chk_limit_by_session.CheckedChanged, AddressOf checked_event
    AddHandler chk_limit_num_recharges.CheckedChanged, AddressOf checked_event
    AddHandler chk_limit_recharge.CheckedChanged, AddressOf checked_event

  End Sub ' GUI_SetScreenData

  ' PURPOSE: Process button actions in order to branch to a child screen
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)
    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_OK
        'ACCEPT
        If GUI_IsScreenDataOk() Then
          'Accept changes
          Call MyBase.GUI_ButtonClick(ButtonId)
        End If

      Case Else
        MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Get data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_GetScreenData()

    m_mobile_bank = Me.DbEditedObject

    If Me.chk_limit_num_recharges.Checked And Not String.IsNullOrEmpty(Me.ef_limit_num_recharges.Value) Then
      m_mobile_bank.NumberOfRechargesLimit = GUI_FormatNumber(Me.ef_limit_num_recharges.Value, 0)
    Else
      m_mobile_bank.NumberOfRechargesLimit = GUI_FormatNumber(0, 0)
    End If

    If Me.chk_limit_recharge.Checked And Not String.IsNullOrEmpty(Me.ef_limit_recharge.Value) Then
      m_mobile_bank.RechargeLimit = GUI_FormatCurrency(Me.ef_limit_recharge.Value, 2)
    Else
      m_mobile_bank.RechargeLimit = GUI_FormatCurrency(0, 2)
    End If

    If Me.chk_limit_by_session.Checked And Not String.IsNullOrEmpty(Me.ef_limit_by_session.Value) Then
      m_mobile_bank.TotalLimit = GUI_FormatCurrency(Me.ef_limit_by_session.Value, 2)
    Else
      m_mobile_bank.TotalLimit = GUI_FormatCurrency(0, 2)
    End If

    m_mobile_bank.HolderName = Me.ef_name.Value
    m_mobile_bank.EmployeeCode = Me.ef_employee_code.Value
    m_mobile_bank.Pin = Me.ef_pin.Value
    m_mobile_bank.Blocked = Me.chk_disabled.Checked

  End Sub ' GUI_GetScreenData

  ' PURPOSE: Check all data in the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - True if all is OK / False if any field is KO
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Dim _pattern As String

    ' Name
    If Not Me.ef_name.ValidateFormat() Then
      ' "The format of the field %1 is not correct."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.ef_name.Text)
      Call Me.ef_name.Focus()
      Return False
    End If

    ' Employee code has particular validation in GP
    _pattern = WSI.Common.GeneralParam.GetString("User", "EmployeeCode.ValidationRegex")
    If Not String.IsNullOrEmpty(_pattern) Then
      If Not ValidateFormat.CheckRegularExpression(Me.ef_employee_code.Value, _pattern) Then
        ' 4705 "Invalid employee code"
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4705), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call ef_employee_code.Focus()
        Return False
      End If
    End If

    ' PIN is not empty and is numeric and has 4 digits
    If String.IsNullOrEmpty(Me.ef_pin.Value) Then
      ' 1302  "Field '%1' must not be left blank."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.ef_pin.Text)
      Call ef_pin.Focus()
      Return False
    End If

    If Not WSI.Common.ValidateFormat.RawNumber(Me.ef_pin.Value) Then
      ' "The format of the field %1 is not correct."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.ef_pin.Text)
      Call Me.ef_pin.Focus()
      Return False
    End If

    If Not Me.ef_pin.Value.Length = 4 Then
      ' 5699 - �PIN code must be @p0 digits long.�
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5699), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , 4)
      Call Me.ef_pin.Focus()
      Return False
    End If

    ' Limit global is not empty and > 0 and > Limit recharge
    If Me.chk_limit_by_session.Checked Then
      If String.IsNullOrEmpty(Me.ef_limit_by_session.Value) Then
        ' 1302  "Field '%1' must not be left blank."
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.chk_limit_by_session.Text)
        Call ef_limit_by_session.Focus()
        Return False
      End If

      If GUI_ParseCurrency(Me.ef_limit_by_session.Value) <= 0 Then
        ' 125  "You must enter a %1 value greater than 0."
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(125), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.chk_limit_by_session.Text, 0)
        Call ef_limit_by_session.Focus()
        Return False
      End If


      If Me.chk_limit_recharge.Checked Then
        If GUI_ParseCurrency(Me.ef_limit_by_session.Value) < GUI_ParseCurrency(Me.ef_limit_recharge.Value) Then
          ' 1195  '%1' field value must be greater or equal than %2.
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1195), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.chk_limit_by_session.Text, Me.chk_limit_recharge.Text)
          Call ef_limit_by_session.Focus()
          Return False
        End If
      End If

    End If

    ' Limit for recharge is not empty  and > 0
    If Me.chk_limit_recharge.Checked Then
      If String.IsNullOrEmpty(Me.ef_limit_recharge.Value) Then
        ' 1302  "Field '%1' must not be left blank."
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.chk_limit_recharge.Text)
        Call ef_limit_recharge.Focus()
        Return False
      End If

      If GUI_ParseCurrency(Me.ef_limit_recharge.Value) <= 0 Then
        ' 125  "You must enter a %1 value greater than 0."
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(125), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.chk_limit_recharge.Text, 0)
        Call ef_limit_recharge.Focus()
        Return False
      End If
    End If

    ' Limit for num of recharge is not empty  and > 0
    If Me.chk_limit_num_recharges.Checked Then
      If String.IsNullOrEmpty(Me.ef_limit_num_recharges.Value) Then
        ' 1302  "Field '%1' must not be left blank."
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.chk_limit_num_recharges.Text)
        Call ef_limit_num_recharges.Focus()
        Return False
      End If

      If GUI_ParseNumber(Me.ef_limit_num_recharges.Value) <= 0 Then
        ' 125  "You must enter a %1 value greater than 0."
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(125), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.chk_limit_num_recharges.Text, 0)
        Call ef_limit_num_recharges.Focus()
        Return False
      End If
    End If

    Return True
  End Function ' GUI_IsScreenDataOk

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New cls_mobile_bank
        m_mobile_bank = DbEditedObject

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

#End Region ' OVERRIDES

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode with filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - MdiParent
  '           - Mobile Bank ID
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window, _
                         ByVal MobileBankID As Integer)

    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
    Me.DbObjectId = MobileBankID

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)

    If Me.DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If Me.DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "


#End Region ' Private Functions

#Region " Events"

  ' PURPOSE: Handler to enable or disable entry filed whose checkbox is checked
  '
  '  PARAMS:
  '     - INPUT:
  '           - Sender Object
  '           - Event Args
  '     - OUTPUT:
  '           - None
  ' RETURNS:
  '     
  Private Sub checked_event(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _object As CheckBox = sender

    ' Get check box
    Select Case _object.Text
      Case Me.chk_limit_by_session.Text
        Me.ef_limit_by_session.Enabled = Me.chk_limit_by_session.Checked
        If String.IsNullOrEmpty(Me.ef_limit_by_session.Value) Then
          Me.ef_limit_by_session.Value = 0
        End If

      Case Me.chk_limit_num_recharges.Text
        Me.ef_limit_num_recharges.Enabled = Me.chk_limit_num_recharges.Checked
        If String.IsNullOrEmpty(Me.ef_limit_num_recharges.Value) Then
          Me.ef_limit_num_recharges.Value = 0
        End If

      Case Me.chk_limit_recharge.Text
        Me.ef_limit_recharge.Enabled = Me.chk_limit_recharge.Checked
        If String.IsNullOrEmpty(Me.ef_limit_recharge.Value) Then
          Me.ef_limit_recharge.Value = 0
        End If

      Case Else
        ' Do nothing - Unknown check
    End Select

  End Sub ' rb_checked_event

#End Region ' Events

End Class