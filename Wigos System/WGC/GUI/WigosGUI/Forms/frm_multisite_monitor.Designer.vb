<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_multisite_monitor
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Me.tf_last_update = New GUI_Controls.uc_text_field()
    Me.timer = New System.Windows.Forms.Timer(Me.components)
    Me.dg_upload_tasks = New GUI_Controls.uc_grid()
    Me.dg_download_tasks = New GUI_Controls.uc_grid()
    Me.tf_current_status = New GUI_Controls.uc_text_field()
    Me.tf_status_change = New GUI_Controls.uc_text_field()
    Me.tf_last_address = New GUI_Controls.uc_text_field()
    Me.lbl_uploading_processes = New System.Windows.Forms.Label()
    Me.lbl_downloading_processes = New System.Windows.Forms.Label()
    Me.tf_last_message_sent = New GUI_Controls.uc_text_field()
    Me.panel_data.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.tf_last_message_sent)
    Me.panel_data.Controls.Add(Me.lbl_downloading_processes)
    Me.panel_data.Controls.Add(Me.lbl_uploading_processes)
    Me.panel_data.Controls.Add(Me.tf_last_address)
    Me.panel_data.Controls.Add(Me.tf_status_change)
    Me.panel_data.Controls.Add(Me.tf_current_status)
    Me.panel_data.Controls.Add(Me.dg_download_tasks)
    Me.panel_data.Controls.Add(Me.dg_upload_tasks)
    Me.panel_data.Controls.Add(Me.tf_last_update)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(843, 652)
    '
    'tf_last_update
    '
    Me.tf_last_update.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.tf_last_update.IsReadOnly = True
    Me.tf_last_update.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_last_update.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_last_update.Location = New System.Drawing.Point(599, 0)
    Me.tf_last_update.Name = "tf_last_update"
    Me.tf_last_update.Size = New System.Drawing.Size(242, 24)
    Me.tf_last_update.SufixText = "xSeconds"
    Me.tf_last_update.SufixTextVisible = True
    Me.tf_last_update.TabIndex = 6
    Me.tf_last_update.TabStop = False
    Me.tf_last_update.TextWidth = 180
    Me.tf_last_update.Value = "x99.999"
    '
    'dg_upload_tasks
    '
    Me.dg_upload_tasks.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.dg_upload_tasks.CurrentCol = -1
    Me.dg_upload_tasks.CurrentRow = -1
    Me.dg_upload_tasks.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_upload_tasks.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_upload_tasks.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_upload_tasks.Location = New System.Drawing.Point(3, 139)
    Me.dg_upload_tasks.Name = "dg_upload_tasks"
    Me.dg_upload_tasks.PanelRightVisible = False
    Me.dg_upload_tasks.Redraw = True
    Me.dg_upload_tasks.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_upload_tasks.Size = New System.Drawing.Size(833, 260)
    Me.dg_upload_tasks.Sortable = False
    Me.dg_upload_tasks.SortAscending = True
    Me.dg_upload_tasks.SortByCol = 0
    Me.dg_upload_tasks.TabIndex = 4
    Me.dg_upload_tasks.ToolTipped = True
    Me.dg_upload_tasks.TopRow = -2
    '
    'dg_download_tasks
    '
    Me.dg_download_tasks.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.dg_download_tasks.CurrentCol = -1
    Me.dg_download_tasks.CurrentRow = -1
    Me.dg_download_tasks.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_download_tasks.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_download_tasks.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_download_tasks.Location = New System.Drawing.Point(3, 445)
    Me.dg_download_tasks.Name = "dg_download_tasks"
    Me.dg_download_tasks.PanelRightVisible = False
    Me.dg_download_tasks.Redraw = True
    Me.dg_download_tasks.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_download_tasks.Size = New System.Drawing.Size(833, 196)
    Me.dg_download_tasks.Sortable = False
    Me.dg_download_tasks.SortAscending = True
    Me.dg_download_tasks.SortByCol = 0
    Me.dg_download_tasks.TabIndex = 5
    Me.dg_download_tasks.ToolTipped = True
    Me.dg_download_tasks.TopRow = -2
    '
    'tf_current_status
    '
    Me.tf_current_status.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.tf_current_status.IsReadOnly = True
    Me.tf_current_status.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_current_status.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_current_status.Location = New System.Drawing.Point(6, 18)
    Me.tf_current_status.Name = "tf_current_status"
    Me.tf_current_status.Size = New System.Drawing.Size(421, 31)
    Me.tf_current_status.SufixText = "Sufix Text"
    Me.tf_current_status.SufixTextVisible = True
    Me.tf_current_status.TabIndex = 0
    Me.tf_current_status.TabStop = False
    Me.tf_current_status.TextWidth = 220
    Me.tf_current_status.Value = "xConnected"
    '
    'tf_status_change
    '
    Me.tf_status_change.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.tf_status_change.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
    Me.tf_status_change.IsReadOnly = True
    Me.tf_status_change.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_status_change.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_status_change.Location = New System.Drawing.Point(6, 43)
    Me.tf_status_change.Name = "tf_status_change"
    Me.tf_status_change.Size = New System.Drawing.Size(830, 28)
    Me.tf_status_change.SufixText = "Sufix Text"
    Me.tf_status_change.SufixTextVisible = True
    Me.tf_status_change.TabIndex = 2
    Me.tf_status_change.TabStop = False
    Me.tf_status_change.TextWidth = 220
    Me.tf_status_change.Value = "---"
    '
    'tf_last_address
    '
    Me.tf_last_address.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.tf_last_address.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
    Me.tf_last_address.IsReadOnly = True
    Me.tf_last_address.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_last_address.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_last_address.Location = New System.Drawing.Point(6, 63)
    Me.tf_last_address.Name = "tf_last_address"
    Me.tf_last_address.Size = New System.Drawing.Size(827, 28)
    Me.tf_last_address.SufixText = "Sufix Text"
    Me.tf_last_address.SufixTextVisible = True
    Me.tf_last_address.TabIndex = 3
    Me.tf_last_address.TabStop = False
    Me.tf_last_address.TextWidth = 220
    Me.tf_last_address.Value = "---"
    '
    'lbl_uploading_processes
    '
    Me.lbl_uploading_processes.AutoSize = True
    Me.lbl_uploading_processes.Location = New System.Drawing.Point(6, 119)
    Me.lbl_uploading_processes.Name = "lbl_uploading_processes"
    Me.lbl_uploading_processes.Size = New System.Drawing.Size(70, 13)
    Me.lbl_uploading_processes.TabIndex = 11
    Me.lbl_uploading_processes.Text = "xUploading"
    '
    'lbl_downloading_processes
    '
    Me.lbl_downloading_processes.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.lbl_downloading_processes.AutoSize = True
    Me.lbl_downloading_processes.Location = New System.Drawing.Point(6, 423)
    Me.lbl_downloading_processes.Name = "lbl_downloading_processes"
    Me.lbl_downloading_processes.Size = New System.Drawing.Size(87, 13)
    Me.lbl_downloading_processes.TabIndex = 12
    Me.lbl_downloading_processes.Text = "xDownloading"
    '
    'tf_last_message_sent
    '
    Me.tf_last_message_sent.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.tf_last_message_sent.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
    Me.tf_last_message_sent.IsReadOnly = True
    Me.tf_last_message_sent.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_last_message_sent.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_last_message_sent.Location = New System.Drawing.Point(6, 83)
    Me.tf_last_message_sent.Name = "tf_last_message_sent"
    Me.tf_last_message_sent.Size = New System.Drawing.Size(827, 28)
    Me.tf_last_message_sent.SufixText = "Sufix Text"
    Me.tf_last_message_sent.SufixTextVisible = True
    Me.tf_last_message_sent.TabIndex = 13
    Me.tf_last_message_sent.TabStop = False
    Me.tf_last_message_sent.TextWidth = 220
    Me.tf_last_message_sent.Value = "---"
    '
    'frm_multisite_monitor
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(947, 663)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
    Me.Name = "frm_multisite_monitor"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_multisite_monitor"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents tf_last_update As GUI_Controls.uc_text_field
  Friend WithEvents timer As System.Windows.Forms.Timer
  Friend WithEvents dg_download_tasks As GUI_Controls.uc_grid
  Friend WithEvents dg_upload_tasks As GUI_Controls.uc_grid
  Friend WithEvents tf_current_status As GUI_Controls.uc_text_field
  Friend WithEvents tf_status_change As GUI_Controls.uc_text_field
  Friend WithEvents tf_last_address As GUI_Controls.uc_text_field
  Friend WithEvents lbl_downloading_processes As System.Windows.Forms.Label
  Friend WithEvents lbl_uploading_processes As System.Windows.Forms.Label
  Friend WithEvents tf_last_message_sent As GUI_Controls.uc_text_field
End Class
