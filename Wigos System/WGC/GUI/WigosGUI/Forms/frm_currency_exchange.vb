'-------------------------------------------------------------------
' Copyright © 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_currency_exchange
' DESCRIPTION:   Currency exchange.
'
' AUTHOR:        David Hernández
' CREATION DATE: 26-JUN-2013
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------- ------ -----------------------------------------------
' 26-JUN-2013 DHA    Initial version
'-------------------------------------------------------------------

#Region " Imports "

Option Strict Off
Option Explicit On

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.uc_grid
Imports GUI_Controls.uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common

#End Region

Public Class frm_currency_exchange
  Inherits frm_base_edit


#Region " Constants "

  Private Const GRID_COLUMN_ISO_CODE As Integer = 0
  Private Const GRID_COLUMN_TYPE As Integer = 1
  Private Const GRID_COLUMN_TYPE_DESC As Integer = 2
  Private Const GRID_COLUMN_DESCRIPTION As Integer = 3
  Private Const GRID_COLUMN_STATUS As Integer = 4

  Private Const GRID_COLUMNS_CUR_EXC As Integer = 5
  Private Const GRID_HEADER_ROWS_CUR_EXC As Integer = 1

  Private Const GRID_WIDTH_COLUMN_ISO_CODE As Integer = 1100
  Private Const GRID_WIDTH_COLUMN_TYPE As Integer = 0
  Private Const GRID_WIDTH_COLUMN_TYPE_DESC As Integer = 1500
  Private Const GRID_WIDTH_COLUMN_DESCRIPTION As Integer = 4030
  Private Const GRID_WIDTH_COLUMN_STATUS As Integer = 1000

  Private Const RECHARGE_VALUE_TO_CALCULATE = 100
  Private Const MAX_VARIABLE_VALUE = 100

#End Region ' Constants

#Region " Members "

  Private m_national_currency_exchange As CurrencyExchange
  Private m_currency_exchange_datatable As DataTable
  Private m_disable_update_datatable As Boolean
  Private m_last_row_selected As Integer

#End Region ' Members

#Region " Overrides "

  'PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()
    MyBase.GUI_InitControls()

    m_disable_update_datatable = False

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2079) ' Currency Exchange Configuration

    ' Enable / Disable controls
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = False
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Visible = False

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Visible = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Enabled = True

    Me.ef_change.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 16, 8)
    Me.ef_decimals.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 1, 0)
    Me.ef_fixed_commissions.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)
    Me.ef_variable_commissions.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 6, 2)
    Me.ef_fixed_promotions_NR2.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)
    Me.ef_variable_promotions_NR2.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 6, 2)

    Me.ef_national_currency.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2139)
    Me.ef_national_currency.IsReadOnly = True
    Me.ef_change.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2144)
    Me.ef_decimals.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2145)
    Me.lbl_decimals_msg.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2146)
    Me.lbl_fixed.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2147)
    Me.lbl_variable.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2148)
    Me.ef_fixed_commissions.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2149)
    Me.ef_fixed_promotions_NR2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2150)
    Me.ef_variable_commissions.SufixText = GLB_NLS_GUI_STATISTICS.GetString(460)
    Me.ef_variable_promotions_NR2.SufixText = GLB_NLS_GUI_STATISTICS.GetString(460)
    Me.lbl_currency.Value = ""
    Me.ef_fixed_commissions.SufixText = ""
    Me.ef_fixed_promotions_NR2.SufixText = ""
    Me.lbl_recharge_msg.Value = ""
    Me.lbl_national_amount_msg.Value = ""
    Me.lbl_commissions_msg.Value = ""
    Me.lbl_Net_amount_msg.Value = ""
    Me.lbl_promotion_NR2_msg.Value = ""

    Me.dg_currencies_exchange.SelectionMode = SELECTION_MODE.SELECTION_MODE_SINGLE

    Call GUI_StyleSheet()

  End Sub

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CURRENCY_EXCHANGE

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim currency_exchange_data_edit As CLASS_CURRENCY_EXCHANGE
    Dim currency_exchange_data_read As CLASS_CURRENCY_EXCHANGE

    Try
      currency_exchange_data_edit = Me.DbEditedObject
      currency_exchange_data_read = Me.DbReadObject

      m_currency_exchange_datatable = currency_exchange_data_edit.DataCurrencyExchange
      m_currency_exchange_datatable.AcceptChanges()

      m_national_currency_exchange = currency_exchange_data_edit.DataNationalCurrencyExchange()
      SetScreenCurrenciesExchange(currency_exchange_data_edit.DataCurrencyExchange)

      If Not m_national_currency_exchange Is Nothing Then
        ef_national_currency.Value = String.Format("{0} - {1}", m_national_currency_exchange.CurrencyCode, m_national_currency_exchange.Description)

      End If

      If m_currency_exchange_datatable.Rows.Count = 0 Then
        EnableCurrencyControls(False)
      End If

      Call RefreshLabelTexts()

    Catch ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            Me.Name, _
                            "GUI_SetScreenData", _
                            ex.Message)

      ' 137 "Exception error has been found: \n%1"
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(137), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ex.Message)

    Finally

    End Try

  End Sub ' GUI_SetScreenData

  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)

    Dim currency_data_exchange As CLASS_CURRENCY_EXCHANGE
    ' add here new currency class objects, once per control

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_CURRENCY_EXCHANGE
        currency_data_exchange = DbEditedObject
        ' add here new currency data, once per control

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub         ' GUI_DB_Operation

  Protected Overrides Sub GUI_GetScreenData()

    Dim currency_exchange_config_edit As CLASS_CURRENCY_EXCHANGE

    Try

      currency_exchange_config_edit = Me.DbEditedObject

      GetScreenCurrenciesExchange(currency_exchange_config_edit.DataCurrencyExchange)

      With currency_exchange_config_edit
        .DataCurrencyExchange = m_currency_exchange_datatable.Copy()
      End With

    Catch ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, Me.Name, "GUI_GetScreenData", ex.Message)
    Finally

    End Try

  End Sub        ' GUI_GetScreenData

#End Region ' Overrides

#Region " Private Functions "

  ' PURPOSE: Define Currency Exchange Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()

    With Me.dg_currencies_exchange
      Call .Init(GRID_COLUMNS_CUR_EXC, GRID_HEADER_ROWS_CUR_EXC, True)

      '' Currency
      .Column(GRID_COLUMN_ISO_CODE).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2404)
      .Column(GRID_COLUMN_ISO_CODE).Width = GRID_WIDTH_COLUMN_ISO_CODE
      .Column(GRID_COLUMN_ISO_CODE).Alignment = CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_ISO_CODE).IsColumnPrintable = True
      .Column(GRID_COLUMN_ISO_CODE).Editable = False

      '' Type
      .Column(GRID_COLUMN_TYPE).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2141)
      .Column(GRID_COLUMN_TYPE).Width = GRID_WIDTH_COLUMN_TYPE
      .Column(GRID_COLUMN_TYPE).Alignment = CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_TYPE).IsColumnPrintable = False
      .Column(GRID_COLUMN_TYPE).Editable = False

      '' Type Desc
      .Column(GRID_COLUMN_TYPE_DESC).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2141)
      .Column(GRID_COLUMN_TYPE_DESC).Width = GRID_WIDTH_COLUMN_TYPE_DESC
      .Column(GRID_COLUMN_TYPE_DESC).Alignment = CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_TYPE_DESC).IsColumnPrintable = False
      .Column(GRID_COLUMN_TYPE_DESC).Editable = False

      '' Description
      .Column(GRID_COLUMN_DESCRIPTION).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2142)
      .Column(GRID_COLUMN_DESCRIPTION).Width = GRID_WIDTH_COLUMN_DESCRIPTION
      .Column(GRID_COLUMN_DESCRIPTION).Alignment = CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_DESCRIPTION).IsColumnPrintable = False
      .Column(GRID_COLUMN_DESCRIPTION).Editable = True
      .Column(GRID_COLUMN_DESCRIPTION).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_DESCRIPTION).EditionControl.EntryField.SetFilter(FORMAT_TEXT, 50)

      '' Status
      .Column(GRID_COLUMN_STATUS).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2143)
      .Column(GRID_COLUMN_STATUS).Width = GRID_WIDTH_COLUMN_STATUS
      .Column(GRID_COLUMN_STATUS).IsColumnPrintable = False
      .Column(GRID_COLUMN_STATUS).Editable = True
      .Column(GRID_COLUMN_STATUS).EditionControl.Type = CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
      .Column(GRID_COLUMN_STATUS).EditionControl.Type = CONTROL_TYPE_CHECK_BOX

    End With

  End Sub

  ' PURPOSE: Key Press Control
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    Select Case e.KeyChar
      Case Chr(Keys.Enter)

        If Me.dg_currencies_exchange.ContainsFocus Then
          Me.dg_currencies_exchange.KeyPressed(sender, e)
        End If
        Return True
      Case Chr(Keys.Escape)
        Return Me.dg_currencies_exchange.KeyPressed(sender, e)
      Case Else
    End Select

    ' The keypress event is done in uc_grid control

  End Function        ' GUI_KeyPress

  ' PURPOSE: Set the uc_grid s from the screen data with the values from the currencys DataTable.
  '
  '  PARAMS:
  '     - INPUT:
  '           - currencys As DataTable
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetScreenCurrenciesExchange(ByVal currencies_exchange As DataTable)
    Dim _idx_row As Integer
    Dim _idx_row_selected As Integer = -1

    Me.dg_currencies_exchange.Redraw = False
    Me.dg_currencies_exchange.Clear()
    Me.dg_currencies_exchange.Redraw = True

    Me.dg_currencies_exchange.Redraw = False
    m_disable_update_datatable = True

    Try
      For Each _currency_exchange As DataRow In currencies_exchange.Rows
        Me.dg_currencies_exchange.AddRow()
        _idx_row = dg_currencies_exchange.NumRows - 1

        dg_currencies_exchange.Cell(_idx_row, GRID_COLUMN_ISO_CODE).Value = _currency_exchange(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_ISO_CODE)

        Select Case _currency_exchange(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_TYPE)
          Case CurrencyExchangeType.CURRENCY
            dg_currencies_exchange.Cell(_idx_row, GRID_COLUMN_TYPE).Value = CurrencyExchangeType.CURRENCY
            dg_currencies_exchange.Cell(_idx_row, GRID_COLUMN_TYPE_DESC).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2156)
          Case CurrencyExchangeType.CARD
            dg_currencies_exchange.Cell(_idx_row, GRID_COLUMN_TYPE).Value = CurrencyExchangeType.CARD
            dg_currencies_exchange.Cell(_idx_row, GRID_COLUMN_TYPE_DESC).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2157)
        End Select

        Me.dg_currencies_exchange.Cell(_idx_row, GRID_COLUMN_DESCRIPTION).Value = _currency_exchange(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_DESCRIPTION)
        Me.dg_currencies_exchange.Cell(_idx_row, GRID_COLUMN_STATUS).Value = BoolToGridValue(_currency_exchange(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_STATUS))

      Next

      Me.dg_currencies_exchange.SelectFirstRow(True)
      Me.dg_currencies_exchange_RowSelectedEvent(0)
      Me.dg_currencies_exchange.CurrentRow = 0
      m_last_row_selected = 0

      Me.dg_currencies_exchange.Redraw = True
      m_disable_update_datatable = False


    Catch

    End Try

  End Sub ' SetScreenCurrenciesExchange

  ' PURPOSE: Update the currencies DataTable with the allowed value from uc_grid 
  '
  '  PARAMS:
  '     - INPUT:
  '           - currencys As DataTable
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GetScreenCurrenciesExchange(ByVal currencies_exchange As DataTable)

    Dim _idx_row As Integer

    UpdateCurrenciesExchange(m_last_row_selected)

    ' Update checks from uc_grid to datatable
    For _idx_row = 0 To dg_currencies_exchange.NumRows - 1
      If m_currency_exchange_datatable.Rows(_idx_row)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_STATUS) <> GridValueToBool((dg_currencies_exchange.Cell(_idx_row, GRID_COLUMN_STATUS).Value)) Then
        m_currency_exchange_datatable.Rows(_idx_row)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_STATUS) = GridValueToBool((dg_currencies_exchange.Cell(_idx_row, GRID_COLUMN_STATUS).Value))
      End If
    Next

    currencies_exchange = m_currency_exchange_datatable.Copy()

  End Sub      ' GetScreenCurrencies

  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Dim _count As Integer = 0

    UpdateCurrenciesExchange(dg_currencies_exchange.SelectedRows(0))

    For _idx_row As Integer = 0 To m_currency_exchange_datatable.Rows.Count - 1

      If m_currency_exchange_datatable.Rows(_idx_row)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_ISO_CODE) <> m_national_currency_exchange.CurrencyCode _
        Or m_currency_exchange_datatable.Rows(_idx_row)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_TYPE) <> m_national_currency_exchange.Type Then

        If m_currency_exchange_datatable.Rows(_idx_row)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_DESCRIPTION) Is DBNull.Value Then
          dg_currencies_exchange.ClearSelection()
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2142))
          dg_currencies_exchange.Row(_idx_row).IsSelected = True
          dg_currencies_exchange.Focus()
          SelectRow(_idx_row)
          m_disable_update_datatable = False
          Return False
        End If

        If m_currency_exchange_datatable.Rows(_idx_row)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_CHANGE) Is DBNull.Value Then
          dg_currencies_exchange.ClearSelection()
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_change.Text)
          dg_currencies_exchange.Focus()
          SelectRow(_idx_row)
          ef_change.Focus()
          m_disable_update_datatable = False
          Return False
        End If

        If m_currency_exchange_datatable.Rows(_idx_row)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_CHANGE) < 0 Then
          dg_currencies_exchange.ClearSelection()
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_change.Text)
          dg_currencies_exchange.Focus()
          SelectRow(_idx_row)
          ef_change.Focus()
          m_disable_update_datatable = False
          Return False
        End If

        If m_currency_exchange_datatable.Rows(_idx_row)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_NUM_DECIMALS) Is DBNull.Value Then
          dg_currencies_exchange.ClearSelection()
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_decimals.Text)
          dg_currencies_exchange.Focus()
          SelectRow(_idx_row)
          ef_decimals.Focus()
          m_disable_update_datatable = False
          Return False
        End If

        If m_currency_exchange_datatable.Rows(_idx_row)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_NUM_DECIMALS) < 0 _
              Or m_currency_exchange_datatable.Rows(_idx_row)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_NUM_DECIMALS) > 9 Then
          dg_currencies_exchange.ClearSelection()
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_decimals.Text)
          dg_currencies_exchange.Focus()
          SelectRow(_idx_row)
          ef_decimals.Focus()
          m_disable_update_datatable = False
          Return False
        End If

        If Not m_currency_exchange_datatable.Rows(_idx_row)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_FIXED_COMMISSION) Is DBNull.Value _
              AndAlso m_currency_exchange_datatable.Rows(_idx_row)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_FIXED_COMMISSION) < 0 Then
          dg_currencies_exchange.ClearSelection()
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _
              String.Format("{0} {1}", ef_fixed_commissions.Text, lbl_fixed.Text))
          dg_currencies_exchange.Focus()
          SelectRow(_idx_row)
          ef_fixed_commissions.Focus()
          m_disable_update_datatable = False
          Return False
        End If

        If Not m_currency_exchange_datatable.Rows(_idx_row)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_VARIABLE_COMMISSION) Is DBNull.Value _
              AndAlso (m_currency_exchange_datatable.Rows(_idx_row)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_VARIABLE_COMMISSION) < 0 _
              Or m_currency_exchange_datatable.Rows(_idx_row)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_VARIABLE_COMMISSION) > MAX_VARIABLE_VALUE) Then
          dg_currencies_exchange.ClearSelection()
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _
              String.Format("{0} {1}", ef_fixed_commissions.Text, lbl_variable.Text))
          dg_currencies_exchange.Focus()
          SelectRow(_idx_row)
          ef_variable_commissions.Focus()
          m_disable_update_datatable = False
          Return False
        End If

        If Not m_currency_exchange_datatable.Rows(_idx_row)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_FIXED_NR2) Is DBNull.Value _
              AndAlso m_currency_exchange_datatable.Rows(_idx_row)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_FIXED_NR2) < 0 Then
          dg_currencies_exchange.ClearSelection()
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _
              String.Format("{0} {1}", ef_fixed_promotions_NR2.Text, lbl_fixed.Text))
          dg_currencies_exchange.Focus()
          SelectRow(_idx_row)
          ef_fixed_promotions_NR2.Focus()
          m_disable_update_datatable = False
          Return False
        End If

        If Not m_currency_exchange_datatable.Rows(_idx_row)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_VARIABLE_NR2) Is DBNull.Value _
              AndAlso (m_currency_exchange_datatable.Rows(_idx_row)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_VARIABLE_NR2) < 0 _
              Or m_currency_exchange_datatable.Rows(_idx_row)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_VARIABLE_NR2) > MAX_VARIABLE_VALUE) Then
          dg_currencies_exchange.ClearSelection()
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _
              String.Format("{0} {1}", ef_fixed_promotions_NR2.Text, lbl_variable.Text))
          dg_currencies_exchange.Focus()
          SelectRow(_idx_row)
          ef_variable_promotions_NR2.Focus()
          m_disable_update_datatable = False
          Return False
        End If
      End If
    Next
    Return True

  End Function  ' GUI_IsScreenDataOk

  ' PURPOSE:  Cast from boolean to uc_grid checked
  '
  '  PARAMS:
  '     - INPUT:
  '           - enabled
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function BoolToGridValue(ByVal enabled As Boolean) As String
    If (enabled) Then
      Return uc_grid.GRID_CHK_CHECKED
    Else
      Return uc_grid.GRID_CHK_UNCHECKED
    End If
  End Function     ' BoolToGridValue

  ' PURPOSE:  Cast from uc_grid checked to boolean
  '
  '  PARAMS:
  '     - INPUT:
  '           - enabled
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GridValueToBool(ByVal enabled As String) As Boolean

    If (enabled.Equals(uc_grid.GRID_CHK_CHECKED) Or enabled.Equals(uc_grid.GRID_CHK_CHECKED_DISABLED)) Then
      Return True
    Else
      Return False
    End If

  End Function     ' GridValueToBool

  ' PURPOSE:  Enable/Disable currency controls
  '
  '  PARAMS:
  '     - INPUT:
  '           - enabled
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub EnableCurrencyControls(ByVal enabled As Boolean)
    Me.ef_decimals.Enabled = enabled
    Me.ef_fixed_commissions.Enabled = enabled
    Me.ef_variable_commissions.Enabled = enabled
    Me.ef_fixed_promotions_NR2.Enabled = enabled
    Me.ef_variable_promotions_NR2.Enabled = enabled
    Me.lbl_fixed.Enabled = enabled
    Me.lbl_variable.Enabled = enabled
  End Sub     ' BoolToGridValue

  ' PURPOSE: Timer operation to refresh the test area.
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:

  Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
    Call RefreshLabelTexts()
  End Sub

  ' PURPOSE:  Update the values from labels
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub RefreshLabelTexts()

    Dim _selected_row As Integer
    Dim _current_currency_exchange As CurrencyExchange
    Dim _current_currency_exchange_result As CurrencyExchangeResult

    If Not Me.dg_currencies_exchange.SelectedRows Is Nothing Then

      _selected_row = Me.dg_currencies_exchange.SelectedRows(0)

      If _selected_row >= 0 Then
        _current_currency_exchange = New CurrencyExchange()
        _current_currency_exchange_result = New CurrencyExchangeResult()

        _current_currency_exchange.CurrencyCode = dg_currencies_exchange.Cell(_selected_row, GRID_COLUMN_ISO_CODE).Value
        _current_currency_exchange.Description = dg_currencies_exchange.Cell(_selected_row, GRID_COLUMN_DESCRIPTION).Value
        m_currency_exchange_datatable.Rows(_selected_row)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_DESCRIPTION) = dg_currencies_exchange.Cell(_selected_row, GRID_COLUMN_DESCRIPTION).Value

        If String.IsNullOrEmpty(ef_change.Value) Then
          _current_currency_exchange.ChangeRate = Nothing
        Else
          _current_currency_exchange.ChangeRate = GUI_ParseCurrency(ef_change.Value)
        End If

        If String.IsNullOrEmpty(ef_decimals.Value) Then
          _current_currency_exchange.Decimals = Nothing
        Else
          _current_currency_exchange.Decimals = GUI_ParseNumber(ef_decimals.Value)
        End If

        If String.IsNullOrEmpty(ef_fixed_commissions.Value) Then
          _current_currency_exchange.FixedComission = Nothing
        Else
          _current_currency_exchange.FixedComission = GUI_ParseCurrency(ef_fixed_commissions.Value)
        End If

        If String.IsNullOrEmpty(ef_fixed_promotions_NR2.Value) Then
          _current_currency_exchange.FixedNR2 = Nothing
        Else
          _current_currency_exchange.FixedNR2 = GUI_ParseCurrency(ef_fixed_promotions_NR2.Value)
        End If

        If String.IsNullOrEmpty(ef_variable_commissions.Value) Then
          _current_currency_exchange.VariableComission = Nothing
        Else
          _current_currency_exchange.VariableComission = GUI_ParseCurrency(ef_variable_commissions.Value)
        End If

        If String.IsNullOrEmpty(ef_variable_promotions_NR2.Value) Then
          _current_currency_exchange.VariableNR2 = Nothing
        Else
          _current_currency_exchange.VariableNR2 = GUI_ParseCurrency(ef_variable_promotions_NR2.Value)
        End If

        _current_currency_exchange.ApplyExchange(RECHARGE_VALUE_TO_CALCULATE, _current_currency_exchange_result)

        lbl_currency.Value = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(2140), _current_currency_exchange.CurrencyCode, _current_currency_exchange.Description)
        lbl_recharge_msg.Value = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(2151), Currency.Format(RECHARGE_VALUE_TO_CALCULATE, _current_currency_exchange.CurrencyCode))
        lbl_national_amount_msg.Value = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(2152), Currency.Format(_current_currency_exchange_result.GrossAmount, m_national_currency_exchange.CurrencyCode))
        lbl_commissions_msg.Value = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(2153), Currency.Format(_current_currency_exchange_result.Comission, m_national_currency_exchange.CurrencyCode))
        lbl_Net_amount_msg.Value = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(2154), Currency.Format(_current_currency_exchange_result.NetAmount, m_national_currency_exchange.CurrencyCode))
        lbl_promotion_NR2_msg.Value = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(2155), Currency.Format(_current_currency_exchange_result.NR2Amount, m_national_currency_exchange.CurrencyCode))

        ef_fixed_commissions.SufixText = m_national_currency_exchange.CurrencyCode
        ef_fixed_promotions_NR2.SufixText = m_national_currency_exchange.CurrencyCode

      End If
    Else
      'EnableCurrencyControls(False)
    End If
  End Sub

  ' PURPOSE:  Update the table datarow from controls values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub UpdateCurrenciesExchange(ByVal id_rox As Integer)

    If Not (Me.dg_currencies_exchange.Cell(id_rox, GRID_COLUMN_ISO_CODE).Value = m_national_currency_exchange.CurrencyCode _
        And Me.dg_currencies_exchange.Cell(id_rox, GRID_COLUMN_TYPE).Value = m_national_currency_exchange.Type) Then

      If String.IsNullOrEmpty(Me.dg_currencies_exchange.Cell(id_rox, GRID_COLUMN_DESCRIPTION).Value) Then
        m_currency_exchange_datatable.Rows(id_rox)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_DESCRIPTION) = DBNull.Value
      ElseIf Me.dg_currencies_exchange.Cell(id_rox, GRID_COLUMN_DESCRIPTION).Value <> m_currency_exchange_datatable.Rows(id_rox)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_DESCRIPTION) Then
        m_currency_exchange_datatable.Rows(id_rox)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_DESCRIPTION) = Me.dg_currencies_exchange.Cell(id_rox, GRID_COLUMN_DESCRIPTION).Value
      End If

      If String.IsNullOrEmpty(ef_change.Value) Then
        m_currency_exchange_datatable.Rows(id_rox)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_CHANGE) = DBNull.Value
      ElseIf m_currency_exchange_datatable.Rows(id_rox)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_CHANGE) Is DBNull.Value Then
        m_currency_exchange_datatable.Rows(id_rox)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_CHANGE) = GUI_ParseCurrency(ef_change.Value)
      ElseIf Not GUI_ParseCurrency(ef_change.Value).Equals(GUI_ParseCurrency(m_currency_exchange_datatable.Rows(id_rox)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_CHANGE))) Then
        m_currency_exchange_datatable.Rows(id_rox)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_CHANGE) = GUI_ParseCurrency(ef_change.Value)
      End If

      If String.IsNullOrEmpty(ef_decimals.Value) Then
        m_currency_exchange_datatable.Rows(id_rox)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_NUM_DECIMALS) = DBNull.Value
      ElseIf m_currency_exchange_datatable.Rows(id_rox)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_NUM_DECIMALS) Is DBNull.Value Then
        m_currency_exchange_datatable.Rows(id_rox)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_NUM_DECIMALS) = GUI_ParseCurrency(ef_decimals.Value)
      ElseIf Not GUI_ParseNumber(ef_decimals.Value).Equals(GUI_ParseNumber(m_currency_exchange_datatable.Rows(id_rox)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_NUM_DECIMALS))) Then
        m_currency_exchange_datatable.Rows(id_rox)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_NUM_DECIMALS) = GUI_ParseCurrency(ef_decimals.Value)
      End If

      If String.IsNullOrEmpty(ef_fixed_commissions.Value) Then
        m_currency_exchange_datatable.Rows(id_rox)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_FIXED_COMMISSION) = DBNull.Value
      ElseIf m_currency_exchange_datatable.Rows(id_rox)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_FIXED_COMMISSION) Is DBNull.Value Then
        m_currency_exchange_datatable.Rows(id_rox)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_FIXED_COMMISSION) = GUI_ParseCurrency(ef_fixed_commissions.Value)
      ElseIf Not GUI_ParseCurrency(ef_fixed_commissions.Value).Equals(GUI_ParseCurrency(m_currency_exchange_datatable.Rows(id_rox)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_FIXED_COMMISSION))) Then
        m_currency_exchange_datatable.Rows(id_rox)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_FIXED_COMMISSION) = GUI_ParseCurrency(ef_fixed_commissions.Value)
      End If

      If String.IsNullOrEmpty(ef_fixed_promotions_NR2.Value) Then
        m_currency_exchange_datatable.Rows(id_rox)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_FIXED_NR2) = DBNull.Value
      ElseIf m_currency_exchange_datatable.Rows(id_rox)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_FIXED_NR2) Is DBNull.Value Then
        m_currency_exchange_datatable.Rows(id_rox)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_FIXED_NR2) = GUI_ParseCurrency(ef_fixed_promotions_NR2.Value)
      ElseIf Not GUI_ParseCurrency(ef_fixed_promotions_NR2.Value).Equals(GUI_ParseCurrency(m_currency_exchange_datatable.Rows(id_rox)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_FIXED_NR2))) Then
        m_currency_exchange_datatable.Rows(id_rox)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_FIXED_NR2) = GUI_ParseCurrency(ef_fixed_promotions_NR2.Value)
      End If

      If String.IsNullOrEmpty(ef_variable_commissions.Value) Then
        m_currency_exchange_datatable.Rows(id_rox)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_VARIABLE_COMMISSION) = DBNull.Value
      ElseIf m_currency_exchange_datatable.Rows(id_rox)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_VARIABLE_COMMISSION) Is DBNull.Value Then
        m_currency_exchange_datatable.Rows(id_rox)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_VARIABLE_COMMISSION) = GUI_ParseCurrency(ef_variable_commissions.Value)
      ElseIf Not GUI_ParseCurrency(ef_variable_commissions.Value).Equals(GUI_ParseCurrency(m_currency_exchange_datatable.Rows(id_rox)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_VARIABLE_COMMISSION))) Then
        m_currency_exchange_datatable.Rows(id_rox)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_VARIABLE_COMMISSION) = GUI_ParseCurrency(ef_variable_commissions.Value)
      End If

      If String.IsNullOrEmpty(ef_variable_promotions_NR2.Value) Then
        m_currency_exchange_datatable.Rows(id_rox)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_VARIABLE_NR2) = DBNull.Value
      ElseIf m_currency_exchange_datatable.Rows(id_rox)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_VARIABLE_NR2) Is DBNull.Value Then
        m_currency_exchange_datatable.Rows(id_rox)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_VARIABLE_NR2) = GUI_ParseCurrency(ef_variable_promotions_NR2.Value)
      ElseIf Not GUI_ParseCurrency(ef_variable_promotions_NR2.Value).Equals(GUI_ParseCurrency(m_currency_exchange_datatable.Rows(id_rox)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_VARIABLE_NR2))) Then
        m_currency_exchange_datatable.Rows(id_rox)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_VARIABLE_NR2) = GUI_ParseCurrency(ef_variable_promotions_NR2.Value)
      End If
    End If

  End Sub

  ' PURPOSE:  Fill controls by selected row
  '
  '  PARAMS:
  '     - INPUT:
  '           - SelectedRow
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Sub SetControlsDataFromSelectedRow(ByVal SelectedRow As System.Int32)

    Dim _selected_row As DataRow

    _selected_row = Me.m_currency_exchange_datatable.Rows(SelectedRow)

    If _selected_row(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_CHANGE) Is DBNull.Value Then
      ef_change.Value = ""
    Else
      ef_change.Value = _selected_row(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_CHANGE)
    End If

    If _selected_row(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_NUM_DECIMALS) Is DBNull.Value Then
      ef_decimals.Value = ""
    Else
      ef_decimals.Value = _selected_row(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_NUM_DECIMALS)
    End If

    If _selected_row(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_FIXED_COMMISSION) Is DBNull.Value Then
      ef_fixed_commissions.Value = ""
    Else
      ef_fixed_commissions.Value = _selected_row(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_FIXED_COMMISSION)
    End If

    If _selected_row(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_VARIABLE_COMMISSION) Is DBNull.Value Then
      ef_variable_commissions.Value = ""
    Else
      ef_variable_commissions.Value = _selected_row(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_VARIABLE_COMMISSION)
    End If

    If _selected_row(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_FIXED_NR2) Is DBNull.Value Then
      ef_fixed_promotions_NR2.Value = ""
    Else
      ef_fixed_promotions_NR2.Value = _selected_row(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_FIXED_NR2)
    End If

    If _selected_row(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_VARIABLE_NR2) Is DBNull.Value Then
      ef_variable_promotions_NR2.Value = ""
    Else
      ef_variable_promotions_NR2.Value = _selected_row(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_VARIABLE_NR2)
    End If

    If _selected_row(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_ISO_CODE) = m_national_currency_exchange.CurrencyCode _
        And _selected_row(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_TYPE) = m_national_currency_exchange.Type Then
      EnableCurrencyControls(False)
    Else
      EnableCurrencyControls(True)
    End If

  End Sub

  ' PURPOSE:  Set the selected Row
  '
  '  PARAMS:
  '     - INPUT:
  '           - IndexRow
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SelectRow(ByVal IndexRow As Integer)

    dg_currencies_exchange.Redraw = False

    If dg_currencies_exchange.NumRows > 0 Then
      dg_currencies_exchange.IsSelected(IndexRow) = True
    End If

    dg_currencies_exchange.Redraw = True
    dg_currencies_exchange_RowSelectedEvent(IndexRow)

  End Sub

#End Region ' Private Functions

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Events "

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.

  End Sub


  Private Sub dg_currencies_exchange_RowSelectedEvent(ByVal SelectedRow As System.Int32) Handles dg_currencies_exchange.RowSelectedEvent

    If Not m_national_currency_exchange Is Nothing Then
      If Not (Me.dg_currencies_exchange.Cell(m_last_row_selected, GRID_COLUMN_ISO_CODE).Value = m_national_currency_exchange.CurrencyCode _
          And Me.dg_currencies_exchange.Cell(m_last_row_selected, GRID_COLUMN_TYPE).Value = m_national_currency_exchange.Type) Then
        UpdateCurrenciesExchange(m_last_row_selected)
      End If

      If Not m_currency_exchange_datatable Is Nothing And Not m_national_currency_exchange Is Nothing Then
        ' Set the data to the screen
        SetControlsDataFromSelectedRow(SelectedRow)
        m_last_row_selected = SelectedRow
      End If

      If Not m_currency_exchange_datatable Is Nothing And Not m_national_currency_exchange Is Nothing _
          And m_national_currency_exchange.CurrencyCode = (Me.dg_currencies_exchange.Cell(m_last_row_selected, GRID_COLUMN_ISO_CODE).Value) Then
        ef_change.Value = 1
        ef_change.Enabled = False
      Else
        ef_change.Enabled = True
      End If
    End If
  End Sub   ' dg_currencies_exchange_RowSelectedEvent

  Private Sub dg_currencies_exchange_CellDataChangedEvent(ByVal Row As System.Int32, ByVal Column As System.Int32) Handles dg_currencies_exchange.CellDataChangedEvent
    ' Avoid modifying the description and status of national currency
    If Me.dg_currencies_exchange.Cell(Row, GRID_COLUMN_ISO_CODE).Value = m_national_currency_exchange.CurrencyCode _
          And Me.dg_currencies_exchange.Cell(Row, GRID_COLUMN_TYPE).Value = m_national_currency_exchange.Type Then
      dg_currencies_exchange.Cell(Row, GRID_COLUMN_DESCRIPTION).Value = m_currency_exchange_datatable.Rows(Row)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_DESCRIPTION)
      dg_currencies_exchange.Cell(Row, GRID_COLUMN_STATUS).Value = BoolToGridValue(m_currency_exchange_datatable.Rows(Row)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_STATUS))
    Else
      Me.m_currency_exchange_datatable.Rows(Row)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_DESCRIPTION) = dg_currencies_exchange.Cell(Row, GRID_COLUMN_DESCRIPTION).Value
      Me.m_currency_exchange_datatable.Rows(Row)(CLASS_CURRENCY_EXCHANGE.SQL_COLUMN_CUR_CURRENCY_STATUS) = GridValueToBool(dg_currencies_exchange.Cell(Row, GRID_COLUMN_STATUS).Value)
    End If
  End Sub   ' dg_currencies_exchange_CellDataChangedEvent

#End Region ' Events

End Class