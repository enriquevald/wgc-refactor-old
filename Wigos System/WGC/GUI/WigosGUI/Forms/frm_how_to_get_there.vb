﻿'-------------------------------------------------------------------
' Copyright © 2016 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_how_to_get_there.vb
'
' DESCRIPTION : Window for edit Casino Mobile HowToGetThere and items
'               In this window can add and edit items,
'               edit title and address.
'
' AUTHOR:        Gustavo Alí
'
' CREATION DATE: 20-SEP-2016
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 20-SEP-2016  GDA    Initial version   
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off

#Region "Imports"
Imports GUI_Controls
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls.uc_grid
Imports GUI_Controls.uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE
Imports System.Runtime.InteropServices
Imports System.Windows.Forms.UserControl
Imports Newtonsoft.Json.Linq
Imports WSI.Common

#End Region  ' Imports

Public Class frm_how_to_get_there
  Inherits frm_base_edit
#Region " Constants "

  Private Const LEN_HOW_TO_TITLE As Integer = 50
  Private Const LEN_HOW_TO_ADDRESS As Integer = 50
  Private Const LEN_HOW_TO_VALUE As Integer = 50
  Private Const LANG_SPANISH As Integer = 1

  ' Database codes
  Private Const HOW_TO_TYPE_TEL As Integer = 1
  Private Const HOW_TO_TYPE_EMAIL As Integer = 2

  ' Grid columns
  Private Const GRID_COLUMNS_ITEM As Integer = 6
  Private Const GRID_HEADER_ROWS_ITEM As Integer = 1

  Private Const GRID_COLUMN_ID = 0
  Private Const GRID_COLUMN_INDEX = 1
  Private Const GRID_COLUMN_TITLE = 2
  Private Const GRID_COLUMN_VALUE = 3
  Private Const GRID_COLUMN_TYPE_VALUE = 4
  Private Const GRID_COLUMN_TYPE_COMBO = 5

  Private Const GRID_WIDTH_ID As Integer = 0
  Private Const GRID_WIDTH_INDEX As Integer = 0
  Private Const GRID_WIDTH_TITLE As Integer = 2000
  Private Const GRID_WIDTH_VALUE As Integer = 3200
  Private Const GRID_WIDTH_TYPE_VALUE As Integer = 0
  Private Const GRID_WIDTH_TYPE_COMBO As Integer = 1500

  'RAB 25-JAN-2016
  Private Const ROW_HEIGHT_HOW_TO_ITEMS As Integer = 240

#End Region

#Region " Members "

  Private m_how_to As CLS_HOW_TO_GET_THERE
  Private m_changed_values As Boolean
  Private m_new_count_id As Integer
  Private m_new_items As Stack
  Dim _m_dict As New Dictionary(Of Integer, String)

#End Region ' Members

#Region " Overrides "

  Public Overloads Sub ShowEditItem()

    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT


    Dim url As String = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/howtogetthere/getbylanguage"
    Dim api As New CLS_API(url)
    Dim resp As String = api.getData()

    If resp = "null" Then
      Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW
      Me.DbObjectId = Nothing
    Else
      Dim _jobj As JObject = JObject.Parse(api.sanitizeData(resp))
      Me.DbObjectId = _jobj
    End If

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)

    If Me.DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If Me.DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowEditItem

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_HOW_TO_GET_THERE

    Call MyBase.GUI_SetFormId()
  End Sub 'GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    ' Hide the delete button 
    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False

    If Me.ScreenMode = frm_base_edit.ENUM_SCREEN_MODE.MODE_NEW Then
      Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7627)
      m_changed_values = False
    Else
      ef_how_to_title.TextVisible = True
      ef_how_to_address.TextVisible = True
      Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7627)
      m_changed_values = True
    End If

    'HowToGetThere Title
    Me.lblTitle.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7628)
    Me.ef_how_to_title.TextVisible = True
    Me.ef_how_to_title.Value = ""
    Me.ef_how_to_title.Focus()
    Me.ef_how_to_title.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_HOW_TO_TITLE)

    'HowToGetThere Address
    Me.lblAddress.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7629)
    Me.ef_how_to_address.TextVisible = True
    Me.ef_how_to_address.Value = ""
    Me.ef_how_to_address.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_HOW_TO_ADDRESS)

    'HowToGetThere item grid buttons
    ' Button Add / Delete rows to grid
    Me.btn_items_add.Enabled = Me.Permissions.Write
    Me.btn_items_del.Enabled = Me.Permissions.Write
    btn_items_add.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1684)
    btn_items_del.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1683)

    AddHandler dg_how_to_items.RowSelectedEvent, AddressOf dg_how_to_items_RowSelect
    AddHandler dg_how_to_items.CellDataChangedEvent, AddressOf dg_how_to_items_CellDataChange

    'm_how_to.ReadHowToGetThereItems()
    m_new_count_id = 0
    m_new_items = New Stack

    Call GUI_StyleSheet()

  End Sub ' GUI_InitControls

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    m_how_to = Me.DbReadObject

    ' Name
    Me.ef_how_to_title.Value = m_how_to.Title

    ' Description
    Me.ef_how_to_address.Value = m_how_to.Address

    Call SetHowToGetThereItems()

  End Sub ' GUI_SetScreenData

  ' PURPOSE: Validate the data presented on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - True:  Data is ok
  '     - False: Data is NOT ok
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Dim _all_data_ok As Boolean

    m_how_to = DbEditedObject
    _all_data_ok = True

    ' Check title HowToGetThere
    If Not Me.ef_how_to_title.Value.Length > 0 Then
      _all_data_ok = False
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6697), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(7628)) ' "Field '%1' must not be left blank." param %1-> "Name"
    End If



    ' Check at least one row in grid
    If _all_data_ok And Not Me.dg_how_to_items.NumRows > 0 Then
      _all_data_ok = False
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6699), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR) ' "You must add at least one element to the list."
    Else
      ' Check all data GRID
      For _idx_row As Integer = 0 To Me.dg_how_to_items.NumRows - 1

        ' Check Not Null rows
        If _all_data_ok And IsNullRow(_idx_row) Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6700), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(6702)) ' "You must enter a value for %1." Param $1-> "Value"
          _all_data_ok = False
          Exit For
        End If

      Next
    End If

    Return _all_data_ok

  End Function 'GUI_IsScreenDataOk

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_GetScreenData()

    Dim _how_to_items() As DataRow
    Try
      m_how_to = Me.DbEditedObject

      ' Name
      m_how_to.Title = Me.ef_how_to_title.Value

      ' Description
      m_how_to.Address = Me.ef_how_to_address.Value

      ' Grid Info
      If Not Me.dg_how_to_items Is Nothing AndAlso Me.dg_how_to_items.NumRows > 0 Then
        For _idx_row As Integer = 0 To Me.dg_how_to_items.NumRows - 1
          _how_to_items = m_how_to.HowToGetThereItems.Select("howToGetThereItemId = " & Me.dg_how_to_items.Cell(_idx_row, GRID_COLUMN_ID).Value)

          If Not String.IsNullOrEmpty(Me.dg_how_to_items.Cell(_idx_row, GRID_COLUMN_TITLE).Value) AndAlso _
            _how_to_items(0)(CLS_HOW_TO_GET_THERE.COLUMN_HOW_TO_ITEM_TITLE) <> Me.dg_how_to_items.Cell(_idx_row, GRID_COLUMN_TITLE).Value Then
            _how_to_items(0)(CLS_HOW_TO_GET_THERE.COLUMN_HOW_TO_ITEM_TITLE) = Me.dg_how_to_items.Cell(_idx_row, GRID_COLUMN_TITLE).Value
          End If

          If Not String.IsNullOrEmpty(Me.dg_how_to_items.Cell(_idx_row, GRID_COLUMN_TYPE_VALUE).Value) AndAlso _
            _how_to_items(0)(CLS_HOW_TO_GET_THERE.COLUMN_HOW_TO_ITEM_TYPE) <> Me.dg_how_to_items.Cell(_idx_row, GRID_COLUMN_TYPE_VALUE).Value Then
            _how_to_items(0)(CLS_HOW_TO_GET_THERE.COLUMN_HOW_TO_ITEM_TYPE) = Me.dg_how_to_items.Cell(_idx_row, GRID_COLUMN_TYPE_VALUE).Value
          End If

          If Not String.IsNullOrEmpty(Me.dg_how_to_items.Cell(_idx_row, GRID_COLUMN_VALUE).Value) AndAlso _
                          _how_to_items(0)(CLS_HOW_TO_GET_THERE.COLUMN_HOW_TO_ITEM_VALUE) <> Me.dg_how_to_items.Cell(_idx_row, GRID_COLUMN_VALUE).Value Then
            _how_to_items(0)(CLS_HOW_TO_GET_THERE.COLUMN_HOW_TO_ITEM_VALUE) = Me.dg_how_to_items.Cell(_idx_row, GRID_COLUMN_VALUE).Value
          End If

        Next

      End If
    Catch ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, Me.Name, "GUI_GetScreenData", ex.Message)

    End Try

  End Sub ' GUI_GetScreenData

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLS_HOW_TO_GET_THERE()
        m_how_to = DbEditedObject
        m_how_to.HowToGetThereId = 0
        DbStatus = ENUM_STATUS.STATUS_OK
      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE: Button overridable operations. 
  '          Define actions for each button are pressed.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Select Case ButtonId


      Case frm_base_sel.ENUM_BUTTON.BUTTON_OK
        'ACCEPT
        If GUI_IsScreenDataOk() Then
          'Accept changes
          Call MyBase.GUI_ButtonClick(ButtonId)
        End If

      Case Else
        MyBase.GUI_ButtonClick(ButtonId)

    End Select

  End Sub  ' GUI_ButtonClick

#End Region '  Overrides 

#Region " Private Functions "

  ' PURPOSE: Define style for howtogetthere grid 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  Private Sub GUI_StyleSheet()
    Dim _cls_types As New CLS_HOW_TO_GET_THERE_ITEM_TYPE()

    With Me.dg_how_to_items
      Call .Init(GRID_COLUMNS_ITEM, GRID_HEADER_ROWS_ITEM, True)

      ' ID Empty
      .Column(GRID_COLUMN_ID).Header.Text = " "
      .Column(GRID_COLUMN_ID).Width = GRID_WIDTH_ID
      .Column(GRID_COLUMN_ID).HighLightWhenSelected = False
      .Column(GRID_COLUMN_ID).IsColumnPrintable = False

      ' Index
      .Column(GRID_COLUMN_INDEX).Header.Text = " "
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Title
      .Column(GRID_COLUMN_TITLE).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7628)
      .Column(GRID_COLUMN_TITLE).Width = GRID_WIDTH_TITLE
      .Column(GRID_COLUMN_TITLE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_TITLE).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_TITLE).EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_HOW_TO_TITLE, 0)
      .Column(GRID_COLUMN_TITLE).Editable = True

      ' Value
      .Column(GRID_COLUMN_VALUE).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7631)
      .Column(GRID_COLUMN_VALUE).Width = GRID_WIDTH_VALUE
      .Column(GRID_COLUMN_VALUE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_VALUE).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_VALUE).EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_HOW_TO_VALUE, 0)
      .Column(GRID_COLUMN_VALUE).Editable = True


      ' Type 
      .Column(GRID_COLUMN_TYPE_VALUE).Header.Text = ""
      .Column(GRID_COLUMN_TYPE_VALUE).Width = GRID_WIDTH_TYPE_VALUE
      .Column(GRID_COLUMN_TYPE_VALUE).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_TYPE_VALUE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_TYPE_VALUE).HighLightWhenSelected = False
      .Column(GRID_COLUMN_TYPE_VALUE).IsColumnPrintable = False

      For Each item As KeyValuePair(Of Integer, Integer) In _cls_types.Items()
        _m_dict.Add(item.Key, GLB_NLS_GUI_PLAYER_TRACKING.GetString(item.Value))
      Next

      .Column(GRID_COLUMN_TYPE_COMBO).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7632)
      .Column(GRID_COLUMN_TYPE_COMBO).Width = GRID_WIDTH_TYPE_COMBO
      .Column(GRID_COLUMN_TYPE_COMBO).EditionControl.Type = CONTROL_TYPE_COMBO
      .Column(GRID_COLUMN_TYPE_COMBO).EditionControl.ComboBox.IsReadOnly = False
      .Column(GRID_COLUMN_TYPE_COMBO).EditionControl.ComboBox.Add(_m_dict)
      .Column(GRID_COLUMN_TYPE_COMBO).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_TYPE_COMBO).Editable = True

    End With
  End Sub ' GUI_StyleSheet


  ' PURPOSE: Set HowToGetThere items with information in items datatable
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  Private Sub SetHowToGetThereItems()
    Dim _idx_row As Integer

    _idx_row = 0

    For Each _currentItem As DataRow In Me.m_how_to.HowToGetThereItems.Rows
      dg_how_to_items.AddRow()

      _idx_row = dg_how_to_items.NumRows - 1

      dg_how_to_items.Cell(_idx_row, GRID_COLUMN_ID).Value = _currentItem(CLS_HOW_TO_GET_THERE.COLUMN_HOW_TO_ITEM_ID)
      dg_how_to_items.Cell(_idx_row, GRID_COLUMN_INDEX).Value = m_how_to.HowToGetThereId
      dg_how_to_items.Cell(_idx_row, GRID_COLUMN_TITLE).Value = _currentItem(CLS_HOW_TO_GET_THERE.COLUMN_HOW_TO_ITEM_TITLE)
      dg_how_to_items.Cell(_idx_row, GRID_COLUMN_VALUE).Value = _currentItem(CLS_HOW_TO_GET_THERE.COLUMN_HOW_TO_ITEM_VALUE)
      dg_how_to_items.Cell(_idx_row, GRID_COLUMN_TYPE_VALUE).Value = _currentItem(CLS_HOW_TO_GET_THERE.COLUMN_HOW_TO_ITEM_TYPE)
      dg_how_to_items.Cell(_idx_row, GRID_COLUMN_TYPE_COMBO).Value = TransformComboValueToText(_currentItem(CLS_HOW_TO_GET_THERE.COLUMN_HOW_TO_ITEM_TYPE))

    Next

    dg_how_to_items.Redraw = True
    If dg_how_to_items.NumRows > 0 Then
      dg_how_to_items.IsSelected(0) = True
    End If

  End Sub ' SetHowToGetThereItems

  ' PURPOSE: Insert a new row in grid and in BD
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function InsertRow() As Integer

    Dim _idx_row As Integer
    Dim _str_new_how_to_item(4) As String

    _idx_row = dg_how_to_items.AddRow()
    _idx_row = dg_how_to_items.NumRows - 1
    m_new_count_id = m_new_count_id - 1

    _str_new_how_to_item(CLS_HOW_TO_GET_THERE.COLUMN_HOW_TO_ITEM_LANG) = LANG_SPANISH
    _str_new_how_to_item(CLS_HOW_TO_GET_THERE.COLUMN_HOW_TO_ITEM_ID) = m_new_count_id
    _str_new_how_to_item(CLS_HOW_TO_GET_THERE.COLUMN_HOW_TO_ITEM_TITLE) = String.Empty
    _str_new_how_to_item(CLS_HOW_TO_GET_THERE.COLUMN_HOW_TO_ITEM_VALUE) = String.Empty
    _str_new_how_to_item(CLS_HOW_TO_GET_THERE.COLUMN_HOW_TO_ITEM_TYPE) = HOW_TO_TYPE_TEL

    dg_how_to_items.Cell(_idx_row, GRID_COLUMN_ID).Value = _str_new_how_to_item(CLS_HOW_TO_GET_THERE.COLUMN_HOW_TO_ITEM_ID)
    dg_how_to_items.Cell(_idx_row, GRID_COLUMN_INDEX).Value = m_how_to.HowToGetThereId
    dg_how_to_items.Cell(_idx_row, GRID_COLUMN_TITLE).Value = _str_new_how_to_item(CLS_HOW_TO_GET_THERE.COLUMN_HOW_TO_ITEM_TITLE)
    dg_how_to_items.Cell(_idx_row, GRID_COLUMN_VALUE).Value = _str_new_how_to_item(CLS_HOW_TO_GET_THERE.COLUMN_HOW_TO_ITEM_VALUE)
    dg_how_to_items.Cell(_idx_row, GRID_COLUMN_TYPE_VALUE).Value = _str_new_how_to_item(CLS_HOW_TO_GET_THERE.COLUMN_HOW_TO_ITEM_TYPE)
    dg_how_to_items.Cell(_idx_row, GRID_COLUMN_TYPE_COMBO).Value = TransformComboValueToText(_str_new_how_to_item(CLS_HOW_TO_GET_THERE.COLUMN_HOW_TO_ITEM_TYPE))

    m_how_to = Me.DbEditedObject
    m_how_to.HowToGetThereItems.Rows.Add(_str_new_how_to_item)

    Return _idx_row

  End Function ' InsertRow

  ' PURPOSE: Delete a new row in grid and in BD
  '
  '  PARAMS:
  '     - INPUT:
  '           - _idx_row As Integer 
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub DeleteRow(ByVal _idx_row As Integer)

    Dim _dr_delete_items() As DataRow

    m_how_to = Me.DbEditedObject
    _dr_delete_items = m_how_to.HowToGetThereItems.Select("howToGetThereItemId = " & Me.dg_how_to_items.Cell(_idx_row, GRID_COLUMN_ID).Value)

    _dr_delete_items(0).Delete()


    Me.dg_how_to_items.IsSelected(_idx_row) = False
    Me.dg_how_to_items.DeleteRowFast(_idx_row)


  End Sub ' DeleteRow

  ' PURPOSE:  Check row is null
  '
  '  PARAMS:
  '     - INPUT:
  '           - _idx_row AS Integer
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Boolean
  Private Function IsNullRow(ByVal _idx_row As Integer) As Boolean

    If String.IsNullOrEmpty(dg_how_to_items.Cell(_idx_row, GRID_COLUMN_TITLE).Value) Or dg_how_to_items.Cell(_idx_row, GRID_COLUMN_TITLE).Value = "" Then
      Return True
    End If

  End Function ' IsNullRow

  ' PURPOSE:  Transform Combo Value to Text
  '
  '  PARAMS:
  '     - INPUT:
  '           - value AS Integer
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - string
  Private Function TransformComboValueToText(ByVal value As Integer) As String
    Dim resp As String = ""
    For Each entry As KeyValuePair(Of Integer, String) In _m_dict
      If entry.Key = value Then
        resp = entry.Value
      End If
    Next
    Return resp
  End Function ' TransformComboValueToText

  ' PURPOSE:  Transform Combo Text to Value
  '
  '  PARAMS:
  '     - INPUT:
  '           - text AS String
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Integer
  Private Function TransformComboTextToValue(ByVal text As String) As Integer
    Dim value As Integer
    For Each entry As KeyValuePair(Of Integer, String) In _m_dict
      If entry.Value = text Then
        value = entry.Key
      End If
    Next

    Return value
  End Function ' TransformValueToText

#End Region ' private Functions

#Region "Events"

  Private Sub btn_items_add_ClickEvent() Handles btn_items_add.ClickEvent
    Dim _idx_row As Integer

    ' Check Permissions
    If Not Permissions.Write Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Exit Sub
    End If

    _idx_row = InsertRow()

    ' Focus on Row added
    dg_how_to_items.ClearSelection()
    dg_how_to_items.IsSelected(_idx_row) = True
    dg_how_to_items.Redraw = True
    dg_how_to_items.TopRow = _idx_row
    m_new_items.Push(_idx_row)

    Call GUI_GetScreenData()
  End Sub

  Private Sub btn_items_del_ClickEvent() Handles btn_items_del.ClickEvent
    Dim _idx_row As Integer

    ' Check Permissions
    If Not Permissions.Delete Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Exit Sub
    End If

    ' Check are selected a row
    If Not Me.dg_how_to_items.SelectedRows Is Nothing AndAlso Me.dg_how_to_items.SelectedRows.Length > 0 Then
      _idx_row = Me.dg_how_to_items.SelectedRows.GetValue(0)

      'Check are new item for delete
      'If m_new_items.Contains(_idx_row) Then
      DeleteRow(_idx_row)
      'End If

    End If

    ' Select previous Row
    If dg_how_to_items.NumRows > 0 Then
      If _idx_row > dg_how_to_items.NumRows - 1 Then
        _idx_row = dg_how_to_items.NumRows - 1
      End If

      dg_how_to_items.ClearSelection()
      dg_how_to_items.IsSelected(_idx_row) = True
      dg_how_to_items.Redraw = True
    End If

    Call GUI_GetScreenData()
  End Sub

  ' PURPOSE: Handler to control cells changes  in grid HowToGetThereItems
  '           - Save Denomination value in hide column and apply format in visible denomination column  
  '
  '  PARAMS:
  '     - INPUT:
  '           - Index Row
  '           - Index Col
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub dg_how_to_items_CellDataChange(ByVal Row As Integer, ByVal Column As Integer) Handles dg_how_to_items.CellDataChangedEvent

    RemoveHandler dg_how_to_items.CellDataChangedEvent, AddressOf dg_how_to_items_CellDataChange

    If dg_how_to_items Is Nothing OrElse dg_how_to_items.NumRows <= 0 Then
      Exit Sub
    End If

    dg_how_to_items.Cell(Row, GRID_COLUMN_TITLE).Value = IIf(String.IsNullOrEmpty(dg_how_to_items.Cell(Row, GRID_COLUMN_TITLE).Value), "", dg_how_to_items.Cell(Row, GRID_COLUMN_TITLE).Value)
    dg_how_to_items.Cell(Row, GRID_COLUMN_VALUE).Value = IIf(String.IsNullOrEmpty(dg_how_to_items.Cell(Row, GRID_COLUMN_VALUE).Value), "", dg_how_to_items.Cell(Row, GRID_COLUMN_VALUE).Value)
    dg_how_to_items.Cell(Row, GRID_COLUMN_TYPE_COMBO).Value = IIf(String.IsNullOrEmpty(dg_how_to_items.Cell(Row, GRID_COLUMN_TYPE_COMBO).Value), "", dg_how_to_items.Cell(Row, GRID_COLUMN_TYPE_COMBO).Value)
    dg_how_to_items.Cell(Row, GRID_COLUMN_TYPE_VALUE).Value = TransformComboTextToValue(dg_how_to_items.Cell(Row, GRID_COLUMN_TYPE_COMBO).Value)


    Call GUI_GetScreenData()



    AddHandler dg_how_to_items.CellDataChangedEvent, AddressOf dg_how_to_items_CellDataChange
  End Sub ' dg_how_to_items_CellDataChange

  ' PURPOSE: Handler to control selected row in the grid
  '          - Unpaint column color to view the real color
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub dg_how_to_items_RowSelect(ByVal SelectedRow As Integer) Handles dg_how_to_items.RowSelectedEvent

    If dg_how_to_items Is Nothing OrElse dg_how_to_items.NumRows <= 0 Then
      Exit Sub
    End If

    Try

      If SelectedRow >= 0 And dg_how_to_items.NumRows >= 0 Then

        dg_how_to_items.Redraw = False
        dg_how_to_items.Redraw = True

      End If
    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)

    End Try
  End Sub ' dg_how_to_items_RowSelect

  ' PURPOSE: Handler to control key pressed intro in the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    Select Case e.KeyChar
      Case Chr(Keys.Enter)

        If dg_how_to_items.ContainsFocus Then
          dg_how_to_items.KeyPressed(sender, e)
        End If

        Return True
      Case Chr(Keys.Escape)

        Return MyBase.GUI_KeyPress(sender, e)
      Case Else
    End Select

    ' The keypress event is done in uc_grid control

  End Function ' GUI_KeyPress

#End Region 'Events


End Class