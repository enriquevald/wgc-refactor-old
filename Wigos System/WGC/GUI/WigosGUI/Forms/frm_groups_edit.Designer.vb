<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_groups_edit
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Dim ListViewItem2 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("")
    Dim ListViewItem1 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("")
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_groups_edit))
    Me.ef_group_name = New GUI_Controls.uc_entry_field
    Me.xElements = New System.Windows.Forms.Label
    Me.xTerminals = New System.Windows.Forms.Label
    Me.rtb_description = New System.Windows.Forms.RichTextBox
    Me.xDescription = New System.Windows.Forms.Label
    Me.btn_add = New System.Windows.Forms.Button
    Me.btn_rmv = New System.Windows.Forms.Button
    Me.btn_rvm_all = New System.Windows.Forms.Button
    Me.lv_terminals = New System.Windows.Forms.ListView
    Me.ColumnHeader = New System.Windows.Forms.ColumnHeader
    Me.lv_elements = New System.Windows.Forms.ListView
    Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
    Me.xDataElements = New System.Windows.Forms.Label
    Me.xDataTerminals = New System.Windows.Forms.Label
    Me.gb_enabled = New System.Windows.Forms.GroupBox
    Me.opt_disabled = New System.Windows.Forms.RadioButton
    Me.opt_enabled = New System.Windows.Forms.RadioButton
    Me.uc_group_list = New GUI_Controls.uc_terminals_group_filter
    Me.gb_status = New System.Windows.Forms.GroupBox
    Me.lbl_inserted = New System.Windows.Forms.Label
    Me.lbl_disabled = New System.Windows.Forms.Label
    Me.lbl_enabled = New System.Windows.Forms.Label
    Me.panel_data.SuspendLayout()
    Me.gb_enabled.SuspendLayout()
    Me.gb_status.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_status)
    Me.panel_data.Controls.Add(Me.uc_group_list)
    Me.panel_data.Controls.Add(Me.gb_enabled)
    Me.panel_data.Controls.Add(Me.xDataTerminals)
    Me.panel_data.Controls.Add(Me.xDataElements)
    Me.panel_data.Controls.Add(Me.lv_elements)
    Me.panel_data.Controls.Add(Me.lv_terminals)
    Me.panel_data.Controls.Add(Me.btn_rvm_all)
    Me.panel_data.Controls.Add(Me.btn_rmv)
    Me.panel_data.Controls.Add(Me.btn_add)
    Me.panel_data.Controls.Add(Me.rtb_description)
    Me.panel_data.Controls.Add(Me.xDescription)
    Me.panel_data.Controls.Add(Me.xTerminals)
    Me.panel_data.Controls.Add(Me.xElements)
    Me.panel_data.Controls.Add(Me.ef_group_name)
    Me.panel_data.Size = New System.Drawing.Size(844, 648)
    '
    'ef_group_name
    '
    Me.ef_group_name.DoubleValue = 0
    Me.ef_group_name.IntegerValue = 0
    Me.ef_group_name.IsReadOnly = False
    Me.ef_group_name.Location = New System.Drawing.Point(31, 3)
    Me.ef_group_name.Name = "ef_group_name"
    Me.ef_group_name.Size = New System.Drawing.Size(278, 24)
    Me.ef_group_name.SufixText = "Sufix Text"
    Me.ef_group_name.SufixTextVisible = True
    Me.ef_group_name.TabIndex = 0
    Me.ef_group_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_group_name.TextValue = ""
    Me.ef_group_name.TextWidth = 50
    Me.ef_group_name.Value = ""
    '
    'xElements
    '
    Me.xElements.AutoSize = True
    Me.xElements.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.xElements.Location = New System.Drawing.Point(483, 99)
    Me.xElements.Name = "xElements"
    Me.xElements.Size = New System.Drawing.Size(75, 14)
    Me.xElements.TabIndex = 4
    Me.xElements.Text = "xElements"
    '
    'xTerminals
    '
    Me.xTerminals.AutoSize = True
    Me.xTerminals.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.xTerminals.Location = New System.Drawing.Point(685, 99)
    Me.xTerminals.Name = "xTerminals"
    Me.xTerminals.Size = New System.Drawing.Size(80, 14)
    Me.xTerminals.TabIndex = 5
    Me.xTerminals.Text = "xTerminals"
    '
    'rtb_description
    '
    Me.rtb_description.Location = New System.Drawing.Point(424, 8)
    Me.rtb_description.MaxLength = 255
    Me.rtb_description.Name = "rtb_description"
    Me.rtb_description.Size = New System.Drawing.Size(415, 85)
    Me.rtb_description.TabIndex = 3
    Me.rtb_description.Text = ""
    '
    'xDescription
    '
    Me.xDescription.Location = New System.Drawing.Point(345, 11)
    Me.xDescription.Name = "xDescription"
    Me.xDescription.Size = New System.Drawing.Size(73, 14)
    Me.xDescription.TabIndex = 9
    Me.xDescription.Text = "xDescription"
    '
    'btn_add
    '
    Me.btn_add.Font = New System.Drawing.Font("Agency FB", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btn_add.Location = New System.Drawing.Point(374, 236)
    Me.btn_add.Name = "btn_add"
    Me.btn_add.Size = New System.Drawing.Size(45, 45)
    Me.btn_add.TabIndex = 5
    Me.btn_add.Text = ">"
    Me.btn_add.UseVisualStyleBackColor = True
    '
    'btn_rmv
    '
    Me.btn_rmv.Font = New System.Drawing.Font("Agency FB", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btn_rmv.Location = New System.Drawing.Point(374, 281)
    Me.btn_rmv.Name = "btn_rmv"
    Me.btn_rmv.Size = New System.Drawing.Size(45, 45)
    Me.btn_rmv.TabIndex = 6
    Me.btn_rmv.Text = "<"
    Me.btn_rmv.UseVisualStyleBackColor = True
    '
    'btn_rvm_all
    '
    Me.btn_rvm_all.Font = New System.Drawing.Font("Agency FB", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btn_rvm_all.Location = New System.Drawing.Point(374, 326)
    Me.btn_rvm_all.Margin = New System.Windows.Forms.Padding(0)
    Me.btn_rvm_all.Name = "btn_rvm_all"
    Me.btn_rvm_all.Size = New System.Drawing.Size(45, 45)
    Me.btn_rvm_all.TabIndex = 7
    Me.btn_rvm_all.Text = "<<"
    Me.btn_rvm_all.UseVisualStyleBackColor = True
    '
    'lv_terminals
    '
    Me.lv_terminals.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader})
    Me.lv_terminals.FullRowSelect = True
    Me.lv_terminals.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
    Me.lv_terminals.HideSelection = False
    Me.lv_terminals.Items.AddRange(New System.Windows.Forms.ListViewItem() {ListViewItem2})
    Me.lv_terminals.Location = New System.Drawing.Point(635, 119)
    Me.lv_terminals.Margin = New System.Windows.Forms.Padding(0)
    Me.lv_terminals.MultiSelect = False
    Me.lv_terminals.Name = "lv_terminals"
    Me.lv_terminals.ShowGroups = False
    Me.lv_terminals.Size = New System.Drawing.Size(204, 360)
    Me.lv_terminals.TabIndex = 9
    Me.lv_terminals.UseCompatibleStateImageBehavior = False
    Me.lv_terminals.View = System.Windows.Forms.View.Details
    '
    'ColumnHeader
    '
    Me.ColumnHeader.Text = ""
    Me.ColumnHeader.Width = 184
    '
    'lv_elements
    '
    Me.lv_elements.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
    Me.lv_elements.FullRowSelect = True
    Me.lv_elements.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
    Me.lv_elements.HideSelection = False
    Me.lv_elements.Items.AddRange(New System.Windows.Forms.ListViewItem() {ListViewItem1})
    Me.lv_elements.Location = New System.Drawing.Point(424, 119)
    Me.lv_elements.Margin = New System.Windows.Forms.Padding(0)
    Me.lv_elements.MultiSelect = False
    Me.lv_elements.Name = "lv_elements"
    Me.lv_elements.ShowGroups = False
    Me.lv_elements.Size = New System.Drawing.Size(204, 360)
    Me.lv_elements.TabIndex = 8
    Me.lv_elements.UseCompatibleStateImageBehavior = False
    Me.lv_elements.View = System.Windows.Forms.View.Details
    '
    'ColumnHeader1
    '
    Me.ColumnHeader1.Text = ""
    Me.ColumnHeader1.Width = 184
    '
    'xDataElements
    '
    Me.xDataElements.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.xDataElements.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
    Me.xDataElements.Location = New System.Drawing.Point(421, 491)
    Me.xDataElements.Name = "xDataElements"
    Me.xDataElements.Size = New System.Drawing.Size(207, 150)
    Me.xDataElements.TabIndex = 15
    Me.xDataElements.Text = "xDataElements"
    '
    'xDataTerminals
    '
    Me.xDataTerminals.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.xDataTerminals.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
    Me.xDataTerminals.Location = New System.Drawing.Point(632, 491)
    Me.xDataTerminals.Name = "xDataTerminals"
    Me.xDataTerminals.Size = New System.Drawing.Size(207, 150)
    Me.xDataTerminals.TabIndex = 16
    Me.xDataTerminals.Text = "xDataTerminals"
    '
    'gb_enabled
    '
    Me.gb_enabled.Controls.Add(Me.opt_disabled)
    Me.gb_enabled.Controls.Add(Me.opt_enabled)
    Me.gb_enabled.Location = New System.Drawing.Point(206, 32)
    Me.gb_enabled.Name = "gb_enabled"
    Me.gb_enabled.Size = New System.Drawing.Size(103, 69)
    Me.gb_enabled.TabIndex = 1
    Me.gb_enabled.TabStop = False
    Me.gb_enabled.Text = "xEnabled"
    '
    'opt_disabled
    '
    Me.opt_disabled.Location = New System.Drawing.Point(15, 42)
    Me.opt_disabled.Name = "opt_disabled"
    Me.opt_disabled.Size = New System.Drawing.Size(70, 16)
    Me.opt_disabled.TabIndex = 1
    Me.opt_disabled.Text = "xNo"
    '
    'opt_enabled
    '
    Me.opt_enabled.Location = New System.Drawing.Point(15, 19)
    Me.opt_enabled.Name = "opt_enabled"
    Me.opt_enabled.Size = New System.Drawing.Size(70, 16)
    Me.opt_enabled.TabIndex = 0
    Me.opt_enabled.Text = "xYes"
    '
    'uc_group_list
    '
    Me.uc_group_list.ForbiddenGroups = CType(resources.GetObject("uc_group_list.ForbiddenGroups"), Microsoft.VisualBasic.Collection)
    Me.uc_group_list.HeightOnExpanded = 300
    Me.uc_group_list.Location = New System.Drawing.Point(6, 110)
    Me.uc_group_list.MinimumSize = New System.Drawing.Size(250, 200)
    Me.uc_group_list.Name = "uc_group_list"
    Me.uc_group_list.SetDisplayMode = GUI_Controls.uc_terminals_group_filter.DisplayMode.NotCollapsedWithoutButtons
    Me.uc_group_list.ShowGroupBoxLine = True
    Me.uc_group_list.Size = New System.Drawing.Size(362, 531)
    Me.uc_group_list.TabIndex = 4
    Me.uc_group_list.ThreeStateCheckMode = False
    '
    'gb_status
    '
    Me.gb_status.Controls.Add(Me.lbl_inserted)
    Me.gb_status.Controls.Add(Me.lbl_disabled)
    Me.gb_status.Controls.Add(Me.lbl_enabled)
    Me.gb_status.Location = New System.Drawing.Point(31, 32)
    Me.gb_status.Name = "gb_status"
    Me.gb_status.Size = New System.Drawing.Size(141, 69)
    Me.gb_status.TabIndex = 110
    Me.gb_status.TabStop = False
    Me.gb_status.Text = "xStatus"
    '
    'lbl_inserted
    '
    Me.lbl_inserted.AutoSize = True
    Me.lbl_inserted.CausesValidation = False
    Me.lbl_inserted.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_inserted.Location = New System.Drawing.Point(6, 49)
    Me.lbl_inserted.Name = "lbl_inserted"
    Me.lbl_inserted.Size = New System.Drawing.Size(71, 13)
    Me.lbl_inserted.TabIndex = 113
    Me.lbl_inserted.Text = "xInserted"
    '
    'lbl_disabled
    '
    Me.lbl_disabled.AutoSize = True
    Me.lbl_disabled.ForeColor = System.Drawing.Color.Red
    Me.lbl_disabled.Location = New System.Drawing.Point(6, 34)
    Me.lbl_disabled.Name = "lbl_disabled"
    Me.lbl_disabled.Size = New System.Drawing.Size(63, 13)
    Me.lbl_disabled.TabIndex = 112
    Me.lbl_disabled.Text = "xDisabled"
    '
    'lbl_enabled
    '
    Me.lbl_enabled.AutoSize = True
    Me.lbl_enabled.Location = New System.Drawing.Point(6, 18)
    Me.lbl_enabled.Name = "lbl_enabled"
    Me.lbl_enabled.Size = New System.Drawing.Size(59, 13)
    Me.lbl_enabled.TabIndex = 111
    Me.lbl_enabled.Text = "xEnabled"
    '
    'frm_groups_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(946, 657)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_groups_edit"
    Me.Text = "frm_groups_edit"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.gb_enabled.ResumeLayout(False)
    Me.gb_status.ResumeLayout(False)
    Me.gb_status.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_group_name As GUI_Controls.uc_entry_field
  Friend WithEvents xTerminals As System.Windows.Forms.Label
  Friend WithEvents xElements As System.Windows.Forms.Label
  Friend WithEvents rtb_description As System.Windows.Forms.RichTextBox
  Friend WithEvents xDescription As System.Windows.Forms.Label
  Friend WithEvents btn_rvm_all As System.Windows.Forms.Button
  Friend WithEvents btn_rmv As System.Windows.Forms.Button
  Friend WithEvents btn_add As System.Windows.Forms.Button
  Friend WithEvents lv_terminals As System.Windows.Forms.ListView
  Friend WithEvents ColumnHeader As System.Windows.Forms.ColumnHeader
  Friend WithEvents lv_elements As System.Windows.Forms.ListView
  Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
  Friend WithEvents xDataElements As System.Windows.Forms.Label
  Friend WithEvents xDataTerminals As System.Windows.Forms.Label
  Friend WithEvents gb_enabled As System.Windows.Forms.GroupBox
  Friend WithEvents opt_disabled As System.Windows.Forms.RadioButton
  Friend WithEvents opt_enabled As System.Windows.Forms.RadioButton
  Friend WithEvents uc_group_list As GUI_Controls.uc_terminals_group_filter
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_inserted As System.Windows.Forms.Label
  Friend WithEvents lbl_disabled As System.Windows.Forms.Label
  Friend WithEvents lbl_enabled As System.Windows.Forms.Label
End Class
