'-------------------------------------------------------------------
' Copyright � 2011 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_accounts_adjustment
' DESCRIPTION:   Accounts selection for adjustment
' AUTHOR:        Julio Andr�s
' CREATION DATE: 17-OCT-2011
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 17-OCT-2011  JCA    Initial version
' 27-AUG-2012  RRB    Added reasons and details fields.
' 08-JAN-2013  RCI    Fixed bug #477
' 06-FEB-2013  RRB    Added Points Status radiobutton and combo.
' 05-MAR-2013  ICS    Fixed Bug #631: Validate format of points field
' 27-MAR-2013  LEM    Fixed Bug #674: Max number of characters on entryfields. Validate format of reasons field.
' 22-OCT-2013  LEM    Unify saving account and cashier movements using SharedMovementsTables clases
' 23-NOV-2015  CPC    Reason is mandatory
' 06-JAN-2016  SGB    Backlog Item 7910: Change column AC_POINTS to bucket 1.
' 03-FEB-2016  ETP    FIXED BUG 8941 NLS And bucket movement are incorrect
' 15-FEB-2016  JRC    PBI 7912 Add-Sub-Set Buckets values.
' 22-FEB-2016  FAV    Fixed Bug 9713: unhandled exception error on Accounts
' 22-FEB-2016  ESE    Bug 9543:Garcia River-04: fix pop up order
' 24-FEB-2016  ESE    Bug 9544:Garcia River-04: Wrong message if its only 1 count
' 24-FEB-2016  FGB    If changing points, show message if not bucket or bucket operation selected
' 29-MAR-2016  SMN    Bug 10956:GUI: Resumen de Cuentas - Modificar cuentas clientes. Mensaje Repetido
' 12-APR-2016  JRC    Bug 10438:BUCKETS: defect/mejora en Modificar Cuentas Clientes. Make ef_points wider 
' 23-NOV-2015  CPC    Reason is mandatory
' 25-FEB-2016  ESE    Bug 9544:Garcia River-04: Wrong second message if its only 1 count to being modified
' 29-MAR-2016  SMN    Bug 10956:GUI: Resumen de Cuentas - Modificar cuentas clientes. Mensaje Repetido
' 12-APR-2016  JRC    Bug 10438:BUCKETS: defect/mejora en Modificar Cuentas Clientes. Make ef_points wider 
' 23-NOV-2015  CPC    Reason is mandatory
' 25-FEB-2016  ESE    Bug 9544:Garcia River-04: Wrong second message if its only 1 count to being modified
' 29-MAR-2016  SMN    Bug 10956:GUI: Resumen de Cuentas - Modificar cuentas clientes. Mensaje Repetido
' 12-APR-2016  JRC    Bug 10438:BUCKETS: defect/mejora en Modificar Cuentas Clientes. Make ef_points wider 
' 02-AGO-2016  JRC    PBI 16257:Buckets - RankingLevelPoints_Generated - RankingLevelPoints_Discretional
' 22-SEP-2016  FJC    Fixed Bug 18019:PromoBOX: no se muestran los puntos negativos de la cuenta de un cliente
' 12-JUL-2018  FOS    Fixed Bug 33441:WIGOS-12444 [Ticket #14705] Bajar categor�a socio
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

#Region "Imports"

Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports WSI.Common
Imports System.Text

#End Region  ' Imports

Public Class frm_accounts_adjustment
  Inherits frm_base_edit

#Region "Windows Form Designer generated code"

  Friend WithEvents lbl_message_selected_accounts As GUI_Controls.uc_text_field
  Friend WithEvents ef_details As System.Windows.Forms.TextBox
  Friend WithEvents ef_reasons As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_details As GUI_Controls.uc_text_field
  Friend WithEvents gb_adjustment_type As System.Windows.Forms.GroupBox
  Friend WithEvents opt_adjust_points As System.Windows.Forms.RadioButton
  Friend WithEvents opt_adjust_level As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_buckets As GUI_Controls.uc_combo
  Friend WithEvents ef_points As GUI_Controls.uc_entry_field
  Friend WithEvents cmb_levels As GUI_Controls.uc_combo
  Friend WithEvents opt_points_status As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_points_status As GUI_Controls.uc_combo
  Friend WithEvents cmb_operation As GUI_Controls.uc_combo
  Friend WithEvents lbl_message_cards_same_level As GUI_Controls.uc_text_field
  Friend WithEvents lbl_message_none_buckets_enabled As GUI_Controls.uc_text_field

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()
  End Sub

  Private Sub InitializeComponent()
    Me.gb_adjustment_type = New System.Windows.Forms.GroupBox()
    Me.lbl_message_none_buckets_enabled = New GUI_Controls.uc_text_field()
    Me.cmb_operation = New GUI_Controls.uc_combo()
    Me.cmb_points_status = New GUI_Controls.uc_combo()
    Me.opt_points_status = New System.Windows.Forms.RadioButton()
    Me.ef_details = New System.Windows.Forms.TextBox()
    Me.lbl_details = New GUI_Controls.uc_text_field()
    Me.ef_reasons = New GUI_Controls.uc_entry_field()
    Me.lbl_message_selected_accounts = New GUI_Controls.uc_text_field()
    Me.lbl_message_cards_same_level = New GUI_Controls.uc_text_field()
    Me.cmb_levels = New GUI_Controls.uc_combo()
    Me.ef_points = New GUI_Controls.uc_entry_field()
    Me.cmb_buckets = New GUI_Controls.uc_combo()
    Me.opt_adjust_level = New System.Windows.Forms.RadioButton()
    Me.opt_adjust_points = New System.Windows.Forms.RadioButton()
    Me.panel_data.SuspendLayout()
    Me.gb_adjustment_type.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_adjustment_type)
    Me.panel_data.Size = New System.Drawing.Size(527, 370)
    '
    'gb_adjustment_type
    '
    Me.gb_adjustment_type.Controls.Add(Me.lbl_message_none_buckets_enabled)
    Me.gb_adjustment_type.Controls.Add(Me.cmb_operation)
    Me.gb_adjustment_type.Controls.Add(Me.cmb_points_status)
    Me.gb_adjustment_type.Controls.Add(Me.opt_points_status)
    Me.gb_adjustment_type.Controls.Add(Me.ef_details)
    Me.gb_adjustment_type.Controls.Add(Me.lbl_details)
    Me.gb_adjustment_type.Controls.Add(Me.ef_reasons)
    Me.gb_adjustment_type.Controls.Add(Me.lbl_message_selected_accounts)
    Me.gb_adjustment_type.Controls.Add(Me.lbl_message_cards_same_level)
    Me.gb_adjustment_type.Controls.Add(Me.cmb_levels)
    Me.gb_adjustment_type.Controls.Add(Me.ef_points)
    Me.gb_adjustment_type.Controls.Add(Me.cmb_buckets)
    Me.gb_adjustment_type.Controls.Add(Me.opt_adjust_level)
    Me.gb_adjustment_type.Controls.Add(Me.opt_adjust_points)
    Me.gb_adjustment_type.Location = New System.Drawing.Point(3, 3)
    Me.gb_adjustment_type.Name = "gb_adjustment_type"
    Me.gb_adjustment_type.Size = New System.Drawing.Size(521, 364)
    Me.gb_adjustment_type.TabIndex = 0
    Me.gb_adjustment_type.TabStop = False
    Me.gb_adjustment_type.Text = "xType"
    '
    'lbl_message_none_buckets_enabled
    '
    Me.lbl_message_none_buckets_enabled.AutoSize = True
    Me.lbl_message_none_buckets_enabled.IsReadOnly = True
    Me.lbl_message_none_buckets_enabled.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_message_none_buckets_enabled.LabelForeColor = System.Drawing.Color.Red
    Me.lbl_message_none_buckets_enabled.Location = New System.Drawing.Point(35, 52)
    Me.lbl_message_none_buckets_enabled.Name = "lbl_message_none_buckets_enabled"
    Me.lbl_message_none_buckets_enabled.Size = New System.Drawing.Size(454, 24)
    Me.lbl_message_none_buckets_enabled.SufixText = "Sufix Text"
    Me.lbl_message_none_buckets_enabled.SufixTextVisible = True
    Me.lbl_message_none_buckets_enabled.TabIndex = 13
    Me.lbl_message_none_buckets_enabled.TabStop = False
    Me.lbl_message_none_buckets_enabled.TextVisible = False
    Me.lbl_message_none_buckets_enabled.TextWidth = 0
    Me.lbl_message_none_buckets_enabled.Value = "xThere are no buckets enabled"
    '
    'cmb_operation
    '
    Me.cmb_operation.AllowUnlistedValues = False
    Me.cmb_operation.AutoCompleteMode = False
    Me.cmb_operation.IsReadOnly = False
    Me.cmb_operation.Location = New System.Drawing.Point(308, 24)
    Me.cmb_operation.Name = "cmb_operation"
    Me.cmb_operation.SelectedIndex = -1
    Me.cmb_operation.Size = New System.Drawing.Size(85, 24)
    Me.cmb_operation.SufixText = "Sufix Text"
    Me.cmb_operation.SufixTextVisible = True
    Me.cmb_operation.TabIndex = 2
    Me.cmb_operation.TextCombo = Nothing
    Me.cmb_operation.TextVisible = False
    Me.cmb_operation.TextWidth = 0
    '
    'cmb_points_status
    '
    Me.cmb_points_status.AllowUnlistedValues = False
    Me.cmb_points_status.AutoCompleteMode = False
    Me.cmb_points_status.IsReadOnly = False
    Me.cmb_points_status.Location = New System.Drawing.Point(151, 141)
    Me.cmb_points_status.Name = "cmb_points_status"
    Me.cmb_points_status.SelectedIndex = -1
    Me.cmb_points_status.Size = New System.Drawing.Size(153, 24)
    Me.cmb_points_status.SufixText = "Sufix Text"
    Me.cmb_points_status.SufixTextVisible = True
    Me.cmb_points_status.TabIndex = 8
    Me.cmb_points_status.TextCombo = Nothing
    Me.cmb_points_status.TextVisible = False
    Me.cmb_points_status.TextWidth = 0
    '
    'opt_points_status
    '
    Me.opt_points_status.AutoSize = True
    Me.opt_points_status.Location = New System.Drawing.Point(22, 145)
    Me.opt_points_status.Name = "opt_points_status"
    Me.opt_points_status.Size = New System.Drawing.Size(117, 17)
    Me.opt_points_status.TabIndex = 7
    Me.opt_points_status.TabStop = True
    Me.opt_points_status.Text = "xRedeem Points"
    Me.opt_points_status.UseVisualStyleBackColor = True
    '
    'ef_details
    '
    Me.ef_details.Location = New System.Drawing.Point(104, 218)
    Me.ef_details.Multiline = True
    Me.ef_details.Name = "ef_details"
    Me.ef_details.Size = New System.Drawing.Size(330, 107)
    Me.ef_details.TabIndex = 11
    '
    'lbl_details
    '
    Me.lbl_details.IsReadOnly = True
    Me.lbl_details.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_RIGTH
    Me.lbl_details.LabelForeColor = System.Drawing.Color.Black
    Me.lbl_details.Location = New System.Drawing.Point(19, 220)
    Me.lbl_details.Name = "lbl_details"
    Me.lbl_details.Size = New System.Drawing.Size(85, 24)
    Me.lbl_details.SufixText = "Sufix Text"
    Me.lbl_details.SufixTextVisible = True
    Me.lbl_details.TabIndex = 10
    Me.lbl_details.TextWidth = 0
    Me.lbl_details.Value = "xDetails"
    '
    'ef_reasons
    '
    Me.ef_reasons.DoubleValue = 0.0R
    Me.ef_reasons.IntegerValue = 0
    Me.ef_reasons.IsReadOnly = False
    Me.ef_reasons.Location = New System.Drawing.Point(22, 184)
    Me.ef_reasons.Name = "ef_reasons"
    Me.ef_reasons.PlaceHolder = Nothing
    Me.ef_reasons.Size = New System.Drawing.Size(412, 24)
    Me.ef_reasons.SufixText = "SufixText"
    Me.ef_reasons.SufixTextVisible = True
    Me.ef_reasons.TabIndex = 9
    Me.ef_reasons.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_reasons.TextValue = ""
    Me.ef_reasons.Value = ""
    Me.ef_reasons.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_message_selected_accounts
    '
    Me.lbl_message_selected_accounts.AutoSize = True
    Me.lbl_message_selected_accounts.IsReadOnly = True
    Me.lbl_message_selected_accounts.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_message_selected_accounts.LabelForeColor = System.Drawing.SystemColors.ControlText
    Me.lbl_message_selected_accounts.Location = New System.Drawing.Point(19, 333)
    Me.lbl_message_selected_accounts.Name = "lbl_message_selected_accounts"
    Me.lbl_message_selected_accounts.Size = New System.Drawing.Size(454, 24)
    Me.lbl_message_selected_accounts.SufixText = "Sufix Text"
    Me.lbl_message_selected_accounts.SufixTextVisible = True
    Me.lbl_message_selected_accounts.TabIndex = 12
    Me.lbl_message_selected_accounts.TabStop = False
    Me.lbl_message_selected_accounts.TextVisible = False
    Me.lbl_message_selected_accounts.TextWidth = 0
    Me.lbl_message_selected_accounts.Value = "xSelected accounts"
    '
    'lbl_message_cards_same_level
    '
    Me.lbl_message_cards_same_level.AutoSize = True
    Me.lbl_message_cards_same_level.IsReadOnly = True
    Me.lbl_message_cards_same_level.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_message_cards_same_level.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_message_cards_same_level.Location = New System.Drawing.Point(35, 105)
    Me.lbl_message_cards_same_level.Name = "lbl_message_cards_same_level"
    Me.lbl_message_cards_same_level.Size = New System.Drawing.Size(454, 24)
    Me.lbl_message_cards_same_level.SufixText = "Sufix Text"
    Me.lbl_message_cards_same_level.SufixTextVisible = True
    Me.lbl_message_cards_same_level.TabIndex = 6
    Me.lbl_message_cards_same_level.TabStop = False
    Me.lbl_message_cards_same_level.TextVisible = False
    Me.lbl_message_cards_same_level.TextWidth = 0
    Me.lbl_message_cards_same_level.Value = "xCards with the same level"
    '
    'cmb_levels
    '
    Me.cmb_levels.AllowUnlistedValues = False
    Me.cmb_levels.AutoCompleteMode = False
    Me.cmb_levels.IsReadOnly = False
    Me.cmb_levels.Location = New System.Drawing.Point(151, 80)
    Me.cmb_levels.Name = "cmb_levels"
    Me.cmb_levels.SelectedIndex = -1
    Me.cmb_levels.Size = New System.Drawing.Size(153, 24)
    Me.cmb_levels.SufixText = "Sufix Text"
    Me.cmb_levels.SufixTextVisible = True
    Me.cmb_levels.TabIndex = 5
    Me.cmb_levels.TextCombo = Nothing
    Me.cmb_levels.TextVisible = False
    Me.cmb_levels.TextWidth = 0
    '
    'ef_points
    '
    Me.ef_points.DoubleValue = 0.0R
    Me.ef_points.IntegerValue = 0
    Me.ef_points.IsReadOnly = False
    Me.ef_points.Location = New System.Drawing.Point(395, 24)
    Me.ef_points.Name = "ef_points"
    Me.ef_points.PlaceHolder = Nothing
    Me.ef_points.Size = New System.Drawing.Size(120, 25)
    Me.ef_points.TabIndex = 3
    Me.ef_points.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_points.TextValue = ""
    Me.ef_points.TextWidth = 0
    Me.ef_points.Value = ""
    Me.ef_points.ValueForeColor = System.Drawing.Color.Blue
    '
    'cmb_buckets
    '
    Me.cmb_buckets.AllowUnlistedValues = False
    Me.cmb_buckets.AutoCompleteMode = False
    Me.cmb_buckets.IsReadOnly = False
    Me.cmb_buckets.Location = New System.Drawing.Point(99, 24)
    Me.cmb_buckets.Name = "cmb_buckets"
    Me.cmb_buckets.SelectedIndex = -1
    Me.cmb_buckets.Size = New System.Drawing.Size(205, 24)
    Me.cmb_buckets.SufixText = "Sufix Text"
    Me.cmb_buckets.SufixTextVisible = True
    Me.cmb_buckets.TabIndex = 1
    Me.cmb_buckets.TextCombo = Nothing
    Me.cmb_buckets.TextVisible = False
    Me.cmb_buckets.TextWidth = 0
    '
    'opt_adjust_level
    '
    Me.opt_adjust_level.AutoSize = True
    Me.opt_adjust_level.Location = New System.Drawing.Point(22, 84)
    Me.opt_adjust_level.Name = "opt_adjust_level"
    Me.opt_adjust_level.Size = New System.Drawing.Size(62, 17)
    Me.opt_adjust_level.TabIndex = 4
    Me.opt_adjust_level.TabStop = True
    Me.opt_adjust_level.Text = "xLevel"
    Me.opt_adjust_level.UseVisualStyleBackColor = True
    '
    'opt_adjust_points
    '
    Me.opt_adjust_points.AutoSize = True
    Me.opt_adjust_points.Location = New System.Drawing.Point(22, 28)
    Me.opt_adjust_points.Name = "opt_adjust_points"
    Me.opt_adjust_points.Size = New System.Drawing.Size(66, 17)
    Me.opt_adjust_points.TabIndex = 0
    Me.opt_adjust_points.TabStop = True
    Me.opt_adjust_points.Text = "xPoints"
    Me.opt_adjust_points.UseVisualStyleBackColor = True
    '
    'frm_accounts_adjustment
    '
    Me.ClientSize = New System.Drawing.Size(629, 381)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_accounts_adjustment"
    Me.Text = "frm_accounts_adjustment"
    Me.panel_data.ResumeLayout(False)
    Me.gb_adjustment_type.ResumeLayout(False)
    Me.gb_adjustment_type.PerformLayout()
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region "Structures"

  Private Structure TYPE_ACCOUNT
    Dim id As Integer
    Dim points As Decimal
    Dim level_name As String
    Dim level_number As Integer
    Dim holder_name As String
    Dim balance As Decimal
    Dim date_entered As DateTime
  End Structure

#End Region  ' Structures

#Region "Members"
  Private m_account_ids As List(Of Int64)
  Private m_bucketsList As BucketsList




#End Region ' Members

#Region "Constants"

  ' RRB 27-AUG-2012 Max length details
  Private Const MAX_DETAILS_LAYOUT_FIELDS_LENGTH = 256

#End Region  ' Constants

#Region "Overrides"

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_ACCOUNT_ADJUSTMENT
    Call MyBase.GUI_SetFormId()

  End Sub  ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_CONFIGURATION.GetString(70)

    ' Buttons
    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False

    gb_adjustment_type.Text = GLB_NLS_GUI_CONFIGURATION.GetString(73)

    ' Adjust by points
    opt_adjust_points.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7132)
    opt_adjust_points.Checked = True

    ' list of buckets names
    m_bucketsList = New BucketsList
    m_bucketsList.GetList()

    lbl_message_none_buckets_enabled.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7312)
    Call Fill_Combo_Buckets(cmb_buckets)

    ef_points.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 6, 0)

    ' Adjust by level
    opt_adjust_level.Text = GLB_NLS_GUI_CONFIGURATION.GetString(72)
    lbl_message_cards_same_level.Value = GLB_NLS_GUI_CONFIGURATION.GetString(123)
    Call Fill_Combo_Levels(cmb_levels)

    If GeneralParam.GetBoolean("Site", "MultiSiteMember", False) AndAlso _
       GeneralParam.GetInt32("MultiSite", "PlayerTracking.Mode") <> WSI.Common.PlayerTracking_Mode.Site Then
      Me.opt_adjust_level.Enabled = False
    End If

    lbl_message_selected_accounts.Value = GLB_NLS_GUI_CONFIGURATION.GetString(132) & m_account_ids.Count

    opt_points_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1645)
    Call Fill_Combo_Points_Status(cmb_points_status)

    ' RRB 27-AUG-2012
    ef_reasons.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1262)
    ef_reasons.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 64)

    lbl_details.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1263)
    ef_details.MaxLength = MAX_DETAILS_LAYOUT_FIELDS_LENGTH

  End Sub  ' GUI_InitControls

  ' PURPOSE: Shows the window
  '
  '  PARAMS:
  '     - INPUT:
  '         - AccountsIds: list of accounts identifiers
  '         - NumberAnonymousAccounts: number of anonymous accounts in previous list
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overloads Sub ShowNewItem(ByVal AccountsIds As List(Of Int64))

    m_account_ids = AccountsIds

    GUI_SetFormId()

    Call Me.Display(True)

  End Sub  ' ShowNewItem

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.ef_points
  End Sub  'GUI_SetInitialFocus

  ' PURPOSE : Check if screen data is ok before saving
  '
  '  PARAMS :
  '     -  INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '
  '   NOTES :

  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Dim nls_param1 As String
    Dim nls_param2 As String


    '   - ef_Details not empty
    If ef_reasons.Value.Trim() = "" Then
      ' 6951 "You must enter the reason."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6951), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call ef_reasons.Focus()
      Return False
    End If

    If opt_adjust_points.Checked Then
      ' Update points selection
      '   - Valid Bucket
      '   - Valid Operation
      '   - No empty points allowed
      '   - Valid format of fields when adding points
      '   - No zero points allowed when adding points

      ' No valid bucket selected
      If (cmb_buckets.Value <= 0) Then
        ' 7313 "You must select a bucket"
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7313), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call cmb_buckets.Focus()

        Return False
      End If

      ' No valid bucket operation selected
      If (cmb_operation.Value <= 0) Then
        ' 7314 "You must select a bucket operation"
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7314), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call cmb_operation.Focus()

        Return False
      End If

      '   - No empty points allowed
      If ef_points.Value = "" Then
        ' 76 "You must input the number of points."
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(76), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call ef_points.Focus()

        Return False
      End If

      '   - No zero points allowed when adding points
      If (cmb_operation.Value = 1 Or cmb_operation.Value = 2) And Convert.ToDecimal(ef_points.Value) = 0 Then
        ' 84 "Amount of points must be greater than 0."
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(84), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call ef_points.Focus()

        Return False
      End If

    ElseIf opt_adjust_level.Checked Then
      ' Update account level

      If cmb_levels.SelectedIndex = -1 Then
        ' No level was selected 
        ' 81 "Please select a level."
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(81), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call cmb_levels.Focus()

        Return False
      End If

    ElseIf opt_points_status.Checked Then
      ' Update points status

      If cmb_points_status.SelectedIndex = -1 Then
        ' No item was selected 
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(81), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call cmb_points_status.Focus()

        Return False
      End If
    End If

    '   - ef_Details not empty
    If ef_reasons.Value.Trim() = "" Then
      ' 6951 "You must enter the reason."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6951), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call ef_reasons.Focus()
      Return False
    End If

    ' LEM 27-MAR-2013: Validate format of reasons field
    If Not ef_reasons.ValidateFormat() Then
      Call ef_reasons.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
            mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, ef_reasons.Text)
      Return False
    End If

    ' RRB 27-AUG-2012: Check details layout don't exceed MAX_TICKET_LAYOUT_FIELDS_LENGTH
    If ef_details.Text.Length > MAX_DETAILS_LAYOUT_FIELDS_LENGTH Then
      nls_param1 = lbl_details.Value
      nls_param2 = MAX_DETAILS_LAYOUT_FIELDS_LENGTH

      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(423), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , nls_param1, nls_param2)
      Call ef_details.Focus()

      Return False
    End If

    Return True

  End Function ' GUI_IsScreenDataOk

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)
    Dim _id_NLS As Integer
    Dim _param1_error As String = ""

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CANCEL
        MyBase.GUI_ButtonClick(ButtonId)

      Case ENUM_BUTTON.BUTTON_OK
        'Check previously if the data is OK
        If Not GUI_IsScreenDataOk() Then
          Return
        End If

        'Check if its 1 ore more counts to be modified
        If m_account_ids.Count > 1 Then
          'More than 1 count      
          _id_NLS = 60            '60 "Selected accounts will be modified.\n\nDo you want to continue?"
        Else
          'Only 1 count           
          _id_NLS = 94            '94 "Selected account will be modified.\n\nDo you want to continue?"
        End If

        '60 "Selected accounts will be modified.\n\nDo you want to continue?"
        '94 "Selected account will be modified.\n\nDo you want to continue?"
        If NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(_id_NLS), _
                      mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
                      ENUM_MB_BTN.MB_BTN_YES_NO, _
                      ENUM_MB_DEF_BTN.MB_DEF_BTN_2) <> ENUM_MB_RESULT.MB_RESULT_YES Then

          Return
        End If

        If Not DB_AdjustAccounts(_id_NLS, _param1_error) Then
          If (_id_NLS <> 0) Then
            ' If _id_NLS not equal to 0, it it the message of the error, and the _param1_error has the AccountID 
            Call NLS_MsgBox(_id_NLS, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _param1_error)
          Else
            ' 82 "An error occurred updating the Database."
            _id_NLS = GLB_NLS_GUI_CONFIGURATION.Id(82)
            Call NLS_MsgBox(_id_NLS, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)
          End If

          Return
        End If

        _id_NLS = 0
        If m_account_ids.Count > 1 Then
          'More than 1 count
          _id_NLS = 133
        Else
          'Only 1 count
          _id_NLS = 95
        End If
        ' 133 "All selected accounts have been successfully modified."
        ' 95  "Selected account have been successfully modified."
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(_id_NLS), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO)

        Me.Close()

      Case Else

    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Ignore [ENTER] key when editing details
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean
    ' RRB 27-AUG-2012
    ' The keypress event is done in ef_details (textbox control)
    If e.KeyChar = Chr(Keys.Enter) Then
      If Me.ef_details.ContainsFocus Then
        Return True
      End If
    End If

    Return MyBase.GUI_KeyPress(sender, e)

  End Function ' GUI_KeyPress

#End Region ' Overrides

#Region "Public Functions"

#End Region ' Public Functions

#Region "Private Functions"

  ' PURPOSE : Fill the combo box of points
  '
  '  PARAMS:
  '     - INPUT:
  '         - Combo: control to be filled
  '     - OUTPUT:
  '
  ' RETURNS :
  Private Sub Fill_Combo_Buckets(ByVal Combo As uc_combo)

    Dim _id As Integer
    Dim _buckets_form As BucketsForm
    Dim _properties As Bucket_Properties
    Dim _can_add As Boolean
    Dim _can_subtract As Boolean
    Dim _can_set As Boolean
    Dim _bucket_description As String
    Dim _buckets_id As List(Of Long)
    Dim _only_enabled_buckets As Boolean
    Dim _include_buckets_subset_level As Boolean

    '(7028)   "Puntos de canje"
    '(7029)   "Puntos de nivel"
    '(7030)   "Cr�dito NR"
    '(7031)   "Cr�dito RE"
    '(7032)   "Comp1: Puntos de canje"
    '(7033)   "Comp2: Cr�dito NR"
    '(7034)   "Comp3: Cr�dito RE"

    _buckets_form = New BucketsForm()
    _bucket_description = ""
    _buckets_id = New List(Of Long)
    _only_enabled_buckets = True
    _include_buckets_subset_level = False
    _buckets_form.GetBucketsID(_buckets_id, _only_enabled_buckets, _include_buckets_subset_level)

    If (_buckets_id.Count > 0) Then
      For Each _id In _buckets_id
        _properties = Buckets.GetProperties(_id)
        _can_add = _properties.gui_can_add
        _can_subtract = _properties.gui_can_sub
        _can_set = _properties.gui_can_set

        If _can_add Or _can_set Or _can_subtract Then
          _bucket_description = m_bucketsList.GetBucketName(_id)
          Combo.Add(_id, _bucket_description)
        End If
      Next

    End If

    If (Combo.Count > 0) Then
      Combo.SelectedIndex = 0
    End If

    SwitchLabelMessageNoneBucketsEnabled()
  End Sub ' Fill_Combo_Buckets

  ' PURPOSE : Fill the combo box of levels
  '
  '  PARAMS:
  '     - INPUT:
  '         - Combo: control to be filled
  '     - OUTPUT:
  '
  ' RETURNS :
  Private Sub Fill_Combo_Levels(ByVal Combo As uc_combo)

    Dim _table_levels As DataTable
    Dim _str_sql As String

    _str_sql = "SELECT  CAST(SUBSTRING(GP_SUBJECT_KEY, 6, 2) AS INT) AS LevelNumber " & _
     "                , GP_KEY_VALUE " & _
     "            FROM  GENERAL_PARAMS " & _
     "           WHERE  GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY LIKE 'Level%.Name'" & _
     "           ORDER BY GP_KEY_VALUE "

    _table_levels = GUI_GetTableUsingSQL(_str_sql, Integer.MaxValue)

    If IsNothing(_table_levels) Then
      ' 80 "Error found when reading account levels in Database."
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(80), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)
      Exit Sub
    End If

    Combo.Add(_table_levels)

    Combo.SelectedIndex = 0

  End Sub ' Fill_Combo_Levels

  ' PURPOSE : Fill the combo box of points status
  '
  '  PARAMS:
  '     - INPUT:
  '         - Combo: control to be filled
  '     - OUTPUT:
  '
  ' RETURNS :
  Private Sub Fill_Combo_Points_Status(ByVal Combo As uc_combo)

    Combo.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1643))
    Combo.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1644))

    Combo.SelectedIndex = 1

  End Sub ' Fill_Combo_Points Status

  ' PURPOSE: It saves points or level changed in accounts
  '
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetAccountsWhereString() As String
    Dim _str_account_ids As String = ""

    For Each _account_id As Int64 In m_account_ids
      _str_account_ids += _account_id.ToString() + ","
    Next
    _str_account_ids = _str_account_ids.Substring(0, _str_account_ids.Length - 1)

    Return _str_account_ids

  End Function

  Private Function GetSQLUpdateBuckets(_str_selected_accounts As String) As String
    Dim _str_sql_update_bucket As StringBuilder

    '     - Update CUSTOMER_BUCKET table and use the output clause to generate the related ACCOUNT_MOVEMENTS
    _str_sql_update_bucket = New StringBuilder()
    _str_sql_update_bucket.AppendLine("DECLARE @AUX_TABLE_CUSTOMER_BUCKET table ")
    _str_sql_update_bucket.AppendLine(" (                                       ")
    _str_sql_update_bucket.AppendLine("     CUSTOMER_ID         BIGINT          ")
    _str_sql_update_bucket.AppendLine("   , INITIAL_BALANCE     MONEY           ")
    _str_sql_update_bucket.AppendLine("   , FINAL_BALANCE       MONEY           ")
    _str_sql_update_bucket.AppendLine("   , REAL_SUBTRACTED     MONEY           ")
    _str_sql_update_bucket.AppendLine(" );                                      ")

    If opt_adjust_points.Checked Then
      _str_sql_update_bucket.AppendLine("INSERT INTO   CUSTOMER_BUCKET                            ")
      _str_sql_update_bucket.AppendLine("             (CBU_CUSTOMER_ID                            ")
      _str_sql_update_bucket.AppendLine("            , CBU_BUCKET_ID                              ")
      _str_sql_update_bucket.AppendLine("            , CBU_VALUE                                  ")
      _str_sql_update_bucket.AppendLine("            , CBU_UPDATED)                               ")
      _str_sql_update_bucket.AppendLine("     SELECT   SST_VALUE                                  ")
      _str_sql_update_bucket.AppendLine("            , @pBucketId                                 ")
      _str_sql_update_bucket.AppendLine("            , 0                                          ")
      _str_sql_update_bucket.AppendLine("            , GETDATE()                                  ")
      _str_sql_update_bucket.AppendLine("       FROM   dbo.SplitStringIntoTable('" & _str_selected_accounts & "', ',', 1)")
      _str_sql_update_bucket.AppendLine("  LEFT JOIN   CUSTOMER_BUCKET                            ")
      _str_sql_update_bucket.AppendLine("         ON   CBU_CUSTOMER_ID = CONVERT(BIGINT, SST_VALUE)")
      _str_sql_update_bucket.AppendLine("        AND   CBU_BUCKET_ID = @pBucketId                 ")
      _str_sql_update_bucket.AppendLine("      WHERE   CBU_CUSTOMER_ID IS NULL                    ") 'Customer Bucket does not exist

      _str_sql_update_bucket.AppendLine("     UPDATE   CUSTOMER_BUCKET                            ")
      Select Case cmb_operation.Value
        Case 1
          ' Add Points
          _str_sql_update_bucket.AppendLine("        SET   CBU_VALUE = ISNULL(CBU_VALUE, 0) + @pPoints ")
        Case 2
          ' Subtract Points
          _str_sql_update_bucket.AppendLine("        SET   CBU_VALUE = ISNULL(CBU_VALUE, 0) - @pPoints ")
        Case 3
          ' Set Points
          _str_sql_update_bucket.AppendLine("        SET   CBU_VALUE = @pPoints                        ")
      End Select

      _str_sql_update_bucket.AppendLine("     OUTPUT   INSERTED.CBU_CUSTOMER_ID                   ")
      _str_sql_update_bucket.AppendLine("            , DELETED.CBU_VALUE                          ")
      _str_sql_update_bucket.AppendLine("            , INSERTED.CBU_VALUE                         ")
      _str_sql_update_bucket.AppendLine("            , DELETED.CBU_VALUE - INSERTED.CBU_VALUE     ")
      _str_sql_update_bucket.AppendLine("       INTO   @AUX_TABLE_CUSTOMER_BUCKET                 ")
      _str_sql_update_bucket.AppendLine("       FROM   CUSTOMER_BUCKET                            ")
      _str_sql_update_bucket.AppendLine("      WHERE   CBU_CUSTOMER_ID IN (" & _str_selected_accounts & ") ")
      _str_sql_update_bucket.AppendLine("        AND   CBU_BUCKET_ID = @pBucketId                 ")
    End If

    Return _str_sql_update_bucket.ToString()
  End Function

  Private Function GetSQLUpdateAccounts(_str_selected_accounts As String) As String
    Dim _str_sql As StringBuilder

    '     - Update ACCOUNT table and use the output clause to generate the related ACCOUNT_MOVEMENTS
    _str_sql = New StringBuilder()
    _str_sql.AppendLine("DECLARE @AUX_TABLE table ")
    _str_sql.AppendLine(" ( ")
    _str_sql.AppendLine("     ACCOUNT_ID          BIGINT        ")
    _str_sql.AppendLine("   , DT                  DATETIME      ")
    _str_sql.AppendLine("   , TYPE                INT           ")
    _str_sql.AppendLine("   , INITIAL_BALANCE     MONEY         ")
    _str_sql.AppendLine("   , SUB_AMOUNT          MONEY         ")
    _str_sql.AppendLine("   , ADD_AMOUNT          MONEY         ")
    _str_sql.AppendLine("   , FINAL_BALANCE       MONEY         ")
    _str_sql.AppendLine("   , CASHIER_NAME        NVARCHAR(50)  ")
    _str_sql.AppendLine("   , HOLDER_NAME         NVARCHAR(200) ")
    _str_sql.AppendLine(" );")
    _str_sql.AppendLine("")
    _str_sql.AppendLine(" UPDATE ACCOUNTS                       ")

    If opt_adjust_points.Checked Then

      _str_sql.AppendLine("SET AC_ACCOUNT_ID = AC_ACCOUNT_ID    ")

    ElseIf opt_adjust_level.Checked Then
      _str_sql.AppendLine("                SET AC_HOLDER_LEVEL = @pLevel                                                                                            ")
      _str_sql.AppendLine("                  , AC_HOLDER_LEVEL_EXPIRATION = @pLevelExpiration                                                                       ")
      _str_sql.AppendLine("                  , AC_HOLDER_LEVEL_NOTIFY     = ISNULL (AC_HOLDER_LEVEL_NOTIFY, 0) + (@pLevel - AC_HOLDER_LEVEL)                        ")
      _str_sql.AppendLine("                  , AC_HOLDER_LEVEL_ENTERED    = CASE WHEN (AC_HOLDER_LEVEL <> @pLevel) THEN @pDatetime ELSE AC_HOLDER_LEVEL_ENTERED END ")

    ElseIf opt_points_status.Checked Then
      _str_sql.AppendLine("                SET AC_POINTS_STATUS = @pPointsStatus    ")

    End If

    ' Define the ouput fields to be used by the audit and the insertion into the AMs
    _str_sql.AppendLine("              OUTPUT INSERTED.AC_ACCOUNT_ID              ")               ' ACCOUNT_ID     
    _str_sql.AppendLine("                   , @pDatetime                          ")               ' DT             
    _str_sql.AppendLine("                   , @pType                              ")               ' TYPE           

    If opt_adjust_points.Checked Then
      _str_sql.AppendLine("                 , 0                                   ")               ' INITIAL_BALANCE
      _str_sql.AppendLine("                 , " & IIf(cmb_operation.Value <> 3, "@pPoints", "0"))  ' ADD AMOUNT
      _str_sql.AppendLine("                 , " & IIf(cmb_operation.Value = 2, "@pPoints", "0"))   ' SUB AMOUNT
      _str_sql.AppendLine("                 , 0                                   ")               ' FINAL_BALANCE

    ElseIf opt_adjust_level.Checked Then
      _str_sql.AppendLine("                 , DELETED.AC_BALANCE                  ")               ' INITIAL_BALANCE
      _str_sql.AppendLine("                 , DELETED.AC_HOLDER_LEVEL             ")               ' SUB_AMOUNT
      _str_sql.AppendLine("                 , @pAddAmount                         ")               ' ADD_AMOUNT
      _str_sql.AppendLine("                 , DELETED.AC_BALANCE                  ")               ' FINAL_BALANCE

    ElseIf opt_points_status.Checked Then
      _str_sql.AppendLine("                 , 0                                   ")               ' INITIAL_BALANCE
      _str_sql.AppendLine("                 , ISNULL(DELETED.AC_POINTS_STATUS, 1) ")               ' ADD AMOUNT
      _str_sql.AppendLine("                 , @pPointsStatus                      ")               ' SUB_AMOUNT  (If previous value is NULL, assume is NOT_ALLOWED(1))
      _str_sql.AppendLine("                 , 0                                   ")               ' FINAL_BALANCE
    End If

    _str_sql.AppendLine("                   , @pCashierName               ")                       ' CASHIER_NAME 
    _str_sql.AppendLine("                   , DELETED.AC_HOLDER_NAME      ")                       ' HOLDER_NAME  
    _str_sql.AppendLine("              INTO @AUX_TABLE                    ")
    _str_sql.AppendLine("             WHERE AC_ACCOUNT_ID IN (" & _str_selected_accounts & ")")
    _str_sql.AppendLine("               AND AC_HOLDER_LEVEL <> 0;         ")

    _str_sql.AppendLine("      UPDATE   @AUX_TABLE                        ")
    _str_sql.AppendLine("         SET   ACCOUNT_ID = CB.CUSTOMER_ID       ")
    _str_sql.AppendLine("             , INITIAL_BALANCE = CB.INITIAL_BALANCE")

    If opt_adjust_points.Checked And cmb_operation.Value = 1 Then 'Add 
      _str_sql.AppendLine("             , ADD_AMOUNT = @pPoints           ")
      _str_sql.AppendLine("             , SUB_AMOUNT = 0                  ")
    End If

    If opt_adjust_points.Checked And cmb_operation.Value = 2 Then 'Sub
      _str_sql.AppendLine("             , ADD_AMOUNT = 0                  ")
      _str_sql.AppendLine("             , SUB_AMOUNT = CB.REAL_SUBTRACTED ")
    End If

    If opt_adjust_points.Checked And cmb_operation.Value = 3 Then 'Set
      _str_sql.AppendLine("             , ADD_AMOUNT = @pPoints           ")
      _str_sql.AppendLine("             , SUB_AMOUNT = CB.INITIAL_BALANCE ")
    End If

    _str_sql.AppendLine("             , FINAL_BALANCE = CB.FINAL_BALANCE  ")
    _str_sql.AppendLine("        FROM   @AUX_TABLE AT                     ")
    _str_sql.AppendLine("  INNER JOIN   @AUX_TABLE_CUSTOMER_BUCKET CB     ")
    _str_sql.AppendLine("          ON   CUSTOMER_ID = ACCOUNT_ID          ")

    _str_sql.AppendLine(" SELECT AT.ACCOUNT_ID                            ")
    _str_sql.AppendLine("      , AT.DT                                    ")
    _str_sql.AppendLine("      , AT.TYPE                                  ")
    _str_sql.AppendLine("      , AT.INITIAL_BALANCE                       ")
    _str_sql.AppendLine("      , AT.SUB_AMOUNT                            ")
    _str_sql.AppendLine("      , AT.ADD_AMOUNT                            ")
    _str_sql.AppendLine("      , AT.FINAL_BALANCE                         ")
    _str_sql.AppendLine("      , AT.CASHIER_NAME                          ")
    _str_sql.AppendLine("      , AT.HOLDER_NAME                           ")
    _str_sql.AppendLine("      , (SELECT GP_KEY_VALUE                     ")
    _str_sql.AppendLine("           FROM GENERAL_PARAMS                   ")
    _str_sql.AppendLine("          WHERE GP_GROUP_KEY = 'PlayerTracking'  ")
    _str_sql.AppendLine("            AND GP_SUBJECT_KEY = 'Level' + RIGHT('0' + CAST(CAST(AT.SUB_AMOUNT AS INT) AS NVARCHAR), 2) + '.Name') ")
    _str_sql.AppendLine("         AS LEVEL_NAME                           ")
    _str_sql.AppendLine("      , CB.INITIAL_BALANCE as INITIAL_BAL_BUCKET ")
    _str_sql.AppendLine("      , CB.FINAL_BALANCE as FINAL_BAL_BUCKET     ")
    _str_sql.AppendLine(" FROM   @AUX_TABLE  AT                           ")
    _str_sql.AppendLine(" LEFT JOIN   @AUX_TABLE_CUSTOMER_BUCKET CB   ON   CB.CUSTOMER_ID = AT.ACCOUNT_ID")

    Return _str_sql.ToString()
  End Function


  ''' <summary>
  ''' Saves points or level changed in accounts and their movements and audits
  ''' </summary>
  ''' <param name="ErrorIdNLS">NLS of the error message</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DB_AdjustAccounts(ByRef ErrorIdNLS As Integer, ByRef ErrorAccountId As String) As Boolean

    Dim _str_sql_update_account As String
    Dim _str_sql_update_bucket As String
    Dim _str_selected_accounts As String
    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _num_modified_accounts As Integer
    Dim _level As Int32
    Dim _points As Decimal
    Dim _points_status As Int16
    Dim _account_mov As AccountMovementsTable
    Dim _cashier_info As CashierSessionInfo

    ErrorIdNLS = 0
    ErrorAccountId = String.Empty

    Try
      Using _db_trx As New DB_TRX()
        _points = IIf(opt_adjust_points.Checked, ef_points.Value, 0)
        _level = cmb_levels.Value
        _points_status = cmb_points_status.SelectedIndex

        ' Create the list of involved accounts 
        _str_selected_accounts = GetAccountsWhereString()

        ' Get the SQL for updating ACCOUNT and/or CUSTOMER_BUCKET tables 
        _str_sql_update_bucket = GetSQLUpdateBuckets(_str_selected_accounts)
        _str_sql_update_account = GetSQLUpdateAccounts(_str_selected_accounts)

        ' Update the accounts and the customer buckets
        Using _sql_command As New SqlCommand(_str_sql_update_bucket & " " & _str_sql_update_account, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          ' Parameters for command
          _sql_command.Parameters.Add("@pDatetime", SqlDbType.DateTime).Value = WSI.Common.WGDB.Now
          _sql_command.Parameters.Add("@pCashierName", SqlDbType.NVarChar).Value = CurrentUser.Name + "@" + Environment.MachineName

          If opt_adjust_points.Checked Then
            _sql_command.Parameters.Add("@pPoints", SqlDbType.Money).Value = _points
            _sql_command.Parameters.Add("@pType", SqlDbType.Int).Value = GetMovementTypeByOperation(cmb_operation.Value)
            _sql_command.Parameters.Add("@pBucketId", SqlDbType.Int).Value = cmb_buckets.Value

          ElseIf opt_adjust_level.Checked Then
            _sql_command.Parameters.Add("@pLevel", SqlDbType.Int).Value = _level

            If _level = 1 Then
              ' Level A accounts never expire
              _sql_command.Parameters.Add("@pLevelExpiration", SqlDbType.DateTime).Value = DBNull.Value
            Else
              Dim _expiration As DateTime
              Dim _min_expiration As DateTime
              Dim _expire_on_date As Boolean

              WSI.Common.Accounts.GetMinExpiration(_expire_on_date, _min_expiration)
              _expiration = WSI.Common.Misc.TodayOpening().AddDays(CType(GetCashierPlayerTrackingData("Level" & _level.ToString().PadLeft(2, "0") & ".DaysOnLevel"), Double))

              If (_expire_on_date) Then
                If (_expiration > _min_expiration) Then
                  _expiration = _min_expiration.AddYears(1)
                Else
                  _expiration = _min_expiration
                End If
              End If

              _sql_command.Parameters.Add("@pLevelExpiration", SqlDbType.DateTime).Value = _expiration
            End If

            _sql_command.Parameters.Add("@pType", SqlDbType.Int).Value = WSI.Common.MovementType.ManualHolderLevelChanged
            _sql_command.Parameters.Add("@pAddAmount", SqlDbType.Money).Value = _level

          ElseIf opt_points_status.Checked Then
            _sql_command.Parameters.Add("@pPointsStatus", SqlDbType.Int).Value = _points_status
            _sql_command.Parameters.Add("@pType", SqlDbType.Int).Value = WSI.Common.MovementType.PointsStatusChanged ' Movement type

          End If

          Dim _table As DataTable
          _table = New DataTable

          Using _sql_da As New SqlDataAdapter(_sql_command)
            _db_trx.Fill(_sql_da, _table)
          End Using

          Dim _bucket_amount As Decimal
          Dim _bucket_operation As Buckets.BucketOperation
          If opt_adjust_points.Checked And cmb_operation.Count > 0 Then
            _bucket_operation = GetBucketOperationByOperation(cmb_operation.Value)
          End If

          _num_modified_accounts = 0

          _cashier_info = New CashierSessionInfo()
          _cashier_info.TerminalName = CurrentUser.Name + "@" + Environment.MachineName

          _account_mov = New AccountMovementsTable(_cashier_info)


          For Each _row As DataRow In _table.Rows
            ' Insert Bucket History (If we are updating (ADD, SUB, SET) the buckets)  FGB 2016-FEB-03
            If opt_adjust_points.Checked Then

              'FJC 22-SEP-2016 Fixed Bug 18019:PromoBOX: no se muestran los puntos negativos de la cuenta de un cliente
              'If (_row("FINAL_BAL_BUCKET") < 0) Then
              '  ' 7149 - Can not put a negative value to the bucket
              '  ErrorIdNLS = GLB_NLS_GUI_PLAYER_TRACKING.Id(7149)
              '  ErrorAccountId = _row("ACCOUNT_ID")
              '  Return False
              'End If

              Select Case _bucket_operation
                Case Buckets.BucketOperation.ADD_DISCRETIONAL
                  'Add Points
                  _bucket_amount = (_row("FINAL_BAL_BUCKET") - _row("INITIAL_BAL_BUCKET"))
                Case Buckets.BucketOperation.SUB_DISCRETIONAL
                  'Sub Points
                  _bucket_amount = (_row("INITIAL_BAL_BUCKET") - _row("FINAL_BAL_BUCKET"))
                Case Buckets.BucketOperation.SET_DISCRETIONAL
                  'Set Points
                  _bucket_amount = _row("FINAL_BAL_BUCKET")
              End Select

              ' Update Only bucket History
              If Not BucketsUpdate.UpdateCustomerBucket(_row("ACCOUNT_ID"), cmb_buckets.Value, Nothing, _bucket_amount, _bucket_operation, False, True, _db_trx.SqlTransaction) Then
                ' 7150 - Can not update the value of the bucket
                ErrorIdNLS = GLB_NLS_GUI_PLAYER_TRACKING.Id(7150)
                ErrorAccountId = _row("ACCOUNT_ID")
                Return False
              End If
            End If

            ' Insert account movement
            If _account_mov.Add(0, _row("ACCOUNT_ID"), _row("TYPE"), _
                                _row("INITIAL_BALANCE"), _row("SUB_AMOUNT"), _row("ADD_AMOUNT"), _row("FINAL_BALANCE"), _
                                IIf(ef_details.Text.Trim.Length > 0, ef_details.Text.Trim(), ""), _
                                -1, "", IIf(ef_reasons.Value.Trim.Length > 0, ef_reasons.Value.Trim(), "")) Then
              ' It counts the returned rows from reader
              _num_modified_accounts += 1
            End If

            ' Audit the update operation
            _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_ACCOUNT)
            ' 207  "Account"
            _auditor_data.SetName(GLB_NLS_GUI_STATISTICS.Id(207), _
                                  GLB_NLS_GUI_STATISTICS.GetString(207) & ": " & _row("ACCOUNT_ID").ToString() & " - " & _row("HOLDER_NAME").ToString())

            If opt_adjust_points.Checked Then

              Select Case _bucket_operation
                Case Buckets.BucketOperation.ADD_DISCRETIONAL
                  ' Add Points
                  ' 83 "before"
                  ' 77 "Update points"
                  _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7131), _
                                         m_bucketsList.GetBucketName(cmb_buckets.Value) & " " & GUI_FormatNumber(_points + _row("INITIAL_BALANCE"), 2) & " (" & GLB_NLS_GUI_CONFIGURATION.GetString(83) & " " & GUI_FormatNumber(_row("INITIAL_BALANCE").ToString(), 2) & ")")
                Case Buckets.BucketOperation.SUB_DISCRETIONAL
                  ' Sub Points
                  ' 83 "before"
                  ' 77 "Update points"
                  _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7131), _
                                         m_bucketsList.GetBucketName(cmb_buckets.Value) & " " & GUI_FormatNumber(_points + _row("INITIAL_BALANCE"), 2) & " (" & GLB_NLS_GUI_CONFIGURATION.GetString(83) & " " & GUI_FormatNumber(_row("INITIAL_BALANCE").ToString(), 2) & ")")
                Case Buckets.BucketOperation.SET_DISCRETIONAL
                  ' Set Points
                  ' 83 "before"
                  ' 77 "Update points"
                  _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7131), _
                                         m_bucketsList.GetBucketName(cmb_buckets.Value) & " " & GUI_FormatNumber(_points, 2) & " (" & GLB_NLS_GUI_CONFIGURATION.GetString(83) & " " & GUI_FormatNumber(_row("INITIAL_BALANCE").ToString(), 2) & ")")
              End Select

            ElseIf opt_adjust_level.Checked Then
              ' Set level
              ' 79 "Change level"
              ' 83 "before"
              _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(79), _
                                     cmb_levels.TextValue & " (" & GLB_NLS_GUI_CONFIGURATION.GetString(83) & " " & _row("LEVEL_NAME").ToString() & ")")

            ElseIf opt_points_status.Checked Then
              ' Set points status
              Dim _value As String

              _value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1644)

              If Not _row.IsNull("SUB_AMOUNT") Then
                If _row("SUB_AMOUNT") = WSI.Common.ACCOUNT_POINTS_STATUS.REDEEM_ALLOWED Then
                  _value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1643)
                End If
              End If

              _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1645), _
                                     cmb_points_status.TextValue & _
                                     " (" & GLB_NLS_GUI_CONFIGURATION.GetString(83) & " " & _value & ")")

            End If

            ' RRB 28-AUG-2012 Audit reasons
            _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1262), _
                                   IIf(ef_reasons.Value.Trim.Length > 0, ef_reasons.Value.Trim(), "---"))

            Call _auditor_data.Notify(GLB_CurrentUser.GuiId, _
                                      GLB_CurrentUser.Id, _
                                      GLB_CurrentUser.Name, _
                                      CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, _
                                      0)
          Next 'for _table
        End Using '_sql_command

        ' If the returned rows from reader differ from the selected acounts (except anonymous ones) in the previous window, then rollback
        If _num_modified_accounts <> m_account_ids.Count Then
          Return False
        End If

        If Not _account_mov.Save(_db_trx.SqlTransaction) Then
          Return False
        End If

        _db_trx.Commit()

      End Using '_db_trx

    Catch _ex As Exception

      Return False

    End Try

    Return True

  End Function

  ' PURPOSE: Gets movementype by selected operation.
  '  PARAMS:
  '     - INPUT:
  '           - SelectedValue.
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - MovementType
  Private Function GetMovementTypeByOperation(ByVal SelectedValue As Integer) As MovementType
    Dim _MovementType As MovementType

    Select Case SelectedValue
      Case 1
        _MovementType = MovementType.MULTIPLE_BUCKETS_Manual_Add + cmb_buckets.Value
      Case 2
        _MovementType = MovementType.MULTIPLE_BUCKETS_Manual_Sub + cmb_buckets.Value
      Case 3
        _MovementType = MovementType.MULTIPLE_BUCKETS_Manual_Set + cmb_buckets.Value
      Case Else
        Log.Error("operation not defined: GetMovementTypeByOperation" + SelectedValue)
        _MovementType = 0
    End Select

    Return _MovementType
  End Function

  ' PURPOSE: Gets BucketOperation by selected operation.
  '  PARAMS:
  '     - INPUT:
  '           - SelectedValue.
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Buckets.BucketOperation
  Private Function GetBucketOperationByOperation(ByVal SelectedValue As Integer) As Buckets.BucketOperation
    Dim _BucketOperation As Buckets.BucketOperation

    Select Case SelectedValue
      Case 1
        _BucketOperation = Buckets.BucketOperation.ADD_DISCRETIONAL
      Case 2
        _BucketOperation = Buckets.BucketOperation.SUB_DISCRETIONAL
      Case 3
        _BucketOperation = Buckets.BucketOperation.SET_DISCRETIONAL
      Case Else
        Log.Error("operation not defined: GetBucketOperationByOperation" + SelectedValue)
        Throw New Exception("operation not defined: GetBucketOperationByOperation" + SelectedValue)
    End Select

    Return _BucketOperation
  End Function

  Private Sub SwitchLabelMessageNoneBucketsEnabled()
    lbl_message_none_buckets_enabled.Visible = (opt_adjust_points.Checked And (cmb_buckets.Count = 0))
  End Sub
#End Region  ' Private Functions

#Region "Events"

  Private Sub opt_adjust_points_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_adjust_points.CheckedChanged

    If opt_adjust_points.Checked Then
      cmb_buckets.Enabled = True
      ef_points.Enabled = True

      cmb_levels.Enabled = False
      cmb_points_status.Enabled = False
      cmb_operation.Enabled = cmb_buckets.Enabled
    End If

    SwitchLabelMessageNoneBucketsEnabled()
  End Sub

  Private Sub opt_adjust_level_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_adjust_level.CheckedChanged

    If opt_adjust_level.Checked Then
      cmb_levels.Enabled = True

      cmb_buckets.Enabled = False
      ef_points.Enabled = False
      cmb_points_status.Enabled = False
      cmb_operation.Enabled = cmb_buckets.Enabled
    End If

    SwitchLabelMessageNoneBucketsEnabled()
  End Sub

  Private Sub opt_points_status_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_points_status.CheckedChanged

    If opt_points_status.Checked Then
      cmb_points_status.Enabled = True

      cmb_buckets.Enabled = False
      ef_points.Enabled = False
      cmb_levels.Enabled = False
      cmb_operation.Enabled = cmb_buckets.Enabled
    End If

    SwitchLabelMessageNoneBucketsEnabled()
  End Sub

  Private Sub cmb_buckets_ValueChangedEvent() Handles cmb_buckets.ValueChangedEvent
    Dim _properties As Bucket_Properties
    Dim _bucket_form As BucketsForm
    Dim _Type As Buckets.BucketType

    _bucket_form = New BucketsForm()
    Dim _bucket_id As Buckets.BucketId
    _bucket_id = cmb_buckets.Value
    _bucket_form.GetBucketType(_bucket_id, _Type)

    cmb_operation.Clear()

    _properties = Buckets.GetProperties(cmb_buckets.Value)

    If _properties.gui_can_add Then
      cmb_operation.Add(1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7119))
    End If

    If _properties.gui_can_sub Then
      cmb_operation.Add(2, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7120))
    End If

    If _properties.gui_can_set Then
      cmb_operation.Add(3, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7121))
    End If


    If Buckets.GetBucketUnits(_Type) = Buckets.BucketUnits.Money Then
      ef_points.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9, 2)
    Else
      ef_points.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 6)
    End If





  End Sub
#End Region  ' Events

End Class
