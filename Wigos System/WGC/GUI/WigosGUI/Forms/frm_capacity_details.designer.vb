<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_capacity_details
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
Me.tf_provider = New GUI_Controls.uc_text_field
Me.dg_capacity_hours = New GUI_Controls.uc_grid
Me.panel_data.SuspendLayout()
Me.SuspendLayout()
'
'panel_data
'
Me.panel_data.Controls.Add(Me.dg_capacity_hours)
Me.panel_data.Controls.Add(Me.tf_provider)
Me.panel_data.Size = New System.Drawing.Size(435, 486)
'
'tf_provider
'
Me.tf_provider.IsReadOnly = True
Me.tf_provider.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
Me.tf_provider.LabelForeColor = System.Drawing.Color.Blue
Me.tf_provider.Location = New System.Drawing.Point(6, 13)
Me.tf_provider.Name = "tf_provider"
Me.tf_provider.Size = New System.Drawing.Size(222, 24)
Me.tf_provider.SufixText = "Sufix Text"
Me.tf_provider.SufixTextVisible = True
Me.tf_provider.TabIndex = 124
Me.tf_provider.TabStop = False
Me.tf_provider.Value = "x---"
'
'dg_capacity_hours
'
Me.dg_capacity_hours.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
Me.dg_capacity_hours.CurrentCol = -1
Me.dg_capacity_hours.CurrentRow = -1
Me.dg_capacity_hours.Location = New System.Drawing.Point(0, 64)
Me.dg_capacity_hours.Name = "dg_capacity_hours"
Me.dg_capacity_hours.PanelRightVisible = False
Me.dg_capacity_hours.Redraw = True
Me.dg_capacity_hours.Size = New System.Drawing.Size(432, 422)
Me.dg_capacity_hours.Sortable = False
Me.dg_capacity_hours.SortAscending = True
Me.dg_capacity_hours.SortByCol = 0
Me.dg_capacity_hours.TabIndex = 126
Me.dg_capacity_hours.ToolTipped = True
Me.dg_capacity_hours.TopRow = -2
'
'frm_capacity_details
'
Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
Me.ClientSize = New System.Drawing.Size(537, 494)
Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
Me.Name = "frm_capacity_details"
Me.Text = "frm_capacity_details"
Me.panel_data.ResumeLayout(False)
Me.ResumeLayout(False)

End Sub
  Friend WithEvents tf_provider As GUI_Controls.uc_text_field
  Friend WithEvents dg_capacity_hours As GUI_Controls.uc_grid
End Class
