'-------------------------------------------------------------------
' Copyright � 2007-2014 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_account_request_pin
' DESCRIPTION:   Set the pin request requirement for the different system modules
' AUTHOR:        Joshwa Marcalle
' CREATION DATE: 19-MAR-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 19-MAR-2014  JRM    Initial version.Description contained in RequestPinCodeDUT.docx DUT
' 03-APR-2014  JRM    Fixed bug: WIGOSTITO-1187
' 11-APR-2014  LEM    Deleted PromoBOX funcionality
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient

Public Class frm_account_request_pin
  Inherits frm_base_edit

#Region " Members "

  Private m_form_state As Int32
  Private m_compound_params As CLASS_ACCOUNT_REQUEST_PIN

#End Region ' Members

#Region " Overrides "

  ' PURPOSE : Initializes the form id.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_ACCOUNT_REQUEST_PIN

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE : Form controls initialization.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS  :

  Protected Overrides Sub GUI_InitControls()

    '' Required by the base class
    Call MyBase.GUI_InitControls()

    ' Set form Name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4747) ' Player PIN number requiremenet

    Me.lbl_header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4748) 'Require player PIN number on following operations
    Me.grb_cashier.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4749) 'Cashier
    Me.grb_ebox.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4751) 'eBox
    Me.chk_cashier_withdrawal.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4752) 'Withdrawal
    Me.chk_cashier_gift_redeem.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4754) 'Gift redemption
    Me.chk_ebox_play.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4753) 'Play
    Me.chk_disable_for_anonymous.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4760) 'Disbale for anonymous accounts

    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False

  End Sub ' GUI_InitControls


  ' PURPOSE : Set initial data on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    Dim _cls_account_request_pin As CLASS_ACCOUNT_REQUEST_PIN

    _cls_account_request_pin = DbReadObject

    Me.chk_cashier_gift_redeem.Checked = (_cls_account_request_pin.RequestPinCashier Or WSI.Common.Accounts.PinRequestOperationType.GIFT_REQUEST) = _cls_account_request_pin.RequestPinCashier
    Me.chk_cashier_withdrawal.Checked = (_cls_account_request_pin.RequestPinCashier Or WSI.Common.Accounts.PinRequestOperationType.WITHDRAWAL) = _cls_account_request_pin.RequestPinCashier
    Me.chk_ebox_play.Checked = (_cls_account_request_pin.RequestPinEBox Or WSI.Common.Accounts.PinRequestOperationType.PLAY) = _cls_account_request_pin.RequestPinEBox

    'Anonymous accounts. Disabled options on titoMode
    If WSI.Common.TITO.Utils.IsTitoMode Then
      Me.chk_disable_for_anonymous.Checked = False
      Me.chk_disable_for_anonymous.Enabled = False
      Me.chk_cashier_withdrawal.Enabled = False
      Me.chk_cashier_withdrawal.Checked = False
      Me.chk_ebox_play.Enabled = False
      Me.chk_ebox_play.Checked = False
    Else
      Me.chk_disable_for_anonymous.Checked = (_cls_account_request_pin.DisableForAnonymousAccounts = 1)
    End If

  End Sub 'GUI_SetScreenData


  ' PURPOSE : Validate the data presented on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Return True

  End Function ' GUI_IsScreenDataOk

  ' PURPOSE : Get data from the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_GetScreenData()
    Dim _cashier_request_pin As Int32
    Dim _ebox_request_pin As Int32
    Dim _disable_for_anonymous_accounts As Int32

    Dim _account_request_pin As CLASS_ACCOUNT_REQUEST_PIN

    'Anonymous accounts
    _disable_for_anonymous_accounts = 0
    If chk_disable_for_anonymous.Checked Then
      _disable_for_anonymous_accounts = 1
    End If

    ' Cashier
    _cashier_request_pin = 0
    If chk_cashier_gift_redeem.Checked Then
      _cashier_request_pin = _cashier_request_pin Or WSI.Common.Accounts.PinRequestOperationType.GIFT_REQUEST
    End If
    If chk_cashier_withdrawal.Checked Then
      _cashier_request_pin = _cashier_request_pin Or WSI.Common.Accounts.PinRequestOperationType.WITHDRAWAL
    End If

    'eBox
    _ebox_request_pin = 0
    If chk_ebox_play.Checked Then
      _ebox_request_pin = _ebox_request_pin Or WSI.Common.Accounts.PinRequestOperationType.PLAY
    End If

    ' Set Edit object
    _account_request_pin = DbEditedObject
    _account_request_pin.RequestPinCashier = _cashier_request_pin
    _account_request_pin.RequestPinEBox = _ebox_request_pin
    _account_request_pin.DisableForAnonymousAccounts = _disable_for_anonymous_accounts

  End Sub ' GUI_GetScreenData



  ' PURPOSE : Database overridable operations. 
  '           Define specific DB operation for this form.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        Me.DbEditedObject = New CLASS_ACCOUNT_REQUEST_PIN
        Me.DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_READ
        DbEditedObject = New CLASS_ACCOUNT_REQUEST_PIN()

        Call MyBase.GUI_DB_Operation(DbOperation)

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then

          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2853), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1)

        Else
          m_compound_params = DbEditedObject
          DbReadObject = DbEditedObject.Duplicate()
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_UPDATE

        Call MyBase.GUI_DB_Operation(DbOperation)

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2854), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE
        DbReadObject = DbEditedObject.Duplicate()

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)
    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE : Manage permissions.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)
  End Sub ' GUI_Permissions

#End Region ' Overrides

End Class