﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_gamegateway_draws_prizes_report
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.chk_sort_by_account = New System.Windows.Forms.CheckBox()
    Me.uc_account_sel1 = New GUI_Controls.uc_account_sel()
    Me.gb_date_finished = New System.Windows.Forms.GroupBox()
    Me.dtp_to_finished = New GUI_Controls.uc_date_picker()
    Me.dtp_from_finished = New GUI_Controls.uc_date_picker()
    Me.gb_filter_bets_prizes = New System.Windows.Forms.GroupBox()
    Me.opt_prizes = New System.Windows.Forms.RadioButton()
    Me.opt_bets = New System.Windows.Forms.RadioButton()
    Me.opt_bets_prizes_all = New System.Windows.Forms.RadioButton()
    Me.uc_combo_game_gateway = New GUI_Controls.uc_combo_game_gateway()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_date_finished.SuspendLayout()
    Me.gb_filter_bets_prizes.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.uc_combo_game_gateway)
    Me.panel_filter.Controls.Add(Me.gb_filter_bets_prizes)
    Me.panel_filter.Controls.Add(Me.gb_date_finished)
    Me.panel_filter.Controls.Add(Me.uc_account_sel1)
    Me.panel_filter.Controls.Add(Me.chk_sort_by_account)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1297, 189)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_sort_by_account, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account_sel1, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date_finished, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_filter_bets_prizes, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_combo_game_gateway, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 193)
    Me.panel_data.Size = New System.Drawing.Size(1297, 414)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1291, 17)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1291, 4)
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(6, 6)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(253, 87)
    Me.gb_date.TabIndex = 15
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 50)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(240, 25)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 20)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(240, 25)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'chk_sort_by_account
    '
    Me.chk_sort_by_account.Location = New System.Drawing.Point(605, 146)
    Me.chk_sort_by_account.Name = "chk_sort_by_account"
    Me.chk_sort_by_account.Size = New System.Drawing.Size(199, 24)
    Me.chk_sort_by_account.TabIndex = 21
    Me.chk_sort_by_account.Text = "xSort by account"
    '
    'uc_account_sel1
    '
    Me.uc_account_sel1.Account = ""
    Me.uc_account_sel1.AccountText = ""
    Me.uc_account_sel1.Anon = False
    Me.uc_account_sel1.AutoSize = True
    Me.uc_account_sel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_account_sel1.BirthDate = New Date(CType(0, Long))
    Me.uc_account_sel1.DisabledHolder = False
    Me.uc_account_sel1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_account_sel1.Holder = ""
    Me.uc_account_sel1.Location = New System.Drawing.Point(602, 3)
    Me.uc_account_sel1.MassiveSearchNumbers = ""
    Me.uc_account_sel1.MassiveSearchNumbersToEdit = ""
    Me.uc_account_sel1.MassiveSearchType = 0
    Me.uc_account_sel1.Name = "uc_account_sel1"
    Me.uc_account_sel1.SearchTrackDataAsInternal = True
    Me.uc_account_sel1.ShowMassiveSearch = False
    Me.uc_account_sel1.ShowVipClients = True
    Me.uc_account_sel1.Size = New System.Drawing.Size(307, 134)
    Me.uc_account_sel1.TabIndex = 22
    Me.uc_account_sel1.Telephone = ""
    Me.uc_account_sel1.TrackData = ""
    Me.uc_account_sel1.Vip = False
    Me.uc_account_sel1.WeddingDate = New Date(CType(0, Long))
    '
    'gb_date_finished
    '
    Me.gb_date_finished.Controls.Add(Me.dtp_to_finished)
    Me.gb_date_finished.Controls.Add(Me.dtp_from_finished)
    Me.gb_date_finished.Location = New System.Drawing.Point(6, 96)
    Me.gb_date_finished.Name = "gb_date_finished"
    Me.gb_date_finished.Size = New System.Drawing.Size(253, 87)
    Me.gb_date_finished.TabIndex = 23
    Me.gb_date_finished.TabStop = False
    Me.gb_date_finished.Text = "xDate"
    '
    'dtp_to_finished
    '
    Me.dtp_to_finished.Checked = True
    Me.dtp_to_finished.IsReadOnly = False
    Me.dtp_to_finished.Location = New System.Drawing.Point(6, 47)
    Me.dtp_to_finished.Name = "dtp_to_finished"
    Me.dtp_to_finished.ShowCheckBox = True
    Me.dtp_to_finished.ShowUpDown = False
    Me.dtp_to_finished.Size = New System.Drawing.Size(240, 25)
    Me.dtp_to_finished.SufixText = "Sufix Text"
    Me.dtp_to_finished.SufixTextVisible = True
    Me.dtp_to_finished.TabIndex = 1
    Me.dtp_to_finished.TextWidth = 50
    Me.dtp_to_finished.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from_finished
    '
    Me.dtp_from_finished.Checked = True
    Me.dtp_from_finished.IsReadOnly = False
    Me.dtp_from_finished.Location = New System.Drawing.Point(6, 17)
    Me.dtp_from_finished.Name = "dtp_from_finished"
    Me.dtp_from_finished.ShowCheckBox = True
    Me.dtp_from_finished.ShowUpDown = False
    Me.dtp_from_finished.Size = New System.Drawing.Size(240, 25)
    Me.dtp_from_finished.SufixText = "Sufix Text"
    Me.dtp_from_finished.SufixTextVisible = True
    Me.dtp_from_finished.TabIndex = 0
    Me.dtp_from_finished.TextWidth = 50
    Me.dtp_from_finished.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'gb_filter_bets_prizes
    '
    Me.gb_filter_bets_prizes.Controls.Add(Me.opt_prizes)
    Me.gb_filter_bets_prizes.Controls.Add(Me.opt_bets)
    Me.gb_filter_bets_prizes.Controls.Add(Me.opt_bets_prizes_all)
    Me.gb_filter_bets_prizes.Location = New System.Drawing.Point(268, 96)
    Me.gb_filter_bets_prizes.Name = "gb_filter_bets_prizes"
    Me.gb_filter_bets_prizes.Size = New System.Drawing.Size(327, 87)
    Me.gb_filter_bets_prizes.TabIndex = 24
    Me.gb_filter_bets_prizes.TabStop = False
    Me.gb_filter_bets_prizes.Text = "xFilterBetsPrizes"
    '
    'opt_prizes
    '
    Me.opt_prizes.AutoSize = True
    Me.opt_prizes.Location = New System.Drawing.Point(17, 62)
    Me.opt_prizes.Name = "opt_prizes"
    Me.opt_prizes.Size = New System.Drawing.Size(73, 17)
    Me.opt_prizes.TabIndex = 2
    Me.opt_prizes.TabStop = True
    Me.opt_prizes.Text = "x_Prizes"
    Me.opt_prizes.UseVisualStyleBackColor = True
    '
    'opt_bets
    '
    Me.opt_bets.AutoSize = True
    Me.opt_bets.Location = New System.Drawing.Point(17, 41)
    Me.opt_bets.Name = "opt_bets"
    Me.opt_bets.Size = New System.Drawing.Size(64, 17)
    Me.opt_bets.TabIndex = 1
    Me.opt_bets.TabStop = True
    Me.opt_bets.Text = "x_Bets"
    Me.opt_bets.UseVisualStyleBackColor = True
    '
    'opt_bets_prizes_all
    '
    Me.opt_bets_prizes_all.AutoSize = True
    Me.opt_bets_prizes_all.Location = New System.Drawing.Point(17, 19)
    Me.opt_bets_prizes_all.Name = "opt_bets_prizes_all"
    Me.opt_bets_prizes_all.Size = New System.Drawing.Size(53, 17)
    Me.opt_bets_prizes_all.TabIndex = 0
    Me.opt_bets_prizes_all.TabStop = True
    Me.opt_bets_prizes_all.Text = "x_All"
    Me.opt_bets_prizes_all.UseVisualStyleBackColor = True
    '
    'uc_combo_game_gateway
    '
    Me.uc_combo_game_gateway.Location = New System.Drawing.Point(265, 3)
    Me.uc_combo_game_gateway.Name = "uc_combo_game_gateway"
    Me.uc_combo_game_gateway.Size = New System.Drawing.Size(333, 92)
    Me.uc_combo_game_gateway.TabIndex = 25
    '
    'frm_gamegateway_draws_prizes_report
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1307, 611)
    Me.Name = "frm_gamegateway_draws_prizes_report"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_report_bonoplay_played_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gb_date_finished.ResumeLayout(False)
    Me.gb_filter_bets_prizes.ResumeLayout(False)
    Me.gb_filter_bets_prizes.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents chk_sort_by_account As System.Windows.Forms.CheckBox
  Friend WithEvents uc_account_sel1 As GUI_Controls.uc_account_sel
  Friend WithEvents gb_date_finished As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to_finished As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from_finished As GUI_Controls.uc_date_picker
  Friend WithEvents gb_filter_bets_prizes As System.Windows.Forms.GroupBox
  Friend WithEvents opt_prizes As System.Windows.Forms.RadioButton
  Friend WithEvents opt_bets As System.Windows.Forms.RadioButton
  Friend WithEvents opt_bets_prizes_all As System.Windows.Forms.RadioButton
  Friend WithEvents uc_combo_game_gateway As GUI_Controls.uc_combo_game_gateway
End Class
