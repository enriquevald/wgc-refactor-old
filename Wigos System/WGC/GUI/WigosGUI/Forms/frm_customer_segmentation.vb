'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_customer_segmentation
'
' DESCRIPTION:   Report Segmentation Client
'
' AUTHOR:        Javier G�mez Rubio
'
' CREATION DATE: 16-JAN-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 16-JAN-2012  JGR    Initial version
' 08-JUN-2012  JAB    DoEvents run each second.
' 24-AUG-2012  JAB    Control to show huge amounts of records.
' 29-GEN-2013  ANG    Fix Bug #539
' 12-FEB-2013  JMM    uc_account_sel.ValidateFormat added on FilterCheck
' 24-APR-2013  SMN    Hide Player Tracking data if exists an External Loyalty
' 17-JUN-2013  NMR    Fixed Bug #863: Don't show message when user stops operation
' 21-AUG-2013  JMM    Fixed Defect WIG-141: Several filters errors
' 06-MAY-2014  JBP    Added Vip client filter at reports.
' 27-JUN-2014  LEM & RCI    When card is recycled, the trackdata is shown empty and must be CardNumber.INVALID_TRACKDATA.
' 28-JAN-2015  DCS    In TITO mode anonymous accounts don't displayed
' 10-FEB-2016  DDS    Bug 9207: Total amount's in grid
' 18-MAY-2017  EOR    Fixed Bug 27536: Report Played by player and provider
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports WSI.Common

Public Class frm_customer_segmentation
  Inherits frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call

  End Sub

#End Region

#Region " Constants "

  ' DB Columns
  Private Const SQL_COLUMN_HOLDER_LEVEL = 0
  Private Const SQL_COLUMN_ACCOUNT As Integer = 1
  Private Const SQL_COLUMN_TRACK_DATA As Integer = 2
  Private Const SQL_COLUMN_HOLDER_NAME As Integer = 3
  Private Const SQL_COLUMN_ACCOUNT_CREATION As Integer = 4
  Private Const SQL_COLUMN_HOLDER_GENDER = 5
  Private Const SQL_COLUMN_HOLDER_BIRTHDATE = 6
  Private Const SQL_COLUMN_PROVIDER As Integer = 7
  Private Const SQL_COLUMN_COININ_AMOUNT As Integer = 8
  Private Const SQL_COLUMN_WON_AMOUNT As Integer = 9
  Private Const SQL_COLUMN_THEORETICAL_WIN As Integer = 10
  Private Const SQL_COLUMN_ACCOUNT_TYPE As Integer = 11

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_HOLDER_LEVEL As Integer = 1
  Private Const GRID_COLUMN_ACCOUNT As Integer = 2
  Private Const GRID_COLUMN_TRACK_DATA As Integer = 3
  Private Const GRID_COLUMN_HOLDER_NAME As Integer = 4
  Private Const GRID_COLUMN_ACCOUNT_CREATION As Integer = 5
  Private Const GRID_COLUMN_HOLDER_GENDER = 6
  Private Const GRID_COLUMN_HOLDER_AGE As Integer = 7
  Private Const GRID_COLUMN_HOLDER_BIRTHDATE As Integer = 8
  Private Const GRID_COLUMN_PROVIDER As Integer = 9
  Private Const GRID_COLUMN_COININ_AMOUNT As Integer = 10
  Private Const GRID_COLUMN_WON_AMOUNT As Integer = 11
  Private Const GRID_COLUMN_THEORETICAL_WIN As Integer = 12

  Private Const GRID_COLUMNS As Integer = 13
  Private Const GRID_HEADER_ROWS As Integer = 2

  ' Width
  Private Const GRID_WIDTH_INDEX As Integer = 200
  Private Const GRID_WIDTH_HOLDER_LEVEL As Integer = 1200
  Private Const GRID_WIDTH_ACCOUNT As Integer = 1150
  Private Const GRID_WIDTH_CARD_TRACK_DATA As Integer = 2300
  Private Const GRID_WIDTH_HOLDER_NAME As Integer = 2500
  Private Const GRID_WIDTH_ACCOUNT_CREATION As Integer = 1500
  Private Const GRID_WIDTH_HOLDER_GENDER As Integer = 900
  Private Const GRID_WIDTH_HOLDER_AGE As Integer = 700
  Private Const GRID_WIDTH_HOLDER_BIRTHDAY As Integer = 1700
  Private Const GRID_WIDTH_PROVIDER As Integer = 1900
  Private Const GRID_WIDTH_COININ_AMOUNT As Integer = 1500
  Private Const GRID_WIDTH_WON_AMOUNT As Integer = 1500
  Private Const GRID_WIDTH_THEORETICAL_WIN As Integer = 1500

  ' Holder Gender (Male/Female)
  Private Const HOLDER_GENDER_MALE As Integer = 1
  Private Const HOLDER_GENDER_FEMALE As Integer = 2

  ' Counter Total Account
  Private Const COUNTER_ACCOUNT As Integer = 1

  ' Default Theoretical Payout 
  Private Const DEFAULT_THEORETICAL_PAYOUT As Integer = 0

#End Region ' Constants

#Region " Member"


  ' Report filters 
  Private m_date_from As String
  Private m_date_to As String

  Private m_account As String
  Private m_track_data As String
  Private m_holder As String
  Private m_holder_is_vip As String

  Private m_level As String
  Private m_gender As String
  Private m_only_anonymous_checked As Boolean
  Private m_only_anonymous As String


  ' Totals (account)
  Private m_first_time As Boolean
  Private m_account_id As Int64

  Private m_str_account As String
  Private m_str_track_data As String
  Private m_str_holder_name As String
  Private m_str_account_creation As String
  Private m_str_holder_level As String
  Private m_str_holder_gender As String
  Private m_str_holder_age As String
  Private m_str_holder_birthdate As String

  Private m_coin_in_amount As Decimal
  Private m_won_amount As Decimal
  Private m_theoretical_win As Decimal


  ' Theoretical Payout
  Private m_theoretical_payout As Decimal

  ' Sum
  Private m_sum_total As List(Of Double)

#End Region ' Member

#Region " OVERRIDES "

  ' PURPOSE : Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - None
  '
  '   NOTES :
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CUSTOMER_SEGMENTATION

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE : Initialize every form control
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - None
  '
  '   NOTES :
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Customer segmentation
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(570)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    ' Date time filter
    Me.uc_dsl.Init(GLB_NLS_GUI_INVOICING.Id(201), True)

    ' Level filter
    Me.gb_level.Text = GLB_NLS_GUI_INVOICING.GetString(381)
    Me.chk_level_01.Text = GetCashierPlayerTrackingData("Level01.Name")
    If Me.chk_level_01.Text = "" Then
      Me.chk_level_01.Text = GLB_NLS_GUI_CONFIGURATION.GetString(289)
    End If
    Me.chk_level_02.Text = GetCashierPlayerTrackingData("Level02.Name")
    If chk_level_02.Text = "" Then
      Me.chk_level_02.Text = GLB_NLS_GUI_CONFIGURATION.GetString(290)
    End If
    Me.chk_level_03.Text = GetCashierPlayerTrackingData("Level03.Name")
    If Me.chk_level_03.Text = "" Then
      Me.chk_level_03.Text = GLB_NLS_GUI_CONFIGURATION.GetString(291)
    End If
    Me.chk_level_04.Text = GetCashierPlayerTrackingData("Level04.Name")
    If Me.chk_level_04.Text = "" Then
      Me.chk_level_04.Text = GLB_NLS_GUI_CONFIGURATION.GetString(332)
    End If

    ' Gender filter
    Me.gb_gender.Text = GLB_NLS_GUI_INVOICING.GetString(403)
    Me.chk_gender_male.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(521)
    Me.chk_gender_female.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(522)

    ' Only anonymous
    Me.chk_only_anonymous.Text = GLB_NLS_GUI_CONFIGURATION.GetString(66)
    If WSI.Common.TITO.Utils.IsTitoMode() Then
      Me.chk_only_anonymous.Visible = False
    End If

    ' Only shows customers that have activity
    Me.lbl_info.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(553)

    ' Define layout of all Main Grid Columns
    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ' PURPOSE : Initialize all form filters with their default values
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - None
  '
  '   NOTES :
  '
  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()

  End Sub ' GUI_FilterReset

  ' PURPOSE : Perform preliminary processing for the grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - None
  '
  '   NOTES :
  '
  Protected Overrides Sub GUI_BeforeFirstRow()

    m_first_time = True
    m_account_id = 0

    m_str_account = ""
    m_str_track_data = ""
    m_str_holder_name = ""
    m_str_account_creation = ""
    m_str_holder_level = ""
    m_str_holder_gender = ""
    m_str_holder_age = ""
    m_str_holder_birthdate = ""

    m_coin_in_amount = 0
    m_won_amount = 0
    m_theoretical_win = 0

    For _col As Int32 = 0 To Me.m_sum_total.Count - 1
      Me.m_sum_total(_col) = 0
    Next

  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE : Perform final processing for the grid data
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - None
  '
  '   NOTES :
  '
  Protected Overrides Sub GUI_AfterLastRow()
    Dim _idx_row As Integer

    Me.Grid.AddRow()
    ' If data found, add subtotal after last client 
    If Me.Grid.NumRows > 1 Then
      Me.Grid.AddRow()
    End If

    InsertSubTotals()

    _idx_row = Me.Grid.NumRows - 1

    ' Label - TOTAL:
    Me.Grid.Cell(_idx_row, GRID_COLUMN_HOLDER_LEVEL).Value = GLB_NLS_GUI_INVOICING.GetString(205)

    ' Set values
    For _col As Int32 = GRID_COLUMN_COININ_AMOUNT To Me.m_sum_total.Count - 1
      Me.Grid.Cell(_idx_row, _col).Value = GUI_FormatCurrency(Me.m_sum_total(_col), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Next

    ' Color Row
    Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

  End Sub ' GUI_AfterLastRow

  Protected Overrides Function GUI_FilterCheck() As Boolean

    If Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate >= Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Me.uc_dsl.Focus()
        Return False
      End If
    End If

    ' Check fields on uc_account_sel are ok
    If Not Me.uc_account_sel1.ValidateFormat Then
      Return False
    End If

    Return True
  End Function
  ' PURPOSE : Build an SQL query from conditions set in the filters
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - SQL query text ready to send to the database
  '
  '   NOTES :
  '
  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _str_sql As String
    Dim _dflt_payout_x100 As Integer


    ' General Params: Terminal Default Payout (if is null te_theoretical_payout)
    If Not Integer.TryParse(WSI.Common.Misc.ReadGeneralParams("PlayerTracking", "TerminalDefaultPayout"), _dflt_payout_x100) Then
      _dflt_payout_x100 = DEFAULT_THEORETICAL_PAYOUT
    End If
    _dflt_payout_x100 = Math.Max(0, _dflt_payout_x100)
    _dflt_payout_x100 = Math.Min(100, _dflt_payout_x100)

    ' Get Select and from
    _str_sql = " DECLARE @theoretical_payout DECIMAL(6,2)" & _
              "" & _
              " SET @theoretical_payout = 100 " & _
              " SET @theoretical_payout = " & _dflt_payout_x100.ToString() & " / @theoretical_payout " & _
              "" & _
              " SELECT   HOLDER_NAME_LEVEL " & _
              "        , PS_ACCOUNT_ID " & _
              "        , AC_TRACK_DATA " & _
              "        , AC_HOLDER_NAME " & _
              "        , AC_CREATED " & _
              "        , AC_HOLDER_GENDER " & _
              "        , AC_HOLDER_BIRTH_DATE " & _
              "        , TE_PROVIDER_ID " & _
              "        , COIN_IN " & _
              "        , WON_AMOUNT  " & _
              "        , THEORETICAL_WIN " & _
              "        , AC_TYPE " & _
              "   FROM " & _
              "" & _
              "        ( SELECT	  AC_ACCOUNT_ID " & _
              "                 , AC_HOLDER_NAME " & _
              "                 , AC_TRACK_DATA " & _
              "                 , AC_HOLDER_LEVEL " & _
              "                 , ( SELECT   GP_KEY_VALUE " & _
              "                       FROM   GENERAL_PARAMS " & _
              "                      WHERE	 GP_GROUP_KEY   = 'PlayerTracking' " & _
              "                        AND   GP_SUBJECT_KEY = 'Level' + RIGHT('0' + CAST(AC_HOLDER_LEVEL AS NVARCHAR), 2) + '.Name' " & _
              "                   ) HOLDER_NAME_LEVEL " & _
              "                 , AC_CREATED " & _
              "                 , AC_HOLDER_GENDER " & _
              "                 , AC_HOLDER_BIRTH_DATE " & _
              "                 , AC_TYPE " & _
              "            FROM   ACCOUNTS "
    _str_sql = _str_sql & GetSqlWhereAccount()
    _str_sql = _str_sql & ") ACCOUNTS_INFO, " & _
              "" & _
              "        ( SELECT   PS_ACCOUNT_ID " & _
              "                 , TE_PROVIDER_ID " & _
              "                 , SUM(PS_PLAYED_AMOUNT) COIN_IN " & _
              "                 , SUM(PS_WON_AMOUNT) WON_AMOUNT " & _
              "                 , SUM(PS_PLAYED_AMOUNT * ISNULL(TE_THEORETICAL_PAYOUT, @theoretical_payout)) THEORETICAL_WIN " & _
              "            FROM   PLAY_SESSIONS " & _
              "                 , TERMINALS "
    _str_sql = _str_sql & GetSqlWherePlay()
    _str_sql = _str_sql & " GROUP BY PS_ACCOUNT_ID " & _
              "                    , TE_PROVIDER_ID " & _
              "        ) PLAYED_INFO " & _
              "" & _
              "  WHERE   PLAYED_INFO.PS_ACCOUNT_ID = ACCOUNTS_INFO.AC_ACCOUNT_ID " & _
              " ORDER BY AC_HOLDER_LEVEL DESC " & _
              "        , PS_ACCOUNT_ID " & _
              "        , TE_PROVIDER_ID "

    Return _str_sql

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Get the defined Query Type
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - ENUM_QUERY_TYPE
  '
  '   NOTES :
  '
  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE

    Return ENUM_QUERY_TYPE.QUERY_CUSTOM

  End Function ' GUI_GetQueryType

  ' PURPOSE : Define the ExecuteQuery customized
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - None
  '
  '   NOTES :
  '
  Protected Overrides Sub GUI_ExecuteQueryCustom()

    Dim _str_sql As String
    Dim _db_trx As WSI.Common.DB_TRX
    Dim _sql_da As SqlDataAdapter
    Dim _dataset As DataSet = Nothing
    Dim _datatable As DataTable
    Dim _datarow As System.Data.DataRow

    Try

      ' Obtain the SqlQuery
      _str_sql = GUI_FilterGetSqlQuery()

      If String.IsNullOrEmpty(_str_sql) Then
        Return
      End If

      _db_trx = New WSI.Common.DB_TRX()
      _sql_da = New SqlDataAdapter(New SqlCommand(_str_sql))
      _dataset = New DataSet()

      _db_trx.Fill(_sql_da, _dataset)

      _datatable = _dataset.Tables(0)

      ' JAB 24-AUG-2012: Control to show huge amounts of records.
      If Not ShowHugeNumberOfRows(_datatable.Rows.Count) Then
        MyBase.m_user_canceled_data_shows = True
        Exit Sub
      End If

      Me.Grid.Redraw = False

      Call GUI_BeforeFirstRow()

      For n As Int32 = 0 To _datatable.Rows.Count - 1

        ' JAB 12-JUN-2012: DoEvents run each second.
        If Not GUI_DoEvents(Me.Grid) Then
          Exit Sub
        End If

        _datarow = _datatable.Rows(n)

        Me.Grid.AddRow()
        Call GUI_SetupRow(Me.Grid.NumRows - 1, New CLASS_DB_ROW(_datarow))

      Next n

      Call GUI_AfterLastRow()

      _sql_da.Dispose()
      _db_trx.Dispose()

    Catch ex As Exception
      ' An error has occurred in the query execution
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "Customer base", _
                            "GUI_ExecuteQueryCustom", _
                            ex.Message)

    Finally

      ' JAB 08-JUN-2012: Check if disposed.
      If Not Me.IsDisposed Then
        Me.Grid.Redraw = True
      End If

      If _dataset IsNot Nothing Then
        Call _dataset.Clear()
        Call _dataset.Dispose()
        _dataset = Nothing
      End If
    End Try

  End Sub ' GUI_ExecuteQueryCustom

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS : 
  '     - True (the row should be added) or False (the row can not be added)
  '
  '   NOTES :
  '
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                           ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _track_data As String
    Dim _holder_gender As String

    ' Process first row 
    If m_first_time Then

      ' Counter total account
      With Me.Grid
        .Counter(COUNTER_ACCOUNT).Value += 1
      End With

      m_account_id = DbRow.Value(SQL_COLUMN_ACCOUNT)
      m_first_time = False

    End If


    ' Insert total if diferent account
    If m_account_id <> DbRow.Value(SQL_COLUMN_ACCOUNT) Then

      Me.Grid.AddRow()

      InsertSubTotals()
      ResetSubTotals()

      ' Offest the number of extra rows added
      RowIndex += 1

      ' Counter total account
      With Me.Grid
        .Counter(COUNTER_ACCOUNT).Value += 1
      End With

      m_account_id = DbRow.Value(SQL_COLUMN_ACCOUNT)

    End If


    ' Card Account
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT).Value = DbRow.Value(SQL_COLUMN_ACCOUNT)
    m_str_account = Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT).Value

    ' Card Track Data
    _track_data = ""
    If Not DbRow.IsNull(SQL_COLUMN_TRACK_DATA) Then
      CardNumber.VisibleTrackData(_track_data, DbRow.Value(SQL_COLUMN_TRACK_DATA), MAGNETIC_CARD_TYPES.CARD_TYPE_PLAYER, _
                                     CType(DbRow.Value(SQL_COLUMN_ACCOUNT_TYPE), AccountType))
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TRACK_DATA).Value = _track_data
    End If
    m_str_track_data = Me.Grid.Cell(RowIndex, GRID_COLUMN_TRACK_DATA).Value

    ' Holder Name
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_NAME).Value = DbRow.Value(SQL_COLUMN_HOLDER_NAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_NAME).Value = ""
    End If
    m_str_holder_name = Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_NAME).Value

    ' Date of creation of the account
    If Not DbRow.IsNull(GRID_COLUMN_ACCOUNT_CREATION) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_CREATION).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_ACCOUNT_CREATION), _
                                                                                  ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                  ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
      m_str_account_creation = Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_CREATION).Value
    End If

    ' Level
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_LEVEL) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_LEVEL).Value = DbRow.Value(SQL_COLUMN_HOLDER_LEVEL)
      m_str_holder_level = Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_LEVEL).Value
    End If

    ' Holder Gender
    _holder_gender = ""
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_GENDER) Then
      Select Case DbRow.Value(SQL_COLUMN_HOLDER_GENDER)
        Case HOLDER_GENDER_MALE
          _holder_gender = GLB_NLS_GUI_INVOICING.GetString(413)
        Case HOLDER_GENDER_FEMALE
          _holder_gender = GLB_NLS_GUI_INVOICING.GetString(414)
      End Select
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_GENDER).Value = _holder_gender
    m_str_holder_gender = Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_GENDER).Value

    ' Holder Age (Holder Birth date)
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_BIRTHDATE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_AGE).Value = WSI.Common.Misc.CalculateAge(DbRow.Value(SQL_COLUMN_HOLDER_BIRTHDATE))
      m_str_holder_age = Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_AGE).Value
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_BIRTHDATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_HOLDER_BIRTHDATE), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
      m_str_holder_birthdate = Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_BIRTHDATE).Value
    End If


    '   Provider
    If Not DbRow.IsNull(SQL_COLUMN_PROVIDER) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROVIDER).Value = DbRow.Value(SQL_COLUMN_PROVIDER)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROVIDER).Value = ""
    End If

    ' Coin in (Balance)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_COININ_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_COININ_AMOUNT), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    m_coin_in_amount += DbRow.Value(SQL_COLUMN_COININ_AMOUNT)

    ' WonAmount (Balance)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_WON_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_WON_AMOUNT), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    m_won_amount += DbRow.Value(SQL_COLUMN_WON_AMOUNT)

    ' Theoretical win (Pay Out * Coin in)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_THEORETICAL_WIN).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_THEORETICAL_WIN), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    m_theoretical_win += DbRow.Value(SQL_COLUMN_THEORETICAL_WIN)

    ' Sum
    Me.m_sum_total(GRID_COLUMN_COININ_AMOUNT) += DbRow.Value(SQL_COLUMN_COININ_AMOUNT)
    Me.m_sum_total(GRID_COLUMN_WON_AMOUNT) += DbRow.Value(SQL_COLUMN_WON_AMOUNT)
    Me.m_sum_total(GRID_COLUMN_THEORETICAL_WIN) += DbRow.Value(SQL_COLUMN_THEORETICAL_WIN)

    Return True

  End Function ' GUI_SetupRow

#End Region

#Region " GUI Reports "

  ' PURPOSE : Set proper values for form filters being sent to the report
  '
  '  PARAMS :
  '     - INPUT :
  '           - PrintData
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - None
  '
  '   NOTES :
  '
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    ' Data To/From
    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(261) & " " & GLB_NLS_GUI_AUDITOR.GetString(257), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(261) & " " & GLB_NLS_GUI_AUDITOR.GetString(258), m_date_to)

    ' Account - Track Data - Holder Name
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), m_account)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_track_data)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), m_holder)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4802), m_holder_is_vip)

    ' Holder Gender and Level (if not anonymous)
    If m_only_anonymous_checked Then
      PrintData.SetFilter(GLB_NLS_GUI_CONFIGURATION.GetString(66), m_only_anonymous)
      PrintData.SetFilter("", "", True)
    Else
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(403), m_gender)
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(381), m_level)
    End If

    ' With
    PrintData.FilterValueWidth(1) = 3000
    PrintData.FilterHeaderWidth(2) = 1800
    PrintData.FilterHeaderWidth(3) = 1800
    PrintData.FilterValueWidth(3) = 3200

  End Sub ' GUI_ReportFilter

  ' PURPOSE : Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - None
  '
  '   NOTES :
  '
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_date_from = ""
    m_date_to = ""


    ' From Date
    If Me.uc_dsl.FromDateSelected Then
      m_date_from = GUI_FormatDate(Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime), _
                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    ' To Date
    If Me.uc_dsl.ToDateSelected Then
      m_date_to = GUI_FormatDate(Me.uc_dsl.ToDate.Date.AddHours(Me.uc_dsl.ClosingTime), _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    ' Card Account number
    m_account = Me.uc_account_sel1.Account()
    m_track_data = Me.uc_account_sel1.TrackData()
    m_holder = Me.uc_account_sel1.Holder()
    m_holder_is_vip = Me.uc_account_sel1.HolderIsVip()

    ' Level
    m_level = GetLevelsSelected(False)

    ' Gender
    m_gender = GetGendersSelected(False)

    ' Only anonymous
    m_only_anonymous_checked = Me.chk_only_anonymous.Checked
    m_only_anonymous = IIf(Me.chk_only_anonymous.Checked, _
                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(359), _
                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(360))


  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#Region " Public Functions "

  ' PURPOSE : Opens dialog with default settings for edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '           - MdiParent
  '
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - None
  '
  '   NOTES :
  '
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE : Define layout of all Main Grid Columns
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - None
  '
  '   NOTES :
  '
  Private Sub GUI_StyleSheet()

    m_sum_total = New List(Of Double)

    With Me.Grid

      ' Initialize the index of unfixed columns and return the number of columns
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      ' Disable sortable in Main Grid Columns
      .Sortable = False

      .Counter(COUNTER_ACCOUNT).Visible = True
      .Counter(COUNTER_ACCOUNT).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
      .Counter(COUNTER_ACCOUNT).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Header(1).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False


      ' Account
      ' Account - Number
      .Column(GRID_COLUMN_ACCOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_ACCOUNT).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COLUMN_ACCOUNT).Width = GRID_WIDTH_ACCOUNT
      .Column(GRID_COLUMN_ACCOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Account - Card
      .Column(GRID_COLUMN_TRACK_DATA).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_TRACK_DATA).Header(1).Text = ""
      .Column(GRID_COLUMN_TRACK_DATA).Width = 0

      ' Holder Name
      .Column(GRID_COLUMN_HOLDER_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_HOLDER_NAME).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(235)
      .Column(GRID_COLUMN_HOLDER_NAME).Width = GRID_WIDTH_HOLDER_NAME
      .Column(GRID_COLUMN_HOLDER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Account - Creation     
      .Column(GRID_COLUMN_ACCOUNT_CREATION).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_ACCOUNT_CREATION).Header(1).Text = ""
      .Column(GRID_COLUMN_ACCOUNT_CREATION).Width = 0

      ' Holder Level 
      .Column(GRID_COLUMN_HOLDER_LEVEL).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_HOLDER_LEVEL).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(381)
      .Column(GRID_COLUMN_HOLDER_LEVEL).Width = GRID_WIDTH_HOLDER_LEVEL
      .Column(GRID_COLUMN_HOLDER_LEVEL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Holder Gender
      .Column(GRID_COLUMN_HOLDER_GENDER).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_HOLDER_GENDER).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(403)
      .Column(GRID_COLUMN_HOLDER_GENDER).Width = GRID_WIDTH_HOLDER_GENDER
      .Column(GRID_COLUMN_HOLDER_GENDER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Holder Age 
      .Column(GRID_COLUMN_HOLDER_AGE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_HOLDER_AGE).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(415)
      .Column(GRID_COLUMN_HOLDER_AGE).Width = GRID_WIDTH_HOLDER_AGE
      .Column(GRID_COLUMN_HOLDER_AGE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Holder Birth date
      .Column(GRID_COLUMN_HOLDER_BIRTHDATE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_HOLDER_BIRTHDATE).Header(1).Text = ""
      .Column(GRID_COLUMN_HOLDER_BIRTHDATE).Width = 0


      ' Stadistics
      ' Provider
      .Column(GRID_COLUMN_PROVIDER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(549)
      .Column(GRID_COLUMN_PROVIDER).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(315)
      .Column(GRID_COLUMN_PROVIDER).Width = GRID_WIDTH_PROVIDER
      .Column(GRID_COLUMN_PROVIDER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Coin in (Balance)
      .Column(GRID_COLUMN_COININ_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(549)
      .Column(GRID_COLUMN_COININ_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(537)
      .Column(GRID_COLUMN_COININ_AMOUNT).Width = GRID_WIDTH_COININ_AMOUNT
      .Column(GRID_COLUMN_COININ_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Won amount
      .Column(GRID_COLUMN_WON_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(549)
      .Column(GRID_COLUMN_WON_AMOUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(386)
      .Column(GRID_COLUMN_WON_AMOUNT).Width = GRID_WIDTH_WON_AMOUNT
      .Column(GRID_COLUMN_WON_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Theoretical win (Pay Out * Coin in)
      .Column(GRID_COLUMN_THEORETICAL_WIN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(549)
      .Column(GRID_COLUMN_THEORETICAL_WIN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(571)
      .Column(GRID_COLUMN_THEORETICAL_WIN).Width = GRID_WIDTH_THEORETICAL_WIN
      .Column(GRID_COLUMN_THEORETICAL_WIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT


    End With

    For _idx As Int32 = 0 To Me.Grid.NumColumns - 1
      Me.m_sum_total.Add(0)
    Next


  End Sub ' GUI_StyleSheet

  ' PURPOSE : Insert row with the subtotals
  '
  '  PARAMS :
  '     - INPUT : 
  '           - None
  '
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS : 
  '     - None
  '
  '   NOTES :
  '
  Private Sub InsertSubTotals()

    Dim _idx_row As Integer = Me.Grid.NumRows - 2

    Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT).Value = m_str_account
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TRACK_DATA).Value = m_str_track_data
    Me.Grid.Cell(_idx_row, GRID_COLUMN_HOLDER_NAME).Value = m_str_holder_name
    Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_CREATION).Value = m_str_account_creation
    Me.Grid.Cell(_idx_row, GRID_COLUMN_HOLDER_LEVEL).Value = m_str_holder_level
    Me.Grid.Cell(_idx_row, GRID_COLUMN_HOLDER_GENDER).Value = m_str_holder_gender
    Me.Grid.Cell(_idx_row, GRID_COLUMN_HOLDER_AGE).Value = m_str_holder_age
    Me.Grid.Cell(_idx_row, GRID_COLUMN_HOLDER_BIRTHDATE).Value = m_str_holder_birthdate

    Me.Grid.Cell(_idx_row, GRID_COLUMN_COININ_AMOUNT).Value = GUI_FormatCurrency(m_coin_in_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_WON_AMOUNT).Value = GUI_FormatCurrency(m_won_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_THEORETICAL_WIN).Value = GUI_FormatCurrency(m_theoretical_win, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)


  End Sub ' InsertTotal

  ' PURPOSE : Reset globals variables to the row subtotals
  '
  '  PARAMS :
  '     - INPUT : 
  '           - None
  '
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS : 
  '     - None
  '
  '   NOTES :
  '
  Private Sub ResetSubTotals()

    m_str_account = ""
    m_str_track_data = ""
    m_str_holder_name = ""
    m_str_account_creation = ""
    m_str_holder_level = ""
    m_str_holder_gender = ""
    m_str_holder_age = ""
    m_str_holder_birthdate = ""

    m_coin_in_amount = 0
    m_won_amount = 0
    m_theoretical_win = 0

  End Sub ' ResetTotals

  ' PURPOSE : Set default values to filters
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - None
  '
  '   NOTES :
  '
  Private Sub SetDefaultValues()

    Dim _initial_time As Date
    Dim _final_time As Date
    Dim _closing_time As Integer
    Dim _now As Date

    _now = WGDB.Now

    ' Date time filter
    _initial_time = New DateTime(_now.Year, _now.Month, 1)
    _final_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)
    _closing_time = GetDefaultClosingTime()

    If _final_time > _now Then
      _final_time = _final_time.AddDays(-1)
    End If
    _final_time = _final_time.Date

    Me.uc_dsl.FromDate = _initial_time
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.ToDate = _final_time
    Me.uc_dsl.ToDateSelected = False
    Me.uc_dsl.ClosingTime = _closing_time


    ' Account filter
    Me.uc_account_sel1.Clear()

    ' Level filter
    Me.chk_level_01.Checked = True
    Me.chk_level_02.Checked = True
    Me.chk_level_03.Checked = True
    Me.chk_level_04.Checked = True

    ' Gender filter
    Me.chk_gender_male.Checked = False
    Me.chk_gender_female.Checked = False

    Me.chk_only_anonymous.Checked = False

  End Sub ' SetDefaultValues

  ' PURPOSE : Get Level list for selected levels
  '           Format list to build query: X, X, X
  '           X is the Id if GetIds = True
  '           otherwise, X is the Name of the control
  '  PARAMS :
  '     - INPUT :
  '           - GetIds As Boolean
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - String
  '
  Private Function GetLevelsSelected(Optional ByVal GetIds As Boolean = True) As String

    Dim _list_type As String

    _list_type = ""

    If (GetIds And WSI.Common.TITO.Utils.IsTitoMode() And Not String.IsNullOrEmpty(Me.uc_account_sel1.Account)) Then
      _list_type = "0"
    End If

    If Me.chk_level_01.Checked Then
      If _list_type.Length > 0 Then
        _list_type = _list_type & ", "
      End If
      _list_type += IIf(GetIds, "1", Me.chk_level_01.Text)
    End If
    If Me.chk_level_02.Checked Then
      If _list_type.Length > 0 Then
        _list_type = _list_type & ", "
      End If
      _list_type = _list_type & IIf(GetIds, "2", Me.chk_level_02.Text)
    End If
    If Me.chk_level_03.Checked Then
      If _list_type.Length > 0 Then
        _list_type = _list_type & ", "
      End If
      _list_type = _list_type & IIf(GetIds, "3", Me.chk_level_03.Text)
    End If
    If Me.chk_level_04.Checked Then
      If _list_type.Length > 0 Then
        _list_type = _list_type & ", "
      End If
      _list_type = _list_type & IIf(GetIds, "4", Me.chk_level_04.Text)
    End If

    Return _list_type

  End Function ' GetLevelsSelected

  ' PURPOSE : Get Gender list for selected genders
  '           Format list to build query: X, X, X
  '           X is the Id if GetIds = True
  '           otherwise, X is the Name of the control
  '  PARAMS :
  '     - INPUT :
  '           - GetIds As Boolean
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - String
  '
  '   NOTES :
  '
  Private Function GetGendersSelected(Optional ByVal GetIds As Boolean = True) As String

    Dim _list_type As String

    _list_type = ""
    If chk_gender_male.Checked Then
      _list_type = IIf(GetIds, "1", Me.chk_gender_male.Text)
    End If
    If chk_gender_female.Checked Then
      If _list_type.Length > 0 Then
        _list_type = _list_type & ", "
      End If
      _list_type = _list_type & IIf(GetIds, "2", Me.chk_gender_female.Text)
    End If

    Return _list_type

  End Function ' GetGendersSelected

  ' PURPOSE : Build the variable part of the WHERE clause (the one that depends on filter values) for the
  '           main SQL Query
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - String
  '
  '   NOTES :
  '
  Private Function GetSqlWhereAccount() As String

    Dim _str_where As String = ""

    _str_where = Me.uc_account_sel1.GetFilterSQL()

    If Not Me.uc_account_sel1.DisabledHolder Then

      ' Level filter
      If Not Me.chk_only_anonymous.Checked Then
        If Me.chk_level_01.Checked Or _
             Me.chk_level_02.Checked Or _
             Me.chk_level_03.Checked Or _
             Me.chk_level_04.Checked Then

          If Not String.IsNullOrEmpty(_str_where) Then _str_where = _str_where & " AND "
          _str_where = _str_where & " AC_HOLDER_LEVEL IN (" & GetLevelsSelected() & ") "
        End If

        ' Gender filter
        If Me.chk_gender_male.Checked Or _
           Me.chk_gender_female.Checked Then

          If Not String.IsNullOrEmpty(_str_where) Then _str_where = _str_where & " AND "
          _str_where = _str_where & " AC_HOLDER_GENDER IN (" & GetGendersSelected() & ") "
        End If
      Else
        If Not String.IsNullOrEmpty(_str_where) Then _str_where = _str_where & " AND "
        _str_where = _str_where & " ( AC_USER_TYPE IS NULL OR AC_USER_TYPE = 0 ) "
      End If

    End If

    If Not String.IsNullOrEmpty(_str_where) Then _str_where = " WHERE " & _str_where

    Return _str_where

  End Function ' GetSqlWhereAccount

  Private Function GetSqlWherePlay() As String

    Dim _str_where As String = ""

    ' Dates filter
    _str_where = Me.uc_dsl.GetSqlFilterCondition("PS_FINISHED")
    If _str_where.Length > 0 Then
      _str_where = " AND " & _str_where
    End If

    _str_where = " WHERE PS_TERMINAL_ID = TE_TERMINAL_ID " & _str_where

    Return _str_where

  End Function ' GetSqlWherePlay

#End Region ' Private Functions

#Region " Events "

  ' PURPOSE : Disable Holder Level and Holder Gender if checked Only Anonymous
  '
  '  PARAMS :
  '     - INPUT :
  '           - Sender As System.Object
  '           - Ev As System.EventArgs
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - None
  '
  '   NOTES :
  '
  Private Sub chk_only_anonymous_CheckedChanged(ByVal Sender As System.Object, ByVal Ev As System.EventArgs) Handles chk_only_anonymous.CheckedChanged

    gb_level.Enabled = Not chk_only_anonymous.Checked
    gb_gender.Enabled = Not chk_only_anonymous.Checked

    If chk_only_anonymous.Checked Then
      chk_level_01.Checked = False
      chk_level_02.Checked = False
      chk_level_03.Checked = False
      chk_level_04.Checked = False

      chk_gender_male.Checked = False
      chk_gender_female.Checked = False
    End If

  End Sub

  Private Sub uc_account_sel1_OneAccountFilterChanged() Handles uc_account_sel1.OneAccountFilterChanged
    Me.gb_level.Enabled = Not uc_account_sel1.DisabledHolder
    Me.gb_gender.Enabled = Not uc_account_sel1.DisabledHolder
    Me.chk_only_anonymous.Enabled = Not uc_account_sel1.DisabledHolder
  End Sub

#End Region ' Events

End Class