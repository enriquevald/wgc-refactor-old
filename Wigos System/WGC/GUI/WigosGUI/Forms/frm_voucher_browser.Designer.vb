<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_voucher_browser
  Inherits GUI_Controls.frm_base_print

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_voucher_browser))
    Me.gb_operation_types = New System.Windows.Forms.GroupBox()
    Me.btn_check_all = New GUI_Controls.uc_button()
    Me.btn_uncheck_all = New GUI_Controls.uc_button()
    Me.dg_operations = New GUI_Controls.uc_grid()
    Me.uc_account_sel1 = New GUI_Controls.uc_account_sel()
    Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
    Me.dg_vouchers = New GUI_Controls.uc_grid()
    Me.gb_voucher = New System.Windows.Forms.GroupBox()
    Me.wb_voucher = New System.Windows.Forms.WebBrowser()
    Me.opt_all_users = New System.Windows.Forms.RadioButton()
    Me.gb_user = New System.Windows.Forms.GroupBox()
    Me.chk_show_all = New System.Windows.Forms.CheckBox()
    Me.opt_one_user = New System.Windows.Forms.RadioButton()
    Me.cmb_user = New GUI_Controls.uc_combo()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.ef_operation_id = New GUI_Controls.uc_entry_field()
    Me.opt_by_operation = New System.Windows.Forms.RadioButton()
    Me.ef_voucher_id = New GUI_Controls.uc_entry_field()
    Me.opt_by_folio = New System.Windows.Forms.RadioButton()
    Me.opt_by_date = New System.Windows.Forms.RadioButton()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.opt_by_voucher = New System.Windows.Forms.RadioButton()
    Me.btn_reprint = New GUI_Controls.uc_button()
    Me.gb_print_progress = New System.Windows.Forms.GroupBox()
    Me.btn_print_cancel = New GUI_Controls.uc_button()
    Me.btn_pause_continue = New GUI_Controls.uc_button()
    Me.lbl_print_status = New System.Windows.Forms.Label()
    Me.tmr_status_printing = New System.Windows.Forms.Timer(Me.components)
    Me.panel_grids.SuspendLayout()
    Me.panel_buttons.SuspendLayout()
    Me.panel_filter.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_operation_types.SuspendLayout()
    Me.SplitContainer1.Panel1.SuspendLayout()
    Me.SplitContainer1.Panel2.SuspendLayout()
    Me.SplitContainer1.SuspendLayout()
    Me.gb_voucher.SuspendLayout()
    Me.gb_user.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_print_progress.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_grids
    '
    Me.panel_grids.Controls.Add(Me.SplitContainer1)
    Me.panel_grids.Location = New System.Drawing.Point(5, 242)
    Me.panel_grids.Size = New System.Drawing.Size(1230, 479)
    Me.panel_grids.Controls.SetChildIndex(Me.panel_buttons, 0)
    Me.panel_grids.Controls.SetChildIndex(Me.SplitContainer1, 0)
    '
    'panel_buttons
    '
    Me.panel_buttons.Controls.Add(Me.btn_reprint)
    Me.panel_buttons.Location = New System.Drawing.Point(1142, 0)
    Me.panel_buttons.Size = New System.Drawing.Size(88, 479)
    Me.panel_buttons.TabIndex = 0
    Me.panel_buttons.Controls.SetChildIndex(Me.btn_reprint, 0)
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_print_progress)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Controls.Add(Me.gb_user)
    Me.panel_filter.Controls.Add(Me.uc_account_sel1)
    Me.panel_filter.Controls.Add(Me.gb_operation_types)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1230, 215)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_operation_types, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account_sel1, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_user, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_print_progress, 0)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Location = New System.Drawing.Point(5, 219)
    Me.pn_separator_line.Size = New System.Drawing.Size(1230, 23)
    Me.pn_separator_line.TabIndex = 1
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1230, 4)
    '
    'gb_operation_types
    '
    Me.gb_operation_types.Controls.Add(Me.btn_check_all)
    Me.gb_operation_types.Controls.Add(Me.btn_uncheck_all)
    Me.gb_operation_types.Controls.Add(Me.dg_operations)
    Me.gb_operation_types.Location = New System.Drawing.Point(257, 6)
    Me.gb_operation_types.Name = "gb_operation_types"
    Me.gb_operation_types.Size = New System.Drawing.Size(274, 205)
    Me.gb_operation_types.TabIndex = 1
    Me.gb_operation_types.TabStop = False
    Me.gb_operation_types.Text = "xOperationTypes"
    '
    'btn_check_all
    '
    Me.btn_check_all.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_check_all.Location = New System.Drawing.Point(6, 167)
    Me.btn_check_all.Name = "btn_check_all"
    Me.btn_check_all.Size = New System.Drawing.Size(90, 30)
    Me.btn_check_all.TabIndex = 1
    Me.btn_check_all.ToolTipped = False
    Me.btn_check_all.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_uncheck_all
    '
    Me.btn_uncheck_all.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_uncheck_all.Location = New System.Drawing.Point(102, 167)
    Me.btn_uncheck_all.Name = "btn_uncheck_all"
    Me.btn_uncheck_all.Size = New System.Drawing.Size(90, 30)
    Me.btn_uncheck_all.TabIndex = 2
    Me.btn_uncheck_all.ToolTipped = False
    Me.btn_uncheck_all.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'dg_operations
    '
    Me.dg_operations.CurrentCol = -1
    Me.dg_operations.CurrentRow = -1
    Me.dg_operations.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_operations.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_operations.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_operations.Location = New System.Drawing.Point(6, 14)
    Me.dg_operations.Name = "dg_operations"
    Me.dg_operations.PanelRightVisible = False
    Me.dg_operations.Redraw = True
    Me.dg_operations.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_operations.Size = New System.Drawing.Size(262, 147)
    Me.dg_operations.Sortable = False
    Me.dg_operations.SortAscending = True
    Me.dg_operations.SortByCol = 0
    Me.dg_operations.TabIndex = 0
    Me.dg_operations.ToolTipped = True
    Me.dg_operations.TopRow = -2
    Me.dg_operations.WordWrap = False
    '
    'uc_account_sel1
    '
    Me.uc_account_sel1.Account = ""
    Me.uc_account_sel1.AccountText = ""
    Me.uc_account_sel1.Anon = False
    Me.uc_account_sel1.AutoSize = True
    Me.uc_account_sel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_account_sel1.BirthDate = New Date(CType(0, Long))
    Me.uc_account_sel1.DisabledHolder = False
    Me.uc_account_sel1.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.uc_account_sel1.Holder = ""
    Me.uc_account_sel1.Location = New System.Drawing.Point(535, 77)
    Me.uc_account_sel1.MassiveSearchNumbers = ""
    Me.uc_account_sel1.MassiveSearchNumbersToEdit = ""
    Me.uc_account_sel1.MassiveSearchType = 0
    Me.uc_account_sel1.Name = "uc_account_sel1"
    Me.uc_account_sel1.Padding = New System.Windows.Forms.Padding(3)
    Me.uc_account_sel1.SearchTrackDataAsInternal = True
    Me.uc_account_sel1.ShowMassiveSearch = False
    Me.uc_account_sel1.ShowVipClients = True
    Me.uc_account_sel1.Size = New System.Drawing.Size(310, 137)
    Me.uc_account_sel1.TabIndex = 3
    Me.uc_account_sel1.Telephone = ""
    Me.uc_account_sel1.TrackData = ""
    Me.uc_account_sel1.Vip = False
    Me.uc_account_sel1.WeddingDate = New Date(CType(0, Long))
    '
    'SplitContainer1
    '
    Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
    Me.SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2
    Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
    Me.SplitContainer1.Name = "SplitContainer1"
    '
    'SplitContainer1.Panel1
    '
    Me.SplitContainer1.Panel1.Controls.Add(Me.dg_vouchers)
    '
    'SplitContainer1.Panel2
    '
    Me.SplitContainer1.Panel2.BackColor = System.Drawing.SystemColors.ControlLight
    Me.SplitContainer1.Panel2.Controls.Add(Me.gb_voucher)
    Me.SplitContainer1.Size = New System.Drawing.Size(1142, 479)
    Me.SplitContainer1.SplitterDistance = 829
    Me.SplitContainer1.TabIndex = 10
    '
    'dg_vouchers
    '
    Me.dg_vouchers.CurrentCol = -1
    Me.dg_vouchers.CurrentRow = -1
    Me.dg_vouchers.Dock = System.Windows.Forms.DockStyle.Fill
    Me.dg_vouchers.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_vouchers.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_vouchers.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_vouchers.Location = New System.Drawing.Point(0, 0)
    Me.dg_vouchers.Name = "dg_vouchers"
    Me.dg_vouchers.PanelRightVisible = False
    Me.dg_vouchers.Redraw = True
    Me.dg_vouchers.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_vouchers.Size = New System.Drawing.Size(829, 479)
    Me.dg_vouchers.Sortable = False
    Me.dg_vouchers.SortAscending = True
    Me.dg_vouchers.SortByCol = 0
    Me.dg_vouchers.TabIndex = 0
    Me.dg_vouchers.ToolTipped = True
    Me.dg_vouchers.TopRow = -2
    Me.dg_vouchers.WordWrap = False
    '
    'gb_voucher
    '
    Me.gb_voucher.BackColor = System.Drawing.SystemColors.Control
    Me.gb_voucher.Controls.Add(Me.wb_voucher)
    Me.gb_voucher.Dock = System.Windows.Forms.DockStyle.Fill
    Me.gb_voucher.Location = New System.Drawing.Point(0, 0)
    Me.gb_voucher.Name = "gb_voucher"
    Me.gb_voucher.Size = New System.Drawing.Size(309, 479)
    Me.gb_voucher.TabIndex = 0
    Me.gb_voucher.TabStop = False
    Me.gb_voucher.Text = "xVouchers"
    '
    'wb_voucher
    '
    Me.wb_voucher.AllowWebBrowserDrop = False
    Me.wb_voucher.CausesValidation = False
    Me.wb_voucher.Dock = System.Windows.Forms.DockStyle.Fill
    Me.wb_voucher.Location = New System.Drawing.Point(3, 17)
    Me.wb_voucher.MinimumSize = New System.Drawing.Size(20, 20)
    Me.wb_voucher.Name = "wb_voucher"
    Me.wb_voucher.ScriptErrorsSuppressed = True
    Me.wb_voucher.Size = New System.Drawing.Size(303, 459)
    Me.wb_voucher.TabIndex = 0
    Me.wb_voucher.WebBrowserShortcutsEnabled = False
    '
    'opt_all_users
    '
    Me.opt_all_users.Location = New System.Drawing.Point(8, 40)
    Me.opt_all_users.Name = "opt_all_users"
    Me.opt_all_users.Size = New System.Drawing.Size(64, 24)
    Me.opt_all_users.TabIndex = 1
    Me.opt_all_users.Text = "xAll"
    '
    'gb_user
    '
    Me.gb_user.Controls.Add(Me.chk_show_all)
    Me.gb_user.Controls.Add(Me.opt_one_user)
    Me.gb_user.Controls.Add(Me.opt_all_users)
    Me.gb_user.Controls.Add(Me.cmb_user)
    Me.gb_user.Location = New System.Drawing.Point(537, 6)
    Me.gb_user.Name = "gb_user"
    Me.gb_user.Size = New System.Drawing.Size(304, 74)
    Me.gb_user.TabIndex = 2
    Me.gb_user.TabStop = False
    Me.gb_user.Text = "xUser"
    '
    'chk_show_all
    '
    Me.chk_show_all.AutoSize = True
    Me.chk_show_all.Location = New System.Drawing.Point(107, 45)
    Me.chk_show_all.Name = "chk_show_all"
    Me.chk_show_all.Size = New System.Drawing.Size(102, 17)
    Me.chk_show_all.TabIndex = 2
    Me.chk_show_all.Text = "chk_show_all"
    Me.chk_show_all.UseVisualStyleBackColor = True
    '
    'opt_one_user
    '
    Me.opt_one_user.Location = New System.Drawing.Point(8, 14)
    Me.opt_one_user.Name = "opt_one_user"
    Me.opt_one_user.Size = New System.Drawing.Size(72, 24)
    Me.opt_one_user.TabIndex = 0
    Me.opt_one_user.Text = "xOne"
    '
    'cmb_user
    '
    Me.cmb_user.AllowUnlistedValues = False
    Me.cmb_user.AutoCompleteMode = False
    Me.cmb_user.IsReadOnly = False
    Me.cmb_user.Location = New System.Drawing.Point(103, 14)
    Me.cmb_user.Name = "cmb_user"
    Me.cmb_user.SelectedIndex = -1
    Me.cmb_user.Size = New System.Drawing.Size(184, 24)
    Me.cmb_user.SufixText = "Sufix Text"
    Me.cmb_user.SufixTextVisible = True
    Me.cmb_user.TabIndex = 3
    Me.cmb_user.TextCombo = Nothing
    Me.cmb_user.TextVisible = False
    Me.cmb_user.TextWidth = 0
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.ef_operation_id)
    Me.gb_date.Controls.Add(Me.opt_by_operation)
    Me.gb_date.Controls.Add(Me.ef_voucher_id)
    Me.gb_date.Controls.Add(Me.opt_by_folio)
    Me.gb_date.Controls.Add(Me.opt_by_date)
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Controls.Add(Me.opt_by_voucher)
    Me.gb_date.Location = New System.Drawing.Point(6, 6)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(245, 205)
    Me.gb_date.TabIndex = 0
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xOpen Date"
    '
    'ef_operation_id
    '
    Me.ef_operation_id.DoubleValue = 0.0R
    Me.ef_operation_id.IntegerValue = 0
    Me.ef_operation_id.IsReadOnly = False
    Me.ef_operation_id.Location = New System.Drawing.Point(21, 114)
    Me.ef_operation_id.Name = "ef_operation_id"
    Me.ef_operation_id.PlaceHolder = Nothing
    Me.ef_operation_id.ShortcutsEnabled = True
    Me.ef_operation_id.Size = New System.Drawing.Size(200, 24)
    Me.ef_operation_id.SufixText = "Sufix Text"
    Me.ef_operation_id.SufixTextVisible = True
    Me.ef_operation_id.TabIndex = 4
    Me.ef_operation_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_operation_id.TextValue = ""
    Me.ef_operation_id.Value = ""
    Me.ef_operation_id.ValueForeColor = System.Drawing.Color.Blue
    '
    'opt_by_operation
    '
    Me.opt_by_operation.AutoSize = True
    Me.opt_by_operation.Location = New System.Drawing.Point(6, 94)
    Me.opt_by_operation.Name = "opt_by_operation"
    Me.opt_by_operation.Size = New System.Drawing.Size(107, 17)
    Me.opt_by_operation.TabIndex = 3
    Me.opt_by_operation.TabStop = True
    Me.opt_by_operation.Text = "xBy Operation"
    Me.opt_by_operation.UseVisualStyleBackColor = True
    '
    'ef_voucher_id
    '
    Me.ef_voucher_id.DoubleValue = 0.0R
    Me.ef_voucher_id.IntegerValue = 0
    Me.ef_voucher_id.IsReadOnly = False
    Me.ef_voucher_id.Location = New System.Drawing.Point(21, 167)
    Me.ef_voucher_id.Name = "ef_voucher_id"
    Me.ef_voucher_id.PlaceHolder = Nothing
    Me.ef_voucher_id.ShortcutsEnabled = True
    Me.ef_voucher_id.Size = New System.Drawing.Size(200, 24)
    Me.ef_voucher_id.SufixText = "Sufix Text"
    Me.ef_voucher_id.SufixTextVisible = True
    Me.ef_voucher_id.TabIndex = 7
    Me.ef_voucher_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_voucher_id.TextValue = ""
    Me.ef_voucher_id.Value = ""
    Me.ef_voucher_id.ValueForeColor = System.Drawing.Color.Blue
    '
    'opt_by_folio
    '
    Me.opt_by_folio.AutoSize = True
    Me.opt_by_folio.Location = New System.Drawing.Point(113, 147)
    Me.opt_by_folio.Name = "opt_by_folio"
    Me.opt_by_folio.Size = New System.Drawing.Size(77, 17)
    Me.opt_by_folio.TabIndex = 6
    Me.opt_by_folio.TabStop = True
    Me.opt_by_folio.Text = "xBy Folio"
    Me.opt_by_folio.UseVisualStyleBackColor = True
    Me.opt_by_folio.Visible = False
    '
    'opt_by_date
    '
    Me.opt_by_date.AutoSize = True
    Me.opt_by_date.Location = New System.Drawing.Point(6, 16)
    Me.opt_by_date.Name = "opt_by_date"
    Me.opt_by_date.Size = New System.Drawing.Size(78, 17)
    Me.opt_by_date.TabIndex = 0
    Me.opt_by_date.TabStop = True
    Me.opt_by_date.Text = "xBy Date"
    Me.opt_by_date.UseVisualStyleBackColor = True
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(9, 62)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = False
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(230, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 2
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(9, 36)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = False
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(230, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 1
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'opt_by_voucher
    '
    Me.opt_by_voucher.AutoSize = True
    Me.opt_by_voucher.Location = New System.Drawing.Point(6, 147)
    Me.opt_by_voucher.Name = "opt_by_voucher"
    Me.opt_by_voucher.Size = New System.Drawing.Size(97, 17)
    Me.opt_by_voucher.TabIndex = 5
    Me.opt_by_voucher.TabStop = True
    Me.opt_by_voucher.Text = "xBy Voucher"
    Me.opt_by_voucher.UseVisualStyleBackColor = True
    '
    'btn_reprint
    '
    Me.btn_reprint.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_reprint.Location = New System.Drawing.Point(0, 156)
    Me.btn_reprint.Name = "btn_reprint"
    Me.btn_reprint.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_reprint.Size = New System.Drawing.Size(90, 30)
    Me.btn_reprint.TabIndex = 0
    Me.btn_reprint.ToolTipped = False
    Me.btn_reprint.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.USER
    '
    'gb_print_progress
    '
    Me.gb_print_progress.Controls.Add(Me.btn_print_cancel)
    Me.gb_print_progress.Controls.Add(Me.btn_pause_continue)
    Me.gb_print_progress.Controls.Add(Me.lbl_print_status)
    Me.gb_print_progress.Location = New System.Drawing.Point(848, 6)
    Me.gb_print_progress.Name = "gb_print_progress"
    Me.gb_print_progress.Size = New System.Drawing.Size(272, 74)
    Me.gb_print_progress.TabIndex = 4
    Me.gb_print_progress.TabStop = False
    Me.gb_print_progress.Text = "xPrintProgress"
    '
    'btn_print_cancel
    '
    Me.btn_print_cancel.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_print_cancel.Location = New System.Drawing.Point(160, 38)
    Me.btn_print_cancel.Name = "btn_print_cancel"
    Me.btn_print_cancel.Size = New System.Drawing.Size(90, 30)
    Me.btn_print_cancel.TabIndex = 2
    Me.btn_print_cancel.ToolTipped = False
    Me.btn_print_cancel.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_pause_continue
    '
    Me.btn_pause_continue.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_pause_continue.Location = New System.Drawing.Point(29, 38)
    Me.btn_pause_continue.Name = "btn_pause_continue"
    Me.btn_pause_continue.Size = New System.Drawing.Size(90, 30)
    Me.btn_pause_continue.TabIndex = 1
    Me.btn_pause_continue.ToolTipped = False
    Me.btn_pause_continue.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'lbl_print_status
    '
    Me.lbl_print_status.AutoSize = True
    Me.lbl_print_status.Location = New System.Drawing.Point(6, 20)
    Me.lbl_print_status.Name = "lbl_print_status"
    Me.lbl_print_status.Size = New System.Drawing.Size(76, 13)
    Me.lbl_print_status.TabIndex = 0
    Me.lbl_print_status.Text = "xPrintStatus"
    '
    'tmr_status_printing
    '
    Me.tmr_status_printing.Interval = 300
    '
    'frm_voucher_browser
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1240, 725)
    Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
    Me.Name = "frm_voucher_browser"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_voucher_browser"
    Me.panel_grids.ResumeLayout(False)
    Me.panel_buttons.ResumeLayout(False)
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_operation_types.ResumeLayout(False)
    Me.SplitContainer1.Panel1.ResumeLayout(False)
    Me.SplitContainer1.Panel2.ResumeLayout(False)
    Me.SplitContainer1.ResumeLayout(False)
    Me.gb_voucher.ResumeLayout(False)
    Me.gb_user.ResumeLayout(False)
    Me.gb_user.PerformLayout()
    Me.gb_date.ResumeLayout(False)
    Me.gb_date.PerformLayout()
    Me.gb_print_progress.ResumeLayout(False)
    Me.gb_print_progress.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_operation_types As System.Windows.Forms.GroupBox
  Friend WithEvents dg_operations As GUI_Controls.uc_grid
  Friend WithEvents uc_account_sel1 As GUI_Controls.uc_account_sel
  Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
  Friend WithEvents dg_vouchers As GUI_Controls.uc_grid
  Friend WithEvents gb_voucher As System.Windows.Forms.GroupBox
  Friend WithEvents wb_voucher As System.Windows.Forms.WebBrowser
  Friend WithEvents gb_user As System.Windows.Forms.GroupBox
  Friend WithEvents opt_one_user As System.Windows.Forms.RadioButton
  Friend WithEvents opt_all_users As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_user As GUI_Controls.uc_combo
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents btn_reprint As GUI_Controls.uc_button
  Friend WithEvents gb_print_progress As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_print_status As System.Windows.Forms.Label
  Friend WithEvents btn_pause_continue As GUI_Controls.uc_button
  Friend WithEvents btn_print_cancel As GUI_Controls.uc_button
  Friend WithEvents tmr_status_printing As System.Windows.Forms.Timer
  Friend WithEvents btn_uncheck_all As GUI_Controls.uc_button
  Friend WithEvents btn_check_all As GUI_Controls.uc_button
  Friend WithEvents ef_voucher_id As GUI_Controls.uc_entry_field
  Friend WithEvents opt_by_voucher As System.Windows.Forms.RadioButton
  Friend WithEvents opt_by_date As System.Windows.Forms.RadioButton
  Friend WithEvents opt_by_folio As System.Windows.Forms.RadioButton
  Friend WithEvents chk_show_all As System.Windows.Forms.CheckBox
  Friend WithEvents ef_operation_id As GUI_Controls.uc_entry_field
  Friend WithEvents opt_by_operation As System.Windows.Forms.RadioButton
End Class
