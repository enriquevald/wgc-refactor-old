'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_providers_edit.vb
'
' DESCRIPTION:   Provider edition form
'
' AUTHOR:        Daniel Moreno
'
' CREATION DATE: 30-APR-2013
'
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 30-APR-2013  DMR    Initial version.
' 30-MAY-2013  JCA    Fixed Bug #809: Filter valid chars in provider name: FORMAT_PROVIDERS_NAME.
' 03-JUN-2013  DMR    Added GUI_KeyPress function
' 05-JUN-2013  JCA    Fixed Bug #823: Permission error on Providers form
' 05-JUN-2013  JCA    Fixed Bug #826: When Esc button is clicked empty rows are inserted.
' 11-JUN-2013  JAB    Fixed Bug, count more changes that really are.
' 09-JAN-2014  JCA    Fixed Bug, WIG-513: providers must be created with Multiplier = 1
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

#Region " Imports "

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.uc_grid
Imports GUI_Controls.uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common

#End Region ' Imports
Public Class frm_providers_edit
  Inherits GUI_Controls.frm_base_edit

#Region " Constants "

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_GAME_ID As Integer = 1
  Private Const GRID_COLUMN_GAME_NAME As Integer = 2
  Private Const GRID_COLUMN_PAYOUT_1 As Integer = 3
  Private Const GRID_COLUMN_PAYOUT_2 As Integer = 4
  Private Const GRID_COLUMN_PAYOUT_3 As Integer = 5
  Private Const GRID_COLUMN_PAYOUT_4 As Integer = 6
  Private Const GRID_COLUMN_PAYOUT_5 As Integer = 7
  Private Const GRID_COLUMN_PAYOUT_6 As Integer = 8
  Private Const GRID_COLUMN_PAYOUT_7 As Integer = 9
  Private Const GRID_COLUMN_PAYOUT_8 As Integer = 10
  Private Const GRID_COLUMN_PAYOUT_9 As Integer = 11
  Private Const GRID_COLUMN_PAYOUT_10 As Integer = 12

  Private Const GRID_COLUMNS As Integer = 13
  Private Const GRID_HEADER_ROWS_GAMES As Integer = 2
  Private Const GRID_PAYOUT_COLUMNS As Integer = 10

  Private Const GRID_WIDTH_INDEX As Integer = 150
  Private Const GRID_COLUMN_GAME_ID_WIDTH As Integer = 0
  Private Const GRID_GAME_NAME_WIDTH As Integer = 2300
  Private Const GRID_PAYOUT_WIDTH As Integer = 750


  Private Const SQL_COLUMN_PROVIDER_ID As Integer = 0
  Private Const SQL_COLUMN_GAME_ID As Integer = 1
  Private Const SQL_COLUMN_GAME_NAME As Integer = 2
  Private Const SQL_COLUMN_PAYOUT_1 As Integer = 3
  Private Const SQL_COLUMN_PAYOUT_2 As Integer = 4
  Private Const SQL_COLUMN_PAYOUT_3 As Integer = 5
  Private Const SQL_COLUMN_PAYOUT_4 As Integer = 6
  Private Const SQL_COLUMN_PAYOUT_5 As Integer = 7
  Private Const SQL_COLUMN_PAYOUT_6 As Integer = 8
  Private Const SQL_COLUMN_PAYOUT_7 As Integer = 9
  Private Const SQL_COLUMN_PAYOUT_8 As Integer = 10
  Private Const SQL_COLUMN_PAYOUT_9 As Integer = 11
  Private Const SQL_COLUMN_PAYOUT_10 As Integer = 12

#End Region 'Constants

#Region " Members "

  Private m_input_providers_name As String
  Private m_delete_operation As Boolean
  Private m_data_table_games As DataTable
  Private m_max_payout_allowed As Decimal

#End Region ' Members

#Region "Private Functions"

  ' PURPOSE: Set the uc_grid from the screen data with the values from the providers games DataTable.
  '
  '  PARAMS:
  '     - INPUT:
  '           - currencys As DataTable
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetScreenProvidersGames(ByVal ProvidersGames As DataTable)
    Dim _idx_row As Integer

    Me.dg_games.Redraw = False

    Try

      For Each _provider_game As DataRow In ProvidersGames.Rows
        Me.dg_games.AddRow()

        _idx_row = dg_games.NumRows - 1
        With Me.dg_games
          .Cell(_idx_row, GRID_COLUMN_GAME_ID).Value = _provider_game(SQL_COLUMN_GAME_ID)
          .Cell(_idx_row, GRID_COLUMN_GAME_NAME).Value = _provider_game(SQL_COLUMN_GAME_NAME)

          If Not String.IsNullOrEmpty(_provider_game(SQL_COLUMN_PAYOUT_1).ToString) Then
            .Cell(_idx_row, GRID_COLUMN_PAYOUT_1).Value = GUI_FormatNumber(_provider_game(SQL_COLUMN_PAYOUT_1), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
          Else
            .Cell(_idx_row, GRID_COLUMN_PAYOUT_1).Value = Nothing
          End If

          If Not String.IsNullOrEmpty(_provider_game(SQL_COLUMN_PAYOUT_2).ToString) Then
            .Cell(_idx_row, GRID_COLUMN_PAYOUT_2).Value = GUI_FormatNumber(_provider_game(SQL_COLUMN_PAYOUT_2), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
          Else
            .Cell(_idx_row, GRID_COLUMN_PAYOUT_2).Value = Nothing
          End If

          If Not String.IsNullOrEmpty(_provider_game(SQL_COLUMN_PAYOUT_3).ToString) Then
            .Cell(_idx_row, GRID_COLUMN_PAYOUT_3).Value = GUI_FormatNumber(_provider_game(SQL_COLUMN_PAYOUT_3), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
          Else
            .Cell(_idx_row, GRID_COLUMN_PAYOUT_3).Value = Nothing
          End If

          If Not String.IsNullOrEmpty(_provider_game(SQL_COLUMN_PAYOUT_4).ToString) Then
            .Cell(_idx_row, GRID_COLUMN_PAYOUT_4).Value = GUI_FormatNumber(_provider_game(SQL_COLUMN_PAYOUT_4), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
          Else
            .Cell(_idx_row, GRID_COLUMN_PAYOUT_4).Value = Nothing
          End If

          If Not String.IsNullOrEmpty(_provider_game(SQL_COLUMN_PAYOUT_5).ToString) Then
            .Cell(_idx_row, GRID_COLUMN_PAYOUT_5).Value = GUI_FormatNumber(_provider_game(SQL_COLUMN_PAYOUT_5), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
          Else
            .Cell(_idx_row, GRID_COLUMN_PAYOUT_5).Value = Nothing
          End If

          If Not String.IsNullOrEmpty(_provider_game(SQL_COLUMN_PAYOUT_6).ToString) Then
            .Cell(_idx_row, GRID_COLUMN_PAYOUT_6).Value = GUI_FormatNumber(_provider_game(SQL_COLUMN_PAYOUT_6), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
          Else
            .Cell(_idx_row, GRID_COLUMN_PAYOUT_6).Value = Nothing
          End If

          If Not String.IsNullOrEmpty(_provider_game(SQL_COLUMN_PAYOUT_7).ToString) Then
            .Cell(_idx_row, GRID_COLUMN_PAYOUT_7).Value = GUI_FormatNumber(_provider_game(SQL_COLUMN_PAYOUT_7), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
          Else
            .Cell(_idx_row, GRID_COLUMN_PAYOUT_7).Value = Nothing
          End If

          If Not String.IsNullOrEmpty(_provider_game(SQL_COLUMN_PAYOUT_8).ToString) Then
            .Cell(_idx_row, GRID_COLUMN_PAYOUT_8).Value = GUI_FormatNumber(_provider_game(SQL_COLUMN_PAYOUT_8), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
          Else
            .Cell(_idx_row, GRID_COLUMN_PAYOUT_8).Value = Nothing
          End If

          If Not String.IsNullOrEmpty(_provider_game(SQL_COLUMN_PAYOUT_9).ToString) Then
            .Cell(_idx_row, GRID_COLUMN_PAYOUT_9).Value = GUI_FormatNumber(_provider_game(SQL_COLUMN_PAYOUT_9), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
          Else
            .Cell(_idx_row, GRID_COLUMN_PAYOUT_9).Value = Nothing
          End If

          If Not String.IsNullOrEmpty(_provider_game(SQL_COLUMN_PAYOUT_10).ToString) Then
            .Cell(_idx_row, GRID_COLUMN_PAYOUT_10).Value = GUI_FormatNumber(_provider_game(SQL_COLUMN_PAYOUT_10), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
          Else
            .Cell(_idx_row, GRID_COLUMN_PAYOUT_10).Value = Nothing
          End If
        End With

      Next

    Catch ex As Exception

    Finally
      Me.dg_games.Redraw = True
    End Try

  End Sub      ' SetScreenProvidersGames
  ' PURPOSE: Set controls status.
  '
  '  PARAMS:
  '     - INPUT:
  '           - IsEnabled As Boolean
  '           
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub SetControlsStatus(ByVal IsEnabled As Boolean)
    ' Don't allow delete games from provider
    'dg_games.Enabled = IsEnabled

    Me.cmb_3gs_status.Enabled = Me.Permissions.Write

    If cmb_3gs_status.Enabled = False Then
      Me.ef_providers_games_3gs_ip.Enabled = False
      Me.ef_providers_games_3gs_id.Enabled = False
    End If

    Me.chk_onlyredeemable.Enabled = IsEnabled
    Me.ef_providers_games_multiplier.Enabled = False
    Me.chk_onlyredeemable.Enabled = False
    Me.ef_providers_games_name.Enabled = IsEnabled
    Me.lbl_3gs_status.Enabled = IsEnabled
    GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = Me.Permissions.Write
    Me.btn_add.Enabled = IsEnabled
    Me.btn_del.Enabled = IsEnabled

  End Sub ' SetControlsStatus

  Private Sub btn_add_ClickEvent() Handles btn_add.ClickEvent
    Dim _idx_row As Integer

    With Me.dg_games
      .ClearSelection()
      .AddRow()
      _idx_row = dg_games.NumRows - 1
      .Cell(_idx_row, GRID_COLUMN_GAME_ID).Value = -1
      .IsSelected(_idx_row) = True
      .Redraw = True
      .TopRow = _idx_row
    End With
  End Sub                  ' btn_add_ClickEvent

  ' PURPOSE: Delete row not added.
  '
  '  PARAMS:
  '     - INPUT:
  '           - IsEnabled As Boolean
  '           
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub btn_del_ClickEvent() Handles btn_del.ClickEvent

    If Me.dg_games.NumRows = 0 Then
      Exit Sub
    End If

    'Delete selected row if is possible
    If Not Me.dg_games.SelectedRows Is Nothing Then
      If Me.dg_games.Cell(Me.dg_games.SelectedRows(0), GRID_COLUMN_GAME_ID).Value <> -1 Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2007), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Else
        Me.dg_games.DeleteRow(Me.dg_games.SelectedRows(0))
      End If
    Else
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2040), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
    End If

  End Sub ' btn_del_ClickEvent

  ' PURPOSE: Update the providers games DataTable with the rejected value from uc_grid 
  '
  '  PARAMS:
  '     - INPUT:
  '           - providers games As DataTable
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GetScreenProvGames()

    Dim _idx_row As Integer
    Dim _row As DataRow
    Dim _providers_games As CLASS_PROVIDERS_GAMES
    Dim _count_payout_sql As Integer

    ' Return to initial content. (discard duplicates)
    m_data_table_games.RejectChanges()

    _providers_games = DbEditedObject
    For _idx_row = 0 To dg_games.NumRows - 1
      With Me.dg_games

        ' InsertNewRow
        If .Cell(_idx_row, GRID_COLUMN_GAME_ID).Value.ToString = -1 Then
          _row = m_data_table_games.NewRow()
          '_row(SQL_COLUMN_SITE_ID) = _providers_games.SiteId
          _row(SQL_COLUMN_PROVIDER_ID) = _providers_games.ProvidersId

        Else
          ' Update row
          _row = m_data_table_games.Select("PG_GAME_ID = '" & .Cell(_idx_row, GRID_COLUMN_GAME_ID).Value.ToString & "' ")(0)
        End If


        '''_row(SQL_COLUMN_GAME_NAME) = .Cell(_idx_row, GRID_COLUMN_GAME_NAME).Value
        If Not String.IsNullOrEmpty(.Cell(_idx_row, GRID_COLUMN_GAME_NAME).Value.ToString) Then
          ' SQL_COLUMN_PAYOUT_1
          If IsDBNull(_row(SQL_COLUMN_GAME_NAME)) Then
            _row(SQL_COLUMN_GAME_NAME) = .Cell(_idx_row, GRID_COLUMN_GAME_NAME).Value
          Else
            If _row(SQL_COLUMN_GAME_NAME) <> .Cell(_idx_row, GRID_COLUMN_GAME_NAME).Value Then
              _row(SQL_COLUMN_GAME_NAME) = .Cell(_idx_row, GRID_COLUMN_GAME_NAME).Value
            End If
          End If
        Else

          Continue For
        End If

        _count_payout_sql = SQL_COLUMN_PAYOUT_1
        For _count_payout_grid As Integer = GRID_COLUMN_PAYOUT_1 To GRID_COLUMN_PAYOUT_10
          If Not String.IsNullOrEmpty(.Cell(_idx_row, _count_payout_grid).Value.ToString) Then
            If IsDBNull(_row(_count_payout_grid)) Then
              _row(_count_payout_sql) = .Cell(_idx_row, _count_payout_grid).Value
            Else
              If _row(_count_payout_grid) <> .Cell(_idx_row, _count_payout_grid).Value Then
                _row(_count_payout_sql) = .Cell(_idx_row, _count_payout_grid).Value
              End If
            End If
          Else
            If Not IsDBNull(_row(_count_payout_grid)) Then
              _row(_count_payout_sql) = DBNull.Value
            End If
          End If

          _count_payout_sql += 1
        Next

        '''_row(SQL_COLUMN_PAYOUT_1) = IIf(String.IsNullOrEmpty(.Cell(_idx_row, GRID_COLUMN_PAYOUT_1).Value.ToString), DBNull.Value, .Cell(_idx_row, GRID_COLUMN_PAYOUT_1).Value)
        '''_row(SQL_COLUMN_PAYOUT_2) = IIf(String.IsNullOrEmpty(.Cell(_idx_row, GRID_COLUMN_PAYOUT_2).Value.ToString), DBNull.Value, .Cell(_idx_row, GRID_COLUMN_PAYOUT_2).Value)
        '''_row(SQL_COLUMN_PAYOUT_3) = IIf(String.IsNullOrEmpty(.Cell(_idx_row, GRID_COLUMN_PAYOUT_3).Value.ToString), DBNull.Value, .Cell(_idx_row, GRID_COLUMN_PAYOUT_3).Value)
        '''_row(SQL_COLUMN_PAYOUT_4) = IIf(String.IsNullOrEmpty(.Cell(_idx_row, GRID_COLUMN_PAYOUT_4).Value.ToString), DBNull.Value, .Cell(_idx_row, GRID_COLUMN_PAYOUT_4).Value)
        '''_row(SQL_COLUMN_PAYOUT_5) = IIf(String.IsNullOrEmpty(.Cell(_idx_row, GRID_COLUMN_PAYOUT_5).Value.ToString), DBNull.Value, .Cell(_idx_row, GRID_COLUMN_PAYOUT_5).Value)
        '''_row(SQL_COLUMN_PAYOUT_6) = IIf(String.IsNullOrEmpty(.Cell(_idx_row, GRID_COLUMN_PAYOUT_6).Value.ToString), DBNull.Value, .Cell(_idx_row, GRID_COLUMN_PAYOUT_6).Value)
        '''_row(SQL_COLUMN_PAYOUT_7) = IIf(String.IsNullOrEmpty(.Cell(_idx_row, GRID_COLUMN_PAYOUT_7).Value.ToString), DBNull.Value, .Cell(_idx_row, GRID_COLUMN_PAYOUT_7).Value)
        '''_row(SQL_COLUMN_PAYOUT_8) = IIf(String.IsNullOrEmpty(.Cell(_idx_row, GRID_COLUMN_PAYOUT_8).Value.ToString), DBNull.Value, .Cell(_idx_row, GRID_COLUMN_PAYOUT_8).Value)
        '''_row(SQL_COLUMN_PAYOUT_9) = IIf(String.IsNullOrEmpty(.Cell(_idx_row, GRID_COLUMN_PAYOUT_9).Value.ToString), DBNull.Value, .Cell(_idx_row, GRID_COLUMN_PAYOUT_9).Value)
        '''_row(SQL_COLUMN_PAYOUT_10) = IIf(String.IsNullOrEmpty(.Cell(_idx_row, GRID_COLUMN_PAYOUT_10).Value.ToString), DBNull.Value, .Cell(_idx_row, GRID_COLUMN_PAYOUT_10).Value)

        If .Cell(_idx_row, GRID_COLUMN_GAME_ID).Value.ToString = -1 And Not String.IsNullOrEmpty(_row(SQL_COLUMN_GAME_NAME)) Then

          m_data_table_games.Rows.Add(_row)

        End If

      End With
    Next

  End Sub     ' GetScreenCurrDenomin

#End Region ' Private Functions

#Region "Overrides"

  ' PURPOSE: To set all values in the datagrid before save.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)
    Me.panel_data.Focus()
    MyBase.GUI_ButtonClick(ButtonId)
  End Sub

  ' PURPOSE: Key Press Control
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    Select Case e.KeyChar
      Case Chr(Keys.Enter)

        If Me.dg_games.ContainsFocus Then
          Me.dg_games.KeyPressed(sender, e)
        End If
        Return True
      Case Chr(Keys.Escape)
        Return Me.dg_games.KeyPressed(sender, e)
      Case Else
    End Select

    ' The keypress event is done in uc_grid control

  End Function        ' GUI_KeyPress

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_FORM.FORM_PROVIDERS_GAMES_EDIT
    Call MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    ' - Form title
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1965)

    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_OK).Text = GLB_NLS_GUI_CONTROLS.GetString(13)
    'GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)

    Me.btn_add.Enabled = Me.Permissions.Write
    Me.btn_del.Enabled = Me.Permissions.Write

    Me.btn_add.Text = GLB_NLS_GUI_CONTROLS.GetString(12)
    Me.btn_del.Text = GLB_NLS_GUI_CONTROLS.GetString(11)

    'PROVIDER GROUPBOX
    gb_provider.Text = GLB_NLS_GUI_AUDITOR.GetString(341) ' Proveedor
    chk_onlyredeemable.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1919)  ' Redeemable only
    ef_providers_games_name.TextVisible = True
    ef_providers_games_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_PROVIDERS_NAME, 50)
    ef_providers_games_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(261) ' Nombre

    ef_providers_games_multiplier.TextVisible = True
    ef_providers_games_multiplier.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1921) ' Point(Multiplier)
    ef_providers_games_multiplier.Value = 1
    ef_providers_games_multiplier.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 5, 2)

    '3GS GROUPBOX
    gb_3gs.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1920) ' 3GS
    lbl_3gs_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(280) ' Estado
    cmb_3gs_status.Add(CLASS_PROVIDERS_GAMES.ENUM_3GS_STATUS.STATUS_NO_ACTIVATED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1694)) '1694 "Not activated"
    cmb_3gs_status.Add(CLASS_PROVIDERS_GAMES.ENUM_3GS_STATUS.STATUS_ACTIVATED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1696)) '1696 "Activated"
    cmb_3gs_status.Value = CLASS_PROVIDERS_GAMES.ENUM_3GS_STATUS.STATUS_ACTIVATED

    ef_providers_games_3gs_ip.TextVisible = True
    ef_providers_games_3gs_ip.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1925)  ' IP
    ef_providers_games_3gs_ip.SetFilter(FORMAT_IP_ADDRESS, 15)
    lbl_3gs_ip.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1925)

    lbl_3gs_ID.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2011)

    ef_providers_games_3gs_id.TextVisible = True
    ef_providers_games_3gs_id.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2011)  ' ID
    ef_providers_games_3gs_id.SetFilter(FORMAT_TEXT, 50)

    ' Player Tracking
    Call GUI_StyleSheet()

  End Sub ' GUI_InitControls

  ' PURPOSE: Define all Grid Payouts Per game Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()

    Dim _providers_games As CLASS_PROVIDERS_GAMES

    _providers_games = Me.DbEditedObject

    With Me.dg_games
      If _providers_games.IsMultisiteMember And _providers_games.MasterTablesMode = ENUM_TABLES_MASTER_MODE.MODE_MULTISITE_CENTER Then
        Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS_GAMES, False)
      Else
        Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS_GAMES, Me.Permissions.Write)
      End If

      .SelectionMode = SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Disable sortable in Main Grid Columns
      .Sortable = False

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Header(1).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False
      .Column(GRID_COLUMN_INDEX).Editable = False

      ' Game Id
      .Column(GRID_COLUMN_GAME_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_GAME_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_GAME_ID).Width = 0
      .Column(GRID_COLUMN_GAME_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      '.Column(GRID_COLUMN_GAME_ID).Editable = True

      ' Game name
      .Column(GRID_COLUMN_GAME_NAME).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(215)
      .Column(GRID_COLUMN_GAME_NAME).Header(1).Text = ""
      .Column(GRID_COLUMN_GAME_NAME).Width = GRID_GAME_NAME_WIDTH
      .Column(GRID_COLUMN_GAME_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT_CENTER
      .Column(GRID_COLUMN_GAME_NAME).Editable = True
      .Column(GRID_COLUMN_GAME_NAME).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_GAME_NAME).EditionControl.EntryField.SetFilter(FORMAT_TEXT, 50)

      ' Game Payout 1
      .Column(GRID_COLUMN_PAYOUT_1).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(303)
      .Column(GRID_COLUMN_PAYOUT_1).Header(1).Text = "1"
      .Column(GRID_COLUMN_PAYOUT_1).Width = GRID_PAYOUT_WIDTH
      .Column(GRID_COLUMN_PAYOUT_1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_PAYOUT_1).Editable = True
      .Column(GRID_COLUMN_PAYOUT_1).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_PAYOUT_1).EditionControl.EntryField.SetFilter(FORMAT_NUMBER, 6, 2)

      ' Game Payout 2
      .Column(GRID_COLUMN_PAYOUT_2).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(303)
      .Column(GRID_COLUMN_PAYOUT_2).Header(1).Text = "2"
      .Column(GRID_COLUMN_PAYOUT_2).Width = GRID_PAYOUT_WIDTH
      .Column(GRID_COLUMN_PAYOUT_2).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_PAYOUT_2).Editable = True
      .Column(GRID_COLUMN_PAYOUT_2).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_PAYOUT_2).EditionControl.EntryField.SetFilter(FORMAT_NUMBER, 6, 2)

      ' Game Payout 3
      .Column(GRID_COLUMN_PAYOUT_3).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(303)
      .Column(GRID_COLUMN_PAYOUT_3).Header(1).Text = "3"
      .Column(GRID_COLUMN_PAYOUT_3).Width = GRID_PAYOUT_WIDTH
      .Column(GRID_COLUMN_PAYOUT_3).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_PAYOUT_3).Editable = True
      .Column(GRID_COLUMN_PAYOUT_3).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_PAYOUT_3).EditionControl.EntryField.SetFilter(FORMAT_NUMBER, 6, 2)
      ' Game Payout 4
      .Column(GRID_COLUMN_PAYOUT_4).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(303)
      .Column(GRID_COLUMN_PAYOUT_4).Header(1).Text = "4"
      .Column(GRID_COLUMN_PAYOUT_4).Width = GRID_PAYOUT_WIDTH
      .Column(GRID_COLUMN_PAYOUT_4).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_PAYOUT_4).Editable = True
      .Column(GRID_COLUMN_PAYOUT_4).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_PAYOUT_4).EditionControl.EntryField.SetFilter(FORMAT_NUMBER, 6, 2)

      ' Game Payout 5
      .Column(GRID_COLUMN_PAYOUT_5).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(303)
      .Column(GRID_COLUMN_PAYOUT_5).Header(1).Text = "5"
      .Column(GRID_COLUMN_PAYOUT_5).Width = GRID_PAYOUT_WIDTH
      .Column(GRID_COLUMN_PAYOUT_5).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_PAYOUT_5).Editable = True
      .Column(GRID_COLUMN_PAYOUT_5).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_PAYOUT_5).EditionControl.EntryField.SetFilter(FORMAT_NUMBER, 6, 2)

      ' Game Payout 6
      .Column(GRID_COLUMN_PAYOUT_6).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(303)
      .Column(GRID_COLUMN_PAYOUT_6).Header(1).Text = "6"
      .Column(GRID_COLUMN_PAYOUT_6).Width = GRID_PAYOUT_WIDTH
      .Column(GRID_COLUMN_PAYOUT_6).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_PAYOUT_6).Editable = True
      .Column(GRID_COLUMN_PAYOUT_6).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_PAYOUT_6).EditionControl.EntryField.SetFilter(FORMAT_NUMBER, 6, 2)

      ' Game Payout 7
      .Column(GRID_COLUMN_PAYOUT_7).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(303)
      .Column(GRID_COLUMN_PAYOUT_7).Header(1).Text = "7"
      .Column(GRID_COLUMN_PAYOUT_7).Width = GRID_PAYOUT_WIDTH
      .Column(GRID_COLUMN_PAYOUT_7).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_PAYOUT_7).Editable = True
      .Column(GRID_COLUMN_PAYOUT_7).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_PAYOUT_7).EditionControl.EntryField.SetFilter(FORMAT_NUMBER, 6, 2)

      ' Game Payout 8
      .Column(GRID_COLUMN_PAYOUT_8).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(303)
      .Column(GRID_COLUMN_PAYOUT_8).Header(1).Text = "8"
      .Column(GRID_COLUMN_PAYOUT_8).Width = GRID_PAYOUT_WIDTH
      .Column(GRID_COLUMN_PAYOUT_8).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_PAYOUT_8).Editable = True
      .Column(GRID_COLUMN_PAYOUT_8).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_PAYOUT_8).EditionControl.EntryField.SetFilter(FORMAT_NUMBER, 6, 2)

      ' Game Payout 9
      .Column(GRID_COLUMN_PAYOUT_9).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(303)
      .Column(GRID_COLUMN_PAYOUT_9).Header(1).Text = "9"
      .Column(GRID_COLUMN_PAYOUT_9).Width = GRID_PAYOUT_WIDTH
      .Column(GRID_COLUMN_PAYOUT_9).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_PAYOUT_9).Editable = True
      .Column(GRID_COLUMN_PAYOUT_9).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_PAYOUT_9).EditionControl.EntryField.SetFilter(FORMAT_NUMBER, 6, 2)

      ' Game Payout 10
      .Column(GRID_COLUMN_PAYOUT_10).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(303)
      .Column(GRID_COLUMN_PAYOUT_10).Header(1).Text = "10"
      .Column(GRID_COLUMN_PAYOUT_10).Width = GRID_PAYOUT_WIDTH
      .Column(GRID_COLUMN_PAYOUT_10).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_PAYOUT_10).Editable = True
      .Column(GRID_COLUMN_PAYOUT_10).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_PAYOUT_10).EditionControl.EntryField.SetFilter(FORMAT_NUMBER, 6, 2)

    End With

  End Sub           ' GUI_StyleSheet


  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _providers_games As CLASS_PROVIDERS_GAMES

    _providers_games = Me.DbEditedObject
    m_data_table_games = New DataTable()
    m_data_table_games = _providers_games.DataProvidersGames
    m_data_table_games.AcceptChanges()

    'm_data_table_games_correct = New DataTable()
    'm_data_table_games_correct = _providers_games.DataProvidersGames.Copy()
    'm_data_table_games_correct.AcceptChanges()


    Try

      SetScreenProvidersGames(_providers_games.DataProvidersGames)

      Me.cmb_3gs_status.Value = _providers_games.Pv3gs
      Me.ef_providers_games_3gs_ip.Value = _providers_games.Pv3gsVendorIp
      Me.ef_providers_games_3gs_id.Value = _providers_games.Pv3gsVendorId

      Me.ef_providers_games_name.Value = _providers_games.Name
      Me.ef_providers_games_multiplier.Value = _providers_games.PointsMultiplier
      Me.chk_onlyredeemable.Checked = _providers_games.OnlyRedeemable

      ' Permissions
      If _providers_games.IsMultisiteMember And _providers_games.MasterTablesMode = ENUM_TABLES_MASTER_MODE.MODE_MULTISITE_CENTER Then
        SetControlsStatus(False)
      Else
        SetControlsStatus(Me.Permissions.Write)
      End If

    Catch ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                                  Me.Name, _
                                  "GUI_SetScreenData", _
                                  ex.Message)

      ' 137 "Exception error has been found: \n%1"
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(137), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ex.Message)

    Finally

    End Try

  End Sub ' GUI_SetScreenData

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_GetScreenData()

    Dim _providers_games_item_edit As CLASS_PROVIDERS_GAMES

    Try

      _providers_games_item_edit = Me.DbEditedObject

      With _providers_games_item_edit

        .Pv3gs = cmb_3gs_status.Value
        .Pv3gsVendorIp = ef_providers_games_3gs_ip.Value
        .Pv3gsVendorId = ef_providers_games_3gs_id.Value
        .Name = ef_providers_games_name.Value
        .OnlyRedeemable = chk_onlyredeemable.Checked
        .PointsMultiplier = ef_providers_games_multiplier.Value
        GetScreenProvGames()

        .DataProvidersGames = m_data_table_games.Copy()

      End With

    Catch ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, Me.Name, "GUI_GetScreenData", ex.Message)
    Finally

    End Try

  End Sub ' GUI_GetScreenData

  ' PURPOSE: Validate the data presented on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Dim _sb As StringBuilder
    Dim _result As Integer
    Dim _pv_id As Integer
    Dim _providers_games As CLASS_PROVIDERS_GAMES
    Dim _idx_row As Integer
    Dim _idx_row2 As Integer
    Dim _game_name As String
    Dim _count_payout As Integer
    Dim _current_payout As Decimal


    If ef_providers_games_name.Value.Length = 0 Then
      Call ef_providers_games_name.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_providers_games_name.Text)
      Return False
    End If

    If Not ef_providers_games_name.ValidateFormat Then
      Call ef_providers_games_name.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
            mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.ef_providers_games_name.Text)
      Return False
    End If

    _sb = New StringBuilder()
    _sb.AppendLine(" SELECT COUNT(PV_NAME) ")
    _sb.AppendLine("   FROM   PROVIDERS")
    _sb.AppendLine("  WHERE   PV_NAME = @pProviderName")

    _providers_games = DbEditedObject
    If Not IsNothing(_providers_games.ProvidersId) Then
      If _providers_games.ProvidersId <> 0 Then
        _sb.AppendLine("  AND  PV_ID <> @pProviderId")
      End If
    End If

    Try
      Using _db_trx As New DB_TRX()
        Using _cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _cmd.Parameters.Add("@pProviderName", SqlDbType.NVarChar, 50).Value = ef_providers_games_name.Value
          If Not IsNothing(_providers_games.ProvidersId) Then
            If _providers_games.ProvidersId <> 0 Then
              _cmd.Parameters.Add("@pProviderId", SqlDbType.Int).Value = _providers_games.ProvidersId
            End If
          End If

          _result = _cmd.ExecuteScalar()
        End Using
      End Using

      If _result > 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1957), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_providers_games_name.Value)
        Call ef_providers_games_name.Focus()
        Return False
      End If

      If cmb_3gs_status.TextValue(cmb_3gs_status.SelectedIndex) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1696) And String.IsNullOrEmpty(ef_providers_games_3gs_ip.Value) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1991), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
        Call ef_providers_games_3gs_ip.Focus()

        Return False
      End If

      _pv_id = IIf(String.IsNullOrEmpty(_providers_games.ProvidersId), -1, _providers_games.ProvidersId)

      For _idx_row = 0 To dg_games.NumRows - 1

        m_max_payout_allowed = 100.0

        With Me.dg_games
          If String.IsNullOrEmpty(.Cell(_idx_row, GRID_COLUMN_GAME_NAME).Value) Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1923))
            Call dg_games.Focus()
            Return False
          End If

          _game_name = .Cell(_idx_row, GRID_COLUMN_GAME_NAME).Value
          For _idx_row2 = _idx_row To dg_games.NumRows - 1
            If _game_name = .Cell(_idx_row2, GRID_COLUMN_GAME_NAME).Value And _idx_row2 <> _idx_row Then
              Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1963), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _game_name)
              Call dg_games.Focus()
              Return False
            End If
          Next

          For _count_payout = GRID_COLUMN_PAYOUT_1 To GRID_COLUMN_PAYOUT_10
            _current_payout = GUI_FormatNumber(IIf(String.IsNullOrEmpty(.Cell(_idx_row, _count_payout).Value), 0.0, .Cell(_idx_row, _count_payout).Value), 2, , 8)
            If _current_payout > m_max_payout_allowed Then

              Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2013), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _
              .Column(_count_payout).Header(1).Text, _game_name)

              Call dg_games.Focus()
              Return False
            End If
          Next

        End With

      Next

    Catch ex As Exception
      Return False
    End Try

    Return True

  End Function ' GUI_IsScreenDataOk

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)

    Dim _providers_games As CLASS_PROVIDERS_GAMES
    Dim _aux_nls As String
    Dim _rc As mdl_NLS.ENUM_MB_RESULT
    Dim _bln_providers As Boolean

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE

        DbEditedObject = New CLASS_PROVIDERS_GAMES
        _providers_games = DbEditedObject
        _providers_games.ProvidersId = 0
        _providers_games.PointsMultiplier = 1

        DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ

        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          '1958 "Error producido al leer el proveedor."
          _providers_games = Me.DbEditedObject
          _aux_nls = m_input_providers_name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1958), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT

        If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          ' 1957 "Ya existe un proveedor con el nombre %1."
          _providers_games = Me.DbEditedObject
          _aux_nls = _providers_games.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1957), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
          Call ef_providers_games_name.Focus()
        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          ' 1959 "Se ha producido un error al a�adir el proveedor %1."
          _providers_games = Me.DbEditedObject
          _aux_nls = _providers_games.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1959), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE

        _providers_games = Me.DbEditedObject
        Call MyBase.GUI_DB_Operation(frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE)

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_UPDATE

        Call MyBase.GUI_DB_Operation(DbOperation)

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE

        If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          ' 1957 "Ya existe proveedor con el nombre %1."
          _providers_games = Me.DbEditedObject
          _aux_nls = _providers_games.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1957), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
          Call ef_providers_games_name.Focus()

        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          ' 1960 "Se ha producido un error al modificar el proveedor %1."
          _providers_games = Me.DbEditedObject
          _aux_nls = _providers_games.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1960), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE

        m_delete_operation = False
        _bln_providers = False
        ' 1962 "�Est� seguro que quiere borrar el proveedor? %1?"
        _providers_games = Me.DbEditedObject
        _aux_nls = _providers_games.Name

        _rc = NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1962), _
                         ENUM_MB_TYPE.MB_TYPE_WARNING, _
                         ENUM_MB_BTN.MB_BTN_YES_NO, _
                         ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
                        _aux_nls)

        If _rc = ENUM_MB_RESULT.MB_RESULT_YES Then
          m_delete_operation = True
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE

        If Me.DbStatus <> ENUM_STATUS.STATUS_OK And m_delete_operation Then
          ' 1961 "Se ha producido un error al borrar proveedor %1."
          _providers_games = Me.DbEditedObject
          _aux_nls = _providers_games.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1961), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_DELETE

        If m_delete_operation Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        Else
          DbStatus = ENUM_STATUS.STATUS_ERROR
        End If

      Case Else

        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE : Init form in edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '       - UserId
  '       - Username
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '
  Public Overloads Sub ShowEditItem(ByVal ProviderId As Integer)

    Dim _table As DataTable

    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
    Me.DbObjectId = ProviderId

    _table = GUI_GetTableUsingSQL("SELECT PV_NAME FROM PROVIDERS WHERE PV_ID = " & ProviderId, Integer.MaxValue)

    If Not IsNothing(_table) Then
      If _table.Rows.Count() > 0 Then
        Me.m_input_providers_name = _table.Rows.Item(0).Item("PV_NAME")
      End If
    End If

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub

#End Region ' Overrides

#Region "Events"

  ' PURPOSE : Handle valueChanged event.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '
  Private Sub cmb_3gs_status_ValueChangedEvent() Handles cmb_3gs_status.ValueChangedEvent

    If cmb_3gs_status.Value = CLASS_PROVIDERS_GAMES.ENUM_3GS_STATUS.STATUS_NO_ACTIVATED Or cmb_3gs_status.Enabled = False Then
      ef_providers_games_3gs_ip.Enabled = False
      ef_providers_games_3gs_id.Enabled = False
    Else
      ef_providers_games_3gs_ip.Enabled = True
      ef_providers_games_3gs_id.Enabled = True
      ef_providers_games_3gs_ip.Focus()
    End If

  End Sub

#End Region ' Events

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

  End Sub ' Constructor

End Class