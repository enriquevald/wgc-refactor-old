'-------------------------------------------------------------------
' Copyright � 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
'   MODULE NAME : frm_cage_safe_keeping_sel
'   DESCRIPTION : Selection safe keeping accounts
'        AUTHOR : Alberto Marcos
' CREATION DATE : 09-APR-2015
'
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 09-APR-2015  AMF    Initial version. Backlog Item 680
' 02-JUL-2015  SGB    Fixed bug 2522: Disable button "Edit" if select row "Total" in grid.
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data

Public Class frm_cage_safe_keeping_sel
  Inherits frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub
  Friend WithEvents gb_blocked As System.Windows.Forms.GroupBox
  Friend WithEvents chk_blocked_no As System.Windows.Forms.CheckBox
  Friend WithEvents chk_blocked_yes As System.Windows.Forms.CheckBox
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents chk_with_amount As System.Windows.Forms.CheckBox
  Friend WithEvents gb_filter As System.Windows.Forms.GroupBox
  Friend WithEvents ef_owner_document As GUI_Controls.uc_entry_field
  Friend WithEvents ef_owner_id As GUI_Controls.uc_entry_field
  Friend WithEvents ef_owner_name As GUI_Controls.uc_entry_field

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.gb_blocked = New System.Windows.Forms.GroupBox()
    Me.chk_blocked_no = New System.Windows.Forms.CheckBox()
    Me.chk_blocked_yes = New System.Windows.Forms.CheckBox()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.chk_with_amount = New System.Windows.Forms.CheckBox()
    Me.gb_filter = New System.Windows.Forms.GroupBox()
    Me.ef_owner_document = New GUI_Controls.uc_entry_field()
    Me.ef_owner_id = New GUI_Controls.uc_entry_field()
    Me.ef_owner_name = New GUI_Controls.uc_entry_field()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_blocked.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_filter.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_filter)
    Me.panel_filter.Controls.Add(Me.chk_with_amount)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Controls.Add(Me.gb_blocked)
    Me.panel_filter.Size = New System.Drawing.Size(1025, 105)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_blocked, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_with_amount, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_filter, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 109)
    Me.panel_data.Size = New System.Drawing.Size(1025, 408)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1019, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1019, 4)
    '
    'gb_blocked
    '
    Me.gb_blocked.Controls.Add(Me.chk_blocked_no)
    Me.gb_blocked.Controls.Add(Me.chk_blocked_yes)
    Me.gb_blocked.Location = New System.Drawing.Point(564, 7)
    Me.gb_blocked.Name = "gb_blocked"
    Me.gb_blocked.Size = New System.Drawing.Size(92, 71)
    Me.gb_blocked.TabIndex = 2
    Me.gb_blocked.TabStop = False
    Me.gb_blocked.Text = "xEnabled"
    '
    'chk_blocked_no
    '
    Me.chk_blocked_no.AutoSize = True
    Me.chk_blocked_no.Location = New System.Drawing.Point(16, 42)
    Me.chk_blocked_no.Name = "chk_blocked_no"
    Me.chk_blocked_no.Size = New System.Drawing.Size(48, 17)
    Me.chk_blocked_no.TabIndex = 1
    Me.chk_blocked_no.Text = "xNo"
    Me.chk_blocked_no.UseVisualStyleBackColor = True
    '
    'chk_blocked_yes
    '
    Me.chk_blocked_yes.AutoSize = True
    Me.chk_blocked_yes.Location = New System.Drawing.Point(16, 19)
    Me.chk_blocked_yes.Name = "chk_blocked_yes"
    Me.chk_blocked_yes.Size = New System.Drawing.Size(52, 17)
    Me.chk_blocked_yes.TabIndex = 0
    Me.chk_blocked_yes.Text = "xYes"
    Me.chk_blocked_yes.UseVisualStyleBackColor = True
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(320, 7)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(237, 93)
    Me.gb_date.TabIndex = 1
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xCreationDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(11, 47)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(220, 25)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 42
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(11, 19)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(220, 25)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 42
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'chk_with_amount
    '
    Me.chk_with_amount.AutoSize = True
    Me.chk_with_amount.Location = New System.Drawing.Point(564, 83)
    Me.chk_with_amount.Name = "chk_with_amount"
    Me.chk_with_amount.Size = New System.Drawing.Size(106, 17)
    Me.chk_with_amount.TabIndex = 3
    Me.chk_with_amount.Text = "xWith Amount"
    Me.chk_with_amount.UseVisualStyleBackColor = True
    '
    'gb_filter
    '
    Me.gb_filter.Controls.Add(Me.ef_owner_document)
    Me.gb_filter.Controls.Add(Me.ef_owner_id)
    Me.gb_filter.Controls.Add(Me.ef_owner_name)
    Me.gb_filter.Location = New System.Drawing.Point(18, 7)
    Me.gb_filter.Name = "gb_filter"
    Me.gb_filter.Size = New System.Drawing.Size(295, 93)
    Me.gb_filter.TabIndex = 0
    Me.gb_filter.TabStop = False
    '
    'ef_owner_document
    '
    Me.ef_owner_document.DoubleValue = 0.0R
    Me.ef_owner_document.IntegerValue = 0
    Me.ef_owner_document.IsReadOnly = False
    Me.ef_owner_document.Location = New System.Drawing.Point(11, 63)
    Me.ef_owner_document.Name = "ef_owner_document"
    Me.ef_owner_document.PlaceHolder = Nothing
    Me.ef_owner_document.Size = New System.Drawing.Size(273, 24)
    Me.ef_owner_document.SufixText = "Document"
    Me.ef_owner_document.SufixTextVisible = True
    Me.ef_owner_document.TabIndex = 2
    Me.ef_owner_document.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_owner_document.TextValue = ""
    Me.ef_owner_document.TextWidth = 75
    Me.ef_owner_document.Value = ""
    Me.ef_owner_document.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_owner_id
    '
    Me.ef_owner_id.DoubleValue = 0.0R
    Me.ef_owner_id.IntegerValue = 0
    Me.ef_owner_id.IsReadOnly = False
    Me.ef_owner_id.Location = New System.Drawing.Point(11, 13)
    Me.ef_owner_id.Name = "ef_owner_id"
    Me.ef_owner_id.PlaceHolder = Nothing
    Me.ef_owner_id.Size = New System.Drawing.Size(273, 24)
    Me.ef_owner_id.SufixText = "ID"
    Me.ef_owner_id.SufixTextVisible = True
    Me.ef_owner_id.TabIndex = 0
    Me.ef_owner_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_owner_id.TextValue = ""
    Me.ef_owner_id.TextWidth = 75
    Me.ef_owner_id.Value = ""
    Me.ef_owner_id.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_owner_name
    '
    Me.ef_owner_name.DoubleValue = 0.0R
    Me.ef_owner_name.IntegerValue = 0
    Me.ef_owner_name.IsReadOnly = False
    Me.ef_owner_name.Location = New System.Drawing.Point(11, 38)
    Me.ef_owner_name.Name = "ef_owner_name"
    Me.ef_owner_name.PlaceHolder = Nothing
    Me.ef_owner_name.Size = New System.Drawing.Size(273, 24)
    Me.ef_owner_name.SufixText = "Name"
    Me.ef_owner_name.SufixTextVisible = True
    Me.ef_owner_name.TabIndex = 1
    Me.ef_owner_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_owner_name.TextValue = ""
    Me.ef_owner_name.TextWidth = 75
    Me.ef_owner_name.Value = ""
    Me.ef_owner_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'frm_cage_safe_keeping_sel
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(1033, 521)
    Me.Name = "frm_cage_safe_keeping_sel"
    Me.Text = "frm_terminal_meter_delta_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_blocked.ResumeLayout(False)
    Me.gb_blocked.PerformLayout()
    Me.gb_date.ResumeLayout(False)
    Me.gb_filter.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region ' Windows Form Designer generated code 

#Region " Constants "

  Private Const SQL_COLUMN_IDENTIFIER As Integer = 0
  Private Const SQL_COLUMN_NAME As Integer = 1
  Private Const SQL_COLUMN_DOCUMENT_TYPE As Integer = 2
  Private Const SQL_COLUMN_DOCUMENT_ID As Integer = 3
  Private Const SQL_COLUMN_BLOCKED As Integer = 4
  Private Const SQL_COLUMN_AMOUNT As Integer = 5
  Private Const SQL_COLUMN_LAST_OPERATION As Integer = 6

  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_IDENTIFIER As Integer = 1
  Private Const GRID_COLUMN_NAME As Integer = 2
  Private Const GRID_COLUMN_DOCUMENT As Integer = 3
  Private Const GRID_COLUMN_BLOCKED As Integer = 4
  Private Const GRID_COLUMN_AMOUNT As Integer = 5
  Private Const GRID_COLUMN_LAST_OPERATION As Integer = 6

  Private Const GRID_COLUMNS As Integer = 7
  Private Const GRID_HEADER_ROWS As Integer = 1

  Private Const FORM_DB_MIN_VERSION As Short = 1 'ALBERTO TODO

#End Region ' Constants

#Region " Members "

  ' For report filters 
  Private m_id As String
  Private m_name As String
  Private m_document As String
  Private m_blocked As String
  Private m_date_from As String
  Private m_date_to As String
  Private m_with_amount As String

  ' For Total
  Private m_safe_keeping_amount As Double

#End Region ' Members

#Region " OVERRIDES "

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CAGE_SAFE_KEEPING_SEL
    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)

    'Call Base Form proc
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6103)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Text = GLB_NLS_GUI_CONTROLS.GetString(25)

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1)

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(2)

    ' Deposit / Withdraw
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Type = uc_button.ENUM_BUTTON_TYPE.USER
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6110)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Height += 15

    ' Filters
    Me.gb_filter.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6200)

    Me.ef_owner_id.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6104)
    Me.ef_owner_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 20, 0)

    Me.ef_owner_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6062)
    Me.ef_owner_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 200)

    Me.ef_owner_document.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6106)
    Me.ef_owner_document.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 20)

    ' Status
    Me.gb_blocked.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(558) ' Locked
    Me.chk_blocked_yes.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(698) ' Yes
    Me.chk_blocked_no.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(699) ' No

    ' Last operation
    Me.gb_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6107)
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    ' With amount
    Me.chk_with_amount.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6108)

    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()

  End Sub ' GUI_FilterReset

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_SELECT
        Call ShowAccountDetails()

      Case ENUM_BUTTON.BUTTON_NEW
        Call AddNewAccount()

      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        Call DepositWithDraw()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Enable button in selected row
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_RowSelectedEvent(ByVal SelectedRow As Integer)

    If Not IsValidDataRow(SelectedRow) Then
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False
      Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Enabled = False

      Return
    Else
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = True
      Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Enabled = True

    End If

    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_CAGE_SAFE_KEEPING_ADD_DEPOSIT_MOVEMENT).Read OrElse _
                                                         CurrentUser.Permissions(ENUM_FORM.FORM_CAGE_SAFE_KEEPING_ADD_WITHDRAW_MOVEMENT).Read

  End Sub ' GUI_RowSelectedEvent

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_BeforeFirstRow()

    'Total
    m_safe_keeping_amount = 0

  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE: Perform final processing for the grid data
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_AfterLastRow()

    Dim _idx_row As Integer

    Me.Grid.AddRow()

    _idx_row = Me.Grid.NumRows - 1

    ' Total
    Me.Grid.Cell(_idx_row, GRID_COLUMN_BLOCKED).Value = GLB_NLS_GUI_STATISTICS.GetString(203)

    ' Played Amount
    Me.Grid.Cell(_idx_row, GRID_COLUMN_AMOUNT).Value = GUI_FormatCurrency(m_safe_keeping_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Color Row
    Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

  End Sub ' GUI_AfterLastRow

  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Last operation
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    Return True

  End Function ' GUI_FilterCheck

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _sb As System.Text.StringBuilder

    _sb = New System.Text.StringBuilder()

    _sb.AppendLine(" SELECT   SKA_SAFE_KEEPING_ID ")
    _sb.AppendLine("        , SKA_OWNER_NAME ")
    _sb.AppendLine("        , SKA_OWNER_DOCUMENT_TYPE ")
    _sb.AppendLine("        , SKA_OWNER_DOCUMENT_ID ")
    _sb.AppendLine("        , ISNULL(SKA_BLOCK_REASON, 0) ")
    _sb.AppendLine("        , SKA_BALANCE ")
    _sb.AppendLine("        , SKA_LAST_OPERATION ")
    _sb.AppendLine("   FROM   SAFE_KEEPING_ACCOUNTS ")

    _sb.AppendLine(GetSqlWhere())

    _sb.AppendLine(" ORDER BY SKA_SAFE_KEEPING_ID ")

    Return _sb.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    ' ID
    Me.Grid.Cell(RowIndex, GRID_COLUMN_IDENTIFIER).Value = DbRow.Value(SQL_COLUMN_IDENTIFIER)

    ' Name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NAME).Value = DbRow.Value(SQL_COLUMN_NAME)

    ' Document
    If Not DbRow.IsNull(SQL_COLUMN_DOCUMENT_ID) AndAlso Not String.IsNullOrEmpty(DbRow.Value(SQL_COLUMN_DOCUMENT_ID)) Then
      Grid.Cell(RowIndex, GRID_COLUMN_DOCUMENT).Value = String.Format("{0} - {1}", WSI.Common.IdentificationTypes.DocIdTypeString(DbRow.Value(SQL_COLUMN_DOCUMENT_TYPE)), DbRow.Value(SQL_COLUMN_DOCUMENT_ID))
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DOCUMENT).Value = ""
    End If

    ' Locked
    If DbRow.Value(SQL_COLUMN_BLOCKED) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_BLOCKED).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(558) ' Locked
    End If

    ' Amount
    Me.Grid.Cell(RowIndex, GRID_COLUMN_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_AMOUNT), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    m_safe_keeping_amount += DbRow.Value(SQL_COLUMN_AMOUNT)

    ' Last operation
    If Not DbRow.IsNull(SQL_COLUMN_LAST_OPERATION) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_LAST_OPERATION).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_LAST_OPERATION), _
                                                                               ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                               ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    Return True

  End Function ' GUI_SetupRow

  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.ef_owner_id

  End Sub ' GUI_SetInitialFocus

#Region " GUI Reports "

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6200) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6104).ToLower, m_id)

    If String.IsNullOrEmpty(m_id) Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6200) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6062).ToLower, m_name)
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6200) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6106).ToLower, m_document)
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6107) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6107) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)
      PrintData.SetFilter(Me.gb_blocked.Text, m_blocked)
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6108), m_with_amount)
    End If

  End Sub ' GUI_ReportFilter

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_id = String.Empty
    m_name = String.Empty
    m_document = String.Empty
    m_blocked = String.Empty
    m_date_from = String.Empty
    m_date_to = String.Empty
    m_with_amount = String.Empty

    ' ID
    m_id = Me.ef_owner_id.Value

    ' Name
    m_name = Me.ef_owner_name.Value

    ' Document
    m_document = Me.ef_owner_document.Value

    ' Filter blocked
    If Me.chk_blocked_yes.Checked And Not Me.chk_blocked_no.Checked Then
      m_blocked = Me.chk_blocked_yes.Text
    End If

    If Not Me.chk_blocked_yes.Checked And Me.chk_blocked_no.Checked Then
      m_blocked = Me.chk_blocked_no.Text
    End If

    If (Not Me.chk_blocked_yes.Checked And Not Me.chk_blocked_no.Checked) Or (Me.chk_blocked_yes.Checked And Me.chk_blocked_no.Checked) Then
      m_blocked = Me.chk_blocked_yes.Text & " / " & Me.chk_blocked_no.Text ' Yes / NO
    End If

    ' Last operation
    If Me.dtp_from.Checked Then
      m_date_from = GUI_FormatDate(dtp_from.Value, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If
    If Me.dtp_to.Checked Then
      m_date_to = GUI_FormatDate(dtp_to.Value, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    ' With amount
    m_with_amount = IIf(Me.chk_with_amount.Checked, GLB_NLS_GUI_PLAYER_TRACKING.GetString(359), GLB_NLS_GUI_PLAYER_TRACKING.GetString(360))

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE : Opens dialog with default settings for edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Checks whether the specified row contains data or not
  '
  '  PARAMS:
  '     - INPUT:
  '           - IdxRow: row to check
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: selected row contains data
  '     - FALSE: selected row does not exist or contains no data
  Private Function IsValidDataRow(ByVal IdxRow As Integer) As Boolean

    If IdxRow >= 0 And IdxRow < Me.Grid.NumRows Then
      ' All rows with data have a non-empty value in column BLOCKED_INT.
      Return Not String.IsNullOrEmpty(Me.Grid.Cell(IdxRow, GRID_COLUMN_IDENTIFIER).Value)
    End If

    Return False
  End Function ' IsValidDataRow

  ' PURPOSE : Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS :
  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Index
      .Column(GRID_COLUMN_INDEX).Header.Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 150
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Identifier
      .Column(GRID_COLUMN_IDENTIFIER).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6104)
      .Column(GRID_COLUMN_IDENTIFIER).Width = 1150
      .Column(GRID_COLUMN_IDENTIFIER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Name
      .Column(GRID_COLUMN_NAME).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6105)
      .Column(GRID_COLUMN_NAME).Width = 3887
      .Column(GRID_COLUMN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Document
      .Column(GRID_COLUMN_DOCUMENT).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6106)
      .Column(GRID_COLUMN_DOCUMENT).Width = 2900
      .Column(GRID_COLUMN_DOCUMENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Blocked
      .Column(GRID_COLUMN_BLOCKED).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(558)
      .Column(GRID_COLUMN_BLOCKED).Width = 1147
      .Column(GRID_COLUMN_BLOCKED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Amount
      .Column(GRID_COLUMN_AMOUNT).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6109)
      .Column(GRID_COLUMN_AMOUNT).Width = 1700
      .Column(GRID_COLUMN_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Last operation
      .Column(GRID_COLUMN_LAST_OPERATION).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6107)
      .Column(GRID_COLUMN_LAST_OPERATION).Width = 1750
      .Column(GRID_COLUMN_LAST_OPERATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE : Set default values to filters
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub SetDefaultValues()
    Dim initial_time As Date

    ' ID
    Me.ef_owner_id.Value = String.Empty

    ' Name
    Me.ef_owner_name.Value = String.Empty

    ' Document
    Me.ef_owner_document.Value = String.Empty

    ' Blocked
    Me.chk_blocked_yes.Checked = False
    Me.chk_blocked_no.Checked = False

    ' Last operation
    initial_time = WSI.Common.Misc.TodayOpening()
    Me.dtp_from.Value = initial_time
    Me.dtp_from.Checked = False
    Me.dtp_to.Value = Me.dtp_from.Value.AddDays(1)
    Me.dtp_to.Checked = False

    ' With amount
    Me.chk_with_amount.Checked = False

    Call EnableFilters(True)

  End Sub ' SetDefaultValues

  ' PURPOSE : Get the SQL WHERE
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Function GetSqlWhere() As String

    Dim _str_where As String
    Dim _str_aux1 As String
    Dim _some_filter As Boolean

    _str_where = String.Empty
    _some_filter = False

    If Me.ef_owner_id.Value <> String.Empty Then
      ' ID
      _str_where += " WHERE   SKA_SAFE_KEEPING_ID = " & Me.ef_owner_id.Value
    Else
      ' Name
      If Me.ef_owner_name.Value <> String.Empty Then
        _str_aux1 = UCase(Me.ef_owner_name.Value).Replace("'", "''")
        _str_where += " WHERE ( UPPER(SKA_OWNER_NAME) LIKE '" & _str_aux1 & "%' OR"
        _str_where += "         UPPER(SKA_OWNER_NAME) LIKE '% " & _str_aux1 & "%') "
        _some_filter = True
      End If

      ' Document
      If Me.ef_owner_document.Value <> String.Empty Then
        _str_where += IIf(_some_filter, " AND ", " WHERE ")
        _str_aux1 = UCase(Me.ef_owner_document.Value).Replace("'", "''")
        _str_where += " ( UPPER(SKA_OWNER_DOCUMENT_ID) LIKE '%" & _str_aux1 & "%') "
        _some_filter = True
      End If

      ' Block
      If Me.chk_blocked_yes.Checked = True And Not Me.chk_blocked_no.Checked Then
        _str_where += IIf(_some_filter, " AND ", " WHERE ")
        _str_where += " SKA_BLOCK_REASON = " & 1 & " "
        _some_filter = True
      End If
      If Not Me.chk_blocked_yes.Checked = True And Me.chk_blocked_no.Checked Then
        _str_where += IIf(_some_filter, " AND ", " WHERE ")
        _str_where += " SKA_BLOCK_REASON = " & 0 & " "
        _some_filter = True
      End If

      ' Last operation
      If Me.dtp_from.Checked = True Then
        _str_where += IIf(_some_filter, " AND ", " WHERE ")
        _str_where += " (SKA_LAST_OPERATION >= " & GUI_FormatDateDB(dtp_from.Value) & ") "
        _some_filter = True
      End If
      If Me.dtp_to.Checked = True Then
        _str_where += IIf(_some_filter, " AND ", " WHERE ")
        _str_where += " (SKA_LAST_OPERATION < " & GUI_FormatDateDB(dtp_to.Value) & ") "
        _some_filter = True
      End If

      ' With amount
      If Me.chk_with_amount.Checked Then
        _str_where += IIf(_some_filter, " AND ", " WHERE ")
        _str_where += " SKA_BALANCE > 0 "
        _some_filter = True
      End If

    End If

    Return _str_where

  End Function ' GetSqlWhere

  ' PURPOSE : Adds a new software version package to the system
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub ShowAccountDetails()
    Dim _idx_row As Short
    Dim _row_selected As Boolean
    Dim _frm_edit As frm_cage_safe_keeping_edit

    ' Get the selected row
    _row_selected = False
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        _row_selected = True

        Exit For
      End If
    Next

    If Not _row_selected Then
      ' No selected row 
      Call Me.Grid.Focus()

      Exit Sub
    End If

    If Not Me.Grid.Cell(_idx_row, GRID_COLUMN_IDENTIFIER).Value = Nothing OrElse Not Me.Grid.Cell(_idx_row, GRID_COLUMN_IDENTIFIER).Value = "" Then
      _frm_edit = New frm_cage_safe_keeping_edit

      ' Show the selected Licence
      Call _frm_edit.ShowEditItem(Me.Grid.Cell(_idx_row, GRID_COLUMN_IDENTIFIER).Value)

    Else
      Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Enabled = False

    End If

    _frm_edit = Nothing

    Call Me.Grid.Focus()

  End Sub ' ShowAccountDetails

  ' PURPOSE : Adds a new account
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub AddNewAccount()

    Dim _frm_edit As frm_cage_safe_keeping_edit

    _frm_edit = New frm_cage_safe_keeping_edit

    Call _frm_edit.ShowNewItem()

    _frm_edit = Nothing

    Call Me.Grid.Focus()

  End Sub ' AddNewAccount

  ' PURPOSE : Deposit or Withdraw account
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub DepositWithDraw()
    Dim _account_id As Int64
    Dim _frm As frm_cage_safe_keeping_add_movement

    _account_id = Me.Grid.Cell(Me.Grid.SelectedRows()(0), GRID_COLUMN_IDENTIFIER).Value
    _frm = New frm_cage_safe_keeping_add_movement()
    _frm.ShowEditItem(_account_id)
  End Sub ' DepositWithDraw

  ' PURPOSE : Enable/Disable the filters
  '
  '  PARAMS :
  '     - INPUT  : Boolean indicating if it must enable (true) or disable (false) the filters
  '
  '     - OUTPUT :
  Private Sub EnableFilters(ByVal EnableFilters As Boolean)

    Me.ef_owner_name.Enabled = EnableFilters
    Me.ef_owner_document.Enabled = EnableFilters
    Me.gb_date.Enabled = EnableFilters
    Me.gb_blocked.Enabled = EnableFilters
    Me.chk_with_amount.Enabled = EnableFilters

  End Sub ' EnableFilters

#End Region ' Private Functions

#Region "Events"

  Private Sub ef_owner_id_EntryFieldValueChanged() Handles ef_owner_id.EntryFieldValueChanged

    Call EnableFilters(String.IsNullOrEmpty(Me.ef_owner_id.Value))

  End Sub ' ef_owner_id_EntryFieldValueChanged

#End Region  ' Events

End Class
