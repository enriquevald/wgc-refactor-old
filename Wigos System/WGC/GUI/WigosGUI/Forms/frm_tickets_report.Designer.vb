<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_tickets_report
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_options = New System.Windows.Forms.GroupBox()
    Me.opt_daily_grouped = New System.Windows.Forms.RadioButton()
    Me.opt_daily_detail = New System.Windows.Forms.RadioButton()
    Me.gb_operation_type = New System.Windows.Forms.GroupBox()
    Me.chk_cancel_charge_sale_note = New System.Windows.Forms.CheckBox()
    Me.chk_cancel_player_card_pay = New System.Windows.Forms.CheckBox()
    Me.chk_player_card_pay = New System.Windows.Forms.CheckBox()
    Me.opt_entry = New System.Windows.Forms.RadioButton()
    Me.chk_card_commission = New System.Windows.Forms.CheckBox()
    Me.chk_service_charge_sale_note = New System.Windows.Forms.CheckBox()
    Me.chk_charge_sale_note = New System.Windows.Forms.CheckBox()
    Me.lbl_business_a = New System.Windows.Forms.Label()
    Me.lbl_business_b = New System.Windows.Forms.Label()
    Me.opt_withdraw = New System.Windows.Forms.RadioButton()
    Me.opt_sale_note = New System.Windows.Forms.RadioButton()
    Me.opt_charge = New System.Windows.Forms.RadioButton()
    Me.opt_charge_cancel_sale_note = New System.Windows.Forms.RadioButton()
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector()
    Me.lbl_series = New System.Windows.Forms.Label()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_options.SuspendLayout()
    Me.gb_operation_type.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.lbl_series)
    Me.panel_filter.Controls.Add(Me.uc_dsl)
    Me.panel_filter.Controls.Add(Me.gb_operation_type)
    Me.panel_filter.Controls.Add(Me.gb_options)
    Me.panel_filter.Size = New System.Drawing.Size(1239, 189)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_options, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_operation_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dsl, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_series, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 193)
    Me.panel_data.Size = New System.Drawing.Size(1239, 455)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1233, 18)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1233, 4)
    '
    'gb_options
    '
    Me.gb_options.Controls.Add(Me.opt_daily_grouped)
    Me.gb_options.Controls.Add(Me.opt_daily_detail)
    Me.gb_options.Location = New System.Drawing.Point(278, 8)
    Me.gb_options.Name = "gb_options"
    Me.gb_options.Size = New System.Drawing.Size(140, 80)
    Me.gb_options.TabIndex = 1
    Me.gb_options.TabStop = False
    Me.gb_options.Text = "xOptions"
    '
    'opt_daily_grouped
    '
    Me.opt_daily_grouped.AutoSize = True
    Me.opt_daily_grouped.Location = New System.Drawing.Point(7, 45)
    Me.opt_daily_grouped.Name = "opt_daily_grouped"
    Me.opt_daily_grouped.Size = New System.Drawing.Size(110, 17)
    Me.opt_daily_grouped.TabIndex = 1
    Me.opt_daily_grouped.Text = "xDailyGrouped"
    Me.opt_daily_grouped.UseVisualStyleBackColor = True
    '
    'opt_daily_detail
    '
    Me.opt_daily_detail.AutoSize = True
    Me.opt_daily_detail.Location = New System.Drawing.Point(7, 21)
    Me.opt_daily_detail.Name = "opt_daily_detail"
    Me.opt_daily_detail.Size = New System.Drawing.Size(94, 17)
    Me.opt_daily_detail.TabIndex = 0
    Me.opt_daily_detail.Text = "xDailyDetail"
    Me.opt_daily_detail.UseVisualStyleBackColor = True
    '
    'gb_operation_type
    '
    Me.gb_operation_type.Controls.Add(Me.chk_cancel_charge_sale_note)
    Me.gb_operation_type.Controls.Add(Me.chk_cancel_player_card_pay)
    Me.gb_operation_type.Controls.Add(Me.chk_player_card_pay)
    Me.gb_operation_type.Controls.Add(Me.opt_entry)
    Me.gb_operation_type.Controls.Add(Me.chk_card_commission)
    Me.gb_operation_type.Controls.Add(Me.chk_service_charge_sale_note)
    Me.gb_operation_type.Controls.Add(Me.chk_charge_sale_note)
    Me.gb_operation_type.Controls.Add(Me.lbl_business_a)
    Me.gb_operation_type.Controls.Add(Me.lbl_business_b)
    Me.gb_operation_type.Controls.Add(Me.opt_withdraw)
    Me.gb_operation_type.Controls.Add(Me.opt_sale_note)
    Me.gb_operation_type.Controls.Add(Me.opt_charge)
    Me.gb_operation_type.Controls.Add(Me.opt_charge_cancel_sale_note)
    Me.gb_operation_type.Location = New System.Drawing.Point(433, 8)
    Me.gb_operation_type.Name = "gb_operation_type"
    Me.gb_operation_type.Size = New System.Drawing.Size(705, 178)
    Me.gb_operation_type.TabIndex = 2
    Me.gb_operation_type.TabStop = False
    Me.gb_operation_type.Text = "xOperationType"
    '
    'chk_cancel_charge_sale_note
    '
    Me.chk_cancel_charge_sale_note.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_cancel_charge_sale_note.Location = New System.Drawing.Point(308, 138)
    Me.chk_cancel_charge_sale_note.Name = "chk_cancel_charge_sale_note"
    Me.chk_cancel_charge_sale_note.Size = New System.Drawing.Size(390, 17)
    Me.chk_cancel_charge_sale_note.TabIndex = 12
    Me.chk_cancel_charge_sale_note.Text = "xCancelChargeSaleNote"
    Me.chk_cancel_charge_sale_note.UseVisualStyleBackColor = True
    '
    'chk_cancel_player_card_pay
    '
    Me.chk_cancel_player_card_pay.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_cancel_player_card_pay.Location = New System.Drawing.Point(308, 157)
    Me.chk_cancel_player_card_pay.Name = "chk_cancel_player_card_pay"
    Me.chk_cancel_player_card_pay.Size = New System.Drawing.Size(390, 17)
    Me.chk_cancel_player_card_pay.TabIndex = 11
    Me.chk_cancel_player_card_pay.Text = "xCancelPlayerCardPay"
    Me.chk_cancel_player_card_pay.UseVisualStyleBackColor = True
    '
    'chk_player_card_pay
    '
    Me.chk_player_card_pay.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_player_card_pay.Location = New System.Drawing.Point(308, 104)
    Me.chk_player_card_pay.Name = "chk_player_card_pay"
    Me.chk_player_card_pay.Size = New System.Drawing.Size(390, 17)
    Me.chk_player_card_pay.TabIndex = 10
    Me.chk_player_card_pay.Text = "xPlayerCardPay"
    Me.chk_player_card_pay.UseVisualStyleBackColor = True
    '
    'opt_entry
    '
    Me.opt_entry.AutoSize = True
    Me.opt_entry.Location = New System.Drawing.Point(7, 86)
    Me.opt_entry.MaximumSize = New System.Drawing.Size(340, 0)
    Me.opt_entry.Name = "opt_entry"
    Me.opt_entry.Size = New System.Drawing.Size(62, 17)
    Me.opt_entry.TabIndex = 3
    Me.opt_entry.Text = "xEntry"
    Me.opt_entry.UseVisualStyleBackColor = True
    '
    'chk_card_commission
    '
    Me.chk_card_commission.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_card_commission.Location = New System.Drawing.Point(308, 86)
    Me.chk_card_commission.Name = "chk_card_commission"
    Me.chk_card_commission.Size = New System.Drawing.Size(390, 17)
    Me.chk_card_commission.TabIndex = 8
    Me.chk_card_commission.Text = "xCardCommission"
    Me.chk_card_commission.UseVisualStyleBackColor = True
    '
    'chk_service_charge_sale_note
    '
    Me.chk_service_charge_sale_note.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_service_charge_sale_note.Location = New System.Drawing.Point(308, 68)
    Me.chk_service_charge_sale_note.Name = "chk_service_charge_sale_note"
    Me.chk_service_charge_sale_note.Size = New System.Drawing.Size(390, 17)
    Me.chk_service_charge_sale_note.TabIndex = 7
    Me.chk_service_charge_sale_note.Text = "xServiceChargeSaleNote"
    Me.chk_service_charge_sale_note.UseVisualStyleBackColor = True
    '
    'chk_charge_sale_note
    '
    Me.chk_charge_sale_note.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_charge_sale_note.Location = New System.Drawing.Point(308, 50)
    Me.chk_charge_sale_note.Name = "chk_charge_sale_note"
    Me.chk_charge_sale_note.Size = New System.Drawing.Size(390, 17)
    Me.chk_charge_sale_note.TabIndex = 6
    Me.chk_charge_sale_note.Text = "xChargeSaleNote"
    Me.chk_charge_sale_note.UseVisualStyleBackColor = True
    '
    'lbl_business_a
    '
    Me.lbl_business_a.AutoSize = True
    Me.lbl_business_a.Location = New System.Drawing.Point(6, 17)
    Me.lbl_business_a.MaximumSize = New System.Drawing.Size(150, 13)
    Me.lbl_business_a.MinimumSize = New System.Drawing.Size(150, 13)
    Me.lbl_business_a.Name = "lbl_business_a"
    Me.lbl_business_a.Size = New System.Drawing.Size(150, 13)
    Me.lbl_business_a.TabIndex = 0
    Me.lbl_business_a.Text = "xBusinessA"
    '
    'lbl_business_b
    '
    Me.lbl_business_b.AutoSize = True
    Me.lbl_business_b.Location = New System.Drawing.Point(285, 17)
    Me.lbl_business_b.MaximumSize = New System.Drawing.Size(240, 13)
    Me.lbl_business_b.MinimumSize = New System.Drawing.Size(240, 13)
    Me.lbl_business_b.Name = "lbl_business_b"
    Me.lbl_business_b.Size = New System.Drawing.Size(240, 13)
    Me.lbl_business_b.TabIndex = 4
    Me.lbl_business_b.Text = "xBusinessB"
    '
    'opt_withdraw
    '
    Me.opt_withdraw.AutoSize = True
    Me.opt_withdraw.Location = New System.Drawing.Point(7, 60)
    Me.opt_withdraw.MaximumSize = New System.Drawing.Size(340, 0)
    Me.opt_withdraw.Name = "opt_withdraw"
    Me.opt_withdraw.Size = New System.Drawing.Size(85, 17)
    Me.opt_withdraw.TabIndex = 2
    Me.opt_withdraw.Text = "xWithdraw"
    Me.opt_withdraw.UseVisualStyleBackColor = True
    '
    'opt_sale_note
    '
    Me.opt_sale_note.AutoSize = True
    Me.opt_sale_note.Location = New System.Drawing.Point(288, 32)
    Me.opt_sale_note.MaximumSize = New System.Drawing.Size(242, 0)
    Me.opt_sale_note.Name = "opt_sale_note"
    Me.opt_sale_note.Size = New System.Drawing.Size(83, 17)
    Me.opt_sale_note.TabIndex = 5
    Me.opt_sale_note.Text = "xSaleNote"
    Me.opt_sale_note.UseVisualStyleBackColor = True
    '
    'opt_charge
    '
    Me.opt_charge.AutoSize = True
    Me.opt_charge.Location = New System.Drawing.Point(7, 35)
    Me.opt_charge.MaximumSize = New System.Drawing.Size(282, 0)
    Me.opt_charge.Name = "opt_charge"
    Me.opt_charge.Size = New System.Drawing.Size(74, 17)
    Me.opt_charge.TabIndex = 1
    Me.opt_charge.Text = "xCharge"
    Me.opt_charge.UseVisualStyleBackColor = True
    '
    'opt_charge_cancel_sale_note
    '
    Me.opt_charge_cancel_sale_note.AutoSize = True
    Me.opt_charge_cancel_sale_note.Location = New System.Drawing.Point(288, 121)
    Me.opt_charge_cancel_sale_note.MaximumSize = New System.Drawing.Size(242, 0)
    Me.opt_charge_cancel_sale_note.Name = "opt_charge_cancel_sale_note"
    Me.opt_charge_cancel_sale_note.Size = New System.Drawing.Size(164, 17)
    Me.opt_charge_cancel_sale_note.TabIndex = 9
    Me.opt_charge_cancel_sale_note.Text = "xChargeCancelSaleNote"
    Me.opt_charge_cancel_sale_note.UseVisualStyleBackColor = True
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.ClosingTimeEnabled = True
    Me.uc_dsl.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.Location = New System.Drawing.Point(7, 8)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.ShowBorder = True
    Me.uc_dsl.Size = New System.Drawing.Size(257, 80)
    Me.uc_dsl.TabIndex = 0
    Me.uc_dsl.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    '
    'lbl_series
    '
    Me.lbl_series.AutoSize = True
    Me.lbl_series.Location = New System.Drawing.Point(37, 102)
    Me.lbl_series.Name = "lbl_series"
    Me.lbl_series.Size = New System.Drawing.Size(75, 13)
    Me.lbl_series.TabIndex = 13
    Me.lbl_series.Text = "xSeries: SR"
    '
    'frm_tickets_report
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1247, 652)
    Me.Name = "frm_tickets_report"
    Me.Text = "frm_tickets_report"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_options.ResumeLayout(False)
    Me.gb_options.PerformLayout()
    Me.gb_operation_type.ResumeLayout(False)
    Me.gb_operation_type.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_options As System.Windows.Forms.GroupBox
  Friend WithEvents opt_daily_grouped As System.Windows.Forms.RadioButton
  Friend WithEvents opt_daily_detail As System.Windows.Forms.RadioButton
  Friend WithEvents gb_operation_type As System.Windows.Forms.GroupBox
  Friend WithEvents opt_charge As System.Windows.Forms.RadioButton
  Friend WithEvents opt_sale_note As System.Windows.Forms.RadioButton
  Friend WithEvents opt_charge_cancel_sale_note As System.Windows.Forms.RadioButton
  Friend WithEvents opt_withdraw As System.Windows.Forms.RadioButton
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector
  Friend WithEvents lbl_series As System.Windows.Forms.Label
  Friend WithEvents lbl_business_a As System.Windows.Forms.Label
  Friend WithEvents lbl_business_b As System.Windows.Forms.Label
  Friend WithEvents chk_service_charge_sale_note As System.Windows.Forms.CheckBox
  Friend WithEvents chk_charge_sale_note As System.Windows.Forms.CheckBox
  Friend WithEvents chk_card_commission As System.Windows.Forms.CheckBox
  Friend WithEvents opt_entry As System.Windows.Forms.RadioButton
  Friend WithEvents chk_player_card_pay As System.Windows.Forms.CheckBox
  Friend WithEvents chk_cancel_charge_sale_note As System.Windows.Forms.CheckBox
  Friend WithEvents chk_cancel_player_card_pay As System.Windows.Forms.CheckBox
End Class
