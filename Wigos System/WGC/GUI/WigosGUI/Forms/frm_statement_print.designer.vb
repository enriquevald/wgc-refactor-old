<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_statement_print
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container
    Me.btn_last_print = New GUI_Controls.uc_button
    Me.dg_print_pending = New GUI_Controls.uc_grid
    Me.dg_print_last = New GUI_Controls.uc_grid
    Me.gb_last_print = New System.Windows.Forms.GroupBox
    Me.gb_pending_print = New System.Windows.Forms.GroupBox
    Me.gb_print_progress = New System.Windows.Forms.GroupBox
    Me.btn_print_cancel = New GUI_Controls.uc_button
    Me.btn_pause_continue = New GUI_Controls.uc_button
    Me.lbl_print_status = New System.Windows.Forms.Label
    Me.tmr_status_printing = New System.Windows.Forms.Timer(Me.components)
    Me.panel_data.SuspendLayout()
    Me.gb_last_print.SuspendLayout()
    Me.gb_pending_print.SuspendLayout()
    Me.gb_print_progress.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.panel_data.Controls.Add(Me.gb_print_progress)
    Me.panel_data.Controls.Add(Me.gb_pending_print)
    Me.panel_data.Controls.Add(Me.gb_last_print)
    Me.panel_data.Dock = System.Windows.Forms.DockStyle.Fill
    Me.panel_data.Size = New System.Drawing.Size(635, 581)
    '
    'btn_last_print
    '
    Me.btn_last_print.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_last_print.Location = New System.Drawing.Point(525, 408)
    Me.btn_last_print.Name = "btn_last_print"
    Me.btn_last_print.Size = New System.Drawing.Size(90, 30)
    Me.btn_last_print.TabIndex = 1
    Me.btn_last_print.ToolTipped = False
    Me.btn_last_print.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'dg_print_pending
    '
    Me.dg_print_pending.CurrentCol = -1
    Me.dg_print_pending.CurrentRow = -1
    Me.dg_print_pending.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_print_pending.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_print_pending.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_print_pending.Location = New System.Drawing.Point(6, 20)
    Me.dg_print_pending.Name = "dg_print_pending"
    Me.dg_print_pending.PanelRightVisible = False
    Me.dg_print_pending.Redraw = True
    Me.dg_print_pending.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_print_pending.Size = New System.Drawing.Size(401, 100)
    Me.dg_print_pending.Sortable = False
    Me.dg_print_pending.SortAscending = True
    Me.dg_print_pending.SortByCol = 0
    Me.dg_print_pending.TabIndex = 0
    Me.dg_print_pending.ToolTipped = True
    Me.dg_print_pending.TopRow = -2
    '
    'dg_print_last
    '
    Me.dg_print_last.CurrentCol = -1
    Me.dg_print_last.CurrentRow = -1
    Me.dg_print_last.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_print_last.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_print_last.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_print_last.Location = New System.Drawing.Point(6, 20)
    Me.dg_print_last.Name = "dg_print_last"
    Me.dg_print_last.PanelRightVisible = False
    Me.dg_print_last.Redraw = True
    Me.dg_print_last.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_print_last.Size = New System.Drawing.Size(514, 418)
    Me.dg_print_last.Sortable = False
    Me.dg_print_last.SortAscending = True
    Me.dg_print_last.SortByCol = 0
    Me.dg_print_last.TabIndex = 0
    Me.dg_print_last.ToolTipped = True
    Me.dg_print_last.TopRow = -2
    '
    'gb_last_print
    '
    Me.gb_last_print.Controls.Add(Me.dg_print_last)
    Me.gb_last_print.Controls.Add(Me.btn_last_print)
    Me.gb_last_print.Location = New System.Drawing.Point(9, 134)
    Me.gb_last_print.Name = "gb_last_print"
    Me.gb_last_print.Size = New System.Drawing.Size(621, 444)
    Me.gb_last_print.TabIndex = 2
    Me.gb_last_print.TabStop = False
    Me.gb_last_print.Text = "xLastPrint"
    '
    'gb_pending_print
    '
    Me.gb_pending_print.Controls.Add(Me.dg_print_pending)
    Me.gb_pending_print.Location = New System.Drawing.Point(9, 3)
    Me.gb_pending_print.Name = "gb_pending_print"
    Me.gb_pending_print.Size = New System.Drawing.Size(412, 125)
    Me.gb_pending_print.TabIndex = 0
    Me.gb_pending_print.TabStop = False
    Me.gb_pending_print.Text = "xPendingPrint"
    '
    'gb_print_progress
    '
    Me.gb_print_progress.Controls.Add(Me.btn_print_cancel)
    Me.gb_print_progress.Controls.Add(Me.btn_pause_continue)
    Me.gb_print_progress.Controls.Add(Me.lbl_print_status)
    Me.gb_print_progress.Location = New System.Drawing.Point(425, 3)
    Me.gb_print_progress.Name = "gb_print_progress"
    Me.gb_print_progress.Size = New System.Drawing.Size(205, 125)
    Me.gb_print_progress.TabIndex = 1
    Me.gb_print_progress.TabStop = False
    Me.gb_print_progress.Text = "xPrintProgress"
    '
    'btn_print_cancel
    '
    Me.btn_print_cancel.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_print_cancel.Location = New System.Drawing.Point(105, 89)
    Me.btn_print_cancel.Name = "btn_print_cancel"
    Me.btn_print_cancel.Size = New System.Drawing.Size(90, 30)
    Me.btn_print_cancel.TabIndex = 2
    Me.btn_print_cancel.ToolTipped = False
    Me.btn_print_cancel.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_pause_continue
    '
    Me.btn_pause_continue.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_pause_continue.Location = New System.Drawing.Point(9, 89)
    Me.btn_pause_continue.Name = "btn_pause_continue"
    Me.btn_pause_continue.Size = New System.Drawing.Size(90, 30)
    Me.btn_pause_continue.TabIndex = 1
    Me.btn_pause_continue.ToolTipped = False
    Me.btn_pause_continue.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'lbl_print_status
    '
    Me.lbl_print_status.AutoSize = True
    Me.lbl_print_status.Location = New System.Drawing.Point(53, 45)
    Me.lbl_print_status.Name = "lbl_print_status"
    Me.lbl_print_status.Size = New System.Drawing.Size(76, 13)
    Me.lbl_print_status.TabIndex = 0
    Me.lbl_print_status.Text = "xPrintStatus"
    '
    'tmr_status_printing
    '
    Me.tmr_status_printing.Interval = 1500
    '
    'frm_statement_print
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(731, 589)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
    Me.Name = "frm_statement_print"
    Me.Text = "frm_statement_print"
    Me.panel_data.ResumeLayout(False)
    Me.gb_last_print.ResumeLayout(False)
    Me.gb_pending_print.ResumeLayout(False)
    Me.gb_print_progress.ResumeLayout(False)
    Me.gb_print_progress.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents btn_last_print As GUI_Controls.uc_button
  Friend WithEvents dg_print_pending As GUI_Controls.uc_grid
  Friend WithEvents dg_print_last As GUI_Controls.uc_grid
  Friend WithEvents gb_pending_print As System.Windows.Forms.GroupBox
  Friend WithEvents gb_last_print As System.Windows.Forms.GroupBox
  Friend WithEvents gb_print_progress As System.Windows.Forms.GroupBox
  Friend WithEvents btn_print_cancel As GUI_Controls.uc_button
  Friend WithEvents btn_pause_continue As GUI_Controls.uc_button
  Friend WithEvents lbl_print_status As System.Windows.Forms.Label
  Friend WithEvents tmr_status_printing As System.Windows.Forms.Timer
End Class
