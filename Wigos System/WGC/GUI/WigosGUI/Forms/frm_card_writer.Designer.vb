<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_card_writer
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_card_writer))
    Me.gb_card_writer = New System.Windows.Forms.GroupBox()
    Me.chk_internal_track = New System.Windows.Forms.CheckBox()
    Me.cmb_card_writer_model = New GUI_Controls.uc_combo()
    Me.gp_card_coercivity = New System.Windows.Forms.GroupBox()
    Me.rb_coercivity_high = New System.Windows.Forms.RadioButton()
    Me.rb_coercivity_low = New System.Windows.Forms.RadioButton()
    Me.ef_export_num_cards = New GUI_Controls.uc_entry_field()
    Me.ef_site_id = New GUI_Controls.uc_entry_field()
    Me.panel_data.SuspendLayout()
    Me.gb_card_writer.SuspendLayout()
    Me.gp_card_coercivity.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.ef_site_id)
    Me.panel_data.Controls.Add(Me.ef_export_num_cards)
    Me.panel_data.Controls.Add(Me.gp_card_coercivity)
    Me.panel_data.Controls.Add(Me.gb_card_writer)
    Me.panel_data.Size = New System.Drawing.Size(250, 280)
    '
    'gb_card_writer
    '
    Me.gb_card_writer.Controls.Add(Me.chk_internal_track)
    Me.gb_card_writer.Controls.Add(Me.cmb_card_writer_model)
    Me.gb_card_writer.Location = New System.Drawing.Point(26, 51)
    Me.gb_card_writer.Name = "gb_card_writer"
    Me.gb_card_writer.Size = New System.Drawing.Size(214, 92)
    Me.gb_card_writer.TabIndex = 1
    Me.gb_card_writer.TabStop = False
    Me.gb_card_writer.Text = "xCard Writer Model"
    '
    'chk_internal_track
    '
    Me.chk_internal_track.AutoSize = True
    Me.chk_internal_track.Location = New System.Drawing.Point(13, 70)
    Me.chk_internal_track.Name = "chk_internal_track"
    Me.chk_internal_track.Size = New System.Drawing.Size(137, 17)
    Me.chk_internal_track.TabIndex = 1
    Me.chk_internal_track.Text = "xInternalTrackData"
    Me.chk_internal_track.UseVisualStyleBackColor = True
    '
    'cmb_card_writer_model
    '
    Me.cmb_card_writer_model.AllowUnlistedValues = False
    Me.cmb_card_writer_model.AutoCompleteMode = False
    Me.cmb_card_writer_model.IsReadOnly = False
    Me.cmb_card_writer_model.Location = New System.Drawing.Point(6, 20)
    Me.cmb_card_writer_model.Name = "cmb_card_writer_model"
    Me.cmb_card_writer_model.SelectedIndex = -1
    Me.cmb_card_writer_model.Size = New System.Drawing.Size(191, 24)
    Me.cmb_card_writer_model.SufixText = "Sufix Text"
    Me.cmb_card_writer_model.SufixTextVisible = True
    Me.cmb_card_writer_model.TabIndex = 0
    Me.cmb_card_writer_model.TextCombo = Nothing
    Me.cmb_card_writer_model.TextWidth = 50
    '
    'gp_card_coercivity
    '
    Me.gp_card_coercivity.Controls.Add(Me.rb_coercivity_high)
    Me.gp_card_coercivity.Controls.Add(Me.rb_coercivity_low)
    Me.gp_card_coercivity.Location = New System.Drawing.Point(25, 156)
    Me.gp_card_coercivity.Name = "gp_card_coercivity"
    Me.gp_card_coercivity.Size = New System.Drawing.Size(215, 72)
    Me.gp_card_coercivity.TabIndex = 2
    Me.gp_card_coercivity.TabStop = False
    Me.gp_card_coercivity.Text = "xCard Coercivity"
    '
    'rb_coercivity_high
    '
    Me.rb_coercivity_high.AutoSize = True
    Me.rb_coercivity_high.Location = New System.Drawing.Point(14, 44)
    Me.rb_coercivity_high.Name = "rb_coercivity_high"
    Me.rb_coercivity_high.Size = New System.Drawing.Size(57, 17)
    Me.rb_coercivity_high.TabIndex = 1
    Me.rb_coercivity_high.TabStop = True
    Me.rb_coercivity_high.Text = "xHigh"
    Me.rb_coercivity_high.UseVisualStyleBackColor = True
    '
    'rb_coercivity_low
    '
    Me.rb_coercivity_low.AutoSize = True
    Me.rb_coercivity_low.Location = New System.Drawing.Point(14, 20)
    Me.rb_coercivity_low.Name = "rb_coercivity_low"
    Me.rb_coercivity_low.Size = New System.Drawing.Size(54, 17)
    Me.rb_coercivity_low.TabIndex = 0
    Me.rb_coercivity_low.TabStop = True
    Me.rb_coercivity_low.Text = "xLow"
    Me.rb_coercivity_low.UseVisualStyleBackColor = True
    '
    'ef_export_num_cards
    '
    Me.ef_export_num_cards.DoubleValue = 0.0R
    Me.ef_export_num_cards.IntegerValue = 0
    Me.ef_export_num_cards.IsReadOnly = False
    Me.ef_export_num_cards.Location = New System.Drawing.Point(35, 241)
    Me.ef_export_num_cards.Name = "ef_export_num_cards"
    Me.ef_export_num_cards.PlaceHolder = Nothing
    Me.ef_export_num_cards.Size = New System.Drawing.Size(204, 24)
    Me.ef_export_num_cards.SufixText = "Sufix Text"
    Me.ef_export_num_cards.SufixTextVisible = True
    Me.ef_export_num_cards.TabIndex = 3
    Me.ef_export_num_cards.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_export_num_cards.TextValue = ""
    Me.ef_export_num_cards.TextWidth = 155
    Me.ef_export_num_cards.Value = ""
    Me.ef_export_num_cards.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_site_id
    '
    Me.ef_site_id.DoubleValue = 0.0R
    Me.ef_site_id.IntegerValue = 0
    Me.ef_site_id.IsReadOnly = False
    Me.ef_site_id.Location = New System.Drawing.Point(37, 13)
    Me.ef_site_id.Name = "ef_site_id"
    Me.ef_site_id.PlaceHolder = Nothing
    Me.ef_site_id.Size = New System.Drawing.Size(203, 24)
    Me.ef_site_id.SufixText = "Sufix Text"
    Me.ef_site_id.SufixTextVisible = True
    Me.ef_site_id.TabIndex = 0
    Me.ef_site_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_site_id.TextValue = ""
    Me.ef_site_id.TextWidth = 160
    Me.ef_site_id.Value = ""
    Me.ef_site_id.ValueForeColor = System.Drawing.Color.Blue
    '
    'frm_card_writer
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(352, 291)
    Me.MaximizeBox = True
    Me.MinimizeBox = True
    Me.Name = "frm_card_writer"
    Me.Text = "xMagnetic Card Writer"
    Me.panel_data.ResumeLayout(False)
    Me.gb_card_writer.ResumeLayout(False)
    Me.gb_card_writer.PerformLayout()
    Me.gp_card_coercivity.ResumeLayout(False)
    Me.gp_card_coercivity.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_card_writer As System.Windows.Forms.GroupBox
  Friend WithEvents cmb_card_writer_model As GUI_Controls.uc_combo
  Friend WithEvents gp_card_coercivity As System.Windows.Forms.GroupBox
  Friend WithEvents rb_coercivity_high As System.Windows.Forms.RadioButton
  Friend WithEvents rb_coercivity_low As System.Windows.Forms.RadioButton
  Friend WithEvents ef_export_num_cards As GUI_Controls.uc_entry_field
  Friend WithEvents ef_site_id As GUI_Controls.uc_entry_field
  Friend WithEvents chk_internal_track As System.Windows.Forms.CheckBox
End Class
