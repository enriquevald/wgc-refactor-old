<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_report_draw_tickets
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.uc_account_filter = New GUI_Controls.uc_account_sel()
    Me.ef_draw_name = New GUI_Controls.uc_entry_field()
    Me.gb_ending_date = New System.Windows.Forms.GroupBox()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.gb_order = New System.Windows.Forms.GroupBox()
    Me.opt_order_account = New System.Windows.Forms.RadioButton()
    Me.opt_order_draw = New System.Windows.Forms.RadioButton()
    Me.cmd_draw_name = New System.Windows.Forms.ComboBox()
    Me.gb_sorteo = New System.Windows.Forms.GroupBox()
    Me.opt_draw_list = New System.Windows.Forms.RadioButton()
    Me.opt_draw_name = New System.Windows.Forms.RadioButton()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_ending_date.SuspendLayout()
    Me.gb_order.SuspendLayout()
    Me.gb_sorteo.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_sorteo)
    Me.panel_filter.Controls.Add(Me.gb_order)
    Me.panel_filter.Controls.Add(Me.gb_ending_date)
    Me.panel_filter.Controls.Add(Me.uc_account_filter)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1234, 139)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account_filter, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_ending_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_order, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_sorteo, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 143)
    Me.panel_data.Size = New System.Drawing.Size(1234, 505)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1228, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1228, 4)
    '
    'uc_account_filter
    '
    Me.uc_account_filter.Account = ""
    Me.uc_account_filter.AccountText = ""
    Me.uc_account_filter.Anon = False
    Me.uc_account_filter.AutoSize = True
    Me.uc_account_filter.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_account_filter.BirthDate = New Date(CType(0, Long))
    Me.uc_account_filter.DisabledHolder = False
    Me.uc_account_filter.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_account_filter.Holder = ""
    Me.uc_account_filter.Location = New System.Drawing.Point(476, 3)
    Me.uc_account_filter.MassiveSearchNumbers = ""
    Me.uc_account_filter.MassiveSearchNumbersToEdit = ""
    Me.uc_account_filter.MassiveSearchType = 0
    Me.uc_account_filter.Name = "uc_account_filter"
    Me.uc_account_filter.SearchTrackDataAsInternal = True
    Me.uc_account_filter.ShowMassiveSearch = False
    Me.uc_account_filter.ShowVipClients = True
    Me.uc_account_filter.Size = New System.Drawing.Size(307, 134)
    Me.uc_account_filter.TabIndex = 2
    Me.uc_account_filter.Telephone = ""
    Me.uc_account_filter.TrackData = ""
    Me.uc_account_filter.Vip = False
    Me.uc_account_filter.WeddingDate = New Date(CType(0, Long))
    '
    'ef_draw_name
    '
    Me.ef_draw_name.DoubleValue = 0.0R
    Me.ef_draw_name.IntegerValue = 0
    Me.ef_draw_name.IsReadOnly = False
    Me.ef_draw_name.Location = New System.Drawing.Point(19, 15)
    Me.ef_draw_name.Name = "ef_draw_name"
    Me.ef_draw_name.PlaceHolder = Nothing
    Me.ef_draw_name.Size = New System.Drawing.Size(269, 24)
    Me.ef_draw_name.SufixText = "Sufix Text"
    Me.ef_draw_name.SufixTextVisible = True
    Me.ef_draw_name.TabIndex = 3
    Me.ef_draw_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_draw_name.TextValue = ""
    Me.ef_draw_name.Value = ""
    Me.ef_draw_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_ending_date
    '
    Me.gb_ending_date.Controls.Add(Me.dtp_to)
    Me.gb_ending_date.Controls.Add(Me.dtp_from)
    Me.gb_ending_date.Location = New System.Drawing.Point(789, 6)
    Me.gb_ending_date.Name = "gb_ending_date"
    Me.gb_ending_date.Size = New System.Drawing.Size(254, 72)
    Me.gb_ending_date.TabIndex = 3
    Me.gb_ending_date.TabStop = False
    Me.gb_ending_date.Text = "xEndingDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 38)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(240, 25)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2010, 5, 10, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 14)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(240, 25)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2010, 5, 10, 0, 0, 0, 0)
    '
    'gb_order
    '
    Me.gb_order.Controls.Add(Me.opt_order_account)
    Me.gb_order.Controls.Add(Me.opt_order_draw)
    Me.gb_order.Location = New System.Drawing.Point(6, 6)
    Me.gb_order.Name = "gb_order"
    Me.gb_order.Size = New System.Drawing.Size(168, 76)
    Me.gb_order.TabIndex = 0
    Me.gb_order.TabStop = False
    Me.gb_order.Text = "xOrder"
    '
    'opt_order_account
    '
    Me.opt_order_account.AutoSize = True
    Me.opt_order_account.Location = New System.Drawing.Point(20, 45)
    Me.opt_order_account.Name = "opt_order_account"
    Me.opt_order_account.Size = New System.Drawing.Size(107, 17)
    Me.opt_order_account.TabIndex = 1
    Me.opt_order_account.TabStop = True
    Me.opt_order_account.Text = "xAccountLevel"
    Me.opt_order_account.UseVisualStyleBackColor = True
    '
    'opt_order_draw
    '
    Me.opt_order_draw.AutoSize = True
    Me.opt_order_draw.Location = New System.Drawing.Point(20, 22)
    Me.opt_order_draw.Name = "opt_order_draw"
    Me.opt_order_draw.Size = New System.Drawing.Size(62, 17)
    Me.opt_order_draw.TabIndex = 0
    Me.opt_order_draw.TabStop = True
    Me.opt_order_draw.Text = "xDraw"
    Me.opt_order_draw.UseVisualStyleBackColor = True
    '
    'cmd_draw_name
    '
    Me.cmd_draw_name.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
    Me.cmd_draw_name.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
    Me.cmd_draw_name.FormattingEnabled = True
    Me.cmd_draw_name.Location = New System.Drawing.Point(101, 43)
    Me.cmd_draw_name.Name = "cmd_draw_name"
    Me.cmd_draw_name.Size = New System.Drawing.Size(185, 21)
    Me.cmd_draw_name.TabIndex = 3
    '
    'gb_sorteo
    '
    Me.gb_sorteo.Controls.Add(Me.opt_draw_list)
    Me.gb_sorteo.Controls.Add(Me.opt_draw_name)
    Me.gb_sorteo.Controls.Add(Me.cmd_draw_name)
    Me.gb_sorteo.Controls.Add(Me.ef_draw_name)
    Me.gb_sorteo.Location = New System.Drawing.Point(180, 6)
    Me.gb_sorteo.Name = "gb_sorteo"
    Me.gb_sorteo.Size = New System.Drawing.Size(293, 76)
    Me.gb_sorteo.TabIndex = 1
    Me.gb_sorteo.TabStop = False
    Me.gb_sorteo.Text = "xOrder"
    '
    'opt_draw_list
    '
    Me.opt_draw_list.AutoSize = True
    Me.opt_draw_list.Location = New System.Drawing.Point(10, 45)
    Me.opt_draw_list.Name = "opt_draw_list"
    Me.opt_draw_list.Size = New System.Drawing.Size(14, 13)
    Me.opt_draw_list.TabIndex = 2
    Me.opt_draw_list.TabStop = True
    Me.opt_draw_list.UseVisualStyleBackColor = True
    '
    'opt_draw_name
    '
    Me.opt_draw_name.AutoSize = True
    Me.opt_draw_name.Location = New System.Drawing.Point(10, 18)
    Me.opt_draw_name.Name = "opt_draw_name"
    Me.opt_draw_name.Size = New System.Drawing.Size(47, 17)
    Me.opt_draw_name.TabIndex = 1
    Me.opt_draw_name.TabStop = True
    Me.opt_draw_name.Text = "text"
    Me.opt_draw_name.UseVisualStyleBackColor = True
    '
    'frm_report_draw_tickets
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1244, 652)
    Me.Name = "frm_report_draw_tickets"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_ReportDrawTickets"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_ending_date.ResumeLayout(False)
    Me.gb_order.ResumeLayout(False)
    Me.gb_order.PerformLayout()
    Me.gb_sorteo.ResumeLayout(False)
    Me.gb_sorteo.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_account_filter As GUI_Controls.uc_account_sel
  Friend WithEvents ef_draw_name As GUI_Controls.uc_entry_field
  Friend WithEvents gb_ending_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents gb_order As System.Windows.Forms.GroupBox
  Friend WithEvents opt_order_account As System.Windows.Forms.RadioButton
  Friend WithEvents opt_order_draw As System.Windows.Forms.RadioButton
  Friend WithEvents cmd_draw_name As System.Windows.Forms.ComboBox
  Friend WithEvents gb_sorteo As System.Windows.Forms.GroupBox
  Friend WithEvents opt_draw_list As System.Windows.Forms.RadioButton
  Friend WithEvents opt_draw_name As System.Windows.Forms.RadioButton
End Class
