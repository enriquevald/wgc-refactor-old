'-------------------------------------------------------------------
' Copyright � 2009 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_licence_details
'
' DESCRIPTION : Licence details
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 30-APR-2009  APB    Initial version
' 23-JUN-2014  LEM    Fixed Bug WIG-1046: Not allow resizing
' 19-OCT-2019  ATB    Bug 17833: Search file filter now is searching by .xml
'--------------------------------------------------------------------

Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices

Public Class frm_licence_details
  Inherits frm_base_edit

#Region " Constants "

#End Region ' Constants

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents lbl_expiration_text As System.Windows.Forms.Label
  Friend WithEvents lbl_expiration_date As System.Windows.Forms.Label
  Friend WithEvents lbl_insertion_date As System.Windows.Forms.Label
  Friend WithEvents lbl_insertion_text As System.Windows.Forms.Label
  Public ef_locations As New System.Collections.Generic.List(Of GUI_Controls.uc_entry_field)

  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.lbl_expiration_text = New System.Windows.Forms.Label
    Me.lbl_expiration_date = New System.Windows.Forms.Label
    Me.lbl_insertion_date = New System.Windows.Forms.Label
    Me.lbl_insertion_text = New System.Windows.Forms.Label
    Me.panel_data.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.lbl_insertion_date)
    Me.panel_data.Controls.Add(Me.lbl_insertion_text)
    Me.panel_data.Controls.Add(Me.lbl_expiration_date)
    Me.panel_data.Controls.Add(Me.lbl_expiration_text)
    Me.panel_data.Location = New System.Drawing.Point(7, 4)
    Me.panel_data.Size = New System.Drawing.Size(324, 118)
    '
    'lbl_expiration_text
    '
    Me.lbl_expiration_text.BackColor = System.Drawing.SystemColors.Control
    Me.lbl_expiration_text.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_expiration_text.ForeColor = System.Drawing.SystemColors.ControlText
    Me.lbl_expiration_text.Location = New System.Drawing.Point(15, 36)
    Me.lbl_expiration_text.Name = "lbl_expiration_text"
    Me.lbl_expiration_text.Size = New System.Drawing.Size(135, 22)
    Me.lbl_expiration_text.TabIndex = 52
    Me.lbl_expiration_text.Text = "xExpiration Date"
    Me.lbl_expiration_text.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_expiration_date
    '
    Me.lbl_expiration_date.BackColor = System.Drawing.SystemColors.Control
    Me.lbl_expiration_date.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_expiration_date.ForeColor = System.Drawing.Color.Blue
    Me.lbl_expiration_date.Location = New System.Drawing.Point(164, 36)
    Me.lbl_expiration_date.Name = "lbl_expiration_date"
    Me.lbl_expiration_date.Size = New System.Drawing.Size(146, 22)
    Me.lbl_expiration_date.TabIndex = 53
    Me.lbl_expiration_date.Text = "x30/04/2009"
    Me.lbl_expiration_date.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_insertion_date
    '
    Me.lbl_insertion_date.BackColor = System.Drawing.SystemColors.Control
    Me.lbl_insertion_date.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_insertion_date.ForeColor = System.Drawing.Color.Blue
    Me.lbl_insertion_date.Location = New System.Drawing.Point(164, 66)
    Me.lbl_insertion_date.Name = "lbl_insertion_date"
    Me.lbl_insertion_date.Size = New System.Drawing.Size(146, 22)
    Me.lbl_insertion_date.TabIndex = 55
    Me.lbl_insertion_date.Text = "x30/04/2009"
    Me.lbl_insertion_date.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_insertion_text
    '
    Me.lbl_insertion_text.BackColor = System.Drawing.SystemColors.Control
    Me.lbl_insertion_text.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_insertion_text.ForeColor = System.Drawing.SystemColors.ControlText
    Me.lbl_insertion_text.Location = New System.Drawing.Point(15, 66)
    Me.lbl_insertion_text.Name = "lbl_insertion_text"
    Me.lbl_insertion_text.Size = New System.Drawing.Size(135, 22)
    Me.lbl_insertion_text.TabIndex = 54
    Me.lbl_insertion_text.Text = "xInsertion Date"
    Me.lbl_insertion_text.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'frm_licence_details
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.BackColor = System.Drawing.SystemColors.Control
    Me.ClientSize = New System.Drawing.Size(429, 129)
    Me.ForeColor = System.Drawing.SystemColors.ControlText
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_licence_details"
    Me.Text = "#5272 "
    Me.panel_data.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Members "

  Private m_form_licence As New CLASS_LICENCE
  Private ContinueOperation As Boolean

#End Region ' Members

#Region " Override Functions "

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_LICENCE_DETAILS

    'Call Base Form proc
    MyBase.GUI_SetFormId()
    '------------------------------------------------

  End Sub ' GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    MyBase.GUI_InitControls()

    ' Buttons
    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Visible = True

    Me.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(251)

    lbl_expiration_text.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(254)
    lbl_insertion_text.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(253)

  End Sub ' GUI_InitControls

  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim licence As CLASS_LICENCE

    If Me.ScreenMode = frm_base_edit.ENUM_SCREEN_MODE.MODE_NEW Then
      licence = m_form_licence
    ElseIf Me.ScreenMode = frm_base_edit.ENUM_SCREEN_MODE.MODE_EDIT Then
      ' Cast the DB object
      licence = DbReadObject
    Else
      Return
    End If

    lbl_expiration_date.Text = GUI_FormatDate(licence.ExpirationDate, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)

    If Me.ScreenMode = frm_base_edit.ENUM_SCREEN_MODE.MODE_NEW Then
      ' Make button invisible for new versions
      GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False

      ' No insertion date
      lbl_insertion_date.Text = GUI_FormatDate(Now, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    ElseIf Me.ScreenMode = frm_base_edit.ENUM_SCREEN_MODE.MODE_EDIT Then
      GUI_Button(ENUM_BUTTON.BUTTON_OK).Visible = False

      ' Insertion date
      lbl_insertion_date.Text = GUI_FormatDate(licence.InsertionDate, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    End If

  End Sub ' GUI_SetScreenData

  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Return True

  End Function ' GUI_IsScreenDataOk

  Protected Overrides Sub GUI_GetScreenData()

    Dim licence As CLASS_LICENCE

    licence = DbEditedObject

    ' Populate relevant fields 
    licence.Identifier = m_form_licence.Identifier
    licence.InsertionDate = m_form_licence.InsertionDate
    licence.ExpirationDate = m_form_licence.ExpirationDate
    licence.Licence = m_form_licence.Licence

  End Sub ' GUI_GetScreenData

  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Dim licence As CLASS_LICENCE
    Dim licence_id As CLASS_LICENCE
    Dim rc As Integer

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_LICENCE

        If Me.ScreenMode = frm_base_edit.ENUM_SCREEN_MODE.MODE_EDIT Then
          licence = DbEditedObject
          licence_id = Me.DbObjectId

          licence.Identifier = licence_id.Identifier
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          ' Cast on the DB Object and its ID to copy the ID
          licence = DbEditedObject
          licence_id = Me.DbObjectId

          licence.Identifier = licence_id.Identifier

          ' 121 - "Error reading data related to Software Package=%1"
          NLS_MsgBox(GLB_NLS_GUI_SW_DOWNLOAD.Id(123), _
                     ENUM_MB_TYPE.MB_TYPE_ERROR, _
                     ENUM_MB_BTN.MB_BTN_OK, _
                     ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_INSERT
        ContinueOperation = True
        licence = Me.DbEditedObject

        Call MyBase.GUI_DB_Operation(frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_INSERT)

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_INSERT
        If ContinueOperation Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        Else
          ContinueOperation = False
          DbStatus = ENUM_STATUS.STATUS_ERROR
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        If ContinueOperation Then
          licence = Me.DbEditedObject

          rc = NLS_MsgBox(GLB_NLS_GUI_SW_DOWNLOAD.Id(124), _
                          mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, _
                          mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

          Call MyBase.GUI_DB_Operation(DbOperation)
        Else
          DbStatus = ENUM_STATUS.STATUS_ERROR
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE
        ContinueOperation = False
        licence = Me.DbEditedObject
        rc = NLS_MsgBox(GLB_NLS_GUI_SW_DOWNLOAD.Id(120), _
                        ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        ENUM_MB_BTN.MB_BTN_YES_NO, _
                        ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
                        GUI_FormatDate(licence.ExpirationDate, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT))
        If rc = ENUM_MB_RESULT.MB_RESULT_YES Then
          ContinueOperation = True
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_DELETE
        If ContinueOperation Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        Else
          DbStatus = ENUM_STATUS.STATUS_ERROR
        End If

      Case Else
        MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)

  End Sub ' GUI_Permissions

  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.lbl_expiration_date

  End Sub ' GUI_SetInitialFocus

  ' PURPOSE : Init form in edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '           - DrawId
  '     - OUTPUT :
  '
  ' RETURNS :

  Public Overloads Sub ShowEditItem(ByVal SwPackage As CLASS_SW_PACKAGE)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Me.DbObjectId = SwPackage

    GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Me.Display(True)
    End If
  End Sub ' ShowEditItem

  Public Overloads Sub ShowNewItem()

    Dim fd_browse_licence As New System.Windows.Forms.OpenFileDialog
    Dim rc As Integer
    Dim rc_bool As Boolean
    Dim expiration_date As Date
    Dim licence_text As String = ""

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    Me.DbObjectId = Nothing
    DbStatus = ENUM_STATUS.STATUS_OK

    ' Open file selection dialog
    fd_browse_licence.Filter = "Licences (*.xml)" & "|*.xml"
    fd_browse_licence.FilterIndex = 1
    fd_browse_licence.RestoreDirectory = True

    If fd_browse_licence.ShowDialog() = Windows.Forms.DialogResult.Cancel Then
      ' Cancel operation
      Exit Sub
    End If

    '       - Feed the dialog
    Call Application.DoEvents()

    ' Change cursor appearance
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Read licence and extract expiration date
    rc_bool = CLASS_LICENCE.ReadLicenceFile(fd_browse_licence.FileName, licence_text, expiration_date)

    ' Change cursor appearance
    Windows.Forms.Cursor.Current = Cursors.Default

    If Not rc_bool Then
      ' File error
      ' Show error message
      rc = NLS_MsgBox(GLB_NLS_GUI_SW_DOWNLOAD.Id(122), _
                      mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
                      mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, _
                      , _
                      fd_browse_licence.FileName)
      Exit Sub
    End If

    ' Add new licence
    m_form_licence.Licence = licence_text
    m_form_licence.ExpirationDate = expiration_date

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowNewItem

#End Region ' Override Functions

End Class
