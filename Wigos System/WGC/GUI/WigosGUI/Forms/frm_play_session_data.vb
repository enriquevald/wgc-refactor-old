'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_play_session_data
' DESCRIPTION:   play session data
' AUTHOR:        Alberto Marcos
' CREATION DATE: 22-NOV-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 22-NOV-2013  AMF    Initial version
'--------------------------------------------------------------------
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text

Partial Public Class frm_play_session_data
  Inherits frm_base

#Region " Members "

  Private m_play_session_id As Integer

#End Region ' Members

#Region " Overrides "

  Public Sub New(ByVal PlaySessionId As Integer)
    MyBase.New()
    Me.m_play_session_id = PlaySessionId

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub ' New

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3003)
    btn_exit.Text = GLB_NLS_GUI_CONTROLS.GetString(10)

    Call ShowData()

  End Sub ' GUI_InitControls

#End Region ' Overrides

#Region " Private Functions "

  Private Sub ShowData()
    Dim _sb As StringBuilder
    Dim _table As DataTable
    Dim _details As String
    Dim _cashier_name As String

    _table = New DataTable()
    _sb = New StringBuilder()
    _details = ""
    _cashier_name = ""

    _sb.AppendLine("SELECT   TOP 1 AM_DETAILS ")
    _sb.AppendLine("       , AM_CASHIER_NAME ")
    _sb.AppendLine("  FROM   ACCOUNT_MOVEMENTS  WITH (INDEX(IX_MOVEMENTS)) ")
    _sb.AppendLine(" WHERE   AM_PLAY_SESSION_ID = @pPlaySessionId ")
    _sb.AppendLine("   AND   AM_TYPE = @pType")

    Try
      Using _db_trx As New DB_TRX()
        Using _cmd As New SqlCommand(_sb.ToString())
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = Me.m_play_session_id
          _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = WSI.Common.MovementType.ManualEndSession
          Using _reader As SqlClient.SqlDataReader = _db_trx.ExecuteReader(_cmd)
            If (_reader.Read()) Then
              _details = IIf(IsDBNull(_reader(0)), AUDIT_NONE_STRING, _reader(0))
              _cashier_name = IIf(IsDBNull(_reader(1)), "", _reader(1))
            End If
          End Using
        End Using
      End Using

      txt_data.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3004) + vbNewLine + _
                      _cashier_name + vbNewLine + vbNewLine + _
                      GLB_NLS_GUI_PLAYER_TRACKING.GetString(3005) + vbNewLine + _
                      _details

      txt_data.Select(0, 0)

    Catch ex As Exception

    End Try

  End Sub ' ShowData

#End Region ' Private Functions

#Region " Events "

  Private Sub btn_exit_ClickEvent() Handles btn_exit.ClickEvent

    Me.Close()

  End Sub ' btn_exit_ClickEvent

#End Region ' Events

End Class