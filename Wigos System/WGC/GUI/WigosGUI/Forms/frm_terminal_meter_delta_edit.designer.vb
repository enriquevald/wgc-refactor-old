<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_terminal_meter_delta_edit
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_quantity = New System.Windows.Forms.GroupBox
    Me.gb_cents = New System.Windows.Forms.GroupBox
    Me.ef_group_name = New GUI_Controls.uc_entry_field
    Me.panel_data.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_quantity)
    Me.panel_data.Controls.Add(Me.ef_group_name)
    Me.panel_data.Controls.Add(Me.gb_cents)
    Me.panel_data.Size = New System.Drawing.Size(658, 345)
    '
    'gb_quantity
    '
    Me.gb_quantity.Location = New System.Drawing.Point(320, 38)
    Me.gb_quantity.Name = "gb_quantity"
    Me.gb_quantity.Size = New System.Drawing.Size(317, 301)
    Me.gb_quantity.TabIndex = 17
    Me.gb_quantity.TabStop = False
    Me.gb_quantity.Text = "gb_quantity"
    '
    'gb_cents
    '
    Me.gb_cents.Location = New System.Drawing.Point(15, 38)
    Me.gb_cents.Name = "gb_cents"
    Me.gb_cents.Size = New System.Drawing.Size(299, 301)
    Me.gb_cents.TabIndex = 2
    Me.gb_cents.TabStop = False
    Me.gb_cents.Text = "gb_cents"
    '
    'ef_group_name
    '
    Me.ef_group_name.DoubleValue = 0
    Me.ef_group_name.IntegerValue = 0
    Me.ef_group_name.IsReadOnly = False
    Me.ef_group_name.Location = New System.Drawing.Point(27, 8)
    Me.ef_group_name.Name = "ef_group_name"
    Me.ef_group_name.PlaceHolder = Nothing
    Me.ef_group_name.Size = New System.Drawing.Size(353, 24)
    Me.ef_group_name.SufixText = "Sufix Text"
    Me.ef_group_name.SufixTextVisible = True
    Me.ef_group_name.TabIndex = 0
    Me.ef_group_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_group_name.TextValue = ""
    Me.ef_group_name.TextWidth = 110
    Me.ef_group_name.Value = ""
    Me.ef_group_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'frm_terminal_meter_delta_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(760, 354)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_terminal_meter_delta_edit"
    Me.Text = "frm_terminal_meter_delta_edit"
    Me.panel_data.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_group_name As GUI_Controls.uc_entry_field
  Friend WithEvents gb_quantity As System.Windows.Forms.GroupBox
  Friend WithEvents gb_cents As System.Windows.Forms.GroupBox
End Class
