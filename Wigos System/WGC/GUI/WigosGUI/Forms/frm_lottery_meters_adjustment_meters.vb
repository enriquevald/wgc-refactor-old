'-----------------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd. 
'-----------------------------------------------------------------------------
'
' MODULE NAME:   frm_lottery_meters_adjustment_meters
' DESCRIPTION:   This screen allows to view and adjust the EGM lottery meters.
' AUTHOR:        Francis Gretz
' CREATION DATE: 15-DEC-2017
' 
' REVISION HISTORY:lblTotalWinValue
'
' Date         Author       Description
' -----------  ------       ---------------------------------------------------------
' 15-DEC-2017  FGB          Initial version
' 06-MAR-2018  DPC          Bug 31802:[WIGOS-8350]: AGG - SITE - MULTISITE - Billing: Save button is missing on "Billing - EGM meters" form when the user has P1 or P2 permissions only.
' 06-MAR-2018  DPC          Bug 31803:[WIGOS-8517]: AGG - SITE - MULTISITE - Billing: cut point registers are not exported to excel when previosly has been removed another one in the same terminal.
' 07-MAR-2018  XGJ          Wigos-8686: Billing. It must be possible to create a cup point betwwen two records with the same time (HH:MM)
'------------------------------------------------------------------------------------
#Region "Imports"

Imports System.Text
Imports System.Data.SqlClient

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE
Imports WSI.Common
Imports WSI.Common.EGMMeterAdjustment

#End Region

Public Class frm_lottery_meters_adjustment_meters
  Inherits frm_base_sel_edit

#Region "Constants"
  Private Const GRID_HEADERS_COUNT As Integer = 2
  Private Const COLOR_IMPORTED = ENUM_GUI_COLOR.GUI_COLOR_OCHRE_00

  ' Decimals
  Private Const DECIMALS_METER_VALUE As Int32 = 0
  Private Const DECIMALS_DENOM As Int32 = 2
  Private Const DENOM_BY_DEFAULT As Decimal = 0.01

  'Index Columns
  Private Const GRID_COLUMN_ASTERISK As Integer = 0
  Private Const GRID_COLUMN_WORKING_DAY As Integer = 1
  Private Const GRID_COLUMN_DATETIME As Integer = 2
  Private Const GRID_COLUMN_DENOM As Integer = 3
  Private Const GRID_COLUMN_WIN As Integer = 4
  Private Const GRID_COLUMN_COIN_IN As Integer = 5
  Private Const GRID_COLUMN_COIN_OUT As Integer = 6
  Private Const GRID_COLUMN_JACKPOT As Integer = 7
  Private Const GRID_COLUMN_RECORD_TYPE_ID As Integer = 8
  Private Const GRID_COLUMN_WARNING As Integer = 9
  Private Const GRID_COLUMN_IGNORED As Integer = 10
  Private Const GRID_COLUMN_IGNORED_EXCEL As Integer = 11
  Private Const GRID_COLUMN_CUT As Integer = 12
  Private Const GRID_COLUMN_ROW_INDEX_SEQ As Integer = 13
  Private Const GRID_COLUMN_USER_MODIFIED As Integer = 14
  Private Const GRID_COLUMNS_COUNT As Integer = 15

  'Width
  Private Const GRID_WIDTH_ZERO As Integer = 1
  Private Const GRID_WIDTH_COLUMN_DATETIME As Integer = 1900
  Private Const GRID_WIDTH_COLUMN_DENOM As Integer = 1200
  Private Const GRID_WIDTH_COLUMN_WIN As Integer = 2000
  Private Const GRID_WIDTH_COLUMN_COIN_IN As Integer = 1500
  Private Const GRID_WIDTH_COLUMN_COIN_OUT As Integer = 1500
  Private Const GRID_WIDTH_COLUMN_JACKPOT As Integer = 1500
  Private Const GRID_WIDTH_COLUMN_RECORD_TYPE As Integer = 4050
  Private Const GRID_WIDTH_COLUMN_IGNORE As Integer = 1200
  Private Const GRID_WIDTH_COLUMN_CUT As Integer = 1200
  Private Const GRID_WIDTH_COLUMN_ASTERISK As Integer = 300
  Private Const GRID_WIDTH_COLUMN_USER_MODIFIED As Integer = 2000

  Private Const MAX_METER_VALUE = 1000000
  Private Const DENOM_WITHOUT_VALUE As String = "---"
#End Region

#Region "Members"
  Private m_adjustment_period As EGMMeterAdjustmentByPeriod
  Private m_adjustment_datatable As DataTable

  ' Initial values
  Private m_site_id As Integer
  Private m_site_name As String
  Private m_working_day As DateTime
  Private m_wd_status As EGMWorkingDayStatus
  Private m_terminal_id As Int32
  Private m_terminal_name As String
  Private m_provider_name As String

  Private m_is_multisite_center As Boolean

  Private m_user_permissions As CLASS_GUI_USER.TYPE_PERMISSIONS ' Permission object

  ' Total (computed)
  Private m_total_win As Decimal

  ' Buttons
  Private m_save_button As uc_button
  Private m_add_cut_button As uc_button
  Private m_del_cut_button As uc_button
  Private m_ignore_selected_button As uc_button
  Private m_unignore_selected_button As uc_button

  Private m_is_edit_mode As Boolean
  Private m_saved_almost_once As Boolean

  Private m_text_yes As String
  Private m_text_no As String

  ' Dict with max value
  Private m_dict_maxvalues As IDictionary

  ' XGJ 31-GEN-2018
  ' Necessary to control the resert rows
  Private m_is_reset As Boolean
#End Region

#Region "Properties"
  Property SaveAlmostOnce
    Get
      Return m_saved_almost_once
    End Get
    Set(value)
      m_saved_almost_once = value
    End Set
  End Property
#End Region

#Region "Constructors"

  Public Sub New(ByVal FormId As ENUM_FORM)
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Load Permisions
    Me.m_user_permissions = CurrentUser.Permissions(Me.FormId)

    ' Initialize members
    Me.FormId = FormId

    MyBase.GUI_SetFormId()

    Me.m_save_button = Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0)
    Me.m_del_cut_button = Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1)
    Me.m_add_cut_button = Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2)
    Me.m_unignore_selected_button = Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_3)
    Me.m_ignore_selected_button = Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_4)
  End Sub

#End Region

#Region "Overridden methods: frm_base_sel_edit"

  ''' <summary>
  ''' Initializes the form id.
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_SetFormId() ' GUI_SetFormId 
    Me.FormId = ENUM_FORM.FORM_LOTTERY_METERS_ADJUSTMENT_METERS
    MyBase.GUI_SetFormId()
  End Sub

  ''' <summary>
  ''' Initialize every form control
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_InitControls()
    MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8820)

    'Working day status
    m_wd_status = GetWorkingDayStatus()

    Dim _user_can_edit_and_manage_cut_points As Boolean
    _user_can_edit_and_manage_cut_points = (Me.m_is_edit_mode AndAlso UserCanManageCutPoints())

    ' Buttons
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Enabled = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    Me.m_save_button.Type = uc_button.ENUM_BUTTON_TYPE.USER
    Me.m_save_button.Text = GLB_NLS_GUI_CONTROLS.GetString(13)

    Me.m_add_cut_button.Visible = _user_can_edit_and_manage_cut_points
    Me.m_add_cut_button.Enabled = False
    Me.m_add_cut_button.Type = uc_button.ENUM_BUTTON_TYPE.USER
    Me.m_add_cut_button.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8831)
    Me.m_add_cut_button.Size = New System.Drawing.Size(90, 45)

    Me.m_del_cut_button.Visible = _user_can_edit_and_manage_cut_points
    Me.m_del_cut_button.Enabled = False
    Me.m_del_cut_button.Type = uc_button.ENUM_BUTTON_TYPE.USER
    Me.m_del_cut_button.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8832)
    Me.m_del_cut_button.Size = New System.Drawing.Size(90, 45)

    Me.m_ignore_selected_button.Visible = True
    Me.m_ignore_selected_button.Type = uc_button.ENUM_BUTTON_TYPE.USER
    Me.m_ignore_selected_button.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8937)
    Me.m_ignore_selected_button.Size = New System.Drawing.Size(90, 45)

    Me.m_unignore_selected_button.Visible = True
    Me.m_unignore_selected_button.Type = uc_button.ENUM_BUTTON_TYPE.USER
    Me.m_unignore_selected_button.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8938)
    Me.m_unignore_selected_button.Size = New System.Drawing.Size(90, 45)

    ' Working day
    Me.gb_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(268) ' Date
    Me.dtp_working_day.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8944)
    Me.dtp_working_day.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)

    ' Site
    Me.gb_site.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5658)
    Me.txt_site_name.Value = m_site_name
    Me.txt_site_name.Text = GLB_NLS_GUI_CONFIGURATION.GetString(380)
    Me.txt_site_name.IsReadOnly = True

    ' Terminal name
    Me.gb_terminal.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1097)
    Me.ef_terminal_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(261)
    Me.ef_terminal_name.IsReadOnly = True

    ' Provider name
    Me.ef_provider_name.Value = m_provider_name
    Me.ef_provider_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4530)
    Me.ef_provider_name.IsReadOnly = True

    ' Status
    ' TODO: change the label by a green/red (open/closed) locker
    Me.gb_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(346)
    Me.lbl_status.Text = GetStatusDescription()
    Me.lbl_status.ForeColor = GetStatusColor()

    ' Totals
    Me.lblTotalWinText.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8824) 'Win
    Me.lblTotalWinValue.Text = String.Empty
    Me.panel_totals.Visible = UserCanSeeTotals()

    GUI_StyleSheet()
    Me.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)

    SetButtonsStatus()

    ManagementEnabledAddRemoveCut(IIf(Me.Grid.NumRows > 1 AndAlso Me.Grid.Row(0).Height <= 1, 1, 0))

    SwitchSiteFilter()

    Me.GUI_Button(frm_base_sel_edit.ENUM_BUTTON.BUTTON_FILTER_APPLY).Visible = False
    Me.GUI_Button(frm_base_sel_edit.ENUM_BUTTON.BUTTON_FILTER_RESET).Visible = False
  End Sub

  ''' <summary>
  ''' Show/hide site filter control depending if we are center or not
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SwitchSiteFilter()
    If (Not Me.m_is_multisite_center) Then
      Dim _offset_x As Integer

      gb_site.Visible = False

      'Get offset to move rest of filters
      _offset_x = gb_date.Location.X - gb_site.Location.X

      'Move to the left
      gb_date.Location = New Point(gb_date.Location.X - _offset_x, gb_date.Location.Y)
      gb_terminal.Location = New Point(gb_terminal.Location.X - _offset_x, gb_terminal.Location.Y)
      gb_status.Location = New Point(gb_status.Location.X - _offset_x, gb_status.Location.Y)
    End If
  End Sub

  ''' <summary>
  ''' Get Status Description
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetStatusDescription() As String
    If (m_wd_status = EGMMeterAdjustment.EGMWorkingDayStatus.CLOSED) Then
      Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4350).ToUpper 'Closed
    Else
      Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4349).ToUpper 'Open
    End If

  End Function

  ''' <summary>
  ''' Get Status Color
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetStatusColor() As Color
    If (m_wd_status = EGMMeterAdjustment.EGMWorkingDayStatus.CLOSED) Then
      Return Color.Red    'Closed
    Else
      Return Color.Green  'Open
    End If
  End Function

  ''' <summary>
  ''' Set Button Status
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SetButtonsStatus()
    Dim _enabled As Boolean

    _enabled = (m_wd_status <> EGMMeterAdjustment.EGMWorkingDayStatus.CLOSED AndAlso Me.Grid.NumRows > 0)

    Me.m_save_button.Enabled = _enabled
    Me.m_add_cut_button.Enabled = _enabled
    Me.m_del_cut_button.Enabled = _enabled
    Me.m_ignore_selected_button.Enabled = _enabled
    Me.m_unignore_selected_button.Enabled = _enabled
  End Sub

  ''' <summary>
  ''' Sets initial focus
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.dtp_working_day
  End Sub

  ''' <summary>
  ''' Set number value
  ''' </summary>
  ''' <param name="DbRow"></param>
  ''' <param name="RowIndex"></param>
  ''' <param name="SqlColumn"></param>
  ''' <param name="GridColumn"></param>
  ''' <param name="NumDecimals"></param>
  ''' <remarks></remarks>
  Private Sub FormatNumberValue(ByVal DbRow As CLASS_DB_ROW, _
                                ByVal RowIndex As Integer, _
                                ByVal SqlColumn As Integer, _
                                ByVal GridColumn As Integer,
                                ByVal NumDecimals As Integer)
    If Not DbRow.IsNull(SqlColumn) Then
      Me.Grid.Cell(RowIndex, GridColumn).Value = GUI_FormatNumber(DbRow.Value(SqlColumn), NumDecimals, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE)
    End If
  End Sub

  ''' <summary>
  ''' Set denom value
  ''' </summary>
  ''' <param name="DbRow"></param>
  ''' <param name="RowIndex"></param>
  ''' <param name="SqlColumn"></param>
  ''' <param name="GridColumn"></param>
  ''' <remarks></remarks>
  Private Sub FormatDenomValue(ByVal DbRow As CLASS_DB_ROW, _
                               ByVal RowIndex As Integer, _
                               ByVal SqlColumn As Integer, _
                               ByVal GridColumn As Integer)
    If Not DbRow.IsNull(SqlColumn) Then
      If (DbRow.Value(SqlColumn) < 0) Then
        Me.Grid.Cell(RowIndex, GridColumn).Value = DENOM_WITHOUT_VALUE
      Else
        FormatNumberValue(DbRow, RowIndex, SqlColumn, GridColumn, DECIMALS_DENOM)
      End If
    End If
  End Sub

  ''' <summary>
  ''' Set meter value
  ''' </summary>
  ''' <param name="DbRow"></param>
  ''' <param name="RowIndex"></param>
  ''' <param name="SqlColumn"></param>
  ''' <param name="GridColumn"></param>
  ''' <remarks></remarks>
  Private Sub FormatMeterValue(ByVal DbRow As CLASS_DB_ROW, _
                               ByVal RowIndex As Integer, _
                               ByVal SqlColumn As Integer, _
                               ByVal GridColumn As Integer)
    FormatNumberValue(DbRow, RowIndex, SqlColumn, GridColumn, DECIMALS_METER_VALUE)
  End Sub

  ''' <summary>
  ''' Set meter value
  ''' </summary>
  ''' <param name="DbRow"></param>
  ''' <param name="RowIndex"></param>
  ''' <param name="SqlColumn"></param>
  ''' <param name="GridColumn"></param>
  ''' <param name="NumDecimals"></param>
  ''' <remarks></remarks>
  Private Sub FormaMoneyValue(ByVal DbRow As CLASS_DB_ROW, _
                               ByVal RowIndex As Integer, _
                               ByVal SqlColumn As Integer, _
                               ByVal GridColumn As Integer)

    '   FormatNumberValue(DbRow, RowIndex, SqlColumn, GridColumn, DECIMALS_METER_VALUE)
    If Not DbRow.IsNull(SqlColumn) Then
      Me.Grid.Cell(RowIndex, GridColumn).Value = GUI_FormatCurrency(DbRow.Value(SqlColumn), ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE)
    End If

  End Sub

  ''' <summary>
  ''' Set checkbox value
  ''' </summary>
  ''' <param name="DbRow"></param>
  ''' <param name="RowIndex"></param>
  ''' <param name="SqlColumn"></param>
  ''' <param name="GridColumn"></param>
  ''' <remarks></remarks>
  Private Sub FormatCheckBoxValue(ByVal DbRow As CLASS_DB_ROW, _
                                  ByVal RowIndex As Integer, _
                                  ByVal SqlColumn As Integer, _
                                  ByVal GridColumn As Integer)
    If ((Not DbRow.IsNull(SqlColumn)) AndAlso (DbRow.Value(SqlColumn))) Then
      Me.Grid.Cell(RowIndex, GridColumn).Value = uc_grid.GRID_CHK_CHECKED
    Else
      Me.Grid.Cell(RowIndex, GridColumn).Value = uc_grid.GRID_CHK_UNCHECKED
    End If
  End Sub

  ''' <summary>
  ''' Sets the values of a row
  ''' </summary>
  ''' <param name="RowIndex"></param>
  ''' <param name="DbRow"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As CLASS_DB_ROW) As Boolean ' GUI_SetupRow
    Dim _color_row As System.Drawing.Color
    Dim _str_anomaly As String
    Dim _anomaly As Integer
    Dim _coin_in_aux As Decimal
    Dim _coin_out_aux As Decimal
    Dim _jackpot_aux As Decimal
    Dim _account_denom As Decimal
    Dim _max_value As Decimal

    'Check what records are to be shown in the grid
    If (Not (MustAddRecordToGrid(DbRow))) Then
      Return False
    End If

    Try
      _str_anomaly = String.Empty
      _account_denom = DENOM_BY_DEFAULT

      ' Win
      If Not DbRow.IsNull(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_WORKING_DAY) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_WORKING_DAY).Value = DbRow.Value(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_WORKING_DAY)
      End If

      ' Datetime
      'XGJ 07-MAR-2018 Wigos-8686
      If Not DbRow.IsNull(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_DATETIME) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DATETIME).Value = GUI_FormatDate(DbRow.Value(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_DATETIME), _
                                                                            ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                            ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      End If

      ' Denom
      FormatDenomValue(DbRow, RowIndex, EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_SAS_ACCOUNTING_DENOM, GRID_COLUMN_DENOM)

      ' Win
      If Not DbRow.IsNull(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_WIN) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_WIN).Value = GUI_GetCurrencyValue_Format(DbRow.Value(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_WIN), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End If

      ' Coin In
      Me.Grid.Cell(RowIndex, GRID_COLUMN_COIN_IN).Value = DbRow.Value(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0000)

      ' Coin Out
      Me.Grid.Cell(RowIndex, GRID_COLUMN_COIN_OUT).Value = DbRow.Value(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0001)

      ' Jackpot
      Me.Grid.Cell(RowIndex, GRID_COLUMN_JACKPOT).Value = DbRow.Value(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0002)

      ' RecordType
      If Not DbRow.IsNull(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_RECORD_TYPE) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_RECORD_TYPE_ID).Value = DbRow.Value(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_RECORD_TYPE)
      End If

      ' Anomalies
      ' If (Not DbRow.IsNull(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_RECORD_TYPE) And DbRow.Value(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_RECORD_TYPE) <> EGMMeterAdjustment.EGMMeterAdjustmentConstants.RECORD_TYPE_CUT) Then
      If (Not DbRow.IsNull(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_RECORD_TYPE)) Then
        _anomaly = DbRow.Value(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_RECORD_TYPE)

        EGMMeterAdjustmentCommon.GetColorByPriorityTsmh(_anomaly, _color_row, _str_anomaly)

        Me.Grid.Cell(RowIndex, GRID_COLUMN_WARNING).Value = _str_anomaly
        Me.Grid.Row(RowIndex).BackColor = _color_row

        If (Not String.IsNullOrEmpty(_str_anomaly)) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_ASTERISK).Value = "*"
        Else
          ' Check big increments
          If (Not String.IsNullOrEmpty(Me.Grid.Cell(RowIndex, GRID_COLUMN_DENOM).Value)) Then
            _account_denom = Me.Grid.Cell(RowIndex, GRID_COLUMN_DENOM).Value

            If (_account_denom <> 0) Then
              If m_dict_maxvalues.Contains(_account_denom) Then
                _max_value = m_dict_maxvalues.Item(_account_denom)

                _coin_in_aux = DbRow.Value(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0000_INCREMENT) / _account_denom
                _coin_out_aux = DbRow.Value(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0001_INCREMENT) / _account_denom
                _jackpot_aux = DbRow.Value(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0002_INCREMENT) / _account_denom

                If ((_coin_in_aux > _max_value) OrElse (_coin_out_aux > _max_value) OrElse (_jackpot_aux > _max_value)) Then
                  Me.Grid.Cell(RowIndex, GRID_COLUMN_WARNING).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9005)
                  Me.Grid.Row(RowIndex).BackColor = Color.FromArgb(192, 255, 255)
                End If
              End If
            End If
          End If
        End If
      End If

      ' Ignore
      FormatCheckBoxValue(DbRow, RowIndex, EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_USER_IGNORED, GRID_COLUMN_IGNORED)

      ' Ignore (Excel Report)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_IGNORED_EXCEL).Value = GetBooleanText(DbRow.Value(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_USER_IGNORED))

      ' Cut
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CUT).Value = GetIsCutDescription(DbRow)

      ' Row Index
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ROW_INDEX_SEQ).Value = m_adjustment_datatable.Rows(RowIndex)(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_ROW_INDEX_SEQ)

      ' User
      If IsDBNull(DbRow.Value(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_LAST_UPDATED_USER_ID)) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_MODIFIED).Value = ""
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_MODIFIED).Value = GetUserName(DbRow.Value(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_LAST_UPDATED_USER_ID))
      End If

      If DbRow.Value(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_WORKING_DAY) <> Int32.Parse(m_working_day.ToString("yyyyMMdd")) Then
        'Me.Grid.Row(RowIndex).Height = 0
        Me.Grid.Cell(RowIndex, GRID_COLUMN_WIN).Value = "---"
        Me.Grid.Cell(RowIndex, GRID_COLUMN_WARNING).Value = String.Empty
        Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_02)
      End If

      Return True
    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return False
  End Function

  ''' <summary>
  ''' Get cut description
  ''' </summary>
  ''' <param name="DbRow"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetIsCutDescription(ByVal DbRow As CLASS_DB_ROW) As String
    Dim _value As Boolean
    _value = ((Not DbRow.IsNull(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_RECORD_TYPE)) _
                AndAlso (DbRow.Value(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_RECORD_TYPE) = EGMMeterAdjustment.EGMMeterAdjustmentConstants.RECORD_TYPE_CUT))

    Return GetBooleanText(_value)
  End Function

  ''' <summary>
  ''' Get boolean text
  ''' </summary>
  ''' <param name="Value"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetBooleanText(ByVal Value As Boolean) As String
    If (Value) Then
      Return m_text_yes
    Else
      Return m_text_no
    End If
  End Function

  ''' <summary>
  ''' Select first row only if grid has more than one row
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_SelectFirstRow() As Boolean
    If ((Me.Grid.NumRows > 1) AndAlso (Me.Grid.Row(0).Height <= 1)) Then
      Me.Grid.CurrentRow = 1
      Me.Grid.Row(1).IsSelected = True
      Me.Grid.TopRow = 1
      Call Me.Grid.RepaintRow(1)

      Return False
    End If

    Return Me.Grid.NumRows > 0
  End Function

  ''' <summary>
  ''' Enable or disable Movements Button if grid has more than one row
  ''' </summary>
  ''' <param name="SelectedRow"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_RowSelectedEvent(ByVal SelectedRow As Integer)
    If (SelectedRow < 0) Then

      Return
    End If

    ManagementEnabledAddRemoveCut(SelectedRow)
  End Sub

  ''' <summary>
  ''' Set proper values for form filters being sent to the report
  ''' </summary>
  ''' <param name="PrintData"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA) ' GUI_ReportFilter
    If (Me.m_is_multisite_center) Then
      ' SiteId
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1927), Me.m_site_id)
    End If

    ' Working day
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6177), Me.m_working_day)

    ' TerminalId
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1097), Me.m_terminal_name)

    If (UserCanSeeTotals()) Then
      ' Totals (computed)
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(8824), GUI_GetCurrencyValue_Format(m_total_win, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT))
    End If
  End Sub

  ''' <summary>
  ''' Executed before start Edition
  ''' </summary>
  ''' <param name="Row"></param>
  ''' <param name="Column"></param>
  ''' <param name="EditionCanceled"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_BeforeStartEditionEvent(ByVal Row As Integer, ByVal Column As Integer, ByRef EditionCanceled As Boolean)
    Dim _record_type As Int32

    'If not is editmode can not edit data
    If (Not m_is_edit_mode) Then
      EditionCanceled = True

      Return
    End If

    'If column ignored can edit data
    If (Column = GRID_COLUMN_IGNORED) Then
      EditionCanceled = False

      Return
    End If

    EditionCanceled = Not IsValidDataRow(Row)

    If Not Int32.TryParse(Me.Grid.Cell(Row, GRID_COLUMN_RECORD_TYPE_ID).Value, _record_type) Then
      _record_type = 0
    End If

    If (_record_type <> EGMMeterAdjustment.EGMMeterAdjustmentConstants.RECORD_TYPE_CUT) Then
      EditionCanceled = True
    End If
  End Sub

  ''' <summary>
  ''' Get SQL columnd for a grid column
  ''' </summary>
  ''' <param name="Column"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetSQLColumnForGridColumn(ByVal Column As Integer) As Integer
    Select Case Column
      Case GRID_COLUMN_COIN_IN
        Return EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0000

      Case GRID_COLUMN_COIN_OUT
        Return EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0001

      Case GRID_COLUMN_JACKPOT
        Return EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0002

      Case GRID_COLUMN_IGNORED
        Return EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_USER_IGNORED

      Case Else
        Throw New Exception(String.Format("GetSQLColumnForGridColumn: Columm {0} not defined", Column))
    End Select
  End Function

  ''' <summary>
  ''' Get SQL columnd for a grid column
  ''' </summary>
  ''' <param name="Column"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetSQLColumnForGridColumnIncrement(ByVal Column As Integer) As Integer
    Select Case Column
      Case GRID_COLUMN_COIN_IN
        Return EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0000_INCREMENT

      Case GRID_COLUMN_COIN_OUT
        Return EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0001_INCREMENT

      Case GRID_COLUMN_JACKPOT
        Return EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0002_INCREMENT

      Case Else
        Throw New Exception(String.Format("GetSQLColumnForGridColumnIncrement: Columm {0} not defined", Column))
    End Select
  End Function

  ''' <summary>
  ''' Executed when cell data change
  ''' </summary>
  ''' <param name="Row"></param>
  ''' <param name="Column"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer) ' GUI_CellDataChangedEvent
    Dim _new_value As Long
    Dim _sql_column As Integer
    Dim _row_index_seq As Long
    Dim _row_index_dt As Long

    Try
      If (Me.m_adjustment_datatable Is Nothing) Then
        Return
      End If

      'Get value of cell
      _new_value = 0
      If (Not String.IsNullOrEmpty(Me.Grid.Cell(Row, Column).Value)) Then
        _new_value = Me.Grid.Cell(Row, Column).Value
      End If

      If (Column = GRID_COLUMN_IGNORED) Then
        _new_value = IIf((_new_value = 0), 1, 0) 'We negate to obtain the new value because checkboxes in DataChangedEvent give the old value
      Else
        If String.IsNullOrEmpty(Me.Grid.Cell(Row, Column).Value) Then
          Me.Grid.Cell(Row, Column).Value = 0
        End If
      End If

      _sql_column = GetSQLColumnForGridColumn(Column)

      'Get Apropiate indexs between Grid and Dt
      _row_index_seq = Me.Grid.Cell(Row, GRID_COLUMN_ROW_INDEX_SEQ).Value
      _row_index_dt = GetIndexDataTable(_row_index_seq)

      'Set Values to DT
      Me.m_adjustment_datatable.Rows(_row_index_dt)(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_LAST_UPDATED_USER_ID) = CurrentUser.Id
      Me.m_adjustment_datatable.Rows(_row_index_dt)(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_LAST_UPDATED_USER_DATETIME) = WGDB.Now
      Me.m_adjustment_datatable.Rows(_row_index_dt)(_sql_column) = _new_value

      If (Column = GRID_COLUMN_IGNORED) Then
        If Not Me.m_adjustment_datatable.Rows(_row_index_dt).HasVersion(DataRowVersion.Original) OrElse _
          (Me.m_adjustment_datatable.Rows(_row_index_dt)(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_USER_IGNORED, DataRowVersion.Original) <> _
           Me.m_adjustment_datatable.Rows(_row_index_seq)(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_USER_IGNORED, DataRowVersion.Current)) Then

          Me.m_adjustment_datatable.Rows(_row_index_dt)(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_USER_IGNORED_DATETIME) = WGDB.Now
        End If

        'Only for Excel Report
        Me.Grid.Cell(Row, GRID_COLUMN_IGNORED_EXCEL).Value = GetBooleanText(_new_value)
      End If

      '1. Calculate all Increment & Refresh Grid Win
      If Not ManagementIncrements() Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8983), ENUM_MB_TYPE.MB_TYPE_ERROR)
        Me.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)

        Return
      End If

      'Totals
      ComputeTotals()

    Catch _ex As Exception
      Log.Exception(_ex)

      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8983), ENUM_MB_TYPE.MB_TYPE_ERROR)
      Me.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)
    End Try
  End Sub

  ''' <summary>
  ''' Process clicks on data grid (doible-clicks) and select button
  ''' </summary>
  ''' <param name="ButtonId"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)
    Select Case ButtonId
      Case frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY ' Search
        ApplySearchButton(frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY)

      Case frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0     ' Save button
        SavePendingChanges()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1     ' Remove cut button
        RemoveCutRecord()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2     ' Add cut button
        AddCutRecord()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_3     ' Unignore selected rows
        UnignoreSelectedRows()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_4     ' Ignore selected rows
        IgnoreSelectedRows()

      Case ENUM_BUTTON.BUTTON_EXCEL _
         , ENUM_BUTTON.BUTTON_PRINT                     ' Excel/Print

        Me.Grid.Column(GRID_COLUMN_IGNORED_EXCEL).Width = GRID_WIDTH_COLUMN_IGNORE

        'Mostramos columna para poder exportarla a Excel
        'Pero no queremos que en pantalla se vea como aparece y desaparece la columna
        Me.Grid.Redraw = False
        Me.IgnoreRowHeightFilterInExcelReport = True

        Try
          MyBase.GUI_ButtonClick(ButtonId)
          Me.Grid.Column(GRID_COLUMN_IGNORED_EXCEL).Width = GRID_WIDTH_ZERO
        Finally
          Me.Grid.Redraw = True
        End Try
    End Select
  End Sub

  ''' <summary>
  ''' Executed just before closing form
  ''' </summary>
  ''' <param name="CloseCanceled"></param>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)
    If Me.m_is_edit_mode AndAlso IsPendingChanges() Then
      CloseCanceled = Not DiscardChanges()
    End If
  End Sub
#End Region

#Region "Private"
  ''' <summary>
  ''' Apply search button
  ''' </summary>
  ''' <param name="ButtonId"></param>
  ''' <remarks></remarks>
  Private Sub ApplySearchButton(ByRef ButtonId As ENUM_BUTTON)
    Dim _old_total_win As Decimal

    MyBase.GUI_ButtonClick(ButtonId)

    If (Me.Grid.NumRows > 0) Then
      GUI_RowSelectedEvent(0)
    End If

    'Get total BEFORE recomputing totals
    Me.ComputeTotals()
    _old_total_win = m_total_win

    If m_is_edit_mode Then
      Me.ManagementIncrements()

      'Has changed total win
      If (HasTotalWinChanged(_old_total_win)) Then
        ' Show message recalculated win
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(9006), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, , , m_terminal_name, m_working_day)
      Else
        ' Discard changes if there are no changes in win
        m_adjustment_datatable.RejectChanges()
      End If
    End If
  End Sub

  ''' <summary>
  ''' Has total win changed?
  ''' </summary>
  ''' <param name="OldTotalWin"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function HasTotalWinChanged(ByRef OldTotalWin As Decimal) As Boolean
    Dim _new_total_win As Decimal

    'Get total AFTER recomputing totals
    Me.ComputeTotals()
    _new_total_win = m_total_win

    'Has changed total win
    Return (OldTotalWin <> _new_total_win)
  End Function

  ''' <summary>
  ''' Define all Main Grid Columns 
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GUI_StyleSheet()
    Dim _col As GUI_Controls.uc_grid.CLASS_COL_DATA

    With Me.Grid
      .Init(GRID_COLUMNS_COUNT, GRID_HEADERS_COUNT, True)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE

      'TABLE
      ' ASTERISK
      _col = Me.Grid.Column(GRID_COLUMN_ASTERISK)
      _col.Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8821)
      _col.Header(1).Text = String.Empty
      _col.Width = GRID_WIDTH_COLUMN_ASTERISK
      _col.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Working Day
      _col = Me.Grid.Column(GRID_COLUMN_WORKING_DAY)
      _col.Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8821)
      _col.Header(1).Text = String.Empty
      _col.Width = 0
      _col.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Datetime
      _col = Me.Grid.Column(GRID_COLUMN_DATETIME)
      _col.Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8821)
      _col.Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8822)
      _col.Width = GRID_WIDTH_COLUMN_DATETIME
      _col.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Denom
      _col = Me.Grid.Column(GRID_COLUMN_DENOM)
      _col.Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8821)
      _col.Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8823)
      _col.Width = GRID_WIDTH_COLUMN_DENOM
      _col.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Win
      _col = Me.Grid.Column(GRID_COLUMN_WIN)
      _col.Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2642)
      .Column(GRID_COLUMN_WIN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8824)
      .Column(GRID_COLUMN_WIN).Width = IIf(UserCanSeeTotals(), GRID_WIDTH_COLUMN_WIN, 0)  'Can see totals?
      .Column(GRID_COLUMN_WIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Coin In
      _col = Me.Grid.Column(GRID_COLUMN_COIN_IN)
      _col.Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2642)
      _col.Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8825)
      _col.Width = GRID_WIDTH_COLUMN_COIN_IN
      _col.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      _col.EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      _col.EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, 10, 0)
      _col.Editable = True

      ' Coin Out
      _col = Me.Grid.Column(GRID_COLUMN_COIN_OUT)
      _col.Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2642)
      _col.Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8826)
      _col.Width = GRID_WIDTH_COLUMN_COIN_OUT
      _col.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      _col.EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      _col.EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, 10, 0)
      _col.Editable = True

      ' Jackpot
      _col = Me.Grid.Column(GRID_COLUMN_JACKPOT)
      _col.Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2642)
      _col.Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8827)
      _col.Width = GRID_WIDTH_COLUMN_JACKPOT
      _col.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      _col.EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      _col.EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, 10, 0)
      _col.Editable = True

      ' Record type Id
      _col = Me.Grid.Column(GRID_COLUMN_RECORD_TYPE_ID)
      _col.Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2642)
      _col.Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8828)
      _col.Width = 0
      _col.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Record type
      _col = Me.Grid.Column(GRID_COLUMN_WARNING)
      _col.Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2642)
      _col.Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8828)
      _col.Width = GRID_WIDTH_COLUMN_RECORD_TYPE
      _col.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Ignore
      _col = Me.Grid.Column(GRID_COLUMN_IGNORED)
      _col.Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2642)
      _col.Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8829)
      _col.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      _col.EditionControl.Type = CONTROL_TYPE_CHECK_BOX
      _col.Editable = True
      _col.WidthFixed = GRID_WIDTH_COLUMN_IGNORE
      _col.IsColumnPrintable = False

      ' Ignore (Only for Excel Report)
      _col = Me.Grid.Column(GRID_COLUMN_IGNORED_EXCEL)
      _col.Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2642)
      _col.Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8829)
      _col.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      _col.Width = GRID_WIDTH_ZERO
      _col.IsColumnPrintable = True

      ' Cut
      _col = Me.Grid.Column(GRID_COLUMN_CUT)
      _col.Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2642)
      _col.Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8830)
      _col.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      _col.WidthFixed = GRID_WIDTH_COLUMN_CUT

      'Row Index Seq
      _col = Me.Grid.Column(GRID_COLUMN_ROW_INDEX_SEQ)
      _col.Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2642)
      _col.Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8830)
      _col.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      _col.WidthFixed = 0

      ' User
      _col = Me.Grid.Column(GRID_COLUMN_USER_MODIFIED)
      _col.Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2642)
      _col.Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7786)
      _col.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      _col.WidthFixed = GRID_WIDTH_COLUMN_USER_MODIFIED
    End With
  End Sub

  ''' <summary>
  ''' Ask for custom datatable
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.CUSTOM_DATATABLE
  End Function

  ''' <summary>
  ''' Get datatable
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_GetCustomDataTable() As DataTable
    LoadMeterAdjustmentData()

    Return m_adjustment_datatable.Copy()
  End Function

  ''' <summary>
  ''' Compute totals
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub ComputeTotals()
    panel_totals.Visible = UserCanSeeTotals()

    If (Not panel_totals.Visible) Then
      Return
    End If

    Try
      If (m_adjustment_datatable Is Nothing) Then
        m_total_win = 0
      Else
        Dim _temp_obj As Object
        _temp_obj = m_adjustment_datatable.Compute("SUM(EMP_WIN)", "EMP_WORKING_DAY = " & Int32.Parse(m_working_day.ToString("yyyyMMdd")) & _
                                                                 " AND EMP_USER_IGNORED = 0" & _
                                                                 " AND EMP_RECORD_TYPE IN (" & ENUM_SAS_METER_HISTORY.TSMH_HOURLY & ", " & EGMMeterAdjustmentConstants.RECORD_TYPE_CUT & ")")

        If ((_temp_obj Is Nothing) OrElse (IsDBNull(_temp_obj))) Then
          m_total_win = 0
        Else
          m_total_win = _temp_obj
        End If
      End If
    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    lblTotalWinValue.Text = GUI_GetCurrencyValue_Format(m_total_win, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) ' TO PASS THE WIN IN CENT TO WIN UN COIN
  End Sub

  ''' <summary>
  ''' Change status of selected rows
  ''' </summary>
  ''' <param name="Value"></param>
  ''' <remarks></remarks>
  Private Sub ChangeSelectedRows(ByVal Value As Boolean)
    If ((Me.Grid.SelectedRows() Is Nothing) OrElse (Me.Grid.SelectedRows().Length = 0)) Then
      Return
    End If

    Dim _value_str As String
    Dim _value_int As Integer

    If (Value) Then
      _value_int = 1
      _value_str = uc_grid.GRID_CHK_CHECKED
    Else
      _value_int = 0
      _value_str = uc_grid.GRID_CHK_UNCHECKED
    End If

    Dim _idx_rows As Integer
    Dim _num_row As Integer

    For _idx_rows = 0 To (Grid.SelectedRows().Length - 1)
      _num_row = Grid.SelectedRows(_idx_rows)
      If Grid.Row(_num_row).Height <= 1 Then

        Continue For
      End If

      'Set value to grid
      Grid.Cell(_num_row, GRID_COLUMN_IGNORED).Value = _value_str

      'Set value to row
      Me.m_adjustment_datatable.Rows(_num_row)(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_USER_IGNORED) = _value_int
    Next
  End Sub

  ''' <summary>
  ''' Mark as ignored the selected rows
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub IgnoreSelectedRows()
    ChangeSelectedRows(True)
    ManagementIncrements()
    ComputeTotals()
  End Sub

  ''' <summary>
  ''' Mark as unignored the selected rows
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub UnignoreSelectedRows()
    ChangeSelectedRows(False)
    ManagementIncrements()
    ComputeTotals()
  End Sub

  ''' <summary>
  ''' Is workingday day closed?
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function IsWorkingDayClosed() As Boolean
    Dim _working_day As EGMMeterAdjustment.EGMMeterAdjustmentWorkingDay

    _working_day = New EGMMeterAdjustmentWorkingDay()

    Return _working_day.IsWorkingDayClosed(m_working_day, m_site_id)
  End Function

  ''' <summary>
  ''' Has user rights to adjust meters?
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function UserCanAdjustMeters() As Boolean
    Return (CurrentUser.Permissions(ENUM_FORM.FORM_LOTTERY_METERS_ADJUSTMENT_TERMINALS).Write)
    'Return (CurrentUser.Permissions(ENUM_FORM.FORM_LOTTERY_METERS_ADJUSTMENT_TERMINALS).Write)
  End Function

  ''' <summary>
  ''' Get if user has rights for cut-points
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function UserCanManageCutPoints() As Boolean
    Return (CurrentUser.Permissions(ENUM_FORM.FORM_LOTTERY_METERS_ADJUSTMENT_MANAGE_CUT_POINTS).Read)
  End Function

  ''' <summary>
  ''' Has user rights to see totals?
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function UserCanSeeTotals() As Boolean
    Return (CurrentUser.Permissions(ENUM_FORM.FORM_LOTTERY_METERS_ADJUSTMENT_TOTALS_VISIBLE).Read)
  End Function

  ''' <summary>
  ''' Check if changes have been made in the form
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function IsPendingChanges() As Boolean
    If Me.m_adjustment_datatable Is Nothing Then
      Return False
    End If

    Return m_adjustment_period.IsDataTableModified(m_adjustment_datatable)
  End Function

  ''' <summary>
  ''' Checks whether the specified row is a valid data row
  ''' </summary>
  ''' <param name="IdxRow"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function IsValidDataRow(ByVal IdxRow As Integer) As Boolean
    If ((IdxRow >= 0) AndAlso (IdxRow < Me.Grid.NumRows)) Then
      ' All rows with data have a non-empty value in column GRID_COLUMN_GAME_TABLE_PLAY_SESSION_ID.
      Return Not String.IsNullOrEmpty(Me.Grid.Cell(IdxRow, GRID_COLUMN_DATETIME).Value)
    End If

    Return False
  End Function

  ''' <summary>
  ''' Save DataGrid to DB checking data constraints
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SavePendingChanges()
    Dim _dt_changed_meters As DataTable

    Try
      Me.m_save_button.Enabled = False

      If Not IsPendingChanges() Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1393), _
             mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO)

        Return
      End If

      'Get changed meters (for audit)
      _dt_changed_meters = m_adjustment_datatable.GetChanges()

      Using _db_trx As DB_TRX = New DB_TRX()
        If Not cls_lottery.DB_SavePendingChangesByPeriod(m_adjustment_datatable, _db_trx.SqlTransaction, EGMMeterAdjustment.EGMMeterAdjustmentConstants.RECORD_TYPE_CUT) OrElse _
           Not cls_lottery.DB_SavePendingChangesByTerminal(m_adjustment_datatable, Me.m_site_id, Int32.Parse(Me.m_working_day.ToString("yyyyMMdd")), Me.m_terminal_id, _db_trx.SqlTransaction) OrElse _
           Not DB_UpdateTerminalModified(_db_trx.SqlTransaction) Then

          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8983), ENUM_MB_TYPE.MB_TYPE_ERROR)
          Me.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)

          Return
        End If

        _db_trx.Commit()

        m_saved_almost_once = True

        'Audit
        AuditChanges(_dt_changed_meters, m_terminal_name)
      End Using

      ' Show message OK
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(108), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO)

    Catch _ex As Exception
      Log.Exception(_ex)

      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8983), ENUM_MB_TYPE.MB_TYPE_ERROR)
      Me.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)
    Finally
      Me.m_save_button.Enabled = True
    End Try

  End Sub

  Private Function GetUserName(ByRef UserId As Integer) As String
    Dim _sb As StringBuilder
    Dim _name As String

    _name = String.Empty

    Try
      _sb = New StringBuilder()
      _sb.AppendLine("SELECT   GU_USERNAME             ")
      _sb.AppendLine("  FROM   GUI_USERS               ")
      _sb.AppendLine(" WHERE   GU_USER_ID  =  @pUserId ")

      Using _db_trx As DB_TRX = New DB_TRX()
        Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UserId

          _name = _cmd.ExecuteScalar().ToString()

        End Using
      End Using

    Catch ex As Exception

    End Try

    Return _name
  End Function

  ''' <summary>
  ''' Ask user to discard changes or not
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DiscardChanges() As Boolean
    Dim _result As Boolean

    ' If pending changes MsgBox
    If Me.m_is_edit_mode AndAlso IsPendingChanges() Then
      If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(101), _
                    ENUM_MB_TYPE.MB_TYPE_WARNING, _
                    ENUM_MB_BTN.MB_BTN_YES_NO, _
                    ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_YES Then
        _result = True

        ' To avoid the application to answer about loosing changes for a second time when exiting
        Me.m_adjustment_datatable = Nothing

      End If
    End If

    Return _result
  End Function

  ''' <summary>
  ''' Set Auditor Data
  ''' </summary>
  ''' <param name="DtChangedMeters"></param>
  ''' <remarks></remarks>
  Private Sub AuditChanges(ByVal DtChangedMeters As DataTable, ByVal TerminalName As String)
    Dim _audit_generator As CLS_LOTTERY_METERS_ADJUSTMENT_METERS_AUDIT

    _audit_generator = New CLS_LOTTERY_METERS_ADJUSTMENT_METERS_AUDIT
    _audit_generator.GenerateAuditChanges(DtChangedMeters, TerminalName)
  End Sub

  ''' <summary>
  ''' Get Working Day Status
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetWorkingDayStatus() As EGMMeterAdjustment.EGMWorkingDayStatus
    Dim _wd As EGMMeterAdjustmentWorkingDay

    _wd = New EGMMeterAdjustmentWorkingDay()

    Return _wd.GetWorkingDayStatus(Me.m_working_day, Me.m_site_id)
  End Function

  ''' <summary>
  ''' Get dictionat of max values by denomination
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetDenomMaxValueDict() As IDictionary
    Dim _dict As New Dictionary(Of Decimal, Decimal)
    Dim _str_gp As String
    Dim _str_denoms_list() As String
    Dim _str_values_denom() As String
    Dim _denom As Decimal
    Dim _max_value As Decimal
    Dim _decimal_separator As String

    Try
      _str_gp = GeneralParam.GetString("AGG", "MaxValuesOfBigIncrement", "0.01:5000;0.1:500;1:50")
      _decimal_separator = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator

      If (Not _decimal_separator.Equals(",")) Then
        _str_gp = _str_gp.Replace(",", _decimal_separator)
      End If

      If (Not _decimal_separator.Equals(".")) Then
        _str_gp = _str_gp.Replace(".", _decimal_separator)
      End If

      'Get every denom/max value
      _str_denoms_list = _str_gp.Split(";")

      'Get each denom
      For Each _str_case As String In _str_denoms_list
        _str_values_denom = _str_case.Split(":")

        _denom = Decimal.Parse(_str_values_denom(0))
        _max_value = Decimal.Parse(_str_values_denom(1))

        _dict.Add(_denom, _max_value)   'Denom/MaxValue
      Next

      Return _dict
    Catch ex As Exception
      Log.Warning(ex.Message)
    End Try

    Return New Dictionary(Of Decimal, Decimal)()
  End Function

#End Region

#Region "Public"

  ''' <summary>
  ''' Opens dialog with default settings for edit mode
  ''' </summary>
  ''' <param name="MdiParent"></param>
  ''' <param name="SiteId"></param>
  ''' <param name="WorkingDay"></param>
  ''' <param name="TerminalId"></param>
  ''' <remarks></remarks>
  Public Sub ShowForEdit(ByVal SiteId As Integer, ByVal WorkingDay As DateTime, ByVal TerminalId As Int32, ByVal TerminalName As String, ByVal SiteName As String, ByVal ProviderName As String)
    m_site_id = SiteId
    m_site_name = SiteName
    m_working_day = WorkingDay
    m_terminal_id = TerminalId
    m_total_win = 0
    m_terminal_name = TerminalName
    m_provider_name = ProviderName

    m_text_yes = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278) 'Yes
    m_text_no = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)  'No

    'Is multisite center?
    m_is_multisite_center = WSI.Common.Misc.IsCenter

    Me.m_adjustment_period = New EGMMeterAdjustmentByPeriod(m_site_id, Int32.Parse(m_working_day.ToString("yyyyMMdd")), m_terminal_id)
    Me.m_is_edit_mode = ((Not IsWorkingDayClosed()) AndAlso (UserCanAdjustMeters()))

    dtp_working_day.Value = m_working_day
    ef_terminal_name.Value = m_terminal_name

    ' dict with max value of big increment
    m_dict_maxvalues = GetDenomMaxValueDict()

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(True)

  End Sub ' ShowForEdit

  ''' <summary>
  ''' Load meters adjustment data
  ''' </summary>
  ''' <remarks></remarks>
  Private Function LoadMeterAdjustmentData() As DataTable
    Try
      m_adjustment_period.LoadDataTable()

      If (Not m_adjustment_period.PeriodItemsDT Is Nothing) Then
        m_adjustment_period.PeriodItemsDT.Columns.Add("ROW_INDEX", GetType(Int32))
        LoadRowindex(m_adjustment_period.PeriodItemsDT)
        m_adjustment_period.PeriodItemsDT.AcceptChanges()
        m_adjustment_datatable = m_adjustment_period.PeriodItemsDT.Copy()
      Else
        m_adjustment_datatable = New DataTable()
      End If
    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return m_adjustment_datatable
  End Function

  Private Sub LoadRowindex(Dt As DataTable)
    Dim _row_index As Long

    For Each _dr As DataRow In Dt.Rows
      _dr("ROW_INDEX") = _row_index
      _row_index += 1
    Next
  End Sub

#End Region

#Region "Manage Cut"

  ''' <summary>
  ''' Get date for cut 
  ''' </summary>
  ''' <param name="RowIndex"></param>
  ''' <param name="RowCount"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetDateForCut(ByVal RowIndex As Integer, ByVal RowCount As Integer) As DateTime?
    Dim _date_for_cut As DateTime
    Dim _date_selected As DateTime
    Dim _date_prev As DateTime
    Dim _interval As Integer

    Try
      If (RowIndex = 0) Then
        'First row or there is only a row
        _date_prev = Me.Grid.Cell(RowIndex, GRID_COLUMN_DATETIME).Value

        _interval = GeneralParam.GetInt32("WCP", "SasMetersInterval", 60, 10, 60)

        _date_for_cut = _date_prev.AddMinutes((_interval / 2) * -1)
      Else
        _date_selected = Me.Grid.Cell(RowIndex, GRID_COLUMN_DATETIME).Value
        _date_prev = Me.Grid.Cell((RowIndex - 1), GRID_COLUMN_DATETIME).Value

        'Get the difference of minutes between the selected and the previous record and divide by 2
        'XGJ 07-MAR-2018 Wigos-8686
        _date_for_cut = _date_prev.AddSeconds(DateDiff(DateInterval.Second, _date_prev, _date_selected) / 2)
      End If

      'If its the same minute, no cut-point can be inserted
      'XGJ 07-MAR-2018 Wigos-8686
      If ((_date_for_cut.Year = _date_prev.Year) AndAlso (_date_for_cut.Month = _date_prev.Month) AndAlso (_date_for_cut.Day = _date_prev.Day) AndAlso _
          (_date_for_cut.Hour = _date_prev.Hour) AndAlso (_date_for_cut.Minute = _date_prev.Minute) AndAlso (_date_for_cut.Second = _date_prev.Second)) Then

        Return Nothing
      End If

      Return _date_for_cut
    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return Nothing
  End Function

  ''' <summary>
  ''' Add & Get cut record
  ''' </summary>
  ''' <param name="RowIndex"></param>
  ''' <param name="DateForCut"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function AddCutRecord(ByVal RowIndex As Integer, ByVal DateForCut As DateTime) As DataRow
    Dim _cut_row As DataRow
    Dim _original_row As DataRow
    Dim _row_index_seq As Long
    Dim _row_max_index_seq As Long
    Dim _row_index_dt As Long
    Dim _color_row As Color
    Dim _str_anomaly As String

    _str_anomaly = ""

    Try
      ' Get Max Seq
      _row_max_index_seq = GetMaxSeqId()

      'Add Row to DataTable
      _cut_row = m_adjustment_datatable.NewRow()

      'Get Index Seq Related with DataTable
      _row_index_seq = Me.Grid.Cell(RowIndex, GRID_COLUMN_ROW_INDEX_SEQ).Value

      'Get Real Index of DataTable from Index Seq
      _row_index_dt = GetIndexDataTable(_row_index_seq)

      If _row_index_dt < 0 OrElse _row_index_seq < 0 Then

        Return Nothing
      End If

      'Get original row to duplicate
      _original_row = m_adjustment_datatable.Rows(_row_index_dt)

      'Add DataTable Row
      _cut_row(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_SITE_ID) = _original_row(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_SITE_ID)
      _cut_row(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_WORKING_DAY) = Int32.Parse(m_working_day.ToString("yyyyMMdd"))
      _cut_row(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_DATETIME) = DateForCut
      _cut_row(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_TERMINAL_ID) = _original_row(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_TERMINAL_ID)
      _cut_row(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_GAME_ID) = _original_row(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_GAME_ID)
      _cut_row(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_DENOMINATION) = _original_row(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_DENOMINATION)
      _cut_row(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_SAS_ACCOUNTING_DENOM) = _original_row(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_SAS_ACCOUNTING_DENOM)
      _cut_row(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_WIN) = 0
      _cut_row(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0000) = _original_row(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0000)
      _cut_row(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0001) = _original_row(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0001)
      _cut_row(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0002) = _original_row(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0002)
      _cut_row(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0000_INCREMENT) = 0
      _cut_row(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0001_INCREMENT) = 0
      _cut_row(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0002_INCREMENT) = 0
      _cut_row(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_LAST_REPORTED_DATETIME) = m_adjustment_datatable.Rows(_row_index_dt)(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_LAST_REPORTED_DATETIME)
      _cut_row(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_USER_IGNORED) = 0
      _cut_row(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_RECORD_TYPE) = EGMMeterAdjustment.EGMMeterAdjustmentConstants.RECORD_TYPE_CUT
      _cut_row(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_LAST_UPDATED_USER_ID) = CurrentUser.Id
      _cut_row(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_ROW_INDEX_SEQ) = _row_max_index_seq
      m_adjustment_datatable.Rows.Add(_cut_row)

      'Add Grid Row
      Grid.InsertRow(RowIndex)
      'XGJ 07-MAR-2018 Wigos-8686
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DATETIME).Value = GUI_FormatDate(DateForCut, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      If Not IsDBNull(_cut_row(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_SAS_ACCOUNTING_DENOM)) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DENOM).Value = GUI_FormatNumber(_cut_row(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_SAS_ACCOUNTING_DENOM), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE)
      End If

      Me.Grid.Cell(RowIndex, GRID_COLUMN_WORKING_DAY).Value = _cut_row(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_WORKING_DAY)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_WIN).Value = GUI_GetCurrencyValue_Format(_cut_row(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_WIN), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_COIN_IN).Value = _cut_row(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0000)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_COIN_OUT).Value = _cut_row(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0001)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_JACKPOT).Value = _cut_row(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0002)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_RECORD_TYPE_ID).Value = EGMMeterAdjustment.EGMMeterAdjustmentConstants.RECORD_TYPE_CUT
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CUT).Value = m_text_yes
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ROW_INDEX_SEQ).Value = _cut_row(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_ROW_INDEX_SEQ)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_MODIFIED).Value = CurrentUser.Name

      'Get color and text for cut-point record
      EGMMeterAdjustmentCommon.GetColorByPriorityTsmh(ENUM_SAS_METER_HISTORY.TSMH_MANUAL_INPUT, _color_row, _str_anomaly)

      Me.Grid.Cell(RowIndex, GRID_COLUMN_WARNING).Value = _str_anomaly
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ASTERISK).Value = "*"
      Me.Grid.Row(RowIndex).BackColor = _color_row

      Return _cut_row
    Catch _ex As Exception

      Log.Exception(_ex)
    End Try

    Return Nothing
  End Function

  ''' <summary>
  ''' Add cut
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub AddCutRecord()
    Dim _cut_row As DataRow
    Dim _date_for_cut? As DateTime
    Dim _selected_row As Integer

    Try
      If (Me.Grid.SelectedRows.Length <> 1) Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8982), ENUM_MB_TYPE.MB_TYPE_ERROR)

        Return
      End If

      ' Get Date
      _date_for_cut = GetDateForCut(Me.Grid.SelectedRows(0), Me.Grid.NumRows)
      If (_date_for_cut Is Nothing) Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8987), ENUM_MB_TYPE.MB_TYPE_ERROR)

        Return
      End If

      ' Add Cut
      _selected_row = Me.Grid.SelectedRows(0)
      _cut_row = AddCutRecord(_selected_row, _date_for_cut)
      If (_cut_row Is Nothing) Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8977), ENUM_MB_TYPE.MB_TYPE_ERROR)
        Me.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)

        Return
      End If

      '1. Calculate all Increment & Refresh Grid Win
      If Not ManagementIncrements() Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8977), ENUM_MB_TYPE.MB_TYPE_ERROR)
        Me.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)

        Return
      End If

      'Totals
      ComputeTotals()

    Catch _ex As Exception
      Log.Exception(_ex)

      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8977), ENUM_MB_TYPE.MB_TYPE_ERROR)
      Me.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)
    End Try
  End Sub

  ''' <summary>
  ''' Remove cut record
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub RemoveCutRecord()
    Dim _row_index_seq As Long
    Dim _row_index_dt As Long
    Dim _select_row As Long

    Try
      If (Me.Grid.SelectedRows.Length <> 1) Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8982), ENUM_MB_TYPE.MB_TYPE_ERROR)

        Return
      End If

      If Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_RECORD_TYPE_ID).Value = EGMMeterAdjustment.EGMMeterAdjustmentConstants.RECORD_TYPE_CUT Then
        If Not NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8980), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_YES Then

          Return
        End If

        _select_row = Me.Grid.SelectedRows(0)

        'Get Index Sequence Value from Grid
        _row_index_seq = Me.Grid.Cell(_select_row, GRID_COLUMN_ROW_INDEX_SEQ).Value

        'Get Index DataTable from Grid
        _row_index_dt = Me.GetIndexDataTable(_row_index_seq)

        'Delete Row from DataTable
        Me.m_adjustment_datatable.Rows(_row_index_dt).Delete()

        'Delete Row from Grid
        Me.Grid.DeleteRowFast(_select_row)

        'Manage Grid View & State Buttons
        Me.Grid.Row(_select_row).IsSelected = True
        Me.Grid.RepaintRow(_select_row)

        '1. Calculate all Increment & Refresh Grid Win
        If Not ManagementIncrements() Then
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8981), ENUM_MB_TYPE.MB_TYPE_ERROR)
          Me.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)

          Return
        End If

        'Totals
        ComputeTotals()

        ' Manage Buttons
        Me.ManagementEnabledAddRemoveCut(_select_row)
      End If
    Catch ex As Exception
      Log.Exception(ex)

      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8981), ENUM_MB_TYPE.MB_TYPE_ERROR)
      Me.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)
    End Try
  End Sub

  ''' <summary>
  ''' Update statuts of Working Day
  ''' </summary>
  ''' <param name="NewStatus"></param>
  ''' <param name="SqlTrans"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DB_UpdateTerminalModified(ByVal SqlTrans As SqlTransaction) As Boolean
    Dim _query As StringBuilder

    _query = New StringBuilder()

    Try
      _query.AppendLine("UPDATE   EGM_METERS_BY_DAY                         ")
      _query.AppendLine("   SET   EMD_USER_PERIOD_MODIFIED = 1              ")
      _query.AppendLine("       , EMD_LAST_UPDATED = GETDATE()              ")
      _query.AppendLine("       , EMD_LAST_UPDATED_USER_ID = @pUserId       ")
      _query.AppendLine(" WHERE   EMD_WORKING_DAY          = @pWorkingDay   ")
      _query.AppendLine("   AND   EMD_SITE_ID              = @pSiteId       ")
      _query.AppendLine("   AND   EMD_TERMINAL_ID          = @pTerminalId   ")

      _query.AppendLine("UPDATE   EGM_DAILY                                 ")
      _query.AppendLine("   SET   ED_LAST_UPDATED_USER = @pUserId           ")
      _query.AppendLine("       , ED_LAST_UPDATED_USER_DATETIME = GETDATE() ")
      _query.AppendLine("       , ED_STATUS = @pNewStatus                   ")
      _query.AppendLine(" WHERE   ED_WORKING_DAY          = @pWorkingDay    ")
      _query.AppendLine("   AND   ED_SITE_ID              = @pSiteId        ")

      Using _cmd As New SqlCommand(_query.ToString, SqlTrans.Connection, SqlTrans)
        '* WHERE
        _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = Me.m_site_id
        _cmd.Parameters.Add("@pWorkingDay", SqlDbType.Int).Value = Int32.Parse(Me.m_working_day.ToString("yyyyMMdd"))
        _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = Me.m_terminal_id
        _cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = CurrentUser.Id
        _cmd.Parameters.Add("@pNewStatus", SqlDbType.Int).Value = EGMMeterAdjustment.EGMWorkingDayStatus.IN_PROGRESS

        Return (_cmd.ExecuteNonQuery() = 2)
      End Using
    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return False
  End Function ' DB_UpdateTerminalModified

  ''' <summary>
  ''' Management for Enabled Buttons (Add Cut & Remove Cut)
  ''' </summary>
  ''' <param name="SelectedRow"></param>
  ''' <remarks></remarks>
  Private Sub ManagementEnabledAddRemoveCut(ByVal SelectedRow As Long)
    If (m_wd_status = EGMWorkingDayStatus.CLOSED OrElse m_wd_status = EGMWorkingDayStatus.NONE) Then

      Return
    End If

    Me.m_add_cut_button.Enabled = Not String.IsNullOrEmpty(Me.Grid.Cell(SelectedRow, GRID_COLUMN_DATETIME).Value) AndAlso _
                                  Me.Grid.Cell(SelectedRow, GRID_COLUMN_RECORD_TYPE_ID).Value <> EGMMeterAdjustment.EGMMeterAdjustmentConstants.RECORD_TYPE_CUT AndAlso _
                                  UserCanManageCutPoints()

    Me.m_del_cut_button.Enabled = Not Me.Grid.SelectedRows Is Nothing AndAlso _
                                  Me.Grid.SelectedRows.Length = 1 AndAlso _
                                  Not String.IsNullOrEmpty(Me.Grid.Cell(SelectedRow, GRID_COLUMN_RECORD_TYPE_ID).Value) AndAlso _
                                  Me.Grid.Cell(SelectedRow, GRID_COLUMN_RECORD_TYPE_ID).Value = EGMMeterAdjustment.EGMMeterAdjustmentConstants.RECORD_TYPE_CUT AndAlso _
                                  UserCanManageCutPoints()
  End Sub

  ''' <summary>
  ''' Get the index of the DataTable according to the Sequence Index of the grid
  ''' </summary>
  ''' <param name="RowIndexGrid"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetIndexDataTable(ByVal RowIndexGrid As Long) As Long
    Dim _index_dt As Long

    For Each _dr As DataRow In Me.m_adjustment_datatable.Rows
      If _dr.RowState <> DataRowState.Deleted AndAlso _
          _dr(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_ROW_INDEX_SEQ) = RowIndexGrid Then

        Return _index_dt
      End If
      _index_dt += 1
    Next

    Return -1

  End Function

  ''' <summary>
  ''' Get Max Sequence Id from DataTable
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetMaxSeqId() As Long
    Dim _data_table_aux As DataTable
    Dim _data_row As DataRow()
    Dim _max_index_row As Long

    _max_index_row = -1

    Try
      _data_table_aux = New DataTable

      _data_table_aux = Me.m_adjustment_datatable.Copy()
      _data_row = _data_table_aux.Select(String.Empty, _data_table_aux.Columns(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_ROW_INDEX_SEQ).ColumnName & " DESC")

      If Not _data_row Is Nothing Then
        _max_index_row = _data_row(0)(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_ROW_INDEX_SEQ)
        _max_index_row += 1
      End If
    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return _max_index_row

  End Function
#End Region

#Region "Manage Cut Increments"

  ''' <summary>
  ''' Function for compute win for all increments (Coin In, Coin Out & JackPot)
  ''' </summary>
  ''' <param name="Row"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CalculateWin(ByVal DataRowWin As DataRow) As Decimal
    Dim _win As Decimal

    _win = (DataRowWin(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0000_INCREMENT) - _
           (DataRowWin(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0001_INCREMENT) + _
            DataRowWin(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0002_INCREMENT)))

    Return _win
  End Function

  Private Sub RefreshWinGrid(ByVal DataAux As DataTable)
    Dim _index_grid As Integer

    For Each _dr As DataRow In DataAux.Rows
      If _dr.RowState = DataRowState.Deleted OrElse _
         _dr(EGMMeterAdjustmentConstants.SQL_COLUMN_WORKING_DAY) <> Int32.Parse(m_working_day.ToString("yyyyMMdd")) Then

        Continue For
      End If

      Me.Grid.Cell(_index_grid, GRID_COLUMN_WIN).Value = GUI_GetCurrencyValue_Format(_dr(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_WIN), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      _index_grid = _index_grid + 1
    Next
  End Sub

  ''' <summary>
  ''' IsRowForIgnore
  ''' </summary>
  ''' <param name="DataTableAux"></param>
  ''' <param name="RowIndex"></param>
  ''' <param name="SetZeroIncrement"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function IsRowForIgnore(ByVal DataTableAux As DataTable, ByVal RowIndex As Integer, ByRef SetZeroIncrement As Boolean) As Boolean
    Dim _data_row As DataRow

    _data_row = DataTableAux.Rows(RowIndex)

    SetZeroIncrement = True

    If ((_data_row.RowState = DataRowState.Deleted) OrElse _
        (_data_row(EGMMeterAdjustmentConstants.SQL_COLUMN_USER_IGNORED))) Then

      Return True
    End If

    Dim _record_type As Integer
    _record_type = _data_row(EGMMeterAdjustmentConstants.SQL_COLUMN_RECORD_TYPE)
    If ((_record_type = WSI.Common.ENUM_SAS_METER_HISTORY.TSMH_METER_GROUP_SERVICE_RAM_CLEAR) OrElse _
        (_record_type = WSI.Common.ENUM_SAS_METER_HISTORY.TSMH_METER_GROUP_MACHINE_RAM_CLEAR) OrElse _
        (_record_type = WSI.Common.ENUM_SAS_METER_HISTORY.TSMH_METER_RESET) OrElse _
        (_record_type = WSI.Common.ENUM_SAS_METER_HISTORY.TSMH_METER_FIRST_TIME)) Then

      m_is_reset = True

      Return True
    End If

    If (_record_type = WSI.Common.ENUM_SAS_METER_HISTORY.TSMH_METER_ROLLOVER) Then
      SetZeroIncrement = False

      Return True
    End If

    Return False
  End Function

  ''' <summary>
  ''' RecalculeAllIncrementsDataTableTemp
  ''' </summary>
  ''' <param name="DataTableAux"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function RecalculeAllIncrements(ByRef DataTableAux As DataTable) As Boolean
    Dim _coin_in_absolute As Long
    Dim _coin_out_absolute As Long
    Dim _jackpot_absolute As Long
    Dim _win As Decimal
    Dim _set_zero_increment As Boolean
    Dim _first_time As Boolean

    Try
      _first_time = True
      m_is_reset = False

      'Index  Hour   Ignored   Denom   CI     CO    Jackpot
      '    0  01:00  No            1   1100   2300  3500
      '    1  02:00  No            1   3100   4300  4500
      '    2  03:00  Yes           1   5100   5300  5500
      '    3  04:00  Yes           1   6100   7300  6500
      '    4  05:00  No            1   8100   8300  7500
      '    5  06:00  Yes           5   9100   9300  8500
      'Calculate from last record to first record
      For _index_below As Integer = DataTableAux.Rows.Count - 1 To 0 Step -1
        'Skip ignored records
        If IsRowForIgnore(DataTableAux, _index_below, _set_zero_increment) Then
          If _set_zero_increment Then
            ZeroIncrement(DataTableAux, _index_below)
          End If

          ' Necessary to control the resert rows
          If (m_is_reset) Then
            ZeroIncrement(DataTableAux, _index_below - 1)
            m_is_reset = False
          End If

          Continue For
        End If

        'Here the first time: "_index_below" will take index:    4  05:00  No            5   8100   8300  7500

        'Get previous record not ignored to calculate increments
        For _index_above As Integer = _index_below - 1 To 0 Step -1
          'Skip ignored records
          If IsRowForIgnore(DataTableAux, _index_above, False) Then
            Continue For
          End If

          If _first_time AndAlso _
            DataTableAux.Rows(_index_above)(EGMMeterAdjustmentConstants.SQL_COLUMN_WORKING_DAY) = Int32.Parse(m_working_day.ToString("yyyyMMdd")) Then

            ZeroIncrement(DataTableAux, _index_above)
            _first_time = False
          End If

          'Here the first time: "_index_above" will take index:    1  02:00  No            1   3100   4300  4500
          'Get absolutes values of above
          _coin_in_absolute = DataTableAux.Rows(_index_above)(EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0000)
          _coin_out_absolute = DataTableAux.Rows(_index_above)(EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0001)
          _jackpot_absolute = DataTableAux.Rows(_index_above)(EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0002)

          ' Coin In
          SetIncrement(DataTableAux, _index_below, _index_above, EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0000, EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0000_INCREMENT, _coin_in_absolute)

          ' Coin Out
          SetIncrement(DataTableAux, _index_below, _index_above, EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0001, EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0001_INCREMENT, _coin_out_absolute)

          ' JackPot
          SetIncrement(DataTableAux, _index_below, _index_above, EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0002, EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0002_INCREMENT, _jackpot_absolute)

          ' Calculate & Set Win
          DataTableAux.Rows(_index_above)(EGMMeterAdjustmentConstants.SQL_COLUMN_WIN) = 0
          _win = CalculateWin(DataTableAux.Rows(_index_above))

          If DataTableAux.Rows(_index_above)(EGMMeterAdjustmentConstants.SQL_COLUMN_WIN) <> _win Then
            DataTableAux.Rows(_index_above)(EGMMeterAdjustmentConstants.SQL_COLUMN_WIN) = _win
          End If

          Exit For
        Next
      Next

      'Refresh Grid
      RefreshWinGrid(DataTableAux)

      Return True
    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return False
  End Function

  ''' <summary>
  ''' RecalculeAllIncrements
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ManagementIncrements() As Boolean
    Dim _dr_rows As DataRow()
    Dim _dt_aux As New DataTable

    Try
      '1. Order Datatable by DateTime
      _dr_rows = m_adjustment_datatable.Select(String.Empty, m_adjustment_datatable.Columns(EGMMeterAdjustmentConstants.SQL_COLUMN_DATETIME).ColumnName & " ASC ",
                                               DataViewRowState.Unchanged Or _
                                               DataViewRowState.Deleted Or _
                                               DataViewRowState.Added Or _
                                               DataViewRowState.ModifiedCurrent)

      '2. Copy DataTable into Temp
      _dt_aux = m_adjustment_datatable.Clone()
      For Each _dr As DataRow In _dr_rows
        _dt_aux.ImportRow(_dr)
      Next

      '3. Calculate Increments
      If Not (RecalculeAllIncrements(_dt_aux)) Then

        Return False
      End If

      '4. Order DataTable by Row index
      _dr_rows = _dt_aux.Select(String.Empty, m_adjustment_datatable.Columns(EGMMeterAdjustmentConstants.SQL_COLUMN_ROW_INDEX_SEQ).ColumnName & " ASC ",
                                DataViewRowState.Unchanged Or _
                                DataViewRowState.Deleted Or _
                                DataViewRowState.Added Or _
                                DataViewRowState.ModifiedCurrent)

      '5. Copy Temp Datatable into destination datatable
      m_adjustment_datatable.Clear()
      m_adjustment_datatable = _dt_aux.Clone()

      For Each _dr As DataRow In _dr_rows
        m_adjustment_datatable.ImportRow(_dr)
      Next

      Return True
    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return False
  End Function

  ''' <summary>
  ''' Get number of rollovers
  ''' </summary>
  ''' <param name="DataTableAux"></param>
  ''' <param name="IndexBelow"></param>
  ''' <param name="RowCheckLastIgnored"></param>
  ''' <param name="ColumnCheck"></param>
  ''' <param name="NumRollOvers"></param>
  ''' <param name="IncrementRollOver"></param>
  ''' <remarks></remarks>
  Private Sub GetNumRollOvers(ByVal DataTableAux As DataTable, ByVal IndexBelow As Integer, ByVal IndexAbove As Integer, ByVal ColumnCheck As Integer, _
                              ByRef NumRollOvers As Integer, ByRef IncrementRollOver As Decimal)

    Dim _num_roll_over_aux As Integer
    Dim _date_start As DateTime
    Dim _date_end As DateTime
    Dim _dt_aux As DataTable
    Dim _meter_max_value As Decimal
    Dim _account_denomination As Decimal

    If (IndexBelow > 0) Then
      _num_roll_over_aux = 0
      _meter_max_value = -1
      _account_denomination = GetRowSASAccountDenomination(DataTableAux.Rows(IndexBelow))

      'Get Range Dates
      _date_start = DataTableAux.Rows(IndexAbove)(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_DATETIME)
      _date_end = DataTableAux.Rows(IndexBelow)(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_DATETIME)

      'Get rows between dates, not ignored and rollover type
      DataTableAux.DefaultView.RowFilter = "EMP_DATETIME >= '" & _date_start & "'" _
                                    & " AND EMP_DATETIME <= '" & _date_end & "'" _
                                    & " AND EMP_RECORD_TYPE IN (" & ENUM_SAS_METER_HISTORY.TSMH_METER_ROLLOVER & ")" _
                                    & " AND EMP_USER_IGNORED = FALSE"

      _dt_aux = DataTableAux.DefaultView.ToTable()

      'Calculate RollOvers
      For _idx As Integer = 0 To _dt_aux.Rows.Count - 1
        If _dt_aux.Rows(_idx)(ColumnCheck) = 0 Then

          Continue For
        End If

        NumRollOvers += 1
        _num_roll_over_aux += 1

        If ((_meter_max_value < 0) AndAlso (Not IsDBNull(_dt_aux.Rows(_idx)(EGMMeterAdjustmentConstants.SQL_COLUMN_METER_MAX_VALUE))) AndAlso (_account_denomination <> 0)) Then

          _meter_max_value = _dt_aux.Rows(_idx)(EGMMeterAdjustmentConstants.SQL_COLUMN_METER_MAX_VALUE) / _account_denomination
        End If
      Next

      If (_num_roll_over_aux > 0) Then
        If (_meter_max_value < 0) Then
          _meter_max_value = MAX_METER_VALUE
        End If

        IncrementRollOver = ((_meter_max_value - _
                              DataTableAux.Rows(IndexAbove)(ColumnCheck)) + _
                              DataTableAux.Rows(IndexBelow)(ColumnCheck))

        _num_roll_over_aux = _num_roll_over_aux - 1

        If (_num_roll_over_aux > 0) Then
          IncrementRollOver = IncrementRollOver + (_meter_max_value * _num_roll_over_aux)
        End If

        IncrementRollOver *= _account_denomination
      End If
    End If
  End Sub

  ''' <summary>
  ''' Get SAS accounting denomination
  ''' </summary>
  ''' <param name="Row"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetRowSASAccountDenomination(ByVal Row As DataRow) As Decimal
    Dim _account_denomination As Decimal

    _account_denomination = 0

    If (Not IsDBNull(Row(EGMMeterAdjustmentConstants.SQL_COLUMN_SAS_ACCOUNTING_DENOM))) Then
      _account_denomination = Row(EGMMeterAdjustmentConstants.SQL_COLUMN_SAS_ACCOUNTING_DENOM)
    End If

    'If (_account_denomination = 0) Then
    '  _account_denomination = DENOM_BY_DEFAULT
    'End If

    Return _account_denomination
  End Function

  ''' <summary>
  ''' Management one increment
  ''' </summary>
  ''' <param name="DataTableAux"></param>
  ''' <param name="IndexBelow"></param>
  ''' <param name="IndexAbove"></param>
  ''' <param name="ColumnMeter"></param>
  ''' <param name="ColumnMeterIncrement"></param>
  ''' <param name="MeterValueAbsolut"></param>
  ''' <remarks></remarks>
  Private Sub SetIncrement(ByVal DataTableAux As DataTable, ByVal IndexBelow As Integer, ByVal IndexAbove As Integer, ByVal ColumnMeter As Integer, ByVal ColumnMeterIncrement As Integer, ByVal MeterValueAbsolut As Long)
    Dim _num_roll_overs As Integer
    Dim _increment_rollover As Decimal
    Dim _account_denom_above As Decimal
    Dim _increment_last_ignored As Decimal

    If (Not IsDBNull(DataTableAux.Rows(IndexAbove)(ColumnMeterIncrement))) Then
      'Get number of rollovers between records (above & below)
      _num_roll_overs = 0
      _increment_rollover = 0
      GetNumRollOvers(DataTableAux, IndexBelow, IndexAbove, ColumnMeter, _num_roll_overs, _increment_rollover)

      _account_denom_above = GetRowSASAccountDenomination(DataTableAux.Rows(IndexAbove))

      If (_num_roll_overs > 0) Then
        If DataTableAux.Rows(IndexAbove)(ColumnMeterIncrement) <> _increment_rollover Then
          DataTableAux.Rows(IndexAbove)(ColumnMeterIncrement) = _increment_rollover
        End If
      Else
        _increment_last_ignored = (DataTableAux.Rows(IndexBelow)(ColumnMeter) - MeterValueAbsolut) * _account_denom_above

        If (DataTableAux.Rows(IndexAbove)(ColumnMeterIncrement) <> _increment_last_ignored) Then
          DataTableAux.Rows(IndexAbove)(ColumnMeterIncrement) = _increment_last_ignored
        End If
      End If
    End If
  End Sub

  ''' <summary>
  ''' Sets row increments to 0
  ''' </summary>
  ''' <param name="DataTableAux"></param>
  ''' <param name="RowIndex"></param>
  ''' <remarks></remarks>
  Private Sub ZeroIncrement(ByVal DataTableAux As DataTable, ByVal RowIndex As Integer)
    Dim _data_row As DataRow

    _data_row = DataTableAux.Rows(RowIndex)

    If (_data_row.RowState <> DataRowState.Deleted) Then
      _data_row(EGMMeterAdjustmentConstants.SQL_COLUMN_WIN) = 0
      _data_row(EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0000_INCREMENT) = 0
      _data_row(EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0001_INCREMENT) = 0
      _data_row(EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0002_INCREMENT) = 0
    End If
  End Sub

  ''' <summary>
  ''' Must add record to grid?
  ''' </summary>
  ''' <param name="DbRow"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function MustAddRecordToGrid(ByVal DbRow As CLASS_DB_ROW) As Boolean
    'Records of type TSMH_METER_SAS_ACCOUNT_DENOM_CHANGE are used for computations
    ' , but it mustn't be shown in the grid
    If (Not DbRow.IsNull(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_RECORD_TYPE)) Then
      If (DbRow.Value(EGMMeterAdjustment.EGMMeterAdjustmentConstants.SQL_COLUMN_RECORD_TYPE) = ENUM_SAS_METER_HISTORY.TSMH_METER_SAS_ACCOUNT_DENOM_CHANGE) Then
        Return False
      End If
    End If

    Return True
  End Function

#End Region

End Class