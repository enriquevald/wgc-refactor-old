<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_purchase_and_sales
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.uc_date = New GUI_Controls.uc_daily_session_selector()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.ef_operation_id = New GUI_Controls.uc_entry_field()
    Me.opt_by_date = New System.Windows.Forms.RadioButton()
    Me.opt_by_operation = New System.Windows.Forms.RadioButton()
    Me.gb_operation_type = New System.Windows.Forms.GroupBox()
    Me.chk_sales_anulation = New System.Windows.Forms.CheckBox()
    Me.chk_purchase_anulation = New System.Windows.Forms.CheckBox()
    Me.chk_sales = New System.Windows.Forms.CheckBox()
    Me.chk_purchase = New System.Windows.Forms.CheckBox()
    Me.Uc_multi_currency_site_sel1 = New GUI_Controls.uc_multi_currency_site_sel()
    Me.uc_multicurrency = New GUI_Controls.uc_multi_currency_site_sel()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_operation_type.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.uc_multicurrency)
    Me.panel_filter.Controls.Add(Me.gb_operation_type)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1174, 122)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_operation_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_multicurrency, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 126)
    Me.panel_data.Size = New System.Drawing.Size(1174, 450)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1168, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1168, 4)
    '
    'uc_date
    '
    Me.uc_date.ClosingTime = 0
    Me.uc_date.ClosingTimeEnabled = False
    Me.uc_date.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_date.FromDateSelected = False
    Me.uc_date.Location = New System.Drawing.Point(6, 18)
    Me.uc_date.Name = "uc_date"
    Me.uc_date.ShowBorder = False
    Me.uc_date.Size = New System.Drawing.Size(257, 76)
    Me.uc_date.TabIndex = 2
    Me.uc_date.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_date.ToDateSelected = True
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.ef_operation_id)
    Me.gb_date.Controls.Add(Me.opt_by_date)
    Me.gb_date.Controls.Add(Me.uc_date)
    Me.gb_date.Controls.Add(Me.opt_by_operation)
    Me.gb_date.Location = New System.Drawing.Point(6, 2)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(405, 110)
    Me.gb_date.TabIndex = 0
    Me.gb_date.TabStop = False
    '
    'ef_operation_id
    '
    Me.ef_operation_id.DoubleValue = 0.0R
    Me.ef_operation_id.IntegerValue = 0
    Me.ef_operation_id.IsReadOnly = False
    Me.ef_operation_id.Location = New System.Drawing.Point(284, 38)
    Me.ef_operation_id.Name = "ef_operation_id"
    Me.ef_operation_id.PlaceHolder = Nothing
    Me.ef_operation_id.Size = New System.Drawing.Size(112, 24)
    Me.ef_operation_id.SufixText = "Sufix Text"
    Me.ef_operation_id.SufixTextVisible = True
    Me.ef_operation_id.TabIndex = 4
    Me.ef_operation_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_operation_id.TextValue = ""
    Me.ef_operation_id.TextVisible = False
    Me.ef_operation_id.TextWidth = 0
    Me.ef_operation_id.Value = ""
    Me.ef_operation_id.ValueForeColor = System.Drawing.Color.Blue
    '
    'opt_by_date
    '
    Me.opt_by_date.AutoSize = True
    Me.opt_by_date.Location = New System.Drawing.Point(7, 16)
    Me.opt_by_date.Name = "opt_by_date"
    Me.opt_by_date.Size = New System.Drawing.Size(78, 17)
    Me.opt_by_date.TabIndex = 2
    Me.opt_by_date.TabStop = True
    Me.opt_by_date.Text = "xBy Date"
    Me.opt_by_date.UseVisualStyleBackColor = True
    '
    'opt_by_operation
    '
    Me.opt_by_operation.AutoSize = True
    Me.opt_by_operation.Location = New System.Drawing.Point(265, 16)
    Me.opt_by_operation.Name = "opt_by_operation"
    Me.opt_by_operation.Size = New System.Drawing.Size(107, 17)
    Me.opt_by_operation.TabIndex = 3
    Me.opt_by_operation.TabStop = True
    Me.opt_by_operation.Text = "xBy Operation"
    Me.opt_by_operation.UseVisualStyleBackColor = True
    '
    'gb_operation_type
    '
    Me.gb_operation_type.Controls.Add(Me.chk_sales_anulation)
    Me.gb_operation_type.Controls.Add(Me.chk_purchase_anulation)
    Me.gb_operation_type.Controls.Add(Me.chk_sales)
    Me.gb_operation_type.Controls.Add(Me.chk_purchase)
    Me.gb_operation_type.Location = New System.Drawing.Point(417, 3)
    Me.gb_operation_type.Name = "gb_operation_type"
    Me.gb_operation_type.Size = New System.Drawing.Size(166, 109)
    Me.gb_operation_type.TabIndex = 5
    Me.gb_operation_type.TabStop = False
    Me.gb_operation_type.Text = "xOperationType"
    '
    'chk_sales_anulation
    '
    Me.chk_sales_anulation.AutoSize = True
    Me.chk_sales_anulation.Location = New System.Drawing.Point(9, 80)
    Me.chk_sales_anulation.Name = "chk_sales_anulation"
    Me.chk_sales_anulation.Size = New System.Drawing.Size(123, 17)
    Me.chk_sales_anulation.TabIndex = 3
    Me.chk_sales_anulation.Text = "xSales_anulation"
    Me.chk_sales_anulation.UseVisualStyleBackColor = True
    '
    'chk_purchase_anulation
    '
    Me.chk_purchase_anulation.AutoSize = True
    Me.chk_purchase_anulation.Location = New System.Drawing.Point(9, 60)
    Me.chk_purchase_anulation.Name = "chk_purchase_anulation"
    Me.chk_purchase_anulation.Size = New System.Drawing.Size(144, 17)
    Me.chk_purchase_anulation.TabIndex = 2
    Me.chk_purchase_anulation.Text = "xPurchase_anulation"
    Me.chk_purchase_anulation.UseVisualStyleBackColor = True
    '
    'chk_sales
    '
    Me.chk_sales.AutoSize = True
    Me.chk_sales.Location = New System.Drawing.Point(9, 40)
    Me.chk_sales.Name = "chk_sales"
    Me.chk_sales.Size = New System.Drawing.Size(64, 17)
    Me.chk_sales.TabIndex = 1
    Me.chk_sales.Text = "xSales"
    Me.chk_sales.UseVisualStyleBackColor = True
    '
    'chk_purchase
    '
    Me.chk_purchase.AutoSize = True
    Me.chk_purchase.Location = New System.Drawing.Point(9, 20)
    Me.chk_purchase.Name = "chk_purchase"
    Me.chk_purchase.Size = New System.Drawing.Size(85, 17)
    Me.chk_purchase.TabIndex = 0
    Me.chk_purchase.Text = "xPurchase"
    Me.chk_purchase.UseVisualStyleBackColor = True
    '
    'Uc_multi_currency_site_sel1
    '
    Me.Uc_multi_currency_site_sel1.GroupBoxText = Nothing
    Me.Uc_multi_currency_site_sel1.Location = New System.Drawing.Point(589, 6)
    Me.Uc_multi_currency_site_sel1.Name = "Uc_multi_currency_site_sel1"
    Me.Uc_multi_currency_site_sel1.SelectedCurrency = Nothing
    Me.Uc_multi_currency_site_sel1.Size = New System.Drawing.Size(169, 86)
    Me.Uc_multi_currency_site_sel1.TabIndex = 11
    '
    'uc_multicurrency
    '
    Me.uc_multicurrency.GroupBoxText = Nothing
    Me.uc_multicurrency.Location = New System.Drawing.Point(589, 2)
    Me.uc_multicurrency.Name = "uc_multicurrency"
    Me.uc_multicurrency.SelectedCurrency = Nothing
    Me.uc_multicurrency.Size = New System.Drawing.Size(169, 86)
    Me.uc_multicurrency.TabIndex = 6
    '
    'frm_purchase_and_sales
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1184, 580)
    Me.Name = "frm_purchase_and_sales"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_purchase_and_sales"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gb_date.PerformLayout()
    Me.gb_operation_type.ResumeLayout(False)
    Me.gb_operation_type.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_date As GUI_Controls.uc_daily_session_selector
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents opt_by_operation As System.Windows.Forms.RadioButton
  Friend WithEvents opt_by_date As System.Windows.Forms.RadioButton
  Friend WithEvents ef_operation_id As GUI_Controls.uc_entry_field
  Friend WithEvents gb_operation_type As System.Windows.Forms.GroupBox
  Friend WithEvents chk_sales_anulation As System.Windows.Forms.CheckBox
  Friend WithEvents ck_buy_anulation As System.Windows.Forms.CheckBox
  Friend WithEvents chk_sales As System.Windows.Forms.CheckBox
  Friend WithEvents chk_buy As System.Windows.Forms.CheckBox
  Friend WithEvents chk_purchase_anulation As System.Windows.Forms.CheckBox
  Friend WithEvents chk_purchase As System.Windows.Forms.CheckBox
  Friend WithEvents uc_multicurrency As GUI_Controls.uc_multi_currency_site_sel
  Friend WithEvents Uc_multi_currency_site_sel1 As GUI_Controls.uc_multi_currency_site_sel
End Class
