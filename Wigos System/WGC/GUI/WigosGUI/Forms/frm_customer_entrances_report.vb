﻿'-------------------------------------------------------------------
' Copyright © 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_customer_entrances_report
' DESCRIPTION:   Customer Entrances Report
' AUTHOR:        Ariel Vazquez Zerpa
' CREATION DATE: 10-DIC-2015
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 10-DEC-2015  AVZ    Initial version
' 23-DEC-2015  JCA    TFS-BUG 7992:Reporte de Recepción: No se ordenan los registros por fecha
' 02-FEB-2016  DDS    Bug 8833: Reporte de Entradas: Scroll lateral incorrecto
' 19-SEP-2016  XGJ    Eliminar contador de filas de la grid y substutuilo por una etiqueta con el número de entradas 
' 20-SEP-2016  JRC    PBI 17677:Visitas / Recepción: MEJORAS II - Ref. 48 - Reportes recepción - separar columna Nombre (GUI)
' 29-SEP-2016  FJC    PBI 17681:Visitas / Recepción: MEJORAS II - Agrupar menú (GUI)
' 05-OCT-2017  AMF    PBI 30006:Reception - Register customer output
'-------------------------------------------------------------------- 

Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Text
Imports System.Globalization

Public Class frm_customer_entrances_report
  Inherits GUI_Controls.frm_base_sel

#Region " Constants "

  ' SQL Columns
  Private Const SQL_COLUMN_VISIT_DATE As Integer = 0
  Private Const SQL_COLUMN_ENTRANCE_DATE As Integer = 1
  Private Const SQL_COLUMN_EXIT_DATE As Integer = 2
  Private Const SQL_COLUMN_EXIT_REASON As Integer = 3
  Private Const SQL_COLUMN_CARD_ACCOUNT As Integer = 4
  Private Const SQL_COLUMN_CARD_TRACK As Integer = 5
  Private Const SQL_COLUMN_HOLDER_NAME As Integer = 6
  Private Const SQL_COLUMN_CARD_BLOCKED As Integer = 7
  Private Const SQL_COLUMN_CARD_LAST_ACTIVITY As Integer = 8
  Private Const SQL_COLUMN_CARD_CREATED As Integer = 9
  Private Const SQL_COLUMN_ACCOUNT_TYPE As Integer = 10
  Private Const SQL_INIT_COLUMNS_HOLDER_DATA As Integer = 11

  ' Num grid columns / headers
  Private Const GRID_HEADER_ROWS As Integer = 2

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_VISIT_DATE As Integer = 1
  Private Const GRID_COLUMN_ENTRANCE_DATE As Integer = 2
  Private Const GRID_COLUMN_EXIT_REASON As Integer = 3
  Private Const GRID_COLUMN_EXIT_DATE As Integer = 4
  Private Const GRID_COLUMN_ACCOUNT As Integer = 5
  Private Const GRID_COLUMN_TRACK_DATA As Integer = 6
  Private Const GRID_COLUMN_HOLDER_NAME As Integer = 7
  Private Const GRID_COLUMN_CREATED As Integer = 8
  Private Const GRID_COLUMN_BLOCKED As Integer = 9
  Private Const GRID_COLUMN_BLOCKED_INT As Integer = 10
  Private Const GRID_COLUMN_LAST_ACTIVITY As Integer = 11

  Private Const GRID_COLUMN_INIT_HOLDER_DATA As Integer = 12
  Private Const GRID_COLUMNS As Integer = GRID_COLUMN_INIT_HOLDER_DATA + mdl_account_for_report.GRID_NUM_COLUMNS_HOLDER_DATA

  Private Const GRID_WIDTH_INDEX As Integer = 150
  Private Const GRID_WIDTH_DATE As Integer = 1650
  Private Const GRID_WIDTH_DATE_SHORT As Integer = 1100
  Private Const GRID_WIDTH_DATE_LONG As Integer = 2000
  Private Const GRID_WIDTH_ACCOUNT As Integer = 1150
  Private Const GRID_WIDTH_CARD_TRACK_DATA As Integer = 2300
  Private Const GRID_WIDTH_CARD_HOLDER_NAME As Integer = 2150
  Private Const GRID_WIDTH_CARD_BLOCKED As Integer = 300
  Private Const GRID_WIDTH_EXIT_REASON As Integer = 1100

  ' Type of card for where clause
  Private Const CARD_TYPE As Integer = 2
#End Region

#Region " Enums "

  ' Report Order by
  Private Enum CUSTOMER_ENTRANCES_REPORT_ORDER
    CE_REPORT_ORDER_BY_DATE_ACCOUNT = 0
    CE_REPORT_ORDER_BY_ACCOUNT_DATE = 1
  End Enum

#End Region

#Region " Members "

  ' For report filters 
  Private m_show_holder_data As Boolean
  Private m_show_totals As Boolean

  ' Totals
  Private m_total_count As Integer
  Private m_subtotal_count As Integer
  Private m_subtotal_group_by_data As Integer
  Private m_account As String
  Private m_track_data As String
  Private m_holder As String
  Private m_date_from As String
  Private m_date_to As String
  Private m_vip_holder As String
  Private m_order_by As String
  Private m_chk_holder_data As String
  Private m_chk_show_totals As String
  Private m_chk_inside_customers As String

  ' Report order
  Private m_order As CUSTOMER_ENTRANCES_REPORT_ORDER

#End Region

#Region " OVERRIDES "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS: 
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1328), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1329), m_date_to)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(792), m_order_by)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), m_account)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_track_data)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), m_holder)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4802), m_vip_holder)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(552), m_chk_holder_data)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6993), m_chk_show_totals)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(8726), m_chk_inside_customers)

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_date_from = String.Empty
    m_date_to = String.Empty

    m_order_by = String.Empty

    m_account = String.Empty
    m_track_data = String.Empty
    m_holder = String.Empty
    m_vip_holder = String.Empty

    m_chk_holder_data = String.Empty
    m_chk_show_totals = String.Empty
    m_chk_inside_customers = String.Empty

    m_date_from = GUI_FormatDate(Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime), _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMM)
    m_date_to = GUI_FormatDate(Me.uc_dsl.ToDate.AddHours(Me.uc_dsl.ClosingTime), _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMM)

    If rb_order_by_date_account.Checked Then
      m_order_by = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6994)
    Else
      m_order_by = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7481)
    End If

    m_account = uc_account_sel1.Account
    m_track_data = uc_account_sel1.TrackData
    m_holder = uc_account_sel1.Holder
    m_vip_holder = uc_account_sel1.HolderIsVip()

    m_chk_holder_data = IIf(Me.chk_holder_data.Checked, GLB_NLS_GUI_PLAYER_TRACKING.GetString(359), GLB_NLS_GUI_PLAYER_TRACKING.GetString(360))
    m_chk_show_totals = IIf(Me.chk_show_totals.Checked, GLB_NLS_GUI_PLAYER_TRACKING.GetString(359), GLB_NLS_GUI_PLAYER_TRACKING.GetString(360))
    m_chk_inside_customers = IIf(Me.chk_inside_customers.Checked, GLB_NLS_GUI_PLAYER_TRACKING.GetString(359), GLB_NLS_GUI_PLAYER_TRACKING.GetString(360))

  End Sub 'GUI_ReportUpdateFilters

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_FORM.FORM_CUSTOMER_ENTRANCES_REPORT

    Call MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    Call MyBase.GUI_InitControls()

    ' In TITO Mode set account filter not extended to hide virtual accounts
    If WSI.Common.TITO.Utils.IsTitoMode() Then
      uc_account_sel1.InitExtendedQuery(False)
    End If

    ' Form Text
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7036) & ": " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6992)

    ' Buttons
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_INFO).Visible = False

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY).Text = GLB_NLS_GUI_CONTROLS.GetString(8)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_RESET).Text = GLB_NLS_GUI_CONTROLS.GetString(7)

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(2)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_EXCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(27)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_PRINT).Text = GLB_NLS_GUI_CONTROLS.GetString(5)

    ' Date time filter
    Me.uc_dsl.Init(GLB_NLS_GUI_PLAYER_TRACKING.Id(6996), True)
    Me.uc_dsl.ClosingTimeEnabled = False

    ' Show Holder Data
    Me.chk_holder_data.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(552)

    ' Show Totals
    Me.chk_show_totals.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6993)

    ' Inside customers
    Me.chk_inside_customers.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8726)

    ' Add fields for extended search - Show Massive Search
    Me.uc_account_sel1.InitExtendedSearch()
    Me.uc_account_sel1.ShowMassiveSearch = True

    gb_last_activity.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(792)
    rb_order_by_date_account.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6994)
    rb_order_by_account_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7481)

    ' Grid
    Call GUI_StyleSheet()
    Me.Grid.IsSortable = False

    ' Set filter default values
    Call SetDefaultValues()

    ' Hidden counter on rows
    Me.Grid.PanelRightVisible = False

    Me.lbl_total_entradas.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7595)

  End Sub ' GUI_InitControls

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.uc_dsl
  End Sub 'GUI_SetInitialFocus

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Process clicks on data grid (double-clicks) and select button
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)
    Select Case ButtonId
      Case frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY
        m_total_count = 0
        lbl_numero_total_entradas.Text = m_total_count.ToString()
        m_show_holder_data = chk_holder_data.Checked
        m_show_totals = chk_show_totals.Checked
        m_order = IIf(rb_order_by_account_date.Checked, CUSTOMER_ENTRANCES_REPORT_ORDER.CE_REPORT_ORDER_BY_ACCOUNT_DATE, CUSTOMER_ENTRANCES_REPORT_ORDER.CE_REPORT_ORDER_BY_DATE_ACCOUNT)

        Call GUI_StyleSheet()
        Call MyBase.GUI_ButtonClick(ButtonId)

      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        Return
      Case Else

        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub ' GUI_ButtonClick

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean
    ' Account selection
    If Not Me.uc_account_sel1.ValidateFormat() Then
      Return False
    End If

    ' Dates selection 
    If Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate > Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _sb As StringBuilder

    _sb = New StringBuilder()

    ' Massive search
    If Not String.IsNullOrEmpty(Me.uc_account_sel1.MassiveSearchNumbers) Then
      _sb.AppendLine(Me.uc_account_sel1.CreateAndInsertAccountData())
    End If

    _sb.AppendLine("     SELECT   CV.CUT_GAMING_DAY                                                    ")
    _sb.AppendLine("            , CE.CUE_ENTRANCE_DATETIME                                             ")
    _sb.AppendLine("            , CE.CUE_EXIT_DATETIME                                                 ")
    _sb.AppendLine("            , CE.CUE_EXIT_REASON                                                   ")
    _sb.AppendLine("            , AC_ACCOUNT_ID                                                        ")
    _sb.AppendLine("            , AC_TRACK_DATA                                                        ")
    _sb.AppendLine("            , AC_HOLDER_NAME                                                       ")
    _sb.AppendLine("            , AC_BLOCKED                                                           ")
    _sb.AppendLine("            , AC_LAST_ACTIVITY                                                     ")
    _sb.AppendLine("            , AC_CREATED                                                           ")
    _sb.AppendLine("            , AC_TYPE                                                              ")

    _sb.AppendLine(mdl_account_for_report.AccountFieldsSql())

    _sb.AppendLine("       FROM   CUSTOMER_VISITS CV                                                   ")
    _sb.AppendLine(" INNER JOIN   CUSTOMER_ENTRANCES CE ON CV.CUT_VISIT_ID    = CE.CUE_VISIT_ID        ")
    _sb.AppendLine(" INNER JOIN   ACCOUNTS              ON CV.CUT_CUSTOMER_ID = ACCOUNTS.AC_ACCOUNT_ID ")

    'Add the account numbers selected in the massive search form.
    If Not String.IsNullOrEmpty(Me.uc_account_sel1.MassiveSearchNumbers) Then
      If Me.uc_account_sel1.MassiveSearchType = GUI_Controls.uc_account_sel.ENUM_MASSIVE_SEARCH_TYPE.ID_ACCOUNT Then
        _sb.AppendLine(Me.uc_account_sel1.GetInnerJoinAccountMassiveSearch("AC_ACCOUNT_ID"))
      Else
        _sb.AppendLine(Me.uc_account_sel1.GetInnerJoinAccountMassiveSearch("AC_TRACK_DATA"))
      End If
    End If

    _sb.AppendLine(GetSqlWhere())

    If (Me.rb_order_by_date_account.Checked) Then
      _sb.AppendLine(" ORDER BY CV.CUT_GAMING_DAY DESC, CE.CUE_ENTRANCE_DATETIME DESC ")
    Else
      _sb.AppendLine(" ORDER BY AC_ACCOUNT_ID ASC, CV.CUT_GAMING_DAY DESC, CE.CUE_ENTRANCE_DATETIME DESC ")
    End If

    ' Delete the temporary table
    If Not String.IsNullOrEmpty(Me.uc_account_sel1.MassiveSearchNumbers) Then
      _sb.AppendLine(Me.uc_account_sel1.DropTableAccountMassiveSearch())
    End If

    Return _sb.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _track_data As String
    Dim _account_type As AccountType

    If (m_show_totals) Then

      'Subtotal change
      Dim _group_row_data = IIf(m_order = CUSTOMER_ENTRANCES_REPORT_ORDER.CE_REPORT_ORDER_BY_ACCOUNT_DATE, DbRow.Value(SQL_COLUMN_CARD_ACCOUNT), DbRow.Value(SQL_COLUMN_VISIT_DATE))
      If m_subtotal_group_by_data > 0 And m_subtotal_group_by_data <> _group_row_data Then

        Call InsertSubtotal(RowIndex, m_subtotal_group_by_data)
        RowIndex = RowIndex + 1

        'Reset subtotals
        ResetSubTotalCounters()
      End If
      m_subtotal_group_by_data = _group_row_data

    End If

    ' Visit Date
    Me.Grid.Cell(RowIndex, GRID_COLUMN_VISIT_DATE).Value = GUI_FormatDate(DateTime.ParseExact(DbRow.Value(SQL_COLUMN_VISIT_DATE), "yyyyMMdd", CultureInfo.InvariantCulture),
                                                                          ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)

    ' Entrance Date
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ENTRANCE_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_ENTRANCE_DATE),
                                                                             ENUM_FORMAT_DATE.FORMAT_DATE_SHORT,
                                                                             ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Exit Date
    If Not DbRow.IsNull(SQL_COLUMN_EXIT_DATE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_EXIT_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_EXIT_DATE),
                                                                           ENUM_FORMAT_DATE.FORMAT_DATE_SHORT,
                                                                           ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Exit Reason
    If Not DbRow.IsNull(SQL_COLUMN_EXIT_REASON) Then
      If DbRow.Value(SQL_COLUMN_EXIT_REASON) = Entrances.Enums.ENUM_EXIT_REASON.RECEPTION Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_EXIT_REASON).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7036)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_EXIT_REASON).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7521)
      End If

    End If

    ' Card Account
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT).Value = DbRow.Value(SQL_COLUMN_CARD_ACCOUNT)

    ' Card Track Data
    _account_type = CType(DbRow.Value(SQL_COLUMN_ACCOUNT_TYPE), WSI.Common.AccountType)
    _track_data = ""
    If Not DbRow.IsNull(SQL_COLUMN_CARD_TRACK) Then
      CardNumber.VisibleTrackData(_track_data, DbRow.Value(SQL_COLUMN_CARD_TRACK), MAGNETIC_CARD_TYPES.CARD_TYPE_PLAYER, _account_type)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TRACK_DATA).Value = _track_data
    End If

    ' Holder Name
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_NAME).Value = DbRow.Value(SQL_COLUMN_HOLDER_NAME)
    End If

    ' Card State
    If DbRow.Value(SQL_COLUMN_CARD_BLOCKED) = 0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_BLOCKED).Value = "" 'GLB_NLS_GUI_INVOICING.GetString(239)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_BLOCKED).Value = GLB_NLS_GUI_INVOICING.GetString(419)
    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_BLOCKED_INT).Value = DbRow.Value(SQL_COLUMN_CARD_BLOCKED)

    ' Last Activity
    If Not DbRow.IsNull(SQL_COLUMN_CARD_LAST_ACTIVITY) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_LAST_ACTIVITY).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_CARD_LAST_ACTIVITY), _
                                                                               ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                               ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    ' Created
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CREATED).Value = GUI_FormatDate(WSI.Common.Misc.Opening((GUI_FormatDate(DbRow.Value(SQL_COLUMN_CARD_CREATED), _
                                                                       ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                       ENUM_FORMAT_TIME.FORMAT_HHMM))), _
                                                                       ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                       ENUM_FORMAT_TIME.FORMAT_TIME_NONE)

    ' HOLDER DATA
    mdl_account_for_report.SetupRowHolderData(Me.Grid, DbRow, RowIndex, GRID_COLUMN_INIT_HOLDER_DATA, SQL_INIT_COLUMNS_HOLDER_DATA)

    m_subtotal_count += 1
    m_total_count += 1

    Return True
  End Function ' GUI_SetupRow

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()

    Call ResetTotalCounters()
    Call ResetSubTotalCounters()

  End Sub ' GUI_BeforeFirstRow

  ' PURPOSE: Print totalizator row in the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()

    If (m_total_count <> 0) Then
      Me.lbl_total_entradas.Visible = True
      Me.lbl_numero_total_entradas.Visible = True
      Me.lbl_numero_total_entradas.Text = String.Format(m_total_count, "##,##0")
    End If

    If (m_show_totals) Then
      If Me.Grid.NumRows > 0 Then
        Me.Grid.AddRow()

        InsertSubtotal(Me.Grid.NumRows - 1, m_subtotal_group_by_data)
        InsertTotal(Me.Grid.NumRows - 1)

      End If
      ResetTotalCounters()
    End If

  End Sub ' GUI_AfterLastRow

#End Region

#Region "Private Functions"

  ' PURPOSE : Reset Total Counters variable
  '
  '  PARAMS :
  '     - INPUT : 
  '           - Total As TYPE_TOTAL_COUNTERS
  '           - Key As String
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub ResetTotalCounters()
    m_subtotal_group_by_data = 0
    m_total_count = 0
  End Sub ' ResetTotalCounters

  ' PURPOSE : Reset SubTotal Counters variable
  '
  '  PARAMS :
  '     - INPUT : 
  '           - Total As TYPE_TOTAL_COUNTERS
  '           - Key As String
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub ResetSubTotalCounters()

    m_subtotal_count = 0

  End Sub ' ResetSubTotalCounters

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    ' Accounts
    Me.uc_account_sel1.Clear()

    ' Date time filter
    Dim _closing_time = GetDefaultClosingTime()
    Me.uc_dsl.ToDate = New DateTime(Now.Year, Now.Month, Now.Day, _closing_time, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    Me.uc_dsl.FromDate = Me.uc_dsl.ToDate.AddDays(-1)

    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.ClosingTime = _closing_time


    ' Order by
    Me.rb_order_by_date_account.Checked = True
    Me.rb_order_by_account_date.Checked = False

    ' Show Holder data filter
    Me.chk_holder_data.Checked = False

    ' Show Totals
    Me.chk_show_totals.Checked = False

    ' Inside customers
    Me.chk_inside_customers.Checked = False

  End Sub ' SetDefaultValues

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub GUI_StyleSheet()
    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Header(1).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Visit Date
      .Column(GRID_COLUMN_VISIT_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6996)
      .Column(GRID_COLUMN_VISIT_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6995)
      .Column(GRID_COLUMN_VISIT_DATE).Width = GRID_WIDTH_DATE_SHORT
      .Column(GRID_COLUMN_VISIT_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Entrance Date
      .Column(GRID_COLUMN_ENTRANCE_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6996)
      .Column(GRID_COLUMN_ENTRANCE_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6994)
      .Column(GRID_COLUMN_ENTRANCE_DATE).Width = GRID_WIDTH_DATE_LONG
      .Column(GRID_COLUMN_ENTRANCE_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Exit Reason
      .Column(GRID_COLUMN_EXIT_REASON).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8736)
      .Column(GRID_COLUMN_EXIT_REASON).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6784)
      .Column(GRID_COLUMN_EXIT_REASON).Width = GRID_WIDTH_DATE_LONG
      .Column(GRID_COLUMN_EXIT_REASON).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Exit Date
      .Column(GRID_COLUMN_EXIT_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8736)
      .Column(GRID_COLUMN_EXIT_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6994)
      .Column(GRID_COLUMN_EXIT_DATE).Width = GRID_WIDTH_DATE_LONG
      .Column(GRID_COLUMN_EXIT_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Account - Number
      .Column(GRID_COLUMN_ACCOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_ACCOUNT).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COLUMN_ACCOUNT).Width = GRID_WIDTH_ACCOUNT
      .Column(GRID_COLUMN_ACCOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Account - Card
      .Column(GRID_COLUMN_TRACK_DATA).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_TRACK_DATA).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(212)
      .Column(GRID_COLUMN_TRACK_DATA).Width = GRID_WIDTH_CARD_TRACK_DATA
      .Column(GRID_COLUMN_TRACK_DATA).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Account - Holder Name
      .Column(GRID_COLUMN_HOLDER_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_HOLDER_NAME).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(235)
      .Column(GRID_COLUMN_HOLDER_NAME).Width = GRID_WIDTH_CARD_HOLDER_NAME
      .Column(GRID_COLUMN_HOLDER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Account - Created
      .Column(GRID_COLUMN_CREATED).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_CREATED).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(468)
      .Column(GRID_COLUMN_CREATED).Width = GRID_WIDTH_DATE - 350
      .Column(GRID_COLUMN_CREATED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Account - Status (Blocked/Non Blocked)
      .Column(GRID_COLUMN_BLOCKED).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_BLOCKED).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(418)
      .Column(GRID_COLUMN_BLOCKED).Width = GRID_WIDTH_CARD_BLOCKED
      .Column(GRID_COLUMN_BLOCKED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER


      .Column(GRID_COLUMN_BLOCKED_INT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_BLOCKED_INT).Header(1).Text = ""
      .Column(GRID_COLUMN_BLOCKED_INT).Width = 0

      ' Account - Last Activity
      .Column(GRID_COLUMN_LAST_ACTIVITY).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_LAST_ACTIVITY).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(467)
      .Column(GRID_COLUMN_LAST_ACTIVITY).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_LAST_ACTIVITY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Account holder Data
      Call mdl_account_for_report.StyleSheetHolderData(Grid, GRID_COLUMN_INIT_HOLDER_DATA, , m_show_holder_data)

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the main SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Function GetSqlWhere() As String
    Dim str_where As String = ""
    Dim str_where_account As String = ""

    str_where_account = Me.uc_account_sel1.GetFilterSQL()

    If str_where_account <> "" Then
      str_where = str_where_account
    End If

    If str_where <> String.Empty Then
      str_where &= " AND "
    End If

    str_where &= String.Format(" CE.CUE_ENTRANCE_DATETIME >= {0}", GUI_FormatDateDB(Me.uc_dsl.FromDate.AddHours(uc_dsl.ClosingTime)))

    If Me.chk_inside_customers.Checked Then
      str_where &= " AND CE.CUE_EXIT_DATETIME IS NULL "
    End If

    If (Me.uc_dsl.ToDateSelected) Then
      If str_where <> String.Empty Then
        str_where &= " AND "
      End If
      str_where &= String.Format(" CE.CUE_ENTRANCE_DATETIME <= {0}", GUI_FormatDateDB(Me.uc_dsl.ToDate.AddHours(uc_dsl.ClosingTime)))
    End If

    Return IIf(str_where = String.Empty, str_where, " WHERE " & str_where)

  End Function ' GetSqlWhere

  ' PURPOSE: Insert Subtotal
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub InsertSubtotal(ByVal RowIndex As Integer, Optional GamingDay As String = vbNullString)

    ' GG_NAME
    Me.Grid.Cell(RowIndex, GRID_COLUMN_VISIT_DATE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1046) 'subtotal_name_translated

    If (GamingDay <> vbNullString) Then
      If m_order = CUSTOMER_ENTRANCES_REPORT_ORDER.CE_REPORT_ORDER_BY_DATE_ACCOUNT Then
        GamingDay = GUI_FormatDate(DateTime.ParseExact(GamingDay, "yyyyMMdd", CultureInfo.InvariantCulture),
                                                                            ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)
      End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ENTRANCE_DATE).Value = GamingDay
    End If

    ' COUNT
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT).Value = m_subtotal_count

    ' Color Row
    Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

    Me.Grid.AddRow()

  End Sub ' InsertSubtotal

  ' PURPOSE: Insert Total
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub InsertTotal(ByVal RowIndex As Integer)

    ' GG_NAME_TRANSLATED
    Me.Grid.Cell(RowIndex, GRID_COLUMN_VISIT_DATE).Value = GLB_NLS_GUI_STATISTICS.GetString(203)

    ' COUNT
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT).Value = m_total_count

    ' Color Row
    Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)


  End Sub ' InsertTotal

#End Region

#Region "Public Functions"

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window, Optional Parameters As List(Of String) = Nothing,
                                              Optional TypeOfReport As SByte = -1)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

    If Not (Parameters Is Nothing) Then

      Select Case TypeOfReport

        Case frm_customers_entrances_visit_sel.TYPE_OF_REPORT.EntrancesByVisits,
                 frm_customers_entrances_visit_sel.TYPE_OF_REPORT.Both

          uc_account_sel1.Holder = Parameters.Item(0)
          uc_dsl.FromDate = Parameters.Item(1)
          uc_dsl.ToDate = Parameters.Item(1)
          uc_dsl.ToDate = uc_dsl.ToDate.AddDays(1)
          uc_account_sel1.TrackData = Parameters.Item(2)

        Case frm_customers_entrances_visit_sel.TYPE_OF_REPORT.EntrancesAccumulate
          uc_dsl.FromDate = Parameters.Item(0)
          uc_dsl.ToDate = Parameters.Item(0)
          uc_dsl.ToDate = uc_dsl.ToDate.AddDays(1)
      End Select

      Call Me.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)
    End If
  End Sub ' ShowForEdit

#End Region

End Class