<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_player_tracking_player_edit
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.ef_account = New GUI_Controls.uc_entry_field
    Me.ef_player_name = New GUI_Controls.uc_entry_field
    Me.gb_player_info = New System.Windows.Forms.GroupBox
    Me.TabControl1 = New System.Windows.Forms.TabControl
    Me.tab_address = New System.Windows.Forms.TabPage
    Me.ef_address_01 = New GUI_Controls.uc_entry_field
    Me.ef_address_02 = New GUI_Controls.uc_entry_field
    Me.ef_zip = New GUI_Controls.uc_entry_field
    Me.ef_address_03 = New GUI_Controls.uc_entry_field
    Me.ef_city = New GUI_Controls.uc_entry_field
    Me.tab_contact = New System.Windows.Forms.TabPage
    Me.ef_email_02 = New GUI_Controls.uc_entry_field
    Me.ef_email_01 = New GUI_Controls.uc_entry_field
    Me.ef_phone_01 = New GUI_Controls.uc_entry_field
    Me.ef_phone_02 = New GUI_Controls.uc_entry_field
    Me.tab_info = New System.Windows.Forms.TabPage
    Me.lbl_comments = New System.Windows.Forms.Label
    Me.txt_comments = New System.Windows.Forms.TextBox
    Me.dtp_born_date = New GUI_Controls.uc_date_picker
    Me.cmb_marital_status = New GUI_Controls.uc_combo
    Me.opt_sex_female = New System.Windows.Forms.RadioButton
    Me.lbl_sex = New System.Windows.Forms.Label
    Me.opt_sex_male = New System.Windows.Forms.RadioButton
    Me.GroupBox1 = New System.Windows.Forms.GroupBox
    Me.lbl_added_points_date = New GUI_Controls.uc_text_field
    Me.lbl_creation_date = New GUI_Controls.uc_text_field
    Me.lbl_points = New GUI_Controls.uc_text_field
    Me.cmb_member_level = New GUI_Controls.uc_combo
    Me.panel_data.SuspendLayout()
    Me.gb_player_info.SuspendLayout()
    Me.TabControl1.SuspendLayout()
    Me.tab_address.SuspendLayout()
    Me.tab_contact.SuspendLayout()
    Me.tab_info.SuspendLayout()
    Me.GroupBox1.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.GroupBox1)
    Me.panel_data.Controls.Add(Me.gb_player_info)
    Me.panel_data.Controls.Add(Me.ef_account)
    Me.panel_data.Size = New System.Drawing.Size(498, 389)
    '
    'ef_account
    '
    Me.ef_account.DoubleValue = 0
    Me.ef_account.IntegerValue = 0
    Me.ef_account.IsReadOnly = False
    Me.ef_account.Location = New System.Drawing.Point(13, 12)
    Me.ef_account.Name = "ef_account"
    Me.ef_account.Size = New System.Drawing.Size(312, 24)
    Me.ef_account.SufixText = "Sufix Text"
    Me.ef_account.SufixTextVisible = True
    Me.ef_account.TabIndex = 5
    Me.ef_account.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_account.TextValue = ""
    Me.ef_account.TextWidth = 120
    Me.ef_account.Value = ""
    '
    'ef_player_name
    '
    Me.ef_player_name.DoubleValue = 0
    Me.ef_player_name.IntegerValue = 0
    Me.ef_player_name.IsReadOnly = False
    Me.ef_player_name.Location = New System.Drawing.Point(20, 20)
    Me.ef_player_name.Name = "ef_player_name"
    Me.ef_player_name.Size = New System.Drawing.Size(454, 24)
    Me.ef_player_name.SufixText = "Sufix Text"
    Me.ef_player_name.SufixTextVisible = True
    Me.ef_player_name.TabIndex = 6
    Me.ef_player_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_player_name.TextValue = ""
    Me.ef_player_name.Value = ""
    '
    'gb_player_info
    '
    Me.gb_player_info.Controls.Add(Me.TabControl1)
    Me.gb_player_info.Controls.Add(Me.ef_player_name)
    Me.gb_player_info.Location = New System.Drawing.Point(3, 42)
    Me.gb_player_info.Name = "gb_player_info"
    Me.gb_player_info.Size = New System.Drawing.Size(491, 238)
    Me.gb_player_info.TabIndex = 7
    Me.gb_player_info.TabStop = False
    Me.gb_player_info.Text = "xPlayerInfo"
    '
    'TabControl1
    '
    Me.TabControl1.Controls.Add(Me.tab_address)
    Me.TabControl1.Controls.Add(Me.tab_contact)
    Me.TabControl1.Controls.Add(Me.tab_info)
    Me.TabControl1.Location = New System.Drawing.Point(10, 50)
    Me.TabControl1.Name = "TabControl1"
    Me.TabControl1.SelectedIndex = 0
    Me.TabControl1.Size = New System.Drawing.Size(474, 181)
    Me.TabControl1.TabIndex = 9
    '
    'tab_address
    '
    Me.tab_address.Controls.Add(Me.ef_address_01)
    Me.tab_address.Controls.Add(Me.ef_address_02)
    Me.tab_address.Controls.Add(Me.ef_zip)
    Me.tab_address.Controls.Add(Me.ef_address_03)
    Me.tab_address.Controls.Add(Me.ef_city)
    Me.tab_address.Location = New System.Drawing.Point(4, 22)
    Me.tab_address.Name = "tab_address"
    Me.tab_address.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_address.Size = New System.Drawing.Size(466, 155)
    Me.tab_address.TabIndex = 0
    Me.tab_address.Text = "xAddress"
    Me.tab_address.UseVisualStyleBackColor = True
    '
    'ef_address_01
    '
    Me.ef_address_01.DoubleValue = 0
    Me.ef_address_01.IntegerValue = 0
    Me.ef_address_01.IsReadOnly = False
    Me.ef_address_01.Location = New System.Drawing.Point(6, 6)
    Me.ef_address_01.Name = "ef_address_01"
    Me.ef_address_01.Size = New System.Drawing.Size(454, 24)
    Me.ef_address_01.SufixText = "Sufix Text"
    Me.ef_address_01.TabIndex = 7
    Me.ef_address_01.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_address_01.TextValue = ""
    Me.ef_address_01.TextVisible = False
    Me.ef_address_01.Value = ""
    '
    'ef_address_02
    '
    Me.ef_address_02.DoubleValue = 0
    Me.ef_address_02.IntegerValue = 0
    Me.ef_address_02.IsReadOnly = False
    Me.ef_address_02.Location = New System.Drawing.Point(6, 36)
    Me.ef_address_02.Name = "ef_address_02"
    Me.ef_address_02.Size = New System.Drawing.Size(454, 24)
    Me.ef_address_02.SufixText = "Sufix Text"
    Me.ef_address_02.TabIndex = 17
    Me.ef_address_02.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_address_02.TextValue = ""
    Me.ef_address_02.TextVisible = False
    Me.ef_address_02.Value = ""
    '
    'ef_zip
    '
    Me.ef_zip.DoubleValue = 0
    Me.ef_zip.IntegerValue = 0
    Me.ef_zip.IsReadOnly = False
    Me.ef_zip.Location = New System.Drawing.Point(6, 126)
    Me.ef_zip.Name = "ef_zip"
    Me.ef_zip.Size = New System.Drawing.Size(161, 24)
    Me.ef_zip.SufixText = "Sufix Text"
    Me.ef_zip.TabIndex = 20
    Me.ef_zip.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_zip.TextValue = "0123456789"
    Me.ef_zip.TextVisible = False
    Me.ef_zip.Value = "0123456789"
    '
    'ef_address_03
    '
    Me.ef_address_03.DoubleValue = 0
    Me.ef_address_03.IntegerValue = 0
    Me.ef_address_03.IsReadOnly = False
    Me.ef_address_03.Location = New System.Drawing.Point(6, 66)
    Me.ef_address_03.Name = "ef_address_03"
    Me.ef_address_03.Size = New System.Drawing.Size(454, 24)
    Me.ef_address_03.SufixText = "Sufix Text"
    Me.ef_address_03.TabIndex = 18
    Me.ef_address_03.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_address_03.TextValue = ""
    Me.ef_address_03.TextVisible = False
    Me.ef_address_03.Value = ""
    '
    'ef_city
    '
    Me.ef_city.DoubleValue = 0
    Me.ef_city.IntegerValue = 0
    Me.ef_city.IsReadOnly = False
    Me.ef_city.Location = New System.Drawing.Point(6, 96)
    Me.ef_city.Name = "ef_city"
    Me.ef_city.Size = New System.Drawing.Size(454, 24)
    Me.ef_city.SufixText = "Sufix Text"
    Me.ef_city.TabIndex = 19
    Me.ef_city.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_city.TextValue = ""
    Me.ef_city.TextVisible = False
    Me.ef_city.Value = ""
    '
    'tab_contact
    '
    Me.tab_contact.Controls.Add(Me.ef_email_02)
    Me.tab_contact.Controls.Add(Me.ef_email_01)
    Me.tab_contact.Controls.Add(Me.ef_phone_01)
    Me.tab_contact.Controls.Add(Me.ef_phone_02)
    Me.tab_contact.Location = New System.Drawing.Point(4, 22)
    Me.tab_contact.Name = "tab_contact"
    Me.tab_contact.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_contact.Size = New System.Drawing.Size(466, 155)
    Me.tab_contact.TabIndex = 1
    Me.tab_contact.Text = "xContact"
    Me.tab_contact.UseVisualStyleBackColor = True
    '
    'ef_email_02
    '
    Me.ef_email_02.DoubleValue = 0
    Me.ef_email_02.IntegerValue = 0
    Me.ef_email_02.IsReadOnly = False
    Me.ef_email_02.Location = New System.Drawing.Point(6, 36)
    Me.ef_email_02.Name = "ef_email_02"
    Me.ef_email_02.Size = New System.Drawing.Size(454, 24)
    Me.ef_email_02.SufixText = "Sufix Text"
    Me.ef_email_02.SufixTextVisible = True
    Me.ef_email_02.TabIndex = 22
    Me.ef_email_02.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_email_02.TextValue = ""
    Me.ef_email_02.Value = ""
    '
    'ef_email_01
    '
    Me.ef_email_01.DoubleValue = 0
    Me.ef_email_01.IntegerValue = 0
    Me.ef_email_01.IsReadOnly = False
    Me.ef_email_01.Location = New System.Drawing.Point(6, 6)
    Me.ef_email_01.Name = "ef_email_01"
    Me.ef_email_01.Size = New System.Drawing.Size(454, 24)
    Me.ef_email_01.SufixText = "Sufix Text"
    Me.ef_email_01.SufixTextVisible = True
    Me.ef_email_01.TabIndex = 21
    Me.ef_email_01.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_email_01.TextValue = ""
    Me.ef_email_01.Value = ""
    '
    'ef_phone_01
    '
    Me.ef_phone_01.DoubleValue = 0
    Me.ef_phone_01.IntegerValue = 0
    Me.ef_phone_01.IsReadOnly = False
    Me.ef_phone_01.Location = New System.Drawing.Point(6, 66)
    Me.ef_phone_01.Name = "ef_phone_01"
    Me.ef_phone_01.Size = New System.Drawing.Size(231, 24)
    Me.ef_phone_01.SufixText = "Sufix Text"
    Me.ef_phone_01.SufixTextVisible = True
    Me.ef_phone_01.TabIndex = 9
    Me.ef_phone_01.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_phone_01.TextValue = "01234567890123456789"
    Me.ef_phone_01.Value = "01234567890123456789"
    '
    'ef_phone_02
    '
    Me.ef_phone_02.DoubleValue = 0
    Me.ef_phone_02.IntegerValue = 0
    Me.ef_phone_02.IsReadOnly = False
    Me.ef_phone_02.Location = New System.Drawing.Point(7, 97)
    Me.ef_phone_02.Name = "ef_phone_02"
    Me.ef_phone_02.Size = New System.Drawing.Size(230, 24)
    Me.ef_phone_02.SufixText = "Sufix Text"
    Me.ef_phone_02.SufixTextVisible = True
    Me.ef_phone_02.TabIndex = 10
    Me.ef_phone_02.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_phone_02.TextValue = ""
    Me.ef_phone_02.Value = ""
    '
    'tab_info
    '
    Me.tab_info.Controls.Add(Me.lbl_comments)
    Me.tab_info.Controls.Add(Me.txt_comments)
    Me.tab_info.Controls.Add(Me.dtp_born_date)
    Me.tab_info.Controls.Add(Me.cmb_marital_status)
    Me.tab_info.Controls.Add(Me.opt_sex_female)
    Me.tab_info.Controls.Add(Me.lbl_sex)
    Me.tab_info.Controls.Add(Me.opt_sex_male)
    Me.tab_info.Location = New System.Drawing.Point(4, 22)
    Me.tab_info.Name = "tab_info"
    Me.tab_info.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_info.Size = New System.Drawing.Size(466, 155)
    Me.tab_info.TabIndex = 2
    Me.tab_info.Text = "xInformation"
    Me.tab_info.UseVisualStyleBackColor = True
    '
    'lbl_comments
    '
    Me.lbl_comments.AutoSize = True
    Me.lbl_comments.Location = New System.Drawing.Point(7, 70)
    Me.lbl_comments.Name = "lbl_comments"
    Me.lbl_comments.Size = New System.Drawing.Size(87, 13)
    Me.lbl_comments.TabIndex = 18
    Me.lbl_comments.Text = "xComentarios"
    '
    'txt_comments
    '
    Me.txt_comments.Location = New System.Drawing.Point(7, 89)
    Me.txt_comments.Multiline = True
    Me.txt_comments.Name = "txt_comments"
    Me.txt_comments.Size = New System.Drawing.Size(453, 60)
    Me.txt_comments.TabIndex = 17
    '
    'dtp_born_date
    '
    Me.dtp_born_date.Checked = True
    Me.dtp_born_date.IsReadOnly = False
    Me.dtp_born_date.Location = New System.Drawing.Point(6, 6)
    Me.dtp_born_date.Name = "dtp_born_date"
    Me.dtp_born_date.ShowCheckBox = False
    Me.dtp_born_date.ShowUpDown = False
    Me.dtp_born_date.Size = New System.Drawing.Size(223, 24)
    Me.dtp_born_date.SufixText = "Sufix Text"
    Me.dtp_born_date.TabIndex = 15
    Me.dtp_born_date.TextVisible = False
    Me.dtp_born_date.Value = New Date(2002, 6, 6, 13, 40, 22, 110)
    '
    'cmb_marital_status
    '
    Me.cmb_marital_status.IsReadOnly = False
    Me.cmb_marital_status.Location = New System.Drawing.Point(237, 6)
    Me.cmb_marital_status.Name = "cmb_marital_status"
    Me.cmb_marital_status.SelectedIndex = -1
    Me.cmb_marital_status.Size = New System.Drawing.Size(223, 24)
    Me.cmb_marital_status.SufixText = "Sufix Text"
    Me.cmb_marital_status.TabIndex = 16
    Me.cmb_marital_status.TextVisible = False
    '
    'opt_sex_female
    '
    Me.opt_sex_female.Location = New System.Drawing.Point(394, 36)
    Me.opt_sex_female.Name = "opt_sex_female"
    Me.opt_sex_female.Size = New System.Drawing.Size(66, 16)
    Me.opt_sex_female.TabIndex = 13
    Me.opt_sex_female.Text = "xMujer"
    '
    'lbl_sex
    '
    Me.lbl_sex.AutoSize = True
    Me.lbl_sex.Location = New System.Drawing.Point(271, 36)
    Me.lbl_sex.Name = "lbl_sex"
    Me.lbl_sex.Size = New System.Drawing.Size(43, 13)
    Me.lbl_sex.TabIndex = 14
    Me.lbl_sex.Text = "xSexo"
    '
    'opt_sex_male
    '
    Me.opt_sex_male.Location = New System.Drawing.Point(318, 36)
    Me.opt_sex_male.Name = "opt_sex_male"
    Me.opt_sex_male.Size = New System.Drawing.Size(71, 16)
    Me.opt_sex_male.TabIndex = 12
    Me.opt_sex_male.Text = "xHombre"
    '
    'GroupBox1
    '
    Me.GroupBox1.Controls.Add(Me.lbl_added_points_date)
    Me.GroupBox1.Controls.Add(Me.lbl_creation_date)
    Me.GroupBox1.Controls.Add(Me.lbl_points)
    Me.GroupBox1.Controls.Add(Me.cmb_member_level)
    Me.GroupBox1.Location = New System.Drawing.Point(3, 286)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Size = New System.Drawing.Size(491, 96)
    Me.GroupBox1.TabIndex = 8
    Me.GroupBox1.TabStop = False
    Me.GroupBox1.Text = "xFidelityProgram"
    '
    'lbl_added_points_date
    '
    Me.lbl_added_points_date.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_added_points_date.IsReadOnly = True
    Me.lbl_added_points_date.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_added_points_date.LabelForeColor = System.Drawing.Color.Blue
    Me.lbl_added_points_date.Location = New System.Drawing.Point(235, 20)
    Me.lbl_added_points_date.Name = "lbl_added_points_date"
    Me.lbl_added_points_date.Size = New System.Drawing.Size(223, 25)
    Me.lbl_added_points_date.SufixText = "Sufix Text"
    Me.lbl_added_points_date.SufixTextVisible = True
    Me.lbl_added_points_date.TabIndex = 20
    Me.lbl_added_points_date.Value = "12345"
    '
    'lbl_creation_date
    '
    Me.lbl_creation_date.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_creation_date.IsReadOnly = True
    Me.lbl_creation_date.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_creation_date.LabelForeColor = System.Drawing.Color.Blue
    Me.lbl_creation_date.Location = New System.Drawing.Point(235, 51)
    Me.lbl_creation_date.Name = "lbl_creation_date"
    Me.lbl_creation_date.Size = New System.Drawing.Size(223, 25)
    Me.lbl_creation_date.SufixText = "Sufix Text"
    Me.lbl_creation_date.SufixTextVisible = True
    Me.lbl_creation_date.TabIndex = 19
    Me.lbl_creation_date.Value = "12345"
    '
    'lbl_points
    '
    Me.lbl_points.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_points.IsReadOnly = True
    Me.lbl_points.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_points.LabelForeColor = System.Drawing.Color.Blue
    Me.lbl_points.Location = New System.Drawing.Point(6, 20)
    Me.lbl_points.Name = "lbl_points"
    Me.lbl_points.Size = New System.Drawing.Size(223, 25)
    Me.lbl_points.SufixText = "Sufix Text"
    Me.lbl_points.SufixTextVisible = True
    Me.lbl_points.TabIndex = 18
    Me.lbl_points.Value = "12345"
    '
    'cmb_member_level
    '
    Me.cmb_member_level.IsReadOnly = False
    Me.cmb_member_level.Location = New System.Drawing.Point(6, 51)
    Me.cmb_member_level.Name = "cmb_member_level"
    Me.cmb_member_level.SelectedIndex = -1
    Me.cmb_member_level.Size = New System.Drawing.Size(223, 24)
    Me.cmb_member_level.SufixText = "Sufix Text"
    Me.cmb_member_level.SufixTextVisible = True
    Me.cmb_member_level.TabIndex = 17
    '
    'frm_player_tracking_player_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(600, 398)
    Me.Name = "frm_player_tracking_player_edit"
    Me.Text = "frm_player_tracking_player_edit"
    Me.panel_data.ResumeLayout(False)
    Me.gb_player_info.ResumeLayout(False)
    Me.TabControl1.ResumeLayout(False)
    Me.tab_address.ResumeLayout(False)
    Me.tab_contact.ResumeLayout(False)
    Me.tab_info.ResumeLayout(False)
    Me.tab_info.PerformLayout()
    Me.GroupBox1.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_account As GUI_Controls.uc_entry_field
  Friend WithEvents ef_player_name As GUI_Controls.uc_entry_field
  Friend WithEvents gb_player_info As System.Windows.Forms.GroupBox
  Friend WithEvents ef_phone_02 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_phone_01 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_address_01 As GUI_Controls.uc_entry_field
  Friend WithEvents opt_sex_female As System.Windows.Forms.RadioButton
  Friend WithEvents opt_sex_male As System.Windows.Forms.RadioButton
  Friend WithEvents lbl_sex As System.Windows.Forms.Label
  Friend WithEvents dtp_born_date As GUI_Controls.uc_date_picker
  Friend WithEvents cmb_marital_status As GUI_Controls.uc_combo
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
  Friend WithEvents cmb_member_level As GUI_Controls.uc_combo
  Friend WithEvents lbl_points As GUI_Controls.uc_text_field
  Friend WithEvents ef_address_03 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_address_02 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_zip As GUI_Controls.uc_entry_field
  Friend WithEvents ef_city As GUI_Controls.uc_entry_field
  Friend WithEvents ef_email_02 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_email_01 As GUI_Controls.uc_entry_field
  Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
  Friend WithEvents tab_address As System.Windows.Forms.TabPage
  Friend WithEvents tab_contact As System.Windows.Forms.TabPage
  Friend WithEvents tab_info As System.Windows.Forms.TabPage
  Friend WithEvents lbl_comments As System.Windows.Forms.Label
  Friend WithEvents txt_comments As System.Windows.Forms.TextBox
  Friend WithEvents lbl_creation_date As GUI_Controls.uc_text_field
  Friend WithEvents lbl_added_points_date As GUI_Controls.uc_text_field
End Class
