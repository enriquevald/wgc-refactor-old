'-------------------------------------------------------------------
' Copyright � 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
'   MODULE NAME : frm_terminal_meter_delta_sel
'   DESCRIPTION : Selection terminal meter delta
'        AUTHOR : Alberto Marcos
' CREATION DATE : 25-MAR-2015
'
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 25-MAR-2015  AMF    Initial version. Item 981
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data

Public Class frm_terminal_meter_delta_sel
  Inherits frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub
  Friend WithEvents ef_delta_name As GUI_Controls.uc_entry_field

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.ef_delta_name = New GUI_Controls.uc_entry_field
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.ef_delta_name)
    Me.panel_filter.Size = New System.Drawing.Size(389, 69)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_delta_name, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 73)
    Me.panel_data.Size = New System.Drawing.Size(389, 273)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(383, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(383, 4)
    '
    'ef_delta_name
    '
    Me.ef_delta_name.DoubleValue = 0
    Me.ef_delta_name.IntegerValue = 0
    Me.ef_delta_name.IsReadOnly = False
    Me.ef_delta_name.Location = New System.Drawing.Point(6, 24)
    Me.ef_delta_name.Name = "ef_delta_name"
    Me.ef_delta_name.PlaceHolder = Nothing
    Me.ef_delta_name.Size = New System.Drawing.Size(227, 25)
    Me.ef_delta_name.SufixText = "Sufix Text"
    Me.ef_delta_name.SufixTextVisible = True
    Me.ef_delta_name.TabIndex = 12
    Me.ef_delta_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_delta_name.TextValue = ""
    Me.ef_delta_name.TextWidth = 60
    Me.ef_delta_name.Value = ""
    Me.ef_delta_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'frm_terminal_meter_delta_sel
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(397, 350)
    Me.Name = "frm_terminal_meter_delta_sel"
    Me.Text = "frm_terminal_meter_delta_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Constants "

  Private Const SQL_COLUMN_IDENTIFIER As Integer = 0
  Private Const SQL_COLUMN_DELTA_NAME As Integer = 1

  Private Const GRID_COLUMN_IDENTIFIER As Integer = 0
  Private Const GRID_COLUMN_DELTA_NAME As Integer = 1

  Private Const GRID_COLUMNS As Integer = 2
  Private Const GRID_HEADER_ROWS As Integer = 1

  Private Const FORM_DB_MIN_VERSION As Short = 264

#End Region ' Constants

#Region " Members "

  Private m_form_licence As New CLASS_LICENCE

  Private m_refreshing_grid As Boolean

  ' For report filters 
  Private m_name_delta As String

#End Region ' Members

#Region " OVERRIDES "

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_TERMINAL_METER_DELTA_SEL
    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)

    'Call Base Form proc
    Call MyBase.GUI_SetFormId()
    '------------------------------------------------

  End Sub ' GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6061)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Text = GLB_NLS_GUI_CONTROLS.GetString(25)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    ' Cancel button text =>  "Exit"
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(2)

    ' Filter
    Me.ef_delta_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6062)

    Call Me.ef_delta_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()

  End Sub ' GUI_FilterReset

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_SELECT
        Call ShowDeltaDetails()

      Case ENUM_BUTTON.BUTTON_NEW
        Call AddNewDelta()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Name selection
    'TODO ALBERTO

    Return True

  End Function ' GUI_FilterCheck

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _sb As System.Text.StringBuilder

    'todo alberto
    _sb = New System.Text.StringBuilder()

    _sb.AppendLine(" SELECT   TMD_ID ")
    _sb.AppendLine("        , TMD_DESCRIPTION ")
    _sb.AppendLine("   FROM   TERMINAL_METER_DELTA ")

    _sb.AppendLine(GetSqlWhere())

    _sb.AppendLine(" ORDER BY TMD_DESCRIPTION ")

    Return _sb.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Me.Grid.Cell(RowIndex, GRID_COLUMN_IDENTIFIER).Value = DbRow.Value(SQL_COLUMN_IDENTIFIER)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DELTA_NAME).Value = DbRow.Value(SQL_COLUMN_DELTA_NAME)

    Return True

  End Function ' GUI_SetupRow

  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.ef_delta_name

  End Sub ' GUI_SetInitialFocus

#Region " GUI Reports "

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6062), m_name_delta)

  End Sub ' GUI_ReportFilter

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_name_delta = String.Empty

    m_name_delta = Me.ef_delta_name.Value


  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE : Opens dialog with default settings for edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE : Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS :
  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Internal identifier
      .Column(GRID_COLUMN_IDENTIFIER).Header.Text = ""
      .Column(GRID_COLUMN_IDENTIFIER).Width = 0

      ' Delta name
      .Column(GRID_COLUMN_DELTA_NAME).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6062)
      .Column(GRID_COLUMN_DELTA_NAME).Width = 3135
      .Column(GRID_COLUMN_DELTA_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE : Set default values to filters
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub SetDefaultValues()

    Me.ef_delta_name.Value = String.Empty

  End Sub ' SetDefaultValues

  ' PURPOSE : Get the SQL WHERE
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Function GetSqlWhere() As String

    Dim _str_where As String
    Dim _str_aux1 As String

    _str_where = ""

    If Me.ef_delta_name.Value <> String.Empty Then
      _str_aux1 = UCase(Me.ef_delta_name.Value).Replace("'", "''")
      _str_where += "WHERE ( UPPER(TMD_DESCRIPTION) LIKE '" & _str_aux1 & "%' OR"
      _str_where += " UPPER(TMD_DESCRIPTION) LIKE '% " & _str_aux1 & "%') "
    End If

    Return _str_where

  End Function ' GetSqlWhere

  ' PURPOSE : Adds a new software version package to the system
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub ShowDeltaDetails()

    Dim _idx_row As Short
    Dim _row_selected As Boolean
    Dim _frm_edit As frm_terminal_meter_delta_edit

    ' Get the selected row
    _row_selected = False
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        _row_selected = True

        Exit For
      End If
    Next

    If Not _row_selected Then
      ' No selected row 
      Call Me.Grid.Focus()

      Exit Sub
    End If

    _frm_edit = New frm_terminal_meter_delta_edit

    ' Show the selected Licence
    Call _frm_edit.ShowEditItem(Me.Grid.Cell(_idx_row, GRID_COLUMN_IDENTIFIER).Value)

    _frm_edit = Nothing

    Call Me.Grid.Focus()

  End Sub ' ShowPackageDetails

  ' PURPOSE : Adds a new licence to the system
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub AddNewDelta()

    Dim _frm_edit As frm_terminal_meter_delta_edit

    _frm_edit = New frm_terminal_meter_delta_edit

    Call _frm_edit.ShowNewItem()

    _frm_edit = Nothing

    Call Me.Grid.Focus()

  End Sub ' AddNewLicence

#End Region ' Private Functions

#Region "Events"

#End Region  ' Events

End Class
