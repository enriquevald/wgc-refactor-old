<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_evidence_viewer
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.wb_evidence = New System.Windows.Forms.WebBrowser()
    Me.tab_evidence_viewer = New System.Windows.Forms.TabControl()
    Me.gb_operation = New System.Windows.Forms.GroupBox()
    Me.ef_total_payment = New GUI_Controls.uc_entry_field()
    Me.ef_tax3_amount = New GUI_Controls.uc_entry_field()
    Me.ef_tax2_amount = New GUI_Controls.uc_entry_field()
    Me.ef_tax1_amount = New GUI_Controls.uc_entry_field()
    Me.ef_prize_amount = New GUI_Controls.uc_entry_field()
    Me.ef_evidence_CURP = New GUI_Controls.uc_entry_field()
    Me.ef_evidence_RFC = New GUI_Controls.uc_entry_field()
    Me.ef_evidence_holder_name = New GUI_Controls.uc_entry_field()
    Me.dt_operation_date = New GUI_Controls.uc_date_picker()
    Me.panel_data.SuspendLayout()
    Me.gb_operation.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_operation)
    Me.panel_data.Controls.Add(Me.tab_evidence_viewer)
    Me.panel_data.Controls.Add(Me.wb_evidence)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(1073, 518)
    '
    'wb_evidence
    '
    Me.wb_evidence.Location = New System.Drawing.Point(322, 640)
    Me.wb_evidence.MinimumSize = New System.Drawing.Size(23, 20)
    Me.wb_evidence.Name = "wb_evidence"
    Me.wb_evidence.Size = New System.Drawing.Size(63, 51)
    Me.wb_evidence.TabIndex = 1
    '
    'tab_evidence_viewer
    '
    Me.tab_evidence_viewer.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.tab_evidence_viewer.Location = New System.Drawing.Point(364, 13)
    Me.tab_evidence_viewer.Name = "tab_evidence_viewer"
    Me.tab_evidence_viewer.SelectedIndex = 0
    Me.tab_evidence_viewer.Size = New System.Drawing.Size(709, 502)
    Me.tab_evidence_viewer.TabIndex = 2
    '
    'gb_operation
    '
    Me.gb_operation.Controls.Add(Me.ef_total_payment)
    Me.gb_operation.Controls.Add(Me.ef_tax3_amount)
    Me.gb_operation.Controls.Add(Me.ef_tax2_amount)
    Me.gb_operation.Controls.Add(Me.ef_tax1_amount)
    Me.gb_operation.Controls.Add(Me.ef_prize_amount)
    Me.gb_operation.Controls.Add(Me.ef_evidence_CURP)
    Me.gb_operation.Controls.Add(Me.ef_evidence_RFC)
    Me.gb_operation.Controls.Add(Me.ef_evidence_holder_name)
    Me.gb_operation.Controls.Add(Me.dt_operation_date)
    Me.gb_operation.Location = New System.Drawing.Point(3, 3)
    Me.gb_operation.Name = "gb_operation"
    Me.gb_operation.Size = New System.Drawing.Size(355, 309)
    Me.gb_operation.TabIndex = 3
    Me.gb_operation.TabStop = False
    Me.gb_operation.Text = "xOperation"
    '
    'ef_total_payment
    '
    Me.ef_total_payment.DoubleValue = 0.0R
    Me.ef_total_payment.IntegerValue = 0
    Me.ef_total_payment.IsReadOnly = False
    Me.ef_total_payment.Location = New System.Drawing.Point(7, 255)
    Me.ef_total_payment.Name = "ef_total_payment"
    Me.ef_total_payment.PlaceHolder = Nothing
    Me.ef_total_payment.Size = New System.Drawing.Size(226, 24)
    Me.ef_total_payment.SufixText = "Sufix Text"
    Me.ef_total_payment.SufixTextVisible = True
    Me.ef_total_payment.TabIndex = 7
    Me.ef_total_payment.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_total_payment.TextValue = ""
    Me.ef_total_payment.TextWidth = 120
    Me.ef_total_payment.Value = ""
    Me.ef_total_payment.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_tax3_amount
    '
    Me.ef_tax3_amount.DoubleValue = 0.0R
    Me.ef_tax3_amount.IntegerValue = 0
    Me.ef_tax3_amount.IsReadOnly = False
    Me.ef_tax3_amount.Location = New System.Drawing.Point(48, 226)
    Me.ef_tax3_amount.Name = "ef_tax3_amount"
    Me.ef_tax3_amount.PlaceHolder = Nothing
    Me.ef_tax3_amount.Size = New System.Drawing.Size(185, 25)
    Me.ef_tax3_amount.SufixText = "Sufix Text"
    Me.ef_tax3_amount.SufixTextVisible = True
    Me.ef_tax3_amount.TabIndex = 8
    Me.ef_tax3_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_tax3_amount.TextValue = ""
    Me.ef_tax3_amount.Value = ""
    Me.ef_tax3_amount.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_tax2_amount
    '
    Me.ef_tax2_amount.DoubleValue = 0.0R
    Me.ef_tax2_amount.IntegerValue = 0
    Me.ef_tax2_amount.IsReadOnly = False
    Me.ef_tax2_amount.Location = New System.Drawing.Point(7, 196)
    Me.ef_tax2_amount.Name = "ef_tax2_amount"
    Me.ef_tax2_amount.PlaceHolder = Nothing
    Me.ef_tax2_amount.Size = New System.Drawing.Size(226, 24)
    Me.ef_tax2_amount.SufixText = "Sufix Text"
    Me.ef_tax2_amount.SufixTextVisible = True
    Me.ef_tax2_amount.TabIndex = 6
    Me.ef_tax2_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_tax2_amount.TextValue = ""
    Me.ef_tax2_amount.TextWidth = 120
    Me.ef_tax2_amount.Value = ""
    Me.ef_tax2_amount.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_tax1_amount
    '
    Me.ef_tax1_amount.DoubleValue = 0.0R
    Me.ef_tax1_amount.IntegerValue = 0
    Me.ef_tax1_amount.IsReadOnly = False
    Me.ef_tax1_amount.Location = New System.Drawing.Point(7, 166)
    Me.ef_tax1_amount.Name = "ef_tax1_amount"
    Me.ef_tax1_amount.PlaceHolder = Nothing
    Me.ef_tax1_amount.Size = New System.Drawing.Size(226, 24)
    Me.ef_tax1_amount.SufixText = "Sufix Text"
    Me.ef_tax1_amount.SufixTextVisible = True
    Me.ef_tax1_amount.TabIndex = 5
    Me.ef_tax1_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_tax1_amount.TextValue = ""
    Me.ef_tax1_amount.TextWidth = 120
    Me.ef_tax1_amount.Value = ""
    Me.ef_tax1_amount.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_prize_amount
    '
    Me.ef_prize_amount.DoubleValue = 0.0R
    Me.ef_prize_amount.IntegerValue = 0
    Me.ef_prize_amount.IsReadOnly = False
    Me.ef_prize_amount.Location = New System.Drawing.Point(7, 136)
    Me.ef_prize_amount.Name = "ef_prize_amount"
    Me.ef_prize_amount.PlaceHolder = Nothing
    Me.ef_prize_amount.Size = New System.Drawing.Size(226, 24)
    Me.ef_prize_amount.SufixText = "Sufix Text"
    Me.ef_prize_amount.SufixTextVisible = True
    Me.ef_prize_amount.TabIndex = 4
    Me.ef_prize_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_prize_amount.TextValue = ""
    Me.ef_prize_amount.TextWidth = 120
    Me.ef_prize_amount.Value = ""
    Me.ef_prize_amount.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_evidence_CURP
    '
    Me.ef_evidence_CURP.DoubleValue = 0.0R
    Me.ef_evidence_CURP.IntegerValue = 0
    Me.ef_evidence_CURP.IsReadOnly = False
    Me.ef_evidence_CURP.Location = New System.Drawing.Point(7, 106)
    Me.ef_evidence_CURP.Name = "ef_evidence_CURP"
    Me.ef_evidence_CURP.PlaceHolder = Nothing
    Me.ef_evidence_CURP.Size = New System.Drawing.Size(285, 24)
    Me.ef_evidence_CURP.SufixText = "Sufix Text"
    Me.ef_evidence_CURP.SufixTextVisible = True
    Me.ef_evidence_CURP.TabIndex = 3
    Me.ef_evidence_CURP.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_evidence_CURP.TextValue = ""
    Me.ef_evidence_CURP.TextWidth = 120
    Me.ef_evidence_CURP.Value = ""
    Me.ef_evidence_CURP.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_evidence_RFC
    '
    Me.ef_evidence_RFC.DoubleValue = 0.0R
    Me.ef_evidence_RFC.IntegerValue = 0
    Me.ef_evidence_RFC.IsReadOnly = False
    Me.ef_evidence_RFC.Location = New System.Drawing.Point(7, 80)
    Me.ef_evidence_RFC.Name = "ef_evidence_RFC"
    Me.ef_evidence_RFC.PlaceHolder = Nothing
    Me.ef_evidence_RFC.Size = New System.Drawing.Size(285, 24)
    Me.ef_evidence_RFC.SufixText = "Sufix Text"
    Me.ef_evidence_RFC.SufixTextVisible = True
    Me.ef_evidence_RFC.TabIndex = 2
    Me.ef_evidence_RFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_evidence_RFC.TextValue = ""
    Me.ef_evidence_RFC.TextWidth = 120
    Me.ef_evidence_RFC.Value = ""
    Me.ef_evidence_RFC.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_evidence_holder_name
    '
    Me.ef_evidence_holder_name.DoubleValue = 0.0R
    Me.ef_evidence_holder_name.IntegerValue = 0
    Me.ef_evidence_holder_name.IsReadOnly = False
    Me.ef_evidence_holder_name.Location = New System.Drawing.Point(6, 50)
    Me.ef_evidence_holder_name.Name = "ef_evidence_holder_name"
    Me.ef_evidence_holder_name.PlaceHolder = Nothing
    Me.ef_evidence_holder_name.Size = New System.Drawing.Size(343, 24)
    Me.ef_evidence_holder_name.SufixText = "Sufix Text"
    Me.ef_evidence_holder_name.SufixTextVisible = True
    Me.ef_evidence_holder_name.TabIndex = 1
    Me.ef_evidence_holder_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_evidence_holder_name.TextValue = ""
    Me.ef_evidence_holder_name.TextWidth = 120
    Me.ef_evidence_holder_name.Value = ""
    Me.ef_evidence_holder_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'dt_operation_date
    '
    Me.dt_operation_date.Checked = True
    Me.dt_operation_date.IsReadOnly = True
    Me.dt_operation_date.Location = New System.Drawing.Point(7, 20)
    Me.dt_operation_date.Name = "dt_operation_date"
    Me.dt_operation_date.ShowCheckBox = False
    Me.dt_operation_date.ShowUpDown = False
    Me.dt_operation_date.Size = New System.Drawing.Size(262, 24)
    Me.dt_operation_date.SufixText = "Sufix Text"
    Me.dt_operation_date.SufixTextVisible = True
    Me.dt_operation_date.TabIndex = 0
    Me.dt_operation_date.TextWidth = 120
    Me.dt_operation_date.Value = New Date(2012, 2, 9, 16, 37, 32, 958)
    '
    'frm_evidence_viewer
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1204, 530)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
    Me.MaximizeBox = True
    Me.Name = "frm_evidence_viewer"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_evidence_viewer"
    Me.panel_data.ResumeLayout(False)
    Me.gb_operation.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents wb_evidence As System.Windows.Forms.WebBrowser
  Friend WithEvents tab_evidence_viewer As System.Windows.Forms.TabControl
  Friend WithEvents gb_operation As System.Windows.Forms.GroupBox
  Friend WithEvents dt_operation_date As GUI_Controls.uc_date_picker
  Friend WithEvents ef_total_payment As GUI_Controls.uc_entry_field
  Friend WithEvents ef_tax3_amount As GUI_Controls.uc_entry_field
  Friend WithEvents ef_tax2_amount As GUI_Controls.uc_entry_field
  Friend WithEvents ef_tax1_amount As GUI_Controls.uc_entry_field
  Friend WithEvents ef_prize_amount As GUI_Controls.uc_entry_field
  Friend WithEvents ef_evidence_CURP As GUI_Controls.uc_entry_field
  Friend WithEvents ef_evidence_RFC As GUI_Controls.uc_entry_field
  Friend WithEvents ef_evidence_holder_name As GUI_Controls.uc_entry_field
End Class
