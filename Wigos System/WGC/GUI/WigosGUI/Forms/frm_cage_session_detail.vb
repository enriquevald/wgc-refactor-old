'-------------------------------------------------------------------
' Copyright � 2009 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_cage_session_detail.vb
'
' DESCRIPTION:   Show full details for a Cage session 
'
' AUTHOR:        Jes�s �ngel Blanco Blanco
'
' CREATION DATE: 17-DEC-2013
'
' REVISION HISTORY:
'
' Date         Author     Description
' -----------  ---------  ---------------------------------------------------
' 17-DEC-2013  JAB        Initial Draft.
' 09-JAN-2014  JAB        Fixed Bug WIGOSTITO-956: When press update button, caption text is wrong assigned
' 24-JAN-2014  AMF        Fixed Bug WIGOSTITO-1007
' 21-FEB-2014  ICS        Added the audit of renaming cage session and remove unused buttons
' 20-FEB-2014  JAB        Fixed Bug WIGOSTITO-1085: Stock is not updated to tickets tito.
' 06-MAR-2014  JAB        Only show stock when Currency is Accepted or "Total" is more than zero.
' 20-MAR-2014  CCG-JCA    Fixed Bug WIG-753: Update Cage tickets stock for stacker collections.
' 26-MAR-2014  FBA        Fixed Bug WIG-773
' 26-MAR-2014  JAB        Fixed Bug WIG-768
' 07-ABR-2014  JCA        Fixed Bug WIG-798: Value of column "Envio" and column "Recaudaci�n" exchanged
' 09-ABR-2014  LEM        Fixed Bug WIGOSTITO-1197: Some denominations errors 
' 30-APR-2014  CCG        Fixed Bug WIG-872: Hide Quantity columng when GP: Cage.ShowDenominations = 0.
' 05-MAY-2014  CCG        Fixed Bug WIG-886: Hide denomination 1 when GP: Cage.ShowDenominations = 2 if it has not stock.
' 26-MAY-2014  LEM        Fixed Bug WIG-950: Session name is not visible in Excel or Print.
' 05-JUN-2014  JAB        Fixed Bug WIG-984: Session name cannot be saved by user without permissions.
' 05-JUN-2014  JAB        Fixed Bug WIG-987: Changes in session name are not audited.
' 11-JUN-2014  JAB        Fixed Bug WIG-1027: In cage stock, denominations with amount aren't showing if are disabled.
' 28-AUG-2014  LEM & DLL  Fixed Bug WIG-1218: Cage stock mismatch with tips
' 10-OCT-2014  JAB        Fixed Bug WIG-1191: Cage movements canceled should not be counted.
' 23-DEC-2014  SGB        Fixed Bug WIG-1747: No information display of 'CageOperationType.RequestOperation' and change information show of order cage.
' 30-DEC-2014  MPO        Fixed Bug WIG-1902: The "form id" used it is not correct.
' 26-JAN-2015  MPO        Fixed Bug WIG-1973: It should show the "CurrencyISOCode" for movement "CageOperationType.FromTerminal"
' 10-FEB-2015  OPC        Fixed Bug WIG-1902: Added new permission.
' 14-MAY-2015  JBC        Fixed Bug WIG-2348: Error saving gaming day
' 28-MAY-2015  TPF        Fixed Bug WIG-2394: Change vault session date 
' 12-AUG-2015  DCS        Add filter multicheck for filter source / destination
' 04-DEC-2015  JPJ        Bug 7352:Incorrect value shown in the grid
' 14-MAR-2016  FAV        Fixed Bug 10373, 10376, 10365: Fixed value showed in Deposits and Withdrawals
' 26-APR-2016  FOS        Fixed Bug 12340: exchange currency values appear on national currency values
' 18-MAY-2016  RAB        PBI 13541: UNPLANNED - Winions Sprint 25 (Adapt to new chips)
' 24-MAY-2016  DHA        Product Backlog Item 9848: drop box gaming tables
' 26-MAY-2016  RAB        PBI 13541: UNPLANNED - Winions Sprint 25 (Adapt to new chips)
' 09-JUN-2016  RAB        PBI 11755: Tables (Phase 1): Correction errors as a result of the backward compatibility of the chips group
' 28-JUN-2016  DHA        Product Backlog Item 15903: Gaming tables (Fase 5): Drop box collection on Cashier
' 16-SEP-2016  FAV        Bug 17707: Denomination column shows '0.00' always
' 09-FEN-2017  JML        PBI 24378: Reabrir sesiones de caja y mesas de juego: rehacer reapertura y correcci�n de Bugs
'--------------------------------------------------------------------
Option Strict Off
Option Explicit On

Imports WSI.Common
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_Reports
Imports System.Text
Imports System.Data.SqlClient
Imports GUI_CommonOperations

Public Class frm_cage_session_detail
  Inherits frm_base_sel

#Region " Enums "
  Public Enum FORM_MODE
    SHOW_STOCK = 0
    SHOW_DETAIL = 1
  End Enum
#End Region

#Region " Constants "

  ' EXPORT EXCEL
  Private Const EXCEL_COLUMN_CURRENCY As Integer = 4

  ' FORM_MODE[SHOW_DETAIL]-------------------------------------------------
  ' Sql Columns
  Private Const SQL_COLUMN_ISO_CODE As Int32 = 0
  Private Const SQL_COLUMN_CHIP_GROUP As Int32 = 1
  Private Const SQL_COLUMN_CHIP_NAME As Int32 = 2
  Private Const SQL_COLUMN_CHIP_DRAW As Int32 = 3
  Private Const SQL_COLUMN_DENOMINATION As Int32 = 4
  Private Const SQL_COLUMN_QUANTITY As Int32 = 5
  Private Const SQL_COLUMN_TYPE As Int32 = 6
  Private Const SQL_COLUMN_STATUS As Int32 = 7
  Private Const SQL_COLUMN_MOVEMENT_ID As Int32 = 8
  Private Const SQL_COLUMN_OPENINGS As Int32 = 9
  Private Const SQL_COLUMN_DEPOSITS As Int32 = 10
  Private Const SQL_COLUMN_WITHDRAWALS As Int32 = 11
  Private Const SQL_COLUMN_DIFFERENCE As Int32 = 12
  Private Const SQL_COLUMN_LOST_QUADRATURE As Int32 = 13
  Private Const SQL_COLUMN_GAIN_QUADRATURE As Int32 = 14
  Private Const SQL_COLUMN_CLOSING As Int32 = 15
  Private Const SQL_COLUMN_DENOMINATION_REAL As Int32 = 16
  Private Const SQL_COLUMN_CURRENCY_TYPE As Int32 = 17

  Private Const SQL_COLUMNS As Int32 = 18

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Int32 = 0
  Private Const GRID_COLUMN_ISO_CODE As Int32 = 1
  Private Const GRID_COLUMN_CHIP_GROUP As Int32 = 2
  Private Const GRID_COLUMN_CHIP_NAME As Int32 = 3
  Private Const GRID_COLUMN_CHIP_DRAW As Int32 = 4
  Private Const GRID_COLUMN_DENOMINATION As Int32 = 5
  Private Const GRID_COLUMN_CURRENCY_TYPE As Int32 = 6
  Private Const GRID_COLUMN_TYPE_ID As Int32 = 7
  Private Const GRID_COLUMN_TYPE_NAME As Int32 = 8
  Private Const GRID_COLUMN_STATUS_ID As Int32 = 9
  Private Const GRID_COLUMN_STATUS_NAME As Int32 = 10
  Private Const GRID_COLUMN_MOVEMENT_ID As Int32 = 11
  Private Const GRID_COLUMN_OPENINGS As Int32 = 12
  Private Const GRID_COLUMN_DEPOSITS As Int32 = 13
  Private Const GRID_COLUMN_WITHDRAWALS As Int32 = 14
  Private Const GRID_COLUMN_DIFFERENCE As Int32 = 15
  Private Const GRID_COLUMN_LOST_QUADRATURE As Int32 = 16
  Private Const GRID_COLUMN_GAIN_QUADRATURE As Int32 = 17
  Private Const GRID_COLUMN_CLOSING As Int32 = 18

  ' Grid Width
  Private Const WIDTH_GRID_COLUMN_INDEX As Int32 = 200
  Private Const WIDTH_GRID_COLUMN_ISO_CODE As Int32 = 800
  Private Const WIDTH_GRID_COLUMN_CHIP_GROUP As Int32 = 2200
  Private Const WIDTH_GRID_COLUMN_CHIP_NAME As Int32 = 1900
  Private Const WIDTH_GRID_COLUMN_CHIP_DRAW As Int32 = 1900
  Private Const WIDTH_GRID_COLUMN_DENOMINATION As Int32 = 1600
  Private WIDTH_GRID_COLUMN_CURRENCY_TYPE As Int32 = 2700
  Private Const WIDTH_GRID_COLUMN_TYPE_ID As Int32 = 0
  Private Const WIDTH_GRID_COLUMN_TYPE_NAME As Int32 = 3390
  Private Const WIDTH_GRID_COLUMN_STATUS_ID As Int32 = 0
  Private Const WIDTH_GRID_COLUMN_STATUS_NAME As Int32 = 1360
#If DEBUG Then
  Private Const WIDTH_GRID_COLUMN_MOVEMENT_ID As Int32 = 800
#Else
  Private Const WIDTH_GRID_COLUMN_MOVEMENT_ID As Int32 = 0
#End If
  Private Const WIDTH_GRID_COLUMN_OPENINGS As Int32 = 0
  Private Const WIDTH_GRID_COLUMN_DEPOSITS As Int32 = 1650
  Private Const WIDTH_GRID_COLUMN_WITHDRAWALS As Int32 = 1650
  Private Const WIDTH_GRID_COLUMN_DIFFERENCE As Int32 = 1650
  Private Const WIDTH_GRID_COLUMN_LOST_QUADRATURE As Int32 = 1650
  Private Const WIDTH_GRID_COLUMN_GAIN_QUADRATURE As Int32 = 1700
  Private Const WIDTH_GRID_COLUMN_CLOSING As Int32 = 0

  Private Const GRID_COLUMNS As Int32 = 19
  Private Const GRID_HEADER_ROWS As Int32 = 2

  ' Totals grid columns
  Private Const TB_COLUMN_ISO_CODE As Int32 = 0
  Private Const TB_COLUMN_CHIP_GROUP As Int32 = 1
  Private Const TB_COLUMN_CHIP_NAME As Int32 = 2
  Private Const TB_COLUMN_CHIP_DRAW As Int32 = 3
  Private Const TB_COLUMN_DENOMINATION As Int32 = 4
  Private Const TB_COLUMN_DENOMINATION_WITH_SIMBOL As Int32 = 5
  Private Const TB_COLUMN_SUBTOTAL_TOTAL As Int32 = 6
  Private Const TB_COLUMN_OPENINGS As Int32 = 7
  Private Const TB_COLUMN_DEPOSITS As Int32 = 8
  Private Const TB_COLUMN_WITHDRAWALS As Int32 = 9
  Private Const TB_COLUMN_DIFFERENCE As Int32 = 10
  Private Const TB_COLUMN_LOST_QUADRATURE As Int32 = 11
  Private Const TB_COLUMN_GAIN_QUADRATURE As Int32 = 12
  Private Const TB_COLUMN_CLOSING As Int32 = 13
  Private Const TB_COLUMN_DENOMINATION_REAL As Int32 = 14
  Private Const TB_COLUMN_CURRENCY_TYPE As Int32 = 15

  Private Const TB_COLUMNS As Int32 = 15

  ' FORM_MODE[SHOW_STOCK]-------------------------------------------------
  ' Sql Columns
  Private Const SQL_COLUMN_ISO_CODE_STOCK As Int32 = 0
  Private Const SQL_COLUMN_DENOMINATION_STOCK As Int32 = 1
  Private Const SQL_COLUMN_CURRENCY_TYPE_STOCK As Int32 = 2
  Private Const SQL_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK As Int32 = 9
  Private Const SQL_COLUMN_QUANTITY_STOCK As Int32 = 10
  Private Const SQL_COLUMN_TOTAL_STOCK As Int32 = 11

  Private Const SQL_COLUMNS_STOCK As Int32 = 5

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX_STOCK As Int32 = 0
  Private Const GRID_COLUMN_ISO_CODE_STOCK As Int32 = 1
  Private Const GRID_COLUMN_DENOMINATION_STOCK As Int32 = 2
  Private Const GRID_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK As Int32 = 3
  Private Const GRID_COLUMN_CURRENCY_TYPE_STOCK As Int32 = 4
  Private Const GRID_COLUMN_QUANTITY_STOCK As Int32 = 5
  Private Const GRID_COLUMN_TOTAL_STOCK As Int32 = 6

  ' Grid Width
  Private Const WIDTH_GRID_COLUMN_INDEX_STOCK As Int32 = 0
  Private WIDTH_GRID_COLUMN_ISO_CODE_STOCK As Int32 = 1000
  Private Const WIDTH_GRID_COLUMN_DENOMINATION_STOCK As Int32 = 0
  Private WIDTH_GRID_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK As Int32 = 2500
  Private WIDTH_GRID_COLUMN_CURRENCY_TYPE_STOCK As Int32 = 1000
  Private WIDTH_GRID_COLUMN_QUANTITY_STOCK As Int32 = 1800
  Private WIDTH_GRID_COLUMN_TOTAL_STOCK As Int32 = 1800

  Private Const GRID_COLUMNS_STOCK As Int32 = 7
  Private Const GRID_HEADER_ROWS_STOCK As Int32 = 1

  ' Totals grid columns
  Private Const TB_COLUMN_ISO_CODE_STOCK As Int32 = 0
  Private Const TB_COLUMN_DENOMINATION_STOCK As Int32 = 1
  Private Const TB_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK As Int32 = 2
  Private Const TB_COLUMN_GRID_TYPE As Int32 = 3
  Private Const TB_COLUMN_QUANTITY_STOCK As Int32 = 4
  Private Const TB_COLUMN_TOTAL_STOCK As Int32 = 5

  Private Const TB_COLUMNS_STOCK As Int32 = 6

  Private Const FORM_WIDTH_STOCK As Int32 = 682
  Private Const FORM_HEIGHT_STOCK As Int32 = 647

  Private Const CHECKED_LIST_ID_CASHIER As Int32 = 1
  Private Const CHECKED_LIST_ID_TERMINAL As Int32 = 2
  Private Const CHECKED_LIST_ID_GAMING_TABLE As Int32 = 3
  Private Const CHECKED_LIST_ID_CUSTOM As Int32 = 4
  Private Const CHECKED_LIST_ID_CAGE As Int32 = 5

#End Region

#Region " Members "
  Private m_session_detail As cls_cage_session_detail
  Private m_form_mode As Int32
  Private m_dt_totals As DataTable
  Private m_rw_current_total As DataRow
  Private m_rw_current_subtotal As DataRow
  Private m_rw_current_total_stock As DataRow
  Private m_total_rw_current_chip_group As String

  Private m_theoric_stock As CLASS_CAGE_CONTROL

  ' To custom query
  Private m_grid_overflow As Boolean
  Private m_grid_refresh_step As Integer
  Private m_grid_refresh_pending_rows As Integer

  Private m_row_index As Int64

  Private m_show_only_stock As Boolean = False
  Private m_first_time As Boolean = True
  Private m_show_message_when_closing As Boolean = True
  Private m_cage_show_denominations As Integer = 1 ' = Show all denominations
  Private m_selected_source_destinations As String

#End Region

#Region " Builder "

  Public Sub New(ByVal FormId As ENUM_FORM)
    MyBase.New()

    Me.FormId = FormId

    Call MyBase.GUI_SetFormId()

    InitializeComponent()
  End Sub

  Public Sub New()
    MyBase.New()

    Me.FormId = ENUM_FORM.FORM_CAGE_STOCK

    Call MyBase.GUI_SetFormId()

    InitializeComponent()
  End Sub

#End Region

#Region " Public Properties "

  Public Property SessionDetail() As cls_cage_session_detail
    Get
      Return m_session_detail
    End Get
    Set(ByVal value As cls_cage_session_detail)
      m_session_detail = value
    End Set
  End Property ' SessionDetail

#End Region   ' Public Properties

#Region " Publics "

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window, ByVal Item As Object, Optional ByVal ShowOnlyStock As Boolean = False)

    Me.SessionDetail = Item
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.m_show_only_stock = ShowOnlyStock
    If ShowOnlyStock Then
      Me.Width = FORM_WIDTH_STOCK
    End If
    Me.Height = FORM_HEIGHT_STOCK
    Me.Display(False)

  End Sub

#End Region

#Region " Private "

  ' PURPOSE: Set the labels' information 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub SetLabelsText()

    ' Data Session
    Me.ef_cg_session_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(253)        ' Nombre de la sesi�n
    Me.ef_cg_session_name.IsReadOnly = Me.m_show_only_stock
    Me.ef_cg_session_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, 50)

    Me.ef_cg_cage_opening_user.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4450)   ' Usuario Apertura
    Me.ef_cg_cage_opening_user.IsReadOnly = True
    Me.ef_cg_cage_opening_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4452)   ' Fecha Apertura
    Me.ef_cg_cage_opening_date.IsReadOnly = True
    Me.ef_cg_cage_closing_user.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4451)   ' Usuario Cierre
    Me.ef_cg_cage_closing_user.IsReadOnly = True
    Me.ef_cg_cage_closing_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4453)   ' Fecha Cierre
    Me.ef_cg_cage_closing_date.IsReadOnly = True
    Me.ef_cg_cage_session_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3456) ' Estado
    Me.ef_cg_cage_session_status.IsReadOnly = True
    Me.dtp_working_day.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5723)             ' Jornada

    If m_session_detail.m_session_id <> -1 Then
      m_session_detail.m_session_name = m_session_detail.GetSessionName()
    Else
      m_session_detail.m_session_name = AUDIT_NONE_STRING
    End If
    Me.ef_cg_session_name.Value = m_session_detail.m_session_name
    Me.ef_cg_cage_opening_user.Value = m_session_detail.m_session_opening_user
    Me.ef_cg_cage_opening_date.Value = m_session_detail.m_session_opening_date
    Me.ef_cg_cage_closing_user.Value = m_session_detail.m_session_closing_user
    Me.ef_cg_cage_closing_date.Value = m_session_detail.m_session_closing_date
    If m_session_detail.m_session_working_day IsNot Nothing Then
      Me.dtp_working_day.Value = m_session_detail.m_session_working_day
    End If

    Select Case m_session_detail.m_session_status
      Case WSI.Common.CASHIER_SESSION_STATUS.OPEN
        Me.ef_cg_cage_session_status.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4349) ' Abierta
        '        Me.ef_cg_cage_session_status.LabelForeColor = Color.Red
      Case WSI.Common.CASHIER_SESSION_STATUS.CLOSED
        Me.ef_cg_cage_session_status.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4350) ' Cerrada
        '        Me.ef_cg_cage_session_status.LabelForeColor = Color.Green
      Case Else
        Me.ef_cg_cage_session_status.Value = AUDIT_NONE_STRING
    End Select

  End Sub ' SetLabelsText

  ' PURPOSE: Fill and set the uc_checked_list control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub SetCheckedList()

    Dim _dt_cage_concepts As DataTable

    _dt_cage_concepts = New DataTable

    Me.uc_checked_list.GroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2629) ' Movements
    Me.uc_checked_list.ColumnWidth(uc_checked_list.GRID_COLUMN_DESC, 4250)

    Me.uc_checked_list.Clear()
    Me.uc_checked_list.ReDraw(False)

    Me.uc_checked_list.Width = 300
    Me.uc_checked_list.btn_check_all.Width = 100
    Me.uc_checked_list.btn_uncheck_all.Width = 115

    ' Cashier
    Me.uc_checked_list.Add(CHECKED_LIST_ID_CASHIER, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1181))

    ' Terminal
    If WSI.Common.Misc.IsNoteAcceptorEnabled() Or TITO.Utils.IsTitoMode() Then
      Me.uc_checked_list.Add(CHECKED_LIST_ID_TERMINAL, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1968))
    End If

    ' Gaming Tables
    If GamingTableBusinessLogic.IsGamingTablesEnabled() Then
      Me.uc_checked_list.Add(CHECKED_LIST_ID_GAMING_TABLE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3416))
    End If

    ' Custom
    Using _db_trx As New DB_TRX()
      If WSI.Common.CageMeters.GetCageSourceTargetConcepts(_dt_cage_concepts, _db_trx.SqlTransaction) Then
        If _dt_cage_concepts.Select("CSTC_SOURCE_TARGET_ID >= 1000").LongLength > 0 Then
          Me.uc_checked_list.Add(CHECKED_LIST_ID_CUSTOM, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3393))
        End If
      End If
    End Using

    ' Cage
    Me.uc_checked_list.Add(CHECKED_LIST_ID_CAGE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2876))

    If Me.uc_checked_list.Count() > 0 Then
      Me.uc_checked_list.CurrentRow(0)
      Me.uc_checked_list.SetDefaultValue(True)
    End If

    Me.uc_checked_list.ReDraw(True)
    Me.uc_checked_list.ResizeGrid()

  End Sub ' SetCheckedList

  ' PURPOSE: Do String for Query with ids selected in uuser control cehck list
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - String QuerySelectedListIds
  '
  Private Function GetListIdsFromCheckedList() As String
    Dim _str_query_selected_list_ids As String
    Dim _selected_ids As Integer()

    _str_query_selected_list_ids = String.Empty

    _selected_ids = Me.uc_checked_list.SelectedIndexes()

    For Each _id As Integer In _selected_ids
      Select Case _id
        Case CHECKED_LIST_ID_CASHIER
          _str_query_selected_list_ids = _str_query_selected_list_ids & Cage.CageOperationType.FromCashier & ", " & Cage.CageOperationType.FromCashierClosing & ", " & Cage.CageOperationType.ToCashier & ", " & Cage.CageOperationType.RequestOperation & ", "
        Case CHECKED_LIST_ID_TERMINAL
          _str_query_selected_list_ids = _str_query_selected_list_ids & Cage.CageOperationType.FromTerminal & ", " & Cage.CageOperationType.ToTerminal & ", "
        Case CHECKED_LIST_ID_GAMING_TABLE
          _str_query_selected_list_ids = _str_query_selected_list_ids & Cage.CageOperationType.FromGamingTable & ", " & Cage.CageOperationType.FromGamingTableClosing & ", " & Cage.CageOperationType.ToGamingTable & ", " & Cage.CageOperationType.FromGamingTableDropbox & ", " & Cage.CageOperationType.FromGamingTableDropboxWithExpected & ", "
        Case CHECKED_LIST_ID_CUSTOM
          _str_query_selected_list_ids = _str_query_selected_list_ids & Cage.CageOperationType.FromCustom & ", " & Cage.CageOperationType.ToCustom & ", "
        Case CHECKED_LIST_ID_CAGE
          _str_query_selected_list_ids = _str_query_selected_list_ids & Cage.CageOperationType.OpenCage & ", " & Cage.CageOperationType.CloseCage & ", " & Cage.CageOperationType.GainQuadrature & ", " & Cage.CageOperationType.LostQuadrature & ", "
      End Select
    Next

    If _str_query_selected_list_ids.EndsWith(", ") Then
      _str_query_selected_list_ids = _str_query_selected_list_ids.Remove(_str_query_selected_list_ids.Length - 2)
    End If

    ' Update value for report params (auitory and excel)
    m_selected_source_destinations = Me.uc_checked_list.SelectedValuesList()

    Return _str_query_selected_list_ids

  End Function ' GetListIdsFromCheckedList

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub GUI_StyleSheet()
    Dim _offset_with As Integer = WIDTH_GRID_COLUMN_QUANTITY_STOCK / 3

    Me.m_cage_show_denominations = GeneralParam.GetInt32("Cage", "ShowDenominations", 1)

    With Me.Grid

      Select Case m_form_mode
        Case FORM_MODE.SHOW_STOCK

          If Me.m_cage_show_denominations = Cage.ShowDenominationsMode.HideAllDenominations Then
            WIDTH_GRID_COLUMN_QUANTITY_STOCK = 0
            WIDTH_GRID_COLUMN_ISO_CODE_STOCK += _offset_with
            WIDTH_GRID_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK += _offset_with
            WIDTH_GRID_COLUMN_TOTAL_STOCK += _offset_with
          End If

          Call .Init(GRID_COLUMNS_STOCK, GRID_HEADER_ROWS_STOCK)

          ' Index
          .Column(GRID_COLUMN_INDEX_STOCK).Header(0).Text = " "
          .Column(GRID_COLUMN_INDEX_STOCK).Width = WIDTH_GRID_COLUMN_INDEX_STOCK
          .Column(GRID_COLUMN_INDEX_STOCK).HighLightWhenSelected = False
          .Column(GRID_COLUMN_INDEX_STOCK).IsColumnPrintable = False

          ' Iso Code
          .Column(GRID_COLUMN_ISO_CODE_STOCK).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4356)  ' Divisa
          .Column(GRID_COLUMN_ISO_CODE_STOCK).Width = WIDTH_GRID_COLUMN_ISO_CODE_STOCK
          .Column(GRID_COLUMN_ISO_CODE_STOCK).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
          .Column(GRID_COLUMN_ISO_CODE_STOCK).IsMerged = True

          ' Denomination [number]
          .Column(GRID_COLUMN_DENOMINATION_STOCK).Header(0).Text = " "  ' Denominaciones
          .Column(GRID_COLUMN_DENOMINATION_STOCK).Width = WIDTH_GRID_COLUMN_DENOMINATION_STOCK
          .Column(GRID_COLUMN_DENOMINATION_STOCK).IsMerged = True
          .Column(GRID_COLUMN_DENOMINATION_STOCK).IsColumnPrintable = False

          ' Denomination [number or text]
          .Column(GRID_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4357)  ' Denominaciones
          .Column(GRID_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK).Width = WIDTH_GRID_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK
          .Column(GRID_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          ' Currency type
          .Column(GRID_COLUMN_CURRENCY_TYPE_STOCK).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5678) ' Tipo
          .Column(GRID_COLUMN_CURRENCY_TYPE_STOCK).Width = WIDTH_GRID_COLUMN_CURRENCY_TYPE_STOCK
          .Column(GRID_COLUMN_CURRENCY_TYPE_STOCK).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

          ' Quantity
          .Column(GRID_COLUMN_QUANTITY_STOCK).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2265)    ' Quantity
          .Column(GRID_COLUMN_QUANTITY_STOCK).Width = WIDTH_GRID_COLUMN_QUANTITY_STOCK
          .Column(GRID_COLUMN_QUANTITY_STOCK).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          ' Total
          .Column(GRID_COLUMN_TOTAL_STOCK).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1340) ' Total
          .Column(GRID_COLUMN_TOTAL_STOCK).Width = WIDTH_GRID_COLUMN_TOTAL_STOCK
          .Column(GRID_COLUMN_TOTAL_STOCK).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          Call HideShowHeader(False)

        Case Else

          Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

          ' Index
          .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
          .Column(GRID_COLUMN_INDEX).Header(1).Text = " "
          .Column(GRID_COLUMN_INDEX).Width = WIDTH_GRID_COLUMN_INDEX
          .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
          .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

          ' Iso Code
          .Column(GRID_COLUMN_ISO_CODE).Header(0).Text = ""
          .Column(GRID_COLUMN_ISO_CODE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4356)  ' Divisa
          .Column(GRID_COLUMN_ISO_CODE).Width = WIDTH_GRID_COLUMN_ISO_CODE
          .Column(GRID_COLUMN_ISO_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
          .Column(GRID_COLUMN_ISO_CODE).IsMerged = True

          ' Chip group
          .Column(GRID_COLUMN_CHIP_GROUP).Header(0).Text = ""
          .Column(GRID_COLUMN_CHIP_GROUP).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2420)  ' Grupo
          .Column(GRID_COLUMN_CHIP_GROUP).Width = WIDTH_GRID_COLUMN_CHIP_GROUP
          .Column(GRID_COLUMN_CHIP_GROUP).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
          .Column(GRID_COLUMN_CHIP_GROUP).IsMerged = True

          ' Chip name
          .Column(GRID_COLUMN_CHIP_NAME).Header(0).Text = ""
          .Column(GRID_COLUMN_CHIP_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4979)  ' Nombre
          .Column(GRID_COLUMN_CHIP_NAME).Width = WIDTH_GRID_COLUMN_CHIP_NAME
          .Column(GRID_COLUMN_CHIP_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

          ' Chip draw
          .Column(GRID_COLUMN_CHIP_DRAW).Header(0).Text = ""
          .Column(GRID_COLUMN_CHIP_DRAW).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6924)  ' Dibujo
          .Column(GRID_COLUMN_CHIP_DRAW).Width = WIDTH_GRID_COLUMN_CHIP_DRAW
          .Column(GRID_COLUMN_CHIP_DRAW).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

          ' Denomination
          .Column(GRID_COLUMN_DENOMINATION).Header(0).Text = ""
          .Column(GRID_COLUMN_DENOMINATION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4357)  ' Denominaciones
          .Column(GRID_COLUMN_DENOMINATION).Width = WIDTH_GRID_COLUMN_DENOMINATION
          .Column(GRID_COLUMN_DENOMINATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          ' Currency type
          .Column(GRID_COLUMN_CURRENCY_TYPE).Header(0).Text = ""
          .Column(GRID_COLUMN_CURRENCY_TYPE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5678) ' Tipo
          .Column(GRID_COLUMN_CURRENCY_TYPE).Width = WIDTH_GRID_COLUMN_CURRENCY_TYPE
          .Column(GRID_COLUMN_CURRENCY_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

          ' Type Id
          .Column(GRID_COLUMN_TYPE_ID).Header(0).Text = ""
          .Column(GRID_COLUMN_TYPE_ID).Header(1).Text = ""
          .Column(GRID_COLUMN_TYPE_ID).Width = WIDTH_GRID_COLUMN_TYPE_ID
          .Column(GRID_COLUMN_TYPE_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
          .Column(GRID_COLUMN_TYPE_ID).IsColumnPrintable = False

          ' Operation name
          .Column(GRID_COLUMN_TYPE_NAME).Header(0).Text = ""
          .Column(GRID_COLUMN_TYPE_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3458)
          .Column(GRID_COLUMN_TYPE_NAME).Width = WIDTH_GRID_COLUMN_TYPE_NAME
          .Column(GRID_COLUMN_TYPE_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

          ' Status id
          .Column(GRID_COLUMN_STATUS_ID).Header(0).Text = ""
          .Column(GRID_COLUMN_STATUS_ID).Header(1).Text = ""
          .Column(GRID_COLUMN_STATUS_ID).Width = WIDTH_GRID_COLUMN_STATUS_ID
          .Column(GRID_COLUMN_STATUS_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
          .Column(GRID_COLUMN_STATUS_ID).IsColumnPrintable = False

          ' Status name
          .Column(GRID_COLUMN_STATUS_NAME).Header(0).Text = ""
          .Column(GRID_COLUMN_STATUS_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3456)
          .Column(GRID_COLUMN_STATUS_NAME).Width = WIDTH_GRID_COLUMN_STATUS_NAME
          .Column(GRID_COLUMN_STATUS_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

          ' Movement Id
          .Column(GRID_COLUMN_MOVEMENT_ID).Header(0).Text = ""
          .Column(GRID_COLUMN_MOVEMENT_ID).Header(1).Text = ""
          .Column(GRID_COLUMN_MOVEMENT_ID).Width = WIDTH_GRID_COLUMN_MOVEMENT_ID
          .Column(GRID_COLUMN_MOVEMENT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
          .Column(GRID_COLUMN_MOVEMENT_ID).IsColumnPrintable = False

          ' Openings
          .Column(GRID_COLUMN_OPENINGS).Header(0).Text = ""
          .Column(GRID_COLUMN_OPENINGS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3462)  ' Apertura
          .Column(GRID_COLUMN_OPENINGS).Width = WIDTH_GRID_COLUMN_OPENINGS
          .Column(GRID_COLUMN_OPENINGS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          ' Deposits
          .Column(GRID_COLUMN_DEPOSITS).Header(0).Text = ""
          .Column(GRID_COLUMN_DEPOSITS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4359) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(3470)   ' Recaudaci�n (R)
          .Column(GRID_COLUMN_DEPOSITS).Width = WIDTH_GRID_COLUMN_DEPOSITS
          .Column(GRID_COLUMN_DEPOSITS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          ' Withdrawls
          .Column(GRID_COLUMN_WITHDRAWALS).Header(0).Text = ""
          .Column(GRID_COLUMN_WITHDRAWALS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4358) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(3469)     ' Env�o (E)
          .Column(GRID_COLUMN_WITHDRAWALS).Width = WIDTH_GRID_COLUMN_WITHDRAWALS
          .Column(GRID_COLUMN_WITHDRAWALS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          ' Difference
          .Column(GRID_COLUMN_DIFFERENCE).Header(0).Text = ""
          .Column(GRID_COLUMN_DIFFERENCE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3468)                                                       ' Total(E-R)
          .Column(GRID_COLUMN_DIFFERENCE).Width = WIDTH_GRID_COLUMN_DIFFERENCE
          .Column(GRID_COLUMN_DIFFERENCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          ' Gain quadrature
          .Column(GRID_COLUMN_GAIN_QUADRATURE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4334)   ' Ajuste
          .Column(GRID_COLUMN_GAIN_QUADRATURE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4335)   ' Positivo                                                     ' Total(E-R)
          .Column(GRID_COLUMN_GAIN_QUADRATURE).Width = WIDTH_GRID_COLUMN_GAIN_QUADRATURE
          .Column(GRID_COLUMN_GAIN_QUADRATURE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          ' Lost quadrature
          .Column(GRID_COLUMN_LOST_QUADRATURE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4334)   ' Ajuste
          .Column(GRID_COLUMN_LOST_QUADRATURE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4336)   ' Negativo                                                     ' Total(E-R)
          .Column(GRID_COLUMN_LOST_QUADRATURE).Width = WIDTH_GRID_COLUMN_LOST_QUADRATURE
          .Column(GRID_COLUMN_LOST_QUADRATURE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          ' Closing
          .Column(GRID_COLUMN_CLOSING).Header(0).Text = ""
          .Column(GRID_COLUMN_CLOSING).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3463)           ' Cierre                                                     ' Total(E-R)
          .Column(GRID_COLUMN_CLOSING).Width = WIDTH_GRID_COLUMN_CLOSING
          .Column(GRID_COLUMN_CLOSING).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          Call HideShowHeader(True)

      End Select

    End With
  End Sub ' GUI_StyleSheet

  ' PURPOSE: Hides or shows header fields
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub HideShowHeader(ByVal Visible As Boolean)
    Me.ef_cg_cage_closing_date.Visible = Visible
    Me.ef_cg_cage_closing_user.Visible = Visible
    Me.ef_cg_cage_opening_date.Visible = Visible
    Me.ef_cg_cage_opening_user.Visible = Visible
    Me.ef_cg_cage_session_status.Visible = Visible
    Me.ef_cg_session_name.IsReadOnly = Not Visible
    Me.ef_cg_session_name.Visible = Visible
    Me.dtp_working_day.Visible = Visible
    Me.uc_checked_list.Visible = Visible
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = Visible
    If Not Visible Then
      Me.panel_filter.Height = 40
      'Else
      '  Me.panel_filter.Height = 112
    End If
  End Sub ' HideShowHeader

  ' PURPOSE: Initialize totals' table
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub CreateTotalsTable(ByVal FrmMode As FORM_MODE)
    Dim _col As DataColumn

    m_dt_totals = New DataTable()

    Select Case FrmMode
      Case FORM_MODE.SHOW_STOCK
        _col = m_dt_totals.Columns.Add()
        _col.DataType = System.Type.GetType("System.String")
        _col.AllowDBNull = True
        _col.Caption = "TB_COLUMN_ISO_CODE_STOCK"
        _col.ColumnName = "TB_COLUMN_ISO_CODE_STOCK"

        _col = m_dt_totals.Columns.Add()
        _col.DataType = System.Type.GetType("System.Decimal")
        _col.AllowDBNull = True
        _col.Caption = "TB_COLUMN_DENOMINATION_STOCK"
        _col.ColumnName = "TB_COLUMN_DENOMINATION_STOCK"

        _col = m_dt_totals.Columns.Add()
        _col.DataType = System.Type.GetType("System.String")
        _col.AllowDBNull = True
        _col.Caption = "TB_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK"
        _col.ColumnName = "TB_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK"

        _col = m_dt_totals.Columns.Add()
        _col.DataType = System.Type.GetType("System.String")
        _col.AllowDBNull = True
        _col.Caption = "TB_COLUMN_GRID_TYPE"
        _col.ColumnName = "TB_COLUMN_GRID_TYPE"

        _col = m_dt_totals.Columns.Add()
        _col.DataType = System.Type.GetType("System.Decimal")
        _col.AllowDBNull = True
        _col.Caption = "TB_COLUMN_QUANTITY_STOCK"
        _col.ColumnName = "TB_COLUMN_QUANTITY_STOCK"

        _col = m_dt_totals.Columns.Add()
        _col.DataType = System.Type.GetType("System.Decimal")
        _col.AllowDBNull = True
        _col.Caption = "TB_COLUMN_TOTAL_STOCK"
        _col.ColumnName = "TB_COLUMN_TOTAL_STOCK"

      Case Else ' FORM_MODE.SHOW_DETAILS and ALLs

        _col = m_dt_totals.Columns.Add()
        _col.DataType = System.Type.GetType("System.String")
        _col.AllowDBNull = True
        _col.Caption = "TB_COLUMN_ISO_CODE"
        _col.ColumnName = "TB_COLUMN_ISO_CODE"

        _col = m_dt_totals.Columns.Add()
        _col.DataType = System.Type.GetType("System.String")
        _col.AllowDBNull = True
        _col.Caption = "TB_COLUMN_CHIP_GROUP"
        _col.ColumnName = "TB_COLUMN_CHIP_GROUP"

        _col = m_dt_totals.Columns.Add()
        _col.DataType = System.Type.GetType("System.String")
        _col.AllowDBNull = True
        _col.Caption = "TB_COLUMN_CHIP_NAME"
        _col.ColumnName = "TB_COLUMN_CHIP_NAME"

        _col = m_dt_totals.Columns.Add()
        _col.DataType = System.Type.GetType("System.String")
        _col.AllowDBNull = True
        _col.Caption = "TB_COLUMN_CHIP_DRAW"
        _col.ColumnName = "TB_COLUMN_CHIP_DRAW"

        _col = m_dt_totals.Columns.Add()
        _col.DataType = System.Type.GetType("System.Decimal")
        _col.AllowDBNull = True
        _col.Caption = "TB_COLUMN_DENOMINATION"
        _col.ColumnName = "TB_COLUMN_DENOMINATION"

        _col = m_dt_totals.Columns.Add()
        _col.DataType = System.Type.GetType("System.String")
        _col.AllowDBNull = True
        _col.Caption = "TB_COLUMN_DENOMINATION_WITH_SIMBOL"
        _col.ColumnName = "TB_COLUMN_DENOMINATION_WITH_SIMBOL"

        _col = m_dt_totals.Columns.Add()
        _col.DataType = System.Type.GetType("System.String")
        _col.AllowDBNull = True
        _col.Caption = "TB_COLUMN_SUBTOTAL_TOTAL"
        _col.ColumnName = "TB_COLUMN_SUBTOTAL_TOTAL"

        _col = m_dt_totals.Columns.Add()
        _col.DataType = System.Type.GetType("System.Decimal")
        _col.AllowDBNull = True
        _col.Caption = "TB_COLUMN_OPENINGS"
        _col.ColumnName = "TB_COLUMN_OPENINGS"

        _col = m_dt_totals.Columns.Add()
        _col.DataType = System.Type.GetType("System.Decimal")
        _col.AllowDBNull = True
        _col.Caption = "TB_COLUMN_DEPOSITS"
        _col.ColumnName = "TB_COLUMN_DEPOSITS"

        _col = m_dt_totals.Columns.Add()
        _col.DataType = System.Type.GetType("System.Decimal")
        _col.AllowDBNull = True
        _col.Caption = "TB_COLUMN_WITHDRAWALS"
        _col.ColumnName = "TB_COLUMN_WITHDRAWALS"

        _col = m_dt_totals.Columns.Add()
        _col.DataType = System.Type.GetType("System.Decimal")
        _col.AllowDBNull = True
        _col.Caption = "TB_COLUMN_DIFFERENCE"
        _col.ColumnName = "TB_COLUMN_DIFFERENCE"

        _col = m_dt_totals.Columns.Add()
        _col.DataType = System.Type.GetType("System.Decimal")
        _col.AllowDBNull = True
        _col.Caption = "TB_COLUMN_LOST_QUADRATURE"
        _col.ColumnName = "TB_COLUMN_LOST_QUADRATURE"

        _col = m_dt_totals.Columns.Add()
        _col.DataType = System.Type.GetType("System.Decimal")
        _col.AllowDBNull = True
        _col.Caption = "TB_COLUMN_GAIN_QUADRATURE"
        _col.ColumnName = "TB_COLUMN_GAIN_QUADRATURE"

        _col = m_dt_totals.Columns.Add()
        _col.DataType = System.Type.GetType("System.Decimal")
        _col.AllowDBNull = True
        _col.Caption = "TB_COLUMN_CLOSING"
        _col.ColumnName = "TB_COLUMN_CLOSING"

        _col = m_dt_totals.Columns.Add()
        _col.DataType = System.Type.GetType("System.Decimal")
        _col.AllowDBNull = True
        _col.Caption = "TB_COLUMN_DENOMINATION_REAL"
        _col.ColumnName = "TB_COLUMN_DENOMINATION_REAL"

        _col = m_dt_totals.Columns.Add()
        _col.DataType = System.Type.GetType("System.Int32")
        _col.AllowDBNull = True
        _col.Caption = "TB_COLUMN_CAGE_CURRENCY_TYPE"
        _col.ColumnName = "TB_COLUMN_CAGE_CURRENCY_TYPE"

    End Select
  End Sub ' CreateTotalsTable

  ' PURPOSE: Refresh stock grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub CheckRefreshGrid()

    If m_grid_refresh_pending_rows > m_grid_refresh_step Then

      Me.Grid.Redraw = True
      Call Application.DoEvents()

      ' DoEvents run each second.
      If Not GUI_DoEvents(Me.Grid) Then
        Exit Sub
      End If

      Windows.Forms.Cursor.Current = Cursors.WaitCursor
      Me.Grid.Redraw = False

      m_grid_refresh_pending_rows = 0

    End If

    If Me.Grid.NumRows >= GUI_MaxRows() Then
      m_grid_overflow = True
    End If

  End Sub   ' CheckRefreshGrid

  ' PURPOSE: Insert a totals' row
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub InsertTotals()
    Dim _row As DataRow
    Dim _idx As Int32
    Dim _iso_code As String
    Dim _special_denomination As String
    Dim _denomination As String
    Dim _currency_type As String

    Select Case m_form_mode
      Case FORM_MODE.SHOW_STOCK

        m_dt_totals.Rows.Add(m_rw_current_total_stock)

        For Each _row In m_dt_totals.Rows
          If Not IsNothing(_row.Item(TB_COLUMN_ISO_CODE_STOCK)) AndAlso Not IsDBNull(_row.Item(TB_COLUMN_ISO_CODE_STOCK)) Then
            _idx = Me.Grid.NumRows()
            Me.Grid.AddRow()
            _iso_code = _row.Item(TB_COLUMN_ISO_CODE_STOCK)
            Me.Grid.Cell(_idx, GRID_COLUMN_ISO_CODE_STOCK).Value = _row.Item(TB_COLUMN_ISO_CODE_STOCK)
            Me.Grid.Cell(_idx, GRID_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK).Value = _row.Item(TB_COLUMN_GRID_TYPE)
            If _row.IsNull(TB_COLUMN_QUANTITY_STOCK) OrElse _row.Item(TB_COLUMN_QUANTITY_STOCK) = 0 Then
              Me.Grid.Cell(_idx, GRID_COLUMN_QUANTITY_STOCK).Value = ""
            Else
              Me.Grid.Cell(_idx, GRID_COLUMN_QUANTITY_STOCK).Value = _row.Item(TB_COLUMN_QUANTITY_STOCK) 'SessionDetail.FormatAmount(_row.Item(TB_COLUMN_QUANTITY_STOCK), _iso_code)
            End If
            Me.Grid.Cell(_idx, GRID_COLUMN_TOTAL_STOCK).Value = SessionDetail.FormatAmount(_row.Item(TB_COLUMN_TOTAL_STOCK), _iso_code)

            Me.Grid.Row(_idx).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00) ' Total:
          End If
        Next

      Case Else ' FORM_MODE.SHOW_DETAIL and ALL

        m_dt_totals.Rows.Add(m_rw_current_subtotal)
        m_dt_totals.Rows.Add(m_rw_current_total)

        For Each _row In m_dt_totals.Rows
          _idx = Me.Grid.NumRows()
          Me.Grid.AddRow()
          _iso_code = _row.Item(TB_COLUMN_ISO_CODE)

          If (IsDBNull(_row.Item(TB_COLUMN_CURRENCY_TYPE))) Then
            Me.Grid.Cell(_idx, GRID_COLUMN_ISO_CODE).Value = ""
          Else
            If _row.Item(TB_COLUMN_ISO_CODE) = Cage.CHIPS_COLOR Then
              Me.Grid.Cell(_idx, GRID_COLUMN_ISO_CODE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7286) ' Color
            ElseIf _row.Item(TB_COLUMN_ISO_CODE) = Cage.CHIPS_ISO_CODE Then
              Me.Grid.Cell(_idx, GRID_COLUMN_ISO_CODE).Value = CurrencyExchange.GetNationalCurrency()
            Else
              Me.Grid.Cell(_idx, GRID_COLUMN_ISO_CODE).Value = _row.Item(TB_COLUMN_ISO_CODE)
            End If
          End If

          If (IsDBNull(_row.Item(TB_COLUMN_CHIP_GROUP))) Then
            Me.Grid.Cell(_idx, GRID_COLUMN_CHIP_GROUP).Value = ""
          Else
            Me.Grid.Cell(_idx, GRID_COLUMN_CHIP_GROUP).Value = _row.Item(TB_COLUMN_CHIP_GROUP)
          End If

          If (IsDBNull(_row.Item(TB_COLUMN_CHIP_DRAW))) Then
            Me.Grid.Cell(_idx, GRID_COLUMN_CHIP_DRAW).Value = ""
          Else
            Me.Grid.Cell(_idx, GRID_COLUMN_CHIP_DRAW).Value = _row.Item(TB_COLUMN_CHIP_DRAW)
          End If

          If (IsDBNull(_row.Item(TB_COLUMN_CHIP_NAME))) Then
            Me.Grid.Cell(_idx, GRID_COLUMN_CHIP_NAME).Value = ""
          Else
            Me.Grid.Cell(_idx, GRID_COLUMN_CHIP_NAME).Value = _row.Item(TB_COLUMN_CHIP_NAME)
          End If

          _special_denomination = _row.Item(TB_COLUMN_DENOMINATION_WITH_SIMBOL).ToString()
          _denomination = SessionDetail.FormatAmount(_row.Item(TB_COLUMN_DENOMINATION), "")

          If String.IsNullOrEmpty(_special_denomination) AndAlso Not IsDBNull(_row.Item(TB_COLUMN_CURRENCY_TYPE)) AndAlso _iso_code <> Cage.CHIPS_ISO_CODE Then
            _currency_type = GetDescriptionCurrencyType(_row.Item(TB_COLUMN_CURRENCY_TYPE))
          ElseIf (_iso_code = Cage.CHIPS_ISO_CODE) Then
            _currency_type = GetDescriptionCurrencyType(CageCurrencyType.ChipsRedimible)
          Else
            _currency_type = ""
          End If

          If _row.Item(TB_COLUMN_SUBTOTAL_TOTAL) = GLB_NLS_GUI_INVOICING.GetString(375) Then ' SubTotal
            Me.Grid.Cell(_idx, GRID_COLUMN_DENOMINATION).Value = IIf(Not String.IsNullOrEmpty(_special_denomination), _special_denomination, _denomination)
            Me.Grid.Cell(_idx, GRID_COLUMN_CURRENCY_TYPE).Value = _currency_type
          Else ' Total
            Me.Grid.Cell(_idx, GRID_COLUMN_DENOMINATION).Value = ""
            Me.Grid.Cell(_idx, GRID_COLUMN_CURRENCY_TYPE).Value = ""
          End If


          If _special_denomination = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3365) OrElse _row.Item(TB_COLUMN_ISO_CODE) = Cage.CHIPS_COLOR Then ' Tickets
            Me.Grid.Cell(_idx, GRID_COLUMN_TYPE_NAME).Value = _row.Item(TB_COLUMN_SUBTOTAL_TOTAL)
            Me.Grid.Cell(_idx, GRID_COLUMN_OPENINGS).Value = GUI_FormatNumber(_row.Item(TB_COLUMN_OPENINGS), 0)
            Me.Grid.Cell(_idx, GRID_COLUMN_DEPOSITS).Value = GUI_FormatNumber(_row.Item(TB_COLUMN_DEPOSITS), 0)
            Me.Grid.Cell(_idx, GRID_COLUMN_WITHDRAWALS).Value = GUI_FormatNumber(_row.Item(TB_COLUMN_WITHDRAWALS), 0)
            Me.Grid.Cell(_idx, GRID_COLUMN_DIFFERENCE).Value = GUI_FormatNumber(_row.Item(TB_COLUMN_DIFFERENCE), 0)
            Me.Grid.Cell(_idx, GRID_COLUMN_LOST_QUADRATURE).Value = GUI_FormatNumber(_row.Item(TB_COLUMN_LOST_QUADRATURE), 0)
            Me.Grid.Cell(_idx, GRID_COLUMN_GAIN_QUADRATURE).Value = GUI_FormatNumber(_row.Item(TB_COLUMN_GAIN_QUADRATURE), 0)
            Me.Grid.Cell(_idx, GRID_COLUMN_CLOSING).Value = GUI_FormatNumber(_row.Item(TB_COLUMN_CLOSING), 0)
          Else
            Me.Grid.Cell(_idx, GRID_COLUMN_TYPE_NAME).Value = _row.Item(TB_COLUMN_SUBTOTAL_TOTAL)
            Me.Grid.Cell(_idx, GRID_COLUMN_OPENINGS).Value = SessionDetail.FormatAmount(_row.Item(TB_COLUMN_OPENINGS), _iso_code)
            Me.Grid.Cell(_idx, GRID_COLUMN_DEPOSITS).Value = SessionDetail.FormatAmount(_row.Item(TB_COLUMN_DEPOSITS), _iso_code)
            Me.Grid.Cell(_idx, GRID_COLUMN_WITHDRAWALS).Value = SessionDetail.FormatAmount(_row.Item(TB_COLUMN_WITHDRAWALS), _iso_code)
            Me.Grid.Cell(_idx, GRID_COLUMN_DIFFERENCE).Value = SessionDetail.FormatAmount(_row.Item(TB_COLUMN_DIFFERENCE), _iso_code)
            Me.Grid.Cell(_idx, GRID_COLUMN_LOST_QUADRATURE).Value = SessionDetail.FormatAmount(_row.Item(TB_COLUMN_LOST_QUADRATURE), _iso_code)
            Me.Grid.Cell(_idx, GRID_COLUMN_GAIN_QUADRATURE).Value = SessionDetail.FormatAmount(_row.Item(TB_COLUMN_GAIN_QUADRATURE), _iso_code)
            Me.Grid.Cell(_idx, GRID_COLUMN_CLOSING).Value = SessionDetail.FormatAmount(_row.Item(TB_COLUMN_CLOSING), _iso_code)
          End If

          If _row.Item(TB_COLUMN_SUBTOTAL_TOTAL) <> GLB_NLS_GUI_INVOICING.GetString(375) AndAlso _row.Item(TB_COLUMN_SUBTOTAL_TOTAL) <> (GLB_NLS_GUI_PLAYER_TRACKING.GetString(2265) + ":") Then
            Me.Grid.Row(_idx).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00) ' Total

          Else
            Me.Grid.Row(_idx).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01) ' Subtotal: or Quantity:
          End If
        Next

    End Select

  End Sub ' InsertTotals

  ' PURPOSE: Show cage stock
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Function ShowCageStock(ByVal CageStock As DataTable) As Boolean
    Dim _dr As DataRow
    Dim _idx_row As Int32
    Dim _str_iso_code As String
    Dim _dec_quantity_stock As Decimal
    Dim _dec_total_stock As Decimal
    Dim _str_denomination As String
    Dim _currencies_accepted As String
    Dim _str_query As String

    Call CheckRefreshGrid()

    If m_grid_overflow Then
      Return False
    End If

    _str_iso_code = ""
    m_rw_current_total_stock = m_dt_totals.NewRow()

    _currencies_accepted = "'" & GeneralParam.GetString("RegionalOptions", "CurrenciesAccepted").Replace(";", "', '") & "'"

    _str_query = "(ISO_CODE IN (" & _currencies_accepted & ") AND ((QUANTITY > 0) OR (TOTAL > 0) OR (DENOMINATION = " & Cage.COINS_CODE & ")" & "))"
    _str_query += " OR (QUANTITY > 0 OR TOTAL > 0 )"

    If Me.m_cage_show_denominations = Cage.ShowDenominationsMode.HideAllDenominations Then
      _str_query += " OR (ISO_CODE = '" & Cage.CHIPS_ISO_CODE & "' AND DENOMINATION = 1)"
    End If

    For Each _dr In CageStock.Select(_str_query, "ISO_CODE ASC, TYPE ASC, DENOMINATION DESC")
      Call CheckRefreshGrid()

      ' JAB 08-JUN-2012: Check if disposed.
      If m_grid_overflow Or Me.IsDisposed Then
        Return False
      End If

      ' RRR 28-JUL-2014: ADDED BANK CARDS TO STOCK VIEW
      ' Only shows bills, coins (tips in case of chips) and checks.
      If (_dr.Item(SQL_COLUMN_DENOMINATION_STOCK) > 0 _
        Or (_dr.Item(SQL_COLUMN_DENOMINATION_STOCK) = Cage.COINS_CODE AndAlso _dr.Item(SQL_COLUMN_ISO_CODE) <> Cage.CHIPS_ISO_CODE) _
        Or _dr.Item(SQL_COLUMN_DENOMINATION_STOCK) = Cage.CHECK_CODE _
        Or _dr.Item(SQL_COLUMN_DENOMINATION_STOCK) = Cage.BANK_CARD_CODE) _
        And (_dr.Item(SQL_COLUMN_CURRENCY_TYPE_STOCK) <> CageCurrencyType.ChipsRedimible And _dr.Item(SQL_COLUMN_CURRENCY_TYPE_STOCK) <> CageCurrencyType.ChipsNoRedimible) Then

        Me.Grid.AddRow()
        m_grid_refresh_pending_rows += 1
        _idx_row = Me.Grid.NumRows - 1

        m_row_index += 1

        ' IsoCode
        If Not _dr.IsNull(SQL_COLUMN_ISO_CODE_STOCK) Then
          _str_iso_code = _dr.Item(SQL_COLUMN_ISO_CODE_STOCK)
          Me.Grid.Cell(_idx_row, GRID_COLUMN_ISO_CODE_STOCK).Value = _dr.Item(SQL_COLUMN_ISO_CODE_STOCK)
        Else
          Me.Grid.Cell(_idx_row, GRID_COLUMN_ISO_CODE_STOCK).Value = ""
        End If

        ' Denomination [number]
        If Not _dr.IsNull(SQL_COLUMN_DENOMINATION_STOCK) Then
          Me.Grid.Cell(_idx_row, GRID_COLUMN_DENOMINATION_STOCK).Value = _dr.Item(SQL_COLUMN_DENOMINATION_STOCK)
        Else
          Me.Grid.Cell(_idx_row, GRID_COLUMN_DENOMINATION_STOCK).Value = ""
        End If
        _str_denomination = Me.Grid.Cell(_idx_row, GRID_COLUMN_DENOMINATION_STOCK).Value.ToString()

        ' Denomination [number or text]
        If Not _dr.IsNull(SQL_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK) Then
          If IsNumeric(_dr.Item(SQL_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK).ToString()) Then
            Me.Grid.Cell(_idx_row, GRID_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK).Value = SessionDetail.FormatAmount(_dr.Item(SQL_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK), "")
            ' Currency type
            If _str_iso_code <> Cage.CHIPS_ISO_CODE Then
              Me.Grid.Cell(_idx_row, GRID_COLUMN_CURRENCY_TYPE_STOCK).Value = IIf(_dr.Item(SQL_COLUMN_CURRENCY_TYPE_STOCK) = CageCurrencyType.Coin, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5676), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5675))
            End If
          Else
            Me.Grid.Cell(_idx_row, GRID_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK).Value = _dr.Item(SQL_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK)
          End If
        Else
          Me.Grid.Cell(_idx_row, GRID_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK).Value = ""
        End If

        ' Quantity
        If Not _dr.IsNull(SQL_COLUMN_QUANTITY_STOCK) Then
          If _dr.Item(SQL_COLUMN_DENOMINATION_STOCK) > 0 OrElse _str_denomination = Cage.TICKETS_CODE Then
            Me.Grid.Cell(_idx_row, GRID_COLUMN_QUANTITY_STOCK).Value = SessionDetail.FormatAmount(_dr.Item(SQL_COLUMN_QUANTITY_STOCK), "-")
          Else ' Coins, bank cards ,checks and tickets
            If Not IsDBNull(_dr.Item(SQL_COLUMN_TOTAL_STOCK)) AndAlso _dr.Item(SQL_COLUMN_TOTAL_STOCK) > 0 AndAlso _str_denomination <> Cage.COINS_CODE Then
              Me.Grid.Cell(_idx_row, GRID_COLUMN_QUANTITY_STOCK).Value = 1
            End If
          End If
        Else
          Me.Grid.Cell(_idx_row, GRID_COLUMN_QUANTITY_STOCK).Value = ""
        End If

        ' Total Stock
        If Me.Grid.Cell(_idx_row, GRID_COLUMN_DENOMINATION_STOCK).Value <> Cage.TICKETS_CODE Then
          If Not _dr.IsNull(SQL_COLUMN_TOTAL_STOCK) Then
            Me.Grid.Cell(_idx_row, GRID_COLUMN_TOTAL_STOCK).Value = SessionDetail.FormatAmount(_dr.Item(SQL_COLUMN_TOTAL_STOCK), _str_iso_code)
          Else
            Me.Grid.Cell(_idx_row, GRID_COLUMN_TOTAL_STOCK).Value = SessionDetail.FormatAmount(0, _str_iso_code)
          End If
        Else
          Me.Grid.Cell(_idx_row, GRID_COLUMN_TOTAL_STOCK).Value = ""
        End If


        ' Update Stock totals table
        ' First time
        If m_rw_current_total_stock(TB_COLUMN_ISO_CODE_STOCK).Equals(DBNull.Value) Then

          ' Total
          m_rw_current_total_stock.Item(TB_COLUMN_ISO_CODE_STOCK) = _dr.Item(SQL_COLUMN_ISO_CODE_STOCK)
          m_rw_current_total_stock.Item(TB_COLUMN_DENOMINATION_STOCK) = _dr.Item(SQL_COLUMN_DENOMINATION_STOCK)
          m_rw_current_total_stock.Item(TB_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK) = _dr.Item(SQL_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK)
          m_rw_current_total_stock.Item(TB_COLUMN_GRID_TYPE) = GLB_NLS_GUI_INVOICING.GetString(205) ' Total:
          Decimal.TryParse(_dr.Item(SQL_COLUMN_QUANTITY_STOCK).ToString(), _dec_quantity_stock)
          Decimal.TryParse(_dr.Item(SQL_COLUMN_TOTAL_STOCK).ToString(), _dec_total_stock)
          If _str_denomination <> Cage.COINS_CODE Then
            m_rw_current_total_stock.Item(TB_COLUMN_QUANTITY_STOCK) = _dec_quantity_stock
          End If
          m_rw_current_total_stock.Item(TB_COLUMN_TOTAL_STOCK) = _dec_total_stock

        Else
          ' Update
          If _dr.Item(SQL_COLUMN_ISO_CODE_STOCK) = m_rw_current_total_stock(TB_COLUMN_ISO_CODE_STOCK) Then
            Decimal.TryParse(_dr.Item(SQL_COLUMN_QUANTITY_STOCK).ToString(), _dec_quantity_stock)
            Decimal.TryParse(_dr.Item(SQL_COLUMN_TOTAL_STOCK).ToString(), _dec_total_stock)
            If _str_denomination <> Cage.COINS_CODE Then
              m_rw_current_total_stock.Item(TB_COLUMN_QUANTITY_STOCK) += _dec_quantity_stock
            End If
            m_rw_current_total_stock.Item(TB_COLUMN_TOTAL_STOCK) += _dec_total_stock

          End If

          ' Currency change [Add & New total]
          If _dr.Item(SQL_COLUMN_ISO_CODE_STOCK) <> m_rw_current_total_stock(TB_COLUMN_ISO_CODE_STOCK) Then

            m_dt_totals.Rows.Add(m_rw_current_total_stock)
            m_rw_current_total_stock = m_dt_totals.NewRow()
            m_rw_current_total_stock.Item(TB_COLUMN_ISO_CODE_STOCK) = _dr.Item(SQL_COLUMN_ISO_CODE_STOCK)
            m_rw_current_total_stock.Item(TB_COLUMN_DENOMINATION_STOCK) = _dr.Item(SQL_COLUMN_DENOMINATION_STOCK)
            m_rw_current_total_stock.Item(TB_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK) = _dr.Item(SQL_COLUMN_DENOMINATION_WITH_SIMBOL_STOCK)
            m_rw_current_total_stock.Item(TB_COLUMN_GRID_TYPE) = GLB_NLS_GUI_INVOICING.GetString(205) ' Total:
            Decimal.TryParse(_dr.Item(SQL_COLUMN_QUANTITY_STOCK).ToString(), _dec_quantity_stock)
            Decimal.TryParse(_dr.Item(SQL_COLUMN_TOTAL_STOCK).ToString(), _dec_total_stock)
            m_rw_current_total_stock.Item(TB_COLUMN_QUANTITY_STOCK) = _dec_quantity_stock
            m_rw_current_total_stock.Item(TB_COLUMN_TOTAL_STOCK) = _dec_total_stock

          End If
        End If
      End If
    Next

    Call InsertTotals()

    ' JAB 11-JUN-2012: Check if disposed.
    If Me.IsDisposed Then
      Return False
    End If

    Return True

  End Function ' ShowCageStock

  ' PURPOSE: updates session name or working day
  '
  ' PARAMS:
  '
  ' RETURNS:
  '     - db result
  '
  ' NOTES:
  Private Sub UpdateSessionNameWorkingDay()
    Dim _sb As StringBuilder
    Dim _original_name As String
    Dim _original_working_day As String
    Dim _closing_time As DateTime
    Dim _working_day As Date
    Dim _already_opened_cage_session As Boolean

    Try
      _already_opened_cage_session = False
      _closing_time = WSI.Common.Misc.TodayOpening()
      _working_day = New DateTime(Me.dtp_working_day.Value.Year, Me.dtp_working_day.Value.Month, Me.dtp_working_day.Value.Day, _closing_time.Hour, _closing_time.Minute, 0)

      _sb = New StringBuilder

      _sb.AppendLine("UPDATE   CAGE_SESSIONS ")
      _sb.AppendLine("   SET   CGS_SESSION_NAME = @pSessionName ")
      _sb.AppendLine("       , CGS_WORKING_DAY  = @pWorkingDay ")
      _sb.AppendLine(" WHERE   CGS_CAGE_SESSION_ID = @pSessionId ")

      Using _db_trx As New DB_TRX()
        If m_session_detail.m_session_working_day <> GUI_FormatDate(_working_day, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE).ToString() _
        AndAlso CheckIfAlreadyOpenedCageSession(_working_day, _db_trx) Then
          _already_opened_cage_session = True

          Return
        End If

        Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _cmd.Parameters.Add("@pSessionName", SqlDbType.VarChar).Value = Me.ef_cg_session_name.TextValue
          _cmd.Parameters.Add("@pWorkingDay", SqlDbType.DateTime).Value = _working_day
          _cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = Me.m_session_detail.m_session_id

          Call _cmd.ExecuteNonQuery()

        End Using
        _db_trx.Commit()
      End Using

      _original_name = m_session_detail.m_session_name
      _original_working_day = m_session_detail.m_session_working_day

      ' Update global member session
      m_session_detail.m_session_name = Me.ef_cg_session_name.TextValue
      m_session_detail.m_session_working_day = Me.dtp_working_day.Value

      ' Audit changes
      GUI_WriteAuditoryChanges(_original_name, m_session_detail.m_session_name, _original_working_day, m_session_detail.m_session_working_day)

    Catch _ex As Exception
      Log.Exception(_ex)

    Finally

      If _already_opened_cage_session Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6345), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, , CurrentUser.Name, GUI_FormatDate(_working_day, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE))
      End If
    End Try

  End Sub ' UpdateSessionName

  ' PURPOSE: checks if there is already an opened cage session for this user
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Private Function CheckIfAlreadyOpenedCageSession(ByVal GamingDay As DateTime, ByVal DBTrx As DB_TRX) As Boolean
    Dim _sb As StringBuilder

    _sb = New StringBuilder
    _sb.AppendLine(" SELECT   TOP 1 *                                ")
    _sb.AppendLine("   FROM   CAGE_SESSIONS                          ")
    _sb.AppendLine("  WHERE   CGS_OPEN_USER_ID  = @pUserID           ")
    _sb.AppendLine("    AND   CGS_CLOSE_DATETIME IS NULL             ")
    _sb.AppendLine("    AND   CGS_WORKING_DAY = @pGamingDay        ")

    Using _cmd As New SqlCommand(_sb.ToString())
      _cmd.Parameters.Add("@pUserID", SqlDbType.Int).Value = m_session_detail.m_session_opening_user_id
      _cmd.Parameters.Add("@pGamingDay", SqlDbType.DateTime).Value = GamingDay

      Using _reader As SqlDataReader = DBTrx.ExecuteReader(_cmd)
        If _reader.Read() Then
          Return True
        End If
      End Using
    End Using

    Return False

  End Function

  ' PURPOSE: Write auditory changes
  '
  ' PARAMS:        
  '
  Private Sub GUI_WriteAuditoryChanges(ByVal OriginalName As String, ByVal ChangedName As String, ByVal OriginalWorkingDay As String, ByVal ChangedWorkingDay As String)
    Dim _original_auditor_data As CLASS_AUDITOR_DATA
    Dim _auditor_data As CLASS_AUDITOR_DATA

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_CAGE)
    _original_auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_CAGE)

    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(3388), ChangedName)
    Call _original_auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(3388), OriginalName)

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(253), ChangedName)
    Call _original_auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(253), OriginalName)

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5723), ChangedWorkingDay)
    Call _original_auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5723), OriginalWorkingDay)

    _auditor_data.Notify(GLB_CurrentUser.GuiId, _
                                   GLB_CurrentUser.Id, _
                                   GLB_CurrentUser.Name, _
                                   CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.UPDATE, _
                                   0, _
                                   _original_auditor_data)
    _auditor_data = Nothing
    _original_auditor_data = Nothing

  End Sub 'GUI_WriteAuditoryChanges

#End Region

#Region " Overrides "

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    'Me.FormId = m_form_id
    '' Set the form icon from the embedded resource (ICO file must be built in the project as "Embedded Resource")
    '' Call could be made as GetManifestResourceStream("GUI_JackpotManager.GUI_JackpotManager.ico"))
    '' Me.Icon = New Icon(System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream(Me.GetType(), "GUI_JackpotManager.ico"))
    'Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  '
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _iso_code As String
    Dim _denomination As String

    Select Case m_form_mode
      Case FORM_MODE.SHOW_STOCK

      Case Else
        ' IsoCode
        If Not DbRow.IsNull(SQL_COLUMN_ISO_CODE) Then
          If (DbRow.Value(SQL_COLUMN_ISO_CODE) = Cage.CHIPS_COLOR) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_ISO_CODE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7286) ' Color
          ElseIf (DbRow.Value(SQL_COLUMN_ISO_CODE) = Cage.CHIPS_ISO_CODE) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_ISO_CODE).Value = CurrencyExchange.GetNationalCurrency()
          Else
            Me.Grid.Cell(RowIndex, GRID_COLUMN_ISO_CODE).Value = DbRow.Value(SQL_COLUMN_ISO_CODE)
          End If
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_ISO_CODE).Value = ""
        End If

        If (DbRow.Value(SQL_COLUMN_ISO_CODE) = Cage.CHIPS_ISO_CODE) Then
          _iso_code = Cage.CHIPS_ISO_CODE
        Else
          _iso_code = Me.Grid.Cell(RowIndex, GRID_COLUMN_ISO_CODE).Value
        End If

        ' Chip group
        If Not DbRow.IsNull(SQL_COLUMN_CHIP_GROUP) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_CHIP_GROUP).Value = DbRow.Value(SQL_COLUMN_CHIP_GROUP)
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_CHIP_GROUP).Value = ""
        End If

        ' Chip name
        If Not DbRow.IsNull(SQL_COLUMN_CHIP_NAME) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_CHIP_NAME).Value = DbRow.Value(SQL_COLUMN_CHIP_NAME)
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_CHIP_NAME).Value = ""
        End If

        ' Chip draw
        If Not DbRow.IsNull(SQL_COLUMN_CHIP_DRAW) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_CHIP_DRAW).Value = DbRow.Value(SQL_COLUMN_CHIP_DRAW)
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_CHIP_DRAW).Value = ""
        End If

        ' Denomination
        If Not DbRow.IsNull(SQL_COLUMN_QUANTITY) Then
          _denomination = DbRow.Value(SQL_COLUMN_QUANTITY)
        Else
          _denomination = ""
        End If

        Select Case _denomination
          Case Cage.COINS_CODE.ToString()
            If _iso_code = Cage.CHIPS_ISO_CODE Then
              _denomination = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3401) ' Propinas
            Else
              _denomination = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2952) ' Monedas
            End If
          Case Cage.TICKETS_CODE.ToString()
            _denomination = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3365) ' Tickets
          Case Cage.CHECK_CODE.ToString()
            _denomination = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1569) ' Cheque
          Case Cage.BANK_CARD_CODE.ToString()
            _denomination = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2157) ' Tarjeta
          Case Else
            _denomination = ""
        End Select

        If String.IsNullOrEmpty(_denomination) Then
          If Not DbRow.IsNull(SQL_COLUMN_DENOMINATION) Then
            If (DbRow.Value(SQL_COLUMN_CURRENCY_TYPE) = CageCurrencyType.ChipsColor) Then
              Me.Grid.Cell(RowIndex, GRID_COLUMN_DENOMINATION).Value = ""
            Else
              Me.Grid.Cell(RowIndex, GRID_COLUMN_DENOMINATION).Value = SessionDetail.FormatAmount(DbRow.Value(SQL_COLUMN_DENOMINATION), "")
            End If

            ' Currency type
            If _iso_code <> Cage.CHIPS_ISO_CODE Then
              Me.Grid.Cell(RowIndex, GRID_COLUMN_CURRENCY_TYPE).Value = GetDescriptionCurrencyType(DbRow.Value(SQL_COLUMN_CURRENCY_TYPE))
            Else
              Me.Grid.Cell(RowIndex, GRID_COLUMN_CURRENCY_TYPE).Value = GetDescriptionCurrencyType(CageCurrencyType.ChipsRedimible)
            End If
          Else
            Me.Grid.Cell(RowIndex, GRID_COLUMN_DENOMINATION).Value = ""
          End If
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_DENOMINATION).Value = _denomination
        End If

        ' Type Id
        If Not DbRow.IsNull(SQL_COLUMN_TYPE) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE_ID).Value = DbRow.Value(SQL_COLUMN_TYPE)
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE_ID).Value = ""
        End If

        ' Type name / Operation name
        If Not IsDBNull(DbRow.Value(SQL_COLUMN_TYPE)) Then
          Select Case DbRow.Value(SQL_COLUMN_TYPE)
            Case Cage.CageOperationType.ToCashier
              Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4376)

            Case Cage.CageOperationType.FromCashier
              Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4377)

            Case Cage.CageOperationType.FromCashierClosing
              Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7898)

            Case Cage.CageOperationType.ToCustom
              Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3394)

            Case Cage.CageOperationType.FromCustom
              Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3395)

            Case Cage.CageOperationType.OpenCage
              Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3381)

            Case Cage.CageOperationType.CloseCage
              Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3382)

            Case Cage.CageOperationType.CountCage _
               , Cage.CageOperationType.GainQuadrature _
               , Cage.CageOperationType.LostQuadrature

              Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4352)

            Case Cage.CageOperationType.ToGamingTable
              Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4394)

            Case Cage.CageOperationType.FromGamingTable
              Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4395)

            Case Cage.CageOperationType.FromGamingTableClosing
              Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7900)

            Case Cage.CageOperationType.FromGamingTableDropbox, _
              Cage.CageOperationType.FromGamingTableDropboxWithExpected
              Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7329) ' Gambling Table drop box collection

            Case Cage.CageOperationType.FromTerminal
              Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3308)

            Case Cage.CageOperationType.RequestOperation
              Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4785)

            Case Else
              Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE_NAME).Value = AUDIT_NONE_STRING

          End Select
        End If

        ' Status Id
        If Not DbRow.IsNull(SQL_COLUMN_STATUS) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_ID).Value = DbRow.Value(SQL_COLUMN_STATUS)
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_ID).Value = ""
        End If

        If Not IsDBNull(DbRow.Value(SQL_COLUMN_STATUS)) Then
          Select Case DbRow.Value(SQL_COLUMN_STATUS)
            Case Cage.CageStatus.Sent
              If DbRow.Value(SQL_COLUMN_TYPE) = Cage.CageOperationType.ToCashier Then
                Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2891)
              Else
                If DbRow.Value(SQL_COLUMN_TYPE) = Cage.CageOperationType.RequestOperation Then
                  Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4786)
                Else
                  Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3011)
                End If
              End If

            Case Cage.CageStatus.OK
              If DbRow.Value(SQL_COLUMN_TYPE) = Cage.CageOperationType.ToCashier Then
                Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2892)
              Else
                Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3305)
              End If


            Case Cage.CageStatus.CanceledDueToImbalanceOrOther
              Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2893)

            Case Cage.CageStatus.OkDenominationImbalance
              If DbRow.Value(SQL_COLUMN_TYPE) = Cage.CageOperationType.ToCashier Then
                Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2944)
              Else
                Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3021)
              End If

            Case Cage.CageStatus.OkAmountImbalance
              If DbRow.Value(SQL_COLUMN_TYPE) = Cage.CageOperationType.ToCashier Then
                Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2894)
              Else
                Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3022)
              End If

            Case Cage.CageStatus.Canceled
              Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1891)
              Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_03)

            Case Cage.CageStatus.SentToCustom
              Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2892)

            Case Cage.CageStatus.ReceptionFromCustom
              Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3305)

            Case Cage.CageStatus.FinishedOpenCloseCage _
               , Cage.CageStatus.FinishedCountCage
              Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2892)

            Case Else
              Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_NAME).Value = AUDIT_NONE_STRING

          End Select

        End If

        ' Movement Id
        If Not DbRow.IsNull(SQL_COLUMN_MOVEMENT_ID) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_MOVEMENT_ID).Value = DbRow.Value(SQL_COLUMN_MOVEMENT_ID)
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_MOVEMENT_ID).Value = ""
        End If

        If _denomination = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3365) Or DbRow.Value(SQL_COLUMN_ISO_CODE) = Cage.CHIPS_COLOR Then ' Tickets or X02
          ' Openings
          If Not DbRow.IsNull(SQL_COLUMN_OPENINGS) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_OPENINGS).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_OPENINGS), 0)
          Else
            Me.Grid.Cell(RowIndex, GRID_COLUMN_OPENINGS).Value = ""
          End If

          ' Deposits
          If Not DbRow.IsNull(SQL_COLUMN_DEPOSITS) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_DEPOSITS).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_DEPOSITS), 0)
          Else
            Me.Grid.Cell(RowIndex, GRID_COLUMN_DEPOSITS).Value = ""
          End If

          ' Withdrawals
          If Not DbRow.IsNull(SQL_COLUMN_WITHDRAWALS) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_WITHDRAWALS).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_WITHDRAWALS), 0)
          Else
            Me.Grid.Cell(RowIndex, GRID_COLUMN_WITHDRAWALS).Value = ""
          End If

          ' Difference
          If Not DbRow.IsNull(SQL_COLUMN_DIFFERENCE) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_DIFFERENCE).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_DIFFERENCE), 0)
          Else
            Me.Grid.Cell(RowIndex, GRID_COLUMN_DIFFERENCE).Value = ""
          End If

          ' Lost quadrature
          If Not DbRow.IsNull(SQL_COLUMN_LOST_QUADRATURE) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_LOST_QUADRATURE).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_LOST_QUADRATURE), 0)
          Else
            Me.Grid.Cell(RowIndex, GRID_COLUMN_LOST_QUADRATURE).Value = ""
          End If

          ' Gain quadrature
          If Not DbRow.IsNull(SQL_COLUMN_GAIN_QUADRATURE) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_GAIN_QUADRATURE).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_GAIN_QUADRATURE), 0)
          Else
            Me.Grid.Cell(RowIndex, GRID_COLUMN_GAIN_QUADRATURE).Value = ""
          End If

          ' Closing
          If Not DbRow.IsNull(SQL_COLUMN_CLOSING) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_CLOSING).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_CLOSING), 0)
          Else
            Me.Grid.Cell(RowIndex, GRID_COLUMN_CLOSING).Value = ""
          End If

        Else

          ' Openings
          If Not DbRow.IsNull(SQL_COLUMN_OPENINGS) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_OPENINGS).Value = SessionDetail.FormatAmount(DbRow.Value(SQL_COLUMN_OPENINGS), _iso_code)
          Else
            Me.Grid.Cell(RowIndex, GRID_COLUMN_OPENINGS).Value = ""
          End If

          ' Deposits
          If Not DbRow.IsNull(SQL_COLUMN_DEPOSITS) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_DEPOSITS).Value = SessionDetail.FormatAmount(DbRow.Value(SQL_COLUMN_DEPOSITS), _iso_code)
          Else
            Me.Grid.Cell(RowIndex, GRID_COLUMN_DEPOSITS).Value = ""
          End If

          ' Withdrawals
          If Not DbRow.IsNull(SQL_COLUMN_WITHDRAWALS) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_WITHDRAWALS).Value = SessionDetail.FormatAmount(DbRow.Value(SQL_COLUMN_WITHDRAWALS), _iso_code)
          Else
            Me.Grid.Cell(RowIndex, GRID_COLUMN_WITHDRAWALS).Value = ""
          End If

          ' Difference
          If Not DbRow.IsNull(SQL_COLUMN_DIFFERENCE) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_DIFFERENCE).Value = SessionDetail.FormatAmount(DbRow.Value(SQL_COLUMN_DIFFERENCE), _iso_code)
          Else
            Me.Grid.Cell(RowIndex, GRID_COLUMN_DIFFERENCE).Value = ""
          End If

          ' Lost quadrature
          If Not DbRow.IsNull(SQL_COLUMN_LOST_QUADRATURE) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_LOST_QUADRATURE).Value = SessionDetail.FormatAmount(DbRow.Value(SQL_COLUMN_LOST_QUADRATURE), _iso_code)
          Else
            Me.Grid.Cell(RowIndex, GRID_COLUMN_LOST_QUADRATURE).Value = ""
          End If

          ' Gain quadrature
          If Not DbRow.IsNull(SQL_COLUMN_GAIN_QUADRATURE) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_GAIN_QUADRATURE).Value = SessionDetail.FormatAmount(DbRow.Value(SQL_COLUMN_GAIN_QUADRATURE), _iso_code)
          Else
            Me.Grid.Cell(RowIndex, GRID_COLUMN_GAIN_QUADRATURE).Value = ""
          End If

          ' Closing
          If Not DbRow.IsNull(SQL_COLUMN_CLOSING) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_CLOSING).Value = SessionDetail.FormatAmount(DbRow.Value(SQL_COLUMN_CLOSING), _iso_code)
          Else
            Me.Grid.Cell(RowIndex, GRID_COLUMN_CLOSING).Value = ""
          End If


        End If

        ' Update totals table
        ' First time
        If m_rw_current_total(TB_COLUMN_ISO_CODE).Equals(DBNull.Value) And _
           m_rw_current_subtotal(TB_COLUMN_ISO_CODE).Equals(DBNull.Value) Then

          ' SubTotal
          m_rw_current_subtotal.Item(TB_COLUMN_ISO_CODE) = DbRow.Value(SQL_COLUMN_ISO_CODE)
          m_rw_current_subtotal.Item(TB_COLUMN_CHIP_GROUP) = DbRow.Value(SQL_COLUMN_CHIP_GROUP)
          m_rw_current_subtotal.Item(TB_COLUMN_CHIP_NAME) = DbRow.Value(SQL_COLUMN_CHIP_NAME)
          m_rw_current_subtotal.Item(TB_COLUMN_CHIP_DRAW) = DbRow.Value(SQL_COLUMN_CHIP_DRAW)
          m_rw_current_subtotal.Item(TB_COLUMN_DENOMINATION) = DbRow.Value(SQL_COLUMN_DENOMINATION)
          m_rw_current_subtotal.Item(TB_COLUMN_DENOMINATION_WITH_SIMBOL) = IIf(Not String.IsNullOrEmpty(_denomination), _denomination, "")

          If (DbRow.Value(SQL_COLUMN_CURRENCY_TYPE) = CageCurrencyType.ChipsColor) Then
            m_rw_current_subtotal.Item(TB_COLUMN_SUBTOTAL_TOTAL) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2265) + ":" ' Cantidad:
          Else
            m_rw_current_subtotal.Item(TB_COLUMN_SUBTOTAL_TOTAL) = GLB_NLS_GUI_INVOICING.GetString(375) ' Subtotal:
          End If

          m_rw_current_subtotal.Item(TB_COLUMN_OPENINGS) = DbRow.Value(SQL_COLUMN_OPENINGS)
          m_rw_current_subtotal.Item(TB_COLUMN_DEPOSITS) = DbRow.Value(SQL_COLUMN_DEPOSITS)
          m_rw_current_subtotal.Item(TB_COLUMN_WITHDRAWALS) = DbRow.Value(SQL_COLUMN_WITHDRAWALS)
          m_rw_current_subtotal.Item(TB_COLUMN_DIFFERENCE) = DbRow.Value(SQL_COLUMN_DIFFERENCE)
          m_rw_current_subtotal.Item(TB_COLUMN_LOST_QUADRATURE) = DbRow.Value(SQL_COLUMN_LOST_QUADRATURE)
          m_rw_current_subtotal.Item(TB_COLUMN_GAIN_QUADRATURE) = DbRow.Value(SQL_COLUMN_GAIN_QUADRATURE)
          m_rw_current_subtotal.Item(TB_COLUMN_CLOSING) = DbRow.Value(SQL_COLUMN_CLOSING)
          m_rw_current_subtotal.Item(TB_COLUMN_DENOMINATION_REAL) = DbRow.Value(SQL_COLUMN_DENOMINATION_REAL)
          m_rw_current_subtotal.Item(TB_COLUMN_CURRENCY_TYPE) = DbRow.Value(SQL_COLUMN_CURRENCY_TYPE)

          ' Total
          m_rw_current_total.Item(TB_COLUMN_ISO_CODE) = DbRow.Value(SQL_COLUMN_ISO_CODE)
          m_total_rw_current_chip_group = DbRow.Value(SQL_COLUMN_CHIP_GROUP)
          m_rw_current_total.Item(TB_COLUMN_DENOMINATION) = DbRow.Value(SQL_COLUMN_DENOMINATION)
          m_rw_current_total.Item(TB_COLUMN_SUBTOTAL_TOTAL) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4559, DbRow.Value(SQL_COLUMN_ISO_CODE))
          m_rw_current_total.Item(TB_COLUMN_OPENINGS) = DbRow.Value(SQL_COLUMN_OPENINGS)
          m_rw_current_total.Item(TB_COLUMN_DEPOSITS) = DbRow.Value(SQL_COLUMN_DEPOSITS)
          m_rw_current_total.Item(TB_COLUMN_WITHDRAWALS) = DbRow.Value(SQL_COLUMN_WITHDRAWALS)
          m_rw_current_total.Item(TB_COLUMN_DIFFERENCE) = DbRow.Value(SQL_COLUMN_DIFFERENCE)
          m_rw_current_total.Item(TB_COLUMN_LOST_QUADRATURE) = DbRow.Value(SQL_COLUMN_LOST_QUADRATURE)
          m_rw_current_total.Item(TB_COLUMN_GAIN_QUADRATURE) = DbRow.Value(SQL_COLUMN_GAIN_QUADRATURE)
          m_rw_current_total.Item(TB_COLUMN_CLOSING) = DbRow.Value(SQL_COLUMN_CLOSING)

        Else
          ' Update
          If DbRow.Value(SQL_COLUMN_ISO_CODE) = m_rw_current_total(TB_COLUMN_ISO_CODE) And _
             DbRow.Value(SQL_COLUMN_DENOMINATION_REAL) = m_rw_current_subtotal(TB_COLUMN_DENOMINATION_REAL) And _
             DbRow.Value(SQL_COLUMN_CHIP_NAME) = m_rw_current_subtotal(TB_COLUMN_CHIP_NAME) Then

            m_rw_current_subtotal.Item(TB_COLUMN_OPENINGS) += DbRow.Value(SQL_COLUMN_OPENINGS)
            m_rw_current_subtotal.Item(TB_COLUMN_DEPOSITS) += DbRow.Value(SQL_COLUMN_DEPOSITS)
            m_rw_current_subtotal.Item(TB_COLUMN_WITHDRAWALS) += DbRow.Value(SQL_COLUMN_WITHDRAWALS)
            m_rw_current_subtotal.Item(TB_COLUMN_DIFFERENCE) += DbRow.Value(SQL_COLUMN_DIFFERENCE)
            m_rw_current_subtotal.Item(TB_COLUMN_LOST_QUADRATURE) += DbRow.Value(SQL_COLUMN_LOST_QUADRATURE)
            m_rw_current_subtotal.Item(TB_COLUMN_GAIN_QUADRATURE) += DbRow.Value(SQL_COLUMN_GAIN_QUADRATURE)
            m_rw_current_subtotal.Item(TB_COLUMN_CLOSING) += DbRow.Value(SQL_COLUMN_CLOSING)

            If DbRow.Value(SQL_COLUMN_DENOMINATION_REAL) <> Cage.TICKETS_CODE Then
              m_rw_current_total.Item(TB_COLUMN_OPENINGS) += DbRow.Value(SQL_COLUMN_OPENINGS)
              m_rw_current_total.Item(TB_COLUMN_DEPOSITS) += DbRow.Value(SQL_COLUMN_DEPOSITS)
              m_rw_current_total.Item(TB_COLUMN_WITHDRAWALS) += DbRow.Value(SQL_COLUMN_WITHDRAWALS)
              m_rw_current_total.Item(TB_COLUMN_DIFFERENCE) += DbRow.Value(SQL_COLUMN_DIFFERENCE)
              m_rw_current_total.Item(TB_COLUMN_LOST_QUADRATURE) += DbRow.Value(SQL_COLUMN_LOST_QUADRATURE)
              m_rw_current_total.Item(TB_COLUMN_GAIN_QUADRATURE) += DbRow.Value(SQL_COLUMN_GAIN_QUADRATURE)
              m_rw_current_total.Item(TB_COLUMN_CLOSING) += DbRow.Value(SQL_COLUMN_CLOSING)
            End If

          End If

          ' Currency change [New subtotal; Add & New total]
          If DbRow.Value(SQL_COLUMN_ISO_CODE) <> m_rw_current_total(TB_COLUMN_ISO_CODE) Or
              DbRow.Value(SQL_COLUMN_CHIP_GROUP) <> m_total_rw_current_chip_group Then

            m_dt_totals.Rows.Add(m_rw_current_subtotal)
            m_rw_current_subtotal = m_dt_totals.NewRow()
            m_rw_current_subtotal.Item(TB_COLUMN_ISO_CODE) = DbRow.Value(SQL_COLUMN_ISO_CODE)
            m_rw_current_subtotal.Item(TB_COLUMN_CHIP_GROUP) = DbRow.Value(SQL_COLUMN_CHIP_GROUP)
            m_rw_current_subtotal.Item(TB_COLUMN_CHIP_NAME) = DbRow.Value(SQL_COLUMN_CHIP_NAME)
            m_rw_current_subtotal.Item(TB_COLUMN_CHIP_DRAW) = DbRow.Value(SQL_COLUMN_CHIP_DRAW)
            m_rw_current_subtotal.Item(TB_COLUMN_DENOMINATION) = DbRow.Value(SQL_COLUMN_DENOMINATION)
            m_rw_current_subtotal.Item(TB_COLUMN_DENOMINATION_WITH_SIMBOL) = IIf(Not String.IsNullOrEmpty(_denomination), _denomination, "")

            If (DbRow.Value(SQL_COLUMN_CURRENCY_TYPE) = CageCurrencyType.ChipsColor) Then
              m_rw_current_subtotal.Item(TB_COLUMN_SUBTOTAL_TOTAL) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2265) + ":" ' Cantidad:
            Else
              m_rw_current_subtotal.Item(TB_COLUMN_SUBTOTAL_TOTAL) = GLB_NLS_GUI_INVOICING.GetString(375) ' Subtotal:
            End If

            m_rw_current_subtotal.Item(TB_COLUMN_OPENINGS) = DbRow.Value(SQL_COLUMN_OPENINGS)
            m_rw_current_subtotal.Item(TB_COLUMN_DEPOSITS) = DbRow.Value(SQL_COLUMN_DEPOSITS)
            m_rw_current_subtotal.Item(TB_COLUMN_WITHDRAWALS) = DbRow.Value(SQL_COLUMN_WITHDRAWALS)
            m_rw_current_subtotal.Item(TB_COLUMN_DIFFERENCE) = DbRow.Value(SQL_COLUMN_DIFFERENCE)
            m_rw_current_subtotal.Item(TB_COLUMN_LOST_QUADRATURE) = DbRow.Value(SQL_COLUMN_LOST_QUADRATURE)
            m_rw_current_subtotal.Item(TB_COLUMN_GAIN_QUADRATURE) = DbRow.Value(SQL_COLUMN_GAIN_QUADRATURE)
            m_rw_current_subtotal.Item(TB_COLUMN_CLOSING) = DbRow.Value(SQL_COLUMN_CLOSING)
            m_rw_current_subtotal.Item(TB_COLUMN_DENOMINATION_REAL) = DbRow.Value(SQL_COLUMN_DENOMINATION_REAL)
            m_rw_current_subtotal.Item(TB_COLUMN_CURRENCY_TYPE) = DbRow.Value(SQL_COLUMN_CURRENCY_TYPE)

            m_dt_totals.Rows.Add(m_rw_current_total)
            m_rw_current_total = m_dt_totals.NewRow()
            m_rw_current_total.Item(TB_COLUMN_ISO_CODE) = DbRow.Value(SQL_COLUMN_ISO_CODE)
            m_total_rw_current_chip_group = DbRow.Value(SQL_COLUMN_CHIP_GROUP)
            m_rw_current_total.Item(TB_COLUMN_DENOMINATION) = DbRow.Value(SQL_COLUMN_DENOMINATION)

            If (String.IsNullOrEmpty(DbRow.Value(SQL_COLUMN_CHIP_GROUP))) Then
              m_rw_current_total.Item(TB_COLUMN_SUBTOTAL_TOTAL) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4559, DbRow.Value(SQL_COLUMN_ISO_CODE))
            Else
              m_rw_current_total.Item(TB_COLUMN_SUBTOTAL_TOTAL) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4559, DbRow.Value(SQL_COLUMN_CHIP_GROUP))
            End If

            m_rw_current_total.Item(TB_COLUMN_OPENINGS) = DbRow.Value(SQL_COLUMN_OPENINGS)
            m_rw_current_total.Item(TB_COLUMN_DEPOSITS) = DbRow.Value(SQL_COLUMN_DEPOSITS)
            m_rw_current_total.Item(TB_COLUMN_WITHDRAWALS) = DbRow.Value(SQL_COLUMN_WITHDRAWALS)
            m_rw_current_total.Item(TB_COLUMN_DIFFERENCE) = DbRow.Value(SQL_COLUMN_DIFFERENCE)
            m_rw_current_total.Item(TB_COLUMN_LOST_QUADRATURE) = DbRow.Value(SQL_COLUMN_LOST_QUADRATURE)
            m_rw_current_total.Item(TB_COLUMN_GAIN_QUADRATURE) = DbRow.Value(SQL_COLUMN_GAIN_QUADRATURE)
            m_rw_current_total.Item(TB_COLUMN_CLOSING) = DbRow.Value(SQL_COLUMN_CLOSING)

          End If

          ' Denomination change [Add & New subtotal; update total]
          If DbRow.Value(SQL_COLUMN_ISO_CODE) = m_rw_current_total(TB_COLUMN_ISO_CODE) And _
             (DbRow.Value(SQL_COLUMN_DENOMINATION_REAL) <> m_rw_current_subtotal(TB_COLUMN_DENOMINATION_REAL) Or _
              DbRow.Value(SQL_COLUMN_CHIP_NAME) <> m_rw_current_subtotal(SQL_COLUMN_CHIP_NAME)) Then

            m_dt_totals.Rows.Add(m_rw_current_subtotal)
            m_rw_current_subtotal = m_dt_totals.NewRow()
            m_rw_current_subtotal.Item(TB_COLUMN_ISO_CODE) = DbRow.Value(SQL_COLUMN_ISO_CODE)
            m_rw_current_subtotal.Item(TB_COLUMN_CHIP_GROUP) = DbRow.Value(SQL_COLUMN_CHIP_GROUP)
            m_rw_current_subtotal.Item(TB_COLUMN_CHIP_NAME) = DbRow.Value(SQL_COLUMN_CHIP_NAME)
            m_rw_current_subtotal.Item(TB_COLUMN_CHIP_DRAW) = DbRow.Value(SQL_COLUMN_CHIP_DRAW)
            m_rw_current_subtotal.Item(TB_COLUMN_DENOMINATION) = DbRow.Value(SQL_COLUMN_DENOMINATION)
            m_rw_current_subtotal.Item(TB_COLUMN_DENOMINATION_WITH_SIMBOL) = IIf(Not String.IsNullOrEmpty(_denomination), _denomination, "")

            If (DbRow.Value(SQL_COLUMN_CURRENCY_TYPE) = CageCurrencyType.ChipsColor) Then
              m_rw_current_subtotal.Item(TB_COLUMN_SUBTOTAL_TOTAL) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2265) + ":" ' Cantidad:
            Else
              m_rw_current_subtotal.Item(TB_COLUMN_SUBTOTAL_TOTAL) = GLB_NLS_GUI_INVOICING.GetString(375) ' Subtotal:
            End If

            m_rw_current_subtotal.Item(TB_COLUMN_OPENINGS) = DbRow.Value(SQL_COLUMN_OPENINGS)
            m_rw_current_subtotal.Item(TB_COLUMN_DEPOSITS) = DbRow.Value(SQL_COLUMN_DEPOSITS)
            m_rw_current_subtotal.Item(TB_COLUMN_WITHDRAWALS) = DbRow.Value(SQL_COLUMN_WITHDRAWALS)
            m_rw_current_subtotal.Item(TB_COLUMN_DIFFERENCE) = DbRow.Value(SQL_COLUMN_DIFFERENCE)
            m_rw_current_subtotal.Item(TB_COLUMN_LOST_QUADRATURE) = DbRow.Value(SQL_COLUMN_LOST_QUADRATURE)
            m_rw_current_subtotal.Item(TB_COLUMN_GAIN_QUADRATURE) = DbRow.Value(SQL_COLUMN_GAIN_QUADRATURE)
            m_rw_current_subtotal.Item(TB_COLUMN_CLOSING) = DbRow.Value(SQL_COLUMN_CLOSING)
            m_rw_current_subtotal.Item(TB_COLUMN_DENOMINATION_REAL) = DbRow.Value(SQL_COLUMN_DENOMINATION_REAL)
            m_rw_current_subtotal.Item(TB_COLUMN_CURRENCY_TYPE) = DbRow.Value(SQL_COLUMN_CURRENCY_TYPE)

            If DbRow.Value(SQL_COLUMN_DENOMINATION_REAL) <> Cage.TICKETS_CODE Then
              m_rw_current_total.Item(TB_COLUMN_OPENINGS) += DbRow.Value(SQL_COLUMN_OPENINGS)
              m_rw_current_total.Item(TB_COLUMN_DEPOSITS) += DbRow.Value(SQL_COLUMN_DEPOSITS)
              m_rw_current_total.Item(TB_COLUMN_WITHDRAWALS) += DbRow.Value(SQL_COLUMN_WITHDRAWALS)
              m_rw_current_total.Item(TB_COLUMN_DIFFERENCE) += DbRow.Value(SQL_COLUMN_DIFFERENCE)
              m_rw_current_total.Item(TB_COLUMN_LOST_QUADRATURE) += DbRow.Value(SQL_COLUMN_LOST_QUADRATURE)
              m_rw_current_total.Item(TB_COLUMN_GAIN_QUADRATURE) += DbRow.Value(SQL_COLUMN_GAIN_QUADRATURE)
              m_rw_current_total.Item(TB_COLUMN_CLOSING) += DbRow.Value(SQL_COLUMN_CLOSING)
            End If
          End If
        End If

    End Select

    m_row_index += 1

    Return True

  End Function ' GUI_SetupRow

  ''' <summary>
  ''' Get currency type description from set grid currency type column
  ''' </summary>
  ''' <param name="CurrencyType">Value to currency type dbrow</param>
  ''' <returns>Description currency type</returns>
  ''' <remarks></remarks>
  Private Function GetDescriptionCurrencyType(ByVal CurrencyType As Integer) As String
    Dim _chip_currency_exchange_type As CurrencyExchangeType
    Dim _result As String

    Select Case CurrencyType
      Case CageCurrencyType.Bill
        _result = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5675)
      Case CageCurrencyType.Coin
        _result = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5676)
      Case CageCurrencyType.ChipsRedimible,
           CageCurrencyType.ChipsNoRedimible,
           CageCurrencyType.ChipsColor
        _chip_currency_exchange_type = FeatureChips.ConvertToCurrencyExchangeType(CType(CurrencyType, CageCurrencyType))
        _result = FeatureChips.GetChipTypeDescription(_chip_currency_exchange_type, "")
      Case Else
        _result = ""
    End Select

    Return _result
  End Function

  ' PURPOSE : Set query type
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Protected Overrides Function GUI_GetQueryType() As GUI_Controls.frm_base_sel.ENUM_QUERY_TYPE

    Select Case m_form_mode
      Case FORM_MODE.SHOW_DETAIL
        Return ENUM_QUERY_TYPE.QUERY_DATABASE

      Case FORM_MODE.SHOW_STOCK
        Return ENUM_QUERY_TYPE.QUERY_CUSTOM

    End Select

  End Function ' GUI_GetQueryType

  ' PURPOSE : Define the ExecuteQuery customized
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '
  Protected Overrides Sub GUI_ExecuteQueryCustom()

    Call CreateTotalsTable(m_form_mode)
    Call m_theoric_stock.GetGlobalCageStock(True, False, True)

    Try
      Me.Grid.Redraw = False

      ' Grid load and refresh
      m_grid_overflow = False
      m_grid_refresh_pending_rows = 0
      m_grid_refresh_step = Math.Max(MAX_RECORDS_FIRST_LOAD, GUI_MaxRows() / 20)
      m_grid_refresh_step = 10

      ' Refresh labels before executing SQL Query.
      Call Application.DoEvents()

      If m_theoric_stock.TableCurrencies().Rows.Count = 0 Then
        Return
      End If

      ' m_theoric_stock.TableCurrencies
      ' m_theoric_stock.TableTheoricValues()
      If Not ShowCageStock(m_theoric_stock.TableCurrencies()) Then

        If Me.IsDisposed Then

          Return
        End If

        ' 123 "Se ha producido un error al acceder a la base de datos."
        Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

        Return
      End If

    Catch _ex As Exception
      ' An error has occurred in the query execution
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Trace.WriteLine(_ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "Cage Stock", _
                            "GUI_ExecuteQueryCustom", _
                            _ex.Message)

    Finally
      ' Check if disposed.
      If Not Me.IsDisposed Then
        Me.Grid.Redraw = True
      End If

      Call Application.DoEvents()

    End Try

  End Sub ' GUI_ExecuteQueryCustom

  ' PURPOSE : Actions to doing before insert the first row
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Protected Overrides Sub GUI_BeforeFirstRow()

    m_rw_current_total = m_dt_totals.NewRow()
    m_rw_current_subtotal = m_dt_totals.NewRow()

  End Sub ' GUI_BeforeFirstRow

  ' PURPOSE : Actions to doing after insert the last row
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Protected Overrides Sub GUI_AfterLastRow()
    If m_row_index > 0 Then
      Call InsertTotals()
    End If
  End Sub ' GUI_AfterLastRow

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4560)

    '- Buttons
    ' Disable unused buttons: Delete button is not needed
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4348)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_RESET).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(1) ' Salir

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = Not Me.m_show_only_stock
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_CONTROLS.GetString(13)  ' Guardar
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Type = uc_button.ENUM_BUTTON_TYPE.USER
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_CAGE_SESSIONS_DETAIL).Write

    Me.Grid.PanelRightVisible = False

    m_row_index = 0

    m_form_mode = FORM_MODE.SHOW_DETAIL
    m_theoric_stock = New CLASS_CAGE_CONTROL()
    m_total_rw_current_chip_group = Nothing

    Call SetCheckedList()

    Call SetLabelsText()
    Call GUI_StyleSheet()
    Call GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)

  End Sub ' GUI_InitControls

  ' PURPOSE : Actions to doing when button is clicked
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base.ENUM_BUTTON)

    If Me.m_show_only_stock And Me.m_first_time Then
      ButtonId = ENUM_BUTTON.BUTTON_CUSTOM_1
      Me.m_first_time = False
    End If

    Select Case ButtonId

      Case ENUM_BUTTON.BUTTON_CUSTOM_2 ' Detalle de B�veda
        m_row_index = 0
        Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = True
        Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = False

        m_form_mode = FORM_MODE.SHOW_DETAIL
        Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4393) '& " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(474)
        Call GUI_StyleSheet()
        Call MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)

      Case ENUM_BUTTON.BUTTON_CUSTOM_1 ' Stock de B�veda
        m_row_index = 0
        Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = False
        Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = True

        m_form_mode = FORM_MODE.SHOW_STOCK
        Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4561) '"Stock de b�veda"
        'm_form_title = Me.Text
        Call GUI_StyleSheet()
        MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)

      Case ENUM_BUTTON.BUTTON_FILTER_APPLY
        m_row_index = 0
        If m_form_mode = FORM_MODE.SHOW_DETAIL Then
          Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4393) '& " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(474) ' - Detalle
        Else
          Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4561) '"Stock de b�veda"
        End If

        MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)

      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        If Not String.IsNullOrEmpty(Me.ef_cg_session_name.TextValue) Then
          If m_session_detail.m_session_name <> Me.ef_cg_session_name.TextValue Or m_session_detail.m_session_working_day <> Me.dtp_working_day.Value Then
            Call UpdateSessionNameWorkingDay()
          End If
        Else
          '"Debe introducir un valor para %1."
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.ef_cg_session_name.Text)
        End If

      Case ENUM_BUTTON.BUTTON_CANCEL
        If m_session_detail.m_session_name <> Me.ef_cg_session_name.TextValue Then
          '"Los cambios realizados se perder�n.\n�Est� seguro que quiere salir?"
          If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, , _
             Me.ef_cg_session_name.Text) = ENUM_MB_RESULT.MB_RESULT_NO Then

            Exit Select
          Else
            m_show_message_when_closing = False
          End If
        End If
        MyBase.GUI_ButtonClick(ButtonId)

      Case Else
        MyBase.GUI_ButtonClick(ButtonId)

    End Select
  End Sub ' GUI_ButtonClick

  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    Call GUI_ReportParamsX(Me.Grid, ExcelData, Me.Text, GUI_GetPrintDateTime())

    ExcelData.SetColumnFormat(EXCEL_COLUMN_CURRENCY, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.CURRENCY, 2)

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    If Not m_show_only_stock Then
      PrintData.FilterHeaderWidth(1) = 4000

      PrintData.SetFilter(ef_cg_session_name.Text, m_session_detail.m_session_name)
      PrintData.SetFilter(ef_cg_cage_session_status.Text, ef_cg_cage_session_status.Value)
      PrintData.SetFilter(ef_cg_cage_opening_date.Text, ef_cg_cage_opening_date.Value)
      PrintData.SetFilter(ef_cg_cage_opening_user.Text, ef_cg_cage_opening_user.Value)
      PrintData.SetFilter(ef_cg_cage_closing_date.Text, ef_cg_cage_closing_date.Value)
      PrintData.SetFilter(ef_cg_cage_closing_user.Text, ef_cg_cage_closing_user.Value)
      PrintData.SetFilter(dtp_working_day.Text, dtp_working_day.Value)
      PrintData.SetFilter(Me.uc_checked_list.GroupBoxText & ": ", m_selected_source_destinations)
    End If

  End Sub ' GUI_ReportFilter

  ' PURPOSE : Implement the sql query
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _str_sql As StringBuilder
    Dim _str_query_selected_list_ids As String

    Call CreateTotalsTable(m_form_mode)

    _str_sql = New StringBuilder()

    _str_query_selected_list_ids = GetListIdsFromCheckedList()

    Select Case m_form_mode
      Case FORM_MODE.SHOW_STOCK

      Case Else

        _str_sql.AppendLine("       SELECT   X.cmd_iso_code                                                                                                ")
        _str_sql.AppendLine("              , x.chs_name                                                                                                    ")
        _str_sql.AppendLine("              , x.ch_name                                                                                                     ")
        _str_sql.AppendLine("              , x.ch_drawing                                                                                                  ")
        _str_sql.AppendLine("              , X.cmd_denomination                                                                                            ")
        _str_sql.AppendLine("              , X.cmd_quantity                                                                                                ")
        _str_sql.AppendLine("              , cm.cgm_type                                                                                                   ")
        _str_sql.AppendLine("              , cm.cgm_status                                                                                                 ")
        _str_sql.AppendLine("              , X.cgm_movement_id                                                                                             ")
        _str_sql.AppendLine("              , SUM(X.cmd_openings) AS cmd_openings                                                                           ")
        _str_sql.AppendLine("              , SUM(X.cmd_deposits) AS cmd_deposits                                                                           ")
        _str_sql.AppendLine("              , SUM(X.cmd_withdrawals) AS cmd_withdrawals                                                                     ")
        _str_sql.AppendLine("              , SUM(X.cmd_difference) AS cmd_difference                                                                       ")
        _str_sql.AppendLine("              , SUM(X.cmd_lost_quadrature) AS cmd_lost_quadrature                                                             ")
        _str_sql.AppendLine("              , SUM(X.cmd_gain_quadrature) AS cmd_gain_quadrature                                                             ")
        _str_sql.AppendLine("              , SUM(X.cmd_closing) AS cmd_closing                                                                             ")
        _str_sql.AppendLine("              , X.cmd_denomination_real                                                                                       ")
        _str_sql.AppendLine("              , X.cmd_cage_currency_type                                                                                      ")
        _str_sql.AppendLine("       INTO     #TMP_CAGE_STOCK                                                                                               ")
        _str_sql.AppendLine("         FROM   (                                                                                                             ")
        _str_sql.AppendLine("                SELECT   cmd_iso_code                                                                                         ")
        _str_sql.AppendLine("                              , ISNULL(chs_name, '') AS chs_name                                                              ")
        _str_sql.AppendLine("                              , ISNULL(ch_name, '') AS ch_name                                                                ")
        _str_sql.AppendLine("                              , ISNULL(ch_drawing, '') AS ch_drawing                                                          ")
        _str_sql.AppendLine("                              , cmd_denomination                                                                              ")
        _str_sql.AppendLine("                              , cmd_quantity                                                                                  ")
        _str_sql.AppendLine("                              , cgm_movement_id                                                                               ")
        _str_sql.AppendLine("                              , CASE                                                                                          ")
        _str_sql.AppendLine("                                    WHEN cgm_type = 102                                                                       ")
        _str_sql.AppendLine("                                        THEN ISNULL(SUM(CASE                                                                  ")
        _str_sql.AppendLine("                                                                       WHEN cmd_cage_currency_type = " + Convert.ToString(CageCurrencyType.ChipsColor))
        _str_sql.AppendLine("                                                                           THEN cmd_quantity                                  ")
        _str_sql.AppendLine("                                                                       WHEN cmd_quantity < 0                                  ")
        _str_sql.AppendLine("                                                                           THEN cmd_denomination                              ")
        _str_sql.AppendLine("                                                                       ELSE cmd_quantity * cmd_denomination                   ")
        _str_sql.AppendLine("                                                                       END), 0)                                               ")
        _str_sql.AppendLine("                                    ELSE 0                                                                                    ")
        _str_sql.AppendLine("                                    END AS cmd_openings                                                                       ")
        _str_sql.AppendLine("                              , CASE                                                                                          ")
        _str_sql.AppendLine("                                    WHEN (cgm_type >= 100 OR (cgm_status = 1)) and cgm_type not IN(2, 102, 99, 199)           ")
        _str_sql.AppendLine("                                        THEN ISNULL(SUM(CASE                                                                  ")
        _str_sql.AppendLine("                                                                       WHEN cmd_cage_currency_type = " + Convert.ToString(CageCurrencyType.ChipsColor))
        _str_sql.AppendLine("                                                                           THEN cmd_quantity                                  ")
        _str_sql.AppendLine("                                                                       WHEN cmd_quantity < 0                                  ")
        _str_sql.AppendLine("                                                                           THEN cmd_denomination                              ")
        _str_sql.AppendLine("                                                                       ELSE cmd_quantity * cmd_denomination                   ")
        _str_sql.AppendLine("                                                                       END), 0)                                               ")
        _str_sql.AppendLine("                                    ELSE 0                                                                                    ")
        _str_sql.AppendLine("                                    END AS cmd_deposits                                                                       ")
        _str_sql.AppendLine("                              , CASE                                                                                          ")
        _str_sql.AppendLine("                                    WHEN (cgm_type < 100 OR (cgm_status = 1)) and cgm_type not IN(2, 102, 99, 199)            ")
        _str_sql.AppendLine("                                        THEN ISNULL(SUM(CASE                                                                  ")
        _str_sql.AppendLine("                                                                       WHEN cmd_cage_currency_type = " + Convert.ToString(CageCurrencyType.ChipsColor))
        _str_sql.AppendLine("                                                                           THEN cmd_quantity                                  ")
        _str_sql.AppendLine("                                                                       WHEN cmd_quantity < 0                                  ")
        _str_sql.AppendLine("                                                                           THEN cmd_denomination                              ")
        _str_sql.AppendLine("                                                                       ELSE cmd_quantity * cmd_denomination                   ")
        _str_sql.AppendLine("                                                                       END), 0)                                               ")
        _str_sql.AppendLine("                                    ELSE 0                                                                                    ")
        _str_sql.AppendLine("                                    END AS cmd_withdrawals                                                                    ")

        _str_sql.AppendLine("                              , CASE                                                                                          ")
        _str_sql.AppendLine("                                 WHEN cgm_status <> 1                                                                         ")
        _str_sql.AppendLine("                                   THEN CASE                                                                                  ")
        _str_sql.AppendLine("                                    WHEN cgm_type >= 100                                                                      ")
        _str_sql.AppendLine("                                        THEN ISNULL(SUM(CASE                                                                  ")
        _str_sql.AppendLine("                                                                       WHEN cmd_cage_currency_type = " + Convert.ToString(CageCurrencyType.ChipsColor))
        _str_sql.AppendLine("                                                                           THEN cmd_quantity                                  ")
        _str_sql.AppendLine("                                                                       WHEN cmd_quantity < 0                                  ")
        _str_sql.AppendLine("                                                                           THEN cmd_denomination                              ")
        _str_sql.AppendLine("                                                                       ELSE cmd_quantity * cmd_denomination                   ")
        _str_sql.AppendLine("                                                                       END), 0)                                               ")
        _str_sql.AppendLine("                                    ELSE - 1 * ISNULL(SUM(CASE                                                                ")
        _str_sql.AppendLine("                                                                       WHEN cmd_cage_currency_type = " + Convert.ToString(CageCurrencyType.ChipsColor))
        _str_sql.AppendLine("                                                                           THEN cmd_quantity                                  ")
        _str_sql.AppendLine("                                                                          WHEN cmd_quantity < 0                               ")
        _str_sql.AppendLine("                                                                             THEN cmd_denomination                            ")
        _str_sql.AppendLine("                                                                          ELSE cmd_quantity * cmd_denomination                ")
        _str_sql.AppendLine("                                                                          END), 0)                                            ")
        _str_sql.AppendLine("                                    END                                                                                       ")
        _str_sql.AppendLine("                                 ELSE 0                                                                                       ")
        _str_sql.AppendLine("                                END AS cmd_difference                                                                         ")

        _str_sql.AppendLine("                              , CASE                                                                                          ")
        _str_sql.AppendLine("                                    WHEN cgm_type = 99                                                                        ")
        _str_sql.AppendLine("                                        THEN ISNULL(SUM(CASE                                                                  ")
        _str_sql.AppendLine("                                                                       WHEN cmd_cage_currency_type = " + Convert.ToString(CageCurrencyType.ChipsColor))
        _str_sql.AppendLine("                                                                           THEN cmd_quantity                                  ")
        _str_sql.AppendLine("                                                                       WHEN cmd_quantity < 0                                  ")
        _str_sql.AppendLine("                                                                           THEN cmd_denomination                              ")
        _str_sql.AppendLine("                                                                       ELSE cmd_quantity * cmd_denomination                   ")
        _str_sql.AppendLine("                                                                       END), 0)                                               ")
        _str_sql.AppendLine("                                    ELSE 0                                                                                    ")
        _str_sql.AppendLine("                                    END AS cmd_lost_quadrature                                                                ")
        _str_sql.AppendLine("                              , CASE                                                                                          ")
        _str_sql.AppendLine("                                    WHEN cgm_type = 199                                                                       ")
        _str_sql.AppendLine("                                        THEN ISNULL(SUM(CASE                                                                  ")
        _str_sql.AppendLine("                                                                       WHEN cmd_cage_currency_type = " + Convert.ToString(CageCurrencyType.ChipsColor))
        _str_sql.AppendLine("                                                                           THEN cmd_quantity                                  ")
        _str_sql.AppendLine("                                                                       WHEN cmd_quantity < 0                                  ")
        _str_sql.AppendLine("                                                                           THEN cmd_denomination                              ")
        _str_sql.AppendLine("                                                                       ELSE cmd_quantity * cmd_denomination                   ")
        _str_sql.AppendLine("                                                                       END), 0)                                               ")
        _str_sql.AppendLine("                                    ELSE 0                                                                                    ")
        _str_sql.AppendLine("                                    END AS cmd_gain_quadrature                                                                ")
        _str_sql.AppendLine("                                                                                                                              ")
        _str_sql.AppendLine("                              , CASE                                                                                          ")
        _str_sql.AppendLine("                                    WHEN cgm_type = 2                                                                         ")
        _str_sql.AppendLine("                                        THEN ISNULL(SUM(CASE                                                                  ")
        _str_sql.AppendLine("                                                                       WHEN cmd_cage_currency_type = " + Convert.ToString(CageCurrencyType.ChipsColor))
        _str_sql.AppendLine("                                                                           THEN cmd_quantity                                  ")
        _str_sql.AppendLine("                                                                       WHEN cmd_quantity < 0                                  ")
        _str_sql.AppendLine("                                                                           THEN cmd_denomination                              ")
        _str_sql.AppendLine("                                                                       ELSE cmd_quantity * cmd_denomination                   ")
        _str_sql.AppendLine("                                                                       END), 0)                                               ")
        _str_sql.AppendLine("                                    ELSE 0                                                                                    ")
        _str_sql.AppendLine("                                    END AS cmd_closing                                                                        ")
        _str_sql.AppendLine("                       , CASE                                                                                                 ")
        _str_sql.AppendLine("                         WHEN cmd_quantity < 0                                                                                ")
        _str_sql.AppendLine("                           THEN cmd_quantity                                                                                  ")
        _str_sql.AppendLine("                           ELSE cmd_denomination                                                                              ")
        _str_sql.AppendLine("                         END as cmd_denomination_real                                                                         ")
        _str_sql.AppendLine("                    ,CASE WHEN cmd_quantity > 0 THEN isnull(cmd_cage_currency_type,0) ELSE 99 END AS cmd_cage_currency_type   ")
        _str_sql.AppendLine("                  FROM   cage_movements                                                                                       ")
        _str_sql.AppendLine("            INNER JOIN   cage_movement_details                                                                                ")
        _str_sql.AppendLine("                           ON   cage_movements.cgm_movement_id = cage_movement_details.cmd_movement_id                        ")
        _str_sql.AppendLine("            LEFT JOIN   chips                                                                                                 ")
        _str_sql.AppendLine("                           ON   cage_movement_details.cmd_chip_id = chips.ch_chip_id                                          ")
        _str_sql.AppendLine("            LEFT JOIN   chips_sets_chips                                                                                      ")
        _str_sql.AppendLine("                           ON   chips_sets_chips.csc_chip_id = chips.ch_chip_id                                               ")
        _str_sql.AppendLine("            LEFT JOIN   chips_sets                                                                                            ")
        _str_sql.AppendLine("                           ON   chips_sets.chs_chip_set_id = chips_sets_chips.csc_set_id                                      ")
        _str_sql.AppendLine("                 WHERE   cgm_cage_session_id = " & Me.m_session_detail.m_session_id)
        _str_sql.AppendLine("                 GROUP BY   cmd_iso_code                                                                                      ")
        _str_sql.AppendLine("                              , chs_name                                                                                      ")
        _str_sql.AppendLine("                              , ch_name                                                                                       ")
        _str_sql.AppendLine("                              , ch_drawing                                                                                    ")
        _str_sql.AppendLine("                              , cmd_denomination                                                                              ")
        _str_sql.AppendLine("                              , cgm_type                                                                                      ")
        _str_sql.AppendLine("                              , cgm_status                                                                                    ")
        _str_sql.AppendLine("                              , cgm_movement_id                                                                               ")
        _str_sql.AppendLine("                              , cmd_quantity                                                                                  ")
        _str_sql.AppendLine("                              , cmd_cage_currency_type                                                                        ")
        _str_sql.AppendLine("                                                                                                                              ")
        _str_sql.AppendLine("                         ) AS X                                                                                               ")
        _str_sql.AppendLine("   INNER JOIN   cage_movements cm                                                                                             ")
        _str_sql.AppendLine("           ON   cm.cgm_movement_id = X.cgm_movement_id                                                                        ")
        _str_sql.AppendLine("        WHERE   cm.cgm_type <> 200                                                                                                ")
        _str_sql.AppendLine("     GROUP BY   X.cmd_iso_code                                                                                                ")
        _str_sql.AppendLine("                 , x.chs_name                                                                                                 ")
        _str_sql.AppendLine("                 , x.ch_name                                                                                                  ")
        _str_sql.AppendLine("                 , x.ch_drawing                                                                                               ")
        _str_sql.AppendLine("                 , x.cmd_denomination_real                                                                                    ")
        _str_sql.AppendLine("                 , cm.cgm_type                                                                                                ")
        _str_sql.AppendLine("                 , cm.cgm_status                                                                                              ")
        _str_sql.AppendLine("                 , X.cgm_movement_id                                                                                          ")
        _str_sql.AppendLine("                 , X.cmd_quantity                                                                                             ")
        _str_sql.AppendLine("                 , x.cmd_denomination                                                                                         ")
        _str_sql.AppendLine("                 , x.cmd_cage_currency_type                                                                                   ")
        _str_sql.AppendLine("                                                                                                                              ")
        _str_sql.AppendLine("     ORDER BY   X.cmd_iso_code                                                                                                ")
        _str_sql.AppendLine("                 , x.cmd_cage_currency_type ASC                                                                               ")
        _str_sql.AppendLine("                       , CASE WHEN x.cmd_cage_currency_type IN (" + Convert.ToString(CageCurrencyType.ChipsRedimible) + ",   " +
                                                                                                 Convert.ToString(CageCurrencyType.ChipsNoRedimible) + ", " +
                                                                                                 Convert.ToString(CageCurrencyType.ChipsColor) + ")        ")
        _str_sql.AppendLine("                       THEN x.chs_name END ASC                                                                                ")
        _str_sql.AppendLine("                   , CASE WHEN x.cmd_cage_currency_type IN (" + Convert.ToString(CageCurrencyType.ChipsColor) + ")            ")
        _str_sql.AppendLine("                       THEN x.ch_name END ASC                                                                                 ")
        _str_sql.AppendLine("              , X.cmd_denomination_real DESC                                                                                  ")
        _str_sql.AppendLine("                 , cm.cgm_type DESC                                                                                           ")
        _str_sql.AppendLine("                 , X.cgm_movement_id                                                                                          ")
        _str_sql.AppendLine("                 , X.cmd_quantity                                                                                             ")

        ' The next one is for the Stackers / Terminals collection
        _str_sql.AppendLine("   INSERT INTO #TMP_CAGE_STOCK                                                                                                ")
        _str_sql.AppendLine("   SELECT X.*                                                                                                                 ")
        _str_sql.AppendLine("   FROM                                                                                                                       ")
        _str_sql.AppendLine("   (                                                                                                                          ")
        If Not WSI.Common.Misc.IsFloorDualCurrencyEnabled() Then
          _str_sql.AppendLine("   SELECT '" & CurrencyExchange.GetNationalCurrency() & "' AS cmd_iso_code                                                  ")
        Else
          _str_sql.AppendLine("   SELECT       te_iso_code AS cmd_iso_code                                                   ")
        End If
        _str_sql.AppendLine("          ,'' AS chs_name                                                                                                     ")
        _str_sql.AppendLine("          ,'' AS ch_name                                                                                                      ")
        _str_sql.AppendLine("          ,'' AS ch_drawing                                                                                                   ")
        _str_sql.AppendLine("          ,(CASE WHEN ISNULL(mcd_face_value,0) < 0 THEN  mcd_num_collected ELSE mcd_face_value END) AS cmd_denomination       ")
        _str_sql.AppendLine("          ,(CASE WHEN ISNULL(mcd_face_value,0) < 0 THEN  mcd_face_value ELSE mcd_num_collected END) AS cmd_quantity           ")
        _str_sql.AppendLine("          ,cgm_type                                                                                                           ")
        _str_sql.AppendLine("          ,cgm_status                                                                                                         ")
        _str_sql.AppendLine("          ,cgm_movement_id                                                                                                    ")
        _str_sql.AppendLine("          ,0 AS cmd_openings                                                                                                  ")
        _str_sql.AppendLine("          ,(                                                                                                                  ")
        _str_sql.AppendLine("              CASE                                                                                                            ")
        _str_sql.AppendLine("                WHEN ISNULL(mcd_face_value,0) < 0 THEN mcd_num_collected                                                      ")
        _str_sql.AppendLine("                ELSE (ISNULL(mcd_face_value,0) * ISNULL(mcd_num_collected,0)) END                                             ")
        _str_sql.AppendLine("           ) AS cmd_deposits                                                                                                  ")
        _str_sql.AppendLine("          ,0 AS cmd_withdrawals                                                                                               ")
        _str_sql.AppendLine("          ,(                                                                                                                  ")
        _str_sql.AppendLine("              CASE                                                                                                            ")
        _str_sql.AppendLine("                WHEN ISNULL(mcd_face_value,0) < 0 THEN mcd_num_collected                                                      ")
        _str_sql.AppendLine("                ELSE (ISNULL(mcd_face_value,0) * ISNULL(mcd_num_collected,0)) END                                             ")
        _str_sql.AppendLine("           ) AS cmd_difference                                                                                                ")
        _str_sql.AppendLine("          ,0 AS cmd_lost_quadrature                                                                                           ")
        _str_sql.AppendLine("          ,0 AS cmd_gain_quadrature                                                                                           ")
        _str_sql.AppendLine("          ,0 AS cmd_closing                                                                                                   ")
        _str_sql.AppendLine("          ,mcd_face_value AS cmd_denomination_real                                                                            ")
        _str_sql.AppendLine("          ,mcd_cage_currency_type AS cmd_cage_currency_type                                                                                       ")
        _str_sql.AppendLine("   FROM money_collection_details                                                                                              ")
        _str_sql.AppendLine("   inner JOIN money_collections ON money_collections.mc_collection_id = money_collection_details.mcd_collection_id            ")
        _str_sql.AppendLine("   INNER JOIN cage_movements ON mc_collection_id = cage_movements.cgm_mc_collection_id                                        ")
        If WSI.Common.Misc.IsFloorDualCurrencyEnabled() Then
          _str_sql.AppendLine("    INNER JOIN    terminals ON te_terminal_id = cgm_terminal_cashier_id                         ")
        End If
        _str_sql.AppendLine("   WHERE    cage_movements.cgm_cage_session_id = " & Me.m_session_detail.m_session_id)
        _str_sql.AppendLine("     AND    cage_movements.cgm_type = " & Cage.CageOperationType.FromTerminal)
        _str_sql.AppendLine("     AND    ISNULL(mcd_num_collected,0) > 0                                                                                   ")
        _str_sql.AppendLine("     AND    MCD_FACE_VALUE <> " & WSI.Common.Cage.COINS_COLLECTION_CODE)
        _str_sql.AppendLine("   ) AS X                                                                                                                     ")
        _str_sql.AppendLine("   ORDER BY   X.cmd_iso_code                                                                                                  ")
        _str_sql.AppendLine("                       , CASE WHEN x.cmd_cage_currency_type IN (" + Convert.ToString(CageCurrencyType.ChipsRedimible) + ",   " +
                                                                                                 Convert.ToString(CageCurrencyType.ChipsNoRedimible) + ", " +
                                                                                                 Convert.ToString(CageCurrencyType.ChipsColor) + ")        ")
        _str_sql.AppendLine("                       THEN x.chs_name END ASC                                                                                ")
        _str_sql.AppendLine("                   , CASE WHEN x.cmd_cage_currency_type IN (" + Convert.ToString(CageCurrencyType.ChipsColor) + ")            ")
        _str_sql.AppendLine("                       THEN x.ch_name END ASC                                                                                 ")
        _str_sql.AppendLine("                   , X.cmd_denomination_real DESC                                                                             ")
        _str_sql.AppendLine("                       , X.cgm_type DESC                                                                                      ")
        _str_sql.AppendLine("                       , X.cgm_movement_id                                                                                    ")
        _str_sql.AppendLine("                       , X.cmd_quantity                                                                                       ")

        _str_sql.AppendLine("   SELECT * FROM #TMP_CAGE_STOCK AS X                                                                                         ")
        If Not String.IsNullOrEmpty(_str_query_selected_list_ids) Then
          _str_sql.AppendLine("         WHERE X.cgm_type IN (" & _str_query_selected_list_ids & ")                                                         ")
        End If
        _str_sql.AppendLine("   ORDER BY   X.cmd_iso_code                                                                                                  ")
        _str_sql.AppendLine("                       , x.cmd_cage_currency_type ASC                                                                         ")
        _str_sql.AppendLine("                       , CASE WHEN x.cmd_cage_currency_type IN (" + Convert.ToString(CageCurrencyType.ChipsRedimible) + ",   " +
                                                                                                 Convert.ToString(CageCurrencyType.ChipsNoRedimible) + ", " +
                                                                                                 Convert.ToString(CageCurrencyType.ChipsColor) + ")        ")
        _str_sql.AppendLine("                           THEN x.chs_name END ASC                                                                            ")
        _str_sql.AppendLine("                       , CASE WHEN x.cmd_cage_currency_type IN (" + Convert.ToString(CageCurrencyType.ChipsColor) + ")        ")
        _str_sql.AppendLine("                           THEN x.ch_name END ASC                                                                             ")
        _str_sql.AppendLine("                       , X.cmd_denomination_real DESC                                                                         ")
        _str_sql.AppendLine("                       , X.cgm_type DESC                                                                                      ")
        _str_sql.AppendLine("                       , X.cgm_movement_id                                                                                    ")
        _str_sql.AppendLine("                       , X.cmd_quantity                                                                                       ")

        _str_sql.AppendLine("   DROP TABLE #TMP_CAGE_STOCK                                                                                                 ")

    End Select

    Return _str_sql.ToString()

  End Function ' GUI_FilterGetSqlQuery


#End Region

#Region "Events"

  Private Sub frm_cage_session_detail_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

    If m_show_message_when_closing AndAlso m_session_detail.m_session_name <> Me.ef_cg_session_name.TextValue Then
      '"Los cambios realizados se perder�n.\n�Est� seguro que quiere salir?"
      If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, , _
         Me.ef_cg_session_name.Text) = ENUM_MB_RESULT.MB_RESULT_NO Then
        e.Cancel = True
      End If
    End If

  End Sub

#End Region   ' Events
End Class