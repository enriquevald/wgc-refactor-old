'-------------------------------------------------------------------
' Copyright � 2007-2013 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_account_card_change
' DESCRIPTION:   This screen allows to view account card change movements from cashier sessions.
'                           - for an account, card number or holder name
'                           - between dates
'                           - Options: Detailed or summarized report
' AUTHOR:        Ramon Moncl�s
' CREATION DATE: 21-OCT-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 21-OCT-2013   RMS    First version
' 14-NOV-2013   RMS    Added account filter check and search in new and old card fields to see a complete card number history.
'                      Added movement type selections for Creation, Replacement and Recycling movements.
' 16-DEC-2013   RMS    In TITO Mode virtual accounts should be hidden
' 03-APR-2014   RMS    Fixed Bug WIG-797: In card recycling, card number must be shown on old card column
' 06-MAY-2014   JBP    Added Vip client filter at reports.
' 07-MAY-2014   DRV    Fixed Bug WIG-901: changed the row order
' 07-JUL-2014   AMF    Personal Card Replacement Free
' 01-JUL-2015   FOS    New movements create and replace card
' 15-JUL-2015   DCS    Add movements for replace card in company B and adjust query
'-------------------------------------------------------------------

#Region " Imports "

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common
Imports System.Text

#End Region

Public Class frm_account_card_changes
  Inherits frm_base_sel

#Region " Private Constants "

  ' Grid sortable
  Private Const GRID_SORTABLE As Boolean = True

  ' Selector column
  Private Const GRID_COLUMN_SELECTOR As Integer = 0             ' Empty column 
  Private Const GRID_WIDTH_SELECTOR As Integer = 200            ' Empty column width 

  ' Grid Header
  Private Const GRID_HEADER_ROWS As Integer = 2

  ' Colors for total and subtotal rows
  Private Const COLOR_ROW_SUBTOTAL As GUI_CommonMisc.ENUM_GUI_COLOR = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01
  Private Const COLOR_ROW_TOTAL As GUI_CommonMisc.ENUM_GUI_COLOR = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00

  ' SubTotal Line positions for caption
  Private Const GRID_COLUMN_TOTALS_TITLE As Integer = 1

  ' Counters
  Private Const GRID_COUNTER_DEFAULT As Integer = 0

#Region " Summarized Report "

  ' Grid initialization data
  Private Const GRID_SUMMARIZED_COLUMNS As Integer = 5

  ' Columns
  Private Const GRID_SUMMARIZED_COLUMN_ACCOUNT_ID As Integer = 1
  Private Const GRID_SUMMARIZED_COLUMN_ACCOUNT_HOLDER_NAME As Integer = 2
  Private Const GRID_SUMMARIZED_COLUMN_CHANGES As Integer = 3
  Private Const GRID_SUMMARIZED_COLUMN_RECYCLING As Integer = 4

  ' Column Widths
  Private Const GRID_SUMMARIZED_WIDTH_ACCOUNT_ID As Integer = 1800
  Private Const GRID_SUMMARIZED_WIDTH_ACCOUNT_HOLDER_NAME As Integer = 3200
  Private Const GRID_SUMMARIZED_WIDTH_CHANGES As Integer = 1325
  Private Const GRID_SUMMARIZED_WIDTH_RECYCLING As Integer = 1325

  ' SQL Columns
  Private Const SQL_SUMMARIZED_COLUMN_ACCOUNT_ID As Integer = 0
  Private Const SQL_SUMMARIZED_COLUMN_ACCOUNT_HOLDER_NAME As Integer = 1
  Private Const SQL_SUMMARIZED_COLUMN_CHANGES As Integer = 2
  Private Const SQL_SUMMARIZED_COLUMN_RECYCLING As Integer = 3

#End Region

#Region " Detailed Report "

  ' Grid initialization data
  Private Const GRID_DETAILED_COLUMNS As Integer = 11

  ' Columns
  Private Const GRID_DETAILED_COLUMN_DATE As Integer = 1
  Private Const GRID_DETAILED_COLUMN_ACCOUNT_ID As Integer = 2
  Private Const GRID_DETAILED_COLUMN_ACCOUNT_HOLDER_NAME As Integer = 3
  Private Const GRID_DETAILED_COLUMN_MOVEMENT_TYPE As Integer = 4
  Private Const GRID_DETAILED_COLUMN_NEW_CARD As Integer = 5
  Private Const GRID_DETAILED_COLUMN_OLD_CARD As Integer = 6
  Private Const GRID_DETAILED_COLUMN_CHANGES As Integer = 7
  Private Const GRID_DETAILED_COLUMN_RECYCLING As Integer = 8
  Private Const GRID_DETAILED_COLUMN_TERMINAL As Integer = 9
  Private Const GRID_DETAILED_COLUMN_USER As Integer = 10

  ' Column Widths
  Private Const GRID_DETAILED_WIDTH_DATE As Integer = 2025
  Private Const GRID_DETAILED_WIDTH_ACCOUNT_ID As Integer = 1140
  Private Const GRID_DETAILED_WIDTH_ACCOUNT_HOLDER_NAME As Integer = 2505
  Private Const GRID_DETAILED_WIDTH_MOVEMENT_TYPE As Integer = 2325
  Private Const GRID_DETAILED_WIDTH_NEW_CARD As Integer = 2295
  Private Const GRID_DETAILED_WIDTH_OLD_CARD As Integer = 2295
  Private Const GRID_DETAILED_WIDTH_CHANGES As Integer = 1320
  Private Const GRID_DETAILED_WIDTH_RECYCLING As Integer = 1215
  Private Const GRID_DETAILED_WIDTH_TERMINAL As Integer = 1500
  Private Const GRID_DETAILED_WIDTH_USER As Integer = 1305

  ' SQL Columns
  Private Const SQL_DETAILED_COLUMN_DATE As Integer = 0
  Private Const SQL_DETAILED_COLUMN_TERMINAL As Integer = 1
  Private Const SQL_DETAILED_COLUMN_USER As Integer = 2
  Private Const SQL_DETAILED_COLUMN_ACCOUNT_ID As Integer = 3
  Private Const SQL_DETAILED_COLUMN_ACCOUNT_HOLDER_NAME As Integer = 4
  Private Const SQL_DETAILED_COLUMN_NEW_CARD As Integer = 5
  Private Const SQL_DETAILED_COLUMN_MOVEMENT_TYPE As Integer = 6
  Private Const SQL_DETAILED_COLUMN_OLD_CARD As Integer = 7

#End Region

#End Region

#Region " Enumerations "

  ' Kind of report enumeration
  Private Enum EN_REPORT_TYPE
    REPORT_TYPE_SUMMARIZED = 0
    REPORT_TYPE_DETAILED = 1
  End Enum

#End Region

#Region " Structures "

  ' Structure to keep totals data
  Private Structure TYPE_TOTAL_COUNTER
    Dim changes As Integer                     ' Number of changes
    Dim recycling As Integer                   ' Number of card recycling
    Dim account_id As String                   ' Account of changes (In Subtotal)
    Dim account_holder As String               ' Account holder name (In Subtotal)
  End Structure

#End Region

#Region " Members "

  Private m_totals As TYPE_TOTAL_COUNTER           ' For query totals
  Private m_account_totals As TYPE_TOTAL_COUNTER   ' Totals by account (subtotals)

  ' Filters data
  Private m_report_type As EN_REPORT_TYPE          ' Indicates the kind of report 
  Private m_date_from As String                    ' Date From 
  Private m_date_to As String                      ' Date To
  Private m_account As String                      ' Account Id
  Private m_track_data As String                   ' Account track data (Card Number)
  Private m_holder_name As String                  ' Account Holder Name
  Private m_holder_is_vip As String                ' Account Holder Is VIP
  Private m_show_movements As String               ' Check to show movements or not
  Private m_movement_types As String               ' Selected movement types

#End Region

#Region " Overrides "

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' TITO Mode
    If WSI.Common.TITO.Utils.IsTitoMode() Then
      uc_account_sel1.InitExtendedQuery(False)
    End If

    ' Form
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2804)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(1)

    ' Date
    Me.gb_date.Text = GLB_NLS_GUI_INVOICING.GetString(201)
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Options
    Me.chk_show_movements.Text = GLB_NLS_GUI_INVOICING.GetString(316)

    ' Movements
    Me.gb_movements.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2811)

    ' Movement options
    Me.chk_include_movement_changes.Text = GLB_NLS_GUI_INVOICING.GetString(70)
    Me.chk_include_movement_created.Text = GLB_NLS_GUI_INVOICING.GetString(90)
    Me.chk_include_movement_recycled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(654)

    ' Setting Default values for filters
    Call SetDefaultValues()

    ' Grid
    Call GUI_StyleSheet()

  End Sub

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()

    Me.dtp_from.Focus()

  End Sub

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    ' Format data in the grid depending on kind of report
    Select Case m_report_type
      Case EN_REPORT_TYPE.REPORT_TYPE_DETAILED

        Return SetupRowDetailed(RowIndex, DbRow)

      Case EN_REPORT_TYPE.REPORT_TYPE_SUMMARIZED

        Return SetupRowSummarized(RowIndex, DbRow)

    End Select

  End Function

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _sb As New StringBuilder
    Dim _filters As String

    'Get the filters
    _filters = GetSqlWhere()

    ' Select depend on kind of report
    Select Case m_report_type
      Case EN_REPORT_TYPE.REPORT_TYPE_SUMMARIZED
        _sb.AppendLine(" SELECT   PivotTable.CM_ACCOUNT_ID ")
        _sb.AppendLine("        , PivotTable.AC_HOLDER_NAME ")
        _sb.AppendLine("        , [" & WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT & "]")
        _sb.AppendLine("          + [" & WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_FOR_FREE & "]")
        _sb.AppendLine("          + [" & WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_CREDIT & "]")
        _sb.AppendLine("          + [" & WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_DEBIT & "]")
        _sb.AppendLine("          + [" & WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_GENERIC & "]")
        _sb.AppendLine("          + [" & WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CHECK & "]")
        _sb.AppendLine("          + [" & WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT & "]")
        _sb.AppendLine("          + [" & WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_CREDIT & "]")
        _sb.AppendLine("          + [" & WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_DEBIT & "]")
        _sb.AppendLine("          + [" & WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_GENERIC & "]")
        _sb.AppendLine("          + [" & WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CHECK & "]")
        _sb.AppendLine("          + [" & WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_IN_POINTS & "]")
        _sb.AppendLine("        , [" & WSI.Common.CASHIER_MOVEMENT.CARD_RECYCLED & "] ")
        _sb.AppendLine("   FROM   (SELECT   CM_ACCOUNT_ID ")
        _sb.AppendLine("                  , AC_TRACK_DATA ")
        _sb.AppendLine("                  , AC_HOLDER_NAME ")
        _sb.AppendLine("                  , CM_TYPE ")
        _sb.AppendLine("             FROM   CASHIER_MOVEMENTS ")
        _sb.AppendLine("            INNER   JOIN ACCOUNTS ON AC_ACCOUNT_ID = CM_ACCOUNT_ID ")

        ' Filters
        _sb.AppendLine(_filters)

        _sb.AppendLine("          ) AS SourceTable ")

        _sb.AppendLine("  PIVOT   ( COUNT(SourceTable.CM_TYPE) FOR SourceTable.CM_TYPE IN ( [" & WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT & "]" & _
                                                                                         ", [" & WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_FOR_FREE & "]" & _
                                                                                         ", [" & WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_CREDIT & "]" & _
                                                                                         ", [" & WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_DEBIT & "]" & _
                                                                                         ", [" & WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_GENERIC & "]" & _
                                                                                         ", [" & WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CHECK & "]" & _
                                                                                         ", [" & WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT & "]" & _
                                                                                         ", [" & WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_CREDIT & "]" & _
                                                                                         ", [" & WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_DEBIT & "]" & _
                                                                                         ", [" & WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_GENERIC & "]" & _
                                                                                         ", [" & WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CHECK & "]" & _
                                                                                         ", [" & WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_IN_POINTS & "]" & _
                                                                                         ", [" & WSI.Common.CASHIER_MOVEMENT.CARD_RECYCLED & "] ) ) AS PivotTable ")

      Case EN_REPORT_TYPE.REPORT_TYPE_DETAILED
        _sb.AppendLine("  SELECT   CM_DATE")
        _sb.AppendLine("         , CT_NAME")
        _sb.AppendLine("         , CM_USER_NAME")
        _sb.AppendLine("         , CM_ACCOUNT_ID")
        _sb.AppendLine("         , AC_HOLDER_NAME")
        _sb.AppendLine("         , CM_CARD_TRACK_DATA")
        _sb.AppendLine("         , CM_TYPE")
        _sb.AppendLine("         , CM_DETAILS")
        _sb.AppendLine("    FROM   CASHIER_MOVEMENTS")
        _sb.AppendLine("    LEFT   JOIN CASHIER_TERMINALS ON CT_CASHIER_ID = CM_CASHIER_ID")
        _sb.AppendLine("    LEFT   JOIN ACCOUNTS          ON AC_ACCOUNT_ID = CM_ACCOUNT_ID")

        ' Filters
        _sb.AppendLine(_filters)

    End Select


    ' Order ang groupping
    Select Case m_report_type
      Case EN_REPORT_TYPE.REPORT_TYPE_SUMMARIZED
        _sb.AppendLine("ORDER BY   CM_ACCOUNT_ID ASC")

      Case EN_REPORT_TYPE.REPORT_TYPE_DETAILED
        _sb.AppendLine("ORDER BY   CM_ACCOUNT_ID ASC, CM_DATE DESC")

    End Select

    Return _sb.ToString()
  End Function

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Date selection check
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        ' Interval date not valid
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    ' Check fields on uc_account_sel are ok
    If Not Me.uc_account_sel1.ValidateFormat() Then
      Return False
    End If

    Return True
  End Function

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()

    ' Last subtotal
    If (m_account_totals.changes > 0) Or (m_account_totals.recycling > 0) Then
      DrawRow(m_account_totals, COLOR_ROW_SUBTOTAL)
    End If

    ' Total
    DrawRow(m_totals, COLOR_ROW_TOTAL)

  End Sub

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_ACCOUNT_CARD_CHANGES

    Call MyBase.GUI_SetFormId()

  End Sub

  ' PURPOSE : Checks out the DB row before adding it to the grid
  '              and process counters (& totals rows)
  '
  '  PARAMS :
  '     - INPUT :
  '           - DataRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the data row can be added) or False (the data row can not be added)
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    ' Process the total counters
    Call ProcessCounters(DbRow)

    Return True
  End Function

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()

    ' Reset total and subtotal data
    Call ResetTotalCounters(m_totals)
    Call ResetTotalCounters(m_account_totals)

    ' Style the grid for the current kind of report
    Call GUI_StyleSheet()

  End Sub

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS: 
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    ' Dates filter checks
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & Space(1) & GLB_NLS_GUI_AUDITOR.GetString(257), _
                        m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & Space(1) & GLB_NLS_GUI_AUDITOR.GetString(258), _
                        m_date_to)

    ' Space
    PrintData.SetFilter(String.Empty, String.Empty, True)

    ' Account selector filter
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(207), m_account)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(209), m_track_data)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(210), m_holder_name)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4802), m_holder_is_vip)

    ' Showing movements or not
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(316), m_show_movements)

    ' Detailed mode report movements
    If m_report_type = EN_REPORT_TYPE.REPORT_TYPE_DETAILED Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2460), m_movement_types)
    End If

    ' Space
    PrintData.SetFilter(String.Empty, String.Empty, True)
    PrintData.SetFilter(String.Empty, String.Empty, True)
    PrintData.SetFilter(String.Empty, String.Empty, True)

    ' Width from header filter
    PrintData.Settings.FilterHeaderWidth(1) = 1600
    PrintData.Settings.FilterHeaderWidth(2) = 1000
    PrintData.Settings.FilterHeaderWidth(3) = 1400

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_date_from = String.Empty
    m_date_to = String.Empty
    m_account = String.Empty
    m_track_data = String.Empty
    m_holder_name = String.Empty
    m_holder_is_vip = String.Empty
    m_show_movements = String.Empty
    m_movement_types = String.Empty

    ' Report Type
    m_report_type = IIf(chk_show_movements.Checked, EN_REPORT_TYPE.REPORT_TYPE_DETAILED, EN_REPORT_TYPE.REPORT_TYPE_SUMMARIZED)

    ' Dates filter checks
    If Me.dtp_from.Checked Then
      m_date_from = GUI_FormatDate(Me.dtp_from.Value, _
                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If
    If Me.dtp_to.Checked Then
      m_date_to = GUI_FormatDate(Me.dtp_to.Value, _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Account filter
    m_account = Me.uc_account_sel1.Account
    m_track_data = Me.uc_account_sel1.TrackData
    m_holder_name = Me.uc_account_sel1.Holder
    m_holder_is_vip = Me.uc_account_sel1.HolderIsVip

    ' Show movements
    If Me.chk_show_movements.Checked Then
      m_show_movements = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278) ' S�

      ' Movement types
      If Not chk_include_movement_created.Checked And Not chk_include_movement_changes.Checked And Not chk_include_movement_recycled.Checked Then
        ' If no movement selected, then all selected
        m_movement_types = GLB_NLS_GUI_INVOICING.GetString(90) & ", " & GLB_NLS_GUI_INVOICING.GetString(70) & ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(654)
      Else
        ' Check if created movement is selected
        If chk_include_movement_created.Checked Then
          m_movement_types &= IIf(String.IsNullOrEmpty(m_movement_types), String.Empty, ", ") & GLB_NLS_GUI_INVOICING.GetString(90)
        End If
        ' Check if change movement is selected
        If chk_include_movement_changes.Checked Then
          m_movement_types &= IIf(String.IsNullOrEmpty(m_movement_types), String.Empty, ", ") & GLB_NLS_GUI_INVOICING.GetString(70)
        End If
        ' Check if recycled movement is selected
        If chk_include_movement_recycled.Checked Then
          m_movement_types &= IIf(String.IsNullOrEmpty(m_movement_types), String.Empty, ", ") & GLB_NLS_GUI_PLAYER_TRACKING.GetString(654)
        End If
      End If

    Else
      m_show_movements = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279) ' No
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region

#Region " Private Functions "

  ' PURPOSE: Format the data in the grid when report kind is summarized
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: True
  Private Function SetupRowSummarized(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    ' Card Account
    If Not DbRow.IsNull(SQL_SUMMARIZED_COLUMN_ACCOUNT_ID) Then
      Me.Grid.Cell(RowIndex, GRID_SUMMARIZED_COLUMN_ACCOUNT_ID).Value = DbRow.Value(SQL_SUMMARIZED_COLUMN_ACCOUNT_ID)
    Else
      Me.Grid.Cell(RowIndex, GRID_SUMMARIZED_COLUMN_ACCOUNT_ID).Value = String.Empty
    End If

    ' Card Holder Name
    If Not DbRow.IsNull(SQL_SUMMARIZED_COLUMN_ACCOUNT_HOLDER_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_SUMMARIZED_COLUMN_ACCOUNT_HOLDER_NAME).Value = DbRow.Value(SQL_SUMMARIZED_COLUMN_ACCOUNT_HOLDER_NAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_SUMMARIZED_COLUMN_ACCOUNT_HOLDER_NAME).Value = String.Empty
    End If

    ' Changes
    If Not DbRow.IsNull(SQL_SUMMARIZED_COLUMN_CHANGES) Then
      Me.Grid.Cell(RowIndex, GRID_SUMMARIZED_COLUMN_CHANGES).Value = DbRow.Value(SQL_SUMMARIZED_COLUMN_CHANGES)
    Else
      Me.Grid.Cell(RowIndex, GRID_SUMMARIZED_COLUMN_CHANGES).Value = String.Empty
    End If

    ' Recycling
    If Not DbRow.IsNull(SQL_SUMMARIZED_COLUMN_RECYCLING) Then
      Me.Grid.Cell(RowIndex, GRID_SUMMARIZED_COLUMN_RECYCLING).Value = DbRow.Value(SQL_SUMMARIZED_COLUMN_RECYCLING)
    Else
      Me.Grid.Cell(RowIndex, GRID_SUMMARIZED_COLUMN_RECYCLING).Value = String.Empty
    End If

    Return True
  End Function

  ' PURPOSE: Format the data in the grid when report kind is detailed
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: True
  Private Function SetupRowDetailed(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    ' Date
    Me.Grid.Cell(RowIndex, GRID_DETAILED_COLUMN_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_DETAILED_COLUMN_DATE), _
                                                                             ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                             ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Movement type
    If Not DbRow.IsNull(SQL_DETAILED_COLUMN_MOVEMENT_TYPE) Then
      Select Case DbRow.Value(SQL_DETAILED_COLUMN_MOVEMENT_TYPE)
        Case WSI.Common.CASHIER_MOVEMENT.CARD_CREATION
          Me.Grid.Cell(RowIndex, GRID_DETAILED_COLUMN_MOVEMENT_TYPE).Value = GLB_NLS_GUI_INVOICING.GetString(90)

        Case WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT _
           , WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_FOR_FREE _
           , WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT
          Me.Grid.Cell(RowIndex, GRID_DETAILED_COLUMN_MOVEMENT_TYPE).Value = GLB_NLS_GUI_INVOICING.GetString(70)

        Case WSI.Common.CASHIER_MOVEMENT.CARD_RECYCLED
          Me.Grid.Cell(RowIndex, GRID_DETAILED_COLUMN_MOVEMENT_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(654)

        Case WSI.Common.CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CHECK
          Me.Grid.Cell(RowIndex, GRID_DETAILED_COLUMN_MOVEMENT_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6513)

        Case WSI.Common.CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CURRENCY_EXCHANGE
          Me.Grid.Cell(RowIndex, GRID_DETAILED_COLUMN_MOVEMENT_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6514)

        Case WSI.Common.CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_CREDIT
          Me.Grid.Cell(RowIndex, GRID_DETAILED_COLUMN_MOVEMENT_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6515)

        Case WSI.Common.CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_DEBIT
          Me.Grid.Cell(RowIndex, GRID_DETAILED_COLUMN_MOVEMENT_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6516)

        Case WSI.Common.CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_GENERIC
          Me.Grid.Cell(RowIndex, GRID_DETAILED_COLUMN_MOVEMENT_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6517)

        Case WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CHECK _
           , WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CHECK
          Me.Grid.Cell(RowIndex, GRID_DETAILED_COLUMN_MOVEMENT_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6508)

        Case WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_CREDIT _
           , WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_CREDIT
          Me.Grid.Cell(RowIndex, GRID_DETAILED_COLUMN_MOVEMENT_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6510)

        Case WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_DEBIT _
           , WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_DEBIT
          Me.Grid.Cell(RowIndex, GRID_DETAILED_COLUMN_MOVEMENT_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6511)

        Case WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_GENERIC _
           , WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_GENERIC
          Me.Grid.Cell(RowIndex, GRID_DETAILED_COLUMN_MOVEMENT_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6512)
      End Select
    Else
      Me.Grid.Cell(RowIndex, GRID_DETAILED_COLUMN_MOVEMENT_TYPE).Value = String.Empty
    End If

    ' Terminal
    If Not DbRow.IsNull(SQL_DETAILED_COLUMN_TERMINAL) Then
      Me.Grid.Cell(RowIndex, GRID_DETAILED_COLUMN_TERMINAL).Value = DbRow.Value(SQL_DETAILED_COLUMN_TERMINAL)
    Else
      Me.Grid.Cell(RowIndex, GRID_DETAILED_COLUMN_TERMINAL).Value = String.Empty
    End If

    ' User
    If Not DbRow.IsNull(SQL_DETAILED_COLUMN_USER) Then
      Me.Grid.Cell(RowIndex, GRID_DETAILED_COLUMN_USER).Value = DbRow.Value(SQL_DETAILED_COLUMN_USER)
    Else
      Me.Grid.Cell(RowIndex, GRID_DETAILED_COLUMN_USER).Value = String.Empty
    End If

    ' Card Account
    If Not DbRow.IsNull(SQL_DETAILED_COLUMN_ACCOUNT_ID) Then
      Me.Grid.Cell(RowIndex, GRID_DETAILED_COLUMN_ACCOUNT_ID).Value = DbRow.Value(SQL_DETAILED_COLUMN_ACCOUNT_ID)
    Else
      Me.Grid.Cell(RowIndex, GRID_DETAILED_COLUMN_ACCOUNT_ID).Value = String.Empty
    End If

    ' Card Holder Name
    If Not DbRow.IsNull(SQL_DETAILED_COLUMN_ACCOUNT_HOLDER_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_DETAILED_COLUMN_ACCOUNT_HOLDER_NAME).Value = DbRow.Value(SQL_DETAILED_COLUMN_ACCOUNT_HOLDER_NAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_DETAILED_COLUMN_ACCOUNT_HOLDER_NAME).Value = String.Empty
    End If

    ' Showing card numbers depending on movement type
    Select Case DbRow.Value(SQL_DETAILED_COLUMN_MOVEMENT_TYPE)
      Case WSI.Common.CASHIER_MOVEMENT.CARD_CREATION _
      , WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT _
      , WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT _
      , WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_FOR_FREE _
      , WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CHECK _
      , WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CHECK _
      , WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_CREDIT _
      , WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_CREDIT _
      , WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_DEBIT _
      , WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_DEBIT _
      , WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_GENERIC _
      , WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_GENERIC

        ' Card Number
        If Not DbRow.IsNull(SQL_DETAILED_COLUMN_NEW_CARD) Then
          Me.Grid.Cell(RowIndex, GRID_DETAILED_COLUMN_NEW_CARD).Value = DbRow.Value(SQL_DETAILED_COLUMN_NEW_CARD)
        Else
          Me.Grid.Cell(RowIndex, GRID_DETAILED_COLUMN_NEW_CARD).Value = String.Empty
        End If

        ' Old Card Number
        Me.Grid.Cell(RowIndex, GRID_DETAILED_COLUMN_OLD_CARD).Value = String.Empty
        If Not DbRow.IsNull(SQL_DETAILED_COLUMN_OLD_CARD) AndAlso _
           DbRow.Value(SQL_DETAILED_COLUMN_OLD_CARD) <> WSI.Common.CardData.RECYCLED_TRACK_DATA Then
          Me.Grid.Cell(RowIndex, GRID_DETAILED_COLUMN_OLD_CARD).Value = DbRow.Value(SQL_DETAILED_COLUMN_OLD_CARD)
        End If

      Case WSI.Common.CASHIER_MOVEMENT.CARD_RECYCLED
        ' In this case we show card number in old card column, because new card goes "blank"

        ' Card Number
        Me.Grid.Cell(RowIndex, GRID_DETAILED_COLUMN_NEW_CARD).Value = String.Empty

        ' Old Card Number
        If Not DbRow.IsNull(SQL_DETAILED_COLUMN_NEW_CARD) Then
          Me.Grid.Cell(RowIndex, GRID_DETAILED_COLUMN_OLD_CARD).Value = DbRow.Value(SQL_DETAILED_COLUMN_NEW_CARD)
        Else
          Me.Grid.Cell(RowIndex, GRID_DETAILED_COLUMN_OLD_CARD).Value = String.Empty
        End If

    End Select

    Return True
  End Function

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the
  '          main SQL Query.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: A String with the WHERE clausule for the query
  Private Function GetSqlWhere() As String
    Dim _account_filter As String
    Dim _movements As String
    Dim _sb As New StringBuilder

    _movements = String.Empty
    Select Case Me.m_report_type
      Case EN_REPORT_TYPE.REPORT_TYPE_DETAILED

        If Not chk_include_movement_created.Checked And Not chk_include_movement_changes.Checked And Not chk_include_movement_recycled.Checked Then
          ' If no movement selected, then all selected
          _movements = WSI.Common.CASHIER_MOVEMENT.CARD_CREATION & _
                  ", " & WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT & _
                  ", " & WSI.Common.CASHIER_MOVEMENT.CARD_RECYCLED & _
                  ", " & WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_FOR_FREE & _
                  ", " & WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_CREDIT & _
                  ", " & WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_DEBIT & _
                  ", " & WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_GENERIC & _
                  ", " & WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CHECK & _
                  ", " & WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT & _
                  ", " & WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_CREDIT & _
                  ", " & WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_DEBIT & _
                  ", " & WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_GENERIC & _
                  ", " & WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CHECK & _
                  ", " & WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_IN_POINTS
        Else
          ' Check if change movement is selected
          If chk_include_movement_changes.Checked Then
            _movements &= IIf(String.IsNullOrEmpty(_movements), String.Empty, ", ") & _
                         WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT & _
                  ", " & WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_FOR_FREE & _
                  ", " & WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_CREDIT & _
                  ", " & WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_DEBIT & _
                  ", " & WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_GENERIC & _
                  ", " & WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CHECK & _
                  ", " & WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT & _
                  ", " & WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_CREDIT & _
                  ", " & WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_DEBIT & _
                  ", " & WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_GENERIC & _
                  ", " & WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CHECK & _
                  ", " & WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_IN_POINTS
          End If
          ' Check if created movement is selected
          If chk_include_movement_created.Checked Then
            _movements &= IIf(String.IsNullOrEmpty(_movements), String.Empty, ", ") & WSI.Common.CASHIER_MOVEMENT.CARD_CREATION
          End If
          ' Check if recycled movement is selected
          If chk_include_movement_recycled.Checked Then
            _movements &= IIf(String.IsNullOrEmpty(_movements), String.Empty, ", ") & WSI.Common.CASHIER_MOVEMENT.CARD_RECYCLED
          End If
        End If

      Case EN_REPORT_TYPE.REPORT_TYPE_SUMMARIZED
        _movements = WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT & _
              ", " & WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_FOR_FREE & _
              ", " & WSI.Common.CASHIER_MOVEMENT.CARD_RECYCLED & _
              ", " & WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_CREDIT & _
              ", " & WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_DEBIT & _
              ", " & WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_GENERIC & _
              ", " & WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CHECK & _
              ", " & WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT & _
              ", " & WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_CREDIT & _
              ", " & WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_DEBIT & _
              ", " & WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_GENERIC & _
              ", " & WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CHECK & _
              ", " & WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_IN_POINTS

    End Select

    ' Join the selected movements - (FilterCheck ensures that at least one movement type is checked)
    _sb.Append("   WHERE   CM_TYPE IN ( " & _movements & " )")

    ' Filter Dates
    If Me.dtp_from.Checked = True Then
      _sb.AppendLine("           AND (CM_DATE >= " & GUI_FormatDateDB(dtp_from.Value) & ")")
    End If
    If Me.dtp_to.Checked = True Then
      _sb.AppendLine("           AND (CM_DATE < " & GUI_FormatDateDB(dtp_to.Value) & ") ")
    End If

    ' Account ID or trackdata or holder name, if trackdata search in both cards and account card
    If Not String.IsNullOrEmpty(Me.uc_account_sel1.TrackData) Then
      _sb.AppendLine("           AND (CM_CARD_TRACK_DATA = '" & Me.uc_account_sel1.TrackData & "' OR CM_DETAILS = '" & Me.uc_account_sel1.TrackData & "')")
    Else
      _account_filter = Me.uc_account_sel1.GetFilterSQL()
      If Not String.IsNullOrEmpty(_account_filter) Then
        _sb.AppendLine("           AND " & _account_filter)
      End If
    End If

    Return _sb.ToString()
  End Function

  '
  ' PURPOSE : Reset Total Counters variable
  '
  '  PARAMS :
  '     - INPUT : 
  '           - Total As TYPE_TOTAL_COUNTERS
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub ResetTotalCounters(ByRef Total As TYPE_TOTAL_COUNTER)

    With Total
      .account_holder = String.Empty
      .account_id = 0
      .changes = 0
      .recycling = 0
    End With

  End Sub

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()

    ' Common style
    With Grid

      ' Sortable yes or not
      .Sortable = GRID_SORTABLE

      ' Default Counter
      .Counter(GRID_COUNTER_DEFAULT).Visible = True

    End With

    ' Style by kind of report
    Select Case m_report_type
      Case EN_REPORT_TYPE.REPORT_TYPE_DETAILED

        Call StyleSheetDetailed()

      Case EN_REPORT_TYPE.REPORT_TYPE_SUMMARIZED

        Call StyleSheetSummarized()

    End Select

  End Sub 'GUI_StyleSheet

  ' PURPOSE: Define layout of all Main Grid Columns for detailed report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub StyleSheetDetailed()

    With Me.Grid

      ' Grid initialization
      Call .Init(GRID_DETAILED_COLUMNS, GRID_HEADER_ROWS)

      ' Empty column
      Call StyleSheetEmptyColumnSelector()

      ' Date
      .Column(GRID_DETAILED_COLUMN_DATE).Header(0).Text = String.Empty
      .Column(GRID_DETAILED_COLUMN_DATE).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(201)
      .Column(GRID_DETAILED_COLUMN_DATE).Width = GRID_DETAILED_WIDTH_DATE
      .Column(GRID_DETAILED_COLUMN_DATE).IsColumnPrintable = True
      .Column(GRID_DETAILED_COLUMN_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Account Id
      .Column(GRID_DETAILED_COLUMN_ACCOUNT_ID).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_DETAILED_COLUMN_ACCOUNT_ID).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_DETAILED_COLUMN_ACCOUNT_ID).Width = GRID_DETAILED_WIDTH_ACCOUNT_ID
      .Column(GRID_DETAILED_COLUMN_ACCOUNT_ID).IsColumnPrintable = True
      .Column(GRID_DETAILED_COLUMN_ACCOUNT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Account holder name
      .Column(GRID_DETAILED_COLUMN_ACCOUNT_HOLDER_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_DETAILED_COLUMN_ACCOUNT_HOLDER_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(794)
      .Column(GRID_DETAILED_COLUMN_ACCOUNT_HOLDER_NAME).Width = GRID_DETAILED_WIDTH_ACCOUNT_HOLDER_NAME
      .Column(GRID_DETAILED_COLUMN_ACCOUNT_HOLDER_NAME).IsColumnPrintable = True
      .Column(GRID_DETAILED_COLUMN_ACCOUNT_HOLDER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Movement type
      .Column(GRID_DETAILED_COLUMN_MOVEMENT_TYPE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2809)
      .Column(GRID_DETAILED_COLUMN_MOVEMENT_TYPE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2811)
      .Column(GRID_DETAILED_COLUMN_MOVEMENT_TYPE).Width = GRID_DETAILED_WIDTH_MOVEMENT_TYPE
      .Column(GRID_DETAILED_COLUMN_MOVEMENT_TYPE).IsColumnPrintable = True
      .Column(GRID_DETAILED_COLUMN_MOVEMENT_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' New Card
      .Column(GRID_DETAILED_COLUMN_NEW_CARD).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2809)
      .Column(GRID_DETAILED_COLUMN_NEW_CARD).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2805)
      .Column(GRID_DETAILED_COLUMN_NEW_CARD).Width = GRID_DETAILED_WIDTH_NEW_CARD
      .Column(GRID_DETAILED_COLUMN_NEW_CARD).IsColumnPrintable = True
      .Column(GRID_DETAILED_COLUMN_NEW_CARD).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Old Card
      .Column(GRID_DETAILED_COLUMN_OLD_CARD).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2809)
      .Column(GRID_DETAILED_COLUMN_OLD_CARD).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2812)
      .Column(GRID_DETAILED_COLUMN_OLD_CARD).Width = GRID_DETAILED_WIDTH_NEW_CARD
      .Column(GRID_DETAILED_COLUMN_OLD_CARD).IsColumnPrintable = True
      .Column(GRID_DETAILED_COLUMN_OLD_CARD).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Changes
      If chk_include_movement_changes.Checked Or _
        (Not chk_include_movement_changes.Checked And Not chk_include_movement_created.Checked And Not chk_include_movement_recycled.Checked) Then
        .Column(GRID_DETAILED_COLUMN_CHANGES).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2936)
        .Column(GRID_DETAILED_COLUMN_CHANGES).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2810)
        .Column(GRID_DETAILED_COLUMN_CHANGES).Width = GRID_DETAILED_WIDTH_CHANGES
        .Column(GRID_DETAILED_COLUMN_CHANGES).IsColumnPrintable = True
        .Column(GRID_DETAILED_COLUMN_CHANGES).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      Else
        .Column(GRID_DETAILED_COLUMN_CHANGES).Width = 0
        .Column(GRID_DETAILED_COLUMN_CHANGES).IsColumnPrintable = False
      End If

      ' Recycling
      If chk_include_movement_recycled.Checked Or _
        (Not chk_include_movement_changes.Checked And Not chk_include_movement_created.Checked And Not chk_include_movement_recycled.Checked) Then
        .Column(GRID_DETAILED_COLUMN_RECYCLING).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2936)
        .Column(GRID_DETAILED_COLUMN_RECYCLING).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2934)
        .Column(GRID_DETAILED_COLUMN_RECYCLING).Width = GRID_DETAILED_WIDTH_RECYCLING
        .Column(GRID_DETAILED_COLUMN_RECYCLING).IsColumnPrintable = True
        .Column(GRID_DETAILED_COLUMN_RECYCLING).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      Else
        .Column(GRID_DETAILED_COLUMN_RECYCLING).Width = 0
        .Column(GRID_DETAILED_COLUMN_RECYCLING).IsColumnPrintable = False
      End If

      ' Terminal Name
      .Column(GRID_DETAILED_COLUMN_TERMINAL).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(210)
      .Column(GRID_DETAILED_COLUMN_TERMINAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1097)
      .Column(GRID_DETAILED_COLUMN_TERMINAL).Width = GRID_DETAILED_WIDTH_TERMINAL
      .Column(GRID_DETAILED_COLUMN_TERMINAL).IsColumnPrintable = True
      .Column(GRID_DETAILED_COLUMN_TERMINAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' User Name
      .Column(GRID_DETAILED_COLUMN_USER).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(210)
      .Column(GRID_DETAILED_COLUMN_USER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1278)
      .Column(GRID_DETAILED_COLUMN_USER).Width = GRID_DETAILED_WIDTH_USER
      .Column(GRID_DETAILED_COLUMN_USER).IsColumnPrintable = True
      .Column(GRID_DETAILED_COLUMN_USER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With

  End Sub

  ' PURPOSE: Define layout of all Main Grid Columns for summarized report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub StyleSheetSummarized()

    With Me.Grid

      ' Grid initialization
      Call .Init(GRID_SUMMARIZED_COLUMNS, GRID_HEADER_ROWS)

      ' Empty column
      Call StyleSheetEmptyColumnSelector()

      ' Account Id
      .Column(GRID_SUMMARIZED_COLUMN_ACCOUNT_ID).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_SUMMARIZED_COLUMN_ACCOUNT_ID).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_SUMMARIZED_COLUMN_ACCOUNT_ID).Width = GRID_SUMMARIZED_WIDTH_ACCOUNT_ID
      .Column(GRID_SUMMARIZED_COLUMN_ACCOUNT_ID).IsColumnPrintable = True
      .Column(GRID_SUMMARIZED_COLUMN_ACCOUNT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Account holder name
      .Column(GRID_SUMMARIZED_COLUMN_ACCOUNT_HOLDER_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_SUMMARIZED_COLUMN_ACCOUNT_HOLDER_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(794)
      .Column(GRID_SUMMARIZED_COLUMN_ACCOUNT_HOLDER_NAME).Width = GRID_SUMMARIZED_WIDTH_ACCOUNT_HOLDER_NAME
      .Column(GRID_SUMMARIZED_COLUMN_ACCOUNT_HOLDER_NAME).IsColumnPrintable = True
      .Column(GRID_SUMMARIZED_COLUMN_ACCOUNT_HOLDER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Changes
      .Column(GRID_SUMMARIZED_COLUMN_CHANGES).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2936)
      .Column(GRID_SUMMARIZED_COLUMN_CHANGES).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2810)
      .Column(GRID_SUMMARIZED_COLUMN_CHANGES).Width = GRID_SUMMARIZED_WIDTH_CHANGES
      .Column(GRID_SUMMARIZED_COLUMN_CHANGES).IsColumnPrintable = True
      .Column(GRID_SUMMARIZED_COLUMN_CHANGES).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Recycling
      .Column(GRID_SUMMARIZED_COLUMN_RECYCLING).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2936)
      .Column(GRID_SUMMARIZED_COLUMN_RECYCLING).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2934)
      .Column(GRID_SUMMARIZED_COLUMN_RECYCLING).Width = GRID_SUMMARIZED_WIDTH_RECYCLING
      .Column(GRID_SUMMARIZED_COLUMN_RECYCLING).IsColumnPrintable = True
      .Column(GRID_SUMMARIZED_COLUMN_RECYCLING).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

  End Sub

  ' PURPOSE: Layout the empty column selector
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub StyleSheetEmptyColumnSelector()

    With Me.Grid

      ' Empty column
      .Column(GRID_COLUMN_SELECTOR).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_SELECTOR).Header(1).Text = String.Empty
      .Column(GRID_COLUMN_SELECTOR).Width = GRID_WIDTH_SELECTOR
      .Column(GRID_COLUMN_SELECTOR).IsColumnPrintable = False
      .Column(GRID_COLUMN_SELECTOR).HighLightWhenSelected = False

    End With

  End Sub

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Dim _closing_time As Integer
    Dim _initial_time As Date
    Dim _final_time As Date
    Dim _now As Date

    ' Date filter
    _now = WGDB.Now

    _closing_time = GetDefaultClosingTime()
    _initial_time = New DateTime(_now.Year, _now.Month, 1, _closing_time, 0, 0)
    _final_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)

    Me.dtp_from.Value = _initial_time
    Me.dtp_from.Checked = True

    Me.dtp_to.Value = _final_time
    Me.dtp_to.Checked = False

    ' Account
    With uc_account_sel1
      .Account = String.Empty
      .TrackData = String.Empty
      .Holder = String.Empty
    End With

    ' Set default kind of report
    m_report_type = EN_REPORT_TYPE.REPORT_TYPE_SUMMARIZED
    chk_show_movements.Checked = (m_report_type = EN_REPORT_TYPE.REPORT_TYPE_DETAILED)
    gb_movements.Enabled = (m_report_type = EN_REPORT_TYPE.REPORT_TYPE_DETAILED)

    ' Movements options default values
    chk_include_movement_changes.Checked = True
    chk_include_movement_created.Checked = True
    chk_include_movement_recycled.Checked = True

  End Sub 'SetDefaultValues

  ' PURPOSE : Process total counters: Draw row of totals, update and reset total counters.
  '
  '  PARAMS :
  '     - INPUT : 
  '           - DbRow As CLASS_DB_ROW
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  Private Sub ProcessCounters(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW)

    Select Case m_report_type
      Case EN_REPORT_TYPE.REPORT_TYPE_DETAILED

        If (DbRow.Value(SQL_DETAILED_COLUMN_ACCOUNT_ID) <> m_account_totals.account_id) Then

          ' another account id, draw current subtotals 
          If (m_account_totals.account_id <> 0) Then

            DrawRow(m_account_totals, COLOR_ROW_SUBTOTAL)
          End If

          ' Reset subtotal counter for next account id
          m_account_totals.account_id = DbRow.Value(SQL_DETAILED_COLUMN_ACCOUNT_ID)
          m_account_totals.account_holder = IIf(DbRow.IsNull(SQL_DETAILED_COLUMN_ACCOUNT_HOLDER_NAME), String.Empty, DbRow.Value(SQL_DETAILED_COLUMN_ACCOUNT_HOLDER_NAME))
          m_account_totals.changes = 0
          m_account_totals.recycling = 0
        End If

        ' Increment depends on movement type (Replacement or recycled) ( 28, 57 )
        Select Case DbRow.Value(SQL_DETAILED_COLUMN_MOVEMENT_TYPE)
          Case WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT _
            , WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT _
            , WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_FOR_FREE _
            , WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CHECK _
            , WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CHECK _
            , WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_CREDIT _
            , WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_CREDIT _
            , WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_DEBIT _
            , WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_DEBIT _
            , WSI.Common.CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_GENERIC _
            , WSI.Common.CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_GENERIC
            m_account_totals.changes += 1
            m_totals.changes += 1
          Case WSI.Common.CASHIER_MOVEMENT.CARD_RECYCLED
            m_account_totals.recycling += 1
            m_totals.recycling += 1
        End Select

      Case EN_REPORT_TYPE.REPORT_TYPE_SUMMARIZED

        ' Accumulate the changes per row
        m_totals.changes += DbRow.Value(SQL_SUMMARIZED_COLUMN_CHANGES)
        m_totals.recycling += DbRow.Value(SQL_SUMMARIZED_COLUMN_RECYCLING)

    End Select

  End Sub

  ' PURPOSE: Show results to screen drawing the total counter data in a new row with the specified color
  '
  '  PARAMS:
  '     - INPUT:
  '           - Provider items with data
  '           - color of row
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Function DrawRow(ByVal Total As TYPE_TOTAL_COUNTER, ByVal Color As Integer) As Boolean
    Dim _idx_row As Integer
    Dim _col_account_id As Integer
    Dim _col_account_holder As Integer
    Dim _col_changes As Integer
    Dim _col_recycling As Integer

    ' Set columns by kind of report
    Select Case m_report_type
      Case EN_REPORT_TYPE.REPORT_TYPE_DETAILED

        ' Don't show any total if only creation movement visible
        If Not Me.chk_include_movement_changes.Checked And Not Me.chk_include_movement_recycled.Checked And Me.chk_include_movement_created.Checked Then
          Return True
        End If

        _col_account_id = GRID_DETAILED_COLUMN_ACCOUNT_ID
        _col_account_holder = GRID_DETAILED_COLUMN_ACCOUNT_HOLDER_NAME
        _col_changes = GRID_DETAILED_COLUMN_CHANGES
        _col_recycling = GRID_DETAILED_COLUMN_RECYCLING

      Case EN_REPORT_TYPE.REPORT_TYPE_SUMMARIZED
        _col_account_id = GRID_SUMMARIZED_COLUMN_ACCOUNT_ID
        _col_account_holder = GRID_SUMMARIZED_COLUMN_ACCOUNT_HOLDER_NAME
        _col_changes = GRID_SUMMARIZED_COLUMN_CHANGES
        _col_recycling = GRID_SUMMARIZED_COLUMN_RECYCLING

    End Select

    ' Add the totals row & data
    With Me.Grid
      .AddRow()
      _idx_row = Me.Grid.NumRows - 1

      ' Caption in first column
      .Cell(_idx_row, GRID_COLUMN_TOTALS_TITLE).Value = IIf(Total.account_id <> 0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1046), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1047)) & ":"  ' Subtotal or Total
      .Cell(_idx_row, GRID_COLUMN_TOTALS_TITLE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' In subtotals, fill the columns with account id and holder name
      If Total.account_id <> 0 Then
        .Cell(_idx_row, _col_account_id).Value = Total.account_id
        .Cell(_idx_row, _col_account_holder).Value = Total.account_holder
      End If

      ' Total in changes column
      .Cell(_idx_row, _col_changes).Value = GUI_FormatNumber(Total.changes, 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE)
      .Cell(_idx_row, _col_changes).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Total in recycling column
      .Cell(_idx_row, _col_recycling).Value = GUI_FormatNumber(Total.recycling, 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE)
      .Cell(_idx_row, _col_recycling).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Color only when deatiled or when is TOTAL
      If (m_report_type <> EN_REPORT_TYPE.REPORT_TYPE_SUMMARIZED) Or (Total.account_id = 0) Then
        .Row(_idx_row).BackColor = GetColor(Color)
      End If

    End With

    Return True
  End Function ' DrawRow

#End Region

#Region " Events "

  ' PURPOSE: Set the kind of report to be executed and enable/disable movements groupbox
  '
  '  PARAMS:
  '     - INPUT:
  '                 - sender of event
  '                 - event data
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub chk_show_movements_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_show_movements.CheckedChanged

    gb_movements.Enabled = chk_show_movements.Checked

  End Sub

#End Region

End Class