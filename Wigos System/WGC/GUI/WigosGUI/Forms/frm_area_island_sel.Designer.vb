<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_area_island_sel
  Inherits GUI_Controls.frm_base_print

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.gb_smoking = New System.Windows.Forms.GroupBox
    Me.chk_smoking_no = New System.Windows.Forms.CheckBox
    Me.chk_smoking_yes = New System.Windows.Forms.CheckBox
    Me.dgv_areas = New System.Windows.Forms.DataGridView
    Me.lbl_areas = New System.Windows.Forms.Label
    Me.pnl_areas = New System.Windows.Forms.Panel
    Me.pnl_islands = New System.Windows.Forms.Panel
    Me.lbl_islands = New System.Windows.Forms.Label
    Me.dgv_islands = New System.Windows.Forms.DataGridView
    Me.ef_area_name = New GUI_Controls.uc_entry_field
    Me.ef_island_name = New GUI_Controls.uc_entry_field
    Me.pnl_terminals = New System.Windows.Forms.Panel
    Me.lbl_terminals = New System.Windows.Forms.Label
    Me.dgv_terminals = New System.Windows.Forms.DataGridView
    Me.uc_pr_list = New GUI_Controls.uc_provider
    Me.ef_terminal_name = New GUI_Controls.uc_entry_field
    Me.panel_grids.SuspendLayout()
    Me.panel_filter.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_smoking.SuspendLayout()
    CType(Me.dgv_areas, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.pnl_areas.SuspendLayout()
    Me.pnl_islands.SuspendLayout()
    CType(Me.dgv_islands, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.pnl_terminals.SuspendLayout()
    CType(Me.dgv_terminals, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'panel_grids
    '
    Me.panel_grids.Controls.Add(Me.pnl_islands)
    Me.panel_grids.Controls.Add(Me.pnl_terminals)
    Me.panel_grids.Controls.Add(Me.pnl_areas)
    Me.panel_grids.Location = New System.Drawing.Point(4, 220)
    Me.panel_grids.Size = New System.Drawing.Size(1148, 441)
    Me.panel_grids.TabIndex = 1
    Me.panel_grids.Controls.SetChildIndex(Me.pnl_areas, 0)
    Me.panel_grids.Controls.SetChildIndex(Me.pnl_terminals, 0)
    Me.panel_grids.Controls.SetChildIndex(Me.pnl_islands, 0)
    Me.panel_grids.Controls.SetChildIndex(Me.panel_buttons, 0)
    '
    'panel_buttons
    '
    Me.panel_buttons.Location = New System.Drawing.Point(1060, 0)
    Me.panel_buttons.Size = New System.Drawing.Size(88, 441)
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.ef_terminal_name)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Controls.Add(Me.ef_island_name)
    Me.panel_filter.Controls.Add(Me.ef_area_name)
    Me.panel_filter.Controls.Add(Me.gb_smoking)
    Me.panel_filter.Size = New System.Drawing.Size(1148, 193)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_smoking, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_area_name, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_island_name, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_terminal_name, 0)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Location = New System.Drawing.Point(4, 197)
    Me.pn_separator_line.Size = New System.Drawing.Size(1148, 23)
    Me.pn_separator_line.TabIndex = 1
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1148, 4)
    '
    'gb_smoking
    '
    Me.gb_smoking.Controls.Add(Me.chk_smoking_no)
    Me.gb_smoking.Controls.Add(Me.chk_smoking_yes)
    Me.gb_smoking.Location = New System.Drawing.Point(262, 6)
    Me.gb_smoking.Name = "gb_smoking"
    Me.gb_smoking.Size = New System.Drawing.Size(109, 64)
    Me.gb_smoking.TabIndex = 2
    Me.gb_smoking.TabStop = False
    Me.gb_smoking.Text = "xSmoking"
    '
    'chk_smoking_no
    '
    Me.chk_smoking_no.AutoSize = True
    Me.chk_smoking_no.Location = New System.Drawing.Point(28, 42)
    Me.chk_smoking_no.Name = "chk_smoking_no"
    Me.chk_smoking_no.Size = New System.Drawing.Size(41, 17)
    Me.chk_smoking_no.TabIndex = 1
    Me.chk_smoking_no.Text = "No"
    '
    'chk_smoking_yes
    '
    Me.chk_smoking_yes.AutoSize = True
    Me.chk_smoking_yes.Location = New System.Drawing.Point(28, 22)
    Me.chk_smoking_yes.Name = "chk_smoking_yes"
    Me.chk_smoking_yes.Size = New System.Drawing.Size(46, 17)
    Me.chk_smoking_yes.TabIndex = 0
    Me.chk_smoking_yes.Text = "Yes"
    '
    'dgv_areas
    '
    Me.dgv_areas.AllowUserToResizeColumns = False
    Me.dgv_areas.AllowUserToResizeRows = False
    Me.dgv_areas.BorderStyle = System.Windows.Forms.BorderStyle.None
    Me.dgv_areas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
    Me.dgv_areas.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.dgv_areas.Location = New System.Drawing.Point(0, 33)
    Me.dgv_areas.MultiSelect = False
    Me.dgv_areas.Name = "dgv_areas"
    Me.dgv_areas.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
    Me.dgv_areas.Size = New System.Drawing.Size(296, 216)
    Me.dgv_areas.TabIndex = 1
    '
    'lbl_areas
    '
    Me.lbl_areas.BackColor = System.Drawing.SystemColors.ActiveCaption
    Me.lbl_areas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_areas.Dock = System.Windows.Forms.DockStyle.Fill
    Me.lbl_areas.Font = New System.Drawing.Font("Verdana", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_areas.Location = New System.Drawing.Point(0, 0)
    Me.lbl_areas.Name = "lbl_areas"
    Me.lbl_areas.Size = New System.Drawing.Size(296, 33)
    Me.lbl_areas.TabIndex = 0
    Me.lbl_areas.Text = "xAreas"
    Me.lbl_areas.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'pnl_areas
    '
    Me.pnl_areas.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.pnl_areas.Controls.Add(Me.lbl_areas)
    Me.pnl_areas.Controls.Add(Me.dgv_areas)
    Me.pnl_areas.Location = New System.Drawing.Point(6, 93)
    Me.pnl_areas.Name = "pnl_areas"
    Me.pnl_areas.Size = New System.Drawing.Size(300, 253)
    Me.pnl_areas.TabIndex = 12
    '
    'pnl_islands
    '
    Me.pnl_islands.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.pnl_islands.Controls.Add(Me.lbl_islands)
    Me.pnl_islands.Controls.Add(Me.dgv_islands)
    Me.pnl_islands.Location = New System.Drawing.Point(318, 49)
    Me.pnl_islands.Name = "pnl_islands"
    Me.pnl_islands.Size = New System.Drawing.Size(241, 330)
    Me.pnl_islands.TabIndex = 13
    '
    'lbl_islands
    '
    Me.lbl_islands.BackColor = System.Drawing.SystemColors.ActiveCaption
    Me.lbl_islands.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_islands.Dock = System.Windows.Forms.DockStyle.Fill
    Me.lbl_islands.Font = New System.Drawing.Font("Verdana", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_islands.Location = New System.Drawing.Point(0, 0)
    Me.lbl_islands.Name = "lbl_islands"
    Me.lbl_islands.Size = New System.Drawing.Size(237, 44)
    Me.lbl_islands.TabIndex = 0
    Me.lbl_islands.Text = "xIslands in Area ---"
    Me.lbl_islands.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'dgv_islands
    '
    Me.dgv_islands.AllowUserToResizeColumns = False
    Me.dgv_islands.AllowUserToResizeRows = False
    Me.dgv_islands.BorderStyle = System.Windows.Forms.BorderStyle.None
    Me.dgv_islands.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
    Me.dgv_islands.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.dgv_islands.Location = New System.Drawing.Point(0, 44)
    Me.dgv_islands.MultiSelect = False
    Me.dgv_islands.Name = "dgv_islands"
    Me.dgv_islands.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
    Me.dgv_islands.Size = New System.Drawing.Size(237, 282)
    Me.dgv_islands.TabIndex = 1
    '
    'ef_area_name
    '
    Me.ef_area_name.DoubleValue = 0
    Me.ef_area_name.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_area_name.IntegerValue = 0
    Me.ef_area_name.IsReadOnly = False
    Me.ef_area_name.Location = New System.Drawing.Point(6, 12)
    Me.ef_area_name.Name = "ef_area_name"
    Me.ef_area_name.Size = New System.Drawing.Size(250, 25)
    Me.ef_area_name.SufixText = "Sufix Text"
    Me.ef_area_name.SufixTextVisible = True
    Me.ef_area_name.TabIndex = 0
    Me.ef_area_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_area_name.TextValue = ""
    Me.ef_area_name.TextWidth = 70
    Me.ef_area_name.Value = ""
    '
    'ef_island_name
    '
    Me.ef_island_name.DoubleValue = 0
    Me.ef_island_name.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_island_name.IntegerValue = 0
    Me.ef_island_name.IsReadOnly = False
    Me.ef_island_name.Location = New System.Drawing.Point(6, 45)
    Me.ef_island_name.Name = "ef_island_name"
    Me.ef_island_name.Size = New System.Drawing.Size(250, 25)
    Me.ef_island_name.SufixText = "Sufix Text"
    Me.ef_island_name.SufixTextVisible = True
    Me.ef_island_name.TabIndex = 1
    Me.ef_island_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_island_name.TextValue = ""
    Me.ef_island_name.TextWidth = 70
    Me.ef_island_name.Value = ""
    '
    'pnl_terminals
    '
    Me.pnl_terminals.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.pnl_terminals.Controls.Add(Me.lbl_terminals)
    Me.pnl_terminals.Controls.Add(Me.dgv_terminals)
    Me.pnl_terminals.Location = New System.Drawing.Point(571, 6)
    Me.pnl_terminals.Name = "pnl_terminals"
    Me.pnl_terminals.Size = New System.Drawing.Size(480, 429)
    Me.pnl_terminals.TabIndex = 14
    '
    'lbl_terminals
    '
    Me.lbl_terminals.BackColor = System.Drawing.SystemColors.ActiveCaption
    Me.lbl_terminals.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_terminals.Dock = System.Windows.Forms.DockStyle.Fill
    Me.lbl_terminals.Font = New System.Drawing.Font("Verdana", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_terminals.Location = New System.Drawing.Point(0, 0)
    Me.lbl_terminals.Name = "lbl_terminals"
    Me.lbl_terminals.Size = New System.Drawing.Size(476, 33)
    Me.lbl_terminals.TabIndex = 0
    Me.lbl_terminals.Text = "xTerminals in Island ---"
    Me.lbl_terminals.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'dgv_terminals
    '
    Me.dgv_terminals.AllowUserToResizeColumns = False
    Me.dgv_terminals.AllowUserToResizeRows = False
    Me.dgv_terminals.BorderStyle = System.Windows.Forms.BorderStyle.None
    Me.dgv_terminals.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
    Me.dgv_terminals.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.dgv_terminals.Location = New System.Drawing.Point(0, 33)
    Me.dgv_terminals.Name = "dgv_terminals"
    Me.dgv_terminals.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
    Me.dgv_terminals.Size = New System.Drawing.Size(476, 392)
    Me.dgv_terminals.TabIndex = 1
    '
    'uc_pr_list
    '
    Me.uc_pr_list.Location = New System.Drawing.Point(377, 3)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(394, 188)
    Me.uc_pr_list.TabIndex = 11
    '
    'ef_terminal_name
    '
    Me.ef_terminal_name.DoubleValue = 0
    Me.ef_terminal_name.IntegerValue = 0
    Me.ef_terminal_name.IsReadOnly = False
    Me.ef_terminal_name.Location = New System.Drawing.Point(6, 76)
    Me.ef_terminal_name.Name = "ef_terminal_name"
    Me.ef_terminal_name.Size = New System.Drawing.Size(250, 24)
    Me.ef_terminal_name.SufixText = "Sufix Text"
    Me.ef_terminal_name.SufixTextVisible = True
    Me.ef_terminal_name.TabIndex = 12
    Me.ef_terminal_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_terminal_name.TextValue = ""
    Me.ef_terminal_name.TextWidth = 70
    Me.ef_terminal_name.Value = ""
    '
    'frm_area_island_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1156, 665)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_area_island_sel"
    Me.Text = "frm_area_island_sel"
    Me.panel_grids.ResumeLayout(False)
    Me.panel_filter.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_smoking.ResumeLayout(False)
    Me.gb_smoking.PerformLayout()
    CType(Me.dgv_areas, System.ComponentModel.ISupportInitialize).EndInit()
    Me.pnl_areas.ResumeLayout(False)
    Me.pnl_islands.ResumeLayout(False)
    CType(Me.dgv_islands, System.ComponentModel.ISupportInitialize).EndInit()
    Me.pnl_terminals.ResumeLayout(False)
    CType(Me.dgv_terminals, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_smoking As System.Windows.Forms.GroupBox
  Friend WithEvents chk_smoking_no As System.Windows.Forms.CheckBox
  Friend WithEvents chk_smoking_yes As System.Windows.Forms.CheckBox
  Private WithEvents dgv_areas As System.Windows.Forms.DataGridView
  Friend WithEvents lbl_areas As System.Windows.Forms.Label
  Friend WithEvents pnl_islands As System.Windows.Forms.Panel
  Friend WithEvents lbl_islands As System.Windows.Forms.Label
  Friend WithEvents pnl_areas As System.Windows.Forms.Panel
  Friend WithEvents ef_area_name As GUI_Controls.uc_entry_field
  Friend WithEvents ef_island_name As GUI_Controls.uc_entry_field
  Friend WithEvents pnl_terminals As System.Windows.Forms.Panel
  Friend WithEvents lbl_terminals As System.Windows.Forms.Label
  Private WithEvents dgv_terminals As System.Windows.Forms.DataGridView
  Private WithEvents dgv_islands As System.Windows.Forms.DataGridView
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents ef_terminal_name As GUI_Controls.uc_entry_field
End Class
