﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_winup_notifications_edit
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_winup_notifications_edit))
    Me.lbl_type_section = New System.Windows.Forms.Label()
    Me.lbl_title = New System.Windows.Forms.Label()
    Me.cmb_section_type = New GUI_Controls.uc_combo()
    Me.cmb_section = New GUI_Controls.uc_combo()
    Me.ef_title = New GUI_Controls.uc_entry_field()
    Me.txt_message = New System.Windows.Forms.TextBox()
    Me.lbl_message = New System.Windows.Forms.Label()
    Me.chk_enabled = New System.Windows.Forms.CheckBox()
    Me.gb_segmentation = New System.Windows.Forms.GroupBox()
    Me.pnl_customer = New System.Windows.Forms.Panel()
    Me.btn_find_accounts = New System.Windows.Forms.Button()
    Me.uc_grid_accounts = New GUI_Controls.uc_grid()
    Me.uc_account_sel = New GUI_Controls.uc_account_sel()
    Me.pnl_filter = New System.Windows.Forms.Panel()
    Me.tab_filters = New System.Windows.Forms.TabControl()
    Me.tab_gender = New System.Windows.Forms.TabPage()
    Me.opt_gender_male = New System.Windows.Forms.RadioButton()
    Me.opt_gender_female = New System.Windows.Forms.RadioButton()
    Me.chk_by_gender = New System.Windows.Forms.CheckBox()
    Me.tab_birth_date = New System.Windows.Forms.TabPage()
    Me.Panel1 = New System.Windows.Forms.Panel()
    Me.ef_age_from1 = New GUI_Controls.uc_entry_field()
    Me.ef_age_to1 = New GUI_Controls.uc_entry_field()
    Me.chk_by_age = New System.Windows.Forms.CheckBox()
    Me.ef_age_from2 = New GUI_Controls.uc_entry_field()
    Me.opt_birth_exclude_age = New System.Windows.Forms.RadioButton()
    Me.opt_birth_include_age = New System.Windows.Forms.RadioButton()
    Me.ef_age_to2 = New GUI_Controls.uc_entry_field()
    Me.opt_birth_date_only = New System.Windows.Forms.RadioButton()
    Me.opt_birth_month_only = New System.Windows.Forms.RadioButton()
    Me.chk_by_birth_date = New System.Windows.Forms.CheckBox()
    Me.tab_created_account = New System.Windows.Forms.TabPage()
    Me.Panel3 = New System.Windows.Forms.Panel()
    Me.ef_created_account_anniversary_year_to = New GUI_Controls.uc_entry_field()
    Me.chk_by_anniversary_range_of_years = New System.Windows.Forms.CheckBox()
    Me.opt_created_account_anniversary_day_month = New System.Windows.Forms.RadioButton()
    Me.opt_created_account_anniversary_whole_month = New System.Windows.Forms.RadioButton()
    Me.ef_created_account_anniversary_year_from = New GUI_Controls.uc_entry_field()
    Me.Panel2 = New System.Windows.Forms.Panel()
    Me.opt_created_account_working_day_plus = New System.Windows.Forms.RadioButton()
    Me.opt_created_account_working_day_only = New System.Windows.Forms.RadioButton()
    Me.ef_created_account_working_day_plus = New GUI_Controls.uc_entry_field()
    Me.opt_created_account_anniversary = New System.Windows.Forms.RadioButton()
    Me.opt_created_account_date = New System.Windows.Forms.RadioButton()
    Me.chk_by_created_account = New System.Windows.Forms.CheckBox()
    Me.tab_level = New System.Windows.Forms.TabPage()
    Me.chk_only_vip = New System.Windows.Forms.CheckBox()
    Me.chk_level_anonymous = New System.Windows.Forms.CheckBox()
    Me.chk_level_04 = New System.Windows.Forms.CheckBox()
    Me.chk_level_03 = New System.Windows.Forms.CheckBox()
    Me.chk_level_02 = New System.Windows.Forms.CheckBox()
    Me.chk_level_01 = New System.Windows.Forms.CheckBox()
    Me.chk_by_level = New System.Windows.Forms.CheckBox()
    Me.tab_freq = New System.Windows.Forms.TabPage()
    Me.lbl_freq_filter_02 = New GUI_Controls.uc_text_field()
    Me.lbl_freq_filter_01 = New GUI_Controls.uc_text_field()
    Me.ef_freq_min_cash_in = New GUI_Controls.uc_entry_field()
    Me.ef_freq_min_days = New GUI_Controls.uc_entry_field()
    Me.ef_freq_last_days = New GUI_Controls.uc_entry_field()
    Me.chk_by_freq = New System.Windows.Forms.CheckBox()
    Me.opt_all_devices = New System.Windows.Forms.RadioButton()
    Me.pnl_customer_group = New System.Windows.Forms.Panel()
    Me.rtb_report = New System.Windows.Forms.RichTextBox()
    Me.btn_import = New GUI_Controls.uc_button()
    Me.pnl_not_account = New System.Windows.Forms.Panel()
    Me.opt_customer = New System.Windows.Forms.RadioButton()
    Me.opt_customer_group = New System.Windows.Forms.RadioButton()
    Me.opt_not_account = New System.Windows.Forms.RadioButton()
    Me.opt_filter = New System.Windows.Forms.RadioButton()
    Me.gb_scheduler = New System.Windows.Forms.GroupBox()
    Me.uc_schedule = New GUI_Controls.uc_schedule()
    Me.opt_schedule = New System.Windows.Forms.RadioButton()
    Me.opt_inmediate = New System.Windows.Forms.RadioButton()
    Me.btn_sent = New System.Windows.Forms.Button()
    Me.ef_url = New GUI_Controls.uc_entry_field()
    Me.opt_section = New System.Windows.Forms.RadioButton()
    Me.opt_url = New System.Windows.Forms.RadioButton()
    Me.uc_image = New GUI_Controls.uc_image()
    Me.optInformative = New System.Windows.Forms.RadioButton()
    Me.gb_goto = New System.Windows.Forms.GroupBox()
    Me.panel_data.SuspendLayout()
    Me.gb_segmentation.SuspendLayout()
    Me.pnl_customer.SuspendLayout()
    Me.pnl_filter.SuspendLayout()
    Me.tab_filters.SuspendLayout()
    Me.tab_gender.SuspendLayout()
    Me.tab_birth_date.SuspendLayout()
    Me.Panel1.SuspendLayout()
    Me.tab_created_account.SuspendLayout()
    Me.Panel3.SuspendLayout()
    Me.Panel2.SuspendLayout()
    Me.tab_level.SuspendLayout()
    Me.tab_freq.SuspendLayout()
    Me.pnl_customer_group.SuspendLayout()
    Me.gb_scheduler.SuspendLayout()
    Me.gb_goto.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_goto)
    Me.panel_data.Controls.Add(Me.uc_image)
    Me.panel_data.Controls.Add(Me.gb_scheduler)
    Me.panel_data.Controls.Add(Me.gb_segmentation)
    Me.panel_data.Controls.Add(Me.chk_enabled)
    Me.panel_data.Controls.Add(Me.lbl_message)
    Me.panel_data.Controls.Add(Me.txt_message)
    Me.panel_data.Controls.Add(Me.lbl_title)
    Me.panel_data.Controls.Add(Me.ef_title)
    Me.panel_data.Size = New System.Drawing.Size(944, 628)
    '
    'lbl_type_section
    '
    Me.lbl_type_section.Location = New System.Drawing.Point(325, 17)
    Me.lbl_type_section.Name = "lbl_type_section"
    Me.lbl_type_section.Size = New System.Drawing.Size(43, 24)
    Me.lbl_type_section.TabIndex = 23
    Me.lbl_type_section.Text = "Item"
    Me.lbl_type_section.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_title
    '
    Me.lbl_title.Location = New System.Drawing.Point(19, 130)
    Me.lbl_title.Name = "lbl_title"
    Me.lbl_title.Size = New System.Drawing.Size(85, 24)
    Me.lbl_title.TabIndex = 22
    Me.lbl_title.Text = "xTitle"
    Me.lbl_title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'cmb_section_type
    '
    Me.cmb_section_type.AllowUnlistedValues = False
    Me.cmb_section_type.AutoCompleteMode = False
    Me.cmb_section_type.IsReadOnly = False
    Me.cmb_section_type.Location = New System.Drawing.Point(367, 17)
    Me.cmb_section_type.Name = "cmb_section_type"
    Me.cmb_section_type.SelectedIndex = -1
    Me.cmb_section_type.Size = New System.Drawing.Size(231, 24)
    Me.cmb_section_type.SufixText = "Sufix Text"
    Me.cmb_section_type.SufixTextVisible = True
    Me.cmb_section_type.TabIndex = 2
    Me.cmb_section_type.TextCombo = Nothing
    Me.cmb_section_type.TextVisible = False
    Me.cmb_section_type.TextWidth = 0
    '
    'cmb_section
    '
    Me.cmb_section.AllowUnlistedValues = False
    Me.cmb_section.AutoCompleteMode = False
    Me.cmb_section.IsReadOnly = False
    Me.cmb_section.Location = New System.Drawing.Point(121, 17)
    Me.cmb_section.Name = "cmb_section"
    Me.cmb_section.SelectedIndex = -1
    Me.cmb_section.Size = New System.Drawing.Size(184, 24)
    Me.cmb_section.SufixText = "Sufix Text"
    Me.cmb_section.SufixTextVisible = True
    Me.cmb_section.TabIndex = 1
    Me.cmb_section.TextCombo = Nothing
    Me.cmb_section.TextVisible = False
    Me.cmb_section.TextWidth = 0
    '
    'ef_title
    '
    Me.ef_title.AllowDrop = True
    Me.ef_title.DoubleValue = 0.0R
    Me.ef_title.IntegerValue = 0
    Me.ef_title.IsReadOnly = False
    Me.ef_title.Location = New System.Drawing.Point(125, 129)
    Me.ef_title.Name = "ef_title"
    Me.ef_title.PlaceHolder = Nothing
    Me.ef_title.RightToLeft = System.Windows.Forms.RightToLeft.No
    Me.ef_title.Size = New System.Drawing.Size(299, 24)
    Me.ef_title.SufixText = "Sufix Text"
    Me.ef_title.SufixTextVisible = True
    Me.ef_title.TabIndex = 6
    Me.ef_title.TabStop = False
    Me.ef_title.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_title.TextValue = ""
    Me.ef_title.TextWidth = 0
    Me.ef_title.Value = ""
    Me.ef_title.ValueForeColor = System.Drawing.Color.Blue
    '
    'txt_message
    '
    Me.txt_message.Location = New System.Drawing.Point(126, 161)
    Me.txt_message.MaxLength = 1000
    Me.txt_message.Multiline = True
    Me.txt_message.Name = "txt_message"
    Me.txt_message.Size = New System.Drawing.Size(478, 103)
    Me.txt_message.TabIndex = 7
    '
    'lbl_message
    '
    Me.lbl_message.Location = New System.Drawing.Point(19, 164)
    Me.lbl_message.Name = "lbl_message"
    Me.lbl_message.Size = New System.Drawing.Size(76, 13)
    Me.lbl_message.TabIndex = 25
    Me.lbl_message.Text = "xMessage"
    Me.lbl_message.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'chk_enabled
    '
    Me.chk_enabled.AutoSize = True
    Me.chk_enabled.Location = New System.Drawing.Point(127, 270)
    Me.chk_enabled.Name = "chk_enabled"
    Me.chk_enabled.Size = New System.Drawing.Size(89, 17)
    Me.chk_enabled.TabIndex = 8
    Me.chk_enabled.Text = "xHabilitado"
    Me.chk_enabled.UseVisualStyleBackColor = True
    '
    'gb_segmentation
    '
    Me.gb_segmentation.Controls.Add(Me.pnl_customer)
    Me.gb_segmentation.Controls.Add(Me.pnl_filter)
    Me.gb_segmentation.Controls.Add(Me.opt_all_devices)
    Me.gb_segmentation.Controls.Add(Me.pnl_customer_group)
    Me.gb_segmentation.Controls.Add(Me.pnl_not_account)
    Me.gb_segmentation.Controls.Add(Me.opt_customer)
    Me.gb_segmentation.Controls.Add(Me.opt_customer_group)
    Me.gb_segmentation.Controls.Add(Me.opt_not_account)
    Me.gb_segmentation.Controls.Add(Me.opt_filter)
    Me.gb_segmentation.Location = New System.Drawing.Point(14, 299)
    Me.gb_segmentation.Name = "gb_segmentation"
    Me.gb_segmentation.Size = New System.Drawing.Size(481, 324)
    Me.gb_segmentation.TabIndex = 27
    Me.gb_segmentation.TabStop = False
    Me.gb_segmentation.Text = "GroupBox1"
    '
    'pnl_customer
    '
    Me.pnl_customer.Controls.Add(Me.btn_find_accounts)
    Me.pnl_customer.Controls.Add(Me.uc_grid_accounts)
    Me.pnl_customer.Controls.Add(Me.uc_account_sel)
    Me.pnl_customer.Location = New System.Drawing.Point(6, 76)
    Me.pnl_customer.Name = "pnl_customer"
    Me.pnl_customer.Size = New System.Drawing.Size(469, 242)
    Me.pnl_customer.TabIndex = 40
    '
    'btn_find_accounts
    '
    Me.btn_find_accounts.Location = New System.Drawing.Point(365, 15)
    Me.btn_find_accounts.Name = "btn_find_accounts"
    Me.btn_find_accounts.Size = New System.Drawing.Size(90, 30)
    Me.btn_find_accounts.TabIndex = 4
    Me.btn_find_accounts.Text = "Button1"
    Me.btn_find_accounts.UseVisualStyleBackColor = True
    '
    'uc_grid_accounts
    '
    Me.uc_grid_accounts.CurrentCol = -1
    Me.uc_grid_accounts.CurrentRow = -1
    Me.uc_grid_accounts.EditableCellBackColor = System.Drawing.Color.Empty
    Me.uc_grid_accounts.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.uc_grid_accounts.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.uc_grid_accounts.Location = New System.Drawing.Point(13, 143)
    Me.uc_grid_accounts.Name = "uc_grid_accounts"
    Me.uc_grid_accounts.PanelRightVisible = False
    Me.uc_grid_accounts.Redraw = True
    Me.uc_grid_accounts.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    Me.uc_grid_accounts.Size = New System.Drawing.Size(443, 95)
    Me.uc_grid_accounts.Sortable = False
    Me.uc_grid_accounts.SortAscending = True
    Me.uc_grid_accounts.SortByCol = 0
    Me.uc_grid_accounts.TabIndex = 2
    Me.uc_grid_accounts.ToolTipped = True
    Me.uc_grid_accounts.TopRow = -2
    Me.uc_grid_accounts.WordWrap = False
    '
    'uc_account_sel
    '
    Me.uc_account_sel.Account = ""
    Me.uc_account_sel.AccountText = ""
    Me.uc_account_sel.Anon = False
    Me.uc_account_sel.AutoSize = True
    Me.uc_account_sel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_account_sel.BirthDate = New Date(CType(0, Long))
    Me.uc_account_sel.DisabledHolder = False
    Me.uc_account_sel.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_account_sel.Holder = ""
    Me.uc_account_sel.Location = New System.Drawing.Point(13, 3)
    Me.uc_account_sel.MassiveSearchNumbers = ""
    Me.uc_account_sel.MassiveSearchNumbersToEdit = ""
    Me.uc_account_sel.MassiveSearchType = 0
    Me.uc_account_sel.Name = "uc_account_sel"
    Me.uc_account_sel.SearchTrackDataAsInternal = True
    Me.uc_account_sel.ShowMassiveSearch = False
    Me.uc_account_sel.ShowVipClients = True
    Me.uc_account_sel.Size = New System.Drawing.Size(307, 134)
    Me.uc_account_sel.TabIndex = 1
    Me.uc_account_sel.Telephone = ""
    Me.uc_account_sel.TrackData = ""
    Me.uc_account_sel.Vip = False
    Me.uc_account_sel.WeddingDate = New Date(CType(0, Long))
    '
    'pnl_filter
    '
    Me.pnl_filter.Controls.Add(Me.tab_filters)
    Me.pnl_filter.Location = New System.Drawing.Point(6, 76)
    Me.pnl_filter.Name = "pnl_filter"
    Me.pnl_filter.Size = New System.Drawing.Size(469, 242)
    Me.pnl_filter.TabIndex = 39
    '
    'tab_filters
    '
    Me.tab_filters.Controls.Add(Me.tab_gender)
    Me.tab_filters.Controls.Add(Me.tab_birth_date)
    Me.tab_filters.Controls.Add(Me.tab_created_account)
    Me.tab_filters.Controls.Add(Me.tab_level)
    Me.tab_filters.Controls.Add(Me.tab_freq)
    Me.tab_filters.Location = New System.Drawing.Point(6, 4)
    Me.tab_filters.Name = "tab_filters"
    Me.tab_filters.SelectedIndex = 0
    Me.tab_filters.Size = New System.Drawing.Size(463, 235)
    Me.tab_filters.TabIndex = 9
    '
    'tab_gender
    '
    Me.tab_gender.Controls.Add(Me.opt_gender_male)
    Me.tab_gender.Controls.Add(Me.opt_gender_female)
    Me.tab_gender.Controls.Add(Me.chk_by_gender)
    Me.tab_gender.Location = New System.Drawing.Point(4, 22)
    Me.tab_gender.Name = "tab_gender"
    Me.tab_gender.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_gender.Size = New System.Drawing.Size(455, 209)
    Me.tab_gender.TabIndex = 0
    Me.tab_gender.Text = "xBy Gender"
    Me.tab_gender.UseVisualStyleBackColor = True
    '
    'opt_gender_male
    '
    Me.opt_gender_male.Location = New System.Drawing.Point(26, 29)
    Me.opt_gender_male.Name = "opt_gender_male"
    Me.opt_gender_male.Size = New System.Drawing.Size(314, 17)
    Me.opt_gender_male.TabIndex = 1
    Me.opt_gender_male.Text = "xApplies to Men Only"
    '
    'opt_gender_female
    '
    Me.opt_gender_female.Location = New System.Drawing.Point(26, 52)
    Me.opt_gender_female.Name = "opt_gender_female"
    Me.opt_gender_female.Size = New System.Drawing.Size(314, 17)
    Me.opt_gender_female.TabIndex = 2
    Me.opt_gender_female.Text = "xApplies to Women Only"
    '
    'chk_by_gender
    '
    Me.chk_by_gender.AutoSize = True
    Me.chk_by_gender.Location = New System.Drawing.Point(6, 6)
    Me.chk_by_gender.Name = "chk_by_gender"
    Me.chk_by_gender.Size = New System.Drawing.Size(68, 17)
    Me.chk_by_gender.TabIndex = 0
    Me.chk_by_gender.Text = "xActive"
    Me.chk_by_gender.UseVisualStyleBackColor = True
    '
    'tab_birth_date
    '
    Me.tab_birth_date.Controls.Add(Me.Panel1)
    Me.tab_birth_date.Controls.Add(Me.opt_birth_date_only)
    Me.tab_birth_date.Controls.Add(Me.opt_birth_month_only)
    Me.tab_birth_date.Controls.Add(Me.chk_by_birth_date)
    Me.tab_birth_date.Location = New System.Drawing.Point(4, 22)
    Me.tab_birth_date.Name = "tab_birth_date"
    Me.tab_birth_date.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_birth_date.Size = New System.Drawing.Size(455, 209)
    Me.tab_birth_date.TabIndex = 1
    Me.tab_birth_date.Text = "xBy Birth Date"
    Me.tab_birth_date.UseVisualStyleBackColor = True
    '
    'Panel1
    '
    Me.Panel1.Controls.Add(Me.ef_age_from1)
    Me.Panel1.Controls.Add(Me.ef_age_to1)
    Me.Panel1.Controls.Add(Me.chk_by_age)
    Me.Panel1.Controls.Add(Me.ef_age_from2)
    Me.Panel1.Controls.Add(Me.opt_birth_exclude_age)
    Me.Panel1.Controls.Add(Me.opt_birth_include_age)
    Me.Panel1.Controls.Add(Me.ef_age_to2)
    Me.Panel1.Location = New System.Drawing.Point(3, 75)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(383, 81)
    Me.Panel1.TabIndex = 7
    '
    'ef_age_from1
    '
    Me.ef_age_from1.DoubleValue = 0.0R
    Me.ef_age_from1.IntegerValue = 0
    Me.ef_age_from1.IsReadOnly = False
    Me.ef_age_from1.Location = New System.Drawing.Point(138, 22)
    Me.ef_age_from1.Name = "ef_age_from1"
    Me.ef_age_from1.PlaceHolder = Nothing
    Me.ef_age_from1.Size = New System.Drawing.Size(39, 24)
    Me.ef_age_from1.SufixText = "Sufix Text"
    Me.ef_age_from1.TabIndex = 8
    Me.ef_age_from1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_age_from1.TextValue = ""
    Me.ef_age_from1.TextVisible = False
    Me.ef_age_from1.TextWidth = 1
    Me.ef_age_from1.Value = ""
    Me.ef_age_from1.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_age_to1
    '
    Me.ef_age_to1.DoubleValue = 0.0R
    Me.ef_age_to1.IntegerValue = 0
    Me.ef_age_to1.IsReadOnly = False
    Me.ef_age_to1.Location = New System.Drawing.Point(183, 22)
    Me.ef_age_to1.Name = "ef_age_to1"
    Me.ef_age_to1.PlaceHolder = Nothing
    Me.ef_age_to1.Size = New System.Drawing.Size(153, 24)
    Me.ef_age_to1.SufixText = "Sufix Text"
    Me.ef_age_to1.TabIndex = 9
    Me.ef_age_to1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_age_to1.TextValue = ""
    Me.ef_age_to1.TextVisible = False
    Me.ef_age_to1.TextWidth = 115
    Me.ef_age_to1.Value = ""
    Me.ef_age_to1.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_by_age
    '
    Me.chk_by_age.AutoSize = True
    Me.chk_by_age.Location = New System.Drawing.Point(3, 3)
    Me.chk_by_age.Name = "chk_by_age"
    Me.chk_by_age.Size = New System.Drawing.Size(68, 17)
    Me.chk_by_age.TabIndex = 3
    Me.chk_by_age.Text = "xActive"
    Me.chk_by_age.UseVisualStyleBackColor = True
    '
    'ef_age_from2
    '
    Me.ef_age_from2.DoubleValue = 0.0R
    Me.ef_age_from2.IntegerValue = 0
    Me.ef_age_from2.IsReadOnly = False
    Me.ef_age_from2.Location = New System.Drawing.Point(138, 49)
    Me.ef_age_from2.Name = "ef_age_from2"
    Me.ef_age_from2.PlaceHolder = Nothing
    Me.ef_age_from2.Size = New System.Drawing.Size(39, 24)
    Me.ef_age_from2.SufixText = "Sufix Text"
    Me.ef_age_from2.TabIndex = 6
    Me.ef_age_from2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_age_from2.TextValue = ""
    Me.ef_age_from2.TextVisible = False
    Me.ef_age_from2.TextWidth = 1
    Me.ef_age_from2.Value = ""
    Me.ef_age_from2.ValueForeColor = System.Drawing.Color.Blue
    '
    'opt_birth_exclude_age
    '
    Me.opt_birth_exclude_age.Location = New System.Drawing.Point(23, 53)
    Me.opt_birth_exclude_age.Name = "opt_birth_exclude_age"
    Me.opt_birth_exclude_age.Size = New System.Drawing.Size(110, 17)
    Me.opt_birth_exclude_age.TabIndex = 5
    Me.opt_birth_exclude_age.Text = "xYounger than"
    '
    'opt_birth_include_age
    '
    Me.opt_birth_include_age.Location = New System.Drawing.Point(23, 26)
    Me.opt_birth_include_age.Name = "opt_birth_include_age"
    Me.opt_birth_include_age.Size = New System.Drawing.Size(110, 17)
    Me.opt_birth_include_age.TabIndex = 4
    Me.opt_birth_include_age.Text = "xOlder than"
    '
    'ef_age_to2
    '
    Me.ef_age_to2.DoubleValue = 0.0R
    Me.ef_age_to2.IntegerValue = 0
    Me.ef_age_to2.IsReadOnly = False
    Me.ef_age_to2.Location = New System.Drawing.Point(183, 49)
    Me.ef_age_to2.Name = "ef_age_to2"
    Me.ef_age_to2.PlaceHolder = Nothing
    Me.ef_age_to2.Size = New System.Drawing.Size(153, 24)
    Me.ef_age_to2.SufixText = "Sufix Text"
    Me.ef_age_to2.TabIndex = 7
    Me.ef_age_to2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_age_to2.TextValue = ""
    Me.ef_age_to2.TextVisible = False
    Me.ef_age_to2.TextWidth = 115
    Me.ef_age_to2.Value = ""
    Me.ef_age_to2.ValueForeColor = System.Drawing.Color.Blue
    '
    'opt_birth_date_only
    '
    Me.opt_birth_date_only.Location = New System.Drawing.Point(26, 52)
    Me.opt_birth_date_only.Name = "opt_birth_date_only"
    Me.opt_birth_date_only.Size = New System.Drawing.Size(344, 17)
    Me.opt_birth_date_only.TabIndex = 2
    Me.opt_birth_date_only.Text = "xBirth day and Month"
    '
    'opt_birth_month_only
    '
    Me.opt_birth_month_only.Location = New System.Drawing.Point(26, 29)
    Me.opt_birth_month_only.Name = "opt_birth_month_only"
    Me.opt_birth_month_only.Size = New System.Drawing.Size(344, 17)
    Me.opt_birth_month_only.TabIndex = 1
    Me.opt_birth_month_only.Text = "xAny day within birthday's month"
    '
    'chk_by_birth_date
    '
    Me.chk_by_birth_date.AutoSize = True
    Me.chk_by_birth_date.Location = New System.Drawing.Point(6, 6)
    Me.chk_by_birth_date.Name = "chk_by_birth_date"
    Me.chk_by_birth_date.Size = New System.Drawing.Size(68, 17)
    Me.chk_by_birth_date.TabIndex = 0
    Me.chk_by_birth_date.Text = "xActive"
    Me.chk_by_birth_date.UseVisualStyleBackColor = True
    '
    'tab_created_account
    '
    Me.tab_created_account.Controls.Add(Me.Panel3)
    Me.tab_created_account.Controls.Add(Me.Panel2)
    Me.tab_created_account.Controls.Add(Me.opt_created_account_anniversary)
    Me.tab_created_account.Controls.Add(Me.opt_created_account_date)
    Me.tab_created_account.Controls.Add(Me.chk_by_created_account)
    Me.tab_created_account.Location = New System.Drawing.Point(4, 22)
    Me.tab_created_account.Name = "tab_created_account"
    Me.tab_created_account.Size = New System.Drawing.Size(455, 209)
    Me.tab_created_account.TabIndex = 5
    Me.tab_created_account.Text = "xBy Created Account"
    Me.tab_created_account.UseVisualStyleBackColor = True
    '
    'Panel3
    '
    Me.Panel3.Controls.Add(Me.ef_created_account_anniversary_year_to)
    Me.Panel3.Controls.Add(Me.chk_by_anniversary_range_of_years)
    Me.Panel3.Controls.Add(Me.opt_created_account_anniversary_day_month)
    Me.Panel3.Controls.Add(Me.opt_created_account_anniversary_whole_month)
    Me.Panel3.Controls.Add(Me.ef_created_account_anniversary_year_from)
    Me.Panel3.Location = New System.Drawing.Point(33, 109)
    Me.Panel3.Name = "Panel3"
    Me.Panel3.Size = New System.Drawing.Size(353, 66)
    Me.Panel3.TabIndex = 7
    '
    'ef_created_account_anniversary_year_to
    '
    Me.ef_created_account_anniversary_year_to.DoubleValue = 0.0R
    Me.ef_created_account_anniversary_year_to.IntegerValue = 0
    Me.ef_created_account_anniversary_year_to.IsReadOnly = False
    Me.ef_created_account_anniversary_year_to.Location = New System.Drawing.Point(207, 39)
    Me.ef_created_account_anniversary_year_to.Name = "ef_created_account_anniversary_year_to"
    Me.ef_created_account_anniversary_year_to.PlaceHolder = Nothing
    Me.ef_created_account_anniversary_year_to.Size = New System.Drawing.Size(143, 24)
    Me.ef_created_account_anniversary_year_to.SufixText = "xYear_to"
    Me.ef_created_account_anniversary_year_to.SufixTextWidth = 115
    Me.ef_created_account_anniversary_year_to.TabIndex = 12
    Me.ef_created_account_anniversary_year_to.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_created_account_anniversary_year_to.TextValue = ""
    Me.ef_created_account_anniversary_year_to.TextVisible = False
    Me.ef_created_account_anniversary_year_to.TextWidth = 0
    Me.ef_created_account_anniversary_year_to.Value = ""
    Me.ef_created_account_anniversary_year_to.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_by_anniversary_range_of_years
    '
    Me.chk_by_anniversary_range_of_years.Location = New System.Drawing.Point(7, 45)
    Me.chk_by_anniversary_range_of_years.Name = "chk_by_anniversary_range_of_years"
    Me.chk_by_anniversary_range_of_years.Size = New System.Drawing.Size(150, 17)
    Me.chk_by_anniversary_range_of_years.TabIndex = 10
    Me.chk_by_anniversary_range_of_years.Text = "xBy_anniversary_range_of_years"
    Me.chk_by_anniversary_range_of_years.UseVisualStyleBackColor = True
    '
    'opt_created_account_anniversary_day_month
    '
    Me.opt_created_account_anniversary_day_month.AccessibleDescription = ""
    Me.opt_created_account_anniversary_day_month.Location = New System.Drawing.Point(7, 23)
    Me.opt_created_account_anniversary_day_month.Name = "opt_created_account_anniversary_day_month"
    Me.opt_created_account_anniversary_day_month.Size = New System.Drawing.Size(337, 17)
    Me.opt_created_account_anniversary_day_month.TabIndex = 9
    Me.opt_created_account_anniversary_day_month.TabStop = True
    Me.opt_created_account_anniversary_day_month.Tag = ""
    Me.opt_created_account_anniversary_day_month.Text = "xCreated_account_anniversary_day_month"
    '
    'opt_created_account_anniversary_whole_month
    '
    Me.opt_created_account_anniversary_whole_month.Location = New System.Drawing.Point(7, 3)
    Me.opt_created_account_anniversary_whole_month.Name = "opt_created_account_anniversary_whole_month"
    Me.opt_created_account_anniversary_whole_month.Size = New System.Drawing.Size(337, 17)
    Me.opt_created_account_anniversary_whole_month.TabIndex = 8
    Me.opt_created_account_anniversary_whole_month.TabStop = True
    Me.opt_created_account_anniversary_whole_month.Tag = ""
    Me.opt_created_account_anniversary_whole_month.Text = "xCreated_account_anniversary_whole_month"
    '
    'ef_created_account_anniversary_year_from
    '
    Me.ef_created_account_anniversary_year_from.DoubleValue = 0.0R
    Me.ef_created_account_anniversary_year_from.IntegerValue = 0
    Me.ef_created_account_anniversary_year_from.IsReadOnly = False
    Me.ef_created_account_anniversary_year_from.Location = New System.Drawing.Point(155, 40)
    Me.ef_created_account_anniversary_year_from.Name = "ef_created_account_anniversary_year_from"
    Me.ef_created_account_anniversary_year_from.PlaceHolder = Nothing
    Me.ef_created_account_anniversary_year_from.Size = New System.Drawing.Size(56, 24)
    Me.ef_created_account_anniversary_year_from.SufixText = "xYear_from"
    Me.ef_created_account_anniversary_year_from.SufixTextWidth = 30
    Me.ef_created_account_anniversary_year_from.TabIndex = 11
    Me.ef_created_account_anniversary_year_from.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_created_account_anniversary_year_from.TextValue = ""
    Me.ef_created_account_anniversary_year_from.TextVisible = False
    Me.ef_created_account_anniversary_year_from.TextWidth = 0
    Me.ef_created_account_anniversary_year_from.Value = ""
    Me.ef_created_account_anniversary_year_from.ValueForeColor = System.Drawing.Color.Blue
    '
    'Panel2
    '
    Me.Panel2.Controls.Add(Me.opt_created_account_working_day_plus)
    Me.Panel2.Controls.Add(Me.opt_created_account_working_day_only)
    Me.Panel2.Controls.Add(Me.ef_created_account_working_day_plus)
    Me.Panel2.Location = New System.Drawing.Point(33, 42)
    Me.Panel2.Name = "Panel2"
    Me.Panel2.Size = New System.Drawing.Size(353, 47)
    Me.Panel2.TabIndex = 2
    '
    'opt_created_account_working_day_plus
    '
    Me.opt_created_account_working_day_plus.Location = New System.Drawing.Point(7, 24)
    Me.opt_created_account_working_day_plus.Name = "opt_created_account_working_day_plus"
    Me.opt_created_account_working_day_plus.Size = New System.Drawing.Size(213, 17)
    Me.opt_created_account_working_day_plus.TabIndex = 4
    Me.opt_created_account_working_day_plus.TabStop = True
    Me.opt_created_account_working_day_plus.Tag = ""
    Me.opt_created_account_working_day_plus.Text = "xCreated_account_working_day_plus"
    '
    'opt_created_account_working_day_only
    '
    Me.opt_created_account_working_day_only.Location = New System.Drawing.Point(7, 3)
    Me.opt_created_account_working_day_only.Name = "opt_created_account_working_day_only"
    Me.opt_created_account_working_day_only.Size = New System.Drawing.Size(337, 17)
    Me.opt_created_account_working_day_only.TabIndex = 3
    Me.opt_created_account_working_day_only.TabStop = True
    Me.opt_created_account_working_day_only.Tag = ""
    Me.opt_created_account_working_day_only.Text = "xCreated_account_working_day_only"
    '
    'ef_created_account_working_day_plus
    '
    Me.ef_created_account_working_day_plus.DoubleValue = 0.0R
    Me.ef_created_account_working_day_plus.IntegerValue = 0
    Me.ef_created_account_working_day_plus.IsReadOnly = False
    Me.ef_created_account_working_day_plus.Location = New System.Drawing.Point(218, 21)
    Me.ef_created_account_working_day_plus.Name = "ef_created_account_working_day_plus"
    Me.ef_created_account_working_day_plus.PlaceHolder = Nothing
    Me.ef_created_account_working_day_plus.Size = New System.Drawing.Size(132, 24)
    Me.ef_created_account_working_day_plus.SufixText = "xWorkingDay(s) to apply, after created account working day"
    Me.ef_created_account_working_day_plus.SufixTextWidth = 100
    Me.ef_created_account_working_day_plus.TabIndex = 5
    Me.ef_created_account_working_day_plus.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_created_account_working_day_plus.TextValue = ""
    Me.ef_created_account_working_day_plus.TextVisible = False
    Me.ef_created_account_working_day_plus.TextWidth = 0
    Me.ef_created_account_working_day_plus.Value = ""
    Me.ef_created_account_working_day_plus.ValueForeColor = System.Drawing.Color.Blue
    '
    'opt_created_account_anniversary
    '
    Me.opt_created_account_anniversary.Location = New System.Drawing.Point(22, 90)
    Me.opt_created_account_anniversary.Name = "opt_created_account_anniversary"
    Me.opt_created_account_anniversary.Size = New System.Drawing.Size(344, 17)
    Me.opt_created_account_anniversary.TabIndex = 6
    Me.opt_created_account_anniversary.TabStop = True
    Me.opt_created_account_anniversary.Tag = ""
    Me.opt_created_account_anniversary.Text = "xCreated_account_anniversary"
    '
    'opt_created_account_date
    '
    Me.opt_created_account_date.Location = New System.Drawing.Point(22, 23)
    Me.opt_created_account_date.Name = "opt_created_account_date"
    Me.opt_created_account_date.Size = New System.Drawing.Size(344, 17)
    Me.opt_created_account_date.TabIndex = 1
    Me.opt_created_account_date.TabStop = True
    Me.opt_created_account_date.Text = "xCreated_account_date"
    '
    'chk_by_created_account
    '
    Me.chk_by_created_account.AutoSize = True
    Me.chk_by_created_account.Location = New System.Drawing.Point(6, 6)
    Me.chk_by_created_account.Name = "chk_by_created_account"
    Me.chk_by_created_account.Size = New System.Drawing.Size(68, 17)
    Me.chk_by_created_account.TabIndex = 0
    Me.chk_by_created_account.Text = "xActive"
    Me.chk_by_created_account.UseVisualStyleBackColor = True
    '
    'tab_level
    '
    Me.tab_level.Controls.Add(Me.chk_only_vip)
    Me.tab_level.Controls.Add(Me.chk_level_anonymous)
    Me.tab_level.Controls.Add(Me.chk_level_04)
    Me.tab_level.Controls.Add(Me.chk_level_03)
    Me.tab_level.Controls.Add(Me.chk_level_02)
    Me.tab_level.Controls.Add(Me.chk_level_01)
    Me.tab_level.Controls.Add(Me.chk_by_level)
    Me.tab_level.Location = New System.Drawing.Point(4, 22)
    Me.tab_level.Name = "tab_level"
    Me.tab_level.Size = New System.Drawing.Size(455, 209)
    Me.tab_level.TabIndex = 2
    Me.tab_level.Text = "xBy Level"
    Me.tab_level.UseVisualStyleBackColor = True
    '
    'chk_only_vip
    '
    Me.chk_only_vip.AutoSize = True
    Me.chk_only_vip.Location = New System.Drawing.Point(6, 145)
    Me.chk_only_vip.Name = "chk_only_vip"
    Me.chk_only_vip.Size = New System.Drawing.Size(83, 17)
    Me.chk_only_vip.TabIndex = 22
    Me.chk_only_vip.Text = "xOnlyVips"
    Me.chk_only_vip.UseVisualStyleBackColor = True
    '
    'chk_level_anonymous
    '
    Me.chk_level_anonymous.AutoSize = True
    Me.chk_level_anonymous.Location = New System.Drawing.Point(26, 29)
    Me.chk_level_anonymous.Name = "chk_level_anonymous"
    Me.chk_level_anonymous.Size = New System.Drawing.Size(100, 17)
    Me.chk_level_anonymous.TabIndex = 1
    Me.chk_level_anonymous.Text = "xAnonymous"
    Me.chk_level_anonymous.UseVisualStyleBackColor = True
    '
    'chk_level_04
    '
    Me.chk_level_04.AutoSize = True
    Me.chk_level_04.Location = New System.Drawing.Point(26, 121)
    Me.chk_level_04.Name = "chk_level_04"
    Me.chk_level_04.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_04.TabIndex = 5
    Me.chk_level_04.Text = "xLevel 04"
    Me.chk_level_04.UseVisualStyleBackColor = True
    '
    'chk_level_03
    '
    Me.chk_level_03.AutoSize = True
    Me.chk_level_03.Location = New System.Drawing.Point(26, 98)
    Me.chk_level_03.Name = "chk_level_03"
    Me.chk_level_03.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_03.TabIndex = 4
    Me.chk_level_03.Text = "xLevel 03"
    Me.chk_level_03.UseVisualStyleBackColor = True
    '
    'chk_level_02
    '
    Me.chk_level_02.AutoSize = True
    Me.chk_level_02.Location = New System.Drawing.Point(26, 75)
    Me.chk_level_02.Name = "chk_level_02"
    Me.chk_level_02.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_02.TabIndex = 3
    Me.chk_level_02.Text = "xLevel 02"
    Me.chk_level_02.UseVisualStyleBackColor = True
    '
    'chk_level_01
    '
    Me.chk_level_01.AutoSize = True
    Me.chk_level_01.Location = New System.Drawing.Point(26, 52)
    Me.chk_level_01.Name = "chk_level_01"
    Me.chk_level_01.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_01.TabIndex = 2
    Me.chk_level_01.Text = "xLevel 01"
    Me.chk_level_01.UseVisualStyleBackColor = True
    '
    'chk_by_level
    '
    Me.chk_by_level.AutoSize = True
    Me.chk_by_level.Location = New System.Drawing.Point(6, 6)
    Me.chk_by_level.Name = "chk_by_level"
    Me.chk_by_level.Size = New System.Drawing.Size(68, 17)
    Me.chk_by_level.TabIndex = 0
    Me.chk_by_level.Text = "xActive"
    Me.chk_by_level.UseVisualStyleBackColor = True
    '
    'tab_freq
    '
    Me.tab_freq.Controls.Add(Me.lbl_freq_filter_02)
    Me.tab_freq.Controls.Add(Me.lbl_freq_filter_01)
    Me.tab_freq.Controls.Add(Me.ef_freq_min_cash_in)
    Me.tab_freq.Controls.Add(Me.ef_freq_min_days)
    Me.tab_freq.Controls.Add(Me.ef_freq_last_days)
    Me.tab_freq.Controls.Add(Me.chk_by_freq)
    Me.tab_freq.Location = New System.Drawing.Point(4, 22)
    Me.tab_freq.Name = "tab_freq"
    Me.tab_freq.Size = New System.Drawing.Size(455, 209)
    Me.tab_freq.TabIndex = 3
    Me.tab_freq.Text = "xBy Freq"
    Me.tab_freq.UseVisualStyleBackColor = True
    '
    'lbl_freq_filter_02
    '
    Me.lbl_freq_filter_02.AutoSize = True
    Me.lbl_freq_filter_02.IsReadOnly = True
    Me.lbl_freq_filter_02.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_freq_filter_02.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_freq_filter_02.Location = New System.Drawing.Point(9, 134)
    Me.lbl_freq_filter_02.Name = "lbl_freq_filter_02"
    Me.lbl_freq_filter_02.Size = New System.Drawing.Size(364, 24)
    Me.lbl_freq_filter_02.SufixText = "Sufix Text"
    Me.lbl_freq_filter_02.SufixTextVisible = True
    Me.lbl_freq_filter_02.TabIndex = 5
    Me.lbl_freq_filter_02.TabStop = False
    Me.lbl_freq_filter_02.TextVisible = False
    Me.lbl_freq_filter_02.TextWidth = 0
    Me.lbl_freq_filter_02.Value = "xrecharging minimum [] per day"
    '
    'lbl_freq_filter_01
    '
    Me.lbl_freq_filter_01.AutoSize = True
    Me.lbl_freq_filter_01.IsReadOnly = True
    Me.lbl_freq_filter_01.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_freq_filter_01.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_freq_filter_01.Location = New System.Drawing.Point(9, 110)
    Me.lbl_freq_filter_01.Name = "lbl_freq_filter_01"
    Me.lbl_freq_filter_01.Size = New System.Drawing.Size(364, 24)
    Me.lbl_freq_filter_01.SufixText = "Sufix Text"
    Me.lbl_freq_filter_01.SufixTextVisible = True
    Me.lbl_freq_filter_01.TabIndex = 4
    Me.lbl_freq_filter_01.TabStop = False
    Me.lbl_freq_filter_01.TextVisible = False
    Me.lbl_freq_filter_01.TextWidth = 0
    Me.lbl_freq_filter_01.Value = "xIn the last [] days, he has come at least [] days"
    '
    'ef_freq_min_cash_in
    '
    Me.ef_freq_min_cash_in.DoubleValue = 0.0R
    Me.ef_freq_min_cash_in.IntegerValue = 0
    Me.ef_freq_min_cash_in.IsReadOnly = False
    Me.ef_freq_min_cash_in.Location = New System.Drawing.Point(9, 81)
    Me.ef_freq_min_cash_in.Name = "ef_freq_min_cash_in"
    Me.ef_freq_min_cash_in.PlaceHolder = Nothing
    Me.ef_freq_min_cash_in.Size = New System.Drawing.Size(200, 24)
    Me.ef_freq_min_cash_in.SufixText = "Sufix Text"
    Me.ef_freq_min_cash_in.SufixTextVisible = True
    Me.ef_freq_min_cash_in.TabIndex = 3
    Me.ef_freq_min_cash_in.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_freq_min_cash_in.TextValue = ""
    Me.ef_freq_min_cash_in.TextVisible = False
    Me.ef_freq_min_cash_in.TextWidth = 110
    Me.ef_freq_min_cash_in.Value = ""
    Me.ef_freq_min_cash_in.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_freq_min_days
    '
    Me.ef_freq_min_days.DoubleValue = 0.0R
    Me.ef_freq_min_days.IntegerValue = 0
    Me.ef_freq_min_days.IsReadOnly = False
    Me.ef_freq_min_days.Location = New System.Drawing.Point(9, 54)
    Me.ef_freq_min_days.Name = "ef_freq_min_days"
    Me.ef_freq_min_days.PlaceHolder = Nothing
    Me.ef_freq_min_days.Size = New System.Drawing.Size(150, 24)
    Me.ef_freq_min_days.SufixText = "Sufix Text"
    Me.ef_freq_min_days.SufixTextVisible = True
    Me.ef_freq_min_days.TabIndex = 2
    Me.ef_freq_min_days.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_freq_min_days.TextValue = ""
    Me.ef_freq_min_days.TextVisible = False
    Me.ef_freq_min_days.TextWidth = 110
    Me.ef_freq_min_days.Value = ""
    Me.ef_freq_min_days.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_freq_last_days
    '
    Me.ef_freq_last_days.DoubleValue = 0.0R
    Me.ef_freq_last_days.IntegerValue = 0
    Me.ef_freq_last_days.IsReadOnly = False
    Me.ef_freq_last_days.Location = New System.Drawing.Point(9, 26)
    Me.ef_freq_last_days.Name = "ef_freq_last_days"
    Me.ef_freq_last_days.PlaceHolder = Nothing
    Me.ef_freq_last_days.Size = New System.Drawing.Size(150, 24)
    Me.ef_freq_last_days.SufixText = "Sufix Text"
    Me.ef_freq_last_days.SufixTextVisible = True
    Me.ef_freq_last_days.TabIndex = 1
    Me.ef_freq_last_days.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_freq_last_days.TextValue = ""
    Me.ef_freq_last_days.TextVisible = False
    Me.ef_freq_last_days.TextWidth = 110
    Me.ef_freq_last_days.Value = ""
    Me.ef_freq_last_days.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_by_freq
    '
    Me.chk_by_freq.AutoSize = True
    Me.chk_by_freq.Location = New System.Drawing.Point(6, 6)
    Me.chk_by_freq.Name = "chk_by_freq"
    Me.chk_by_freq.Size = New System.Drawing.Size(68, 17)
    Me.chk_by_freq.TabIndex = 0
    Me.chk_by_freq.Text = "xActive"
    Me.chk_by_freq.UseVisualStyleBackColor = True
    '
    'opt_all_devices
    '
    Me.opt_all_devices.AutoSize = True
    Me.opt_all_devices.Location = New System.Drawing.Point(170, 46)
    Me.opt_all_devices.Name = "opt_all_devices"
    Me.opt_all_devices.Size = New System.Drawing.Size(155, 17)
    Me.opt_all_devices.TabIndex = 13
    Me.opt_all_devices.TabStop = True
    Me.opt_all_devices.Text = "xTodos los dispositivos"
    Me.opt_all_devices.UseVisualStyleBackColor = True
    '
    'pnl_customer_group
    '
    Me.pnl_customer_group.Controls.Add(Me.rtb_report)
    Me.pnl_customer_group.Controls.Add(Me.btn_import)
    Me.pnl_customer_group.Location = New System.Drawing.Point(6, 76)
    Me.pnl_customer_group.Name = "pnl_customer_group"
    Me.pnl_customer_group.Size = New System.Drawing.Size(469, 242)
    Me.pnl_customer_group.TabIndex = 41
    '
    'rtb_report
    '
    Me.rtb_report.Location = New System.Drawing.Point(6, 44)
    Me.rtb_report.Name = "rtb_report"
    Me.rtb_report.ReadOnly = True
    Me.rtb_report.Size = New System.Drawing.Size(456, 185)
    Me.rtb_report.TabIndex = 7
    Me.rtb_report.Text = ""
    '
    'btn_import
    '
    Me.btn_import.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_import.Location = New System.Drawing.Point(6, 7)
    Me.btn_import.Name = "btn_import"
    Me.btn_import.Size = New System.Drawing.Size(90, 30)
    Me.btn_import.TabIndex = 4
    Me.btn_import.ToolTipped = False
    Me.btn_import.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.USER
    '
    'pnl_not_account
    '
    Me.pnl_not_account.Location = New System.Drawing.Point(6, 76)
    Me.pnl_not_account.Name = "pnl_not_account"
    Me.pnl_not_account.Size = New System.Drawing.Size(469, 242)
    Me.pnl_not_account.TabIndex = 42
    '
    'opt_customer
    '
    Me.opt_customer.AutoSize = True
    Me.opt_customer.Location = New System.Drawing.Point(170, 20)
    Me.opt_customer.Name = "opt_customer"
    Me.opt_customer.Size = New System.Drawing.Size(72, 17)
    Me.opt_customer.TabIndex = 10
    Me.opt_customer.TabStop = True
    Me.opt_customer.Text = "xCliente"
    Me.opt_customer.UseVisualStyleBackColor = True
    '
    'opt_customer_group
    '
    Me.opt_customer_group.AutoSize = True
    Me.opt_customer_group.Location = New System.Drawing.Point(308, 20)
    Me.opt_customer_group.Name = "opt_customer_group"
    Me.opt_customer_group.Size = New System.Drawing.Size(113, 17)
    Me.opt_customer_group.TabIndex = 11
    Me.opt_customer_group.TabStop = True
    Me.opt_customer_group.Text = "xGrupoClientes"
    Me.opt_customer_group.UseVisualStyleBackColor = True
    '
    'opt_not_account
    '
    Me.opt_not_account.AutoSize = True
    Me.opt_not_account.Location = New System.Drawing.Point(9, 46)
    Me.opt_not_account.Name = "opt_not_account"
    Me.opt_not_account.Size = New System.Drawing.Size(92, 17)
    Me.opt_not_account.TabIndex = 12
    Me.opt_not_account.TabStop = True
    Me.opt_not_account.Text = "xNo Cuenta"
    Me.opt_not_account.UseVisualStyleBackColor = True
    '
    'opt_filter
    '
    Me.opt_filter.AutoSize = True
    Me.opt_filter.Location = New System.Drawing.Point(9, 20)
    Me.opt_filter.Name = "opt_filter"
    Me.opt_filter.Size = New System.Drawing.Size(60, 17)
    Me.opt_filter.TabIndex = 9
    Me.opt_filter.TabStop = True
    Me.opt_filter.Text = "xFiltro"
    Me.opt_filter.UseVisualStyleBackColor = True
    '
    'gb_scheduler
    '
    Me.gb_scheduler.Controls.Add(Me.uc_schedule)
    Me.gb_scheduler.Controls.Add(Me.opt_schedule)
    Me.gb_scheduler.Controls.Add(Me.opt_inmediate)
    Me.gb_scheduler.Controls.Add(Me.btn_sent)
    Me.gb_scheduler.Location = New System.Drawing.Point(504, 299)
    Me.gb_scheduler.Name = "gb_scheduler"
    Me.gb_scheduler.Size = New System.Drawing.Size(428, 324)
    Me.gb_scheduler.TabIndex = 28
    Me.gb_scheduler.TabStop = False
    Me.gb_scheduler.Text = "GroupBox2"
    '
    'uc_schedule
    '
    Me.uc_schedule.CheckedDateTo = True
    Me.uc_schedule.DateFrom = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_schedule.DateTo = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_schedule.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_schedule.Location = New System.Drawing.Point(6, 77)
    Me.uc_schedule.Name = "uc_schedule"
    Me.uc_schedule.SecondTime = False
    Me.uc_schedule.SecondTimeFrom = 0
    Me.uc_schedule.SecondTimeTo = 0
    Me.uc_schedule.ShowCheckBoxDateTo = False
    Me.uc_schedule.Size = New System.Drawing.Size(416, 239)
    Me.uc_schedule.TabIndex = 34
    Me.uc_schedule.TimeFrom = 0
    Me.uc_schedule.TimeTo = 0
    Me.uc_schedule.Weekday = 0
    '
    'opt_schedule
    '
    Me.opt_schedule.AutoSize = True
    Me.opt_schedule.Location = New System.Drawing.Point(150, 20)
    Me.opt_schedule.Name = "opt_schedule"
    Me.opt_schedule.Size = New System.Drawing.Size(102, 17)
    Me.opt_schedule.TabIndex = 15
    Me.opt_schedule.TabStop = True
    Me.opt_schedule.Text = "xProgramada"
    Me.opt_schedule.UseVisualStyleBackColor = True
    '
    'opt_inmediate
    '
    Me.opt_inmediate.AutoSize = True
    Me.opt_inmediate.Location = New System.Drawing.Point(10, 20)
    Me.opt_inmediate.Name = "opt_inmediate"
    Me.opt_inmediate.Size = New System.Drawing.Size(90, 17)
    Me.opt_inmediate.TabIndex = 14
    Me.opt_inmediate.TabStop = True
    Me.opt_inmediate.Text = "xInmediata"
    Me.opt_inmediate.UseVisualStyleBackColor = True
    '
    'btn_sent
    '
    Me.btn_sent.Location = New System.Drawing.Point(274, 285)
    Me.btn_sent.Name = "btn_sent"
    Me.btn_sent.Size = New System.Drawing.Size(148, 30)
    Me.btn_sent.TabIndex = 35
    Me.btn_sent.Text = "xSent Notification"
    Me.btn_sent.UseVisualStyleBackColor = True
    '
    'ef_url
    '
    Me.ef_url.AllowDrop = True
    Me.ef_url.DoubleValue = 0.0R
    Me.ef_url.IntegerValue = 0
    Me.ef_url.IsReadOnly = False
    Me.ef_url.Location = New System.Drawing.Point(120, 50)
    Me.ef_url.Name = "ef_url"
    Me.ef_url.PlaceHolder = Nothing
    Me.ef_url.Size = New System.Drawing.Size(298, 24)
    Me.ef_url.SufixText = "Sufix Text"
    Me.ef_url.SufixTextVisible = True
    Me.ef_url.TabIndex = 4
    Me.ef_url.TabStop = False
    Me.ef_url.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_url.TextValue = ""
    Me.ef_url.TextWidth = 0
    Me.ef_url.Value = ""
    Me.ef_url.ValueForeColor = System.Drawing.Color.Blue
    '
    'opt_section
    '
    Me.opt_section.AutoSize = True
    Me.opt_section.Location = New System.Drawing.Point(11, 24)
    Me.opt_section.Name = "opt_section"
    Me.opt_section.Size = New System.Drawing.Size(76, 17)
    Me.opt_section.TabIndex = 0
    Me.opt_section.TabStop = True
    Me.opt_section.Text = "xSeccion"
    Me.opt_section.UseVisualStyleBackColor = True
    '
    'opt_url
    '
    Me.opt_url.AutoSize = True
    Me.opt_url.Location = New System.Drawing.Point(11, 54)
    Me.opt_url.Name = "opt_url"
    Me.opt_url.Size = New System.Drawing.Size(47, 17)
    Me.opt_url.TabIndex = 3
    Me.opt_url.TabStop = True
    Me.opt_url.Text = "URL"
    Me.opt_url.UseVisualStyleBackColor = True
    '
    'uc_image
    '
    Me.uc_image.AutoSize = True
    Me.uc_image.ButtonDeleteEnabled = True
    Me.uc_image.FreeResize = False
    Me.uc_image.Image = Nothing
    Me.uc_image.ImageInfoFont = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_image.ImageLayout = System.Windows.Forms.ImageLayout.Zoom
    Me.uc_image.ImageName = Nothing
    Me.uc_image.Location = New System.Drawing.Point(629, 16)
    Me.uc_image.Margin = New System.Windows.Forms.Padding(0)
    Me.uc_image.Name = "uc_image"
    Me.uc_image.Size = New System.Drawing.Size(303, 284)
    Me.uc_image.TabIndex = 32
    Me.uc_image.Transparent = False
    '
    'optInformative
    '
    Me.optInformative.AutoSize = True
    Me.optInformative.Location = New System.Drawing.Point(11, 84)
    Me.optInformative.Name = "optInformative"
    Me.optInformative.Size = New System.Drawing.Size(78, 17)
    Me.optInformative.TabIndex = 5
    Me.optInformative.TabStop = True
    Me.optInformative.Text = "xNinguno"
    Me.optInformative.UseVisualStyleBackColor = True
    '
    'gb_goto
    '
    Me.gb_goto.Controls.Add(Me.cmb_section_type)
    Me.gb_goto.Controls.Add(Me.optInformative)
    Me.gb_goto.Controls.Add(Me.cmb_section)
    Me.gb_goto.Controls.Add(Me.lbl_type_section)
    Me.gb_goto.Controls.Add(Me.opt_url)
    Me.gb_goto.Controls.Add(Me.ef_url)
    Me.gb_goto.Controls.Add(Me.opt_section)
    Me.gb_goto.Location = New System.Drawing.Point(9, 7)
    Me.gb_goto.Name = "gb_goto"
    Me.gb_goto.Size = New System.Drawing.Size(606, 116)
    Me.gb_goto.TabIndex = 34
    Me.gb_goto.TabStop = False
    Me.gb_goto.Text = "GroupBox1"
    '
    'frm_winup_notifications_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1046, 637)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.KeyPreview = False
    Me.Name = "frm_winup_notifications_edit"
    Me.Text = "frm_winup_notifications_edit"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.gb_segmentation.ResumeLayout(False)
    Me.gb_segmentation.PerformLayout()
    Me.pnl_customer.ResumeLayout(False)
    Me.pnl_customer.PerformLayout()
    Me.pnl_filter.ResumeLayout(False)
    Me.tab_filters.ResumeLayout(False)
    Me.tab_gender.ResumeLayout(False)
    Me.tab_gender.PerformLayout()
    Me.tab_birth_date.ResumeLayout(False)
    Me.tab_birth_date.PerformLayout()
    Me.Panel1.ResumeLayout(False)
    Me.Panel1.PerformLayout()
    Me.tab_created_account.ResumeLayout(False)
    Me.tab_created_account.PerformLayout()
    Me.Panel3.ResumeLayout(False)
    Me.Panel2.ResumeLayout(False)
    Me.tab_level.ResumeLayout(False)
    Me.tab_level.PerformLayout()
    Me.tab_freq.ResumeLayout(False)
    Me.tab_freq.PerformLayout()
    Me.pnl_customer_group.ResumeLayout(False)
    Me.gb_scheduler.ResumeLayout(False)
    Me.gb_scheduler.PerformLayout()
    Me.gb_goto.ResumeLayout(False)
    Me.gb_goto.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents lbl_message As System.Windows.Forms.Label
  Friend WithEvents txt_message As System.Windows.Forms.TextBox
  Friend WithEvents lbl_type_section As System.Windows.Forms.Label
  Friend WithEvents lbl_title As System.Windows.Forms.Label
  Friend WithEvents cmb_section_type As GUI_Controls.uc_combo
  Friend WithEvents cmb_section As GUI_Controls.uc_combo
  Friend WithEvents ef_title As GUI_Controls.uc_entry_field
  Friend WithEvents chk_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents gb_scheduler As System.Windows.Forms.GroupBox
  Friend WithEvents gb_segmentation As System.Windows.Forms.GroupBox
  Friend WithEvents opt_url As System.Windows.Forms.RadioButton
  Friend WithEvents opt_section As System.Windows.Forms.RadioButton
  Friend WithEvents ef_url As GUI_Controls.uc_entry_field
  Friend WithEvents opt_schedule As System.Windows.Forms.RadioButton
  Friend WithEvents opt_inmediate As System.Windows.Forms.RadioButton
  Friend WithEvents uc_schedule As GUI_Controls.uc_schedule
  Friend WithEvents opt_customer As System.Windows.Forms.RadioButton
  Friend WithEvents opt_customer_group As System.Windows.Forms.RadioButton
  Friend WithEvents opt_not_account As System.Windows.Forms.RadioButton
  Friend WithEvents opt_filter As System.Windows.Forms.RadioButton
  Friend WithEvents pnl_not_account As System.Windows.Forms.Panel
  Friend WithEvents pnl_customer_group As System.Windows.Forms.Panel
  Friend WithEvents pnl_customer As System.Windows.Forms.Panel
  Friend WithEvents pnl_filter As System.Windows.Forms.Panel
  Friend WithEvents tab_filters As System.Windows.Forms.TabControl
  Friend WithEvents tab_gender As System.Windows.Forms.TabPage
  Friend WithEvents opt_gender_male As System.Windows.Forms.RadioButton
  Friend WithEvents opt_gender_female As System.Windows.Forms.RadioButton
  Friend WithEvents chk_by_gender As System.Windows.Forms.CheckBox
  Friend WithEvents tab_birth_date As System.Windows.Forms.TabPage
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
  Friend WithEvents ef_age_from1 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_age_to1 As GUI_Controls.uc_entry_field
  Friend WithEvents chk_by_age As System.Windows.Forms.CheckBox
  Friend WithEvents ef_age_from2 As GUI_Controls.uc_entry_field
  Friend WithEvents opt_birth_exclude_age As System.Windows.Forms.RadioButton
  Friend WithEvents opt_birth_include_age As System.Windows.Forms.RadioButton
  Friend WithEvents ef_age_to2 As GUI_Controls.uc_entry_field
  Friend WithEvents opt_birth_date_only As System.Windows.Forms.RadioButton
  Friend WithEvents opt_birth_month_only As System.Windows.Forms.RadioButton
  Friend WithEvents chk_by_birth_date As System.Windows.Forms.CheckBox
  Friend WithEvents tab_created_account As System.Windows.Forms.TabPage
  Friend WithEvents Panel3 As System.Windows.Forms.Panel
  Friend WithEvents ef_created_account_anniversary_year_to As GUI_Controls.uc_entry_field
  Friend WithEvents chk_by_anniversary_range_of_years As System.Windows.Forms.CheckBox
  Friend WithEvents opt_created_account_anniversary_day_month As System.Windows.Forms.RadioButton
  Friend WithEvents opt_created_account_anniversary_whole_month As System.Windows.Forms.RadioButton
  Friend WithEvents ef_created_account_anniversary_year_from As GUI_Controls.uc_entry_field
  Friend WithEvents Panel2 As System.Windows.Forms.Panel
  Friend WithEvents opt_created_account_working_day_plus As System.Windows.Forms.RadioButton
  Friend WithEvents opt_created_account_working_day_only As System.Windows.Forms.RadioButton
  Friend WithEvents ef_created_account_working_day_plus As GUI_Controls.uc_entry_field
  Friend WithEvents opt_created_account_anniversary As System.Windows.Forms.RadioButton
  Friend WithEvents opt_created_account_date As System.Windows.Forms.RadioButton
  Friend WithEvents chk_by_created_account As System.Windows.Forms.CheckBox
  Friend WithEvents tab_level As System.Windows.Forms.TabPage
  Friend WithEvents chk_only_vip As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_anonymous As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_04 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_03 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_02 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_01 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_by_level As System.Windows.Forms.CheckBox
  Friend WithEvents tab_freq As System.Windows.Forms.TabPage
  Friend WithEvents lbl_freq_filter_02 As GUI_Controls.uc_text_field
  Friend WithEvents lbl_freq_filter_01 As GUI_Controls.uc_text_field
  Friend WithEvents ef_freq_min_cash_in As GUI_Controls.uc_entry_field
  Friend WithEvents ef_freq_min_days As GUI_Controls.uc_entry_field
  Friend WithEvents ef_freq_last_days As GUI_Controls.uc_entry_field
  Friend WithEvents chk_by_freq As System.Windows.Forms.CheckBox
  Friend WithEvents uc_account_sel As GUI_Controls.uc_account_sel
  Friend WithEvents uc_grid_accounts As GUI_Controls.uc_grid
  Friend WithEvents btn_find_accounts As System.Windows.Forms.Button
  Friend WithEvents rtb_report As System.Windows.Forms.RichTextBox
  Friend WithEvents btn_import As GUI_Controls.uc_button
  Friend WithEvents opt_all_devices As System.Windows.Forms.RadioButton
  Friend WithEvents btn_sent As System.Windows.Forms.Button
  Friend WithEvents uc_image As GUI_Controls.uc_image
  Friend WithEvents optInformative As System.Windows.Forms.RadioButton
  Friend WithEvents gb_goto As System.Windows.Forms.GroupBox
End Class
