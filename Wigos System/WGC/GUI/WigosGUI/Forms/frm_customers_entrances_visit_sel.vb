﻿'-------------------------------------------------------------------
' Copyright © 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_customer_entrances_report_sel
' DESCRIPTION:   Customer Entrances Report by visits and entries
' AUTHOR:        Alberto Dios Ibáñez
' CREATION DATE: 05-FEB-2016
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 05-FEB-2016  ADI    Initial version
' 21-SEP-2016  OVS    PBI 17666 Visitas / Recepción: MEJORAS II - Ref. 01 - Reporte de visitas y entradas por cliente (GUI)
' 19-SEP-2016  XGJ    Eliminar contador de filas de la grid y substutuilo por una etiqueta con el número de entradas
' 20-SEP-2016  JRC    PBI 17677:Visitas / Recepción: MEJORAS II - Ref. 48 - Reportes recepción - separar columna Nombre (GUI)
' 29-SEP-2016  FJC    PBI 17681:Visitas / Recepción: MEJORAS II - Agrupar menú (GUI)
' 10-OCT-2016  OVS    Bug 18838:Recepción-GUI: En la columna “Jornada” del Reporte de visitas y entradas por cliente el mes se visualiza cortado
' 17-GEN-2017  XGJ    Bug Bug 22770:Reporte de visitas y entradas por cliente: línea en blanco debajo del TOTAL
'-------------------------------------------------------------------- 

Imports GUI_Controls
Imports System.Text
Imports System.Data.SqlClient
Imports System.Globalization
Imports GUI_CommonOperations
Imports WSI.Common
Imports GUI_CommonMisc.ControlColor
Imports GUI_CommonMisc.mdl_NLS

Public Class frm_customers_entrances_visit_sel
  Inherits GUI_Controls.frm_base_sel

#Region "Privates Enum"
  Enum TYPE_OF_REPORT
    EntrancesByVisits = 0
    EntrancesAccumulate = 1
    Both = 2
  End Enum
#End Region

#Region "Private Properties"

  Dim m_last_day As String
  Dim m_type_report_activate As Integer = -1
  Dim m_last_month_to_compare As Integer
  Dim m_accum_theorical As Decimal
  Dim m_accum_real As Decimal
  Dim m_accum_difference As Decimal
  Dim m_accum_visits As Integer
  Dim m_accum_theorical_by_month As Decimal
  Dim m_total_theorical As Decimal
  Dim m_total_entry_by_day As Integer
  Dim m_accum_entries As Integer
  Dim m_total_entries As Integer
  Dim m_total_entries_by_month As Integer
  Dim m_accum_real_by_month As Decimal
  Dim m_total_real As Decimal
  Dim m_accum_difference_by_month As Decimal
  Dim m_total_difference As Decimal
  Dim m_accum_visits_by_month As Integer
  Dim m_total_visits As Integer
  Dim m_row_index As Integer
  Dim m_last_month As String
  Dim m_is_last_record As Boolean
  Dim m_count_header As Integer
  Dim m_db_row As GUI_Controls.frm_base_sel.CLASS_DB_ROW
  Dim m_sub_header As Boolean
  Dim m_print_totals As Boolean
  Dim m_add_header As Boolean
  Dim m_rpt_visits As String
  Dim m_rpt_entries As String
  Dim m_date_from As String
  Dim m_date_to As String
  Dim m_last_dbrow As GUI_Controls.frm_base_sel.CLASS_DB_ROW
  Dim m_check_box As Boolean
  Dim m_day_counter As Integer

#End Region

#Region " Constants "

  ' SQL Columns
  Private Const SQL_COLUMN_VISIT_DATE As Integer = 0
  Private Const SQL_COLUMN_VISIT_NUMBER As Integer = 1
  Private Const SQL_COLUMN_HOLDER_NAME As Integer = 2
  Private Const SQL_COLUMN_HOLDER_LASTNAME As Integer = 3
  Private Const SQL_COLUMN_CARD_TRACK As Integer = 4
  Private Const SQL_COLUMN_ENTRIES As Integer = 5
  Private Const SQL_COLUMN_THEORICAL_BY_DAY As Integer = 6
  Private Const SQL_COLUMN_REAL_BY_DAY As Integer = 7
  Private Const SQL_COLUMN_DIFFERENCE_BY_DAY As Integer = 8
  Private Const SQL_COLUMN_THEORICAL_COLLECTION As Integer = 9
  Private Const SQL_COLUMN_REAL_COLLECTION As Integer = 10
  Private Const SQL_COLUMN_DIFFERENCE As Integer = 11
  Private Const SQL_COLUMN_HOLDER_FULLNAME As Integer = 12
  Private Const SQL_COLUMN_INPUT_TYPE As Integer = 13

  ' Num grid columns / headers
  Private Const GRID_HEADER_ROWS As Integer = 1
  Private Const GRID_COLUMNS As Integer = 14

  ' Grid Columns
  Private Const GRID_COLUMN_ID As Integer = 0
  Private Const GRID_COLUMN_VISIT_DATE As Integer = 1
  Private Const GRID_COLUMN_VISIT_NUMBER As Integer = 2
  Private Const GRID_COLUMN_HOLDER_NAME As Integer = 3
  Private Const GRID_COLUMN_HOLDER_LASTNAME As Integer = 4
  Private Const GRID_COLUMN_CARD_TRACK As Integer = 5
  Private Const GRID_COLUMN_ENTRIES As Integer = 6
  Private Const GRID_COLUMN_INPUT_TYPE As Integer = 7
  Private Const GRID_COLUMN_THEORICAL_COLLECTION As Integer = 8
  Private Const GRID_COLUMN_DIFFERENCE As Integer = 9
  Private Const GRID_COLUMN_REAL_COLLECTION As Integer = 10
  Private Const GRID_COLUMN_VISIT_DAY_DETAIL As Integer = 11
  Private Const GRID_COLUMN_THEORICAL_BY_DAY As Integer = 12
  Private Const GRID_COLUMN_HOLDER_FULLNAME As Integer = 13

  'Width Columns
  Private Const GRID_WIDTH_ID As Integer = 200
  Private Const GRID_WIDTH_VISIT_DATE As Integer = 2500
  Private Const GRID_WIDTH_VISIT_NUMBER As Integer = 1500
  Private Const GRID_WIDTH_HOLDER_NAME As Integer = 2000
  Private Const GRID_WIDTH_HOLDER_LASTNAME As Integer = 2000

  Private Const GRID_WIDTH_CARD_TRACK As Integer = 2500
  Private Const GRID_WIDTH_ENTRIES As Integer = 1600
  Private Const GRID_WIDTH_INPUT_TYPE As Integer = 2000
  Private Const GRID_WIDTH_THEORICAL_COLLECTION As Integer = 2150
  Private Const GRID_WIDTH_REAL_COLLECTION As Integer = 2150
  Private Const GRID_WIDTH_DIFFERENCE As Integer = 1450
  Private Const GRID_WIDTH_VISIT_DAY_DETAIL As Integer = 0

  ' Type of card for where clause
  Private Const CARD_TYPE As Integer = 2

  Private Const ALLOWS_EXPAND_SYMBOL As String = "[+] "
  Private Const ALLOWS_COLLAPSE_SYMBOL As String = "[-]  "

#End Region

#Region "Private Methods"

  ''' <summary>
  ''' Print details values in grid when report is by entries
  ''' </summary>
  ''' <param name="DbRow"></param>
  ''' <param name="MonthIsChanged"></param>
  ''' <remarks></remarks>
  Private Sub InsertDetailAccumulate(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW, Optional MonthIsChanged As Boolean = False)

    If m_add_header Or m_is_last_record Then
      m_row_index = m_row_index + 1
      Me.Grid.AddRow()
    End If

    'Date
    Me.Grid.Cell(m_row_index, GRID_COLUMN_VISIT_DATE).Value = GUI_FormatDate(DateTime.ParseExact(DbRow.Value(SQL_COLUMN_VISIT_DATE), "yyyyMMdd", CultureInfo.InvariantCulture),
                                                                                                                                                                                                                                                                                           ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)
    'Number of visits
    Me.Grid.Cell(m_row_index, GRID_COLUMN_VISIT_NUMBER).Value = DbRow.Value(SQL_COLUMN_VISIT_NUMBER)
    m_accum_visits_by_month = m_accum_visits_by_month + DbRow.Value(SQL_COLUMN_VISIT_NUMBER)
    m_total_visits = m_total_visits + DbRow.Value(SQL_COLUMN_VISIT_NUMBER)

    'Theorical collected
    Me.Grid.Cell(m_row_index, GRID_COLUMN_THEORICAL_COLLECTION).Value =
    GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_THEORICAL_BY_DAY), _
                                              ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    m_accum_theorical_by_month = m_accum_theorical_by_month + DbRow.Value(SQL_COLUMN_THEORICAL_BY_DAY)
    m_total_theorical = m_total_theorical + DbRow.Value(SQL_COLUMN_THEORICAL_BY_DAY)

    ' Total Real 
    Me.Grid.Cell(m_row_index, GRID_COLUMN_REAL_COLLECTION).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_REAL_BY_DAY), _
                                            ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    m_accum_real_by_month = m_accum_real_by_month + DbRow.Value(SQL_COLUMN_REAL_BY_DAY)
    m_total_real = m_total_real + DbRow.Value(SQL_COLUMN_REAL_BY_DAY)

    'difference 
    Me.Grid.Cell(m_row_index, GRID_COLUMN_DIFFERENCE).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_DIFFERENCE_BY_DAY), _
                                           ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    m_accum_difference_by_month = m_accum_difference_by_month + DbRow.Value(SQL_COLUMN_DIFFERENCE_BY_DAY)
    m_total_difference = m_total_difference + DbRow.Value(SQL_COLUMN_DIFFERENCE_BY_DAY)
    m_add_header = False

  End Sub 'InsertDetailAccumulate

  ''' <summary>
  ''' Print details values in grid
  ''' </summary>
  ''' <param name="DbRow"></param>
  ''' <remarks></remarks>
  Private Sub InsertDetail(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW, Optional MonthIsChanged As Boolean = False)

    If m_sub_header Then
      m_row_index = m_row_index + 1
      Me.Grid.AddRow()
    End If

    Me.Grid.Cell(m_row_index, GRID_COLUMN_VISIT_DAY_DETAIL).Value = GUI_FormatDate(DateTime.ParseExact(DbRow.Value(SQL_COLUMN_VISIT_DATE), "yyyyMMdd", CultureInfo.InvariantCulture),
                                                                                                                                                                                                                                                                                         ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)

    Me.Grid.Cell(m_row_index, GRID_COLUMN_VISIT_DATE).Value = GUI_FormatDate(DateTime.ParseExact(DbRow.Value(SQL_COLUMN_VISIT_DATE), "yyyyMMdd", CultureInfo.InvariantCulture),
                                                                                                                                                                                                                                                                                         ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)
    'Holder Name
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_HOLDER_NAME)) Then
      Me.Grid.Cell(m_row_index, GRID_COLUMN_HOLDER_NAME).Value = DbRow.Value(SQL_COLUMN_HOLDER_NAME)
    End If
    'Holder LastName
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_HOLDER_LASTNAME)) Then
      Me.Grid.Cell(m_row_index, GRID_COLUMN_HOLDER_LASTNAME).Value = DbRow.Value(SQL_COLUMN_HOLDER_LASTNAME)
    End If

    'Holder Full Name: invisible pero necesaria para pasar como parametro al  GUI_ShowSelectedItem() frm_customer_entrances_report
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_HOLDER_FULLNAME)) Then
      Me.Grid.Cell(m_row_index, GRID_COLUMN_HOLDER_FULLNAME).Value = DbRow.Value(SQL_COLUMN_HOLDER_FULLNAME)
    End If

    'TrackData
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_CARD_TRACK)) Then
      Me.Grid.Cell(m_row_index, GRID_COLUMN_CARD_TRACK).Value = DbRow.Value(SQL_COLUMN_CARD_TRACK)
    End If

    'Number of entries by holder....
    Me.Grid.Cell(m_row_index, GRID_COLUMN_ENTRIES).Value = DbRow.Value(SQL_COLUMN_ENTRIES)
    m_total_entry_by_day = m_total_entry_by_day + DbRow.Value(SQL_COLUMN_ENTRIES)

    'Input type
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_INPUT_TYPE)) Then
      Me.Grid.Cell(m_row_index, GRID_COLUMN_INPUT_TYPE).Value = FormatString(DbRow.Value(SQL_COLUMN_INPUT_TYPE))
    End If

    'Theorical collection
    Me.Grid.Cell(m_row_index, GRID_COLUMN_THEORICAL_COLLECTION).Value =
    GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_THEORICAL_COLLECTION), _
                                              ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    ' Total Real 
    Me.Grid.Cell(m_row_index, GRID_COLUMN_REAL_COLLECTION).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_REAL_COLLECTION), _
                                            ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'difference by holder
    Me.Grid.Cell(m_row_index, GRID_COLUMN_DIFFERENCE).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_DIFFERENCE), _
                                            ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    m_add_header = False
    m_sub_header = False

  End Sub 'InsertDetail

  ''' <summary>
  '''  Print SubHeaders values in grid
  ''' </summary>
  ''' <param name="DbRow"></param>
  ''' <remarks></remarks>
  Private Sub InsertSubTotal(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW, Optional DayIsChanged As Boolean = False)

    m_sub_header = True

    Me.Grid.Row(m_row_index).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
    ' Visit Date

    Me.Grid.Cell(m_row_index, GRID_COLUMN_VISIT_DATE).Value = GUI_FormatDate(DateTime.ParseExact(m_last_day, "yyyyMMdd", CultureInfo.InvariantCulture),
                                                                          ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)

    'Number of visits
    Me.Grid.Cell(m_row_index, GRID_COLUMN_VISIT_NUMBER).Value = DbRow.Value(SQL_COLUMN_VISIT_NUMBER)
    m_accum_visits_by_month = m_accum_visits_by_month + DbRow.Value(SQL_COLUMN_VISIT_NUMBER)
    m_total_visits = m_total_visits + DbRow.Value(SQL_COLUMN_VISIT_NUMBER)

    'Total number of entries
    Me.Grid.Cell(m_row_index, GRID_COLUMN_ENTRIES).Value = m_total_entry_by_day
    m_total_entries_by_month = m_total_entries_by_month + m_total_entry_by_day
    m_total_entry_by_day = 0

    'Total Theoric 
    Me.Grid.Cell(m_row_index, GRID_COLUMN_THEORICAL_COLLECTION).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_THEORICAL_BY_DAY), _
                           ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    m_accum_theorical_by_month = m_accum_theorical_by_month + DbRow.Value(SQL_COLUMN_THEORICAL_BY_DAY)
    m_total_theorical = m_total_theorical + DbRow.Value(SQL_COLUMN_THEORICAL_BY_DAY)

    'Total Real 
    Me.Grid.Cell(m_row_index, GRID_COLUMN_REAL_COLLECTION).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_REAL_BY_DAY), _
                                           ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    m_accum_real_by_month = m_accum_real_by_month + DbRow.Value(SQL_COLUMN_REAL_BY_DAY)
    m_total_real = m_total_real + DbRow.Value(SQL_COLUMN_REAL_BY_DAY)

    'Daily difference 
    Me.Grid.Cell(m_row_index, GRID_COLUMN_DIFFERENCE).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_DIFFERENCE_BY_DAY), _
                                            ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    m_accum_difference_by_month = m_accum_difference_by_month + DbRow.Value(SQL_COLUMN_DIFFERENCE_BY_DAY)
    m_total_difference = m_total_difference + DbRow.Value(SQL_COLUMN_DIFFERENCE_BY_DAY)

  End Sub 'InsertSubHeader

  ''' <summary>
  ''' Establish accumulates when type of report is both
  ''' </summary>
  ''' <param name="MonthIsChanged"></param>
  ''' <remarks></remarks>
  Private Sub InsertTotalByMonth(Optional MonthIsChanged As Boolean = False)

    If m_is_last_record Then
      If m_type_report_activate = TYPE_OF_REPORT.Both Then
        Me.Grid.AddRow()
        InsertSubTotal(m_last_dbrow)
        m_row_index = m_row_index + 1
        Me.Grid.AddRow()
      Else
        m_row_index = m_row_index + 1
        Me.Grid.AddRow()
      End If
    End If
    If MonthIsChanged And m_type_report_activate <> TYPE_OF_REPORT.EntrancesAccumulate Then
      m_row_index = m_row_index + 1
      Me.Grid.AddRow()
    End If
    Me.Grid.Row(m_row_index).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_01)

    Me.Grid.Cell(m_row_index, GRID_COLUMN_VISIT_DATE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1047) & " " & FormatDate(GUI_FormatDate(DateTime.ParseExact(m_last_day, "yyyyMMdd", CultureInfo.InvariantCulture),
                                                                          ENUM_FORMAT_DATE.FORMAT_DATE_YEAR_MONTH))

    'Number of visits
    Me.Grid.Cell(m_row_index, GRID_COLUMN_VISIT_NUMBER).Value = m_accum_visits_by_month

    'Number of entries
    Me.Grid.Cell(m_row_index, GRID_COLUMN_ENTRIES).Value = m_total_entries_by_month
    m_total_entries = m_total_entries + m_total_entries_by_month
    m_total_entries_by_month = 0

    'Total Theoric 
    Me.Grid.Cell(m_row_index, GRID_COLUMN_THEORICAL_COLLECTION).Value = GUI_FormatCurrency(m_accum_theorical_by_month, _
                             ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Total Real 
    Me.Grid.Cell(m_row_index, GRID_COLUMN_REAL_COLLECTION).Value = GUI_FormatCurrency(m_accum_real_by_month, _
                                            ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Daily difference 
    Me.Grid.Cell(m_row_index, GRID_COLUMN_DIFFERENCE).Value = GUI_FormatCurrency(m_accum_difference_by_month, _
                                            ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    m_accum_visits_by_month = 0
    m_accum_theorical_by_month = 0
    m_accum_real_by_month = 0
    m_accum_difference_by_month = 0
    m_total_entries_by_month = 0
  End Sub 'InsertTotalByMonth

  ''' <summary>
  ''' Establish accumulates when type of report accumulate
  ''' </summary>
  ''' <param name="MonthIsChanged"></param>
  ''' <remarks></remarks>
  Private Sub InsertTotalByMonthAccumulate(Optional MonthIsChanged As Boolean = False, Optional DayCounter As Integer = 0)

    If m_is_last_record Then
      If m_type_report_activate = TYPE_OF_REPORT.Both Then
        Me.Grid.AddRow()
        InsertSubTotal(m_last_dbrow)
        m_row_index = m_row_index + 1
        Me.Grid.AddRow()
      Else
        m_row_index = m_row_index + 1
      End If
    End If
    If MonthIsChanged And m_type_report_activate <> TYPE_OF_REPORT.EntrancesAccumulate Then
      m_row_index = m_row_index + 1
      Me.Grid.AddRow()
    End If

    Dim index As Integer
    index = LoadReverseDayNumber(m_row_index)
    index = m_row_index - index - 1

    If Not m_is_last_record Then
      Me.Grid.Row(m_row_index).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
    End If

    Me.Grid.Cell(index, GRID_COLUMN_VISIT_DATE).Value = FormatDate(GUI_FormatDate(DateTime.ParseExact(m_last_day, "yyyyMMdd", CultureInfo.InvariantCulture), ENUM_FORMAT_DATE.FORMAT_DATE_YEAR_MONTH))
    Me.Grid.Column(GRID_COLUMN_VISIT_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    'Number of visits
    Me.Grid.Cell(index, GRID_COLUMN_VISIT_NUMBER).Value = m_accum_visits_by_month

    'Number of entries
    Me.Grid.Cell(index, GRID_COLUMN_ENTRIES).Value = m_total_entries_by_month
    m_total_entries = m_total_entries + m_total_entries_by_month
    m_total_entries_by_month = 0

    'Total Theoric 
    Me.Grid.Cell(index, GRID_COLUMN_THEORICAL_COLLECTION).Value = GUI_FormatCurrency(m_accum_theorical_by_month, _
                             ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Total Real 
    Me.Grid.Cell(index, GRID_COLUMN_REAL_COLLECTION).Value = GUI_FormatCurrency(m_accum_real_by_month, _
                                            ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Daily difference 
    Me.Grid.Cell(index, GRID_COLUMN_DIFFERENCE).Value = GUI_FormatCurrency(m_accum_difference_by_month, _
                                            ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    m_accum_visits_by_month = 0
    m_accum_theorical_by_month = 0
    m_accum_real_by_month = 0
    m_accum_difference_by_month = 0
    m_total_entries_by_month = 0

    Dim indice
    indice = m_row_index - DayCounter - 1

    Me.Grid.CollapseExpand(indice, indice + DayCounter, 1, 1)

  End Sub 'InsertTotalByMonth

  ''' <summary>
  ''' Format of a date
  ''' </summary>
  ''' <param name="source"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function FormatDate(ByVal source As String) As String
    Dim _chain As String
    _chain = source

    If _chain.Length > 2 Then
      _chain = _chain.Substring(0, 1).ToUpper() + _chain.Substring(1)
    End If

    Return _chain.Trim()
  End Function 'FormatDate

  ''' <summary>
  ''' Set default values to filters
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SetDefaultValues()

    m_last_day = String.Empty
    m_last_month_to_compare = 0
    m_accum_difference = 0
    m_accum_real = 0
    m_accum_theorical = 0
    m_accum_visits = 0
    m_accum_difference_by_month = 0
    m_accum_real_by_month = 0
    m_accum_theorical_by_month = 0
    m_accum_visits_by_month = 0
    m_is_last_record = False
    m_row_index = 0
    m_last_month_to_compare = 0
    m_last_day = String.Empty
    m_count_header = 0
    m_sub_header = False
    m_print_totals = False
    m_add_header = False
    m_total_difference = 0
    m_total_real = 0
    m_total_theorical = 0
    m_total_visits = 0
    m_last_dbrow = Nothing
    m_accum_entries = 0
    m_total_entries = 0
    m_total_entries_by_month = 0
  End Sub 'SetDefaultValues

  ''' <summary>
  ''' Print results of query when report is Visits by Customer
  ''' </summary>
  ''' <param name="RowIndex"></param>
  ''' <param name="DbRow"></param>
  ''' <remarks></remarks>
  Private Sub VisitsByCustomer(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW)

    Dim _day_is_changed As Boolean
    Dim _month_is_changed As Boolean
    Dim _actual_month As String
    Dim _actual_day As String


    _day_is_changed = False
    _month_is_changed = False
    _actual_day = DbRow.Value(SQL_COLUMN_VISIT_DATE)
    _actual_month = Convert.ToInt32(_actual_day.ToString.Substring(4, 2))


    If RowIndex = 0 Then
      m_row_index = RowIndex
      InsertHeader(DbRow)
      m_row_index = m_row_index + 1
      Me.Grid.AddRow()
      InsertDetail(DbRow)
      m_last_dbrow = DbRow
    Else
      If m_last_dbrow.Value(SQL_COLUMN_VISIT_DATE) <> DbRow.Value(SQL_COLUMN_VISIT_DATE) Then
        _day_is_changed = True
        InsertSubTotal(m_last_dbrow)

      End If
      If Not m_last_month_to_compare.Equals("0") And _actual_month <> m_last_month_to_compare Then
        _month_is_changed = True
      End If
      If _month_is_changed Then
        m_add_header = False

        InsertTotalByMonth(_month_is_changed)
        InsertHeader(DbRow, _month_is_changed)
        InsertDetail(DbRow, _month_is_changed)
      Else
        InsertDetail(DbRow)
      End If

    End If

    m_last_day = DbRow.Value(SQL_COLUMN_VISIT_DATE)
    m_last_month_to_compare = Convert.ToInt32(DbRow.Value(SQL_COLUMN_VISIT_DATE).ToString.Substring(4, 2)).ToString
    m_row_index = m_row_index + 1
    m_last_dbrow = DbRow

  End Sub 'VisitsByCustomer

  ''' <summary>
  ''' Print header in reports
  ''' </summary>
  ''' <param name="DbRow"></param>
  ''' <remarks></remarks>
  Private Sub InsertHeader(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW, Optional MonthIsChanged As Boolean = False)

    m_last_dbrow = DbRow

    If MonthIsChanged Then
      m_row_index = m_row_index + 1
      Me.Grid.AddRow()

    End If

    Me.Grid.Cell(m_row_index, GRID_COLUMN_VISIT_DATE).Value = FormatDate((GUI_FormatDate(DateTime.ParseExact(DbRow.Value(SQL_COLUMN_VISIT_DATE), "yyyyMMdd", CultureInfo.InvariantCulture), ENUM_FORMAT_DATE.FORMAT_DATE_YEAR_MONTH)))

    Me.Grid.Row(m_row_index).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

    m_count_header = m_count_header + 1
    m_add_header = True

  End Sub 'InsertHeaderAccumulate

  ''' <summary>
  ''' Print header accumulate in reports
  ''' </summary>
  ''' <param name="DbRow"></param>
  ''' <remarks></remarks>
  Private Sub InsertHeaderAccumulate(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW, Optional MonthIsChanged As Boolean = False)

    'm_count_header = m_count_header + 1
    m_add_header = True

  End Sub 'InsertHeaderAccumulate

  ''' <summary>
  ''' Print results of query when report is Entries accumulate
  ''' </summary>
  ''' <param name="RowIndex"></param>
  ''' <param name="DbRow"></param>
  ''' <remarks></remarks>
  Private Sub EntrancesAccumulate(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW)

    Dim _month_is_changed As Boolean
    Dim _month_to_compare As String
    Dim _day_to_compare As Date
    Dim _header_inserted As Boolean

    m_row_index = RowIndex
    _header_inserted = True
    _month_is_changed = False
    _day_to_compare = GUI_FormatDate(DateTime.ParseExact(DbRow.Value(SQL_COLUMN_VISIT_DATE), "yyyyMMdd", CultureInfo.InvariantCulture),
                                                                         ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)
    _month_to_compare = Convert.ToInt32(DbRow.Value(SQL_COLUMN_VISIT_DATE).ToString.Substring(4, 2)).ToString
    If RowIndex = 0 Then
      m_day_counter = 1
      InsertHeader(DbRow)
      InsertDetailAccumulate(DbRow)
    Else
      If Not m_is_last_record Then
        If (m_last_month_to_compare <> _month_to_compare) Then _month_is_changed = True
        If _month_is_changed Then
          InsertTotalByMonthAccumulate(_month_is_changed, m_day_counter)
          InsertHeaderAccumulate(DbRow, _month_is_changed)
          InsertDetailAccumulate(DbRow, True)
          m_day_counter = 1
        Else
          m_day_counter = m_day_counter + 1
          InsertDetailAccumulate(DbRow)
        End If
      Else
        InsertTotalByMonth()
      End If
    End If

    m_last_day = DbRow.Value(SQL_COLUMN_VISIT_DATE)
    m_last_month_to_compare = Convert.ToInt32(DbRow.Value(SQL_COLUMN_VISIT_DATE).ToString.Substring(4, 2)).ToString

  End Sub 'EntrancesAccumulate

  ''' <summary>
  ''' Print last row in grid with total 
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub TotalGeneral()

    Me.Grid.AddRow()

    ' XGJ 17-GEN-2017
    If (opt_visits_entries.Checked) Then
      m_row_index = m_row_index + 1
    End If

    'm_row_index = m_row_index + 1
    Me.Grid.Row(m_row_index).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
    Me.Grid.Cell(m_row_index, GRID_COLUMN_VISIT_DATE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1047)

    'Number of visits
    Me.Grid.Cell(m_row_index, GRID_COLUMN_VISIT_NUMBER).Value = m_total_visits

    'Number of  Entrances
    Me.Grid.Cell(m_row_index, GRID_COLUMN_ENTRIES).Value = m_total_entries

    'Total Theoric 
    Me.Grid.Cell(m_row_index, GRID_COLUMN_THEORICAL_COLLECTION).Value = GUI_FormatCurrency(m_total_theorical, _
                             ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Total Real 
    Me.Grid.Cell(m_row_index, GRID_COLUMN_REAL_COLLECTION).Value = GUI_FormatCurrency(m_total_real, _
                                            ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Total difference 
    Me.Grid.Cell(m_row_index, GRID_COLUMN_DIFFERENCE).Value = GUI_FormatCurrency(m_total_difference, _
                                            ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    If (m_total_entries <> 0) Then
      Me.lbl_total_entradas.Visible = True
      Me.lbl_numero_total_entradas.Visible = True
      Me.lbl_numero_total_entradas.Text = String.Format(m_total_entries, "##,##0")
    Else
      Me.lbl_total_entradas.Visible = False
      Me.lbl_numero_total_entradas.Visible = False
    End If
  End Sub

  Private Function IsCollapsed(ByVal ColumnForSignal As Integer, ByVal Row As Integer) As Boolean
    Dim cell_value As String

    cell_value = Me.Grid.Cell(Row, ColumnForSignal).Value()

    If Mid(cell_value, 1, Len(ALLOWS_EXPAND_SYMBOL)) = ALLOWS_EXPAND_SYMBOL Then
      Return True
    Else
      Return False
    End If

  End Function

  Private Function IsExpanded(ByVal ColumnForSignal As Integer, ByVal Row As Integer) As Boolean
    Dim cell_value As String

    cell_value = Me.Grid.Cell(Row, ColumnForSignal).Value()

    If Mid(cell_value, 1, Len(ALLOWS_COLLAPSE_SYMBOL)) = ALLOWS_COLLAPSE_SYMBOL Then
      Return True
    Else
      Return False
    End If

  End Function

  Private Sub SetSignalAllowCollapse(ByVal ColumnForSignal As Integer, ByVal Row As Integer)
    Dim cell_value_no_signal As String
    Dim cell_value As String

    cell_value = Me.Grid.Cell(Row, ColumnForSignal).Value()
    cell_value_no_signal = Mid(cell_value, Len(ALLOWS_EXPAND_SYMBOL) + 1)
    Me.Grid.Cell(Row, ColumnForSignal).Value() = ALLOWS_COLLAPSE_SYMBOL & cell_value_no_signal

  End Sub

  Private Sub SetSignalAllowExpand(ByVal ColumnForSignal As Integer, ByVal Row As Integer)
    Dim cell_value_no_signal As String
    Dim cell_value As String

    cell_value = Me.Grid.Cell(Row, ColumnForSignal).Value
    cell_value_no_signal = Mid(cell_value, Len(ALLOWS_COLLAPSE_SYMBOL) + 1)
    Me.Grid.Cell(Row, ColumnForSignal).Value = ALLOWS_EXPAND_SYMBOL & cell_value_no_signal

  End Sub

  Private Function LoadDayNumber(index As Integer) As Integer
    Dim _dayNumber
    index = index + 1
    _dayNumber = 0

    While (Me.Grid.Row(index).BackColor.ToArgb() = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00).ToArgb())
      index = index + 1
      _dayNumber = _dayNumber + 1
    End While

    Return _dayNumber
  End Function

  Private Function LoadReverseDayNumber(index As Integer) As Integer
    Dim _dayNumber
    index = index - 1
    _dayNumber = 0

    While (Me.Grid.Row(index).BackColor.ToArgb() = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00).ToArgb())
      index = index - 1
      _dayNumber = _dayNumber + 1
    End While

    Return _dayNumber
  End Function

  Private Function FormatString(Value As String) As String
    Value = Value.Trim()
    Value = Mid(Value, 1, Len(Value) - 1)

    Return Value
  End Function

#End Region

#Region "Overrides"

  ''' <summary>
  '''  Check for consistency values provided for every filter
  ''' </summary>
  ''' <returns> TRUE: filter values are accepted</returns>
  ''' <returns>FALSE: filter values are not accepted</returns> 
  ''' <remarks></remarks>
  Protected Overrides Function GUI_FilterCheck() As Boolean

    If Me.dtp_from.Value > Me.dtp_to.Value Then
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Return False
    End If

    Return True
  End Function 'GUI_FilterCheck

  ''' <summary>
  ''' Set proper values for form filters being sent to the report
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_date_from = String.Empty
    m_date_to = String.Empty

  End Sub 'GUI_ReportUpdateFilters

  ''' <summary>
  ''' Set proper values for form filters being sent to the report
  ''' </summary>
  ''' <param name="PrintData"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    m_date_from = Me.dtp_from.Value
    If m_check_box Then
      m_date_to = Me.dtp_to.Value.Date
    Else
      m_date_to = String.Empty
    End If
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1328), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1329), m_date_to)

    If Me.opt_only_visits.Checked Then
      PrintData.SetFilter(m_rpt_entries, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6692))
      PrintData.SetFilter(m_rpt_visits, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6693))
    ElseIf Me.opt_visits_entries.Checked Then
      PrintData.SetFilter(m_rpt_entries, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6693))
      PrintData.SetFilter(m_rpt_visits, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6692))
    End If

    PrintData.FilterHeaderWidth(2) = 1500
    PrintData.FilterValueWidth(2) = 4000

  End Sub ' GUI_ReportFilter

  ''' <summary>
  '''  Establish Form Id, according to the enumeration in the application
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_FORM.FORM_CUSTOMER_ENTRANCES_REPORT_VISITS
    Call MyBase.GUI_SetFormId()
  End Sub 'GUI_SetFormId

  ''' <summary>
  ''' Sets the values of a row in the data grid
  ''' </summary>
  ''' <param name="RowIndex"></param>
  ''' <param name="DbRow"></param>
  ''' <returns>True: the row could be added</returns>
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    If Me.opt_visits_entries.Checked Then
      Me.opt_only_visits.Checked = False
      m_type_report_activate = TYPE_OF_REPORT.Both
      Call VisitsByCustomer(m_row_index, DbRow)
    ElseIf Me.opt_only_visits.Checked Then
      Me.opt_visits_entries.Checked = False
      m_type_report_activate = TYPE_OF_REPORT.EntrancesAccumulate
      Call EntrancesAccumulate(RowIndex, DbRow)
    End If
    Return True

  End Function 'GUI_SetupRow

  ''' <summary>
  '''Inserts total and subtotal
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_AfterLastRow()
    m_is_last_record = True
    If Me.Grid.NumRows > 1 Then
      If Me.opt_only_visits.Checked Then
        InsertTotalByMonthAccumulate(False, m_day_counter)
      Else
        InsertTotalByMonth()
      End If

      TotalGeneral()
    End If
    MyBase.GUI_AfterLastRow()
  End Sub 'GUI_AfterLastRow

  ''' <summary>
  ''' Get the defined Query Type
  ''' </summary>
  ''' <returns>ENUM_QUERY_TYPE</returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_COMMAND
  End Function ' GUI_GetQueryType

  ''' <summary>
  ''' Open additional form to show details for the select row
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ShowSelectedItem()
    Dim _idx_row As Int32
    Dim _frm_entrances As frm_customer_entrances_report
    Dim _list_parameters As List(Of String)
    Dim _dayNumber As Integer

    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    If (Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)) Then
      If Me.opt_visits_entries.Checked = False Then
        If IsCollapsed(GRID_COLUMN_VISIT_DATE, _idx_row) Then
          SetSignalAllowCollapse(GRID_COLUMN_VISIT_DATE, _idx_row)
          _dayNumber = LoadDayNumber(_idx_row)
          Me.Grid.CollapseExpand(_idx_row, _idx_row + _dayNumber, GRID_COLUMN_VISIT_DATE, 0)
        Else
          SetSignalAllowExpand(GRID_COLUMN_VISIT_DATE, _idx_row)
          _dayNumber = LoadDayNumber(_idx_row)
          Me.Grid.CollapseExpand(_idx_row, _idx_row + _dayNumber, GRID_COLUMN_VISIT_DATE, 1)
        End If
      End If
    ElseIf (Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)) Then
      Return
    ElseIf (Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_01)) Then
      Return
    Else
      _frm_entrances = New frm_customer_entrances_report
      _list_parameters = New List(Of String)

      If Me.opt_visits_entries.Checked Then
        _list_parameters.Add(Me.Grid.Cell(_idx_row, GRID_COLUMN_HOLDER_FULLNAME).Value)
        _list_parameters.Add(Me.Grid.Cell(_idx_row, GRID_COLUMN_VISIT_DAY_DETAIL).Value)
        _list_parameters.Add(Me.Grid.Cell(_idx_row, GRID_COLUMN_CARD_TRACK).Value)
        _list_parameters.Add(Me.Grid.Cell(_idx_row, GRID_COLUMN_INPUT_TYPE).Value)
        m_type_report_activate = TYPE_OF_REPORT.EntrancesByVisits
      ElseIf Me.opt_only_visits.Checked Then
        _list_parameters.Add(Me.Grid.Cell(_idx_row, GRID_COLUMN_VISIT_DATE).Value)
        m_type_report_activate = TYPE_OF_REPORT.EntrancesAccumulate
      End If

      _frm_entrances.ShowForEdit(Me.MdiParent, _list_parameters, m_type_report_activate)
      _list_parameters = Nothing
    End If

  End Sub ' GUI_EditSelectedItem

  ''' <summary>
  '''  SQL Command query ready to send to the database
  ''' </summary>
  ''' <returns>Build an SQL Command query from conditions set in the filters</returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_GetSqlCommand() As SqlCommand
    Dim _sql_command As SqlCommand
    Dim _string_SQL As String
    Dim _string_WHERE As String

    _sql_command = New SqlCommand()
    _string_SQL = String.Empty

    SetDefaultValues()
    If Me.opt_only_visits.Checked Then
      _string_SQL = "SELECT * FROM v_TotalEntriesByMonth "
    ElseIf Me.opt_visits_entries.Checked Then
      _string_SQL = "SELECT * FROM v_TotalEntrancesByVisits"
    End If

    If m_check_box Then
      _string_WHERE = " WHERE SQL_COLUMN_GAMING_DAY  >= @pInitDate AND SQL_COLUMN_GAMING_DAY <=@pFinalDate ORDER BY SQL_COLUMN_GAMING_DAY DESC "
    Else
      _string_WHERE = " WHERE SQL_COLUMN_GAMING_DAY  >= @pInitDate ORDER BY SQL_COLUMN_GAMING_DAY DESC"
    End If
    _sql_command.CommandType = CommandType.Text
    _sql_command.CommandText = String.Format("{0}{1}", _string_SQL, _string_WHERE)
    _sql_command.Parameters.Add("@pInitDate", SqlDbType.BigInt).Value = dtp_from.Value().ToString("yyyyMMdd")
    _sql_command.Parameters.Add("@pFinalDate", SqlDbType.BigInt).Value = dtp_to.Value().ToString("yyyyMMdd")

    Return _sql_command

  End Function 'GUI_GetSqlCommand

  ''' <summary>
  '''  Initialize every form control
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_InitControls()

    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_INFO).Visible = False

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7036) & ": " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7093)
    grp_box_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7110)
    Me.dtp_from.Value = WGDB.Now().Date.AddDays(-1)
    Me.dtp_from.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7094)
    Me.dtp_to.Value = WGDB.Now()
    Me.dtp_to.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7095)
    Me.grp_box_accum.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7130)
    Me.opt_visits_entries.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7098)
    Me.opt_only_visits.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7099)
    Me.opt_only_visits.Checked = True
    'Me.chk_total_entries_by_month.Checked = True
    m_type_report_activate = TYPE_OF_REPORT.EntrancesAccumulate
    Me.GUI_StyleSheet(m_type_report_activate)

    ' Hide counter of rows
    Me.Grid.PanelRightVisible = False

    ' Hidden counter on rows
    Me.Grid.PanelRightVisible = False
    Me.lbl_total_entradas.Visible = False
    Me.lbl_numero_total_entradas.Visible = False
    Me.lbl_total_entradas.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7595)

    m_rpt_entries = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7099)
    m_rpt_visits = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7098)

    Call MyBase.GUI_InitControls()
    Me.dtp_from.Focus()
  End Sub 'GUI_InitControls

  ''' <summary>
  ''' Define layout of all Main Grid Columns 
  ''' </summary>
  ''' <param name="TypeOfReport"></param>
  ''' <remarks></remarks>
  Private Sub GUI_StyleSheet(TypeOfReport As TYPE_OF_REPORT)

    m_last_month_to_compare = dtp_from.Value.Month.ToString

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, True)

      ' GRID INDEX
      .Column(GRID_COLUMN_ID).Header.Text = " "
      .Column(GRID_COLUMN_ID).Width = GRID_WIDTH_ID
      .Column(GRID_COLUMN_ID).HighLightWhenSelected = False
      .Column(GRID_COLUMN_ID).IsColumnPrintable = False

      ' VISIT_DATE
      .Column(GRID_COLUMN_VISIT_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6995)
      .Column(GRID_COLUMN_VISIT_DATE).Width = GRID_WIDTH_VISIT_DATE
      .Column(GRID_COLUMN_VISIT_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_VISIT_DATE).IsColumnPrintable = True

      ' VISIT_NUMBER
      .Column(GRID_COLUMN_VISIT_NUMBER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7096)
      .Column(GRID_COLUMN_VISIT_NUMBER).Width = GRID_WIDTH_VISIT_NUMBER
      .Column(GRID_COLUMN_VISIT_NUMBER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' NAME_HOLDER
      .Column(GRID_COLUMN_HOLDER_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(248)
      .Column(GRID_COLUMN_HOLDER_NAME).Width = GRID_WIDTH_HOLDER_NAME


      ' LASTNAME_HOLDER
      .Column(GRID_COLUMN_HOLDER_LASTNAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7477)
      .Column(GRID_COLUMN_HOLDER_LASTNAME).Width = GRID_WIDTH_HOLDER_LASTNAME



      'TRACKDATA
      .Column(GRID_COLUMN_CARD_TRACK).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(212)
      .Column(GRID_COLUMN_CARD_TRACK).Width = GRID_WIDTH_CARD_TRACK
      .Column(GRID_COLUMN_CARD_TRACK).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      'N. ENTRIES
      .Column(GRID_COLUMN_ENTRIES).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7097)
      .Column(GRID_COLUMN_ENTRIES).Width = GRID_WIDTH_ENTRIES
      .Column(GRID_COLUMN_ENTRIES).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'INPUT TYPE
      .Column(GRID_COLUMN_INPUT_TYPE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7614)
      .Column(GRID_COLUMN_INPUT_TYPE).Width = GRID_WIDTH_INPUT_TYPE
      .Column(GRID_COLUMN_INPUT_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      '.Column(GRID_COL).Header(0).Text = "HOLA QUE TAL "
      .Column(GRID_COLUMN_THEORICAL_COLLECTION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7100)
      .Column(GRID_COLUMN_THEORICAL_COLLECTION).Width = GRID_WIDTH_THEORICAL_COLLECTION
      ' .Column(GRID_COLUMN_THEORICAL_COLLECTION).HighLightWhenSelected = False
      ' .Column(GRID_COLUMN_THEORICAL_COLLECTION).IsColumnPrintable = False
      .Column(GRID_COLUMN_THEORICAL_COLLECTION).ColDataType = uc_grid.CLASS_COL_DATA.ENUM_COLDATATYPE.FLEX_DT_CURRENCY

      '.Column(GRID_COL).Header(0).Text = "HOLA QUE TAL "
      .Column(GRID_COLUMN_REAL_COLLECTION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7101)
      .Column(GRID_COLUMN_REAL_COLLECTION).Width = GRID_WIDTH_REAL_COLLECTION
      '  .Column(GRID_COLUMN_REAL_COLLECTION).HighLightWhenSelected = False
      ' .Column(GRID_COLUMN_REAL_COLLECTION).IsColumnPrintable = False
      .Column(GRID_COLUMN_REAL_COLLECTION).ColDataType = uc_grid.CLASS_COL_DATA.ENUM_COLDATATYPE.FLEX_DT_CURRENCY

      '.Column(GRID_COL).Header(0).Text = "HOLA QUE TAL "
      .Column(GRID_COLUMN_DIFFERENCE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7613)
      .Column(GRID_COLUMN_DIFFERENCE).Width = GRID_WIDTH_DIFFERENCE
      ' .Column(GRID_COLUMN_DIFFERENCE).HighLightWhenSelected = False
      '  .Column(GRID_COLUMN_DIFFERENCE).IsColumnPrintable = False
      .Column(GRID_COLUMN_DIFFERENCE).ColDataType = uc_grid.CLASS_COL_DATA.ENUM_COLDATATYPE.FLEX_DT_CURRENCY
      .Sortable = False

      If Me.opt_only_visits.Checked Then
        With Me.Grid
          .Column(GRID_COLUMN_HOLDER_NAME).Width = 0
          .Column(GRID_COLUMN_HOLDER_LASTNAME).Width = 0
          .Column(GRID_COLUMN_CARD_TRACK).Width = 0
          .Column(GRID_COLUMN_ENTRIES).Width = 0
          .Column(GRID_COLUMN_VISIT_DAY_DETAIL).Width = 0
          .Column(GRID_COLUMN_THEORICAL_BY_DAY).Width = 0
          .Column(GRID_COLUMN_INPUT_TYPE).Width = 0
        End With

      End If
      .Column(GRID_COLUMN_HOLDER_FULLNAME).Width = 0
    End With
  End Sub ' GUI_StyleSheet

  ''' <summary>
  '''  Process clicks on data grid (double-clicks) and select button
  ''' </summary>
  ''' <param name="ButtonId"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)
    Select Case ButtonId
      Case frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY
        SetDefaultValues()
        Call GUI_StyleSheet(m_type_report_activate)
        Call MyBase.GUI_ButtonClick(ButtonId)
      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        Call GUI_ShowSelectedItem()
      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub 'GUI_ButtonClick

  ''' <summary>
  ''' Initialize all form filters with their default values
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

#End Region

#Region " Public Methods"

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_SELECTION
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region

#Region "Events"

  Private Sub dtp_to_DatePickerValueChanged() Handles dtp_to.DatePickerValueChanged
    If dtp_to.Checked Then
      m_check_box = True
    Else
      m_check_box = False
    End If
  End Sub
#End Region

  Private Sub opt_only_visits_Click(sender As Object, e As EventArgs) Handles opt_only_visits.Click
    m_type_report_activate = TYPE_OF_REPORT.EntrancesAccumulate
  End Sub

  Private Sub opt_visits_entries_Click(sender As Object, e As EventArgs) Handles opt_visits_entries.Click
    m_type_report_activate = TYPE_OF_REPORT.Both
  End Sub

End Class