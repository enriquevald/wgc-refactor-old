﻿'-------------------------------------------------------------------
' Copyright © 2016 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_customer_entrance_ticket_sel.vb
' DESCRIPTION:   Form to manage Customer Entry Prices 
' AUTHOR:        Fernando Jiménez Cepeda
' CREATION DATE: 20-JAN-2016
'
' REVISION HISTORY:
'
' Date         Author     Description
' -----------  ------     -----------------------------------------------
' 20-JAN-2016  FJC        Initial version. (Product Backlog Item 8554:Visitas / Recepción: Crear ticket de entrada: GUI precios entrada)
' 18-MAR-2016  ETP        Fixed bug 10766 NLS corrected when check default checkbox.
' 09-MAY-2016  FJC        Product Backlog Item 12927:Visitas / Recepción: Caducidad de las entradas variable
' 09-MAY-2016  FJC        Fixed Bug 12965:Visitas / Recepción: No se graba correctamente la auditoría al eliminar un precio de entrada.
' 29-SEP-2016  FJC        PBI 17681:Visitas / Recepción: MEJORAS II - Agrupar menú (GUI)
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonMisc
Imports System.Text
Imports WSI.Common

Public Class frm_customer_entrance_prices_edit
    Inherits frm_base_edit

#Region " Enums"
    Private Enum ROWACTION
        Editable = 2
        Added = 5
        Updated = 6
    End Enum
#End Region

#Region " Constants"

    ' Grid Columns
    Private Const GRID_COLUMNS As Integer = 7
    Private Const GRID_HEADER_ROWS As Integer = 1

    Private Const GRID_COLUMN_INDEX As Integer = 0
    Private Const GRID_COLUMN_DESCRIPTION As Integer = 1
    Private Const GRID_COLUMN_CUSTOMER_LEVEL As Integer = 2
    Private Const GRID_COLUMN_PRICE As Integer = 3
    Private Const GRID_COLUMN_EXPIRATIONS_DAYS As Integer = 4
    Private Const GRID_COLUMN_CHECK_DEFAULT As Integer = 5
    Private Const GRID_COLUMN_EDITABLE As Integer = 6 '0-No editable, 1-Editable, 8-Deleted

    ' Grid Columns WIDTH
    Private Const GRID_COLUMN_INDEX_WIDTH As Integer = 0
    Private Const GRID_COLUMN_CHECK_DEFAULT_WIDTH As Integer = 500
    Private Const GRID_COLUMN_PRICE_WIDTH As Integer = 1700
    Private Const GRID_COLUMN_DESCRIPTION_WIDTH As Integer = 3000
    Private Const GRID_COLUMN_CUSTOMER_LEVEL_WIDTH As Integer = 1000
    Private Const GRID_COLUMN_VALID_GAMING_DAYS As Integer = 2000

#End Region

#Region " Members"
    Private m_dt_entrance_prices As DataTable
    Private m_dict_levels As Dictionary(Of Integer, String)
    Private m_min_level As Integer
    Private m_max_level As Integer
    Private m_min_expiration_gaming_days As Integer
    Private m_max_expiration_gaming_days As Integer


    Private m_index As Integer = 0
    Private m_raise_celldatachanged_event As Boolean
    Private m_old_value As Integer
#End Region

#Region " Overrides "

    ''' <summary>
    ''' To control the key pressed.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

        Select Case e.KeyChar
            Case Chr(Keys.Enter)

                If Me.dg_entrance_prices.ContainsFocus Then

                    Me.dg_entrance_prices.KeyPressed(sender, e)

                End If

                Return True
            Case Chr(Keys.Escape)

                Return MyBase.GUI_KeyPress(sender, e)
            Case Else
        End Select

        ' The keypress event is done in uc_grid control

    End Function ' GUI_KeyPress

    ''' <summary>
    ''' Establish Form Id, according to the enumeration in the application
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub GUI_SetFormId()
        Me.FormId = ENUM_FORM.FORM_CUSTOMER_ENTRANCES_PRICES

        Call MyBase.GUI_SetFormId()

    End Sub 'GUI_SetFormId

    ''' <summary>
    ''' Set focus to a control when first entering the form
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overrides Sub GUI_SetInitialFocus()
        Me.ActiveControl = Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL)
    End Sub      ' GUI_SetInitialFocus

    ''' <summary>
    ''' Initializes form controls
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overrides Sub GUI_InitControls()
        ' Initialize parent control, required
        Call MyBase.GUI_InitControls()

        m_dict_levels = New Dictionary(Of Integer, String)

        Call AddHandlers()

        ' Enable / Disable controls
        Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = False
        Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Visible = False

        Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Visible = True
        Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Enabled = True
        Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Text = GLB_NLS_GUI_CONTROLS.GetString(1)

        Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Enabled = Me.Permissions.Write
        Me.btn_add.Enabled = Me.Permissions.Write
        Me.btn_del.Enabled = Me.Permissions.Write

        Me.btn_add.Text = GLB_NLS_GUI_CONTROLS.GetString(12)
        Me.btn_del.Text = GLB_NLS_GUI_CONTROLS.GetString(11)

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7036) & ": " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7072)

        Me.gb_customer_entrances.Text = String.Empty

        Me.dg_entrance_prices.SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

        Me.lblLegend.ForeColor = Color.Navy

        m_min_expiration_gaming_days = 1
        m_max_expiration_gaming_days = 999


        m_raise_celldatachanged_event = False

        Call GUI_StyleSheet()

        Call GetLevels()

        Call SetScreenLevels()

    End Sub ' GUI_InitControls

    ''' <summary>
    ''' Get data from the screen
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overrides Sub GUI_GetScreenData()
        Dim _customer_entrance_price_edited As cls_customer_entrance_price
        Dim _row_select() As DataRow
        Dim _price_value As Decimal

        Try
            _customer_entrance_price_edited = Me.DbEditedObject
            For _idx_row As Integer = 0 To Me.dg_entrance_prices.NumRows - 1
                _row_select = _customer_entrance_price_edited.CustomerEntrancePricesAll.Select(cls_customer_entrance_price.SQL_COLUMN_ENTRANCE_PRICE_ID_DESCRIPTION & _
                                                                                              " = " & Me.dg_entrance_prices.Cell(_idx_row, GRID_COLUMN_INDEX).Value)
                If _row_select.Length >= 1 Then
                    _row_select(_row_select.Length - 1)(cls_customer_entrance_price.SQL_COLUMN_DESCRIPTION) = Me.dg_entrance_prices.Cell(_idx_row, GRID_COLUMN_DESCRIPTION).Value
                    _price_value = IIf(Not String.IsNullOrEmpty(Me.dg_entrance_prices.Cell(_idx_row, GRID_COLUMN_PRICE).Value.ToString()), CDec(Me.dg_entrance_prices.Cell(_idx_row, GRID_COLUMN_PRICE).Value), 0)
                    _row_select(_row_select.Length - 1)(cls_customer_entrance_price.SQL_COLUMN_PRICE) = GUI_FormatNumber(_price_value, 2)
                    _row_select(_row_select.Length - 1)(cls_customer_entrance_price.SQL_COLUMN_CUSTOMER_LEVEL) = Me.dg_entrance_prices.Cell(_idx_row, GRID_COLUMN_CUSTOMER_LEVEL).Value
                    _row_select(_row_select.Length - 1)(cls_customer_entrance_price.SQL_COLUMN_DEFAULT) = (Me.dg_entrance_prices.Cell(_idx_row, GRID_COLUMN_CHECK_DEFAULT).Value = uc_grid.GRID_CHK_CHECKED)
                    _row_select(_row_select.Length - 1)(cls_customer_entrance_price.SQL_COLUMN_VALID_GAMING_DAYS) = IIf(Not String.IsNullOrEmpty(Me.dg_entrance_prices.Cell(_idx_row, GRID_COLUMN_EXPIRATIONS_DAYS).Value), Me.dg_entrance_prices.Cell(_idx_row, GRID_COLUMN_EXPIRATIONS_DAYS).Value, 0)
                End If
            Next

        Catch ex As Exception
            Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, Me.Name, "GUI_GetScreenData", ex.Message)

        End Try

    End Sub ' GUI_GetScreenData

    ''' <summary>
    ''' Set initial data on the screen.
    ''' </summary>
    ''' <param name="SqlCtx"></param>
    ''' <remarks></remarks>
    Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
        Dim _customer_entrance_price As cls_customer_entrance_price
        Try

            _customer_entrance_price = Me.DbEditedObject
            SetDataGridEntrancePrices(_customer_entrance_price.CustomerEntrancePricesAll)

        Catch ex As Exception
            Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                                  Me.Name, _
                                  "GUI_SetScreenData", _
                                  ex.Message)

            Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(137), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ex.Message)

        Finally

        End Try

    End Sub        ' GUI_SetScreenData

    ''' <summary>
    ''' Validate the data presented on the screen.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Overrides Function GUI_IsScreenDataOk() As Boolean
        Dim _one_default As Boolean

        _one_default = False

        For _idx_row As Int32 = 0 To Me.dg_entrance_prices.NumRows - 1

            'Check Description
            If String.IsNullOrEmpty(dg_entrance_prices.Cell(_idx_row, GRID_COLUMN_DESCRIPTION).Value.Trim) Then
                Call dg_entrance_prices.Focus()

                Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(6860))

                Return False
            End If

            'Check price
            If String.IsNullOrEmpty(dg_entrance_prices.Cell(_idx_row, GRID_COLUMN_PRICE).Value.Trim) Then
                Call dg_entrance_prices.Focus()

                Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(318))

                Return False
            End If

            'Check Expiration gaming Days
            If String.IsNullOrEmpty(dg_entrance_prices.Cell(_idx_row, GRID_COLUMN_EXPIRATIONS_DAYS).Value.Trim) OrElse _
              dg_entrance_prices.Cell(_idx_row, GRID_COLUMN_EXPIRATIONS_DAYS).Value < m_min_expiration_gaming_days Then

                dg_entrance_prices.Cell(_idx_row, GRID_COLUMN_EXPIRATIONS_DAYS).Value = 1
                Call dg_entrance_prices.Focus()

                Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1190), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(7301), m_min_expiration_gaming_days, m_max_expiration_gaming_days)

                Return False
            End If

            'Check level
            If String.IsNullOrEmpty(dg_entrance_prices.Cell(_idx_row, GRID_COLUMN_CUSTOMER_LEVEL).Value.Trim) Then

                Call dg_entrance_prices.Focus()
                Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(7008))

                Return False

            ElseIf Not Me.m_dict_levels.ContainsKey(dg_entrance_prices.Cell(_idx_row, GRID_COLUMN_CUSTOMER_LEVEL).Value) AndAlso _
                   dg_entrance_prices.Cell(_idx_row, GRID_COLUMN_CUSTOMER_LEVEL).Value <> 0 Then

                Call dg_entrance_prices.Focus()
                Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1190), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(7008), m_min_level, m_max_level)

                Return False

            ElseIf dg_entrance_prices.Cell(_idx_row, GRID_COLUMN_CUSTOMER_LEVEL).Value <> 0 AndAlso _
               dg_entrance_prices.Cell(_idx_row, GRID_COLUMN_CHECK_DEFAULT).Value = uc_grid.GRID_CHK_CHECKED Then

                Call dg_entrance_prices.Focus()
                Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7107), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , m_min_level)

                Return False

            ElseIf (Not Me.m_dict_levels.ContainsKey(dg_entrance_prices.Cell(_idx_row, GRID_COLUMN_CUSTOMER_LEVEL).Value) AndAlso _
                    dg_entrance_prices.Cell(_idx_row, GRID_COLUMN_CUSTOMER_LEVEL).Value <> m_min_level) AndAlso _
                    dg_entrance_prices.Cell(_idx_row, GRID_COLUMN_CHECK_DEFAULT).Value = uc_grid.GRID_CHK_UNCHECKED Then

                Call dg_entrance_prices.Focus()
                Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1190), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(7008), m_min_level, m_max_level)

                Return False

            End If

            'Check Default
            If Not _one_default AndAlso _
               Not String.IsNullOrEmpty(dg_entrance_prices.Cell(_idx_row, GRID_COLUMN_CHECK_DEFAULT).Value) AndAlso _
               dg_entrance_prices.Cell(_idx_row, GRID_COLUMN_CHECK_DEFAULT).Value = uc_grid.GRID_CHK_CHECKED Then

                _one_default = True
            End If
        Next

        If Not _one_default Then

            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7073), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)

            Return False
        End If

        Return True
    End Function ' GUI_IsScreenDataOk

    ''' <summary>
    ''' Database overridable operations. Define specific DB operation for this form.
    ''' </summary>
    ''' <param name="DbOperation"></param>
    ''' <remarks></remarks>
    Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)

        Select Case DbOperation
            Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
                Me.DbEditedObject = New cls_customer_entrance_price()

            Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
                If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
                    Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
                End If

            Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
                If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
                    Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
                End If

            Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
                If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
                    Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
                End If

            Case Else
                Call MyBase.GUI_DB_Operation(DbOperation)

        End Select
    End Sub         ' GUI_DB_Operation

    ''' <summary>
    ''' Process clicks on data grid (double-clicks) and select button
    ''' </summary>
    ''' <param name="ButtonId"></param>
    ''' <remarks></remarks>
    Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

        MyBase.GUI_ButtonClick(ButtonId)

    End Sub          ' GUI_ButtonClick

#End Region

#Region " Public Methods "

    ' PURPOSE: Opens dialog with selection mode settings
    '
    '  PARAMS:
    '     - INPUT:
    '           - CageSessionId
    '     - OUTPUT:
    '           - None
    '
    ' RETURNS:
    '     - None
    '
    Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)
        Me.MdiParent = MdiParent
        Me.Display(False)

    End Sub

#End Region

#Region " Private Methods"

    ''' <summary>
    ''' Loads Data of Entrance Prices to the Grid
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SetDataGridEntrancePrices(ByVal CustomerEntrancePrices As DataTable)
        Dim _idx_row As Integer

        Try
            Call RemoveHandlers()

            For Each _dt_row As DataRow In CustomerEntrancePrices.Rows
                Me.dg_entrance_prices.AddRow()

                Me.dg_entrance_prices.Cell(_idx_row, GRID_COLUMN_INDEX).Value = _dt_row(cls_customer_entrance_price.SQL_COLUMN_ENTRANCE_PRICE_ID)
                Me.dg_entrance_prices.Cell(_idx_row, GRID_COLUMN_DESCRIPTION).Value = _dt_row(cls_customer_entrance_price.SQL_COLUMN_DESCRIPTION)
                Me.dg_entrance_prices.Cell(_idx_row, GRID_COLUMN_PRICE).Value = GUI_FormatCurrency(_dt_row(cls_customer_entrance_price.SQL_COLUMN_PRICE), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
                Me.dg_entrance_prices.Cell(_idx_row, GRID_COLUMN_CUSTOMER_LEVEL).Value = _dt_row(cls_customer_entrance_price.SQL_COLUMN_CUSTOMER_LEVEL)
                Me.dg_entrance_prices.Cell(_idx_row, GRID_COLUMN_EXPIRATIONS_DAYS).Value = _dt_row(cls_customer_entrance_price.SQL_COLUMN_VALID_GAMING_DAYS)
                Me.dg_entrance_prices.Cell(_idx_row, GRID_COLUMN_CHECK_DEFAULT).Value = IIf(_dt_row(cls_customer_entrance_price.SQL_COLUMN_DEFAULT), uc_grid.GRID_CHK_CHECKED, uc_grid.GRID_CHK_UNCHECKED)
                _idx_row += 1

            Next
            If dg_entrance_prices.NumRows > 0 Then
                Me.dg_entrance_prices.IsSelected(0) = True
            End If

            Call AddHandlers()

        Catch _ex As Exception

            WSI.Common.Log.Message("Error in SetDataGridEntrancePrices() ")
            WSI.Common.Log.Exception(_ex)
        End Try

    End Sub ' SetDataGridEntrancePrices

    ''' <summary>
    ''' Define layout of all Main Grid Columns 
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Private Sub GUI_StyleSheet()
        With Me.dg_entrance_prices

            Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, True)

            .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

            ' Index
            .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
            .Column(GRID_COLUMN_INDEX).Width = GRID_COLUMN_INDEX_WIDTH
            .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
            .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False
            .Column(GRID_COLUMN_INDEX).Editable = False

            ' Description
            .Column(GRID_COLUMN_DESCRIPTION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6860)
            .Column(GRID_COLUMN_DESCRIPTION).Width = GRID_COLUMN_DESCRIPTION_WIDTH
            .Column(GRID_COLUMN_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
            .Column(GRID_COLUMN_DESCRIPTION).Editable = Me.Permissions.Write
            .Column(GRID_COLUMN_DESCRIPTION).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
            .Column(GRID_COLUMN_DESCRIPTION).EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

            ' Level
            .Column(GRID_COLUMN_CUSTOMER_LEVEL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7008)
            .Column(GRID_COLUMN_CUSTOMER_LEVEL).Width = GRID_COLUMN_CUSTOMER_LEVEL_WIDTH
            .Column(GRID_COLUMN_CUSTOMER_LEVEL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
            .Column(GRID_COLUMN_CUSTOMER_LEVEL).Editable = Me.Permissions.Write
            .Column(GRID_COLUMN_CUSTOMER_LEVEL).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
            .Column(GRID_COLUMN_CUSTOMER_LEVEL).EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 1, 0)

            ' Price
            .Column(GRID_COLUMN_PRICE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(318)
            .Column(GRID_COLUMN_PRICE).Width = GRID_COLUMN_PRICE_WIDTH
            .Column(GRID_COLUMN_PRICE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
            .Column(GRID_COLUMN_PRICE).Editable = Me.Permissions.Write
            .Column(GRID_COLUMN_PRICE).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
            .Column(GRID_COLUMN_PRICE).EditionControl.EntryField.SetFilter(FORMAT_MONEY, 6, 2)

            ' Expiration Days
            .Column(GRID_COLUMN_EXPIRATIONS_DAYS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7301)
            .Column(GRID_COLUMN_EXPIRATIONS_DAYS).Width = GRID_COLUMN_VALID_GAMING_DAYS
            .Column(GRID_COLUMN_EXPIRATIONS_DAYS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
            .Column(GRID_COLUMN_EXPIRATIONS_DAYS).Editable = Me.Permissions.Write
            .Column(GRID_COLUMN_EXPIRATIONS_DAYS).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
            .Column(GRID_COLUMN_EXPIRATIONS_DAYS).EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3, 0)

            ' Check by default
            .Column(GRID_COLUMN_CHECK_DEFAULT).Header(0).Text = "  "
            .Column(GRID_COLUMN_CHECK_DEFAULT).Width = GRID_COLUMN_CHECK_DEFAULT_WIDTH
            .Column(GRID_COLUMN_CHECK_DEFAULT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
            .Column(GRID_COLUMN_CHECK_DEFAULT).Editable = Me.Permissions.Write
            .Column(GRID_COLUMN_CHECK_DEFAULT).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX

            ' Editable
            .Column(GRID_COLUMN_EDITABLE).Header(0).Text = "  "
            .Column(GRID_COLUMN_EDITABLE).Header(1).Text = "  "
            .Column(GRID_COLUMN_EDITABLE).Width = 0

        End With

    End Sub ' GUI_StyleSheet

    ''' <summary>
    ''' Add to Grid new row (entrance price)
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub InsertEntrancePrice()
        Dim _row As Int32
        Dim _edit As cls_customer_entrance_price
        Dim _dt_row As DataRow

        If Not Permissions.Write Then
            Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

            Exit Sub
        End If

        m_index += -1

        With dg_entrance_prices
            _row = .AddRow()

            .ClearSelection()
            .IsSelected(_row) = True
            .Redraw = True
            .TopRow = _row

            .Cell(_row, GRID_COLUMN_INDEX).Value = m_index
            .Cell(_row, GRID_COLUMN_CUSTOMER_LEVEL).Value = 0
            .Cell(_row, GRID_COLUMN_DESCRIPTION).Value = String.Empty
            .Cell(_row, GRID_COLUMN_PRICE).Value = GUI_FormatNumber(0, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
            .Cell(_row, GRID_COLUMN_EXPIRATIONS_DAYS).Value = GUI_FormatNumber(Me.m_min_expiration_gaming_days, 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

            .Cell(_row, GRID_COLUMN_EDITABLE).Value = ROWACTION.Added
            .CurrentRow = _row
            .Focus()
        End With
        _edit = Me.DbEditedObject

        _dt_row = _edit.CustomerEntrancePricesAll.NewRow()
        _dt_row(cls_customer_entrance_price.SQL_COLUMN_ENTRANCE_PRICE_ID) = m_index
        _dt_row(cls_customer_entrance_price.SQL_COLUMN_PRICE) = 0
        _dt_row(cls_customer_entrance_price.SQL_COLUMN_CUSTOMER_LEVEL) = 0
        _dt_row(cls_customer_entrance_price.SQL_COLUMN_DESCRIPTION) = String.Empty
        _dt_row(cls_customer_entrance_price.SQL_COLUMN_VALID_GAMING_DAYS) = 0

        _edit.CustomerEntrancePricesAll.Rows.Add(_dt_row)

    End Sub ' InsertEntrancePrice

    ''' <summary>
    ''' Check if can delete a row
    ''' </summary>
    ''' <param name="Type"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function EvaluateDelete(Optional ByVal Type As CageCurrencyType = -1) As Boolean
        Dim _can_be_deleted As Boolean

        _can_be_deleted = True

        Try
            If Not Permissions.Delete Then
                Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

                Return False

            End If

            If Not Me.dg_entrance_prices.SelectedRows Is Nothing AndAlso _
               Me.dg_entrance_prices.SelectedRows.Length > 0 Then

                For Each _selected_row As Integer In Me.dg_entrance_prices.SelectedRows

                    If Me.dg_entrance_prices.Cell(_selected_row, GRID_COLUMN_CHECK_DEFAULT).Value = uc_grid.GRID_CHK_CHECKED Then
                        _can_be_deleted = False

                        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7092), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)

                        Exit For
                    End If
                Next

                If _can_be_deleted Then
                    DeleteSelectedRows(Me.dg_entrance_prices)

                    If Me.dg_entrance_prices.NumRows > 0 Then

                        Me.dg_entrance_prices.IsSelected(0) = True
                    End If
                End If

            End If

        Catch ex As Exception
            WSI.Common.Log.Message("Error in EvaluateDelete() ")
            WSI.Common.Log.Exception(ex)
        End Try

        Return _can_be_deleted
    End Function 'EvaluateDelete

    ''' <summary>
    ''' Performs a delete action in the grid
    ''' </summary>
    ''' <param name="Grid"></param>
    ''' <remarks></remarks>
    Private Sub DeleteSelectedRows(ByVal Grid As GUI_Controls.uc_grid)
        Dim _customer_entrance_prices As cls_customer_entrance_price
        Dim _customer_entrance_prices_selected() As DataRow
        Dim _selected_rows() As Integer
        Dim _sql As String

        _selected_rows = Grid.SelectedRows

        If IsNothing(_selected_rows) Then
            Exit Sub
        End If

        _customer_entrance_prices = Me.DbEditedObject

        For Each _selected_row As Integer In Grid.SelectedRows

            _sql = cls_customer_entrance_price.SQL_COLUMN_ENTRANCE_PRICE_ID_DESCRIPTION & " = " & Grid.Cell(_selected_row, cls_customer_entrance_price.SQL_COLUMN_ENTRANCE_PRICE_ID).Value
            _customer_entrance_prices_selected = _customer_entrance_prices.CustomerEntrancePricesAll.Select(_sql)

            If _customer_entrance_prices_selected.Length > 0 Then
                _customer_entrance_prices_selected(0)(cls_customer_entrance_price.SQL_COLUMN_ROWSTATE) = System.Data.DataRowState.Deleted
            End If

            Grid.IsSelected(_selected_row) = False
            Grid.DeleteRowFast(_selected_row)

            Call GUI_GetScreenData()

            Exit For

        Next

    End Sub ' DeleteSelectedRows

    ''' <summary>
    ''' Check and set only one row is checked
    ''' </summary>
    ''' <param name="Grid"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CheckIfOnlyOneChecked(ByVal Grid As GUI_Controls.uc_grid)
        Dim _idx As Integer
        Dim _num_checks As Integer

        _num_checks = 0
        _idx = 0

        For _idx = 0 To Grid.NumRows - 1
            Grid.Cell(_idx, GRID_COLUMN_CHECK_DEFAULT).Value = uc_grid.GRID_CHK_UNCHECKED
            If Grid.Cell(_idx, GRID_COLUMN_CUSTOMER_LEVEL).Value = 0 Then
                Grid.Cell(_idx, GRID_COLUMN_CUSTOMER_LEVEL).Value = 1
            End If
        Next

        Return (_num_checks = 1)

    End Function ' CheckIfOnlyOneCheck

    ''' <summary>
    ''' Set text legend
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SetScreenLevels()
        Me.lblLegend.Text = String.Empty
        Me.lblLegend.Text = "* " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7008) & ": "
        For Each _kvp As KeyValuePair(Of Integer, String) In m_dict_levels
            Me.lblLegend.Text &= _kvp.Key & ": -" & _kvp.Value & "-  "

        Next
    End Sub ' SetScreenLevels

    ''' <summary>
    ''' Get diferent Levels from GP
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetLevels() As Boolean
        Dim _idx As Integer
        Dim _level_name As String

        Try
            _level_name = String.Empty
            m_dict_levels.Clear()

            For _idx = 1 To 4
                _level_name = GeneralParam.GetString("PlayerTracking", String.Format("Level0{0}.Name", _idx), String.Empty)
                If Not _level_name = String.Empty Then
                    m_dict_levels.Add(_idx, _level_name)

                End If
            Next

            m_min_level = 0
            m_max_level = m_dict_levels.Values.Count
            m_dict_levels.Add(m_min_level, "Default")

            Return True

        Catch ex As Exception

            WSI.Common.Log.Message("Error in SetDataGridEntrancePrices() ")

        End Try

        Return False

    End Function ' GetLevels

    ''' <summary>
    ''' Loads Old value in a cell when not is correct
    ''' </summary>
    ''' <param name="Row"></param>
    ''' <param name="Column"></param>
    ''' <remarks></remarks>
    Private Sub LoadOldValue(ByVal Row As Integer, ByVal Column As Integer)

        Call RemoveHandlers()

        Me.dg_entrance_prices.Cell(Row, Column).Value = m_old_value

        Call AddHandlers()

    End Sub ' LoadOldValue

    ''' <summary>
    ''' Add Handlers to uc_grid
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub AddHandlers()
        AddHandler dg_entrance_prices.RowSelectedEvent, AddressOf dg_entrance_prices_RowSelectedEvent
        AddHandler dg_entrance_prices.CellDataChangedEvent, AddressOf dg_entrance_prices_CellDataChangedEvent
    End Sub

    ''' <summary>
    ''' Remove Handlers to uc_grid
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub RemoveHandlers()
        RemoveHandler dg_entrance_prices.RowSelectedEvent, AddressOf dg_entrance_prices_RowSelectedEvent
        RemoveHandler dg_entrance_prices.CellDataChangedEvent, AddressOf dg_entrance_prices_CellDataChangedEvent
    End Sub

#End Region

#Region " Events"

    ''' <summary>
    ''' Button click Add Event
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub btn_add_ClickEvent() Handles btn_add.ClickEvent
        InsertEntrancePrice()
    End Sub

    ''' <summary>
    ''' Button click Delete Event
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub btn_del_ClickEvent() Handles btn_del.ClickEvent
        EvaluateDelete()
    End Sub

    ''' <summary>
    ''' Manage event BeforeStartEdition
    ''' </summary>
    ''' <param name="Row"></param>
    ''' <param name="Column"></param>
    ''' <param name="EditionCanceled"></param>
    ''' <remarks></remarks>
    Private Sub dg_entrance_prices_BeforeStartEditionEvent(Row As Integer, Column As Integer, ByRef EditionCanceled As Boolean) Handles dg_entrance_prices.BeforeStartEditionEvent
        Select Case Column
            Case GRID_COLUMN_CUSTOMER_LEVEL

                m_old_value = dg_entrance_prices.Cell(Row, Column).Value

                m_raise_celldatachanged_event = True
            Case GRID_COLUMN_CHECK_DEFAULT

                'If (Not String.IsNullOrEmpty(Me.dg_entrance_prices.Cell(Row, GRID_COLUMN_CUSTOMER_LEVEL).Value) AndAlso _
                '    Me.dg_entrance_prices.Cell(Row, GRID_COLUMN_CUSTOMER_LEVEL).Value >= m_min_level + 1 AndAlso _
                '    Me.dg_entrance_prices.Cell(Row, GRID_COLUMN_CUSTOMER_LEVEL).Value <= m_max_level) Then

                '  EditionCanceled = True
                '  Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7108), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , m_min_level)

                'End If
        End Select
    End Sub ' dg_entrance_prices_BeforeStartEditionEvent

    ''' <summary>
    ''' Manage event CellDataChanged
    ''' </summary>
    ''' <param name="Row"></param>
    ''' <param name="Column"></param>
    ''' <remarks></remarks>
    Private Sub dg_entrance_prices_CellDataChangedEvent(Row As Integer, Column As Integer)

        Select Case Column

            Case GRID_COLUMN_CHECK_DEFAULT

                CheckIfOnlyOneChecked(Me.dg_entrance_prices)

                Me.dg_entrance_prices.Cell(Row, Column).Value = uc_grid.GRID_CHK_CHECKED
                Me.dg_entrance_prices.Cell(Row, GRID_COLUMN_CUSTOMER_LEVEL).Value = 0

            Case GRID_COLUMN_CUSTOMER_LEVEL

                If m_raise_celldatachanged_event Then

                    If String.IsNullOrEmpty(Me.dg_entrance_prices.Cell(Row, Column).Value) Then

                        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1190), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(7008), m_min_level, m_max_level)

                        LoadOldValue(Row, Column)

                    ElseIf Not Me.m_dict_levels.ContainsKey(Me.dg_entrance_prices.Cell(Row, Column).Value) AndAlso _
                           Me.dg_entrance_prices.Cell(Row, GRID_COLUMN_CHECK_DEFAULT).Value = uc_grid.GRID_CHK_UNCHECKED Then

                        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1190), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(7008), m_min_level + 1, m_max_level)

                        LoadOldValue(Row, Column)

                    ElseIf (Me.dg_entrance_prices.Cell(Row, Column).Value >= m_min_level + 1 AndAlso _
                            Me.dg_entrance_prices.Cell(Row, Column).Value <= m_max_level) AndAlso _
                            Me.dg_entrance_prices.Cell(Row, GRID_COLUMN_CHECK_DEFAULT).Value = uc_grid.GRID_CHK_CHECKED Then

                        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , m_min_level + 1, m_max_level)

                        LoadOldValue(Row, Column)
                    End If
                Else
                    LoadOldValue(Row, Column)
                End If

        End Select

        m_raise_celldatachanged_event = False
    End Sub ' dg_entrance_prices_CellDataChangedEvent

    ''' <summary>
    ''' Manage event rowselected 
    ''' </summary>
    ''' <param name="SelectedRow"></param>
    ''' <remarks></remarks>
    Private Sub dg_entrance_prices_RowSelectedEvent(ByVal SelectedRow As Integer)

    End Sub ' dg_entrance_prices_RowSelectedEvent

#End Region

End Class