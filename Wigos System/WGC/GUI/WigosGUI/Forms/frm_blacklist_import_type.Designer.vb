﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_blacklist_import_type
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_types = New System.Windows.Forms.GroupBox()
    Me.btn_add = New GUI_Controls.uc_button()
    Me.btn_del = New GUI_Controls.uc_button()
    Me.dg_types = New GUI_Controls.uc_grid()
    Me.panel_data.SuspendLayout()
    Me.gb_types.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_types)
    Me.panel_data.Size = New System.Drawing.Size(484, 291)
    '
    'gb_types
    '
    Me.gb_types.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.gb_types.Controls.Add(Me.btn_add)
    Me.gb_types.Controls.Add(Me.btn_del)
    Me.gb_types.Controls.Add(Me.dg_types)
    Me.gb_types.Location = New System.Drawing.Point(9, 5)
    Me.gb_types.Name = "gb_types"
    Me.gb_types.Size = New System.Drawing.Size(472, 283)
    Me.gb_types.TabIndex = 1
    Me.gb_types.TabStop = False
    Me.gb_types.Text = "xTypes"
    '
    'btn_add
    '
    Me.btn_add.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btn_add.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_add.Location = New System.Drawing.Point(6, 253)
    Me.btn_add.Name = "btn_add"
    Me.btn_add.Size = New System.Drawing.Size(50, 21)
    Me.btn_add.TabIndex = 3
    Me.btn_add.ToolTipped = False
    Me.btn_add.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.ADD_DELETE
    '
    'btn_del
    '
    Me.btn_del.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btn_del.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_del.Location = New System.Drawing.Point(416, 253)
    Me.btn_del.Name = "btn_del"
    Me.btn_del.Size = New System.Drawing.Size(50, 21)
    Me.btn_del.TabIndex = 4
    Me.btn_del.ToolTipped = False
    Me.btn_del.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.ADD_DELETE
    '
    'dg_types
    '
    Me.dg_types.CurrentCol = -1
    Me.dg_types.CurrentRow = -1
    Me.dg_types.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_types.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_types.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_types.Location = New System.Drawing.Point(7, 20)
    Me.dg_types.Name = "dg_types"
    Me.dg_types.PanelRightVisible = False
    Me.dg_types.Redraw = True
    Me.dg_types.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    Me.dg_types.Size = New System.Drawing.Size(459, 227)
    Me.dg_types.Sortable = False
    Me.dg_types.SortAscending = True
    Me.dg_types.SortByCol = 0
    Me.dg_types.TabIndex = 2
    Me.dg_types.ToolTipped = True
    Me.dg_types.TopRow = -2
    '
    'frm_blacklist_import_type
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(586, 302)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_blacklist_import_type"
    Me.Text = "frm_customer_entrance_ticket_sel"
    Me.panel_data.ResumeLayout(False)
    Me.gb_types.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_types As System.Windows.Forms.GroupBox
  Friend WithEvents btn_add As GUI_Controls.uc_button
  Friend WithEvents btn_del As GUI_Controls.uc_button
  Friend WithEvents dg_types As GUI_Controls.uc_grid
End Class
