'-------------------------------------------------------------------
' Copyright � 2008 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_sw_package_versions
'
' DESCRIPTION : This screen allows to browse the software packages
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 08-SEP-2008  TJG    Initial version
' 25-APR-2013  RBG    #746 It makes no sense to work in this form with multiselection
' 30-SEP-2014  OPC    Canged the format of the column Client Id on excel report.
' 08-SEP-2017  RAB    Bug 29680: WIGOS-3662 Title is cut when exporting data in Component updates screen
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data

Public Class frm_sw_package_versions
  Inherits frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents cb_latest As System.Windows.Forms.CheckBox
  Friend WithEvents uc_sw_list As GUI_Controls.uc_sw_products
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.gb_date = New System.Windows.Forms.GroupBox
    Me.cb_latest = New System.Windows.Forms.CheckBox
    Me.dtp_to = New GUI_Controls.uc_date_picker
    Me.dtp_from = New GUI_Controls.uc_date_picker
    Me.uc_sw_list = New GUI_Controls.uc_sw_products
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.uc_sw_list)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Size = New System.Drawing.Size(530, 340)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_sw_list, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 344)
    Me.panel_data.Size = New System.Drawing.Size(530, 386)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(524, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(524, 10)
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.cb_latest)
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(4, 241)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(347, 96)
    Me.gb_date.TabIndex = 2
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'cb_latest
    '
    Me.cb_latest.Location = New System.Drawing.Point(87, 19)
    Me.cb_latest.Name = "cb_latest"
    Me.cb_latest.Size = New System.Drawing.Size(140, 17)
    Me.cb_latest.TabIndex = 0
    Me.cb_latest.Text = "xLatest"
    Me.cb_latest.UseVisualStyleBackColor = True
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(36, 66)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(261, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 2
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2003, 5, 20, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(36, 42)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(261, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 1
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2003, 5, 20, 0, 0, 0, 0)
    '
    'uc_sw_list
    '
    Me.uc_sw_list.Location = New System.Drawing.Point(1, 2)
    Me.uc_sw_list.Name = "uc_sw_list"
    Me.uc_sw_list.Size = New System.Drawing.Size(356, 240)
    Me.uc_sw_list.TabIndex = 1
    '
    'frm_sw_package_versions
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(538, 734)
    Me.Name = "frm_sw_package_versions"
    Me.Text = "frm_sw_package_versions"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Constants "

  Private Const SQL_COLUMN_CLIENT_ID As Integer = 0
  Private Const SQL_COLUMN_BUILD_ID As Integer = 1
  Private Const SQL_COLUMN_TARGET As Integer = 2
  Private Const SQL_COLUMN_INSERTION_DATE As Integer = 3

  Private Const GRID_COLUMN_CLIENT_ID As Integer = 0
  Private Const GRID_COLUMN_BUILD_ID As Integer = 1
  Private Const GRID_COLUMN_BUILD_TEXT As Integer = 2
  Private Const GRID_COLUMN_TARGET As Integer = 3
  Private Const GRID_COLUMN_TARGET_TEXT As Integer = 4
  Private Const GRID_COLUMN_INSERTION_DATE As Integer = 5

  Private Const GRID_COLUMNS As Integer = 6
  Private Const GRID_HEADER_ROWS As Integer = 2

  Private Const GRID_EXCEL_COLUMN_CLIENT_ID As Integer = 0

#End Region ' Constants

#Region " Members "

  Private m_refreshing_grid As Boolean

  ' For report filters 
  Private m_terminal_type As String
  Private m_date_from As String
  Private m_date_to As String
  Private m_latest As String

#End Region ' Members

#Region " OVERRIDES "

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_SW_DOWNLOAD_VERSIONS

    ' TJG 01-FEB-2005
    ' Set the form icon from the embedded resource (ICO file must be built in the project as "Embedded Resource")
    ' Call could be made as GetManifestResourceStream("GUI_Auditor.GUI_Auditor.ico"))
    'Me.Icon = New Icon(System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream(Me.GetType(), "WigosGUI.ico"))

    '------------------------------------------------
    'XVV 13/04/2007
    'Call Base Form proc
    Call MyBase.GUI_SetFormId()
    '------------------------------------------------

  End Sub ' GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(201)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = True
    ' Cancel button text =>  "Exit"
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(2)

    ' Insertion Date
    Me.gb_date.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(205)
    Me.cb_latest.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(228)
    Me.dtp_from.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(206)
    Me.dtp_to.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(207)

    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    Call GUI_StyleSheet()

    Call uc_sw_list.Init()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()

  End Sub ' GUI_FilterReset

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_SELECT
        Call ShowPackageDetails()

      Case ENUM_BUTTON.BUTTON_NEW
        Call AddNewPackage()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked And Me.cb_latest.Checked = False Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        ' 101 - "Error in dates interval: Initial date must be earlier than the final date."
        Call NLS_MsgBox(GLB_NLS_GUI_SW_DOWNLOAD.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    Return True

  End Function ' GUI_FilterCheck

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim str_sql As String

    If Me.cb_latest.Checked = True Then
      str_sql = "select TSV_CLIENT_ID, TSV_BUILD_ID, X.PACKET, X.LATEST" _
              & " from ( select MAX(TSV_INSERTION_DATE) LATEST, TSV_TERMINAL_TYPE PACKET" _
                       & " from TERMINAL_SOFTWARE_VERSIONS" _
                   & " group by TSV_TERMINAL_TYPE) X, " _
                   & " TERMINAL_SOFTWARE_VERSIONS" _
              & " where X.PACKET = TSV_TERMINAL_TYPE" _
                & " and X.LATEST = TSV_INSERTION_DATE"
      str_sql = str_sql & GetSqlWhere()
      str_sql = str_sql & " order by TSV_TERMINAL_TYPE ASC"

    Else
      str_sql = "select TSV_CLIENT_ID" _
                   & ", TSV_BUILD_ID" _
                   & ", TSV_TERMINAL_TYPE" _
                   & ", TSV_INSERTION_DATE" _
               & " from TERMINAL_SOFTWARE_VERSIONS"
      str_sql = str_sql & GetSqlWhere()
      str_sql = str_sql & " ORDER BY TSV_CLIENT_ID, TSV_BUILD_ID ASC"

    End If

    Return str_sql

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Me.Grid.Cell(RowIndex, GRID_COLUMN_CLIENT_ID).Value = DbRow.Value(SQL_COLUMN_CLIENT_ID)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_BUILD_ID).Value = DbRow.Value(SQL_COLUMN_BUILD_ID)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_BUILD_TEXT).Value = CLASS_SW_PACKAGE.FormatBuild(DbRow.Value(SQL_COLUMN_CLIENT_ID), DbRow.Value(SQL_COLUMN_BUILD_ID))
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TARGET).Value = DbRow.Value(SQL_COLUMN_TARGET)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TARGET_TEXT).Value = CLASS_SW_PACKAGE.GetTerminalTypeName(DbRow.Value(SQL_COLUMN_TARGET))
    Me.Grid.Cell(RowIndex, GRID_COLUMN_INSERTION_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_INSERTION_DATE), , ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    Return True

  End Function ' GUI_SetupRow

  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.uc_sw_list

  End Sub 'GUI_SetInitialFocus

#Region " GUI Reports "

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_SW_DOWNLOAD.GetString(234) & " " & GLB_NLS_GUI_SW_DOWNLOAD.GetString(206), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_SW_DOWNLOAD.GetString(234) & " " & GLB_NLS_GUI_SW_DOWNLOAD.GetString(207), m_date_to)
    PrintData.SetFilter(cb_latest.Text, m_latest)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(592), m_terminal_type)
    PrintData.FilterHeaderWidth(1) = 1500
    PrintData.FilterHeaderWidth(2) = 1500

  End Sub ' GUI_ReportFilter

  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)
    MyBase.GUI_ReportParams(ExcelData, FirstColIndex)

    ExcelData.SetColumnFormat(GRID_EXCEL_COLUMN_CLIENT_ID, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)
    ExcelData.TitleFontSize = 17
    ExcelData.SetReportTitle()

  End Sub

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Params.Title = GLB_NLS_GUI_SW_DOWNLOAD.GetString(201)

    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_PORTRAIT


  End Sub ' GUI_ReportParams

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_date_from = ""
    m_date_to = ""
    m_terminal_type = ""
    m_latest = ""

    If cb_latest.Checked = True Then
      m_latest = GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_latest = GLB_NLS_GUI_INVOICING.GetString(480)
    End If

    ' Insertion Date
    If Me.dtp_from.Checked Then
      m_date_from = GUI_FormatDate(dtp_from.Value, _
                                   ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    End If

    If Me.dtp_to.Checked Then
      m_date_to = GUI_FormatDate(dtp_to.Value, _
                                 ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    m_terminal_type = uc_sw_list.GetSwProductsSelectedNames()

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE : Opens dialog with default settings for edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE : Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS :

  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Client
      .Column(GRID_COLUMN_CLIENT_ID).Header(0).Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(230)
      .Column(GRID_COLUMN_CLIENT_ID).Header(1).Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(231)
      .Column(GRID_COLUMN_CLIENT_ID).Width = 0
      .Column(GRID_COLUMN_CLIENT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Build
      .Column(GRID_COLUMN_BUILD_ID).Header(0).Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(230)
      .Column(GRID_COLUMN_BUILD_ID).Header(1).Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(231)
      .Column(GRID_COLUMN_BUILD_ID).Width = 0
      .Column(GRID_COLUMN_BUILD_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Build Text
      .Column(GRID_COLUMN_BUILD_TEXT).Header(0).Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(230)
      .Column(GRID_COLUMN_BUILD_TEXT).Header(1).Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(231)
      .Column(GRID_COLUMN_BUILD_TEXT).Width = 1200
      .Column(GRID_COLUMN_BUILD_TEXT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Target
      .Column(GRID_COLUMN_TARGET).Header(0).Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(235)
      .Column(GRID_COLUMN_TARGET).Header(1).Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(208)
      .Column(GRID_COLUMN_TARGET).Width = 0
      .Column(GRID_COLUMN_TARGET).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Target Text
      .Column(GRID_COLUMN_TARGET_TEXT).Header(0).Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(235)
      .Column(GRID_COLUMN_TARGET_TEXT).Header(1).Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(208)
      .Column(GRID_COLUMN_TARGET_TEXT).Width = 1900
      .Column(GRID_COLUMN_TARGET_TEXT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Package Inserted
      .Column(GRID_COLUMN_INSERTION_DATE).Header(0).Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(235)
      .Column(GRID_COLUMN_INSERTION_DATE).Header(1).Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(234)
      .Column(GRID_COLUMN_INSERTION_DATE).Width = 2100
      .Column(GRID_COLUMN_INSERTION_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE : Set default values to filters
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Sub SetDefaultValues()

    Me.dtp_from.Value = New DateTime(Now.Year, Now.Month, Now.Day, 0, 0, 0)
    Me.dtp_from.Checked = False

    Me.dtp_to.Value = New DateTime(Now.Year, Now.Month, Now.Day, 23, 59, 59)
    Me.dtp_to.Checked = False

    Me.cb_latest.Checked = True
    Me.uc_sw_list.Init()

  End Sub ' SetDefaultValues

  ' PURPOSE : Get the SQL WHERE
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Function GetSqlWhere() As String

    Dim str_where As String
    Dim aux_date As DateTime

    str_where = ""

    ' Insertion Date 
    If Me.dtp_from.Checked = True And Me.cb_latest.Checked = False Then
      str_where = str_where & " AND (TSV_INSERTION_DATE >= " & GUI_FormatDateDB(dtp_from.Value) & ")"
    End If

    If Me.dtp_to.Checked = True And Me.cb_latest.Checked = False Then
      aux_date = dtp_to.Value
      aux_date = aux_date.AddSeconds(1)
      str_where = str_where & " AND (TSV_INSERTION_DATE < " & GUI_FormatDateDB(aux_date) & ")"
    End If

    str_where = str_where & uc_sw_list.GetSwProductsSelected()

    ' Final processing
    If Len(str_where) > 0 And Me.cb_latest.Checked = False Then
      ' Discard the leading ' AND ' and place 'Where' instead
      str_where = Strings.Right(str_where, Len(str_where) - 5)
      str_where = " WHERE " & str_where
    End If

    Return str_where

  End Function ' GetSqlWhere

  ' PURPOSE : Adds a new software version package to the system
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Sub ShowPackageDetails()

    Dim idx_row As Short
    Dim row_selected As Boolean
    Dim frm_edit As Object
    Dim sw_package As New CLASS_SW_PACKAGE

    ' Get the selected row
    row_selected = False
    For idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(idx_row).IsSelected Then
        row_selected = True

        Exit For
      End If
    Next

    If Not row_selected Then
      ' No selected row 
      Call Me.Grid.Focus()

      Exit Sub
    End If

    frm_edit = New frm_sw_package_details

    ' Show the selected Sw Package
    sw_package.ClientId = Me.Grid.Cell(idx_row, GRID_COLUMN_CLIENT_ID).Value
    sw_package.BuildId = Me.Grid.Cell(idx_row, GRID_COLUMN_BUILD_ID).Value
    sw_package.TerminalType = Me.Grid.Cell(idx_row, GRID_COLUMN_TARGET).Value

    Call frm_edit.ShowEditItem(sw_package)

    frm_edit = Nothing

    Call Me.Grid.Focus()

  End Sub ' ShowPackageDetails

  ' PURPOSE : Adds a new software version package to the system
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Sub AddNewPackage()

    Dim frm_edit As Object

    frm_edit = New frm_sw_package_details

    Call frm_edit.ShowNewItem()

    frm_edit = Nothing

    Call Me.Grid.Focus()

  End Sub ' AddNewPackage

#End Region ' Private Functions

#Region "Events"

  Private Sub cb_latest_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cb_latest.CheckedChanged
    If Me.cb_latest.Checked = True Then
      Me.dtp_from.Enabled = False
      Me.dtp_to.Enabled = False
    Else
      Me.dtp_from.Enabled = True
      Me.dtp_to.Enabled = True
    End If
  End Sub ' cb_latest_CheckedChanged

#End Region  ' Events

End Class
