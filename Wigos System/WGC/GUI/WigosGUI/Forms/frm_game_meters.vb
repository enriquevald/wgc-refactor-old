'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_game_meters
' DESCRIPTION:   Show GAME_METERS content
'                   
' AUTHOR:        Miquel Beltran Febrer
' CREATION DATE: 15-MAR-2010
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 15-MAR-2010  MBF    Initial version
' 19-APR-2012  MPO    Defect #274 and #181: Show only SAS_HOST terminals
' 04-JUN-2012  JAB    Eliminate top clause of sql query
' 22-MAR-2013  JAR    Add Provider column to the grid
' 16_SEP-2013  RRR    Show MACHINE_METERS and GAME_METERS content
' 19-NOV-2013  JBC    Fixed Bug WIG-418 . Not show total if grid is empty.
' 19-NOV-2013  JBC    TITO mode with Bills
' 17-DEC-2013  RCI    Fixed Bug WIG-490: Show in "red" when there is no difference between machine and game meters
'                     Use Decimal instead of Double.
' 07-MAY-2014  RRR    Fixed Bug WIG-903: No last report date in last row
' 03-SEP-2014  LEM    Added functionality to show terminal location data.
' 26-MAR-2015  ANM    Calling GetProviderIdListSelected always returns a query
' 15-JUN-2015  ANM    Fixed Bug WIG-2442: Avoid multiselected rows in a specific search
' 08-OCT-2015  FOS    Add total coins column in report
' 07-FEB-2017  RAB    Bug 24283: Reports without properly informing the account denomination
' 09-FEB-2017  RAB    Bug 24283: Reports without properly informing the account denomination
' 19-JUL-2017  EOR    Bug 28866: WIGOS-2802 Contadores Actuales GUI - Difiere resultado en excel
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports WSI.Common
Imports System.Text

Public Class frm_game_meters
  Inherits frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents lb_session_id As System.Windows.Forms.Label
  Friend WithEvents gb_last_update As System.Windows.Forms.GroupBox
  Friend WithEvents chk_old As System.Windows.Forms.CheckBox
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents gb_counter_type_sel As System.Windows.Forms.GroupBox
  Public WithEvents opt_only_machines As System.Windows.Forms.RadioButton
  Public WithEvents opt_machines_and_games As System.Windows.Forms.RadioButton
  Public WithEvents opt_only_games As System.Windows.Forms.RadioButton
  Friend WithEvents chk_only_actives As System.Windows.Forms.CheckBox
  Friend WithEvents chk_show_game_detail As System.Windows.Forms.CheckBox
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox
  Friend WithEvents chk_reciente As System.Windows.Forms.CheckBox
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.lb_session_id = New System.Windows.Forms.Label
    Me.gb_last_update = New System.Windows.Forms.GroupBox
    Me.chk_old = New System.Windows.Forms.CheckBox
    Me.chk_reciente = New System.Windows.Forms.CheckBox
    Me.uc_pr_list = New GUI_Controls.uc_provider
    Me.gb_counter_type_sel = New System.Windows.Forms.GroupBox
    Me.chk_show_game_detail = New System.Windows.Forms.CheckBox
    Me.opt_only_machines = New System.Windows.Forms.RadioButton
    Me.opt_machines_and_games = New System.Windows.Forms.RadioButton
    Me.opt_only_games = New System.Windows.Forms.RadioButton
    Me.chk_only_actives = New System.Windows.Forms.CheckBox
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_last_update.SuspendLayout()
    Me.gb_counter_type_sel.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.chk_terminal_location)
    Me.panel_filter.Controls.Add(Me.chk_only_actives)
    Me.panel_filter.Controls.Add(Me.gb_counter_type_sel)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Controls.Add(Me.lb_session_id)
    Me.panel_filter.Controls.Add(Me.gb_last_update)
    Me.panel_filter.Size = New System.Drawing.Size(1226, 200)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_last_update, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lb_session_id, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_counter_type_sel, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_only_actives, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_terminal_location, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 204)
    Me.panel_data.Size = New System.Drawing.Size(1226, 504)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1220, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1220, 4)
    '
    'lb_session_id
    '
    Me.lb_session_id.AutoSize = True
    Me.lb_session_id.Location = New System.Drawing.Point(34, 93)
    Me.lb_session_id.Name = "lb_session_id"
    Me.lb_session_id.Size = New System.Drawing.Size(0, 13)
    Me.lb_session_id.TabIndex = 1
    '
    'gb_last_update
    '
    Me.gb_last_update.Controls.Add(Me.chk_old)
    Me.gb_last_update.Controls.Add(Me.chk_reciente)
    Me.gb_last_update.Location = New System.Drawing.Point(621, 9)
    Me.gb_last_update.Name = "gb_last_update"
    Me.gb_last_update.Size = New System.Drawing.Size(273, 51)
    Me.gb_last_update.TabIndex = 5
    Me.gb_last_update.TabStop = False
    Me.gb_last_update.Text = "xActualizacion"
    Me.gb_last_update.Visible = False
    '
    'chk_old
    '
    Me.chk_old.AutoSize = True
    Me.chk_old.Location = New System.Drawing.Point(136, 21)
    Me.chk_old.Name = "chk_old"
    Me.chk_old.Size = New System.Drawing.Size(76, 17)
    Me.chk_old.TabIndex = 1
    Me.chk_old.Text = "xAntigua"
    Me.chk_old.UseVisualStyleBackColor = True
    '
    'chk_reciente
    '
    Me.chk_reciente.AutoSize = True
    Me.chk_reciente.Location = New System.Drawing.Point(6, 21)
    Me.chk_reciente.Name = "chk_reciente"
    Me.chk_reciente.Size = New System.Drawing.Size(82, 17)
    Me.chk_reciente.TabIndex = 0
    Me.chk_reciente.Text = "xReciente"
    Me.chk_reciente.UseVisualStyleBackColor = True
    '
    'uc_pr_list
    '
    Me.uc_pr_list.Location = New System.Drawing.Point(284, 6)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(394, 197)
    Me.uc_pr_list.TabIndex = 4
    '
    'gb_counter_type_sel
    '
    Me.gb_counter_type_sel.Controls.Add(Me.chk_show_game_detail)
    Me.gb_counter_type_sel.Controls.Add(Me.opt_only_machines)
    Me.gb_counter_type_sel.Controls.Add(Me.opt_machines_and_games)
    Me.gb_counter_type_sel.Controls.Add(Me.opt_only_games)
    Me.gb_counter_type_sel.Location = New System.Drawing.Point(6, 9)
    Me.gb_counter_type_sel.Name = "gb_counter_type_sel"
    Me.gb_counter_type_sel.Size = New System.Drawing.Size(272, 125)
    Me.gb_counter_type_sel.TabIndex = 0
    Me.gb_counter_type_sel.TabStop = False
    Me.gb_counter_type_sel.Text = "xContadores"
    '
    'chk_show_game_detail
    '
    Me.chk_show_game_detail.AutoSize = True
    Me.chk_show_game_detail.Location = New System.Drawing.Point(24, 97)
    Me.chk_show_game_detail.Name = "chk_show_game_detail"
    Me.chk_show_game_detail.Size = New System.Drawing.Size(137, 17)
    Me.chk_show_game_detail.TabIndex = 3
    Me.chk_show_game_detail.Text = "xShowGameDetails"
    Me.chk_show_game_detail.UseVisualStyleBackColor = True
    '
    'opt_only_machines
    '
    Me.opt_only_machines.Location = New System.Drawing.Point(6, 18)
    Me.opt_only_machines.Name = "opt_only_machines"
    Me.opt_only_machines.Size = New System.Drawing.Size(150, 24)
    Me.opt_only_machines.TabIndex = 0
    Me.opt_only_machines.Text = "xOnlyTerminals"
    '
    'opt_machines_and_games
    '
    Me.opt_machines_and_games.Location = New System.Drawing.Point(6, 70)
    Me.opt_machines_and_games.Name = "opt_machines_and_games"
    Me.opt_machines_and_games.Size = New System.Drawing.Size(150, 24)
    Me.opt_machines_and_games.TabIndex = 2
    Me.opt_machines_and_games.Text = "xTerminalAndGames"
    '
    'opt_only_games
    '
    Me.opt_only_games.Location = New System.Drawing.Point(6, 44)
    Me.opt_only_games.Name = "opt_only_games"
    Me.opt_only_games.Size = New System.Drawing.Size(150, 24)
    Me.opt_only_games.TabIndex = 1
    Me.opt_only_games.Text = "xOnlyGames"
    '
    'chk_only_actives
    '
    Me.chk_only_actives.AutoSize = True
    Me.chk_only_actives.Location = New System.Drawing.Point(9, 143)
    Me.chk_only_actives.Name = "chk_only_actives"
    Me.chk_only_actives.Size = New System.Drawing.Size(150, 17)
    Me.chk_only_actives.TabIndex = 2
    Me.chk_only_actives.Text = "xOnlyActiveTerminals"
    Me.chk_only_actives.UseVisualStyleBackColor = True
    '
    'chk_terminal_location
    '
    Me.chk_terminal_location.Location = New System.Drawing.Point(9, 170)
    Me.chk_terminal_location.Name = "chk_terminal_location"
    Me.chk_terminal_location.Size = New System.Drawing.Size(269, 16)
    Me.chk_terminal_location.TabIndex = 3
    Me.chk_terminal_location.Text = "xShow terminals location"
    '
    'frm_game_meters
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(1234, 712)
    Me.Name = "frm_game_meters"
    Me.Text = "frm_game_meters"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_last_update.ResumeLayout(False)
    Me.gb_last_update.PerformLayout()
    Me.gb_counter_type_sel.ResumeLayout(False)
    Me.gb_counter_type_sel.PerformLayout()
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Members "

  Private m_refreshing_grid As Boolean

  ' For report filters 
  Private m_terminals As String
  Private m_update_filter_recent As String
  Private m_update_filter_old As String

  Private m_bl_where As Boolean = True

  ' For grid formatting
  Private m_current_terminal_id As Int32

  ' For amount grid
  Private m_last_terminal_data As List(Of Object)

  Private m_machine_played_amount As Decimal
  Private m_machine_won_amount As Decimal
  Private m_machine_jackpot_amount As Decimal
  Private m_total_game_played_amount As Decimal
  Private m_total_game_won_amount As Decimal
  Private m_total_game_jackpot_amount As Decimal
  Private m_last_report_date As String

  ' For tito mode
  Private m_tito_mode As Boolean
  Private m_grid_bills_column_ini As Integer
  Private m_sql_bills_column_ini As Integer
  Private m_code_list() As Integer = Nothing
  Private m_last_row As CLASS_DB_ROW
  Private m_there_are_rows As Boolean

  Private m_terminal_report_type As ReportType = ReportType.Provider

#End Region ' Members

#Region " Constants "

  Private TERMINAL_DATA_COLUMNS As Int32

  Private Const SQL_COLUMN_DATETIME As Integer = 0
  Private Const SQL_COLUMN_PROVIDER As Integer = 1
  Private Const SQL_COLUMN_TERMINAL As Integer = 2
  Private Const SQL_COLUMN_TERMINAL_ID As Integer = 3
  Private Const SQL_COLUMN_PLAYED_AMOUNT As Integer = 4
  Private Const SQL_COLUMN_WON_AMOUNT As Integer = 5
  Private Const SQL_COLUMN_JACKPOT As Integer = 6
  Private Const SQL_COLUMN_GAME_NAME As Integer = 7
  Private Const SQL_COLUMN_GAME_PLAYED_AMOUNT As Integer = 8
  Private Const SQL_COLUMN_GAME_WON_AMOUNT As Integer = 9
  Private Const SQL_COLUMN_GAME_JACKPOT As Integer = 10
  Private Const SQL_COLUMN_GAME_DATETIME As Integer = 11

  Private GRID_COLUMN_INDEX As Integer
  Private GRID_INIT_TERMINAL_DATA As Integer
  Private GRID_COLUMN_PLAYED_AMOUNT As Integer
  Private GRID_COLUMN_WON_AMOUNT As Integer
  Private GRID_COLUMN_JACKPOT As Integer
  Private GRID_COLUMN_DATETIME As Integer
  Private GRID_COLUMN_GAME_NAME As Integer
  Private GRID_COLUMN_GAME_PLAYED_AMOUNT As Integer
  Private GRID_COLUMN_GAME_WON_AMOUNT As Integer
  Private GRID_COLUMN_GAME_JACKPOT As Integer
  Private GRID_COLUMN_GAME_DATETIME As Integer

  Private GRID_COLUMNS As Integer
  Private Const GRID_HEADER_ROWS As Integer = 2

  'Width
  Private Const GRID_WIDTH_DATE As Integer = 2190
  Private Const GRID_WIDTH_AMOUNT As Integer = 1475
  Private Const GRID_WIDTH_COUNT As Integer = 1475
  Private Const GRID_WIDTH_PROVIDER As Integer = 2500
  Private Const GRID_WIDTH_TERMINAL As Integer = 2500
  Private Const GRID_WIDTH_GAME As Integer = 3000

#End Region ' Constants

#Region " OVERRIDES "

  Protected Overrides Function GUI_MaxRows() As Integer
    Return 10000
  End Function ' GUI_MaxRows

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_GAME_METERS

    ' TJG 01-FEB-2005
    ' Set the form icon from the embedded resource (ICO file must be built in the project as "Embedded Resource")
    ' Call could be made as GetManifestResourceStream("GUI_Auditor.GUI_Auditor.ico"))
    'Me.Icon = New Icon(System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream(Me.GetType(), "WigosGUI.ico"))

    '------------------------------------------------
    'XVV 13/04/2007
    'Call Base Form proc
    Call MyBase.GUI_SetFormId()
    '------------------------------------------------

  End Sub ' GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_STATISTICS.GetString(395)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    ' Me.gb_sdfsdf
    Me.gb_last_update.Text = GLB_NLS_GUI_STATISTICS.GetString(416)
    Me.chk_reciente.Text = GLB_NLS_GUI_STATISTICS.GetString(417)
    Me.chk_old.Text = GLB_NLS_GUI_STATISTICS.GetString(418)

    ' Me.gb_counter_type_sel
    Me.gb_counter_type_sel.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2642)
    Me.opt_only_machines.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2643)
    Me.opt_only_games.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2644)
    Me.opt_machines_and_games.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2645)
    Me.chk_show_game_detail.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2938)

    ' Me.chk_only_actives
    Me.chk_only_actives.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2646)

    ' Providers - Terminals
    Dim _terminal_types() As Integer = Nothing
    ReDim Preserve _terminal_types(0 To 1)
    _terminal_types(0) = TerminalTypes.SAS_HOST
    _terminal_types(1) = TerminalTypes.OFFLINE

    Call Me.uc_pr_list.Init(_terminal_types)

    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)

    Call GenerateListofBills() ' GENERA LA LISTA

    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

    m_tito_mode = TITO.Utils.IsTitoMode

    IgnoreRowHeightFilterInExcelReport = True

  End Sub ' GUI_InitControls

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  Protected Overrides Sub GUI_BeforeFirstRow()

    Call GUI_StyleSheet()

    If m_tito_mode AndAlso (opt_only_machines.Checked Or opt_machines_and_games.Checked) Then
      m_grid_bills_column_ini = m_code_list.Length + 1
      m_sql_bills_column_ini = m_code_list.Length
    Else
      m_grid_bills_column_ini = 0
      m_sql_bills_column_ini = 0
    End If

    Call SetColumnView()

    m_last_row = Nothing

    m_there_are_rows = False

    m_machine_played_amount = -1
    m_machine_won_amount = -1
    m_machine_jackpot_amount = -1

    m_current_terminal_id = 0

  End Sub ' GUI_BeforeFirsRow

  Protected Overrides Sub GUI_AfterLastRow()

    Dim _idx_row As Integer

    If opt_machines_and_games.Checked And m_there_are_rows Then

      Me.Grid.AddRow()
      _idx_row = Me.Grid.NumRows - 1

      ' Terminal Report
      For _idx As Int32 = 0 To m_last_terminal_data.Count - 1
        If Not m_last_terminal_data(_idx) Is Nothing AndAlso Not m_last_terminal_data(_idx) Is DBNull.Value Then
          Me.Grid.Cell(_idx_row, GRID_INIT_TERMINAL_DATA + _idx).Value = m_last_terminal_data(_idx)
        End If
      Next

      ' PlayedAmount
      If m_machine_played_amount < 0 Then
        Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_AMOUNT).Value = String.Empty
      Else
        Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_AMOUNT).Value = GUI_FormatCurrency(m_machine_played_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End If

      ' WonAmount
      If m_machine_won_amount < 0 Then
        Me.Grid.Cell(_idx_row, GRID_COLUMN_WON_AMOUNT).Value = String.Empty
      Else
        Me.Grid.Cell(_idx_row, GRID_COLUMN_WON_AMOUNT).Value = GUI_FormatCurrency(m_machine_won_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End If

      ' Jackpot
      If m_machine_jackpot_amount < 0 Then
        Me.Grid.Cell(_idx_row, GRID_COLUMN_JACKPOT).Value = String.Empty
      Else
        Me.Grid.Cell(_idx_row, GRID_COLUMN_JACKPOT).Value = GUI_FormatCurrency(m_machine_jackpot_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End If

      If m_grid_bills_column_ini > 0 Then
        Call PrintGridBills(_idx_row, GRID_COLUMN_DATETIME, m_last_row)
      End If

      ' Game PlayedAmount
      If m_total_game_played_amount > -1 Then
        Me.Grid.Cell(_idx_row, GRID_COLUMN_GAME_PLAYED_AMOUNT + m_grid_bills_column_ini).Value = GUI_FormatCurrency(m_total_game_played_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Else
        Me.Grid.Cell(_idx_row, GRID_COLUMN_GAME_PLAYED_AMOUNT + m_grid_bills_column_ini).Value = String.Empty
      End If

      ' Game WonAmount
      If m_total_game_won_amount > -1 Then
        Me.Grid.Cell(_idx_row, GRID_COLUMN_GAME_WON_AMOUNT + m_grid_bills_column_ini).Value = GUI_FormatCurrency(m_total_game_won_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Else
        Me.Grid.Cell(_idx_row, GRID_COLUMN_GAME_WON_AMOUNT + m_grid_bills_column_ini).Value = String.Empty
      End If

      ' Game Jackpot
      If m_total_game_won_amount > -1 Then
        Me.Grid.Cell(_idx_row, GRID_COLUMN_GAME_JACKPOT + m_grid_bills_column_ini).Value = GUI_FormatCurrency(m_total_game_jackpot_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Else
        Me.Grid.Cell(_idx_row, GRID_COLUMN_GAME_JACKPOT + m_grid_bills_column_ini).Value = String.Empty
      End If

      ' 07-MAY-2014 RRR Fixed Bug WIG-903
      ' DateTime
      Me.Grid.Cell(_idx_row, GRID_COLUMN_DATETIME).Value = m_last_report_date

      ' Set Colors
      For _index As Integer = 0 To Me.Grid.NumColumns - 1
        Me.Grid.Cell(_idx_row, _index).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
      Next

      If m_machine_played_amount <> m_total_game_played_amount Then
        Call Me.SetRedCells(Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_AMOUNT), _
                            Me.Grid.Cell(_idx_row, GRID_COLUMN_GAME_PLAYED_AMOUNT + m_grid_bills_column_ini))
      End If

      If m_machine_won_amount <> m_total_game_won_amount Then
        Call Me.SetRedCells(Me.Grid.Cell(_idx_row, GRID_COLUMN_WON_AMOUNT), _
                            Me.Grid.Cell(_idx_row, GRID_COLUMN_GAME_WON_AMOUNT + m_grid_bills_column_ini))
      End If

      If m_machine_jackpot_amount <> m_total_game_jackpot_amount Then
        Call Me.SetRedCells(Me.Grid.Cell(_idx_row, GRID_COLUMN_JACKPOT), _
                            Me.Grid.Cell(_idx_row, GRID_COLUMN_GAME_JACKPOT + m_grid_bills_column_ini))
      End If

    End If


    Me.Grid.SelectFirstRow(True)

  End Sub ' GUI_AfterLastRow

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case ENUM_BUTTON.BUTTON_FILTER_APPLY
        Call SetColumnView()

    End Select

    MyBase.GUI_ButtonClick(ButtonId)

  End Sub

  Protected Overrides Function GUI_FilterCheck() As Boolean
    Return True
  End Function ' GUI_FilterCheck

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _str_sql As StringBuilder
    Dim _list_bills As String
    Dim _list_bills_dots As String

    _str_sql = New StringBuilder()
    _list_bills = String.Empty
    _list_bills_dots = String.Empty

    _str_sql.AppendLine("SELECT   MM_LAST_REPORTED")
    _str_sql.AppendLine("       , TE_PROVIDER_ID")
    _str_sql.AppendLine("       , TE_NAME")
    _str_sql.AppendLine("       , TE_TERMINAL_ID")
    _str_sql.AppendLine("       , MM_PLAYED_AMOUNT")
    _str_sql.AppendLine("       , MM_WON_AMOUNT")
    _str_sql.AppendLine("       , MM_JACKPOT_AMOUNT")

    If Not m_tito_mode Or opt_only_games.Checked Then

      _str_sql.AppendLine("          , ISNULL(GM_GAME_BASE_NAME, '')")
      _str_sql.AppendLine("          , GM_PLAYED_AMOUNT")
      _str_sql.AppendLine("          , GM_WON_AMOUNT")
      _str_sql.AppendLine("          , GM_JACKPOT_AMOUNT")
      _str_sql.AppendLine("          , GM_LAST_REPORTED")
      _str_sql.AppendLine("     FROM   TERMINALS AS TE")
      _str_sql.AppendLine("LEFT JOIN   MACHINE_METERS AS MM ON MM.MM_TERMINAL_ID = TE.TE_TERMINAL_ID")
      _str_sql.AppendLine("LEFT JOIN   GAME_METERS AS GM ON GM.GM_TERMINAL_ID = TE.TE_TERMINAL_ID")
      _str_sql.AppendLine("    WHERE   TE.TE_TERMINAL_TYPE = 5") ' SAS_HOST
      _str_sql.AppendLine(GetSqlWhere())
      _str_sql.AppendLine(" ORDER BY   TE.TE_PROVIDER_ID, TE.TE_NAME, GM.GM_GAME_BASE_NAME DESC ")

    Else
      _list_bills = GetBillsStringList(False)
      _list_bills_dots = _list_bills.Replace("[", "").Replace("]", "")

      _str_sql.AppendLine("," & _list_bills)

      If opt_machines_and_games.Checked Then
        _str_sql.AppendLine("       , ISNULL(GM_GAME_BASE_NAME, '') AS GM_NAME")
        _str_sql.AppendLine("       , GM_PLAYED_AMOUNT")
        _str_sql.AppendLine("       , GM_WON_AMOUNT")
        _str_sql.AppendLine("       , GM_JACKPOT_AMOUNT")
        _str_sql.AppendLine("       , GM_LAST_REPORTED")
      End If

      _str_sql.AppendLine("     FROM  TERMINALS AS TE ")
      _str_sql.AppendLine("LEFT JOIN  (")
      _str_sql.AppendLine("   SELECT  TSM_TERMINAL_ID")
      _str_sql.AppendLine("           , TSM_METER_CODE, TSM_METER_VALUE ")
      _str_sql.AppendLine("     FROM  TERMINAL_SAS_METERS ")
      _str_sql.AppendLine("     WHERE TSM_METER_CODE IN (" & _list_bills_dots & ") )	AS TABLE1 ")
      _str_sql.AppendLine("PIVOT ( ")
      _str_sql.AppendLine("   SUM(TSM_METER_VALUE) FOR TSM_METER_CODE IN ( " & _list_bills & "))	AS PVT 	")
      _str_sql.AppendLine("ON PVT.TSM_TERMINAL_ID =TE.TE_TERMINAL_ID ")
      _str_sql.AppendLine("LEFT JOIN MACHINE_METERS AS MM ON MM.MM_TERMINAL_ID = TE.TE_TERMINAL_ID ")

      If opt_machines_and_games.Checked Then
        _str_sql.AppendLine("LEFT JOIN GAME_METERS AS GM ON GM.GM_TERMINAL_ID = TE.TE_TERMINAL_ID ")
      End If

      _str_sql.AppendLine("WHERE TE.TE_TERMINAL_TYPE = 5 ")
      _str_sql.AppendLine(GetSqlWhere())
      _str_sql.AppendLine("ORDER BY TE.TE_PROVIDER_ID, TE.TE_NAME ")

      If opt_machines_and_games.Checked Then
        _str_sql.AppendLine(", GM_GAME_BASE_NAME ")
      End If

      _str_sql.AppendLine("DESC")

    End If

    Return _str_sql.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _totals_row_inserted As Boolean
    Dim _terminal_data As List(Of Object)

    _totals_row_inserted = False
    _terminal_data = TerminalReport.GetReportDataList(DbRow.Value(SQL_COLUMN_TERMINAL_ID), m_terminal_report_type)

    ' Machines and Totals Column Values
    If m_current_terminal_id <> DbRow.Value(SQL_COLUMN_TERMINAL_ID) And opt_machines_and_games.Checked Then

      If m_current_terminal_id <> 0 Then

        ' Insert Totals row
        Me.Grid.InsertRow(RowIndex)

        ' Terminal Report
        For _idx As Int32 = 0 To m_last_terminal_data.Count - 1
          If Not m_last_terminal_data(_idx) Is Nothing AndAlso Not m_last_terminal_data(_idx) Is DBNull.Value Then
            Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = m_last_terminal_data(_idx)
          End If
        Next

        ' PlayedAmount
        If m_machine_played_amount < 0 Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED_AMOUNT).Value = String.Empty
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED_AMOUNT).Value = GUI_FormatCurrency(m_machine_played_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If

        ' WonAmount
        If m_machine_won_amount < 0 Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_WON_AMOUNT).Value = String.Empty
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_WON_AMOUNT).Value = GUI_FormatCurrency(m_machine_won_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If

        ' Jackpot
        If m_machine_jackpot_amount < 0 Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_JACKPOT).Value = String.Empty
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_JACKPOT).Value = GUI_FormatCurrency(m_machine_jackpot_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If

        If m_grid_bills_column_ini > 0 Then
          Call PrintGridBills(RowIndex, GRID_COLUMN_DATETIME, m_last_row)
        End If

        ' Game PlayedAmount
        If m_total_game_played_amount > -1 Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_PLAYED_AMOUNT + m_grid_bills_column_ini).Value = GUI_FormatCurrency(m_total_game_played_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_PLAYED_AMOUNT + m_grid_bills_column_ini).Value = String.Empty
        End If

        ' Game WonAmount
        If m_total_game_won_amount > -1 Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_WON_AMOUNT + m_grid_bills_column_ini).Value = GUI_FormatCurrency(m_total_game_won_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_WON_AMOUNT + m_grid_bills_column_ini).Value = String.Empty
        End If

        ' Game Jackpot
        If m_total_game_won_amount > -1 Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_JACKPOT + m_grid_bills_column_ini).Value = GUI_FormatCurrency(m_total_game_jackpot_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_JACKPOT + m_grid_bills_column_ini).Value = String.Empty
        End If

        ' Datetime
        If IsDBNull(DbRow.Value(SQL_COLUMN_DATETIME)) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_DATETIME).Value = String.Empty
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATETIME), _
                                                                              ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                              ENUM_FORMAT_TIME.FORMAT_HHMMSS)
        End If

        ' Set Colors
        For _index As Integer = 0 To Me.Grid.NumColumns - 1
          Me.Grid.Cell(RowIndex, _index).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
        Next

        If m_machine_played_amount <> m_total_game_played_amount Then
          Call Me.SetRedCells(Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED_AMOUNT), _
                              Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_PLAYED_AMOUNT + m_grid_bills_column_ini))
        End If

        If m_machine_won_amount <> m_total_game_won_amount Then
          Call Me.SetRedCells(Me.Grid.Cell(RowIndex, GRID_COLUMN_WON_AMOUNT), _
                              Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_WON_AMOUNT + m_grid_bills_column_ini))
        End If

        If m_machine_jackpot_amount <> m_total_game_jackpot_amount Then
          Call Me.SetRedCells(Me.Grid.Cell(RowIndex, GRID_COLUMN_JACKPOT), _
                                Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_JACKPOT + m_grid_bills_column_ini))
        End If

        _totals_row_inserted = True

      End If

      m_last_terminal_data = _terminal_data

      If IsDBNull(DbRow.Value(SQL_COLUMN_DATETIME)) Then
        m_last_report_date = String.Empty
      Else
        m_last_report_date = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATETIME), _
                                                        ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                        ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      End If


      If IsDBNull(DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT)) Then
        m_machine_played_amount = -1
      Else
        m_machine_played_amount = DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT)
      End If

      If IsDBNull(DbRow.Value(SQL_COLUMN_WON_AMOUNT)) Then
        m_machine_won_amount = -1
      Else
        m_machine_won_amount = DbRow.Value(SQL_COLUMN_WON_AMOUNT)
      End If

      If IsDBNull(DbRow.Value(SQL_COLUMN_JACKPOT)) Then
        m_machine_jackpot_amount = -1
      Else
        m_machine_jackpot_amount = DbRow.Value(SQL_COLUMN_JACKPOT)
      End If

      m_total_game_played_amount = -1
      m_total_game_won_amount = -1
      m_total_game_jackpot_amount = -1

      m_current_terminal_id = DbRow.Value(SQL_COLUMN_TERMINAL_ID)

    End If

    If _totals_row_inserted Then
      RowIndex = RowIndex + 1
    End If

    If Not opt_only_machines.Checked Then

      ' Game Datetime
      If IsDBNull(DbRow.Value(SQL_COLUMN_GAME_DATETIME + m_sql_bills_column_ini)) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_DATETIME + m_grid_bills_column_ini).Value = String.Empty
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_DATETIME + m_grid_bills_column_ini).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_GAME_DATETIME + m_sql_bills_column_ini), _
                                                                            ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                            ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      End If

      ' Terminal Report
      For _idx As Int32 = 0 To _terminal_data.Count - 1
        If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value Then
          Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
        End If
      Next

      ' Game Name 
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_NAME + m_grid_bills_column_ini).Value = DbRow.Value(SQL_COLUMN_GAME_NAME + m_sql_bills_column_ini)

      ' Game PlayedAmount
      If IsDBNull(DbRow.Value(SQL_COLUMN_GAME_PLAYED_AMOUNT + m_sql_bills_column_ini)) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_PLAYED_AMOUNT + m_grid_bills_column_ini).Value = ""
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_PLAYED_AMOUNT + m_grid_bills_column_ini).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_GAME_PLAYED_AMOUNT + m_sql_bills_column_ini), _
                                                                                          ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        If m_total_game_played_amount > -1 Then
          m_total_game_played_amount += DbRow.Value(SQL_COLUMN_GAME_PLAYED_AMOUNT + m_sql_bills_column_ini)
        Else
          m_total_game_played_amount = DbRow.Value(SQL_COLUMN_GAME_PLAYED_AMOUNT + m_sql_bills_column_ini)
        End If
      End If

      ' Game WonAmount
      If IsDBNull(DbRow.Value(SQL_COLUMN_GAME_WON_AMOUNT + m_sql_bills_column_ini)) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_WON_AMOUNT + m_grid_bills_column_ini).Value = ""
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_WON_AMOUNT + m_grid_bills_column_ini).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_GAME_WON_AMOUNT + m_sql_bills_column_ini), _
                                                                                       ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        If m_total_game_won_amount > -1 Then
          m_total_game_won_amount += DbRow.Value(SQL_COLUMN_GAME_WON_AMOUNT + m_sql_bills_column_ini)
        Else
          m_total_game_won_amount = DbRow.Value(SQL_COLUMN_GAME_WON_AMOUNT + m_sql_bills_column_ini)
        End If
      End If

      ' Game Jackpot
      If IsDBNull(DbRow.Value(SQL_COLUMN_GAME_JACKPOT + m_sql_bills_column_ini)) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_JACKPOT + m_grid_bills_column_ini).Value = ""
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_JACKPOT + m_grid_bills_column_ini).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_GAME_JACKPOT + m_sql_bills_column_ini), _
                                                                                    ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        If m_total_game_jackpot_amount > -1 Then
          m_total_game_jackpot_amount += DbRow.Value(SQL_COLUMN_GAME_JACKPOT + m_sql_bills_column_ini)
        Else
          m_total_game_jackpot_amount = DbRow.Value(SQL_COLUMN_GAME_JACKPOT + m_sql_bills_column_ini)
        End If
      End If

      If Me.opt_machines_and_games.Checked And Not Me.chk_show_game_detail.Checked Then
        Me.Grid.DeleteRow(RowIndex)
        'Avoid multiselected rows in a specific search (DeleteRow highlights rows)
        If RowIndex > 0 Then
          Me.Grid.IsSelected(RowIndex - 1) = False
        End If
      End If

    ElseIf m_current_terminal_id <> DbRow.Value(SQL_COLUMN_TERMINAL_ID) Then

      ' Datetime
      If IsDBNull(DbRow.Value(SQL_COLUMN_DATETIME)) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DATETIME).Value = String.Empty
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATETIME), _
                                                                            ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                            ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      End If

      ' Terminal Report
      For _idx As Int32 = 0 To _terminal_data.Count - 1
        If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value Then
          Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
        End If
      Next

      ' PlayedAmount
      If IsDBNull(DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT)) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED_AMOUNT).Value = ""
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT), _
                                                                                          ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End If

      ' WonAmount
      If IsDBNull(DbRow.Value(SQL_COLUMN_WON_AMOUNT)) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_WON_AMOUNT).Value = ""
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_WON_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_WON_AMOUNT), _
                                                                                       ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End If

      ' Jackpot
      If IsDBNull(DbRow.Value(SQL_COLUMN_JACKPOT)) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_JACKPOT).Value = ""
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_JACKPOT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_JACKPOT), _
                                                                                    ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End If

      m_current_terminal_id = DbRow.Value(SQL_COLUMN_TERMINAL_ID)

      If m_grid_bills_column_ini > 0 Then
        Call PrintGridBills(RowIndex, GRID_COLUMN_DATETIME, DbRow)
      End If

    Else

      Me.Grid.DeleteRow(RowIndex)

    End If

    m_there_are_rows = True
    m_last_row = DbRow

    Return True

  End Function ' GUI_SetupRow

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.chk_reciente
  End Sub 'GUI_SetInitialFocus


#Region " GUI Reports "

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    Dim _filter As String
    Dim _meters As String

    _filter = ""

    If m_update_filter_recent <> "" And m_update_filter_old <> "" Then
      _filter = m_update_filter_recent & "; " & m_update_filter_old
    ElseIf m_update_filter_recent <> "" Then
      _filter = m_update_filter_recent
    ElseIf m_update_filter_old <> "" Then
      _filter = m_update_filter_old
    End If

    If opt_only_machines.Checked Then
      _meters = opt_only_machines.Text
    ElseIf opt_only_games.Checked Then
      _meters = opt_only_games.Text
    Else
      _meters = opt_machines_and_games.Text
    End If

    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(416), _filter)

    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_terminals)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2642), _meters)

    If chk_only_actives.Checked Then
      PrintData.SetFilter("", GLB_NLS_GUI_PLAYER_TRACKING.GetString(2646))
    Else
      PrintData.SetFilter("", "", True)
    End If

    PrintData.FilterValueWidth(1) = 3000
    PrintData.FilterValueWidth(2) = 3000

  End Sub ' GUI_ReportFilter

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)

    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_terminals = ""
    m_update_filter_recent = ""
    m_update_filter_old = ""

    ' Update
    If Me.chk_reciente.Checked Then
      m_update_filter_recent = GLB_NLS_GUI_STATISTICS.GetString(417)
    End If

    If Me.chk_old.Checked Then
      m_update_filter_old = GLB_NLS_GUI_STATISTICS.GetString(418)
    End If

    ' Providers - Terminals
    m_terminals = Me.uc_pr_list.GetTerminalReportText()

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region  ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    Dim _grid_columns As Integer
    Dim _init_bills As Integer
    Dim _terminal_columns As List(Of ColumnSettings)

    Call GridColumnReIndex()

    With Me.Grid
      If m_tito_mode AndAlso (opt_only_machines.Checked Or opt_machines_and_games.Checked) Then
        _init_bills = m_code_list.Length + 1 ' Bills + Total Column
        _grid_columns = GRID_COLUMNS + _init_bills
      Else
        _init_bills = 0
        m_grid_bills_column_ini = 0
        _grid_columns = GRID_COLUMNS
      End If

      Call .Init(_grid_columns, GRID_HEADER_ROWS)
      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      '  Terminal Report
      _terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)
      For _idx As Int32 = 0 To _terminal_columns.Count - 1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2640)
        If _terminal_columns(_idx).Column = ReportColumn.Terminal Then
          .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = _terminal_columns(_idx).Header0
        Else
          .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = _terminal_columns(_idx).Header1
        End If
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = _terminal_columns(_idx).Width
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = _terminal_columns(_idx).Alignment
      Next

      '  Played Amount
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2640)
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(385)
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Won Amount
      .Column(GRID_COLUMN_WON_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2640)
      .Column(GRID_COLUMN_WON_AMOUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(386)
      .Column(GRID_COLUMN_WON_AMOUNT).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_WON_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Jackpot
      .Column(GRID_COLUMN_JACKPOT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2640)
      .Column(GRID_COLUMN_JACKPOT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(396)
      .Column(GRID_COLUMN_JACKPOT).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_JACKPOT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Datetime
      .Column(GRID_COLUMN_DATETIME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2640)
      .Column(GRID_COLUMN_DATETIME).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(416)
      .Column(GRID_COLUMN_DATETIME).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      If m_tito_mode AndAlso Not opt_only_games.Checked Then


        For _idx_bills_column As Integer = 1 To m_code_list.Length
          If _idx_bills_column = 1 Then
            ' Total
            .Column(GRID_COLUMN_DATETIME + 1).Header(0).Text = String.Empty
            .Column(GRID_COLUMN_DATETIME + 1).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5616)
            .Column(GRID_COLUMN_DATETIME + 1).Width = GRID_WIDTH_AMOUNT + 200
            .Column(GRID_COLUMN_DATETIME + 1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
          Else
            ' Total
            .Column(GRID_COLUMN_DATETIME + 2).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2266)
            .Column(GRID_COLUMN_DATETIME + 2).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2271)
            .Column(GRID_COLUMN_DATETIME + 2).Width = GRID_WIDTH_AMOUNT + 200
            .Column(GRID_COLUMN_DATETIME + 2).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

            ' ticket1
            .Column(GRID_COLUMN_DATETIME + _idx_bills_column + 1).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2266)
            .Column(GRID_COLUMN_DATETIME + _idx_bills_column + 1).Header(1).Text = GUI_FormatCurrency(SAS_Meter.GetBillDenomination(m_code_list(_idx_bills_column - 1)))
            .Column(GRID_COLUMN_DATETIME + _idx_bills_column + 1).Width = .Column(GRID_COLUMN_DATETIME + _idx_bills_column + 1).Header(1).Text.Length * 120 'grow depending lenght of text.
            .Column(GRID_COLUMN_DATETIME + _idx_bills_column + 1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
          End If
        Next
      End If

      ' Game Name
      .Column(GRID_COLUMN_GAME_NAME + _init_bills).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2641)
      .Column(GRID_COLUMN_GAME_NAME + _init_bills).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(411)
      .Column(GRID_COLUMN_GAME_NAME + _init_bills).Width = GRID_WIDTH_GAME
      .Column(GRID_COLUMN_GAME_NAME + _init_bills).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Game Played Amount
      .Column(GRID_COLUMN_GAME_PLAYED_AMOUNT + _init_bills).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2641)
      .Column(GRID_COLUMN_GAME_PLAYED_AMOUNT + _init_bills).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(385)
      .Column(GRID_COLUMN_GAME_PLAYED_AMOUNT + _init_bills).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_GAME_PLAYED_AMOUNT + _init_bills).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Game Won Amount
      .Column(GRID_COLUMN_GAME_WON_AMOUNT + _init_bills).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2641)
      .Column(GRID_COLUMN_GAME_WON_AMOUNT + _init_bills).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(386)
      .Column(GRID_COLUMN_GAME_WON_AMOUNT + _init_bills).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_GAME_WON_AMOUNT + _init_bills).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Game Jackpot
      .Column(GRID_COLUMN_GAME_JACKPOT + _init_bills).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2641)
      .Column(GRID_COLUMN_GAME_JACKPOT + _init_bills).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(396)
      .Column(GRID_COLUMN_GAME_JACKPOT + _init_bills).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_GAME_JACKPOT + _init_bills).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Game Datetime
      .Column(GRID_COLUMN_GAME_DATETIME + _init_bills).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2641)
      .Column(GRID_COLUMN_GAME_DATETIME + _init_bills).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(416)
      .Column(GRID_COLUMN_GAME_DATETIME + _init_bills).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_GAME_DATETIME + _init_bills).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    End With

  End Sub 'GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Me.chk_reciente.Checked = False
    Me.chk_old.Checked = False
    Me.opt_machines_and_games.Checked = True
    Me.chk_only_actives.Checked = True
    Me.chk_show_game_detail.Enabled = True
    Me.chk_show_game_detail.Checked = True

    Call Me.uc_pr_list.SetDefaultValues()

    Me.chk_terminal_location.Checked = False

    Me.chk_terminal_location.Checked = False

  End Sub ' SetDefaultValues

  ' PURPOSE: Get Sql WHERE to buil SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String

    Dim _str_where As StringBuilder

    _str_where = New StringBuilder()

    ' Filter Dates
    If Me.chk_reciente.Checked And Not Me.chk_old.Checked Then
      _str_where.AppendLine(" AND MM_LAST_REPORTED >= " & GUI_FormatDateDB(WSI.Common.WGDB.Now.AddMinutes(-1)))
    ElseIf Me.chk_old.Checked And Not Me.chk_reciente.Checked Then
      _str_where.AppendLine(" AND MM_LAST_REPORTED <= " & GUI_FormatDateDB(WSI.Common.WGDB.Now.AddMinutes(-1)))
    End If

    ' Filter Providers - Terminals
    _str_where.AppendLine(" AND TE_TERMINAL_ID IN " & Me.uc_pr_list.GetProviderIdListSelected())

    ' Filter Providers - Terminals
    If Me.chk_only_actives.Checked Then
      _str_where.AppendLine(" AND TE_STATUS = 0")
    End If

    Return _str_where.ToString()
  End Function ' GetSqlWhere

  Private Sub SetColumnView()
    Me.Grid.Column(GRID_COLUMN_PLAYED_AMOUNT).Fixed = False
    Me.Grid.Column(GRID_COLUMN_WON_AMOUNT).Fixed = False
    Me.Grid.Column(GRID_COLUMN_JACKPOT).Fixed = False
    Me.Grid.Column(GRID_COLUMN_DATETIME).Fixed = False
    Me.Grid.Column(GRID_COLUMN_GAME_NAME).Fixed = False
    Me.Grid.Column(GRID_COLUMN_GAME_PLAYED_AMOUNT).Fixed = False
    Me.Grid.Column(GRID_COLUMN_GAME_WON_AMOUNT).Fixed = False
    Me.Grid.Column(GRID_COLUMN_GAME_JACKPOT).Fixed = False
    Me.Grid.Column(GRID_COLUMN_GAME_DATETIME).Fixed = False

    If opt_only_machines.Checked = True Then
      Me.Grid.Column(GRID_COLUMN_PLAYED_AMOUNT).Width = GRID_WIDTH_AMOUNT
      Me.Grid.Column(GRID_COLUMN_WON_AMOUNT).Width = GRID_WIDTH_AMOUNT
      Me.Grid.Column(GRID_COLUMN_JACKPOT).Width = GRID_WIDTH_AMOUNT
      Me.Grid.Column(GRID_COLUMN_DATETIME).Width = GRID_WIDTH_DATE
      Me.Grid.Column(GRID_COLUMN_GAME_NAME + m_grid_bills_column_ini).Width = 0
      Me.Grid.Column(GRID_COLUMN_GAME_PLAYED_AMOUNT + m_grid_bills_column_ini).Width = 0
      Me.Grid.Column(GRID_COLUMN_GAME_WON_AMOUNT + m_grid_bills_column_ini).Width = 0
      Me.Grid.Column(GRID_COLUMN_GAME_JACKPOT + m_grid_bills_column_ini).Width = 0
      Me.Grid.Column(GRID_COLUMN_GAME_DATETIME + m_grid_bills_column_ini).Width = 0

    ElseIf opt_only_games.Checked = True Then
      Me.Grid.Column(GRID_COLUMN_PLAYED_AMOUNT).Width = 0
      Me.Grid.Column(GRID_COLUMN_WON_AMOUNT).Width = 0
      Me.Grid.Column(GRID_COLUMN_JACKPOT).Width = 0
      Me.Grid.Column(GRID_COLUMN_DATETIME).Width = 0
      Me.Grid.Column(GRID_COLUMN_GAME_NAME).Width = GRID_WIDTH_GAME
      Me.Grid.Column(GRID_COLUMN_GAME_PLAYED_AMOUNT).Width = GRID_WIDTH_AMOUNT
      Me.Grid.Column(GRID_COLUMN_GAME_WON_AMOUNT).Width = GRID_WIDTH_AMOUNT
      Me.Grid.Column(GRID_COLUMN_GAME_JACKPOT).Width = GRID_WIDTH_AMOUNT
      Me.Grid.Column(GRID_COLUMN_GAME_DATETIME).Width = GRID_WIDTH_DATE

    Else
      Me.Grid.Column(GRID_COLUMN_PLAYED_AMOUNT).Width = GRID_WIDTH_AMOUNT
      Me.Grid.Column(GRID_COLUMN_WON_AMOUNT).Width = GRID_WIDTH_AMOUNT
      Me.Grid.Column(GRID_COLUMN_JACKPOT).Width = GRID_WIDTH_AMOUNT
      Me.Grid.Column(GRID_COLUMN_DATETIME).Width = GRID_WIDTH_DATE
      Me.Grid.Column(GRID_COLUMN_GAME_NAME + m_grid_bills_column_ini).Width = GRID_WIDTH_GAME
      Me.Grid.Column(GRID_COLUMN_GAME_PLAYED_AMOUNT + m_grid_bills_column_ini).Width = GRID_WIDTH_AMOUNT
      Me.Grid.Column(GRID_COLUMN_GAME_WON_AMOUNT + m_grid_bills_column_ini).Width = GRID_WIDTH_AMOUNT
      Me.Grid.Column(GRID_COLUMN_GAME_JACKPOT + m_grid_bills_column_ini).Width = GRID_WIDTH_AMOUNT
      Me.Grid.Column(GRID_COLUMN_GAME_DATETIME + m_grid_bills_column_ini).Width = GRID_WIDTH_DATE

    End If
  End Sub

  Private Sub GenerateListofBills()
    Dim _code_list() As Integer = Nothing
    ReDim Preserve _code_list(24)
    Dim _idx_value As Integer

    _code_list(0) = 8

    For _idx_value = 1 To 24
      _code_list(_idx_value) = (_idx_value - 1) + 64
    Next

    m_code_list = _code_list

  End Sub

  ' PURPOSE: Returns a String like [],[],... from a integer list
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String [],[],[],....
  Private Function GetBillsStringList(ByVal Commas As Boolean) As String
    Dim _idx_value As Integer
    Dim _str_result As StringBuilder
    Dim _result As String

    _str_result = New StringBuilder()
    _result = ""

    If Commas Then
      For _idx_value = 0 To m_code_list.Length - 1
        _str_result.AppendLine(m_code_list(_idx_value) & ", ")
      Next
    Else
      For _idx_value = 0 To m_code_list.Length - 1
        _str_result.AppendLine("[" & m_code_list(_idx_value) & "], ")
      Next
    End If

    _result = Strings.Mid(_str_result.ToString(), 1, _str_result.Length - 4)

    Return _result

  End Function

  Private Sub SetRedCells(ByVal MachinesCell As uc_grid.CLASS_CELL_DATA, ByVal GamesCell As uc_grid.CLASS_CELL_DATA)

    MachinesCell.BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_00)
    MachinesCell.ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
    GamesCell.BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_00)
    GamesCell.ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)

  End Sub

  ' PURPOSE: calculates bill_value*count_bills
  '
  '  PARAMS:
  '     - INPUT:
  '           - FromColumn, DbRow
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Decimal Total
  Private Function GetBillTotal(ByVal FromColumn As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Decimal
    Dim _totals As Decimal
    Dim _total_bill As Decimal
    Dim _row_value As Decimal

    _totals = 0
    _row_value = 0

    For _idx As Integer = 0 To m_code_list.Length - 1

      _row_value = IIf(IsDBNull(DbRow.Value(FromColumn + _idx)), 0, DbRow.Value(FromColumn + _idx))

      _total_bill = SAS_Meter.GetBillDenomination(m_code_list(_idx)) * _row_value
      _totals += _total_bill

    Next

    Return _totals
  End Function

  ' PURPOSE: Draw Bill rows and calculate totals.
  '
  '  PARAMS:
  '     - INPUT:
  '           - FromColumn, DbRow
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Decimal Total
  Private Sub PrintGridBills(ByVal RowIndex As Integer, ByVal FirstColumn As Integer, ByVal DbRow As CLASS_DB_ROW)
    Dim _subtotal_column As Integer
    Dim _total As Decimal
    Dim _row_value As Decimal
    Dim _total_bill As Decimal
    Dim _first_sql_column As Int32

    _total = 0
    _subtotal_column = FirstColumn + 2
    _total_bill = 0
    _row_value = 0

    _first_sql_column = FirstColumn + 1 - (TERMINAL_DATA_COLUMNS)

    For _idx_bills_column As Integer = 2 To m_grid_bills_column_ini

      If IsDBNull(DbRow.Value(_first_sql_column + _idx_bills_column)) Then
        _row_value = 0
      Else
        _row_value = DbRow.Value(_first_sql_column + _idx_bills_column)
      End If

      If m_tito_mode AndAlso _idx_bills_column = 2 Then
        Me.Grid.Cell(RowIndex, FirstColumn + _idx_bills_column - 1).Value = GUI_FormatCurrency(_row_value / 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Else
        Me.Grid.Cell(RowIndex, FirstColumn + _idx_bills_column).Value = _row_value
      End If


      _total_bill = SAS_Meter.GetBillDenomination(m_code_list(_idx_bills_column - 2)) * _row_value
      _total += _total_bill

    Next

    Me.Grid.Cell(RowIndex, _subtotal_column).Value = GUI_FormatCurrency(_total, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

  End Sub

  Private Sub GridColumnReIndex()

    TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(m_terminal_report_type)

    GRID_COLUMN_INDEX = 0
    GRID_INIT_TERMINAL_DATA = 1
    GRID_COLUMN_PLAYED_AMOUNT = TERMINAL_DATA_COLUMNS + 1
    GRID_COLUMN_WON_AMOUNT = TERMINAL_DATA_COLUMNS + 2
    GRID_COLUMN_JACKPOT = TERMINAL_DATA_COLUMNS + 3
    GRID_COLUMN_DATETIME = TERMINAL_DATA_COLUMNS + 4
    GRID_COLUMN_GAME_NAME = TERMINAL_DATA_COLUMNS + 5
    GRID_COLUMN_GAME_PLAYED_AMOUNT = TERMINAL_DATA_COLUMNS + 6
    GRID_COLUMN_GAME_WON_AMOUNT = TERMINAL_DATA_COLUMNS + 7
    GRID_COLUMN_GAME_JACKPOT = TERMINAL_DATA_COLUMNS + 8
    GRID_COLUMN_GAME_DATETIME = TERMINAL_DATA_COLUMNS + 9

    GRID_COLUMNS = TERMINAL_DATA_COLUMNS + 10

  End Sub

#End Region  ' Private Functions

#Region "Events"

  Private Sub opt_machines_and_games_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_machines_and_games.CheckedChanged
    If Me.chk_show_game_detail.Enabled = True Then
      Me.chk_show_game_detail.Enabled = False
    Else
      Me.chk_show_game_detail.Enabled = True
    End If
  End Sub

  Private Sub chk_terminal_location_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminal_location.CheckedChanged
    If chk_terminal_location.Checked Then
      m_terminal_report_type = ReportType.Provider + ReportType.Location
    Else
      m_terminal_report_type = ReportType.Provider
    End If
  End Sub

#End Region ' Events

End Class
