﻿'-----------------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd. 
'-----------------------------------------------------------------------------
'
' MODULE NAME:   frm_table_players_report
' DESCRIPTION:   This screen allows to view table player games report.
' AUTHOR:        Rodrigo González
' CREATION DATE: 03-OCT-2017
' 
' REVISION HISTORY:
'
' Date         Author       Description
' -----------  ------       ---------------------------------------------------------
' 26-SEP-2017  RGR          Initial version
' 25-OCT-2017  RGR          WIGOS-6155 Sprint 52 review (Mex) - Player reports  
' 07-NOV-2017  RGR          Bug 30612:WIGOS-6416 Table players report: reset button missing clean one criterion 
' 15-JUN-2018  MS           Bug 33074: [WIGOS-12958] Filter by Card on 'Table Players Report' does not work.
' -----------------------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Text
Imports System.Data.SqlClient

Public Class frm_table_players_report
  Inherits frm_base_sel

#Region "Constants"
  Private Const SQL_COLUMN_NUMBER As Integer = 0
  Private Const SQL_COLUMN_ACCOUNT_NAME As Integer = 1
  Private Const SQL_COLUMN_TABLE As Integer = 2
  Private Const SQL_COLUMN_START As Integer = 3
  Private Const SQL_COLUMN_END As Integer = 4
  Private Const SQL_COLUMN_BUY_IN As Integer = 5
  Private Const SQL_COLUMN_CHIPS_IN As Integer = 6
  Private Const SQL_COLUMN_NETWIN As Integer = 7

  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_NUMBER As Integer = 1
  Private Const GRID_COLUMN_ACCOUNT_NAME As Integer = 2
  Private Const GRID_COLUMN_TABLE As Integer = 3
  Private Const GRID_COLUMN_START As Integer = 4
  Private Const GRID_COLUMN_END As Integer = 5
  Private Const GRID_COLUMN_BUY_IN As Integer = 6
  Private Const GRID_COLUMN_CHIPS_IN As Integer = 7
  Private Const GRID_COLUMN_NETWIN As Integer = 8

  Private Const GRID_HEADER_ROWS As Integer = 1
  Private Const GRID_COLUMN_TOTAL As Integer = 9

#End Region

#Region "Members"
  Private m_date_from As Date
  Private m_date_to As Date
  Private m_selected_currency As String
  Private m_account As String
  Private m_track_data As String
  Private m_holder_name As String
  Private m_holder_is_vip As Boolean
  Private m_selected_types As String
  Private m_selected_types_names As String

  Private m_total_drop As String
  Private m_total_win As String
  Private m_total_buy_in As String
  Private m_total_chips_in As String
  Private m_total_netwin As String

#End Region

#Region "Structs"
  Private Structure TotalAmount
    Public BuyIN As Decimal
    Public ChipsIN As Decimal
    Public Netwin As Decimal
  End Structure

#End Region

#Region "Overrides"

  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_FORM.FORM_TABLE_PLAYERS_REPORT
    Call MyBase.GUI_SetFormId()

  End Sub

  Protected Overrides Sub GUI_InitControls()

    MyBase.GUI_InitControls()
    MyBase.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False
    MyBase.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8668)

    Me.uc_dsl.Init(GLB_NLS_GUI_ALARMS.Id(443), True)
    Me.uc_dsl.FromDateText = GLB_NLS_GUI_CONTROLS.GetString(339)
    Me.uc_dsl.ToDateText = GLB_NLS_GUI_CONTROLS.GetString(340)

    Me.uc_checked_list_table_type.GroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3409)
    Call Me.uc_checked_list_table_type.ColumnWidth(uc_checked_list.GRID_COLUMN_DESC, 4250)

    If WSI.Common.TITO.Utils.IsTitoMode() Then
      Me.uc_account_select.InitExtendedQuery(False)
    End If

    Me.gb_report.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8669)
    Me.uc_entry_drop.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3411)
    Me.uc_entry_win.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3412)
    Me.uc_entry_buy_in.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5492)
    Me.uc_entry_chips_in.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5287)
    Me.uc_entry_netwin.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8436)

    MyBase.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_AUDITOR.GetString(452) ' Salir

    Call FillTableTypeFilterGrid()
    Call SetDefaultValues()
    Call GUI_StyleSheet()

  End Sub

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()

  End Sub

  Protected Overrides Function GUI_GetQueryType() As GUI_Controls.frm_base_sel.ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM

  End Function

  Protected Overrides Function GUI_FilterCheck() As Boolean
    If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate > Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    End If

    If Not Me.uc_account_select.ValidateFormat() Then
      Return False
    End If

    Return True

  End Function

  Protected Overrides Sub GUI_ReportUpdateFilters()
    m_date_from = Date.MinValue
    m_date_to = WSI.Common.WGDB.Now
    m_account = String.Empty
    m_track_data = String.Empty
    m_holder_name = String.Empty


    If Me.uc_dsl.FromDateSelected Then
      m_date_from = Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime)
    End If

    If Me.uc_dsl.ToDateSelected Then
      m_date_to = Me.uc_dsl.ToDate.AddHours(Me.uc_dsl.ClosingTime)
    End If

    m_selected_currency = uc_multicurrency.SelectedCurrency

    m_selected_types = Me.uc_checked_list_table_type.SelectedIndexesList()
    m_selected_types_names = Me.uc_checked_list_table_type.SelectedValuesList()

    m_account = Me.uc_account_select.Account
    m_track_data = Me.uc_account_select.TrackData
    m_holder_name = Me.uc_account_select.Holder
    m_holder_is_vip = IIf(String.IsNullOrEmpty(Me.uc_account_select.HolderIsVip), False, True)

  End Sub

  Protected Overrides Sub GUI_ExecuteQueryCustom()
    Dim _db_trx As WSI.Common.DB_TRX
    Dim _cmd As SqlCommand
    Dim _sql_da As SqlDataAdapter
    Dim _data As DataSet
    Dim _account As String
    Dim _internalCardNumber As String
    Dim _rc As Boolean
    Dim card_type As Integer

    _db_trx = Nothing
    _cmd = Nothing
    _sql_da = Nothing
    _data = Nothing
    _account = String.Empty
    _internalCardNumber = String.Empty

    If Not String.IsNullOrEmpty(m_account) Then
      _account = GUI_FormatNumber(m_account, 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
    End If

    Try
      _rc = WSI.Common.CardNumber.TrackDataToInternal(m_track_data, _internalCardNumber, card_type)
      _db_trx = New WSI.Common.DB_TRX()
      _cmd = New SqlCommand("GetTablePlayersReport", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
      _cmd.CommandType = CommandType.StoredProcedure
      _cmd.Parameters.Add("@pStart", SqlDbType.DateTime).Value = m_date_from
      _cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).Value = m_selected_currency
      _cmd.Parameters.Add("@pTableTypes", SqlDbType.NVarChar).Value = m_selected_types
      _cmd.Parameters.Add("@pEnd", SqlDbType.DateTime).Value = m_date_to
      _cmd.Parameters.Add("@pAccountNumber", SqlDbType.NVarChar).Value = _account
      _cmd.Parameters.Add("@pTrackData", SqlDbType.NVarChar).Value = _internalCardNumber
      _cmd.Parameters.Add("@pHolderName", SqlDbType.NVarChar).Value = m_holder_name
      _cmd.Parameters.Add("@pIsVip", SqlDbType.Bit).Value = m_holder_is_vip
      _data = New DataSet()
      _sql_da = New SqlDataAdapter(_cmd)
      _sql_da.Fill(_data)


      Call SetTotals(_data.Tables(1), _data.Tables(0))
      Call SetDataGrid(_data.Tables(2))

    Catch ex As Exception
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "frm_table_players_report", _
                            "GUI_ExecuteQueryCustom", _
                            ex.Message)

    Finally
      If _data IsNot Nothing Then
        _data.Dispose()

      End If

      If _sql_da IsNot Nothing Then
        _sql_da.Dispose()

      End If

      If _cmd IsNot Nothing Then
        _cmd.Dispose()

      End If

      If _db_trx IsNot Nothing Then
        _db_trx.Dispose()

      End If

    End Try
  End Sub

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    Dim _totals As String

    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & Space(1) & GLB_NLS_GUI_AUDITOR.GetString(257), _
                        GUI_FormatDate(m_date_from, _
                        ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                        ENUM_FORMAT_TIME.FORMAT_HHMMSS))
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & Space(1) & GLB_NLS_GUI_AUDITOR.GetString(258), _
                        GUI_FormatDate(m_date_to, _
                        ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                        ENUM_FORMAT_TIME.FORMAT_HHMMSS))

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7382), m_selected_currency)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(3409), m_selected_types_names)

    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(207), m_account)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(209), m_track_data)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(210), m_holder_name)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4802),
                        IIf(m_holder_is_vip, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)))

    PrintData.SetFilter("", " ")

    _totals = String.Format("{0}: {1}, {2}: {3}, {4}: {5}, {6}: {7}, {8}: {9}", Me.uc_entry_drop.Text, m_total_drop,
                                                                                Me.uc_entry_win.Text, m_total_win,
                                                                                Me.uc_entry_buy_in.Text, m_total_buy_in,
                                                                                Me.uc_entry_chips_in.Text, m_total_chips_in,
                                                                                Me.uc_entry_netwin.Text, m_total_netwin)   
    PrintData.SetFilter(gb_report.Text, _totals, False, False)

  End Sub

#End Region

#Region "Public Methods"
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING

    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub
#End Region

#Region "Private Methods"

  Private Sub GUI_StyleSheet()

    With MyBase.Grid
      Call .Init(GRID_COLUMN_TOTAL, GRID_HEADER_ROWS)
      .Sortable = False
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      .Column(GRID_COLUMN_INDEX).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      .Column(GRID_COLUMN_NUMBER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(795)
      .Column(GRID_COLUMN_NUMBER).Width = 1100
      .Column(GRID_COLUMN_NUMBER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_COLUMN_ACCOUNT_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1186)
      .Column(GRID_COLUMN_ACCOUNT_NAME).Width = 3000
      .Column(GRID_COLUMN_ACCOUNT_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT_CENTER

      .Column(GRID_COLUMN_TABLE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3410)
      .Column(GRID_COLUMN_TABLE).Width = 3350
      .Column(GRID_COLUMN_TABLE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT_CENTER

      .Column(GRID_COLUMN_START).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(269)
      .Column(GRID_COLUMN_START).Width = 1700
      .Column(GRID_COLUMN_START).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_COLUMN_END).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(270)
      .Column(GRID_COLUMN_END).Width = 1700
      .Column(GRID_COLUMN_END).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_COLUMN_BUY_IN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5492)
      .Column(GRID_COLUMN_BUY_IN).Width = 1450
      .Column(GRID_COLUMN_BUY_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER

      .Column(GRID_COLUMN_CHIPS_IN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5287)
      .Column(GRID_COLUMN_CHIPS_IN).Width = 1450
      .Column(GRID_COLUMN_CHIPS_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER

      .Column(GRID_COLUMN_NETWIN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8436)
      .Column(GRID_COLUMN_NETWIN).Width = 1450
      .Column(GRID_COLUMN_NETWIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER

    End With

  End Sub

  Private Sub FillTableTypeFilterGrid()
    Dim _data_table_tables_type As DataTable

    Call Me.uc_checked_list_table_type.Clear()
    Call Me.uc_checked_list_table_type.ReDraw(False)

    _data_table_tables_type = GamingTablesType_DBRead()
    Me.uc_checked_list_table_type.Add(_data_table_tables_type, "ID", "NAME")

    If Me.uc_checked_list_table_type.Count() > 0 Then
      Call Me.uc_checked_list_table_type.CurrentRow(0)
    End If

    Call Me.uc_checked_list_table_type.ReDraw(True)

  End Sub

  Private Function GamingTablesType_DBRead() As DataTable
    Dim _str_sql As String
    Dim _data_table_tables_type As DataTable

    _str_sql = "SELECT   gtt_gaming_table_type_id AS ID" & _
              "        , GTT_NAME  AS NAME" & _
              "   FROM   GAMING_TABLES_TYPES " & _
              "  WHERE   GTT_ENABLED =  " & 1 & ""

    _data_table_tables_type = GUI_GetTableUsingSQL(_str_sql, 5000)

    Return _data_table_tables_type
  End Function

  Private Sub SetDefaultValues()
    Dim _current_date As DateTime

    _current_date = WSI.Common.WGDB.Now.AddDays(-1)

    Me.uc_dsl.ClosingTime = GetDefaultClosingTime()

    Me.uc_dsl.FromDate = New DateTime(_current_date.Year, _current_date.Month, _current_date.Day, 0, 0, 0)

    Me.uc_dsl.FromDateSelected = True

    Me.uc_dsl.ToDate = Me.uc_dsl.FromDate.AddDays(1)

    Me.uc_dsl.ToDateSelected = False

    Call Me.uc_checked_list_table_type.SetDefaultValue(True)

    uc_account_select.Clear()

    Me.uc_multicurrency.Init()


  End Sub

  Private Sub SetTotals(ByVal DataOne As DataTable, ByVal DataTwo As DataTable)
    Dim _row As DataRow

    Me.uc_entry_drop.Value = String.Empty
    Me.uc_entry_win.Value = String.Empty
    Me.uc_entry_buy_in.Value = String.Empty
    Me.uc_entry_chips_in.Value = String.Empty
    Me.uc_entry_netwin.Value = String.Empty

    If DataOne IsNot Nothing Then
      If (DataOne.Rows.Count > 0) Then
        _row = DataOne.Rows(0)
        Me.uc_entry_drop.Value = GUI_FormatCurrency(_row(0), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, False)
        Me.uc_entry_win.Value = GUI_FormatCurrency(_row(1), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, False)

      End If

    End If

    If DataTwo IsNot Nothing Then
      If (DataTwo.Rows.Count > 0) Then
        _row = DataTwo.Rows(0)       
        Me.uc_entry_buy_in.Value = GUI_FormatCurrency(_row(0), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, False)
        Me.uc_entry_chips_in.Value = GUI_FormatCurrency(_row(1), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, False)
        Me.uc_entry_netwin.Value = GUI_FormatCurrency(_row(2), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, False)

      End If

    End If

    m_total_drop = Me.uc_entry_drop.Value
    m_total_win = Me.uc_entry_win.Value
    m_total_buy_in = Me.uc_entry_buy_in.Value
    m_total_chips_in = Me.uc_entry_chips_in.Value
    m_total_netwin = Me.uc_entry_netwin.Value


  End Sub

  Private Sub SetDataGrid(ByVal Data As DataTable)
    Dim _totalAmount As TotalAmount

    If (Data IsNot Nothing) Then

      If (Data.Rows.Count > 0) Then
        _totalAmount = New TotalAmount()
        Me.Grid.Clear()

        If Data IsNot Nothing Then

          Call SetDataGridWithCard(Data)

          Call SetDataGridWithNotCard(Data)
        End If

        _totalAmount.BuyIN = Data.Compute("SUM(GTPS_BUY_IN)", "")
        _totalAmount.ChipsIN = Data.Compute("SUM(GTPS_CHIPS_IN)", "")
        _totalAmount.Netwin = Data.Compute("SUM(GTPS_NETWIN)", "")

        AddRowTotalInGrid(_totalAmount, ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00, String.Format("{0}:", GLB_NLS_GUI_PLAYER_TRACKING.GetString(4708)))
      End If

    End If
  End Sub

  Private Sub SetDataGridWithCard(ByVal Data As DataTable)
    Dim _index As Integer
    Dim _row As DataRow
    Dim _totalAmount As TotalAmount
    Dim _rows As DataRow()
    Dim _lsAccounts As List(Of Long)

    _lsAccounts = New List(Of Long)()
    _totalAmount = New TotalAmount()
    _rows = Data.Select(String.Format("AC_IS_ANONYMOUS = {0}", 0))

    For Each _item As DataRow In _rows
      If (Not _lsAccounts.Contains(_item(SQL_COLUMN_NUMBER))) Then
        _lsAccounts.Add(_item(SQL_COLUMN_NUMBER))
      End If

    Next

    For Each _item As Long In _lsAccounts
      _rows = Data.Select(String.Format("AC_ACCOUNT_ID = {0}", _item))
      For _index = 0 To _rows.Length - 1
        _row = _rows(_index)
        If (_index = 0) Then
          Call AddRowInGrid(_row, False, _row(SQL_COLUMN_ACCOUNT_NAME))
        Else
          Call AddRowInGrid(_row, False)
        End If

      Next
      _totalAmount.BuyIN = Data.Compute("SUM(GTPS_BUY_IN)", String.Format("AC_ACCOUNT_ID = {0}", _item))
      _totalAmount.ChipsIN = Data.Compute("SUM(GTPS_CHIPS_IN)", String.Format("AC_ACCOUNT_ID = {0}", _item))
      _totalAmount.Netwin = Data.Compute("SUM(GTPS_NETWIN)", String.Format("AC_ACCOUNT_ID = {0}", _item))
      AddRowTotalInGrid(_totalAmount, ENUM_GUI_COLOR.GUI_COLOR_BLUE_01)
    Next

  End Sub

  Private Sub SetDataGridWithNotCard(ByVal Data As DataTable)
    Dim _index As Integer
    Dim _row As DataRow
    Dim _totalAmount As TotalAmount
    Dim _rows As DataRow()

    _totalAmount = New TotalAmount()
    _rows = Data.Select(String.Format("AC_IS_ANONYMOUS = {0}", 1), "GT_NAME, GTPS_STARTED")

    For _index = 0 To _rows.Length - 1
      _row = _rows(_index)
      Call AddRowInGrid(_row, True, WSI.Common.Resource.String("STR_CASHIER_GAMING_TABLE_PLAYER_UNKNOWN_BUTTON"))

    Next

    _totalAmount.BuyIN = IIf(IsDBNull(Data.Compute("SUM(GTPS_BUY_IN)", String.Format("AC_IS_ANONYMOUS = {0}", 1))), 0, Data.Compute("SUM(GTPS_BUY_IN)", String.Format("AC_IS_ANONYMOUS = {0}", 1)))
    _totalAmount.ChipsIN = IIf(IsDBNull(Data.Compute("SUM(GTPS_CHIPS_IN)", String.Format("AC_IS_ANONYMOUS = {0}", 1))), 0, Data.Compute("SUM(GTPS_CHIPS_IN)", String.Format("AC_IS_ANONYMOUS = {0}", 1)))
    _totalAmount.Netwin = IIf(IsDBNull(Data.Compute("SUM(GTPS_NETWIN)", String.Format("AC_IS_ANONYMOUS = {0}", 1))), 0, Data.Compute("SUM(GTPS_NETWIN)", String.Format("AC_IS_ANONYMOUS = {0}", 1)))
    AddRowTotalInGrid(_totalAmount, ENUM_GUI_COLOR.GUI_COLOR_BLUE_01)

  End Sub

  Private Sub AddRowInGrid(ByVal row As DataRow, ByVal isNotCard As Boolean, Optional ByVal accountName As String = "")
    Dim _index As Integer

    _index = Me.Grid.AddRow()

    If Not row.IsNull(SQL_COLUMN_NUMBER) Then
      If (accountName <> String.Empty OrElse isNotCard) Then
        Me.Grid.Cell(_index, GRID_COLUMN_NUMBER).Value = row(SQL_COLUMN_NUMBER)

      End If
    End If

    Me.Grid.Cell(_index, GRID_COLUMN_ACCOUNT_NAME).Value = accountName

    If Not row.IsNull(SQL_COLUMN_TABLE) Then
      Me.Grid.Cell(_index, GRID_COLUMN_TABLE).Value = row(SQL_COLUMN_TABLE)
    End If

    If Not row.IsNull(SQL_COLUMN_START) Then
      Me.Grid.Cell(_index, GRID_COLUMN_START).Value = GUI_FormatDate(row(SQL_COLUMN_START), _
                                                                                  ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                 ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    If Not row.IsNull(SQL_COLUMN_END) Then
      Me.Grid.Cell(_index, GRID_COLUMN_END).Value = GUI_FormatDate(row(SQL_COLUMN_END), _
                                                                                  ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                 ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    If Not row.IsNull(SQL_COLUMN_BUY_IN) Then
      Me.Grid.Cell(_index, GRID_COLUMN_BUY_IN).Value = GUI_FormatCurrency(row(SQL_COLUMN_BUY_IN), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, False)
    End If

    If Not row.IsNull(SQL_COLUMN_CHIPS_IN) Then
      Me.Grid.Cell(_index, GRID_COLUMN_CHIPS_IN).Value = GUI_FormatCurrency(row(SQL_COLUMN_CHIPS_IN), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, False)
    End If

    If Not row.IsNull(SQL_COLUMN_NETWIN) Then
      Me.Grid.Cell(_index, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(row(SQL_COLUMN_NETWIN), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, False)
    End If

  End Sub

  Private Sub AddRowTotalInGrid(ByVal totalAmount As TotalAmount, ByVal color As ENUM_GUI_COLOR, Optional ByVal label As String = "")
    Dim _index As Integer

    _index = Me.Grid.AddRow()

    Me.Grid.Cell(_index, GRID_COLUMN_BUY_IN).Value = GUI_FormatCurrency(totalAmount.BuyIN, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, False)
    Me.Grid.Cell(_index, GRID_COLUMN_CHIPS_IN).Value = GUI_FormatCurrency(totalAmount.ChipsIN, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, False)
    Me.Grid.Cell(_index, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(totalAmount.Netwin, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, False)
    Me.Grid.Cell(_index, GRID_COLUMN_NUMBER).Value = label

    Me.Grid.Cell(_index, GRID_COLUMN_INDEX).BackColor = GetColor(color)
    Me.Grid.Cell(_index, GRID_COLUMN_NUMBER).BackColor = GetColor(color)
    Me.Grid.Cell(_index, GRID_COLUMN_ACCOUNT_NAME).BackColor = GetColor(color)
    Me.Grid.Cell(_index, GRID_COLUMN_TABLE).BackColor = GetColor(color)
    Me.Grid.Cell(_index, GRID_COLUMN_START).BackColor = GetColor(color)
    Me.Grid.Cell(_index, GRID_COLUMN_END).BackColor = GetColor(color)
    Me.Grid.Cell(_index, GRID_COLUMN_BUY_IN).BackColor = GetColor(color)
    Me.Grid.Cell(_index, GRID_COLUMN_CHIPS_IN).BackColor = GetColor(color)
    Me.Grid.Cell(_index, GRID_COLUMN_NETWIN).BackColor = GetColor(color)

  End Sub

#End Region

End Class