'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_points_to_credit.vb
' DESCRIPTION:   Manages the points to credit grid and the associated days and terminals.
' AUTHOR:        Joan Marc Pepi� Jamison
' CREATION DATE: 04-OCT-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 04-OCT-2013  JPJ    Initial version
' 31-OCT-2013  JPJ    Defect WIG-349 Name swap.
' 31-OCT-2013  JPJ    Defect WIG-353 Case sensitive names.
' 04-NOV-2013  JPJ    Defect WIG-366 Permissions visible/invisible for enabled/disabled.
' 06-NOV-2013  ANG    Fix low bug, Not show wrong db version message twice
' 28-APR-2014  JBP    Added VIP column for set GIFT property. 
'--------------------------------------------------------------------

Imports System.Text
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_Controls.uc_points_to_credit
Imports WSI.Common

Public Class frm_points_to_credit
  Inherits frm_base_edit

#Region " Members "

  Private m_permission As CLASS_GUI_USER.TYPE_PERMISSIONS     ' Permission object
  Private m_delete_operation As Boolean                       ' Shows if the we are deleting a group
  Private m_new As Boolean                                    ' Indicates if there is a new permission for the redemption table
  Private m_delete As Boolean                                 ' Indicates if there is a delete permission for the redemption table

#End Region ' Members

#Region " Constants "

  Private Const FORM_DB_MIN_VERSION As Int16 = 204
  Private Const MAX_NUMBER_TABLES As Int16 = 10

#End Region ' Constants

#Region " Overrides "

  ' PURPOSE: Controls the form button click events
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '    
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Select Case ButtonId
      Case frm_base_edit.ENUM_BUTTON.BUTTON_CUSTOM_0
        Call DeleteRedemptionTable()

      Case frm_base_edit.ENUM_BUTTON.BUTTON_CUSTOM_1
        Call NewRedemptionTable(Me.tab_Tables.TabPages.Count)

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_POINTS_TO_CREDITS

    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId 

  ' PURPOSE: Controls the keypress.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    Try

      If e.KeyChar = Chr(Keys.Enter) AndAlso CType(Me.tab_Tables.SelectedTab.Controls(0), uc_points_to_credit).PreventReturn(sender, e) Then

        Return True
      End If

      Return MyBase.GUI_KeyPress(sender, e)

    Catch ex As Exception

      Debug.WriteLine(ex.Message)

    End Try

  End Function ' GUI_KeyPress

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    ' Set form Name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2714) 'Tablas de regalos de cr�dito

    'Load Permisions
    m_permission = CurrentUser.Permissions(ENUM_FORM.FORM_POINTS_TO_CREDITS)
    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False

    ' Delete button is available depending on the permissions
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1085) 'Borrar

    'New table is available depending on the permissions
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1) 'Nueva
    m_new = m_permission.Write
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = m_new
    'If the new button is not visible then the delete button must not be visible
    m_delete = IIf(m_new, m_permission.Delete, False)
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False

  End Sub ' GUI_InitControls

  ' PURPOSE : Database overridable operations. 
  '           Define specific DB operation for this form.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_POINTS_TO_CREDITS

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          If Me.DbStatus = ENUM_STATUS.STATUS_NOT_SUPPORTED Then
            '' Trick : Unsupported feature message already shown. Not show again
            Exit Sub
          End If
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          If Me.DbStatus = ENUM_STATUS.STATUS_NOT_SUPPORTED Then
            '' Trick : Unsupported feature message already shown. Not show again
            Exit Sub
          End If
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
        End If


      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)
    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE : Set initial data on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    Dim _points_to_credits As CLASS_POINTS_TO_CREDITS
    Dim _item As CLASS_POINTS_TO_CREDITS.TYPE_POINTS_TO_CREDITS
    Dim _tab_number As Integer = 0
    Dim _uc_points As uc_points_to_credit

    Try

      _points_to_credits = DbReadObject

      For Each _item In _points_to_credits.PointsToCreaditsList
        _uc_points = New uc_points_to_credit
        'Tabla
        Me.tab_Tables.TabPages.Add(_tab_number.ToString, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2793) & " '" & _item.Points_Name.ToString & "'")
        Me.tab_Tables.TabPages(_tab_number).Controls.Add(_uc_points)

        AddHandler _uc_points.ButtonClickEvent, AddressOf uc_points_to_credit_ButtonClickEvent
        AddHandler _uc_points.TableNameLeaveEvent, AddressOf uc_points_to_credit_TableNameLeaveEvent

        _uc_points.EnabledAddButton = m_permission.Write
        _uc_points.EnabledDeleteButton = IIf(m_permission.Write, m_permission.Delete, False)
        _uc_points.EnabledGiftButton = m_permission.Write
        _uc_points.TableName = _item.Points_Name
        _uc_points.Description = _item.Points_Description
        _uc_points.SetRedimible(IIf(_item.Points_Credit_Type = ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE, True, False))
        _uc_points.SetTableEnabled(_item.Points_Enabled)
        _uc_points.SetWeekday(_item.Points_Schedule_Weekday)
        _uc_points.TimeFrom = _item.Points_Schedule_Time_From1
        _uc_points.TimeTo = _item.Points_Schedule_Time_To1
        _uc_points.SecondTime = _item.Points_Schedule2_Enabled
        _uc_points.SecondTimeFrom = _item.Points_Schedule_Time_From2
        _uc_points.SecondTimeTo = _item.Points_Schedule_Time_To2
        _uc_points.Sufix = _item.Points_Sufix
        _uc_points.PromotionId = _item.Promotion_Id
        _uc_points.SetTerminalList(_item.Restricted_To_Terminal0, DayOfWeek.Sunday)
        _uc_points.SetTerminalList(_item.Restricted_To_Terminal1, DayOfWeek.Monday)
        _uc_points.SetTerminalList(_item.Restricted_To_Terminal2, DayOfWeek.Tuesday)
        _uc_points.SetTerminalList(_item.Restricted_To_Terminal3, DayOfWeek.Wednesday)
        _uc_points.SetTerminalList(_item.Restricted_To_Terminal4, DayOfWeek.Thursday)
        _uc_points.SetTerminalList(_item.Restricted_To_Terminal5, DayOfWeek.Friday)
        _uc_points.SetTerminalList(_item.Restricted_To_Terminal6, DayOfWeek.Saturday)
        _uc_points.SetGiftTable(_item.Gift_Table)

        If WSI.Common.GeneralParam.GetBoolean("Features", "CashDesk.Draw.02", False) Then
          _uc_points.SetAwardGame(_item.AwardGame)
        End If

        _tab_number = _tab_number + 1

      Next

    Catch ex As Exception

      Debug.WriteLine(ex.Message)

    End Try

  End Sub ' GUI_SetScreenData

  ' PURPOSE : Get data from the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_GetScreenData()
    Dim _points_to_credits As CLASS_POINTS_TO_CREDITS
    Dim _uc_points_tab As uc_points_to_credit
    Dim _item As CLASS_POINTS_TO_CREDITS.TYPE_POINTS_TO_CREDITS
    Dim _tab As TabPage
    Dim _idx_list_id As Int16

    _points_to_credits = DbEditedObject

    _idx_list_id = 0
    For Each _tab In Me.tab_Tables.TabPages
      _uc_points_tab = CType(_tab.Controls(0), uc_points_to_credit)
      _item = _points_to_credits.PointsToCreaditsList(_idx_list_id)

      _item.Points_Name = _uc_points_tab.TableName
      _item.Points_Description = _uc_points_tab.Description
      _item.Points_Credit_Type = IIf(_uc_points_tab.IsRedimible, ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE, ACCOUNT_PROMO_CREDIT_TYPE.NR1)
      _item.Points_Enabled = _uc_points_tab.IsTableEnabled
      _item.Points_Schedule_Weekday = _uc_points_tab.GetWeekday
      _item.Points_Schedule_Time_From1 = _uc_points_tab.TimeFrom
      _item.Points_Schedule_Time_To1 = _uc_points_tab.TimeTo
      _item.Points_Schedule2_Enabled = _uc_points_tab.SecondTime
      _item.Points_Schedule_Time_From2 = _uc_points_tab.SecondTimeFrom
      _item.Points_Schedule_Time_To2 = _uc_points_tab.SecondTimeTo
      _item.Points_Sufix = _uc_points_tab.Sufix
      _item.Restricted_To_Terminal0 = _uc_points_tab.GetTerminalList(DayOfWeek.Sunday)
      _item.Restricted_To_Terminal1 = _uc_points_tab.GetTerminalList(DayOfWeek.Monday)
      _item.Restricted_To_Terminal2 = _uc_points_tab.GetTerminalList(DayOfWeek.Tuesday)
      _item.Restricted_To_Terminal3 = _uc_points_tab.GetTerminalList(DayOfWeek.Wednesday)
      _item.Restricted_To_Terminal4 = _uc_points_tab.GetTerminalList(DayOfWeek.Thursday)
      _item.Restricted_To_Terminal5 = _uc_points_tab.GetTerminalList(DayOfWeek.Friday)
      _item.Restricted_To_Terminal6 = _uc_points_tab.GetTerminalList(DayOfWeek.Saturday)
      _item.Gift_Table = _uc_points_tab.GetChangedDatatable

      If WSI.Common.GeneralParam.GetBoolean("Features", "CashDesk.Draw.02", False) Then
        _item.AwardGame = _uc_points_tab.GetAwardGame
      End If

      _idx_list_id = _idx_list_id + 1
    Next

  End Sub ' GUI_GetScreenData

  ' PURPOSE : Validate the data presented on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Dim _tab As TabPage
    Dim _uc_points_tab As uc_points_to_credit
    Dim _table_error As ENUM_TABLE_ERRORS
    Dim _time_error As ENUM_TIME_ERRORS
    Dim _terminal_list_error As ENUM_TERMINAL_LIST_ERRORS
    Dim _day As String = ""

    'We look over all the dinamic created elements to verify data
    For Each _tab In Me.tab_Tables.TabPages
      _uc_points_tab = CType(_tab.Controls(0), uc_points_to_credit)

      'Tabla name can't be empty
      If _uc_points_tab.TableName = String.Empty Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(253)) 'Nombre
        Me.tab_Tables.SelectedIndex = _tab.TabIndex
        Call _uc_points_tab.SetFocus(ENUM_FOCUS.FOCUS_NAME)

        Return False
      End If

      'Sufix name can't be duplicated
      If Not String.IsNullOrEmpty(_uc_points_tab.Sufix) Then
        If SufixNameDuplicated(_uc_points_tab.Sufix, _tab.TabIndex) Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(114), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2851), _uc_points_tab.Sufix) 'Table
          Me.tab_Tables.SelectedIndex = _tab.TabIndex
          Call _uc_points_tab.SetFocus(ENUM_FOCUS.FOCUS_SUFIX)

          Return False
        End If
      End If

      'Table name can't be duplicated
      If TableNameDuplicated(_uc_points_tab.TableName, _tab.TabIndex) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(114), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2793), _uc_points_tab.TableName) 'Table
        Me.tab_Tables.SelectedIndex = _tab.TabIndex
        Call _uc_points_tab.SetFocus(ENUM_FOCUS.FOCUS_NAME)

        Return False
      End If

      'At least one of the days has to be selected
      If Not _uc_points_tab.CheckAtLeastOneDaySelected Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(110), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        Me.tab_Tables.SelectedIndex = _tab.TabIndex
        Call _uc_points_tab.SetFocus(ENUM_FOCUS.FOCUS_TERMINALS_MONDAY)

        Return False
      End If

      'The selected days must have at least one selected
      _terminal_list_error = _uc_points_tab.CheckAllAtLeastOneSelected
      If _terminal_list_error <> ENUM_TERMINAL_LIST_ERRORS.NO_ERRORS Then
        Me.tab_Tables.SelectedIndex = _tab.TabIndex

        Select Case _terminal_list_error
          Case ENUM_TERMINAL_LIST_ERRORS.MONDAY_LIST
            _day = GLB_NLS_GUI_PLAYER_TRACKING.GetString(301) '"Monday "
            Call _uc_points_tab.SetFocus(ENUM_FOCUS.FOCUS_TERMINALS_MONDAY)
          Case ENUM_TERMINAL_LIST_ERRORS.TUESDAY_LIST
            _day = GLB_NLS_GUI_PLAYER_TRACKING.GetString(302) '"Tuesday "
            Call _uc_points_tab.SetFocus(ENUM_FOCUS.FOCUS_TERMINALS_TUESDAY)
          Case ENUM_TERMINAL_LIST_ERRORS.WEDNESDAY_LIST
            _day = GLB_NLS_GUI_PLAYER_TRACKING.GetString(303) '"Wednesday "
            Call _uc_points_tab.SetFocus(ENUM_FOCUS.FOCUS_TERMINALS_WEDNESDAY)
          Case ENUM_TERMINAL_LIST_ERRORS.THURSDAY_LIST
            _day = GLB_NLS_GUI_PLAYER_TRACKING.GetString(304) '"Thursday "
            Call _uc_points_tab.SetFocus(ENUM_FOCUS.FOCUS_TERMINALS_THURSDAY)
          Case ENUM_TERMINAL_LIST_ERRORS.FRIDAY_LIST
            _day = GLB_NLS_GUI_PLAYER_TRACKING.GetString(305) '"Friday "
            Call _uc_points_tab.SetFocus(ENUM_FOCUS.FOCUS_TERMINALS_FRIDAY)
          Case ENUM_TERMINAL_LIST_ERRORS.SATURDAY_LIST
            _day = GLB_NLS_GUI_PLAYER_TRACKING.GetString(306) '"Saturday "
            Call _uc_points_tab.SetFocus(ENUM_FOCUS.FOCUS_TERMINALS_SATURDAY)
          Case ENUM_TERMINAL_LIST_ERRORS.SUNDAY_LIST
            _day = GLB_NLS_GUI_PLAYER_TRACKING.GetString(307) '"Sunday "
            Call _uc_points_tab.SetFocus(ENUM_FOCUS.FOCUS_TERMINALS_SUNDAY)
        End Select
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2840), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2794), _day)

        Return False
      End If

      'We check the scheduled time
      _time_error = _uc_points_tab.CheckScheduledTime
      If _time_error <> ENUM_TIME_ERRORS.NO_ERRORS Then

        Me.tab_Tables.SelectedIndex = _tab.TabIndex
        Select Case _time_error
          Case ENUM_TIME_ERRORS.TIME1_FROM_TO
            '.
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1668), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
            Call _uc_points_tab.SetFocus(ENUM_FOCUS.FOCUS_FROM_SCHEDULE)
          Case ENUM_TIME_ERRORS.TIME2_FROM_TO
            '.
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1668), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
            Call _uc_points_tab.SetFocus(ENUM_FOCUS.FOCUS_TO_SCHEDULE)
          Case ENUM_TIME_ERRORS.OVERLAPING_TIMES
            'overlapping time ranges.
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1667), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1666))
            Call _uc_points_tab.SetFocus(ENUM_FOCUS.FOCUS_FROM_SCHEDULE)
        End Select

        Return False
      End If

      'We check the gift table
      _table_error = _uc_points_tab.CheckGiftGrid
      If _table_error <> ENUM_TABLE_ERRORS.NO_ERRORS Then
        Select Case _table_error
          Case ENUM_TABLE_ERRORS.LEVELS_EMPY
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2795), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING) 'Debe indicar un valor por puntos para al menos uno de los niveles
          Case ENUM_TABLE_ERRORS.CREDIT_EMPTY
            If _uc_points_tab.IsRedimible Then
              Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(114), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(313)) '"El campo %1 no puede estar vac�o" Cr�ditos Redimibles
            Else
              Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(114), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(357)) '"El campo %1 no puede estar vac�o" Cr�ditos No Redimibles
            End If
          Case ENUM_TABLE_ERRORS.CREDIT_DUPLICATED
            'Duplicate values found
            If _uc_points_tab.IsRedimible Then
              Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2796), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(313)) '"El campo %1 no puede estar vac�o" Cr�ditos Redimibles
            Else
              Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2796), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(357)) '"El campo %1 no puede estar vac�o" Cr�ditos No Redimibles
            End If
        End Select
        Me.tab_Tables.SelectedIndex = _tab.TabIndex
        Call _uc_points_tab.SetFocus(ENUM_FOCUS.FOCUS_TABLE)

        Return False
      End If

      ' TODO-UNRANKED TEAM: PENDING MARGE FROM MAIN-TELEVISA
      'If WSI.Common.GeneralParam.GetBoolean("Features", "CashDesk.Draw.02", False) AndAlso Not _uc_points_tab.CheckAwardWithGame() Then
      '  Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8677), ENUM_MB_TYPE.MB_TYPE_WARNING)
      '  Call _uc_points_tab.SetFocus(ENUM_FOCUS.FOCUS_AWARD_GAME)

      '  Return False
      'End If

    Next

    Return True

  End Function ' GUI_IsScreenDataOk

#End Region ' Overrides

#Region " Events "

  ' PURPOSE : Handles the button that is pressed from the control
  '
  '  PARAMS :
  '     - INPUT :
  '       -TableName: Name of the redemption table
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub uc_points_to_credit_ButtonClickEvent(ByVal ButtonId As ENUM_BUTTON_TYPE, _
                                                   ByVal Gift_Id As Int64)
    Dim frm As frm_gift_edit

    If Gift_Id <> 0 Then
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      ' Create instance
      frm = New frm_gift_edit
      Call frm.ShowEditItem(Gift_Id, "")

      Windows.Forms.Cursor.Current = Cursors.Default
    End If

    frm = Nothing

  End Sub ' uc_points_to_credit_ButtonClickEvent

  ' PURPOSE : Handles the lost_focus of the textbox in the user control
  '
  '  PARAMS :
  '     - INPUT :
  '       -TableName: Name of the redemption table
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub uc_points_to_credit_TableNameLeaveEvent(ByVal TableName As String)

    If Not Me.tab_Tables.SelectedTab Is Nothing Then
      Me.tab_Tables.SelectedTab.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2793) & " '" & TableName & "'"
    End If

  End Sub ' uc_points_to_credit_TableNameLeaveEvent

  ' PURPOSE : Handles the tab change
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub tab_Tables_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tab_Tables.SelectedIndexChanged

    Try

      If m_delete AndAlso GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = False AndAlso m_permission.Delete Then
        GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = (CType(Me.tab_Tables.SelectedTab.Controls(0), uc_points_to_credit).PromotionId = 0)
      End If

    Catch ex As Exception

      Debug.WriteLine(ex.Message)

    End Try

  End Sub ' tab_Tables_SelectedIndexChanged

#End Region ' Events

#Region " Private functions "

  ' PURPOSE : Creates a new redemption table
  '
  '  PARAMS :
  '     - INPUT :
  '       -TabNumber: 
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub NewRedemptionTable(ByVal TabNumber As Integer)
    Dim _item As New CLASS_POINTS_TO_CREDITS.TYPE_POINTS_TO_CREDITS
    Dim _uc_points As uc_points_to_credit
    Dim _points_to_credits As CLASS_POINTS_TO_CREDITS

    Try

      If TabNumber >= GeneralParam.GetInt32("Gifts.PointsToCredits", "MaxNumberTables", MAX_NUMBER_TABLES) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2848), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GeneralParam.GetInt32("Gifts.PointsToCredits", "MaxNumberTables", MAX_NUMBER_TABLES))

        Return
      End If

      _uc_points = New uc_points_to_credit
      'Tab page
      Me.tab_Tables.TabPages.Add(TabNumber.ToString, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2793) & " ''")
      Me.tab_Tables.TabPages(TabNumber).Controls.Add(_uc_points)

      AddHandler _uc_points.ButtonClickEvent, AddressOf uc_points_to_credit_ButtonClickEvent
      AddHandler _uc_points.TableNameLeaveEvent, AddressOf uc_points_to_credit_TableNameLeaveEvent

      _item.Points_Name = _uc_points.TableName
      _item.Points_Description = _uc_points.Description
      _item.Points_Credit_Type = IIf(_uc_points.IsRedimible, ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE, ACCOUNT_PROMO_CREDIT_TYPE.NR1)
      _item.Points_Enabled = _uc_points.IsTableEnabled
      _item.Points_Schedule_Weekday = _uc_points.GetWeekday
      _item.Points_Schedule_Time_From1 = _uc_points.TimeFrom
      _item.Points_Schedule_Time_To1 = _uc_points.TimeTo
      _item.Points_Schedule2_Enabled = _uc_points.SecondTime
      _item.Points_Schedule_Time_From2 = _uc_points.SecondTimeFrom
      _item.Points_Schedule_Time_To2 = _uc_points.SecondTimeTo
      _item.Points_Sufix = _uc_points.Sufix
      _item.Restricted_To_Terminal0 = New TerminalList()
      _item.Restricted_To_Terminal1 = New TerminalList()
      _item.Restricted_To_Terminal2 = New TerminalList()
      _item.Restricted_To_Terminal3 = New TerminalList()
      _item.Restricted_To_Terminal4 = New TerminalList()
      _item.Restricted_To_Terminal5 = New TerminalList()
      _item.Restricted_To_Terminal6 = New TerminalList()
      _uc_points.SetTerminalList(_item.Restricted_To_Terminal0, DayOfWeek.Sunday)
      _uc_points.SetTerminalList(_item.Restricted_To_Terminal1, DayOfWeek.Monday)
      _uc_points.SetTerminalList(_item.Restricted_To_Terminal2, DayOfWeek.Tuesday)
      _uc_points.SetTerminalList(_item.Restricted_To_Terminal3, DayOfWeek.Wednesday)
      _uc_points.SetTerminalList(_item.Restricted_To_Terminal4, DayOfWeek.Thursday)
      _uc_points.SetTerminalList(_item.Restricted_To_Terminal5, DayOfWeek.Friday)
      _uc_points.SetTerminalList(_item.Restricted_To_Terminal6, DayOfWeek.Saturday)
      _uc_points.TabId = TabNumber
      _uc_points.EnabledAddButton = m_permission.Write
      _uc_points.EnabledDeleteButton = IIf(m_permission.Write, m_permission.Delete, False)
      _uc_points.EnabledGiftButton = m_permission.Write
      _uc_points.SetGiftTable(_item.Gift_Table)

      _points_to_credits = DbEditedObject

      Call _points_to_credits.PointsToCreaditsList.Add(_item)

      'We set the focus in the tab page and in the entry name 
      Me.tab_Tables.SelectedIndex = TabNumber
      Call _uc_points.SetFocus(ENUM_FOCUS.FOCUS_NAME)

      If m_delete Then
        GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = True
      End If
      If m_new Then
        GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = False
      End If
    Catch ex As Exception

      Debug.WriteLine(ex.Message)

    End Try

  End Sub ' NewRedemptionTable

  ' PURPOSE : Check if there ir more than one table with the same name
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '         
  ' RETURNS :
  '     -Boolean:
  Private Function TableNameDuplicated(ByVal TableName As String, ByVal TabIndex As Int16) As Boolean
    Dim _uc_points_tab As uc_points_to_credit
    Dim _tab As TabPage

    Try

      For Each _tab In Me.tab_Tables.TabPages
        _uc_points_tab = CType(_tab.Controls(0), uc_points_to_credit)

        If TableName.Trim.ToLower = _uc_points_tab.TableName.Trim.ToLower AndAlso TabIndex <> _tab.TabIndex Then

          Return True
        End If
      Next

    Catch ex As Exception

      Debug.WriteLine(ex.Message)

    End Try

    Return False

  End Function ' TableNameDuplicated

  ' PURPOSE : Check if there ir more than one table with the same sufix
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '         
  ' RETURNS :
  '     -Boolean:
  Private Function SufixNameDuplicated(ByVal Sufix As String, ByVal TabIndex As Int16) As Boolean
    Dim _uc_points_tab As uc_points_to_credit
    Dim _tab As TabPage

    Try

      For Each _tab In Me.tab_Tables.TabPages
        _uc_points_tab = CType(_tab.Controls(0), uc_points_to_credit)

        If Sufix.Trim.ToLower = _uc_points_tab.Sufix.Trim.ToLower AndAlso TabIndex <> _tab.TabIndex Then

          Return True
        End If
      Next

    Catch ex As Exception

      Debug.WriteLine(ex.Message)

    End Try

    Return False

  End Function ' SufixNameDuplicated

  ' PURPOSE : Deletes a redemption table
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '         
  ' RETURNS :
  Private Sub DeleteRedemptionTable()
    Dim _points_to_credits As CLASS_POINTS_TO_CREDITS

    Try
      If Not Me.tab_Tables.SelectedTab Is Nothing AndAlso CType(Me.tab_Tables.SelectedTab.Controls(0), uc_points_to_credit).PromotionId = 0 Then
        _points_to_credits = DbEditedObject
        Call _points_to_credits.PointsToCreaditsList.RemoveAt(Me.tab_Tables.SelectedTab.TabIndex - 1)

        Me.tab_Tables.TabPages.Remove(Me.tab_Tables.SelectedTab)

        If m_delete Then
          GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False
        End If
        If m_new Then
          GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = True
        End If
      End If

    Catch ex As Exception

      Debug.WriteLine(ex.Message)

    End Try

  End Sub ' DeleteRedemptionTable

#End Region ' Private functions

End Class ' frm_points_to_credit