'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_bank_payment_order_sel
' DESCRIPTION:   This screen allows to view the payment orders.
' AUTHOR:        Humberto Braojos
' CREATION DATE: 14-JAN-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 17-JAN-2013  HBB    Initial version
' 12-FEB-2013  JMM    uc_account_sel.ValidateFormat added on FilterCheck
' 25-APR-2013  RBG    #746 It makes no sense to work in this form with multiselection
' 13-DEC-2013  RMS    Adapted to TITO mode to show payment orders linked to virtual accounts
' 06-MAY-2014  JBP    Added Vip client filter at reports.
' 13-MAR-2015  FOS    TASK 623: GUI: modify PaymentCheck Form 
' 27-AUG-2015  ETP    BUG-3830 Fixed. Applicacion crashes when selecting Total Row.
' 14-OCT-2015  DLL    Fixed BUG-3830: Any rows can be selected
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Text
Imports WSI.Common
Imports System.Data.SqlClient

Public Class frm_payment_order_sel
  Inherits frm_base_sel

#Region "Constants"

  Private Const GRID_COLUMNS_COUNT As Integer = 11
  Private Const GRID_HEADERS_COUNT As Integer = 2

  ' DB Columns
  Private Const SQL_COLUMN_PAYMENT_ORDER_ID As Integer = 0
  Private Const SQL_COLUMN_ACCOUNT_NUMBER As Integer = 1
  Private Const SQL_COLUMN_CLIENT_NAME1 As Integer = 2
  Private Const SQL_COLUMN_CLIENT_NAME2 As Integer = 3
  Private Const SQL_COLUMN_CLIENT_NAME3 As Integer = 4
  Private Const SQL_COLUMN_BANK_DESCRIPTION = 5
  Private Const SQL_COLUMN_BANK_NAME = 6
  Private Const SQL_COLUMN_AWARD_DATE As Integer = 7
  Private Const SQL_COLUMN_CHECKS_PAYMENT As Integer = 8
  Private Const SQL_COLUMN_CASH_PAYMENT As Integer = 9
  Private Const SQL_COLUMN_FULL_PAYMENT As Integer = 10
  Private Const SQL_COLUMN_BANK_ACCOUNT_NUMBER As Integer = 11
  Private Const SQL_COLUMN_ACCOUNT_TYPE As Integer = 12

  'Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_PAYMENT_ORDER_ID As Integer = 1
  Private Const GRID_COLUMN_AWARD_DATE As Integer = 2
  Private Const GRID_COLUMN_CHECK_PAYMENT As Integer = 3
  Private Const GRID_COLUMN_CASH_PAYMENT As Integer = 4
  Private Const GRID_COLUMN_FULL_PAYMENT As Integer = 5
  Private Const GRID_COLUMN_BANK_ACCOUNT_NUMBER = 8
  Private Const GRID_COLUMN_BANK_DESCRIPTION = 6
  Private Const GRID_COLUMN_BANK_NAME = 7
  Private Const GRID_COLUMN_ACCOUNT_NUMBER As Integer = 9
  Private Const GRID_COLUMN_CLIENT_NAME As Integer = 10

  'Width
  Private Const GRID_WIDTH_INDEX As Integer = 200
  Private Const GRID_WIDTH_PAYMENT_ORDER_ID As Integer = 0
  Private Const GRID_WIDTH_ACCOUNT_NUMBER As Integer = 1150
  Private Const GRID_WIDTH_CLIENT_NAME As Integer = 2500
  Private Const GRID_WIDTH_AWARD_DATE As Integer = 2200
  Private Const GRID_WIDTH_CHECKS_PAYMENT As Integer = 1600
  Private Const GRID_WIDTH_CASH_PAYMENT As Integer = 1600
  Private Const GRID_WIDTH_FULL_PAYMENT As Integer = 1600
  Private Const GRID_WIDTH_BANK_DESCRIPTION = 2700
  Private Const GRID_WIDTH_BANK_NAME = 2000
  Private Const GRID_WIDTH_BANK_ACCOUNT_NUMBER = 2000

  'Filter Length
  Private Const LENGTH_BANK_NAME_FILTER As Integer = 50
  Private Const LENGTH_ACCOUNT_INFO_FILTER As Integer = 50

  'Bank Grid
  Private Const GRID_BANK_ACCOUNT_HEADER_ROWS As Integer = 0
  Private Const GRID_BANK_ACCOUNT_COLUMNS As Integer = 3
  Private Const GRID_BANK_ACCOUNT_ID As Integer = 0
  Private Const GRID_BANK_ACCOUNT_COLUMN_CHECKED As Integer = 1
  Private Const GRID_BANK_ACCOUNT_COLUMN_DESCRIPTION As Integer = 2

  'Bank Grid Width
  Private Const GRID_BANK_ACCOUNT_WIDTH_COLUMN_DESCRIPTION As Integer = 3355
  Private Const GRID_BANK_ACCOUNT_WIDTH_COLUMN_CHECKED As Integer = 300

  Private Const COUNTER_PAYMENT_ORDER_OK As Integer = 1

  Private Const COLOR_DEVICE_OK_BACK = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00

#End Region

#Region "MEMBERS"
  Private m_date_from As String
  Private m_date_to As String
  Private m_bank_accounts As String
  Private m_account As String
  Private m_track_data As String
  Private m_holder As String
  Private m_holder_is_vip As String
  Private m_barcode As WSI.Common.Barcode

  'DRV 10-NOV-14
  Private m_total_check As Decimal
  Private m_total_cash As Decimal
  Private m_total_payment As Decimal

  Private m_barcode_handler As KbdMsgFilterHandler
#End Region

#Region "OVERRIDES"

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_PAYMENT_ORDER_SEL

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' For TITO Mode
    If WSI.Common.TITO.Utils.IsTitoMode Then
      ' Payment orders can be linked to a virtual account
      uc_account_filter.InitExtendedQuery(True)
    End If

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1563)

    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False

    '13-MAR-2015 FOS
    If Not PaymentOrder.IsGenerateDocumentEnabled(True) Then
      Me.uc_account_filter.Location = New Point(Me.uc_account_filter.Location.X - Me.gb_bank_accounts.Location.X + 10, Me.uc_account_filter.Location.Y)
      Me.gb_bank_accounts.Visible = False
    End If

    Me.ef_barcode.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4919)
    Me.ef_barcode.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, 20)

    KeyboardMessageFilter.Init()
    m_barcode_handler = AddressOf Me.BarcodeEvent
    KeyboardMessageFilter.RegisterHandler(Me, BarcodeType.PayOrder, m_barcode_handler)

    Me.dg_bank_account.PanelRightVisible = False
    Me.dg_bank_account.Sortable = False
    Me.dg_bank_account.TopRow = -2
    Me.gb_bank_accounts.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1553)

    ds_from_to.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1574)

    Call SetDefaultDate()
    Call GUI_StyleSheetAccountAlias()
    Call GUI_StyleSheet()
    Call SetGridBankAccountValues()

  End Sub ' GUI_InitControls

  'PURPOSE: Executed just before closing form
  '         
  ' PARAMS:
  '    - INPUT :
  '
  '    - OUTPUT :
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)
    KeyboardMessageFilter.UnregisterHandler(Me, m_barcode_handler)
    MyBase.GUI_Closing(CloseCanceled)
  End Sub

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _sb As StringBuilder
    Dim _string_where As String
    Dim _with_index As String

    _string_where = GetSqlWhere()

    _with_index = ""

    If Me.uc_account_filter.GetFilterSQL() <> "" Then
      _with_index = " WITH(INDEX(IX_apo_account_datetime)) "
    Else
      If Me.ds_from_to.GetSqlFilterCondition("APO_DATETIME") <> "" Then
        _with_index = " WITH(INDEX(IX_apo_datetime)) "
      End If
    End If

    _sb = New StringBuilder()
    _sb.AppendLine("SELECT   APO_OPERATION_ID   ")
    _sb.AppendLine("       , APO_ACCOUNT_ID     ")
    _sb.AppendLine("       , APO_PLAYER_NAME1   ")
    _sb.AppendLine("       , APO_PLAYER_NAME2   ")
    _sb.AppendLine("       , APO_PLAYER_NAME3   ")
    _sb.AppendLine("       , BA_DESCRIPTION     ")
    _sb.AppendLine("       , APO_BANK_NAME      ")
    _sb.AppendLine("       , APO_DATETIME       ")
    _sb.AppendLine("       , APO_CHECK_PAYMENT  ")
    _sb.AppendLine("       , APO_CASH_PAYMENT   ")
    _sb.AppendLine("       , APO_TOTAL_PAYMENT  ")
    _sb.AppendLine("       , APO_BANK_ACCOUNT_NUMBER ")
    _sb.AppendLine("       , AC_TYPE                 ")
    _sb.AppendLine("  FROM   ACCOUNT_PAYMENT_ORDERS           " & _with_index)
    _sb.AppendLine(" INNER   JOIN ACCOUNTS      ON APO_ACCOUNT_ID = AC_ACCOUNT_ID ")
    _sb.AppendLine("  LEFT   JOIN BANK_ACCOUNTS ON BA_ACCOUNT_ID  = APO_BANK_ID ")

    _sb.AppendLine(_string_where)
    _sb.AppendLine("ORDER BY APO_DATETIME DESC                ")

    Return _sb.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE: Checks whether the specified row contains data or not
  '
  '  PARAMS:
  '     - INPUT:
  '           - IdxRow: row to check
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - TRUE: selected row contains data
  '     - FALSE: selected row does not exist or contains no data

  Private Function IsValidDataRow(ByVal IdxRow As Integer) As Boolean

    ' Skip Totalizator row!!!
    Return (Me.Grid.Cell(IdxRow, GRID_COLUMN_PAYMENT_ORDER_ID).Value <> "")

  End Function ' IsValidDataRow

  Protected Overrides Sub GUI_BeforeFirstRow()
    m_total_cash = 0
    m_total_check = 0
    m_total_payment = 0

    MyBase.GUI_BeforeFirstRow()
  End Sub
  ' PURPOSE : Fills the rows
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                        ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _full_name As String

    ' PAYMENT ORDER ID
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PAYMENT_ORDER_ID).Value = DbRow.Value(SQL_COLUMN_PAYMENT_ORDER_ID)

    ' ACCOUNT NUNBER
    If WSI.Common.TITO.Utils.IsVirtualAccountByType(CType(DbRow.Value(SQL_COLUMN_ACCOUNT_TYPE), AccountType)) Then
      ' Don't show virtual accounts in TITO mode
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_NUMBER).Value = String.Empty

    Else
      ' Show client account
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_NUMBER).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_NUMBER)

    End If

    ' CLIENT NAME 
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_CLIENT_NAME1)) Then
      _full_name = DbRow.Value(SQL_COLUMN_CLIENT_NAME1)

      If Not IsDBNull(DbRow.Value(SQL_COLUMN_CLIENT_NAME2)) Then
        _full_name = _full_name & " " & DbRow.Value(SQL_COLUMN_CLIENT_NAME2)
      End If

      If Not IsDBNull(DbRow.Value(SQL_COLUMN_CLIENT_NAME3)) Then
        _full_name = _full_name & " " & DbRow.Value(SQL_COLUMN_CLIENT_NAME3)
      End If

      Me.Grid.Cell(RowIndex, GRID_COLUMN_CLIENT_NAME).Value = _full_name
    End If

    ' AWARD DATE
    Me.Grid.Cell(RowIndex, GRID_COLUMN_AWARD_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_AWARD_DATE), _
                                                                          ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                          ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    '13-MAR-2015 FOS
    If PaymentOrder.IsGenerateDocumentEnabled(True) Then
      ' BANK ACCOUNT DESCRIPTION
      If IsDBNull(DbRow.Value(SQL_COLUMN_BANK_DESCRIPTION)) OrElse DbRow.Value(SQL_COLUMN_BANK_DESCRIPTION) Is Nothing OrElse DbRow.Value(SQL_COLUMN_BANK_DESCRIPTION).ToString().Trim() = "" Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_BANK_DESCRIPTION).Value = ""
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_BANK_DESCRIPTION).Value = DbRow.Value(SQL_COLUMN_BANK_DESCRIPTION)
      End If

      ' BANK NAME
      Me.Grid.Cell(RowIndex, GRID_COLUMN_BANK_NAME).Value = DbRow.Value(SQL_COLUMN_BANK_NAME)
      ' BANK ACCOUNT NUMBER
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_BANK_ACCOUNT_NUMBER)) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_BANK_ACCOUNT_NUMBER).Value = DbRow.Value(SQL_COLUMN_BANK_ACCOUNT_NUMBER)
      End If
    End If
    '-------------------------------------------------------------------------------

    ' CHECK PAYMENT
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CHECK_PAYMENT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_CHECKS_PAYMENT))
    m_total_check += DbRow.Value(SQL_COLUMN_CHECKS_PAYMENT)

    ' CASH PAYMENT
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_PAYMENT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_CASH_PAYMENT))
    m_total_cash += DbRow.Value(SQL_COLUMN_CASH_PAYMENT)

    ' FULL PAYMENT
    Me.Grid.Cell(RowIndex, GRID_COLUMN_FULL_PAYMENT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_FULL_PAYMENT))
    m_total_payment += DbRow.Value(SQL_COLUMN_FULL_PAYMENT)

    Return True

  End Function ' GUI_SetupRow

  Protected Overrides Sub GUI_AfterLastRow()
    Dim _idx_row As Integer

    _idx_row = Me.Grid.AddRow()

    Me.Grid.Cell(_idx_row, GRID_COLUMN_AWARD_DATE).Value = GLB_NLS_GUI_INVOICING.GetString(205) 'TOTAL:
    Me.Grid.Cell(_idx_row, GRID_COLUMN_AWARD_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    Me.Grid.Cell(_idx_row, GRID_COLUMN_CHECK_PAYMENT).Value = GUI_FormatCurrency(m_total_check)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_CASH_PAYMENT).Value = GUI_FormatCurrency(m_total_cash)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_FULL_PAYMENT).Value = GUI_FormatCurrency(m_total_payment)

    Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

    MyBase.GUI_AfterLastRow()
  End Sub

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean
    Dim _barcode As WSI.Common.Barcode

    If Me.ds_from_to.FromDateSelected And Me.ds_from_to.ToDateSelected Then
      If Me.ds_from_to.FromDate > Me.ds_from_to.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.ds_from_to.Focus()

        Return False
      End If
    End If

    ' Check fields on uc_account_sel are ok
    If Not Me.uc_account_filter.ValidateFormat Then
      Return False
    End If

    ' Check if enter value in field barcode is a barcode
    If Me.ef_barcode.Value.Length > 0 Then
      If WSI.Common.Barcode.IsValid(Me.ef_barcode.Value) Then

        _barcode = Nothing
        Barcode.TryParse(Me.ef_barcode.Value, _barcode)

        If _barcode Is Nothing Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
                                mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.ef_barcode.Text)
          'Call Me.ef_barcode.Focus()
          Return False
        End If

      Else
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
                                     mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.ef_barcode.Text)
        Return False
      End If

    End If

    Return True

  End Function ' GUI_FilterCheck

  ' PURPOSE: Call GUI_ButtonClick with the Cursor in Wait Mode. Finally restore Cursor to Default.
  '
  '  PARAMS:
  '     - INPUT:
  '           - ButtonId As ENUM_BUTTON
  '     - OUTPUT:
  '           -
  '
  ' RETURNS:
  '     -
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_NEW

      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        Call GUI_EditSelectedItem()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY
        Call GUI_StyleSheet()
        Call MyBase.GUI_ButtonClick(ButtonId)

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub ' GUI_ButtonClick

  ' PURPOSE: Activated when a row of the grid is double clicked or selected, so it can be edited
  '  PARAMS:
  '     - INPUT:
  '               
  '     - OUTPUT:
  '          
  ' RETURNS:
  '     - none
  Protected Overrides Sub GUI_EditSelectedItem()

    Dim _idx_row As Int32
    Dim _payment_order_id As Integer
    Dim _frm_edit As frm_payment_order_edit

    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    If _idx_row = Me.Grid.NumRows Then

      Return
    End If

    'ETP BUG Fixed Applicacion crashes when selecting Total Row.
    If Not IsValidDataRow(_idx_row) Then

      Return
    End If

    ' Get the BankAccount ID 
    _payment_order_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_PAYMENT_ORDER_ID).Value
    _frm_edit = New frm_payment_order_edit()

    Call _frm_edit.ShowSelectedItem(_payment_order_id)

    _frm_edit = Nothing

    Call Me.Grid.Focus()

  End Sub ' GUI_EditSelectedItem

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    SetDefaultValues()
  End Sub 'GUI_FilterReset

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS: 
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    If ef_barcode.Value.Length > 0 Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4919), ef_barcode.Value)
    Else
      PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1328), m_date_from)
      PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1329), m_date_to)

      If PaymentOrder.IsGenerateDocumentEnabled(True) Then
        PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1553), m_bank_accounts)
      End If

      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), m_account)
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_track_data)
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), m_holder)
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4802), m_holder_is_vip)
    End If

    PrintData.FilterHeaderWidth(2) = 1500
    PrintData.FilterValueWidth(2) = 4000

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()
    Dim _external_code As String

    _external_code = Me.ef_barcode.Value
    Barcode.TryParse(_external_code, m_barcode)

    m_date_from = ""
    m_date_to = ""

    'date from
    If Me.ds_from_to.FromDateSelected Then
      m_date_from = GUI_FormatDate(Me.ds_from_to.FromDate.AddHours(ds_from_to.ClosingTime), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    'date to
    If Me.ds_from_to.ToDateSelected Then
      m_date_to = GUI_FormatDate(Me.ds_from_to.ToDate.AddHours(ds_from_to.ClosingTime), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    'client account
    m_account = uc_account_filter.Account
    m_track_data = uc_account_filter.TrackData
    m_holder = uc_account_filter.Holder
    m_holder_is_vip = uc_account_filter.HolderIsVip

    'bank accounts
    m_bank_accounts = GetBankListSelected(GRID_BANK_ACCOUNT_COLUMN_DESCRIPTION)
  End Sub 'GUI_ReportUpdateFilters

#End Region

#Region "Public Functions"

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region

#Region "Private Functions"
  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()

    Dim _is_generate_document_enabled As Boolean

    With Me.Grid
      Call .Init(GRID_COLUMNS_COUNT, GRID_HEADERS_COUNT)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      .Counter(COUNTER_PAYMENT_ORDER_OK).Visible = False
      .Counter(COUNTER_PAYMENT_ORDER_OK).BackColor = GetColor(COLOR_DEVICE_OK_BACK)

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' BANK PAYMENT ORDER ID
      .Column(GRID_COLUMN_PAYMENT_ORDER_ID).Header(0).Text = " "
      .Column(GRID_COLUMN_PAYMENT_ORDER_ID).Header(1).Text = " "
      .Column(GRID_COLUMN_PAYMENT_ORDER_ID).Width = GRID_WIDTH_PAYMENT_ORDER_ID
      .Column(GRID_COLUMN_PAYMENT_ORDER_ID).Mapping = SQL_COLUMN_PAYMENT_ORDER_ID

      ' AWARD DATE
      .Column(GRID_COLUMN_AWARD_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1578)
      .Column(GRID_COLUMN_AWARD_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1574)
      .Column(GRID_COLUMN_AWARD_DATE).Width = GRID_WIDTH_AWARD_DATE
      .Column(GRID_COLUMN_AWARD_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '13-MAR-2015 FOS
      _is_generate_document_enabled = PaymentOrder.IsGenerateDocumentEnabled(True)

      ' BANK ACCOUNT DESCRIPTION
      .Column(GRID_COLUMN_BANK_DESCRIPTION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1609)
      .Column(GRID_COLUMN_BANK_DESCRIPTION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1577)
      .Column(GRID_COLUMN_BANK_DESCRIPTION).Width = IIf(_is_generate_document_enabled, GRID_WIDTH_BANK_DESCRIPTION, 0)
      .Column(GRID_COLUMN_BANK_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' BANK NAME
      .Column(GRID_COLUMN_BANK_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1609)
      .Column(GRID_COLUMN_BANK_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1554)
      .Column(GRID_COLUMN_BANK_NAME).Width = IIf(_is_generate_document_enabled, GRID_WIDTH_BANK_NAME, 0)
      .Column(GRID_COLUMN_BANK_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' BANK ACCOUNT NUMBER
      .Column(GRID_COLUMN_BANK_ACCOUNT_NUMBER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1609)
      .Column(GRID_COLUMN_BANK_ACCOUNT_NUMBER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1558)
      .Column(GRID_COLUMN_BANK_ACCOUNT_NUMBER).Width = IIf(_is_generate_document_enabled, GRID_WIDTH_BANK_ACCOUNT_NUMBER, 0)
      .Column(GRID_COLUMN_BANK_ACCOUNT_NUMBER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' CHECK PAYMENT
      .Column(GRID_COLUMN_CHECK_PAYMENT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1578)
      .Column(GRID_COLUMN_CHECK_PAYMENT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1569)
      .Column(GRID_COLUMN_CHECK_PAYMENT).Width = GRID_WIDTH_CHECKS_PAYMENT
      .Column(GRID_COLUMN_CHECK_PAYMENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' CASH PAYMENT
      .Column(GRID_COLUMN_CASH_PAYMENT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1578)
      .Column(GRID_COLUMN_CASH_PAYMENT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1570)
      .Column(GRID_COLUMN_CASH_PAYMENT).Width = GRID_WIDTH_CASH_PAYMENT
      .Column(GRID_COLUMN_CASH_PAYMENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' FULL PAYMENT
      .Column(GRID_COLUMN_FULL_PAYMENT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1578)
      .Column(GRID_COLUMN_FULL_PAYMENT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1573)
      .Column(GRID_COLUMN_FULL_PAYMENT).Width = GRID_WIDTH_FULL_PAYMENT
      .Column(GRID_COLUMN_FULL_PAYMENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' ACCOUNT NUMBER
      .Column(GRID_COLUMN_ACCOUNT_NUMBER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1564)
      .Column(GRID_COLUMN_ACCOUNT_NUMBER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1579)
      .Column(GRID_COLUMN_ACCOUNT_NUMBER).Width = GRID_WIDTH_ACCOUNT_NUMBER
      .Column(GRID_COLUMN_ACCOUNT_NUMBER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' CLIENT NAME
      .Column(GRID_COLUMN_CLIENT_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1564)
      .Column(GRID_COLUMN_CLIENT_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1567)
      .Column(GRID_COLUMN_CLIENT_NAME).Width = GRID_WIDTH_CLIENT_NAME
      .Column(GRID_COLUMN_CLIENT_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Define all Grid Account Alias columns
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheetAccountAlias()
    With Me.dg_bank_account
      Call .Init(GRID_BANK_ACCOUNT_COLUMNS, GRID_BANK_ACCOUNT_HEADER_ROWS, True)
      .Counter(0).Visible = False
      .Sortable = False

      ' Hidden colums
      .Column(GRID_BANK_ACCOUNT_ID).Header(1).Text = ""
      .Column(GRID_BANK_ACCOUNT_ID).WidthFixed = 0

      ' GRID_COL_CHECKBOX
      .Column(GRID_BANK_ACCOUNT_COLUMN_CHECKED).Header(1).Text = ""
      .Column(GRID_BANK_ACCOUNT_COLUMN_CHECKED).WidthFixed = GRID_BANK_ACCOUNT_WIDTH_COLUMN_CHECKED
      .Column(GRID_BANK_ACCOUNT_COLUMN_CHECKED).Fixed = True
      .Column(GRID_BANK_ACCOUNT_COLUMN_CHECKED).Editable = True
      .Column(GRID_BANK_ACCOUNT_COLUMN_CHECKED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX

      ' GRID_COL_DESCRIPTION
      .Column(GRID_BANK_ACCOUNT_COLUMN_DESCRIPTION).Header(1).Text = ""
      .Column(GRID_BANK_ACCOUNT_COLUMN_DESCRIPTION).WidthFixed = GRID_BANK_ACCOUNT_WIDTH_COLUMN_DESCRIPTION
      .Column(GRID_BANK_ACCOUNT_COLUMN_DESCRIPTION).Fixed = True
      .Column(GRID_BANK_ACCOUNT_COLUMN_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With

  End Sub 'GUI_StyleSheetType

  ' PURPOSE: Sets the defaul date values to the daily_session_selector control
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultDate()

    Dim _today_opening As Date

    _today_opening = WSI.Common.Misc.TodayOpening()

    ds_from_to.FromDate = _today_opening
    ds_from_to.FromDateSelected = True
    ds_from_to.ToDate = _today_opening.AddDays(1)
    ds_from_to.ToDateSelected = True
    ds_from_to.ClosingTime = _today_opening.Hour

  End Sub ' SetDefaultDate

  ' PURPOSE: Get the sql 'where' statement
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - an string with the 'where' statement
  Private Function GetSqlWhere() As String

    Dim _sb_where As StringBuilder
    Dim _date_condition As String
    Dim _order_fields_condition As String
    Dim _bank_id_list As String

    _sb_where = New StringBuilder()

    '13-MAR-2015 FOS
    If Not m_barcode Is Nothing Then
      If _sb_where.ToString().Length > 0 Then
        _sb_where.Append(" AND ")
      End If
      _sb_where.AppendLine(" APO_OPERATION_ID = " & m_barcode.UniqueId.ToString() & " ")

    Else

      _order_fields_condition = uc_account_filter.GetFilterSQL()

      _date_condition = ds_from_to.GetSqlFilterCondition("APO_DATETIME")

      _bank_id_list = GetBankListSelected(GRID_BANK_ACCOUNT_ID)

      If _order_fields_condition.Length > 0 Then
        _sb_where.AppendLine(_order_fields_condition)
      End If

      If _date_condition.Length > 0 Then
        If _sb_where.ToString().Length > 0 Then
          _sb_where.Append(" AND ")
        End If
        _sb_where.AppendLine(_date_condition)
      End If

      If _bank_id_list.Length > 0 Then
        If _sb_where.ToString().Length > 0 Then
          _sb_where.Append(" AND ")
        End If
        _sb_where.AppendLine(" BA_ACCOUNT_ID IN (" & _bank_id_list & ") ")
      End If

    End If

    If _sb_where.ToString().Length > 0 Then
      _sb_where.Insert(0, "WHERE ")
    End If

    Return _sb_where.ToString()

  End Function ' GetSqlWhere

  ' PURPOSE: Fill the bank account filter
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetGridBankAccountValues()

    Dim _sb As StringBuilder
    Dim _table As DataTable

    _table = New DataTable()
    _sb = New StringBuilder()

    Using _db_trx As DB_TRX = New DB_TRX()
      _sb.AppendLine("  SELECT     BA_ACCOUNT_ID   ")
      _sb.AppendLine("           , BA_DESCRIPTION  ")
      _sb.AppendLine("    FROM     BANK_ACCOUNTS   ")
      _sb.AppendLine("ORDER BY     BA_DESCRIPTION  ")

      Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

        Using _adapter As sqldataAdapter = New SqlDataAdapter(_cmd)
          _adapter.Fill(_table)

          Call GUI_SetupRowsBankAliasGrid(_table)
        End Using
      End Using
    End Using

  End Sub ' SetGridBankAccountValues

  ' PURPOSE: Fill the grid with the bank account alias
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_SetupRowsBankAliasGrid(ByVal Table As DataTable)

    Dim _actual_row As Int32

    _actual_row = 0

    For Each r As DataRow In Table.Rows
      dg_bank_account.AddRow()
      Me.dg_bank_account.Cell(_actual_row, 0).Value = r(0)
      Me.dg_bank_account.Cell(_actual_row, 2).Value = r(1)

      _actual_row += 1
    Next

  End Sub ' GUI_SetupRowBankAliasGrid

  ' PURPOSE: get the bank accounts selected
  '
  '  PARAMS:
  '     - INPUT:
  '           - ColumnNeeded : It could be the Id column or alias column
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - it could be a list of Ids or a list of alias
  Private Function GetBankListSelected(ByVal ColumnNeeded As Int16) As String

    Dim _idx_row As Integer
    Dim _bank_list As String

    _bank_list = ""

    For _idx_row = 0 To Me.dg_bank_account.NumRows - 1
      If dg_bank_account.Cell(_idx_row, GRID_BANK_ACCOUNT_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        If _bank_list.Length = 0 Then
          _bank_list = dg_bank_account.Cell(_idx_row, ColumnNeeded).Value
        Else
          _bank_list = _bank_list & ", " & dg_bank_account.Cell(_idx_row, ColumnNeeded).Value
        End If
      End If

    Next

    Return _bank_list

  End Function ' GetBankListSelected

  ' PURPOSE: Set default values to the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub SetDefaultValues()

    Dim _num_row As Integer

    With Me.dg_bank_account

      For _num_row = 0 To .NumRows - 1
        .Cell(_num_row, GRID_BANK_ACCOUNT_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
      Next

    End With

    Call SetDefaultDate()
    uc_account_filter.Clear()
    ef_barcode.Value = String.Empty

    ef_barcode_EntryFieldValueChanged()

  End Sub ' SetDefaultValues

  ' PURPOSE: Manages barcode received events
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub BarcodeEvent(ByVal e As KbdMsgEvent, ByVal Data As Object)

    Try
      If Not Data Is Nothing Then
        Dim _data_read As WSI.Common.Barcode = DirectCast(Data, WSI.Common.Barcode)

        Select Case CType(_data_read.Type, WSI.Common.BarcodeType)

          Case BarcodeType.PayOrder
            Me.ef_barcode.TextValue = _data_read.ExternalCode
            Call GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)
            ef_barcode_EntryFieldValueChanged()

          Case Else

        End Select
      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub frm_payment_order_sel_VisibleChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.VisibleChanged
    KeyboardMessageFilter.RegisterHandler(Me, BarcodeType.PayOrder, m_barcode_handler)
  End Sub

#End Region

  Private Sub ef_barcode_EntryFieldValueChanged() Handles ef_barcode.EntryFieldValueChanged
    If ef_barcode.Value.Length > 0 Then
      ds_from_to.Enabled = False
      uc_account_filter.Enabled = False
      gb_bank_accounts.Enabled = False
    Else
      ds_from_to.Enabled = True
      uc_account_filter.Enabled = True
      gb_bank_accounts.Enabled = True
    End If
  End Sub
End Class