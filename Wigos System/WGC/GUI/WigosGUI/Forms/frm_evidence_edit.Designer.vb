<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_evidence_edit
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.tab_evidence_viewer = New System.Windows.Forms.TabControl()
    Me.TabPage1 = New System.Windows.Forms.TabPage()
    Me.lnkEvidenceFile = New System.Windows.Forms.LinkLabel()
    Me.id_holder_data = New GUI_Controls.uc_identity_data()
    Me.id_company = New GUI_Controls.uc_identity_data()
    Me.id_representative = New GUI_Controls.uc_identity_data()
    Me.gb_operation = New System.Windows.Forms.GroupBox()
    Me.ef_total_payment = New GUI_Controls.uc_entry_field()
    Me.wb_evidence = New System.Windows.Forms.WebBrowser()
    Me.ef_tax1_amount = New GUI_Controls.uc_entry_field()
    Me.ef_tax2_amount = New GUI_Controls.uc_entry_field()
    Me.ef_tax3_amount = New GUI_Controls.uc_entry_field()
    Me.ef_prize_amount = New GUI_Controls.uc_entry_field()
    Me.dt_operation_date = New GUI_Controls.uc_date_picker()
    Me.panel_data.SuspendLayout()
    Me.tab_evidence_viewer.SuspendLayout()
    Me.TabPage1.SuspendLayout()
    Me.gb_operation.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.panel_data.Controls.Add(Me.tab_evidence_viewer)
    Me.panel_data.Dock = System.Windows.Forms.DockStyle.Fill
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(930, 590)
    '
    'tab_evidence_viewer
    '
    Me.tab_evidence_viewer.Controls.Add(Me.TabPage1)
    Me.tab_evidence_viewer.Dock = System.Windows.Forms.DockStyle.Fill
    Me.tab_evidence_viewer.Location = New System.Drawing.Point(0, 0)
    Me.tab_evidence_viewer.Name = "tab_evidence_viewer"
    Me.tab_evidence_viewer.SelectedIndex = 0
    Me.tab_evidence_viewer.Size = New System.Drawing.Size(930, 590)
    Me.tab_evidence_viewer.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight
    Me.tab_evidence_viewer.TabIndex = 0
    '
    'TabPage1
    '
    Me.TabPage1.Controls.Add(Me.lnkEvidenceFile)
    Me.TabPage1.Controls.Add(Me.id_holder_data)
    Me.TabPage1.Controls.Add(Me.id_company)
    Me.TabPage1.Controls.Add(Me.id_representative)
    Me.TabPage1.Controls.Add(Me.gb_operation)
    Me.TabPage1.Location = New System.Drawing.Point(4, 22)
    Me.TabPage1.Name = "TabPage1"
    Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage1.Size = New System.Drawing.Size(922, 564)
    Me.TabPage1.TabIndex = 0
    Me.TabPage1.Text = "TabPage1"
    Me.TabPage1.UseVisualStyleBackColor = True
    '
    'lnkEvidenceFile
    '
    Me.lnkEvidenceFile.AutoSize = True
    Me.lnkEvidenceFile.Location = New System.Drawing.Point(823, 540)
    Me.lnkEvidenceFile.Name = "lnkEvidenceFile"
    Me.lnkEvidenceFile.Size = New System.Drawing.Size(84, 13)
    Me.lnkEvidenceFile.TabIndex = 1
    Me.lnkEvidenceFile.TabStop = True
    Me.lnkEvidenceFile.Text = "xEvidenceFile"
    '
    'id_holder_data
    '
    Me.id_holder_data.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.id_holder_data.CompanyID1 = ""
    Me.id_holder_data.CompanyID2 = ""
    Me.id_holder_data.EditableMinutes = 0
    Me.id_holder_data.EvidenceEnabled = False
    Me.id_holder_data.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.id_holder_data.GeneratedWitholdingDocument = 0
    Me.id_holder_data.IdentityID1 = ""
    Me.id_holder_data.IdentityID2 = ""
    Me.id_holder_data.IdentityLastName1 = ""
    Me.id_holder_data.IdentityLastName2 = ""
    Me.id_holder_data.IdentityName = ""
    Me.id_holder_data.Location = New System.Drawing.Point(19, 14)
    Me.id_holder_data.Margin = New System.Windows.Forms.Padding(0)
    Me.id_holder_data.Name = "id_holder_data"
    Me.id_holder_data.PrizeGratherThan = New Decimal(New Integer() {0, 0, 0, 0})
    Me.id_holder_data.RepresentationMode = GUI_Controls.uc_identity_data.Mode.INDETITY_DATA
    Me.id_holder_data.RepresentativeID1 = ""
    Me.id_holder_data.RepresentativeID2 = ""
    Me.id_holder_data.RepresentativeName = ""
    Me.id_holder_data.RepresentativeSignature = Nothing
    Me.id_holder_data.Size = New System.Drawing.Size(722, 163)
    Me.id_holder_data.TabIndex = 0
    Me.id_holder_data.TextGroupBox = "xIdentity"
    '
    'id_company
    '
    Me.id_company.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.id_company.CompanyID1 = ""
    Me.id_company.CompanyID2 = ""
    Me.id_company.EditableMinutes = 0
    Me.id_company.EvidenceEnabled = False
    Me.id_company.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.id_company.GeneratedWitholdingDocument = 0
    Me.id_company.IdentityID1 = ""
    Me.id_company.IdentityID2 = ""
    Me.id_company.IdentityLastName1 = ""
    Me.id_company.IdentityLastName2 = ""
    Me.id_company.IdentityName = ""
    Me.id_company.Location = New System.Drawing.Point(19, 334)
    Me.id_company.Margin = New System.Windows.Forms.Padding(0)
    Me.id_company.Name = "id_company"
    Me.id_company.PrizeGratherThan = New Decimal(New Integer() {0, 0, 0, 0})
    Me.id_company.RepresentationMode = GUI_Controls.uc_identity_data.Mode.COMPANY
    Me.id_company.RepresentativeID1 = ""
    Me.id_company.RepresentativeID2 = ""
    Me.id_company.RepresentativeName = ""
    Me.id_company.RepresentativeSignature = Nothing
    Me.id_company.Size = New System.Drawing.Size(722, 104)
    Me.id_company.TabIndex = 0
    Me.id_company.TextGroupBox = "xCompany"
    '
    'id_representative
    '
    Me.id_representative.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.id_representative.CompanyID1 = ""
    Me.id_representative.CompanyID2 = ""
    Me.id_representative.EditableMinutes = 0
    Me.id_representative.EvidenceEnabled = False
    Me.id_representative.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.id_representative.GeneratedWitholdingDocument = 0
    Me.id_representative.IdentityID1 = ""
    Me.id_representative.IdentityID2 = ""
    Me.id_representative.IdentityLastName1 = ""
    Me.id_representative.IdentityLastName2 = ""
    Me.id_representative.IdentityName = ""
    Me.id_representative.Location = New System.Drawing.Point(19, 444)
    Me.id_representative.Margin = New System.Windows.Forms.Padding(0)
    Me.id_representative.Name = "id_representative"
    Me.id_representative.PrizeGratherThan = New Decimal(New Integer() {0, 0, 0, 0})
    Me.id_representative.RepresentationMode = GUI_Controls.uc_identity_data.Mode.REPRESENTATIVE
    Me.id_representative.RepresentativeID1 = ""
    Me.id_representative.RepresentativeID2 = ""
    Me.id_representative.RepresentativeName = ""
    Me.id_representative.RepresentativeSignature = Nothing
    Me.id_representative.Size = New System.Drawing.Size(722, 109)
    Me.id_representative.TabIndex = 0
    Me.id_representative.TextGroupBox = "xRepresentative"
    '
    'gb_operation
    '
    Me.gb_operation.Controls.Add(Me.ef_tax3_amount)
    Me.gb_operation.Controls.Add(Me.ef_total_payment)
    Me.gb_operation.Controls.Add(Me.wb_evidence)
    Me.gb_operation.Controls.Add(Me.ef_tax2_amount)
    Me.gb_operation.Controls.Add(Me.ef_tax1_amount)
    Me.gb_operation.Controls.Add(Me.ef_prize_amount)
    Me.gb_operation.Controls.Add(Me.dt_operation_date)
    Me.gb_operation.Location = New System.Drawing.Point(19, 180)
    Me.gb_operation.Name = "gb_operation"
    Me.gb_operation.Size = New System.Drawing.Size(722, 151)
    Me.gb_operation.TabIndex = 0
    Me.gb_operation.TabStop = False
    Me.gb_operation.Text = "xOperation"
    '
    'ef_tax3_amount
    '
    Me.ef_tax3_amount.DoubleValue = 0.0R
    Me.ef_tax3_amount.IntegerValue = 0
    Me.ef_tax3_amount.IsReadOnly = True
    Me.ef_tax3_amount.Location = New System.Drawing.Point(237, 94)
    Me.ef_tax3_amount.Name = "ef_tax3_amount"
    Me.ef_tax3_amount.PlaceHolder = Nothing
    Me.ef_tax3_amount.Size = New System.Drawing.Size(216, 25)
    Me.ef_tax3_amount.SufixText = "Sufix Text"
    Me.ef_tax3_amount.SufixTextVisible = True
    Me.ef_tax3_amount.TabIndex = 1
    Me.ef_tax3_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_tax3_amount.TextValue = ""
    Me.ef_tax3_amount.TextWidth = 130
    Me.ef_tax3_amount.Value = ""
    Me.ef_tax3_amount.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_total_payment
    '
    Me.ef_total_payment.DoubleValue = 0.0R
    Me.ef_total_payment.IntegerValue = 0
    Me.ef_total_payment.IsReadOnly = True
    Me.ef_total_payment.Location = New System.Drawing.Point(17, 119)
    Me.ef_total_payment.Name = "ef_total_payment"
    Me.ef_total_payment.PlaceHolder = Nothing
    Me.ef_total_payment.Size = New System.Drawing.Size(436, 24)
    Me.ef_total_payment.SufixText = "Sufix Text"
    Me.ef_total_payment.SufixTextVisible = True
    Me.ef_total_payment.TabIndex = 0
    Me.ef_total_payment.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_total_payment.TextValue = ""
    Me.ef_total_payment.TextWidth = 130
    Me.ef_total_payment.Value = ""
    Me.ef_total_payment.ValueForeColor = System.Drawing.Color.Blue
    '
    'wb_evidence
    '
    Me.wb_evidence.Location = New System.Drawing.Point(696, 125)
    Me.wb_evidence.MinimumSize = New System.Drawing.Size(20, 20)
    Me.wb_evidence.Name = "wb_evidence"
    Me.wb_evidence.Size = New System.Drawing.Size(20, 20)
    Me.wb_evidence.TabIndex = 0
    '
    'ef_tax2_amount
    '
    Me.ef_tax2_amount.DoubleValue = 0.0R
    Me.ef_tax2_amount.IntegerValue = 0
    Me.ef_tax2_amount.IsReadOnly = True
    Me.ef_tax2_amount.Location = New System.Drawing.Point(17, 94)
    Me.ef_tax2_amount.Name = "ef_tax2_amount"
    Me.ef_tax2_amount.PlaceHolder = Nothing
    Me.ef_tax2_amount.Size = New System.Drawing.Size(216, 24)
    Me.ef_tax2_amount.SufixText = "Sufix Text"
    Me.ef_tax2_amount.SufixTextVisible = True
    Me.ef_tax2_amount.TabIndex = 0
    Me.ef_tax2_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_tax2_amount.TextValue = ""
    Me.ef_tax2_amount.TextWidth = 130
    Me.ef_tax2_amount.Value = ""
    Me.ef_tax2_amount.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_tax1_amount
    '
    Me.ef_tax1_amount.DoubleValue = 0.0R
    Me.ef_tax1_amount.IntegerValue = 0
    Me.ef_tax1_amount.IsReadOnly = True
    Me.ef_tax1_amount.Location = New System.Drawing.Point(17, 69)
    Me.ef_tax1_amount.Name = "ef_tax1_amount"
    Me.ef_tax1_amount.PlaceHolder = Nothing
    Me.ef_tax1_amount.Size = New System.Drawing.Size(436, 24)
    Me.ef_tax1_amount.SufixText = "Sufix Text"
    Me.ef_tax1_amount.SufixTextVisible = True
    Me.ef_tax1_amount.TabIndex = 0
    Me.ef_tax1_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_tax1_amount.TextValue = ""
    Me.ef_tax1_amount.TextWidth = 130
    Me.ef_tax1_amount.Value = ""
    Me.ef_tax1_amount.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_prize_amount
    '
    Me.ef_prize_amount.DoubleValue = 0.0R
    Me.ef_prize_amount.IntegerValue = 0
    Me.ef_prize_amount.IsReadOnly = True
    Me.ef_prize_amount.Location = New System.Drawing.Point(17, 44)
    Me.ef_prize_amount.Name = "ef_prize_amount"
    Me.ef_prize_amount.PlaceHolder = Nothing
    Me.ef_prize_amount.Size = New System.Drawing.Size(436, 24)
    Me.ef_prize_amount.SufixText = "Sufix Text"
    Me.ef_prize_amount.SufixTextVisible = True
    Me.ef_prize_amount.TabIndex = 0
    Me.ef_prize_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_prize_amount.TextValue = ""
    Me.ef_prize_amount.TextWidth = 130
    Me.ef_prize_amount.Value = ""
    Me.ef_prize_amount.ValueForeColor = System.Drawing.Color.Blue
    '
    'dt_operation_date
    '
    Me.dt_operation_date.Checked = True
    Me.dt_operation_date.IsReadOnly = True
    Me.dt_operation_date.Location = New System.Drawing.Point(17, 20)
    Me.dt_operation_date.Name = "dt_operation_date"
    Me.dt_operation_date.ShowCheckBox = False
    Me.dt_operation_date.ShowUpDown = False
    Me.dt_operation_date.Size = New System.Drawing.Size(328, 24)
    Me.dt_operation_date.SufixText = "Sufix Text"
    Me.dt_operation_date.SufixTextVisible = True
    Me.dt_operation_date.TabIndex = 0
    Me.dt_operation_date.TextWidth = 130
    Me.dt_operation_date.Value = New Date(2012, 2, 9, 16, 37, 32, 958)
    '
    'frm_evidence_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.AutoScroll = True
    Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.ClientSize = New System.Drawing.Size(1028, 598)
    Me.MaximizeBox = True
    Me.Name = "frm_evidence_edit"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = ""
    Me.panel_data.ResumeLayout(False)
    Me.tab_evidence_viewer.ResumeLayout(False)
    Me.TabPage1.ResumeLayout(False)
    Me.TabPage1.PerformLayout()
    Me.gb_operation.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents tab_evidence_viewer As System.Windows.Forms.TabControl
  Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
  Friend WithEvents id_representative As GUI_Controls.uc_identity_data
  Friend WithEvents gb_operation As System.Windows.Forms.GroupBox
  Friend WithEvents ef_total_payment As GUI_Controls.uc_entry_field
  Friend WithEvents wb_evidence As System.Windows.Forms.WebBrowser
  Friend WithEvents ef_tax1_amount As GUI_Controls.uc_entry_field
  Friend WithEvents ef_tax2_amount As GUI_Controls.uc_entry_field
  Friend WithEvents ef_tax3_amount As GUI_Controls.uc_entry_field
  Friend WithEvents ef_prize_amount As GUI_Controls.uc_entry_field
  Friend WithEvents dt_operation_date As GUI_Controls.uc_date_picker
  Friend WithEvents id_company As GUI_Controls.uc_identity_data
  Friend WithEvents id_holder_data As GUI_Controls.uc_identity_data
  Friend WithEvents lnkEvidenceFile As System.Windows.Forms.LinkLabel
End Class
