'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_promotions_delivered
' DESCRIPTION:   Report to show the promotions delivered
' AUTHOR:        Daniel Dom�nguez
' CREATION DATE: 24-MAY-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 28-AUG-2012  DDM    Initial version
' 21-SEP-2012  XIT    Added Categories
' 27-SEP-2012  XCD    Changed query combo users
' 07-NOV-2012  DDM    Modified the query:
'                       - Added date  in the ACCOUNT_OPERATIONS.
'                       - Index to the temporal table.
'                       - No included the Cover cupon and Prize Cupon 
'                         in the filter from all promotions.
' 12-FEB-2013  JMM    uc_account_sel.ValidateFormat added on FilterCheck
' 24-APR-2013  SMN    Hide Player Tracking data if exists an External Loyalty
' 11-JUN-2013  RBG    Fixed Bug #834: Force format in excel columns
' 28-AUG-2013  LEM    Added option to group by account and date
' 29-AUG-2013  DHA    Fixed Bug WIG-168: Added currency exchange promotions
' 05-SEP-2013  LEM    Added AccountMassiveSearch option
' 12-SEP-2013  DHA    Added Cash Desk Draw promotions
' 26-SEP-2013  CCG    Added Multitype massive search
' 16-DEC-2013  RMS    In TITO Mode virtual account id should be hidden
' 13-FEB-2014  JBC    Fixed bug WIGOSTITO-1065: Hidden virtual accounts results
' 27-MAR-2014  LEM    Added m_only_user_accounts to indicate if virtual accounts are hidden    
' 06-MAY-2014  JBP    Added Vip client filter at reports.
' 21-AGO-2015  FJC    Product BackLog Item: 3702
' 13-JAN-2015  JML    Product Backlog Item 6559: Cashier draw (RE in kind)
' 24-FEB-2016  RAB    Product Backlog Item 8257:Multiple buckets: Canje de buckets NR
' 13-FEB-2017  DPC    Bug 24190:Error en versi�n 03.004.0064 -- Reporte de Promociones Entregadas, excel informaci�n incompleta
' 11-MAY-2017  EOR    Fixed Bug 27344: Send error when search in the delivered promotions report
' 29-MAY-2017  EOR    Fixed Bug 27344: Send error when search in the delivered promotions report
' 31-MAY-2017  EOR    Fixed Bug 27344: Send error when search in the delivered promotions report
' 28-SEP-2017  RLO    Product Backlog Item: WIGOS-3806
' 14-NOV-2017  RAB    Bug 30787:WIGOS-6520 [Ticket #10313] Discrepancia resultado Reporte de promociones entregadas
'--------------------------------------------------------------------
Imports GUI_Controls
Imports System.Data.SqlClient
Imports GUI_CommonOperations
Imports WSI.Common
Imports GUI_CommonMisc

Public Class frm_promotions_delivered_report
  Inherits GUI_Controls.frm_base_sel

#Region " Constants "

  ' DB Columns
  Private Const SQL_COLUMN_LEVEL As Integer = 0
  Private Const SQL_COLUMN_ACCOUNT_NAME As Integer = 1
  Private Const SQL_COLUMN_ACCOUNT_ID As Integer = 2
  Private Const SQL_COLUMN_PROMOTION_DATE As Integer = 3
  Private Const SQL_COLUMN_PROMOTION_NAME As Integer = 4
  Private Const SQL_COLUMN_INIT_BALANCE As Integer = 5
  Private Const SQL_COLUMN_PLAYED As Integer = 6
  Private Const SQL_COLUMN_BALANCE As Integer = 7
  Private Const SQL_COLUMN_STATUS As Integer = 8
  Private Const SQL_COLUMN_CREDIT_TYPE As Integer = 9
  Private Const SQL_COLUMN_CASH_IN As Integer = 10
  Private Const SQL_COLUMN_PROMO_ID As Integer = 11
  Private Const SQL_COLUMN_ACP_POINT As Integer = 12
  Private Const SQL_COLUMN_WON As Integer = 13
  Private Const SQL_COLUMN_DETAILS As Integer = 14
  Private Const SQL_COLUMN_PROMOTION_ACTIVATION As Integer = 15
  Private Const SQL_COLUMN_UNIQUE_ID As Integer = 16
  Private Const SQL_COLUMN_UD_CASHIER_SESION_ID As Integer = 17
  Private Const SQL_COLUMN_UD_CASHIER_SESSION_NAME As Integer = 18
  Private Const SQL_COLUMN_UD_USER_ID As Integer = 19
  Private Const SQL_COLUMN_UD_USER As Integer = 20
  Private Const SQL_COLUMN_UD_NAME As Integer = 21
  Private Const SQL_COLUMN_PROMO_CATEGORY As Integer = 22

  Private Const SQL_COLUMN_ACCOUNT_TYPE As Integer = 23
  Private Const SQL_COLUMN_NUM_ROWS_GROUP As Integer = 24

  Private Const SQL_INIT_COLUMNS_HOLDER_DATA As Integer = 24

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private GRID_COLUMN_PROMOTION_NAME As Integer = 1
  Private GRID_COLUMN_PROMOTION_CATEGORY As Integer = 2
  Private GRID_COLUMN_CASHIER_SESSION_NAME As Integer = 3
  Private GRID_COLUMN_USER As Integer = 4
  Private GRID_COLUMN_NAME As Integer = 5
  Private GRID_COLUMN_PROMOTION_ACTIVATION As Integer = 6
  Private GRID_COLUMN_PROMO_POINTS As Integer = 7
  Private GRID_COLUMN_PROMO_REDIM As Integer = 8
  Private GRID_COLUMN_PROMO_NON_REDIM As Integer = 9
  Private GRID_COLUMN_PROMO_STATUS As Integer = 10
  Private GRID_COLUMN_PLAYED As Integer = 11
  Private GRID_COLUMN_WON As Integer = 12
  Private GRID_COLUMN_BALANCE As Integer = 13
  Private GRID_COLUMN_DETAILS As Integer = 14
  Private GRID_COLUMN_POINTS As Integer = 15
  Private GRID_COLUMN_CASH_IN As Integer = 16
  Private GRID_COLUMN_UNIQUE As Integer = 17
  Private GRID_COLUMN_CREDIT_TYPE As Integer = 18
  Private GRID_COLUMN_LEVEL As Integer = 19
  Private GRID_COLUMN_ACCOUNT_ID As Integer = 20
  Private GRID_COLUMN_ACCOUNT_NAME As Integer = 21


  Private Const GRID_COLUMNS As Integer = 22
  Private Const GRID_HEADER_ROWS As Integer = 2

  Private Const LINE_TYPE_SUBTOTAL_OR_TOTAL As Integer = 0
  Private Const INITIAL_VALUE_SUBTOTAL As Integer = -2

  ' Text format columns
  Private Const EXCEL_COLUMN_TELEPHONE_NUMBER_1 As Integer = 19
  Private Const EXCEL_COLUMN_TELEPHONE_NUMBER_2 As Integer = 20
  Private Const EXCEL_COLUMN_POSTAL_CODE As Integer = 25

  Private Const FORM_DB_MIN_VERSION As Short = 160

  'Width
  Private Const GRID_WIDTH_EXCEL_POINTS As Decimal = 9
  Private Const GRID_WIDTH_EXCEL_CASH_IN As Decimal = 9

#End Region

#Region " MEMBERS "
  Private m_level_names As Dictionary(Of Integer, String)
  ' filter
  Private m_init_date_from As String
  Private m_init_date_to As String
  Private m_order As String
  Private m_status As String
  Private m_promotion_type As String
  Private m_promo_name As String
  Private m_promo_type_credit As String
  Private m_account As String
  Private m_track_data As String
  Private m_holder As String
  Private m_holder_is_vip As String
  Private m_users As String
  Private m_details_and_account_data As String
  Private m_category As String
  Private m_only_user_accounts As Boolean

  ' Subtotal 1
  Private m_subtotal1_cash_in As Decimal
  Private m_subtotal1_promo_points As Decimal
  Private m_subtotal1_redim As Decimal
  Private m_subtotal1_noredim As Decimal
  Private m_subtotal1_played As Decimal
  Private m_subtotal1_balance As Decimal
  Private m_subtotal1_won As Decimal
  Private m_subtotal1_points As Decimal

  ' Subtotal 2
  Private m_subtotal2_cash_in As Decimal
  Private m_subtotal2_promo_points As Decimal
  Private m_subtotal2_redim As Decimal
  Private m_subtotal2_noredim As Decimal
  Private m_subtotal2_played As Decimal
  Private m_subtotal2_balance As Decimal
  Private m_subtotal2_won As Decimal
  Private m_subtotal2_points As Decimal

  ' Total 
  Private m_total_cash_in As Decimal
  Private m_total_promo_points As Decimal
  Private m_total_redim As Decimal
  Private m_total_noredim As Decimal
  Private m_total_played As Decimal
  Private m_total_balance As Decimal
  Private m_total_won As Decimal
  Private m_total_points As Decimal

  ' save last value the subtotal.
  Private m_last_subtotal1 As Object
  ' save last value the subtotal.
  Private m_last_subtotal2 As Object

  Private m_sql_column_subtotal2 As Integer
  Private m_sql_column_subtotal1 As Integer

  Private m_num_rows_detail As Integer

  Private m_num_rows_subtotal1 As Integer
  Private m_num_rows_subtotal2 As Integer

  Private m_last_db_row_detail As CLASS_DB_ROW

  Private m_holder_data As Boolean

#End Region

#Region " OVERRIDES "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_PROMOTIONS_DELIVERED_REPORT

    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    If WSI.Common.TITO.Utils.IsTitoMode() Then
      uc_account_sel.InitExtendedQuery(True)
    End If

    Me.m_holder_data = True

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1271)

    ' Grid
    Me.Grid.PanelRightVisible = False

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    ' Date time filter
    Me.uc_dsl.Init(GLB_NLS_GUI_PLAYER_TRACKING.Id(1275))
    Me.uc_dsl.ClosingTimeEnabled = False

    ' Users 
    Me.gb_user.Text = GLB_NLS_GUI_INVOICING.GetString(220)
    Me.opt_one_user.Text = GLB_NLS_GUI_INVOICING.GetString(218)
    Me.opt_all_users.Text = GLB_NLS_GUI_INVOICING.GetString(219)
    Me.chk_show_all.Text = GLB_NLS_GUI_CONFIGURATION.GetString(90)

    ' XCD 27-Sep-2012 Changed query combo users
    Call SetCombo(Me.cmb_user, "SELECT GU_USER_ID, GU_USERNAME FROM GUI_USERS WHERE GU_BLOCK_REASON = " & _
                    WSI.Common.GUI_USER_BLOCK_REASON.NONE & " ORDER BY GU_USERNAME")

    Me.cmb_user.Enabled = False
    Me.chk_show_all.Enabled = False

    ' Promo type
    Me.gb_promo_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(779)
    Me.opt_promo_all.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(287) & "*"
    Me.opt_promo_manual.Text = GLB_NLS_GUI_INVOICING.GetString(330)
    Me.opt_promo_system_period.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(780)
    Me.opt_promo_system_auto.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(809)
    Me.opt_promo_per_points.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(810)
    Me.opt_promo_preassigned.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1231)

    lbl_msg_no_includes_auto.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1550)
    Call GUI_StyleSheet()

    ' Order by
    Me.gb_order.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(781)
    Me.opt_order_prom_session_user.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4403) ' Promotion and session
    Me.opt_order_session_user_prom.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4404) ' Session and user
    Me.opt_order_account_day.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4405)       ' Account and day

    ' Status
    If (WSI.Common.Misc.IsFeatureTerminalDrawEnabled() And StatusVisibleForGeneralParam()) Then
      Me.gb_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8708)
      Me.opt_status_all.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8709) ' All
      Me.opt_status_pending_player.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8710) ' Pre-Granted
      Me.opt_status_bought.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8711)       ' Bought
      Me.opt_status_pending_draw.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8712)       ' Granted / Given
    Else
      Me.gb_status.Visible = False
      Me.opt_status_all.Visible = False
      Me.opt_status_pending_player.Visible = False
      Me.opt_status_bought.Visible = False
      Me.opt_status_pending_draw.Visible = False
    End If

    'caption
    Me.gb_caption.Text = GLB_NLS_GUI_STATISTICS.GetString(449)
    Me.lbl_color_caption_subtotal1.BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_01)
    Me.lbl_color_caption_subtotal2.BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

    ' gb options
    Me.gb_options.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1277)

    ' View data to the account
    Me.chk_view_data_account.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(552)

    ' Details checkbox
    Me.chk_details.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1274)

    ' Promo filter
    Me.ef_promo_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(256)
    Me.ef_promo_name.TextVisible = True
    Me.ef_promo_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    m_last_db_row_detail = Nothing

    ' Categories filter
    Me.cmb_categories.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1336)  ' Category
    Me.cmb_categories.Add(-1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1337))
    Me.cmb_categories.Add(PromotionCategories.Categories())

    ' Added to focus datepicker
    Me.uc_dsl.Select()

    'Activate account massive search
    Me.uc_account_sel.ShowMassiveSearch = True

    ' Set filter default values
    Call SetDefaultValues()

    ' Get Levels name for the Dictionary
    m_level_names = mdl_account_for_report.GetLevelNames()

    Me.gb_promo_credit_type.Text = GLB_NLS_GUI_INVOICING.GetString(336)
    Me.chk_credit_type_points.Text = GLB_NLS_GUI_INVOICING.GetString(343)
    Me.chk_credit_type_redeem.Text = GLB_NLS_GUI_INVOICING.GetString(337)
    Me.chk_credit_type_non_redeem.Text = GLB_NLS_GUI_INVOICING.GetString(338)

  End Sub ' GUI_InitControls

  ' PURPOSE : Manage permissions.
  '
  '  PARAMS :
  '     - INPUT :
  '           - AndPerm TYPE_PERMISSIONS
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As GUI_Controls.CLASS_GUI_USER.TYPE_PERMISSIONS)

    MyBase.GUI_Permissions(AndPerm)

    ' GroupBox Permissions
    Me.chk_view_data_account.Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_SHOW_HOLDER_DATA).Read

  End Sub ' GUI_Permissions

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate > Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    End If

    ' Check fields on uc_account_sel are ok
    If Not Me.uc_account_sel.ValidateFormat Then
      Return False
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _str_sql As System.Text.StringBuilder
    'Dim _str_sql_where_user_data As String

    '_str_sql_where_user_data = GetSqlWhereUserData()
    _str_sql = New System.Text.StringBuilder()

    '_str_sql.AppendLine(" -------------------------------------------- ")
    '_str_sql.AppendLine(" -- #USER_DATA: Temporal table. Get user data ")
    '_str_sql.AppendLine(" ---------------------------------------------")
    '_str_sql.AppendLine(" SELECT   AO_OPERATION_ID                                                       AS UD_OPERATION_ID ")
    '_str_sql.AppendLine("        , CS_SESSION_ID                                                         AS UD_CASHIER_SESSION_ID ")
    '_str_sql.AppendLine(" 	     , CS_NAME                                                               AS UD_CASHIER_SESSION_NAME ")
    '_str_sql.AppendLine("        , ISNULL(AO_USER_ID, CS_USER_ID)                                        AS UD_USER_ID")
    '_str_sql.AppendLine(" 	     , ISNULL('MB - ' + CAST(AO_MB_ACCOUNT_ID AS NVARCHAR(50)), GU_USERNAME) AS UD_USER ")
    '_str_sql.AppendLine(" 	     , ISNULL(MB_HOLDER_NAME, GU_FULL_NAME)                                  AS UD_NAME ")
    '_str_sql.AppendLine("   into   #USER_DATA ")
    '_str_sql.AppendLine("   FROM   ACCOUNT_OPERATIONS ")
    '_str_sql.AppendLine("   LEFT   JOIN CASHIER_SESSIONS ON AO_CASHIER_SESSION_ID          = CS_SESSION_ID ")
    '_str_sql.AppendLine("   LEFT   JOIN GUI_USERS        ON ISNULL(AO_USER_ID, CS_USER_ID) = GU_USER_ID ")
    '_str_sql.AppendLine("   LEFT   JOIN MOBILE_BANKS     ON AO_MB_ACCOUNT_ID               = MB_ACCOUNT_ID ")
    '_str_sql.AppendLine(_str_sql_where_user_data)

    ''Create index in the temporal table
    '_str_sql.AppendLine(" CREATE NONCLUSTERED INDEX [IX_ud_operation_id] ON [dbo].[#USER_DATA] ")
    '_str_sql.AppendLine(" ( ")
    '_str_sql.AppendLine("     [UD_OPERATION_ID] ASC ")
    '_str_sql.AppendLine(" ) ")

    If Not String.IsNullOrEmpty(uc_account_sel.MassiveSearchNumbers) Then
      _str_sql.AppendLine(uc_account_sel.CreateAndInsertAccountData())
    End If

    _str_sql.AppendLine("                        ")
    _str_sql.AppendLine(" ---------------------- ")
    _str_sql.AppendLine(" -- ACCOUNT_PROMOTIONS  ")
    _str_sql.AppendLine(" -----------------------")
    _str_sql.AppendLine(" SELECT	 ACP_ACCOUNT_LEVEL       ") '0 
    _str_sql.AppendLine("        , AC_HOLDER_NAME          ") '1
    _str_sql.AppendLine("        , ACP_ACCOUNT_ID          ") '2 
    _str_sql.AppendLine("        , ACP_PROMO_DATE          ") '3
    _str_sql.AppendLine("        , ACP_PROMO_NAME          ") '4
    _str_sql.AppendLine("        , ACP_INI_BALANCE         ") '5
    _str_sql.AppendLine("        , ACP_PLAYED              ") '6
    _str_sql.AppendLine("        , ACP_BALANCE             ") '7
    _str_sql.AppendLine("        , ACP_STATUS              ") '8
    _str_sql.AppendLine("        , ACP_CREDIT_TYPE         ") '9
    _str_sql.AppendLine("        , ACP_CASH_IN             ") '10
    _str_sql.AppendLine("        , ACP_PROMO_ID            ") '11
    _str_sql.AppendLine("        , ACP_POINTS              ") '12
    _str_sql.AppendLine("        , ACP_WON                 ") '13
    _str_sql.AppendLine("        , ACP_DETAILS             ") '14
    _str_sql.AppendLine("        , ACP_ACTIVATION          ") '15
    _str_sql.AppendLine("        , ACP_UNIQUE_ID           ") '16
    _str_sql.AppendLine("        , ISNULL(CS_SESSION_ID, -1) CSI_NN ") '17
    _str_sql.AppendLine("        , CS_NAME                 ") '18
    _str_sql.AppendLine("        , ISNULL(ISNULL(AO_USER_ID, CS_USER_ID), -1) UI_NN ") '19
    _str_sql.AppendLine("        , ISNULL('MB - ' + CAST(AO_MB_ACCOUNT_ID AS NVARCHAR(50)), GU_USERNAME) UN_NN ") '20
    _str_sql.AppendLine("        , ISNULL(MB_HOLDER_NAME, GU_FULL_NAME) ") '21
    _str_sql.AppendLine("        , ACP_PROMO_CATEGORY_ID   ") '22
    _str_sql.AppendLine("        , AC_TYPE   ") '23

    'Data to the account
    If chk_view_data_account.Checked Then
      _str_sql.AppendLine(mdl_account_for_report.AccountFieldsSql())
    End If

    _str_sql.AppendLine("   FROM   ACCOUNT_PROMOTIONS   WITH (INDEX(IX_acp_activation)) ")

    _str_sql.AppendLine(" LEFT   JOIN ACCOUNT_OPERATIONS	ON ACP_OPERATION_ID					= AO_OPERATION_ID ")
    _str_sql.AppendLine(" LEFT   JOIN CASHIER_SESSIONS		ON AO_CASHIER_SESSION_ID			= CS_SESSION_ID ")
    _str_sql.AppendLine(" LEFT   JOIN GUI_USERS			ON ISNULL(AO_USER_ID, CS_USER_ID)	= GU_USER_ID ")
    _str_sql.AppendLine(" LEFT   JOIN MOBILE_BANKS			ON AO_MB_ACCOUNT_ID					= MB_ACCOUNT_ID ")
    _str_sql.AppendLine("  INNER   JOIN ACCOUNTS      ON ACP_ACCOUNT_ID   = AC_ACCOUNT_ID ")

    If Not String.IsNullOrEmpty(uc_account_sel.MassiveSearchNumbers) Then
      If Me.uc_account_sel.MassiveSearchType = GUI_Controls.uc_account_sel.ENUM_MASSIVE_SEARCH_TYPE.ID_ACCOUNT Then
        _str_sql.AppendLine(Me.uc_account_sel.GetInnerJoinAccountMassiveSearch("ACP_ACCOUNT_ID"))
      Else
        _str_sql.AppendLine(Me.uc_account_sel.GetInnerJoinAccountMassiveSearch("AC_TRACK_DATA"))
      End If
    End If

    _str_sql.AppendLine(GetSqlWhere())

    If opt_order_session_user_prom.Checked Then
      _str_sql.AppendLine(" ORDER   BY  CS_NAME, CSI_NN, UN_NN, UI_NN, ACP_PROMO_NAME, ACP_PROMO_ID, ACP_ACTIVATION DESC ")
    ElseIf opt_order_prom_session_user.Checked Then
      _str_sql.AppendLine(" ORDER   BY  ACP_PROMO_NAME, ACP_PROMO_ID, CS_NAME, CSI_NN, UN_NN, UI_NN, ACP_ACTIVATION DESC ")
    ElseIf opt_order_account_day.Checked Then
      _str_sql.AppendLine(" ORDER   BY   ACP_ACCOUNT_ID, ACP_PROMO_DATE, ACP_PROMO_NAME, ACP_PROMO_ID, CS_NAME, CSI_NN, UN_NN, UI_NN, ACP_ACTIVATION DESC ")
    End If

    '_str_sql.AppendLine(" DROP TABLE #USER_DATA ")

    If Not String.IsNullOrEmpty(uc_account_sel.MassiveSearchNumbers) Then
      _str_sql.AppendLine(uc_account_sel.DropTableAccountMassiveSearch())
    End If

    Return _str_sql.ToString()
  End Function ' GUI_FilterGetSqlQuery  

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()
    Call GUI_StyleSheet()

    Call InitSubtotalValues(2)

    m_total_cash_in = 0
    m_total_promo_points = 0
    m_total_redim = 0
    m_total_noredim = 0
    m_total_played = 0
    m_total_balance = 0
    m_total_won = 0
    m_total_points = 0

    m_last_subtotal2 = INITIAL_VALUE_SUBTOTAL
    m_last_subtotal1 = INITIAL_VALUE_SUBTOTAL

    m_num_rows_subtotal1 = 0
    m_num_rows_subtotal2 = 0
    m_num_rows_detail = 0

    If Me.opt_order_session_user_prom.Checked Then
      'columns
      m_sql_column_subtotal2 = SQL_COLUMN_UD_USER_ID
      m_sql_column_subtotal1 = SQL_COLUMN_UD_CASHIER_SESION_ID
    ElseIf Me.opt_order_prom_session_user.Checked Then
      'columns
      m_sql_column_subtotal2 = SQL_COLUMN_UD_CASHIER_SESION_ID
      m_sql_column_subtotal1 = SQL_COLUMN_PROMO_ID
    ElseIf Me.opt_order_account_day.Checked Then
      'columns
      m_sql_column_subtotal2 = SQL_COLUMN_PROMOTION_DATE
      m_sql_column_subtotal1 = SQL_COLUMN_ACCOUNT_ID
    End If

  End Sub ' GUI_BeforeFirstRow

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    Dim _row_index As Integer

    If m_num_rows_detail <> 0 Then

      _row_index = Grid.NumRows

      ' Subtotal 1
      Grid.AddRow()
      Call AddSubtotalRow(_row_index, 1, m_last_db_row_detail)
      ' Subtotal 2
      Me.Grid.AddRow()
      _row_index = _row_index + 1
      Call AddSubtotalRow(_row_index, 2, m_last_db_row_detail)
    End If
    ' Total

    _row_index = Me.Grid.AddRow()

    If Not opt_order_account_day.Checked Then
      Me.Grid.Cell(_row_index, 4).Value = GLB_NLS_GUI_STATISTICS.GetString(203) ' TOTAL
      Me.Grid.Cell(_row_index, 4).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      Me.Grid.Cell(_row_index, 5).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1256, GUI_FormatNumber(m_num_rows_detail, 0)) ' Amount: %1
      Me.Grid.Cell(_row_index, 5).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    Else
      Me.Grid.Cell(_row_index, GRID_COLUMN_USER).Value = GLB_NLS_GUI_STATISTICS.GetString(203)
      Me.Grid.Cell(_row_index, GRID_COLUMN_USER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      Me.Grid.Cell(_row_index, GRID_COLUMN_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1256, GUI_FormatNumber(m_num_rows_detail, 0))
      Me.Grid.Cell(_row_index, GRID_COLUMN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    End If

    Me.Grid.Cell(_row_index, GRID_COLUMN_CASH_IN).Value = GUI_FormatCurrency(m_total_cash_in, 2)
    Me.Grid.Cell(_row_index, GRID_COLUMN_PROMO_POINTS).Value = GUI_FormatNumber(m_total_promo_points, 0)
    Me.Grid.Cell(_row_index, GRID_COLUMN_PROMO_REDIM).Value = GUI_FormatCurrency(m_total_redim, 2)
    Me.Grid.Cell(_row_index, GRID_COLUMN_PROMO_NON_REDIM).Value = GUI_FormatCurrency(m_total_noredim, 2)
    Me.Grid.Cell(_row_index, GRID_COLUMN_PLAYED).Value = GUI_FormatCurrency(m_total_played, 2)
    Me.Grid.Cell(_row_index, GRID_COLUMN_BALANCE).Value = GUI_FormatCurrency(m_total_balance, 2)
    Me.Grid.Cell(_row_index, GRID_COLUMN_WON).Value = GUI_FormatCurrency(m_total_won, 2)
    Me.Grid.Cell(_row_index, GRID_COLUMN_POINTS).Value = GUI_FormatNumber(m_total_points, 0)
    Me.Grid.Cell(_row_index, GRID_COLUMN_UNIQUE).Value = LINE_TYPE_SUBTOTAL_OR_TOTAL

    ' Style
    Me.Grid.Row(_row_index).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

  End Sub ' GUI_AfterLastRow

  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As CLASS_DB_ROW) As Boolean
    Dim idx_row As Integer

    If Not chk_details.Checked Then
      idx_row = Me.Grid.NumRows - 1

      If idx_row = -1 Then
        m_last_db_row_detail = Nothing
        m_num_rows_subtotal1 = 0
      End If

      If Not m_last_db_row_detail Is Nothing Then
        If (DbRow.Value(SQL_COLUMN_PROMOTION_NAME) <> m_last_db_row_detail.Value(SQL_COLUMN_PROMOTION_NAME)) Then
          Me.Grid.AddRow()
          idx_row = idx_row + 1

          Me.Grid.Cell(idx_row, GRID_COLUMN_USER).Value = GLB_NLS_GUI_INVOICING.GetString(375)
          Me.Grid.Cell(idx_row, GRID_COLUMN_PROMOTION_ACTIVATION).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1256, m_num_rows_subtotal1.ToString())
          Me.Grid.Cell(idx_row, GRID_COLUMN_PROMOTION_ACTIVATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
          Me.Grid.Cell(idx_row, GRID_COLUMN_PROMO_POINTS).Value = IIf(m_subtotal1_promo_points > 0, GUI_FormatNumber(m_subtotal1_promo_points, 0), "")
          Me.Grid.Cell(idx_row, GRID_COLUMN_PROMO_REDIM).Value = IIf(m_subtotal1_redim > 0, GUI_FormatCurrency(m_subtotal1_redim, 2), "")
          Me.Grid.Cell(idx_row, GRID_COLUMN_PROMO_NON_REDIM).Value = IIf(m_subtotal1_noredim > 0, GUI_FormatCurrency(m_subtotal1_noredim, 2), "")

          Me.Grid.Row(idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
          m_num_rows_detail += m_num_rows_subtotal1
          m_total_redim += m_subtotal1_redim
          m_subtotal1_redim = 0
          m_num_rows_subtotal1 = DbRow.Value(SQL_COLUMN_NUM_ROWS_GROUP)
        Else
          m_num_rows_subtotal1 += DbRow.Value(SQL_COLUMN_NUM_ROWS_GROUP)
        End If
      Else
        m_num_rows_subtotal1 += DbRow.Value(SQL_COLUMN_NUM_ROWS_GROUP)
      End If
    End If

    Return True

  End Function 'GUI_CheckOutRowBeforeAdd

  Private Function GUI_SetupRowWithoutDetails(ByVal RowIndex As Integer, _
                                                ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTION_NAME).Value = DbRow.Value(SQL_COLUMN_PROMOTION_NAME)
    ' Promotion Category
    If DbRow.Value(SQL_COLUMN_PROMO_CATEGORY) Is DBNull.Value Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTION_CATEGORY).Value = ""
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTION_CATEGORY).Value = PromotionCategories.Value(DbRow.Value(SQL_COLUMN_PROMO_CATEGORY))
    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_CASHIER_SESSION_NAME).Value = IIf(DbRow.IsNull(SQL_COLUMN_UD_CASHIER_SESSION_NAME), _
                                                                        "", DbRow.Value(SQL_COLUMN_UD_CASHIER_SESSION_NAME))
    ' number rows subtotal
    If DbRow.Value(SQL_COLUMN_ACCOUNT_NAME).ToString() <> String.Empty Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTION_ACTIVATION).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1256, DbRow.Value(1))
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTION_ACTIVATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    End If

    Select Case DbRow.Value(SQL_COLUMN_CREDIT_TYPE)
      Case ACCOUNT_PROMO_CREDIT_TYPE.NR1, ACCOUNT_PROMO_CREDIT_TYPE.NR2
        If DbRow.Value(SQL_COLUMN_LEVEL).ToString() <> "1" Then
          m_subtotal1_noredim += DbRow.Value(SQL_COLUMN_INIT_BALANCE)
        End If
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_NON_REDIM).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_INIT_BALANCE), 2)
      Case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_REDIM).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_INIT_BALANCE), 2)
        If DbRow.Value(SQL_COLUMN_LEVEL).ToString() <> "1" Then
          m_subtotal1_redim += DbRow.Value(SQL_COLUMN_INIT_BALANCE)
        End If
      Case ACCOUNT_PROMO_CREDIT_TYPE.POINT
        If DbRow.Value(SQL_COLUMN_LEVEL).ToString() <> "1" Then
          m_subtotal1_promo_points += DbRow.Value(SQL_COLUMN_INIT_BALANCE)
        End If
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_POINTS).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_INIT_BALANCE), 0)
    End Select

    If DbRow.Value(SQL_COLUMN_CASH_IN).ToString() <> String.Empty Then
      If DbRow.Value(SQL_COLUMN_LEVEL).ToString() <> "1" Then
        m_total_cash_in += DbRow.Value(SQL_COLUMN_CASH_IN)
      End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_IN).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_CASH_IN), 2)
    End If

    If DbRow.Value(SQL_COLUMN_ACP_POINT).ToString() <> String.Empty Then
      If DbRow.Value(SQL_COLUMN_LEVEL).ToString() <> "1" Then
        m_total_points += DbRow.Value(SQL_COLUMN_ACP_POINT)
      End If
      Me.Grid.Cell(RowIndex, SQL_COLUMN_ACP_POINT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_ACP_POINT), 0)
    End If

    If DbRow.Value(SQL_COLUMN_LEVEL).ToString() = "1" Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_USER).Value = GLB_NLS_GUI_INVOICING.GetString(375)
      Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
    Else
      Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_01)
    End If

  End Function

  Private Function GUI_SetupRowWithDetails(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _date_value As Date
    Dim _date_last_value As Date

    ' count rows the detail
    m_num_rows_detail = m_num_rows_detail + 1

    If Not opt_order_account_day.Checked Then
      If m_last_subtotal2 <> DbRow.Value(m_sql_column_subtotal2) AndAlso m_last_subtotal2 <> INITIAL_VALUE_SUBTOTAL Or _
         m_last_subtotal1 <> DbRow.Value(m_sql_column_subtotal1) AndAlso m_last_subtotal1 <> INITIAL_VALUE_SUBTOTAL Then
        Call AddSubtotalRow(RowIndex, 1, m_last_db_row_detail)
        Me.Grid.AddRow()
        RowIndex = RowIndex + 1
      End If
    Else
      If TypeOf m_last_subtotal2 Is Date Then
        _date_value = DbRow.Value(m_sql_column_subtotal2)
        _date_last_value = m_last_subtotal2

        If _date_value.ToString("yyyy-MM-dd") <> _date_last_value.ToString("yyyy-MM-dd") Or _
         m_last_subtotal1 <> DbRow.Value(m_sql_column_subtotal1) AndAlso m_last_subtotal1 <> INITIAL_VALUE_SUBTOTAL Then
          Call AddSubtotalRow(RowIndex, 1, m_last_db_row_detail)
          Me.Grid.AddRow()
          RowIndex = RowIndex + 1
        End If
      End If
    End If

    If m_last_subtotal1 <> DbRow.Value(m_sql_column_subtotal1) AndAlso m_last_subtotal1 <> INITIAL_VALUE_SUBTOTAL Then
      Call AddSubtotalRow(RowIndex, 2, m_last_db_row_detail)
      Me.Grid.AddRow()
      RowIndex = RowIndex + 1
    End If

    m_num_rows_subtotal1 = m_num_rows_subtotal1 + 1
    m_num_rows_subtotal2 = m_num_rows_subtotal2 + 1

    m_last_subtotal2 = DbRow.Value(m_sql_column_subtotal2)
    m_last_subtotal1 = DbRow.Value(m_sql_column_subtotal1)

    ' Assign Mapped columns (search for 'mapping' string in this file)
    Call MyBase.GUI_SetupRow(RowIndex, DbRow)


    ' Level
    If m_level_names.ContainsKey(DbRow.Value(SQL_COLUMN_LEVEL)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_LEVEL).Value = m_level_names(DbRow.Value(SQL_COLUMN_LEVEL))
    End If

    ' Account name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_NAME).Value = IIf(DbRow.IsNull(SQL_COLUMN_ACCOUNT_NAME), "", DbRow.Value(SQL_COLUMN_ACCOUNT_NAME))

    ' Accound id
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_ID)

    ' Date
    If DbRow.IsNull(SQL_COLUMN_PROMOTION_ACTIVATION) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTION_ACTIVATION).Value = ""
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTION_ACTIVATION).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_PROMOTION_ACTIVATION), _
                                                                                          ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                          ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    ' Promotion name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTION_NAME).Value = DbRow.Value(SQL_COLUMN_PROMOTION_NAME)
    If Me.opt_promo_system_period.Checked Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTION_NAME).Value = DbRow.Value(SQL_COLUMN_PROMOTION_NAME) & " " & _
                                                                 Strings.Format(DbRow.Value(SQL_COLUMN_PROMOTION_DATE), "dd/MMM")
    End If

    ' Promotion Category
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTION_CATEGORY).Value = PromotionCategories.Value(DbRow.Value(SQL_COLUMN_PROMO_CATEGORY))

    ' Cash in
    If Not DbRow.IsNull(SQL_COLUMN_CASH_IN) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_IN).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_CASH_IN), 2)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_IN).Value = ""
    End If

    ' Init. Balance. is Redimible or non redimible.
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_REDIM).Value = ""
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_NON_REDIM).Value = ""

    ' Credit type
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CREDIT_TYPE).Value = DbRow.Value(SQL_COLUMN_CREDIT_TYPE)

    Select Case DbRow.Value(SQL_COLUMN_CREDIT_TYPE)
      Case ACCOUNT_PROMO_CREDIT_TYPE.NR1, ACCOUNT_PROMO_CREDIT_TYPE.NR2
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_NON_REDIM).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_INIT_BALANCE), 2)
      Case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_REDIM).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_INIT_BALANCE), 2)
      Case ACCOUNT_PROMO_CREDIT_TYPE.POINT
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_POINTS).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_INIT_BALANCE), 0)
    End Select

    ' Played
    If DbRow.Value(SQL_COLUMN_PLAYED) > 0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PLAYED), 2)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED).Value = ""
    End If

    ' Balance.
    If DbRow.Value(SQL_COLUMN_CREDIT_TYPE) <> ACCOUNT_PROMO_CREDIT_TYPE.POINT Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_BALANCE).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_BALANCE), 2)
    End If

    ' Points
    If Not DbRow.IsNull(SQL_COLUMN_ACP_POINT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_ACP_POINT), 0)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS).Value = ""
    End If

    ' Details
    If Not DbRow.IsNull(SQL_COLUMN_DETAILS) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAILS).Value = DbRow.Value(SQL_COLUMN_DETAILS)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAILS).Value = ""
    End If

    ' Won
    If DbRow.Value(SQL_COLUMN_WON) > 0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_WON).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_WON), 2)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_WON).Value = ""
    End If

    ' Unique.
    Me.Grid.Cell(RowIndex, GRID_COLUMN_UNIQUE).Value = DbRow.Value(SQL_COLUMN_UNIQUE_ID)

    ' Cashier sesion
    If Not DbRow.IsNull(SQL_COLUMN_UD_CASHIER_SESSION_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CASHIER_SESSION_NAME).Value = DbRow.Value(SQL_COLUMN_UD_CASHIER_SESSION_NAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CASHIER_SESSION_NAME).Value = ""
    End If

    ' User
    If Not DbRow.IsNull(SQL_COLUMN_UD_USER) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_USER).Value = DbRow.Value(SQL_COLUMN_UD_USER)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_USER).Value = ""
    End If

    ' Name
    If Not DbRow.IsNull(SQL_COLUMN_UD_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NAME).Value = DbRow.Value(SQL_COLUMN_UD_NAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NAME).Value = ""
    End If

    ' Holder data colums
    If Me.chk_view_data_account.Checked Then
      mdl_account_for_report.SetupRowHolderData(Me.Grid, DbRow, RowIndex, GRID_COLUMNS, SQL_INIT_COLUMNS_HOLDER_DATA)
    End If

    'Promotion Status
    If (WSI.Common.Misc.IsFeatureTerminalDrawEnabled() And StatusVisibleForGeneralParam()) Then
      If Not DbRow.IsNull(SQL_COLUMN_STATUS) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_STATUS).Value = CastPromoStatus(DbRow.Value(SQL_COLUMN_STATUS))
      End If
    End If

    Call CalculateSubtotalRow(DbRow)
    m_last_db_row_detail = DbRow

  End Function

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  'RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    If chk_details.Checked Then
      GUI_SetupRowWithDetails(RowIndex, DbRow)
    End If

    Return True

  End Function ' GUI_SetupRowf

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS: 
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_AUDITOR.GetString(257), m_init_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_AUDITOR.GetString(258), m_init_date_to)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(781), m_order)
    PrintData.SetFilter(gb_promo_type.Text, m_promotion_type)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(256), m_promo_name)
    PrintData.SetFilter(gb_options.Text, m_details_and_account_data)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), m_account)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_track_data)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), m_holder)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4802), m_holder_is_vip)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(336), m_promo_type_credit)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(220), m_users)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1336), m_category)

    If (WSI.Common.Misc.IsFeatureTerminalDrawEnabled() And StatusVisibleForGeneralParam()) Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(8708), m_status)
    End If

    ' Width from header filter
    PrintData.Settings.FilterHeaderWidth(1) = 1600
    PrintData.Settings.FilterHeaderWidth(2) = 1000
    PrintData.Settings.FilterHeaderWidth(3) = 1400

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_init_date_from = ""
    m_init_date_to = ""
    m_promo_type_credit = ""

    ' Init Date From
    If Me.uc_dsl.FromDateSelected Then
      m_init_date_from = GUI_FormatDate(Me.uc_dsl.FromDate.AddHours(uc_dsl.ClosingTime), _
                                        ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    ' Init Date To
    If Me.uc_dsl.ToDateSelected Then
      m_init_date_to = GUI_FormatDate(Me.uc_dsl.ToDate.AddHours(uc_dsl.ClosingTime), _
                                      ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    ' Promotion type
    m_promotion_type = ""

    If Me.opt_promo_all.Checked Then
      m_promotion_type = Me.opt_promo_all.Text
    ElseIf Me.opt_promo_manual.Checked Then
      m_promotion_type = Me.opt_promo_manual.Text
    ElseIf Me.opt_promo_system_period.Checked Then
      m_promotion_type = Me.opt_promo_system_period.Text
    ElseIf Me.opt_promo_system_auto.Checked Then
      m_promotion_type = Me.opt_promo_system_auto.Text
    ElseIf Me.opt_promo_per_points.Checked Then
      m_promotion_type = Me.opt_promo_per_points.Text
    ElseIf Me.opt_promo_preassigned.Checked Then
      m_promotion_type = Me.opt_promo_preassigned.Text
    End If


    ' Promo
    m_promo_name = Me.ef_promo_name.Value

    ' details and show account data
    m_details_and_account_data = ""
    If chk_details.Checked Then
      m_details_and_account_data = Me.chk_details.Text
      If chk_view_data_account.Checked Then
        m_details_and_account_data = m_details_and_account_data & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(484) & " " & chk_view_data_account.Text
      End If
    End If

    ' Order
    If opt_order_session_user_prom.Checked Then
      m_order = opt_order_session_user_prom.Text
    ElseIf opt_order_prom_session_user.Checked Then
      m_order = opt_order_prom_session_user.Text
    ElseIf opt_order_account_day.Checked Then
      m_order = opt_order_account_day.Text
    End If

    ' Status
    If (WSI.Common.Misc.IsFeatureTerminalDrawEnabled() And StatusVisibleForGeneralParam()) Then

      If opt_status_all.Checked Then
        m_status = opt_status_all.Text
      ElseIf opt_status_pending_player.Checked Then
        m_status = opt_status_pending_player.Text
      ElseIf opt_status_bought.Checked Then
        m_status = opt_status_bought.Text
      ElseIf opt_status_pending_draw.Checked Then
        m_status = opt_status_pending_draw.Text
      End If

    End If

    m_users = ""
    ' Users
    If Me.opt_all_users.Checked Then
      m_users = Me.opt_all_users.Text
    Else
      m_users = Me.cmb_user.TextValue
    End If

    ' Account
    m_account = Me.uc_account_sel.Account()
    m_track_data = Me.uc_account_sel.TrackData()
    m_holder = Me.uc_account_sel.Holder()
    m_holder_is_vip = Me.uc_account_sel.HolderIsVip()

    ' Promotion Credit Type
    If Me.chk_credit_type_points.Checked Then
      If Not String.IsNullOrEmpty(m_promo_type_credit) Then
        m_promo_type_credit = m_promo_type_credit & ", "
      End If
      m_promo_type_credit = m_promo_type_credit & GLB_NLS_GUI_INVOICING.GetString(343)
    End If
    If Me.chk_credit_type_redeem.Checked Then
      If Not String.IsNullOrEmpty(m_promo_type_credit) Then
        m_promo_type_credit = m_promo_type_credit & ", "
      End If
      m_promo_type_credit = m_promo_type_credit & GLB_NLS_GUI_INVOICING.GetString(337)
    End If
    If Me.chk_credit_type_non_redeem.Checked Then
      If Not String.IsNullOrEmpty(m_promo_type_credit) Then
        m_promo_type_credit = m_promo_type_credit & ", "
      End If
      m_promo_type_credit = m_promo_type_credit & GLB_NLS_GUI_INVOICING.GetString(338)
    End If

    ' Promotion Category
    m_category = Me.cmb_categories.TextValue

    m_only_user_accounts = False

  End Sub ' GUI_ReportUpdateFilters

  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(ExcelData)

    ' Set specific column formats.
    If m_holder_data Then

      ExcelData.SetColumnFormat(EXCEL_COLUMN_TELEPHONE_NUMBER_1, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)
      ExcelData.SetColumnFormat(EXCEL_COLUMN_TELEPHONE_NUMBER_2, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)
      ExcelData.SetColumnFormat(EXCEL_COLUMN_POSTAL_CODE, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)

    End If

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call mdl_account_for_report.HolderColumnsPrintable(Me.Grid, GRID_COLUMNS, False)

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  ' PURPOSE: Process clicks on data grid (double-clicks) and select button
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)


    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_FILTER_APPLY

        If chk_details.Checked Then
          m_holder_data = chk_view_data_account.Checked
          Call MyBase.GUI_ButtonClick(ButtonId)
          Me.Grid.Counter(0).Value = m_num_rows_detail
        Else
          Call GUI_BeforeFirstRow()
          Call GUI_ClearGrid()
          LoadGridWithoutDetails()
        End If

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)

    End Select

  End Sub ' GUI_ButtonClick

#End Region

#Region " Public Functions  "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '         - mdiparent
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    Me.MdiParent = MdiParent
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Call Me.Display(False)

  End Sub ' ShowForEdit 

#End Region

#Region "Private Functions "

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - RowIndex: Index of grid
  '           - TypeSubtotal: The type of subtotal, we says that subtotal is, according with order.
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub AddSubtotalRow(ByVal RowIndex As Integer, ByVal TypeSubtotal As Integer, ByVal LastDbRowDetail As CLASS_DB_ROW)

    'Case the selected order "cashier session, user and promotion"  TypeSubtotal = 1 is about user and  TypeSubtotal = 2 is about cashier session
    '                        "promotion, cashier session, user"  TypeSubtotal = 1 is about cashier session and  TypeSubtotal = 2 is about promotion 
    If TypeSubtotal = 1 Then
      ' Subtotal 1
      ' add data in reference to subtotal
      If Me.opt_order_session_user_prom.Checked Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CASHIER_SESSION_NAME).Value = IIf(LastDbRowDetail.IsNull(SQL_COLUMN_UD_CASHIER_SESSION_NAME), _
                                                                            "", LastDbRowDetail.Value(SQL_COLUMN_UD_CASHIER_SESSION_NAME))
        Me.Grid.Cell(RowIndex, GRID_COLUMN_USER).Value = IIf(LastDbRowDetail.IsNull(SQL_COLUMN_UD_USER), _
                                                                            "", LastDbRowDetail.Value(SQL_COLUMN_UD_USER))
        Me.Grid.Cell(RowIndex, GRID_COLUMN_NAME).Value = IIf(LastDbRowDetail.IsNull(SQL_COLUMN_UD_NAME), _
                                                                            "", LastDbRowDetail.Value(SQL_COLUMN_UD_NAME))
        If chk_details.Checked Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTION_ACTIVATION).Value = GLB_NLS_GUI_INVOICING.GetString(375)
          Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTION_ACTIVATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
        End If

        ' number rows subtotal
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTION_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1256, GUI_FormatNumber(m_num_rows_subtotal1, 0))

      ElseIf Me.opt_order_prom_session_user.Checked Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTION_NAME).Value = LastDbRowDetail.Value(SQL_COLUMN_PROMOTION_NAME)
        ' Promotion Category
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTION_CATEGORY).Value = PromotionCategories.Value(LastDbRowDetail.Value(SQL_COLUMN_PROMO_CATEGORY))

        Me.Grid.Cell(RowIndex, GRID_COLUMN_CASHIER_SESSION_NAME).Value = IIf(LastDbRowDetail.IsNull(SQL_COLUMN_UD_CASHIER_SESSION_NAME), _
                                                                            "", LastDbRowDetail.Value(SQL_COLUMN_UD_CASHIER_SESSION_NAME))
        If chk_details.Checked Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_USER).Value = GLB_NLS_GUI_INVOICING.GetString(375)
        End If
        ' number rows subtotal
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTION_ACTIVATION).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1256, GUI_FormatNumber(m_num_rows_subtotal1, 0))
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTION_ACTIVATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ElseIf Me.opt_order_account_day.Checked Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID).Value = LastDbRowDetail.Value(SQL_COLUMN_ACCOUNT_ID)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_LEVEL).Value = m_level_names(LastDbRowDetail.Value(SQL_COLUMN_LEVEL))
        If Not LastDbRowDetail.IsNull(SQL_COLUMN_ACCOUNT_NAME) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_NAME).Value = LastDbRowDetail.Value(SQL_COLUMN_ACCOUNT_NAME)
        End If
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTION_ACTIVATION).Value = GUI_FormatDate(LastDbRowDetail.Value(SQL_COLUMN_PROMOTION_DATE), _
                                                                                            ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                            ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_USER).Value = GLB_NLS_GUI_INVOICING.GetString(375)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1256, GUI_FormatNumber(m_num_rows_subtotal1, 0))

      End If

      ' calculated fields
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_IN).Value = IIf(m_subtotal1_cash_in > 0, GUI_FormatCurrency(m_subtotal1_cash_in, 2), "")
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_POINTS).Value = IIf(m_subtotal1_promo_points > 0, GUI_FormatNumber(m_subtotal1_promo_points, 0), "")
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_REDIM).Value = IIf(m_subtotal1_redim > 0, GUI_FormatCurrency(m_subtotal1_redim, 2), "")
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_NON_REDIM).Value = IIf(m_subtotal1_noredim > 0, GUI_FormatCurrency(m_subtotal1_noredim, 2), "")
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED).Value = IIf(m_subtotal1_played > 0, GUI_FormatCurrency(m_subtotal1_played, 2), "")
      Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS).Value = IIf(m_subtotal1_points > 0, GUI_FormatNumber(m_subtotal1_points, 0), "")
      Me.Grid.Cell(RowIndex, GRID_COLUMN_WON).Value = IIf(m_subtotal1_won > 0, GUI_FormatCurrency(m_subtotal1_won, 2), "")
      Me.Grid.Cell(RowIndex, GRID_COLUMN_BALANCE).Value = IIf(m_subtotal1_balance > 0, GUI_FormatCurrency(m_subtotal1_balance, 2), "")

      m_num_rows_subtotal1 = 0
      ' style
      Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_01)

      m_subtotal2_cash_in = m_subtotal2_cash_in + m_subtotal1_cash_in
      m_subtotal2_promo_points = m_subtotal2_promo_points + m_subtotal1_promo_points
      m_subtotal2_redim = m_subtotal2_redim + m_subtotal1_redim
      m_subtotal2_noredim = m_subtotal2_noredim + m_subtotal1_noredim
      m_subtotal2_played = m_subtotal2_played + m_subtotal1_played
      m_subtotal2_balance = m_subtotal2_balance + m_subtotal1_balance
      m_subtotal2_won = m_subtotal2_won + m_subtotal1_won
      m_subtotal2_points = m_subtotal2_points + m_subtotal1_points

    ElseIf TypeSubtotal = 2 Then
      ' Subtotal 2
      ' add data in reference to subtotal
      If Me.opt_order_session_user_prom.Checked Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CASHIER_SESSION_NAME).Value = IIf(LastDbRowDetail.IsNull(SQL_COLUMN_UD_CASHIER_SESSION_NAME), _
                                                                            "", LastDbRowDetail.Value(SQL_COLUMN_UD_CASHIER_SESSION_NAME))
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTION_ACTIVATION).Value = GLB_NLS_GUI_INVOICING.GetString(375)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTION_ACTIVATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
        ' number rows subtotal
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTION_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1256, GUI_FormatNumber(m_num_rows_subtotal2, 0))
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTION_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      ElseIf Me.opt_order_prom_session_user.Checked Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTION_NAME).Value = LastDbRowDetail.Value(SQL_COLUMN_PROMOTION_NAME)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTION_CATEGORY).Value = PromotionCategories.Value(LastDbRowDetail.Value(SQL_COLUMN_PROMO_CATEGORY))

        Me.Grid.Cell(RowIndex, GRID_COLUMN_USER).Value = GLB_NLS_GUI_INVOICING.GetString(375)
        ' number rows subtotal
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTION_ACTIVATION).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1256, GUI_FormatNumber(m_num_rows_subtotal2, 0))
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTION_ACTIVATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ElseIf Me.opt_order_account_day.Checked Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID).Value = LastDbRowDetail.Value(SQL_COLUMN_ACCOUNT_ID)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_LEVEL).Value = m_level_names(LastDbRowDetail.Value(SQL_COLUMN_LEVEL))
        If Not LastDbRowDetail.IsNull(SQL_COLUMN_ACCOUNT_NAME) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_NAME).Value = LastDbRowDetail.Value(SQL_COLUMN_ACCOUNT_NAME)
        End If
        Me.Grid.Cell(RowIndex, GRID_COLUMN_USER).Value = GLB_NLS_GUI_INVOICING.GetString(375)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1256, GUI_FormatNumber(m_num_rows_subtotal2, 0))

      End If

      Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_IN).Value = IIf(m_subtotal2_cash_in > 0, GUI_FormatCurrency(m_subtotal2_cash_in, 2), "")
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_POINTS).Value = IIf(m_subtotal2_promo_points > 0, GUI_FormatNumber(m_subtotal2_promo_points, 0), "")
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_REDIM).Value = IIf(m_subtotal2_redim > 0, GUI_FormatCurrency(m_subtotal2_redim, 2), "")
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_NON_REDIM).Value = IIf(m_subtotal2_noredim > 0, GUI_FormatCurrency(m_subtotal2_noredim, 2), "")
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED).Value = IIf(m_subtotal2_played > 0, GUI_FormatCurrency(m_subtotal2_played, 2), "")
      Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS).Value = IIf(m_subtotal2_points > 0, GUI_FormatNumber(m_subtotal2_points, 0), "")
      Me.Grid.Cell(RowIndex, GRID_COLUMN_WON).Value = IIf(m_subtotal2_won > 0, GUI_FormatCurrency(m_subtotal2_won, 2), "")
      Me.Grid.Cell(RowIndex, GRID_COLUMN_BALANCE).Value = IIf(m_subtotal2_balance > 0, GUI_FormatCurrency(m_subtotal2_balance, 2), "")
      m_num_rows_subtotal2 = 0
      ' style
      Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

      m_total_cash_in = m_total_cash_in + m_subtotal2_cash_in
      m_total_promo_points = m_total_promo_points + m_subtotal2_promo_points
      m_total_redim = m_total_redim + m_subtotal2_redim
      m_total_noredim = m_total_noredim + m_subtotal2_noredim
      m_total_played = m_total_played + m_subtotal2_played
      m_total_balance = m_total_balance + m_subtotal2_balance
      m_total_points = m_total_points + m_subtotal2_points
      m_total_won = m_total_won + m_subtotal2_won
    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_UNIQUE).Value = LINE_TYPE_SUBTOTAL_OR_TOTAL
    'init values
    Call InitSubtotalValues(TypeSubtotal)

  End Sub ' AddSubtotaRow

  ' PURPOSE: Calculate subtotal
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub CalculateSubtotalRow(ByVal DbRow As CLASS_DB_ROW)

    ' cash in
    If Not DbRow.IsNull(SQL_COLUMN_CASH_IN) Then
      m_subtotal1_cash_in = m_subtotal1_cash_in + DbRow.Value(SQL_COLUMN_CASH_IN)
    End If

    ' Init Balance 
    Select Case DbRow.Value(SQL_COLUMN_CREDIT_TYPE)
      Case ACCOUNT_PROMO_CREDIT_TYPE.NR1, ACCOUNT_PROMO_CREDIT_TYPE.NR2
        m_subtotal1_noredim = m_subtotal1_noredim + DbRow.Value(SQL_COLUMN_INIT_BALANCE)
      Case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE
        m_subtotal1_redim = m_subtotal1_redim + DbRow.Value(SQL_COLUMN_INIT_BALANCE)
      Case ACCOUNT_PROMO_CREDIT_TYPE.POINT
        m_subtotal1_promo_points = m_subtotal1_promo_points + DbRow.Value(SQL_COLUMN_INIT_BALANCE)
    End Select

    ' Played
    If DbRow.Value(SQL_COLUMN_PLAYED) > 0 Then
      m_subtotal1_played = m_subtotal1_played + DbRow.Value(SQL_COLUMN_PLAYED)
    End If

    ' Balance.
    If DbRow.Value(SQL_COLUMN_CREDIT_TYPE) <> ACCOUNT_PROMO_CREDIT_TYPE.POINT Then
      m_subtotal1_balance = m_subtotal1_balance + DbRow.Value(SQL_COLUMN_BALANCE)
    End If

    ' Points
    If Not DbRow.IsNull(SQL_COLUMN_ACP_POINT) Then
      m_subtotal1_points = m_subtotal1_points + DbRow.Value(SQL_COLUMN_ACP_POINT)
    End If

    ' Won
    If DbRow.Value(SQL_COLUMN_WON) > 0 Then
      m_subtotal1_won = m_subtotal1_won + DbRow.Value(SQL_COLUMN_WON)
    End If

  End Sub ' CalculateSubtotalRow

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()

    Dim _grid_num_columns As Integer

    ' Order columns
    If opt_order_session_user_prom.Checked Then
      GRID_COLUMN_CASHIER_SESSION_NAME = 1
      GRID_COLUMN_USER = 2
      GRID_COLUMN_NAME = 3
      GRID_COLUMN_PROMOTION_ACTIVATION = 4
      GRID_COLUMN_PROMOTION_NAME = 5
      GRID_COLUMN_PROMOTION_CATEGORY = 6
      GRID_COLUMN_PROMO_POINTS = 7
      GRID_COLUMN_PROMO_REDIM = 8
      GRID_COLUMN_PROMO_NON_REDIM = 9
      GRID_COLUMN_PROMO_STATUS = 10
      GRID_COLUMN_PLAYED = 11
      GRID_COLUMN_WON = 12
      GRID_COLUMN_BALANCE = 13
      GRID_COLUMN_DETAILS = 14
      GRID_COLUMN_POINTS = 15
      GRID_COLUMN_CASH_IN = 16
      GRID_COLUMN_UNIQUE = 17
      GRID_COLUMN_CREDIT_TYPE = 18
      GRID_COLUMN_LEVEL = 19
      GRID_COLUMN_ACCOUNT_ID = 20
      GRID_COLUMN_ACCOUNT_NAME = 21

    ElseIf opt_order_prom_session_user.Checked Then
      GRID_COLUMN_PROMOTION_NAME = 1
      GRID_COLUMN_PROMOTION_CATEGORY = 2
      GRID_COLUMN_CASHIER_SESSION_NAME = 3
      GRID_COLUMN_USER = 4
      GRID_COLUMN_NAME = 5
      GRID_COLUMN_PROMOTION_ACTIVATION = 6
      GRID_COLUMN_PROMO_POINTS = 7
      GRID_COLUMN_PROMO_REDIM = 8
      GRID_COLUMN_PROMO_NON_REDIM = 9
      GRID_COLUMN_PROMO_STATUS = 10
      GRID_COLUMN_PLAYED = 11
      GRID_COLUMN_WON = 12
      GRID_COLUMN_BALANCE = 13
      GRID_COLUMN_DETAILS = 14
      GRID_COLUMN_POINTS = 15
      GRID_COLUMN_CASH_IN = 16
      GRID_COLUMN_UNIQUE = 17
      GRID_COLUMN_CREDIT_TYPE = 18
      GRID_COLUMN_LEVEL = 19
      GRID_COLUMN_ACCOUNT_ID = 20
      GRID_COLUMN_ACCOUNT_NAME = 21

    ElseIf opt_order_account_day.Checked Then
      GRID_COLUMN_LEVEL = 3
      GRID_COLUMN_ACCOUNT_ID = 1
      GRID_COLUMN_ACCOUNT_NAME = 2
      GRID_COLUMN_CASHIER_SESSION_NAME = 5
      GRID_COLUMN_USER = 6
      GRID_COLUMN_NAME = 7
      GRID_COLUMN_PROMOTION_ACTIVATION = 4
      GRID_COLUMN_PROMOTION_NAME = 8
      GRID_COLUMN_PROMOTION_CATEGORY = 9
      GRID_COLUMN_PROMO_POINTS = 10
      GRID_COLUMN_PROMO_REDIM = 11
      GRID_COLUMN_PROMO_NON_REDIM = 12
      GRID_COLUMN_PROMO_STATUS = 13
      GRID_COLUMN_PLAYED = 14
      GRID_COLUMN_WON = 15
      GRID_COLUMN_BALANCE = 16
      GRID_COLUMN_DETAILS = 17
      GRID_COLUMN_POINTS = 18
      GRID_COLUMN_CASH_IN = 19
      GRID_COLUMN_UNIQUE = 20
      GRID_COLUMN_CREDIT_TYPE = 21

    End If

    ' view account columns
    If Me.chk_view_data_account.Checked Then
      _grid_num_columns = GRID_COLUMNS + mdl_account_for_report.GRID_NUM_COLUMNS_HOLDER_DATA
    Else
      _grid_num_columns = GRID_COLUMNS
    End If

    With Me.Grid

      Call .Init(_grid_num_columns, GRID_HEADER_ROWS)

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Account
      '  Level
      .Column(GRID_COLUMN_LEVEL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(496)
      .Column(GRID_COLUMN_LEVEL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(443)
      .Column(GRID_COLUMN_LEVEL).Width = IIf(chk_details.Checked Or opt_order_account_day.Checked, 1200, 0)
      .Column(GRID_COLUMN_LEVEL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      '  Account name
      .Column(GRID_COLUMN_ACCOUNT_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(496)
      .Column(GRID_COLUMN_ACCOUNT_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(794)
      .Column(GRID_COLUMN_ACCOUNT_NAME).Width = IIf(chk_details.Checked Or opt_order_account_day.Checked, 2500, 0)
      .Column(GRID_COLUMN_ACCOUNT_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      '  Account id
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(496)
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(795)
      .Column(GRID_COLUMN_ACCOUNT_ID).Width = IIf(chk_details.Checked Or opt_order_account_day.Checked, 1200, 0)
      .Column(GRID_COLUMN_ACCOUNT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT


      ' Cashier Session name
      .Column(GRID_COLUMN_CASHIER_SESSION_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(837)
      .Column(GRID_COLUMN_CASHIER_SESSION_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(629)
      .Column(GRID_COLUMN_CASHIER_SESSION_NAME).Width = 2800
      .Column(GRID_COLUMN_CASHIER_SESSION_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' USER
      .Column(GRID_COLUMN_USER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(837)
      .Column(GRID_COLUMN_USER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(603)
      .Column(GRID_COLUMN_USER).Width = 1250
      .Column(GRID_COLUMN_USER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' NAME
      .Column(GRID_COLUMN_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(837)
      .Column(GRID_COLUMN_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(693)
      .Column(GRID_COLUMN_NAME).Width = 2000
      .Column(GRID_COLUMN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Activation Date
      .Column(GRID_COLUMN_PROMOTION_ACTIVATION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(837)
      .Column(GRID_COLUMN_PROMOTION_ACTIVATION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(604)
      .Column(GRID_COLUMN_PROMOTION_ACTIVATION).Width = 2000
      .Column(GRID_COLUMN_PROMOTION_ACTIVATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Promotion name
      .Column(GRID_COLUMN_PROMOTION_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(609)
      .Column(GRID_COLUMN_PROMOTION_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(693) & " " ' Concatenated a space to differentiate this column and username column when are adjacent
      .Column(GRID_COLUMN_PROMOTION_NAME).Width = 2500
      .Column(GRID_COLUMN_PROMOTION_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Promotion Category
      .Column(GRID_COLUMN_PROMOTION_CATEGORY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(609)
      .Column(GRID_COLUMN_PROMOTION_CATEGORY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1336)
      .Column(GRID_COLUMN_PROMOTION_CATEGORY).Width = 2100
      .Column(GRID_COLUMN_PROMOTION_CATEGORY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Points Credit
      .Column(GRID_COLUMN_PROMO_POINTS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(609)
      .Column(GRID_COLUMN_PROMO_POINTS).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(343)
      .Column(GRID_COLUMN_PROMO_POINTS).Width = 1600
      .Column(GRID_COLUMN_PROMO_POINTS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' No redimible Credit
      .Column(GRID_COLUMN_PROMO_NON_REDIM).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(609)
      .Column(GRID_COLUMN_PROMO_NON_REDIM).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(338)
      .Column(GRID_COLUMN_PROMO_NON_REDIM).Width = 1600
      .Column(GRID_COLUMN_PROMO_NON_REDIM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Redimible Credit 
      .Column(GRID_COLUMN_PROMO_REDIM).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(609)
      .Column(GRID_COLUMN_PROMO_REDIM).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(337)
      .Column(GRID_COLUMN_PROMO_REDIM).Width = 1600
      .Column(GRID_COLUMN_PROMO_REDIM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      If (WSI.Common.Misc.IsFeatureTerminalDrawEnabled() And StatusVisibleForGeneralParam()) Then
        ' Promo Status
        .Column(GRID_COLUMN_PROMO_STATUS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(609)
        .Column(GRID_COLUMN_PROMO_STATUS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(262)
        .Column(GRID_COLUMN_PROMO_STATUS).Width = 1600
        .Column(GRID_COLUMN_PROMO_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      Else
        .Column(GRID_COLUMN_PROMO_STATUS).Width = 0
      End If

      ' Played - hide
      .Column(GRID_COLUMN_PLAYED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(609)
      .Column(GRID_COLUMN_PLAYED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(537)
      .Column(GRID_COLUMN_PLAYED).Width = 0
      .Column(GRID_COLUMN_PLAYED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Balance - hide
      .Column(GRID_COLUMN_BALANCE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(609)
      .Column(GRID_COLUMN_BALANCE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(541)
      .Column(GRID_COLUMN_BALANCE).Width = 0
      .Column(GRID_COLUMN_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Won - hide 
      .Column(GRID_COLUMN_WON).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(609)
      .Column(GRID_COLUMN_WON).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(798)
      .Column(GRID_COLUMN_WON).Width = 0
      .Column(GRID_COLUMN_WON).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Details - hide
      .Column(GRID_COLUMN_DETAILS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(609)
      .Column(GRID_COLUMN_DETAILS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(474)
      .Column(GRID_COLUMN_DETAILS).Width = 0
      .Column(GRID_COLUMN_DETAILS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Point
      .Column(GRID_COLUMN_POINTS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(818)
      .Column(GRID_COLUMN_POINTS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(350)
      .Column(GRID_COLUMN_POINTS).Width = 1600
      .Column(GRID_COLUMN_POINTS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Column are fixed in a specific width when printed
      .Column(GRID_COLUMN_POINTS).ExcelWidth = GRID_WIDTH_EXCEL_POINTS

      ' Cash-in
      .Column(GRID_COLUMN_CASH_IN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(818)
      .Column(GRID_COLUMN_CASH_IN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(233)
      .Column(GRID_COLUMN_CASH_IN).Width = 1600
      .Column(GRID_COLUMN_CASH_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Column are fixed in a specific width when printed
      .Column(GRID_COLUMN_CASH_IN).ExcelWidth = GRID_WIDTH_EXCEL_CASH_IN

      ' Line type.
      .Column(GRID_COLUMN_UNIQUE).Header(0).Text = " "
      .Column(GRID_COLUMN_UNIQUE).Header(1).Text = " "
      .Column(GRID_COLUMN_UNIQUE).Width = 0

      ' Credit Type
      .Column(GRID_COLUMN_CREDIT_TYPE).Header(0).Text = ""
      .Column(GRID_COLUMN_CREDIT_TYPE).Header(1).Text = ""
      .Column(GRID_COLUMN_CREDIT_TYPE).Width = 0

    End With

    ' Add account columns
    If Me.chk_view_data_account.Checked Then
      Call mdl_account_for_report.StyleSheetHolderData(Grid, GRID_COLUMNS, , chk_view_data_account.Checked)
    End If

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Dim _closing_time As Integer
    Dim _final_time As Date

    ' uc date control
    _final_time = WSI.Common.Misc.TodayOpening()
    _closing_time = _final_time.Hour

    Me.uc_dsl.FromDate = _final_time.AddDays(1 - _final_time.Day)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.ToDate = _final_time.AddDays(1)
    Me.uc_dsl.ToDateSelected = False
    Me.uc_dsl.ClosingTime = _closing_time

    ' Order
    Me.opt_order_prom_session_user.Checked = True

    ' Status
    If (WSI.Common.Misc.IsFeatureTerminalDrawEnabled() And StatusVisibleForGeneralParam()) Then
      Me.opt_status_all.Checked = True
    End If

    'caption 
    'Important! set order (opt_order_prom_session_user or opt_order_session_user_prom ) before the call
    Call SetCaptionValues()

    ' Promotion type.
    Me.opt_promo_manual.Checked = True

    ' Promotion filter 
    Me.ef_promo_name.Value = ""

    ' Details
    Me.chk_details.Checked = True
    ' Show account data 
    Me.chk_view_data_account.Checked = False
    Me.chk_view_data_account.Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_SHOW_HOLDER_DATA).Read

    ' Account filter
    Me.uc_account_sel.Clear()

    ' Promotion Credit Type
    Me.chk_credit_type_points.Checked = False
    Me.chk_credit_type_redeem.Checked = False
    Me.chk_credit_type_non_redeem.Checked = False

    ' Users
    Me.opt_all_users.Checked = True
    Me.cmb_user.Enabled = False
    Me.chk_show_all.Checked = False
    Me.chk_show_all.Enabled = False

    ' Promotion Categories
    Me.cmb_categories.Value = -1

  End Sub ' SetDefaultValues

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the
  '          main SQL Query.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String
    Dim _str_sql_where As String
    Dim _str_where_account As String
    Dim _str_where_credit_type As String
    Dim _filter_condition As String

    _str_sql_where = ""
    _str_where_credit_type = ""
    _filter_condition = ""

    'Only User Accounts 
    If m_only_user_accounts Then
      _str_sql_where &= " AND AC_TYPE NOT IN (" & AccountType.ACCOUNT_VIRTUAL_CASHIER & ", " & AccountType.ACCOUNT_VIRTUAL_TERMINAL & ")"
    End If

    ' Filter dates
    _filter_condition = Me.uc_dsl.GetSqlFilterCondition("ACP_ACTIVATION")

    If _filter_condition.Length > 0 Then
      _str_sql_where = _str_sql_where & " AND " & _filter_condition
    End If

    ' Filter promotion type
    If Not Me.opt_promo_all.Checked Then
      '  Promo manual
      If Me.opt_promo_manual.Checked Then
        _str_sql_where = _str_sql_where & " AND ACP_PROMO_TYPE = " & Promotion.PROMOTION_TYPE.MANUAL
      End If
      '  Promo system periodic
      If Me.opt_promo_system_period.Checked Then
        _str_sql_where = _str_sql_where & " AND ACP_PROMO_TYPE IN (" & Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL & _
                                              " , " & Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL_AND_MIN_PLAYED & _
                                              " , " & Promotion.PROMOTION_TYPE.PERIODIC_PER_BIRTHDAY & " )"
      End If
      '  Promo system automatic
      If Me.opt_promo_system_auto.Checked Then
        _str_sql_where = _str_sql_where & " AND ACP_PROMO_TYPE IN (" & Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_COVER_COUPON & _
                                                      " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_PER_PRIZE & _
                                                      " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_PRIZE_PAYOUT & _
                                                      " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_PRIZE_PAYOUT_AND_RECHARGE & _
                                                      " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_WITH_CURRENCY_EXCHANGE & _
                                                      " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_PRIZE_COUPON & _
                                                      " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_LOSER_NR & _
                                                      " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_LOSER_RE & _
                                                      " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_NR & _
                                                      " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_RE & _
                                                      " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_PLAYER_SAID_NO & _
                                                      " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_SERVER_DOWN & _
                                                      " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_NR2_IN_KIND & _
                                                      " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_LOSER_NR2_IN_KIND & _
                                                      " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_RE_IN_KIND & _
                                                      " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_BUCKET_NR_CREDIT & _
                                                      " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_BUCKET_RE_CREDIT & " )"

      End If
      '  Promo per points
      If Me.opt_promo_per_points.Checked Then
        _str_sql_where = _str_sql_where & " AND ACP_PROMO_TYPE IN ( " & Promotion.PROMOTION_TYPE.PER_POINTS_NOT_REDEEMABLE & _
                                          " , " & Promotion.PROMOTION_TYPE.PER_POINTS_REDEEMABLE & " ) "
      End If

      ' Promo preassigned
      If Me.opt_promo_preassigned.Checked Then
        _str_sql_where = _str_sql_where & " AND ACP_PROMO_TYPE = " & Promotion.PROMOTION_TYPE.PREASSIGNED
      End If
    Else
      _str_sql_where = _str_sql_where & " AND ACP_PROMO_TYPE NOT  IN (" & Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_COVER_COUPON & _
                                                                    " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_WITH_CURRENCY_EXCHANGE & _
                                                                    " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_PRIZE_COUPON & _
                                                                    " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_LOSER_NR & _
                                                                    " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_LOSER_RE & _
                                                                    " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_NR & _
                                                                    " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_RE & _
                                                                    " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_PLAYER_SAID_NO & _
                                                                    " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_SERVER_DOWN & _
                                                                    " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_NR2_IN_KIND & _
                                                                    " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_LOSER_NR2_IN_KIND & _
                                                                    " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_PLAY_SESSION_WINNER_NR_HIDDEN & _
                                                                    " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_RE_IN_KIND & _
                                                                    " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_BUCKET_NR_CREDIT & _
                                                                    " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_BUCKET_RE_CREDIT & " )"
    End If 'end opt_promo_all

    _str_where_account = Me.uc_account_sel.GetFilterSQL()
    If _str_where_account <> "" Then
      _str_sql_where = _str_sql_where & " AND " & _
                  " ACP_ACCOUNT_ID IN (SELECT AC_ACCOUNT_ID FROM ACCOUNTS WHERE " & _str_where_account & ") "
    End If

    ' Filter Promo Name
    If Me.ef_promo_name.Value <> "" Then
      _str_sql_where = _str_sql_where & " AND (" & GUI_FilterField("ACP_PROMO_NAME", Me.ef_promo_name.Value, False, False, True) & ") "
    End If

    ' Filter Credit Type
    If Me.chk_credit_type_points.Checked Then
      _str_where_credit_type = ACCOUNT_PROMO_CREDIT_TYPE.POINT
    End If
    If Me.chk_credit_type_redeem.Checked Then
      If Not String.IsNullOrEmpty(_str_where_credit_type) Then
        _str_where_credit_type = _str_where_credit_type & ","
      End If
      _str_where_credit_type = _str_where_credit_type & " " & ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE
    End If
    If Me.chk_credit_type_non_redeem.Checked Then
      If Not String.IsNullOrEmpty(_str_where_credit_type) Then
        _str_where_credit_type = _str_where_credit_type & ","
      End If
      _str_where_credit_type = _str_where_credit_type & " " & ACCOUNT_PROMO_CREDIT_TYPE.NR1 & ", " & ACCOUNT_PROMO_CREDIT_TYPE.NR2
    End If
    If Not String.IsNullOrEmpty(_str_where_credit_type) Then
      _str_where_credit_type = " ACP_CREDIT_TYPE IN ( " & _str_where_credit_type & ")"
      If Not String.IsNullOrEmpty(_str_sql_where) Then
        _str_sql_where = _str_sql_where & " AND "
      End If
      _str_sql_where = _str_sql_where & _str_where_credit_type
    End If

    ' Filter User
    If Me.opt_one_user.Checked = True Then
      _str_sql_where = _str_sql_where & " AND (ISNULL(ISNULL(AO_USER_ID, CS_USER_ID), -1) = " & Me.cmb_user.Value & ") "
    End If

    ' Filter Promo Category
    If Not cmb_categories.Value = -1 Then
      _str_sql_where = _str_sql_where & " AND ACP_PROMO_CATEGORY_ID = " & cmb_categories.Value
    End If

    If _str_sql_where <> "" Then
      _str_sql_where = Strings.Right(_str_sql_where, Len(_str_sql_where) - 5)
      _str_sql_where = " WHERE " & _str_sql_where
    End If

    'Filter Promo Status
    If (WSI.Common.Misc.IsFeatureTerminalDrawEnabled() And StatusVisibleForGeneralParam()) Then
      If Me.opt_status_all.Checked = True Then
        _str_sql_where = _str_sql_where & " AND ACP_STATUS  IN (" & ACCOUNT_PROMO_STATUS.ACTIVE & _
                                                            " , " & ACCOUNT_PROMO_STATUS.AWARDED & _
                                                            " , " & ACCOUNT_PROMO_STATUS.BOUGHT & _
                                                            " , " & ACCOUNT_PROMO_STATUS.CANCELLED & _
                                                            " , " & ACCOUNT_PROMO_STATUS.CANCELLED_BY_PLAYER & _
                                                            " , " & ACCOUNT_PROMO_STATUS.CANCELLED_NOT_ADDED & _
                                                            " , " & ACCOUNT_PROMO_STATUS.ERROR & _
                                                            " , " & ACCOUNT_PROMO_STATUS.EXHAUSTED & _
                                                            " , " & ACCOUNT_PROMO_STATUS.EXPIRED & _
                                                            " , " & ACCOUNT_PROMO_STATUS.PENDING_DRAW & _
                                                            " , " & ACCOUNT_PROMO_STATUS.NOT_AWARDED & _
                                                            " , " & ACCOUNT_PROMO_STATUS.PREASSIGNED & _
                                                            " , " & ACCOUNT_PROMO_STATUS.PENDING_PLAYER & _
                                                            " , " & ACCOUNT_PROMO_STATUS.REDEEMED & _
                                                            " , " & ACCOUNT_PROMO_STATUS.UNKNOWN & " )"
      End If

      If Me.opt_status_bought.Checked = True Then
        _str_sql_where = _str_sql_where & " AND ACP_STATUS = " & ACCOUNT_PROMO_STATUS.BOUGHT
      End If

      If Me.opt_status_pending_draw.Checked = True Then
        _str_sql_where = _str_sql_where & " AND ACP_STATUS = " & ACCOUNT_PROMO_STATUS.PENDING_DRAW
      End If

      If Me.opt_status_pending_player.Checked = True Then
        _str_sql_where = _str_sql_where & " AND ACP_STATUS = " & ACCOUNT_PROMO_STATUS.PENDING_PLAYER
      End If
    End If

    Return _str_sql_where

  End Function ' GetSqlWhere

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the
  '           temporal table.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhereUserData() As String
    Dim _str_sql_where As String

    _str_sql_where = ""

    ' Filter dates user date
    _str_sql_where = Me.uc_dsl.GetSqlFilterCondition("AO_DATETIME")
    If _str_sql_where.Length > 0 Then
      _str_sql_where = " WHERE  " & _str_sql_where
    End If

    Return _str_sql_where

  End Function ' GetSqlWhereUserData

  ' PURPOSE: Initialization the subtotals.
  '
  '  PARAMS:
  '     - INPUT :
  '               - TypeSubtotal 
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Private Sub InitSubtotalValues(ByVal TypeSubtotal As Integer)

    'always initialize subtotal1
    m_subtotal1_cash_in = 0
    m_subtotal1_promo_points = 0
    m_subtotal1_redim = 0
    m_subtotal1_noredim = 0
    m_subtotal1_played = 0
    m_subtotal1_balance = 0
    m_subtotal1_points = 0
    m_subtotal1_won = 0

    If TypeSubtotal = 2 Then
      m_subtotal2_cash_in = 0
      m_subtotal2_promo_points = 0
      m_subtotal2_redim = 0
      m_subtotal2_noredim = 0
      m_subtotal2_played = 0
      m_subtotal2_balance = 0
      m_subtotal2_points = 0
      m_subtotal2_won = 0
    End If

  End Sub ' InitSubtotalValues


  Private Sub SetCaptionValues()
    If Me.opt_order_session_user_prom.Checked Then
      Me.lbl_caption_subtotal1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4412)  ' Subtotal User
      Me.lbl_caption_subtotal2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4410)  ' Subtotal Cashier Sesion

    ElseIf Me.opt_order_prom_session_user.Checked Then
      Me.lbl_caption_subtotal1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4410)  ' Subtotal Cashier Sesion
      Me.lbl_caption_subtotal2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4411)  ' Subtotal Promotion

    ElseIf Me.opt_order_account_day.Checked Then
      Me.lbl_caption_subtotal1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4413)   ' Subtotal Day
      Me.lbl_caption_subtotal2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4414)   ' Subtotal Account

    End If
  End Sub ' SetCaptionValues

  ' PURPOSE: Load grid without details filter
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub LoadGridWithoutDetails()

    Dim _table As DataTable = Nothing
    Dim _table_details As DataTable = Nothing
    Dim _new_row_table_details As DataRow = Nothing
    Dim _num_row As Integer = 0
    Dim _count_aux As Decimal
    Dim _filter_select As String

    _filter_select = String.Empty
    _count_aux = 0
    _table = GUI_GetTableUsingSQL(GUI_FilterGetSqlQuery(), Integer.MaxValue)

    _table_details = _table.Copy
    _table_details.Rows.Clear()

    For Each tableRow As DataRow In _table.Rows

      If tableRow(SQL_COLUMN_UD_CASHIER_SESSION_NAME) Is DBNull.Value Then
        _filter_select = String.Format(" ACP_PROMO_NAME = '{0}' ", tableRow(SQL_COLUMN_PROMOTION_NAME))
      Else
        _filter_select = String.Format(" ACP_PROMO_NAME = '{0}' and CS_NAME = '{1}' ", tableRow(SQL_COLUMN_PROMOTION_NAME), tableRow(SQL_COLUMN_UD_CASHIER_SESSION_NAME))
      End If

      If _table_details.Select(_filter_select).Length = 0 Then
        If _table_details.Rows.Count > 0 Then
          If _table_details.Select(String.Format(" ACP_PROMO_NAME = '{0}' ", tableRow(SQL_COLUMN_PROMOTION_NAME))).Length = 0 Then
            'Subtotal
            _new_row_table_details = _table_details.NewRow
            AddSubTotal(_new_row_table_details, _table, _table_details, _num_row)
            _table_details.Rows.Add(_new_row_table_details)
          End If
        End If

        'Group sessions
        _new_row_table_details = _table_details.NewRow
        AddGroupRows(_new_row_table_details, _table, tableRow)
        _table_details.Rows.Add(_new_row_table_details)

      End If
      _num_row += 1
    Next

    If _table_details.Rows.Count > 0 Then
      'last Subtotal
      _new_row_table_details = _table_details.NewRow
      AddSubTotal(_new_row_table_details, _table, _table_details, _table.Rows.Count - 1)
      _table_details.Rows.Add(_new_row_table_details)
    End If

    AddRowsInGrid(_table, _table_details)

    'Enable buttons
    Me.GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_EXCEL_ENABLED).Read
    Me.GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_PRINT_ENABLED).Read

  End Sub

  ' PURPOSE: Add row group 
  '
  '  PARAMS:
  '     - INPUT:
  '           - _new_row_table_details: new row from new table
  '           - _table: get table from SQL
  '           - _table_row: original row 
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub AddGroupRows(ByRef _new_row_table_details As DataRow, ByRef _table As DataTable, ByRef _table_row As DataRow)

    Dim _count_aux As Decimal
    Dim _filter_select As String
    Dim _array_subgroup() As DataRow

    'EOR 29-MAY-2017
    Try
      _count_aux = 0
      _new_row_table_details(SQL_COLUMN_PROMOTION_NAME) = _table_row(SQL_COLUMN_PROMOTION_NAME)
      If _table_row(SQL_COLUMN_UD_CASHIER_SESSION_NAME) Is DBNull.Value Then
        _filter_select = String.Format(" ACP_PROMO_NAME = '{0}' ", _table_row(SQL_COLUMN_PROMOTION_NAME))
      Else
        _filter_select = String.Format(" ACP_PROMO_NAME = '{0}' and CS_NAME = '{1}' ", _table_row(SQL_COLUMN_PROMOTION_NAME), _table_row(SQL_COLUMN_UD_CASHIER_SESSION_NAME))
      End If

      _array_subgroup = _table.Select(_filter_select)

      If _array_subgroup.Length > 0 Then
        _new_row_table_details(SQL_COLUMN_PROMO_CATEGORY) = _array_subgroup(_array_subgroup.Length - 1)(SQL_COLUMN_PROMO_CATEGORY)
      End If

      _count_aux = 0
      For Each m As DataRow In _array_subgroup
        _count_aux += IIf(IsDBNull(m(SQL_COLUMN_INIT_BALANCE)), 0, m(SQL_COLUMN_INIT_BALANCE))
      Next

      _new_row_table_details(SQL_COLUMN_INIT_BALANCE) = _count_aux
      _new_row_table_details(SQL_COLUMN_ACCOUNT_NAME) = _table.Select(_filter_select).Length
      _new_row_table_details(SQL_COLUMN_CREDIT_TYPE) = _table_row(SQL_COLUMN_CREDIT_TYPE)
      _new_row_table_details(SQL_COLUMN_UD_CASHIER_SESSION_NAME) = _table_row(SQL_COLUMN_UD_CASHIER_SESSION_NAME)

      _count_aux = 0
      For Each m As DataRow In _array_subgroup
        If m(SQL_COLUMN_CASH_IN).ToString <> String.Empty Then
          _count_aux += IIf(IsDBNull(m(SQL_COLUMN_CASH_IN)), 0, m(SQL_COLUMN_CASH_IN))
        End If
      Next
      If _count_aux > 0 Then
        _new_row_table_details(SQL_COLUMN_CASH_IN) = _count_aux
      End If

      _count_aux = 0
      For Each m As DataRow In _array_subgroup
        If m(SQL_COLUMN_ACP_POINT).ToString <> String.Empty Then
          _count_aux += IIf(IsDBNull(m(SQL_COLUMN_ACP_POINT)), 0, m(SQL_COLUMN_ACP_POINT))
        End If
      Next
      If _count_aux > 0 Then
        _new_row_table_details(SQL_COLUMN_ACP_POINT) = _count_aux
      End If

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try
  End Sub

  ' PURPOSE: Add row subtotal group 
  '
  '  PARAMS:
  '     - INPUT:
  '           - _new_row_table_details: new row from new table
  '           - _table: get table from SQL
  '           - _table_details: table destination
  '           - _num_row: num row from original row
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub AddSubTotal(ByRef _new_row_table_details As DataRow, ByRef _table As DataTable, ByRef _table_details As DataTable, ByVal _num_row As Integer)
    'Subtotal
    Dim _count_aux As Decimal
    Dim _array_subgroup() As DataRow

    'EOR 31-MAY-2017
    Dim _num_row_aux As Integer

    Try
      _count_aux = 0
      _num_row_aux = _num_row - 1

      If _num_row_aux < 0 Then
        _num_row_aux = 0
      End If

      _new_row_table_details(SQL_COLUMN_LEVEL) = 1 'Mark for subtotal row
      _new_row_table_details(SQL_COLUMN_ACCOUNT_NAME) = _table.Select(String.Format(" ACP_PROMO_NAME = '{0}' ", _table.Rows(_num_row_aux)(SQL_COLUMN_PROMOTION_NAME))).Length.ToString()
      _new_row_table_details(SQL_COLUMN_PROMOTION_NAME) = _table.Rows(_num_row_aux)(SQL_COLUMN_PROMOTION_NAME)
      _new_row_table_details(SQL_COLUMN_PROMO_CATEGORY) = _table.Rows(_num_row_aux)(SQL_COLUMN_PROMO_CATEGORY)
      _new_row_table_details(SQL_COLUMN_CREDIT_TYPE) = _table.Rows(_num_row_aux)(SQL_COLUMN_CREDIT_TYPE)

      _array_subgroup = _table_details.Select(String.Format(" ACP_PROMO_NAME = '{0}' ", _table.Rows(_num_row_aux)(SQL_COLUMN_PROMOTION_NAME)))
      _count_aux = 0
      For Each m As DataRow In _array_subgroup
        _count_aux += IIf(IsDBNull(m(SQL_COLUMN_INIT_BALANCE)), 0, m(SQL_COLUMN_INIT_BALANCE))
      Next

      _new_row_table_details(SQL_COLUMN_INIT_BALANCE) = _count_aux

      _count_aux = 0
      For Each m As DataRow In _array_subgroup
        If m(SQL_COLUMN_CASH_IN).ToString <> String.Empty Then
          _count_aux += IIf(IsDBNull(m(SQL_COLUMN_CASH_IN)), 0, m(SQL_COLUMN_CASH_IN))
        End If
      Next

      If _count_aux > 0 Then
        _new_row_table_details(SQL_COLUMN_CASH_IN) = _count_aux
      End If

      _count_aux = 0
      For Each m As DataRow In _array_subgroup
        If m(SQL_COLUMN_ACP_POINT).ToString <> String.Empty Then
          _count_aux += IIf(IsDBNull(m(SQL_COLUMN_ACP_POINT)), 0, m(SQL_COLUMN_ACP_POINT))
        End If
      Next
      If _count_aux > 0 Then
        _new_row_table_details(SQL_COLUMN_ACP_POINT) = _count_aux
      End If
    Catch _ex As Exception
      Log.Exception(_ex)
    End Try
  End Sub

  ' PURPOSE: Add rows in grid and add total row
  '
  '  PARAMS:
  '     - INPUT:
  '           - _table: get table from SQL
  '           - _table_details: table destination
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub AddRowsInGrid(ByRef _table As DataTable, ByRef _table_details As DataTable)

    Dim _db_row As CLASS_DB_ROW
    Dim _num_row As Integer = 0

    'Add rows in grid
    For Each row As DataRow In _table_details.Rows
      _db_row = New CLASS_DB_ROW(row)
      Me.Grid.AddRow()
      _num_row = Me.Grid.NumRows - 1
      GUI_SetupRowWithoutDetails(_num_row, _db_row)
    Next

    'Add TOTAL row
    _num_row = Grid.NumRows

    Me.Grid.AddRow()

    m_num_rows_subtotal1 = _table.Rows.Count
    Me.Grid.Cell(_num_row, GRID_COLUMN_USER).Value = GLB_NLS_GUI_STATISTICS.GetString(203)
    Me.Grid.Cell(_num_row, GRID_COLUMN_USER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
    Me.Grid.Cell(_num_row, GRID_COLUMN_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1256, m_num_rows_subtotal1.ToString())
    Me.Grid.Cell(_num_row, GRID_COLUMN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    Me.Grid.Cell(_num_row, GRID_COLUMN_PROMO_POINTS).Value = IIf(m_subtotal1_promo_points > 0, GUI_FormatNumber(m_subtotal1_promo_points, 0), "")
    Me.Grid.Cell(_num_row, GRID_COLUMN_PROMO_REDIM).Value = IIf(m_subtotal1_redim > 0, GUI_FormatCurrency(m_subtotal1_redim, 2), "")
    Me.Grid.Cell(_num_row, GRID_COLUMN_PROMO_NON_REDIM).Value = IIf(m_subtotal1_noredim > 0, GUI_FormatCurrency(m_subtotal1_noredim, 2), "")
    Me.Grid.Cell(_num_row, GRID_COLUMN_CASH_IN).Value = GUI_FormatCurrency(m_total_cash_in, 2)
    Me.Grid.Cell(_num_row, GRID_COLUMN_POINTS).Value = GUI_FormatNumber(m_total_points, 0)

    Me.Grid.Row(_num_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

  End Sub

  Private Function CastPromoStatus(_status As ACCOUNT_PROMO_STATUS) As String

    Select Case _status
      Case ACCOUNT_PROMO_STATUS.ACTIVE
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(788)
      Case ACCOUNT_PROMO_STATUS.AWARDED
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(787)
      Case ACCOUNT_PROMO_STATUS.BOUGHT
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(8721)
      Case ACCOUNT_PROMO_STATUS.CANCELLED
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(1076)
      Case ACCOUNT_PROMO_STATUS.CANCELLED_BY_PLAYER
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(1077)
      Case ACCOUNT_PROMO_STATUS.CANCELLED_NOT_ADDED
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(1075)
      Case ACCOUNT_PROMO_STATUS.ERROR
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(8722)
      Case ACCOUNT_PROMO_STATUS.EXHAUSTED
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(789)
      Case ACCOUNT_PROMO_STATUS.EXPIRED
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(790)
      Case ACCOUNT_PROMO_STATUS.NOT_AWARDED
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(813)
      Case ACCOUNT_PROMO_STATUS.PREASSIGNED
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(8723)
      Case ACCOUNT_PROMO_STATUS.PENDING_PLAYER
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(8724)
      Case ACCOUNT_PROMO_STATUS.REDEEMED
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(791)
      Case ACCOUNT_PROMO_STATUS.UNKNOWN
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(786)
      Case ACCOUNT_PROMO_STATUS.PENDING_DRAW
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(8738)
    End Select

    Return String.Empty

  End Function

  Private Function StatusVisibleForGeneralParam() As Boolean

    Return WSI.Common.GeneralParam.GetBoolean("DisplayTouch", "AwardedPromotionsReport.ShowStatus", False)

  End Function



#End Region

#Region "EVENTS"
  Private Sub chk_show_unsubscribed_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_show_all.CheckedChanged
    If chk_show_all.Checked Then
      Call SetCombo(Me.cmb_user, "SELECT GU_USER_ID, GU_USERNAME FROM GUI_USERS ORDER BY GU_USERNAME")
    Else
      Call SetCombo(Me.cmb_user, "SELECT GU_USER_ID, GU_USERNAME FROM GUI_USERS WHERE GU_BLOCK_REASON = " & WSI.Common.GUI_USER_BLOCK_REASON.NONE & " ORDER BY GU_USERNAME")
    End If
  End Sub 'chk_show_unsubscribed

  Private Sub opt_all_users_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_all_users.CheckedChanged
    Me.cmb_user.Enabled = False
    Me.chk_show_all.Enabled = False
  End Sub ' opt_all_users_CheckedChanged

  Private Sub opt_one_user_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_one_user.CheckedChanged
    Me.cmb_user.Enabled = True
    Me.chk_show_all.Enabled = True
  End Sub ' opt_one_user_CheckedChanged

  Private Sub chk_details_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_details.CheckedChanged
    chk_view_data_account.Enabled = chk_details.Checked AndAlso CurrentUser.Permissions(ENUM_FORM.FORM_SHOW_HOLDER_DATA).Read
    If Not chk_details.Checked Then
      chk_view_data_account.Checked = False
    End If

  End Sub ' chk_details_CheckedChanged

  Private Sub opt_order_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_order_prom_session_user.CheckedChanged _
                                                                                                          , opt_order_prom_session_user.CheckedChanged _
                                                                                                          , opt_order_account_day.CheckedChanged
    Call SetCaptionValues()
  End Sub ' opt_order_CheckedChanged

#End Region

End Class