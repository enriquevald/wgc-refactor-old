﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_lottery_meters_adjustment_terminals
  Inherits GUI_Controls.frm_base_sel_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_site_name = New System.Windows.Forms.GroupBox()
    Me.uc_entry_site = New GUI_Controls.uc_entry_field()
    Me.gb_working_day = New System.Windows.Forms.GroupBox()
    Me.uc_working_day = New GUI_Controls.uc_date_picker()
    Me.gb_credit_currency = New System.Windows.Forms.GroupBox()
    Me.opt_Money = New System.Windows.Forms.RadioButton()
    Me.opt_Credit = New System.Windows.Forms.RadioButton()
    Me.chk_show_only_with_anomalies = New System.Windows.Forms.CheckBox()
    Me.panel_totals = New System.Windows.Forms.Panel()
    Me.lbl_jackpot_caption = New System.Windows.Forms.Label()
    Me.lbl_coin_out_caption = New System.Windows.Forms.Label()
    Me.lbl_coin_in_caption = New System.Windows.Forms.Label()
    Me.lbl_gain_caption = New System.Windows.Forms.Label()
    Me.lblTotalJackPot = New System.Windows.Forms.Label()
    Me.lblTotalCoinOut = New System.Windows.Forms.Label()
    Me.lblTotalCoinIn = New System.Windows.Forms.Label()
    Me.lblTotalGain = New System.Windows.Forms.Label()
    Me.lblTotales = New System.Windows.Forms.Label()
    Me.gb_status = New System.Windows.Forms.GroupBox()
    Me.lbl_status = New System.Windows.Forms.Label()
    Me.chk_show_increments = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_site_name.SuspendLayout()
    Me.gb_working_day.SuspendLayout()
    Me.gb_credit_currency.SuspendLayout()
    Me.panel_totals.SuspendLayout()
    Me.gb_status.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.chk_show_increments)
    Me.panel_filter.Controls.Add(Me.gb_status)
    Me.panel_filter.Controls.Add(Me.panel_totals)
    Me.panel_filter.Controls.Add(Me.chk_show_only_with_anomalies)
    Me.panel_filter.Controls.Add(Me.gb_credit_currency)
    Me.panel_filter.Controls.Add(Me.gb_working_day)
    Me.panel_filter.Controls.Add(Me.gb_site_name)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1292, 146)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_site_name, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_working_day, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_credit_currency, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_show_only_with_anomalies, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.panel_totals, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_status, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_show_increments, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 150)
    Me.panel_data.Size = New System.Drawing.Size(1292, 509)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1286, 10)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1286, 5)
    '
    'gb_site_name
    '
    Me.gb_site_name.Controls.Add(Me.uc_entry_site)
    Me.gb_site_name.Location = New System.Drawing.Point(6, 8)
    Me.gb_site_name.Name = "gb_site_name"
    Me.gb_site_name.Size = New System.Drawing.Size(285, 60)
    Me.gb_site_name.TabIndex = 0
    Me.gb_site_name.TabStop = False
    Me.gb_site_name.Text = "xSite"
    '
    'uc_entry_site
    '
    Me.uc_entry_site.DoubleValue = 0.0R
    Me.uc_entry_site.Enabled = False
    Me.uc_entry_site.IntegerValue = 0
    Me.uc_entry_site.IsReadOnly = False
    Me.uc_entry_site.Location = New System.Drawing.Point(6, 20)
    Me.uc_entry_site.Name = "uc_entry_site"
    Me.uc_entry_site.PlaceHolder = Nothing
    Me.uc_entry_site.Size = New System.Drawing.Size(273, 24)
    Me.uc_entry_site.SufixText = "Sufix Text"
    Me.uc_entry_site.SufixTextVisible = True
    Me.uc_entry_site.TabIndex = 0
    Me.uc_entry_site.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_entry_site.TextValue = ""
    Me.uc_entry_site.TextWidth = 50
    Me.uc_entry_site.Value = ""
    Me.uc_entry_site.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_working_day
    '
    Me.gb_working_day.Controls.Add(Me.uc_working_day)
    Me.gb_working_day.Location = New System.Drawing.Point(297, 8)
    Me.gb_working_day.Name = "gb_working_day"
    Me.gb_working_day.Size = New System.Drawing.Size(225, 60)
    Me.gb_working_day.TabIndex = 1
    Me.gb_working_day.TabStop = False
    Me.gb_working_day.Text = "xWorkingDay"
    '
    'uc_working_day
    '
    Me.uc_working_day.Checked = True
    Me.uc_working_day.IsReadOnly = True
    Me.uc_working_day.Location = New System.Drawing.Point(6, 20)
    Me.uc_working_day.Name = "uc_working_day"
    Me.uc_working_day.ShowCheckBox = False
    Me.uc_working_day.ShowUpDown = False
    Me.uc_working_day.Size = New System.Drawing.Size(200, 24)
    Me.uc_working_day.SufixText = "Sufix Text"
    Me.uc_working_day.SufixTextVisible = True
    Me.uc_working_day.TabIndex = 0
    Me.uc_working_day.Value = New Date(2017, 12, 18, 0, 0, 0, 0)
    '
    'gb_credit_currency
    '
    Me.gb_credit_currency.Controls.Add(Me.opt_Money)
    Me.gb_credit_currency.Controls.Add(Me.opt_Credit)
    Me.gb_credit_currency.Location = New System.Drawing.Point(720, 8)
    Me.gb_credit_currency.Name = "gb_credit_currency"
    Me.gb_credit_currency.Size = New System.Drawing.Size(200, 60)
    Me.gb_credit_currency.TabIndex = 3
    Me.gb_credit_currency.TabStop = False
    Me.gb_credit_currency.Text = "xShowCreditMoney"
    '
    'opt_Money
    '
    Me.opt_Money.AutoSize = True
    Me.opt_Money.Location = New System.Drawing.Point(113, 27)
    Me.opt_Money.Name = "opt_Money"
    Me.opt_Money.Size = New System.Drawing.Size(69, 17)
    Me.opt_Money.TabIndex = 1
    Me.opt_Money.TabStop = True
    Me.opt_Money.Text = "xMoney"
    Me.opt_Money.UseVisualStyleBackColor = True
    '
    'opt_Credit
    '
    Me.opt_Credit.AutoSize = True
    Me.opt_Credit.Location = New System.Drawing.Point(15, 27)
    Me.opt_Credit.Name = "opt_Credit"
    Me.opt_Credit.Size = New System.Drawing.Size(67, 17)
    Me.opt_Credit.TabIndex = 0
    Me.opt_Credit.TabStop = True
    Me.opt_Credit.Text = "xCredit"
    Me.opt_Credit.UseVisualStyleBackColor = True
    '
    'chk_show_only_with_anomalies
    '
    Me.chk_show_only_with_anomalies.AutoSize = True
    Me.chk_show_only_with_anomalies.Location = New System.Drawing.Point(6, 73)
    Me.chk_show_only_with_anomalies.Name = "chk_show_only_with_anomalies"
    Me.chk_show_only_with_anomalies.Size = New System.Drawing.Size(226, 17)
    Me.chk_show_only_with_anomalies.TabIndex = 4
    Me.chk_show_only_with_anomalies.Text = "xShowOnlyMachinesWithAnomalies"
    Me.chk_show_only_with_anomalies.UseVisualStyleBackColor = True
    '
    'panel_totals
    '
    Me.panel_totals.Controls.Add(Me.lbl_jackpot_caption)
    Me.panel_totals.Controls.Add(Me.lbl_coin_out_caption)
    Me.panel_totals.Controls.Add(Me.lbl_coin_in_caption)
    Me.panel_totals.Controls.Add(Me.lbl_gain_caption)
    Me.panel_totals.Controls.Add(Me.lblTotalJackPot)
    Me.panel_totals.Controls.Add(Me.lblTotalCoinOut)
    Me.panel_totals.Controls.Add(Me.lblTotalCoinIn)
    Me.panel_totals.Controls.Add(Me.lblTotalGain)
    Me.panel_totals.Controls.Add(Me.lblTotales)
    Me.panel_totals.Location = New System.Drawing.Point(195, 94)
    Me.panel_totals.Name = "panel_totals"
    Me.panel_totals.Size = New System.Drawing.Size(632, 52)
    Me.panel_totals.TabIndex = 20
    '
    'lbl_jackpot_caption
    '
    Me.lbl_jackpot_caption.AutoSize = True
    Me.lbl_jackpot_caption.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_jackpot_caption.Location = New System.Drawing.Point(482, 6)
    Me.lbl_jackpot_caption.Name = "lbl_jackpot_caption"
    Me.lbl_jackpot_caption.Size = New System.Drawing.Size(65, 13)
    Me.lbl_jackpot_caption.TabIndex = 28
    Me.lbl_jackpot_caption.Text = "xJackpot"
    Me.lbl_jackpot_caption.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'lbl_coin_out_caption
    '
    Me.lbl_coin_out_caption.AutoSize = True
    Me.lbl_coin_out_caption.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_coin_out_caption.Location = New System.Drawing.Point(347, 6)
    Me.lbl_coin_out_caption.Name = "lbl_coin_out_caption"
    Me.lbl_coin_out_caption.Size = New System.Drawing.Size(65, 13)
    Me.lbl_coin_out_caption.TabIndex = 27
    Me.lbl_coin_out_caption.Text = "xCoinOut"
    Me.lbl_coin_out_caption.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'lbl_coin_in_caption
    '
    Me.lbl_coin_in_caption.AutoSize = True
    Me.lbl_coin_in_caption.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_coin_in_caption.Location = New System.Drawing.Point(212, 6)
    Me.lbl_coin_in_caption.Name = "lbl_coin_in_caption"
    Me.lbl_coin_in_caption.Size = New System.Drawing.Size(57, 13)
    Me.lbl_coin_in_caption.TabIndex = 26
    Me.lbl_coin_in_caption.Text = "xCoinIn"
    Me.lbl_coin_in_caption.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'lbl_gain_caption
    '
    Me.lbl_gain_caption.AutoSize = True
    Me.lbl_gain_caption.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_gain_caption.Location = New System.Drawing.Point(77, 6)
    Me.lbl_gain_caption.Name = "lbl_gain_caption"
    Me.lbl_gain_caption.Size = New System.Drawing.Size(44, 13)
    Me.lbl_gain_caption.TabIndex = 25
    Me.lbl_gain_caption.Text = "xGain"
    Me.lbl_gain_caption.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'lblTotalJackPot
    '
    Me.lblTotalJackPot.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lblTotalJackPot.Location = New System.Drawing.Point(482, 25)
    Me.lblTotalJackPot.Name = "lblTotalJackPot"
    Me.lblTotalJackPot.Size = New System.Drawing.Size(130, 23)
    Me.lblTotalJackPot.TabIndex = 24
    Me.lblTotalJackPot.Text = "xTotal Jackpot"
    '
    'lblTotalCoinOut
    '
    Me.lblTotalCoinOut.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lblTotalCoinOut.Location = New System.Drawing.Point(347, 25)
    Me.lblTotalCoinOut.Name = "lblTotalCoinOut"
    Me.lblTotalCoinOut.Size = New System.Drawing.Size(130, 23)
    Me.lblTotalCoinOut.TabIndex = 23
    Me.lblTotalCoinOut.Text = "xTotal Coin Out"
    '
    'lblTotalCoinIn
    '
    Me.lblTotalCoinIn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lblTotalCoinIn.Location = New System.Drawing.Point(212, 25)
    Me.lblTotalCoinIn.Name = "lblTotalCoinIn"
    Me.lblTotalCoinIn.Size = New System.Drawing.Size(130, 23)
    Me.lblTotalCoinIn.TabIndex = 22
    Me.lblTotalCoinIn.Text = "xTotal Coin In"
    '
    'lblTotalGain
    '
    Me.lblTotalGain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lblTotalGain.Location = New System.Drawing.Point(77, 25)
    Me.lblTotalGain.Name = "lblTotalGain"
    Me.lblTotalGain.Size = New System.Drawing.Size(130, 23)
    Me.lblTotalGain.TabIndex = 21
    Me.lblTotalGain.Text = "xTotal Ganancia"
    '
    'lblTotales
    '
    Me.lblTotales.AutoSize = True
    Me.lblTotales.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblTotales.Location = New System.Drawing.Point(4, 30)
    Me.lblTotales.Name = "lblTotales"
    Me.lblTotales.Size = New System.Drawing.Size(72, 13)
    Me.lblTotales.TabIndex = 20
    Me.lblTotales.Text = "xTOTALES"
    '
    'gb_status
    '
    Me.gb_status.Controls.Add(Me.lbl_status)
    Me.gb_status.Location = New System.Drawing.Point(529, 8)
    Me.gb_status.Name = "gb_status"
    Me.gb_status.Size = New System.Drawing.Size(185, 60)
    Me.gb_status.TabIndex = 2
    Me.gb_status.TabStop = False
    Me.gb_status.Text = "xStatus"
    '
    'lbl_status
    '
    Me.lbl_status.AutoSize = True
    Me.lbl_status.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_status.Location = New System.Drawing.Point(25, 25)
    Me.lbl_status.Name = "lbl_status"
    Me.lbl_status.Size = New System.Drawing.Size(135, 13)
    Me.lbl_status.TabIndex = 15
    Me.lbl_status.Text = "xWorkingDayStatus"
    Me.lbl_status.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'chk_show_increments
    '
    Me.chk_show_increments.AutoSize = True
    Me.chk_show_increments.Location = New System.Drawing.Point(6, 96)
    Me.chk_show_increments.Name = "chk_show_increments"
    Me.chk_show_increments.Size = New System.Drawing.Size(226, 17)
    Me.chk_show_increments.TabIndex = 21
    Me.chk_show_increments.Text = "xShowOnlyMachinesWithAnomalies"
    Me.chk_show_increments.UseVisualStyleBackColor = True
    '
    'frm_lottery_meters_adjustment_terminals
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1302, 663)
    Me.Name = "frm_lottery_meters_adjustment_terminals"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_lottery_meters_adjustment_terminals"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_site_name.ResumeLayout(False)
    Me.gb_working_day.ResumeLayout(False)
    Me.gb_credit_currency.ResumeLayout(False)
    Me.gb_credit_currency.PerformLayout()
    Me.panel_totals.ResumeLayout(False)
    Me.panel_totals.PerformLayout()
    Me.gb_status.ResumeLayout(False)
    Me.gb_status.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents chk_show_only_with_anomalies As System.Windows.Forms.CheckBox
  Friend WithEvents gb_credit_currency As System.Windows.Forms.GroupBox
  Friend WithEvents opt_Money As System.Windows.Forms.RadioButton
  Friend WithEvents opt_Credit As System.Windows.Forms.RadioButton
  Friend WithEvents gb_working_day As System.Windows.Forms.GroupBox
  Friend WithEvents uc_working_day As GUI_Controls.uc_date_picker
  Friend WithEvents gb_site_name As System.Windows.Forms.GroupBox
  Friend WithEvents uc_entry_site As GUI_Controls.uc_entry_field
  Friend WithEvents panel_totals As System.Windows.Forms.Panel
  Friend WithEvents lblTotalJackPot As System.Windows.Forms.Label
  Friend WithEvents lblTotalCoinOut As System.Windows.Forms.Label
  Friend WithEvents lblTotalCoinIn As System.Windows.Forms.Label
  Friend WithEvents lblTotalGain As System.Windows.Forms.Label
  Friend WithEvents lblTotales As System.Windows.Forms.Label
  Friend WithEvents lbl_jackpot_caption As System.Windows.Forms.Label
  Friend WithEvents lbl_coin_out_caption As System.Windows.Forms.Label
  Friend WithEvents lbl_coin_in_caption As System.Windows.Forms.Label
  Friend WithEvents lbl_gain_caption As System.Windows.Forms.Label
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_status As System.Windows.Forms.Label
  Friend WithEvents chk_show_increments As System.Windows.Forms.CheckBox

End Class
