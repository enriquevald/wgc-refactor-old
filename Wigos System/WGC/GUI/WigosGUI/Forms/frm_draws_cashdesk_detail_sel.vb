'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_draws_cashdesk_detail_sel
' DESCRIPTION:   Cash desk draws detailed report
' AUTHOR:        Marcos Mohedano
' CREATION DATE: 02-JUL-2013
'
' REVISION HISTORY:
' 
' Date         Author Description
' -----------  ------ --------------------------------------------------------------------------------------------------------------------
' 02-JUL-2013  MMG    Initial version
' 12-AUG-2013  DMR    Added ShowSelectedItem function
' 13-AUG-2013  DMR    SetDefaultValues modified
' 19-AUG-2013  MMG    Modified form controls TabIndex order
' 23-AUG-2013  MMG    Modified ExecuteQueryCustom function (
'                     Changed acces to 'CashierVoucherType.CashIn_B'voucher type by 'CashierVoucherType.Notset')
'
' 26-SEP-2013  MMG    Added Override GUI_ReportParams to format correctly columns 
'                     GRID_COLUMN_COMBINATON_BET and GRID_COLUMN_COMBINATON_WON
' 21-NOV-2013  JPJ    Defect WIG-421 Exception overflow due to incorrect variable type.    
' 25-NOV-2013  MMG    Added Site ID and Draw ID format verifications in GUI_FilterCheck function.
' 06-MAY-2014  JBP    Added Vip client filter at reports.
' 07-MAY-2014  DHA    Fixed Bug WIG-900: timeout due to virtual accounts filter
' 04-OCT-2016  ESE    PBI 17687: Columns added to the draws report
' 31-OCT-2016  LTC    Bug 19228:WigosGUI: Wrong values  for looser on cash desk draw report
' 23-NOV-2016  ETP    Bug 20355: Wrong format of columns Redimible won and not redimible won.
'------------------------------------------------------------------------------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports WSI.Common
Imports System.Data.SqlClient


Public Class frm_draws_cashdesk_detail_sel
  Inherits GUI_Controls.frm_base_sel

#Region " Constants "

  Private Const FORM_DB_MIN_VERSION As Short = 149
  ' DB Columns 
  Dim SQL_COLUMN_SITE_ID As Integer
  Dim SQL_COLUMN_DRAW_ID As Integer
  Dim SQL_COLUMN_DRAW_DATETIME As Integer
  Dim SQL_COLUMN_DRAW_TERMINAL_TYPE As Integer
  Dim SQL_COLUMN_CASHIER_NAME As Integer
  Dim SQL_COLUMN_CASHIER_USERNAME As Integer
  Dim SQL_COLUMN_OPERATION_ID As Integer
  Dim SQL_COLUMN_OPERATION_DATETIME As Integer
  Dim SQL_COLUMN_ACCOUNT_ID As Integer
  Dim SQL_COLUMN_ACCOUNT_NAME As Integer
  Dim SQL_COLUMN_COMBINATON_BET As Integer
  Dim SQL_COLUMN_COMBINATON_WON As Integer
  Dim SQL_COLUMN_REDIMIBLE_BET As Integer
  Dim SQL_COLUMN_REDIMIBLE_WON As Integer
  Dim SQL_COLUMN_NONREDIMIBLE_WON As Integer
  Dim SQL_COLUMN_PRIZE_ID As Integer
  Dim SQL_COLUMN_CT_NAME As Integer

  ' Grid Columns
  Dim GRID_COLUMN_INDEX As Integer
  Dim GRID_COLUMN_SITE_ID As Integer
  Dim GRID_COLUMN_DRAW_ID As Integer
  Dim GRID_COLUMN_DRAW_DATETIME As Integer
  Dim GRID_COLUMN_REDIMIBLE_BET As Integer
  Dim GRID_COLUMN_REDIMIBLE_WON As Integer
  Dim GRID_COLUMN_NONREDIMIBLE_WON As Integer
  Dim GRID_COLUMN_COMBINATON_BET As Integer
  Dim GRID_COLUMN_COMBINATON_WON As Integer
  Dim GRID_COLUMN_PRIZE_ID As Integer
  Dim GRID_COLUMN_DRAW_TERMINAL_TYPE As Integer
  Dim GRID_COLUMN_CASHIER_NAME As Integer
  Dim GRID_COLUMN_CASHIER_USERNAME As Integer
  Dim GRID_COLUMN_OPERATION_ID As Integer
  Dim GRID_COLUMN_OPERATION_DATETIME As Integer
  Dim GRID_COLUMN_ACCOUNT_ID As Integer
  Dim GRID_COLUMN_ACCOUNT_NAME As Integer

  Dim GRID_COLUMNS As Integer
  Dim GRID_HEADER_ROWS As Integer

  ' Columns Width
  Private Const GRID_WIDTH_INDEX As Integer = 200
  Private Const GRID_WIDTH_DRAW_ID As Integer = 1100
  Private Const GRID_WIDTH_DRAW_DATETIME As Integer = 2100
  Private Const GRID_WIDTH_DRAW_TERMINAL_TYPE As Integer = 1500
  Private Const GRID_WIDTH_DRAW_CASHIER_NAME As Integer = 1585
  Private Const GRID_WIDTH_DRAW_CASHIER_USERNAME As Integer = 1300
  Private Const GRID_WIDTH_OPERATION_ID As Integer = 1500
  Private Const GRID_WIDTH_OPERATION_DATETIME As Integer = 2500
  Private Const GRID_WIDTH_ACCOUNT_ID As Integer = 1300
  Private Const GRID_WIDTH_ACCOUNT_NAME As Integer = 2150
  Private Const GRID_WIDTH_COMBINATON_BET As Integer = 1570
  Private Const GRID_WIDTH_COMBINATON_WON As Integer = 5870
  Private Const GRID_WIDTH_REDIMIBLE_BET As Integer = 1300
  Private Const GRID_WIDTH_REDIMIBLE_WON As Integer = 1500
  Private Const GRID_WIDTH_NONREDIMIBLE_WON As Integer = 1900
  Private Const GRID_WIDTH_SITE_ID As Integer = 600
  Private Const GRID_WIDTH_PRIZE_ID As Integer = 3000 ' LTC 31-OCT-2016
  Private Const GRID_WIDTH_ZERO As Integer = 0

  Private Const EXCEL_COLUMN_COMBINATON_BET As Integer = 8
  Private Const EXCEL_COLUMN_COMBINATON_WON As Integer = 9

#End Region ' Constants

#Region " Members "

  'Parameter for choosing between write or not Subtotals
  Dim m_write_subtotals As Boolean

  ' For grid subtotals
  Private m_previous_date As String
  Private m_subtotal_redimible_bet As Double
  Private m_subtotal_redimible_won As Double
  Private m_subtotal_nonredimible_won As Double

  ' For grid totalizators
  Dim m_total_redimible_bet As Double
  Dim m_total_redimible_won As Double
  Dim m_total_nonredimible_won As Double

  ' For report filters 
  Private m_account As String
  Private m_track_data As String
  Private m_holder As String
  Private m_holder_is_vip As String
  Private m_date_from As String
  Private m_date_to As String
  Private m_draw_id As String
  Private m_site_id As String
  Private m_cashiers As String
  Private m_users As String
  Private m_chk_cashdesk_draw As Boolean
  Private m_chk_gaming_table_draw As Boolean

  Private m_date_from_summary As Date
  Private m_site_id_from_summary As String

#End Region ' Members

#Region " OVERRIDES "
  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_DRAWS_CASHDESK_DETAIL

    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    Dim ctrl_card_track As uc_entry_field
    Dim ctrl_card_holder As uc_entry_field

    Call MyBase.GUI_InitControls()

    'Set form name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2245)

    'Display/hide controls depending of the GUI mode   
    If GLB_GuiMode = public_globals.ENUM_GUI.CASHDESK_DRAWS_GUI Then 'Cash desk draws Generator GUI mode
      Me.gb_cashier.Visible = False
      Me.gb_user.Visible = False
      ctrl_card_track = Me.uc_asl.Controls.Find("ef_card_track", True)(0)
      ctrl_card_holder = Me.uc_asl.Controls.Find("ef_card_holder", True)(0)
      ctrl_card_track.IsReadOnly = True
      ctrl_card_holder.IsReadOnly = True
      Me.uc_asl.Visible = False ' At this moment, the account filter won't be displayed 

      Me.ef_draw_id.Location = New System.Drawing.Point(244, 22)
      Me.ef_site_id.Location = New System.Drawing.Point(244, 47)

    Else
      Me.ef_site_id.Visible = False

    End If

    'Set value of m_write_subtotals, for show or not subtotals on the Grid
    m_write_subtotals = False

    'If we're showing subtotals, don't let reorder Grid columns
    If m_write_subtotals = True Then Me.Grid.IsSortable = False

    'Set date time filter properties
    Me.gb_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2286)
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    'Draw ID filter
    Me.ef_draw_id.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2284)
    Me.ef_draw_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, 10, 0)

    'Site ID filter
    Me.ef_site_id.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1906)
    Me.ef_site_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, 3, 0)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(1)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_INFO).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False

    ' Terminals filter controls
    Me.gb_cashier.Text = GLB_NLS_GUI_INVOICING.GetString(209)
    Me.opt_one_cashier.Text = GLB_NLS_GUI_INVOICING.GetString(218)
    Me.opt_all_cashiers.Text = GLB_NLS_GUI_INVOICING.GetString(219)
    Me.gp_draws_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7615)
    Me.chk_CashDesk_draw.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7620)
    Me.chk_GamingTable_draw.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7621)

    ' Users filter controls
    Me.gb_user.Text = GLB_NLS_GUI_INVOICING.GetString(220)
    Me.opt_one_user.Text = GLB_NLS_GUI_INVOICING.GetString(218)
    Me.opt_all_users.Text = GLB_NLS_GUI_INVOICING.GetString(219)
    Me.chk_show_all.Text = GLB_NLS_GUI_CONFIGURATION.GetString(90)

    ' Set combo with Cashier Terminals
    If Not (GLB_GuiMode = public_globals.ENUM_GUI.CASHDESK_DRAWS_GUI) Then 'If not Cash desk draws Generator GUI mode      
      Call SetComboCashierAlias(Me.cmb_cashier)
    End If
    Me.cmb_cashier.Enabled = False

    ' Set combo with Users
    If Not (GLB_GuiMode = public_globals.ENUM_GUI.CASHDESK_DRAWS_GUI) Then 'If not Cash desk draws Generator GUI mode      
      Call SetCombo(Me.cmb_user, "SELECT GU_USER_ID, GU_USERNAME FROM GUI_USERS WHERE GU_BLOCK_REASON = " & _
                          WSI.Common.GUI_USER_BLOCK_REASON.NONE & " ORDER BY GU_USERNAME")
    End If

    ' DHA 07-MAY-2014
    uc_asl.InitExtendedQuery(True)
    ' Grid
    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Initialize members and accumulators related to the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()

    'Inicialize previous_date
    m_previous_date = ""

    'Inicialize Subtotals
    ResetSubTotals()

    'Inicialize Totals 
    m_total_redimible_bet = 0
    m_total_redimible_won = 0
    m_total_nonredimible_won = 0

  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE: Print totalizator row in the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_AfterLastRow()

    Dim text_total As String
    Dim text_subtotal As String

    If Me.Grid.NumRows <> 0 Then

      text_subtotal = GLB_NLS_GUI_INVOICING.GetString(375) 'Subtotal:

      If Me.m_write_subtotals = True Then WriteTotal(GRID_COLUMN_DRAW_ID, _
                                                      m_subtotal_redimible_bet, _
                                                      m_subtotal_redimible_won, _
                                                      m_subtotal_nonredimible_won, _
                                                      ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01, text_subtotal)

    End If
    text_total = GLB_NLS_GUI_INVOICING.GetString(205) 'TOTAL:

    WriteTotal(GRID_COLUMN_DRAW_ID, _
                    m_total_redimible_bet, _
                    m_total_redimible_won, _
                    m_total_nonredimible_won, _
                    ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00, text_total)

  End Sub ' GUI_AfterLastRow

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                             ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    If GLB_GuiMode = ENUM_GUI.CASHDESK_DRAWS_GUI Then 'Cash desk draws Generator GUI mode      
      Call SetupRowDrawGenerator(RowIndex, DbRow)

    Else
      Call SetupRowCashDeskDrawsDetail(RowIndex, DbRow)

    End If

    'Subtotals increment
    m_subtotal_redimible_bet += IIf(DbRow.IsNull(SQL_COLUMN_REDIMIBLE_BET), 0, DbRow.Value(SQL_COLUMN_REDIMIBLE_BET))
    m_subtotal_redimible_won += IIf(DbRow.IsNull(SQL_COLUMN_REDIMIBLE_WON), 0, DbRow.Value(SQL_COLUMN_REDIMIBLE_WON))
    m_subtotal_nonredimible_won += IIf(DbRow.IsNull(SQL_COLUMN_NONREDIMIBLE_WON), 0, DbRow.Value(SQL_COLUMN_NONREDIMIBLE_WON))

    'Totalizators increment
    m_total_redimible_bet += IIf(DbRow.IsNull(SQL_COLUMN_REDIMIBLE_BET), 0, DbRow.Value(SQL_COLUMN_REDIMIBLE_BET))
    m_total_redimible_won += IIf(DbRow.IsNull(SQL_COLUMN_REDIMIBLE_WON), 0, DbRow.Value(SQL_COLUMN_REDIMIBLE_WON))
    m_total_nonredimible_won += IIf(DbRow.IsNull(SQL_COLUMN_NONREDIMIBLE_WON), 0, DbRow.Value(SQL_COLUMN_NONREDIMIBLE_WON))

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Check dates selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    ' Check fields on uc_account_sel are ok
    If Not Me.uc_asl.ValidateFormat Then
      Return False
    End If

    'Check Draw ID format
    If Not Me.ef_draw_id.ValidateFormat Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.ef_draw_id.Text)
      Me.ef_draw_id.Focus()

      Return False
    End If

    'Check Site ID format
    If Me.ef_site_id.Visible And Not Me.ef_site_id.ValidateFormat Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.ef_site_id.Text)
      Me.ef_site_id.Focus()

      Return False
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Get the defined Query Type
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_QUERY_TYPE
  '
  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM
  End Function ' GUI_GetQueryType

  ' PURPOSE : Define the ExecuteQuery customized
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_ExecuteQueryCustom()

    Dim _sql_cmd As SqlCommand
    Dim _table As DataTable
    Dim _row As DataRow

    Dim _rows() As DataRow
    Dim _db_row As CLASS_DB_ROW
    Dim _count As Integer
    Dim _idx_row As Integer
    Dim _prev_redraw As Boolean

    If GLB_GuiMode = ENUM_GUI.CASHDESK_DRAWS_GUI Then 'Cash desk draws Generator GUI mode
      _sql_cmd = QueryDrawGenerator()

    Else
      _sql_cmd = QueryCashdeskDrawsDetail()

    End If

    _table = GUI_GetTableUsingCommand(_sql_cmd, Integer.MaxValue)

    Call GUI_BeforeFirstRow()

    _rows = _table.Select("")

    Try

      _prev_redraw = Me.Grid.Redraw
      Me.Grid.Redraw = False

      For Each _row In _rows

        If Not GUI_DoEvents(Me.Grid) Then
          Exit Sub
        End If

        _db_row = New CLASS_DB_ROW(_row)

        If GUI_CheckOutRowBeforeAdd(_db_row) Then

          ' Add the db row to the grid
          Me.Grid.AddRow()

          _count = _count + 1
          _idx_row = Me.Grid.NumRows - 1

          If Not GUI_SetupRow(_idx_row, _db_row) Then
            ' The row can not be added
            Me.Grid.DeleteRowFast(_idx_row)
            _count = _count - 1
          End If

        End If

      Next

      _db_row = Nothing

      Call GUI_AfterLastRow()

      m_chk_cashdesk_draw = Me.chk_CashDesk_draw.Checked
      m_chk_gaming_table_draw = Me.chk_GamingTable_draw.Checked

    Catch ex As OutOfMemoryException
      Throw ex

    Catch exception As Exception
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Trace.WriteLine(exception.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "frm_draws_cashdesk_detail_sel", _
                            "GUI_ExecuteQueryCustom", _
                            exception.Message)

    Finally
      Me.Grid.Redraw = _prev_redraw
    End Try


  End Sub ' GUI_ExecuteQueryCustom

  ' PURPOSE : Checks out the DB row before adding it to the grid
  '              and process counters (& totals rows)
  '
  '  PARAMS :
  '     - INPUT :
  '           - DataRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the data row can be added) or False (the data row can not be added)
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As CLASS_DB_ROW) As Boolean

    If Me.m_write_subtotals = True Then ProcessCounters(DbRow)

    Return True

  End Function ' GUI_CheckOutRowBeforeAdd

  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(ExcelData)

    ExcelData.SetColumnFormat(EXCEL_COLUMN_COMBINATON_BET, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)
    ExcelData.SetColumnFormat(EXCEL_COLUMN_COMBINATON_WON, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)

  End Sub ' GUI_ReportParams


#End Region ' OVERRIDES

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2284), m_draw_id)


    If Me.ef_site_id.Visible Then
      'Site ID 
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1906), m_site_id)

    End If

    If Me.gb_cashier.Visible And Me.gb_user.Visible Then
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(209), m_cashiers)
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(220), m_users)

    End If

    If Me.uc_asl.Visible Then
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), m_account)
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_track_data)
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), m_holder)
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4802), m_holder_is_vip)

    End If

    If (m_chk_cashdesk_draw And m_chk_gaming_table_draw) Or (Not m_chk_cashdesk_draw And Not m_chk_gaming_table_draw) Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7615), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7620) + ", " + GLB_NLS_GUI_PLAYER_TRACKING.GetString(7621))
    ElseIf (m_chk_cashdesk_draw) Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7615), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7620))
    ElseIf (m_chk_gaming_table_draw) Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7615), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7621))
    End If


    ' With
    PrintData.FilterHeaderWidth(1) = 1600
    PrintData.FilterValueWidth(1) = 2500

    PrintData.FilterHeaderWidth(2) = 1600
    PrintData.FilterValueWidth(2) = 2500

    PrintData.FilterHeaderWidth(3) = 1000
    PrintData.FilterValueWidth(3) = 2500

  End Sub ' GUI_ReportFilter


  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_date_from = ""
    m_date_to = ""
    m_draw_id = ""
    m_site_id = ""
    m_cashiers = ""
    m_users = ""

    m_account = Me.uc_asl.Account()
    m_track_data = Me.uc_asl.TrackData()
    m_holder = Me.uc_asl.Holder()
    m_holder_is_vip = Me.uc_asl.HolderIsVip()

    'Date 
    If Me.dtp_from.Checked Then
      m_date_from = GUI_FormatDate(dtp_from.Value, _
                    ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Me.dtp_to.Checked Then
      m_date_to = GUI_FormatDate(dtp_to.Value, _
                  ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    'Draw ID
    If Me.ef_draw_id.TextValue <> "" Then
      m_draw_id = Me.ef_draw_id.TextValue

    End If

    'Site ID
    If ef_site_id.Visible And Me.ef_site_id.TextValue <> "" Then
      m_site_id = Me.ef_site_id.TextValue

    End If

    ' Cashiers
    If Me.opt_all_cashiers.Checked Then
      m_cashiers = Me.opt_all_cashiers.Text
    Else
      m_cashiers = Me.cmb_cashier.TextValue
    End If

    ' Users
    If Me.opt_all_users.Checked Then
      m_users = Me.opt_all_users.Text
    Else
      m_users = Me.cmb_user.TextValue
    End If

  End Sub 'GUI_ReportUpdateFilters

#End Region ' GUI Reports

#Region " Public Functions "

  ' PURPOSE: Open additional form to show details for the select row
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Public Sub ShowSelectedItem(ByVal MdiParent As System.Windows.Forms.IWin32Window, ByVal DateFrom As DateTime, ByVal SiteID As String)
    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING

    Me.m_date_from_summary = DateFrom
    Me.m_site_id_from_summary = SiteID

    Me.MdiParent = MdiParent
    Call Me.Display(False)

    If Me.Permissions.Read Then
      Call Application.DoEvents()
      Me.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)
    End If

  End Sub 'ShowSelectedItem

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Call Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Dim _closing_time As Integer
    Dim _initial_time As Date
    Dim _now As Date
    Dim _date_from As Date

    ' Date filter
    _closing_time = GetDefaultClosingTime()

    _now = WGDB.Now

    If Me.m_date_from_summary <> Nothing Then
      _date_from = Me.m_date_from_summary
      _initial_time = New DateTime(_date_from.Year, _date_from.Month, _date_from.Day, _closing_time, 0, 0)

    Else
      _initial_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)

    End If

    If _initial_time > _now Then
      _initial_time = _initial_time.AddDays(-1)

    End If

    Me.dtp_from.Value = _initial_time
    Me.dtp_from.Checked = True

    Me.dtp_to.Value = _initial_time.AddDays(1)

    Me.dtp_to.Checked = (Me.m_date_from_summary <> Nothing)

    'Draw ID filter
    Me.ef_draw_id.TextValue = ""

    'Site ID filter (Only in Cash desk Draw GUI Mode)
    If GLB_GuiMode = public_globals.ENUM_GUI.CASHDESK_DRAWS_GUI Then

      If Me.m_site_id_from_summary <> Nothing Then
        Me.ef_site_id.Value = Me.m_site_id_from_summary

      Else
        Me.ef_site_id.Value = ""

      End If

    End If

    'Cashier filter
    Me.opt_all_cashiers.Checked = True
    Me.cmb_cashier.Enabled = False

    'User filter
    Me.opt_all_users.Checked = True
    Me.cmb_user.Enabled = False
    Me.chk_show_all.Checked = False
    Me.chk_show_all.Enabled = False

    ' Account filter
    Me.uc_asl.Clear()

    Me.m_date_from_summary = Nothing
    Me.m_site_id_from_summary = Nothing

    Me.chk_CashDesk_draw.Checked = True
    Me.chk_GamingTable_draw.Checked = True

  End Sub ' SetDefaultValues

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub GUI_StyleSheet()
    If GLB_GuiMode = public_globals.ENUM_GUI.CASHDESK_DRAWS_GUI Then 'Cash desk draws Generator GUI mode
      Call StyleSheetDrawGenerator()
    Else
      Call StyleSheetCashDeskDrawsDetail()
    End If


    Me.Grid.Width += System.Windows.Forms.SystemInformation.VerticalScrollBarWidth

  End Sub ' GUI_StyleSheet

  ' PURPOSE : Process total counters: Draw row of totals, update and reset total counters.
  '
  '  PARAMS :
  '     - INPUT : 
  '           - DbRow As CLASS_DB_ROW
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub ProcessCounters(ByVal DbRow As CLASS_DB_ROW)

    Dim row_date As String
    Dim text_subtotal As String

    row_date = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DRAW_DATETIME), _
                                            ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                            ENUM_FORMAT_TIME.FORMAT_TIME_NONE)

    If (Not String.IsNullOrEmpty(m_previous_date)) And (m_previous_date <> row_date) Then

      text_subtotal = GLB_NLS_GUI_INVOICING.GetString(375)

      WriteTotal(GRID_COLUMN_DRAW_ID, m_subtotal_redimible_bet, m_subtotal_redimible_won, m_subtotal_nonredimible_won, ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01, text_subtotal)

      ResetSubTotals()

    End If

    m_previous_date = row_date

  End Sub ' ProcessCounters

  ' PURPOSE: Write accumulated values on a new row of the Grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub WriteTotal(ByVal IdxColumnTotal As Integer, ByVal TotalRedimibleBet As Double, ByVal TotalRedimibleWon As Double, ByVal TotalNonRedimibleWon As Double, ByVal ENUM_GUI As ControlColor.ENUM_GUI_COLOR, ByVal TextTotal As String)

    Dim idx_row As Integer


    idx_row = Me.Grid.NumRows

    Me.Grid.AddRow()

    ' Label - Subtotal
    Me.Grid.Cell(idx_row, IdxColumnTotal).Value = TextTotal

    ' Redimible Bet Subtotal
    Me.Grid.Cell(idx_row, GRID_COLUMN_REDIMIBLE_BET).Value = GUI_FormatCurrency(TotalRedimibleBet, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Redimible Won Subtotal
    Me.Grid.Cell(idx_row, GRID_COLUMN_REDIMIBLE_WON).Value = GUI_FormatCurrency(TotalRedimibleWon, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Non redimible Won Subtotal
    Me.Grid.Cell(idx_row, GRID_COLUMN_NONREDIMIBLE_WON).Value = GUI_FormatCurrency(TotalNonRedimibleWon, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Color Row
    Me.Grid.Row(idx_row).BackColor = GetColor(ENUM_GUI)

  End Sub ' WriteTotal


  ' PURPOSE: Set value '0' on subtotals
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ResetSubTotals()

    m_subtotal_redimible_bet = 0
    m_subtotal_redimible_won = 0
    m_subtotal_nonredimible_won = 0

  End Sub 'ResetSubTotals

  ' PURPOSE: Returns an SqlCommand according to Site GUI mode
  '
  '  PARAMS:
  '     - INPUT: None
  '     - OUTPUT: The SqlCommand to be returned
  '
  ' RETURNS:
  '     - None 
  Private Function QueryCashdeskDrawsDetail() As SqlCommand
    Dim _sql_cmd As SqlCommand
    Dim _sql_where_account As String

    _sql_cmd = New SqlCommand("ReportCashDeskDraws")
    _sql_cmd.CommandType = CommandType.StoredProcedure

    'Date filter
    _sql_cmd.Parameters.Add("@pDrawFrom", SqlDbType.DateTime).Value = IIf(Not Me.dtp_from.Checked, System.DBNull.Value, dtp_from.Value)
    _sql_cmd.Parameters.Add("@pDrawTo", SqlDbType.DateTime).Value = IIf(Not Me.dtp_to.Checked, System.DBNull.Value, dtp_to.Value)

    'Draw ID filter
    ' 21-NOV-2013  JPJ    Defect WIG-421 Exception overflow due to incorrect variable type.    
    _sql_cmd.Parameters.Add("@pDrawId", SqlDbType.BigInt).Value = IIf(Me.ef_draw_id.TextValue = "", System.DBNull.Value, Me.ef_draw_id.TextValue)

    'Cashier filter
    _sql_cmd.Parameters.Add("@pCashierId", SqlDbType.Int).Value = IIf(Not Me.opt_one_cashier.Checked, System.DBNull.Value, Me.cmb_cashier.Value)

    'User filter
    _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = IIf(Not Me.opt_one_user.Checked, System.DBNull.Value, Me.cmb_user.Value)

    'Draw filter
    _sql_cmd.Parameters.Add("@pCashDeskDraw", SqlDbType.Int).Value = IIf(Me.chk_CashDesk_draw.Checked, 1, 0)

    'Draw filter
    _sql_cmd.Parameters.Add("@pGamingTableDraw", SqlDbType.Int).Value = IIf(Me.chk_GamingTable_draw.Checked, 1, 0)

    'Account filter
    _sql_where_account = Me.uc_asl.GetFilterSQL()
    _sql_cmd.Parameters.Add("@pSqlAccount", SqlDbType.NVarChar).Value = IIf(String.IsNullOrEmpty(_sql_where_account), System.DBNull.Value, "WHERE " & _sql_where_account)

    'Cashier movement type
    _sql_cmd.Parameters.Add("@pCashierMovement", SqlDbType.Int).Value = CType(CASHIER_MOVEMENT.PARTICIPATION_IN_CASHDESK_DRAW, Integer)

    ' LTC 31-OCT-2016
    'Looser prize id
    _sql_cmd.Parameters.Add("@pLooserPrizeId", SqlDbType.Int).Value = CType(PrizeCategory.Prize_5, Integer)

    Return _sql_cmd

  End Function 'QueryCashdeskDrawsDetail

  ' PURPOSE: Returns an SqlCommand according to Cashdesk Draws Generator GUI mode
  '
  '  PARAMS:
  '     - INPUT: None
  '     - OUTPUT: The SqlCommand to be returned
  '
  ' RETURNS:
  '     - None 
  Private Function QueryDrawGenerator() As SqlCommand
    Dim _sql_cmd As SqlCommand
    Dim _sql_where_account As String

    _sql_cmd = New SqlCommand("ReportCashDeskDrawsGenerator")
    _sql_cmd.CommandType = CommandType.StoredProcedure

    'Date filter
    _sql_cmd.Parameters.Add("@pDrawFrom", SqlDbType.DateTime).Value = IIf(Not Me.dtp_from.Checked, System.DBNull.Value, dtp_from.Value)
    _sql_cmd.Parameters.Add("@pDrawTo", SqlDbType.DateTime).Value = IIf(Not Me.dtp_to.Checked, System.DBNull.Value, dtp_to.Value)

    'Draw ID filter
    ' 21-NOV-2013  JPJ    Defect WIG-421 Exception overflow due to incorrect variable type.    
    _sql_cmd.Parameters.Add("@pDrawId", SqlDbType.BigInt).Value = IIf(String.IsNullOrEmpty(Me.ef_draw_id.TextValue), System.DBNull.Value, Me.ef_draw_id.TextValue)

    'Account filter
    _sql_where_account = Me.uc_asl.GetFilterSQL()
    _sql_cmd.Parameters.Add("@pSqlAccount", SqlDbType.NVarChar).Value = IIf(String.IsNullOrEmpty(_sql_where_account), System.DBNull.Value, "WHERE " & _sql_where_account)

    'Site ID filter
    _sql_cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = IIf(String.IsNullOrEmpty(Me.ef_site_id.TextValue), System.DBNull.Value, Me.ef_site_id.TextValue)

    Return _sql_cmd

  End Function 'QueryDrawGenerator

  ' PURPOSE: Sets up row according to Site GUI mode
  '
  '  PARAMS:
  '     - INPUT: None
  '     - OUTPUT: None
  '
  ' RETURNS:
  '     - None 
  Private Sub SetupRowCashDeskDrawsDetail(ByVal RowIndex As Integer, _
                                               ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW)


    ' Draw ID
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DRAW_ID).Value = DbRow.Value(SQL_COLUMN_DRAW_ID)

    'Draw date
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DRAW_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DRAW_DATETIME), _
                                                                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                   ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    'Draw terminal type
    If Not DbRow.IsNull(SQL_COLUMN_DRAW_TERMINAL_TYPE) Then
      Select Case (DbRow.Value(SQL_COLUMN_DRAW_TERMINAL_TYPE))
        Case TerminalTypes.CASHDESK_DRAW
          Me.Grid.Cell(RowIndex, GRID_COLUMN_DRAW_TERMINAL_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7620)

        Case TerminalTypes.CASHDESK_DRAW_TABLE
          Me.Grid.Cell(RowIndex, GRID_COLUMN_DRAW_TERMINAL_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7621)
      End Select
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DRAW_TERMINAL_TYPE).Value = ""
    End If

    'Cashier name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CASHIER_NAME).Value = DbRow.Value(SQL_COLUMN_CASHIER_NAME)

    'Cashier username
    If Not DbRow.IsNull(SQL_COLUMN_CASHIER_USERNAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CASHIER_USERNAME).Value = DbRow.Value(SQL_COLUMN_CASHIER_USERNAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CASHIER_USERNAME).Value = ""
    End If

    'Operation ID 
    If Not DbRow.IsNull(SQL_COLUMN_OPERATION_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_ID).Value = DbRow.Value(SQL_COLUMN_OPERATION_ID)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_ID).Value = ""
    End If

    'Operation Date 
    Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_OPERATION_DATETIME), _
                                                                                  ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                   ENUM_FORMAT_TIME.FORMAT_HHMM)

    'Account ID  
    If Not DbRow.IsNull(SQL_COLUMN_ACCOUNT_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_ID)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID).Value = ""
    End If

    'Account holder name  
    If Not DbRow.IsNull(SQL_COLUMN_ACCOUNT_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_NAME).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_NAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_NAME).Value = ""
    End If

    'Bet combination  
    Me.Grid.Cell(RowIndex, GRID_COLUMN_COMBINATON_BET).Value = DbRow.Value(SQL_COLUMN_COMBINATON_BET)

    'Winner combination
    Me.Grid.Cell(RowIndex, GRID_COLUMN_COMBINATON_WON).Value = DbRow.Value(SQL_COLUMN_COMBINATON_WON)

    'Redimible bet  
    If Not DbRow.IsNull(SQL_COLUMN_REDIMIBLE_BET) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_REDIMIBLE_BET).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_REDIMIBLE_BET), _
                                                                                    ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_REDIMIBLE_BET).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    'Redimible won
    If Not (DbRow.IsNull(SQL_COLUMN_REDIMIBLE_WON)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_REDIMIBLE_WON).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_REDIMIBLE_WON), _
                                                                                    ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_REDIMIBLE_WON).Value = ""
    End If

    'Non redimible won
    If Not (DbRow.IsNull(SQL_COLUMN_NONREDIMIBLE_WON)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NONREDIMIBLE_WON).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_NONREDIMIBLE_WON), _
                                                                                    ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NONREDIMIBLE_WON).Value = ""
    End If

    'Prize id
    If Not DbRow.IsNull(SQL_COLUMN_PRIZE_ID) Then
      Select Case (DbRow.Value(SQL_COLUMN_PRIZE_ID))
        Case PrizeCategory.Prize_1
          Me.Grid.Cell(RowIndex, GRID_COLUMN_PRIZE_ID).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2456)

        Case PrizeCategory.Prize_2
          Me.Grid.Cell(RowIndex, GRID_COLUMN_PRIZE_ID).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2457)

        Case PrizeCategory.Prize_3
          Me.Grid.Cell(RowIndex, GRID_COLUMN_PRIZE_ID).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2458)

        Case PrizeCategory.Prize_4
          Me.Grid.Cell(RowIndex, GRID_COLUMN_PRIZE_ID).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2459)
          ' LTC 31-OCT-2016
        Case PrizeCategory.Prize_5
          Me.Grid.Cell(RowIndex, GRID_COLUMN_PRIZE_ID).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2449)

        Case Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_PRIZE_ID).Value = "- - - "
      End Select
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PRIZE_ID).Value = "- - - "
    End If

  End Sub 'SetupRowCashDeskDrawsDetail

  ' PURPOSE: Sets up row according to Cashdesk Draws Generator GUI mode
  '
  '  PARAMS:
  '     - INPUT: None
  '     - OUTPUT: None
  '
  ' RETURNS:
  '     - None 
  Private Sub SetupRowDrawGenerator(ByVal RowIndex As Integer, _
                                               ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW)

    ' Draw ID
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DRAW_ID).Value = DbRow.Value(SQL_COLUMN_DRAW_ID)

    'Draw date
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DRAW_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DRAW_DATETIME), _
                                                                                  ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                   ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    'Operation ID 
    If Not DbRow.IsNull(SQL_COLUMN_OPERATION_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_ID).Value = DbRow.Value(SQL_COLUMN_OPERATION_ID)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_ID).Value = ""
    End If

    'Account ID  
    If Not DbRow.IsNull(SQL_COLUMN_ACCOUNT_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_ID)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID).Value = ""
    End If

    'Bet combination  
    Me.Grid.Cell(RowIndex, GRID_COLUMN_COMBINATON_BET).Value = DbRow.Value(SQL_COLUMN_COMBINATON_BET)

    'Winner combination
    Me.Grid.Cell(RowIndex, GRID_COLUMN_COMBINATON_WON).Value = DbRow.Value(SQL_COLUMN_COMBINATON_WON)

    'Redimible bet  
    If Not DbRow.IsNull(SQL_COLUMN_REDIMIBLE_BET) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_REDIMIBLE_BET).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_REDIMIBLE_BET), _
                                                                                    ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_REDIMIBLE_BET).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    'Redimible won
    If Not ((DbRow.IsNull(SQL_COLUMN_REDIMIBLE_WON)) OrElse (DbRow.Value(SQL_COLUMN_REDIMIBLE_WON) = 0)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_REDIMIBLE_WON).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_REDIMIBLE_WON), _
                                                                                    ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_REDIMIBLE_WON).Value = ""
    End If

    'Non redimible won
    If Not ((DbRow.IsNull(SQL_COLUMN_NONREDIMIBLE_WON)) OrElse (DbRow.Value(SQL_COLUMN_NONREDIMIBLE_WON) = 0)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NONREDIMIBLE_WON).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_NONREDIMIBLE_WON), _
                                                                                    ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NONREDIMIBLE_WON).Value = ""
    End If

    'Site ID
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SITE_ID).Value = DbRow.Value(SQL_COLUMN_SITE_ID)

  End Sub 'SetupRowDrawGenerator

  Private Sub StyleSheetCashDeskDrawsDetail()
    SQL_COLUMN_DRAW_ID = 0
    SQL_COLUMN_DRAW_DATETIME = 1
    SQL_COLUMN_DRAW_TERMINAL_TYPE = 2
    SQL_COLUMN_CASHIER_NAME = 3
    SQL_COLUMN_CASHIER_USERNAME = 4
    SQL_COLUMN_OPERATION_ID = 5
    SQL_COLUMN_OPERATION_DATETIME = 6
    SQL_COLUMN_ACCOUNT_ID = 7
    SQL_COLUMN_ACCOUNT_NAME = 8
    SQL_COLUMN_COMBINATON_BET = 9
    SQL_COLUMN_COMBINATON_WON = 10
    SQL_COLUMN_REDIMIBLE_BET = 11
    SQL_COLUMN_REDIMIBLE_WON = 12
    SQL_COLUMN_NONREDIMIBLE_WON = 13
    SQL_COLUMN_PRIZE_ID = 14

    ' Grid Columns    
    GRID_COLUMN_INDEX = 0
    GRID_COLUMN_DRAW_ID = 1
    GRID_COLUMN_DRAW_DATETIME = 2
    GRID_COLUMN_DRAW_TERMINAL_TYPE = 3
    GRID_COLUMN_REDIMIBLE_BET = 4
    GRID_COLUMN_PRIZE_ID = 5
    GRID_COLUMN_REDIMIBLE_WON = 6
    GRID_COLUMN_NONREDIMIBLE_WON = 7
    GRID_COLUMN_COMBINATON_BET = 8
    GRID_COLUMN_COMBINATON_WON = 9
    GRID_COLUMN_CASHIER_NAME = 10
    GRID_COLUMN_CASHIER_USERNAME = 11
    GRID_COLUMN_OPERATION_ID = 12
    GRID_COLUMN_OPERATION_DATETIME = 13
    GRID_COLUMN_ACCOUNT_ID = 14
    GRID_COLUMN_ACCOUNT_NAME = 15

    GRID_COLUMNS = 16
    GRID_HEADER_ROWS = 2

    With Me.Grid
      'Grid creation
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .Sortable = False

      ' Index Id
      With .Column(GRID_COLUMN_INDEX)
        .Header(0).Text = ""
        .Header(1).Text = ""
        .Width = GRID_WIDTH_INDEX
        .HighLightWhenSelected = False
        .IsColumnPrintable = False
      End With

      ' Draw ID
      With .Column(GRID_COLUMN_DRAW_ID)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2569)
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2285)
        .Width = GRID_WIDTH_DRAW_ID
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      End With

      'Draw date  
      With .Column(GRID_COLUMN_DRAW_DATETIME)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2569)
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2287)
        .Width = GRID_WIDTH_DRAW_DATETIME
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      End With

      'Draw terminal type
      With .Column(GRID_COLUMN_DRAW_TERMINAL_TYPE)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2569)
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7615)
        .Width = GRID_WIDTH_DRAW_TERMINAL_TYPE
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      End With

      'Cashier name
      With .Column(GRID_COLUMN_CASHIER_NAME)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(491)
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(492)
        .Width = GRID_WIDTH_DRAW_CASHIER_NAME
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      End With

      'Cashier username
      With .Column(GRID_COLUMN_CASHIER_USERNAME)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(491)
        .Header(1).Text = GLB_NLS_GUI_AUDITOR.GetString(254)
        .Width = GRID_WIDTH_DRAW_CASHIER_USERNAME
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      End With

      'Operation ID
      With .Column(GRID_COLUMN_OPERATION_ID)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(491)
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(627)
        .Width = GRID_WIDTH_OPERATION_ID
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      End With

      'Operation date
      With .Column(GRID_COLUMN_OPERATION_DATETIME)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(491)
        .Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(201)
        .Width = GRID_WIDTH_ZERO
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      End With

      'Account ID
      With .Column(GRID_COLUMN_ACCOUNT_ID)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(496)
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(795)
        .Width = GRID_WIDTH_ACCOUNT_ID
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      End With

      'Account holder name
      With .Column(GRID_COLUMN_ACCOUNT_NAME)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(496)
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(794)
        .Width = GRID_WIDTH_ACCOUNT_NAME
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      End With

      'Bet combination
      With .Column(GRID_COLUMN_COMBINATON_BET)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2313)
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2314)
        .Width = GRID_WIDTH_COMBINATON_BET
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      End With

      'Winner combination 
      With .Column(GRID_COLUMN_COMBINATON_WON)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2313)
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2315)
        .Width = GRID_WIDTH_COMBINATON_WON
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      End With

      'Redimible bet
      With .Column(GRID_COLUMN_REDIMIBLE_BET)
        .Header(0).Text = ""
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6943)
        .Width = GRID_WIDTH_REDIMIBLE_BET
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      End With

      'Redimible won
      With .Column(GRID_COLUMN_REDIMIBLE_WON)
        .Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(33)
        .Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(337)
        .Width = GRID_WIDTH_REDIMIBLE_WON
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      End With

      'Non redimible won
      With .Column(GRID_COLUMN_NONREDIMIBLE_WON)
        .Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(33)
        .Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(241)
        .Width = GRID_WIDTH_NONREDIMIBLE_WON
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      End With

      'Prize ID
      With .Column(GRID_COLUMN_PRIZE_ID)
        .Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(33)
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7622)
        .Width = GRID_WIDTH_PRIZE_ID
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      End With

    End With 'StyleSheetCashDeskDrawsDetail
  End Sub

  Private Sub StyleSheetDrawGenerator()
    ' SQL Columns
    SQL_COLUMN_SITE_ID = 0
    SQL_COLUMN_DRAW_ID = 1
    SQL_COLUMN_DRAW_DATETIME = 2
    SQL_COLUMN_OPERATION_ID = 3
    SQL_COLUMN_ACCOUNT_ID = 4
    SQL_COLUMN_COMBINATON_BET = 5
    SQL_COLUMN_COMBINATON_WON = 6
    SQL_COLUMN_REDIMIBLE_BET = 7
    SQL_COLUMN_REDIMIBLE_WON = 8
    SQL_COLUMN_NONREDIMIBLE_WON = 9

    ' Grid Columns    
    GRID_COLUMN_INDEX = 0
    GRID_COLUMN_SITE_ID = 1
    GRID_COLUMN_DRAW_ID = 2
    GRID_COLUMN_DRAW_DATETIME = 3
    GRID_COLUMN_REDIMIBLE_BET = 4
    GRID_COLUMN_REDIMIBLE_WON = 5
    GRID_COLUMN_NONREDIMIBLE_WON = 6
    GRID_COLUMN_COMBINATON_BET = 7
    GRID_COLUMN_COMBINATON_WON = 8
    GRID_COLUMN_OPERATION_ID = 9
    GRID_COLUMN_ACCOUNT_ID = 10

    GRID_COLUMNS = 11
    GRID_HEADER_ROWS = 2

    Me.panel_filter.Height = 125
    Me.panel_filter.Width = 1184

    With Me.Grid
      'Grid creation
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .Sortable = False

      ' Index Id
      With .Column(GRID_COLUMN_INDEX)
        .Header(0).Text = ""
        .Header(1).Text = ""
        .Width = GRID_WIDTH_INDEX
        .HighLightWhenSelected = False
        .IsColumnPrintable = False
      End With

      ' Draw ID
      With .Column(GRID_COLUMN_DRAW_ID)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2569)
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2285)
        .Width = GRID_WIDTH_DRAW_ID
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      End With

      'Draw date  
      With .Column(GRID_COLUMN_DRAW_DATETIME)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2569)
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2287)
        .Width = GRID_WIDTH_DRAW_DATETIME
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      End With

      'Operation ID
      With .Column(GRID_COLUMN_OPERATION_ID)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(491)
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(627)
        .Width = GRID_WIDTH_OPERATION_ID
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      End With

      'Site ID
      With .Column(GRID_COLUMN_SITE_ID)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2569)
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1906)
        .Width = GRID_WIDTH_SITE_ID
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      End With

      'Account ID
      With .Column(GRID_COLUMN_ACCOUNT_ID)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(496)
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(795)
        .Width = GRID_WIDTH_ACCOUNT_ID
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      End With

      'Bet combination
      With .Column(GRID_COLUMN_COMBINATON_BET)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2313)
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2314)
        .Width = GRID_WIDTH_COMBINATON_BET
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      End With

      'Winner combination 
      With .Column(GRID_COLUMN_COMBINATON_WON)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2313)
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2315)
        .Width = GRID_WIDTH_COMBINATON_WON
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      End With

      'Redimible bet
      With .Column(GRID_COLUMN_REDIMIBLE_BET)
        .Header(0).Text = ""
        .Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(395)
        .Width = GRID_WIDTH_REDIMIBLE_BET
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      End With

      'Redimible won
      With .Column(GRID_COLUMN_REDIMIBLE_WON)
        .Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(257)
        .Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(337)
        .Width = GRID_WIDTH_REDIMIBLE_WON
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      End With

      'Non redimible won
      With .Column(GRID_COLUMN_NONREDIMIBLE_WON)
        .Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(257)
        .Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(241)
        .Width = GRID_WIDTH_NONREDIMIBLE_WON
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      End With

    End With

  End Sub 'StyleSheetDrawGenerator

#End Region ' Private Functions

#Region "Events"

  Private Sub opt_several_cashiers_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_one_cashier.Click
    Me.cmb_cashier.Enabled = True
  End Sub ' opt_several_cashiers_Click

  Private Sub opt_all_cashiers_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_all_cashiers.Click
    Me.cmb_cashier.Enabled = False
  End Sub ' opt_all_cashiers_Click

  Private Sub opt_several_users_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_one_user.Click
    Me.cmb_user.Enabled = True
    Me.chk_show_all.Enabled = True
  End Sub ' opt_several_users_Click

  Private Sub opt_all_users_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_all_users.Click
    Me.cmb_user.Enabled = False
    Me.chk_show_all.Enabled = False
  End Sub ' opt_all_users_Click

  Private Sub chk_show_all_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_show_all.CheckedChanged
    If chk_show_all.Checked Then
      Call SetCombo(Me.cmb_user, "SELECT GU_USER_ID, GU_USERNAME FROM GUI_USERS ORDER BY GU_USERNAME")
    Else
      Call SetCombo(Me.cmb_user, "SELECT GU_USER_ID, GU_USERNAME FROM GUI_USERS WHERE GU_BLOCK_REASON = " & WSI.Common.GUI_USER_BLOCK_REASON.NONE & " ORDER BY GU_USERNAME")
    End If
  End Sub 'chk_show_all.

#End Region ' Events

End Class