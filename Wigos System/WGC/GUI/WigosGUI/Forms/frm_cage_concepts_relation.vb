'-------------------------------------------------------------------
' Copyright � 2014 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_cage_concepts_relation.vb
' DESCRIPTION:   Relation between concepts and source target of cage
' AUTHOR:        Alberto Marcos
' CREATION DATE: 17-SEPTEMBER-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 17-SEP-2014  AMF    Initial version.
' 01-OCT-2014  SMN    Add Excel button
' 07-OCT-2014  SMN    Change icons MB_TYPE_ERROR --> MB_TYPE_WARNING
' 07-OCT-2014  RRR    Fixed Bug WIG-1417: Can't relation concepts with source-targets
' 08-OCT-2014  SMN    Accept button does'nt detect changes
' 09-OCT-2014  OPC    Resized the column: "Or�gen/Destino" and "Uso en b�veda".
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

#Region "Imports"

Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports WSI.Common

#End Region ' Imports

Public Class frm_cage_concepts_relation
  Inherits frm_base_edit

#Region " Enums "

  Enum FORM_MODE
    SOURCE_TARGET = 0
    CONCEPT = 1
  End Enum ' FORM_MODE

  Enum PROCESS_MODE
    INSERT = 0
    EDIT = 1
  End Enum ' FORM_MODE

  Enum CASHIER_ACTION_DESCRIPTION_PURPOSE
    SHOW_IN_GUI = 0
    SAVE_IN_DATABASE = 1
  End Enum ' CASHIER_ACTION_DESCRIPTION_PURPOSE

  Enum ENABLE_DISABLE
    DISABLE_ALL = 0
    ENABLE_ALL = 1
  End Enum ' ENABLE_DISABLE

  Enum ENABLE_DISABLE_ACTION
    ROW_SELECTED_EVENT = 0
    BTN_RESET_EVENT = 1
  End Enum ' ACTION

#End Region ' Enums

#Region " Constants "

  ' Grid columns
  Private Const GRID_COLUMN_INDEX = 0
  Private Const GRID_COLUMN_SOURCE_TARGET_ID = 1
  Private Const GRID_COLUMN_SOURCE_TARGET = 2
  Private Const GRID_COLUMN_CONCEPT_ID = 3
  Private Const GRID_COLUMN_CONCEPT = 4
  Private Const GRID_COLUMN_TYPE = 5
  Private Const GRID_COLUMN_TYPE_DESCRIPTION = 6
  Private Const GRID_COLUMN_CASHIER_ACTION = 7
  Private Const GRID_COLUMN_CASHIER_ACTION_DESCRIPTION = 8
  Private Const GRID_COLUMN_ONLY_NATIONAL_CURRENCY = 9
  Private Const GRID_COLUMN_ONLY_NATIONAL_CURRENCY_DESCRIPTION = 10
  Private Const GRID_COLUMN_ENABLED = 11
  Private Const GRID_COLUMN_ENABLED_DESCRIPTION = 12
  Private Const GRID_COLUMN_PRICE_FACTOR = 13
  Private Const GRID_COLUMN_ENABLED_CONCEPT = 14


  Private Const GRID_COLUMNS As Integer = 15
  Private Const GRID_HEADER_ROWS As Integer = 1

#End Region ' Constants

#Region " Members "

  Private m_table_source_target As New DataTable
  Private m_table_concepts As New DataTable
  Private m_table_relations As New DataTable

  Private m_concept_id As Integer
  Private m_source_target_id As Integer

  Private m_form_mode As FORM_MODE
  Private m_process_mode As PROCESS_MODE
  Private m_print_datetime As Date

  Private m_screendata_has_changed As Boolean

  ' 08-OCT-2014
  Private m_initial_value_cmb_cashier_action As Int32
  Private m_initial_value_cmb_type As Int32
  Private m_initial_value_cmb_concepts As Int32
  Private m_initial_value_cmb_source_target As Int32
  Private m_initial_value_chk_enabled As Boolean
  Private m_initial_value_chk_only_national_currency As Boolean
  Private m_initial_value_ef_price_factor As String


#End Region ' Members

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CAGE_CONCEPTS_RELATION

    Call MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    m_print_datetime = frm_base_sel.GUI_GetPrintDateTime()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5522)

    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False

    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False 'True
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_CONTROLS.GetString(27) ' Excel

    Me.cmb_source_target.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5523)
    Call GetSourceTarget()
    If Me.m_table_source_target.Rows.Count > 0 Then
      Me.cmb_source_target.Add(m_table_source_target, "CST_SOURCE_TARGET_ID", "CST_SOURCE_TARGET_NAME")
    Else
      Me.cmb_source_target.Enabled = False
      Me.btn_process.Enabled = False
    End If

    Me.cmb_concepts.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5480)
    Call GetConcepts()
    If Me.m_table_concepts.Rows.Count > 0 Then
      Me.cmb_concepts.Add(m_table_concepts, "CC_CONCEPT_ID", "CC_NAME")
    Else
      Me.cmb_concepts.Enabled = False
      Me.btn_process.Enabled = False
    End If

    Me.cmb_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5608)
    Call ComboTypesFill()

    Me.cmb_cashier_action.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5609)
    ComboCashierActionFill()

    Me.chk_only_national_currency.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5525)

    Me.chk_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5218)
    Me.chk_enabled.Checked = True
    Me.chk_enabled.Enabled = False

    Me.ef_price_factor.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5526)
    Me.ef_price_factor.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 5, 2)
    Me.ef_price_factor.Value = 1

    Me.btn_process.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1684)
    Me.m_process_mode = PROCESS_MODE.INSERT

    Me.btn_reset.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2418)

    RemoveHandler dg_relations.RowSelectedEvent, AddressOf dg_relations_RowSelectedEvent
    Call GUI_Relations_StyleSheet()
    AddHandler dg_relations.RowSelectedEvent, AddressOf dg_relations_RowSelectedEvent

    ef_price_factor.Visible = False
    cmb_cashier_action.Visible = False

  End Sub ' GUI_InitControls

  ' PURPOSE: Init form in edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '       - DrawId
  '       - DrawName
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overloads Sub ShowEditItem(ByVal SourceTargetId As Integer, ByVal ConceptId As Integer)

    Me.m_source_target_id = SourceTargetId
    Me.m_concept_id = ConceptId

    If SourceTargetId <> 0 Then
      Me.m_form_mode = FORM_MODE.SOURCE_TARGET
      Me.DbObjectId = SourceTargetId
    Else
      Me.m_form_mode = FORM_MODE.CONCEPT
      Me.DbObjectId = ConceptId
    End If

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowEditItem

  ' PURPOSE: Database overridable operations.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)

    Dim _cage_concepts_relation As cls_cage_concepts_relation
    ' add here new currency class objects, once per control

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New cls_cage_concepts_relation

        Select Case Me.m_form_mode
          Case FORM_MODE.SOURCE_TARGET
            CType(DbEditedObject, cls_cage_concepts_relation).Mode = FORM_MODE.SOURCE_TARGET

          Case FORM_MODE.CONCEPT
            CType(DbEditedObject, cls_cage_concepts_relation).Mode = FORM_MODE.CONCEPT

        End Select

        _cage_concepts_relation = DbEditedObject
        ' add here new currency data, once per control

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    Dim _cage_concepts_relation As cls_cage_concepts_relation
    Dim _concepts_rows() As DataRow

    Select Case (Me.m_form_mode)

      Case FORM_MODE.SOURCE_TARGET
        Me.cmb_source_target.Value = Me.m_source_target_id
        Me.cmb_source_target.Enabled = False
        Me.cmb_concepts.SelectedIndex = -1

      Case FORM_MODE.CONCEPT
        Me.cmb_source_target.SelectedIndex = -1
        Me.cmb_concepts.Value = Me.m_concept_id
        Me.cmb_concepts.Enabled = False

    End Select

    If cmb_source_target.Value = WSI.Common.CageMeters.CageSystemSourceTarget.Cashiers Then ' Cashier
      cmb_cashier_action.Visible = True
    Else
      cmb_cashier_action.Visible = False
      cmb_cashier_action.Value = CageMeters.CageSourceTargetCashierAction.None
    End If

    Try
      _cage_concepts_relation = Me.DbReadObject

      m_table_relations = _cage_concepts_relation.DataCageConceptRelation

      Call RefreshRelations()

      ef_price_factor.Visible = False
      _concepts_rows = m_table_concepts.Select(String.Format("CC_CONCEPT_ID = {0}", cmb_concepts.Value))
      If _concepts_rows.Length > 0 Then
        If _concepts_rows(0)("CC_UNIT_PRICE") <> 0 Then
          ef_price_factor.Visible = True
        End If

      End If

      Call InitialValuesSet()

    Catch ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            Me.Name, _
                            "GUI_SetScreenData", _
                            ex.Message)

      ' 137 "Exception error has been found: \n%1"
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(137), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ex.Message)

    Finally

    End Try

  End Sub 'GUI_SetScreenData

  ' PURPOSE : Get data from the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_GetScreenData()

    Dim CAGE_concepts_relation_edit As cls_cage_concepts_relation
    Dim CAGE_concepts_relation_read As cls_cage_concepts_relation

    Try

      CAGE_concepts_relation_edit = Me.DbEditedObject
      CAGE_concepts_relation_read = Me.DbReadObject

      Call GetScreenConceptsRelation(CAGE_concepts_relation_edit.DataCageConceptRelation)

    Catch ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, Me.Name, "GUI_GetScreenData", ex.Message)
    Finally

    End Try

  End Sub ' GUI_GetScreenData

  ' PURPOSE : Validate the data presented on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Return True
  End Function ' GUI_IsScreenDataOk

  ' PURPOSE : Sets initial focus
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  '          
  ' NOTES
  ' 
  Protected Overrides Sub GUI_SetInitialFocus()

    Select Case (Me.m_form_mode)

      Case FORM_MODE.SOURCE_TARGET
        Me.ActiveControl = Me.cmb_concepts

      Case FORM_MODE.CONCEPT
        Me.ActiveControl = Me.cmb_source_target

    End Select

  End Sub ' GUI_SetInitialFocus

#End Region ' Overrides

#Region " Private/Public Functions "

  ' PURPOSE: Format appearance of the relations data grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Private Sub GUI_Relations_StyleSheet()

    With dg_relations

      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, False)

      .Sortable = False

      ' Index
      .Column(GRID_COLUMN_INDEX).Header.Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 150
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Source Target Id
      .Column(GRID_COLUMN_SOURCE_TARGET_ID).Header.Text = ""
      .Column(GRID_COLUMN_SOURCE_TARGET_ID).Width = 0
      .Column(GRID_COLUMN_SOURCE_TARGET_ID).HighLightWhenSelected = True

      ' Source Target
      .Column(GRID_COLUMN_SOURCE_TARGET).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5523)
      .Column(GRID_COLUMN_SOURCE_TARGET).Width = 2500
      .Column(GRID_COLUMN_SOURCE_TARGET).HighLightWhenSelected = True
      .Column(GRID_COLUMN_SOURCE_TARGET).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Concept Id
      .Column(GRID_COLUMN_CONCEPT_ID).Header.Text = ""
      .Column(GRID_COLUMN_CONCEPT_ID).Width = 0
      .Column(GRID_COLUMN_CONCEPT_ID).HighLightWhenSelected = True

      ' Concept
      .Column(GRID_COLUMN_CONCEPT).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5480)
      .Column(GRID_COLUMN_CONCEPT).Width = 2550
      .Column(GRID_COLUMN_CONCEPT).HighLightWhenSelected = True
      .Column(GRID_COLUMN_CONCEPT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Type
      .Column(GRID_COLUMN_TYPE).Header.Text = ""
      .Column(GRID_COLUMN_TYPE).Width = 0
      .Column(GRID_COLUMN_TYPE).HighLightWhenSelected = True

      ' Type Description
      .Column(GRID_COLUMN_TYPE_DESCRIPTION).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5608)
      .Column(GRID_COLUMN_TYPE_DESCRIPTION).Width = 1605
      .Column(GRID_COLUMN_TYPE_DESCRIPTION).HighLightWhenSelected = True
      .Column(GRID_COLUMN_TYPE_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Cashier Action
      .Column(GRID_COLUMN_CASHIER_ACTION).Header.Text = ""
      .Column(GRID_COLUMN_CASHIER_ACTION).Width = 0
      .Column(GRID_COLUMN_CASHIER_ACTION).HighLightWhenSelected = True

      ' Cashier Action Description
      .Column(GRID_COLUMN_CASHIER_ACTION_DESCRIPTION).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5609)
      .Column(GRID_COLUMN_CASHIER_ACTION_DESCRIPTION).Width = 1500
      .Column(GRID_COLUMN_CASHIER_ACTION_DESCRIPTION).HighLightWhenSelected = True
      .Column(GRID_COLUMN_CASHIER_ACTION_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Only National Currency
      .Column(GRID_COLUMN_ONLY_NATIONAL_CURRENCY).Header.Text = ""
      .Column(GRID_COLUMN_ONLY_NATIONAL_CURRENCY).Width = 0
      .Column(GRID_COLUMN_ONLY_NATIONAL_CURRENCY).HighLightWhenSelected = True

      ' Only National Currency Description
      .Column(GRID_COLUMN_ONLY_NATIONAL_CURRENCY_DESCRIPTION).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5525)
      .Column(GRID_COLUMN_ONLY_NATIONAL_CURRENCY_DESCRIPTION).Width = 2200
      .Column(GRID_COLUMN_ONLY_NATIONAL_CURRENCY_DESCRIPTION).HighLightWhenSelected = True
      .Column(GRID_COLUMN_ONLY_NATIONAL_CURRENCY_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Enabled
      .Column(GRID_COLUMN_ENABLED).Header.Text = ""
      .Column(GRID_COLUMN_ENABLED).Width = 0
      .Column(GRID_COLUMN_ENABLED).HighLightWhenSelected = True

      ' Enabled Description
      .Column(GRID_COLUMN_ENABLED_DESCRIPTION).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5218)
      .Column(GRID_COLUMN_ENABLED_DESCRIPTION).Width = 1000
      .Column(GRID_COLUMN_ENABLED_DESCRIPTION).HighLightWhenSelected = True
      .Column(GRID_COLUMN_ENABLED_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Price Factor
      .Column(GRID_COLUMN_PRICE_FACTOR).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5526)
      .Column(GRID_COLUMN_PRICE_FACTOR).Width = 1200
      .Column(GRID_COLUMN_PRICE_FACTOR).HighLightWhenSelected = True
      .Column(GRID_COLUMN_PRICE_FACTOR).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Concept Enabled
      .Column(GRID_COLUMN_ENABLED_CONCEPT).Header.Text = ""
      .Column(GRID_COLUMN_ENABLED_CONCEPT).Width = 0
      .Column(GRID_COLUMN_ENABLED_CONCEPT).HighLightWhenSelected = True

    End With

  End Sub ' GUI_Relations_StyleSheet

  ' PURPOSE: Fills relations grid from draw item detail
  '
  '  PARAMS:
  '     - INPUT: 
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub RefreshRelations()
    Dim _idx As Integer
    Dim _idx_new_row As Integer

    Me.dg_relations.Clear()


    RemoveHandler dg_relations.RowSelectedEvent, AddressOf dg_relations_RowSelectedEvent

    For _idx = 0 To m_table_relations.Rows.Count - 1
      If m_table_relations.Rows.Item(_idx).Item("CC_ENABLED") Then

        _idx_new_row = Me.dg_relations.AddRow()
        Me.dg_relations.Cell(_idx_new_row, GRID_COLUMN_CONCEPT_ID).Value = m_table_relations.Rows.Item(_idx).Item("CSTC_CONCEPT_ID")
        Me.dg_relations.Cell(_idx_new_row, GRID_COLUMN_CONCEPT).Value = m_table_relations.Rows.Item(_idx).Item("CC_NAME")
        Me.dg_relations.Cell(_idx_new_row, GRID_COLUMN_SOURCE_TARGET_ID).Value = m_table_relations.Rows.Item(_idx).Item("CSTC_SOURCE_TARGET_ID")
        Me.dg_relations.Cell(_idx_new_row, GRID_COLUMN_SOURCE_TARGET).Value = m_table_relations.Rows.Item(_idx).Item("CST_SOURCE_TARGET_NAME")
        Me.dg_relations.Cell(_idx_new_row, GRID_COLUMN_TYPE).Value = m_table_relations.Rows.Item(_idx).Item("CSTC_TYPE")
        Me.dg_relations.Cell(_idx_new_row, GRID_COLUMN_TYPE_DESCRIPTION).Value = GetTypeDescription(m_table_relations.Rows.Item(_idx).Item("CSTC_TYPE"))
        Me.dg_relations.Cell(_idx_new_row, GRID_COLUMN_CASHIER_ACTION).Value = IIf(IsDBNull(m_table_relations.Rows.Item(_idx).Item("CSTC_CASHIER_ACTION")), -1, m_table_relations.Rows.Item(_idx).Item("CSTC_CASHIER_ACTION"))
        Me.dg_relations.Cell(_idx_new_row, GRID_COLUMN_CASHIER_ACTION_DESCRIPTION).Value = GetCashierActionDescription(IIf(IsDBNull(m_table_relations.Rows.Item(_idx).Item("CSTC_CASHIER_ACTION")), -1, m_table_relations.Rows.Item(_idx).Item("CSTC_CASHIER_ACTION")), CASHIER_ACTION_DESCRIPTION_PURPOSE.SHOW_IN_GUI)
        Me.dg_relations.Cell(_idx_new_row, GRID_COLUMN_ONLY_NATIONAL_CURRENCY).Value = m_table_relations.Rows.Item(_idx).Item("CSTC_ONLY_NATIONAL_CURRENCY")
        Me.dg_relations.Cell(_idx_new_row, GRID_COLUMN_ONLY_NATIONAL_CURRENCY_DESCRIPTION).Value = IIf(m_table_relations.Rows.Item(_idx).Item("CSTC_ONLY_NATIONAL_CURRENCY"), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5304), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5305))
        Me.dg_relations.Cell(_idx_new_row, GRID_COLUMN_ENABLED).Value = m_table_relations.Rows.Item(_idx).Item("CSTC_ENABLED")
        Me.dg_relations.Cell(_idx_new_row, GRID_COLUMN_ENABLED_DESCRIPTION).Value = IIf(m_table_relations.Rows.Item(_idx).Item("CSTC_ENABLED"), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5304), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5305))
        ' Me.dg_relations.Cell(_idx, GRID_COLUMN_PRICE_FACTOR).Value = GUI_FormatNumber(m_table_relations.Rows.Item(_idx).Item("CSTC_PRICE_FACTOR"), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        If IsDBNull(m_table_relations.Rows.Item(_idx).Item("CSTC_PRICE_FACTOR")) Then
          Me.dg_relations.Cell(_idx_new_row, GRID_COLUMN_PRICE_FACTOR).Value = String.Empty
        Else
          Me.dg_relations.Cell(_idx_new_row, GRID_COLUMN_PRICE_FACTOR).Value = GUI_FormatNumber(m_table_relations.Rows.Item(_idx).Item("CSTC_PRICE_FACTOR"), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If
        Me.dg_relations.Cell(_idx_new_row, GRID_COLUMN_ENABLED_CONCEPT).Value = m_table_relations.Rows.Item(_idx).Item("CC_ENABLED")

      End If

    Next

    AddHandler dg_relations.RowSelectedEvent, AddressOf dg_relations_RowSelectedEvent

  End Sub ' RefreshRelations

  ' PURPOSE: Get source target from database
  '
  '  PARAMS:
  '     - INPUT: 
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub GetSourceTarget()
    Dim _sb As StringBuilder

    _sb = New StringBuilder()
    _sb.AppendLine("   SELECT   CST_SOURCE_TARGET_ID ")
    _sb.AppendLine("          , CST_SOURCE_TARGET_NAME ")
    _sb.AppendLine("     FROM   CAGE_SOURCE_TARGET ")
    _sb.AppendLine("    WHERE   CST_SOURCE_TARGET_ID > 0 ")

    If Me.m_form_mode = FORM_MODE.CONCEPT Then
      _sb.AppendLine("    AND   NOT EXISTS (SELECT   1 ")
      _sb.AppendLine("                        FROM   CAGE_SOURCE_TARGET_CONCEPTS ")
      _sb.AppendLine("                       WHERE   CSTC_SOURCE_TARGET_ID = CST_SOURCE_TARGET_ID ")
      _sb.AppendLine("                         AND   CSTC_CONCEPT_ID = " & Me.m_concept_id & ")")
    End If

    _sb.AppendLine(" ORDER BY   CST_SOURCE_TARGET_NAME ")

    m_table_source_target = GUI_GetTableUsingSQL(_sb.ToString(), Integer.MaxValue)

  End Sub ' GetSourceTarget

  ' PURPOSE: Get concepts from database
  '
  '  PARAMS:
  '     - INPUT: 
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub GetConcepts()
    Dim _sb As StringBuilder

    _sb = New StringBuilder()
    _sb.AppendLine("   SELECT   CC_CONCEPT_ID ")
    _sb.AppendLine("          , CC_NAME ")
    _sb.AppendLine("          , ISNULL(CC_UNIT_PRICE, 0.00) CC_UNIT_PRICE")
    _sb.AppendLine("     FROM   CAGE_CONCEPTS ")
    _sb.AppendLine("    WHERE   CC_CONCEPT_ID > 0 ")
    _sb.AppendLine("      AND   CC_TYPE = " & CageMeters.CageConceptType.UserCalculated)
    _sb.AppendLine("      AND   ISNULL(CC_ENABLED,0) = 1 ")

    If Me.m_form_mode = FORM_MODE.SOURCE_TARGET Then
      _sb.AppendLine("    AND   NOT EXISTS (SELECT   1 ")
      _sb.AppendLine("                        FROM   CAGE_SOURCE_TARGET_CONCEPTS ")
      _sb.AppendLine("                       WHERE   CSTC_CONCEPT_ID = CC_CONCEPT_ID ")
      _sb.AppendLine("                         AND   CSTC_SOURCE_TARGET_ID = " & Me.m_source_target_id & ")")
    End If

    _sb.AppendLine(" ORDER BY   CC_NAME ")

    m_table_concepts = GUI_GetTableUsingSQL(_sb.ToString(), Integer.MaxValue)

  End Sub ' GetSourceTarget

  ' PURPOSE: Load types
  '
  '  PARAMS:
  '     - INPUT: 
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub ComboTypesFill()

    Me.cmb_type.Add(CageMeters.CageSourceTargetType.CageInput, GetTypeDescription(CageMeters.CageSourceTargetType.CageInput))
    Me.cmb_type.Add(CageMeters.CageSourceTargetType.CageOutput, GetTypeDescription(CageMeters.CageSourceTargetType.CageOutput))
    Me.cmb_type.Add(CageMeters.CageSourceTargetType.CageInputOutput, GetTypeDescription(CageMeters.CageSourceTargetType.CageInputOutput))
    Me.cmb_type.Value = -1
    Me.cmb_type.SelectedIndex = -1

  End Sub ' ComboTypesFill

  ' PURPOSE: Load cashier action
  '
  '  PARAMS:
  '     - INPUT: 
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub ComboCashierActionFill()

    ' Me.cmb_cashier_action.Add(CageMeters.CageSourceTargetCashierAction.None, GetCashierActionDescription(CageMeters.CageSourceTargetCashierAction.None))
    Me.cmb_cashier_action.Add(CageMeters.CageSourceTargetCashierAction.CashierInput, GetCashierActionDescription(CageMeters.CageSourceTargetCashierAction.CashierInput, CASHIER_ACTION_DESCRIPTION_PURPOSE.SHOW_IN_GUI))
    Me.cmb_cashier_action.Add(CageMeters.CageSourceTargetCashierAction.CashierOutput, GetCashierActionDescription(CageMeters.CageSourceTargetCashierAction.CashierOutput, CASHIER_ACTION_DESCRIPTION_PURPOSE.SHOW_IN_GUI))

    Me.cmb_cashier_action.SelectedIndex = -1
    Me.cmb_cashier_action.Value = -1

  End Sub ' ComboCashierActionFill

  ' PURPOSE: Get type description
  '
  '  PARAMS:
  '     - INPUT: 
  '         - CodType: CageSourceTargetType
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Function GetTypeDescription(ByVal CodType As CageMeters.CageSourceTargetType) As String

    Select Case (CodType)

      Case CageMeters.CageSourceTargetType.CageInput
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(5365)

      Case CageMeters.CageSourceTargetType.CageOutput
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(2906)

      Case CageMeters.CageSourceTargetType.CageInputOutput
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(5530)

      Case Else

        Return ""

    End Select

  End Function ' GetTypeDescription

  ' PURPOSE: Get type description
  '
  '  PARAMS:
  '     - INPUT: 
  '         - CodCashierAction: CageSourceTargetCashierAction
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Function GetCashierActionDescription(ByVal CodCashierAction As CageMeters.CageSourceTargetCashierAction, ByVal Destination As CASHIER_ACTION_DESCRIPTION_PURPOSE) As String

    Select Case (CodCashierAction)

      Case CageMeters.CageSourceTargetCashierAction.None
        Return ""

      Case CageMeters.CageSourceTargetCashierAction.CashierInput
        If Destination = CASHIER_ACTION_DESCRIPTION_PURPOSE.SAVE_IN_DATABASE Then
          Return "STR_CASHIER_CONCEPTS_IN" ' (Cashier Resource)
        Else
          Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(5528) ' Entradas
        End If



      Case CageMeters.CageSourceTargetCashierAction.CashierOutput
        If Destination = CASHIER_ACTION_DESCRIPTION_PURPOSE.SAVE_IN_DATABASE Then
          Return "STR_CASHIER_CONCEPTS_OUT" ' (Cashier Resource)
        Else
          Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(5529) ' Salidas
        End If
      Case Else

        Return ""

    End Select

  End Function ' GetCashierActionDescription

  ' PURPOSE : Validate the data presented on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Function CheckScreenData() As Boolean

    If Me.cmb_source_target.SelectedIndex < 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4678), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(5523))
      Call cmb_source_target.Focus()

      Return False
    End If

    If Me.cmb_concepts.SelectedIndex < 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4678), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(5480))
      Call cmb_concepts.Focus()

      Return False
    End If

    If Me.cmb_type.SelectedIndex < 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4678), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(5608))
      Call cmb_type.Focus()

      Return False
    End If

    If cmb_source_target.Value = CageMeters.CageSystemSourceTarget.Cashiers AndAlso Me.cmb_cashier_action.SelectedIndex < 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4678), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(5609))
      Call cmb_cashier_action.Focus()

      Return False
    End If
    If ef_price_factor.Visible Then
      If String.IsNullOrEmpty(ef_price_factor.Value) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4678), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(5526))
        Call ef_price_factor.Focus()

        Return False
      ElseIf ef_price_factor.Value < 1 Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1195), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(5526), 1) ' 07-OCT-2014 SMN
        Call ef_price_factor.Focus()

        Return False '03-OCT-2014 SMN
      End If
    End If
    Return True

  End Function ' CheckScreenData

  ' PURPOSE: Process Changes on grid
  '
  '  PARAMS:
  '     - INPUT: 
  '         - NumRow: number of row to change
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub ProcessChanges(ByVal NumRow As Integer)

    Me.dg_relations.Cell(NumRow, GRID_COLUMN_CONCEPT_ID).Value = Me.cmb_concepts.Value
    Me.dg_relations.Cell(NumRow, GRID_COLUMN_CONCEPT).Value = Me.cmb_concepts.TextValue
    Me.dg_relations.Cell(NumRow, GRID_COLUMN_SOURCE_TARGET_ID).Value = Me.cmb_source_target.Value
    Me.dg_relations.Cell(NumRow, GRID_COLUMN_SOURCE_TARGET).Value = Me.cmb_source_target.TextValue
    Me.dg_relations.Cell(NumRow, GRID_COLUMN_TYPE).Value = Me.cmb_type.Value
    Me.dg_relations.Cell(NumRow, GRID_COLUMN_TYPE_DESCRIPTION).Value = Me.cmb_type.TextValue
    Me.dg_relations.Cell(NumRow, GRID_COLUMN_CASHIER_ACTION).Value = IIf(Me.cmb_source_target.Value = CageMeters.CageSystemSourceTarget.Cashiers, Me.cmb_cashier_action.Value, -1)
    Me.dg_relations.Cell(NumRow, GRID_COLUMN_CASHIER_ACTION_DESCRIPTION).Value = Me.cmb_cashier_action.TextValue
    Me.dg_relations.Cell(NumRow, GRID_COLUMN_ONLY_NATIONAL_CURRENCY).Value = Me.chk_only_national_currency.Checked
    Me.dg_relations.Cell(NumRow, GRID_COLUMN_ONLY_NATIONAL_CURRENCY_DESCRIPTION).Value = IIf(Me.chk_only_national_currency.Checked, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5304), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5305))
    Me.dg_relations.Cell(NumRow, GRID_COLUMN_ENABLED).Value = Me.chk_enabled.Checked
    Me.dg_relations.Cell(NumRow, GRID_COLUMN_ENABLED_DESCRIPTION).Value = IIf(Me.chk_enabled.Checked, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5304), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5305))
    If ef_price_factor.Visible Then
      Me.dg_relations.Cell(NumRow, GRID_COLUMN_PRICE_FACTOR).Value = GUI_FormatNumber(Me.ef_price_factor.Value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Else
      Me.dg_relations.Cell(NumRow, GRID_COLUMN_PRICE_FACTOR).Value = String.Empty
    End If

    Call InitialValuesSet()

  End Sub ' ProcessChanges

  ' PURPOSE: Update the relations DataTable with the allowed value from uc_grid 
  '
  '  PARAMS:
  '     - INPUT:
  '           - currencies As DataTable
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GetScreenConceptsRelation(ByVal Relations As DataTable)

    Dim _concept_relation() As DataRow
    Dim _source_target_id As String
    Dim _concept_id As String

    If Not Me.dg_relations Is Nothing AndAlso Not Me.dg_relations.IsDisposed = True AndAlso Me.dg_relations.NumRows > 0 Then
      For _idx_row As Integer = 0 To Me.dg_relations.NumRows - 1

        _source_target_id = Me.dg_relations.Cell(_idx_row, GRID_COLUMN_SOURCE_TARGET_ID).Value
        _concept_id = Me.dg_relations.Cell(_idx_row, GRID_COLUMN_CONCEPT_ID).Value

        _concept_relation = Relations.Select("CSTC_SOURCE_TARGET_ID = " & _source_target_id & " AND  CSTC_CONCEPT_ID = " & _concept_id)

        If _concept_relation.Length = 0 Then
          Continue For
        End If

        If _concept_relation(0)(cls_cage_concepts_relation.SQL_COLUMN_TYPE) <> Me.dg_relations.Cell(_idx_row, GRID_COLUMN_TYPE).Value.ToString() Then
          _concept_relation(0)(cls_cage_concepts_relation.SQL_COLUMN_TYPE) = Me.dg_relations.Cell(_idx_row, GRID_COLUMN_TYPE).Value
        End If

        If IIf(IsDBNull(_concept_relation(0)(cls_cage_concepts_relation.SQL_COLUMN_CASHIER_ACTION)), -1, _concept_relation(0)(cls_cage_concepts_relation.SQL_COLUMN_CASHIER_ACTION)) <> Me.dg_relations.Cell(_idx_row, GRID_COLUMN_CASHIER_ACTION).Value.ToString() Then
          _concept_relation(0)(cls_cage_concepts_relation.SQL_COLUMN_CASHIER_ACTION) = Me.dg_relations.Cell(_idx_row, GRID_COLUMN_CASHIER_ACTION).Value
        End If

        If _concept_relation(0)(cls_cage_concepts_relation.SQL_COLUMN_ONLY_NATIONAL_CURRENCY) <> Me.dg_relations.Cell(_idx_row, GRID_COLUMN_ONLY_NATIONAL_CURRENCY).Value.ToString() Then
          _concept_relation(0)(cls_cage_concepts_relation.SQL_COLUMN_ONLY_NATIONAL_CURRENCY) = Me.dg_relations.Cell(_idx_row, GRID_COLUMN_ONLY_NATIONAL_CURRENCY).Value
        End If

        If _concept_relation(0)(cls_cage_concepts_relation.SQL_COLUMN_ENABLED) <> Me.dg_relations.Cell(_idx_row, GRID_COLUMN_ENABLED).Value.ToString() Then
          _concept_relation(0)(cls_cage_concepts_relation.SQL_COLUMN_ENABLED) = Me.dg_relations.Cell(_idx_row, GRID_COLUMN_ENABLED).Value
        End If

        _concept_relation(0)(cls_cage_concepts_relation.SQL_COLUMN_PRICE_FACTOR) = cls_cage_concepts_relation.FACTOR_VALUE ' default value
        If Not IsDBNull(_concept_relation(0)(cls_cage_concepts_relation.SQL_COLUMN_PRICE_FACTOR)) Then
          If IsNumeric(Me.dg_relations.Cell(_idx_row, GRID_COLUMN_PRICE_FACTOR).Value) AndAlso _concept_relation(0)(cls_cage_concepts_relation.SQL_COLUMN_PRICE_FACTOR) <> Me.dg_relations.Cell(_idx_row, GRID_COLUMN_PRICE_FACTOR).Value.ToString() Then
            _concept_relation(0)(cls_cage_concepts_relation.SQL_COLUMN_PRICE_FACTOR) = GUI_ParseNumber(Me.dg_relations.Cell(_idx_row, GRID_COLUMN_PRICE_FACTOR).Value.ToString())
          End If
        End If

      Next
    End If

  End Sub ' GetScreenConceptsRelation

  ' PURPOSE: Initializes initial_values
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub InitialValuesSet() ' 08-OCT-2014
    m_initial_value_cmb_cashier_action = cmb_cashier_action.SelectedIndex
    m_initial_value_cmb_type = cmb_type.SelectedIndex
    m_initial_value_cmb_concepts = cmb_concepts.SelectedIndex
    m_initial_value_cmb_source_target = cmb_source_target.SelectedIndex
    m_initial_value_chk_enabled = chk_enabled.Checked
    m_initial_value_chk_only_national_currency = chk_only_national_currency.Checked
    m_initial_value_ef_price_factor = ef_price_factor.Value
  End Sub

  ' PURPOSE: Check changes in intial_values
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - True: initial values has changed
  '     - False initial values not has changed
  Private Function InitialValuesHasChanged() As Boolean ' 08-OCT-2014
    Return m_initial_value_cmb_cashier_action <> cmb_cashier_action.SelectedIndex _
      OrElse m_initial_value_cmb_type <> cmb_type.SelectedIndex _
      OrElse m_initial_value_cmb_concepts <> cmb_concepts.SelectedIndex _
      OrElse m_initial_value_cmb_source_target <> cmb_source_target.SelectedIndex _
      OrElse m_initial_value_chk_enabled <> chk_enabled.Checked _
      OrElse m_initial_value_chk_only_national_currency <> chk_only_national_currency.Checked _
      OrElse m_initial_value_ef_price_factor <> ef_price_factor.Value
  End Function ' InitialValuesHasChanged

  ' PURPOSE: Disable all controls. Only will be enabled cancel button.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub DisableOrEnabledControls(ByVal Value As ENABLE_DISABLE, ByVal Action As ENABLE_DISABLE_ACTION)
    Dim _value As Boolean

    _value = IIf(Value = ENABLE_DISABLE.ENABLE_ALL, True, False)

    Me.cmb_type.Enabled = _value
    Me.cmb_cashier_action.Enabled = _value
    Me.chk_only_national_currency.Enabled = _value
    Me.chk_enabled.Enabled = _value
    Me.btn_process.Enabled = _value
    GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = _value

    Select Case (Me.m_form_mode)
      Case FORM_MODE.SOURCE_TARGET
        If _value Then
          Me.cmb_source_target.Enabled = False
          If Action = ENABLE_DISABLE_ACTION.BTN_RESET_EVENT Then
            Me.cmb_concepts.Enabled = True
          End If
        Else
          Me.cmb_source_target.Enabled = False
          Me.cmb_concepts.Enabled = False
        End If

      Case FORM_MODE.CONCEPT
        If _value Then
          Me.cmb_concepts.Enabled = False
          If Action = ENABLE_DISABLE_ACTION.BTN_RESET_EVENT Then
            Me.cmb_source_target.Enabled = True
          End If
        Else
          Me.cmb_source_target.Enabled = False
          Me.cmb_concepts.Enabled = False
        End If
    End Select

  End Sub ' DisableOrEnabledAll

#End Region ' Private/Public Functions

#Region " Events "

  ' PURPOSE:  Handle RowSelectedEvent event
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub dg_relations_RowSelectedEvent(ByVal SelectedRow As Integer) Handles dg_relations.RowSelectedEvent

    Me.btn_process.Text = GLB_NLS_GUI_CONFIGURATION.GetString(69)
    Me.btn_process.Enabled = True
    Me.m_process_mode = PROCESS_MODE.EDIT

    Select Case (Me.m_form_mode)

      Case FORM_MODE.SOURCE_TARGET
        Me.cmb_concepts.Enabled = False
        Me.cmb_concepts.Value = CInt(dg_relations.Cell(SelectedRow, GRID_COLUMN_CONCEPT_ID).Value)
        If Me.cmb_concepts.Value <> CInt(dg_relations.Cell(SelectedRow, GRID_COLUMN_CONCEPT_ID).Value) Then
          Me.cmb_concepts.Add(CInt(dg_relations.Cell(SelectedRow, GRID_COLUMN_CONCEPT_ID).Value), dg_relations.Cell(SelectedRow, GRID_COLUMN_CONCEPT).Value.ToString())
          Me.cmb_concepts.Value = CInt(dg_relations.Cell(SelectedRow, GRID_COLUMN_CONCEPT_ID).Value)
        End If

      Case FORM_MODE.CONCEPT
        Me.cmb_source_target.Enabled = False
        Me.cmb_source_target.Value = CInt(dg_relations.Cell(SelectedRow, GRID_COLUMN_SOURCE_TARGET_ID).Value)
        If Me.cmb_source_target.Value <> CInt(dg_relations.Cell(SelectedRow, GRID_COLUMN_SOURCE_TARGET_ID).Value) Then
          Me.cmb_source_target.Add(CInt(dg_relations.Cell(SelectedRow, GRID_COLUMN_SOURCE_TARGET_ID).Value), dg_relations.Cell(SelectedRow, GRID_COLUMN_SOURCE_TARGET).Value.ToString())
          Me.cmb_source_target.Value = CInt(dg_relations.Cell(SelectedRow, GRID_COLUMN_SOURCE_TARGET_ID).Value)
        End If

    End Select

    Me.cmb_type.Value = dg_relations.Cell(SelectedRow, GRID_COLUMN_TYPE).Value
    Me.cmb_cashier_action.Value = dg_relations.Cell(SelectedRow, GRID_COLUMN_CASHIER_ACTION).Value
    Me.chk_only_national_currency.Checked = dg_relations.Cell(SelectedRow, GRID_COLUMN_ONLY_NATIONAL_CURRENCY).Value
    Me.chk_enabled.Enabled = True
    Me.chk_enabled.Checked = dg_relations.Cell(SelectedRow, GRID_COLUMN_ENABLED).Value
    Me.ef_price_factor.Value = dg_relations.Cell(SelectedRow, GRID_COLUMN_PRICE_FACTOR).Value

    Call InitialValuesSet()

    If Me.cmb_source_target.Value < 1000 AndAlso Me.cmb_concepts.Value < 1000 Then
      Call DisableOrEnabledControls(ENABLE_DISABLE.DISABLE_ALL, ENABLE_DISABLE_ACTION.ROW_SELECTED_EVENT)
    Else
      Call DisableOrEnabledControls(ENABLE_DISABLE.ENABLE_ALL, ENABLE_DISABLE_ACTION.ROW_SELECTED_EVENT)
    End If

  End Sub ' dg_relations_RowSelectedEvent

  ' PURPOSE:  Handle ClickEvent event
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub btn_reset_ClickEvent() Handles btn_reset.ClickEvent

    Call DisableOrEnabledControls(ENABLE_DISABLE.ENABLE_ALL, ENABLE_DISABLE_ACTION.BTN_RESET_EVENT)

    Select Case (Me.m_form_mode)

      Case FORM_MODE.SOURCE_TARGET
        Me.cmb_concepts.Enabled = True
        Me.cmb_source_target.Enabled = False
        Me.cmb_concepts.Clear()
        If Me.m_table_concepts.Rows.Count > 0 Then
          Me.cmb_concepts.Add(m_table_concepts, "CC_CONCEPT_ID", "CC_NAME")
          Me.cmb_concepts.SelectedIndex = -1
          Me.cmb_concepts.Enabled = True
          Me.cmb_concepts.Focus()
        Else
          Me.cmb_concepts.Enabled = False
          Me.btn_process.Enabled = False
          Me.cmb_type.Focus()
        End If

      Case FORM_MODE.CONCEPT
        Me.cmb_source_target.Enabled = True
        Me.cmb_concepts.Enabled = False
        Me.cmb_source_target.Clear()
        If Me.m_table_source_target.Rows.Count > 0 Then
          Me.cmb_source_target.Add(m_table_source_target, "CST_SOURCE_TARGET_ID", "CST_SOURCE_TARGET_NAME")
          Me.cmb_source_target.SelectedIndex = -1
          Me.cmb_source_target.Enabled = True
          Me.cmb_source_target.Focus()
        Else
          Me.cmb_source_target.Enabled = False
          Me.btn_process.Enabled = False
          Me.cmb_source_target.Focus()
        End If

    End Select

    Me.cmb_type.Value = 0
    Me.cmb_type.SelectedIndex = -1
    Me.cmb_cashier_action.Value = -1
    Me.cmb_cashier_action.SelectedIndex = -1
    Me.chk_only_national_currency.Checked = False
    Me.chk_enabled.Enabled = False
    Me.chk_enabled.Checked = True
    Me.ef_price_factor.Value = 1
    RemoveHandler dg_relations.RowSelectedEvent, AddressOf dg_relations_RowSelectedEvent
    If Me.dg_relations.CurrentRow >= 0 Then
      Me.dg_relations.IsSelected(Me.dg_relations.CurrentRow) = False
    End If
    Me.dg_relations.CurrentRow = -1
    AddHandler dg_relations.RowSelectedEvent, AddressOf dg_relations_RowSelectedEvent

    Me.btn_process.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1684)
    Me.m_process_mode = PROCESS_MODE.INSERT

    Call InitialValuesSet()
  End Sub ' btn_reset_ClickEvent

  ' PURPOSE:  Handle ClickEvent event
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub btn_process_ClickEvent() Handles btn_process.ClickEvent
    Dim CAGE_concepts_relation_edit As cls_cage_concepts_relation
    Dim _array_value(8) As String

    If Not CheckScreenData() Then

      Exit Sub
    End If

    Select Case (Me.m_process_mode)

      Case PROCESS_MODE.INSERT

        Me.dg_relations.AddRow()
        Call ProcessChanges(Me.dg_relations.NumRows - 1)

        Select Case (Me.m_form_mode)

          Case FORM_MODE.SOURCE_TARGET
            Me.m_table_concepts.Rows.Remove(Me.m_table_concepts.Select(" CC_CONCEPT_ID = " & Me.cmb_concepts.Value)(0))

          Case FORM_MODE.CONCEPT
            Me.m_table_source_target.Rows.Remove(Me.m_table_source_target.Select(" CST_SOURCE_TARGET_ID = " & Me.cmb_source_target.Value)(0))

        End Select

        _array_value(0) = dg_relations.Cell(Me.dg_relations.NumRows - 1, GRID_COLUMN_CONCEPT_ID).Value
        _array_value(1) = dg_relations.Cell(Me.dg_relations.NumRows - 1, GRID_COLUMN_CONCEPT).Value
        _array_value(2) = dg_relations.Cell(Me.dg_relations.NumRows - 1, GRID_COLUMN_SOURCE_TARGET_ID).Value
        _array_value(3) = dg_relations.Cell(Me.dg_relations.NumRows - 1, GRID_COLUMN_SOURCE_TARGET).Value
        _array_value(4) = dg_relations.Cell(Me.dg_relations.NumRows - 1, GRID_COLUMN_TYPE).Value
        _array_value(5) = dg_relations.Cell(Me.dg_relations.NumRows - 1, GRID_COLUMN_CASHIER_ACTION).Value
        _array_value(6) = dg_relations.Cell(Me.dg_relations.NumRows - 1, GRID_COLUMN_ONLY_NATIONAL_CURRENCY).Value
        _array_value(7) = dg_relations.Cell(Me.dg_relations.NumRows - 1, GRID_COLUMN_ENABLED).Value
        _array_value(8) = GUI_FormatNumber(GUI_ParseNumber(dg_relations.Cell(Me.dg_relations.NumRows - 1, GRID_COLUMN_PRICE_FACTOR).Value.ToString()), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, False)

        CAGE_concepts_relation_edit = Me.DbEditedObject
        CAGE_concepts_relation_edit.DataCageConceptRelation.Rows.Add(_array_value)

      Case PROCESS_MODE.EDIT

        Call ProcessChanges(dg_relations.CurrentRow)

    End Select

    Call btn_reset_ClickEvent()

    Call InitialValuesSet()

  End Sub ' btn_process_ClickEvent

  ' PURPOSE: Handle cmb_source_target Change
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub cmb_source_target_Change() Handles cmb_source_target.ValueChangedEvent
    If cmb_source_target.Value = WSI.Common.CageMeters.CageSystemSourceTarget.Cashiers Then ' Cashier
      cmb_cashier_action.Visible = True

    Else
      cmb_cashier_action.Visible = False
      cmb_cashier_action.Value = CageMeters.CageSourceTargetCashierAction.None
    End If
  End Sub ' cmb_source_target_Change

  ' PURPOSE: Handle cmb_source_target Change
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub cmb_concepts_ChangeEvent() Handles cmb_concepts.ValueChangedEvent
    Dim _concepts_rows() As DataRow

    ef_price_factor.Visible = False

    Try
      _concepts_rows = m_table_concepts.Select(String.Format("CC_CONCEPT_ID = {0}", cmb_concepts.Value))
      If _concepts_rows.Length > 0 Then
        If _concepts_rows(0)("CC_UNIT_PRICE") <> 0 Then
          'TODO
          ef_price_factor.Visible = True
        End If

      End If

    Catch ex As Exception

    End Try
  End Sub ' cmb_concepts_ChangeEvent

#End Region ' Events

  ' PURPOSE: Overload function to use the buttons of type "custom" assigning them functions
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)
    Select Case ButtonId
      'Case ENUM_BUTTON.BUTTON_CUSTOM_0 'Imprimir

      Case ENUM_BUTTON.BUTTON_CUSTOM_1 'Excel
        ButtonId = ENUM_BUTTON.BUTTON_EXCEL
      Case ENUM_BUTTON.BUTTON_OK, ENUM_BUTTON.BUTTON_CANCEL

        If InitialValuesHasChanged() Then
          If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5610), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then

            Return
          End If
        End If

    End Select

    MyBase.GUI_ButtonClick(ButtonId)

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Operations to be performed to generate the excel file
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ExcelResultList()

    Dim _print_data As GUI_Reports.CLASS_PRINT_DATA
    Dim _prev_cursor As Windows.Forms.Cursor

    _prev_cursor = Me.Cursor
    Me.Cursor = Cursors.WaitCursor

    Try
      _print_data = New GUI_Reports.CLASS_PRINT_DATA()
      'Call GUI_ReportFilter(_print_data)

      frm_base_sel.GUI_GenerateExcel(dg_relations, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5522), m_print_datetime, _print_data, False) 'm_only_preview)

    Finally
      Me.Cursor = _prev_cursor
    End Try

  End Sub ' GUI_ExcelResultList

End Class ' frm_cage_concepts_relation


