﻿Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports GUI_Controls.frm_base_sel

Public Class frm_new_section_schema_edit
  Inherits frm_base_edit

#Region "Constants"

  Private Const LEN_SECTION_NAME As Integer = 100
  Private Const LEN_SECTION_TITLE As Integer = 100
  Private Const LEN_SECTION_SUB_TITLE As Integer = 100
  Private Const LEN_SECTION_MAIN_TITLE As Integer = 100
  Private Const LEN_SECTION_MAIN_SUB_TITLE As Integer = 100
  Private Const LEN_SECTION_DESCRIPTION As Integer = 500
  Private Const LEN_SECTION_ABSTRACT As Integer = 500
  Private Const LEN_SECTION_BUTTON As Integer = 100
  Private Const LEN_SECTION_HEADLINE As Integer = 100
  Private Const LEN_SECTION_ORDER As Integer = 2

  ''GRID ITEMS
  Private Const COLUMN_GRID_ITEM_ID As Integer = 0
  Private Const COLUMN_GRID_ITEM_SUPERIOR_NAME As Integer = 1
  Private Const COLUMN_GRID_ITEM_TITLE As Integer = 2
  Private Const COLUMN_GRID_ITEM_VISIBLE As Integer = 3
  Private Const COLUMN_GRID_ITEM_ORDER As Integer = 4

  Dim _listImagesLayout As New Dictionary(Of Integer, String)
  Dim _listImagesLayoutItem As New Dictionary(Of Integer, String)

#End Region

#Region " Members "

  Private m_title As String
  Private m_delete_operation As Boolean
  Private m_section_schema As CLS_SECTION_SCHEMA
  Private m_class_base As CLS_SECTION_SCHEMA
  Private m_current_template_code As String
  Private m_is_item As Boolean
  Private m_parent_data As CLS_SECTION_SCHEMA.ParentData
  Private m_change_backimage As Boolean
  Private m_change_iconimage As Boolean
  Private m_default_parent_id As Integer

#End Region

#Region " Overrides "

  ' PURPOSE : Initializes the form id.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_SECTION_SCHEMA_EDIT

    m_class_base = New CLS_SECTION_SCHEMA

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE : Form controls initialization.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS  :
  Protected Overrides Sub GUI_InitControls()
    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7596)
    Me.lbl_Order.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7751)
    Me.lbl_title.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8145)
    Me.lbl_abstract.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7606)

    '' Set Label values
    Me.lbl_section_parent.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8143)
    Me.lbl_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8144)
    Me.lbl_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8140)

    Me.lbl_content.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8141)
    Me.lbl_subtitle.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7830)

    Me.lbl_button_text.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7609)

    Me.lbl_main_title.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7834)
    Me.lbl_main_subtitle.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7835)
    Me.lbl_headline_text.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8151)

    Me.gb_image_reference_1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8146)
    Me.gb_image_reference_2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8146)
    Me.gb_image_reference_3.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8146)
    Me.gb_superior_level.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8142)
    Me.gb_detail.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8148)
    Me.gb_items.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8153)

    Me.lbl_template.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8137)
    Me.lbl_layout.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8138)
    Me.lbl_layout_item.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8138)
    Me.lbl_back_image.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8152)
    Me.lbl_icon_image.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8147)
    Me.chk_visible.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(754)
    Me.lbl_description.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8154)
    Me.gb_childrens.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8185)

    Me.txt_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_SECTION_NAME)
    Me.txt_main_title.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_SECTION_TITLE)
    Me.txt_main_subtitle.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_SECTION_SUB_TITLE)
    Me.txt_button_text.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_SECTION_BUTTON)
    Me.txt_headline_text.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_SECTION_HEADLINE)
    Me.txt_subtitle.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_SECTION_MAIN_SUB_TITLE)
    Me.txt_title.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_SECTION_MAIN_TITLE)
    Me.txt_abstract.MaxLength = LEN_SECTION_DESCRIPTION
    Me.txt_description.MaxLength = LEN_SECTION_DESCRIPTION
    Me.ef_order.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, LEN_SECTION_ORDER)

    Me.btn_delete_child.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2191)
    Me.btn_add_child.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7570)

    'Voy a buscar las imagenes la primera vez
    _listImagesLayout = CLS_SECTION_SCHEMA.SetListImageLayout()
    _listImagesLayoutItem = CLS_SECTION_SCHEMA.SetListImageLayoutItem()

    LoadSectionsSchemas()

    If m_default_parent_id > 0 Then
      Me.uc_section_parent.Value = m_default_parent_id
      Me.uc_section_parent.Enabled = False
    End If

    LoadTemplates()
    LoadLayouts()
    LoadLayoutsItems()
    LoadImageFromLayout()
    LoadImageFromLayoutItem()
    LoadParentData()
    SetGridItems()

    AddHandler uc_template.ValueChangedEvent, AddressOf Me.uc_template_ValueChangedEvent
    AddHandler uc_layout.ValueChangedEvent, AddressOf Me.uc_layout_ValueChangedEvent
    AddHandler uc_section_parent.ValueChangedEvent, AddressOf Me.uc_section_parent_ValueChangedEvent
    AddHandler uc_layout_item.ValueChangedEvent, AddressOf Me.uc_layout_item_ValueChangedEvent

  End Sub

  ' PURPOSE : Set initial data on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)


    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then

      If (m_parent_data Is Nothing) Then
        If uc_template.Count > 0 Then
          uc_template.SelectedIndex = 0
        End If
      ElseIf m_parent_data.LayoutCode <> "Carrousel" Then
        If uc_template.Count > 0 Then
          uc_template.SelectedIndex = 0
        End If
      End If

      If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW And uc_section_parent.Value > 0 Then
        ef_order.Value = m_class_base.GetNextOrderByParent(uc_section_parent.Value)
      End If

      gb_childrens.Visible = False

      Return
    End If

    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT And m_section_schema.Type = 2 Then

    End If

    Try

      m_section_schema = DbReadObject

      'Header
      Me.txt_name.Value = m_section_schema.Name
      Me.ef_order.Value = m_section_schema.Order
      Me.chk_visible.Checked = m_section_schema.Active
      Me.uc_section_parent.Value = m_section_schema.ParentId
      Me.uc_template.Value = m_section_schema.TemplateId
      Me.uc_layout.Value = m_section_schema.LayoutId

      'Superior level
      Me.txt_main_title.Value = m_section_schema.MainTitle
      Me.txt_main_subtitle.Value = m_section_schema.MainSubTitle
      Me.txt_button_text.Value = m_section_schema.Button
      Me.uc_image_icon.Image = m_section_schema.IconImage
      Me.txt_abstract.Text = m_section_schema.Abstract

      'Details
      Me.txt_headline_text.Value = m_section_schema.HeadLine
      Me.txt_subtitle.Value = m_section_schema.SubTitle
      Me.txt_title.Value = m_section_schema.Title
      Me.uc_image_background_detail.Image = m_section_schema.BackGroundImage
      Me.txt_description.Text = m_section_schema.Description

      'Items
      If uc_layout_item.Count > 0 Then
        Me.uc_layout_item.Value = m_section_schema.LayoutItemId
      End If

      Select Case m_section_schema.Type
        Case 1
          Me.lbl_content.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8141)
        Case 2
          Me.lbl_content.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8242)
      End Select

      If m_section_schema.TemplateId > 0 Then
        If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT And m_class_base.TemplateCodes.Item(m_section_schema.TemplateId) = "SectionsListTemplate" Then
          LoadGridChildrens()
        Else
          Me.gb_childrens.Visible = False
        End If
      Else
        Me.gb_childrens.Visible = False
      End If

      If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT And m_section_schema.Type = 2 Then

        RemoveHandler uc_template.ValueChangedEvent, AddressOf Me.uc_template_ValueChangedEvent
        RemoveHandler uc_section_parent.ValueChangedEvent, AddressOf Me.uc_section_parent_ValueChangedEvent
        RemoveHandler uc_layout_item.ValueChangedEvent, AddressOf Me.uc_layout_item_ValueChangedEvent
        RemoveHandler uc_layout.ValueChangedEvent, AddressOf Me.uc_layout_ValueChangedEvent

        If m_section_schema.LayoutId = 0 Then
          uc_layout.Clear()
          uc_layout.Add(0, "---")
          uc_layout.SelectedIndex = 0
          pb_image_reference_detail.Image = Nothing
        End If

        If m_section_schema.TemplateId = 0 Then
          uc_template.Clear()
          uc_template.Add(0, "---")
          uc_template.SelectedIndex = 0
        End If

        uc_section_parent.Enabled = False
        uc_template.Enabled = False
        uc_layout.Enabled = False
        txt_name.Enabled = False
        gb_childrens.Enabled = False

      End If

    Catch
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7662) + " " + GLB_NLS_GUI_PLAYER_TRACKING.Id(7661), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(7662), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7661))
    End Try

  End Sub ' GUI_SetScreenData

  ' PURPOSE : Get data from the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_GetScreenData()

    Try

      m_section_schema = DbEditedObject

      'Encabezado
      m_section_schema.Name = Me.txt_name.Value
      m_section_schema.Order = Me.ef_order.Value
      m_section_schema.Active = Me.chk_visible.Checked
      m_section_schema.TemplateId = Me.uc_template.Value
      m_section_schema.ParentId = Me.uc_section_parent.Value
      m_section_schema.Active = Me.chk_visible.Checked

      If ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
        m_section_schema.Type = 1
      End If

      m_section_schema.Deleted = False
      m_section_schema.LayoutId = Me.uc_layout.Value

      'Nivel Superior
      m_section_schema.MainTitle = Me.txt_main_title.Value
      m_section_schema.MainSubTitle = Me.txt_main_subtitle.Value
      m_section_schema.Button = Me.txt_button_text.Value
      m_section_schema.Abstract = Me.txt_abstract.Text

      If Not Me.uc_image_icon.Image Is Nothing Then
        m_section_schema.IconImage = Me.uc_image_icon.Image
      Else
        m_change_iconimage = True
        m_section_schema.IconImage = Nothing
      End If
      If m_change_iconimage Then
        m_section_schema.IconImageId = 0
      End If

      'Detalle
      m_section_schema.HeadLine = Me.txt_headline_text.Value
      m_section_schema.SubTitle = Me.txt_subtitle.Value
      m_section_schema.Title = Me.txt_title.Value
      m_section_schema.Description = Me.txt_description.Text

      If Not Me.uc_image_background_detail.Image Is Nothing Then
        m_section_schema.BackGroundImage = Me.uc_image_background_detail.Image
      Else
        m_change_backimage = True
        m_section_schema.BackGroundImage = Nothing
      End If

      If m_change_backimage Then
        m_section_schema.BackGroundImageId = 0
      End If

      'Items
      m_section_schema.LayoutItemId = Me.uc_layout_item.Value

    Catch
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7662) + " " + GLB_NLS_GUI_PLAYER_TRACKING.Id(7661), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(7662), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7661))

    End Try

  End Sub ' GUI_GetScreenData

  ' PURPOSE : Validate the data presented on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    If String.IsNullOrEmpty(Me.txt_name.Value) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(7828))
      Call txt_name.Focus()
      Return False
    Else
      If Me.txt_name.Value.Trim().Contains(" ") Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(7828))
        Call txt_name.Focus()
        Return False
      End If
    End If

    If Convert.ToInt32(ef_order.Value) <= 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8162), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Return False
    Else
      If Not m_class_base.IsOrderValid(Convert.ToInt32(ef_order.Value), uc_section_parent.Value, Convert.ToInt64(DbObjectId)) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8162), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Return False
      End If
    End If

    If String.IsNullOrEmpty(Me.txt_main_title.Value) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(7829))
      Call txt_main_title.Focus()
      Return False
    End If


    If uc_section_parent.TextValue = "Home" Then

      If String.IsNullOrEmpty(Me.txt_main_subtitle.Value) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(7830))
        Call txt_name.Focus()

        Return False
      End If

      If String.IsNullOrEmpty(Me.txt_button_text.Value) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(7609))
        Call txt_name.Focus()
        Return False
      End If

      If String.IsNullOrEmpty(Me.txt_abstract.Text) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(7606))
        Call txt_abstract.Focus()
        Return False
      End If

    Else

      If m_parent_data.AbstractMandatory Then
        If String.IsNullOrEmpty(Me.txt_abstract.Text) Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(7606))
          Call txt_abstract.Focus()
          Return False
        End If
      End If

      If m_parent_data.IconMandatory Then
        If (Me.uc_image_icon.Image Is Nothing) Then
          ' Must have picture
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8161), ENUM_MB_TYPE.MB_TYPE_WARNING)
          Return False
        End If
      End If

    End If

    If String.IsNullOrEmpty(Me.txt_title.Value) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(8149))
      Call txt_title.Focus()

      Return False
    End If

    'If (Me.uc_image_background_detail.Image Is Nothing) Then
    '  ' Must have picture
    '  Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8160), ENUM_MB_TYPE.MB_TYPE_WARNING)
    '  Return False
    'End If

    Return True
  End Function ' GUI_IsScreenDataOk

  ' PURPOSE : Init form in new mode
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '
  Public Overloads Sub ShowNewItem(ByVal ParentId As Integer)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW
    m_default_parent_id = ParentId
    DbStatus = ENUM_STATUS.STATUS_OK

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowNewItem

  ' PURPOSE : Init form in edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '       - UserId
  '       - Username
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Overloads Sub ShowEditItem(ByVal id As Integer)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Me.DbObjectId = id

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    Else
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7661), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(7662), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1316))

    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If
  End Sub ' ShowEditItem

  ' PURPOSE : Database overridable operations. 
  '           Define specific DB operation for this form.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)


    Dim _rc As Integer

    Select Case DbOperation

      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        Me.DbEditedObject = New CLS_SECTION_SCHEMA
        m_section_schema = Me.DbEditedObject

        With m_section_schema
          .SectionSchemaId = Me.DbObjectId
        End With
        Me.DbStatus = ENUM_STATUS.STATUS_OK

      Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE

        m_delete_operation = False

        ' Delete confirmation
        _rc = NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(718), _
                                 ENUM_MB_TYPE.MB_TYPE_WARNING, _
                                 ENUM_MB_BTN.MB_BTN_YES_NO, _
                                 ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
                                 Me.m_title)
        If _rc = ENUM_MB_RESULT.MB_RESULT_YES Then
          m_delete_operation = True
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_DELETE
        If m_delete_operation Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        Else
          DbStatus = ENUM_STATUS.STATUS_ERROR
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)
    Call MyBase.GUI_ButtonClick(ButtonId)
  End Sub

#End Region

#Region "Private Functions"

  Private Sub LoadSectionsSchemas()

    uc_section_parent.Clear()
    uc_section_parent.Add(m_class_base.GetSections(Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT And m_section_schema.Type = 2))

  End Sub

  Private Sub LoadTemplates()
    uc_template.Clear()
    uc_template.Add(m_class_base.GetTemplates())
  End Sub

  Private Sub LoadLayouts()
    uc_layout.Clear()
    uc_layout.Add(m_class_base.GetLayouts(uc_template.Value))
  End Sub

  Private Sub LoadLayoutsItems()

    uc_layout_item.Clear()
    pb_image_reference_items.Image = Nothing

    gb_items.Enabled = False

    If uc_template.Value > 0 Then
      If m_class_base.TemplateCodes.Item(uc_template.Value) = "PromotionalContentList" Or m_class_base.TemplateCodes.Item(uc_template.Value) = "AdvertisingCampaignsList" Then
        gb_items.Enabled = True
        uc_layout_item.Add(m_class_base.GetLayouts(uc_template.Value, True))
      Else
        gb_items.Enabled = False
      End If
    End If


  End Sub

  Private Sub LoadImageFromLayout()
    If uc_layout.Value > 0 Then
      pb_image_reference_detail.Image = m_class_base.GetImageLayout(_listImagesLayout, "_detail_", uc_layout.Value)
    End If
  End Sub

  Private Sub LoadImageFromLayoutItem()
    If uc_layout.Value > 0 Then
      pb_image_reference_items.Image = m_class_base.GetImageLayoutItem(_listImagesLayoutItem, "_detail_", uc_layout_item.Value)
    End If
  End Sub

  Private Sub LoadParentData()

    If uc_section_parent.TextValue = "Home" Then

      pb_image_reference_superior.Image = m_class_base.GetImageHome()

    Else
      m_parent_data = m_class_base.GetParentData(uc_section_parent.Value)
      pb_image_reference_superior.Image = m_class_base.GetImageParentLayout(_listImagesLayout, m_parent_data.LayoutId)

      If m_parent_data.LayoutCode = "Carrousel" Then
        uc_template.Value = GetTemplateIdByCode("PromotionalContentList")
        LoadLayouts()
        uc_layout.Value = GetLayoutIdByCode("MosaicItems")
        LoadImageFromLayout()
        LoadLayoutsItems()
        LoadImageFromLayoutItem()

        uc_template.Enabled = False
        uc_layout.Enabled = False
      Else
        uc_template.Enabled = True
        uc_layout.Enabled = True
      End If
    End If

    ResizeSuperiorLevel()

  End Sub

  Private Function GetTemplateIdByCode(ByVal Code As String) As Integer

    If m_class_base.TemplateCodes.ContainsValue(Code) Then

      Dim pair As KeyValuePair(Of Integer, String)

      For Each pair In m_class_base.TemplateCodes
        If pair.Value = Code Then
          Return pair.Key
        End If
      Next
    Else
      Return 0
    End If
  End Function

  Private Function GetLayoutIdByCode(ByVal Code As String) As Integer

    If m_class_base.LayoutCodes.ContainsValue(Code) Then

      Dim pair As KeyValuePair(Of Integer, String)

      For Each pair In m_class_base.LayoutCodes
        If pair.Value = Code Then
          Return pair.Key
        End If
      Next
    Else
      Return 0
    End If
  End Function


  Private Sub ResizeSuperiorLevel()

    If uc_section_parent.TextValue = "Home" Then

      lbl_abstract.Location = New Point(7, 100)
      txt_abstract.Location = New Point(113, 97)
      txt_main_subtitle.Visible = True
      txt_button_text.Visible = True
      lbl_subtitle.Visible = True
      lbl_button_text.Visible = True
      uc_image_icon.Visible = False
      lbl_icon_image.Visible = False
      lbl_abstract.Visible = True
      txt_abstract.Visible = True
      uc_image_icon.Location = New Point(113, 174)
      lbl_icon_image.Location = New Point(7, 189)
      Return

    Else

      txt_main_subtitle.Visible = False
      txt_button_text.Visible = False
      lbl_subtitle.Visible = False
      lbl_button_text.Visible = False
      uc_image_icon.Visible = True
      lbl_icon_image.Visible = True
      lbl_abstract.Visible = True
      txt_abstract.Visible = True
      lbl_abstract.Location = New Point(7, 48)
      txt_abstract.Location = New Point(113, 48)
      uc_image_icon.Location = New Point(113, 130)
      lbl_icon_image.Location = New Point(7, 145)

      If Not m_parent_data.IconMandatory Then
        uc_image_icon.Visible = False
        lbl_icon_image.Visible = False
      End If

      If Not m_parent_data.AbstractMandatory Then
        lbl_abstract.Visible = False
        txt_abstract.Visible = False
      End If

    End If

  End Sub

  Private Sub uc_template_ValueChangedEvent()
    If uc_template.Value > 0 Then
      m_current_template_code = m_class_base.TemplateCodes.Item(uc_template.Value)
    Else
      m_current_template_code = ""
    End If

    LoadLayouts()
    LoadImageFromLayout()
    LoadLayoutsItems()
    LoadImageFromLayoutItem()
  End Sub

  Private Sub uc_layout_ValueChangedEvent()
    LoadImageFromLayout()
  End Sub

  Private Sub uc_section_parent_ValueChangedEvent()

    LoadParentData()

    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
      ef_order.Value = m_class_base.GetNextOrderByParent(uc_section_parent.Value)
    Else
      If uc_section_parent.Value <> m_section_schema.ParentId Then
        ef_order.Value = m_class_base.GetNextOrderByParent(uc_section_parent.Value)
      Else
        ef_order.Value = m_section_schema.Order
      End If

    End If

  End Sub

  Private Sub uc_layout_item_ValueChangedEvent()
    LoadImageFromLayoutItem()
  End Sub

  Private Sub SetGridItems()

    With Me.uc_grid_childrens
      Call .Init(5, 1)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
      .Sortable = False

      ' ID
      With .Column(COLUMN_GRID_ITEM_ID)
        .Width = 0
      End With

      'PARENT NAME
      With .Column(COLUMN_GRID_ITEM_SUPERIOR_NAME)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8143) ' Superior level
        .Width = 5000
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      End With

      'NAME
      With .Column(COLUMN_GRID_ITEM_TITLE)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8145) ' Title
        .Width = 7000
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      End With

      'MAIN TITLE
      With .Column(COLUMN_GRID_ITEM_VISIBLE)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(754) ' Visible
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
        .Width = 1000
      End With

      ' ACTIVE
      With .Column(COLUMN_GRID_ITEM_ORDER)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7751) ' Order
        .Width = 1000
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      End With

    End With
  End Sub

  Private Sub LoadGridChildrens()
    Dim _max_rows As Integer
    Dim _table As DataTable
    Dim _idx_row As Integer
    Dim _count As Integer
    Dim _more_than_max As Boolean
    Dim _db_row As CLASS_DB_ROW
    Dim _grid_rows As Integer

    _table = Nothing

    Try

      _table = m_section_schema.GetSectionsListByParentId(m_section_schema.SectionSchemaId)

      _grid_rows = _table.Rows.Count

      _more_than_max = False

      _count = 0
      _idx_row = 0

      Me.uc_grid_childrens.Clear()

      For _idx_row = 0 To _table.Rows.Count - 1

        If _idx_row >= Me.uc_grid_childrens.NumRows Then
          Me.uc_grid_childrens.AddRow()
        End If

        _db_row = New CLASS_DB_ROW(_table.Rows(_idx_row))
        Me.uc_grid_childrens.Redraw = False

        If SetupRow(_idx_row, _db_row) Then
          Me.uc_grid_childrens.Redraw = True
        End If

      Next

      If _more_than_max Then
        Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(111), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, , , CStr(_max_rows))
      End If

    Catch ex As Exception
      ' An error has occurred in the query execution
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "Cash Monitor", _
                            "LoadGridChildrens", _
                            ex.Message)
      Me.uc_grid_childrens.Redraw = True
    Finally

    End Try
  End Sub

  Public Function SetupRow(ByVal RowIndex As Integer, _
                                       ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean


    Dim _active As String

    Me.uc_grid_childrens.Cell(RowIndex, COLUMN_GRID_ITEM_ID).Value = DbRow.Value(0).ToString()
    Me.uc_grid_childrens.Cell(RowIndex, COLUMN_GRID_ITEM_SUPERIOR_NAME).Value = Me.m_section_schema.Name
    Me.uc_grid_childrens.Cell(RowIndex, COLUMN_GRID_ITEM_TITLE).Value = DbRow.Value(3).ToString()
    Me.uc_grid_childrens.Cell(RowIndex, COLUMN_GRID_ITEM_ORDER).Value = DbRow.Value(16).ToString

    If CType(DbRow.Value(11), Boolean) Then
      _active = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278) ' Yes
    Else
      _active = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279) ' No
    End If

    Me.uc_grid_childrens.Cell(RowIndex, COLUMN_GRID_ITEM_VISIBLE).Value = _active

    Return True


  End Function

#End Region

#Region "Events"


  Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
    Dim frm As frm_image_visualizer
    frm = New frm_image_visualizer(pb_image_reference_superior.Image)
    frm.ShowDialog()
  End Sub

  Private Sub LinkLabel2_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel2.LinkClicked
    Dim frm As frm_image_visualizer
    frm = New frm_image_visualizer(pb_image_reference_detail.Image)
    frm.ShowDialog()
  End Sub

  Private Sub LinkLabel3_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel3.LinkClicked
    Dim frm As frm_image_visualizer
    frm = New frm_image_visualizer(pb_image_reference_items.Image)
    frm.ShowDialog()
  End Sub

  Private Sub uc_image_icon_ImageSelected(ByRef [Continue] As Boolean, ImageSelected As Image) Handles uc_image_icon.ImageSelected
    m_change_iconimage = True
  End Sub

  Private Sub uc_image_background_detail_ImageSelected(ByRef [Continue] As Boolean, ImageSelected As Image) Handles uc_image_background_detail.ImageSelected
    m_change_backimage = True
  End Sub

  Private Sub btn_delete_child_Click(sender As Object, e As EventArgs) Handles btn_delete_child.Click

    Dim _idx_row As Int32
    Dim _sch_id As Integer
    Dim _name As String
    Dim _rows_to_move() As Integer

    _rows_to_move = Me.uc_grid_childrens.SelectedRows()

    ' ICS 18-DEC-2012: Check if there is a row selected
    If _rows_to_move Is Nothing Then
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(130), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Return
    End If

    _idx_row = _rows_to_move(0)

    _sch_id = Me.uc_grid_childrens.Cell(_idx_row, COLUMN_GRID_ITEM_ID).Value
    _name = Me.uc_grid_childrens.Cell(_idx_row, COLUMN_GRID_ITEM_TITLE).Value

    If _sch_id > 0 Then

      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6537), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, _
         mdl_NLS.ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8231) + " " + _name) = ENUM_MB_RESULT.MB_RESULT_YES Then

        If m_class_base.DeleteSection(_sch_id) Then
          LoadGridChildrens()
        End If

      End If

    End If

  End Sub

  Private Sub btn_add_child_Click_1(sender As Object, e As EventArgs) Handles btn_add_child.Click
    Dim frm_edit As frm_new_section_schema_edit

    frm_edit = New frm_new_section_schema_edit
    Call frm_edit.ShowNewItem(m_section_schema.SectionSchemaId)
    frm_edit = Nothing
    LoadGridChildrens()

  End Sub


  Private Sub uc_grid_childrens_DataSelectedEvent() Handles uc_grid_childrens.DataSelectedEvent
    Dim _idx_row As Int32
    Dim _sch_id As Integer
    'Dim _sch_name As String
    Dim _frm_edit As frm_new_section_schema_edit  'GUI_Controls.frm_base_edit
    Dim _rows_to_move() As Integer

    _rows_to_move = Me.uc_grid_childrens.SelectedRows()

    ' ICS 18-DEC-2012: Check if there is a row selected
    If _rows_to_move Is Nothing Then
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(130), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Return
    End If

    _idx_row = _rows_to_move(0)
    _sch_id = Me.uc_grid_childrens.Cell(_idx_row, COLUMN_GRID_ITEM_ID).Value
    _frm_edit = New frm_new_section_schema_edit
    Call _frm_edit.ShowEditItem(_sch_id)
    _frm_edit = Nothing
    LoadGridChildrens()
    Call Me.uc_grid_childrens.Focus()

  End Sub

#End Region


End Class