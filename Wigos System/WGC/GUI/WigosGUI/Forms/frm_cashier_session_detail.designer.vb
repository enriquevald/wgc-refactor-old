<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_cashier_session_detail
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.gb_header = New System.Windows.Forms.GroupBox
    Me.gb_currencies = New System.Windows.Forms.GroupBox
    Me.lbl_currencies_detail = New System.Windows.Forms.Label
    Me.lbl_currencies_site = New System.Windows.Forms.Label
    Me.txt_currencies_sites = New System.Windows.Forms.TextBox
    Me.txt_currencies_details = New System.Windows.Forms.TextBox
    Me.tf_cs_sites = New GUI_Controls.uc_text_field
    Me.tf_cm_to_date = New GUI_Controls.uc_text_field
    Me.tf_cm_from_date = New GUI_Controls.uc_text_field
    Me.tf_cs_close_date = New GUI_Controls.uc_text_field
    Me.tf_cs_open_date = New GUI_Controls.uc_text_field
    Me.tf_cs_user_name = New GUI_Controls.uc_text_field
    Me.tf_cs_cash_desk_name = New GUI_Controls.uc_text_field
    Me.tf_cs_status = New GUI_Controls.uc_text_field
    Me.lbl_cs_name = New GUI_Controls.uc_text_field
    Me.gb_detail = New System.Windows.Forms.GroupBox
    Me.web_browser = New System.Windows.Forms.WebBrowser
    Me.panel_data.SuspendLayout()
    Me.gb_header.SuspendLayout()
    Me.gb_currencies.SuspendLayout()
    Me.gb_detail.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_header)
    Me.panel_data.Controls.Add(Me.gb_detail)
    Me.panel_data.Size = New System.Drawing.Size(789, 765)
    '
    'gb_header
    '
    Me.gb_header.Controls.Add(Me.gb_currencies)
    Me.gb_header.Controls.Add(Me.tf_cs_sites)
    Me.gb_header.Controls.Add(Me.tf_cm_to_date)
    Me.gb_header.Controls.Add(Me.tf_cm_from_date)
    Me.gb_header.Controls.Add(Me.tf_cs_close_date)
    Me.gb_header.Controls.Add(Me.tf_cs_open_date)
    Me.gb_header.Controls.Add(Me.tf_cs_user_name)
    Me.gb_header.Controls.Add(Me.tf_cs_cash_desk_name)
    Me.gb_header.Controls.Add(Me.tf_cs_status)
    Me.gb_header.Controls.Add(Me.lbl_cs_name)
    Me.gb_header.Location = New System.Drawing.Point(3, 3)
    Me.gb_header.Name = "gb_header"
    Me.gb_header.Size = New System.Drawing.Size(779, 97)
    Me.gb_header.TabIndex = 8
    Me.gb_header.TabStop = False
    '
    'gb_currencies
    '
    Me.gb_currencies.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.gb_currencies.Controls.Add(Me.lbl_currencies_detail)
    Me.gb_currencies.Controls.Add(Me.lbl_currencies_site)
    Me.gb_currencies.Controls.Add(Me.txt_currencies_sites)
    Me.gb_currencies.Controls.Add(Me.txt_currencies_details)
    Me.gb_currencies.Location = New System.Drawing.Point(327, 0)
    Me.gb_currencies.Name = "gb_currencies"
    Me.gb_currencies.Size = New System.Drawing.Size(452, 97)
    Me.gb_currencies.TabIndex = 134
    Me.gb_currencies.TabStop = False
    '
    'lbl_currencies_detail
    '
    Me.lbl_currencies_detail.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.lbl_currencies_detail.Location = New System.Drawing.Point(220, 14)
    Me.lbl_currencies_detail.Name = "lbl_currencies_detail"
    Me.lbl_currencies_detail.Size = New System.Drawing.Size(79, 13)
    Me.lbl_currencies_detail.TabIndex = 141
    Me.lbl_currencies_detail.Text = "xCurrenciesDetail"
    Me.lbl_currencies_detail.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'lbl_currencies_site
    '
    Me.lbl_currencies_site.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.lbl_currencies_site.Location = New System.Drawing.Point(2, 14)
    Me.lbl_currencies_site.Name = "lbl_currencies_site"
    Me.lbl_currencies_site.Size = New System.Drawing.Size(54, 13)
    Me.lbl_currencies_site.TabIndex = 140
    Me.lbl_currencies_site.Text = "xSites"
    Me.lbl_currencies_site.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'txt_currencies_sites
    '
    Me.txt_currencies_sites.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.txt_currencies_sites.BackColor = System.Drawing.SystemColors.Control
    Me.txt_currencies_sites.BorderStyle = System.Windows.Forms.BorderStyle.None
    Me.txt_currencies_sites.ForeColor = System.Drawing.Color.Blue
    Me.txt_currencies_sites.Location = New System.Drawing.Point(56, 14)
    Me.txt_currencies_sites.Multiline = True
    Me.txt_currencies_sites.Name = "txt_currencies_sites"
    Me.txt_currencies_sites.ReadOnly = True
    Me.txt_currencies_sites.Size = New System.Drawing.Size(150, 75)
    Me.txt_currencies_sites.TabIndex = 139
    Me.txt_currencies_sites.WordWrap = False
    '
    'txt_currencies_details
    '
    Me.txt_currencies_details.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.txt_currencies_details.BackColor = System.Drawing.SystemColors.Control
    Me.txt_currencies_details.BorderStyle = System.Windows.Forms.BorderStyle.None
    Me.txt_currencies_details.ForeColor = System.Drawing.Color.Blue
    Me.txt_currencies_details.Location = New System.Drawing.Point(300, 14)
    Me.txt_currencies_details.Multiline = True
    Me.txt_currencies_details.Name = "txt_currencies_details"
    Me.txt_currencies_details.ReadOnly = True
    Me.txt_currencies_details.Size = New System.Drawing.Size(146, 75)
    Me.txt_currencies_details.TabIndex = 138
    Me.txt_currencies_details.WordWrap = False
    '
    'tf_cs_sites
    '
    Me.tf_cs_sites.IsReadOnly = True
    Me.tf_cs_sites.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_cs_sites.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_cs_sites.Location = New System.Drawing.Point(550, 14)
    Me.tf_cs_sites.Name = "tf_cs_sites"
    Me.tf_cs_sites.Size = New System.Drawing.Size(192, 24)
    Me.tf_cs_sites.SufixText = "Sufix Text"
    Me.tf_cs_sites.SufixTextVisible = True
    Me.tf_cs_sites.TabIndex = 133
    Me.tf_cs_sites.TabStop = False
    Me.tf_cs_sites.TextWidth = 100
    Me.tf_cs_sites.Value = "xSites"
    '
    'tf_cm_to_date
    '
    Me.tf_cm_to_date.IsReadOnly = True
    Me.tf_cm_to_date.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_cm_to_date.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_cm_to_date.Location = New System.Drawing.Point(550, 66)
    Me.tf_cm_to_date.Name = "tf_cm_to_date"
    Me.tf_cm_to_date.Size = New System.Drawing.Size(216, 24)
    Me.tf_cm_to_date.SufixText = "Sufix Text"
    Me.tf_cm_to_date.SufixTextVisible = True
    Me.tf_cm_to_date.TabIndex = 132
    Me.tf_cm_to_date.TabStop = False
    Me.tf_cm_to_date.Value = "22/09/2202 22:20:58"
    Me.tf_cm_to_date.Visible = False
    '
    'tf_cm_from_date
    '
    Me.tf_cm_from_date.IsReadOnly = True
    Me.tf_cm_from_date.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_cm_from_date.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_cm_from_date.Location = New System.Drawing.Point(550, 41)
    Me.tf_cm_from_date.Name = "tf_cm_from_date"
    Me.tf_cm_from_date.Size = New System.Drawing.Size(217, 24)
    Me.tf_cm_from_date.SufixText = "Sufix Text"
    Me.tf_cm_from_date.SufixTextVisible = True
    Me.tf_cm_from_date.TabIndex = 131
    Me.tf_cm_from_date.TabStop = False
    Me.tf_cm_from_date.Value = "22/09/2202 22:20:58"
    Me.tf_cm_from_date.Visible = False
    '
    'tf_cs_close_date
    '
    Me.tf_cs_close_date.IsReadOnly = True
    Me.tf_cs_close_date.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_cs_close_date.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_cs_close_date.Location = New System.Drawing.Point(314, 64)
    Me.tf_cs_close_date.Name = "tf_cs_close_date"
    Me.tf_cs_close_date.Size = New System.Drawing.Size(216, 24)
    Me.tf_cs_close_date.SufixText = "Sufix Text"
    Me.tf_cs_close_date.SufixTextVisible = True
    Me.tf_cs_close_date.TabIndex = 126
    Me.tf_cs_close_date.TabStop = False
    Me.tf_cs_close_date.Value = "22/09/2202 22:20:58"
    '
    'tf_cs_open_date
    '
    Me.tf_cs_open_date.IsReadOnly = True
    Me.tf_cs_open_date.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_cs_open_date.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_cs_open_date.Location = New System.Drawing.Point(6, 64)
    Me.tf_cs_open_date.Name = "tf_cs_open_date"
    Me.tf_cs_open_date.Size = New System.Drawing.Size(222, 24)
    Me.tf_cs_open_date.SufixText = "Sufix Text"
    Me.tf_cs_open_date.SufixTextVisible = True
    Me.tf_cs_open_date.TabIndex = 125
    Me.tf_cs_open_date.TabStop = False
    Me.tf_cs_open_date.Value = "22/09/2202 22:20:58"
    '
    'tf_cs_user_name
    '
    Me.tf_cs_user_name.IsReadOnly = True
    Me.tf_cs_user_name.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_cs_user_name.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_cs_user_name.Location = New System.Drawing.Point(294, 39)
    Me.tf_cs_user_name.Name = "tf_cs_user_name"
    Me.tf_cs_user_name.Size = New System.Drawing.Size(250, 24)
    Me.tf_cs_user_name.SufixText = "Sufix Text"
    Me.tf_cs_user_name.SufixTextVisible = True
    Me.tf_cs_user_name.TabIndex = 124
    Me.tf_cs_user_name.TabStop = False
    Me.tf_cs_user_name.TextWidth = 100
    Me.tf_cs_user_name.Value = "xUser Name"
    '
    'tf_cs_cash_desk_name
    '
    Me.tf_cs_cash_desk_name.IsReadOnly = True
    Me.tf_cs_cash_desk_name.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_cs_cash_desk_name.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_cs_cash_desk_name.Location = New System.Drawing.Point(6, 39)
    Me.tf_cs_cash_desk_name.Name = "tf_cs_cash_desk_name"
    Me.tf_cs_cash_desk_name.Size = New System.Drawing.Size(302, 24)
    Me.tf_cs_cash_desk_name.SufixText = "Sufix Text"
    Me.tf_cs_cash_desk_name.SufixTextVisible = True
    Me.tf_cs_cash_desk_name.TabIndex = 123
    Me.tf_cs_cash_desk_name.TabStop = False
    Me.tf_cs_cash_desk_name.Value = "xCash Desk Name"
    '
    'tf_cs_status
    '
    Me.tf_cs_status.IsReadOnly = True
    Me.tf_cs_status.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_cs_status.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_cs_status.Location = New System.Drawing.Point(294, 14)
    Me.tf_cs_status.Name = "tf_cs_status"
    Me.tf_cs_status.Size = New System.Drawing.Size(250, 24)
    Me.tf_cs_status.SufixText = "Sufix Text"
    Me.tf_cs_status.SufixTextVisible = True
    Me.tf_cs_status.TabIndex = 122
    Me.tf_cs_status.TabStop = False
    Me.tf_cs_status.TextWidth = 100
    Me.tf_cs_status.Value = "xStatus"
    '
    'lbl_cs_name
    '
    Me.lbl_cs_name.IsReadOnly = True
    Me.lbl_cs_name.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_cs_name.LabelForeColor = System.Drawing.SystemColors.ControlText
    Me.lbl_cs_name.Location = New System.Drawing.Point(6, 14)
    Me.lbl_cs_name.Name = "lbl_cs_name"
    Me.lbl_cs_name.Size = New System.Drawing.Size(297, 24)
    Me.lbl_cs_name.SufixText = "Sufix Text"
    Me.lbl_cs_name.SufixTextVisible = True
    Me.lbl_cs_name.TabIndex = 10
    Me.lbl_cs_name.TextWidth = 0
    Me.lbl_cs_name.Value = "xCashier Session Name"
    '
    'gb_detail
    '
    Me.gb_detail.Controls.Add(Me.web_browser)
    Me.gb_detail.Location = New System.Drawing.Point(3, 101)
    Me.gb_detail.Name = "gb_detail"
    Me.gb_detail.Size = New System.Drawing.Size(779, 662)
    Me.gb_detail.TabIndex = 9
    Me.gb_detail.TabStop = False
    '
    'web_browser
    '
    Me.web_browser.Location = New System.Drawing.Point(19, 13)
    Me.web_browser.MinimumSize = New System.Drawing.Size(20, 20)
    Me.web_browser.Name = "web_browser"
    Me.web_browser.Size = New System.Drawing.Size(743, 642)
    Me.web_browser.TabIndex = 205
    '
    'frm_cashier_session_detail
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(891, 773)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_cashier_session_detail"
    Me.Text = "frm_cashier_session_detail"
    Me.panel_data.ResumeLayout(False)
    Me.gb_header.ResumeLayout(False)
    Me.gb_currencies.ResumeLayout(False)
    Me.gb_currencies.PerformLayout()
    Me.gb_detail.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_header As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_cs_name As GUI_Controls.uc_text_field
  Friend WithEvents tf_cs_status As GUI_Controls.uc_text_field
  Friend WithEvents tf_cs_close_date As GUI_Controls.uc_text_field
  Friend WithEvents tf_cs_open_date As GUI_Controls.uc_text_field
  Friend WithEvents tf_cs_user_name As GUI_Controls.uc_text_field
  Friend WithEvents tf_cs_cash_desk_name As GUI_Controls.uc_text_field
  Friend WithEvents gb_detail As System.Windows.Forms.GroupBox
  Friend WithEvents tf_cm_to_date As GUI_Controls.uc_text_field
  Friend WithEvents tf_cm_from_date As GUI_Controls.uc_text_field
  Friend WithEvents web_browser As System.Windows.Forms.WebBrowser
  Friend WithEvents tf_cs_sites As GUI_Controls.uc_text_field
  Friend WithEvents gb_currencies As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_currencies_detail As System.Windows.Forms.Label
  Friend WithEvents lbl_currencies_site As System.Windows.Forms.Label
  Friend WithEvents txt_currencies_sites As System.Windows.Forms.TextBox
  Friend WithEvents txt_currencies_details As System.Windows.Forms.TextBox
End Class
