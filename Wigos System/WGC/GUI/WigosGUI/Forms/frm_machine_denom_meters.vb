
'-------------------------------------------------------------------
' Copyright © 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_machine_denom_meters
' DESCRIPTION:   Selection base class
' AUTHOR:        Carlos Rodrigo
' CREATION DATE: 02-JUL-2015
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 02-JUL-2015 CSR    Initial work
' 06-AUG-2015 GMV    Reworking 
' 13-OCT 2015 ETP    BUG-FIXED 5136 Swaped columns WonCount and WonAmount.
' 13-OCT 2015 ETP    BUG-FIXED 5139 Removed Yellow line. 
' 11-NOV-2015 FOS    Product Backlog Item 5839:Garcia River change NLS
'-------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Data
Imports WSI.Common

Public Class frm_machine_denom_meters
  Inherits frm_base_sel

#Region " Constants "
  Private m_terminal_report_type As ReportType = ReportType.Provider
  Private m_refresh_grid As Boolean = True


  Private TERMINAL_DATA_COLUMNS As Integer = TerminalReport.NumColumns(m_terminal_report_type)
  ' Grid Columns
  Private GRID_COLUMN_INDEX As Integer = 0
  Private GRID_COLUMN_DATE_FROM As Integer = 1
  Private GRID_COLUMN_DATE_TO As Integer = 2

  Private GRID_COLUMN_PROVIDER_ID As Integer = 3 '4
  Private GRID_COLUMN_TERMINAL_NAME As Integer = 4 '5
  Private GRID_COLUMN_GAME_NAME As Integer = 5
  Private GRID_INIT_TERMINAL_DATA As Integer
  Private GRID_COLUMN_DENOMINATION As Integer = 6

  Private GRID_COLUMN_PLAYED_AMOUNT As Integer = 7
  Private GRID_COLUMN_WON_AMOUNT As Integer = 8
  Private GRID_COLUMN_JACKPOT_AMOUNT As Integer = 9
  Private GRID_COLUMN_PLAYED_COUNT As Integer = 10
  Private GRID_COLUMN_WON_COUNT As Integer = 11
  Private GRID_COLUMN_WON_TOTAL As Integer = 12
  Private GRID_COLUMN_PAYOUT_PCT As Integer = 13
  Private GRID_COLUMN_THEORICAL_PAYOUT_PCT As Integer = 14
  Private GRID_COLUMN_NETWIN As Integer = 15
  Private GRID_COLUMN_NETWIN_PCT As Integer = 16
  Private GRID_COLUMN_WON_PCT As Integer = 17
  Private GRID_COLUMN_BET_AVERAGE As Integer = 18


  ' DB Columns
  Private Const SQL_COLUMN_TERMINAL_ID As Integer = 0
  Private Const SQL_COLUMN_PROVIDER_ID As Integer = 1
  Private Const SQL_COLUMN_TERMINAL_NAME As Integer = 2
  Private Const SQL_COLUMN_BASEHOUR As Integer = 3
  Private Const SQL_COLUMN_DENOMINATION As Integer = 4
  Private Const SQL_COLUMN_PLAYED_AMOUNT As Integer = 5
  Private Const SQL_COLUMN_WON_AMOUNT As Integer = 6
  Private Const SQL_COLUMN_JACKPOT_AMOUNT As Integer = 7
  Private Const SQL_COLUMN_PLAYED_COUNT As Integer = 8
  Private Const SQL_COLUMN_WON_COUNT As Integer = 9
  Private Const SQL_COLUMN_TERMINAL_THEORETICAL_PAYOUT As Integer = 10
  Private Const SQL_COLUMN_GAME_NAME As Integer = 11


  ' Width
  Private Const GRID_COLUMN_WIDTH_SELECT As Integer = 200
  Private Const GRID_COLUMN_WIDTH_DATERANGE As Integer = 1700
  Private Const GRID_COLUMN_WIDTH_TERMINAL_NAME As Integer = 2200
  Private Const GRID_COLUMN_WIDTH_PROVIDER_ID As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_GAME_NAME As Integer = 1950

  Private Const GRID_COLUMN_WIDTH_DENOMINATION As Integer = 1400
  Private Const GRID_COLUMN_WIDTH_PLAYED_AMOUNT As Integer = 1250
  Private Const GRID_COLUMN_WIDTH_WON_AMOUNT As Integer = 1950
  Private Const GRID_COLUMN_WIDTH_JACKPOT_AMOUNT As Integer = 1600
  Private Const GRID_COLUMN_WIDTH_PLAYED_COUNT As Integer = 950
  Private Const GRID_COLUMN_WIDTH_WON_COUNT As Integer = 950
  Private Const GRID_COLUMN_WIDTH_WON_TOTAL As Integer = 1950
  Private Const GRID_COLUMN_WIDTH_PAYOUT_PCT As Integer = 1250
  Private Const GRID_COLUMN_WIDTH_NETWIN As Integer = 1250
  Private Const GRID_COLUMN_WIDTH_BET_AVERAGE As Integer = 1400
  Private Const GRID_COLUMN_WIDTH_WON_PCT As Integer = 1250
  Private Const GRID_COLUMN_WIDTH_NETWIN_PCT As Integer = 1250



  ' Grid configuration
  Private GRID_COLUMNS As Integer = 22
  Private Const GRID_HEADER_ROWS As Integer = 2


  'null date value denomination
  Private Const NULL_DATE_VALUE As String = "01/01/1900 00:00"

  ' Excel Columns
  Private Const EXCEL_COLUMN_PROVIDER_ID As Integer = 1
  Private Const EXCEL_COLUMN_TERMINAL_NAME As Integer = 2
  Private Const EXCEL_COLUMN_BANK_ID As Integer = 10
  Private Const EXCEL_COLUMN_LOCATION_LEVEL As Integer = 12
  Private Const EXCEL_COLUMN_VERSION As Integer = 14
  Private Const EXCEL_COLUMN_EXTERNAL_ID As Integer = 15
  Private Const EXCEL_COLUMN_VENDOR_ID As Integer = 16
  Private Const EXCEL_COLUMN_PROGRAM As Integer = 17
  Private Const EXCEL_COLUMN_REGISTRATION_CODE As Integer = 18
  Private Const EXCEL_COLUMN_SERIAL_NUMBER As Integer = 19
  Private Const EXCEL_COLUMN_MACHINE_ID As Integer = 20
  Private Const EXCEL_COLUMN_DENOMINATION As Integer = 21
  Private Const EXCEL_COLUMN_CABINET_TYPE As Integer = 23
  Private Const EXCEL_COLUMN_NUMBER_LINES As Integer = 28
  Private Const EXCEL_COLUMN_CONTRACT_TYPE As Integer = 30
  Private Const EXCEL_COLUMN_CONTRACT_ID As Integer = 31
  Private Const EXCEL_COLUMN_ORDER_NUMBER As Integer = 32

  'Stats Index
  Private Const STATS_TOTAL_KEY As Decimal = -1
  Private Const STATS_GROUP_KEY As Decimal = -2

  Private Const FORM_DB_MIN_VERSION As Short = 226 ' TODO: Real MIN_VERSION = 227

#End Region ' Constants 

#Region " Enums "

  Public Enum ENUM_TERMINALS_SELECTION_MODE
    NONE = 0
    ONE = 1
    MULTI = 2
  End Enum

#End Region

#Region " Members "

  'Strings
  Private m_terminal_name As String
  Private m_common_sql_select_part As String
  Private m_report_terminals As String
  Private m_report_terminal_type As String
  Private m_report_terminal_status As String
  Private m_serial_number As String
  Private m_asset_number As String

  'Integers
  Private m_timeout As Integer

  'Others types
  Private m_sel_params As TYPE_TERMINAL_SEL_PARAMS
  Private m_data_view_terminals As DataView
  Private m_stats As New Dictionary(Of Decimal, CLASS_MACHINE_DENOM_STATS)
  Private m_current_terminal_id As Integer
  Private m_current_terminal_name As String


  ' For terminal selection  
  Private m_selection_mode As ENUM_TERMINALS_SELECTION_MODE
  Private m_selected_terminal_id As Integer
  Private m_selected_multi_terminals_id As New List(Of Integer)

  'date report filters 
  Private m_date_to As String
  Private m_date_from As String


#End Region ' Members

#Region " Overrides "
  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_FORM.FORM_MACHINE_DENOM_METERS
    Call MyBase.GUI_SetFormId()
  End Sub
  ' PURPOSE: check filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Function GUI_FilterCheck() As Boolean

    If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate > Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    End If

    Return True
  End Function

  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _terminal_id As Integer = DbRow.Value(SQL_COLUMN_TERMINAL_ID)

    If (m_current_terminal_id <> -1 AndAlso _terminal_id <> m_current_terminal_id) Then
      PrintGridSubtotal()
      m_stats.Remove(STATS_GROUP_KEY)
      m_stats.Add(STATS_GROUP_KEY, New CLASS_MACHINE_DENOM_STATS())
      m_current_terminal_id = _terminal_id
      m_current_terminal_name = DbRow.Value(SQL_COLUMN_TERMINAL_NAME)
    End If

    Return True

  End Function


  Protected Overrides Sub GUI_AfterLastRow()
    Dim idx_row As Integer

    'Última linea de subtotal
    If (m_current_terminal_id <> -1) Then
      PrintGridSubtotal()
    End If

    'Denomination Totals Row
    For Each _key As Decimal In m_stats.Keys
      If (_key <> STATS_TOTAL_KEY AndAlso _key <> STATS_GROUP_KEY) Then
        Me.Grid.AddRow()
        idx_row = Me.Grid.NumRows - 1

        ' Date from
        If m_date_from <> "" Then
          Me.Grid.Cell(idx_row, GRID_COLUMN_DATE_FROM).Value = GUI_FormatDate(m_date_from, _
                                                                               ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                               ENUM_FORMAT_TIME.FORMAT_HHMM)
        Else
          Me.Grid.Cell(idx_row, GRID_COLUMN_DATE_FROM).Value = ""
        End If

        ' Date to
        If m_date_to <> "" Then
          Me.Grid.Cell(idx_row, GRID_COLUMN_DATE_TO).Value = GUI_FormatDate(m_date_to, _
                                                                             ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                             ENUM_FORMAT_TIME.FORMAT_HHMM)
        Else
          Me.Grid.Cell(idx_row, GRID_COLUMN_DATE_TO).Value = ""
        End If


        ' Denomination
        Me.Grid.Cell(idx_row, GRID_COLUMN_DENOMINATION).Value = GUI_FormatCurrency(_key, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        ' Played Amount
        Me.Grid.Cell(idx_row, GRID_COLUMN_PLAYED_AMOUNT).Value = GUI_FormatCurrency(m_stats(_key).PlayedAmount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        ' Won Amount
        Me.Grid.Cell(idx_row, GRID_COLUMN_WON_AMOUNT).Value = GUI_FormatCurrency(m_stats(_key).CoinOutAmount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        ' Jackpot Amount
        Me.Grid.Cell(idx_row, GRID_COLUMN_JACKPOT_AMOUNT).Value = GUI_FormatCurrency(m_stats(_key).JackpotAmount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        ' Played count
        Me.Grid.Cell(idx_row, GRID_COLUMN_PLAYED_COUNT).Value = GUI_FormatNumber(m_stats(_key).PlayedCount, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
        ' Won count
        Me.Grid.Cell(idx_row, GRID_COLUMN_WON_COUNT).Value = GUI_FormatNumber(m_stats(_key).WonCount, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
        'Won GrandTotal
        Me.Grid.Cell(idx_row, GRID_COLUMN_WON_TOTAL).Value = GUI_FormatCurrency(m_stats(_key).WonAmount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        'NetWin
        Me.Grid.Cell(idx_row, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(m_stats(_key).NetWinAmount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        'BetAverage
        Me.Grid.Cell(idx_row, GRID_COLUMN_BET_AVERAGE).Value = GUI_FormatCurrency(m_stats(_key).BetAverageAmount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        'Payout pct
        Me.Grid.Cell(idx_row, GRID_COLUMN_PAYOUT_PCT).Value = GUI_FormatNumber(m_stats(_key).PayOutPercentage, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"
        'Won pct
        Me.Grid.Cell(idx_row, GRID_COLUMN_WON_PCT).Value = GUI_FormatNumber(m_stats(_key).WonPercentage, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"
        'NetWin pct
        Me.Grid.Cell(idx_row, GRID_COLUMN_NETWIN_PCT).Value = GUI_FormatNumber(m_stats(_key).NetWinPercentage, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"
        'Payout Theoretical pct
        Me.Grid.Cell(idx_row, GRID_COLUMN_THEORICAL_PAYOUT_PCT).Value = GUI_FormatNumber(m_stats(_key).TheoreticalPayOutPercentage, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)

        ' Color Row
        Me.Grid.Row(idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_01)
      End If
    Next


    'Grand Totals Row



    Me.Grid.AddRow()
    idx_row = Me.Grid.NumRows - 1

    Me.Grid.Cell(idx_row, GRID_COLUMN_DATE_TO).Value = GLB_NLS_GUI_STATISTICS.GetString(203)
    ' Played Amount
    Me.Grid.Cell(idx_row, GRID_COLUMN_PLAYED_AMOUNT).Value = GUI_FormatCurrency(m_stats(STATS_TOTAL_KEY).PlayedAmount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    ' Won Amount
    Me.Grid.Cell(idx_row, GRID_COLUMN_WON_AMOUNT).Value = GUI_FormatCurrency(m_stats(STATS_TOTAL_KEY).CoinOutAmount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    ' Jackpot Amount
    Me.Grid.Cell(idx_row, GRID_COLUMN_JACKPOT_AMOUNT).Value = GUI_FormatCurrency(m_stats(STATS_TOTAL_KEY).JackpotAmount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    ' Played count
    Me.Grid.Cell(idx_row, GRID_COLUMN_PLAYED_COUNT).Value = GUI_FormatNumber(m_stats(STATS_TOTAL_KEY).PlayedCount, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
    ' Won count
    Me.Grid.Cell(idx_row, GRID_COLUMN_WON_COUNT).Value = GUI_FormatNumber(m_stats(STATS_TOTAL_KEY).WonCount, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
    'Won GrandTotal
    Me.Grid.Cell(idx_row, GRID_COLUMN_WON_TOTAL).Value = GUI_FormatCurrency(m_stats(STATS_TOTAL_KEY).WonAmount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    'NetWin
    Me.Grid.Cell(idx_row, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(m_stats(STATS_TOTAL_KEY).NetWinAmount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    'BetAverage
    Me.Grid.Cell(idx_row, GRID_COLUMN_BET_AVERAGE).Value = GUI_FormatCurrency(m_stats(STATS_TOTAL_KEY).BetAverageAmount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    'Payout pct
    Me.Grid.Cell(idx_row, GRID_COLUMN_PAYOUT_PCT).Value = GUI_FormatNumber(m_stats(STATS_TOTAL_KEY).PayOutPercentage, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"
    'Won pct
    Me.Grid.Cell(idx_row, GRID_COLUMN_WON_PCT).Value = GUI_FormatNumber(m_stats(STATS_TOTAL_KEY).WonPercentage, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"
    'NetWin pct
    Me.Grid.Cell(idx_row, GRID_COLUMN_NETWIN_PCT).Value = GUI_FormatNumber(m_stats(STATS_TOTAL_KEY).NetWinPercentage, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"
    'Payout Theoretical pct
    Me.Grid.Cell(idx_row, GRID_COLUMN_THEORICAL_PAYOUT_PCT).Value = GUI_FormatNumber(m_stats(STATS_TOTAL_KEY).TheoreticalPayOutPercentage, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)

    ' Color Row
    Me.Grid.Row(idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

  End Sub ' GUI_AfterLastRow
  Protected Overrides Sub GUI_BeforeFirstRow()

    'Terminal actual (agrupación)
    m_current_terminal_id = -1
    'Totales generales
    m_stats = New Dictionary(Of Decimal, CLASS_MACHINE_DENOM_STATS)

    If m_refresh_grid Then
      Call GUI_StyleSheet()
    End If

    m_refresh_grid = False

  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()
    Call Me.uc_pr_list.SetDefaultValues()

  End Sub ' GUI_FilterReset

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()
    Call MyBase.GUI_InitControls()
    Dim _terminal_types() As Integer

    ' - Form title
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6597)
    ' - Check Terminals Location
    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)


    ' Labels
    Me.lbl_payout.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6643)
    Me.lbl_netwin.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6644)

    ' - Buttons
    GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Text = IIf(m_selection_mode = ENUM_TERMINALS_SELECTION_MODE.NONE, GLB_NLS_GUI_CONTROLS.GetString(25), GLB_NLS_GUI_CONTROLS.GetString(9))
    GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)

    GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Visible = IIf(m_selection_mode = ENUM_TERMINALS_SELECTION_MODE.NONE, True, False)
    GUI_Button(ENUM_BUTTON.BUTTON_INFO).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Visible = IIf(m_selection_mode = ENUM_TERMINALS_SELECTION_MODE.NONE, True, False)
    GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Enabled = False


    GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False

    _terminal_types = {WSI.Common.TerminalTypes.SAS_HOST}


    Call Me.uc_pr_list.Init(_terminal_types)

    ' Date time filter
    Me.uc_dsl.Init(GLB_NLS_GUI_INVOICING.Id(201))
    Me.uc_dsl.ClosingTimeEnabled = False

    If Not m_sel_params Is Nothing Then
      Call GUI_FilterReset()
    Else
      ' Set filter default values
      Call SetDefaultValues()
    End If

    Call GUI_StyleSheet()

    Call TerminalReport.ForceRefresh()

  End Sub ' GUI_InitControls

  ' PURPOSE: Build an SQL query from conditions set in the filters of this view
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _new_str_sql As System.Text.StringBuilder

    GUI_ReportUpdateFilters()

    _new_str_sql = New System.Text.StringBuilder()

    If (m_common_sql_select_part = Nothing) Then
      m_common_sql_select_part = GetCommonSqlSelectPart()
    End If

    _new_str_sql.AppendLine(m_common_sql_select_part)

    _new_str_sql.AppendLine(" WHERE TE_TERMINAL_ID IN " & Me.uc_pr_list.GetProviderIdListSelected())

    If Me.uc_dsl.FromDateSelected Then
      _new_str_sql.AppendLine(" AND (MDSH_BASE_HOUR >= " & GUI_FormatDateDB(Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime)) & ") ")
    End If

    If Me.uc_dsl.ToDateSelected Then
      _new_str_sql.AppendLine(" AND (MDSH_BASE_HOUR < " & GUI_FormatDateDB(Me.uc_dsl.ToDate.Date.AddHours(Me.uc_dsl.ClosingTime)) & ") ")
    End If

    _new_str_sql.AppendLine(" ORDER BY TE_TERMINAL_ID ")

    Return _new_str_sql.ToString()

  End Function ' GUI_FilterGetSqlQuery


  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean


    Dim _terminal_data As List(Of Object)

    Dim _terminal_id As Integer = DbRow.Value(SQL_COLUMN_TERMINAL_ID)
    Dim _terminal_name As String = DbRow.Value(SQL_COLUMN_TERMINAL_NAME)
    Dim _played_amount As Decimal = DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT)
    Dim _won_total_amount As Decimal = DbRow.Value(SQL_COLUMN_WON_AMOUNT)
    Dim _denomination As Decimal = DbRow.Value(SQL_COLUMN_DENOMINATION)
    Dim _won_count As Long = DbRow.Value(SQL_COLUMN_WON_COUNT)
    Dim _played_count As Long = DbRow.Value(SQL_COLUMN_PLAYED_COUNT)
    Dim _jackpot_amount As Decimal = 0
    Dim _default_theoretical_payout As Decimal = GeneralParam.GetDecimal("PlayerTracking", "TerminalDefaultPayout") / 100
    Dim _payout_percentage As Decimal = 0
    Dim _theoretical_payout_percentage As Decimal = 0
    Dim _coin_out_amount As Decimal = 0
    Dim _net_win_amount As Decimal
    Dim _net_win_percentage As Decimal = 0
    Dim _won_percentage As Decimal = 0
    Dim _bet_average_amount As Decimal = 0
    Dim _base_hour As DateTime


    If DateTime.TryParse(DbRow.Value(SQL_COLUMN_BASEHOUR), _base_hour) Then
      _base_hour = _base_hour.AddMinutes(-_base_hour.Minute)
    End If


    'Calculated fields
    If DbRow.IsNull(SQL_COLUMN_JACKPOT_AMOUNT) Then
      _jackpot_amount = 0
    Else
      _jackpot_amount = DbRow.Value(SQL_COLUMN_JACKPOT_AMOUNT)
    End If

    If (_played_amount <> 0) Then
      _payout_percentage = (_won_total_amount * 100) / _played_amount
    End If

    If (_played_amount <> 0) Then
      If DbRow.IsNull(SQL_COLUMN_TERMINAL_THEORETICAL_PAYOUT) Then
        _theoretical_payout_percentage = (_played_amount * _default_theoretical_payout) / _played_amount * 100
      Else
        _theoretical_payout_percentage = (_played_amount * DbRow.Value(SQL_COLUMN_TERMINAL_THEORETICAL_PAYOUT)) / _played_amount * 100
      End If
    End If



    _coin_out_amount = _won_total_amount - _jackpot_amount

    _net_win_amount = _played_amount - _won_total_amount

    If (_played_amount <> 0) Then
      _net_win_percentage = (_net_win_amount * 100) / _played_amount
    End If

    If (_played_count <> 0) Then
      _won_percentage = _won_count * 100 / _played_count
      _bet_average_amount = _played_amount / _played_count
    End If

    'Acumulador por Terminal
    If (m_current_terminal_id = -1) Then
      m_current_terminal_id = _terminal_id
      m_current_terminal_name = _terminal_name
    End If
    If (Not m_stats.ContainsKey(STATS_GROUP_KEY)) Then
      m_stats.Add(STATS_GROUP_KEY, New CLASS_MACHINE_DENOM_STATS())
    End If

    With m_stats(STATS_GROUP_KEY)
      .PlayedAmount += _played_amount
      .PlayedCount += _played_count
      .WonAmount += _won_total_amount
      .WonCount += _won_count
      .CoinOutAmount += _coin_out_amount
      .JackpotAmount += _jackpot_amount
      .NetWinAmount += _net_win_amount
      .TheoreticalPayOutAmount += (_played_amount * _theoretical_payout_percentage / 100)
    End With


    'Acumuladores generales
    If (Not m_stats.ContainsKey(_denomination)) Then
      m_stats.Add(_denomination, New CLASS_MACHINE_DENOM_STATS())
    End If

    With m_stats(_denomination)
      .PlayedAmount += _played_amount
      .PlayedCount += _played_count
      .WonAmount += _won_total_amount
      .WonCount += _won_count
      .CoinOutAmount += _coin_out_amount
      .JackpotAmount += _jackpot_amount
      .NetWinAmount += _net_win_amount
      .TheoreticalPayOutAmount += (_played_amount * _theoretical_payout_percentage / 100)
    End With

    'Totales 
    If (Not m_stats.ContainsKey(STATS_TOTAL_KEY)) Then
      m_stats.Add(STATS_TOTAL_KEY, New CLASS_MACHINE_DENOM_STATS())
    End If

    With m_stats(STATS_TOTAL_KEY)
      .PlayedAmount += _played_amount
      .PlayedCount += _played_count
      .WonAmount += _won_total_amount
      .WonCount += _won_count
      .CoinOutAmount += _coin_out_amount
      .JackpotAmount += _jackpot_amount
      .NetWinAmount += _net_win_amount
      .TheoreticalPayOutAmount += (_played_amount * _theoretical_payout_percentage / 100)
    End With



    'Write to grid --------------------------

    ' Date from
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_FROM).Value = GUI_FormatDate(_base_hour, _
                                                                         ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                         ENUM_FORMAT_TIME.FORMAT_HHMM)

    ' Date to
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_TO).Value = GUI_FormatDate(_base_hour.AddHours(1), _
                                                                       ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                       ENUM_FORMAT_TIME.FORMAT_HHMM)

    ' Game Name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_NAME).Value = DbRow.Value(SQL_COLUMN_GAME_NAME)

    ' Provider
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROVIDER_ID).Value = DbRow.Value(SQL_COLUMN_PROVIDER_ID)

    ' Nombre
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_NAME).Value = DbRow.Value(SQL_COLUMN_TERMINAL_NAME)

    ' Terminal Report
    If Not DbRow.IsNull(SQL_COLUMN_TERMINAL_ID) Then
      _terminal_data = TerminalReport.GetReportDataList(DbRow.Value(SQL_COLUMN_TERMINAL_ID), m_terminal_report_type)
    Else
      _terminal_data = New List(Of Object)()
    End If

    For _idx As Int32 = 0 To _terminal_data.Count - 1
      If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value Then
        Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
      End If
    Next

    ' Denomination
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DENOMINATION).Value = GUI_FormatCurrency(_denomination, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Played Amount
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED_AMOUNT).Value = GUI_FormatCurrency(_played_amount)

    ' CoinOut
    Me.Grid.Cell(RowIndex, GRID_COLUMN_WON_AMOUNT).Value = GUI_FormatCurrency(_coin_out_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Jackpot Amount
    Me.Grid.Cell(RowIndex, GRID_COLUMN_JACKPOT_AMOUNT).Value = GUI_FormatCurrency(_jackpot_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Won Total
    Me.Grid.Cell(RowIndex, GRID_COLUMN_WON_TOTAL).Value = GUI_FormatCurrency(_won_total_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Payout pct
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PAYOUT_PCT).Value = GUI_FormatNumber(_payout_percentage, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"

    'Payout Theoretical pct
    Me.Grid.Cell(RowIndex, GRID_COLUMN_THEORICAL_PAYOUT_PCT).Value = GUI_FormatNumber(_theoretical_payout_percentage, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)

    'NetWin
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(_net_win_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'NetWin pct
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN_PCT).Value = GUI_FormatNumber(_net_win_percentage, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"

    ' Played count
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED_COUNT).Value = GUI_FormatNumber(_played_count, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)

    ' Won count
    Me.Grid.Cell(RowIndex, GRID_COLUMN_WON_COUNT).Value = GUI_FormatNumber(_won_count, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)

    'Won pct
    Me.Grid.Cell(RowIndex, GRID_COLUMN_WON_PCT).Value = GUI_FormatNumber(_won_percentage, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"

    'Bet average
    Me.Grid.Cell(RowIndex, GRID_COLUMN_BET_AVERAGE).Value = GUI_FormatCurrency(_bet_average_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)


    Return True

  End Function ' GUI_SetupRow 

  ' PURPOSE : Sets parameters for the report
  '
  '  PARAMS :
  '     - INPUT :
  '           - PrintData As GUI_Reports.CLASS_PRINT_DATA
  '           - Optional FirstColIndex As Integer
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)


  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                          Optional ByVal FirstColIndex As Integer = 0)


    Call MyBase.GUI_ReportParams(PrintData)


    PrintData.Params.Title = GLB_NLS_GUI_STATISTICS.GetString(212)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams





  ' PURPOSE: Update the member variables used for the printed report
  '  PARAMS:
  '     - INPUT:
  '               
  '     - OUTPUT:
  '          
  ' RETURNS:
  '     - none
  Protected Overrides Sub GUI_ReportUpdateFilters()
    ' Gather all values from the view
    Dim _selected_providers_array As String()
    Dim _selected_terminal_ids_array As Long()

    _selected_terminal_ids_array = Me.uc_pr_list.GetTerminalIdListSelected()
    _selected_providers_array = Me.uc_pr_list.GetOnlyProviderIdListSelected()


    ' Providers - Terminals
    m_report_terminals = Me.uc_pr_list.GetTerminalReportText()

    'Dates
    m_date_from = ""
    m_date_to = ""
    If Me.uc_dsl.FromDateSelected Then
      m_date_from = GUI_FormatDate(Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime), _
                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    If Me.uc_dsl.ToDateSelected Then
      m_date_to = GUI_FormatDate(Me.uc_dsl.ToDate.Date.AddHours(Me.uc_dsl.ClosingTime), _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' Overrides

#Region " Options Events "

  Private Sub chk_terminal_location_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminal_location.CheckedChanged
    If chk_terminal_location.Checked Then
      m_terminal_report_type = ReportType.Location
    Else
      m_terminal_report_type = Nothing
    End If

    m_refresh_grid = True
  End Sub

#End Region

#Region " Public functions "
  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit


#End Region ' Public functions

#Region " Private Functions "

  ' PURPOSE: Prints subtotal row on datagrid using values from current group
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Private Sub PrintGridSubtotal()
    Dim idx_row As Integer

    'SubTotals Row

    Me.Grid.AddRow()
    idx_row = Me.Grid.NumRows - 1

    Me.Grid.Cell(idx_row, GRID_COLUMN_DATE_TO).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1280)
    ' Terminal Name
    Me.Grid.Cell(idx_row, GRID_COLUMN_TERMINAL_NAME).Value = m_current_terminal_name
    ' Played Amount
    Me.Grid.Cell(idx_row, GRID_COLUMN_PLAYED_AMOUNT).Value = GUI_FormatCurrency(m_stats(STATS_GROUP_KEY).PlayedAmount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    ' Won Amount
    Me.Grid.Cell(idx_row, GRID_COLUMN_WON_AMOUNT).Value = GUI_FormatCurrency(m_stats(STATS_GROUP_KEY).CoinOutAmount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    ' Jackpot Amount
    Me.Grid.Cell(idx_row, GRID_COLUMN_JACKPOT_AMOUNT).Value = GUI_FormatCurrency(m_stats(STATS_GROUP_KEY).JackpotAmount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    ' Played count
    Me.Grid.Cell(idx_row, GRID_COLUMN_PLAYED_COUNT).Value = GUI_FormatNumber(m_stats(STATS_GROUP_KEY).PlayedCount, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
    ' Won count
    Me.Grid.Cell(idx_row, GRID_COLUMN_WON_COUNT).Value = GUI_FormatNumber(m_stats(STATS_GROUP_KEY).WonCount, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
    'Won GrandTotal
    Me.Grid.Cell(idx_row, GRID_COLUMN_WON_TOTAL).Value = GUI_FormatCurrency(m_stats(STATS_GROUP_KEY).WonAmount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    'NetWin
    Me.Grid.Cell(idx_row, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(m_stats(STATS_GROUP_KEY).NetWinAmount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    'BetAverage
    Me.Grid.Cell(idx_row, GRID_COLUMN_BET_AVERAGE).Value = GUI_FormatCurrency(m_stats(STATS_GROUP_KEY).BetAverageAmount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    'Payout pct
    Me.Grid.Cell(idx_row, GRID_COLUMN_PAYOUT_PCT).Value = GUI_FormatNumber(m_stats(STATS_GROUP_KEY).PayOutPercentage, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"
    'Won pct
    Me.Grid.Cell(idx_row, GRID_COLUMN_WON_PCT).Value = GUI_FormatNumber(m_stats(STATS_GROUP_KEY).WonPercentage, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"
    'NetWin pct
    Me.Grid.Cell(idx_row, GRID_COLUMN_NETWIN_PCT).Value = GUI_FormatNumber(m_stats(STATS_GROUP_KEY).NetWinPercentage, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"
    'Payout Theoretical pct
    Me.Grid.Cell(idx_row, GRID_COLUMN_THEORICAL_PAYOUT_PCT).Value = GUI_FormatNumber(m_stats(STATS_GROUP_KEY).TheoreticalPayOutPercentage, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)

    ' Color Row
    Me.Grid.Row(idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

  End Sub ' GUI_AfterLastRow

  ' PURPOSE: Load rows of selected datagrid rows from DB and repaint the cells without refreshing the un-selected ones
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Private Sub RefreshGridWithNoReload()
    Dim _selected_terminal_ids_list As String
    Dim selectedRows() As Integer

    ' Get a list of the currently seleceted rows
    selectedRows = Me.Grid.SelectedRows()

    ' If there are no selected rows, then there is nothing to do here
    If selectedRows.Length = 0 Then
      Return
    End If

    ' Init ids list string
    _selected_terminal_ids_list = ""


    ' Take away trailling coma from terminal id list
    _selected_terminal_ids_list = _selected_terminal_ids_list.Substring(0, _selected_terminal_ids_list.Length - 1)




  End Sub ' RefreshGridWithNoReload


  ' PURPOSE: Sets the default values for filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - 
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Boolean
  Protected Sub SetDefaultValues()
    Dim _closing_time As Integer
    Dim _final_time As Date
    Dim _default_filter As Boolean
    Dim _initial_time As Date
    Dim _now As Date

    _now = WSI.Common.WGDB.Now
    _initial_time = WSI.Common.WGDB.Now

    chk_terminal_location.Checked = False

    Call Me.uc_pr_list.SetDefaultValues()

    _default_filter = True

    If Not m_sel_params Is Nothing Then
      _default_filter = m_sel_params.FilterTypeDefault
    End If


    If _initial_time > _now Then
      _initial_time = _initial_time.AddDays(-1)
    End If


    _closing_time = GetDefaultClosingTime()
    _final_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)
    If _final_time > _now Then
      _final_time = _final_time.AddDays(-1)
    End If

    _final_time = _final_time.Date
    Me.uc_dsl.ToDate = _final_time
    Me.uc_dsl.ToDateSelected = True
    Me.uc_dsl.FromDate = _final_time.AddDays(-1)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.ClosingTime = _closing_time


  End Sub 'SetDefaultValues
  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    Dim _terminal_columns As List(Of ColumnSettings)

    Call GridColumnReIndex()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      Select Case m_selection_mode
        Case ENUM_TERMINALS_SELECTION_MODE.MULTI, ENUM_TERMINALS_SELECTION_MODE.NONE
          Me.Grid.SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE

        Case ENUM_TERMINALS_SELECTION_MODE.ONE
          Me.Grid.SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
      End Select

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Date from
      .Column(GRID_COLUMN_DATE_FROM).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(204)
      .Column(GRID_COLUMN_DATE_FROM).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(309)
      .Column(GRID_COLUMN_DATE_FROM).Width = GRID_COLUMN_WIDTH_DATERANGE
      .Column(GRID_COLUMN_DATE_FROM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Date to
      .Column(GRID_COLUMN_DATE_TO).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(204)
      .Column(GRID_COLUMN_DATE_TO).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(310)
      .Column(GRID_COLUMN_DATE_TO).Width = GRID_COLUMN_WIDTH_DATERANGE
      .Column(GRID_COLUMN_DATE_TO).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER



      'Terminal (Proveedor, Nombre, ID en planta, Denominación)

      ' Grid columns

      ' Provider Id
      .Column(GRID_COLUMN_PROVIDER_ID).Header(0).Text = GLB_NLS_GUI_AUDITOR.GetString(333) ' Terminal 
      .Column(GRID_COLUMN_PROVIDER_ID).Header(1).Text = GLB_NLS_GUI_AUDITOR.GetString(341) ' Proveedor
      .Column(GRID_COLUMN_PROVIDER_ID).Width = GRID_COLUMN_WIDTH_PROVIDER_ID
      .Column(GRID_COLUMN_PROVIDER_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Terminal name
      .Column(GRID_COLUMN_TERMINAL_NAME).Header(0).Text = GLB_NLS_GUI_AUDITOR.GetString(333) ' Terminal
      .Column(GRID_COLUMN_TERMINAL_NAME).Header(1).Text = GLB_NLS_GUI_AUDITOR.GetString(330) ' Nombre
      .Column(GRID_COLUMN_TERMINAL_NAME).Width = GRID_COLUMN_WIDTH_TERMINAL_NAME
      .Column(GRID_COLUMN_TERMINAL_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'Game Name
      .Column(GRID_COLUMN_GAME_NAME).Header(0).Text = GLB_NLS_GUI_AUDITOR.GetString(333) ' Terminal
      .Column(GRID_COLUMN_GAME_NAME).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(215)
      .Column(GRID_COLUMN_GAME_NAME).Width = GRID_COLUMN_WIDTH_GAME_NAME
      .Column(GRID_COLUMN_GAME_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Denomination
      .Column(GRID_COLUMN_DENOMINATION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(537)
      .Column(GRID_COLUMN_DENOMINATION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2270)
      .Column(GRID_COLUMN_DENOMINATION).Width = GRID_COLUMN_WIDTH_DENOMINATION
      .Column(GRID_COLUMN_DENOMINATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Terminal Report
      If m_terminal_report_type = ReportType.Location Then
        _terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)
        For _idx As Int32 = 0 To _terminal_columns.Count - 1
          .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(300)
          .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = _terminal_columns(_idx).Header1
          .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = _terminal_columns(_idx).Width
          .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = _terminal_columns(_idx).Alignment
        Next
      End If



      ' Played Amount
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(537)
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6275) 'Coin In
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Width = GRID_COLUMN_WIDTH_PLAYED_AMOUNT
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT


      'Ganado (Coin-Out, Jackpot, Ganado Total)

      ' Coin-Out
      .Column(GRID_COLUMN_WON_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(798) ' Won
      .Column(GRID_COLUMN_WON_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2662)
      .Column(GRID_COLUMN_WON_AMOUNT).Width = GRID_COLUMN_WIDTH_WON_AMOUNT
      .Column(GRID_COLUMN_WON_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Jackpot
      .Column(GRID_COLUMN_JACKPOT_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(798)
      .Column(GRID_COLUMN_JACKPOT_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2686)
      .Column(GRID_COLUMN_JACKPOT_AMOUNT).Width = GRID_COLUMN_WIDTH_JACKPOT_AMOUNT
      .Column(GRID_COLUMN_JACKPOT_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Ganado Total
      .Column(GRID_COLUMN_WON_TOTAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(798)
      .Column(GRID_COLUMN_WON_TOTAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4500)
      .Column(GRID_COLUMN_WON_TOTAL).Width = GRID_COLUMN_WIDTH_WON_TOTAL
      .Column(GRID_COLUMN_WON_TOTAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT



      'Payout (Payout %, Payout Teórico %)

      ' Payout %
      .Column(GRID_COLUMN_PAYOUT_PCT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1924)
      .Column(GRID_COLUMN_PAYOUT_PCT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6645)
      .Column(GRID_COLUMN_PAYOUT_PCT).Width = GRID_COLUMN_WIDTH_PAYOUT_PCT
      .Column(GRID_COLUMN_PAYOUT_PCT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      ' Payout Teórico %
      .Column(GRID_COLUMN_THEORICAL_PAYOUT_PCT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1924)
      .Column(GRID_COLUMN_THEORICAL_PAYOUT_PCT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1809)
      .Column(GRID_COLUMN_THEORICAL_PAYOUT_PCT).Width = GRID_COLUMN_THEORICAL_PAYOUT_WIDTH
      .Column(GRID_COLUMN_THEORICAL_PAYOUT_PCT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT


      'Netwin (Netwin, Netwin %)
      .Column(GRID_COLUMN_NETWIN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(539)
      .Column(GRID_COLUMN_NETWIN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(539)
      .Column(GRID_COLUMN_NETWIN).Width = GRID_COLUMN_WIDTH_NETWIN
      .Column(GRID_COLUMN_NETWIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_COLUMN_NETWIN_PCT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(539)
      .Column(GRID_COLUMN_NETWIN_PCT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6646)
      .Column(GRID_COLUMN_NETWIN_PCT).Width = GRID_COLUMN_WIDTH_NETWIN_PCT
      .Column(GRID_COLUMN_NETWIN_PCT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT


      'Jugadas (Jugadas, Ganadas, G. %, Apuesta Media)

      ' Played Count
      .Column(GRID_COLUMN_PLAYED_COUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(308)
      .Column(GRID_COLUMN_PLAYED_COUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(304)
      .Column(GRID_COLUMN_PLAYED_COUNT).Width = GRID_COLUMN_WIDTH_PLAYED_COUNT
      .Column(GRID_COLUMN_PLAYED_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Won Count
      .Column(GRID_COLUMN_WON_COUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(308)
      .Column(GRID_COLUMN_WON_COUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(305)
      .Column(GRID_COLUMN_WON_COUNT).Width = GRID_COLUMN_WIDTH_WON_COUNT
      .Column(GRID_COLUMN_WON_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' G. %
      .Column(GRID_COLUMN_WON_PCT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(308)
      .Column(GRID_COLUMN_WON_PCT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(316)
      .Column(GRID_COLUMN_WON_PCT).Width = GRID_COLUMN_WIDTH_WON_PCT
      .Column(GRID_COLUMN_WON_PCT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Apuesta Media
      .Column(GRID_COLUMN_BET_AVERAGE).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(308)
      .Column(GRID_COLUMN_BET_AVERAGE).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(347)
      .Column(GRID_COLUMN_BET_AVERAGE).Width = GRID_COLUMN_WIDTH_BET_AVERAGE
      .Column(GRID_COLUMN_BET_AVERAGE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

  End Sub ' GUI_StyleSheet

  Private Function GridColumnReIndex() As Integer

    TERMINAL_DATA_COLUMNS = If(m_terminal_report_type = ReportType.Location, TerminalReport.NumColumns(m_terminal_report_type), 0)


    GRID_COLUMN_INDEX = 0
    GRID_COLUMN_DATE_FROM = 1
    GRID_COLUMN_DATE_TO = 2

    GRID_COLUMN_PROVIDER_ID = 3
    GRID_COLUMN_TERMINAL_NAME = 4
    GRID_COLUMN_GAME_NAME = 5

    GRID_INIT_TERMINAL_DATA = 6

    GRID_COLUMN_DENOMINATION = TERMINAL_DATA_COLUMNS + 6
    GRID_COLUMN_PLAYED_AMOUNT = TERMINAL_DATA_COLUMNS + 7
    GRID_COLUMN_WON_AMOUNT = TERMINAL_DATA_COLUMNS + 8
    GRID_COLUMN_JACKPOT_AMOUNT = TERMINAL_DATA_COLUMNS + 9
    GRID_COLUMN_WON_TOTAL = TERMINAL_DATA_COLUMNS + 10
    GRID_COLUMN_PAYOUT_PCT = TERMINAL_DATA_COLUMNS + 11
    GRID_COLUMN_THEORICAL_PAYOUT_PCT = TERMINAL_DATA_COLUMNS + 12
    GRID_COLUMN_NETWIN = TERMINAL_DATA_COLUMNS + 13
    GRID_COLUMN_NETWIN_PCT = TERMINAL_DATA_COLUMNS + 14
    GRID_COLUMN_PLAYED_COUNT = TERMINAL_DATA_COLUMNS + 15
    GRID_COLUMN_WON_COUNT = TERMINAL_DATA_COLUMNS + 16
    GRID_COLUMN_WON_PCT = TERMINAL_DATA_COLUMNS + 17
    GRID_COLUMN_BET_AVERAGE = TERMINAL_DATA_COLUMNS + 18

    GRID_COLUMNS = TERMINAL_DATA_COLUMNS + 19

  End Function


  ' PURPOSE: Set filter related to terminal status
  '
  '  PARAMS:
  '     - INPUT :
  '           - SqlFilter
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS:
  '     - String
  Private Function SetTerminalStatusFilter() As String
    Dim _result_string As String
    _result_string = ""

    _result_string = IIf(_result_string.Length = 0, WSI.Common.TerminalStatus.ACTIVE & "," & _
                                                    WSI.Common.TerminalStatus.OUT_OF_SERVICE & "," & _
                                                    WSI.Common.TerminalStatus.RETIRED _
                                                  , _result_string)

    Return _result_string

  End Function ' SetTerminalStatusFilter
  ' PURPOSE: Set filter related to terminal status
  '
  '  PARAMS:
  '     - INPUT :
  '           - SqlFilter
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS:
  '     - String
  Private Function GetCommonSqlSelectPart() As String

    Dim _sql_select_str As System.Text.StringBuilder
    Dim _str_several As String
    _str_several = GLB_NLS_GUI_PLAYER_TRACKING.GetString(634) 'Multijuego


    _sql_select_str = New System.Text.StringBuilder()

    _sql_select_str.AppendLine("  SELECT  TE.TE_TERMINAL_ID ")
    _sql_select_str.AppendLine("         ,TE.TE_PROVIDER_ID ")
    _sql_select_str.AppendLine("         ,TE.TE_NAME")
    _sql_select_str.AppendLine("         ,MDSH_BASE_HOUR ")
    _sql_select_str.AppendLine("         ,MDSH_DENOMINATION ")
    _sql_select_str.AppendLine("         ,MDSH_PLAYED_AMOUNT ")
    _sql_select_str.AppendLine("         ,MDSH_WON_AMOUNT ")
    _sql_select_str.AppendLine("         ,MDSH_JACKPOT_AMOUNT ")
    _sql_select_str.AppendLine("         ,MDSH_PLAYED_COUNT ")
    _sql_select_str.AppendLine("         ,MDSH_WON_COUNT ")
    _sql_select_str.AppendLine("         ,TE.TE_THEORETICAL_PAYOUT")
    _sql_select_str.AppendLine("         ,ISNULL( ")
    _sql_select_str.AppendLine("          	    (SELECT   CASE COUNT(PG_GAME_NAME) WHEN 1 THEN MAX(PG_GAME_NAME) ELSE '" + _str_several + "' END ")
    _sql_select_str.AppendLine("                   FROM   TERMINAL_GAME_TRANSLATION, PROVIDERS_GAMES WHERE TGT_TRANSLATED_GAME_ID = PG_GAME_ID AND TGT_TERMINAL_ID = TE.TE_TERMINAL_ID  ")
    _sql_select_str.AppendLine("                  GROUP   BY TGT_TERMINAL_ID) ")
    _sql_select_str.AppendLine("                 ,'UNKNOWN')                                                                            AS GAME ")
    _sql_select_str.AppendLine("  FROM   DBO.MACHINE_DENOM_STATS_PER_HOUR MDSH")
    _sql_select_str.AppendLine("  INNER  JOIN TERMINALS AS TE ON MDSH.MDSH_TERMINAL_ID = TE.TE_TERMINAL_ID")


    Return _sql_select_str.ToString()

  End Function ' GetCommonSqlSelectPart

#End Region ' Private functions

#Region " GUI Reports "
  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)

    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(339), m_terminal_name)

    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_report_terminals)
    PrintData.SetFilter(GLB_NLS_GUI_SW_DOWNLOAD.GetString(322), m_report_terminal_type)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(262), m_report_terminal_status)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6056), m_serial_number)

    PrintData.FilterValueWidth(1) = 3000
    PrintData.FilterValueWidth(2) = 4000
    PrintData.FilterValueWidth(3) = 3000

  End Sub ' GUI_ReportFilter
#End Region ' GUI reports


  Public Sub New()

    ' This call is required by the designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.

  End Sub
End Class ' frm_terminals


Public Class CLASS_MACHINE_DENOM_STATS

  Public Property PlayedAmount As Decimal
  Public Property CoinOutAmount As Decimal
  Public Property JackpotAmount As Decimal
  Public Property PlayedCount As Long
  Public Property WonCount As Long
  Public Property WonAmount As Decimal
  Public Property NetWinAmount As Decimal
  Public Property TheoreticalPayOutAmount As Decimal


  Public ReadOnly Property BetAverageAmount() As Decimal
    Get
      If PlayedCount <> 0 Then
        Return PlayedAmount / PlayedCount
      Else
        Return 0
      End If
    End Get
  End Property

  Public ReadOnly Property PayOutPercentage() As Decimal
    Get
      If PlayedAmount <> 0 Then
        Return WonAmount * 100 / PlayedAmount
      Else
        Return 0
      End If
    End Get
  End Property

  Public ReadOnly Property WonPercentage() As Decimal
    Get
      If PlayedCount <> 0 Then
        Return WonCount * 100 / PlayedCount
      Else
        Return 0
      End If
    End Get
  End Property

  Public ReadOnly Property TheoreticalPayOutPercentage() As Decimal
    Get
      If PlayedAmount <> 0 Then
        Return TheoreticalPayOutAmount * 100 / PlayedAmount
      Else
        Return 0
      End If
    End Get
  End Property

  Public ReadOnly Property NetWinPercentage() As Decimal
    Get
      If PlayedAmount <> 0 Then
        Return NetWinAmount * 100 / PlayedAmount
      Else
        Return 0
      End If
    End Get
  End Property


End Class