<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_recommendation_report
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.uc_dsl)
    Me.panel_filter.Size = New System.Drawing.Size(1183, 80)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dsl, 0)
    '
    'panel_data
    '
    Me.panel_data.Size = New System.Drawing.Size(1183, 646)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1177, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1177, 4)
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.ClosingTimeEnabled = False
    Me.uc_dsl.FromDate = New Date(2003, 5, 20, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.Location = New System.Drawing.Point(6, 0)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.Size = New System.Drawing.Size(260, 80)
    Me.uc_dsl.TabIndex = 11
    Me.uc_dsl.ToDate = New Date(2003, 5, 20, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    '
    'frm_recommendation_report
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1191, 734)
    Me.Name = "frm_recommendation_report"
    Me.Text = "frm_recommendation_report"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector
End Class
