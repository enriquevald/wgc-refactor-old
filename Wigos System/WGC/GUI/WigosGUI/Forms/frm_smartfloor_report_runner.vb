﻿'-------------------------------------------------------------------
' Copyright © 2007-2015 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_smartfloor_report_runner
'
' DESCRIPTION:   This screen allows to view, for smartfloor runners 
'                           - count of taskts
'                           - total time to initiate a task and avg
'                           - total time to resolve a task and avg
' AUTHOR:        Jorge Concheyro
' CREATION DATE: 21-OCT-2015
'
' REVISION HISTORY:
'
' Date         Author Description
' 21-OCT-2015  JRC    Initial version
' 05-NOV-2015  SMN    Bug 6246: Reset button
' 13-JUN-2018  AGS    Bug 33012: WIGOS-12740 The export to Excel of Tasks runner report is not completed
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Threading
Imports System.Data
Imports System.Text
Imports WSI.Common

Public Class frm_smartfloor_report_runner
  Inherits GUI_Controls.frm_base_sel

#Region " Constants "

  ' DB Columns
  Private Const SQL_COLUMN_USER_ID As Integer = 0
  Private Const SQL_COLUMN_USER_NAME As Integer = 1
  Private Const SQL_COLUMN_GROUP_ALARM As Integer = 2
  Private Const SQL_COLUMN_TASK_COUNT As Integer = 3
  Private Const SQL_COLUMN_INIT_TASK_COUNT As Integer = 4
  Private Const SQL_COLUMN_TOTAL_TIME_TO_INIT_TASK As Integer = 5
  Private Const SQL_COLUMN_AVG_TIME_TO_INIT_TASK As Integer = 6
  Private Const SQL_COLUMN_SOLVING_TASK_COUNT As Integer = 7
  Private Const SQL_COLUMN_TOTAL_TIME_SOLVING_TASK As Integer = 8
  Private Const SQL_COLUMN_AVG_TIME_SOLVING_TASK As Integer = 9


  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_USER_NAME As Integer = 1
  Private Const GRID_COLUMN_GROUP_ALARM As Integer = 2
  Private Const GRID_COLUMN_TASK_COUNT As Integer = 3
  Private Const GRID_COLUMN_INIT_TASK_COUNT As Integer = 4
  Private Const GRID_COLUMN_TOTAL_TIME_TO_INIT_TASK As Integer = 5
  Private Const GRID_COLUMN_AVG_TIME_TO_INIT_TASK As Integer = 6
  Private Const GRID_COLUMN_SOLVING_TASK_COUNT As Integer = 7
  Private Const GRID_COLUMN_TOTAL_TIME_SOLVING_TASK As Integer = 8
  Private Const GRID_COLUMN_AVG_TIME_SOLVING_TASK As Integer = 9

  Private GRID_COLUMNS As Integer = 10
  Private Const GRID_HEADER_ROWS As Integer = 2

  ' Width
  Private Const GRID_WIDTH_USER_NAME As Integer = 3000
  Private Const GRID_WIDTH_GROUP_ALARM As Integer = 3000
  Private Const GRID_WIDTH_TASK_COUNT As Integer = 1800

  Private Const GRID_WIDTH_INIT_TASK_COUNT As Integer = 1000
  Private Const GRID_WIDTH_TOTAL_TIME_TO_INIT_TASK As Integer = 1500
  Private Const GRID_WIDTH_AVG_TIME_TO_INIT_TASK As Integer = 1500

  Private Const GRID_WIDTH_SOLVING_TASK_COUNT As Integer = 1000
  Private Const GRID_WIDTH_TOTAL_TIME_SOLVING_TASK As Integer = 1500
  Private Const GRID_WIDTH_AVG_TIME_SOLVING_TASK As Integer = 1500

  ' Database codes
  Private Const FORM_DB_MIN_VERSION As Short = 156


  ' Colors for total and subtotal rows
  Private Const COLOR_ROW_SUBTOTAL As GUI_CommonMisc.ENUM_GUI_COLOR = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01
  Private Const COLOR_ROW_TOTAL As GUI_CommonMisc.ENUM_GUI_COLOR = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00


#End Region ' Constants

#Region " Enums "

#End Region ' Enums

#Region " Members "

  ' For report filters 
  Private m_selecteddatefield As Integer
  Private m_date_from As String
  Private m_date_to As String
  Private m_showdetails As Boolean

  ' For grid totalizators
  ' subtotals

  Dim m_user_subtotal_count As Integer
  Dim m_user_subtotal_count_to_init As Integer
  Dim m_user_subtotal_total_time_to_init As Integer
  Dim m_user_subtotal_average_time_to_init As Decimal
  Dim m_user_subtotal_count_to_solve As Integer
  Dim m_user_subtotal_total_time_to_solve As Integer
  Dim m_user_subtotal_average_time_to_solve As Decimal


  ' totalizers
  Dim m_total_count As Integer
  Dim m_total_count_time_to_init As Integer
  Dim m_total_total_time_to_init As Integer
  Dim m_total_average_time_to_init As Decimal
  Dim m_total_count_time_to_solve As Integer
  Dim m_total_total_time_to_solve As Integer
  Dim m_total_average_time_to_solve As Decimal
  Dim m_current_user_id As Integer
  Dim m_current_user As String
  Dim m_first_row As Boolean




#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_FORM.FORM_SMARTFLOOR_RUNNERS_REPORT
    Call MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    Call MyBase.GUI_InitControls()

    ' TITO Mode

    ' Form Name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6902) ' Reporte de Runners

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(3)

    ' Date
    Me.gb_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6891) ' Task
    Me.ds_selector.Init(GLB_NLS_GUI_INVOICING.Id(201), True)
    Me.chk_Show_Detail.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6914)
    Me.rb_create.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6903)   'Creación
    Me.rb_solve.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6904)    'Resolución

    m_first_row = True
    m_current_user_id = 0
    m_current_user = ""
    Me.IgnoreRowHeightFilterInExcelReport = True

    ' Grid
    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean
    ' Date selection 
    If ds_selector.FromDateSelected And ds_selector.ToDateSelected Then
      If ds_selector.FromDate > ds_selector.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call ds_selector.Focus()
        Return False
      End If
    End If
    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _sb As System.Text.StringBuilder
    _sb = New System.Text.StringBuilder()
    ' Get Select and from
    _sb.AppendLine("SELECT    GU_USER_ID                                                                                                     ")
    _sb.AppendLine("       , USER_NAME                                                                                                       ")
    _sb.AppendLine("       , N.ALARM_DESC                                                                                                    ")
    _sb.AppendLine("       , TASK_COUNT                                                                                                      ")
    _sb.AppendLine("			 , COUNT_ACCEPTED                                                                                                  ")
    _sb.AppendLine("       , TOTAL_TIME_TO_INIT_TASK                                                                                         ")
    _sb.AppendLine("       , (TOTAL_TIME_TO_INIT_TASK/ISNULL(NULLIF(COUNT_ACCEPTED,0),1)) AS AVG_TIME_TO_INIT_TASK                           ")
    _sb.AppendLine("			 , COUNT_SOLVED                                                                                                    ")
    _sb.AppendLine("       , TOTAL_TIME_SOLVING_TASK                                                                                         ")
    _sb.AppendLine("       , (TOTAL_TIME_SOLVING_TASK/ISNULL(NULLIF(COUNT_SOLVED,0),1)) AS AVG_TIME_SOLVING_TASK                             ")
    _sb.AppendLine("  FROM   (                                                                                                               ")
    _sb.AppendLine("            SELECT                                                                                                       ")
    _sb.AppendLine("                         GU_USER_ID                                                                                      ")
    _sb.AppendLine("                       , GU_USERNAME AS [USER_NAME]                                                                      ")
    _sb.AppendLine("                       , LSA_ALARM_ID                                                                                    ")
    _sb.AppendLine("                       , COUNT(1) AS TASK_COUNT                                                                          ")
    _sb.AppendLine("                       , SUM(DATEDIFF (SECOND, LST_ASSIGNED, LST_ACCEPTED)) AS TOTAL_TIME_TO_INIT_TASK                   ")
    _sb.AppendLine("					             , COUNT(LST_ACCEPTED) AS COUNT_ACCEPTED                                                           ")
    _sb.AppendLine("                       , SUM(DATEDIFF (SECOND, LST_ASSIGNED, LST_SOLVED)) AS TOTAL_TIME_SOLVING_TASK                     ")
    _sb.AppendLine("					             , COUNT(LST_SOLVED) AS COUNT_SOLVED                                                               ")
    _sb.AppendLine("                  FROM   LAYOUT_SITE_TASKS                                                                               ")
    _sb.AppendLine("            INNER JOIN   GUI_USERS ON GUI_USERS.GU_USER_ID = COALESCE(LST_ACCEPTED_USER_ID,LST_ASSIGNED_USER_ID)         ")
    _sb.AppendLine("            INNER JOIN   LAYOUT_SITE_ALARMS ON LAYOUT_SITE_ALARMS.LSA_TASK_ID = LST_ID                                   ")
    _sb.AppendLine(GetSqlWhere())
    _sb.AppendLine("             GROUP BY LST_ASSIGNED_USER_ID,GU_USERNAME,GU_USER_ID,LSA_ALARM_ID                                           ")
    _sb.AppendLine("       ) AS A                                                                                                            ")
    _sb.AppendLine("    INNER JOIN                                                                                                           ")
    _sb.AppendLine("    (                                                                                                                    ")
    _sb.AppendLine("				SELECT		                                                                                                       ")
    _sb.AppendLine("						          LR_NAME + ' - ' + LRL_LABEL AS [ALARM_DESC]                                                        ")
    _sb.AppendLine("						        , CAST((LR_FIELD * 1000) + LRL_VALUE1 AS INT) AS [SUBCATEGORY]                                       ")
    _sb.AppendLine("				       FROM   LAYOUT_RANGES AS R                                                                                 ")
    _sb.AppendLine("				   INNER JOIN LAYOUT_RANGES_LEGENDS AS L ON R.LR_ID = L.LRL_RANGE_ID                                             ")
    _sb.AppendLine("				   WHERE      LR_SECTION_ID =  41 OR LR_SECTION_ID = 42 OR LR_SECTION_ID = 43  /*41 JUGADOR / 42 MÁQUINA*/       ")
    _sb.AppendLine("    ) AS N ON N.SUBCATEGORY = A.LSA_ALARM_ID                                                                             ")
    _sb.AppendLine("                                                                                                                         ")
    _sb.AppendLine("ORDER BY USER_NAME,ALARM_DESC                                                                                            ")

    Return _sb.ToString()
  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()
    m_current_user = ""
    m_current_user_id = 0
    m_first_row = True
    InitSubTotals()
    InitTotals()
    Call GUI_StyleSheet()
  End Sub ' GUI_BeforeFirstRow

  ' PURPOSE: Print totalizator row in the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    Dim _idx_row As Integer
    Dim _time As TimeSpan
    PrintSubTotals(m_current_user, True)

    _idx_row = Me.Grid.AddRow()

    ' Label - TOTAL:
    Me.Grid.Cell(_idx_row, GRID_COLUMN_USER_NAME).Value = GLB_NLS_GUI_INVOICING.GetString(205)

    ' Total - Points
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TASK_COUNT).Value = GUI_FormatNumber(Me.m_total_count, 0)

    Me.Grid.Cell(_idx_row, GRID_COLUMN_INIT_TASK_COUNT).Value = GUI_FormatNumber(Me.m_total_count_time_to_init, 0)

    _time = New TimeSpan(0, 0, Me.m_total_total_time_to_init)
    If _time.TotalSeconds > 0 Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_TOTAL_TIME_TO_INIT_TASK).Value = TimeSpanToString(_time)
    End If

    _time = New TimeSpan(0, 0, Me.m_total_average_time_to_init)
    If _time.TotalSeconds > 0 Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_AVG_TIME_TO_INIT_TASK).Value = TimeSpanToString(_time)
    End If

    Me.Grid.Cell(_idx_row, GRID_COLUMN_SOLVING_TASK_COUNT).Value = GUI_FormatNumber(Me.m_total_count_time_to_solve, 0)

    _time = New TimeSpan(0, 0, Me.m_total_total_time_to_solve)
    If _time.TotalSeconds > 0 Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_TOTAL_TIME_SOLVING_TASK).Value = TimeSpanToString(_time)
    End If

    _time = New TimeSpan(0, 0, Me.m_total_average_time_to_solve)
    If _time.TotalSeconds > 0 Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_AVG_TIME_SOLVING_TASK).Value = TimeSpanToString(_time)
    End If

    ' Color Row
    Me.Grid.Row(_idx_row).BackColor = GetColor(COLOR_ROW_TOTAL)

    InitSubTotals()
    InitTotals()

  End Sub ' GUI_AfterLastRow

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _time As TimeSpan
    Dim _row_user_id As Integer
    Dim _row_user As String
    _row_user_id = DbRow.Value(SQL_COLUMN_USER_ID)
    _row_user = DbRow.Value(SQL_COLUMN_USER_NAME)

    If _row_user_id <> m_current_user_id Then
      If Not m_first_row Then
        PrintSubTotals(m_current_user, False)
        If chk_Show_Detail.Checked Then
          RowIndex = Me.Grid.AddRow()
        End If
      Else
        m_first_row = False
      End If
      m_current_user_id = _row_user_id
      m_current_user = _row_user
    End If

    Call MyBase.GUI_SetupRow(RowIndex, DbRow)

    ' User Name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_NAME).Value = DbRow.Value(SQL_COLUMN_USER_NAME)
    ' Group Alarm
    Me.Grid.Cell(RowIndex, GRID_COLUMN_GROUP_ALARM).Value = DbRow.Value(SQL_COLUMN_GROUP_ALARM)
    ' Task count
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TASK_COUNT).Value = DbRow.Value(SQL_COLUMN_TASK_COUNT)
    ' Init Task count
    Me.Grid.Cell(RowIndex, GRID_COLUMN_INIT_TASK_COUNT).Value = DbRow.Value(SQL_COLUMN_INIT_TASK_COUNT)
    ' Init Task time
    If Not DbRow.Value(SQL_COLUMN_TOTAL_TIME_TO_INIT_TASK) Is DBNull.Value Then
      _time = New TimeSpan(0, 0, DbRow.Value(SQL_COLUMN_TOTAL_TIME_TO_INIT_TASK))
      If _time.TotalSeconds > 0 Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_TIME_TO_INIT_TASK).Value = TimeSpanToString(_time)
      End If
    End If
    ' Init Task avg time
    If Not DbRow.Value(SQL_COLUMN_AVG_TIME_TO_INIT_TASK) Is DBNull.Value Then
      _time = New TimeSpan(0, 0, DbRow.Value(SQL_COLUMN_AVG_TIME_TO_INIT_TASK))
      If _time.TotalSeconds > 0 Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_AVG_TIME_TO_INIT_TASK).Value = TimeSpanToString(_time)
      End If
    End If
    ' Solved task count
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SOLVING_TASK_COUNT).Value = DbRow.Value(SQL_COLUMN_SOLVING_TASK_COUNT)
    ' Solved task time
    If Not DbRow.Value(SQL_COLUMN_TOTAL_TIME_SOLVING_TASK) Is DBNull.Value Then
      _time = New TimeSpan(0, 0, DbRow.Value(SQL_COLUMN_TOTAL_TIME_SOLVING_TASK))
      If _time.TotalSeconds > 0 Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_TIME_SOLVING_TASK).Value = TimeSpanToString(_time)
      End If
    End If
    ' Solved task avg time
    If Not DbRow.Value(SQL_COLUMN_AVG_TIME_SOLVING_TASK) Is DBNull.Value Then
      _time = New TimeSpan(0, 0, DbRow.Value(SQL_COLUMN_AVG_TIME_SOLVING_TASK))
      If _time.TotalSeconds > 0 Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_AVG_TIME_SOLVING_TASK).Value = TimeSpanToString(_time)
      End If
    End If

    UpdateTotalizers(DbRow)

    Return chk_Show_Detail.Checked

  End Function ' GUI_SetupRow


  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.gb_date
  End Sub ' GUI_SetInitialFocus

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    If rb_create.Checked Then
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6903) & "  " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6903) & "  " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)
    Else
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6904) & "  " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6904) & "  " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)
    End If

    If m_showdetails Then
      PrintData.SetFilter(GLB_NLS_REPORT.GetString(0), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6914))
    End If

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)
    Call MyBase.GUI_ReportParams(PrintData)
  End Sub ' GUI_ReportParams

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()
    m_date_from = ""
    m_date_to = ""
    m_showdetails = chk_Show_Detail.Checked

    'Date 
    m_date_from = GUI_FormatDate(Me.ds_selector.FromDate.AddHours(Me.ds_selector.ClosingTime), _
                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ENUM_FORMAT_TIME.FORMAT_HHMM)

    If Me.ds_selector.ToDateSelected Then
      m_date_to = GUI_FormatDate(Me.ds_selector.ToDate.AddHours(Me.ds_selector.ClosingTime), _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region ' Overrides

#Region "Public Functions"

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region

#Region " Private Functions "

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()
    Dim _initial_time As Date
    _initial_time = WSI.Common.Misc.TodayOpening()
    Me.ds_selector.ToDateSelected = True
    Me.ds_selector.ToDate = _initial_time
    Me.ds_selector.FromDate = Me.ds_selector.ToDate.AddDays(-1)
    Me.ds_selector.ClosingTime = _initial_time.Hour
    Me.rb_create.Checked = True
    Me.chk_Show_Detail.Checked = False
    m_showdetails = False

  End Sub ' Sub SetDefaultValues

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    Dim _total_columns As Integer

    _total_columns = GRID_COLUMNS

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .IsSortable = False
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Header(1).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' UserId  - 0                                           
      .Column(GRID_COLUMN_USER_NAME).Header(0).Text = " "
      .Column(GRID_COLUMN_USER_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6905)  '"Runner"
      If m_showdetails Then
        .Column(GRID_COLUMN_USER_NAME).Width = GRID_WIDTH_USER_NAME
      Else
        .Column(GRID_COLUMN_USER_NAME).Width = GRID_WIDTH_USER_NAME + GRID_WIDTH_GROUP_ALARM
      End If

      ' Group Alarm - 1
      .Column(GRID_COLUMN_GROUP_ALARM).Header(0).Text = " "
      .Column(GRID_COLUMN_GROUP_ALARM).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6906) '"Tipo"
      .Column(GRID_COLUMN_GROUP_ALARM).Width = IIf(m_showdetails, GRID_WIDTH_GROUP_ALARM, 0)
      .Column(GRID_COLUMN_GROUP_ALARM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT_CENTER

      ' Cant rec - 2
      .Column(GRID_COLUMN_TASK_COUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6907) '"Tareas Asignadas"
      .Column(GRID_COLUMN_TASK_COUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6908) '"Cantidad"
      .Column(GRID_COLUMN_TASK_COUNT).Width = GRID_WIDTH_TASK_COUNT
      .Column(GRID_COLUMN_TASK_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER

      ' total time to init task - 3
      .Column(GRID_COLUMN_INIT_TASK_COUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6909) '"Tareas Aceptadas"
      .Column(GRID_COLUMN_INIT_TASK_COUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6910) '"Cantidad "
      .Column(GRID_COLUMN_INIT_TASK_COUNT).Width = GRID_WIDTH_INIT_TASK_COUNT
      .Column(GRID_COLUMN_INIT_TASK_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER

      ' total time to init task - 4
      .Column(GRID_COLUMN_TOTAL_TIME_TO_INIT_TASK).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6909)
      .Column(GRID_COLUMN_TOTAL_TIME_TO_INIT_TASK).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6911) '"Tiempo Total"
      .Column(GRID_COLUMN_TOTAL_TIME_TO_INIT_TASK).Width = GRID_WIDTH_TOTAL_TIME_TO_INIT_TASK
      .Column(GRID_COLUMN_TOTAL_TIME_TO_INIT_TASK).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER

      ' avg time to init task - 5
      .Column(GRID_COLUMN_AVG_TIME_TO_INIT_TASK).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6909)
      .Column(GRID_COLUMN_AVG_TIME_TO_INIT_TASK).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6912) '"Tiempo Medio"
      .Column(GRID_COLUMN_AVG_TIME_TO_INIT_TASK).Width = GRID_WIDTH_AVG_TIME_TO_INIT_TASK
      .Column(GRID_COLUMN_AVG_TIME_TO_INIT_TASK).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER

      ' total time to solve task - 6
      .Column(GRID_COLUMN_SOLVING_TASK_COUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6913) '"Tareas Resueltas"
      .Column(GRID_COLUMN_SOLVING_TASK_COUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6908)
      .Column(GRID_COLUMN_SOLVING_TASK_COUNT).Width = GRID_WIDTH_SOLVING_TASK_COUNT
      .Column(GRID_COLUMN_SOLVING_TASK_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER

      ' total time to solve task - 7
      .Column(GRID_COLUMN_TOTAL_TIME_SOLVING_TASK).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6913)
      .Column(GRID_COLUMN_TOTAL_TIME_SOLVING_TASK).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6911)
      .Column(GRID_COLUMN_TOTAL_TIME_SOLVING_TASK).Width = GRID_WIDTH_AVG_TIME_SOLVING_TASK
      .Column(GRID_COLUMN_TOTAL_TIME_SOLVING_TASK).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER

      ' avg time to solve task - 8
      .Column(GRID_COLUMN_AVG_TIME_SOLVING_TASK).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6913)
      .Column(GRID_COLUMN_AVG_TIME_SOLVING_TASK).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6912)
      .Column(GRID_COLUMN_AVG_TIME_SOLVING_TASK).Width = GRID_WIDTH_AVG_TIME_SOLVING_TASK
      .Column(GRID_COLUMN_AVG_TIME_SOLVING_TASK).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER
    End With
  End Sub ' GUI_StyleSheet

  ' PURPOSE: Updates subtotals and totals variables
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub UpdateTotalizers(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW)

    m_user_subtotal_count = m_user_subtotal_count + DbRow.Value(SQL_COLUMN_TASK_COUNT)
    m_total_count = m_total_count + DbRow.Value(SQL_COLUMN_TASK_COUNT)

    If Not DbRow.Value(SQL_COLUMN_INIT_TASK_COUNT) Is DBNull.Value Then
      m_user_subtotal_count_to_init += DbRow.Value(SQL_COLUMN_INIT_TASK_COUNT)
      m_total_count_time_to_init += DbRow.Value(SQL_COLUMN_INIT_TASK_COUNT)
    End If

    If Not DbRow.Value(SQL_COLUMN_TOTAL_TIME_TO_INIT_TASK) Is DBNull.Value Then
      m_user_subtotal_total_time_to_init += DbRow.Value(SQL_COLUMN_TOTAL_TIME_TO_INIT_TASK)
      m_total_total_time_to_init += DbRow.Value(SQL_COLUMN_TOTAL_TIME_TO_INIT_TASK)
    End If

    If Not DbRow.Value(SQL_COLUMN_SOLVING_TASK_COUNT) Is DBNull.Value Then
      m_user_subtotal_count_to_solve += DbRow.Value(SQL_COLUMN_SOLVING_TASK_COUNT)
      m_total_count_time_to_solve += DbRow.Value(SQL_COLUMN_SOLVING_TASK_COUNT)
    End If

    If Not DbRow.Value(SQL_COLUMN_TOTAL_TIME_SOLVING_TASK) Is DBNull.Value Then
      m_user_subtotal_total_time_to_solve += DbRow.Value(SQL_COLUMN_TOTAL_TIME_SOLVING_TASK)
      m_total_total_time_to_solve += DbRow.Value(SQL_COLUMN_TOTAL_TIME_SOLVING_TASK)
    End If

    If m_total_count_time_to_init > 0 Then
      m_total_average_time_to_init = m_total_total_time_to_init / m_total_count_time_to_init
    End If
    If m_total_count_time_to_solve > 0 Then
      m_total_average_time_to_solve = m_total_total_time_to_solve / m_total_count_time_to_solve
    End If

    If m_user_subtotal_count_to_solve > 0 Then
      m_user_subtotal_average_time_to_solve = m_user_subtotal_total_time_to_solve / m_user_subtotal_count_to_solve
    End If

    If m_user_subtotal_count_to_init > 0 Then
      m_user_subtotal_average_time_to_init = m_user_subtotal_total_time_to_init / m_user_subtotal_count_to_init
    End If
  End Sub ' UpdateTotalizers

  ' PURPOSE: Print totalizator row in the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub PrintSubTotals(ByVal current_user As String, ByVal IsLastRow As Boolean)
    Dim _idx_row As Integer
    Dim _time As TimeSpan

    If IsLastRow Then
      _idx_row = Me.Grid.AddRow()
    Else
      If chk_Show_Detail.Checked Then
        _idx_row = Me.Grid.NumRows - 1
      Else
        _idx_row = Me.Grid.AddRow()
      End If
    End If

    ' :
    If chk_Show_Detail.Checked Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_USER_NAME).Value = GLB_NLS_GUI_INVOICING.GetString(375) & " " & current_user
    Else
      Me.Grid.Cell(_idx_row, GRID_COLUMN_USER_NAME).Value = current_user
    End If

    ' Label - TOTAL:
    ' Me.Grid.Cell(_idx_row, GRID_COLUMN_GROUP_ALARM).Value = GLB_NLS_GUI_INVOICING.GetString(375)

    ' Total - Points
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TASK_COUNT).Value = GUI_FormatNumber(Me.m_user_subtotal_count, 0)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_INIT_TASK_COUNT).Value = GUI_FormatNumber(Me.m_user_subtotal_count_to_init, 0)

    _time = New TimeSpan(0, 0, Me.m_user_subtotal_total_time_to_init)
    If _time.TotalSeconds > 0 Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_TOTAL_TIME_TO_INIT_TASK).Value = TimeSpanToString(_time)
    End If

    _time = New TimeSpan(0, 0, Me.m_user_subtotal_average_time_to_init)
    If _time.TotalSeconds > 0 Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_AVG_TIME_TO_INIT_TASK).Value = TimeSpanToString(_time)
    End If

    Me.Grid.Cell(_idx_row, GRID_COLUMN_SOLVING_TASK_COUNT).Value = GUI_FormatNumber(Me.m_user_subtotal_count_to_solve, 0)

    _time = New TimeSpan(0, 0, Me.m_user_subtotal_total_time_to_solve)
    If _time.TotalSeconds > 0 Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_TOTAL_TIME_SOLVING_TASK).Value = TimeSpanToString(_time)
    End If

    _time = New TimeSpan(0, 0, Me.m_user_subtotal_average_time_to_solve)
    If _time.TotalSeconds > 0 Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_AVG_TIME_SOLVING_TASK).Value = TimeSpanToString(_time)
    End If

    ' Color Row
    Me.Grid.Row(_idx_row).BackColor = GetColor(COLOR_ROW_SUBTOTAL)

    InitSubTotals()
  End Sub ' PrintSubTotals

  ' PURPOSE: Initializes totals variables 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub InitTotals()
    Me.m_total_count = 0
    Me.m_total_count_time_to_init = 0
    Me.m_total_total_time_to_init = 0
    Me.m_total_average_time_to_init = 0
    Me.m_total_count_time_to_solve = 0
    Me.m_total_total_time_to_solve = 0
    Me.m_total_average_time_to_solve = 0
  End Sub ' InitTotals

  ' PURPOSE: Initializes subtotals variables 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub InitSubTotals()
    Me.m_user_subtotal_count = 0
    Me.m_user_subtotal_count_to_init = 0
    Me.m_user_subtotal_total_time_to_init = 0
    Me.m_user_subtotal_average_time_to_init = 0
    Me.m_user_subtotal_count_to_solve = 0
    Me.m_user_subtotal_total_time_to_solve = 0
    Me.m_user_subtotal_average_time_to_solve = 0
  End Sub ' InitSubTotals

  ' PURPOSE: Get the sql 'where' statement
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - an string with the 'where' statement
  Private Function GetSqlWhere() As String

    Dim _str_where As String = " WHERE "
    Dim _date_from As Date
    Dim _date_to As Date

    _date_from = Me.ds_selector.FromDate.AddHours(Me.ds_selector.ClosingTime)
    _date_to = Me.ds_selector.ToDate.AddHours(Me.ds_selector.ClosingTime)

    If rb_create.Checked Then
      ' Filter Dates
      _str_where = _str_where & " LSA_DATE_CREATED >= " & GUI_FormatDateDB(_date_from)
      If Me.ds_selector.ToDateSelected = True Then
        _str_where = _str_where & " AND LSA_DATE_CREATED < " & GUI_FormatDateDB(_date_to)
      End If
    Else
      _str_where = _str_where & " LST_SOLVED >= " & GUI_FormatDateDB(_date_from)
      If Me.ds_selector.ToDateSelected = True Then
        _str_where = _str_where & " AND LST_SOLVED < " & GUI_FormatDateDB(_date_to)
      End If
    End If
    _str_where += " AND LST_ASSIGNED IS NOT NULL AND (LST_ACCEPTED_USER_ID<>0 OR LST_ASSIGNED_USER_ID<>0)"
    Return _str_where
  End Function ' GetSqlWhere
#End Region

End Class