﻿'-------------------------------------------------------------------
' Copyright © 2016 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_generic_report_view.vb
' DESCRIPTION:   View Excel in Grid for Generic Report
' AUTHOR:        Ervin Olvera Ruiz
' CREATION DATE: 25-MAY-2016
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 17-MAY-2012  EOR    Initial version
' 11-JUL-2016  EOR    Product Backlog Item 13573 : Generic Report: GUI generation Screen
' 16-AUG-2016  EOR    Fixed Bug 16679: Generic Reports - Wrong format date
' 23-AUG-2016  EOR    Product Backlog Item 13573 : Generic Report: GUI generation Screen
' 05-JAN-2018  EOR    PBI 30849: Multisite: Reporte genérico terminales
' 09-APR-2018  EOR    Bug 32266: WIGOS-9773 AGG - SITE - MULTISITE: The terminal master head is the same to the three documents printed from the "Terminal Master" , "Multiplace - Roulettes" and "Multiplace - Progressive" tabs 
' -------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports GUI_Controls.frm_base_sel
Imports WSI.Common.GenericReport
Imports GUI_Reports

Public Class frm_generic_report_view
  Inherits frm_base_edit

#Region " Members"

  Private m_meta_data_Report As IList(Of ReportToolDesignSheetsDataDTO)
  Private m_num_columns As Integer

  'EOR 05-JUL-2016
  Private m_report_name As String
  Private m_excel_data As CLASS_EXCEL_DATA
  Private m_print_data As CLASS_PRINT_DATA
  Private m_print_datetime As Date
  Private m_list_grids As IList(Of uc_grid)
  Private m_index_tab As Integer

  'EOR 15-AUG-2016
  Private m_list_data_table As IList(Of DataTable)
  Private m_dictionary_columns As Dictionary(Of Integer, Type)

  'EOR 23-AUG-2016
  Private m_enum_form As ENUM_FORM

#End Region

#Region " Constants "
  Private Const FORM_DB_MIN_VERSION As Short = 183
#End Region

#Region " Construct"
  Public Sub New(ByVal metadataReport As IList(Of ReportToolDesignSheetsDataDTO), ByVal reportName As String, ByVal parametersReports As CLASS_PRINT_DATA, ByVal enumForm As ENUM_FORM)
    ' This call is required by the designer.
    InitializeComponent()
    ' Add any initialization after the InitializeComponent() call.

    m_enum_form = enumForm
    m_meta_data_Report = metadataReport
    m_report_name = reportName
    m_print_data = parametersReports
  End Sub
#End Region

#Region "Overrides"

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = m_enum_form

    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Check for screen data.
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    ' Nothing to check
    Return True

  End Function ' GUI_IsScreenDataOk

  ' PURPOSE: Sets database permissions.
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    Dim _tabPage As System.Windows.Forms.TabPage
    Dim _dgSheet As GUI_Controls.uc_grid
    Dim _row As DataRow
    Dim _db_row As CLASS_DB_ROW
    Dim _i As Integer
    Dim _idx_row As Integer
    Dim _grid As AxVSFlex8.AxVSFlexGrid

    _i = 1
    m_list_grids = New List(Of uc_grid)
    m_list_data_table = New List(Of DataTable)
    m_index_tab = 0

    For Each _dt_excel As DataTable In m_meta_data_Report(0).DataReport.DataSet.Tables
      m_dictionary_columns = New Dictionary(Of Integer, Type)

      For Each _dc_column As DataColumn In _dt_excel.Columns
        m_dictionary_columns.Add(_dc_column.Ordinal, _dc_column.DataType)
      Next

      _dgSheet = New GUI_Controls.uc_grid()
      _dgSheet.Dock = System.Windows.Forms.DockStyle.Fill
      _dgSheet.Name = "dg_excel_" & _i.ToString()
      _dgSheet.PanelRightVisible = False

      _tabPage = New System.Windows.Forms.TabPage()
      _tabPage.Name = "tab_page_" & _i.ToString()
      _tabPage.Text = _dt_excel.TableName
      _tabPage.Controls.Add(_dgSheet)

      tab_grid.Controls.Add(_tabPage)

      Call GUI_StyleSheet(_dgSheet, _dt_excel)

      m_num_columns = _dt_excel.Columns.Count

      For Each _row In _dt_excel.Rows
        Try
          _db_row = New CLASS_DB_ROW(_row)

          _dgSheet.AddRow()

          _idx_row = _dgSheet.NumRows - 1

          If Not GUI_SetupRow(_dgSheet, _idx_row, _db_row) Then
            _dgSheet.DeleteRowFast(_idx_row)
          End If

        Catch ex As OutOfMemoryException
          Throw
        Catch exception As Exception
          Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
          Call Trace.WriteLine(exception.ToString())
          Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                                "frm_report_generic_view", _
                                "GUI_SetupRow", _
                                exception.Message)
          Exit For
        End Try

        _db_row = Nothing

        If Not GUI_DoEvents(_dgSheet) Then
          Exit Sub
        End If

      Next

      m_list_grids.Insert(_i - 1, _dgSheet)
      m_list_data_table.Insert(_i - 1, _dt_excel)

      _i += 1

      For Each _control As Object In _dgSheet.Controls
        If _control.GetType().ToString() = "AxVSFlex8.AxVSFlexGrid" Then

          _grid = CType(_control, AxVSFlex8.AxVSFlexGrid)
          _grid.AutoSizeMode = VSFlex8.AutoSizeSettings.flexAutoSizeColWidth
          _grid.AutoSize(0, _grid.Cols - 1, False, 0)

          Exit For
        End If
      Next

    Next


  End Sub

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    ' Text Window
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7363) & " - " & m_report_name

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = False
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_PRINT).Visible = False

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Text = GLB_NLS_GUI_CONTROLS.GetString(27)
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Text = GLB_NLS_GUI_CONTROLS.GetString(5)
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)

  End Sub ' GUI_InitControls

  ' PURPOSE: Sets the initial focus
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Focus()
  End Sub

  ' PURPOSE: Process button actions in order to branch to a child screen
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)
    Select Case ButtonId
      Case frm_base_sel.ENUM_BUTTON.BUTTON_DELETE
        Call MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_EXCEL)
      Case frm_base_sel.ENUM_BUTTON.BUTTON_OK
        Call MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_PRINT)
      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)

    End Select
  End Sub ' GUI_ButtonClick
  Protected Overrides Sub GUI_ExcelResultList()
    Call GUI_Report(False)
  End Sub

  Protected Overrides Sub GUI_PrintResultList()
    Call GUI_Report(True)
  End Sub

  'PURPOSE: Executed just before closing form
  '         
  ' PARAMS:
  '    - INPUT :
  '
  '    - OUTPUT :
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)
    MyBase.GUI_Closing(CloseCanceled)
  End Sub ' GUI_Closing

#End Region

#Region "Public Functions"

  ' PURPOSE: Init form in edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - DateFrom
  '           - DateTo
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Overloads Sub ShowEditItem()
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Call Me.Display(True)
  End Sub

#End Region

#Region "Private Functions"

  ' PURPOSE : Sets the values of a row after show in datagrid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Private Function GUI_SetupRow(ByRef Grid As uc_grid, RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _i As Integer
    Dim _columnIndex As Integer

    For _i = 0 To m_num_columns - 1
      _columnIndex = _i + 1

      If Not m_dictionary_columns.ContainsKey(_i) Then
        Grid.Cell(RowIndex, _columnIndex).Value = IIf(DbRow.IsNull(_i), "", DbRow.Value(_i))
      Else
        If m_dictionary_columns.Item(_i) Is GetType(Decimal) Or m_dictionary_columns.Item(_i) Is GetType(Double) Then
          If DbRow.IsNull(_i) Then
            Grid.Cell(RowIndex, _columnIndex).Value = ""
          Else
            Grid.Cell(RowIndex, _columnIndex).Value = GUI_FormatCurrency(DbRow.Value(_i), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, False)
          End If
        Else
          Grid.Cell(RowIndex, _columnIndex).Value = IIf(DbRow.IsNull(_i), "", DbRow.Value(_i))
        End If
      End If
    Next

    Return True
  End Function 'GUI_SetupRow

  Private Sub GUI_StyleSheet(ByRef Grid As uc_grid, ByVal dt_excel As DataTable)
    Dim GRID_COLUMN As Integer
    Dim GRID_COLUMNS As Integer

    GRID_COLUMNS = dt_excel.Columns.Count + 1

    With Grid
      ' Initialize Grid
      Call .Init(GRID_COLUMNS, 1)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' GRID INDEX
      .Column(0).Header(0).Text = ""
      .Column(0).Header(1).Text = ""
      .Column(0).Width = 200
      .Column(0).HighLightWhenSelected = False
      .Column(0).IsColumnPrintable = False

      ' Header Text
      For Each dc_column_excel As DataColumn In dt_excel.Columns
        GRID_COLUMN = dc_column_excel.Ordinal + 1

        .Column(GRID_COLUMN).Header(0).Text = dc_column_excel.ColumnName
        .Column(GRID_COLUMN).IsColumnPrintable = True

        If dc_column_excel.DataType Is GetType(String) Then
          .Column(GRID_COLUMN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
        Else
          .Column(GRID_COLUMN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
        End If

      Next

    End With

  End Sub

  Private Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA)
    Call frm_base_sel.GUI_ReportParamsX(m_list_grids(m_index_tab), ExcelData, m_report_name, WGDB.Now)

    Dim _dt As DataTable = m_list_data_table(m_index_tab)
    Dim _i As Integer

    For Each dc As DataColumn In _dt.Columns
      _i = dc.Ordinal

      If dc.DataType Is GetType(String) Then
        m_excel_data.SetColumnFormat(_i, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)
        Continue For
      End If

      If dc.DataType Is GetType(DateTime) Then
        m_excel_data.SetColumnFormat(_i, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.DATETIME)
        Continue For
      End If

      If dc.DataType Is GetType(Date) Then
        m_excel_data.SetColumnFormat(_i, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.DATE)
        Continue For
      End If

      If dc.DataType Is GetType(Decimal) Or dc.DataType Is GetType(Double) Then
        m_excel_data.SetColumnFormat(_i, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.CURRENCY)
        Continue For
      End If

      If dc.DataType Is GetType(Int16) Or dc.DataType Is GetType(Int32) Or dc.DataType Is GetType(Int64) Then
        m_excel_data.SetColumnFormat(_i, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.NUMERIC)
        Continue For
      End If

      m_excel_data.SetColumnFormat(_i, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.CALCULATED)
    Next

  End Sub ' GUI_ReportParams

  Private Sub GUI_ReportFilter(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA)
    ' Insert filter data
    ExcelData.UpdateReportFilters(m_print_data.Filter.ItemArray)
  End Sub ' GUI_ReportFilter

  Private Sub GUI_ReportData(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA)
    Call frm_base_sel.GUI_ReportDataX(m_list_grids(m_index_tab), ExcelData)
  End Sub 'GUI_ReportData

  Private Sub GUI_PrintReport(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA)
    Call ExcelData.Preview()
  End Sub ' GUI_PrintReport

  Private Sub GUI_Report(ByVal IsPreview As Boolean)
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    Try
      If Not IsNothing(m_excel_data) Then
        m_excel_data = Nothing
      End If

      If IsPreview Then
        m_excel_data = New GUI_Reports.CLASS_EXCEL_DATA(True, True)
      Else
        m_excel_data = New GUI_Reports.CLASS_EXCEL_DATA()
      End If


      If m_excel_data.IsNull() Then
        Return
      End If

      If Not m_excel_data.Cancelled Then
        Call GUI_ReportParams(m_excel_data)
      End If
      If Not m_excel_data.Cancelled Then
        Call GUI_ReportFilter(m_excel_data)
      End If
      If Not m_excel_data.Cancelled Then
        Call GUI_ReportData(m_excel_data)
      End If
      If Not m_excel_data.Cancelled Then
        Call GUI_PrintReport(m_excel_data)
      End If

    Catch ex As Exception
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(146), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            GLB_ApplicationName, _
                            "frm_generic_report_view.GUI_PrintResultList()", _
                            "", _
                            "", _
                            "", _
                           "Error in Print.")
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            GLB_ApplicationName, _
                            "Exception", _
                            "", _
                            "", _
                            "", _
                            ex.Message)
    Finally
      If m_excel_data Is Nothing Then
      Else
        m_excel_data.Dispose()
        m_excel_data = Nothing
      End If
    End Try

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

#End Region

#Region "Events"

  Private Sub tab_grid_Selected(sender As Object, e As TabControlEventArgs) Handles tab_grid.Selected
    m_index_tab = e.TabPageIndex
    m_report_name = e.TabPage.Text  'EOR 09-APR-2018
  End Sub

#End Region

End Class