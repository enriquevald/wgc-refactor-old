<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_site_jackpot_configuration
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_site_jackpot_configuration))
    Me.gb_contribution = New System.Windows.Forms.GroupBox
    Me.opt_week_days = New System.Windows.Forms.RadioButton
    Me.lbl_wednesday = New System.Windows.Forms.Label
    Me.lbl_info_avg_contribution_week_days = New System.Windows.Forms.Label
    Me.lbl_percentaje_sign = New System.Windows.Forms.Label
    Me.ef_saturday = New GUI_Controls.uc_entry_field
    Me.ef_friday = New GUI_Controls.uc_entry_field
    Me.ef_thursday = New GUI_Controls.uc_entry_field
    Me.ef_wednesday = New GUI_Controls.uc_entry_field
    Me.ef_tuesday = New GUI_Controls.uc_entry_field
    Me.ef_monday = New GUI_Controls.uc_entry_field
    Me.ef_sunday = New GUI_Controls.uc_entry_field
    Me.lbl_saturday = New System.Windows.Forms.Label
    Me.lbl_friday = New System.Windows.Forms.Label
    Me.lbl_thursday = New System.Windows.Forms.Label
    Me.lbl_tuesday = New System.Windows.Forms.Label
    Me.lbl_monday = New System.Windows.Forms.Label
    Me.lbl_sunday = New System.Windows.Forms.Label
    Me.opt_fix_contribution = New System.Windows.Forms.RadioButton
    Me.lbl_percentaje = New System.Windows.Forms.Label
    Me.btn_load_last_month_played = New GUI_Controls.uc_button
    Me.ef_last_month_played = New GUI_Controls.uc_entry_field
    Me.chk_only_redeemable = New System.Windows.Forms.CheckBox
    Me.ef_contribution = New GUI_Controls.uc_entry_field
    Me.gb_jackpot_flag = New System.Windows.Forms.GroupBox
    Me.opt_enabled = New System.Windows.Forms.RadioButton
    Me.opt_disabled = New System.Windows.Forms.RadioButton
    Me.gb_wrk_days = New System.Windows.Forms.GroupBox
    Me.chk_exclude_anonymous_accounts = New System.Windows.Forms.CheckBox
    Me.ef_min_occupation = New GUI_Controls.uc_entry_field
    Me.chk_exclude_account_with_promotion = New System.Windows.Forms.CheckBox
    Me.dtp_working_from = New GUI_Controls.uc_date_picker
    Me.dtp_working_to = New GUI_Controls.uc_date_picker
    Me.chk_sunday = New System.Windows.Forms.CheckBox
    Me.chk_saturday = New System.Windows.Forms.CheckBox
    Me.chk_friday = New System.Windows.Forms.CheckBox
    Me.chk_thursday = New System.Windows.Forms.CheckBox
    Me.chk_wednesday = New System.Windows.Forms.CheckBox
    Me.chk_tuesday = New System.Windows.Forms.CheckBox
    Me.chk_monday = New System.Windows.Forms.CheckBox
    Me.dg_jackpot_list = New GUI_Controls.uc_grid
    Me.gb_parameters = New System.Windows.Forms.GroupBox
    Me.chk_winner_name = New System.Windows.Forms.CheckBox
    Me.chk_terminal_name = New System.Windows.Forms.CheckBox
    Me.lbl_animation_interval_02 = New System.Windows.Forms.Label
    Me.cmb_anim_interval_02 = New GUI_Controls.uc_combo
    Me.lbl_recent_interval = New System.Windows.Forms.Label
    Me.cmb_recent_interval = New GUI_Controls.uc_combo
    Me.lbl_animation_interval_01 = New System.Windows.Forms.Label
    Me.cmb_anim_interval_01 = New GUI_Controls.uc_combo
    Me.gb_promo_message = New System.Windows.Forms.GroupBox
    Me.lbl_message_3 = New System.Windows.Forms.Label
    Me.txt_message_3 = New System.Windows.Forms.TextBox
    Me.lbl_message_2 = New System.Windows.Forms.Label
    Me.txt_message_2 = New System.Windows.Forms.TextBox
    Me.lbl_message_1 = New System.Windows.Forms.Label
    Me.txt_message_1 = New System.Windows.Forms.TextBox
    Me.lbl_accumulated_total = New GUI_Controls.uc_text_field
    Me.lbl_to_compensate = New GUI_Controls.uc_text_field
    Me.tmr_refresh_jackpot = New System.Windows.Forms.Timer(Me.components)
    Me.gb_payment_mode = New System.Windows.Forms.GroupBox
    Me.opt_manual_payment = New System.Windows.Forms.RadioButton
    Me.opt_automatic_payment = New System.Windows.Forms.RadioButton
    Me.dg_providers = New GUI_Controls.uc_terminals_group_filter
    Me.chk_allow_exceed_max = New System.Windows.Forms.CheckBox
    Me.lbl_current_compensate_pct = New GUI_Controls.uc_text_field
    Me.panel_data.SuspendLayout()
    Me.gb_contribution.SuspendLayout()
    Me.gb_jackpot_flag.SuspendLayout()
    Me.gb_wrk_days.SuspendLayout()
    Me.gb_parameters.SuspendLayout()
    Me.gb_promo_message.SuspendLayout()
    Me.gb_payment_mode.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.lbl_current_compensate_pct)
    Me.panel_data.Controls.Add(Me.chk_allow_exceed_max)
    Me.panel_data.Controls.Add(Me.dg_providers)
    Me.panel_data.Controls.Add(Me.gb_payment_mode)
    Me.panel_data.Controls.Add(Me.lbl_to_compensate)
    Me.panel_data.Controls.Add(Me.lbl_accumulated_total)
    Me.panel_data.Controls.Add(Me.gb_promo_message)
    Me.panel_data.Controls.Add(Me.gb_parameters)
    Me.panel_data.Controls.Add(Me.gb_wrk_days)
    Me.panel_data.Controls.Add(Me.dg_jackpot_list)
    Me.panel_data.Controls.Add(Me.gb_contribution)
    Me.panel_data.Controls.Add(Me.gb_jackpot_flag)
    Me.panel_data.Size = New System.Drawing.Size(895, 715)
    '
    'gb_contribution
    '
    Me.gb_contribution.Controls.Add(Me.opt_week_days)
    Me.gb_contribution.Controls.Add(Me.lbl_wednesday)
    Me.gb_contribution.Controls.Add(Me.lbl_info_avg_contribution_week_days)
    Me.gb_contribution.Controls.Add(Me.lbl_percentaje_sign)
    Me.gb_contribution.Controls.Add(Me.ef_saturday)
    Me.gb_contribution.Controls.Add(Me.ef_friday)
    Me.gb_contribution.Controls.Add(Me.ef_thursday)
    Me.gb_contribution.Controls.Add(Me.ef_wednesday)
    Me.gb_contribution.Controls.Add(Me.ef_tuesday)
    Me.gb_contribution.Controls.Add(Me.ef_monday)
    Me.gb_contribution.Controls.Add(Me.ef_sunday)
    Me.gb_contribution.Controls.Add(Me.lbl_saturday)
    Me.gb_contribution.Controls.Add(Me.lbl_friday)
    Me.gb_contribution.Controls.Add(Me.lbl_thursday)
    Me.gb_contribution.Controls.Add(Me.lbl_tuesday)
    Me.gb_contribution.Controls.Add(Me.lbl_monday)
    Me.gb_contribution.Controls.Add(Me.lbl_sunday)
    Me.gb_contribution.Controls.Add(Me.opt_fix_contribution)
    Me.gb_contribution.Controls.Add(Me.lbl_percentaje)
    Me.gb_contribution.Controls.Add(Me.btn_load_last_month_played)
    Me.gb_contribution.Controls.Add(Me.ef_last_month_played)
    Me.gb_contribution.Controls.Add(Me.chk_only_redeemable)
    Me.gb_contribution.Controls.Add(Me.ef_contribution)
    Me.gb_contribution.Location = New System.Drawing.Point(146, 3)
    Me.gb_contribution.Name = "gb_contribution"
    Me.gb_contribution.Size = New System.Drawing.Size(741, 132)
    Me.gb_contribution.TabIndex = 2
    Me.gb_contribution.TabStop = False
    Me.gb_contribution.Text = "xContribution"
    '
    'opt_week_days
    '
    Me.opt_week_days.AutoSize = True
    Me.opt_week_days.BackColor = System.Drawing.Color.Transparent
    Me.opt_week_days.Location = New System.Drawing.Point(129, 46)
    Me.opt_week_days.Name = "opt_week_days"
    Me.opt_week_days.Size = New System.Drawing.Size(93, 17)
    Me.opt_week_days.TabIndex = 4
    Me.opt_week_days.TabStop = True
    Me.opt_week_days.Text = "xWeekDays"
    Me.opt_week_days.UseVisualStyleBackColor = False
    '
    'lbl_wednesday
    '
    Me.lbl_wednesday.BackColor = System.Drawing.Color.Transparent
    Me.lbl_wednesday.Location = New System.Drawing.Point(451, 48)
    Me.lbl_wednesday.Name = "lbl_wednesday"
    Me.lbl_wednesday.Size = New System.Drawing.Size(74, 13)
    Me.lbl_wednesday.TabIndex = 10
    Me.lbl_wednesday.Text = "xWednesday"
    Me.lbl_wednesday.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_info_avg_contribution_week_days
    '
    Me.lbl_info_avg_contribution_week_days.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.lbl_info_avg_contribution_week_days.Location = New System.Drawing.Point(476, 93)
    Me.lbl_info_avg_contribution_week_days.Name = "lbl_info_avg_contribution_week_days"
    Me.lbl_info_avg_contribution_week_days.Size = New System.Drawing.Size(251, 34)
    Me.lbl_info_avg_contribution_week_days.TabIndex = 8
    Me.lbl_info_avg_contribution_week_days.Text = "xInfoAvgContributionWeekDays"
    Me.lbl_info_avg_contribution_week_days.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_percentaje_sign
    '
    Me.lbl_percentaje_sign.AutoSize = True
    Me.lbl_percentaje_sign.Location = New System.Drawing.Point(708, 69)
    Me.lbl_percentaje_sign.Name = "lbl_percentaje_sign"
    Me.lbl_percentaje_sign.Size = New System.Drawing.Size(19, 13)
    Me.lbl_percentaje_sign.TabIndex = 22
    Me.lbl_percentaje_sign.Text = "%"
    '
    'ef_saturday
    '
    Me.ef_saturday.DoubleValue = 0
    Me.ef_saturday.IntegerValue = 0
    Me.ef_saturday.IsReadOnly = False
    Me.ef_saturday.Location = New System.Drawing.Point(652, 63)
    Me.ef_saturday.Name = "ef_saturday"
    Me.ef_saturday.PlaceHolder = Nothing
    Me.ef_saturday.Size = New System.Drawing.Size(55, 24)
    Me.ef_saturday.SufixText = "Sufix Text"
    Me.ef_saturday.SufixTextVisible = True
    Me.ef_saturday.TabIndex = 11
    Me.ef_saturday.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_saturday.TextValue = ""
    Me.ef_saturday.TextWidth = 0
    Me.ef_saturday.Value = ""
    Me.ef_saturday.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_friday
    '
    Me.ef_friday.DoubleValue = 0
    Me.ef_friday.IntegerValue = 0
    Me.ef_friday.IsReadOnly = False
    Me.ef_friday.Location = New System.Drawing.Point(589, 63)
    Me.ef_friday.Name = "ef_friday"
    Me.ef_friday.PlaceHolder = Nothing
    Me.ef_friday.Size = New System.Drawing.Size(55, 24)
    Me.ef_friday.SufixText = "Sufix Text"
    Me.ef_friday.SufixTextVisible = True
    Me.ef_friday.TabIndex = 10
    Me.ef_friday.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_friday.TextValue = ""
    Me.ef_friday.TextWidth = 0
    Me.ef_friday.Value = ""
    Me.ef_friday.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_thursday
    '
    Me.ef_thursday.DoubleValue = 0
    Me.ef_thursday.IntegerValue = 0
    Me.ef_thursday.IsReadOnly = False
    Me.ef_thursday.Location = New System.Drawing.Point(525, 63)
    Me.ef_thursday.Name = "ef_thursday"
    Me.ef_thursday.PlaceHolder = Nothing
    Me.ef_thursday.Size = New System.Drawing.Size(55, 24)
    Me.ef_thursday.SufixText = "Sufix Text"
    Me.ef_thursday.SufixTextVisible = True
    Me.ef_thursday.TabIndex = 9
    Me.ef_thursday.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_thursday.TextValue = ""
    Me.ef_thursday.TextWidth = 0
    Me.ef_thursday.Value = ""
    Me.ef_thursday.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_wednesday
    '
    Me.ef_wednesday.DoubleValue = 0
    Me.ef_wednesday.IntegerValue = 0
    Me.ef_wednesday.IsReadOnly = False
    Me.ef_wednesday.Location = New System.Drawing.Point(458, 63)
    Me.ef_wednesday.Name = "ef_wednesday"
    Me.ef_wednesday.PlaceHolder = Nothing
    Me.ef_wednesday.Size = New System.Drawing.Size(55, 24)
    Me.ef_wednesday.SufixText = "Sufix Text"
    Me.ef_wednesday.SufixTextVisible = True
    Me.ef_wednesday.TabIndex = 8
    Me.ef_wednesday.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_wednesday.TextValue = ""
    Me.ef_wednesday.TextWidth = 0
    Me.ef_wednesday.Value = ""
    Me.ef_wednesday.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_tuesday
    '
    Me.ef_tuesday.DoubleValue = 0
    Me.ef_tuesday.IntegerValue = 0
    Me.ef_tuesday.IsReadOnly = False
    Me.ef_tuesday.Location = New System.Drawing.Point(388, 63)
    Me.ef_tuesday.Name = "ef_tuesday"
    Me.ef_tuesday.PlaceHolder = Nothing
    Me.ef_tuesday.Size = New System.Drawing.Size(55, 24)
    Me.ef_tuesday.SufixText = "Sufix Text"
    Me.ef_tuesday.SufixTextVisible = True
    Me.ef_tuesday.TabIndex = 7
    Me.ef_tuesday.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_tuesday.TextValue = ""
    Me.ef_tuesday.TextWidth = 0
    Me.ef_tuesday.Value = ""
    Me.ef_tuesday.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_monday
    '
    Me.ef_monday.DoubleValue = 0
    Me.ef_monday.IntegerValue = 0
    Me.ef_monday.IsReadOnly = False
    Me.ef_monday.Location = New System.Drawing.Point(324, 63)
    Me.ef_monday.Name = "ef_monday"
    Me.ef_monday.PlaceHolder = Nothing
    Me.ef_monday.Size = New System.Drawing.Size(55, 24)
    Me.ef_monday.SufixText = "Sufix Text"
    Me.ef_monday.SufixTextVisible = True
    Me.ef_monday.TabIndex = 6
    Me.ef_monday.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_monday.TextValue = ""
    Me.ef_monday.TextWidth = 0
    Me.ef_monday.Value = ""
    Me.ef_monday.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_sunday
    '
    Me.ef_sunday.DoubleValue = 0
    Me.ef_sunday.IntegerValue = 0
    Me.ef_sunday.IsReadOnly = False
    Me.ef_sunday.Location = New System.Drawing.Point(258, 63)
    Me.ef_sunday.Name = "ef_sunday"
    Me.ef_sunday.PlaceHolder = Nothing
    Me.ef_sunday.Size = New System.Drawing.Size(55, 24)
    Me.ef_sunday.SufixText = "Sufix Text"
    Me.ef_sunday.SufixTextVisible = True
    Me.ef_sunday.TabIndex = 5
    Me.ef_sunday.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_sunday.TextValue = ""
    Me.ef_sunday.TextWidth = 0
    Me.ef_sunday.Value = ""
    Me.ef_sunday.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_saturday
    '
    Me.lbl_saturday.BackColor = System.Drawing.Color.Transparent
    Me.lbl_saturday.Location = New System.Drawing.Point(649, 48)
    Me.lbl_saturday.Name = "lbl_saturday"
    Me.lbl_saturday.Size = New System.Drawing.Size(66, 13)
    Me.lbl_saturday.TabIndex = 13
    Me.lbl_saturday.Text = "xSaturday"
    Me.lbl_saturday.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_friday
    '
    Me.lbl_friday.BackColor = System.Drawing.Color.Transparent
    Me.lbl_friday.Location = New System.Drawing.Point(586, 48)
    Me.lbl_friday.Name = "lbl_friday"
    Me.lbl_friday.Size = New System.Drawing.Size(57, 13)
    Me.lbl_friday.TabIndex = 12
    Me.lbl_friday.Text = "xFriday"
    Me.lbl_friday.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_thursday
    '
    Me.lbl_thursday.BackColor = System.Drawing.Color.Transparent
    Me.lbl_thursday.Location = New System.Drawing.Point(524, 48)
    Me.lbl_thursday.Name = "lbl_thursday"
    Me.lbl_thursday.Size = New System.Drawing.Size(70, 13)
    Me.lbl_thursday.TabIndex = 11
    Me.lbl_thursday.Text = "xThursday"
    Me.lbl_thursday.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_tuesday
    '
    Me.lbl_tuesday.BackColor = System.Drawing.Color.Transparent
    Me.lbl_tuesday.Location = New System.Drawing.Point(383, 48)
    Me.lbl_tuesday.Name = "lbl_tuesday"
    Me.lbl_tuesday.Size = New System.Drawing.Size(64, 13)
    Me.lbl_tuesday.TabIndex = 9
    Me.lbl_tuesday.Text = "xTuesday"
    Me.lbl_tuesday.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_monday
    '
    Me.lbl_monday.BackColor = System.Drawing.Color.Transparent
    Me.lbl_monday.Location = New System.Drawing.Point(321, 48)
    Me.lbl_monday.Name = "lbl_monday"
    Me.lbl_monday.Size = New System.Drawing.Size(60, 13)
    Me.lbl_monday.TabIndex = 6
    Me.lbl_monday.Text = "xMonday"
    Me.lbl_monday.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_sunday
    '
    Me.lbl_sunday.BackColor = System.Drawing.Color.Transparent
    Me.lbl_sunday.Location = New System.Drawing.Point(258, 48)
    Me.lbl_sunday.Name = "lbl_sunday"
    Me.lbl_sunday.Size = New System.Drawing.Size(59, 13)
    Me.lbl_sunday.TabIndex = 5
    Me.lbl_sunday.Text = "xSunday"
    Me.lbl_sunday.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'opt_fix_contribution
    '
    Me.opt_fix_contribution.AutoSize = True
    Me.opt_fix_contribution.Location = New System.Drawing.Point(129, 26)
    Me.opt_fix_contribution.Name = "opt_fix_contribution"
    Me.opt_fix_contribution.Size = New System.Drawing.Size(118, 17)
    Me.opt_fix_contribution.TabIndex = 2
    Me.opt_fix_contribution.TabStop = True
    Me.opt_fix_contribution.Text = "xFixContribution"
    Me.opt_fix_contribution.UseVisualStyleBackColor = True
    '
    'lbl_percentaje
    '
    Me.lbl_percentaje.AutoSize = True
    Me.lbl_percentaje.Location = New System.Drawing.Point(125, 8)
    Me.lbl_percentaje.Name = "lbl_percentaje"
    Me.lbl_percentaje.Size = New System.Drawing.Size(75, 13)
    Me.lbl_percentaje.TabIndex = 1
    Me.lbl_percentaje.Text = "xPercentaje"
    '
    'btn_load_last_month_played
    '
    Me.btn_load_last_month_played.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_load_last_month_played.Location = New System.Drawing.Point(333, 92)
    Me.btn_load_last_month_played.Name = "btn_load_last_month_played"
    Me.btn_load_last_month_played.Size = New System.Drawing.Size(90, 30)
    Me.btn_load_last_month_played.TabIndex = 13
    Me.btn_load_last_month_played.ToolTipped = False
    Me.btn_load_last_month_played.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'ef_last_month_played
    '
    Me.ef_last_month_played.DoubleValue = 0
    Me.ef_last_month_played.IntegerValue = 0
    Me.ef_last_month_played.IsReadOnly = False
    Me.ef_last_month_played.Location = New System.Drawing.Point(8, 95)
    Me.ef_last_month_played.Name = "ef_last_month_played"
    Me.ef_last_month_played.PlaceHolder = Nothing
    Me.ef_last_month_played.Size = New System.Drawing.Size(320, 24)
    Me.ef_last_month_played.SufixText = "Sufix Text"
    Me.ef_last_month_played.SufixTextVisible = True
    Me.ef_last_month_played.TabIndex = 12
    Me.ef_last_month_played.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_last_month_played.TextValue = ""
    Me.ef_last_month_played.TextWidth = 180
    Me.ef_last_month_played.Value = ""
    Me.ef_last_month_played.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_only_redeemable
    '
    Me.chk_only_redeemable.AutoSize = True
    Me.chk_only_redeemable.Location = New System.Drawing.Point(6, 26)
    Me.chk_only_redeemable.Name = "chk_only_redeemable"
    Me.chk_only_redeemable.Size = New System.Drawing.Size(130, 17)
    Me.chk_only_redeemable.TabIndex = 0
    Me.chk_only_redeemable.Text = "xOnlyRedeemable"
    Me.chk_only_redeemable.UseVisualStyleBackColor = True
    '
    'ef_contribution
    '
    Me.ef_contribution.DoubleValue = 0
    Me.ef_contribution.IntegerValue = 0
    Me.ef_contribution.IsReadOnly = False
    Me.ef_contribution.Location = New System.Drawing.Point(259, 24)
    Me.ef_contribution.Name = "ef_contribution"
    Me.ef_contribution.PlaceHolder = Nothing
    Me.ef_contribution.Size = New System.Drawing.Size(75, 24)
    Me.ef_contribution.SufixText = "%"
    Me.ef_contribution.SufixTextVisible = True
    Me.ef_contribution.SufixTextWidth = 20
    Me.ef_contribution.TabIndex = 3
    Me.ef_contribution.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_contribution.TextValue = ""
    Me.ef_contribution.TextWidth = 0
    Me.ef_contribution.Value = ""
    Me.ef_contribution.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_jackpot_flag
    '
    Me.gb_jackpot_flag.Controls.Add(Me.opt_enabled)
    Me.gb_jackpot_flag.Controls.Add(Me.opt_disabled)
    Me.gb_jackpot_flag.Location = New System.Drawing.Point(3, 3)
    Me.gb_jackpot_flag.Name = "gb_jackpot_flag"
    Me.gb_jackpot_flag.Size = New System.Drawing.Size(138, 60)
    Me.gb_jackpot_flag.TabIndex = 0
    Me.gb_jackpot_flag.TabStop = False
    Me.gb_jackpot_flag.Text = "xStatus"
    '
    'opt_enabled
    '
    Me.opt_enabled.Location = New System.Drawing.Point(16, 20)
    Me.opt_enabled.Name = "opt_enabled"
    Me.opt_enabled.Size = New System.Drawing.Size(88, 15)
    Me.opt_enabled.TabIndex = 0
    Me.opt_enabled.Text = "xEnabled"
    '
    'opt_disabled
    '
    Me.opt_disabled.Location = New System.Drawing.Point(16, 40)
    Me.opt_disabled.Name = "opt_disabled"
    Me.opt_disabled.Size = New System.Drawing.Size(112, 15)
    Me.opt_disabled.TabIndex = 1
    Me.opt_disabled.Text = "xDisabled"
    '
    'gb_wrk_days
    '
    Me.gb_wrk_days.Controls.Add(Me.chk_exclude_anonymous_accounts)
    Me.gb_wrk_days.Controls.Add(Me.ef_min_occupation)
    Me.gb_wrk_days.Controls.Add(Me.chk_exclude_account_with_promotion)
    Me.gb_wrk_days.Controls.Add(Me.dtp_working_from)
    Me.gb_wrk_days.Controls.Add(Me.dtp_working_to)
    Me.gb_wrk_days.Controls.Add(Me.chk_sunday)
    Me.gb_wrk_days.Controls.Add(Me.chk_saturday)
    Me.gb_wrk_days.Controls.Add(Me.chk_friday)
    Me.gb_wrk_days.Controls.Add(Me.chk_thursday)
    Me.gb_wrk_days.Controls.Add(Me.chk_wednesday)
    Me.gb_wrk_days.Controls.Add(Me.chk_tuesday)
    Me.gb_wrk_days.Controls.Add(Me.chk_monday)
    Me.gb_wrk_days.Location = New System.Drawing.Point(3, 268)
    Me.gb_wrk_days.Name = "gb_wrk_days"
    Me.gb_wrk_days.Size = New System.Drawing.Size(472, 156)
    Me.gb_wrk_days.TabIndex = 4
    Me.gb_wrk_days.TabStop = False
    Me.gb_wrk_days.Text = "xWorking Days"
    '
    'chk_exclude_anonymous_accounts
    '
    Me.chk_exclude_anonymous_accounts.AutoSize = True
    Me.chk_exclude_anonymous_accounts.Location = New System.Drawing.Point(147, 129)
    Me.chk_exclude_anonymous_accounts.Name = "chk_exclude_anonymous_accounts"
    Me.chk_exclude_anonymous_accounts.Size = New System.Drawing.Size(201, 17)
    Me.chk_exclude_anonymous_accounts.TabIndex = 11
    Me.chk_exclude_anonymous_accounts.Text = "xExclude anonymous accounts"
    Me.chk_exclude_anonymous_accounts.UseVisualStyleBackColor = True
    '
    'ef_min_occupation
    '
    Me.ef_min_occupation.DoubleValue = 0
    Me.ef_min_occupation.IntegerValue = 0
    Me.ef_min_occupation.IsReadOnly = False
    Me.ef_min_occupation.Location = New System.Drawing.Point(130, 82)
    Me.ef_min_occupation.Name = "ef_min_occupation"
    Me.ef_min_occupation.PlaceHolder = Nothing
    Me.ef_min_occupation.Size = New System.Drawing.Size(218, 24)
    Me.ef_min_occupation.SufixText = "%"
    Me.ef_min_occupation.SufixTextVisible = True
    Me.ef_min_occupation.SufixTextWidth = 20
    Me.ef_min_occupation.TabIndex = 9
    Me.ef_min_occupation.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_min_occupation.TextValue = ""
    Me.ef_min_occupation.TextWidth = 135
    Me.ef_min_occupation.Value = ""
    Me.ef_min_occupation.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_exclude_account_with_promotion
    '
    Me.chk_exclude_account_with_promotion.AutoSize = True
    Me.chk_exclude_account_with_promotion.Location = New System.Drawing.Point(147, 112)
    Me.chk_exclude_account_with_promotion.Name = "chk_exclude_account_with_promotion"
    Me.chk_exclude_account_with_promotion.Size = New System.Drawing.Size(220, 17)
    Me.chk_exclude_account_with_promotion.TabIndex = 10
    Me.chk_exclude_account_with_promotion.Text = "xExclude accounts with promotion"
    Me.chk_exclude_account_with_promotion.UseVisualStyleBackColor = True
    '
    'dtp_working_from
    '
    Me.dtp_working_from.Checked = True
    Me.dtp_working_from.IsReadOnly = False
    Me.dtp_working_from.Location = New System.Drawing.Point(147, 20)
    Me.dtp_working_from.Name = "dtp_working_from"
    Me.dtp_working_from.ShowCheckBox = False
    Me.dtp_working_from.ShowUpDown = False
    Me.dtp_working_from.Size = New System.Drawing.Size(112, 24)
    Me.dtp_working_from.SufixText = "Sufix Text"
    Me.dtp_working_from.SufixTextVisible = True
    Me.dtp_working_from.TabIndex = 7
    Me.dtp_working_from.TextWidth = 50
    Me.dtp_working_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_working_to
    '
    Me.dtp_working_to.Checked = True
    Me.dtp_working_to.IsReadOnly = False
    Me.dtp_working_to.Location = New System.Drawing.Point(147, 52)
    Me.dtp_working_to.Name = "dtp_working_to"
    Me.dtp_working_to.ShowCheckBox = False
    Me.dtp_working_to.ShowUpDown = False
    Me.dtp_working_to.Size = New System.Drawing.Size(112, 24)
    Me.dtp_working_to.SufixText = "Sufix Text"
    Me.dtp_working_to.SufixTextVisible = True
    Me.dtp_working_to.TabIndex = 8
    Me.dtp_working_to.TextWidth = 50
    Me.dtp_working_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'chk_sunday
    '
    Me.chk_sunday.AutoSize = True
    Me.chk_sunday.Location = New System.Drawing.Point(9, 129)
    Me.chk_sunday.Name = "chk_sunday"
    Me.chk_sunday.Size = New System.Drawing.Size(76, 17)
    Me.chk_sunday.TabIndex = 6
    Me.chk_sunday.Text = "xSunday"
    Me.chk_sunday.UseVisualStyleBackColor = True
    '
    'chk_saturday
    '
    Me.chk_saturday.AutoSize = True
    Me.chk_saturday.Location = New System.Drawing.Point(9, 112)
    Me.chk_saturday.Name = "chk_saturday"
    Me.chk_saturday.Size = New System.Drawing.Size(85, 17)
    Me.chk_saturday.TabIndex = 5
    Me.chk_saturday.Text = "xSaturday"
    Me.chk_saturday.UseVisualStyleBackColor = True
    '
    'chk_friday
    '
    Me.chk_friday.AutoSize = True
    Me.chk_friday.Location = New System.Drawing.Point(9, 95)
    Me.chk_friday.Name = "chk_friday"
    Me.chk_friday.Size = New System.Drawing.Size(68, 17)
    Me.chk_friday.TabIndex = 4
    Me.chk_friday.Text = "xFriday"
    Me.chk_friday.UseVisualStyleBackColor = True
    '
    'chk_thursday
    '
    Me.chk_thursday.AutoSize = True
    Me.chk_thursday.Location = New System.Drawing.Point(9, 78)
    Me.chk_thursday.Name = "chk_thursday"
    Me.chk_thursday.Size = New System.Drawing.Size(86, 17)
    Me.chk_thursday.TabIndex = 3
    Me.chk_thursday.Text = "xThursday"
    Me.chk_thursday.UseVisualStyleBackColor = True
    '
    'chk_wednesday
    '
    Me.chk_wednesday.AutoSize = True
    Me.chk_wednesday.Location = New System.Drawing.Point(9, 61)
    Me.chk_wednesday.Name = "chk_wednesday"
    Me.chk_wednesday.Size = New System.Drawing.Size(99, 17)
    Me.chk_wednesday.TabIndex = 2
    Me.chk_wednesday.Text = "xWednesday"
    Me.chk_wednesday.UseVisualStyleBackColor = True
    '
    'chk_tuesday
    '
    Me.chk_tuesday.AutoSize = True
    Me.chk_tuesday.Location = New System.Drawing.Point(9, 44)
    Me.chk_tuesday.Name = "chk_tuesday"
    Me.chk_tuesday.Size = New System.Drawing.Size(81, 17)
    Me.chk_tuesday.TabIndex = 1
    Me.chk_tuesday.Text = "xTuesday"
    Me.chk_tuesday.UseVisualStyleBackColor = True
    '
    'chk_monday
    '
    Me.chk_monday.AutoSize = True
    Me.chk_monday.Location = New System.Drawing.Point(9, 27)
    Me.chk_monday.Name = "chk_monday"
    Me.chk_monday.Size = New System.Drawing.Size(77, 17)
    Me.chk_monday.TabIndex = 0
    Me.chk_monday.Text = "xMonday"
    Me.chk_monday.UseVisualStyleBackColor = True
    '
    'dg_jackpot_list
    '
    Me.dg_jackpot_list.CurrentCol = -1
    Me.dg_jackpot_list.CurrentRow = -1
    Me.dg_jackpot_list.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_jackpot_list.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_jackpot_list.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_jackpot_list.Location = New System.Drawing.Point(3, 141)
    Me.dg_jackpot_list.Name = "dg_jackpot_list"
    Me.dg_jackpot_list.PanelRightVisible = False
    Me.dg_jackpot_list.Redraw = True
    Me.dg_jackpot_list.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_jackpot_list.Size = New System.Drawing.Size(886, 69)
    Me.dg_jackpot_list.Sortable = False
    Me.dg_jackpot_list.SortAscending = True
    Me.dg_jackpot_list.SortByCol = 0
    Me.dg_jackpot_list.TabIndex = 3
    Me.dg_jackpot_list.ToolTipped = True
    Me.dg_jackpot_list.TopRow = -2
    '
    'gb_parameters
    '
    Me.gb_parameters.Controls.Add(Me.chk_winner_name)
    Me.gb_parameters.Controls.Add(Me.chk_terminal_name)
    Me.gb_parameters.Controls.Add(Me.lbl_animation_interval_02)
    Me.gb_parameters.Controls.Add(Me.cmb_anim_interval_02)
    Me.gb_parameters.Controls.Add(Me.lbl_recent_interval)
    Me.gb_parameters.Controls.Add(Me.cmb_recent_interval)
    Me.gb_parameters.Controls.Add(Me.lbl_animation_interval_01)
    Me.gb_parameters.Controls.Add(Me.cmb_anim_interval_01)
    Me.gb_parameters.Location = New System.Drawing.Point(3, 430)
    Me.gb_parameters.Name = "gb_parameters"
    Me.gb_parameters.Size = New System.Drawing.Size(472, 169)
    Me.gb_parameters.TabIndex = 5
    Me.gb_parameters.TabStop = False
    Me.gb_parameters.Text = "xJackpot Award Parameters"
    '
    'chk_winner_name
    '
    Me.chk_winner_name.AutoSize = True
    Me.chk_winner_name.Enabled = False
    Me.chk_winner_name.Location = New System.Drawing.Point(15, 112)
    Me.chk_winner_name.Name = "chk_winner_name"
    Me.chk_winner_name.Size = New System.Drawing.Size(142, 17)
    Me.chk_winner_name.TabIndex = 6
    Me.chk_winner_name.Text = "xShow winner name"
    Me.chk_winner_name.UseVisualStyleBackColor = True
    '
    'chk_terminal_name
    '
    Me.chk_terminal_name.AutoSize = True
    Me.chk_terminal_name.Enabled = False
    Me.chk_terminal_name.Location = New System.Drawing.Point(15, 138)
    Me.chk_terminal_name.Name = "chk_terminal_name"
    Me.chk_terminal_name.Size = New System.Drawing.Size(193, 17)
    Me.chk_terminal_name.TabIndex = 7
    Me.chk_terminal_name.Text = "xShow winner terminal name"
    Me.chk_terminal_name.UseVisualStyleBackColor = True
    '
    'lbl_animation_interval_02
    '
    Me.lbl_animation_interval_02.AutoSize = True
    Me.lbl_animation_interval_02.Location = New System.Drawing.Point(11, 55)
    Me.lbl_animation_interval_02.Name = "lbl_animation_interval_02"
    Me.lbl_animation_interval_02.Size = New System.Drawing.Size(178, 13)
    Me.lbl_animation_interval_02.TabIndex = 2
    Me.lbl_animation_interval_02.Text = "xAward Animation Interval 02"
    Me.lbl_animation_interval_02.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'cmb_anim_interval_02
    '
    Me.cmb_anim_interval_02.AllowUnlistedValues = False
    Me.cmb_anim_interval_02.AutoCompleteMode = False
    Me.cmb_anim_interval_02.IsReadOnly = False
    Me.cmb_anim_interval_02.Location = New System.Drawing.Point(278, 46)
    Me.cmb_anim_interval_02.Name = "cmb_anim_interval_02"
    Me.cmb_anim_interval_02.SelectedIndex = -1
    Me.cmb_anim_interval_02.Size = New System.Drawing.Size(184, 24)
    Me.cmb_anim_interval_02.SufixText = "Sufix Text"
    Me.cmb_anim_interval_02.SufixTextVisible = True
    Me.cmb_anim_interval_02.TabIndex = 3
    Me.cmb_anim_interval_02.TextVisible = False
    Me.cmb_anim_interval_02.TextWidth = 0
    '
    'lbl_recent_interval
    '
    Me.lbl_recent_interval.AutoSize = True
    Me.lbl_recent_interval.Location = New System.Drawing.Point(12, 84)
    Me.lbl_recent_interval.Name = "lbl_recent_interval"
    Me.lbl_recent_interval.Size = New System.Drawing.Size(98, 13)
    Me.lbl_recent_interval.TabIndex = 4
    Me.lbl_recent_interval.Text = "xRecentInterval"
    Me.lbl_recent_interval.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'cmb_recent_interval
    '
    Me.cmb_recent_interval.AllowUnlistedValues = False
    Me.cmb_recent_interval.AutoCompleteMode = False
    Me.cmb_recent_interval.IsReadOnly = False
    Me.cmb_recent_interval.Location = New System.Drawing.Point(278, 75)
    Me.cmb_recent_interval.Name = "cmb_recent_interval"
    Me.cmb_recent_interval.SelectedIndex = -1
    Me.cmb_recent_interval.Size = New System.Drawing.Size(184, 24)
    Me.cmb_recent_interval.SufixText = "Sufix Text"
    Me.cmb_recent_interval.SufixTextVisible = True
    Me.cmb_recent_interval.TabIndex = 5
    Me.cmb_recent_interval.TextVisible = False
    Me.cmb_recent_interval.TextWidth = 0
    '
    'lbl_animation_interval_01
    '
    Me.lbl_animation_interval_01.AutoSize = True
    Me.lbl_animation_interval_01.Location = New System.Drawing.Point(11, 25)
    Me.lbl_animation_interval_01.Name = "lbl_animation_interval_01"
    Me.lbl_animation_interval_01.Size = New System.Drawing.Size(178, 13)
    Me.lbl_animation_interval_01.TabIndex = 0
    Me.lbl_animation_interval_01.Text = "xAward Animation Interval 01"
    Me.lbl_animation_interval_01.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'cmb_anim_interval_01
    '
    Me.cmb_anim_interval_01.AllowUnlistedValues = False
    Me.cmb_anim_interval_01.AutoCompleteMode = False
    Me.cmb_anim_interval_01.IsReadOnly = False
    Me.cmb_anim_interval_01.Location = New System.Drawing.Point(278, 16)
    Me.cmb_anim_interval_01.Name = "cmb_anim_interval_01"
    Me.cmb_anim_interval_01.SelectedIndex = -1
    Me.cmb_anim_interval_01.Size = New System.Drawing.Size(184, 24)
    Me.cmb_anim_interval_01.SufixText = "Sufix Text"
    Me.cmb_anim_interval_01.SufixTextVisible = True
    Me.cmb_anim_interval_01.TabIndex = 1
    Me.cmb_anim_interval_01.TextVisible = False
    Me.cmb_anim_interval_01.TextWidth = 0
    '
    'gb_promo_message
    '
    Me.gb_promo_message.Controls.Add(Me.lbl_message_3)
    Me.gb_promo_message.Controls.Add(Me.txt_message_3)
    Me.gb_promo_message.Controls.Add(Me.lbl_message_2)
    Me.gb_promo_message.Controls.Add(Me.txt_message_2)
    Me.gb_promo_message.Controls.Add(Me.lbl_message_1)
    Me.gb_promo_message.Controls.Add(Me.txt_message_1)
    Me.gb_promo_message.Location = New System.Drawing.Point(3, 605)
    Me.gb_promo_message.Name = "gb_promo_message"
    Me.gb_promo_message.Size = New System.Drawing.Size(886, 105)
    Me.gb_promo_message.TabIndex = 7
    Me.gb_promo_message.TabStop = False
    Me.gb_promo_message.Text = "xPromoMessage"
    '
    'lbl_message_3
    '
    Me.lbl_message_3.AutoSize = True
    Me.lbl_message_3.Location = New System.Drawing.Point(12, 75)
    Me.lbl_message_3.Name = "lbl_message_3"
    Me.lbl_message_3.Size = New System.Drawing.Size(70, 13)
    Me.lbl_message_3.TabIndex = 4
    Me.lbl_message_3.Text = "xMessage3"
    '
    'txt_message_3
    '
    Me.txt_message_3.Location = New System.Drawing.Point(88, 75)
    Me.txt_message_3.MaxLength = 255
    Me.txt_message_3.Name = "txt_message_3"
    Me.txt_message_3.Size = New System.Drawing.Size(773, 21)
    Me.txt_message_3.TabIndex = 5
    '
    'lbl_message_2
    '
    Me.lbl_message_2.AutoSize = True
    Me.lbl_message_2.Location = New System.Drawing.Point(12, 48)
    Me.lbl_message_2.Name = "lbl_message_2"
    Me.lbl_message_2.Size = New System.Drawing.Size(70, 13)
    Me.lbl_message_2.TabIndex = 2
    Me.lbl_message_2.Text = "xMessage2"
    '
    'txt_message_2
    '
    Me.txt_message_2.Location = New System.Drawing.Point(88, 48)
    Me.txt_message_2.MaxLength = 255
    Me.txt_message_2.Name = "txt_message_2"
    Me.txt_message_2.Size = New System.Drawing.Size(773, 21)
    Me.txt_message_2.TabIndex = 3
    '
    'lbl_message_1
    '
    Me.lbl_message_1.AutoSize = True
    Me.lbl_message_1.Location = New System.Drawing.Point(12, 23)
    Me.lbl_message_1.Name = "lbl_message_1"
    Me.lbl_message_1.Size = New System.Drawing.Size(70, 13)
    Me.lbl_message_1.TabIndex = 0
    Me.lbl_message_1.Text = "xMessage1"
    '
    'txt_message_1
    '
    Me.txt_message_1.Location = New System.Drawing.Point(88, 20)
    Me.txt_message_1.MaxLength = 255
    Me.txt_message_1.Name = "txt_message_1"
    Me.txt_message_1.Size = New System.Drawing.Size(773, 21)
    Me.txt_message_1.TabIndex = 1
    '
    'lbl_accumulated_total
    '
    Me.lbl_accumulated_total.IsReadOnly = True
    Me.lbl_accumulated_total.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_RIGTH
    Me.lbl_accumulated_total.LabelForeColor = System.Drawing.Color.Blue
    Me.lbl_accumulated_total.Location = New System.Drawing.Point(685, 215)
    Me.lbl_accumulated_total.Name = "lbl_accumulated_total"
    Me.lbl_accumulated_total.Size = New System.Drawing.Size(204, 24)
    Me.lbl_accumulated_total.SufixText = "Sufix Text"
    Me.lbl_accumulated_total.SufixTextVisible = True
    Me.lbl_accumulated_total.TabIndex = 6
    Me.lbl_accumulated_total.TextWidth = 110
    Me.lbl_accumulated_total.Value = "x35.000.00"
    '
    'lbl_to_compensate
    '
    Me.lbl_to_compensate.IsReadOnly = True
    Me.lbl_to_compensate.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_RIGTH
    Me.lbl_to_compensate.LabelForeColor = System.Drawing.Color.Blue
    Me.lbl_to_compensate.Location = New System.Drawing.Point(685, 239)
    Me.lbl_to_compensate.Name = "lbl_to_compensate"
    Me.lbl_to_compensate.Size = New System.Drawing.Size(204, 24)
    Me.lbl_to_compensate.SufixText = "Sufix Text"
    Me.lbl_to_compensate.SufixTextVisible = True
    Me.lbl_to_compensate.TabIndex = 7
    Me.lbl_to_compensate.TextWidth = 110
    Me.lbl_to_compensate.Value = "x134.00"
    '
    'tmr_refresh_jackpot
    '
    '
    'gb_payment_mode
    '
    Me.gb_payment_mode.Controls.Add(Me.opt_manual_payment)
    Me.gb_payment_mode.Controls.Add(Me.opt_automatic_payment)
    Me.gb_payment_mode.Location = New System.Drawing.Point(3, 66)
    Me.gb_payment_mode.Name = "gb_payment_mode"
    Me.gb_payment_mode.Size = New System.Drawing.Size(138, 69)
    Me.gb_payment_mode.TabIndex = 1
    Me.gb_payment_mode.TabStop = False
    Me.gb_payment_mode.Text = "xPaymentMode"
    '
    'opt_manual_payment
    '
    Me.opt_manual_payment.Location = New System.Drawing.Point(16, 20)
    Me.opt_manual_payment.Name = "opt_manual_payment"
    Me.opt_manual_payment.Size = New System.Drawing.Size(116, 16)
    Me.opt_manual_payment.TabIndex = 0
    Me.opt_manual_payment.Text = "xManual"
    '
    'opt_automatic_payment
    '
    Me.opt_automatic_payment.Location = New System.Drawing.Point(16, 42)
    Me.opt_automatic_payment.Name = "opt_automatic_payment"
    Me.opt_automatic_payment.Size = New System.Drawing.Size(116, 17)
    Me.opt_automatic_payment.TabIndex = 1
    Me.opt_automatic_payment.Text = "xAutomatic"
    '
    'dg_providers
    '
    Me.dg_providers.ForbiddenGroups = CType(resources.GetObject("dg_providers.ForbiddenGroups"), Microsoft.VisualBasic.Collection)
    Me.dg_providers.HeightOnExpanded = 300
    Me.dg_providers.Location = New System.Drawing.Point(488, 268)
    Me.dg_providers.Name = "dg_providers"
    Me.dg_providers.SelectedIndex = 0
    Me.dg_providers.SetDisplayMode = GUI_Controls.uc_terminals_group_filter.DisplayMode.NotCollapsedWithoutCollapseButton    
    Me.dg_providers.SetInitMode = GUI_Controls.uc_terminals_group_filter.InitMode.NormalMode
    Me.dg_providers.ShowGroupBoxLine = True
    Me.dg_providers.ShowGroups = Nothing
    Me.dg_providers.Size = New System.Drawing.Size(401, 331)
    Me.dg_providers.TabIndex = 6
    Me.dg_providers.ThreeStateCheckMode = True
    '
    'chk_allow_exceed_max
    '
    Me.chk_allow_exceed_max.AutoSize = True
    Me.chk_allow_exceed_max.Location = New System.Drawing.Point(12, 230)
    Me.chk_allow_exceed_max.Name = "chk_allow_exceed_max"
    Me.chk_allow_exceed_max.Size = New System.Drawing.Size(135, 17)
    Me.chk_allow_exceed_max.TabIndex = 41
    Me.chk_allow_exceed_max.Text = "xExceed maximum"
    Me.chk_allow_exceed_max.UseVisualStyleBackColor = True
    '
    'lbl_current_compensate_pct
    '
    Me.lbl_current_compensate_pct.IsReadOnly = True
    Me.lbl_current_compensate_pct.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_RIGTH
    Me.lbl_current_compensate_pct.LabelForeColor = System.Drawing.Color.Blue
    Me.lbl_current_compensate_pct.Location = New System.Drawing.Point(475, 238)
    Me.lbl_current_compensate_pct.Name = "lbl_current_compensate_pct"
    Me.lbl_current_compensate_pct.Size = New System.Drawing.Size(204, 24)
    Me.lbl_current_compensate_pct.SufixText = "Sufix Text"
    Me.lbl_current_compensate_pct.SufixTextVisible = True
    Me.lbl_current_compensate_pct.TabIndex = 42
    Me.lbl_current_compensate_pct.TextWidth = 110
    Me.lbl_current_compensate_pct.Value = "x93.82%"
    '
    'frm_site_jackpot_configuration
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(997, 726)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_site_jackpot_configuration"
    Me.Text = "frm_jackpot_configuration"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.gb_contribution.ResumeLayout(False)
    Me.gb_contribution.PerformLayout()
    Me.gb_jackpot_flag.ResumeLayout(False)
    Me.gb_wrk_days.ResumeLayout(False)
    Me.gb_wrk_days.PerformLayout()
    Me.gb_parameters.ResumeLayout(False)
    Me.gb_parameters.PerformLayout()
    Me.gb_promo_message.ResumeLayout(False)
    Me.gb_promo_message.PerformLayout()
    Me.gb_payment_mode.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_contribution As System.Windows.Forms.GroupBox
  Friend WithEvents ef_contribution As GUI_Controls.uc_entry_field
  Friend WithEvents gb_jackpot_flag As System.Windows.Forms.GroupBox
  Friend WithEvents opt_enabled As System.Windows.Forms.RadioButton
  Friend WithEvents opt_disabled As System.Windows.Forms.RadioButton
  Friend WithEvents gb_wrk_days As System.Windows.Forms.GroupBox
  Friend WithEvents chk_monday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_saturday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_friday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_thursday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_wednesday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_tuesday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_sunday As System.Windows.Forms.CheckBox
  Friend WithEvents dg_jackpot_list As GUI_Controls.uc_grid
  Friend WithEvents dtp_working_from As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_working_to As GUI_Controls.uc_date_picker
  Friend WithEvents gb_parameters As System.Windows.Forms.GroupBox
  Friend WithEvents cmb_recent_interval As GUI_Controls.uc_combo
  Friend WithEvents cmb_anim_interval_01 As GUI_Controls.uc_combo
  Friend WithEvents lbl_recent_interval As System.Windows.Forms.Label
  Friend WithEvents lbl_animation_interval_01 As System.Windows.Forms.Label
  Friend WithEvents gb_promo_message As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_message_3 As System.Windows.Forms.Label
  Friend WithEvents txt_message_3 As System.Windows.Forms.TextBox
  Friend WithEvents lbl_message_2 As System.Windows.Forms.Label
  Friend WithEvents txt_message_2 As System.Windows.Forms.TextBox
  Friend WithEvents lbl_message_1 As System.Windows.Forms.Label
  Friend WithEvents txt_message_1 As System.Windows.Forms.TextBox
  Friend WithEvents chk_only_redeemable As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_accumulated_total As GUI_Controls.uc_text_field
  Friend WithEvents lbl_to_compensate As GUI_Controls.uc_text_field
  Friend WithEvents tmr_refresh_jackpot As System.Windows.Forms.Timer
  Friend WithEvents ef_last_month_played As GUI_Controls.uc_entry_field
  Friend WithEvents chk_exclude_account_with_promotion As System.Windows.Forms.CheckBox
  Friend WithEvents btn_load_last_month_played As GUI_Controls.uc_button
  Friend WithEvents ef_min_occupation As GUI_Controls.uc_entry_field
  Friend WithEvents chk_exclude_anonymous_accounts As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_animation_interval_02 As System.Windows.Forms.Label
  Friend WithEvents cmb_anim_interval_02 As GUI_Controls.uc_combo
  Friend WithEvents chk_terminal_name As System.Windows.Forms.CheckBox
  Friend WithEvents chk_winner_name As System.Windows.Forms.CheckBox
  Friend WithEvents gb_payment_mode As System.Windows.Forms.GroupBox
  Friend WithEvents opt_manual_payment As System.Windows.Forms.RadioButton
  Friend WithEvents opt_automatic_payment As System.Windows.Forms.RadioButton
  Friend WithEvents dg_providers As GUI_Controls.uc_terminals_group_filter
  Friend WithEvents chk_allow_exceed_max As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_current_compensate_pct As GUI_Controls.uc_text_field
  Friend WithEvents lbl_percentaje As System.Windows.Forms.Label
  Friend WithEvents opt_fix_contribution As System.Windows.Forms.RadioButton
  Friend WithEvents opt_week_days As System.Windows.Forms.RadioButton
  Friend WithEvents lbl_saturday As System.Windows.Forms.Label
  Friend WithEvents lbl_friday As System.Windows.Forms.Label
  Friend WithEvents lbl_thursday As System.Windows.Forms.Label
  Friend WithEvents lbl_wednesday As System.Windows.Forms.Label
  Friend WithEvents lbl_tuesday As System.Windows.Forms.Label
  Friend WithEvents lbl_monday As System.Windows.Forms.Label
  Friend WithEvents lbl_sunday As System.Windows.Forms.Label
  Friend WithEvents ef_saturday As GUI_Controls.uc_entry_field
  Friend WithEvents ef_friday As GUI_Controls.uc_entry_field
  Friend WithEvents ef_thursday As GUI_Controls.uc_entry_field
  Friend WithEvents ef_wednesday As GUI_Controls.uc_entry_field
  Friend WithEvents ef_tuesday As GUI_Controls.uc_entry_field
  Friend WithEvents ef_monday As GUI_Controls.uc_entry_field
  Friend WithEvents ef_sunday As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_percentaje_sign As System.Windows.Forms.Label
  Friend WithEvents lbl_info_avg_contribution_week_days As System.Windows.Forms.Label
End Class
