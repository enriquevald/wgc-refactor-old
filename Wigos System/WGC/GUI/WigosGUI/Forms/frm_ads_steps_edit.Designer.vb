<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_ads_steps_edit
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_ads_steps_edit))
    Me.AppPromobox = New System.Windows.Forms.TabPage()
    Me.lbl_optimum_size = New System.Windows.Forms.Label()
    Me.img_image = New GUI_Controls.uc_image()
    Me.Uc_schedule1 = New GUI_Controls.uc_schedule()
    Me.AdsFields = New System.Windows.Forms.TabPage()
    Me.chk_enabled = New System.Windows.Forms.CheckBox()
    Me.lbl_description = New System.Windows.Forms.Label()
    Me.lbl_name = New System.Windows.Forms.Label()
    Me.chk_winup = New System.Windows.Forms.CheckBox()
    Me.chk_promobox = New System.Windows.Forms.CheckBox()
    Me.ef_description = New System.Windows.Forms.TextBox()
    Me.ef_name = New GUI_Controls.uc_entry_field()
    Me.TabContent = New System.Windows.Forms.TabControl()
    Me.AppFields = New System.Windows.Forms.TabPage()
    Me.txt_winup_titulo = New System.Windows.Forms.TextBox()
    Me.lbl_winup_titulo = New System.Windows.Forms.Label()
    Me.ef_list_description = New System.Windows.Forms.TextBox()
    Me.lbl_valid_to = New System.Windows.Forms.Label()
    Me.lbl_valid_from = New System.Windows.Forms.Label()
    Me.lbl_list_description = New System.Windows.Forms.Label()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.tab_control_HTML = New System.Windows.Forms.TabControl()
    Me.tab_html = New System.Windows.Forms.TabPage()
    Me.chk_add_style = New System.Windows.Forms.CheckBox()
    Me.txt_abstract = New System.Windows.Forms.RichTextBox()
    Me.btn_insert_tag = New System.Windows.Forms.Button()
    Me.cmbTag = New System.Windows.Forms.ComboBox()
    Me.tab_preview = New System.Windows.Forms.TabPage()
    Me.web_browser_html = New System.Windows.Forms.WebBrowser()
    Me.lbl_abstract = New System.Windows.Forms.Label()
    Me.lbl_section = New System.Windows.Forms.Label()
    Me.cmb_Section = New GUI_Controls.uc_combo()
    Me.tab_app_images = New System.Windows.Forms.TabControl()
    Me.TabBankCard = New System.Windows.Forms.TabPage()
    Me.Uc_imageList = New GUI_Controls.uc_image()
    Me.TabCheck = New System.Windows.Forms.TabPage()
    Me.Uc_imageDetail = New GUI_Controls.uc_image()
    Me.ef_order = New GUI_Controls.uc_entry_field()
    Me.panel_data.SuspendLayout()
    Me.AppPromobox.SuspendLayout()
    Me.AdsFields.SuspendLayout()
    Me.TabContent.SuspendLayout()
    Me.AppFields.SuspendLayout()
    Me.tab_control_HTML.SuspendLayout()
    Me.tab_html.SuspendLayout()
    Me.tab_preview.SuspendLayout()
    Me.tab_app_images.SuspendLayout()
    Me.TabBankCard.SuspendLayout()
    Me.TabCheck.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.TabContent)
    Me.panel_data.Location = New System.Drawing.Point(5, 5)
    Me.panel_data.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.panel_data.Size = New System.Drawing.Size(1171, 658)
    '
    'AppPromobox
    '
    Me.AppPromobox.Controls.Add(Me.lbl_optimum_size)
    Me.AppPromobox.Controls.Add(Me.img_image)
    Me.AppPromobox.Controls.Add(Me.Uc_schedule1)
    Me.AppPromobox.Location = New System.Drawing.Point(4, 26)
    Me.AppPromobox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.AppPromobox.Name = "AppPromobox"
    Me.AppPromobox.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.AppPromobox.Size = New System.Drawing.Size(1156, 613)
    Me.AppPromobox.TabIndex = 1
    Me.AppPromobox.Text = "App Promobox"
    Me.AppPromobox.UseVisualStyleBackColor = True
    '
    'lbl_optimum_size
    '
    Me.lbl_optimum_size.AutoSize = True
    Me.lbl_optimum_size.Location = New System.Drawing.Point(544, 20)
    Me.lbl_optimum_size.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
    Me.lbl_optimum_size.Name = "lbl_optimum_size"
    Me.lbl_optimum_size.Size = New System.Drawing.Size(110, 17)
    Me.lbl_optimum_size.TabIndex = 19
    Me.lbl_optimum_size.Text = "xOptimumSize"
    '
    'img_image
    '
    Me.img_image.AutoSize = True
    Me.img_image.ButtonDeleteEnabled = True
    Me.img_image.FreeResize = False
    Me.img_image.Image = Nothing
    Me.img_image.ImageInfoFont = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.img_image.ImageLayout = System.Windows.Forms.ImageLayout.Zoom
    Me.img_image.ImageName = Nothing
    Me.img_image.Location = New System.Drawing.Point(545, 43)
    Me.img_image.Margin = New System.Windows.Forms.Padding(0)
    Me.img_image.Name = "img_image"
    Me.img_image.Size = New System.Drawing.Size(605, 524)
    Me.img_image.TabIndex = 18
    Me.img_image.Transparent = False
    '
    'Uc_schedule1
    '
    Me.Uc_schedule1.CheckedDateTo = True
    Me.Uc_schedule1.DateFrom = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.Uc_schedule1.DateTo = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.Uc_schedule1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Uc_schedule1.Location = New System.Drawing.Point(8, 8)
    Me.Uc_schedule1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.Uc_schedule1.Name = "Uc_schedule1"
    Me.Uc_schedule1.SecondTime = False
    Me.Uc_schedule1.SecondTimeFrom = 0
    Me.Uc_schedule1.SecondTimeTo = 0
    Me.Uc_schedule1.ShowCheckBoxDateTo = False
    Me.Uc_schedule1.Size = New System.Drawing.Size(534, 313)
    Me.Uc_schedule1.TabIndex = 13
    Me.Uc_schedule1.TimeFrom = 0
    Me.Uc_schedule1.TimeTo = 0
    Me.Uc_schedule1.Weekday = 0
    '
    'AdsFields
    '
    Me.AdsFields.Controls.Add(Me.chk_enabled)
    Me.AdsFields.Controls.Add(Me.lbl_description)
    Me.AdsFields.Controls.Add(Me.lbl_name)
    Me.AdsFields.Controls.Add(Me.chk_winup)
    Me.AdsFields.Controls.Add(Me.chk_promobox)
    Me.AdsFields.Controls.Add(Me.ef_description)
    Me.AdsFields.Controls.Add(Me.ef_name)
    Me.AdsFields.Location = New System.Drawing.Point(4, 26)
    Me.AdsFields.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.AdsFields.Name = "AdsFields"
    Me.AdsFields.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.AdsFields.Size = New System.Drawing.Size(1156, 613)
    Me.AdsFields.TabIndex = 0
    Me.AdsFields.Text = "Ads Fields"
    Me.AdsFields.UseVisualStyleBackColor = True
    '
    'chk_enabled
    '
    Me.chk_enabled.AutoSize = True
    Me.chk_enabled.Location = New System.Drawing.Point(118, 73)
    Me.chk_enabled.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.chk_enabled.Name = "chk_enabled"
    Me.chk_enabled.Size = New System.Drawing.Size(107, 21)
    Me.chk_enabled.TabIndex = 4
    Me.chk_enabled.Text = "xHabilitado"
    Me.chk_enabled.UseVisualStyleBackColor = True
    '
    'lbl_description
    '
    Me.lbl_description.Location = New System.Drawing.Point(8, 97)
    Me.lbl_description.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
    Me.lbl_description.Name = "lbl_description"
    Me.lbl_description.Size = New System.Drawing.Size(108, 31)
    Me.lbl_description.TabIndex = 32
    Me.lbl_description.Text = "description"
    Me.lbl_description.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_name
    '
    Me.lbl_name.Location = New System.Drawing.Point(8, 34)
    Me.lbl_name.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
    Me.lbl_name.Name = "lbl_name"
    Me.lbl_name.Size = New System.Drawing.Size(108, 31)
    Me.lbl_name.TabIndex = 31
    Me.lbl_name.Text = "name"
    Me.lbl_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'chk_winup
    '
    Me.chk_winup.AutoSize = True
    Me.chk_winup.Location = New System.Drawing.Point(240, 330)
    Me.chk_winup.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.chk_winup.Name = "chk_winup"
    Me.chk_winup.Size = New System.Drawing.Size(76, 21)
    Me.chk_winup.TabIndex = 26
    Me.chk_winup.Text = "WinUp"
    Me.chk_winup.UseVisualStyleBackColor = True
    '
    'chk_promobox
    '
    Me.chk_promobox.AutoSize = True
    Me.chk_promobox.Location = New System.Drawing.Point(118, 330)
    Me.chk_promobox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.chk_promobox.Name = "chk_promobox"
    Me.chk_promobox.Size = New System.Drawing.Size(102, 21)
    Me.chk_promobox.TabIndex = 25
    Me.chk_promobox.Text = "Promobox"
    Me.chk_promobox.UseVisualStyleBackColor = True
    '
    'ef_description
    '
    Me.ef_description.Location = New System.Drawing.Point(118, 103)
    Me.ef_description.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.ef_description.Multiline = True
    Me.ef_description.Name = "ef_description"
    Me.ef_description.Size = New System.Drawing.Size(1008, 208)
    Me.ef_description.TabIndex = 13
    '
    'ef_name
    '
    Me.ef_name.DoubleValue = 0.0R
    Me.ef_name.IntegerValue = 0
    Me.ef_name.IsReadOnly = False
    Me.ef_name.Location = New System.Drawing.Point(13, 34)
    Me.ef_name.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.ef_name.Name = "ef_name"
    Me.ef_name.PlaceHolder = Nothing
    Me.ef_name.Size = New System.Drawing.Size(1115, 28)
    Me.ef_name.SufixText = "Sufix Text"
    Me.ef_name.TabIndex = 11
    Me.ef_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_name.TextValue = ""
    Me.ef_name.TextVisible = False
    Me.ef_name.TextWidth = 103
    Me.ef_name.Value = ""
    Me.ef_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'TabContent
    '
    Me.TabContent.Controls.Add(Me.AdsFields)
    Me.TabContent.Controls.Add(Me.AppPromobox)
    Me.TabContent.Controls.Add(Me.AppFields)
    Me.TabContent.Location = New System.Drawing.Point(4, 14)
    Me.TabContent.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.TabContent.Name = "TabContent"
    Me.TabContent.SelectedIndex = 0
    Me.TabContent.Size = New System.Drawing.Size(1164, 643)
    Me.TabContent.TabIndex = 17
    '
    'AppFields
    '
    Me.AppFields.Controls.Add(Me.txt_winup_titulo)
    Me.AppFields.Controls.Add(Me.lbl_winup_titulo)
    Me.AppFields.Controls.Add(Me.ef_list_description)
    Me.AppFields.Controls.Add(Me.lbl_valid_to)
    Me.AppFields.Controls.Add(Me.lbl_valid_from)
    Me.AppFields.Controls.Add(Me.lbl_list_description)
    Me.AppFields.Controls.Add(Me.dtp_to)
    Me.AppFields.Controls.Add(Me.dtp_from)
    Me.AppFields.Controls.Add(Me.tab_control_HTML)
    Me.AppFields.Controls.Add(Me.lbl_abstract)
    Me.AppFields.Controls.Add(Me.lbl_section)
    Me.AppFields.Controls.Add(Me.cmb_Section)
    Me.AppFields.Controls.Add(Me.tab_app_images)
    Me.AppFields.Controls.Add(Me.ef_order)
    Me.AppFields.Location = New System.Drawing.Point(4, 26)
    Me.AppFields.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.AppFields.Name = "AppFields"
    Me.AppFields.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.AppFields.Size = New System.Drawing.Size(1156, 613)
    Me.AppFields.TabIndex = 3
    Me.AppFields.Text = "App Fields"
    Me.AppFields.UseVisualStyleBackColor = True
    '
    'txt_winup_titulo
    '
    Me.txt_winup_titulo.Location = New System.Drawing.Point(154, 95)
    Me.txt_winup_titulo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.txt_winup_titulo.Multiline = True
    Me.txt_winup_titulo.Name = "txt_winup_titulo"
    Me.txt_winup_titulo.Size = New System.Drawing.Size(427, 25)
    Me.txt_winup_titulo.TabIndex = 35
    '
    'lbl_winup_titulo
    '
    Me.lbl_winup_titulo.Location = New System.Drawing.Point(9, 95)
    Me.lbl_winup_titulo.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
    Me.lbl_winup_titulo.Name = "lbl_winup_titulo"
    Me.lbl_winup_titulo.Size = New System.Drawing.Size(158, 26)
    Me.lbl_winup_titulo.TabIndex = 34
    Me.lbl_winup_titulo.Text = "T�tulo"
    Me.lbl_winup_titulo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'ef_list_description
    '
    Me.ef_list_description.Location = New System.Drawing.Point(154, 133)
    Me.ef_list_description.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.ef_list_description.Multiline = True
    Me.ef_list_description.Name = "ef_list_description"
    Me.ef_list_description.Size = New System.Drawing.Size(427, 99)
    Me.ef_list_description.TabIndex = 33
    '
    'lbl_valid_to
    '
    Me.lbl_valid_to.Location = New System.Drawing.Point(361, 54)
    Me.lbl_valid_to.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
    Me.lbl_valid_to.Name = "lbl_valid_to"
    Me.lbl_valid_to.Size = New System.Drawing.Size(28, 31)
    Me.lbl_valid_to.TabIndex = 32
    Me.lbl_valid_to.Text = "To"
    Me.lbl_valid_to.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_valid_from
    '
    Me.lbl_valid_from.Location = New System.Drawing.Point(9, 54)
    Me.lbl_valid_from.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
    Me.lbl_valid_from.Name = "lbl_valid_from"
    Me.lbl_valid_from.Size = New System.Drawing.Size(98, 31)
    Me.lbl_valid_from.TabIndex = 31
    Me.lbl_valid_from.Text = "Valid From"
    Me.lbl_valid_from.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_list_description
    '
    Me.lbl_list_description.Location = New System.Drawing.Point(9, 126)
    Me.lbl_list_description.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
    Me.lbl_list_description.Name = "lbl_list_description"
    Me.lbl_list_description.Size = New System.Drawing.Size(158, 31)
    Me.lbl_list_description.TabIndex = 30
    Me.lbl_list_description.Text = "Descripci�n listado"
    Me.lbl_list_description.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(375, 52)
    Me.dtp_to.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = False
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(208, 28)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 28
    Me.dtp_to.TextVisible = False
    Me.dtp_to.TextWidth = 26
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(48, 52)
    Me.dtp_from.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = False
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(285, 28)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 27
    Me.dtp_from.TextVisible = False
    Me.dtp_from.TextWidth = 103
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'tab_control_HTML
    '
    Me.tab_control_HTML.Controls.Add(Me.tab_html)
    Me.tab_control_HTML.Controls.Add(Me.tab_preview)
    Me.tab_control_HTML.Location = New System.Drawing.Point(8, 280)
    Me.tab_control_HTML.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.tab_control_HTML.Name = "tab_control_HTML"
    Me.tab_control_HTML.SelectedIndex = 0
    Me.tab_control_HTML.Size = New System.Drawing.Size(576, 330)
    Me.tab_control_HTML.TabIndex = 25
    '
    'tab_html
    '
    Me.tab_html.Controls.Add(Me.chk_add_style)
    Me.tab_html.Controls.Add(Me.txt_abstract)
    Me.tab_html.Controls.Add(Me.btn_insert_tag)
    Me.tab_html.Controls.Add(Me.cmbTag)
    Me.tab_html.Location = New System.Drawing.Point(4, 26)
    Me.tab_html.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.tab_html.Name = "tab_html"
    Me.tab_html.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.tab_html.Size = New System.Drawing.Size(568, 300)
    Me.tab_html.TabIndex = 0
    Me.tab_html.Text = "HTML"
    Me.tab_html.UseVisualStyleBackColor = True
    '
    'chk_add_style
    '
    Me.chk_add_style.AutoSize = True
    Me.chk_add_style.Location = New System.Drawing.Point(328, 18)
    Me.chk_add_style.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.chk_add_style.Name = "chk_add_style"
    Me.chk_add_style.Size = New System.Drawing.Size(98, 21)
    Me.chk_add_style.TabIndex = 24
    Me.chk_add_style.Text = "Add Style"
    Me.chk_add_style.UseVisualStyleBackColor = True
    '
    'txt_abstract
    '
    Me.txt_abstract.Location = New System.Drawing.Point(8, 61)
    Me.txt_abstract.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.txt_abstract.Name = "txt_abstract"
    Me.txt_abstract.Size = New System.Drawing.Size(549, 225)
    Me.txt_abstract.TabIndex = 19
    Me.txt_abstract.Text = ""
    '
    'btn_insert_tag
    '
    Me.btn_insert_tag.Location = New System.Drawing.Point(474, 13)
    Me.btn_insert_tag.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.btn_insert_tag.Name = "btn_insert_tag"
    Me.btn_insert_tag.Size = New System.Drawing.Size(84, 30)
    Me.btn_insert_tag.TabIndex = 23
    Me.btn_insert_tag.Text = "insert"
    Me.btn_insert_tag.UseVisualStyleBackColor = True
    '
    'cmbTag
    '
    Me.cmbTag.FormattingEnabled = True
    Me.cmbTag.Location = New System.Drawing.Point(8, 16)
    Me.cmbTag.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.cmbTag.Name = "cmbTag"
    Me.cmbTag.Size = New System.Drawing.Size(311, 25)
    Me.cmbTag.TabIndex = 21
    '
    'tab_preview
    '
    Me.tab_preview.Controls.Add(Me.web_browser_html)
    Me.tab_preview.Location = New System.Drawing.Point(4, 26)
    Me.tab_preview.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.tab_preview.Name = "tab_preview"
    Me.tab_preview.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.tab_preview.Size = New System.Drawing.Size(568, 300)
    Me.tab_preview.TabIndex = 2
    Me.tab_preview.Text = "Preview"
    Me.tab_preview.UseVisualStyleBackColor = True
    '
    'web_browser_html
    '
    Me.web_browser_html.Location = New System.Drawing.Point(14, 13)
    Me.web_browser_html.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.web_browser_html.MinimumSize = New System.Drawing.Size(26, 26)
    Me.web_browser_html.Name = "web_browser_html"
    Me.web_browser_html.Size = New System.Drawing.Size(442, 319)
    Me.web_browser_html.TabIndex = 22
    '
    'lbl_abstract
    '
    Me.lbl_abstract.Location = New System.Drawing.Point(9, 245)
    Me.lbl_abstract.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
    Me.lbl_abstract.Name = "lbl_abstract"
    Me.lbl_abstract.Size = New System.Drawing.Size(145, 31)
    Me.lbl_abstract.TabIndex = 16
    Me.lbl_abstract.Text = "Description Detail"
    Me.lbl_abstract.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_section
    '
    Me.lbl_section.Location = New System.Drawing.Point(9, 13)
    Me.lbl_section.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
    Me.lbl_section.Name = "lbl_section"
    Me.lbl_section.Size = New System.Drawing.Size(116, 31)
    Me.lbl_section.TabIndex = 15
    Me.lbl_section.Text = "Sections"
    Me.lbl_section.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'cmb_Section
    '
    Me.cmb_Section.AllowUnlistedValues = False
    Me.cmb_Section.AutoCompleteMode = False
    Me.cmb_Section.IsReadOnly = False
    Me.cmb_Section.Location = New System.Drawing.Point(152, 12)
    Me.cmb_Section.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.cmb_Section.Name = "cmb_Section"
    Me.cmb_Section.SelectedIndex = -1
    Me.cmb_Section.Size = New System.Drawing.Size(275, 28)
    Me.cmb_Section.SufixText = "Sufix Text"
    Me.cmb_Section.SufixTextVisible = True
    Me.cmb_Section.TabIndex = 6
    Me.cmb_Section.TextCombo = Nothing
    Me.cmb_Section.TextVisible = False
    Me.cmb_Section.TextWidth = 0
    '
    'tab_app_images
    '
    Me.tab_app_images.Controls.Add(Me.TabBankCard)
    Me.tab_app_images.Controls.Add(Me.TabCheck)
    Me.tab_app_images.Location = New System.Drawing.Point(600, 8)
    Me.tab_app_images.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.tab_app_images.Name = "tab_app_images"
    Me.tab_app_images.SelectedIndex = 0
    Me.tab_app_images.Size = New System.Drawing.Size(525, 556)
    Me.tab_app_images.TabIndex = 12
    '
    'TabBankCard
    '
    Me.TabBankCard.Controls.Add(Me.Uc_imageList)
    Me.TabBankCard.Location = New System.Drawing.Point(4, 26)
    Me.TabBankCard.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.TabBankCard.Name = "TabBankCard"
    Me.TabBankCard.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.TabBankCard.Size = New System.Drawing.Size(517, 526)
    Me.TabBankCard.TabIndex = 0
    Me.TabBankCard.Text = "Image List"
    Me.TabBankCard.UseVisualStyleBackColor = True
    '
    'Uc_imageList
    '
    Me.Uc_imageList.AutoSize = True
    Me.Uc_imageList.ButtonDeleteEnabled = True
    Me.Uc_imageList.FreeResize = False
    Me.Uc_imageList.Image = Nothing
    Me.Uc_imageList.ImageInfoFont = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Uc_imageList.ImageLayout = System.Windows.Forms.ImageLayout.Zoom
    Me.Uc_imageList.ImageName = Nothing
    Me.Uc_imageList.Location = New System.Drawing.Point(6, 4)
    Me.Uc_imageList.Margin = New System.Windows.Forms.Padding(0)
    Me.Uc_imageList.Name = "Uc_imageList"
    Me.Uc_imageList.Size = New System.Drawing.Size(605, 556)
    Me.Uc_imageList.TabIndex = 11
    Me.Uc_imageList.Transparent = False
    '
    'TabCheck
    '
    Me.TabCheck.Controls.Add(Me.Uc_imageDetail)
    Me.TabCheck.Location = New System.Drawing.Point(4, 26)
    Me.TabCheck.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.TabCheck.Name = "TabCheck"
    Me.TabCheck.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.TabCheck.Size = New System.Drawing.Size(517, 526)
    Me.TabCheck.TabIndex = 1
    Me.TabCheck.Text = "Image Detail"
    Me.TabCheck.UseVisualStyleBackColor = True
    '
    'Uc_imageDetail
    '
    Me.Uc_imageDetail.AutoSize = True
    Me.Uc_imageDetail.ButtonDeleteEnabled = True
    Me.Uc_imageDetail.FreeResize = False
    Me.Uc_imageDetail.Image = Nothing
    Me.Uc_imageDetail.ImageInfoFont = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Uc_imageDetail.ImageLayout = System.Windows.Forms.ImageLayout.Zoom
    Me.Uc_imageDetail.ImageName = Nothing
    Me.Uc_imageDetail.Location = New System.Drawing.Point(6, 4)
    Me.Uc_imageDetail.Margin = New System.Windows.Forms.Padding(0)
    Me.Uc_imageDetail.Name = "Uc_imageDetail"
    Me.Uc_imageDetail.Size = New System.Drawing.Size(605, 556)
    Me.Uc_imageDetail.TabIndex = 13
    Me.Uc_imageDetail.Transparent = False
    '
    'ef_order
    '
    Me.ef_order.DoubleValue = 0.0R
    Me.ef_order.IntegerValue = 0
    Me.ef_order.IsReadOnly = False
    Me.ef_order.Location = New System.Drawing.Point(437, 12)
    Me.ef_order.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.ef_order.Name = "ef_order"
    Me.ef_order.PlaceHolder = Nothing
    Me.ef_order.Size = New System.Drawing.Size(147, 28)
    Me.ef_order.SufixText = "Sufix Text"
    Me.ef_order.SufixTextVisible = True
    Me.ef_order.TabIndex = 8
    Me.ef_order.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_order.TextValue = ""
    Me.ef_order.TextVisible = False
    Me.ef_order.TextWidth = 103
    Me.ef_order.Value = ""
    Me.ef_order.ValueForeColor = System.Drawing.Color.Blue
    '
    'frm_ads_steps_edit
    '
    'Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 17.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1302, 670)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.KeyPreview = False
    Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.Name = "frm_ads_steps_edit"
    Me.Padding = New System.Windows.Forms.Padding(5, 5, 5, 5)
    Me.Text = "frm_ads_steps_edit"
    Me.panel_data.ResumeLayout(False)
    Me.AppPromobox.ResumeLayout(False)
    Me.AppPromobox.PerformLayout()
    Me.AdsFields.ResumeLayout(False)
    Me.AdsFields.PerformLayout()
    Me.TabContent.ResumeLayout(False)
    Me.AppFields.ResumeLayout(False)
    Me.AppFields.PerformLayout()
    Me.tab_control_HTML.ResumeLayout(False)
    Me.tab_html.ResumeLayout(False)
    Me.tab_html.PerformLayout()
    Me.tab_preview.ResumeLayout(False)
    Me.tab_app_images.ResumeLayout(False)
    Me.TabBankCard.ResumeLayout(False)
    Me.TabBankCard.PerformLayout()
    Me.TabCheck.ResumeLayout(False)
    Me.TabCheck.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents TabContent As System.Windows.Forms.TabControl
  Friend WithEvents AdsFields As System.Windows.Forms.TabPage
  Friend WithEvents ef_description As System.Windows.Forms.TextBox
  Friend WithEvents ef_name As GUI_Controls.uc_entry_field
  Friend WithEvents AppPromobox As System.Windows.Forms.TabPage
  Friend WithEvents AppFields As System.Windows.Forms.TabPage
  Friend WithEvents tab_control_HTML As System.Windows.Forms.TabControl
  Friend WithEvents tab_html As System.Windows.Forms.TabPage
  Friend WithEvents chk_add_style As System.Windows.Forms.CheckBox
  Friend WithEvents txt_abstract As System.Windows.Forms.RichTextBox
  Friend WithEvents btn_insert_tag As System.Windows.Forms.Button
  Friend WithEvents cmbTag As System.Windows.Forms.ComboBox
  Friend WithEvents tab_preview As System.Windows.Forms.TabPage
  Friend WithEvents web_browser_html As System.Windows.Forms.WebBrowser
  Friend WithEvents lbl_abstract As System.Windows.Forms.Label
  Friend WithEvents lbl_section As System.Windows.Forms.Label
  Friend WithEvents cmb_Section As GUI_Controls.uc_combo
  Friend WithEvents tab_app_images As System.Windows.Forms.TabControl
  Friend WithEvents TabBankCard As System.Windows.Forms.TabPage
  Friend WithEvents Uc_imageList As GUI_Controls.uc_image
  Friend WithEvents TabCheck As System.Windows.Forms.TabPage
  Friend WithEvents Uc_imageDetail As GUI_Controls.uc_image
  Friend WithEvents ef_order As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_schedule1 As GUI_Controls.uc_schedule
  Friend WithEvents chk_winup As System.Windows.Forms.CheckBox
  Friend WithEvents chk_promobox As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_optimum_size As System.Windows.Forms.Label
  Friend WithEvents img_image As GUI_Controls.uc_image
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents lbl_list_description As System.Windows.Forms.Label
  Friend WithEvents lbl_valid_to As System.Windows.Forms.Label
  Friend WithEvents lbl_valid_from As System.Windows.Forms.Label
  Friend WithEvents lbl_name As System.Windows.Forms.Label
  Friend WithEvents lbl_description As System.Windows.Forms.Label
  Friend WithEvents ef_list_description As System.Windows.Forms.TextBox
  Friend WithEvents txt_winup_titulo As System.Windows.Forms.TextBox
  Friend WithEvents lbl_winup_titulo As System.Windows.Forms.Label
  Friend WithEvents chk_enabled As System.Windows.Forms.CheckBox
End Class
