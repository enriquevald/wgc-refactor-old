<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_service_charge_edit
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing AndAlso components IsNot Nothing Then
      components.Dispose()
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_service_charge_edit))
    Me.chk_enable_service_charge = New System.Windows.Forms.CheckBox()
    Me.ef_percent_over_devolution_lower_amount = New GUI_Controls.uc_entry_field()
    Me.ef_percent_over_devolution_greater_amount = New GUI_Controls.uc_entry_field()
    Me.ef_percent_over_award_amount = New GUI_Controls.uc_entry_field()
    Me.ef_concept = New GUI_Controls.uc_entry_field()
    Me.gb_amount_lower = New System.Windows.Forms.GroupBox()
    Me.gb_amount_greater = New System.Windows.Forms.GroupBox()
    Me.cmb_awardcalculation = New GUI_Controls.uc_combo()
    Me.lbl_note = New System.Windows.Forms.Label()
    Me.cmb_on_company = New GUI_Controls.uc_combo()
    Me.panel_data.SuspendLayout()
    Me.gb_amount_lower.SuspendLayout()
    Me.gb_amount_greater.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.cmb_on_company)
    Me.panel_data.Controls.Add(Me.lbl_note)
    Me.panel_data.Controls.Add(Me.cmb_awardcalculation)
    Me.panel_data.Controls.Add(Me.gb_amount_greater)
    Me.panel_data.Controls.Add(Me.gb_amount_lower)
    Me.panel_data.Controls.Add(Me.ef_concept)
    Me.panel_data.Controls.Add(Me.chk_enable_service_charge)
    Me.panel_data.Size = New System.Drawing.Size(333, 390)
    '
    'chk_enable_service_charge
    '
    Me.chk_enable_service_charge.AutoSize = True
    Me.chk_enable_service_charge.Location = New System.Drawing.Point(27, 29)
    Me.chk_enable_service_charge.Name = "chk_enable_service_charge"
    Me.chk_enable_service_charge.Size = New System.Drawing.Size(156, 17)
    Me.chk_enable_service_charge.TabIndex = 0
    Me.chk_enable_service_charge.Text = "xEnableServiceCharge"
    Me.chk_enable_service_charge.UseVisualStyleBackColor = True
    '
    'ef_percent_over_devolution_lower_amount
    '
    Me.ef_percent_over_devolution_lower_amount.DoubleValue = 0.0R
    Me.ef_percent_over_devolution_lower_amount.IntegerValue = 0
    Me.ef_percent_over_devolution_lower_amount.IsReadOnly = False
    Me.ef_percent_over_devolution_lower_amount.Location = New System.Drawing.Point(50, 23)
    Me.ef_percent_over_devolution_lower_amount.Name = "ef_percent_over_devolution_lower_amount"
    Me.ef_percent_over_devolution_lower_amount.PlaceHolder = Nothing
    Me.ef_percent_over_devolution_lower_amount.Size = New System.Drawing.Size(200, 24)
    Me.ef_percent_over_devolution_lower_amount.SufixText = "Sufix Text"
    Me.ef_percent_over_devolution_lower_amount.SufixTextVisible = True
    Me.ef_percent_over_devolution_lower_amount.SufixTextWidth = 120
    Me.ef_percent_over_devolution_lower_amount.TabIndex = 0
    Me.ef_percent_over_devolution_lower_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_percent_over_devolution_lower_amount.TextValue = ""
    Me.ef_percent_over_devolution_lower_amount.TextWidth = 0
    Me.ef_percent_over_devolution_lower_amount.Value = ""
    Me.ef_percent_over_devolution_lower_amount.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_percent_over_devolution_greater_amount
    '
    Me.ef_percent_over_devolution_greater_amount.DoubleValue = 0.0R
    Me.ef_percent_over_devolution_greater_amount.IntegerValue = 0
    Me.ef_percent_over_devolution_greater_amount.IsReadOnly = False
    Me.ef_percent_over_devolution_greater_amount.Location = New System.Drawing.Point(50, 25)
    Me.ef_percent_over_devolution_greater_amount.Name = "ef_percent_over_devolution_greater_amount"
    Me.ef_percent_over_devolution_greater_amount.PlaceHolder = Nothing
    Me.ef_percent_over_devolution_greater_amount.Size = New System.Drawing.Size(200, 24)
    Me.ef_percent_over_devolution_greater_amount.SufixText = "Sufix Text"
    Me.ef_percent_over_devolution_greater_amount.SufixTextVisible = True
    Me.ef_percent_over_devolution_greater_amount.SufixTextWidth = 120
    Me.ef_percent_over_devolution_greater_amount.TabIndex = 0
    Me.ef_percent_over_devolution_greater_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_percent_over_devolution_greater_amount.TextValue = ""
    Me.ef_percent_over_devolution_greater_amount.TextWidth = 0
    Me.ef_percent_over_devolution_greater_amount.Value = ""
    Me.ef_percent_over_devolution_greater_amount.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_percent_over_award_amount
    '
    Me.ef_percent_over_award_amount.DoubleValue = 0.0R
    Me.ef_percent_over_award_amount.IntegerValue = 0
    Me.ef_percent_over_award_amount.IsReadOnly = False
    Me.ef_percent_over_award_amount.Location = New System.Drawing.Point(50, 59)
    Me.ef_percent_over_award_amount.Name = "ef_percent_over_award_amount"
    Me.ef_percent_over_award_amount.PlaceHolder = Nothing
    Me.ef_percent_over_award_amount.Size = New System.Drawing.Size(200, 24)
    Me.ef_percent_over_award_amount.SufixText = "Sufix Text"
    Me.ef_percent_over_award_amount.SufixTextVisible = True
    Me.ef_percent_over_award_amount.SufixTextWidth = 120
    Me.ef_percent_over_award_amount.TabIndex = 1
    Me.ef_percent_over_award_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_percent_over_award_amount.TextValue = ""
    Me.ef_percent_over_award_amount.TextWidth = 0
    Me.ef_percent_over_award_amount.Value = ""
    Me.ef_percent_over_award_amount.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_concept
    '
    Me.ef_concept.DoubleValue = 0.0R
    Me.ef_concept.IntegerValue = 0
    Me.ef_concept.IsReadOnly = False
    Me.ef_concept.Location = New System.Drawing.Point(26, 56)
    Me.ef_concept.Name = "ef_concept"
    Me.ef_concept.PlaceHolder = Nothing
    Me.ef_concept.RightToLeft = System.Windows.Forms.RightToLeft.No
    Me.ef_concept.Size = New System.Drawing.Size(285, 24)
    Me.ef_concept.SufixText = "Sufix Text"
    Me.ef_concept.SufixTextVisible = True
    Me.ef_concept.TabIndex = 1
    Me.ef_concept.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_concept.TextValue = ""
    Me.ef_concept.TextWidth = 75
    Me.ef_concept.Value = ""
    Me.ef_concept.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_amount_lower
    '
    Me.gb_amount_lower.Controls.Add(Me.ef_percent_over_devolution_lower_amount)
    Me.gb_amount_lower.Location = New System.Drawing.Point(26, 207)
    Me.gb_amount_lower.Name = "gb_amount_lower"
    Me.gb_amount_lower.Size = New System.Drawing.Size(284, 64)
    Me.gb_amount_lower.TabIndex = 3
    Me.gb_amount_lower.TabStop = False
    Me.gb_amount_lower.Text = "xRetiro sin premio"
    '
    'gb_amount_greater
    '
    Me.gb_amount_greater.Controls.Add(Me.ef_percent_over_devolution_greater_amount)
    Me.gb_amount_greater.Controls.Add(Me.ef_percent_over_award_amount)
    Me.gb_amount_greater.Location = New System.Drawing.Point(27, 283)
    Me.gb_amount_greater.Name = "gb_amount_greater"
    Me.gb_amount_greater.Size = New System.Drawing.Size(284, 100)
    Me.gb_amount_greater.TabIndex = 4
    Me.gb_amount_greater.TabStop = False
    Me.gb_amount_greater.Text = "xRetiro con premio"
    '
    'cmb_awardcalculation
    '
    Me.cmb_awardcalculation.AllowUnlistedValues = False
    Me.cmb_awardcalculation.AutoCompleteMode = False
    Me.cmb_awardcalculation.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.cmb_awardcalculation.IsReadOnly = False
    Me.cmb_awardcalculation.Location = New System.Drawing.Point(28, 120)
    Me.cmb_awardcalculation.Name = "cmb_awardcalculation"
    Me.cmb_awardcalculation.SelectedIndex = -1
    Me.cmb_awardcalculation.Size = New System.Drawing.Size(261, 24)
    Me.cmb_awardcalculation.SufixText = "Sufix Text"
    Me.cmb_awardcalculation.SufixTextVisible = True
    Me.cmb_awardcalculation.TabIndex = 2
    Me.cmb_awardcalculation.TextCombo = Nothing
    Me.cmb_awardcalculation.TextWidth = 125
    '
    'lbl_note
    '
    Me.lbl_note.BackColor = System.Drawing.SystemColors.Control
    Me.lbl_note.Font = New System.Drawing.Font("Courier New", 8.25!)
    Me.lbl_note.ForeColor = System.Drawing.Color.Navy
    Me.lbl_note.Location = New System.Drawing.Point(24, 153)
    Me.lbl_note.Name = "lbl_note"
    Me.lbl_note.Size = New System.Drawing.Size(284, 42)
    Me.lbl_note.TabIndex = 5
    Me.lbl_note.Text = "xNote"
    '
    'cmb_on_company
    '
    Me.cmb_on_company.AllowUnlistedValues = False
    Me.cmb_on_company.AutoCompleteMode = False
    Me.cmb_on_company.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.cmb_on_company.IsReadOnly = False
    Me.cmb_on_company.Location = New System.Drawing.Point(28, 90)
    Me.cmb_on_company.Name = "cmb_on_company"
    Me.cmb_on_company.SelectedIndex = -1
    Me.cmb_on_company.Size = New System.Drawing.Size(261, 24)
    Me.cmb_on_company.SufixText = "Sufix Text"
    Me.cmb_on_company.SufixTextVisible = True
    Me.cmb_on_company.TabIndex = 6
    Me.cmb_on_company.TextCombo = Nothing
    Me.cmb_on_company.TextWidth = 125
    '
    'frm_service_charge_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(435, 399)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_service_charge_edit"
    Me.Text = "frm_cash_service_edit"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.gb_amount_lower.ResumeLayout(False)
    Me.gb_amount_greater.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents chk_enable_service_charge As System.Windows.Forms.CheckBox
  Friend WithEvents ef_percent_over_devolution_lower_amount As GUI_Controls.uc_entry_field
  Friend WithEvents ef_percent_over_devolution_greater_amount As GUI_Controls.uc_entry_field
  Friend WithEvents ef_percent_over_award_amount As GUI_Controls.uc_entry_field
  Friend WithEvents ef_concept As GUI_Controls.uc_entry_field
  Friend WithEvents gb_amount_lower As System.Windows.Forms.GroupBox
  Friend WithEvents gb_amount_greater As System.Windows.Forms.GroupBox
  Friend WithEvents cmb_awardcalculation As GUI_Controls.uc_combo
  Friend WithEvents lbl_note As System.Windows.Forms.Label
  Friend WithEvents cmb_on_company As GUI_Controls.uc_combo
End Class
