'-------------------------------------------------------------------
' Copyright � 2007-2012 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_cash_report
' DESCRIPTION:   Comparison of different cash meters by day
' AUTHOR:        Jos� Mart�nez
' CREATION DATE: 05-JUN-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 05-JUN-2012  JML    Initial version.
' 12-NOV-2013  JAB    Modified "GUI_GetSqlCommand" function. Accounts with ac_type equal to 'ACCOUNT_VIRTUAL_CASHIER' or 'ACCOUNT_VIRTUAL_TERMINAL' will not show.
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports WSI.Common

Public Class frm_recommendation_report
  Inherits frm_base_sel

#Region " Constants "

  Private Const SQL_COLUMN_OLD_CLIENT_ID As Integer = 0
  Private Const SQL_COLUMN_OLD_CLIENT_NAME As Integer = 1
  Private Const SQL_COLUMN_OLD_CLIENT_LEVEL As Integer = 2
  Private Const SQL_COLUMN_NEW_CLIENT_CREATED As Integer = 3
  Private Const SQL_COLUMN_NEW_CLIENT_ID As Integer = 4
  Private Const SQL_COLUMN_NEW_CLIENT_NAME As Integer = 5
  Private Const SQL_COLUMN_NEW_CLIENT_LEVEL As Integer = 6

  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_OLD_CLIENT_ID As Integer = 1
  Private Const GRID_COLUMN_OLD_CLIENT_NAME As Integer = 2
  Private Const GRID_COLUMN_OLD_CLIENT_LEVEL As Integer = 3
  Private Const GRID_COLUMN_NEW_CLIENT_CREATED As Integer = 4
  Private Const GRID_COLUMN_NEW_CLIENT_ID As Integer = 5
  Private Const GRID_COLUMN_NEW_CLIENT_NAME As Integer = 6
  Private Const GRID_COLUMN_NEW_CLIENT_LEVEL As Integer = 7

  Private Const GRID_COLUMNS As Integer = 8
  Private Const GRID_HEADER_ROWS As Integer = 2

  Private Const COUNTER_RECOMMENDER As Integer = 1

#End Region ' Constants

#Region " Enums "

#End Region ' Enums 

#Region " Structures "

  Public Structure TYPE_TOTAL_COUNTERS
    Dim client_id As Integer
    Dim client_name As String
    Dim total_recommended As Integer
  End Structure

#End Region ' Structures

#Region " Member "

  ' For report filters 
  Private m_date_from As String
  Private m_date_to As String

  Private m_date_filter As Date

  ' For sub-totals & totals
  Private m_allow_totals As Boolean
  Private m_total_client As TYPE_TOTAL_COUNTERS
  Private m_total As TYPE_TOTAL_COUNTERS

#End Region ' Member

#Region " Overrides "

  ' PURPOSE: Set the internal Form Id
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_RECOMMENDATION_REPORT

    'Call Base Form proc
    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_STATISTICS.GetString(493)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(1)

    ' Date time filter
    Me.uc_dsl.Init(GLB_NLS_GUI_STATISTICS.Id(498), True)

    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  '
  Protected Overrides Function GUI_FilterCheck() As Boolean

    Dim to_date As DateTime

    ' Dates selection 
    If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate > Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    Else
      to_date = WSI.Common.WGDB.Now
      to_date = New DateTime(to_date.Year, to_date.Month, to_date.Day, Me.uc_dsl.ClosingTime, 0, 0)
      If Me.uc_dsl.FromDate > to_date Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Get the defined Query Type
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_QUERY_TYPE
  '
  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE

    Return ENUM_QUERY_TYPE.QUERY_COMMAND

  End Function ' GUI_GetQueryType

  ' PURPOSE : Checks out the DB row before adding it to the grid
  '              and process counters (& totals rows)
  '
  '  PARAMS :
  '     - INPUT :
  '           - DataRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the data row can be added) or False (the data row can not be added)
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As CLASS_DB_ROW) As Boolean

    Call ProcessCounters(DbRow)

    Return True

  End Function ' GUI_CheckOutRowBeforeAdd

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()

    m_total_client.client_id = 0
    m_total_client.client_name = ""
    m_allow_totals = False

    ResetTotalCounters(m_total_client)
    ResetTotalCounters(m_total)
    Me.Grid.Counter(COUNTER_RECOMMENDER).Value = 0

  End Sub ' GUI_BeforeFirstRow

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _old_client_level As String
    Dim _new_client_level As String

    m_total_client.client_id = DbRow.Value(SQL_COLUMN_OLD_CLIENT_ID)
    m_total_client.client_name = DbRow.Value(SQL_COLUMN_OLD_CLIENT_NAME).ToString()

    _old_client_level = "00" + DbRow.Value(SQL_COLUMN_OLD_CLIENT_LEVEL).ToString()
    _old_client_level = _old_client_level.Substring(_old_client_level.Length - 2, 2)
    _old_client_level = GeneralParam.Value("PlayerTracking", "Level" + _old_client_level + ".Name")

    _new_client_level = "00" + DbRow.Value(SQL_COLUMN_NEW_CLIENT_LEVEL).ToString()
    _new_client_level = _new_client_level.Substring(_new_client_level.Length - 2, 2)
    _new_client_level = GeneralParam.Value("PlayerTracking", "Level" + _new_client_level + ".Name")

    With Me.Grid
      .Cell(RowIndex, GRID_COLUMN_OLD_CLIENT_ID).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_OLD_CLIENT_ID), 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
      .Cell(RowIndex, GRID_COLUMN_OLD_CLIENT_NAME).Value = DbRow.Value(SQL_COLUMN_OLD_CLIENT_NAME).ToString()
      .Cell(RowIndex, GRID_COLUMN_OLD_CLIENT_LEVEL).Value = _old_client_level
      .Cell(RowIndex, GRID_COLUMN_NEW_CLIENT_CREATED).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_NEW_CLIENT_CREATED), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      .Cell(RowIndex, GRID_COLUMN_NEW_CLIENT_ID).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_NEW_CLIENT_ID), 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
      .Cell(RowIndex, GRID_COLUMN_NEW_CLIENT_NAME).Value = DbRow.Value(SQL_COLUMN_NEW_CLIENT_NAME).ToString()
      .Cell(RowIndex, GRID_COLUMN_NEW_CLIENT_LEVEL).Value = _new_client_level
    End With

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Perform final processing for the grid data (totalisator rows)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()

    'Last Client
    If m_total_client.total_recommended > 0 Then
      Call ProcessSubTotal()
    End If

    'Total Clients
    If m_total.total_recommended > 0 Then
      Call ProcessTotal()
    End If

  End Sub ' GUI_AfterLastRow

  ' PURPOSE: Build an SQL Command query 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL Command query ready to send to the database
  Protected Overrides Function GUI_GetSqlCommand() As SqlCommand

    Dim _sql_command As SqlCommand

    Dim _str_bld As System.Text.StringBuilder
    _str_bld = New System.Text.StringBuilder()

    _str_bld.AppendLine("SELECT   OLD_CLIENT.AC_ACCOUNT_ID    AS OLD_CLIENT_ID              ")
    _str_bld.AppendLine("       , OLD_CLIENT.AC_HOLDER_NAME   AS OLD_CLIENT_NAME            ")
    _str_bld.AppendLine("       , OLD_CLIENT.AC_HOLDER_LEVEL  AS OLD_CLIENT_LEVEL           ")
    _str_bld.AppendLine("       , NEW_CLIENT.AC_CREATED       AS NEW_CLIENT_CREATED         ")
    _str_bld.AppendLine("       , NEW_CLIENT.AC_ACCOUNT_ID    AS NEW_CLIENT_ID              ")
    _str_bld.AppendLine("       , NEW_CLIENT.AC_HOLDER_NAME   AS NEW_CLIENT_NAME            ")
    _str_bld.AppendLine("       , NEW_CLIENT.AC_HOLDER_LEVEL  AS NEW_CLIENT_LEVEL           ")
    _str_bld.AppendLine("  FROM   ACCOUNTS NEW_CLIENT                                       ")
    _str_bld.AppendLine(" INNER   JOIN ACCOUNTS OLD_CLIENT                                  ")
    _str_bld.AppendLine("    ON   NEW_CLIENT.AC_RECOMMENDED_BY =  OLD_CLIENT.AC_ACCOUNT_ID  ")
    _str_bld.AppendLine(" WHERE   NEW_CLIENT.AC_CREATED        >= @pStartDate               ")
    _str_bld.AppendLine("   AND   NEW_CLIENT.AC_CREATED        <  @pEndDate                 ")

    If TITO.Utils.IsTitoMode Then
      _str_bld.AppendLine(" AND (NEW_CLIENT.AC_TYPE NOT IN(" & AccountType.ACCOUNT_VIRTUAL_CASHIER & ", " & AccountType.ACCOUNT_VIRTUAL_TERMINAL & "))")
    End If

    _str_bld.AppendLine(" ORDER   BY OLD_CLIENT.AC_HOLDER_LEVEL DESC                        ")
    _str_bld.AppendLine("       , OLD_CLIENT.AC_ACCOUNT_ID ASC                              ")
    _str_bld.AppendLine("       , NEW_CLIENT.AC_CREATED ASC                                 ")

    _sql_command = New SqlCommand(_str_bld.ToString())

    _sql_command.Parameters.Add("@pStartDate", SqlDbType.DateTime).Value = m_date_from

    _sql_command.Parameters.Add("@pEndDate", SqlDbType.DateTime).Value = m_date_to

    Return _sql_command

  End Function ' GUI_GetSqlCommand

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.uc_dsl
  End Sub ' GUI_SetInitialFocus

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(498) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(498) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)


    PrintData.FilterHeaderWidth(1) = 3000
    PrintData.FilterValueWidth(1) = 1500

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()

    Dim aux_date As Date

    m_date_from = ""
    m_date_to = ""

    'Date
    m_date_from = GUI_FormatDate(Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime), _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMM)

    If Me.uc_dsl.ToDateSelected Then
      m_date_to = GUI_FormatDate(Me.uc_dsl.ToDate.AddHours(Me.uc_dsl.ClosingTime), _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMM)
    Else
      If WSI.Common.WGDB.Now.Hour >= Me.uc_dsl.ClosingTime Then
        aux_date = New Date(WSI.Common.WGDB.Now.Year, WSI.Common.WGDB.Now.Month, WSI.Common.WGDB.Now.Day, Me.uc_dsl.ClosingTime, 0, 0)
        aux_date = aux_date.AddDays(1)
      Else
        aux_date = New Date(WSI.Common.WGDB.Now.Year, WSI.Common.WGDB.Now.Month, WSI.Common.WGDB.Now.Day, Me.uc_dsl.ClosingTime, 0, 0)
      End If
      m_date_to = GUI_FormatDate(aux_date, _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region ' Overrides

#Region " Private Functions "

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, False)

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 150
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Recommender: Account
      .Column(GRID_COLUMN_OLD_CLIENT_ID).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(494)
      .Column(GRID_COLUMN_OLD_CLIENT_ID).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(207)
      .Column(GRID_COLUMN_OLD_CLIENT_ID).Width = 1150
      .Column(GRID_COLUMN_OLD_CLIENT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Recommender: Name
      .Column(GRID_COLUMN_OLD_CLIENT_NAME).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(494)
      .Column(GRID_COLUMN_OLD_CLIENT_NAME).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(210)
      .Column(GRID_COLUMN_OLD_CLIENT_NAME).Width = 4000
      .Column(GRID_COLUMN_OLD_CLIENT_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Recommender: Level
      .Column(GRID_COLUMN_OLD_CLIENT_LEVEL).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(494)
      .Column(GRID_COLUMN_OLD_CLIENT_LEVEL).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(496)
      .Column(GRID_COLUMN_OLD_CLIENT_LEVEL).Width = 1250
      .Column(GRID_COLUMN_OLD_CLIENT_LEVEL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Recommended: Create date
      .Column(GRID_COLUMN_NEW_CLIENT_CREATED).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(495)
      .Column(GRID_COLUMN_NEW_CLIENT_CREATED).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(497)
      .Column(GRID_COLUMN_NEW_CLIENT_CREATED).Width = 2000
      .Column(GRID_COLUMN_NEW_CLIENT_CREATED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Recommended: Account
      .Column(GRID_COLUMN_NEW_CLIENT_ID).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(495)
      .Column(GRID_COLUMN_NEW_CLIENT_ID).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(207)
      .Column(GRID_COLUMN_NEW_CLIENT_ID).Width = 1150
      .Column(GRID_COLUMN_NEW_CLIENT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Recommended: Name
      .Column(GRID_COLUMN_NEW_CLIENT_NAME).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(495)
      .Column(GRID_COLUMN_NEW_CLIENT_NAME).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(210)
      .Column(GRID_COLUMN_NEW_CLIENT_NAME).Width = 4000
      .Column(GRID_COLUMN_NEW_CLIENT_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Recommended: Level
      .Column(GRID_COLUMN_NEW_CLIENT_LEVEL).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(495)
      .Column(GRID_COLUMN_NEW_CLIENT_LEVEL).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(496)
      .Column(GRID_COLUMN_NEW_CLIENT_LEVEL).Width = 1250
      .Column(GRID_COLUMN_NEW_CLIENT_LEVEL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      .Counter(COUNTER_RECOMMENDER).Visible = True
      .Counter(COUNTER_RECOMMENDER).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

      .Sortable = False

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()
    Dim closing_time As Integer
    Dim final_time As Date

    closing_time = GetDefaultClosingTime()
    final_time = New DateTime(WSI.Common.WGDB.Now.Year, WSI.Common.WGDB.Now.Month, WSI.Common.WGDB.Now.Day, closing_time, 0, 0)
    If final_time > WSI.Common.WGDB.Now Then
      final_time = final_time.AddDays(-1)
    End If

    final_time = final_time.Date
    Me.uc_dsl.ToDate = final_time
    Me.uc_dsl.ToDateSelected = True
    Me.uc_dsl.FromDate = final_time.AddMonths(-1).AddDays(final_time.Day * -1 + 1)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.ClosingTime = closing_time

    Call Me.Grid.Clear()

    'Me.Grid.PanelRightVisible = False

  End Sub ' SetDefaultValues 

  ' PURPOSE : Reset Total Counters variable
  '
  '  PARAMS :
  '     - INPUT : 
  '           - Total As TYPE_TOTAL_COUNTERS
  '           - Key As String
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub ResetTotalCounters(ByRef Total As TYPE_TOTAL_COUNTERS)

    Total.total_recommended = 0

  End Sub ' ResetTotalCounters

  ' PURPOSE : Update total counters.
  '
  '  PARAMS :
  '     - INPUT :
  '           - Total As TYPE_TOTAL_COUNTERS
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub UpdateTotalCounters(ByRef Total As TYPE_TOTAL_COUNTERS, ByVal DbRow As CLASS_DB_ROW)
    With Total
      .total_recommended += 1
    End With
  End Sub ' UpdateTotalCounters

  ' PURPOSE : Process total counters: Draw row of totals, update and reset total counters.
  '
  '  PARAMS :
  '     - INPUT : 
  '           - DbRow As CLASS_DB_ROW
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub ProcessCounters(ByVal DbRow As CLASS_DB_ROW)

    If m_allow_totals And m_total_client.client_id <> DbRow.Value(SQL_COLUMN_OLD_CLIENT_ID) Then
      Call ProcessSubTotal()
      Call ResetTotalCounters(m_total_client)
    End If

    UpdateTotalCounters(m_total, DbRow)
    UpdateTotalCounters(m_total_client, DbRow)

    m_allow_totals = True

  End Sub ' ProcessCounters

  ' PURPOSE : Process sub-total data for DrawRow
  '
  '  PARAMS :
  '     - INPUT : 
  '           - DbRow As CLASS_DB_ROW
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub ProcessSubTotal()
    Dim _color As Integer

    m_total_client.client_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(828) & m_total_client.client_name
    _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01
    DrawRow(m_total_client, _color)
    Me.Grid.Counter(COUNTER_RECOMMENDER).Value += 1

  End Sub ' ProcessSubTotal

  ' PURPOSE : Process total data for DrawRow
  '
  '  PARAMS :
  '     - INPUT : 
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub ProcessTotal()
    Dim _color As Integer

    m_total.client_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(829)
    _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
    DrawRow(m_total, _color)

  End Sub ' ProcessTotal

  ' PURPOSE: Show results to screen
  '
  '  PARAMS:
  '     - INPUT:
  '           - Provider items with data
  '           - color of row
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Function DrawRow(ByVal Total As TYPE_TOTAL_COUNTERS, ByVal Color As Integer) As Boolean

    Dim idx_row As Integer

    With Me.Grid
      .AddRow()
      idx_row = Me.Grid.NumRows - 1

      .Cell(idx_row, GRID_COLUMN_OLD_CLIENT_NAME).Value = Total.client_name
      .Cell(idx_row, GRID_COLUMN_OLD_CLIENT_LEVEL).Value = Total.total_recommended

      .Row(idx_row).BackColor = GetColor(Color)

    End With

    Return True

  End Function ' DrawRow

#End Region ' Private Functions

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Events "

#End Region ' Events

End Class  ' frm_recommendation_report

