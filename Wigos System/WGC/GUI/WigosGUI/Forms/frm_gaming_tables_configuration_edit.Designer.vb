<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_gaming_tables_configuration_edit
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_gaming_tables_configuration_edit))
    Me.gb_params = New System.Windows.Forms.GroupBox()
    Me.pnl_gaming_tables_points_assignment_mode = New System.Windows.Forms.Panel()
    Me.lbl_gaming_tables_points_assignment_days = New System.Windows.Forms.Label()
    Me.txt_gaming_tables_points_assignment_days = New System.Windows.Forms.TextBox()
    Me.cmb_gaming_tables_points_assignment_mode = New GUI_Controls.uc_combo()
    Me.tb_currencies = New System.Windows.Forms.TabControl()
    Me.ef_max_allowed_sale = New GUI_Controls.uc_entry_field()
    Me.ef_max_allowed_purchase = New GUI_Controls.uc_entry_field()
    Me.chk_insert_visits = New System.Windows.Forms.CheckBox()
    Me.chk_stock_control = New System.Windows.Forms.CheckBox()
    Me.gb_cashiermode = New System.Windows.Forms.GroupBox()
    Me.btn_ticket_configuration = New GUI_Controls.uc_button()
    Me.chk_cashier_concilition_check_account = New System.Windows.Forms.CheckBox()
    Me.chk_cashier_concilition_enable = New System.Windows.Forms.CheckBox()
    Me.ef_dealercopy_title = New GUI_Controls.uc_entry_field()
    Me.lbl_dealercopy_title = New System.Windows.Forms.Label()
    Me.cmb_cashier_mode = New GUI_Controls.uc_combo()
    Me.chk_dealercopy_barcode = New System.Windows.Forms.CheckBox()
    Me.chk_dealercopy_signature = New System.Windows.Forms.CheckBox()
    Me.chk_player_tracking = New System.Windows.Forms.CheckBox()
    Me.cmb_gaming_tables_mode = New GUI_Controls.uc_combo()
    Me.panel_data.SuspendLayout()
    Me.gb_params.SuspendLayout()
    Me.pnl_gaming_tables_points_assignment_mode.SuspendLayout()
    Me.gb_cashiermode.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.cmb_gaming_tables_mode)
    Me.panel_data.Controls.Add(Me.chk_player_tracking)
    Me.panel_data.Controls.Add(Me.gb_cashiermode)
    Me.panel_data.Controls.Add(Me.gb_params)
    Me.panel_data.Size = New System.Drawing.Size(503, 398)
    '
    'gb_params
    '
    Me.gb_params.Controls.Add(Me.pnl_gaming_tables_points_assignment_mode)
    Me.gb_params.Controls.Add(Me.cmb_gaming_tables_points_assignment_mode)
    Me.gb_params.Controls.Add(Me.tb_currencies)
    Me.gb_params.Controls.Add(Me.ef_max_allowed_sale)
    Me.gb_params.Controls.Add(Me.ef_max_allowed_purchase)
    Me.gb_params.Controls.Add(Me.chk_insert_visits)
    Me.gb_params.Controls.Add(Me.chk_stock_control)
    Me.gb_params.Location = New System.Drawing.Point(4, 75)
    Me.gb_params.Name = "gb_params"
    Me.gb_params.Size = New System.Drawing.Size(494, 157)
    Me.gb_params.TabIndex = 1
    Me.gb_params.TabStop = False
    Me.gb_params.Text = "xConfiguration"
    '
    'pnl_gaming_tables_points_assignment_mode
    '
    Me.pnl_gaming_tables_points_assignment_mode.Controls.Add(Me.lbl_gaming_tables_points_assignment_days)
    Me.pnl_gaming_tables_points_assignment_mode.Controls.Add(Me.txt_gaming_tables_points_assignment_days)
    Me.pnl_gaming_tables_points_assignment_mode.Location = New System.Drawing.Point(388, 122)
    Me.pnl_gaming_tables_points_assignment_mode.Name = "pnl_gaming_tables_points_assignment_mode"
    Me.pnl_gaming_tables_points_assignment_mode.Size = New System.Drawing.Size(101, 27)
    Me.pnl_gaming_tables_points_assignment_mode.TabIndex = 6
    '
    'lbl_gaming_tables_points_assignment_days
    '
    Me.lbl_gaming_tables_points_assignment_days.AutoSize = True
    Me.lbl_gaming_tables_points_assignment_days.Location = New System.Drawing.Point(51, 7)
    Me.lbl_gaming_tables_points_assignment_days.Name = "lbl_gaming_tables_points_assignment_days"
    Me.lbl_gaming_tables_points_assignment_days.Size = New System.Drawing.Size(43, 13)
    Me.lbl_gaming_tables_points_assignment_days.TabIndex = 7
    Me.lbl_gaming_tables_points_assignment_days.Text = "xDays"
    '
    'txt_gaming_tables_points_assignment_days
    '
    Me.txt_gaming_tables_points_assignment_days.Location = New System.Drawing.Point(5, 3)
    Me.txt_gaming_tables_points_assignment_days.Name = "txt_gaming_tables_points_assignment_days"
    Me.txt_gaming_tables_points_assignment_days.Size = New System.Drawing.Size(41, 21)
    Me.txt_gaming_tables_points_assignment_days.TabIndex = 6
    Me.txt_gaming_tables_points_assignment_days.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    '
    'cmb_gaming_tables_points_assignment_mode
    '
    Me.cmb_gaming_tables_points_assignment_mode.AllowUnlistedValues = False
    Me.cmb_gaming_tables_points_assignment_mode.AutoCompleteMode = False
    Me.cmb_gaming_tables_points_assignment_mode.IsReadOnly = False
    Me.cmb_gaming_tables_points_assignment_mode.Location = New System.Drawing.Point(11, 124)
    Me.cmb_gaming_tables_points_assignment_mode.Name = "cmb_gaming_tables_points_assignment_mode"
    Me.cmb_gaming_tables_points_assignment_mode.SelectedIndex = -1
    Me.cmb_gaming_tables_points_assignment_mode.Size = New System.Drawing.Size(376, 24)
    Me.cmb_gaming_tables_points_assignment_mode.SufixText = "Sufix Text"
    Me.cmb_gaming_tables_points_assignment_mode.SufixTextVisible = True
    Me.cmb_gaming_tables_points_assignment_mode.TabIndex = 5
    Me.cmb_gaming_tables_points_assignment_mode.TextCombo = ""
    Me.cmb_gaming_tables_points_assignment_mode.TextWidth = 190
    '
    'tb_currencies
    '
    Me.tb_currencies.Location = New System.Drawing.Point(171, 13)
    Me.tb_currencies.Name = "tb_currencies"
    Me.tb_currencies.SelectedIndex = 0
    Me.tb_currencies.Size = New System.Drawing.Size(318, 104)
    Me.tb_currencies.TabIndex = 4
    '
    'ef_max_allowed_sale
    '
    Me.ef_max_allowed_sale.DoubleValue = 0.0R
    Me.ef_max_allowed_sale.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_max_allowed_sale.IntegerValue = 0
    Me.ef_max_allowed_sale.IsReadOnly = False
    Me.ef_max_allowed_sale.Location = New System.Drawing.Point(187, 61)
    Me.ef_max_allowed_sale.Name = "ef_max_allowed_sale"
    Me.ef_max_allowed_sale.PlaceHolder = Nothing
    Me.ef_max_allowed_sale.Size = New System.Drawing.Size(240, 25)
    Me.ef_max_allowed_sale.SufixText = "Sufix Text"
    Me.ef_max_allowed_sale.SufixTextVisible = True
    Me.ef_max_allowed_sale.TabIndex = 3
    Me.ef_max_allowed_sale.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_max_allowed_sale.TextValue = ""
    Me.ef_max_allowed_sale.TextWidth = 120
    Me.ef_max_allowed_sale.Value = ""
    Me.ef_max_allowed_sale.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_max_allowed_purchase
    '
    Me.ef_max_allowed_purchase.DoubleValue = 0.0R
    Me.ef_max_allowed_purchase.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_max_allowed_purchase.IntegerValue = 0
    Me.ef_max_allowed_purchase.IsReadOnly = False
    Me.ef_max_allowed_purchase.Location = New System.Drawing.Point(187, 35)
    Me.ef_max_allowed_purchase.Name = "ef_max_allowed_purchase"
    Me.ef_max_allowed_purchase.PlaceHolder = Nothing
    Me.ef_max_allowed_purchase.Size = New System.Drawing.Size(240, 25)
    Me.ef_max_allowed_purchase.SufixText = "Sufix Text"
    Me.ef_max_allowed_purchase.SufixTextVisible = True
    Me.ef_max_allowed_purchase.TabIndex = 1
    Me.ef_max_allowed_purchase.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_max_allowed_purchase.TextValue = ""
    Me.ef_max_allowed_purchase.TextWidth = 120
    Me.ef_max_allowed_purchase.Value = ""
    Me.ef_max_allowed_purchase.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_insert_visits
    '
    Me.chk_insert_visits.AutoSize = True
    Me.chk_insert_visits.Location = New System.Drawing.Point(11, 64)
    Me.chk_insert_visits.Name = "chk_insert_visits"
    Me.chk_insert_visits.Size = New System.Drawing.Size(63, 17)
    Me.chk_insert_visits.TabIndex = 2
    Me.chk_insert_visits.Text = "xVisits"
    Me.chk_insert_visits.UseVisualStyleBackColor = True
    '
    'chk_stock_control
    '
    Me.chk_stock_control.AutoSize = True
    Me.chk_stock_control.Location = New System.Drawing.Point(11, 38)
    Me.chk_stock_control.Name = "chk_stock_control"
    Me.chk_stock_control.Size = New System.Drawing.Size(107, 17)
    Me.chk_stock_control.TabIndex = 0
    Me.chk_stock_control.Text = "xStockControl"
    Me.chk_stock_control.UseVisualStyleBackColor = True
    '
    'gb_cashiermode
    '
    Me.gb_cashiermode.Controls.Add(Me.btn_ticket_configuration)
    Me.gb_cashiermode.Controls.Add(Me.chk_cashier_concilition_check_account)
    Me.gb_cashiermode.Controls.Add(Me.chk_cashier_concilition_enable)
    Me.gb_cashiermode.Controls.Add(Me.ef_dealercopy_title)
    Me.gb_cashiermode.Controls.Add(Me.lbl_dealercopy_title)
    Me.gb_cashiermode.Controls.Add(Me.cmb_cashier_mode)
    Me.gb_cashiermode.Controls.Add(Me.chk_dealercopy_barcode)
    Me.gb_cashiermode.Controls.Add(Me.chk_dealercopy_signature)
    Me.gb_cashiermode.Location = New System.Drawing.Point(4, 238)
    Me.gb_cashiermode.Name = "gb_cashiermode"
    Me.gb_cashiermode.Size = New System.Drawing.Size(494, 156)
    Me.gb_cashiermode.TabIndex = 2
    Me.gb_cashiermode.TabStop = False
    Me.gb_cashiermode.Text = "xCashierMode"
    '
    'btn_ticket_configuration
    '
    Me.btn_ticket_configuration.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_ticket_configuration.Location = New System.Drawing.Point(180, 20)
    Me.btn_ticket_configuration.Name = "btn_ticket_configuration"
    Me.btn_ticket_configuration.Size = New System.Drawing.Size(63, 21)
    Me.btn_ticket_configuration.TabIndex = 7
    Me.btn_ticket_configuration.ToolTipped = False
    Me.btn_ticket_configuration.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.SMALL
    '
    'chk_cashier_concilition_check_account
    '
    Me.chk_cashier_concilition_check_account.AutoSize = True
    Me.chk_cashier_concilition_check_account.Location = New System.Drawing.Point(64, 133)
    Me.chk_cashier_concilition_check_account.Name = "chk_cashier_concilition_check_account"
    Me.chk_cashier_concilition_check_account.Size = New System.Drawing.Size(217, 17)
    Me.chk_cashier_concilition_check_account.TabIndex = 6
    Me.chk_cashier_concilition_check_account.Text = "xCashierConcilitionCheckAccount"
    Me.chk_cashier_concilition_check_account.UseVisualStyleBackColor = True
    '
    'chk_cashier_concilition_enable
    '
    Me.chk_cashier_concilition_enable.AutoSize = True
    Me.chk_cashier_concilition_enable.Location = New System.Drawing.Point(64, 109)
    Me.chk_cashier_concilition_enable.Name = "chk_cashier_concilition_enable"
    Me.chk_cashier_concilition_enable.Size = New System.Drawing.Size(174, 17)
    Me.chk_cashier_concilition_enable.TabIndex = 5
    Me.chk_cashier_concilition_enable.Text = "xCashierConcilitionEnable"
    Me.chk_cashier_concilition_enable.UseVisualStyleBackColor = True
    '
    'ef_dealercopy_title
    '
    Me.ef_dealercopy_title.DoubleValue = 0.0R
    Me.ef_dealercopy_title.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_dealercopy_title.IntegerValue = 0
    Me.ef_dealercopy_title.IsReadOnly = False
    Me.ef_dealercopy_title.Location = New System.Drawing.Point(95, 76)
    Me.ef_dealercopy_title.Name = "ef_dealercopy_title"
    Me.ef_dealercopy_title.PlaceHolder = Nothing
    Me.ef_dealercopy_title.Size = New System.Drawing.Size(333, 25)
    Me.ef_dealercopy_title.SufixText = "Sufix Text"
    Me.ef_dealercopy_title.SufixTextVisible = True
    Me.ef_dealercopy_title.TabIndex = 4
    Me.ef_dealercopy_title.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_dealercopy_title.TextValue = ""
    Me.ef_dealercopy_title.TextWidth = 0
    Me.ef_dealercopy_title.Value = ""
    Me.ef_dealercopy_title.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_dealercopy_title
    '
    Me.lbl_dealercopy_title.AutoSize = True
    Me.lbl_dealercopy_title.Location = New System.Drawing.Point(61, 81)
    Me.lbl_dealercopy_title.Name = "lbl_dealercopy_title"
    Me.lbl_dealercopy_title.Size = New System.Drawing.Size(38, 13)
    Me.lbl_dealercopy_title.TabIndex = 3
    Me.lbl_dealercopy_title.Text = "xTitle"
    '
    'cmb_cashier_mode
    '
    Me.cmb_cashier_mode.AllowUnlistedValues = False
    Me.cmb_cashier_mode.AutoCompleteMode = False
    Me.cmb_cashier_mode.IsReadOnly = False
    Me.cmb_cashier_mode.Location = New System.Drawing.Point(11, 18)
    Me.cmb_cashier_mode.Name = "cmb_cashier_mode"
    Me.cmb_cashier_mode.SelectedIndex = -1
    Me.cmb_cashier_mode.Size = New System.Drawing.Size(163, 24)
    Me.cmb_cashier_mode.SufixText = "Sufix Text"
    Me.cmb_cashier_mode.SufixTextVisible = True
    Me.cmb_cashier_mode.TabIndex = 0
    Me.cmb_cashier_mode.TextCombo = Nothing
    Me.cmb_cashier_mode.TextWidth = 50
    '
    'chk_dealercopy_barcode
    '
    Me.chk_dealercopy_barcode.AutoSize = True
    Me.chk_dealercopy_barcode.Location = New System.Drawing.Point(158, 52)
    Me.chk_dealercopy_barcode.Name = "chk_dealercopy_barcode"
    Me.chk_dealercopy_barcode.Size = New System.Drawing.Size(148, 17)
    Me.chk_dealercopy_barcode.TabIndex = 2
    Me.chk_dealercopy_barcode.Text = "xDealerCopyBarcode"
    Me.chk_dealercopy_barcode.UseVisualStyleBackColor = True
    Me.chk_dealercopy_barcode.Visible = False
    '
    'chk_dealercopy_signature
    '
    Me.chk_dealercopy_signature.AutoSize = True
    Me.chk_dealercopy_signature.Location = New System.Drawing.Point(64, 52)
    Me.chk_dealercopy_signature.Name = "chk_dealercopy_signature"
    Me.chk_dealercopy_signature.Size = New System.Drawing.Size(156, 17)
    Me.chk_dealercopy_signature.TabIndex = 1
    Me.chk_dealercopy_signature.Text = "xDealerCopySignature"
    Me.chk_dealercopy_signature.UseVisualStyleBackColor = True
    '
    'chk_player_tracking
    '
    Me.chk_player_tracking.AutoSize = True
    Me.chk_player_tracking.Location = New System.Drawing.Point(70, 52)
    Me.chk_player_tracking.Name = "chk_player_tracking"
    Me.chk_player_tracking.Size = New System.Drawing.Size(163, 17)
    Me.chk_player_tracking.TabIndex = 5
    Me.chk_player_tracking.Text = "xPlayerTrackingEnabled"
    Me.chk_player_tracking.UseVisualStyleBackColor = True
    '
    'cmb_gaming_tables_mode
    '
    Me.cmb_gaming_tables_mode.AllowUnlistedValues = False
    Me.cmb_gaming_tables_mode.AutoCompleteMode = False
    Me.cmb_gaming_tables_mode.IsReadOnly = False
    Me.cmb_gaming_tables_mode.Location = New System.Drawing.Point(20, 12)
    Me.cmb_gaming_tables_mode.Name = "cmb_gaming_tables_mode"
    Me.cmb_gaming_tables_mode.SelectedIndex = -1
    Me.cmb_gaming_tables_mode.Size = New System.Drawing.Size(473, 24)
    Me.cmb_gaming_tables_mode.SufixText = "Sufix Text"
    Me.cmb_gaming_tables_mode.SufixTextVisible = True
    Me.cmb_gaming_tables_mode.TabIndex = 0
    Me.cmb_gaming_tables_mode.TextCombo = ""
    Me.cmb_gaming_tables_mode.TextWidth = 150
    '
    'frm_gaming_tables_configuration_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(605, 409)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_gaming_tables_configuration_edit"
    Me.Text = "frm_gaming_tables_configuration_edit"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.gb_params.ResumeLayout(False)
    Me.gb_params.PerformLayout()
    Me.pnl_gaming_tables_points_assignment_mode.ResumeLayout(False)
    Me.pnl_gaming_tables_points_assignment_mode.PerformLayout()
    Me.gb_cashiermode.ResumeLayout(False)
    Me.gb_cashiermode.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_params As System.Windows.Forms.GroupBox
  Friend WithEvents chk_stock_control As System.Windows.Forms.CheckBox
  Friend WithEvents chk_insert_visits As System.Windows.Forms.CheckBox
  Friend WithEvents ef_max_allowed_sale As GUI_Controls.uc_entry_field
  Friend WithEvents ef_max_allowed_purchase As GUI_Controls.uc_entry_field
  Friend WithEvents gb_cashiermode As System.Windows.Forms.GroupBox
  Friend WithEvents chk_dealercopy_barcode As System.Windows.Forms.CheckBox
  Friend WithEvents chk_dealercopy_signature As System.Windows.Forms.CheckBox
  Friend WithEvents ef_dealercopy_title As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_dealercopy_title As System.Windows.Forms.Label
  Friend WithEvents cmb_cashier_mode As GUI_Controls.uc_combo
  Friend WithEvents tb_currencies As System.Windows.Forms.TabControl
  Friend WithEvents cmb_gaming_tables_mode As GUI_Controls.uc_combo
  Friend WithEvents chk_player_tracking As System.Windows.Forms.CheckBox
  Friend WithEvents chk_cashier_concilition_enable As System.Windows.Forms.CheckBox
  Friend WithEvents chk_cashier_concilition_check_account As System.Windows.Forms.CheckBox
  Friend WithEvents btn_ticket_configuration As GUI_Controls.uc_button
  Friend WithEvents cmb_gaming_tables_points_assignment_mode As GUI_Controls.uc_combo
  Friend WithEvents pnl_gaming_tables_points_assignment_mode As System.Windows.Forms.Panel
  Friend WithEvents lbl_gaming_tables_points_assignment_days As System.Windows.Forms.Label
  Friend WithEvents txt_gaming_tables_points_assignment_days As System.Windows.Forms.TextBox
End Class
