Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_log_viewer
    Inherits frm_base


    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.btn_exit = New GUI_Controls.uc_button
    Me.wb_log_viewer = New System.Windows.Forms.WebBrowser
    Me.SuspendLayout()
    '
    'btn_exit
    '
    Me.btn_exit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btn_exit.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btn_exit.Location = New System.Drawing.Point(936, 425)
    Me.btn_exit.Name = "btn_exit"
    Me.btn_exit.Size = New System.Drawing.Size(90, 30)
    Me.btn_exit.TabIndex = 0
    Me.btn_exit.ToolTipped = False
    Me.btn_exit.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'wb_log_viewer
    '
    Me.wb_log_viewer.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.wb_log_viewer.Location = New System.Drawing.Point(8, 7)
    Me.wb_log_viewer.MinimumSize = New System.Drawing.Size(20, 20)
    Me.wb_log_viewer.Name = "wb_log_viewer"
    Me.wb_log_viewer.Size = New System.Drawing.Size(1018, 410)
    Me.wb_log_viewer.TabIndex = 1
    '
    'frm_log_viewer
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.CancelButton = Me.btn_exit
    Me.ClientSize = New System.Drawing.Size(1034, 462)
    Me.Controls.Add(Me.btn_exit)
    Me.Controls.Add(Me.wb_log_viewer)
    Me.Name = "frm_log_viewer"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_log_viewer"
    Me.TopMost = True
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents btn_exit As GUI_Controls.uc_button
  Friend WithEvents wb_log_viewer As System.Windows.Forms.WebBrowser
End Class
