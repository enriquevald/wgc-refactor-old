﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_junkets_flyer_edit.vb
' DESCRIPTION:   Create and edit flyer
' AUTHOR:        David Perelló
' CREATION DATE: 05-APR-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 05-APR-2017  DPC    Initial version
' 31-JUL-2017  DPC    WIGOS-4084: Junkets: wrong label in the Flyer definition
' -------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports WSI.Common
Imports GUI_CommonMisc
Imports GUI_Controls.frm_base_sel

Public Class frm_junkets_flyer_edit
  Inherits frm_base_edit

#Region " Constants"

  ' SELECT FLAG GRID
  Private Const GRID_FLAG_IDX As Integer = 0
  Private Const GRID_FLAG_CHECKED As Integer = 1
  Private Const GRID_FLAG_COLOR As Integer = 2
  Private Const GRID_FLAG_NAME As Integer = 3
  Private Const GRID_FLAG_COUNT As Integer = 4
  Private Const GRID_FLAG_ID_COLOR As Integer = 5
  Private Const GRID_FLAG_TYPE As Integer = 6

  Private Const GRID_SELECT_FLAG_COLUMNS As Integer = 7
  Private Const GRID_SELECT_FLAG_HEADER_ROWS As Integer = 1
  Private Const FORM_DB_MIN_VERSION As Short = 158

  Private Const LEN_FLAG_AWARDED_DIGITS As Integer = 2

#End Region ' Constants

#Region " Members "

  Private m_flyer_read As CLASS_JUNKETS_FLYER
  Private m_flyer_edit As CLASS_JUNKETS_FLYER
  Private m_promotion As CLASS_JUNKETS_FLYER.TYPE_PROMOTION

#End Region ' Members

#Region " Properties "

  Public InfoFormFlyer As CLASS_JUNKETS_FLYER.InfoFormFlyer

#End Region ' Properties

#Region " Overrides "

  ''' <summary>
  '''    GUI_SetFormId
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_JUNKETS_FLYER_EDIT
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ''' <summary>
  '''   ShowEditItem
  ''' </summary>
  ''' <param name="id"></param>
  ''' <remarks></remarks>
  Public Overloads Sub ShowEditItem(ByVal id As Object)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Me.DbObjectId = InfoFormFlyer

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If
  End Sub ' ShowEditItem

  ''' <summary>
  '''   GUI_InitControls
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_InitControls()

    MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8091) ' Form Name

    ef_flyer_code.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5710) ' Code
    Call Me.ef_flyer_code.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_ALPHA_NUMERIC, 6)

    ef_num_flyers_created.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8077) ' Number of flyers printed
    Call Me.ef_num_flyers_created.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, 6)

    chk_reuse.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8075) ' Can be reused

    chk_show_pop_up.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2715) ' Message to show
    txt_text_pop_up.MaxLength = 256

    chk_print_voucher.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8076) ' Print Voucher in Cashier

    ef_voucher_title.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(392) ' Tile voucher
    Call Me.ef_voucher_title.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    ef_voucher_text.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8212) ' Text voucher
    Call Me.ef_voucher_text.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    ef_promotion_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3419) ' Name
    Call Me.ef_promotion_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_ALPHA_NUMERIC, 100)
    ef_promotion_name.IsReadOnly = True

    chk_print_promotional_voucher.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8232) ' Text in promotional voucher
    Me.txt_text_promotional.MaxLength = 50

    bt_select.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1894) ' Select
    bt_remove_promotion.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1085) ' Delete

    Me.gb_comments.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1716) ' Comments
    Me.gb_details.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(474) ' Details
    Me.gb_flags.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8190) ' Applied flags
    Me.gb_pop_up.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1428) ' Message
    Me.gb_promotions.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8189) ' Cashier promotion
    Me.gb_voucher.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7494) ' Voucher

    Me.txt_comments.MaxLength = 512

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_DELETE).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_PRINT).Visible = False

    If m_flyer_read Is Nothing Then
      m_flyer_read = DbReadObject
    End If

    m_flyer_read.isNew = (ScreenMode = ENUM_SCREEN_MODE.MODE_NEW)

    Call InitializeFlags()

    If ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT Then
      ef_flyer_code.IsReadOnly = True
      ef_flyer_code.TabStop = False
    End If

  End Sub ' GUI_InitControls

  ''' <summary>
  '''    GUI_SetScreenData
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    ef_flyer_code.Value = m_flyer_read.Code

    If ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT Then

      ef_flyer_code.Value = m_flyer_read.Code
      ef_num_flyers_created.Value = m_flyer_read.NumCreated
      chk_reuse.Checked = m_flyer_read.AllowReuse

      chk_show_pop_up.Checked = m_flyer_read.ShowPopUp
      txt_text_pop_up.Text = m_flyer_read.TextPopUp

      chk_print_voucher.Checked = m_flyer_read.PrintVoucher
      ef_voucher_title.Value = m_flyer_edit.TitleVoucher
      ef_voucher_text.Value = m_flyer_read.TextVoucher

      chk_print_promotional_voucher.Checked = m_flyer_read.PrintTextPromotionalVoucher
      txt_text_promotional.Text = m_flyer_read.TextPromotionalVoucher

      ef_promotion_name.Value = m_flyer_read.Promotion.name

      txt_comments.Text = m_flyer_read.Comment

    End If

    Call FillFlagsGrid(Me.dg_flags, True)

  End Sub ' GUI_SetScreenData

  ''' <summary>
  '''    GUI_IsScreenDataOk
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    If String.IsNullOrEmpty(ef_flyer_code.Value.Trim()) Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(5710))
      ef_flyer_code.Focus()

      Return False
    End If

    If ef_flyer_code.Value.Trim().Length < 6 Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8155), ENUM_MB_TYPE.MB_TYPE_WARNING)
      ef_flyer_code.Focus()

      Return False
    End If

    If ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
      If CLASS_JUNKETS_FLYER.ExistCodeFlyer(ef_flyer_code.Value, True) Then

        If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8156), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO) <> ENUM_MB_RESULT.MB_RESULT_YES Then
          ef_flyer_code.Focus()

          Return False
        End If
      End If
    End If

    If chk_print_promotional_voucher.Checked Then
      If String.IsNullOrEmpty(txt_text_promotional.Text.Trim()) Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(8233))
        txt_text_promotional.Focus()

        Return False
      End If
    End If

    If chk_show_pop_up.Checked Then
      If String.IsNullOrEmpty(txt_text_pop_up.Text.Trim()) Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1428))
        txt_text_pop_up.Focus()

        Return False
      End If
    End If

    If chk_print_voucher.Checked Then
      If String.IsNullOrEmpty(ef_voucher_title.Value) Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_WARNING, , , String.Format("{0} - {1}", GLB_NLS_GUI_PLAYER_TRACKING.GetString(7494), GLB_NLS_GUI_PLAYER_TRACKING.GetString(392)))
        ef_voucher_title.Focus()

        Return False
      End If

      If String.IsNullOrEmpty(ef_voucher_text.Value) Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_WARNING, , , String.Format("{0} - {1}", GLB_NLS_GUI_PLAYER_TRACKING.GetString(7494), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8212)))
        ef_voucher_text.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_IsScreenDataOk

  Protected Overrides Sub GUI_GetScreenData()

    m_flyer_edit.Code = ef_flyer_code.Value
    m_flyer_edit.JunketId = Me.InfoFormFlyer.junket_id
    m_flyer_edit.NumCreated = IIf(String.IsNullOrEmpty(ef_num_flyers_created.Value), 0, ef_num_flyers_created.Value)
    m_flyer_edit.AllowReuse = chk_reuse.Checked
    m_flyer_edit.ShowPopUp = chk_show_pop_up.Checked
    m_flyer_edit.TextPopUp = txt_text_pop_up.Text
    m_flyer_edit.PrintVoucher = chk_print_voucher.Checked
    m_flyer_edit.TextVoucher = ef_voucher_text.Value
    m_flyer_edit.TitleVoucher = ef_voucher_title.Value
    m_flyer_edit.PrintTextPromotionalVoucher = chk_print_promotional_voucher.Checked
    m_flyer_edit.TextPromotionalVoucher = txt_text_promotional.Text

    If Not m_promotion Is Nothing Then
      m_flyer_edit.Promotion.id = m_promotion.id
      m_flyer_edit.Promotion.name = m_promotion.name
    End If

    m_flyer_edit.Comment = txt_comments.Text

    GetFlagsGrid()

  End Sub ' GUI_GetScreenData

  ''' <summary>
  '''   GUI_ButtonClick
  ''' </summary>
  ''' <param name="ButtonId"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Call MyBase.GUI_ButtonClick(ButtonId)

  End Sub 'GUI_ButtonClick

  ''' <summary>
  '''    GUI_DB_Operation
  ''' </summary>
  ''' <param name="DbOperation"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)
    Dim ctx As Integer

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        Me.DbEditedObject = New CLASS_JUNKETS_FLYER
        Me.DbStatus = ENUM_STATUS.STATUS_OK

        If m_flyer_edit Is Nothing Then
          m_flyer_edit = DbEditedObject
          m_flyer_edit.isNew = (ScreenMode = ENUM_SCREEN_MODE.MODE_NEW)
        End If

        m_flyer_edit.Junket.id = InfoFormFlyer.junket_id
        m_flyer_edit.Junket.code = InfoFormFlyer.junket_code
        m_flyer_edit.Junket.name = InfoFormFlyer.junket_name
        m_flyer_edit.Junket.date_to = InfoFormFlyer.junket_expiration_date
        m_flyer_edit.Code = InfoFormFlyer.flyer_code

      Case ENUM_DB_OPERATION.DB_OPERATION_INSERT
        DbStatus = DbEditedObject.DB_Insert(ctx)

      Case ENUM_DB_OPERATION.DB_OPERATION_UPDATE
        DbStatus = DbEditedObject.DB_Update(ctx)

      Case ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE

        DbReadObject = DbEditedObject.Duplicate()

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

#End Region ' Overrides

#Region " Private Methods "

#Region " Table Flags "

  ''' <summary>
  ''' Fill flags on grid
  ''' </summary>
  ''' <param name="FlagsGrid"></param>
  ''' <param name="bShowAutomaticFlags"></param>
  ''' <remarks></remarks>
  Private Sub FillFlagsGrid(ByRef FlagsGrid As uc_grid, ByVal bShowAutomaticFlags As Boolean)

    Dim _idx_all_flags As Integer
    Dim _all_flags As DataTable = Nothing
    Dim _view As DataView
    Dim _sql_tx As SqlClient.SqlTransaction = Nothing
    Dim _flag_color As Color
    Dim _rows() As DataRow
    Dim _list_rows() As DataRow
    Dim _row_flag_auto As DataRow

    ' Get all flags
    If Not GUI_BeginSQLTransaction(_sql_tx) Then
      FlagsGrid.Clear()
      Exit Sub
    Else
      CLASS_FLAG.GetAllFlags(_sql_tx, _all_flags, bShowAutomaticFlags)
      Call GUI_EndSQLTransaction(_sql_tx, False)
    End If

    'Remove automatic flags type no flyer
    _list_rows = _all_flags.Select(String.Format(" FL_TYPE = {1} AND FL_RELATED_TYPE <> {1} ", Int32.Parse(AccountFlag.FlagTypes.Automatic), Int32.Parse(AccountFlag.RelatedTypes.FLYER)))

    For Each _row As DataRow In _list_rows
      _all_flags.Rows.Remove(_row)
    Next

    'Remove automatic flags type flyer except the related type
    _list_rows = _all_flags.Select(String.Format(" FL_TYPE = {0} and FL_RELATED_ID <> {1} ", Int32.Parse(AccountFlag.FlagTypes.Automatic), m_flyer_read.Id))

    For Each _row As DataRow In _list_rows
      _all_flags.Rows.Remove(_row)
    Next

    _list_rows = _all_flags.Select(String.Format(" FL_TYPE = {0} ", Int32.Parse(AccountFlag.FlagTypes.Automatic)))

    ' Add flag count to all flags
    _all_flags.Columns.Add("PF_FLAG_COUNT", Type.GetType("System.Int32"))

    Dim primaryKey(1) As DataColumn
    primaryKey(0) = _all_flags.Columns("FL_FLAG_ID")
    _all_flags.PrimaryKey = primaryKey

    For Each _flag As CLASS_JUNKETS_FLYER.TYPE_FLAG In m_flyer_read.Flags
      _rows = _all_flags.Select(String.Format("FL_FLAG_ID = '{0}'", _flag.flag_id))
      If _rows.Length > 0 AndAlso _flag.count > 0 Then
        _rows(0)("PF_FLAG_COUNT") = _flag.count
      End If
    Next

    ' Get active flags or flags with count > 0
    _view = _all_flags.DefaultView()
    ' free resources
    _all_flags.Dispose()

    _view.RowFilter = "      ( FL_STATUS = " & CLASS_FLAG.STATUS.ENABLED & _
                      " AND  ( FL_EXPIRATION_TYPE <> " & FLAG_EXPIRATION_TYPE.DATETIME & _
                      " OR   ( FL_EXPIRATION_TYPE = " & FLAG_EXPIRATION_TYPE.DATETIME & _
                      " AND FL_EXPIRATION_DATE > '" & GUI_FormatDate(GUI_GetDateTime(), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS) & "' )))" & _
                      " OR   ( PF_FLAG_COUNT > 0 ) "

    '_view.Sort = "CONVERT(PF_FLAG_COUNT,'System.Int32') DESC, FL_NAME"
    _view.Sort = "FL_TYPE DESC, PF_FLAG_COUNT DESC, FL_NAME"

    ' Change column name
    _all_flags.Columns("FL_FLAG_ID").ColumnName = "PF_FLAG_ID"

    'Add automatic flag if not exist
    If _list_rows.Length = 0 Then
      _row_flag_auto = _all_flags.NewRow
      _row_flag_auto("PF_FLAG_ID") = -1
      _row_flag_auto("FL_NAME") = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8197) 'Automatic flag
      _row_flag_auto("FL_COLOR") = -1
      _row_flag_auto("FL_TYPE") = 1
      _row_flag_auto("PF_FLAG_COUNT") = 1

      _all_flags.Rows.Add(_row_flag_auto)
    End If

    ' Save filter flags
    _all_flags = _view.ToTable()
    ' free resources
    _view.Dispose()
    ' Remove irrelevant data
    _all_flags.Columns.Remove("FL_STATUS")
    _all_flags.Columns.Remove("FL_EXPIRATION_TYPE")
    _all_flags.Columns.Remove("FL_EXPIRATION_DATE")

    ' Result - _all_flags datatable
    '-------------------------------------------------'
    ' PF_FLAG_ID | FL_NAME | FL_COLOR | PF_FLAG_COUNT '
    '-------------------------------------------------'

    ' Init Grid
    For _idx_all_flags = 0 To _all_flags.Rows.Count - 1
      FlagsGrid.AddRow()

      ' Promotion_Flag_Id
      FlagsGrid.Cell(_idx_all_flags, GRID_FLAG_IDX).Value = _all_flags.Rows(_idx_all_flags).Item("PF_FLAG_ID").ToString

      ' Flag Color
      FlagsGrid.Cell(_idx_all_flags, GRID_FLAG_ID_COLOR).Value = _all_flags.Rows(_idx_all_flags).Item("FL_COLOR")
      _flag_color = Color.FromArgb(_all_flags.Rows(_idx_all_flags).Item("FL_COLOR"))
      ' CHAPUZA para pintar la celda de color negro
      If (_flag_color.A = Color.Black.A And _
          _flag_color.R = Color.Black.R And _
          _flag_color.G = Color.Black.G And _
          _flag_color.B = Color.Black.B) Then
        _flag_color = Color.FromArgb(_flag_color.A, _flag_color.R, _flag_color.G, _flag_color.B + 1)
      End If
      FlagsGrid.Cell(_idx_all_flags, GRID_FLAG_COLOR).BackColor = _flag_color

      ' Flag Name
      FlagsGrid.Cell(_idx_all_flags, GRID_FLAG_NAME).Value = _all_flags.Rows(_idx_all_flags).Item("FL_NAME").ToString
      FlagsGrid.Cell(_idx_all_flags, GRID_FLAG_TYPE).Value = _all_flags.Rows(_idx_all_flags).Item("FL_TYPE").ToString

      'Flag Count
      If (String.IsNullOrEmpty(_all_flags.Rows(_idx_all_flags).Item("PF_FLAG_COUNT").ToString)) Then
        FlagsGrid.Cell(_idx_all_flags, GRID_FLAG_COUNT).Value = ""
        If FlagsGrid.Enabled Then
          FlagsGrid.Cell(_idx_all_flags, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
        Else
          FlagsGrid.Cell(_idx_all_flags, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED_DISABLED
        End If
      Else
        FlagsGrid.Cell(_idx_all_flags, GRID_FLAG_COUNT).Value = GUI_FormatNumber(_all_flags.Rows(_idx_all_flags).Item("PF_FLAG_COUNT"), 0)
        If FlagsGrid.Enabled Then
          FlagsGrid.Cell(_idx_all_flags, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
        Else
          FlagsGrid.Cell(_idx_all_flags, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
        End If
      End If

      If _all_flags.Rows(_idx_all_flags).Item("FL_TYPE") = AccountFlag.FlagTypes.Automatic Then
        FlagsGrid.Cell(_idx_all_flags, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
        FlagsGrid.Row(_idx_all_flags).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_01)
      End If

    Next

  End Sub ' FillFlagsGrid

  ''' <summary>
  ''' Init grid flags
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub InitializeFlags()

    Call Me.dg_flags.Init(GRID_SELECT_FLAG_COLUMNS, GRID_SELECT_FLAG_HEADER_ROWS, True)

    ' FLAG_ID ( NOT VISIBLE )
    With Me.dg_flags.Column(GRID_FLAG_IDX)
      .Width = 0
      .IsColumnPrintable = False
    End With

    ' FLAG CHECKED
    With Me.dg_flags.Column(GRID_FLAG_CHECKED)
      .Header.Text = ""
      .WidthFixed = 300
      .Fixed = True
      .Editable = True
      .EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
      .HighLightWhenSelected = False
    End With

    ' FLAG COLOR
    With Me.dg_flags.Column(GRID_FLAG_COLOR)
      .Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286) 'Bandera
      .WidthFixed = 200
      .HighLightWhenSelected = False
    End With

    ' COLUMN_FLAG_NAME
    With Me.dg_flags.Column(GRID_FLAG_NAME)
      .Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286) 'Bandera
      .Width = 2750
      .Editable = False
      .HighLightWhenSelected = False
    End With

    ' COLUMN_FLAG_COUNT
    With Me.dg_flags.Column(GRID_FLAG_COUNT)
      .Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1414) 'Otorgadas
      .WidthFixed = 1300
      .Fixed = True
      .Editable = True
      .EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
      .EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, LEN_FLAG_AWARDED_DIGITS)
    End With

    ' COLUMN_FLAG_ID_COLOR
    With Me.dg_flags.Column(GRID_FLAG_ID_COLOR)
      .Width = 0
      .IsColumnPrintable = False
    End With

    ' GRID_FLAG_TYPE
    With Me.dg_flags.Column(GRID_FLAG_TYPE)
      .Width = 0
      .IsColumnPrintable = False
    End With

  End Sub ' InitializeFlags

  ''' <summary>
  ''' Get flags form grid
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GetFlagsGrid()

    Dim _row As Integer
    Dim _flag As CLASS_JUNKETS_FLYER.TYPE_FLAG

    For _row = 0 To dg_flags.NumRows() - 1

      _flag = m_flyer_edit.Flags.Find(Function(_flg) _flg.flag_id = dg_flags.Cell(_row, GRID_FLAG_IDX).Value)

      If Not _flag Is Nothing Then

        If dg_flags.Cell(_row, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
          _flag.count = GUI_ParseNumber(dg_flags.Cell(_row, GRID_FLAG_COUNT).Value)
        Else
          _flag.count = 0
        End If

      End If

    Next

  End Sub ' GetFlagsGrid

#End Region ' Table Flags

#End Region 'Private Methods

#Region " Events "

  ''' <summary>
  ''' Open promotions selection form for select a promotion linked with flyer
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub bt_select_Click(sender As Object, e As EventArgs) Handles bt_select.Click

    Dim _frm As frm_promotions_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_promotions_sel()

    _frm.IsSelectionMode = True

    Call _frm.ShowDialog(Me)

    If Not _frm.PromotionSelected Is Nothing Then
      m_promotion = _frm.PromotionSelected
      ef_promotion_name.Value = m_promotion.name
    End If

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub ' bt_select_Click

  ''' <summary>
  ''' Before edit check if the automatic flag, if it is stop the edition
  ''' </summary>
  ''' <param name="Row"></param>
  ''' <param name="Column"></param>
  ''' <param name="EditionCanceled"></param>
  ''' <remarks></remarks>
  Private Sub dg_flags_BeforeStartEditionEvent(Row As Integer, Column As Integer, ByRef EditionCanceled As Boolean) Handles dg_flags.BeforeStartEditionEvent

    If dg_flags.Cell(Row, GRID_FLAG_TYPE).Value <> 0 Then
      EditionCanceled = True
    End If

  End Sub ' dg_flags_BeforeStartEditionEvent

  ''' <summary>
  ''' Event of change state of checkbox of grid
  ''' </summary>
  ''' <param name="Row"></param>
  ''' <param name="Column"></param>
  ''' <remarks></remarks>
  Private Sub dg_flags_CellDataChangedEvent(Row As Integer, Column As Integer) Handles dg_flags.CellDataChangedEvent

    If dg_flags.Redraw = False Then
      Return
    End If

    If Column = GRID_FLAG_COUNT Then
      If String.IsNullOrEmpty(dg_flags.Cell(Row, GRID_FLAG_COUNT).Value) Then
        dg_flags.Cell(Row, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
      Else
        dg_flags.Cell(Row, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
      End If
    End If

  End Sub ' dg_flags_CellDataChangedEvent

  ''' <summary>
  ''' Event button for asign a promotion
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub bt_remove_promotion_Click(sender As Object, e As EventArgs) Handles bt_remove_promotion.Click

    m_promotion = Nothing

    If ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT Then
      m_flyer_edit.Promotion = New CLASS_JUNKETS_FLYER.TYPE_PROMOTION
    End If

    ef_promotion_name.Value = String.Empty

  End Sub ' bt_remove_promotion_Click

#End Region ' Events

End Class