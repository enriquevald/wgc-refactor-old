<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_booking_terminal_config
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_booking_terminal_config))
    Me.grp_terminal_minutes = New System.Windows.Forms.GroupBox()
    Me.lbl_terminals = New System.Windows.Forms.Label()
    Me.lbl_time = New System.Windows.Forms.Label()
    Me.lbl_levels = New System.Windows.Forms.Label()
    Me.txt_coin_level_04 = New GUI_Controls.uc_entry_field()
    Me.txt_coin_level_03 = New GUI_Controls.uc_entry_field()
    Me.txt_coin_level_02 = New GUI_Controls.uc_entry_field()
    Me.txt_coin_level_01 = New GUI_Controls.uc_entry_field()
    Me.chk_enabled_level_04 = New System.Windows.Forms.CheckBox()
    Me.chk_enabled_level_03 = New System.Windows.Forms.CheckBox()
    Me.chk_enabled_level_02 = New System.Windows.Forms.CheckBox()
    Me.chk_enabled_level_01 = New System.Windows.Forms.CheckBox()
    Me.lbl_coinin = New System.Windows.Forms.Label()
    Me.cmb_minutes_level_04 = New System.Windows.Forms.ComboBox()
    Me.cmb_minutes_level_03 = New System.Windows.Forms.ComboBox()
    Me.cmb_minutes_level_02 = New System.Windows.Forms.ComboBox()
    Me.cmb_minutes_level_01 = New System.Windows.Forms.ComboBox()
    Me.lbl04_minutes = New System.Windows.Forms.Label()
    Me.lbl03_minutes = New System.Windows.Forms.Label()
    Me.lbl02_minutes = New System.Windows.Forms.Label()
    Me.lbl01_minutes = New System.Windows.Forms.Label()
    Me.pnl_terminals = New System.Windows.Forms.Panel()
    Me.uc_terminals_group_filter = New GUI_Controls.uc_terminals_group_filter()
    Me.uc_terminals_group_level_01 = New GUI_Controls.uc_terminals_group_filter()
    Me.uc_terminals_group_level_02 = New GUI_Controls.uc_terminals_group_filter()
    Me.uc_terminals_group_level_03 = New GUI_Controls.uc_terminals_group_filter()
    Me.uc_terminals_group_level_04 = New GUI_Controls.uc_terminals_group_filter()
    Me.chk_reservation = New System.Windows.Forms.CheckBox()
    Me.lbl_reservation_time_legend = New System.Windows.Forms.Label()
    Me.lbl_coin_in_legend = New System.Windows.Forms.Label()
    Me.TextBox_interval = New System.Windows.Forms.TextBox()
    Me.cb_another = New System.Windows.Forms.CheckBox()
    Me.label_interval = New System.Windows.Forms.Label()
    Me.panel_data.SuspendLayout()
    Me.grp_terminal_minutes.SuspendLayout()
    Me.pnl_terminals.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.panel_data.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.panel_data.Controls.Add(Me.label_interval)
    Me.panel_data.Controls.Add(Me.cb_another)
    Me.panel_data.Controls.Add(Me.TextBox_interval)
    Me.panel_data.Controls.Add(Me.uc_terminals_group_level_01)
    Me.panel_data.Controls.Add(Me.uc_terminals_group_level_02)
    Me.panel_data.Controls.Add(Me.uc_terminals_group_level_03)
    Me.panel_data.Controls.Add(Me.uc_terminals_group_level_04)
    Me.panel_data.Controls.Add(Me.chk_reservation)
    Me.panel_data.Controls.Add(Me.grp_terminal_minutes)
    Me.panel_data.Controls.Add(Me.lbl_coin_in_legend)
    Me.panel_data.Controls.Add(Me.lbl_reservation_time_legend)
    Me.panel_data.Dock = System.Windows.Forms.DockStyle.Top
    Me.panel_data.Size = New System.Drawing.Size(866, 379)
    '
    'grp_terminal_minutes
    '
    Me.grp_terminal_minutes.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.grp_terminal_minutes.Controls.Add(Me.lbl_terminals)
    Me.grp_terminal_minutes.Controls.Add(Me.lbl_time)
    Me.grp_terminal_minutes.Controls.Add(Me.lbl_levels)
    Me.grp_terminal_minutes.Controls.Add(Me.txt_coin_level_04)
    Me.grp_terminal_minutes.Controls.Add(Me.txt_coin_level_03)
    Me.grp_terminal_minutes.Controls.Add(Me.txt_coin_level_02)
    Me.grp_terminal_minutes.Controls.Add(Me.txt_coin_level_01)
    Me.grp_terminal_minutes.Controls.Add(Me.chk_enabled_level_04)
    Me.grp_terminal_minutes.Controls.Add(Me.chk_enabled_level_03)
    Me.grp_terminal_minutes.Controls.Add(Me.chk_enabled_level_02)
    Me.grp_terminal_minutes.Controls.Add(Me.chk_enabled_level_01)
    Me.grp_terminal_minutes.Controls.Add(Me.lbl_coinin)
    Me.grp_terminal_minutes.Controls.Add(Me.cmb_minutes_level_04)
    Me.grp_terminal_minutes.Controls.Add(Me.cmb_minutes_level_03)
    Me.grp_terminal_minutes.Controls.Add(Me.cmb_minutes_level_02)
    Me.grp_terminal_minutes.Controls.Add(Me.cmb_minutes_level_01)
    Me.grp_terminal_minutes.Controls.Add(Me.lbl04_minutes)
    Me.grp_terminal_minutes.Controls.Add(Me.lbl03_minutes)
    Me.grp_terminal_minutes.Controls.Add(Me.lbl02_minutes)
    Me.grp_terminal_minutes.Controls.Add(Me.lbl01_minutes)
    Me.grp_terminal_minutes.Location = New System.Drawing.Point(19, 79)
    Me.grp_terminal_minutes.Name = "grp_terminal_minutes"
    Me.grp_terminal_minutes.Size = New System.Drawing.Size(834, 176)
    Me.grp_terminal_minutes.TabIndex = 2
    Me.grp_terminal_minutes.TabStop = False
    Me.grp_terminal_minutes.Text = "M�ximo de minutos a reservar por nivel"
    '
    'lbl_terminals
    '
    Me.lbl_terminals.AutoSize = True
    Me.lbl_terminals.Location = New System.Drawing.Point(343, 27)
    Me.lbl_terminals.Name = "lbl_terminals"
    Me.lbl_terminals.Size = New System.Drawing.Size(69, 13)
    Me.lbl_terminals.TabIndex = 37
    Me.lbl_terminals.Text = "xTerminals"
    '
    'lbl_time
    '
    Me.lbl_time.AutoSize = True
    Me.lbl_time.Location = New System.Drawing.Point(203, 27)
    Me.lbl_time.Name = "lbl_time"
    Me.lbl_time.Size = New System.Drawing.Size(110, 13)
    Me.lbl_time.TabIndex = 34
    Me.lbl_time.Text = "xReservationTime"
    '
    'lbl_levels
    '
    Me.lbl_levels.AutoSize = True
    Me.lbl_levels.Location = New System.Drawing.Point(23, 27)
    Me.lbl_levels.Name = "lbl_levels"
    Me.lbl_levels.Size = New System.Drawing.Size(44, 13)
    Me.lbl_levels.TabIndex = 33
    Me.lbl_levels.Text = "xLevel"
    '
    'txt_coin_level_04
    '
    Me.txt_coin_level_04.DoubleValue = 0.0R
    Me.txt_coin_level_04.IntegerValue = 0
    Me.txt_coin_level_04.IsReadOnly = False
    Me.txt_coin_level_04.Location = New System.Drawing.Point(696, 128)
    Me.txt_coin_level_04.Name = "txt_coin_level_04"
    Me.txt_coin_level_04.PlaceHolder = Nothing
    Me.txt_coin_level_04.ShortcutsEnabled = True
    Me.txt_coin_level_04.Size = New System.Drawing.Size(87, 24)
    Me.txt_coin_level_04.SufixText = "Sufix Text"
    Me.txt_coin_level_04.SufixTextVisible = True
    Me.txt_coin_level_04.TabIndex = 19
    Me.txt_coin_level_04.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.txt_coin_level_04.TextValue = ""
    Me.txt_coin_level_04.TextWidth = 0
    Me.txt_coin_level_04.Value = ""
    Me.txt_coin_level_04.ValueForeColor = System.Drawing.Color.Blue
    '
    'txt_coin_level_03
    '
    Me.txt_coin_level_03.DoubleValue = 0.0R
    Me.txt_coin_level_03.IntegerValue = 0
    Me.txt_coin_level_03.IsReadOnly = False
    Me.txt_coin_level_03.Location = New System.Drawing.Point(696, 101)
    Me.txt_coin_level_03.Name = "txt_coin_level_03"
    Me.txt_coin_level_03.PlaceHolder = Nothing
    Me.txt_coin_level_03.ShortcutsEnabled = True
    Me.txt_coin_level_03.Size = New System.Drawing.Size(87, 24)
    Me.txt_coin_level_03.SufixText = "Sufix Text"
    Me.txt_coin_level_03.SufixTextVisible = True
    Me.txt_coin_level_03.TabIndex = 15
    Me.txt_coin_level_03.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.txt_coin_level_03.TextValue = ""
    Me.txt_coin_level_03.TextWidth = 0
    Me.txt_coin_level_03.Value = ""
    Me.txt_coin_level_03.ValueForeColor = System.Drawing.Color.Blue
    '
    'txt_coin_level_02
    '
    Me.txt_coin_level_02.DoubleValue = 0.0R
    Me.txt_coin_level_02.IntegerValue = 0
    Me.txt_coin_level_02.IsReadOnly = False
    Me.txt_coin_level_02.Location = New System.Drawing.Point(696, 74)
    Me.txt_coin_level_02.Name = "txt_coin_level_02"
    Me.txt_coin_level_02.PlaceHolder = Nothing
    Me.txt_coin_level_02.ShortcutsEnabled = True
    Me.txt_coin_level_02.Size = New System.Drawing.Size(87, 24)
    Me.txt_coin_level_02.SufixText = "Sufix Text"
    Me.txt_coin_level_02.SufixTextVisible = True
    Me.txt_coin_level_02.TabIndex = 11
    Me.txt_coin_level_02.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.txt_coin_level_02.TextValue = ""
    Me.txt_coin_level_02.TextWidth = 0
    Me.txt_coin_level_02.Value = ""
    Me.txt_coin_level_02.ValueForeColor = System.Drawing.Color.Blue
    '
    'txt_coin_level_01
    '
    Me.txt_coin_level_01.DoubleValue = 0.0R
    Me.txt_coin_level_01.IntegerValue = 0
    Me.txt_coin_level_01.IsReadOnly = False
    Me.txt_coin_level_01.Location = New System.Drawing.Point(696, 46)
    Me.txt_coin_level_01.Name = "txt_coin_level_01"
    Me.txt_coin_level_01.PlaceHolder = Nothing
    Me.txt_coin_level_01.ShortcutsEnabled = True
    Me.txt_coin_level_01.Size = New System.Drawing.Size(87, 24)
    Me.txt_coin_level_01.SufixText = "Sufix Text"
    Me.txt_coin_level_01.SufixTextVisible = True
    Me.txt_coin_level_01.TabIndex = 7
    Me.txt_coin_level_01.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.txt_coin_level_01.TextValue = ""
    Me.txt_coin_level_01.TextWidth = 0
    Me.txt_coin_level_01.Value = ""
    Me.txt_coin_level_01.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_enabled_level_04
    '
    Me.chk_enabled_level_04.AutoSize = True
    Me.chk_enabled_level_04.Location = New System.Drawing.Point(23, 131)
    Me.chk_enabled_level_04.Name = "chk_enabled_level_04"
    Me.chk_enabled_level_04.RightToLeft = System.Windows.Forms.RightToLeft.No
    Me.chk_enabled_level_04.Size = New System.Drawing.Size(77, 17)
    Me.chk_enabled_level_04.TabIndex = 16
    Me.chk_enabled_level_04.Text = "xLevel04"
    Me.chk_enabled_level_04.UseVisualStyleBackColor = True
    '
    'chk_enabled_level_03
    '
    Me.chk_enabled_level_03.AutoSize = True
    Me.chk_enabled_level_03.Location = New System.Drawing.Point(23, 104)
    Me.chk_enabled_level_03.Name = "chk_enabled_level_03"
    Me.chk_enabled_level_03.RightToLeft = System.Windows.Forms.RightToLeft.No
    Me.chk_enabled_level_03.Size = New System.Drawing.Size(77, 17)
    Me.chk_enabled_level_03.TabIndex = 12
    Me.chk_enabled_level_03.Text = "xLevel03"
    Me.chk_enabled_level_03.UseVisualStyleBackColor = True
    '
    'chk_enabled_level_02
    '
    Me.chk_enabled_level_02.AutoSize = True
    Me.chk_enabled_level_02.Location = New System.Drawing.Point(23, 77)
    Me.chk_enabled_level_02.Name = "chk_enabled_level_02"
    Me.chk_enabled_level_02.RightToLeft = System.Windows.Forms.RightToLeft.No
    Me.chk_enabled_level_02.Size = New System.Drawing.Size(77, 17)
    Me.chk_enabled_level_02.TabIndex = 8
    Me.chk_enabled_level_02.Text = "xLevel02"
    Me.chk_enabled_level_02.UseVisualStyleBackColor = True
    '
    'chk_enabled_level_01
    '
    Me.chk_enabled_level_01.AutoSize = True
    Me.chk_enabled_level_01.Location = New System.Drawing.Point(23, 50)
    Me.chk_enabled_level_01.Name = "chk_enabled_level_01"
    Me.chk_enabled_level_01.RightToLeft = System.Windows.Forms.RightToLeft.No
    Me.chk_enabled_level_01.Size = New System.Drawing.Size(77, 17)
    Me.chk_enabled_level_01.TabIndex = 4
    Me.chk_enabled_level_01.Text = "xLevel01"
    Me.chk_enabled_level_01.UseVisualStyleBackColor = True
    '
    'lbl_coinin
    '
    Me.lbl_coinin.AutoSize = True
    Me.lbl_coinin.Location = New System.Drawing.Point(696, 27)
    Me.lbl_coinin.Name = "lbl_coinin"
    Me.lbl_coinin.Size = New System.Drawing.Size(52, 13)
    Me.lbl_coinin.TabIndex = 27
    Me.lbl_coinin.Text = "xCoinIn"
    '
    'cmb_minutes_level_04
    '
    Me.cmb_minutes_level_04.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.cmb_minutes_level_04.FormattingEnabled = True
    Me.cmb_minutes_level_04.Location = New System.Drawing.Point(204, 131)
    Me.cmb_minutes_level_04.Name = "cmb_minutes_level_04"
    Me.cmb_minutes_level_04.Size = New System.Drawing.Size(57, 21)
    Me.cmb_minutes_level_04.TabIndex = 17
    '
    'cmb_minutes_level_03
    '
    Me.cmb_minutes_level_03.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.cmb_minutes_level_03.FormattingEnabled = True
    Me.cmb_minutes_level_03.Location = New System.Drawing.Point(204, 104)
    Me.cmb_minutes_level_03.Name = "cmb_minutes_level_03"
    Me.cmb_minutes_level_03.Size = New System.Drawing.Size(57, 21)
    Me.cmb_minutes_level_03.TabIndex = 13
    '
    'cmb_minutes_level_02
    '
    Me.cmb_minutes_level_02.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.cmb_minutes_level_02.FormattingEnabled = True
    Me.cmb_minutes_level_02.Location = New System.Drawing.Point(204, 77)
    Me.cmb_minutes_level_02.Name = "cmb_minutes_level_02"
    Me.cmb_minutes_level_02.Size = New System.Drawing.Size(57, 21)
    Me.cmb_minutes_level_02.TabIndex = 9
    '
    'cmb_minutes_level_01
    '
    Me.cmb_minutes_level_01.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.cmb_minutes_level_01.FormattingEnabled = True
    Me.cmb_minutes_level_01.Location = New System.Drawing.Point(204, 50)
    Me.cmb_minutes_level_01.Name = "cmb_minutes_level_01"
    Me.cmb_minutes_level_01.Size = New System.Drawing.Size(57, 21)
    Me.cmb_minutes_level_01.TabIndex = 5
    '
    'lbl04_minutes
    '
    Me.lbl04_minutes.AutoSize = True
    Me.lbl04_minutes.Location = New System.Drawing.Point(267, 134)
    Me.lbl04_minutes.Name = "lbl04_minutes"
    Me.lbl04_minutes.Size = New System.Drawing.Size(59, 13)
    Me.lbl04_minutes.TabIndex = 0
    Me.lbl04_minutes.Text = "xminutes"
    '
    'lbl03_minutes
    '
    Me.lbl03_minutes.AutoSize = True
    Me.lbl03_minutes.Location = New System.Drawing.Point(267, 107)
    Me.lbl03_minutes.Name = "lbl03_minutes"
    Me.lbl03_minutes.Size = New System.Drawing.Size(59, 13)
    Me.lbl03_minutes.TabIndex = 0
    Me.lbl03_minutes.Text = "xminutes"
    '
    'lbl02_minutes
    '
    Me.lbl02_minutes.AutoSize = True
    Me.lbl02_minutes.Location = New System.Drawing.Point(267, 80)
    Me.lbl02_minutes.Name = "lbl02_minutes"
    Me.lbl02_minutes.Size = New System.Drawing.Size(59, 13)
    Me.lbl02_minutes.TabIndex = 0
    Me.lbl02_minutes.Text = "xminutes"
    '
    'lbl01_minutes
    '
    Me.lbl01_minutes.AutoSize = True
    Me.lbl01_minutes.Location = New System.Drawing.Point(267, 53)
    Me.lbl01_minutes.Name = "lbl01_minutes"
    Me.lbl01_minutes.Size = New System.Drawing.Size(59, 13)
    Me.lbl01_minutes.TabIndex = 0
    Me.lbl01_minutes.Text = "xminutes"
    '
    'pnl_terminals
    '
    Me.pnl_terminals.Controls.Add(Me.uc_terminals_group_filter)
    Me.pnl_terminals.Dock = System.Windows.Forms.DockStyle.Fill
    Me.pnl_terminals.Location = New System.Drawing.Point(4, 4)
    Me.pnl_terminals.Name = "pnl_terminals"
    Me.pnl_terminals.Size = New System.Drawing.Size(866, 382)
    Me.pnl_terminals.TabIndex = 25
    '
    'uc_terminals_group_filter
    '
    Me.uc_terminals_group_filter.ForbiddenGroups = CType(resources.GetObject("uc_terminals_group_filter.ForbiddenGroups"), Microsoft.VisualBasic.Collection)
    Me.uc_terminals_group_filter.HeightOnExpanded = 300
    Me.uc_terminals_group_filter.Location = New System.Drawing.Point(19, 12)
    Me.uc_terminals_group_filter.Name = "uc_terminals_group_filter"
    Me.uc_terminals_group_filter.SelectedIndex = 0
    Me.uc_terminals_group_filter.SetDisplayMode = GUI_Controls.uc_terminals_group_filter.DisplayMode.CollapsedWithButtons
    Me.uc_terminals_group_filter.SetInitMode = GUI_Controls.uc_terminals_group_filter.InitMode.NormalMode
    Me.uc_terminals_group_filter.ShowGroupBoxLine = True
    Me.uc_terminals_group_filter.ShowGroups = Nothing
    Me.uc_terminals_group_filter.ShowNodeAll = True
    Me.uc_terminals_group_filter.Size = New System.Drawing.Size(466, 54)
    Me.uc_terminals_group_filter.TabIndex = 10
    Me.uc_terminals_group_filter.ThreeStateCheckMode = True
    Me.uc_terminals_group_filter.Visible = False
    '
    'uc_terminals_group_level_01
    '
    Me.uc_terminals_group_level_01.ForbiddenGroups = CType(resources.GetObject("uc_terminals_group_level_01.ForbiddenGroups"), Microsoft.VisualBasic.Collection)
    Me.uc_terminals_group_level_01.HeightOnExpanded = 300
    Me.uc_terminals_group_level_01.Location = New System.Drawing.Point(362, 85)
    Me.uc_terminals_group_level_01.Name = "uc_terminals_group_level_01"
    Me.uc_terminals_group_level_01.SelectedIndex = 0
    Me.uc_terminals_group_level_01.SetDisplayMode = GUI_Controls.uc_terminals_group_filter.DisplayMode.CollapsedWithButtons
    Me.uc_terminals_group_level_01.SetInitMode = GUI_Controls.uc_terminals_group_filter.InitMode.NormalMode
    Me.uc_terminals_group_level_01.ShowGroupBoxLine = False
    Me.uc_terminals_group_level_01.ShowGroups = Nothing
    Me.uc_terminals_group_level_01.ShowNodeAll = True
    Me.uc_terminals_group_level_01.Size = New System.Drawing.Size(320, 28)
    Me.uc_terminals_group_level_01.TabIndex = 6
    Me.uc_terminals_group_level_01.ThreeStateCheckMode = True
    '
    'uc_terminals_group_level_02
    '
    Me.uc_terminals_group_level_02.ForbiddenGroups = CType(resources.GetObject("uc_terminals_group_level_02.ForbiddenGroups"), Microsoft.VisualBasic.Collection)
    Me.uc_terminals_group_level_02.HeightOnExpanded = 300
    Me.uc_terminals_group_level_02.Location = New System.Drawing.Point(362, 113)
    Me.uc_terminals_group_level_02.Name = "uc_terminals_group_level_02"
    Me.uc_terminals_group_level_02.SelectedIndex = 0
    Me.uc_terminals_group_level_02.SetDisplayMode = GUI_Controls.uc_terminals_group_filter.DisplayMode.CollapsedWithButtons
    Me.uc_terminals_group_level_02.SetInitMode = GUI_Controls.uc_terminals_group_filter.InitMode.NormalMode
    Me.uc_terminals_group_level_02.ShowGroupBoxLine = False
    Me.uc_terminals_group_level_02.ShowGroups = Nothing
    Me.uc_terminals_group_level_02.ShowNodeAll = True
    Me.uc_terminals_group_level_02.Size = New System.Drawing.Size(320, 28)
    Me.uc_terminals_group_level_02.TabIndex = 10
    Me.uc_terminals_group_level_02.ThreeStateCheckMode = True
    '
    'uc_terminals_group_level_03
    '
    Me.uc_terminals_group_level_03.ForbiddenGroups = CType(resources.GetObject("uc_terminals_group_level_03.ForbiddenGroups"), Microsoft.VisualBasic.Collection)
    Me.uc_terminals_group_level_03.HeightOnExpanded = 300
    Me.uc_terminals_group_level_03.Location = New System.Drawing.Point(362, 140)
    Me.uc_terminals_group_level_03.Name = "uc_terminals_group_level_03"
    Me.uc_terminals_group_level_03.SelectedIndex = 0
    Me.uc_terminals_group_level_03.SetDisplayMode = GUI_Controls.uc_terminals_group_filter.DisplayMode.CollapsedWithButtons
    Me.uc_terminals_group_level_03.SetInitMode = GUI_Controls.uc_terminals_group_filter.InitMode.NormalMode
    Me.uc_terminals_group_level_03.ShowGroupBoxLine = False
    Me.uc_terminals_group_level_03.ShowGroups = Nothing
    Me.uc_terminals_group_level_03.ShowNodeAll = True
    Me.uc_terminals_group_level_03.Size = New System.Drawing.Size(320, 26)
    Me.uc_terminals_group_level_03.TabIndex = 14
    Me.uc_terminals_group_level_03.ThreeStateCheckMode = True
    '
    'uc_terminals_group_level_04
    '
    Me.uc_terminals_group_level_04.ForbiddenGroups = CType(resources.GetObject("uc_terminals_group_level_04.ForbiddenGroups"), Microsoft.VisualBasic.Collection)
    Me.uc_terminals_group_level_04.HeightOnExpanded = 300
    Me.uc_terminals_group_level_04.Location = New System.Drawing.Point(362, 167)
    Me.uc_terminals_group_level_04.Name = "uc_terminals_group_level_04"
    Me.uc_terminals_group_level_04.SelectedIndex = 0
    Me.uc_terminals_group_level_04.SetDisplayMode = GUI_Controls.uc_terminals_group_filter.DisplayMode.CollapsedWithButtons
    Me.uc_terminals_group_level_04.SetInitMode = GUI_Controls.uc_terminals_group_filter.InitMode.NormalMode
    Me.uc_terminals_group_level_04.ShowGroupBoxLine = False
    Me.uc_terminals_group_level_04.ShowGroups = Nothing
    Me.uc_terminals_group_level_04.ShowNodeAll = True
    Me.uc_terminals_group_level_04.Size = New System.Drawing.Size(320, 30)
    Me.uc_terminals_group_level_04.TabIndex = 18
    Me.uc_terminals_group_level_04.ThreeStateCheckMode = True
    '
    'chk_reservation
    '
    Me.chk_reservation.AutoSize = True
    Me.chk_reservation.Location = New System.Drawing.Point(25, 16)
    Me.chk_reservation.Name = "chk_reservation"
    Me.chk_reservation.Size = New System.Drawing.Size(101, 17)
    Me.chk_reservation.TabIndex = 1
    Me.chk_reservation.Text = "xReservation"
    Me.chk_reservation.UseVisualStyleBackColor = True
    '
    'lbl_reservation_time_legend
    '
    Me.lbl_reservation_time_legend.Font = New System.Drawing.Font("Verdana", 7.25!)
    Me.lbl_reservation_time_legend.ForeColor = System.Drawing.Color.Navy
    Me.lbl_reservation_time_legend.Location = New System.Drawing.Point(17, 258)
    Me.lbl_reservation_time_legend.Name = "lbl_reservation_time_legend"
    Me.lbl_reservation_time_legend.Size = New System.Drawing.Size(836, 17)
    Me.lbl_reservation_time_legend.TabIndex = 34
    Me.lbl_reservation_time_legend.Text = "xTime: Maximum time the machine will remain reserved, after that it will be opera" & _
    "tive again."
    '
    'lbl_coin_in_legend
    '
    Me.lbl_coin_in_legend.Font = New System.Drawing.Font("Verdana", 7.25!)
    Me.lbl_coin_in_legend.ForeColor = System.Drawing.Color.Navy
    Me.lbl_coin_in_legend.Location = New System.Drawing.Point(17, 275)
    Me.lbl_coin_in_legend.Name = "lbl_coin_in_legend"
    Me.lbl_coin_in_legend.Size = New System.Drawing.Size(836, 17)
    Me.lbl_coin_in_legend.TabIndex = 35
    Me.lbl_coin_in_legend.Text = "xCoin-in: Minimum amount necessary to allow machine reservation."
    '
    'TextBox_interval
    '
    Me.TextBox_interval.Location = New System.Drawing.Point(385, 47)
    Me.TextBox_interval.Name = "TextBox_interval"
    Me.TextBox_interval.Size = New System.Drawing.Size(100, 21)
    Me.TextBox_interval.TabIndex = 3
    '
    'cb_another
    '
    Me.cb_another.AutoSize = True
    Me.cb_another.Location = New System.Drawing.Point(159, 16)
    Me.cb_another.Name = "cb_another"
    Me.cb_another.Size = New System.Drawing.Size(91, 17)
    Me.cb_another.TabIndex = 37
    Me.cb_another.Text = "CheckBox1"
    Me.cb_another.UseVisualStyleBackColor = True
    '
    'label_interval
    '
    Me.label_interval.AutoSize = True
    Me.label_interval.Location = New System.Drawing.Point(22, 50)
    Me.label_interval.Name = "label_interval"
    Me.label_interval.Size = New System.Drawing.Size(44, 13)
    Me.label_interval.TabIndex = 38
    Me.label_interval.Text = "Label1"
    '
    'frm_booking_terminal_config
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(962, 390)
    Me.Controls.Add(Me.pnl_terminals)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_booking_terminal_config"
    Me.Text = "frm_booking_terminal_config"
    Me.Controls.SetChildIndex(Me.pnl_terminals, 0)
    Me.Controls.SetChildIndex(Me.panel_data, 0)
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.grp_terminal_minutes.ResumeLayout(False)
    Me.grp_terminal_minutes.PerformLayout()
    Me.pnl_terminals.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents grp_terminal_minutes As System.Windows.Forms.GroupBox
  Friend WithEvents lbl04_minutes As System.Windows.Forms.Label
  Friend WithEvents lbl03_minutes As System.Windows.Forms.Label
  Friend WithEvents lbl02_minutes As System.Windows.Forms.Label
  Friend WithEvents lbl01_minutes As System.Windows.Forms.Label
  Friend WithEvents pnl_terminals As System.Windows.Forms.Panel
  Friend WithEvents uc_terminals_group_filter As GUI_Controls.uc_terminals_group_filter
  Friend WithEvents cmb_minutes_level_04 As System.Windows.Forms.ComboBox
  Friend WithEvents cmb_minutes_level_03 As System.Windows.Forms.ComboBox
  Friend WithEvents cmb_minutes_level_02 As System.Windows.Forms.ComboBox
  Friend WithEvents cmb_minutes_level_01 As System.Windows.Forms.ComboBox
  Friend WithEvents lbl_coinin As System.Windows.Forms.Label
  Friend WithEvents chk_enabled_level_04 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_enabled_level_03 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_enabled_level_02 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_enabled_level_01 As System.Windows.Forms.CheckBox
  Friend WithEvents uc_terminals_group_level_01 As GUI_Controls.uc_terminals_group_filter
  Friend WithEvents uc_terminals_group_level_02 As GUI_Controls.uc_terminals_group_filter
  Friend WithEvents uc_terminals_group_level_04 As GUI_Controls.uc_terminals_group_filter
  Friend WithEvents uc_terminals_group_level_03 As GUI_Controls.uc_terminals_group_filter
  Friend WithEvents chk_reservation As System.Windows.Forms.CheckBox
  Friend WithEvents txt_coin_level_01 As GUI_Controls.uc_entry_field
  Friend WithEvents txt_coin_level_02 As GUI_Controls.uc_entry_field
  Friend WithEvents txt_coin_level_04 As GUI_Controls.uc_entry_field
  Friend WithEvents txt_coin_level_03 As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_terminals As System.Windows.Forms.Label
  Friend WithEvents lbl_time As System.Windows.Forms.Label
  Friend WithEvents lbl_levels As System.Windows.Forms.Label
  Friend WithEvents lbl_coin_in_legend As System.Windows.Forms.Label
  Friend WithEvents lbl_reservation_time_legend As System.Windows.Forms.Label
  Friend WithEvents label_interval As System.Windows.Forms.Label
  Friend WithEvents cb_another As System.Windows.Forms.CheckBox
  Friend WithEvents TextBox_interval As System.Windows.Forms.TextBox

  'Friend WithEvents gb_promotion_taxes As System.Windows.Forms.GroupBox
  'Friend WithEvents gb_prize_taxes As System.Windows.Forms.GroupBox
  'Friend WithEvents gb_taxes As System.Windows.Forms.GroupBox

End Class
