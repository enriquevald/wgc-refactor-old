'-------------------------------------------------------------------
' Copyright � 2011 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_capacity
' DESCRIPTION : Capacity by Provider/Site-Day and Day-Provider/Site
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------- ------ -----------------------------------------------
' 25-JUL-2011 RCI    Initial version: Capacity by provider-hour, total by provider and general totals
' 18-JAN-2012 TJG    Capacity by Provider/Site-Day and Day-Provider/Site
' 27-APR-2012 JCM    Updated parameter of uc_provider.Init() to the properly new type (Boolean to Enum UC_FILTER_TYPE.ONLY_PROVIDERS)
' 08-JUN-2012 JAB    DoEvents run each second.
' 11-JUN-2012 JAB    DoEvents run each second (control "doEvents" bugs).
' 30-AUG-2012 RCI    Added explicit use of index IX_ps_started.
' 17-SEP-2012 JAB    Timeout Exception.
' 25-APR-2013 RBG    Fixed Bug #746: It makes no sense to work in this form with multiselection
' 17-MAY-2013 DHA    Fixed Bug #786: Wrong formatting "WHERE" filter, due to apostrophe
' 27-MAY-2013 RBG    Fixed Bug #801: The bug #786 is not completely resolved
' 16-JAN-2015 SGB    Fixed Bug WIG-1916: Format column date in export excel
' 28-JAN-2015 DCS    In TITO mode anonymous accounts don't displayed
' 01-MAR-2016 RAB    Bug 8596:Aforo por proveedor: no se actualiza el campo "an�nimos"
' 26-JUL-2017 ETP    Bug 28988: [WIGOS-3800] Statistics - Capacity by provider is not showing all columns.
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports System.Globalization

Public Class frm_capacity
  Inherits frm_base_sel

#Region " Constants "

  ' Text format columns
  Private Const EXCEL_COLUMN_DATE As Integer = 0

  Private Const SQL_COL_PROVIDER As Integer = 0
  Private Const SQL_COL_DATE As Integer = 1
  Private Const SQL_COL_DAY_ORDER As Integer = 2
  Private Const SQL_COL_VISITS As Integer = 3
  Private Const SQL_COL_VISITS_MEN As Integer = 4
  Private Const SQL_COL_VISITS_WOMEN As Integer = 5
  Private Const SQL_COL_VISITS_ANONYMOUS As Integer = 6

  Private Const GRID_COL_INDEX As Integer = 0
  Private Const GRID_COL_FIRST As Integer = 1
  Private Const GRID_COL_SECOND As Integer = 2
  Private Const GRID_COL_MEN As Integer = 3
  Private Const GRID_COL_WOMEN As Integer = 4
  Private Const GRID_COL_ANONYMOUS As Integer = 5
  Private Const GRID_COL_ALL As Integer = 6
  Private Const GRID_COL_DETAILS As Integer = 7

  Private Const GRID_COLUMNS As Integer = 8
  Private Const GRID_HEADER_ROWS As Integer = 1

#End Region ' Constants

#Region " Members "

  ' Report filters 
  Private m_report_date_from As String
  Private m_report_date_to As String
  Private m_report_terminal As String
  Private m_report_options(0 To 3) As String

  Private m_num_days As Integer
  Private m_by_day As Integer
  Private m_by_provider As Integer
  Private m_grid_col_day As Integer
  Private m_grid_col_provider As Integer
  Private m_from_date As Date
  Private m_to_date As Date
  Private m_include_empty_providers As Boolean
  Private m_include_empty_dates As Boolean
  Private m_show_providers_capacity As Boolean
  Private m_show_site_capacity As Boolean
  Private m_show_average As Boolean

  Private m_grid_overflow As Boolean
  Private m_grid_refresh_step As Integer
  Private m_grid_refresh_pending_rows As Integer

  Private m_color_average As System.Drawing.Color
  Private m_color_average_site As System.Drawing.Color
  Private m_color_capacity_site As System.Drawing.Color

  Private m_is_tito_mode As Boolean

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE : Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CAPACITY
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE : Initialize every form control
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_AUDITOR.GetString(400)    ' 400 "Aforo"

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_AUDITOR.GetString(12)

    ' Details 
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_INFO).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_INFO).Text = GLB_NLS_GUI_AUDITOR.GetString(424)   ' 424 "Detail"
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_INFO).Enabled = False

    ' Date time filter
    Me.uc_dsl.Init(GLB_NLS_GUI_AUDITOR.Id(261), True, False)   ' 261 "Date"
    Me.panel_filter.Size = New Size(Me.panel_filter.Size.Width, Me.panel_filter.Size.Height - 23)
    Me.Size = New Size(Me.Size.Width, Me.Size.Height - 23)

    ' Providers
    Call Me.uc_pr_list.Init(WSI.Common.Misc.GamingTerminalTypeList(), uc_provider.UC_FILTER_TYPE.ONLY_PROVIDERS)

    ' Options
    Me.gb_options.Text = GLB_NLS_GUI_INVOICING.GetString(369)
    Me.chk_show_providers_capacity.Text = GLB_NLS_GUI_AUDITOR.GetString(407)  ' 407 "Mostrar aforo de proveedores"
    Me.chk_show_site_capacity.Text = GLB_NLS_GUI_AUDITOR.GetString(408)       ' 408 "Mostrar aforo de la sala"
    Me.chk_show_average.Text = GLB_NLS_GUI_AUDITOR.GetString(418)             ' 418 "Mostrar media diaria"
    Me.chk_exclude_empty_providers.Text = GLB_NLS_GUI_AUDITOR.GetString(409)  ' 409 "Excluir proveedores sin actividad"
    Me.opt_by_day.Text = GLB_NLS_GUI_AUDITOR.GetString(416)                   ' 416 "Por d�a y proveedor"
    Me.opt_by_provider.Text = GLB_NLS_GUI_AUDITOR.GetString(417)              ' 417 "Por proveedor y d�a"

    ' Color labels
    m_color_capacity_site = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
    m_color_average = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
    m_color_average_site = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00) ' GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

    Me.lbl_color_site_capacity.BackColor = m_color_capacity_site
    Me.lbl_color_average.BackColor = m_color_average

    ' Labels
    Me.lbl_num_days.Value = GLB_NLS_GUI_AUDITOR.GetString(412, "---")   ' 412 "N�mero de d�as en el per�odo: %1"

    ' Set filter default values
    Call SetDefaultValues()

    m_is_tito_mode = WSI.Common.TITO.Utils.IsTitoMode

    Call GUI_StyleSheet()

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub ' GUI_InitControls

  ' PURPOSE : Initialize all form filters with their default values
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()

  End Sub ' GUI_FilterReset

  ' PURPOSE : Check for consistency values provided for every filter
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted

  Protected Overrides Function GUI_FilterCheck() As Boolean

    Dim _now As Date

    _now = WSI.Common.WGDB.Now

    If Me.uc_dsl.FromDate >= _now Then
      ' 419 "Error en el intervalo de fechas: La fecha inicial no puede ser posterior a la fecha actual."
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(419), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call Me.uc_dsl.Focus()

      Return False
    End If

    If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate >= Me.uc_dsl.ToDate Then
        ' 101 "Error en el intervalo de fechas: la fecha inicial tiene que ser anterior a la final."
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    End If

    If Me.uc_pr_list.SeveralTerminalsChecked _
   And Me.uc_pr_list.GetOnlyProviderIdListSelected() Is Nothing Then
      ' 127 "Debe seleccionar al menos un proveedor."
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(127), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call Me.uc_pr_list.Focus()

      Return False
    End If

    If Not Me.chk_show_providers_capacity.Checked _
   And Not Me.chk_show_site_capacity.Checked _
   And Not Me.chk_show_average.Checked Then
      ' 129 "Debe seleccionar al menos una opci�n entre '%1', '%2' y '%3'."
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(129), ENUM_MB_TYPE.MB_TYPE_WARNING, , , _
                      Me.chk_show_providers_capacity.Text, _
                      Me.chk_show_site_capacity.Text, _
                      Me.chk_show_average.Text)

      Call Me.chk_show_providers_capacity.Focus()

      Return False
    End If

    If Me.chk_show_providers_capacity.Checked Then
      m_by_day = opt_by_day.Checked
      m_by_provider = opt_by_provider.Checked
    Else
      m_by_day = True
      m_by_provider = False
    End If

    ' Columns vary depending on the filter selection
    If m_by_day Then
      m_grid_col_day = GRID_COL_FIRST
      m_grid_col_provider = GRID_COL_SECOND
    Else
      m_grid_col_day = GRID_COL_SECOND
      m_grid_col_provider = GRID_COL_FIRST
    End If

    Call SelectGridColumns()

    Return True

  End Function ' GUI_FilterCheck

  ' PURPOSE : Get the defined Query Type
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS : ENUM_QUERY_TYPE.QUERY_CUSTOM

  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE

    Return ENUM_QUERY_TYPE.QUERY_CUSTOM

  End Function ' GUI_GetQueryType

  ' PURPOSE : Build an SQL query from conditions set in the filters
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - SQL query text ready to send to the database

  Private Function GetSqlQuery() As String

    Dim _str_sql As String

    _str_sql = " DECLARE @from_date AS DATETIME" _
             + " DECLARE @to_date AS DATETIME" _
             + " SET @from_date = " & GUI_FormatDateDB(m_from_date) _
             + " SET @to_date = " & GUI_FormatDateDB(m_to_date)

    ' Dummy SQL Query (SqlQueryDummy) is used to obtain always the same number of DataTables.

    If m_show_providers_capacity Or m_show_average Then
      _str_sql += SqlQueryProvider()
    Else
      _str_sql += SqlQueryDummy()
    End If

    If m_show_site_capacity Or m_show_average Then
      _str_sql += SqlQuerySite()
    Else
      _str_sql += SqlQueryDummy()
    End If

    Return _str_sql

  End Function ' GetSqlQuery

  Private Sub CheckRefreshGrid()

    If m_grid_refresh_pending_rows > m_grid_refresh_step Then

      Me.Grid.Redraw = True
      Call Application.DoEvents()

      ' JAB 12-JUN-2012: DoEvents run each second.
      If Not GUI_DoEvents(Me.Grid) Then
        Exit Sub
      End If

      Windows.Forms.Cursor.Current = Cursors.WaitCursor
      Me.Grid.Redraw = False

      m_grid_refresh_pending_rows = 0

    End If

    If Me.Grid.NumRows >= GUI_MaxRows() Then
      m_grid_overflow = True
    End If

  End Sub   ' CheckRefreshGrid

  ' PURPOSE : Define the ExecuteQuery customized
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_ExecuteQueryCustom()

    Dim _str_sql As String
    Dim _db_trx As WSI.Common.DB_TRX
    Dim _da As SqlDataAdapter
    Dim _ds As DataSet
    Dim _dt_site As DataTable
    Dim _dt_providers As DataTable
    Dim _providers As String()

    _ds = Nothing

    Try
      Me.Grid.Redraw = False

      ' Grid load and refresh
      m_grid_overflow = False
      m_grid_refresh_pending_rows = 0
      m_grid_refresh_step = Math.Max(MAX_RECORDS_FIRST_LOAD, GUI_MaxRows() / 20)
      m_grid_refresh_step = 10

      ' Refresh labels before executing SQL Query.
      Call Application.DoEvents()

      ' Obtain the SqlQuery
      _str_sql = GetSqlQuery()

      If String.IsNullOrEmpty(_str_sql) Then
        Return
      End If

      _da = New SqlDataAdapter(New SqlCommand(_str_sql))
      ' JAB 17-SEP-2012: Timeout Exception.
      _da.SelectCommand.CommandTimeout = 100
      _ds = New DataSet()

      _db_trx = New WSI.Common.DB_TRX()
      _db_trx.Fill(_da, _ds)

      _da.Dispose()
      _db_trx.Dispose()

      'If _ds.Tables.Count < 2 Then
      '  Throw New Exception("Minimum tables has not been retrieved. Tables.Count = " & _ds.Tables.Count)
      'End If

      _dt_site = _ds.Tables(_ds.Tables.Count - 1).Copy()
      _ds.Tables.RemoveAt(_ds.Tables.Count - 1)

      _dt_providers = _ds.Tables(_ds.Tables.Count - 1).Copy()
      _ds.Tables.RemoveAt(_ds.Tables.Count - 1)

      _providers = Me.uc_pr_list.GetOnlyProviderIdListSelected()

      If m_show_providers_capacity Then
        ' Show the capacity detailed by provider 
        '     - Grouped by date-provider
        '     - Grouped by provider-date

        If m_by_day Then

          '   - Grouped by date-provider
          If Not ShowCapacityByDate(_providers, _dt_providers, _dt_site) Then

            ' JAB 11-JUN-2012: DoEvents run each second (control "doEvents" bugs).
            If Me.IsDisposed Then
              Return
            End If

            ' 123 "Se ha producido un error al acceder a la base de datos."
            Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

            Return
          End If

        ElseIf m_by_provider Then

          '   - Grouped by provider-date
          If Not ShowCapacityByProvider(_providers, _dt_providers, _dt_site) Then

            ' JAB 11-JUN-2012: DoEvents run each second (control "doEvents" bugs).
            If Me.IsDisposed Then
              Return
            End If

            ' 123 "Se ha producido un error al acceder a la base de datos."
            Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

            Return
          End If

        End If

      ElseIf m_show_site_capacity Then
        ' Show the site capacity 
        Call ShowCapacityBySite(_dt_site)

        ' JAB 11-JUN-2012: DoEvents run each second (control "doEvents" bugs).
        If Me.IsDisposed Then
          Return
        End If

      End If

      If m_show_average And m_by_day Then
        If Not m_grid_overflow Then
          ' Show capacity daily average
          Call ShowAllCapacityAverage(_providers, _dt_providers, _dt_site)
        End If
      End If

      If m_grid_overflow Then
        ' 111 "The number of results exceeds the maximum allowed.\nOnly %1 will be displayed."
        Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(111), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, , , CStr(GUI_MaxRows()))
      End If

    Catch _ex_sql As SqlException

      If _ex_sql.Number = -2 Then
        ' RCI 30-AUG-2012: Timeout Exception.
        Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(144), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Else
        Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      End If

    Catch ex As Exception
      ' An error has occurred in the query execution
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "Capacity", _
                            "GUI_ExecuteQueryCustom", _
                            ex.Message)

    Finally
      ' JAB 08-JUN-2012: Check if disposed.
      If Not Me.IsDisposed Then
        Me.Grid.Redraw = True
      End If

      Call Application.DoEvents()

      If _ds IsNot Nothing Then
        Call _ds.Clear()
        Call _ds.Dispose()
        _ds = Nothing
      End If

    End Try

  End Sub ' GUI_ExecuteQueryCustom

  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(ExcelData)

    ExcelData.SetColumnFormat(EXCEL_COLUMN_DATE, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.DATE)

  End Sub

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - IdxRow
  '           - DbRow
  '           - IsProvider
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)

  Private Function SetupGridRow(ByVal IdxRow As Integer, _
                                ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW, _
                                ByVal IsProvider As Boolean) As Boolean

    Dim _aux_string As String

    ' Columns
    '   - Details can be browsed?
    '   - Date
    '   - Provider
    '   - Men Visits
    '   - Women Visits
    '   - Anonymous Visits
    '   - Total Visits

    If Not IsProvider Then
      Me.Grid.Row(IdxRow).BackColor = m_color_capacity_site
    End If

    '   - Details can be browsed?
    Me.Grid.Cell(IdxRow, GRID_COL_DETAILS).Value = IIf(IsProvider, "1", "0")

    '   - Date
    If Not DbRow.IsNull(SQL_COL_DATE) Then
      Me.Grid.Cell(IdxRow, m_grid_col_day).Value = GUI_FormatDate(DbRow.Value(SQL_COL_DATE), _
                                                                  ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)
    End If

    '   - Provider
    _aux_string = ""

    If Not IsProvider Then
      _aux_string = GLB_NLS_GUI_AUDITOR.GetString(406)   ' 406 "Toda la sala"
    ElseIf Not DbRow.IsNull(SQL_COL_PROVIDER) Then
      _aux_string = DbRow.Value(SQL_COL_PROVIDER)
    End If

    Me.Grid.Cell(IdxRow, m_grid_col_provider).Value = _aux_string

    '   - Men Visits
    _aux_string = ""
    If Not DbRow.IsNull(SQL_COL_VISITS_MEN) Then
      _aux_string = GUI_FormatNumber(DbRow.Value(SQL_COL_VISITS_MEN), 0)
    End If

    Me.Grid.Cell(IdxRow, GRID_COL_MEN).Value = _aux_string

    '   - Women Visits
    _aux_string = ""
    If Not DbRow.IsNull(SQL_COL_VISITS_WOMEN) Then
      _aux_string = GUI_FormatNumber(DbRow.Value(SQL_COL_VISITS_WOMEN), 0)
    End If

    Me.Grid.Cell(IdxRow, GRID_COL_WOMEN).Value = _aux_string

    '   - Anonymous Visits
    _aux_string = ""
    If Not DbRow.IsNull(SQL_COL_VISITS_ANONYMOUS) Then
      _aux_string = GUI_FormatNumber(DbRow.Value(SQL_COL_VISITS_ANONYMOUS), 0)
    End If

    Me.Grid.Cell(IdxRow, GRID_COL_ANONYMOUS).Value = _aux_string

    '   - Total Visits
    _aux_string = ""
    If Not DbRow.IsNull(SQL_COL_VISITS) Then
      _aux_string = GUI_FormatNumber(DbRow.Value(SQL_COL_VISITS), 0)
    End If

    Me.Grid.Cell(IdxRow, GRID_COL_ALL).Value = _aux_string

    Return True

  End Function ' SetupGridRow

  ' PURPOSE : Set focus to a control when first entering the form
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.uc_dsl

  End Sub ' GUI_SetInitialFocus

#Region " GUI Reports "

  ' PURPOSE : Set proper values for form filters being sent to the report
  '
  '  PARAMS :
  '     - INPUT :
  '           - PrintData
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(261) & " " & GLB_NLS_GUI_AUDITOR.GetString(257), m_report_date_from)  ' 261 "Fecha"
    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(261) & " " & GLB_NLS_GUI_AUDITOR.GetString(258), m_report_date_to)    ' 261 "Fecha"
    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(413), GUI_FormatNumber(m_num_days, 0))  ' 413 "D�as"

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(469), m_report_terminal)        ' 469 "Proveedor"

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(369), m_report_options(0))            ' 369 "Opciones"
    PrintData.SetFilter("", m_report_options(1))
    PrintData.SetFilter("", m_report_options(2))
    PrintData.SetFilter("", m_report_options(3))

    PrintData.FilterHeaderWidth(1) = 1100
    PrintData.FilterValueWidth(1) = 1400
    PrintData.FilterHeaderWidth(2) = 900
    PrintData.FilterValueWidth(2) = 2800
    PrintData.FilterHeaderWidth(3) = 800
    PrintData.FilterValueWidth(3) = 3100

  End Sub ' GUI_ReportFilter

  ' PURPOSE : Set form specific requirements/parameters forthe report
  '
  '  PARAMS :
  '     - INPUT :
  '           - PrintData
  '           - FirstColIndex
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)

    PrintData.Params.Title = GLB_NLS_GUI_AUDITOR.GetString(400)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_PORTRAIT

  End Sub ' GUI_ReportParams

  ' PURPOSE : Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_ReportUpdateFilters()

    Dim _idx As Integer
    Dim _txt_yes As String
    Dim _txt_no As String
    Dim _today As Date
    Dim _tomorrow As Date
    Dim _now As Date

    m_report_date_from = ""
    m_report_date_to = ""
    m_report_terminal = ""

    For _idx = 0 To m_report_options.Length - 1
      m_report_options(_idx) = ""
    Next

    ' Calculate Filters
    '   - From Date
    '   - To Date
    '   - Providers, Site and Daily Average
    '   - Include empty values?

    If Me.chk_show_providers_capacity.Checked Then
      m_by_day = opt_by_day.Checked
      m_by_provider = opt_by_provider.Checked
    Else
      m_by_day = True
      m_by_provider = False
    End If

    _now = WSI.Common.WGDB.Now

    _now = New DateTime(_now.Year, _now.Month, _now.Day, _now.Hour, 0, 0)

    _today = New DateTime(_now.Year, _now.Month, _now.Day, Me.uc_dsl.ClosingTime, 0, 0)
    _tomorrow = _today.AddDays(1)

    '   - From Date
    m_from_date = Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime)

    If m_from_date > _now Then
      m_from_date = _now
    End If

    '   - To Date
    If Me.uc_dsl.ToDateSelected Then
      m_to_date = Me.uc_dsl.ToDate.AddHours(Me.uc_dsl.ClosingTime)

      If m_to_date > _now Then
        m_to_date = _tomorrow
      End If
    Else
      m_to_date = _tomorrow
    End If

    ' Obtain the number of days between FromDate and ToDate.
    m_num_days = Math.Max(m_to_date.Subtract(m_from_date).Days, 1)

    '   - Providers, Site and Daily Average
    m_show_providers_capacity = Me.chk_show_providers_capacity.Checked
    m_show_site_capacity = Me.chk_show_site_capacity.Checked
    ' Force the daily average when selecting "By Provider" filter (= provider-day) in order to have a 
    ' separator line (yellow provider average) between providers
    m_show_average = Me.chk_show_average.Checked

    '   - Include empty values?
    m_include_empty_providers = Not Me.chk_exclude_empty_providers.Checked
    m_include_empty_dates = True

    ' 412 "N�mero de d�as en el per�odo: %1"
    Me.lbl_num_days.Value = GLB_NLS_GUI_AUDITOR.GetString(412, GUI_FormatNumber(m_num_days, 0))

    ' Date
    If Me.uc_dsl.FromDateSelected Then
      m_report_date_from = GUI_FormatDate(Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime), _
                                          ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                          ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    If Me.uc_dsl.ToDateSelected Then
      m_report_date_to = GUI_FormatDate(Me.uc_dsl.ToDate.AddHours(Me.uc_dsl.ClosingTime), _
                                        ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                        ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    ' Providers - Terminals
    m_report_terminal = Me.uc_pr_list.GetTerminalReportAllOrSeveralText()

    ' Options
    '     - Show providers capacity         - By day-provider or By provider-day
    '     - Show site capacity              - Exclude empty provider
    '     - Show daily average     

    ' 479 "Si"
    ' 480 "No"
    _txt_yes = GLB_NLS_GUI_INVOICING.GetString(479)
    _txt_no = GLB_NLS_GUI_INVOICING.GetString(480)

    _idx = 0

    '     - Show providers capacity
    If m_show_providers_capacity Then

      m_report_options(_idx) = chk_show_providers_capacity.Text + ": "

      If m_by_provider Then
        m_report_options(_idx) += Me.opt_by_provider.Text
      ElseIf m_by_day Then
        m_report_options(_idx) += Me.opt_by_day.Text
      End If

      _idx += 1

      ' Exclude empty providers
      m_report_options(_idx) = chk_exclude_empty_providers.Text + ": " + IIf(Not m_include_empty_providers, _txt_yes, _txt_no)
      _idx += 1

    Else

      m_report_options(_idx) = chk_show_providers_capacity.Text + ": " + _txt_no
      _idx += 1

    End If

    '     - Show site capacity
    m_report_options(_idx) = chk_show_site_capacity.Text + ": " + IIf(m_show_site_capacity, _txt_yes, _txt_no)
    _idx += 1

    '     - Show daily average
    m_report_options(_idx) = chk_show_average.Text + ": " + IIf(m_show_average, _txt_yes, _txt_no)
    _idx += 1

  End Sub ' GUI_ReportUpdateFilters

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_INFO _
         , frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        Call ShowCapacityDetails()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)

    End Select

  End Sub ' GUI_ButtonClick


#End Region ' GUI Reports

#End Region  ' Overrides

#Region " Public Functions "

  ' PURPOSE : Opens dialog with default settings for edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  '
  ' RETURNS :
  '
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE : Define all Main Grid Columns 
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      .Sortable = False
      .PanelRightVisible = False

      ' Index
      .Column(GRID_COL_INDEX).Header(0).Text = ""
      .Column(GRID_COL_INDEX).Width = 200
      .Column(GRID_COL_INDEX).HighLightWhenSelected = False
      .Column(GRID_COL_INDEX).IsColumnPrintable = False

      Call SelectGridColumns()

      ' Men
      .Column(GRID_COL_MEN).Header(0).Text = GLB_NLS_GUI_AUDITOR.GetString(401)         ' 401 "Hombres"
      .Column(GRID_COL_MEN).Width = 1100
      .Column(GRID_COL_MEN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Women
      .Column(GRID_COL_WOMEN).Header(0).Text = GLB_NLS_GUI_AUDITOR.GetString(402)       ' 402 "Mujeres"
      .Column(GRID_COL_WOMEN).Width = 1100
      .Column(GRID_COL_WOMEN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Anonymous / Uncarded
      .Column(GRID_COL_ANONYMOUS).Header(0).Text = IIf(m_is_tito_mode, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8554), GLB_NLS_GUI_AUDITOR.GetString(403))
      'RAB 01-MAR-2016
      .Column(GRID_COL_ANONYMOUS).Width = 1100
      .Column(GRID_COL_ANONYMOUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' All
      .Column(GRID_COL_ALL).Header(0).Text = GLB_NLS_GUI_AUDITOR.GetString(263)         ' 263 "Todos"
      .Column(GRID_COL_ALL).Width = 1100
      .Column(GRID_COL_ALL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Details can be browsed?
      .Column(GRID_COL_DETAILS).Header(0).Text = ""
      .Column(GRID_COL_DETAILS).Width = 0
      .Column(GRID_COL_DETAILS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COL_DETAILS).HighLightWhenSelected = False
      .Column(GRID_COL_DETAILS).IsColumnPrintable = False

    End With
  End Sub ' GUI_StyleSheet

  ' PURPOSE : Set default values to filters
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  '
  ' RETURNS :

  Private Sub SetDefaultValues()

    Dim _today_opening As Date

    ' Date filter
    _today_opening = WSI.Common.Misc.TodayOpening()
    Me.uc_dsl.FromDate = _today_opening
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.ToDate = _today_opening.AddDays(1)
    Me.uc_dsl.ToDateSelected = False
    Me.uc_dsl.ClosingTime = _today_opening.Hour

    Me.lbl_num_days.Value = GLB_NLS_GUI_AUDITOR.GetString(412, "---")   ' 412 "N�mero de d�as en el per�odo: %1"

    ' Provider filter
    Call Me.uc_pr_list.SetDefaultValues()

    ' Don't show the providers capacity by default
    Me.chk_show_providers_capacity.Checked = False
    ' Disable chk_show_providers_capacity's dependant controls 
    Me.chk_exclude_empty_providers.Enabled = False
    Me.opt_by_day.Enabled = False
    Me.opt_by_provider.Enabled = False
    ' However we mark the dependant controls in case chk_show_providers_capacity gets enabled
    Me.chk_exclude_empty_providers.Checked = True
    Me.opt_by_day.Checked = True
    Me.opt_by_provider.Checked = False

    Me.chk_show_site_capacity.Checked = True
    Me.chk_show_average.Checked = False

    ' Default initial values
    m_by_day = True
    m_by_provider = False
    m_grid_col_day = GRID_COL_FIRST
    m_grid_col_provider = GRID_COL_SECOND

  End Sub ' SetDefaultValues

  ' PURPOSE : Filter the terminal types and providers
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Function GetSqlWhere(ByVal SelectedProviders As Boolean) As String

    Dim _str_where As String = ""
    Dim _provider As String
    Dim _provider_list As String()
    Dim _first_item As Boolean

    ' Just terminals 
    '   - Types: WIN, T3GS, SAS_HOST
    '   - Belonging to the selected providers

    '   - Types: WIN, T3GS, SAS_HOST
    _str_where += " AND TE_TERMINAL_TYPE IN (" + Me.uc_pr_list.GetTerminalTypes() + ") "

    '   - Belonging to the selected providers
    If SelectedProviders Then
      _provider_list = uc_pr_list.GetOnlyProviderIdListSelected

      If _provider_list.Length > 0 Then
        _first_item = True

        For Each _provider In _provider_list
          If _first_item Then
            _str_where += " AND TE_PROVIDER_ID IN ("

            _first_item = False
          Else
            _str_where += ","
          End If

          _str_where += "'" + _provider.Replace("'", "''") + "'"
        Next

        _str_where += ")"
      End If
    End If

    Return _str_where

  End Function ' GetSqlWhere

  ' PURPOSE : Return the SQL Query to obtain the total capacity.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - String

  Private Function SqlQueryProvider() As String

    Dim _str_sql As String = ""

    _str_sql += " " _
              + "SELECT TE_PROVIDER_ID" _
                   + ", DATEADD(day, DATEDIFF(hour, @from_date, MAX(FirstSessionStarted)) / 24, @from_date) FirstVisit " _
                   + ", DayOrder" _
                   + ", SUM(CASE WHEN (NumPlaySessions > 0) THEN 1 ELSE 0 END) Visits" _
                   + ", SUM(CASE WHEN (HowManyMen > 0) THEN 1 ELSE 0 END)      VisitsMen" _
                   + ", SUM(CASE WHEN (HowManyWomen > 0) THEN 1 ELSE 0 END)    VisitsWomen" _
                   + ", SUM(CASE WHEN (HowManyAnonym > 0) THEN 1 ELSE 0 END)   VisitsAnonym" _
                   + " FROM (" _
                      + " SELECT TE_PROVIDER_ID" _
                            + ", ISNULL(SUM(CASE AC_HOLDER_GENDER WHEN 1 THEN 1 ELSE 0 END), 0)             HowManyMen" _
                            + ", ISNULL(SUM(CASE AC_HOLDER_GENDER WHEN 2 THEN 1 ELSE 0 END), 0)             HowManyWomen" _
                            + ", ISNULL(SUM(CASE ISNULL (AC_HOLDER_LEVEL, 0) WHEN 0 THEN 1 ELSE 0 END), 0) HowManyAnonym" _
                            + ", DATEDIFF(hour, @from_date, PS_STARTED) / 24 AS DayOrder" _
                            + ", MIN(PS_STARTED) FirstSessionStarted" _
                            + ", COUNT(*) NumPlaySessions" _
                        + " FROM PLAY_SESSIONS with(index(IX_ps_started))" _
                            + ", TERMINALS" _
                            + ", ACCOUNTS" _
                       + " WHERE PS_STARTED >= @from_date" _
                         + " AND PS_STARTED <  @to_date" _
                         + " AND PS_TERMINAL_ID = TE_TERMINAL_ID" _
                         + GetSqlWhere(True) _
                         + " AND AC_ACCOUNT_ID  = PS_ACCOUNT_ID" _
                    + " GROUP BY TE_PROVIDER_ID" _
                            + ", PS_ACCOUNT_ID" _
                            + ", DATEDIFF(hour, @from_date, PS_STARTED) / 24" _
                            + ", AC_HOLDER_GENDER" _
                    + ") VisitsPerDayProvider" _
             + " GROUP BY DayOrder" _
                     + ", TE_PROVIDER_ID" _
             + " ORDER BY DayOrder" _
                     + ", TE_PROVIDER_ID"

    Return _str_sql

  End Function ' SqlQueryProvider

  ' PURPOSE : Return the SQL Query to obtain the total capacity.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - String

  Private Function SqlQuerySite() As String

    Dim _str_sql As String = ""

    _str_sql &= " " _
              + "SELECT '' TE_PROVIDER_ID" _
                      + ", DATEADD(day, datediff(hour, @from_date, MAX(FirstSessionStarted)) / 24, @from_date) FirstVisit " _
                      + ", DayOrder" _
                      + ", SUM(CASE WHEN (NumPlaySessions > 0) THEN 1 ELSE 0 END) Visits" _
                      + ", SUM(CASE WHEN (HowManyMen > 0) THEN 1 ELSE 0 END)      VisitsMen" _
                      + ", SUM(CASE WHEN (HowManyWomen > 0) THEN 1 ELSE 0 END)    VisitsWomen" _
                      + ", SUM(CASE WHEN (HowManyAnonym > 0) THEN 1 ELSE 0 END)   VisitsAnonym" _
                      + " FROM (" _
                        + " SELECT ISNULL(SUM(CASE AC_HOLDER_GENDER WHEN 1 THEN 1 ELSE 0 END), 0)             HowManyMen" _
                              + ", ISNULL(SUM(CASE AC_HOLDER_GENDER WHEN 2 THEN 1 ELSE 0 END), 0)             HowManyWomen" _
                              + ", ISNULL(SUM(CASE ISNULL (AC_HOLDER_GENDER, 0) WHEN 0 THEN 1 ELSE 0 END), 0) HowManyAnonym" _
                              + ", DATEDIFF(hour, @from_date, PS_STARTED) / 24 AS DayOrder" _
                              + ", MIN(PS_STARTED) FirstSessionStarted" _
                              + ", COUNT(*) NumPlaySessions" _
                          + " FROM PLAY_SESSIONS with(index(IX_ps_started))" _
                              + ", TERMINALS" _
                              + ", ACCOUNTS" _
                         + " WHERE PS_STARTED >= @from_date" _
                           + " AND PS_STARTED <  @to_date" _
                           + " AND PS_TERMINAL_ID = TE_TERMINAL_ID" _
                           + GetSqlWhere(False) _
                           + " AND AC_ACCOUNT_ID  = PS_ACCOUNT_ID" _
                      + " GROUP BY PS_ACCOUNT_ID" _
                              + ", DATEDIFF(hour, @from_date, PS_STARTED) / 24" _
                              + ", AC_HOLDER_GENDER" _
                       + " ) VisitsPerDaySite" _
                + " GROUP BY DayOrder" _
                + " ORDER BY DayOrder"

    Return _str_sql

  End Function ' SqlQuerySite

  ' PURPOSE : Return the Dummy SQL Query to obtain always the same number of DataTables.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - String

  Private Function SqlQueryDummy() As String

    Dim _str_sql As String

    _str_sql = " SELECT '' TE_PROVIDER_ID " _
                   + ", '' FirstVisit " _
                   + ", 0 Visits" _
                   + ", 0 VisitsMen" _
                   + ", 0 VisitsWomen" _
                   + ", 0 VisitsAnonym" _
                   + ";"

    Return _str_sql

  End Function ' SqlQueryDummy

  ' PURPOSE : Adds the capacity of the item received that can be either a provider or the site
  '
  '  PARAMS :
  '     - INPUT :
  '           - TableCapacity
  '           - Name : Provider name. An empty value indicates that the Site capacity is received
  ' 
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Sub AddCapacityToGrid(ByVal TableCapacity As DataTable, _
                                ByVal Name As String)

    Dim _idx_row As Integer
    Dim _dr As DataRow
    Dim _site As Boolean

    _site = (Name <> "")

    For Each _dr In TableCapacity.Select("", "TE_PROVIDER_ID, DayOrder")

      Call CheckRefreshGrid()

      ' JAB 08-JUN-2012: Check if disposed.
      If m_grid_overflow Or Me.IsDisposed Then
        Return
      End If

      Me.Grid.AddRow()

      m_grid_refresh_pending_rows += 1

      _idx_row = Me.Grid.NumRows - 1

      If _dr.IsNull(SQL_COL_VISITS) Then
        _dr(SQL_COL_VISITS) = 0
        _dr(SQL_COL_VISITS_MEN) = 0
        _dr(SQL_COL_VISITS_WOMEN) = 0
        _dr(SQL_COL_VISITS_ANONYMOUS) = 0
      End If

      SetupGridRow(_idx_row, New CLASS_DB_ROW(_dr), (Name <> ""))
    Next

  End Sub ' AddCapacityToGrid

  ' PURPOSE : Adds the capacity of the item received that can be either a provider or the site
  '
  '  PARAMS :
  '     - INPUT :
  '           - SourceName 
  '           - DailyAverage
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Sub AddDailyAverageToGrid(ByVal SourceName As String, _
                                    ByVal DailyAverage As Double())

    Dim _idx_row As Integer

    Me.Grid.AddRow()
    m_grid_refresh_pending_rows += 1

    _idx_row = Me.Grid.NumRows - 1

    ' Columns
    '   - Date 
    '   - Provider
    '   - Men Visits
    '   - Women Visits
    '   - Anonymous Visits
    '   - Total Visits
    '   - Details can be browsed?

    '   - Date 
    Me.Grid.Cell(_idx_row, m_grid_col_day).Value = GLB_NLS_GUI_AUDITOR.GetString(420)   ' 420 "Promedio Aforo

    '   - Provider
    If SourceName = "" Then
      SourceName = GLB_NLS_GUI_AUDITOR.GetString(406)   ' 406 "Toda la sala"
      Me.Grid.Row(_idx_row).BackColor = m_color_average_site
    Else
      Me.Grid.Row(_idx_row).BackColor = m_color_average
    End If

    Me.Grid.Cell(_idx_row, m_grid_col_provider).Value = SourceName

    '   - Men Visits
    Me.Grid.Cell(_idx_row, GRID_COL_MEN).Value = GUI_FormatNumber(DailyAverage(0), 2)

    '   - Women Visits
    Me.Grid.Cell(_idx_row, GRID_COL_WOMEN).Value = GUI_FormatNumber(DailyAverage(1), 2)

    '   - Anonymous Visits
    Me.Grid.Cell(_idx_row, GRID_COL_ANONYMOUS).Value = GUI_FormatNumber(DailyAverage(2), 2)

    '   - Total Visits
    Me.Grid.Cell(_idx_row, GRID_COL_ALL).Value = GUI_FormatNumber(DailyAverage(3), 2)

    '   - Details can be browsed?
    Me.Grid.Cell(_idx_row, GRID_COL_DETAILS).Value = ""

  End Sub ' AddDailyAverageToGrid

  ' PURPOSE : 
  '
  '  PARAMS :
  '     - INPUT :
  '           - 
  ' 
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Sub ShowCapacityDetails()

    Dim _idx_row As Integer
    Dim _capacity_from As Date
    'Dim _capacity_to As Date
    Dim _capacity_provider As String
    Dim _capacity_terminal_types As String
    Dim _form As frm_capacity_details

    If Me.Grid.NumRows <= 0 Then
      Return
    End If

    ' Search the selected row 
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    ' ICS 17-DEC-2012 Check if there is no row selected
    If _idx_row > Grid.NumRows - 1 Then
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(130), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return
    End If

    If Me.Grid.Cell(_idx_row, GRID_COL_DETAILS).Value = "0" Then
      ' Site
      _capacity_provider = ""

    ElseIf Me.Grid.Cell(_idx_row, GRID_COL_DETAILS).Value = "1" Then
      ' Provider
      _capacity_provider = Me.Grid.Cell(_idx_row, m_grid_col_provider).Value

    Else
      Return

    End If

    _capacity_from = Me.Grid.Cell(_idx_row, m_grid_col_day).Value
    _capacity_from = _capacity_from.AddHours(Me.uc_dsl.ClosingTime)

    _capacity_terminal_types = Me.uc_pr_list.GetTerminalTypes()

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _form = New frm_capacity_details

    _form.ShowEditItem(_capacity_provider, _
                       _capacity_from, _
                       _capacity_terminal_types)

  End Sub ' ShowCapacityDetails

  ' PURPOSE : Insert empty rows to Table and return the Table
  '
  '  PARAMS :
  '      - INPUT :
  '          - Table
  '
  '      - OUTPUT :
  '
  ' RETURNS :
  '      - DataTable

  Private Function InsertEmptyRows(ByVal Table As DataTable) As DataTable

    Dim _new_table As DataTable
    Dim _current_date As DateTime
    Dim _date_filter As String
    Dim _day_order As Integer

    ' Don't care about the sort order because rows are sorted when inserted in the grid (AddCapacityToGrid)   
    _new_table = Table.Copy()

    _current_date = m_from_date
    _day_order = 0
    While _current_date < m_to_date

      ' JAB 12-JUN-2012: DoEvents run each second.
      If Not GUI_DoEvents(Me.Grid) Then
        Return Nothing
      End If

      _date_filter = "DayOrder = " + _day_order.ToString()

      If Table.Select(_date_filter).Length < 1 Then
        InsertEmptySingleRow(_current_date, _day_order, _new_table)
      End If

      ' Step to next day
      _current_date = _current_date.AddDays(1)
      _day_order += 1
    End While

    Return _new_table

  End Function ' InsertEmptyRows

  ' PURPOSE : Adds the provider + site results grouping them by day
  '
  '  PARAMS :
  '      - INPUT :
  '           - ProvidersList : list of the providers
  '           - ProvidersTable
  '
  '      - OUTPUT :
  '
  ' RETURNS : Whether the insertion was executed OK (TRUE) or not (FALSE)

  Private Function ShowCapacityByDate(ByVal ProvidersList As String(), _
                                      ByVal ProvidersTable As DataTable, _
                                      ByVal SiteTable As DataTable) As Boolean

    Dim _dt_providers As DataTable
    Dim _dt_site As DataTable
    Dim _current_date As DateTime
    Dim _visits_in_a_day As DataRow()
    Dim _dr As DataRow
    Dim _provider_name As String
    Dim _date_filter As String
    Dim _day_order As Integer

    Try

      _dt_providers = ProvidersTable.Clone()
      _dt_site = SiteTable.Clone()

      _current_date = m_from_date
      _day_order = 0

      ' Browse the results day by day
      While _current_date < m_to_date

        Call CheckRefreshGrid()

        If m_grid_overflow Then
          Return True
        End If

        _date_filter = "DayOrder = " + _day_order.ToString()

        ' Show the results of a day 
        '     - Selected Providers 
        '     - Site 

        '     - Selected Providers 
        For Each _provider_name In ProvidersList

          _visits_in_a_day = ProvidersTable.Select("TE_PROVIDER_ID = '" + _provider_name.Replace("'", "''") + "' AND " + _date_filter)

          _dt_providers.Clear()
          _dt_providers.TableName = _provider_name

          If _visits_in_a_day.Length = 0 Then
            ' No visits to the provider's terminals on current day
            If m_include_empty_providers Then
              ' Add empty visit for the day-provider
              InsertEmptySingleRow(_current_date, _day_order, _dt_providers)

              ' Add day-provider capacity
              Call AddCapacityToGrid(_dt_providers, _provider_name)

              ' JAB 08-JUN-2012: Check if disposed.
              If Me.IsDisposed Then
                Return False
              End If

            End If
          Else
            ' Add visits for the day-provider
            For Each _dr In _visits_in_a_day
              _dt_providers.ImportRow(_dr)
            Next

            ' Add day-provider capacity
            Call AddCapacityToGrid(_dt_providers, _provider_name)

            ' JAB 11-JUN-2012: Check if disposed.
            If Me.IsDisposed Then
              Return False
            End If

          End If
        Next  ' Selected Providers

        '     - Site 
        If m_show_site_capacity Then
          _visits_in_a_day = SiteTable.Select(_date_filter)

          _dt_site.Clear()
          _dt_site.TableName = ""

          If _visits_in_a_day.Length = 0 Then
            ' No visits to the site on current day
            If m_include_empty_dates Then
              ' Add empty visit for the day-provider
              InsertEmptySingleRow(_current_date, _day_order, _dt_site)

              ' Add site capacity
              Call AddCapacityToGrid(_dt_site, "")

              ' JAB 11-JUN-2012: Check if disposed.
              If Me.IsDisposed Then
                Return False
              End If

            End If
          Else
            ' And visits for the day-site
            For Each _dr In _visits_in_a_day
              _dt_site.ImportRow(_dr)
            Next

            ' Add site capacity
            Call AddCapacityToGrid(_dt_site, "")

            ' JAB 11-JUN-2012: Check if disposed.
            If Me.IsDisposed Then
              Return False
            End If

          End If

        End If

        ' Step to next day
        _current_date = _current_date.AddDays(1)
        _day_order += 1
      End While

      Return True

    Catch ex As Exception
      ' An error has occurred in the query execution
      Call Trace.WriteLine(ex.ToString())

      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, "Capacity", "ShowCapacityByDate", ex.Message)

      Return False

    Finally

    End Try

  End Function ' ShowCapacityByDate

  ' PURPOSE : Calculates the capacity average for a certain provider or for the site
  '           and returns it back as a new row in AverageTable
  '
  '  PARAMS :
  '      - INPUT :
  '           - Visits : list of visits for that provider/site 
  '           - DailyAverage
  '
  '      - OUTPUT :
  '           - AverageTable : DataTable that holds the capacity average
  '
  ' RETURNS : 

  Private Sub CalculateCapacityAverage(ByVal Visits As DataRow(), _
                                       ByRef DailyAverage As Double())

    Dim _dr As DataRow

    DailyAverage(0) = 0
    DailyAverage(1) = 0
    DailyAverage(2) = 0
    DailyAverage(3) = 0

    If Visits.Length > 0 Then
      ' And visits for the day-provider
      For Each _dr In Visits
        DailyAverage(0) += _dr(SQL_COL_VISITS_MEN)
        DailyAverage(1) += _dr(SQL_COL_VISITS_WOMEN)
        DailyAverage(2) += _dr(SQL_COL_VISITS_ANONYMOUS)
        DailyAverage(3) += _dr(SQL_COL_VISITS)
      Next

      DailyAverage(0) /= m_num_days
      DailyAverage(1) /= m_num_days
      DailyAverage(2) /= m_num_days
      DailyAverage(3) /= m_num_days
    End If

  End Sub

  ' PURPOSE : Calculates and shows the daily average of the selected provider
  '
  '  PARAMS :
  '      - INPUT :
  '           - ProviderName : Name of the selected provider
  '           - ProvidersTable
  '
  '      - OUTPUT :
  '
  ' RETURNS : Whether the insertion was executed OK (TRUE) or not (FALSE)

  Private Function ShowProviderCapacityAverage(ByVal ProviderName As String, _
                                               ByVal ProvidersTable As DataTable) As Boolean

    Dim _visits As DataRow()
    Dim _daily_average(0 To 3) As Double

    Try

      ' Calculate the daily average
      '     - Selected Providers 
      '     - Site

      _visits = ProvidersTable.Select("TE_PROVIDER_ID = '" + ProviderName.Replace("'", "''") + "'")

      Call CalculateCapacityAverage(_visits, _daily_average)

      ' Add the calculated capacities to the grid
      Call AddDailyAverageToGrid(ProviderName, _daily_average)

      Return True

    Catch ex As Exception
      ' An error has occurred in the query execution
      Call Trace.WriteLine(ex.ToString())

      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, "Capacity", "ShowProviderCapacityAverage", ex.Message)

      Return False

    Finally

    End Try

  End Function ' ShowProviderCapacityAverage

  ' PURPOSE : Calculates and shows the daily average of the providers and the site
  '
  '  PARAMS :
  '      - INPUT :
  '           - SiteTable : Site Visits 
  '
  '      - OUTPUT :
  '
  ' RETURNS : Whether the insertion was executed OK (TRUE) or not (FALSE)

  Private Function ShowSiteCapacityAverage(ByVal SiteTable As DataTable) As Boolean

    Dim _visits As DataRow()
    Dim _daily_average(0 To 3) As Double

    Try

      ' Calculate the daily average of the site
      _visits = SiteTable.Select()

      Call CalculateCapacityAverage(_visits, _daily_average)

      ' Add the calculated capacities to the grid
      Call AddDailyAverageToGrid("", _daily_average)

      Return True

    Catch ex As Exception
      ' An error has occurred in the query execution
      Call Trace.WriteLine(ex.ToString())

      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, "Capacity", "ShowSiteCapacityAverage", ex.Message)

      Return False

    Finally

    End Try

  End Function ' ShowSiteCapacityAverage

  ' PURPOSE : Calculates and shows the daily average of the providers and the site
  '
  '  PARAMS :
  '      - INPUT :
  '           - ProvidersList : list of providers
  '           - ProvidersTable
  '           - SiteTable : Site Visits
  '
  '      - OUTPUT :
  '
  ' RETURNS : Whether the insertion was executed OK (TRUE) or not (FALSE)

  Private Function ShowAllCapacityAverage(ByVal ProvidersList As String(), _
                                          ByVal ProvidersTable As DataTable, _
                                          ByVal SiteTable As DataTable) As Boolean

    Dim _provider_name As String

    Try

      ' Calculate the daily average
      '     - Selected Providers 
      '     - Site

      '     - Selected Providers 
      For Each _provider_name In ProvidersList
        ' Add the provider's calculated capacities to the grid
        ShowProviderCapacityAverage(_provider_name, ProvidersTable)
      Next

      '     - Site
      ShowSiteCapacityAverage(SiteTable)

      Return True

    Catch ex As Exception
      ' An error has occurred in the query execution
      Call Trace.WriteLine(ex.ToString())

      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, "Capacity", "ShowAllCapacityAverage", ex.Message)

      Return False

    Finally

    End Try

  End Function ' ShowAllCapacityAverage

  ' PURPOSE : Adds the provider + site results grouping them by day
  '
  '  PARAMS :
  '      - INPUT :
  '           - ProvidersList : list of the providers
  '           - ProvidersTable
  '
  '      - OUTPUT :
  '
  ' RETURNS : Whether the insertion was executed OK (TRUE) or not (FALSE)

  Private Function ShowCapacityByProvider(ByVal ProvidersList As String(), _
                                          ByVal ProvidersTable As DataTable, _
                                          ByVal SiteTable As DataTable) As Boolean
    Dim _dt_providers As DataTable
    Dim _provider_visits As DataRow()
    Dim _dr As DataRow
    Dim _provider_name As String
    Dim _show_capacity As Boolean

    Try

      _dt_providers = ProvidersTable.Clone()

      ' Show the results of a day 
      '     - All days for the selected Providers 
      '     - All days for the Site 

      '     - All days for the selected Providers 

      ' Browse the results provider by provider
      For Each _provider_name In ProvidersList

        ' JAB 12-JUN-2012: DoEvents run each second.
        If Not GUI_DoEvents(Me.Grid) Then
          Return True
        End If

        Call CheckRefreshGrid()

        If m_grid_overflow Then
          Return True
        End If

        _provider_visits = ProvidersTable.Select("TE_PROVIDER_ID = '" + _provider_name.Replace("'", "''") + "'")

        _dt_providers.Clear()
        _dt_providers.TableName = _provider_name

        _show_capacity = True

        If _provider_visits.Length = 0 Then
          ' No visits to the provider's terminals in the selected period of time
          If m_include_empty_providers Then
            ' Add empty visits to the provider's terminals for the selected period of time
            _dt_providers = InsertEmptyRows(_dt_providers)
            If _dt_providers Is Nothing Then
              Return True
            End If
          Else
            _show_capacity = False
          End If
        Else
          ' There are visits to the provider's terminals in the selected period of time
          For Each _dr In _provider_visits
            _dt_providers.ImportRow(_dr)
          Next

          If m_include_empty_dates Then
            ' Complete the registered visits to the provider's terminals by adding 
            ' empty visits to the provider's terminals for the selected period of time 
            _dt_providers = InsertEmptyRows(_dt_providers)
            If _dt_providers Is Nothing Then
              Return True
            End If
          End If
        End If

        If _show_capacity Then

          ' Add provider capacity to the grid
          Call AddCapacityToGrid(_dt_providers, _provider_name)

          ' JAB 11-JUN-2012: Check if disposed.
          If Me.IsDisposed Then
            Return False
          End If

        End If

        ' Add the provider capacity if required
        If m_show_average Then
          ' Show the daily average of the provider's capacity 
          Call ShowProviderCapacityAverage(_provider_name, _dt_providers)
        End If

      Next  ' Selected Providers

      '     - All days for the Site 
      If m_show_site_capacity Then
        ' Show the site capacity 
        Call ShowCapacityBySite(SiteTable)

        ' JAB 11-JUN-2012: Check if disposed.
        If Me.IsDisposed Then
          Return False
        End If

        ' Show the site average capacity 
        Call ShowSiteCapacityAverage(SiteTable)
      End If

      Return True

    Catch ex As Exception
      ' An error has occurred in the query execution
      Call Trace.WriteLine(ex.ToString())

      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, "Capacity", "ShowCapacityByProvider", ex.Message)

      Return False

    Finally

    End Try

  End Function ' ShowCapacityByProvider

  ' PURPOSE : Adds the provider + site results grouping them by day
  '
  '  PARAMS :
  '      - INPUT :
  '           - SiteTable : Site Visits
  '
  '      - OUTPUT :
  '
  ' RETURNS : Whether the insertion was executed OK (TRUE) or not (FALSE)

  Private Function ShowCapacityBySite(ByVal SiteTable As DataTable) As Boolean

    Dim _dt_site As DataTable

    Try

      _dt_site = SiteTable.Copy
      _dt_site.TableName = ""

      ' Add site's capacity
      If m_include_empty_dates Then
        _dt_site = InsertEmptyRows(_dt_site)
        _dt_site = InsertEmptyRows(_dt_site)
        If _dt_site Is Nothing Then
          Return True
        End If
      End If

      ' Add site capacity to the grid
      Call AddCapacityToGrid(_dt_site, "")

      ' JAB 11-JUN-2012: Check if disposed.
      If Me.IsDisposed Then
        Return False
      End If

      Return True

    Catch ex As Exception
      ' An error has occurred in the query execution
      Call Trace.WriteLine(ex.ToString())

      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, "Capacity", "ShowCapacityBySite", ex.Message)

      Return False

    Finally

    End Try

  End Function ' ShowCapacityBySite

  ' PURPOSE : Insert an empty row to Table
  '
  '  PARAMS :
  '      - INPUT :
  '           - CurrentDate
  '           - DayOrder
  '           - Table 
  '
  '      - OUTPUT :
  '
  ' RETURNS :

  Private Sub InsertEmptySingleRow(ByVal CurrentDate As Date, _
                                   ByVal DayOrder As Integer, _
                                   ByVal Table As DataTable)

    Dim _row As DataRow

    _row = Table.NewRow()

    _row(SQL_COL_PROVIDER) = Table.TableName
    _row(SQL_COL_DATE) = CurrentDate
    _row(SQL_COL_DAY_ORDER) = DayOrder
    _row(SQL_COL_VISITS) = DBNull.Value
    _row(SQL_COL_VISITS_MEN) = DBNull.Value
    _row(SQL_COL_VISITS_WOMEN) = DBNull.Value
    _row(SQL_COL_VISITS_ANONYMOUS) = DBNull.Value

    Table.Rows.Add(_row)

  End Sub   ' InsertEmptySingleRow

  ' PURPOSE : Select the grid columns depending on the filter selection
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Sub SelectGridColumns()

    With Me.Grid
      ' Day
      .Column(m_grid_col_day).Header(0).Text = GLB_NLS_GUI_AUDITOR.GetString(415)         ' 415 "D�a"
      .Column(m_grid_col_day).Width = 1560
      .Column(m_grid_col_day).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Provider
      .Column(m_grid_col_provider).Header(0).Text = GLB_NLS_GUI_AUDITOR.GetString(404)    ' 404 "Proveedor"
      .Column(m_grid_col_provider).Width = 5200
      .Column(m_grid_col_provider).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    End With

  End Sub ' SelectGridColumns

#End Region  ' Private Functions

#Region " Events "

  Private Sub chk_show_providers_capacity_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_show_providers_capacity.CheckedChanged

    Me.chk_exclude_empty_providers.Enabled = chk_show_providers_capacity.Checked
    Me.opt_by_day.Enabled = chk_show_providers_capacity.Checked
    Me.opt_by_provider.Enabled = chk_show_providers_capacity.Checked

    If Me.chk_show_providers_capacity.Checked And Me.opt_by_provider.Checked Then
      ' Force show average option and don't allow the user to uncheck it
      Me.chk_show_average.Enabled = False
      Me.chk_show_average.Checked = True
    Else
      Me.chk_show_average.Enabled = True
    End If

  End Sub

  Private Sub opt_by_provider_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_by_provider.CheckedChanged

    If Me.opt_by_provider.Checked Then
      ' Force show average option and don't allow the user to uncheck it
      Me.chk_show_average.Enabled = False
      Me.chk_show_average.Checked = True
    Else
      Me.chk_show_average.Enabled = True
    End If

  End Sub

#End Region ' Events

End Class ' frm_capacity
