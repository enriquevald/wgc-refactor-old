<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_collection_file_sel
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_soft_count_date = New System.Windows.Forms.GroupBox()
    Me.opt_daily_cage_session = New System.Windows.Forms.RadioButton()
    Me.opt_collected = New System.Windows.Forms.RadioButton()
    Me.opt_daily_imported = New System.Windows.Forms.RadioButton()
    Me.opt_imported = New System.Windows.Forms.RadioButton()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.gb_soft_count_status = New System.Windows.Forms.GroupBox()
    Me.lbl_collected = New System.Windows.Forms.Label()
    Me.lbl_pending = New System.Windows.Forms.Label()
    Me.chk_soft_count_pending = New System.Windows.Forms.CheckBox()
    Me.chk_soft_count_collected = New System.Windows.Forms.CheckBox()
    Me.ef_soft_count_name = New GUI_Controls.uc_entry_field()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_soft_count_date.SuspendLayout()
    Me.gb_soft_count_status.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.ef_soft_count_name)
    Me.panel_filter.Controls.Add(Me.gb_soft_count_status)
    Me.panel_filter.Controls.Add(Me.gb_soft_count_date)
    Me.panel_filter.Size = New System.Drawing.Size(1226, 181)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_soft_count_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_soft_count_status, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_soft_count_name, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 185)
    Me.panel_data.Size = New System.Drawing.Size(1226, 376)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1220, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1220, 4)
    '
    'gb_soft_count_date
    '
    Me.gb_soft_count_date.Controls.Add(Me.opt_daily_cage_session)
    Me.gb_soft_count_date.Controls.Add(Me.opt_collected)
    Me.gb_soft_count_date.Controls.Add(Me.opt_daily_imported)
    Me.gb_soft_count_date.Controls.Add(Me.opt_imported)
    Me.gb_soft_count_date.Controls.Add(Me.dtp_from)
    Me.gb_soft_count_date.Controls.Add(Me.dtp_to)
    Me.gb_soft_count_date.Location = New System.Drawing.Point(6, 6)
    Me.gb_soft_count_date.Name = "gb_soft_count_date"
    Me.gb_soft_count_date.Size = New System.Drawing.Size(237, 169)
    Me.gb_soft_count_date.TabIndex = 0
    Me.gb_soft_count_date.TabStop = False
    Me.gb_soft_count_date.Text = "xgbDate"
    '
    'opt_daily_cage_session
    '
    Me.opt_daily_cage_session.AutoSize = True
    Me.opt_daily_cage_session.Location = New System.Drawing.Point(7, 89)
    Me.opt_daily_cage_session.Name = "opt_daily_cage_session"
    Me.opt_daily_cage_session.Size = New System.Drawing.Size(135, 17)
    Me.opt_daily_cage_session.TabIndex = 3
    Me.opt_daily_cage_session.Text = "xDailyCageSession"
    Me.opt_daily_cage_session.UseVisualStyleBackColor = True
    '
    'opt_collected
    '
    Me.opt_collected.AutoSize = True
    Me.opt_collected.Location = New System.Drawing.Point(7, 43)
    Me.opt_collected.Name = "opt_collected"
    Me.opt_collected.Size = New System.Drawing.Size(85, 17)
    Me.opt_collected.TabIndex = 1
    Me.opt_collected.Text = "xCollected"
    Me.opt_collected.UseVisualStyleBackColor = True
    '
    'opt_daily_imported
    '
    Me.opt_daily_imported.AutoSize = True
    Me.opt_daily_imported.Location = New System.Drawing.Point(7, 66)
    Me.opt_daily_imported.Name = "opt_daily_imported"
    Me.opt_daily_imported.Size = New System.Drawing.Size(114, 17)
    Me.opt_daily_imported.TabIndex = 2
    Me.opt_daily_imported.Text = "xDailyImported"
    Me.opt_daily_imported.UseVisualStyleBackColor = True
    '
    'opt_imported
    '
    Me.opt_imported.AutoSize = True
    Me.opt_imported.Checked = True
    Me.opt_imported.Location = New System.Drawing.Point(7, 20)
    Me.opt_imported.Name = "opt_imported"
    Me.opt_imported.Size = New System.Drawing.Size(85, 17)
    Me.opt_imported.TabIndex = 0
    Me.opt_imported.TabStop = True
    Me.opt_imported.Text = "xImported"
    Me.opt_imported.UseVisualStyleBackColor = True
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(14, 112)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(217, 25)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 4
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2015, 4, 14, 0, 0, 0, 0)
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(14, 139)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(217, 25)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 5
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2015, 4, 14, 0, 0, 0, 0)
    '
    'gb_soft_count_status
    '
    Me.gb_soft_count_status.Controls.Add(Me.lbl_collected)
    Me.gb_soft_count_status.Controls.Add(Me.lbl_pending)
    Me.gb_soft_count_status.Controls.Add(Me.chk_soft_count_pending)
    Me.gb_soft_count_status.Controls.Add(Me.chk_soft_count_collected)
    Me.gb_soft_count_status.Location = New System.Drawing.Point(249, 94)
    Me.gb_soft_count_status.Name = "gb_soft_count_status"
    Me.gb_soft_count_status.Size = New System.Drawing.Size(243, 81)
    Me.gb_soft_count_status.TabIndex = 2
    Me.gb_soft_count_status.TabStop = False
    Me.gb_soft_count_status.Text = "xSoftCountStatus"
    '
    'lbl_collected
    '
    Me.lbl_collected.Anchor = System.Windows.Forms.AnchorStyles.Right
    Me.lbl_collected.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
    Me.lbl_collected.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_collected.Location = New System.Drawing.Point(216, 49)
    Me.lbl_collected.Name = "lbl_collected"
    Me.lbl_collected.Size = New System.Drawing.Size(16, 16)
    Me.lbl_collected.TabIndex = 114
    '
    'lbl_pending
    '
    Me.lbl_pending.Anchor = System.Windows.Forms.AnchorStyles.Right
    Me.lbl_pending.BackColor = System.Drawing.Color.FromArgb(CType(CType(130, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(255, Byte), Integer))
    Me.lbl_pending.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_pending.Location = New System.Drawing.Point(216, 24)
    Me.lbl_pending.Name = "lbl_pending"
    Me.lbl_pending.Size = New System.Drawing.Size(16, 16)
    Me.lbl_pending.TabIndex = 113
    '
    'chk_soft_count_pending
    '
    Me.chk_soft_count_pending.AutoSize = True
    Me.chk_soft_count_pending.Location = New System.Drawing.Point(19, 24)
    Me.chk_soft_count_pending.Name = "chk_soft_count_pending"
    Me.chk_soft_count_pending.Size = New System.Drawing.Size(78, 17)
    Me.chk_soft_count_pending.TabIndex = 0
    Me.chk_soft_count_pending.Text = "xPending"
    Me.chk_soft_count_pending.UseVisualStyleBackColor = True
    '
    'chk_soft_count_collected
    '
    Me.chk_soft_count_collected.AutoSize = True
    Me.chk_soft_count_collected.Location = New System.Drawing.Point(19, 48)
    Me.chk_soft_count_collected.Name = "chk_soft_count_collected"
    Me.chk_soft_count_collected.Size = New System.Drawing.Size(86, 17)
    Me.chk_soft_count_collected.TabIndex = 1
    Me.chk_soft_count_collected.Text = "xCollected"
    Me.chk_soft_count_collected.UseVisualStyleBackColor = True
    '
    'ef_soft_count_name
    '
    Me.ef_soft_count_name.DoubleValue = 0.0R
    Me.ef_soft_count_name.IntegerValue = 0
    Me.ef_soft_count_name.IsReadOnly = False
    Me.ef_soft_count_name.Location = New System.Drawing.Point(249, 19)
    Me.ef_soft_count_name.Name = "ef_soft_count_name"
    Me.ef_soft_count_name.PlaceHolder = Nothing
    Me.ef_soft_count_name.Size = New System.Drawing.Size(243, 24)
    Me.ef_soft_count_name.SufixText = "Sufix Text"
    Me.ef_soft_count_name.SufixTextVisible = True
    Me.ef_soft_count_name.TabIndex = 1
    Me.ef_soft_count_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_soft_count_name.TextValue = ""
    Me.ef_soft_count_name.TextWidth = 60
    Me.ef_soft_count_name.Value = ""
    Me.ef_soft_count_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'frm_collection_file_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1234, 565)
    Me.Name = "frm_collection_file_sel"
    Me.Text = "frm_collection_file_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_soft_count_date.ResumeLayout(False)
    Me.gb_soft_count_date.PerformLayout()
    Me.gb_soft_count_status.ResumeLayout(False)
    Me.gb_soft_count_status.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_soft_count_date As System.Windows.Forms.GroupBox
  Friend WithEvents opt_daily_imported As System.Windows.Forms.RadioButton
  Friend WithEvents opt_imported As System.Windows.Forms.RadioButton
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents gb_soft_count_status As System.Windows.Forms.GroupBox
  Friend WithEvents chk_soft_count_pending As System.Windows.Forms.CheckBox
  Friend WithEvents chk_soft_count_collected As System.Windows.Forms.CheckBox
  Friend WithEvents ef_soft_count_name As GUI_Controls.uc_entry_field
  Friend WithEvents opt_collected As System.Windows.Forms.RadioButton
  Friend WithEvents opt_daily_cage_session As System.Windows.Forms.RadioButton
  Friend WithEvents lbl_collected As System.Windows.Forms.Label
  Friend WithEvents lbl_pending As System.Windows.Forms.Label
End Class
