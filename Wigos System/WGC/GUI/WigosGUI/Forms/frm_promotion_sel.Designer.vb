<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_promotions_sel
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_promotions_sel))
    Me.gb_init_date = New System.Windows.Forms.GroupBox()
    Me.dtp_init_to = New GUI_Controls.uc_date_picker()
    Me.dtp_init_from = New GUI_Controls.uc_date_picker()
    Me.gb_promo_status = New System.Windows.Forms.GroupBox()
    Me.chk_disabled = New System.Windows.Forms.CheckBox()
    Me.chk_enabled = New System.Windows.Forms.CheckBox()
    Me.chk_active = New System.Windows.Forms.CheckBox()
    Me.ef_promo_name = New GUI_Controls.uc_entry_field()
    Me.gb_promo_credit_type = New System.Windows.Forms.GroupBox()
    Me.chk_credit_type_points = New System.Windows.Forms.CheckBox()
    Me.chk_credit_type_redeem = New System.Windows.Forms.CheckBox()
    Me.chk_credit_type_non_redeem = New System.Windows.Forms.CheckBox()
    Me.cmb_category = New GUI_Controls.uc_combo()
    Me.chk_not_show_system_promotions = New System.Windows.Forms.CheckBox()
    Me.chk_only_vip = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_init_date.SuspendLayout()
    Me.gb_promo_status.SuspendLayout()
    Me.gb_promo_credit_type.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.chk_only_vip)
    Me.panel_filter.Controls.Add(Me.chk_not_show_system_promotions)
    Me.panel_filter.Controls.Add(Me.cmb_category)
    Me.panel_filter.Controls.Add(Me.gb_promo_credit_type)
    Me.panel_filter.Controls.Add(Me.ef_promo_name)
    Me.panel_filter.Controls.Add(Me.gb_promo_status)
    Me.panel_filter.Controls.Add(Me.gb_init_date)
    Me.panel_filter.Size = New System.Drawing.Size(1219, 130)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_init_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_promo_status, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_promo_name, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_promo_credit_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.cmb_category, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_not_show_system_promotions, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_only_vip, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 134)
    Me.panel_data.Size = New System.Drawing.Size(1219, 511)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1213, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1213, 4)
    '
    'gb_init_date
    '
    Me.gb_init_date.Controls.Add(Me.dtp_init_to)
    Me.gb_init_date.Controls.Add(Me.dtp_init_from)
    Me.gb_init_date.Location = New System.Drawing.Point(6, 6)
    Me.gb_init_date.Name = "gb_init_date"
    Me.gb_init_date.Size = New System.Drawing.Size(242, 89)
    Me.gb_init_date.TabIndex = 0
    Me.gb_init_date.TabStop = False
    Me.gb_init_date.Text = "xInitDate"
    '
    'dtp_init_to
    '
    Me.dtp_init_to.Checked = True
    Me.dtp_init_to.IsReadOnly = False
    Me.dtp_init_to.Location = New System.Drawing.Point(6, 50)
    Me.dtp_init_to.Name = "dtp_init_to"
    Me.dtp_init_to.ShowCheckBox = True
    Me.dtp_init_to.ShowUpDown = False
    Me.dtp_init_to.Size = New System.Drawing.Size(230, 25)
    Me.dtp_init_to.SufixText = "Sufix Text"
    Me.dtp_init_to.SufixTextVisible = True
    Me.dtp_init_to.TabIndex = 1
    Me.dtp_init_to.TextWidth = 50
    Me.dtp_init_to.Value = New Date(2010, 5, 10, 0, 0, 0, 0)
    '
    'dtp_init_from
    '
    Me.dtp_init_from.Checked = True
    Me.dtp_init_from.IsReadOnly = False
    Me.dtp_init_from.Location = New System.Drawing.Point(6, 21)
    Me.dtp_init_from.Name = "dtp_init_from"
    Me.dtp_init_from.ShowCheckBox = True
    Me.dtp_init_from.ShowUpDown = False
    Me.dtp_init_from.Size = New System.Drawing.Size(230, 25)
    Me.dtp_init_from.SufixText = "Sufix Text"
    Me.dtp_init_from.SufixTextVisible = True
    Me.dtp_init_from.TabIndex = 0
    Me.dtp_init_from.TextWidth = 50
    Me.dtp_init_from.Value = New Date(2010, 5, 10, 0, 0, 0, 0)
    '
    'gb_promo_status
    '
    Me.gb_promo_status.Controls.Add(Me.chk_disabled)
    Me.gb_promo_status.Controls.Add(Me.chk_enabled)
    Me.gb_promo_status.Controls.Add(Me.chk_active)
    Me.gb_promo_status.Location = New System.Drawing.Point(254, 6)
    Me.gb_promo_status.Name = "gb_promo_status"
    Me.gb_promo_status.Size = New System.Drawing.Size(140, 89)
    Me.gb_promo_status.TabIndex = 1
    Me.gb_promo_status.TabStop = False
    '
    'chk_disabled
    '
    Me.chk_disabled.AutoSize = True
    Me.chk_disabled.Location = New System.Drawing.Point(8, 61)
    Me.chk_disabled.Name = "chk_disabled"
    Me.chk_disabled.Size = New System.Drawing.Size(82, 17)
    Me.chk_disabled.TabIndex = 2
    Me.chk_disabled.Text = "xDisabled"
    Me.chk_disabled.UseVisualStyleBackColor = True
    '
    'chk_enabled
    '
    Me.chk_enabled.AutoSize = True
    Me.chk_enabled.Location = New System.Drawing.Point(8, 39)
    Me.chk_enabled.Name = "chk_enabled"
    Me.chk_enabled.Size = New System.Drawing.Size(78, 17)
    Me.chk_enabled.TabIndex = 1
    Me.chk_enabled.Text = "xEnabled"
    Me.chk_enabled.UseVisualStyleBackColor = True
    '
    'chk_active
    '
    Me.chk_active.AutoSize = True
    Me.chk_active.Location = New System.Drawing.Point(8, 17)
    Me.chk_active.Name = "chk_active"
    Me.chk_active.Size = New System.Drawing.Size(68, 17)
    Me.chk_active.TabIndex = 0
    Me.chk_active.Text = "xActive"
    Me.chk_active.UseVisualStyleBackColor = True
    '
    'ef_promo_name
    '
    Me.ef_promo_name.DoubleValue = 0.0R
    Me.ef_promo_name.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_promo_name.IntegerValue = 0
    Me.ef_promo_name.IsReadOnly = False
    Me.ef_promo_name.Location = New System.Drawing.Point(3, 101)
    Me.ef_promo_name.Name = "ef_promo_name"
    Me.ef_promo_name.PlaceHolder = Nothing
    Me.ef_promo_name.Size = New System.Drawing.Size(281, 25)
    Me.ef_promo_name.SufixText = "Sufix Text"
    Me.ef_promo_name.SufixTextVisible = True
    Me.ef_promo_name.TabIndex = 5
    Me.ef_promo_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_promo_name.TextValue = ""
    Me.ef_promo_name.TextWidth = 60
    Me.ef_promo_name.Value = ""
    Me.ef_promo_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_promo_credit_type
    '
    Me.gb_promo_credit_type.Controls.Add(Me.chk_credit_type_points)
    Me.gb_promo_credit_type.Controls.Add(Me.chk_credit_type_redeem)
    Me.gb_promo_credit_type.Controls.Add(Me.chk_credit_type_non_redeem)
    Me.gb_promo_credit_type.Location = New System.Drawing.Point(400, 6)
    Me.gb_promo_credit_type.Name = "gb_promo_credit_type"
    Me.gb_promo_credit_type.Size = New System.Drawing.Size(175, 89)
    Me.gb_promo_credit_type.TabIndex = 2
    Me.gb_promo_credit_type.TabStop = False
    Me.gb_promo_credit_type.Text = "xPromotionCreditType"
    '
    'chk_credit_type_points
    '
    Me.chk_credit_type_points.AutoSize = True
    Me.chk_credit_type_points.Location = New System.Drawing.Point(19, 61)
    Me.chk_credit_type_points.Name = "chk_credit_type_points"
    Me.chk_credit_type_points.Size = New System.Drawing.Size(67, 17)
    Me.chk_credit_type_points.TabIndex = 2
    Me.chk_credit_type_points.Text = "xPoints"
    Me.chk_credit_type_points.UseVisualStyleBackColor = True
    '
    'chk_credit_type_redeem
    '
    Me.chk_credit_type_redeem.AutoSize = True
    Me.chk_credit_type_redeem.Location = New System.Drawing.Point(19, 39)
    Me.chk_credit_type_redeem.Name = "chk_credit_type_redeem"
    Me.chk_credit_type_redeem.Size = New System.Drawing.Size(111, 17)
    Me.chk_credit_type_redeem.TabIndex = 1
    Me.chk_credit_type_redeem.Text = "xRedeemeable"
    Me.chk_credit_type_redeem.UseVisualStyleBackColor = True
    '
    'chk_credit_type_non_redeem
    '
    Me.chk_credit_type_non_redeem.AutoSize = True
    Me.chk_credit_type_non_redeem.Location = New System.Drawing.Point(19, 19)
    Me.chk_credit_type_non_redeem.Name = "chk_credit_type_non_redeem"
    Me.chk_credit_type_non_redeem.Size = New System.Drawing.Size(130, 17)
    Me.chk_credit_type_non_redeem.TabIndex = 0
    Me.chk_credit_type_non_redeem.Text = "xNon Redeemable"
    Me.chk_credit_type_non_redeem.UseVisualStyleBackColor = True
    '
    'cmb_category
    '
    Me.cmb_category.AllowUnlistedValues = False
    Me.cmb_category.AutoCompleteMode = False
    Me.cmb_category.IsReadOnly = False
    Me.cmb_category.Location = New System.Drawing.Point(297, 102)
    Me.cmb_category.Name = "cmb_category"
    Me.cmb_category.SelectedIndex = -1
    Me.cmb_category.Size = New System.Drawing.Size(254, 24)
    Me.cmb_category.SufixText = "Sufix Text"
    Me.cmb_category.SufixTextVisible = True
    Me.cmb_category.TabIndex = 6
    Me.cmb_category.TextCombo = Nothing
    Me.cmb_category.TextWidth = 79
    '
    'chk_not_show_system_promotions
    '
    Me.chk_not_show_system_promotions.AutoSize = True
    Me.chk_not_show_system_promotions.Location = New System.Drawing.Point(592, 45)
    Me.chk_not_show_system_promotions.Name = "chk_not_show_system_promotions"
    Me.chk_not_show_system_promotions.Size = New System.Drawing.Size(190, 17)
    Me.chk_not_show_system_promotions.TabIndex = 4
    Me.chk_not_show_system_promotions.Text = "xNotShowSystemPromotions"
    Me.chk_not_show_system_promotions.UseVisualStyleBackColor = True
    '
    'chk_only_vip
    '
    Me.chk_only_vip.AutoSize = True
    Me.chk_only_vip.Location = New System.Drawing.Point(592, 23)
    Me.chk_only_vip.Name = "chk_only_vip"
    Me.chk_only_vip.Size = New System.Drawing.Size(83, 17)
    Me.chk_only_vip.TabIndex = 3
    Me.chk_only_vip.Text = "xOnlyVips"
    Me.chk_only_vip.UseVisualStyleBackColor = True
    '
    'frm_promotions_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1227, 649)
    Me.Name = "frm_promotions_sel"
    Me.Text = "frm_promotions"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_init_date.ResumeLayout(False)
    Me.gb_promo_status.ResumeLayout(False)
    Me.gb_promo_status.PerformLayout()
    Me.gb_promo_credit_type.ResumeLayout(False)
    Me.gb_promo_credit_type.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_init_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_init_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_init_from As GUI_Controls.uc_date_picker
  Friend WithEvents gb_promo_status As System.Windows.Forms.GroupBox
  Friend WithEvents chk_active As System.Windows.Forms.CheckBox
  Friend WithEvents ef_promo_name As GUI_Controls.uc_entry_field
  Friend WithEvents chk_disabled As System.Windows.Forms.CheckBox
  Friend WithEvents chk_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents gb_promo_credit_type As System.Windows.Forms.GroupBox
  Friend WithEvents chk_credit_type_points As System.Windows.Forms.CheckBox
  Friend WithEvents chk_credit_type_redeem As System.Windows.Forms.CheckBox
  Friend WithEvents chk_credit_type_non_redeem As System.Windows.Forms.CheckBox
  Friend WithEvents cmb_category As GUI_Controls.uc_combo
  Friend WithEvents chk_not_show_system_promotions As System.Windows.Forms.CheckBox
  Friend WithEvents chk_only_vip As System.Windows.Forms.CheckBox
End Class
