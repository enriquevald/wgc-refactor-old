﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_junkets_flyer_edit
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.gb_details = New System.Windows.Forms.GroupBox()
    Me.ef_num_flyers_created = New GUI_Controls.uc_entry_field()
    Me.chk_reuse = New System.Windows.Forms.CheckBox()
    Me.ef_flyer_code = New GUI_Controls.uc_entry_field()
    Me.chk_print_voucher = New System.Windows.Forms.CheckBox()
    Me.chk_show_pop_up = New System.Windows.Forms.CheckBox()
    Me.gb_promotions = New System.Windows.Forms.GroupBox()
    Me.chk_print_promotional_voucher = New System.Windows.Forms.CheckBox()
    Me.txt_text_promotional = New System.Windows.Forms.TextBox()
    Me.bt_remove_promotion = New System.Windows.Forms.Button()
    Me.ef_promotion_name = New GUI_Controls.uc_entry_field()
    Me.bt_select = New System.Windows.Forms.Button()
    Me.gb_voucher = New System.Windows.Forms.GroupBox()
    Me.ef_voucher_text = New GUI_Controls.uc_entry_field()
    Me.ef_voucher_title = New GUI_Controls.uc_entry_field()
    Me.gb_pop_up = New System.Windows.Forms.GroupBox()
    Me.txt_text_pop_up = New System.Windows.Forms.TextBox()
    Me.gb_comments = New System.Windows.Forms.GroupBox()
    Me.txt_comments = New System.Windows.Forms.TextBox()
    Me.gb_flags = New System.Windows.Forms.GroupBox()
    Me.dg_flags = New GUI_Controls.uc_grid()
    Me.panel_data.SuspendLayout()
    Me.gb_details.SuspendLayout()
    Me.gb_promotions.SuspendLayout()
    Me.gb_voucher.SuspendLayout()
    Me.gb_pop_up.SuspendLayout()
    Me.gb_comments.SuspendLayout()
    Me.gb_flags.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_flags)
    Me.panel_data.Controls.Add(Me.gb_comments)
    Me.panel_data.Controls.Add(Me.gb_pop_up)
    Me.panel_data.Controls.Add(Me.gb_voucher)
    Me.panel_data.Controls.Add(Me.gb_promotions)
    Me.panel_data.Controls.Add(Me.gb_details)
    Me.panel_data.Size = New System.Drawing.Size(858, 521)
    '
    'gb_details
    '
    Me.gb_details.Controls.Add(Me.ef_num_flyers_created)
    Me.gb_details.Controls.Add(Me.chk_reuse)
    Me.gb_details.Controls.Add(Me.ef_flyer_code)
    Me.gb_details.Location = New System.Drawing.Point(13, 3)
    Me.gb_details.Name = "gb_details"
    Me.gb_details.Size = New System.Drawing.Size(488, 85)
    Me.gb_details.TabIndex = 0
    Me.gb_details.TabStop = False
    Me.gb_details.Text = "xDetails"
    '
    'ef_num_flyers_created
    '
    Me.ef_num_flyers_created.DoubleValue = 0.0R
    Me.ef_num_flyers_created.IntegerValue = 0
    Me.ef_num_flyers_created.IsReadOnly = False
    Me.ef_num_flyers_created.Location = New System.Drawing.Point(223, 19)
    Me.ef_num_flyers_created.Name = "ef_num_flyers_created"
    Me.ef_num_flyers_created.PlaceHolder = Nothing
    Me.ef_num_flyers_created.Size = New System.Drawing.Size(259, 25)
    Me.ef_num_flyers_created.SufixText = "Sufix Text"
    Me.ef_num_flyers_created.SufixTextVisible = True
    Me.ef_num_flyers_created.TabIndex = 1
    Me.ef_num_flyers_created.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_num_flyers_created.TextValue = ""
    Me.ef_num_flyers_created.TextWidth = 165
    Me.ef_num_flyers_created.Value = ""
    Me.ef_num_flyers_created.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_reuse
    '
    Me.chk_reuse.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_reuse.Location = New System.Drawing.Point(17, 50)
    Me.chk_reuse.Name = "chk_reuse"
    Me.chk_reuse.RightToLeft = System.Windows.Forms.RightToLeft.Yes
    Me.chk_reuse.Size = New System.Drawing.Size(464, 25)
    Me.chk_reuse.TabIndex = 2
    Me.chk_reuse.Text = "xReUse"
    Me.chk_reuse.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_reuse.UseVisualStyleBackColor = True
    '
    'ef_flyer_code
    '
    Me.ef_flyer_code.DoubleValue = 0.0R
    Me.ef_flyer_code.IntegerValue = 0
    Me.ef_flyer_code.IsReadOnly = False
    Me.ef_flyer_code.Location = New System.Drawing.Point(6, 19)
    Me.ef_flyer_code.Name = "ef_flyer_code"
    Me.ef_flyer_code.PlaceHolder = Nothing
    Me.ef_flyer_code.Size = New System.Drawing.Size(167, 24)
    Me.ef_flyer_code.SufixText = "Sufix Text"
    Me.ef_flyer_code.SufixTextVisible = True
    Me.ef_flyer_code.TabIndex = 0
    Me.ef_flyer_code.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_flyer_code.TextValue = ""
    Me.ef_flyer_code.TextWidth = 50
    Me.ef_flyer_code.Value = ""
    Me.ef_flyer_code.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_print_voucher
    '
    Me.chk_print_voucher.AutoSize = True
    Me.chk_print_voucher.Location = New System.Drawing.Point(17, 17)
    Me.chk_print_voucher.Name = "chk_print_voucher"
    Me.chk_print_voucher.Size = New System.Drawing.Size(105, 17)
    Me.chk_print_voucher.TabIndex = 0
    Me.chk_print_voucher.Text = "xPrintVoucher"
    Me.chk_print_voucher.UseVisualStyleBackColor = True
    '
    'chk_show_pop_up
    '
    Me.chk_show_pop_up.AutoSize = True
    Me.chk_show_pop_up.Location = New System.Drawing.Point(17, 20)
    Me.chk_show_pop_up.Name = "chk_show_pop_up"
    Me.chk_show_pop_up.Size = New System.Drawing.Size(100, 17)
    Me.chk_show_pop_up.TabIndex = 0
    Me.chk_show_pop_up.Text = "xShowPopUp"
    Me.chk_show_pop_up.UseVisualStyleBackColor = True
    '
    'gb_promotions
    '
    Me.gb_promotions.Controls.Add(Me.chk_print_promotional_voucher)
    Me.gb_promotions.Controls.Add(Me.txt_text_promotional)
    Me.gb_promotions.Controls.Add(Me.bt_remove_promotion)
    Me.gb_promotions.Controls.Add(Me.ef_promotion_name)
    Me.gb_promotions.Controls.Add(Me.bt_select)
    Me.gb_promotions.Location = New System.Drawing.Point(505, 3)
    Me.gb_promotions.Name = "gb_promotions"
    Me.gb_promotions.Size = New System.Drawing.Size(341, 244)
    Me.gb_promotions.TabIndex = 3
    Me.gb_promotions.TabStop = False
    Me.gb_promotions.Text = "xPromotions"
    '
    'chk_print_promotional_voucher
    '
    Me.chk_print_promotional_voucher.AutoSize = True
    Me.chk_print_promotional_voucher.Location = New System.Drawing.Point(6, 111)
    Me.chk_print_promotional_voucher.Name = "chk_print_promotional_voucher"
    Me.chk_print_promotional_voucher.Size = New System.Drawing.Size(173, 17)
    Me.chk_print_promotional_voucher.TabIndex = 5
    Me.chk_print_promotional_voucher.Text = "xPrintPromotionalVoucher"
    Me.chk_print_promotional_voucher.UseVisualStyleBackColor = True
    '
    'txt_text_promotional
    '
    Me.txt_text_promotional.Location = New System.Drawing.Point(6, 130)
    Me.txt_text_promotional.Multiline = True
    Me.txt_text_promotional.Name = "txt_text_promotional"
    Me.txt_text_promotional.Size = New System.Drawing.Size(329, 108)
    Me.txt_text_promotional.TabIndex = 4
    '
    'bt_remove_promotion
    '
    Me.bt_remove_promotion.Location = New System.Drawing.Point(117, 52)
    Me.bt_remove_promotion.Name = "bt_remove_promotion"
    Me.bt_remove_promotion.Size = New System.Drawing.Size(106, 23)
    Me.bt_remove_promotion.TabIndex = 1
    Me.bt_remove_promotion.Text = "xRemove"
    Me.bt_remove_promotion.UseVisualStyleBackColor = True
    '
    'ef_promotion_name
    '
    Me.ef_promotion_name.DoubleValue = 0.0R
    Me.ef_promotion_name.IntegerValue = 0
    Me.ef_promotion_name.IsReadOnly = False
    Me.ef_promotion_name.Location = New System.Drawing.Point(6, 19)
    Me.ef_promotion_name.Name = "ef_promotion_name"
    Me.ef_promotion_name.PlaceHolder = Nothing
    Me.ef_promotion_name.Size = New System.Drawing.Size(329, 24)
    Me.ef_promotion_name.SufixText = "Sufix Text"
    Me.ef_promotion_name.SufixTextVisible = True
    Me.ef_promotion_name.TabIndex = 0
    Me.ef_promotion_name.TabStop = False
    Me.ef_promotion_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_promotion_name.TextValue = ""
    Me.ef_promotion_name.Value = ""
    Me.ef_promotion_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'bt_select
    '
    Me.bt_select.Location = New System.Drawing.Point(229, 52)
    Me.bt_select.Name = "bt_select"
    Me.bt_select.Size = New System.Drawing.Size(106, 23)
    Me.bt_select.TabIndex = 2
    Me.bt_select.Text = "xSelect"
    Me.bt_select.UseVisualStyleBackColor = True
    '
    'gb_voucher
    '
    Me.gb_voucher.Controls.Add(Me.ef_voucher_text)
    Me.gb_voucher.Controls.Add(Me.ef_voucher_title)
    Me.gb_voucher.Controls.Add(Me.chk_print_voucher)
    Me.gb_voucher.Location = New System.Drawing.Point(13, 253)
    Me.gb_voucher.Name = "gb_voucher"
    Me.gb_voucher.Size = New System.Drawing.Size(833, 100)
    Me.gb_voucher.TabIndex = 2
    Me.gb_voucher.TabStop = False
    Me.gb_voucher.Text = "xVoucher"
    '
    'ef_voucher_text
    '
    Me.ef_voucher_text.DoubleValue = 0.0R
    Me.ef_voucher_text.IntegerValue = 0
    Me.ef_voucher_text.IsReadOnly = False
    Me.ef_voucher_text.Location = New System.Drawing.Point(4, 66)
    Me.ef_voucher_text.Name = "ef_voucher_text"
    Me.ef_voucher_text.PlaceHolder = Nothing
    Me.ef_voucher_text.Size = New System.Drawing.Size(823, 25)
    Me.ef_voucher_text.SufixText = "Sufix Text"
    Me.ef_voucher_text.SufixTextVisible = True
    Me.ef_voucher_text.TabIndex = 3
    Me.ef_voucher_text.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_voucher_text.TextValue = ""
    Me.ef_voucher_text.TextWidth = 50
    Me.ef_voucher_text.Value = ""
    Me.ef_voucher_text.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_voucher_title
    '
    Me.ef_voucher_title.DoubleValue = 0.0R
    Me.ef_voucher_title.IntegerValue = 0
    Me.ef_voucher_title.IsReadOnly = False
    Me.ef_voucher_title.Location = New System.Drawing.Point(3, 40)
    Me.ef_voucher_title.Name = "ef_voucher_title"
    Me.ef_voucher_title.PlaceHolder = Nothing
    Me.ef_voucher_title.Size = New System.Drawing.Size(824, 25)
    Me.ef_voucher_title.SufixText = "Sufix Text"
    Me.ef_voucher_title.SufixTextVisible = True
    Me.ef_voucher_title.TabIndex = 2
    Me.ef_voucher_title.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_voucher_title.TextValue = ""
    Me.ef_voucher_title.TextWidth = 50
    Me.ef_voucher_title.Value = ""
    Me.ef_voucher_title.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_pop_up
    '
    Me.gb_pop_up.Controls.Add(Me.txt_text_pop_up)
    Me.gb_pop_up.Controls.Add(Me.chk_show_pop_up)
    Me.gb_pop_up.Location = New System.Drawing.Point(13, 94)
    Me.gb_pop_up.Name = "gb_pop_up"
    Me.gb_pop_up.Size = New System.Drawing.Size(488, 153)
    Me.gb_pop_up.TabIndex = 1
    Me.gb_pop_up.TabStop = False
    Me.gb_pop_up.Text = "xPopUp"
    '
    'txt_text_pop_up
    '
    Me.txt_text_pop_up.Location = New System.Drawing.Point(16, 39)
    Me.txt_text_pop_up.Multiline = True
    Me.txt_text_pop_up.Name = "txt_text_pop_up"
    Me.txt_text_pop_up.Size = New System.Drawing.Size(465, 108)
    Me.txt_text_pop_up.TabIndex = 1
    '
    'gb_comments
    '
    Me.gb_comments.Controls.Add(Me.txt_comments)
    Me.gb_comments.Location = New System.Drawing.Point(13, 359)
    Me.gb_comments.Name = "gb_comments"
    Me.gb_comments.Size = New System.Drawing.Size(488, 158)
    Me.gb_comments.TabIndex = 5
    Me.gb_comments.TabStop = False
    Me.gb_comments.Text = "xComments"
    '
    'txt_comments
    '
    Me.txt_comments.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.txt_comments.Location = New System.Drawing.Point(16, 25)
    Me.txt_comments.Multiline = True
    Me.txt_comments.Name = "txt_comments"
    Me.txt_comments.Size = New System.Drawing.Size(466, 127)
    Me.txt_comments.TabIndex = 0
    '
    'gb_flags
    '
    Me.gb_flags.Controls.Add(Me.dg_flags)
    Me.gb_flags.Location = New System.Drawing.Point(505, 359)
    Me.gb_flags.Name = "gb_flags"
    Me.gb_flags.Size = New System.Drawing.Size(341, 158)
    Me.gb_flags.TabIndex = 4
    Me.gb_flags.TabStop = False
    Me.gb_flags.Text = "xFlags"
    '
    'dg_flags
    '
    Me.dg_flags.CurrentCol = -1
    Me.dg_flags.CurrentRow = -1
    Me.dg_flags.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_flags.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_flags.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_flags.Location = New System.Drawing.Point(6, 20)
    Me.dg_flags.Name = "dg_flags"
    Me.dg_flags.PanelRightVisible = False
    Me.dg_flags.Redraw = True
    Me.dg_flags.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_flags.Size = New System.Drawing.Size(329, 132)
    Me.dg_flags.Sortable = False
    Me.dg_flags.SortAscending = True
    Me.dg_flags.SortByCol = 0
    Me.dg_flags.TabIndex = 0
    Me.dg_flags.ToolTipped = True
    Me.dg_flags.TopRow = -2
    Me.dg_flags.WordWrap = False
    '
    'frm_junkets_flyer_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(961, 529)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
    Me.Name = "frm_junkets_flyer_edit"
    Me.Text = "frm_junkets_flyer_edit"
    Me.panel_data.ResumeLayout(False)
    Me.gb_details.ResumeLayout(False)
    Me.gb_promotions.ResumeLayout(False)
    Me.gb_promotions.PerformLayout()
    Me.gb_voucher.ResumeLayout(False)
    Me.gb_voucher.PerformLayout()
    Me.gb_pop_up.ResumeLayout(False)
    Me.gb_pop_up.PerformLayout()
    Me.gb_comments.ResumeLayout(False)
    Me.gb_comments.PerformLayout()
    Me.gb_flags.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_promotions As System.Windows.Forms.GroupBox
  Friend WithEvents gb_details As System.Windows.Forms.GroupBox
  Friend WithEvents chk_show_pop_up As System.Windows.Forms.CheckBox
  Friend WithEvents chk_reuse As System.Windows.Forms.CheckBox
  Friend WithEvents ef_flyer_code As GUI_Controls.uc_entry_field
  Friend WithEvents chk_print_voucher As System.Windows.Forms.CheckBox
  Friend WithEvents bt_select As System.Windows.Forms.Button
  Friend WithEvents gb_pop_up As System.Windows.Forms.GroupBox
  Friend WithEvents gb_voucher As System.Windows.Forms.GroupBox
  Friend WithEvents ef_promotion_name As GUI_Controls.uc_entry_field
  Friend WithEvents ef_num_flyers_created As GUI_Controls.uc_entry_field
  Friend WithEvents gb_comments As System.Windows.Forms.GroupBox
  Friend WithEvents txt_comments As System.Windows.Forms.TextBox
  Friend WithEvents gb_flags As System.Windows.Forms.GroupBox
  Friend WithEvents txt_text_pop_up As System.Windows.Forms.TextBox
  Friend WithEvents dg_flags As GUI_Controls.uc_grid
  Friend WithEvents bt_remove_promotion As System.Windows.Forms.Button
  Friend WithEvents ef_voucher_text As GUI_Controls.uc_entry_field
  Friend WithEvents ef_voucher_title As GUI_Controls.uc_entry_field
  Friend WithEvents chk_print_promotional_voucher As System.Windows.Forms.CheckBox
  Friend WithEvents txt_text_promotional As System.Windows.Forms.TextBox
End Class
