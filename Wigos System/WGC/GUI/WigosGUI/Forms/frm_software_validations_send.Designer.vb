<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_software_validations_send
  Inherits GUI_Controls.frm_base

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container
    Me.lbl_unknown_text = New System.Windows.Forms.Label
    Me.lbl_error_text = New System.Windows.Forms.Label
    Me.lbl_canceled_or_timeout_text = New System.Windows.Forms.Label
    Me.pn_line = New System.Windows.Forms.Panel
    Me.SplitContainer2 = New System.Windows.Forms.SplitContainer
    Me.gd_software_validation = New GUI_Controls.uc_grid
    Me.btn_exit = New GUI_Controls.uc_button
    Me.uc_pr_list = New GUI_Controls.uc_provider
    Me.gb_status = New System.Windows.Forms.GroupBox
    Me.lbl_validation_error_text = New System.Windows.Forms.Label
    Me.lbl_validation_ok_text = New System.Windows.Forms.Label
    Me.lbl_validation_error = New System.Windows.Forms.Label
    Me.lbl_validation_ok = New System.Windows.Forms.Label
    Me.lbl_pending_text = New System.Windows.Forms.Label
    Me.lbl_unknown = New System.Windows.Forms.Label
    Me.lbl_error = New System.Windows.Forms.Label
    Me.lbl_canceled_or_timeout = New System.Windows.Forms.Label
    Me.lbl_pending = New System.Windows.Forms.Label
    Me.btn_clear = New GUI_Controls.uc_button
    Me.btn_validate = New GUI_Controls.uc_button
    Me.panel_filter_buttons = New System.Windows.Forms.Panel
    Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
    Me.tmr_refresh = New System.Windows.Forms.Timer(Me.components)
    Me.SplitContainer2.Panel1.SuspendLayout()
    Me.SplitContainer2.Panel2.SuspendLayout()
    Me.SplitContainer2.SuspendLayout()
    Me.gb_status.SuspendLayout()
    Me.panel_filter_buttons.SuspendLayout()
    Me.SplitContainer1.Panel1.SuspendLayout()
    Me.SplitContainer1.Panel2.SuspendLayout()
    Me.SplitContainer1.SuspendLayout()
    Me.SuspendLayout()
    '
    'lbl_unknown_text
    '
    Me.lbl_unknown_text.AutoSize = True
    Me.lbl_unknown_text.Location = New System.Drawing.Point(28, 107)
    Me.lbl_unknown_text.Name = "lbl_unknown_text"
    Me.lbl_unknown_text.Size = New System.Drawing.Size(66, 13)
    Me.lbl_unknown_text.TabIndex = 117
    Me.lbl_unknown_text.Text = "xUnknown"
    '
    'lbl_error_text
    '
    Me.lbl_error_text.AutoSize = True
    Me.lbl_error_text.Location = New System.Drawing.Point(28, 85)
    Me.lbl_error_text.Name = "lbl_error_text"
    Me.lbl_error_text.Size = New System.Drawing.Size(43, 13)
    Me.lbl_error_text.TabIndex = 116
    Me.lbl_error_text.Text = "xError"
    '
    'lbl_canceled_or_timeout_text
    '
    Me.lbl_canceled_or_timeout_text.AutoSize = True
    Me.lbl_canceled_or_timeout_text.Location = New System.Drawing.Point(28, 129)
    Me.lbl_canceled_or_timeout_text.Name = "lbl_canceled_or_timeout_text"
    Me.lbl_canceled_or_timeout_text.Size = New System.Drawing.Size(127, 13)
    Me.lbl_canceled_or_timeout_text.TabIndex = 115
    Me.lbl_canceled_or_timeout_text.Text = "xCanceledOrTimeout"
    '
    'pn_line
    '
    Me.pn_line.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.pn_line.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.pn_line.Location = New System.Drawing.Point(0, 192)
    Me.pn_line.Name = "pn_line"
    Me.pn_line.Size = New System.Drawing.Size(1101, 4)
    Me.pn_line.TabIndex = 2
    '
    'SplitContainer2
    '
    Me.SplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill
    Me.SplitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel2
    Me.SplitContainer2.Location = New System.Drawing.Point(0, 0)
    Me.SplitContainer2.Name = "SplitContainer2"
    '
    'SplitContainer2.Panel1
    '
    Me.SplitContainer2.Panel1.Controls.Add(Me.gd_software_validation)
    '
    'SplitContainer2.Panel2
    '
    Me.SplitContainer2.Panel2.Controls.Add(Me.btn_exit)
    Me.SplitContainer2.Size = New System.Drawing.Size(1101, 320)
    Me.SplitContainer2.SplitterDistance = 1005
    Me.SplitContainer2.TabIndex = 5
    '
    'gd_software_validation
    '
    Me.gd_software_validation.CurrentCol = -1
    Me.gd_software_validation.CurrentRow = -1
    Me.gd_software_validation.Dock = System.Windows.Forms.DockStyle.Fill
    Me.gd_software_validation.EditableCellBackColor = System.Drawing.Color.Empty
    Me.gd_software_validation.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.gd_software_validation.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.gd_software_validation.Location = New System.Drawing.Point(0, 0)
    Me.gd_software_validation.Name = "gd_software_validation"
    Me.gd_software_validation.PanelRightVisible = True
    Me.gd_software_validation.Redraw = True
    Me.gd_software_validation.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.gd_software_validation.Size = New System.Drawing.Size(1005, 320)
    Me.gd_software_validation.Sortable = False
    Me.gd_software_validation.SortAscending = True
    Me.gd_software_validation.SortByCol = 0
    Me.gd_software_validation.TabIndex = 0
    Me.gd_software_validation.ToolTipped = True
    Me.gd_software_validation.TopRow = -2
    '
    'btn_exit
    '
    Me.btn_exit.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btn_exit.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.btn_exit.Location = New System.Drawing.Point(0, 290)
    Me.btn_exit.Name = "btn_exit"
    Me.btn_exit.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_exit.Size = New System.Drawing.Size(90, 30)
    Me.btn_exit.TabIndex = 0
    Me.btn_exit.ToolTipped = False
    Me.btn_exit.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'uc_pr_list
    '
    Me.uc_pr_list.FilterByCurrency = False
    Me.uc_pr_list.Location = New System.Drawing.Point(3, 3)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(338, 188)
    Me.uc_pr_list.TabIndex = 0
    '
    'gb_status
    '
    Me.gb_status.Controls.Add(Me.lbl_validation_error_text)
    Me.gb_status.Controls.Add(Me.lbl_validation_ok_text)
    Me.gb_status.Controls.Add(Me.lbl_validation_error)
    Me.gb_status.Controls.Add(Me.lbl_validation_ok)
    Me.gb_status.Controls.Add(Me.lbl_unknown_text)
    Me.gb_status.Controls.Add(Me.lbl_error_text)
    Me.gb_status.Controls.Add(Me.lbl_canceled_or_timeout_text)
    Me.gb_status.Controls.Add(Me.lbl_pending_text)
    Me.gb_status.Controls.Add(Me.lbl_unknown)
    Me.gb_status.Controls.Add(Me.lbl_error)
    Me.gb_status.Controls.Add(Me.lbl_canceled_or_timeout)
    Me.gb_status.Controls.Add(Me.lbl_pending)
    Me.gb_status.Location = New System.Drawing.Point(766, 6)
    Me.gb_status.Name = "gb_status"
    Me.gb_status.Size = New System.Drawing.Size(173, 155)
    Me.gb_status.TabIndex = 6
    Me.gb_status.TabStop = False
    Me.gb_status.Text = "xStatus"
    '
    'lbl_validation_error_text
    '
    Me.lbl_validation_error_text.AutoSize = True
    Me.lbl_validation_error_text.Location = New System.Drawing.Point(28, 63)
    Me.lbl_validation_error_text.Name = "lbl_validation_error_text"
    Me.lbl_validation_error_text.Size = New System.Drawing.Size(99, 13)
    Me.lbl_validation_error_text.TabIndex = 122
    Me.lbl_validation_error_text.Text = "xValidationError"
    '
    'lbl_validation_ok_text
    '
    Me.lbl_validation_ok_text.AutoSize = True
    Me.lbl_validation_ok_text.Location = New System.Drawing.Point(28, 41)
    Me.lbl_validation_ok_text.Name = "lbl_validation_ok_text"
    Me.lbl_validation_ok_text.Size = New System.Drawing.Size(86, 13)
    Me.lbl_validation_ok_text.TabIndex = 121
    Me.lbl_validation_ok_text.Text = "xValidationOk"
    '
    'lbl_validation_error
    '
    Me.lbl_validation_error.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_validation_error.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_validation_error.Location = New System.Drawing.Point(6, 62)
    Me.lbl_validation_error.Name = "lbl_validation_error"
    Me.lbl_validation_error.Size = New System.Drawing.Size(16, 16)
    Me.lbl_validation_error.TabIndex = 120
    '
    'lbl_validation_ok
    '
    Me.lbl_validation_ok.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_validation_ok.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_validation_ok.Location = New System.Drawing.Point(6, 40)
    Me.lbl_validation_ok.Name = "lbl_validation_ok"
    Me.lbl_validation_ok.Size = New System.Drawing.Size(16, 16)
    Me.lbl_validation_ok.TabIndex = 119
    '
    'lbl_pending_text
    '
    Me.lbl_pending_text.AutoSize = True
    Me.lbl_pending_text.Location = New System.Drawing.Point(28, 19)
    Me.lbl_pending_text.Name = "lbl_pending_text"
    Me.lbl_pending_text.Size = New System.Drawing.Size(59, 13)
    Me.lbl_pending_text.TabIndex = 113
    Me.lbl_pending_text.Text = "xPending"
    '
    'lbl_unknown
    '
    Me.lbl_unknown.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_unknown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_unknown.Location = New System.Drawing.Point(6, 106)
    Me.lbl_unknown.Name = "lbl_unknown"
    Me.lbl_unknown.Size = New System.Drawing.Size(16, 16)
    Me.lbl_unknown.TabIndex = 112
    '
    'lbl_error
    '
    Me.lbl_error.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_error.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_error.Location = New System.Drawing.Point(6, 84)
    Me.lbl_error.Name = "lbl_error"
    Me.lbl_error.Size = New System.Drawing.Size(16, 16)
    Me.lbl_error.TabIndex = 111
    '
    'lbl_canceled_or_timeout
    '
    Me.lbl_canceled_or_timeout.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_canceled_or_timeout.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_canceled_or_timeout.Location = New System.Drawing.Point(6, 128)
    Me.lbl_canceled_or_timeout.Name = "lbl_canceled_or_timeout"
    Me.lbl_canceled_or_timeout.Size = New System.Drawing.Size(16, 16)
    Me.lbl_canceled_or_timeout.TabIndex = 110
    '
    'lbl_pending
    '
    Me.lbl_pending.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_pending.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_pending.Location = New System.Drawing.Point(6, 18)
    Me.lbl_pending.Name = "lbl_pending"
    Me.lbl_pending.Size = New System.Drawing.Size(16, 16)
    Me.lbl_pending.TabIndex = 107
    '
    'btn_clear
    '
    Me.btn_clear.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_clear.Location = New System.Drawing.Point(0, 157)
    Me.btn_clear.Margin = New System.Windows.Forms.Padding(10)
    Me.btn_clear.Name = "btn_clear"
    Me.btn_clear.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_clear.Size = New System.Drawing.Size(90, 30)
    Me.btn_clear.TabIndex = 0
    Me.btn_clear.ToolTipped = False
    Me.btn_clear.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_validate
    '
    Me.btn_validate.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_validate.Location = New System.Drawing.Point(347, 157)
    Me.btn_validate.Name = "btn_validate"
    Me.btn_validate.Size = New System.Drawing.Size(90, 30)
    Me.btn_validate.TabIndex = 1
    Me.btn_validate.ToolTipped = False
    Me.btn_validate.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'panel_filter_buttons
    '
    Me.panel_filter_buttons.Controls.Add(Me.btn_clear)
    Me.panel_filter_buttons.Dock = System.Windows.Forms.DockStyle.Right
    Me.panel_filter_buttons.Location = New System.Drawing.Point(1009, 0)
    Me.panel_filter_buttons.Name = "panel_filter_buttons"
    Me.panel_filter_buttons.Size = New System.Drawing.Size(92, 192)
    Me.panel_filter_buttons.TabIndex = 11
    '
    'SplitContainer1
    '
    Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
    Me.SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
    Me.SplitContainer1.Location = New System.Drawing.Point(4, 4)
    Me.SplitContainer1.Name = "SplitContainer1"
    Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
    '
    'SplitContainer1.Panel1
    '
    Me.SplitContainer1.Panel1.Controls.Add(Me.panel_filter_buttons)
    Me.SplitContainer1.Panel1.Controls.Add(Me.gb_status)
    Me.SplitContainer1.Panel1.Controls.Add(Me.pn_line)
    Me.SplitContainer1.Panel1.Controls.Add(Me.uc_pr_list)
    Me.SplitContainer1.Panel1.Controls.Add(Me.btn_validate)
    '
    'SplitContainer1.Panel2
    '
    Me.SplitContainer1.Panel2.Controls.Add(Me.SplitContainer2)
    Me.SplitContainer1.Size = New System.Drawing.Size(1101, 520)
    Me.SplitContainer1.SplitterDistance = 196
    Me.SplitContainer1.TabIndex = 5
    '
    'tmr_refresh
    '
    '
    'frm_software_validations_send
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.CancelButton = Me.btn_exit
    Me.ClientSize = New System.Drawing.Size(1109, 528)
    Me.Controls.Add(Me.SplitContainer1)
    Me.Name = "frm_software_validations_send"
    Me.Text = "frm_software_validations_send"
    Me.SplitContainer2.Panel1.ResumeLayout(False)
    Me.SplitContainer2.Panel2.ResumeLayout(False)
    Me.SplitContainer2.ResumeLayout(False)
    Me.gb_status.ResumeLayout(False)
    Me.gb_status.PerformLayout()
    Me.panel_filter_buttons.ResumeLayout(False)
    Me.SplitContainer1.Panel1.ResumeLayout(False)
    Me.SplitContainer1.Panel2.ResumeLayout(False)
    Me.SplitContainer1.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents lbl_unknown_text As System.Windows.Forms.Label
  Friend WithEvents lbl_error_text As System.Windows.Forms.Label
  Friend WithEvents lbl_canceled_or_timeout_text As System.Windows.Forms.Label
  Protected WithEvents pn_line As System.Windows.Forms.Panel
  Friend WithEvents SplitContainer2 As System.Windows.Forms.SplitContainer
  Friend WithEvents gd_software_validation As GUI_Controls.uc_grid
  Friend WithEvents btn_exit As GUI_Controls.uc_button
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_pending_text As System.Windows.Forms.Label
  Friend WithEvents lbl_unknown As System.Windows.Forms.Label
  Friend WithEvents lbl_error As System.Windows.Forms.Label
  Friend WithEvents lbl_canceled_or_timeout As System.Windows.Forms.Label
  Friend WithEvents lbl_pending As System.Windows.Forms.Label
  Friend WithEvents btn_clear As GUI_Controls.uc_button
  Friend WithEvents btn_validate As GUI_Controls.uc_button
  Private WithEvents panel_filter_buttons As System.Windows.Forms.Panel
  Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
  Friend WithEvents tmr_refresh As System.Windows.Forms.Timer
  Friend WithEvents lbl_validation_error_text As System.Windows.Forms.Label
  Friend WithEvents lbl_validation_ok_text As System.Windows.Forms.Label
  Friend WithEvents lbl_validation_error As System.Windows.Forms.Label
  Friend WithEvents lbl_validation_ok As System.Windows.Forms.Label
End Class
