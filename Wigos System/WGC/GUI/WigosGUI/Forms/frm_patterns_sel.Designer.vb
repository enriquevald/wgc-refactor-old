<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_patterns_sel
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_patterns_sel))
    Me.ef_name = New GUI_Controls.uc_entry_field
    Me.gb_status = New System.Windows.Forms.GroupBox
    Me.opt_all = New System.Windows.Forms.RadioButton
    Me.opt_disabled = New System.Windows.Forms.RadioButton
    Me.opt_enabled = New System.Windows.Forms.RadioButton
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_status.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.ef_name)
    Me.panel_filter.Controls.Add(Me.gb_status)
    resources.ApplyResources(Me.panel_filter, "panel_filter")
    Me.panel_filter.Controls.SetChildIndex(Me.gb_status, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_name, 0)
    '
    'panel_data
    '
    resources.ApplyResources(Me.panel_data, "panel_data")
    '
    'pn_separator_line
    '
    resources.ApplyResources(Me.pn_separator_line, "pn_separator_line")
    '
    'pn_line
    '
    resources.ApplyResources(Me.pn_line, "pn_line")
    '
    'ef_name
    '
    Me.ef_name.DoubleValue = 0
    Me.ef_name.IntegerValue = 0
    Me.ef_name.IsReadOnly = False
    resources.ApplyResources(Me.ef_name, "ef_name")
    Me.ef_name.Name = "ef_name"
    Me.ef_name.SufixText = "Sufix Text"
    Me.ef_name.SufixTextVisible = True
    Me.ef_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_name.TextValue = ""
    Me.ef_name.Value = ""
    '
    'gb_status
    '
    Me.gb_status.Controls.Add(Me.opt_all)
    Me.gb_status.Controls.Add(Me.opt_disabled)
    Me.gb_status.Controls.Add(Me.opt_enabled)
    resources.ApplyResources(Me.gb_status, "gb_status")
    Me.gb_status.Name = "gb_status"
    Me.gb_status.TabStop = False
    '
    'opt_all
    '
    resources.ApplyResources(Me.opt_all, "opt_all")
    Me.opt_all.Name = "opt_all"
    Me.opt_all.TabStop = True
    Me.opt_all.UseVisualStyleBackColor = True
    '
    'opt_disabled
    '
    resources.ApplyResources(Me.opt_disabled, "opt_disabled")
    Me.opt_disabled.Name = "opt_disabled"
    Me.opt_disabled.TabStop = True
    Me.opt_disabled.UseVisualStyleBackColor = True
    '
    'opt_enabled
    '
    resources.ApplyResources(Me.opt_enabled, "opt_enabled")
    Me.opt_enabled.Name = "opt_enabled"
    Me.opt_enabled.TabStop = True
    Me.opt_enabled.UseVisualStyleBackColor = True
    '
    'frm_patterns_sel
    '
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit
    resources.ApplyResources(Me, "$this")
    Me.Name = "frm_patterns_sel"
    Me.ShowInTaskbar = False
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_status.ResumeLayout(False)
    Me.gb_status.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_name As GUI_Controls.uc_entry_field
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents opt_all As System.Windows.Forms.RadioButton
  Friend WithEvents opt_disabled As System.Windows.Forms.RadioButton
  Friend WithEvents opt_enabled As System.Windows.Forms.RadioButton
End Class
