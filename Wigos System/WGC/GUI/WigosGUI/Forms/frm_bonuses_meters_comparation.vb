'-------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_bonuses_meters_comparation
' DESCRIPTION:   Bonuses report 
' AUTHOR:        David Rigal
' CREATION DATE: 04-JUN-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 04-JUN-2014  DRV    Initial version
' 05-JUN-2014  DRV    Fixed Bug WIG-1007
' 03-SEP-2014  LEM    Added functionality to show terminal location data.
' 05-FEB-2014  DRV    Fixed Bug WIG-2008: "Subtotal" text not shown
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common
Imports System.Text
Imports System.Data.SqlClient


Public Class frm_bonuses_meters_comparation
  Inherits frm_base_sel


#Region " Constants "
  Private TERMINAL_DATA_COLUMNS As Int32

  Private GRID_COLUMNS_COUNT As Integer
  Private Const GRID_HEADER_ROWS_COUNT = 2

  Private GRID_COLUMN_SELECT As Integer
  Private GRID_INIT_TERMINAL_DATA As Integer
  Private GRID_COLUMN_PROVIDER As Integer
  Private GRID_COLUMN_TERMINAL As Int32
  Private GRID_COLUMN_SYSTEM_AWARDED As Integer
  Private GRID_COLUMN_SYSTEM_IN_DOUBT As Integer
  Private GRID_COLUMN_SYSTEM_TOTAL As Integer
  Private GRID_COLUMN_COUNTERS_PAYED_BY_TERM As Integer
  Private GRID_COLUMN_COUNTERS_PAYED_BY_ATTENDANT As Integer
  Private GRID_COLUMN_COUNTERS_TOTAL As Integer
  Private GRID_COLUMN_DIFFERENCE_AMOUNT As Integer
  Private GRID_COLUMN_DIFFERENCE_PER_CENT As Integer

  Private Const GRID_COLUMN_WIDTH_SELECT = 200
  Private Const GRID_COLUMN_WIDTH_PROVIDER = 2200
  Private Const GRID_COLUMN_WIDTH_TERMINAL_NAME = 2200
  Private Const GRID_COLUMN_WIDTH_SYSTEM_AWARDED = 1500
  Private Const GRID_COLUMN_WIDTH_SYSTEM_IN_DOUBT = 1500
  Private Const GRID_COLUMN_WIDTH_SYSTEM_TOTAL = 1500
  Private Const GRID_COLUMN_WIDTH_COUNTERS_PAYED_BY_TERM = 1600
  Private Const GRID_COLUMN_WIDTH_COUNTERS_PAYED_BY_ATTENDANT = 2200
  Private Const GRID_COLUMN_WIDTH_COUNTERS_TOTAL = 1500
  Private Const GRID_COLUMN_WIDTH_DIFFERENCE_AMOUNT = 1500
  Private Const GRID_COLUMN_WIDTH_DIFFERENCE_PER_CENT = 1500

  Private Const SQL_COLUMN_TERMINAL_ID = 0
  Private Const SQL_COLUMN_PROVIDER_NAME = 1
  Private Const SQL_COLUMN_TERMINAL_NAME = 2
  Private Const SQL_COLUMN_SYSTEM_AWARDED = 3
  Private Const SQL_COLUMN_SYSTEM_IN_DOUBT = 4
  Private Const SQL_COLUMN_SYSTEM_TOTAL = 5
  Private Const SQL_COLUMN_COUNTERS_PAYED_BY_TERM = 6
  Private Const SQL_COLUMN_COUNTERS_PAYED_BY_ATTENDANT = 7
  Private Const SQL_COLUMN_COUNTERS_TOTAL = 8
  Private Const SQL_COLUMN_DIFFERENCE_AMOUNT = 9
  Private Const SQL_COLUMN_DIFFERENCE_PER_CENT = 10
  Private Const SQL_COLUMN_TRANSFERRED_DATETIME = 11

#End Region

#Region " Members "

  Private m_date_from_filter As String
  Private m_date_to_filter As String
  Private m_terminals As String
  Private m_only_unbalanced As Boolean
  Private m_with_activity As Boolean
  Private m_totals As TOTAL_DATA

  Private m_terminal_report_type As ReportType = ReportType.Provider
  Private m_refresh_grid As Boolean = False

  Private m_subtotal_system_awarded As Decimal
  Private m_subtotal_system_in_doubt As Decimal
  Private m_subtotal_system_total As Decimal
  Private m_subtotal_counters_payed_byterm As Decimal
  Private m_subtotal_counters_payed_by_attendance As Decimal
  Private m_subtotal_counters_total As Decimal
  Private m_subtotal_difference_amount As Decimal
  Private m_subtotal_provider As String
  Private m_subtotals_calculated As Boolean
  Private m_options As String
  Private m_show_details As Boolean
  Private m_insert_last_row As Boolean


#End Region

#Region " Structures "

  Private Structure TOTAL_DATA
    Dim SystemAwarded As Decimal
    Dim systemInDoubt As Decimal
    Dim SystemTotal As Decimal
    Dim CountersPayedByTerm As Decimal
    Dim CountersPayedByAttendant As Decimal
    Dim CountersTotal As Decimal
    Dim DifferenceAmount As Decimal
  End Structure
#End Region

#Region " Overrides"

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_BONUSES_METERS_COMPARATION

    MyBase.GUI_SetFormId()
  End Sub


  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()
    MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5028)

    Call uc_provider.Init(WSI.Common.Misc.GamingTerminalTypeList())
    Me.uc_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5029)
    Me.uc_date.ClosingTimeEnabled = False

    chk_only_unbalanced.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5043)
    chk_with_activity.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5044)

    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False

    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)

    gb_group_by.Text = GLB_NLS_GUI_INVOICING.GetString(369)
    chk_group_by_provider.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2990) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(469)
    chk_show_detail.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5721)

    chk_show_detail.Enabled = False
    chk_show_detail.Checked = True

    Call SetDefaultValues()

    Call GUI_StyleSheet()

  End Sub


  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset  

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted

  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.uc_date.FromDateSelected And Me.uc_date.ToDateSelected Then
      If Me.uc_date.FromDate > Me.uc_date.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_date.Focus()

        Return False
      End If
    End If

    Return True
  End Function


  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub GUI_StyleSheet()
    Dim _terminal_columns As List(Of ColumnSettings)

    Call GridColumnReIndex()

    With Me.Grid
      Call .Init(GRID_COLUMNS_COUNT, GRID_HEADER_ROWS_COUNT)
      .Sortable = False
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      .Column(GRID_COLUMN_SELECT).Header(0).Text = ""
      .Column(GRID_COLUMN_SELECT).Header(1).Text = ""
      .Column(GRID_COLUMN_SELECT).Width = GRID_COLUMN_WIDTH_SELECT
      .Column(GRID_COLUMN_SELECT).HighLightWhenSelected = False
      .Column(GRID_COLUMN_SELECT).IsColumnPrintable = False

      '  Terminal Report
      _terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)
      For _idx As Int32 = 0 To _terminal_columns.Count - 1
        If _terminal_columns(_idx).Column = ReportColumn.Terminal Then
          GRID_COLUMN_TERMINAL = GRID_INIT_TERMINAL_DATA + _idx
        End If

        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5030)
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = _terminal_columns(_idx).Header1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = _terminal_columns(_idx).Width
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = _terminal_columns(_idx).Alignment
        If _terminal_columns(_idx).Column = ReportColumn.Provider Then
          GRID_COLUMN_PROVIDER = GRID_INIT_TERMINAL_DATA + _idx
        End If
      Next

      .Column(GRID_COLUMN_SYSTEM_AWARDED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5033)
      .Column(GRID_COLUMN_SYSTEM_AWARDED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5034)
      .Column(GRID_COLUMN_SYSTEM_AWARDED).Width = GRID_COLUMN_WIDTH_SYSTEM_AWARDED

      .Column(GRID_COLUMN_SYSTEM_IN_DOUBT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5033)
      .Column(GRID_COLUMN_SYSTEM_IN_DOUBT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5035)
      .Column(GRID_COLUMN_SYSTEM_IN_DOUBT).Width = GRID_COLUMN_WIDTH_SYSTEM_IN_DOUBT

      .Column(GRID_COLUMN_SYSTEM_TOTAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5033)
      .Column(GRID_COLUMN_SYSTEM_TOTAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5036)
      .Column(GRID_COLUMN_SYSTEM_TOTAL).Width = GRID_COLUMN_WIDTH_SYSTEM_TOTAL

      .Column(GRID_COLUMN_COUNTERS_PAYED_BY_TERM).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5039)
      .Column(GRID_COLUMN_COUNTERS_PAYED_BY_TERM).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5037)
      .Column(GRID_COLUMN_COUNTERS_PAYED_BY_TERM).Width = GRID_COLUMN_WIDTH_COUNTERS_PAYED_BY_TERM

      .Column(GRID_COLUMN_COUNTERS_PAYED_BY_ATTENDANT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5039)
      .Column(GRID_COLUMN_COUNTERS_PAYED_BY_ATTENDANT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5038)
      .Column(GRID_COLUMN_COUNTERS_PAYED_BY_ATTENDANT).Width = GRID_COLUMN_WIDTH_COUNTERS_PAYED_BY_ATTENDANT

      .Column(GRID_COLUMN_COUNTERS_TOTAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5039)
      .Column(GRID_COLUMN_COUNTERS_TOTAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5036)
      .Column(GRID_COLUMN_COUNTERS_TOTAL).Width = GRID_COLUMN_WIDTH_COUNTERS_TOTAL

      .Column(GRID_COLUMN_DIFFERENCE_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5040)
      .Column(GRID_COLUMN_DIFFERENCE_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5041)
      .Column(GRID_COLUMN_DIFFERENCE_AMOUNT).Width = GRID_COLUMN_WIDTH_DIFFERENCE_AMOUNT

      .Column(GRID_COLUMN_DIFFERENCE_PER_CENT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5040)
      .Column(GRID_COLUMN_DIFFERENCE_PER_CENT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5042)
      .Column(GRID_COLUMN_DIFFERENCE_PER_CENT).Width = GRID_COLUMN_WIDTH_DIFFERENCE_PER_CENT

    End With
  End Sub

  Protected Overrides Function GUI_GetQueryType() As GUI_Controls.frm_base_sel.ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_COMMAND
  End Function

  Protected Overrides Function GUI_GetSqlCommand() As System.Data.SqlClient.SqlCommand
    Dim _sql_cmd As SqlCommand

    _sql_cmd = New SqlCommand("CompareMeterBonus")
    _sql_cmd.CommandType = CommandType.StoredProcedure

    _sql_cmd.Parameters.Add("@pFrom", SqlDbType.DateTime).Value = IIf(Me.uc_date.FromDateSelected, Me.uc_date.FromDate.AddHours(uc_date.ClosingTime), DBNull.Value)
    _sql_cmd.Parameters.Add("@pTo", SqlDbType.DateTime).Value = IIf(Me.uc_date.ToDateSelected, Me.uc_date.ToDate.AddHours(uc_date.ClosingTime), DBNull.Value)
    _sql_cmd.Parameters.Add("@pSqlProviderIdList", SqlDbType.NVarChar).Value = "(" & Me.uc_provider.GetProviderIdListSelected() & ")"
    _sql_cmd.Parameters.Add("@pOnlyMismatch", SqlDbType.Bit).Value = chk_only_unbalanced.Checked
    _sql_cmd.Parameters.Add("@pExcludeNoActivity", SqlDbType.Bit).Value = chk_with_activity.Checked

    If chk_group_by_provider.Checked Then
      _sql_cmd.Parameters.Add("@pOrderBy", SqlDbType.NVarChar).Value = "TE_PROVIDER_ID"
    End If

    Return _sql_cmd
  End Function

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()

    m_totals.SystemAwarded = 0
    m_totals.systemInDoubt = 0
    m_totals.SystemTotal = 0
    m_totals.CountersPayedByTerm = 0
    m_totals.CountersPayedByAttendant = 0
    m_totals.CountersTotal = 0
    m_totals.DifferenceAmount = 0

    If m_refresh_grid Then
      Call GUI_StyleSheet()
    End If

    m_refresh_grid = False

  End Sub ' GUI_BeforeFirstRow

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _must_show_total As Boolean
    Dim _terminal_data As List(Of Object)

    _must_show_total = False

    ' Terminal Report
    _terminal_data = TerminalReport.GetReportDataList(DbRow.Value(SQL_COLUMN_TERMINAL_ID), m_terminal_report_type)
    For _idx As Int32 = 0 To _terminal_data.Count - 1
      If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value Then
        Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
      End If
    Next

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_SYSTEM_AWARDED)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SYSTEM_AWARDED).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_SYSTEM_AWARDED))
      m_totals.SystemAwarded += DbRow.Value(SQL_COLUMN_SYSTEM_AWARDED)
      _must_show_total = True
    End If
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_SYSTEM_IN_DOUBT)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SYSTEM_IN_DOUBT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_SYSTEM_IN_DOUBT))
      If DbRow.Value(SQL_COLUMN_SYSTEM_IN_DOUBT) > 0 Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_SYSTEM_IN_DOUBT).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
      End If
      m_totals.systemInDoubt += DbRow.Value(SQL_COLUMN_SYSTEM_IN_DOUBT)
      _must_show_total = True
    End If
    If _must_show_total AndAlso Not IsDBNull(DbRow.Value(SQL_COLUMN_SYSTEM_TOTAL)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SYSTEM_TOTAL).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_SYSTEM_TOTAL))
      m_totals.SystemTotal += DbRow.Value(SQL_COLUMN_SYSTEM_TOTAL)
    End If

    _must_show_total = False
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_COUNTERS_PAYED_BY_TERM)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_COUNTERS_PAYED_BY_TERM).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_COUNTERS_PAYED_BY_TERM))
      m_totals.CountersPayedByTerm += DbRow.Value(SQL_COLUMN_COUNTERS_PAYED_BY_TERM)
      _must_show_total = True
    End If
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_COUNTERS_PAYED_BY_ATTENDANT)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_COUNTERS_PAYED_BY_ATTENDANT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_COUNTERS_PAYED_BY_ATTENDANT))
      m_totals.CountersPayedByAttendant += DbRow.Value(SQL_COLUMN_COUNTERS_PAYED_BY_ATTENDANT)
      _must_show_total = True
    End If
    If _must_show_total AndAlso Not IsDBNull(DbRow.Value(SQL_COLUMN_COUNTERS_TOTAL)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_COUNTERS_TOTAL).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_COUNTERS_TOTAL))
      m_totals.CountersTotal += DbRow.Value(SQL_COLUMN_COUNTERS_TOTAL)
    End If
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_DIFFERENCE_AMOUNT)) AndAlso DbRow.Value(SQL_COLUMN_DIFFERENCE_AMOUNT) <> 0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DIFFERENCE_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_DIFFERENCE_AMOUNT))
      m_totals.DifferenceAmount += DbRow.Value(SQL_COLUMN_DIFFERENCE_AMOUNT)
    End If
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_DIFFERENCE_PER_CENT)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DIFFERENCE_PER_CENT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_DIFFERENCE_PER_CENT), 2) & "%"
    End If

    Call CheckMissmatch(RowIndex)

    Return True
  End Function


  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    Dim _last_row As Integer
    Dim _difference_per_cent As Decimal

    If m_insert_last_row Then
      If chk_group_by_provider.Checked Then
        Call PrintSubtotal(GRID_COLUMN_TERMINAL, GRID_COLUMN_PROVIDER, m_subtotal_provider)
      End If

    End If
    _last_row = Me.Grid.NumRows
    Me.Grid.AddRow()
    Me.Grid.Row(_last_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
    Me.Grid.Cell(_last_row, GRID_COLUMN_PROVIDER).Value = GLB_NLS_GUI_STATISTICS.GetString(203) '203 "TOTAL: "
    Me.Grid.Cell(_last_row, GRID_COLUMN_SYSTEM_AWARDED).Value = GUI_FormatCurrency(m_totals.SystemAwarded)
    Me.Grid.Cell(_last_row, GRID_COLUMN_SYSTEM_IN_DOUBT).Value = GUI_FormatCurrency(m_totals.systemInDoubt)
    Me.Grid.Cell(_last_row, GRID_COLUMN_SYSTEM_TOTAL).Value = GUI_FormatCurrency(m_totals.SystemTotal)
    Me.Grid.Cell(_last_row, GRID_COLUMN_COUNTERS_PAYED_BY_TERM).Value = GUI_FormatCurrency(m_totals.CountersPayedByTerm)
    Me.Grid.Cell(_last_row, GRID_COLUMN_COUNTERS_PAYED_BY_ATTENDANT).Value = GUI_FormatCurrency(m_totals.CountersPayedByAttendant)
    Me.Grid.Cell(_last_row, GRID_COLUMN_COUNTERS_TOTAL).Value = GUI_FormatCurrency(m_totals.CountersTotal)
    Me.Grid.Cell(_last_row, GRID_COLUMN_DIFFERENCE_AMOUNT).Value = GUI_FormatCurrency(m_totals.DifferenceAmount)

    _difference_per_cent = CalculateDifferencePercentage(m_totals.SystemTotal, m_totals.CountersTotal)
    Me.Grid.Cell(_last_row, GRID_COLUMN_DIFFERENCE_PER_CENT).Value = GUI_FormatNumber(_difference_per_cent, 2) & "%"

  End Sub ' GUI_AfterLastRow

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()
    m_subtotals_calculated = False
    ' Date filter
    m_date_from_filter = ""
    m_date_to_filter = ""
    m_options = ""
    m_insert_last_row = False

    'Date 
    If Me.uc_date.FromDateSelected Then
      m_date_from_filter = GUI_FormatDate(uc_date.FromDate, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Me.uc_date.ToDateSelected Then
      m_date_to_filter = GUI_FormatDate(uc_date.ToDate, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Providers - Terminals
    m_terminals = ""

    m_terminals = Me.uc_provider.GetTerminalReportText()

    ' Options
    If Me.chk_group_by_provider.Checked Then
      m_options = chk_group_by_provider.Text
      m_show_details = Me.chk_show_detail.Checked

    End If

    m_with_activity = chk_with_activity.Checked
    m_only_unbalanced = chk_only_unbalanced.Checked


  End Sub ' GUI_ReportUpdateFilters

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    'First col
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from_filter)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to_filter)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)

    'Second col
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5043), IIf(m_only_unbalanced, GLB_NLS_GUI_PLAYER_TRACKING.GetString(359), GLB_NLS_GUI_PLAYER_TRACKING.GetString(360)))
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5044), IIf(m_with_activity, GLB_NLS_GUI_PLAYER_TRACKING.GetString(359), GLB_NLS_GUI_PLAYER_TRACKING.GetString(360)))
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)

    'Third col
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_terminals)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(369), m_options)
    If Not String.IsNullOrEmpty(m_options) Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5721), IIf(m_show_details, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)))
    End If

    PrintData.FilterValueWidth(1) = 1500
    PrintData.FilterHeaderWidth(2) = 1500
    PrintData.FilterValueWidth(3) = 3000
    PrintData.FilterHeaderWidth(4) = 300

  End Sub ' GUI_ReportFilter

  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As CLASS_DB_ROW) As Boolean
    Dim _show_normal_row As Boolean
    Dim _current_terminal As DateTime
    Dim _reset_subtotal_values As Boolean

    _current_terminal = Nothing
    _show_normal_row = True
    _reset_subtotal_values = True

    If chk_group_by_provider.Checked Then
      _show_normal_row = chk_show_detail.Checked
      If String.IsNullOrEmpty(m_subtotal_provider) OrElse m_subtotal_provider <> DbRow.Value(SQL_COLUMN_PROVIDER_NAME).ToString() Then
        'Call UpdateSubtotals(True, DbRow) 'reset subtotals
        If m_subtotals_calculated Then
          Call PrintSubtotal(GRID_COLUMN_TERMINAL, GRID_COLUMN_PROVIDER, m_subtotal_provider)
          'print subtotals
        Else
          m_subtotals_calculated = True
        End If
        m_subtotal_provider = DbRow.Value(SQL_COLUMN_PROVIDER_NAME).ToString()
      Else
        _reset_subtotal_values = False 'Call UpdateSubtotals(False, DbRow) 'update subtotals
      End If

      Call UpdateSubtotals(_reset_subtotal_values, DbRow) 'update subtotals
    End If

    Return _show_normal_row
  End Function

#End Region

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent

    Me.Display(False)

  End Sub ' ShowForEdit

#End Region

#Region " Private Functions "
  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub SetDefaultValues()

    Dim from_date As DateTime
    Dim closing_time As Integer

    closing_time = GetDefaultClosingTime()
    m_subtotal_provider = ""

    from_date = New DateTime(Now.Year, Now.Month, Now.Day, closing_time, 0, 0)
    Me.uc_date.FromDate = from_date
    Me.uc_date.FromDateSelected = True

    Me.uc_date.ToDate = Me.uc_date.FromDate.AddDays(1)
    Me.uc_date.ToDateSelected = True

    Me.uc_date.ClosingTime = closing_time

    Call uc_provider.SetDefaultValues()

    chk_only_unbalanced.Checked = False
    chk_with_activity.Checked = False

    Me.chk_terminal_location.Checked = False
    Me.chk_group_by_provider.Checked = False
    Me.chk_show_detail.Checked = True

  End Sub ' SetDefaultValues

  Private Sub CheckMissmatch(ByVal RowIndex As Integer)

    If Me.Grid.Cell(RowIndex, GRID_COLUMN_DIFFERENCE_AMOUNT).Value <> "" Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DIFFERENCE_AMOUNT).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_00)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DIFFERENCE_AMOUNT).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DIFFERENCE_PER_CENT).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_00)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DIFFERENCE_PER_CENT).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SELECT).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_00)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SELECT).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
    End If

  End Sub

  Private Function CalculateDifferencePercentage(ByVal SystemTotal As Decimal, ByVal CountersTotal As Decimal) As Decimal
    Dim _difference_per_cent As Decimal

    _difference_per_cent = 0

    If SystemTotal > 0 Then
      _difference_per_cent = ((SystemTotal - CountersTotal) / SystemTotal) * 100
    Else
      If CountersTotal > 0 Then
        _difference_per_cent = -100
      Else
        _difference_per_cent = 0
      End If
    End If


    Return _difference_per_cent

  End Function

  Private Sub GridColumnReIndex()
    TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(m_terminal_report_type)

    GRID_COLUMN_SELECT = 0
    GRID_INIT_TERMINAL_DATA = 1
    GRID_COLUMN_PROVIDER = GRID_INIT_TERMINAL_DATA
    GRID_COLUMN_SYSTEM_AWARDED = TERMINAL_DATA_COLUMNS + 1
    GRID_COLUMN_SYSTEM_IN_DOUBT = TERMINAL_DATA_COLUMNS + 2
    GRID_COLUMN_SYSTEM_TOTAL = TERMINAL_DATA_COLUMNS + 3
    GRID_COLUMN_COUNTERS_PAYED_BY_TERM = TERMINAL_DATA_COLUMNS + 4
    GRID_COLUMN_COUNTERS_PAYED_BY_ATTENDANT = TERMINAL_DATA_COLUMNS + 5
    GRID_COLUMN_COUNTERS_TOTAL = TERMINAL_DATA_COLUMNS + 6
    GRID_COLUMN_DIFFERENCE_AMOUNT = TERMINAL_DATA_COLUMNS + 7
    GRID_COLUMN_DIFFERENCE_PER_CENT = TERMINAL_DATA_COLUMNS + 8

    GRID_COLUMNS_COUNT = TERMINAL_DATA_COLUMNS + 9
  End Sub

  Private Sub UpdateSubtotals(ByVal ResetValues As Boolean, ByVal DbRow As CLASS_DB_ROW)

    Dim _subtotal_system_awarded As Decimal
    Dim _subtotal_system_in_doubt As Decimal
    Dim _subtotal_system_total As Decimal
    Dim _subtotal_counters_payed_byterm As Decimal
    Dim _subtotal_counters_payed_by_attendance As Decimal
    Dim _subtotal_counters_total As Decimal
    Dim _subtotal_difference_amount As Decimal

    _subtotal_system_awarded = 0
    _subtotal_system_in_doubt = 0
    _subtotal_system_total = 0
    _subtotal_counters_payed_byterm = 0
    _subtotal_counters_payed_by_attendance = 0
    _subtotal_counters_total = 0
    _subtotal_difference_amount = 0

    m_insert_last_row = True

    If DbRow.Value(SQL_COLUMN_SYSTEM_AWARDED) IsNot DBNull.Value Then
      _subtotal_system_awarded = DbRow.Value(SQL_COLUMN_SYSTEM_AWARDED)
    End If
    If DbRow.Value(SQL_COLUMN_SYSTEM_IN_DOUBT) IsNot DBNull.Value Then
      _subtotal_system_in_doubt = DbRow.Value(SQL_COLUMN_SYSTEM_IN_DOUBT)
    End If
    If DbRow.Value(SQL_COLUMN_SYSTEM_TOTAL) IsNot DBNull.Value Then
      _subtotal_system_total = DbRow.Value(SQL_COLUMN_SYSTEM_TOTAL)
    End If
    If DbRow.Value(SQL_COLUMN_COUNTERS_PAYED_BY_TERM) IsNot DBNull.Value Then
      _subtotal_counters_payed_byterm = DbRow.Value(SQL_COLUMN_COUNTERS_PAYED_BY_TERM)
    End If
    If DbRow.Value(SQL_COLUMN_COUNTERS_PAYED_BY_ATTENDANT) IsNot DBNull.Value Then
      _subtotal_counters_payed_by_attendance = DbRow.Value(SQL_COLUMN_COUNTERS_PAYED_BY_ATTENDANT)
    End If
    If DbRow.Value(SQL_COLUMN_COUNTERS_TOTAL) IsNot DBNull.Value Then
      _subtotal_counters_total = DbRow.Value(SQL_COLUMN_COUNTERS_TOTAL)
    End If
    If DbRow.Value(SQL_COLUMN_COUNTERS_TOTAL) IsNot DBNull.Value Then
      _subtotal_difference_amount = DbRow.Value(SQL_COLUMN_DIFFERENCE_AMOUNT)
    End If

    If ResetValues Then
      m_subtotal_system_awarded = _subtotal_system_awarded
      m_subtotal_system_in_doubt = _subtotal_system_in_doubt
      m_subtotal_system_total = _subtotal_system_total
      m_subtotal_counters_payed_byterm = _subtotal_counters_payed_byterm
      m_subtotal_counters_payed_by_attendance = _subtotal_counters_payed_by_attendance
      m_subtotal_counters_total = _subtotal_counters_total
      m_subtotal_difference_amount = _subtotal_difference_amount
    Else
      m_subtotal_system_awarded = m_subtotal_system_awarded + _subtotal_system_awarded
      m_subtotal_system_in_doubt = m_subtotal_system_in_doubt + _subtotal_system_in_doubt
      m_subtotal_system_total = m_subtotal_system_total + _subtotal_system_total
      m_subtotal_counters_payed_byterm = m_subtotal_counters_payed_byterm + _subtotal_counters_payed_byterm
      m_subtotal_counters_payed_by_attendance = m_subtotal_counters_payed_by_attendance + _subtotal_counters_payed_by_attendance
      m_subtotal_counters_total = m_subtotal_counters_total + _subtotal_counters_total
      m_subtotal_difference_amount = m_subtotal_difference_amount + _subtotal_difference_amount
    End If
  End Sub

  Private Sub PrintSubtotal(ByVal IndexToPrintSubtotal As Int32, ByVal IndexToPrintAgroupedValue As Int32, ByVal ValueToPrint As String)
    Dim _row_index As Int32
    Dim _difference_per_cent As Decimal

    With Me.Grid
      Me.Grid.AddRow()
      _row_index = Me.Grid.NumRows - 1
      Me.Grid.Row(_row_index).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
      Me.Grid.Cell(_row_index, IndexToPrintSubtotal).Value = GLB_NLS_GUI_INVOICING.GetString(375) ' "Subtotal: "
      Me.Grid.Cell(_row_index, IndexToPrintAgroupedValue).Value = ValueToPrint
      Me.Grid.Cell(_row_index, GRID_COLUMN_SYSTEM_AWARDED).Value = GUI_FormatCurrency(m_subtotal_system_awarded)
      Me.Grid.Cell(_row_index, GRID_COLUMN_SYSTEM_IN_DOUBT).Value = GUI_FormatCurrency(m_subtotal_system_in_doubt)
      Me.Grid.Cell(_row_index, GRID_COLUMN_SYSTEM_TOTAL).Value = GUI_FormatCurrency(m_subtotal_system_total)
      Me.Grid.Cell(_row_index, GRID_COLUMN_COUNTERS_PAYED_BY_TERM).Value = GUI_FormatCurrency(m_subtotal_counters_payed_byterm)
      Me.Grid.Cell(_row_index, GRID_COLUMN_COUNTERS_PAYED_BY_ATTENDANT).Value = GUI_FormatCurrency(m_subtotal_counters_payed_by_attendance)
      Me.Grid.Cell(_row_index, GRID_COLUMN_COUNTERS_TOTAL).Value = GUI_FormatCurrency(m_subtotal_counters_total)
      Me.Grid.Cell(_row_index, GRID_COLUMN_DIFFERENCE_AMOUNT).Value = GUI_FormatCurrency(m_subtotal_difference_amount)

      _difference_per_cent = CalculateDifferencePercentage(m_subtotal_system_total, m_subtotal_counters_total)
      Me.Grid.Cell(_row_index, GRID_COLUMN_DIFFERENCE_PER_CENT).Value = GUI_FormatNumber(_difference_per_cent, 2) & "%"

    End With
  End Sub

#End Region

#Region " Events "

  Private Sub chk_terminal_location_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminal_location.CheckedChanged
    If chk_terminal_location.Checked Then
      m_terminal_report_type = ReportType.Provider + ReportType.Location
    Else
      m_terminal_report_type = ReportType.Provider
    End If

    m_refresh_grid = True
  End Sub


  Private Sub chk_group_by_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_group_by_provider.CheckedChanged
    chk_show_detail.Enabled = chk_group_by_provider.Checked
  End Sub

#End Region

End Class
