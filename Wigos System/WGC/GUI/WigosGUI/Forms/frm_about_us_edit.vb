﻿'-------------------------------------------------------------------
' Copyright © 2016 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_about_us
' DESCRIPTION:   About Us Form
' AUTHOR:        Gustavo Alí
' CREATION DATE: 07-SEP-2016
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 07-SEP-2016  GDA    Initial version
'--------------------------------------------------------------------

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data

Public Class frm_about_us_edit
  Inherits frm_base_edit
#Region " Constants "

  Private Const LEN_ABOUT_US_TITLE As Integer = 100
#End Region

#Region " Members "

  Private m_title As String
  Private m_delete_operation As Boolean
  Dim _aboutUs As CLS_ABOUT_US
#End Region

#Region " Overrides "

  ' PURPOSE : Initializes the form id.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_ABOUT_US_EDIT

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE : Form controls initialization.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS  :
  Protected Overrides Sub GUI_InitControls()
    Call MyBase.GUI_InitControls()

    Dim _show_delete_button As Boolean
    _show_delete_button = False

    ' Set Label values
    Me.ef_title.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7593) ' Label name
    Me.tb_content.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7594) ' Label description
    Me.lbl_content.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7594)

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7592) + " - " + GLB_NLS_GUI_PLAYER_TRACKING.GetString(7591)

    Me.ef_title.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_ABOUT_US_TITLE)
    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
      _show_delete_button = False

    ElseIf Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT Then
      _show_delete_button = True
    End If

    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = _show_delete_button

    tab_html.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7567)
    tab_preview.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7568)
    chk_add_style.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7569)
    btn_insert_tag.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7570)
    cmbTag.Items.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7571)) 'titulo 1
    cmbTag.Items.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7572)) 'titulo 2
    cmbTag.Items.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7573)) 'titulo 3
    cmbTag.Items.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7574)) 'div
    cmbTag.Items.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7575)) 'span
    cmbTag.Items.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7576)) 'list
    cmbTag.Items.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7577)) 'bold
    cmbTag.Items.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7578)) 'italic
    cmbTag.Items.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7579)) 'underline
    cmbTag.Items.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7580)) 'paragraph
  End Sub

  ' PURPOSE : Set initial data on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    _aboutUs = DbReadObject

    Me.tb_content.Text = _aboutUs.ContentText
    Me.ef_title.Value = _aboutUs.Title

  End Sub ' GUI_SetScreenData

  ' PURPOSE : Get data from the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_GetScreenData()
    _aboutUs = DbEditedObject

    _aboutUs.Title = Me.ef_title.Value
    _aboutUs.ContentText = Me.tb_content.Text.Replace(Chr(34), Chr(39))

  End Sub ' GUI_GetScreenData

  ' PURPOSE : Validate the data presented on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    'If String.IsNullOrEmpty(Me.ef_title.Value) Then
    '  Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(7546))
    '  Call ef_title.Focus()

    '  Return False
    'End If

    If String.IsNullOrEmpty(Me.tb_content.Text) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(7546))
      Call tb_content.Focus()

      Return False
    End If

    Return True
  End Function ' GUI_IsScreenDataOk

  ' PURPOSE : Init form in new mode
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '
  Public Overloads Sub ShowNewItem(ByVal id As Integer)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    DbObjectId = id
    DbStatus = ENUM_STATUS.STATUS_OK

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowNewItem

  ' PURPOSE : Init form in edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '       - UserId
  '       - Username
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Overloads Sub ShowEditItem(ByVal id As Integer)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Me.DbObjectId = id

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If
  End Sub ' ShowEditItem

  ' PURPOSE : Database overridable operations. 
  '           Define specific DB operation for this form.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Dim _about_us As CLS_ABOUT_US
    Dim _rc As Integer

    Select Case DbOperation

      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        Me.DbEditedObject = New CLS_ABOUT_US
        _about_us = Me.DbEditedObject

        With _about_us
          .ID = Me.DbObjectId
        End With
        Me.DbStatus = ENUM_STATUS.STATUS_OK

      Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE

        m_delete_operation = False

        ' Delete confirmation
        _rc = NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(718), _
                                 ENUM_MB_TYPE.MB_TYPE_WARNING, _
                                 ENUM_MB_BTN.MB_BTN_YES_NO, _
                                 ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
                                 Me.m_title)
        If _rc = ENUM_MB_RESULT.MB_RESULT_YES Then
          m_delete_operation = True
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_DELETE
        If m_delete_operation Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        Else
          DbStatus = ENUM_STATUS.STATUS_ERROR
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)

    Call MyBase.GUI_ButtonClick(ButtonId)

  End Sub

  ' PURPOSE : Get string of play session
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - String: 
  '


  ' PURPOSE : Override default behavior when user press key
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :


#End Region

#Region "Events"
  Private Sub tab_control_HTML_SelectedIndexChanged(sender As Object, e As EventArgs) Handles tab_control_HTML.SelectedIndexChanged
    Dim current As TabPage = TryCast(sender, TabControl).SelectedTab
    If current.Name = "tab_preview" Then

      web_browser_html.DocumentText = vbCrLf + tb_content.Text
    End If
  End Sub

  Private Sub btn_insert_tag_Click(sender As Object, e As EventArgs) Handles btn_insert_tag.Click

    Select Case cmbTag.Text
      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(7571)
        AddControl("H1")

      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(7572)
        AddControl("H2")
      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(7573)
        AddControl("H3")
      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(7577)
        AddControl("B")
      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(7578)
        AddControl("I")

      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(7579)
        AddControl("U")
      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(7576)
        tb_content.SelectionColor = Color.DarkGreen
        If chk_add_style.Checked = True Then
          tb_content.AppendText(vbCrLf + "<UL style= ''>")
          tb_content.AppendText(vbCrLf + "<LI style= ''>")
          tb_content.SelectionColor = Color.Black
          tb_content.AppendText("item1")
          tb_content.SelectionColor = Color.DarkGreen
          tb_content.AppendText("</LI>")
          tb_content.AppendText(vbCrLf + "</UL>")
        Else
          tb_content.AppendText(vbCrLf + "<UL>")
          tb_content.AppendText(vbCrLf + "<LI>")
          tb_content.SelectionColor = Color.Black
          tb_content.AppendText("item2")
          tb_content.SelectionColor = Color.DarkGreen
          tb_content.AppendText("</LI>")
          tb_content.AppendText(vbCrLf + "</UL>")
        End If


      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(7574)
        AddControl("DIV")
      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(7575)
        AddControl("SPAN")
      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(7580)
        AddControl("P")
    End Select
  End Sub
#End Region

#Region "Privates"
  'SDS 27-08-2016 PBI 17065 html description
  Private Sub AddControl(control As String)
    tb_content.SelectionColor = Color.DarkGreen
    If chk_add_style.Checked = True Then
      tb_content.AppendText(vbCrLf + "<" + control + " style= '' " + ">")
    Else
      tb_content.AppendText(vbCrLf + "<" + control + ">")
    End If
    tb_content.SelectionColor = Color.Black
    tb_content.AppendText("texto")
    tb_content.SelectionColor = Color.DarkGreen
    tb_content.AppendText("</" + control + ">")
  End Sub
#End Region




End Class