'-----------------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd. 
'-----------------------------------------------------------------------------
'
' MODULE NAME:   frm_table_play_sessions
' DESCRIPTION:   This screen allows to view and edit the player games report.
' AUTHOR:        Javier Fern�ndez
' CREATION DATE: 15-MAY-2014
' 
' REVISION HISTORY:
'
' Date         Author       Description
' -----------  ------       ---------------------------------------------------------
' 15-MAY-2014  JFC          Initial version
' 06-AGU-2014  JML & DHA    Fixed Bug #WIG-1157: corrected value formats and show virtual accounts
' 26-SEP-2014  DHA          Fixed Bug #WIG-1273: corrected Current bet filter
' 30-SEP-2014  DCS          Fixed Bug #WIG-1351: corrected visible info for Unknown accounts
' 01-OCT-2014  DCS          Fixed Bug #WIG-1372: do uneditable field netwin and autocalculated
' 07-OCT-2014  DHA          Fixed Bug WIG-1424: set filters properly
' 07-OCT-2014  DHA          Fixed Bug WIG-1424 & 1157: set filters properly and enable button save changes
' 28-OCT-2014  DHA          Fixed Bug WIG-1587: when is ExternalLoyaltyProgram, awared points cells are locked
' 27-NOV-2014  DCS          Fixed Bug WIG-1774: Column names not shown when any data in grid
' 02-DIC-2014  DCS          Modify date filter, adding end date
' 12-JAN-2015  DCS          Fixed Bug WIG-1917: Don't show open sessions filter, and leave original value and show error message when added empty value in grid.
' 28-JAN-2015  DHA          Added filter for entry manual gaming table sessions
' 10-FEB-2015  JML          Fixed Bug #WIG-2036: Gaming Tables: An unhandled exception in table game sessions edit
' 25-JUN-2015  FAV          Fixed Bug #WIG-2434: The'Table gaming movements' report loses the filters when the user press the 'search' button again.
' 23-OCT-2015  YNM          Fixed Bug TFS-5633: unhandled exception opening movement form
' 14-JUL-2016  RAB          Product Backlog Item 15066: GamingTables (Phase 4): Adapt reports to MultiCurrency
' 13-APR-2017  RAB          Bug 26778: WIGOS-1078 User with read permissions in gaming tables report can modify the report gaming session editor
' 18-APR-2017  JML          Fixed bug: WIGOS-1341 Validation tickets column can be editing on edition gaming table session report
' 07-SEP-2017  RAB          PBI 28944: WIGOS-3414 MES16 - Points assignment: Gaming table session edition
' 07-SEP-2017  RAB          Bug 29640: WIGOS-4975 Gaming tables: the points added manually on gaming table session editor are no calculated like buckets
' 03-OCT-2017  RGR          WIGOS-4852 Table players report
' 03-NOV-2017  RAB          Bug 30546:WIGOS-6322 Gambling table point assignment: error is displayed when manual entry point assignment is modified
' 14-MAR-2018  EOR          Bug 31922:WIGOS-8178 [Ticket #12451] Permiso para Edici�n de Sesi�n de Juego En Mesa Version V03.06.0040 
'------------------------------------------------------------------------------------

#Region "Imports"

Imports System.Text
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions

Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations
Imports WSI.Common
Imports WSI.Common.Terminal
Imports WSI.Common.MultiPromos
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE

#End Region

Public Class frm_table_play_sessions
  Inherits frm_base_sel_edit

#Region "Constants"

  Private Const GRID_HEADERS_COUNT As Integer = 2
  Private Const COLOR_IMPORTED = ENUM_GUI_COLOR.GUI_COLOR_OCHRE_00

  ' Grid counters
  Private Const COUNTER_NOT_IMPORTED As Integer = 1
  Private Const COUNTER_IMPORTED As Integer = 2

  'Index Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_GAME_TABLE_PLAY_SESSION_ID As Integer = 1
  Private Const GRID_COLUMN_GAME_TABLE_TYPE As Integer = 2
  Private Const GRID_COLUMN_GAME_TABLE As Integer = 3
  Private Const GRID_COLUMN_ATTENDANT As Integer = 4
  Private Const GRID_COLUMN_SEAT As Integer = 5
  Private Const GRID_COLUMN_INITIAL_DATE As Integer = 6
  Private Const GRID_COLUMN_GAME_STATUS As Integer = 7
  Private Const GRID_COLUMN_TIME_AT_TABLE As Integer = 8
  Private Const GRID_COLUMN_TIME_OFF_AMOUNT As Integer = 9
  Private Const GRID_COLUMN_FINISH_DATE As Integer = 10
  Private Const GRID_COLUMN_PLAYS_COUNT As Integer = 11
  Private Const GRID_COLUMN_SPEED As Integer = 12
  Private Const GRID_COLUMN_SKILL As Integer = 13
  Private Const GRID_COLUMN_ACCOUNT_ID As Integer = 14
  Private Const GRID_COLUMN_NAME As Integer = 15
  Private Const GRID_COLUMN_COMPUTED_POINTS As Integer = 16
  Private Const GRID_COLUMN_POINTS As Integer = 17
  Private Const GRID_COLUMN_ISO_CODE As Integer = 18
  Private Const GRID_COLUMN_PLAYED_CURRENT As Integer = 19
  Private Const GRID_COLUMN_PLAYED_AMOUNT As Integer = 20
  Private Const GRID_COLUMN_AVERAGE_PLAYED As Integer = 21
  Private Const GRID_COLUMN_PLAYED_MIN As Integer = 22
  Private Const GRID_COLUMN_PLAYED_MAX As Integer = 23
  Private Const GRID_COLUMN_CHIPS_IN As Integer = 24
  Private Const GRID_COLUMN_CHIPS_OUT As Integer = 25
  Private Const GRID_COLUMN_CHIPS_TOTAL_BUY As Integer = 26
  Private Const GRID_COLUMN_WON_AMOUNT As Integer = 27
  Private Const GRID_COLUMN_ACCOUNT_TYPE As Integer = 28

  Private Const GRID_COLUMNS_COUNT As Integer = 29

  'SQL Columns
  Private Const SQL_COLUMN_GAME_TABLE_PLAY_SESSION_ID As Integer = 0
  Private Const SQL_COLUMN_GAME_TABLE_TYPE As Integer = 1
  Private Const SQL_COLUMN_GAME_TABLE As Integer = 2
  Private Const SQL_COLUMN_ATTENDANT As Integer = 3
  Private Const SQL_COLUMN_SEAT As Integer = 4
  Private Const SQL_COLUMN_INITIAL_DATE As Integer = 5
  Private Const SQL_COLUMN_FINISH_DATE As Integer = 6
  Private Const SQL_COLUMN_GAME_STATUS As Integer = 7
  Private Const SQL_COLUMN_TIME_AT_TABLE As Integer = 8
  Private Const SQL_COLUMN_TIME_OFF_AMOUNT As Integer = 9
  Private Const SQL_COLUMN_PLAYS_COUNT As Integer = 10
  Private Const SQL_COLUMN_ACCOUNT_ID As Integer = 11
  Private Const SQL_COLUMN_HOLDER_NAME As Integer = 12
  Private Const SQL_COLUMN_SPEED As Integer = 13
  Private Const SQL_COLUMN_SKILL As Integer = 14
  Private Const SQL_COLUMN_COMPUTED_POINTS As Integer = 15
  Private Const SQL_COLUMN_POINTS As Integer = 16
  Private Const SQL_COLUMN_ISO_CODE As Integer = 17
  Private Const SQL_COLUMN_PLAYED_CURRENT As Integer = 18
  Private Const SQL_COLUMN_PLAYED_AMOUNT As Integer = 19
  Private Const SQL_COLUMN_AVERAGE_PLAYED As Integer = 20
  Private Const SQL_COLUMN_PLAYED_MIN As Integer = 21
  Private Const SQL_COLUMN_PLAYED_MAX As Integer = 22
  Private Const SQL_COLUMN_CHIPS_IN As Integer = 23
  Private Const SQL_COLUMN_CHIPS_OUT As Integer = 24
  Private Const SQL_COLUMN_CHIPS_TOTAL_BUY As Integer = 25
  Private Const SQL_COLUMN_WON_AMOUNT As Integer = 26
  Private Const SQL_COLUMN_ACCOUNT_TYPE As Integer = 27
  Private Const SQL_COLUMN_TERMINAL_ID As Integer = 28

  'Width
  Private Const GRID_WIDTH_INDEX As Integer = 200
  Private Const GRID_WIDTH_GAME_TABLE_PLAY_SESSION_ID As Integer = 0
  Private Const GRID_WIDTH_GAME_TABLE_TYPE As Integer = 1600
  Private Const GRID_WIDTH_GAME_TABLE As Integer = 1600
  Private Const GRID_WIDTH_ATTENDANT As Integer = 1500
  Private Const GRID_WIDTH_SEAT As Integer = 850
  Private Const GRID_WIDTH_INITIAL_DATE As Integer = 2050
  Private Const GRID_WIDTH_FINISH_DATE As Integer = 2050
  Private Const GRID_WIDTH_GAME_STATUS As Integer = 1000
  Private Const GRID_WIDTH_TIME_AT_TABLE As Integer = 1600
  Private Const GRID_WIDTH_TIME_OFF_AMOUNT As Integer = 1600
  Private Const GRID_WIDTH_PLAYS_COUNT As Integer = 1200
  Private Const GRID_WIDTH_ACCOUNT_ID As Integer = 1150
  Private Const GRID_WIDTH_NAME As Integer = 2350
  Private Const GRID_WIDTH_SPEED As Integer = 1200
  Private Const GRID_WIDTH_SKILL As Integer = 1200
  Private Const GRID_WIDTH_COMPUTED_POINTS As Integer = 1200
  Private Const GRID_WIDTH_POINTS As Integer = 1200
  Private Const GRID_WIDTH_ISO_CODE As Integer = 1200
  Private Const GRID_WIDTH_PLAYED_CURRENT As Integer = 1200
  Private Const GRID_WIDTH_PLAYED_AMOUNT As Integer = 1200
  Private Const GRID_WIDTH_AVERAGE_PLAYED As Integer = 1200
  Private Const GRID_WIDTH_PLAYED_MIN As Integer = 1200
  Private Const GRID_WIDTH_PLAYED_MAX As Integer = 1200
  Private Const GRID_WIDTH_CHIPS_IN As Integer = 1200
  Private Const GRID_WIDTH_CHIPS_OUT As Integer = 1200
  Private Const GRID_WIDTH_CHIPS_TOTAL_BUY As Integer = 2100
  Private Const GRID_WIDTH_WON_AMOUNT As Integer = 1200

#End Region

#Region " Query Index Constants "

  Private Const INDEX_STARTED_STATUS As String = " WITH( INDEX(IX_gtps_started_status))"
  Private Const INDEX_ACCOUNTS_STARTED As String = " WITH( INDEX(IX_gtps_account_id_started))"
  Private Const INDEX_FINISHED_STATUS As String = " WITH( INDEX(IX_gtps_finished_status))"
  Private Const INDEX_ACCOUNT_FINISHED As String = " WITH( INDEX(IX_gtps_account_id_finished))"

#End Region

#Region "Structures"

  Private Structure GamingTablesGroup
    Public Id As Int32
    Public Type As String
    Public Enabled As Boolean
    Public GamingTables As List(Of GamingTable)
  End Structure

  Private Structure GamingTable
    Public Id As Int32
    Public Name As String
  End Structure

#End Region

#Region "Members"

  Private m_all_gaming_tables As List(Of GamingTablesGroup)
  Private m_query_as_datatable As DataTable

  ' For report filters
  Private m_date_from As String
  Private m_date_to As String
  Private m_date_filter As String
  Private m_date_open_sessions As Boolean
  Private m_current_bet_from As String
  Private m_current_bet_to As String
  Private m_employee_id As String
  Private m_card_account As String
  Private m_card_track_data As String
  Private m_card_holder_name As String
  Private m_holder_is_vip As String
  Private m_gaming_tables As String
  Private m_session_status As String
  Private m_manual_entry As String

  Private m_counter_not_imported As Integer
  Private m_counter_imported As Integer

  Private m_user_permissions As CLASS_GUI_USER.TYPE_PERMISSIONS ' Permission object

  Private m_elp_mode As Int32
  Private m_selected_currency As String

  ' Buttons
  Private m_save_button As uc_button

  Private ReadOnly m_is_edit_mode As Boolean
  Private m_error_null_value As Boolean
  Private m_show_message As Boolean

#End Region

#Region "Constructors"

  Public Sub New(ByVal FormId As ENUM_FORM)
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Load Permisions
    Me.m_user_permissions = CurrentUser.Permissions(FormId)

    ' Initialize members
    Me.FormId = FormId

    MyBase.GUI_SetFormId()

    Select Case FormId

      Case ENUM_FORM.FORM_TABLE_PLAY_SESSIONS
        Me.m_is_edit_mode = False

      Case ENUM_FORM.FORM_TABLE_PLAY_SESSIONS_EDIT
        Me.m_is_edit_mode = True

    End Select

    Me.m_all_gaming_tables = LoadGamingTablesByType()
    Me.m_save_button = Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2)

  End Sub

#End Region

#Region "Overridden methods: frm_base_sel_edit"

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId() ' GUI_SetFormId 
    Me.FormId = ENUM_FORM.FORM_TABLE_PLAY_SESSIONS
    MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId 

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    MyBase.GUI_InitControls()

    m_elp_mode = GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode")

    If Me.m_is_edit_mode Then
      Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4989)
    Else
      Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4967)
    End If

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2629) '2629 "Movements"

    If Me.m_is_edit_mode Then
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_3).Visible = True
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_3).Type = uc_button.ENUM_BUTTON_TYPE.USER
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_3).Height += 10
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_3).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5869)
    End If

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    If Me.m_is_edit_mode Then
      Me.m_save_button.Visible = True
      Me.m_save_button.Enabled = False
      Me.m_save_button.Type = uc_button.ENUM_BUTTON_TYPE.USER
      Me.m_save_button.Text = GLB_NLS_GUI_CONTROLS.GetString(13)
    End If

    ' Date
    Me.gb_date.Text = GLB_NLS_GUI_INVOICING.GetString(201) ' 201 - Date
    Me.rb_open_sessions.Text = GLB_NLS_GUI_STATISTICS.GetString(345) ' 345 - Open Sessions
    Me.rb_historic.Text = GLB_NLS_GUI_CONTROLS.GetString(331) ' 331 - History
    Me.rb_ini_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5285) ' 5285 - Arrival
    Me.rb_finish_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5286) ' 5286 - Departure
    Me.dtp_from.Text = GLB_NLS_GUI_AUDITOR.GetString(257)
    Me.dtp_to.Text = GLB_NLS_GUI_AUDITOR.GetString(258)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    If Me.m_is_edit_mode Then
      Me.rb_open_sessions.Visible = False
      Me.rb_historic.Location = New Point(Me.rb_historic.Location.X, Me.rb_historic.Location.Y - 19)
      Me.rb_ini_date.Location = New Point(Me.rb_ini_date.Location.X, Me.rb_ini_date.Location.Y - 15)
      Me.rb_finish_date.Location = New Point(Me.rb_finish_date.Location.X, Me.rb_finish_date.Location.Y - 12)
      Me.dtp_from.Location = New Point(Me.dtp_from.Location.X, Me.dtp_from.Location.Y - 9)
      Me.dtp_to.Location = New Point(Me.dtp_to.Location.X, Me.dtp_to.Location.Y - 6)
    Else
      AddHandler rb_open_sessions.Click, AddressOf rb_date_CheckedChanged
    End If

    AddHandler rb_historic.Click, AddressOf rb_date_CheckedChanged
    AddHandler rb_ini_date.Click, AddressOf rb_date_CheckedChanged
    AddHandler rb_finish_date.Click, AddressOf rb_date_CheckedChanged

    ' Average bet
    Me.gb_average_bet.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4970)
    Me.ef_current_bet_from.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(297)
    Me.ef_current_bet_from.IsReadOnly = False
    Me.ef_current_bet_from.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 12, 2)
    Me.ef_current_bet_to.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(298)
    Me.ef_current_bet_to.IsReadOnly = False
    Me.ef_current_bet_to.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 12, 2)

    ' Game status
    Me.game_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4980)
    Me.game_status.Combo.Add(0, GLB_NLS_GUI_STATISTICS.GetString(400)) ' Open
    Me.game_status.Combo.Add(1, GLB_NLS_GUI_STATISTICS.GetString(401)) ' Closed
    Me.game_status.Combo.Add(2, GLB_NLS_GUI_STATISTICS.GetString(409)) ' Abandoned

    ' Users 
    Me.gb_user.Text = GLB_NLS_GUI_INVOICING.GetString(220)
    Me.opt_one_user.Text = GLB_NLS_GUI_INVOICING.GetString(218)
    Me.opt_all_users.Text = GLB_NLS_GUI_INVOICING.GetString(219)
    Me.chk_show_all.Text = GLB_NLS_GUI_CONFIGURATION.GetString(90)

    SetCombo(Me.cmb_user, "SELECT GU_USER_ID, GU_USERNAME FROM GUI_USERS WHERE GU_BLOCK_REASON = " & _
                    WSI.Common.GUI_USER_BLOCK_REASON.NONE & " ORDER BY GU_USERNAME")

    ' Groups
    uc_checked_list_groups.GroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3416)
    uc_checked_list_groups.btn_check_all.Width = 115
    uc_checked_list_groups.btn_uncheck_all.Width = 115

    ' TITO Mode
    If WSI.Common.TITO.Utils.IsTitoMode() Then
      uc_account_sel1.InitExtendedQuery(True)
    End If

    'Results
    gb_manual_entry.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5869)
    chk_manual_entry_yes.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
    lbl_color_imported.BackColor = GetColor(COLOR_IMPORTED)
    chk_manual_entry_no.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)

    SetDefaultValues()

    GUI_StyleSheet()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()

    SetDefaultValues()

  End Sub ' GUI_FilterReset

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Me.dtp_to.Focus()

        Return False
      End If
    End If

    '' Average Played filter
    'If GUI_ParseNumber(Me.ef_avg_bet_from.Value) > GUI_ParseNumber(Me.ef_avg_bet_to.Value) Then
    '  NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(338), ENUM_MB_TYPE.MB_TYPE_WARNING, , , _
    '             Me.gb_average_bet.Text + " (" + Me.ef_avg_bet_from.Text + ")", _
    '             Me.gb_average_bet.Text + " (" + Me.ef_avg_bet_to.Text + ")")
    '  Me.dtp_to.Focus()

    '  Return False
    'End If

    Return True

  End Function ' GUI_FilterCheck

  ' PURPOSE : Sets initial focus
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  '          
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.gb_average_bet
  End Sub ' GUI_SetInitialFocus

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As CLASS_DB_ROW) As Boolean ' GUI_SetupRow
    Dim _time As TimeSpan
    Dim _account_type As AccountType

    If IsDBNull(DbRow.Value(SQL_COLUMN_GAME_TABLE_PLAY_SESSION_ID)) Then
      Return False
    End If

    'TABLE

    ' Session Id. GRID_COLUMN_GAME_TABLE_PLAY_SESSION_ID
    If Not DbRow.IsNull(SQL_COLUMN_GAME_TABLE_PLAY_SESSION_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_TABLE_PLAY_SESSION_ID).Value = DbRow.Value(SQL_COLUMN_GAME_TABLE_PLAY_SESSION_ID)
    End If

    ' Table Type 
    If Not DbRow.IsNull(SQL_COLUMN_GAME_TABLE_TYPE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_TABLE_TYPE).Value = DbRow.Value(SQL_COLUMN_GAME_TABLE_TYPE)
    End If

    ' Table name
    If Not DbRow.IsNull(SQL_COLUMN_GAME_TABLE_TYPE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_TABLE).Value = DbRow.Value(SQL_COLUMN_GAME_TABLE)
    End If

    ' Attendant
    If Not DbRow.IsNull(SQL_COLUMN_ATTENDANT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ATTENDANT).Value = DbRow.Value(SQL_COLUMN_ATTENDANT)
    End If

    ' SESSION

    ' Started
    If Not DbRow.IsNull(SQL_COLUMN_INITIAL_DATE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_INITIAL_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_INITIAL_DATE), _
                                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                 ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Finished
    If Not DbRow.IsNull(SQL_COLUMN_FINISH_DATE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_FINISH_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_FINISH_DATE), _
                                                                                      ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                     ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Status
    If Not DbRow.IsNull(SQL_COLUMN_GAME_STATUS) Then
      Select Case DbRow.Value(SQL_COLUMN_GAME_STATUS)
        Case GTPlaySessionStatus.Opened
          Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_STATUS).Value = GLB_NLS_GUI_STATISTICS.GetString(400)
        Case GTPlaySessionStatus.Closed
          Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_STATUS).Value = GLB_NLS_GUI_STATISTICS.GetString(401)
        Case GTPlaySessionStatus.Away
          Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_STATUS).Value = GLB_NLS_GUI_STATISTICS.GetString(409)
        Case Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_STATUS).Value = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(222)
      End Select
    End If

    ' Gaming time
    If Not DbRow.IsNull(SQL_COLUMN_TIME_AT_TABLE) Then
      _time = New TimeSpan(0, 0, DbRow.Value(SQL_COLUMN_TIME_AT_TABLE))

      Me.Grid.Cell(RowIndex, GRID_COLUMN_TIME_AT_TABLE).Value = TimeSpanToString(_time)
    End If

    ' Walk
    If Not DbRow.IsNull(SQL_COLUMN_TIME_OFF_AMOUNT) Then
      _time = New TimeSpan(0, 0, DbRow.Value(SQL_COLUMN_TIME_OFF_AMOUNT))

      Me.Grid.Cell(RowIndex, GRID_COLUMN_TIME_OFF_AMOUNT).Value = TimeSpanToString(_time)
    End If

    ' Seat
    If Not DbRow.IsNull(SQL_COLUMN_SEAT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SEAT).Value = DbRow.Value(SQL_COLUMN_SEAT)
    End If

    ' Plays count
    If Not DbRow.IsNull(SQL_COLUMN_PLAYS_COUNT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYS_COUNT).Value = DbRow.Value(SQL_COLUMN_PLAYS_COUNT)
    End If

    ' ACCOUNT
    _account_type = DbRow.Value(SQL_COLUMN_ACCOUNT_TYPE)

    If Not DbRow.IsNull(SQL_COLUMN_ACCOUNT_TYPE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_TYPE).Value = _account_type
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_TYPE).Value = 0
    End If

    ' Account Id.
    If Not DbRow.IsNull(SQL_COLUMN_ACCOUNT_ID) Then
      If _account_type = AccountType.ACCOUNT_VIRTUAL_TERMINAL Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID).Value = String.Empty
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_ID)
      End If
    End If

    ' Account name
    If _account_type = AccountType.ACCOUNT_VIRTUAL_TERMINAL Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NAME).Value = WSI.Common.Resource.String("STR_CASHIER_GAMING_TABLE_PLAYER_UNKNOWN_BUTTON")
    Else
      If Not DbRow.IsNull(SQL_COLUMN_HOLDER_NAME) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_NAME).Value = DbRow.Value(SQL_COLUMN_HOLDER_NAME)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_NAME).Value = ""
      End If
    End If

    ' Game speed
    If Not DbRow.IsNull(SQL_COLUMN_SPEED) Then
      Select Case DbRow.Value(SQL_COLUMN_SPEED)
        Case GTPlayerTrackingSpeed.Unknown
          Me.Grid.Cell(RowIndex, GRID_COLUMN_SPEED).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5020)
        Case GTPlayerTrackingSpeed.Slow
          Me.Grid.Cell(RowIndex, GRID_COLUMN_SPEED).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5021)
        Case GTPlayerTrackingSpeed.Medium
          Me.Grid.Cell(RowIndex, GRID_COLUMN_SPEED).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5022)
        Case GTPlayerTrackingSpeed.Fast
          Me.Grid.Cell(RowIndex, GRID_COLUMN_SPEED).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5023)
      End Select
    End If

    ' Game skill
    If Not DbRow.IsNull(SQL_COLUMN_SKILL) Then
      Select Case DbRow.Value(SQL_COLUMN_SKILL)
        Case GTPlaySessionPlayerSkill.Unknown
          Me.Grid.Cell(RowIndex, GRID_COLUMN_SKILL).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5020)
        Case GTPlaySessionPlayerSkill.Soft
          Me.Grid.Cell(RowIndex, GRID_COLUMN_SKILL).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5025)
        Case GTPlaySessionPlayerSkill.Average
          Me.Grid.Cell(RowIndex, GRID_COLUMN_SKILL).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5026)
        Case GTPlaySessionPlayerSkill.Hard
          Me.Grid.Cell(RowIndex, GRID_COLUMN_SKILL).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5027)
      End Select
    End If

    ' POINTS

    ' Computed points
    If Not DbRow.IsNull(SQL_COLUMN_COMPUTED_POINTS) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_COMPUTED_POINTS).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_COMPUTED_POINTS), 2)
    End If

    ' Awarded points
    If Not DbRow.IsNull(SQL_COLUMN_POINTS) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_POINTS), 2)
    End If

    ' PLAYED

    ' Iso code
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ISO_CODE).Value = DbRow.Value(SQL_COLUMN_ISO_CODE)

    ' Current amount
    If Not DbRow.IsNull(SQL_COLUMN_PLAYED_CURRENT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED_CURRENT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PLAYED_CURRENT), _
                                                                                   2, _
                                                                                   ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, _
                                                                                   False)
    End If

    ' Played amount
    If Not DbRow.IsNull(SQL_COLUMN_PLAYED_AMOUNT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT), _
                                                                                   2, _
                                                                                   ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, _
                                                                                   False)
    End If

    ' Average played
    If Not DbRow.IsNull(SQL_COLUMN_AVERAGE_PLAYED) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_AVERAGE_PLAYED).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_AVERAGE_PLAYED), _
                                                                                    2, _
                                                                                    ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, _
                                                                                    False)
    End If

    ' Min played
    If Not DbRow.IsNull(SQL_COLUMN_PLAYED_MIN) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED_MIN).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PLAYED_MIN), _
                                                                                2, _
                                                                                ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, _
                                                                                False)
    End If

    ' Max played
    If Not DbRow.IsNull(SQL_COLUMN_PLAYED_MAX) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED_MAX).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PLAYED_MAX), _
                                                                                2, _
                                                                                ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, _
                                                                                False)
    End If

    ' WON

    ' Won amount
    If Not DbRow.IsNull(SQL_COLUMN_WON_AMOUNT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_WON_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_WON_AMOUNT), _
                                                                                2, _
                                                                                ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, _
                                                                                False)
    End If

    ' Chips in
    If Not DbRow.IsNull(SQL_COLUMN_CHIPS_IN) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CHIPS_IN).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_CHIPS_IN))
    End If

    ' Chips out
    If Not DbRow.IsNull(SQL_COLUMN_CHIPS_OUT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CHIPS_OUT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_CHIPS_OUT))
    End If

    ' Total buy Chips in
    If Not DbRow.IsNull(SQL_COLUMN_CHIPS_TOTAL_BUY) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CHIPS_TOTAL_BUY).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_CHIPS_TOTAL_BUY))
    End If

    ' Set color if play session is imported
    If DbRow.IsNull(SQL_COLUMN_SEAT) Then
      Me.Grid.Row(RowIndex).BackColor = GetColor(COLOR_IMPORTED)
      Me.m_counter_imported += 1
    Else
      Me.m_counter_not_imported += 1
    End If

    Return True
  End Function ' GUI_SetupRow

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow() ' GUI_AfterLastRow

    Me.AddTotalRow(Me.m_query_as_datatable)

    Me.Grid.Counter(COUNTER_IMPORTED).Value = Me.m_counter_imported
    Me.Grid.Counter(COUNTER_NOT_IMPORTED).Value = Me.m_counter_not_imported

    Me.m_counter_imported = 0
    Me.m_counter_not_imported = 0

  End Sub ' GUI_AfterLastRow

  ' PURPOSE: Select first row only if grid has more than one row
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_SelectFirstRow() As Boolean
    Return Me.Grid.NumRows > 1
  End Function ' GUI_SelectFirstRow

  ' PURPOSE: Enable or disable Movements Button if grid has more than one row
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_RowSelectedEvent(ByVal SelectedRow As Integer)
    If String.IsNullOrEmpty(Me.Grid.Cell(SelectedRow, GRID_COLUMN_GAME_TABLE_PLAY_SESSION_ID).Value) Then
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False
    Else
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = True
    End If
  End Sub ' GUI_RowSelectedEvent

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA) ' GUI_ReportFilter
    Dim _average_betting As Double

    ' Dates filter
    If m_date_open_sessions Then
      PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(261), GLB_NLS_GUI_STATISTICS.GetString(345))
    Else
      PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(261), GLB_NLS_GUI_CONTROLS.GetString(331) & " - " & m_date_filter)
      PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(257), m_date_from)
      PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(258), m_date_to)
      ' Session status
      If Me.game_status.Visible Then
        PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4980), Me.m_session_status)
      End If
    End If

    ' Current bet from
    If Me.m_current_bet_from <> String.Empty AndAlso Double.TryParse(Me.m_current_bet_from, _average_betting) Then
      Me.m_current_bet_from = GUI_FormatCurrency(_average_betting, _
                                                2, _
                                                ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, _
                                                False)
    End If

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5620) + " " + _
                        GLB_NLS_GUI_AUDITOR.GetString(257), _
                        Me.m_current_bet_from)

    ' Current bet to
    If Me.m_current_bet_to <> String.Empty AndAlso Double.TryParse(Me.m_current_bet_to, _average_betting) Then
      Me.m_current_bet_to = GUI_FormatCurrency(_average_betting, _
                                                2, _
                                                ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, _
                                                False)
    End If

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5620) + " " + _
                        GLB_NLS_GUI_INVOICING.GetString(203), _
                        Me.m_current_bet_to)

    ' Users
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4974), Me.m_employee_id)

    ' Account
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), Me.m_card_account)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), Me.m_card_track_data)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), Me.m_card_holder_name)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4802), Me.m_holder_is_vip)

    ' Gaming Tables
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4973), Me.m_gaming_tables)

    ' Manual Entry
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5869), Me.m_manual_entry)

    ' uc_multi_currency_site_sel
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7382), m_selected_currency)

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()
    Me.m_date_from = String.Empty
    Me.m_date_to = String.Empty
    Me.m_current_bet_from = String.Empty
    Me.m_date_filter = String.Empty
    Me.m_date_open_sessions = False
    Me.m_current_bet_to = String.Empty
    Me.m_employee_id = String.Empty
    Me.m_card_account = String.Empty
    Me.m_card_track_data = String.Empty
    Me.m_card_holder_name = String.Empty
    Me.m_holder_is_vip = String.Empty
    Me.m_gaming_tables = String.Empty
    Me.m_session_status = String.Empty
    Me.m_manual_entry = String.Empty

    'Date 
    m_date_open_sessions = Me.rb_open_sessions.Checked
    If Me.rb_ini_date.Checked Then
      m_date_filter = GLB_NLS_GUI_STATISTICS.GetString(381)
    End If
    If Me.rb_finish_date.Checked Then
      m_date_filter = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5704)
    End If
    If Me.dtp_from.Checked Then
      m_date_from = GUI_FormatDate(dtp_from.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If
    If Me.dtp_to.Checked Then
      m_date_to = GUI_FormatDate(dtp_to.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Current bet from
    If Me.ef_current_bet_from.Value <> String.Empty Then
      Me.m_current_bet_from = Me.ef_current_bet_from.Value
    End If

    ' Current bet to
    If Me.ef_current_bet_to.Value <> String.Empty Then
      Me.m_current_bet_to = Me.ef_current_bet_to.Value
    End If

    ' Users
    If Me.opt_all_users.Checked Then
      Me.m_employee_id = Me.opt_all_users.Text
    Else
      Me.m_employee_id = Me.cmb_user.TextValue
    End If

    ' Account
    Me.m_card_account = Me.uc_account_sel1.Account()
    Me.m_card_track_data = Me.uc_account_sel1.TrackData()
    Me.m_card_holder_name = Me.uc_account_sel1.Holder()
    Me.m_holder_is_vip = Me.uc_account_sel1.HolderIsVip()

    ' Gaming Tables
    If uc_checked_list_groups.SelectedIndexes.Length = uc_checked_list_groups.Count() Then
      Me.m_gaming_tables = GLB_NLS_GUI_PLAYER_TRACKING.GetString(287) ' All
    ElseIf uc_checked_list_groups.SelectedIndexes.Length > 1 Then
      Me.m_gaming_tables = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1808) ' Several
    ElseIf uc_checked_list_groups.SelectedIndexes.Length = 1 Then
      Me.m_gaming_tables = Me.uc_checked_list_groups.SelectedValuesList ' Only one
    End If

    ' Session Status 
    If Me.game_status.Visible Then
      If Me.game_status.IsOneChecked Then
        m_session_status = Me.game_status.Combo.TextValue
      Else
        m_session_status = GLB_NLS_GUI_AUDITOR.GetString(263) ' All
      End If
    End If

    ' Manual Entry
    If chk_manual_entry_yes.Checked Xor chk_manual_entry_no.Checked Then
      If chk_manual_entry_yes.Checked Then
        m_manual_entry = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
      End If
      If chk_manual_entry_no.Checked Then
        m_manual_entry = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)
      End If
    End If

  End Sub ' GUI_ReportUpdateFilters 

  'PURPOSE: Executed before start Edition
  '         
  ' PARAMS:
  '    - INPUT :
  '             Row
  '             Column
  '             EditionCanceled
  '
  '    - OUTPUT :
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeStartEditionEvent(ByVal Row As Integer, ByVal Column As Integer, ByRef EditionCanceled As Boolean)
    Dim _column_account_type_value As Int32
    Dim _finish_date As DateTime

    If Row < m_query_as_datatable.Rows.Count Then
      _finish_date = m_query_as_datatable.Rows(Row).Item(SQL_COLUMN_FINISH_DATE)
    Else
      ' Is total row
      _finish_date = Nothing
    End If

    EditionCanceled = Not IsValidDataRow(Row, _finish_date)

    If Not Int32.TryParse(Me.Grid.Cell(Row, GRID_COLUMN_ACCOUNT_TYPE).Value, _column_account_type_value) Then
      _column_account_type_value = 0
    End If

    If Column = GRID_COLUMN_POINTS AndAlso _
        (_column_account_type_value = AccountType.ACCOUNT_VIRTUAL_TERMINAL Or _
         m_elp_mode > 0 Or _
         Me.Grid.Cell(Row, GRID_COLUMN_NAME).Value = "") Then
      EditionCanceled = True
    End If

    ' DCS 12-01-2015: The GUI_CellDataChangedEvent function jumps 2 times because of control events uc_grid. Therefore added variable for not display two tiems the error message pop-up.
    m_show_message = True

  End Sub ' GUI_BeforeStartEditionEvent

  'PURPOSE: Executed when cell data change
  '         
  ' PARAMS:
  '    - INPUT :
  '             Row
  '             Column
  '
  '    - OUTPUT :
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer) ' GUI_CellDataChangedEvent
    Dim _new_value As Integer

    If Me.m_query_as_datatable Is Nothing Then
      Return
    End If

    If m_error_null_value Then
      Return
    End If

    If String.Equals(Me.Grid.Cell(Row, Column).Value, String.Empty) Then
      If m_show_message Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), _
                         ENUM_MB_TYPE.MB_TYPE_ERROR, _
                         ENUM_MB_BTN.MB_BTN_OK, , Me.Grid.Column(Column).Header(1).Text)
      End If
      m_error_null_value = True
      ' DCS 12-01-2015: The GUI_CellDataChangedEvent function jumps 2 times because of control events uc_grid. Therefore added variable for not display two tiems the error message pop-up.
      m_show_message = False
    Else
      _new_value = Me.Grid.Cell(Row, Column).Value
    End If

    Select Case Column
      Case GRID_COLUMN_POINTS
        If _new_value < 0 Or m_error_null_value Then
          Me.Grid.Cell(Row, GRID_COLUMN_POINTS).Value = GUI_FormatNumber(Me.m_query_as_datatable.Rows(Row)(SQL_COLUMN_POINTS, DataRowVersion.Original), 2)
        Else
          Me.m_query_as_datatable.Rows(Row)(SQL_COLUMN_POINTS) = _new_value
        End If
      Case GRID_COLUMN_PLAYED_CURRENT
        If m_error_null_value Then
          Me.Grid.Cell(Row, GRID_COLUMN_PLAYED_CURRENT).Value = GUI_FormatCurrency(Me.m_query_as_datatable.Rows(Row)(SQL_COLUMN_PLAYED_CURRENT, DataRowVersion.Original), _
                                                                                   2, _
                                                                                   ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, _
                                                                                   False)
        Else
          Me.m_query_as_datatable.Rows(Row)(SQL_COLUMN_PLAYED_CURRENT) = _new_value
        End If
      Case GRID_COLUMN_PLAYED_AMOUNT
        If m_error_null_value Then
          Me.Grid.Cell(Row, GRID_COLUMN_PLAYED_AMOUNT).Value = GUI_FormatCurrency(Me.m_query_as_datatable.Rows(Row)(SQL_COLUMN_PLAYED_AMOUNT, DataRowVersion.Original), _
                                                                                   2, _
                                                                                   ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, _
                                                                                   False)
        Else
          Me.m_query_as_datatable.Rows(Row)(SQL_COLUMN_PLAYED_AMOUNT) = _new_value
        End If
      Case GRID_COLUMN_AVERAGE_PLAYED
        If m_error_null_value Then
          Me.Grid.Cell(Row, GRID_COLUMN_AVERAGE_PLAYED).Value = GUI_FormatCurrency(Me.m_query_as_datatable.Rows(Row)(SQL_COLUMN_AVERAGE_PLAYED, DataRowVersion.Original), _
                                                                                   2, _
                                                                                   ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, _
                                                                                   False)
        Else
          Me.m_query_as_datatable.Rows(Row)(SQL_COLUMN_AVERAGE_PLAYED) = _new_value
        End If
      Case GRID_COLUMN_PLAYED_MIN
        If m_error_null_value Then
          Me.Grid.Cell(Row, GRID_COLUMN_PLAYED_MIN).Value = GUI_FormatCurrency(Me.m_query_as_datatable.Rows(Row)(SQL_COLUMN_PLAYED_MIN, DataRowVersion.Original), _
                                                                                   2, _
                                                                                   ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, _
                                                                                   False)
        Else
          Me.m_query_as_datatable.Rows(Row)(SQL_COLUMN_PLAYED_MIN) = _new_value
        End If
      Case GRID_COLUMN_PLAYED_MAX
        If m_error_null_value Then
          Me.Grid.Cell(Row, GRID_COLUMN_PLAYED_MAX).Value = GUI_FormatCurrency(Me.m_query_as_datatable.Rows(Row)(SQL_COLUMN_PLAYED_MAX, DataRowVersion.Original), _
                                                                                   2, _
                                                                                   ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, _
                                                                                   False)
        Else
          Me.m_query_as_datatable.Rows(Row)(SQL_COLUMN_PLAYED_MAX) = _new_value
        End If
        ' DCS 1-10-2014: Do uneditable field and autocalculated
        'Case GRID_COLUMN_WON_AMOUNT
        '  Me.m_query_as_datatable.Rows(Row)(SQL_COLUMN_WON_AMOUNT) = _new_value
      Case GRID_COLUMN_CHIPS_IN
        If m_error_null_value Then
          Me.Grid.Cell(Row, GRID_COLUMN_CHIPS_IN).Value = GUI_FormatNumber(Me.m_query_as_datatable.Rows(Row)(SQL_COLUMN_CHIPS_IN, DataRowVersion.Original))
        Else
          Me.m_query_as_datatable.Rows(Row)(SQL_COLUMN_CHIPS_IN) = _new_value
        End If
      Case GRID_COLUMN_CHIPS_OUT
        If m_error_null_value Then
          Me.Grid.Cell(Row, GRID_COLUMN_CHIPS_OUT).Value = GUI_FormatNumber(Me.m_query_as_datatable.Rows(Row)(SQL_COLUMN_CHIPS_OUT, DataRowVersion.Original))
        Else
          Me.m_query_as_datatable.Rows(Row)(SQL_COLUMN_CHIPS_OUT) = _new_value
        End If
      Case GRID_COLUMN_CHIPS_TOTAL_BUY
        If m_error_null_value Then
          Me.Grid.Cell(Row, GRID_COLUMN_CHIPS_TOTAL_BUY).Value = GUI_FormatNumber(Me.m_query_as_datatable.Rows(Row)(SQL_COLUMN_CHIPS_TOTAL_BUY, DataRowVersion.Original))
        Else
          Me.m_query_as_datatable.Rows(Row)(SQL_COLUMN_CHIPS_TOTAL_BUY) = _new_value
        End If
    End Select

    ' DCS 1-10-2014: Do uneditable field and autocalculated
    If (Column = GRID_COLUMN_CHIPS_IN) Or (Column = GRID_COLUMN_CHIPS_OUT) Or (Column = GRID_COLUMN_CHIPS_TOTAL_BUY) Then
      Me.m_query_as_datatable.Rows(Row)(SQL_COLUMN_WON_AMOUNT) = Me.m_query_as_datatable.Rows(Row)(SQL_COLUMN_CHIPS_IN) + Me.m_query_as_datatable.Rows(Row)(SQL_COLUMN_CHIPS_TOTAL_BUY) - Me.m_query_as_datatable.Rows(Row)(SQL_COLUMN_CHIPS_OUT)
      Me.Grid.Cell(Row, GRID_COLUMN_WON_AMOUNT).Value() = GUI_FormatCurrency(Me.m_query_as_datatable.Rows(Row)(SQL_COLUMN_WON_AMOUNT), _
                                                                                2, _
                                                                                ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, _
                                                                                False)

    End If

    AddTotalRow(Me.m_query_as_datatable)

    Me.m_save_button.Enabled = Me.m_is_edit_mode AndAlso Me.m_user_permissions.Write AndAlso IsPendingChanges()

    m_error_null_value = False

  End Sub ' GUI_CellDataChangedEvent

  ' PURPOSE: Process clicks on data grid (doible-clicks) and select button
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON) ' GUI_ButtonClick
    Select Case ButtonId
      Case frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2 ' Save button
        SavePendingChanges()
      Case frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY
        If Me.m_is_edit_mode AndAlso IsPendingChanges() Then
          If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(122), _
                        ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        ENUM_MB_BTN.MB_BTN_YES_NO, _
                        ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then
            Return
          Else
            AcceptChanges()
          End If
        End If
        MyBase.GUI_ButtonClick(ButtonId)

        If (Me.Grid.NumRows > 0) Then
          GUI_RowSelectedEvent(0)
        End If

      Case frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0
        ShowSessionMovements()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_3 ' Add button
        MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_NEW)
      Case ENUM_BUTTON.BUTTON_SELECT
        If Not Me.m_is_edit_mode Then
          ShowSessionMovements()
        End If

      Case Else
        MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub ' GUI_ButtonClick

  'PURPOSE: Executed just before closing form
  '         
  ' PARAMS:
  '    - INPUT :
  '
  '    - OUTPUT :
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)
    If Me.m_is_edit_mode AndAlso IsPendingChanges() Then
      CloseCanceled = Not DiscardChanges()
    End If
  End Sub ' GUI_Closing

  Protected Overrides Sub GUI_EditNewItem()
    Dim _frm As frm_gaming_tables_session_edit

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_gaming_tables_session_edit
    _frm.ShowNewItem()

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub ' GUI_EditNewItem

#End Region

#Region "Private"

  ' PURPOSE: Sets the default date values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()
    Dim _today_opening As Date

    _today_opening = WSI.Common.Misc.TodayOpening()

    ' Dates
    dtp_from.Value = _today_opening
    dtp_to.Value = _today_opening.AddDays(1)
    Me.rb_finish_date.Checked = True
    Me.rb_historic.Checked = True
    Me.rb_open_sessions.Checked = False
    Me.rb_ini_date.Checked = False
    Me.game_status.Enabled = True
    Me.dtp_from.Enabled = True
    Me.dtp_to.Enabled = True
    Me.dtp_to.Checked = True
    Me.dtp_from.Checked = True

    ' Average bet
    Me.ef_current_bet_from.Value = String.Empty
    Me.ef_current_bet_to.Value = String.Empty

    ' Game status
    Me.game_status.SetDefaultValues()
    If Me.m_is_edit_mode Then
      game_status.SelectOne = 1
      game_status.Visible = False
    End If


    ' Users
    Me.opt_all_users.Checked = True
    Me.cmb_user.Enabled = False
    Me.chk_show_all.Checked = False
    Me.chk_show_all.Enabled = False

    ' Account
    Me.uc_account_sel1.Clear()

    ' Gaming Tables
    uc_checked_list_groups.Clear()
    FillGroups()
    Me.uc_checked_list_groups.SetDefaultValue(True)
    Me.uc_checked_list_groups.CollapseAll()

    ' uc_multi_curreny_site_sel
    Me.uc_multicurrency.OpenModeControl = uc_multi_currency_site_sel.OPEN_MODE.OnlyCurrency
    Me.uc_multicurrency.Init()
    Me.uc_multicurrency.Size = New Size(Me.uc_multicurrency.Size.Width, Me.uc_multicurrency.Size.Height + 10)


  End Sub ' SetDefaultValues

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()

    Dim _str_name_header As String

    _str_name_header = String.Empty

    SetCounterColors()

    With Me.Grid
      .Init(GRID_COLUMNS_COUNT, GRID_HEADERS_COUNT, True)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_INDEX).Header(1).Text = String.Empty
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      'TABLE

      ' Session Id.
      .Column(GRID_COLUMN_GAME_TABLE_PLAY_SESSION_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4419)
      .Column(GRID_COLUMN_GAME_TABLE_PLAY_SESSION_ID).Header(1).Text = String.Empty
      .Column(GRID_COLUMN_GAME_TABLE_PLAY_SESSION_ID).Width = GRID_WIDTH_GAME_TABLE_PLAY_SESSION_ID

      ' Table Type
      .Column(GRID_COLUMN_GAME_TABLE_TYPE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4419)
      .Column(GRID_COLUMN_GAME_TABLE_TYPE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(254)
      .Column(GRID_COLUMN_GAME_TABLE_TYPE).Width = GRID_WIDTH_GAME_TABLE_TYPE
      .Column(GRID_COLUMN_GAME_TABLE_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Table name
      .Column(GRID_COLUMN_GAME_TABLE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4419)
      .Column(GRID_COLUMN_GAME_TABLE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4979)
      .Column(GRID_COLUMN_GAME_TABLE).Width = GRID_WIDTH_GAME_TABLE
      .Column(GRID_COLUMN_GAME_TABLE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Attendant
      .Column(GRID_COLUMN_ATTENDANT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4419)
      .Column(GRID_COLUMN_ATTENDANT).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(220)
      .Column(GRID_COLUMN_ATTENDANT).Width = GRID_WIDTH_ATTENDANT
      .Column(GRID_COLUMN_ATTENDANT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Seat
      .Column(GRID_COLUMN_SEAT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4419)
      .Column(GRID_COLUMN_SEAT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4975)
      .Column(GRID_COLUMN_SEAT).Width = GRID_WIDTH_SEAT
      .Column(GRID_COLUMN_SEAT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' SESSION

      ' Started
      .Column(GRID_COLUMN_INITIAL_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4531)
      .Column(GRID_COLUMN_INITIAL_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5285)
      .Column(GRID_COLUMN_INITIAL_DATE).Width = GRID_WIDTH_INITIAL_DATE
      .Column(GRID_COLUMN_INITIAL_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Finished
      .Column(GRID_COLUMN_FINISH_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4531)
      .Column(GRID_COLUMN_FINISH_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5286)
      .Column(GRID_COLUMN_FINISH_DATE).Width = GRID_WIDTH_FINISH_DATE
      .Column(GRID_COLUMN_FINISH_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Status
      .Column(GRID_COLUMN_GAME_STATUS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4531)
      .Column(GRID_COLUMN_GAME_STATUS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4980)
      .Column(GRID_COLUMN_GAME_STATUS).Width = GRID_WIDTH_GAME_STATUS
      .Column(GRID_COLUMN_GAME_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Gaming time
      .Column(GRID_COLUMN_TIME_AT_TABLE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4531)
      .Column(GRID_COLUMN_TIME_AT_TABLE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4977)
      .Column(GRID_COLUMN_TIME_AT_TABLE).Width = GRID_WIDTH_TIME_AT_TABLE
      .Column(GRID_COLUMN_TIME_AT_TABLE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Walk
      .Column(GRID_COLUMN_TIME_OFF_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4531)
      .Column(GRID_COLUMN_TIME_OFF_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4984)
      .Column(GRID_COLUMN_TIME_OFF_AMOUNT).Width = GRID_WIDTH_TIME_OFF_AMOUNT
      .Column(GRID_COLUMN_TIME_OFF_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Plays count
      .Column(GRID_COLUMN_PLAYS_COUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4531)
      .Column(GRID_COLUMN_PLAYS_COUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4983)
      .Column(GRID_COLUMN_PLAYS_COUNT).Width = GRID_WIDTH_PLAYS_COUNT
      .Column(GRID_COLUMN_PLAYS_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Game speed
      .Column(GRID_COLUMN_SPEED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4531)
      .Column(GRID_COLUMN_SPEED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5019)
      .Column(GRID_COLUMN_SPEED).Width = GRID_WIDTH_SPEED
      .Column(GRID_COLUMN_SPEED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Game skill
      .Column(GRID_COLUMN_SKILL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4531)
      .Column(GRID_COLUMN_SKILL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5024)
      .Column(GRID_COLUMN_SKILL).Width = GRID_WIDTH_SKILL
      .Column(GRID_COLUMN_SKILL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' ACCOUNT

      ' Account Id.
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COLUMN_ACCOUNT_ID).Width = GRID_WIDTH_ACCOUNT_ID
      .Column(GRID_COLUMN_ACCOUNT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Account name
      .Column(GRID_COLUMN_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_NAME).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(235)
      .Column(GRID_COLUMN_NAME).Width = GRID_WIDTH_NAME
      .Column(GRID_COLUMN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT


      ' POINTS

      ' Computed points
      .Column(GRID_COLUMN_COMPUTED_POINTS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2612)
      .Column(GRID_COLUMN_COMPUTED_POINTS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2683)
      .Column(GRID_COLUMN_COMPUTED_POINTS).Width = GRID_WIDTH_COMPUTED_POINTS
      .Column(GRID_COLUMN_COMPUTED_POINTS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Points
      .Column(GRID_COLUMN_POINTS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2612)
      .Column(GRID_COLUMN_POINTS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2684)
      .Column(GRID_COLUMN_POINTS).Width = GRID_WIDTH_POINTS
      .Column(GRID_COLUMN_POINTS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' PLAYED

      ' Iso code
      .Column(GRID_COLUMN_ISO_CODE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2116)
      .Column(GRID_COLUMN_ISO_CODE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2430)
      .Column(GRID_COLUMN_ISO_CODE).Width = GRID_WIDTH_ISO_CODE
      .Column(GRID_COLUMN_ISO_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Played current
      .Column(GRID_COLUMN_PLAYED_CURRENT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2116)
      .Column(GRID_COLUMN_PLAYED_CURRENT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4486)
      .Column(GRID_COLUMN_PLAYED_CURRENT).Width = GRID_WIDTH_PLAYED_CURRENT
      .Column(GRID_COLUMN_PLAYED_CURRENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Played amount
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2116)
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3372)
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Width = GRID_WIDTH_PLAYED_AMOUNT
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Average played
      .Column(GRID_COLUMN_AVERAGE_PLAYED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2116)
      .Column(GRID_COLUMN_AVERAGE_PLAYED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4981)
      .Column(GRID_COLUMN_AVERAGE_PLAYED).Width = GRID_WIDTH_AVERAGE_PLAYED
      .Column(GRID_COLUMN_AVERAGE_PLAYED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Min played
      .Column(GRID_COLUMN_PLAYED_MIN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2116)
      .Column(GRID_COLUMN_PLAYED_MIN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(420)
      .Column(GRID_COLUMN_PLAYED_MIN).Width = GRID_WIDTH_PLAYED_MIN
      .Column(GRID_COLUMN_PLAYED_MIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Max played
      .Column(GRID_COLUMN_PLAYED_MAX).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2116)
      .Column(GRID_COLUMN_PLAYED_MAX).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(845)
      .Column(GRID_COLUMN_PLAYED_MAX).Width = GRID_WIDTH_PLAYED_MAX
      .Column(GRID_COLUMN_PLAYED_MAX).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' CHIPS

      ' Chips IN 
      .Column(GRID_COLUMN_CHIPS_IN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2941)
      .Column(GRID_COLUMN_CHIPS_IN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5287)
      .Column(GRID_COLUMN_CHIPS_IN).Width = GRID_WIDTH_CHIPS_IN
      .Column(GRID_COLUMN_CHIPS_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Chips out
      .Column(GRID_COLUMN_CHIPS_OUT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2941)
      .Column(GRID_COLUMN_CHIPS_OUT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5288)
      .Column(GRID_COLUMN_CHIPS_OUT).Width = GRID_WIDTH_CHIPS_OUT
      .Column(GRID_COLUMN_CHIPS_OUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Chips total buy IN 
      .Column(GRID_COLUMN_CHIPS_TOTAL_BUY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2941)
      If WSI.Common.GamingTableBusinessLogic.IsEnableCashDeskDealerTicketsValidation() Then
        .Column(GRID_COLUMN_CHIPS_TOTAL_BUY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8063)                                                                     ' 8063 : Validated tickets
      Else
        If (GeneralParam.GetString("GamingTable.PlayerTracking", "PlayerTracking.SellChips.Text", String.Empty) = String.Empty) Then
          .Column(GRID_COLUMN_CHIPS_TOTAL_BUY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5289)                                                                     ' 5289 : Total buy - IN
        Else
          .Column(GRID_COLUMN_CHIPS_TOTAL_BUY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5073, GeneralParam.GetString("GamingTable.PlayerTracking", "PlayerTracking.SellChips.Text")) ' 5073 :  Total %1   
        End If
      End If

      .Column(GRID_COLUMN_CHIPS_TOTAL_BUY).Width = GRID_WIDTH_CHIPS_TOTAL_BUY
      .Column(GRID_COLUMN_CHIPS_TOTAL_BUY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ''' WON

      ' Won amount
      .Column(GRID_COLUMN_WON_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2941)
      .Column(GRID_COLUMN_WON_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(539)
      .Column(GRID_COLUMN_WON_AMOUNT).Width = GRID_WIDTH_WON_AMOUNT
      .Column(GRID_COLUMN_WON_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      If Me.m_is_edit_mode Then

        '
        ' EDITABLE COLUMNS
        '

        ' Points
        .Column(GRID_COLUMN_POINTS).Editable = True
        .Column(GRID_COLUMN_POINTS).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
        .Column(GRID_COLUMN_POINTS).EditionControl.EntryField.SetFilter(FORMAT_NUMBER, 7, 2)

        ' Played amount
        .Column(GRID_COLUMN_PLAYED_CURRENT).Editable = True
        .Column(GRID_COLUMN_PLAYED_CURRENT).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
        .Column(GRID_COLUMN_PLAYED_CURRENT).EditionControl.EntryField.SetFilter(FORMAT_MONEY_ALSO_NEGATIVES, 7, 2)

        ' Played amount
        .Column(GRID_COLUMN_PLAYED_AMOUNT).Editable = True
        .Column(GRID_COLUMN_PLAYED_AMOUNT).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
        .Column(GRID_COLUMN_PLAYED_AMOUNT).EditionControl.EntryField.SetFilter(FORMAT_MONEY_ALSO_NEGATIVES, 7, 2)

        ' Average played
        .Column(GRID_COLUMN_AVERAGE_PLAYED).Editable = True
        .Column(GRID_COLUMN_AVERAGE_PLAYED).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
        .Column(GRID_COLUMN_AVERAGE_PLAYED).EditionControl.EntryField.SetFilter(FORMAT_MONEY_ALSO_NEGATIVES, 7, 2)

        ' Min played
        .Column(GRID_COLUMN_PLAYED_MIN).Editable = True
        .Column(GRID_COLUMN_PLAYED_MIN).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
        .Column(GRID_COLUMN_PLAYED_MIN).EditionControl.EntryField.SetFilter(FORMAT_MONEY_ALSO_NEGATIVES, 7, 2)

        ' Max played
        .Column(GRID_COLUMN_PLAYED_MAX).Editable = True
        .Column(GRID_COLUMN_PLAYED_MAX).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
        .Column(GRID_COLUMN_PLAYED_MAX).EditionControl.EntryField.SetFilter(FORMAT_MONEY_ALSO_NEGATIVES, 7, 2)

        ' DCS 1-10-2014: Do uneditable field and autocalculated
        '' Won amount
        '.Column(GRID_COLUMN_WON_AMOUNT).Editable = True
        '.Column(GRID_COLUMN_WON_AMOUNT).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
        '.Column(GRID_COLUMN_WON_AMOUNT).EditionControl.EntryField.SetFilter(FORMAT_MONEY_ALSO_NEGATIVES, 7, 2)

        ' Chips IN 
        .Column(GRID_COLUMN_CHIPS_IN).Editable = True
        .Column(GRID_COLUMN_CHIPS_IN).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
        .Column(GRID_COLUMN_CHIPS_IN).EditionControl.EntryField.SetFilter(FORMAT_NUMBER, 7, 2)

        ' Chips out
        .Column(GRID_COLUMN_CHIPS_OUT).Editable = True
        .Column(GRID_COLUMN_CHIPS_OUT).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
        .Column(GRID_COLUMN_CHIPS_OUT).EditionControl.EntryField.SetFilter(FORMAT_NUMBER, 7, 2)

      End If

      .Column(GRID_COLUMN_ACCOUNT_TYPE).Header(0).Text = ""
      .Column(GRID_COLUMN_ACCOUNT_TYPE).Header(1).Text = ""
      .Column(GRID_COLUMN_ACCOUNT_TYPE).Width = 0

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Sets counter colors for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetCounterColors()
    With Me.Grid
      Me.m_counter_imported = 0
      .Counter(COUNTER_IMPORTED).Visible = True
      .Counter(COUNTER_IMPORTED).BackColor = GetColor(COLOR_IMPORTED)
      .Counter(COUNTER_IMPORTED).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)

      Me.m_counter_not_imported = 0
      .Counter(COUNTER_NOT_IMPORTED).Visible = True
      .Counter(COUNTER_NOT_IMPORTED).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
      .Counter(COUNTER_NOT_IMPORTED).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
    End With
  End Sub ' SetCounterColors

  ' PURPOSE: Fill uc_checked_list control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS
  '
  Private Sub FillGroups()

    ' Fill CheckedList control with groups
    Me.uc_checked_list_groups.Clear()
    Me.uc_checked_list_groups.ReDraw(False)

    For Each _group As GamingTablesGroup In Me.m_all_gaming_tables
      uc_checked_list_groups.Add(1, _group.Id, _group.Type)
      For Each _gaming_table As GamingTable In _group.GamingTables
        uc_checked_list_groups.Add(2, _gaming_table.Id, "  " + _gaming_table.Name)
      Next
    Next

    If Me.uc_checked_list_groups.Count > 0 Then
      Me.uc_checked_list_groups.ColumnWidth(3, 4450)
      Me.uc_checked_list_groups.CurrentRow(0)
      Me.uc_checked_list_groups.ReDraw(True)
    End If

  End Sub ' FillGroups

  ' PURPOSE: Get all gaming tables
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: Gaming tables list, order by table type.
  '
  Private Function LoadGamingTablesByType() As List(Of GamingTablesGroup)
    Dim _tables_groups As List(Of GamingTablesGroup)
    Dim _str_sql As StringBuilder
    Dim _dt_counters As DataTable
    Dim _group_id As Integer
    Dim _new_gaming_table As GamingTable
    Dim _new_tables_group As GamingTablesGroup

    _tables_groups = New List(Of GamingTablesGroup)
    _new_tables_group = New GamingTablesGroup()
    _str_sql = New StringBuilder()
    _dt_counters = New DataTable()
    _group_id = 0

    Try

      Using _db_trx As DB_TRX = New DB_TRX()

        _str_sql.AppendLine("     SELECT   GTT_GAMING_TABLE_TYPE_ID              ")
        _str_sql.AppendLine("            , GTT_NAME                              ")
        _str_sql.AppendLine("            , GTT_ENABLED                           ")
        _str_sql.AppendLine("            , GT_GAMING_TABLE_ID                    ")
        _str_sql.AppendLine("            , GT_NAME                               ")
        _str_sql.AppendLine("       FROM   GAMING_TABLES_TYPES                   ")
        _str_sql.AppendLine(" INNER JOIN   GAMING_TABLES                         ")
        _str_sql.AppendLine("         ON   GTT_GAMING_TABLE_TYPE_ID = GT_TYPE_ID ")

        Using _sql_cmd As SqlCommand = New SqlCommand(_str_sql.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          Using _sql_adapter As SqlDataAdapter = New SqlDataAdapter(_sql_cmd)

            If _sql_adapter.Fill(_dt_counters) > 0 Then

              For Each _row As DataRow In _dt_counters.Rows

                If _group_id <> CInt(_row(0)) Then
                  _group_id = _row(0) ' GroupId
                  _new_tables_group = New GamingTablesGroup()
                  _new_tables_group.GamingTables = New List(Of GamingTable)
                  _new_tables_group.Id = _group_id
                  _new_tables_group.Type = _row(1)
                  _new_tables_group.Enabled = _row(2)
                  _tables_groups.Add(_new_tables_group)
                End If

                _new_gaming_table = New GamingTable()
                _new_gaming_table.Id = _row(3)
                _new_gaming_table.Name = _row(4)
                _tables_groups(_tables_groups.Count - 1).GamingTables.Add(_new_gaming_table)

              Next
            End If
          End Using
        End Using
      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return _tables_groups
  End Function ' LoadGamingTablesByType

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  '
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _sb As System.Text.StringBuilder
    _sb = New System.Text.StringBuilder()

    ' Get selected currency iso code
    m_selected_currency = uc_multicurrency.SelectedCurrency

    ' Table
    _sb.AppendLine("     SELECT   GTPS_PLAY_SESSION_ID                                             ")
    _sb.AppendLine("            , GTT_NAME                                                         ")
    _sb.AppendLine("            , GT_NAME                                                          ")
    _sb.AppendLine("            , GU_USERNAME AS ATTENDANT                                         ")
    _sb.AppendLine("            , GTS_POSITION                                                     ")

    ' Session
    _sb.AppendLine("            , GTPS_STARTED                                                     ")
    _sb.AppendLine("            , GTPS_FINISHED                                                    ")
    _sb.AppendLine("            , GTPS_STATUS                                                      ")
    _sb.AppendLine("            , CASE WHEN GTPS_STATUS = " & GTPlaySessionStatus.Opened & "       ")
    _sb.AppendLine("                   THEN DATEDIFF(SECOND, GTPS_STARTED, GETDATE()) - ISNULL(GTPS_WALK, 0)      ")
    _sb.AppendLine("                   ELSE ISNULL(GTPS_GAME_TIME, 0)                                             ") ' Walk or Close.
    _sb.AppendLine("                   END AS GTPS_GAME_TIME                                                      ")
    _sb.AppendLine("            , CASE WHEN GTPS_STATUS = " & GTPlaySessionStatus.Away & "                        ")
    _sb.AppendLine("                   THEN ISNULL(GTPS_WALK, 0) + DATEDIFF(SECOND, GTPS_START_WALK, GETDATE())   ")
    _sb.AppendLine("                   ELSE ISNULL(GTPS_WALK, 0)                                                  ") ' Walk or Close.
    _sb.AppendLine("                   END AS GTPS_WALK                                            ")
    _sb.AppendLine("            , ISNULL(GTPS_PLAYS, 0) AS GTPS_PLAYS                              ")

    ' Account
    _sb.AppendLine("            , GTPS_ACCOUNT_ID                                                  ")
    _sb.AppendLine("            , AC_HOLDER_NAME                                                   ")
    _sb.AppendLine("            , GTPS_PLAYER_SPEED                                                ")
    _sb.AppendLine("            , GTPS_PLAYER_SKILL                                                ")

    ' Points
    _sb.AppendLine("            , ISNULL(GTPS_COMPUTED_POINTS, 0) AS GTPS_COMPUTED_POINTS          ")
    _sb.AppendLine("            , ISNULL(GTPS_POINTS, 0) AS GTPS_POINTS                            ")

    ' Played
    _sb.AppendLine("            , ISNULL(GTPS_ISO_CODE, '')                                        ")
    _sb.AppendLine("            , GTPS_CURRENT_BET                                                 ")
    _sb.AppendLine("            , GTPS_PLAYED_AMOUNT                                               ")
    _sb.AppendLine("            , GTPS_PLAYED_AVERAGE                                              ")
    _sb.AppendLine("            , GTPS_BET_MIN                                                     ")
    _sb.AppendLine("            , GTPS_BET_MAX                                                     ")

    ' Chips
    _sb.AppendLine("            , ISNULL(GTPS_CHIPS_IN,0) AS GTPS_CHIPS_IN                         ")
    _sb.AppendLine("            , ISNULL(GTPS_CHIPS_OUT,0) AS GTPS_CHIPS_OUT                       ")
    _sb.AppendLine("            , ISNULL(GTPS_TOTAL_SELL_CHIPS,0) AS GTPS_TOTAL_SELL_CHIPS         ")

    ' Won
    _sb.AppendLine("            , GTPS_NETWIN                                                      ")

    _sb.AppendLine("            , AC_TYPE                                                          ")
    _sb.AppendLine("            , ISNULL(GTS_TERMINAL_ID, 0)                                       ")

    _sb.AppendLine("       FROM   GT_PLAY_SESSIONS                                                 " & GetSQLIndex())
    _sb.AppendLine(" INNER JOIN   ACCOUNTS                                                         ")
    _sb.AppendLine("         ON   GTPS_ACCOUNT_ID = AC_ACCOUNT_ID                                  ")
    _sb.AppendLine(" INNER JOIN   GAMING_TABLES                                                    ")
    _sb.AppendLine("         ON   GTPS_GAMING_TABLE_ID = GT_GAMING_TABLE_ID                        ")
    _sb.AppendLine(" INNER JOIN   GAMING_TABLES_TYPES                                              ")
    _sb.AppendLine("         ON   GT_TYPE_ID = GTT_GAMING_TABLE_TYPE_ID                            ")
    _sb.AppendLine("  LEFT JOIN   GT_SEATS                                                         ")
    _sb.AppendLine("         ON   GTS_SEAT_ID = GTPS_SEAT_ID                                       ")
    _sb.AppendLine("  LEFT JOIN   GUI_USERS                                                        ")
    _sb.AppendLine("         ON   GU_USER_ID = ISNULL(GTPS_FINISHED_USER_ID, GTPS_UPDATED_USER_ID) ")

    _sb.AppendLine(GetSqlWhere())

    _sb.AppendLine(GetSqlOrderBy())

    ' Set data table
    Me.m_query_as_datatable = GUI_GetTableUsingSQL(_sb.ToString(), Integer.MaxValue)

    Return _sb.ToString()
  End Function ' GUI_FilterGetSqlQuery

  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - The string to use the index for the SQL query
  '
  Private Function GetSQLIndex() As String
    Dim _str_index As String

    '-	Si seleccionada sesiones abiertas IX_gtps_started_status
    '-	Si seleccionada fecha de inicio IX_gtps_started_status
    _str_index = INDEX_STARTED_STATUS

    '-	Si seleccionada fecha de fin IX_gtps_finished_status
    If Me.rb_finish_date.Checked Then
      _str_index = INDEX_FINISHED_STATUS
    End If

    'Si se ha introducido alguna cuenta y seleccionada fecha de inicio IX_gtps_account_id_started O  seleccionada fecha de fin IX_gtps_account_id_finished
    If String.IsNullOrEmpty(Me.uc_account_sel1.GetFilterSQL()) Then
      If Me.rb_finish_date.Checked Then
        _str_index = INDEX_ACCOUNTS_STARTED
      Else
        _str_index = INDEX_ACCOUNT_FINISHED
      End If
    End If

    Return _str_index
  End Function

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the main SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String

    Dim _result As String
    Dim _sb_where As StringBuilder
    Dim _account_filter As String
    Dim _pattern As String
    Dim _rgx As Regex

    _sb_where = New System.Text.StringBuilder()

    ' Filter Open Sessions / Historic sessions
    If Me.rb_open_sessions.Checked Then
      _sb_where.AppendLine(" WHERE GTPS_STATUS = " & WSI.Common.GTPlaySessionStatus.Opened)
    Else
      ' Filter Dates
      If Me.rb_ini_date.Checked Then
        If Me.dtp_from.Checked = True Then
          _sb_where.AppendLine(" WHERE (GTPS_STARTED >= " & GUI_FormatDateDB(dtp_from.Value) & ") ")
        End If

        If Me.dtp_to.Checked = True Then
          _sb_where.AppendLine(" AND (GTPS_STARTED < " & GUI_FormatDateDB(dtp_to.Value) & ") ")
        End If
      Else
        If Me.dtp_from.Checked = True Then
          _sb_where.AppendLine(" WHERE (GTPS_FINISHED >= " & GUI_FormatDateDB(dtp_from.Value) & ") ")
        End If

        If Me.dtp_to.Checked = True Then
          _sb_where.AppendLine(" AND (GTPS_FINISHED < " & GUI_FormatDateDB(dtp_to.Value) & ") ")
        End If
      End If
    End If

    ' Average bet filter
    If Me.ef_current_bet_from.Value <> String.Empty Then
      _sb_where.AppendLine("   AND   (GTPS_CURRENT_BET >= " & GUI_LocalNumberToDBNumber(Me.ef_current_bet_from.Value) & ") ")
    End If

    If Me.ef_current_bet_to.Value <> String.Empty Then
      _sb_where.AppendLine("   AND   (GTPS_CURRENT_BET <= " & GUI_LocalNumberToDBNumber(Me.ef_current_bet_to.Value) & ") ")
    End If

    ' Game status
    If Me.game_status.IsOneChecked Then
      _sb_where.AppendLine("   AND   (GTPS_STATUS = " & Me.game_status.Combo.Value & ") ")
    End If

    ' User filter
    If Me.opt_one_user.Checked = True Then
      _sb_where.AppendLine("   AND   (GU_USER_ID = " & Me.cmb_user.Value & ") ")
    End If

    'Account filter
    _account_filter = Me.uc_account_sel1.GetFilterSQL()
    If _account_filter <> String.Empty Then
      _sb_where.AppendLine("   AND   " & _account_filter)
    End If

    ' Gaming table filter
    If Me.uc_checked_list_groups.SelectedIndexes.Length <> Me.uc_checked_list_groups.Count() Then
      _sb_where.AppendLine("   AND   GT_GAMING_TABLE_ID IN (" & Me.uc_checked_list_groups.SelectedIndexesListLevel2 & ") ")
    End If

    If chk_manual_entry_yes.Checked Xor chk_manual_entry_no.Checked Then
      If chk_manual_entry_yes.Checked Then
        _sb_where.AppendLine("   AND   GTS_POSITION IS NULL ")
      End If
      If chk_manual_entry_no.Checked Then
        _sb_where.AppendLine("   AND   NOT GTS_POSITION IS NULL ")
      End If
    End If

    _sb_where.AppendLine("   AND   GTPS_ISO_CODE IN ('" & m_selected_currency & "')")

    ' "AND" first occurrence is replaced by "WHERE"
    _result = _sb_where.ToString()

    If _result <> String.Empty Then
      _pattern = "^\s*AND\b"
      _rgx = New Regex(_pattern, RegexOptions.IgnoreCase)
      _result = _rgx.Replace(_result, " WHERE")
    End If

    Return _result
  End Function ' GetSqlWhere

  ''' <summary>
  ''' Returns the SQL order by
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetSqlOrderBy() As String
    Dim _sql_order As String

    _sql_order = "   ORDER BY   GTT_NAME, GT_NAME"
    If Me.rb_finish_date.Checked Then
      _sql_order &= ", GTPS_FINISHED DESC "
    Else
      _sql_order &= ", GTPS_STARTED DESC "
    End If

    Return _sql_order
  End Function

  ' PURPOSE: It adds to the results table a final total row
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '     - None
  Private Sub AddTotalRow(ByVal PlaySessionsDataTable As DataTable)
    Dim _idx_total_row As Integer
    Dim _total_time_playing As TimeSpan
    Dim _total_time_walking As TimeSpan
    Dim _total_games_number As Double
    Dim _total_computed_points As Double
    Dim _total_points As Double
    Dim _total_played_amount As Double
    Dim _total_won_amount As Double
    Dim _total_sell_chips As Double
    Dim _total_chips_in As Double
    Dim _total_chips_out As Double

    If PlaySessionsDataTable Is DBNull.Value OrElse PlaySessionsDataTable.Rows.Count = 0 Then
      Call AddTotalRowWithoutValues()
    Else
      _idx_total_row = Me.Grid.NumRows - 1

      ' If total row not exists, create it
      If IsValidDataRow(_idx_total_row) Then
        Me.Grid.AddRow()
      End If

      _idx_total_row = Me.Grid.NumRows - 1

      ' Label - TOTAL
      Me.Grid.Row(_idx_total_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
      Me.Grid.Cell(_idx_total_row, GRID_COLUMN_GAME_TABLE_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4708)
      Me.Grid.Cell(_idx_total_row, GRID_COLUMN_GAME_TABLE_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Time amount in playing
      _total_time_playing = New TimeSpan(0, 0, PlaySessionsDataTable.Compute("SUM(GTPS_GAME_TIME)", Nothing))
      Me.Grid.Cell(_idx_total_row, GRID_COLUMN_TIME_AT_TABLE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      Me.Grid.Cell(_idx_total_row, GRID_COLUMN_TIME_AT_TABLE).Value = TimeSpanToString(_total_time_playing)

      ' Time amount out
      _total_time_walking = New TimeSpan(0, 0, PlaySessionsDataTable.Compute("SUM(GTPS_WALK)", Nothing))
      Me.Grid.Cell(_idx_total_row, GRID_COLUMN_TIME_OFF_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      Me.Grid.Cell(_idx_total_row, GRID_COLUMN_TIME_OFF_AMOUNT).Value = TimeSpanToString(_total_time_walking)

      ' Games number
      _total_games_number = PlaySessionsDataTable.Compute("SUM(GTPS_PLAYS)", Nothing)
      Me.Grid.Cell(_idx_total_row, GRID_COLUMN_PLAYS_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      Me.Grid.Cell(_idx_total_row, GRID_COLUMN_PLAYS_COUNT).Value = GUI_FormatNumber(_total_games_number, 0)

      ' Computed points
      _total_computed_points = PlaySessionsDataTable.Compute("SUM(GTPS_COMPUTED_POINTS)", Nothing)
      Me.Grid.Cell(_idx_total_row, GRID_COLUMN_COMPUTED_POINTS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      Me.Grid.Cell(_idx_total_row, GRID_COLUMN_COMPUTED_POINTS).Value = GUI_FormatNumber(_total_computed_points, 2)

      ' Points
      _total_points = PlaySessionsDataTable.Compute("SUM(GTPS_POINTS)", Nothing)
      Me.Grid.Cell(_idx_total_row, GRID_COLUMN_POINTS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      Me.Grid.Cell(_idx_total_row, GRID_COLUMN_POINTS).Value = GUI_FormatNumber(_total_points, 2)

      ' Played amount
      _total_played_amount = PlaySessionsDataTable.Compute("SUM(GTPS_PLAYED_AMOUNT)", Nothing)
      Me.Grid.Cell(_idx_total_row, GRID_COLUMN_PLAYED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      Me.Grid.Cell(_idx_total_row, GRID_COLUMN_PLAYED_AMOUNT).Value = GUI_FormatCurrency(_total_played_amount, _
                                                                                         2, _
                                                                                         ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, _
                                                                                         False)

      ' Won amount
      _total_won_amount = PlaySessionsDataTable.Compute("SUM(GTPS_NETWIN)", Nothing)
      Me.Grid.Cell(_idx_total_row, GRID_COLUMN_WON_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      Me.Grid.Cell(_idx_total_row, GRID_COLUMN_WON_AMOUNT).Value = GUI_FormatCurrency(_total_won_amount, _
                                                                                      2, _
                                                                                      ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, _
                                                                                      False)

      ' Chips IN
      _total_chips_in = PlaySessionsDataTable.Compute("SUM(GTPS_CHIPS_IN)", Nothing)
      Me.Grid.Cell(_idx_total_row, GRID_COLUMN_CHIPS_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      Me.Grid.Cell(_idx_total_row, GRID_COLUMN_CHIPS_IN).Value = GUI_FormatNumber(_total_chips_in, 2)

      ' Chips OUT
      _total_chips_out = PlaySessionsDataTable.Compute("SUM(GTPS_CHIPS_OUT)", Nothing)
      Me.Grid.Cell(_idx_total_row, GRID_COLUMN_CHIPS_OUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      Me.Grid.Cell(_idx_total_row, GRID_COLUMN_CHIPS_OUT).Value = GUI_FormatNumber(_total_chips_out, 2)

      ' Total Sell Chips
      _total_sell_chips = PlaySessionsDataTable.Compute("SUM(GTPS_TOTAL_SELL_CHIPS)", Nothing)
      Me.Grid.Cell(_idx_total_row, GRID_COLUMN_CHIPS_TOTAL_BUY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      Me.Grid.Cell(_idx_total_row, GRID_COLUMN_CHIPS_TOTAL_BUY).Value = GUI_FormatNumber(_total_sell_chips, 2)
    End If

  End Sub ' AddTotalRow

  Private Sub AddTotalRowWithoutValues()
    Dim _idx_total_row As Integer

    _idx_total_row = Me.Grid.AddRow()

    ' Label - TOTAL
    Me.Grid.Row(_idx_total_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
    Me.Grid.Cell(_idx_total_row, GRID_COLUMN_GAME_TABLE_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4708)
    Me.Grid.Cell(_idx_total_row, GRID_COLUMN_GAME_TABLE_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    ' Time amount in playing
    Me.Grid.Cell(_idx_total_row, GRID_COLUMN_TIME_AT_TABLE).Value = ""
    ' Time amount out
    Me.Grid.Cell(_idx_total_row, GRID_COLUMN_TIME_OFF_AMOUNT).Value = ""
    ' Games number
    Me.Grid.Cell(_idx_total_row, GRID_COLUMN_PLAYS_COUNT).Value = GUI_FormatNumber(0, 0)
    ' Computed points
    Me.Grid.Cell(_idx_total_row, GRID_COLUMN_COMPUTED_POINTS).Value = GUI_FormatNumber(0, 2)
    ' Points
    Me.Grid.Cell(_idx_total_row, GRID_COLUMN_POINTS).Value = GUI_FormatNumber(0, 2)
    ' Played amount
    Me.Grid.Cell(_idx_total_row, GRID_COLUMN_PLAYED_AMOUNT).Value = GUI_FormatCurrency(0, _
                                                                                       2, _
                                                                                       ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, _
                                                                                       False)
    ' Won amount
    Me.Grid.Cell(_idx_total_row, GRID_COLUMN_WON_AMOUNT).Value = GUI_FormatCurrency(0, _
                                                                                    2, _
                                                                                    ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, _
                                                                                    False)
    ' Chips IN
    Me.Grid.Cell(_idx_total_row, GRID_COLUMN_CHIPS_IN).Value = GUI_FormatNumber(0, 2)
    ' Chips OUT
    Me.Grid.Cell(_idx_total_row, GRID_COLUMN_CHIPS_OUT).Value = GUI_FormatNumber(0, 2)
    ' Total Sell Chips
    Me.Grid.Cell(_idx_total_row, GRID_COLUMN_CHIPS_TOTAL_BUY).Value = GUI_FormatNumber(0, 2)
  End Sub
  ' PURPOSE: Check if changes have been made in the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - True: Changes have been made. False: Otherwise
  '
  Private Function IsPendingChanges() As Boolean

    If Me.m_query_as_datatable Is Nothing Then
      Return False
    End If

    For _idx_row As Integer = 0 To Me.m_query_as_datatable.Rows.Count - 1
      If Me.m_query_as_datatable.Rows(_idx_row).RowState = DataRowState.Modified Then
        For _idx_col As Integer = 0 To Me.m_query_as_datatable.Columns.Count - 1
          If Not IsDBNull(Me.m_query_as_datatable.Rows(_idx_row).Item(_idx_col)) Then
            If Me.m_query_as_datatable.Rows(_idx_row).Item(_idx_col, DataRowVersion.Current) <> Me.m_query_as_datatable.Rows(_idx_row).Item(_idx_col, DataRowVersion.Original) Then
              Return True
            End If
          End If

        Next
      End If
    Next

    Return False
  End Function ' IsPendingChanges

  ' PURPOSE: Checks whether the specified row is a valid data row
  '
  '  PARAMS:
  '     - INPUT:
  '           - IdxRow: row to check
  '           - FinishDate: (Optional value) Finish datetime of the specified row
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - TRUE: selected row contains valid data
  '     - FALSE: selected row does not exist or contains no valid data
  '
  Private Function IsValidDataRow(ByVal IdxRow As Integer, Optional ByVal FinishDate As DateTime = Nothing) As Boolean
    Dim _points_assignment_days As Integer
    _points_assignment_days = WSI.Common.GamingTableBusinessLogic.PointsAssignmentDays()

    If IdxRow >= 0 And IdxRow < Me.Grid.NumRows Then
      ' All rows with data have a non-empty value in column GRID_COLUMN_GAME_TABLE_PLAY_SESSION_ID.
      If (String.IsNullOrEmpty(Me.Grid.Cell(IdxRow, GRID_COLUMN_GAME_TABLE_PLAY_SESSION_ID).Value)) Then
        Return False
      Else
        If Not FinishDate = Nothing AndAlso _points_assignment_days > 0 AndAlso FinishDate.AddDays(_points_assignment_days) < WGDB.Now Then
          Return False
        End If

        Return True
      End If
    End If

    Return False
  End Function ' IsValidDataRow

  ' PURPOSE: Save DataGrid to DB checking data constraints
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  '
  Private Sub SavePendingChanges()
    Dim _row As DataRow
    Dim _is_all_updated As Boolean
    Dim _negative_accounts As List(Of String)
    Dim _won_bucket_list As New WonBucketDict
    Dim _account_id As Int64
    Dim _terminal_name As String
    Dim _terminal_id As Int64
    Dim _result As Boolean

    If Not (Me.m_is_edit_mode AndAlso Me.m_user_permissions.Write AndAlso IsPendingChanges()) Then

      Return
    End If

    _is_all_updated = True

    Try

      Using _db_trx As DB_TRX = New DB_TRX()

        _negative_accounts = New List(Of String)

        For _idx_row As Integer = 0 To Me.m_query_as_datatable.Rows.Count - 1
          _row = Me.m_query_as_datatable.Rows(_idx_row)
          _account_id = _row(SQL_COLUMN_ACCOUNT_ID)
          _terminal_id = _row(SQL_COLUMN_TERMINAL_ID)
          _result = False

          If _row.RowState = DataRowState.Modified Then
            _result = DB_UpdateGtPlaySessionsRegister(_row, _db_trx.SqlTransaction)

            If _result AndAlso _row(SQL_COLUMN_POINTS, DataRowVersion.Original) <> _row(SQL_COLUMN_POINTS) AndAlso _terminal_id <> 0 Then
              If (GamingTableBusinessLogic.Get_CalculateBuckets(_terminal_id, _account_id, 0, 0, 0, _won_bucket_list, _db_trx.SqlTransaction) = GamingTableBusinessLogic.GT_RESPONSE_CODE.ERROR_GENERIC) Then
                _is_all_updated = False
                Exit For
              End If

              For Each _key_value As KeyValuePair(Of Buckets.BucketId, WonBucket) In _won_bucket_list.Dict
                _key_value.Value.AccountId = _account_id
                _key_value.Value.TerminalId = _row(SQL_COLUMN_TERMINAL_ID)
                _key_value.Value.WonValue = _row(SQL_COLUMN_POINTS) - _row(SQL_COLUMN_POINTS, DataRowVersion.Original)
              Next

              _terminal_name = WSI.Common.CommonCashierInformation.AuthorizedByUserName + "/" + _row(SQL_COLUMN_GAME_TABLE) + " - " + Resource.String("STR_GTPS_SEAT", _row(SQL_COLUMN_SEAT))
              If Not GamingTableBusinessLogic.UpdateAccountBuckets(_account_id, _terminal_name, _won_bucket_list, _db_trx.SqlTransaction) Then
                _is_all_updated = False
                Exit For
              End If
            End If
          End If

          If Not _is_all_updated Then
            Exit For
          End If

        Next _idx_row

        If _is_all_updated Then
          _db_trx.Commit()
        Else
          _db_trx.Rollback()
        End If

      End Using

      If _is_all_updated Then

        AuditChanges(Me.m_query_as_datatable)

        AcceptChanges()

        If _negative_accounts.Count > 0 Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2808), _
                          mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
                          mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, _
                          , _
                          Environment.NewLine & String.Join(Environment.NewLine, _negative_accounts.ToArray()))

        End If

        NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(108), _
                   ENUM_MB_TYPE.MB_TYPE_INFO, _
                   ENUM_MB_BTN.MB_BTN_OK)
      Else
        NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(109), ENUM_MB_TYPE.MB_TYPE_ERROR)
      End If

    Catch _ex As Exception
      Log.Exception(_ex)
      NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(109), _
                 ENUM_MB_TYPE.MB_TYPE_ERROR, _
                 ENUM_MB_BTN.MB_BTN_OK, , _ex.Message)
    End Try
  End Sub ' SavePendingChanges

  ' PURPOSE: Update data for no detect changes
  '
  '  PARAMS:
  '     - INPUT:
  '           - Boolean: If we must preserve DataTable object with values
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub AcceptChanges()
    Me.m_query_as_datatable.AcceptChanges()
    Me.m_save_button.Enabled = False
  End Sub ' AcceptChanges

  ' PURPOSE: Update user changes in database
  '
  '  PARAMS:
  '     - INPUT:
  '           - DataRow: row with user changes
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - True: database has been updated. False: otherwise
  '
  Private Function DB_UpdateGtPlaySessionsRegister(ByVal CurrentGtPlaySessionsRow As DataRow, _
                                                   ByVal SqlTrans As SqlTransaction) As Boolean
    Dim _result As Boolean
    Dim _query As StringBuilder

    _query = New StringBuilder()

    Try

      Using _da As New SqlDataAdapter()

        _query.AppendLine(" UPDATE   GT_PLAY_SESSIONS                                ")
        _query.AppendLine("    SET   GTPS_POINTS            = @pCurrentPoints        ")
        _query.AppendLine("        , GTPS_CURRENT_BET    = @pCurrentPlayedCurrent  ")
        _query.AppendLine("        , GTPS_PLAYED_AMOUNT     = @pCurrentPlayedAmount  ")
        _query.AppendLine("        , GTPS_PLAYED_AVERAGE    = @pCurrentPlayedAverage ")
        _query.AppendLine("        , GTPS_BET_MIN        = @pCurrentPlayedMin     ")
        _query.AppendLine("        , GTPS_BET_MAX        = @pCurrentPlayedMax     ")
        _query.AppendLine("        , GTPS_CHIPS_IN          = @pGtChipsIN            ")
        _query.AppendLine("        , GTPS_CHIPS_OUT         = @pGtChipsOUT           ")
        _query.AppendLine("        , GTPS_TOTAL_SELL_CHIPS  = @pGtTotalChipsBuy      ")
        _query.AppendLine("        , GTPS_NETWIN        = @pCurrentWonAmount     ")
        _query.AppendLine("  WHERE   GTPS_PLAY_SESSION_ID   = @pGtPlaySessionId      ")

        Using _cmd As New SqlCommand(_query.ToString, SqlTrans.Connection, SqlTrans)
          _cmd.Parameters.Add("@pCurrentPoints", SqlDbType.Money).Value = CurrentGtPlaySessionsRow(SQL_COLUMN_POINTS)
          _cmd.Parameters.Add("@pCurrentPlayedCurrent", SqlDbType.Money).Value = CurrentGtPlaySessionsRow(SQL_COLUMN_PLAYED_CURRENT)
          _cmd.Parameters.Add("@pCurrentPlayedAmount", SqlDbType.Money).Value = CurrentGtPlaySessionsRow(SQL_COLUMN_PLAYED_AMOUNT)
          _cmd.Parameters.Add("@pCurrentPlayedAverage", SqlDbType.Money).Value = CurrentGtPlaySessionsRow(SQL_COLUMN_AVERAGE_PLAYED)
          _cmd.Parameters.Add("@pCurrentPlayedMin", SqlDbType.Money).Value = CurrentGtPlaySessionsRow(SQL_COLUMN_PLAYED_MIN)
          _cmd.Parameters.Add("@pCurrentPlayedMax", SqlDbType.Money).Value = CurrentGtPlaySessionsRow(SQL_COLUMN_PLAYED_MAX)
          _cmd.Parameters.Add("@pCurrentWonAmount", SqlDbType.Money).Value = CurrentGtPlaySessionsRow(SQL_COLUMN_WON_AMOUNT)
          _cmd.Parameters.Add("@pGtPlaySessionId", SqlDbType.BigInt).Value = CurrentGtPlaySessionsRow(SQL_COLUMN_GAME_TABLE_PLAY_SESSION_ID)
          _cmd.Parameters.Add("@pGtChipsIN", SqlDbType.BigInt).Value = CurrentGtPlaySessionsRow(SQL_COLUMN_CHIPS_IN)
          _cmd.Parameters.Add("@pGtChipsOUT", SqlDbType.BigInt).Value = CurrentGtPlaySessionsRow(SQL_COLUMN_CHIPS_OUT)
          _cmd.Parameters.Add("@pGtTotalChipsBuy", SqlDbType.BigInt).Value = CurrentGtPlaySessionsRow(SQL_COLUMN_CHIPS_TOTAL_BUY)

          _result = (_cmd.ExecuteNonQuery() = 1)
        End Using
      End Using

    Catch _ex As Exception
      _result = False
      Log.Exception(_ex)
    End Try

    Return _result
  End Function ' DB_UpdateGtPlaySessionsRegister

  ' PURPOSE: Ask user to discard changes or not
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - True: Discard changes. False: Otherwise
  '
  Private Function DiscardChanges() As Boolean
    Dim _result As Boolean

    ' If pending changes MsgBox
    If Me.m_is_edit_mode AndAlso IsPendingChanges() Then
      If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(101) _
                  , ENUM_MB_TYPE.MB_TYPE_WARNING _
                  , ENUM_MB_BTN.MB_BTN_YES_NO _
                  , ENUM_MB_DEF_BTN.MB_DEF_BTN_2) _
                  = ENUM_MB_RESULT.MB_RESULT_YES Then
        _result = True

        ' To avoid the application to answer about loosing changes for a second time when exiting
        Me.m_query_as_datatable = Nothing

      End If
    End If

    Return _result
  End Function ' DiscardChanges

  ' PURPOSE: Auditor Data
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None 
  '
  Private Sub AuditChanges(ByVal GtPlaySessions As DataTable)
    Dim _curr_auditor As CLASS_AUDITOR_DATA
    Dim _orig_auditor As CLASS_AUDITOR_DATA
    Dim _original_value As String
    Dim _current_value As String
    Dim _started As String
    Dim _account_id As String

    If Not (Me.m_is_edit_mode AndAlso IsPendingChanges()) Then
      Return
    End If

    _curr_auditor = New CLASS_AUDITOR_DATA(AUDIT_CODE_GAMING_TABLES)
    _orig_auditor = New CLASS_AUDITOR_DATA(AUDIT_CODE_GAMING_TABLES)

    ' Set Name and Identifier
    _curr_auditor.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(4989), "")
    _orig_auditor.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(4989), "")

    For _idx_row As Integer = 0 To GtPlaySessions.Rows.Count - 1
      _started = GLB_NLS_GUI_PLAYER_TRACKING.GetString(251) & ":" & GUI_FormatDate(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_INITIAL_DATE), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)   'Start date
      _account_id = GLB_NLS_GUI_INVOICING.GetString(231) & ":" & GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_ACCOUNT_ID)       'Number

      ' Points
      _original_value = IIf(IsDBNull(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_POINTS, DataRowVersion.Original)), _
          AUDIT_NONE_STRING, GUI_FormatNumber(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_POINTS, DataRowVersion.Original)))

      _current_value = IIf(IsDBNull(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_POINTS)), _
          AUDIT_NONE_STRING, GUI_FormatNumber(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_POINTS)))

      If (_original_value <> _current_value) Then
        '_current_value =  & _current_value
        _orig_auditor.SetField(0, _original_value, _started & " - " & _account_id & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(205))
        _curr_auditor.SetField(0, _current_value, _started & " - " & _account_id & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(205))
      End If

      ' Played current
      _original_value = IIf(IsDBNull(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_PLAYED_CURRENT, DataRowVersion.Original)), _
          AUDIT_NONE_STRING, GUI_FormatCurrency(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_PLAYED_CURRENT, DataRowVersion.Original), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False))

      _current_value = IIf(IsDBNull(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_PLAYED_CURRENT)), _
          AUDIT_NONE_STRING, GUI_FormatCurrency(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_PLAYED_CURRENT), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False))

      If (_original_value <> _current_value) Then
        '_current_value =  & _current_value
        _orig_auditor.SetField(0, _original_value, _started & " - " & _account_id & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4486))
        _curr_auditor.SetField(0, _current_value, _started & " - " & _account_id & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4486))
      End If

      ' Played amount
      _original_value = IIf(IsDBNull(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_PLAYED_AMOUNT, DataRowVersion.Original)), _
          AUDIT_NONE_STRING, GUI_FormatCurrency(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_PLAYED_AMOUNT, DataRowVersion.Original), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False))

      _current_value = IIf(IsDBNull(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_PLAYED_AMOUNT)), _
          AUDIT_NONE_STRING, GUI_FormatCurrency(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_PLAYED_AMOUNT), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False))

      If (_original_value <> _current_value) Then
        '_current_value =  & _current_value
        _orig_auditor.SetField(0, _original_value, _started & " - " & _account_id & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5041))
        _curr_auditor.SetField(0, _current_value, _started & " - " & _account_id & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5041))
      End If

      ' Played average
      _original_value = IIf(IsDBNull(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_AVERAGE_PLAYED, DataRowVersion.Original)), _
          AUDIT_NONE_STRING, GUI_FormatCurrency(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_AVERAGE_PLAYED, DataRowVersion.Original), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False))

      _current_value = IIf(IsDBNull(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_AVERAGE_PLAYED)), _
          AUDIT_NONE_STRING, GUI_FormatCurrency(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_AVERAGE_PLAYED), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False))

      If (_original_value <> _current_value) Then
        '_current_value =  & _current_value
        _orig_auditor.SetField(0, _original_value, _started & " - " & _account_id & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4981))
        _curr_auditor.SetField(0, _current_value, _started & " - " & _account_id & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4981))
      End If

      ' Played min
      _original_value = IIf(IsDBNull(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_PLAYED_MIN, DataRowVersion.Original)), _
          AUDIT_NONE_STRING, GUI_FormatCurrency(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_PLAYED_MIN, DataRowVersion.Original), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False))

      _current_value = IIf(IsDBNull(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_PLAYED_MIN)), _
          AUDIT_NONE_STRING, GUI_FormatCurrency(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_PLAYED_MIN), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False))

      If (_original_value <> _current_value) Then
        '_current_value =  & _current_value
        _orig_auditor.SetField(0, _original_value, _started & " - " & _account_id & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(420))
        _curr_auditor.SetField(0, _current_value, _started & " - " & _account_id & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(420))
      End If

      ' Played max
      _original_value = IIf(IsDBNull(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_PLAYED_MAX, DataRowVersion.Original)), _
          AUDIT_NONE_STRING, GUI_FormatCurrency(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_PLAYED_MAX, DataRowVersion.Original), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False))

      _current_value = IIf(IsDBNull(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_PLAYED_MAX)), _
          AUDIT_NONE_STRING, GUI_FormatCurrency(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_PLAYED_MAX), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False))

      If (_original_value <> _current_value) Then
        '_current_value =  & _current_value
        _orig_auditor.SetField(0, _original_value, _started & " - " & _account_id & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(845))
        _curr_auditor.SetField(0, _current_value, _started & " - " & _account_id & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(845))
      End If

      ' Chips IN
      _original_value = IIf(IsDBNull(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_CHIPS_IN, DataRowVersion.Original)), _
          AUDIT_NONE_STRING, GUI_FormatCurrency(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_CHIPS_IN, DataRowVersion.Original), CurrencySymbol:=False))

      _current_value = IIf(IsDBNull(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_CHIPS_IN)), _
          AUDIT_NONE_STRING, GUI_FormatCurrency(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_CHIPS_IN), CurrencySymbol:=False))

      If (_original_value <> _current_value) Then
        '_current_value =  & _current_value
        _orig_auditor.SetField(0, _original_value, _started & " - " & _account_id & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5287))
        _curr_auditor.SetField(0, _current_value, _started & " - " & _account_id & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5287))
      End If

      ' Chips OUT
      _original_value = IIf(IsDBNull(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_CHIPS_OUT, DataRowVersion.Original)), _
          AUDIT_NONE_STRING, GUI_FormatCurrency(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_CHIPS_OUT, DataRowVersion.Original), CurrencySymbol:=False))

      _current_value = IIf(IsDBNull(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_CHIPS_OUT)), _
          AUDIT_NONE_STRING, GUI_FormatCurrency(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_CHIPS_OUT), CurrencySymbol:=False))

      If (_original_value <> _current_value) Then
        '_current_value =  & _current_value
        _orig_auditor.SetField(0, _original_value, _started & " - " & _account_id & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5288))
        _curr_auditor.SetField(0, _current_value, _started & " - " & _account_id & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5288))
      End If

      ' Total Buy Chips
      _original_value = IIf(IsDBNull(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_CHIPS_TOTAL_BUY, DataRowVersion.Original)), _
          AUDIT_NONE_STRING, GUI_FormatCurrency(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_CHIPS_TOTAL_BUY, DataRowVersion.Original), CurrencySymbol:=False))

      _current_value = IIf(IsDBNull(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_CHIPS_TOTAL_BUY)), _
          AUDIT_NONE_STRING, GUI_FormatCurrency(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_CHIPS_TOTAL_BUY), CurrencySymbol:=False))

      If (_original_value <> _current_value) Then
        '_current_value =  & _current_value
        _orig_auditor.SetField(0, _original_value, _started & " - " & _account_id & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5289))
        _curr_auditor.SetField(0, _current_value, _started & " - " & _account_id & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5289))
      End If

      ' Netwin
      _original_value = IIf(IsDBNull(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_WON_AMOUNT, DataRowVersion.Original)), _
          AUDIT_NONE_STRING, GUI_FormatCurrency(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_WON_AMOUNT, DataRowVersion.Original), CurrencySymbol:=False))

      _current_value = IIf(IsDBNull(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_WON_AMOUNT)), _
          AUDIT_NONE_STRING, GUI_FormatCurrency(GtPlaySessions.Rows(_idx_row)(SQL_COLUMN_WON_AMOUNT), CurrencySymbol:=False))

      If (_original_value <> _current_value) Then
        '_current_value =  & _current_value
        _orig_auditor.SetField(0, _original_value, _started & " - " & _account_id & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(539))
        _curr_auditor.SetField(0, _current_value, _started & " - " & _account_id & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(539))
      End If

    Next ' For _idx_row

    ' Notify
    _curr_auditor.Notify(CurrentUser.GuiId, _
                         CurrentUser.Id, _
                         CurrentUser.Name, _
                         CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.UPDATE, _
                         0, _
                         _orig_auditor)

  End Sub ' AuditChanges

  Private Sub ShowSessionMovements()
    Dim _frm As frm_table_player_movements
    Dim _session_id As Int64

    If Not Me.Grid.SelectedRows() Is Nothing Then

      If Me.Grid.SelectedRows().Length = 1 Then

        If Not String.IsNullOrEmpty(Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_GAME_TABLE_PLAY_SESSION_ID).Value) Then

          _session_id = Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_GAME_TABLE_PLAY_SESSION_ID).Value

          If _session_id <> 0 Then

            Windows.Forms.Cursor.Current = Cursors.WaitCursor

            ' Create instance
            _frm = New frm_table_player_movements()

            If m_date_to = "" Then
              _frm.ShowSessionID(Me.MdiParent, _session_id, m_date_from)
            Else
              _frm.ShowSessionID(Me.MdiParent, _session_id, m_date_from, m_date_to)
            End If

            Windows.Forms.Cursor.Current = Cursors.Default

          End If

        End If

      End If

    End If

  End Sub

#End Region

#Region "Public"

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region

#Region "Events"

  Private Sub opt_several_users_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_one_user.Click
    Me.cmb_user.Enabled = True
    Me.chk_show_all.Enabled = True
  End Sub ' opt_several_users_Click

  Private Sub opt_all_users_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_all_users.Click
    Me.cmb_user.Enabled = False
    Me.chk_show_all.Enabled = False
  End Sub ' opt_all_users_Click

  Private Sub chk_show_all_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_show_all.CheckedChanged
    If chk_show_all.Checked Then
      SetCombo(Me.cmb_user, "SELECT GU_USER_ID, GU_USERNAME FROM GUI_USERS ORDER BY GU_USERNAME")
    Else
      SetCombo(Me.cmb_user, "SELECT GU_USER_ID, GU_USERNAME FROM GUI_USERS WHERE GU_BLOCK_REASON = " & WSI.Common.GUI_USER_BLOCK_REASON.NONE & " ORDER BY GU_USERNAME")
    End If
  End Sub 'chk_show_all

  Private Sub rb_date_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Dim _rb As RadioButton = sender

    If Not Me.m_is_edit_mode Then
      RemoveHandler rb_open_sessions.Click, AddressOf rb_date_CheckedChanged
    End If
    RemoveHandler rb_historic.Click, AddressOf rb_date_CheckedChanged
    RemoveHandler rb_ini_date.Click, AddressOf rb_date_CheckedChanged
    RemoveHandler rb_finish_date.Click, AddressOf rb_date_CheckedChanged

    Select Case _rb.Text
      Case GLB_NLS_GUI_STATISTICS.GetString(345) ' 345 - Open Sessions
        Me.rb_open_sessions.Checked = True
        Me.rb_finish_date.Checked = False
        Me.rb_historic.Checked = False
        Me.rb_ini_date.Checked = False
        Me.game_status.Enabled = False
        Me.game_status.SetDefaultValues()
        Me.dtp_from.Enabled = False
        Me.dtp_to.Enabled = False
      Case GLB_NLS_GUI_CONTROLS.GetString(331) ' 331 - History
        Me.rb_historic.Checked = True
        Me.rb_open_sessions.Checked = False
        Me.rb_ini_date.Checked = True
        Me.rb_finish_date.Checked = False
        Me.game_status.Enabled = True
        Me.dtp_to.Enabled = True
        Me.dtp_from.Enabled = True
      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(5285) ' 5285 - Arrival
        Me.rb_ini_date.Checked = True
        Me.rb_finish_date.Checked = False
        Me.rb_historic.Checked = True
        Me.rb_open_sessions.Checked = False
        Me.game_status.Enabled = True
        Me.dtp_to.Enabled = True
        Me.dtp_from.Enabled = True
      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(5286) ' 5286 - Departure
        Me.rb_finish_date.Checked = True
        Me.rb_historic.Checked = True
        Me.rb_open_sessions.Checked = False
        Me.rb_ini_date.Checked = False
        Me.game_status.Enabled = True
        Me.dtp_from.Enabled = True
        Me.dtp_to.Enabled = True
    End Select

    If Not Me.m_is_edit_mode Then
      AddHandler rb_open_sessions.Click, AddressOf rb_date_CheckedChanged
    End If
    AddHandler rb_historic.Click, AddressOf rb_date_CheckedChanged
    AddHandler rb_ini_date.Click, AddressOf rb_date_CheckedChanged
    AddHandler rb_finish_date.Click, AddressOf rb_date_CheckedChanged

  End Sub ' rb_date_CheckedChanged

#End Region ' Events

End Class