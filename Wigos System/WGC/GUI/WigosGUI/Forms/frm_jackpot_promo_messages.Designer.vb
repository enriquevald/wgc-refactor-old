<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_jackpot_promo_messages
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_promo_message = New System.Windows.Forms.GroupBox
    Me.lbl_message_3 = New System.Windows.Forms.Label
    Me.txt_message_3 = New System.Windows.Forms.TextBox
    Me.lbl_message_2 = New System.Windows.Forms.Label
    Me.txt_message_2 = New System.Windows.Forms.TextBox
    Me.lbl_message_1 = New System.Windows.Forms.Label
    Me.txt_message_1 = New System.Windows.Forms.TextBox
    Me.panel_data.SuspendLayout()
    Me.gb_promo_message.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_promo_message)
    Me.panel_data.Size = New System.Drawing.Size(877, 112)
    '
    'gb_promo_message
    '
    Me.gb_promo_message.Controls.Add(Me.lbl_message_3)
    Me.gb_promo_message.Controls.Add(Me.txt_message_3)
    Me.gb_promo_message.Controls.Add(Me.lbl_message_2)
    Me.gb_promo_message.Controls.Add(Me.txt_message_2)
    Me.gb_promo_message.Controls.Add(Me.lbl_message_1)
    Me.gb_promo_message.Controls.Add(Me.txt_message_1)
    Me.gb_promo_message.Location = New System.Drawing.Point(19, 3)
    Me.gb_promo_message.Name = "gb_promo_message"
    Me.gb_promo_message.Size = New System.Drawing.Size(855, 102)
    Me.gb_promo_message.TabIndex = 12
    Me.gb_promo_message.TabStop = False
    Me.gb_promo_message.Text = "xPromoMessage"
    '
    'lbl_message_3
    '
    Me.lbl_message_3.AutoSize = True
    Me.lbl_message_3.Location = New System.Drawing.Point(6, 73)
    Me.lbl_message_3.Name = "lbl_message_3"
    Me.lbl_message_3.Size = New System.Drawing.Size(63, 13)
    Me.lbl_message_3.TabIndex = 5
    Me.lbl_message_3.Text = "xMEssage"
    '
    'txt_message_3
    '
    Me.txt_message_3.Location = New System.Drawing.Point(75, 70)
    Me.txt_message_3.MaxLength = 255
    Me.txt_message_3.Name = "txt_message_3"
    Me.txt_message_3.Size = New System.Drawing.Size(774, 21)
    Me.txt_message_3.TabIndex = 4
    '
    'lbl_message_2
    '
    Me.lbl_message_2.AutoSize = True
    Me.lbl_message_2.Location = New System.Drawing.Point(6, 46)
    Me.lbl_message_2.Name = "lbl_message_2"
    Me.lbl_message_2.Size = New System.Drawing.Size(63, 13)
    Me.lbl_message_2.TabIndex = 3
    Me.lbl_message_2.Text = "xMEssage"
    '
    'txt_message_2
    '
    Me.txt_message_2.Location = New System.Drawing.Point(75, 43)
    Me.txt_message_2.MaxLength = 255
    Me.txt_message_2.Name = "txt_message_2"
    Me.txt_message_2.Size = New System.Drawing.Size(774, 21)
    Me.txt_message_2.TabIndex = 2
    '
    'lbl_message_1
    '
    Me.lbl_message_1.AutoSize = True
    Me.lbl_message_1.Location = New System.Drawing.Point(6, 21)
    Me.lbl_message_1.Name = "lbl_message_1"
    Me.lbl_message_1.Size = New System.Drawing.Size(63, 13)
    Me.lbl_message_1.TabIndex = 1
    Me.lbl_message_1.Text = "xMEssage"
    '
    'txt_message_1
    '
    Me.txt_message_1.Location = New System.Drawing.Point(75, 18)
    Me.txt_message_1.MaxLength = 255
    Me.txt_message_1.Name = "txt_message_1"
    Me.txt_message_1.Size = New System.Drawing.Size(774, 21)
    Me.txt_message_1.TabIndex = 0
    '
    'frm_jackpot_promo_messages
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(979, 121)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_jackpot_promo_messages"
    Me.Text = "frm_jackpot_promo_message"
    Me.panel_data.ResumeLayout(False)
    Me.gb_promo_message.ResumeLayout(False)
    Me.gb_promo_message.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_promo_message As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_message_1 As System.Windows.Forms.Label
  Friend WithEvents txt_message_1 As System.Windows.Forms.TextBox
  Friend WithEvents lbl_message_3 As System.Windows.Forms.Label
  Friend WithEvents txt_message_3 As System.Windows.Forms.TextBox
  Friend WithEvents lbl_message_2 As System.Windows.Forms.Label
  Friend WithEvents txt_message_2 As System.Windows.Forms.TextBox
End Class
