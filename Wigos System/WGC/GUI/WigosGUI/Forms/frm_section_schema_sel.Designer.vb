<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_section_schema_sel
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_section_schema_sel))
    Me.gb_group_status = New System.Windows.Forms.GroupBox()
    Me.chk_deactive = New System.Windows.Forms.CheckBox()
    Me.chk_active = New System.Windows.Forms.CheckBox()
    Me.gb_sections = New System.Windows.Forms.GroupBox()
    Me.txt_title = New GUI_Controls.uc_entry_field()
    Me.uc_section_parent = New GUI_Controls.uc_combo()
    Me.lblParent = New System.Windows.Forms.Label()
    Me.lblTitle = New System.Windows.Forms.Label()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_group_status.SuspendLayout()
    Me.gb_sections.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_sections)
    Me.panel_filter.Controls.Add(Me.gb_group_status)
    resources.ApplyResources(Me.panel_filter, "panel_filter")
    Me.panel_filter.Controls.SetChildIndex(Me.gb_group_status, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_sections, 0)
    '
    'panel_data
    '
    resources.ApplyResources(Me.panel_data, "panel_data")
    '
    'pn_separator_line
    '
    resources.ApplyResources(Me.pn_separator_line, "pn_separator_line")
    '
    'pn_line
    '
    resources.ApplyResources(Me.pn_line, "pn_line")
    '
    'gb_group_status
    '
    Me.gb_group_status.Controls.Add(Me.chk_deactive)
    Me.gb_group_status.Controls.Add(Me.chk_active)
    resources.ApplyResources(Me.gb_group_status, "gb_group_status")
    Me.gb_group_status.Name = "gb_group_status"
    Me.gb_group_status.TabStop = False
    '
    'chk_deactive
    '
    resources.ApplyResources(Me.chk_deactive, "chk_deactive")
    Me.chk_deactive.Name = "chk_deactive"
    '
    'chk_active
    '
    resources.ApplyResources(Me.chk_active, "chk_active")
    Me.chk_active.Name = "chk_active"
    '
    'gb_sections
    '
    Me.gb_sections.Controls.Add(Me.lblTitle)
    Me.gb_sections.Controls.Add(Me.lblParent)
    Me.gb_sections.Controls.Add(Me.txt_title)
    Me.gb_sections.Controls.Add(Me.uc_section_parent)
    resources.ApplyResources(Me.gb_sections, "gb_sections")
    Me.gb_sections.Name = "gb_sections"
    Me.gb_sections.TabStop = False
    '
    'txt_title
    '
    Me.txt_title.DoubleValue = 0.0R
    Me.txt_title.IntegerValue = 0
    Me.txt_title.IsReadOnly = False
    resources.ApplyResources(Me.txt_title, "txt_title")
    Me.txt_title.Name = "txt_title"
    Me.txt_title.PlaceHolder = Nothing
    Me.txt_title.SufixText = "Sufix Text"
    Me.txt_title.SufixTextVisible = True
    Me.txt_title.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.txt_title.TextValue = ""
    Me.txt_title.TextWidth = 0
    Me.txt_title.Value = ""
    Me.txt_title.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_section_parent
    '
    Me.uc_section_parent.AllowUnlistedValues = False
    Me.uc_section_parent.AutoCompleteMode = False
    Me.uc_section_parent.IsReadOnly = False
    resources.ApplyResources(Me.uc_section_parent, "uc_section_parent")
    Me.uc_section_parent.Name = "uc_section_parent"
    Me.uc_section_parent.SelectedIndex = -1
    Me.uc_section_parent.SufixText = "Sufix Text"
    Me.uc_section_parent.SufixTextVisible = True
    Me.uc_section_parent.TextCombo = Nothing
    Me.uc_section_parent.TextWidth = 0
    '
    'lblParent
    '
    resources.ApplyResources(Me.lblParent, "lblParent")
    Me.lblParent.Name = "lblParent"
    '
    'lblTitle
    '
    resources.ApplyResources(Me.lblTitle, "lblTitle")
    Me.lblTitle.Name = "lblTitle"
    '
    'frm_section_schema_sel
    '
    resources.ApplyResources(Me, "$this")
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_section_schema_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_group_status.ResumeLayout(False)
    Me.gb_sections.ResumeLayout(False)
    Me.gb_sections.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_group_status As System.Windows.Forms.GroupBox
  Friend WithEvents gb_sections As System.Windows.Forms.GroupBox
  Friend WithEvents txt_title As GUI_Controls.uc_entry_field
  Friend WithEvents uc_section_parent As GUI_Controls.uc_combo
  Friend WithEvents chk_deactive As System.Windows.Forms.CheckBox
  Friend WithEvents chk_active As System.Windows.Forms.CheckBox
  Friend WithEvents lblTitle As System.Windows.Forms.Label
  Friend WithEvents lblParent As System.Windows.Forms.Label
End Class
