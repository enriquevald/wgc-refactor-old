'-------------------------------------------------------------------
' Copyright � 2016 Win Systems Ltd.
'-------------------------------------------------------------------
'
'   MODULE NAME : frm_ticket_audits
'   DESCRIPTION : Listado de registros de auditor�a de tickets
'        AUTHOR : Jorge Concheyro
' CREATION DATE : 11-OCT-2016
'
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 11-OCT-2016  JRC    Initial version. PBI 18421:Pantalla de Log-Seguimiento de Tickets - Creaci�n nueva pantalla GUI
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonMisc
Imports System.Data.SqlClient
Imports WSI.Common
Imports GUI_Controls.frm_base_print
Imports GUI_Controls.frm_base_sel
Imports WSI.Common.TITO


Public Class frm_ticket_audits
  Inherits frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents gb_ticket_type As System.Windows.Forms.GroupBox
  Friend WithEvents chk_offline As System.Windows.Forms.CheckBox
  Friend WithEvents ef_ticket_number As GUI_Controls.uc_entry_field
  Friend WithEvents chk_handpay As System.Windows.Forms.CheckBox
  Friend WithEvents chk_jackpot As System.Windows.Forms.CheckBox
  Friend WithEvents chk_promo_nonredeem_ticket As System.Windows.Forms.CheckBox
  Friend WithEvents chk_promo_redeem_ticket As System.Windows.Forms.CheckBox
  Friend WithEvents chk_redeemable_ticket As System.Windows.Forms.CheckBox
  Friend WithEvents lb_session_id As System.Windows.Forms.Label
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.lb_session_id = New System.Windows.Forms.Label()
    Me.gb_ticket_type = New System.Windows.Forms.GroupBox()
    Me.chk_offline = New System.Windows.Forms.CheckBox()
    Me.ef_ticket_number = New GUI_Controls.uc_entry_field()
    Me.chk_handpay = New System.Windows.Forms.CheckBox()
    Me.chk_jackpot = New System.Windows.Forms.CheckBox()
    Me.chk_promo_nonredeem_ticket = New System.Windows.Forms.CheckBox()
    Me.chk_promo_redeem_ticket = New System.Windows.Forms.CheckBox()
    Me.chk_redeemable_ticket = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_ticket_type.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_ticket_type)
    Me.panel_filter.Controls.Add(Me.lb_session_id)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Size = New System.Drawing.Size(1116, 213)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lb_session_id, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_ticket_type, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 217)
    Me.panel_data.Size = New System.Drawing.Size(1116, 306)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1110, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1110, 4)
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(680, 6)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(237, 72)
    Me.gb_date.TabIndex = 1
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    Me.gb_date.Visible = False
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 38)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(222, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 14)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(222, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'lb_session_id
    '
    Me.lb_session_id.AutoSize = True
    Me.lb_session_id.Location = New System.Drawing.Point(34, 93)
    Me.lb_session_id.Name = "lb_session_id"
    Me.lb_session_id.Size = New System.Drawing.Size(0, 13)
    Me.lb_session_id.TabIndex = 6
    '
    'gb_ticket_type
    '
    Me.gb_ticket_type.Controls.Add(Me.chk_offline)
    Me.gb_ticket_type.Controls.Add(Me.ef_ticket_number)
    Me.gb_ticket_type.Controls.Add(Me.chk_handpay)
    Me.gb_ticket_type.Controls.Add(Me.chk_jackpot)
    Me.gb_ticket_type.Controls.Add(Me.chk_promo_nonredeem_ticket)
    Me.gb_ticket_type.Controls.Add(Me.chk_promo_redeem_ticket)
    Me.gb_ticket_type.Controls.Add(Me.chk_redeemable_ticket)
    Me.gb_ticket_type.Location = New System.Drawing.Point(8, 6)
    Me.gb_ticket_type.Name = "gb_ticket_type"
    Me.gb_ticket_type.Size = New System.Drawing.Size(238, 195)
    Me.gb_ticket_type.TabIndex = 11
    Me.gb_ticket_type.TabStop = False
    Me.gb_ticket_type.Text = "xTicket Type"
    '
    'chk_offline
    '
    Me.chk_offline.AutoSize = True
    Me.chk_offline.Location = New System.Drawing.Point(9, 167)
    Me.chk_offline.Name = "chk_offline"
    Me.chk_offline.Size = New System.Drawing.Size(70, 17)
    Me.chk_offline.TabIndex = 6
    Me.chk_offline.Text = "xOffline"
    Me.chk_offline.UseVisualStyleBackColor = True
    '
    'ef_ticket_number
    '
    Me.ef_ticket_number.DoubleValue = 0.0R
    Me.ef_ticket_number.IntegerValue = 0
    Me.ef_ticket_number.IsReadOnly = False
    Me.ef_ticket_number.Location = New System.Drawing.Point(9, 20)
    Me.ef_ticket_number.Name = "ef_ticket_number"
    Me.ef_ticket_number.PlaceHolder = Nothing
    Me.ef_ticket_number.Size = New System.Drawing.Size(192, 24)
    Me.ef_ticket_number.SufixText = "Sufix Text"
    Me.ef_ticket_number.SufixTextVisible = True
    Me.ef_ticket_number.TabIndex = 0
    Me.ef_ticket_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_ticket_number.TextValue = ""
    Me.ef_ticket_number.TextWidth = 50
    Me.ef_ticket_number.Value = ""
    Me.ef_ticket_number.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_handpay
    '
    Me.chk_handpay.AutoSize = True
    Me.chk_handpay.Location = New System.Drawing.Point(9, 144)
    Me.chk_handpay.Name = "chk_handpay"
    Me.chk_handpay.Size = New System.Drawing.Size(83, 17)
    Me.chk_handpay.TabIndex = 5
    Me.chk_handpay.Text = "xHandPay"
    Me.chk_handpay.UseVisualStyleBackColor = True
    '
    'chk_jackpot
    '
    Me.chk_jackpot.AutoSize = True
    Me.chk_jackpot.Location = New System.Drawing.Point(9, 121)
    Me.chk_jackpot.Name = "chk_jackpot"
    Me.chk_jackpot.Size = New System.Drawing.Size(76, 17)
    Me.chk_jackpot.TabIndex = 4
    Me.chk_jackpot.Text = "xJackpot"
    Me.chk_jackpot.UseVisualStyleBackColor = True
    '
    'chk_promo_nonredeem_ticket
    '
    Me.chk_promo_nonredeem_ticket.AutoSize = True
    Me.chk_promo_nonredeem_ticket.Location = New System.Drawing.Point(9, 98)
    Me.chk_promo_nonredeem_ticket.Name = "chk_promo_nonredeem_ticket"
    Me.chk_promo_nonredeem_ticket.Size = New System.Drawing.Size(107, 17)
    Me.chk_promo_nonredeem_ticket.TabIndex = 3
    Me.chk_promo_nonredeem_ticket.Text = "xPlayableOnly"
    Me.chk_promo_nonredeem_ticket.UseVisualStyleBackColor = True
    '
    'chk_promo_redeem_ticket
    '
    Me.chk_promo_redeem_ticket.AutoSize = True
    Me.chk_promo_redeem_ticket.Location = New System.Drawing.Point(9, 75)
    Me.chk_promo_redeem_ticket.Name = "chk_promo_redeem_ticket"
    Me.chk_promo_redeem_ticket.Size = New System.Drawing.Size(196, 17)
    Me.chk_promo_redeem_ticket.TabIndex = 2
    Me.chk_promo_redeem_ticket.Text = "Promotional Non-Redeemable"
    Me.chk_promo_redeem_ticket.UseVisualStyleBackColor = True
    '
    'chk_redeemable_ticket
    '
    Me.chk_redeemable_ticket.AutoSize = True
    Me.chk_redeemable_ticket.Location = New System.Drawing.Point(9, 52)
    Me.chk_redeemable_ticket.Name = "chk_redeemable_ticket"
    Me.chk_redeemable_ticket.Size = New System.Drawing.Size(104, 17)
    Me.chk_redeemable_ticket.TabIndex = 1
    Me.chk_redeemable_ticket.Text = "xRedeemable"
    Me.chk_redeemable_ticket.UseVisualStyleBackColor = True
    '
    'frm_ticket_audits
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(1124, 527)
    Me.Name = "frm_ticket_audits"
    Me.Text = "frm_ticket_audits"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gb_ticket_type.ResumeLayout(False)
    Me.gb_ticket_type.PerformLayout()
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Constants "

  Private Const GRID_COLUMNS As Integer = 6
  Private Const GRID_HEADER_ROWS As Integer = 2

  'DB Columns

  Private Const SQL_COLUMN_AUDIT_DATE As Integer = 0
  Private Const SQL_COLUMN_AMOUNT As Integer = 1
  Private Const SQL_COLUMN_TITO_TICKET_TYPE As Integer = 2
  Private Const SQL_COLUMN_TITO_STATE As Integer = 3

  Private Const SQL_COLUMN_TERMINAL_CREATION As Integer = 4
  Private Const SQL_COLUMN_ACCOUNT_CREATION As Integer = 5

  Private Const SQL_COLUMN_COLLECTED_STATUS = 6
  Private Const SQL_COLUMN_EXPIRATION_DATE_TIME = 7

  Private Const SQL_COLUMN_CAGE_SESSION_NAME = 8




  'Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_AUDIT_DATE As Integer = 1
  Private Const GRID_COLUMN_AMOUNT As Integer = 2
  Private Const GRID_COLUMN_TITO_TYCKET_TYPE As Integer = 3
  Private Const GRID_COLUMN_STATE As Integer = 4
  Private Const GRID_COLUMN_DESCRIPTION = 5


  ' Width
  Private Const GRID_WIDTH_INDEX As Integer = 200
  Private Const GRID_WIDTH_AUDIT_DATE As Integer = 2280
  Private Const GRID_WIDTH_AMOUNT As Integer = 1100
  Private Const GRID_WIDTH_TITO_TYCKET_TYPE As Integer = 1605
  Private Const GRID_WIDTH_STATE As Integer = 2865
  Private Const GRID_WIDTH_DESCRIPTION = 6000

#End Region ' Constants



#Region " Members "

  Private m_print_datetime As Date
  Private m_ticket_number As String
  Private m_ticket_type As String


  ' For report filters 
  Private m_date_from As String
  Private m_date_to As String

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_TICKET_AUDIT_SEL

    'Call Base Form proc
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialites controls settings
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None  
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7674)
    ef_ticket_number.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(795)
    ef_ticket_number.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, 18)


    ' Buttons
    Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_INFO).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_FILTER_RESET).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Visible = True

    GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)

    ' Date
    Me.gb_date.Text = GLB_NLS_GUI_ALARMS.GetString(441)
    Me.dtp_from.Text = GLB_NLS_GUI_AUDITOR.GetString(257)
    Me.dtp_to.Text = GLB_NLS_GUI_AUDITOR.GetString(258)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.gb_ticket_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2134)

    Me.ef_ticket_number.TextValue = m_ticket_number

    chk_redeemable_ticket.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2125)
    chk_promo_redeem_ticket.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2701, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2125))
    chk_promo_nonredeem_ticket.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2701, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2126))
    chk_handpay.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2685)
    chk_jackpot.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2686)
    chk_offline.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3327)



    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

    ' 5 minutes timeout
    Call GUI_SetQueryTimeout(300)

    Call GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)

  End Sub ' GUI_InitControls

  ' PURPOSE: Resets filters values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None  
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
    ef_ticket_number.Text = ""
  End Sub ' GUI_FilterReset


  ' PURPOSE: Checks filter's values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None  
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If


    If String.IsNullOrEmpty(ef_ticket_number.TextValue.Trim()) Then
      Call ef_ticket_number.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(114), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
                      mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, ef_ticket_number.Text)
      Return False
    End If

    If Not String.IsNullOrEmpty(ef_ticket_number.TextValue.Trim()) AndAlso Not ef_ticket_number.ValidateFormat() Then
      Call ef_ticket_number.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
                      mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, ef_ticket_number.Text)

      Return False
    End If



    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _sb As System.Text.StringBuilder

    _sb = New System.Text.StringBuilder()

    _sb.AppendLine("    	SELECT                                                                                                                                                ")
    _sb.AppendLine("		            TIA_INSERT_DATE                                                                                                                             ")
    _sb.AppendLine("  	          , TIA_AMOUNT                                                                                                                                  ")
    _sb.AppendLine("	  	        , TIA_TYPE_ID                                                                                                                                 ")
    _sb.AppendLine("              , TIA_STATUS                                                                                                                                  ")
    _sb.AppendLine("	            , CASE WHEN TIA_CREATED_TERMINAL_TYPE = 0                                                                                                     ")
    _sb.AppendLine("                  THEN CASHIER_TERMINAL_CREATION.CT_NAME                                                                                                    ")
    _sb.AppendLine("                  ELSE TERMINAL_CREATION.TE_NAME                                                                                                            ")
    _sb.AppendLine("                  END AS TE_TERMINAL_NAME                                                                                                                   ")
    _sb.AppendLine("  	          , ACCOUNTS.AC_HOLDER_NAME                                                                                                                     ")
    _sb.AppendLine("  	          , TIA_COLLECTED                                                                                                                               ")
    _sb.AppendLine("              , TIA_EXPIRATION_DATETIME                                                                                                                     ")
    _sb.AppendLine("              , CAGE_SESSIONS.CGS_SESSION_NAME                                                                                                              ")
    _sb.AppendLine("       FROM TICKETS_AUDIT_STATUS_CHANGE                                                                                                                     ")
    _sb.AppendLine("  LEFT JOIN   TERMINALS TERMINAL_CREATION ON TIA_CREATED_TERMINAL_ID = TERMINAL_CREATION.TE_TERMINAL_ID                                                     ")
    _sb.AppendLine("  LEFT JOIN   CASHIER_TERMINALS CASHIER_TERMINAL_CREATION ON  TICKETS_AUDIT_STATUS_CHANGE.TIA_CREATED_TERMINAL_ID = CASHIER_TERMINAL_CREATION.CT_CASHIER_ID ")
    _sb.AppendLine("	LEFT JOIN   ACCOUNTS ON ACCOUNTS.AC_ACCOUNT_ID = TICKETS_AUDIT_STATUS_CHANGE.TIA_CREATED_ACCOUNT_ID                                                       ")

    _sb.AppendLine("	LEFT JOIN   CAGE_MOVEMENTS ON TICKETS_AUDIT_STATUS_CHANGE.TIA_CAGE_MOVEMENT_ID = CAGE_MOVEMENTS.CGM_MOVEMENT_ID                                           ")
    _sb.AppendLine("	LEFT JOIN   CAGE_SESSIONS ON CAGE_SESSIONS.CGS_CAGE_SESSION_ID = CAGE_MOVEMENTS.CGM_CAGE_SESSION_ID                                                       ")


    _sb.AppendLine(GetSqlWhere())
    _sb.AppendLine("    ORDER BY   TIA_VALIDATION_NUMBER, TIA_ID                                                                                                                ")



    Return _sb.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _terminal As String
    Dim _account As String
    _terminal = ""
    _account = ""


    With Me.Grid

      'audit date 
      .Cell(RowIndex, GRID_COLUMN_AUDIT_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_AUDIT_DATE), _
                                                                            ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                            ENUM_FORMAT_TIME.FORMAT_HHMMSS)


      'ticket amount
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_AMOUNT)) Then
        .Cell(RowIndex, GRID_COLUMN_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_AMOUNT))
      End If


      'TITO ticket type
      Select Case DbRow.Value(SQL_COLUMN_TITO_TICKET_TYPE)
        Case TITO_TICKET_TYPE.CASHABLE
          Me.Grid.Cell(RowIndex, GRID_COLUMN_TITO_TYCKET_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2125)

        Case TITO_TICKET_TYPE.PROMO_REDEEM
          Me.Grid.Cell(RowIndex, GRID_COLUMN_TITO_TYCKET_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2701, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3320))

        Case TITO_TICKET_TYPE.PROMO_NONREDEEM
          Me.Grid.Cell(RowIndex, GRID_COLUMN_TITO_TYCKET_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2701, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3321))

        Case TITO_TICKET_TYPE.HANDPAY
          Me.Grid.Cell(RowIndex, GRID_COLUMN_TITO_TYCKET_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2685)

        Case TITO_TICKET_TYPE.JACKPOT
          Me.Grid.Cell(RowIndex, GRID_COLUMN_TITO_TYCKET_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2686)

        Case TITO_TICKET_TYPE.OFFLINE
          Me.Grid.Cell(RowIndex, GRID_COLUMN_TITO_TYCKET_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3327)

      End Select


      'ticket status
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_TITO_STATE)) Then
        .Cell(RowIndex, GRID_COLUMN_STATE).Value = DbRow.Value(SQL_COLUMN_TITO_STATE)
      End If


      Select Case (DbRow.Value(SQL_COLUMN_TITO_STATE))
        Case WSI.Common.TITO_TICKET_STATUS.VALID
          Me.Grid.Cell(RowIndex, GRID_COLUMN_STATE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3010)
        Case WSI.Common.TITO_TICKET_STATUS.CANCELED
          Me.Grid.Cell(RowIndex, GRID_COLUMN_STATE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7671)
        Case WSI.Common.TITO_TICKET_STATUS.REDEEMED
          Me.Grid.Cell(RowIndex, GRID_COLUMN_STATE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2138)
        Case WSI.Common.TITO_TICKET_STATUS.EXPIRED
          Me.Grid.Cell(RowIndex, GRID_COLUMN_STATE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2137)
        Case WSI.Common.TITO_TICKET_STATUS.DISCARDED
          Me.Grid.Cell(RowIndex, GRID_COLUMN_STATE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2989)
        Case WSI.Common.TITO_TICKET_STATUS.PENDING_CANCEL
          Me.Grid.Cell(RowIndex, GRID_COLUMN_STATE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7672)
        Case WSI.Common.TITO_TICKET_STATUS.PENDING_PRINT
          Me.Grid.Cell(RowIndex, GRID_COLUMN_STATE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7648)
        Case WSI.Common.TITO_TICKET_STATUS.PAID_AFTER_EXPIRATION
          Me.Grid.Cell(RowIndex, GRID_COLUMN_STATE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2138) & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2137)
        Case WSI.Common.TITO_TICKET_STATUS.SWAP_FOR_CHIPS
          Me.Grid.Cell(RowIndex, GRID_COLUMN_STATE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7145) 'EOR 08-MAR-2016 Type State
        Case WSI.Common.TITO_TICKET_STATUS.PENDING_PAYMENT
          Me.Grid.Cell(RowIndex, GRID_COLUMN_STATE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7278)
        Case WSI.Common.TITO_TICKET_STATUS.PENDING_PAYMENT_AFTER_EXPIRATION
          Me.Grid.Cell(RowIndex, GRID_COLUMN_STATE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7279)
      End Select

      'description    

      _terminal = GLB_NLS_GUI_PLAYER_TRACKING.GetString(592) & ": " & DbRow.Value(SQL_COLUMN_TERMINAL_CREATION)

      If Not IsDBNull(DbRow.Value(SQL_COLUMN_ACCOUNT_CREATION)) Then
        _account = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1202) & ": " & DbRow.Value(SQL_COLUMN_ACCOUNT_CREATION)
      Else
        _account = GLB_NLS_GUI_PLAYER_TRACKING.GetString(405)
      End If


      Select Case (DbRow.Value(SQL_COLUMN_TITO_STATE))
        Case WSI.Common.TITO_TICKET_STATUS.PENDING_PRINT
          Me.Grid.Cell(RowIndex, GRID_COLUMN_DESCRIPTION).Value = _terminal & ", " & _account
        Case WSI.Common.TITO_TICKET_STATUS.VALID
          Me.Grid.Cell(RowIndex, GRID_COLUMN_DESCRIPTION).Value = _terminal & ", " & _account
        Case WSI.Common.TITO_TICKET_STATUS.EXPIRED
          Me.Grid.Cell(RowIndex, GRID_COLUMN_DESCRIPTION).Value = _terminal & ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(437) & ": " & GUI_FormatDate(DbRow.Value(SQL_COLUMN_EXPIRATION_DATE_TIME), _
                                                                            ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                            ENUM_FORMAT_TIME.FORMAT_HHMMSS)
        Case Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_DESCRIPTION).Value = _terminal
      End Select


      If Not IsDBNull(DbRow.Value(SQL_COLUMN_CAGE_SESSION_NAME)) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DESCRIPTION).Value = Me.Grid.Cell(RowIndex, GRID_COLUMN_DESCRIPTION).Value & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(3388) & ": " & DbRow.Value(SQL_COLUMN_CAGE_SESSION_NAME)
      End If



    End With

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Sets initial focus
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None  

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.dtp_from
  End Sub 'GUI_SetInitialFocus


#Region " GUI Reports "

  ' PURPOSE: Sets report filters values
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData 
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None  
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    'PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(261) & " " & GLB_NLS_GUI_AUDITOR.GetString(257), m_date_from)
    'PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(261) & " " & GLB_NLS_GUI_AUDITOR.GetString(258), m_date_to)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7664), m_ticket_number)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2134), m_ticket_type)

    PrintData.FilterValueWidth(1) = 3000
    PrintData.FilterValueWidth(2) = 3000

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Sets report filters values
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData 
  '           - FirstColIndex  
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None  
  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)

    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  ' PURPOSE: Sets report filters values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None  
  Protected Overrides Sub GUI_ReportUpdateFilters()

    Dim _no_one As Boolean
    Dim _all As Boolean

    m_date_from = ""
    m_date_to = ""
    m_ticket_type = ""

    ' Date 
    If Me.dtp_from.Checked Then
      m_date_from = GUI_FormatDate(dtp_from.Value, _
                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Me.dtp_to.Checked Then
      m_date_to = GUI_FormatDate(dtp_to.Value, _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If


    If chk_redeemable_ticket.Checked Then
      m_ticket_type = m_ticket_type & Me.chk_redeemable_ticket.Text
      _no_one = False
    Else
      _all = False
    End If

    If chk_promo_redeem_ticket.Checked Then
      If m_ticket_type.Length > 0 Then
        m_ticket_type = m_ticket_type & ", "
      End If
      m_ticket_type = m_ticket_type & Me.chk_promo_redeem_ticket.Text
      _no_one = False
    Else
      _all = False
    End If

    If chk_promo_nonredeem_ticket.Checked Then
      If m_ticket_type.Length > 0 Then
        m_ticket_type = m_ticket_type & ", "
      End If
      m_ticket_type = m_ticket_type & Me.chk_promo_nonredeem_ticket.Text
      _no_one = False
    Else
      _all = False
    End If

    If chk_jackpot.Checked Then
      If m_ticket_type.Length > 0 Then
        m_ticket_type = m_ticket_type & ", "
      End If
      m_ticket_type = m_ticket_type & Me.chk_jackpot.Text
      _no_one = False
    Else
      _all = False
    End If

    If chk_handpay.Checked Then
      If m_ticket_type.Length > 0 Then
        m_ticket_type = m_ticket_type & ", "
      End If
      m_ticket_type = m_ticket_type & Me.chk_handpay.Text
      _no_one = False
    Else
      _all = False
    End If

    If chk_offline.Checked Then
      If m_ticket_type.Length > 0 Then
        m_ticket_type = m_ticket_type & ", "
      End If
      m_ticket_type = m_ticket_type & Me.chk_offline.Text
      _no_one = False
    Else
      _all = False
    End If

    m_ticket_number = Me.ef_ticket_number.TextValue


  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region  ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window, ByVal TicketValidationNumber As String)
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    m_ticket_number = TicketValidationNumber
    Me.Display(True)
  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    Dim _total_columns As Integer

    _total_columns = GRID_COLUMNS

    With Me.Grid
      Call .Init(_total_columns, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE


      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Header(1).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' audit date
      .Column(GRID_COLUMN_AUDIT_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7675)
      .Column(GRID_COLUMN_AUDIT_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7663)
      .Column(GRID_COLUMN_AUDIT_DATE).Width = GRID_WIDTH_AUDIT_DATE
      .Column(GRID_COLUMN_AUDIT_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' amount
      .Column(GRID_COLUMN_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7670)
      .Column(GRID_COLUMN_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7665)
      .Column(GRID_COLUMN_AMOUNT).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT


      ' type
      .Column(GRID_COLUMN_TITO_TYCKET_TYPE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7670)
      .Column(GRID_COLUMN_TITO_TYCKET_TYPE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(254)
      .Column(GRID_COLUMN_TITO_TYCKET_TYPE).Width = GRID_WIDTH_TITO_TYCKET_TYPE
      .Column(GRID_COLUMN_TITO_TYCKET_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT



      ' state
      .Column(GRID_COLUMN_STATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7670)
      .Column(GRID_COLUMN_STATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7666)
      .Column(GRID_COLUMN_STATE).Width = GRID_WIDTH_STATE
      .Column(GRID_COLUMN_STATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' description
      .Column(GRID_COLUMN_DESCRIPTION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7670)
      .Column(GRID_COLUMN_DESCRIPTION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(688)
      .Column(GRID_COLUMN_DESCRIPTION).Width = GRID_WIDTH_DESCRIPTION
      .Column(GRID_COLUMN_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT



    End With
  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Dim filter_date_nothing As Date
    chk_redeemable_ticket.Checked = False
    chk_promo_redeem_ticket.Checked = False
    chk_promo_nonredeem_ticket.Checked = False
    chk_jackpot.Checked = False
    chk_handpay.Checked = False
    chk_offline.Checked = False
    chk_redeemable_ticket.Enabled = True
    chk_promo_redeem_ticket.Enabled = True
    chk_promo_nonredeem_ticket.Enabled = True
    chk_jackpot.Enabled = True
    chk_handpay.Enabled = True
    chk_offline.Enabled = True

    Me.dtp_from.Value = New DateTime(Now.Year, Now.Month, Now.Day, 0, 0, 0)

    Me.dtp_from.Checked = False
    Me.dtp_to.Value = Me.dtp_from.Value.AddDays(1)
    Me.dtp_to.Checked = False

    filter_date_nothing = Nothing

  End Sub ' SetDefaultValues

  ' PURPOSE: Get Sql WHERE to buil SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String

    Dim _str_where As String = ""
    Dim _where_added As Boolean
    Dim _ticket_type As String

    _where_added = False
    _ticket_type = ""

    ' Filter Dates
    If Me.dtp_from.Checked = True Then

      If _where_added = False Then
        _str_where = _str_where & " WHERE "
        _where_added = True
      Else
        _str_where = _str_where & " AND "
      End If

      _str_where = _str_where & " (TIA_INSERT_DATE >= " & GUI_FormatDateDB(dtp_from.Value) & ") "
    End If

    If Me.dtp_to.Checked = True Then

      If _where_added = False Then
        _str_where = _str_where & " WHERE "
        _where_added = True
      Else
        _str_where = _str_where & " AND "
      End If

      _str_where = _str_where & " (TIA_INSERT_DATE < " & GUI_FormatDateDB(dtp_to.Value) & ") "
    End If

    If _where_added = False Then
      _str_where = _str_where & " WHERE "
      _where_added = True
    Else
      _str_where = _str_where & " AND "
    End If

    _str_where = _str_where & " TIA_VALIDATION_NUMBER = " & m_ticket_number


    'Ticket Type filters
    If chk_redeemable_ticket.Checked Then
      _ticket_type = _ticket_type & " TIA_TYPE_ID = " & TITO_TICKET_TYPE.CASHABLE
    End If

    If chk_promo_redeem_ticket.Checked Then
      If _ticket_type.Length > 0 Then
        _ticket_type = _ticket_type & " OR TIA_TYPE_ID = " & TITO_TICKET_TYPE.PROMO_REDEEM
      Else
        _ticket_type = " TIA_TYPE_ID = " & TITO_TICKET_TYPE.PROMO_REDEEM
      End If
    End If

    If chk_promo_nonredeem_ticket.Checked Then
      If _ticket_type.Length > 0 Then
        _ticket_type = _ticket_type & " OR TIA_TYPE_ID = " & TITO_TICKET_TYPE.PROMO_NONREDEEM
      Else
        _ticket_type = " TIA_TYPE_ID = " & TITO_TICKET_TYPE.PROMO_NONREDEEM
      End If
    End If

    If chk_handpay.Checked Then
      If _ticket_type.Length > 0 Then
        _ticket_type = _ticket_type & " OR TIA_TYPE_ID = " & TITO_TICKET_TYPE.HANDPAY
      Else
        _ticket_type = " TIA_TYPE_ID = " & TITO_TICKET_TYPE.HANDPAY
      End If
    End If

    If chk_jackpot.Checked Then
      If _ticket_type.Length > 0 Then
        _ticket_type = _ticket_type & " OR TIA_TYPE_ID = " & TITO_TICKET_TYPE.JACKPOT
      Else
        _ticket_type = " TIA_TYPE_ID = " & TITO_TICKET_TYPE.JACKPOT
      End If
    End If

    If chk_offline.Checked Then
      If _ticket_type.Length > 0 Then
        _ticket_type = _ticket_type & " OR TIA_TYPE_ID = " & TITO_TICKET_TYPE.OFFLINE
      Else
        _ticket_type = " TIA_TYPE_ID = " & TITO_TICKET_TYPE.OFFLINE
      End If
    End If

    If _ticket_type.Length > 0 Then
      _str_where = (IIf(_str_where.Length > 0, _str_where & " AND ", _str_where))
      _str_where = _str_where & "(" & _ticket_type & ") "
    End If



    Return _str_where
  End Function ' GetSqlWhere


#End Region  ' Private Functions

#Region "Events"

#End Region ' Events

End Class
