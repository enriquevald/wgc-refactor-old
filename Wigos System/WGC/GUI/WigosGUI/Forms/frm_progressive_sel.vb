'-------------------------------------------------------------------
' Copyright � 2007 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_progressive_sel.vb
' DESCRIPTION:   This screen allows view, create and edit progressives.
' AUTHOR:        Jes�s �ngel Blanco Blanco
' CREATION DATE: 06-AUG-2014
'
' REVISION HISTORY:
'
' Date         Author     Description
' -----------  ------     -----------------------------------------------
' 06-AUG-2014  JAB        Initial version.
' 08-SEP-2014  FJC        Did not apply the terminal filter correctly 
' 10-SEP-2014  FJC        Add radioButton Group Box with details (progressive, levels, terminals)
' 07-OCT-2014  FJC        Fixed Bug 1426: Error en la b�squeda de Jackpots Progresivos.
' 29-OCT-2014  SGB        Fixed Bug WIG-1595: Error excel filter status & show levels.
' 26-MAR-2015  ANM        Calling GetProviderIdListSelected always returns a query
' 29-DEC-2016  DPC        Bug 21602:Jackpot Progresivo: excepci�n no controlada al filtrar por Detalle-Terminal
' 13-FEB-2017  ETP        Bug 24283: Extra column showed when TERMINAL_DATA_COLUMNS has been corrected.
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports System.Text
Imports WSI.Common

Public Class frm_progressive_sel
  Inherits frm_base_sel

#Region " Constans "

  Private Const FORM_DB_MIN_VERSION As Short = 224 ' TODO: Revisar que la base de datos sea la correcta.
  Private Const MAX_DATABASE_NAME_LENGTH As Integer = 50
  Private TERMINAL_DATA_COLUMNS As Int32

  ' QUERY columns
  Private Const SQL_COLUMN_PGS_PROGRESSIVE_ID As Integer = 0
  Private Const SQL_COLUMN_PGS_NAME As Integer = 1
  Private Const SQL_COLUMN_PGS_CREATED As Integer = 2
  Private Const SQL_COLUMN_PGS_CONTRIBUTION_PCT As Integer = 3
  Private Const SQL_COLUMN_PGS_NUM_LEVELS As Integer = 4
  Private Const SQL_COLUMN_PGS_AMOUNT As Integer = 5
  Private Const SQL_COLUMN_PGS_TERMINALS_LIST As Integer = 6
  Private Const SQL_COLUMN_PGS_STATUS As Integer = 7
  Private Const SQL_COLUMN_PGS_STATUS_CHANGED As Integer = 8

  '   Detail Levels
  Private Const SQL_COLUMN_PGL_LEVEL_ID As Integer = 9
  Private Const SQL_COLUMN_PGL_NAME As Integer = 10
  Private Const SQL_COLUMN_PGL_CONTRIBUTION_PCT As Integer = 11
  Private Const SQL_COLUMN_PGL_AMOUNT As Integer = 12

  '   Detail Terminals 
  Private Const SQL_COLUMN_PV_NAME As Integer = 9
  Private Const SQL_COLUMN_TE_NAME As Integer = 10
  Private Const SQL_COLUMN_TERMINAL_ID As Integer = 11

  ' Grid Columns
  ' Hiden
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_PGS_PROGRESSIVE_ID As Integer = 1
  Private Const GRID_COLUMN_PGS_CREATED As Integer = 2
  Private Const GRID_COLUMN_PGS_TERMINALS_LIST As Integer = 3
  Private Const GRID_COLUMN_PGS_STATUS_CHANGED As Integer = 4
  
  ' Showed
  Private Const GRID_COLUMN_PGS_NAME As Integer = 5
  Private Const GRID_COLUMN_PGS_NUM_LEVELS As Integer = 6
  Private Const GRID_COLUMN_PGS_STATUS As Integer = 7
  Private Const GRID_COLUMN_PGS_CONTRIBUTION_PCT As Integer = 8
  Private Const GRID_COLUMN_PGS_AMOUNT As Integer = 9

  '   Detail Levels
  Private Const GRID_COLUMN_PGL_LEVEL_ID As Integer = 10
  Private Const GRID_COLUMN_PGL_NAME As Integer = 11
  Private Const GRID_COLUMN_PGL_CONTRIBUTION_PCT As Integer = 12
  Private Const GRID_COLUMN_PGL_AMOUNT As Integer = 13

  '   Detail Terminals 
  Private GRID_INIT_TERMINAL_DATA As Integer

  Private Const GRID_HEADERS As Integer = 2
  Private Const GRID_COLUMNS As Integer = 10                  'Detail Progessive 
  Private GRID_COLUMNS_WITH_TERMINALS As Integer              'Detail Terminals
  Private Const GRID_COLUMNS_WITH_LEVELS As Integer = 14      'Detail Levels

  ' Width Columns
  ' Hiden
  Private Const GRID_WIDTH_COLUMN_PGS_PROGRESSIVE_ID As Integer = 0
  Private Const GRID_WIDTH_COLUMN_PGS_CREATED As Integer = 0
  Private Const GRID_WIDTH_COLUMN_PGS_TERMINALS_LIST As Integer = 0
  Private Const GRID_WIDTH_COLUMN_PGS_STATUS_CHANGED As Integer = 0

  ' Showed
  Private Const GRID_WIDTH_COLUMN_INDEX As Integer = 200
  Private Const GRID_WIDTH_COLUMN_PGS_NAME As Integer = 3300 '2910
  Private Const GRID_WIDTH_COLUMN_PGS_NUM_LEVELS As Integer = 885
  Private Const GRID_WIDTH_COLUMN_PGS_STATUS As Integer = 1725
  Private Const GRID_WIDTH_COLUMN_PGS_CONTRIBUTION_PCT As Integer = 1500
  Private Const GRID_WIDTH_COLUMN_PGS_AMOUNT As Integer = 1710
  Private Const GRID_WIDTH_COLUMN_PGL_LEVEL_ID As Integer = 1500
  Private Const GRID_WIDTH_COLUMN_PGL_NAME As Integer = 2500
  Private Const GRID_WIDTH_COLUMN_PGL_CONTRIBUTION_PCT As Integer = 1500
  Private Const GRID_WIDTH_COLUMN_PGL_AMOUNT As Integer = 1500
  Private Const GRID_WIDTH_COLUMN_PV_NAME As Integer = 3500
  Private Const GRID_WIDTH_COLUMN_TE_NAME As Integer = 3500

  ' Excel columns
  Private Const TYPE_EXCEL_COLUMN_NAME As Integer = 0
  Private Const TYPE_EXCEL_COLUMN_LEVEL As Integer = 1
  Private Const TYPE_EXCEL_COLUMN_STATUS As Integer = 2
  Private Const TYPE_EXCEL_COLUMN_CONTRIBUTION As Integer = 3
  Private Const TYPE_EXCEL_COLUMN_AMOUNT As Integer = 4

#End Region

#Region " Members "

  ' Report filters
  Public m_progressive_name As String
  Public m_progressive_details As String
  Public m_progressive_status As String
  Public m_progressive_terminals As String

  Private m_terminal_report_type As ReportType = ReportType.Provider
  Private m_refresh_grid As Boolean = False

#End Region

#Region " Overrides "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_PROGRESSIVE_SEL
    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)
    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = ef_progressive_name
  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    Dim _terminal_types() As Integer

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5202)                                         ' Progresivos

    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(1)            ' Salir

    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_PROGRESSIVE_EDIT).Write

    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5215) ' Provisionar

    ' ACC 03-OCT-2014 Remove this funtionallity
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Type = uc_button.ENUM_BUTTON_TYPE.USER
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Height += 25
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5216) ' Registrar progresivo otorgado

    Me.gb_progressive_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5217)                     ' Progresivo
    Me.ef_progressive_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5209)                     ' Nombre
    Me.ef_progressive_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, MAX_DATABASE_NAME_LENGTH)

    Me.gb_progressive_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5211)                   ' Estatus

    'Show Detail
    Me.gb_progressive_detail.Text = GLB_NLS_GUI_STATISTICS.GetString(377)                         'Detail
    Me.opt_progressive.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5217)                         'Progressivo
    Me.opt_level.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5210)                               'Niveles
    Me.opt_terminal.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5220)                            'Terminales

    'Filter
    Me.chk_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5218)                             ' Habilitado
    Me.chk_disabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5219)                            ' Deshabilitado

    _terminal_types = WSI.Common.Misc.AllTerminalTypesExcept3GS()
    Me.uc_pr_list.SetGroupBoxTitle(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5220))                   ' Terminales
    Call Me.uc_pr_list.Init(_terminal_types)

    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)

    ' Set Defaults 
    Call GUI_FilterReset()

    ' Grid
    Call GUI_StyleSheet()
  End Sub

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _sql As StringBuilder
    Dim _slq_where As String

    _sql = New StringBuilder()
    _sql.AppendLine("   SELECT   PGS_PROGRESSIVE_ID                                      ")
    _sql.AppendLine("          , PGS_NAME                                                ")
    _sql.AppendLine("          , PGS_CREATED                                             ")
    _sql.AppendLine("          , PGS_CONTRIBUTION_PCT                                    ")
    _sql.AppendLine("          , PGS_NUM_LEVELS                                          ")
    _sql.AppendLine("          , PGS_AMOUNT                                              ")
    _sql.AppendLine("          , PGS_TERMINAL_LIST                                       ")
    _sql.AppendLine("          , PGS_STATUS                                              ")
    _sql.AppendLine("          , PGS_STATUS_CHANGED                                      ")

    Select Case True
      Case Me.opt_level.Checked
        _sql.AppendLine("           , PGL_LEVEL_ID                                       ")
        _sql.AppendLine("           , PGL_NAME                                           ")
        _sql.AppendLine("           , PGL_CONTRIBUTION_PCT                               ")
        _sql.AppendLine("           , PGL_AMOUNT                                         ")
      Case Me.opt_terminal.Checked
        _sql.AppendLine("           , PV_NAME                                            ")
        _sql.AppendLine("           , TE_NAME                                            ")
        _sql.AppendLine("           , TE_TERMINAL_ID                                     ")
    End Select
    _sql.AppendLine("          FROM   PROGRESSIVES                                       ")
    Select Case True
      Case Me.opt_level.Checked
        _sql.AppendLine(" LEFT JOIN   PROGRESSIVES_LEVELS                                ")
        _sql.AppendLine("        ON   PGS_PROGRESSIVE_ID= PGL_PROGRESSIVE_ID             ")

      Case Me.opt_terminal.Checked
        _sql.AppendLine(" LEFT JOIN		TERMINAL_GROUPS                                    ")
        _sql.AppendLine("        ON		PGS_PROGRESSIVE_ID =  TG_ELEMENT_ID                ")
        _sql.AppendLine("       AND		TERMINAL_GROUPS.TG_ELEMENT_TYPE = " & WSI.Common.EXPLOIT_ELEMENT_TYPE.PROGRESSIVE)

        _sql.AppendLine(" LEFT JOIN		TERMINALS                                          ")
        _sql.AppendLine("        ON		TE_TERMINAL_ID = TG_TERMINAL_ID                    ")

        _sql.AppendLine(" LEFT JOIN		PROVIDERS                                          ")
        _sql.AppendLine("        ON		PV_ID = TE_PROV_ID                                 ")
    End Select

    _slq_where = GetSqlWhere()
    If Not String.IsNullOrEmpty(_slq_where) Then
      _sql.AppendLine("  WHERE " & _slq_where)
    End If

    _sql.AppendLine(" ORDER BY   PGS_NAME                                                ")

    Return _sql.ToString()

  End Function 'GUI_FilterGetSqlQuery

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_FilterReset()

    Me.ef_progressive_name.TextValue = ""
    Me.chk_enabled.Checked = True
    Me.chk_disabled.Checked = False
    Me.opt_progressive.Checked = True
    Call Me.uc_pr_list.SetDefaultValues()

    Me.chk_terminal_location.Checked = False
    Me.chk_terminal_location.Enabled = False

  End Sub ' GUI_FilterReset

  ' PURPOSE: Enable button in selected row
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_RowSelectedEvent(ByVal SelectedRow As Integer)

    If Me.Grid.NumRows > 0 AndAlso Me.Grid.Cell(SelectedRow, GRID_COLUMN_PGS_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5260) Then
      ' Functionality under development
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = True
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = True
    Else
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = False
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False
    End If

  End Sub  ' GUI_RowSelectedEvent

  ' PURPOSE: Process button actions in order to branch to a child screen
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)
    Select Case ButtonId

      Case ENUM_BUTTON.BUTTON_NEW, _
           ENUM_BUTTON.BUTTON_SELECT

        Dim _frm_progressive_edit As Object
        Dim _current_progressive_id As Integer

        _frm_progressive_edit = New frm_progressive_edit
        If (ButtonId = ENUM_BUTTON.BUTTON_NEW) Then
          _current_progressive_id = -1
        Else
          _current_progressive_id = Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_PGS_PROGRESSIVE_ID).Value
        End If

        Call _frm_progressive_edit.ShowEditItem(_current_progressive_id)

        _frm_progressive_edit = Nothing

        Call Me.Grid.Focus()

      Case ENUM_BUTTON.BUTTON_FILTER_APPLY
        Call GUI_StyleSheet()
        Call MyBase.GUI_ButtonClick(ButtonId)

      Case ENUM_BUTTON.BUTTON_CUSTOM_1
        Dim _frm_progressive_handpay As Object
        Dim _current_progressive_id As Integer

        _frm_progressive_handpay = New frm_progressive_handpay
        _current_progressive_id = Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_PGS_PROGRESSIVE_ID).Value
        Call _frm_progressive_handpay.ShowEditItem(_current_progressive_id)

        _frm_progressive_handpay = Nothing
        Call Me.Grid.Focus()

      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        Dim _frm As frm_progressive_provision
        Dim _progressive_id As Int64

        Windows.Forms.Cursor.Current = Cursors.WaitCursor

        Try
          If Me.Grid.Cell(Me.Grid.SelectedRows()(0), GRID_COLUMN_PGS_PROGRESSIVE_ID).Value <> String.Empty Then

            _progressive_id = Me.Grid.Cell(Me.Grid.SelectedRows()(0), GRID_COLUMN_PGS_PROGRESSIVE_ID).Value

            _frm = New frm_progressive_provision()
            _frm.ShowNewItem(_progressive_id)
          End If

        Finally
          Windows.Forms.Cursor.Current = Cursors.Default

        End Try

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub ' GUI_ButtonClick

  ' PURPOSE: Edit selected item
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ShowSelectedItem()
    Dim _frm_progressive_edit As Object
    Dim _frm_edit As frm_cage_control

    _frm_edit = New frm_cage_control()
    _frm_progressive_edit = New frm_progressive_edit

    Call _frm_progressive_edit.ShowEditItem()

    _frm_progressive_edit = Nothing
    Call Me.Grid.Focus()

  End Sub ' GUI_GetSelectedItems

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  '
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _terminal_data As List(Of Object)

    ' Assign Mapped columns (search for 'mapping' string in this file)
    Call MyBase.GUI_SetupRow(RowIndex, DbRow)

    ' Progressive Id
    If Not DbRow.IsNull(SQL_COLUMN_PGS_PROGRESSIVE_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PGS_PROGRESSIVE_ID).Value = DbRow.Value(SQL_COLUMN_PGS_PROGRESSIVE_ID)
    End If

    ' Progressive Name
    If Not DbRow.IsNull(SQL_COLUMN_PGS_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PGS_NAME).Value = DbRow.Value(SQL_COLUMN_PGS_NAME)
    End If

    ' Created
    If Not DbRow.IsNull(SQL_COLUMN_PGS_CREATED) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PGS_CREATED).Value = FormatDateTime(DbRow.Value(SQL_COLUMN_PGS_CREATED), DateFormat.LongDate)
    End If

    ' Contribution
    If Not DbRow.IsNull(SQL_COLUMN_PGS_CONTRIBUTION_PCT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PGS_CONTRIBUTION_PCT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_PGS_CONTRIBUTION_PCT), 4, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, 7) & "%"
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PGS_CONTRIBUTION_PCT).Value = GUI_FormatNumber(0, 4, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, 7) & "%"
    End If

    ' Num levels
    If Not DbRow.IsNull(SQL_COLUMN_PGS_NUM_LEVELS) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PGS_NUM_LEVELS).Value = DbRow.Value(SQL_COLUMN_PGS_NUM_LEVELS)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PGS_NUM_LEVELS).Value = 0
    End If

    ' Amount
    If Not DbRow.IsNull(SQL_COLUMN_PGS_AMOUNT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PGS_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PGS_AMOUNT), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PGS_AMOUNT).Value = GUI_FormatNumber(0, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE)
    End If

    ' Terminal list
    If Not DbRow.IsNull(SQL_COLUMN_PGS_TERMINALS_LIST) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PGS_TERMINALS_LIST).Value = DbRow.Value(SQL_COLUMN_PGS_TERMINALS_LIST)
    End If

    ' Status
    If Not DbRow.IsNull(SQL_COLUMN_PGS_STATUS) Then
      Dim _nls As Integer
      Dim _back_color As System.Drawing.Color
      Dim _fore_color As System.Drawing.Color

      _fore_color = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
      If DbRow.Value(SQL_COLUMN_PGS_STATUS) <> 0 Then
        _nls = 5260 ' Habilitado
        _back_color = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
      Else
        _nls = 5261 ' Deshabilitado
        _back_color = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_03)
      End If

      Me.Grid.Cell(RowIndex, GRID_COLUMN_PGS_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PGS_STATUS).BackColor = _back_color
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PGS_STATUS).ForeColor = _fore_color

    End If

    ' Status changed
    If Not DbRow.IsNull(SQL_COLUMN_PGS_STATUS_CHANGED) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PGS_STATUS_CHANGED).Value = FormatDateTime(DbRow.Value(SQL_COLUMN_PGS_STATUS_CHANGED), DateFormat.LongDate)
    End If

    Select Case True
      Case opt_level.Checked                                                                              ' Detail Levels

        ' Level Id
        If Not DbRow.IsNull(SQL_COLUMN_PGL_LEVEL_ID) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_PGL_LEVEL_ID).Value = DbRow.Value(SQL_COLUMN_PGL_LEVEL_ID)
        End If

        ' Level Name
        If Not DbRow.IsNull(SQL_COLUMN_PGL_NAME) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_PGL_NAME).Value = DbRow.Value(SQL_COLUMN_PGL_NAME)
        End If

        ' Level Contribution
        If Not DbRow.IsNull(SQL_COLUMN_PGL_CONTRIBUTION_PCT) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_PGL_CONTRIBUTION_PCT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_PGL_CONTRIBUTION_PCT), 4, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, 7) & "%"
        End If

        ' Level Amount
        If Not DbRow.IsNull(SQL_COLUMN_PGL_AMOUNT) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_PGL_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PGL_AMOUNT), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE)
        End If

      Case Me.opt_terminal.Checked                                                                        ' Detail Terminal
        ' Terminal Report
        If Not DbRow.IsNull(SQL_COLUMN_TERMINAL_ID) Then
          _terminal_data = TerminalReport.GetReportDataList(DbRow.Value(SQL_COLUMN_TERMINAL_ID), m_terminal_report_type)
          For _idx As Int32 = 0 To _terminal_data.Count - 1
            If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value Then
              Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
            End If
          Next
        End If

    End Select

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5217), m_progressive_name)        ' Progressive
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(377), m_progressive_details)           ' Details
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5211), m_progressive_status)      ' Status
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5220), m_progressive_terminals)   ' Terminals

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_progressive_name = ""
    m_progressive_details = ""
    m_progressive_status = ""
    m_progressive_terminals = ""

    ' Progressive name
    m_progressive_name = IIf(String.IsNullOrEmpty(ef_progressive_name.TextValue), AUDIT_NONE_STRING, ef_progressive_name.TextValue)

    ' Progressive option
    If opt_progressive.Checked Then
      m_progressive_details = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5217) 'Progressive
    ElseIf opt_terminal.Checked Then
      m_progressive_details = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5220) 'Terminals
    Else
      m_progressive_details = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5210) 'Level
    End If

    ' Progressive status
    If chk_enabled.Checked Xor chk_disabled.Checked Then
      m_progressive_status = IIf(chk_enabled.Checked, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5260), _
                                                      GLB_NLS_GUI_PLAYER_TRACKING.GetString(5261)) ' Enabled / Disabled
    Else
      m_progressive_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2645) ' Both
    End If

    ' Progressive terminals
    m_progressive_terminals = Me.uc_pr_list.GetTerminalReportText()

  End Sub ' GUI_ReportUpdateFilters

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()
    If m_refresh_grid Then
      Call GUI_StyleSheet()
    End If

    m_refresh_grid = False
  End Sub

  ' PURPOSE: Perform Report Data
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)
    Call MyBase.GUI_ReportParams(ExcelData)

    ExcelData.SetColumnFormat(TYPE_EXCEL_COLUMN_LEVEL, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.NUMERIC, 0)
    ExcelData.SetColumnFormat(TYPE_EXCEL_COLUMN_CONTRIBUTION, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.PERCENTAGE, 4)

  End Sub

#End Region

#Region " Public "

  ' PURPOSE: Opens dialog with selection mode settings
  '
  '  PARAMS:
  '     - INPUT:
  '           - CageSessionId
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub

#End Region

#Region " Privates "

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub GUI_StyleSheet()
    Dim _terminal_columns As List(Of ColumnSettings)

    Call GridColumnReIndex()

    With Me.Grid

      Select Case True
        Case opt_progressive.Checked
          ' Common Fields

          Call .Init(GRID_COLUMNS, GRID_HEADERS)
        Case opt_level.Checked
          ' With level fields

          Call .Init(GRID_COLUMNS_WITH_LEVELS, GRID_HEADERS)
        Case opt_terminal.Checked
          ' With terminal fields

          Call .Init(GRID_COLUMNS_WITH_TERMINALS, GRID_HEADERS)
      End Select

      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
      .IsSortable = False

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Header(1).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_COLUMN_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Progressive Id
      .Column(GRID_COLUMN_PGS_PROGRESSIVE_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_PGS_PROGRESSIVE_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_PGS_PROGRESSIVE_ID).Width = GRID_WIDTH_COLUMN_PGS_PROGRESSIVE_ID
      .Column(GRID_COLUMN_PGS_PROGRESSIVE_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_PGS_PROGRESSIVE_ID).IsColumnPrintable = False

      ' Progressive name
      .Column(GRID_COLUMN_PGS_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5217)                      ' Progresivo
      .Column(GRID_COLUMN_PGS_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5209)                      ' Nombre
      .Column(GRID_COLUMN_PGS_NAME).Width = GRID_WIDTH_COLUMN_PGS_NAME
      .Column(GRID_COLUMN_PGS_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_PGS_NAME).IsMerged = False

      ' Created
      .Column(GRID_COLUMN_PGS_CREATED).Header(0).Text = ""
      .Column(GRID_COLUMN_PGS_CREATED).Header(1).Text = ""
      .Column(GRID_COLUMN_PGS_CREATED).Width = GRID_WIDTH_COLUMN_PGS_CREATED
      .Column(GRID_COLUMN_PGS_CREATED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_PGS_CREATED).IsColumnPrintable = False

      ' Contribution pct
      .Column(GRID_COLUMN_PGS_CONTRIBUTION_PCT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5217)          ' Progresivo
      .Column(GRID_COLUMN_PGS_CONTRIBUTION_PCT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5213)          ' Contribuci�n
      .Column(GRID_COLUMN_PGS_CONTRIBUTION_PCT).Width = GRID_WIDTH_COLUMN_PGS_CONTRIBUTION_PCT
      .Column(GRID_COLUMN_PGS_CONTRIBUTION_PCT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Num levels
      .Column(GRID_COLUMN_PGS_NUM_LEVELS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5217)                ' Progresivo
      .Column(GRID_COLUMN_PGS_NUM_LEVELS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5210)                ' Niveles
      .Column(GRID_COLUMN_PGS_NUM_LEVELS).Width = GRID_WIDTH_COLUMN_PGS_NUM_LEVELS
      .Column(GRID_COLUMN_PGS_NUM_LEVELS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Amount
      .Column(GRID_COLUMN_PGS_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5217)                    ' Monto
      .Column(GRID_COLUMN_PGS_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5214)                    ' Monto
      .Column(GRID_COLUMN_PGS_AMOUNT).Width = GRID_WIDTH_COLUMN_PGS_AMOUNT
      .Column(GRID_COLUMN_PGS_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Terminal list
      .Column(GRID_COLUMN_PGS_TERMINALS_LIST).Header(0).Text = ""
      .Column(GRID_COLUMN_PGS_TERMINALS_LIST).Header(1).Text = ""
      .Column(GRID_COLUMN_PGS_TERMINALS_LIST).Width = GRID_WIDTH_COLUMN_PGS_TERMINALS_LIST
      .Column(GRID_COLUMN_PGS_TERMINALS_LIST).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_PGS_TERMINALS_LIST).IsColumnPrintable = False

      ' Status
      .Column(GRID_COLUMN_PGS_STATUS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5217)                    ' Progresivo
      .Column(GRID_COLUMN_PGS_STATUS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5211)                    ' Estado
      .Column(GRID_COLUMN_PGS_STATUS).Width = GRID_WIDTH_COLUMN_PGS_STATUS
      .Column(GRID_COLUMN_PGS_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Status changed
      .Column(GRID_COLUMN_PGS_STATUS_CHANGED).Header(0).Text = ""
      .Column(GRID_COLUMN_PGS_STATUS_CHANGED).Header(1).Text = ""
      .Column(GRID_COLUMN_PGS_STATUS_CHANGED).Width = GRID_WIDTH_COLUMN_PGS_STATUS_CHANGED
      .Column(GRID_COLUMN_PGS_STATUS_CHANGED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_PGS_STATUS_CHANGED).IsColumnPrintable = False

      Select Case True
        Case opt_progressive.Checked
          ' Common Fields

        Case opt_level.Checked
          ' Level Progressive Id
          .Column(GRID_COLUMN_PGL_LEVEL_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5210)                ' Niveles
          .Column(GRID_COLUMN_PGL_LEVEL_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5281)
          ''& " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5282)                ' Nivel Id.
          .Column(GRID_COLUMN_PGL_LEVEL_ID).Width = GRID_WIDTH_COLUMN_PGL_LEVEL_ID
          .Column(GRID_COLUMN_PGL_LEVEL_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

          ' Level Progressive name
          .Column(GRID_COLUMN_PGL_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5210)                    ' Niveles
          .Column(GRID_COLUMN_PGL_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5209)                    ' Nombre
          .Column(GRID_COLUMN_PGL_NAME).Width = GRID_WIDTH_COLUMN_PGL_NAME
          .Column(GRID_COLUMN_PGL_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

          ' Level Contribution pct
          .Column(GRID_COLUMN_PGL_CONTRIBUTION_PCT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5210)        ' Niveles
          .Column(GRID_COLUMN_PGL_CONTRIBUTION_PCT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5213)        ' Contribuci�n
          .Column(GRID_COLUMN_PGL_CONTRIBUTION_PCT).Width = GRID_WIDTH_COLUMN_PGL_CONTRIBUTION_PCT
          .Column(GRID_COLUMN_PGL_CONTRIBUTION_PCT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          ' Level Amount
          .Column(GRID_COLUMN_PGL_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5210)                  ' Niveles
          .Column(GRID_COLUMN_PGL_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5214)                  ' Monto
          .Column(GRID_COLUMN_PGL_AMOUNT).Width = GRID_WIDTH_COLUMN_PGL_AMOUNT
          .Column(GRID_COLUMN_PGL_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        Case opt_terminal.Checked
          '  Terminal Report
          _terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)
          For _idx As Int32 = 0 To _terminal_columns.Count - 1
            .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(232)
            .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = _terminal_columns(_idx).Header1
            .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = _terminal_columns(_idx).Width
            .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = _terminal_columns(_idx).Alignment
          Next
         
        Case Else
          ' Other future cases

      End Select

    End With

  End Sub 'GUI_StyleSheet

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the main SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Function GetSqlWhere() As String
    Dim _str_where As String
    Dim _str_xml As String
    Dim _tmp_check_box As System.Windows.Forms.CheckBox

    _str_where = ""
    _str_xml = ""

    ' Name filter
    If Not String.IsNullOrEmpty(ef_progressive_name.TextValue) Then
      _str_where &= " " & GUI_FilterField("PGS_NAME", Me.ef_progressive_name.Value, False, False, True)
    End If

    ' Status filter
    _tmp_check_box = New System.Windows.Forms.CheckBox()
    If chk_enabled.Checked Xor chk_disabled.Checked Then
      _tmp_check_box = IIf(chk_enabled.Checked, chk_enabled, chk_disabled)
      _str_where += IIf(String.IsNullOrEmpty(_str_where), "", " AND ") & " PGS_STATUS = " & IIf(_tmp_check_box.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5218), 1, 0)
    End If

    ' Terminal filter
    If Not Me.uc_pr_list.AllSelectedChecked Then

      _str_where += IIf(String.IsNullOrEmpty(_str_where), "", " AND ")
      _str_where += "     PGS_PROGRESSIVE_ID IN (                                                               "
      _str_where += "                             SELECT DISTINCT   TG_ELEMENT_ID                               "
      _str_where += "                                        FROM   TERMINAL_GROUPS                             "
      _str_where += "                                       WHERE   TG_TERMINAL_ID  IN " & Me.uc_pr_list.GetProviderIdListSelected()

      ' FJC 08-SEP-2014 did not apply the filter correctly
      ''_str_where += " AND tg_terminal_id = " & WSI.Common.EXPLOIT_ELEMENT_TYPE.PROGRESSIVE & ")"
      _str_where += " AND TG_ELEMENT_TYPE = " & WSI.Common.EXPLOIT_ELEMENT_TYPE.PROGRESSIVE & ")"

    End If

    Return _str_where

  End Function ' GetSqlWhere

  ' PURPOSE: Set Addittional info when show detail terminal (Area, Bank, etc.)
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GridColumnReIndex()
    TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(m_terminal_report_type)

    GRID_INIT_TERMINAL_DATA = 10

    GRID_COLUMNS_WITH_TERMINALS = GRID_INIT_TERMINAL_DATA + TERMINAL_DATA_COLUMNS
  End Sub

#End Region

#Region " Events "
  Private Sub chk_terminal_location_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminal_location.CheckedChanged
    If chk_terminal_location.Checked Then
      m_terminal_report_type = ReportType.Provider + ReportType.Location
    Else
      m_terminal_report_type = ReportType.Provider
    End If

    m_refresh_grid = True
  End Sub

  Private Sub opt_terminal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_terminal.CheckedChanged
    chk_terminal_location.Enabled = opt_terminal.Checked
  End Sub
#End Region

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
  End Sub

End Class