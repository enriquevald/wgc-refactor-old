﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_jackpots_meters_history.vb
' DESCRIPTION:   List Jackpot Meters History 
' AUTHOR:        Carlos Cáceres González
' CREATION DATE: 04-MAY-2017
'
' REVISION HISTORY:
'
' Date         Author  Description
' -----------  ------  -----------------------------------------------
' 04-MAY-2017   CCG     Initial version
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Text
Imports WSI.Common
Imports WSI.Common.Jackpot

Public Class frm_jackpots_history
  Inherits frm_base_sel

#Region " Constants "

  Private TERMINAL_DATA_COLUMNS As Int32

  ' Grid setup
  Private Const GRID_HEADER_ROWS As Integer = 2
  Private GRID_COLUMNS As Integer = 12

  ' DB Columns
  Private Const SQL_COLUMN_JACKPOT_NAME As Integer = 0
  Private Const SQL_COLUMN_JACKPOT_DATE_AWARDED As Integer = 1
  Private Const SQL_COLUMN_JACKPOT_TYPE As Integer = 2
  Private Const SQL_COLUMN_JACKPOT_ISO_CODE As Integer = 3
  Private Const SQL_COLUMN_AWARDED_AMOUNT As Integer = 4
  Private Const SQL_COLUMN_ACCOUNT_NAME As Integer = 5
  Private Const SQL_COLUMN_TERMINAL_PROVIDER As Integer = 6
  Private Const SQL_COLUMN_TERMINAL_NAME As Integer = 7
  Private Const SQL_COLUMN_TERMINAL_FLOOR As Integer = 8
  Private Const SQL_COLUMN_TERMINAL_AREA As Integer = 9
  Private Const SQL_COLUMN_TERMINAL_BANK As Integer = 10
  Private Const SQL_COLUMN_TERMINAL_POSITION As Integer = 11
  Private Const SQL_COLUMN_TERMINAL_ID As Integer = 12
  Private Const SQL_COLUMN_JACKPOT_STATUS As Integer = 13
  Private Const SQL_COLUMN_JACKPOT_POSITION As Integer = 14
  Private Const SQL_COLUMN_JACKPOT_TRY As Integer = 15

  ' Grid Columns
  Private GRID_COLUMN_INDEX As Integer
  Private GRID_COLUMN_JACKPOT_NAME As Integer
  Private GRID_COLUMN_JACKPOT_AWARDED_DATE As Integer
  Private GRID_COLUMN_JACKPOT_STATUS As Integer
  Private GRID_COLUMN_JACKPOT_TYPE As Integer
  Private GRID_COLUMN_JACKPOT_POSITION As Integer
  Private GRID_COLUMN_JACKPOT_TRY As Integer
  Private GRID_COLUMN_JACKPOT_ISO_CODE As Integer
  Private GRID_COLUMN_JACKPOT_AWARDED_AMOUNT As Integer
  Private GRID_COLUMN_ACCOUNT_NAME As Integer
  Private GRID_COLUMN_TERMINAL_PROVIDER As Integer
  Private GRID_COLUMN_TERMINAL_NAME As Integer
  Private GRID_COLUMN_TERMINAL_FLOOR As Integer
  Private GRID_INIT_TERMINAL_DATA As Integer
  Private GRID_COLUMN_TERMINAL_AREA As Integer
  Private GRID_COLUMN_TERMINAL_BANK As Integer
  Private GRID_COLUMN_TERMINAL_POSITION As Integer

  ' Width
  Private Const GRID_WIDTH_INDEX As Integer = 250
  Private Const GRID_WIDTH_JACKPOT_NAME As Integer = 2000
  Private Const GRID_WIDTH_JACKPOT_AWARDED_DATE As Integer = 1800
  Private Const GRID_WIDTH_JACKPOT_STATUS As Integer = 4000
  Private Const GRID_WIDTH_JACKPOT_TYPE As Integer = 1400
  Private Const GRID_WIDTH_JACKPOT_POSITION As Integer = 1400
  Private Const GRID_WIDTH_JACKPOT_TRY As Integer = 1000
  Private Const GRID_WIDTH_JACKPOT_ISO_CODE As Integer = 1000
  Private Const GRID_WIDTH_AWARDED_AMOUNT As Integer = 1800
  Private Const GRID_WIDTH_ACCOUNT_NAME As Integer = 2500
  Private Const GRID_WIDTH_TERMINAL_PROVIDER As Integer = 1400
  Private Const GRID_WIDTH_TERMINAL_NAME As Integer = 2600
  Private Const GRID_WIDTH_TERMINAL_FLOOR As Integer = 1400
  Private Const GRID_WIDTH_TERMINAL_AREA As Integer = 800
  Private Const GRID_WIDTH_TERMINAL_BANK As Integer = 800
  Private Const GRID_WIDTH_TERMINAL_POSITION As Integer = 800

#End Region ' Constants

#Region " Members "

  ' For report filters 
  Private m_date_from As String
  Private m_date_to As String
  Private m_jackpot_type As StringBuilder
  Private m_jackpot_status As String
  Private m_grouped_by As String
  Private m_selected_jackpots As String
  Private m_selected_terminals As String
  Private m_terminal_report_type As ReportType

  Private m_refresh_grid As Boolean = False

  ' Subtotaling
  Private m_subtotal_id As String
  Private m_subtotal_amount As Decimal
  Private m_total_amount As Decimal

#End Region ' Members

#Region " Constructor "

  Public Sub New()

    ' This call is required by the designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
  End Sub

#End Region ' Constructor

#Region " Overrides "

  ''' <summary>
  ''' Set Form Id
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_JACKPOT_HISTORY_SEL
    MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ''' <summary>
  '''  Initialize every form control
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8265)

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False

    ' Date time filter
    Me.uc_dsl.Init(GLB_NLS_GUI_JACKPOT_MGR.Id(207))
    Me.uc_dsl.HideClosingHourCombo()
    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)

    ' Jackpot type filter
    Me.Uc_jackpot_type_sel.Init()

    Call InitJackpotCheckedList()

    ' Providers - Terminals
    Call Me.uc_pr_list.Init(WSI.Common.Misc.GamingTerminalTypeList())

    ' Group By
    Me.gb_order.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(781)
    opt_order_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1597)
    opt_order_Jackpot.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6945)

    Call SetDefaultValues()

    Call GUI_StyleSheet()

  End Sub ' GUI_InitControls

  ''' <summary>
  ''' Set Focus in a control
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.uc_dsl
  End Sub 'GUI_SetInitialFocus

  ''' <summary>
  ''' Set default values to filters
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ''' <summary>
  ''' Process clicks on data grid (double-clicks) and select button
  ''' </summary>
  ''' <param name="ButtonId"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Dim _indx_sel As Integer

    _indx_sel = -1

    Select Case ButtonId
      Case frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY
        Call GUI_StyleSheet()
        Call MyBase.GUI_ButtonClick(ButtonId)

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  ''' <summary>
  ''' Sets the values of a row
  ''' </summary>
  ''' <param name="RowIndex"></param>
  ''' <param name="DbRow"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _result As Boolean

    _result = True

    With Me.Grid

      ' Jackpot name
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_JACKPOT_NAME)) Then
        .Cell(RowIndex, GRID_COLUMN_JACKPOT_NAME).Value = DbRow.Value(SQL_COLUMN_JACKPOT_NAME)
      End If

      ' Jackpot date awarded
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_JACKPOT_DATE_AWARDED)) Then
        .Cell(RowIndex, GRID_COLUMN_JACKPOT_AWARDED_DATE).Value =
          GUI_FormatDate(DbRow.Value(SQL_COLUMN_JACKPOT_DATE_AWARDED), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
      End If

      ' Jackpot status
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_JACKPOT_STATUS)) Then
        .Cell(RowIndex, GRID_COLUMN_JACKPOT_STATUS).Value = GetJackpotStatus(DbRow.Value(SQL_COLUMN_JACKPOT_STATUS))
      End If

      'Jackpot position
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_JACKPOT_POSITION)) Then
        .Cell(RowIndex, GRID_COLUMN_JACKPOT_POSITION).Value = DbRow.Value(SQL_COLUMN_JACKPOT_POSITION)
      End If

      'Jackpot type
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_JACKPOT_TYPE)) Then
        .Cell(RowIndex, GRID_COLUMN_JACKPOT_TYPE).Value = GetJackpotTypeName(DbRow.Value(SQL_COLUMN_JACKPOT_TYPE))
      End If

      'Jackpot Retry
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_JACKPOT_TRY)) Then
        .Cell(RowIndex, GRID_COLUMN_JACKPOT_TRY).Value = DbRow.Value(SQL_COLUMN_JACKPOT_TRY)
      End If

      ' Jackpot IsoCode
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_JACKPOT_ISO_CODE)) Then
        .Cell(RowIndex, GRID_COLUMN_JACKPOT_ISO_CODE).Value = DbRow.Value(SQL_COLUMN_JACKPOT_ISO_CODE)
      End If

      ' Awarded amount
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_AWARDED_AMOUNT)) Then
        .Cell(RowIndex, GRID_COLUMN_JACKPOT_AWARDED_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_AWARDED_AMOUNT))
      End If

      ' Account name
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_ACCOUNT_NAME)) Then
        .Cell(RowIndex, GRID_COLUMN_ACCOUNT_NAME).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_NAME)
      End If

      ' Terminal provider
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_TERMINAL_PROVIDER)) Then
        .Cell(RowIndex, GRID_COLUMN_TERMINAL_PROVIDER).Value = DbRow.Value(SQL_COLUMN_TERMINAL_PROVIDER)
      End If

      ' Terminal name
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_TERMINAL_NAME)) Then
        .Cell(RowIndex, GRID_COLUMN_TERMINAL_NAME).Value = DbRow.Value(SQL_COLUMN_TERMINAL_NAME)
      End If

      ' Terminal Floor id
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_TERMINAL_FLOOR)) Then
        .Cell(RowIndex, GRID_COLUMN_TERMINAL_FLOOR).Value = DbRow.Value(SQL_COLUMN_TERMINAL_FLOOR)
      End If

      If chk_terminal_location.Checked Then

        'Area
        If Not IsDBNull(DbRow.Value(SQL_COLUMN_TERMINAL_AREA)) Then
          .Cell(RowIndex, GRID_COLUMN_TERMINAL_AREA).Value = DbRow.Value(SQL_COLUMN_TERMINAL_AREA)
        End If

        'Bank
        If Not IsDBNull(DbRow.Value(SQL_COLUMN_TERMINAL_BANK)) Then
          .Cell(RowIndex, GRID_COLUMN_TERMINAL_BANK).Value = DbRow.Value(SQL_COLUMN_TERMINAL_BANK)
        End If

        'Position
        If Not IsDBNull(DbRow.Value(SQL_COLUMN_TERMINAL_POSITION)) Then
          .Cell(RowIndex, GRID_COLUMN_TERMINAL_POSITION).Value = DbRow.Value(SQL_COLUMN_TERMINAL_POSITION)
        End If

      End If

    End With

    Return _result

  End Function ' GUI_SetupRow

  ''' <summary>
  ''' Get the defined Query Type
  ''' </summary>
  ''' <returns>ENUM_QUERY_TYPE</returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_GetQueryType() As GUI_Controls.frm_base_sel.ENUM_QUERY_TYPE

    Return MyBase.GUI_GetQueryType()

  End Function ' GUI_GetQueryType

  ''' <summary>
  '''  SQL Command query ready to send to the database
  ''' </summary>
  ''' <returns>Build an SQL Command query from conditions set in the filters</returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_GetSqlCommand() As SqlClient.SqlCommand

    Return MyBase.GUI_GetSqlCommand()

  End Function ' GUI_GetSqlCommand

  ''' <summary>
  ''' Show form to edit new Item
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_EditNewItem()
    Dim _frm As frm_countr_edit

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_countr_edit
    _frm.ShowNewItem()

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub ' GUI_EditNewItem

  ''' <summary>
  ''' Build an SQL query from conditions set in the filters
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _str_sql As StringBuilder

    _str_sql = New StringBuilder()

    _str_sql.AppendLine("     SELECT   JS.JS_NAME AS NAME                                             ")
    _str_sql.AppendLine("            , JAD.JAD_DATE_AWARD                         AS DATE_AWARDED     ")
    _str_sql.AppendLine("            , JA.JA_TYPE                                 AS JACKPOT_TYPE     ")
    _str_sql.AppendLine("            , JS.JS_ISO_CODE                             AS ISO_CODE         ")
    _str_sql.AppendLine("            , JAD.JAD_AMOUNT                             AS AMOUNT           ")
    _str_sql.AppendLine("            , AC.AC_HOLDER_NAME                          AS ACCOUNT_NAME     ")
    _str_sql.AppendLine("            , TE.TE_PROVIDER_ID                          AS PROVIDER         ")
    _str_sql.AppendLine("            , TE.TE_NAME                                 AS TERMINAL         ")
    _str_sql.AppendLine("            , TE.TE_FLOOR_ID                             AS FLOOR_ID         ")
    _str_sql.AppendLine("            , AR.AR_NAME                                 AS AREA             ")
    _str_sql.AppendLine("            , BA.BK_NAME                                 AS BANK             ")
    _str_sql.AppendLine("            , TE.TE_POSITION                             AS POSITION         ")
    _str_sql.AppendLine("            , TE.TE_TERMINAL_ID                          AS TERMINAL_ID      ")
    _str_sql.AppendLine("            , JAD.JAD_STATUS                             AS JACKPOT_STATUS   ")
    _str_sql.AppendLine("            , JAD.JAD_POSITION                           AS JACKPOT_POSITION ")
    _str_sql.AppendLine("            , JAD.JAD_RETRIES                            AS JACKPOT_RETRIES  ")
    _str_sql.AppendLine("       FROM   JACKPOTS_AWARD_DETAIL                      AS JAD              ")
    _str_sql.AppendLine(" INNER JOIN   JACKPOTS_AWARD                             AS JA               ")
    _str_sql.AppendLine("         ON   JA.JA_ID = JAD.JAD_AWARD_ID                                    ")
    _str_sql.AppendLine(" INNER JOIN   JACKPOTS_SETTINGS                          AS JS               ")
    _str_sql.AppendLine("         ON   JS.JS_JACKPOT_ID = JA.JA_JACKPOT_ID                            ")
    _str_sql.AppendLine("  LEFT JOIN   TERMINALS                                  AS TE               ")
    _str_sql.AppendLine("         ON   TE.TE_TERMINAL_ID = JAD.JAD_TERMINAL_ID                        ")
    _str_sql.AppendLine("  LEFT JOIN   BANKS                                      AS BA               ")
    _str_sql.AppendLine("         ON   BA.BK_BANK_ID = TE.TE_BANK_ID                                  ")
    _str_sql.AppendLine("  LEFT JOIN   AREAS                                      AS AR               ")
    _str_sql.AppendLine("         ON   AR.AR_AREA_ID = BA.BK_AREA_ID                                  ")
    _str_sql.AppendLine("  LEFT JOIN   ACCOUNTS                                   AS AC               ")
    _str_sql.AppendLine("         ON   AC.AC_ACCOUNT_ID = JAD.JAD_ACCOUNT_ID                          ")
    ' Where 
    _str_sql.Append(GetSqlWhere())

    ' Order
    If opt_order_date.Checked Then
      _str_sql.AppendLine("   ORDER BY   JAD.JAD_DATE_AWARD DESC, JS.JS_NAME ASC ")
    ElseIf opt_order_Jackpot.Checked Then
      _str_sql.AppendLine("   ORDER BY   JS.JS_NAME ASC, JAD.JAD_DATE_AWARD DESC ")
    End If

    Return _str_sql.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ''' <summary>
  ''' Set proper values for form filters being sent to the report
  ''' </summary>
  ''' <param name="PrintData"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_JACKPOT_MGR.GetString(208), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_JACKPOT_MGR.GetString(209), m_date_to)
    PrintData.SetFilter(GLB_NLS_GUI_CONTROLS.GetString(261), m_jackpot_status)
    PrintData.SetFilter(GLB_NLS_GUI_JACKPOT_MGR.GetString(559), m_jackpot_type.ToString())
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2990), m_grouped_by)
    PrintData.SetFilter(GLB_NLS_GUI_JACKPOT_MGR.GetString(546), m_selected_jackpots)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_selected_terminals)

  End Sub ' GUI_ReportFilter

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Params.Title = GLB_NLS_GUI_JACKPOT_MGR.GetString(206)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  ''' <summary>
  ''' Set texts for date and jackpot type filters
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportUpdateFilters()
    Dim _separator As String

    _separator = ""
    m_date_from = ""
    m_date_to = ""
    m_selected_jackpots = ""
    Me.m_jackpot_type = New StringBuilder()

    ' Date 
    If Me.uc_dsl.FromDateSelected Then
      m_date_from = GUI_FormatDate(Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime), _
                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    If Me.uc_dsl.ToDateSelected Then
      m_date_to = GUI_FormatDate(Me.uc_dsl.ToDate.AddHours(Me.uc_dsl.ClosingTime), _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    ' Jackpot type
    For Each _type As Jackpot.JackpotType In Me.Uc_jackpot_type_sel.SelectedTypes

      Me.m_jackpot_type.Append(_separator + GetJackpotTypeName(_type))

      If String.IsNullOrEmpty(_separator) Then
        _separator = ", "
      End If

    Next

    Me.m_jackpot_status = uc_jackpot_award_status_sel.GetCurrentFilter()

    If (opt_order_date.Checked) Then
      m_grouped_by = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1597)
    End If

    If (opt_order_Jackpot.Checked) Then
      m_grouped_by = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6945)
    End If

    Call ReportSelectedJackpotsFilter()

    ' Providers - Terminals
    Me.m_selected_terminals = Me.uc_pr_list.GetTerminalReportText()

  End Sub ' GUI_ReportUpdateFilters

  ''' <summary>
  ''' Previous check in filters
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_FilterCheck() As Boolean
    ' Dates selection 
    If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate > Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    End If

    Return True
  End Function 'GUI_FilterCheck

  ''' <summary>
  ''' cleans grid
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ClearGrid()

    MyBase.GUI_ClearGrid()

  End Sub ' GUI_ClearGrid

  ''' <summary>
  ''' Actions before first Row created
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_BeforeFirstRow()

    If m_refresh_grid Then
      Call GUI_StyleSheet()
    End If

    m_refresh_grid = False

    Call ResetSubtotals()

    Call ResetTotals()

    m_subtotal_id = ""

  End Sub ' GUI_BeforeFirstRow

  ''' <summary>
  ''' Before Adding Row Calculate Totals & Subtotals
  ''' </summary>
  ''' <param name="DbRow"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _amount As Decimal

    If Not DbRow.IsNull(SQL_COLUMN_AWARDED_AMOUNT) Then
      _amount = DbRow.Value(SQL_COLUMN_AWARDED_AMOUNT)
    End If

    If opt_order_date.Checked Then
      If Not String.Equals(m_subtotal_id, GUI_FormatDate(DbRow.Value(SQL_COLUMN_JACKPOT_DATE_AWARDED), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)) Then
        If Not String.IsNullOrEmpty(m_subtotal_id) Then
          DrawRow(False)
        End If
        m_subtotal_id = GUI_FormatDate(DbRow.Value(SQL_COLUMN_JACKPOT_DATE_AWARDED), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)

        ResetSubtotals()
      End If
    Else
      If Not String.Equals(m_subtotal_id, DbRow.Value(SQL_COLUMN_JACKPOT_NAME)) Then
        If Not String.IsNullOrEmpty(m_subtotal_id) Then
          DrawRow(False)
        End If
        m_subtotal_id = DbRow.Value(SQL_COLUMN_JACKPOT_NAME)

        ResetSubtotals()
      End If

    End If

    ' Update subtotals
    m_subtotal_amount += _amount

    'Update totals
    m_total_amount += _amount

    Return True

  End Function ' GUI_CheckOutRowBeforeAdd

#End Region ' Overrides

#Region " Private Functions "

  ''' <summary>
  ''' Set Style to the Grid
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GUI_StyleSheet()
    Dim _terminal_columns As List(Of ColumnSettings)

    Call GridColumnReIndex()

    ' Grid StyleSheet
    With Me.Grid

      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, False)

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_INDEX).Header(1).Text = String.Empty
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Jackpot name
      .Column(GRID_COLUMN_JACKPOT_NAME).Header(0).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(546)
      .Column(GRID_COLUMN_JACKPOT_NAME).Header(1).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(560)
      .Column(GRID_COLUMN_JACKPOT_NAME).Width = GRID_WIDTH_JACKPOT_NAME
      .Column(GRID_COLUMN_JACKPOT_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Jackpot awarded date
      .Column(GRID_COLUMN_JACKPOT_AWARDED_DATE).Header(0).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(546)
      .Column(GRID_COLUMN_JACKPOT_AWARDED_DATE).Header(1).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(561)
      .Column(GRID_COLUMN_JACKPOT_AWARDED_DATE).Width = GRID_WIDTH_JACKPOT_AWARDED_DATE
      .Column(GRID_COLUMN_JACKPOT_AWARDED_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Jackpot status
      .Column(GRID_COLUMN_JACKPOT_STATUS).Header(0).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(546)
      .Column(GRID_COLUMN_JACKPOT_STATUS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1747)
      .Column(GRID_COLUMN_JACKPOT_STATUS).Width = GRID_WIDTH_JACKPOT_STATUS
      .Column(GRID_COLUMN_JACKPOT_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Jackpot type
      .Column(GRID_COLUMN_JACKPOT_TYPE).Header(0).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(546)
      .Column(GRID_COLUMN_JACKPOT_TYPE).Header(1).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(562)
      .Column(GRID_COLUMN_JACKPOT_TYPE).Width = GRID_WIDTH_JACKPOT_TYPE
      .Column(GRID_COLUMN_JACKPOT_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Jackpot Position
      .Column(GRID_COLUMN_JACKPOT_POSITION).Header(0).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(546)
      .Column(GRID_COLUMN_JACKPOT_POSITION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5222)
      .Column(GRID_COLUMN_JACKPOT_POSITION).Width = GRID_WIDTH_JACKPOT_POSITION
      .Column(GRID_COLUMN_JACKPOT_POSITION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Jackpot Tries
      .Column(GRID_COLUMN_JACKPOT_TRY).Header(0).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(546)
      .Column(GRID_COLUMN_JACKPOT_TRY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7652)
      .Column(GRID_COLUMN_JACKPOT_TRY).Width = GRID_WIDTH_JACKPOT_TRY
      .Column(GRID_COLUMN_JACKPOT_TRY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Jackpot Iso Code
      .Column(GRID_COLUMN_JACKPOT_ISO_CODE).Header(0).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(296)
      .Column(GRID_COLUMN_JACKPOT_ISO_CODE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8085)
      .Column(GRID_COLUMN_JACKPOT_ISO_CODE).Width = GRID_WIDTH_JACKPOT_ISO_CODE
      .Column(GRID_COLUMN_JACKPOT_ISO_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Jackpot awarded amount
      .Column(GRID_COLUMN_JACKPOT_AWARDED_AMOUNT).Header(0).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(296)
      .Column(GRID_COLUMN_JACKPOT_AWARDED_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3372)
      .Column(GRID_COLUMN_JACKPOT_AWARDED_AMOUNT).Width = GRID_WIDTH_AWARDED_AMOUNT
      .Column(GRID_COLUMN_JACKPOT_AWARDED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Account name
      .Column(GRID_COLUMN_ACCOUNT_NAME).Header(0).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(296)
      .Column(GRID_COLUMN_ACCOUNT_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2615)
      .Column(GRID_COLUMN_ACCOUNT_NAME).Width = GRID_WIDTH_ACCOUNT_NAME
      .Column(GRID_COLUMN_ACCOUNT_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Terminal provider
      .Column(GRID_COLUMN_TERMINAL_PROVIDER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(492)
      .Column(GRID_COLUMN_TERMINAL_PROVIDER).Header(1).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(564)
      .Column(GRID_COLUMN_TERMINAL_PROVIDER).Width = GRID_WIDTH_TERMINAL_PROVIDER
      .Column(GRID_COLUMN_TERMINAL_PROVIDER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Terminal name
      .Column(GRID_COLUMN_TERMINAL_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(492)
      .Column(GRID_COLUMN_TERMINAL_NAME).Header(1).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(560)
      .Column(GRID_COLUMN_TERMINAL_NAME).Width = GRID_WIDTH_TERMINAL_NAME
      .Column(GRID_COLUMN_TERMINAL_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Terminal floor
      .Column(GRID_COLUMN_TERMINAL_FLOOR).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(492)
      .Column(GRID_COLUMN_TERMINAL_FLOOR).Header(1).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(565)
      .Column(GRID_COLUMN_TERMINAL_FLOOR).Width = GRID_WIDTH_TERMINAL_FLOOR
      .Column(GRID_COLUMN_TERMINAL_FLOOR).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      '  Terminal Report
      _terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)
      For _idx As Int32 = 0 To _terminal_columns.Count - 1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(492)
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = _terminal_columns(_idx).Header1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = _terminal_columns(_idx).Width
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = _terminal_columns(_idx).Alignment
      Next

    End With

  End Sub ' GUI_StyleSheet

  Private Sub GridColumnReIndex()

    TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(m_terminal_report_type)

    GRID_COLUMN_INDEX = 0
    GRID_COLUMN_JACKPOT_NAME = 1
    GRID_COLUMN_JACKPOT_AWARDED_DATE = 2
    GRID_COLUMN_JACKPOT_STATUS = 3
    GRID_COLUMN_JACKPOT_TYPE = 4
    GRID_COLUMN_JACKPOT_POSITION = 5
    GRID_COLUMN_JACKPOT_TRY = 6
    GRID_COLUMN_JACKPOT_ISO_CODE = 7
    GRID_COLUMN_JACKPOT_AWARDED_AMOUNT = 8
    GRID_COLUMN_ACCOUNT_NAME = 9
    GRID_COLUMN_TERMINAL_PROVIDER = 10
    GRID_COLUMN_TERMINAL_NAME = 11
    GRID_COLUMN_TERMINAL_FLOOR = 12
    GRID_INIT_TERMINAL_DATA = 13
    GRID_COLUMN_TERMINAL_AREA = TERMINAL_DATA_COLUMNS + 10
    GRID_COLUMN_TERMINAL_BANK = TERMINAL_DATA_COLUMNS + 11
    GRID_COLUMN_TERMINAL_POSITION = TERMINAL_DATA_COLUMNS + 12

    GRID_COLUMNS = TERMINAL_DATA_COLUMNS + 13

  End Sub ' GridColumnReIndex

  ''' <summary>
  ''' Set Default Values
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SetDefaultValues()
    Dim _closing_time As Integer
    Dim _final_time As Date
    Dim _now As Date

    _now = WGDB.Now

    _closing_time = GetDefaultClosingTime()
    _final_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)
    If _final_time > _now Then
      _final_time = _final_time.AddDays(-1)
    End If

    _final_time = _final_time.Date
    Me.uc_dsl.ToDate = _final_time
    Me.uc_dsl.ToDateSelected = True
    Me.uc_dsl.FromDate = _final_time.AddDays(-1)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.ClosingTime = _closing_time
    Me.Uc_jackpot_type_sel.SetDefaultValues()

    Me.chk_terminal_location.Checked = False
    Me.m_terminal_report_type = ReportType.None

    opt_order_date.Checked = True

    'Filter Jackpots
    Me.uc_jackpot_checked.SetDefaultValues()

    Call Me.uc_pr_list.SetDefaultValues()

    Call Me.uc_jackpot_award_status_sel.SetDefaultValues(False)

  End Sub ' SetDefaultValues

  ''' <summary>
  '''  Build the variable part of the WHERE clause (the one that depends on filter values) for the
  '''  main SQL Query.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetSqlWhere() As String

    Dim _str_where As StringBuilder
    Dim _date_filter_query As String
    Dim _list As List(Of String)
    Dim _where_status As String

    _str_where = New StringBuilder

    _str_where.AppendLine(String.Format("      WHERE   JA.JA_TYPE != {0}", Jackpot.JackpotType.Notification.GetHashCode.ToString))

    ' Date awarded Filter
    _date_filter_query = Me.uc_dsl.GetSqlFilterCondition("JAD.JAD_DATE_AWARD")

    If Not String.IsNullOrEmpty(_date_filter_query) Then
      _str_where.AppendLine(String.Format("        AND   {0}", _date_filter_query))
    End If

    ' Jackpot type Filter
    If Me.Uc_jackpot_type_sel.SelectedTypes.Count > 0 Then

      _list = New List(Of String)

      For Each _type As Jackpot.JackpotType In Me.Uc_jackpot_type_sel.SelectedTypes

        Select Case _type
          Case Jackpot.JackpotType.Main
            _list.Add(Jackpot.JackpotType.Main.GetHashCode.ToString)
          Case Jackpot.JackpotType.Hidden
            _list.Add(Jackpot.JackpotType.Hidden.GetHashCode.ToString)
          Case Jackpot.JackpotType.PrizeSharing
            _list.Add(Jackpot.JackpotType.PrizeSharing.GetHashCode.ToString)
          Case Jackpot.JackpotType.HappyHour
            _list.Add(Jackpot.JackpotType.HappyHourBefore.GetHashCode.ToString)
            _list.Add(Jackpot.JackpotType.HappyHourAfter.GetHashCode.ToString)

        End Select

      Next

      _str_where.AppendLine(String.Format("        AND   JA.JA_TYPE IN ({0})", String.Join(",", _list.ToArray)))

    End If

    ' Filter jackpots
    If Not Me.uc_jackpot_checked.IsAllSelected() Then
      _str_where.AppendLine(String.Format("        AND   JA.JA_JACKPOT_ID IN ({0})", Me.uc_jackpot_checked.SelectedIndexesList()))
    End If

    ' Filter Providers - Terminals
    If Not Me.uc_pr_list.AllSelectedChecked AndAlso Me.uc_pr_list.TerminalListHasValues Then
      _str_where.AppendLine(String.Format("        AND   TE.TE_TERMINAL_ID IN ({0})", Me.uc_pr_list.GetProviderIdListSelected()))
    End If

    _where_status = Me.uc_jackpot_award_status_sel.GetSqlWhere()

    If Not String.IsNullOrEmpty(_where_status) Then
      _str_where.AppendLine(String.Format("        {0} ", _where_status))
    End If

    Return _str_where.ToString

  End Function ' GetSqlWhere

  Private Function GetJackpotTypeName(Type As Jackpot.JackpotType) As String

    Dim _type_name As String

    _type_name = ""

    Select Case Type

      Case Jackpot.JackpotType.Main
        _type_name = GLB_NLS_GUI_JACKPOT_MGR.GetString(555)
      Case Jackpot.JackpotType.Hidden
        _type_name = GLB_NLS_GUI_JACKPOT_MGR.GetString(556)
      Case Jackpot.JackpotType.PrizeSharing
        _type_name = GLB_NLS_GUI_JACKPOT_MGR.GetString(557)
      Case Jackpot.JackpotType.HappyHour _
         , Jackpot.JackpotType.HappyHourBefore _
         , Jackpot.JackpotType.HappyHourAfter
        _type_name = GLB_NLS_GUI_JACKPOT_MGR.GetString(558)

    End Select

    Return _type_name
  End Function ' GetJackpotTypeName

  ''' <summary>
  ''' Get Coma separated status by Jackpot
  ''' </summary>
  ''' <param name="Status"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetJackpotStatus(Status As Int32) As String

    Dim _sb As StringBuilder
    Dim _binary_array As String
    Dim _count As Int32
    Dim _idx As Int32

    _sb = New StringBuilder()
    _count = [Enum].GetNames(GetType(JackpotAwardEvents.JackpotStatusFlag)).Length
    _binary_array = String.Format("{0}0", Convert.ToString(Status, 2))
    _binary_array = StrReverse(_binary_array)
    _idx = 0

    For Each _status As JackpotAwardEvents.JackpotStatusFlag In [Enum].GetValues(GetType(JackpotAwardEvents.JackpotStatusFlag))

      If (_idx = _binary_array.Length) Then
        Exit For
      End If

      If _binary_array(_idx) = "1" Then
        _sb.Append(IIf(_sb.Length() = 0, "", ", "))
        _sb.Append(JackpotAwardEvents.AwardStatusToString(_status))
      End If

      _idx += 1
    Next

    Return _sb.ToString

  End Function ' GetJackpotStatus

  Private Sub InitJackpotCheckedList()
    Dim _dt_jackpots As DataTable = Nothing

    Me.uc_jackpot_checked.GroupBoxText = GLB_NLS_GUI_JACKPOT_MGR.GetString(546)  '"Jackpots"     
    Me.uc_jackpot_checked.ColumnWidth(2, 2600)
    If New Jackpot().GetAllJackpots(_dt_jackpots) Then
      Me.uc_jackpot_checked.Add(_dt_jackpots)
    End If

  End Sub ' InitJackpotCheckedList

  Private Sub ReportSelectedJackpotsFilter()

    Me.m_selected_jackpots = IIf(Me.uc_jackpot_checked.IsAllSelected(), _
                                 GLB_NLS_GUI_AUDITOR.GetString(263), _
                                 Me.uc_jackpot_checked.SelectedValuesList())

  End Sub ' ReportSelectedJackpotsFilter

  ''' <summary>
  ''' Reset the subtotals
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub ResetSubtotals()

    Me.m_subtotal_amount = 0.0

  End Sub ' ResetSubtotals

  ''' <summary>
  ''' Reset the totals
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub ResetTotals()

    m_total_amount = 0.0

  End Sub ' ResetTotals

  Private Function DrawRow(ByVal IsTotal As Boolean) As Boolean
    Dim _idx_row As Integer
    Dim _subtotal_title As String

    _subtotal_title = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2271) + " "

    With Me.Grid

      .AddRow()
      _idx_row = Me.Grid.NumRows - 1

      If IsTotal Then
        .Cell(_idx_row, GRID_COLUMN_JACKPOT_NAME).Value = _subtotal_title
      Else
        .Cell(_idx_row, GRID_COLUMN_JACKPOT_NAME).Value = _subtotal_title + m_subtotal_id
      End If

      If Not IsTotal Then
        .Cell(_idx_row, GRID_COLUMN_JACKPOT_AWARDED_AMOUNT).Value = GUI_FormatCurrency(m_subtotal_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        .Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
      Else
        .Cell(_idx_row, GRID_COLUMN_JACKPOT_AWARDED_AMOUNT).Value = GUI_FormatCurrency(m_total_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        .Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
      End If

    End With

    Return True
  End Function ' DrawRow

  ''' <summary>
  ''' Output Final Subtotal and Total
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_AfterLastRow()

    ' Draw Subtotals
    Call DrawRow(False)

    ' Draw totals
    Call DrawRow(True)

  End Sub ' GUI_AfterLastRow

#End Region ' Private Functions

#Region " Public Functions "

  ''' <summary>
  ''' Open form for edit
  ''' </summary>
  ''' <param name="MdiParent"></param>
  ''' <remarks></remarks>
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Events "

  Private Sub chk_terminal_location_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminal_location.CheckedChanged

    If chk_terminal_location.Checked Then
      m_terminal_report_type = ReportType.Location
    Else
      m_terminal_report_type = ReportType.None
    End If

    m_refresh_grid = True
  End Sub

#End Region ' Events

End Class