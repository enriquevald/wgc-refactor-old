﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_jackpots_meters_history
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing AndAlso components IsNot Nothing Then
      components.Dispose()
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector()
    Me.gb_grouped = New System.Windows.Forms.GroupBox()
    Me.rb_per_hour = New System.Windows.Forms.RadioButton()
    Me.rb_monthly = New System.Windows.Forms.RadioButton()
    Me.rb_weekly = New System.Windows.Forms.RadioButton()
    Me.rb_daily = New System.Windows.Forms.RadioButton()
    Me.uc_jackpot_checked = New GUI_Controls.uc_checked_several_all_list()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_grouped.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.uc_jackpot_checked)
    Me.panel_filter.Controls.Add(Me.uc_dsl)
    Me.panel_filter.Controls.Add(Me.gb_grouped)
    Me.panel_filter.Size = New System.Drawing.Size(993, 197)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_grouped, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dsl, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_jackpot_checked, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 201)
    Me.panel_data.Size = New System.Drawing.Size(993, 524)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(987, 21)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(987, 5)
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.ClosingTimeEnabled = True
    Me.uc_dsl.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.Location = New System.Drawing.Point(105, 2)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.ShowBorder = True
    Me.uc_dsl.Size = New System.Drawing.Size(259, 84)
    Me.uc_dsl.TabIndex = 1
    Me.uc_dsl.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    '
    'gb_grouped
    '
    Me.gb_grouped.Controls.Add(Me.rb_per_hour)
    Me.gb_grouped.Controls.Add(Me.rb_monthly)
    Me.gb_grouped.Controls.Add(Me.rb_weekly)
    Me.gb_grouped.Controls.Add(Me.rb_daily)
    Me.gb_grouped.Location = New System.Drawing.Point(6, 2)
    Me.gb_grouped.Name = "gb_grouped"
    Me.gb_grouped.Size = New System.Drawing.Size(91, 141)
    Me.gb_grouped.TabIndex = 0
    Me.gb_grouped.TabStop = False
    Me.gb_grouped.Text = "xGrouped"
    '
    'rb_per_hour
    '
    Me.rb_per_hour.Location = New System.Drawing.Point(8, 20)
    Me.rb_per_hour.Name = "rb_per_hour"
    Me.rb_per_hour.Size = New System.Drawing.Size(75, 24)
    Me.rb_per_hour.TabIndex = 0
    Me.rb_per_hour.Text = "Per hour"
    '
    'rb_monthly
    '
    Me.rb_monthly.Location = New System.Drawing.Point(8, 110)
    Me.rb_monthly.Name = "rb_monthly"
    Me.rb_monthly.Size = New System.Drawing.Size(75, 24)
    Me.rb_monthly.TabIndex = 3
    Me.rb_monthly.Text = "Monthly"
    '
    'rb_weekly
    '
    Me.rb_weekly.Location = New System.Drawing.Point(8, 80)
    Me.rb_weekly.Name = "rb_weekly"
    Me.rb_weekly.Size = New System.Drawing.Size(80, 24)
    Me.rb_weekly.TabIndex = 2
    Me.rb_weekly.Text = "Weekly"
    '
    'rb_daily
    '
    Me.rb_daily.Location = New System.Drawing.Point(8, 50)
    Me.rb_daily.Name = "rb_daily"
    Me.rb_daily.Size = New System.Drawing.Size(80, 24)
    Me.rb_daily.TabIndex = 1
    Me.rb_daily.Text = "Daily"
    '
    'uc_jackpot_checked
    '
    Me.uc_jackpot_checked.GroupBoxText = "xTitle"
    Me.uc_jackpot_checked.Location = New System.Drawing.Point(370, 3)
    Me.uc_jackpot_checked.Name = "uc_jackpot_checked"
    Me.uc_jackpot_checked.Size = New System.Drawing.Size(359, 178)
    Me.uc_jackpot_checked.TabIndex = 2
    '
    'frm_jackpots_meters_history
    '
    Me.ClientSize = New System.Drawing.Size(1001, 729)
    Me.Name = "frm_jackpots_meters_history"
    Me.Text = "frm_jackpots_meters_history"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_grouped.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector
  Friend WithEvents gb_grouped As System.Windows.Forms.GroupBox
  Friend WithEvents rb_monthly As System.Windows.Forms.RadioButton
  Friend WithEvents rb_weekly As System.Windows.Forms.RadioButton
  Friend WithEvents rb_daily As System.Windows.Forms.RadioButton
  Friend WithEvents rb_per_hour As System.Windows.Forms.RadioButton
  Friend WithEvents uc_jackpot_checked As GUI_Controls.uc_checked_several_all_list

End Class
