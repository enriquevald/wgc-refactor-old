﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' UC NAME:       frm_jackpot_add_amount
' DESCRIPTION:   Form to add an amount value to ajackpot
' AUTHOR:        Ferran Ortner
' CREATION DATE: 20-ABR-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 20-ABR-2017  FOS    Initial version
'--------------------------------------------------------------------
Imports GUI_Controls
Imports WSI.Common.Jackpot
Imports GUI_CommonOperations
Imports GUI_CommonMisc.mdl_NLS
Imports System.Text
Imports WSI.Common

Public Class frm_jackpot_add_amount
  Inherits frm_base_edit

  Dim m_jackpot As Jackpot
  Dim m_jackpot_type_list As New Dictionary(Of Int32, String)
  Dim m_selected_jackpot_amount As Decimal
  Dim m_selected_jackpot_maximum_amount As Decimal
  Dim m_current_jackpot_type As Jackpot.JackpotType

#Region "Public"

  ''' <summary>
  ''' Jackpot add amount constructor
  ''' </summary>
  ''' <param name="Jackpot"></param>
  ''' <remarks></remarks>
  Public Sub New(ByRef Jackpot As Jackpot)

    Me.FormId = ENUM_FORM.FORM_JACKPOT_ADD_AMOUNT

    Call MyBase.GUI_SetFormId()

    m_jackpot = Jackpot

    ' Add any initialization after the InitializeComponent() call.
    Call Me.GUI_Init()

    ' This call is required by the designer.
    InitializeComponent()
    Me.ef_amount.Focus()

  End Sub 'New

#End Region

#Region "Protected"
  ''' <summary>
  ''' GUI_InitControls
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_InitControls()
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8208)
    Me.btn_add_amount.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1684)
    Me.ef_amount.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3372)
    Me.ef_amount.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)
    'Me.lbl_accumulated.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8176) + ": " + GUI_FormatCurrency(m_jackpot.Accumulated)
    Me.lbl_jackpot_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8305)
    '' TODO HULK
    ''Me.lbl_lastUpdate.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8208) + " " + WGDB.Now().ToString("HH:mm") + "h"
    Me.HidePanelButtons = True

    Me.BuildJackpotTypeDictionary()

    Me.cb_jackpot_type.DataSource = New BindingSource(Me.m_jackpot_type_list, Nothing)
    Me.cb_jackpot_type.DisplayMember = "Value"
    Me.cb_jackpot_type.ValueMember = "key"

  End Sub 'GUI_InitControls

  ''' <summary>
  ''' GUI_Permissions
  ''' </summary>
  ''' <param name="AndPerm"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)
  End Sub ' GUI_Permissions

  ''' <summary>
  ''' GUI_FirstActivation
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_FirstActivation()
  End Sub ' GUI_FirstActivation

  ''' <summary>
  ''' GUI_ButtonClick
  ''' </summary>
  ''' <param name="ButtonId"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)
    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_OK

        ' Save without close form
        'Call AddAmountToJackpot()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

#End Region

#Region "Private"

  ''' <summary>
  ''' Add amount to JACKPOTS_AMOUNT_OPERATIONS
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub AddAmountToJackpot()

    Dim _amount_ops As New JackpotAmountOperations()

    ' No Amount Specified
    If String.IsNullOrEmpty(Me.ef_amount.Value) OrElse Convert.ToDecimal(Me.ef_amount.Value) <= 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8177), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK)
      Return
    End If

    ' Add the Amount
    _amount_ops.Id = -1
    _amount_ops.JackpotId = Me.m_jackpot.Id
    _amount_ops.JackpotType = Me.m_current_jackpot_type
    _amount_ops.Type = JackpotAmountOperations.OperationType.Add
    _amount_ops.Status = JackpotAmountOperations.OperationStatus.Pending
    _amount_ops.Amount = Convert.ToDecimal(ef_amount.Value)
    _amount_ops.UserName = GLB_CurrentUser.Name

    If _amount_ops.Save() Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8178), ENUM_MB_TYPE.MB_TYPE_INFO, ENUM_MB_BTN.MB_BTN_OK)

      Me.Close()
    Else
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8179), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)
    End If

  End Sub ' AddAmountToJackpot

  ''' <summary>
  ''' BuildJackpotTypeDictionary
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub BuildJackpotTypeDictionary()

    Dim _jackpot_type As Jackpot.JackpotType

    For Each _jackpot_type In [Enum].GetValues(GetType(Jackpot.JackpotType))

      Select Case _jackpot_type
        Case Jackpot.JackpotType.Main
          If Me.m_jackpot.MainPct > 0 Then
            Me.m_jackpot_type_list.Add(_jackpot_type, _jackpot_type.ToString())
          End If
        Case Jackpot.JackpotType.HappyHour
          If Me.m_jackpot.HappyHourPct > 0 Then
            Me.m_jackpot_type_list.Add(_jackpot_type, _jackpot_type.ToString())
          End If
        Case Jackpot.JackpotType.PrizeSharing
          If Me.m_jackpot.PrizeSharingPct > 0 Then
            Me.m_jackpot_type_list.Add(_jackpot_type, _jackpot_type.ToString())
          End If
      End Select

    Next _jackpot_type

  End Sub ' BuildJackpotTypeDictionary

#End Region

#Region "Events"

  ''' <summary>
  ''' btn_add_amount_Click
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub btn_add_amount_Click(sender As Object, e As EventArgs) Handles btn_add_amount.Click
    Me.AddAmountToJackpot()
  End Sub ' btn_add_amount_Click

  ''' <summary>
  ''' frm_jackpot_add_amount_KeyPress
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub frm_jackpot_add_amount_KeyPress(sender As Object, e As KeyPressEventArgs) Handles MyBase.KeyPress

    Select Case e.KeyChar
      Case Chr(Keys.Enter)
        Call AddAmountToJackpot()
      Case Chr(Keys.Escape)
        Me.Close()
      Case Else
    End Select

  End Sub ' frm_jackpot_add_amount_KeyPress

  ''' <summary>
  ''' cb_jackpot_type_SelectedIndexChanged
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub cb_jackpot_type_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cb_jackpot_type.SelectedIndexChanged
    Dim _idx As KeyValuePair(Of Int32, String)

    _idx = cb_jackpot_type.SelectedItem

    Select Case _idx.Key
      Case Jackpot.JackpotType.Main
        Me.m_selected_jackpot_amount = Me.m_jackpot.ContributionMeters.MainAmount
        Me.m_selected_jackpot_maximum_amount = Me.m_jackpot.Maximum
        Me.m_current_jackpot_type = Jackpot.JackpotType.Main
      Case Jackpot.JackpotType.PrizeSharing
        Me.m_selected_jackpot_amount = Me.m_jackpot.ContributionMeters.SharedAmount
        Me.m_selected_jackpot_maximum_amount = Me.m_jackpot.AwardPrizeConfig.PrizeSharing.Maximum
        Me.m_current_jackpot_type = Jackpot.JackpotType.PrizeSharing
      Case Jackpot.JackpotType.HappyHour
        Me.m_selected_jackpot_amount = Me.m_jackpot.ContributionMeters.HappyHourAmount
        Me.m_selected_jackpot_maximum_amount = Me.m_jackpot.AwardPrizeConfig.HappyHour.Maximum
        Me.m_current_jackpot_type = Jackpot.JackpotType.HappyHour
    End Select

    Me.lbl_accumulated.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8176) + ": " + GUI_FormatCurrency(Me.m_selected_jackpot_amount)

    '' TODO Hulk
    Me.lbl_lastUpdate.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8357) + ": " + GUI_FormatCurrency(Me.m_selected_jackpot_maximum_amount)


  End Sub ' cb_jackpot_type_SelectedIndexChanged

#End Region


End Class