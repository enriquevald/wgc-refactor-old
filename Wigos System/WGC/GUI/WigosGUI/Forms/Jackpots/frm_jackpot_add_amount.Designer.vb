﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_jackpot_add_amount
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.btn_add_amount = New System.Windows.Forms.Button()
    Me.lbl_accumulated = New System.Windows.Forms.Label()
    Me.ef_amount = New GUI_Controls.uc_entry_field()
    Me.lbl_lastUpdate = New System.Windows.Forms.Label()
    Me.cb_jackpot_type = New System.Windows.Forms.ComboBox()
    Me.lbl_jackpot_type = New System.Windows.Forms.Label()
    Me.panel_data.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.lbl_jackpot_type)
    Me.panel_data.Controls.Add(Me.cb_jackpot_type)
    Me.panel_data.Controls.Add(Me.lbl_lastUpdate)
    Me.panel_data.Controls.Add(Me.btn_add_amount)
    Me.panel_data.Controls.Add(Me.ef_amount)
    Me.panel_data.Controls.Add(Me.lbl_accumulated)
    Me.panel_data.Dock = System.Windows.Forms.DockStyle.Fill
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(286, 153)
    '
    'btn_add_amount
    '
    Me.btn_add_amount.Location = New System.Drawing.Point(217, 118)
    Me.btn_add_amount.Name = "btn_add_amount"
    Me.btn_add_amount.Size = New System.Drawing.Size(87, 27)
    Me.btn_add_amount.TabIndex = 1
    Me.btn_add_amount.Text = "Add"
    Me.btn_add_amount.UseVisualStyleBackColor = True
    '
    'lbl_accumulated
    '
    Me.lbl_accumulated.AutoSize = True
    Me.lbl_accumulated.ForeColor = System.Drawing.Color.Navy
    Me.lbl_accumulated.Location = New System.Drawing.Point(3, 10)
    Me.lbl_accumulated.Name = "lbl_accumulated"
    Me.lbl_accumulated.Size = New System.Drawing.Size(87, 13)
    Me.lbl_accumulated.TabIndex = 4
    Me.lbl_accumulated.Text = "xAccumulated"
    '
    'ef_amount
    '
    Me.ef_amount.DoubleValue = 0.0R
    Me.ef_amount.IntegerValue = 0
    Me.ef_amount.IsReadOnly = False
    Me.ef_amount.Location = New System.Drawing.Point(20, 83)
    Me.ef_amount.Name = "ef_amount"
    Me.ef_amount.PlaceHolder = Nothing
    Me.ef_amount.Size = New System.Drawing.Size(229, 24)
    Me.ef_amount.SufixText = "Sufix Text"
    Me.ef_amount.SufixTextVisible = True
    Me.ef_amount.TabIndex = 0
    Me.ef_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_amount.TextValue = ""
    Me.ef_amount.Value = ""
    Me.ef_amount.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_lastUpdate
    '
    Me.lbl_lastUpdate.AutoSize = True
    Me.lbl_lastUpdate.ForeColor = System.Drawing.Color.Navy
    Me.lbl_lastUpdate.Location = New System.Drawing.Point(7, 125)
    Me.lbl_lastUpdate.Name = "lbl_lastUpdate"
    Me.lbl_lastUpdate.Size = New System.Drawing.Size(70, 13)
    Me.lbl_lastUpdate.TabIndex = 5
    Me.lbl_lastUpdate.Text = "TODO Hulk"
    '
    'cb_jackpot_type
    '
    Me.cb_jackpot_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.cb_jackpot_type.FormattingEnabled = True
    Me.cb_jackpot_type.Location = New System.Drawing.Point(103, 41)
    Me.cb_jackpot_type.Name = "cb_jackpot_type"
    Me.cb_jackpot_type.Size = New System.Drawing.Size(145, 21)
    Me.cb_jackpot_type.TabIndex = 6
    '
    'lbl_jackpot_type
    '
    Me.lbl_jackpot_type.AutoSize = True
    Me.lbl_jackpot_type.Location = New System.Drawing.Point(3, 44)
    Me.lbl_jackpot_type.Name = "lbl_jackpot_type"
    Me.lbl_jackpot_type.Size = New System.Drawing.Size(44, 13)
    Me.lbl_jackpot_type.TabIndex = 7
    Me.lbl_jackpot_type.Text = "Label1"
    '
    'frm_jackpot_add_amount
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(384, 161)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
    Me.MaximumSize = New System.Drawing.Size(400, 200)
    Me.MinimumSize = New System.Drawing.Size(329, 152)
    Me.Name = "frm_jackpot_add_amount"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_jackpot_add_amount"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents btn_add_amount As System.Windows.Forms.Button
  Friend WithEvents lbl_accumulated As System.Windows.Forms.Label
  Friend WithEvents ef_amount As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_lastUpdate As System.Windows.Forms.Label
  Friend WithEvents lbl_jackpot_type As System.Windows.Forms.Label
  Friend WithEvents cb_jackpot_type As System.Windows.Forms.ComboBox
End Class
