﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_jackpots_history
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox()
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector()
    Me.Uc_jackpot_type_sel = New GUI_Controls.uc_jackpot_type_sel()
    Me.uc_pr_list = New GUI_Controls.uc_provider()
    Me.opt_order_date = New System.Windows.Forms.RadioButton()
    Me.opt_order_Jackpot = New System.Windows.Forms.RadioButton()
    Me.gb_order = New System.Windows.Forms.GroupBox()
    Me.uc_jackpot_award_status_sel = New GUI_Controls.uc_jackpot_award_status_sel()
    Me.uc_jackpot_checked = New GUI_Controls.uc_checked_several_all_list()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_order.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.uc_jackpot_checked)
    Me.panel_filter.Controls.Add(Me.uc_jackpot_award_status_sel)
    Me.panel_filter.Controls.Add(Me.gb_order)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Controls.Add(Me.Uc_jackpot_type_sel)
    Me.panel_filter.Controls.Add(Me.uc_dsl)
    Me.panel_filter.Controls.Add(Me.chk_terminal_location)
    Me.panel_filter.Size = New System.Drawing.Size(1259, 258)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_terminal_location, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dsl, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.Uc_jackpot_type_sel, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_order, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_jackpot_award_status_sel, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_jackpot_checked, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 262)
    Me.panel_data.Size = New System.Drawing.Size(1259, 426)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1253, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1253, 4)
    '
    'chk_terminal_location
    '
    Me.chk_terminal_location.Location = New System.Drawing.Point(272, 212)
    Me.chk_terminal_location.Name = "chk_terminal_location"
    Me.chk_terminal_location.Size = New System.Drawing.Size(199, 19)
    Me.chk_terminal_location.TabIndex = 4
    Me.chk_terminal_location.Text = "xShow terminals location"
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.ClosingTimeEnabled = True
    Me.uc_dsl.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.Location = New System.Drawing.Point(6, 6)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.ShowBorder = True
    Me.uc_dsl.Size = New System.Drawing.Size(257, 84)
    Me.uc_dsl.TabIndex = 0
    Me.uc_dsl.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    '
    'Uc_jackpot_type_sel
    '
    Me.Uc_jackpot_type_sel.Location = New System.Drawing.Point(269, 2)
    Me.Uc_jackpot_type_sel.Name = "Uc_jackpot_type_sel"
    Me.Uc_jackpot_type_sel.SelectedTypes = Nothing
    Me.Uc_jackpot_type_sel.Size = New System.Drawing.Size(167, 129)
    Me.Uc_jackpot_type_sel.TabIndex = 2
    '
    'uc_pr_list
    '
    Me.uc_pr_list.Location = New System.Drawing.Point(777, 1)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(325, 179)
    Me.uc_pr_list.TabIndex = 6
    Me.uc_pr_list.TerminalListHasValues = False
    '
    'opt_order_date
    '
    Me.opt_order_date.AutoSize = True
    Me.opt_order_date.Location = New System.Drawing.Point(20, 20)
    Me.opt_order_date.Name = "opt_order_date"
    Me.opt_order_date.Size = New System.Drawing.Size(74, 17)
    Me.opt_order_date.TabIndex = 0
    Me.opt_order_date.TabStop = True
    Me.opt_order_date.Text = "xByDate"
    Me.opt_order_date.UseVisualStyleBackColor = True
    '
    'opt_order_Jackpot
    '
    Me.opt_order_Jackpot.AutoSize = True
    Me.opt_order_Jackpot.Location = New System.Drawing.Point(20, 43)
    Me.opt_order_Jackpot.Name = "opt_order_Jackpot"
    Me.opt_order_Jackpot.Size = New System.Drawing.Size(90, 17)
    Me.opt_order_Jackpot.TabIndex = 1
    Me.opt_order_Jackpot.TabStop = True
    Me.opt_order_Jackpot.Text = "xByJackpot"
    Me.opt_order_Jackpot.UseVisualStyleBackColor = True
    '
    'gb_order
    '
    Me.gb_order.Controls.Add(Me.opt_order_date)
    Me.gb_order.Controls.Add(Me.opt_order_Jackpot)
    Me.gb_order.Location = New System.Drawing.Point(272, 137)
    Me.gb_order.Name = "gb_order"
    Me.gb_order.Size = New System.Drawing.Size(164, 69)
    Me.gb_order.TabIndex = 3
    Me.gb_order.TabStop = False
    Me.gb_order.Text = "xOrder"
    '
    'uc_jackpot_award_status_sel
    '
    Me.uc_jackpot_award_status_sel.Location = New System.Drawing.Point(6, 97)
    Me.uc_jackpot_award_status_sel.Name = "uc_jackpot_award_status_sel"
    Me.uc_jackpot_award_status_sel.Size = New System.Drawing.Size(257, 155)
    Me.uc_jackpot_award_status_sel.TabIndex = 1
    '
    'uc_jackpot_checked
    '
    Me.uc_jackpot_checked.GroupBoxText = "xTitle"
    Me.uc_jackpot_checked.Location = New System.Drawing.Point(442, 2)
    Me.uc_jackpot_checked.Name = "uc_jackpot_checked"
    Me.uc_jackpot_checked.Size = New System.Drawing.Size(329, 178)
    Me.uc_jackpot_checked.TabIndex = 5
    '
    'frm_jackpots_history
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1267, 692)
    Me.Name = "frm_jackpots_history"
    Me.Text = "frm_jackpots_history"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_order.ResumeLayout(False)
    Me.gb_order.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector
  Friend WithEvents Uc_jackpot_type_sel As GUI_Controls.uc_jackpot_type_sel
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents gb_order As System.Windows.Forms.GroupBox
  Friend WithEvents opt_order_date As System.Windows.Forms.RadioButton
  Friend WithEvents opt_order_Jackpot As System.Windows.Forms.RadioButton
  Friend WithEvents uc_jackpot_award_status_sel As GUI_Controls.uc_jackpot_award_status_sel
  Friend WithEvents uc_jackpot_checked As GUI_Controls.uc_checked_several_all_list
End Class
