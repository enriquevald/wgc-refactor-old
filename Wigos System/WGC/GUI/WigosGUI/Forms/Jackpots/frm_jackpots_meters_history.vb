﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_jackpots_meters_history.vb
' DESCRIPTION:   List Jackpot Meters History 
' AUTHOR:        Javier Barea
' CREATION DATE: 02-MAY-2017
'
' REVISION HISTORY:
'
' Date         Author  Description
' -----------  ------  -----------------------------------------------
' 02-MAY-2017   JBP     Initial version
' 22-JUN-2017   AMF     Changes after fill the table
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Text
Imports WSI.Common.Jackpot

Public Class frm_jackpots_meters_history
  Inherits frm_base_sel

#Region " Constants "

  ' Grid setup
  Private Const GRID_HEADER_ROWS As Integer = 2
  Private Const GRID_COLUMNS As Integer = 7

  ' DB Columns
  Private Const SQL_COLUMN_DATE As Integer = 0
  Private Const SQL_COLUMN_JACKPOT_NAME As Integer = 1
  Private Const SQL_COLUMN_JACKPOT_MINIMUM As Integer = 2
  Private Const SQL_COLUMN_JACKPOT_MAXIMUM As Integer = 3
  Private Const SQL_COLUMN_JACKPOT_AVERAGE As Integer = 4
  Private Const SQL_COLUMN_JACKPOT_ISO_CODE As Integer = 5

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_DATE As Integer = 1
  Private Const GRID_COLUMN_JACKPOT_NAME As Integer = 2
  Private Const GRID_COLUMN_JACKPOT_MINIMUM As Integer = 3
  Private Const GRID_COLUMN_JACKPOT_MAXIMUM As Integer = 4
  Private Const GRID_COLUMN_JACKPOT_AVERAGE As Integer = 5
  Private Const GRID_COLUMN_JACKPOT_ISO_CODE As Integer = 6

  ' Width
  Private Const GRID_WIDTH_INDEX As Integer = 250
  Private Const GRID_WIDTH_DATE As Integer = 2200
  Private Const GRID_WIDTH_JACKPOT_NAME As Integer = 3012
  Private Const GRID_WIDTH_JACKPOT_MINIMUM As Integer = 1900
  Private Const GRID_WIDTH_JACKPOT_MAXIMUM As Integer = 1900
  Private Const GRID_WIDTH_JACKPOT_AVERAGE As Integer = 1900
  Private Const GRID_WIDTH_JACKPOT_ISO_CODE As Integer = 1000

#End Region ' Constants

#Region " Members "

  ' For report filters 
  Private m_grouped As String
  Private m_date_from As String
  Private m_date_to As String
  Private m_jackpots As String

#End Region ' Members

#Region " Overrides "

  ''' <summary>
  ''' Set Form Id
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_JACKPOT_METERS_HISTORY_SEL

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ''' <summary>
  '''  Initialize every form control
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_InitControls()
    Dim _dt_jackpots As DataTable = Nothing

    Call MyBase.GUI_InitControls()

    ' - Form title
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8235)

    '' - Buttons
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False

    ' Date time filter
    Me.uc_dsl.Init(GLB_NLS_GUI_STATISTICS.Id(202))

    ' Grouped date
    Me.gb_grouped.Text = GLB_NLS_GUI_STATISTICS.GetString(419)
    Me.rb_daily.Text = GLB_NLS_GUI_STATISTICS.GetString(420)
    Me.rb_weekly.Text = GLB_NLS_GUI_STATISTICS.GetString(421)
    Me.rb_monthly.Text = GLB_NLS_GUI_STATISTICS.GetString(422)
    Me.rb_per_hour.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8364)

    ' Progressives Check List
    Me.uc_jackpot_checked.GroupBoxText = GLB_NLS_GUI_JACKPOT_MGR.GetString(546)  '"Jackpots" 
    Me.uc_jackpot_checked.ColumnWidth(2, 3050)

    If New Jackpot().GetAllJackpots(_dt_jackpots) Then
      Me.uc_jackpot_checked.Add(_dt_jackpots)
    End If


    ' Style Sheet
    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ''' <summary>
  ''' Set Focus in a control
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.rb_daily
  End Sub 'GUI_SetInitialFocus

  ''' <summary>
  ''' Sets the values of a row
  ''' </summary>
  ''' <param name="RowIndex"></param>
  ''' <param name="DbRow"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    With Me.Grid

      'Date
      If Not DbRow.IsNull(SQL_COLUMN_DATE) Then
        .Cell(RowIndex, GRID_COLUMN_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATE), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      End If

      'Jackpot Name
      If Not DbRow.IsNull(SQL_COLUMN_JACKPOT_NAME) Then
        .Cell(RowIndex, GRID_COLUMN_JACKPOT_NAME).Value = DbRow.Value(SQL_COLUMN_JACKPOT_NAME)
      End If

      'Jackpot Minimum
      If Not DbRow.IsNull(GRID_COLUMN_JACKPOT_MINIMUM) Then
        .Cell(RowIndex, GRID_COLUMN_JACKPOT_MINIMUM).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_JACKPOT_MINIMUM), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End If

      'Jackpot Maximum
      If Not DbRow.IsNull(SQL_COLUMN_JACKPOT_MAXIMUM) Then
        .Cell(RowIndex, GRID_COLUMN_JACKPOT_MAXIMUM).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_JACKPOT_MAXIMUM), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End If

      'Jackpot Average
      If Not DbRow.IsNull(SQL_COLUMN_JACKPOT_AVERAGE) Then
        .Cell(RowIndex, GRID_COLUMN_JACKPOT_AVERAGE).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_JACKPOT_AVERAGE), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End If

      'Jackpot IsoCode
      If Not DbRow.IsNull(SQL_COLUMN_JACKPOT_ISO_CODE) Then
        .Cell(RowIndex, GRID_COLUMN_JACKPOT_ISO_CODE).Value = DbRow.Value(SQL_COLUMN_JACKPOT_ISO_CODE)
      End If

    End With

    Return True

  End Function ' GUI_SetupRow

  ''' <summary>
  ''' Build an SQL query from conditions set in the filters
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _report_type As JackpotMeters.ReportType
    Dim _filter_jackpots As String

    If Me.rb_daily.Checked Then
      _report_type = JackpotMeters.ReportType.Daily
    ElseIf Me.rb_weekly.Checked Then
      _report_type = JackpotMeters.ReportType.Weekly
    ElseIf Me.rb_monthly.Checked Then
      _report_type = JackpotMeters.ReportType.Monthly
    ElseIf Me.rb_per_hour.Checked Then
      _report_type = JackpotMeters.ReportType.Hour
    End If

    If Not Me.uc_jackpot_checked.IsAllSelected Then
      _filter_jackpots = Me.uc_jackpot_checked.SelectedIndexesList
    Else
      _filter_jackpots = String.Empty
    End If

    Return JackpotMeters.GetMeters(_report_type, Me.uc_dsl.ClosingTime, Me.uc_dsl.GetSqlFilterCondition("JM_DATE_TIME"), _filter_jackpots)

  End Function ' GUI_FilterGetSqlQuery

  ''' <summary>
  ''' Set texts corresponding to the provided filter values for the report
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_grouped = String.Empty
    m_date_from = String.Empty
    m_date_to = String.Empty
    m_jackpots = String.Empty

    ' Grouped
    If Me.rb_daily.Checked = True Then
      m_grouped = Me.rb_daily.Text
    ElseIf Me.rb_weekly.Checked = True Then
      m_grouped = Me.rb_weekly.Text
    ElseIf Me.rb_monthly.Checked = True Then
      m_grouped = Me.rb_monthly.Text
    ElseIf Me.rb_per_hour.Checked = True Then
      m_grouped = Me.rb_per_hour.Text
    End If

    ' Dates
    If Me.uc_dsl.FromDateSelected Then
      m_date_from = GUI_FormatDate(Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    If Me.uc_dsl.ToDateSelected Then
      m_date_to = GUI_FormatDate(Me.uc_dsl.ToDate.AddHours(Me.uc_dsl.ClosingTime), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    ' Jackpots
    m_jackpots = IIf(uc_jackpot_checked.IsAllSelected, GLB_NLS_GUI_AUDITOR.GetString(263), uc_jackpot_checked.SelectedValuesList())

  End Sub ' GUI_ReportUpdateFilters

  ''' <summary>
  ''' Set proper values for form filters being sent to the report
  ''' </summary>
  ''' <param name="PrintData"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(419), m_grouped)
    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(261) & " " & GLB_NLS_GUI_AUDITOR.GetString(257), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(261) & " " & GLB_NLS_GUI_AUDITOR.GetString(258), m_date_to)
    PrintData.SetFilter(GLB_NLS_GUI_JACKPOT_MGR.GetString(546), m_jackpots)

  End Sub ' GUI_ReportFilter

  ''' <summary>
  ''' Set default values to filters
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()

  End Sub ' GUI_FilterReset

  ''' <summary>
  ''' Previous check in filters
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate > Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    End If

    Return True

  End Function 'GUI_FilterCheck

#End Region ' Overrides

#Region " Private Functions "

  ''' <summary>
  ''' Set Style to the Grid
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GUI_StyleSheet()

    ' Grid StyleSheet
    With Me.Grid

      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, False)

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_INDEX).Header(1).Text = String.Empty
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Date      
      .Column(GRID_COLUMN_DATE).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1597) ' Fecha
      .Column(GRID_COLUMN_DATE).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Jackpot Name
      .Column(GRID_COLUMN_JACKPOT_NAME).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_JACKPOT_NAME).Header(1).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(568) ' Jackpot Name
      .Column(GRID_COLUMN_JACKPOT_NAME).Width = GRID_WIDTH_JACKPOT_NAME
      .Column(GRID_COLUMN_JACKPOT_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Jackpot Minimum
      .Column(GRID_COLUMN_JACKPOT_MINIMUM).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6975) 'Acumulado
      .Column(GRID_COLUMN_JACKPOT_MINIMUM).Header(1).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(569) 'Mínimo 
      .Column(GRID_COLUMN_JACKPOT_MINIMUM).Width = GRID_WIDTH_JACKPOT_MINIMUM
      .Column(GRID_COLUMN_JACKPOT_MINIMUM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Jackpot Maximum
      .Column(GRID_COLUMN_JACKPOT_MAXIMUM).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6975) 'Acumulado
      .Column(GRID_COLUMN_JACKPOT_MAXIMUM).Header(1).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(570) 'Máximo
      .Column(GRID_COLUMN_JACKPOT_MAXIMUM).Width = GRID_WIDTH_JACKPOT_MAXIMUM
      .Column(GRID_COLUMN_JACKPOT_MAXIMUM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Jackpot Average
      .Column(GRID_COLUMN_JACKPOT_AVERAGE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6975) 'Acumulado
      .Column(GRID_COLUMN_JACKPOT_AVERAGE).Header(1).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(571) 'Media 
      .Column(GRID_COLUMN_JACKPOT_AVERAGE).Width = GRID_WIDTH_JACKPOT_AVERAGE
      .Column(GRID_COLUMN_JACKPOT_AVERAGE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Jackpot Iso Code
      .Column(GRID_COLUMN_JACKPOT_ISO_CODE).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_JACKPOT_ISO_CODE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8085) 'Currency 
      .Column(GRID_COLUMN_JACKPOT_ISO_CODE).Width = GRID_WIDTH_JACKPOT_ISO_CODE
      .Column(GRID_COLUMN_JACKPOT_ISO_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    End With

  End Sub ' GUI_StyleSheet

  ''' <summary>
  ''' Set Default Values
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SetDefaultValues()

    Dim _closing_time As Integer
    Dim _final_time As Date

    ' Type
    Me.rb_daily.Checked = True

    ' Dates
    _closing_time = GetDefaultClosingTime()
    _final_time = New DateTime(Now.Year, Now.Month, Now.Day, _closing_time, 0, 0)

    If _final_time > Now() Then
      _final_time = _final_time.AddDays(-1)
    End If

    _final_time = _final_time.Date

    Me.uc_dsl.FromDate = _final_time
    Me.uc_dsl.FromDateSelected = True

    Me.uc_dsl.ToDate = _final_time.AddDays(1)
    Me.uc_dsl.ToDateSelected = True

    Me.uc_dsl.ClosingTime = _closing_time

    'Filter Jackpots
    Me.uc_jackpot_checked.SetDefaultValues()

  End Sub ' SetDefaultValues

  ''' <summary>
  ''' Change status of closing hour 
  ''' </summary>
  ''' <param name="checked"></param>
  ''' <remarks></remarks>
  Private Sub EnableClosingHour(ByVal checked As Boolean)

    Me.uc_dsl.cmb_closing_hour.Enabled = checked
    Me.uc_dsl.lbl_from_hour.Visible = checked
    Me.uc_dsl.lbl_to_hour.Visible = checked

  End Sub ' EnableClosingHour

#End Region ' Private Functions

#Region " Public Functions "

  ''' <summary>
  ''' Open form for edit
  ''' </summary>
  ''' <param name="MdiParent"></param>
  ''' <remarks></remarks>
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION
    Me.MdiParent = MdiParent
    Me.Display(False)


  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Events "

  ''' <summary>
  ''' Radio button monthly event
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub rb_monthly_CheckedChanged(sender As Object, e As EventArgs) Handles rb_monthly.CheckedChanged
    EnableClosingHour(Not rb_monthly.Checked)
  End Sub ' rb_monthly_CheckedChanged

  ''' <summary>
  ''' Radio button per hour event
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub rb_per_hour_CheckedChanged(sender As Object, e As EventArgs) Handles rb_per_hour.CheckedChanged
    EnableClosingHour(rb_per_hour.Checked)
  End Sub ' rb_per_hour_CheckedChanged

  ''' <summary>
  ''' Radio button weekly event
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub rb_weekly_CheckedChanged(sender As Object, e As EventArgs) Handles rb_weekly.CheckedChanged
    EnableClosingHour(Not rb_weekly.Checked)
  End Sub ' rb_weekly_CheckedChanged

  ''' <summary>
  ''' Radio button daily event
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub rb_daily_CheckedChanged(sender As Object, e As EventArgs) Handles rb_daily.CheckedChanged
    EnableClosingHour(Not rb_daily.Checked)
  End Sub ' rb_daily_CheckedChanged

#End Region ' Events

End Class
