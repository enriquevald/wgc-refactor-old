﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_jackpots_groups
' DESCRIPTION:   Jackpot New Treview form
' AUTHOR:        Rubén Lama Ordóñez
' CREATION DATE: 30-MAR-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 30-MAR-2017  RLO    Initial version
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonMisc
Imports System.Text
Imports WSI.Common
Imports WSI.Common.Jackpot

Public Class frm_jackpots_treeview_config
  Inherits frm_treeview_edit

#Region "Constants"

#End Region ' Constants

#Region "Members"

  Private m_jackpot As CLASS_JACKPOT
  Private m_new_jackpot_label As String

#End Region ' Members

#Region "Propeties"

  Public Property Jackpot As CLASS_JACKPOT
    Get
      Return Me.m_jackpot
    End Get
    Set(value As CLASS_JACKPOT)
      Me.m_jackpot = value
    End Set
  End Property

  Public Property NewJackpotLabel As String
    Get
      Return Me.m_new_jackpot_label
    End Get
    Set(value As String)
      Me.m_new_jackpot_label = value
    End Set
  End Property

#End Region ' Members

#Region "Overrides"

  ''' <summary>
  ''' Set form ID
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_JACKPOT_TREVIEW_EDIT

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ''' <summary>
  ''' GUI_DB_Operation manager
  ''' </summary>
  ''' <param name="DbOperation"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        Me.DbStatus = ENUM_STATUS.STATUS_OK

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          If Me.DbStatus = ENUM_STATUS.STATUS_NOT_FOUND Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8026), ENUM_MB_TYPE.MB_TYPE_ERROR) ' "Jackpot was not found." 
          Else
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8027), ENUM_MB_TYPE.MB_TYPE_ERROR) ' "An error occurred while reading Jackpot data." 
          End If

        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8028), ENUM_MB_TYPE.MB_TYPE_ERROR) ' "An error occurred while creating the Jackpot." 
        Else
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8201), ENUM_MB_TYPE.MB_TYPE_INFO, ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE

        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          If Me.DbStatus = ENUM_STATUS.STATUS_DEPENDENCIES Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8029), ENUM_MB_TYPE.MB_TYPE_ERROR) ' "he Jackpot cannot be disabled because it has actually in use." 
          ElseIf Me.DbStatus = ENUM_STATUS.STATUS_NOT_FOUND Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8030), ENUM_MB_TYPE.MB_TYPE_ERROR) ' "Jackpot was not found."
          Else
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8031), ENUM_MB_TYPE.MB_TYPE_ERROR) ' "An error occurred while creating the Jackpot."
          End If
        Else
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8201), ENUM_MB_TYPE.MB_TYPE_INFO, ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)
    End Select

  End Sub ' GUI_DB_Operation

  ''' <summary>
  ''' Key press event
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean
    Return False
  End Function ' GUI_KeyPress

  ''' <summary>
  ''' Key up event
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) As Boolean

    If Me.NodeInEdition Is Nothing Then
      Return True
    End If

    Select Case (Me.NodeInEdition.Tag)
      Case EditSelectionItemType.JACKPOT_CONFIG
        KeyUp_JackpotConfig(sender, e)

      Case EditSelectionItemType.JACKPOT_AWARD_CONFIG
        KeyUp_JackpotAwardConfig(sender, e)

      Case EditSelectionItemType.JACKPOT_AWARD_PRIZE_CONFIG
        KeyUp_JackpotAwardPrizeConfig(sender, e)

    End Select

    Return False

  End Function ' GUI_KeyUp

  ''' <summary>
  ''' Check screen data
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    If Me.NodeInEdition Is Nothing Then
      Return True
    End If

    Select Case (Me.NodeInEdition.Tag)

      Case EditSelectionItemType.JACKPOT_CONFIG
        Return IsScreenDataOk_JackpotConfig()

      Case EditSelectionItemType.JACKPOT_AWARD_CONFIG
        Return IsScreenDataOk_JackpotAwardConfig()

      Case EditSelectionItemType.JACKPOT_AWARD_PRIZE_CONFIG
        Return IsScreenDataOk_JackpotAwardPrizeConfig()

      Case EditSelectionItemType.JACKPOT_DASHBOARD
        Return IsScreenDataOk_JackpotDashboard()

    End Select

    Return False

  End Function ' GUI_IsScreenDataOk

  ''' <summary>
  ''' Get screen data from specific control
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_GetScreenData()

    If Me.DbEditedObject Is Nothing Then
      Return
    End If

    Me.Jackpot = Me.DbEditedObject

    If Me.NodeInEdition Is Nothing Then
      Return
    End If

    Select Case (Me.NodeInEdition.Tag)

      Case EditSelectionItemType.JACKPOT_CONFIG
        GetScreenData_JackpotConfig()

      Case EditSelectionItemType.JACKPOT_AWARD_CONFIG
        GetScreenData_JackpotAwardConfig()

      Case EditSelectionItemType.JACKPOT_AWARD_PRIZE_CONFIG
        GetScreenData_JackpotAwardPrizeConfig()

    End Select

    ' Set last modification (Always Update Jackpot Main Class LastUpdate)
    Me.Jackpot.Selected.LastModification = WGDB.Now

  End Sub ' GUI_GetScreenData

  ''' <summary>
  ''' Set screen data base method
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    MyBase.GUI_SetScreenData(SqlCtx)

  End Sub ' GUI_SetScreenData

  ''' <summary>
  ''' Initializes resources for controls
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_InitControls()

    Dim _flags As DataTable
    _flags = New DataTable()

    Me.CloseOnOkClick = False
    Me.DbEditedObject = New CLASS_JACKPOT()
    Me.Jackpot = Me.DbEditedObject

    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8064) ' 7225 "Edición de Jackpot"

    ' Bind TreeView     
    Me.InitializeJackpotTreeView()
    Me.BindTreeNodeToEdit()

    ' Controls:
    Me.GUI_Button(frm_treeview_edit.ENUM_BUTTON.BUTTON_NEW).Visible = False

  End Sub ' GUI_InitControls

  ''' <summary>
  ''' Handle Form buttons cliks
  ''' </summary>
  ''' <param name="ButtonId"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)
    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_OK
        ' Save without close form
        MyBase.CloseOnOkClick = False
        Call MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_OK)
        MyBase.CloseOnOkClick = True

        'Me.GUI_ShowEditItem(Me.Jackpot.Selected.Id)

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick


  ''' <summary>
  ''' Initialize treeview control
  ''' </summary>
  ''' <param name="RootDescription"></param>
  ''' <remarks></remarks>
  Public Overrides Sub InitTreeView(Optional ByVal RootDescription As String = uc_treeview.DEFAULT_TREEVIEW_ROOT_DESCRIPTION)

    Me.TreeView.ItemList = Me.Jackpot.List.ToTreeViewItemList()

    TreeView.IconPerNode = False

    If TreeView.IconPerNode Then
      TreeView.ImageList = New ImageList()

      ' TODO JBP: Made generic. Actualmente se hace aquí porque en el GUI_Controls no tenemos acceso al fichero de recursos. Esto debería estar en el uc_treeview. Buscar solución. 
      TreeView.ImageList.Images.Add(CLASS_GUI_RESOURCES.GetImage("node_enabled"))
      TreeView.ImageList.Images.Add(CLASS_GUI_RESOURCES.GetImage("node_disabled"))
      TreeView.ImageList.Images.Add(CLASS_GUI_RESOURCES.GetImage("node_warning"))
    End If

    MyBase.InitTreeView(RootDescription)

  End Sub ' InitTreeView

  ''' <summary>
  ''' Discard jackpot treeview changes
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub DiscardTreeViewChanges(ByRef TreeNode As TreeNode)

    MyBase.DiscardTreeViewChanges(TreeNode)

    ' Only for jackpot treeview item
    If Me.TreeView.SelectedNode Is Nothing Then
      Return
    End If

    ' Restore original name
    If Me.TreeView.SelectedNode.Tag = EditSelectionItemType.JACKPOT_CONFIG Then
      Me.SetSelectedNodeDescription(CType(DbReadObject, CLASS_JACKPOT).Selected.Name)
    End If

  End Sub ' DiscardTreeViewChanges

  ''' <summary>
  ''' Manage selected TreeNode item
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Public Overrides Sub TreeView_AfterSelect(sender As Object, e As TreeViewEventArgs)

    MyBase.TreeView_AfterSelect(sender, e)

    If Me.TreeView.IsNewNode() Then
      Me.Jackpot.Selected = New Jackpot()
    Else
      Me.SetSelectedJackpot()
    End If

  End Sub ' TreeView_AfterSelect

  ''' <summary>
  ''' Get object form treenode
  ''' </summary>
  ''' <param name="TreeNode"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function GetObjectFromTreeNode(ByVal TreeNode As TreeNode) As Object

    ' Check TreeNode item id
    If GUI_ParseNumber(TreeNode.Name) <= 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8087), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)

      Return Nothing
    End If

    Return Me.Jackpot.List.GetJackpotById(CInt(TreeNode.Name))

  End Function ' GetObjectFromTreeNode

  Public Overrides Sub TreeView_NodeMouseClick(sender As Object, e As TreeNodeMouseClickEventArgs)

    MyBase.TreeView_NodeMouseClick(sender, e)

    If e.Node.ContextMenu IsNot Nothing Then
      For Each item As MenuItem In e.Node.ContextMenu.MenuItems
        If item.Tag = ContextMenuAction.JACKPOT_ADD Then

          If Me.Jackpot.List.GetJackpotById(CInt(e.Node.Name)).Enabled Then
            item.Enabled = True
          Else
            item.Enabled = False

          End If

        End If
      Next
    End If




  End Sub ' TreeView_NodeMouseClick

  ''' <summary>
  ''' Set show new item mode
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_ShowNewItem()

    ' Sets the screen mode
    MyBase.GUI_ShowNewItem()

  End Sub ' GUI_ShowNewItem

  ''' <summary>
  ''' Set show edit item mode
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_ShowEditItem(ByVal ObjectId As Object)
    ' Sets the screen mode
    MyBase.GUI_ShowEditItem(ObjectId)

    Me.Jackpot = Me.DbEditedObject
  End Sub ' GUI_ShowEditItem

  ''' <summary>
  ''' Get specific context menu
  ''' </summary>
  ''' <param name="ObjectId"></param>
  ''' <param name="Type"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function TreeView_GetContextMenu(ByVal JackpotId As String, ByVal Type As EditSelectionItemType) As ContextMenu

    ' Get context menu
    Select Case Type
      Case EditSelectionItemType.JACKPOT_CONFIG ' Jackpot
        Return Me.Jackpot.List.GetJackpotById(JackpotId).GetContextMenu(AddressOf MenuItem_OnClick)

      Case EditSelectionItemType.NONE ' Root
        Return Me.CreateRootContextMenu()

    End Select

    Return Nothing

  End Function ' TreeView_GetContextMenu

#End Region ' Overrides

#Region "Public Functions"

  Public Sub New()

    ' This call is required by the designer.
    Me.InitializeComponent()

  End Sub

  ''' <summary>
  ''' ContextMenuItem click
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Public Sub MenuItem_OnClick(sender As Object, e As EventArgs)
    Dim _menu_item As MenuItem

    _menu_item = CType(sender, MenuItem)

    If Not _menu_item Is Nothing Then
      Call OnClickEvent(_menu_item)
    End If

  End Sub ' MenuItem_OnClick

#End Region

#Region " Private Methods "

  ''' <summary>
  ''' Manage click on context menu
  ''' </summary>
  ''' <param name="TreeNodeMenuItem"></param>
  ''' <remarks></remarks>
  Private Sub OnClickEvent(ByVal TreeNodeMenuItem As MenuItem)
    Dim _action As ContextMenuAction
    Dim _jackpot_id As Integer

    _jackpot_id = 0
    _action = CType(TreeNodeMenuItem.Tag, ContextMenuAction)

    If Not TreeNodeMenuItem Is Nothing AndAlso Not String.IsNullOrEmpty(TreeNodeMenuItem.Name) Then
      _jackpot_id = CInt(TreeNodeMenuItem.Name)
    End If

    Select Case _action

      Case ContextMenuAction.ROOT_COLLAPSE_ALL
        Call Me.OnclickEvent_TreeViewCollapseAll()

      Case ContextMenuAction.ROOT_EXPAND_ALL
        Call Me.OnclickEvent_TreeViewExpandAll()

      Case ContextMenuAction.ROOT_REFRESH
        Call Me.OnclickEvent_TreeViewRefresh()

      Case ContextMenuAction.JACKPOT_ADD
        Call Me.OnclickEvent_JackpotAddAmount(_jackpot_id)

      Case ContextMenuAction.JACKPOT_CLONE
        Call Me.OnclickEvent_JackpotClone(_jackpot_id)

      Case ContextMenuAction.JACKPOT_NEW
        Call Me.OnclickEvent_JackpotNew()

      Case ContextMenuAction.JACKPOT_RESET
        Call Me.OnclickEvent_JackpotReset(_jackpot_id)

    End Select

  End Sub ' OnClickEvent

  ''' <summary>
  ''' Add new jackpot node 
  ''' </summary>
  ''' <param name="JackpotId"></param>
  ''' <param name="IsClon"></param>
  ''' <remarks></remarks>
  Private Sub AddNewJackpotNode(ByVal JackpotId As Integer, ByVal IsClon As Boolean)
    ' Set New Jackpot label for replace in case Name Textbox is empty
    Me.NewJackpotLabel = IIf(IsClon, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8363), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8163))

    Me.TreeView.AddNewNode(JackpotId.ToString(), Me.NewJackpotLabel, EditSelectionItemType.JACKPOT_CONFIG)

  End Sub ' AddNewJackpotNode

  ''' <summary>
  ''' Add new jackpot node 
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Sub AddNewJackpotNode(ByVal JackpotId As Integer)
    Me.TreeView.AddNewNode(JackpotId.ToString(), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8163), EditSelectionItemType.JACKPOT_CONFIG)
  End Sub ' AddNewJackpotNode

  ''' <summary>
  ''' Create root context menu
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CreateRootContextMenu() As ContextMenu
    Dim _root_contextmenu As ContextMenu

    _root_contextmenu = New ContextMenu()
    _root_contextmenu.Name = Me.TreeView.GetRootName()
    _root_contextmenu.MenuItems.Add(CreateMenuItem(ContextMenuAction.JACKPOT_NEW, GLB_NLS_GUI_JACKPOT_MGR.GetString(526), AddressOf MenuItem_OnClick))
    _root_contextmenu.MenuItems.Add(CreateMenuItem(ContextMenuAction.NONE, "-", Nothing))
    _root_contextmenu.MenuItems.Add(CreateMenuItem(ContextMenuAction.ROOT_COLLAPSE_ALL, GLB_NLS_GUI_JACKPOT_MGR.GetString(527), AddressOf MenuItem_OnClick))
    _root_contextmenu.MenuItems.Add(CreateMenuItem(ContextMenuAction.ROOT_EXPAND_ALL, GLB_NLS_GUI_JACKPOT_MGR.GetString(528), AddressOf MenuItem_OnClick))
    _root_contextmenu.MenuItems.Add(CreateMenuItem(ContextMenuAction.ROOT_REFRESH, GLB_NLS_GUI_JACKPOT_MGR.GetString(529), AddressOf MenuItem_OnClick))

    Return _root_contextmenu

  End Function ' CreateRootContextMenu

  ''' <summary>
  ''' Create menu item for context menu
  ''' </summary>
  ''' <param name="Tag"></param>
  ''' <param name="Text"></param>
  ''' <param name="MenuItem_OnClick"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CreateMenuItem(Tag As ContextMenuAction, Text As String, MenuItem_OnClick As EventHandler) As MenuItem
    Dim _menu_item As MenuItem

    _menu_item = New MenuItem(Text, MenuItem_OnClick)
    _menu_item.Tag = Tag

    Return _menu_item
  End Function ' CreateMenuItem

  ''' <summary>
  ''' Initialize Jackpot tree view
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub InitializeJackpotTreeView()
    Me.InitTreeView(GLB_NLS_GUI_PLAYER_TRACKING.GetString(8065))
  End Sub ' InitializeJackpotTreeView

#Region " GetScreenData by uc Methods "

  ''' <summary>
  ''' Get Jackpot config screen data
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GetScreenData_JackpotConfig()
    ' Get selected Jackpot edited
    Me.Jackpot.Selected = CType(Me.CurrentUcEdit, uc_jackpot_config).GetJackpotConfig()
  End Sub ' GetScreenData_JackpotConfig

  ''' <summary>
  ''' Get Jackpot award config screen data
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GetScreenData_JackpotAwardConfig()
    ' Get selected Jackpot.AwardConfig edited
    Me.Jackpot.Selected.AwardConfig = CType(Me.CurrentUcEdit, uc_jackpot_award_config).GetJackpotAwardConfig()
  End Sub ' GetScreenData_JackpotAwardConfig

  ''' <summary>
  ''' Get Jackpot award prize config screen data
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GetScreenData_JackpotAwardPrizeConfig()
    ' Get selected Jackpot.AwardPrizeConfig edited
    Me.Jackpot.Selected.AwardPrizeConfig = CType(Me.CurrentUcEdit, uc_jackpot_award_prize_config).GetJackpotAwardPrizeConfig()
  End Sub ' GetScreenData_JackpotAwardConfig

#End Region

#Region " IsScreenData by uc Methods "

  ''' <summary>
  ''' Chack if is jackpot config screen data ok 
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function IsScreenDataOk_JackpotConfig() As Boolean
    Return CType(Me.CurrentUcEdit, uc_jackpot_config).IsScreenDataOk()
  End Function ' IsScreenDataOk_JackpotConfig

  ''' <summary>
  ''' Chack if is jackpot dashboard screen data ok 
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function IsScreenDataOk_JackpotDashboard() As Boolean
    Return CType(Me.CurrentUcEdit, uc_jackpot_dashboard).IsScreenDataOk()
  End Function ' IsScreenDataOk_JackpotDashboard

  ''' <summary>
  ''' Chack if is jackpot award configuration screen data ok 
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function IsScreenDataOk_JackpotAwardConfig() As Boolean
    Return CType(Me.CurrentUcEdit, uc_jackpot_award_config).IsScreenDataOk()
  End Function ' IsScreenDataOk_JackpotAwardConfig

  ''' <summary>
  ''' Chack if is jackpot award prize configuration screen data ok 
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function IsScreenDataOk_JackpotAwardPrizeConfig() As Boolean
    Dim _str_message As String

    If Not CType(Me.CurrentUcEdit, uc_jackpot_award_prize_config).IsScreenDataOk() Then
      Return False
    End If

    If ((Me.Jackpot.Selected.ContributionMeters.HappyHourAmount > 0) And _
       (CType(Me.CurrentUcEdit, uc_jackpot_award_prize_config).HappyHoursDisabled)) Then

      _str_message = GUI_FormatCurrency(Me.Jackpot.Selected.ContributionMeters.HappyHourAmount)
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8297), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _str_message) = ENUM_MB_RESULT.MB_RESULT_NO Then
        Return False
      End If
    End If

    If ((Me.Jackpot.Selected.ContributionMeters.SharedAmount > 0) And _
       (CType(Me.CurrentUcEdit, uc_jackpot_award_prize_config).PrizeSharingDisabled)) Then

      _str_message = GUI_FormatCurrency(Me.Jackpot.Selected.ContributionMeters.SharedAmount)
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8298), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _str_message) = ENUM_MB_RESULT.MB_RESULT_NO Then
        Return False
      End If
    End If

    Return True
  End Function ' IsScreenDataOk_JackpotAwardPrizeConfig

  ''' <summary>
  ''' Update main pct value
  ''' </summary>
  ''' <param name="UcAwardPrizeConfig"></param>
  ''' <remarks></remarks>
  Private Sub UpdateMainPctValue(ByRef UcAwardPrizeConfig As uc_jackpot_award_prize_config)
    UcAwardPrizeConfig.ef_pct_main.Value = Math.Max(UcAwardPrizeConfig.JackpotConfig.MainPct, 0)
  End Sub ' UpdateMainPctValue

#End Region

#Region " GetScreenData by uc Methods "

  ''' <summary>
  ''' Get Jackpot config screen data
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub KeyUp_JackpotConfig(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)

    If CType(Me.CurrentUcEdit, uc_jackpot_config).ef_name.ContainsFocus Then
      Me.NodeInEdition.Text = CType(Me.CurrentUcEdit, uc_jackpot_config).ef_name.Value

      If (String.IsNullOrEmpty(Me.NodeInEdition.Text)) Then
        Me.NodeInEdition.Text = Me.NewJackpotLabel
      End If

      Me.TreeView.Refresh()
    End If

  End Sub ' KeyUp_JackpotConfig

  ''' <summary>
  ''' Get Jackpot award config screen data
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub KeyUp_JackpotAwardConfig(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)


  End Sub ' KeyUp_JackpotAwardConfig

  ''' <summary>
  ''' Get Jackpot award prize config screen data
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub KeyUp_JackpotAwardPrizeConfig(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
    Dim _award_prize_config As uc_jackpot_award_prize_config

    _award_prize_config = CType(Me.CurrentUcEdit, uc_jackpot_award_prize_config)

    If _award_prize_config.ef_pct_prize_sharing.ContainsFocus OrElse _
      _award_prize_config.ef_pct_hidden.ContainsFocus OrElse _
      _award_prize_config.ef_pct_happy_hour.ContainsFocus Then

      '_award_prize_config.JackpotConfig.PrizeSharingPct = IIf(_award_prize_config.ef_pct_prize_sharing.Value.Equals(String.Empty), 0, _award_prize_config.ef_pct_prize_sharing.Value)
      '_award_prize_config.JackpotConfig.HiddenPct = IIf(_award_prize_config.ef_pct_hidden.Value.Equals(String.Empty), 0, _award_prize_config.ef_pct_hidden.Value)
      '_award_prize_config.JackpotConfig.HappyHourPct = IIf(_award_prize_config.ef_pct_happy_hour.Value.Equals(String.Empty), 0, _award_prize_config.ef_pct_happy_hour.Value)

      '_award_prize_config.ef_pct_hidden.Value = IIf(_award_prize_config.ef_pct_hidden.Value.Equals(String.Empty), 0, _award_prize_config.ef_pct_hidden.Value)

      Me.Jackpot.Selected.PrizeSharingPct = IIf(_award_prize_config.ef_pct_prize_sharing.Value.Equals(String.Empty), 0, _award_prize_config.ef_pct_prize_sharing.Value)
      Me.Jackpot.Selected.HiddenPct = IIf(_award_prize_config.ef_pct_hidden.Value.Equals(String.Empty), 0, _award_prize_config.ef_pct_hidden.Value)
      Me.Jackpot.Selected.HappyHourPct = IIf(_award_prize_config.ef_pct_happy_hour.Value.Equals(String.Empty), 0, _award_prize_config.ef_pct_happy_hour.Value)


      Call UpdateMainPctValue(CType(Me.CurrentUcEdit, uc_jackpot_award_prize_config))
    End If

  End Sub ' KeyUp_JackpotAwardPrizeConfig

#End Region

#Region " OnclickEvents "

  ''' <summary>
  ''' Expand all TreeView
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub OnclickEvent_TreeViewExpandAll()
    If Not Me.TreeView.IsNewNode() Then
      Me.TreeView.ExpandAll()
    End If
  End Sub

  ''' <summary>
  ''' Collapse all TreeView
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub OnclickEvent_TreeViewCollapseAll()
    If Not Me.TreeView.IsNewNode() Then
      Me.TreeView.CollapseAll()
    End If
  End Sub ' OnclickEvent_TreeViewCollapseAll

  ''' <summary>
  ''' Expand all TreeView
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub OnclickEvent_TreeViewRefresh()
    Me.TreeView.Refresh()
  End Sub ' OnclickEvent_TreeViewRefresh

  ''' <summary>
  ''' Create new Jackpot
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub OnclickEvent_JackpotNew()

    ' If exist new node in edition...
    If Me.TreeView.IsNewNode() Then
      Return
    End If

    ' Add new TreeView node
    Me.AddNewTreeViewNode(uc_treeview.DEFAULT_TREEVIEW_NEW_ITEM_ID)

  End Sub ' OnclickEvent_JackpotNew

  ''' <summary>
  ''' Clone Jackpot
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub OnclickEvent_JackpotClone(ByVal JackpotId As Integer)

    ' If exist new node in edition...
    If Me.TreeView.IsNewNode() Then
      Return
    End If

    ' Add new TreeView node
    Me.AddNewTreeViewNode(JackpotId, True)

  End Sub ' OnclickEvent_JackpotClone

  ''' <summary>
  ''' Add amount to Jackpot accumulated
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub OnclickEvent_JackpotAddAmount(ByVal JackpotId As Integer)

    If CurrentUser.Permissions(ENUM_FORM.FORM_JACKPOT_ADD_AMOUNT).Write Then

      Me.GUI_ShowEditItem(JackpotId)

      Dim _frm As frm_jackpot_add_amount
      _frm = New frm_jackpot_add_amount(Me.Jackpot.List.GetJackpotById(JackpotId))
      _frm.ShowDialog()

      m_jackpot.Selected = Me.Jackpot.List.GetJackpotById(JackpotId)

    Else

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8196), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)
    End If

  End Sub ' OnclickEvent_JackpotAddAmount

  ''' <summary>
  ''' Add amount to Jackpot accumulated
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub OnclickEvent_JackpotReset(ByVal JackpotId As Integer)

    Dim _reset_ops As New JackpotAmountOperations()

    Me.GUI_ShowEditItem(JackpotId)

    If CurrentUser.Permissions(ENUM_FORM.FORM_JACKPOT_ADD_AMOUNT).Write Then

      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8206), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_YES Then
        ' Add the Amount
        _reset_ops.Id = -1
        _reset_ops.JackpotId = JackpotId
        _reset_ops.JackpotType = WSI.Common.Jackpot.Jackpot.JackpotType.All
        _reset_ops.Type = JackpotAmountOperations.OperationType.Reset
        _reset_ops.Status = JackpotAmountOperations.OperationStatus.Pending
        _reset_ops.Amount = 0.0
        _reset_ops.UserName = GLB_CurrentUser.Name

        If _reset_ops.Save() Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8384), ENUM_MB_TYPE.MB_TYPE_INFO, ENUM_MB_BTN.MB_BTN_OK)
        Else
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8385), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)
        End If

      End If
    Else
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8196), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)
    End If
  End Sub ' OnclickEvent_JackpotReset

#End Region

  ''' <summary>
  ''' Set selected Jackpot
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SetSelectedJackpot()

    Dim _jackpot_id As Integer

    If Me.TreeView_CurrentNodeIsCompatible() AndAlso Not Me.TreeView.SelectedNode Is Nothing AndAlso Me.TreeView.SelectedNode.Name <> Me.TreeView.GetRootName() Then
      _jackpot_id = CInt(Me.TreeView.SelectedNode.Name)

      If _jackpot_id > 0 Then
        Me.Jackpot.Selected = Me.Jackpot.List.GetJackpotById(_jackpot_id)
      End If
    End If

  End Sub ' SetSelectedJackpot

  ''' <summary>
  ''' Add new TreeView node 
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub AddNewTreeViewNode(ByVal JackpotId As Integer)
    Me.AddNewTreeViewNode(JackpotId, False)
  End Sub
  Private Sub AddNewTreeViewNode(ByVal JackpotId As Integer, ByVal IsClon As Boolean)

    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW
    Me.AddNewJackpotNode(JackpotId, IsClon)

    ' If jackpot Id Not exist in list of Jackpots: New Jackpot.
    ' If jackpot Id exist in list of Jackpots: New cloned Jackpot.
    Me.BindTreeNodeToEdit(EditSelectionItemType.JACKPOT_CONFIG, Me.Jackpot.List.GetJackpotById(JackpotId))

  End Sub ' AddNewTreeViewNode

  ''' <summary>
  ''' Current node is compatible
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function TreeView_CurrentNodeIsCompatible() As Boolean

    ' Check if node in edition is compatible with Jackpots
    Return (Me.NodeInEdition.Tag = EditSelectionItemType.JACKPOT_CONFIG Or _
            Me.NodeInEdition.Tag = EditSelectionItemType.JACKPOT_AWARD_CONFIG Or _
            Me.NodeInEdition.Tag = EditSelectionItemType.JACKPOT_AWARD_PRIZE_CONFIG)

  End Function ' TreeView_CurrentNodeIsCompatible

  ''' <summary>
  ''' Click on control inside container and this haven't access to forms. 
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Sub ClickEventManager(ByVal Type As ENUM_BUTTON_CLICK_EVENT_TYPE)

    Select Case Type
      Case ENUM_BUTTON_CLICK_EVENT_TYPE.EVENT_AFFECTED_ACCOUNTS
        Me.ShowAffectedAccounts()

    End Select

  End Sub ' ClickEventManager

  Private Sub ShowAffectedAccounts()
    Dim _frm As frm_account_flags
    Dim _flags As New List(Of JackpotAwardFilterFlag)
    Dim _flag As New JackpotAwardFilterFlag
    Dim _idx As Integer
    Dim _flags_fake As New JackpotAwardFilterFlagList
    _frm = New frm_account_flags

    _flags = Me.Jackpot.Selected.AwardConfig.Filter.Flags.RelatedFlags

    'set levels and datetable
    _frm.FlagsAccountLevelsAdd()

    For _idx = 0 To _flags.Count - 1
      ' Set flags
      _flag = _flags(_idx)

      If Not _flag.IsDeleted Then
        _frm.FlagsAccountAdd(_idx, _flag.FlagId, _flag.FlagCount, _flag.Name, IIf(_flag.Color.ToString().Length > 0, _flag.Color, 0))
      End If
    Next

    If _flags.Count > 0 Then
      _frm.ShowForEdit(Me.MdiParent, True)
    Else
      Call _frm.Dispose()
      _frm = Nothing
    End If

  End Sub

#End Region

End Class
