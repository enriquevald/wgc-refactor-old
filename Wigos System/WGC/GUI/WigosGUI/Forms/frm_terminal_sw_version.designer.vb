<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_terminal_sw_version
  Inherits GUI_Controls.frm_base

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Me.dg_list = New GUI_Controls.uc_grid()
    Me.btn_exit = New GUI_Controls.uc_button()
    Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
    Me.gb_status = New System.Windows.Forms.GroupBox()
    Me.lbl_standby = New System.Windows.Forms.Label()
    Me.tf_outdated = New GUI_Controls.uc_text_field()
    Me.lbl_running = New System.Windows.Forms.Label()
    Me.tf_to_date = New GUI_Controls.uc_text_field()
    Me.tf_last_update = New GUI_Controls.uc_text_field()
    Me.btn_refresh = New GUI_Controls.uc_button()
    Me.uc_pr_list = New GUI_Controls.uc_provider()
    Me.gb_session = New System.Windows.Forms.GroupBox()
    Me.cb_session_free = New System.Windows.Forms.CheckBox()
    Me.cb_session_oc = New System.Windows.Forms.CheckBox()
    Me.gb_wc2 = New System.Windows.Forms.GroupBox()
    Me.lbl_color_wc2_disconnected = New System.Windows.Forms.Label()
    Me.lbl_color_wc2_connected = New System.Windows.Forms.Label()
    Me.cb_wc2_disconnected = New System.Windows.Forms.CheckBox()
    Me.cb_wc2_connected = New System.Windows.Forms.CheckBox()
    Me.gb_wcp = New System.Windows.Forms.GroupBox()
    Me.lbl_color_wcp_disconnected = New System.Windows.Forms.Label()
    Me.lbl_color_wcp_connected = New System.Windows.Forms.Label()
    Me.cb_wcp_disconnected = New System.Windows.Forms.CheckBox()
    Me.cb_wcp_connected = New System.Windows.Forms.CheckBox()
    Me.cb_active_only = New System.Windows.Forms.CheckBox()
    Me.uc_type_list = New GUI_Controls.uc_terminal_type()
    Me.btn_excel = New GUI_Controls.uc_button()
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox()
    Me.btn_filter_reset = New GUI_Controls.uc_button()
    Me.gb_status.SuspendLayout()
    Me.gb_session.SuspendLayout()
    Me.gb_wc2.SuspendLayout()
    Me.gb_wcp.SuspendLayout()
    Me.SuspendLayout()
    '
    'dg_list
    '
    Me.dg_list.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.dg_list.CurrentCol = -1
    Me.dg_list.CurrentRow = -1
    Me.dg_list.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_list.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_list.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_list.Location = New System.Drawing.Point(9, 221)
    Me.dg_list.Name = "dg_list"
    Me.dg_list.PanelRightVisible = True
    Me.dg_list.Redraw = True
    Me.dg_list.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_list.Size = New System.Drawing.Size(1005, 435)
    Me.dg_list.Sortable = False
    Me.dg_list.SortAscending = True
    Me.dg_list.SortByCol = 0
    Me.dg_list.TabIndex = 8
    Me.dg_list.ToolTipped = True
    Me.dg_list.TopRow = -2
    Me.dg_list.WordWrap = False
    '
    'btn_exit
    '
    Me.btn_exit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btn_exit.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btn_exit.Location = New System.Drawing.Point(1061, 620)
    Me.btn_exit.Name = "btn_exit"
    Me.btn_exit.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_exit.Size = New System.Drawing.Size(90, 30)
    Me.btn_exit.TabIndex = 10
    Me.btn_exit.ToolTipped = False
    Me.btn_exit.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'Timer1
    '
    '
    'gb_status
    '
    Me.gb_status.Controls.Add(Me.lbl_standby)
    Me.gb_status.Controls.Add(Me.tf_outdated)
    Me.gb_status.Controls.Add(Me.lbl_running)
    Me.gb_status.Controls.Add(Me.tf_to_date)
    Me.gb_status.Location = New System.Drawing.Point(9, 7)
    Me.gb_status.Name = "gb_status"
    Me.gb_status.Size = New System.Drawing.Size(148, 64)
    Me.gb_status.TabIndex = 109
    Me.gb_status.TabStop = False
    Me.gb_status.Text = "xUpdate Status"
    '
    'lbl_standby
    '
    Me.lbl_standby.BackColor = System.Drawing.SystemColors.GrayText
    Me.lbl_standby.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_standby.Location = New System.Drawing.Point(10, 41)
    Me.lbl_standby.Name = "lbl_standby"
    Me.lbl_standby.Size = New System.Drawing.Size(16, 16)
    Me.lbl_standby.TabIndex = 107
    '
    'tf_outdated
    '
    Me.tf_outdated.IsReadOnly = True
    Me.tf_outdated.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_outdated.LabelForeColor = System.Drawing.Color.Black
    Me.tf_outdated.Location = New System.Drawing.Point(4, 37)
    Me.tf_outdated.Name = "tf_outdated"
    Me.tf_outdated.Size = New System.Drawing.Size(135, 24)
    Me.tf_outdated.SufixText = "Sufix Text"
    Me.tf_outdated.SufixTextVisible = True
    Me.tf_outdated.TabIndex = 108
    Me.tf_outdated.TabStop = False
    Me.tf_outdated.TextVisible = False
    Me.tf_outdated.TextWidth = 30
    Me.tf_outdated.Value = "xOutdated"
    '
    'lbl_running
    '
    Me.lbl_running.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_running.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_running.Location = New System.Drawing.Point(10, 19)
    Me.lbl_running.Name = "lbl_running"
    Me.lbl_running.Size = New System.Drawing.Size(16, 16)
    Me.lbl_running.TabIndex = 106
    '
    'tf_to_date
    '
    Me.tf_to_date.IsReadOnly = True
    Me.tf_to_date.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_to_date.LabelForeColor = System.Drawing.Color.Black
    Me.tf_to_date.Location = New System.Drawing.Point(4, 15)
    Me.tf_to_date.Name = "tf_to_date"
    Me.tf_to_date.Size = New System.Drawing.Size(135, 24)
    Me.tf_to_date.SufixText = "Sufix Text"
    Me.tf_to_date.SufixTextVisible = True
    Me.tf_to_date.TabIndex = 104
    Me.tf_to_date.TabStop = False
    Me.tf_to_date.TextVisible = False
    Me.tf_to_date.TextWidth = 30
    Me.tf_to_date.Value = "xUltimaVersion"
    '
    'tf_last_update
    '
    Me.tf_last_update.IsReadOnly = True
    Me.tf_last_update.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_last_update.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_last_update.Location = New System.Drawing.Point(9, 194)
    Me.tf_last_update.Name = "tf_last_update"
    Me.tf_last_update.Size = New System.Drawing.Size(250, 24)
    Me.tf_last_update.SufixText = "xSeconds"
    Me.tf_last_update.SufixTextVisible = True
    Me.tf_last_update.TabIndex = 112
    Me.tf_last_update.TabStop = False
    Me.tf_last_update.TextWidth = 150
    Me.tf_last_update.Value = "x99.999"
    '
    'btn_refresh
    '
    Me.btn_refresh.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btn_refresh.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btn_refresh.Location = New System.Drawing.Point(1061, 188)
    Me.btn_refresh.Name = "btn_refresh"
    Me.btn_refresh.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_refresh.Size = New System.Drawing.Size(90, 30)
    Me.btn_refresh.TabIndex = 7
    Me.btn_refresh.ToolTipped = False
    Me.btn_refresh.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'uc_pr_list
    '
    Me.uc_pr_list.FilterByCurrency = False
    Me.uc_pr_list.Location = New System.Drawing.Point(305, 3)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(337, 191)
    Me.uc_pr_list.TabIndex = 5
    Me.uc_pr_list.TerminalListHasValues = False
    '
    'gb_session
    '
    Me.gb_session.Controls.Add(Me.cb_session_free)
    Me.gb_session.Controls.Add(Me.cb_session_oc)
    Me.gb_session.Location = New System.Drawing.Point(9, 75)
    Me.gb_session.Name = "gb_session"
    Me.gb_session.Size = New System.Drawing.Size(148, 66)
    Me.gb_session.TabIndex = 0
    Me.gb_session.TabStop = False
    Me.gb_session.Text = "xSession"
    '
    'cb_session_free
    '
    Me.cb_session_free.AutoSize = True
    Me.cb_session_free.Checked = True
    Me.cb_session_free.CheckState = System.Windows.Forms.CheckState.Checked
    Me.cb_session_free.Location = New System.Drawing.Point(6, 41)
    Me.cb_session_free.Name = "cb_session_free"
    Me.cb_session_free.Size = New System.Drawing.Size(58, 17)
    Me.cb_session_free.TabIndex = 1
    Me.cb_session_free.Text = "xFree"
    Me.cb_session_free.UseVisualStyleBackColor = True
    '
    'cb_session_oc
    '
    Me.cb_session_oc.AutoSize = True
    Me.cb_session_oc.Checked = True
    Me.cb_session_oc.CheckState = System.Windows.Forms.CheckState.Checked
    Me.cb_session_oc.Location = New System.Drawing.Point(6, 20)
    Me.cb_session_oc.Name = "cb_session_oc"
    Me.cb_session_oc.Size = New System.Drawing.Size(83, 17)
    Me.cb_session_oc.TabIndex = 0
    Me.cb_session_oc.Text = "xOcupado"
    Me.cb_session_oc.UseVisualStyleBackColor = True
    '
    'gb_wc2
    '
    Me.gb_wc2.Controls.Add(Me.lbl_color_wc2_disconnected)
    Me.gb_wc2.Controls.Add(Me.lbl_color_wc2_connected)
    Me.gb_wc2.Controls.Add(Me.cb_wc2_disconnected)
    Me.gb_wc2.Controls.Add(Me.cb_wc2_connected)
    Me.gb_wc2.Location = New System.Drawing.Point(164, 75)
    Me.gb_wc2.Name = "gb_wc2"
    Me.gb_wc2.Size = New System.Drawing.Size(139, 66)
    Me.gb_wc2.TabIndex = 4
    Me.gb_wc2.TabStop = False
    Me.gb_wc2.Text = "xWC2"
    '
    'lbl_color_wc2_disconnected
    '
    Me.lbl_color_wc2_disconnected.BackColor = System.Drawing.SystemColors.GrayText
    Me.lbl_color_wc2_disconnected.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_color_wc2_disconnected.Location = New System.Drawing.Point(6, 41)
    Me.lbl_color_wc2_disconnected.Name = "lbl_color_wc2_disconnected"
    Me.lbl_color_wc2_disconnected.Size = New System.Drawing.Size(16, 16)
    Me.lbl_color_wc2_disconnected.TabIndex = 109
    '
    'lbl_color_wc2_connected
    '
    Me.lbl_color_wc2_connected.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_color_wc2_connected.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_color_wc2_connected.Location = New System.Drawing.Point(6, 19)
    Me.lbl_color_wc2_connected.Name = "lbl_color_wc2_connected"
    Me.lbl_color_wc2_connected.Size = New System.Drawing.Size(16, 16)
    Me.lbl_color_wc2_connected.TabIndex = 108
    '
    'cb_wc2_disconnected
    '
    Me.cb_wc2_disconnected.AutoSize = True
    Me.cb_wc2_disconnected.Checked = True
    Me.cb_wc2_disconnected.CheckState = System.Windows.Forms.CheckState.Checked
    Me.cb_wc2_disconnected.Location = New System.Drawing.Point(33, 41)
    Me.cb_wc2_disconnected.Name = "cb_wc2_disconnected"
    Me.cb_wc2_disconnected.Size = New System.Drawing.Size(109, 17)
    Me.cb_wc2_disconnected.TabIndex = 1
    Me.cb_wc2_disconnected.Text = "xDisconnected"
    Me.cb_wc2_disconnected.UseVisualStyleBackColor = True
    '
    'cb_wc2_connected
    '
    Me.cb_wc2_connected.AutoSize = True
    Me.cb_wc2_connected.Checked = True
    Me.cb_wc2_connected.CheckState = System.Windows.Forms.CheckState.Checked
    Me.cb_wc2_connected.Location = New System.Drawing.Point(33, 18)
    Me.cb_wc2_connected.Name = "cb_wc2_connected"
    Me.cb_wc2_connected.Size = New System.Drawing.Size(94, 17)
    Me.cb_wc2_connected.TabIndex = 0
    Me.cb_wc2_connected.Text = "xConnected"
    Me.cb_wc2_connected.UseVisualStyleBackColor = True
    '
    'gb_wcp
    '
    Me.gb_wcp.Controls.Add(Me.lbl_color_wcp_disconnected)
    Me.gb_wcp.Controls.Add(Me.lbl_color_wcp_connected)
    Me.gb_wcp.Controls.Add(Me.cb_wcp_disconnected)
    Me.gb_wcp.Controls.Add(Me.cb_wcp_connected)
    Me.gb_wcp.Location = New System.Drawing.Point(164, 7)
    Me.gb_wcp.Name = "gb_wcp"
    Me.gb_wcp.Size = New System.Drawing.Size(139, 62)
    Me.gb_wcp.TabIndex = 3
    Me.gb_wcp.TabStop = False
    Me.gb_wcp.Text = "xWCP"
    '
    'lbl_color_wcp_disconnected
    '
    Me.lbl_color_wcp_disconnected.BackColor = System.Drawing.SystemColors.GrayText
    Me.lbl_color_wcp_disconnected.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_color_wcp_disconnected.Location = New System.Drawing.Point(6, 41)
    Me.lbl_color_wcp_disconnected.Name = "lbl_color_wcp_disconnected"
    Me.lbl_color_wcp_disconnected.Size = New System.Drawing.Size(16, 16)
    Me.lbl_color_wcp_disconnected.TabIndex = 108
    '
    'lbl_color_wcp_connected
    '
    Me.lbl_color_wcp_connected.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_color_wcp_connected.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_color_wcp_connected.Location = New System.Drawing.Point(6, 18)
    Me.lbl_color_wcp_connected.Name = "lbl_color_wcp_connected"
    Me.lbl_color_wcp_connected.Size = New System.Drawing.Size(16, 16)
    Me.lbl_color_wcp_connected.TabIndex = 107
    '
    'cb_wcp_disconnected
    '
    Me.cb_wcp_disconnected.AutoSize = True
    Me.cb_wcp_disconnected.Checked = True
    Me.cb_wcp_disconnected.CheckState = System.Windows.Forms.CheckState.Checked
    Me.cb_wcp_disconnected.Location = New System.Drawing.Point(33, 41)
    Me.cb_wcp_disconnected.Name = "cb_wcp_disconnected"
    Me.cb_wcp_disconnected.Size = New System.Drawing.Size(102, 17)
    Me.cb_wcp_disconnected.TabIndex = 1
    Me.cb_wcp_disconnected.Text = "xDisconected"
    Me.cb_wcp_disconnected.UseVisualStyleBackColor = True
    '
    'cb_wcp_connected
    '
    Me.cb_wcp_connected.AutoSize = True
    Me.cb_wcp_connected.Checked = True
    Me.cb_wcp_connected.CheckState = System.Windows.Forms.CheckState.Checked
    Me.cb_wcp_connected.Location = New System.Drawing.Point(33, 18)
    Me.cb_wcp_connected.Name = "cb_wcp_connected"
    Me.cb_wcp_connected.Size = New System.Drawing.Size(94, 17)
    Me.cb_wcp_connected.TabIndex = 0
    Me.cb_wcp_connected.Text = "xConnected"
    Me.cb_wcp_connected.UseVisualStyleBackColor = True
    '
    'cb_active_only
    '
    Me.cb_active_only.AutoSize = True
    Me.cb_active_only.Checked = True
    Me.cb_active_only.CheckState = System.Windows.Forms.CheckState.Checked
    Me.cb_active_only.Location = New System.Drawing.Point(13, 147)
    Me.cb_active_only.Name = "cb_active_only"
    Me.cb_active_only.Size = New System.Drawing.Size(94, 17)
    Me.cb_active_only.TabIndex = 1
    Me.cb_active_only.Text = "xActiveOnly"
    Me.cb_active_only.UseVisualStyleBackColor = True
    '
    'uc_type_list
    '
    Me.uc_type_list.Location = New System.Drawing.Point(635, 7)
    Me.uc_type_list.Name = "uc_type_list"
    Me.uc_type_list.Size = New System.Drawing.Size(317, 155)
    Me.uc_type_list.TabIndex = 6
    '
    'btn_excel
    '
    Me.btn_excel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btn_excel.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_excel.Location = New System.Drawing.Point(1061, 593)
    Me.btn_excel.Name = "btn_excel"
    Me.btn_excel.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_excel.Size = New System.Drawing.Size(90, 30)
    Me.btn_excel.TabIndex = 9
    Me.btn_excel.ToolTipped = False
    Me.btn_excel.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'chk_terminal_location
    '
    Me.chk_terminal_location.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.chk_terminal_location.Location = New System.Drawing.Point(13, 171)
    Me.chk_terminal_location.Name = "chk_terminal_location"
    Me.chk_terminal_location.Size = New System.Drawing.Size(250, 19)
    Me.chk_terminal_location.TabIndex = 2
    Me.chk_terminal_location.Text = "xShow terminals location"
    '
    'btn_filter_reset
    '
    Me.btn_filter_reset.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btn_filter_reset.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btn_filter_reset.Location = New System.Drawing.Point(1061, 152)
    Me.btn_filter_reset.Name = "btn_filter_reset"
    Me.btn_filter_reset.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_filter_reset.Size = New System.Drawing.Size(90, 30)
    Me.btn_filter_reset.TabIndex = 113
    Me.btn_filter_reset.ToolTipped = False
    Me.btn_filter_reset.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'frm_terminal_sw_version
    '
    Me.CancelButton = Me.btn_exit
    Me.ClientSize = New System.Drawing.Size(1158, 657)
    Me.Controls.Add(Me.btn_filter_reset)
    Me.Controls.Add(Me.chk_terminal_location)
    Me.Controls.Add(Me.btn_excel)
    Me.Controls.Add(Me.uc_type_list)
    Me.Controls.Add(Me.cb_active_only)
    Me.Controls.Add(Me.gb_wcp)
    Me.Controls.Add(Me.gb_wc2)
    Me.Controls.Add(Me.gb_session)
    Me.Controls.Add(Me.uc_pr_list)
    Me.Controls.Add(Me.btn_refresh)
    Me.Controls.Add(Me.tf_last_update)
    Me.Controls.Add(Me.gb_status)
    Me.Controls.Add(Me.btn_exit)
    Me.Controls.Add(Me.dg_list)
    Me.Name = "frm_terminal_sw_version"
    Me.Text = "xfrm_terminal_sw_version"
    Me.gb_status.ResumeLayout(False)
    Me.gb_session.ResumeLayout(False)
    Me.gb_session.PerformLayout()
    Me.gb_wc2.ResumeLayout(False)
    Me.gb_wc2.PerformLayout()
    Me.gb_wcp.ResumeLayout(False)
    Me.gb_wcp.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

  Private Sub Grid_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dg_list.Load

  End Sub
  Friend WithEvents btn_exit As GUI_Controls.uc_button
  Friend WithEvents Timer1 As System.Windows.Forms.Timer
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_standby As System.Windows.Forms.Label
  Friend WithEvents tf_outdated As GUI_Controls.uc_text_field
  Friend WithEvents lbl_running As System.Windows.Forms.Label
  Friend WithEvents tf_to_date As GUI_Controls.uc_text_field
  Friend WithEvents tf_last_update As GUI_Controls.uc_text_field
  Friend WithEvents btn_refresh As GUI_Controls.uc_button
  Friend WithEvents dg_list As GUI_Controls.uc_grid
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents gb_session As System.Windows.Forms.GroupBox
  Friend WithEvents cb_session_free As System.Windows.Forms.CheckBox
  Friend WithEvents cb_session_oc As System.Windows.Forms.CheckBox
  Friend WithEvents gb_wc2 As System.Windows.Forms.GroupBox
  Friend WithEvents cb_wc2_disconnected As System.Windows.Forms.CheckBox
  Friend WithEvents cb_wc2_connected As System.Windows.Forms.CheckBox
  Friend WithEvents gb_wcp As System.Windows.Forms.GroupBox
  Friend WithEvents cb_wcp_disconnected As System.Windows.Forms.CheckBox
  Friend WithEvents cb_wcp_connected As System.Windows.Forms.CheckBox
  Friend WithEvents cb_active_only As System.Windows.Forms.CheckBox
  Friend WithEvents uc_type_list As GUI_Controls.uc_terminal_type
  Friend WithEvents btn_excel As GUI_Controls.uc_button
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_color_wcp_connected As System.Windows.Forms.Label
  Friend WithEvents lbl_color_wc2_connected As System.Windows.Forms.Label
  Friend WithEvents lbl_color_wcp_disconnected As System.Windows.Forms.Label
  Friend WithEvents lbl_color_wc2_disconnected As System.Windows.Forms.Label
  Friend WithEvents btn_filter_reset As GUI_Controls.uc_button
End Class
