'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_terminals_3gs_edit
' DESCRIPTION : Manage 3GS (Third-Party Game System) Terminals
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 13-MAY-2010 RCI    Initial version.
' 20-FEB-2012 MPO    Fixed bug: The field T3GS_TERMINAL_ID has a id of provider when insert the terminal.
' 20-FEB-2013 JCM    Fixed bug: DataChangedOk, when define some fields Null throw exception.
' 13-MAR-2013 HBB    Fixed Bug #641: If only changed the column "ID m�quina", the changes are not saved.
' 20-MAR-2013 LEM    Fixed Bug #652: Database error if terminal name include character ('). 
' 15-APR-2013 DMR    Fixed Bug #714: Changed to correct date format at TP_REPORTED column.
' 22-MAY-2013 ACM    Fixed Bug #794: Filter any occurrence in description. Not just the first characters.
' 05-JUN-2013 DHA    Fixed Bug #819: Modifications in the query for filters with character '%'.
' 26-JUN-2013 DMR    Added Excel Functionality.
' 05-JUL-2013 JCOR   Call to frm_terminals_sel changed to frm_terminals.
' 29-JUL-2013 DMR    Modified GUI_ReportUpdateFilters to report all filters.
' 17-SEP-2013 ANG    Fixed Bug #WIG-208.
' 18-OCT-2013 ACM    Fixed Bug WIG-287 Check for non printable characters in terminals name.
' 19-NOV-2013 LJM    Changes for auditing forms.
' 29-MAY-2014 JAB    Added permissions: To buttons "Excel" and "Print".
' 13-OCT-2014 LEM    Fixed Bug WIG-1482: Don't show created terminal name correctly.
' 09-JUL-2015 FAV    Fixed Bug WIG-WIG-2543: Null reference exception with 3GS Terminals
' 05-AUG-2015 DCS    Fixed Bug WIG-WIG-2624: SQL exception when insert new 3GS Terminals in field TE_NAME (now it's computed)
' 22-FEB-2016 DDS    Fixed Bug 9862: TE_ISO_CODE needs default value
' 09-NOV-2016 JBP    Bug 20284:Terminales 3GS: Al tratar de guardar el "Juego" de un terminal 3GS salta una excepci�n no controlada
' 05-JAN-2017 CCG    Bug 21898:GUI: Terminales pendientes - mensaje de error incorrecto
' 19-JUN-2017 DPC    Bug 28211:The 3GS terminals created are not having a correct serial number value in the database
'-------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common
Imports GUI_Reports.PrintDataset
Imports System.Data.SqlClient
Imports System.Text

Public Class frm_terminals_3gs_edit
  Inherits frm_base_print

#Region " Constants "

  Private Const TYPE_PENDING As Integer = 1
  Private Const TYPE_3GS As Integer = 2
  Private Const TYPE_GLOBAL As Integer = 3

  Private Const SOURCE_GAME_NAME_3GS As String = "GAME_3GS"

  ' DataGrid PENDING Columns
  Private Const GRID_COLUMN_PENDING_VENDOR_ID As Integer = 0
  Private Const GRID_COLUMN_PENDING_SERIAL_NUMBER As Integer = 1
  Private Const GRID_COLUMN_PENDING_MACHINE_NUMBER As Integer = 2
  Private Const GRID_COLUMN_PENDING_REPORTED As Integer = 3
  Private Const GRID_COLUMN_PENDING_IGNORE As Integer = 4

  Private Const GRID_PENDING_COLUMNS As Integer = 5

  ' DataGrid 3GS Columns
  Public Const GRID_COLUMN_3GS_TE_PROV_ID As Integer = 0
  Public Const GRID_COLUMN_3GS_GAME_ID As Integer = 1
  Public Const GRID_COLUMN_3GS_VENDOR_ID As Integer = 2
  Public Const GRID_COLUMN_3GS_SERIAL_NUMBER As Integer = 3
  Public Const GRID_COLUMN_3GS_MACHINE_NUMBER As Integer = 4
  Public Const GRID_COLUMN_3GS_PROVIDER_ID As Integer = 5
  Public Const GRID_COLUMN_3GS_TERMINAL_NAME As Integer = 6
  Public Const GRID_COLUMN_3GS_GAME_NAME As Integer = 7
  Public Const GRID_COLUMN_3GS_FLAG_REPLACEMENT As Integer = 8
  Public Const GRID_COLUMN_3GS_FLAG_REPLACEMENT_RETIRED As Integer = 9

  Private Const GRID_3GS_COLUMNS As Integer = 9

  ' Width
  Private Const GRID_WIDTH_VENDOR_ID As Integer = 120
  Private Const GRID_WIDTH_SERIAL_NUMBER As Integer = 180
  Private Const GRID_WIDTH_MACHINE_NUMBER As Integer = 120
  Private Const GRID_WIDTH_PROVIDER_ID As Integer = 120
  Private Const GRID_WIDTH_TERMINAL_NAME As Integer = 180
  Private Const GRID_WIDTH_GAME_NAME As Integer = 150
  Private Const GRID_WIDTH_TP_REPORTED As Integer = 180
  Private Const GRID_WIDTH_TP_IGNORE As Integer = 60

  Private Const STRING_CONCAT_VENDOR_SN As String = "-"

#End Region ' Constants

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents gb_terminal_3gs As System.Windows.Forms.GroupBox
  Friend WithEvents ef_terminal_3gs_serial_number As GUI_Controls.uc_entry_field
  Friend WithEvents ef_terminal_3gs_vendor As GUI_Controls.uc_entry_field
  Friend WithEvents dg_terminals_pending As WigosGUI.MyDataGrid
  Friend WithEvents dg_terminals_3gs As WigosGUI.MyDataGrid
  Friend WithEvents Splitter1 As System.Windows.Forms.Splitter
  Friend WithEvents gb_terminal_pending As System.Windows.Forms.GroupBox
  Friend WithEvents ef_terminal_pending_vendor As GUI_Controls.uc_entry_field
  Friend WithEvents ef_terminal_pending_serial_number As GUI_Controls.uc_entry_field
  Friend WithEvents chk_hide_ignored As System.Windows.Forms.CheckBox
  Friend WithEvents cmb_source As GUI_Controls.uc_combo
  Friend WithEvents btn_add As GUI_Controls.uc_button
  Friend WithEvents btn_replace As GUI_Controls.uc_button

  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.gb_terminal_pending = New System.Windows.Forms.GroupBox
    Me.cmb_source = New GUI_Controls.uc_combo
    Me.chk_hide_ignored = New System.Windows.Forms.CheckBox
    Me.ef_terminal_pending_serial_number = New GUI_Controls.uc_entry_field
    Me.ef_terminal_pending_vendor = New GUI_Controls.uc_entry_field
    Me.Splitter1 = New System.Windows.Forms.Splitter
    Me.ef_terminal_3gs_vendor = New GUI_Controls.uc_entry_field
    Me.ef_terminal_3gs_serial_number = New GUI_Controls.uc_entry_field
    Me.gb_terminal_3gs = New System.Windows.Forms.GroupBox
    Me.btn_add = New GUI_Controls.uc_button
    Me.dg_terminals_3gs = New WigosGUI.MyDataGrid
    Me.dg_terminals_pending = New WigosGUI.MyDataGrid
    Me.btn_replace = New GUI_Controls.uc_button
    Me.panel_grids.SuspendLayout()
    Me.panel_buttons.SuspendLayout()
    Me.panel_filter.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_terminal_pending.SuspendLayout()
    Me.gb_terminal_3gs.SuspendLayout()
    CType(Me.dg_terminals_3gs, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.dg_terminals_pending, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'panel_grids
    '
    Me.panel_grids.Controls.Add(Me.dg_terminals_3gs)
    Me.panel_grids.Controls.Add(Me.Splitter1)
    Me.panel_grids.Controls.Add(Me.dg_terminals_pending)
    Me.panel_grids.Location = New System.Drawing.Point(4, 150)
    Me.panel_grids.Size = New System.Drawing.Size(1017, 556)
    Me.panel_grids.Controls.SetChildIndex(Me.panel_buttons, 0)
    Me.panel_grids.Controls.SetChildIndex(Me.dg_terminals_pending, 0)
    Me.panel_grids.Controls.SetChildIndex(Me.Splitter1, 0)
    Me.panel_grids.Controls.SetChildIndex(Me.dg_terminals_3gs, 0)
    '
    'panel_buttons
    '
    Me.panel_buttons.Controls.Add(Me.btn_replace)
    Me.panel_buttons.Controls.Add(Me.btn_add)
    Me.panel_buttons.Location = New System.Drawing.Point(929, 0)
    Me.panel_buttons.Size = New System.Drawing.Size(88, 556)
    Me.panel_buttons.Controls.SetChildIndex(Me.btn_add, 0)
    Me.panel_buttons.Controls.SetChildIndex(Me.btn_replace, 0)
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_terminal_pending)
    Me.panel_filter.Controls.Add(Me.gb_terminal_3gs)
    Me.panel_filter.Size = New System.Drawing.Size(1017, 123)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_terminal_3gs, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_terminal_pending, 0)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Location = New System.Drawing.Point(4, 127)
    Me.pn_separator_line.Size = New System.Drawing.Size(1017, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1017, 4)
    '
    'gb_terminal_pending
    '
    Me.gb_terminal_pending.Controls.Add(Me.cmb_source)
    Me.gb_terminal_pending.Controls.Add(Me.chk_hide_ignored)
    Me.gb_terminal_pending.Controls.Add(Me.ef_terminal_pending_serial_number)
    Me.gb_terminal_pending.Controls.Add(Me.ef_terminal_pending_vendor)
    Me.gb_terminal_pending.Location = New System.Drawing.Point(6, 6)
    Me.gb_terminal_pending.Name = "gb_terminal_pending"
    Me.gb_terminal_pending.Size = New System.Drawing.Size(279, 109)
    Me.gb_terminal_pending.TabIndex = 1
    Me.gb_terminal_pending.TabStop = False
    Me.gb_terminal_pending.Text = "xTerminalPending"
    '
    'cmb_source
    '
    Me.cmb_source.IsReadOnly = False
    Me.cmb_source.Location = New System.Drawing.Point(133, 78)
    Me.cmb_source.Name = "cmb_source"
    Me.cmb_source.SelectedIndex = -1
    Me.cmb_source.Size = New System.Drawing.Size(200, 24)
    Me.cmb_source.SufixText = "Sufix Text"
    Me.cmb_source.SufixTextVisible = True
    Me.cmb_source.TabIndex = 3
    Me.cmb_source.Visible = False
    '
    'chk_hide_ignored
    '
    Me.chk_hide_ignored.Checked = True
    Me.chk_hide_ignored.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_hide_ignored.Location = New System.Drawing.Point(16, 79)
    Me.chk_hide_ignored.Name = "chk_hide_ignored"
    Me.chk_hide_ignored.Size = New System.Drawing.Size(221, 23)
    Me.chk_hide_ignored.TabIndex = 2
    Me.chk_hide_ignored.Text = "xHideIgnored"
    '
    'ef_terminal_pending_serial_number
    '
    Me.ef_terminal_pending_serial_number.DoubleValue = 0
    Me.ef_terminal_pending_serial_number.IntegerValue = 0
    Me.ef_terminal_pending_serial_number.IsReadOnly = False
    Me.ef_terminal_pending_serial_number.Location = New System.Drawing.Point(6, 48)
    Me.ef_terminal_pending_serial_number.Name = "ef_terminal_pending_serial_number"
    Me.ef_terminal_pending_serial_number.Size = New System.Drawing.Size(264, 24)
    Me.ef_terminal_pending_serial_number.SufixText = "Sufix Text"
    Me.ef_terminal_pending_serial_number.SufixTextVisible = True
    Me.ef_terminal_pending_serial_number.TabIndex = 1
    Me.ef_terminal_pending_serial_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_terminal_pending_serial_number.TextValue = ""
    Me.ef_terminal_pending_serial_number.TextWidth = 100
    Me.ef_terminal_pending_serial_number.Value = ""
    '
    'ef_terminal_pending_vendor
    '
    Me.ef_terminal_pending_vendor.DoubleValue = 0
    Me.ef_terminal_pending_vendor.IntegerValue = 0
    Me.ef_terminal_pending_vendor.IsReadOnly = False
    Me.ef_terminal_pending_vendor.Location = New System.Drawing.Point(6, 22)
    Me.ef_terminal_pending_vendor.Name = "ef_terminal_pending_vendor"
    Me.ef_terminal_pending_vendor.Size = New System.Drawing.Size(264, 24)
    Me.ef_terminal_pending_vendor.SufixText = "Sufix Text"
    Me.ef_terminal_pending_vendor.SufixTextVisible = True
    Me.ef_terminal_pending_vendor.TabIndex = 0
    Me.ef_terminal_pending_vendor.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_terminal_pending_vendor.TextValue = ""
    Me.ef_terminal_pending_vendor.TextWidth = 100
    Me.ef_terminal_pending_vendor.Value = ""
    '
    'Splitter1
    '
    Me.Splitter1.Dock = System.Windows.Forms.DockStyle.Top
    Me.Splitter1.Location = New System.Drawing.Point(0, 167)
    Me.Splitter1.Name = "Splitter1"
    Me.Splitter1.Size = New System.Drawing.Size(929, 10)
    Me.Splitter1.TabIndex = 7
    Me.Splitter1.TabStop = False
    '
    'ef_terminal_3gs_vendor
    '
    Me.ef_terminal_3gs_vendor.DoubleValue = 0
    Me.ef_terminal_3gs_vendor.IntegerValue = 0
    Me.ef_terminal_3gs_vendor.IsReadOnly = False
    Me.ef_terminal_3gs_vendor.Location = New System.Drawing.Point(6, 22)
    Me.ef_terminal_3gs_vendor.Name = "ef_terminal_3gs_vendor"
    Me.ef_terminal_3gs_vendor.Size = New System.Drawing.Size(264, 24)
    Me.ef_terminal_3gs_vendor.SufixText = "Sufix Text"
    Me.ef_terminal_3gs_vendor.SufixTextVisible = True
    Me.ef_terminal_3gs_vendor.TabIndex = 0
    Me.ef_terminal_3gs_vendor.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_terminal_3gs_vendor.TextValue = ""
    Me.ef_terminal_3gs_vendor.TextWidth = 100
    Me.ef_terminal_3gs_vendor.Value = ""
    '
    'ef_terminal_3gs_serial_number
    '
    Me.ef_terminal_3gs_serial_number.DoubleValue = 0
    Me.ef_terminal_3gs_serial_number.IntegerValue = 0
    Me.ef_terminal_3gs_serial_number.IsReadOnly = False
    Me.ef_terminal_3gs_serial_number.Location = New System.Drawing.Point(6, 48)
    Me.ef_terminal_3gs_serial_number.Name = "ef_terminal_3gs_serial_number"
    Me.ef_terminal_3gs_serial_number.Size = New System.Drawing.Size(264, 24)
    Me.ef_terminal_3gs_serial_number.SufixText = "Sufix Text"
    Me.ef_terminal_3gs_serial_number.SufixTextVisible = True
    Me.ef_terminal_3gs_serial_number.TabIndex = 1
    Me.ef_terminal_3gs_serial_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_terminal_3gs_serial_number.TextValue = ""
    Me.ef_terminal_3gs_serial_number.TextWidth = 100
    Me.ef_terminal_3gs_serial_number.Value = ""
    '
    'gb_terminal_3gs
    '
    Me.gb_terminal_3gs.Controls.Add(Me.ef_terminal_3gs_serial_number)
    Me.gb_terminal_3gs.Controls.Add(Me.ef_terminal_3gs_vendor)
    Me.gb_terminal_3gs.Location = New System.Drawing.Point(291, 6)
    Me.gb_terminal_3gs.Name = "gb_terminal_3gs"
    Me.gb_terminal_3gs.Size = New System.Drawing.Size(279, 109)
    Me.gb_terminal_3gs.TabIndex = 2
    Me.gb_terminal_3gs.TabStop = False
    Me.gb_terminal_3gs.Text = "xTerminal3gs"
    '
    'btn_add
    '
    Me.btn_add.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_add.Location = New System.Drawing.Point(0, 105)
    Me.btn_add.Name = "btn_add"
    Me.btn_add.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_add.Size = New System.Drawing.Size(90, 30)
    Me.btn_add.TabIndex = 0
    Me.btn_add.ToolTipped = False
    Me.btn_add.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'dg_terminals_3gs
    '
    Me.dg_terminals_3gs.CaptionFont = New System.Drawing.Font("Verdana", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.dg_terminals_3gs.CaptionText = "x3gsTerminals"
    Me.dg_terminals_3gs.DataMember = ""
    Me.dg_terminals_3gs.Dock = System.Windows.Forms.DockStyle.Fill
    Me.dg_terminals_3gs.HeaderFont = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.dg_terminals_3gs.HeaderForeColor = System.Drawing.SystemColors.ControlText
    Me.dg_terminals_3gs.Location = New System.Drawing.Point(0, 177)
    Me.dg_terminals_3gs.Name = "dg_terminals_3gs"
    Me.dg_terminals_3gs.Size = New System.Drawing.Size(929, 379)
    Me.dg_terminals_3gs.TabIndex = 5
    '
    'dg_terminals_pending
    '
    Me.dg_terminals_pending.CaptionFont = New System.Drawing.Font("Verdana", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.dg_terminals_pending.CaptionText = "xPendingTerminals"
    Me.dg_terminals_pending.DataMember = ""
    Me.dg_terminals_pending.Dock = System.Windows.Forms.DockStyle.Top
    Me.dg_terminals_pending.HeaderFont = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.dg_terminals_pending.HeaderForeColor = System.Drawing.SystemColors.ControlText
    Me.dg_terminals_pending.Location = New System.Drawing.Point(0, 0)
    Me.dg_terminals_pending.Name = "dg_terminals_pending"
    Me.dg_terminals_pending.Size = New System.Drawing.Size(929, 167)
    Me.dg_terminals_pending.TabIndex = 4
    '
    'btn_replace
    '
    Me.btn_replace.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_replace.Location = New System.Drawing.Point(0, 138)
    Me.btn_replace.Name = "btn_replace"
    Me.btn_replace.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_replace.Size = New System.Drawing.Size(90, 30)
    Me.btn_replace.TabIndex = 9
    Me.btn_replace.ToolTipped = False
    Me.btn_replace.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'frm_terminals_3gs_edit
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(1025, 710)
    Me.Name = "frm_terminals_3gs_edit"
    Me.Text = "frm_terminals_3gs_edit"
    Me.panel_grids.ResumeLayout(False)
    Me.panel_buttons.ResumeLayout(False)
    Me.panel_filter.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_terminal_pending.ResumeLayout(False)
    Me.gb_terminal_3gs.ResumeLayout(False)
    CType(Me.dg_terminals_3gs, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.dg_terminals_pending, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)

  End Sub

#End Region ' Windows Form Designer generated code

#Region " Enums "

#End Region ' Enums

#Region " Members "

  Shared m_sql_conn As SqlConnection

  Private m_sql_adap_pending As SqlDataAdapter
  Private m_sql_dataset_pending As DataSet
  Private m_data_view_pending As DataView
  Private m_sql_commandbuilder_pending As SqlCommandBuilder

  Private m_sql_adap_3gs As SqlDataAdapter
  Private m_sql_dataset_3gs As DataSet
  Private m_data_view_3gs As DataView
  Private m_sql_commandbuilder_3gs As SqlCommandBuilder

  ' TODO: Should be read from a SQL Table, not already created.
  Private m_next_machine_number As Integer

  Private m_filter_collection As Collection

  Private m_relgames_added As Boolean
  Private m_terms_global_added As Boolean
  Private m_terms_3gs_added As Boolean

  ' For report filters 
  Private m_report_vendor_3G As String
  Private m_report_serial_3G As String
  Private m_report_vendor As String
  Private m_report_serial As String
  Private m_report_ignored As String

  ' Original DataGrid.RowHeaderWidth for 3gs.
  ' After RowError, RowHeaderWidth is increased. Use this value to restore it.
  Public m_original_row_header_width_3gs As Integer

  Private m_error_insert_terminal As String

#End Region ' Members

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_TERMINALS_3GS_EDIT
    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  'PURPOSE: Executed just before closing form.
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)

    '' Force to grid to lost focus
    '' to allow to detect changes on uncommited cells
    Me.ActiveControl = Nothing


    If DiscardChanges() Then
      CloseCanceled = False
    Else
      CloseCanceled = True
    End If

  End Sub 'GUI_Closing

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' - Form title
    Me.Text = GLB_NLS_GUI_AUDITOR.GetString(461)

    ' - Buttons
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_CONTROLS.GetString(13) ' 13 "Save"
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Text = GLB_NLS_GUI_AUDITOR.GetString(17)  ' 17 "Fill"
    btn_add.Text = GLB_NLS_GUI_AUDITOR.GetString(18)                                  ' 18 "Add"
    btn_replace.Text = GLB_NLS_GUI_AUDITOR.GetString(19)                              ' 19 "Replace"

    GUI_Button(ENUM_BUTTON.BUTTON_INFO).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Enabled = False
    GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Enabled = False
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = False   ' Hide Fill button
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = False
    GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False

    btn_add.Enabled = False
    btn_replace.Enabled = False

    dg_terminals_pending.CaptionText = GLB_NLS_GUI_AUDITOR.GetString(347)
    dg_terminals_3gs.CaptionText = GLB_NLS_GUI_AUDITOR.GetString(348)

    gb_terminal_pending.Text = GLB_NLS_GUI_AUDITOR.GetString(345)
    gb_terminal_3gs.Text = GLB_NLS_GUI_AUDITOR.GetString(346)
    ef_terminal_pending_vendor.Text = GLB_NLS_GUI_CONFIGURATION.GetString(205)
    ef_terminal_3gs_vendor.Text = GLB_NLS_GUI_CONFIGURATION.GetString(205)
    ef_terminal_pending_serial_number.Text = GLB_NLS_GUI_CONFIGURATION.GetString(214)
    ef_terminal_3gs_serial_number.Text = GLB_NLS_GUI_CONFIGURATION.GetString(214)
    chk_hide_ignored.Text = GLB_NLS_GUI_AUDITOR.GetString(353)
    cmb_source.Text = GLB_NLS_GUI_AUDITOR.GetString(349)

    ' Possible source for pending terminals.
    cmb_source.Add(0, GLB_NLS_GUI_AUDITOR.GetString(355))
    cmb_source.Add(1, GLB_NLS_GUI_AUDITOR.GetString(356))
    cmb_source.Add(2, GLB_NLS_GUI_AUDITOR.GetString(357))
    cmb_source.Add(3, GLB_NLS_GUI_AUDITOR.GetString(358))
    cmb_source.Add(4, GLB_NLS_GUI_AUDITOR.GetString(359))
    cmb_source.Add(5, GLB_NLS_GUI_AUDITOR.GetString(360))

    Call ef_terminal_pending_vendor.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    Call ef_terminal_pending_serial_number.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 40)
    Call ef_terminal_3gs_vendor.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    Call ef_terminal_3gs_serial_number.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 40)

    dg_terminals_pending.Enabled = False
    dg_terminals_3gs.Enabled = False

    m_original_row_header_width_3gs = dg_terminals_3gs.RowHeaderWidth

    m_next_machine_number = 100000

    m_filter_collection = New Collection()

  End Sub ' GUI_InitControls

  ' PURPOSE: Manage permissions.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)

  End Sub 'GUI_Permissions

  ' PURPOSE: Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = gb_terminal_pending

  End Sub 'GUI_SetInitialFocus

  ' PURPOSE: Manage buttons pressed.
  '
  '  PARAMS:
  '     - INPUT:
  '         - ButtonId: Id. of the button clicked.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        GUI_SaveChanges()

      Case ENUM_BUTTON.BUTTON_CUSTOM_1

      Case ENUM_BUTTON.BUTTON_CUSTOM_2
        GUI_FillEmptyMachineId()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_FilterReset()

    ef_terminal_pending_vendor.Value = ""
    ef_terminal_pending_serial_number.Value = ""
    ef_terminal_3gs_vendor.Value = ""
    ef_terminal_3gs_serial_number.Value = ""
    chk_hide_ignored.Checked = True
    cmb_source.Value = 0

  End Sub 'GUI_FilterReset

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted

  Protected Overrides Function GUI_FilterCheck() As Boolean

    Return True
  End Function 'GUI_FilterCheck

  ' PURPOSE: GUI execute query with filter checks and pending changes control
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ExecuteQuery()
    Dim user_msg As Boolean
    Dim data_table_changes_terminals_pending As New DataTable
    Dim data_table_changes_terminals_3gs As New DataTable

    Try
      ' Check the filter
      If GUI_FilterCheck() Then

        Windows.Forms.Cursor.Current = Cursors.WaitCursor

        ' If pending changes MsgBox
        user_msg = False
        If Not IsNothing(m_data_view_pending) Then
          data_table_changes_terminals_pending = m_data_view_pending.Table.GetChanges

          If Not IsNothing(data_table_changes_terminals_pending) Then
            If data_table_changes_terminals_pending.Rows.Count > 0 Then
              If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(122), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then
                Return
              End If
              user_msg = True
            End If
          End If
        End If

        If Not IsNothing(m_data_view_3gs) And Not user_msg Then
          data_table_changes_terminals_3gs = m_data_view_3gs.Table.GetChanges

          If Not IsNothing(data_table_changes_terminals_3gs) Then
            If data_table_changes_terminals_3gs.Rows.Count > 0 Then
              If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(122), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then
                Return
              End If
            End If
          End If
        End If

        ' Invalidate datagrid
        GUI_ClearGrid()

        ' Execute the query        
        Call ExecuteQuery()

        ' For report filters
        Call GUI_ReportUpdateFilters(Me.dg_terminals_3gs)

        GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = Me.Permissions.Write

        btn_add.Enabled = (m_data_view_pending.Count > 0 And Me.Permissions.Write)
        btn_replace.Enabled = (m_data_view_pending.Count > 0 And Me.Permissions.Write)

        GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = m_data_view_3gs.Count > 0 _
                                                      And Me.Permissions.Write

        GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Enabled = m_data_view_3gs.Count > 0 And CurrentUser.Permissions(ENUM_FORM.FORM_PRINT_ENABLED).Read
        GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Enabled = m_data_view_3gs.Count > 0 And CurrentUser.Permissions(ENUM_FORM.FORM_EXCEL_ENABLED).Read

      End If

    Catch exception As Exception
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "Terminals3gsEdit", _
                            "GUI_ExecuteQuery", _
                            "", _
                            "", _
                            "", _
                            exception.Message)

    Finally
      Windows.Forms.Cursor.Current = Cursors.Default
    End Try

  End Sub 'GUI_ExecuteQuery

#End Region ' Overrides

#Region " Private Functions "

  ' PURPOSE: Save the changes made in the DataViews (pending and 3gs terminals dataviews).
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Private Function GUI_SaveChanges() As Boolean

    Dim data_table_changes_terminals_pending As New DataTable
    Dim data_table_changes_terminals_3gs As New DataTable
    Dim dt_pending_orig As DataTable = Nothing
    Dim dt_3gs_orig As DataTable = Nothing
    Dim trx As SqlTransaction = Nothing
    Dim flag_error As Boolean
    Dim trx_close As Boolean
    Dim msg_error As String
    Dim rows_3gs_orig() As DataRow = Nothing
    Dim rows_3gs_modif() As DataRow

    msg_error = ""
    flag_error = False
    trx_close = False

    m_relgames_added = False
    m_terms_global_added = False
    m_terms_3gs_added = False

    Try
      If Not IsNothing(m_data_view_pending) Then
        data_table_changes_terminals_pending = m_data_view_pending.Table.GetChanges
      End If

      If Not IsNothing(m_data_view_3gs) Then
        Call TrimDataValues(m_sql_dataset_3gs.Tables(0).Select("", "", DataViewRowState.ModifiedCurrent Or DataViewRowState.Added))
        data_table_changes_terminals_3gs = m_data_view_3gs.Table.GetChanges
      End If

      ' Nothing has changed... exit!
      If IsNothing(data_table_changes_terminals_pending) _
         And IsNothing(data_table_changes_terminals_3gs) Then
        Return False
      End If

      If Not GUI_IsDataChangedOk() Then
        Return False
      End If

      ' RCI 14-JUN-2010: Calculate the Virtual Vendor-SerialNumber for TERMINALS.TE_EXTERNAL_ID.
      Call ProcessVirtualVendorSerialNumber()

      '
      ' UPDATE 
      '

      ' Use only one transaction, because the SqlConnection is the same and it doesn't support parallel transactions.
      trx = m_sql_adap_pending.SelectCommand.Connection.BeginTransaction

      m_sql_adap_pending.DeleteCommand.Transaction = trx
      m_sql_adap_pending.UpdateCommand.Transaction = trx
      m_sql_adap_pending.ContinueUpdateOnError = True

      m_sql_adap_3gs.UpdateCommand.Transaction = trx
      m_sql_adap_3gs.InsertCommand.Transaction = trx
      m_sql_adap_3gs.ContinueUpdateOnError = True

      ' TERMINALS_PENDING

      ' Copy DataTable with originals values
      dt_pending_orig = m_sql_dataset_pending.Tables(0).Copy()

      ' Deleted rows will be processed after add rows in TERMINALS_3GS

      ' Modified rows
      If Not ExecuteAdapterUpdate(TYPE_PENDING, DataViewRowState.ModifiedCurrent, msg_error) Then
        flag_error = True

        Return False
      End If

      ' Copy DataTable with originals values
      dt_3gs_orig = m_sql_dataset_3gs.Tables(0).Copy()
      rows_3gs_orig = dt_3gs_orig.Select(m_data_view_3gs.RowFilter, "", _
                                         DataViewRowState.ModifiedCurrent Or DataViewRowState.Added)

      ' TERMINALS

      ' Modified rows
      If Not UpdateTerminals(trx, msg_error) Then
        flag_error = True

        Return False
      End If

      ' Added rows
      If Not InsertNewTerminals(trx, msg_error) Then
        flag_error = True

        Return False
      Else
        If Not IsNothing(data_table_changes_terminals_3gs) Then
          ' update datatable data_table_changes_terminals_3gs ID's after the insert
          For Each _row As DataRow In data_table_changes_terminals_3gs.Rows
            For Each _row_view As DataRow In m_data_view_3gs.Table.Rows
              If _row.Item("TE_NAME") = _row_view.Item("TE_NAME") Then
                _row.Item("T3GS_TERMINAL_ID") = _row_view.Item("T3GS_TERMINAL_ID")
              End If
            Next
          Next
        End If
      End If

      ' GAMES
      If Not UpdateDataGame(trx, msg_error) Then
        flag_error = True

        Return False
      End If

      ' TERMINALS_3GS
      ' This must be done at the end of the SaveChanges process

      ' Modified rows
      If Not ExecuteAdapterUpdate(TYPE_3GS, DataViewRowState.ModifiedCurrent, msg_error) Then
        flag_error = True

        Return False
      End If

      ' Added rows
      If Not ExecuteAdapterUpdate(TYPE_3GS, DataViewRowState.Added, msg_error) Then
        flag_error = True

        Return False
      End If

      ' TERMINALS_PENDING
      ' Deleted rows
      If Not ExecuteAdapterUpdate(TYPE_PENDING, DataViewRowState.Deleted, msg_error) Then
        flag_error = True

        Return False
      End If

      trx.Commit()
      trx_close = True

      ' Auditory
      GUI_WriteAuditoryChangesPending(data_table_changes_terminals_pending)
      GUI_WriteAuditoryChanges3gs(data_table_changes_terminals_3gs)
      GUI_WriteAuditoryChangesGames(data_table_changes_terminals_3gs)

      m_sql_dataset_pending.AcceptChanges()
      m_sql_dataset_3gs.AcceptChanges()

      ' LEM & RCI 2014-10-13: Refresh data for TerminalReport module on new terminal creation
      Call TerminalReport.ForceRefresh()

      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(108), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO)

      Return True

    Catch ex As Exception
      If Not trx Is Nothing Then
        trx.Rollback()
      End If

      trx_close = True
      flag_error = True

      'TODO: Do nothing with m_sql_datasets... 
      'm_sql_dataset_pending.RejectChanges()
      'm_sql_dataset_3gs.RejectChanges()
      Debug.WriteLine(ex.Message)

      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ex.Message)

      Return False

    Finally

      If flag_error Then

        If Not dt_pending_orig Is Nothing Then
          m_sql_dataset_pending.Tables.RemoveAt(0)
          m_sql_dataset_pending.Tables.Add(dt_pending_orig)
          m_data_view_pending.Table = m_sql_dataset_pending.Tables(0)
        End If

        If Not dt_3gs_orig Is Nothing Then
          Dim row_filter As String
          Dim row_sort As String
          Dim idx_row As Integer
          Dim idx_modif As Integer

          rows_3gs_modif = m_sql_dataset_3gs.Tables(0).Select(m_data_view_3gs.RowFilter, "", _
                                                              DataViewRowState.ModifiedCurrent Or DataViewRowState.Added)

          ' Copy back errors from rows_3gs_modif to rows_3gs_orig.
          If rows_3gs_modif.Length > 0 Then
            For idx_row = 0 To rows_3gs_orig.Length - 1

              For idx_modif = 0 To rows_3gs_modif.Length - 1
                If rows_3gs_modif(idx_modif).Item("TE_NAME") = rows_3gs_orig(idx_row).Item("TE_NAME") Then

                  Exit For
                End If
              Next

              If idx_modif = rows_3gs_modif.Length Then
                Exit For
              End If

              rows_3gs_orig(idx_row).RowError = rows_3gs_modif(idx_modif).RowError
            Next
          End If

          row_filter = m_data_view_3gs.RowFilter
          row_sort = m_data_view_3gs.Sort
          m_sql_dataset_3gs.Tables.RemoveAt(0)

          m_sql_dataset_3gs.Tables.Add(dt_3gs_orig)
          m_data_view_3gs.Table = m_sql_dataset_3gs.Tables(0)
          m_data_view_3gs.RowFilter = row_filter
          m_data_view_3gs.Sort = row_sort
        End If
      End If

      If Not trx Is Nothing Then
        If Not trx_close Then
          trx.Rollback()
        End If
      End If

      ' Restore original RowHeaderWidth.
      If dg_terminals_3gs.TableStyles.Count > 0 Then
        dg_terminals_3gs.TableStyles(0).RowHeaderWidth = m_original_row_header_width_3gs
      End If

      If msg_error <> "" Then
        If m_error_insert_terminal <> "" Then
          Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(471), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , m_error_insert_terminal)
        Else
          Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , msg_error)
        End If
      End If

    End Try

  End Function ' GUI_SaveChanges

  ' PURPOSE: Check if data in the dataviews is ok.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - True:  Data is ok.
  '     - False: Data is NOT ok.

  Private Function GUI_IsDataChangedOk() As Boolean

    Dim total_rows() As DataRow
    Dim row As DataRow
    Dim nls_string As String
    Dim _regex As RegularExpressions.Regex

    total_rows = m_sql_dataset_3gs.Tables(0).Select("", "", DataViewRowState.ModifiedCurrent Or DataViewRowState.Added)
    _regex = New RegularExpressions.Regex("[\x00-\x1f]") 'Non printable characters

    For Each row In total_rows
      row.ClearErrors()
      ' If there is no filter, don't add it.
      If m_data_view_3gs.RowFilter <> "" Then
        ' Add new or modified values to data_view_3gs.RowFilter
        AddFilter3gs(row)
      End If
    Next

    For Each row In total_rows

      ' Null values are not allowed
      If row.Item("T3GS_VENDOR_ID") Is DBNull.Value _
      OrElse row.Item("T3GS_VENDOR_ID") = "" Then
        ' 114 "Field %1 can't be empty"
        nls_string = NLS_GetString(GLB_NLS_GUI_AUDITOR.Id(114), GLB_NLS_GUI_CONFIGURATION.GetString(205))
        AssignRowError(row, nls_string)
        Call NLS_MountedMsgBox(GLB_NLS_GUI_AUDITOR.Id(114), nls_string, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

        Return False

      ElseIf _regex.IsMatch(row.Item("T3GS_VENDOR_ID")) Then
        nls_string = NLS_GetString(GLB_NLS_GUI_AUDITOR.Id(131), GLB_NLS_GUI_CONFIGURATION.GetString(205))
        AssignRowError(row, nls_string)
        Call NLS_MountedMsgBox(GLB_NLS_GUI_AUDITOR.Id(131), nls_string, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

        Return False
      End If

      If row.Item("T3GS_SERIAL_NUMBER") Is DBNull.Value _
      OrElse row.Item("T3GS_SERIAL_NUMBER") = "" Then
        ' 114 "Field %1 can't be empty"
        nls_string = NLS_GetString(GLB_NLS_GUI_AUDITOR.Id(114), GLB_NLS_GUI_CONFIGURATION.GetString(214))
        AssignRowError(row, nls_string)
        Call NLS_MountedMsgBox(GLB_NLS_GUI_AUDITOR.Id(114), nls_string, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

        Return False

      ElseIf _regex.IsMatch(row.Item("T3GS_SERIAL_NUMBER")) Then
        nls_string = NLS_GetString(GLB_NLS_GUI_AUDITOR.Id(131), GLB_NLS_GUI_CONFIGURATION.GetString(214))
        AssignRowError(row, nls_string)
        Call NLS_MountedMsgBox(GLB_NLS_GUI_AUDITOR.Id(131), nls_string, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

        Return False
      End If

      If row.Item("T3GS_MACHINE_NUMBER") Is DBNull.Value Then
        ' 114 "Field %1 can't be empty"
        nls_string = NLS_GetString(GLB_NLS_GUI_AUDITOR.Id(114), GLB_NLS_GUI_CONFIGURATION.GetString(211))
        AssignRowError(row, nls_string)
        Call NLS_MountedMsgBox(GLB_NLS_GUI_AUDITOR.Id(114), nls_string, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

        Return False
      End If

      If row.Item("TE_PROVIDER_ID") Is DBNull.Value _
      OrElse row.Item("TE_PROVIDER_ID") = "" Then
        ' 114 "Field %1 can't be empty"
        nls_string = NLS_GetString(GLB_NLS_GUI_AUDITOR.Id(114), GLB_NLS_GUI_CONFIGURATION.GetString(469))
        AssignRowError(row, nls_string)
        Call NLS_MountedMsgBox(GLB_NLS_GUI_AUDITOR.Id(114), nls_string, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

        Return False
      End If

      If row.Item("TE_NAME") Is DBNull.Value _
      OrElse row.Item("TE_NAME") = "" Then
        ' 114 "Field %1 can't be empty"
        nls_string = NLS_GetString(GLB_NLS_GUI_AUDITOR.Id(114), GLB_NLS_GUI_CONFIGURATION.GetString(210))
        AssignRowError(row, nls_string)
        Call NLS_MountedMsgBox(GLB_NLS_GUI_AUDITOR.Id(114), nls_string, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

        Return False
      ElseIf _regex.IsMatch(row.Item("TE_NAME")) Then
        nls_string = NLS_GetString(GLB_NLS_GUI_AUDITOR.Id(131), GLB_NLS_GUI_CONFIGURATION.GetString(210))
        AssignRowError(row, nls_string)
        Call NLS_MountedMsgBox(GLB_NLS_GUI_AUDITOR.Id(131), nls_string, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

        Return False
      End If

      If row.Item("GAME_NAME") Is DBNull.Value _
      OrElse row.Item("GAME_NAME") = "" Then
        ' 114 "Field %1 can't be empty"
        nls_string = NLS_GetString(GLB_NLS_GUI_AUDITOR.Id(114), GLB_NLS_GUI_CONTROLS.GetString(280))
        AssignRowError(row, nls_string)
        Call NLS_MountedMsgBox(GLB_NLS_GUI_AUDITOR.Id(114), nls_string, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

        Return False
      End If

      If row.Item("T3GS_MACHINE_NUMBER") <= 0 Then
        nls_string = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(125), GLB_NLS_GUI_CONFIGURATION.GetString(211))
        AssignRowError(row, nls_string)
        Call NLS_MountedMsgBox(GLB_NLS_GUI_CONFIGURATION.Id(125), nls_string, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

        Return False
      End If

      ' FLAG_REPLACEMENT is only set when replacing a terminal. Otherwise is NULL.
      If row.Item("FLAG_REPLACEMENT") Is DBNull.Value Then
        ' Set FLAG_REPLACEMENT = 0  =>  NOT replacing a terminal 
        m_sql_dataset_3gs.Tables(0).Columns("FLAG_REPLACEMENT").ReadOnly = False
        row.Item("FLAG_REPLACEMENT") = 0
        m_sql_dataset_3gs.Tables(0).Columns("FLAG_REPLACEMENT").ReadOnly = True
      End If

      ' FLAG_REPLACEMENT_RETIRED is only set when replacing a retired terminal. Otherwise is NULL.
      If row.Item("FLAG_REPLACEMENT_RETIRED") Is DBNull.Value Then
        ' Set FLAG_REPLACEMENT_RETIRED = 0  =>  NOT replacing a retired terminal => Not to insert into TERMINALS_3GS 
        m_sql_dataset_3gs.Tables(0).Columns("FLAG_REPLACEMENT_RETIRED").ReadOnly = False
        row.Item("FLAG_REPLACEMENT_RETIRED") = 0
        m_sql_dataset_3gs.Tables(0).Columns("FLAG_REPLACEMENT_RETIRED").ReadOnly = True
      End If

      ' FLAG_IS_NULL_TE_EXTERNAL_ID is set to 0 by default for all terminals read from DB. 
      ' It must be set to 1 when the terminal 
      '     - Was manually entered or manually added from the pending list :  NULL => 1
      '     - Was loaded from the DB and TE_EXTERNAL_ID is NULL :             NULL => 1

      If row.Item("TE_EXTERNAL_ID") Is DBNull.Value Then
        m_sql_dataset_3gs.Tables(0).Columns("FLAG_IS_NULL_TE_EXTERNAL_ID").ReadOnly = False
        row.Item("FLAG_IS_NULL_TE_EXTERNAL_ID") = 1
        m_sql_dataset_3gs.Tables(0).Columns("FLAG_IS_NULL_TE_EXTERNAL_ID").ReadOnly = True
      Else
        m_sql_dataset_3gs.Tables(0).Columns("FLAG_IS_NULL_TE_EXTERNAL_ID").ReadOnly = False
        row.Item("FLAG_IS_NULL_TE_EXTERNAL_ID") = 0
        m_sql_dataset_3gs.Tables(0).Columns("FLAG_IS_NULL_TE_EXTERNAL_ID").ReadOnly = True
      End If

      If Not CheckDataRow(row, False) Then
        Return False
      End If

    Next

    Return True
  End Function ' GUI_IsDataChangedOk

  ' PURPOSE: Set RowError for the Row. Also restore the RowHeaderWidth for the 3gs DataGrid.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row As DataRow
  '           - MsgError As String
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     -
  Private Sub AssignRowError(ByVal Row As DataRow, ByVal MsgError As String)
    Row.RowError = MsgError
    ' Restore original RowHeaderWidth.
    If dg_terminals_3gs.TableStyles.Count > 0 Then
      dg_terminals_3gs.TableStyles(0).RowHeaderWidth = m_original_row_header_width_3gs
    End If
  End Sub ' AssignRowError

  ' PURPOSE: Calculate field Virtual VENDOR_ID - SERIAL_NUMBER for added rows.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Private Sub ProcessVirtualVendorSerialNumber()
    Dim total_rows() As DataRow
    Dim row As DataRow

    total_rows = m_sql_dataset_3gs.Tables(0).Select("", "", DataViewRowState.ModifiedCurrent Or DataViewRowState.Added)

    m_sql_dataset_3gs.Tables(0).Columns("TE_EXTERNAL_ID").ReadOnly = False

    For Each row In total_rows
      row.Item("TE_EXTERNAL_ID") = row.Item("T3GS_VENDOR_ID") & STRING_CONCAT_VENDOR_SN & row.Item("T3GS_SERIAL_NUMBER")
    Next

    m_sql_dataset_3gs.Tables(0).Columns("TE_EXTERNAL_ID").ReadOnly = True

  End Sub ' ProcessVirtualVendorSerialNumber

  ' PURPOSE: Add a filter (if not previously added) to the 3gs data view to be able to show new and modified values
  '
  '  PARAMS:
  '     - INPUT: Row As DataRow
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Private Sub AddFilter3gs(ByVal Row As DataRow)
    Dim str_filter As String
    Dim _vendor_id As String
    Dim _serial_number As String

    _vendor_id = Row.Item("T3GS_VENDOR_ID")
    _serial_number = Row.Item("T3GS_SERIAL_NUMBER")

    str_filter = _vendor_id & " - " & _serial_number
    If Me.m_filter_collection.Contains(str_filter) Then
      Return
    End If
    m_filter_collection.Add(str_filter, str_filter)

    m_data_view_3gs.RowFilter = m_data_view_3gs.RowFilter & _
           " OR (    T3GS_VENDOR_ID = '" & _vendor_id.Replace("'", "''") & "'" & _
               " AND T3GS_SERIAL_NUMBER = '" & _serial_number.Replace("'", "''") & "' )"

  End Sub

  ' PURPOSE: GUI Assign a pending terminal to a 3GS provider: control pending changes
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - True:  Assignation was ok
  '     - False: Assignation was NOT ok

  Private Function GUI_AssignPendingTerminal() As Boolean

    Dim data_table_changes_terminals_pending As New DataTable
    Dim data_table_changes_terminals_3gs As New DataTable
    Dim rc As Boolean

    Try
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      ' Execute assign        
      rc = AssignPendingTerminal()

      btn_add.Enabled = (m_data_view_pending.Count > 0 And Me.Permissions.Write)
      btn_replace.Enabled = (m_data_view_pending.Count > 0 And Me.Permissions.Write)

      GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = m_data_view_3gs.Count > 0 _
                                                    And Me.Permissions.Write

      Return rc

    Catch ex As Exception
      Debug.WriteLine(ex.Message)
      ' 113 "Error managing terminal: \n%1"
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(113), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ex.Message)

      Return False

    Finally
      Windows.Forms.Cursor.Current = Cursors.Default
    End Try

  End Function 'GUI_AssignPendingTerminal

  ' PURPOSE: Replaces an existing terminal with a pending terminal
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - True:  Change was ok
  '     - False: Change was NOT ok

  Private Function GUI_ReplacePendingTerminal() As Boolean

    Try
      ReplacePendingTerminal()

      btn_add.Enabled = (m_data_view_pending.Count > 0 And Me.Permissions.Write)
      btn_replace.Enabled = (m_data_view_pending.Count > 0 And Me.Permissions.Write)
      GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = (m_data_view_3gs.Count > 0 And Me.Permissions.Write)

    Catch ex As Exception
      Debug.WriteLine(ex.Message)

      ' 113 "Error managing terminal: \n%1"
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(113), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ex.Message)

      Return False

    Finally
      Windows.Forms.Cursor.Current = Cursors.Default
    End Try

  End Function ' GUI_ReplacePendingTerminal

  ' PURPOSE: Assign a pending terminal to a 3GS provider in memory
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - True:  Assignation was ok
  '     - False: Assignation was NOT ok

  Private Function AssignPendingTerminal() As Boolean

    Dim row_pending As DataRowView
    Dim new_row As DataRowView
    Dim idx_row As Integer
    Dim select_idx As Collection
    Dim has_errors As Boolean

    select_idx = New Collection()
    has_errors = False

    Try
      ' Need to save selected index before process them, if not, the selected property for all indexes is deleted after the
      ' first index processed. Also save them in reverse order (highest to lowest), because data view is modified while
      ' processing.
      For idx_row = m_data_view_pending.Count - 1 To 0 Step -1
        If dg_terminals_pending.IsSelected(idx_row) Then
          select_idx.Add(idx_row)
        End If
      Next

      If select_idx.Count = 0 Then
        ' 118 "You must select at least one terminal."
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

        Return True
      End If

      If Not Me.Permissions.Write Then
        Return True
      End If

      m_data_view_3gs.AllowNew = Me.Permissions.Write

      For Each idx_row In select_idx
        row_pending = m_data_view_pending(idx_row)

        new_row = m_data_view_3gs.AddNew()
        new_row.Item("T3GS_VENDOR_ID") = row_pending.Item("TP_VENDOR_ID")
        new_row.Item("T3GS_SERIAL_NUMBER") = row_pending.Item("TP_SERIAL_NUMBER")
        new_row.Item("T3GS_MACHINE_NUMBER") = row_pending.Item("TP_MACHINE_NUMBER")
        new_row.Item("T3GS_TERMINAL_ID") = -1
        new_row.Item("TP_ID") = row_pending.Item("TP_ID")

        If CheckDataRow(new_row.Row, True) Then
          new_row.EndEdit()

          ' RCI 04-AUG-2010: Row can have data that is not in the filter and then
          ' it will be not shown as it is supposed. Add to the filter to show the new row (only if RowFilter is used).
          If m_data_view_3gs.RowFilter <> "" Then
            AddFilter3gs(new_row.Row)
          End If

          m_data_view_pending.AllowDelete = True
          row_pending.Delete()
          row_pending.EndEdit()
        Else
          has_errors = True
          new_row.CancelEdit()
        End If
      Next

      Return Not has_errors

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)
      has_errors = True

      Return False

    Finally
      m_data_view_pending.AllowDelete = False
      ' m_data_view_terminals.AllowNew = False ???

      If has_errors Then
        ' 117 "Could not add all the terminals."
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(117), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)
      End If
    End Try

  End Function ' AssignPendingTerminal

  ' PURPOSE : Replaces an existing terminal with a pending terminal
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - True:  Assignation was ok
  '     - False: Assignation was NOT ok

  Private Function ReplacePendingTerminal() As Boolean

    Dim row_pending As DataRowView
    Dim idx_row As Integer
    Dim has_errors As Boolean
    Dim has_selected_row As Boolean
    Dim _selected_terminal As DataRow()
    Dim _terminal_row As DataRow
    Dim frm_sel As frm_terminals
    Dim _terminal_id_selected As Integer
    Dim _sel_params As TYPE_TERMINAL_SEL_PARAMS
    Dim _aux_terminal As CLASS_TERMINAL
    Dim _game_id As Integer
    Dim _game_name As String

    has_errors = False
    has_selected_row = False

    For idx_row = m_data_view_pending.Count - 1 To 0 Step -1
      If dg_terminals_pending.IsSelected(idx_row) Then
        has_selected_row = True

        Exit For    ' Process only first selected row
      End If
    Next

    If Not has_selected_row Then
      ' 118 "You must select at least one terminal."
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

      Return True
    End If

    frm_sel = New frm_terminals()

    _sel_params = New TYPE_TERMINAL_SEL_PARAMS

    _sel_params.FilterTypeDefault = False
    _sel_params.FilterTypeEnable(0) = False    ' Disable all 
    ' Check the 3GS and don't allow unselection
    _sel_params.FilterTypeValue(WSI.Common.TerminalTypes.T3GS) = True
    _sel_params.FilterTypeEnable(WSI.Common.TerminalTypes.T3GS) = True
    _sel_params.FilterTypeLocked(WSI.Common.TerminalTypes.T3GS) = True

    _terminal_id_selected = frm_sel.ShowForSelect(_sel_params)

    If _terminal_id_selected = 0 Then
      ' 118 "You must select at least one terminal."
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

      Return True
      'If terminal_id is equals to -1 means that te user just close frm_terminals without doing nothing
    ElseIf _terminal_id_selected = -1 Then
      Return True
    End If

    m_data_view_3gs.AllowNew = Me.Permissions.Write

    Try
      row_pending = m_data_view_pending(idx_row)

      ' Search in the 3GS terminals list the selected 3Gs terminal to bereplace by the selected pending terminal 
      _selected_terminal = m_sql_dataset_3gs.Tables(0).Select("T3GS_TERMINAL_ID = " & _terminal_id_selected)

      _aux_terminal = Nothing
      _game_id = 0
      _game_name = ""

      If _selected_terminal.Length < 1 Then
        ' The 3GS Terminal to be replaced may not exist in the table if it was retired.
        _aux_terminal = New CLASS_TERMINAL
        If _aux_terminal.DB_Read(_terminal_id_selected, 0) <> ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then
          ' 118 "You must select at least one terminal."
          Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

          Return True
        End If

        _aux_terminal.TerminalGame(_game_id, _game_name)
      End If

      If Not _aux_terminal Is Nothing Then
        ' The 3GS Terminal to be replaced does not exist in table TERMINALS_3GS
        _terminal_row = m_sql_dataset_3gs.Tables(0).NewRow()
        _terminal_row.Item("T3GS_VENDOR_ID") = ""
        _terminal_row.Item("T3GS_SERIAL_NUMBER") = ""
        _terminal_row.Item("T3GS_MACHINE_NUMBER") = 0
        _terminal_row.Item("T3GS_TERMINAL_ID") = _aux_terminal.TerminalId
        _terminal_row.Item("TE_PROVIDER_ID") = _aux_terminal.ProviderId
        _terminal_row.Item("TE_NAME") = _aux_terminal.Name
        _terminal_row.Item("TE_EXTERNAL_ID") = _aux_terminal.ExternalId
        _terminal_row.Item("GAME_NAME") = _game_name
        _terminal_row.Item("GAME_ID") = _game_id
        _terminal_row.Item("GAME_3GS_ID") = -1
        _terminal_row.Item("FLAG_REPLACEMENT") = 1            ' Flag to allow setting replaced terminals as active
        _terminal_row.Item("FLAG_REPLACEMENT_RETIRED") = 1    ' Flag to allow inserting the terminals to TERMINALS_3GS table
        ' Note that FLAG_IS_NULL_TE_EXTERNAL_ID is always set in GUI_IsDataChangedOk()

        m_sql_dataset_3gs.Tables(0).Rows.Add(_terminal_row)
      Else
        ' The 3GS Terminal to be replaced already exists in table TERMINALS_3GS
        _terminal_row = _selected_terminal(0)

        _selected_terminal(0).Table.Columns("FLAG_REPLACEMENT").ReadOnly = False
        _terminal_row.Item("FLAG_REPLACEMENT") = 1            ' Flag to allow setting replaced terminals as active
        _selected_terminal(0).Table.Columns("FLAG_REPLACEMENT").ReadOnly = True
      End If

      m_sql_dataset_3gs.Tables(0).Columns("T3GS_VENDOR_ID").ReadOnly = False
      _terminal_row.Item("T3GS_VENDOR_ID") = row_pending.Item("TP_VENDOR_ID")

      m_sql_dataset_3gs.Tables(0).Columns("T3GS_SERIAL_NUMBER").ReadOnly = False
      _terminal_row.Item("T3GS_SERIAL_NUMBER") = row_pending.Item("TP_SERIAL_NUMBER")

      m_sql_dataset_3gs.Tables(0).Columns("T3GS_MACHINE_NUMBER").ReadOnly = False
      _terminal_row.Item("T3GS_MACHINE_NUMBER") = row_pending.Item("TP_MACHINE_NUMBER")

      ' Field TP_ID is necessary for undelete operation of pending terminals. See routine UndeleteAssignedRow().
      m_sql_dataset_3gs.Tables(0).Columns("TP_ID").ReadOnly = False
      _terminal_row.Item("TP_ID") = row_pending.Item("TP_ID")

      ' Delete the Pending terminal
      m_data_view_pending.AllowDelete = True
      row_pending.Delete()
      row_pending.EndEdit()

      Return Not has_errors

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)
      has_errors = True

      Return False

    Finally
      m_data_view_pending.AllowDelete = False
      ' m_data_view_3gs.AllowNew = False

      If has_errors Then
        ' 120 "Could not replace all the terminals."
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(120), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)
      End If
    End Try

  End Function ' ReplacePendingTerminal

  ' PURPOSE: Check Data Row for constraints.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row As DataRow: Row to check.
  '           - IsPendingRow As Boolean: Indicates if the checked row come from Pending Table.
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function CheckDataRow(ByVal Row As DataRow, ByVal IsPendingRow As Boolean) As Boolean
    Dim data_view As DataView
    Dim row_filter As String
    Dim count_plus As Integer
    Dim nls_string As String
    Dim _terminal_name As String
    Dim _vendor_id As String
    Dim _serial_number As String
    Dim _machine_number As String

    count_plus = 0
    If IsPendingRow Then
      count_plus = 1
    End If

    data_view = New DataView(Me.m_sql_dataset_3gs.Tables(0))

    _terminal_name = Row.Item("TE_NAME")
    _vendor_id = Row.Item("T3GS_VENDOR_ID")
    _serial_number = Row.Item("T3GS_SERIAL_NUMBER")
    _machine_number = Row.Item("T3GS_MACHINE_NUMBER")

    ' Check couple VENDOR_ID / SERIAL_NUMBER
    row_filter = "T3GS_VENDOR_ID = '" & _vendor_id.Replace("'", "''") & "'" & _
            " AND T3GS_SERIAL_NUMBER = '" & _serial_number.Replace("'", "''") & "'"
    data_view.RowFilter = row_filter

    If data_view.Count + count_plus > 1 Then
      If Not IsPendingRow Then
        ' 115 "The couple %1 / %2 already exists.\nThe couple %3 / %4 must be unique."
        nls_string = NLS_GetString(GLB_NLS_GUI_AUDITOR.Id(115), _
                                   _vendor_id, _serial_number, _
                                   GLB_NLS_GUI_CONFIGURATION.GetString(205), _
                                   GLB_NLS_GUI_CONFIGURATION.GetString(214))
        AssignRowError(Row, nls_string)
        Call NLS_MountedMsgBox(GLB_NLS_GUI_AUDITOR.Id(115), nls_string, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)
      End If
      Return False
    End If

    ' Check couple VENDOR_ID / MACHINE_NUMBER
    row_filter = "T3GS_VENDOR_ID = '" & _vendor_id.Replace("'", "''") & "'" & _
            " AND T3GS_MACHINE_NUMBER = " & _machine_number.Replace("'", "''")
    data_view.RowFilter = row_filter

    If data_view.Count + count_plus > 1 Then
      If IsPendingRow Then
        Row.Item("T3GS_MACHINE_NUMBER") = DBNull.Value
      Else
        ' 115 "The couple %1 / %2 already exists.\nThe couple %3 / %4 must be unique."
        nls_string = NLS_GetString(GLB_NLS_GUI_AUDITOR.Id(115), _
                                   _vendor_id, _machine_number, _
                                   GLB_NLS_GUI_CONFIGURATION.GetString(205), _
                                   GLB_NLS_GUI_CONFIGURATION.GetString(211))
        AssignRowError(Row, nls_string)
        Call NLS_MountedMsgBox(GLB_NLS_GUI_AUDITOR.Id(115), nls_string, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)
        Return False
      End If
    End If

    ' Pending Rows have not TE_NAME, don't check it for them.
    If Not IsPendingRow Then
      row_filter = "TE_NAME = '" & _terminal_name.Replace("'", "''") & "'"
      data_view.RowFilter = row_filter

      If data_view.Count > 1 Then
        nls_string = NLS_GetString(GLB_NLS_GUI_AUDITOR.Id(116), _
                                   _terminal_name, GLB_NLS_GUI_CONFIGURATION.GetString(210))
        AssignRowError(Row, nls_string)
        Call NLS_MountedMsgBox(GLB_NLS_GUI_AUDITOR.Id(116), nls_string, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)
        Return False
      End If
    End If

    Return True
  End Function

  ' PURPOSE: Fill empty values for machine id in dataview terminals 3gs
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GUI_FillEmptyMachineId() As Boolean
    Dim idx_row As Integer

    For idx_row = 0 To m_data_view_3gs.Count - 1
      If m_data_view_3gs(idx_row).Item("T3GS_MACHINE_NUMBER") Is DBNull.Value Then

        m_data_view_3gs(idx_row).Item("T3GS_MACHINE_NUMBER") = m_next_machine_number
        m_next_machine_number = m_next_machine_number + 1
      End If
    Next

    m_data_view_3gs.EndInit()

    Return True
  End Function ' GUI_FillEmptyMachineId

  ' PURPOSE: Initialize data in the grids
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub GUI_ClearGrid()

    dg_terminals_pending.DataSource = Nothing
    dg_terminals_3gs.DataSource = Nothing

  End Sub 'GUI_ClearGrid

  ' PURPOSE: Create Data Grids with SQL queries, and show generated info in screen.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub ExecuteQuery()

    Dim _table_style As DataGridTableStyle
    Dim sql_query_pending As StringBuilder
    Dim sql_query_3gs As StringBuilder
    Dim row_filter As StringBuilder
    Dim str_filter As StringBuilder

    ' Initialize DataView for database
    Try
      m_sql_conn = Nothing
      m_sql_conn = NewSQLConnection()

      If Not (m_sql_conn Is Nothing) Then
        ' Connected
        If m_sql_conn.State <> ConnectionState.Open Then
          ' Connecting ..
          m_sql_conn.Open()
        End If

        If Not m_sql_conn.State = ConnectionState.Open Then
          Exit Try
        End If
      End If

      m_sql_dataset_pending = New DataSet()
      m_sql_dataset_3gs = New DataSet()

      ' Build Query -------------------------------------------------

      '
      ' TERMINALS_PENDING
      '

      sql_query_pending = New StringBuilder()

      sql_query_pending.AppendLine(" SELECT   TP_ID             ")
      sql_query_pending.AppendLine("        , TP_SOURCE         ")
      sql_query_pending.AppendLine("        , TP_VENDOR_ID      ")
      sql_query_pending.AppendLine("        , TP_SERIAL_NUMBER  ")
      sql_query_pending.AppendLine("        , TP_MACHINE_NUMBER ")
      sql_query_pending.AppendLine("        , TP_REPORTED       ")
      sql_query_pending.AppendLine("        , TP_IGNORE         ")
      sql_query_pending.AppendLine("   FROM   TERMINALS_PENDING ")
      sql_query_pending.AppendLine("  WHERE   TP_SOURCE = 0     ")

      If Len(ef_terminal_pending_vendor.Value) > 0 Then
        sql_query_pending.AppendLine(" AND " & GUI_FilterField("TP_VENDOR_ID", ef_terminal_pending_vendor.Value))
      End If

      If Len(ef_terminal_pending_serial_number.Value) > 0 Then
        sql_query_pending.AppendLine(" AND " & GUI_FilterField("TP_SERIAL_NUMBER", ef_terminal_pending_serial_number.Value, False, False, True))
      End If

      If Me.chk_hide_ignored.Checked Then
        sql_query_pending.AppendLine(" AND TP_IGNORE = 0 ")
      End If

      '
      ' TERMINALS_3GS
      '

      ' Field GAME_3GS_ID was ( SELECT GM_GAME_ID FROM GAMES  WHERE GM_NAME = 'GAME_3GS'), but
      ' for manually added 3gs terminals, we don't have this value and have to search for it. So, we supress this
      ' SELECT and always find value, for added and updated terminals.

      ' Field TP_ID is necessary for undelete operation of pending terminals. See routine UndeleteAssignedRow().

      sql_query_3gs = New StringBuilder()

      sql_query_3gs.AppendLine(" SELECT (SELECT TE_PROV_ID FROM TERMINALS WHERE TE_TERMINAL_ID = T3GS_TERMINAL_ID) TE_PROV_ID")
      sql_query_3gs.AppendLine("        , T3GS_VENDOR_ID ")
      sql_query_3gs.AppendLine("        , T3GS_SERIAL_NUMBER ")
      sql_query_3gs.AppendLine("        , T3GS_MACHINE_NUMBER ")
      sql_query_3gs.AppendLine("        , T3GS_TERMINAL_ID ")
      sql_query_3gs.AppendLine("        , ISNULL (( SELECT TE_PROVIDER_ID FROM TERMINALS WHERE TE_TERMINAL_ID = T3GS_TERMINAL_ID ), '') TE_PROVIDER_ID ")
      sql_query_3gs.AppendLine("        , ISNULL (( SELECT TE_BASE_NAME        FROM TERMINALS WHERE TE_TERMINAL_ID = T3GS_TERMINAL_ID ), '') TE_NAME ")
      sql_query_3gs.AppendLine("        , ( SELECT TE_EXTERNAL_ID FROM TERMINALS WHERE TE_TERMINAL_ID = T3GS_TERMINAL_ID )              TE_EXTERNAL_ID ")
      'sql_query_3gs.AppendLine("        , ISNULL (( SELECT PG_GAME_NAME FROM PROVIDERS_GAMES WHERE PG_PV_ID = (SELECT TE_PROV_ID FROM TERMINALS WHERE TE_TERMINAL_ID = T3GS_TERMINAL_ID) AND PG_GAME_ID = ( SELECT TGT_TARGET_GAME_ID FROM TERMINAL_GAME_TRANSLATION WHERE TGT_TERMINAL_ID = T3GS_TERMINAL_ID AND TGT_SOURCE_GAME_ID = (SELECT GM_GAME_ID FROM GAMES WHERE GM_NAME = '" & SOURCE_GAME_NAME_3GS & "'))), 'UNKNOWN') GAME_NAME ")
      sql_query_3gs.AppendLine("        , ISNULL (( SELECT PG_GAME_NAME FROM PROVIDERS_GAMES WHERE PG_GAME_ID = ( SELECT TGT_TRANSLATED_GAME_ID FROM TERMINAL_GAME_TRANSLATION WHERE TGT_TERMINAL_ID = T3GS_TERMINAL_ID AND TGT_SOURCE_GAME_ID = (SELECT GM_GAME_ID FROM GAMES WHERE GM_NAME = '" & SOURCE_GAME_NAME_3GS & "'))), 'UNKNOWN') GAME_NAME ")
      sql_query_3gs.AppendLine("        , ( -1 ) GAME_3GS_ID ")
      'sql_query_3gs.AppendLine("        , ( SELECT TGT_TARGET_GAME_ID FROM TERMINAL_GAME_TRANSLATION WHERE TGT_TERMINAL_ID = T3GS_TERMINAL_ID AND TGT_SOURCE_GAME_ID = (SELECT GM_GAME_ID FROM GAMES WHERE GM_NAME = '" & SOURCE_GAME_NAME_3GS & "')) GAME_ID ")
      sql_query_3gs.AppendLine("        , ( SELECT TGT_TRANSLATED_GAME_ID FROM TERMINAL_GAME_TRANSLATION WHERE TGT_TERMINAL_ID = T3GS_TERMINAL_ID AND TGT_SOURCE_GAME_ID = (SELECT GM_GAME_ID FROM GAMES WHERE GM_NAME = '" & SOURCE_GAME_NAME_3GS & "')) GAME_ID ")
      sql_query_3gs.AppendLine("        , ( -1 ) TP_ID ")
      sql_query_3gs.AppendLine("        , ( 0 )  FLAG_REPLACEMENT ")
      sql_query_3gs.AppendLine("        , ( 0 )  FLAG_REPLACEMENT_RETIRED ")
      sql_query_3gs.AppendLine("        , ( 0 )  FLAG_IS_NULL_TE_EXTERNAL_ID ")
      sql_query_3gs.AppendLine("   FROM  TERMINALS_3GS ")

      'sql_query_3gs.AppendLine(" SELECT   T3GS_VENDOR_ID ")
      'sql_query_3gs.AppendLine("        , T3GS_SERIAL_NUMBER ")
      'sql_query_3gs.AppendLine("        , T3GS_MACHINE_NUMBER ")
      'sql_query_3gs.AppendLine("        , T3GS_TERMINAL_ID ")
      'sql_query_3gs.AppendLine("        , ISNULL (( SELECT TE_PROVIDER_ID FROM TERMINALS WHERE TE_TERMINAL_ID = T3GS_TERMINAL_ID ), '') TE_PROVIDER_ID ")
      'sql_query_3gs.AppendLine("        , ISNULL (( SELECT TE_NAME        FROM TERMINALS WHERE TE_TERMINAL_ID = T3GS_TERMINAL_ID ), '') TE_NAME ")
      'sql_query_3gs.AppendLine("        , ( SELECT TE_EXTERNAL_ID FROM TERMINALS WHERE TE_TERMINAL_ID = T3GS_TERMINAL_ID )              TE_EXTERNAL_ID ")
      'sql_query_3gs.AppendLine("        , ISNULL (( SELECT GM_NAME        FROM GAMES     WHERE GM_GAME_ID     = ( SELECT TGT_TARGET_GAME_ID FROM TERMINAL_GAME_TRANSLATION WHERE TGT_TERMINAL_ID = T3GS_TERMINAL_ID AND TGT_SOURCE_GAME_ID = (SELECT GM_GAME_ID FROM GAMES WHERE GM_NAME = '" & SOURCE_GAME_NAME_3GS & "'))), '') GAME_NAME ")
      'sql_query_3gs.AppendLine("        , ( -1 ) GAME_3GS_ID ")
      'sql_query_3gs.AppendLine("        , ( SELECT TGT_TARGET_GAME_ID FROM TERMINAL_GAME_TRANSLATION WHERE TGT_TERMINAL_ID = T3GS_TERMINAL_ID AND TGT_SOURCE_GAME_ID = (SELECT GM_GAME_ID FROM GAMES WHERE GM_NAME = '" & SOURCE_GAME_NAME_3GS & "')) GAME_ID ")
      'sql_query_3gs.AppendLine("        , ( -1 ) TP_ID ")
      'sql_query_3gs.AppendLine("        , ( 0 )  FLAG_REPLACEMENT ")
      'sql_query_3gs.AppendLine("        , ( 0 )  FLAG_REPLACEMENT_RETIRED ")
      'sql_query_3gs.AppendLine("        , ( 0 )  FLAG_IS_NULL_TE_EXTERNAL_ID ")
      'sql_query_3gs.AppendLine("   FROM  TERMINALS_3GS ")


      'sql_query_3gs.AppendLine(" SELECT   T3GS_VENDOR_ID ")
      'sql_query_3gs.AppendLine("        , T3GS_SERIAL_NUMBER ")
      'sql_query_3gs.AppendLine("        , T3GS_MACHINE_NUMBER ")
      'sql_query_3gs.AppendLine("        , T3GS_TERMINAL_ID ")
      'sql_query_3gs.AppendLine("        , ISNULL (TE_PROVIDER_ID, '') TE_PROVIDER_ID ")
      'sql_query_3gs.AppendLine("        , ISNULL (TE_NAME       , '') TE_NAME ")
      'sql_query_3gs.AppendLine("        , TE_EXTERNAL_ID  TE_EXTERNAL_ID ")
      'sql_query_3gs.AppendLine("        , PG_GAME_NAME GAME_NAME")
      'sql_query_3gs.AppendLine("        , ( -1 ) GAME_3GS_ID ")
      'sql_query_3gs.AppendLine("        , TGT_TARGET_GAME_ID GAME_ID")
      'sql_query_3gs.AppendLine("        , ( -1 ) TP_ID ")
      'sql_query_3gs.AppendLine("        , ( 0 )  FLAG_REPLACEMENT ")
      'sql_query_3gs.AppendLine("        , ( 0 )  FLAG_REPLACEMENT_RETIRED ")
      'sql_query_3gs.AppendLine("        , ( 0 )  FLAG_IS_NULL_TE_EXTERNAL_ID ")
      'sql_query_3gs.AppendLine("   FROM   TERMINALS_3GS ")
      'sql_query_3gs.AppendLine("   LEFT JOIN TERMINALS ON TE_TERMINAL_ID = T3GS_TERMINAL_ID ")
      'sql_query_3gs.AppendLine("   LEFT JOIN TERMINAL_GAME_TRANSLATION ON TGT_TERMINAL_ID = TE_TERMINAL_ID ")
      'sql_query_3gs.AppendLine("   LEFT JOIN PROVIDERS_GAMES ON PG_PV_ID = TE_PROV_ID AND PG_GAME_ID = TGT_TARGET_GAME_ID ")

      row_filter = New StringBuilder()
      str_filter = New StringBuilder()

      If Len(ef_terminal_3gs_vendor.Value) > 0 Then

        If row_filter.Length <> 0 Then
          row_filter.AppendLine(" AND ")
        End If

        row_filter.AppendLine(" T3GS_VENDOR_ID like '" & DataTable_FilterValue(ef_terminal_3gs_vendor.Value) & "*'")

        str_filter.AppendLine(ef_terminal_3gs_vendor.Value & " - ")
      End If

      If Len(ef_terminal_3gs_serial_number.Value) > 0 Then

        If row_filter.Length <> 0 Then
          row_filter.AppendLine(" AND ")
        End If

        row_filter.AppendLine("T3GS_SERIAL_NUMBER like '*" & DataTable_FilterValue(ef_terminal_3gs_serial_number.Value) & "*'")

        If str_filter.Length <> 0 Then
          str_filter.AppendLine(" - " & ef_terminal_3gs_serial_number.Value)
        Else
          str_filter.AppendLine(ef_terminal_3gs_serial_number.Value)
        End If
      End If

      ' Collection of filters used in the 3gs data view to be able to show new and modified values.
      m_filter_collection.Clear()
      If str_filter.Length <> 0 Then
        m_filter_collection.Add(str_filter, str_filter.ToString())
      End If

      ' End Build Query -------------------------------------------------

      '
      ' TERMINALS_PENDING
      '

      If Not IsNothing(m_sql_adap_pending) Then
        m_sql_adap_pending.Dispose()
        m_sql_adap_pending = Nothing
      End If
      m_sql_adap_pending = New SqlDataAdapter(sql_query_pending.ToString(), m_sql_conn)
      m_sql_adap_pending.FillSchema(m_sql_dataset_pending, SchemaType.Source)
      m_sql_adap_pending.Fill(m_sql_dataset_pending)

      m_sql_dataset_pending.Tables(0).Columns("TP_VENDOR_ID").ReadOnly = True
      m_sql_dataset_pending.Tables(0).Columns("TP_SERIAL_NUMBER").ReadOnly = True
      m_sql_dataset_pending.Tables(0).Columns("TP_MACHINE_NUMBER").ReadOnly = True
      m_sql_dataset_pending.Tables(0).Columns("TP_REPORTED").ReadOnly = True
      m_sql_dataset_pending.Tables(0).Columns("TP_IGNORE").ReadOnly = False

      m_sql_commandbuilder_pending = New SqlCommandBuilder(m_sql_adap_pending)
      'm_sql_adap_pending.UpdateCommand = m_sql_commandbuilder_pending.GetUpdateCommand
      m_sql_adap_pending.UpdateCommand = UpdateCommandPending()
      m_sql_adap_pending.DeleteCommand = m_sql_commandbuilder_pending.GetDeleteCommand
      m_sql_adap_pending.InsertCommand = Nothing

      m_data_view_pending = New DataView(m_sql_dataset_pending.Tables(0))
      m_data_view_pending.AllowDelete = False
      m_data_view_pending.AllowNew = False
      m_data_view_pending.AllowEdit = Me.Permissions.Write

      '
      ' TERMINALS_3GS
      '

      If Not IsNothing(m_sql_adap_3gs) Then
        m_sql_adap_3gs.Dispose()
        m_sql_adap_3gs = Nothing
      End If

      m_sql_adap_3gs = New SqlDataAdapter(sql_query_3gs.ToString(), m_sql_conn)
      m_sql_adap_3gs.FillSchema(m_sql_dataset_3gs, SchemaType.Source)
      m_sql_adap_3gs.Fill(m_sql_dataset_3gs)

      ' Remove constraints. Control them manually in function GUI_IsDataChangedOk().
      m_sql_dataset_3gs.Tables(0).Constraints.Clear()

      ' Default Values for Table TERMINALS_3GS
      m_sql_dataset_3gs.Tables(0).Columns("T3GS_VENDOR_ID").DefaultValue = ""
      m_sql_dataset_3gs.Tables(0).Columns("T3GS_SERIAL_NUMBER").DefaultValue = ""
      ' Don't want DefaultValue for MACHINE_NUMBER, we must allow null values.
      'm_sql_dataset_3gs.Tables(0).Columns("T3GS_MACHINE_NUMBER").DefaultValue = 0
      m_sql_dataset_3gs.Tables(0).Columns("T3GS_MACHINE_NUMBER").AllowDBNull = True
      m_sql_dataset_3gs.Tables(0).Columns("TE_NAME").DefaultValue = ""
      m_sql_dataset_3gs.Tables(0).Columns("TE_PROVIDER_ID").DefaultValue = ""
      m_sql_dataset_3gs.Tables(0).Columns("GAME_NAME").DefaultValue = ""

      m_sql_dataset_3gs.Tables(0).Columns("TE_PROV_ID").ReadOnly = False
      m_sql_dataset_3gs.Tables(0).Columns("T3GS_VENDOR_ID").ReadOnly = False
      m_sql_dataset_3gs.Tables(0).Columns("T3GS_SERIAL_NUMBER").ReadOnly = False
      m_sql_dataset_3gs.Tables(0).Columns("T3GS_MACHINE_NUMBER").ReadOnly = False
      m_sql_dataset_3gs.Tables(0).Columns("TE_NAME").ReadOnly = False
      m_sql_dataset_3gs.Tables(0).Columns("TE_PROVIDER_ID").ReadOnly = False
      m_sql_dataset_3gs.Tables(0).Columns("GAME_NAME").ReadOnly = False
      m_sql_dataset_3gs.Tables(0).Columns("GAME_ID").ReadOnly = False
      m_sql_dataset_3gs.Tables(0).Columns("GAME_3GS_ID").ReadOnly = False
      m_sql_dataset_3gs.Tables(0).Columns("TE_EXTERNAL_ID").ReadOnly = False

      m_sql_commandbuilder_3gs = New SqlCommandBuilder(m_sql_adap_3gs)
      m_sql_adap_3gs.UpdateCommand = m_sql_commandbuilder_3gs.GetUpdateCommand
      m_sql_adap_3gs.DeleteCommand = Nothing
      m_sql_adap_3gs.InsertCommand = m_sql_commandbuilder_3gs.GetInsertCommand

      m_data_view_3gs = New DataView(m_sql_dataset_3gs.Tables(0))
      m_data_view_3gs.RowFilter = row_filter.ToString()
      m_data_view_3gs.AllowDelete = Me.Permissions.Write
      m_data_view_3gs.AllowNew = Me.Permissions.Write
      m_data_view_3gs.AllowEdit = Me.Permissions.Write

      '
      ' DATA GRIDS
      '

      ' Enable Data Grids
      dg_terminals_pending.Enabled = True
      dg_terminals_3gs.Enabled = True

      dg_terminals_pending.DataSource = m_data_view_pending
      dg_terminals_3gs.DataSource = m_data_view_3gs

      ' TODO: What does AllowNavigation???
      dg_terminals_pending.AllowNavigation = False

      ' TODO: AllowDrop is not for permissions!!! It's Drag'n'Drop!!
      ' Delete Permissions
      'dg_terminals_pending.AllowDrop = False
      'dg_terminals_3gs.AllowDrop = False

      ' Columns for TERMINALS_PENDING
      If IsNothing(dg_terminals_pending.TableStyles.Item(m_data_view_pending.Table.TableName)) Then

        _table_style = NewTableStylePending(m_data_view_pending.Table.TableName, _
                                            m_sql_dataset_pending.Tables(0).Columns)
        dg_terminals_pending.TableStyles.Add(_table_style)

      End If

      ' Columns for Terminals
      If IsNothing(dg_terminals_3gs.TableStyles.Item(m_data_view_3gs.Table.TableName)) Then

        _table_style = NewTableStyle3gs(m_data_view_3gs.Table.TableName, _
                                        m_sql_dataset_3gs.Tables(0).Columns)
        dg_terminals_3gs.TableStyles.Add(_table_style)

      End If

      ' Restore original RowHeaderWidth.
      If dg_terminals_3gs.TableStyles.Count > 0 Then
        dg_terminals_3gs.TableStyles(0).RowHeaderWidth = m_original_row_header_width_3gs
      End If

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)

    Finally
    End Try

  End Sub 'ExecuteQuery

  ' PURPOSE: Create table style for pending terminals view.
  '
  '  PARAMS:
  '     - INPUT:
  '           - TableName As String
  '           - Columns As DataColumnCollection
  '     - OUTPUT:
  '           -
  '
  ' RETURNS:
  '     - _table_style As DataGridTableStyle

  Private Function NewTableStylePending(ByVal TableName As String, ByVal Columns As DataColumnCollection) As DataGridTableStyle
    Dim _table_style As DataGridTableStyle
    Dim _dgb_pvendor As DataGridTextBoxColumn
    Dim _dgb_pserial As DataGridTextBoxColumn
    Dim _dgb_pmachine As DataGridTextBoxColumn
    Dim _dgb_reported As DataGridTextBoxColumn
    Dim _dgb_ignore As DataGridBoolColumn
    Dim _idx_col As Integer

    _table_style = New DataGridTableStyle()
    _table_style.MappingName = TableName

    _dgb_pvendor = New DataGridTextBoxColumn()
    _dgb_pvendor.MappingName = "TP_VENDOR_ID"
    _dgb_pvendor.HeaderText = GLB_NLS_GUI_CONFIGURATION.GetString(205)   ' 205 "Vendor ID"
    _dgb_pvendor.Width = GRID_WIDTH_VENDOR_ID
    _dgb_pvendor.ReadOnly = Columns(_dgb_pvendor.MappingName).ReadOnly

    _dgb_pserial = New DataGridTextBoxColumn()
    _dgb_pserial.MappingName = "TP_SERIAL_NUMBER"
    _dgb_pserial.HeaderText = GLB_NLS_GUI_CONFIGURATION.GetString(214)   ' 214 "Serial number"
    _dgb_pserial.Width = GRID_WIDTH_SERIAL_NUMBER
    _dgb_pserial.ReadOnly = Columns(_dgb_pserial.MappingName).ReadOnly

    _dgb_pmachine = New DataGridTextBoxColumn()
    _dgb_pmachine.MappingName = "TP_MACHINE_NUMBER"
    _dgb_pmachine.HeaderText = GLB_NLS_GUI_CONFIGURATION.GetString(211)  ' 211 "Machine ID"
    _dgb_pmachine.Width = GRID_WIDTH_MACHINE_NUMBER
    _dgb_pmachine.ReadOnly = Columns(_dgb_pmachine.MappingName).ReadOnly
    _dgb_pmachine.Alignment = HorizontalAlignment.Center

    _dgb_reported = New DataGridTextBoxColumn()
    _dgb_reported.MappingName = "TP_REPORTED"
    _dgb_reported.HeaderText = GLB_NLS_GUI_AUDITOR.GetString(351)        ' 351 "Reported Date"
    _dgb_reported.Width = GRID_WIDTH_TP_REPORTED
    _dgb_reported.ReadOnly = Columns(_dgb_reported.MappingName).ReadOnly
    ' Necessary to format date and show also the time.
    _dgb_reported.Format = Format.DateTimeCustomFormatString(True)

    _dgb_ignore = New DataGridBoolColumn()
    _dgb_ignore.MappingName = "TP_IGNORE"
    _dgb_ignore.HeaderText = GLB_NLS_GUI_AUDITOR.GetString(352)          ' 352 "Ignored"
    _dgb_ignore.Width = GRID_WIDTH_TP_IGNORE
    _dgb_ignore.ReadOnly = Columns(_dgb_ignore.MappingName).ReadOnly
    ' To disable null values in DataGridBoolColumn
    _dgb_ignore.AllowNull = False

    ' The order added is the order shown
    For _idx_col = 0 To GRID_PENDING_COLUMNS
      Select Case _idx_col
        Case GRID_COLUMN_PENDING_VENDOR_ID
          _table_style.GridColumnStyles.Add(_dgb_pvendor)
        Case GRID_COLUMN_PENDING_SERIAL_NUMBER
          _table_style.GridColumnStyles.Add(_dgb_pserial)
        Case GRID_COLUMN_PENDING_MACHINE_NUMBER
          _table_style.GridColumnStyles.Add(_dgb_pmachine)
        Case GRID_COLUMN_PENDING_REPORTED
          _table_style.GridColumnStyles.Add(_dgb_reported)
        Case GRID_COLUMN_PENDING_IGNORE
          _table_style.GridColumnStyles.Add(_dgb_ignore)
      End Select
    Next

    Return _table_style

  End Function 'NewTableStylePending

  ' PURPOSE: Create table style for 3gs terminals view.
  '
  '  PARAMS:
  '     - INPUT:
  '           - TableName As String
  '           - Columns As DataColumnCollection
  '     - OUTPUT:
  '           -
  '
  ' RETURNS:
  '     - _table_style As DataGridTableStyle

  Private Function NewTableStyle3gs(ByVal TableName As String, ByVal Columns As DataColumnCollection) As DataGridTableStyle
    Dim _table_style As DataGridTableStyle
    Dim _text_column As ColoredTextBoxColumn
    Dim _text_column_2 As ColoredTextBoxColumn
    Dim _dgb_vendor As ColoredTextBoxColumn
    Dim _dgb_te_name As ColoredTextBoxColumn
    Dim _dgb_cmbox_provider As DataGridComboBoxColumn
    Dim _dgb_cmbox_game As DataGridComboBoxColumn
    Dim _dgb_serial As ColoredTextBoxColumn
    Dim _dgb_machine As ColoredTextBoxColumn
    Dim _dgb_flag_replacement As ColoredTextBoxColumn
    Dim _dgb_flag_replacement_retired As ColoredTextBoxColumn
    Dim _idx_col As Integer

    Dim _str_sql As String

    _table_style = New DataGridTableStyle()
    _table_style.MappingName = TableName

    _text_column = New ColoredTextBoxColumn()
    _text_column.MappingName = "TE_PROV_ID"
    _text_column.ReadOnly = False
    _text_column.HeaderText = ""
    _text_column.Width = 0

    _text_column_2 = New ColoredTextBoxColumn()
    _text_column_2.MappingName = "GAME_ID"
    _text_column_2.ReadOnly = False
    '_text_column.HeaderText = GLB_NLS_GUI_CONFIGURATION.GetString(469)       ' 
    _text_column_2.Width = 0


    _dgb_vendor = New ColoredTextBoxColumn()
    _dgb_vendor.MappingName = "T3GS_VENDOR_ID"
    _dgb_vendor.HeaderText = GLB_NLS_GUI_CONFIGURATION.GetString(205)
    _dgb_vendor.Width = GRID_WIDTH_VENDOR_ID
    _dgb_vendor.ReadOnly = Columns(_dgb_vendor.MappingName).ReadOnly

    '_dgb_provider = New ColoredTextBoxColumn()
    '_dgb_provider.MappingName = "TE_PROVIDER_ID"
    '_dgb_provider.HeaderText = GLB_NLS_GUI_AUDITOR.GetString(341)
    '_dgb_provider.Width = GRID_WIDTH_PROVIDER_ID
    '_dgb_provider.ReadOnly = Columns(_dgb_provider.MappingName).ReadOnly

    _str_sql = "SELECT DISTINCT PV_ID,PV_NAME FROM PROVIDERS WHERE PV_HIDE='0' ORDER BY PV_NAME"
    _dgb_cmbox_provider = New DataGridComboBoxColumn()

    _dgb_cmbox_provider.MappingName = "TE_PROVIDER_ID"
    _dgb_cmbox_provider.HeaderText = GLB_NLS_GUI_AUDITOR.GetString(341)   ' "Proveedor"
    _dgb_cmbox_provider.Width = GRID_WIDTH_VENDOR_ID
    _dgb_cmbox_provider.ColumnComboBox.Tag = Me.FormId
    _dgb_cmbox_provider.ColumnComboBox.DataSource = New DataView(GUI_GetTableUsingSQL(_str_sql, 1000))
    _dgb_cmbox_provider.ColumnComboBox.DisplayMember = "PV_NAME"
    _dgb_cmbox_provider.ColumnComboBox.ValueMember = "PV_NAME"
    _dgb_cmbox_provider.ReadOnly = Columns(_dgb_cmbox_provider.MappingName).ReadOnly

    _dgb_te_name = New ColoredTextBoxColumn()
    _dgb_te_name.MappingName = "TE_NAME"
    _dgb_te_name.HeaderText = GLB_NLS_GUI_CONFIGURATION.GetString(210)
    _dgb_te_name.Width = GRID_WIDTH_TERMINAL_NAME
    _dgb_te_name.ReadOnly = Columns(_dgb_te_name.MappingName).ReadOnly

    _dgb_serial = New ColoredTextBoxColumn()
    _dgb_serial.MappingName = "T3GS_SERIAL_NUMBER"
    _dgb_serial.HeaderText = GLB_NLS_GUI_CONFIGURATION.GetString(214)
    _dgb_serial.Width = GRID_WIDTH_SERIAL_NUMBER
    _dgb_serial.ReadOnly = Columns(_dgb_serial.MappingName).ReadOnly

    _dgb_machine = New ColoredTextBoxColumn()
    _dgb_machine.MappingName = "T3GS_MACHINE_NUMBER"
    _dgb_machine.HeaderText = GLB_NLS_GUI_CONFIGURATION.GetString(211)
    _dgb_machine.Width = GRID_WIDTH_MACHINE_NUMBER
    _dgb_machine.ReadOnly = Columns(_dgb_machine.MappingName).ReadOnly
    _dgb_machine.NullText = ""
    _dgb_machine.Alignment = HorizontalAlignment.Center

    '_dgb_game = New ColoredTextBoxColumn()
    '_dgb_game.MappingName = "GAME_NAME"
    '_dgb_game.HeaderText = GLB_NLS_GUI_CONTROLS.GetString(280)
    '_dgb_game.Width = GRID_WIDTH_GAME_NAME
    '_dgb_game.ReadOnly = Columns(_dgb_game.MappingName).ReadOnly

    _str_sql = "SELECT PG_GAME_NAME GM_NAME FROM PROVIDERS_GAMES ORDER BY PG_GAME_NAME"
    _dgb_cmbox_game = New DataGridComboBoxColumn()
    _dgb_cmbox_game.MappingName = "GAME_NAME"
    _dgb_cmbox_game.HeaderText = GLB_NLS_GUI_CONTROLS.GetString(280)   ' "Juegos"
    _dgb_cmbox_game.Width = GRID_WIDTH_GAME_NAME
    _dgb_cmbox_game.ColumnComboBox.Tag = Me.FormId
    _dgb_cmbox_game.ColumnComboBox.DataSource = New DataView(GUI_GetTableUsingSQL(_str_sql, 1000))
    _dgb_cmbox_game.ColumnComboBox.DisplayMember = "GM_NAME"
    _dgb_cmbox_game.ColumnComboBox.ValueMember = "GM_NAME"
    _dgb_cmbox_game.ReadOnly = Columns(_dgb_cmbox_game.MappingName).ReadOnly


    ' Invisible column to set replaced terminals to active
    _dgb_flag_replacement = New ColoredTextBoxColumn()
    _dgb_flag_replacement.MappingName = "FLAG_REPLACEMENT"
    _dgb_flag_replacement.HeaderText = ""
    _dgb_flag_replacement.Width = 0
    _dgb_flag_replacement.ReadOnly = False

    ' Invisible column to prevent (=0) or force (=1) inserting terminals to TERMINALS_3GS table
    _dgb_flag_replacement_retired = New ColoredTextBoxColumn()
    _dgb_flag_replacement_retired.MappingName = "FLAG_REPLACEMENT_RETIRED"
    _dgb_flag_replacement_retired.HeaderText = ""
    _dgb_flag_replacement_retired.Width = 0
    _dgb_flag_replacement_retired.ReadOnly = False

    ' The order added is the order shown
    For _idx_col = 0 To GRID_3GS_COLUMNS
      Select Case _idx_col
        Case GRID_COLUMN_3GS_TE_PROV_ID
          _table_style.GridColumnStyles.Add(_text_column)
        Case GRID_COLUMN_3GS_GAME_ID
          _table_style.GridColumnStyles.Add(_text_column_2)
        Case GRID_COLUMN_3GS_VENDOR_ID
          _table_style.GridColumnStyles.Add(_dgb_vendor)
        Case GRID_COLUMN_3GS_SERIAL_NUMBER
          _table_style.GridColumnStyles.Add(_dgb_serial)
        Case GRID_COLUMN_3GS_MACHINE_NUMBER
          _table_style.GridColumnStyles.Add(_dgb_machine)
        Case GRID_COLUMN_3GS_PROVIDER_ID
          '_table_style.GridColumnStyles.Add(_dgb_provider)
          _table_style.GridColumnStyles.Add(_dgb_cmbox_provider)
        Case GRID_COLUMN_3GS_TERMINAL_NAME
          _table_style.GridColumnStyles.Add(_dgb_te_name)
        Case GRID_COLUMN_3GS_GAME_NAME
          '_table_style.GridColumnStyles.Add(_dgb_game)
          _table_style.GridColumnStyles.Add(_dgb_cmbox_game)
        Case GRID_COLUMN_3GS_FLAG_REPLACEMENT
          _table_style.GridColumnStyles.Add(_dgb_flag_replacement)
        Case GRID_COLUMN_3GS_FLAG_REPLACEMENT_RETIRED
          _table_style.GridColumnStyles.Add(_dgb_flag_replacement_retired)
      End Select
    Next

    Return _table_style

  End Function 'NewTableStyle3gs

  ' PURPOSE: Execute function Update for the proper SqlAdapter (using Type parameter) and according to RowState.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Type As Integer
  '           - RowState As DataViewRowState
  '     - OUTPUT:
  '           - MsgError As String
  '
  ' RETURNS:
  '     - True:  If inserts are ok
  '     - False: If inserts are NOT ok

  Private Function ExecuteAdapterUpdate(ByVal Type As Integer, ByVal RowState As DataViewRowState, ByRef MsgError As String) As Boolean
    Dim rows_update As DataRow()
    Dim dataset As DataSet
    Dim sqladap As SqlDataAdapter
    Dim dataview As DataView
    Dim nr As Integer

    Select Case Type
      Case TYPE_PENDING
        dataset = m_sql_dataset_pending
        sqladap = m_sql_adap_pending
        dataview = m_data_view_pending
      Case TYPE_3GS
        dataset = m_sql_dataset_3gs
        sqladap = m_sql_adap_3gs
        dataview = m_data_view_3gs
      Case Else
        MsgError = "Internal Error!"
        Return False
    End Select

    rows_update = dataset.Tables(0).Select(dataview.RowFilter, "", RowState)

    If rows_update.Length > 0 Then

      nr = sqladap.Update(rows_update)

      If rows_update.Length <> nr Then
        MsgError = GetMsgErrorFromRow(rows_update)
        Return False
      End If

      If Type = TYPE_3GS And RowState = DataViewRowState.Added Then
        m_terms_3gs_added = True
      End If
    End If

    Return True
  End Function ' ExecuteAdapterUpdate

  ' PURPOSE: Find the first message error in a array of rows.
  '
  '  PARAMS:
  '     - INPUT: DataRows() Array of rows to search for a message error
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - First msg_error found
  '
  Private Function GetMsgErrorFromRow(ByVal DataRows() As DataRow) As String
    Dim row As DataRow
    Dim msg_error As String = ""
    Dim _str_sql As String

    m_error_insert_terminal = ""

    For Each row In DataRows
      If row.HasErrors Then
        msg_error = row.RowError

        If row.Table.Columns.Contains("TE_NAME") Then
          Using _db_trx As New WSI.Common.DB_TRX()

            _str_sql = "SELECT COUNT(*) FROM TERMINALS WHERE TE_NAME = @pTerminalName"
            Using _cmd As New SqlCommand(_str_sql)
              _cmd.Parameters.Add("@pTerminalName", SqlDbType.NVarChar).Value = row.Item("TE_NAME")

              If CInt(_db_trx.ExecuteScalar(_cmd)) > 0 Then
                m_error_insert_terminal = m_error_insert_terminal & row.Item("TE_NAME") & ", "
              Else
                Exit For
              End If
            End Using
          End Using
        Else
          Exit For
        End If

      End If
    Next

    If m_error_insert_terminal.Length > 2 Then
      m_error_insert_terminal = m_error_insert_terminal.Substring(0, m_error_insert_terminal.Length - 2)
    End If

    Return msg_error

  End Function ' GetMsgErrorFromRow

  ' PURPOSE: Insert new terminals in table TERMINALS.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Trx As SqlTransaction
  '     - OUTPUT:
  '           - MsgError As String
  '
  ' RETURNS:
  '     - True:  If inserts are ok
  '     - False: If inserts are NOT ok

  Private Function InsertNewTerminals(ByVal Trx As SqlTransaction, ByRef MsgError As String) As Boolean

    Dim _rows_added_terminals() As DataRow
    Dim _rows_added_3gs() As DataRow
    Dim _sql_adap_terminals As SqlDataAdapter
    Dim _sql_adap_3gs As SqlDataAdapter
    Dim _nr_terminals As Integer
    Dim _nr_3gs As Integer
    Dim _row As DataRow
    Dim _filter As String
    Dim _parameter As SqlParameter
    Dim _sql_cmd As SqlCommand
    Dim _sb As StringBuilder

    _sb = New StringBuilder()

    If m_data_view_3gs.RowFilter = "" Then
      _filter = "FLAG_REPLACEMENT_RETIRED = "
    Else
      _filter = "(" + m_data_view_3gs.RowFilter + ")" + " AND FLAG_REPLACEMENT_RETIRED = "
    End If

    ' Terminals to be inserted into terminals table ( FLAG_REPLACEMENT_RETIRED = 0 )
    _rows_added_terminals = m_sql_dataset_3gs.Tables(0).Select(_filter + "0", "", DataViewRowState.Added)

    ' Replacing a retired 3GS terminal
    _rows_added_3gs = m_sql_dataset_3gs.Tables(0).Select(_filter + "1", "", DataViewRowState.Added)

    If _rows_added_terminals.Length = 0 And _rows_added_3gs.Length = 0 Then
      Return True
    End If

    If _rows_added_terminals.Length > 0 Then
      _sql_adap_terminals = New SqlDataAdapter()
      _sql_adap_terminals.SelectCommand = Nothing
      _sql_adap_terminals.UpdateCommand = Nothing
      _sql_adap_terminals.DeleteCommand = Nothing
      '_sql_adap_terminals.ContinueUpdateOnError = True
      _sql_adap_terminals.UpdateBatchSize = 500

      _sb.AppendLine(" INSERT INTO   TERMINALS                ")
      _sb.AppendLine("             ( TE_TYPE                  ")
      _sb.AppendLine("             , TE_BASE_NAME             ")
      _sb.AppendLine("             , TE_EXTERNAL_ID           ")
      _sb.AppendLine("             , TE_BLOCKED               ")
      _sb.AppendLine("             , TE_ACTIVE                ")
      _sb.AppendLine("             , TE_PROVIDER_ID           ")
      _sb.AppendLine("             , TE_TERMINAL_TYPE         ")
      _sb.AppendLine("             , TE_VENDOR_ID             ")
      _sb.AppendLine("             , TE_MASTER_ID             ")
      _sb.AppendLine("             , TE_ISO_CODE              ")
      _sb.AppendLine("             , TE_SERIAL_NUMBER         ")
      _sb.AppendLine("             , TE_CREATION_DATE         ")
      _sb.AppendLine("             )                          ")
      _sb.AppendLine("      VALUES                            ")
      _sb.AppendLine("             ( 1                        ")
      _sb.AppendLine("             , @TermName                ")
      _sb.AppendLine("             , @VendorSerialNumber      ")
      _sb.AppendLine("             , 0                        ")
      _sb.AppendLine("             , 1                        ")
      _sb.AppendLine("             , @ProviderId              ")
      _sb.AppendLine("             , @pTerminalType3GS        ")
      _sb.AppendLine("             , @VendorId                ")
      _sb.AppendLine("             , 0                        ")
      _sb.AppendLine("             , @pCurrency               ")
      _sb.AppendLine("             , @pSerialNumeber          ")
      _sb.AppendLine("             , @pCreation               ")
      _sb.AppendLine("             )                          ")

      _sb.AppendLine(" SET @pTerminalId = SCOPE_IDENTITY()    ")

      _sb.AppendLine(" UPDATE   TERMINALS                     ")
      _sb.AppendLine("    SET   TE_MASTER_ID = TE_TERMINAL_ID ")
      _sb.AppendLine("  WHERE   TE_TERMINAL_ID = @pTerminalId ")

      _sql_cmd = New SqlCommand(_sb.ToString, Trx.Connection, Trx)

      '
      ' MPO 22-FEB-2012: ADD SCOPE_IDENTITY() IN THE SQL COMMAND UPDATE
      '
      '' Add Event: Get new T3GS_TERMINAL_ID in insert statement
      'AddHandler _sql_adap_terminals.RowUpdated, New SqlRowUpdatedEventHandler(AddressOf sql_adap_terms_global_OnRowUpdated)

      _sql_cmd.Parameters.Add("@VendorId", SqlDbType.NVarChar, 50, "T3GS_VENDOR_ID")
      _sql_cmd.Parameters.Add("@TermName", SqlDbType.NVarChar, 50, "TE_NAME")
      _sql_cmd.Parameters.Add("@ProviderId", SqlDbType.NVarChar, 50, "TE_PROVIDER_ID")
      _sql_cmd.Parameters.Add("@VendorSerialNumber", SqlDbType.NVarChar, 40, "TE_EXTERNAL_ID")
      _sql_cmd.Parameters.Add("@pTerminalType3GS", SqlDbType.SmallInt).Value = WSI.Common.TerminalTypes.T3GS
      _sql_cmd.Parameters.Add("@pCurrency", SqlDbType.NVarChar).Value = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")
      _sql_cmd.Parameters.Add("@pSerialNumeber", SqlDbType.NVarChar, 50, "T3GS_SERIAL_NUMBER")
      _sql_cmd.Parameters.Add("@pCreation", SqlDbType.DateTime).Value = WGDB.Now

      '
      ' MPO 22-FEB-2012
      '
      _parameter = _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int)
      _parameter.SourceColumn = "T3GS_TERMINAL_ID"
      _parameter.Direction = ParameterDirection.Output
      _sql_cmd.UpdatedRowSource = UpdateRowSource.OutputParameters

      _sql_adap_terminals.InsertCommand = _sql_cmd

      _nr_terminals = _sql_adap_terminals.Update(_rows_added_terminals)

      If _rows_added_terminals.Length <> _nr_terminals Then
        MsgError = GetMsgErrorFromRow(_rows_added_terminals)

        Return False
      End If

      ' Need to let RowState as Added (previous state), to be able to continue work on others tables: GAMES and 3GS.
      For Each _row In _rows_added_terminals
        If _row.RowState = DataRowState.Unchanged Then
          _row.SetAdded()
        End If
      Next
    End If

    ' Replacing a retired 3GS terminal
    If _rows_added_3gs.Length > 0 Then
      _sql_adap_3gs = New SqlDataAdapter()
      _sql_adap_3gs.SelectCommand = Nothing
      _sql_adap_3gs.UpdateCommand = Nothing
      _sql_adap_3gs.DeleteCommand = Nothing
      _sql_adap_3gs.ContinueUpdateOnError = True

      ' Do an UPDATE as an InsertCommand, because the RowState is Added.
      _sb = New StringBuilder()
      _sb.AppendLine(" UPDATE   TERMINALS                                                                                           ")
      _sb.AppendLine("    SET   TE_EXTERNAL_ID = @VendorSerialNumber                                                                ")
      _sb.AppendLine("        , TE_STATUS = CASE WHEN @FlagReplacement = 1 THEN {0} ELSE TE_STATUS END                              ")
      _sb.AppendLine("        , TE_RETIREMENT_DATE = CASE WHEN @FlagReplacement = 1 THEN NULL ELSE TE_RETIREMENT_DATE END           ")
      _sb.AppendLine("        , TE_RETIREMENT_REQUESTED = CASE WHEN @FlagReplacement = 1 THEN NULL ELSE TE_RETIREMENT_REQUESTED END ")
      _sb.AppendLine("  WHERE   TE_TERMINAL_ID = @TerminalId                                                                        ")

      _sql_adap_3gs.InsertCommand = New SqlCommand(String.Format(_sb.ToString, WSI.Common.TerminalStatus.ACTIVE))
      _sql_adap_3gs.InsertCommand.Connection = Trx.Connection
      _sql_adap_3gs.InsertCommand.Transaction = Trx

      _sql_adap_3gs.InsertCommand.Parameters.Add("@TerminalId", SqlDbType.Int, 4, "T3GS_TERMINAL_ID")
      _sql_adap_3gs.InsertCommand.Parameters.Add("@FlagReplacement", SqlDbType.Int, 0, "FLAG_REPLACEMENT")
      _sql_adap_3gs.InsertCommand.Parameters.Add("@VendorSerialNumber", SqlDbType.NVarChar, 40, "TE_EXTERNAL_ID")

      _nr_3gs = _sql_adap_3gs.Update(_rows_added_3gs)

      If _rows_added_3gs.Length <> _nr_3gs Then
        MsgError = GetMsgErrorFromRow(_rows_added_3gs)

        Return False
      End If

      ' Need to let RowState as Added (previous state), to be able to continue work on others tables: GAMES and 3GS.
      For Each _row In _rows_added_3gs
        If _row.RowState = DataRowState.Unchanged Then
          _row.SetAdded()
        End If
      Next
    End If

    m_terms_global_added = 1

    Return True
  End Function 'InsertNewTerminals

  ' PURPOSE: Update data in table TERMINALS.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Trx As SqlTransaction
  '     - OUTPUT:
  '           - MsgError As String
  '
  ' RETURNS:
  '     - True:  If updates are ok
  '     - False: If updates are NOT ok

  Private Function UpdateTerminals(ByVal Trx As SqlTransaction, ByRef MsgError As String) As Boolean
    Dim rows_orig() As DataRow
    Dim rows_updated() As DataRow
    Dim sql_adap As SqlDataAdapter
    Dim sql_query As String
    Dim nr As Integer

    rows_orig = m_sql_dataset_3gs.Tables(0).Select(m_data_view_3gs.RowFilter, "", DataViewRowState.ModifiedCurrent)
    rows_updated = CopyArrayRows(m_sql_dataset_3gs.Tables(0), rows_orig)

    If rows_updated.Length = 0 Then
      Return True
    End If

    sql_adap = New SqlDataAdapter()
    sql_adap.SelectCommand = Nothing
    sql_adap.InsertCommand = Nothing
    sql_adap.DeleteCommand = Nothing
    sql_adap.ContinueUpdateOnError = True

    sql_query = "UPDATE TERMINALS" & _
                  " SET TE_BASE_NAME        = @TermName" & _
                     ", TE_EXTERNAL_ID = @ExternalId" & _
                     ", TE_PROVIDER_ID = @ProviderId" & _
                     ", TE_VENDOR_ID   = @VendorId" & _
                     ", TE_STATUS               = CASE WHEN @FlagReplacement = 1 THEN " & WSI.Common.TerminalStatus.ACTIVE & " ELSE TE_STATUS END" & _
                     ", TE_RETIREMENT_DATE      = CASE WHEN @FlagReplacement = 1 THEN NULL ELSE TE_RETIREMENT_DATE END" & _
                     ", TE_RETIREMENT_REQUESTED = CASE WHEN @FlagReplacement = 1 THEN NULL ELSE TE_RETIREMENT_REQUESTED END" & _
                " WHERE TE_TERMINAL_ID = @TerminalId" & _
                  " AND TE_BASE_NAME = @TermName_Original" & _
                  " AND ( (    TE_EXTERNAL_ID IS NULL" & _
                         " AND @FlagIsNullTeExternalId_Original = 1" & _
                         ")" & _
                     " OR (    TE_EXTERNAL_ID IS NOT NULL" & _
                         " AND @FlagIsNullTeExternalId_Original = 0" & _
                         " AND TE_EXTERNAL_ID = @ExternalId_Original" & _
                         ")" & _
                       ")" & _
                  " AND TE_PROVIDER_ID = @ProviderId_Original" & _
                  " AND TE_VENDOR_ID = @VendorId_Original"

    sql_adap.UpdateCommand = New SqlCommand(sql_query)
    sql_adap.UpdateCommand.Connection = Trx.Connection
    sql_adap.UpdateCommand.Transaction = Trx

    ' SET
    sql_adap.UpdateCommand.Parameters.Add("@TermName", SqlDbType.NVarChar, 50, "TE_NAME")
    sql_adap.UpdateCommand.Parameters.Add("@ExternalId", SqlDbType.NVarChar, 40, "TE_EXTERNAL_ID")
    sql_adap.UpdateCommand.Parameters.Add("@ProviderId", SqlDbType.NVarChar, 50, "TE_PROVIDER_ID")
    sql_adap.UpdateCommand.Parameters.Add("@VendorId", SqlDbType.NVarChar, 50, "T3GS_VENDOR_ID")
    sql_adap.UpdateCommand.Parameters.Add("@FlagReplacement", SqlDbType.Int, 0, "FLAG_REPLACEMENT")

    ' WHERE
    sql_adap.UpdateCommand.Parameters.Add("@TerminalId", SqlDbType.Int, 0, "T3GS_TERMINAL_ID")
    sql_adap.UpdateCommand.Parameters.Add("@FlagIsNullTeExternalId_Original", SqlDbType.Int, 0, "FLAG_IS_NULL_TE_EXTERNAL_ID")

    sql_adap.UpdateCommand.Parameters.Add("@TermName_Original", SqlDbType.NVarChar, 50, "TE_NAME")
    sql_adap.UpdateCommand.Parameters("@TermName_Original").SourceVersion = DataRowVersion.Original

    sql_adap.UpdateCommand.Parameters.Add("@ExternalId_Original", SqlDbType.NVarChar, 40, "TE_EXTERNAL_ID")
    sql_adap.UpdateCommand.Parameters("@ExternalId_Original").SourceVersion = DataRowVersion.Original

    sql_adap.UpdateCommand.Parameters.Add("@ProviderId_Original", SqlDbType.NVarChar, 50, "TE_PROVIDER_ID")
    sql_adap.UpdateCommand.Parameters("@ProviderId_Original").SourceVersion = DataRowVersion.Original

    sql_adap.UpdateCommand.Parameters.Add("@VendorId_Original", SqlDbType.NVarChar, 50, "T3GS_VENDOR_ID")
    sql_adap.UpdateCommand.Parameters("@VendorId_Original").SourceVersion = DataRowVersion.Original

    Try
      nr = sql_adap.Update(rows_updated)
      If rows_updated.Length <> nr Then
        MsgError = GetMsgErrorFromRow(rows_updated)

        Return False
      End If

      Return True

    Finally
      CopyRowsCurrentValues(rows_updated, rows_orig)
    End Try

  End Function 'UpdateTerminals

  ' PURPOSE: Update or insert data in table GAMES and TERMINAL_GAME_TRANSLATION.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Trx As SqlTransaction
  '     - OUTPUT:
  '           - MsgError As String
  '
  ' RETURNS:
  '     - True:  If updates/inserts are ok
  '     - False: If updates/inserts are NOT ok

  Private Function UpdateDataGame(ByVal Trx As SqlTransaction, ByRef MsgError As String) As Boolean
    Dim rows_orig() As DataRow
    Dim rows_updated() As DataRow
    Dim sql_query As StringBuilder
    Dim adap_game As SqlDataAdapter
    Dim table_games As DataTable
    Dim row As DataRow
    Dim rows_games() As DataRow
    Dim filter_game As String
    Dim sql_adap As SqlDataAdapter
    Dim sql_cb_game As SqlCommandBuilder
    Dim nr As Integer
    Dim global_game3gs_id As Integer
    Dim _read_only_switch As Boolean
    Dim _game_name As String

    rows_orig = m_sql_dataset_3gs.Tables(0).Select(m_data_view_3gs.RowFilter, _
                                                   "", _
                                                   DataViewRowState.ModifiedCurrent Or DataViewRowState.Added)
    rows_updated = CopyArrayRows(m_sql_dataset_3gs.Tables(0), rows_orig)

    If rows_updated.Length = 0 Then
      Return True
    End If

    adap_game = New SqlDataAdapter()

    sql_query = New StringBuilder()

    sql_query.AppendLine("SELECT   GM_GAME_ID ")
    sql_query.AppendLine("       , GM_NAME ")
    sql_query.AppendLine("  FROM   GAMES ")

    adap_game.SelectCommand = New SqlCommand(sql_query.ToString())
    adap_game.SelectCommand.Connection = Trx.Connection
    adap_game.SelectCommand.Transaction = Trx
    table_games = New DataTable("GAMES")
    adap_game.Fill(table_games)

    sql_cb_game = New SqlCommandBuilder(adap_game)
    adap_game.InsertCommand = sql_cb_game.GetInsertCommand
    adap_game.UpdateCommand = Nothing
    adap_game.DeleteCommand = Nothing
    adap_game.ContinueUpdateOnError = True

    ' Add Event: Get new GM_GAME_ID in insert statement
    AddHandler adap_game.RowUpdated, New SqlRowUpdatedEventHandler(AddressOf sql_adap_games_OnRowUpdated)

    ' Get global id for 3gs Game for new terminals
    filter_game = "GM_NAME = '" & SOURCE_GAME_NAME_3GS & "'"
    rows_games = table_games.Select(filter_game)
    If rows_games.Length = 0 Then
      MsgError = "Global game name for 3GS TERMINALS '" & SOURCE_GAME_NAME_3GS & "' not found and it would."

      Return False
    End If

    global_game3gs_id = rows_games(0).Item("GM_GAME_ID")

    '' 29-MAY-2013 XCD Now the game has been selected from combo with all provider games

    '' Look for new games.
    'For Each row In rows_updated
    '  _game_name = row.Item("GAME_NAME")
    '  filter_game = "GM_NAME = '" & _game_name.Replace("'", "''") & "'"
    '  ' Look for the game name introduced
    '  rows_games = table_games.Select(filter_game)

    '  ' Game name NOT found. Insert new row in table GAMES.
    '  If rows_games.Length = 0 Then
    '    If _game_name <> "UNKNOWN" Then
    '      new_game = table_games.NewRow()
    '      new_game.Item("GM_NAME") = _game_name
    '      table_games.Rows.Add(new_game)
    '    End If
    '  End If
    'Next

    '' First INSERT new game rows.
    'rows_games_added = table_games.Select("", "", DataViewRowState.Added)
    'If rows_games_added.Length > 0 Then
    '  nr = adap_game.Update(rows_games_added)
    '  If rows_games_added.Length <> nr Then
    '    MsgError = GetMsgErrorFromRow(rows_games_added)
    '    Return False
    '  End If
    'End If

    ' Ok, now all game names exist.

    '' END 29-MAY-2013 XCD Now the game has been selected from combo with all provider games

    For Each row In rows_updated
      _game_name = row.Item("GAME_NAME")
      'filter_game = "GM_NAME = '" & _game_name.Replace("'", "''") & "'"
      ''filter_game = filter_game & " AND PG_PV_ID = '" & row.Item("TE_PROV_ID").ToString().Replace("'", "''") & "'"
      '' Look for the game name introduced
      'rows_games = table_games.Select(filter_game)

      '' Have to found ALWAYS game names!
      '' Set value GAME_ID in the actual row.
      'If rows_games.Length <= 0 Then
      '  MsgError = "Game name '" & row.Item("GAME_NAME") & "' not found and it would."
      'End If

      _read_only_switch = False

      If row.Table.Columns("GAME_ID").ReadOnly Then
        row.Table.Columns("GAME_ID").ReadOnly = False
        row.Table.Columns("GAME_3GS_ID").ReadOnly = False

        _read_only_switch = True
      End If

      'If Not String.IsNullOrEmpty(_game_name) Then
      '  row.Item("GAME_ID") = rows_games(0).Item("GM_GAME_ID")
      'End If
      ' We should test if it's null before set, but it doesn't matter.
      row.Item("GAME_3GS_ID") = global_game3gs_id

      If _read_only_switch Then
        row.Table.Columns("GAME_ID").ReadOnly = True
        row.Table.Columns("GAME_3GS_ID").ReadOnly = True
      End If

    Next

    ' Ready for update/insert relation terminal - game: Table TERMINAL_GAME_TRANSLATION
    sql_adap = New SqlDataAdapter()
    sql_adap.SelectCommand = Nothing
    sql_adap.DeleteCommand = Nothing
    sql_adap.ContinueUpdateOnError = True

    'sql_query = "IF EXISTS ( " & _
    '            "  SELECT   1 " & _
    '            "    FROM   TERMINAL_GAME_TRANSLATION " & _
    '            "   WHERE   TGT_TERMINAL_ID    = @TerminalId " & _
    '            "     AND   TGT_SOURCE_GAME_ID = @SourceGameId " & _
    '            "  ) " & _
    '            "  UPDATE   TERMINAL_GAME_TRANSLATION " & _
    '            "     SET   TGT_TARGET_GAME_ID = @TargetGameId " & _
    '            "          ,TGT_UPDATED        = GETDATE()" & _
    '            "   WHERE   TGT_TERMINAL_ID    = @TerminalId " & _
    '            "     AND   TGT_SOURCE_GAME_ID = @SourceGameId " & _
    '            "ELSE " & _
    '            "  INSERT INTO TERMINAL_GAME_TRANSLATION ( TGT_TERMINAL_ID " & _
    '            "                                        , TGT_SOURCE_GAME_ID " & _
    '            "                                        , TGT_TARGET_GAME_ID " & _
    '            "                                        ) " & _
    '            "                                 VALUES ( @TerminalId " & _
    '            "                                        , @SourceGameId " & _
    '            "                                        , @TargetGameId " & _
    '            "                                        ) "

    sql_query.Length = 0

    sql_query.AppendLine("DECLARE @P_GExist as Int  ")
    sql_query.AppendLine("set @P_GExist =  (SELECT COUNT(*) FROM providers_games where pg_pv_id = @ProvId AND pg_game_id = @TargetGameId)  ")
    sql_query.AppendLine("If @P_GExist = 0 BEGIN set @TargetGameId = NULL END  ")
    ' Change TGT_PAYOUT_IDX when TGT_TRANSLATED_GAME_ID changes
    sql_query.AppendLine("set @P_GExist = (SELECT TGT_PAYOUT_IDX FROM TERMINAL_GAME_TRANSLATION WHERE  TGT_TERMINAL_ID = @TerminalId AND TGT_SOURCE_GAME_ID = @SourceGameId AND TGT_TRANSLATED_GAME_ID = @TargetGameId)")

    sql_query.AppendLine(" IF EXISTS ( ")
    sql_query.AppendLine("                      SELECT   1 ")
    sql_query.AppendLine("             FROM   TERMINAL_GAME_TRANSLATION ")
    sql_query.AppendLine("            WHERE   TGT_TERMINAL_ID    = @TerminalId ")
    sql_query.AppendLine("              AND   TGT_SOURCE_GAME_ID = @SourceGameId ")
    sql_query.AppendLine("           ) ")
    sql_query.AppendLine("    UPDATE   TERMINAL_GAME_TRANSLATION ")
    sql_query.AppendLine("       SET   TGT_TRANSLATED_GAME_ID = @TargetGameId ")
    sql_query.AppendLine("           , TGT_UPDATED        = GETDATE() ")
    sql_query.AppendLine("           , TGT_PAYOUT_IDX     = @P_GExist ")
    sql_query.AppendLine("     WHERE   TGT_TERMINAL_ID    = @TerminalId ")
    sql_query.AppendLine("       AND   TGT_SOURCE_GAME_ID = @SourceGameId ")
    sql_query.AppendLine(" ELSE ")
    sql_query.AppendLine("  INSERT INTO TERMINAL_GAME_TRANSLATION ( TGT_TERMINAL_ID ")
    sql_query.AppendLine("                                        , TGT_SOURCE_GAME_ID ")
    sql_query.AppendLine("                                        , TGT_TARGET_GAME_ID ")
    sql_query.AppendLine("                                        , TGT_TRANSLATED_GAME_ID ")
    sql_query.AppendLine("                                        ) ")
    sql_query.AppendLine("                                 VALUES ( @TerminalId ")
    sql_query.AppendLine("                                        , @SourceGameId ")
    sql_query.AppendLine("                                        , @SourceGameId ")
    sql_query.AppendLine("                                        , @TargetGameId")
    sql_query.AppendLine("                                        ) ")

    sql_adap.UpdateCommand = New SqlCommand(sql_query.ToString(), Trx.Connection, Trx)
    sql_adap.UpdateCommand.Parameters.Add("@TerminalId", SqlDbType.Int, 0, "T3GS_TERMINAL_ID")
    sql_adap.UpdateCommand.Parameters.Add("@SourceGameId", SqlDbType.Int, 0, "GAME_3GS_ID")
    sql_adap.UpdateCommand.Parameters.Add("@TargetGameId", SqlDbType.Int, 0, "GAME_ID")
    sql_adap.UpdateCommand.Parameters.Add("@ProvId", SqlDbType.Int, 0, "TE_PROV_ID")

    sql_adap.InsertCommand = New SqlCommand(sql_query.ToString(), Trx.Connection, Trx)
    sql_adap.InsertCommand.Parameters.Add("@TerminalId", SqlDbType.Int, 0, "T3GS_TERMINAL_ID")
    sql_adap.InsertCommand.Parameters.Add("@SourceGameId", SqlDbType.Int, 0, "GAME_3GS_ID")
    sql_adap.InsertCommand.Parameters.Add("@TargetGameId", SqlDbType.Int, 0, "GAME_ID")
    sql_adap.InsertCommand.Parameters.Add("@ProvId", SqlDbType.Int, 0, "TE_PROV_ID")


    Try
      ' Array rows_updated have both modified and added rows.
      nr = sql_adap.Update(rows_updated)
      If rows_updated.Length <> nr Then
        MsgError = GetMsgErrorFromRow(rows_updated)
        Return False
      End If

      m_relgames_added = m_sql_dataset_3gs.Tables(0).Select(m_data_view_3gs.RowFilter, "", DataViewRowState.Added).Length > 0

      Return True

    Finally
      CopyRowsCurrentValues(rows_updated, rows_orig)
    End Try

  End Function 'UpdateDataGame

  ' PURPOSE: Return an array of DataRow copied from rowsOrig based on tableOrig schema.
  '
  '  PARAMS:
  '     - INPUT:
  '           - TableOrig As DateTable
  '           - RowsOrig() As DataRow
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Array of copied DataRow

  Private Function CopyArrayRows(ByVal TableOrig As DataTable, ByVal RowsOrig() As DataRow) As DataRow()
    Dim table_dest As DataTable
    Dim row As DataRow

    table_dest = TableOrig.Clone()
    For Each row In RowsOrig
      table_dest.ImportRow(row)
    Next

    Return table_dest.Select()
  End Function ' CopyArrayRows

  ' PURPOSE: Copy only current item values from rowsOrig to rowsDest, preserving original values and RowState.
  '
  '  PARAMS:
  '     - INPUT:
  '           - RowsOrig() As DataRow
  '     - OUTPUT:
  '           - RowsDest() As DataRow
  '
  ' RETURNS:
  '     -
  Private Sub CopyRowsCurrentValues(ByVal RowsOrig() As DataRow, ByRef RowsDest() As DataRow)
    Dim idx_row As Integer
    Dim idx_dest As Integer
    Dim idx_col As Integer

    For idx_row = 0 To RowsOrig.Length - 1

      For idx_dest = 0 To RowsDest.Length - 1
        If RowsDest(idx_dest).Item("T3GS_TERMINAL_ID") = RowsOrig(idx_row).Item("T3GS_TERMINAL_ID") Then
          Exit For
        End If
      Next

      ' Critical Error: exit
      If idx_dest = RowsDest.Length Then
        ' Logger message 
        Debug.WriteLine("INTERNAL ERROR in terminals_3gs_edit.vb: CopyRowsCurrentValues()")
        Exit Sub
      End If

      For idx_col = 0 To RowsOrig(idx_row).ItemArray.Length - 1
        If Not RowsDest(idx_dest).Table.Columns(idx_col).ReadOnly() Then
          RowsDest(idx_dest).Item(idx_col) = RowsOrig(idx_row).Item(idx_col, DataRowVersion.Current)
        End If
      Next
      RowsDest(idx_dest).RowError = RowsOrig(idx_row).RowError
    Next

  End Sub ' CopyRowsCurrentValues

  ' PURPOSE: Return the UPDATE SqlCommand for the SQL Adapter for table terminals_pending.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - SqlCommand

  Private Function UpdateCommandPending() As SqlCommand

    Dim cmd As SqlCommand

    cmd = New SqlCommand("UPDATE TERMINALS_PENDING" & _
                           " SET TP_IGNORE = @Ignored" & _
                         " WHERE TP_ID = @TpId" & _
                           " AND TP_IGNORE = @IgnoredOrig")

    cmd.Parameters.Add("@TpId", SqlDbType.Int, 0, "TP_ID")

    cmd.Parameters.Add("@Ignored", SqlDbType.Bit, 1, "TP_IGNORE")

    cmd.Parameters.Add("@IgnoredOrig", SqlDbType.Bit, 1, "TP_IGNORE")
    cmd.Parameters("@IgnoredOrig").SourceVersion = DataRowVersion.Original

    Return cmd
  End Function 'UpdateCommandPending

  ' PURPOSE: If there are pending changes in Data Grids, show a MsgBox asking user to discard changes or not.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - True:  If discard changes
  '     - False: If NOT discard changes

  Private Function DiscardChanges() As Boolean
    Dim user_msg As Boolean
    Dim data_table_changes_terminals_pending As New DataTable
    Dim data_table_changes_terminals_3gs As New DataTable

    ' If pending changes MsgBox
    user_msg = False
    If Not IsNothing(m_data_view_pending) Then
      data_table_changes_terminals_pending = m_data_view_pending.Table.GetChanges

      If Not IsNothing(data_table_changes_terminals_pending) Then
        If data_table_changes_terminals_pending.Rows.Count > 0 Then
          If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then
            Return False
          End If
          user_msg = True
        End If
      End If
    End If

    If Not IsNothing(m_data_view_3gs) And Not user_msg Then
      data_table_changes_terminals_3gs = m_data_view_3gs.Table.GetChanges

      If Not IsNothing(data_table_changes_terminals_3gs) Then
        If data_table_changes_terminals_3gs.Rows.Count > 0 Then
          If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then
            Return False
          End If
        End If
      End If
    End If

    Return True
  End Function 'DiscardChanges

  ' PURPOSE: Write auditory changes made in data table ChangesDT, that is related with pending terminals table.
  '
  '  PARAMS:
  '     - INPUT:
  '           - ChangesDT As DataTable
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub GUI_WriteAuditoryChangesPending(ByVal ChangesDT As DataTable)
    Dim original_modified_dv As DataView
    Dim auditor_data As CLASS_AUDITOR_DATA
    Dim original_auditor_data As CLASS_AUDITOR_DATA
    Dim idx_change As Integer
    Dim idx_original As Integer
    Dim value_string As String

    If IsNothing(ChangesDT) Then
      Exit Sub
    End If

    original_modified_dv = New DataView(ChangesDT, "", "", DataViewRowState.ModifiedOriginal)

    For idx_change = 0 To ChangesDT.Rows.Count - 1

      If ChangesDT.Rows(idx_change).RowState = DataRowState.Modified Then

        auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)
        original_auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)

        ' Set Name
        value_string = ChangesDT.Rows(idx_change).Item("TP_VENDOR_ID") & _
                 "." & ChangesDT.Rows(idx_change).Item("TP_SERIAL_NUMBER")
        '"." & ChangesDT.Rows(idx_change).Item("TP_MACHINE_NUMBER")

        Call auditor_data.SetName(GLB_NLS_GUI_AUDITOR.Id(345), value_string)
        Call original_auditor_data.SetName(GLB_NLS_GUI_AUDITOR.Id(345), value_string)

        '
        ' Details for current rows
        '

        ' Ignore
        value_string = IIf(IsDBNull(ChangesDT.Rows(idx_change).Item("TP_IGNORE")), "", ChangesDT.Rows(idx_change).Item("TP_IGNORE"))
        Call auditor_data.SetField(GLB_NLS_GUI_AUDITOR.Id(352), IIf(value_string = "", AUDIT_NONE_STRING, value_string))

        ' Search original row
        For idx_original = 0 To original_modified_dv.Count - 1
          If original_modified_dv(idx_original).Item("TP_ID") = ChangesDT.Rows(idx_change).Item("TP_ID") Then

            Exit For
          End If
        Next

        ' Critical Error: exit
        If idx_original = original_modified_dv.Count Then
          ' Logger message 

          Exit Sub
        End If

        '
        ' Details for original rows
        '

        ' Ignore
        value_string = IIf(IsDBNull(original_modified_dv(idx_original).Item("TP_IGNORE")), "", original_modified_dv(idx_original).Item("TP_IGNORE"))
        Call original_auditor_data.SetField(GLB_NLS_GUI_AUDITOR.Id(352), IIf(value_string = "", AUDIT_NONE_STRING, value_string))

        If Not auditor_data.Notify(GLB_CurrentUser.GuiId, _
                                       GLB_CurrentUser.Id, _
                                       GLB_CurrentUser.Name, _
                                       CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.UPDATE, _
                                       0, _
                                       original_auditor_data) Then
          ' Logger message 
        End If

        auditor_data = Nothing
        original_auditor_data = Nothing
      End If
    Next

  End Sub ' GUI_WriteAuditoryChangesPending

  ' PURPOSE: Write auditory changes made in data table ChangesDT, that is related with global and 3gs terminals table.
  '
  '  PARAMS:
  '     - INPUT:
  '           - ChangesDT As DataTable
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub GUI_WriteAuditoryChanges3gs(ByVal ChangesDT As DataTable)

    Dim original_modified_dv As DataView
    Dim idx_change As Integer

    If IsNothing(ChangesDT) Then
      Exit Sub
    End If

    original_modified_dv = New DataView(ChangesDT, "", "", DataViewRowState.ModifiedOriginal)

    For idx_change = 0 To ChangesDT.Rows.Count - 1

      If ChangesDT.Rows(idx_change).RowState = DataRowState.Added Or _
         ChangesDT.Rows(idx_change).RowState = DataRowState.Modified Then

        AuditoryRowChangeTerminal(TYPE_GLOBAL, ChangesDT.Rows(idx_change), original_modified_dv)
        AuditoryRowChangeTerminal(TYPE_3GS, ChangesDT.Rows(idx_change), original_modified_dv)

      End If
    Next

  End Sub ' GUI_WriteAuditoryChanges3gs

  ' PURPOSE: Write auditory changes made in a Row, depending of Type (Global or 3gs terminal).
  '
  '  PARAMS:
  '     - INPUT:
  '           - Type As Integer: TYPE_GLOBAL, TYPE_3GS
  '           - Row As DataRow
  '           - OrigDV As DataView
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub AuditoryRowChangeTerminal(ByVal Type As Integer, ByVal Row As DataRow, ByVal OrigDV As DataView)

    Dim auditor_data As CLASS_AUDITOR_DATA
    Dim original_auditor_data As CLASS_AUDITOR_DATA
    Dim has_changes As Boolean = False

    auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)
    original_auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)

    Select Case Type
      Case TYPE_GLOBAL
        ' If not added data... exit.
        If Row.RowState = DataRowState.Added And Not m_terms_global_added Then
          auditor_data = Nothing
          original_auditor_data = Nothing

          Exit Sub
        End If

        AuditoryRowTerminalGlobal(Row, auditor_data, original_auditor_data, OrigDV, has_changes)

      Case TYPE_3GS
        ' If not added data... exit.
        If Row.RowState = DataRowState.Added And Not m_terms_3gs_added Then
          auditor_data = Nothing
          original_auditor_data = Nothing

          Exit Sub
        End If

        AuditoryRowTerminal3gs(Row, auditor_data, original_auditor_data, OrigDV, has_changes)
    End Select

    Select Case Row.RowState
      Case DataRowState.Modified
        If has_changes Then
          If Not auditor_data.Notify(GLB_CurrentUser.GuiId, _
                                     GLB_CurrentUser.Id, _
                                     GLB_CurrentUser.Name, _
                                     CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.UPDATE, _
                                     0, _
                                     original_auditor_data) Then
            ' Logger message 
          End If
        End If

      Case DataRowState.Added
        If Not auditor_data.Notify(GLB_CurrentUser.GuiId, _
                                   GLB_CurrentUser.Id, _
                                   GLB_CurrentUser.Name, _
                                   CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.INSERT, _
                                   0) Then
          ' Logger message 
        End If
    End Select

    auditor_data = Nothing
    original_auditor_data = Nothing

  End Sub ' AuditoryRowChangeTerminal

  ' PURPOSE: Set auditory data for a Row related with global terminals.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row As DataRow
  '           - AuditorData As CLASS_AUDITOR_DATA
  '           - AuditorDataOrig As CLASS_AUDITOR_DATA
  '           - OrigDV As DataView
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub AuditoryRowTerminalGlobal(ByVal Row As DataRow, ByVal AuditorData As CLASS_AUDITOR_DATA, _
                                        ByVal AuditorDataOrig As CLASS_AUDITOR_DATA, ByVal OrigDV As DataView, _
                                        ByRef HasChanges As Boolean)
    Dim idx_original As Integer
    Dim value_string As String
    Dim value_name As String
    Dim value_vendor As String
    Dim value_serial As String
    Dim value_provider As String

    HasChanges = False

    ' Set Name and Identifier
    Call AuditorData.SetName(GLB_NLS_GUI_AUDITOR.Id(333), Row.Item("TE_NAME"))
    Call AuditorDataOrig.SetName(GLB_NLS_GUI_AUDITOR.Id(333), Row.Item("TE_NAME"))

    'Set Related type and related id
    Call AuditorData.SetRelated(CLASS_AUDITOR_DATA.ENUM_AUDITOR_RELATED.TERMINAL, Row.Item("T3GS_TERMINAL_ID"))
    Call AuditorDataOrig.SetRelated(CLASS_AUDITOR_DATA.ENUM_AUDITOR_RELATED.TERMINAL, Row.Item("T3GS_TERMINAL_ID"))
    '
    ' Details for current rows
    '

    ' Name
    value_name = IIf(IsDBNull(Row.Item("TE_NAME")), "", Row.Item("TE_NAME"))
    Call AuditorData.SetField(GLB_NLS_GUI_AUDITOR.Id(330), IIf(value_name = "", AUDIT_NONE_STRING, value_name))

    ' Vendor
    value_vendor = IIf(IsDBNull(Row.Item("T3GS_VENDOR_ID")), "", Row.Item("T3GS_VENDOR_ID"))
    Call AuditorData.SetField(GLB_NLS_GUI_CONFIGURATION.Id(205), IIf(value_vendor = "", AUDIT_NONE_STRING, value_vendor))

    ' Serial Number
    value_serial = IIf(IsDBNull(Row.Item("T3GS_SERIAL_NUMBER")), "", Row.Item("T3GS_SERIAL_NUMBER"))
    Call AuditorData.SetField(GLB_NLS_GUI_CONFIGURATION.Id(214), IIf(value_serial = "", AUDIT_NONE_STRING, value_serial))

    ' Provider
    value_provider = IIf(IsDBNull(Row.Item("TE_PROVIDER_ID")), "", Row.Item("TE_PROVIDER_ID"))
    Call AuditorData.SetField(GLB_NLS_GUI_AUDITOR.Id(341), IIf(value_provider = "", AUDIT_NONE_STRING, value_provider))

    ' Search original row
    If Row.RowState = DataRowState.Modified Then
      For idx_original = 0 To OrigDV.Count - 1
        If OrigDV(idx_original).Item("T3GS_TERMINAL_ID") = Row.Item("T3GS_TERMINAL_ID") Then
          Exit For
        End If
      Next

      ' Critical Error: exit
      If idx_original = OrigDV.Count Then
        ' Logger message 

        Exit Sub
      End If

      '
      ' Details for original rows
      '

      ' Name
      value_string = IIf(IsDBNull(OrigDV(idx_original).Item("TE_NAME")), "", OrigDV(idx_original).Item("TE_NAME"))
      If Not value_string.Equals(value_name) Then
        HasChanges = True
      End If
      Call AuditorDataOrig.SetField(GLB_NLS_GUI_AUDITOR.Id(330), IIf(value_string = "", AUDIT_NONE_STRING, value_string))

      ' Vendor
      value_string = IIf(IsDBNull(OrigDV(idx_original).Item("T3GS_VENDOR_ID")), "", OrigDV(idx_original).Item("T3GS_VENDOR_ID"))
      If Not value_string.Equals(value_vendor) Then
        HasChanges = True
      End If
      Call AuditorDataOrig.SetField(GLB_NLS_GUI_CONFIGURATION.Id(205), IIf(value_string = "", AUDIT_NONE_STRING, value_string))

      ' Serial Number
      value_string = IIf(IsDBNull(OrigDV(idx_original).Item("T3GS_SERIAL_NUMBER")), "", OrigDV(idx_original).Item("T3GS_SERIAL_NUMBER"))
      If Not value_string.Equals(value_serial) Then
        HasChanges = True
      End If
      Call AuditorDataOrig.SetField(GLB_NLS_GUI_CONFIGURATION.Id(214), IIf(value_string = "", AUDIT_NONE_STRING, value_string))

      ' Provider
      value_string = IIf(IsDBNull(OrigDV(idx_original).Item("TE_PROVIDER_ID")), "", OrigDV(idx_original).Item("TE_PROVIDER_ID"))
      If Not value_string.Equals(value_provider) Then
        HasChanges = True
      End If
      Call AuditorDataOrig.SetField(GLB_NLS_GUI_AUDITOR.Id(341), IIf(value_string = "", AUDIT_NONE_STRING, value_string))

    End If

  End Sub ' AuditoryRowTerminalGlobal

  ' PURPOSE: Set auditory data for a Row related with 3gs terminals.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row As DataRow
  '           - AuditorData As CLASS_AUDITOR_DATA
  '           - AuditorDataOrig As CLASS_AUDITOR_DATA
  '           - OrigDV As DataView
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub AuditoryRowTerminal3gs(ByVal Row As DataRow, ByVal AuditorData As CLASS_AUDITOR_DATA, _
                                     ByVal AuditorDataOrig As CLASS_AUDITOR_DATA, ByVal OrigDV As DataView, _
                                     ByRef HasChanges As Boolean)
    Dim idx_original As Integer
    Dim value_string As String
    Dim value_vendor As String
    Dim value_serial As String
    Dim value_machine As String

    HasChanges = False

    ' Set Name and Identifier
    Call AuditorData.SetName(GLB_NLS_GUI_AUDITOR.Id(346), Row.Item("TE_NAME"))
    Call AuditorDataOrig.SetName(GLB_NLS_GUI_AUDITOR.Id(346), Row.Item("TE_NAME"))

    'Set Related type and related id
    Call AuditorData.SetRelated(CLASS_AUDITOR_DATA.ENUM_AUDITOR_RELATED.TERMINAL, Row.Item("T3GS_TERMINAL_ID"))
    Call AuditorDataOrig.SetRelated(CLASS_AUDITOR_DATA.ENUM_AUDITOR_RELATED.TERMINAL, Row.Item("T3GS_TERMINAL_ID"))

    '
    '
    ' Details for current rows
    '

    ' Vendor
    value_vendor = IIf(IsDBNull(Row.Item("T3GS_VENDOR_ID")), "", Row.Item("T3GS_VENDOR_ID"))
    Call AuditorData.SetField(GLB_NLS_GUI_CONFIGURATION.Id(205), IIf(value_vendor = "", AUDIT_NONE_STRING, value_vendor))

    ' Serial Number
    value_serial = IIf(IsDBNull(Row.Item("T3GS_SERIAL_NUMBER")), "", Row.Item("T3GS_SERIAL_NUMBER"))
    Call AuditorData.SetField(GLB_NLS_GUI_CONFIGURATION.Id(214), IIf(value_serial = "", AUDIT_NONE_STRING, value_serial))

    ' Machine Number
    value_machine = IIf(IsDBNull(Row.Item("T3GS_MACHINE_NUMBER")), "", Row.Item("T3GS_MACHINE_NUMBER"))
    Call AuditorData.SetField(GLB_NLS_GUI_CONFIGURATION.Id(211), IIf(value_machine = "", AUDIT_NONE_STRING, value_machine))

    ' Search original row
    If Row.RowState = DataRowState.Modified Then
      For idx_original = 0 To OrigDV.Count - 1
        If OrigDV(idx_original).Item("T3GS_TERMINAL_ID") = Row.Item("T3GS_TERMINAL_ID") Then
          Exit For
        End If
      Next

      ' Critical Error: exit
      If idx_original = OrigDV.Count Then
        ' Logger message 

        Exit Sub
      End If

      '
      ' Details for original rows
      '

      ' Vendor
      value_string = IIf(IsDBNull(OrigDV(idx_original).Item("T3GS_VENDOR_ID")), "", OrigDV(idx_original).Item("T3GS_VENDOR_ID"))
      If Not value_string.Equals(value_vendor) Then
        HasChanges = True
      End If
      Call AuditorDataOrig.SetField(GLB_NLS_GUI_CONFIGURATION.Id(205), IIf(value_string = "", AUDIT_NONE_STRING, value_string))

      ' Serial Number
      value_string = IIf(IsDBNull(OrigDV(idx_original).Item("T3GS_SERIAL_NUMBER")), "", OrigDV(idx_original).Item("T3GS_SERIAL_NUMBER"))
      If Not value_string.Equals(value_serial) Then
        HasChanges = True
      End If
      Call AuditorDataOrig.SetField(GLB_NLS_GUI_CONFIGURATION.Id(214), IIf(value_string = "", AUDIT_NONE_STRING, value_string))

      ' Machine Number
      value_string = IIf(IsDBNull(OrigDV(idx_original).Item("T3GS_MACHINE_NUMBER")), "", OrigDV(idx_original).Item("T3GS_MACHINE_NUMBER"))
      If Not value_string.Equals(value_machine) Then
        HasChanges = True
      End If
      Call AuditorDataOrig.SetField(GLB_NLS_GUI_CONFIGURATION.Id(211), IIf(value_string = "", AUDIT_NONE_STRING, value_string))

    End If

  End Sub ' AuditoryRowTerminal3gs

  ' PURPOSE: Write auditory changes made in data table ChangesDT, that is related with terminal-game relationship.
  '
  '  PARAMS:
  '     - INPUT:
  '           - ChangesDT As DataTable
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub GUI_WriteAuditoryChangesGames(ByVal ChangesDT As DataTable)
    Dim original_modified_dv As DataView
    Dim auditor_data As CLASS_AUDITOR_DATA
    Dim original_auditor_data As CLASS_AUDITOR_DATA
    Dim idx_change As Integer
    Dim idx_original As Integer
    Dim value_string As String
    Dim value_name As String
    Dim has_changes As Boolean

    If IsNothing(ChangesDT) Then
      Exit Sub
    End If

    original_modified_dv = New DataView(ChangesDT, "", "", DataViewRowState.ModifiedOriginal)

    '---------------------------------------------
    ' Insert MODIFIED items
    '---------------------------------------------
    auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINAL_GAME_RELATION)
    original_auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINAL_GAME_RELATION)

    ' Set Parameter
    Call auditor_data.SetName(GLB_NLS_GUI_AUDITOR.Id(73), " ")
    Call original_auditor_data.SetName(GLB_NLS_GUI_AUDITOR.Id(73), " ")

    has_changes = False

    For idx_change = 0 To ChangesDT.Rows.Count - 1

      If ChangesDT.Rows(idx_change).RowState = DataRowState.Modified Then

        '
        ' Details for current rows
        '

        ' Value
        value_name = IIf(IsDBNull(ChangesDT.Rows(idx_change).Item("GAME_NAME")), "", ChangesDT.Rows(idx_change).Item("GAME_NAME"))
        Call auditor_data.SetField(0, IIf(value_name = "", AUDIT_NONE_STRING, value_name), _
                                   ChangesDT.Rows(idx_change).Item("TE_PROVIDER_ID") & _
                                   "." & ChangesDT.Rows(idx_change).Item("TE_NAME") & _
                                   "." & SOURCE_GAME_NAME_3GS)

        ' Search original row
        For idx_original = 0 To original_modified_dv.Count - 1
          If original_modified_dv(idx_original).Item("T3GS_TERMINAL_ID") = ChangesDT.Rows(idx_change).Item("T3GS_TERMINAL_ID") Then
            Exit For
          End If
        Next

        ' Critical Error: exit
        If idx_original = original_modified_dv.Count Then
          ' Logger message 

          Exit Sub
        End If

        '
        ' Details for original rows
        '

        ' Value
        value_string = IIf(IsDBNull(original_modified_dv(idx_original).Item("GAME_NAME")), "", original_modified_dv(idx_original).Item("GAME_NAME"))
        If Not value_string.Equals(value_name) Then
          has_changes = True
        End If
        Call original_auditor_data.SetField(0, IIf(value_string = "", AUDIT_NONE_STRING, value_string), ChangesDT.Rows(idx_change).Item("TE_PROVIDER_ID") & "." & ChangesDT.Rows(idx_change).Item("TE_NAME") & "." & SOURCE_GAME_NAME_3GS)

      End If
    Next

    If has_changes Then
      If Not auditor_data.Notify(GLB_CurrentUser.GuiId, _
                                 GLB_CurrentUser.Id, _
                                 GLB_CurrentUser.Name, _
                                 CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.UPDATE, _
                                 0, _
                                 original_auditor_data) Then
        ' Logger message 
      End If
    End If

    auditor_data = Nothing
    original_auditor_data = Nothing

    If m_relgames_added Then

      '---------------------------------------------
      ' Insert ADDED items
      '---------------------------------------------

      auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINAL_GAME_RELATION)

      ' Set Parameter
      Call auditor_data.SetName(GLB_NLS_GUI_AUDITOR.Id(73), " ")

      For idx_change = 0 To ChangesDT.Rows.Count - 1

        If ChangesDT.Rows(idx_change).RowState = DataRowState.Added Then

          '
          ' Details for current rows
          '

          ' Value
          value_string = IIf(IsDBNull(ChangesDT.Rows(idx_change).Item("GAME_NAME")), "", ChangesDT.Rows(idx_change).Item("GAME_NAME"))
          Call auditor_data.SetField(0, IIf(value_string = "", AUDIT_NONE_STRING, value_string), ChangesDT.Rows(idx_change).Item("TE_PROVIDER_ID") & "." & ChangesDT.Rows(idx_change).Item("TE_NAME") & "." & SOURCE_GAME_NAME_3GS)

        End If
      Next

      If Not auditor_data.Notify(GLB_CurrentUser.GuiId, _
                                 GLB_CurrentUser.Id, _
                                 GLB_CurrentUser.Name, _
                                 CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.INSERT, _
                                 0) Then
        ' Logger message 
      End If

      auditor_data = Nothing

    End If
  End Sub ' GUI_WriteAuditoryChangesGames

#End Region ' Private Functions

#Region " Public functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

  ' PURPOSE: Undelete a previous delete pending terminal row.
  '          Must be Public because Class MyDataGrid calls it.
  '
  '  PARAMS:
  '     - INPUT:
  '           - dataRowView As DataRowView
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Public Sub UndeleteAssignedRow(ByVal dataRowView As DataRowView)
    Dim rows_deleted() As DataRow
    Dim row As DataRow

    rows_deleted = m_sql_dataset_pending.Tables(0).Select("", "", DataViewRowState.Deleted)

    For Each row In rows_deleted
      If Not dataRowView.Item("TP_ID") Is DBNull.Value Then
        If dataRowView.Item("TP_ID") = row.Item("TP_ID", DataRowVersion.Original) Then
          row.RejectChanges()

          Exit For
        End If
      End If
    Next

    btn_add.Enabled = (m_data_view_pending.Count > 0 And Me.Permissions.Write)
    btn_replace.Enabled = (m_data_view_pending.Count > 0 And Me.Permissions.Write)

  End Sub ' UndeleteAssignedRow

#End Region ' Public functions

#Region " Sql Server Events "

  Private Shared Sub sql_adap_terms_global_OnRowUpdated(ByVal sender As Object, ByVal args As SqlRowUpdatedEventArgs)
    Dim new_id As Integer

    If args.StatementType = StatementType.Insert Then
      new_id = GetSqlIdentity(args.Command)
      If new_id <> 0 Then
        args.Row("T3GS_TERMINAL_ID") = new_id
      End If
    End If

  End Sub ' sql_adap_terms_global_OnRowUpdated

  Private Shared Sub sql_adap_games_OnRowUpdated(ByVal sender As Object, ByVal args As SqlRowUpdatedEventArgs)
    Dim new_id As Integer

    If args.StatementType = StatementType.Insert Then
      new_id = GetSqlIdentity(args.Command)
      If new_id <> 0 Then
        args.Row("GM_GAME_ID") = new_id
      End If
    End If

  End Sub ' sql_adap_games_OnRowUpdated

  Private Shared Function GetSqlIdentity(ByVal Command As SqlCommand)
    Dim new_id As Integer = 0
    Dim value As Object
    Dim db_identity_cmd As SqlCommand

    ' Don't use Scope_Identity() for Sql Server, it will not work because the change in command object
    ' changes the scope from the original command.
    db_identity_cmd = New SqlCommand("SELECT @@IDENTITY")
    db_identity_cmd.Connection = Command.Connection
    db_identity_cmd.Transaction = Command.Transaction

    ' Retrieve the identity value and store it in the CategoryID column.
    value = db_identity_cmd.ExecuteScalar()
    If value IsNot Nothing And value IsNot DBNull.Value Then
      new_id = CInt(value)
    End If

    Return new_id
  End Function

#End Region ' Sql Server Events

#Region " DataGrid Events "

  ' PURPOSE: Propose specific values for new rows.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '
  Private Sub dg_terminals_3gs_CurrentCellChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dg_terminals_3gs.CurrentCellChanged
    Dim grid_cell As DataGridCell
    Dim rows() As DataRow
    Dim row As DataRow

    Try
      grid_cell = dg_terminals_3gs.CurrentCell()

      If m_data_view_3gs.RowFilter <> "" Then

        ' Add new possible filters to the RowFilter.
        rows = m_sql_dataset_3gs.Tables(0).Select("", "", DataViewRowState.ModifiedCurrent Or DataViewRowState.Added)
        For Each row In rows
          AddFilter3gs(row)
        Next

      End If

      If grid_cell.RowNumber < 1 Then
        Return
      End If

      If grid_cell.RowNumber <> m_data_view_3gs.Count - 1 Then
        Return
      End If

      Select Case grid_cell.ColumnNumber
        Case GRID_COLUMN_3GS_VENDOR_ID
          If m_data_view_3gs(grid_cell.RowNumber).Item("T3GS_VENDOR_ID") = "" Then
            m_data_view_3gs(grid_cell.RowNumber).Item("T3GS_VENDOR_ID") = _
                                      m_data_view_3gs(grid_cell.RowNumber - 1).Item("T3GS_VENDOR_ID")
          End If
        Case GRID_COLUMN_3GS_MACHINE_NUMBER
          If m_data_view_3gs(grid_cell.RowNumber).Item("T3GS_MACHINE_NUMBER") Is DBNull.Value Then
            m_data_view_3gs(grid_cell.RowNumber).Item("T3GS_MACHINE_NUMBER") = _
                              m_data_view_3gs(grid_cell.RowNumber - 1).Item("T3GS_MACHINE_NUMBER") + 1
          End If

        Case 6
          If m_data_view_3gs(grid_cell.RowNumber).Item("T3GS_MACHINE_NUMBER") Is DBNull.Value Then
            m_data_view_3gs(grid_cell.RowNumber).Item("T3GS_MACHINE_NUMBER") = _
                              m_data_view_3gs(grid_cell.RowNumber - 1).Item("T3GS_MACHINE_NUMBER") + 1
          End If

      End Select

    Catch ex As Exception
      Debug.WriteLine(ex.Message)
    End Try

  End Sub 'dg_terminals_3gs_CurrentCellChanged

  ' PURPOSE: Prevents exit while inside the DataGrid.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  Private Sub dg_terminals_pending_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dg_terminals_pending.Enter
    Me.CancelButton = Nothing
  End Sub ' dg_terminals_pending_Enter

  ' PURPOSE: Restore Cancel Button after leaving the DataGrid.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  Private Sub dg_terminals_pending_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dg_terminals_pending.Leave
    Me.CancelButton = GUI_Button(ENUM_BUTTON.BUTTON_CANCEL)
  End Sub ' dg_terminals_pending_Enter

  ' PURPOSE: Prevents exit while inside the DataGrid.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  Private Sub dg_terminals_3gs_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dg_terminals_3gs.Enter
    Me.CancelButton = Nothing
  End Sub ' dg_terminals_3gs_Enter

  ' PURPOSE: Restore Cancel Button after leaving the DataGrid.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  Private Sub dg_terminals_3gs_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dg_terminals_3gs.Leave
    Me.CancelButton = GUI_Button(ENUM_BUTTON.BUTTON_CANCEL)
  End Sub ' dg_terminals_3gs_Enter

#End Region ' DataGrid Events

#Region " Button Events "

  ' PURPOSE : Event handler routine for the ADD pending terminal button.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Sub btn_add_ClickEvent() Handles btn_add.ClickEvent

    Call GUI_AssignPendingTerminal()

  End Sub   ' btn_assign_ClickEvent

  ' PURPOSE : Event handler routine for the REPLACE pending terminal button.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Sub btn_replace_ClickEvent() Handles btn_replace.ClickEvent

    Call GUI_ReplacePendingTerminal()

  End Sub   ' btn_replace_ClickEvent

#End Region

#Region " GUI Reports "

  ' PURPOSE: Initialize report filters.
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData As GUI_Reports.CLASS_PRINT_DATA
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(345) & " " & GLB_NLS_GUI_CONFIGURATION.GetString(205), m_report_vendor)
    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(345) & " " & GLB_NLS_GUI_CONFIGURATION.GetString(214), m_report_serial)

    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(346) & " " & GLB_NLS_GUI_CONFIGURATION.GetString(205), m_report_vendor_3G)
    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(346) & " " & GLB_NLS_GUI_CONFIGURATION.GetString(214), m_report_serial_3G)
    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(353), m_report_ignored)

    PrintData.FilterValueWidth(1) = 3000
    PrintData.FilterHeaderWidth(1) = 1500

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Update report filters.
  '
  '  PARAMS:
  '     - INPUT:
  '           - MyDataGrid As DataGrid
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters(ByVal MyDataGrid As DataGrid)

    Call MyBase.GUI_ReportUpdateFilters(MyDataGrid)

    m_report_vendor_3G = Me.ef_terminal_3gs_vendor.Value
    m_report_serial_3G = Me.ef_terminal_3gs_serial_number.Value

    m_report_vendor = Me.ef_terminal_pending_vendor.Value
    m_report_serial = Me.ef_terminal_pending_serial_number.Value

    If Me.chk_hide_ignored.Checked Then
      m_report_ignored = GLB_NLS_GUI_AUDITOR.GetString(336)
    Else
      m_report_ignored = GLB_NLS_GUI_AUDITOR.GetString(337)
    End If


  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

End Class ' frm_terminals_3gs_edit

'
' PURPOSE: Own version of DataGrid to be able to process keys inside DataGrid.
'
Public Class MyDataGrid
  Inherits System.Windows.Forms.DataGrid

  Public m_hit_info As DataGrid.HitTestInfo

  ' PURPOSE: Own version for OnMouseDown event routine. Just get HitTest info.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '
  Protected Overrides Sub OnMouseDown(ByVal e As System.Windows.Forms.MouseEventArgs)
    m_hit_info = Me.HitTest(e.X, e.Y)
    Call MyBase.OnMouseDown(e)
  End Sub ' OnMouseDown

  ' PURPOSE: Process pressed keys inside DataGrid.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - True:  If the key was processed by the DataGrid
  '     - False: Otherwise
  '
  Protected Overrides Function ProcessDialogKey(ByVal keyData As System.Windows.Forms.Keys) As Boolean
    Dim rc As Boolean

    If m_hit_info Is Nothing Then
      Return MyBase.ProcessDialogKey(keyData)
    End If
    If m_hit_info.Type = HitTestType.None Then
      Return True
    End If

    If keyData <> Keys.Delete Then
      Return MyBase.ProcessDialogKey(keyData)
    End If

    Select Case m_hit_info.Type
      Case HitTestType.RowHeader
        rc = ProcessKeyDeleteRowHeader()
        If rc Then
          Return True
        End If
    End Select

    Return MyBase.ProcessDialogKey(keyData)
  End Function ' ProcessDialogKey

  ' PURPOSE: Process DEL key inside DataGrid.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - True:  If the key was processed by the DataGrid
  '     - False: Otherwise
  '
  Private Function ProcessKeyDeleteRowHeader() As Boolean
    Dim data_view As DataView
    Dim data_row_view As DataRowView
    Dim form As frm_terminals_3gs_edit
    Dim idx_row As Integer
    Dim select_idx As Collection

    If CurrentRowIndex = -1 Then
      Return True
    End If

    select_idx = New Collection()
    data_view = Me.DataSource

    ' Need to save selected index before process them, if not, the selected property for all indexes is deleted after the
    ' first index processed. Also save them in reverse order (highest to lowest), because data view is modified while
    ' processing.
    For idx_row = data_view.Count - 1 To 0 Step -1
      If Me.IsSelected(idx_row) Then
        select_idx.Add(idx_row)
      End If
    Next

    If select_idx.Count = 0 Then
      select_idx.Add(CurrentRowIndex)
    End If

    form = Me.FindForm()

    For Each idx_row In select_idx
      data_row_view = data_view(idx_row)

      Select Case data_row_view.Row.RowState
        Case DataRowState.Added
          form.UndeleteAssignedRow(data_row_view)
          data_row_view.Row.RejectChanges()
        Case DataRowState.Detached
          data_row_view.CancelEdit()
      End Select
    Next

    ' Restore original RowHeaderWidth.
    If form.dg_terminals_3gs.TableStyles.Count > 0 Then
      form.dg_terminals_3gs.TableStyles(0).RowHeaderWidth = form.m_original_row_header_width_3gs
    End If

    Return True
  End Function ' ProcessKeyDeleteRowHeader

End Class ' MyDataGrid

'
' PURPOSE: Own version of DataGridTextBoxColumn to be able to paint the column.
'
Public Class ColoredTextBoxColumn
  Inherits DataGridTextBoxColumn

  ' PURPOSE: Paint ERROR rows in red.
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overloads Overrides Sub Paint(ByVal g As Graphics, _
       ByVal bounds As Rectangle, ByVal source As _
       CurrencyManager, ByVal rowNum As Integer, ByVal _
       BackColorBrush As Brush, ByVal ForeColorBrush As Brush, _
       ByVal AlignmentRight As Boolean)

    Dim data_view As DataView

    data_view = source.List
    Try
      If data_view(rowNum).Row.HasErrors Then
        BackColorBrush = Brushes.Red
        ForeColorBrush = Brushes.White
      End If
    Catch
    End Try

    MyBase.Paint(g, bounds, source, rowNum, _
        BackColorBrush, ForeColorBrush, AlignmentRight)
  End Sub ' Paint

End Class ' ColoredTextBoxColumn
