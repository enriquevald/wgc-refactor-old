<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_promotion_preassigned_edit
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_promotion_preassigned_edit))
    Me.ef_promo_name = New GUI_Controls.uc_entry_field()
    Me.gb_credit_type = New System.Windows.Forms.GroupBox()
    Me.opt_point = New System.Windows.Forms.RadioButton()
    Me.opt_credit_redeemable = New System.Windows.Forms.RadioButton()
    Me.opt_credit_non_redeemable = New System.Windows.Forms.RadioButton()
    Me.gb_enabled = New System.Windows.Forms.GroupBox()
    Me.opt_disabled = New System.Windows.Forms.RadioButton()
    Me.opt_enabled = New System.Windows.Forms.RadioButton()
    Me.uc_promo_schedule = New GUI_Controls.uc_schedule()
    Me.gb_account = New System.Windows.Forms.GroupBox()
    Me.opt_account_summary = New System.Windows.Forms.RadioButton()
    Me.opt_all_account = New System.Windows.Forms.RadioButton()
    Me.rtb_report = New System.Windows.Forms.RichTextBox()
    Me.btn_import = New GUI_Controls.uc_button()
    Me.ef_expiration_days = New GUI_Controls.uc_entry_field()
    Me.cmb_categories = New GUI_Controls.uc_combo()
    Me.img_icon = New GUI_Controls.uc_image()
    Me.lbl_icon = New System.Windows.Forms.Label()
    Me.lbl_icon_optimun_size = New System.Windows.Forms.Label()
    Me.lbl_image_optimun_size = New System.Windows.Forms.Label()
    Me.img_image = New GUI_Controls.uc_image()
    Me.lbl_image = New System.Windows.Forms.Label()
    Me.tc_ticketfooter_promobox = New System.Windows.Forms.TabControl()
    Me.tab_ticketfooter = New System.Windows.Forms.TabPage()
    Me.tb_ticket_footer_prasigned_promotion = New System.Windows.Forms.TextBox()
    Me.tab_promobox = New System.Windows.Forms.TabPage()
    Me.lbl_apply_on = New System.Windows.Forms.Label()
    Me.chk_award_on_InTouch = New System.Windows.Forms.CheckBox()
    Me.chk_award_on_PromoBOX = New System.Windows.Forms.CheckBox()
    Me.lbl_no_text_on_promobox_info = New System.Windows.Forms.Label()
    Me.ef_text_on_PromoBOX = New GUI_Controls.uc_entry_field()
    Me.chk_visible_on_PromoBOX = New System.Windows.Forms.CheckBox()
    Me.cmb_award_with_game = New System.Windows.Forms.ComboBox()
    Me.chk_award_with_game = New System.Windows.Forms.CheckBox()
    Me.panel_data.SuspendLayout()
    Me.gb_credit_type.SuspendLayout()
    Me.gb_enabled.SuspendLayout()
    Me.gb_account.SuspendLayout()
    Me.tc_ticketfooter_promobox.SuspendLayout()
    Me.tab_ticketfooter.SuspendLayout()
    Me.tab_promobox.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.cmb_award_with_game)
    Me.panel_data.Controls.Add(Me.chk_award_with_game)
    Me.panel_data.Controls.Add(Me.tc_ticketfooter_promobox)
    Me.panel_data.Controls.Add(Me.lbl_image_optimun_size)
    Me.panel_data.Controls.Add(Me.img_image)
    Me.panel_data.Controls.Add(Me.lbl_image)
    Me.panel_data.Controls.Add(Me.img_icon)
    Me.panel_data.Controls.Add(Me.gb_credit_type)
    Me.panel_data.Controls.Add(Me.lbl_icon)
    Me.panel_data.Controls.Add(Me.lbl_icon_optimun_size)
    Me.panel_data.Controls.Add(Me.cmb_categories)
    Me.panel_data.Controls.Add(Me.ef_expiration_days)
    Me.panel_data.Controls.Add(Me.gb_account)
    Me.panel_data.Controls.Add(Me.uc_promo_schedule)
    Me.panel_data.Controls.Add(Me.gb_enabled)
    Me.panel_data.Controls.Add(Me.ef_promo_name)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(872, 625)
    '
    'ef_promo_name
    '
    Me.ef_promo_name.DoubleValue = 0.0R
    Me.ef_promo_name.IntegerValue = 0
    Me.ef_promo_name.IsReadOnly = False
    Me.ef_promo_name.Location = New System.Drawing.Point(12, 5)
    Me.ef_promo_name.Name = "ef_promo_name"
    Me.ef_promo_name.PlaceHolder = Nothing
    Me.ef_promo_name.Size = New System.Drawing.Size(382, 24)
    Me.ef_promo_name.SufixText = "Sufix Text"
    Me.ef_promo_name.SufixTextVisible = True
    Me.ef_promo_name.TabIndex = 0
    Me.ef_promo_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_promo_name.TextValue = ""
    Me.ef_promo_name.TextWidth = 87
    Me.ef_promo_name.Value = ""
    Me.ef_promo_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_credit_type
    '
    Me.gb_credit_type.Controls.Add(Me.opt_point)
    Me.gb_credit_type.Controls.Add(Me.opt_credit_redeemable)
    Me.gb_credit_type.Controls.Add(Me.opt_credit_non_redeemable)
    Me.gb_credit_type.Location = New System.Drawing.Point(8, 64)
    Me.gb_credit_type.Name = "gb_credit_type"
    Me.gb_credit_type.Size = New System.Drawing.Size(197, 100)
    Me.gb_credit_type.TabIndex = 2
    Me.gb_credit_type.TabStop = False
    Me.gb_credit_type.Text = "xCreditType"
    '
    'opt_point
    '
    Me.opt_point.Location = New System.Drawing.Point(15, 63)
    Me.opt_point.Name = "opt_point"
    Me.opt_point.Size = New System.Drawing.Size(135, 16)
    Me.opt_point.TabIndex = 2
    Me.opt_point.Text = "xPoint"
    '
    'opt_credit_redeemable
    '
    Me.opt_credit_redeemable.Location = New System.Drawing.Point(15, 40)
    Me.opt_credit_redeemable.Name = "opt_credit_redeemable"
    Me.opt_credit_redeemable.Size = New System.Drawing.Size(135, 16)
    Me.opt_credit_redeemable.TabIndex = 1
    Me.opt_credit_redeemable.Text = "xRedeemable"
    '
    'opt_credit_non_redeemable
    '
    Me.opt_credit_non_redeemable.Location = New System.Drawing.Point(15, 18)
    Me.opt_credit_non_redeemable.Name = "opt_credit_non_redeemable"
    Me.opt_credit_non_redeemable.Size = New System.Drawing.Size(135, 16)
    Me.opt_credit_non_redeemable.TabIndex = 0
    Me.opt_credit_non_redeemable.Text = "xNon-Redeemable"
    '
    'gb_enabled
    '
    Me.gb_enabled.Controls.Add(Me.opt_disabled)
    Me.gb_enabled.Controls.Add(Me.opt_enabled)
    Me.gb_enabled.Location = New System.Drawing.Point(211, 64)
    Me.gb_enabled.Name = "gb_enabled"
    Me.gb_enabled.Size = New System.Drawing.Size(198, 69)
    Me.gb_enabled.TabIndex = 3
    Me.gb_enabled.TabStop = False
    Me.gb_enabled.Text = "xEnabled"
    '
    'opt_disabled
    '
    Me.opt_disabled.Location = New System.Drawing.Point(15, 43)
    Me.opt_disabled.Name = "opt_disabled"
    Me.opt_disabled.Size = New System.Drawing.Size(70, 16)
    Me.opt_disabled.TabIndex = 1
    Me.opt_disabled.Text = "xNo"
    '
    'opt_enabled
    '
    Me.opt_enabled.Location = New System.Drawing.Point(15, 21)
    Me.opt_enabled.Name = "opt_enabled"
    Me.opt_enabled.Size = New System.Drawing.Size(70, 16)
    Me.opt_enabled.TabIndex = 0
    Me.opt_enabled.Text = "xYes"
    '
    'uc_promo_schedule
    '
    Me.uc_promo_schedule.CheckedDateTo = True
    Me.uc_promo_schedule.DateFrom = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_promo_schedule.DateTo = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_promo_schedule.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_promo_schedule.Location = New System.Drawing.Point(4, 174)
    Me.uc_promo_schedule.Name = "uc_promo_schedule"
    Me.uc_promo_schedule.SecondTime = False
    Me.uc_promo_schedule.SecondTimeFrom = 0
    Me.uc_promo_schedule.SecondTimeTo = 0
    Me.uc_promo_schedule.ShowCheckBoxDateTo = False
    Me.uc_promo_schedule.Size = New System.Drawing.Size(405, 239)
    Me.uc_promo_schedule.TabIndex = 4
    Me.uc_promo_schedule.TimeFrom = 0
    Me.uc_promo_schedule.TimeTo = 0
    Me.uc_promo_schedule.Weekday = 0
    '
    'gb_account
    '
    Me.gb_account.Controls.Add(Me.opt_account_summary)
    Me.gb_account.Controls.Add(Me.opt_all_account)
    Me.gb_account.Controls.Add(Me.rtb_report)
    Me.gb_account.Controls.Add(Me.btn_import)
    Me.gb_account.Location = New System.Drawing.Point(422, 179)
    Me.gb_account.Name = "gb_account"
    Me.gb_account.Size = New System.Drawing.Size(448, 441)
    Me.gb_account.TabIndex = 15
    Me.gb_account.TabStop = False
    Me.gb_account.Text = "xAccounts"
    '
    'opt_account_summary
    '
    Me.opt_account_summary.Appearance = System.Windows.Forms.Appearance.Button
    Me.opt_account_summary.Checked = True
    Me.opt_account_summary.Location = New System.Drawing.Point(12, 58)
    Me.opt_account_summary.Name = "opt_account_summary"
    Me.opt_account_summary.Size = New System.Drawing.Size(133, 30)
    Me.opt_account_summary.TabIndex = 1
    Me.opt_account_summary.TabStop = True
    Me.opt_account_summary.Text = "xAccount Summary"
    Me.opt_account_summary.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.opt_account_summary.UseVisualStyleBackColor = True
    '
    'opt_all_account
    '
    Me.opt_all_account.Appearance = System.Windows.Forms.Appearance.Button
    Me.opt_all_account.Location = New System.Drawing.Point(151, 57)
    Me.opt_all_account.Name = "opt_all_account"
    Me.opt_all_account.Size = New System.Drawing.Size(133, 31)
    Me.opt_all_account.TabIndex = 2
    Me.opt_all_account.Text = "xAll Accounts"
    Me.opt_all_account.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.opt_all_account.UseVisualStyleBackColor = True
    '
    'rtb_report
    '
    Me.rtb_report.Location = New System.Drawing.Point(11, 96)
    Me.rtb_report.Name = "rtb_report"
    Me.rtb_report.ReadOnly = True
    Me.rtb_report.Size = New System.Drawing.Size(428, 330)
    Me.rtb_report.TabIndex = 3
    Me.rtb_report.Text = ""
    '
    'btn_import
    '
    Me.btn_import.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_import.Location = New System.Drawing.Point(11, 20)
    Me.btn_import.Name = "btn_import"
    Me.btn_import.Size = New System.Drawing.Size(90, 30)
    Me.btn_import.TabIndex = 0
    Me.btn_import.ToolTipped = False
    Me.btn_import.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.USER
    '
    'ef_expiration_days
    '
    Me.ef_expiration_days.DoubleValue = 0.0R
    Me.ef_expiration_days.IntegerValue = 0
    Me.ef_expiration_days.IsReadOnly = False
    Me.ef_expiration_days.Location = New System.Drawing.Point(20, 410)
    Me.ef_expiration_days.Name = "ef_expiration_days"
    Me.ef_expiration_days.PlaceHolder = Nothing
    Me.ef_expiration_days.Size = New System.Drawing.Size(233, 24)
    Me.ef_expiration_days.SufixText = "xday(s)"
    Me.ef_expiration_days.SufixTextVisible = True
    Me.ef_expiration_days.SufixTextWidth = 80
    Me.ef_expiration_days.TabIndex = 5
    Me.ef_expiration_days.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_expiration_days.TextValue = ""
    Me.ef_expiration_days.TextWidth = 110
    Me.ef_expiration_days.Value = ""
    Me.ef_expiration_days.ValueForeColor = System.Drawing.Color.Blue
    '
    'cmb_categories
    '
    Me.cmb_categories.AllowUnlistedValues = False
    Me.cmb_categories.AutoCompleteMode = False
    Me.cmb_categories.IsReadOnly = False
    Me.cmb_categories.Location = New System.Drawing.Point(12, 34)
    Me.cmb_categories.Name = "cmb_categories"
    Me.cmb_categories.SelectedIndex = -1
    Me.cmb_categories.Size = New System.Drawing.Size(268, 24)
    Me.cmb_categories.SufixText = "Sufix Text"
    Me.cmb_categories.SufixTextVisible = True
    Me.cmb_categories.TabIndex = 1
    Me.cmb_categories.TextCombo = Nothing
    Me.cmb_categories.TextWidth = 87
    '
    'img_icon
    '
    Me.img_icon.AutoSize = True
    Me.img_icon.ButtonDeleteEnabled = True
    Me.img_icon.FreeResize = False
    Me.img_icon.Image = Nothing
    Me.img_icon.ImageInfoFont = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.img_icon.ImageLayout = System.Windows.Forms.ImageLayout.Center
    Me.img_icon.ImageName = Nothing
    Me.img_icon.Location = New System.Drawing.Point(516, 76)
    Me.img_icon.Margin = New System.Windows.Forms.Padding(0)
    Me.img_icon.Name = "img_icon"
    Me.img_icon.Size = New System.Drawing.Size(146, 102)
    Me.img_icon.TabIndex = 11
    Me.img_icon.Transparent = False
    '
    'lbl_icon
    '
    Me.lbl_icon.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.lbl_icon.ForeColor = System.Drawing.Color.Black
    Me.lbl_icon.Location = New System.Drawing.Point(538, 44)
    Me.lbl_icon.Name = "lbl_icon"
    Me.lbl_icon.Size = New System.Drawing.Size(103, 15)
    Me.lbl_icon.TabIndex = 9
    Me.lbl_icon.Text = "xIcon"
    Me.lbl_icon.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_icon_optimun_size
    '
    Me.lbl_icon_optimun_size.Font = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_icon_optimun_size.Location = New System.Drawing.Point(497, 57)
    Me.lbl_icon_optimun_size.Name = "lbl_icon_optimun_size"
    Me.lbl_icon_optimun_size.Size = New System.Drawing.Size(185, 15)
    Me.lbl_icon_optimun_size.TabIndex = 10
    Me.lbl_icon_optimun_size.Text = "xOptimunSize"
    Me.lbl_icon_optimun_size.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_image_optimun_size
    '
    Me.lbl_image_optimun_size.Font = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_image_optimun_size.Location = New System.Drawing.Point(667, 20)
    Me.lbl_image_optimun_size.Name = "lbl_image_optimun_size"
    Me.lbl_image_optimun_size.Size = New System.Drawing.Size(205, 15)
    Me.lbl_image_optimun_size.TabIndex = 13
    Me.lbl_image_optimun_size.Text = "xOptimunSize"
    Me.lbl_image_optimun_size.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'img_image
    '
    Me.img_image.AutoSize = True
    Me.img_image.ButtonDeleteEnabled = True
    Me.img_image.FreeResize = False
    Me.img_image.Image = Nothing
    Me.img_image.ImageInfoFont = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.img_image.ImageLayout = System.Windows.Forms.ImageLayout.Center
    Me.img_image.ImageName = Nothing
    Me.img_image.Location = New System.Drawing.Point(692, 39)
    Me.img_image.Margin = New System.Windows.Forms.Padding(0)
    Me.img_image.Name = "img_image"
    Me.img_image.Size = New System.Drawing.Size(152, 141)
    Me.img_image.TabIndex = 14
    Me.img_image.Transparent = False
    '
    'lbl_image
    '
    Me.lbl_image.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.lbl_image.ForeColor = System.Drawing.Color.Black
    Me.lbl_image.Location = New System.Drawing.Point(667, 7)
    Me.lbl_image.Name = "lbl_image"
    Me.lbl_image.Size = New System.Drawing.Size(205, 15)
    Me.lbl_image.TabIndex = 12
    Me.lbl_image.Text = "xImage"
    Me.lbl_image.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'tc_ticketfooter_promobox
    '
    Me.tc_ticketfooter_promobox.Controls.Add(Me.tab_ticketfooter)
    Me.tc_ticketfooter_promobox.Controls.Add(Me.tab_promobox)
    Me.tc_ticketfooter_promobox.Location = New System.Drawing.Point(8, 440)
    Me.tc_ticketfooter_promobox.Name = "tc_ticketfooter_promobox"
    Me.tc_ticketfooter_promobox.SelectedIndex = 0
    Me.tc_ticketfooter_promobox.Size = New System.Drawing.Size(397, 180)
    Me.tc_ticketfooter_promobox.TabIndex = 6
    '
    'tab_ticketfooter
    '
    Me.tab_ticketfooter.Controls.Add(Me.tb_ticket_footer_prasigned_promotion)
    Me.tab_ticketfooter.Location = New System.Drawing.Point(4, 22)
    Me.tab_ticketfooter.Name = "tab_ticketfooter"
    Me.tab_ticketfooter.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_ticketfooter.Size = New System.Drawing.Size(389, 154)
    Me.tab_ticketfooter.TabIndex = 0
    Me.tab_ticketfooter.Text = "xTicketFooter"
    Me.tab_ticketfooter.UseVisualStyleBackColor = True
    '
    'tb_ticket_footer_prasigned_promotion
    '
    Me.tb_ticket_footer_prasigned_promotion.Location = New System.Drawing.Point(13, 11)
    Me.tb_ticket_footer_prasigned_promotion.MaxLength = 250
    Me.tb_ticket_footer_prasigned_promotion.Multiline = True
    Me.tb_ticket_footer_prasigned_promotion.Name = "tb_ticket_footer_prasigned_promotion"
    Me.tb_ticket_footer_prasigned_promotion.Size = New System.Drawing.Size(359, 132)
    Me.tb_ticket_footer_prasigned_promotion.TabIndex = 2
    '
    'tab_promobox
    '
    Me.tab_promobox.Controls.Add(Me.lbl_apply_on)
    Me.tab_promobox.Controls.Add(Me.chk_award_on_InTouch)
    Me.tab_promobox.Controls.Add(Me.chk_award_on_PromoBOX)
    Me.tab_promobox.Controls.Add(Me.lbl_no_text_on_promobox_info)
    Me.tab_promobox.Controls.Add(Me.ef_text_on_PromoBOX)
    Me.tab_promobox.Controls.Add(Me.chk_visible_on_PromoBOX)
    Me.tab_promobox.Location = New System.Drawing.Point(4, 22)
    Me.tab_promobox.Name = "tab_promobox"
    Me.tab_promobox.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_promobox.Size = New System.Drawing.Size(389, 154)
    Me.tab_promobox.TabIndex = 1
    Me.tab_promobox.Text = "xPromoBOX"
    Me.tab_promobox.UseVisualStyleBackColor = True
    '
    'lbl_apply_on
    '
    Me.lbl_apply_on.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.lbl_apply_on.ForeColor = System.Drawing.Color.Black
    Me.lbl_apply_on.Location = New System.Drawing.Point(8, 37)
    Me.lbl_apply_on.Name = "lbl_apply_on"
    Me.lbl_apply_on.Size = New System.Drawing.Size(115, 17)
    Me.lbl_apply_on.TabIndex = 26
    Me.lbl_apply_on.Text = "xApplyOn"
    Me.lbl_apply_on.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'chk_award_on_InTouch
    '
    Me.chk_award_on_InTouch.AutoSize = True
    Me.chk_award_on_InTouch.Location = New System.Drawing.Point(270, 37)
    Me.chk_award_on_InTouch.Name = "chk_award_on_InTouch"
    Me.chk_award_on_InTouch.Size = New System.Drawing.Size(95, 17)
    Me.chk_award_on_InTouch.TabIndex = 24
    Me.chk_award_on_InTouch.Text = "xOnInTouch"
    Me.chk_award_on_InTouch.UseVisualStyleBackColor = True
    '
    'chk_award_on_PromoBOX
    '
    Me.chk_award_on_PromoBOX.AutoSize = True
    Me.chk_award_on_PromoBOX.Location = New System.Drawing.Point(145, 37)
    Me.chk_award_on_PromoBOX.Name = "chk_award_on_PromoBOX"
    Me.chk_award_on_PromoBOX.Size = New System.Drawing.Size(111, 17)
    Me.chk_award_on_PromoBOX.TabIndex = 25
    Me.chk_award_on_PromoBOX.Text = "xOnPromoBOX"
    Me.chk_award_on_PromoBOX.UseVisualStyleBackColor = True
    '
    'lbl_no_text_on_promobox_info
    '
    Me.lbl_no_text_on_promobox_info.Font = New System.Drawing.Font("Courier New", 8.25!)
    Me.lbl_no_text_on_promobox_info.ForeColor = System.Drawing.Color.Navy
    Me.lbl_no_text_on_promobox_info.Location = New System.Drawing.Point(128, 95)
    Me.lbl_no_text_on_promobox_info.Name = "lbl_no_text_on_promobox_info"
    Me.lbl_no_text_on_promobox_info.Size = New System.Drawing.Size(252, 38)
    Me.lbl_no_text_on_promobox_info.TabIndex = 21
    Me.lbl_no_text_on_promobox_info.Text = "xWithoutTextToShowOnlyTheAmountWillBeDisplayed"
    '
    'ef_text_on_PromoBOX
    '
    Me.ef_text_on_PromoBOX.DoubleValue = 0.0R
    Me.ef_text_on_PromoBOX.IntegerValue = 0
    Me.ef_text_on_PromoBOX.IsReadOnly = False
    Me.ef_text_on_PromoBOX.Location = New System.Drawing.Point(3, 66)
    Me.ef_text_on_PromoBOX.Name = "ef_text_on_PromoBOX"
    Me.ef_text_on_PromoBOX.PlaceHolder = Nothing
    Me.ef_text_on_PromoBOX.Size = New System.Drawing.Size(382, 24)
    Me.ef_text_on_PromoBOX.SufixText = "Sufix Text"
    Me.ef_text_on_PromoBOX.SufixTextVisible = True
    Me.ef_text_on_PromoBOX.TabIndex = 19
    Me.ef_text_on_PromoBOX.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_text_on_PromoBOX.TextValue = ""
    Me.ef_text_on_PromoBOX.TextVisible = False
    Me.ef_text_on_PromoBOX.TextWidth = 120
    Me.ef_text_on_PromoBOX.Value = ""
    Me.ef_text_on_PromoBOX.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_visible_on_PromoBOX
    '
    Me.chk_visible_on_PromoBOX.AutoSize = True
    Me.chk_visible_on_PromoBOX.Location = New System.Drawing.Point(8, 12)
    Me.chk_visible_on_PromoBOX.Name = "chk_visible_on_PromoBOX"
    Me.chk_visible_on_PromoBOX.Size = New System.Drawing.Size(148, 17)
    Me.chk_visible_on_PromoBOX.TabIndex = 17
    Me.chk_visible_on_PromoBOX.Text = "xVisibleOnPromoBOX"
    Me.chk_visible_on_PromoBOX.UseVisualStyleBackColor = True
    '
    'cmb_award_with_game
    '
    Me.cmb_award_with_game.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.cmb_award_with_game.FormattingEnabled = True
    Me.cmb_award_with_game.Location = New System.Drawing.Point(548, 7)
    Me.cmb_award_with_game.Name = "cmb_award_with_game"
    Me.cmb_award_with_game.Size = New System.Drawing.Size(141, 21)
    Me.cmb_award_with_game.TabIndex = 8
    '
    'chk_award_with_game
    '
    Me.chk_award_with_game.AutoSize = True
    Me.chk_award_with_game.Location = New System.Drawing.Point(422, 9)
    Me.chk_award_with_game.Name = "chk_award_with_game"
    Me.chk_award_with_game.Size = New System.Drawing.Size(128, 17)
    Me.chk_award_with_game.TabIndex = 7
    Me.chk_award_with_game.Text = "xAwardWithGame"
    Me.chk_award_with_game.UseVisualStyleBackColor = True
    '
    'frm_promotion_preassigned_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(991, 636)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_promotion_preassigned_edit"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_promotion_preassigned_edit"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.gb_credit_type.ResumeLayout(False)
    Me.gb_enabled.ResumeLayout(False)
    Me.gb_account.ResumeLayout(False)
    Me.tc_ticketfooter_promobox.ResumeLayout(False)
    Me.tab_ticketfooter.ResumeLayout(False)
    Me.tab_ticketfooter.PerformLayout()
    Me.tab_promobox.ResumeLayout(False)
    Me.tab_promobox.PerformLayout()
    Me.ResumeLayout(False)

End Sub
  Friend WithEvents ef_promo_name As GUI_Controls.uc_entry_field
  Friend WithEvents gb_credit_type As System.Windows.Forms.GroupBox
  Friend WithEvents opt_point As System.Windows.Forms.RadioButton
  Friend WithEvents opt_credit_redeemable As System.Windows.Forms.RadioButton
  Friend WithEvents opt_credit_non_redeemable As System.Windows.Forms.RadioButton
  Friend WithEvents gb_enabled As System.Windows.Forms.GroupBox
  Friend WithEvents opt_disabled As System.Windows.Forms.RadioButton
  Friend WithEvents opt_enabled As System.Windows.Forms.RadioButton
  Friend WithEvents uc_promo_schedule As GUI_Controls.uc_schedule
  Friend WithEvents gb_account As System.Windows.Forms.GroupBox
  Friend WithEvents btn_import As GUI_Controls.uc_button
  Friend WithEvents rtb_report As System.Windows.Forms.RichTextBox
  Friend WithEvents opt_account_summary As System.Windows.Forms.RadioButton
  Friend WithEvents opt_all_account As System.Windows.Forms.RadioButton
  Friend WithEvents ef_expiration_days As GUI_Controls.uc_entry_field
  Friend WithEvents cmb_categories As GUI_Controls.uc_combo
  Friend WithEvents img_icon As GUI_Controls.uc_image
  Friend WithEvents lbl_icon As System.Windows.Forms.Label
  Friend WithEvents lbl_icon_optimun_size As System.Windows.Forms.Label
  Friend WithEvents lbl_image_optimun_size As System.Windows.Forms.Label
  Friend WithEvents img_image As GUI_Controls.uc_image
  Friend WithEvents lbl_image As System.Windows.Forms.Label
  Friend WithEvents tc_ticketfooter_promobox As System.Windows.Forms.TabControl
  Friend WithEvents tab_ticketfooter As System.Windows.Forms.TabPage
  Friend WithEvents tab_promobox As System.Windows.Forms.TabPage
  Friend WithEvents tb_ticket_footer_prasigned_promotion As System.Windows.Forms.TextBox
  Friend WithEvents chk_visible_on_PromoBOX As System.Windows.Forms.CheckBox
  Friend WithEvents ef_text_on_PromoBOX As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_no_text_on_promobox_info As System.Windows.Forms.Label
  Friend WithEvents lbl_apply_on As System.Windows.Forms.Label
  Friend WithEvents chk_award_on_InTouch As System.Windows.Forms.CheckBox
  Friend WithEvents chk_award_on_PromoBOX As System.Windows.Forms.CheckBox
  Friend WithEvents cmb_award_with_game As System.Windows.Forms.ComboBox
  Friend WithEvents chk_award_with_game As System.Windows.Forms.CheckBox
End Class
