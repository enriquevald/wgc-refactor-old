<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_payment_orders_config
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing AndAlso components IsNot Nothing Then
      components.Dispose()
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.ef_min_amount = New GUI_Controls.uc_entry_field
    Me.chk_enabled = New System.Windows.Forms.CheckBox
    Me.lbl_min_amount = New System.Windows.Forms.Label
    Me.chk_PaymentDocument = New System.Windows.Forms.CheckBox
    Me.gb_documents = New System.Windows.Forms.GroupBox
    Me.pnl_tab_container = New System.Windows.Forms.Panel
    Me.tab_document = New System.Windows.Forms.TabControl
    Me.tab_client = New System.Windows.Forms.TabPage
    Me.pnl_sign = New System.Windows.Forms.Panel
    Me.btn_scale_down = New GUI_Controls.uc_button
    Me.btn_scale_up = New GUI_Controls.uc_button
    Me.btn_select_img = New GUI_Controls.uc_button
    Me.lbl_logo = New System.Windows.Forms.Label
    Me.pnl_logo = New System.Windows.Forms.Panel
    Me.ef_reference = New GUI_Controls.uc_entry_field
    Me.ef_doc_title = New GUI_Controls.uc_entry_field
    Me.tab_docs_required = New System.Windows.Forms.TabPage
    Me.lbl_required_documents = New System.Windows.Forms.Label
    Me.dg_identification_documents = New GUI_Controls.uc_grid
    Me.tab_autorize = New System.Windows.Forms.TabPage
    Me.ef_authorit2_name = New GUI_Controls.uc_entry_field
    Me.chk_authorit1 = New System.Windows.Forms.CheckBox
    Me.ef_authorit2_title = New GUI_Controls.uc_entry_field
    Me.ef_authorit1_title = New GUI_Controls.uc_entry_field
    Me.ef_authorit1_name = New GUI_Controls.uc_entry_field
    Me.chk_authorit2 = New System.Windows.Forms.CheckBox
    Me.tab_accounts = New System.Windows.Forms.TabPage
    Me.pnl_tab_accounts_container = New System.Windows.Forms.Panel
    Me.tab_bank_accounts = New System.Windows.Forms.TabControl
    Me.btn_addAccount = New GUI_Controls.uc_button
    Me.btn_deleteAccount = New GUI_Controls.uc_button
    Me.ef_editable_minutes = New GUI_Controls.uc_entry_field
    Me.panel_data.SuspendLayout()
    Me.gb_documents.SuspendLayout()
    Me.pnl_tab_container.SuspendLayout()
    Me.tab_document.SuspendLayout()
    Me.tab_client.SuspendLayout()
    Me.pnl_sign.SuspendLayout()
    Me.tab_docs_required.SuspendLayout()
    Me.tab_autorize.SuspendLayout()
    Me.tab_accounts.SuspendLayout()
    Me.pnl_tab_accounts_container.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_documents)
    Me.panel_data.Controls.Add(Me.chk_PaymentDocument)
    Me.panel_data.Controls.Add(Me.lbl_min_amount)
    Me.panel_data.Controls.Add(Me.chk_enabled)
    Me.panel_data.Controls.Add(Me.ef_min_amount)
    Me.panel_data.Size = New System.Drawing.Size(630, 539)
    '
    'ef_min_amount
    '
    Me.ef_min_amount.DoubleValue = 0
    Me.ef_min_amount.IntegerValue = 0
    Me.ef_min_amount.IsReadOnly = False
    Me.ef_min_amount.Location = New System.Drawing.Point(13, 43)
    Me.ef_min_amount.Name = "ef_min_amount"
    Me.ef_min_amount.PlaceHolder = Nothing
    Me.ef_min_amount.Size = New System.Drawing.Size(260, 24)
    Me.ef_min_amount.SufixText = "Sufix Text"
    Me.ef_min_amount.SufixTextVisible = True
    Me.ef_min_amount.TabIndex = 1
    Me.ef_min_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_min_amount.TextValue = ""
    Me.ef_min_amount.TextWidth = 130
    Me.ef_min_amount.Value = ""
    Me.ef_min_amount.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_enabled
    '
    Me.chk_enabled.AutoSize = True
    Me.chk_enabled.Location = New System.Drawing.Point(13, 14)
    Me.chk_enabled.Name = "chk_enabled"
    Me.chk_enabled.Size = New System.Drawing.Size(182, 17)
    Me.chk_enabled.TabIndex = 0
    Me.chk_enabled.Text = "xHabilitar Órdenes de Pago"
    Me.chk_enabled.UseVisualStyleBackColor = True
    '
    'lbl_min_amount
    '
    Me.lbl_min_amount.BackColor = System.Drawing.SystemColors.Control
    Me.lbl_min_amount.Font = New System.Drawing.Font("Courier New", 8.25!)
    Me.lbl_min_amount.ForeColor = System.Drawing.Color.Navy
    Me.lbl_min_amount.Location = New System.Drawing.Point(279, 43)
    Me.lbl_min_amount.Name = "lbl_min_amount"
    Me.lbl_min_amount.Size = New System.Drawing.Size(334, 40)
    Me.lbl_min_amount.TabIndex = 20
    Me.lbl_min_amount.Text = "xCantidad a partir de la cual se puede generar la orden de pago."
    '
    'chk_PaymentDocument
    '
    Me.chk_PaymentDocument.AutoSize = True
    Me.chk_PaymentDocument.Location = New System.Drawing.Point(13, 90)
    Me.chk_PaymentDocument.Name = "chk_PaymentDocument"
    Me.chk_PaymentDocument.Size = New System.Drawing.Size(58, 17)
    Me.chk_PaymentDocument.TabIndex = 21
    Me.chk_PaymentDocument.Text = "xText"
    Me.chk_PaymentDocument.UseVisualStyleBackColor = True
    '
    'gb_documents
    '
    Me.gb_documents.Controls.Add(Me.pnl_tab_container)
    Me.gb_documents.Controls.Add(Me.ef_editable_minutes)
    Me.gb_documents.Location = New System.Drawing.Point(36, 125)
    Me.gb_documents.Name = "gb_documents"
    Me.gb_documents.Size = New System.Drawing.Size(571, 408)
    Me.gb_documents.TabIndex = 23
    Me.gb_documents.TabStop = False
    Me.gb_documents.Text = "xDocumentos"
    '
    'pnl_tab_container
    '
    Me.pnl_tab_container.Controls.Add(Me.tab_document)
    Me.pnl_tab_container.Location = New System.Drawing.Point(6, 50)
    Me.pnl_tab_container.Name = "pnl_tab_container"
    Me.pnl_tab_container.Size = New System.Drawing.Size(559, 352)
    Me.pnl_tab_container.TabIndex = 4
    '
    'tab_document
    '
    Me.tab_document.Controls.Add(Me.tab_client)
    Me.tab_document.Controls.Add(Me.tab_docs_required)
    Me.tab_document.Controls.Add(Me.tab_autorize)
    Me.tab_document.Controls.Add(Me.tab_accounts)
    Me.tab_document.Dock = System.Windows.Forms.DockStyle.Fill
    Me.tab_document.ItemSize = New System.Drawing.Size(80, 18)
    Me.tab_document.Location = New System.Drawing.Point(0, 0)
    Me.tab_document.Name = "tab_document"
    Me.tab_document.SelectedIndex = 0
    Me.tab_document.Size = New System.Drawing.Size(559, 352)
    Me.tab_document.TabIndex = 8
    '
    'tab_client
    '
    Me.tab_client.Controls.Add(Me.pnl_sign)
    Me.tab_client.Controls.Add(Me.ef_reference)
    Me.tab_client.Controls.Add(Me.ef_doc_title)
    Me.tab_client.Location = New System.Drawing.Point(4, 22)
    Me.tab_client.Name = "tab_client"
    Me.tab_client.Size = New System.Drawing.Size(551, 326)
    Me.tab_client.TabIndex = 1
    Me.tab_client.Text = "xPersonalización"
    Me.tab_client.UseVisualStyleBackColor = True
    '
    'pnl_sign
    '
    Me.pnl_sign.Controls.Add(Me.btn_scale_down)
    Me.pnl_sign.Controls.Add(Me.btn_scale_up)
    Me.pnl_sign.Controls.Add(Me.btn_select_img)
    Me.pnl_sign.Controls.Add(Me.lbl_logo)
    Me.pnl_sign.Controls.Add(Me.pnl_logo)
    Me.pnl_sign.Location = New System.Drawing.Point(141, 72)
    Me.pnl_sign.Name = "pnl_sign"
    Me.pnl_sign.Size = New System.Drawing.Size(316, 233)
    Me.pnl_sign.TabIndex = 8
    '
    'btn_scale_down
    '
    Me.btn_scale_down.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_scale_down.Location = New System.Drawing.Point(218, 58)
    Me.btn_scale_down.Name = "btn_scale_down"
    Me.btn_scale_down.Size = New System.Drawing.Size(90, 30)
    Me.btn_scale_down.TabIndex = 2
    Me.btn_scale_down.ToolTipped = False
    Me.btn_scale_down.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.USER
    '
    'btn_scale_up
    '
    Me.btn_scale_up.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_scale_up.Location = New System.Drawing.Point(218, 22)
    Me.btn_scale_up.Name = "btn_scale_up"
    Me.btn_scale_up.Size = New System.Drawing.Size(90, 30)
    Me.btn_scale_up.TabIndex = 1
    Me.btn_scale_up.ToolTipped = False
    Me.btn_scale_up.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.USER
    '
    'btn_select_img
    '
    Me.btn_select_img.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_select_img.Location = New System.Drawing.Point(218, 187)
    Me.btn_select_img.Name = "btn_select_img"
    Me.btn_select_img.Size = New System.Drawing.Size(90, 30)
    Me.btn_select_img.TabIndex = 0
    Me.btn_select_img.ToolTipped = False
    Me.btn_select_img.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.USER
    '
    'lbl_logo
    '
    Me.lbl_logo.AutoSize = True
    Me.lbl_logo.Location = New System.Drawing.Point(11, 3)
    Me.lbl_logo.Name = "lbl_logo"
    Me.lbl_logo.Size = New System.Drawing.Size(41, 13)
    Me.lbl_logo.TabIndex = 15
    Me.lbl_logo.Text = "xLogo"
    '
    'pnl_logo
    '
    Me.pnl_logo.BackColor = System.Drawing.Color.White
    Me.pnl_logo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
    Me.pnl_logo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.pnl_logo.Cursor = System.Windows.Forms.Cursors.Default
    Me.pnl_logo.Location = New System.Drawing.Point(12, 22)
    Me.pnl_logo.Name = "pnl_logo"
    Me.pnl_logo.Size = New System.Drawing.Size(200, 200)
    Me.pnl_logo.TabIndex = 17
    '
    'ef_reference
    '
    Me.ef_reference.DoubleValue = 0
    Me.ef_reference.IntegerValue = 0
    Me.ef_reference.IsReadOnly = False
    Me.ef_reference.Location = New System.Drawing.Point(21, 37)
    Me.ef_reference.Name = "ef_reference"
    Me.ef_reference.PlaceHolder = Nothing
    Me.ef_reference.Size = New System.Drawing.Size(290, 24)
    Me.ef_reference.SufixText = "Sufix Text"
    Me.ef_reference.SufixTextVisible = True
    Me.ef_reference.TabIndex = 7
    Me.ef_reference.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_reference.TextValue = ""
    Me.ef_reference.TextWidth = 130
    Me.ef_reference.Value = ""
    Me.ef_reference.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_doc_title
    '
    Me.ef_doc_title.DoubleValue = 0
    Me.ef_doc_title.IntegerValue = 0
    Me.ef_doc_title.IsReadOnly = False
    Me.ef_doc_title.Location = New System.Drawing.Point(21, 7)
    Me.ef_doc_title.Name = "ef_doc_title"
    Me.ef_doc_title.PlaceHolder = Nothing
    Me.ef_doc_title.Size = New System.Drawing.Size(332, 24)
    Me.ef_doc_title.SufixText = "Sufix Text"
    Me.ef_doc_title.SufixTextVisible = True
    Me.ef_doc_title.TabIndex = 6
    Me.ef_doc_title.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_doc_title.TextValue = ""
    Me.ef_doc_title.TextWidth = 130
    Me.ef_doc_title.Value = ""
    Me.ef_doc_title.ValueForeColor = System.Drawing.Color.Blue
    '
    'tab_docs_required
    '
    Me.tab_docs_required.Controls.Add(Me.lbl_required_documents)
    Me.tab_docs_required.Controls.Add(Me.dg_identification_documents)
    Me.tab_docs_required.Location = New System.Drawing.Point(4, 22)
    Me.tab_docs_required.Name = "tab_docs_required"
    Me.tab_docs_required.Size = New System.Drawing.Size(551, 326)
    Me.tab_docs_required.TabIndex = 2
    Me.tab_docs_required.Text = "xDocumentos Requeridos"
    Me.tab_docs_required.UseVisualStyleBackColor = True
    '
    'lbl_required_documents
    '
    Me.lbl_required_documents.AutoSize = True
    Me.lbl_required_documents.Location = New System.Drawing.Point(67, 14)
    Me.lbl_required_documents.Name = "lbl_required_documents"
    Me.lbl_required_documents.Size = New System.Drawing.Size(44, 13)
    Me.lbl_required_documents.TabIndex = 5
    Me.lbl_required_documents.Text = "xLabel"
    '
    'dg_identification_documents
    '
    Me.dg_identification_documents.CurrentCol = -1
    Me.dg_identification_documents.CurrentRow = -1
    Me.dg_identification_documents.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_identification_documents.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_identification_documents.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_identification_documents.Location = New System.Drawing.Point(70, 45)
    Me.dg_identification_documents.Name = "dg_identification_documents"
    Me.dg_identification_documents.PanelRightVisible = False
    Me.dg_identification_documents.Redraw = True
    Me.dg_identification_documents.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    Me.dg_identification_documents.Size = New System.Drawing.Size(379, 249)
    Me.dg_identification_documents.Sortable = False
    Me.dg_identification_documents.SortAscending = True
    Me.dg_identification_documents.SortByCol = 0
    Me.dg_identification_documents.TabIndex = 4
    Me.dg_identification_documents.ToolTipped = True
    Me.dg_identification_documents.TopRow = -2
    '
    'tab_autorize
    '
    Me.tab_autorize.Controls.Add(Me.ef_authorit2_name)
    Me.tab_autorize.Controls.Add(Me.chk_authorit1)
    Me.tab_autorize.Controls.Add(Me.ef_authorit2_title)
    Me.tab_autorize.Controls.Add(Me.ef_authorit1_title)
    Me.tab_autorize.Controls.Add(Me.ef_authorit1_name)
    Me.tab_autorize.Controls.Add(Me.chk_authorit2)
    Me.tab_autorize.Location = New System.Drawing.Point(4, 22)
    Me.tab_autorize.Name = "tab_autorize"
    Me.tab_autorize.Size = New System.Drawing.Size(551, 326)
    Me.tab_autorize.TabIndex = 0
    Me.tab_autorize.Text = "xAutorización"
    Me.tab_autorize.UseVisualStyleBackColor = True
    '
    'ef_authorit2_name
    '
    Me.ef_authorit2_name.DoubleValue = 0
    Me.ef_authorit2_name.IntegerValue = 0
    Me.ef_authorit2_name.IsReadOnly = False
    Me.ef_authorit2_name.Location = New System.Drawing.Point(77, 215)
    Me.ef_authorit2_name.Name = "ef_authorit2_name"
    Me.ef_authorit2_name.OnlyUpperCase = True
    Me.ef_authorit2_name.PlaceHolder = Nothing
    Me.ef_authorit2_name.Size = New System.Drawing.Size(350, 24)
    Me.ef_authorit2_name.SufixText = "Sufix Text"
    Me.ef_authorit2_name.SufixTextVisible = True
    Me.ef_authorit2_name.TabIndex = 11
    Me.ef_authorit2_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_authorit2_name.TextValue = ""
    Me.ef_authorit2_name.TextWidth = 150
    Me.ef_authorit2_name.Value = ""
    Me.ef_authorit2_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_authorit1
    '
    Me.chk_authorit1.AutoSize = True
    Me.chk_authorit1.Location = New System.Drawing.Point(84, 42)
    Me.chk_authorit1.Name = "chk_authorit1"
    Me.chk_authorit1.Size = New System.Drawing.Size(175, 17)
    Me.chk_authorit1.TabIndex = 6
    Me.chk_authorit1.Text = "xRequerida autorización 1"
    Me.chk_authorit1.UseVisualStyleBackColor = True
    '
    'ef_authorit2_title
    '
    Me.ef_authorit2_title.DoubleValue = 0
    Me.ef_authorit2_title.IntegerValue = 0
    Me.ef_authorit2_title.IsReadOnly = False
    Me.ef_authorit2_title.Location = New System.Drawing.Point(77, 185)
    Me.ef_authorit2_title.Name = "ef_authorit2_title"
    Me.ef_authorit2_title.OnlyUpperCase = True
    Me.ef_authorit2_title.PlaceHolder = Nothing
    Me.ef_authorit2_title.Size = New System.Drawing.Size(350, 24)
    Me.ef_authorit2_title.SufixText = "Sufix Text"
    Me.ef_authorit2_title.SufixTextVisible = True
    Me.ef_authorit2_title.TabIndex = 10
    Me.ef_authorit2_title.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_authorit2_title.TextValue = ""
    Me.ef_authorit2_title.TextWidth = 150
    Me.ef_authorit2_title.Value = ""
    Me.ef_authorit2_title.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_authorit1_title
    '
    Me.ef_authorit1_title.DoubleValue = 0
    Me.ef_authorit1_title.IntegerValue = 0
    Me.ef_authorit1_title.IsReadOnly = False
    Me.ef_authorit1_title.Location = New System.Drawing.Point(77, 65)
    Me.ef_authorit1_title.Name = "ef_authorit1_title"
    Me.ef_authorit1_title.OnlyUpperCase = True
    Me.ef_authorit1_title.PlaceHolder = Nothing
    Me.ef_authorit1_title.Size = New System.Drawing.Size(350, 24)
    Me.ef_authorit1_title.SufixText = "Sufix Text"
    Me.ef_authorit1_title.SufixTextVisible = True
    Me.ef_authorit1_title.TabIndex = 7
    Me.ef_authorit1_title.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_authorit1_title.TextValue = ""
    Me.ef_authorit1_title.TextWidth = 150
    Me.ef_authorit1_title.Value = ""
    Me.ef_authorit1_title.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_authorit1_name
    '
    Me.ef_authorit1_name.DoubleValue = 0
    Me.ef_authorit1_name.IntegerValue = 0
    Me.ef_authorit1_name.IsReadOnly = False
    Me.ef_authorit1_name.Location = New System.Drawing.Point(77, 95)
    Me.ef_authorit1_name.Name = "ef_authorit1_name"
    Me.ef_authorit1_name.OnlyUpperCase = True
    Me.ef_authorit1_name.PlaceHolder = Nothing
    Me.ef_authorit1_name.Size = New System.Drawing.Size(350, 24)
    Me.ef_authorit1_name.SufixText = "Sufix Text"
    Me.ef_authorit1_name.SufixTextVisible = True
    Me.ef_authorit1_name.TabIndex = 8
    Me.ef_authorit1_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_authorit1_name.TextValue = ""
    Me.ef_authorit1_name.TextWidth = 150
    Me.ef_authorit1_name.Value = ""
    Me.ef_authorit1_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_authorit2
    '
    Me.chk_authorit2.AutoSize = True
    Me.chk_authorit2.Location = New System.Drawing.Point(84, 162)
    Me.chk_authorit2.Name = "chk_authorit2"
    Me.chk_authorit2.Size = New System.Drawing.Size(175, 17)
    Me.chk_authorit2.TabIndex = 9
    Me.chk_authorit2.Text = "xRequerida autorización 2"
    Me.chk_authorit2.UseVisualStyleBackColor = True
    '
    'tab_accounts
    '
    Me.tab_accounts.Controls.Add(Me.pnl_tab_accounts_container)
    Me.tab_accounts.Controls.Add(Me.btn_addAccount)
    Me.tab_accounts.Controls.Add(Me.btn_deleteAccount)
    Me.tab_accounts.Location = New System.Drawing.Point(4, 22)
    Me.tab_accounts.Name = "tab_accounts"
    Me.tab_accounts.Size = New System.Drawing.Size(551, 326)
    Me.tab_accounts.TabIndex = 3
    Me.tab_accounts.Text = "xCuentas"
    Me.tab_accounts.UseVisualStyleBackColor = True
    '
    'pnl_tab_accounts_container
    '
    Me.pnl_tab_accounts_container.Controls.Add(Me.tab_bank_accounts)
    Me.pnl_tab_accounts_container.Location = New System.Drawing.Point(3, 5)
    Me.pnl_tab_accounts_container.Name = "pnl_tab_accounts_container"
    Me.pnl_tab_accounts_container.Size = New System.Drawing.Size(532, 267)
    Me.pnl_tab_accounts_container.TabIndex = 9
    '
    'tab_bank_accounts
    '
    Me.tab_bank_accounts.Dock = System.Windows.Forms.DockStyle.Fill
    Me.tab_bank_accounts.Location = New System.Drawing.Point(0, 0)
    Me.tab_bank_accounts.Name = "tab_bank_accounts"
    Me.tab_bank_accounts.SelectedIndex = 0
    Me.tab_bank_accounts.Size = New System.Drawing.Size(532, 267)
    Me.tab_bank_accounts.TabIndex = 8
    '
    'btn_addAccount
    '
    Me.btn_addAccount.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_addAccount.Location = New System.Drawing.Point(444, 278)
    Me.btn_addAccount.Name = "btn_addAccount"
    Me.btn_addAccount.Size = New System.Drawing.Size(90, 30)
    Me.btn_addAccount.TabIndex = 8
    Me.btn_addAccount.ToolTipped = False
    Me.btn_addAccount.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_deleteAccount
    '
    Me.btn_deleteAccount.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_deleteAccount.Location = New System.Drawing.Point(328, 278)
    Me.btn_deleteAccount.Name = "btn_deleteAccount"
    Me.btn_deleteAccount.Size = New System.Drawing.Size(90, 30)
    Me.btn_deleteAccount.TabIndex = 7
    Me.btn_deleteAccount.ToolTipped = False
    Me.btn_deleteAccount.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'ef_editable_minutes
    '
    Me.ef_editable_minutes.DoubleValue = 0
    Me.ef_editable_minutes.IntegerValue = 0
    Me.ef_editable_minutes.IsReadOnly = False
    Me.ef_editable_minutes.Location = New System.Drawing.Point(6, 20)
    Me.ef_editable_minutes.Name = "ef_editable_minutes"
    Me.ef_editable_minutes.PlaceHolder = Nothing
    Me.ef_editable_minutes.Size = New System.Drawing.Size(321, 24)
    Me.ef_editable_minutes.SufixText = "Sufix Text"
    Me.ef_editable_minutes.SufixTextVisible = True
    Me.ef_editable_minutes.SufixTextWidth = 70
    Me.ef_editable_minutes.TabIndex = 3
    Me.ef_editable_minutes.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_editable_minutes.TextValue = ""
    Me.ef_editable_minutes.TextWidth = 130
    Me.ef_editable_minutes.Value = ""
    Me.ef_editable_minutes.ValueForeColor = System.Drawing.Color.Blue
    '
    'frm_payment_orders_config
    '
    Me.ClientSize = New System.Drawing.Size(732, 548)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_payment_orders_config"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.gb_documents.ResumeLayout(False)
    Me.pnl_tab_container.ResumeLayout(False)
    Me.tab_document.ResumeLayout(False)
    Me.tab_client.ResumeLayout(False)
    Me.pnl_sign.ResumeLayout(False)
    Me.pnl_sign.PerformLayout()
    Me.tab_docs_required.ResumeLayout(False)
    Me.tab_docs_required.PerformLayout()
    Me.tab_autorize.ResumeLayout(False)
    Me.tab_autorize.PerformLayout()
    Me.tab_accounts.ResumeLayout(False)
    Me.pnl_tab_accounts_container.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_min_amount As GUI_Controls.uc_entry_field
  Friend WithEvents chk_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_min_amount As System.Windows.Forms.Label
  Friend WithEvents chk_PaymentDocument As System.Windows.Forms.CheckBox
  Friend WithEvents gb_documents As System.Windows.Forms.GroupBox
  Friend WithEvents pnl_tab_container As System.Windows.Forms.Panel
  Friend WithEvents ef_editable_minutes As GUI_Controls.uc_entry_field
  Friend WithEvents tab_document As System.Windows.Forms.TabControl
  Friend WithEvents tab_client As System.Windows.Forms.TabPage
  Friend WithEvents pnl_sign As System.Windows.Forms.Panel
  Friend WithEvents btn_scale_down As GUI_Controls.uc_button
  Friend WithEvents btn_scale_up As GUI_Controls.uc_button
  Friend WithEvents btn_select_img As GUI_Controls.uc_button
  Friend WithEvents lbl_logo As System.Windows.Forms.Label
  Friend WithEvents pnl_logo As System.Windows.Forms.Panel
  Friend WithEvents ef_reference As GUI_Controls.uc_entry_field
  Friend WithEvents ef_doc_title As GUI_Controls.uc_entry_field
  Friend WithEvents tab_docs_required As System.Windows.Forms.TabPage
  Friend WithEvents dg_identification_documents As GUI_Controls.uc_grid
  Friend WithEvents tab_autorize As System.Windows.Forms.TabPage
  Friend WithEvents ef_authorit2_name As GUI_Controls.uc_entry_field
  Friend WithEvents chk_authorit1 As System.Windows.Forms.CheckBox
  Friend WithEvents ef_authorit2_title As GUI_Controls.uc_entry_field
  Friend WithEvents ef_authorit1_title As GUI_Controls.uc_entry_field
  Friend WithEvents ef_authorit1_name As GUI_Controls.uc_entry_field
  Friend WithEvents chk_authorit2 As System.Windows.Forms.CheckBox
  Friend WithEvents tab_accounts As System.Windows.Forms.TabPage
  Friend WithEvents pnl_tab_accounts_container As System.Windows.Forms.Panel
  Friend WithEvents tab_bank_accounts As System.Windows.Forms.TabControl
  Friend WithEvents btn_addAccount As GUI_Controls.uc_button
  Friend WithEvents btn_deleteAccount As GUI_Controls.uc_button
  Friend WithEvents lbl_required_documents As System.Windows.Forms.Label

End Class
