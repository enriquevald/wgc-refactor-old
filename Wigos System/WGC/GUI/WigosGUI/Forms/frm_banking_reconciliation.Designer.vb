﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_banking_reconciliation
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.Grid = New GUI_Controls.uc_grid()
    Me.gb_delivered = New System.Windows.Forms.GroupBox()
    Me.ef_amount_given_count = New GUI_Controls.uc_entry_field()
    Me.ef_amount_given_amount = New GUI_Controls.uc_entry_field()
    Me.gb_no_delivered = New System.Windows.Forms.GroupBox()
    Me.ef_amount_nogiven_count = New GUI_Controls.uc_entry_field()
    Me.ef_amount_nogiven_amount = New GUI_Controls.uc_entry_field()
    Me.panel_data.SuspendLayout()
    Me.gb_delivered.SuspendLayout()
    Me.gb_no_delivered.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_no_delivered)
    Me.panel_data.Controls.Add(Me.gb_delivered)
    Me.panel_data.Controls.Add(Me.Grid)
    Me.panel_data.Location = New System.Drawing.Point(4, 7)
    Me.panel_data.Size = New System.Drawing.Size(871, 477)
    '
    'Grid
    '
    Me.Grid.CurrentCol = -1
    Me.Grid.CurrentRow = -1
    Me.Grid.EditableCellBackColor = System.Drawing.Color.Empty
    Me.Grid.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.Grid.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.Grid.Location = New System.Drawing.Point(3, 5)
    Me.Grid.Name = "Grid"
    Me.Grid.PanelRightVisible = True
    Me.Grid.Redraw = True
    Me.Grid.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.Grid.Size = New System.Drawing.Size(865, 376)
    Me.Grid.Sortable = False
    Me.Grid.SortAscending = True
    Me.Grid.SortByCol = 0
    Me.Grid.TabIndex = 2
    Me.Grid.ToolTipped = True
    Me.Grid.TopRow = -2
    Me.Grid.WordWrap = False
    '
    'gb_delivered
    '
    Me.gb_delivered.Controls.Add(Me.ef_amount_given_count)
    Me.gb_delivered.Controls.Add(Me.ef_amount_given_amount)
    Me.gb_delivered.Location = New System.Drawing.Point(12, 387)
    Me.gb_delivered.Name = "gb_delivered"
    Me.gb_delivered.Size = New System.Drawing.Size(389, 81)
    Me.gb_delivered.TabIndex = 3
    Me.gb_delivered.TabStop = False
    Me.gb_delivered.Text = "gb_delivered"
    '
    'ef_amount_given_count
    '
    Me.ef_amount_given_count.DoubleValue = 0.0R
    Me.ef_amount_given_count.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_amount_given_count.IntegerValue = 0
    Me.ef_amount_given_count.IsReadOnly = False
    Me.ef_amount_given_count.Location = New System.Drawing.Point(40, 48)
    Me.ef_amount_given_count.Name = "ef_amount_given_count"
    Me.ef_amount_given_count.PlaceHolder = Nothing
    Me.ef_amount_given_count.Size = New System.Drawing.Size(306, 25)
    Me.ef_amount_given_count.SufixText = "Sufix Text"
    Me.ef_amount_given_count.SufixTextVisible = True
    Me.ef_amount_given_count.TabIndex = 14
    Me.ef_amount_given_count.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_amount_given_count.TextValue = ""
    Me.ef_amount_given_count.TextWidth = 200
    Me.ef_amount_given_count.Value = ""
    Me.ef_amount_given_count.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_amount_given_amount
    '
    Me.ef_amount_given_amount.DoubleValue = 0.0R
    Me.ef_amount_given_amount.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_amount_given_amount.IntegerValue = 0
    Me.ef_amount_given_amount.IsReadOnly = False
    Me.ef_amount_given_amount.Location = New System.Drawing.Point(40, 18)
    Me.ef_amount_given_amount.Name = "ef_amount_given_amount"
    Me.ef_amount_given_amount.PlaceHolder = Nothing
    Me.ef_amount_given_amount.Size = New System.Drawing.Size(306, 25)
    Me.ef_amount_given_amount.SufixText = "Sufix Text"
    Me.ef_amount_given_amount.SufixTextVisible = True
    Me.ef_amount_given_amount.TabIndex = 13
    Me.ef_amount_given_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_amount_given_amount.TextValue = ""
    Me.ef_amount_given_amount.TextWidth = 200
    Me.ef_amount_given_amount.Value = ""
    Me.ef_amount_given_amount.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_no_delivered
    '
    Me.gb_no_delivered.Controls.Add(Me.ef_amount_nogiven_count)
    Me.gb_no_delivered.Controls.Add(Me.ef_amount_nogiven_amount)
    Me.gb_no_delivered.Location = New System.Drawing.Point(426, 387)
    Me.gb_no_delivered.Name = "gb_no_delivered"
    Me.gb_no_delivered.Size = New System.Drawing.Size(376, 81)
    Me.gb_no_delivered.TabIndex = 4
    Me.gb_no_delivered.TabStop = False
    Me.gb_no_delivered.Text = "gb_no_delivered"
    '
    'ef_amount_nogiven_count
    '
    Me.ef_amount_nogiven_count.DoubleValue = 0.0R
    Me.ef_amount_nogiven_count.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_amount_nogiven_count.IntegerValue = 0
    Me.ef_amount_nogiven_count.IsReadOnly = False
    Me.ef_amount_nogiven_count.Location = New System.Drawing.Point(39, 47)
    Me.ef_amount_nogiven_count.Name = "ef_amount_nogiven_count"
    Me.ef_amount_nogiven_count.PlaceHolder = Nothing
    Me.ef_amount_nogiven_count.Size = New System.Drawing.Size(306, 25)
    Me.ef_amount_nogiven_count.SufixText = "Sufix Text"
    Me.ef_amount_nogiven_count.SufixTextVisible = True
    Me.ef_amount_nogiven_count.TabIndex = 16
    Me.ef_amount_nogiven_count.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_amount_nogiven_count.TextValue = ""
    Me.ef_amount_nogiven_count.TextWidth = 200
    Me.ef_amount_nogiven_count.Value = ""
    Me.ef_amount_nogiven_count.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_amount_nogiven_amount
    '
    Me.ef_amount_nogiven_amount.DoubleValue = 0.0R
    Me.ef_amount_nogiven_amount.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_amount_nogiven_amount.IntegerValue = 0
    Me.ef_amount_nogiven_amount.IsReadOnly = False
    Me.ef_amount_nogiven_amount.Location = New System.Drawing.Point(39, 18)
    Me.ef_amount_nogiven_amount.Name = "ef_amount_nogiven_amount"
    Me.ef_amount_nogiven_amount.PlaceHolder = Nothing
    Me.ef_amount_nogiven_amount.Size = New System.Drawing.Size(306, 25)
    Me.ef_amount_nogiven_amount.SufixText = "Sufix Text"
    Me.ef_amount_nogiven_amount.SufixTextVisible = True
    Me.ef_amount_nogiven_amount.TabIndex = 15
    Me.ef_amount_nogiven_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_amount_nogiven_amount.TextValue = ""
    Me.ef_amount_nogiven_amount.TextWidth = 200
    Me.ef_amount_nogiven_amount.Value = ""
    Me.ef_amount_nogiven_amount.ValueForeColor = System.Drawing.Color.Blue
    '
    'frm_banking_reconciliation
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(973, 488)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_banking_reconciliation"
    Me.Text = "frm_banking_reconciliation"
    Me.panel_data.ResumeLayout(False)
    Me.gb_delivered.ResumeLayout(False)
    Me.gb_no_delivered.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents Grid As GUI_Controls.uc_grid
  Friend WithEvents gb_no_delivered As System.Windows.Forms.GroupBox
  Friend WithEvents gb_delivered As System.Windows.Forms.GroupBox
  Friend WithEvents ef_amount_given_count As GUI_Controls.uc_entry_field
  Friend WithEvents ef_amount_given_amount As GUI_Controls.uc_entry_field
  Friend WithEvents ef_amount_nogiven_count As GUI_Controls.uc_entry_field
  Friend WithEvents ef_amount_nogiven_amount As GUI_Controls.uc_entry_field
End Class
