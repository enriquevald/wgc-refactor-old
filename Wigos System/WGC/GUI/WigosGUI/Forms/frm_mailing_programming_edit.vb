'-------------------------------------------------------------------
' Copyright © 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_mailing_programming_edit
' DESCRIPTION:   New or Edit Mailing Programming
' AUTHOR:        Raul Cervera
' CREATION DATE: 22-DEC-2010
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 22-DEC-2010  RCI    Initial version
' 29-NOV-2012  RRB    Added Terminals Without Activity mail type
' 08-JAN-2013  HBB    Used the method CheckEmailAddress Moved to WSI.Common.Mailing
' 12-FEB-2013  JMM    Replaced call to WSI.Common.Mailing.CheckEmailAddress with WSI.Common.ValidateFormat.Email
' 07-OCT-2013  DLL    Added WXP SAS Meters mail type.
' 11-JUL-2014  JMM    Weeks days sorted depending on system's first day of week
' 14-JUN-2016  EOR    Product Backlog Item 13672:Sprint Review 24 Winions - Mex
' 28-JUN-2016  EOR    Product Backlog Item 14627: Generic Reports: Added Mailing
' 15-SEP-2016  ETP    Fixed bugs 17585 - 17607: Generic reports: add permissions config.
' 31-OCT-2016  DMT    Product Backlog Item 19343:Feedback Module - GUI - Formulario de Configuración
' 24-NOV-2016  LTC    PBI 19870:R.Franco: New report Machines and Gaming Tables
' 22-JUN-2017  EOR    Fixed Bug 27150: Machine and game report not show in Mailing edit
' 29-JUN-2017  DHA    PBI 28419:WIGOS-80 Score report - Email configuration - Edition form - Update
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports WSI.Common
Imports WSI.Common.GenericReport

Public Class frm_mailing_programming_edit
  Inherits frm_base_edit

#Region " Constants "

  Private Const LEN_PROG_NAME As Integer = 50
  Private Const LEN_SUBJECT As Integer = 200
  Private Const VALUE_ENUM_TYPE As Integer = 100

#End Region ' Constants

#Region " Members "

  Private m_input_prog_name As String
  Private DeleteOperation As Boolean
  Private m_with_wxp_machine_meters As Boolean

#End Region ' Members

#Region " Overrides "

  ' PURPOSE: Control the enter key.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    If txt_address_list.Focused Then
      Return True
    Else
      Return False
    End If

  End Function ' GUI_KeyPress

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_MAILING_PROGRAMMING_EDIT

    Call MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()
    Dim _week_days_chk_array(6) As Windows.Forms.CheckBox
    'EOR 28-JUN-2016
    Dim _service As IReportToolConfigService
    Dim _request As GenericReport.ReportToolConfigRequest
    Dim _reponse As ReportToolConfigResponse

    'General Param
    Me.m_with_wxp_machine_meters = WSI.Common.GeneralParam.GetBoolean("ExternalProtocol", "Enabled", False)

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_STATISTICS.GetString(426)

    ' Hide the delete button if ScreenMode is New
    If Me.ScreenMode = frm_base_edit.ENUM_SCREEN_MODE.MODE_NEW Then
      GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
    Else
      GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = True
    End If

    ' Name
    ef_prog_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(253)
    ef_prog_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_PROG_NAME)

    ' Enabled control
    gb_enabled.Text = GLB_NLS_GUI_CONTROLS.GetString(281)
    opt_enabled.Text = GLB_NLS_GUI_CONTROLS.GetString(283)
    opt_disabled.Text = GLB_NLS_GUI_CONTROLS.GetString(284)

    ' Type
    cmb_prog_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(343)
    cmb_prog_type.Add(WSI.Common.MAILING_PROGRAMMING_TYPE.CASH_INFORMATION, GLB_NLS_GUI_STATISTICS.GetString(427))
    cmb_prog_type.Add(WSI.Common.MAILING_PROGRAMMING_TYPE.WSI_STATISTICS, GLB_NLS_GUI_STATISTICS.GetString(439))

    'EOR 14-JUN-2016
    If FeatureChips.IsGamingTablesEnabled Then
      cmb_prog_type.Add(WSI.Common.MAILING_PROGRAMMING_TYPE.CONNECTED_TERMINALS, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7366))
    Else
      cmb_prog_type.Add(WSI.Common.MAILING_PROGRAMMING_TYPE.CONNECTED_TERMINALS, GLB_NLS_GUI_STATISTICS.GetString(440))
    End If

    cmb_prog_type.Add(WSI.Common.MAILING_PROGRAMMING_TYPE.TERMINALS_WITHOUT_ACTIVITY, GLB_NLS_GUI_INVOICING.GetString(361))

    ' SGB 24-FEB-2015: Don't show ticket out report if we are in TITO or General Param is 1
    If Not WSI.Common.GeneralParam.GetBoolean("WigosGUI", "HideTicketOutReport", True) AndAlso WSI.Common.TITO.Utils.IsTitoMode Then
      cmb_prog_type.Add(WSI.Common.MAILING_PROGRAMMING_TYPE.TITO_TICKET_OUT_REPORT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5805))
    End If

    ' JMV 12-JAN-2015: Don't show machine and game report if we are not in TITO or General Param is 1
    ' LTC 24-NOV-2016 
    ' EOR 22-JUN-2017
    If WSI.Common.Misc.ShowMachineAndGameReport() Then
      _cmb_prog_type.Add(WSI.Common.MAILING_PROGRAMMING_TYPE.MACHINE_AND_GAME_REPORT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5820))
    End If

    'JMM 19-JAN-2012: Temporary disabled
    'cmb_prog_type.Add(WSI.Common.MAILING_PROGRAMMING_TYPE.ACCOUNT_MOVEMENTS, GLB_NLS_GUI_STATISTICS.GetString(473))

    ' JMV 23-MAR-2016: Don't show report if we are not in TITO 
    If Not WSI.Common.GeneralParam.GetBoolean("WigosGUI", "HideTITOHoldvsWinReport", True) AndAlso Not WSI.Common.Misc.IsCashlessMode() Then
      _cmb_prog_type.Add(WSI.Common.MAILING_PROGRAMMING_TYPE.TITO_HOLD_VS_WIN, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7193))
    End If

    'DMT 31/10/2016
    cmb_prog_type.Add(WSI.Common.MAILING_PROGRAMMING_TYPE.WIN_UP_FEEDBACK, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7728))

    If WSI.Common.GamingTableBusinessLogic.IsEnabledGTWinLoss() And WSI.Common.GamingTableBusinessLogic.IsEnabledGTPlayerTracking() Then
      cmb_prog_type.Add(WSI.Common.MAILING_PROGRAMMING_TYPE.SCORE_REPORT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8458))
    End If

    ' EOR 28-JUN-2016
    Dim _cons_request As GenericReport.ConstructorRequest
    _cons_request = New GenericReport.ConstructorRequest()

    _cons_request.ProfileID = CurrentUser.ProfileId
    _cons_request.ShowAll = CurrentUser.IsSuperUser
    _cons_request.OnlyReportsMailing = True

    _cons_request.ModeType = GetVisibleProfileForms(CurrentUser.GuiId)


    _service = New ReportToolConfigService(_cons_request)
    _request = New GenericReport.ReportToolConfigRequest()
    _reponse = New ReportToolConfigResponse

    For Each _locationMenu As GenericReport.LocationMenu In [Enum].GetValues(GetType(GenericReport.LocationMenu))

      If _service.ExistLocationMenu(_locationMenu) Then
        _request.LocationMenu = _locationMenu
        _reponse = _service.GetMenuConfiguration(_request)

        If (_reponse.Success) Then

          For Each item As ReportToolConfigDTO In _reponse.ReportToolConfigs
            Me._cmb_prog_type.Add(item.ReportID + VALUE_ENUM_TYPE, item.ReportName)
          Next

        End If

      End If

    Next



    ' Address List
    gb_address_list.Text = GLB_NLS_GUI_STATISTICS.GetString(428)

    ' Subject
    ef_subject.Text = GLB_NLS_GUI_STATISTICS.GetString(436)
    ef_subject.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_SUBJECT)

    ' Scheduling
    gb_scheduling.Text = GLB_NLS_GUI_STATISTICS.GetString(430)
    dtp_time_from.Text = GLB_NLS_GUI_STATISTICS.GetString(309)
    dtp_time_to.Text = GLB_NLS_GUI_STATISTICS.GetString(310)
    cmb_time_step.Text = GLB_NLS_GUI_STATISTICS.GetString(431)

    chk_monday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(301)
    chk_tuesday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(302)
    chk_wednesday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(303)
    chk_thursday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(304)
    chk_friday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(305)
    chk_saturday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(306)
    chk_sunday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(307)

    _week_days_chk_array(0) = chk_monday
    _week_days_chk_array(1) = chk_tuesday
    _week_days_chk_array(2) = chk_wednesday
    _week_days_chk_array(3) = chk_thursday
    _week_days_chk_array(4) = chk_friday
    _week_days_chk_array(5) = chk_saturday
    _week_days_chk_array(6) = chk_sunday

    GUI_Controls.Misc.SortCheckBoxesByFirstDayOfWeek(_week_days_chk_array)

    ' Set date picker obj properties
    dtp_time_from.SetFormat(ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HOURS)
    dtp_time_from.ShowUpDown = True
    dtp_time_to.SetFormat(ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HOURS)
    dtp_time_to.ShowUpDown = True

    ' Combo Step
    cmb_time_step.Add(0, GLB_NLS_GUI_STATISTICS.GetString(433))
    cmb_time_step.Add(3600, GLB_NLS_GUI_STATISTICS.GetString(434))
    cmb_time_step.Add(7200, GLB_NLS_GUI_STATISTICS.GetString(435, "2"))
    cmb_time_step.Add(10800, GLB_NLS_GUI_STATISTICS.GetString(435, "3"))
    cmb_time_step.Add(14400, GLB_NLS_GUI_STATISTICS.GetString(435, "4"))
    cmb_time_step.Add(28800, GLB_NLS_GUI_STATISTICS.GetString(435, "8"))
    cmb_time_step.Add(43200, GLB_NLS_GUI_STATISTICS.GetString(435, "12"))

  End Sub ' GUI_InitControls

  ' PURPOSE: Validate the data presented on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Dim nls_param1 As String
    Dim emails() As String
    Dim email As String

    ' Name
    If ef_prog_name.Value = "" Then
      nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(261)
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , nls_param1)
      Call ef_prog_name.Focus()

      Return False
    End If

    ' Address List
    If txt_address_list.Text = "" Then
      nls_param1 = GLB_NLS_GUI_STATISTICS.GetString(429)
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , nls_param1)
      Call txt_address_list.Focus()

      Return False
    End If

    ' Subject
    If ef_subject.Value = "" Then
      nls_param1 = GLB_NLS_GUI_STATISTICS.GetString(436)
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , nls_param1)
      Call ef_subject.Focus()

      Return False
    End If

    ' Check email addresses
    Dim sep(0 To 1) As Char
    Dim tmp_address As String

    emails = txt_address_list.Text.Split()
    tmp_address = String.Join(";", emails)
    sep(0) = ";"
    sep(1) = ","
    emails = tmp_address.Split(sep, StringSplitOptions.RemoveEmptyEntries)

    For Each email In emails
      email = email.Trim()
      If email = "" Then
        Continue For
      End If

      If Not WSI.Common.ValidateFormat.Email(email) Then
        Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(114), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , email)
        Call txt_address_list.Focus()

        Return False
      End If
    Next

    ' No Week days selected
    If GetScheduleDays() = 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(110), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call chk_monday.Focus()

      Return False
    End If

    Return True

  End Function ' GUI_IsScreenDataOk

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_GetScreenData()

    Dim promo_item As CLASS_MAILING_PROGRAMMING
    Dim temp_date As Date
    Dim second As Integer

    promo_item = DbEditedObject

    promo_item.Name = ef_prog_name.Value
    promo_item.Enabled = opt_enabled.Checked
    promo_item.Type = cmb_prog_type.Value

    promo_item.AddressList = txt_address_list.Text
    promo_item.Subject = ef_subject.Value

    promo_item.ScheduleWeekday = GetScheduleDays()

    temp_date = dtp_time_from.Value
    second = Format_HhMmSsToSecond(temp_date)
    promo_item.ScheduleTimeFrom = second

    temp_date = dtp_time_to.Value
    second = Format_HhMmSsToSecond(temp_date)
    promo_item.ScheduleTimeTo = second

    promo_item.ScheduleTimeStep = cmb_time_step.Value

  End Sub ' GUI_GetScreenData

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim promo_item As CLASS_MAILING_PROGRAMMING
    Dim second As Integer
    Dim minute As Integer
    Dim hour As Integer
    Dim day As Integer
    Dim date_time As Date
    Dim _report_time_str As String

    promo_item = DbReadObject

    ef_prog_name.Value = promo_item.Name

    If promo_item.Enabled Then
      opt_enabled.Checked = True
      opt_disabled.Checked = False
    Else
      opt_enabled.Checked = False
      opt_disabled.Checked = True
    End If

    If promo_item.Type = WSI.Common.MAILING_PROGRAMMING_TYPE.WXP_MACHINE_METERS AndAlso Me.m_with_wxp_machine_meters Then
      cmb_prog_type.Add(WSI.Common.MAILING_PROGRAMMING_TYPE.WXP_MACHINE_METERS, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2692))
      cmb_prog_type.Enabled = False
      gb_scheduling.Enabled = False
      GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
      Try
        _report_time_str = WSI.Common.Misc.ReadGeneralParams("ExternalProtocol", "ReportTime")
        Int32.TryParse(_report_time_str.Substring(0, _report_time_str.IndexOf(":")), promo_item.ScheduleTimeFrom)
        promo_item.ScheduleTimeFrom *= SECONDS_X_HOUR
      Catch
      End Try

    End If

    cmb_prog_type.Value = promo_item.Type

    txt_address_list.Text = promo_item.AddressList
    ef_subject.Value = promo_item.Subject

    SetScheduleDays(promo_item.ScheduleWeekday)

    date_time = GUI_GetDateTime()
    Call Format_SecondsToDdHhMmSs(promo_item.ScheduleTimeFrom, day, hour, minute, second)
    dtp_time_from.Value = New DateTime(date_time.Year, date_time.Month, date_time.Day, hour, minute, 0)
    Call Format_SecondsToDdHhMmSs(promo_item.ScheduleTimeTo, day, hour, minute, second)
    dtp_time_to.Value = New DateTime(date_time.Year, date_time.Month, date_time.Day, hour, minute, 0)

    cmb_time_step.Value = promo_item.ScheduleTimeStep
    cmb_time_step_ValueChangedEvent()

  End Sub ' GUI_SetScreenData

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Dim mailing_prog As CLASS_MAILING_PROGRAMMING
    Dim nls_param1 As String
    Dim rc As mdl_NLS.ENUM_MB_RESULT

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_MAILING_PROGRAMMING
        mailing_prog = DbEditedObject
        mailing_prog.MailingProgrammingId = 0
        mailing_prog.Enabled = True
        mailing_prog.Type = WSI.Common.MAILING_PROGRAMMING_TYPE.CASH_INFORMATION

        DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          mailing_prog = Me.DbEditedObject
          nls_param1 = m_input_prog_name
          Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(109), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          mailing_prog = Me.DbEditedObject
          nls_param1 = mailing_prog.Name
          Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(108), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1)
          Call ef_prog_name.Focus()
        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          mailing_prog = Me.DbEditedObject
          nls_param1 = mailing_prog.Name
          Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(110), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          mailing_prog = Me.DbEditedObject
          nls_param1 = mailing_prog.Name
          Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(108), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1)
          Call ef_prog_name.Focus()
        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          mailing_prog = Me.DbEditedObject
          nls_param1 = mailing_prog.Name
          Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(111), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK And DeleteOperation Then
          mailing_prog = Me.DbEditedObject
          nls_param1 = mailing_prog.Name
          Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(112), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE
        DeleteOperation = False
        mailing_prog = Me.DbEditedObject
        nls_param1 = mailing_prog.Name
        rc = NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(113), _
                        ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        ENUM_MB_BTN.MB_BTN_YES_NO, _
                        ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
                        nls_param1)
        If rc = ENUM_MB_RESULT.MB_RESULT_YES Then
          DeleteOperation = True
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_DELETE
        If DeleteOperation Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        Else
          DbStatus = ENUM_STATUS.STATUS_ERROR
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE: Manage permissions.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)

  End Sub ' GUI_Permissions

  ' PURPOSE: Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.ef_prog_name

  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Init form in new mode
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overloads Sub ShowNewItem()

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    DbObjectId = Nothing
    DbStatus = ENUM_STATUS.STATUS_OK

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowNewItem

  ' PURPOSE: Init form in edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '       - Prog Id
  '       - Prog Name
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overloads Sub ShowEditItem(ByVal ProgId As Integer, ByVal ProgName As String)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Me.DbObjectId = ProgId
    Me.m_input_prog_name = ProgName

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If
  End Sub ' ShowEditItem

#End Region ' Overrides

#Region " Private "

#End Region ' Private

  Private Sub SetScheduleDays(ByVal Days As Integer)
    Dim str_binary As String
    Dim bit_array As Char()

    str_binary = Convert.ToString(Days, 2)
    str_binary = New String("0", 7 - str_binary.Length) + str_binary

    bit_array = str_binary.ToCharArray()

    If (bit_array(6) = "0") Then Me.chk_sunday.Checked = False Else Me.chk_sunday.Checked = True ' Sunday
    If (bit_array(5) = "0") Then Me.chk_monday.Checked = False Else Me.chk_monday.Checked = True ' Monday 
    If (bit_array(4) = "0") Then Me.chk_tuesday.Checked = False Else Me.chk_tuesday.Checked = True ' Tuesday
    If (bit_array(3) = "0") Then Me.chk_wednesday.Checked = False Else Me.chk_wednesday.Checked = True ' Wednesday
    If (bit_array(2) = "0") Then Me.chk_thursday.Checked = False Else Me.chk_thursday.Checked = True ' Thursday
    If (bit_array(1) = "0") Then Me.chk_friday.Checked = False Else Me.chk_friday.Checked = True ' Friday
    If (bit_array(0) = "0") Then Me.chk_saturday.Checked = False Else Me.chk_saturday.Checked = True ' Saturday

  End Sub ' SetScheduleDays

  Private Function GetScheduleDays() As Integer
    Dim bit_array As Char()

    bit_array = New String("0000000").ToCharArray()

    bit_array(6) = Convert.ToInt32(Me.chk_sunday.Checked).ToString()  ' Sunday
    bit_array(5) = Convert.ToInt32(Me.chk_monday.Checked).ToString()  ' Monday 
    bit_array(4) = Convert.ToInt32(Me.chk_tuesday.Checked).ToString()  ' Tuesday
    bit_array(3) = Convert.ToInt32(Me.chk_wednesday.Checked).ToString()  ' Wednesday
    bit_array(2) = Convert.ToInt32(Me.chk_thursday.Checked).ToString()  ' Thursday
    bit_array(1) = Convert.ToInt32(Me.chk_friday.Checked).ToString()  ' Friday
    bit_array(0) = Convert.ToInt32(Me.chk_saturday.Checked).ToString()  ' Saturday

    Return Convert.ToInt32(bit_array, 2)

  End Function ' GetScheduleDays

#Region " Events "

  Private Sub cmb_time_step_ValueChangedEvent() Handles cmb_time_step.ValueChangedEvent
    If cmb_time_step.Value = 0 Then
      dtp_time_to.Visible = False
      dtp_time_from.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(252)
    Else
      dtp_time_to.Visible = True
      dtp_time_from.Text = GLB_NLS_GUI_STATISTICS.GetString(309)
    End If
  End Sub ' cmb_time_step_ValueChangedEvent

#End Region ' Events
End Class ' frm_mailing_programming_edit
