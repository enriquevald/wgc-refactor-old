'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_gaming_tables_types_sel.vb
' DESCRIPTION:   This screen allows to view the gaming tables types.
' AUTHOR:        Javier Barea
' CREATION DATE: 17-DEC-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 17-DEC-2013  JBP    Initial version
' 18-FEB-2014  RRR    Fixed Bug WIG-643: Search error solved"
' 06-OCT-2017  DHA    Bug 30155:WIGOS-4774 GUI Games type window - Reset button does not clear the name filter
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Text


Public Class frm_gaming_tables_types_sel
  Inherits frm_base_sel



#Region " Constants "
  ' Grid Columns
  Private Const MAX_NAME_LEN As Integer = 50

  ' Grid Columns
  Private Const GRID_COLUMN_SENTINEL As Integer = 0
  Private Const GRID_COLUMN_GAME_TABLE_NAME As Integer = 1
  Private Const GRID_COLUMN_GAME_TABLE_ENABLED As Integer = 2
  Private Const GRID_COLUMN_GAME_TABLE_ID As Integer = 3


  ' Grid Columns
  Private Const GRID_COLUMN_SENTINEL_WIDTH As Integer = 200
  Private Const GRID_COLUMN_GAME_TABLE_NAME_WIDTH As Integer = 2500
  Private Const GRID_COLUMN_GAME_TABLE_ENABLED_WIDTH As Integer = 1200
  Private Const GRID_COLUMN_GAME_TABLE_ID_WIDTH As Integer = 0

  ' Grid setup
  Private Const GRID_HEADER_ROWS As Integer = 1
  Private Const GRID_COLUMNS As Integer = 4

  ' Sql Columns
  Private Const SQL_COLUMN_GAME_TABLE_ID As Integer = 0
  Private Const SQL_COLUMN_GAME_TABLE_NAME As Integer = 1
  Private Const SQL_COLUMN_GAME_TABLE_ENABLED As Integer = 2

#End Region ' Constants

#Region "Enums"

#End Region ' Enums

#Region " Members "

  ' Filter
  Private m_report_name As String
  Private m_report_enabled As String

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_GAMING_TABLES_TYPES_SEL

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' JBP: Use custom buttons for especific buttons design

    ' Gaming Tables Types
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3428)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False

    ' Delete
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_CONTROLS.GetString(11)

    ' Edit
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_CONTROLS.GetString(25)

    ' New
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Text = GLB_NLS_GUI_CONTROLS.GetString(6)

    ' -- Table Name --
    Me.txt_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3419)               ' 3419 "Nombre"
    Me.txt_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, MAX_NAME_LEN, 0)

    ' -- Enabled -- 
    Me.gb_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4400)             ' "Habilitado"
    Me.chk_yes.Text = GLB_NLS_GUI_INVOICING.GetString(479)                       ' "Yes"
    Me.chk_no.Text = GLB_NLS_GUI_INVOICING.GetString(480)                        ' "No"

    ' Excel 
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_EXCEL).Visible = True

    ' Exit
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)

    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ' PURPOSE: Process clicks on data grid (double-clicks) and select button
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Select Case ButtonId

      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        Call MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_DELETE)
      Case ENUM_BUTTON.BUTTON_CUSTOM_1
        Call MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_SELECT)
      Case ENUM_BUTTON.BUTTON_CUSTOM_2
        Call MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_NEW)
      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset  

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  '
  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _str_sql As StringBuilder

    _str_sql = New StringBuilder()

    _str_sql.AppendLine("   SELECT    GTT_GAMING_TABLE_TYPE_ID  ")
    _str_sql.AppendLine("           , GTT_NAME   				        ")
    _str_sql.AppendLine("           , GTT_ENABLED 				      ")
    _str_sql.AppendLine("     FROM    GAMING_TABLES_TYPES	      ")

    ' Where 
    _str_sql.AppendLine(GetSqlWhere())

    ' Order
    _str_sql.AppendLine(" ORDER BY    GTT_NAME                  ")

    Return _str_sql.ToString()

  End Function ' GUI_FilterGetSqlQuery 

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  '
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _id As Integer
    Dim _name As String
    Dim _enabled As Boolean

    ' Game table name
    _name = DbRow.Value(SQL_COLUMN_GAME_TABLE_NAME)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_TABLE_NAME).Value = _name

    ' Game table enabled
    If (Not DbRow.IsNull(SQL_COLUMN_GAME_TABLE_ENABLED)) Then
      _enabled = DbRow.Value(SQL_COLUMN_GAME_TABLE_ENABLED)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_TABLE_ENABLED).Value = IIf(_enabled, GLB_NLS_GUI_INVOICING.GetString(479), GLB_NLS_GUI_INVOICING.GetString(480))
    End If

    ' Game table ID
    If (Not DbRow.IsNull(SQL_COLUMN_GAME_TABLE_ID)) Then
      _id = DbRow.Value(SQL_COLUMN_GAME_TABLE_ID)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_TABLE_ID).Value = _id
    End If

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the
  '          main SQL Query.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Function GetSqlWhere() As String

    Dim _str_where As String = String.Empty

    ' -- Name Filter -- 
    If Not String.IsNullOrEmpty(Me.txt_name.Value) Then
      _str_where = _str_where & " AND (GTT_NAME LIKE '%" & DataTable_FilterValue(Me.txt_name.Value) & "%') "
    End If

    ' -- Enabled Filter -- 
    '   � Only Enableds
    If Me.chk_yes.Checked And Not Me.chk_no.Checked Then
      _str_where = _str_where & " AND (GTT_ENABLED = 1) "
    End If
    '   � Only Disableds
    If Not Me.chk_yes.Checked And Me.chk_no.Checked Then
      _str_where = _str_where & " AND (GTT_ENABLED = 0) "
    End If

    If Not String.IsNullOrEmpty(_str_where) Then
      _str_where = " WHERE " & _str_where.Remove(_str_where.IndexOf("AND"), 3)
    End If

    Return _str_where
  End Function ' GetSqlWhere

  ' PURPOSE: Activated when a row of the grid is double clicked or selected, so it can be edited
  '  PARAMS:
  '     - INPUT:
  '               
  '     - OUTPUT:
  '          
  ' RETURNS:
  '     - none
  Protected Overrides Sub GUI_EditSelectedItem()

    Dim _idx_row As Int32
    Dim _gaming_table_type_id As Integer
    Dim _frm_edit As Object

    'Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    If _idx_row = Me.Grid.NumRows Then
      Return
    End If

    ' Get the Draw ID and Name, and launch the editor
    _gaming_table_type_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_GAME_TABLE_ID).Value

    _frm_edit = New frm_gaming_tables_types_edit

    Call _frm_edit.ShowEditItem(_gaming_table_type_id)

    _frm_edit = Nothing
    Call Me.Grid.Focus()

  End Sub 'GUI_EditSelectedItem

  Protected Overrides Sub GUI_EditNewItem()
    Dim _frm As frm_gaming_tables_types_edit

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_gaming_tables_types_edit
    _frm.ShowNewItem()

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub ' GUI_EditNewItem

  Protected Overrides Sub Finalize()
    MyBase.Finalize()
  End Sub ' Finalize

#Region " GUI Reports "

  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    With Me.Grid

    End With

    Call MyBase.GUI_ReportParams(ExcelData)

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    ' Name Filter
    If Not String.IsNullOrEmpty(m_report_name) Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(3419), m_report_name)
    End If

    ' Enabled Filter
    If Not String.IsNullOrEmpty(m_report_enabled) Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(3422), m_report_enabled)
    End If

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_report_name = String.Empty
    m_report_enabled = String.Empty

    ' Name filter
    If Not String.IsNullOrEmpty(Me.txt_name.Value) Then
      m_report_name = Me.txt_name.Value
    End If

    ' Enabled filter
    If Me.chk_yes.Checked Then
      m_report_enabled = Me.chk_yes.Text
    End If

    If Me.chk_no.Checked Then
      If String.IsNullOrEmpty(m_report_enabled) Then
        m_report_enabled = Me.chk_no.Text
      Else
        m_report_enabled = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353) ' All
      End If
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region

#Region " Private Functions"

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, False)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
      .Sortable = True

      ' SENTINEL
      .Column(GRID_COLUMN_SENTINEL).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_SENTINEL).Width = GRID_COLUMN_SENTINEL_WIDTH
      .Column(GRID_COLUMN_SENTINEL).HighLightWhenSelected = False
      .Column(GRID_COLUMN_SENTINEL).IsColumnPrintable = False

      ' GAME TABLE NAME
      .Column(GRID_COLUMN_GAME_TABLE_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3419)
      .Column(GRID_COLUMN_GAME_TABLE_NAME).Width = GRID_COLUMN_GAME_TABLE_NAME_WIDTH '2200
      .Column(GRID_COLUMN_GAME_TABLE_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' GAME TABLE ENABLED
      .Column(GRID_COLUMN_GAME_TABLE_ENABLED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4400)
      .Column(GRID_COLUMN_GAME_TABLE_ENABLED).Width = GRID_COLUMN_GAME_TABLE_ENABLED_WIDTH '1100
      .Column(GRID_COLUMN_GAME_TABLE_ENABLED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' GAME TABLE ID (No Visible)
      .Column(GRID_COLUMN_GAME_TABLE_ID).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_GAME_TABLE_ID).Width = GRID_COLUMN_GAME_TABLE_ID_WIDTH '0
      .Column(GRID_COLUMN_GAME_TABLE_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub SetDefaultValues()

    chk_yes.Checked = True
    chk_no.Checked = False
    txt_name.Value = String.Empty

  End Sub ' SetDefaultValues

#End Region ' Private 

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region

#Region " Events "

#End Region ' Events

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

  End Sub

End Class