<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_short_over_report
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.opt_working_day = New System.Windows.Forms.RadioButton()
    Me.opt_movement_date = New System.Windows.Forms.RadioButton()
    Me.uc_working_day_selector = New GUI_Controls.uc_daily_session_selector()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1147, 147)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 151)
    Me.panel_data.Size = New System.Drawing.Size(1147, 404)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1141, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1141, 4)
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.opt_working_day)
    Me.gb_date.Controls.Add(Me.opt_movement_date)
    Me.gb_date.Controls.Add(Me.uc_working_day_selector)
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(0, 6)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(301, 138)
    Me.gb_date.TabIndex = 16
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'opt_working_day
    '
    Me.opt_working_day.Location = New System.Drawing.Point(8, 39)
    Me.opt_working_day.Name = "opt_working_day"
    Me.opt_working_day.Size = New System.Drawing.Size(210, 24)
    Me.opt_working_day.TabIndex = 1
    Me.opt_working_day.Text = "xSearchByWorkingDay"
    '
    'opt_movement_date
    '
    Me.opt_movement_date.Location = New System.Drawing.Point(8, 19)
    Me.opt_movement_date.Name = "opt_movement_date"
    Me.opt_movement_date.Size = New System.Drawing.Size(210, 24)
    Me.opt_movement_date.TabIndex = 0
    Me.opt_movement_date.Text = "xSearchByMovementDate"
    '
    'uc_working_day_selector
    '
    Me.uc_working_day_selector.ClosingTime = 0
    Me.uc_working_day_selector.ClosingTimeEnabled = False
    Me.uc_working_day_selector.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_working_day_selector.FromDateSelected = True
    Me.uc_working_day_selector.Location = New System.Drawing.Point(8, 48)
    Me.uc_working_day_selector.Name = "uc_working_day_selector"
    Me.uc_working_day_selector.ShowBorder = False
    Me.uc_working_day_selector.Size = New System.Drawing.Size(287, 77)
    Me.uc_working_day_selector.TabIndex = 17
    Me.uc_working_day_selector.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_working_day_selector.ToDateSelected = True
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(35, 97)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = False
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(222, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 3
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(35, 69)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = False
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(222, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 2
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'frm_short_over_report
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1157, 559)
    Me.Name = "frm_short_over_report"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_short_over_report"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents opt_working_day As System.Windows.Forms.RadioButton
  Friend WithEvents opt_movement_date As System.Windows.Forms.RadioButton
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents uc_working_day_selector As GUI_Controls.uc_daily_session_selector
End Class
