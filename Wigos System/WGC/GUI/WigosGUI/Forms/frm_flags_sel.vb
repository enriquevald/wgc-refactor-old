'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_flags_sel.vb
' DESCRIPTION:   Flags selection form
' AUTHOR:        Xavier Cots
' CREATION DATE: 16-AUG-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 16-AUG-2012  XCD    Initial version
' 06-MAY-2013  RBG    Fixed Bug #746: It makes no sense to work in this form with multiselection
' 17-JUN-2013  NMR    Fixed Bug #863: Don't show message when user stops operation
' 21-APR-2017  ETP    WIGOS-706: Junkets - Technical story - Refactor FlagType Enum to common
'--------------------------------------------------------------------

Option Strict Off
Option Explicit On 

#Region " Imports "

Imports System.Data.OleDb
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Data.SqlClient
Imports WSI.Common

#End Region

Public Class frm_flags_sel
  Inherits GUI_Controls.frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents chk_disabled As System.Windows.Forms.CheckBox
  Friend WithEvents chk_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents ef_name As GUI_Controls.uc_entry_field
  Friend WithEvents chk_show_expired As System.Windows.Forms.CheckBox
  Friend WithEvents gb_type As System.Windows.Forms.GroupBox
  Friend WithEvents chk_automatic As System.Windows.Forms.CheckBox
  Friend WithEvents chk_manual As System.Windows.Forms.CheckBox

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.gb_status = New System.Windows.Forms.GroupBox()
    Me.chk_disabled = New System.Windows.Forms.CheckBox()
    Me.chk_enabled = New System.Windows.Forms.CheckBox()
    Me.ef_name = New GUI_Controls.uc_entry_field()
    Me.chk_show_expired = New System.Windows.Forms.CheckBox()
    Me.gb_type = New System.Windows.Forms.GroupBox()
    Me.chk_automatic = New System.Windows.Forms.CheckBox()
    Me.chk_manual = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_status.SuspendLayout()
    Me.gb_type.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_type)
    Me.panel_filter.Controls.Add(Me.chk_show_expired)
    Me.panel_filter.Controls.Add(Me.ef_name)
    Me.panel_filter.Controls.Add(Me.gb_status)
    Me.panel_filter.Size = New System.Drawing.Size(704, 86)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_status, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_name, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_show_expired, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_type, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 90)
    Me.panel_data.Size = New System.Drawing.Size(704, 363)
    Me.panel_data.TabIndex = 3
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(698, 17)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(698, 4)
    '
    'gb_status
    '
    Me.gb_status.Controls.Add(Me.chk_disabled)
    Me.gb_status.Controls.Add(Me.chk_enabled)
    Me.gb_status.Location = New System.Drawing.Point(230, 6)
    Me.gb_status.Name = "gb_status"
    Me.gb_status.Size = New System.Drawing.Size(142, 66)
    Me.gb_status.TabIndex = 2
    Me.gb_status.TabStop = False
    Me.gb_status.Text = "xStatus"
    '
    'chk_disabled
    '
    Me.chk_disabled.AutoSize = True
    Me.chk_disabled.Location = New System.Drawing.Point(6, 39)
    Me.chk_disabled.Name = "chk_disabled"
    Me.chk_disabled.Size = New System.Drawing.Size(116, 17)
    Me.chk_disabled.TabIndex = 1
    Me.chk_disabled.Text = "xDeshabilitadas"
    Me.chk_disabled.UseVisualStyleBackColor = True
    '
    'chk_enabled
    '
    Me.chk_enabled.AutoSize = True
    Me.chk_enabled.Location = New System.Drawing.Point(6, 16)
    Me.chk_enabled.Name = "chk_enabled"
    Me.chk_enabled.Size = New System.Drawing.Size(95, 17)
    Me.chk_enabled.TabIndex = 0
    Me.chk_enabled.Text = "xHabilitadas"
    Me.chk_enabled.UseVisualStyleBackColor = True
    '
    'ef_name
    '
    Me.ef_name.DoubleValue = 0.0R
    Me.ef_name.IntegerValue = 0
    Me.ef_name.IsReadOnly = False
    Me.ef_name.Location = New System.Drawing.Point(6, 14)
    Me.ef_name.Name = "ef_name"
    Me.ef_name.PlaceHolder = Nothing
    Me.ef_name.Size = New System.Drawing.Size(206, 24)
    Me.ef_name.SufixText = "Sufix Text"
    Me.ef_name.SufixTextVisible = True
    Me.ef_name.TabIndex = 0
    Me.ef_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_name.TextValue = ""
    Me.ef_name.TextWidth = 60
    Me.ef_name.Value = ""
    Me.ef_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_show_expired
    '
    Me.chk_show_expired.AutoSize = True
    Me.chk_show_expired.Location = New System.Drawing.Point(48, 45)
    Me.chk_show_expired.Name = "chk_show_expired"
    Me.chk_show_expired.Size = New System.Drawing.Size(136, 17)
    Me.chk_show_expired.TabIndex = 1
    Me.chk_show_expired.Text = "xMostrar expiradas"
    Me.chk_show_expired.UseVisualStyleBackColor = True
    '
    'gb_type
    '
    Me.gb_type.Controls.Add(Me.chk_automatic)
    Me.gb_type.Controls.Add(Me.chk_manual)
    Me.gb_type.Location = New System.Drawing.Point(378, 6)
    Me.gb_type.Name = "gb_type"
    Me.gb_type.Size = New System.Drawing.Size(142, 66)
    Me.gb_type.TabIndex = 3
    Me.gb_type.TabStop = False
    Me.gb_type.Text = "xType"
    '
    'chk_automatic
    '
    Me.chk_automatic.AutoSize = True
    Me.chk_automatic.Location = New System.Drawing.Point(6, 39)
    Me.chk_automatic.Name = "chk_automatic"
    Me.chk_automatic.Size = New System.Drawing.Size(90, 17)
    Me.chk_automatic.TabIndex = 1
    Me.chk_automatic.Text = "xAutomatic"
    Me.chk_automatic.UseVisualStyleBackColor = True
    '
    'chk_manual
    '
    Me.chk_manual.AutoSize = True
    Me.chk_manual.Location = New System.Drawing.Point(6, 16)
    Me.chk_manual.Name = "chk_manual"
    Me.chk_manual.Size = New System.Drawing.Size(73, 17)
    Me.chk_manual.TabIndex = 0
    Me.chk_manual.Text = "xManual"
    Me.chk_manual.UseVisualStyleBackColor = True
    '
    'frm_flags_sel
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(712, 457)
    Me.Name = "frm_flags_sel"
    Me.ShowInTaskbar = False
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_status.ResumeLayout(False)
    Me.gb_status.PerformLayout()
    Me.gb_type.ResumeLayout(False)
    Me.gb_type.PerformLayout()
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Constants "

  ' Grid Rows
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_FLAG_ID As Integer = 1
  Private Const GRID_COLUMN_FLAG_COLOR As Integer = 2
  Private Const GRID_COLUMN_FLAG_NAME As Integer = 3
  Private Const GRID_COLUMN_FLAG_STATUS As Integer = 4
  Private Const GRID_COLUMN_FLAG_EXPIRATION_INTERVAL As Integer = 5
  Private Const GRID_COLUMN_FLAG_TYPE As Integer = 6
  Private Const GRID_COLUMN_FLAG_TYPE_ENUM As Integer = 7

  Private Const SQL_COLUMN_FLAG_ID As Integer = 0
  Private Const SQL_COLUMN_FLAG_NAME As Integer = 1
  Private Const SQL_COLUMN_FLAG_STATUS As Integer = 2
  Private Const SQL_COLUMN_FLAG_EXPIRATION_INTERVAL As Integer = 3
  Private Const SQL_COLUMN_FLAG_EXPIRATION_TYPE As Integer = 4
  Private Const SQL_COLUMN_FLAG_EXPIRATION_DATE As Integer = 5
  Private Const SQL_COLUMN_FLAG_COLOR As Integer = 6
  Private Const SQL_COLUMN_FLAG_TYPE As Integer = 7

  Private Const GRID_COLUMNS As Integer = 8
  Private Const GRID_HEADER_ROWS As Integer = 1

#End Region

#Region " Members "
  ' For return items selected in selection mode
  Private FlagsSelected() As Integer

  ' For report
  Private m_status As String
  Private m_flag_name As String
  Private m_show_expired As String
  Private m_type As String

#End Region

#Region " Properties "

#End Region

#Region " Overridable GUI function "

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_FLAGS_SEL
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1261)

    ' GUI Buttons - Visible
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_EXCEL).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_PRINT).Visible = False

    ' GUI Buttons - Enabled
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Enabled = False

    ' Text
    ' Cancel button text =>  "Exit"
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)
    ' New button text =>  "Nueva"
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1)

    ' Flag name entry field
    Me.ef_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286) ' Bandera

    ' Status
    Me.chk_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(260)  'Habilitada
    Me.chk_disabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(248) 'Deshabilitada
    Me.chk_show_expired.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1350) 'Mostrar expiradas

    ' Filters set format
    Call ef_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, CLASS_FLAG.MAX_NAME_LEN)

    Me.chk_manual.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2756) ' Manual
    Me.chk_automatic.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8020) ' Automatic

    Me.gb_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(262) ' Manual
    Me.gb_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8084) ' Type

    ' Default values
    SetDefaultValues()

    ' Grid style View
    Call GUI_StyleView()

  End Sub ' GUI_InitControls

  Protected Overrides Sub GUI_GetSelectedItems()

    Dim _idx As Integer

    ' Init
    Erase FlagsSelected
    FlagsSelected = Me.Grid.SelectedRows()

    If IsNothing(FlagsSelected) Then
      Exit Sub
    Else
      _idx = 0
      For Each _selected_row As Integer In Me.Grid.SelectedRows
        FlagsSelected(_idx) = Me.Grid.Cell(_selected_row, GRID_COLUMN_FLAG_ID).Value
        _idx += 1
      Next
    End If

  End Sub 'GUI_GetSelectedItems

  Protected Overrides Sub GUI_EditSelectedItem()

    Dim _idx_row As Short
    Dim _item_id As Integer
    Dim _item_name As String
    Dim _frm_edit As Object

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    If _idx_row = Me.Grid.NumRows Then
      Return
    End If

    If Me.Grid.Cell(_idx_row, GRID_COLUMN_FLAG_TYPE_ENUM).Value = AccountFlag.FlagTypes.Automatic Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8068), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Return
    End If

    ' Get the flag ID and launch editor
    _item_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_FLAG_ID).Value
    _item_name = Me.Grid.Cell(_idx_row, GRID_COLUMN_FLAG_NAME).Value
    _frm_edit = New frm_flags_edit

    Call _frm_edit.ShowEditItem(_item_id, _item_name)
    _frm_edit = Nothing
    Call Me.Grid.Focus()

  End Sub 'GUI_EditSelectedItem

  Protected Overrides Function GUI_FilterCheck() As Boolean
    Return True
  End Function 'GUI_FilterCheck

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _str_sql As System.Text.StringBuilder

    _str_sql = New System.Text.StringBuilder()

    _str_sql.AppendLine(" SELECT FL_FLAG_ID ")
    _str_sql.AppendLine("      , FL_NAME ")
    _str_sql.AppendLine("      , FL_STATUS ")
    _str_sql.AppendLine("      , FL_EXPIRATION_INTERVAL ")
    _str_sql.AppendLine("      , FL_EXPIRATION_TYPE ")
    _str_sql.AppendLine("      , FL_EXPIRATION_DATE ")
    _str_sql.AppendLine("      , FL_COLOR ")
    _str_sql.AppendLine("      , FL_TYPE ")

    _str_sql.AppendLine(" FROM FLAGS ")

    _str_sql.AppendLine(GetSqlWhere())

    _str_sql.AppendLine(" ORDER BY FL_TYPE ASC, FL_NAME ASC, FL_FLAG_ID ASC ")

    Return _str_sql.ToString()

  End Function 'GUI_FilterGetSqlQuery

  Protected Overrides Sub GUI_FilterReset()

    SetDefaultValues()

  End Sub 'GUI_FilterReset

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Call MyBase.GUI_SetupRow(RowIndex, DbRow)
    Dim _str_expiration As String
    Dim _flag_color As Color


    ' Flag Color
    _flag_color = Color.FromArgb(Convert.ToInt32(DbRow.Value(SQL_COLUMN_FLAG_COLOR)))

    ' CHAPUZA para pintar la celda de color negro
    If (_flag_color.A = Color.Black.A And _
        _flag_color.R = Color.Black.R And _
        _flag_color.G = Color.Black.G And _
        _flag_color.B = Color.Black.B) Then
      _flag_color = Color.FromArgb(_flag_color.A, _flag_color.R, _flag_color.G, _flag_color.B + 1)
    End If

    ' Flag Color
    Me.Grid.Cell(RowIndex, GRID_COLUMN_FLAG_COLOR).BackColor = _flag_color

    ' Flag Name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_FLAG_NAME).Value = DbRow.Value(SQL_COLUMN_FLAG_NAME).ToString

    ' Flag Status
    Me.Grid.Cell(RowIndex, GRID_COLUMN_FLAG_STATUS).Value = IIf(DbRow.Value(SQL_COLUMN_FLAG_STATUS), _
                                                                GLB_NLS_GUI_PLAYER_TRACKING.GetString(248), _
                                                                GLB_NLS_GUI_PLAYER_TRACKING.GetString(260))

    ' Flag expiration
    _str_expiration = ""
    If DbRow.Value(SQL_COLUMN_FLAG_EXPIRATION_INTERVAL) Then
      _str_expiration = DbRow.Value(SQL_COLUMN_FLAG_EXPIRATION_INTERVAL)
    End If

    Select Case DbRow.Value(SQL_COLUMN_FLAG_EXPIRATION_TYPE)
      Case FLAG_EXPIRATION_TYPE.MINUTES
        _str_expiration = _str_expiration & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1351) 'Minutos
      Case FLAG_EXPIRATION_TYPE.HOURS
        _str_expiration = _str_expiration & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(259) 'Horas
      Case FLAG_EXPIRATION_TYPE.DAYS
        _str_expiration = _str_expiration & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(258) ' D�as
      Case FLAG_EXPIRATION_TYPE.MONTH
        _str_expiration = _str_expiration & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1289) ' Meses
      Case FLAG_EXPIRATION_TYPE.DATETIME
        _str_expiration = GUI_FormatDate(DbRow.Value(SQL_COLUMN_FLAG_EXPIRATION_DATE), _
                                                    ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                    ENUM_FORMAT_TIME.FORMAT_HHMM)
      Case Else
        _str_expiration = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1287) ' Nunca
    End Select

    Me.Grid.Cell(RowIndex, GRID_COLUMN_FLAG_EXPIRATION_INTERVAL).Value = _str_expiration

    ' Flag type
    Me.Grid.Cell(RowIndex, GRID_COLUMN_FLAG_TYPE).Value = IIf(DbRow.Value(SQL_COLUMN_FLAG_TYPE).ToString = AccountFlag.FlagTypes.Manual, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2756), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8020))
    Me.Grid.Cell(RowIndex, GRID_COLUMN_FLAG_TYPE_ENUM).Value = DbRow.Value(SQL_COLUMN_FLAG_TYPE).ToString

    Return True
  End Function ' GUI_SetupRow

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_NEW
        Call EditNewFlag()
      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub 'GUI_ButtonClick

  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM
  End Function

  'PURPOSE: Define the ExecuteQuery customized

  ' PARAMS:
  '    - INPUT:

  '    - OUTPUT:

  'RETURNS:
  '    -
  Protected Overrides Sub GUI_ExecuteQueryCustom()
    Dim _str_sql As String
    Dim _db_trx As WSI.Common.DB_TRX
    Dim _sql_da As SqlDataAdapter
    Dim _dataset As DataSet = Nothing
    Dim _datatable As DataTable
    Dim _datarow As System.Data.DataRow
    Dim _n As Int32

    Try

      ' Obtain the SqlQuery
      _str_sql = GUI_FilterGetSqlQuery()

      If String.IsNullOrEmpty(_str_sql) Then
        Return
      End If

      _db_trx = New WSI.Common.DB_TRX()
      _sql_da = New SqlDataAdapter(New SqlCommand(_str_sql))
      _sql_da.SelectCommand.CommandTimeout = 100
      _dataset = New DataSet()

      _db_trx.Fill(_sql_da, _dataset)

      _datatable = _dataset.Tables(0)

      Me.Grid.Redraw = False

      If Not ShowHugeNumberOfRows(_datatable.Rows.Count) Then
        MyBase.m_user_canceled_data_shows = True
        Exit Sub
      End If

      For _n = 0 To _datatable.Rows.Count - 1

        If Not GUI_DoEvents(Me.Grid) Then
          Exit Sub
        End If

        _datarow = _datatable.Rows(_n Mod _datatable.Rows.Count)

        Me.Grid.AddRow()
        Call GUI_SetupRow(Me.Grid.NumRows - 1, New CLASS_DB_ROW(_datarow))

      Next _n

      _sql_da.Dispose()
      _db_trx.Dispose()

    Catch ex As OutOfMemoryException
      ' Specific error: Out of memory catch the exception.
      Me.Grid.Redraw = True
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(150), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "Flags selection", _
                            "GUI_ExecuteQueryCustom", _
                            ex.Message)
    Catch ex As Exception
      ' An error has occurred in the query execution
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "Flags selection", _
                            "GUI_ExecuteQueryCustom", _
                            ex.Message)

    Finally
      If Not Me.IsDisposed Then
        Me.Grid.Redraw = True
      End If

      If _dataset IsNot Nothing Then
        Call _dataset.Clear()
        Call _dataset.Dispose()
        _dataset = Nothing
      End If
    End Try

  End Sub

#Region " GUI Reports "

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286), m_flag_name)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(280), m_status)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1350), m_show_expired)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(254), m_type)

  End Sub

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)
    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Params.Title = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1261)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_PORTRAIT

  End Sub

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_status = ""
    m_flag_name = ""
    m_show_expired = IIf(Me.chk_show_expired.Checked, _
                        GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), _
                        GLB_NLS_GUI_PLAYER_TRACKING.GetString(279))

    ' Flag name
    If Not String.IsNullOrEmpty(Me.ef_name.Value()) Then
      m_flag_name = Me.ef_name.Value()
    End If

    ' Status
    If Me.chk_enabled.Checked Then
      m_status = chk_enabled.Text()
    End If

    If Me.chk_disabled.Checked Then
      If String.IsNullOrEmpty(m_status) Then
        m_status = chk_disabled.Text()
      Else
        m_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353) ' All
      End If
    End If

    m_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353) ' All

    If chk_manual.Checked() And Not chk_automatic.Checked() Then
      m_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2756) ' Manual
    End If

    If chk_automatic.Checked() And Not chk_manual.Checked() Then
      m_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8020) ' Automatic
    End If

  End Sub

#End Region ' GUI Reports

#End Region

#Region " Private Functions "

  ' PURPOSE: Open user edit form to create New User
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Sub EditNewFlag()

    Dim idx_row As Short
    ''XVV Variable not use Dim row_type As Integer
    Dim frm_edit As Object

    ' Search the first row selected
    For idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(idx_row).IsSelected Then
        Exit For
      End If
    Next

    frm_edit = New frm_flags_edit

    Call frm_edit.ShowNewItem()
    frm_edit = Nothing
    Call Me.Grid.Focus()

  End Sub 'EditNewUser

  Private Sub GUI_StyleView()

    Call Me.Grid.Init(GRID_COLUMNS, GRID_HEADER_ROWS)

    If Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION Then
      Me.Grid.SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    End If

    ' COLUMN_INDEX
    Me.Grid.Column(GRID_COLUMN_INDEX).Header.Text = " "
    Me.Grid.Column(GRID_COLUMN_INDEX).Width = 200
    Me.Grid.Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
    Me.Grid.Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

    ' FLAG_ID
    Me.Grid.Column(GRID_COLUMN_FLAG_ID).Width = 0
    Me.Grid.Column(GRID_COLUMN_FLAG_ID).Mapping = SQL_COLUMN_FLAG_ID

    ' FLAG_COLOR
    Me.Grid.Column(GRID_COLUMN_FLAG_COLOR).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286) ' Bandera
    Me.Grid.Column(GRID_COLUMN_FLAG_COLOR).Width = 200
    Me.Grid.Column(GRID_COLUMN_FLAG_COLOR).HighLightWhenSelected = False

    ' FLAG_NAME
    Me.Grid.Column(GRID_COLUMN_FLAG_NAME).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286) ' Bandera
    Me.Grid.Column(GRID_COLUMN_FLAG_NAME).Width = 2650
    Me.Grid.Column(GRID_COLUMN_FLAG_NAME).Mapping = SQL_COLUMN_FLAG_NAME
    Me.Grid.Column(GRID_COLUMN_FLAG_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    Me.Grid.Column(GRID_COLUMN_FLAG_NAME).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    ' FLAG_STATUS
    Me.Grid.Column(GRID_COLUMN_FLAG_STATUS).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(262) ' Estado
    Me.Grid.Column(GRID_COLUMN_FLAG_STATUS).Width = 1700
    Me.Grid.Column(GRID_COLUMN_FLAG_STATUS).Mapping = SQL_COLUMN_FLAG_STATUS
    Me.Grid.Column(GRID_COLUMN_FLAG_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    ' FLAG_EXPIRATION_INTERVAL
    Me.Grid.Column(GRID_COLUMN_FLAG_EXPIRATION_INTERVAL).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1288) ' Expiraci�n
    Me.Grid.Column(GRID_COLUMN_FLAG_EXPIRATION_INTERVAL).Width = 1700
    Me.Grid.Column(GRID_COLUMN_FLAG_EXPIRATION_INTERVAL).Mapping = SQL_COLUMN_FLAG_EXPIRATION_INTERVAL
    Me.Grid.Column(GRID_COLUMN_FLAG_EXPIRATION_INTERVAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    ' FLAG_TYPE
    Me.Grid.Column(GRID_COLUMN_FLAG_TYPE).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(254) ' Type
    Me.Grid.Column(GRID_COLUMN_FLAG_TYPE).Width = 1400
    Me.Grid.Column(GRID_COLUMN_FLAG_TYPE).Mapping = SQL_COLUMN_FLAG_TYPE
    Me.Grid.Column(GRID_COLUMN_FLAG_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    ' FLAG_TYPE
    Me.Grid.Column(GRID_COLUMN_FLAG_TYPE_ENUM).Width = 0
    
  End Sub 'GUI_StyleView

  Private Sub SetDefaultValues()
    chk_show_expired.Checked = False
    chk_enabled.Checked = True
    chk_disabled.Checked = False
    chk_automatic.Checked = False
    chk_manual.Checked = True

    If Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_SELECTION Then
      chk_enabled.Enabled = False
      chk_disabled.Enabled = False
      chk_show_expired.Enabled = False
    End If

    ef_name.Value = ""

  End Sub ' SetDefaultValues

  Private Function GetSqlWhere() As String

    Dim _str_where As System.Text.StringBuilder
    Dim _str_expiration As System.Text.StringBuilder

    _str_where = New System.Text.StringBuilder()
    _str_expiration = New System.Text.StringBuilder()

    If Not chk_show_expired.Checked() Then
      ' Only active
      _str_where.AppendLine(" AND (FL_EXPIRATION_DATE IS NULL OR FL_EXPIRATION_DATE > GETDATE()) ")
    End If

    ' Enabled | Disabled
    If chk_enabled.Checked() And Not chk_disabled.Checked() Then
      _str_where.AppendLine(" AND FL_STATUS = " & CLASS_FLAG.STATUS.ENABLED)
    End If

    If chk_disabled.Checked() And Not chk_enabled.Checked() Then
      _str_where.AppendLine(" AND FL_STATUS = " & CLASS_FLAG.STATUS.DISABLED)
    End If

    ' Name filter
    If Not String.IsNullOrEmpty(ef_name.Value()) Then
      _str_where.AppendLine(" AND " & GUI_FilterField("FL_NAME", ef_name.Value(), False, False, True))
    End If

    ' Type
    If chk_manual.Checked() And Not chk_automatic.Checked() Then
      _str_where.AppendLine(" AND FL_TYPE = " & AccountFlag.FlagTypes.Manual)
    End If

    If chk_automatic.Checked() And Not chk_manual.Checked() Then
      _str_where.AppendLine(" AND FL_TYPE = " & AccountFlag.FlagTypes.Automatic)
    End If

    If Not String.IsNullOrEmpty(_str_where.ToString()) Then
      _str_where.Remove(0, 4) ' Remove first " AND"
      _str_where.Insert(0, " WHERE", 1)
    End If

    Return _str_where.ToString()

  End Function ' GetSqlWhere

#End Region

#Region " Public Functions "

  ' PURPOSE : Opens dialog with default settings for edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

  ' PURPOSE : Opens dialog with default settings for select mode
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Function ShowForSelect()

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_SELECTION
    Me.Display(True)
    Return FlagsSelected

  End Function ' ShowForSelect

#End Region

#Region " Events "

#End Region

End Class
