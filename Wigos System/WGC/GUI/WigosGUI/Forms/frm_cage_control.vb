'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_cage_control
' DESCRIPTION:   
' AUTHOR:        Carlos C�ceres Gonz�lez y Francesc Borrell �vila
' CREATION DATE: 13-Nov-2013
'
' REVISION HISTORY:
'
' Date        Author       Description 
' ----------  ------       -----------------------------------------
' 13-NOV-2013 CCG & FBA    First Release.
' 09-JAN-2014 FBA          Fixed bug WIGOSTITO-960.
' 09-JAN-2014 CCG & FBA    Fixed bug WIGOSTITO-959.
' 27-JAN-2014 SMN          Fixed bug WIG-545.
' 14-FEB-2014 CCG          Fixed bug WIG-633.
' 18-FEB-2014 JAB          Added a empty element in the form's combos.
' 20-FEB-2014 JCA          Modified not to repaint the denominations Grid all time.
' 25-FEB-2014 ICS & FBA    Added support for terminal collection with cage movement.
' 26-FEB-2014 ICS & LEM    Fixed Bug WIGOSTITO-1094: Wrong message in stacker collection without cage movement. 
' 26-FEB-2014 HBB          Fixed Bug WIGOSTITO-1093: Error on automatic tickets collection from terminals.
' 27-FEB-2014 FBA          Fixed Bug WIGOS-677: Error when gambling table visits empty.
' 27-FEB-2014 CCG          Fixed Bug WIGOS-679: Avoid check if there are some amount introduced for stacker collection.
' 27-FEB-2014 LEM          Fixed Bug WIGOSTITO-1097: Wrong message to confirm changes on closed collections view.
' 04-MAR-2014 CCG          Fixed Bug WIGOS-687: The Cash Cage Count reports the user that make the operation in the auditory.
' 07-MAR-2014 CCG          Fixed Bug WIG-705: Control enter press event when the button OK isn't visible.
' 12-MAR-2014 CCG          Fixed Bug WIGOSTITO-1110.
' 13-MAR-2014 JCA          Fixed Bug WIG-723.
' 17-MAR-2014 JCA          Fixed Bug WIG-741: Promobox or Sys-Acceptor Collections can not open when is collected.
' 17-MAR-2014 CCG          Fixed Bug WIGOSTITO-1143: Autofill denomination grid in terminal collections.
' 18-MAR-2014 JCA & LEM    Fixed Bug WIG-748.
' 19-MAR-2014 CCG          Fixed Bug WIGOSTITO-1151: Unbalanced Collections appears as balanced.
' 19-MAR-2014 JCA          Fixed Bug WIGOSTITO-1152: The general parameter is ignored "Cage - Cage Collection.Default Session".
' 20-MAR-2014 JAB          Added functionality to print cage movement details.
' 21-MAR-2014 CCG          Fixed Bug WIG-760: Autoselect a cage session in count cage when there is only one session opened.
' 24-MAR-2014 CCG          Fixed Bug WIGOSTITO-1163: National currency tab doesn't appears in the first place.
' 24-MAR-2014 CCG          Fixed Bug WIGOSTITO-1165: When GP Cashier.MaxAllowedDeposit = 0, is impossible to make a send from cash cage.
' 25-MAR-2014 LEM          If expected ticket has been inserted on AddTicketCollection only show a popup (don't generate alarm).
' 28-MAR-2014 CCG          Fixed Bug WIG-771: Changes the terminal entry field size for largest names.
' 08-APR-2014 CCG          Added logic for money requests from Cash Desk.
' 09-APR-2014 CCG          Fixed Bug WIG-811: Remove Tickets TITO row in a Gambling table collection if TITO is no enabled.
' 10-APR-2014 CCG          Fixed Bug WIG-812: Increase to 7 digits for quantity field.
' 22-APR-2014 CCG          Added logic for the new General Param "Cage.ShowDenominations".
' 02-MAY-2014 CCG          Fixed Bug WIG-884: Show "Amount" rather than $1 in a Count Cage. 
' 05-MAY-2014 CCG          Fixed Bug WIG-883: Custom Collection doesn't work only with Chips tips.
' 06-MAY-2014 CCG          Fixed Bug WIG-887: Wrong auditory message.
' 06-MAY-2014 CCG          Fixed Bug WIG-895: Hide Check, Bank Card and Tickets TITO in a pending request.
' 06-MAY-2014 CCG          New logic for GP: Cage.AutoMode.
' 08-MAY-2014 LEM          Fixed Bug WIG-906: Wrong collection with offline tickets.
' 09-MAY-2014 LEM          Fixed Bug WIGOSTITO-1220: Error on collection with unexpected tickets added.
' 09-MAY-2014 JCA          Fixed Bug WIGOSTITO-1221: Error on stacker cancel collection when calculate stock.
' 14-MAY-2014 HBB & DRV    Added new Massive collection mode.
' 19-MAY-2014 RCI & DRV    Fixed Bug WIGOSTITO-1224.
' 20-MAY-2014 LEM & DRV    Fixed Bug WIG-927 Couldn't load a collection that has been canceled and collected again.
' 20-MAY-2014 JCA          Fixed Bug WIG-930: Wrong audit info.
' 22-MAY-2014 DRV          Fixed Bug WIG-940: No suggested amounts are shown in massive collection mode.
' 23-MAY-2014 HBB          Fixed Bug WIG-926: In Costumed send it cant be shown check and Bank card.
' 27-MAY-2014 LEM          Fixed Bug WIG-956: Movement cancellation when cage session is closed.
' 29-MAY-2014 JAB          Added permissions: To buttons "Excel" and "Print".
' 06-JUN-2014 JAB          Added two decimal characters in denominations fields.
' 30-JUN-2014 LEM          Fixed Bug WIG-1058: There are not audit of  RequestOperation cancellation.
' 30-JUN-2014 LEM          Fixed Bug WIG-1059: Wrong message in ToCashier cancellation..
' 30-JUN-2014 JBC & JPJ    Added user templates.
' 11-JUL-2014 JPJ          Fixed Bug WIGOS-1075: Templates should only be shown when sending.
' 24-JUL-2014 LEM          Fixed Bug WIG-1066: Wrong window title in RequestOperation.
' 25-JUL-2014 DLL          Chips are moved to a separate table
' 29-JUL-2014 SGB          Added alarm if amount is 0
' 31-JUL-2014 JBC & DCS    Fixed Bug WIGOS-1139: When Cage.AutoMode active and a template is selected, we selected custom check by default
' 06-AUG-2014 JAB          Fixed Bug WIG-1154: When new cage movement is created with coins only, quantity column has not value.
' 21-AUG-2014 SGB          Fixed Bug WIG-1167: The transmission is controlled from cage to cashier integrated
' 28-AUG-2014 LEM & DLL    Fixed Bug WIG-1218: Cage stock mismatch with tips
' 08-SEP-2014 DLL          Fixed Bug WIGOSTITO 1252: It is not possible to make a money collection
' 15-SEP-2014 DRV          Changed note counter visualization
' 23-SEP-2014 JPJ          Fixed Bug WIGOSTITO 1260:Some elements of the template were aren't getting loaded
' 25-SEP-2014 JPJ          Fixed Bug WIGOSTITO 1288: Saving a template with a 7 digit number
' 25-SEP-2014 JPJ          Fixed Bug WIGOSTITO 1284: Deleting all the elements of the grid shows an error
' 26-SEP-2014 JBC          Fixed Bug WIG-1289: Solved chips without denominations
' 26-SEP-2014 LRS          The totals were incorrect by adding special element
' 29-SEP-2014 JBC          Fixed Bug WIG-1338: Error when we save an existing template.
' 03-NOV-2014 JBC          Fixed Bug WIG-1381: Now audit shows the template used on the send
' 07-OCT-2014 DRV          Show note counter events when a collection is ended
' 10-OCT-2014 JAB          Fixed Bug WIG-1465: Incorrect message when cage movement to gaming table (without cashier), is opened.
' 14-OCT-2014 LEM          Fixed Bug WIG-1488: it's not possible send checks or bank card info to custom target.
' 15-OCT-2014 JPJ          Fixed Bug WIG-1500: Exception occurs when loading a template and language is set to Spanish.
' 20-OCT-2014 DLL          Fixed Bug WIG-1530: Send money with 0 amount
' 20-OCT-2014 RRR          Replaced every "MXN" hard coded appearance for "RegionalOptions.CurrencyISOCode" general parameter value
' 21-OCT-2014 DLL          Fixed Bug WIG-1510:keep certain information entered before loading a template
' 22-OCT-2014 LEM          Use FORM_CAGE_SHOW_EXPECTED_AMOUNTS permission to Show/Hide expected/difference columns reports.
' 24-OCT-2014 LEM          Fixed Bug WIG-1573: related to WIG-1488.
' 28-OCT-2014 LEM          Fixed Bug WIG-1588: GamingTables and Custom movements must hide "expected" data.
' 30-OCT-2014 HBB          Fixed Bug WIG-1470: Tickets TITO can't be shown in gaming table collection
' 04-NOV-2014 SGB          Fixed Bug WIG-1638: Incorrect show alarm in recaudation less than 0.5.
' 13-NOV-2014 HBB          Fixed Bug WIG-1660: Money Collection: when a money collection is cleaned, total ticket sum has value -$0 or +$0 
' 19-NOV-2014 DRV          Fixed Bug WIG-1711: Added Massive collection for cashless mode
' 20-NOV-2014 LEM          Set column system type to grid columns (use for Excel/Print).
' 20-NOV-2014 LEM          Shows message on DB error at initial data reading.
' 21-NOV-2014 JPJ          Fixed Bug WIG-1689: First time entering the form, loading a template and changing destiny
' 25-NOV-2014 LEM          Fixed Bug WIG-1609: Wrong denominations in Cage Count
' 01-DIC-2014 LRS          Fixed Bug WIG-1777: Can save TITO tickets in concepts sended
' 03-DIC-2014 JBC          Fixed Bug WIG-1744: Templates with a disabled destination will select first enabled destination
' 10-DEC-2014 DRV          Decimal currency formatting: DUT 201411 - Tito Chile - Currency formatting without decimals
' 18-DEC-2014 JBC          Fixed Bug WIG-1530: Clean button sets Rest 1.0D to quantity.
' 20-JAN-2015 FJC          Set features form (size and autoscroll panels) when size's form exceeds desktop area
' 26-JAN-2015 MPO & HBB    Fixed Bug WIG-1971: Quantity difference column not hide 
' 16-FEB-2015 FOS          Defect WIG-2069: Change movement_id value from Int16 to Int64
' 16-FEB-2015 DCS          Fixed Bug WIG-2023: Incorrect message in Operation Type
' 05-MAR-2015 OPC          Fixed Bug WIG-2089: Fixed some bugs.
' 13-MAR-2015 YNM          Fixed Bug WIG-2150: The currencies denomination list includes Card and Check denominations.  
' 08-APR-2015 SGB          Backlog Item 541 Task 951: Show form for export of result cash cage count.
' 09-APR-2015 RMS          Gaming Day
' 08-APR-2015 SGB          Fixed Bug 1085: Don't show in excel filter cage session if open multi cage session.
' 21-APR-2015 MPO          Fixed Bug WIG-2157 and WIG-2231
' 21-APR-2015 YNM          Fixed Bug TFS-1142: Error when Massive Recolect  
' 30-APR-2015 MPO          Fixed Bug TFS-Bug 1472: Totals calculated incorrectly
' 04-MAY-2015 MPO          Fixed Bug WIG-2271: Error on clean (reset) data
' 07-MAY-2015 JBC          Fixed Bug WIG-2283: Sending money, cash cage session must not be before the current day.
' 05-MAY-2015 YNM          Fixed Bug WIG-2088: Cage Movements: Incorrect movement data when export to Excel
' 12-MAY-2015 YNM & DLL    Fixed Bug WIG-2295: Cage Count incorrect
' 24-MAY-2015 YNM          Fixed Bug WIG-2365: Cage Count fixed some errors
' 26-MAY-2015 DHA          Fixed Bug WIG-2390: error when gaming table player tracking is disabled
' 28-MAY-2015 MPO          Fixed Bug WIG-2391
' 03-JUN-2015 DHA          Fixed Bug WIG-2408: if player tracking enabled and automode disable, the gaming table can't be managed
' 04-JUN-2015 YNM          Fixed Bug WIG-2406: Totals in a Collection doesn't update properly.
' 04-JUN-2015 YNM          Fixed Bug WIG-2409: Cashier collection, quantity 0.
' 05-JUN-2015 DLL          Fixed Bug WIG-2388: Error when collect gaming table
' 10-JUN-2015 YNM          Fixed Bug WIG-2425: Error Cage Count when Cashier_Quantity = 1 and Denomination = -100
' 11-JUN-2015 YNM          Fixed Bug WIG-2430: Terminal Collection error on Total Collected.
' 16-JUN-2015 SGB          Fixed Bug backlog item 2079: Personalized delivery destination: Allow though not same day.
' 01-JUL-2015 FAV          Fixed Bug WIG-2505: Hides "Cancel" button to a cashier with integrated table
' 08-SEP-2015 FOS          Backlog Item 3709
' 15-SEP-2015 FAV          Fixed Bug WIG-4080: It disables the OK button (read permissions) in "New cage movements"
' 22-SEP-2015 FOS          Task 4458:Recaudaci�n de monedas: Mejoras recaudaci�n
' 23-SEP-2015 FOS          Task 4458:Recaudaci�n de monedas: Mejoras recaudaci�n - arqueo de b�veda
' 23-SEP-2015 FOS          Task 4458:Recaudaci�n de monedas: Mejoras recaudaci�n - recaudaci�n descuadrada (billetes y monedas)
' 05-OCT-2015 YNM          Fixed Bug WIG-4825: Tickets collection is unbalanced when collect close cash movement
' 07-OCT-2015 YNM          Fixed Bug TFS-5058: GUI: error canceling Collection with Tickets
' 15-OCT-2015 YNM          Fixed Bug TFS-5348: GUI: unhandled exception while trying to collect tickets from Gaming Table 
' 27-OCT-2015 FOS          Fixed Bug TFS-5637: GUI: Error Coin collection collect
' 03-NOV-2015 JPJ          Fixed Bug TFS-6042: It can't collect from Cage - Exception
' 05-NOV-2015 JPJ          Fixed Bug TFS-6228: Exception while canceling
' 09-NOV-2015 FOS & JPJ    Fixed Bug TFS-6228: Totals don't appear
' 12-NOV-2015 JPJ          Fixed Bug TFS-6228: Totals don't appear
' 20-NOV-2015 YNM          Fixed Bug 6859: Error when bill scanner is used
' 07-DEC-2015 JPJ          Fixed Bug 7332: Stacker movements, incorrect bill amount 
' 07-JAN-2016 JPJ          Fixed Bug 8177 & 8172: Only the remainder is taken into account when collecting from a cashier.
' 20-JAN-2016 JPJ          Fixed Bug 8177: Incorrect coin cashier collection.
' 21-JAN-2016 JPJ          Fixed Bug 8548: Incorrect coin cashier collection.
' 13-JAN-2016 DHA          Task 8287: Floor Dual Currency
' 25-JAN-2016 JMV          Fixed Bug 8391: Modo "Cashless": excepci�n no controlada al acceder al detalle de movimiento de b�veda
' 26-JAN-2016 JPJ          Product Backlog Item 7890: Floor Dual currency 
' 28-JAN-2016 JPJ          Product Backlog Item 7890: Floor Dual currency 
' 10-FEB-2016 DHA          Bug 9181: Dual currency error cleaning TITO tickets grid
' 17-FEB-2016 DDS          PBI 9512: Currencies grids parametrize editable cells when destination is Gaming table
' 18-FEB-2016 RAB          Bug 9523:Ref. 16572: en los detalles de movimiento de Boveda a veces no aparece el "Usuario" o "Caja"
' 24-FEB-2016 FAV          Fixed Bug 9925: Changed a NLS in a message box
' 25-FEB-2016 DDS          Fixed Bug 9747: Reorganize logic to check currencies and messages when destination is Gaming table and Templates
' 26-FEB-2016 JPJ          Fixed Bug 10036: Note counter events are removed while in validation screen
' 07-MAR-2016 ETP          Fixed Bug 10224: Validate if stock has changed.
' 08-MAR-2016 DDS          Fixed Bug 9747: Coins values are not cleaned from Total Grid
' 09-MAR-2016 ETP          Fixed Bug 10458:Arqueo de b�veda: bot�n sobrante en el formulario
' 08-MAR-2016 RAB & ETP    Bug 10220:Ceuta - different column order "Raised" and "Expected" according to action to display dome
' 14-MAR-2016 DDS          Fixed Bug 10632: TITO + FloorDualCurrency does no show Credit cards on denominations grid
' 14-MAR-2016 FAV          Fixed Bug 10373, 10376, 10365: Fixed value showed in Deposits and Withdrawals
' 16-MAR-2016 RAB          Bug 10658: Appear on "Tito Tickets" in sending personalized destination
' 22-MAR-2016 ETP          Bug 10912: Need to view expected values in gaming tables.
' 30-MAR-2016 DDS          Bug 5378: dataview.ToTable() loses expression column EXPECTED_COLUMN on DataSet
' 05-APR-2016 DDS          Bug 9209: Confusing workflow when cancelling a money collection from GUI
' 23-MAR-2016 JML          Product Backlog Item 9755:Gaming Tables (Fase 1): Cage
' 13-APR-2016 FOS          Fixed Bug 11656: Not controlled exception.
' 25-APR-2016 ETP          Fixed Bug 11160: Not controlled exception when quantity is "9999999" and denomination > 200
' 22-ABR-2016 JML          Product Backlog Item 9758:Gaming Tables (Fase 1): General cage report
' 28-ABR-2016 JML          Product Backlog Item 10825:Gaming Tables (Fase 1): Items, tickets & chips (Cage)
' 02-MAY-2016 JML          Product Backlog Item 10825:Gaming Tables (Fase 1): Items, tickets & chips (Cage)
' 05-MAY-2016 FOS          Fixed Bug 11152: Do not allow to send credit card and ticket concepts to cashiers.
' 26-APR-2016 YNM          PBI 10953: CountR - Cash It Out: GUI - Env�o/Recaudaci�n de CountR - Modificaci�n frm_cage_control
' 24-MAY-2016 DHA          Product Backlog Item 9848: drop box gaming tables
' 09-JUN-2016 RAB          PBI 11755: Tables (Phase 1): Correction errors as a result of the backward compatibility of the chips group
' 10-JUN-2016 JML          Product Backlog Item 13541:UNPLANNED - Temas varios Winions Sprint 25 -  Add closing stock window
' 15-JUN-2016 JMV          Bug 14559:Arqueo de b�veda: excepci�n no controlada tras activar mesas de juego y su seguimiento de sesiones
' 16-JUN-2016 RAB          Product Backlog Item 9853: GamingTables (Phase 3): Fixed Bank (Phase 1)
' 16-JUN-2016 JML          Product Backlog Item 14486: Review - Backward compatibility.
' 17-JUN-2016 JML          Product Backlog Item 14493: Gaming tables (Fase 4): Cage 
' 20-JUN-2016 JMV          Bug 14686:B�veda, env�os personalizados: que aparezcan las tarjetas bancarias
' 27-JUN-2016 JCA          Bug 14909:Countr: Error al realizar un env�o a Terminal. Excepci�n de la aplicaci�n
' 30-JUN-2016 ETP          Bug 14992:GUI: No se puede recaudar terminales si no hay denominaciones de fichas configuradas
' 14-JUL-2016 JCA          Bug 15214:CountR: No se cargan los detalles de movimiento de b�veda despu�s de haber realizado un env�o
' 18-JUL-2016 JCA          Bug 15594:CountR: se puede enviar/recaudar fichas, cheque/tarjeta bancaria
' 16-AGO-2016 FJC          Fixed Bug 16692:Sesi�n de caja: al intentar cancelar una recaudaci�n sale un mensaje irrelevante
' 28-JUN-2016 DHA          Product Backlog Item 15903: Gaming tables (Fase 5): Drop box collection on Cashier
' 25-AGO-2016 FJC          Fixed Bug 16888:CountR: No aparece el numero de tickets en el Recaudado.
' 28-JUN-2016 DHA          Product Backlog Item 16766: added check and bank card on gaming tables collections
' 30-AUG-2016 RGR          Bug 11475: Excel does not export cash movements concepts
' 30-AUG-2016 JBP          Bug 17105: CountR: No se actualiza la columna "entregado" de la sesi�n de CountR cuando se cancela su recaudaci�n asociada.
'                          Bug 17167:CountR: Duplicado de sesiones y otras inconsistencias
' 30-AUG-2016 RGR          Bug 11475: Excel does not export cash movements concepts
' 12-SEP-2016 JML          Bug 17344: Gaming tables: Number of visits is not reported correctly when close the table.
' 12-SEP-2016 JML          Bug 17590: Errors when is active the dual currency.
' 13-SEP-2016 RAB          Bug 17625: Error in terminal fundraising details screen.
' 15-SEP-2016 FOS          Bug 17378: You can not collect a stacker if it includes tickets TITO
' 16-SEP-2016 JML          PBI 16768: Sprint Review 28 - Changes & enhancements
' 21-SEP-2016 FOS          Bug 17901: Exception when realizes a new count cage for chips
' 21-SEP-2016 FAV          Bug 17881: Incorrect value in Tickets Total column (for a collect)
' 22-SEP-2016 FAV          Bug 17806: Mico: Collect with exception
' 27-SEP-2016 FAV          Fixed Bug 8816: Allow request to cage without cage opened.
' 27-SEP-2016 RAB          Bug 17625: Error in terminal fundraising details screen.
' 27-SEP-2016 RAB          Bug 16691: Cage: Excel reports tonnage of vault not appears the description of coin
' 28-SEP-2016 DHA          PBI 17747: cancel gaming tables operations
' 28-SEP-2016 ETP          Bug 18087: Mismatch when coins are reported by terminal.
' 29-SEP-2016 JML          Bug 18252: Cage: unhandled exception to select and modify the template in a new movement cage.
' 29-SEP-2016 JML          Bug 18183: Gaming tables: Having gaming tables enabled as "Only cage Management" is not loaded template with bank fixed to do the first send.
' 29-SEP-2016 FAV          Bug 17806: Mico: Exception loading collect
' 29-SEP-2016 ETP          Bug 18281: Total Grid has incorrect Gaming table chips codes.
' 30-SEP-2016 FOS          Bug 18234: Error in templates when send a new movement to gamingtables
' 30-SEP-2016 JML          Bug 17877: Gaming tables: First time do not save the template
' 10-OCT-2016 ETP          Bug 18640: Cage: Incorrect value when collecting Credit card, check and Rest.
' 11-OCT-2016 FAV          Bug 18849: Cage: Invalid value in 'Tickets TITO' when the user change the 'Rest' value
' 11-OCT-2016 JML          Fixed Bug 18980 You can not open gamming table with template sent from the cage, if it contains more groups of chips that gaming table has autorized.
' 13-OCT-2016 JML          Fixed Bug 18952:GUI: New movement Cage unhandled exception to save a template configured to send bills to cage when the GP GamingTables.TransferCurrency.Enabled = 0
' 20-10-2016  ATB          Bug 18909: Al realizar una recaudaci�n de b�veda, sale un pop up de env�o
' 26-OCT-2016 FAV          PBI 19526: EGASA - CS Ticket 1459: The grid makes scrolling unnecessary
' 15-NOV-2016 RAB          Bug 11368: Maximum restriction fails to deposit in GamingTables.
' 16-NOV-2016 PDM          PBI 20408: Arqueo de b�veda.
' 22-NOV-2016 DHA          Bug 6550: Errors when audit terminal collection on coins case
' 28-NOV-2016 LTC          Bug 18139: Gaming Tables: Card withdrawal must not be permmited en GUI
' 07-DIC-2016 ETP          Bug 21162: Error calculating totals
' 14-DEC-2016 DHA          Bug 21545: error on sending to gaming table without auto-cage, marks the sent as finalized
' 21-DEC-2016 JML          Fixed Bug 16844: The system doubles the values of Machine Collection
' 23-DEC-2016 DPC          Bug 21954:WigosGUI: El rat�n se queda inmovilizado al presionar el bot�n "Clear" en la ventana de "Recaudaci�n"
' 28-DIC-2016 FJC          Bug 21984:CountR: no se recaudan los tickets pagados
' 29-DEC-2016 MS           Bug 21727:Edici�n de Mesa de Juego: error al pulsar "Plantilla" -> Ya no deja realizar ninguna operaci�n
' 11-ENE-2017 DPC          Bug 22496:Nuevo movimiento de b�veda: Cambiar entre Env�o y Recaudaci�n inhabilita el check de Cajero
' 11-JAN-2017 CCG          Bug 22475:Nuevo movimiento de b�veda: chequeos incorrectos en Kiosko de redenci�n
' 17-JAN-2016 DHA          Bug 22799: error when visualize data values on a gaming table sent movement
' 17-JAN-2016 DHA          Bug 22799: error when visualize data values on a gaming table sent movement
' 18-JAN-2017 FAV          Bug 19045: TITO, Error in Collection for tickets with Cage.automode = 1
' 24-JAN-2017 FOS          Bug 23321: CountR, Ticket tito collected error
' 25-JAN-2017 ATB          Bug 22723: Report "Cash Flow by Terminal" do not update the ticket total amount after a stacker change
' 25-JAN-2017 JBP          Bug 23421:Cage Movement - No aparece el detalle en los env�os a b�veda
' 26-JAN-2017 AMF          Bug 23503:Cage: No se cargan correctamente los terminales al crear un nuevo movimiento
' 26-JAN-2017 FOS          Bug 23390: CountR, Now is hidden button "clean"
' 30-JAN-2017 DPC          Bug 23503:Cage: No se cargan correctamente los terminales al crear un nuevo movimiento
' 08-FEB-2017 JML          Fixed Bug 24256:Cage: Deliveries are allowed when it should not be so
' 02-MAR-2017 DHA          Bug 25245: error showing chips sets for gaming tables configuration
' 07-MAR-2017 FOS          Bug 24606: Error showing expected values when user haven't permision
' 16-MAR-2017 FOS          Bug 25839: Error showing State in recaudation.
' 31-MAY-2017 FOS          Bug 26363: Error showing expected amount in total, when general param Collection.SuggestDenominationAmounts = 0.
' 11-APR-2017 RAB          PBI 26539:MES10 Ticket validation - Prevent the use of dealer copy tickets (GUI)
' 12-JUL-2017 LTC          Bug 28669:WIGOS-3573 It does not allow to introduce concepts in personalized collection
' 14-JUL-2017 DHA          Bug 28715:WIGOS-3560 WigosGUI shows an error when the user is doing a stacker collection
' 14-JUL-2017 DHA          Bug 28719:WIGOS-3563 A closed and balanced stacker collection is displaying the tickets TITO area unbalanced
' 26-JUL-2017 RAB          Bug 28941:WIGOS-3801 Cage - Tables: It is allowed to send cash to gaming tables from cage with GP GamingTables.TransferCurrency.Enabled = 0
' 11-SEP-2017 RGR          Bug 29696:WIGOS-3728 Minor errors in Collection History screen
' 13-SEP-2017 JML          Bug 29733:WIGOS-3919 Tables - "Solo Gesti�n B�veda" mode. The checkBox "close" of new cage movement form, clean the before options selected
' 24-OCT-2017 RAB          Bug 30374:WIGOS-5969 [Ticket #9699] Pending Collections - Colocar como pendientes
' 14-DEC-2017 DHA          Bug 31082:WIGOS-7169 Release 03.005.141 - Cage movement, duplicate amount
' 02-FEB-2018 DPC          Bug 31406:[WIGOS-4669]: Cage - Tables: It is allowed to send cash to gaming tables from cage with GP GamingTables.TransferCurrency.Enabled = 0
' 13-FEB-2018 RGR          Bug 31511:WIGOS-8161 GUI log error message - EXCEPTION: Unable to cast object of type
' 09-MAR-2018 AGS          Bug 31877:WIGOS-5938 A new cash cage movement to a gambling table is not loading properly the currency exchanges tabs
' 09-MAR-2017 JML          Fixed Bug 31876:WIGOS-8731 Stacker collection not editable from "Collection" but it is editable from "Collection History"
' 05-APR-2018 AGS          Bug 32193:WIGOS-9359 [Ticket #13266] Problemas en la pantalla de arqueo - Sala 131 Crown Aguascalientes
' 10-APR-2018 AGS          Bug 32276:WIGOS-9938 Cash Cage count: wrong behaviour when removing amounts in the list
' 28-JUN-2017 FOS          Bug 33411:WIGOS-12936 Gaming tables template - Confirmation pop up should not appear when clicking on Cancel button without changes made.
'------------------------------------------------------------------- 

Option Strict Off
Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common
Imports System.Data.SqlClient
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_Reports.PrintDataset
Imports System.Text
Imports System.Runtime.InteropServices
Imports GUI_Controls.frm_base_sel
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE
Imports WSI.Common.TITO
Imports WigosGUI.NEW_COLLECTION
Imports WSI.Common.NoteScanner
Imports WSI.Common.CountR

Public Delegate Sub InvokeCurrencyGrid(ByVal Currency As NoteReceived, ByVal IsoCode As String)
Public Delegate Sub InvokeTicketGrid(ByVal TicketValidationNumber As String, ByVal TicketValue As Decimal, ByVal ReadByScanner As Boolean, ByVal TicketId As Int64)
Public Delegate Sub InvokeError(ByVal ErrorReceived As IScanned)
Public Delegate Sub InvokeChangeLabelVisibility()
Public Delegate Sub InvokeStatusOkToLabel(ByVal statusOkText As String)

Public Class frm_cage_control
  Inherits frm_base_edit

#Region "Enums"

  Private Enum TOTAL_GRID_COLUMNS
    INDEX = 0
    CURRENCY_TYPE = 1
    CURRENCY = 2
    CURRENCY_TYPE_DESCRIPTION = 3
    BILLS = 4
    BILLS_WITH_SIMBOL = 5
    COINS = 6
    COINS_WITH_SIMBOL = 7
    TOTAL = 8
    TOTAL_WITH_SIMBOL = 9
    CASHIER_TOTAL = 10
    CASHIER_TOTAL_WITH_SIMBOL = 11
    DIFFERENCE_TOTAL = 12
    DIFFERENCE_TOTAL_WITH_SIMBOL = 13
  End Enum

  Public Enum TEMPLATE_CHANGES_STATUS
    NONE = 0
    ADDED = 1
    UPDATED = 2
    DELETED = 3
  End Enum

  Public Enum MODE_EDIT
    NONE = 0
    SEND = 1
    COLLECTION = 2
  End Enum

#End Region ' Enums

#Region "Contants"

  'Grid Currency Column
  Private Const GRID_CURRENCY_COLUMN_INDEX As Integer = 0
  Private Const GRID_CURRENCY_COLUMN_ISO_CODE As Integer = 1
  Private Const GRID_CURRENCY_COLUMN_CURRENCY_TYPE As Integer = 2
  Private Const GRID_CURRENCY_COLUMN_DENOMINATION As Integer = 3
  Private Const GRID_CURRENCY_COLUMN_CURRENCY_TYPE_DESCRIPTION As Integer = 4
  Private Const GRID_CURRENCY_COLUMN_CHIP_COLOR As Integer = 5
  Private Const GRID_CURRENCY_COLUMN_DENOMINATION_WITH_SIMBOL As Integer = 6
  Private Const GRID_CURRENCY_COLUMN_CHIP_SET_NAME As Integer = 7
  Private Const GRID_CURRENCY_COLUMN_CHIP_NAME As Integer = 8
  Private Const GRID_CURRENCY_COLUMN_CHIP_DRAWING As Integer = 9
  Private Const GRID_CURRENCY_COLUMN_CHIP_ID As Integer = 10
  Private Const GRID_CURRENCY_COLUMN_CHIP_SET_ID As Integer = 11

  Private Const GRID_CURRENCY_COLUMN_QUANTITY As Integer = 12
  Private Const GRID_CURRENCY_COLUMN_TOTAL As Integer = 13
  Private Const GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL As Integer = 14
  Private Const GRID_CURRENCY_COLUMN_CASHIER_QUANTITY As Integer = 15
  Private Const GRID_CURRENCY_COLUMN_CASHIER_TOTAL As Integer = 16
  Private Const GRID_CURRENCY_COLUMN_QUANTITY_DIFFERENCE As Integer = 17
  Private Const GRID_CURRENCY_COLUMN_TOTAL_DIFFERENCE As Integer = 18

  'Grid Currency Column Width 
  Private Const GRID_WIDTH_INDEX As Integer = 150
  Private Const GRID_CURRENCY_COLUMN_ISO_CODE_WIDTH As Int32 = 0
  Private Const GRID_CURRENCY_COLUMN_CURRENCY_TYPE_WIDTH As Int32 = 0
  Private Const GRID_CURRENCY_COLUMN_DENOMINATION_WIDTH As Int32 = 0
  Private GRID_CURRENCY_COLUMN_CURRENCY_TYPE_DESCRIPTION_WIDTH As Int32 = 900
  Private GRID_CURRENCY_COLUMN_CHIP_COLOR_WIDTH As Integer = 600
  Private GRID_CURRENCY_COLUMN_DENOMINATION_WITH_SIMBOL_WIDTH As Int32 = 1500
  Private GRID_CURRENCY_COLUMN_CHIP_SET_NAME_WIDTH As Integer = 1200
  Private GRID_CURRENCY_COLUMN_CHIP_NAME_WIDTH As Integer = 1300
  Private GRID_CURRENCY_COLUMN_CHIP_DRAW_WIDTH As Integer = 1200
  Private Const GRID_CURRENCY_COLUMN_CHIP_ID_WIDTH As Integer = 0
  Private Const GRID_CURRENCY_COLUMN_CHIP_SET_ID_WIDTH As Integer = 0
  Private GRID_CURRENCY_COLUMN_QUANTITY_WIDTH As Int32 = 1100
  Private GRID_CURRENCY_COLUMN_TOTAL_WIDTH As Int32 = 0
  Private GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL_WIDTH As Int32 = 1600
  Private GRID_CURRENCY_COLUMN_CASHIER_QUANTITY_WIDTH As Int32 = 1100
  Private GRID_CURRENCY_COLUMN_CASHIER_TOTAL_WIDTH As Int32 = 1600
  Private GRID_CURRENCY_COLUMN_QUANTITY_DIFFERENCE_WIDTH As Int32 = 1100
  Private GRID_CURRENCY_COLUMN_TOTAL_DIFFERENCE_WIDTH As Int32 = 1600

  'Grid Total Column
  Private Const GRID_TOTAL_COLUMN_INDEX As Integer = 0
  Private Const GRID_TOTAL_COLUMN_CURRENCY_TYPE As Integer = 1
  Private Const GRID_TOTAL_COLUMN_CURRENCY As Integer = 2
  Private Const GRID_TOTAL_COLUMN_CURRENCY_TYPE_DESCRIPTION As Integer = 3
  Private Const GRID_TOTAL_COLUMN_BILLS As Integer = 4
  Private Const GRID_TOTAL_COLUMN_BILLS_WITH_SIMBOL As Integer = 5
  Private Const GRID_TOTAL_COLUMN_COINS As Integer = 6
  Private Const GRID_TOTAL_COLUMN_COINS_WITH_SIMBOL As Integer = 7
  Private Const GRID_TOTAL_COLUMN_TOTAL As Integer = 8
  Private Const GRID_TOTAL_COLUMN_TOTAL_WITH_SIMBOL As Integer = 9
  Private Const GRID_TOTAL_COLUMN_TOTAL_CASHIER As Integer = 10
  Private Const GRID_TOTAL_COLUMN_TOTAL_CASHIER_WITH_SIMBOL As Integer = 11
  Private Const GRID_TOTAL_COLUMN_DIFFERENCE_TOTAL As Integer = 12
  Private Const GRID_TOTAL_COLUMN_DIFFERENCE_TOTAL_WITH_SIMBOL As Integer = 13

  'Grid Total Column Width   
  Private GRID_TOTAL_COLUMN_CURRENCY_TYPE_WIDTH As Int32 = 0
  Private GRID_TOTAL_COLUMN_CURRENCY_WIDTH As Int32 = 550
  Private GRID_TOTAL_COLUMN_CURRENCY_TYPE_DESCRIPTION_WIDTH As Int32 = 3700
  Private GRID_TOTAL_COLUMN_BILLS_WIDTH As Int32 = 0
  Private GRID_TOTAL_COLUMN_BILLS_WITH_SIMBOL_WIDTH As Int32 = 2000
  Private GRID_TOTAL_COLUMN_COINS_WIDTH As Int32 = 0
  Private GRID_TOTAL_COLUMN_COINS_WITH_SIMBOL_WIDTH As Int32 = 2000
  Private GRID_TOTAL_COLUMN_TOTAL_WIDTH As Int32 = 0
  Private GRID_TOTAL_COLUMN_TOTAL_WITH_SIMBOL_WIDTH As Int32 = 2000
  Private GRID_TOTAL_COLUMN_TOTAL_CASHIER_WIDTH As Int32 = 0
  Private GRID_TOTAL_COLUMN_TOTAL_CASHIER_WITH_SIMBOL_WIDTH As Int32 = 2000
  Private GRID_TOTAL_COLUMN_DIFFERENCE_TOTAL_WIDTH As Int32 = 0
  Private GRID_TOTAL_COLUMN_DIFFERENCE_TOTAL_WITH_SIMBOL_WIDTH As Int32 = 2000

  Private Const GRID_CURRENCY_COLUMN_COUNT As Integer = 19
  Private Const GRID_TOTAL_COLUMN_COUNT As Integer = 14
  Const GRID_HEADERS_COUNT As Int32 = 2
  Const GRID_TICKETS_COLUMNS_COUNT As Int32 = 13

  Private Const GB_CASHIER_HEIGHT_TO_USER = 87
  Private Const GB_CASHIER_HEIGHT_TO_TERMINAL = 61
  Private Const GB_CASHIER_HEIGHT_TO_GAMING_TABLE = 61
  Private TICKETS_ISOCODE_NLS As String = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4600)

  Private Const FORM_DB_MIN_VERSION As Short = 320 ' TODO: Real MIN_VERSION > 320
  Private Const TABNAME_PREFIX As String = "tab_"
  Private Const ISO_CODE_LENGTH As Integer = 3

  ' Grid Concepts
  Private Const GRID_CONCEPT_ID As Integer = 0
  Private Const GRID_CONCEPT_ONLY_NATIONAL_CURRENCY As Integer = 1
  Private Const GRID_CONCEPT_SOURCE_TARGET_ID As Integer = 2
  Private Const GRID_CONCEPT_NAME As Integer = 3
  Private Const GRID_CONCEPT_AMOUNT As Integer = 4


#Region "Tickets indexes and variables"
  'Index Columns for tickets
  Const GRID_COLUMN_TICKET_NUMBER_CORRECT_FORMAT As Int32 = 0
  Const GRID_COLUMN_TICKET_AMOUNT_ISSUE As Int32 = 1
  Const GRID_COLUMN_TICKET_AMOUNT As Int32 = 2
  Const GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF As Int32 = 3
  Const GRID_COLUMN_TICKET_NUMBER_ORIGINAL_FORMAT As Int32 = 4
  Const GRID_COLUMN_TICKET_ID As Int32 = 5
  Const GRID_COLUMN_TICKET_TYPE As Int32 = 6
  Const GRID_COLUMN_TICKET_STATUS As Int32 = 7
  Const GRID_COLUMN_TICKET_BELONG As Int32 = 8
  Const GRID_COLUMN_TICKET_COLLECTED_MONEY_COLLECTION As Int32 = 9
  Const GRID_COLUMN_TICKET_CAGE_MOVEMENT_ID As Int32 = 10
  Const GRID_COLUMN_TICKET_ISO_CODE As Int32 = 11
  Const GRID_COLUMN_TICKET_ISO_CURRENCY_TYPE As Int32 = 12


  'Counters
  Private Const COUNTER_TOTAL_ITEMS As Integer = 0
  Private Const COUNTER_INFO As Integer = 1
  Private Const COUNTER_WARNING As Integer = 2
  Private Const COUNTER_ERROR As Integer = 3
  Private Const MAX_COUNTERS As Integer = 4

  'Width FOR COLLECTION_DETAILS
  Const GRID_WIDTH_TICKET_OR_NOTE As Int32 = 0
  Const GRID_WIDTH_TICKET_VALIDATION_NUMBER As Int32 = 6300
  Const GRID_WIDTH_TICKET_AMOUNT As Int32 = 3000
  Const GRID_WIDTH_TICKET_AMOUNT_ISSUE As Int32 = 0
  Const GRID_WIDTH_TICKET_VALIDATION_TYPE As Int32 = 0
  Const GRID_WIDTH_TICKET_ID As Int32 = 0
  Const GRID_WIDTH_BILL_REAL_VS_THEO_DIFF As Int32 = 2000
  Const GRID_WIDTH_TICKET_TYPE As Int32 = 0
  Const GRID_WIDTH_TICKET_STATUS As Int32 = 0
  Const GRID_WIDTH_TICKET_BELONG As Int32 = 0

  ' DHA 13-JAN-2016: added columns width for dual currency
  Const GRID_WIDTH_TICKET_VALIDATION_NUMBER_DUAL_CUR As Int32 = 3000
  Const GRID_WIDTH_TICKET_AMOUNT_DUAL_CUR As Int32 = 2000
  Const GRID_WIDTH_TICKET_AMOUNT_ISSUE_DUAL_CUR As Int32 = 2000
  Const GRID_WIDTH_BILL_REAL_VS_THEO_DIFF_DUAL_CUR As Int32 = 2000

  'Counters colors
  Private Const COLOR_ERROR_BACK = ENUM_GUI_COLOR.GUI_COLOR_RED_02
  Private Const COLOR_ERROR_FORE = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_WARNING_BACK = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
  Private Const COLOR_WARNING_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_INFO_BACK = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_INFO_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_ALL_ITEMS_BACK = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_ALL_ITEMS_FORE = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00

  Private Const COLOR_NO_DIFFERENCE = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_POSITIVE_DIFFERENCE = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01
  Private Const COLOR_NEGATIVE_OR_POSITIVE_DIFFERENCE = ENUM_GUI_COLOR.GUI_COLOR_RED_03
  Private Const COLOR_PENDING = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00

  Const TICKETDONTBELONG = -1
  Const TICKETDONTEXIST = -2
  Const TICKETNUMBERREPEATED = 2

  Private m_is_tito_collection As Boolean = False
  Private m_new_collecion_mode As Boolean = False

#End Region

#End Region ' Contants

#Region "Members"

  Private m_cage_cls As CLASS_CAGE_CONTROL
  Private m_cage_template As CLASS_CAGE_CONTROL
  Private m_cell_disabled_color As System.Drawing.Color = System.Drawing.Color.FromArgb(240, 240, 240)
  Private m_cell_text_disabled_color As System.Drawing.Color = System.Drawing.Color.FromArgb(109, 109, 109)
  Private m_cell_enable_color As System.Drawing.Color = Drawing.Color.White
  Private m_cell_text_enable_color As System.Drawing.Color = Drawing.Color.Black
  Private m_new_cage As Boolean
  Private m_nfi As Globalization.NumberFormatInfo
  Private m_initial_status As Cage.CageStatus
  Private m_initial_load As Boolean
  Private m_total_before_edit As Decimal

  Private m_national_currency As String
  Private m_currencies_accepted As String

  Private m_y_offset As Int32
  Private m_first_time As Boolean
  Private m_gaming_values As Boolean

  Public m_is_ok As Boolean
  Public m_is_cancel As Boolean
  Public m_allow_imbalance As Boolean = False
  Public m_mode_edit As MODE_EDIT = MODE_EDIT.NONE

  Private m_countr_enabled As Boolean

  Private m_collection_from_tito_terminal As Boolean
  Private m_uc_grid_denominations_created As Boolean = False
  'Private m_jca_movement_type_checked As String = ""
  'Private m_jca_source_target_checked As String = ""
  Private m_selected_currency_tab As String = ""
  Private m_list_of_theoretical_tickets As List(Of NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS)

  Private m_print_enabled As Boolean = False
  Private m_show_denominations As Cage.ShowDenominationsMode
  Private m_cage_auto_mode As Boolean = False

  Private m_loading_stacker_collection_data As Boolean = False
  Private m_after_load As Boolean = False
  Private m_redraw_grid As Boolean = False
  Private m_only_cashier_is_open As Boolean = False

  'Template
  Private m_original_template As DataTable
  Private m_template_id As Integer
  Private m_original_preferences As DataTable
  Private m_changed_values As Boolean
  Private m_show_without_cage_id As Boolean

  Private Const TABLE_NAME_PREFERENCES As String = "Preferences"
  Private Const TABLE_NAME_CURRENCIES As String = "Currencies"
  Private Const TABLE_COLUMN_OPERATION_TYPE As String = "OperationType"
  Private Const TABLE_COLUMN_TARGET_TYPE As String = "TargetType"
  Private Const MAX_NUMBER_TEMPLATES As Integer = 20

  Private WithEvents m_scanner_manager As ScannerManager

  Private m_frm_counter_event As frm_note_counter_events
  Public Event m_throw_event(ByVal EventText As String)
  Public m_list_of_event As List(Of String)

  Private m_note_counter_flag As Boolean

  Private m_ef_stacker_focused As Boolean = True
  Private m_error_received As Boolean = False

  Private m_lbl_counter_status_update As Integer
  Private m_last_checked As RadioButton

  Dim m_first_error As Boolean
  Dim m_second_error As Boolean
  Dim m_error_text As String
  Dim m_scanner_initialized As Boolean = False

  Dim m_preselected_cage_session As Int64 = -1

  Dim m_gp_note_collection_type As Cage.NoteAcCollectionType
  Dim m_show_expected_data As Boolean

  Dim m_cash_cage_print_selected As Boolean = False
  Dim m_cash_cage_export_excel As Boolean = False

  Dim m_gaming_day As Boolean = False

  Dim m_is_coin_collected_enabled As Boolean = False
  Dim m_cells_are_editable As Boolean = False
  Dim m_gamingtables_transfer_currency_enabled As Boolean = False
  Dim m_is_player_tracking_enabled As Boolean = False
  Dim m_applying_template As Boolean = False

  'Dim m_tab_pages_backup As New Collection()
  Dim m_rb_checked As Int32
  Dim m_cmb_selected_index As Int32

  Dim m_semaphore_tab_refresh As Boolean
  Dim m_currencies_types As DataTable

  Dim m_chips_set_filter As String

  Dim m_currency_tabs_dictionary As Dictionary(Of CurrencyIsoType, TabPage) = New Dictionary(Of CurrencyIsoType, TabPage)

  Dim m_force_grid_clear As Boolean = False

  Dim m_row_height As Int32

  Dim m_list_money_collection As List(Of NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS)

#End Region ' Members

#Region " Properties "

  Public Property IsCountREnabled As Boolean
    Get
      Return m_countr_enabled
    End Get
    Set(value As Boolean)
      m_countr_enabled = value
    End Set
  End Property

  Public Property ShowWithoutCageID As Boolean
    Get
      Return m_show_without_cage_id
    End Get
    Set(value As Boolean)
      m_show_without_cage_id = value
    End Set
  End Property

#End Region

#Region "Overrides functions"

  ' PURPOSE: Sets form Id 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()

#If DEBUG Then
    Call GUI_SetMinDbVersion(226)
#Else
    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)
#End If

    ' Leave this sub empty, as the FormId is set now in the Constructor of the class.
    Me.m_cage_cls = New CLASS_CAGE_CONTROL()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Sets initial values for controls
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_InitControls()
    Dim _table_currencies As DataTable
    Dim _is_countr_enabled As Boolean = Me.m_cage_cls.IsCountR

    MyBase.GUI_InitControls()

    'Hide Controls
    Me.gb_collection_params.Visible = False
    Me.gb_note_counter_status.Visible = False
    Me.gb_note_counter_events.Visible = False
    Me.tb_collections.Visible = False

    Me.m_cage_cls.SourceTargetConceptsData = Me.m_cage_cls.GetSourceTargetConceptsData()

    Me.m_cage_auto_mode = Cage.IsCageAutoMode()
    Me.m_show_expected_data = ShowExpectedData()
    Me.m_gp_note_collection_type = GeneralParam.GetInt32("TITO", "NoteAcCollectionType", Cage.NoteAcCollectionType.ShowAll)
    Me.m_national_currency = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")
    Me.m_currencies_accepted = GeneralParam.GetString("RegionalOptions", "CurrenciesAccepted", Me.m_national_currency)
    Me.m_show_denominations = GeneralParam.GetInt32("Cage", "ShowDenominations", Cage.ShowDenominationsMode.ShowAllDenominations)
    Me.m_gamingtables_transfer_currency_enabled = GeneralParam.GetBoolean("Cage", "GamingTables.TransferCurrency.Enabled", False)
    Me.m_is_player_tracking_enabled = WSI.Common.GeneralParam.GetBoolean("GamingTable.PlayerTracking", "Enabled", False)
    Me.m_currencies_types = m_cage_cls.GetAllowedISOCodeWithoutColor()
    Me.m_cmb_selected_index = -1
    Me.m_only_cashier_is_open = GeneralParam.GetBoolean("Cage", "Transfer.OnlyCashierIsOpen", False)

    If Me.m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal Then
      Me.m_show_denominations = Cage.ShowDenominationsMode.ShowAllDenominations

      Me.rb_terminal.Visible = True
      Me.lbl_error.BackColor = GetColor(COLOR_NEGATIVE_OR_POSITIVE_DIFFERENCE)
    Else
      Me.rb_terminal.Enabled = False
    End If

    Me.m_first_time = True
    Me.m_nfi = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat

    Call Me.ShowHideExpectedColumns()

    _table_currencies = New DataTable()

    Call Me.GUI_StyleSheet()
    Call Me.InitLabels()
    Call Me.InitCombos()
    Call Me.InitOpenMode(_table_currencies)

    Me.ef_ticket.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, 18)
    Me.btn_save_tickets.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2262)
    Me.tf_ok1.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1972)
    Me.tf_error1.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1974)
    Me.tf_pending.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5238)
    Me.ef_num_of_tickets.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5628)
    Me.gb_status1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1971)
    Me.gb_collection_params.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2256)
    Me.ef_insertion_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2251)
    Me.ef_extraction_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2263)
    Me.ef_stacker_id.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2252)
    Me.ef_floor_id.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5200)
    Me.ef_employee.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2896)
    Me.ef_terminal.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2254)
    Me.lbl_notes.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2255)
    Me.lbl_counter.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5197)
    Me.btn_open_events.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5346)
    Me.btn_open_events.Visible = False
    Me.tb_concepts.TabPages(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5472) 'Conceptos

    Me.ef_employee.IsReadOnly = True
    Me.ef_terminal.IsReadOnly = True
    Me.ef_insertion_date.IsReadOnly = True
    Me.ef_extraction_date.IsReadOnly = True
    Me.ef_num_of_tickets.IsReadOnly = True
    Me.ef_stacker_id.Enabled = False
    Me.ef_floor_id.Enabled = False
    Me.ef_stacker_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 18)
    Me.ef_floor_id.SetFilter(FORMAT_TEXT, 6)

    Dim _terminal_types() As Integer = Nothing
    ReDim Preserve _terminal_types(0)
    _terminal_types(0) = TerminalTypes.SAS_HOST

    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = Not GLB_CurrentUser.IsSuperUser AndAlso (Me.m_cage_cls.Status <> Cage.CageStatus.CanceledUncollectedCashierRetirement) _
                                                   AndAlso CurrentUser.Permissions(ENUM_FORM.FORM_CAGE_CONTROL).Write

    If m_print_enabled Then
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_PRINT_ENABLED).Read
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_CONTROLS.GetString(5) ' Imprimir

      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_EXCEL_ENABLED).Read
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = True
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_CONTROLS.GetString(27) ' Excel
    Else
      If Me.m_cage_cls.OpenMode <> CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE Then
        Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = True
      End If
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5208)
      If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MASSIVE Then
        Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = False
      Else
        Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = Me.IsEditingEnabled()
      End If
    End If

    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT Or Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.TEMPLATE_MAINTENANCE
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Type = uc_button.ENUM_BUTTON_TYPE.USER
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Height += 10
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5051) ' Guardar plantilla
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_CAGE_CONTROL).Write AndAlso _
                                                        (Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT Or Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.TEMPLATE_MAINTENANCE)

    Me.gb_template.Visible = Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT Or Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.TEMPLATE_MAINTENANCE
    Me.cmb_cage_template.Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_CAGE_CONTROL).Read AndAlso _
                                   (Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT Or Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.TEMPLATE_MAINTENANCE)

    Call Me.InitialLayoutConfiguration()
    Call Me.CreateCurrencyTabs(_table_currencies)
    Call Me.InitTotalGrid(_table_currencies)
    Call Me.SetButtonsText()

    If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.TEMPLATE_MAINTENANCE Then
      Call TemplateChange()
    End If

    m_list_of_event = New List(Of String)
    m_frm_counter_event = New frm_note_counter_events(m_list_of_event)

    'if there isn't note counter, it won't be initialized
    '07-NOV-2014: the bill counter just be shown in TITO collections(Cahier or terminal)
    If WSI.Common.GeneralParam.GetBoolean("Cage", "BillCounter.Enabled") AndAlso TITO.Utils.IsTitoMode() Then
      If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MASSIVE _
                                      OrElse (Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MOVEMENT _
                                              AndAlso Me.m_cage_cls.OperationType <> WSI.Common.Cage.CageOperationType.RequestOperation) _
                                      OrElse Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_FROM_PROMOBOX_OR_ACCEPTOR _
                                      OrElse (Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT _
                                              AndAlso (Me.m_cage_cls.OperationType = WSI.Common.Cage.CageOperationType.FromCashier _
                                              OrElse Me.m_cage_cls.OperationType = WSI.Common.Cage.CageOperationType.FromCashierClosing _
                                              OrElse Me.m_cage_cls.OperationType = WSI.Common.Cage.CageOperationType.FromTerminal)) Then

        Call InitializeScanner()
      End If
    End If

    Call Me.SetConceptsGridAndTabValues()

    ' LEM & XIT 22-OCT-2014: Temporarily hidden
    Me.ef_num_of_tickets.Visible = False

    ' 21-NOV-2014 JPJ          Fixed Bug WIG-1689: First time entering the form, loading a template and changing destiny
    If m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT Then
      ' RAB 16-MAR-2016: Bug 10658
      m_uc_grid_denominations_created = True

      If Me.m_cage_auto_mode AndAlso (Not WSI.Common.Misc.IsCountREnabled()) Then
        Me.rb_cashier.Enabled = False
        If GamingTableBusinessLogic.GamingTablesMode() = GamingTableBusinessLogic.GT_MODE.GUI_AND_CASHIER Then
          Me.rb_custom.Checked = True
        Else
          Me.rb_gaming_table.Checked = True
        End If
      Else
        Me.rb_cashier.Enabled = True
        Me.rb_cashier.Checked = True

        Me.rb_to_user.Checked = Not Me.m_cage_auto_mode
        Me.rb_to_terminal.Checked = Me.m_cage_auto_mode
        Me.rb_to_user.Visible = Not Me.m_cage_auto_mode
        Me.rb_to_terminal.Visible = Not Me.m_cage_auto_mode
      End If

      Call ReDrawGrids()
    End If

    ' Dispose
    Call SafeDispose(_table_currencies)

    ' Mode edit
    If Me.m_mode_edit = MODE_EDIT.SEND And Me.m_cage_cls.OpenMode <> CLASS_CAGE_CONTROL.OPEN_MODE.TEMPLATE_MAINTENANCE Then
      Me.rb_reception.Enabled = False
      Me.rb_gaming_table.Enabled = False
      Me.rb_custom.Enabled = False
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = False ' Guardar plantilla
    End If

    If Cage.IsCageEnabled() AndAlso _
       m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MOVEMENT AndAlso _
       (m_cage_cls.OperationType = Cage.CageOperationType.FromCashier Or m_cage_cls.OperationType = Cage.CageOperationType.FromCashierClosing) Then

      Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = True
      Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Text = GLB_NLS_GUI_CONTROLS.GetString(2)
    End If

    ' Gaming Day
    Me.m_gaming_day = WSI.Common.Misc.IsGamingDayEnabled()


    ' JMV 22-DEC-2015
    If (WSI.Common.Misc.IsGamingHallMode() AndAlso rb_terminal.Checked) Then
      Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Enabled = False
    End If

    ' Fixed Bug 16692
    If m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MOVEMENT Then
      Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Visible = False
    End If

    If _is_countr_enabled Then
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False
    End If

  End Sub ' GUI_InitControls

  ' PURPOSE: Sets initial focus 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_SetInitialFocus()
    If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MASSIVE Then
      If Utils.IsTitoMode Then
        Me.ef_stacker_id.Select()
        Me.ef_stacker_id.Focus()
      Else
        Me.ef_floor_id.Select()
        Me.ef_floor_id.Focus()
      End If
    Else
      Me.cmb_user.Focus()
    End If
  End Sub 'GUI_SetInitialFocus

  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)

    Select Case m_cage_cls.OpenMode
      Case CLASS_CAGE_CONTROL.OPEN_MODE.VIEW_MOVEMENT, _
           CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT, _
           CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT_FROM_GAMINGTABLES
        CloseCanceled = False

      Case CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_FROM_PROMOBOX_OR_ACCEPTOR, _
           CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MASSIVE, _
           CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MOVEMENT, _
           CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT

        Call CloseNoteCounter()

      Case Else
        MyBase.GUI_Closing(CloseCanceled)

    End Select

  End Sub

  ' PURPOSE : Validate the data presented on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '         - Boolean
  ' RETURNS :
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Dim _nls_id As Integer
    Dim _nls_id_error As Integer
    Dim _show_confirmation As Boolean = True
    Dim _frm_export As frm_export_cash_cage_count

    Select Case Me.m_cage_cls.OpenMode
      Case CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT _
         , CLASS_CAGE_CONTROL.OPEN_MODE.CLOSE_VIRTUAL
        If Not CheckScreenDataOkNewMovement() Then

          Return False
        End If

      Case CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MOVEMENT _
         , CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MASSIVE

        ' 26-FEB-2016 JPJ Fixed Bug 10036: Note counter events are removed while in validation screen
        If m_scanner_initialized = True Then
          RemoveHandler m_scanner_manager.NoteCountReceivedEvent, AddressOf CatchMessageFromScanner
        End If

        If Not CheckScreenDataRecolectMovement() Then
          ' 26-FEB-2016 JPJ Fixed Bug 10036: Note counter events are removed while in validation screen
          If m_scanner_initialized = True AndAlso Not m_scanner_manager.IsEventHandlerRegistered() Then
            AddHandler m_scanner_manager.NoteCountReceivedEvent, AddressOf CatchMessageFromScanner
          End If

          Return False
        End If

      Case CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT _
        , CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT_FROM_GAMINGTABLES
        If m_cage_cls.CageSessionStatus = CASHIER_SESSION_STATUS.CLOSED Then
          If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4381), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO) _
                    <> ENUM_MB_RESULT.MB_RESULT_YES Then
            Return False
          End If
        End If

        If Me.m_cage_cls.OperationType = Cage.CageOperationType.ToCashier _
          Or Me.m_cage_cls.OperationType = Cage.CageOperationType.ToGamingTable Then

          _nls_id = 3013
          _nls_id_error = 4717

        Else

          If Me.m_cage_cls.OperationType = Cage.CageOperationType.FromCustom Then
            _nls_id = 4930
          ElseIf Me.m_cage_cls.OperationType = Cage.CageOperationType.ToCustom Then
            _nls_id = 7133
          Else
            _nls_id = 3377
          End If

          If Me.m_cage_cls.Status = Cage.CageStatus.OK Then
            _nls_id_error = 4446
          Else
            _nls_id_error = 4469
          End If

        End If

        If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(_nls_id), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO) _
           = ENUM_MB_RESULT.MB_RESULT_YES Then

          If Not Me.m_cage_cls.CheckIfSameStatus(Me.m_cage_cls.MovementID, Me.m_cage_cls.Status) Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(_nls_id_error), ENUM_MB_TYPE.MB_TYPE_WARNING)

            Return False
          ElseIf Not (Me.m_cage_cls.OperationType = Cage.CageOperationType.ToCashier Or _
                      Me.m_cage_cls.OperationType = Cage.CageOperationType.ToTerminal Or _
                      Me.m_cage_cls.OperationType = Cage.CageOperationType.ToCustom Or _
                      Me.m_cage_cls.OperationType = Cage.CageOperationType.ToGamingTable) _
                 AndAlso Not CheckCageStock() Then 'checks if there is enough stock in cage
            NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4927), ENUM_MB_TYPE.MB_TYPE_WARNING)

            Return False
          End If
        Else

          Return False
        End If

      Case CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE

        If Me.cmb_cage_sessions.Visible Then
          If String.IsNullOrEmpty(Me.cmb_cage_sessions.TextValue) Then
            NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4554), ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.cmb_cage_sessions.Text) ' Select a cash cage session
            Me.cmb_cage_sessions.Focus()

            Return False
          End If

          If Not Cage.GetCageOpenBySessionId(Me.cmb_cage_sessions.Value) Then
            NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4603), ENUM_MB_TYPE.MB_TYPE_WARNING) ' Cash cage session is closed
            Me.cmb_cage_sessions.Focus()

            Return False
          End If
        End If

        'PDM: VERIFICAR SI EL ARQUEO ES PARCIAL Y SI ESTA PERMITIDO O NO.
        If WSI.Common.GeneralParam.GetBoolean("Cage", "InventoryCount.Total") Then

          If IsCellGridEmpty() Then

            NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7762), ENUM_MB_TYPE.MB_TYPE_WARNING)
            Return False

          End If

        Else

          If Me.m_cage_cls.TableCurrencies.Select("QUANTITY >= 0").Length = 0 Then
            If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3028), _
                                            ENUM_MB_TYPE.MB_TYPE_WARNING, _
                                            ENUM_MB_BTN.MB_BTN_OK) Then
              Return False
            Else
              _show_confirmation = False
            End If
          End If

        End If

        'ETP: Fixed BUG 10224 Check if  data has changed.
        If Not CheckAndUpdateStockData() Then
          Return False
        End If

        If _show_confirmation Then
          _frm_export = New frm_export_cash_cage_count()
          _frm_export.ShowDialog()
          If _frm_export.AcceptCashCageCount Then
            If _frm_export.PrintSelected Then
              m_cash_cage_print_selected = True
            ElseIf _frm_export.ExportToExcel Then
              m_cash_cage_export_excel = True
            End If
          Else
            Return False
          End If
        End If

        'ETP: Fixed BUG 10224 Check again if  data has changed.
        If Not CheckAndUpdateStockData() Then
          Return False
        End If

    End Select

    If Not Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MOVEMENT _
      And Not Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CLOSE_CAGE _
      And Not Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE _
      And Not Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT _
      And Not Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT_FROM_GAMINGTABLES Then

      If Not CheckIfDenominationsAllowed() Then
        Me.tb_currency.SelectedTab.Controls(0).Focus()

        Return False
      End If
    End If

    Return True

  End Function ' GUI_IsScreenDataOk


  ' PURPOSE: It sets the read values from the database to the entry fields
  '         
  ' PARAMS:
  '    - INPUT:
  '         - SqlCtx
  '    - OUTPUT:
  '
  ' RETURNS:
  'Note: If DbReadObject has some properties = -1, it means that there were errors reading the database
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    Dim _cage_control As CLASS_CAGE_CONTROL
    Dim _operation_type As Short
    Dim _open_mode As Int16

    _operation_type = Me.m_cage_cls.OperationType
    _cage_control = DbReadObject
    _open_mode = Me.m_cage_cls.OpenMode

    If Not m_is_tito_collection Then

      If _open_mode = CLASS_CAGE_CONTROL.OPEN_MODE.CLOSE_VIRTUAL Then
        Me.cmb_gaming_table.Value = Me.m_cage_cls.GetGamingTableIdFromCashierSession(Me.m_cage_cls.CashierSessionId)
        Me.ef_gaming_table.TextValue = Me.cmb_gaming_table.TextValue
        Me.cmb_cage_sessions.Value = Me.m_cage_cls.CageSessionId
        Me.ef_cage_session.TextValue = Me.cmb_cage_sessions.TextValue
        _cage_control.OperationType = Me.m_cage_cls.OperationType
        Me.cmb_user.TextValue = Nothing
      Else
        If _open_mode = CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE Then
          _cage_control.OperationType = _operation_type
          _cage_control.CageSessionId = Me.m_cage_cls.CageSessionId 'when COUNT_CAGE m_cage_cls.CageSession is always set (via parameter).
        End If

        If Me.m_cage_cls.CageSessionId = -1 Then 'NO SETEADO
          Me.m_cage_cls.CageSessionId = _cage_control.CageSessionId
        End If
        Me.m_cage_cls.GamingTableId = _cage_control.GamingTableId
        ' 16-FEB-2015 DCS Fixed Bug WIG-2023
        _cage_control.OperationType = Me.m_cage_cls.OperationType
      End If

      Me.m_cage_cls.TerminalCashierID = _cage_control.TerminalCashierID
      Me.m_cage_cls.TargetType = _cage_control.TargetType
      Me.m_cage_cls.GamingTableVisits = _cage_control.GamingTableVisits

      Call Me.SetOperationText(_cage_control.OperationType)
      _cage_control.OpenMode = Me.m_cage_cls.OpenMode
      Me.m_initial_status = _cage_control.Status
      Call Me.EnableDisable()
      Me.m_initial_load = False

      Call Me.FirstUpdateTotalGrid()

    Else ' Is ticket collection
      If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT AndAlso _
         Me.m_cage_cls.CageSessionId > 0 Then
        Me.cmb_cage_sessions.Value = Me.m_cage_cls.CageSessionId
        Me.ef_cage_session.TextValue = Me.cmb_cage_sessions.TextValue
      ElseIf Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MASSIVE AndAlso Me.m_cage_cls.CageSessionId = 0 Then
        Me.cmb_cage_sessions.Value = 0
        Me.ef_cage_session.TextValue = Me.cmb_cage_sessions.TextValue
      End If

      m_is_coin_collected_enabled = _cage_control.m_type_new_collection.is_coin_collection_enabled

      If Not _cage_control.m_type_new_collection.extraction_date.IsNull Then
        Me.ef_extraction_date.TextValue = GUI_FormatDate(_cage_control.m_type_new_collection.extraction_date.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
      Else
        Me.ef_extraction_date.TextValue = ""
      End If

      If Not _cage_control.m_type_new_collection.insertion_date.IsNull Then
        Me.ef_insertion_date.TextValue = GUI_FormatDate(_cage_control.m_type_new_collection.insertion_date.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
      Else
        Me.ef_insertion_date.TextValue = ""
      End If

      Me.ef_extraction_date.Width = 220
      Me.ef_extraction_date.TextWidth = 100
      Me.ef_insertion_date.Width = 220
      Me.ef_insertion_date.TextWidth = 100
      Me.ef_terminal.Width = 425
      Me.ef_employee.Width = 425

      Me.ef_terminal.TextValue = _cage_control.m_type_new_collection.terminal_name
      Me.ef_employee.TextValue = _cage_control.m_type_new_collection.user_name
      Me.tb_notes.Text = _cage_control.m_type_new_collection.notes
      Me.m_cage_cls.m_type_new_collection.money_collection_id = _cage_control.m_type_new_collection.money_collection_id
      Me.ef_stacker_id.TextValue = IIf(_cage_control.m_type_new_collection.stacker_id > 0, _cage_control.m_type_new_collection.stacker_id, "")
      Me.ef_floor_id.TextValue = _cage_control.m_type_new_collection.floor_id
      _cage_control.OpenMode = Me.m_cage_cls.OpenMode

      Me.m_initial_load = False

      ef_ticket.Enabled = m_new_collecion_mode
      Me.tb_notes.Enabled = m_new_collecion_mode
      btn_save_tickets.Enabled = m_new_collecion_mode

      'read the events produced during the collection. it is just for collection that are already collected
      If Not String.IsNullOrEmpty(_cage_control.m_type_new_collection.note_counter_events) Then
        Call XMLToEvents(_cage_control.m_type_new_collection.note_counter_events)
        If m_list_of_event.Count > 0 Then
          Call ShowNoteCounterEvents()
        End If
      End If
      'm_list_of_event = New List(Of String)
      Call SafeDispose(m_frm_counter_event, True)
      m_frm_counter_event = New frm_note_counter_events(m_list_of_event)
      btn_open_events.Visible = (m_list_of_event.Count > 0)

      Call Me.SetupDataGrid(_cage_control.m_type_new_collection)
      Call Me.FirstUpdateTotalGrid()
      ef_num_of_tickets.Value = Me.m_cage_cls.m_type_new_collection.real_ticket_count
    End If

    Me.ef_cage_session.Width = 450
    Me.ef_cashier_name.Width = 450
    Me.ef_user_terminals_name.Width = 450
    Me.ef_custom.Width = 425
    Me.ef_gaming_table.Width = 450
    Me.ef_cashier_date.Width = 450


    ' Dispose
    Call SafeDispose(_cage_control)

  End Sub ' GUI_SetScreenData

  ' PURPOSE : Database overridable operations. 
  '           Define specific DB operation for this form.
  '
  '  PARAMS :
  '     - INPUT :
  '         - DbOperation
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)
    Dim _cage_class As CLASS_CAGE_CONTROL
    Dim _set_foot_signature As Boolean
    Dim _foot_signature As String

    If Me.DbStatus = ENUM_STATUS.STATUS_NOT_SUPPORTED Then
      '' Wrong DbVersion message alredy showed,not show error message again.
      Exit Sub
    End If

    Select Case DbOperation

      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_CAGE_CONTROL()
        _cage_class = DbEditedObject
        _cage_class.ShowWithoutCageID = Me.m_show_without_cage_id
        Call _cage_class.GetDenominationAcceptedByTerminals()

        If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT Then
          _cage_class.MovementID = 0
          _cage_class.m_type_new_collection.money_collection_id = Me.m_cage_cls.m_type_new_collection.money_collection_id
          _cage_class.CageSessionId = Me.m_cage_cls.CageSessionId
        ElseIf Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE Then
          _cage_class.CageSessionId = Me.m_cage_cls.CageSessionId
        ElseIf Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MASSIVE Then
          Me.m_cage_cls = New CLASS_CAGE_CONTROL()
          DbReadObject = New CLASS_CAGE_CONTROL()
          Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MASSIVE
          Me.m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal
          Call Me.m_cage_cls.GetDenominationAcceptedByTerminals()
          If Not String.IsNullOrEmpty(ef_stacker_id.TextValue) Then
            _cage_class.m_type_new_collection.stacker_id = CType(ef_stacker_id.TextValue, Int64)
          End If
          If Not String.IsNullOrEmpty(ef_floor_id.TextValue) Then
            _cage_class.m_type_new_collection.floor_id = ef_floor_id.TextValue
          End If
          _cage_class.OperationType = Cage.CageOperationType.FromTerminal
        Else
          _cage_class.MovementID = Me.m_cage_cls.MovementID
        End If

        _cage_class.OpenMode = Me.m_cage_cls.OpenMode
        _cage_class.m_new_collecion_mode = Me.m_new_collecion_mode
        _cage_class.m_type_new_collection.user_id = CurrentUser.Id
        _cage_class.m_type_new_collection.user_name = CurrentUser.Name

        If Me.m_is_tito_collection Then
          _cage_class.m_type_new_collection.insertion_date = New TYPE_DATE_TIME()
          _cage_class.m_type_new_collection.extraction_date = New TYPE_DATE_TIME()
          _cage_class.m_type_new_collection.collection_date = New TYPE_DATE_TIME()
          _cage_class.m_type_new_collection.collection_details = New List(Of NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS)()
          _cage_class.OperationType = Cage.CageOperationType.FromTerminal
        End If

        DbStatus = ENUM_STATUS.STATUS_OK
        m_new_cage = True

      Case ENUM_DB_OPERATION.DB_OPERATION_UPDATE
        If DbStatus = ENUM_STATUS.STATUS_OK Then 'UPDATE_REQUIRED
          Call MyBase.GUI_DB_Operation(DbOperation)
          If Me.m_cage_cls.OpenMode <> CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MASSIVE Then
            Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW
          End If
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_AUDIT_UPDATE, _
            ENUM_DB_OPERATION.DB_OPERATION_AUDIT_INSERT

        Dim _original_class As CLASS_CAGE_CONTROL
        Dim _edited_class As CLASS_CAGE_CONTROL
        'JCA Comentado pues en la auditoria no es necesario comentar esto.
        _original_class = DbReadObject
        _edited_class = DbEditedObject

        _edited_class.WriteAudit(_original_class)

        If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MASSIVE Then

          ' DHA 13-JAN-2016: Restore grids with national currency as the frist time opened window
          If WSI.Common.Misc.IsFloorDualCurrencyEnabled() And Me.m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal Then

            RemoveHandler tb_currency.SelectedIndexChanged, AddressOf tb_currency_SelectedIndexChanged
            ' Clear values
            Me.dg_total.Clear()
            Me.tb_currency.TabPages.Clear()
            ' Set national currency and tickets TITO
            Call Me.AddTab(CurrencyExchange.GetNationalCurrency(), WSI.Common.CurrencyExchangeType.CURRENCY, "")
            Me.tb_currency.TabPages.Add(tab_ticket)
            AddHandler tb_currency.SelectedIndexChanged, AddressOf tb_currency_SelectedIndexChanged

            Me.tb_currency.SelectedIndex = 0
          End If

          Me.ResetMoneyCollectionData()

          ' 26-FEB-2016 JPJ Fixed Bug 10036: Note counter events are removed while in validation screen 
          If m_scanner_initialized = True Then
            AddHandler m_scanner_manager.NoteCountReceivedEvent, AddressOf CatchMessageFromScanner
          End If
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT

        If DbStatus = ENUM_STATUS.STATUS_OK Then

          If GeneralParam.GetBoolean("Cage", "NewMovement.PrintVoucher") Then
            Call PrintCageVoucherNewOperation()
          End If

          'SGB 09-APR-2015: print report
          _set_foot_signature = GeneralParam.GetBoolean("Cage", "CashCageCount.Signatures.Enabled", False)

          If _set_foot_signature Then
            _foot_signature = GeneralParam.GetString("Cage", "CashCageCount.Signatures.Text", "")
          Else
            _foot_signature = ""
          End If

          If m_cash_cage_print_selected Then
            PrintCage(True, _foot_signature)
          ElseIf m_cash_cage_export_excel Then
            PrintCage(False, _foot_signature)
          End If

          If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE Then
            NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4354), ENUM_MB_TYPE.MB_TYPE_INFO)
          End If

        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If m_cage_cls.OperationType = Cage.CageOperationType.RequestOperation _
           AndAlso m_cage_cls.Status = Cage.CageStatus.Canceled Then
          If DbStatus = ENUM_STATUS.STATUS_OK Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4801), ENUM_MB_TYPE.MB_TYPE_INFO)
          Else
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4380), ENUM_MB_TYPE.MB_TYPE_WARNING)
            DbEditedObject = DbReadObject
            Me.Close()
          End If
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If DbStatus <> ENUM_STATUS.STATUS_OK Then
          If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MASSIVE Then
            If Me.m_cage_cls.m_load_cancelled_by_user Then
              Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6241), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
            Else
              If Me.m_cage_cls.m_type_new_collection.stacker_id = 0 Then
                NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5201), ENUM_MB_TYPE.MB_TYPE_ERROR, , , IIf(String.IsNullOrEmpty(ef_stacker_id.TextValue), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5200), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2252)))
              Else
                Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
              End If
            End If
          Else
            Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
          End If
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_DELETE
        If m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MOVEMENT AndAlso _
          (m_cage_cls.OperationType = Cage.CageOperationType.FromCashier Or _
           m_cage_cls.OperationType = Cage.CageOperationType.FromCashierClosing Or _
           m_cage_cls.OperationType = Cage.CageOperationType.FromGamingTable Or _
           m_cage_cls.OperationType = Cage.CageOperationType.FromGamingTableClosing Or _
           m_cage_cls.OperationType = Cage.CageOperationType.FromGamingTableDropboxWithExpected) Then
          If m_cage_cls.WithdrawalCancelation() = ENUM_STATUS.STATUS_ERROR Then
            NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5884), ENUM_MB_TYPE.MB_TYPE_ERROR, , , , , )
          End If
        End If
      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

        If DbStatus = ENUM_STATUS.STATUS_STOCK_ERROR Then
          'ETP: Fixed Bug 10224: check and load stock data: 

          If CheckAndUpdateStockData() Then 'If True means BD error in function UpdateCageStock.
            NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4355), ENUM_MB_TYPE.MB_TYPE_WARNING)
          End If

        End If

    End Select
  End Sub ' GUI_DB_Operation

  ' PURPOSE: It gets the values from the entry field and set them to a CAGE_CONTROL object
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_GetScreenData()
    Dim _cage_movement As CLASS_CAGE_CONTROL
    Dim _has_integrated_cashier As Boolean
    Dim _currency_iso_code As String
    Dim _idx_row As Integer

    _cage_movement = DbEditedObject

    _cage_movement.TemplateID = m_template_id
    _cage_movement.TemplateName = cmb_cage_template.TextValue

    If Not String.IsNullOrEmpty(Me.cmb_cage_sessions.TextValue) Then ' Cage Session Id
      _cage_movement.CageSessionId = Me.cmb_cage_sessions.Value
    End If

    _cage_movement.TableCurrencies = Me.m_cage_cls.TableCurrencies.Copy()
    _cage_movement.OperationType = Me.m_cage_cls.OperationType
    _cage_movement.TerminalCashierID = Me.m_cage_cls.TerminalCashierID

    If String.IsNullOrEmpty(Me.cmb_user.TextValue) Then ' To Terminal 
      If Me.m_cage_cls.Status <> Cage.CageStatus.OK Then
        _cage_movement.UserCashierID = Nothing
      End If

      If Not String.IsNullOrEmpty(Me.cmb_terminals.TextValue) Then ' To cashier terminal
        _cage_movement.TerminalCashierID = Me.cmb_terminals.Value
        _cage_movement.TargetType = Cage.PendingMovementType.ToCashierTerminal
      ElseIf Not String.IsNullOrEmpty(Me.cmb_gaming_table.TextValue) Then ' To Gaming table
        Me.m_cage_cls.HasIntegratedCashier(Me.cmb_gaming_table.Value, _has_integrated_cashier) ' Gambling table with/without integrated cashier
        _cage_movement.GamingTableId = Me.cmb_gaming_table.Value
        _cage_movement.TerminalCashierID = Me.m_cage_cls.GetIntegratedCashierGamingTableId(Me.cmb_gaming_table.Value) ' Integrated Cashier ID
        _cage_movement.TargetType = Cage.PendingMovementType.ToGamingTable
      End If
    Else ' To Cashier User
      If Me.cmb_user.Visible Then
        _cage_movement.UserCashierID = Me.cmb_user.Value
        _cage_movement.TargetType = Cage.PendingMovementType.ToCashierUser
      End If
    End If

    If Not String.IsNullOrEmpty(Me.cmb_custom.TextValue) Then
      _cage_movement.CustomUserId = Me.cmb_custom.Value
    End If

    If m_is_ok Then 'Pendent, moure a la clase (insert/update)
      'inform status only when OK button is pressed
      Select Case Me.m_cage_cls.OpenMode
        Case CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT, _
          CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT_FROM_GAMINGTABLES
          _cage_movement.Status = WSI.Common.Cage.CageStatus.Canceled
          If Me.m_cage_cls.OperationType = Cage.CageOperationType.FromCashier Or _
             Me.m_cage_cls.OperationType = Cage.CageOperationType.FromCashierClosing Or _
             Me.m_cage_cls.OperationType = Cage.CageOperationType.FromGamingTable Or _
             Me.m_cage_cls.OperationType = Cage.CageOperationType.FromGamingTableClosing Then
            _cage_movement.UserCashierID = -1
          End If

        Case CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT _
           , CLASS_CAGE_CONTROL.OPEN_MODE.CLOSE_VIRTUAL

          _cage_movement.UserCageID = CurrentUser.Id

          If Me.rb_reception.Checked Then ' Reception
            If Me.rb_gaming_table.Checked Then ' From gaming table
              If cb_close_gaming_table.Checked Then
                _cage_movement.OperationType = Cage.CageOperationType.FromGamingTableClosing
              Else
                _cage_movement.OperationType = Cage.CageOperationType.FromGamingTable
              End If
              ' 26-05-2015 DHA corrected when gaming table player tracking is not enabled
              If Cage.IsCageEnabled AndAlso Cage.IsCageAutoMode() OrElse Not GamingTableBusinessLogic.IsEnabledGTPlayerTracking() Then
                _cage_movement.Status = Cage.CageStatus.OK
              Else
                _cage_movement.Status = Cage.CageStatus.Sent
              End If

              If Me.cb_close_gaming_table.Checked Then ' Virtual cashier reception and close session
                _cage_movement.ReceptionAndClose = True
                _cage_movement.GamingTableVisits = IIf(Me.ef_gaming_table_visits.Value = "", 0, Me.ef_gaming_table_visits.Value)
              Else ' Just Virtual cashier reception
                _cage_movement.ReceptionAndClose = False
              End If

            ElseIf Me.rb_custom.Checked Then ' Reception from custom
              _cage_movement.OperationType = Cage.CageOperationType.FromCustom
              _cage_movement.Status = Cage.CageStatus.ReceptionFromCustom

              ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ElseIf Me.rb_terminal.Checked Then ' Tickets Collection
              _cage_movement.OperationType = Cage.CageOperationType.FromTerminal
              '_cage_movement.Status = Cage.CageStatus.OK

              _currency_iso_code = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")

              ' DHA 13-JAN-2016: get terminal currency
              If WSI.Common.Misc.IsFloorDualCurrencyEnabled() Then
                _currency_iso_code = Me.m_cage_cls.TerminalIsoCode
              End If

              ' Search the currency iso code row
              For _idx_row = 0 To Me.dg_total.NumRows - 1
                If Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.CURRENCY).Value = _currency_iso_code Then
                  Exit For
                End If
              Next

              If Not Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.TOTAL).Value = 0 Then
                Try
                  _cage_movement.m_type_new_collection.real_bills_sum = Me.m_cage_cls.TableCurrencies.Compute("Sum(TOTAL)", "ISO_CODE = '" & _currency_iso_code & "' AND DENOMINATION > 0")
                Catch ex As Exception
                  _cage_movement.m_type_new_collection.real_bills_sum = Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.TOTAL).Value
                End Try
                If Me.m_cage_cls.TableCurrencies.Select("QUANTITY > 0 AND DENOMINATION > 0").Length > 0 Then
                  _cage_movement.m_type_new_collection.real_bills_count = Me.m_cage_cls.TableCurrencies.Compute("Sum(QUANTITY)", "DENOMINATION > 0")
                End If
              Else
                _cage_movement.m_type_new_collection.real_bills_count = 0
                _cage_movement.m_type_new_collection.real_bills_sum = 0.0
              End If

              _cage_movement.m_type_new_collection.real_ticket_sum = Me.m_cage_cls.m_type_new_collection.real_ticket_sum
              _cage_movement.m_type_new_collection.real_ticket_count = Me.m_cage_cls.m_type_new_collection.real_ticket_count

              _cage_movement.m_type_new_collection.notes = tb_notes.Text

              _cage_movement.m_type_new_collection.balanced = m_cage_cls.m_type_new_collection.balanced 'Set in CheckIfTicketsOk()

              If Me.m_cage_cls.m_type_new_collection.stacker_id > 0 Then
                _cage_movement.m_type_new_collection.stacker_id = Me.m_cage_cls.m_type_new_collection.stacker_id
              End If

              _cage_movement.m_type_new_collection.collection_date = New TYPE_DATE_TIME
              _cage_movement.m_type_new_collection.collection_date.Value = WGDB.Now
              _cage_movement.m_type_new_collection.collection_details = m_cage_cls.m_type_new_collection.collection_details 'Filled in CheckIfTicketsOk()
            End If
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

          Else ' Send
            If Me.rb_custom.Checked Then '  Sent to custom
              _cage_movement.OperationType = Cage.CageOperationType.ToCustom
              _cage_movement.Status = Cage.CageStatus.SentToCustom
            ElseIf Me.rb_gaming_table.Checked Then ' To gaming table
              _cage_movement.OperationType = Cage.CageOperationType.ToGamingTable
              ' 26-05-2015 DHA corrected when gaming table player tracking is not enabled 
              If Cage.IsCageEnabled AndAlso Not Cage.IsCageAutoMode() AndAlso GamingTableBusinessLogic.GamingTablesMode() = GamingTableBusinessLogic.GT_MODE.GUI_AND_CASHIER Or _has_integrated_cashier Then
                _cage_movement.Status = Cage.CageStatus.Sent
              Else
                _cage_movement.Status = Cage.CageStatus.OK
              End If
            Else
              _cage_movement.OperationType = Cage.CageOperationType.ToCashier
              _cage_movement.Status = Cage.CageStatus.Sent
            End If
          End If

        Case CLASS_CAGE_CONTROL.OPEN_MODE.VIEW_MOVEMENT _
           , CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MOVEMENT _
           , CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MASSIVE

          _currency_iso_code = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")

          ' DHA 13-JAN-2016: set currency for terminal
          If WSI.Common.Misc.IsFloorDualCurrencyEnabled() And Me.m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal Then
            _currency_iso_code = Me.m_cage_cls.TerminalIsoCode
          End If

          ' Search the currency iso code row
          For _idx_row = 0 To Me.dg_total.NumRows - 1
            If Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.CURRENCY).Value = _currency_iso_code Then
              Exit For
            End If
          Next

          _cage_movement.Status = Me.m_cage_cls.Status
          _cage_movement.UserCageID = CurrentUser.Id
          If m_is_tito_collection Then
            _cage_movement.OperationType = m_cage_cls.OperationType

            _cage_movement.m_type_new_collection.real_ticket_sum = Me.m_cage_cls.m_type_new_collection.real_ticket_sum
            _cage_movement.m_type_new_collection.real_ticket_count = Me.m_cage_cls.m_type_new_collection.real_ticket_count

            If Not Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.TOTAL).Value = 0 Then
              Try

                If Not m_is_coin_collected_enabled Then
                  '_cage_movement.m_type_new_collection.real_bills_sum = Me.m_cage_cls.TableCurrencies.Compute("Sum(TOTAL)", "ISO_CODE = '" & _currency_iso_code & "' AND DENOMINATION > 0 AND TYPE <> 1 AND TOTAL IS NOT NULL")
                  ' IF not the same�? FJC
                  _cage_movement.m_type_new_collection.real_bills_sum = IIf(IsDBNull(Me.m_cage_cls.TableCurrencies.Compute("Sum(TOTAL)", "ISO_CODE = '" & _currency_iso_code & "' AND DENOMINATION > 0 AND TYPE <> 1")), 0, Me.m_cage_cls.TableCurrencies.Compute("Sum(TOTAL)", "ISO_CODE = '" & _currency_iso_code & "' AND DENOMINATION > 0 AND TYPE <> 1"))
                Else
                  _cage_movement.m_type_new_collection.real_bills_sum = IIf(IsDBNull(Me.m_cage_cls.TableCurrencies.Compute("Sum(TOTAL)", "ISO_CODE = '" & _currency_iso_code & "' AND DENOMINATION > 0 AND TYPE <> 1")), 0, Me.m_cage_cls.TableCurrencies.Compute("Sum(TOTAL)", "ISO_CODE = '" & _currency_iso_code & "' AND DENOMINATION > 0 AND TYPE <> 1"))
                  _cage_movement.m_type_new_collection.real_coins_collection_amount = IIf(IsDBNull(Me.m_cage_cls.TableCurrencies.Compute("Sum(TOTAL)", "ISO_CODE = '" & _currency_iso_code & "' AND DENOMINATION > 0 AND TYPE = 1")), 0, Me.m_cage_cls.TableCurrencies.Compute("Sum(TOTAL)", "ISO_CODE = '" & _currency_iso_code & "' AND DENOMINATION > 0 AND TYPE = 1"))
                End If
              Catch ex As Exception
                _cage_movement.m_type_new_collection.real_bills_sum = Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.TOTAL).Value
                _cage_movement.m_type_new_collection.real_coins_collection_amount = 0
              End Try
              If Me.m_cage_cls.TableCurrencies.Select("ISO_CODE = '" & _currency_iso_code & "' AND QUANTITY > 0 AND DENOMINATION > 0 AND NOT TYPE IN (1, 1001, 1002, 1003)").Length > 0 Then
                _cage_movement.m_type_new_collection.real_bills_count = Me.m_cage_cls.TableCurrencies.Compute("Sum(QUANTITY)", "ISO_CODE = '" & _currency_iso_code & "' AND DENOMINATION > 0  AND NOT TYPE IN (1, 1001, 1002, 1003)")
              End If

            Else
              _cage_movement.m_type_new_collection.real_bills_count = 0
              _cage_movement.m_type_new_collection.real_bills_sum = 0.0
              _cage_movement.m_type_new_collection.real_coins_collection_amount = 0.0
            End If

            _cage_movement.m_type_new_collection.balanced = m_cage_cls.m_type_new_collection.balanced 'Set in CheckIfTicketsOk()

            _cage_movement.m_type_new_collection.notes = tb_notes.Text

            _cage_movement.m_type_new_collection.collection_date.Value = WGDB.Now
            _cage_movement.m_type_new_collection.collection_details = m_cage_cls.m_type_new_collection.collection_details 'Filled in CheckIfTicketsOk()

            If m_list_of_event IsNot Nothing AndAlso m_list_of_event.Count > 0 Then
              _cage_movement.m_type_new_collection.note_counter_events = EventsToXML() ' m_type_new_collection.note_counter_events ' xml with the events occurred during the reading
            End If

          End If

        Case CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE
          _cage_movement.OperationType = Cage.CageOperationType.CountCage
          _cage_movement.Status = Cage.CageStatus.FinishedCountCage
          _cage_movement.UserCageID = CurrentUser.Id

      End Select
    End If

    If Me.rb_cashier.Checked Then
      _cage_movement.SourceTargetId = CageMeters.CageSystemSourceTarget.Cashiers
    ElseIf Me.rb_gaming_table.Checked Then
      _cage_movement.SourceTargetId = CageMeters.CageSystemSourceTarget.GamingTables
    ElseIf Me.rb_terminal.Checked Then
      _cage_movement.SourceTargetId = CageMeters.CageSystemSourceTarget.Terminals
    Else
      _cage_movement.SourceTargetId = Me.cmb_custom.Value
    End If

    If Me.rb_send.Checked Then
      _cage_movement.MovementType = 1
    Else
      _cage_movement.MovementType = 0
    End If

    If Me.cmb_cage_template.SelectedIndex > 0 AndAlso Not m_changed_values Then
      _cage_movement.SendByTemplate = True
    End If

    _cage_movement.ConceptsData = Me.GetConceptsDataTableFromDG(Me.dg_concepts, False)

    m_cage_template = _cage_movement

  End Sub ' GUI_GetScreenData

  ' PURPOSE: Executed when a button is clicked
  '
  '  PARAMS:
  '     - INPUT:
  '           - ButtonId
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)
    Dim _save_as_template As Boolean

    Me.m_is_ok = False
    Me.m_is_cancel = False

    'if cage movemnt is collected, automode is active and it is tryed to cancel it must be passed to (Canceled)Pending Colletion
    ' Fixed Bug #XXX: Cant cancel termnal collection in automode: This ifo must be only for Cashier
    If (m_cage_cls.OperationType = Cage.CageOperationType.FromCashier Or _
        m_cage_cls.OperationType = Cage.CageOperationType.FromCashierClosing) AndAlso _
      Cage.IsCageAutoMode AndAlso ButtonId = ENUM_BUTTON.BUTTON_OK AndAlso Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT _
      Or _
      (m_cage_cls.OperationType = Cage.CageOperationType.FromGamingTable Or _
       m_cage_cls.OperationType = Cage.CageOperationType.FromGamingTableClosing Or _
       m_cage_cls.OperationType = Cage.CageOperationType.FromGamingTableDropboxWithExpected) AndAlso _
      Cage.IsCageAutoMode AndAlso ButtonId = ENUM_BUTTON.BUTTON_OK AndAlso Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT_FROM_GAMINGTABLES Then
      ButtonId = ENUM_BUTTON.BUTTON_DELETE

      'ATB 20-10-2016
      If rb_send.Checked Then
        m_cage_cls.MovementType = CLASS_CAGE_CONTROL.MOVEMENT_TYPE.SEND
      ElseIf rb_reception.Checked Then
        m_cage_cls.MovementType = CLASS_CAGE_CONTROL.MOVEMENT_TYPE.RECEIPT
      End If

      m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MOVEMENT
    End If

    Select Case ButtonId

      Case ENUM_BUTTON.BUTTON_OK

        If Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Visible Then
          _save_as_template = True
          If m_changed_values AndAlso rb_send.Checked AndAlso Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT _
             AndAlso Me.cmb_cage_template.TextValue <> "" Then
            ' Msg: Changes have been made to the template. Do you want to continue without saving?
            If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5057), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then

              _save_as_template = False
            Else
              m_template_id = 0
            End If
          End If

          'Save new template
          If _save_as_template Then
            Me.m_is_ok = True
            MyBase.GUI_ButtonClick(ButtonId)
          End If
        Else
          'If the button is not visible, it is as if we press on leaving
          ButtonId = ENUM_BUTTON.BUTTON_CANCEL
          MyBase.GUI_ButtonClick(ButtonId)
        End If

      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        ' Print/Reset stacker collection
        If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MASSIVE Then
          Call Me.ResetMoneyCollectionData()
        Else
          ButtonId = ENUM_BUTTON.BUTTON_PRINT
          MyBase.GUI_ButtonClick(ButtonId)
        End If
      Case ENUM_BUTTON.BUTTON_CUSTOM_1
        If m_print_enabled Then
          ' Excel
          ButtonId = ENUM_BUTTON.BUTTON_EXCEL
          MyBase.GUI_ButtonClick(ButtonId)
        Else
          Call Me.ClearMoneyCollectionData()
        End If

      Case ENUM_BUTTON.BUTTON_CUSTOM_2
        ' Creates a new template
        Call NewCageTemplate()

        'If is template maintenance, save an leave
        If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.TEMPLATE_MAINTENANCE Then
          ButtonId = ENUM_BUTTON.BUTTON_CANCEL
          MyBase.GUI_ButtonClick(ButtonId)
        End If

      Case ENUM_BUTTON.BUTTON_DELETE
        ' cancel a pending collection from cashier(withdrawal cancelation)
        If m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MOVEMENT AndAlso _
          (m_cage_cls.OperationType = Cage.CageOperationType.FromCashier Or _
           m_cage_cls.OperationType = Cage.CageOperationType.FromCashierClosing Or _
           m_cage_cls.OperationType = Cage.CageOperationType.FromGamingTable Or _
           m_cage_cls.OperationType = Cage.CageOperationType.FromGamingTableClosing Or _
           m_cage_cls.OperationType = Cage.CageOperationType.FromGamingTableDropboxWithExpected) Then
          Dim _cashier_is_open As Boolean
          Using _db_trx As New DB_TRX()
            _cashier_is_open = Me.m_cage_cls.IsCashierSessionOpen(_db_trx.SqlTransaction)
          End Using
          If _cashier_is_open Then
            If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5853), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO) _
                          = ENUM_MB_RESULT.MB_RESULT_YES Then
              Call MyBase.GUI_ButtonClick(ButtonId)
            Else
              Exit Sub
            End If
          Else
            Call MyBase.GUI_ButtonClick(ButtonId)
          End If


        ElseIf Not Me.m_cage_cls.CheckIfSameStatus(Me.m_cage_cls.MovementID, Cage.CageStatus.Sent) Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4856), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Else
          ' cancel request
          If String.IsNullOrEmpty(Me.cmb_cage_sessions.TextValue) AndAlso (String.IsNullOrEmpty(Me.ef_cage_session.TextValue) Or Me.ef_cage_session.TextValue = AUDIT_NONE_STRING) Then
            NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4554), ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.cmb_cage_sessions.Text) ' Select a cash cage session
            Me.cmb_cage_sessions.Focus()

            Exit Sub
          End If

          If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4800), _
                        ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_YES Then
            m_cage_cls.Status = Cage.CageStatus.Canceled
            MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_OK)
          End If
        End If

      Case ENUM_BUTTON.BUTTON_CANCEL
        Me.m_is_cancel = True
        MyBase.GUI_ButtonClick(ButtonId)

      Case Else
        MyBase.GUI_ButtonClick(ButtonId)

    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Init form in edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '       - CageMovementId
  '       - OpenMode
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overloads Sub ShowEditItem(ByVal CageMovementId As Int64 _
                                  , ByVal OpenMode As Int16 _
                                  , Optional ByVal CageSessionId As Integer = -1 _
                                  , Optional ByVal PrintEnabled As Boolean = False _
                                  , Optional ByVal GuiUserType As GU_USER_TYPE = GU_USER_TYPE.NOT_ASSIGNED)

    Dim _table_currencies As DataTable

    Me.m_initial_load = True
    Me.m_print_enabled = PrintEnabled

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    DbObjectId = CageMovementId
    DbStatus = ENUM_STATUS.STATUS_OK

    _table_currencies = Me.m_cage_cls.GetAllowedISOCode()
    Me.m_cage_cls.MovementID = CageMovementId

    If _table_currencies.Rows.Count = 0 Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3027), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Me.Close()
    Else
      Me.m_cage_cls.OpenMode = OpenMode
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)

      If OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_FROM_PROMOBOX_OR_ACCEPTOR OrElse _
         OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT_FROM_PROMOBOX_OR_ACCEPTOR Then
        Me.m_cage_cls = DbEditedObject
        Me.m_cage_cls.IsPromoBoxOrAcceptor = True
        Me.m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal

        If OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT_FROM_PROMOBOX_OR_ACCEPTOR Then
          OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT
        Else
          OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MOVEMENT
        End If

        Me.m_cage_cls.GuiUserType = GuiUserType
        'DbEditedObject = Me.m_cage_cls
      End If

      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)

      Me.m_cage_cls = Me.DbEditedObject
      Me.m_cage_cls.OpenMode = OpenMode

      If CageSessionId <> -1 Then
        Me.m_cage_cls.CageSessionId = CageSessionId
      ElseIf Me.m_cage_cls.OperationType = Cage.CageOperationType.RequestOperation Then
        Cage.GetCageSessionIdFromCashierSessionId(Me.m_cage_cls.CashierSessionId, Me.m_cage_cls.CageSessionId)
      End If

      If DbStatus = ENUM_STATUS.STATUS_OK Then
        Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
      End If

      If DbStatus <> ENUM_STATUS.STATUS_OK Then

        Return
      End If

      If Me.m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal Then
        Me.m_show_denominations = Cage.ShowDenominationsMode.ShowAllDenominations
      End If

      Me.m_collection_from_tito_terminal = (Me.m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal)

      Me.m_is_tito_collection = (m_cage_cls.m_type_new_collection.money_collection_id > 0 _
                                 AndAlso ((Me.m_cage_cls.OperationType = Cage.CageOperationType.FromCashier And Utils.IsTitoMode()) _
                                       OrElse (Me.m_cage_cls.OperationType = Cage.CageOperationType.FromCashierClosing And Utils.IsTitoMode()) _
                                       OrElse (Me.m_cage_cls.OperationType = Cage.CageOperationType.FromGamingTable And Utils.IsTitoMode()) _
                                       OrElse (Me.m_cage_cls.OperationType = Cage.CageOperationType.FromGamingTableClosing And Utils.IsTitoMode()) _
                                       OrElse (Me.m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal) _
                                       OrElse ((Me.m_cage_cls.OperationType = Cage.CageOperationType.FromGamingTableDropbox _
                                                Or Me.m_cage_cls.OperationType = Cage.CageOperationType.FromGamingTableDropboxWithExpected) _
                                               And Utils.IsTitoMode() AndAlso GeneralParam.GetBoolean("Cage", "GamingTables.TicketsCollection.Enabled", 0)) _
                                    ) _
                                 )

      Me.m_new_collecion_mode = Me.m_cage_cls.m_new_collecion_mode

      Call Me.Display(True)
    End If

    ' Dispose
    Call SafeDispose(_table_currencies)

  End Sub ' ShowEditItem

  ' PURPOSE: Init form in new mode
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overloads Sub ShowNewItem(Optional ByVal OpenMode As CLASS_CAGE_CONTROL.OPEN_MODE = CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT _
                                 , Optional ByVal CageSessionId As Integer = -1, Optional ByVal CashierSessionId As Integer = -1 _
                                 , Optional ByVal CashierId As Long = -1, Optional ByVal CashierName As String = "", Optional ByVal IsTemplateEditable As Boolean = True _
                                 , Optional ByRef ClosingStock As ClosingStocks = Nothing)
    Dim _table_currencies As DataTable

    Me.m_initial_load = True

    _table_currencies = Me.m_cage_cls.GetAllowedISOCode()
    Me.m_cage_cls.CageSessionId = CageSessionId
    Me.m_cage_cls.CashierSessionId = CashierSessionId
    Me.m_cage_cls.OpenMode = OpenMode

    If (OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT Or OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE) _
        And Me.m_cage_cls.CageSessionId = 0 Then

      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3378), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Me.Close()

      Return
    End If

    If _table_currencies.Rows.Count = 0 Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3027), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Me.Close()
    Else

      If OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE Then
        Me.m_cage_cls.OperationType = Cage.CageOperationType.CountCage
      ElseIf OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CLOSE_VIRTUAL Then
        If cb_close_gaming_table.Enabled AndAlso cb_close_gaming_table.Checked Then
          Me.m_cage_cls.OperationType = Cage.CageOperationType.FromGamingTableClosing
        Else
          Me.m_cage_cls.OperationType = Cage.CageOperationType.FromGamingTable
        End If
      ElseIf OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MASSIVE Then
        Me.m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal
        Me.m_is_tito_collection = True
        Me.m_collection_from_tito_terminal = True
      ElseIf OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.TEMPLATE_MAINTENANCE Then
        Me.m_cage_cls.OperationType = Cage.CageOperationType.ToGamingTable
      End If

      If Me.m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal Then
        Me.m_show_denominations = Cage.ShowDenominationsMode.ShowAllDenominations
      End If

      ' Sets the screen mode
      Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

      DbObjectId = Nothing
      DbStatus = ENUM_STATUS.STATUS_OK

      Call Me.SelectCageSessionOpened()

      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
      If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE _
        Or Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.TEMPLATE_MAINTENANCE Then
        Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
        Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
        Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
        Me.m_cage_cls = Me.DbEditedObject
        If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.TEMPLATE_MAINTENANCE Then
          Me.m_cage_cls.CashierId = CashierId
          Me.m_cage_cls.CashierName = CashierName
          Me.m_cage_cls.IsTemplateEditable = IsTemplateEditable
          Me.m_cage_cls.ClosingStocks = ClosingStock
        End If
      End If
      If DbStatus = ENUM_STATUS.STATUS_OK Then
        Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
      End If

      If DbStatus = ENUM_STATUS.STATUS_OK Then
        Call Me.Display(True)
      End If
    End If

    ' Dispose
    Call SafeDispose(_table_currencies)

  End Sub ' ShowNewItem

  ' PURPOSE: Keypress 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean
    Dim _datagrid As uc_grid


    Select Case e.KeyChar
      Case Chr(Keys.Enter)
        If Me.ActiveControl.Name.Equals("uc_grid") Then
          _datagrid = Me.ActiveControl
          _datagrid.KeyPressed(sender, e)

          Return True
        End If

        If Me.ActiveControl.Name = ef_ticket.Name Then
          Call btn_save_tickets_ClickEvent()

          Return True
        End If

        If Me.ActiveControl.Name = ef_stacker_id.Name Then
          Call LoadMoneyCollectionByStackerOrFloorId()
          m_ef_stacker_focused = True

          Return True
        End If

        If Me.ActiveControl.Name = ef_floor_id.Name Then
          Call LoadMoneyCollectionByStackerOrFloorId()
          m_ef_stacker_focused = False

          Return True
        End If

        If Me.tb_notes.ContainsFocus() Then

          Return True
        End If

        If Me.dg_concepts.ContainsFocus() Then
          _datagrid = Me.ActiveControl
          _datagrid.KeyPressed(sender, e)

          Return True
        End If

        Return False

      Case Chr(Keys.Escape)

      Case Else
        Return MyBase.GUI_KeyPress(sender, e)

    End Select
  End Function ' GUI_KeyPress

  Protected Overrides Sub GUI_ExcelResultList()
    Call PrintCage(False)
  End Sub ' GUI_ExcelResultList

  Protected Overrides Sub GUI_PrintResultList()
    Call PrintCage(True)
  End Sub ' GUI_PrintResultList

  Protected Overrides Sub GUI_Exit()
    RemoveHandler tb_currency.SelectedIndexChanged, AddressOf tb_currency_SelectedIndexChanged

    For Each _tab As TabPage In tb_currency.TabPages
      For Each _ctrl As Control In _tab.Controls
        _ctrl.Dispose()
      Next

      _tab.Dispose()
    Next

    Call SafeDispose(m_cage_cls, True)
    Call SafeDispose(m_frm_counter_event, True)
    Call SafeDispose(m_original_preferences, True)
    Call SafeDispose(m_list_of_event, True)
    Call SafeDispose(m_original_template, True)
    Call SafeDispose(m_list_of_theoretical_tickets, True)
    Call SafeDispose(m_cage_template, True)

    MyBase.GUI_Exit()
  End Sub

  Private Sub SafeDispose(ByRef Obj As Object, Optional ByVal TryDispose As Boolean = False)
    If TryDispose AndAlso Obj IsNot Nothing AndAlso TypeOf Obj Is IDisposable Then
      CType(Obj, IDisposable).Dispose()
    End If
    Obj = Nothing
    System.GC.Collect()
  End Sub

#End Region ' Overrides functions

#Region "Private functions"

  Private Function ShowExpectedData() As Boolean
    Dim _show As Boolean
    Dim _show_expected_data_read As Boolean
    Dim _show_expected_data_write As Boolean

    _show_expected_data_read = CurrentUser.Permissions(ENUM_FORM.FORM_CAGE_SHOW_EXPECTED_AMOUNTS).Read
    _show_expected_data_write = CurrentUser.Permissions(ENUM_FORM.FORM_CAGE_SHOW_EXPECTED_AMOUNTS).Write

    Select Case Me.m_cage_cls.Status

      'Read permission
      Case Cage.CageStatus.FinishedOpenCloseCage, _
      Cage.CageStatus.FinishedCountCage, _
        Cage.CageStatus.CanceledDueToImbalanceOrOther, _
        Cage.CageStatus.UserCancelToConfirmAmounts, _
        Cage.CageStatus.ErrorTotalImbalance, _
        Cage.CageStatus.OK, _
        Cage.CageStatus.OkDenominationImbalance, _
        Cage.CageStatus.OkAmountImbalance, _
        Cage.CageStatus.SentToCustom, _
        Cage.CageStatus.ReceptionFromCustom, _
        Cage.CageStatus.Canceled
        _show = _show_expected_data_read

        'Write permission
      Case Cage.CageStatus.InProgress, _
        Cage.CageStatus.Sent
        _show = _show_expected_data_write

      Case Cage.CageStatus.Any
        _show = _show_expected_data_read AndAlso _show_expected_data_write

      Case Else
        _show = False

    End Select

    Return _show
  End Function

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()

    Dim _ticket_collection_enabled As String
    Dim _nls_id As Integer

    _ticket_collection_enabled = IIf((m_cage_cls.OperationType = Cage.CageOperationType.FromCashier Or m_cage_cls.OperationType = Cage.CageOperationType.FromCashierClosing), _
                                    GeneralParam.GetBoolean("TITO", "Cashier.TicketsCollection"), _
                                    GeneralParam.GetBoolean("TITO", "TicketsCollection"))

    With Me.dg_total
      Call .Init(GRID_TOTAL_COLUMN_COUNT, GRID_HEADERS_COUNT)

      ' Index
      .Column(GRID_TOTAL_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_TOTAL_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_TOTAL_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_TOTAL_COLUMN_INDEX).HighLightWhenSelected = True
      .Column(GRID_TOTAL_COLUMN_INDEX).IsColumnPrintable = False

      .Column(GRID_TOTAL_COLUMN_CURRENCY_TYPE).Header(0).Text = "" ' Tipo
      .Column(GRID_TOTAL_COLUMN_CURRENCY_TYPE).Header(1).Text = ""
      .Column(GRID_TOTAL_COLUMN_CURRENCY_TYPE).Width = GRID_TOTAL_COLUMN_CURRENCY_TYPE_WIDTH
      .Column(GRID_TOTAL_COLUMN_CURRENCY_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_TOTAL_COLUMN_CURRENCY_TYPE).HighLightWhenSelected = False
      .Column(GRID_TOTAL_COLUMN_CURRENCY_TYPE).IsColumnPrintable = False

      .Column(GRID_TOTAL_COLUMN_CURRENCY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2076) ' Divisa
      .Column(GRID_TOTAL_COLUMN_CURRENCY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7191)
      .Column(GRID_TOTAL_COLUMN_CURRENCY).Width = GRID_TOTAL_COLUMN_CURRENCY_WIDTH
      .Column(GRID_TOTAL_COLUMN_CURRENCY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      '.Column(GRID_TOTAL_COLUMN_CURRENCY).IsMerged = True
      .Column(GRID_TOTAL_COLUMN_CURRENCY).HighLightWhenSelected = False

      .Column(GRID_TOTAL_COLUMN_CURRENCY_TYPE_DESCRIPTION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2076) ' Tipo
      .Column(GRID_TOTAL_COLUMN_CURRENCY_TYPE_DESCRIPTION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7192)
      .Column(GRID_TOTAL_COLUMN_CURRENCY_TYPE_DESCRIPTION).Width = GRID_TOTAL_COLUMN_CURRENCY_TYPE_DESCRIPTION_WIDTH
      .Column(GRID_TOTAL_COLUMN_CURRENCY_TYPE_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      '.Column(GRID_TOTAL_COLUMN_CURRENCY_TYPE_DESCRIPTION).IsMerged = True
      .Column(GRID_TOTAL_COLUMN_CURRENCY_TYPE_DESCRIPTION).HighLightWhenSelected = False

      .Column(GRID_TOTAL_COLUMN_BILLS_WITH_SIMBOL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(628) ' Importe
      .Column(GRID_TOTAL_COLUMN_BILLS_WITH_SIMBOL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5679) ' Efectivo
      .Column(GRID_TOTAL_COLUMN_BILLS_WITH_SIMBOL).Width = GRID_TOTAL_COLUMN_BILLS_WITH_SIMBOL_WIDTH
      .Column(GRID_TOTAL_COLUMN_BILLS_WITH_SIMBOL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_TOTAL_COLUMN_BILLS_WITH_SIMBOL).HighLightWhenSelected = False
      .Column(GRID_TOTAL_COLUMN_BILLS_WITH_SIMBOL).SystemType = Type.GetType("System.Decimal")

      .Column(GRID_TOTAL_COLUMN_COINS_WITH_SIMBOL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(628) ' Importe
      .Column(GRID_TOTAL_COLUMN_COINS_WITH_SIMBOL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2346) ' Otros
      .Column(GRID_TOTAL_COLUMN_COINS_WITH_SIMBOL).Width = GRID_TOTAL_COLUMN_COINS_WITH_SIMBOL_WIDTH
      .Column(GRID_TOTAL_COLUMN_COINS_WITH_SIMBOL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_TOTAL_COLUMN_COINS_WITH_SIMBOL).HighLightWhenSelected = False
      .Column(GRID_TOTAL_COLUMN_COINS_WITH_SIMBOL).SystemType = Type.GetType("System.Decimal")

      .Column(GRID_TOTAL_COLUMN_TOTAL_WITH_SIMBOL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2936) ' Total

      If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE Or Me.m_is_tito_collection _
        Or Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.VIEW_MOVEMENT _
        Or Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT_FROM_GAMINGTABLES Then
        _nls_id = 5011 ' Real
      Else
        _nls_id = 2876 ' B�veda        
      End If

      .Column(GRID_TOTAL_COLUMN_TOTAL_WITH_SIMBOL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_id)
      .Column(GRID_TOTAL_COLUMN_TOTAL_WITH_SIMBOL).Width = GRID_TOTAL_COLUMN_TOTAL_WITH_SIMBOL_WIDTH
      .Column(GRID_TOTAL_COLUMN_TOTAL_WITH_SIMBOL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_TOTAL_COLUMN_TOTAL_WITH_SIMBOL).HighLightWhenSelected = False
      .Column(GRID_TOTAL_COLUMN_TOTAL_WITH_SIMBOL).SystemType = Type.GetType("System.Decimal")

      .Column(GRID_TOTAL_COLUMN_TOTAL_CASHIER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2936) ' Total
      .Column(GRID_TOTAL_COLUMN_TOTAL_CASHIER).Header(1).Text = "xCash"
      .Column(GRID_TOTAL_COLUMN_TOTAL_CASHIER).Width = GRID_TOTAL_COLUMN_TOTAL_CASHIER_WIDTH
      .Column(GRID_TOTAL_COLUMN_TOTAL_CASHIER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_TOTAL_COLUMN_TOTAL_CASHIER).HighLightWhenSelected = False
      .Column(GRID_TOTAL_COLUMN_TOTAL_CASHIER).SystemType = Type.GetType("System.Decimal")

      .Column(GRID_TOTAL_COLUMN_TOTAL_CASHIER_WITH_SIMBOL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2936) ' Total

      If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CLOSE_CAGE _
        Or Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE _
          Or Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.VIEW_MOVEMENT _
         Or Me.m_is_tito_collection _
         Or Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT_FROM_GAMINGTABLES Then

        _nls_id = 5010 ' Teorico
      ElseIf m_gaming_values Then
        _nls_id = 4406 ' Mesa de juego
      Else
        _nls_id = 1818 ' Cajero
      End If

      .Column(GRID_TOTAL_COLUMN_TOTAL_CASHIER_WITH_SIMBOL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_id)
      .Column(GRID_TOTAL_COLUMN_TOTAL_CASHIER_WITH_SIMBOL).Width = GRID_TOTAL_COLUMN_TOTAL_CASHIER_WITH_SIMBOL_WIDTH
      .Column(GRID_TOTAL_COLUMN_TOTAL_CASHIER_WITH_SIMBOL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_TOTAL_COLUMN_TOTAL_CASHIER_WITH_SIMBOL).HighLightWhenSelected = False
      .Column(GRID_TOTAL_COLUMN_TOTAL_CASHIER_WITH_SIMBOL).SystemType = Type.GetType("System.Decimal")

      .Column(GRID_TOTAL_COLUMN_DIFFERENCE_TOTAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2936) ' Total
      .Column(GRID_TOTAL_COLUMN_DIFFERENCE_TOTAL).Header(1).Text = "xDiff"
      .Column(GRID_TOTAL_COLUMN_DIFFERENCE_TOTAL).Width = GRID_TOTAL_COLUMN_DIFFERENCE_TOTAL_WIDTH
      .Column(GRID_TOTAL_COLUMN_DIFFERENCE_TOTAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_TOTAL_COLUMN_DIFFERENCE_TOTAL).HighLightWhenSelected = False
      .Column(GRID_TOTAL_COLUMN_DIFFERENCE_TOTAL).SystemType = Type.GetType("System.Decimal")

      .Column(GRID_TOTAL_COLUMN_DIFFERENCE_TOTAL_WITH_SIMBOL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2936) ' Total
      .Column(GRID_TOTAL_COLUMN_DIFFERENCE_TOTAL_WITH_SIMBOL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2610) ' Diferencia
      .Column(GRID_TOTAL_COLUMN_DIFFERENCE_TOTAL_WITH_SIMBOL).Width = GRID_TOTAL_COLUMN_DIFFERENCE_TOTAL_WITH_SIMBOL_WIDTH
      .Column(GRID_TOTAL_COLUMN_DIFFERENCE_TOTAL_WITH_SIMBOL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_TOTAL_COLUMN_DIFFERENCE_TOTAL_WITH_SIMBOL).HighLightWhenSelected = False
      .Column(GRID_TOTAL_COLUMN_DIFFERENCE_TOTAL_WITH_SIMBOL).SystemType = Type.GetType("System.Decimal")

      .Column(GRID_TOTAL_COLUMN_BILLS).Header(0).Text = "xBill"
      .Column(GRID_TOTAL_COLUMN_BILLS).Width = GRID_TOTAL_COLUMN_BILLS_WIDTH
      .Column(GRID_TOTAL_COLUMN_BILLS).HighLightWhenSelected = False
      .Column(GRID_TOTAL_COLUMN_BILLS).SystemType = Type.GetType("System.Decimal")

      .Column(GRID_TOTAL_COLUMN_COINS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(628) ' Importe
      .Column(GRID_TOTAL_COLUMN_COINS).Width = GRID_TOTAL_COLUMN_COINS_WIDTH
      .Column(GRID_TOTAL_COLUMN_COINS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_TOTAL_COLUMN_COINS).HighLightWhenSelected = False
      .Column(GRID_TOTAL_COLUMN_COINS).SystemType = Type.GetType("System.Decimal")

      If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CLOSE_CAGE _
        Or Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE _
          Or Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.VIEW_MOVEMENT _
         Or Me.m_is_tito_collection _
         Or Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT_FROM_GAMINGTABLES Then

        _nls_id = 5010 ' Teorico
      ElseIf m_gaming_values Then
        _nls_id = 4406 ' Mesa de juego
      ElseIf Me.m_cage_cls.OperationType = Cage.CageOperationType.RequestOperation Then
        _nls_id = 4784 ' Solicitado
      Else
        _nls_id = 1818 ' Cajero        
      End If

      .Column(GRID_TOTAL_COLUMN_TOTAL_CASHIER_WITH_SIMBOL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_id) ' Cajero

      If (Me.m_cage_cls.Status = Cage.CageStatus.FinishedOpenCloseCage _
        And Me.m_cage_cls.OperationType = Cage.CageOperationType.CloseCage) Then

        .Column(GRID_TOTAL_COLUMN_BILLS_WITH_SIMBOL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5010) ' Teorico
        .Column(GRID_TOTAL_COLUMN_COINS_WITH_SIMBOL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5010) ' Teorico
        .Column(GRID_TOTAL_COLUMN_COINS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5010) ' Teorico
        .Column(GRID_TOTAL_COLUMN_TOTAL_WITH_SIMBOL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5010) ' Teorico
        .Column(GRID_TOTAL_COLUMN_TOTAL_CASHIER_WITH_SIMBOL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5011) ' Real
      End If

      .Column(GRID_TOTAL_COLUMN_TOTAL).Header(0).Text = "xTot"
      .Column(GRID_TOTAL_COLUMN_TOTAL).Width = GRID_TOTAL_COLUMN_TOTAL_WIDTH
      .Column(GRID_TOTAL_COLUMN_TOTAL).HighLightWhenSelected = False
    End With

    With Me.dg_concepts

      'AddHandler Me.dg_concepts.CellDataChangedEvent, AddressOf CellDataChangeHandler
      AddHandler .BeforeStartEditionEvent, AddressOf BeforeStartConceptEditionHandler

      Dim _dg_padding As Windows.Forms.Padding
      _dg_padding.All = 3

      .PanelRightVisible = False
      .Padding = _dg_padding
      .Margin = _dg_padding
      .Sortable = False

      Call .Init(GRID_CONCEPT_AMOUNT + m_currencies_types.Rows.Count, GRID_HEADERS_COUNT, True)

      ' Index
      .Column(GRID_CONCEPT_ID).Header(0).Text = " "
      .Column(GRID_CONCEPT_ID).Header(1).Text = " "
      .Column(GRID_CONCEPT_ID).Width = 0
      .Column(GRID_CONCEPT_ID).HighLightWhenSelected = True
      .Column(GRID_CONCEPT_ID).IsColumnPrintable = False

      ' IsNationalCurrency for edition
      .Column(GRID_CONCEPT_ONLY_NATIONAL_CURRENCY).Header(0).Text = " "
      .Column(GRID_CONCEPT_ONLY_NATIONAL_CURRENCY).Header(1).Text = " "
      .Column(GRID_CONCEPT_ONLY_NATIONAL_CURRENCY).Width = 0
      .Column(GRID_CONCEPT_ONLY_NATIONAL_CURRENCY).HighLightWhenSelected = True
      .Column(GRID_CONCEPT_ONLY_NATIONAL_CURRENCY).IsColumnPrintable = False

      ' Source target id
      .Column(GRID_CONCEPT_SOURCE_TARGET_ID).Header(0).Text = " "
      .Column(GRID_CONCEPT_SOURCE_TARGET_ID).Header(1).Text = " "
      .Column(GRID_CONCEPT_SOURCE_TARGET_ID).Width = 0
      .Column(GRID_CONCEPT_SOURCE_TARGET_ID).HighLightWhenSelected = True
      .Column(GRID_CONCEPT_SOURCE_TARGET_ID).IsColumnPrintable = False

      ' Concept Description
      .Column(GRID_CONCEPT_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5472) 'Conceptos
      .Column(GRID_CONCEPT_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5472) 'Conceptos
      .Column(GRID_CONCEPT_NAME).Width = 1780
      .Column(GRID_CONCEPT_NAME).IsMerged = True
      .Column(GRID_CONCEPT_NAME).HighLightWhenSelected = True
      .Column(GRID_CONCEPT_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Currencies
      Dim _col_index As Int32
      _col_index = GRID_CONCEPT_AMOUNT
      For Each _row As DataRow In m_currencies_types.Rows

        .Column(_col_index).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2078) ' Divisas
        .Column(_col_index).Header(1).Text = _row("NAME").ToString()
        .Column(_col_index).Width = 2000
        .Column(_col_index).HighLightWhenSelected = False
        .Column(_col_index).Editable = True
        .Column(_col_index).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
        .Column(_col_index).EditionControl.EntryField.SetFilter(FORMAT_MONEY, 11, 2, _row("CGC_ISO_CODE"))
        .Column(_col_index).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
        .Column(_col_index).SystemType = Type.GetType("System.Decimal")

        _col_index = _col_index + 1

      Next

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: To set the data grid value
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row
  '           - Column
  '           - Dg
  '           - Value
  '           - IsAmount
  '           - IsDenomination
  '           - IsoCode
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDataGridValue(ByVal Row As Integer _
                                  , ByVal Column As Integer _
                                  , ByRef Dg As uc_grid _
                                  , ByVal Value As Object _
                                  , ByVal IsAmount As Boolean _
                                  , ByVal IsDenomination As Boolean _
                                  , Optional ByVal IsoCode As String = "")

    Dim _value As Object

    _value = Me.m_cage_cls.GetRowValue(Value, True)

    If Not _value Is Nothing Then
      If IsAmount Then
        Dg.Cell(Row, Column).Value = Me.m_cage_cls.FormatAmount(_value, IsDenomination, IsoCode)
      Else
        Dg.Cell(Row, Column).Value = GUI_FormatNumber(_value, 0)
      End If
    Else
      Dg.Cell(Row, Column).Value = Nothing
    End If

  End Sub ' SetDataGridValue

  ' PURPOSE: Initializes Total Grid 
  '
  '  PARAMS:
  '     - INPUT:
  '           - TableCur
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub InitTotalGrid(ByVal TableCur As DataTable)
    Dim _row As DataRow
    Dim _iso_code As String
    Dim _currency_type As CageCurrencyType
    Dim _iso_name As String

    _row = Nothing
    _iso_code = String.Empty
    _iso_name = String.Empty

    ' DHA 13-JAN-2016: set data by terminal currency
    If WSI.Common.Misc.IsFloorDualCurrencyEnabled() And Me.m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal And Not String.IsNullOrEmpty(Me.m_cage_cls.TerminalIsoCode) Then
      Call Me.SetupRowInitTotalGrid(Me.m_cage_cls.TerminalIsoCode, CurrencyExchangeType.CURRENCY)
    Else
      For Each _row In TableCur.Rows
        _iso_code = _row.Item(1)
        _currency_type = _row.Item(2)
        _iso_name = _row.Item(3)
        If Me.m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal Then
          If _row.Item(1).Equals(Me.m_national_currency) AndAlso (Not FeatureChips.IsCageCurrencyTypeChips(CType(_row.Item("TYPE"), CageCurrencyType))) Then
            Call Me.SetupRowInitTotalGrid(_iso_code, _currency_type, _iso_name, False)
          End If
        Else
          Call Me.SetupRowInitTotalGrid(_iso_code, _currency_type, _iso_name, False)
        End If

      Next
    End If

    ' Tickets tab
    If Me.m_is_tito_collection And TITO.Utils.IsTitoMode() Then
      _iso_code = CurrencyExchange.GetNationalCurrency()
      _currency_type = CageCurrencyType.Others
      _iso_name = TICKETS_ISOCODE_NLS
      Call Me.SetupRowInitTotalGrid(_iso_code, _currency_type, _iso_name, True)
    End If

    Call Me.SelectRowDgTotal()
  End Sub ' InitTotalGrid

  ' PURPOSE: Sets a new row for Total Grid 
  '
  '  PARAMS:
  '     - INPUT:
  '           - IsoCode
  '           - IsTickets
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetupRowInitTotalGrid(ByVal IsoCode As String, ByVal IsoType As Int32, Optional ByVal IsTickets As Boolean = False)
    SetupRowInitTotalGrid(IsoCode, 0, "", IsTickets)
  End Sub
  Private Sub SetupRowInitTotalGrid(ByVal IsoCode As String, ByVal IsoType As Int32, ByVal Name As String, ByVal IsTickets As Boolean)
    Dim _idx_row As Integer

    Me.dg_total.AddRow()
    _idx_row = Me.dg_total.NumRows - 1

    Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.CURRENCY).Value = IsoCode
    Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.CURRENCY_TYPE).Value = IsoType
    Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.CURRENCY_TYPE_DESCRIPTION).Value = Name

    Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.BILLS).Value = 0
    Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.COINS).Value = 0
    Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.TOTAL).Value = 0
    Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.CASHIER_TOTAL).Value = 0
    Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.DIFFERENCE_TOTAL).Value = 0

    If IsoType = FeatureChips.ChipType.COLOR Then
      Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.CURRENCY).ForeColor = Color.White
      Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.BILLS_WITH_SIMBOL).Value = FormatNumber(0)
      Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.COINS_WITH_SIMBOL).Value = FormatNumber(0)
      Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.TOTAL_WITH_SIMBOL).Value = FormatNumber(0)
      Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.CASHIER_TOTAL_WITH_SIMBOL).Value = FormatNumber(0)
      Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.DIFFERENCE_TOTAL_WITH_SIMBOL).Value = FormatNumber(0)
    Else
      Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.BILLS_WITH_SIMBOL).Value = Me.m_cage_cls.FormatAmount(0, False, IsoCode)
      Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.COINS_WITH_SIMBOL).Value = Me.m_cage_cls.FormatAmount(0, False, IsoCode)
      Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.TOTAL_WITH_SIMBOL).Value = Me.m_cage_cls.FormatAmount(0, False, IsoCode)
      Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.CASHIER_TOTAL_WITH_SIMBOL).Value = Me.m_cage_cls.FormatAmount(0, False, IsoCode)
      Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.DIFFERENCE_TOTAL_WITH_SIMBOL).Value = Me.m_cage_cls.FormatAmount(0, False, IsoCode)
    End If

  End Sub ' SetupRowInitTotalGrid

  ' PURPOSE: Set values for total grid 
  '
  '  PARAMS:
  '     - INPUT:
  '           - NewValue
  '           - OldValue
  '           - IsCoin
  '           - IsoCode
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetTotalGrid(ByVal NewValue As Decimal, ByVal OldValue As Decimal, ByVal IsCoin As Boolean, ByVal IsoCode As String)
    Dim _idx_row As Integer
    Dim _diff As Decimal
    Dim _currency As String
    Dim _coins As Decimal
    Dim _bills As Decimal
    Dim _total As Decimal

    For _idx_row = 0 To Me.dg_total.NumRows - 1

      _currency = Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.CURRENCY).Value

      'If _currency = IsoCode Or (IsoCode = Cage.CHIPS_ISO_CODE AndAlso _currency = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2941)) Then
      If _currency = IsoCode Then

        If IsCoin Then
          Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.COINS).Value -= OldValue
          Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.COINS).Value += NewValue
        Else
          Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.BILLS).Value -= OldValue
          Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.BILLS).Value += NewValue
        End If

        _coins = CDbl(Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.COINS).Value)
        _bills = CDbl(Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.BILLS).Value)

        Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.BILLS_WITH_SIMBOL).Value = Me.m_cage_cls.FormatAmount(_bills, False, IsoCode)
        Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.COINS_WITH_SIMBOL).Value = Me.m_cage_cls.FormatAmount(_coins, False, IsoCode)

        _total = _bills + _coins
        Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.TOTAL).Value = _total
        Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.TOTAL_WITH_SIMBOL).Value = Me.m_cage_cls.FormatAmount(_total, False, IsoCode)

        _diff = _total - Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.CASHIER_TOTAL).Value
        Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.DIFFERENCE_TOTAL).Value = _diff

        If _diff > 0 Then
          Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.DIFFERENCE_TOTAL_WITH_SIMBOL).Value = "+" & Me.m_cage_cls.FormatAmount(_diff, False, IsoCode)
        Else
          Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.DIFFERENCE_TOTAL_WITH_SIMBOL).Value = Me.m_cage_cls.FormatAmount(_diff, False, IsoCode)
        End If

        Me.SetRowColor(_idx_row, Me.dg_total, True)
      End If
    Next

  End Sub ' SetTotalGrid

  ' PURPOSE: Set values for total grid 
  '
  '  PARAMS:
  '     - INPUT:
  '           - NewValue
  '           - OldValue
  '           - IsCoin
  '           - IsoCode
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub RefreshTotalGrid(ByVal IsCash As Boolean, ByVal IsoCode As String, ByVal IsoType As Int32)
    Dim _idx_row As Integer
    Dim _diff As Decimal
    Dim _currency As String
    Dim _currency_type As CageCurrencyType
    Dim _coins As Decimal
    Dim _bills As Decimal
    Dim _total As Decimal
    Dim _total_denomination As Decimal
    Dim _total_denomination_obj As Object
    Dim _isCountCage As Boolean
    Dim _type_condition As String
    Dim _curr_description As String
    Dim _denomination_condition As String

    Try

      _isCountCage = (Me.m_cage_cls.OperationType = Cage.CageOperationType.CountCage)

      For _idx_row = 0 To Me.dg_total.NumRows - 1

        _currency = Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.CURRENCY).Value
        _currency_type = Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.CURRENCY_TYPE).Value

        If _currency_type = CageCurrencyType.Bill And Not IsCash And m_show_denominations <> Cage.ShowDenominationsMode.HideAllDenominations Then
          _type_condition = " AND TYPE IN (" & CageCurrencyType.Bill & ", " & CageCurrencyType.Coin & ") "
        ElseIf _currency_type = CageCurrencyType.Bill And Not IsCash And m_show_denominations = Cage.ShowDenominationsMode.HideAllDenominations Then
          _type_condition = " AND TYPE IN (" & CageCurrencyType.Others & ") "
        ElseIf _currency_type = CageCurrencyType.Bill And IsCash Then
          _type_condition = " AND TYPE IN (" & CageCurrencyType.Others & ") "
        Else
          _type_condition = " AND TYPE = '" & IsoType & "' "
        End If

        If _currency = IsoCode And (_currency_type = IsoType Or (_currency_type = CageCurrencyType.Bill And (IsoType = CageCurrencyType.Coin Or IsoType = CageCurrencyType.Others))) Then
          _denomination_condition = ""
          _curr_description = Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.CURRENCY_TYPE_DESCRIPTION).Value

          If _curr_description.Equals(TICKETS_ISOCODE_NLS) Then
            _denomination_condition = " AND DENOMINATION IN (-200)"
          End If

          If _isCountCage Then
            _total_denomination_obj = Me.m_cage_cls.TableCurrencies.Compute("Sum(EXPECTED_COLUMN)", "ISO_CODE = '" & IsoCode & "' " & _type_condition & " " & _denomination_condition & " ")
          Else
            _total_denomination_obj = Me.m_cage_cls.TableCurrencies.Compute("Sum(TOTAL)", "ISO_CODE = '" & IsoCode & "' " & _type_condition & " " & _denomination_condition & " ")
          End If

          If (DBNull.Value.Equals(_total_denomination_obj)) Then
            _total_denomination = 0
          Else
            _total_denomination = Convert.ToDecimal(_total_denomination_obj)
          End If


          If IsCash Then
            Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.COINS).Value = _total_denomination
          Else
            Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.BILLS).Value = _total_denomination
          End If

          _coins = CDbl(Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.COINS).Value)
          _bills = CDbl(Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.BILLS).Value)

          If _currency_type = FeatureChips.ChipType.COLOR Then
            Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.BILLS_WITH_SIMBOL).Value = FormatNumber(_bills)
            Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.COINS_WITH_SIMBOL).Value = FormatNumber(_coins)
          Else
            Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.BILLS_WITH_SIMBOL).Value = Me.m_cage_cls.FormatAmount(_bills, False, IsoCode)
            Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.COINS_WITH_SIMBOL).Value = Me.m_cage_cls.FormatAmount(_coins, False, IsoCode)
          End If

          _total = _bills + _coins
          If m_show_expected_data Then
            _diff = _total - Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.CASHIER_TOTAL).Value()
          Else
            _diff = 0
          End If


          Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.TOTAL).Value = _total
          If _currency_type = FeatureChips.ChipType.COLOR Then
            Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.TOTAL_WITH_SIMBOL).Value = FormatNumber(_total)
          Else
            Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.TOTAL_WITH_SIMBOL).Value = Me.m_cage_cls.FormatAmount(_total, False, IsoCode)
          End If

          Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.DIFFERENCE_TOTAL).Value = _diff

          If _currency_type = FeatureChips.ChipType.COLOR Then
            If _diff > 0 Then
              Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.DIFFERENCE_TOTAL_WITH_SIMBOL).Value = "+" & FormatNumber(_diff)
            Else
              Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.DIFFERENCE_TOTAL_WITH_SIMBOL).Value = FormatNumber(_diff)
            End If

          Else
            If _diff > 0 Then
              Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.DIFFERENCE_TOTAL_WITH_SIMBOL).Value = "+" & Me.m_cage_cls.FormatAmount(_diff, False, IsoCode)
            Else
              Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.DIFFERENCE_TOTAL_WITH_SIMBOL).Value = Me.m_cage_cls.FormatAmount(_diff, False, IsoCode)
            End If

          End If


          Me.SetRowColor(_idx_row, Me.dg_total, True)
        End If
      Next

    Catch ex As Exception

    End Try

  End Sub ' SetTotalGrid

  ' PURPOSE: Disable all controls except currencies combo
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub EnableDisable()

    If m_cage_cls.OpenMode <> CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT Then
      Me.cmb_user.Enabled = False
      Me.Enabled = True
    Else
      Me.cmb_user.Enabled = True
    End If

  End Sub ' EnableDisable


  ' PURPOSE: Set text for buttons
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub SetButtonsText()
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Visible = False
    Select Case m_cage_cls.OpenMode
      Case CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT
        Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2903) ' Anular
        Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_AUDITOR.GetString(452) ' Salir

      Case CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MOVEMENT
        Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_AUDITOR.GetString(452) ' Salir

        If Me.m_cage_cls.Status = Cage.CageStatus.Sent And Me.m_cage_cls.OperationType = Cage.CageOperationType.RequestOperation Then
          Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Visible = True
          Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6) ' Cancelar (solicitud)
        End If

      Case CLASS_CAGE_CONTROL.OPEN_MODE.VIEW_MOVEMENT
        Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_AUDITOR.GetString(452) ' Salir

      Case CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT_FROM_GAMINGTABLES
        Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2903) ' Anular
        Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_AUDITOR.GetString(452) ' Salir

      Case Else
        If Me.m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal Then
          If Not Me.m_new_collecion_mode Then
            Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
            Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_AUDITOR.GetString(452) ' Salir
          End If
        End If

    End Select
  End Sub ' SetButtonsText

  ' PURPOSE: Update data from TOTAL grid at the begining
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub FirstUpdateTotalGrid(Optional ByVal IsFirstUpdate As Boolean = True)
    Dim _row As DataRow
    Dim _rows_selected As DataRow()
    Dim _idx_dg As Integer
    Dim _diff As Decimal
    Dim _iso_code As String
    Dim _currency_type As CageCurrencyType
    Dim _condition As String
    Dim _cage_total As Decimal
    Dim _cashier_total As Decimal
    Dim _take_care_in_total As Boolean
    Dim _isCountCage As Boolean
    Dim _amount As Decimal
    Dim _type_condition As String

    Dim _suggested_denominations As Boolean

    _condition = ""
    _rows_selected = Nothing
    _amount = 0

    _isCountCage = (Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE)
    _suggested_denominations = GeneralParam.GetBoolean("Cage", "Collection.SuggestDenominationAmounts", False)

    ' 09-NOV-2015 FOS & JPJ    Fixed Bug TFS-6228: Totals don't appear
    ' 07-JAN-2016 JPJ          Fixed Bug 8177 & 8172: Only the remainder is taken into account when collecting from a cashier. 
    If Not WSI.Common.Misc.IsMico2Mode() AndAlso
        ((_isCountCage AndAlso TITO.Utils.IsTitoMode()) OrElse
        ((Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MOVEMENT OrElse Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.VIEW_MOVEMENT _
          OrElse (Not m_show_expected_data)) AndAlso _
        (Me.m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal AndAlso TITO.Utils.IsTitoMode()))) _
        OrElse (Not _suggested_denominations AndAlso Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MASSIVE) Then
      FirstUpdateTotalGridCountCage(_isCountCage, IsFirstUpdate)
    Else
      For _idx_dg = 0 To Me.dg_total.NumRows - 1
        'Default values
        _iso_code = Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.CURRENCY).Value
        _currency_type = Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.CURRENCY_TYPE).Value


        If Me.m_cage_cls.IsCountR And _currency_type = CageCurrencyType.Others Then
          _type_condition = " AND TYPE = '" & _currency_type & "' AND DENOMINATION = -200"
        Else
          If _currency_type = CageCurrencyType.Bill Then
            _type_condition = " AND TYPE IN (" & CageCurrencyType.Bill & ", " & CageCurrencyType.Coin & ", " & CageCurrencyType.Others & ") "
          ElseIf _currency_type = CageCurrencyType.Others Then
            _type_condition = " AND TYPE = '" & _currency_type & "' AND DENOMINATION = -200"
          Else
            _type_condition = " AND TYPE = '" & _currency_type & "' "
          End If
        End If

        _condition = "ISO_CODE = '" & _iso_code & "' " & _type_condition & " AND (QUANTITY > 0 OR CASHIER_QUANTITY > 0)"

        'ETP 07/12/2016 Init total values.
        'FJC 28-DIC-2016 Bug 21984:CountR: no se recaudan los tickets pagados
        If (Not Me.m_cage_cls.IsCountR) Then
          Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.COINS).Value = 0
          Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.BILLS).Value = 0
        End If

        ' Chips values
        If FeatureChips.IsCurrencyExchangeTypeChips(_currency_type) Then
          If m_gaming_values Then
            _condition = "ISO_CODE = '" & _iso_code & "' " & _type_condition & " AND (TOTAL > 0 OR CASHIER_TOTAL <> 0 )"
          Else
            _condition = "ISO_CODE = '" & _iso_code & "' " & _type_condition & " AND (TOTAL > 0 OR CASHIER_TOTAL > 0)"
          End If
        End If

        _rows_selected = Me.m_cage_cls.TableCurrencies.Select(_condition)

        For Each _row In _rows_selected
          _take_care_in_total = True
          _amount = 0
          If (_isCountCage) Then
            _amount = _row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.EXPECTED_COLUMN)
          ElseIf Not (_row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.TOTAL) Is DBNull.Value) Then
            _amount = _row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.TOTAL)
          End If

          If FeatureChips.IsCurrencyExchangeTypeChips(_currency_type) Or _row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CURRENCY_TYPE) = CageCurrencyType.Others Then ' Chips
            If _row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.DENOMINATION) <> Cage.COINS_COLLECTION_CODE Then
              Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.COINS).Value += CDbl(_amount)

              If _currency_type = FeatureChips.ChipType.COLOR Then
                Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.COINS_WITH_SIMBOL).Value = Me.FormatNumber(Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.COINS).Value)
              Else
                Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.COINS_WITH_SIMBOL).Value = _
                                                        Me.m_cage_cls.FormatAmount(Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.COINS).Value, False, _iso_code)
              End If

            Else
              _take_care_in_total = False
            End If
          Else  ' Coins
            ' 25-SEP-2014 JPJ Fixed Bug WIGOSTITO 1284: Deleting all the elements of the grid shows an error
            If _row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.DENOMINATION) < 0 AndAlso _
                Not _row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.DENOMINATION) = Cage.COINS_COLLECTION_CODE AndAlso
              Not _row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.DENOMINATION) = Cage.TICKETS_CODE Then

              Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.COINS).Value += CDbl(_amount)

              Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.COINS_WITH_SIMBOL).Value = _
                                                      Me.m_cage_cls.FormatAmount(Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.COINS).Value, False, _iso_code)
            Else
              If Not _row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CURRENCY_TYPE) = CageCurrencyType.Coin OrElse Not (TITO.Utils.IsTitoMode) Then
                If _row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.DENOMINATION) <> Cage.COINS_COLLECTION_CODE Then
                  _amount = 0

                  If (_isCountCage) Then
                    _amount = Me.m_cage_cls.GetRowValue(_row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.EXPECTED_COLUMN)) 'Bills
                  ElseIf Not _row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.DENOMINATION) = Cage.TICKETS_CODE Then
                    _amount = Me.m_cage_cls.GetRowValue(_row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.TOTAL)) 'Coins
                  End If

                  Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.COINS).Value += 0
                  Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.BILLS).Value += CDbl(_amount)

                  Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.BILLS_WITH_SIMBOL).Value = _
                                                          Me.m_cage_cls.FormatAmount(Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.BILLS).Value, False, _iso_code)
                End If
              ElseIf _row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.DENOMINATION) <> Cage.COINS_COLLECTION_CODE Then
                If (_isCountCage) Then
                  _amount = Me.m_cage_cls.GetRowValue(_row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.EXPECTED_COLUMN)) 'Bills
                Else
                  _amount = Me.m_cage_cls.GetRowValue(_row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.TOTAL)) 'Coins
                End If

                Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.COINS).Value += 0
                Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.BILLS).Value += CDbl(_amount)

                Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.BILLS_WITH_SIMBOL).Value = _
                                                        Me.m_cage_cls.FormatAmount(Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.BILLS).Value, False, _iso_code)

              End If
            End If
          End If

          If _take_care_in_total Then
            If (_row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.DENOMINATION) = -200 And (Cage.IsCageAutoMode() Or Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT)) Then
              _cage_total = Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.TOTAL).Value
            Else
              _cage_total = CDbl(Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.BILLS).Value) _
                          + CDbl(Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.COINS).Value)
            End If

            Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.TOTAL).Value = _cage_total
            If _currency_type = FeatureChips.ChipType.COLOR Then
              Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.TOTAL_WITH_SIMBOL).Value = Me.FormatNumber(_cage_total)
            Else
              Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.TOTAL_WITH_SIMBOL).Value = Me.m_cage_cls.FormatAmount(_cage_total, False, _iso_code)
            End If

            Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.CASHIER_TOTAL).Value += _
                                                        CDbl(Me.m_cage_cls.GetRowValue(_row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CASHIER_TOTAL)))


            _cashier_total = Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.CASHIER_TOTAL).Value

            If _currency_type = FeatureChips.ChipType.COLOR Then
              Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.CASHIER_TOTAL_WITH_SIMBOL).Value = Me.FormatNumber(_cashier_total)
            Else
              Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.CASHIER_TOTAL_WITH_SIMBOL).Value = Me.m_cage_cls.FormatAmount(_cashier_total, False, _iso_code)
            End If

          End If

          If (Me.m_cage_cls.Status = Cage.CageStatus.FinishedOpenCloseCage And Me.m_cage_cls.OperationType = Cage.CageOperationType.CloseCage) Then
            _diff = _cashier_total - _cage_total
          Else
            _diff = _cage_total - _cashier_total

          End If
          Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.DIFFERENCE_TOTAL).Value = _diff
          If _currency_type = FeatureChips.ChipType.COLOR Then
            Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.DIFFERENCE_TOTAL_WITH_SIMBOL).Value = Me.FormatNumber(_diff)
          Else
            Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.DIFFERENCE_TOTAL_WITH_SIMBOL).Value = Me.m_cage_cls.FormatAmount(_diff, False, _iso_code)
          End If

          If _diff > 0 Then
            If _currency_type = FeatureChips.ChipType.COLOR Then
              Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.DIFFERENCE_TOTAL_WITH_SIMBOL).Value = "+" & Me.FormatNumber(_diff)
            Else
              Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.DIFFERENCE_TOTAL_WITH_SIMBOL).Value = "+" & Me.m_cage_cls.FormatAmount(_diff, False, _iso_code)
            End If

          End If

          Me.SetRowColor(_idx_dg, Me.dg_total, True)
        Next

      Next
    End If
    ' Dispose
    Call SafeDispose(_rows_selected)
  End Sub ' FirstUpdateTotalGrid


  Private Sub FirstUpdateTotalGridCountCage(ByVal IsCountCage As Boolean, Optional ByVal IsFirstUpdate As Boolean = True)
    Dim _idx_dg As Integer
    Dim _diff As Decimal
    Dim _iso_code As String
    Dim _curr_type As String
    Dim _total As Decimal
    Dim _total_expected As Decimal
    Dim _total_bills_expected As Decimal
    Dim _total_coins_expected As Decimal
    Dim _total_others_expected As Decimal
    Dim _total_bills_collected As Decimal
    Dim _total_coins_collected As Decimal
    Dim _total_others_collected As Decimal
    Dim _list_of_types_coin_bill_and_chips As String
    Dim _type_condition As String
    Dim _condition As String
    Dim _expr_expected As String
    Dim _expr_cashier As String
    Dim _expr_total As String
    Dim _curr_description As String

    For _idx_dg = 0 To Me.dg_total.NumRows - 1
      _total_bills_expected = 0
      _total_coins_expected = 0
      _total_others_expected = 0
      _total_bills_collected = 0
      _total_coins_collected = 0
      _total_others_collected = 0
      _total = 0
      _total_expected = 0

      _iso_code = Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.CURRENCY).Value
      _curr_type = Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.CURRENCY_TYPE).Value
      _curr_description = Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.CURRENCY_TYPE_DESCRIPTION).Value

      _list_of_types_coin_bill_and_chips = "0, 1, " & FeatureChips.ChipType.RE & ", " & FeatureChips.ChipType.NR & ", " & FeatureChips.ChipType.COLOR & " "

      If _curr_type = CageCurrencyType.Bill Then
        _type_condition = " AND TYPE IN (" & _curr_type & ", " & CageCurrencyType.Coin & ") "
      Else
        _type_condition = " AND TYPE = '" & _curr_type & "' "
      End If


      If _curr_description.Equals(TICKETS_ISOCODE_NLS) Then ' TITO tickets
        ' CURRENCY_TYPE_DESCRIPTION 
        Continue For
      End If

      ' Set expressions
      _expr_total = "Sum(TOTAL)"
      _expr_expected = "Sum(EXPECTED_COLUMN)"
      _expr_cashier = "Sum(CASHIER_TOTAL)"

      If FeatureChips.IsCurrencyExchangeTypeChips(_curr_type) Then ' Chips

        ' Set condition
        _condition = String.Format("ISO_CODE = '" & _iso_code & "' " & _type_condition & " AND (TOTAL > 0 OR CASHIER_TOTAL {0} 0 ) ", IIf(m_gaming_values, "<>", ">"))

        ' Set values
        _total_expected = IIf(Not IsDBNull(Me.m_cage_cls.TableCurrencies.Compute(_expr_expected, _condition)), Me.m_cage_cls.TableCurrencies.Compute(_expr_expected, _condition), 0)
        _total = IIf(Not IsDBNull(Me.m_cage_cls.TableCurrencies.Compute(_expr_cashier, _condition)), Me.m_cage_cls.TableCurrencies.Compute(_expr_expected, _condition), 0)
        _total_others_expected = _total_expected

      Else

        If (IsFirstUpdate) Then
          _total_bills_expected = IIf(Not IsDBNull(Me.m_cage_cls.TableCurrencies.Compute(_expr_expected, "ISO_CODE = '" & _iso_code & "' AND DENOMINATION > 0 AND TYPE = 0 ")), Me.m_cage_cls.TableCurrencies.Compute(_expr_expected, "ISO_CODE = '" & _iso_code & "' AND DENOMINATION > 0 AND TYPE = 0 "), 0)
          _total_coins_expected = IIf(Not IsDBNull(Me.m_cage_cls.TableCurrencies.Compute(_expr_expected, "ISO_CODE = '" & _iso_code & "' AND DENOMINATION > 0 AND TYPE = 1 ")), Me.m_cage_cls.TableCurrencies.Compute(_expr_expected, "ISO_CODE = '" & _iso_code & "' AND DENOMINATION > 0 AND TYPE = 1 "), 0)
          If _total_coins_expected = 0 Then
            _total_others_expected = IIf(Not IsDBNull(Me.m_cage_cls.TableCurrencies.Compute(_expr_expected, "ISO_CODE ='" & _iso_code & "' AND DENOMINATION NOT IN (-200) AND TYPE NOT IN (" & _list_of_types_coin_bill_and_chips & ")  ")), Me.m_cage_cls.TableCurrencies.Compute(_expr_expected, "ISO_CODE ='" & _iso_code & "'AND DENOMINATION NOT IN (-200) AND TYPE NOT IN (" & _list_of_types_coin_bill_and_chips & ") "), 0)
          Else
            _total_others_expected = IIf(Not IsDBNull(Me.m_cage_cls.TableCurrencies.Compute(_expr_expected, "ISO_CODE ='" & _iso_code & "' AND DENOMINATION NOT IN (-200, -50) AND TYPE NOT IN (" & _list_of_types_coin_bill_and_chips & ")  ")), Me.m_cage_cls.TableCurrencies.Compute(_expr_expected, "ISO_CODE ='" & _iso_code & "' AND DENOMINATION NOT IN (-200, -50) AND TYPE NOT IN (" & _list_of_types_coin_bill_and_chips & ") "), 0)
          End If

          _total_bills_collected = IIf(Not IsDBNull(Me.m_cage_cls.TableCurrencies.Compute(_expr_total, "ISO_CODE = '" & _iso_code & "' AND DENOMINATION > 0 AND TYPE = 0  ")), Me.m_cage_cls.TableCurrencies.Compute(_expr_total, "ISO_CODE = '" & _iso_code & "' AND DENOMINATION > 0 AND TYPE = 0 "), 0)
          _total_coins_collected = IIf(Not IsDBNull(Me.m_cage_cls.TableCurrencies.Compute(_expr_total, "ISO_CODE = '" & _iso_code & "' AND DENOMINATION > 0 AND TYPE = 1  ")), Me.m_cage_cls.TableCurrencies.Compute(_expr_total, "ISO_CODE = '" & _iso_code & "' AND DENOMINATION > 0 AND TYPE = 1 "), 0)
          _total_others_collected = IIf(Not IsDBNull(Me.m_cage_cls.TableCurrencies.Compute(_expr_total, "ISO_CODE ='" & _iso_code & "' AND DENOMINATION NOT IN (-200, -50) AND  TYPE NOT IN (" & _list_of_types_coin_bill_and_chips & ")  ")), Me.m_cage_cls.TableCurrencies.Compute(_expr_total, "ISO_CODE ='" & _iso_code & "' AND DENOMINATION NOT IN (-200, -50) AND TYPE NOT IN (" & _list_of_types_coin_bill_and_chips & ") "), 0)

        Else
          _total_bills_expected = IIf(Not IsDBNull(Me.m_cage_cls.TableCurrencies.Compute(_expr_cashier, "ISO_CODE = '" & _iso_code & "' AND DENOMINATION > 0 AND TYPE = 0  ")), Me.m_cage_cls.TableCurrencies.Compute(_expr_cashier, "ISO_CODE = '" & _iso_code & "' AND DENOMINATION > 0 AND TYPE = 0 "), 0)
          _total_coins_expected = IIf(Not IsDBNull(Me.m_cage_cls.TableCurrencies.Compute(_expr_cashier, "ISO_CODE = '" & _iso_code & "' AND DENOMINATION > 0 AND TYPE = 1  ")), Me.m_cage_cls.TableCurrencies.Compute(_expr_cashier, "ISO_CODE = '" & _iso_code & "' AND DENOMINATION > 0 AND TYPE = 1 "), 0)
          If _total_coins_expected = 0 Then
            _total_others_expected = IIf(Not IsDBNull(Me.m_cage_cls.TableCurrencies.Compute(_expr_cashier, "ISO_CODE ='" & _iso_code & "' AND DENOMINATION NOT IN (-200) AND TYPE NOT IN (" & _list_of_types_coin_bill_and_chips & ")  ")), Me.m_cage_cls.TableCurrencies.Compute(_expr_cashier, "ISO_CODE ='" & _iso_code & "' AND DENOMINATION NOT IN (-200) AND TYPE NOT IN (" & _list_of_types_coin_bill_and_chips & ") "), 0)
          Else
            _total_others_expected = IIf(Not IsDBNull(Me.m_cage_cls.TableCurrencies.Compute(_expr_cashier, "ISO_CODE ='" & _iso_code & "' AND DENOMINATION NOT IN (-200, -50) AND TYPE NOT IN (" & _list_of_types_coin_bill_and_chips & ")  ")), Me.m_cage_cls.TableCurrencies.Compute(_expr_cashier, "ISO_CODE ='" & _iso_code & "' AND DENOMINATION NOT IN (-200, -50) AND TYPE NOT IN (" & _list_of_types_coin_bill_and_chips & ") "), 0)
          End If

          _total_bills_collected = IIf(Not IsDBNull(Me.m_cage_cls.TableCurrencies.Compute(_expr_expected, "ISO_CODE = '" & _iso_code & "' AND DENOMINATION > 0 AND TYPE = 0  ")), Me.m_cage_cls.TableCurrencies.Compute(_expr_expected, "ISO_CODE = '" & _iso_code & "' AND DENOMINATION > 0 AND TYPE = 0 "), 0)
          _total_coins_collected = IIf(Not IsDBNull(Me.m_cage_cls.TableCurrencies.Compute(_expr_expected, "ISO_CODE = '" & _iso_code & "' AND DENOMINATION > 0 AND TYPE = 1  ")), Me.m_cage_cls.TableCurrencies.Compute(_expr_expected, "ISO_CODE = '" & _iso_code & "' AND DENOMINATION > 0 AND TYPE = 1 "), 0)
          _total_others_collected = IIf(Not IsDBNull(Me.m_cage_cls.TableCurrencies.Compute(_expr_expected, "ISO_CODE ='" & _iso_code & "' AND DENOMINATION NOT IN (-200, -50) AND TYPE NOT IN (" & _list_of_types_coin_bill_and_chips & ")  ")), Me.m_cage_cls.TableCurrencies.Compute(_expr_expected, "ISO_CODE ='" & _iso_code & "' AND DENOMINATION NOT IN (-200, -50) AND TYPE NOT IN (" & _list_of_types_coin_bill_and_chips & ") "), 0)

        End If

        _total = _total_bills_collected + _total_coins_collected + _total_others_collected
        _total_expected = _total_bills_expected + _total_coins_expected + _total_others_expected

      End If

      ' Fill datagrid cells
      If IsCountCage = True And IsFirstUpdate Then
        Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.COINS).Value = _total_others_expected
        Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.BILLS).Value = _total_bills_expected + _total_coins_expected
        _total = _total_expected
      Else
        Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.COINS).Value = _total_others_collected
        Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.BILLS).Value = _total_bills_collected + _total_coins_collected
      End If

      Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.BILLS_WITH_SIMBOL).Value = _
                                                      Me.m_cage_cls.FormatAmount(Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.BILLS).Value, False, _iso_code)
      Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.COINS_WITH_SIMBOL).Value = _
                                                   Me.m_cage_cls.FormatAmount(Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.COINS).Value, False, _iso_code)

      Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.CASHIER_TOTAL).Value = _total_expected
      Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.CASHIER_TOTAL_WITH_SIMBOL).Value = Me.m_cage_cls.FormatAmount(_total_expected, False, _iso_code)


      Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.TOTAL).Value = _total
      Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.TOTAL_WITH_SIMBOL).Value = Me.m_cage_cls.FormatAmount(_total, False, _iso_code)

      _diff = _total - _total_expected

      Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.DIFFERENCE_TOTAL).Value = _diff
      Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.DIFFERENCE_TOTAL_WITH_SIMBOL).Value = Me.m_cage_cls.FormatAmount(_diff, False, _iso_code)

      Me.SetRowColor(_idx_dg, Me.dg_total, True)

    Next

  End Sub 'FirstUpdateTotalGrid_countCage


  ' PURPOSE: To save the quantities on the currency datatable
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row
  '           - IsoCode
  '           - Dg
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SaveCurrenciesQuantities(ByVal RowNumber As System.Int32, ByVal IsoCode As String, ByVal IsoType As Int32, ByRef Dg As uc_grid)
    Dim _rows As DataRow()
    Dim _row As DataRow
    Dim _denomination As String
    Dim _str_filter As String

    _rows = Nothing

    ' JAB 06-AUG-2014: Fixed Bug WIG-1154
    _denomination = WSI.Common.Format.LocalNumberToDBNumber(Dg.Cell(RowNumber, GRID_CURRENCY_COLUMN_DENOMINATION).Value)

    If IsoType = CageCurrencyType.Bill Then
      _str_filter = String.Format("ISO_CODE = '{0}' AND DENOMINATION = {1} AND TYPE IN ({2}, {3}, {4}) ", IsoCode, _denomination, IsoType, CInt(CageCurrencyType.Coin), CInt(CageCurrencyType.Others))
    ElseIf FeatureChips.IsCurrencyExchangeTypeChips(IsoType) Then
      _str_filter = String.Format("ISO_CODE = '{0}' AND TYPE = {1} AND CHIP_ID = {2} ", IsoCode, IsoType, Dg.Cell(RowNumber, GRID_CURRENCY_COLUMN_CHIP_ID).Value)
    Else
      _str_filter = String.Format("ISO_CODE = '{0}' AND DENOMINATION = {1} AND TYPE = {2} ", IsoCode, _denomination, IsoType)
    End If

    If Me.m_cage_cls.TableCurrencies IsNot Nothing Then
      _rows = Me.m_cage_cls.TableCurrencies.Select(_str_filter)
      For Each _row In _rows
        If _row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.DENOMINATION) _
           = Dg.Cell(RowNumber, GRID_CURRENCY_COLUMN_DENOMINATION).Value Then

          If _denomination = Cage.COINS_CODE.ToString() Then
            ' JBC 18-DEC-2014: Fixed Bug WIG-1530
            If Not String.IsNullOrEmpty(Dg.Cell(RowNumber, GRID_CURRENCY_COLUMN_TOTAL).Value) AndAlso Dg.Cell(RowNumber, GRID_CURRENCY_COLUMN_TOTAL).Value > 0 Then
              _row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.QUANTITY) = 1.0
            Else
              _row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.QUANTITY) = 0
            End If

          ElseIf Not String.IsNullOrEmpty(Dg.Cell(RowNumber, GRID_CURRENCY_COLUMN_QUANTITY).Value) Then
            _row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.QUANTITY) _
                        = CDbl(Me.m_cage_cls.GetRowValue(Dg.Cell(RowNumber, GRID_CURRENCY_COLUMN_QUANTITY).Value))
          Else
            _row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.QUANTITY) = DBNull.Value
          End If

          _row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.TOTAL) = CDbl(Me.m_cage_cls.GetRowValue(Dg.Cell(RowNumber, GRID_CURRENCY_COLUMN_TOTAL).Value))
          Exit For

        End If
      Next
    End If

    ' Dispose
    Call SafeDispose(_rows)

  End Sub ' SaveCurrenciesQuantities

  ' PURPOSE: Sets color in grid according to difference
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row
  '           - Grid
  '           - TotalGrid
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetRowColor(ByVal Row As Integer, ByVal Grid As uc_grid, Optional ByVal TotalGrid As Boolean = False)

    Select Case Me.m_cage_cls.Status
      Case Cage.CageStatus.Sent
        If Me.m_cage_cls.OperationType = Cage.CageOperationType.FromCashier OrElse Me.m_cage_cls.OperationType = Cage.CageOperationType.FromCashierClosing Then
          Call Me.DrawColorRow(Row, Grid, TotalGrid)
        ElseIf Me.m_cage_cls.OperationType = Cage.CageOperationType.CloseCage OrElse Me.m_cage_cls.OperationType = Cage.CageOperationType.CountCage Then
          Call Me.DrawColorRow(Row, Grid, TotalGrid)
        ElseIf m_gaming_values Then
          Call Me.DrawColorRow(Row, Grid, TotalGrid)
        ElseIf Me.m_is_tito_collection Then
          Call Me.DrawColorRow(Row, Grid, TotalGrid)
        ElseIf Me.m_cage_cls.OperationType = Cage.CageOperationType.RequestOperation Then
          Call Me.DrawColorRow(Row, Grid, TotalGrid)
        ElseIf Me.m_cage_cls.OperationType = Cage.CageOperationType.FromGamingTable Or Me.m_cage_cls.OperationType = Cage.CageOperationType.FromGamingTableClosing Then
          Call Me.DrawColorRow(Row, Grid, TotalGrid)
        End If

      Case Cage.CageStatus.OK _
        , Cage.CageStatus.OkAmountImbalance _
        , Cage.CageStatus.OkDenominationImbalance _
        , Cage.CageStatus.FinishedCountCage
        Call Me.DrawColorRow(Row, Grid, TotalGrid)

      Case Cage.CageStatus.FinishedOpenCloseCage
        If Me.m_cage_cls.OperationType = Cage.CageOperationType.CloseCage Then
          Call Me.DrawColorRow(Row, Grid, TotalGrid)
        End If

    End Select

  End Sub ' SetRowColor

  ' PURPOSE: Sets color in grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row
  '           - Grid
  '           - TotalGrid
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub DrawColorRow(ByVal Row As Integer, ByVal Grid As uc_grid, ByVal TotalGrid As Boolean)
    Dim _column As Integer
    Dim _cell_value As String
    Dim _iso_code As String
    Dim _denomination As Long
    Dim _currency_type As CageCurrencyType

    If TotalGrid Then
      _column = GRID_TOTAL_COLUMN_DIFFERENCE_TOTAL
      _iso_code = Grid.Cell(Row, GRID_TOTAL_COLUMN_CURRENCY).Value
      _currency_type = Grid.Cell(Row, GRID_TOTAL_COLUMN_CURRENCY_TYPE).Value
      _denomination = Grid.Cell(Row, GRID_TOTAL_COLUMN_TOTAL).Value
    Else
      If Grid.Cell(Row, GRID_CURRENCY_COLUMN_DENOMINATION).Value >= 0 _
        Or Grid.Cell(Row, GRID_CURRENCY_COLUMN_DENOMINATION).Value = Cage.TICKETS_CODE Then
        _column = GRID_CURRENCY_COLUMN_QUANTITY_DIFFERENCE
      Else
        _column = GRID_CURRENCY_COLUMN_TOTAL_DIFFERENCE
      End If
      _iso_code = Grid.Cell(Row, GRID_CURRENCY_COLUMN_ISO_CODE).Value
      _currency_type = Grid.Cell(Row, GRID_CURRENCY_COLUMN_CURRENCY_TYPE).Value
      _denomination = Grid.Cell(Row, GRID_CURRENCY_COLUMN_DENOMINATION).Value
    End If


    'If is chips, just get the value
    If _currency_type = FeatureChips.ChipType.RE Or _currency_type = FeatureChips.ChipType.NR Then
      If _column = GRID_CURRENCY_COLUMN_TOTAL_DIFFERENCE Then
        _cell_value = Me.m_cage_cls.GetRowValue(Grid.Cell(Row, _column).Value)
      Else
        _cell_value = Me.GetRawNumber(Grid.Cell(Row, _column).Value, _iso_code)
      End If
    Else
      _cell_value = Me.GetRawNumber(Grid.Cell(Row, _column).Value, _iso_code)
    End If
    GUI_DBNumberToLocalNumber(_cell_value)

    If Not String.IsNullOrEmpty(_cell_value) AndAlso GUI_DBNumberToLocalNumber(_cell_value) <> 0 Then
      If _cell_value > 0 Then
        If TotalGrid Then
          Grid.Cell(Row, GRID_TOTAL_COLUMN_DIFFERENCE_TOTAL_WITH_SIMBOL).BackColor = GetColor(COLOR_POSITIVE_DIFFERENCE)
        Else
          If _denomination <> Cage.COINS_COLLECTION_CODE Then
            Grid.Cell(Row, GRID_CURRENCY_COLUMN_QUANTITY_DIFFERENCE).BackColor = GetColor(COLOR_POSITIVE_DIFFERENCE)
          Else
            Grid.Cell(Row, GRID_CURRENCY_COLUMN_QUANTITY_DIFFERENCE).BackColor = GetColor(COLOR_NO_DIFFERENCE)
          End If
          Grid.Cell(Row, GRID_CURRENCY_COLUMN_TOTAL_DIFFERENCE).BackColor = GetColor(COLOR_POSITIVE_DIFFERENCE)
        End If

      ElseIf _cell_value < 0 Then
        If TotalGrid Then
          Grid.Cell(Row, GRID_TOTAL_COLUMN_DIFFERENCE_TOTAL_WITH_SIMBOL).BackColor = GetColor(COLOR_NEGATIVE_OR_POSITIVE_DIFFERENCE)
        Else
          If _denomination <> Cage.COINS_COLLECTION_CODE Then
            Grid.Cell(Row, GRID_CURRENCY_COLUMN_QUANTITY_DIFFERENCE).BackColor = GetColor(COLOR_NEGATIVE_OR_POSITIVE_DIFFERENCE)
          Else
            Grid.Cell(Row, GRID_CURRENCY_COLUMN_QUANTITY_DIFFERENCE).BackColor = GetColor(COLOR_NO_DIFFERENCE)
          End If
          Grid.Cell(Row, GRID_CURRENCY_COLUMN_TOTAL_DIFFERENCE).BackColor = GetColor(COLOR_NEGATIVE_OR_POSITIVE_DIFFERENCE)
        End If
      End If
    Else
      If Not String.IsNullOrEmpty(_cell_value) Then
        If TotalGrid Then
          Grid.Cell(Row, GRID_TOTAL_COLUMN_DIFFERENCE_TOTAL_WITH_SIMBOL).BackColor = GetColor(COLOR_NO_DIFFERENCE)
        Else
          Grid.Cell(Row, GRID_CURRENCY_COLUMN_QUANTITY_DIFFERENCE).BackColor = GetColor(COLOR_NO_DIFFERENCE)
          Grid.Cell(Row, GRID_CURRENCY_COLUMN_TOTAL_DIFFERENCE).BackColor = GetColor(COLOR_NO_DIFFERENCE)
        End If
      End If
    End If
  End Sub ' DrawColorRow

  ' PURPOSE: Return the number without currency symbol
  '
  '  PARAMS:
  '     - INPUT:
  '           - Number
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - GetRawNumber
  Private Function GetRawNumber(ByVal Number As String, ByVal IsoCode As String) As String
    Dim _currency_exchange As WSI.Common.CurrencyExchangeProperties

    'GetRawNumber = Number

    'If Not String.IsNullOrEmpty(Number) Then
    '  _currency_exchange = WSI.Common.CurrencyExchangeProperties.GetProperties(IsoCode)

    '  If Not _currency_exchange Is Nothing Then
    '    GetRawNumber = Number.Replace(_currency_exchange.Symbol, String.Empty)
    '  End If
    'End If

    'Return GetRawNumber
    Dim is_number As Boolean
    Dim result As Double
    Dim _culture_info As System.Globalization.CultureInfo

    _currency_exchange = WSI.Common.CurrencyExchangeProperties.GetProperties(IsoCode)

    If _currency_exchange Is Nothing Then
      _currency_exchange = WSI.Common.CurrencyExchangeProperties.GetProperties(GeneralParam.GetString("RegionalOptions", "CurrencyISOCode"))
    End If
    _culture_info = System.Globalization.CultureInfo.CurrentCulture.Clone()
    _culture_info.NumberFormat.CurrencySymbol = _currency_exchange.Symbol

    'OC-changed to parse only currency
    is_number = Double.TryParse(Number, Globalization.NumberStyles.Currency, _culture_info.NumberFormat, result)

    If Not is_number Then
      result = 0
    End If

    Return result
  End Function '  GetRawNumber

  ' PURPOSE: To select the Datagrid Total row in function of the selected item in combo currencies
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SelectRowDgTotal()
    Dim _idx As Integer
    Dim _selected_tag As String

    If Me.tb_currency.TabCount > 0 Then
      _selected_tag = Me.tb_currency.SelectedTab.Tag

      If Not _selected_tag = Nothing Then
        Me.dg_total.ClearSelection()
        For _idx = 0 To Me.dg_total.NumRows - 1
          If _selected_tag.Contains(Me.dg_total.Cell(_idx, TOTAL_GRID_COLUMNS.CURRENCY).Value + Me.dg_total.Cell(_idx, TOTAL_GRID_COLUMNS.CURRENCY_TYPE).Value) Then
            Me.dg_total.IsSelected(_idx) = True
            Me.dg_total.TopRow = _idx
            Me.dg_total.Refresh()
            If Me.tb_currency.TabCount > _idx Then
              Me.tb_currency.SelectTab(_idx)
            End If
            Exit For
          End If
        Next
      End If

    End If

  End Sub ' SelectRowDgTotal

  ' PURPOSE: To create tabs with every allowed currency
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub CreateCurrencyTabs(ByVal TableCurr As DataTable)
    Dim _row As DataRow
    Dim _national_cur_finded As Boolean

    _national_cur_finded = False

    m_chips_set_filter = String.Empty
    m_chips_set_filter = Me.m_cage_cls.GetAllowedChipSets(Me.cmb_gaming_table.Value)
    If m_chips_set_filter.Length() > 0 Then
      m_chips_set_filter = " AND CHIP_SET_ID IN (" & m_chips_set_filter & ") "
    End If

    ' DHA 13-JAN-2016: set data by terminal currency
    If WSI.Common.Misc.IsFloorDualCurrencyEnabled() And Me.m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal And Not String.IsNullOrEmpty(Me.m_cage_cls.TerminalIsoCode) Then
      Call Me.AddTab(Me.m_cage_cls.TerminalIsoCode, WSI.Common.CurrencyExchangeType.CURRENCY, "")
    Else
      For Each _row In TableCurr.Rows

        If Not _national_cur_finded AndAlso _row.Item(1).Equals(Me.m_national_currency) Then
          _national_cur_finded = True
          'Continue For
        End If

        If Me.m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal Then
          If _row.Item(1).Equals(Me.m_national_currency) And (Not FeatureChips.IsCageCurrencyTypeChips(CType(_row.Item("TYPE"), CageCurrencyType))) Then
            Call Me.AddTab(_row.Item(1), _row.Item(2), _row.Item(3))
          End If
        Else
          Call Me.AddTab(_row.Item(1), _row.Item(2), _row.Item(3))
        End If
      Next

      If Not _national_cur_finded Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3015), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        DbEditedObject = DbReadObject 'TODO: Use another way to avoid the message from the auditory
        Me.Close()

        Exit Sub
      End If
    End If

    ' Tickets tab
    If Me.m_is_tito_collection And TITO.Utils.IsTitoMode() Then
      Me.tb_collections.TabPages(0).Text = TICKETS_ISOCODE_NLS
      Me.tb_collections.TabPages(0).Tag = CurrencyExchange.GetNationalCurrency() & CageCurrencyType.Others
      Me.tb_currency.TabPages.Add(Me.tb_collections.TabPages(0))
      Call GUI_StyleSheetTicket()
    End If

  End Sub ' CreateCurrencyTabs

  ' PURPOSE: Check if allowed currency when has values
  '
  '  PARAMS:
  '     - INPUT:
  '           - IsoCode
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - True if allowed, False another case
  Private Function AllowedHasQuantity(ByVal IsoCode As String) As Boolean
    Dim _rows As DataRow()
    Dim _query As String
    ' 16-FEB-2015 FOS 
    Dim _movement_id As Int64

    _movement_id = Me.m_cage_cls.MovementID
    _query = "ISO_CODE = '" & IsoCode & "'"

    _rows = Nothing

    If Me.m_cage_cls.OperationType = Cage.CageOperationType.FromCashier Or Me.m_cage_cls.OperationType = Cage.CageOperationType.FromCashierClosing Then
      _query &= " AND CASHIER_QUANTITY <> 0"
    ElseIf Me.m_cage_cls.OperationType = Cage.CageOperationType.CloseCage Or Me.m_cage_cls.OperationType = Cage.CageOperationType.CountCage Or Me.m_cage_cls.OperationType = Cage.CageOperationType.LostQuadrature Or Me.m_cage_cls.OperationType = Cage.CageOperationType.GainQuadrature Then
      _query &= " AND CASHIER_QUANTITY <> 0"
      _movement_id = -1
    Else
      _query &= " AND QUANTITY <> 0"
    End If
    AllowedHasQuantity = False
    If _movement_id <> 0 Then
      _rows = Me.m_cage_cls.TableCurrencies.Select(_query)
      If _rows.Length > 0 Then
        AllowedHasQuantity = True
      End If
    End If

    ' Dispose
    Call SafeDispose(_rows)

    Return AllowedHasQuantity
  End Function ' AllowedHasQuantity

  ' PURPOSE: To configure the data grid column width for General Param Cage.ShowDenominations
  '
  '  PARAMS:
  '     - INPUT:
  '           - IsoCode
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDataGridShowDenominationsGP(ByVal IsoCode As String, ByVal IsoType As Int32)
    Dim _show_mode As Cage.ShowDenominationsMode
    Dim _show_columns As Boolean

    Dim _chip_types As String

    _chip_types = FeatureChips.ChipType.RE & ", " & FeatureChips.ChipType.NR & ", " & FeatureChips.ChipType.COLOR

    If m_cage_cls.TableCurrencies.Select(" ISO_CODE = '" & IsoCode & "' AND TYPE NOT IN (" & _chip_types & ") AND (DENOMINATION > 0 OR DENOMINATION = " & Cage.TICKETS_CODE & " OR ISO_CODE = '" & Cage.CHIPS_COLOR & "') ").Length > 0 Then
      _show_mode = Cage.ShowDenominationsMode.ShowAllDenominations
    Else
      _show_mode = m_show_denominations
    End If

    Select Case _show_mode
      Case Cage.ShowDenominationsMode.HideAllDenominations
        _show_columns = False

      Case Cage.ShowDenominationsMode.ShowOnlyChipsDenominations
        _show_columns = FeatureChips.IsCurrencyExchangeTypeChips(IsoType)

      Case Cage.ShowDenominationsMode.ShowAllDenominations
        _show_columns = True

    End Select

    If _show_columns Then
      GRID_CURRENCY_COLUMN_QUANTITY_WIDTH = 1005
      'GRID_CURRENCY_COLUMN_CASHIER_QUANTITY_WIDTH = 1005
      'GRID_CURRENCY_COLUMN_QUANTITY_DIFFERENCE_WIDTH = 1005

    Else
      If IsoType = FeatureChips.ChipType.COLOR Then
        GRID_CURRENCY_COLUMN_QUANTITY_WIDTH = 1005
      Else
        GRID_CURRENCY_COLUMN_QUANTITY_WIDTH = 0
      End If
      'GRID_CURRENCY_COLUMN_CASHIER_QUANTITY_WIDTH = 0
      'GRID_CURRENCY_COLUMN_QUANTITY_DIFFERENCE_WIDTH = 0

      GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL_WIDTH = IIf(GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL_WIDTH > 0, 2800, 0)
      GRID_CURRENCY_COLUMN_CASHIER_TOTAL_WIDTH = IIf(GRID_CURRENCY_COLUMN_CASHIER_TOTAL_WIDTH > 0, 2800, 0)
      GRID_CURRENCY_COLUMN_TOTAL_DIFFERENCE_WIDTH = IIf(GRID_CURRENCY_COLUMN_TOTAL_DIFFERENCE_WIDTH > 0, 2800, 0)
    End If

    'If m_cage_cls.TableCurrencies.Select(" ISO_CODE = '" & IsoCode & "' AND (DENOMINATION > 0 " & ")").Length > 0 Then
    '  GRID_CURRENCY_COLUMN_CURRENCY_TYPE_DESCRIPTION_WIDTH = GRID_CURRENCY_COLUMN_CHIP_COLOR_WIDTH
    'Else
    '  GRID_CURRENCY_COLUMN_CURRENCY_TYPE_DESCRIPTION_WIDTH = 0
    'End If

  End Sub ' SetDataGridShowDenominationsGP

  ' PURPOSE: Add a new currency tab in the control
  '
  '  PARAMS:
  '     - INPUT:
  '           - IsoCode
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub AddTab(ByVal IsoCode As String, ByVal Type As Int32, ByVal Name As String)
    Dim _tab As Windows.Forms.TabPage
    Dim _datagrid As uc_grid
    Dim _dg_size As System.Drawing.Size
    Dim _dg_padding As Windows.Forms.Padding
    Dim _currency_iso_type As CurrencyIsoType

    'Harcoded size
    _dg_size.Width = 933
    _dg_size.Height = 266
    _dg_padding.All = 3

    Call Me.SetDataGridShowDenominationsGP(IsoCode, Type)

    _tab = New Windows.Forms.TabPage
    _datagrid = New uc_grid

    _datagrid.WordWrap = True

    AddHandler _datagrid.CellDataChangedEvent, AddressOf CellDataChangeHandler
    AddHandler _datagrid.BeforeStartEditionEvent, AddressOf BeforeStartEditionHandler

    _datagrid.Size = _dg_size
    _datagrid.PanelRightVisible = False
    _datagrid.Padding = _dg_padding
    _datagrid.Margin = _dg_padding
    _datagrid.Sortable = False

    Call Me.SetStyleSheetGrid(_datagrid, IsoCode, Type)
    Call Me.SetCurrencyGridData(IsoCode, Type, _datagrid)

    _tab.Name = TABNAME_PREFIX & IsoCode & Type.ToString()
    _tab.Text = IsoCode
    _tab.Tag = IsoCode & Type.ToString()

    If Type > 0 Then
      _tab.Text = Name
    End If

    _tab.Controls.Add(_datagrid)

    _currency_iso_type = New CurrencyIsoType()
    _currency_iso_type.IsoCode = IsoCode
    _currency_iso_type.Type = Type

    If Not m_currency_tabs_dictionary.ContainsKey(_currency_iso_type) Then
      m_currency_tabs_dictionary.Add(_currency_iso_type, _tab)
    End If

    Me.tb_currency.TabPages.Add(_tab)

  End Sub ' AddTab

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - DataGrid
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetStyleSheetGrid(ByRef DataGrid As uc_grid, Optional ByVal ISOCode As String = "", Optional ByVal ISOType As Int32 = 0)
    Dim _nls_id As Integer

    With DataGrid

      Select Case m_cage_cls.OpenMode
        Case CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT _
           , CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MOVEMENT _
           , CLASS_CAGE_CONTROL.OPEN_MODE.CLOSE_VIRTUAL _
           , CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE _
           , CLASS_CAGE_CONTROL.OPEN_MODE.TEMPLATE_MAINTENANCE

          Call .Init(GRID_CURRENCY_COLUMN_COUNT, 2, True)
          If m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.TEMPLATE_MAINTENANCE Then
            Call .Init(GRID_CURRENCY_COLUMN_COUNT, 2, False)
            If Me.m_cage_cls.IsTemplateEditable Then
              Call .Init(GRID_CURRENCY_COLUMN_COUNT, 2, FeatureChips.IsCageCurrencyTypeChips(ISOType) Or Me.m_gamingtables_transfer_currency_enabled)
            End If
          End If
        Case CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MASSIVE
          If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT Then
            Call .Init(GRID_CURRENCY_COLUMN_COUNT, 2, True)
          Else
            Call .Init(GRID_CURRENCY_COLUMN_COUNT, 2, False)
          End If
        Case Else
          Call .Init(GRID_CURRENCY_COLUMN_COUNT, 2, False)
      End Select

      If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT _
          AndAlso Not Me.m_new_collecion_mode AndAlso Me.m_is_tito_collection Then

        Call .Init(GRID_CURRENCY_COLUMN_COUNT, 2, False)
      End If

      ' Index
      .Column(GRID_CURRENCY_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_CURRENCY_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_CURRENCY_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_CURRENCY_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_CURRENCY_COLUMN_INDEX).IsColumnPrintable = False

      .Column(GRID_CURRENCY_COLUMN_ISO_CODE).Header(0).Text = "xIsoCode"
      .Column(GRID_CURRENCY_COLUMN_ISO_CODE).Width = GRID_CURRENCY_COLUMN_ISO_CODE_WIDTH

      .Column(GRID_CURRENCY_COLUMN_CURRENCY_TYPE).Header(0).Text = "xIsoType"
      .Column(GRID_CURRENCY_COLUMN_CURRENCY_TYPE).Width = GRID_CURRENCY_COLUMN_CURRENCY_TYPE_WIDTH

      .Column(GRID_CURRENCY_COLUMN_DENOMINATION_WITH_SIMBOL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2270) ' Denominacion
      .Column(GRID_CURRENCY_COLUMN_DENOMINATION_WITH_SIMBOL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1060) ' Valor
      .Column(GRID_CURRENCY_COLUMN_DENOMINATION_WITH_SIMBOL).Width = GRID_CURRENCY_COLUMN_DENOMINATION_WITH_SIMBOL_WIDTH
      .Column(GRID_CURRENCY_COLUMN_DENOMINATION_WITH_SIMBOL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      '.Column(GRID_CURRENCY_COLUMN_DENOMINATION_WITH_SIMBOL).IsMerged = True
      .Column(GRID_CURRENCY_COLUMN_DENOMINATION_WITH_SIMBOL).HighLightWhenSelected = True

      ' Currency Type
      .Column(GRID_CURRENCY_COLUMN_CURRENCY_TYPE_DESCRIPTION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2270) ' Denominacion
      .Column(GRID_CURRENCY_COLUMN_CURRENCY_TYPE_DESCRIPTION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5678) ' Tipo
      .Column(GRID_CURRENCY_COLUMN_CURRENCY_TYPE_DESCRIPTION).Width = GRID_CURRENCY_COLUMN_CURRENCY_TYPE_DESCRIPTION_WIDTH
      .Column(GRID_CURRENCY_COLUMN_CURRENCY_TYPE_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_CURRENCY_COLUMN_CURRENCY_TYPE_DESCRIPTION).HighLightWhenSelected = False

      ' Chip color
      .Column(GRID_CURRENCY_COLUMN_CHIP_COLOR).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2270) ' Denomination
      .Column(GRID_CURRENCY_COLUMN_CHIP_COLOR).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1291) ' Color
      .Column(GRID_CURRENCY_COLUMN_CHIP_COLOR).Width = GRID_CURRENCY_COLUMN_CHIP_COLOR_WIDTH
      .Column(GRID_CURRENCY_COLUMN_CHIP_COLOR).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_CURRENCY_COLUMN_CHIP_COLOR).HighLightWhenSelected = False

      ' Chip set name
      .Column(GRID_CURRENCY_COLUMN_CHIP_SET_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2270) ' Denomination
      .Column(GRID_CURRENCY_COLUMN_CHIP_SET_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2420) ' Grupo
      .Column(GRID_CURRENCY_COLUMN_CHIP_SET_NAME).Width = GRID_CURRENCY_COLUMN_CHIP_SET_NAME_WIDTH
      .Column(GRID_CURRENCY_COLUMN_CHIP_SET_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT_CENTER
      .Column(GRID_CURRENCY_COLUMN_CHIP_SET_NAME).HighLightWhenSelected = False
      .Column(GRID_CURRENCY_COLUMN_CHIP_SET_NAME).IsMerged = True

      ' Chip name
      .Column(GRID_CURRENCY_COLUMN_CHIP_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2270) ' Denomination
      .Column(GRID_CURRENCY_COLUMN_CHIP_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4979) ' Name
      .Column(GRID_CURRENCY_COLUMN_CHIP_NAME).Width = GRID_CURRENCY_COLUMN_CHIP_NAME_WIDTH
      .Column(GRID_CURRENCY_COLUMN_CHIP_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_CURRENCY_COLUMN_CHIP_NAME).HighLightWhenSelected = False

      ' Chip draw
      .Column(GRID_CURRENCY_COLUMN_CHIP_DRAWING).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2270) ' Denomination
      .Column(GRID_CURRENCY_COLUMN_CHIP_DRAWING).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6924) ' Drawing
      .Column(GRID_CURRENCY_COLUMN_CHIP_DRAWING).Width = GRID_CURRENCY_COLUMN_CHIP_DRAW_WIDTH
      .Column(GRID_CURRENCY_COLUMN_CHIP_DRAWING).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_CURRENCY_COLUMN_CHIP_DRAWING).HighLightWhenSelected = False

      ' Chip id
      .Column(GRID_CURRENCY_COLUMN_CHIP_ID).Header(0).Text = "" 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(2270) ' Denomination
      .Column(GRID_CURRENCY_COLUMN_CHIP_ID).Header(1).Text = "" 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(6924) ' Drawing
      .Column(GRID_CURRENCY_COLUMN_CHIP_ID).Width = GRID_CURRENCY_COLUMN_CHIP_ID_WIDTH
      .Column(GRID_CURRENCY_COLUMN_CHIP_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_CURRENCY_COLUMN_CHIP_ID).HighLightWhenSelected = False
      .Column(GRID_CURRENCY_COLUMN_CHIP_ID).Editable = False
      .Column(GRID_CURRENCY_COLUMN_CHIP_ID).IsColumnPrintable = False

      ' Chip set id
      .Column(GRID_CURRENCY_COLUMN_CHIP_SET_ID).Header(0).Text = "" 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(2270) ' Denomination
      .Column(GRID_CURRENCY_COLUMN_CHIP_SET_ID).Header(1).Text = "" 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(6924) ' Drawing
      .Column(GRID_CURRENCY_COLUMN_CHIP_SET_ID).Width = GRID_CURRENCY_COLUMN_CHIP_SET_ID_WIDTH
      .Column(GRID_CURRENCY_COLUMN_CHIP_SET_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_CURRENCY_COLUMN_CHIP_SET_ID).HighLightWhenSelected = False
      .Column(GRID_CURRENCY_COLUMN_CHIP_SET_ID).Editable = False
      .Column(GRID_CURRENCY_COLUMN_CHIP_SET_ID).IsColumnPrintable = False

      If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE Or Me.m_is_tito_collection Then
        _nls_id = 5011 ' Real        
      Else
        _nls_id = 2876 ' Boveda        
      End If

      .Column(GRID_CURRENCY_COLUMN_QUANTITY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_id)
      .Column(GRID_CURRENCY_COLUMN_QUANTITY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2953) ' Diferencia cantidad
      .Column(GRID_CURRENCY_COLUMN_QUANTITY).Width = GRID_CURRENCY_COLUMN_QUANTITY_WIDTH
      .Column(GRID_CURRENCY_COLUMN_QUANTITY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_CURRENCY_COLUMN_QUANTITY).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_CURRENCY_COLUMN_QUANTITY).EditionControl.EntryField.SetFilter(FORMAT_NUMBER, 7)
      .Column(GRID_CURRENCY_COLUMN_QUANTITY).HighLightWhenSelected = False
      .Column(GRID_CURRENCY_COLUMN_QUANTITY).SystemType = Type.GetType("System.Int32")

      If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE Or Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.VIEW_MOVEMENT Or Me.m_is_tito_collection Then
        _nls_id = 5011 ' Real      
      Else
        _nls_id = 2876 ' Boveda
      End If

      .Column(GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_id)
      .Column(GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2936) ' Diferencia Total
      .Column(GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).Width = GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL_WIDTH
      .Column(GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).EditionControl.EntryField.SetFilter(FORMAT_MONEY, 11, 2, ISOCode) '(FORMAT_NUMBER, 4)
      .Column(GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).HighLightWhenSelected = False
      .Column(GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).SystemType = Type.GetType("System.Decimal")

      .Column(GRID_CURRENCY_COLUMN_DENOMINATION).Header(1).Text = "xDenom"
      .Column(GRID_CURRENCY_COLUMN_DENOMINATION).Width = GRID_CURRENCY_COLUMN_DENOMINATION_WIDTH
      .Column(GRID_CURRENCY_COLUMN_DENOMINATION).HighLightWhenSelected = False

      If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE Or Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.VIEW_MOVEMENT Or Me.m_is_tito_collection Then
        _nls_id = 5011 ' Real      
      Else
        _nls_id = 2876 ' Boveda
      End If

      .Column(GRID_CURRENCY_COLUMN_TOTAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_id)  ' Boveda 
      .Column(GRID_CURRENCY_COLUMN_TOTAL).Header(1).Text = "xTot"
      .Column(GRID_CURRENCY_COLUMN_TOTAL).Width = GRID_CURRENCY_COLUMN_TOTAL_WIDTH
      .Column(GRID_CURRENCY_COLUMN_TOTAL).HighLightWhenSelected = False
      .Column(GRID_CURRENCY_COLUMN_TOTAL).SystemType = Type.GetType("System.Decimal")

      If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE Or Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.VIEW_MOVEMENT Or Me.m_is_tito_collection Then
        _nls_id = 5010 ' Teorico
      ElseIf m_gaming_values Then
        _nls_id = 4406 ' Mesa de juego
      ElseIf Me.m_cage_cls.OperationType = Cage.CageOperationType.RequestOperation Then
        _nls_id = 4784 ' Solicitado
      Else
        _nls_id = 1818 ' Cajero        
      End If

      .Column(GRID_CURRENCY_COLUMN_CASHIER_QUANTITY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_id)
      'CASHIER COLUMN
      .Column(GRID_CURRENCY_COLUMN_CASHIER_QUANTITY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2953) ' Diferencia cantidad
      .Column(GRID_CURRENCY_COLUMN_CASHIER_QUANTITY).Width = GRID_CURRENCY_COLUMN_CASHIER_QUANTITY_WIDTH
      .Column(GRID_CURRENCY_COLUMN_CASHIER_QUANTITY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_CURRENCY_COLUMN_CASHIER_QUANTITY).HighLightWhenSelected = False
      .Column(GRID_CURRENCY_COLUMN_CASHIER_QUANTITY).SystemType = Type.GetType("System.Int32")

      If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE Or Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.VIEW_MOVEMENT Or Me.m_is_tito_collection Then
        _nls_id = 5010 ' Teorico
      ElseIf m_gaming_values Then
        _nls_id = 4406 ' Mesa de juego
      ElseIf Me.m_cage_cls.OperationType = Cage.CageOperationType.RequestOperation Then
        _nls_id = 4784 ' Solicitado
      Else
        _nls_id = 1818 ' Cajero        
      End If

      .Column(GRID_CURRENCY_COLUMN_CASHIER_TOTAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_id) ' Cajero
      .Column(GRID_CURRENCY_COLUMN_CASHIER_TOTAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2936) ' Diferencia Total
      .Column(GRID_CURRENCY_COLUMN_CASHIER_TOTAL).Width = GRID_CURRENCY_COLUMN_CASHIER_TOTAL_WIDTH
      .Column(GRID_CURRENCY_COLUMN_CASHIER_TOTAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_CURRENCY_COLUMN_CASHIER_TOTAL).HighLightWhenSelected = False
      .Column(GRID_CURRENCY_COLUMN_CASHIER_TOTAL).SystemType = Type.GetType("System.Decimal")

      'DIFFERENCE COLUMN
      .Column(GRID_CURRENCY_COLUMN_QUANTITY_DIFFERENCE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2610) ' Diferencia
      .Column(GRID_CURRENCY_COLUMN_QUANTITY_DIFFERENCE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2953) ' Diferencia cantidad
      .Column(GRID_CURRENCY_COLUMN_QUANTITY_DIFFERENCE).Width = GRID_CURRENCY_COLUMN_QUANTITY_DIFFERENCE_WIDTH

      .Column(GRID_CURRENCY_COLUMN_QUANTITY_DIFFERENCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_CURRENCY_COLUMN_QUANTITY_DIFFERENCE).HighLightWhenSelected = False
      .Column(GRID_CURRENCY_COLUMN_QUANTITY_DIFFERENCE).SystemType = Type.GetType("System.Int32")

      .Column(GRID_CURRENCY_COLUMN_TOTAL_DIFFERENCE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2610) ' Diferencia
      .Column(GRID_CURRENCY_COLUMN_TOTAL_DIFFERENCE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2954) ' Diferencia Total
      .Column(GRID_CURRENCY_COLUMN_TOTAL_DIFFERENCE).Width = GRID_CURRENCY_COLUMN_TOTAL_DIFFERENCE_WIDTH
      .Column(GRID_CURRENCY_COLUMN_TOTAL_DIFFERENCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_CURRENCY_COLUMN_TOTAL_DIFFERENCE).HighLightWhenSelected = False
      .Column(GRID_CURRENCY_COLUMN_TOTAL_DIFFERENCE).SystemType = Type.GetType("System.Decimal")

      If (Me.m_cage_cls.Status = Cage.CageStatus.FinishedOpenCloseCage And Me.m_cage_cls.OperationType = Cage.CageOperationType.CloseCage) Then

        .Column(GRID_CURRENCY_COLUMN_QUANTITY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5010) ' Teorico
        .Column(GRID_CURRENCY_COLUMN_TOTAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5010) ' Teorico
        .Column(GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5010) ' Teorico

        .Column(GRID_CURRENCY_COLUMN_CASHIER_TOTAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5011) ' Real
        .Column(GRID_CURRENCY_COLUMN_CASHIER_QUANTITY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5011) ' Real
      End If

    End With

    Call SetGrids()

  End Sub ' SetStyleSheetGrid

  ' PURPOSE: Sets values for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - IsoCode
  '           - DataGrid
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetCurrencyGridData(ByVal IsoCode As String _
                                , ByVal IsoType As Int32 _
                                , ByRef DataGrid As uc_grid _
                                , Optional ByVal IsGamblingCollection As Boolean = False _
                                , Optional ByVal IsCustomCollection As Boolean = False)

    Dim _row_count As Integer
    Dim _row As DataRow
    Dim _rows As DataRow()
    Dim _denomination As Decimal
    Dim _dt_Deno_Colors As DataTable
    Dim _currency_iso_code As String
    Dim _type_filter As String
    Dim _order_by As String


    If Not DataGrid Is Nothing Then
      Call DataGrid.Clear()
    End If

    _currency_iso_code = m_national_currency

    ' DHA 13-JAN-16: set currency terminal
    If Me.m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal AndAlso WSI.Common.Misc.IsFloorDualCurrencyEnabled() Then
      _currency_iso_code = Me.m_cage_cls.TerminalIsoCode
    End If
    _row_count = 0
    If IsoType = CageCurrencyType.Bill Or IsoType = CageCurrencyType.Coin Then
      _type_filter = " AND TYPE IN (" & CageCurrencyType.Bill & ", " & CageCurrencyType.Coin & ", " & CageCurrencyType.Others & ") "
    ElseIf FeatureChips.IsChipsType(CType(IsoType, CageCurrencyType)) Then
      _type_filter = " AND TYPE = '" & IsoType & "' "
      If Me.rb_send.Checked And Me.rb_gaming_table.Checked Then
        _type_filter = _type_filter & " " & m_chips_set_filter
      End If
    Else
      _type_filter = " AND TYPE = '" & IsoType & "' "
    End If

    If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.TEMPLATE_MAINTENANCE Then
      _type_filter &= " AND DENOMINATION >= 0 "
    End If

    _order_by = "TYPE ASC, ISO_CODE ASC, CHIP_SET_ID ASC, DENOMINATION DESC, CHIP_ID ASC"

    _rows = Me.m_cage_cls.TableCurrencies.Select("ISO_CODE = '" & IsoCode & "' " & _type_filter & " ", _order_by)

    _dt_Deno_Colors = GetAllDenominationColor()

    If FeatureChips.IsCurrencyExchangeTypeChips(IsoType) Then
      DataGrid.Column(GRID_CURRENCY_COLUMN_CURRENCY_TYPE_DESCRIPTION).Width = 0
      DataGrid.Column(GRID_CURRENCY_COLUMN_CHIP_COLOR).Width = GRID_CURRENCY_COLUMN_CHIP_COLOR_WIDTH

      If IsoType = FeatureChips.ChipType.COLOR Then
        DataGrid.Column(GRID_CURRENCY_COLUMN_ISO_CODE).Width = 0
        DataGrid.Column(GRID_CURRENCY_COLUMN_DENOMINATION).Width = 0
        DataGrid.Column(GRID_CURRENCY_COLUMN_DENOMINATION_WITH_SIMBOL).Width = 0
        DataGrid.Column(GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).Width = 0
        DataGrid.Column(GRID_CURRENCY_COLUMN_CASHIER_TOTAL).Width = 0
        DataGrid.Column(GRID_CURRENCY_COLUMN_TOTAL_DIFFERENCE).Width = 0
        DataGrid.Column(GRID_CURRENCY_COLUMN_CHIP_SET_NAME).Width = GRID_CURRENCY_COLUMN_CHIP_SET_NAME_WIDTH + 1920
        DataGrid.Column(GRID_CURRENCY_COLUMN_CHIP_DRAWING).Width = GRID_CURRENCY_COLUMN_CHIP_DRAW_WIDTH + 1920
        DataGrid.Column(GRID_CURRENCY_COLUMN_CHIP_NAME).Width = GRID_CURRENCY_COLUMN_CHIP_NAME_WIDTH + 1920
        DataGrid.Column(GRID_CURRENCY_COLUMN_QUANTITY).Width = GRID_CURRENCY_COLUMN_QUANTITY_WIDTH + 200
        DataGrid.Column(GRID_CURRENCY_COLUMN_CASHIER_QUANTITY).Width = GRID_CURRENCY_COLUMN_CASHIER_QUANTITY_WIDTH + 200
        DataGrid.Column(GRID_CURRENCY_COLUMN_QUANTITY_DIFFERENCE).Width = GRID_CURRENCY_COLUMN_QUANTITY_DIFFERENCE_WIDTH + 200
      End If

    Else
      DataGrid.Column(GRID_CURRENCY_COLUMN_CURRENCY_TYPE_DESCRIPTION).Width = GRID_CURRENCY_COLUMN_CURRENCY_TYPE_DESCRIPTION_WIDTH + 200
      DataGrid.Column(GRID_CURRENCY_COLUMN_DENOMINATION_WITH_SIMBOL).Width = GRID_CURRENCY_COLUMN_DENOMINATION_WITH_SIMBOL_WIDTH + 3200
      DataGrid.Column(GRID_CURRENCY_COLUMN_CHIP_SET_NAME).Width = 0
      DataGrid.Column(GRID_CURRENCY_COLUMN_CHIP_COLOR).Width = 0
      DataGrid.Column(GRID_CURRENCY_COLUMN_CHIP_DRAWING).Width = 0
      DataGrid.Column(GRID_CURRENCY_COLUMN_CHIP_NAME).Width = 0
    End If

    For Each _row In _rows

      _denomination = _row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.DENOMINATION)
      IsoType = _row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CURRENCY_TYPE)

      If ((_currency_iso_code <> IsoCode) And (_denomination <> -1 And _denomination <> -2)) Or (_currency_iso_code = IsoCode) Then

        If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE AndAlso _
        FeatureChips.IsCurrencyExchangeTypeChips(IsoType) AndAlso _denomination = Cage.COINS_CODE Then

          Continue For
        End If

        '11-NOV-2014 HBB: denomination among 0-1 should not be shown when is tito terminal collection
        If Me.m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal Then
          Call SetCurrencyDataFromTerminal(IsoCode, IsoType, DataGrid, _row_count, _row, _dt_Deno_Colors, _denomination)
          Continue For

        ElseIf Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT Then
          Call SetCurrencyDataNewMovement(IsoCode, IsoType, DataGrid, _row_count, _row, _dt_Deno_Colors, _denomination, _currency_iso_code, IsGamblingCollection, IsCustomCollection)

        Else
          Call SetCurrencyDataToCustom(IsoCode, IsoType, DataGrid, _row_count, _row, _dt_Deno_Colors, _denomination, _currency_iso_code)
        End If
      End If

    Next

    ' Dispose
    Call SafeDispose(_rows)
    Call SafeDispose(_dt_Deno_Colors)

  End Sub ' SetCurrencyGridData
#Region "SetCurrencyGridDataFunctions"

  Private Sub SetCurrencyDataFromTerminal(ByVal IsoCode As String _
                                     , ByVal IsoType As String _
                                     , ByRef DataGrid As uc_grid _
                                     , ByRef RowCount As Integer _
                                     , ByRef Row As DataRow _
                                     , ByRef DtDenoColors As DataTable _
                                     , ByRef denomination As Decimal)


    Dim _add_new_denomination_row As Boolean

    _add_new_denomination_row = AddNewDenominationRow(IsoCode, Row, DtDenoColors, denomination)

    If m_is_coin_collected_enabled Then
      If _add_new_denomination_row OrElse (TITO.Utils.IsTitoMode) _
        AndAlso (denomination = Cage.TICKETS_CODE OrElse denomination = Cage.COINS_COLLECTION_CODE) _
        OrElse Row(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CURRENCY_TYPE) = CageCurrencyType.Coin Then

        Call Me.LoadRowsValue(RowCount, Row, DataGrid, IsoCode, IsoType, DtDenoColors)
        RowCount += 1
      End If

    Else
      If Row(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CURRENCY_TYPE) = CageCurrencyType.Bill AndAlso _add_new_denomination_row _
           OrElse (TITO.Utils.IsTitoMode) _
          AndAlso (denomination = Cage.TICKETS_CODE) Then
        Call Me.LoadRowsValue(RowCount, Row, DataGrid, IsoCode, IsoType, DtDenoColors)

        RowCount += 1
      End If
    End If
  End Sub

  Private Function AddNewDenominationRow(ByVal IsoCode As String _
                                     , ByRef Row As DataRow _
                                     , ByRef DtDenoColors As DataTable _
                                     , ByRef Denomination As Decimal) As Boolean

    Dim _sql_condition As String
    Dim _dt_Rows As DataRow()
    Dim _add_row_denomination As Boolean

    _add_row_denomination = False

    If Me.m_cage_cls.DenominationAceptedByTerminals.Contains(Denomination) Then
      _add_row_denomination = True
    Else
      If (Row(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CURRENCY_TYPE) = CageCurrencyType.Bill) Then
        _sql_condition = " ISO_CODE = '" & IsoCode & "' AND DENOMINATION ='" & Denomination & "' AND BILL = 1"
      Else
        _sql_condition = " ISO_CODE = '" & IsoCode & "' AND DENOMINATION ='" & Denomination & "' AND COIN = 1"
      End If

      _dt_Rows = DtDenoColors.Select(_sql_condition, "")

      If _dt_Rows.Length > 0 Then
        _add_row_denomination = True
      End If
    End If

    Return _add_row_denomination
  End Function


  Private Sub SetCurrencyDataNewMovement(ByVal IsoCode As String _
                                     , ByVal IsoType As Int32 _
                                     , ByRef DataGrid As uc_grid _
                                     , ByRef RowCount As Integer _
                                     , ByRef Row As DataRow _
                                     , ByRef DtDenoColors As DataTable _
                                     , ByRef Denomination As Decimal _
                                     , ByRef CurrencyIsoCode As String _
                                     , Optional ByVal IsGamblingCollection As Boolean = False _
                                     , Optional ByVal IsCustomCollection As Boolean = False)

    If IsGamblingCollection Or (IsCustomCollection AndAlso Denomination > 0) Or (IsCustomCollection AndAlso IsoType = FeatureChips.ChipType.COLOR) Then
      If Denomination <> Cage.TICKETS_CODE Or (Denomination = Cage.TICKETS_CODE And CurrencyIsoCode = IsoCode) Then
        Call Me.LoadRowsValue(RowCount, Row, DataGrid, IsoCode, IsoType, DtDenoColors)
        RowCount += 1
      End If
    ElseIf IsCustomCollection Then
      If Denomination > 0 Or IsoType = FeatureChips.ChipType.COLOR Then
        Call Me.LoadRowsValue(RowCount, Row, DataGrid, IsoCode, IsoType, DtDenoColors)
        RowCount += 1
      End If
    Else
      If Denomination > 0 Or _
        IsoCode.Equals(Cage.CHIPS_COLOR) Or _
        (Denomination = Cage.COINS_CODE) Then ' Just Bills And Coins

        Call Me.LoadRowsValue(RowCount, Row, DataGrid, IsoCode, IsoType, DtDenoColors)
        RowCount += 1

      ElseIf Denomination = Cage.TICKETS_CODE And CurrencyIsoCode = IsoCode And Me.m_collection_from_tito_terminal Then

        Call Me.LoadRowsValue(RowCount, Row, DataGrid, IsoCode, IsoType, DtDenoColors)
        RowCount += 1
      ElseIf (Denomination = Cage.BANK_CARD_CODE OrElse Denomination = Cage.CHECK_CODE) Then
        'if operation is sent to custom include bankcards and checks
        Call Me.LoadRowsValue(RowCount, Row, DataGrid, IsoCode, IsoType, DtDenoColors)
        RowCount += 1
      End If
    End If
  End Sub

  Private Sub SetCurrencyDataToCustom(ByVal IsoCode As String _
                                     , ByVal IsoType As String _
                                     , ByRef DataGrid As uc_grid _
                                     , ByRef RowCount As Integer _
                                     , ByRef Row As DataRow _
                                     , ByRef DtDenoColors As DataTable _
                                     , ByRef Denomination As Decimal _
                                     , ByRef CurrencyIsoCode As String)

    If Me.m_cage_cls.OperationType = Cage.CageOperationType.ToCustom Then

      Call Me.LoadRowsValue(RowCount, Row, DataGrid, IsoCode, IsoType, DtDenoColors)
      RowCount += 1

    ElseIf Me.m_cage_cls.OperationType <> Cage.CageOperationType.RequestOperation Then
      ' 09-NOV-2015 FOS & JPJ    Fixed Bug TFS-6228: Totals don't appear
      If m_is_coin_collected_enabled Then
        If Denomination <> Cage.TICKETS_CODE AndAlso Denomination <> Cage.COINS_CODE _
                                            Or (Denomination = Cage.TICKETS_CODE _
                                                  AndAlso CurrencyIsoCode = IsoCode _
                                                  AndAlso Me.m_cage_cls.OperationType <> Cage.CageOperationType.FromGamingTable _
                                                  AndAlso Me.m_cage_cls.OperationType <> Cage.CageOperationType.FromGamingTableClosing _
                                                  AndAlso Me.m_cage_cls.OperationType <> Cage.CageOperationType.ToGamingTable _
                                                  AndAlso Me.m_cage_cls.OperationType <> Cage.CageOperationType.ToCashier) Then
          Call Me.LoadRowsValue(RowCount, Row, DataGrid, IsoCode, IsoType, DtDenoColors)
          RowCount += 1
        End If
      Else
        If Denomination <> Cage.TICKETS_CODE AndAlso Denomination <> Cage.COINS_COLLECTION_CODE _
                                              Or (Denomination = Cage.TICKETS_CODE _
                                                    AndAlso CurrencyIsoCode = IsoCode _
                                                  AndAlso Me.m_cage_cls.OperationType <> Cage.CageOperationType.FromGamingTable _
                                                  AndAlso Me.m_cage_cls.OperationType <> Cage.CageOperationType.FromGamingTableClosing _
                                                  AndAlso Me.m_cage_cls.OperationType <> Cage.CageOperationType.ToGamingTable _
                                                  AndAlso Me.m_cage_cls.OperationType <> Cage.CageOperationType.ToCashier) Then
          Call Me.LoadRowsValue(RowCount, Row, DataGrid, IsoCode, IsoType, DtDenoColors)
          RowCount += 1
        End If
      End If
    ElseIf Denomination > 0 Or Denomination = Cage.COINS_CODE Or IsoType = FeatureChips.ChipType.COLOR Then
      Call Me.LoadRowsValue(RowCount, Row, DataGrid, IsoCode, IsoType, DtDenoColors)
      RowCount += 1
    End If

  End Sub
#End Region ' SetCurrencyGridDataFunctions



  ' PURPOSE: Handler to control CellDataChange in the grids
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row
  '           - Column
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub CellDataChangeHandler(ByVal Row As Integer, ByVal Column As Integer)
    If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE Then
      Call CellDataChangeHandler(Row, Column, False, Nothing, True)
    Else
      Call CellDataChangeHandler(Row, Column, False, Nothing, False)
    End If

    'Bug 21727:Edici�n de Mesa de Juego: error al pulsar "Plantilla" -> Ya no deja realizar ninguna operaci�n
    'Prevents the Enter from being called if opened in this mode
    If Me.m_cage_cls.OpenMode <> CLASS_CAGE_CONTROL.OPEN_MODE.TEMPLATE_MAINTENANCE Then
      SendKeys.Send("{ENTER}")
    End If

  End Sub
  Private Sub CellDataChangeHandler(ByVal Row As Integer, ByVal Column As Integer, ByVal IsFromBillCounter As Boolean, ByRef DataGrid As uc_grid, Optional ByVal IsClearGrid As Boolean = False)
    Dim _total_before_edit As Decimal
    Dim _total_after_edit As Decimal
    Dim _iso_code As String
    Dim _currency_type As CageCurrencyType
    Dim _is_null As Boolean
    Dim _cell_data_value As Decimal
    Dim _is_cash As Boolean
    Dim _datagrid As uc_grid

    m_changed_values = True
    m_force_grid_clear = False

    If DataGrid Is Nothing Then
      If IsFromBillCounter Then
        _datagrid = Me.tb_currency.TabPages(0).Controls(0)
      Else
        _datagrid = Me.tb_currency.SelectedTab.Controls.Item(0)
      End If
    Else
      _datagrid = DataGrid
    End If

    _iso_code = _datagrid.Cell(Row, GRID_CURRENCY_COLUMN_ISO_CODE).Value.Substring(0, 3)
    _currency_type = _datagrid.Cell(Row, GRID_CURRENCY_COLUMN_CURRENCY_TYPE).Value
    _is_cash = False

    RemoveHandler _datagrid.CellDataChangedEvent, AddressOf CellDataChangeHandler
    Call Me.StepNextRow(_datagrid, Column)

    If Column = GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL Then ' Check if selected column is total column.
      _is_cash = True
      _cell_data_value = 1
      _total_before_edit = Me.m_total_before_edit
      _total_after_edit = Me.m_cage_cls.GetRowValue(_datagrid.Cell(Row, Column).Value)

      If String.IsNullOrEmpty(_datagrid.Cell(Row, Column).Value) Then ' Empty cell value
        _is_null = True
        _cell_data_value = 0
        _total_after_edit = 0
      ElseIf _datagrid.Cell(Row, Column).Value <= 0 Then ' Negative value
        _cell_data_value = 0
        _total_after_edit = 0
      End If

      If _datagrid.Cell(Row, GRID_CURRENCY_COLUMN_DENOMINATION).Value < 0 Then ' Money Row
        If (Not _is_null) Then
          _datagrid.Cell(Row, GRID_CURRENCY_COLUMN_TOTAL).Value = _total_after_edit
          If _datagrid.Cell(Row, GRID_CURRENCY_COLUMN_DENOMINATION).Value <> Cage.COINS_CODE Then
            _datagrid.Cell(Row, GRID_CURRENCY_COLUMN_QUANTITY).Value = _cell_data_value
          End If
          _datagrid.Cell(Row, GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).Value = Me.m_cage_cls.FormatAmount(_total_after_edit, False, _iso_code)
        Else
          _datagrid.Cell(Row, GRID_CURRENCY_COLUMN_TOTAL).Value = String.Empty
          _datagrid.Cell(Row, GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).Value = String.Empty
          _datagrid.Cell(Row, GRID_CURRENCY_COLUMN_QUANTITY).Value = String.Empty
        End If

      ElseIf Me.m_show_denominations = Cage.ShowDenominationsMode.HideAllDenominations Then
        _datagrid.Cell(Row, GRID_CURRENCY_COLUMN_QUANTITY).Value = _total_after_edit
        _datagrid.Cell(Row, GRID_CURRENCY_COLUMN_TOTAL).Value = _total_after_edit
      End If
    Else
      ' Other rows
      _is_null = False
      If String.IsNullOrEmpty(_datagrid.Cell(Row, GRID_CURRENCY_COLUMN_QUANTITY).Value) Then ' Empty cell value
        _datagrid.Cell(Row, GRID_CURRENCY_COLUMN_QUANTITY).Value = String.Empty
        _is_null = True
        ' 25-SEP-2014 JPJ Fixed Bug WIGOSTITO 1284: Deleting all the elements of the grid shows an error
      ElseIf _datagrid.Cell(Row, GRID_CURRENCY_COLUMN_QUANTITY).Value < 0 Then ' Negative value
        _datagrid.Cell(Row, GRID_CURRENCY_COLUMN_QUANTITY).Value = String.Empty
      End If

      ' Value introduced in datagrid
      _cell_data_value = Me.m_cage_cls.GetRowValue(_datagrid.Cell(Row, Column).Value)
      ' Total before edit
      _total_before_edit = Me.m_cage_cls.GetRowValue(_datagrid.Cell(Row, GRID_CURRENCY_COLUMN_TOTAL).Value)
      ' Denomination * Quantity
      If _currency_type = FeatureChips.ChipType.COLOR Then
        _total_after_edit = _cell_data_value
      Else
        _total_after_edit = _datagrid.Cell(Row, GRID_CURRENCY_COLUMN_DENOMINATION).Value * _cell_data_value
      End If

      If _datagrid.Cell(Row, GRID_CURRENCY_COLUMN_DENOMINATION).Value = Cage.TICKETS_CODE Then
        _datagrid.Cell(Row, GRID_CURRENCY_COLUMN_TOTAL).Value = String.Empty
        _datagrid.Cell(Row, GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).Value = String.Empty
      Else
        If _is_null Then
          _datagrid.Cell(Row, GRID_CURRENCY_COLUMN_TOTAL).Value = String.Empty
          _datagrid.Cell(Row, GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).Value = String.Empty
        Else
          _datagrid.Cell(Row, GRID_CURRENCY_COLUMN_TOTAL).Value = _total_after_edit
          If _currency_type = FeatureChips.ChipType.COLOR Then
            _datagrid.Cell(Row, GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).Value = FormatNumber(_total_after_edit)
          Else
            _datagrid.Cell(Row, GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).Value = Me.m_cage_cls.FormatAmount(_total_after_edit, False, _iso_code)
          End If
        End If
      End If

      If _currency_type = CageCurrencyType.ChipsRedimible Or _currency_type = CageCurrencyType.ChipsNoRedimible Or _currency_type = CageCurrencyType.ChipsColor Then
        _is_cash = True
      End If
    End If

    Call Me.SaveCurrenciesQuantities(Row, _iso_code, _currency_type, _datagrid)

    If _datagrid.Cell(Row, GRID_CURRENCY_COLUMN_DENOMINATION).Value <> Cage.TICKETS_CODE Then
      'Call Me.SetTotalGrid(_total_after_edit, _total_before_edit, _is_coin, _iso_code)
      Call Me.RefreshTotalGrid(_is_cash, _iso_code, _currency_type)

      If Not IsClearGrid AndAlso m_is_coin_collected_enabled Then
        Call SetCurrencyGridData(_iso_code, _currency_type, _datagrid)
        _datagrid.CurrentRow = Row + 1
        _datagrid.CurrentCol = GRID_CURRENCY_COLUMN_QUANTITY
      End If
    End If

    Call Me.SetDifference(_datagrid, Row, _iso_code, _currency_type)
    Call Me.SetRowColor(Row, _datagrid)

    AddHandler _datagrid.CellDataChangedEvent, AddressOf CellDataChangeHandler

  End Sub ' CellDataChangeHandler

  ' PURPOSE: Step to the next row
  '
  '  PARAMS:
  '     - INPUT:
  '           - DataGrid
  '           - Column
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub StepNextRow(ByVal DataGrid As uc_grid, ByVal Column As Integer)
    Dim _index As Integer
    Dim _value As String
    Dim _denom As String

    _value = DataGrid.Cell(DataGrid.CurrentRow, GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).Value
    _index = DataGrid.CurrentRow + 1

    If _index > DataGrid.NumRows - 1 Then ' Return to first row
      _index = 0
    End If

    _denom = DataGrid.Cell(_index, GRID_CURRENCY_COLUMN_DENOMINATION).Value ' Get next denomination

    If _denom < 0 Then ' Bank card, check, coins and tickets

      If _denom = Cage.TICKETS_CODE Then ' Tickets
        If Column = GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL Then
          Column -= 2
        End If
      Else ' Bank card, check, coins
        If Column = GRID_CURRENCY_COLUMN_QUANTITY Then
          Column += 2
        End If
      End If

    Else ' Bills
      If Column = GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL Then
        Column -= 2
      End If
    End If

    'DataGrid.TopRow = IIf(_index = 0, _index, _index - 1) ' Set the scroll position
    If (DataGrid.CurrentRow = DataGrid.BottomRow) Then
      If (DataGrid.CurrentRow = DataGrid.NumRows - 1) Then
        DataGrid.TopRow = 0
      Else
        DataGrid.TopRow = DataGrid.BottomRow
      End If
    End If

    DataGrid.IsSelected(DataGrid.CurrentRow) = False
    DataGrid.Cell(DataGrid.CurrentRow, GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).Value = _value
    DataGrid.IsSelected(_index) = True
    DataGrid.CurrentCol = Column
    DataGrid.CurrentRow = _index

  End Sub ' StepNextRow

  ' PURPOSE: To update differences
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row
  '           - Column
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDifference(ByVal DataGrid As uc_grid, ByVal Row As Integer, ByVal IsoCode As String, ByVal IsoType As Int32)

    Dim _rows As DataRow()
    Dim _denomination As Decimal
    Dim _result_quantity As Decimal
    Dim _result_total As Decimal
    Dim _chip_color As String
    Dim _chip_set_name As String
    Dim _chip_name As String
    Dim _chip_drawing As String
    Dim _currency_type As CageCurrencyType
    Dim _str_filter As String = ""
    Dim _quantity As String = ""
    Dim _quantity_expected As String = ""
    Dim _total As String = ""
    Dim _total_expected As String = ""
    Dim _isCountCage As Boolean
    Dim _total_coins_amount As Decimal
    Dim _chip_id As Int64
    Dim _chip_set_id As Int64

    _rows = Nothing

    If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MOVEMENT _
       Or Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CLOSE_CAGE _
       Or Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE _
       Or Me.m_gaming_values _
       Or Me.m_is_tito_collection Then

      _denomination = DataGrid.Cell(Row, GRID_CURRENCY_COLUMN_DENOMINATION).Value
      _quantity = DataGrid.Cell(Row, GRID_CURRENCY_COLUMN_QUANTITY).Value
      _quantity_expected = DataGrid.Cell(Row, GRID_CURRENCY_COLUMN_CASHIER_QUANTITY).Value
      _total = DataGrid.Cell(Row, GRID_CURRENCY_COLUMN_TOTAL).Value
      _total_expected = DataGrid.Cell(Row, GRID_CURRENCY_COLUMN_CASHIER_TOTAL).Value
      _chip_color = DataGrid.Cell(Row, GRID_CURRENCY_COLUMN_CHIP_COLOR).Value
      _chip_set_name = DataGrid.Cell(Row, GRID_CURRENCY_COLUMN_CHIP_SET_NAME).Value
      _chip_name = DataGrid.Cell(Row, GRID_CURRENCY_COLUMN_CHIP_NAME).Value
      _chip_drawing = DataGrid.Cell(Row, GRID_CURRENCY_COLUMN_CHIP_DRAWING).Value
      _currency_type = DataGrid.Cell(Row, GRID_CURRENCY_COLUMN_CURRENCY_TYPE).Value
      If FeatureChips.IsCageCurrencyTypeChips(_currency_type) Then
        _chip_id = DataGrid.Cell(Row, GRID_CURRENCY_COLUMN_CHIP_ID).Value
        _chip_set_id = DataGrid.Cell(Row, GRID_CURRENCY_COLUMN_CHIP_SET_ID).Value
      End If

      If _denomination = Cage.COINS_COLLECTION_CODE Then
        DataGrid.Cell(Row, GRID_CURRENCY_COLUMN_QUANTITY_DIFFERENCE).Value = String.Empty

        If _total_expected = String.Empty Then
          _total_expected = "0"
        End If

        If _total = String.Empty Then
          _total = "0"
        End If

        If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT Then
          _result_total = _total
        Else
          _total_coins_amount = GetTotalCoinCollection(IsoCode, True)
          _result_total = _total_coins_amount
        End If

        Try ' Exception control  when floordualcurrency is activated
          _result_total = _result_total - _total_expected
        Catch ex As Exception
          _result_total = _result_total - Me.GetRawNumber(_total_expected, IsoCode)
        End Try



        DataGrid.Cell(Row, GRID_CURRENCY_COLUMN_TOTAL_DIFFERENCE).Value = Me.m_cage_cls.FormatAmount(_result_total, False, IsoCode, True)


      Else
        _isCountCage = (Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE)

        If (_isCountCage And ((_quantity = String.Empty And Not _denomination = Cage.COINS_CODE) Or _
                                      ((_total = String.Empty Or Me.m_initial_load) And _denomination = Cage.COINS_CODE))) Or _
            (Not _isCountCage And String.IsNullOrEmpty(_quantity) And String.IsNullOrEmpty(_total_expected)) Then
          _result_quantity = Me.m_cage_cls.GetRowValue(_quantity)
          _result_total = Me.m_cage_cls.GetRowValue(_total)
        Else
          _result_quantity = Me.m_cage_cls.GetRowValue(Me.GetRawNumber(_quantity, IsoCode)) - Me.m_cage_cls.GetRowValue(Me.GetRawNumber(_quantity_expected, IsoCode))
          _result_total = Me.m_cage_cls.GetRowValue(Me.GetRawNumber(_total, IsoCode)) - Me.m_cage_cls.GetRowValue(Me.GetRawNumber(_total_expected, IsoCode))
        End If

        ' Update DataGrid (Quantity difference)
        If _denomination <> Cage.COINS_CODE Then

          If ((Not String.IsNullOrEmpty(_quantity) And _isCountCage) Or _
             (Not String.IsNullOrEmpty(_total_expected) And Not _isCountCage)) Or _
              Not m_show_expected_data Then
            DataGrid.Cell(Row, GRID_CURRENCY_COLUMN_QUANTITY_DIFFERENCE).Value = GUI_FormatNumber(_result_quantity, 0)
            DataGrid.Cell(Row, GRID_CURRENCY_COLUMN_TOTAL_DIFFERENCE).Value = Me.m_cage_cls.FormatAmount(_result_total, False, IsoCode, True)

          Else
            DataGrid.Cell(Row, GRID_CURRENCY_COLUMN_QUANTITY_DIFFERENCE).Value = DataGrid.Cell(Row, GRID_CURRENCY_COLUMN_QUANTITY).Value
            DataGrid.Cell(Row, GRID_CURRENCY_COLUMN_TOTAL_DIFFERENCE).Value = Me.m_cage_cls.FormatAmount(_result_total, False, IsoCode, False)
          End If
        Else
          If (Me.m_initial_load Or _total = String.Empty) Then
            DataGrid.Cell(Row, GRID_CURRENCY_COLUMN_QUANTITY_DIFFERENCE).Value = DataGrid.Cell(Row, GRID_CURRENCY_COLUMN_QUANTITY).Value
            DataGrid.Cell(Row, GRID_CURRENCY_COLUMN_TOTAL_DIFFERENCE).Value = Me.m_cage_cls.FormatAmount(_result_total, False, IsoCode, False)
          Else
            DataGrid.Cell(Row, GRID_CURRENCY_COLUMN_QUANTITY_DIFFERENCE).Value = GUI_FormatNumber(_result_quantity, 0)
            DataGrid.Cell(Row, GRID_CURRENCY_COLUMN_TOTAL_DIFFERENCE).Value = Me.m_cage_cls.FormatAmount(_result_total, False, IsoCode, True)
          End If
        End If

      End If

      ' Select the rows in the Datatable (This operation should return just 1 row)
      If Me.m_cage_cls.TableCurrencies IsNot Nothing Then

        If _currency_type = CageCurrencyType.Bill Then
          _str_filter = String.Format("ISO_CODE = '{0}' AND DENOMINATION = {1} AND TYPE IN ({2}, {3}) ", IsoCode, GUI_LocalNumberToDBNumber(_denomination.ToString()), CInt(_currency_type), CInt(CageCurrencyType.Coin))
        ElseIf FeatureChips.IsCurrencyExchangeTypeChips(_currency_type) Then
          _str_filter = String.Format("ISO_CODE = '{0}' AND TYPE = {1} AND CHIP_ID = {2}", IsoCode, CInt(_currency_type), _chip_id.ToString())
        Else
          _str_filter = String.Format("ISO_CODE = '{0}' AND DENOMINATION = {1} AND TYPE = {2} ", IsoCode, GUI_LocalNumberToDBNumber(_denomination.ToString()), CInt(_currency_type))
        End If

        _rows = Me.m_cage_cls.TableCurrencies.Select(_str_filter)

        If _rows.Length > 0 Then
          ' Update DataTable Currencies (Quantity difference)
          _rows(0).Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.QUANTITY_DIFFERENCE) = _result_quantity
          ' Update DataTable Currencies (Total difference)
          _rows(0).Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.TOTAL_DIFFERENCE) = _result_total
        End If
      End If
    End If

    ' Dispose
    Call SafeDispose(_rows)

  End Sub ' SetDifference

  ' PURPOSE: Handler to control BeforeStartEdition in the grids
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row
  '           - Column
  '           - EditionCanceled
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub BeforeStartEditionHandler(ByVal Row As System.Int32, ByVal Column As System.Int32, ByRef EditionCanceled As System.Boolean)
    Dim _datagrid As uc_grid
    Dim _denom_value As Decimal

    EditionCanceled = False
    _datagrid = Me.tb_currency.SelectedTab.Controls(0)
    _denom_value = _datagrid.Cell(Row, GRID_CURRENCY_COLUMN_DENOMINATION).Value


    If Column = GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL Then ' Total column
      ' If column edited is total, we need to save the previous value before call CellDataChangeHandler
      Me.m_total_before_edit = Me.m_cage_cls.GetRowValue(_datagrid.Cell(Row, GRID_CURRENCY_COLUMN_TOTAL).Value)

      If _denom_value >= 0 _
         Or _denom_value = Cage.TICKETS_CODE _
         Or _denom_value = Cage.COINS_COLLECTION_CODE Then ' Don't allow edition if Tab Chips
        EditionCanceled = True
      ElseIf _denom_value = Cage.COINS_CODE _
        Or _denom_value = Cage.BANK_CARD_CODE _
          Or _denom_value = Cage.CHECK_CODE Then

        EditionCanceled = False
      End If

      If Me.m_show_denominations = Cage.ShowDenominationsMode.HideAllDenominations _
        And FeatureChips.IsCurrencyExchangeTypeChips(_datagrid.Cell(Row, GRID_CURRENCY_COLUMN_CURRENCY_TYPE).Value) _
          And _denom_value = 1 _
            And (Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT _
              Or Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MOVEMENT _
                Or Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE) Then

        EditionCanceled = False
      End If

    ElseIf Column = GRID_CURRENCY_COLUMN_QUANTITY Then

      If _denom_value < 0 Then
        If _denom_value <> Cage.TICKETS_CODE Then
          EditionCanceled = True
        Else
          EditionCanceled = m_is_tito_collection
        End If

      ElseIf Me.m_show_denominations = Cage.ShowDenominationsMode.HideAllDenominations _
             And FeatureChips.IsCurrencyExchangeTypeChips(_datagrid.Cell(Row, GRID_CURRENCY_COLUMN_CURRENCY_TYPE).Value) _
             And _datagrid.Cell(Row, GRID_CURRENCY_COLUMN_CURRENCY_TYPE).Value <> FeatureChips.ChipType.COLOR _
             And _denom_value = 1 Then
        EditionCanceled = True
      End If

    End If

  End Sub ' BeforeStartEditionHandler

  ' PURPOSE: Handler to control BeforeStartEdition in the grids
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row
  '           - Column
  '           - EditionCanceled
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub BeforeStartConceptEditionHandler(ByVal Row As System.Int32, ByVal Column As System.Int32, ByRef EditionCanceled As System.Boolean)

    EditionCanceled = False

    If CType(Me.dg_concepts.Cell(Row, 1).Value, Boolean) And _
      (m_currencies_types.Rows(Column - GRID_CONCEPT_AMOUNT)("CGC_ISO_CODE") <> Me.m_national_currency Or FeatureChips.IsChipsType(CType(m_currencies_types.Rows(Column - GRID_CONCEPT_AMOUNT)("TYPE"), CageCurrencyType))) Then
      EditionCanceled = True
    End If

  End Sub ' BeforeStartConceptEditionHandler

  ' PURPOSE: To load values from database to the grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - RowCount
  '           - Row
  '           - DataGrid
  '           - IsoCode
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub LoadRowsValue(ByVal RowCount As Integer _
                          , ByVal Row As DataRow _
                          , ByRef DataGrid As uc_grid _
                          , ByVal IsoCode As String _
                          , ByVal IsoType As Int32 _
                          , ByVal DtDenoColors As DataTable)


    Dim _show_zero As Boolean
    Dim _denomination As Decimal
    Dim _currency_type As String
    Dim _currency_type_value As Integer
    Dim _total_row As Decimal
    Dim _chip_value_color As Color

    _show_zero = False
    _denomination = IIf(IsDBNull(Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.DENOMINATION)), 0, Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.DENOMINATION))
    _total_row = 0

    DataGrid.Redraw = False
    DataGrid.Redraw = True

    With DataGrid
      .AddRow()
      .Cell(RowCount, GRID_CURRENCY_COLUMN_ISO_CODE).Value = Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.ISO_CODE) ' Iso code
      .Cell(RowCount, GRID_CURRENCY_COLUMN_CURRENCY_TYPE).Value = Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CURRENCY_TYPE) ' Currency type
      .Cell(RowCount, GRID_CURRENCY_COLUMN_DENOMINATION).Value = _denomination ' Denomination
      _currency_type = CLASS_CAGE_CONTROL.GetCurrencyTypeDescription(Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CURRENCY_TYPE))
      .Cell(RowCount, GRID_CURRENCY_COLUMN_CURRENCY_TYPE_DESCRIPTION).Value = _currency_type

      If m_row_height <= 0 Then
        m_row_height = .Row(0).Height
      End If

      If FeatureChips.IsChipsType(CType(Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CURRENCY_TYPE), CageCurrencyType)) Then
        _chip_value_color = Color.FromArgb(Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CHIP_COLOR))
        .Cell(RowCount, GRID_CURRENCY_COLUMN_CHIP_COLOR).BackColor = _chip_value_color   ' Chip color
        .Cell(RowCount, GRID_CURRENCY_COLUMN_CHIP_COLOR).ForeColor = _chip_value_color
        '.Cell(RowCount, GRID_CURRENCY_COLUMN_CHIP_COLOR).Value = _chip_value_color.Name
        .Cell(RowCount, GRID_CURRENCY_COLUMN_CHIP_SET_NAME).Value = Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CHIP_SET_NAME) ' Chip name

        ' Chip name
        If (IsDBNull(Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CHIP_NAME))) Then
          .Cell(RowCount, GRID_CURRENCY_COLUMN_CHIP_NAME).Value = ""
        Else
          .Cell(RowCount, GRID_CURRENCY_COLUMN_CHIP_NAME).Value = Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CHIP_NAME)
        End If

        ' Chip draw
        If (IsDBNull(Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CHIP_DRAWING))) Then
          .Cell(RowCount, GRID_CURRENCY_COLUMN_CHIP_DRAWING).Value = ""
        Else
          .Cell(RowCount, GRID_CURRENCY_COLUMN_CHIP_DRAWING).Value = Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CHIP_DRAWING)
        End If

        .Cell(RowCount, GRID_CURRENCY_COLUMN_CHIP_ID).Value = Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CHIP_ID) 'Chip Id
        .Cell(RowCount, GRID_CURRENCY_COLUMN_CHIP_SET_ID).Value = Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CHIP_SET_ID) 'Chip Set Id
      End If
      _currency_type_value = Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CURRENCY_TYPE)


      If IsDBNull(Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.TOTAL)) Then
        _total_row = 0
      Else
        _total_row = Convert.ToInt32(Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.TOTAL))
      End If

      If HideConceptsToGrid(_denomination) AndAlso _total_row <= 0 Then
        .Row(RowCount).Height = 0
      End If

      If _denomination = Cage.COINS_COLLECTION_CODE AndAlso _currency_type_value = CageCurrencyType.Others Then
        LoadRowsValueCoinsCode(DataGrid, RowCount, IsoCode, _denomination, _currency_type_value, Row)
      Else
        LoadRowsValueDefault(DataGrid, RowCount, IsoCode, _denomination, _currency_type_value, Row, DtDenoColors)
      End If

    End With

    Call Me.SetDifference(DataGrid, RowCount, IsoCode, _currency_type_value)
    Call Me.SetRowColor(RowCount, DataGrid)
  End Sub ' LoadRowsValue

  Private Function HideConceptsToGrid(ByVal Denomination As Decimal) As Boolean

    Dim _is_denomination_to_hide As Boolean
    Dim _is_operation_type_to_hide As Boolean

    _is_denomination_to_hide = False
    _is_operation_type_to_hide = False

    If (Denomination = Cage.TICKETS_CODE OrElse _
      Denomination = Cage.COINS_COLLECTION_CODE) Then
      _is_denomination_to_hide = True
    End If

    Select Case m_cage_cls.OperationType
      Case Cage.CageOperationType.ToGamingTable
      Case Cage.CageOperationType.ToCustom
      Case Cage.CageOperationType.RequestOperation
      Case Cage.CageOperationType.ToTerminal
      Case Cage.CageOperationType.ProgressiveProvision
      Case Cage.CageOperationType.ProgressiveAwarded
      Case Cage.CageOperationType.ProgressiveCancelProvision
      Case Cage.CageOperationType.ProgressiveCancelAwarded
      Case Cage.CageOperationType.ToCashier
        _is_operation_type_to_hide = True
      Case Else
        _is_operation_type_to_hide = False
    End Select

    Return _is_denomination_to_hide AndAlso _is_operation_type_to_hide

  End Function


  Private Sub LoadRowsValueCoinsCode(ByRef DtDetail As uc_grid, _
                                     ByVal RowCount As Integer, _
                                     ByVal IsoCode As String, _
                                     ByVal Denomination As Decimal, _
                                     ByVal CurrencyTypeValue As Integer, _
                                     ByRef Row As DataRow)



    Dim _show_zero As Boolean
    Dim _total_coins_amount As Decimal
    Dim _total_coins_informated As Decimal
    Dim _currency_symbol As String


    _total_coins_amount = 0
    _total_coins_informated = 0

    With DtDetail
      ' ShowDenominations Configuration
      .Cell(RowCount, GRID_CURRENCY_COLUMN_DENOMINATION_WITH_SIMBOL).Value _
                    = Me.m_cage_cls.GetCurrencyDescription(IsoCode, Denomination, CurrencyTypeValue) ' Coins, Cards, Check, Tickets


      .Cell(RowCount, GRID_CURRENCY_COLUMN_TOTAL).Value _
                = Me.m_cage_cls.GetRowValue(Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.TOTAL), True) ' Cage total
      'Column total with symbol
      If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT Then
        _total_coins_informated = IIf(Not IsDBNull(Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.TOTAL)), Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.TOTAL), 0)
        If _total_coins_informated = 0 Then
          _total_coins_informated = GetTotalCoinCollection(IsoCode, True)
        End If
        .Cell(RowCount, GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).Value _
                = Me.m_cage_cls.FormatAmount(_total_coins_informated, False, IsoCode)
      Else

        _total_coins_amount = GetTotalCoinCollection(IsoCode, True)
        If (_total_coins_amount > 0) Then
          .Cell(RowCount, GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).Value _
                 = Me.m_cage_cls.FormatAmount(_total_coins_amount, False, IsoCode)
          _currency_symbol = WSI.Common.CurrencyExchangeProperties.GetCurrencySymbol(.Cell(RowCount, GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).Value)
          Decimal.TryParse(.Cell(RowCount, GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).Value.Replace(_currency_symbol, ""), _total_coins_informated)
          .Cell(RowCount, GRID_CURRENCY_COLUMN_TOTAL_DIFFERENCE).Value _
                 = Me.m_cage_cls.FormatAmount(_total_coins_amount - _total_coins_informated, False, IsoCode)
        End If

        _show_zero = False

        If Not String.IsNullOrEmpty(.Cell(RowCount, GRID_CURRENCY_COLUMN_QUANTITY_DIFFERENCE).Value) Then
          _show_zero = True
        End If

        'Column Total difference
        .Cell(RowCount, GRID_CURRENCY_COLUMN_TOTAL_DIFFERENCE).Value _
                  = Me.m_cage_cls.FormatAmount(Me.m_cage_cls.GetRowValue(Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CASHIER_TOTAL)) - _total_coins_informated, False, IsoCode, False) ' Total difference
      End If

      'Colum cashier total
      .Cell(RowCount, GRID_CURRENCY_COLUMN_CASHIER_TOTAL).Value _
              = Me.m_cage_cls.FormatAmount(Me.m_cage_cls.GetRowValue(Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CASHIER_TOTAL)), False, IsoCode, _show_zero) ' Cashier total

      'Column quantity difference
      .Cell(RowCount, GRID_CURRENCY_COLUMN_QUANTITY_DIFFERENCE).Value _
             = String.Empty

      'column Cashier quantity
      .Cell(RowCount, GRID_CURRENCY_COLUMN_CASHIER_QUANTITY).Value _
           = String.Empty
    End With
  End Sub


  Private Sub LoadRowsValueDefault(ByRef DtDetail As uc_grid, _
                                   ByVal RowCount As Integer, _
                                   ByVal IsoCode As String, _
                                   ByVal Denomination As Decimal, _
                                   ByVal CurrencyTypeValue As Integer, _
                                   ByRef Row As DataRow, _
                                   ByRef DtDenoColors As DataTable)


    Dim _show_zero As Boolean
    Dim _valor_Color As Integer

    With DtDetail
      'CHIPS. Add Color
      If CurrencyTypeValue = FeatureChips.ChipType.RE Or CurrencyTypeValue = FeatureChips.ChipType.NR Or CurrencyTypeValue = FeatureChips.ChipType.COLOR Then
        _valor_Color = GetColorByDenomination(IsoCode, CurrencyTypeValue, Denomination, DtDenoColors)
        .Cell(RowCount, GRID_CURRENCY_COLUMN_INDEX).BackColor = System.Drawing.Color.FromArgb(_valor_Color)
      End If

      ' ShowDenominations Configuration
      If Denomination > 0 Or CurrencyTypeValue = FeatureChips.ChipType.COLOR Then

        If Me.m_show_denominations = Cage.ShowDenominationsMode.HideAllDenominations _
            AndAlso FeatureChips.IsCurrencyExchangeTypeChips(CurrencyTypeValue) _
              AndAlso Denomination = 1 Then
          .Cell(RowCount, GRID_CURRENCY_COLUMN_DENOMINATION_WITH_SIMBOL).Value _
                                    = Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.DENOMINATION_WITH_SIMBOL) ' Monto
        Else
          If CurrencyTypeValue = FeatureChips.ChipType.COLOR Then
            .Cell(RowCount, GRID_CURRENCY_COLUMN_DENOMINATION_WITH_SIMBOL).Value = Me.FormatNumber(Denomination) ' Bills
          Else
            .Cell(RowCount, GRID_CURRENCY_COLUMN_DENOMINATION_WITH_SIMBOL).Value _
                          = Me.m_cage_cls.FormatAmount(Denomination, True, IsoCode) ' Bills
          End If

        End If

      Else
        If Me.m_show_denominations <> Cage.ShowDenominationsMode.ShowAllDenominations _
        AndAlso Denomination = Cage.COINS_CODE Then

          If IsNumeric(Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.DENOMINATION_WITH_SIMBOL)) Then
            .Cell(RowCount, GRID_CURRENCY_COLUMN_DENOMINATION_WITH_SIMBOL).Value _
                          = Me.m_cage_cls.GetCurrencyDescription(IsoCode, Denomination) ' Coins, Cards, Check, Tickets
          Else
            .Cell(RowCount, GRID_CURRENCY_COLUMN_DENOMINATION_WITH_SIMBOL).Value _
                            = Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.DENOMINATION_WITH_SIMBOL) ' Monto
          End If

        Else
          .Cell(RowCount, GRID_CURRENCY_COLUMN_DENOMINATION_WITH_SIMBOL).Value _
                          = Me.m_cage_cls.GetCurrencyDescription(IsoCode, Denomination) ' Coins, Cards, Check, Tickets
        End If
      End If

      If CurrencyTypeValue = CageCurrencyType.Coin Then

        If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE Then
          .Cell(RowCount, GRID_CURRENCY_COLUMN_TOTAL).Value = String.Empty ' Cage total
        Else
          .Cell(RowCount, GRID_CURRENCY_COLUMN_TOTAL).Value = Me.m_cage_cls.GetRowValue(Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.TOTAL), True) ' Cage total
        End If

        .Cell(RowCount, GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).Value _
                  = Me.m_cage_cls.FormatAmount(Me.m_cage_cls.GetRowValue(Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.TOTAL), True), False, IsoCode, False)


        If (Me.m_cage_cls.GetRowValue(Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.QUANTITY)) > 0) Then
          .Cell(RowCount, GRID_CURRENCY_COLUMN_QUANTITY).Value _
            = Me.m_cage_cls.GetRowValue(Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.QUANTITY), True) ' Cage quantity
        End If

        .Cell(RowCount, GRID_CURRENCY_COLUMN_CASHIER_QUANTITY).Value _
                  = Me.FormatNumber(Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CASHIER_QUANTITY)) ' Cashier quantity

        .Cell(RowCount, GRID_CURRENCY_COLUMN_QUANTITY_DIFFERENCE).Value _
          = Me.FormatNumber(Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.QUANTITY_DIFFERENCE)) ' Quantity difference

      Else
        If Denomination <> Cage.TICKETS_CODE Then

          If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE Then
            .Cell(RowCount, GRID_CURRENCY_COLUMN_TOTAL).Value = String.Empty ' Cage total
          Else
            .Cell(RowCount, GRID_CURRENCY_COLUMN_TOTAL).Value = Me.m_cage_cls.GetRowValue(Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.TOTAL), True) ' Cage total
          End If

          If CurrencyTypeValue = FeatureChips.ChipType.COLOR Then
            .Cell(RowCount, GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).Value = Me.FormatNumber(Me.m_cage_cls.GetRowValue(Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.TOTAL), True))
          Else
            .Cell(RowCount, GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).Value _
                      = Me.m_cage_cls.FormatAmount(Me.m_cage_cls.GetRowValue(Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.TOTAL), True), False, IsoCode, False)
          End If

        End If

        If Denomination <> Cage.COINS_CODE Then
          If (Me.m_cage_cls.GetRowValue(Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.QUANTITY)) > 0) Then
            .Cell(RowCount, GRID_CURRENCY_COLUMN_QUANTITY).Value _
              = Me.m_cage_cls.GetRowValue(Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.QUANTITY), True) ' Cage quantity
          End If

          .Cell(RowCount, GRID_CURRENCY_COLUMN_CASHIER_QUANTITY).Value _
                    = Me.FormatNumber(Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CASHIER_QUANTITY)) ' Cashier quantity

          .Cell(RowCount, GRID_CURRENCY_COLUMN_QUANTITY_DIFFERENCE).Value _
            = Me.FormatNumber(Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.QUANTITY_DIFFERENCE)) ' Quantity difference
        End If

      End If

      If (Denomination <> Cage.COINS_CODE) And (Not String.IsNullOrEmpty(.Cell(RowCount, GRID_CURRENCY_COLUMN_CASHIER_QUANTITY).Value) _
            Or (String.IsNullOrEmpty(.Cell(RowCount, GRID_CURRENCY_COLUMN_CASHIER_QUANTITY).Value) And Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE)) Then
        _show_zero = True
        .Cell(RowCount, GRID_CURRENCY_COLUMN_CASHIER_QUANTITY).Value _
                       = Me.FormatNumber(Me.m_cage_cls.GetRowValue(Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CASHIER_QUANTITY))) ' Cashier total
      End If

      'If (Denomination = Cage.COINS_CODE) Then
      '  .Cell(RowCount, GRID_CURRENCY_COLUMN_CASHIER_QUANTITY).Value _
      '  = Me.m_cage_cls.FormatAmount(Me.m_cage_cls.GetRowValue(Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CASHIER_QUANTITY)), False, IsoCode, False) ' Cashier total
      'End If

      If Denomination = Cage.TICKETS_CODE Then
        .Cell(RowCount, GRID_CURRENCY_COLUMN_CASHIER_TOTAL).Value _
                = Me.m_cage_cls.FormatAmount(Me.m_cage_cls.GetRowValue(Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CASHIER_TOTAL)), False, IsoCode, False) ' Cashier total
      Else
        If CurrencyTypeValue = FeatureChips.ChipType.COLOR Then
          .Cell(RowCount, GRID_CURRENCY_COLUMN_CASHIER_TOTAL).Value _
                              = Me.FormatNumber(Me.m_cage_cls.GetRowValue(Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CASHIER_TOTAL))) ' Cashier total
        Else
          .Cell(RowCount, GRID_CURRENCY_COLUMN_CASHIER_TOTAL).Value _
                        = Me.m_cage_cls.FormatAmount(Me.m_cage_cls.GetRowValue(Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CASHIER_TOTAL)), False, IsoCode, _show_zero) ' Cashier total
        End If

      End If
      _show_zero = False
      If Not String.IsNullOrEmpty(.Cell(RowCount, GRID_CURRENCY_COLUMN_QUANTITY_DIFFERENCE).Value) Then
        _show_zero = True
      End If
      If Denomination = Cage.TICKETS_CODE Then
        .Cell(RowCount, GRID_CURRENCY_COLUMN_TOTAL_DIFFERENCE).Value _
                = Me.m_cage_cls.FormatAmount(Me.m_cage_cls.GetRowValue(Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.TOTAL_DIFFERENCE)), False, IsoCode, False) ' Total difference
      ElseIf CurrencyTypeValue = FeatureChips.ChipType.COLOR Then
        .Cell(RowCount, GRID_CURRENCY_COLUMN_TOTAL_DIFFERENCE).Value _
                = Me.FormatNumber(Me.m_cage_cls.GetRowValue(Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.TOTAL_DIFFERENCE))) ' Total difference
      Else
        .Cell(RowCount, GRID_CURRENCY_COLUMN_TOTAL_DIFFERENCE).Value _
                = Me.m_cage_cls.FormatAmount(Me.m_cage_cls.GetRowValue(Row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.TOTAL_DIFFERENCE)), False, IsoCode, _show_zero) ' Total difference
      End If

    End With
    Call Me.SetDifference(DtDetail, RowCount, IsoCode, CurrencyTypeValue)
    Call Me.SetRowColor(RowCount, DtDetail)
  End Sub


  Private Function GetTotalCoinCollection(ByRef IsoCode As String, Optional IsEditMode As Boolean = False) As Decimal

    Dim _totalCoincollectionAmount_obj As Object
    Dim _totalCoincollectionAmount As Decimal
    Dim _str_filter As String
    Dim _row_count As DataRow()

    _totalCoincollectionAmount_obj = 0
    _totalCoincollectionAmount = 0
    _str_filter = "ISO_CODE = '" & IsoCode & "' AND DENOMINATION >= 0 AND TYPE= 1 "

    _row_count = Me.m_cage_cls.TableCurrencies.Select(_str_filter)

    If _row_count.Length > 0 Then
      If IsEditMode Then
        _totalCoincollectionAmount_obj = Me.m_cage_cls.TableCurrencies.Compute("Sum(TOTAL)", _str_filter)
      Else
        _totalCoincollectionAmount_obj = Me.m_cage_cls.TableCurrencies.Compute("Sum(EXPECTED_COLUMN)", _str_filter)
      End If
    End If

    _totalCoincollectionAmount = IIf(IsDBNull(_totalCoincollectionAmount_obj), 0, _totalCoincollectionAmount_obj)

    Return _totalCoincollectionAmount
  End Function ' GetTotalCoinCollection 
  '
  '  PARAMS:
  '     - INPUT: 
  '           - Value of Denomination
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - Value of Color formatting en integer
  Private Function GetAllDenominationColor() As DataTable
    Dim currency_data As New cls_cage_amounts
    Dim _dt As New DataTable

    Try

      Using _db_trx As New DB_TRX()

        _dt = currency_data.GetCageDenominations_CurrenciesAndChips(_db_trx)

      End Using

    Catch ex As Exception

      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            Me.Name, _
                            "GetAllDenominationColor", _
                            ex.Message)

      ' Exception error
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(137), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ex.Message)

    Finally

    End Try

    Call SafeDispose(currency_data)

    Return _dt

  End Function ' GetAllDenominationColor

  ' PURPOSE: Get Color by Denomination 
  '
  '  PARAMS:
  '     - INPUT: 
  '           - Value of Denomination
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - Value of Color formatting en integer
  Private Function GetColorByDenomination(ByRef IsoCode As String, ByRef IsoType As Int32, ByVal Denomination As Decimal, ByVal dt As DataTable) As Integer
    Dim _return As Integer
    Dim _row As DataRow()

    'DLL11111
    '15-OCT-2014 JPJ Fixed Bug WIG-1500: Exception occurs when loading a template and language is set to spanish.
    If IsoType = CageCurrencyType.Bill Then
      _row = dt.Select("ISO_CODE = '" & IsoCode & "' AND CAGE_CURRENCY_TYPE IN (" & IsoType & ", " & CageCurrencyType.Coin & ") AND DENOMINATION = " & GUI_LocalNumberToDBNumber(Denomination.ToString))
    Else
      _row = dt.Select("ISO_CODE = '" & IsoCode & "' AND CAGE_CURRENCY_TYPE = '" & IsoType & "' AND DENOMINATION = " & GUI_LocalNumberToDBNumber(Denomination.ToString))
    End If

    If _row.Length > 0 Then
      _return = _row(0).Item(4)
    Else
      _return = 0
    End If

    ' Dispose
    Call SafeDispose(_row)

    Return _return

  End Function ' GetColorByDenomination

  ' PURPOSE: Sets Format Number
  '
  '  PARAMS:
  '     - INPUT:
  '           - Value
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Number formated as string
  Private Function FormatNumber(ByVal Value As Object) As String
    FormatNumber = ""

    If Not Value Is DBNull.Value Then
      FormatNumber = GUI_FormatNumber(Value, 0)
    End If

    Return FormatNumber
  End Function ' FormatNumber

  Private Sub ShowHideExpectedColumns()
    Dim _hide_totals As Boolean
    Dim _hide_currencies As Boolean

    _hide_totals = False
    _hide_currencies = False

    Select Case m_cage_cls.OperationType
      Case Cage.CageOperationType.ToCustom, _
           Cage.CageOperationType.FromCustom, _
           Cage.CageOperationType.ToGamingTable, _
           Cage.CageOperationType.ToCashier, _
           Cage.CageOperationType.ToTerminal
        _hide_totals = True
        _hide_currencies = True

      Case Cage.CageOperationType.FromCashier, _
           Cage.CageOperationType.FromCashierClosing, _
           Cage.CageOperationType.RequestOperation, _
           Cage.CageOperationType.CountCage, _
           Cage.CageOperationType.GainQuadrature, _
           Cage.CageOperationType.LostQuadrature
        _hide_totals = False
        _hide_currencies = False

      Case Cage.CageOperationType.FromGamingTable, _
           Cage.CageOperationType.FromGamingTableClosing, _
        Cage.CageOperationType.FromGamingTableDropboxWithExpected
        _hide_totals = Not GeneralParam.GetBoolean("GamingTable.PlayerTracking", "Enabled", False)
        _hide_currencies = _hide_totals

      Case Cage.CageOperationType.FromGamingTableDropbox
        _hide_totals = True
        _hide_currencies = True

      Case Cage.CageOperationType.FromTerminal
        _hide_totals = (m_gp_note_collection_type = Cage.NoteAcCollectionType.HideAll AndAlso Not m_cage_cls.m_type_new_collection.already_collected)
        _hide_currencies = (m_gp_note_collection_type <> Cage.NoteAcCollectionType.ShowAll AndAlso Not m_cage_cls.m_type_new_collection.already_collected)

    End Select

    If Not m_show_expected_data OrElse _hide_totals Then
      If Me.m_cage_cls.Status <> Cage.CageStatus.FinishedCountCage Then
        GRID_TOTAL_COLUMN_TOTAL_CASHIER_WITH_SIMBOL_WIDTH = 0
      Else
        GRID_TOTAL_COLUMN_BILLS_WITH_SIMBOL_WIDTH = 0
        GRID_TOTAL_COLUMN_COINS_WITH_SIMBOL_WIDTH = 0
        GRID_TOTAL_COLUMN_TOTAL_WITH_SIMBOL_WIDTH = 0
      End If
      GRID_TOTAL_COLUMN_DIFFERENCE_TOTAL_WITH_SIMBOL_WIDTH = 0

    Else
      GRID_TOTAL_COLUMN_TOTAL_CASHIER_WITH_SIMBOL_WIDTH = 1780
      GRID_TOTAL_COLUMN_DIFFERENCE_TOTAL_WITH_SIMBOL_WIDTH = 1780

    End If

    If Not m_show_expected_data OrElse _hide_currencies Then
      ' Expected
      If m_cage_cls.Status <> Cage.CageStatus.FinishedCountCage Then
        GRID_CURRENCY_COLUMN_CASHIER_QUANTITY_WIDTH = 0
        GRID_CURRENCY_COLUMN_CASHIER_TOTAL_WIDTH = 0
      Else
        'LEM: It's necessary because when m_cage_cls.Status = Cage.CageStatus.FinishedCountCage, "collected" columns  and "expected" columns are swapped
        GRID_CURRENCY_COLUMN_QUANTITY_WIDTH = 0
        GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL_WIDTH = 0
      End If
      ' Difference
      GRID_CURRENCY_COLUMN_QUANTITY_DIFFERENCE_WIDTH = 0
      GRID_CURRENCY_COLUMN_TOTAL_DIFFERENCE_WIDTH = 0

    Else
      GRID_CURRENCY_COLUMN_CASHIER_QUANTITY_WIDTH = 1005
      GRID_CURRENCY_COLUMN_CASHIER_TOTAL_WIDTH = 1630
      GRID_CURRENCY_COLUMN_QUANTITY_WIDTH = 1005
      GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL_WIDTH = 1630
      GRID_CURRENCY_COLUMN_QUANTITY_DIFFERENCE_WIDTH = 1005
      GRID_CURRENCY_COLUMN_TOTAL_DIFFERENCE_WIDTH = 1630

    End If

  End Sub

  ' PURPOSE: Checks if currencies and denominations are still allowed
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function CheckIfDenominationsAllowed() As Boolean
    Dim _row As DataRow
    Dim _table_denominations As DataTable
    Dim _result As Boolean
    Dim _denomination_error As String
    Dim _denomination As Decimal
    Dim _str_filter As String

    _table_denominations = Me.m_cage_cls.GetAllowedDenominations()
    _result = True
    For Each _row In m_cage_cls.TableCurrencies.Select("QUANTITY > 0")
      _denomination = _row("DENOMINATION")

      If _row("TYPE") = CageCurrencyType.Bill Then
        _str_filter = "ISO = '" & _row("ISO_CODE") & "'  AND TYPE IN (" & _row("TYPE") & ", " & CageCurrencyType.Coin & ") AND DENOMINATION = '" & _denomination.ToString() & "' "
      ElseIf _row("TYPE") = CageCurrencyType.ChipsColor Then
        _str_filter = "ISO = '" & _row("ISO_CODE") & "'  AND TYPE = " & _row("TYPE") & " AND CHIP_ID = " & _row("CHIP_ID")
      Else
        _str_filter = "ISO = '" & _row("ISO_CODE") & "'  AND TYPE = " & _row("TYPE") & " AND DENOMINATION = '" & _denomination.ToString() & "' "
      End If

      If _table_denominations.Select(_str_filter).Length = 0 Then
        _denomination_error = _row("ISO_CODE") & " - " & Me.m_cage_cls.FormatAmount(_row("DENOMINATION"), True, _row("ISO_CODE"))
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3031), ENUM_MB_TYPE.MB_TYPE_WARNING, , , _denomination_error)
        _result = False
        Exit For
      End If
    Next

    ' Dispose
    Call SafeDispose(_table_denominations)

    Return _result
  End Function ' CheckIfDenominationsAllowed

  ' PURPOSE: Checks if there are unbalances in denominations
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - CheckIfDenominationOk
  Private Function CheckIfDenominationOk() As Boolean
    Dim _imbalance_condition As String

    _imbalance_condition = " QUANTITY_DIFFERENCE <> '0' "

    If m_is_tito_collection Then
      If m_show_expected_data Then
        _imbalance_condition &= " AND DENOMINATION <> " & Cage.TICKETS_CODE
      Else
        Dim _initial_expected_denomination_string As String
        Dim _denomination_not_ok As Boolean
        _initial_expected_denomination_string = String.Empty

        _denomination_not_ok = CheckImbalancNotShowExpected(_initial_expected_denomination_string)

        If Not _denomination_not_ok Then
          Return False
        End If

        If _initial_expected_denomination_string.Length > 0 Then
          _imbalance_condition &= " AND DENOMINATION NOT IN (" & _initial_expected_denomination_string & ")"
        End If
      End If

    End If

    If m_is_coin_collected_enabled Then
      _imbalance_condition &= " AND DENOMINATION <> " & Cage.COINS_COLLECTION_CODE & " AND TYPE <> 1"
    End If

    If Me.m_cage_cls.Status = Cage.CageStatus.OK Then
      If Me.m_cage_cls.TableCurrencies.Select(_imbalance_condition).Length <> 0 Then
        Me.m_cage_cls.Status = Cage.CageStatus.OkDenominationImbalance

        Return False
      End If
    End If

    Return True
  End Function ' CheckIfDenominationOk

  ' PURPOSE: Checks if there are unbalances in amount
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - CheckIfTotalOk
  Private Function CheckIfTotalOk() As Boolean
    Dim _idx As Integer
    Dim _totals_imbalance As DataTable
    Dim _aux_row As DataRow

    CheckIfTotalOk = True
    _totals_imbalance = New DataTable
    _totals_imbalance.Columns.Add("ISO_CODE")
    _totals_imbalance.Columns.Add("TOTAL")

    For _idx = 0 To Me.dg_total.NumRows - 1
      If Me.dg_total.Cell(_idx, TOTAL_GRID_COLUMNS.DIFFERENCE_TOTAL).Value <> 0 _
         AndAlso Me.dg_total.Cell(_idx, TOTAL_GRID_COLUMNS.CURRENCY_TYPE_DESCRIPTION).Value <> TICKETS_ISOCODE_NLS Then 'not tickets row
        _aux_row = _totals_imbalance.NewRow()
        _aux_row("ISO_CODE") = Me.dg_total.Cell(_idx, TOTAL_GRID_COLUMNS.CURRENCY).Value
        _aux_row("TOTAL") = Me.dg_total.Cell(_idx, TOTAL_GRID_COLUMNS.DIFFERENCE_TOTAL).Value
        _totals_imbalance.Rows.Add(_aux_row)
        CheckIfTotalOk = False
        Me.m_cage_cls.Status = Cage.CageStatus.OkAmountImbalance
      End If
    Next

    ' save datatable of total imbalances for alarms
    Me.m_cage_cls.m_total_imbalance = _totals_imbalance.Copy

    ' Dispose
    Call SafeDispose(_totals_imbalance)

    Return CheckIfTotalOk
  End Function ' CheckIfTotalOk

  ' PURPOSE: To check if there is an inbalance
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function CheckImbalance() As Boolean
    Dim _balanced As Boolean
    Dim _val As Double
    Dim _sb As StringBuilder
    Dim _nls_id As Integer
    Dim _ticket_pending As Boolean
    Dim _total As Double
    Dim _iso_code As String
    Dim _currency_type As CageCurrencyType
    Dim _currency_type_desc As String

    Me.m_cage_cls.Status = Cage.CageStatus.OK

    _balanced = CheckIfTotalOk() AndAlso CheckIfDenominationOk()

    If Me.m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal Then
      Me.m_cage_cls.m_type_new_collection.balanced = _balanced
    ElseIf Me.m_cage_cls.OperationType = Cage.CageOperationType.FromCashier _
        Or Me.m_cage_cls.OperationType = Cage.CageOperationType.FromCashierClosing _
        Or Me.m_cage_cls.OperationType = Cage.CageOperationType.FromGamingTable _
        Or Me.m_cage_cls.OperationType = Cage.CageOperationType.FromGamingTableClosing _
        Or Me.m_cage_cls.OperationType = Cage.CageOperationType.FromGamingTableDropboxWithExpected Then
      Me.m_cage_cls.m_type_new_collection.balanced = True
    ElseIf Me.m_cage_cls.OperationType = Cage.CageOperationType.FromGamingTableDropbox Then
      ' Force the are not imbalance due to is blind collection
      Me.m_cage_cls.Status = Cage.CageStatus.OK
      _balanced = True
      Me.m_cage_cls.m_type_new_collection.balanced = _balanced
    End If

    If m_is_tito_collection Then
      m_cage_cls.m_type_new_collection.collection_details = FillDetailsList()
      _ticket_pending = Me.CheckIfTicketPending()

      'there is a ticket with "?" simbol. it means that there are some tickets with the same validation number 
      'and it has to be specified what of those tickets will be collected
      If _ticket_pending Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5163), _
                           ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)
        Return False
      End If

      If Me.m_cage_cls.OperationType <> Cage.CageOperationType.FromGamingTableDropbox Then
        _balanced = Me.CheckIfTicketsOk() And _balanced
      End If
    End If

    _sb = New StringBuilder()
    _val = 0

    _sb.AppendLine()

    For _idx As Int32 = 0 To dg_total.NumRows - 1
      If Double.TryParse(dg_total.Cell(_idx, GRID_TOTAL_COLUMN_TOTAL).Value, _val) Then
        _iso_code = dg_total.Cell(_idx, GRID_TOTAL_COLUMN_CURRENCY).Value
        _currency_type = dg_total.Cell(_idx, GRID_TOTAL_COLUMN_CURRENCY_TYPE).Value
        _currency_type_desc = dg_total.Cell(_idx, GRID_TOTAL_COLUMN_CURRENCY_TYPE_DESCRIPTION).Value

        If (String.IsNullOrEmpty(_currency_type_desc)) Then
          _currency_type_desc = _iso_code
        End If

        If _currency_type = FeatureChips.ChipType.RE Or _currency_type = FeatureChips.ChipType.NR Then
          _sb.AppendLine(String.Format("{0}: {1}", _currency_type_desc, GUI_FormatCurrency(_val)))
          _total += dg_total.Cell(_idx, GRID_TOTAL_COLUMN_TOTAL).Value
        ElseIf _currency_type = FeatureChips.ChipType.COLOR Then
          _sb.AppendLine(String.Format("{0}: {1}", _currency_type_desc, Me.FormatNumber(_val)))
          _total += dg_total.Cell(_idx, GRID_TOTAL_COLUMN_TOTAL).Value
        Else
          _sb.AppendLine(String.Format("{0}: {1}", _currency_type_desc, GUI_FormatCurrency(_val)))
          _total += dg_total.Cell(_idx, GRID_TOTAL_COLUMN_TOTAL).Value
        End If

      End If
    Next

    _sb.AppendLine()

    If Me.m_cage_cls.OperationType = Cage.CageOperationType.RequestOperation Then
      _nls_id = 4810
    Else
      _nls_id = 2371
    End If

    If _balanced Then
      'if everything is ok the collection is balanced
      _sb.AppendLine(GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_id, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2372)))
    Else
      'if there are difference between the real and theoretical values the collecion is unbalanced
      _sb.AppendLine(GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_id, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2373)))
    End If

    If Me.m_cage_cls.OperationType = Cage.CageOperationType.RequestOperation Then
      _nls_id = 4811
    Else
      _nls_id = 2368
    End If

    If Not GeneralParam.GetBoolean("Cage", "Collection.AutoClose") Then
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(_nls_id), _
                     ENUM_MB_TYPE.MB_TYPE_WARNING, _
                     ENUM_MB_BTN.MB_BTN_YES_NO, , _
                     _sb.ToString()) = ENUM_MB_RESULT.MB_RESULT_NO Then
        Return False
      End If
    End If

    If _total = 0 Then

      If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_FROM_PROMOBOX_OR_ACCEPTOR Or _
        Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MASSIVE Or _
        Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MOVEMENT Then

        Try

          Dim _source_name As String = String.Empty
          Dim _user_name As String = String.Empty
          Dim _cashier_session As String = String.Empty
          Dim _machine As String = String.Empty
          Dim _format_message As String = String.Empty

          _source_name = GLB_CurrentUser.Name & "@" & Environment.MachineName

          If (Not String.IsNullOrEmpty(cmb_user.TextValue) _
             AndAlso cmb_user.TextValue.Contains("-") _
             AndAlso cmb_user.TextValue.Split("-").Length = 2) Then

            _user_name = cmb_user.TextValue.Split("-")(1).Replace("[", String.Empty).Replace("]", String.Empty)
          End If

          If (m_cage_cls.OperationType = Cage.CageOperationType.FromCashier _
             Or m_cage_cls.OperationType = Cage.CageOperationType.FromCashierClosing _
             Or m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal) Then

            Dim _cashier_output As New Cashier.CashierSessionData
            Call Cashier.GetCashierSessionData(m_cage_cls.CashierSessionId, _cashier_output)

            _cashier_session = _cashier_output.Cashier_Name
          End If

          If (m_cage_cls.OperationType = Cage.CageOperationType.FromCashier _
             Or m_cage_cls.OperationType = Cage.CageOperationType.FromCashierClosing) Then
            _machine = GLB_NLS_GUI_INVOICING.GetString(210).ToLower()
            _format_message = _user_name & "@" & Me.m_cage_cls.m_type_new_collection.terminal_name & " (" & _cashier_session & ")"
          Else
            _machine = GLB_NLS_GUI_INVOICING.GetString(209).ToLower()
            _format_message = Me.m_cage_cls.m_type_new_collection.terminal_name & " (" & _cashier_session & ")"
          End If

          Call Alarm.Register(AlarmSourceCode.User, _
                              GLB_CurrentUser.Id, _source_name, _
                    AlarmCode.User_DenominationCollectionWithZero, _
                              GLB_NLS_GUI_PLAYER_TRACKING.GetString(5242, _machine, _format_message), AlarmSeverity.Info)

        Catch _ex As Exception
          Log.Exception(_ex)
        End Try

      End If

    End If

    Return True
  End Function 'CheckImbalance

  ''' <summary>
  ''' CheckImbalancNotShowExpected - Function that compare one money collection with values that user introduce
  ''' </summary>
  ''' <param name="_initial_expected_denomination_string">String with denominations that exclude</param>
  ''' <returns>Return True when all denominations is ok, otherwise return false</returns>
  ''' <remarks></remarks>
  Private Function CheckImbalancNotShowExpected(ByRef _initial_expected_denomination_string As String) As Boolean

    Dim _initial_expected_denomination_value As String
    Dim _initial_expected_denomination_quantity As String
    Dim _collected_denomination As String
    Dim _collected_quantity_value As String
    Dim _dg_currencies As uc_grid

    _initial_expected_denomination_value = String.Empty
    _initial_expected_denomination_quantity = String.Empty
    _collected_denomination = String.Empty
    _collected_quantity_value = String.Empty

    If m_list_money_collection Is Nothing Then
      m_list_money_collection = New List(Of NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS)
    End If

    For Each _item As TYPE_MONEY_COLLECTION_DETAILS In m_list_money_collection
      _initial_expected_denomination_value = GUI_FormatNumber(_item.bill_denomination, 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, 0).ToString()
      _initial_expected_denomination_quantity = GUI_FormatNumber(_item.bill_theoretical_count, 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, 0).ToString()
      _dg_currencies = Me.tb_currency.TabPages(0).Controls.Item(0)

      If _initial_expected_denomination_quantity > 0 Then
        For item As Integer = 0 To _dg_currencies.NumRows - 1

          _collected_denomination = GUI_FormatNumber(_dg_currencies.Cell(item, GRID_CURRENCY_COLUMN_DENOMINATION).Value, 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, 0).ToString()

          If String.IsNullOrEmpty(_dg_currencies.Cell(item, GRID_CURRENCY_COLUMN_QUANTITY).Value) OrElse GUI_FormatNumber(_dg_currencies.Cell(item, GRID_CURRENCY_COLUMN_DENOMINATION).Value, 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, 0) < 0 Then
            _collected_quantity_value = 0
          Else
            _collected_quantity_value = GUI_FormatNumber(_dg_currencies.Cell(item, GRID_CURRENCY_COLUMN_QUANTITY).Value, 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, 0).ToString()
          End If

          If (_collected_denomination.Equals(_initial_expected_denomination_value)) Then
            If Not _collected_quantity_value.Equals(_initial_expected_denomination_quantity) Then
              Return False

            Else

              If _initial_expected_denomination_string.Length = 0 Then
                _initial_expected_denomination_string = GUI_FormatNumber(_collected_denomination, 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, 0).ToString()
              Else
                _initial_expected_denomination_string = _initial_expected_denomination_string & "," & GUI_FormatNumber(_collected_denomination, 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, 0).ToString()
              End If
            End If
          End If
        Next
      End If
    Next


    Return True
  End Function

  ' PURPOSE: select tab depending on the selected row in datagrid total
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SelectTab(ByVal Row As Integer)
    Dim _idx As Integer
    Dim _datagrid As uc_grid

    If Not Me.m_initial_load Then
      For _idx = 0 To Me.tb_currency.TabCount - 1
        If Me.tb_currency.TabPages(_idx).Tag.Contains(Me.dg_total.Cell(Row, TOTAL_GRID_COLUMNS.CURRENCY).Value & Me.dg_total.Cell(Row, TOTAL_GRID_COLUMNS.CURRENCY_TYPE).Value) Then
          Me.m_selected_currency_tab = Me.dg_total.Cell(Row, TOTAL_GRID_COLUMNS.CURRENCY).Value & Me.dg_total.Cell(Row, TOTAL_GRID_COLUMNS.CURRENCY_TYPE).Value
          If Me.tb_currency.SelectedIndex <> _idx Then
            Me.tb_currency.SelectedIndex = _idx
            If m_redraw_grid Then
              _datagrid = Me.tb_currency.SelectedTab.Controls.Item(0)
              _datagrid.Redraw = True
            End If
            Exit For
          End If
        End If
      Next
    End If

  End Sub ' SelectTab

  ' PURPOSE: Gets the status string from the ID value
  '
  '  PARAMS:
  '     - INPUT:
  '           - status id, operation id
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetStatusString(ByVal Status As Integer, Optional ByVal OperationId As Cage.CageOperationType = Cage.CageOperationType.ToCashier) As String
    GetStatusString = ""

    Select Case Status
      Case Cage.CageStatus.Sent
        If OperationId = Cage.CageOperationType.ToCashier _
           Or OperationId = Cage.CageOperationType.ToGamingTable Then

          GetStatusString = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2891)
        Else
          If Me.m_cage_cls.OperationType = Cage.CageOperationType.RequestOperation Then
            GetStatusString = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4786)
          Else
            GetStatusString = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3011)
          End If

        End If

      Case Cage.CageStatus.OK
        If OperationId = Cage.CageOperationType.ToCashier _
           Or OperationId = Cage.CageOperationType.ToGamingTable Then

          GetStatusString = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2892)
        Else
          If Me.m_cage_cls.OperationType = Cage.CageOperationType.RequestOperation Then
            GetStatusString = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4788)
          Else
            GetStatusString = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3305)
          End If
        End If

      Case Cage.CageStatus.OkAmountImbalance
        If OperationId = Cage.CageOperationType.ToCashier _
           Or OperationId = Cage.CageOperationType.ToGamingTable Then

          GetStatusString = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2894)
        Else
          If Me.m_cage_cls.OperationType = Cage.CageOperationType.RequestOperation Then
            GetStatusString = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4788)
          Else
            GetStatusString = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3022)
          End If
        End If

      Case Cage.CageStatus.OkDenominationImbalance
        If OperationId = Cage.CageOperationType.ToCashier _
           Or OperationId = Cage.CageOperationType.ToGamingTable Then

          GetStatusString = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2944)
        Else
          If Me.m_cage_cls.OperationType = Cage.CageOperationType.RequestOperation Then
            GetStatusString = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4788)
          Else
            GetStatusString = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3021)
          End If
        End If

      Case Cage.CageStatus.Canceled
        If Me.m_cage_cls.OperationType = Cage.CageOperationType.RequestOperation Then
          GetStatusString = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4787)
        Else
          GetStatusString = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2908)
        End If

      Case Cage.CageStatus.CanceledDueToImbalanceOrOther
        GetStatusString = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3024)

      Case Cage.CageStatus.ErrorTotalImbalance
        GetStatusString = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2893)

      Case Cage.CageStatus.SentToCustom
        GetStatusString = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2892)

      Case Cage.CageStatus.ReceptionFromCustom
        GetStatusString = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2892)

      Case Cage.CageStatus.FinishedOpenCloseCage
        GetStatusString = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2892)

      Case Cage.CageStatus.FinishedCountCage
        GetStatusString = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2892)

      Case Else
        GetStatusString = AUDIT_NONE_STRING

    End Select

    Return GetStatusString
  End Function ' GetStatusString

  ' PURPOSE: Checks if max allowed diposit is not surpassed
  '
  '  PARAMS:
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - true/false
  Private Function CheckMaxAllowedDeposit(ByRef IsoCode As String, ByRef MaxAllowed As Long) As Boolean
    Dim _idx_dg As Integer
    Dim _result As Boolean

    _result = True

    MaxAllowed = GeneralParam.GetInt64("Cashier", "MaxAllowedDeposit", 0)

    If MaxAllowed <> 0 Then

      For _idx_dg = 0 To Me.dg_total.NumRows - 1

        If CLng(Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.TOTAL).Value) > MaxAllowed Then
          IsoCode = Me.dg_total.Cell(_idx_dg, TOTAL_GRID_COLUMNS.CURRENCY).Value
          _result = False
          Exit For
        End If

      Next
    End If

    Return _result
  End Function ' CheckMaxAllowedDeposit

  ' PURPOSE: Sets text labels and init
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub InitLabels()

    Me.gb_movement_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3391)
    Me.rb_send.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2906)
    Me.rb_reception.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2121)
    Me.gb_source_target.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3396)
    Me.rb_custom.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3393)
    Me.rb_cashier.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1818)
    Me.gb_custom.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3393)
    Me.cmb_custom.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3396)
    Me.cmb_terminals.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1097)
    Me.cmb_gaming_table.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4419)  ' Mesa 
    Me.gb_cage_sessions.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3379)  ' Sesiones de boveda
    Me.cmb_cage_sessions.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(605) ' Sesion
    Me.gb_template.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5052)  ' Template
    Me.cmb_cage_template.Text = GLB_NLS_GUI_CONTROLS.GetString(323) ' Nombre
    Me.gb_gaming_table.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3416)   ' Mesas de juego
    Me.rb_to_user.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3449)
    Me.rb_to_terminal.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3450)
    Me.rb_gaming_table.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4474)   ' Mesas de juego
    Me.btn_load_gaming_values.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3457) ' Totales   
    Me.cb_close_gaming_table.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4436)  ' Cerrar mesa

    Me.gb_note_counter_events.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5452)
    Me.gb_note_counter_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5451)

    Me.rb_terminal.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4473)  ' Terminales
    Me.ef_cage_session.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(605) ' Sesion de boveda

    Me.lb_gt_without_integrated_cashier.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4922) ' S�lo mesas de juego sin cajero integrado
    Me.lb_gt_without_integrated_cashier.Location = New Point(Me.lb_gt_without_integrated_cashier.Location.X, cmb_gaming_table.Location.Y)

    Me.lb_error_messages.ForeColor = Color.Blue
    Me.lb_error_messages.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5199)
    Me.lb_error_messages.Visible = False
    Me.lbl_counter.Visible = False
    Me.lb_note_counter_status.Visible = False

    If GeneralParam.GetString("WigosGUI", "Language").Equals("en") Then
      Me.rb_terminal.Location = New Point(Me.rb_terminal.Location.X - 30, Me.rb_terminal.Location.Y)
    End If

    Me.ef_cashier_date.TextValue = AUDIT_NONE_STRING
    Me.ef_cashier_name.TextValue = AUDIT_NONE_STRING
    Me.ef_user_terminals_name.TextValue = AUDIT_NONE_STRING
    Me.ef_gaming_table.TextValue = AUDIT_NONE_STRING
    Me.ef_custom.TextValue = AUDIT_NONE_STRING
    Me.ef_operation.TextValue = AUDIT_NONE_STRING
    Me.ef_status_movement.TextValue = AUDIT_NONE_STRING
    Me.ef_cage_user.TextValue = AUDIT_NONE_STRING
    Me.ef_date_cage.TextValue = AUDIT_NONE_STRING
    Me.ef_gaming_table.TextValue = AUDIT_NONE_STRING

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4576)  ' Aceptar

    If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE Then

      Call Me.CageOpenModeLabels(Me.m_cage_cls.OpenMode)
    Else
      Call Me.NormalOpenModeLabels()
    End If
  End Sub ' InitLabels

  ' PURPOSE: Sets text labels and init
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub NormalOpenModeLabels()
    Me.gb_cashier.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1818)
    Me.ef_cashier_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1597)
    Me.ef_cashier_date.IsReadOnly = True
    Me.ef_cashier_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1818)
    Me.ef_cashier_name.IsReadOnly = True
    Me.ef_user_terminals_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2696)
    Me.ef_user_terminals_name.IsReadOnly = True
    Me.ef_gaming_table_visits.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4437) ' N� visitas  
    Me.ef_gaming_table_visits.SetFilter(FORMAT_NUMBER, 8)
    Me.ef_gaming_table_visits.Enabled = False

    Select Case Me.m_cage_cls.OpenMode
      Case CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT
        If Me.m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal Then
          Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4943) ' Recaudaci�n
        Else
          Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3001) ' Nuevo env�o desde b�veda      
        End If
      Case CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MASSIVE _
         , CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MOVEMENT
        ' 24-JUL-2014 LEM          Fixed Bug WIG-1066: Wrong window title in RequestOperation.
        Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4943) 'Recaudaci�n
        If m_cage_cls.OperationType = Cage.CageOperationType.RequestOperation Then
          Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4785) 'Solicitud desde cajero
        End If
      Case CLASS_CAGE_CONTROL.OPEN_MODE.TEMPLATE_MAINTENANCE
        Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7355) 'Stock de cierre (Mantenimiento de plantillas)

      Case Else
        If Me.FormId = ENUM_FORM.FORM_STACKER_COLLECTION Then
          Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4968) ' Detalles de recaudaci�n
        Else
          If Me.m_cage_cls.OperationType = Cage.CageOperationType.ToTerminal Then
            Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2906) ' Env�o
          Else
            Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2905) ' Detalles de movimiento de b�veda
          End If
        End If
    End Select

    If Me.m_mode_edit = MODE_EDIT.SEND And Me.m_cage_cls.OpenMode <> CLASS_CAGE_CONTROL.OPEN_MODE.TEMPLATE_MAINTENANCE Then
      Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4376) ' Envio a Cajero
    End If

    Me.gb_total.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2936)
    Me.cmb_user.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2696)
    Me.gb_operation.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(627)
    Me.cmb_user.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2696)
    Me.ef_status_movement.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1752)
    Me.ef_date_cage.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1597)
    Me.ef_cage_user.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2896)
    Me.ef_operation.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(254)
    Me.ef_gaming_table.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3416)

    ' Set ReadOnly 
    Me.ef_cage_session.IsReadOnly = True
    Me.ef_date_cage.IsReadOnly = True
    Me.ef_operation.IsReadOnly = True
    Me.ef_cage_user.IsReadOnly = True
    Me.ef_status_movement.IsReadOnly = True
    Me.ef_custom.IsReadOnly = True

  End Sub ' NormalOpenModeLabels

  ' PURPOSE: Sets text labels and init
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub CageOpenModeLabels(ByVal OpenMode As CLASS_CAGE_CONTROL.OPEN_MODE)

    If OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE Then
      Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4352) ' count Cage
    End If

    Me.gb_total.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2936)
    Me.cmb_user.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2696)
    Me.gb_operation.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(627)
    Me.cmb_user.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2696)
    Me.ef_status_movement.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1752)
    Me.ef_date_cage.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1597)
    Me.ef_cage_user.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2896)
    Me.ef_operation.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(254)

    ' Set ReadOnly 
    Me.ef_cage_session.IsReadOnly = True
    Me.ef_date_cage.IsReadOnly = True
    Me.ef_operation.IsReadOnly = True
    Me.ef_cage_user.IsReadOnly = True
    Me.ef_status_movement.IsReadOnly = True
  End Sub ' CageOpenModeLabels

  ' PURPOSE: Initializes combos
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub InitCombos()
    Dim _sb As StringBuilder
    ' DCS 13-11-2014 : Add GP for view only users / terminas with cashier session OPEN

    Me.IsCountREnabled = WSI.Common.Misc.IsCountREnabled()
    _sb = New StringBuilder

    ' RAB 18-FEB-2016
    If (Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT) Then
      _sb.AppendLine("     SELECT   GU_USER_ID ")
    Else
      _sb.AppendLine("     SELECT   DISTINCT(GU_USER_ID) ")
    End If
    _sb.AppendLine("            , GU_FULL_NAME + ' - [' + GU_USERNAME + ']' AS GU_FULL_NAME ")
    _sb.AppendLine("       FROM   GUI_USERS GU ")
    _sb.AppendLine(" INNER JOIN   GUI_USER_PROFILES GUP ")
    _sb.AppendLine("         ON   GU.GU_PROFILE_ID = GUP.GUP_PROFILE_ID ")
    _sb.AppendLine(" INNER JOIN   GUI_PROFILE_FORMS GPF ")
    _sb.AppendLine("         ON   GUP.GUP_PROFILE_ID = GPF.GPF_PROFILE_ID ")
    If m_only_cashier_is_open Then
      _sb.AppendLine(" INNER JOIN   CASHIER_SESSIONS CS ")
      _sb.AppendLine("         ON   CS.CS_USER_ID = GU.GU_USER_ID ")
    End If

    ' RAB 18-FEB-2016
    If (Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT) Then
      _sb.AppendLine("      WHERE   GU_BLOCK_REASON = " & WSI.Common.GUI_USER_BLOCK_REASON.NONE)
      _sb.AppendLine("        AND   GU_USER_TYPE = " & WSI.Common.GU_USER_TYPE.USER)
      _sb.AppendLine("        AND   GPF.GPF_GUI_ID = " & WSI.Common.ENUM_GUI.CASHIER)
      _sb.AppendLine("        AND   GPF.GPF_FORM_ID = " & ENUM_CASHIER_FORM.FORM_MAIN)
      If m_only_cashier_is_open Then
        _sb.AppendLine("        AND   CS.CS_STATUS = " & WSI.Common.CASHIER_SESSION_STATUS.OPEN)
      End If
      _sb.AppendLine("        AND   GPF.GPF_READ_PERM = 1 ")
    End If
    _sb.AppendLine("   ORDER BY   GU_FULL_NAME ")

    ' Set combo with Users (only users with cashier permission)
    Call SetCombo(Me.cmb_user, _sb.ToString, True)
    ' Set combo with cashier terminals
    Call Me.m_cage_cls.SetComboTerminals(Me.cmb_terminals)
    ' Set combo with gaming table
    Call Me.m_cage_cls.SetComboGamingTable(Me.cmb_gaming_table, False, True, Me.m_cage_auto_mode)
    ' Set combo with cage sessions
    Call Me.m_cage_cls.SetComboCageSessions(Me.cmb_cage_sessions, True)
    ' Set combo with template cage sessions
    Call Me.m_cage_cls.SetComboTemplateCageSessions(Me.cmb_cage_template, True)
    Call Me.SetDefaultCageSession()
  End Sub ' InitCombos

  ' PURPOSE: Initializes Open Mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - TableCurrencies
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub InitOpenMode(ByRef TableCurrencies As DataTable)

    Select Case Me.m_cage_cls.OpenMode

      Case CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT _
         , CLASS_CAGE_CONTROL.OPEN_MODE.CLOSE_VIRTUAL
        Call Me.m_cage_cls.InitCurrencyData(True)
    End Select

    TableCurrencies = Me.m_cage_cls.GetAllowedISOCode()

    If Me.m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal Then
      Call Me.m_cage_cls.InitCurrencyData(True)
    End If

  End Sub ' InitOpenMode

  ' PURPOSE: Sets the description of the operation
  '
  '  PARAMS:
  '     - INPUT:
  '           - CageControl
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetOperationText(ByVal OperationType As Cage.CageOperationType)
    ' Set Operation text
    Select Case OperationType

      Case Cage.CageOperationType.ToCashier _
         , Cage.CageOperationType.RequestOperation
        If (m_cage_cls.TargetType = Cage.PendingMovementType.ToGamingTable) Then
          Me.ef_operation.TextValue = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4394) ' Cage to Gaming table
        Else
          Me.ef_operation.TextValue = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2890) ' Cage to Cashier
        End If
      Case Cage.CageOperationType.FromCashier
        Me.ef_operation.TextValue = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2889) ' Cashier to Cage

      Case Cage.CageOperationType.FromCashierClosing
        Me.ef_operation.TextValue = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7897) ' Cashier to Cage

      Case Cage.CageOperationType.ToCustom
        Me.ef_operation.TextValue = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3394) ' Cage To Custom

      Case Cage.CageOperationType.FromCustom
        Me.ef_operation.TextValue = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3395) ' Custom To Cage

      Case Cage.CageOperationType.OpenCage
        Me.ef_operation.TextValue = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3381) ' Open Cage

      Case Cage.CageOperationType.CloseCage
        Me.ef_operation.TextValue = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3382) ' Close Cage

      Case Cage.CageOperationType.CountCage, Cage.CageOperationType.GainQuadrature, Cage.CageOperationType.LostQuadrature
        Me.ef_operation.TextValue = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4352) ' Count Cage

      Case Cage.CageOperationType.ToGamingTable
        Me.ef_operation.TextValue = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4394) ' Cage to Gaming table

      Case Cage.CageOperationType.FromGamingTable
        Me.ef_operation.TextValue = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7900) ' Gaming table to Cage

      Case Cage.CageOperationType.FromGamingTableClosing
        Me.ef_operation.TextValue = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7900) ' Gambling table to Cage

      Case Cage.CageOperationType.FromGamingTableDropbox, _
        Cage.CageOperationType.FromGamingTableDropboxWithExpected
        Me.ef_operation.TextValue = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7329) ' Gambling Table drop box collection

    End Select
  End Sub ' SetOperationText

  Private Sub AddRemoveHandler(ByVal IsAddHandler As Boolean)

    RemoveHandler rb_send.CheckedChanged, AddressOf rb_send_CheckedChanged
    RemoveHandler rb_reception.CheckedChanged, AddressOf rb_reception_CheckedChanged
    RemoveHandler rb_cashier.CheckedChanged, AddressOf rb_cashier_CheckedChanged
    RemoveHandler rb_to_user.CheckedChanged, AddressOf rb_to_user_CheckedChanged
    RemoveHandler rb_to_terminal.CheckedChanged, AddressOf rb_to_terminal_CheckedChanged
    RemoveHandler rb_gaming_table.CheckedChanged, AddressOf rb_gaming_table_CheckedChanged
    RemoveHandler rb_custom.CheckedChanged, AddressOf rb_custom_CheckedChanged
    RemoveHandler dg_total.RowSelectedEvent, AddressOf dg_total_RowSelectedEvent

    If IsAddHandler Then
      AddHandler rb_send.CheckedChanged, AddressOf rb_send_CheckedChanged
      AddHandler rb_reception.CheckedChanged, AddressOf rb_reception_CheckedChanged
      AddHandler rb_cashier.CheckedChanged, AddressOf rb_cashier_CheckedChanged
      AddHandler rb_to_user.CheckedChanged, AddressOf rb_to_user_CheckedChanged
      AddHandler rb_to_terminal.CheckedChanged, AddressOf rb_to_terminal_CheckedChanged
6:    AddHandler rb_gaming_table.CheckedChanged, AddressOf rb_gaming_table_CheckedChanged
      AddHandler rb_custom.CheckedChanged, AddressOf rb_custom_CheckedChanged
      AddHandler dg_total.RowSelectedEvent, AddressOf dg_total_RowSelectedEvent
    End If
  End Sub 'AddRemoveHandler

  ' PURPOSE: Sets all combos with the default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValuesCombos()
    Me.cmb_user.SelectedIndex = -1
    ' Me.cmb_user.TextValue = String.Empty
    Me.cmb_terminals.SelectedIndex = -1
    ' Me.cmb_terminals.TextValue = String.Empty
    If Not Me.rb_gaming_table.Checked Then
      Me.cmb_gaming_table.SelectedIndex = -1
      ' Me.cmb_gaming_table.TextValue = String.Empty
    End If
    Me.cmb_custom.SelectedIndex = -1
    ' Me.cmb_custom.TextValue = String.Empty

  End Sub ' SetDefaultValuesCombos

  ' PURPOSE: Sets the checked radiobuttons to set the operation text
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub CheckRadioButtonsToSetLayouts()
    Dim _panel_aux_x As Integer
    Dim _panel_aux_y As Integer
    Dim _cage_session_Y As Integer
    Dim _gaming_table_name As String

    Call AddRemoveHandler(False)

    Me.RefreshTabsBySelection()

    _panel_aux_x = panel_aux.Location.X
    _panel_aux_y = panel_aux.Location.Y
    _cage_session_Y = Me.gb_cage_sessions.Location.Y
    _gaming_table_name = String.Empty

    Try

      Call Me.SetOperationText(Me.m_cage_cls.OperationType)

      If Me.m_first_time Then
        Me.ef_cage_session.Location = Me.cmb_cage_sessions.Location
      End If

      Select Case Me.m_cage_cls.OpenMode
        Case CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT
          Call CheckRadioButtonsToLayouts_NewMovement(_panel_aux_x, _panel_aux_y)

        Case CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT _
           , CLASS_CAGE_CONTROL.OPEN_MODE.VIEW_MOVEMENT _
           , CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MOVEMENT _
           , CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT_FROM_GAMINGTABLES
          Call CheckRadioButtonsToSetLayouts_CancelOrViewMovement(_panel_aux_x, _panel_aux_y, _gaming_table_name)

        Case CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE
          Call CheckRadioButtonsToSetLayouts_CountCage(_panel_aux_x, _panel_aux_y)

        Case CLASS_CAGE_CONTROL.OPEN_MODE.CLOSE_VIRTUAL
          Call CheckRadioButtonsToSetLayouts_CloseVirtual()

        Case CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MASSIVE
          Call CheckRadioButtonsToSetLayouts_RecolectMassive(_panel_aux_x, _panel_aux_y)

        Case CLASS_CAGE_CONTROL.OPEN_MODE.TEMPLATE_MAINTENANCE
          Call CheckRadioButtonsToSetLayouts_TemplateMaintenance(_panel_aux_x, _panel_aux_y)

      End Select

      Call CheckRadioButtonsFinalSet(_panel_aux_x, _panel_aux_y)

    Catch ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, GLB_ApplicationName, "CheckRadioButtonsToSetLayouts", _
                            "", "", "", ex.Message)
    End Try


  End Sub ' CheckRadioButtonsToLayouts

  ''' <summary>
  ''' Set the radio buttons, when is a new movement operation
  ''' </summary>
  ''' <param name="_panel_aux_x"></param>
  ''' <param name="_panel_aux_y"></param>
  ''' <remarks></remarks>
  Private Sub CheckRadioButtonsToLayouts_NewMovement(ByRef _panel_aux_x As Integer, ByRef _panel_aux_y As Integer)
    ' Check if it has a cage session id
    If Me.m_first_time Then
      If Me.m_cage_cls.CageSessionId <> -1 Then
        If Me.m_is_tito_collection Then

          If Not Me.m_new_collecion_mode Then
            Me.ef_ticket.Enabled = False
            Me.btn_save_tickets.Enabled = False
            Me.tb_notes.Enabled = False
          End If

        Else
          Call Me.SetCageSessionEfValue()
        End If
      Else
        Me.ef_cage_session.Visible = False
      End If
    End If

    If Me.m_cage_cls.OperationType <> Cage.CageOperationType.FromTerminal Then

      Me.ef_gaming_table_visits.Value = String.Empty

      If m_first_time Then
        ' DHA 03-JUN-2015 if player tracking enabled and automode disable, the gaming table can't be managed
        If Me.m_cage_auto_mode And GamingTableBusinessLogic.GamingTablesMode() = GamingTableBusinessLogic.GT_MODE.GUI_AND_CASHIER Then
          Me.rb_cashier.Enabled = False
          Me.rb_gaming_table.Enabled = False
          Me.rb_custom.Checked = True
        Else
          Me.rb_cashier.Checked = True
          Me.rb_to_user.Checked = True
        End If

        ' RAB 16-MAR-2016: Bug 10658
        Me.gb_custom.Visible = Me.rb_custom.Checked
        Me.gb_cashier.Visible = Me.rb_cashier.Checked

        Me.rb_send.Checked = True
        Me.rb_gaming_table.Enabled = Me.rb_gaming_table.Enabled And GamingTableBusinessLogic.IsGamingTablesEnabled
        Me.rb_terminal.Enabled = False 'GeneralParam.GetBoolean("TITO", "TITOMode", False)

        m_first_time = False
        ' 16-FEB-2015 DCS Fixed Bug WIG-2023
        ' Call events for change OperationType
        If Me.rb_gaming_table.Checked Then
          Call rb_gaming_table_CheckedChanged(Nothing, Nothing)
        End If
        If Me.rb_cashier.Checked Then
          Call rb_cashier_CheckedChanged(Nothing, Nothing)
        End If
        If Me.rb_custom.Checked Then
          Call rb_custom_CheckedChanged(Nothing, Nothing)
        End If

      End If 'If m_first_time

      Me.ef_cashier_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1818)
      Me.ef_status_movement.TextValue = AUDIT_NONE_STRING
      Me.ef_date_cage.TextValue = GUI_FormatDate(Date.Now, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
      Me.ef_cage_user.TextValue = Me.m_cage_cls.GetUserFullName(CurrentUser.Id)
      Me.ef_user_terminals_name.Visible = False

      ' To Cashier (User or Cashier Terminal)
      If Me.rb_cashier.Checked Then

        Call Me.SetDefaultValuesCombos()

        If cmb_cage_sessions.Count = 2 Then
          Me.cmb_cage_sessions.SelectedIndex = Me.cmb_cage_sessions.Count - 1
          Me.cmb_cage_sessions.IsReadOnly = True
        Else
          Me.cmb_cage_sessions.IsReadOnly = False
        End If

        If Me.rb_send.Checked Then
          If m_after_load Then
            Call Me.ReDrawGrids()
          End If
        End If

        Call Me.SetToUserToTerminal(Me.rb_to_user.Checked, Me.rb_to_terminal.Checked)

        m_y_offset = (gb_cashier.Location.Y + gb_cashier.Size.Height) - (gb_movement_type.Location.Y + gb_movement_type.Size.Height)
        _panel_aux_x = Me.gb_cashier.Location.X
        _panel_aux_y = Me.gb_cashier.Location.Y + m_y_offset - 5

      End If 'Me.rb_cashier.Checked

      ' Gaming Table
      If Me.rb_gaming_table.Checked Then

        If Me.m_cage_auto_mode Then
          If Me.rb_send.Checked Then
            Me.lb_gt_without_integrated_cashier.Visible = True
          Else
            Me.lb_gt_without_integrated_cashier.Visible = False
          End If
        End If

        Call Me.SetDefaultValuesCombos()

        If cmb_cage_sessions.Count = 2 Then
          Me.cmb_cage_sessions.SelectedIndex = Me.cmb_cage_sessions.Count - 1
          Me.cmb_cage_sessions.IsReadOnly = True
        Else
          Me.cmb_cage_sessions.IsReadOnly = False
        End If

        Me.panel_gaming_table_type.Parent.Visible = Me.rb_reception.Checked
        Me.panel_gaming_table_type.Visible = Me.rb_reception.Checked
        Me.btn_load_gaming_values.Visible = False 'Me.rb_reception.Checked
        Me.ef_gaming_table_visits.Visible = GeneralParam.GetBoolean("GamingTables", "EnabledInsertVisits", False)

        If Not Me.ef_gaming_table_visits.Visible Then
          cb_close_gaming_table.Top = Me.ef_gaming_table_visits.Top
        Else
          cb_close_gaming_table.Top = 1
        End If

        Me.ef_gaming_table.Visible = False

        m_y_offset = (gb_gaming_table.Location.Y + gb_gaming_table.Size.Height) - (gb_movement_type.Location.Y + gb_movement_type.Size.Height)
        _panel_aux_x = Me.gb_gaming_table.Location.X
        _panel_aux_y = Me.gb_gaming_table.Location.Y + m_y_offset + 26

        If Me.rb_send.Checked Then
          Me.m_gaming_values = False
          Call Me.ReDrawGrids()
        Else
          Call Me.ReDrawGrids(True)
        End If

      End If 'Me.rb_gaming_table.Checked

      ' Custom
      If Me.rb_custom.Checked Then

        Call Me.SetDefaultValuesCombos()

        If cmb_cage_sessions.Count = 2 Then
          Me.cmb_cage_sessions.SelectedIndex = Me.cmb_cage_sessions.Count - 1
          Me.cmb_cage_sessions.IsReadOnly = True
        Else
          Me.cmb_cage_sessions.IsReadOnly = False
        End If

        Me.cmb_custom.Visible = True
        Me.ef_custom.Visible = False

        If Me.m_gaming_values Then
          Me.m_gaming_values = False
          Call Me.ReDrawGrids()
        End If

        If Me.rb_reception.Checked Then
          Call Me.ReDrawGrids(, True)
        Else
          If m_after_load Then
            Call Me.ReDrawGrids()
          End If
        End If

        m_y_offset = (gb_custom.Location.Y + gb_custom.Size.Height) - (gb_movement_type.Location.Y + gb_movement_type.Size.Height)
        _panel_aux_x = Me.gb_custom.Location.X
        _panel_aux_y = Me.gb_custom.Location.Y + m_y_offset + 26

        ' Set combo with source/target customs
        Call Me.m_cage_cls.SetComboCustom(Me.rb_send.Checked, Me.cmb_custom, True)
      End If 'If Me.rb_custom.Checked

      ' Terminal
      Me.gb_operation.Visible = Not Me.rb_terminal.Checked
      If Me.rb_terminal.Checked Then
        Me.gb_collection_params.Location = New Point(Me.panel_aux.Location.X, _
                                                    (Me.gb_movement_type.Location.Y + Me.gb_movement_type.Height + 3))
        Me.gb_collection_params.Visible = True

        m_y_offset = (Me.gb_collection_params.Location.Y + Me.gb_collection_params.Size.Height) _
                       - (Me.gb_movement_type.Location.Y + Me.gb_movement_type.Size.Height) _
                           - (Me.gb_operation.Location.Y + Me.gb_operation.Size.Height)

        _panel_aux_x = Me.gb_collection_params.Location.X
        _panel_aux_y = Me.gb_collection_params.Location.Y + m_y_offset
      End If
    Else ' Is tickets collection 'If Me.m_cage_cls.OperationType <>
      If Me.m_new_collecion_mode Then
        Me.ef_cage_session.Visible = False

        If Me.m_cage_cls.CageSessionId <> 0 Then
          Me.cmb_cage_sessions.IsReadOnly = True
        End If
      Else
        Call Me.SetCageSessionEfValue()
        Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Visible = False
      End If

      Me.gb_operation.Visible = Not Me.rb_terminal.Checked
      Me.rb_reception.Checked = True
      Me.rb_terminal.Checked = True

      Me.gb_source_target.Enabled = False
      Me.gb_movement_type.Enabled = False
      Me.gb_collection_params.Location = New Point(Me.panel_aux.Location.X, _
                                                  (Me.gb_movement_type.Location.Y + Me.gb_movement_type.Height + 3))
      Me.gb_collection_params.Visible = True

      m_y_offset = (Me.gb_collection_params.Location.Y + Me.gb_collection_params.Size.Height) _
                     - (Me.gb_movement_type.Location.Y + Me.gb_movement_type.Size.Height) _
                         - (Me.gb_operation.Location.Y + Me.gb_operation.Size.Height)

      _panel_aux_x = Me.gb_collection_params.Location.X
      _panel_aux_y = Me.gb_collection_params.Location.Y + m_y_offset
    End If ' Is tickets collection 'If Me.m_cage_cls.OperationType <>
  End Sub ' CheckRadioButtonsToLayouts_NewMovement

  ''' <summary>
  ''' Set the radiobuttons when the open mode is cancel movement, view movement, recolet movement or cancel movement from gamingtables
  ''' </summary>
  ''' <param name="_panel_aux_x"></param>
  ''' <param name="_panel_aux_y"></param>
  ''' <param name="_gaming_table_name"></param>
  ''' <remarks></remarks>
  Private Sub CheckRadioButtonsToSetLayouts_CancelOrViewMovement(ByRef _panel_aux_x As Integer, ByRef _panel_aux_y As Integer, ByRef _gaming_table_name As String)
    Call Me.SetCageSessionEfValue()

    Me.gb_movement_type.Enabled = False
    Me.gb_source_target.Enabled = False
    Me.panel_gaming_table_type.Visible = False
    Me.cmb_gaming_table.Visible = False
    Me.btn_load_gaming_values.Visible = False
    Me.ef_cage_user.TextValue = Me.m_cage_cls.GetUserFullName(Me.m_cage_cls.UserCageID)
    Me.ef_status_movement.TextValue = Me.GetStatusString(Me.m_cage_cls.Status, Me.m_cage_cls.OperationType)
    Me.ef_date_cage.TextValue = GUI_FormatDate(Me.m_cage_cls.MovementDatetime, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    Select Case Me.m_cage_cls.OperationType
      Case Cage.CageOperationType.FromTerminal
        Call SetLayoutFromMoneyCollection(_panel_aux_x, _panel_aux_y)
      Case Cage.CageOperationType.ToTerminal
        Call SetLayoutFromMoneyCollection(_panel_aux_x, _panel_aux_y)
        rb_send.Checked = True
      Case Cage.CageOperationType.ToCashier _
         , Cage.CageOperationType.FromCashier _
         , Cage.CageOperationType.FromCashierClosing _
         , Cage.CageOperationType.RequestOperation

        If Me.m_cage_cls.OperationType = Cage.CageOperationType.FromCashier Or Me.m_cage_cls.OperationType = Cage.CageOperationType.FromCashierClosing Then
          If m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT Then
            Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Visible = True
          End If

          Me.rb_reception.Checked = True
          Me.panel_cashier_type.Visible = False
        ElseIf Me.m_cage_cls.OperationType = Cage.CageOperationType.RequestOperation Then
          If Not Me.m_cage_cls.Status = Cage.CageStatus.OK AndAlso Not Me.m_cage_cls.Status = Cage.CageStatus.Canceled Then
            Me.ef_cage_user.TextValue = Me.m_cage_cls.GetUserFullName(CurrentUser.Id)
          End If

          If m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT Then
            Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Visible = True
          ElseIf m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.VIEW_MOVEMENT Then
            Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Visible = False
          End If

          Me.rb_send.Checked = True
        Else
          If m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT Then
            Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Visible = True
          ElseIf m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.VIEW_MOVEMENT Then
            Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Visible = False
          End If

          Me.rb_send.Checked = True
        End If

        Me.ef_user_terminals_name.Visible = True
        Me.rb_cashier.Checked = True
        Me.panel_cashier_type.Enabled = False
        Me.cmb_user.Visible = False
        Me.cmb_terminals.Visible = False

        Select Case m_cage_cls.TargetType
          Case Cage.PendingMovementType.ToCashierUser

            Me.rb_to_user.Checked = True
            Me.cmb_user.Value = Me.m_cage_cls.UserCashierID
            Me.ef_user_terminals_name.TextValue = Me.cmb_user.TextValue

          Case Cage.PendingMovementType.ToCashierTerminal

            Me.rb_to_terminal.Checked = True

            RemoveHandler Me.cmb_terminals.ValueChangedEvent, AddressOf cmb_terminals_ValueChangedEvent
            Me.cmb_terminals.Value = Me.m_cage_cls.TerminalCashierID 'Me.m_terminal_cashier_id 
            AddHandler Me.cmb_terminals.ValueChangedEvent, AddressOf cmb_terminals_ValueChangedEvent

            Me.ef_user_terminals_name.TextValue = Me.cmb_terminals.TextValue

            Me.ef_cashier_name.Visible = False
            Me.ef_cashier_date.Visible = False

            Me.ef_user_terminals_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2254) ' Terminal

            Me.gb_cashier.Height = GB_CASHIER_HEIGHT_TO_TERMINAL

          Case Cage.PendingMovementType.ToGamingTable
            Me.rb_cashier.Checked = False
            Me.rb_gaming_table.Checked = True
            Using _db_trx As New DB_TRX()
              If (GamingTablesSessions.GetGamingTableName(m_cage_cls.GamingTableId, _gaming_table_name, _db_trx.SqlTransaction)) Then
                ef_gaming_table.Value = _gaming_table_name
              Else
                ef_gaming_table.Value = AUDIT_NONE_STRING
              End If
            End Using
          Case Else
            Me.cmb_user.Value = Me.m_cage_cls.UserCashierID
            Me.ef_user_terminals_name.TextValue = Me.cmb_user.TextValue
            Me.gb_cashier.Height = GB_CASHIER_HEIGHT_TO_USER

        End Select

        m_y_offset = (gb_cashier.Location.Y + gb_cashier.Size.Height) - (gb_movement_type.Location.Y + gb_movement_type.Size.Height)
        _panel_aux_x = Me.gb_cashier.Location.X
        _panel_aux_y = Me.gb_cashier.Location.Y + m_y_offset

      Case Cage.CageOperationType.ToGamingTable _
         , Cage.CageOperationType.FromGamingTable _
         , Cage.CageOperationType.FromGamingTableClosing _
         , Cage.CageOperationType.FromGamingTableDropbox _
         , Cage.CageOperationType.FromGamingTableDropboxWithExpected

        If m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT Or _
          m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT_FROM_GAMINGTABLES Or _
           m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MOVEMENT Then
          Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Visible = True
        Else
          Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Visible = False
        End If

        If Me.m_cage_cls.OperationType = Cage.CageOperationType.FromGamingTable Or _
          Me.m_cage_cls.OperationType = Cage.CageOperationType.FromGamingTableClosing Or _
          Me.m_cage_cls.OperationType = Cage.CageOperationType.FromGamingTableDropbox Or _
          Me.m_cage_cls.OperationType = Cage.CageOperationType.FromGamingTableDropboxWithExpected Then

          Me.panel_gaming_table_type.Visible = True
          Me.cb_close_gaming_table.Visible = False
          Me.panel_gaming_table_type.Height -= Me.cb_close_gaming_table.Height
          Me.ef_gaming_table_visits.Location = Me.cb_close_gaming_table.Location
          Me.panel_gaming_table_type.Location = New System.Drawing.Point(Me.panel_gaming_table_type.Location.X, Me.ef_gaming_table.Location.Y)

          'Me.ef_gaming_table_visits.IsReadOnly = False

          Me.ef_gaming_table_visits.Value = Me.m_cage_cls.GetGamblingTableVisits(Me.m_cage_cls.GamingTableId, Me.m_cage_cls.CashierSessionId)

          Me.rb_reception.Checked = True

          ' Set number of visits
          ' TODO
        Else
          If m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT Then
            Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Visible = True
          End If
          Me.rb_send.Checked = True
        End If

        If Me.m_cage_cls.OperationType <> Cage.CageOperationType.FromGamingTable And Me.m_cage_cls.OperationType <> Cage.CageOperationType.FromGamingTableClosing Then
          Me.ef_gaming_table_visits.Visible = False
        End If

        Me.rb_gaming_table.Checked = True

        Me.cmb_gaming_table.Value = CLng(m_cage_cls.GamingTableId)
        Me.ef_gaming_table.TextValue = Me.cmb_gaming_table.TextValue

        m_y_offset = (gb_gaming_table.Location.Y + gb_gaming_table.Size.Height) - (gb_movement_type.Location.Y + gb_movement_type.Size.Height)
        _panel_aux_x = Me.gb_gaming_table.Location.X
        _panel_aux_y = Me.gb_gaming_table.Location.Y + m_y_offset

      Case Cage.CageOperationType.ToCustom _
         , Cage.CageOperationType.FromCustom

        Me.rb_custom.Checked = True
        Me.cmb_custom.Visible = False
        Me.ef_custom.Visible = True
        Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Visible = False

        If Me.m_cage_cls.OperationType = Cage.CageOperationType.FromCustom Then

          If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT Then
            Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Visible = True
          End If

          Me.rb_reception.Checked = True
          Me.ef_custom.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3427)
        Else
          If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT Then
            Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Visible = True
          End If

          Me.rb_send.Checked = True
          Me.ef_custom.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3396)
        End If

        ' Set combo with source/target customs
        Call Me.m_cage_cls.SetComboCustom(Me.rb_send.Checked, Me.cmb_custom, True)
        Me.ef_custom.TextValue = Me.m_cage_cls.GetCustomUserName(Me.m_cage_cls.CustomUserId)
        Me.cmb_custom.Value = Me.m_cage_cls.CustomUserId

        m_y_offset = (gb_custom.Location.Y + gb_custom.Size.Height) - (gb_movement_type.Location.Y + gb_movement_type.Size.Height)
        _panel_aux_x = Me.gb_custom.Location.X
        _panel_aux_y = Me.gb_custom.Location.Y + m_y_offset

      Case Cage.CageOperationType.OpenCage _
         , Cage.CageOperationType.CloseCage _
         , Cage.CageOperationType.CountCage _
         , Cage.CageOperationType.GainQuadrature _
         , Cage.CageOperationType.LostQuadrature

        Me.gb_movement_type.Visible = False
        Me.gb_source_target.Visible = False

        Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Visible = False

        _panel_aux_x = Me.gb_movement_type.Location.X
        _panel_aux_y = Me.gb_movement_type.Location.Y

    End Select
  End Sub ' CheckRadioButtonsToSetLayouts_CancelOrViewMovement

  ''' <summary>
  ''' Set the radion buttons on the screen when the open mode is Count cage
  ''' </summary>
  ''' <param name="_panel_aux_x"></param>
  ''' <param name="_panel_aux_y"></param>
  ''' <remarks></remarks>
  Private Sub CheckRadioButtonsToSetLayouts_CountCage(ByRef _panel_aux_x As Integer, ByRef _panel_aux_y As Integer)
    Call Me.SetCageSessionEfValue()

    Me.gb_movement_type.Visible = False
    Me.gb_source_target.Visible = False

    ' cmb_cage_sessions.Count = 2 because when there is just one cash cage session open, the combo has 2 records, 
    ' one empty and another with the cash cage session.
    If cmb_cage_sessions.Count = 2 Then
      Me.cmb_cage_sessions.Visible = False
      Me.cmb_cage_sessions.SelectedIndex = Me.cmb_cage_sessions.Count - 1
      Me.ef_cage_session.Visible = True
      Me.ef_cage_session.TextValue = Me.cmb_cage_sessions.TextValue
      Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Enabled = Not GLB_CurrentUser.IsSuperUser
    ElseIf cmb_cage_sessions.Count > 0 Then
      ef_cage_session.Visible = False
      cmb_cage_sessions.IsReadOnly = False
      cmb_cage_sessions.SelectedIndex = -1
      cmb_cage_sessions.Visible = True
      Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Enabled = Not GLB_CurrentUser.IsSuperUser
    End If

    Me.ef_status_movement.TextValue = AUDIT_NONE_STRING
    Me.ef_date_cage.TextValue = GUI_FormatDate(Date.Now, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.ef_cage_user.TextValue = Me.m_cage_cls.GetUserFullName(CurrentUser.Id)

    _panel_aux_x = Me.gb_movement_type.Location.X
    _panel_aux_y = Me.gb_movement_type.Location.Y

  End Sub ' CheckRadioButtonsToSetLayouts_CountCage

  ''' <summary>
  ''' Set the radiobuttons when the open mode is close virtual
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub CheckRadioButtonsToSetLayouts_CloseVirtual()

    Call Me.SetDefaultValuesCombos()
    Me.rb_gaming_table.Checked = True
    Me.rb_reception.Checked = True
    Me.rb_send.Enabled = False
    Me.rb_reception.Enabled = False
    Me.rb_cashier.Enabled = False
    Me.rb_gaming_table.Enabled = False
    Me.rb_custom.Enabled = False
    Me.ef_gaming_table.Visible = True
    Me.ef_cage_session.Visible = True
    Me.gb_custom.Visible = False
    Me.gb_gaming_table.Visible = True
    Me.cmb_cage_sessions.Visible = False
    Me.cmb_gaming_table.Visible = False
    Me.ef_gaming_table_visits.Enabled = True
    Me.cb_close_gaming_table.Checked = True
    Me.cb_close_gaming_table.Enabled = False
    Me.btn_load_gaming_values.Visible = False
    Me.btn_load_gaming_values.Enabled = True

  End Sub ' CheckRadioButtonsToSetLayouts_CloseVirtual

  ''' <summary>
  ''' Set the radionbuttons on the screen when the open mode is recolect massive
  ''' </summary>
  ''' <param name="_panel_aux_x"></param>
  ''' <param name="_panel_aux_y"></param>
  ''' <remarks></remarks>
  Private Sub CheckRadioButtonsToSetLayouts_RecolectMassive(ByRef _panel_aux_x As Integer, ByRef _panel_aux_y As Integer)
    Call Me.SetCageSessionEfValue()

    Me.gb_movement_type.Enabled = False
    Me.gb_source_target.Enabled = False
    Me.panel_gaming_table_type.Visible = False
    Me.cmb_gaming_table.Visible = False
    Me.btn_load_gaming_values.Visible = False
    Me.ef_cage_user.TextValue = Me.m_cage_cls.GetUserFullName(Me.m_cage_cls.UserCageID)
    Me.ef_status_movement.TextValue = Me.GetStatusString(Me.m_cage_cls.Status, Me.m_cage_cls.OperationType)
    Me.ef_date_cage.TextValue = GUI_FormatDate(Me.m_cage_cls.MovementDatetime, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    If Utils.IsTitoMode() Then
      Me.ef_stacker_id.Enabled = True
    Else
      Me.ef_stacker_id.Visible = False
      Me.ef_floor_id.Location = New Point(Me.ef_stacker_id.Location.X - 21, Me.ef_stacker_id.Location.Y)
    End If
    Me.ef_floor_id.Enabled = True
    Call SetLayoutFromMoneyCollection(_panel_aux_x, _panel_aux_y)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Text = GLB_NLS_GUI_CONTROLS.GetString(13)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_CONTROLS.GetString(7)
    Me.CloseOnOkClick = False

  End Sub ' CheckRadioButtonsToSetLayouts_RecolectMassive

  ''' <summary>
  ''' Set the radioButtons when the open mode is template maintenance
  ''' </summary>
  ''' <param name="_panel_aux_x"></param>
  ''' <param name="_panel_aux_y"></param>
  ''' <remarks></remarks>
  Private Sub CheckRadioButtonsToSetLayouts_TemplateMaintenance(ByRef _panel_aux_x As Integer, ByRef _panel_aux_y As Integer)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False

    Me.rb_send.Checked = True
    Me.rb_reception.Enabled = False
    Me.rb_gaming_table.Checked = True
    Me.rb_gaming_table.Enabled = True
    Me.rb_cashier.Enabled = False
    Me.rb_custom.Enabled = False

    Me.cmb_gaming_table.Visible = False
    Me.ef_gaming_table.Visible = True
    Me.ef_gaming_table.TextValue = Me.m_cage_cls.CashierName

    Me.gb_cage_sessions.Visible = False
    Me.gb_template.Visible = True

    Me.gb_source_target.Visible = False
    Me.gb_movement_type.Visible = False
    Me.gb_cashier.Visible = False
    Me.gb_gaming_table.Visible = True
    Me.panel_gaming_table_type.Visible = False
    Me.btn_load_gaming_values.Visible = False
    Me.lb_gt_without_integrated_cashier.Visible = False
    Me.gb_custom.Visible = False
    Me.gb_operation.Visible = False

    _panel_aux_x = Me.gb_gaming_table.Location.X
    _panel_aux_y = 3

    Me.gb_gaming_table.Location = New Point(_panel_aux_x, _panel_aux_y)

    For Each _control As Control In Me.panel_aux.Controls
      If _control.Visible Then
        _panel_aux_x = _control.Location.X
        _panel_aux_y = _control.Location.Y - Me.gb_operation.Height

        _control.Location = New Point(_panel_aux_x, _panel_aux_y)
      End If
    Next

    Me.panel_aux.Height = Me.panel_aux.Height - Me.gb_operation.Height

    _panel_aux_x = Me.gb_gaming_table.Location.X
    _panel_aux_y = Me.gb_gaming_table.Location.Y + Me.gb_gaming_table.Height
  End Sub ' CheckRadioButtonsToSetLayouts_TemplateMaintenance

  ''' <summary>
  ''' Set the rest properties of the screen when is open it
  ''' </summary>
  ''' <param name="_panel_aux_x"></param>
  ''' <param name="_panel_aux_y"></param>
  ''' <remarks></remarks>
  Private Sub CheckRadioButtonsFinalSet(ByRef _panel_aux_x As Integer, ByRef _panel_aux_y As Integer)

    Dim _extra_offset_y As Integer

    Me.gb_cashier.Visible = rb_cashier.Checked
    Me.gb_gaming_table.Visible = rb_gaming_table.Checked
    Me.gb_custom.Visible = rb_custom.Checked
    Me.gb_collection_params.Visible = Me.rb_terminal.Checked

    If Me.rb_send.Checked Then
      Me.gb_source_target.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3396)
    ElseIf Me.rb_reception.Checked Then
      Me.gb_source_target.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3427)
    End If

    _extra_offset_y = 0

    If Me.m_is_tito_collection Then
      _extra_offset_y = 6
    End If

    Me.panel_aux.Location = New System.Drawing.Point(_panel_aux_x, _panel_aux_y + _extra_offset_y)

    If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MASSIVE AndAlso m_scanner_manager IsNot Nothing Then
      Me.Height = Me.panel_aux.Location.Y + Me.panel_aux.Height + 70
    Else
      ' RAB 16-MAR-2016: Bug 10658: Large height due to being in a different cashier and return to it radiobutton.
      Me.Height = Me.panel_aux.Location.Y + Me.panel_aux.Height + 78
    End If

    ' 11-JUL-2014 JPJ Fixed Bug WIGOS-1075: Templates should only be showen when sending.
    'We check if the template options should be enabled
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT Or Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.TEMPLATE_MAINTENANCE

    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = (CurrentUser.Permissions(ENUM_FORM.FORM_CAGE_CONTROL).Write AndAlso _
                                                          rb_send.Checked AndAlso _
                                                          ((Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT AndAlso Me.m_mode_edit = MODE_EDIT.NONE) _
                                                           Or Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.TEMPLATE_MAINTENANCE))

    Me.gb_template.Visible = Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT
    Me.cmb_cage_template.Enabled = (CurrentUser.Permissions(ENUM_FORM.FORM_CAGE_CONTROL).Read AndAlso _
                                    rb_send.Checked AndAlso _
                                    Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT)

    Call AddRemoveHandler(True)
  End Sub ' CheckRadioButtonsFinalSet

  

  ' PURPOSE: Set the enter field value for cage sassion.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetCageSessionEfValue()

    If Me.m_cage_cls.OperationType = Cage.CageOperationType.RequestOperation _
        And Me.m_cage_cls.CageSessionId = 0 Then

      If Me.m_cage_cls.Status = Cage.CageStatus.Canceled Then
        Me.cmb_cage_sessions.Visible = False
        Me.ef_cage_session.Visible = True
        Me.ef_cage_session.TextValue = AUDIT_NONE_STRING
      Else

        Me.cmb_cage_sessions.Visible = True
        Me.ef_cage_session.Visible = False

        If Me.m_cage_cls.Status = Cage.CageStatus.Sent Then
          If Me.m_cage_cls.CageSessionId <> 0 Then

            Me.cmb_cage_sessions.Visible = False
            Me.ef_cage_session.Visible = True
          End If
        End If

      End If
    Else
      Me.cmb_cage_sessions.Visible = False
      Me.cmb_cage_sessions.Value = Me.m_cage_cls.CageSessionId
      Me.ef_cage_session.Visible = True
      Me.ef_cage_session.TextValue = Me.cmb_cage_sessions.TextValue
    End If

    If String.IsNullOrEmpty(Me.ef_cage_session.TextValue) Then
      ' if it's null or empty means that cage session is already closed
      Me.ef_cage_session.TextValue = Cage.GetCageSessionName(Me.m_cage_cls.CageSessionId)
      Me.cmb_cage_sessions.Value = -1
      'Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Enabled = False
    End If

  End Sub ' SetCageSessionEfValue


  ' PURPOSE: Checks if current stock is OK. If current stock is not ok updates tabcurrencies and shows error message.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - true if current stock is ok, false if not
  Private Function CheckAndUpdateStockData() As Boolean
    Dim _sql_cmd As SqlCommand
    Dim _tab_page As TabPage
    Dim _table As DataTable
    Dim _iso_code As String
    Dim _currency_type As CageCurrencyType

    _sql_cmd = New SqlCommand("CageCurrentStock")
    _sql_cmd.CommandType = CommandType.StoredProcedure

    _table = GUI_GetTableUsingCommand(_sql_cmd, Integer.MaxValue)

    If Not (m_cage_cls.CheckChangeStock(m_cage_cls.LoadedStock, _table)) Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7179), ENUM_MB_TYPE.MB_TYPE_WARNING)

      m_cage_cls.SetGlobalCageValues(_table, Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE, True)
      For Each _tab_page In Me.tb_currency.TabPages

        If TypeOf _tab_page.Controls(0) Is uc_grid Then
          _iso_code = _tab_page.Tag.ToString().Substring(0, ISO_CODE_LENGTH)
          If _iso_code.Equals(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2941)) Then
            _iso_code = Cage.CHIPS_ISO_CODE
          End If

          _currency_type = _tab_page.Tag.ToString().Substring(ISO_CODE_LENGTH, _tab_page.Tag.ToString().Length - ISO_CODE_LENGTH)

          Call Me.SetCurrencyGridData(_iso_code, _currency_type, _tab_page.Controls(0))
        End If

      Next

      RemoveHandler dg_total.RowSelectedEvent, AddressOf dg_total_RowSelectedEvent

      Call Me.dg_total.Clear()
      InitTotalGrid(Me.m_cage_cls.GetAllowedISOCode())
      FirstUpdateTotalGrid(False)

      m_cage_cls.AddExpectedColumnInCurrencies()
      AddHandler dg_total.RowSelectedEvent, AddressOf dg_total_RowSelectedEvent

      For Each _tab_page In Me.tb_currency.TabPages

        _iso_code = _tab_page.Tag.ToString().Substring(0, ISO_CODE_LENGTH)
        _currency_type = _tab_page.Tag.ToString().Substring(ISO_CODE_LENGTH, _tab_page.Tag.ToString().Length - ISO_CODE_LENGTH)

        If TypeOf _tab_page.Controls(0) Is uc_grid Then
          Call Me.SetCurrencyGridData(_iso_code, _currency_type, _tab_page.Controls(0))
        End If

        m_cage_cls.LoadedStock = _table

        Return False
      Next
    End If

    Return True
  End Function


  ' PURPOSE: Set the form components for user o terminal.
  '
  '  PARAMS:
  '     - INPUT:
  '           - ToUser
  '           - ToTermianl
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetToUserToTerminal(ByVal ToUser As Boolean, ByVal ToTermianl As Boolean)
    Me.cmb_user.Visible = ToUser
    Me.cmb_terminals.Visible = ToTermianl
    Me.ef_cashier_name.Visible = ToUser
    Me.ef_cashier_date.Visible = ToUser

    If ToUser Then
      Me.gb_cashier.Height = GB_CASHIER_HEIGHT_TO_USER
    ElseIf ToTermianl Then
      Me.gb_cashier.Height = GB_CASHIER_HEIGHT_TO_TERMINAL
    End If


    If Me.m_gaming_values Then
      Me.m_gaming_values = False
      Call Me.ReDrawGrids()
    End If

  End Sub ' SetToUserToTerminal

  ' PURPOSE: checks if screen data ok for new movements
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function CheckScreenDataOkNewMovement() As Boolean
    Dim _max_allowed As Long
    Dim _iso_code As String

    Dim _cage_session_gaming_day As DateTime = Nothing

    _iso_code = ""

    If Me.m_gaming_day Then
      ' When gaming day active, pick the cage session working day
      Using _db_trx As New DB_TRX()
        _cage_session_gaming_day = Cage.GetCageSessionWorkingDayById(Me.cmb_cage_sessions.Value, _db_trx.SqlTransaction)
      End Using
    End If

    If Me.cmb_cage_sessions.Visible Then

      If String.IsNullOrEmpty(Me.cmb_cage_sessions.TextValue) Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4554), ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.cmb_cage_sessions.Text) ' Select a cash cage session
        Me.cmb_cage_sessions.Focus()

        Return False
      End If

      If Not Cage.GetCageOpenBySessionId(Me.cmb_cage_sessions.Value) Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4603), ENUM_MB_TYPE.MB_TYPE_WARNING) ' Cash cage session is closed
        Me.cmb_cage_sessions.Focus()

        Return False
      End If
    End If

    If Me.rb_cashier.Checked Then
      If Me.rb_to_user.Checked Then
        If String.IsNullOrEmpty(Me.cmb_user.TextValue) Then
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3029), ENUM_MB_TYPE.MB_TYPE_WARNING) ' Select a user
          Me.cmb_user.Focus()

          Return False
        End If

        If Me.m_cage_cls.CheckPendingMovement(Me.cmb_user.Value, _cage_session_gaming_day) Then
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2942), ENUM_MB_TYPE.MB_TYPE_WARNING) ' User with pending movements
          Me.cmb_user.Focus()

          Return False
        End If
      ElseIf Me.rb_to_terminal.Checked Then
        If String.IsNullOrEmpty(Me.cmb_terminals.TextValue) Then
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3451), ENUM_MB_TYPE.MB_TYPE_WARNING) ' Select a terminal
          Me.cmb_user.Focus()

          Return False
        End If

        ' 13-APR-2015   RMS   Gaming Day
        If m_gaming_day Then
          Using _db_trx As New DB_TRX()
            If Not Cage.CheckSameGamingDay(_cage_session_gaming_day, Me.cmb_terminals.Value, True, _db_trx.SqlTransaction) Then
              NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6178), ENUM_MB_TYPE.MB_TYPE_WARNING) ' Not the same gaming day between cashier session and selected cage session

              Return False
            End If
          End Using
        End If

      End If 'If Me.rb_to_user.Checked
    Else 'Me.rb_cashier.Checked 
      If Me.rb_custom.Checked Then ' To Custom
        If String.IsNullOrEmpty(Me.cmb_custom.TextValue) Then
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.cmb_custom.Text) ' Select a custom
          Me.cmb_custom.Focus()

          Return False
        End If
        If Not Me.m_cage_cls.CheckIfStillExistCustom(CLng(Me.cmb_custom.Value)) Then
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4456), ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.cmb_custom.Text) ' Custom was deleted
          Me.cmb_custom.Focus()

          Return False
        End If
      ElseIf Me.rb_gaming_table.Checked Then ' To Gaming table
        If String.IsNullOrEmpty(Me.cmb_gaming_table.TextValue) Then
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.cmb_gaming_table.Text) ' Select a gaming table
          Me.cmb_gaming_table.Focus()

          Return False
        End If
        ' RCI 09-OCT-2014: Only check if gambling table is enabled if its a send (accept recolection even the table is disabled)
        If rb_send.Checked AndAlso Not Me.m_cage_cls.CheckIfGamblingTableStillEnabled(Me.cmb_gaming_table.Value) Then
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4457), ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.cmb_gaming_table.Text) ' Gambling table disabled
          Me.cmb_gaming_table.Focus()

          Return False
        End If

        If Me.cb_close_gaming_table.Checked Then
          If String.IsNullOrEmpty(ef_gaming_table_visits.Value) OrElse ef_gaming_table_visits.Value < 0 Then
            ef_gaming_table_visits.Value = 0
          End If
        End If
      ElseIf Me.rb_terminal.Checked Then
        If Not CheckImbalance() Then

          Return False
        End If
      End If
    End If 'Me.rb_cashier.Checked 

    If Not Me.rb_terminal.Checked Then
      'DLL 20-10-2014 Check total because Quantity is 1 when report Coins

      If Not Me.m_cage_cls.TableCurrencies.Select("QUANTITY > 0").Length > 0 Then
        If Me.cb_close_gaming_table.Checked Then
          If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4458), _
                                ENUM_MB_TYPE.MB_TYPE_WARNING, _
                                ENUM_MB_BTN.MB_BTN_YES_NO) <> ENUM_MB_RESULT.MB_RESULT_YES Then
            Return False
          End If
        Else
          If Me.m_cage_cls.OperationType <> Cage.CageOperationType.ToGamingTable Then ' Gambling tables allow sending without amount. Otherwise you can't send without amount
            NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3028), ENUM_MB_TYPE.MB_TYPE_WARNING) ' Introduce quantities
            Me.tb_currency.SelectedTab.Controls(0).Focus()
            Return False
          End If
        End If
      End If
    End If

    If Me.rb_send.Checked Then
      If Me.rb_cashier.Checked Then
        If Not CheckMaxAllowedDeposit(_iso_code, _max_allowed) Then
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3328), ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.m_cage_cls.FormatAmount(_max_allowed, False, _iso_code)) ' Amount limit exceded.

          Return False
        End If
      ElseIf Me.rb_gaming_table.Checked Then
        If Not CheckMaxAllowedDeposit(_iso_code, _max_allowed) Then
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3328), ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.m_cage_cls.FormatAmount(_max_allowed, False, _iso_code)) ' Amount limit exceded.

          Return False
        End If
      End If
    End If

    If Me.rb_send.Checked Then
      'checks if there is enough stock in cage
      If Not CheckCageStock() Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3436), ENUM_MB_TYPE.MB_TYPE_WARNING)

        Return False
      End If

      If (WSI.Common.Misc.IsGamingDayEnabled() AndAlso _cage_session_gaming_day < WSI.Common.Misc.TodayOpening() AndAlso Not Me.rb_custom.Checked) Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6302), ENUM_MB_TYPE.MB_TYPE_WARNING) ' Not the same gaming day between cashier session and selected cage session

        Return False
      End If
    End If

    If Not CheckConcepts() Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5602), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Return False
    End If

    Return True
  End Function ' CheckScreenDataOkNewMovement

  ' PURPOSE: checks if screen data ok for new movements
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function CheckScreenDataRecolectMovement() As Boolean
    Dim _iso_code As String
    Dim _max_allowed As Long

    _iso_code = ""

    If Not CheckIfDenominationsAllowed() Then
      Me.tb_currency.SelectedTab.Controls(0).Focus()

      Return False
    End If

    If Me.m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal And Me.m_cage_cls.CageSessionStatus = CASHIER_SESSION_STATUS.CLOSED Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8744), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    ElseIf Me.m_cage_cls.OperationType <> Cage.CageOperationType.FromTerminal Then
      If Me.m_cage_cls.MovementID > 0 And Not Me.m_cage_cls.CheckIfSameStatus(Me.m_cage_cls.MovementID, Cage.CageStatus.Sent) Then
        If Me.m_cage_cls.OperationType = Cage.CageOperationType.RequestOperation Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4834), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Else
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4467), ENUM_MB_TYPE.MB_TYPE_WARNING)
        End If

        Return False
      End If

    End If

    If Me.m_cage_cls.OperationType = Cage.CageOperationType.RequestOperation Then
      If Me.m_cage_cls.Status = Cage.CageStatus.Canceled Then

        Return True
      Else
        If Me.m_cage_cls.OperationType = Cage.CageOperationType.RequestOperation Then
          If Not Me.m_cage_cls.TableCurrencies.Select("QUANTITY > 0").Length > 0 Then
            NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3028), ENUM_MB_TYPE.MB_TYPE_WARNING) ' Introduce quantities
            Me.tb_currency.SelectedTab.Controls(0).Focus()

            Return False
          End If

          If Not CheckMaxAllowedDeposit(_iso_code, _max_allowed) Then
            NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3328), ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.m_cage_cls.FormatAmount(_max_allowed, False, _iso_code)) ' Amount limit exceded.

            Return False
          End If

          ' Select a cash cage session
          If String.IsNullOrEmpty(Me.cmb_cage_sessions.TextValue) AndAlso (String.IsNullOrEmpty(Me.ef_cage_session.TextValue) Or Me.ef_cage_session.TextValue = AUDIT_NONE_STRING) Then
            NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4554), ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.cmb_cage_sessions.Text)
            Me.cmb_cage_sessions.Focus()

            Return False
          End If

          'checks if there is enough stock in cage
          If Not CheckCageStock() Then
            NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3436), ENUM_MB_TYPE.MB_TYPE_WARNING)

            Return False
          End If
        End If
      End If
    End If

    If Not CheckImbalance() Then

      Return False
    End If

    If Not CheckConcepts() Then
      If Me.rb_reception.Checked And Me.rb_cashier.Checked Then
        If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5603), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_NO Then
          Return False
        End If
      Else
        Return False
      End If
    End If

    Return True
  End Function ' CheckScreenDataRecolectMovement

  ' PURPOSE: compares tablecurrencies with current stock
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - true if there is current stock, false if not
  Private Function CheckCageStock() As Boolean
    Dim _row As DataRow
    Dim _row_theoric As DataRow()
    Dim _iso_code As String
    Dim _currency_type As CageCurrencyType
    Dim _denomination As Double
    Dim _quantity As Integer
    Dim _quantity_coins As Double
    Dim _theoric_quantity As Double
    Dim _str_filter As String

    CheckCageStock = True

    _row_theoric = Nothing

    If Me.m_cage_cls.GetGlobalCageStock(False) Then
      For Each _row In Me.m_cage_cls.TableCurrencies.Rows
        If Not _row("QUANTITY") Is DBNull.Value AndAlso Not _row("DENOMINATION") = Cage.COINS_COLLECTION_CODE Then
          _iso_code = _row("ISO_CODE")
          _currency_type = _row("TYPE")
          If _currency_type = FeatureChips.ChipType.COLOR Then
            _denomination = 0
          Else
            _denomination = _row("DENOMINATION")
          End If
          _quantity = _row("QUANTITY")
          _quantity_coins = IIf(_row("TOTAL") Is DBNull.Value, 0, _row("TOTAL"))

          If _currency_type = CageCurrencyType.Bill Then
            _str_filter = String.Format("ISO_CODE = '{0}' AND DENOMINATION = {1} AND TYPE IN ({2}, {3}) ", _iso_code, GUI_LocalNumberToDBNumber(_denomination.ToString), CInt(_currency_type), CInt(CageCurrencyType.Coin))
          ElseIf FeatureChips.IsCageCurrencyTypeChips(_currency_type) Then
            _str_filter = String.Format("ISO_CODE = '{0}' AND TYPE = {1} AND CHIP_ID = {2} ", _iso_code, CInt(_currency_type), _row("CHIP_ID"))
          Else
            _str_filter = String.Format("ISO_CODE = '{0}' AND DENOMINATION = {1} AND TYPE = {2} ", _iso_code, GUI_LocalNumberToDBNumber(_denomination.ToString), CInt(_currency_type))
          End If

          _row_theoric = Me.m_cage_cls.TableTheoricValues.Select(_str_filter)
          If _row_theoric.Length > 0 Then
            If _quantity > 0 Then
              If _denomination < 0 Then
                _theoric_quantity = _row_theoric(0).Item("CASHIER_TOTAL")
                If _theoric_quantity < _quantity_coins Then
                  CheckCageStock = False

                  Exit For
                End If
              Else
                _theoric_quantity = _row_theoric(0).Item("CASHIER_QUANTITY")
                If _theoric_quantity < _quantity Then
                  CheckCageStock = False

                  Exit For
                End If
              End If
            End If
          End If
        End If
      Next
    Else
      CheckCageStock = False
    End If

    ' Dispose
    Call SafeDispose(_row_theoric)

    Return CheckCageStock

  End Function ' CheckCageStock

  ' PURPOSE: Return if introduced concepts is not greather than amount
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - true if introduced concepts is not greather than amount
  Private Function CheckConcepts() As Boolean
    Dim _total_amount As Object
    Dim _total_concepts As Decimal
    Dim _concepts_data As DataTable
    Dim _iso_code As String
    Dim _currency_type As CageCurrencyType

    If Me.rb_reception.Checked And Me.rb_cashier.Checked Then
      Return True
    End If

    _concepts_data = Me.GetConceptsDataTableFromDG(Me.dg_concepts, True)

    For Each _row As DataRow In m_currencies_types.Rows
      _iso_code = _row("CGC_ISO_CODE")
      _currency_type = _row("TYPE")

      _total_amount = Me.m_cage_cls.TableCurrencies.Compute("Sum(TOTAL)", "ISO_CODE = '" & _iso_code & "' AND TYPE = '" & _currency_type & "' ")

      _total_concepts = 0
      If _concepts_data.Select("CONCEPT_ISO_CODE = '" & _iso_code & "' AND CONCEPT_CAGE_CURRENCY_TYPE = '" & _currency_type & "' ").Length > 0 Then
        _total_concepts = _concepts_data.Compute("Sum(CONCEPT_AMOUNT)", "CONCEPT_ISO_CODE = '" & _iso_code & "' AND CONCEPT_CAGE_CURRENCY_TYPE = '" & _currency_type & "' ")
      End If

      If _total_amount Is Nothing OrElse _total_amount Is DBNull.Value Then
        _total_amount = 0
      End If

      If CDec(_total_amount) < _total_concepts Then
        Return False
      End If
    Next

    ' Dispose
    Call SafeDispose(_concepts_data)
    'Call SafeDispose(m_currencies_types)

    Return True

  End Function ' CheckTips


  ' PURPOSE: Redraw grids
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ReDrawGrids(Optional ByVal IsGamblingCollection As Boolean = False,
                          Optional ByVal IsCustomCollection As Boolean = False,
                          Optional ByVal IsCountRCollection As Boolean = False)
    Try

      Dim _tab As TabPage
      Dim _uc_grid As uc_grid
      Dim _iso_code As String
      Dim _currency_type As CageCurrencyType
      Dim _currency_iso_code As String
      Dim _deleted_currencys As System.Collections.ObjectModel.Collection(Of DeletedCurrencys) = Nothing
      Dim _add_denom_send_custom As Boolean

      _currency_iso_code = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")

      If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT And m_uc_grid_denominations_created Then

        If Me.rb_send.Checked Then

          _deleted_currencys = GridDeleteNegativeDenominations()

          For Each _tab In Me.tb_currency.TabPages
            If TypeOf _tab.Controls(0) Is uc_grid Then
              If TypeOf _tab.Controls(0) Is uc_grid Then
                _uc_grid = _tab.Controls(0)

                If Not String.IsNullOrEmpty(_tab.Tag) Then
                  _iso_code = _tab.Tag.ToString().Substring(0, ISO_CODE_LENGTH)
                  _currency_type = _tab.Tag.ToString().Substring(ISO_CODE_LENGTH, _tab.Tag.ToString().Length - ISO_CODE_LENGTH)

                Else
                  _iso_code = _uc_grid.Cell(0, GRID_CURRENCY_COLUMN_ISO_CODE).Value
                  _currency_type = _uc_grid.Cell(0, GRID_CURRENCY_COLUMN_CURRENCY_TYPE).Value
                End If

                If Not FeatureChips.IsCurrencyExchangeTypeChips(_currency_type) Then
                  _currency_type = CageCurrencyType.Others ' _uc_grid.Cell(0, GRID_CURRENCY_COLUMN_ISO_TYPE).Value
                  _add_denom_send_custom = Me.rb_send.Checked AndAlso Me.rb_custom.Checked 'AndAlso (Me.rb_custom.Checked OrElse Me.rb_cashier.Checked)
                  ' if the new movement type is send and also the destination is customed 
                  ' RAB 16-MAR-2016: Bug 10658
                  'If Utils.IsTitoMode AndAlso Not _add_denom_send_custom Then
                  ' JMV 20-JUN-2016: Bug 14686
                  If _add_denom_send_custom AndAlso Not IsCountRCollection Then
                    Call GridAddNegativeDenominations(Cage.BANK_CARD_CODE, _uc_grid, _iso_code, _currency_type)
                  End If

                  ' if the new movement type is send and also the destination is customed 
                  If _add_denom_send_custom Then
                    Call GridAddNegativeDenominations(Cage.CHECK_CODE, _uc_grid, _iso_code, _currency_type)
                  End If

                  If (Not IsCountRCollection) Then
                    Call GridAddNegativeDenominations(Cage.COINS_CODE, _uc_grid, _iso_code, DeletedCurrencys.SearchDenomination(Cage.COINS_CODE, _iso_code, _currency_type, _deleted_currencys))
                  End If

                  If Utils.IsTitoMode AndAlso _add_denom_send_custom AndAlso _iso_code = _currency_iso_code Then
                    Call GridAddNegativeDenominations(Cage.TICKETS_CODE, _uc_grid, _iso_code, _currency_type)
                  End If
                End If

                Substract(_iso_code, _deleted_currencys)
              End If
            End If
          Next

        Else 'Me.rb_reception.Checked
          _deleted_currencys = GridDeleteNegativeDenominations()

          For Each _tab In Me.tb_currency.TabPages
            If TypeOf _tab.Controls(0) Is uc_grid Then
              If TypeOf _tab.Controls(0) Is uc_grid Then
                _uc_grid = _tab.Controls(0)
                ' LTC 2017-JUL-12
                If Not String.IsNullOrEmpty(_tab.Tag) Then
                  _iso_code = _tab.Tag.ToString().Substring(0, ISO_CODE_LENGTH)
                  _currency_type = _tab.Tag.ToString().Substring(ISO_CODE_LENGTH, _tab.Tag.ToString().Length - ISO_CODE_LENGTH)
                Else
                  _iso_code = _uc_grid.Cell(0, GRID_CURRENCY_COLUMN_ISO_CODE).Value
                  _currency_type = _uc_grid.Cell(0, GRID_CURRENCY_COLUMN_CURRENCY_TYPE).Value
                End If



                If Utils.IsTitoMode AndAlso IsCountRCollection Then
                  Call GridAddNegativeDenominations(Cage.TICKETS_CODE, _uc_grid, _iso_code, DeletedCurrencys.SearchDenomination(Cage.TICKETS_CODE, _iso_code, _currency_type, _deleted_currencys))
                ElseIf _iso_code = _currency_iso_code & CageCurrencyType.Bill And Me.rb_gaming_table.Checked Then
                  ' LTC 28-NOV-2016
                  If Me.cb_close_gaming_table.Checked Then
                    Call GridAddNegativeDenominations(Cage.BANK_CARD_CODE, _uc_grid, _iso_code, DeletedCurrencys.SearchDenomination(Cage.BANK_CARD_CODE, _iso_code, _currency_type, _deleted_currencys))
                  End If
                  Call GridAddNegativeDenominations(Cage.CHECK_CODE, _uc_grid, _iso_code, DeletedCurrencys.SearchDenomination(Cage.CHECK_CODE, _iso_code, _currency_type, _deleted_currencys))
                  Call GridAddNegativeDenominations(Cage.COINS_CODE, _uc_grid, _iso_code, DeletedCurrencys.SearchDenomination(Cage.COINS_CODE, _iso_code, _currency_type, _deleted_currencys))
                Else
                  If Not FeatureChips.IsCurrencyExchangeTypeChips(_currency_type) Then
                    Call GridAddNegativeDenominations(Cage.COINS_CODE, _uc_grid, _iso_code, DeletedCurrencys.SearchDenomination(Cage.COINS_CODE, _iso_code, _currency_type, _deleted_currencys))
                  End If
                End If

                Substract(_iso_code, _deleted_currencys)

              End If
            End If
          Next
        End If
      End If 'If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT And m_uc_grid_denominations_created Then

      If m_uc_grid_denominations_created Then
        Return
      End If

      m_uc_grid_denominations_created = True

      Dim _table_currencies As DataTable

      If m_gaming_values Then
        GRID_CURRENCY_COLUMN_CASHIER_QUANTITY_WIDTH = 1005
        GRID_CURRENCY_COLUMN_CASHIER_TOTAL_WIDTH = 1450
        GRID_TOTAL_COLUMN_TOTAL_CASHIER_WITH_SIMBOL_WIDTH = 1560

        GRID_CURRENCY_COLUMN_QUANTITY_DIFFERENCE_WIDTH = 1005
        GRID_CURRENCY_COLUMN_TOTAL_DIFFERENCE_WIDTH = 1450
        GRID_TOTAL_COLUMN_DIFFERENCE_TOTAL_WITH_SIMBOL_WIDTH = 1560
      Else
        If m_cage_cls.OpenMode <> CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE Then
          GRID_CURRENCY_COLUMN_QUANTITY_DIFFERENCE_WIDTH = 0
          GRID_CURRENCY_COLUMN_CASHIER_QUANTITY_WIDTH = 0
          GRID_CURRENCY_COLUMN_CASHIER_TOTAL_WIDTH = 0
          GRID_CURRENCY_COLUMN_TOTAL_DIFFERENCE_WIDTH = 0
          GRID_CURRENCY_COLUMN_QUANTITY_DIFFERENCE_WIDTH = 0
          GRID_TOTAL_COLUMN_TOTAL_CASHIER_WITH_SIMBOL_WIDTH = 0
          GRID_TOTAL_COLUMN_DIFFERENCE_TOTAL_WITH_SIMBOL_WIDTH = 0
        End If

        Call Me.m_cage_cls.InitCurrencyData(True)
      End If

      _table_currencies = New DataTable

      Call Me.GUI_StyleSheet()
      _table_currencies = Me.m_cage_cls.GetAllowedISOCode()
      Call Me.InitTotalGrid(_table_currencies)

      For Each _tab In Me.tb_currency.TabPages

        If TypeOf _tab.Controls(0) Is uc_grid Then

          If _tab.Text.Equals(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2941)) Then ' Chips
            _iso_code = Cage.CHIPS_ISO_CODE
          Else
            _iso_code = _tab.Text
          End If

          _currency_type = _tab.Tag.ToString().Substring(ISO_CODE_LENGTH, _tab.Tag.ToString().Length - ISO_CODE_LENGTH)

          Call Me.SetDataGridShowDenominationsGP(_iso_code, _currency_type)
          Call Me.SetStyleSheetGrid(_tab.Controls(0), _iso_code, _currency_type)
          Call Me.SetCurrencyGridData(_iso_code, _currency_type, _tab.Controls(0), IsGamblingCollection, IsCustomCollection)

        End If

      Next

      Call Me.FirstUpdateTotalGrid()

      'Me.m_jca_movement_type_checked = IIf(Me.rb_send.Checked, Me.rb_send.Name, Me.rb_reception.Name)
      'Me.m_jca_source_target_checked = IIf(Me.rb_custom.Checked, Me.rb_custom.Name, "OTHER")

      'AddHandler dg_total.RowSelectedEvent, AddressOf dg_total_RowSelectedEvent

      Call SelectTab(0)

    Catch ex As Exception

    End Try
  End Sub ' ReDrawGrids


  Private Sub Substract(ByVal IsoCode As String, ByVal CurrencysDeleted As System.Collections.ObjectModel.Collection(Of DeletedCurrencys))
    ' substract in totals grid
    Dim _idx_row As Integer

    For _idx_row = dg_total.NumRows - 1 To 0 Step -1

      If dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.CURRENCY).Value = IsoCode Then

        dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.COINS).Value = dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.COINS).Value - DeletedCurrencys.PendingDelete(IsoCode, CurrencysDeleted)
        dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.COINS_WITH_SIMBOL).Value = Me.m_cage_cls.FormatAmount(dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.COINS).Value, False, IsoCode)
        dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.TOTAL).Value = dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.TOTAL).Value - DeletedCurrencys.PendingDelete(IsoCode, CurrencysDeleted)
        dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.TOTAL_WITH_SIMBOL).Value = Me.m_cage_cls.FormatAmount(dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.TOTAL).Value, False, IsoCode)
      End If

    Next
  End Sub

  ' PURPOSE: Add denomination to Grid (Coins, Cards, Check, Tickets, etc)
  '
  '  PARAMS:
  '     - INPUT:
  '           - Denomination Code
  '           - Grid to add denomination
  '           - IsoCode
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GridAddNegativeDenominations(ByVal Denomination As Integer, ByRef UcGrid As uc_grid, ByVal IsoCode As String, ByVal IsoType As Int32, Optional ByVal value As Double = 0)
    Dim _idx_row As Integer

    UcGrid.AddRow()
    _idx_row = UcGrid.NumRows - 1
    UcGrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_ISO_CODE).Value = IsoCode
    UcGrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_CURRENCY_TYPE).Value = IsoType
    UcGrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_DENOMINATION).Value = Denomination

    If Denomination = Cage.COINS_CODE _
      And Me.m_show_denominations <> Cage.ShowDenominationsMode.ShowAllDenominations _
        And IsoType <> FeatureChips.ChipType.COLOR Then

      UcGrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_DENOMINATION_WITH_SIMBOL).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3372) ' Monto
    Else
      UcGrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_DENOMINATION_WITH_SIMBOL).Value = Me.m_cage_cls.GetCurrencyDescription(IsoCode, Denomination) ' Coins, Cards, Check, Tickets
    End If
    If value > 0 Then
      UcGrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_TOTAL).Value = value
      UcGrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).Value = Me.m_cage_cls.FormatAmount(value, False, IsoCode)

    End If

  End Sub

  ' PURPOSE: Delete all negative denominations Coins, Cards, Check, Tickets
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GridDeleteNegativeDenominations() As System.Collections.ObjectModel.Collection(Of DeletedCurrencys)


    Dim _idx_row As Integer
    Dim _tab As TabPage
    Dim _uc_grid As uc_grid
    Dim _deleted_currencys As System.Collections.ObjectModel.Collection(Of DeletedCurrencys) = Nothing
    Dim _deleted_currency As DeletedCurrencys
    For Each _tab In Me.tb_currency.TabPages

      If TypeOf _tab.Controls(0) Is uc_grid Then
        _uc_grid = _tab.Controls(0)

        For _idx_row = _uc_grid.NumRows - 1 To 0 Step -1

          If _uc_grid.Cell(_idx_row, GRID_CURRENCY_COLUMN_DENOMINATION).Value < 0 Then
            If (Me.m_cage_cls.IsCountRTerminal AndAlso _
                (_uc_grid.Cell(_idx_row, GRID_CURRENCY_COLUMN_DENOMINATION).Value = Cage.COINS_CODE OrElse _uc_grid.Cell(_idx_row, GRID_CURRENCY_COLUMN_DENOMINATION).Value = Cage.TICKETS_CODE)) Then
              Continue For
            End If

            If IsNumeric(_uc_grid.Cell(_idx_row, GRID_CURRENCY_COLUMN_TOTAL).Value) AndAlso _uc_grid.Cell(_idx_row, GRID_CURRENCY_COLUMN_TOTAL).Value > 0 Then
              If _deleted_currencys Is Nothing Then
                _deleted_currencys = New System.Collections.ObjectModel.Collection(Of DeletedCurrencys)
              End If
              _deleted_currency = New DeletedCurrencys
              _deleted_currency.Import = _uc_grid.Cell(_idx_row, GRID_CURRENCY_COLUMN_TOTAL).Value
              _deleted_currency.CurrencyId = _uc_grid.Cell(_idx_row, GRID_CURRENCY_COLUMN_DENOMINATION).Value
              _deleted_currency.IsoCode = _uc_grid.Cell(_idx_row, GRID_CURRENCY_COLUMN_ISO_CODE).Value
              _deleted_currency.IsoType = _uc_grid.Cell(_idx_row, GRID_CURRENCY_COLUMN_CURRENCY_TYPE).Value
              _deleted_currencys.Add(_deleted_currency)
            End If
            _uc_grid.IsSelected(_idx_row) = False
            _uc_grid.DeleteRowFast(_idx_row)
          End If

        Next
      End If

    Next

    Return _deleted_currencys
  End Function

  ' PURPOSE: Initialize the form layout configuration
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub InitialLayoutConfiguration()
    Me.btn_load_gaming_values.Visible = False

    Me.gb_cashier.Visible = rb_cashier.Checked
    Me.gb_gaming_table.Visible = rb_gaming_table.Checked
    Me.gb_gaming_table.Location = New System.Drawing.Point(Me.gb_cashier.Location.X, Me.gb_cashier.Location.Y)
    Me.gb_custom.Visible = rb_custom.Checked
    Me.gb_custom.Location = New System.Drawing.Point(Me.gb_cashier.Location.X, Me.gb_cashier.Location.Y)

    Me.gb_cashier.Height = GB_CASHIER_HEIGHT_TO_USER ' 87
    Me.cmb_terminals.Location = New System.Drawing.Point(cmb_user.Location.X, cmb_user.Location.Y)
    Me.ef_user_terminals_name.Location = New System.Drawing.Point(cmb_user.Location.X, cmb_user.Location.Y)
    Me.cmb_custom.Location = New System.Drawing.Point(ef_custom.Location.X, cmb_custom.Location.Y)

    Me.gb_gaming_table.Height = GB_CASHIER_HEIGHT_TO_GAMING_TABLE
    Me.ef_gaming_table.Location = New System.Drawing.Point(cmb_gaming_table.Location.X, cmb_gaming_table.Location.Y)

    m_y_offset = (gb_cashier.Location.Y + gb_cashier.Size.Height) - (gb_movement_type.Location.Y + gb_movement_type.Size.Height)
    Me.panel_aux.Location = New System.Drawing.Point(Me.gb_cashier.Location.X, Me.gb_cashier.Location.Y + m_y_offset)
    Me.Height = Me.panel_aux.Location.Y + Me.panel_aux.Height + 50

    Call Me.CheckRadioButtonsToSetLayouts()

    Me.cmb_user.AutoCompleteMode = False
    Me.cmb_terminals.AutoCompleteMode = False
    Me.cmb_custom.AutoCompleteMode = False
    Me.cmb_gaming_table.AutoCompleteMode = False
    Me.cmb_cage_sessions.AutoCompleteMode = False

  End Sub ' InitialLayoutConfiguration

  ' PURPOSE: Select the only cash cage session opened
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SelectCageSessionOpened()
    If Cage.GetNumberOfOpenedCages() = 1 And _
      (Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT Or _
        (Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MOVEMENT And _
          Me.m_cage_cls.OperationType = Cage.CageOperationType.RequestOperation) Or _
            Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE) Then

      'set default cage session id only if there is only 1 open cage session and it's a NEW MOVEMENT
      Dim _cage_session_id As Long
      'Call Cage.GetOpenCageSessionId(_cage_session_id, False)
      Call Cage.GetCurrentCageSessionId(_cage_session_id, False)
      Me.m_cage_cls.CageSessionId = _cage_session_id
      Me.cmb_cage_sessions.Value = _cage_session_id
      Me.cmb_cage_sessions.IsReadOnly = True
    End If
  End Sub ' SelectCageSessionOpened

  ' PURPOSE: Print Cage voucher
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  ' PURPOSE: Print Cage voucher
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub PrintCageVoucherNewOperation()
    Dim _cage_movement As CLASS_CAGE_CONTROL
    Dim _voucher_cage_body As VoucherCageFillInFillOutBody
    Dim _voucher_cage_fill_out As VoucherCageNewOperation
    Dim _tmp_item As WSI.Common.CageDenominationsItems.CageDenominationsItem
    Dim _dt As DataTable
    Dim _dr As DataRow
    Dim _cage_amounts As WSI.Common.CageDenominationsItems.CageDenominationsDictionary
    Dim _current_iso_code As String
    Dim _current_total As Decimal
    Dim _dec_tmp As Decimal
    Dim _vc_info(4) As String
    Dim _source_destination As String
    Dim _cashier_movement As CASHIER_MOVEMENT
    Dim _str_currency_type As String
    Dim _currency_exchange_type As WSI.Common.CurrencyExchangeType
    Dim _currency_exchange_type_last As WSI.Common.CurrencyExchangeType

    _cage_movement = DbEditedObject
    _tmp_item = New WSI.Common.CageDenominationsItems.CageDenominationsItem()
    _cage_amounts = New CageDenominationsItems.CageDenominationsDictionary()
    _cashier_movement = CASHIER_MOVEMENT.CAGE_FILLER_IN
    _dt = New DataTable()

    _vc_info(0) = cmb_cage_sessions.TextValue
    _vc_info(1) = ef_cage_user.TextValue
    _vc_info(3) = ef_operation.TextValue

    If rb_send.Checked Then
      _source_destination = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3396) & ": " ' Destino

      If rb_cashier.Checked Then
        If rb_to_user.Checked Then
          _source_destination += cmb_user.TextValue()
        Else
          _source_destination += cmb_terminals.TextValue()
        End If
      End If
      If rb_gaming_table.Checked Then
        _source_destination += cmb_gaming_table.TextValue()
      End If
      If rb_custom.Checked Then
        _source_destination += cmb_custom.TextValue()
      End If
      If rb_terminal.Checked Then
        _source_destination += cmb_terminals.TextValue()
      End If

    Else
      _source_destination = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3427) & ": " ' Origen

      If rb_gaming_table.Checked Then
        _source_destination += cmb_gaming_table.TextValue()
      End If
      If rb_custom.Checked Then
        _source_destination += cmb_custom.TextValue()
      End If

    End If

    _vc_info(4) = _source_destination

    If _cage_movement.TableCurrencies.Rows.Count > 0 Then

      _current_iso_code = ""

      Call PrintCageVoucherInitDatTable(_dt)

      For Each _row As DataRow In _cage_movement.TableCurrencies.Rows
        _dr = _dt.NewRow()

        _str_currency_type = ""

        Select Case _row("TYPE")
          Case CageCurrencyType.Bill
            _currency_exchange_type = CurrencyExchangeType.CURRENCY
            _str_currency_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5675).Substring(0, 1) ' "B"
          Case CageCurrencyType.Coin
            _currency_exchange_type = CurrencyExchangeType.CURRENCY
            _str_currency_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5676).Substring(0, 1) ' "M"
          Case CageCurrencyType.ChipsRedimible
            _currency_exchange_type = CurrencyExchangeType.CASINO_CHIP_RE
          Case CageCurrencyType.ChipsNoRedimible
            _currency_exchange_type = CurrencyExchangeType.CASINO_CHIP_NRE
          Case CageCurrencyType.ChipsColor
            _currency_exchange_type = CurrencyExchangeType.CASINO_CHIP_COLOR
          Case Else
            _currency_exchange_type = CurrencyExchangeType.CURRENCY
        End Select

        If String.IsNullOrEmpty(_current_iso_code) Then
          '_dr = _dt.NewRow()
          _dr("ISO_CODE") = _row("ISO_CODE")
          _dr("DENOMINATION") = _row("DENOMINATION")
          _dr("DENOMINATION_SHOWED") = _row("DENOMINATION_WITH_SIMBOL")
          _dr("CURRENCY_TYPE") = _str_currency_type
          _dr("UN") = _row("QUANTITY")
          _dr("TOTAL") = _row("TOTAL")

          _dr("CHIP_ID") = _row("CHIP_ID")
          _dr("CHIP_NAME") = _row("NAME")
          _dr("CHIP_DRAW") = _row("DRAWING")
          _dr("CHIP_COLOR") = _row("COLOR")

          _current_iso_code = _row("ISO_CODE")
          _current_total = Me.m_cage_cls.GetRowValue(_row("TOTAL"))
          _currency_exchange_type_last = _currency_exchange_type
          _dt.Rows.Add(_dr)

        ElseIf _current_iso_code <> _row("ISO_CODE") Or _currency_exchange_type_last <> _currency_exchange_type Then

          '_dr = _dt.NewRow()
          If Not _cage_amounts.ContainsKey(_current_iso_code, _currency_exchange_type_last) Then

            _tmp_item.ItemAmounts = _dt
            _tmp_item.TotalAmount = _current_total
            _tmp_item.AllowedFillOut = True
            _cage_amounts(_current_iso_code, _currency_exchange_type_last) = _tmp_item

            _tmp_item = New WSI.Common.CageDenominationsItems.CageDenominationsItem()
            Call PrintCageVoucherInitDatTable(_dt)
            _dr = _dt.NewRow()
            _dr("ISO_CODE") = _row("ISO_CODE")
            _dr("DENOMINATION") = _row("DENOMINATION")
            _dr("DENOMINATION_SHOWED") = _row("DENOMINATION_WITH_SIMBOL")
            _dr("CURRENCY_TYPE") = _str_currency_type
            _dr("UN") = _row("QUANTITY")
            _dr("TOTAL") = _row("TOTAL")

            _dr("CHIP_ID") = _row("CHIP_ID")
            _dr("CHIP_NAME") = _row("NAME")
            _dr("CHIP_DRAW") = _row("DRAWING")
            _dr("CHIP_COLOR") = _row("COLOR")

            _dt.Rows.Add(_dr)

          End If

          _current_iso_code = _row("ISO_CODE")
          _current_total = Me.m_cage_cls.GetRowValue(_row("TOTAL"))
          _currency_exchange_type_last = _currency_exchange_type

        Else

          '_dr = _dt.NewRow()
          _dr("ISO_CODE") = _row("ISO_CODE")
          _dr("DENOMINATION") = _row("DENOMINATION")
          _dr("DENOMINATION_SHOWED") = _row("DENOMINATION_WITH_SIMBOL")
          _dr("CURRENCY_TYPE") = _str_currency_type
          _dr("UN") = _row("QUANTITY")
          _dr("TOTAL") = _row("TOTAL")

          _dr("CHIP_ID") = _row("CHIP_ID")
          _dr("CHIP_NAME") = _row("NAME")
          _dr("CHIP_DRAW") = _row("DRAWING")
          _dr("CHIP_COLOR") = _row("COLOR")

          Decimal.TryParse(Me.m_cage_cls.GetRowValue(_row("TOTAL")), _dec_tmp)
          _current_total += _dec_tmp
          _currency_exchange_type_last = _currency_exchange_type

          _dt.Rows.Add(_dr)
        End If

        If Not _dr("UN") Is DBNull.Value AndAlso _dr("DENOMINATION") = Cage.TICKETS_CODE Then
          _tmp_item.HasTicketsTito = (_dr("UN") > 0)
        End If

      Next

      If _dt.Rows.Count > 0 Then
        _tmp_item.ItemAmounts = _dt
        _tmp_item.TotalAmount = _current_total
        _tmp_item.AllowedFillOut = True
        _cage_amounts(_current_iso_code, _currency_exchange_type) = _tmp_item
      End If

    End If

    Using _db_trx As New DB_TRX()
      Cashier.GetSystemCashierSessionInfo(GU_USER_TYPE.SYS_SYSTEM, _db_trx.SqlTransaction)

      If Me.cb_close_gaming_table.Checked Then
        _cashier_movement = CASHIER_MOVEMENT.CLOSE_SESSION
      End If
      _voucher_cage_body = New VoucherCageFillInFillOutBody(_cashier_movement, _cage_amounts)

      _voucher_cage_fill_out = New VoucherCageNewOperation(PrintMode.Print, _vc_info, _db_trx.SqlTransaction)
      _voucher_cage_fill_out.SetParameterValue("@VOUCHER_CAGE_NEW_OPERATION_BODY", _voucher_cage_body.VoucherHTML)

      _voucher_cage_fill_out.PrintMode = PrintMode.Print
      _voucher_cage_fill_out.PrintVoucher = True

      Voucher.PageSetup()
      _voucher_cage_fill_out.VoucherHTML = VoucherManager.FontSetup(_voucher_cage_fill_out.VoucherHTML)

      If (_voucher_cage_fill_out.Save(_db_trx.SqlTransaction)) Then
        _db_trx.Commit()
        HtmlPrinter.AddHtml(_voucher_cage_fill_out.VoucherHTML)

      Else
        _db_trx.Rollback()
      End If

    End Using

    'Dispose
    Call SafeDispose(_vc_info)
    Call SafeDispose(_cage_amounts)
    Call SafeDispose(_tmp_item)
    Call SafeDispose(_cage_movement)
    Call SafeDispose(_voucher_cage_fill_out)
    Call SafeDispose(_dt)
    Call SafeDispose(_voucher_cage_body)

  End Sub ' PrintCageVoucherNewOperation

  ' PURPOSE: Init Datatable
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub PrintCageVoucherInitDatTable(ByRef CageDataTable As DataTable)

    CageDataTable = New DataTable()
    CageDataTable.Columns.Add("COLOR", Type.GetType("System.Int32"))
    CageDataTable.Columns.Add("ISO_CODE", Type.GetType("System.String"))
    CageDataTable.Columns.Add("DENOMINATION", Type.GetType("System.Decimal"))
    CageDataTable.Columns.Add("ALLOWED", Type.GetType("System.Boolean"))
    CageDataTable.Columns.Add("DENOMINATION_SHOWED", Type.GetType("System.String"))
    CageDataTable.Columns.Add("UN", Type.GetType("System.Int32"))
    CageDataTable.Columns.Add("TOTAL", Type.GetType("System.String"))
    CageDataTable.Columns.Add("CURRENCY_TYPE", Type.GetType("System.String"))
    CageDataTable.Columns.Add("CHIP_ID", Type.GetType("System.Int32"))
    CageDataTable.Columns.Add("CHIP_NAME", Type.GetType("System.String"))
    CageDataTable.Columns.Add("CHIP_DRAW", Type.GetType("System.String"))
    CageDataTable.Columns.Add("CHIP_COLOR", Type.GetType("System.String"))

  End Sub ' PrintCageVoucherInitDatTable

  Private Sub SetDefaultCageSession()
    If Me.cmb_cage_sessions.Count = 1 Then
      Me.cmb_cage_sessions.SelectedIndex = 0
      Me.cmb_cage_sessions.IsReadOnly = True
    End If
  End Sub

  Private Sub SetLayoutFromMoneyCollection(ByRef PanelAuxX As Integer, ByRef PanelAuxY As Integer)

    RemoveHandler rb_terminal.CheckedChanged, AddressOf rb_terminal_CheckedChanged

    Me.ef_cashier_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1818)
    Me.ef_status_movement.TextValue = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3011)
    Me.ef_user_terminals_name.Visible = False
    Me.rb_reception.Checked = True
    Me.gb_operation.Visible = False
    Me.gb_collection_params.Visible = True
    Me.rb_terminal.Checked = True

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Visible = True

    If m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT Then
      Me.tb_notes.Enabled = False
    Else
      Me.tb_notes.Enabled = True
    End If

    Call SetGBCollectionParamsLocation(PanelAuxX, PanelAuxY)

    AddHandler rb_terminal.CheckedChanged, AddressOf rb_terminal_CheckedChanged

  End Sub


  Private Sub SetGBCollectionParamsLocation(ByRef PanelAuxX As Integer, ByRef PanelAuxY As Integer)

    Me.gb_collection_params.Location = New Point(Me.panel_aux.Location.X, _
                                                (Me.gb_movement_type.Location.Y + Me.gb_movement_type.Height + 3))
    Me.gb_collection_params.Visible = True

    m_y_offset = (Me.gb_collection_params.Location.Y + Me.gb_collection_params.Size.Height) _
                   - (Me.gb_movement_type.Location.Y + Me.gb_movement_type.Size.Height) _
                       - (Me.gb_operation.Location.Y + Me.gb_operation.Size.Height)

    PanelAuxX = Me.gb_collection_params.Location.X
    PanelAuxY = Me.gb_collection_params.Location.Y + m_y_offset
  End Sub

  Private Sub PrintCage(ByVal WithPreview As Boolean, Optional ByVal FootSignature As String = "")
    Dim _excel_data As GUI_Reports.CLASS_EXCEL_DATA
    Dim _group_dictionary As Dictionary(Of String, String)
    Dim _ds_cage_movement As DataSet
    Dim _num_max_columns As Int32
    Dim _num_max_rows As Int32

    Windows.Forms.Cursor.Current = Cursors.WaitCursor
    _excel_data = Nothing
    _ds_cage_movement = New DataSet()
    _group_dictionary = New Dictionary(Of String, String)
    _num_max_columns = 0
    _num_max_rows = 0

    Try
      If Not IsNothing(_excel_data) Then
        _excel_data = Nothing
      End If

      _excel_data = New GUI_Reports.CLASS_EXCEL_DATA(WithPreview, WithPreview)

      If _excel_data.IsNull() Then
        Return
      End If

      FootSignature = FootSignature.Replace("\n", vbCrLf)

      Call GetControlsDataForPrint(_group_dictionary)
      Call GetTableFromTotals(_ds_cage_movement, _num_max_columns, _num_max_rows)
      Call GetTablesFromDenominations(_ds_cage_movement, _num_max_columns, _num_max_rows)

      If Me.tb_concepts.Visible Then
        Call GetTableFromConcepts(_ds_cage_movement, _num_max_columns, _num_max_rows)
      End If

      _excel_data.PrintDateTime = GUI_FormatDate(WSI.Common.WGDB.Now(), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      _excel_data.PrintUser = CurrentUser.Name & "@" & Environment.MachineName
      _excel_data.Title = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4968)
      _excel_data.Site = GetSiteOrMultiSiteName()
      _excel_data.PrintCage(_group_dictionary, _ds_cage_movement, _num_max_columns, _num_max_rows, WithPreview, COLOR_POSITIVE_DIFFERENCE, COLOR_NEGATIVE_OR_POSITIVE_DIFFERENCE, FootSignature)

    Catch _ex As Exception

      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(146), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            GLB_ApplicationName, _
                            "frm_cage_control.PrintCageMovement()", _
                            "", _
                            "", _
                            "", _
                           "Error in Excel data export.")
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            GLB_ApplicationName, _
                            "Exception", _
                            "", _
                            "", _
                            "", _
                            _ex.Message)
    Finally
      Call SafeDispose(_excel_data)
      Call SafeDispose(_ds_cage_movement)
      Call SafeDispose(_group_dictionary)

      Windows.Forms.Cursor.Current = Cursors.Default
    End Try

  End Sub ' PrintCageMovement

  ' PURPOSE: To load data from new request.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub LoadNewRequest()
    Dim _table_curr As DataTable

    _table_curr = New DataTable
    Me.m_cage_cls.MovementID = Me.m_cage_cls.GetNewRequest()

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)

    Me.m_cage_cls = Me.DbEditedObject
    Call Me.InitOpenMode(_table_curr)

    ' Clear currency tabs.
    RemoveHandler tb_currency.SelectedIndexChanged, AddressOf tb_currency_SelectedIndexChanged
    Call Me.tb_currency.Controls.Clear()
    AddHandler tb_currency.SelectedIndexChanged, AddressOf tb_currency_SelectedIndexChanged

    ' Clear Total datagrid and load new data.
    Call Me.dg_total.Clear()
    Call Me.CreateCurrencyTabs(_table_curr)
    Call Me.InitTotalGrid(_table_curr)
    Call Me.FirstUpdateTotalGrid()

    ' Dispose
    Call SafeDispose(_table_curr)

  End Sub ' LoadNewRequest

  Private Sub ClearMoneyCollectionData()
    Dim _grid As uc_grid
    Dim i As Integer
    Dim _denomination As String
    Dim _ticket_collection_enabled As Boolean
    Dim _ticket_amount As Decimal

    For Each _tb_page As TabPage In Me.tb_currency.TabPages
      If _tb_page.Name <> tab_ticket.Name Then
        _grid = _tb_page.Controls(0)
        For i = 0 To _grid.NumRows - 1
          _denomination = WSI.Common.Format.LocalNumberToDBNumber(_grid.Cell(i, GRID_CURRENCY_COLUMN_DENOMINATION).Value)

          If _denomination <> Cage.COINS_CODE.ToString() AndAlso _
             _denomination <> Cage.TICKETS_CODE.ToString() AndAlso _
             _denomination <> Cage.CHECK_CODE.ToString() AndAlso _
             _denomination <> Cage.COINS_COLLECTION_CODE.ToString() AndAlso _
             _denomination <> Cage.BANK_CARD_CODE.ToString() Then
            _grid.Cell(i, GRID_CURRENCY_COLUMN_QUANTITY).Value = Nothing
            Call CellDataChangeHandler(i, GRID_CURRENCY_COLUMN_QUANTITY, True, _grid, True)
          Else
            If _denomination <> Cage.TICKETS_CODE.ToString() Then
              _grid.Cell(i, GRID_CURRENCY_COLUMN_TOTAL).Value = Nothing
              _grid.Cell(i, GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).Value = Nothing
              Call CellDataChangeHandler(i, GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL, True, _grid, True)
            End If
          End If
        Next
      End If
    Next

    i = 0
    _ticket_collection_enabled = GeneralParam.GetBoolean("TITO", "TicketsCollection")
    While i < dg_collection_tickets.NumRows
      If dg_collection_tickets.Cell(i, GRID_COLUMN_TICKET_BELONG).Value Then
        If dg_collection_tickets.Cell(i, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Value = "" And _ticket_collection_enabled Then
          Call Me.UpdateDifferenceAndColor(i, -1, False)
          Call IncrementCurrencyTabTicketCount(-1)

          ' DHA 13-JAN-2016: dual currency concatenated de ISO Code, to get amount must be Unformated
          If WSI.Common.Misc.IsFloorDualCurrencyEnabled() And Me.m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal Then
            _ticket_amount = Currency.Unformat(Me.dg_collection_tickets.Cell(i, GRID_COLUMN_TICKET_AMOUNT).Value, Me.m_cage_cls.TerminalIsoCode)
          Else
            _ticket_amount = Me.dg_collection_tickets.Cell(i, GRID_COLUMN_TICKET_AMOUNT).Value
          End If
          Call Me.UpdateTotalGridTicketsRow(-_ticket_amount)

          Me.m_cage_cls.m_type_new_collection.real_ticket_sum -= CDbl(_ticket_amount)
          Me.m_cage_cls.m_type_new_collection.real_ticket_count -= 1

        ElseIf dg_collection_tickets.Cell(i, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Value = "-1" And Not _ticket_collection_enabled Then
          Call Me.UpdateDifferenceAndColor(i, 0, False)
          Call IncrementCurrencyTabTicketCount(1)
          ' DHA 13-JAN-2016: dual currency concatenated de ISO Code, to get amount must be Unformated
          If WSI.Common.Misc.IsFloorDualCurrencyEnabled() And Me.m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal Then
            _ticket_amount = Currency.Unformat(Me.dg_collection_tickets.Cell(i, GRID_COLUMN_TICKET_AMOUNT).Value, Me.m_cage_cls.TerminalIsoCode)
          Else
            _ticket_amount = Me.dg_collection_tickets.Cell(i, GRID_COLUMN_TICKET_AMOUNT).Value
          End If
          Call Me.UpdateTotalGridTicketsRow(_ticket_amount)
          Me.m_cage_cls.m_type_new_collection.real_ticket_sum += CDbl(_ticket_amount)
          Me.m_cage_cls.m_type_new_collection.real_ticket_count += 1
        ElseIf dg_collection_tickets.Cell(i, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Value = "?" Then
          Call Me.UpdateDifferenceAndColor(i, -1, False)
        End If
        i = i + 1

      Else
        If Me.dg_collection_tickets.Cell(i, GRID_COLUMN_TICKET_AMOUNT).Value <> "" Then
          ' DHA 13-JAN-2016: dual currency concatenated de ISO Code, to get amount must be Unformated
          If WSI.Common.Misc.IsFloorDualCurrencyEnabled() And Me.m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal Then
            _ticket_amount = Currency.Unformat(Me.dg_collection_tickets.Cell(i, GRID_COLUMN_TICKET_AMOUNT).Value, Me.m_cage_cls.TerminalIsoCode)
          Else
            _ticket_amount = Me.dg_collection_tickets.Cell(i, GRID_COLUMN_TICKET_AMOUNT).Value
          End If
          Call Me.UpdateTotalGridTicketsRow(-_ticket_amount)
          Call IncrementCurrencyTabTicketCount(-1)
          Me.m_cage_cls.m_type_new_collection.real_ticket_sum -= CDbl(_ticket_amount)
          Me.m_cage_cls.m_type_new_collection.real_ticket_count -= 1
        End If
        Me.dg_collection_tickets.DeleteRow(i)

      End If
    End While

    If Me.tb_concepts.Visible Then
      Call SetConceptsValues()
    End If
    Call ResetNoteCounterEvents()

  End Sub ' PrivateMoneyCollectionData

  Private Sub ResetMoneyCollectionData()
    Dim _table_currencies As DataTable

    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW
    Me.m_new_collecion_mode = False
    Me.ef_stacker_id.TextValue = ""
    Me.ef_floor_id.TextValue = ""

    Call CheckRadioButtonsToSetLayouts()
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)

    Me.m_cage_cls = Me.DbEditedObject
    'Me.m_cage_cls = New CLASS_CAGE_CONTROL()
    'Me.m_cage_cls.m_type_new_collection.stacker_id = 0
    Me.m_cage_cls.GetAllowedISOCode()
    Call Me.m_cage_cls.InitCurrencyData(True)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If
    'm_cage_cls = DbReadObject
    _table_currencies = New DataTable()
    dg_collection_tickets.Clear()
    dg_total.Clear()
    Call Me.InitOpenMode(_table_currencies)
    Call Me.InitTotalGrid(_table_currencies)
    Call Me.SetStyleSheetGrid(tb_currency.TabPages(0).Controls(0))
    Call GUI_SetScreenData(0)
    Call ResetNoteCounterEvents()

    Me.tb_currency.SelectedIndex = 0
    Me.ef_cage_session.TextValue = ""
    Me.ef_stacker_id.Enabled = True
    Me.ef_stacker_id.Focus()
    Me.ef_floor_id.Enabled = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = False
    Me.lb_error_messages.Text = ""

    If m_ef_stacker_focused Then
      Me.ef_stacker_id.Focus()
    Else
      Me.ef_floor_id.Focus()
    End If

    'Dispose
    Call SafeDispose(_table_currencies)

  End Sub

  Private Sub LoadMoneyCollectionByStackerOrFloorId()
    Dim _cage_session_id As Int32
    Dim _frm_cage_sessions As frm_cage_sessions
    Dim _stacker_id As Int64

    If m_loading_stacker_collection_data Then
      Return
    End If

    m_loading_stacker_collection_data = True

    Try
      Call ClearMoneyCollectionData()
      If Not Int64.TryParse(Me.ef_stacker_id.TextValue, _stacker_id) AndAlso String.IsNullOrEmpty(ef_floor_id.TextValue) Then

        Return
      End If

      Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
      Me.m_new_collecion_mode = True

      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)

      Me.m_cage_cls = Me.DbEditedObject
      Me.m_cage_cls.m_type_new_collection.stacker_id = _stacker_id
      Me.m_cage_cls.m_type_new_collection.floor_id = ef_floor_id.TextValue

      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
      If DbStatus <> ENUM_STATUS.STATUS_OK Then
        Call ResetMoneyCollectionData()
        Return
      End If

      Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MASSIVE
      Me.m_cage_cls.GetAllowedISOCode()
      Call Me.m_cage_cls.InitCurrencyData(True)
      Cage.GetCageSessionIdFromCashierSessionId(Me.m_cage_cls.CashierSessionId, Me.m_cage_cls.CageSessionId)

      If Me.m_cage_cls.CageSessionId = 0 Then
        Select Case Cage.CollectionDefaultCageSession()
          Case Cage.DefaultCageSession.FirstCageSessionOpened _
             , Cage.DefaultCageSession.LastCageSessionOpened
            If Not Cage.GetCurrentCageSessionId(_cage_session_id, Cage.CollectionDefaultCageSession()) Then
              NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5014), ENUM_MB_TYPE.MB_TYPE_ERROR)
              Return
            End If

          Case Cage.DefaultCageSession.ChooseCageSession
            If m_preselected_cage_session <> -1 And GeneralParam.GetBoolean("Cage", "MassiveCollection.PreselectCageSession") Then
              _cage_session_id = m_preselected_cage_session
            Else
              If Cage.GetNumberOfOpenedCages() = 1 Then
                ' True Or False doesn't matter, because we just want the Cash Cage session opened and there is only one.
                Call Cage.GetCurrentCageSessionId(_cage_session_id, True)
              Else
                If Not CurrentUser.Permissions(ENUM_FORM.FORM_CAGE_SESSIONS).Read Then
                  NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4373), ENUM_MB_TYPE.MB_TYPE_WARNING)

                  Return
                End If

                _frm_cage_sessions = New frm_cage_sessions()
                Call _frm_cage_sessions.ShowForEdit(_cage_session_id)

                Call SafeDispose(_frm_cage_sessions)

                Select Case _cage_session_id
                  Case -1
                    NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5014), ENUM_MB_TYPE.MB_TYPE_ERROR)
                    Return

                  Case 0
                    Return
                End Select
              End If
              m_preselected_cage_session = _cage_session_id
            End If

          Case Else
            NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5014), ENUM_MB_TYPE.MB_TYPE_ERROR)
            Return
        End Select

        If _cage_session_id <> -1 Then
          Me.m_cage_cls.CageSessionId = _cage_session_id
        Else
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5014), ENUM_MB_TYPE.MB_TYPE_ERROR)

          Return
        End If

      End If

      Call Me.SetCageSessionEfValue()

      If DbStatus = ENUM_STATUS.STATUS_OK Then
        Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
      End If

      ' DHA 13-JAN-2016: set the currency for stacker collection when the currency is different than national
      If WSI.Common.Misc.IsFloorDualCurrencyEnabled() And Me.m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal _
        And Not Me.m_cage_cls.TerminalIsoCode Is Nothing AndAlso Me.m_cage_cls.TerminalIsoCode <> CurrencyExchange.GetNationalCurrency() Then

        RemoveHandler tb_currency.SelectedIndexChanged, AddressOf tb_currency_SelectedIndexChanged

        ' Clear all data
        Me.dg_total.Clear()
        Me.tb_currency.TabPages.Clear()
        ' Add currency/tickets controls
        Call Me.AddTab(Me.m_cage_cls.TerminalIsoCode, WSI.Common.CurrencyExchangeType.CURRENCY, "")
        Me.tb_currency.TabPages.Add(tab_ticket)
        AddHandler tb_currency.SelectedIndexChanged, AddressOf tb_currency_SelectedIndexChanged

        Call Me.InitTotalGrid(Me.m_cage_cls.TableCurrencies)

        Me.tb_currency.SelectedIndex = 0
      End If

      Call CheckRadioButtonsToSetLayouts()
      Call Me.SetStyleSheetGrid(tb_currency.TabPages(0).Controls(0))
      Call GUI_SetScreenData(0)
      Me.ef_stacker_id.Enabled = False
      Me.ef_floor_id.Enabled = False
      Me.lb_error_messages.Text = ""
      Me.btn_open_events.Visible = False
      Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = Not GLB_CurrentUser.IsSuperUser
      If Not m_print_enabled Then 'DRV if it's clean button visible, enable it when the money collection is loaded
        Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = True
      End If
      Call Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Focus()

      Call ResetNoteCounterEvents()

    Finally
      m_loading_stacker_collection_data = False
    End Try

  End Sub

  ' PURPOSE: Create a new cage session template.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub NewCageTemplate()
    Dim _frm_edit As frm_template_edit
    Dim _template_id As Long
    Dim _template_name As String
    Dim _dt_differences As DataTable
    Dim _new_pattern As Boolean
    Dim _rows_to_delete As DataRow()

    _new_pattern = False
    _template_id = 0
    If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.TEMPLATE_MAINTENANCE Then
      _rows_to_delete = m_cage_cls.TableCurrencies.Select(" DENOMINATION < 0 ")
      For Each _row As DataRow In _rows_to_delete
        m_cage_cls.TableCurrencies.Rows.Remove(_row)
      Next
      m_cage_cls.ClosingStocks.MapDatatable(Me.m_cage_cls.TableCurrencies, "ISO_CODE", "DENOMINATION", "TYPE", "CHIP_ID", "QUANTITY", "TOTAL")

      DbReadObject = DbEditedObject.Duplicate()
      Return
    Else

      If Me.cmb_cage_template.SelectedIndex <= 0 Then
        If Me.cmb_cage_template.Count > WSI.Common.GeneralParam.GetInt32("Cage", "MaxNumberTemplates", MAX_NUMBER_TEMPLATES) Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5054), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GeneralParam.GetInt32("Cage", "MaxNumberTemplates", MAX_NUMBER_TEMPLATES))

          Exit Sub
        End If

        'Ask for a new template name
        _frm_edit = New frm_template_edit
        Call _frm_edit.ShowNewItem()
        If _frm_edit.Canceled = True Then

          Exit Sub
        End If
        _template_name = _frm_edit.TemplateName

        Call SafeDispose(_frm_edit)

      Else
        _template_id = Me.cmb_cage_template.Value
        _template_name = Me.cmb_cage_template.TextValue
      End If

      'Collect data from the form
      GUI_GetScreenData()

      If Me.rb_to_user.Checked Then
        m_cage_template.TargetType = Cage.PendingMovementType.ToCashierUser
      Else
        m_cage_template.TargetType = Cage.PendingMovementType.ToCashierTerminal
      End If

      _dt_differences = GetTemplateDifferences(Me.m_original_template, m_cage_template.TableCurrencies.Copy, m_original_preferences)

      'Save new template
      If m_cage_template.SaveTemplate(_template_name, m_cage_template, _template_id, _new_pattern) Then

        m_changed_values = False

        If _new_pattern Then
          'Add new template into the template combo
          cmb_cage_template.Add(Convert.ToInt32(_template_id), _template_name)
        End If

        If _dt_differences.Rows.Count > 0 Then
          m_cage_template.AuditTemplate(_dt_differences, _new_pattern, _template_name, "TYPE")
        End If

        'Cambo set to the template
        RemoveHandler cmb_cage_template.ValueChangedEvent, AddressOf TemplateChange
        cmb_cage_template.Value = _template_id
        AddHandler cmb_cage_template.ValueChangedEvent, AddressOf TemplateChange
        Me.m_original_template = m_cage_template.TableCurrencies
        Me.m_original_preferences = m_cage_template.TablePreferences
        ' 108 "Los cambios se han guardado correctamente."
        If _dt_differences.Select("ISO_CODE = '" & Cage.CHIPS_ISO_CODE & "' ").Length = 0 Then
          Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(108), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO)
        End If
      End If
    End If

  End Sub ' LoadNewRequest

  Private Sub CatchMessageFromScanner(ByVal MessageReceived As IScanned)
    Try
      ' tmr_note_counter_working.Start()
      Select Case MessageReceived.GetScannedType()
        Case ScannedType.Currencies
          m_error_received = False
          SetScreenScannerNotes(CType(MessageReceived, NotesReceived))
          m_note_counter_flag = True
        Case ScannedType.Tickets
          If Me.tab_ticket.InvokeRequired AndAlso tb_currency.Contains(tab_ticket) Then
            m_error_received = False
            Me.tab_ticket.Invoke(New InvokeTicketGrid(AddressOf AddTicketToCollection), New Object() {CType(MessageReceived, TicketReceived).ValidationNumber.ToString(), 0D, True, 0})
          End If
          m_note_counter_flag = True
        Case ScannedType.Error
          If lb_error_messages.InvokeRequired() Then
            m_error_received = True
            Me.lb_error_messages.Invoke(New InvokeError(AddressOf CounterErrorReceived), New Object() {MessageReceived})
          End If
          m_note_counter_flag = True
        Case ScannedType.Information
          Dim _info As InformationReceived
          _info = CType(MessageReceived, InformationReceived)
          Select Case _info.InformationType
            Case InformationType.NOTECOUNTER_INITIALIZED
              If lbl_counter.InvokeRequired() Then
                Me.lb_error_messages.Invoke(New InvokeStatusOkToLabel(AddressOf SetStatusOkToLabel), New Object() {GLB_NLS_GUI_PLAYER_TRACKING.GetString(5196)})
              End If
              If lb_error_messages.InvokeRequired() Then
                Me.lb_error_messages.Invoke(New InvokeError(AddressOf CounterErrorReceived), New Object() {MessageReceived})
              End If
            Case InformationType.LABEL_RECEIVED
              If lbl_counter.InvokeRequired() Then
                m_note_counter_flag = True
                Me.lb_error_messages.Invoke(New InvokeError(AddressOf CounterErrorReceived), New Object() {MessageReceived})
              End If

            Case InformationType.UNDEFINED_CURRENCY
              If lb_error_messages.InvokeRequired() Then
                m_error_received = True
                Me.lb_error_messages.Invoke(New InvokeError(AddressOf CounterErrorReceived), New Object() {MessageReceived})
              End If
          End Select

      End Select

      'm_note_counter_flag = True

    Catch ex As InvalidOperationException
      Log.Error(ex.Message())
    End Try

  End Sub

  Private Sub SetScreenScannerNotes(ByVal NotesReceived As NotesReceived)
    Dim _currency As NoteReceived

    If Me.tb_currency.InvokeRequired Then
      For Each _currency In NotesReceived.Notes
        Me.tb_currency.Invoke(New InvokeCurrencyGrid(AddressOf AddCurrencyToCollection), New Object() {_currency, NotesReceived.IsoCode})
      Next
    End If
  End Sub ' LoadNewRequest

  Private Sub CounterErrorReceived(ByVal MessageReceived As IScanned)
    Dim _source_name As String
    Dim _text As String
    Dim _denom() As String
    Select Case MessageReceived.GetScannedType()
      Case ScannedType.Error
        Dim _error As ErrorReceived
        _error = CType(MessageReceived, ErrorReceived)

        _text = GetCounterErrorDescription(_error.AlarmCode)
        _source_name = GLB_CurrentUser.Name & "@" & Environment.MachineName

        lbl_counter.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5454)

        lb_error_messages.Visible = True

        lb_note_counter_status.BackColor = Color.Red
        m_lbl_counter_status_update = 0

        Call SetErrorToLabel(_text)

      Case ScannedType.Information
        Dim _info As InformationReceived
        _info = CType(MessageReceived, InformationReceived)

        Select Case _info.InformationType
          Case InformationType.NOTECOUNTER_INITIALIZED
            Call SetErrorToLabel(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5196))

          Case InformationType.UNDEFINED_CURRENCY
            _denom = _info.Info.Split("-")
            Call SetErrorToLabel(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5496, _denom(1), _denom(0)))

          Case InformationType.LABEL_RECEIVED
            Call SetErrorToLabel(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5537))
        End Select

    End Select

  End Sub

  Private Sub SetErrorToLabel(ByVal ErrorText As String)

    'RaiseEvent m_throw_event(ErrorText)
    m_frm_counter_event.SetEventToGrid(ErrorText)
    m_list_of_event.Add(ErrorText)

    If m_first_error Then ' or el mensaje inicial de configurado ok
      lb_error_messages.Text = ErrorText
      m_first_error = False
    ElseIf m_second_error Then
      lb_error_messages.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5188)
      btn_open_events.Visible = True
      m_second_error = False
    End If

  End Sub

  Private Sub SetStatusOkToLabel(ByVal StatusOkTextText As String)
    m_note_counter_flag = False
    m_error_received = False
    m_lbl_counter_status_update = 0
    lb_note_counter_status.BackColor = Color.Green
    m_first_error = True
    m_second_error = True

  End Sub


  Private Function GetCounterErrorDescription(ByVal ErrorCode As AlarmCode) As String
    Dim _text As String

    Select Case ErrorCode
      Case AlarmCode.Reader_Chain_Detected
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5174)
      Case AlarmCode.Reader_Changing_Operating_Mode
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5178)
      Case AlarmCode.Reader_Combined_Denomination_Change_Strap_Limit
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5176)
      Case AlarmCode.Reader_Comunication_Error
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5177)
      Case AlarmCode.Reader_Configuration
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5175)
      Case AlarmCode.Reader_Denomination_Changed
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5165)
      Case AlarmCode.Reader_Double_Detected
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5169)
      Case AlarmCode.Reader_Facing_Error
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5167)
      Case AlarmCode.Reader_Jam_Detected
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5171)
      Case AlarmCode.Reader_No_Call
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5173)
      Case AlarmCode.Reader_Orientation_Error
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5168)
      Case AlarmCode.Reader_Stacker_Full
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5172)
      Case AlarmCode.Reader_Stranger_Detected
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5164)
      Case AlarmCode.Reader_Strap_Limit
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5166)
      Case AlarmCode.Reader_Suspect_Document
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5170)
      Case AlarmCode.Reader_Ticket_Already_Collected
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5153)
      Case AlarmCode.Reader_Ticket_Sent_Two_Times
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5155)
      Case AlarmCode.Reader_Ticket_With_Duplicated_Number
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5154)
      Case AlarmCode.Reder_Invalid_Ticket
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5152)
      Case AlarmCode.Reader_Undefined_Elements
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5491)
      Case AlarmCode.Reader_No_Count
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5518)
      Case Else
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5198)

    End Select

    Return _text
  End Function

  Private Sub SetConceptsValues()
    Dim _source_target_id As Int32
    Dim _type As Int32

    If Not m_cage_cls.SourceTargetConceptsData Is Nothing Then

      Call Me.GetSourceTargetAndType(_source_target_id, _type)

      Me.dg_concepts.Clear()

      If Me.rb_reception.Checked And Me.rb_cashier.Checked Then
        Call Me.SetCashierConceptsValues(CageMeters.CageSystemSourceTarget.Cashiers, _type)
        If Me.m_cage_cls.MovementID > 0 Then
          Call Me.SetConceptsGridFromMovementDetails()
        End If
      Else
        Call Me.SetNoCashierConceptsValues(_source_target_id, _type)
        If Me.m_cage_cls.MovementID > 0 Then
          Call Me.SetConceptsGridFromMovementDetails()
        End If
      End If

      SetConceptsColWidth()

    End If

  End Sub

  Private Function SetCashierConceptsValues(ByVal SourcetargetId As Int64, ByVal Type As Int32) As Boolean

    Dim _col_index As Int32
    Dim _meter_value As Currency
    Dim _dt_concepts_to_show() As DataRow
    Dim _sql_filter As String
    Dim _dt_concepts_movements As DataTable

    _sql_filter = "CSTC_SOURCE_TARGET_ID = " & SourcetargetId.ToString() ' & " AND CSTC_TYPE IN (2, " & Type.ToString() & ")"
    _dt_concepts_to_show = m_cage_cls.SourceTargetConceptsData.Select(_sql_filter)

    _dt_concepts_movements = New DataTable()
    Using _db_trx As New DB_TRX()
      If Not CageMeters.GetCashierConceptMovements(m_cage_cls.CashierSessionId, _dt_concepts_movements, _db_trx.SqlTransaction) Then
        Return False
      End If
    End Using

    For Each _dr As DataRow In _dt_concepts_to_show

      If _dr("CSTC_CONCEPT_ID").ToString() <> "0" Then

        Me.dg_concepts.AddRow()
        Me.dg_concepts.Cell(Me.dg_concepts.NumRows - 1, GRID_CONCEPT_ID).Value = _dr("CSTC_CONCEPT_ID").ToString()
        Me.dg_concepts.Cell(Me.dg_concepts.NumRows - 1, GRID_CONCEPT_ONLY_NATIONAL_CURRENCY).Value = CType(_dr("CSTC_ONLY_NATIONAL_CURRENCY"), Boolean)
        Me.dg_concepts.Cell(Me.dg_concepts.NumRows - 1, GRID_CONCEPT_SOURCE_TARGET_ID).Value = _dr("CSTC_SOURCE_TARGET_ID").ToString()
        Me.dg_concepts.Cell(Me.dg_concepts.NumRows - 1, GRID_CONCEPT_NAME).Value = _dr("CC_DESCRIPTION").ToString()

        _col_index = GRID_CONCEPT_AMOUNT

        For Each _row As DataRow In m_currencies_types.Rows  ' _currencies_array

          _meter_value = 0

          For Each _concept_row As DataRow In _dt_concepts_movements.Rows

            If CType(_dr("CSTC_CONCEPT_ID"), Int64) = (CType(_concept_row("CM_TYPE"), Int64) - 1000000) _
               And _concept_row("CM_CURRENCY_ISO_CODE").ToString() = _row("CGC_ISO_CODE") _
               And _concept_row("CM_CAGE_CURRENCY_TYPE").ToString() = _row("TYPE") Then

              _meter_value = _meter_value + CType(_concept_row("CM_ADD_AMOUNT"), Decimal)

            ElseIf CType(_dr("CSTC_CONCEPT_ID"), Int64) = (CType(_concept_row("CM_TYPE"), Int64) - 2000000) _
               And _concept_row("CM_CURRENCY_ISO_CODE").ToString() = _row("CGC_ISO_CODE") _
               And _concept_row("CM_CAGE_CURRENCY_TYPE").ToString() = _row("TYPE") Then

              _meter_value = _meter_value - CType(_concept_row("CM_SUB_AMOUNT"), Decimal)

            End If

          Next

          Me.dg_concepts.Cell(Me.dg_concepts.NumRows - 1, _col_index).Value = _meter_value.ToString()

          Me.dg_concepts.Column(_col_index).Editable = False

          _col_index = _col_index + 1

        Next

      End If

    Next

    Me.m_cage_cls.ConceptsData = Me.GetConceptsDataTableFromDG(Me.dg_concepts, True)

    ' Dispose
    Call SafeDispose(_dt_concepts_to_show)
    'Call SafeDispose(_currencies_array)
    'Call SafeDispose(_dt_currencies_types)
    Call SafeDispose(_dt_concepts_movements)

    Return True

  End Function

  Private Function SetNoCashierConceptsValues(ByVal SourcetargetId As Int64, ByVal Type As Int32) As Boolean
    Dim _currencies_accepted As String
    Dim _currencies_array() As String
    Dim _col_index As String
    Dim _national_currency As String
    Dim _only_national_currency As Boolean
    Dim _dt_concepts_to_show() As DataRow
    Dim _sql_filter As String

    _sql_filter = "CSTC_SOURCE_TARGET_ID = " & SourcetargetId.ToString() & " AND CSTC_TYPE IN (2, " & Type.ToString() & ")"
    _dt_concepts_to_show = m_cage_cls.SourceTargetConceptsData.Select(_sql_filter)

    _currencies_accepted = GeneralParam.GetString("RegionalOptions", "CurrenciesAccepted")
    _national_currency = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")
    _currencies_array = _currencies_accepted.Split(";")

    For Each _dr As DataRow In _dt_concepts_to_show

      If _dr("CSTC_CONCEPT_ID").ToString() <> "0" Then

        _only_national_currency = Boolean.Parse(_dr("CSTC_ONLY_NATIONAL_CURRENCY").ToString())

        Me.dg_concepts.AddRow()
        Me.dg_concepts.Cell(Me.dg_concepts.NumRows - 1, GRID_CONCEPT_ID).Value = _dr("CSTC_CONCEPT_ID").ToString()
        Me.dg_concepts.Cell(Me.dg_concepts.NumRows - 1, GRID_CONCEPT_ONLY_NATIONAL_CURRENCY).Value = CType(_dr("CSTC_ONLY_NATIONAL_CURRENCY"), Boolean)
        Me.dg_concepts.Cell(Me.dg_concepts.NumRows - 1, GRID_CONCEPT_SOURCE_TARGET_ID).Value = _dr("CSTC_SOURCE_TARGET_ID").ToString()
        Me.dg_concepts.Cell(Me.dg_concepts.NumRows - 1, GRID_CONCEPT_NAME).Value = _dr("CC_DESCRIPTION").ToString()

        _col_index = GRID_CONCEPT_AMOUNT

        For Each _curr As DataRow In m_currencies_types.Rows

          Me.dg_concepts.Cell(Me.dg_concepts.NumRows - 1, _col_index).Value = Me.m_cage_cls.FormatAmount(Currency.Zero, True, _curr("CGC_ISO_CODE"))  ' Currency.Zero.ToString()

          If _only_national_currency And (_curr("CGC_ISO_CODE") <> _national_currency Or FeatureChips.IsChipsType(CType(_curr("TYPE"), CageCurrencyType))) Then
            Me.dg_concepts.Cell(Me.dg_concepts.NumRows - 1, _col_index).ForeColor = Color.Gray
          End If

          If Me.m_cage_cls.MovementID > 0 Then
            Me.dg_concepts.Column(_col_index).Editable = False
          Else
            Me.dg_concepts.Column(_col_index).Editable = True
          End If

          _col_index = _col_index + 1

        Next

      End If

    Next

    Me.m_cage_cls.ConceptsData = Me.GetConceptsDataTableFromDG(Me.dg_concepts, True)

    Return True

  End Function


  Private Sub GetSourceTargetAndType(ByRef SourceTarget, ByRef Type)

    If Me.rb_send.Checked Then
      Type = 1
    Else
      Type = 0
    End If

    If Me.rb_cashier.Checked Then
      SourceTarget = 10
    ElseIf Me.rb_gaming_table.Checked Then
      SourceTarget = 11
    ElseIf Me.rb_terminal.Checked Then
      SourceTarget = 12
    Else
      SourceTarget = Me.cmb_custom.Value
    End If

  End Sub

  Private Function GetConceptsDataTableFromDG(ByVal Grid As GUI_Controls.uc_grid, ByVal IgnoreTicketsAndCash As Boolean) As DataTable

    Dim _dt As DataTable
    Dim _new_row As DataRow
    Dim _idx_column As Int32

    _dt = New DataTable()
    _dt.Columns.Add("CONCEPT_ID", Type.GetType("System.Int64"))
    _dt.Columns.Add("CONCEPT_ONLY_NATIONAL_CURRENCY", Type.GetType("System.Boolean"))
    _dt.Columns.Add("CONCEPT_SOURCE_TARGET_ID", Type.GetType("System.Int32"))
    _dt.Columns.Add("CONCEPT_NAME", Type.GetType("System.String"))
    _dt.Columns.Add("CONCEPT_ISO_CODE", Type.GetType("System.String"))
    _dt.Columns.Add("CONCEPT_CAGE_CURRENCY_TYPE", Type.GetType("System.Int32"))
    _dt.Columns.Add("CONCEPT_AMOUNT", Type.GetType("System.Decimal"))

    ' For each concept
    For _idx_row As Int32 = 0 To Grid.NumRows - 1
      ' For each currency
      _idx_column = GRID_CONCEPT_AMOUNT
      For Each _row_cur As DataRow In m_currencies_types.Rows
        _new_row = _dt.NewRow()
        ' Concept Id
        _new_row("CONCEPT_ID") = Grid.Cell(_idx_row, GRID_CONCEPT_ID).Value
        ' Only national currency
        _new_row("CONCEPT_ONLY_NATIONAL_CURRENCY") = Grid.Cell(_idx_row, GRID_CONCEPT_ONLY_NATIONAL_CURRENCY).Value
        ' Source target id
        _new_row("CONCEPT_SOURCE_TARGET_ID") = Grid.Cell(_idx_row, GRID_CONCEPT_SOURCE_TARGET_ID).Value
        ' Concept Name
        _new_row("CONCEPT_NAME") = Grid.Cell(_idx_row, GRID_CONCEPT_NAME).Value
        ' Iso Code
        _new_row("CONCEPT_ISO_CODE") = _row_cur(1)
        ' CageCurrencyType
        _new_row("CONCEPT_CAGE_CURRENCY_TYPE") = _row_cur(2)
        ' Amount
        _new_row("CONCEPT_AMOUNT") = WSI.Common.Format.ParseCurrency(Grid.Cell(_idx_row, _idx_column).Value)

        _dt.Rows.Add(_new_row)

        _idx_column += 1

      Next
    Next

    ' If tickets and cash
    If Not IgnoreTicketsAndCash Then
      If WSI.Common.Misc.IsFloorDualCurrencyEnabled() Then
        Call Me.AddCashAndTicketsRowsFloorDual(Grid, _dt)
      Else
        Call Me.AddCashAndTicketsRows(Grid, _dt)
      End If
    End If

    Return _dt

  End Function

  Private Sub AddCashAndTicketsRows(ByVal Grid As GUI_Controls.uc_grid, ByRef Data As DataTable)

    Dim _new_row As DataRow
    Dim _total_cash As Decimal

    'For _idx_curr As Int32 = 0 To m_currencies_types.Rows.Count - 1

    '  ' Cash
    '  _new_row = Data.NewRow()

    '  _new_row.Item("CONCEPT_ID") = CInt(CageMeters.CageConceptId.Bills)
    '  _new_row.Item("CONCEPT_SOURCE_TARGET_ID") = Me.m_cage_cls.SourceTargetId
    '  _new_row.Item("CONCEPT_NAME") = "Cash"
    '  _new_row.Item("CONCEPT_ISO_CODE") = m_currencies_types.Rows(_idx_curr)("CGC_ISO_CODE")
    '  _new_row.Item("CONCEPT_CAGE_CURRENCY_TYPE") = m_currencies_types.Rows(_idx_curr)("TYPE")
    '  _new_row.Item("CONCEPT_AMOUNT") = Me.dg_total.Cell(_idx_curr, GRID_TOTAL_COLUMN_TOTAL).Value

    '  _total_cash = CType(_new_row.Item("CONCEPT_AMOUNT"), System.Decimal) 'total efectivo billetes y monedas

    '  If _total_cash - Me.m_cage_cls.m_type_new_collection.real_coins_collection_amount > 0 Then
    '    If _new_row.Item("CONCEPT_ISO_CODE") = m_national_currency And CType(_new_row.Item("CONCEPT_CAGE_CURRENCY_TYPE"), CageCurrencyType) = CageCurrencyType.Bill Then
    '      ' Add bills
    '      _new_row.Item("CONCEPT_AMOUNT") = _total_cash - Me.m_cage_cls.m_type_new_collection.real_coins_collection_amount
    '    End If
    '  End If

    '  Data.Rows.Add(_new_row)

    'Next

    For _idx_curr As Int32 = 0 To m_currencies_types.Rows.Count - 1

      If m_currencies_types.Rows(_idx_curr)("CGC_ISO_CODE") = m_national_currency And CType(m_currencies_types.Rows(_idx_curr)("TYPE"), CageCurrencyType) = CageCurrencyType.Bill Then
        ' Cash
        _new_row = Data.NewRow()
        _new_row.Item("CONCEPT_ID") = CInt(CageMeters.CageConceptId.Bills)
        _new_row.Item("CONCEPT_SOURCE_TARGET_ID") = Me.m_cage_cls.SourceTargetId
        _new_row.Item("CONCEPT_NAME") = "Cash"
        _new_row.Item("CONCEPT_ISO_CODE") = m_currencies_types.Rows(_idx_curr)("CGC_ISO_CODE")
        _new_row.Item("CONCEPT_CAGE_CURRENCY_TYPE") = m_currencies_types.Rows(_idx_curr)("TYPE")

        If (_idx_curr < Me.dg_total.NumRows) Then
          _new_row.Item("CONCEPT_AMOUNT") = Me.dg_total.Cell(_idx_curr, GRID_TOTAL_COLUMN_TOTAL).Value
        Else
          _new_row.Item("CONCEPT_AMOUNT") = 0
        End If

        _total_cash = CType(_new_row.Item("CONCEPT_AMOUNT"), System.Decimal) 'total efectivo billetes y monedas

        If _total_cash - Me.m_cage_cls.m_type_new_collection.real_coins_collection_amount > 0 Then
          ' Add bills
          _new_row.Item("CONCEPT_AMOUNT") = _total_cash - Me.m_cage_cls.m_type_new_collection.real_coins_collection_amount
          Data.Rows.Add(_new_row)
        End If

        If Me.m_cage_cls.m_type_new_collection.real_coins_collection_amount > 0 Then
          ' Add Coins
          _new_row = Data.NewRow()
          _new_row.Item("CONCEPT_ID") = CInt(CageMeters.CageConceptId.Coins)
          _new_row.Item("CONCEPT_SOURCE_TARGET_ID") = Me.m_cage_cls.SourceTargetId
          _new_row.Item("CONCEPT_NAME") = "Coins"
          _new_row.Item("CONCEPT_ISO_CODE") = m_currencies_types.Rows(_idx_curr)("CGC_ISO_CODE")
          _new_row.Item("CONCEPT_CAGE_CURRENCY_TYPE") = m_currencies_types.Rows(_idx_curr)("TYPE")
          _new_row.Item("CONCEPT_AMOUNT") = Me.m_cage_cls.m_type_new_collection.real_coins_collection_amount

          Data.Rows.Add(_new_row)
        End If

        If Me.m_cage_cls.m_type_new_collection.real_ticket_sum > 0 Then
          ' Add tickets
          _new_row = Data.NewRow()
          _new_row.Item("CONCEPT_ID") = CInt(CageMeters.CageConceptId.Tickets)
          _new_row.Item("CONCEPT_SOURCE_TARGET_ID") = Me.m_cage_cls.SourceTargetId
          _new_row.Item("CONCEPT_NAME") = "Tickets"
          _new_row.Item("CONCEPT_ISO_CODE") = m_currencies_types.Rows(_idx_curr)("CGC_ISO_CODE")
          _new_row.Item("CONCEPT_CAGE_CURRENCY_TYPE") = m_currencies_types.Rows(_idx_curr)("TYPE")
          _new_row.Item("CONCEPT_AMOUNT") = Me.m_cage_cls.m_type_new_collection.real_ticket_sum

          Data.Rows.Add(_new_row)
        End If

      End If
    Next

  End Sub

  Private Sub AddCashAndTicketsRowsFloorDual(ByVal Grid As GUI_Controls.uc_grid, ByRef Data As DataTable)

    Dim _new_row As DataRow
    Dim _total_cash As Decimal
    Dim _idx_row As Integer
    Dim _tickets_iso_code As Dictionary(Of CurrencyIsoType, Decimal)
    Dim _iso_code As String
    Dim _idx_curr As Int32

    If Me.m_cage_cls.TerminalIsoCode = Nothing Then
      Me.m_cage_cls.TerminalIsoCode = CurrencyExchange.GetNationalCurrency()
    End If

    _idx_curr = 0

    For Each _dt_row As DataRow In m_currencies_types.Select("CGC_ISO_CODE = '" & Me.m_cage_cls.TerminalIsoCode & "' AND TYPE = " & CageCurrencyType.Bill)

      If _dt_row("CGC_ISO_CODE") = Me.m_cage_cls.TerminalIsoCode And CType(_dt_row("TYPE"), CageCurrencyType) = CageCurrencyType.Bill Then
        ' Cash
        _new_row = Data.NewRow()
        _new_row.Item("CONCEPT_ID") = CInt(CageMeters.CageConceptId.Bills)
        _new_row.Item("CONCEPT_SOURCE_TARGET_ID") = Me.m_cage_cls.SourceTargetId
        _new_row.Item("CONCEPT_NAME") = "Cash"
        _new_row.Item("CONCEPT_ISO_CODE") = _dt_row("CGC_ISO_CODE")
        _new_row.Item("CONCEPT_CAGE_CURRENCY_TYPE") = _dt_row("TYPE")
        _new_row.Item("CONCEPT_AMOUNT") = Me.dg_total.Cell(_idx_curr, GRID_TOTAL_COLUMN_TOTAL).Value

        _total_cash = CType(_new_row.Item("CONCEPT_AMOUNT"), System.Decimal) 'total efectivo billetes y monedas

        If _total_cash - Me.m_cage_cls.m_type_new_collection.real_coins_collection_amount > 0 Then
          ' Add bills
          _new_row.Item("CONCEPT_AMOUNT") = _total_cash - Me.m_cage_cls.m_type_new_collection.real_coins_collection_amount
          Data.Rows.Add(_new_row)
        End If

        If Me.m_cage_cls.m_type_new_collection.real_coins_collection_amount > 0 Then
          ' Add Coins
          _new_row = Data.NewRow()
          _new_row.Item("CONCEPT_ID") = CInt(CageMeters.CageConceptId.Coins)
          _new_row.Item("CONCEPT_SOURCE_TARGET_ID") = Me.m_cage_cls.SourceTargetId
          _new_row.Item("CONCEPT_NAME") = "Coins"
          _new_row.Item("CONCEPT_ISO_CODE") = _dt_row("CGC_ISO_CODE")
          _new_row.Item("CONCEPT_CAGE_CURRENCY_TYPE") = _dt_row("TYPE")
          _new_row.Item("CONCEPT_AMOUNT") = Me.m_cage_cls.m_type_new_collection.real_coins_collection_amount

          Data.Rows.Add(_new_row)
        End If

        ' Tickets
        If Me.m_cage_cls.m_type_new_collection.real_ticket_sum > 0 Then
          ' Add tickets
          _tickets_iso_code = New Dictionary(Of CurrencyIsoType, Decimal)

          For _idx_row = 0 To Me.dg_collection_tickets.NumRows - 1
            _iso_code = Me.dg_collection_tickets.Cell(_idx_row, GRID_COLUMN_TICKET_ISO_CODE).Value
            If _tickets_iso_code.ContainsKey(New CurrencyIsoType(_iso_code, CurrencyExchangeType.CURRENCY)) Then
              _tickets_iso_code(New CurrencyIsoType(_iso_code, CurrencyExchangeType.CURRENCY)) += Currency.Unformat(Me.dg_collection_tickets.Cell(_idx_row, GRID_COLUMN_TICKET_AMOUNT_ISSUE).Value, _iso_code)
            Else
              _tickets_iso_code.Add(New CurrencyIsoType(_iso_code, CurrencyExchangeType.CURRENCY), Currency.Unformat(Me.dg_collection_tickets.Cell(_idx_row, GRID_COLUMN_TICKET_AMOUNT_ISSUE).Value, _iso_code))
            End If
          Next

          For Each _item As KeyValuePair(Of CurrencyIsoType, Decimal) In _tickets_iso_code
            _new_row = Data.NewRow()
            _new_row.Item("CONCEPT_ID") = CInt(CageMeters.CageConceptId.Tickets)
            _new_row.Item("CONCEPT_SOURCE_TARGET_ID") = Me.m_cage_cls.SourceTargetId
            _new_row.Item("CONCEPT_NAME") = "Tickets"
            _new_row.Item("CONCEPT_ISO_CODE") = _item.Key.IsoCode
            _new_row.Item("CONCEPT_CAGE_CURRENCY_TYPE") = _item.Key.Type
            _new_row.Item("CONCEPT_AMOUNT") = _item.Value

            Data.Rows.Add(_new_row)
          Next
        End If

      End If
      _idx_curr += 1
    Next

  End Sub

  Private Function GetAllowedCurrenciesByGamingTable() As DataTable
    Dim _iso_code As String
    Dim _table_curr As DataTable
    Dim _gaming_table As Int32?
    Dim _currency_type As CageCurrencyType
    Dim _row_to_delete As DataRow()
    Dim _rows_to_update As DataRow()

    _table_curr = Me.m_cage_cls.GetAllowedISOCode()

    _gaming_table = IIf(Me.cmb_gaming_table.SelectedIndex > 0, Me.cmb_gaming_table.Value(Me.cmb_gaming_table.SelectedIndex), Nothing)

    If Not _gaming_table Is Nothing And rb_gaming_table.Checked Then
      FeatureChips.ChipsSets.GamingTableId = _gaming_table
      For Each _row As DataRow In _table_curr.Copy.Rows
        _iso_code = _row.Item(1)
        _currency_type = _row.Item(2)
        If FeatureChips.IsCurrencyExchangeTypeChips(_currency_type) AndAlso Not FeatureChips.AllowedChipOperation(FeatureChips.ChipsOperation.Operation.CAGE_REQUEST, _currency_type, _iso_code, _gaming_table) Then
          _row_to_delete = _table_curr.Select("CGC_ISO_CODE = '" & _iso_code & "' AND TYPE = " & _currency_type & " ")
          _row_to_delete(0).Delete()
          _rows_to_update = Me.m_cage_cls.TableCurrencies.Select("ISO_CODE = '" & _iso_code & "' AND TYPE = " & _currency_type & " ")
          For Each _row_to_set_zero As DataRow In _rows_to_update
            _row_to_set_zero.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.QUANTITY) = 0
            _row_to_set_zero.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.TOTAL) = 0
            _row_to_set_zero.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.QUANTITY_DIFFERENCE) = 0
            _row_to_set_zero.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.TOTAL_DIFFERENCE) = 0
          Next
        End If
      Next

    End If

    Return _table_curr

  End Function

  Private Sub RefreshTabsBySelection(Optional ByVal IsoCode As String = "")
    Dim _iso_code As String
    Dim _currency_type As CageCurrencyType
    Dim _iso_code_national As String
    Dim _gaming_table As Int32?
    Dim _table_curr As DataTable
    Dim _rows_to_update As DataRow()
    Dim _row_to_check As DataRow()
    Dim _rb_checked As Int32
    Dim _cmb_selected_index As Int32
    Dim _deno_name As String
    Dim _currency_iso_type As CurrencyIsoType
    Dim _exit_without_refresh As Boolean
    Dim _is_cash As Boolean
    Dim _cashier_id As Int64
    Dim _loading_stock As Boolean
    Dim _tab As TabPage
    Dim _dg As uc_grid

    Try

      _exit_without_refresh = True

      If m_semaphore_tab_refresh Then
        Return
      End If
      m_semaphore_tab_refresh = True

      _rb_checked = 0
      _cmb_selected_index = cmb_gaming_table.SelectedIndex
      _loading_stock = False


      If rb_custom.Checked Then
        _rb_checked = 1
        _cmb_selected_index = -1
        m_cmb_selected_index = -1
      ElseIf rb_gaming_table.Checked Then
        _rb_checked = 2
      ElseIf rb_terminal.Checked Then
        _rb_checked = 3
        _cmb_selected_index = -1
        m_cmb_selected_index = -1
      ElseIf rb_cashier.Checked And rb_to_terminal.Checked Then
        _cmb_selected_index = cmb_terminals.SelectedIndex
      ElseIf rb_cashier.Checked And rb_to_user.Checked Then
        _cmb_selected_index = cmb_user.SelectedIndex
      Else
        _cmb_selected_index = -1
        m_cmb_selected_index = -1
      End If

      If _rb_checked <> m_rb_checked And m_rb_checked = 2 Or _cmb_selected_index <> m_cmb_selected_index Then
        m_rb_checked = _rb_checked
        m_cmb_selected_index = _cmb_selected_index
      Else
        m_semaphore_tab_refresh = False
        Return
      End If

      Me.SuspendLayout()


      If rb_send.Checked And rb_gaming_table.Checked Then
        _cashier_id = Me.m_cage_cls.GetIntegratedCashierGamingTableId(Me.cmb_gaming_table.Value)
        m_cage_cls.ClosingStocks = New ClosingStocks(_cashier_id)
      End If

      If m_force_grid_clear Then
        _exit_without_refresh = False
      End If

      If Not m_countr_enabled Or Not rb_cashier.Checked Or Not rb_to_terminal.Checked Then
        _table_curr = GetAllowedCurrenciesByGamingTable()
      Else
        _table_curr = Me.m_cage_cls.GetAllowedISOCode(IsoCode)
      End If
      _gaming_table = IIf(Me.cmb_gaming_table.SelectedIndex > 0, Me.cmb_gaming_table.Value(Me.cmb_gaming_table.SelectedIndex), Nothing)


      _loading_stock = CheckLoadClosingStock()

      If _table_curr.Rows.Count() <> Me.tb_currency.TabPages.Count() Or _loading_stock Then
        ' _
        'Not Me.m_cage_cls.ClosingStocks Is Nothing AndAlso Me.m_cage_cls.ClosingStocks.Type = ClosingStocks.ClosingStockType.FIXED AndAlso Not Me.m_cage_cls.ClosingStocks.SleepsOnTable Then
        _exit_without_refresh = False
      Else

        For Each _tab_page As TabPage In Me.tb_currency.TabPages
          _iso_code = _tab_page.Tag.ToString().Substring(0, ISO_CODE_LENGTH)
          If _iso_code.Equals(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2941)) Then
            _iso_code = Cage.CHIPS_ISO_CODE
          End If
          _currency_type = _tab_page.Tag.ToString().Substring(ISO_CODE_LENGTH, _tab_page.Tag.ToString().Length - ISO_CODE_LENGTH)

          _row_to_check = _table_curr.Select("CGC_ISO_CODE = '" & _iso_code & "' AND TYPE = " & _currency_type & " ")
          If _row_to_check.Length = 0 Then
            _exit_without_refresh = False
          End If
        Next

      End If

      If _exit_without_refresh Then
        m_semaphore_tab_refresh = False
        Return
      End If

      If Me.tb_currency.TabPages.Count() > 0 Then  ' AndAlso _table_curr.Rows.Count() <> Me.tb_currency.TabPages.Count() Then
        _iso_code_national = CurrencyExchange.GetNationalCurrency()
        If Me.tb_currency.SelectedTab Is Nothing Then
          Me.tb_currency.SelectedIndex = 0
        End If

        RemoveHandler dg_total.RowSelectedEvent, AddressOf dg_total_RowSelectedEvent
        RemoveHandler tb_currency.SelectedIndexChanged, AddressOf tb_currency_SelectedIndexChanged

        For Each _tab_page As TabPage In Me.tb_currency.TabPages
          _iso_code = _tab_page.Tag.ToString().Substring(0, ISO_CODE_LENGTH)
          If _iso_code.Equals(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2941)) Then
            _iso_code = Cage.CHIPS_ISO_CODE
          End If
          _currency_type = _tab_page.Tag.ToString().Substring(ISO_CODE_LENGTH, _tab_page.Tag.ToString().Length - ISO_CODE_LENGTH)

          _row_to_check = _table_curr.Select("CGC_ISO_CODE = '" & _iso_code & "' AND TYPE = " & _currency_type & " ")
          If _row_to_check.Length = 0 Or m_force_grid_clear Then
            Dim _grid As uc_grid
            _grid = _tab_page.Controls(0)
            For _idx_row As Int64 = 0 To _grid.NumRows - 1
              _grid.Cell(_idx_row, GRID_CURRENCY_COLUMN_QUANTITY).Value = String.Empty
              _grid.Cell(_idx_row, GRID_CURRENCY_COLUMN_TOTAL).Value = String.Empty
              _grid.Cell(_idx_row, GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).Value = String.Empty
            Next
            _rows_to_update = Me.m_cage_cls.TableCurrencies.Select(String.Format("ISO_CODE = '{0}' AND TYPE = {1}", _iso_code, CInt(_currency_type)))
            For Each _row_to_set_zero As DataRow In _rows_to_update
              _row_to_set_zero.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.QUANTITY) = 0
              _row_to_set_zero.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.TOTAL) = 0
              _row_to_set_zero.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.QUANTITY_DIFFERENCE) = 0
              _row_to_set_zero.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.TOTAL_DIFFERENCE) = 0
            Next

            Me.RefreshTotalGrid(False, _iso_code, _currency_type)
          End If
          Me.tb_currency.TabPages.Remove(_tab_page)
        Next

        Me.dg_total.Clear()
        Me.InitTotalGrid(_table_curr)

        FeatureChips.ChipsSets.GamingTableId = cmb_gaming_table.Value
        FeatureChips.ChipsSets.ForceRefresh()

        For Each _row As DataRow In _table_curr.Rows
          _iso_code = _row.Item("CGC_ISO_CODE")
          _currency_type = _row.Item("TYPE")
          _deno_name = _row.Item("NAME")
          _currency_iso_type = New CurrencyIsoType()

          _currency_iso_type.IsoCode = _iso_code
          _currency_iso_type.Type = _currency_type

          _is_cash = False
          Me.tb_currency.TabPages.Add(m_currency_tabs_dictionary(_currency_iso_type))
          If _currency_type = CageCurrencyType.ChipsRedimible Or _currency_type = CageCurrencyType.ChipsNoRedimible Or _currency_type = CageCurrencyType.ChipsColor Then
            _is_cash = True

            _tab = m_currency_tabs_dictionary(_currency_iso_type)

            For Each grd As Control In m_currency_tabs_dictionary(_currency_iso_type).Controls

              If Not TypeOf grd Is uc_grid Then
                Continue For
              End If
              _dg = TryCast(grd, uc_grid)

              For _idx_row As Int32 = 0 To _dg.NumRows - 1
                If FeatureChips.ChipsSets.GetChipsSetsEnabled().ContainsKey(_dg.Cell(_idx_row, GRID_CURRENCY_COLUMN_CHIP_SET_ID).Value) Then
                  _dg.Row(_idx_row).Height = m_row_height
                Else
                  _dg.Row(_idx_row).Height = 0
                End If
              Next
            Next

          End If
          Me.RefreshTotalGrid(_is_cash, _iso_code, _currency_type)
        Next
        AddHandler tb_currency.SelectedIndexChanged, AddressOf tb_currency_SelectedIndexChanged
        AddHandler dg_total.RowSelectedEvent, AddressOf dg_total_RowSelectedEvent

        Me.tb_currency.SelectedIndex = 0
      End If

      If rb_send.Checked And rb_gaming_table.Checked AndAlso _loading_stock Then
        LoadClosingStock()
        m_force_grid_clear = True
        Me.tb_currency.SelectedIndex = 0
      End If

      m_semaphore_tab_refresh = False
      Me.ResumeLayout(False)

    Catch _ex As Exception
      Log.Message(_ex.Message)
    End Try

  End Sub

  ' Load closing stock 
  Private Function CheckLoadClosingStock() As Boolean
    Dim _return As Boolean
    Dim _gt_sesion As GamingTablesSessions
    Dim _is_gt_close_session As Boolean
    Dim _is_first_gt_session As Boolean
    Dim _gaming_table_session_id As Int64
    Dim _ds As DataSet

    _gt_sesion = Nothing
    _ds = Nothing

    _return = True
    _is_first_gt_session = False
    _gaming_table_session_id = 0

    Try

      If m_cage_cls.ClosingStocks Is Nothing OrElse m_cage_cls.MovementID > 0 Then
        Return False
      End If

      Using _db_trx As New DB_TRX()
        GamingTablesSessions.GetOpenedSession(Me.cmb_gaming_table.Value, _gt_sesion, _db_trx.SqlTransaction)
        If Not _gt_sesion Is Nothing Then
          _gaming_table_session_id = _gt_sesion.GamingTableSessionId
        End If

        If m_cage_cls.ClosingStocks.SleepsOnTable AndAlso _gaming_table_session_id = 0 AndAlso GetLastSession(Me.cmb_gaming_table.Value, _gaming_table_session_id, _db_trx.SqlTransaction) Then
          If _gaming_table_session_id = 0 Then
            _is_first_gt_session = True
          End If
        End If
      End Using

      _is_gt_close_session = False
      If _gt_sesion Is Nothing Then
        _is_gt_close_session = True
      End If

      ' Only load stock when is "fixed stock", "session is close" and "chips don't sleep" or "is first send to a table" 
      ' Else clear
      If Not (m_cage_cls.ClosingStocks.Type = ClosingStocks.ClosingStockType.FIXED And _is_gt_close_session And (Not m_cage_cls.ClosingStocks.SleepsOnTable Or _is_first_gt_session)) Then
        Return False
      End If

      ' Don't clear a loaded template
      If Me.cmb_cage_template.Value > 0 Then
        If Me.m_cage_cls.LoadCageTemplate(Me.cmb_cage_template.Value, _ds) Then
          'Template
          m_cage_cls.ClosingStocks.CurrenciesStocks = _ds.Tables(TABLE_NAME_CURRENCIES)
        End If
        '_return = False
      End If

      ' Don't clear if have some problem
      If m_cage_cls.ClosingStocks Is Nothing Then
        _return = False
      End If

      ' Don't clear if have some problem
    Catch ex As Exception
      _return = False
    End Try

    Return _return

  End Function  '  CheckLoadStock


  Private Function GetLastSession(GamingTableId As Int32, ByRef GamingTableSesionId As Int64, Trx As SqlTransaction) As Boolean
    Dim _sb As StringBuilder

    _sb = New StringBuilder()

    Try
      'Check for exist one GamingTablesSession
      _sb = New StringBuilder()

      _sb.AppendLine("SELECT   TOP 1 ")
      _sb.AppendLine("         GTS_GAMING_TABLE_SESSION_ID  ")
      _sb.AppendLine("  FROM   GAMING_TABLES_SESSIONS ")
      _sb.AppendLine(" INNER   JOIN CASHIER_SESSIONS ON CS_SESSION_ID = GTS_CASHIER_SESSION_ID ")
      _sb.AppendLine(" WHERE   GTS_GAMING_TABLE_ID    = @pGamingTableId")
      _sb.AppendLine(" ORDER   BY GTS_GAMING_TABLE_SESSION_ID DESC ")

      Using _sql_cmd As New SqlCommand(_sb.ToString(), Trx.Connection, Trx)

        _sql_cmd.Parameters.Add("@pGamingTableId", SqlDbType.Int).Value = GamingTableId

        Using _reader As SqlDataReader = _sql_cmd.ExecuteReader()

          ' If Exists, Load values 
          If (_reader.Read) Then
            GamingTableSesionId = _reader.GetInt64(0)
          Else
            GamingTableSesionId = 0
          End If
        End Using

      End Using

      Return True
    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return False

  End Function ' GetLastSession()

  Private Function LoadClosingStock() As Boolean
    Dim _currencies As DataTable
    Dim _idx_tab As Integer
    Dim _datagrid As uc_grid
    Dim _idx_row As Integer
    Dim _iso_code As String
    Dim _currency_type As CageCurrencyType
    Dim _chip_id As Int64
    Dim _only_chips As Boolean
    Dim _denomination As String
    Dim _bill_coin_currency_type As CageCurrencyType
    Dim _str_filter As String
    Dim _row_money() As DataRow
    Dim _column_name As String

    _currencies = Nothing
    _only_chips = False
    _row_money = Nothing
    _column_name = String.Empty

    Try

      _currencies = m_cage_cls.ClosingStocks.CurrenciesStocks.Copy()
      m_original_template = _currencies.Copy

      If Not _currencies Is Nothing Then
        Dim _has_currencies As Boolean
        If _currencies.Columns.Contains("CAGE_CURRENCY_TYPE") Then
          _column_name = "CAGE_CURRENCY_TYPE"
        Else
          _column_name = "TYPE"
        End If


        _has_currencies = 0 < _currencies.Select("ISNULL(QUANTITY, 0) > 0 AND " & _column_name & " NOT IN (" & FeatureChips.ChipType.RE & ", " & FeatureChips.ChipType.NR & ", " & FeatureChips.ChipType.COLOR & ") AND ISO_CODE <> '" & Cage.CHIPS_ISO_CODE & "' ").Length

        If Not Me.m_gamingtables_transfer_currency_enabled And
           _has_currencies Then

          _only_chips = True
        End If

        'Datatable data is cleared
        For Each _row As DataRow In Me.m_cage_cls.TableCurrencies.Rows
          _row(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.QUANTITY) = DBNull.Value
          _row(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.TOTAL) = 0
        Next


        'We loop around all the tabs and the controls inside them
        m_redraw_grid = False
        For _idx_tab = 0 To Me.tb_currency.TabCount - 1
          _datagrid = Me.tb_currency.TabPages(_idx_tab).Controls.Item(0)
          _datagrid.Redraw = False
          Me.tb_currency.SelectTab(_idx_tab)
          'Loop around the elements to put the template value of each one of them
          For _idx_row = 0 To _datagrid.NumRows - 1
            _iso_code = _datagrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_ISO_CODE).Value
            _currency_type = _datagrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_CURRENCY_TYPE).Value
            If _datagrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_CHIP_ID).Value <> "" Then
              _chip_id = _datagrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_CHIP_ID).Value
            Else
              _chip_id = -1
            End If

            If (_only_chips) And Not FeatureChips.IsCurrencyExchangeTypeChips(_currency_type) Then
              Continue For
            End If

            If _datagrid.Row(_idx_row).Height = 0 Then
              Continue For
            End If

            _denomination = _datagrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_DENOMINATION).Value
            If _currency_type = CageCurrencyType.ChipsRedimible And Not _currencies.Columns.Contains("CHIP_ID") Then
              _iso_code = Cage.CHIPS_ISO_CODE
              _bill_coin_currency_type = CageCurrencyType.Bill
            ElseIf FeatureChips.IsCurrencyExchangeTypeChips(_currency_type) Then
              _bill_coin_currency_type = _currency_type
            Else
              _bill_coin_currency_type = Me.m_cage_cls.GetCurrencyTypeEnum(_datagrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_CURRENCY_TYPE_DESCRIPTION).Value)
            End If
            If CInt(_bill_coin_currency_type) = CageCurrencyType.Bill Then
              _str_filter = String.Format("ISO_CODE = '{0}' AND DENOMINATION = {1} AND " & _column_name & " IN ({2}, {3}) ", _iso_code, GUI_LocalNumberToDBNumber(_denomination.ToString), CInt(_bill_coin_currency_type), CInt(CageCurrencyType.Coin))
            ElseIf FeatureChips.IsCurrencyExchangeTypeChips(_currency_type) And _currencies.Columns.Contains("CHIP_ID") Then
              _str_filter = String.Format("ISO_CODE = '{0}' AND CHIP_ID = {1} AND " & _column_name & " IN ({2}) ", _iso_code, _chip_id, CInt(_bill_coin_currency_type))
            Else
              _str_filter = String.Format("ISO_CODE = '{0}' AND DENOMINATION = {1} AND " & _column_name & " = {2} ", _iso_code, GUI_LocalNumberToDBNumber(_denomination.ToString), CInt(_bill_coin_currency_type))
            End If

            _row_money = _currencies.Select(_str_filter)
            _datagrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_QUANTITY).Value = ""
            _datagrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).Value = ""
            If _row_money.Length > 0 Then
              If Not _row_money(0)("QUANTITY") Is DBNull.Value Then
                If _row_money(0)("DENOMINATION") > 0 Or _chip_id >= 0 Then
                  _datagrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_QUANTITY).Value = _row_money(0)("QUANTITY")
                End If
                If _row_money(0)("DENOMINATION") = 0 Then
                  _datagrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).Value = _row_money(0)("QUANTITY")
                Else
                  _datagrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).Value = _row_money(0)("QUANTITY") * _row_money(0)("DENOMINATION")
                End If

                _row_money(0).Delete()
                If _denomination >= 0 Then
                  CellDataChangeHandler(_idx_row, GRID_CURRENCY_COLUMN_QUANTITY)
                Else
                  CellDataChangeHandler(_idx_row, GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL)
                End If
              End If
            End If
            _datagrid.IsSelected(_idx_row) = False
          Next
        Next

      End If

      'The Totals grid is refreshed
      If Me.tb_currency.TabCount > 0 Then
        Me.dg_total.Clear()
        InitTotalGrid(Me.m_cage_cls.GetAllowedISOCode())
        FirstUpdateTotalGrid()
        Me.tb_currency.SelectTab(0)
        _datagrid = Me.tb_currency.TabPages(0).Controls.Item(0)
        _datagrid.TopRow = 0
        _datagrid.Redraw = True
      End If
      m_redraw_grid = True

    Catch ex As Exception
      m_redraw_grid = True
    End Try

  End Function  '  LoadStock

#Region " CountR "

  ''' <summary>
  ''' Get CountR cashier session id
  ''' </summary>
  ''' <param name="ExistOpenSession"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetCountRCashierSessionId(ByRef ExistOpenSession As Boolean) As Boolean
    Dim _cr As CountR
    Dim _csi As CashierSessionInfo
    Dim _user_name As String
    Dim _user_id As Integer

    _cr = New CountR()
    _csi = New CashierSessionInfo()
    _user_name = String.Empty
    _user_id = 0

    If Not ExistOpenSession Then
      Using _db_trx As New DB_TRX()

        ' Get CountR data
        If Not CountR.DB_GetCountR(Me.m_cage_cls.CountR.Id, _cr, _db_trx.SqlTransaction) Then
          Return False
        End If

        ' Get SystemUser ID 
        If Not Cashier.GetSystemUser(GU_USER_TYPE.SYS_REDEMPTION, _db_trx.SqlTransaction, _user_id, _user_name) Then
          Return False
        End If

        ' Get CashierSessionInfo and CountRSessionInfo. 
        ExistOpenSession = Cashier.GetCashierSessionFromCountR(_cr.Id, _user_id, CASHIER_SESSION_STATUS.OPEN, _csi, _db_trx.SqlTransaction)

        If ExistOpenSession Then
          Me.m_cage_cls.CashierSessionId = _csi.CashierSessionId
        End If

      End Using
    End If

    Return True

  End Function


  Private Sub CountR_GetISOCode()

    Dim _tab As TabPage
    Dim _uc_grid As uc_grid
    Dim _iso_code As String
    Dim _deleted_currencys As System.Collections.ObjectModel.Collection(Of DeletedCurrencys) = Nothing
    Dim _terminal_isocode As String

    If (Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MOVEMENT OrElse _
        Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT) AndAlso _
        Me.panel_cashier_type.Visible = False AndAlso Me.cmb_terminals.Visible = False Then

      _terminal_isocode = m_cage_cls.GetCountRIsoCode(Me.m_cage_cls.TerminalCashierID)

      If Not (_terminal_isocode Is Nothing OrElse String.IsNullOrEmpty(_terminal_isocode)) Then

        _deleted_currencys = GridDeleteNegativeDenominations()

        For Each _tab In Me.tb_currency.TabPages
          If TypeOf _tab.Controls(0) Is uc_grid Then
            _uc_grid = _tab.Controls(0)
            If Not String.IsNullOrEmpty(_tab.Tag) Then
              _iso_code = _tab.Tag
            Else
              _iso_code = _uc_grid.Cell(0, GRID_CURRENCY_COLUMN_ISO_CODE).Value
            End If

            If _iso_code = _terminal_isocode Then
              If Utils.IsTitoMode Then 'AndAlso IsCountRCollection
                'Call GridAddNegativeDenominations(Cage.TICKETS_CODE, _uc_grid, _iso_code)
              Else
                Call GridAddNegativeDenominations(Cage.BANK_CARD_CODE, _uc_grid, _iso_code, CageCurrencyType.Others, DeletedCurrencys.SearchDenomination(Cage.BANK_CARD_CODE, _iso_code, CageCurrencyType.Others, _deleted_currencys))
                Call GridAddNegativeDenominations(Cage.CHECK_CODE, _uc_grid, _iso_code, CageCurrencyType.Others, DeletedCurrencys.SearchDenomination(Cage.CHECK_CODE, _iso_code, CageCurrencyType.Others, _deleted_currencys))
                Call GridAddNegativeDenominations(Cage.COINS_CODE, _uc_grid, _iso_code, CageCurrencyType.Others, DeletedCurrencys.SearchDenomination(Cage.COINS_CODE, _iso_code, CageCurrencyType.Others, _deleted_currencys))

              End If
            End If

            Substract(_iso_code, _deleted_currencys)

          End If
        Next
      End If
    End If

  End Sub

#End Region


  ' PURPOSE: Return true if some cell is empty.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Function IsCellGridEmpty() As Boolean

    Dim _tab As TabPage
    Dim _ctrl As Control
    Dim _grid As uc_grid
    Dim _value As String
    Dim _value1 As String
    Dim _index As Integer

    For Each _tab In Me.tb_currency.TabPages

      For Each _ctrl In _tab.Controls

        If (_ctrl.GetType() Is GetType(uc_grid)) Then

          _grid = CType(_ctrl, uc_grid)

          For _index = 0 To _grid.NumRows - 1

            _value = _grid.Cell(_index, 12).Value
            _value1 = _grid.Cell(_index, 13).Value

            If String.IsNullOrEmpty(_value.Trim()) AndAlso String.IsNullOrEmpty(_value1.Trim()) Then
              Return True
            End If

          Next

        End If

      Next

    Next

    Return False

  End Function 'IsCellGridEmpty

  ''' <summary>
  ''' Set grids if the currency is ediatable or not
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SetGrids()

    Dim _grid As uc_grid

    For Each _tab As TabPage In tb_currency.TabPages

      Me.m_cells_are_editable = True

      If rb_send.Checked AndAlso rb_gaming_table.Checked AndAlso Not FeatureChips.IsCurrencyExchangeTypeChips(_tab.Tag.ToString().Substring(ISO_CODE_LENGTH, _tab.Tag.ToString().Length - ISO_CODE_LENGTH)) Then
        Me.m_cells_are_editable = Me.m_gamingtables_transfer_currency_enabled
      End If

      If (_tab.Controls(0).GetType() Is GetType(uc_grid)) Then
        _grid = _tab.Controls(0)
        _grid.Column(GRID_CURRENCY_COLUMN_QUANTITY).Editable = Me.m_cells_are_editable
        _grid.Column(GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).Editable = Me.m_cells_are_editable

      End If

    Next

  End Sub ' SetGrids

#End Region ' Private functions

#Region "Events"

  Private Sub cmb_user_ValueChangedEvent() Handles cmb_user.ValueChangedEvent
    Dim _cage_session_id As Long
    Dim _id_value As Long
    Dim _str_field As String
    Dim _verify_cashier_session_is_closed As Boolean

    Call m_cage_cls.SetCashierData(Me.cmb_user.Value, Me.ef_cashier_date, Me.ef_cashier_name)
    Me.cmb_user.Update()
    _verify_cashier_session_is_closed = False

    If Me.m_cage_cls.OpenMode <> CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MOVEMENT Then

      If Me.m_cage_cls.MovementID <> 0 Then
        _id_value = Me.m_cage_cls.MovementID
        _str_field = "CGM_MOVEMENT_ID"
      Else
        _id_value = Me.cmb_user.Value
        _str_field = "CGM_USER_CASHIER_ID"
        _verify_cashier_session_is_closed = True
      End If

      'If _id_value > 0 AndAlso Cage.IsCageSessionOpen(_cage_session_id, _id_value, _str_field) Then
      If _id_value > 0 AndAlso Cage.IsCageSessionOpen(_cage_session_id, _id_value, _str_field, _verify_cashier_session_is_closed) Then

        If Not Me.m_gaming_day Then

          ' If this user has another cage session related
          If Me.m_cage_cls.CageSessionId > 0 _
             And Me.m_cage_cls.CageSessionId <> _cage_session_id Then

            NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4582), ENUM_MB_TYPE.MB_TYPE_WARNING)
            Me.cmb_user.SelectedIndex = -1
            Me.cmb_user.TextValue = String.Empty
          Else
            Me.cmb_cage_sessions.Value = _cage_session_id
            ' RMS - If gaming day enabled, session can be changed
            Me.cmb_cage_sessions.IsReadOnly = Not Me.m_gaming_day
          End If

        End If

      ElseIf Me.m_cage_cls.CageSessionId <= 0 Then
        If Cage.GetNumberOfOpenedCages() > 1 Then
          ' If it has a value selected - don't set as default
          If Me.cmb_cage_sessions.TextValue = String.Empty _
             Or Me.cmb_cage_sessions.IsReadOnly = True Then

            Me.cmb_cage_sessions.IsReadOnly = False
            Me.cmb_cage_sessions.SelectedIndex = -1
            Me.cmb_cage_sessions.TextValue = String.Empty
          End If
        End If
      End If
    End If

  End Sub ' cmb_user_ValueChangedEvent

  Private Sub cmb_gaming_table_ValueChangedEvent() Handles cmb_gaming_table.ValueChangedEvent
    Dim _cage_session_id As Long
    Dim _cashier_id As Long

    If rb_gaming_table.Checked Then

      If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.VIEW_MOVEMENT Or Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT_FROM_GAMINGTABLES Then
        Exit Sub
      End If

      Call m_cage_cls.SetCashierData(Me.cmb_user.Value, Me.ef_cashier_date, Me.ef_cashier_name)
      Me.cmb_user.Update()

      If Me.m_cage_cls.OpenMode <> CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MOVEMENT Then

        If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CLOSE_VIRTUAL Then
          Return
        End If

        _cashier_id = Me.m_cage_cls.GetIntegratedCashierGamingTableId(Me.cmb_gaming_table.Value)

        ' If this user has another cage session related
        If _cashier_id <> -1 AndAlso Cage.AreCageAndCashierSessionOpen(_cage_session_id, _cashier_id) Then
          If Me.m_cage_cls.CageSessionId > 0 And Me.m_cage_cls.CageSessionId <> _cage_session_id Then
            NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4582), ENUM_MB_TYPE.MB_TYPE_WARNING)
            Me.cmb_user.SelectedIndex = -1
            Me.cmb_user.TextValue = String.Empty
          Else
            Me.cmb_cage_sessions.Value = _cage_session_id
            Me.cmb_cage_sessions.IsReadOnly = True
          End If
        ElseIf Me.m_cage_cls.CageSessionId <= 0 Then
          If Cage.GetNumberOfOpenedCages() > 1 Then
            ' If it has a value selected - don't set as default
            If Me.cmb_cage_sessions.TextValue = String.Empty _
               Or Me.cmb_cage_sessions.IsReadOnly = True Then

              Me.cmb_cage_sessions.IsReadOnly = False
              Me.cmb_cage_sessions.SelectedIndex = -1
              Me.cmb_cage_sessions.TextValue = String.Empty
            End If
          End If
        End If

        Call RefreshTabsBySelection()
      End If
    End If

  End Sub ' cmb_gaming_table_ValueChangedEvent

  Private Sub tb_currency_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tb_currency.SelectedIndexChanged
    Dim _datagrid As uc_grid

    If m_redraw_grid Then
      _datagrid = Me.tb_currency.SelectedTab.Controls.Item(0)
      _datagrid.Redraw = True
    End If

    RemoveHandler dg_total.RowSelectedEvent, AddressOf dg_total_RowSelectedEvent
    Call Me.SelectRowDgTotal()
    AddHandler dg_total.RowSelectedEvent, AddressOf dg_total_RowSelectedEvent

  End Sub ' tb_currency_SelectedIndexChanged

  Private Sub dg_total_RowSelectedEvent(ByVal SelectedRow As System.Int32) Handles dg_total.RowSelectedEvent
    If SelectedRow >= 0 _
      And m_selected_currency_tab <> Me.dg_total.Cell(SelectedRow, TOTAL_GRID_COLUMNS.CURRENCY).Value & Me.dg_total.Cell(SelectedRow, TOTAL_GRID_COLUMNS.CURRENCY_TYPE).Value Then

      RemoveHandler tb_currency.SelectedIndexChanged, AddressOf tb_currency_SelectedIndexChanged
      Call Me.SelectTab(SelectedRow)
      AddHandler tb_currency.SelectedIndexChanged, AddressOf tb_currency_SelectedIndexChanged

      Me.dg_concepts.Visible = False

    End If
  End Sub ' dg_total_RowSelectedEvent

  Private Sub rb_custom_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_custom.CheckedChanged

    m_changed_values = True

    If rb_custom.Checked Then

      If rb_send.Checked Then
        Me.m_cage_cls.OperationType = Cage.CageOperationType.ToCustom
      Else
        Me.m_cage_cls.OperationType = Cage.CageOperationType.FromCustom
      End If

      Call Me.CheckRadioButtonsToSetLayouts()
    Else
      Me.m_last_checked = rb_custom
    End If

    Me.SetConceptsGridAndTabValues()


  End Sub ' rb_custom_CheckedChanged

  Private Sub rb_cashier_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_cashier.CheckedChanged

    m_changed_values = True

    If rb_cashier.Checked Then

      If rb_send.Checked Then
        Me.m_cage_cls.OperationType = Cage.CageOperationType.ToCashier
      Else
        Me.m_cage_cls.OperationType = Cage.CageOperationType.FromCashier
      End If

      Call Me.CheckRadioButtonsToSetLayouts()
    Else
      Me.m_last_checked = rb_cashier
    End If

    Me.SetConceptsGridAndTabValues()

  End Sub ' rb_cashier_CheckedChanged

  Private Sub rb_reception_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_reception.CheckedChanged

    m_changed_values = True

    If Me.rb_reception.Checked Then

      Call Me.SetEditableOnCurrencyGrids()

      If (Not WSI.Common.Misc.IsCountREnabled()) Then
        If Me.rb_cashier.Checked Then
          Me.rb_cashier.Checked = False
          If Me.rb_gaming_table.Enabled Then
            Me.rb_gaming_table.Checked = True
          Else
            Me.rb_custom.Checked = True
          End If
        End If
        Me.rb_cashier.Enabled = False
      Else
        Me.rb_cashier.Enabled = True
        Me.rb_cashier.Checked = True
        Me.rb_to_terminal.Checked = True
        Me.rb_to_user.Visible = False
        Me.rb_to_terminal.Visible = False

        Call Me.m_cage_cls.SetComboTerminals(Me.cmb_terminals, True)
      End If

      Me.cmb_custom.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3427)

      If rb_cashier.Checked Then
        m_cage_cls.OperationType = Cage.CageOperationType.FromCashier
      ElseIf rb_gaming_table.Checked Then
        If cb_close_gaming_table.Checked Then
          m_cage_cls.OperationType = Cage.CageOperationType.FromGamingTableClosing
        Else
          m_cage_cls.OperationType = Cage.CageOperationType.FromGamingTable
        End If
        Call ReDrawGrids(True)
      ElseIf rb_custom.Checked Then
        m_cage_cls.OperationType = Cage.CageOperationType.FromCustom
        Call ReDrawGrids(, True)
      End If

      ' Set combo with gaming table
      Call Me.m_cage_cls.SetComboGamingTable(Me.cmb_gaming_table, True, True)
      Me.cmb_gaming_table.TextValue = " "
    Else
      Me.m_last_checked = Me.rb_reception
    End If

    If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT Then
      Call Me.CheckRadioButtonsToSetLayouts()
    End If

    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT
    'We check if the template options should be enabled
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = (rb_send.Checked AndAlso _
                                                          Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT AndAlso _
                                                          Me.m_mode_edit = MODE_EDIT.NONE)

    Me.gb_template.Visible = Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT
    Me.cmb_cage_template.Enabled = (rb_send.Checked AndAlso Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT)

    Call Me.SetConceptsGridAndTabValues()

  End Sub ' rb_reception_CheckedChanged

  Private Sub rb_send_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_send.CheckedChanged

    m_changed_values = True

    If Me.rb_send.Checked Then

      Call Me.ReDrawGrids()

      Me.gb_source_target.Enabled = True

      If Me.m_cage_auto_mode AndAlso (Not WSI.Common.Misc.IsCountREnabled()) Then
        Me.rb_cashier.Enabled = False
        If m_is_player_tracking_enabled Then
          Me.rb_custom.Checked = True
        Else
          Me.rb_gaming_table.Checked = True
        End If
      Else
        Me.rb_cashier.Enabled = True
        Me.rb_cashier.Checked = True

        Me.rb_to_user.Checked = Not Me.m_cage_auto_mode
        Me.rb_to_terminal.Checked = Me.m_cage_auto_mode
        Me.rb_to_user.Visible = Not Me.m_cage_auto_mode
        Me.rb_to_terminal.Visible = Not Me.m_cage_auto_mode
      End If

      If rb_cashier.Checked Then
        m_cage_cls.OperationType = Cage.CageOperationType.ToCashier
      ElseIf rb_gaming_table.Checked Then
        m_cage_cls.OperationType = Cage.CageOperationType.ToGamingTable
      ElseIf rb_custom.Checked Then
        m_cage_cls.OperationType = Cage.CageOperationType.ToCustom
      End If

      Me.cmb_custom.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3396)
      Call Me.m_cage_cls.SetComboGamingTable(Me.cmb_gaming_table, False, True, Me.m_cage_auto_mode)
      Me.cmb_gaming_table.TextValue = " "

      Call Me.m_cage_cls.SetComboTerminals(Me.cmb_terminals)
    Else
      Me.m_last_checked = Me.rb_send
    End If

    Call Me.CheckRadioButtonsToSetLayouts()

    Me.SetConceptsGridAndTabValues()

  End Sub

  Private Sub rb_to_user_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_to_user.CheckedChanged

    m_changed_values = True

    If rb_to_user.Checked Then
      Call Me.CheckRadioButtonsToSetLayouts()
    End If

  End Sub ' rb_to_user_CheckedChanged

  Private Sub rb_to_terminal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_to_terminal.CheckedChanged

    m_changed_values = True

    If rb_to_terminal.Checked Then
      Call Me.CheckRadioButtonsToSetLayouts()
    End If
  End Sub ' rb_to_terminal_CheckedChanged

  Private Sub rb_gaming_table_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_gaming_table.CheckedChanged

    m_changed_values = True

    Call RefreshTabsBySelection()

    If rb_gaming_table.Checked Then

      If rb_send.Checked And Not Me.m_applying_template Then
        'Preguntar
        Dim _continue As Boolean

        _continue = Me.ShowMessageCurrenciesDataWillBeDeleted()

        If Not _continue Then
          AddRemoveHandler(False)
          Me.m_last_checked.Checked = True
          AddRemoveHandler(True)
          Exit Sub
        End If
        Me.cmb_cage_template.SelectedIndex = -1

      End If
      If rb_send.Checked Then
        Me.m_cage_cls.OperationType = Cage.CageOperationType.ToGamingTable
      Else
        If cb_close_gaming_table.Checked Then
          Me.m_cage_cls.OperationType = Cage.CageOperationType.FromGamingTableClosing
        Else
          Me.m_cage_cls.OperationType = Cage.CageOperationType.FromGamingTable
        End If
      End If

      Call Me.CheckRadioButtonsToSetLayouts()

    Else
      Me.m_last_checked = rb_gaming_table
    End If

    Call Me.SetEditableOnCurrencyGrids()

    Me.SetConceptsGridAndTabValues()

  End Sub ' rb_gaming_table_CheckedChanged

  Private Function ShowMessageCurrenciesDataWillBeDeleted(Optional ByVal IsoCode As String = "") As Boolean
    Dim _id_nls As Integer

    If Me.m_gamingtables_transfer_currency_enabled Or Not Me.CheckAreValuesOnCurrencyGrids(IsoCode) Then
      Return True
    End If
    'Preguntar
    Dim rc As mdl_NLS.ENUM_MB_RESULT
    _id_nls = IIf(Me.m_cage_cls.IsCountR, 7288, 7144)

    rc = NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(_id_nls), _
                   ENUM_MB_TYPE.MB_TYPE_WARNING, _
                   ENUM_MB_BTN.MB_BTN_YES_NO, _
                   ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
                   IsoCode, _
                   Nothing)
    If rc = ENUM_MB_RESULT.MB_RESULT_NO Then
      Return False
    End If
    Return True
  End Function

  Private Sub btn_load_gaming_values_ClickEvent() Handles btn_load_gaming_values.ClickEvent
    If Me.cmb_gaming_table.TextValue = "" Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.cmb_gaming_table.Text)
      Me.cmb_gaming_table.Focus()
    Else
      If Not Me.m_cage_cls.LoadGamingTableValuesToTableCurrencies(Me.cmb_gaming_table.Value) Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4429), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Else
        m_gaming_values = True
        Call ReDrawGrids(True)
      End If
    End If
  End Sub ' btn_load_gaming_values_ClickEvent

  Private Sub cb_close_gaming_table_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cb_close_gaming_table.CheckedChanged
    If Me.cb_close_gaming_table.Checked Then
      Me.ef_gaming_table_visits.Enabled = True
    Else
      Me.ef_gaming_table_visits.Enabled = False
    End If

    ' LTC 28-NOV-2016
    Call CheckRadioButtonsToSetLayouts()

  End Sub ' cb_close_gaming_table_CheckedChanged

  Private Sub GetControlsDataForPrint(ByRef GroupDictionary As Dictionary(Of String, String))
    Dim _str_tmp As String

    _str_tmp = ""

    ' gb_cage_sessions + gb_movement_type + gb_source_target
    If gb_movement_type.Visible Then
      If rb_send.Checked Then
        _str_tmp = rb_send.Text
      Else
        _str_tmp = rb_reception.Text
      End If
    End If

    If Not String.IsNullOrEmpty(_str_tmp) Then
      _str_tmp += " - "
    End If

    If gb_source_target.Visible Then
      If rb_cashier.Checked Then
        _str_tmp += rb_cashier.Text
      ElseIf rb_gaming_table.Checked Then
        _str_tmp += rb_gaming_table.Text
      ElseIf rb_custom.Checked Then
        _str_tmp += rb_custom.Text
      Else
        _str_tmp += rb_terminal.Text
      End If
    End If

    'SGB 16-APR-2015: Fixed Bug 1085: Don't show in excel filter cage sesion if open multi cage sesion.
    If Not gb_movement_type.Visible And _
       Not gb_source_target.Visible And _
       gb_operation.Visible Then
      _str_tmp += ef_operation.TextValue
    End If

    If gb_cage_sessions.Visible Then
      _str_tmp += " (" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(3388) & ": " & IIf(Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE, cmb_cage_sessions.TextValue, ef_cage_session.Value) & ")"
      GroupDictionary.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2466), _str_tmp)
    End If

    ' gb_cashier
    If gb_cashier.Visible Then
      GroupDictionary.Add(ef_user_terminals_name.Text, ef_user_terminals_name.Value)
      GroupDictionary.Add(ef_cashier_name.Text, ef_cashier_name.Value & "(" & ef_cashier_date.Value & ")")
    End If

    ' gb_gaming_table
    If gb_gaming_table.Visible Then
      _str_tmp = ef_gaming_table.Value & " (" & ef_gaming_table_visits.Text & ": " & ef_gaming_table_visits.Value & ")"
      GroupDictionary.Add(gb_gaming_table.Text, _str_tmp)
    End If

    ' gb_custom
    If gb_custom.Visible And Not Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE Then
      GroupDictionary.Add(gb_custom.Text, ef_custom.Value)
    End If

    ' gb_collection_params
    If gb_collection_params.Visible Then
      _str_tmp = ef_terminal.Value & " (" & ef_insertion_date.Text & ": " & ef_insertion_date.Value & ", " _
               & ef_extraction_date.Text & ": " & ef_extraction_date.Value & ")" & ", " & ef_floor_id.Text & ": " & ef_floor_id.Value
      GroupDictionary.Add(ef_stacker_id.Text, ef_stacker_id.TextValue)
      GroupDictionary.Add(ef_terminal.Text, _str_tmp)
      GroupDictionary.Add(ef_employee.Text, ef_employee.Value)
      GroupDictionary.Add(lbl_notes.Text, tb_notes.Text)
      GroupDictionary.Add(ef_floor_id.Text, ef_floor_id.TextValue)
    End If

    If gb_operation.Visible Then
      GroupDictionary.Add(gb_operation.Text, ef_operation.Value)
      GroupDictionary.Add(ef_status_movement.Text, ef_status_movement.Value)
      GroupDictionary.Add(ef_cage_user.Text, ef_cage_user.Value)
      GroupDictionary.Add(ef_date_cage.Text, ef_date_cage.Value)
    End If

  End Sub ' GetControlsDataForPrint

  Private Sub GetTableFromTotals(ByVal DsCageMovement As DataSet, ByRef NumMaxColumns As Int32, ByRef NumMaxRows As Int32)
    Dim _idx_row As Int16
    Dim _idx_col As Int16
    Dim _idx_cur_col As Int16
    Dim _dt_tmp As DataTable
    Dim _dr_tmp As DataRow
    Dim _str_tmp As String
    Dim _str_cur_iso_code As String

    _str_cur_iso_code = ""

    _str_tmp = ""
    _dt_tmp = New DataTable()
    For _idx_col = 0 To Me.dg_total.NumColumns - 1
      If Me.dg_total.Column(_idx_col).Width > GRID_WIDTH_INDEX Then
        _str_tmp = Me.dg_total.Column(_idx_col).Header(0).Text() & "_" & Me.dg_total.Column(_idx_col).Header(1).Text()

        _dt_tmp.Columns.Add(_str_tmp, dg_total.Column(_idx_col).SystemType)

        NumMaxColumns += 1
      End If
    Next

    For _idx_row = 0 To Me.dg_total.NumRows - 1
      _idx_cur_col = 0
      _dr_tmp = _dt_tmp.NewRow()
      For _idx_col = 0 To Me.dg_total.NumColumns - 1
        If Me.dg_total.Column(_idx_col).Width > GRID_WIDTH_INDEX Then

          ' GetRawNumber
          If _idx_cur_col > 0 Then
            'If (_str_cur_iso_code = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2941)) Then
            '  _str_cur_iso_code = Cage.CHIPS_ISO_CODE
            'Else
            If (_str_cur_iso_code = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4600)) Then
              _str_cur_iso_code = m_national_currency
            End If

            If (_idx_cur_col = 1) Then
              _dr_tmp(_idx_cur_col) = Me.dg_total.Cell(_idx_row, _idx_col).Value
            Else
              _dr_tmp(_idx_cur_col) = GetRawNumber(Me.dg_total.Cell(_idx_row, _idx_col).Value, _str_cur_iso_code)
            End If
          Else
            If (Me.dg_total.Cell(_idx_row, _idx_col).Value <> Cage.CHIPS_COLOR) Then
              _dr_tmp(_idx_cur_col) = Me.dg_total.Cell(_idx_row, _idx_col).Value
            Else
              _dr_tmp(_idx_cur_col) = String.Empty
            End If
          End If

          _str_cur_iso_code = _dr_tmp(0)
          _idx_cur_col += 1
        End If
      Next

      _dt_tmp.Rows.Add(_dr_tmp)
    Next

    NumMaxRows = _idx_row
    DsCageMovement.Tables.Add(_dt_tmp)

    ' Dispose
    Call SafeDispose(_dt_tmp)

  End Sub ' GetTableFromTotals

  Private Sub GetTablesFromDenominations(ByVal DsCageMovement As DataSet, ByRef NumMaxColumns As Int32, ByRef NumMaxRows As Int32)
    Dim _idx_row As Int16
    Dim _idx_cur_col As Int16
    Dim _dt_tmp As DataTable
    Dim _dr_tmp As DataRow
    Dim _tab As TabPage
    Dim _tmp_grid As uc_grid
    Dim _str_tmp As String
    Dim _idx_col As Int16
    Dim _int_max_columns As Int32
    Dim _str_cur_iso_code As String
    Dim _isCountCage As Boolean

    _isCountCage = (Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE)
    _str_cur_iso_code = ""
    _dt_tmp = Nothing

    For Each _tab In Me.tb_currency.TabPages
      _int_max_columns = 0
      _dt_tmp = New DataTable(_tab.Text)
      _str_cur_iso_code = _tab.Tag.ToString.Substring(0, 3)

      'If _str_cur_iso_code.Equals(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2941)) Then
      '  _str_cur_iso_code = Cage.CHIPS_ISO_CODE
      'End If

      If _tab.Text.Equals(TICKETS_ISOCODE_NLS) Then
        _tmp_grid = _tab.Controls(dg_collection_tickets.Name)
        _str_cur_iso_code = m_national_currency
      Else
        _tmp_grid = _tab.Controls(0)
      End If

      For _idx_col = 0 To _tmp_grid.NumColumns - 1
        If _tmp_grid.Column(_idx_col).Width > GRID_WIDTH_INDEX Then
          _str_tmp = _tmp_grid.Column(_idx_col).Header(0).Text().Replace(".", "") & "_" & _tmp_grid.Column(_idx_col).Header(1).Text().Replace(".", "")

          _dt_tmp.Columns.Add(_str_tmp, _tmp_grid.Column(_idx_col).SystemType)

          _int_max_columns += 1
        End If
      Next

      If _int_max_columns > NumMaxColumns Then
        NumMaxColumns = _int_max_columns
      End If

      Dim _dec_value As Decimal
      Dim _to_include As Boolean

      For _idx_row = 0 To _tmp_grid.NumRows - 1
        _to_include = False
        _idx_cur_col = 0
        _dr_tmp = _dt_tmp.NewRow()
        For _idx_col = 0 To _tmp_grid.NumColumns - 1
          If _tmp_grid.Column(_idx_col).Width > GRID_WIDTH_INDEX Then

            If (Not _tmp_grid.Column(_idx_col).SystemType.Name = Type.GetType("System.String").Name _
                 And Not _tmp_grid.Cell(_idx_row, _idx_col).Value = "") Then
              _dec_value = GetRawNumber(_tmp_grid.Cell(_idx_row, _idx_col).Value, _str_cur_iso_code)
              _dr_tmp(_idx_cur_col) = _dec_value

              If _dec_value <> 0 Or _isCountCage Then
                _to_include = True
              End If
            Else
              If (_tmp_grid.Cell(_idx_row, _idx_col).Value = "") Then
                If (_tmp_grid.Column(_idx_col).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2270) And _tmp_grid.Column(_idx_col).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1291)) Then
                  _dr_tmp(_idx_cur_col) = _tmp_grid.Cell(_idx_row, _idx_col).BackColor().Name
                Else
                  _dr_tmp(_idx_cur_col) = System.DBNull.Value
                End If
                '_to_include = False
              Else
                _dr_tmp(_idx_cur_col) = _tmp_grid.Cell(_idx_row, _idx_col).Value

              End If


            End If

            _idx_cur_col += 1
          End If
        Next

        If _to_include Then
          _dt_tmp.Rows.Add(_dr_tmp)
        End If
      Next

      NumMaxRows += _idx_row
      DsCageMovement.Tables.Add(_dt_tmp)
    Next

    ' Dispose
    Call SafeDispose(_dt_tmp)

  End Sub ' GetTableFromTotals

  Private Sub GetTableFromConcepts(ByVal DsCageMovement As DataSet, ByRef NumMaxColumns As Int32, ByRef NumMaxRows As Int32)
    Dim _idx_row As Int16
    Dim _idx_col As Int16
    Dim _idx_cur_col As Int16
    Dim _dt_tmp As DataTable
    Dim _dr_tmp As DataRow
    Dim _str_tmp As String
    Dim _str_cur_iso_code As String

    _str_cur_iso_code = ""

    _str_tmp = ""
    _dt_tmp = New DataTable(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5472))
    For _idx_col = 0 To Me.dg_concepts.NumColumns - 1
      If Me.dg_concepts.Column(_idx_col).Width > GRID_WIDTH_INDEX Then
        _str_tmp = Me.dg_concepts.Column(_idx_col).Header(0).Text() & "_" & Me.dg_concepts.Column(_idx_col).Header(1).Text()

        _dt_tmp.Columns.Add(_str_tmp, dg_concepts.Column(_idx_col).SystemType)

        NumMaxColumns += 1
      End If
    Next

    For _idx_row = 0 To Me.dg_concepts.NumRows - 1
      _idx_cur_col = 0
      _dr_tmp = _dt_tmp.NewRow()
      For _idx_col = 0 To Me.dg_concepts.NumColumns - 1
        If Me.dg_concepts.Column(_idx_col).Width > GRID_WIDTH_INDEX Then

          ' GetRawNumber
          If _idx_cur_col > 0 Then
            _dr_tmp(_idx_cur_col) = GetRawNumber(Me.dg_concepts.Cell(_idx_row, _idx_col).Value, _str_cur_iso_code)
          Else
            _dr_tmp(_idx_cur_col) = Me.dg_concepts.Cell(_idx_row, _idx_col).Value
          End If

          _str_cur_iso_code = _dr_tmp(0)
          _idx_cur_col += 1
        End If
      Next

      _dt_tmp.Rows.Add(_dr_tmp)
    Next

    NumMaxRows = _idx_row
    DsCageMovement.Tables.Add(_dt_tmp)

    ' Dispose
    Call SafeDispose(_dt_tmp)

  End Sub ' GetTableFromConcepts


  Private Sub frm_cage_control_ClickEvent() Handles btn_open_events.ClickEvent

    m_frm_counter_event.ShowForEdit(Me.Parent)

  End Sub

  Private Sub tb_concepts_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tb_concepts.Click

    If (Me.dg_concepts.NumRows = 0) Then
      Me.SetConceptsValues()
    End If

    Me.dg_concepts.Visible = True

  End Sub ' tb_concepts_Click

  Private Sub tb_currency_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tb_currency.Click

    Me.dg_concepts.Visible = False

  End Sub ' tb_currency_Click

  Private Sub cmb_custom_ValueChangedEvent() Handles cmb_custom.ValueChangedEvent

    Me.SetConceptsValues()

  End Sub ' cmb_custom_ValueChangedEvent

  Private Sub cmb_terminals_ValueChangedEvent() Handles cmb_terminals.ValueChangedEvent

    Dim _terminal_isocode As String
    Dim _selected_terminal_id As Integer
    Dim _deleted_currencys As System.Collections.ObjectModel.Collection(Of DeletedCurrencys) = Nothing

    m_changed_values = True
    _terminal_isocode = Nothing

    Me.m_cage_cls.IsCountR = False

    If cmb_terminals.Count() > 0 And cmb_terminals.SelectedIndex > 0 And cmb_terminals.Value > 0 Then
      If (rb_send.Checked And rb_cashier.Checked And rb_to_terminal.Checked) Or _
         (rb_reception.Checked And rb_cashier.Checked And rb_to_terminal.Checked) Then

        _selected_terminal_id = cmb_terminals.Value

        Using _db_trx As New DB_TRX()
          Me.m_cage_cls.IsCountR = Me.m_cage_cls.IsCountRTerminal(_selected_terminal_id, _db_trx.SqlTransaction)
        End Using

        If (Me.m_cage_cls.IsCountR) Then
          If (Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.VIEW_MOVEMENT OrElse Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT) Then
            Return
          Else
            _terminal_isocode = m_cage_cls.GetCountRIsoCode(_selected_terminal_id)
          End If
        End If

        If _terminal_isocode Is Nothing Then
          Call Me.SetEditableOnCurrencyGrids()
        Else
          If Not Me.ShowMessageCurrenciesDataWillBeDeleted(_terminal_isocode) Then
            Me.cmb_terminals.SelectedIndex = -1
          Else
            Call Me.SetEditableOnCurrencyGrids(_terminal_isocode)
          End If

        End If
        'If Me.m_cage_cls.IsCountR Then

        '  If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MOVEMENT AndAlso Me.panel_cashier_type.Visible = False AndAlso Me.cmb_terminals.Visible = False Then
        '    'JCA: DO NOTHING
        '  ElseIf Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT Then
        '    Me.cb_close_countr.Enabled = True
        '  End If
        'Else
        '  Me.cb_close_countr.Enabled = False
        '  Call ReDrawGrids(, , Me.m_cage_cls.IsCountR)
        'End If
        If Not Me.m_cage_cls.IsCountR Then
          Call ReDrawGrids(, , Me.m_cage_cls.IsCountR)
        End If
      End If
    End If

  End Sub

#End Region ' Events

#Region "Tito's functions"
  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheetTicket()

    With Me.dg_collection_tickets

      Call .Init(GRID_TICKETS_COLUMNS_COUNT, GRID_HEADERS_COUNT)

      .Counter(COUNTER_TOTAL_ITEMS).Visible = False
      .Counter(COUNTER_TOTAL_ITEMS).BackColor = GetColor(COLOR_ALL_ITEMS_BACK)
      .Counter(COUNTER_TOTAL_ITEMS).ForeColor = GetColor(COLOR_ALL_ITEMS_FORE)

      .Counter(COUNTER_INFO).Visible = False
      .Counter(COUNTER_INFO).BackColor = GetColor(COLOR_INFO_BACK)
      .Counter(COUNTER_INFO).ForeColor = GetColor(COLOR_INFO_FORE)

      .Counter(COUNTER_WARNING).Visible = False
      .Counter(COUNTER_WARNING).BackColor = GetColor(COLOR_WARNING_BACK)
      .Counter(COUNTER_WARNING).ForeColor = GetColor(COLOR_WARNING_FORE)

      .Counter(COUNTER_ERROR).Visible = False
      .Counter(COUNTER_ERROR).BackColor = GetColor(COLOR_ERROR_BACK)
      .Counter(COUNTER_ERROR).ForeColor = GetColor(COLOR_ERROR_FORE)

      ' Ticket number
      .Column(GRID_COLUMN_TICKET_NUMBER_CORRECT_FORMAT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2269)
      .Column(GRID_COLUMN_TICKET_NUMBER_CORRECT_FORMAT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2259)
      .Column(GRID_COLUMN_TICKET_NUMBER_CORRECT_FORMAT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      ' DHA 13-JAN-2016: dual currency for tito tickets
      If WSI.Common.Misc.IsFloorDualCurrencyEnabled() Then
        .Column(GRID_COLUMN_TICKET_NUMBER_CORRECT_FORMAT).Width = GRID_WIDTH_TICKET_VALIDATION_NUMBER_DUAL_CUR
      Else
        .Column(GRID_COLUMN_TICKET_NUMBER_CORRECT_FORMAT).Width = GRID_WIDTH_TICKET_VALIDATION_NUMBER
      End If

      ' DHA 13-JAN-2016: dual currency for tito tickets
      ' Ticket amount issue
      .Column(GRID_COLUMN_TICKET_AMOUNT_ISSUE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2269)
      .Column(GRID_COLUMN_TICKET_AMOUNT_ISSUE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7069)
      .Column(GRID_COLUMN_TICKET_AMOUNT_ISSUE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_TICKET_AMOUNT_ISSUE).SystemType = Type.GetType("System.Decimal")
      If WSI.Common.Misc.IsFloorDualCurrencyEnabled() Then
        .Column(GRID_COLUMN_TICKET_AMOUNT_ISSUE).Width = GRID_WIDTH_TICKET_AMOUNT_ISSUE_DUAL_CUR
      Else
        .Column(GRID_COLUMN_TICKET_AMOUNT_ISSUE).Width = GRID_WIDTH_TICKET_AMOUNT_ISSUE
      End If

      ' Ticket amount currency terminal
      .Column(GRID_COLUMN_TICKET_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2269)
      .Column(GRID_COLUMN_TICKET_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_TICKET_AMOUNT).SystemType = Type.GetType("System.Decimal")
      ' DHA 13-JAN-2016: added fields for dual currency
      If WSI.Common.Misc.IsFloorDualCurrencyEnabled() Then
        .Column(GRID_COLUMN_TICKET_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2260)
        .Column(GRID_COLUMN_TICKET_AMOUNT).Width = GRID_WIDTH_TICKET_AMOUNT_DUAL_CUR
      Else
        .Column(GRID_COLUMN_TICKET_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2260)
        .Column(GRID_COLUMN_TICKET_AMOUNT).Width = GRID_WIDTH_TICKET_AMOUNT
      End If

      ' Balanced
      .Column(GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Header(0).Text = "" 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(2274)
      .Column(GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2278)
      If Not m_show_expected_data OrElse (m_gp_note_collection_type <> Cage.NoteAcCollectionType.ShowAll _
                             AndAlso Me.m_collection_from_tito_terminal _
                                          AndAlso Not Me.m_cage_cls.m_type_new_collection.already_collected) _
                                   OrElse Me.m_cage_cls.OperationType = Cage.CageOperationType.FromGamingTableDropbox Then

        .Column(GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Width = 0

        ' Hide status legend
        gb_status1.Visible = False
        ef_num_of_tickets.Top = gb_status1.Top
      Else
        ' DHA 13-JAN-2016: dual currency for tito tickets
        If WSI.Common.Misc.IsFloorDualCurrencyEnabled() Then
          .Column(GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Width = GRID_WIDTH_BILL_REAL_VS_THEO_DIFF_DUAL_CUR
        Else
          .Column(GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Width = GRID_WIDTH_BILL_REAL_VS_THEO_DIFF
        End If
      End If
      .Column(GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).SystemType = Type.GetType("System.Int32")

      'Validation Type
      .Column(GRID_COLUMN_TICKET_NUMBER_ORIGINAL_FORMAT).Width = GRID_WIDTH_TICKET_VALIDATION_TYPE

      ' Ticket Id
      .Column(GRID_COLUMN_TICKET_ID).Width = GRID_WIDTH_TICKET_ID

      'Ticket Type
      .Column(GRID_COLUMN_TICKET_TYPE).Width = GRID_WIDTH_TICKET_TYPE

      'Ticket Status
      .Column(GRID_COLUMN_TICKET_STATUS).Width = GRID_WIDTH_TICKET_STATUS

      'Ticket belong to the collection or not
      .Column(GRID_COLUMN_TICKET_BELONG).Width = GRID_WIDTH_TICKET_BELONG

      'Ticket belong to the collection or not
      .Column(GRID_COLUMN_TICKET_COLLECTED_MONEY_COLLECTION).Width = 0
      .Column(GRID_COLUMN_TICKET_CAGE_MOVEMENT_ID).Width = 0

      .Column(GRID_COLUMN_TICKET_ISO_CODE).Width = 0

      .Column(GRID_COLUMN_TICKET_ISO_CURRENCY_TYPE).Width = 0

    End With

  End Sub ' GUI_StyleSheetTicket

  ' PURPOSE: Init form in edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '       - StackerId
  '       - CashierSessionId
  '       - MoneyCollectionId
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overloads Sub ShowEditItem(ByVal StackerId As Int64 _
                                  , ByVal MoneyCollectionId As Int64 _
                                  , ByVal IsTicketCollection As Boolean _
                                  , Optional ByVal CageSessionId As Integer = -1 _
                                  , Optional ByVal CollectionFromTITOTerminal As Boolean = False _
                                  , Optional ByVal OpenMode As Int16 = CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT _
                                  , Optional ByVal CashierSessionId As Integer = -1 _
                                  , Optional ByVal GuiUserType As GU_USER_TYPE = GU_USER_TYPE.NOT_ASSIGNED _
                                  , Optional ByVal PrintEnabled As Boolean = False)

    Me.m_print_enabled = PrintEnabled
    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
    DbObjectId = MoneyCollectionId
    DbStatus = ENUM_STATUS.STATUS_OK

    Me.m_initial_load = True
    Me.m_new_collecion_mode = IsTicketCollection
    Me.m_is_tito_collection = True
    m_collection_from_tito_terminal = CollectionFromTITOTerminal

    If CageSessionId <> -1 Then
      Me.m_cage_cls.CageSessionId = CageSessionId
    End If

    Me.m_cage_cls.m_type_new_collection.money_collection_id = MoneyCollectionId

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)

    Me.m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal
    Me.m_show_denominations = Cage.ShowDenominationsMode.ShowAllDenominations

    If OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_FROM_PROMOBOX_OR_ACCEPTOR OrElse _
       OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT_FROM_PROMOBOX_OR_ACCEPTOR Then
      Me.m_cage_cls = DbEditedObject
      Me.m_cage_cls.IsPromoBoxOrAcceptor = True
      Me.m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal

      If OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT_FROM_PROMOBOX_OR_ACCEPTOR Then
        OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT
      Else
        OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT
      End If

      Me.m_cage_cls.CashierSessionId = CashierSessionId
      Me.m_cage_cls.GuiUserType = GuiUserType
      'DbEditedObject = Me.m_cage_cls
    End If

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)

    Me.m_cage_cls = Me.DbEditedObject
    Me.m_cage_cls.m_type_new_collection.stacker_id = StackerId
    Me.m_cage_cls.OpenMode = OpenMode

    If CageSessionId <> -1 Then
      Me.m_cage_cls.CageSessionId = CageSessionId
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus <> ENUM_STATUS.STATUS_OK Then

      Return
    End If

    Call Me.Display(True)
  End Sub ' ShowEditItem

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - CollectionDetail
  '           - CurrencyIsoCode
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS : 
  '     - True (the row should be added) or False (the row can not be added)
  '
  '   NOTES :
  '
  Public Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal CollectionDetail As NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS _
                                                        , ByVal CurrencyIsoCode As String _
                                                        , ByVal CurrentMoneyCollection As TYPE_NEW_COLLECTION) As Boolean
    Dim _idxBackgroundColor As Int16
    Dim _idxForeColor As Int16
    Dim _rows_selected As DataRow()
    Dim _ticket_collection_enabled As Boolean
    Dim _gp_name As String
    Dim _param_array(17) As Object
    Dim _dataview As DataView
    Dim _sort As Boolean
    Dim _automatic_data_fill As Boolean
    Dim _row As DataRow
    Dim _ticket_detail As NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS
    Dim _str_filter As String
    Dim _expected_column_expression As String

    _ticket_detail = New NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS()
    _gp_name = ""
    _str_filter = ""

    _rows_selected = Nothing

    'filling theoretical ticket values and adding those to the theoretical list of tickets

    ' FJC 08-08-2016
    _ticket_detail.ticket_validation_number = CollectionDetail.ticket_validation_number
    _ticket_detail.ticket_validation_type = CollectionDetail.ticket_validation_type
    ' FJC 08-08-2016

    _ticket_detail.ticket_amount = CollectionDetail.ticket_amount
    _ticket_detail.ticket_id = CollectionDetail.ticket_id
    _ticket_detail.ticket_type = CollectionDetail.ticket_type
    m_list_of_theoretical_tickets.Add(_ticket_detail)

    _ticket_collection_enabled = IIf((m_cage_cls.OperationType = Cage.CageOperationType.FromCashier Or m_cage_cls.OperationType = Cage.CageOperationType.FromCashierClosing), _
                                    GeneralParam.GetBoolean("TITO", "Cashier.TicketsCollection"), _
                                    GeneralParam.GetBoolean("TITO", "TicketsCollection"))

    _automatic_data_fill = GeneralParam.GetBoolean("Cage", "Collection.SuggestDenominationAmounts", False) _
                            And (Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MOVEMENT _
                              Or Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_FROM_PROMOBOX_OR_ACCEPTOR _
                                Or Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT _
                              Or Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MASSIVE) _
                         AndAlso Me.m_show_expected_data


    If CollectionDetail.ticket_validation_number > 0 AndAlso (Not _ticket_collection_enabled OrElse (m_gp_note_collection_type = Cage.NoteAcCollectionType.ShowAll) _
                                                     OrElse Not Me.m_new_collecion_mode) Then
      dg_collection_tickets.AddRow()

      'setting the validation number to the cell
      Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_NUMBER_CORRECT_FORMAT).Value = ValidationNumberManager.FormatValidationNumber(CollectionDetail.ticket_validation_number, CollectionDetail.ticket_validation_type, False)
      Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_NUMBER_ORIGINAL_FORMAT).Value = CollectionDetail.ticket_validation_number

      ' DHA 13-JAN-2016: set ticket amount converted to currency machine
      If WSI.Common.Misc.IsFloorDualCurrencyEnabled() And Me.m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal Then
        Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_AMOUNT).Value = Currency.Format(CollectionDetail.ticket_amount, Me.m_cage_cls.TerminalIsoCode)
      ElseIf WSI.Common.Misc.IsFloorDualCurrencyEnabled() Then
        Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_AMOUNT).Value = Currency.Format(CollectionDetail.ticket_amount, CurrencyIsoCode)
      Else
        Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_AMOUNT).Value = GUI_FormatCurrency(CollectionDetail.ticket_amount)
      End If

      ' DHA 13-JAN-2016: set issue amount TITO tickets
      If WSI.Common.Misc.IsFloorDualCurrencyEnabled() Then
        Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_AMOUNT_ISSUE).Value = Currency.Format(CollectionDetail.ticket_amount_issue, CollectionDetail.ticket_currency_issue)
        Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_ISO_CODE).Value = CollectionDetail.ticket_currency_issue
      End If

      Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_ID).Value = CollectionDetail.ticket_id
      Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_TYPE).Value = CollectionDetail.ticket_type
      Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_STATUS).Value = CollectionDetail.ticket_status
      Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_CAGE_MOVEMENT_ID).Value = CollectionDetail.ticket_cage_movement_id
      Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_COLLECTED_MONEY_COLLECTION).Value = CollectionDetail.ticketd_collected_money_collection

      'the ticket collection could be taken as they are all collected, so they will appear as collected since the begining of the collection
      'the difference column will be in blank
      If m_collection_from_tito_terminal Then
        _gp_name = "TicketsCollection"
      Else
        _gp_name = "Cashier.TicketsCollection"
      End If

      If (WSI.Common.GeneralParam.GetInt32("TITO", _gp_name) = 0 _
                                    AndAlso Not _ticket_collection_enabled _
                                    AndAlso Me.m_new_collecion_mode) _
                                    OrElse (CollectionDetail.money_collection_id = CurrentMoneyCollection.money_collection_id _
                                    AndAlso CollectionDetail.ticketd_collected_money_collection = CurrentMoneyCollection.money_collection_id) Then

        _idxBackgroundColor = COLOR_NO_DIFFERENCE
        _idxForeColor = COLOR_NO_DIFFERENCE
        Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_BELONG).Value = True
      Else
        'LEM 17-FEB-2014: if it is a collected ticket, shows 1 otherwise -1
        If CollectionDetail.ticketd_collected_money_collection = CurrentMoneyCollection.money_collection_id Then
          Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Value = 1
          Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_BELONG).Value = False
        Else
          Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Value = -1
          Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_BELONG).Value = True

        End If
        'Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Value = IIf(CollectionDetail.ticketd_collected_money_collection = CurrentMoneyCollection.money_collection_id, 1, -1)

        _idxBackgroundColor = COLOR_NEGATIVE_OR_POSITIVE_DIFFERENCE
        _idxForeColor = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00

        Me.dg_collection_tickets.Counter(COUNTER_WARNING).Value += 1
      End If

      Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).BackColor = GetColor(_idxBackgroundColor)
      Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).ForeColor = GetColor(_idxForeColor)
    End If

    If Me.m_cage_cls.OperationType <> Cage.CageOperationType.FromCashier And Me.m_cage_cls.OperationType <> Cage.CageOperationType.FromCashierClosing Then

      If CollectionDetail.bill_denomination <> 0 Then
        If CInt(CollectionDetail.currency_type) = CageCurrencyType.Bill Then
          _str_filter = String.Format("ISO_CODE = '{0}' AND DENOMINATION = {1} AND TYPE IN ({2}, {3}) ", CurrencyIsoCode, GUI_LocalNumberToDBNumber(CollectionDetail.bill_denomination.ToString()), CInt(CollectionDetail.currency_type), CInt(CageCurrencyType.Coin))
        Else
          _str_filter = String.Format("ISO_CODE = '{0}' AND DENOMINATION = {1} AND TYPE = {2} ", CurrencyIsoCode, GUI_LocalNumberToDBNumber(CollectionDetail.bill_denomination.ToString()), CInt(CollectionDetail.currency_type))
        End If

        _rows_selected = Me.m_cage_cls.TableCurrencies.Select(_str_filter)

        If _rows_selected.Length > 0 Then
          If Me.m_cage_cls.OpenMode <> CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT Then
            If Not Me.m_new_collecion_mode Then
              If CollectionDetail.bill_denomination > 0 Then
                _rows_selected(0).Item("TOTAL") = CollectionDetail.bill_real_count * CollectionDetail.bill_denomination
                _rows_selected(0).Item("QUANTITY") = CollectionDetail.bill_real_count
              ElseIf CollectionDetail.bill_denomination < 0 AndAlso CollectionDetail.bill_denomination <> Cage.TICKETS_CODE Then 'cardmoney, check or coins
                _rows_selected(0).Item("TOTAL") = CollectionDetail.bill_real_count
                _rows_selected(0).Item("QUANTITY") = 1
              ElseIf CollectionDetail.bill_denomination = Cage.TICKETS_CODE Then 'tickets has -200 as demonitation in database
                _rows_selected(0).Item("QUANTITY") = CollectionDetail.bill_real_count
              End If
            End If
            _rows_selected(0).Item("CASHIER_QUANTITY") = Me.m_cage_cls.GetRowValue(CollectionDetail.bill_theoretical_count)
            If CollectionDetail.bill_denomination > 0 Then
              _rows_selected(0).Item("CASHIER_TOTAL") = Me.m_cage_cls.GetRowValue(_rows_selected(0).Item("CASHIER_QUANTITY") * CollectionDetail.bill_denomination)
            End If
          Else
            If CollectionDetail.bill_denomination = Cage.COINS_COLLECTION_CODE Then
              _rows_selected(0).Item("TOTAL") = CollectionDetail.bill_real_count
              _rows_selected(0).Item("QUANTITY") = 1
              _rows_selected(0).Item("CASHIER_QUANTITY") = 1
              _rows_selected(0).Item("CASHIER_TOTAL") = Me.m_cage_cls.GetRowValue(CollectionDetail.bill_theoretical_count)
            ElseIf CollectionDetail.bill_denomination <> Cage.COINS_COLLECTION_CODE AndAlso CollectionDetail.bill_denomination <> Cage.TICKETS_CODE Then
              _rows_selected(0).Item("TOTAL") = CollectionDetail.bill_real_count * CollectionDetail.bill_denomination
              _rows_selected(0).Item("QUANTITY") = CollectionDetail.bill_real_count
              If CollectionDetail.currency_type = CageCurrencyType.Bill Then
                _rows_selected(0).Item("CASHIER_QUANTITY") = Me.m_cage_cls.GetRowValue(CollectionDetail.bill_theoretical_count)
                If CollectionDetail.bill_denomination > 0 Then
                  _rows_selected(0).Item("CASHIER_TOTAL") = Me.m_cage_cls.GetRowValue(_rows_selected(0).Item("CASHIER_QUANTITY") * CollectionDetail.bill_denomination)
                End If
              End If
            ElseIf CollectionDetail.bill_denomination = Cage.TICKETS_CODE Then
              _rows_selected(0).Item("QUANTITY") = CollectionDetail.bill_real_count 'tickets has -200 as demonitation in database
            End If
          End If
          ' automatic terminal data fill.
          If _automatic_data_fill And Me.m_new_collecion_mode Then
            _rows_selected(0).Item("QUANTITY") = Me.m_cage_cls.GetRowValue(_rows_selected(0).Item("CASHIER_QUANTITY"))
            _rows_selected(0).Item("TOTAL") = Me.m_cage_cls.GetRowValue(_rows_selected(0).Item("CASHIER_TOTAL"))
          End If

        Else ' The denomination doesn't exist in cage denomination
          _param_array(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.ISO_CODE) = CurrencyIsoCode
          _param_array(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.DENOMINATION) = CollectionDetail.bill_denomination
          _param_array(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CURRENCY_TYPE) = CInt(CollectionDetail.currency_type)
          _param_array(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.DENOMINATION_WITH_SIMBOL) = CollectionDetail.bill_denomination
          ' 30-MAR-2016 DDS. La columna expression EXPECTED_COLUMN espera un NULL (o denominaciones negativas) para calcular
          If CollectionDetail.bill_real_count > 0 Then
            _param_array(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.QUANTITY) = CollectionDetail.bill_real_count
            _param_array(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.TOTAL) = CollectionDetail.bill_real_count * CollectionDetail.bill_denomination
          End If
          _param_array(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.TOTAL_WITH_SIMBOL) = 0
          _param_array(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CASHIER_QUANTITY) = CollectionDetail.bill_theoretical_count
          _param_array(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CASHIER_TOTAL) = CollectionDetail.bill_theoretical_count * CollectionDetail.bill_denomination
          _param_array(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.QUANTITY_DIFFERENCE) = 0
          _param_array(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.TOTAL_DIFFERENCE) = 0
          Me.m_cage_cls.TableCurrencies.Rows.Add(_param_array)
          _sort = True
        End If

        If _sort Then
          ' 30-MAR-2016 DDS. ToTable() pierde la propiedad Expression de la columna
          _expected_column_expression = Me.m_cage_cls.TableCurrencies.Columns(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.EXPECTED_COLUMN).Expression
          _dataview = Me.m_cage_cls.TableCurrencies.DefaultView
          _dataview.Sort = "ISO_CODE, TYPE ASC, DENOMINATION DESC"
          Me.m_cage_cls.TableCurrencies = _dataview.ToTable()
          Me.m_cage_cls.TableCurrencies.Columns(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.EXPECTED_COLUMN).Expression = _expected_column_expression
        End If

      ElseIf CollectionDetail.ticket_id > 0 Then
        _str_filter = String.Format("ISO_CODE = '{0}' AND DENOMINATION = {1} ", CurrencyIsoCode, Cage.TICKETS_CODE)
        _rows_selected = Me.m_cage_cls.TableCurrencies.Select(_str_filter)
        _rows_selected(0).Item("CASHIER_QUANTITY") += 1
        If _rows_selected(0).Item("QUANTITY") Is DBNull.Value Then
          _rows_selected(0).Item("QUANTITY") = 0
        End If
        If (Not _ticket_collection_enabled _
                                           AndAlso Me.m_new_collecion_mode) _
                                           OrElse (CollectionDetail.money_collection_id = CurrentMoneyCollection.money_collection_id _
                                           AndAlso CollectionDetail.ticketd_collected_money_collection = CurrentMoneyCollection.money_collection_id) Then
          _rows_selected(0).Item("QUANTITY") += 1 'JCA tickets + 1
        End If
      ElseIf CollectionDetail.coin_total_from_terminals > 0 Then
        _str_filter = String.Format("ISO_CODE = '{0}' AND DENOMINATION = {1} ", CurrencyIsoCode, Cage.COINS_COLLECTION_CODE)
        _rows_selected = Me.m_cage_cls.TableCurrencies.Select(_str_filter)
        ' 05-NOV-2015 JPJ Fixed Bug TFS-6228: Exception while canceling
        If _rows_selected.Length > 0 Then
          _rows_selected(0).Item("CASHIER_QUANTITY") = 1
          _rows_selected(0).Item("CASHIER_TOTAL") = CollectionDetail.coin_total_from_terminals
        End If
      End If
    End If

    ' Delete zeros from datagrid
    For Each _row In Me.m_cage_cls.TableCurrencies.Rows
      Call Me.m_cage_cls.CheckForZero(_row)
    Next

    ' Dispose
    Call SafeDispose(_rows_selected)
    Call SafeDispose(_ticket_detail)

    Return True
  End Function ' GUI_SetupRow

  Private Sub SetupDataGrid(ByVal MoneyCollection As TYPE_NEW_COLLECTION)
    Dim _idx As Int32
    Dim _currency_iso_code As String
    Dim _currency_iso_type As Int32
    Dim _total_ticket_theoretical_sum As Double
    Dim _total_ticket_real_sum As Double
    Dim _gp_name As String

    _idx = 0
    _currency_iso_code = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")
    _currency_iso_type = CageCurrencyType.Bill

    ' DHA 13-JAN-16:set terminal currency
    If WSI.Common.Misc.IsFloorDualCurrencyEnabled() And Me.m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal And Not String.IsNullOrEmpty(Me.m_cage_cls.TerminalIsoCode) Then
      _currency_iso_code = Me.m_cage_cls.TerminalIsoCode
    End If

    m_list_of_theoretical_tickets = New List(Of NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS)
    m_list_money_collection = New List(Of NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS)

    If MoneyCollection.collection_details.Count > 0 Then

      m_list_money_collection = MoneyCollection.collection_details

      For Each _detail As TYPE_MONEY_COLLECTION_DETAILS In MoneyCollection.collection_details



        If m_show_expected_data OrElse _detail.ticket_id > 0 OrElse Me.m_cage_cls.OpenMode <> CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MASSIVE Then

          Call GUI_SetupRow(_idx, _detail, _currency_iso_code, MoneyCollection)

          _idx = _idx + 1
          If _detail.money_collection_id = MoneyCollection.money_collection_id Then
            _total_ticket_theoretical_sum += _detail.ticket_amount
          End If


          If m_collection_from_tito_terminal Then
            _gp_name = "TicketsCollection"
          Else
            _gp_name = "Cashier.TicketsCollection"
          End If

          ' the first condition checks if the ticket has been collected or not   '_detail.ticketd_collected_money_collection = _detail.money_collection_id AndAlso 
          If (_detail.ticketd_collected_money_collection = MoneyCollection.money_collection_id) _
                                              OrElse ((WSI.Common.GeneralParam.GetInt32("TITO", _gp_name) = 0 _
                                              AndAlso _detail.ticket_validation_number > 0) _
                                              AndAlso Me.m_new_collecion_mode) Then

            _total_ticket_real_sum += _detail.ticket_amount
          End If
        End If

      Next
    End If

    Call Me.UpdateTotalGridTheoreticalTicketsRow(_total_ticket_theoretical_sum)
    Call Me.UpdateTotalGridTicketsRow(_total_ticket_real_sum) 'Me.m_cage_cls.m_type_new_collection.real_ticket_sum)

    Me.SetCurrencyGridData(_currency_iso_code, _currency_iso_type, Me.GetLocalCurrencyDatagrid(_currency_iso_code, _currency_iso_type))

  End Sub 'SetupDataGrid

  Private Sub IncrementCurrencyTabTicketCount(ByVal Increment As Int32)
    Dim _grid As uc_grid
    Dim _iso_code As String
    Dim _currency_type As CageCurrencyType
    Dim _select_tab As TabPage
    Dim _count As Int32
    Try
      _iso_code = ""
      _currency_type = CageCurrencyType.Others
      _select_tab = tab_ticket
      _grid = Me.GetLocalCurrencyDatagrid(_iso_code, 0, _select_tab)

      For _idx As Int32 = 0 To _grid.NumRows - 1
        If _grid.Cell(_idx, GRID_CURRENCY_COLUMN_DENOMINATION).Value = Cage.TICKETS_CODE Then

          _count = Me.m_cage_cls.GetRowValue(_grid.Cell(_idx, GRID_CURRENCY_COLUMN_QUANTITY).Value)
          _grid.Cell(_idx, GRID_CURRENCY_COLUMN_QUANTITY).Value = _count + Increment
          ef_num_of_tickets.Value = (_count + Increment).ToString()

          Call Me.SaveCurrenciesQuantities(_idx, _iso_code, _currency_type, _grid)
          Call Me.SetDifference(_grid, _idx, _iso_code, _currency_type)
          Call Me.SetRowColor(_idx, _grid)

          Exit For
        End If
      Next
    Catch ex As Exception
    End Try

  End Sub

  Private Sub AddTicketToCollection(ByVal ValidationNumber As String, Optional ByVal TicketValue As Decimal = 0, Optional ByVal ReadByScanner As Boolean = False, Optional ByVal TicketId As Int64 = 0)

    Dim _row As Int32
    Dim _ticket_value As Double
    Dim _ticket_value_issue As Double
    Dim _ticket_curr_issue As String
    Dim _ticket_id As Int64
    Dim _validation_number As Int64
    Dim _table_ticket As DataTable
    Dim _money_collection_id As Int64
    Dim _ticket_type As Int32
    Dim _ticket_index As Int32
    Dim _ticket_last_datetime As Date
    Dim _ticket_status As TITO_TICKET_STATUS
    Dim _frm_select_ticket As frm_TITO_select_ticket
    Dim _source_name As String
    Dim _difference_value As String
    Dim _error_description As String
    Dim _ticket_increment As Integer
    Dim _new_ticket_diff As String
    Dim _color_selector As Int32
    Dim _allow_pay_tickets_pending_print As Boolean


    Const SQL_TICKET_AMOUNT As Int16 = 0
    Const SQL_TICKET_ID As Int16 = 1
    Const SQL_TICKET_REAL_COLLECTION_ID As Int16 = 2
    Const SQL_TICKET_COLLECTION_ID As Int16 = 3
    Const SQL_TICKET_TYPE As Int16 = 4
    Const SQL_TICKET_LAST_ACTION_DATETIME As Int16 = 5
    Const SQL_TICKET_STATUS As Int16 = 6
    Const SQL_TICKET_AMOUNT_ISSUE As Int16 = 7
    Const SQL_TICKET_CURR_ISSUE As Int16 = 8


    _validation_number = 0
    _ticket_id = 0
    _ticket_value = 0
    _ticket_value_issue = 0
    _ticket_index = 0
    _money_collection_id = 0
    _difference_value = "1"
    _new_ticket_diff = ""
    _ticket_curr_issue = CurrencyExchange.GetNationalCurrency()
    _allow_pay_tickets_pending_print = GeneralParam.GetBoolean("Site", "MultiSiteMember", False)


    _source_name = GLB_CurrentUser.Name & "@" & Environment.MachineName
    _table_ticket = TicketAmount(ValidationNumber, TicketId)

    ' Avoid to show message with empty validation number
    If String.IsNullOrEmpty(ValidationNumber) Then
      Return
    End If

    Select Case (_table_ticket.Rows.Count)
      Case 0                                                                                 ' ticket is not valid 
        ' el ticket no existe
        If ReadByScanner Then
          _error_description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5152, ValidationNumber)
          Call SetErrorToLabel(_error_description)
          Return
        End If
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2280), _
                                          mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
                                          ENUM_MB_BTN.MB_BTN_OK, , ValidationNumber)
        ef_ticket.TextValue = ""
        Return

      Case 1
        If _table_ticket.Rows(0).Item(SQL_TICKET_REAL_COLLECTION_ID) Is DBNull.Value Then     'ticket is not collected
          If _table_ticket.Rows(0).Item(SQL_TICKET_AMOUNT) IsNot DBNull.Value Then
            _ticket_value = _table_ticket.Rows(0).Item(SQL_TICKET_AMOUNT)                     'get the ticket value
            _ticket_value_issue = _ticket_value
          End If

          ' DHA 13-JAN-2016: set values for issue ticket amount with iso code
          If WSI.Common.Misc.IsFloorDualCurrencyEnabled() Then
            If _table_ticket.Rows(0).Item(SQL_TICKET_AMOUNT_ISSUE) IsNot DBNull.Value And _table_ticket.Rows(0).Item(SQL_TICKET_CURR_ISSUE) IsNot DBNull.Value Then
              _ticket_value_issue = _table_ticket.Rows(0).Item(SQL_TICKET_AMOUNT_ISSUE)
              _ticket_curr_issue = _table_ticket.Rows(0).Item(SQL_TICKET_CURR_ISSUE)
            End If
          End If

          If _table_ticket.Rows(0).Item(SQL_TICKET_COLLECTION_ID) IsNot DBNull.Value Then
            _money_collection_id = _table_ticket.Rows(0).Item(SQL_TICKET_COLLECTION_ID)
          End If
          If _table_ticket.Rows(0).Item(SQL_TICKET_ID) IsNot DBNull.Value Then
            _ticket_id = _table_ticket.Rows(0).Item(SQL_TICKET_ID)
          End If
          If _table_ticket.Rows(0).Item(SQL_TICKET_TYPE) IsNot DBNull.Value Then
            _ticket_type = _table_ticket.Rows(0).Item(SQL_TICKET_TYPE)
          End If
          If _table_ticket.Rows(0).Item(SQL_TICKET_LAST_ACTION_DATETIME) IsNot DBNull.Value Then
            _ticket_last_datetime = _table_ticket.Rows(0).Item(SQL_TICKET_LAST_ACTION_DATETIME)
          End If
          If _table_ticket.Rows(0).Item(SQL_TICKET_STATUS) IsNot DBNull.Value Then
            _ticket_status = _table_ticket.Rows(0).Item(SQL_TICKET_STATUS)
            If (_allow_pay_tickets_pending_print) Then
              If (_ticket_status = TITO_TICKET_STATUS.VALID Or _ticket_status = TITO_TICKET_STATUS.PENDING_PRINT) Then
                _error_description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5328, ValidationNumber)
                Call SetErrorToLabel(_error_description)
                'Call Alarm.Register(AlarmSourceCode.User, GLB_CurrentUser.Id, _source_name, AlarmCode.Reder_Invalid_Ticket, _error_description, AlarmSeverity.Warning)
              End If
            Else
              If _ticket_status = TITO_TICKET_STATUS.VALID Then
                _error_description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5328, ValidationNumber)
                Call SetErrorToLabel(_error_description)
                'Call Alarm.Register(AlarmSourceCode.User, GLB_CurrentUser.Id, _source_name, AlarmCode.Reder_Invalid_Ticket, _error_description, AlarmSeverity.Warning)
              End If
            End If
          End If

          If Int64.TryParse(ValidationNumber, _validation_number) Then
            _row = SearchRowOfTicket(_validation_number, GRID_COLUMN_TICKET_NUMBER_ORIGINAL_FORMAT, TicketId)
          End If

        Else
          ' el ticket ya ha sido inser
          If ReadByScanner Then
            ' generar alarma de ticket ya recaudado y si el label de error esta en blanco poner el error
            'Call Alarm.Register(AlarmSourceCode.User, GLB_CurrentUser.Id, _source_name, AlarmCode.Reder_Invalid_Ticket, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5153, ValidationNumber), AlarmSeverity.Warning)

            Return
          End If

          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4435), _
                                            mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
                                            ENUM_MB_BTN.MB_BTN_OK, , ValidationNumber)
          ef_ticket.TextValue = ""

          Return
        End If
        ' there are more than one ticket with the same validation number
      Case Is > 1
        'if ticket has been read by the scanner and there are more than one ticket with the same number,
        ' in the grid, will appear just one ticket with that validation number and in the difference column will be insert an "?" simbol
        If ReadByScanner Then
          _difference_value = "?"
          'Call Alarm.Register(AlarmSourceCode.User, GLB_CurrentUser.Id, _source_name, AlarmCode.Reder_Invalid_Ticket, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5155, ValidationNumber), AlarmSeverity.Warning)

          If Int64.TryParse(ValidationNumber, _validation_number) Then
            _row = SearchRowOfTicket(_validation_number, GRID_COLUMN_TICKET_NUMBER_ORIGINAL_FORMAT, TicketId)
          End If

        Else
          _frm_select_ticket = New frm_TITO_select_ticket()
          If _frm_select_ticket.ShowForSelect(Me.MdiParent, ValidationNumber) = DialogResult.Cancel Then 'the selection was canceled
            ef_ticket.TextValue = ""
            Return
          Else
            _ticket_id = _frm_select_ticket.SelectedTicketId
            'HBB 07-OCT-2014: Duplicated tickets not found in the grid
            _validation_number = ValidationNumber

          End If

          Call SafeDispose(_frm_select_ticket)

          If _ticket_id > 0 Then
            _row = SearchRowOfTicket(_validation_number, GRID_COLUMN_TICKET_ID, _ticket_id)

            'ticket with repeated validation number is already collected
            If _table_ticket.Rows(_frm_select_ticket.SelectedTicketIndex).Item(SQL_TICKET_REAL_COLLECTION_ID) IsNot DBNull.Value Then
              ' ticket repetido ya esta recaudado
              NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4435), _
                                                          mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
                                                                            ENUM_MB_BTN.MB_BTN_OK, , ValidationNumber)
              ef_ticket.TextValue = ""

              Return
            End If
            _ticket_value = _frm_select_ticket.SelectedTicketAmount
            _ticket_id = _frm_select_ticket.SelectedTicketId
            _ticket_type = _frm_select_ticket.SelectedTicketType
            _ticket_index = _frm_select_ticket.SelectedTicketIndex

            If _table_ticket.Rows(_ticket_index).Item(SQL_TICKET_COLLECTION_ID) IsNot DBNull.Value Then
              _money_collection_id = _table_ticket.Rows(_ticket_index).Item(SQL_TICKET_COLLECTION_ID)
            End If
            If _table_ticket.Rows(_ticket_index).Item(SQL_TICKET_LAST_ACTION_DATETIME) IsNot DBNull.Value Then
              _ticket_last_datetime = _table_ticket.Rows(_ticket_index).Item(SQL_TICKET_LAST_ACTION_DATETIME)
            End If
            If _table_ticket.Rows(_ticket_index).Item(SQL_TICKET_STATUS) IsNot DBNull.Value Then
              _ticket_status = _table_ticket.Rows(_ticket_index).Item(SQL_TICKET_STATUS)
              If (_allow_pay_tickets_pending_print) Then
                If (_ticket_status = TITO_TICKET_STATUS.VALID Or _ticket_status = TITO_TICKET_STATUS.PENDING_PRINT) Then
                  _error_description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5328, ValidationNumber)
                  Call SetErrorToLabel(_error_description)
                  'Call Alarm.Register(AlarmSourceCode.User, GLB_CurrentUser.Id, _source_name, AlarmCode.Reder_Invalid_Ticket, _error_description, AlarmSeverity.Warning)
                End If
              Else
                If _ticket_status = TITO_TICKET_STATUS.VALID Then
                  _error_description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5328, ValidationNumber)
                  Call SetErrorToLabel(_error_description)
                  'Call Alarm.Register(AlarmSourceCode.User, GLB_CurrentUser.Id, _source_name, AlarmCode.Reder_Invalid_Ticket, _error_description, AlarmSeverity.Warning)
                End If
              End If
            End If
          End If
        End If
    End Select

    _ticket_increment = 0
    'if row = -1 it means that there is no theoretical ticket with that number in the grid
    If _row >= 0 Then
      ' partial collection
      If _money_collection_id = m_cage_cls.m_type_new_collection.money_collection_id _
        AndAlso m_cage_cls.m_is_fill_out_movement _
        AndAlso m_cage_cls.MovementDatetime < _ticket_last_datetime Then

        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4724), _
                   mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
                   ENUM_MB_BTN.MB_BTN_OK, , ValidationNumber)
        ef_ticket.TextValue = ""

        Return
      End If

      Select Case dg_collection_tickets.Cell(_row, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Value
        Case "-1"
          If _table_ticket.Rows.Count > 1 AndAlso ReadByScanner Then ' More than one ticket with the same Validation Number, user interaction required
            _new_ticket_diff = "?"
            _ticket_increment = 0
            _color_selector = -2
          Else
            _new_ticket_diff = ""
            _ticket_increment = 1
            _color_selector = 0
          End If
        Case ""
          If Not ReadByScanner Then
            _new_ticket_diff = "-1"
            _ticket_increment = -1
            _color_selector = -1
          End If
        Case "1"
          If Not ReadByScanner Then
            _new_ticket_diff = ""
            _ticket_increment = -1
            _color_selector = 0
          Else
            Return
          End If
        Case "?"
          If Me.dg_collection_tickets.Cell(_row, GRID_COLUMN_TICKET_BELONG).Value Then

            If Not ReadByScanner Then
              _new_ticket_diff = ""
              _color_selector = 0
              Me.m_cage_cls.m_type_new_collection.real_ticket_count += 1
              Me.m_cage_cls.m_type_new_collection.real_ticket_sum += CDbl(_ticket_value)
              Call Me.UpdateTotalGridTicketsRow(_ticket_value)
              'Call Me.UpdateTotalGridTicketsRow(Me.dg_collection_tickets.Cell(_row, GRID_COLUMN_TICKET_AMOUNT).Value)
              Call Me.UpdateDifferenceAndColor(_row, _color_selector, False)
              Call IncrementCurrencyTabTicketCount(1)
            Else
              _new_ticket_diff = "?"
              _color_selector = -2
            End If
          Else  'Code to not belong
            If Not ReadByScanner Then
              _new_ticket_diff = "1"
              _color_selector = 1
              Me.m_cage_cls.m_type_new_collection.real_ticket_count += 1
              Me.m_cage_cls.m_type_new_collection.real_ticket_sum += CDbl(_ticket_value)
              Call Me.UpdateTotalGridTicketsRow(_ticket_value)
              Me.dg_collection_tickets.Cell(_row, GRID_COLUMN_TICKET_AMOUNT).Value = GUI_FormatCurrency(_ticket_value)
              Me.dg_collection_tickets.Cell(_row, GRID_COLUMN_TICKET_ID).Value = _ticket_id
              Call Me.UpdateDifferenceAndColor(_row, _color_selector, False)
              Call IncrementCurrencyTabTicketCount(1)

            Else
              _new_ticket_diff = "?"
              _color_selector = -2
            End If
          End If

        Case Else
          ' _new_ticket_diff = ""
      End Select

      Me.m_cage_cls.m_type_new_collection.real_ticket_count += _ticket_increment
      Me.m_cage_cls.m_type_new_collection.real_ticket_sum += CDbl(_ticket_value) * _ticket_increment

      If _ticket_value > 0 Then
        Call Me.UpdateTotalGridTicketsRow(_ticket_value * _ticket_increment)
        ' Call Me.UpdateTotalGridTicketsRow(Me.dg_collection_tickets.Cell(_row, GRID_COLUMN_TICKET_AMOUNT).Value * _ticket_increment)
      End If
      Call Me.UpdateDifferenceAndColor(_row, _color_selector, False)
      'If (_new_ticket_diff = "") Then
      '  Call Me.UpdateDifferenceAndColor(_row, 0, False)
      'Else
      '  Call Me.UpdateDifferenceAndColor(_row, 1, False)
      'End If
      Me.dg_collection_tickets.Cell(_row, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Value = _new_ticket_diff
      Call IncrementCurrencyTabTicketCount(_ticket_increment)

      If Me.dg_collection_tickets.Cell(_row, GRID_COLUMN_TICKET_BELONG).Value = False AndAlso _ticket_increment = -1 Then
        Me.dg_collection_tickets.DeleteRow(_row)
      End If
    Else
      If _table_ticket.Rows.Count > 1 AndAlso ReadByScanner Then
        _new_ticket_diff = "?"
        _color_selector = -2
      Else
        _new_ticket_diff = "1"
        _color_selector = 1
      End If
      ' Adds new row to the tickets grid.
      dg_collection_tickets.AddRow()
      Call Me.UpdateDifferenceAndColor(dg_collection_tickets.NumRows - 1, _color_selector, False)
      Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_NUMBER_CORRECT_FORMAT).Value = ValidationNumberManager.FormatValidationNumber(ValidationNumber, 1, False)
      Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_NUMBER_ORIGINAL_FORMAT).Value = ValidationNumber

      ' DHA 13-JAN-2016: set values for issue ticket amount with iso code
      If WSI.Common.Misc.IsFloorDualCurrencyEnabled() Then
        Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_AMOUNT).Value = IIf(_new_ticket_diff = "?", "", Currency.Format(_ticket_value, Me.m_cage_cls.TerminalIsoCode))
      Else
        Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_AMOUNT).Value = IIf(_new_ticket_diff = "?", "", GUI_FormatCurrency(_ticket_value))
      End If

      ' DHA 13-JAN-2016: set values for issue ticket amount with iso code
      If WSI.Common.Misc.IsFloorDualCurrencyEnabled() Then
        Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_AMOUNT_ISSUE).Value = Currency.Format(_ticket_value_issue, _ticket_curr_issue)
        Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_ISO_CODE).Value = _ticket_curr_issue
      End If

      Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_ISO_CURRENCY_TYPE).Value = CageCurrencyType.Others

      Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_ID).Value = _ticket_id
      Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Value = _new_ticket_diff
      Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_BELONG).Value = False
      Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_TYPE).Value = _ticket_type
      Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_STATUS).Value = _ticket_status
      Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_CAGE_MOVEMENT_ID).Value = 0
      Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_COLLECTED_MONEY_COLLECTION).Value = _money_collection_id

      If Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Value <> "?" Then
        Me.m_cage_cls.m_type_new_collection.real_ticket_count += 1
        Me.m_cage_cls.m_type_new_collection.real_ticket_sum += CDbl(_ticket_value)
        Call Me.UpdateTotalGridTicketsRow(_ticket_value)
        Call IncrementCurrencyTabTicketCount(1)
        CheckIfTicketBelongToCollection(ValidationNumber)
      End If

    End If

    ef_ticket.TextValue = ""

  End Sub

  Private Sub CheckIfTicketBelongToCollection(ByVal ValidatioNumber As String)

    Dim _idx As Int32

    For _idx = 0 To dg_collection_tickets.NumRows - 1
      If dg_collection_tickets.Cell(_idx, GRID_COLUMN_TICKET_NUMBER_ORIGINAL_FORMAT).Value = ValidatioNumber _
                                AndAlso Me.dg_collection_tickets.Cell(_idx, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Value = "?" _
                                AndAlso Me.dg_collection_tickets.Cell(_idx, GRID_COLUMN_TICKET_BELONG).Value Then
        Me.dg_collection_tickets.Cell(_idx, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Value = "-1"
      End If
    Next

  End Sub

  Private Sub AddCurrencyToCollection(ByVal Currency As NoteReceived, ByVal Isocode As String)
    Dim _datagrid As uc_grid
    Dim _currency_not_found As Boolean

    _datagrid = Nothing

    If Currency.Quantity <= 0 Then

      Return
    End If

    Try
      ' searching the grid to insert the notes
      For Each _tab As TabPage In Me.tb_currency.TabPages
        If _tab.Text = Isocode Then
          _datagrid = _tab.Controls.Item(0)
          Exit For
        End If
      Next

      ' _datagrid = Me.tb_currency.TabPages(0).Controls(0)
      If _datagrid IsNot Nothing Then
        _currency_not_found = True
        For i As Int32 = 0 To _datagrid.NumRows - 1
          If Convert.ToDecimal(_datagrid.Cell(i, GRID_CURRENCY_COLUMN_DENOMINATION).Value) = Currency.Denomination Then
            If (_datagrid.Cell(i, GRID_CURRENCY_COLUMN_QUANTITY).Value = "") Then
              _datagrid.Cell(i, GRID_CURRENCY_COLUMN_QUANTITY).Value = Currency.Quantity
              CellDataChangeHandler(i, GRID_CURRENCY_COLUMN_QUANTITY, True, _datagrid)
            Else
              _datagrid.Cell(i, GRID_CURRENCY_COLUMN_QUANTITY).Value = Convert.ToInt32(_datagrid.Cell(i, GRID_CURRENCY_COLUMN_QUANTITY).Value) + Currency.Quantity
              CellDataChangeHandler(i, GRID_CURRENCY_COLUMN_QUANTITY, True, _datagrid)
            End If

            Return
            _currency_not_found = False
          End If
        Next
        If _currency_not_found Then
          Call SetErrorToLabel(GLB_NLS_GUI_PLAYER_TRACKING.GetString(3031, Me.m_cage_cls.FormatAmount(Currency.Denomination.ToString(), False, Isocode)))
        End If
      Else
        Call SetErrorToLabel(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5443))
      End If

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try
  End Sub

  Private Function TicketAmount(ByVal TicketNumber As String, Optional ByVal TicketId As Int64 = 0) As DataTable
    Dim _sb As StringBuilder
    Dim _ticket_number As Int64
    Dim _table_result As DataTable

    _table_result = New DataTable()

    If TicketNumber = "" Or Not Int64.TryParse(TicketNumber, _ticket_number) Then
      Return _table_result
    End If

    _sb = New StringBuilder()

    _sb.AppendLine("   SELECT                                                  ")
    ' DHA 13-JAN-2016: get ticket amount converted to currency machine
    If WSI.Common.Misc.IsFloorDualCurrencyEnabled() And Me.m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal Then
      _sb.AppendLine("               CASE WHEN TI_AMT1 IS NULL AND @pTerminalISOCode = @pNationalCurrency                 THEN TI_AMOUNT   ")
      _sb.AppendLine("                    WHEN TI_AMT1 IS NULL AND @pTerminalISOCode <> @pNationalCurrency                THEN 0           ")
      _sb.AppendLine("                    WHEN TI_CUR1 <> @pTerminalISOCode AND @pTerminalISOCode = @pNationalCurrency    THEN TI_AMOUNT   ")
      _sb.AppendLine("                    WHEN TI_CUR1 <> @pTerminalISOCode AND @pTerminalISOCode <> @pNationalCurrency   THEN 0           ")
      _sb.AppendLine("                    ELSE TI_AMT1                                                                      ")
      _sb.AppendLine("               END AS TI_AMOUNT                                                                       ")
    Else
      _sb.AppendLine("               TI_AMOUNT                       AS TI_AMOUNT                      ")
    End If
    _sb.AppendLine("          , TI_TICKET_ID                                    ")
    _sb.AppendLine("          , TI_COLLECTED_MONEY_COLLECTION                   ")
    _sb.AppendLine("          , TI_MONEY_COLLECTION_ID                          ")
    _sb.AppendLine("          , TI_TYPE_ID                                      ")
    _sb.AppendLine("          , TI_LAST_ACTION_DATETIME                         ")
    _sb.AppendLine("          , TI_STATUS                                       ")
    ' DHA 13-JAN-2016: added fields for dual currency
    _sb.AppendLine("          , TI_AMT0                                         ")
    _sb.AppendLine("          , TI_CUR0                                         ")
    _sb.AppendLine("     FROM   TICKETS                                         ")
    _sb.AppendLine("     WHERE  TI_VALIDATION_NUMBER = @pTicketNumber   ")
    _sb.AppendLine("      AND   TI_COLLECTED_MONEY_COLLECTION IS NULL           ")
    If TicketId > 0 Then
      _sb.AppendLine("    AND   TI_TICKET_ID = @pTicketId                       ")
    End If
    _sb.AppendLine("      AND   TI_TYPE_ID <> @pChipsDealerCopy                 ")

    Try
      Using _db_trx As New DB_TRX()
        Using _sql_cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _sql_cmd.Parameters.Add("@pTicketNumber", SqlDbType.BigInt).Value = TicketNumber
          _sql_cmd.Parameters.Add("@pTicketId", SqlDbType.BigInt).Value = TicketId

          If WSI.Common.Misc.IsFloorDualCurrencyEnabled() And Me.m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal Then
            _sql_cmd.Parameters.Add("@pTerminalISOCode", SqlDbType.NVarChar).Value = Me.m_cage_cls.TerminalIsoCode
            _sql_cmd.Parameters.Add("@pNationalCurrency", SqlDbType.NVarChar).Value = CurrencyExchange.GetNationalCurrency()
          End If

          _sql_cmd.Parameters.Add("@pChipsDealerCopy", SqlDbType.Int).Value = TITO_TICKET_TYPE.CHIPS_DEALER_COPY
          Using _sql_da As New SqlDataAdapter(_sql_cmd)
            _db_trx.Fill(_sql_da, _table_result)

          End Using

        End Using
      End Using
    Catch ex As Exception
      Return _table_result
    End Try

    Return _table_result

  End Function

  Private Function SearchRowOfTicket(ByVal ValidationNumber As Int64, ByVal Column As Int32, Optional ByVal TicketId As Int64 = 0) As Int32

    Dim _idx As Int32
    Dim _number_to_compare

    _number_to_compare = 0

    If TicketId > 0 Then
      _number_to_compare = TicketId
      Column = GRID_COLUMN_TICKET_ID
    Else
      _number_to_compare = ValidationNumber
    End If

    For _idx = 0 To dg_collection_tickets.NumRows - 1
      If _number_to_compare = dg_collection_tickets.Cell(_idx, Column).Value _
                                       OrElse (dg_collection_tickets.Cell(_idx, Column).Value = 0 _
                                       AndAlso dg_collection_tickets.Cell(_idx, GRID_COLUMN_TICKET_NUMBER_ORIGINAL_FORMAT).Value = ValidationNumber) Then
        Return _idx
      End If
    Next

    Return -1

  End Function

  Private Sub UpdateDifferenceAndColor(ByVal RowIndex As Int32, ByVal NewDiff As Int32, ByVal IsBill As Boolean)

    Dim _idxBackgroundColor As Int16
    Dim _idxForeColor As Int16

    Select Case NewDiff
      Case 0
        _idxBackgroundColor = COLOR_NO_DIFFERENCE
      Case -2
        _idxBackgroundColor = COLOR_PENDING
      Case Else
        _idxBackgroundColor = COLOR_NEGATIVE_OR_POSITIVE_DIFFERENCE
    End Select

    '_idxBackgroundColor = IIf(NewDiff = 0, COLOR_NO_DIFFERENCE, COLOR_NEGATIVE_OR_POSITIVE_DIFFERENCE)
    _idxForeColor = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00


    If RowIndex >= 0 Then
      dg_collection_tickets.Cell(RowIndex, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Value = IIf(NewDiff = 0, "", NewDiff.ToString())
      dg_collection_tickets.Cell(RowIndex, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).BackColor = GetColor(_idxBackgroundColor)
      dg_collection_tickets.Cell(RowIndex, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).ForeColor = GetColor(_idxForeColor)
    End If

    'Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).BackColor = GetColor(_idxBackgroundColor)
    'Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).ForeColor = GetColor(_idxForeColor)

  End Sub

  Private Function FillDetailsList() As List(Of NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS)
    Dim _current_collection_detail As NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS
    Dim _list_of_collection_details As List(Of NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS)
    Dim _rows_selected As DataRow()
    Dim _row As DataRow
    Dim _idx As Integer
    Dim _currency_iso_code As String
    Dim _quantity As Integer
    Dim _money_collection As String
    Dim _movement_id As String
    Dim _dg_collection_tickets_value As Decimal

    _currency_iso_code = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")
    _current_collection_detail = New NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS()

    ' DHA 13-JAN-2016: set terminal currency
    If WSI.Common.Misc.IsFloorDualCurrencyEnabled() And m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal Then
      _currency_iso_code = m_cage_cls.TerminalIsoCode
    End If

    _list_of_collection_details = New List(Of NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS)()
    _rows_selected = Me.m_cage_cls.TableCurrencies.Select("ISO_CODE ='" & _currency_iso_code & "'")
    _quantity = 0

    m_cage_cls.m_type_new_collection.real_redeem_ticket_count = 0
    m_cage_cls.m_type_new_collection.real_redeem_ticket_amount = 0
    m_cage_cls.m_type_new_collection.real_non_redeem_ticket_count = 0
    m_cage_cls.m_type_new_collection.real_non_redeem_ticket_amount = 0
    m_cage_cls.m_type_new_collection.real_promo_redeem_ticket_count = 0
    m_cage_cls.m_type_new_collection.real_promo_redeem_ticket_amount = 0

    If m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal Then
      For Each _row In _rows_selected

        If Not _row.IsNull("CASHIER_QUANTITY") AndAlso _row.Item("CASHIER_QUANTITY") > 0 _
           Or Not _row.IsNull("QUANTITY") AndAlso _row.Item("QUANTITY") > 0 Then

          _current_collection_detail.bill_denomination = GUI_ParseCurrency(_row.Item("DENOMINATION"))
          _current_collection_detail.currency_type = _row.Item("TYPE")

          If _current_collection_detail.bill_denomination >= 0 OrElse _current_collection_detail.bill_denomination = Cage.TICKETS_CODE Then
            If Not _row.Item("QUANTITY") Is DBNull.Value Then
              _quantity = GUI_FormatNumber(_row.Item("QUANTITY"))
            Else
              _quantity = 0
            End If
          Else
            If Not _row.Item("TOTAL") Is DBNull.Value Then
              _quantity = GUI_FormatNumber(_row.Item("TOTAL"))
            Else
              _quantity = 1
            End If
          End If

          _current_collection_detail.bill_real_count = _quantity ' Real Count

          If Not _row.Item("CASHIER_QUANTITY") Is DBNull.Value Then
            _current_collection_detail.bill_theoretical_count = GUI_FormatNumber(_row.Item("CASHIER_QUANTITY")) ' Theoretical Count
          Else
            If m_show_expected_data Then
              _current_collection_detail.bill_theoretical_count = 0 ' Theoretical Count
            Else
              _current_collection_detail.bill_theoretical_count = _quantity  ' Theoretical Count
            End If

          End If
          _current_collection_detail.type = 2
          _current_collection_detail.matched = (_current_collection_detail.bill_real_count = _current_collection_detail.bill_theoretical_count)
          _list_of_collection_details.Add(_current_collection_detail)

        End If
      Next
    End If

    For _idx = 0 To dg_collection_tickets.NumRows - 1
      If dg_collection_tickets.Value(_idx, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF) = "?" Then
        _current_collection_detail.pending_to_especify = True
        _current_collection_detail.type = 1
        _list_of_collection_details.Add(_current_collection_detail)

      ElseIf dg_collection_tickets.Value(_idx, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF) <> "-1" Then
        _current_collection_detail = New NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS()
        _current_collection_detail.ticket_validation_number = dg_collection_tickets.Value(_idx, GRID_COLUMN_TICKET_NUMBER_ORIGINAL_FORMAT)
        _current_collection_detail.ticket_amount = Currency.Unformat(dg_collection_tickets.Value(_idx, GRID_COLUMN_TICKET_AMOUNT), m_cage_cls.TerminalIsoCode)
        _current_collection_detail.ticket_id = dg_collection_tickets.Value(_idx, GRID_COLUMN_TICKET_ID)
        _current_collection_detail.ticket_type = dg_collection_tickets.Value(_idx, GRID_COLUMN_TICKET_TYPE)
        _current_collection_detail.ticket_status = dg_collection_tickets.Value(_idx, GRID_COLUMN_TICKET_STATUS)

        _money_collection = dg_collection_tickets.Value(_idx, GRID_COLUMN_TICKET_COLLECTED_MONEY_COLLECTION)
        _current_collection_detail.ticketd_collected_money_collection = IIf(String.IsNullOrEmpty(_money_collection), 0, _money_collection)

        _movement_id = dg_collection_tickets.Value(_idx, GRID_COLUMN_TICKET_CAGE_MOVEMENT_ID)
        _current_collection_detail.ticket_cage_movement_id = IIf(String.IsNullOrEmpty(_movement_id), 0, _movement_id)
        _current_collection_detail.matched = TicketBelongToCollection(_current_collection_detail.ticket_id) '(dg_collection_tickets.Value(_idx, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF) = "")
        _current_collection_detail.type = 1
        _list_of_collection_details.Add(_current_collection_detail)


        'ATB 25-JAN-2017
        'Reading the multicurrency
        If WSI.Common.Misc.IsFloorDualCurrencyEnabled() Then
          _dg_collection_tickets_value = Currency.Unformat(dg_collection_tickets.Value(_idx, GRID_COLUMN_TICKET_AMOUNT), m_cage_cls.TerminalIsoCode)
        Else
          _dg_collection_tickets_value = GUI_ParseCurrency(dg_collection_tickets.Value(_idx, GRID_COLUMN_TICKET_AMOUNT))
        End If

        Select Case (_current_collection_detail.ticket_type) '(_current_collection_detail.ticket_type)
          Case TITO_TICKET_TYPE.OFFLINE
            If _current_collection_detail.ticket_status = TITO_TICKET_STATUS.REDEEMED Then
              m_cage_cls.m_type_new_collection.real_redeem_ticket_amount += _dg_collection_tickets_value
              m_cage_cls.m_type_new_collection.real_redeem_ticket_count += 1
            Else
              m_cage_cls.m_type_new_collection.real_non_redeem_ticket_amount += _dg_collection_tickets_value
              m_cage_cls.m_type_new_collection.real_non_redeem_ticket_count += 1
            End If
          Case TITO_TICKET_TYPE.CASHABLE _
              , TITO_TICKET_TYPE.HANDPAY _
              , TITO_TICKET_TYPE.JACKPOT
            m_cage_cls.m_type_new_collection.real_redeem_ticket_amount += _dg_collection_tickets_value
            m_cage_cls.m_type_new_collection.real_redeem_ticket_count += 1
          Case TITO_TICKET_TYPE.PROMO_REDEEM
            m_cage_cls.m_type_new_collection.real_promo_redeem_ticket_amount += _dg_collection_tickets_value
            m_cage_cls.m_type_new_collection.real_promo_redeem_ticket_count += 1
          Case TITO_TICKET_TYPE.PROMO_NONREDEEM
            m_cage_cls.m_type_new_collection.real_non_redeem_ticket_amount += _dg_collection_tickets_value
            m_cage_cls.m_type_new_collection.real_non_redeem_ticket_count += 1

        End Select
      End If
    Next

    ' Dispose
    Call SafeDispose(_rows_selected)

    Return _list_of_collection_details
  End Function

  Private Sub rb_terminal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_terminal.CheckedChanged

    m_changed_values = True

    If Me.rb_reception.Checked AndAlso Me.rb_terminal.Checked Then
      Me.m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal
    End If

    If Not Me.rb_terminal.Checked Then
      Me.m_last_checked = Me.rb_terminal
    End If

    Call Me.CheckRadioButtonsToSetLayouts()

    Me.SetConceptsGridAndTabValues()

  End Sub

  Private Sub btn_save_tickets_ClickEvent() Handles btn_save_tickets.ClickEvent
    Call Me.AddTicketToCollection(ef_ticket.Value)
  End Sub

  Private Sub UpdateTotalGridTicketsRow(ByVal TicketValue As Decimal)
    Dim _idx_row As Integer
    Dim _coins As Decimal
    Dim _diff As Decimal
    Dim _currency_iso_code As String

    _currency_iso_code = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")

    If WSI.Common.Misc.IsFloorDualCurrencyEnabled() And Me.m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal And Not String.IsNullOrEmpty(Me.m_cage_cls.TerminalIsoCode) Then
      _currency_iso_code = Me.m_cage_cls.TerminalIsoCode
    End If

    For _idx_row = 0 To Me.dg_total.NumRows - 1
      ' Search to get the tickets row
      If Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.CURRENCY_TYPE_DESCRIPTION).Value.Equals(TICKETS_ISOCODE_NLS) Then

        ' 07-NOV-2014 HBB: _coins value is calculate before setting the value to the grid
        _coins = CDec(Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.COINS).Value) + TicketValue

        Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.COINS).Value = _coins
        Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.COINS_WITH_SIMBOL).Value = Me.m_cage_cls.FormatAmount(_coins, False, _currency_iso_code)

        Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.TOTAL).Value = _coins
        Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.TOTAL_WITH_SIMBOL).Value = Me.m_cage_cls.FormatAmount(_coins, False, _currency_iso_code)

        _diff = _coins - Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.CASHIER_TOTAL).Value
        Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.DIFFERENCE_TOTAL).Value = _diff

        If _diff > 0 Then
          Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.DIFFERENCE_TOTAL_WITH_SIMBOL).Value = "+" & Me.m_cage_cls.FormatAmount(_diff, False, _currency_iso_code)
        Else
          Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.DIFFERENCE_TOTAL_WITH_SIMBOL).Value = Me.m_cage_cls.FormatAmount(_diff, False, _currency_iso_code)
        End If

        Me.SetRowColor(_idx_row, Me.dg_total, True)
      End If
    Next

  End Sub ' UpdateTotalTicketsRow

  Private Sub UpdateTotalGridTheoreticalTicketsRow(ByVal TicketValue As Decimal)
    Dim _idx_row As Integer
    Dim _currency_iso_code As String
    Dim _ticket_collection_enabled As Boolean

    _currency_iso_code = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")
    _ticket_collection_enabled = IIf((m_cage_cls.OperationType = Cage.CageOperationType.FromCashier Or m_cage_cls.OperationType = Cage.CageOperationType.FromCashierClosing), _
                                    GeneralParam.GetBoolean("TITO", "Cashier.TicketsCollection"), _
                                    GeneralParam.GetBoolean("TITO", "TicketsCollection"))

    If WSI.Common.Misc.IsFloorDualCurrencyEnabled() And m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal And Not String.IsNullOrEmpty(Me.m_cage_cls.TerminalIsoCode) Then
      _currency_iso_code = Me.m_cage_cls.TerminalIsoCode
    End If

    For _idx_row = 0 To Me.dg_total.NumRows - 1

      ' Search to get the tickets row. This row must be filled out if NoteAcCollectionType <> 0 or we are trying to open a collected money collection
      If Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.CURRENCY_TYPE_DESCRIPTION).Value.Equals(TICKETS_ISOCODE_NLS) Then

        If m_gp_note_collection_type <> Cage.NoteAcCollectionType.HideAll OrElse Not Me.m_new_collecion_mode OrElse Not _ticket_collection_enabled Then

          Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.CASHIER_TOTAL).Value = TicketValue
          Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.CASHIER_TOTAL_WITH_SIMBOL).Value = Me.m_cage_cls.FormatAmount(TicketValue, False, _currency_iso_code)
          Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.DIFFERENCE_TOTAL).Value = TicketValue
          Me.dg_total.Cell(_idx_row, TOTAL_GRID_COLUMNS.DIFFERENCE_TOTAL_WITH_SIMBOL).Value = Me.m_cage_cls.FormatAmount(0 - TicketValue, False, _currency_iso_code)
          Me.SetRowColor(_idx_row, Me.dg_total, True)
        End If
      End If
    Next

  End Sub ' UpdateTotalTicketsRow

  Private Function GetLocalCurrencyDatagrid(Optional ByRef IsoCode As String = "", Optional ByRef IsoType As Int32 = -1, Optional ByRef Tab As TabPage = Nothing) As uc_grid
    Dim _currency_iso_code As String
    Dim _tab As TabPage

    GetLocalCurrencyDatagrid = Nothing

    _currency_iso_code = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")

    ' DHA 13-JAN-2016: set terminal currency
    If WSI.Common.Misc.IsFloorDualCurrencyEnabled() And Me.m_cage_cls.OperationType = Cage.CageOperationType.FromTerminal And Not Me.m_cage_cls.TerminalIsoCode Is Nothing Then
      _currency_iso_code = Me.m_cage_cls.TerminalIsoCode
    End If

    IsoCode = _currency_iso_code

    For Each _tab In Me.tb_currency.TabPages
      If _tab.Tag.Equals(_currency_iso_code & IsoType) Then
        For Each _ctrl As Control In _tab.Controls
          If TypeOf _ctrl Is uc_grid Then
            GetLocalCurrencyDatagrid = _ctrl
            Exit For
          End If
        Next
        Tab = _tab
        Exit For
      End If
    Next

    Return GetLocalCurrencyDatagrid
  End Function ' GetLocalCurrencyDatagrid

  Private Function CheckIfTicketsOk() As Boolean

    '    m_cage_cls.m_type_new_collection.collection_details = FillDetailsList()

    If Not m_cage_cls.m_is_fill_out_movement Then
      If Not Math.Round(Me.m_cage_cls.m_type_new_collection.theoretical_ticket_sum, 2) = Math.Round(Me.m_cage_cls.m_type_new_collection.real_ticket_sum, 2) _
                                                    OrElse Not Me.m_cage_cls.m_type_new_collection.real_ticket_count = Me.m_cage_cls.m_type_new_collection.theoretical_ticket_count Then
        m_cage_cls.m_type_new_collection.balanced = False
        m_cage_cls.m_type_new_collection.alarm_message = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4721, Me.m_cage_cls.m_type_new_collection.terminal_name & " " & TICKETS_ISOCODE_NLS & " " & GUI_FormatCurrency(Me.m_cage_cls.m_type_new_collection.real_ticket_sum - Me.m_cage_cls.m_type_new_collection.theoretical_ticket_sum), GUI_FormatDate(WGDB.Now, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS))
      End If 'GUI_FormatCurrency(Me.m_cage_cls.m_type_new_collection.real_ticket_sum)   Me.m_cage_cls.FormatAmount(Me.m_cage_cls.m_type_new_collection.real_ticket_sum, False, Cage.TICKETS_CODE)
    End If

    If m_cage_cls.m_type_new_collection.balanced Then
      For Each _detail As NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS In m_cage_cls.m_type_new_collection.collection_details
        If Not _detail.matched AndAlso _detail.type = 1 Then
          m_cage_cls.m_type_new_collection.alarm_message = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4727, TICKETS_ISOCODE_NLS, Me.m_cage_cls.m_type_new_collection.terminal_name & " " & GUI_FormatDate(WGDB.Now, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS))
          m_cage_cls.m_type_new_collection.balanced = False
          Exit For
        End If
      Next
    End If

    If Not m_cage_cls.m_type_new_collection.balanced AndAlso Me.m_cage_cls.Status <> Cage.CageStatus.OkDenominationImbalance Then
      Me.m_cage_cls.Status = Cage.CageStatus.OkAmountImbalance
    End If

    Return m_cage_cls.m_type_new_collection.balanced
  End Function 'CheckIfTicketsOk

  Private Function CheckIfTicketPending()

    For Each _detail As NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS In m_cage_cls.m_type_new_collection.collection_details
      If _detail.pending_to_especify AndAlso _detail.type = 1 Then
        Return True
        Exit For
      End If
    Next
    Return False
  End Function

  Private Function TicketBelongToCollection(ByVal TicketId As Int64) As Boolean

    For Each _detail As NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS In m_list_of_theoretical_tickets
      If _detail.ticket_id = TicketId Then

        Return True
      End If
    Next

    Return False
  End Function

#End Region

  Public Sub New()
    MyBase.New()

    Me.FormId = ENUM_FORM.FORM_CAGE_CONTROL

    Call MyBase.GUI_SetFormId()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.

  End Sub

  Public Sub New(ByVal FormId As ENUM_FORM)
    MyBase.New()

    Me.FormId = FormId

    Call MyBase.GUI_SetFormId()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call
  End Sub

  Private Sub frm_cage_control_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    m_after_load = True
  End Sub

  Private Sub TemplateChange() Handles cmb_cage_template.ValueChangedEvent
    Dim _ds As DataSet = New DataSet
    Dim _idx_row As Integer
    Dim _iso_code As String
    Dim _currency_type As CageCurrencyType
    Dim _chip_id As Int64
    Dim _denomination As String
    Dim _row_money() As DataRow
    Dim _idx_tab As Integer
    Dim _datagrid As uc_grid
    Dim _preferences As DataTable
    Dim _currencies As DataTable
    Dim _bill_coin_currency_type As CageCurrencyType
    Dim _str_filter As String
    Dim _only_chips_message_showed As Boolean
    Dim _data_column As DataColumn
    Dim _str_filter_default As String
    Dim _chip_id_default As Int64
    Dim _denomination_default As Decimal
    Dim _rows As DataRow()

    _datagrid = Nothing
    _currencies = Nothing
    _preferences = Nothing
    _row_money = Nothing
    _only_chips_message_showed = False
    Me.m_applying_template = True

    Try
      If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.TEMPLATE_MAINTENANCE Then

        If m_cage_cls.ClosingStocks Is Nothing AndAlso m_cage_cls.CashierId > 0 Then
          m_cage_cls.ClosingStocks = New ClosingStocks(m_cage_cls.CashierId)
        End If

        _currencies = m_cage_cls.ClosingStocks.CurrenciesStocks.Copy()
        If Not _currencies.Columns.Contains("CAGE_CURRENCY_TYPE") Then
          _currencies.Columns.Add("CAGE_CURRENCY_TYPE", System.Type.GetType("System.Int32")).SetOrdinal(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CURRENCY_TYPE)
          For _idx_row = 0 To _currencies.Rows.Count - 1
            _currency_type = _currencies.Rows(_idx_row)(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CURRENCY_TYPE)
            If _currencies.Rows(_idx_row)(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.DENOMINATION) < 0 Then
              _currencies.Rows(_idx_row)(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CURRENCY_TYPE) = 99
            ElseIf FeatureChips.IsCurrencyExchangeTypeChips(_currency_type) Then
              _currencies.Rows(_idx_row)(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CURRENCY_TYPE) = _currency_type
            Else
              _currencies.Rows(_idx_row)(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CURRENCY_TYPE) = 0
            End If
          Next
        End If
        m_original_template = _currencies.Copy

        If Not _currencies Is Nothing Then
          Dim _has_currencies As Boolean

          _has_currencies = 0 < _currencies.Select("ISNULL(QUANTITY, 0) > 0 AND CAGE_CURRENCY_TYPE NOT IN (" & FeatureChips.ChipType.RE & ", " & FeatureChips.ChipType.NR & ", " & FeatureChips.ChipType.COLOR & ") AND ISO_CODE <> '" & Cage.CHIPS_ISO_CODE & "' ").Length

          If Not Me.m_gamingtables_transfer_currency_enabled And
             _has_currencies And
             rb_send.Checked And
             rb_gaming_table.Checked Then
            Dim rc As mdl_NLS.ENUM_MB_RESULT
            rc = NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7129), _
                           ENUM_MB_TYPE.MB_TYPE_WARNING, _
                           ENUM_MB_BTN.MB_BTN_OK, _
                           ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                           Nothing, _
                           Nothing)
            _only_chips_message_showed = True
          End If

          'Datatable data is cleared
          For Each _row As DataRow In Me.m_cage_cls.TableCurrencies.Rows
            _row(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.QUANTITY) = DBNull.Value
            _row(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.TOTAL) = 0
          Next
          'We loop around all the tabs and the controls inside them
          m_redraw_grid = False
          For _idx_tab = 0 To Me.tb_currency.TabCount - 1
            _datagrid = Me.tb_currency.TabPages(_idx_tab).Controls.Item(0)
            _datagrid.Redraw = False
            Me.tb_currency.SelectTab(_idx_tab)
            'Loop around the elements to put the template value of each one of them
            For _idx_row = 0 To _datagrid.NumRows - 1
              _iso_code = _datagrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_ISO_CODE).Value
              _currency_type = _datagrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_CURRENCY_TYPE).Value
              If _datagrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_CHIP_ID).Value <> "" Then
                _chip_id = _datagrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_CHIP_ID).Value
              Else
                _chip_id = -1
              End If

              If (_only_chips_message_showed) And Not FeatureChips.IsCurrencyExchangeTypeChips(_currency_type) Then
                Continue For
              End If
              _denomination = _datagrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_DENOMINATION).Value
              If _currency_type = CageCurrencyType.ChipsRedimible And Not _currencies.Columns.Contains("CHIP_ID") Then
                _iso_code = Cage.CHIPS_ISO_CODE
                _bill_coin_currency_type = CageCurrencyType.Bill
              ElseIf FeatureChips.IsCurrencyExchangeTypeChips(_currency_type) Then
                _bill_coin_currency_type = _currency_type
              Else
                _bill_coin_currency_type = Me.m_cage_cls.GetCurrencyTypeEnum(_datagrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_CURRENCY_TYPE_DESCRIPTION).Value)
              End If
              If CInt(_bill_coin_currency_type) = CageCurrencyType.Bill Then
                _str_filter = String.Format("ISO_CODE = '{0}' AND DENOMINATION = {1} AND CAGE_CURRENCY_TYPE IN ({2}, {3}) ", _iso_code, GUI_LocalNumberToDBNumber(_denomination.ToString), CInt(_bill_coin_currency_type), CInt(CageCurrencyType.Coin))
              ElseIf FeatureChips.IsCurrencyExchangeTypeChips(_currency_type) And _currencies.Columns.Contains("CHIP_ID") Then
                _str_filter = String.Format("ISO_CODE = '{0}' AND CHIP_ID = {1} AND CAGE_CURRENCY_TYPE IN ({2}) ", _iso_code, _chip_id, CInt(_bill_coin_currency_type))
              Else
                _str_filter = String.Format("ISO_CODE = '{0}' AND DENOMINATION = {1} AND CAGE_CURRENCY_TYPE = {2} ", _iso_code, GUI_LocalNumberToDBNumber(_denomination.ToString), CInt(_bill_coin_currency_type))
              End If

              _row_money = _currencies.Select(_str_filter)
              _datagrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_QUANTITY).Value = ""
              _datagrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).Value = ""
              If _row_money.Length > 0 Then
                If Not _row_money(0)("QUANTITY") Is DBNull.Value Then
                  If _row_money(0)("DENOMINATION") > 0 Or _chip_id >= 0 Then
                    _datagrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_QUANTITY).Value = _row_money(0)("QUANTITY")
                  End If
                  If _row_money(0)("DENOMINATION") = 0 Then
                    _datagrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).Value = _row_money(0)("QUANTITY")
                  Else
                    _datagrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).Value = _row_money(0)("QUANTITY") * _row_money(0)("DENOMINATION")
                  End If

                  _row_money(0).Delete()
                  If _denomination >= 0 Then
                    CellDataChangeHandler(_idx_row, GRID_CURRENCY_COLUMN_QUANTITY)
                  Else
                    CellDataChangeHandler(_idx_row, GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL)
                  End If
                End If
              End If
              _datagrid.IsSelected(_idx_row) = False
            Next
          Next

        End If
        'The Totals grid is refreshed
        If Me.tb_currency.TabCount > 0 Then
          Me.dg_total.Clear()
          InitTotalGrid(Me.m_cage_cls.GetAllowedISOCode())
          'FirstUpdateTotalGrid()
          Me.tb_currency.SelectTab(0)
          _datagrid = Me.tb_currency.TabPages(0).Controls.Item(0)
          _datagrid.TopRow = 0
          _datagrid.Redraw = True
        End If
        m_redraw_grid = True

      Else
        'An element has been choosen
        If Me.cmb_cage_template.Value > 0 Then
          'Template is loaded
          If Me.m_cage_cls.LoadCageTemplate(Me.cmb_cage_template.Value, _ds) Then
            'Preferences
            _preferences = _ds.Tables(TABLE_NAME_PREFERENCES)
            m_original_preferences = _preferences

            If Me.rb_to_user.Checked Then
              m_cage_cls.TargetType = Cage.PendingMovementType.ToCashierUser
            Else
              m_cage_cls.TargetType = Cage.PendingMovementType.ToCashierTerminal
            End If

            If Not _preferences Is Nothing Then

              If Me.m_cage_cls.OperationType <> _preferences.Rows(0)(TABLE_COLUMN_OPERATION_TYPE) OrElse _
                 (Me.m_cage_cls.TargetType <> _preferences.Rows(0)(TABLE_COLUMN_TARGET_TYPE) AndAlso Me.m_cage_cls.OperationType = Cage.CageOperationType.ToCashier) Then

                Me.m_cage_cls.OperationType = _preferences.Rows(0)(TABLE_COLUMN_OPERATION_TYPE)

                If Me.m_cage_auto_mode Then
                  If Me.m_cage_cls.OperationType = Cage.CageOperationType.ToGamingTable AndAlso Me.rb_gaming_table.Enabled Then
                    Me.rb_gaming_table.Checked = True
                  Else
                    Me.rb_custom.Checked = True
                  End If
                Else

                  Select Case Me.m_cage_cls.OperationType
                    Case Cage.CageOperationType.ToCashier And Me.rb_cashier.Enabled
                      Me.rb_cashier.Checked = True
                    Case Cage.CageOperationType.ToCustom And Me.rb_custom.Enabled
                      Me.rb_custom.Checked = True
                    Case Cage.CageOperationType.ToGamingTable And Me.rb_gaming_table.Enabled
                      Me.rb_gaming_table.Checked = True
                    Case Else

                      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5784), ENUM_MB_TYPE.MB_TYPE_INFO)

                      'Check first radiobutton enabled to check it.
                      For Each _ctrl As Control In Me.gb_source_target.Controls
                        If TypeOf _ctrl Is RadioButton AndAlso CType(_ctrl, RadioButton).Enabled Then
                          CType(_ctrl, RadioButton).Checked = True

                          Select Case CType(_ctrl, RadioButton).Name
                            Case Me.rb_cashier.Name
                              Me.m_cage_cls.OperationType = Cage.CageOperationType.ToCashier

                            Case Me.rb_custom.Name
                              Me.m_cage_cls.OperationType = Cage.CageOperationType.ToCustom

                            Case Me.rb_gaming_table.Name
                              Me.m_cage_cls.OperationType = Cage.CageOperationType.ToGamingTable

                            Case Else
                              Me.m_cage_cls.OperationType = Cage.CageOperationType.ToCashier

                          End Select

                          Exit For
                        End If
                      Next
                  End Select
                End If

                Me.m_cage_cls.TargetType = _preferences.Rows(0)(TABLE_COLUMN_TARGET_TYPE)

                If Me.m_cage_cls.TargetType = Cage.PendingMovementType.ToCashierUser Then
                  Me.rb_to_user.Checked = True
                Else
                  Me.rb_to_terminal.Checked = True
                End If
                ' Refresh
                Call Me.CheckRadioButtonsToSetLayouts()
              End If
            End If

            _currencies = _ds.Tables(TABLE_NAME_CURRENCIES)
            If Not _currencies.Columns.Contains("CHIP_ID") Then
              _data_column = New DataColumn("CHIP_ID", System.Type.GetType("System.Int64"))
              _data_column.DefaultValue = 0
              _currencies.Columns.Add(_data_column)
            End If
            If Not _currencies.Columns.Contains("TYPE") Then
              _data_column = New DataColumn("TYPE", System.Type.GetType("System.Int32"))
              _data_column.DefaultValue = 0
              _currencies.Columns.Add(_data_column)
              _currencies.Columns.Item("TYPE").SetOrdinal(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CURRENCY_TYPE)

              For _idx_row = 0 To _currencies.Rows.Count - 1
                _currency_type = _currencies.Rows(_idx_row)(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CURRENCY_TYPE)
                If _currencies.Rows(_idx_row)(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.DENOMINATION) < 0 Then
                  _currencies.Rows(_idx_row)(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CURRENCY_TYPE) = 99
                ElseIf _currencies.Rows(_idx_row)(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.ISO_CODE) = Cage.CHIPS_ISO_CODE Then
                  _currencies.Rows(_idx_row)(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CURRENCY_TYPE) = CageCurrencyType.ChipsRedimible
                  _currencies.Rows(_idx_row)(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.ISO_CODE) = CurrencyExchange.GetNationalCurrency()
                ElseIf FeatureChips.IsCurrencyExchangeTypeChips(_currency_type) Then
                  _currencies.Rows(_idx_row)(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CURRENCY_TYPE) = _currency_type
                Else
                  _currencies.Rows(_idx_row)(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CURRENCY_TYPE) = 0
                End If
                If _currencies.Rows(_idx_row)(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.CURRENCY_TYPE) = CageCurrencyType.ChipsRedimible AndAlso _currencies.Rows(_idx_row)("CHIP_ID") = 0 Then
                  _denomination_default = _currencies.Rows(_idx_row)(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.DENOMINATION)
                  _str_filter_default = String.Format("ISO_CODE = '{0}' AND TYPE = {1} AND DENOMINATION = {2} ", CurrencyExchange.GetNationalCurrency(), Convert.ToString(CageCurrencyType.ChipsRedimible), GUI_LocalNumberToDBNumber(_denomination_default.ToString()))
                  _rows = Me.m_cage_cls.TableCurrencies.Select(_str_filter_default)
                  If _rows.Length > 0 Then
                    _chip_id_default = _rows(0).Item("CHIP_ID")
                    _currencies.Rows(_idx_row)("CHIP_ID") = _chip_id_default
                  End If
                End If
              Next
            End If
            m_original_template = _currencies

            If Not _currencies Is Nothing Then
              Dim _has_currencies As Boolean

              _has_currencies = _currencies.Compute("Sum(TOTAL)", "TYPE <> '" & FeatureChips.ChipType.RE & "' AND TYPE <> '" & FeatureChips.ChipType.NR & "' AND TYPE <> '" & FeatureChips.ChipType.COLOR & "' ")

              If Not Me.m_gamingtables_transfer_currency_enabled And
                 _has_currencies And
                 rb_send.Checked And
                 rb_gaming_table.Checked Then
                Dim rc As mdl_NLS.ENUM_MB_RESULT
                rc = NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7129), _
                               ENUM_MB_TYPE.MB_TYPE_WARNING, _
                               ENUM_MB_BTN.MB_BTN_OK, _
                               ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                               Nothing, _
                               Nothing)
                _only_chips_message_showed = True
              End If

              'Datatable data is cleared
              For Each _row As DataRow In Me.m_cage_cls.TableCurrencies.Rows
                _row(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.QUANTITY) = DBNull.Value
                _row(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.TOTAL) = 0
              Next
              'We loop around all the tabs and the controls inside them
              m_redraw_grid = False
              For _idx_tab = 0 To Me.tb_currency.TabCount - 1
                _datagrid = Me.tb_currency.TabPages(_idx_tab).Controls.Item(0)
                _datagrid.Redraw = False
                Me.tb_currency.SelectTab(_idx_tab)
                'Loop around the elements to put the template value of each one of them
                For _idx_row = 0 To _datagrid.NumRows - 1
                  _iso_code = _datagrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_ISO_CODE).Value
                  _currency_type = _datagrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_CURRENCY_TYPE).Value
                  If _datagrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_CHIP_ID).Value <> "" Then
                    _chip_id = _datagrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_CHIP_ID).Value
                  Else
                    _chip_id = -1
                  End If

                  If (_only_chips_message_showed) And Not FeatureChips.IsCurrencyExchangeTypeChips(_currency_type) Then
                    Continue For
                  End If
                  _denomination = _datagrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_DENOMINATION).Value
                  If _currency_type = CageCurrencyType.ChipsRedimible And Not _currencies.Columns.Contains("CHIP_ID") Then
                    _iso_code = Cage.CHIPS_ISO_CODE
                    _bill_coin_currency_type = CageCurrencyType.Bill
                  ElseIf FeatureChips.IsCurrencyExchangeTypeChips(_currency_type) Then
                    _bill_coin_currency_type = _currency_type
                  Else
                    _bill_coin_currency_type = Me.m_cage_cls.GetCurrencyTypeEnum(_datagrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_CURRENCY_TYPE_DESCRIPTION).Value)
                  End If
                  If CInt(_bill_coin_currency_type) = CageCurrencyType.Bill Then
                    _str_filter = String.Format("ISO_CODE = '{0}' AND DENOMINATION = {1} AND TYPE IN ({2}, {3}) ", _iso_code, GUI_LocalNumberToDBNumber(_denomination.ToString), CInt(_bill_coin_currency_type), CInt(CageCurrencyType.Coin))
                  ElseIf FeatureChips.IsCurrencyExchangeTypeChips(_currency_type) And _currencies.Columns.Contains("CHIP_ID") Then
                    _str_filter = String.Format("ISO_CODE = '{0}' AND CHIP_ID = {1} AND TYPE IN ({2}) ", _iso_code, _chip_id, CInt(_bill_coin_currency_type))
                  Else
                    _str_filter = String.Format("ISO_CODE = '{0}' AND DENOMINATION = {1} AND TYPE = {2} ", _iso_code, GUI_LocalNumberToDBNumber(_denomination.ToString), CInt(_bill_coin_currency_type))
                  End If

                  _row_money = _currencies.Select(_str_filter)
                  _datagrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_QUANTITY).Value = ""
                  _datagrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).Value = ""
                  If _row_money.Length > 0 Then
                    If Not _row_money(0)("QUANTITY") Is DBNull.Value Then
                      If _row_money(0)("DENOMINATION") > 0 Or _chip_id >= 0 Then
                        _datagrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_QUANTITY).Value = _row_money(0)("QUANTITY")
                      End If
                      _datagrid.Cell(_idx_row, GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).Value = _row_money(0)("TOTAL")
                      If _denomination >= 0 Then
                        CellDataChangeHandler(_idx_row, GRID_CURRENCY_COLUMN_QUANTITY)
                      Else
                        CellDataChangeHandler(_idx_row, GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL)
                      End If
                    End If
                  End If
                  _datagrid.IsSelected(_idx_row) = False
                Next
              Next

            End If
            'The Totals grid is refreshed
            If Me.tb_currency.TabCount > 0 Then
              Me.dg_total.Clear()
              InitTotalGrid(GetAllowedCurrenciesByGamingTable())
              FirstUpdateTotalGrid()
              Me.tb_currency.SelectTab(0)
              _datagrid = Me.tb_currency.TabPages(0).Controls.Item(0)
              _datagrid.TopRow = 0
              _datagrid.Redraw = True
            End If
            m_redraw_grid = True
          End If
        End If
      End If

      ' The template has old format and it should be change
      If Not _currencies Is Nothing AndAlso Not _currencies.Columns.Contains("CHIP_ID") Then
        NewCageTemplate()
      Else
        If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.TEMPLATE_MAINTENANCE AndAlso DbReadObject Is Nothing Then
          DbReadObject = New CLASS_CAGE_CONTROL
        End If
          DbReadObject = DbEditedObject.Duplicate
        End If

      'The template that has been used
        m_template_id = Me.cmb_cage_template.Value
        m_changed_values = False
        Me.m_applying_template = False ' Reset value

        ' Dispose
        Call SafeDispose(_datagrid)
        Call SafeDispose(_currencies)
        Call SafeDispose(_preferences)
        Call SafeDispose(_row_money)
        Call SafeDispose(_ds)

    Catch ex As Exception
      m_redraw_grid = True
    End Try
  End Sub

  Private Function GetTemplateDifferences(ByVal dtFirst As DataTable, ByVal dtSecond As DataTable, ByVal dtPreferences As DataTable) As DataTable
    Dim _dt_differences As DataTable
    Dim _dr_value() As DataRow
    Dim _dr_template() As Object
    Dim _old_value As Decimal
    Dim _new_value As Decimal
    Dim _operation_type As Integer
    Dim _target_type As Integer
    Dim _column_name As String
    Dim _str_filter As String
    Dim _column_type As String

    _dt_differences = New DataTable()
    _column_name = String.Empty

    If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.TEMPLATE_MAINTENANCE Then
      _column_type = "CAGE_CURRENCY_TYPE"
    Else
      _column_type = "TYPE"
    End If



    ReDim _dr_template(9)

    _dt_differences.Columns.Add("ISO_CODE", Type.GetType("System.String"))
    _dt_differences.Columns.Add("DENOMINATION", Type.GetType("System.Int64"))
    _dt_differences.Columns.Add(_column_type, Type.GetType("System.Int32"))
    _dt_differences.Columns.Add("STATUS", Type.GetType("System.Int32"))
    _dt_differences.Columns.Add("OLD_VALUE", Type.GetType("System.Int64"))
    _dt_differences.Columns.Add("NEW_VALUE", Type.GetType("System.Int64"))
    _dt_differences.Columns.Add("DENOM_NAME", Type.GetType("System.String"))
    _dt_differences.Columns.Add("CHIP_NAME", Type.GetType("System.String"))
    _dt_differences.Columns.Add("CHIP_DRAW", Type.GetType("System.String"))
    _dt_differences.Columns.Add("CHIP_GROUP", Type.GetType("System.String"))

    If dtFirst Is Nothing Then
      dtFirst = dtSecond.Clone()
    End If

    'ADD column R (readed) to First
    dtFirst.Columns.Add("R", Type.GetType("System.Int32")).DefaultValue = 0

    If Not dtPreferences Is Nothing Then
      _operation_type = dtPreferences.Rows(0).Item("OperationType")
      _target_type = dtPreferences.Rows(0).Item("TargetType")

      If m_cage_cls.OperationType <> _operation_type Then
        _dr_template(0) = TABLE_COLUMN_OPERATION_TYPE
        _dr_template(1) = 0
        _dr_template(3) = TEMPLATE_CHANGES_STATUS.NONE
        _dr_template(4) = _operation_type
        _dr_template(5) = m_cage_cls.OperationType

        _dt_differences.Rows.Add(_dr_template)
      End If

      If m_cage_template.TargetType <> _target_type Then
        _dr_template(0) = TABLE_COLUMN_TARGET_TYPE
        _dr_template(1) = 0
        _dr_template(3) = TEMPLATE_CHANGES_STATUS.NONE
        _dr_template(4) = _target_type
        _dr_template(5) = m_cage_template.TargetType

        _dt_differences.Rows.Add(_dr_template)

      End If
    End If

    For Each _dr_element As DataRow In dtSecond.Rows

      'Find rows by iso code and denomination
      '15-OCT-2014 JPJ Fixed Bug WIG-1500: Exception occurs when loading a template and language is set to spanish.
      If _dr_element("TYPE") = CageCurrencyType.Bill Then
        _str_filter = String.Format("ISO_CODE = '{0}' AND DENOMINATION = {1} AND " & _column_type & " IN ({2}, {3}) ", _dr_element("ISO_CODE"), GUI_LocalNumberToDBNumber(_dr_element("DENOMINATION")), _dr_element("TYPE"), CInt(CageCurrencyType.Coin))
      ElseIf FeatureChips.IsCageCurrencyTypeChips(CInt(_dr_element("TYPE"))) Then
        _str_filter = String.Format("ISO_CODE = '{0}' AND CHIP_ID = {1} AND " & _column_type & " = {2} ", _dr_element("ISO_CODE"), GUI_LocalNumberToDBNumber(_dr_element("CHIP_ID")), _dr_element("TYPE"))
      Else
        _str_filter = String.Format("ISO_CODE = '{0}' AND DENOMINATION = {1} AND " & _column_type & " = {2} ", _dr_element("ISO_CODE"), GUI_LocalNumberToDBNumber(_dr_element("DENOMINATION")), _dr_element("TYPE"))
      End If

      _dr_value = dtFirst.Select(_str_filter)

      If Not _dr_value.Length = 0 Then
        'Mark row as readed
        _dr_value(0).Item(dtFirst.Columns.Count - 1) = 1
      End If

      If _dr_element("DENOMINATION") < 0 And Me.m_cage_cls.OpenMode <> CLASS_CAGE_CONTROL.OPEN_MODE.TEMPLATE_MAINTENANCE Then
        _column_name = "TOTAL"
      Else
        _column_name = "QUANTITY"
      End If

      'If row doesn't exists, we add as new row
      If (_dr_value.Length = 0 OrElse _dr_value Is Nothing) Then
        _dr_template(0) = _dr_element("ISO_CODE")
        _dr_template(1) = _dr_element("DENOMINATION")
        _dr_template(2) = _dr_element("TYPE")
        _dr_template(3) = TEMPLATE_CHANGES_STATUS.ADDED ' NEW
        _dr_template(4) = 0

        ' If the current row is money, cards, etc (id <0) we take total value, not quantity
        _dr_template(5) = IIf(IsDBNull(_dr_element(_column_name)), 0, _dr_element(_column_name))
        If Me.m_cage_cls.OpenMode <> CLASS_CAGE_CONTROL.OPEN_MODE.TEMPLATE_MAINTENANCE Then
          _dr_template(6) = _dr_element("DENOMINATION_WITH_SIMBOL")
        Else
          _dr_template(7) = _dr_element("NAME")
          _dr_template(8) = _dr_element("DRAWING")
          _dr_template(9) = _dr_element("CHIP_SET_NAME")
        End If
        'Add
        _dt_differences.Rows.Add(_dr_template)
      Else

        If IsDBNull(_dr_value(0).Item(_column_name)) Then
          _old_value = 0
        Else
          _old_value = Convert.ToDecimal(_dr_value(0).Item(_column_name))
        End If

        If IsDBNull(_dr_element(_column_name)) Then
          _new_value = 0
        Else
          _new_value = Convert.ToDecimal(_dr_element(_column_name))
        End If

        If _old_value > 0 Then
          If _new_value <> _old_value Then

            _dr_template(0) = _dr_element("ISO_CODE")
            _dr_template(1) = _dr_element("DENOMINATION")
            _dr_template(2) = _dr_element("TYPE")
            _dr_template(3) = TEMPLATE_CHANGES_STATUS.UPDATED 'UPDATED
            _dr_template(4) = _old_value
            _dr_template(5) = _new_value
            If Me.m_cage_cls.OpenMode <> CLASS_CAGE_CONTROL.OPEN_MODE.TEMPLATE_MAINTENANCE Then
              _dr_template(6) = _dr_element("DENOMINATION_WITH_SIMBOL")
            Else
              _dr_template(7) = _dr_element("NAME")
              _dr_template(8) = _dr_element("DRAWING")
              _dr_template(9) = _dr_element("CHIP_SET_NAME")
            End If

            _dt_differences.Rows.Add(_dr_template)
          End If
        Else
          If _new_value <> _old_value Then

            _dr_template(0) = _dr_element("ISO_CODE")
            _dr_template(1) = _dr_element("DENOMINATION")
            _dr_template(2) = _dr_element("TYPE")
            _dr_template(3) = TEMPLATE_CHANGES_STATUS.ADDED ' NEW
            _dr_template(4) = _old_value
            _dr_template(5) = _new_value
            If Me.m_cage_cls.OpenMode <> CLASS_CAGE_CONTROL.OPEN_MODE.TEMPLATE_MAINTENANCE Then
              _dr_template(6) = _dr_element("DENOMINATION_WITH_SIMBOL")
            Else
              _dr_template(7) = _dr_element("NAME")
              _dr_template(8) = _dr_element("DRAWING")
              _dr_template(9) = _dr_element("CHIP_SET_NAME")
            End If

            _dt_differences.Rows.Add(_dr_template)
          End If
        End If
      End If
    Next

    ' Search the rows not readed to mark as deleted
    _dr_value = dtFirst.Select("R IS NULL")

    If _dr_value.Length <> 0 Then
      For Each _dr As DataRow In _dr_value
        _dr_template(0) = _dr("ISO_CODE")
        _dr_template(1) = _dr("DENOMINATION")
        _dr_template(2) = _dr(_column_type)
        _dr_template(3) = TEMPLATE_CHANGES_STATUS.DELETED ' DELETED
        If Me.m_cage_cls.OpenMode <> CLASS_CAGE_CONTROL.OPEN_MODE.TEMPLATE_MAINTENANCE Then
          _dr_template(6) = _dr("DENOMINATION_WITH_SIMBOL")
        End If

        _dt_differences.Rows.Add(_dr_template)
      Next
    End If

    ' Dispose
    Call SafeDispose(_dr_template)
    Call SafeDispose(_dr_value)

    Return _dt_differences
  End Function

  Private Sub dg_list_DataSelectedEvent() Handles dg_collection_tickets.DataSelectedEvent
    Dim _idx_row As Int32
    Dim _frm_select_ticket As frm_TITO_select_ticket
    Dim _validation_number As String

    _frm_select_ticket = New frm_TITO_select_ticket()
    _validation_number = ""

    For _idx_row = 0 To Me.dg_collection_tickets.NumRows - 1
      If Me.dg_collection_tickets.Row(_idx_row).IsSelected Then
        _validation_number = dg_collection_tickets.Cell(_idx_row, GRID_COLUMN_TICKET_NUMBER_ORIGINAL_FORMAT).Value
        Exit For
      End If
    Next

    If dg_collection_tickets.Cell(_idx_row, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Value = "?" Then
      If _frm_select_ticket.ShowForSelect(Me.MdiParent, _validation_number) = DialogResult.Cancel Then 'the selection was canceled
        ef_ticket.TextValue = ""

        Return
      End If
      ' dg_collection_tickets.DeleteRow(_idx_row)

    End If

    If _frm_select_ticket.SelectedTicketId > 0 Then
      AddTicketToCollection(_validation_number, 0, False, _frm_select_ticket.SelectedTicketId)
    End If

    Call SafeDispose(_frm_select_ticket)

  End Sub

  Private Sub InitializeScanner()

    Dim _note_counter_info As NoteCounterInfo
    Dim _application_id As Int32

    m_first_error = True
    m_second_error = True

    If CommonScannerFunctions.GetApplicationId(_application_id) Then
      Try
        If m_scanner_manager Is Nothing Then

          _note_counter_info = CommonScannerFunctions.GetScannerInfo(_application_id)
          If _note_counter_info Is Nothing Then
            lb_error_messages.Visible = False
            lb_note_counter_status.Visible = False
            lbl_counter.Visible = False
            Return
          End If
          m_scanner_manager = New ScannerManager(_note_counter_info)

          AddHandler m_scanner_manager.NoteCountReceivedEvent, AddressOf CatchMessageFromScanner

          m_list_of_event = New List(Of String)
          m_frm_counter_event = New frm_note_counter_events(m_list_of_event)

          If m_scanner_manager.InitializeScanner() Then

            lb_error_messages.ForeColor = Color.Blue
            lb_error_messages.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5196)
            lb_note_counter_status.BackColor = Color.Green
            m_note_counter_flag = False
            m_lbl_counter_status_update = 0
            lbl_counter.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5448)
            tmr_note_counter_working.Start()
            m_scanner_initialized = True
          Else
            lb_error_messages.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5189)
            lbl_counter.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5450)
            m_lbl_counter_status_update = 7
          End If
          lb_error_messages.Visible = True
          lbl_counter.Visible = True
          lb_note_counter_status.Visible = True
          If Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MASSIVE Then
            Me.Size = New Size(Me.Size.Width, Me.Size.Height + 20)
            Me.gb_total.Location = New Point(Me.gb_total.Location.X, Me.gb_total.Location.Y - 10)
            Me.tb_currency.Location = New Point(Me.tb_currency.Location.X, Me.tb_currency.Location.Y - 15)
            Me.tb_concepts.Location = New Point(Me.tb_concepts.Location.X, Me.tb_concepts.Location.Y - 15)
            Me.dg_concepts.Location = New Point(Me.dg_concepts.Location.X, Me.dg_concepts.Location.Y - 15)
            Me.gb_note_counter_status.Location = New Point(3, 683)
            Me.gb_note_counter_events.Location = New Point(169, 683)
          Else
            Me.Size = New Size(Me.Size.Width, Me.Size.Height + 40)
            Me.gb_total.Location = New Point(Me.gb_total.Location.X, Me.gb_total.Location.Y - 3)
            Me.tb_currency.Location = New Point(Me.tb_currency.Location.X, Me.tb_currency.Location.Y - 3)
            Me.dg_concepts.Location = New Point(Me.dg_concepts.Location.X, Me.dg_concepts.Location.Y - 3)
            Me.gb_note_counter_status.Location = New Point(3, 707)
            Me.gb_note_counter_events.Location = New Point(169, 707)
          End If
          Me.gb_note_counter_status.Visible = True
          Me.gb_note_counter_events.Visible = True

          'Call set form features resolutions when size's form exceeds desktop area
          Call GUI_AutoAdjustResolutionForm(Me, Nothing, Nothing)

        End If

      Catch ex As ArgumentNullException
        lb_error_messages.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5189)
        Log.Error(ex.Message())
      End Try
    End If

  End Sub

  Private Function IsEditingEnabled() As Boolean
    Dim _is_editing_enabled As Boolean

    Select Case Me.m_cage_cls.OpenMode
      Case CLASS_CAGE_CONTROL.OPEN_MODE.CLOSE_CAGE
        _is_editing_enabled = True
      Case CLASS_CAGE_CONTROL.OPEN_MODE.CLOSE_VIRTUAL
        _is_editing_enabled = True
      Case CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE
        _is_editing_enabled = True
      Case CLASS_CAGE_CONTROL.OPEN_MODE.NEW_MOVEMENT
        _is_editing_enabled = True
      Case CLASS_CAGE_CONTROL.OPEN_MODE.OPEN_CAGE
        _is_editing_enabled = True
      Case CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_FROM_PROMOBOX_OR_ACCEPTOR
        _is_editing_enabled = True
      Case CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MASSIVE
        _is_editing_enabled = True
      Case CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MOVEMENT
        _is_editing_enabled = True
      Case CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT_FROM_PROMOBOX_OR_ACCEPTOR
        _is_editing_enabled = False
      Case CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT
        _is_editing_enabled = False
      Case CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT_FROM_GAMINGTABLES
        _is_editing_enabled = False
      Case CLASS_CAGE_CONTROL.OPEN_MODE.VIEW_MOVEMENT
        _is_editing_enabled = False
      Case Else
        _is_editing_enabled = False
    End Select

    Return _is_editing_enabled
  End Function

  Private Function EventsToXML() As String

    Dim _ds As DataSet
    Dim _dt As DataTable
    Dim _dr As DataRow
    _ds = New DataSet("EventsList")

    _dt = New DataTable("Events")
    _dt.Columns.Add("EventText", Type.GetType("System.String"))

    For Each _str As String In m_list_of_event
      _dr = _dt.NewRow()
      _dr("EventText") = _str
      _dt.Rows.Add(_dr)
    Next
    _ds.Tables.Add(_dt)

    Return WSI.Common.Misc.DataSetToXml(_ds, XmlWriteMode.IgnoreSchema)
    ' Dispose
    Call SafeDispose(_ds)

  End Function

  Private Sub XMLToEvents(ByVal EventsXML As String)
    Dim _ds As DataSet

    _ds = New DataSet()

    WSI.Common.Misc.DataSetFromXml(_ds, EventsXML, XmlReadMode.Auto)

    If _ds IsNot Nothing AndAlso _ds.Tables(0).Rows.Count > 0 Then
      For _idx As Int32 = 0 To _ds.Tables(0).Rows.Count - 1
        If _ds.Tables(0).Rows(_idx).Item(0) IsNot DBNull.Value _
        AndAlso Not String.IsNullOrEmpty(_ds.Tables(0).Rows(_idx).Item(0)) Then

          m_list_of_event.Add(_ds.Tables(0).Rows(_idx).Item(0).ToString())
        End If
      Next
    End If

    ' Dispose
    Call SafeDispose(_ds)

  End Sub

  Private Sub ChangeLabelVisibility()

    If m_lbl_counter_status_update > 6 Then
      lbl_counter.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5449)
      If m_note_counter_flag Then
        If lb_note_counter_status.BackColor <> Color.Green Then
          lb_note_counter_status.BackColor = Color.Green
        Else
          lb_note_counter_status.BackColor = Control.DefaultBackColor
        End If
        m_note_counter_flag = False
      Else
        lb_note_counter_status.BackColor = Color.Green
      End If
    Else
      m_lbl_counter_status_update += 1
    End If
  End Sub

  Private Sub ResetNoteCounterEvents()

    m_list_of_event = New List(Of String)
    m_first_error = True
    m_second_error = True
    Me.btn_open_events.Visible = False
    Me.lb_error_messages.Text = ""
    If m_frm_counter_event IsNot Nothing Then
      m_frm_counter_event.Clear()
    End If
    If m_scanner_initialized Then
      Me.lbl_counter.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5449)
      Me.lb_note_counter_status.BackColor = Color.Green
    End If
  End Sub

  Private Sub ShowNoteCounterEvents()
    Me.Size = New Size(Me.Size.Width, Me.Size.Height + 50)
    Me.gb_note_counter_events.Size = New Size(733, Me.gb_note_counter_events.Height)
    Me.gb_note_counter_events.Location = New Point(3, Me.tb_collections.Location.Y + Me.tb_collections.Size.Height - 45)
    Me.gb_note_counter_events.Visible = True
    Me.btn_open_events.Location = New Point(Me.btn_open_events.Location.X + 167, Me.btn_open_events.Location.Y)
    Me.lb_error_messages.Visible = True
    Me.lb_error_messages.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5188)

    'Call set form features resolutions when size's form exceeds desktop area
    Call GUI_AutoAdjustResolutionForm(Me, Nothing, Nothing)
  End Sub

  Private Sub tmr_note_counter_working_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmr_note_counter_working.Tick

    If Me.lb_note_counter_status.InvokeRequired() Then
      Me.lb_note_counter_status.Invoke(New InvokeChangeLabelVisibility(AddressOf ChangeLabelVisibility))
    Else
      Call ChangeLabelVisibility()
    End If
  End Sub

  Private Sub SetConceptsGridAndTabValues()

    If Me.m_cage_cls.OperationType = Cage.CageOperationType.CountCage _
      OrElse Me.m_cage_cls.OperationType = Cage.CageOperationType.GainQuadrature _
      OrElse Me.m_cage_cls.OperationType = Cage.CageOperationType.LostQuadrature _
      OrElse (Me.rb_send.Checked And Me.rb_cashier.Checked And WSI.Common.GeneralParam.GetBoolean("Cage", "Transfer.CashDesk.HideItems", True)) _
      OrElse Me.m_cage_cls.OpenMode = CLASS_CAGE_CONTROL.OPEN_MODE.TEMPLATE_MAINTENANCE Then
      Me.tb_concepts.Visible = False
      Me.dg_concepts.Visible = False
    Else
      If Me.rb_reception.Checked And Me.rb_cashier.Checked And Me.m_cage_cls.m_is_fill_out_movement Then
        Me.tb_concepts.Visible = False
        Me.dg_concepts.Visible = False
      Else
        Me.tb_concepts.Visible = True
      End If
      Me.SetConceptsValues()
    End If

    Call SetGrids()

  End Sub

  Private Sub SetConceptsGridFromMovementDetails()
    Dim _concept_id As Int64
    Dim _idx_row_grid As Int32
    Dim _idx_col_grid As Int32

    Me.m_cage_cls.ConceptsData = CageMeters.GetItemsTableFromMovementsDetails(Me.m_cage_cls.MovementID, Me.m_cage_cls.ConceptsData, m_currencies_types)

    _concept_id = 0
    _idx_row_grid = 0
    _idx_col_grid = GRID_CONCEPT_AMOUNT

    For Each _concept_row As DataRow In Me.m_cage_cls.ConceptsData.Rows
      If _concept_id <> _concept_row("CONCEPT_ID") Then
        _concept_id = _concept_row("CONCEPT_ID")
        _idx_row_grid += 1
        _idx_col_grid = GRID_CONCEPT_AMOUNT
        Me.dg_concepts.Cell(_idx_row_grid - 1, 0).Value = _concept_row("CONCEPT_ID")
        Me.dg_concepts.Cell(_idx_row_grid - 1, 1).Value = _concept_row("CONCEPT_ONLY_NATIONAL_CURRENCY")
        Me.dg_concepts.Cell(_idx_row_grid - 1, 2).Value = _concept_row("CONCEPT_SOURCE_TARGET_ID")
        Me.dg_concepts.Cell(_idx_row_grid - 1, 3).Value = _concept_row("CONCEPT_NAME")
      End If
      Me.dg_concepts.Cell(_idx_row_grid - 1, _idx_col_grid).Value = _concept_row("CONCEPT_AMOUNT")
      _idx_col_grid += 1
    Next

  End Sub

  ' PURPOSE: Check if gaming table selected is cashier 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - True is only gaming table, False another case
  Private Function IsOnlyGamingTable() As Boolean
    Dim _is_gaming_table_integrated As Boolean

    _is_gaming_table_integrated = True
    If Me.m_cage_cls.OperationType = Cage.CageOperationType.ToGamingTable Then
      Me.m_cage_cls.HasIntegratedCashier(Me.cmb_gaming_table.Value, _is_gaming_table_integrated)
      '_is_gaming_table_integrated = Not _is_gaming_table_integrated
    End If

    Return Not _is_gaming_table_integrated
  End Function

  Private Sub SetConceptsColWidth()
    Const ConversionMedida As Double = 12.3835
    Dim _cuenta_columnas As Integer = 0
    Dim _tama�o_currency As Integer = dg_concepts.Column(3).Width / ConversionMedida
    Dim _tama�o As Integer = 0
    For iFor As Integer = 0 To dg_concepts.NumColumns - 1

      If dg_concepts.Column(iFor).Width > 0 Then _cuenta_columnas += 1
    Next
    _tama�o = Me.Width - (_tama�o_currency * (_cuenta_columnas - 1))
    If _tama�o >= _tama�o_currency Then dg_concepts.Column(2).Width = _tama�o * ConversionMedida

  End Sub

  Private Function IsGamingTable() As Boolean
    Dim _gaming_table_id = Me.m_cage_cls.GetGamingTableIdFromCashierSession(Me.m_cage_cls.CashierSessionId)
    Return (GamingTableBusinessLogic.IsGamingTablesEnabled() And _gaming_table_id > 0)
  End Function

  Private Sub SetEditableOnCurrencyGrids(Optional ByVal IsoCode As String = "")
    Dim _idx_row As Integer
    Dim _grids As List(Of uc_grid)
    Dim _iso_code As String
    Dim _currency_type As CageCurrencyType
    Dim _empty_cells As Boolean
    Dim _enabled_all As Boolean
    Dim _same_iso_code As Boolean

    _grids = Me.GetAllCurrencyGrids()

    Call RefreshTabsBySelection(IsoCode)

    If rb_cashier.Checked And Not String.IsNullOrEmpty(IsoCode) Then
      _empty_cells = (rb_send.Checked Or rb_reception.Checked) AndAlso rb_to_terminal.Checked AndAlso WSI.Common.Misc.IsCountREnabled
    ElseIf (rb_gaming_table.Checked) Then
      _empty_cells = rb_send.Checked And Not Me.m_gamingtables_transfer_currency_enabled
      Me.m_cells_are_editable = Not rb_send.Checked Or Me.m_gamingtables_transfer_currency_enabled
      IsoCode = Cage.CHIPS_ISO_CODE
    Else
      _enabled_all = True
    End If

    If _empty_cells And Me.CheckAreValuesOnCurrencyGrids(IsoCode) Then
      For Each datarow As DataRow In Me.m_cage_cls.TableCurrencies.Rows
        If Not FeatureChips.IsCurrencyExchangeTypeChips(datarow("TYPE")) Then
          datarow("QUANTITY") = System.DBNull.Value
          datarow("TOTAL") = 0
        End If
      Next
    End If

    For Each grd As uc_grid In _grids
      With grd

        If grd Is Nothing OrElse grd.NumRows = 0 Then
          Continue For
        End If

        _same_iso_code = False
        _currency_type = grd.Cell(0, GRID_CURRENCY_COLUMN_CURRENCY_TYPE).Value

        If Me.m_cage_cls.IsCountR Then
          _iso_code = grd.Cell(0, GRID_CURRENCY_COLUMN_ISO_CODE).Value
          _same_iso_code = (_iso_code = IsoCode)
          Me.m_cells_are_editable = (_same_iso_code Or _enabled_all)
        Else
          If (rb_send.Checked Or rb_reception.Checked) Then
            If (Not Me.m_gamingtables_transfer_currency_enabled And Not FeatureChips.IsCurrencyExchangeTypeChips(_currency_type)) And rb_gaming_table.Checked Then
              Me.m_cells_are_editable = False
            Else
              Me.m_cells_are_editable = True
            End If
          Else
            Me.m_cells_are_editable = True
          End If
        End If

        .Column(GRID_CURRENCY_COLUMN_QUANTITY).Editable = Me.m_cells_are_editable
        .Column(GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).Editable = Me.m_cells_are_editable

        If Not _empty_cells Then
          Continue For
        End If

        If Not Me.m_cage_cls.IsCountR OrElse Not _same_iso_code Then

          _iso_code = grd.Cell(0, GRID_CURRENCY_COLUMN_ISO_CODE).Value
          _currency_type = grd.Cell(0, GRID_CURRENCY_COLUMN_CURRENCY_TYPE).Value

          If _empty_cells And Not FeatureChips.IsCurrencyExchangeTypeChips(_currency_type) Then
            For _idx_row = 0 To .NumRows - 1
              .Cell(_idx_row, GRID_CURRENCY_COLUMN_QUANTITY).Value = String.Empty
              .Cell(_idx_row, GRID_CURRENCY_COLUMN_TOTAL).Value = String.Empty
              .Cell(_idx_row, GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).Value = String.Empty

            Next

          End If

          If Not FeatureChips.IsCurrencyExchangeTypeChips(_currency_type) Then
            Call Me.RefreshTotalGrid(True, _iso_code, _currency_type)
          End If

          Call Me.RefreshTotalGrid(False, _iso_code, _currency_type)

        End If
      End With
    Next

  End Sub

  Private Function CheckAreValuesOnCurrencyGrids(Optional ByVal IsoCode As String = "") As Boolean
    Dim _idx_row As Integer
    Dim _grids As List(Of uc_grid)

    _grids = Me.GetCurrencyGrids()

    For Each grd As uc_grid In _grids
      With grd
        If IsoCode <> "" And .Parent.Text = IsoCode Then
          Continue For
        Else
          For _idx_row = 0 To .NumRows - 1
            If Not .Cell(_idx_row, GRID_CURRENCY_COLUMN_QUANTITY).Value = String.Empty Or
              Not .Cell(_idx_row, GRID_CURRENCY_COLUMN_TOTAL_WITH_SIMBOL).Value = String.Empty Or
             (Not .Cell(_idx_row, GRID_CURRENCY_COLUMN_TOTAL).Value = String.Empty And Not .Cell(_idx_row, GRID_CURRENCY_COLUMN_TOTAL).Value = "0") Then
              Return True
            End If
          Next
        End If
      End With
    Next

    Return False
  End Function

  Private Function GetCurrencyGrids() As List(Of uc_grid)
    Dim _grids As List(Of uc_grid)
    Dim _dg As uc_grid
    Dim _tab As TabPage

    _grids = New List(Of uc_grid)
    For Each ctl As Control In Me.tb_currency.Controls

      If Not TypeOf ctl Is TabPage Then
        Continue For
      End If

      If ctl.Name = TABNAME_PREFIX & Cage.CHIPS_ISO_CODE _
        Or FeatureChips.IsCurrencyExchangeTypeChips(ctl.Tag.ToString().Substring(ISO_CODE_LENGTH, ctl.Tag.ToString().Length - ISO_CODE_LENGTH)) Then
        Continue For
      End If

      _tab = TryCast(ctl, TabPage)
      For Each grd As Control In _tab.Controls

        If Not TypeOf grd Is uc_grid Then
          Continue For
        End If
        _dg = TryCast(grd, uc_grid)
        _grids.Add(_dg)
      Next
    Next
    Return _grids
  End Function

  Private Function GetAllCurrencyGrids() As List(Of uc_grid)
    Dim _grid_list As List(Of uc_grid)
    Dim _tab_page_value As TabPage
    Dim _grid As uc_grid

    _grid_list = New List(Of uc_grid)

    For Each tab_dic_key_value As KeyValuePair(Of CurrencyIsoType, TabPage) In m_currency_tabs_dictionary
      _tab_page_value = tab_dic_key_value.Value
      For Each tab_control As Control In _tab_page_value.Controls
        If Not TypeOf tab_control Is uc_grid Then
          Continue For
        End If
        _grid = TryCast(tab_control, uc_grid)
        _grid_list.Add(_grid)
      Next
    Next

    Return _grid_list
  End Function

  Private Sub CloseNoteCounter()

    If m_scanner_manager IsNot Nothing Then
      m_scanner_manager.CloseScanner()
      RemoveHandler m_scanner_manager.NoteCountReceivedEvent, AddressOf CatchMessageFromScanner
      tmr_note_counter_working.Stop()
    End If

  End Sub

  Private Class DeletedCurrencys
    Public IsoCode As String
    Public IsoType As Integer
    Public CurrencyId As Integer
    Public Import As Double
    Public Used As Boolean

    Public Shared Function PendingDelete(ByVal IsoCode As String, ByVal Coleccion As System.Collections.ObjectModel.Collection(Of DeletedCurrencys)) As Double
      If Coleccion IsNot Nothing Then
        Dim _aux As Double = 0
        For Each el As DeletedCurrencys In Coleccion
          If Not el.Used And el.IsoCode = IsoCode Then
            _aux = _aux + el.Import
          End If
        Next
        Return _aux
      End If
    End Function

    Public Shared Function SearchDenomination(ByVal Denomination As Integer, ByVal IsoCode As String, ByVal IsoType As Int32, ByVal Coleccion As System.Collections.ObjectModel.Collection(Of DeletedCurrencys)) As Double
      If Not Coleccion Is Nothing Then
        Dim _aux As Double = 0
        For Each el As DeletedCurrencys In Coleccion
          If el.CurrencyId = Denomination And el.IsoCode = IsoCode And el.IsoType = IsoType Then
            el.Used = True
            Return el.Import
          End If
        Next
      End If
      Return 0
    End Function



  End Class

End Class