'-------------------------------------------------------------------
' Copyright � 2007-2009 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_cashier_sessions
' DESCRIPTION:   This screen allows to view cashier sessions:
'                           - between two dates 
'                           - for one or all cashiers
'                           - for one or all users
'                           - for one or all movements types
' AUTHOR:        Agust� Poch
' CREATION DATE: 06-SEP-2007
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 06-SEP-2007  APB    Initial version
' 27-FEB-2012  RCI    In SELECT, when calculating SESS_NO_REDEEMABLE and PROMO_NO_REDEEMABLE values,
'                     take in account movements of type CoverCoupon.
' 01-MAR-2012  MPO    Optimize query
' 23-MAR-2012  MPO    fix bug: m_str_where not filled
' 13-JUN-2012  SSC    Added new movements for Gifts/Points
' 01-AUG-2012  DDM    Fixed Bug #327: Add variable for check the company B when printing to Excel
' 07-AUG-2012  JCM    Added Cashier Sesion Sales Limit, total sales. Cashier Sesion all MB Sales Limit, all MB credit sales reserved
' 16-AUG-2012  XCD    Added show unsubscribed to filter users 
' 20-SEP-2012  RRB    Added columns: Tax Returning and Decimal Rounding
' 11-OCT-2012  RRB    Remove the calls to GUI_FormatCurrency function in SetupRow
' 12-DEC-2012  ICS    Added columns: Deposits - Withdrawals and Inputs - Outputs
' 21-FEB-2013  MPO & JAB Improved display columns.
' 26-FEB-2013  ICS    Fixed Bug #614: When Select button is disabled, double-click must not work
' 16-APR-2013  DMR    Changing cashier name for its alias
' 25-APR-2013  RBG    #746 It makes no sense to work in this form with multiselection
' 30-APR-2013  QMP    Added cashier session status "Pending Closing"
' 30-MAY-2013  ICS    Fixed Bug #810: Changed the query of GUI_FilterGetSqlQuery()
' 06-JUN-2013  AMF    Fix bug #824
' 11-JUN-2013  DHA    Fixed Bug #830: Changed the length from the field 'Nombre Completo'
' 19-JUL-2013  LEM    Added new option to search by cashier's movements date
' 26-JUL-2013  ICS    Added new columns regarding to Currency Exchange operations
' 27-AUG-2013  FBA    Added control for new movement Recharge for points, Cards and Currency exchange (split1, split2)
' 29-AUG-2013  DRV    Fixed Bug WIG-169
' 03-SEP-2013  RCI    Fixed Bug WIG-183: Recharge for points: invalid cashier movements
' 12-SEP-2013  ICS    Added suport for Check recharges
' 17-SEP-2013  ICS    Hide currency columns if they don't have values
' 11-OCT-2013  DLL & RCI     UNR's Taxes
' 16-OCT-2013  DLL    Organize columns
' 21-OCT-2013  DLL    Added new columns
' 31-OCT-2013  DLL    Fixed Bug WIG-361
' 02-DEC-2013  AMF    Hide company in TITO mode
' 31-DEC-2013  DLL    Fixed Bug WIGOSTITO-943: Show columns Balance, Collected, Difference when m_is_tito_mode = true
' 02-JAN-2014  RCI    Added support for casino chips
' 09-JAN-2014  DHA    Added control to hide system sessions
' 27-JAN-2014  RCI    Added CashIn Tax info (Movement CASH_IN_TAX_SPLIT1: 142)
' 10-FEB-2014  DRV    Removed all references to frm_cashier_collection_edit
' 21-FEB-2014  RMS    Fixed Bug WIG-665: Added Mobile Bank, Note Acceptor and Points to Redeemable movements in sum_input calculations.
' 25-FEB-2014  JAB    Added Cage session data.
' 11-MAR-2014  RMS    Changed to use historical data when query sessions by openning date
' 12-MAR-2014  RMS    Fixed Bug WIG-718: Render Promotions - Non-Redimable to Redimable for each session
'                     Corrected some columns visibility - Prizes in Kind 1,2 , Taxes Cash-In, Chips,...
'                     Fixed Bug WIG-721: Fixed system user query when checkbox disabled but checked 
' 18-MAR-2014  DLL    Fixed Bug WIG-744: System sessions are not filter when click in report
' 18-MAR-2014  LEM    Hide Company filter when company B are disabled or TITOMode
' 20-MAR-2014  DLL    Fixed Bug WIG-754: Total row can show detail info
' 20-MAR-2014  RMS    Added Differences columns between session-cashier-mb
' 02-APR-2014  RMS    Fixed Bug WIG-792: Fixed column CASH_IN_TAX_SPLIT1 value
' 08-APR-2014  LEM    Fixed Bug WIG-808: Wrong label for company B when filter both selected 
' 09-APR-2014  RMS    Fixed Bug WIG-813: Cashier Difference in positive when collected is less than expected.
' 15-APR-2014  LEM    Fixed Bug WIG-832: Do not show B columns in Excel 
' 20-MAY-2014  CCG    Fixed Bug WIG-932: Select a Cage session for the later close cashier/collections operations.
' 21-MAY-2014  DRV    Fixed Bug WIG-934: Exception clicking details or BM when there is no data in the grid
' 04-JUN-2014  DHA    Fixed Bug WIG-999: Hide Redeemable as cashout when is company b selected
' 10-JUN-2014  DLL    Fixed Bug WIG-1024: Incorrect messagen when cash balance = 0 and game profit > 0
' 16-JUL-2014  DRV    Added Chip Sale Register column
' 22-SEP-2014  DLL    Added Cash Advance column
' 03-NOV-2014  JPJ    Fixed Bug WIGOSTITO-1258: Mobile bank sales limit column and mobile bank button have to be hidden when in TITO mode
' 20-NOV-2014  LEM    Only show expected column for bank card, check, chips and currencies when searching by movements date. Rename to "Balance".
' 20-NOV-2014  LEM    Fixed Bug WIG-1680: Shows wrong Chips information (Expected, Collected, Difference).
' 16-DEC-2014  JPJ    Fixed Bug WIG-1837: Cash advance commissions are not taken into account in their respective columns.
' 16-FEB-2015  ANM    Fixed Bug WIG-1975: Text custom button from Report to Global report
' 09-APR-2015  RMS    Gaming Day
' 15-APR-2015  JMV    Fixed Bug TFS-1086: Error message in cashier sessions
' 20-APR-2015  DLL    Fixed Bug WIG-2225: Error Excel exportation without data
' 07-MAY-2015  JBC    Fixed Bug WIG-2287: GamingDay disabled showed GamingDay filter.
' 28-JAN-2016  FOS    Product Backlog Item 7885: Add currency combo floor dual currency
' 09-FEB-2016  DHA    Product Backlog Item 9042: Floor Dual Currency
' 02-MAR-2016  DDS    Fixed bug 10157: 'Recargas con tarjeta' -> 'Operaciones con tarjeta bancaria'
' 03-MAR-2016  JML    Product Backlog Item 10085:Winpot - Tax Provisions: Include movement reports
' 19-APR-2016  RGR    Product Backlog Item 11297: Codere - Tax Custody: Reports and cash movements and account
' 27-APR-2016  RGR    Product Backlog Item 11298: Codere - Tax Custody: Modify reports GUI Income and Return
' 28-APR-2016  FAV    Fixed Bug 12323: The player card shows a valid value for company B. 
' 19-JUL-2016  FOS    Product Backlog Item 15068: Gaming tables (Fase 4): Adapt reports for the multi currency chips
' 09-AGO-2016  JML    Product Backlog Item 15908: Gaming tables (Fase 5): Change nls "Retiros" & "Depositos" by "Fills" & "Credits"
' 17-AUG-2016  RAB    Product Backlog Item 15914: Gaming tables (Phase 5): Order summary box and voucher currency
' 23-SEP-2016  FOS    Fixed Bug 18110: Duplicate Columns
' 26-SEP-2016  EOR    PBI 17468: Televisa - Service Charge: Cashier Movements
' 28-SEP-2016  ESE    Fixed Bug 17623 Add columns to report 'Cash Session'
' 06-OCT-2016  ETP    Fixed Bug 18720: BankCard.Conciliate colums apears when is not configured
' 05-OCT-2016  EOR    PBI 17963: Televisa - Service Charge: Modifications Voucher/Report/Settings
' 26-JAN-2017  JML    Bug 23480:WigosGUI: Errors in cashier sessions report on balance, delivered and difference.
' 03-FEB-2017  FOS    Bug 13296:cage result in company A is different than the same value in Statistics balance report
' 06-MAR-2017  FOS    Bug 24403:Error in totalizer Company A/B
' 05-APR-2017  FOS    Bug 26570:Error in NLS
' 13-MAR-2017  FGB    PBI 25736: Third TAX - Cash sessions
' 19-JUN-2017  EOR    Bug 28187: WIGOS-2976 - The report "Sesiones de caja" is displaying a wrong string in one of the columns
' 22-AUG-2017  EOR    Bug 29430: WIGOS-3822 Reset button is not enabled all selection criteria in Cash Sessions screen
' 25-OCT-2017  RAB    Bug 30408:WIGOS-5967 [Ticket #9627] v03.005.0112QA - El reporte de sesiones de caja no muestra el apartado de Recargas ni Vouchers
' 19-DIC-2017  RAB    Bug 30942:WIGOS-6509 PinPad: wrong cashier movements when some vouchers are withdrawn
' 09-JUL-2018  DLM    Bug 33535:WIGOS-13440 Credit card wrong information in Cash session screen
'-------------------------------------------------------------------------
Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports System.Data.SqlClient
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Text
Imports WSI.Common

Public Class frm_cashier_sessions
  Inherits frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents opt_one_cashier As System.Windows.Forms.RadioButton
  Friend WithEvents opt_all_cashiers As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_cashier As GUI_Controls.uc_combo
  Friend WithEvents gb_user As System.Windows.Forms.GroupBox
  Friend WithEvents opt_one_user As System.Windows.Forms.RadioButton
  Friend WithEvents opt_all_users As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_user As GUI_Controls.uc_combo
  Friend WithEvents fra_session_state As System.Windows.Forms.GroupBox
  Friend WithEvents chk_session_closed As System.Windows.Forms.CheckBox
  Friend WithEvents chk_session_open As System.Windows.Forms.CheckBox
  Friend WithEvents gb_company As System.Windows.Forms.GroupBox
  Friend WithEvents opt_company_a As System.Windows.Forms.RadioButton
  Friend WithEvents opt_company_both As System.Windows.Forms.RadioButton
  Friend WithEvents opt_company_b As System.Windows.Forms.RadioButton
  Friend WithEvents chk_show_all As System.Windows.Forms.CheckBox
  Friend WithEvents chk_session_pending As System.Windows.Forms.CheckBox
  Friend WithEvents opt_opening_session As System.Windows.Forms.RadioButton
  Friend WithEvents opt_movement_session As System.Windows.Forms.RadioButton
  Friend WithEvents chk_not_show_system_sessions As System.Windows.Forms.CheckBox
  Friend WithEvents opt_movement_gaming_day As System.Windows.Forms.RadioButton
  Public WithEvents cmb_currencies As GUI_Controls.uc_combo

  Friend WithEvents lbl_currency As System.Windows.Forms.Label

  Friend WithEvents gb_cashier As System.Windows.Forms.GroupBox
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_cashier_sessions))
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.opt_movement_gaming_day = New System.Windows.Forms.RadioButton()
    Me.opt_opening_session = New System.Windows.Forms.RadioButton()
    Me.opt_movement_session = New System.Windows.Forms.RadioButton()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.gb_cashier = New System.Windows.Forms.GroupBox()
    Me.cmb_currencies = New GUI_Controls.uc_combo()
    Me.opt_one_cashier = New System.Windows.Forms.RadioButton()
    Me.lbl_currency = New System.Windows.Forms.Label()
    Me.opt_all_cashiers = New System.Windows.Forms.RadioButton()
    Me.cmb_cashier = New GUI_Controls.uc_combo()
    Me.gb_user = New System.Windows.Forms.GroupBox()
    Me.chk_show_all = New System.Windows.Forms.CheckBox()
    Me.opt_one_user = New System.Windows.Forms.RadioButton()
    Me.opt_all_users = New System.Windows.Forms.RadioButton()
    Me.cmb_user = New GUI_Controls.uc_combo()
    Me.fra_session_state = New System.Windows.Forms.GroupBox()
    Me.chk_session_pending = New System.Windows.Forms.CheckBox()
    Me.chk_session_closed = New System.Windows.Forms.CheckBox()
    Me.chk_session_open = New System.Windows.Forms.CheckBox()
    Me.gb_company = New System.Windows.Forms.GroupBox()
    Me.opt_company_b = New System.Windows.Forms.RadioButton()
    Me.opt_company_a = New System.Windows.Forms.RadioButton()
    Me.opt_company_both = New System.Windows.Forms.RadioButton()
    Me.chk_not_show_system_sessions = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_cashier.SuspendLayout()
    Me.gb_user.SuspendLayout()
    Me.fra_session_state.SuspendLayout()
    Me.gb_company.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.chk_not_show_system_sessions)
    Me.panel_filter.Controls.Add(Me.gb_company)
    Me.panel_filter.Controls.Add(Me.fra_session_state)
    Me.panel_filter.Controls.Add(Me.gb_user)
    Me.panel_filter.Controls.Add(Me.gb_cashier)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Size = New System.Drawing.Size(1178, 185)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_cashier, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_user, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.fra_session_state, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_company, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_not_show_system_sessions, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 189)
    Me.panel_data.Size = New System.Drawing.Size(1178, 460)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1172, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1172, 4)
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.opt_movement_gaming_day)
    Me.gb_date.Controls.Add(Me.opt_opening_session)
    Me.gb_date.Controls.Add(Me.opt_movement_session)
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(8, 8)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(258, 130)
    Me.gb_date.TabIndex = 0
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'opt_movement_gaming_day
    '
    Me.opt_movement_gaming_day.Location = New System.Drawing.Point(14, 55)
    Me.opt_movement_gaming_day.Name = "opt_movement_gaming_day"
    Me.opt_movement_gaming_day.Size = New System.Drawing.Size(210, 17)
    Me.opt_movement_gaming_day.TabIndex = 4
    Me.opt_movement_gaming_day.Text = "xSearchByGamingDay"
    '
    'opt_opening_session
    '
    Me.opt_opening_session.Location = New System.Drawing.Point(14, 15)
    Me.opt_opening_session.Name = "opt_opening_session"
    Me.opt_opening_session.Size = New System.Drawing.Size(210, 17)
    Me.opt_opening_session.TabIndex = 0
    Me.opt_opening_session.Text = "xSearchByOpeningDate"
    '
    'opt_movement_session
    '
    Me.opt_movement_session.Location = New System.Drawing.Point(14, 35)
    Me.opt_movement_session.Name = "opt_movement_session"
    Me.opt_movement_session.Size = New System.Drawing.Size(210, 17)
    Me.opt_movement_session.TabIndex = 1
    Me.opt_movement_session.Text = "xSearchByMovementsDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 100)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(240, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 3
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 76)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(240, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 2
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'gb_cashier
    '
    Me.gb_cashier.Controls.Add(Me.cmb_currencies)
    Me.gb_cashier.Controls.Add(Me.opt_one_cashier)
    Me.gb_cashier.Controls.Add(Me.lbl_currency)
    Me.gb_cashier.Controls.Add(Me.opt_all_cashiers)
    Me.gb_cashier.Controls.Add(Me.cmb_cashier)
    Me.gb_cashier.Location = New System.Drawing.Point(272, 8)
    Me.gb_cashier.Name = "gb_cashier"
    Me.gb_cashier.Size = New System.Drawing.Size(272, 72)
    Me.gb_cashier.TabIndex = 2
    Me.gb_cashier.TabStop = False
    Me.gb_cashier.Text = "xCashier"
    '
    'cmb_currencies
    '
    Me.cmb_currencies.AllowUnlistedValues = False
    Me.cmb_currencies.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.cmb_currencies.AutoCompleteMode = False
    Me.cmb_currencies.IsReadOnly = False
    Me.cmb_currencies.Location = New System.Drawing.Point(204, 42)
    Me.cmb_currencies.Name = "cmb_currencies"
    Me.cmb_currencies.SelectedIndex = -1
    Me.cmb_currencies.Size = New System.Drawing.Size(60, 24)
    Me.cmb_currencies.SufixText = "Sufix Text"
    Me.cmb_currencies.SufixTextVisible = True
    Me.cmb_currencies.TabIndex = 12
    Me.cmb_currencies.TextCombo = Nothing
    Me.cmb_currencies.TextVisible = False
    Me.cmb_currencies.TextWidth = 0
    '
    'opt_one_cashier
    '
    Me.opt_one_cashier.Location = New System.Drawing.Point(8, 14)
    Me.opt_one_cashier.Name = "opt_one_cashier"
    Me.opt_one_cashier.Size = New System.Drawing.Size(72, 24)
    Me.opt_one_cashier.TabIndex = 0
    Me.opt_one_cashier.Text = "xOne"
    '
    'lbl_currency
    '
    Me.lbl_currency.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.lbl_currency.AutoSize = True
    Me.lbl_currency.Location = New System.Drawing.Point(135, 48)
    Me.lbl_currency.Name = "lbl_currency"
    Me.lbl_currency.Size = New System.Drawing.Size(67, 13)
    Me.lbl_currency.TabIndex = 11
    Me.lbl_currency.Text = "xCurrency"
    '
    'opt_all_cashiers
    '
    Me.opt_all_cashiers.Location = New System.Drawing.Point(8, 40)
    Me.opt_all_cashiers.Name = "opt_all_cashiers"
    Me.opt_all_cashiers.Size = New System.Drawing.Size(64, 24)
    Me.opt_all_cashiers.TabIndex = 2
    Me.opt_all_cashiers.Text = "xAll"
    '
    'cmb_cashier
    '
    Me.cmb_cashier.AllowUnlistedValues = False
    Me.cmb_cashier.AutoCompleteMode = False
    Me.cmb_cashier.IsReadOnly = False
    Me.cmb_cashier.Location = New System.Drawing.Point(80, 14)
    Me.cmb_cashier.Name = "cmb_cashier"
    Me.cmb_cashier.SelectedIndex = -1
    Me.cmb_cashier.Size = New System.Drawing.Size(184, 24)
    Me.cmb_cashier.SufixText = "Sufix Text"
    Me.cmb_cashier.SufixTextVisible = True
    Me.cmb_cashier.TabIndex = 1
    Me.cmb_cashier.TextCombo = Nothing
    Me.cmb_cashier.TextVisible = False
    Me.cmb_cashier.TextWidth = 0
    '
    'gb_user
    '
    Me.gb_user.Controls.Add(Me.chk_show_all)
    Me.gb_user.Controls.Add(Me.opt_one_user)
    Me.gb_user.Controls.Add(Me.opt_all_users)
    Me.gb_user.Controls.Add(Me.cmb_user)
    Me.gb_user.Location = New System.Drawing.Point(552, 8)
    Me.gb_user.Name = "gb_user"
    Me.gb_user.Size = New System.Drawing.Size(272, 72)
    Me.gb_user.TabIndex = 4
    Me.gb_user.TabStop = False
    Me.gb_user.Text = "xUser"
    '
    'chk_show_all
    '
    Me.chk_show_all.AutoSize = True
    Me.chk_show_all.Location = New System.Drawing.Point(81, 45)
    Me.chk_show_all.Name = "chk_show_all"
    Me.chk_show_all.Size = New System.Drawing.Size(102, 17)
    Me.chk_show_all.TabIndex = 3
    Me.chk_show_all.Text = "chk_show_all"
    Me.chk_show_all.UseVisualStyleBackColor = True
    '
    'opt_one_user
    '
    Me.opt_one_user.Location = New System.Drawing.Point(8, 14)
    Me.opt_one_user.Name = "opt_one_user"
    Me.opt_one_user.Size = New System.Drawing.Size(72, 24)
    Me.opt_one_user.TabIndex = 0
    Me.opt_one_user.Text = "xOne"
    '
    'opt_all_users
    '
    Me.opt_all_users.Location = New System.Drawing.Point(8, 40)
    Me.opt_all_users.Name = "opt_all_users"
    Me.opt_all_users.Size = New System.Drawing.Size(64, 24)
    Me.opt_all_users.TabIndex = 2
    Me.opt_all_users.Text = "xAll"
    '
    'cmb_user
    '
    Me.cmb_user.AllowUnlistedValues = False
    Me.cmb_user.AutoCompleteMode = False
    Me.cmb_user.IsReadOnly = False
    Me.cmb_user.Location = New System.Drawing.Point(78, 14)
    Me.cmb_user.Name = "cmb_user"
    Me.cmb_user.SelectedIndex = -1
    Me.cmb_user.Size = New System.Drawing.Size(184, 24)
    Me.cmb_user.SufixText = "Sufix Text"
    Me.cmb_user.SufixTextVisible = True
    Me.cmb_user.TabIndex = 1
    Me.cmb_user.TextCombo = Nothing
    Me.cmb_user.TextVisible = False
    Me.cmb_user.TextWidth = 0
    '
    'fra_session_state
    '
    Me.fra_session_state.Controls.Add(Me.chk_session_pending)
    Me.fra_session_state.Controls.Add(Me.chk_session_closed)
    Me.fra_session_state.Controls.Add(Me.chk_session_open)
    Me.fra_session_state.Location = New System.Drawing.Point(832, 8)
    Me.fra_session_state.Name = "fra_session_state"
    Me.fra_session_state.Size = New System.Drawing.Size(194, 81)
    Me.fra_session_state.TabIndex = 5
    Me.fra_session_state.TabStop = False
    Me.fra_session_state.Text = "xState"
    '
    'chk_session_pending
    '
    Me.chk_session_pending.Location = New System.Drawing.Point(6, 59)
    Me.chk_session_pending.Name = "chk_session_pending"
    Me.chk_session_pending.Size = New System.Drawing.Size(150, 16)
    Me.chk_session_pending.TabIndex = 2
    Me.chk_session_pending.Text = "xPending Closing"
    '
    'chk_session_closed
    '
    Me.chk_session_closed.Location = New System.Drawing.Point(6, 39)
    Me.chk_session_closed.Name = "chk_session_closed"
    Me.chk_session_closed.Size = New System.Drawing.Size(150, 16)
    Me.chk_session_closed.TabIndex = 1
    Me.chk_session_closed.Text = "xClosed"
    '
    'chk_session_open
    '
    Me.chk_session_open.Location = New System.Drawing.Point(6, 19)
    Me.chk_session_open.Name = "chk_session_open"
    Me.chk_session_open.Size = New System.Drawing.Size(150, 16)
    Me.chk_session_open.TabIndex = 0
    Me.chk_session_open.Text = "xOpen"
    '
    'gb_company
    '
    Me.gb_company.Controls.Add(Me.opt_company_b)
    Me.gb_company.Controls.Add(Me.opt_company_a)
    Me.gb_company.Controls.Add(Me.opt_company_both)
    Me.gb_company.Location = New System.Drawing.Point(272, 80)
    Me.gb_company.Name = "gb_company"
    Me.gb_company.Size = New System.Drawing.Size(552, 90)
    Me.gb_company.TabIndex = 3
    Me.gb_company.TabStop = False
    Me.gb_company.Text = "xCompany"
    '
    'opt_company_b
    '
    Me.opt_company_b.Location = New System.Drawing.Point(10, 60)
    Me.opt_company_b.Name = "opt_company_b"
    Me.opt_company_b.Size = New System.Drawing.Size(532, 23)
    Me.opt_company_b.TabIndex = 2
    Me.opt_company_b.Text = "xCompanyB"
    Me.opt_company_b.UseVisualStyleBackColor = True
    '
    'opt_company_a
    '
    Me.opt_company_a.Location = New System.Drawing.Point(10, 40)
    Me.opt_company_a.Name = "opt_company_a"
    Me.opt_company_a.Size = New System.Drawing.Size(532, 21)
    Me.opt_company_a.TabIndex = 1
    Me.opt_company_a.Text = "xCompanyA"
    Me.opt_company_a.UseVisualStyleBackColor = True
    '
    'opt_company_both
    '
    Me.opt_company_both.Checked = True
    Me.opt_company_both.Location = New System.Drawing.Point(10, 20)
    Me.opt_company_both.Name = "opt_company_both"
    Me.opt_company_both.Size = New System.Drawing.Size(376, 17)
    Me.opt_company_both.TabIndex = 0
    Me.opt_company_both.TabStop = True
    Me.opt_company_both.Text = "xBoth"
    Me.opt_company_both.UseVisualStyleBackColor = True
    '
    'chk_not_show_system_sessions
    '
    Me.chk_not_show_system_sessions.AutoSize = True
    Me.chk_not_show_system_sessions.Location = New System.Drawing.Point(8, 150)
    Me.chk_not_show_system_sessions.Name = "chk_not_show_system_sessions"
    Me.chk_not_show_system_sessions.Size = New System.Drawing.Size(176, 17)
    Me.chk_not_show_system_sessions.TabIndex = 1
    Me.chk_not_show_system_sessions.Text = "xNotShowSystemSessions"
    Me.chk_not_show_system_sessions.UseVisualStyleBackColor = True
    '
    'frm_cashier_sessions
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(1186, 653)
    Me.Name = "frm_cashier_sessions"
    Me.Text = "frm_cashier_sessions"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gb_cashier.ResumeLayout(False)
    Me.gb_cashier.PerformLayout()
    Me.gb_user.ResumeLayout(False)
    Me.gb_user.PerformLayout()
    Me.fra_session_state.ResumeLayout(False)
    Me.gb_company.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Constants "

  ' DB Columns
  Private Const SQL_COLUMN_SESSION_ID As Integer = 0
  Private Const SQL_COLUMN_SESSION_NAME As Integer = 1
  Private Const SQL_COLUMN_USER_ID As Integer = 2
  Private Const SQL_COLUMN_USER_NAME As Integer = 3
  Private Const SQL_COLUMN_OPENING_DATE As Integer = 4
  Private Const SQL_COLUMN_CLOSE_DATE As Integer = 5
  Private Const SQL_COLUMN_CASHIER_ID As Integer = 6
  Private Const SQL_COLUMN_CASHIER_NAME As Integer = 7
  Private Const SQL_COLUMN_STATUS As Integer = 8

  Private Const SQL_COLUMN_CS_LIMIT_SALE As Integer = 9
  Private Const SQL_COLUMN_MB_LIMIT_SALE As Integer = 10

  Private Const SQL_COLUMN_SESSION_COLLECTED As Integer = 11

  Private Const SQL_COLUMN_CGS_CAGE_SESSION_ID As Integer = 12
  Private Const SQL_COLUMN_CGS_SESSION_NAME As Integer = 13

  Private Const SQL_COLUMN_GAMING_DAY As Integer = 14

  'ESE 28-SEP-2016
  Private Const SQL_COLUMN_MONEY_RECHARGE_AMOUNT As Integer = 15
  Private Const SQL_COLUMN_BANK_CARD_RECHARGE_AMOUNT As Integer = 16
  Private Const SQL_COLUMN_TOTAL_VOUCHERS As Integer = 17
  Private Const SQL_COLUMN_DELIVERED_VOUCHERS As Integer = 18
  Private Const SQL_COLUMN_TOTAL_VOUCHERS_AMOUNT As Integer = 19

  Private Const SQL_COLUMN_SESSION_DATA As Integer = 20

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  ' Cash Session
  Private Const GRID_COLUMN_SESSION_HEADER As Integer = 1
  Private Const GRID_COLUMN_SESSION_ID As Integer = GRID_COLUMN_SESSION_HEADER        '1
  Private Const GRID_COLUMN_SESSION_NAME As Integer = GRID_COLUMN_SESSION_HEADER + 1  '2
  Private Const GRID_COLUMN_USER_ID As Integer = GRID_COLUMN_SESSION_HEADER + 2       '3
  Private Const GRID_COLUMN_USER_NAME As Integer = GRID_COLUMN_SESSION_HEADER + 3     '4
  Private Const GRID_COLUMN_GAMING_DAY As Integer = GRID_COLUMN_SESSION_HEADER + 4    '5
  Private Const GRID_COLUMN_OPENING_DATE As Integer = GRID_COLUMN_SESSION_HEADER + 5  '6
  Private Const GRID_COLUMN_CLOSE_DATE As Integer = GRID_COLUMN_SESSION_HEADER + 6    '7
  Private Const GRID_COLUMN_CASHIER_ID As Integer = GRID_COLUMN_SESSION_HEADER + 7    '8
  Private Const GRID_COLUMN_CASHIER_NAME As Integer = GRID_COLUMN_SESSION_HEADER + 8  '9
  Private Const GRID_COLUMN_STATUS As Integer = GRID_COLUMN_SESSION_HEADER + 9        '10

  'Cash Result
  Private Const GRID_COLUMN_CASH_RESULT_HEADER As Integer = 11
  Private Const GRID_COLUMN_CARD_IN As Integer = GRID_COLUMN_CASH_RESULT_HEADER  '11
  Private Const GRID_COLUMN_CARD_OUT As Integer = GRID_COLUMN_CASH_RESULT_HEADER + 1  '12
  Private Const GRID_COLUMN_INPUTS_LESS_OUTPUTS As Integer = GRID_COLUMN_CASH_RESULT_HEADER + 2  '13
  Private Const GRID_COLUMN_TAXES As Integer = GRID_COLUMN_CASH_RESULT_HEADER + 3  '14
  Private Const GRID_COLUMN_SESSION_RESULT As Integer = GRID_COLUMN_CASH_RESULT_HEADER + 4  '15

  'Cash Balance
  Private Const GRID_COLUMN_CASH_BALANCE_HEADER As Integer = 16
  Private Const GRID_COLUMN_DEPOSITS As Integer = GRID_COLUMN_CASH_BALANCE_HEADER  '16
  Private Const GRID_COLUMN_WITHDRAWS As Integer = GRID_COLUMN_CASH_BALANCE_HEADER + 1  '17
  Private Const GRID_COLUMN_DEPOSITS_LESS_WITHDRAWALS As Integer = GRID_COLUMN_CASH_BALANCE_HEADER + 2  '18
  Private Const GRID_COLUMN_CASH_CARD_IN As Integer = GRID_COLUMN_CASH_BALANCE_HEADER + 3  '19
  Private Const GRID_COLUMN_CASH_CARD_OUT As Integer = GRID_COLUMN_CASH_BALANCE_HEADER + 4  '20
  Private Const GRID_COLUMN_CHECK_PAYMENT As Integer = GRID_COLUMN_CASH_BALANCE_HEADER + 5  '21
  Private Const GRID_COLUMN_TOTAL_BANK_CARD As Integer = GRID_COLUMN_CASH_BALANCE_HEADER + 6  '22
  Private Const GRID_COLUMN_TOTAL_CHECK_RECHARGES As Integer = GRID_COLUMN_CASH_BALANCE_HEADER + 7  '23
  Private Const GRID_COLUMN_TOTAL_CURRENCY_EXCHANGE As Integer = GRID_COLUMN_CASH_BALANCE_HEADER + 8  '24

  Private Const GRID_COLUMN_EXPECTED As Integer = GRID_COLUMN_CASH_BALANCE_HEADER + 9  '25
  Private Const GRID_COLUMN_COLLECTED As Integer = GRID_COLUMN_CASH_BALANCE_HEADER + 10  '26
  Private Const GRID_COLUMN_DIFFERENCE_TOTAL As Integer = GRID_COLUMN_CASH_BALANCE_HEADER + 11  '27
  Private Const GRID_COLUMN_DIFFERENCE_CASHIER As Integer = GRID_COLUMN_CASH_BALANCE_HEADER + 12  '28
  Private Const GRID_COLUMN_DIFFERENCE_MB As Integer = GRID_COLUMN_CASH_BALANCE_HEADER + 13  '29
  Private Const GRID_COLUMN_PENDING_MB As Integer = GRID_COLUMN_CASH_BALANCE_HEADER + 14  '30
  Private Const GRID_COLUMN_SHORTFALL_EXCESS_MB As Integer = GRID_COLUMN_CASH_BALANCE_HEADER + 15  '31

  ' Inputs
  Private Const GRID_COLUMN_INPUTS_HEADER As Integer = 32
  Private Const GRID_COLUMN_INPUTS_A As Integer = GRID_COLUMN_INPUTS_HEADER  '32
  Private Const GRID_COLUMN_TAXES_CASH_IN As Integer = GRID_COLUMN_INPUTS_HEADER + 1 '33
  Private Const GRID_COLUMN_TAX_PROVISIONS As Integer = GRID_COLUMN_INPUTS_HEADER + 2 '34
  Private Const GRID_COLUMN_TAX_CUSTODY As Integer = GRID_COLUMN_INPUTS_HEADER + 3 '35
  Private Const GRID_COLUMN_INPUTS_B As Integer = GRID_COLUMN_INPUTS_HEADER + 4 '36
  Private Const GRID_COLUMN_CARD_USAGE As Integer = GRID_COLUMN_INPUTS_HEADER + 5 '37
  Private Const GRID_COLUMN_SERVICE_CHARGE As Integer = GRID_COLUMN_INPUTS_HEADER + 6  '38
  Private Const GRID_COLUMN_COMMISSIONS_BANK_CARD As Integer = GRID_COLUMN_INPUTS_HEADER + 7  '39
  Private Const GRID_COLUMN_COMMISSIONS_CHECK As Integer = GRID_COLUMN_INPUTS_HEADER + 8  '40
  Private Const GRID_COLUMN_COMMISSIONS_CUR_EXCH As Integer = GRID_COLUMN_INPUTS_HEADER + 9  '41

  ' Outputs
  Private Const GRID_COLUMN_OUTPUTS_HEADER As Integer = 42
  Private Const GRID_COLUMN_OUTPUTS_A As Integer = GRID_COLUMN_OUTPUTS_HEADER  '42
  Private Const GRID_COLUMN_OUTPUTS_B As Integer = GRID_COLUMN_OUTPUTS_HEADER + 1  '43
  Private Const GRID_COLUMN_REFUND_CARD_USAGE As Integer = GRID_COLUMN_OUTPUTS_HEADER + 2 '44
  Private Const GRID_COLUMN_TOTAL_PRIZES As Integer = GRID_COLUMN_OUTPUTS_HEADER + 3  '45
  Private Const GRID_COLUMN_TAX_CUSTODY_RETURN As Integer = GRID_COLUMN_OUTPUTS_HEADER + 4 '46
  Private Const GRID_COLUMN_DECIMAL_ROUNDING As Integer = GRID_COLUMN_OUTPUTS_HEADER + 5  '47

  ' Prize
  Private Const GRID_COLUMN_PRIZE_HEADER As Integer = 48
  Private Const GRID_COLUMN_PRIZES As Integer = GRID_COLUMN_PRIZE_HEADER  '48
  Private Const GRID_COLUMN_PRIZES_TAXES As Integer = GRID_COLUMN_PRIZE_HEADER + 1 '49
  Private Const GRID_COLUMN_TAX_RETURNING As Integer = GRID_COLUMN_PRIZE_HEADER + 2 '50
  Private Const GRID_COLUMN_UNR_KIND1 As Integer = GRID_COLUMN_PRIZE_HEADER + 3 '51
  Private Const GRID_COLUMN_UNR_KIND2 As Integer = GRID_COLUMN_PRIZE_HEADER + 4 '52

  ' Recharge for points
  Private Const GRID_COLUMN_RECHARGE_FOR_POINTS_HEADER As Integer = 53
  Private Const GRID_COLUMN_REDEEMABLE_AS_CASHIN As Integer = GRID_COLUMN_RECHARGE_FOR_POINTS_HEADER  '53
  Private Const GRID_COLUMN_REDEEMABLE_AS_CASHOUT As Integer = GRID_COLUMN_RECHARGE_FOR_POINTS_HEADER + 1  '54

  ' Handpays
  Private Const GRID_COLUMN_HANDPAYS_HEADER As Integer = 55
  Private Const GRID_COLUMN_HANDPAYS As Integer = GRID_COLUMN_HANDPAYS_HEADER  '55
  Private Const GRID_COLUMN_CANCELATION_HANDPAYS As Integer = GRID_COLUMN_HANDPAYS_HEADER + 1  '56
  Private Const GRID_COLUMN_TOTAL_HANDPAYS As Integer = GRID_COLUMN_HANDPAYS_HEADER + 2  '57

  ' Promotions
  Private Const GRID_COLUMN_PROMOTIONS_HEADER As Integer = 58
  Private Const GRID_COLUMN_PROMO_REDEEMABLE As Integer = GRID_COLUMN_PROMOTIONS_HEADER  '58
  Private Const GRID_COLUMN_PROMO_NO_REDEEMABLE As Integer = GRID_COLUMN_PROMOTIONS_HEADER + 1  '59
  Private Const GRID_COLUMN_NO_REDEEMABLE_TO_REDEEMABLE As Integer = GRID_COLUMN_PROMOTIONS_HEADER + 2  '60
  Private Const GRID_COLUMN_CANCEL_PROMO_REDEEMABLE As Integer = GRID_COLUMN_PROMOTIONS_HEADER + 3  '61
  Private Const GRID_COLUMN_CANCEL_PROMO_NO_REDEEMABLE As Integer = GRID_COLUMN_PROMOTIONS_HEADER + 4  '62

  ' Expirations
  Private Const GRID_COLUMN_EXPIRATIONS_HEADER As Integer = 63
  Private Const GRID_COLUMN_EXPIRATIONS_DEV As Integer = GRID_COLUMN_EXPIRATIONS_HEADER  '63
  Private Const GRID_COLUMN_EXPIRATIONS_PRIZE As Integer = GRID_COLUMN_EXPIRATIONS_HEADER + 1  '64
  Private Const GRID_COLUMN_EXPIRATIONS_PROMO_NR As Integer = GRID_COLUMN_EXPIRATIONS_HEADER + 2 '65

  ' Others
  Private Const GRID_COLUMN_CS_LIMIT_SALE As Integer = 66

  Private Const GRID_COLUMN_MB_LIMIT_SALE As Integer = 67

  Private Const GRID_COLUMN_CASH_ADVANCE As Integer = 68

  Private Const GRID_COLUMN_TOTAL_CHIP_SALE_REGISTER As Integer = 69

  ' Cage session data
  Private Const GRID_COLUMN_CGS_SESSION_NAME As Integer = 70

  Private Const GRID_COLUMN_BANK_CARD_CREDIT_BALANCE As Integer = 71
  Private Const GRID_COLUMN_BANK_CARD_DEBIT_BALANCE As Integer = 72
  Private Const GRID_COLUMN_BANK_CARD_BALANCE As Integer = 73
  Private Const GRID_COLUMN_BANK_CARD_COLLECTED As Integer = 74
  Private Const GRID_COLUMN_BANK_CARD_DIFFERENCE As Integer = 75

  Private Const GRID_COLUMN_CHECK_BALANCE As Integer = 76
  Private Const GRID_COLUMN_CHECK_COLLECTED As Integer = 77
  Private Const GRID_COLUMN_CHECK_DIFFERENCE As Integer = 78

  Private Const GRID_COLUMN_CHIPS_BALANCE As Integer = 79
  Private Const GRID_COLUMN_CHIPS_COLLECTED As Integer = 80
  Private Const GRID_COLUMN_CHIPS_DIFFERENCE As Integer = 81

  'ESE 28-SEP-2016
  'Bank card transactions
  Private Const GRID_COLUMN_CASH_RECHARGE As Integer = 82
  Private Const GRID_COLUMN_CREDIT_CARD_RECHARGE As Integer = 83
  Private Const GRID_COLUMN_VOUCHERS_COUNT As Integer = 84
  Private Const GRID_COLUMN_DELIVERED_VOUCHERS As Integer = 85
  Private Const GRID_COLUMN_AMOUNT_DELIVERED_VOUCHERS As Integer = 86

  Private Const GRID_COLUMNS As Integer = 87
  Private Const GRID_HEADER_ROWS As Integer = 2

  ' Width
  Private Const GRID_WIDTH_DATE As Integer = 1700
  Private Const GRID_WIDTH_TIME As Integer = 1300
  Private Const GRID_WIDTH_CASHIER As Integer = 1700
  Private Const GRID_WIDTH_NAME As Integer = 1800
  Private Const GRID_WIDTH_AMOUNT_SHORT As Integer = 1150
  Private Const GRID_WIDTH_AMOUNT_LONG As Integer = 1550
  Private Const GRID_WIDTH_USER As Integer = 1550
  Private Const GRID_WIDTH_SESSION_NAME As Integer = 2700
  Private Const GRID_WIDTH_CARD_TRACK_DATA As Integer = 2000
  Private Const GRID_WIDTH_TAX_RETURNING As Integer = 2200

  Private Const GRID_COLUMN_WIDTH_CGS_SESSION_NAME As Integer = 2700

  ' Constants for Stored Result Tables
  Private Const RESULT_TABLE_MOVEMENTS As Integer = 0
  Private Const RESULT_TABLE_SESSIONS As Integer = 1
  Private Const RESULT_TABLE_CURRENCIES As Integer = 2

#End Region ' Constants

#Region " Members "

  ' Used to avoid data-changed recursive calls when re-painting a data grid
  Private m_refreshing_grid As Boolean

  '
  Private m_national_currency As String
  Private m_split_data As New TYPE_SPLITS

  ' For report filters 
  Private m_cashiers As String
  Private m_users As String
  Private m_filter_from As String
  Private m_filter_to As String
  Private m_session As String
  Private m_state As List(Of String)
  Private m_company As String
  Private m_str_where As String
  Private m_not_show_system_sessions As Boolean
  Private m_both_company_selected As Boolean
  Private m_sessions_from_movements As String
  Private m_date_by As String
  Private m_hidde_system_sessions As String

  Dim m_from_date As Date
  Dim m_to_date As Date
  Dim m_floor_dual_currency As String

  Private m_company_b As Boolean
  Private m_has_data_company_b As Boolean

  Private m_cage_session_id As Long

  ' For currency exchange dynamic columns
  Private m_currency_exchange_iso_codes As List(Of String)
  Private m_currency_exchange_iso_types As List(Of CurrencyIsoType)
  Private m_currency_exchange_iso_description As List(Of String)

  Private m_card_differentiate_type As Boolean

  ' For grid totalizators
  Dim total_deposits As Decimal
  Dim total_withdraws As Decimal
  Dim total_card_in As Decimal
  Dim total_card_out As Decimal
  Dim total_expected As Decimal
  Dim total_collected As Decimal
  Dim total_difference_cashier As Decimal
  Dim total_difference_mb As Decimal
  Dim total_taxes As Decimal
  Dim total_service_charge As Decimal
  Dim total_tax_returning As Decimal
  Dim total_decimal_rounding As Decimal
  Dim total_result As Decimal
  Dim total_promo_no_redeemable As Decimal
  Dim total_promo_redeemable As Decimal
  Dim total_no_redeemable_to_redeemable As Decimal
  Dim total_deposits_less_withdrawals As Decimal
  Dim total_inputs_less_outputs As Decimal
  Dim total_check_payment As Decimal
  Dim total_commissions_cur_exch As Decimal
  Dim total_commissions_bank_card As Decimal
  Dim total_commissions_check As Decimal
  Dim total_bank_card As Decimal
  Dim total_check_recharges As Decimal
  Dim total_chips_sale_register As Decimal
  Dim total_currency_exchange As Decimal
  Dim total_currency_exchange_expected As Decimal()
  Dim total_currency_exchange_collected As Decimal()
  Dim total_currency_exchange_difference As Decimal()
  Dim total_recharge_for_points As Decimal
  Dim total_recharge_for_points_out As Decimal
  Dim total_check_balance As Decimal
  Dim total_check_collected As Decimal
  Dim total_check_difference As Decimal
  Dim total_bank_card_balance As Decimal
  Dim total_bank_card_collected As Decimal
  Dim total_bank_card_difference As Decimal
  Dim total_bank_card_credit As Decimal
  Dim total_bank_card_debit As Decimal
  Dim total_chips_balance As Decimal
  Dim total_chips_collected As Decimal
  Dim total_chips_difference As Decimal

  Dim total_inputs_a As Decimal
  Dim total_inputs_b As Decimal
  Dim total_card_usage As Decimal
  Dim total_outputs_a As Decimal
  Dim total_outputs_b As Decimal
  Dim total_refund_card_usage As Decimal
  Dim total_total_prizes As Decimal
  Dim total_custody_return As Decimal
  Dim total_prizes As Decimal
  Dim total_prizes_taxes As Decimal
  Dim total_unr_kind1 As Decimal
  Dim total_unr_kind2 As Decimal
  Dim total_handpays As Decimal
  Dim total_cancelation_handpays As Decimal
  Dim total_cancel_promo_redeemable As Decimal
  Dim total_cancel_promo_no_redeemable As Decimal
  Dim total_expirations_dev As Decimal
  Dim total_expirations_prize As Decimal
  Dim total_expirations_promo_nr As Decimal
  Dim total_taxes_cash_in As Decimal
  Dim total_tax_custody As Decimal
  Dim total_tax_provisions As Decimal

  Dim m_hide_promotion_unr As Boolean
  Dim m_is_tito_mode As Boolean
  Dim m_cash_in_tax_enabled As Boolean
  Dim m_tax_custody_enabled As Boolean
  Dim m_tax_provisions_enabled As Boolean
  Dim m_cash_in_tax_also_apply_split_b As Boolean
  Dim m_chip_sale_register_enabled As Boolean

  Dim total_difference As Decimal
  Dim total_pending_mb As Decimal
  Dim total_excess_mb As Decimal

  Dim total_cash_advance As Decimal

  '28-SEP-2016
  Dim total_amount_money_recharge As Decimal
  Dim total_amount_bank_card_recharge As Decimal
  Dim total_vouchers_count As Integer
  Dim total_delivered_vouchers As Integer
  Dim total_amount_delivered_vouchers As Decimal

  Private m_gaming_day As Boolean = False
  Private m_opt_gaming_day As Boolean = False

  Private m_dt_chips As DataTable
  Private m_total_currency_chips_expected As Decimal()
  Private m_total_currency_chips_collected As Decimal()
  Private m_total_currency_chips_difference As Decimal()

  'EOR 22-SEP-2016
  Dim total_service_charge_a As Decimal
  Dim total_service_charge_b As Decimal
  Dim m_service_charge_company As String  'EOR 05-OCT-2016

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CASHIER_SESSIONS

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_INVOICING.GetString(227)

    m_is_tito_mode = TITO.Utils.IsTitoMode()

    Me.m_hide_promotion_unr = GeneralParam.GetBoolean("Cashier", "Promotion.HideUNR", True)
    Me.m_cash_in_tax_enabled = GeneralParam.GetBoolean("Cashier", "CashInTax.Enabled")
    Me.m_cash_in_tax_also_apply_split_b = Me.m_cash_in_tax_enabled AndAlso GeneralParam.GetBoolean("Cashier", "CashInTax.AlsoApplyToSplitB")
    Me.m_chip_sale_register_enabled = GeneralParam.GetBoolean("GamingTables", "Cashier.RegisterChipSaleAtTableEnabled")
    Me.m_card_differentiate_type = GeneralParam.GetBoolean("Cashier.PaymentMethod", "BankCard.DifferentiateType", False)
    Me.m_tax_custody_enabled = WSI.Common.Misc.IsTaxCustodyEnabled()
    Me.m_tax_provisions_enabled = WSI.Common.Misc.IsTaxProvisionsEnabled
    Me.m_service_charge_company = GeneralParam.GetString("Cashier.ServiceCharge", "AttributedToCompany", "B").ToUpper() 'EOR 05-OCT-2016

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(1)

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_INFO).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_INFO).Text = GLB_NLS_GUI_INVOICING.GetString(2)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_INFO).Enabled = False

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Text = GLB_NLS_GUI_INVOICING.GetString(5)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Enabled = False

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = Not m_is_tito_mode
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_INVOICING.GetString(6)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = IIf(Cage.IsCageEnabled() AndAlso Cage.CollectionDefaultCageSession() = Cage.DefaultCageSession.ChooseCageSession, True, False)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3388)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Type = uc_button.ENUM_BUTTON_TYPE.USER
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Size = New System.Drawing.Size(90, 45)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = False

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Text = GLB_NLS_GUI_INVOICING.GetString(494)   'Report (7) --> Global report (949)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Type = uc_button.ENUM_BUTTON_TYPE.USER
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Size = New System.Drawing.Size(90, 45)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = False

    ' Date
    Me.gb_date.Text = GLB_NLS_GUI_INVOICING.GetString(201)
    Me.opt_opening_session.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2101)
    Me.opt_movement_session.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2102)
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Terminals 
    Me.gb_cashier.Text = GLB_NLS_GUI_INVOICING.GetString(209)
    Me.opt_one_cashier.Text = GLB_NLS_GUI_INVOICING.GetString(218)
    Me.opt_all_cashiers.Text = GLB_NLS_GUI_INVOICING.GetString(219)

    ' Users 
    Me.gb_user.Text = GLB_NLS_GUI_INVOICING.GetString(220)
    Me.opt_one_user.Text = GLB_NLS_GUI_INVOICING.GetString(218)
    Me.opt_all_users.Text = GLB_NLS_GUI_INVOICING.GetString(219)
    Me.chk_show_all.Text = GLB_NLS_GUI_CONFIGURATION.GetString(90)

    ' State 
    fra_session_state.Text = GLB_NLS_GUI_INVOICING.GetString(152)
    chk_session_open.Text = GLB_NLS_GUI_INVOICING.GetString(153)
    chk_session_closed.Text = GLB_NLS_GUI_INVOICING.GetString(154)
    chk_session_pending.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1913)

    ' Splits 
    Try
      ' Populate structure from DB
      If WSI.Common.Split.ReadSplitParameters(m_split_data) Then

        Me.opt_company_a.Text = m_split_data.company_a.company_name
        Me.gb_company.Text = GLB_NLS_GUI_INVOICING.GetString(150)
        Me.opt_company_both.Text = GLB_NLS_GUI_INVOICING.GetString(151)
        m_both_company_selected = True

      End If
    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Call ShowOrHideSplitB()

    If Not Cage.IsCageEnabled() Then
      chk_session_pending.Visible = False
      fra_session_state.Size = New Size(fra_session_state.Size.Width, fra_session_state.Size.Height - 20)
    End If

    m_state = New List(Of String)

    ' - Gaming Day
    m_gaming_day = WSI.Common.Misc.IsGamingDayEnabled()
    opt_movement_gaming_day.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6176)
    opt_movement_gaming_day.Text = Char.ToUpperInvariant(opt_movement_gaming_day.Text(0)) & opt_movement_gaming_day.Text.Remove(0, 1)
    opt_movement_gaming_day.Visible = m_gaming_day

    ' Grid
    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

    If (WSI.Common.Misc.IsFloorDualCurrencyEnabled) Then

      Call FillCurrenciesCombo()
      Me.lbl_currency.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1057)
      lbl_currency.Visible = True
      cmb_currencies.Visible = True
      Call GetComboTerminalsByCurrencies()
    Else
      Call SetComboCashierAlias(Me.cmb_cashier)
      lbl_currency.Visible = False
      cmb_currencies.Visible = False
    End If

    ' XCD 16-Aug-2012 Set combo with Users
    Call SetCombo(Me.cmb_user, "SELECT GU_USER_ID, GU_USERNAME FROM GUI_USERS WHERE GU_BLOCK_REASON = " & _
                    WSI.Common.GUI_USER_BLOCK_REASON.NONE & " ORDER BY GU_USERNAME")

    'DHA 09-Jan-2014 Add control to hidde system sessions
    Me.chk_not_show_system_sessions.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4445)

    m_cage_session_id = 0

    m_national_currency = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()

  End Sub ' GUI_FilterReset

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '   - TRUE: filter values are accepted
  '   - FALSE: filter values are not accepted

  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Date selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Initialize members and accumulators related to the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_BeforeFirstRow()

    total_deposits = 0
    total_withdraws = 0
    total_card_in = 0
    total_card_out = 0
    total_expected = 0
    total_collected = 0
    total_difference_cashier = 0
    total_difference_mb = 0
    total_taxes = 0
    total_service_charge = 0
    total_tax_returning = 0
    total_decimal_rounding = 0
    total_result = 0
    total_promo_no_redeemable = 0
    total_promo_redeemable = 0
    total_no_redeemable_to_redeemable = 0
    total_deposits_less_withdrawals = 0
    total_inputs_less_outputs = 0
    total_check_payment = 0
    total_commissions_bank_card = 0
    total_commissions_cur_exch = 0
    total_bank_card = 0
    total_currency_exchange = 0
    total_recharge_for_points = 0
    total_recharge_for_points_out = 0
    total_commissions_check = 0
    total_check_recharges = 0
    total_chips_sale_register = 0
    total_check_balance = 0
    total_check_collected = 0
    total_check_difference = 0
    total_bank_card_balance = 0
    total_bank_card_collected = 0
    total_bank_card_difference = 0
    total_bank_card_credit = 0
    total_bank_card_debit = 0
    total_chips_balance = 0
    total_chips_collected = 0
    total_chips_difference = 0

    total_inputs_a = 0
    total_inputs_b = 0
    total_card_usage = 0
    total_outputs_a = 0
    total_outputs_b = 0
    total_refund_card_usage = 0
    total_total_prizes = 0
    total_custody_return = 0
    total_prizes = 0
    total_prizes_taxes = 0
    total_unr_kind1 = 0
    total_unr_kind2 = 0
    total_handpays = 0
    total_cancelation_handpays = 0
    total_cancel_promo_redeemable = 0
    total_cancel_promo_no_redeemable = 0
    total_expirations_dev = 0
    total_expirations_prize = 0
    total_expirations_promo_nr = 0
    total_taxes_cash_in = 0
    total_tax_custody = 0
    total_tax_provisions = 0

    total_difference = 0
    total_pending_mb = 0
    total_excess_mb = 0

    total_cash_advance = 0

    m_has_data_company_b = False

    'EOR 22-SEP-2016
    total_service_charge_a = 0
    total_service_charge_b = 0

    ' Currency Exchange columns
    LoadIsoCodes(m_currency_exchange_iso_codes, m_currency_exchange_iso_description, m_currency_exchange_iso_types)

    total_currency_exchange_expected = New Decimal(m_currency_exchange_iso_codes.Count() - 1) {}
    total_currency_exchange_collected = New Decimal(m_currency_exchange_iso_codes.Count() - 1) {}
    total_currency_exchange_difference = New Decimal(m_currency_exchange_iso_codes.Count() - 1) {}

    m_dt_chips = GetAllowedISOCode()

    m_total_currency_chips_expected = New Decimal(m_dt_chips.Rows.Count() - 1) {}
    m_total_currency_chips_collected = New Decimal(m_dt_chips.Rows.Count() - 1) {}
    m_total_currency_chips_difference = New Decimal(m_dt_chips.Rows.Count() - 1) {}

    'ESE 28-SEP-2016
    total_amount_money_recharge = 0
    total_amount_bank_card_recharge = 0
    total_vouchers_count = 0
    total_delivered_vouchers = 0
    total_amount_delivered_vouchers = 0

    Call GUI_StyleSheet()

  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE: Print totalizator row in the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    Dim _idx_row As Int32
    Dim _num_iso_codes As Int32
    Dim _column_offset As Int32

    Call ShowOrHideSplitB()

    _num_iso_codes = m_currency_exchange_iso_codes.Count()
    _column_offset = 0
    _idx_row = Me.Grid.AddRow()

    Me.Grid.Cell(_idx_row, GRID_COLUMN_USER_NAME).Value = GLB_NLS_GUI_INVOICING.GetString(205)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_DEPOSITS).Value = GUI_FormatCurrency(total_deposits, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_WITHDRAWS).Value = GUI_FormatCurrency(total_withdraws, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_CARD_IN).Value = GUI_FormatCurrency(total_card_in, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_CARD_OUT).Value = GUI_FormatCurrency(total_card_out, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_CASH_CARD_IN).Value = GUI_FormatCurrency(total_card_in, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_CASH_CARD_OUT).Value = GUI_FormatCurrency(total_card_out, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_EXPECTED).Value = GUI_FormatCurrency(total_expected, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) ' Expected

    Me.Grid.Cell(_idx_row, GRID_COLUMN_DEPOSITS_LESS_WITHDRAWALS).Value = GUI_FormatCurrency(total_deposits_less_withdrawals, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_INPUTS_LESS_OUTPUTS).Value = GUI_FormatCurrency(total_inputs_less_outputs, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TOTAL_CURRENCY_EXCHANGE).Value = GUI_FormatCurrency(total_currency_exchange, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_REDEEMABLE_AS_CASHIN).Value = GUI_FormatCurrency(total_recharge_for_points, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_REDEEMABLE_AS_CASHOUT).Value = GUI_FormatCurrency(total_recharge_for_points_out, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TOTAL_BANK_CARD).Value = GUI_FormatCurrency(total_bank_card, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TOTAL_CHECK_RECHARGES).Value = GUI_FormatCurrency(total_check_recharges, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TOTAL_CHIP_SALE_REGISTER).Value = GUI_FormatCurrency(total_chips_sale_register, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_CHECK_BALANCE).Value = GUI_FormatCurrency(total_check_balance, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_BANK_CARD_BALANCE).Value = GUI_FormatCurrency(total_bank_card_balance, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_CHIPS_BALANCE).Value = GUI_FormatCurrency(total_chips_balance, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_BANK_CARD_CREDIT_BALANCE).Value = GUI_FormatCurrency(total_bank_card_credit, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_BANK_CARD_DEBIT_BALANCE).Value = GUI_FormatCurrency(total_bank_card_debit, 2)

    If m_both_company_selected Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_COLLECTED).Value = GUI_FormatCurrency(total_collected, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_DIFFERENCE_CASHIER).Value = GUI_FormatCurrency(total_difference_cashier, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_DIFFERENCE_TOTAL).Value = GUI_FormatCurrency(total_difference, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      Me.Grid.Cell(_idx_row, GRID_COLUMN_CHECK_COLLECTED).Value = GUI_FormatCurrency(total_check_collected, 2)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_CHECK_DIFFERENCE).Value = GUI_FormatCurrency(total_check_difference, 2)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_BANK_CARD_COLLECTED).Value = GUI_FormatCurrency(total_bank_card_collected, 2)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_BANK_CARD_DIFFERENCE).Value = GUI_FormatCurrency(total_bank_card_difference, 2)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_CHIPS_COLLECTED).Value = GUI_FormatCurrency(total_chips_collected, 2)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_CHIPS_DIFFERENCE).Value = GUI_FormatCurrency(total_chips_difference, 2)
    End If

    'EOR 24-SEP-2016
    If opt_company_a.Checked Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_SERVICE_CHARGE).Value = GUI_FormatCurrency(total_service_charge_a, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    ElseIf opt_company_b.Checked Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_SERVICE_CHARGE).Value = GUI_FormatCurrency(total_service_charge_b, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Else
      Me.Grid.Cell(_idx_row, GRID_COLUMN_SERVICE_CHARGE).Value = GUI_FormatCurrency(total_service_charge, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    Me.Grid.Cell(_idx_row, GRID_COLUMN_COMMISSIONS_BANK_CARD).Value = GUI_FormatCurrency(total_commissions_bank_card, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_COMMISSIONS_CHECK).Value = GUI_FormatCurrency(total_commissions_check, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_COMMISSIONS_CUR_EXCH).Value = GUI_FormatCurrency(total_commissions_cur_exch, 2)

    If Not opt_company_b.Checked Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_PROMO_NO_REDEEMABLE).Value = GUI_FormatCurrency(total_promo_no_redeemable, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_PROMO_REDEEMABLE).Value = GUI_FormatCurrency(total_promo_redeemable, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_NO_REDEEMABLE_TO_REDEEMABLE).Value = GUI_FormatCurrency(total_no_redeemable_to_redeemable, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_TAXES).Value = GUI_FormatCurrency(total_taxes, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_TAX_RETURNING).Value = GUI_FormatCurrency(total_tax_returning, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_DECIMAL_ROUNDING).Value = GUI_FormatCurrency(total_decimal_rounding, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_CHECK_PAYMENT).Value = GUI_FormatCurrency(total_check_payment, 2)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_COMMISSIONS_CUR_EXCH).Value = GUI_FormatCurrency(total_commissions_cur_exch, 2)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_DIFFERENCE_MB).Value = GUI_FormatCurrency(total_difference_mb, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      If total_pending_mb > 0 Then
        Me.Grid.Cell(_idx_row, GRID_COLUMN_PENDING_MB).Value = GUI_FormatCurrency(total_pending_mb, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End If

      Me.Grid.Cell(_idx_row, GRID_COLUMN_SHORTFALL_EXCESS_MB).Value = GUI_FormatCurrency(total_excess_mb, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_SESSION_RESULT).Value = GUI_FormatCurrency(total_result, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_COMMISSIONS_CHECK).Value = GUI_FormatCurrency(total_commissions_check, 2)
    End If

    Me.Grid.Cell(_idx_row, GRID_COLUMN_INPUTS_A).Value = GUI_FormatCurrency(total_inputs_a, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_INPUTS_B).Value = GUI_FormatCurrency(total_inputs_b, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TAXES_CASH_IN).Value = GUI_FormatCurrency(total_taxes_cash_in, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TAX_CUSTODY).Value = GUI_FormatCurrency(total_tax_custody, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TAX_PROVISIONS).Value = GUI_FormatCurrency(total_tax_provisions, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_CARD_USAGE).Value = GUI_FormatCurrency(total_card_usage, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_OUTPUTS_A).Value = GUI_FormatCurrency(total_outputs_a, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_OUTPUTS_B).Value = GUI_FormatCurrency(total_outputs_b, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_REFUND_CARD_USAGE).Value = GUI_FormatCurrency(total_refund_card_usage, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TOTAL_PRIZES).Value = GUI_FormatCurrency(total_total_prizes, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TAX_CUSTODY_RETURN).Value = GUI_FormatCurrency(total_custody_return, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_PRIZES).Value = GUI_FormatCurrency(total_prizes, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_PRIZES_TAXES).Value = GUI_FormatCurrency(total_prizes_taxes, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_UNR_KIND1).Value = GUI_FormatCurrency(total_unr_kind1, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_UNR_KIND2).Value = GUI_FormatCurrency(total_unr_kind2, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_HANDPAYS).Value = GUI_FormatCurrency(total_handpays, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_CANCELATION_HANDPAYS).Value = GUI_FormatCurrency(total_cancelation_handpays, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TOTAL_HANDPAYS).Value = GUI_FormatCurrency(total_handpays - total_cancelation_handpays, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_CANCEL_PROMO_REDEEMABLE).Value = GUI_FormatCurrency(total_cancel_promo_redeemable, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_CANCEL_PROMO_NO_REDEEMABLE).Value = GUI_FormatCurrency(total_cancel_promo_no_redeemable, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_EXPIRATIONS_DEV).Value = GUI_FormatCurrency(total_expirations_dev, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_EXPIRATIONS_PRIZE).Value = GUI_FormatCurrency(total_expirations_prize, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_EXPIRATIONS_PROMO_NR).Value = GUI_FormatCurrency(total_expirations_promo_nr, 2)

    Me.Grid.Cell(_idx_row, GRID_COLUMN_CASH_ADVANCE).Value = GUI_FormatCurrency(total_cash_advance, 2)

    ' Currency exchange columns
    For _index As Integer = 0 To _num_iso_codes - 1
      'Total expected
      Me.Grid.Cell(_idx_row, GRID_COLUMNS + _column_offset).Value = Currency.Format(total_currency_exchange_expected(_index), m_currency_exchange_iso_codes(_index))
      _column_offset += 1

      'Total collected
      Me.Grid.Cell(_idx_row, GRID_COLUMNS + _column_offset).Value = Currency.Format(total_currency_exchange_collected(_index), m_currency_exchange_iso_codes(_index))
      _column_offset += 1

      'Total difference
      Me.Grid.Cell(_idx_row, GRID_COLUMNS + _column_offset).Value = Currency.Format(total_currency_exchange_difference(_index), m_currency_exchange_iso_codes(_index))
      _column_offset += 1
    Next

    ' Currency chips columns
    For _index As Integer = 0 To m_dt_chips.Rows.Count - 1
      'Total expected
      Me.Grid.Cell(_idx_row, GRID_COLUMNS + _column_offset).Value = Currency.Format(m_total_currency_chips_expected(_index), "")
      _column_offset += 1

      'Total collected
      Me.Grid.Cell(_idx_row, GRID_COLUMNS + _column_offset).Value = Currency.Format(m_total_currency_chips_collected(_index), "")
      _column_offset += 1

      'Total difference
      Me.Grid.Cell(_idx_row, GRID_COLUMNS + _column_offset).Value = Currency.Format(m_total_currency_chips_difference(_index), "")
      _column_offset += 1
    Next

    'ESE 28-SEP-2016
    Me.Grid.Cell(_idx_row, GRID_COLUMN_CASH_RECHARGE).Value = GUI_FormatCurrency(total_amount_money_recharge, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_CREDIT_CARD_RECHARGE).Value = GUI_FormatCurrency(total_amount_bank_card_recharge, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_VOUCHERS_COUNT).Value = Integer.Parse(total_vouchers_count)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_DELIVERED_VOUCHERS).Value = Integer.Parse(total_delivered_vouchers)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_AMOUNT_DELIVERED_VOUCHERS).Value = GUI_FormatCurrency(total_amount_delivered_vouchers, 2)

    ' Color Row
    Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

    ' Hide columns that don't have values
    ShowHideCurrencyColumns()

    ' Show or Hide Columns Service Charge   EOR 05-OCT-2016
    Call ShowHideServiceChargeColumns()

  End Sub ' GUI_AfterLastRow

  ' PURPOSE: Process button actions in order to branch to a child screen
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY
        If GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY).Enabled Then
          Call MyBase.GUI_ButtonClick(ButtonId)

          If Me.IsDisposed Then
            Exit Sub
          End If

          If Not m_both_company_selected Then
            Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Enabled = m_is_tito_mode
            Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False
            Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = m_is_tito_mode
          Else
            Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = True
            Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = True
            If Me.Grid.NumRows > 0 Then
              Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Enabled = True
              Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = True
            Else
              Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Enabled = False
              Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False
            End If
          End If

        End If

      Case frm_base_sel.ENUM_BUTTON.BUTTON_GRID_INFO
        Call GUI_ShowSelectedItem()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        Call GUI_ShowSelectedItemSummary()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0
        Call GUI_ShowSelectedItemMBMovements()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1
        Call GUI_SelectCageSession()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2
        Call GUI_ShowSummarySessions()

      Case ENUM_BUTTON.BUTTON_PRINT
        Call MyBase.GUI_ButtonClick(ButtonId)

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub ' GUI_ButtonClick

  ' PURPOSE: Call to child window to show details for the selected row
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ShowSelectedItem()

    Dim idx_row As Integer
    Dim session_id As Integer
    Dim session_name As String
    Dim session_date As String
    Dim frm As frm_cashier_movements

    ' Search the first row selected
    For idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(idx_row).IsSelected Then
        Exit For
      End If
    Next

    ' Protect totalizator row
    If Not IsValidDataRow(idx_row) Then
      Return
    End If

    If Me.Grid.Cell(idx_row, GRID_COLUMN_SESSION_ID).Value = "" Then
      session_id = 0
      session_name = ""
    Else
      session_id = Me.Grid.Cell(idx_row, GRID_COLUMN_SESSION_ID).Value
      session_name = Me.Grid.Cell(idx_row, GRID_COLUMN_SESSION_NAME).Value
    End If

    If Me.Grid.Cell(idx_row, GRID_COLUMN_OPENING_DATE).Value = "" Then
      session_date = ""
    Else
      session_date = Me.Grid.Cell(idx_row, GRID_COLUMN_OPENING_DATE).Value.ToString()
    End If

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    frm = New frm_cashier_movements
    If m_sessions_from_movements <> "" Then
      frm.ShowForEdit(Me.MdiParent, session_id, session_name, m_from_date, m_to_date)
    Else
      frm.ShowForEdit(Me.MdiParent, session_id, session_name)
    End If

  End Sub ' GUI_ShowSelectedItem

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _result As Boolean

    _result = True

    ' Common session information
    ' Session Id
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SESSION_ID).Value = DbRow.Value(SQL_COLUMN_SESSION_ID)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SESSION_NAME).Value = DbRow.Value(SQL_COLUMN_SESSION_NAME)

    ' User Id
    Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_ID).Value = DbRow.Value(SQL_COLUMN_USER_ID)

    ' User Name
    If Not DbRow.IsNull(SQL_COLUMN_USER_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_NAME).Value = DbRow.Value(SQL_COLUMN_USER_NAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_NAME).Value = String.Empty
    End If

    ' Gaming Day
    If Not DbRow.IsNull(SQL_COLUMN_GAMING_DAY) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GAMING_DAY).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_GAMING_DAY), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GAMING_DAY).Value = String.Empty
    End If

    ' Opening date
    Me.Grid.Cell(RowIndex, GRID_COLUMN_OPENING_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_OPENING_DATE), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    ' Closing date
    If Not DbRow.IsNull(SQL_COLUMN_CLOSE_DATE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CLOSE_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_CLOSE_DATE), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CLOSE_DATE).Value = String.Empty
    End If

    ' Cashier Id
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CASHIER_ID).Value = DbRow.Value(SQL_COLUMN_CASHIER_ID)

    ' Cashier Name
    If Not DbRow.IsNull(SQL_COLUMN_CASHIER_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CASHIER_NAME).Value = Computer.Alias(DbRow.Value(SQL_COLUMN_CASHIER_NAME))
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CASHIER_NAME).Value = String.Empty
    End If

    ' Status
    If Not DbRow.IsNull(SQL_COLUMN_STATUS) Then
      Select Case DbRow.Value(SQL_COLUMN_STATUS)

        Case WSI.Common.CASHIER_SESSION_STATUS.OPEN, _
             WSI.Common.CASHIER_SESSION_STATUS.OPEN_PENDING
          Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_INVOICING.GetString(156)

        Case WSI.Common.CASHIER_SESSION_STATUS.CLOSED
          Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_INVOICING.GetString(157)

        Case WSI.Common.CASHIER_SESSION_STATUS.PENDING_CLOSING
          Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1914)

      End Select

    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = String.Empty
    End If

    ' Balance and monetary data 
    If m_both_company_selected Then
      ' Both Companies
      _result = GUI_SetupRow_Both_Companies(RowIndex, DbRow)
      ' Totals
      _result = UpdateTotals_Both_Companies(DbRow)
    ElseIf opt_company_a.Checked Then
      ' Company A
      _result = GUI_SetupRow_Company_A(RowIndex, DbRow)
      ' Totals
      _result = UpdateTotals_Company_A(DbRow)
    ElseIf opt_company_b.Checked Then
      ' Company B
      _result = GUI_SetupRow_Company_B(RowIndex, DbRow)
      ' Totals
      _result = UpdateTotals_Company_B(DbRow)
    End If

    Call CheckCompanyBHasData()

    ' Draw Other currencies & Common columns for both companies and Company A
    If m_both_company_selected Or opt_company_a.Checked Then

      ' Sales Limit
      If Not DbRow.IsNull(SQL_COLUMN_CS_LIMIT_SALE) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CS_LIMIT_SALE).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_CS_LIMIT_SALE), 2)
      End If

      ' MB Sales Limit
      If Not DbRow.IsNull(SQL_COLUMN_MB_LIMIT_SALE) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_LIMIT_SALE).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_MB_LIMIT_SALE), 2)
      End If

      ' Cash Cage session

      If Not DbRow.IsNull(SQL_COLUMN_CGS_SESSION_NAME) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CGS_SESSION_NAME).Value = DbRow.Value(SQL_COLUMN_CGS_SESSION_NAME)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CGS_SESSION_NAME).Value = AUDIT_NONE_STRING
      End If

      _result = DrawCurrencies(RowIndex, DbRow.Value(SQL_COLUMN_STATUS), DbRow.Value(SQL_COLUMN_SESSION_DATA))

      _result = DrawChips(RowIndex, DbRow.Value(SQL_COLUMN_STATUS), DbRow.Value(SQL_COLUMN_SESSION_DATA))
    End If

    'ESE 29-SEP-2016
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_RECHARGE).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_MONEY_RECHARGE_AMOUNT), 2)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CREDIT_CARD_RECHARGE).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_BANK_CARD_RECHARGE_AMOUNT), 2)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_VOUCHERS_COUNT).Value = Int32.Parse(DbRow.Value(SQL_COLUMN_TOTAL_VOUCHERS))
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DELIVERED_VOUCHERS).Value = Int32.Parse(DbRow.Value(SQL_COLUMN_DELIVERED_VOUCHERS))
    Me.Grid.Cell(RowIndex, GRID_COLUMN_AMOUNT_DELIVERED_VOUCHERS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TOTAL_VOUCHERS_AMOUNT), 2)

    Return _result

  End Function

  Private Sub ShowOrHideSplitB()
    Me.gb_company.Visible = m_split_data.enabled_b
    Me.opt_company_b.Visible = m_split_data.enabled_b OrElse m_has_data_company_b

    If Not String.IsNullOrEmpty(m_split_data.company_b.company_name) Then
      Me.opt_company_b.Text = m_split_data.company_b.company_name
    Else
      Me.opt_company_b.Text = "[" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(361) & " B: " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(362) & "]"
    End If

  End Sub

  Private Sub StyleSheetSplitB()
    Dim _b_name_col As String

    If Not String.IsNullOrEmpty(m_split_data.company_b.name) Then
      _b_name_col = m_split_data.company_b.name
    Else
      _b_name_col = "[" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(361) & " B: " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(363) & "]"
    End If

    If Not m_cash_in_tax_also_apply_split_b Then
      Me.Grid.Column(GRID_COLUMN_TAXES_CASH_IN).Width = 1
    End If

    If Not WSI.Common.Misc.IsTaxCustodyEnabled() Then
      Me.Grid.Column(GRID_COLUMN_TAX_CUSTODY).Width = 1
      Me.Grid.Column(GRID_COLUMN_TAX_CUSTODY_RETURN).Width = 1
    End If

    If Not WSI.Common.Misc.IsTaxProvisionsEnabled Then
      Me.Grid.Column(GRID_COLUMN_TAX_PROVISIONS).Width = 1
    End If

    With Me.Grid
      If opt_company_a.Checked OrElse (Not m_split_data.enabled_b AndAlso Not m_has_data_company_b) Then
        .Column(GRID_COLUMN_INPUTS_B).Header(1).Text = ""
        .Column(GRID_COLUMN_INPUTS_B).Width = 1
        .Column(GRID_COLUMN_INPUTS_B).IsColumnPrintable = False

        .Column(GRID_COLUMN_OUTPUTS_B).Header(1).Text = ""
        .Column(GRID_COLUMN_OUTPUTS_B).Width = 1
        .Column(GRID_COLUMN_OUTPUTS_B).IsColumnPrintable = False
      Else
        .Column(GRID_COLUMN_INPUTS_B).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(289)
        .Column(GRID_COLUMN_INPUTS_B).Header(1).Text = _b_name_col
        .Column(GRID_COLUMN_INPUTS_B).Width = GRID_WIDTH_AMOUNT_LONG + 700
        .Column(GRID_COLUMN_INPUTS_B).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
        .Column(GRID_COLUMN_INPUTS_B).IsColumnPrintable = True

        .Column(GRID_COLUMN_OUTPUTS_B).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(290)
        .Column(GRID_COLUMN_OUTPUTS_B).Header(1).Text = _b_name_col
        .Column(GRID_COLUMN_OUTPUTS_B).Width = GRID_WIDTH_TAX_RETURNING
        .Column(GRID_COLUMN_OUTPUTS_B).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
        .Column(GRID_COLUMN_OUTPUTS_B).IsColumnPrintable = True
      End If
    End With
  End Sub

  Private Sub CheckCompanyBHasData()
    If Not m_has_data_company_b Then
      m_has_data_company_b = (total_inputs_b > 0 _
                             OrElse total_outputs_b > 0)
    End If
  End Sub

  ' PURPOSE : Update the totals line values for Company B only
  '
  '  PARAMS :
  '     - INPUT :
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  Private Function UpdateTotals_Company_B(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _session_data As WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS

    _session_data = DbRow.Value(SQL_COLUMN_SESSION_DATA)

    ' Cash Result
    total_card_in += _session_data.total_cash_in.SqlMoney - _session_data.total_cash_in_only_a.SqlMoney
    total_card_out += _session_data.total_cash_out.SqlMoney - _session_data.total_cash_out_only_a.SqlMoney
    total_inputs_less_outputs += (_session_data.total_cash_in.SqlMoney - _session_data.total_cash_in_only_a.SqlMoney) - (_session_data.total_cash_out.SqlMoney - _session_data.total_cash_out_only_a.SqlMoney)

    ' Cash Desk
    total_deposits += 0
    total_withdraws += 0
    total_deposits_less_withdrawals += 0
    'total_cash_in
    'total_cash_out
    total_bank_card += _session_data.bank_card_amount_only_b.SqlMoney()
    total_check_recharges += _session_data.check_amount_only_b.SqlMoney
    total_currency_exchange += _session_data.currency_exchange_amount_only_b.SqlMoney
    total_recharge_for_points += _session_data.points_as_cashin_only_b.SqlMoney
    total_recharge_for_points_out += _session_data.points_as_cashin_prize.SqlMoney
    total_taxes_cash_in += _session_data.cash_in_tax_split2
    total_tax_custody += _session_data.tax_custody
    total_tax_provisions += _session_data.tax_provisions

    ' Inputs
    total_inputs_b += _session_data.b_total_in.SqlMoney
    total_card_usage += _session_data.cards_usage_b.SqlMoney

    'EOR 22-SEP-2016
    total_service_charge_b += _session_data.service_charge_b.SqlMoney

    ' Outputs
    total_outputs_b += _session_data.b_total_dev.SqlMoney

    ' Bank Card
    total_bank_card_balance += _session_data.total_balance_bank_card_b.SqlMoney

    'Bank Card Credit
    total_bank_card_credit += _session_data.bank_credit_card_amount_only_b.SqlMoney + _session_data.company_b_cash_advance_credit_card_comissions.SqlMoney

    'Bank Card debit
    total_bank_card_debit += _session_data.bank_debit_card_amount_only_b.SqlMoney + _session_data.company_b_cash_advance_debit_card_comissions.SqlMoney

    total_commissions_bank_card += _session_data.company_b_commissions_bank_card.SqlMoney + _
                                   _session_data.company_b_commissions_credit_card.SqlMoney + _
                                   _session_data.company_b_commissions_debit_card.SqlMoney + _
                                   _session_data.total_cash_advance_bank_card_commission_b.SqlMoney

    total_commissions_check += _session_data.company_b_commissions_check.SqlMoney + _
                               _session_data.company_b_cash_advance_check_comissions.SqlMoney

    total_commissions_cur_exch += _session_data.company_b_commissions_currency_exchange.SqlMoney

    ' Check
    total_check_balance += _session_data.check_amount_only_b.SqlMoney

    ' Chips
    total_chips_balance += GetIsoCodeTypeValue(Cage.CHIPS_ISO_CODE, CurrencyExchangeType.CURRENCY, _session_data.currencies_balance)

    ' RMS : Not Used?
    total_no_redeemable_to_redeemable += _session_data.promo_nr_to_re.SqlMoney

    'ESE 28-SEP-2016
    total_amount_money_recharge += DbRow.Value(SQL_COLUMN_MONEY_RECHARGE_AMOUNT)
    total_amount_bank_card_recharge += DbRow.Value(SQL_COLUMN_BANK_CARD_RECHARGE_AMOUNT)
    total_vouchers_count += Integer.Parse(DbRow.Value(SQL_COLUMN_TOTAL_VOUCHERS))
    total_delivered_vouchers += Integer.Parse(DbRow.Value(SQL_COLUMN_DELIVERED_VOUCHERS))
    total_amount_delivered_vouchers += DbRow.Value(SQL_COLUMN_TOTAL_VOUCHERS_AMOUNT)

    Return True

  End Function

  ' PURPOSE : Update the totals line values for Company A only
  '
  '  PARAMS :
  '     - INPUT :
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  Private Function UpdateTotals_Company_A(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _session_data As WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS

    Dim _expected As Double
    Dim _collected As Double
    Dim _difference As Double

    _expected = 0
    _collected = 0
    _difference = 0

    _session_data = DbRow.Value(SQL_COLUMN_SESSION_DATA)

    ' Cash Result
    total_card_in += _session_data.total_cash_in_only_a.SqlMoney
    total_card_out += _session_data.total_cash_out_only_a.SqlMoney
    total_inputs_less_outputs += _session_data.total_cash_in_only_a.SqlMoney - _session_data.total_cash_out_only_a.SqlMoney
    total_taxes += _session_data.total_taxes.SqlMoney
    total_result += _session_data.result_cashier_only_a.SqlMoney - _session_data.decimal_rounding.SqlMoney

    ' Cash Desk
    total_deposits += _session_data.deposits.SqlMoney
    total_withdraws += _session_data.withdraws.SqlMoney
    total_deposits_less_withdrawals += _session_data.deposits.SqlMoney - _session_data.withdraws.SqlMoney
    'total_cash_in
    'total_cash_out
    total_difference_mb += _session_data.mb_total_balance_lost.SqlMoney
    total_pending_mb += _session_data.mb_pending.SqlMoney
    total_excess_mb += _session_data.total_cash_over.SqlMoney

    _expected = _session_data.final_balance

    total_bank_card_credit += GUI_FormatCurrency(_session_data.bank_credit_card_amount_only_a.SqlMoney + _session_data.cash_advance_credit_card.SqlMoney, 2)
    total_bank_card_debit += GUI_FormatCurrency(_session_data.bank_debit_card_amount_only_a.SqlMoney + _session_data.cash_advance_debit_card.SqlMoney, 2)
    total_expected += _expected

    If Not DbRow.IsNull(SQL_COLUMN_SESSION_COLLECTED) Then
      _collected = DbRow.Value(SQL_COLUMN_SESSION_COLLECTED)
      _difference = DbRow.Value(SQL_COLUMN_SESSION_COLLECTED) - _expected

      total_collected += _collected
      total_difference_cashier += _difference - _session_data.mb_total_balance_lost.SqlMoney.ToDecimal()
      total_difference += _difference
    End If

    total_check_payment += _session_data.payment_orders.SqlMoney
    total_bank_card += _session_data.bank_card_amount_only_a.SqlMoney
    total_check_recharges += _session_data.check_amount_only_a.SqlMoney
    total_currency_exchange += _session_data.currency_exchange_amount_only_a.SqlMoney
    total_recharge_for_points += _session_data.points_as_cashin_only_a.SqlMoney
    total_recharge_for_points_out += _session_data.points_as_cashin_prize.SqlMoney

    ' Inputs
    total_inputs_a += _session_data.a_total_in.SqlMoney
    total_taxes_cash_in += _session_data.cash_in_tax_split1.SqlMoney
    total_tax_custody += _session_data.tax_custody.SqlMoney
    total_tax_provisions += _session_data.tax_provisions.SqlMoney
    total_card_usage += _session_data.cards_usage_a.SqlMoney
    total_commissions_bank_card += _session_data.total_banck_card_commission_a.SqlMoney
    total_commissions_check += _session_data.commissions_check.SqlMoney + _
                               _session_data.cash_advance_check_comissions.SqlMoney
    total_commissions_cur_exch += _session_data.commissions_currency_exchange.SqlMoney

    'EOR 22-SEP-2016
    total_service_charge_a += _session_data.service_charge_a.SqlMoney

    ' Outputs
    total_outputs_a += _session_data.a_total_dev.SqlMoney
    total_refund_card_usage += _session_data.refund_cards_usage.SqlMoney
    total_total_prizes += _session_data.prize_payment.SqlMoney
    total_custody_return += _session_data.tax_custody_return.SqlMoney
    total_decimal_rounding += _session_data.decimal_rounding.SqlMoney

    ' Winnings
    total_prizes += _session_data.prize_gross.SqlMoney
    total_prizes_taxes += _session_data.prize_taxes.SqlMoney
    total_tax_returning += _session_data.tax_returning_on_prize_coupon.SqlMoney
    total_unr_kind1 += _session_data.unr_k1_gross.SqlMoney
    total_unr_kind2 += _session_data.unr_k2_gross.SqlMoney

    ' Handpays
    total_handpays += _session_data.handpay_payment.SqlMoney
    total_cancelation_handpays += _session_data.handpay_canceled.SqlMoney

    ' Promotions
    total_promo_redeemable += _session_data.promo_re.SqlMoney
    total_promo_no_redeemable += _session_data.promo_nr.SqlMoney
    total_no_redeemable_to_redeemable += _session_data.promo_nr_to_re.SqlMoney
    total_cancel_promo_redeemable += _session_data.cancel_promo_re.SqlMoney
    total_cancel_promo_no_redeemable += _session_data.cancel_promo_nr.SqlMoney

    ' Expiration
    total_expirations_dev += _session_data.redeemable_dev_expired.SqlMoney
    total_expirations_prize += _session_data.redeemable_prize_expired.SqlMoney
    total_expirations_promo_nr += _session_data.not_redeemable_expired.SqlMoney

    ' Bank Card
    total_bank_card_balance += _session_data.total_balance_bank_card_a.SqlMoney

    ' Check
    total_check_balance += _session_data.check_amount_only_a.SqlMoney

    ' Chips
    total_chips_balance += GetIsoCodeTypeValue(Cage.CHIPS_ISO_CODE, CurrencyExchangeType.CURRENCY, _session_data.currencies_balance)

    ' Cash Advance
    total_cash_advance += _session_data.total_cash_advance

    'ESE 28-SEP-2016
    total_amount_money_recharge += DbRow.Value(SQL_COLUMN_MONEY_RECHARGE_AMOUNT)
    total_amount_bank_card_recharge += DbRow.Value(SQL_COLUMN_BANK_CARD_RECHARGE_AMOUNT)
    total_vouchers_count += Integer.Parse(DbRow.Value(SQL_COLUMN_TOTAL_VOUCHERS))
    total_delivered_vouchers += Integer.Parse(DbRow.Value(SQL_COLUMN_DELIVERED_VOUCHERS))
    total_amount_delivered_vouchers += DbRow.Value(SQL_COLUMN_TOTAL_VOUCHERS_AMOUNT)

    Return True

  End Function

  ' PURPOSE : Update the totals line values for BOTH Companies
  '
  '  PARAMS :
  '     - INPUT :
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  Private Function UpdateTotals_Both_Companies(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _session_data As WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS

    Dim _expected As Double
    Dim _collected As Double
    Dim _difference As Double

    _expected = 0
    _collected = 0
    _difference = 0

    _session_data = DbRow.Value(SQL_COLUMN_SESSION_DATA)

    ' Cash Result
    total_card_in += _session_data.total_cash_in.SqlMoney
    total_card_out += _session_data.total_cash_out.SqlMoney
    total_inputs_less_outputs += _session_data.total_cash_in.SqlMoney - _session_data.total_cash_out.SqlMoney
    total_taxes += _session_data.total_taxes.SqlMoney
    total_result += _session_data.result_cashier.SqlMoney

    ' Cash Desk
    total_deposits += _session_data.deposits.SqlMoney
    total_withdraws += _session_data.withdraws.SqlMoney
    total_deposits_less_withdrawals += _session_data.deposits.SqlMoney - _session_data.withdraws.SqlMoney
    'total_cash_in
    'total_cash_out
    total_difference_mb -= _session_data.mb_total_balance_lost.SqlMoney
    total_pending_mb += _session_data.mb_pending.SqlMoney
    total_excess_mb += _session_data.total_cash_over.SqlMoney
    total_check_payment += _session_data.payment_orders.SqlMoney
    total_chips_sale_register += _session_data.chips_sale_register_total.SqlMoney
    total_bank_card += _session_data.total_bank_card.SqlMoney
    total_check_recharges += _session_data.total_check.SqlMoney
    total_currency_exchange += _session_data.total_currency_exchange.SqlMoney
    total_recharge_for_points += _session_data.points_as_cashin.SqlMoney
    total_recharge_for_points_out += _session_data.points_as_cashin_prize.SqlMoney

    _expected = _session_data.final_balance

    '09-JUL-2018 DLM. Bug 33535:WIGOS-13440 Credit card wrong information in Cash session screen
    'total_bank_card_credit += GUI_FormatCurrency(_session_data.total_bank_credit_card.SqlMoney + _session_data.total_cash_advance_bank_credit_card.SqlMoney, 2)
    total_bank_card_credit += GUI_FormatCurrency(_session_data.total_bank_card.SqlMoney - (_session_data.total_bank_credit_card.SqlMoney + _session_data.total_cash_advance_bank_credit_card.SqlMoney), 2)
    '09-JUL-2018 DLM. Bug 33535:WIGOS-13440 Credit card wrong information in Cash session screen

    total_bank_card_debit += GUI_FormatCurrency(_session_data.total_bank_debit_card.SqlMoney + _session_data.total_cash_advance_bank_debit_card.SqlMoney, 2)
    total_expected += _expected

    If Not DbRow.IsNull(SQL_COLUMN_SESSION_COLLECTED) Then
      _collected = DbRow.Value(SQL_COLUMN_SESSION_COLLECTED)
      _difference = DbRow.Value(SQL_COLUMN_SESSION_COLLECTED) - _expected

      total_collected += _collected
      total_difference_cashier += _difference - _session_data.mb_total_balance_lost.SqlMoney.ToDecimal()
      total_difference += _difference
    End If

    ' Inputs
    total_inputs_a += _session_data.a_total_in.SqlMoney
    total_taxes_cash_in += _session_data.cash_in_tax_split1.SqlMoney + _session_data.cash_in_tax_split2.SqlMoney
    total_tax_custody += _session_data.tax_custody.SqlMoney
    total_tax_provisions += _session_data.tax_provisions.SqlMoney
    total_inputs_b += _session_data.b_total_in.SqlMoney
    total_card_usage += _session_data.cards_usage.SqlMoney
    total_service_charge += _session_data.service_charge.SqlMoney
    total_commissions_bank_card += _session_data.total_commission_bank_card.SqlMoney + _session_data.total_cash_advance_bank_card_comissions.SqlMoney

    total_commissions_check += _session_data.total_commission_check.SqlMoney + _
                               _session_data.cash_advance_check_comissions.SqlMoney + _session_data.company_b_cash_advance_check_comissions.SqlMoney
    total_commissions_cur_exch += _session_data.commissions_currency_exchange.SqlMoney + _session_data.company_b_commissions_currency_exchange.SqlMoney

    ' Outputs
    total_outputs_a += _session_data.a_total_dev.SqlMoney
    total_outputs_b += _session_data.b_total_dev.SqlMoney
    total_refund_card_usage += _session_data.refund_cards_usage.SqlMoney
    total_total_prizes += _session_data.prize_payment.SqlMoney
    total_custody_return += _session_data.tax_custody_return.SqlMoney
    total_decimal_rounding += _session_data.decimal_rounding.SqlMoney

    ' Winnings
    total_prizes += _session_data.prize_gross.SqlMoney
    total_prizes_taxes += _session_data.prize_taxes.SqlMoney
    total_tax_returning += _session_data.tax_returning_on_prize_coupon.SqlMoney
    total_unr_kind1 += _session_data.unr_k1_gross.SqlMoney
    total_unr_kind2 += _session_data.unr_k2_gross.SqlMoney

    ' Handpays
    total_handpays += _session_data.handpay_payment.SqlMoney
    total_cancelation_handpays += _session_data.handpay_canceled.SqlMoney

    ' Promotions
    total_promo_redeemable += _session_data.promo_re.SqlMoney
    total_promo_no_redeemable += _session_data.promo_nr.SqlMoney
    total_no_redeemable_to_redeemable += _session_data.promo_nr_to_re.SqlMoney
    total_cancel_promo_redeemable += _session_data.cancel_promo_re.SqlMoney
    total_cancel_promo_no_redeemable += _session_data.cancel_promo_nr.SqlMoney

    ' Expiration
    total_expirations_dev += _session_data.redeemable_dev_expired.SqlMoney
    total_expirations_prize += _session_data.redeemable_prize_expired.SqlMoney
    total_expirations_promo_nr += _session_data.not_redeemable_expired.SqlMoney

    ' Bank Card
    _expected = GetIsoCodeTypeValue(m_national_currency, CurrencyExchangeType.CARD, _session_data.currencies_balance)
    _collected = GetIsoCodeTypeValue(m_national_currency, CurrencyExchangeType.CARD, _session_data.collected)
    _difference = _collected - _expected
    total_bank_card_balance += _expected

    If DbRow.Value(SQL_COLUMN_STATUS) = WSI.Common.CASHIER_SESSION_STATUS.CLOSED Then
      total_bank_card_collected += _collected
    End If

    If DbRow.Value(SQL_COLUMN_STATUS) = WSI.Common.CASHIER_SESSION_STATUS.CLOSED Then
      total_bank_card_difference += _difference
    End If

    ' Check
    _expected = GetIsoCodeTypeValue(m_national_currency, CurrencyExchangeType.CHECK, _session_data.currencies_balance)
    _collected = GetIsoCodeTypeValue(m_national_currency, CurrencyExchangeType.CHECK, _session_data.collected)
    _difference = _collected - _expected
    total_check_balance += _expected
    If DbRow.Value(SQL_COLUMN_STATUS) = WSI.Common.CASHIER_SESSION_STATUS.CLOSED Then
      total_check_collected += _collected
    End If
    If DbRow.Value(SQL_COLUMN_STATUS) = WSI.Common.CASHIER_SESSION_STATUS.CLOSED Then
      total_check_difference += _difference
    End If

    ' Chips
    _expected = GetIsoCodeTypeValue(Cage.CHIPS_ISO_CODE, CurrencyExchangeType.CASINOCHIP, _session_data.currencies_balance)
    _collected = GetIsoCodeTypeValue(Cage.CHIPS_ISO_CODE, CurrencyExchangeType.CASINOCHIP, _session_data.collected)
    _difference = _collected - _expected
    total_chips_balance += _expected
    If DbRow.Value(SQL_COLUMN_STATUS) = WSI.Common.CASHIER_SESSION_STATUS.CLOSED Then
      total_chips_collected += _collected
    End If
    If DbRow.Value(SQL_COLUMN_STATUS) = WSI.Common.CASHIER_SESSION_STATUS.CLOSED Then
      total_chips_difference += _difference
    End If

    ' Cash Advance
    total_cash_advance += _session_data.total_cash_advance

    'ESE 28-SEP-2016
    total_amount_money_recharge += DbRow.Value(SQL_COLUMN_MONEY_RECHARGE_AMOUNT)
    total_amount_bank_card_recharge += DbRow.Value(SQL_COLUMN_BANK_CARD_RECHARGE_AMOUNT)
    total_vouchers_count += Integer.Parse(DbRow.Value(SQL_COLUMN_TOTAL_VOUCHERS))
    total_delivered_vouchers += Integer.Parse(DbRow.Value(SQL_COLUMN_DELIVERED_VOUCHERS))
    total_amount_delivered_vouchers += DbRow.Value(SQL_COLUMN_TOTAL_VOUCHERS_AMOUNT)

    Return True

  End Function

  ' PURPOSE : Draws the data for the Company B
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  Private Function GUI_SetupRow_Company_B(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _session_data As Cashier.TYPE_CASHIER_SESSION_STATS

    _session_data = DbRow.Value(SQL_COLUMN_SESSION_DATA)

    ' Cash Result - Inputs
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_IN).Value = GUI_FormatCurrency(_session_data.total_cash_in.SqlMoney - _session_data.total_cash_in_only_a.SqlMoney, 2)
    ' Cash Result - Outputs
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_OUT).Value = GUI_FormatCurrency(_session_data.total_cash_out.SqlMoney - _session_data.total_cash_out_only_a.SqlMoney, 2)
    ' Cash Result - Inputs - Outputs
    Me.Grid.Cell(RowIndex, GRID_COLUMN_INPUTS_LESS_OUTPUTS).Value = GUI_FormatCurrency((_session_data.total_cash_in.SqlMoney - _session_data.total_cash_in_only_a.SqlMoney) - (_session_data.total_cash_out.SqlMoney - _session_data.total_cash_out_only_a.SqlMoney), 2)

    ' Cash Desk - Deposits
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DEPOSITS).Value = GUI_FormatCurrency(0, 2)
    ' Cash Desk - WithDraws
    Me.Grid.Cell(RowIndex, GRID_COLUMN_WITHDRAWS).Value = GUI_FormatCurrency(0, 2)
    ' Cash Desk - Deposits - WithDraws
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DEPOSITS_LESS_WITHDRAWALS).Value = GUI_FormatCurrency(0, 2)
    ' Cash Desk - Inputs
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_CARD_IN).Value = GUI_FormatCurrency(_session_data.total_cash_in.SqlMoney - _session_data.total_cash_in_only_a.SqlMoney, 2)
    ' Cash Desk - Outputs
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_CARD_OUT).Value = GUI_FormatCurrency(_session_data.total_cash_out.SqlMoney - _session_data.total_cash_out_only_a.SqlMoney, 2)
    ' Cash Desk - Bank Card Recharges
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_BANK_CARD).Value = GUI_FormatCurrency(_session_data.bank_card_amount_only_b, 2)
    ' Cash Desk - Check Recharges
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_CHECK_RECHARGES).Value = GUI_FormatCurrency(_session_data.check_amount_only_b, 2)
    ' Cash Desk - Currency Exchange
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_CURRENCY_EXCHANGE).Value = GUI_FormatCurrency(_session_data.currency_exchange_amount_only_b, 2)
    ' Recharges for points - In
    Me.Grid.Cell(RowIndex, GRID_COLUMN_REDEEMABLE_AS_CASHIN).Value = GUI_FormatCurrency(_session_data.points_as_cashin_only_b, 2)
    ' Recharges for points - out
    Me.Grid.Cell(RowIndex, GRID_COLUMN_REDEEMABLE_AS_CASHOUT).Value = GUI_FormatCurrency(_session_data.points_as_cashin_prize, 2)

    ' Cash Desk - Inputs - Outputs
    Me.Grid.Cell(RowIndex, GRID_COLUMN_EXPECTED).Value = GUI_FormatCurrency((_session_data.total_cash_in.SqlMoney - _session_data.total_cash_in_only_a.SqlMoney) - (_session_data.total_cash_out.SqlMoney - _session_data.total_cash_out_only_a.SqlMoney), 2)

    ' Inputs - "Nota de Venta"
    Me.Grid.Cell(RowIndex, GRID_COLUMN_INPUTS_B).Value = GUI_FormatCurrency(_session_data.b_total_in, 2)
    ' Inputs - Impuesto "IEJC"
    ' This column should be showed if not Cashier.CashInTax.AlsoApplyToSplitB but have values 
    If Me.Grid.Column(GRID_COLUMN_TAXES_CASH_IN).Width <> GRID_WIDTH_NAME Then
      If _session_data.cash_in_tax_split2.SqlMoney <> 0 Then
        Me.Grid.Column(GRID_COLUMN_TAXES_CASH_IN).Width = GRID_WIDTH_NAME
      End If
    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_TAXES_CASH_IN).Value = GUI_FormatCurrency(_session_data.cash_in_tax_split2, 2)

    '' Inputs - Tax provisions
    '' This column should be showed if not IsTaxProvisionsEnabled but have values 
    'If Me.Grid.Column(GRID_COLUMN_TAX_PROVISIONS).Width <> GRID_WIDTH_NAME Then
    '  If _session_data.tax_provisions.SqlMoney <> 0 Then
    '    Me.Grid.Column(GRID_COLUMN_TAX_PROVISIONS).Width = GRID_WIDTH_NAME
    '  End If
    'End If
    'Me.Grid.Cell(RowIndex, GRID_COLUMN_TAX_PROVISIONS).Value = GUI_FormatCurrency(_session_data.tax_provisions, 2)

    ' Inputs - Card Deposit
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_USAGE).Value = GUI_FormatCurrency(_session_data.cards_usage_b, 2)

    ' Inputs - Service Charge
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SERVICE_CHARGE).Value = GUI_FormatCurrency(_session_data.service_charge_b, 2)

    ' Inputs - Bank Card Commissions
    Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMISSIONS_BANK_CARD).Value = GUI_FormatCurrency(_session_data.company_b_commissions_bank_card.SqlMoney + _
                                                                                         _session_data.company_b_commissions_credit_card.SqlMoney + _
                                                                                         _session_data.company_b_commissions_debit_card.SqlMoney + _
                                                                                         _session_data.total_cash_advance_bank_card_commission_b.SqlMoney, 2)

    ' Inputs - Check Commissions
    Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMISSIONS_CHECK).Value = GUI_FormatCurrency(_session_data.company_b_commissions_check.SqlMoney + _
                                                                                     _session_data.company_b_cash_advance_check_comissions.SqlMoney, 2)
    ' Inputs - Exchange Commissions
    Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMISSIONS_CUR_EXCH).Value = GUI_FormatCurrency(_session_data.company_b_commissions_currency_exchange, 2)

    ' Outputs - "Nota de venta"
    Me.Grid.Cell(RowIndex, GRID_COLUMN_OUTPUTS_B).Value = GUI_FormatCurrency(_session_data.b_total_dev, 2)

    ' Bank Card - Cash Desk
    Me.Grid.Cell(RowIndex, GRID_COLUMN_BANK_CARD_BALANCE).Value = GUI_FormatCurrency(_session_data.total_balance_bank_card_b, 2)

    ' Bank Card - Cash Desk
    Me.Grid.Cell(RowIndex, GRID_COLUMN_BANK_CARD_CREDIT_BALANCE).Value = GUI_FormatCurrency(_session_data.bank_credit_card_amount_only_b.SqlMoney + _
                                                                                            _session_data.company_b_cash_advance_credit_card_comissions.SqlMoney, 2)

    ' Bank Card - Cash Desk
    Me.Grid.Cell(RowIndex, GRID_COLUMN_BANK_CARD_DEBIT_BALANCE).Value = GUI_FormatCurrency(_session_data.bank_debit_card_amount_only_b.SqlMoney + _
                                                                                            _session_data.company_b_cash_advance_debit_card_comissions.SqlMoney, 2)

    ' Check - Cash Desk
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CHECK_BALANCE).Value = GUI_FormatCurrency(_session_data.check_amount_only_b, 2)

    ' Chips - Cash Desk
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CHIPS_BALANCE).Value = GUI_FormatCurrency(GetIsoCodeTypeValue(Cage.CHIPS_ISO_CODE, CurrencyExchangeType.CASINOCHIP, _session_data.currencies_balance), 2)

    Return True
  End Function

  ' PURPOSE : Draws the data for the Company A
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  Private Function GUI_SetupRow_Company_A(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _session_data As WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS

    Dim _expected As Double
    Dim _collected As Double
    Dim _difference As Double
    Dim _value_without_rounding As Decimal

    _expected = 0
    _collected = 0
    _difference = 0

    _session_data = DbRow.Value(SQL_COLUMN_SESSION_DATA)

    ' Cash Result - Inputs
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_IN).Value = GUI_FormatCurrency(_session_data.total_cash_in_only_a, 2)
    ' Cash Result - Outputs
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_OUT).Value = GUI_FormatCurrency(_session_data.total_cash_out_only_a, 2)
    ' Cash Result - Inputs - Outputs
    Me.Grid.Cell(RowIndex, GRID_COLUMN_INPUTS_LESS_OUTPUTS).Value = GUI_FormatCurrency(_session_data.total_cash_in_only_a.SqlMoney - _session_data.total_cash_out_only_a.SqlMoney, 2)
    ' Cash Result - Taxes
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TAXES).Value = GUI_FormatCurrency(_session_data.total_taxes, 2)
    ' Cash Result - Cash Result
    _value_without_rounding = Convert.ToDecimal(_session_data.result_cashier_only_a) - Convert.ToDecimal(_session_data.decimal_rounding)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SESSION_RESULT).Value = GUI_FormatCurrency(_value_without_rounding, 2)
    ' Cash Desk - Deposits
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DEPOSITS).Value = GUI_FormatCurrency(_session_data.deposits, 2)
    ' Cash Desk - WithDraws
    Me.Grid.Cell(RowIndex, GRID_COLUMN_WITHDRAWS).Value = GUI_FormatCurrency(_session_data.withdraws, 2)
    ' Cash Desk - Deposits - WithDraws
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DEPOSITS_LESS_WITHDRAWALS).Value = GUI_FormatCurrency(_session_data.deposits.SqlMoney - _session_data.withdraws.SqlMoney, 2)
    ' Cash Desk - Inputs
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_CARD_IN).Value = GUI_FormatCurrency(_session_data.total_cash_in_only_a, 2)
    ' Cash Desk - Outputs
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_CARD_OUT).Value = GUI_FormatCurrency(_session_data.total_cash_out_only_a, 2)
    ' Cash Desk - Check Payments
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CHECK_PAYMENT).Value = GUI_FormatCurrency(_session_data.payment_orders, 2)
    ' Cash Desk - Bank Card Recharges
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_BANK_CARD).Value = GUI_FormatCurrency(_session_data.bank_card_amount_only_a, 2)
    ' Cash Desk - Check Recharges
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_CHECK_RECHARGES).Value = GUI_FormatCurrency(_session_data.check_amount_only_a, 2)
    ' Cash Desk - Currency Exchange
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_CURRENCY_EXCHANGE).Value = GUI_FormatCurrency(_session_data.currency_exchange_amount_only_a, 2)
    ' Recharges for points - In
    Me.Grid.Cell(RowIndex, GRID_COLUMN_REDEEMABLE_AS_CASHIN).Value = GUI_FormatCurrency(_session_data.points_as_cashin_only_a, 2)
    ' Recharges for points - out
    Me.Grid.Cell(RowIndex, GRID_COLUMN_REDEEMABLE_AS_CASHOUT).Value = GUI_FormatCurrency(_session_data.points_as_cashin_prize, 2)

    _expected = _session_data.final_balance

    _collected = IIf(DbRow.IsNull(SQL_COLUMN_SESSION_COLLECTED), 0, DbRow.Value(SQL_COLUMN_SESSION_COLLECTED))
    _difference = _collected - _expected

    ' Cash Desk - Expected
    Me.Grid.Cell(RowIndex, GRID_COLUMN_EXPECTED).Value = GUI_FormatCurrency(_expected, 2)
    If Not DbRow.IsNull(SQL_COLUMN_SESSION_COLLECTED) Then
      ' ** Cash Desk - Collected
      Me.Grid.Cell(RowIndex, GRID_COLUMN_COLLECTED).Value = GUI_FormatCurrency(_collected, 2)
      ' ** Cash Desk - Total Difference
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DIFFERENCE_TOTAL).Value = GUI_FormatCurrency(_difference, 2)
      ' ** Cash Desk - Cash Desk Diff.
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DIFFERENCE_CASHIER).Value = GUI_FormatCurrency(_difference - _session_data.mb_total_balance_lost.SqlMoney, 2)
      ' ** Cash Desk - MB Difference
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DIFFERENCE_MB).Value = GUI_FormatCurrency(_session_data.mb_total_balance_lost.SqlMoney, 2)
    End If

    ' Cash Desk - MB Pending
    If _session_data.mb_pending.SqlMoney > 0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PENDING_MB).Value = GUI_FormatCurrency(_session_data.mb_pending.SqlMoney, 2)
    End If

    ' Cash Desk - MB Excess and MB ShortFall
    If _session_data.total_cash_over.SqlMoney <> 0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SHORTFALL_EXCESS_MB).Value = GUI_FormatCurrency(_session_data.total_cash_over.SqlMoney, 2)
    End If

    ' Inputs - "Depositos de juego"
    Me.Grid.Cell(RowIndex, GRID_COLUMN_INPUTS_A).Value = GUI_FormatCurrency(_session_data.a_total_in, 2)

    ' Inputs - Impuesto "IEJC"
    ' This column should be showed if not Cashier.CashInTax.Enabled but have values 
    If Me.Grid.Column(GRID_COLUMN_TAXES_CASH_IN).Width <> GRID_WIDTH_NAME Then
      If _session_data.cash_in_tax_split1.SqlMoney <> 0 Then
        Me.Grid.Column(GRID_COLUMN_TAXES_CASH_IN).Width = GRID_WIDTH_NAME
      End If
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TAXES_CASH_IN).Value = GUI_FormatCurrency(_session_data.cash_in_tax_split1, 2)

    If Me.Grid.Column(GRID_COLUMN_TAX_CUSTODY).Width <> GRID_WIDTH_NAME Then
      If _session_data.tax_custody.SqlMoney <> 0 Then
        Me.Grid.Column(GRID_COLUMN_TAX_CUSTODY).Width = GRID_WIDTH_NAME
      End If
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TAX_CUSTODY).Value = GUI_FormatCurrency(_session_data.tax_custody, 2)

    ' Inputs - Tax provisions
    ' This column should be showed if not IsTaxProvisionsEnabled but have values 
    If Me.Grid.Column(GRID_COLUMN_TAX_PROVISIONS).Width <> GRID_WIDTH_NAME Then
      If _session_data.tax_provisions.SqlMoney <> 0 Then
        Me.Grid.Column(GRID_COLUMN_TAX_PROVISIONS).Width = GRID_WIDTH_NAME
      End If
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TAX_PROVISIONS).Value = GUI_FormatCurrency(_session_data.tax_provisions, 2)

    ' Inputs - Card Deposit
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_USAGE).Value = GUI_FormatCurrency(_session_data.cards_usage_a, 2)
    ' Inputs - Service Charge
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SERVICE_CHARGE).Value = GUI_FormatCurrency(_session_data.service_charge_a, 2)
    ' Inputs - Bank Card Commissions
    Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMISSIONS_BANK_CARD).Value = GUI_FormatCurrency(_session_data.total_banck_card_commission_a, 2)
    ' Inputs - Check Commissions
    Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMISSIONS_CHECK).Value = GUI_FormatCurrency(_session_data.commissions_check.SqlMoney + _
                                                                                     _session_data.cash_advance_check_comissions.SqlMoney, 2)
    ' Inputs - Exchange Commissions
    Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMISSIONS_CUR_EXCH).Value = GUI_FormatCurrency(_session_data.commissions_currency_exchange, 2)

    ' Outputs - "Depositos de juego"
    Me.Grid.Cell(RowIndex, GRID_COLUMN_OUTPUTS_A).Value = GUI_FormatCurrency(_session_data.a_total_dev, 2)
    ' Outputs - Card Deposit 
    Me.Grid.Cell(RowIndex, GRID_COLUMN_REFUND_CARD_USAGE).Value = GUI_FormatCurrency(_session_data.refund_cards_usage, 2)
    ' Outputs - Winnings Payout
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_PRIZES).Value = GUI_FormatCurrency(_session_data.prize_payment, 2)

    If Me.Grid.Column(GRID_COLUMN_TAX_CUSTODY_RETURN).Width <> GRID_WIDTH_NAME Then
      If _session_data.tax_custody_return.SqlMoney <> 0 Then
        Me.Grid.Column(GRID_COLUMN_TAX_CUSTODY_RETURN).Width = GRID_WIDTH_NAME
      End If
    End If
    ' Outputs - Return Custody
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TAX_CUSTODY_RETURN).Value = GUI_FormatCurrency(_session_data.tax_custody_return, 2)

    ' Outputs - Decimal Rounding
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DECIMAL_ROUNDING).Value = GUI_FormatCurrency(_session_data.decimal_rounding, 2)

    ' Winnings - Winnings
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PRIZES).Value = GUI_FormatCurrency(_session_data.prize_gross, 2)
    ' Winnings - Winnings Taxes
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PRIZES_TAXES).Value = GUI_FormatCurrency(_session_data.prize_taxes, 2)
    ' Winnings - Tax Rounding
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TAX_RETURNING).Value = GUI_FormatCurrency(_session_data.tax_returning_on_prize_coupon, 2)

    ' Winnings - Prizes in Kind (1)
    ' Winnings - Prizes in Kind (2)
    ' This columns should be showed if not Cashier.Promotion.HideUNR but have values 
    If Me.Grid.Column(GRID_COLUMN_UNR_KIND1).Width <> GRID_WIDTH_TAX_RETURNING Then
      If _session_data.unr_k1_gross.SqlMoney <> 0 OrElse _session_data.unr_k2_gross.SqlMoney <> 0 Then
        Me.Grid.Column(GRID_COLUMN_UNR_KIND1).Width = GRID_WIDTH_TAX_RETURNING
        Me.Grid.Column(GRID_COLUMN_UNR_KIND2).Width = GRID_WIDTH_TAX_RETURNING
      End If
    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_UNR_KIND1).Value = GUI_FormatCurrency(_session_data.unr_k1_gross, 2)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_UNR_KIND2).Value = GUI_FormatCurrency(_session_data.unr_k2_gross, 2)

    ' Handpays - Payment
    Me.Grid.Cell(RowIndex, GRID_COLUMN_HANDPAYS).Value = GUI_FormatCurrency(_session_data.handpay_payment, 2)
    ' Handpays - Undone
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CANCELATION_HANDPAYS).Value = GUI_FormatCurrency(_session_data.handpay_canceled, 2)
    ' Handpays - Total
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_HANDPAYS).Value = GUI_FormatCurrency(_session_data.handpay_payment.SqlMoney - _session_data.handpay_canceled.SqlMoney, 2)

    ' Promotions - Promo. Redeemable
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_REDEEMABLE).Value = GUI_FormatCurrency(_session_data.promo_re, 2)
    ' Promotions - Promo. Non-Redeemable 
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_NO_REDEEMABLE).Value = GUI_FormatCurrency(_session_data.promo_nr, 2)
    ' Promotions - Non-Redeemable to Redeemable
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NO_REDEEMABLE_TO_REDEEMABLE).Value = GUI_FormatCurrency(_session_data.promo_nr_to_re, 2)
    ' Promotions - Redeemable Promo. Cancelation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CANCEL_PROMO_REDEEMABLE).Value = GUI_FormatCurrency(_session_data.cancel_promo_re, 2)
    ' Promotions - Non-Redeemable Promo. Cancelation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CANCEL_PROMO_NO_REDEEMABLE).Value = GUI_FormatCurrency(_session_data.cancel_promo_nr, 2)

    ' Expiration - Expired Redeemable - Refund
    Me.Grid.Cell(RowIndex, GRID_COLUMN_EXPIRATIONS_DEV).Value = GUI_FormatCurrency(_session_data.redeemable_dev_expired, 2)
    ' Expiration - Expired Redeemable - Winnings
    Me.Grid.Cell(RowIndex, GRID_COLUMN_EXPIRATIONS_PRIZE).Value = GUI_FormatCurrency(_session_data.redeemable_prize_expired, 2)
    ' Expiration - Expired Non-Redeemable
    Me.Grid.Cell(RowIndex, GRID_COLUMN_EXPIRATIONS_PROMO_NR).Value = GUI_FormatCurrency(_session_data.not_redeemable_expired, 2)

    ' Bank Card - Cash Desk
    Me.Grid.Cell(RowIndex, GRID_COLUMN_BANK_CARD_BALANCE).Value = GUI_FormatCurrency(_session_data.total_balance_bank_card_a, 2)

    ' Bank Card - Cash Desk
    Me.Grid.Cell(RowIndex, GRID_COLUMN_BANK_CARD_CREDIT_BALANCE).Value = GUI_FormatCurrency(_session_data.bank_credit_card_amount_only_a.SqlMoney + _
                                                                                            _session_data.cash_advance_credit_card.SqlMoney + _
                                                                                            _session_data.cash_advance_credit_card_comissions.SqlMoney, 2)

    ' Bank Card - Cash Desk
    Me.Grid.Cell(RowIndex, GRID_COLUMN_BANK_CARD_DEBIT_BALANCE).Value = GUI_FormatCurrency(_session_data.bank_debit_card_amount_only_a.SqlMoney + _
                                                                                           _session_data.cash_advance_debit_card.SqlMoney + _
                                                                                            _session_data.cash_advance_debit_card_comissions.SqlMoney, 2)

    ' Check - Cash Desk
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CHECK_BALANCE).Value = GUI_FormatCurrency(_session_data.check_amount_only_a, 2)

    ' Chips - Cash Desk
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CHIPS_BALANCE).Value = GUI_FormatCurrency(GetIsoCodeTypeValue(Cage.CHIPS_ISO_CODE, CurrencyExchangeType.CASINOCHIP, _session_data.currencies_balance), 2)

    If CurrencyExchange.IsCashAdvanceEnabled() Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_ADVANCE).Value = GUI_FormatCurrency(_session_data.total_cash_advance, 2)
    End If

    Return True
  End Function

  ' PURPOSE : Draws the data for BOTH companies filter selected
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  Private Function GUI_SetupRow_Both_Companies(ByVal RowIndex As Integer, _
                                             ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _session_data As WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS

    Dim _expected As Double
    Dim _collected As Double
    Dim _difference As Double

    _expected = 0
    _collected = 0
    _difference = 0

    _session_data = DbRow.Value(SQL_COLUMN_SESSION_DATA)

    ' Cash Result - Inputs
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_IN).Value = GUI_FormatCurrency(_session_data.total_cash_in, 2)
    ' Cash Result - Outputs
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_OUT).Value = GUI_FormatCurrency(_session_data.total_cash_out, 2)
    ' Cash Result - Inputs - Outputs
    Me.Grid.Cell(RowIndex, GRID_COLUMN_INPUTS_LESS_OUTPUTS).Value = GUI_FormatCurrency(_session_data.total_cash_in.SqlMoney - _session_data.total_cash_out.SqlMoney, 2)
    ' Cash Result - Taxes
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TAXES).Value = GUI_FormatCurrency(_session_data.total_taxes, 2)
    ' Cash Result - Cash Result
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SESSION_RESULT).Value = GUI_FormatCurrency(_session_data.result_cashier, 2)
    ' Cash Desk - Deposits
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DEPOSITS).Value = GUI_FormatCurrency(_session_data.deposits, 2)
    ' Cash Desk - WithDraws
    Me.Grid.Cell(RowIndex, GRID_COLUMN_WITHDRAWS).Value = GUI_FormatCurrency(_session_data.withdraws, 2)
    ' Cash Desk - Deposits - WithDraws
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DEPOSITS_LESS_WITHDRAWALS).Value = GUI_FormatCurrency(_session_data.deposits.SqlMoney - _session_data.withdraws.SqlMoney, 2)
    ' Cash Desk - Inputs
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_CARD_IN).Value = GUI_FormatCurrency(_session_data.total_cash_in, 2)
    ' Cash Desk - Outputs
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_CARD_OUT).Value = GUI_FormatCurrency(_session_data.total_cash_out, 2)
    ' Cash Desk - Check Payments
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CHECK_PAYMENT).Value = GUI_FormatCurrency(_session_data.payment_orders, 2)
    ' Cash Desk - Bank Card Recharges
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_BANK_CARD).Value = GUI_FormatCurrency(_session_data.total_bank_card, 2)
    ' Cash Desk - Check Recharges
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_CHECK_RECHARGES).Value = GUI_FormatCurrency(_session_data.total_check, 2)
    ' Cash Desk - Currency Exchange
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_CURRENCY_EXCHANGE).Value = GUI_FormatCurrency(_session_data.total_currency_exchange, 2)
    ' Cash Desk - Chip Sale Register
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_CHIP_SALE_REGISTER).Value = GUI_FormatCurrency(_session_data.chips_sale_register_total, 2)
    ' Cash Desk - Chip Sale Register column must be shown if it's not enabled but has value
    If _session_data.chips_sale_register_total.SqlMoney <> 0 And Me.Grid.Column(GRID_COLUMN_TOTAL_CHIP_SALE_REGISTER).Width <> GRID_WIDTH_AMOUNT_LONG + 300 Then
      Me.Grid.Column(GRID_COLUMN_TOTAL_CHIP_SALE_REGISTER).Width = GRID_WIDTH_AMOUNT_LONG + 300
    End If
    ' Recharges for points - In
    Me.Grid.Cell(RowIndex, GRID_COLUMN_REDEEMABLE_AS_CASHIN).Value = GUI_FormatCurrency(_session_data.points_as_cashin, 2)
    ' Recharges for points - out
    Me.Grid.Cell(RowIndex, GRID_COLUMN_REDEEMABLE_AS_CASHOUT).Value = GUI_FormatCurrency(_session_data.points_as_cashin_prize, 2)

    _expected = _session_data.final_balance

    ' Cash Desk - Expected
    Me.Grid.Cell(RowIndex, GRID_COLUMN_EXPECTED).Value = GUI_FormatCurrency(_expected, 2)
    If Not DbRow.IsNull(SQL_COLUMN_SESSION_COLLECTED) Then
      _collected = DbRow.Value(SQL_COLUMN_SESSION_COLLECTED)
      _difference = _collected - _expected

      ' ** Cash Desk - Collected
      Me.Grid.Cell(RowIndex, GRID_COLUMN_COLLECTED).Value = GUI_FormatCurrency(_collected, 2)
      ' ** Cash Desk - Total Difference
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DIFFERENCE_TOTAL).Value = GUI_FormatCurrency(_difference, 2)
      ' ** Cash Desk - Cash Desk Diff.
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DIFFERENCE_CASHIER).Value = GUI_FormatCurrency(_difference - _session_data.mb_total_balance_lost.SqlMoney.ToDecimal(), 2)
      ' ** Cash Desk - MB Difference
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DIFFERENCE_MB).Value = GUI_FormatCurrency(-_session_data.mb_total_balance_lost.SqlMoney, 2)
    End If

    ' Cash Desk - MB Pending
    If _session_data.mb_pending.SqlMoney > 0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PENDING_MB).Value = GUI_FormatCurrency(_session_data.mb_pending.SqlMoney, 2)
    End If

    ' Cash Desk - MB Excess and MB ShortFall
    If _session_data.total_cash_over.SqlMoney <> 0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SHORTFALL_EXCESS_MB).Value = GUI_FormatCurrency(_session_data.total_cash_over.SqlMoney, 2)
    End If

    ' Inputs - "Depositos de juego"
    Me.Grid.Cell(RowIndex, GRID_COLUMN_INPUTS_A).Value = GUI_FormatCurrency(_session_data.a_total_in, 2)

    ' Inputs - Impuesto "IEJC"
    ' This column should be showed if not Cashier.CashInTax.Enabled but have values 
    If Me.Grid.Column(GRID_COLUMN_TAXES_CASH_IN).Width <> GRID_WIDTH_NAME Then
      If _session_data.cash_in_tax_split1.SqlMoney + _session_data.cash_in_tax_split2.SqlMoney <> 0 Then
        Me.Grid.Column(GRID_COLUMN_TAXES_CASH_IN).Width = GRID_WIDTH_NAME
      End If
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TAXES_CASH_IN).Value = GUI_FormatCurrency(_session_data.cash_in_tax_split1.SqlMoney + _session_data.cash_in_tax_split2.SqlMoney, 2)

    ' Inputs - Tax custody
    ' This column should be showed if not IsTaxCustodyEnabled but have values 
    If Me.Grid.Column(GRID_COLUMN_TAX_CUSTODY).Width <> GRID_WIDTH_NAME Then
      If _session_data.tax_custody.SqlMoney <> 0 Then
        Me.Grid.Column(GRID_COLUMN_TAX_CUSTODY).Width = GRID_WIDTH_NAME
      End If
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TAX_CUSTODY).Value = GUI_FormatCurrency(_session_data.tax_custody, 2)

    ' Inputs - Tax provisions
    ' This column should be showed if not IsTaxProvisionsEnabled but have values 
    If Me.Grid.Column(GRID_COLUMN_TAX_PROVISIONS).Width <> GRID_WIDTH_NAME Then
      If _session_data.tax_provisions.SqlMoney <> 0 Then
        Me.Grid.Column(GRID_COLUMN_TAX_PROVISIONS).Width = GRID_WIDTH_NAME
      End If
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TAX_PROVISIONS).Value = GUI_FormatCurrency(_session_data.tax_provisions, 2)

    ' Inputs - "Nota de Venta"
    Me.Grid.Cell(RowIndex, GRID_COLUMN_INPUTS_B).Value = GUI_FormatCurrency(_session_data.b_total_in, 2)
    ' Inputs - Card Deposit
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_USAGE).Value = GUI_FormatCurrency(_session_data.cards_usage, 2)
    ' Inputs - Service Charge
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SERVICE_CHARGE).Value = GUI_FormatCurrency(_session_data.service_charge, 2)
    ' Inputs - Bank Card Commissions
    Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMISSIONS_BANK_CARD).Value = GUI_FormatCurrency(_session_data.total_commission_bank_card.SqlMoney + _session_data.total_cash_advance_bank_card_comissions.SqlMoney, 2)
    ' Inputs - Check Commissions
    Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMISSIONS_CHECK).Value = GUI_FormatCurrency(_session_data.total_commission_check.SqlMoney + _
                                                                                     _session_data.cash_advance_check_comissions.SqlMoney + _session_data.company_b_cash_advance_check_comissions.SqlMoney, 2)
    ' Inputs - Exchange Commissions
    Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMISSIONS_CUR_EXCH).Value = GUI_FormatCurrency(_session_data.commissions_currency_exchange.SqlMoney + _session_data.company_b_commissions_currency_exchange.SqlMoney, 2)

    ' Outputs - "Depositos de juego"
    Me.Grid.Cell(RowIndex, GRID_COLUMN_OUTPUTS_A).Value = GUI_FormatCurrency(_session_data.a_total_dev, 2)
    ' Outputs - "Nota de venta"
    Me.Grid.Cell(RowIndex, GRID_COLUMN_OUTPUTS_B).Value = GUI_FormatCurrency(_session_data.b_total_dev, 2)
    ' Outputs - Card Deposit 
    Me.Grid.Cell(RowIndex, GRID_COLUMN_REFUND_CARD_USAGE).Value = GUI_FormatCurrency(_session_data.refund_cards_usage, 2)
    ' Outputs - Winnings Payout
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_PRIZES).Value = GUI_FormatCurrency(_session_data.prize_payment, 2)

    If Me.Grid.Column(GRID_COLUMN_TAX_CUSTODY_RETURN).Width <> GRID_WIDTH_NAME Then
      If _session_data.tax_custody_return.SqlMoney <> 0 Then
        Me.Grid.Column(GRID_COLUMN_TAX_CUSTODY_RETURN).Width = GRID_WIDTH_NAME
      End If
    End If
    ' Outputs - Return Custody
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TAX_CUSTODY_RETURN).Value = GUI_FormatCurrency(_session_data.tax_custody_return, 2)

    ' Outputs - Decimal Rounding
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DECIMAL_ROUNDING).Value = GUI_FormatCurrency(_session_data.decimal_rounding, 2)

    ' Winnings - Winnings
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PRIZES).Value = GUI_FormatCurrency(_session_data.prize_gross, 2)
    ' Winnings - Winnings Taxes
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PRIZES_TAXES).Value = GUI_FormatCurrency(_session_data.prize_taxes, 2)
    ' Winnings - Tax Rounding
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TAX_RETURNING).Value = GUI_FormatCurrency(_session_data.tax_returning_on_prize_coupon, 2)

    ' Winnings - Prizes in Kind (1)
    ' Winnings - Prizes in Kind (2)
    ' This columns should be showed if not Cashier.Promotion.HideUNR but have values 
    If Me.Grid.Column(GRID_COLUMN_UNR_KIND1).Width <> GRID_WIDTH_TAX_RETURNING Then
      If _session_data.unr_k1_gross.SqlMoney <> 0 OrElse _session_data.unr_k2_gross.SqlMoney <> 0 Then
        Me.Grid.Column(GRID_COLUMN_UNR_KIND1).Width = GRID_WIDTH_TAX_RETURNING
        Me.Grid.Column(GRID_COLUMN_UNR_KIND2).Width = GRID_WIDTH_TAX_RETURNING
      End If
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_UNR_KIND1).Value = GUI_FormatCurrency(_session_data.unr_k1_gross, 2)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_UNR_KIND2).Value = GUI_FormatCurrency(_session_data.unr_k2_gross, 2)

    ' Handpays - Payment
    Me.Grid.Cell(RowIndex, GRID_COLUMN_HANDPAYS).Value = GUI_FormatCurrency(_session_data.handpay_payment, 2)
    ' Handpays - Undone
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CANCELATION_HANDPAYS).Value = GUI_FormatCurrency(_session_data.handpay_canceled, 2)
    ' Handpays - Total
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_HANDPAYS).Value = GUI_FormatCurrency(_session_data.handpay_payment.SqlMoney - _session_data.handpay_canceled.SqlMoney, 2)

    ' Promotions - Promo. Redeemable
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_REDEEMABLE).Value = GUI_FormatCurrency(_session_data.promo_re, 2)
    ' Promotions - Promo. Non-Redeemable 
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_NO_REDEEMABLE).Value = GUI_FormatCurrency(_session_data.promo_nr, 2)
    ' Promotions - Non-Redeemable to Redeemable
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NO_REDEEMABLE_TO_REDEEMABLE).Value = GUI_FormatCurrency(_session_data.promo_nr_to_re, 2)
    ' Promotions - Redeemable Promo. Cancelation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CANCEL_PROMO_REDEEMABLE).Value = GUI_FormatCurrency(_session_data.cancel_promo_re, 2)
    ' Promotions - Non-Redeemable Promo. Cancelation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CANCEL_PROMO_NO_REDEEMABLE).Value = GUI_FormatCurrency(_session_data.cancel_promo_nr, 2)

    ' Expiration - Expired Redeemable - Refund
    Me.Grid.Cell(RowIndex, GRID_COLUMN_EXPIRATIONS_DEV).Value = GUI_FormatCurrency(_session_data.redeemable_dev_expired, 2)
    ' Expiration - Expired Redeemable - Winnings
    Me.Grid.Cell(RowIndex, GRID_COLUMN_EXPIRATIONS_PRIZE).Value = GUI_FormatCurrency(_session_data.redeemable_prize_expired, 2)
    ' Expiration - Expired Non-Redeemable
    Me.Grid.Cell(RowIndex, GRID_COLUMN_EXPIRATIONS_PROMO_NR).Value = GUI_FormatCurrency(_session_data.not_redeemable_expired, 2)

    ' Bank Card 
    _expected = GetIsoCodeTypeValue(m_national_currency, CurrencyExchangeType.CARD, _session_data.currencies_balance)
    _collected = GetIsoCodeTypeValue(m_national_currency, CurrencyExchangeType.CARD, _session_data.collected)
    _difference = _collected - _expected

    ' Bank Card - CREDIT
    ' 09-JUL-2018 DLM. Bug 33535:WIGOS-13440 Credit card wrong information in Cash session screen
    'Me.Grid.Cell(RowIndex, GRID_COLUMN_BANK_CARD_CREDIT_BALANCE).Value = GUI_FormatCurrency(_session_data.total_bank_credit_card.SqlMoney + _session_data.total_cash_advance_bank_credit_card.SqlMoney, 2)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_BANK_CARD_CREDIT_BALANCE).Value = GUI_FormatCurrency(_session_data.total_bank_card.SqlMoney -
                                                                                           (_session_data.total_bank_credit_card.SqlMoney + _session_data.total_cash_advance_bank_credit_card.SqlMoney), 2)
    ' 09-JUL-2018 DLM. Bug 33535:WIGOS-13440 Credit card wrong information in Cash session screen

    ' Bank Card - DEBIT
    Me.Grid.Cell(RowIndex, GRID_COLUMN_BANK_CARD_DEBIT_BALANCE).Value = GUI_FormatCurrency(_session_data.total_bank_debit_card.SqlMoney + _session_data.total_cash_advance_bank_debit_card.SqlMoney, 2)

    ' Bank Card - Cash Desk
    Me.Grid.Cell(RowIndex, GRID_COLUMN_BANK_CARD_BALANCE).Value = GUI_FormatCurrency(_expected, 2)
    ' Bank Card - Collected
    If DbRow.Value(SQL_COLUMN_STATUS) = WSI.Common.CASHIER_SESSION_STATUS.CLOSED Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_BANK_CARD_COLLECTED).Value = GUI_FormatCurrency(_collected, 2)
    End If
    ' Bank Card - Difference
    If DbRow.Value(SQL_COLUMN_STATUS) = WSI.Common.CASHIER_SESSION_STATUS.CLOSED Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_BANK_CARD_DIFFERENCE).Value = GUI_FormatCurrency(_difference, 2)
    End If

    ' Check
    _expected = GetIsoCodeTypeValue(m_national_currency, CurrencyExchangeType.CHECK, _session_data.currencies_balance)
    _collected = GetIsoCodeTypeValue(m_national_currency, CurrencyExchangeType.CHECK, _session_data.collected)
    _difference = _collected - _expected

    ' Check - Cash Desk
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CHECK_BALANCE).Value = GUI_FormatCurrency(_expected, 2)
    ' Check - Collected
    If DbRow.Value(SQL_COLUMN_STATUS) = WSI.Common.CASHIER_SESSION_STATUS.CLOSED Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CHECK_COLLECTED).Value = GUI_FormatCurrency(_collected, 2)
    End If
    ' Check - Difference
    If DbRow.Value(SQL_COLUMN_STATUS) = WSI.Common.CASHIER_SESSION_STATUS.CLOSED Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CHECK_DIFFERENCE).Value = GUI_FormatCurrency(_difference, 2)
    End If

    ' Chips
    _expected = GetIsoCodeTypeValue(Cage.CHIPS_ISO_CODE, CurrencyExchangeType.CASINOCHIP, _session_data.currencies_balance)
    _collected = GetIsoCodeTypeValue(Cage.CHIPS_ISO_CODE, CurrencyExchangeType.CASINOCHIP, _session_data.collected)
    _difference = _collected - _expected

    ' Chips - Cash Desk
    ' This column should be showed if have values but gaming tables disabled
    If Me.Grid.Column(GRID_COLUMN_CHIPS_BALANCE).Width <> GRID_WIDTH_AMOUNT_LONG + 400 Then
      If _expected <> 0 Then
        Me.Grid.Column(GRID_COLUMN_CHIPS_BALANCE).Width = GRID_WIDTH_AMOUNT_LONG + 400
      End If
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CHIPS_BALANCE).Value = GUI_FormatCurrency(_expected, 2)

    ' Chips - Collected
    ' This column should be showed if have values but gaming tables disabled
    If Me.Grid.Column(GRID_COLUMN_CHIPS_COLLECTED).Width <> GRID_WIDTH_AMOUNT_LONG + 400 Then
      If _collected Then
        Me.Grid.Column(GRID_COLUMN_CHIPS_COLLECTED).Width = GRID_WIDTH_AMOUNT_LONG + 400
      End If
    End If

    If DbRow.Value(SQL_COLUMN_STATUS) = WSI.Common.CASHIER_SESSION_STATUS.CLOSED Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CHIPS_COLLECTED).Value = GUI_FormatCurrency(_collected, 2)
    End If

    ' Chips - Difference
    ' This column should be showed if have values but gaming tables disabled
    If Me.Grid.Column(GRID_COLUMN_CHIPS_DIFFERENCE).Width <> GRID_WIDTH_AMOUNT_LONG + 400 Then
      If _difference <> 0 Then
        Me.Grid.Column(GRID_COLUMN_CHIPS_DIFFERENCE).Width = GRID_WIDTH_AMOUNT_LONG + 400
      End If
    End If

    If DbRow.Value(SQL_COLUMN_STATUS) = WSI.Common.CASHIER_SESSION_STATUS.CLOSED Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CHIPS_DIFFERENCE).Value = GUI_FormatCurrency(_difference, 2)
    End If

    If CurrencyExchange.IsCashAdvanceEnabled() Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_ADVANCE).Value = GUI_FormatCurrency(_session_data.total_cash_advance, 2)
    End If

    Return True
  End Function

  ' PURPOSE : Draw the column values for all currencies
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - SessionStaus
  '           - SessionData
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  Private Function DrawCurrencies(ByVal RowIndex As Integer, ByVal SessionStatus As WSI.Common.CASHIER_SESSION_STATUS, ByVal SessionData As WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS) As Boolean
    Dim _column_offset As Integer
    Dim _total_currency_index As Integer
    Dim _expected_currencies As Double
    Dim _collected_currencies As Double
    Dim _difference_currencies As Double

    _column_offset = 0
    _total_currency_index = 0

    ' Other currencies
    For Each _cit As CurrencyIsoType In m_currency_exchange_iso_types

      ' Expected column
      _expected_currencies = GetIsoCodeTypeValue(_cit, SessionData.currencies_balance)
      ' Collected column
      _collected_currencies = GetIsoCodeTypeValue(_cit, SessionData.collected)
      ' Difference column
      _difference_currencies = _collected_currencies - _expected_currencies

      ' Expected column
      If _expected_currencies <> 0 Or _collected_currencies <> 0 Then
        Me.Grid.Cell(RowIndex, GRID_COLUMNS + _column_offset).Value = Currency.Format(_expected_currencies, m_currency_exchange_iso_codes(_total_currency_index))
        total_currency_exchange_expected(_total_currency_index) += _expected_currencies
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMNS + _column_offset).Value = String.Empty
      End If
      _column_offset += 1

      If m_both_company_selected Then
        ' Collected column
        If SessionStatus = WSI.Common.CASHIER_SESSION_STATUS.CLOSED AndAlso (_expected_currencies <> 0 Or _collected_currencies <> 0) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMNS + _column_offset).Value = Currency.Format(_collected_currencies, _cit.IsoCode)
          total_currency_exchange_collected(_total_currency_index) += _collected_currencies
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMNS + _column_offset).Value = String.Empty
        End If
        _column_offset += 1

        ' Difference column
        If SessionStatus = WSI.Common.CASHIER_SESSION_STATUS.CLOSED AndAlso (_expected_currencies <> 0 Or _collected_currencies <> 0) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMNS + _column_offset).Value = Currency.Format(_difference_currencies, _cit.IsoCode)
          total_currency_exchange_difference(_total_currency_index) += _difference_currencies
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMNS + _column_offset).Value = String.Empty
        End If
        _column_offset += 1
      Else
        ' Jump Collected and Difference columns
        _column_offset += 2
      End If

      _total_currency_index += 1
    Next ' _cit As CurrencyIsoType

    Return True
  End Function

  Private Function DrawChips(ByVal RowIndex As Integer, ByVal SessionStatus As WSI.Common.CASHIER_SESSION_STATUS, ByVal SessionData As WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS) As Boolean
    Dim _column_offset As Integer
    Dim _total_currency_index As Integer
    Dim _expected_currencies As Double
    Dim _collected_currencies As Double
    Dim _difference_currencies As Double
    Dim _cit As CurrencyIsoType

    _column_offset = (m_currency_exchange_iso_types.Count * 3)
    _total_currency_index = 0

    ' Other currencies
    For Each _row As DataRow In m_dt_chips.Rows

      _cit = New CurrencyIsoType(_row("CHS_ISO_CODE"), _row("TYPE"))

      ' Expected column
      _expected_currencies = GetIsoCodeTypeValue(_cit, SessionData.currencies_balance)
      ' Collected column
      _collected_currencies = GetIsoCodeTypeValue(_cit, SessionData.collected)
      ' Difference column
      _difference_currencies = _collected_currencies - _expected_currencies

      ' Expected column
      If _expected_currencies <> 0 Or _collected_currencies <> 0 Then
        If _expected_currencies = 0 Then
          Me.Grid.Cell(RowIndex, GRID_COLUMNS + _column_offset).Value = String.Empty
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMNS + _column_offset).Value = Currency.Format(_expected_currencies, "") '_cit.IsoCode)
        End If
        m_total_currency_chips_expected(_total_currency_index) += _expected_currencies
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMNS + _column_offset).Value = String.Empty
      End If
      _column_offset += 1

      If m_both_company_selected Then
        ' Collected column
        If SessionStatus = WSI.Common.CASHIER_SESSION_STATUS.CLOSED AndAlso (_expected_currencies <> 0 Or _collected_currencies <> 0) Then
          If _collected_currencies = 0 Then
            Me.Grid.Cell(RowIndex, GRID_COLUMNS + _column_offset).Value = String.Empty
          Else
            Me.Grid.Cell(RowIndex, GRID_COLUMNS + _column_offset).Value = Currency.Format(_collected_currencies, "") '_cit.IsoCode)
          End If
          m_total_currency_chips_collected(_total_currency_index) += _collected_currencies
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMNS + _column_offset).Value = String.Empty
        End If
        _column_offset += 1

        ' Difference column
        If SessionStatus = WSI.Common.CASHIER_SESSION_STATUS.CLOSED AndAlso (_expected_currencies <> 0 Or _collected_currencies <> 0) Then
          If _difference_currencies = 0 Then
            Me.Grid.Cell(RowIndex, GRID_COLUMNS + _column_offset).Value = String.Empty
          Else
            Me.Grid.Cell(RowIndex, GRID_COLUMNS + _column_offset).Value = Currency.Format(_difference_currencies, "") '_cit.IsoCode)
          End If
          m_total_currency_chips_difference(_total_currency_index) += _difference_currencies
        Else
          If Not _cit.IsoCode = "X02" Then
            Me.Grid.Cell(RowIndex, GRID_COLUMNS + _column_offset).Value = String.Empty
          End If
        End If
        _column_offset += 1
      Else
        ' Jump Collected and Difference columns
        _column_offset += 2
      End If

      _total_currency_index += 1
    Next ' _cit As CurrencyIsoType

    Return True
  End Function

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.dtp_from
  End Sub ' GUI_SetInitialFocus

  Protected Overrides Sub Finalize()
    MyBase.Finalize()
  End Sub ' Finalize

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.FilterHeaderWidth(1) = 2000
    PrintData.SetFilter(gb_date.Text, m_date_by)
    PrintData.SetFilter(gb_date.Text & " " & dtp_from.Text, m_filter_from)
    PrintData.SetFilter(gb_date.Text & " " & dtp_to.Text, m_filter_to)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4445), m_hidde_system_sessions)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(209), m_cashiers)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(220), m_users)

    If WSI.Common.Misc.IsFloorDualCurrencyEnabled Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1057), m_floor_dual_currency)
    End If

    PrintData.FilterValueWidth(3) = 4000
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(152), String.Join(",", m_state.ToArray()))
    If opt_company_b.Visible Then
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(150), m_company)
    Else
      PrintData.SetFilter("", "", True)
    End If

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set form specific requirements/parameters for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Dim _column_offset As Integer = 0

    ' Prevent date/time column from being squeezed too much and drop the time part to the following line
    PrintData.Settings.Columns(GRID_COLUMN_OPENING_DATE).IsWidthFixed = True
    PrintData.Settings.Columns(GRID_COLUMN_CLOSE_DATE).IsWidthFixed = True

    With Me.Grid
      .Column(GRID_COLUMN_PROMO_NO_REDEEMABLE).IsColumnPrintable = False
      .Column(GRID_COLUMN_PROMO_REDEEMABLE).IsColumnPrintable = False
      .Column(GRID_COLUMN_NO_REDEEMABLE_TO_REDEEMABLE).IsColumnPrintable = False
      .Column(GRID_COLUMN_DEPOSITS_LESS_WITHDRAWALS).IsColumnPrintable = False
      .Column(GRID_COLUMN_INPUTS_LESS_OUTPUTS).IsColumnPrintable = False
      .Column(GRID_COLUMN_EXPECTED).IsColumnPrintable = False
      .Column(GRID_COLUMN_STATUS).IsColumnPrintable = False
      .Column(GRID_COLUMN_CASHIER_NAME).IsColumnPrintable = False
      .Column(GRID_COLUMN_CHECK_PAYMENT).IsColumnPrintable = False
      .Column(GRID_COLUMN_CS_LIMIT_SALE).IsColumnPrintable = False
      .Column(GRID_COLUMN_MB_LIMIT_SALE).IsColumnPrintable = False

      .Column(GRID_COLUMN_TOTAL_BANK_CARD).IsColumnPrintable = False
      .Column(GRID_COLUMN_COMMISSIONS_BANK_CARD).IsColumnPrintable = False
      .Column(GRID_COLUMN_TOTAL_CURRENCY_EXCHANGE).IsColumnPrintable = False
      .Column(GRID_COLUMN_COMMISSIONS_CUR_EXCH).IsColumnPrintable = False
      .Column(GRID_COLUMN_TAXES_CASH_IN).IsColumnPrintable = False
      .Column(GRID_COLUMN_TAX_CUSTODY).IsColumnPrintable = False
      .Column(GRID_COLUMN_TAX_PROVISIONS).IsColumnPrintable = False
      .Column(GRID_COLUMN_TOTAL_CHECK_RECHARGES).IsColumnPrintable = False
      .Column(GRID_COLUMN_COMMISSIONS_CHECK).IsColumnPrintable = False
      .Column(GRID_COLUMN_TAX_CUSTODY_RETURN).IsColumnPrintable = False

      For Each _iso_code As String In m_currency_exchange_iso_codes
        ' Expected column
        .Column(GRID_COLUMNS + _column_offset).IsColumnPrintable = False
        _column_offset += 1

        ' Collected column
        .Column(GRID_COLUMNS + _column_offset).IsColumnPrintable = False
        _column_offset += 1

        ' Difference column          
        .Column(GRID_COLUMNS + _column_offset).IsColumnPrintable = False
        _column_offset += 1
      Next

    End With

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Params.Title = GLB_NLS_GUI_INVOICING.GetString(227)

    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)
    Dim _column_offset As Integer = 0

    With Me.Grid
      If m_company_b Then
        .Column(GRID_COLUMN_PROMO_NO_REDEEMABLE).IsColumnPrintable = False
        .Column(GRID_COLUMN_PROMO_REDEEMABLE).IsColumnPrintable = False
        .Column(GRID_COLUMN_NO_REDEEMABLE_TO_REDEEMABLE).IsColumnPrintable = False
      Else
        .Column(GRID_COLUMN_PROMO_NO_REDEEMABLE).IsColumnPrintable = True
        .Column(GRID_COLUMN_PROMO_REDEEMABLE).IsColumnPrintable = True
        .Column(GRID_COLUMN_NO_REDEEMABLE_TO_REDEEMABLE).IsColumnPrintable = False
      End If
      .Column(GRID_COLUMN_DEPOSITS_LESS_WITHDRAWALS).IsColumnPrintable = True
      .Column(GRID_COLUMN_INPUTS_LESS_OUTPUTS).IsColumnPrintable = True
      .Column(GRID_COLUMN_EXPECTED).IsColumnPrintable = True
      .Column(GRID_COLUMN_STATUS).IsColumnPrintable = True
      .Column(GRID_COLUMN_CASHIER_NAME).IsColumnPrintable = True
      .Column(GRID_COLUMN_CHECK_PAYMENT).IsColumnPrintable = True
      .Column(GRID_COLUMN_CS_LIMIT_SALE).IsColumnPrintable = True
      .Column(GRID_COLUMN_MB_LIMIT_SALE).IsColumnPrintable = True

      .Column(GRID_COLUMN_TOTAL_BANK_CARD).IsColumnPrintable = True
      .Column(GRID_COLUMN_COMMISSIONS_BANK_CARD).IsColumnPrintable = True
      .Column(GRID_COLUMN_TOTAL_CURRENCY_EXCHANGE).IsColumnPrintable = True
      .Column(GRID_COLUMN_COMMISSIONS_CUR_EXCH).IsColumnPrintable = True
      .Column(GRID_COLUMN_TAXES_CASH_IN).IsColumnPrintable = True
      .Column(GRID_COLUMN_TAX_CUSTODY).IsColumnPrintable = True
      .Column(GRID_COLUMN_TAX_PROVISIONS).IsColumnPrintable = True
      .Column(GRID_COLUMN_TOTAL_CHECK_RECHARGES).IsColumnPrintable = True
      .Column(GRID_COLUMN_COMMISSIONS_CHECK).IsColumnPrintable = True
      .Column(GRID_COLUMN_TAX_CUSTODY_RETURN).IsColumnPrintable = True

      If Not m_currency_exchange_iso_codes Is Nothing Then
        For Each _iso_code As String In m_currency_exchange_iso_codes
          ' Expected column
          .Column(GRID_COLUMNS + _column_offset).IsColumnPrintable = True
          _column_offset += 1

          ' Collected column
          .Column(GRID_COLUMNS + _column_offset).IsColumnPrintable = True
          _column_offset += 1

          ' Difference column          
          .Column(GRID_COLUMNS + _column_offset).IsColumnPrintable = True
          _column_offset += 1
        Next
      End If

    End With

    Call MyBase.GUI_ReportParams(ExcelData)

  End Sub

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_cashiers = ""
    m_users = ""
    m_filter_from = ""
    m_filter_to = ""
    m_session = ""
    m_company = ""
    m_date_by = ""
    m_hidde_system_sessions = ""
    m_both_company_selected = False
    m_state = New List(Of String)

    m_from_date = Nothing
    m_to_date = Nothing

    m_opt_gaming_day = False

    'Date 
    If opt_opening_session.Checked Then
      m_date_by = opt_opening_session.Text
    ElseIf opt_movement_session.Checked Then
      m_date_by = opt_movement_session.Text
    ElseIf opt_movement_gaming_day.Checked Then
      m_date_by = opt_movement_gaming_day.Text
      m_opt_gaming_day = True
    End If
    If Me.dtp_from.Checked Then
      m_from_date = dtp_from.Value
      m_filter_from = GUI_FormatDate(dtp_from.Value, _
                    ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Me.dtp_to.Checked Then
      m_to_date = dtp_to.Value
      m_filter_to = GUI_FormatDate(dtp_to.Value, _
                  ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Cashiers
    If Me.opt_all_cashiers.Checked Then
      m_cashiers = Me.opt_all_cashiers.Text
    Else
      m_cashiers = Me.cmb_cashier.TextValue
    End If

    If WSI.Common.Misc.IsFloorDualCurrencyEnabled Then
      m_floor_dual_currency = ""
      m_floor_dual_currency = Me.cmb_currencies.TextValue
    End If

    ' Users
    If Me.opt_all_users.Checked Then
      m_users = Me.opt_all_users.Text
    Else
      m_users = Me.cmb_user.TextValue
    End If

    'State
    If Me.chk_session_open.Checked = True Then
      m_state.Add(GLB_NLS_GUI_INVOICING.GetString(153))
    End If

    If chk_session_closed.Checked = True Then
      m_state.Add(GLB_NLS_GUI_INVOICING.GetString(154))
    End If

    If chk_session_pending.Checked = True Then
      m_state.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1913))
    End If

    ' Company / Companies
    If opt_company_both.Checked Then
      m_both_company_selected = True
      m_company = GLB_NLS_GUI_INVOICING.GetString(151)
    ElseIf opt_company_a.Checked Then
      m_company = opt_company_a.Text
    ElseIf opt_company_b.Checked Then
      m_company = opt_company_b.Text
    End If

    ' System sessions
    m_hidde_system_sessions = IIf(Me.chk_not_show_system_sessions.Checked And Me.chk_not_show_system_sessions.Enabled, GLB_NLS_GUI_PLAYER_TRACKING.GetString(359), GLB_NLS_GUI_PLAYER_TRACKING.GetString(360))

    m_company_b = opt_company_b.Checked

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  Private Function GetSessionsFromMovements() As String

    Dim _sb As StringBuilder
    Dim _from_param As SqlParameter
    Dim _to_param As SqlParameter
    Dim _list As List(Of String)
    Dim _where_date As String

    _where_date = ""
    _list = New List(Of String)
    _from_param = New SqlParameter()
    _to_param = New SqlParameter()

    If dtp_from.Checked Then
      _from_param = New SqlParameter("@pFrom", SqlDbType.DateTime)
      _from_param.Value = dtp_from.Value
    End If
    If dtp_to.Checked Then
      _to_param = New SqlParameter("@pTo", SqlDbType.DateTime)
      _to_param.Value = dtp_to.Value
    End If

    _where_date = WSI.Common.Misc.DateSQLFilter("CM_DATE", _from_param.ParameterName, _to_param.ParameterName, True)

    If _where_date <> "" Then
      _sb = New StringBuilder()
      _sb.AppendLine(" SELECT DISTINCT   CM_SESSION_ID     ")
      _sb.AppendLine("            FROM   CASHIER_MOVEMENTS ")
      _sb.AppendLine(_where_date)

      Try
        Using _db_trx As New DB_TRX()
          Using _cmd As New SqlCommand(_sb.ToString())

            If _from_param.ParameterName <> "" Then
              _cmd.Parameters.Add(_from_param)
            End If
            If _to_param.ParameterName <> "" Then
              _cmd.Parameters.Add(_to_param)
            End If

            Using _reader As SqlDataReader = _db_trx.ExecuteReader(_cmd)
              While (_reader.Read())
                If Not _reader.IsDBNull(0) Then
                  _list.Add(Convert.ToString(_reader.GetInt64(0)))
                End If
              End While
            End Using
          End Using
        End Using

        If _list.Count > 0 Then

          Return String.Join(",", _list.ToArray())
        Else

          Return "-1" ' Empty results
        End If

      Catch _ex As Exception
        Log.Exception(_ex)
      End Try
    End If

    Return ""

  End Function

  Private Function GetSessionsFromGamingDays() As String

    Dim _sb As StringBuilder
    Dim _from_param As SqlParameter
    Dim _to_param As SqlParameter
    Dim _list As List(Of String)
    Dim _where_date As String

    _where_date = ""
    _list = New List(Of String)
    _from_param = New SqlParameter()
    _to_param = New SqlParameter()

    If dtp_from.Checked Then
      _from_param = New SqlParameter("@pFrom", SqlDbType.DateTime)
      _from_param.Value = dtp_from.Value
    End If
    If dtp_to.Checked Then
      _to_param = New SqlParameter("@pTo", SqlDbType.DateTime)
      _to_param.Value = dtp_to.Value
    End If

    _where_date = WSI.Common.Misc.DateSQLFilter("CS_GAMING_DAY", _from_param.ParameterName, _to_param.ParameterName, True)

    If _where_date <> "" Then
      _sb = New StringBuilder()
      _sb.AppendLine(" SELECT   CS_SESSION_ID     ")
      _sb.AppendLine("   FROM   CASHIER_SESSIONS  ")
      _sb.AppendLine(_where_date)

      Try
        Using _db_trx As New DB_TRX()
          Using _cmd As New SqlCommand(_sb.ToString())

            If _from_param.ParameterName <> "" Then
              _cmd.Parameters.Add(_from_param)
            End If
            If _to_param.ParameterName <> "" Then
              _cmd.Parameters.Add(_to_param)
            End If

            Using _reader As SqlDataReader = _db_trx.ExecuteReader(_cmd)
              While (_reader.Read())
                If Not _reader.IsDBNull(0) Then
                  _list.Add(Convert.ToString(_reader.GetInt64(0)))
                End If
              End While
            End Using
          End Using
        End Using

        If _list.Count > 0 Then

          Return String.Join(",", _list.ToArray())
        Else

          Return "-1" ' Empty results
        End If

      Catch _ex As Exception
        Log.Exception(_ex)
      End Try
    End If

    Return ""

  End Function

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub GUI_StyleSheet()
    Dim _total_columns As Integer
    Dim _column_offset As Integer

    _total_columns = GRID_COLUMNS

    If Not m_currency_exchange_iso_codes Is Nothing Then
      ' Three new columns for each iso code: Expected, collected and difference
      _total_columns += (m_currency_exchange_iso_codes.Count() * 3)
      _total_columns += (m_dt_chips.Rows.Count() * 3)
    End If

    With Me.Grid
      Call .Init(_total_columns, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Cash Session
      ' Session Id
      .Column(GRID_COLUMN_SESSION_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_SESSION_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_SESSION_ID).Width = 0
      .Column(GRID_COLUMN_SESSION_ID).IsColumnPrintable = False

      ' Session Name
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_SESSION_NAME, GLB_NLS_GUI_PLAYER_TRACKING.GetString(629), 0, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT, "")

      ' Open Date
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_OPENING_DATE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(629), GRID_WIDTH_DATE, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER, GLB_NLS_GUI_INVOICING.GetString(244))

      ' Close Date
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_CLOSE_DATE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(629), GRID_WIDTH_DATE, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER, GLB_NLS_GUI_INVOICING.GetString(266))

      ' Cashier Id
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_CASHIER_ID, GLB_NLS_GUI_PLAYER_TRACKING.GetString(629), 0, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER, "")

      ' Cashier Name
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_CASHIER_NAME, GLB_NLS_GUI_PLAYER_TRACKING.GetString(629), GRID_WIDTH_CASHIER, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT, GLB_NLS_GUI_INVOICING.GetString(209))

      ' User Id
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_USER_ID, GLB_NLS_GUI_PLAYER_TRACKING.GetString(629), 0)

      ' User Name
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_USER_NAME, GLB_NLS_GUI_PLAYER_TRACKING.GetString(629), GRID_WIDTH_USER, , GLB_NLS_GUI_INVOICING.GetString(220))

      ' Gaming Day
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_GAMING_DAY, GLB_NLS_GUI_PLAYER_TRACKING.GetString(629), IIf(m_gaming_day, GRID_WIDTH_DATE, 0), , GLB_NLS_GUI_PLAYER_TRACKING.GetString(6177), m_gaming_day)

      ' State
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_STATUS, GLB_NLS_GUI_PLAYER_TRACKING.GetString(629), 300, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER, GLB_NLS_GUI_INVOICING.GetString(155))

      ' Cash Result
      ' Card In
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_CARD_IN, GLB_NLS_GUI_INVOICING.GetString(360), GRID_WIDTH_AMOUNT_LONG, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_INVOICING.GetString(289))

      ' Card Out
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_CARD_OUT, GLB_NLS_GUI_INVOICING.GetString(360), GRID_WIDTH_AMOUNT_LONG, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_INVOICING.GetString(290))

      ' Inputs less outputs
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_INPUTS_LESS_OUTPUTS, GLB_NLS_GUI_INVOICING.GetString(360), GRID_WIDTH_AMOUNT_LONG + 200, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1118))

      ' Taxes
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_TAXES, GLB_NLS_GUI_INVOICING.GetString(360), GRID_WIDTH_AMOUNT_SHORT, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_INVOICING.GetString(207))

      ' Session Result
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_SESSION_RESULT, GLB_NLS_GUI_INVOICING.GetString(360), 1600, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_INVOICING.GetString(297))

      'Cash Balance
      ' Deposits
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_DEPOSITS, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2682), GRID_WIDTH_AMOUNT_LONG, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_INVOICING.GetString(496))

      ' Withdraws
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_WITHDRAWS, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2682), GRID_WIDTH_AMOUNT_LONG, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_INVOICING.GetString(497))

      ' Deposits less withdrawals
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_DEPOSITS_LESS_WITHDRAWALS, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2682), GRID_WIDTH_AMOUNT_LONG + 600, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1117))

      ' Card In
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_CASH_CARD_IN, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2682), GRID_WIDTH_AMOUNT_LONG, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_INVOICING.GetString(289))

      ' Card Out
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_CASH_CARD_OUT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2682), GRID_WIDTH_AMOUNT_LONG, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_INVOICING.GetString(290))

      ' Check Payment
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_CHECK_PAYMENT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2682), GRID_WIDTH_AMOUNT_LONG + 200, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1601))

      ' Bank Card Operations
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_TOTAL_BANK_CARD, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2682), GRID_WIDTH_AMOUNT_LONG + 400, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2220))

      ' Check Operations
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_TOTAL_CHECK_RECHARGES, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2682), GRID_WIDTH_AMOUNT_LONG + 400, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2603))

      ' Currency Exchange Recharge
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_TOTAL_CURRENCY_EXCHANGE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2682), GRID_WIDTH_AMOUNT_LONG + 300, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2079))

      'Chip Sale Register
      .Column(GRID_COLUMN_TOTAL_CHIP_SALE_REGISTER).Header(0).Text = ""
      .Column(GRID_COLUMN_TOTAL_CHIP_SALE_REGISTER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5086)
      If m_chip_sale_register_enabled Then
        .Column(GRID_COLUMN_TOTAL_CHIP_SALE_REGISTER).Width = GRID_WIDTH_AMOUNT_LONG + 300
      Else
        .Column(GRID_COLUMN_TOTAL_CHIP_SALE_REGISTER).Width = 1
      End If
      .Column(GRID_COLUMN_TOTAL_CURRENCY_EXCHANGE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Inputs less outputs
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_EXPECTED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2682), GRID_WIDTH_AMOUNT_LONG + 200, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(645))

      ' Collected
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_COLLECTED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2682), GRID_WIDTH_AMOUNT_LONG, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_INVOICING.GetString(498))

      ' TOTAL Difference
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_DIFFERENCE_TOTAL, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2682), GRID_WIDTH_CASHIER, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4762))

      ' CASHIER Difference 
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_DIFFERENCE_CASHIER, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2682), GRID_WIDTH_CASHIER, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4763))

      ' MB Difference / Stacker Difference
      .Column(GRID_COLUMN_DIFFERENCE_MB).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2682)
      If m_is_tito_mode Then
        .Column(GRID_COLUMN_DIFFERENCE_MB).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4764) ' Stacker diff.
      Else
        .Column(GRID_COLUMN_DIFFERENCE_MB).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4761) ' MB Diff.
      End If
      .Column(GRID_COLUMN_DIFFERENCE_MB).Width = GRID_WIDTH_CASHIER
      .Column(GRID_COLUMN_DIFFERENCE_MB).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' BM Pending / Stacker Pending
      .Column(GRID_COLUMN_PENDING_MB).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2682)
      If Not m_is_tito_mode Then
        .Column(GRID_COLUMN_SHORTFALL_EXCESS_MB).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2682)
      End If

      If m_is_tito_mode Then
        .Column(GRID_COLUMN_PENDING_MB).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4730) ' Stacker pending
        .Column(GRID_COLUMN_PENDING_MB).Width = GRID_WIDTH_AMOUNT_LONG + 100
        .Column(GRID_COLUMN_SHORTFALL_EXCESS_MB).Width = 0
      Else
        .Column(GRID_COLUMN_PENDING_MB).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1359) ' MB Pending
        .Column(GRID_COLUMN_PENDING_MB).Width = GRID_WIDTH_AMOUNT_LONG
        If MobileBank.GP_GetRegisterShortfallOnDeposit() = True Then
          'Cash excess - Shortfall
          .Column(GRID_COLUMN_SHORTFALL_EXCESS_MB).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5663) ' MB Cash excess / Shortfall
          .Column(GRID_COLUMN_SHORTFALL_EXCESS_MB).Width = GRID_WIDTH_AMOUNT_LONG + 400
        Else
          'Cash excess
          .Column(GRID_COLUMN_SHORTFALL_EXCESS_MB).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5713) ' MB Cash excess
          .Column(GRID_COLUMN_SHORTFALL_EXCESS_MB).Width = GRID_WIDTH_AMOUNT_LONG
        End If
      End If
      .Column(GRID_COLUMN_PENDING_MB).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_SHORTFALL_EXCESS_MB).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Inputs
      ' A
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_INPUTS_A, GLB_NLS_GUI_INVOICING.GetString(289), GRID_WIDTH_NAME, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, m_split_data.company_a.name)

      ' B
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_INPUTS_B, GLB_NLS_GUI_INVOICING.GetString(289), GRID_WIDTH_AMOUNT_LONG + 700, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, m_split_data.company_b.name)

      ' Card usage
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_CARD_USAGE, GLB_NLS_GUI_INVOICING.GetString(289), GRID_WIDTH_NAME, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5767))

      ' Service Charge
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_SERVICE_CHARGE, GLB_NLS_GUI_INVOICING.GetString(289), GRID_WIDTH_NAME, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1127))

      ' Bank Card Commissions //EOR 19-JUN-2017
      If WSI.Common.Misc.IsVoucherModeTV() Then
        Call GUI_AddColumn(Me.Grid, GRID_COLUMN_COMMISSIONS_BANK_CARD, GLB_NLS_GUI_INVOICING.GetString(289), GRID_WIDTH_AMOUNT_LONG + 700, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7418)))
      Else
        Call GUI_AddColumn(Me.Grid, GRID_COLUMN_COMMISSIONS_BANK_CARD, GLB_NLS_GUI_INVOICING.GetString(289), GRID_WIDTH_AMOUNT_LONG + 700, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2376))
      End If

      ' Check Commissions    
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_COMMISSIONS_CHECK, GLB_NLS_GUI_INVOICING.GetString(289), GRID_WIDTH_AMOUNT_LONG + 400, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2604))

      ' Currency Exchange  Commissions
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_COMMISSIONS_CUR_EXCH, GLB_NLS_GUI_INVOICING.GetString(289), GRID_WIDTH_AMOUNT_LONG + 700, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2377))

      ' CashIn Tax
      If m_cash_in_tax_enabled Then
        .Column(GRID_COLUMN_TAXES_CASH_IN).Width = GRID_WIDTH_NAME
      Else
        .Column(GRID_COLUMN_TAXES_CASH_IN).Width = 1
      End If
      .Column(GRID_COLUMN_TAXES_CASH_IN).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(289)
      .Column(GRID_COLUMN_TAXES_CASH_IN).Header(1).Text = GeneralParam.GetString("Cashier", "CashInTax.Name", GLB_NLS_GUI_PLAYER_TRACKING.GetString(4562))
      .Column(GRID_COLUMN_TAXES_CASH_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Tax custody
      If m_tax_custody_enabled Then
        .Column(GRID_COLUMN_TAX_CUSTODY).Width = GRID_WIDTH_NAME
      Else
        .Column(GRID_COLUMN_TAX_CUSTODY).Width = 1
      End If
      .Column(GRID_COLUMN_TAX_CUSTODY).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(289)
      .Column(GRID_COLUMN_TAX_CUSTODY).Header(1).Text = WSI.Common.Misc.TaxCustodyName()
      .Column(GRID_COLUMN_TAX_CUSTODY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Tax provisions
      If m_tax_provisions_enabled Then
        .Column(GRID_COLUMN_TAX_PROVISIONS).Width = GRID_WIDTH_NAME
      Else
        .Column(GRID_COLUMN_TAX_PROVISIONS).Width = 1
      End If
      .Column(GRID_COLUMN_TAX_PROVISIONS).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(289)
      .Column(GRID_COLUMN_TAX_PROVISIONS).Header(1).Text = GeneralParam.GetString("Cashier.TaxProvisions", "Voucher.Title", GLB_NLS_GUI_PLAYER_TRACKING.GetString(7151))
      .Column(GRID_COLUMN_TAX_PROVISIONS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Outputs
      ' A
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_OUTPUTS_A, GLB_NLS_GUI_INVOICING.GetString(290), GRID_WIDTH_TAX_RETURNING, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, m_split_data.company_a.name)

      ' B
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_OUTPUTS_B, GLB_NLS_GUI_INVOICING.GetString(290), GRID_WIDTH_TAX_RETURNING, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, m_split_data.company_b.name)

      ' Refound Card Usage
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_REFUND_CARD_USAGE, GLB_NLS_GUI_INVOICING.GetString(290), GRID_WIDTH_TAX_RETURNING, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5767))

      ' Refound Card Usage
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_TOTAL_PRIZES, GLB_NLS_GUI_INVOICING.GetString(290), GRID_WIDTH_TAX_RETURNING, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_INVOICING.GetString(485))

      ' Tax custody
      If m_tax_custody_enabled Then
        .Column(GRID_COLUMN_TAX_CUSTODY_RETURN).Width = GRID_WIDTH_NAME
      Else
        .Column(GRID_COLUMN_TAX_CUSTODY_RETURN).Width = 1
      End If
      .Column(GRID_COLUMN_TAX_CUSTODY_RETURN).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(290)
      .Column(GRID_COLUMN_TAX_CUSTODY_RETURN).Header(1).Text = WSI.Common.Misc.TaxCustodyRefundName()
      .Column(GRID_COLUMN_TAX_CUSTODY_RETURN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Decimal Rounding
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_DECIMAL_ROUNDING, GLB_NLS_GUI_INVOICING.GetString(290), GRID_WIDTH_TAX_RETURNING + 200, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1126))


      ' Prize
      ' Prizes
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_PRIZES, GLB_NLS_GUI_INVOICING.GetString(298), GRID_WIDTH_TAX_RETURNING, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_INVOICING.GetString(298))

      ' Prizes Taxes
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_PRIZES_TAXES, GLB_NLS_GUI_INVOICING.GetString(298), GRID_WIDTH_TAX_RETURNING, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_INVOICING.GetString(486))

      ' Tax Returning
      .Column(GRID_COLUMN_TAX_RETURNING).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(298)
      .Column(GRID_COLUMN_TAX_RETURNING).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(483)
      .Column(GRID_COLUMN_TAX_RETURNING).Width = GRID_WIDTH_TAX_RETURNING
      .Column(GRID_COLUMN_TAX_RETURNING).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      If m_hide_promotion_unr Then
        .Column(GRID_COLUMN_UNR_KIND1).Width = 1
        .Column(GRID_COLUMN_UNR_KIND2).Width = 1
      Else
        .Column(GRID_COLUMN_UNR_KIND1).Width = GRID_WIDTH_TAX_RETURNING
        .Column(GRID_COLUMN_UNR_KIND2).Width = GRID_WIDTH_TAX_RETURNING
      End If

      ' UNR kind 1
      .Column(GRID_COLUMN_UNR_KIND1).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(298)
      .Column(GRID_COLUMN_UNR_KIND1).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2788, "1")
      .Column(GRID_COLUMN_UNR_KIND1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' UNR kind 2
      .Column(GRID_COLUMN_UNR_KIND2).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(298)
      .Column(GRID_COLUMN_UNR_KIND2).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2788, "2")
      .Column(GRID_COLUMN_UNR_KIND2).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Redeemable as CASH IN
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_REDEEMABLE_AS_CASHIN, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2560), GRID_WIDTH_AMOUNT_LONG + 300, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_INVOICING.GetString(289))

      ' Redeemable as CASH OUT
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_REDEEMABLE_AS_CASHOUT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2560), GRID_WIDTH_AMOUNT_LONG + 300, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_INVOICING.GetString(290))

      ' Handpays
      ' Handpay
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_HANDPAYS, GLB_NLS_GUI_INVOICING.GetString(481), GRID_WIDTH_TAX_RETURNING, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_INVOICING.GetString(159))

      ' Cancelation Handpay
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_CANCELATION_HANDPAYS, GLB_NLS_GUI_INVOICING.GetString(481), GRID_WIDTH_TAX_RETURNING, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2786))

      ' Total Handpay
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_TOTAL_HANDPAYS, GLB_NLS_GUI_INVOICING.GetString(481), GRID_WIDTH_TAX_RETURNING, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_INVOICING.GetString(488))

      ' Promotions
      ' Promo Redeemable
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_PROMO_REDEEMABLE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(211), 2000, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_CONFIGURATION.GetString(40))

      ' Promo No Redeemable
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_PROMO_NO_REDEEMABLE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(211), 2250, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_CONFIGURATION.GetString(41))

      ' Promo No Redeemable to Redeemable 
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_NO_REDEEMABLE_TO_REDEEMABLE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(211), 3000, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_CONFIGURATION.GetString(42))

      ' Promo Cancel Redeemable
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_CANCEL_PROMO_REDEEMABLE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(211), 3000, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_INVOICING.GetString(162))

      ' Promo Cancel No Redeemable
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_CANCEL_PROMO_NO_REDEEMABLE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(211), 3500, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_INVOICING.GetString(161))

      ' Expirations
      ' Devolutions
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_EXPIRATIONS_DEV, GLB_NLS_GUI_INVOICING.GetString(163), 3000, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_INVOICING.GetString(164))

      ' Prizes
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_EXPIRATIONS_PRIZE, GLB_NLS_GUI_INVOICING.GetString(163), 3000, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_INVOICING.GetString(165))

      ' Promo No Redeemable
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_EXPIRATIONS_PROMO_NR, GLB_NLS_GUI_INVOICING.GetString(163), 3000, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_INVOICING.GetString(166))

      ' Other
      ' Limit Sale
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_CS_LIMIT_SALE, "", GRID_WIDTH_AMOUNT_LONG + 200, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1176))

      ' Limit Sale MB
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_MB_LIMIT_SALE, "", IIf(m_is_tito_mode = True, 0, GRID_WIDTH_AMOUNT_LONG + 600), uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1178))

      ' Cash Advance
      .Column(GRID_COLUMN_CASH_ADVANCE).Header(0).Text = ""
      .Column(GRID_COLUMN_CASH_ADVANCE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5461)
      .Column(GRID_COLUMN_CASH_ADVANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      If CurrencyExchange.IsCashAdvanceEnabled() Then
        .Column(GRID_COLUMN_CASH_ADVANCE).Width = GRID_WIDTH_AMOUNT_LONG + 600
      Else
        .Column(GRID_COLUMN_CASH_ADVANCE).Width = 1
      End If

      ' Bank Card Balance
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_BANK_CARD_CREDIT_BALANCE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2558), GRID_WIDTH_AMOUNT_LONG + 400, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5576))

      ' Bank Card Balance
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_BANK_CARD_DEBIT_BALANCE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2558), GRID_WIDTH_AMOUNT_LONG + 400, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5577))

      ' Bank Card Balance
      .Column(GRID_COLUMN_BANK_CARD_BALANCE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2558)
      If m_card_differentiate_type Then
        .Column(GRID_COLUMN_BANK_CARD_BALANCE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5578)
      Else
        .Column(GRID_COLUMN_BANK_CARD_BALANCE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(645)
      End If
      .Column(GRID_COLUMN_BANK_CARD_BALANCE).Width = GRID_WIDTH_AMOUNT_LONG + 400
      .Column(GRID_COLUMN_BANK_CARD_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Bank Card Collected
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_BANK_CARD_COLLECTED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2558), GRID_WIDTH_AMOUNT_LONG + 400, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2335))

      ' Bank Card Difference
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_BANK_CARD_DIFFERENCE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2558), GRID_WIDTH_AMOUNT_LONG + 400, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2610))

      ' Check Balance
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_CHECK_BALANCE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1569), GRID_WIDTH_AMOUNT_LONG + 400, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(645))

      ' Check Collected
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_CHECK_COLLECTED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1569), GRID_WIDTH_AMOUNT_LONG + 400, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2335))

      ' Check Difference
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_CHECK_DIFFERENCE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1569), GRID_WIDTH_AMOUNT_LONG + 400, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2610))


      Call ChipsBalance()

      Call SplitBothCompany() 'TODO: Needs to refactor 

      Call StyleSheetSplitB()

      Call SplitBCompany() 'TODO: Needs to refactor 

      ' Cage session data
      .Column(GRID_COLUMN_CGS_SESSION_NAME).Header(0).Text = ""
      .Column(GRID_COLUMN_CGS_SESSION_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3388) ' Sesi�n de b�veda
      .Column(GRID_COLUMN_CGS_SESSION_NAME).Width = IIf(Cage.IsCageEnabled, GRID_COLUMN_WIDTH_CGS_SESSION_NAME, 0)
      .Column(GRID_COLUMN_CGS_SESSION_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_CGS_SESSION_NAME).IsColumnPrintable = True

      _column_offset = ExchangeCurrencies(_column_offset)

      _column_offset = ExchangeChips(_column_offset)



      If Not m_card_differentiate_type Then
        .Column(GRID_COLUMN_BANK_CARD_CREDIT_BALANCE).Header(1).Text = ""
        .Column(GRID_COLUMN_BANK_CARD_CREDIT_BALANCE).IsColumnPrintable = False
        .Column(GRID_COLUMN_BANK_CARD_CREDIT_BALANCE).Width = 0

        .Column(GRID_COLUMN_BANK_CARD_DEBIT_BALANCE).Header(1).Text = ""
        .Column(GRID_COLUMN_BANK_CARD_DEBIT_BALANCE).IsColumnPrintable = False
        .Column(GRID_COLUMN_BANK_CARD_DEBIT_BALANCE).Width = 0
      End If

      .Column(GRID_COLUMN_SERVICE_CHARGE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(289)
      .Column(GRID_COLUMN_SERVICE_CHARGE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1127)
      .Column(GRID_COLUMN_SERVICE_CHARGE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_SERVICE_CHARGE).Width = 1
      .Column(GRID_COLUMN_SERVICE_CHARGE).IsColumnPrintable = False

    End With

    If opt_movement_session.Checked Then
      Call GridStyleOnMovementDateFilter()
    End If

    Call GridBankCard(True)


    Me.Grid.Width += System.Windows.Forms.SystemInformation.VerticalScrollBarWidth

  End Sub ' GUI_StyleSheet


  ' PURPOSE: Re-Style grid when opt_movement_session is activated.
  '
  '  PARAMS:
  '     - None
  '
  ' RETURNS:
  '     - None
  Private Sub GridStyleOnMovementDateFilter()

    With Me.Grid
      .Column(GRID_COLUMN_PENDING_MB).Width = 0
      .Column(GRID_COLUMN_SHORTFALL_EXCESS_MB).Width = 0
      .Column(GRID_COLUMN_DIFFERENCE_MB).Width = 0
      .Column(GRID_COLUMN_DIFFERENCE_CASHIER).Width = 0
      .Column(GRID_COLUMN_DIFFERENCE_TOTAL).Width = 0
      .Column(GRID_COLUMN_COLLECTED).Width = 0

      .Column(GRID_COLUMN_BANK_CARD_COLLECTED).Width = 0
      .Column(GRID_COLUMN_BANK_CARD_DIFFERENCE).Width = 0
      .Column(GRID_COLUMN_BANK_CARD_BALANCE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5720)
      .Column(GRID_COLUMN_BANK_CARD_DEBIT_BALANCE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5719)
      .Column(GRID_COLUMN_BANK_CARD_CREDIT_BALANCE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5718)

      .Column(GRID_COLUMN_CHIPS_COLLECTED).Width = 0
      .Column(GRID_COLUMN_CHIPS_DIFFERENCE).Width = 0
      .Column(GRID_COLUMN_CHIPS_BALANCE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(541)

      .Column(GRID_COLUMN_CHECK_DIFFERENCE).Width = 0
      .Column(GRID_COLUMN_CHECK_COLLECTED).Width = 0
      .Column(GRID_COLUMN_CHECK_BALANCE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(541)

      If m_currency_exchange_iso_codes IsNot Nothing Then
        For _index As Integer = 0 To (m_currency_exchange_iso_codes.Count() * 3) - 1
          .Column(GRID_COLUMNS + _index).Width = IIf(.Column(GRID_COLUMNS + _index).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(541), _
                                                     2000, 0)
        Next
      End If
    End With

  End Sub

  ' PURPOSE: Re-Style grid when opt_movement_session is activated.
  '
  '  PARAMS:
  '     - None
  '
  ' RETURNS:
  '     - None
  Private Sub GridBankCard(ByVal Visible As Boolean)
    Dim _width As Int32
    _width = 2000

    If Not Visible Then
      _width = 0
    End If

    With Me.Grid
      .Column(GRID_COLUMN_CASH_RECHARGE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(538)
      .Column(GRID_COLUMN_CASH_RECHARGE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7640)
      .Column(GRID_COLUMN_CASH_RECHARGE).IsColumnPrintable = Visible
      .Column(GRID_COLUMN_CASH_RECHARGE).Width = _width

      .Column(GRID_COLUMN_CREDIT_CARD_RECHARGE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(538)
      .Column(GRID_COLUMN_CREDIT_CARD_RECHARGE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7641)
      .Column(GRID_COLUMN_CREDIT_CARD_RECHARGE).IsColumnPrintable = Visible
      .Column(GRID_COLUMN_CREDIT_CARD_RECHARGE).Width = _width



      .Column(GRID_COLUMN_VOUCHERS_COUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7494)
      .Column(GRID_COLUMN_VOUCHERS_COUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7495)
      .Column(GRID_COLUMN_VOUCHERS_COUNT).IsColumnPrintable = Visible
      .Column(GRID_COLUMN_VOUCHERS_COUNT).Width = _width

      .Column(GRID_COLUMN_DELIVERED_VOUCHERS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7494)
      .Column(GRID_COLUMN_DELIVERED_VOUCHERS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7492)
      .Column(GRID_COLUMN_DELIVERED_VOUCHERS).IsColumnPrintable = Visible
      .Column(GRID_COLUMN_DELIVERED_VOUCHERS).Width = _width

      If Visible Then
        _width = 3000
      End If

      .Column(GRID_COLUMN_AMOUNT_DELIVERED_VOUCHERS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7494)
      .Column(GRID_COLUMN_AMOUNT_DELIVERED_VOUCHERS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7642)
      .Column(GRID_COLUMN_AMOUNT_DELIVERED_VOUCHERS).IsColumnPrintable = Visible
      .Column(GRID_COLUMN_AMOUNT_DELIVERED_VOUCHERS).Width = 0
    End With

  End Sub

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub SetDefaultValues()

    Dim _current_date As DateTime = WSI.Common.Misc.TodayOpening()

    Me.dtp_from.Value = _current_date
    Me.dtp_from.Checked = True

    Me.dtp_to.Value = _current_date.AddDays(1)
    Me.dtp_to.Checked = False

    Me.opt_all_cashiers.Checked = True
    Me.cmb_cashier.Enabled = False

    Me.opt_all_users.Checked = True
    Me.cmb_user.Enabled = False
    Me.chk_show_all.Checked = False
    Me.chk_show_all.Enabled = False

    Me.chk_session_open.Checked = False
    Me.chk_session_closed.Checked = False
    Me.chk_session_pending.Checked = False

    Me.chk_not_show_system_sessions.Enabled = True  'EOR 21-AUG-2017
    Me.chk_not_show_system_sessions.Checked = m_is_tito_mode

    Me.opt_opening_session.Checked = True

  End Sub ' SetDefaultValues

  ' PURPOSE: Build the variable part of the WHERE clause for the main SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Function GetSqlWhere() As String

    Dim _str_where As String = ""
    Dim _status As List(Of String)
    Dim _date_from As String
    Dim _date_to As String

    m_sessions_from_movements = ""

    ' Filter Dates by OpeningDate
    If opt_opening_session.Checked Then
      _date_from = ""
      _date_to = ""

      If Me.dtp_from.Checked Then
        _date_from = GUI_FormatDateDB(dtp_from.Value)
      End If
      If Me.dtp_to.Checked Then
        _date_to = GUI_FormatDateDB(dtp_to.Value)
      End If

      _str_where = _str_where & WSI.Common.Misc.DateSQLFilter("CS_OPENING_DATE", _date_from, _date_to, False)

      ' Filter Dates by MovementsDate
      ' Variable m_sessions_from_movements is informed in methodGetSessionsFromMovements() when returns true.
    ElseIf opt_movement_session.Checked Then
      m_sessions_from_movements = GetSessionsFromMovements()

      If Not String.IsNullOrEmpty(m_sessions_from_movements) Then
        _str_where = _str_where & " AND CS_SESSION_ID IN (" & m_sessions_from_movements & ") "
      End If

    ElseIf opt_movement_gaming_day.Checked Then
      m_sessions_from_movements = GetSessionsFromGamingDays()

      If Not String.IsNullOrEmpty(m_sessions_from_movements) Then
        _str_where = _str_where & " AND CS_SESSION_ID IN (" & m_sessions_from_movements & ") "
      End If

    End If

    ' Filter Cashier
    If Me.opt_one_cashier.Checked = True Then
      _str_where = _str_where & " AND (CS_CASHIER_ID = " & Me.cmb_cashier.Value & ") "
    End If

    ' Filter User
    If Me.opt_one_user.Checked = True Then
      _str_where = _str_where & " AND (CS_USER_ID = " & Me.cmb_user.Value & ") "
    End If

    'Filter State
    _status = New List(Of String)

    If Me.chk_session_open.Checked = True Then
      _status.Add(WSI.Common.CASHIER_SESSION_STATUS.OPEN)
      _status.Add(WSI.Common.CASHIER_SESSION_STATUS.OPEN_PENDING)
    End If

    If Me.chk_session_closed.Checked = True Then
      _status.Add(WSI.Common.CASHIER_SESSION_STATUS.CLOSED)
    End If

    If Me.chk_session_pending.Checked = True Then
      _status.Add(WSI.Common.CASHIER_SESSION_STATUS.PENDING_CLOSING)
    End If

    If _status.Count > 0 Then
      _str_where = _str_where & " AND (CS_STATUS IN ( " & String.Join(",", _status.ToArray()) & " )) "
    End If

    'DHA 09-Jan-2014 Add control to hidde system sessions
    If Me.m_not_show_system_sessions Then
      _str_where &= " AND GU_USER_TYPE NOT IN ( " & WSI.Common.Misc.SystemUsersListListToString() & ")"
    End If

    ' DHA 09-FEB-2016: Add ISO Code filter to Floor Dual Currency
    If WSI.Common.Misc.IsFloorDualCurrencyEnabled() Then
      If cmb_currencies.TextValue <> CurrencyExchange.GetNationalCurrency() Then
        _str_where &= " AND TE_ISO_CODE = '" & cmb_currencies.TextValue & "' "
      Else
        _str_where &= " AND (TE_ISO_CODE = '" & cmb_currencies.TextValue & "' OR TE_ISO_CODE IS NULL) "
      End If
    End If

    ' Final processing
    If Len(_str_where) > 0 Then
      ' Discard the leading ' AND ' and place 'Where' instead
      _str_where = Strings.Right(_str_where, Len(_str_where) - 5)
      _str_where = " WHERE " & _str_where
    End If

    Return _str_where
  End Function ' GetSqlWhere

  ' PURPOSE: Checks whether the specified row contains data or not
  '
  '  PARAMS:
  '     - INPUT:
  '           - IdxRow: row to check
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - TRUE: selected row contains data
  '     - FALSE: selected row does not exist or contains no data

  Private Function IsValidDataRow(ByVal IdxRow As Integer) As Boolean

    ' Skip Totalizator row!!!
    Return (Me.Grid.Cell(IdxRow, GRID_COLUMN_SESSION_ID).Value <> "")

  End Function ' IsValidDataRow

  ' PURPOSE: Call to child window to show details for the selected row
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub GUI_ShowSelectedItemSummary()

    Dim _idx_row As Integer
    Dim _session_id As Integer

    Dim _frm As frm_cashier_session_detail

    ' Check that it can show the information
    If Not GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Enabled Then
      Return
    End If

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    ' Protect totalizator row
    If Not IsValidDataRow(_idx_row) Then
      Return
    End If

    If Me.Grid.Cell(_idx_row, GRID_COLUMN_SESSION_ID).Value = "" Then
      _session_id = 0
    Else
      _session_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_SESSION_ID).Value
    End If

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_cashier_session_detail

    _frm.m_cage_session_id = m_cage_session_id

    If Not m_gaming_day AndAlso m_sessions_from_movements <> "" Then
      _frm.ShowEditItem(_session_id, m_from_date, m_to_date, Me.cmb_currencies.TextValue)
    Else
      _frm.ShowEditItem(_session_id, Nothing, Nothing, Me.cmb_currencies.TextValue)
    End If

  End Sub ' GUI_ShowSelectedItemSummary

  ' PURPOSE: Call to child window to show details for sessions in current period 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub GUI_ShowSummarySessions()

    Dim _frm As frm_cashier_session_detail
    Dim _str_sql As StringBuilder
    Dim _iso_code As String

    _str_sql = New StringBuilder()
    _iso_code = String.Empty

    _str_sql.AppendLine("SELECT   CS_SESSION_ID ")
    _str_sql.AppendLine("  FROM   CASHIER_SESSIONS ")
    If m_not_show_system_sessions Then
      _str_sql.AppendLine("    INNER JOIN   GUI_USERS GU ON CS_USER_ID = GU_USER_ID ")
    End If
    ' DHA 09-FEB-2016: Add Inner Join to filter to Floor Dual Currency
    If WSI.Common.Misc.IsFloorDualCurrencyEnabled() And Not String.IsNullOrEmpty(cmb_currencies.TextValue) Then
      _str_sql.AppendLine("    INNER JOIN CASHIER_TERMINALS ON CT_CASHIER_ID = CS_CASHIER_ID ")
      _str_sql.AppendLine("    LEFT  JOIN TERMINALS ON CT_TERMINAL_ID = TE_TERMINAL_ID ")
    End If
    _str_sql.AppendLine(m_str_where)

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    If m_opt_gaming_day Then
      _frm = New frm_cashier_session_detail(frm_cashier_session_detail.ENUM_CASH_INFORMATION_TYPE_MODE.TYPE_FILTER_GAMING_DAY)
    Else
      _frm = New frm_cashier_session_detail
    End If


    _frm.m_cage_session_id = m_cage_session_id

    If WSI.Common.Misc.IsFloorDualCurrencyEnabled() Then
      _iso_code = cmb_currencies.TextValue
    End If

    If m_sessions_from_movements <> "" Then
      _frm.ShowEditItem(_str_sql.ToString(), m_from_date, m_to_date, False, _iso_code)
    Else
      _frm.ShowEditItem(_str_sql.ToString(), m_from_date, m_to_date, True, _iso_code)
    End If

  End Sub ' GUI_ShowSummarySessions

  ' PURPOSE: Call to the Cage sessions to select one
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_SelectCageSession()
    Dim _frm_cage_sessions As frm_cage_sessions

    _frm_cage_sessions = New frm_cage_sessions()
    Call _frm_cage_sessions.ShowForEdit(Me.m_cage_session_id)
  End Sub ' GUI_SelectCageSession

  ' PURPOSE: Call to child window to show details for the selected row
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub GUI_ShowSelectedItemMBMovements()

    Dim _idx_row As Integer
    Dim _session_id As Integer
    Dim _session_name As String
    Dim _frm As frm_mb_movements

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    ' Protect totalizator row
    If Not IsValidDataRow(_idx_row) Then
      Return
    End If

    If Me.Grid.Cell(_idx_row, GRID_COLUMN_SESSION_ID).Value = "" Then
      _session_id = 0
      _session_name = ""
    Else
      _session_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_SESSION_ID).Value
      _session_name = Me.Grid.Cell(_idx_row, GRID_COLUMN_SESSION_NAME).Value
    End If

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_mb_movements

    If m_sessions_from_movements <> "" Then
      _frm.ShowForEdit(Me.MdiParent, _session_id, _session_name, m_from_date, m_to_date)
    Else
      _frm.ShowForEdit(Me.MdiParent, _session_id, _session_name)
    End If

  End Sub ' GUI_ShowSelectedItemMBMovements

  ' PURPOSE: Get Iso Codes of Currency Exchange columns
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - List of string with the Iso Codes

  Private Function LoadIsoCodes(ByRef IsoCodes As List(Of String), ByRef IsoDescriptions As List(Of String), ByRef IsoTypes As List(Of CurrencyIsoType)) As Boolean
    Dim _str_sql As StringBuilder
    Dim _str_where As String
    Dim _date_from As String
    Dim _date_to As String
    Dim _national_currency As String
    Dim _ct As CurrencyIsoType

    _str_sql = New StringBuilder()
    IsoCodes = New List(Of String)
    IsoDescriptions = New List(Of String)
    IsoTypes = New List(Of CurrencyIsoType)
    _date_from = ""
    _date_to = ""
    _national_currency = CurrencyExchange.GetNationalCurrency()

    ' DHA 09-FEB-2016
    If WSI.Common.Misc.IsFloorDualCurrencyEnabled() And Not String.IsNullOrEmpty(cmb_currencies.TextValue) Then
      _national_currency = cmb_currencies.TextValue
    End If

    _str_where = m_str_where

    Try
      Using _db_trx As DB_TRX = New DB_TRX()

        _str_sql.AppendLine(" SELECT DISTINCT   CSC_ISO_CODE ")
        _str_sql.AppendLine("                 , CE_DESCRIPTION ")

        _str_sql.AppendLine("                 , CE_TYPE ")

        _str_sql.AppendLine("            FROM   CASHIER_SESSIONS_BY_CURRENCY ")
        _str_sql.AppendLine("      INNER JOIN   CASHIER_SESSIONS CS ON CS_SESSION_ID = CSC_SESSION_ID ")
        _str_sql.AppendLine("      INNER JOIN   CURRENCY_EXCHANGE CE ON CE_CURRENCY_ISO_CODE = CSC_ISO_CODE AND CE_TYPE = CSC_TYPE ")
        _str_sql.AppendLine("             AND   CSC_ISO_CODE <> '" & _national_currency & "' ")
        'LEM? TODO RCI 02-JAN-2014: When Boveda is ready to change the CASINOCHIP, this condition will not be necessary anymore.
        _str_sql.AppendLine("             AND   CSC_ISO_CODE <> '" & Cage.CHIPS_ISO_CODE & "' ")
        If m_not_show_system_sessions Then
          _str_sql.AppendLine("    INNER JOIN   GUI_USERS GU ON CS_USER_ID = GU_USER_ID ")
        End If

        ' DHA 09-FEB-2016: Add Inner Join filter to Floor Dual Currency
        If WSI.Common.Misc.IsFloorDualCurrencyEnabled() And Not String.IsNullOrEmpty(cmb_currencies.TextValue) Then
          _str_sql.AppendLine("    INNER JOIN CASHIER_TERMINALS ON CT_CASHIER_ID = CS_CASHIER_ID ")
          _str_sql.AppendLine("    LEFT  JOIN TERMINALS ON CT_TERMINAL_ID = TE_TERMINAL_ID ")
        End If

        If opt_movement_session.Checked Then
          _str_sql.Length = 0
          _str_sql.AppendLine(" SELECT DISTINCT   CM_CURRENCY_ISO_CODE ")
          _str_sql.AppendLine("                 , CE_DESCRIPTION ")

          _str_sql.AppendLine("                 , CE_TYPE ")

          _str_sql.AppendLine("            FROM   CASHIER_MOVEMENTS CM ")
          _str_sql.AppendLine("      INNER JOIN   CASHIER_SESSIONS CS ON CS_SESSION_ID = CM_SESSION_ID ")
          _str_sql.AppendLine("      INNER JOIN   CURRENCY_EXCHANGE CE ON CE_CURRENCY_ISO_CODE = CM_CURRENCY_ISO_CODE AND CE_TYPE = " & CurrencyExchangeType.CURRENCY & " ")
          _str_sql.AppendLine("             AND   CM_CURRENCY_ISO_CODE <> '" & _national_currency & "' ")
          'LEM? TODO RCI 02-JAN-2014: When Boveda is ready to change the CASINOCHIP, this condition will not be necessary anymore.
          _str_sql.AppendLine("             AND   CM_CURRENCY_ISO_CODE <> '" & Cage.CHIPS_ISO_CODE & "' ")

          If m_not_show_system_sessions Then
            _str_sql.AppendLine("    INNER JOIN   GUI_USERS GU ON CS_USER_ID = GU_USER_ID ")
          End If

          ' DHA 09-FEB-2016: Add Inner Join filter to Floor Dual Currency
          If WSI.Common.Misc.IsFloorDualCurrencyEnabled() And Not String.IsNullOrEmpty(cmb_currencies.TextValue) Then
            _str_sql.AppendLine("    INNER JOIN CASHIER_TERMINALS ON CT_CASHIER_ID = CS_CASHIER_ID ")
            _str_sql.AppendLine("    LEFT  JOIN TERMINALS ON CT_TERMINAL_ID = TE_TERMINAL_ID ")
          End If

          If dtp_from.Checked Then
            _date_from = GUI_FormatDateDB(dtp_from.Value)
          End If
          If dtp_to.Checked Then
            _date_to = GUI_FormatDateDB(dtp_to.Value)
          End If

          If Len(_str_where) > 0 Then
            _str_where &= " AND "
          Else
            _str_where = " WHERE "
          End If

          _str_where &= "CM_TYPE IN (0, 1, 2, 3, 78) "
          _str_sql.AppendLine(WSI.Common.Misc.DateSQLFilter("CM_DATE", _date_from, _date_to, False))
        End If

        _str_sql.AppendLine(_str_where)

        Using _cmd As SqlCommand = New SqlCommand(_str_sql.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          Using _reader As SqlDataReader = _cmd.ExecuteReader()

            While _reader.Read()
              If Not _reader.IsDBNull(0) Then
                IsoCodes.Add(_reader.GetString(0))
                IsoDescriptions.Add(_reader.GetString(1))

                _ct = New CurrencyIsoType()
                _ct.IsoCode = _reader.GetString(0)
                _ct.Type = _reader.GetInt32(2)
                IsoTypes.Add(_ct)

              End If
            End While
          End Using
        End Using
      End Using

    Catch ex As Exception
      Return False
    End Try

    Return True

  End Function 'LoadCurrenciesExchange

  Public Function GetAllowedISOCode() As DataTable
    Dim _sql_chip As StringBuilder
    Dim _table_chip As DataTable


    m_national_currency = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")

    _sql_chip = New StringBuilder


    _sql_chip.AppendLine("    SELECT  DISTINCT ISNULL(CE_CURRENCY_ORDER , CASE WHEN CHS_ISO_CODE = '" & m_national_currency & "' THEN 0 ELSE 99999999 END) AS CE_CURRENCY_ORDER ")
    _sql_chip.AppendLine("          , CHS_ISO_CODE  ")
    _sql_chip.AppendLine("          , ISNULL(CHS_CHIP_TYPE, 0) AS TYPE ")
    _sql_chip.AppendLine("          , '' ")
    _sql_chip.AppendLine("          , '' ")
    _sql_chip.AppendLine("      FROM  CHIPS_SETS  ")
    _sql_chip.AppendLine(" LEFT JOIN  CURRENCY_EXCHANGE ON CE_CURRENCY_ISO_CODE = CHS_ISO_CODE AND CE_TYPE = 0 ")
    _sql_chip.AppendLine("     WHERE  CHS_ALLOWED = 1 ")
    _sql_chip.AppendLine("     ORDER  BY 1, 2, 3 ")

    Try
      _table_chip = GUI_GetTableUsingSQL(_sql_chip.ToString, Integer.MaxValue)


    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "frm_cashier_sessions", _
                            "GetAllowedISOCode", _
                            ex.Message)

      Return New DataTable
    End Try

    Return _table_chip

  End Function ' GetAllowedISOCode

  ' EOR 05-OCT-2016
  ' PURPOSE: Shows or hide service charge columns 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  Private Sub ShowHideServiceChargeColumns()
    With Me.Grid
      If opt_company_both.Checked Then
        .Column(GRID_COLUMN_SERVICE_CHARGE).Width = GRID_WIDTH_NAME
        .Column(GRID_COLUMN_SERVICE_CHARGE).IsColumnPrintable = True

      End If

      If opt_company_a.Checked And m_service_charge_company = "A" Then
        .Column(GRID_COLUMN_SERVICE_CHARGE).Width = GRID_WIDTH_NAME
        .Column(GRID_COLUMN_SERVICE_CHARGE).IsColumnPrintable = True

      End If

      If opt_company_b.Checked And m_service_charge_company = "B" Then
        .Column(GRID_COLUMN_SERVICE_CHARGE).Width = GRID_WIDTH_NAME
        .Column(GRID_COLUMN_SERVICE_CHARGE).IsColumnPrintable = True

      End If

      If opt_company_a.Checked And m_service_charge_company <> "A" And total_service_charge_a <> 0 Then
        .Column(GRID_COLUMN_SERVICE_CHARGE).Width = GRID_WIDTH_NAME
        .Column(GRID_COLUMN_SERVICE_CHARGE).IsColumnPrintable = True

      End If

      If opt_company_b.Checked And m_service_charge_company <> "B" And total_service_charge_b <> 0 Then
        .Column(GRID_COLUMN_SERVICE_CHARGE).Width = GRID_WIDTH_NAME
        .Column(GRID_COLUMN_SERVICE_CHARGE).IsColumnPrintable = True

      End If

    End With
  End Sub ' ShowHideServiceChargeColumns

  ' PURPOSE: Shows or hide currencies columns that don't have results
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  Private Sub ShowHideCurrencyColumns()
    Dim _national_currency As New CurrencyExchange()
    Dim _currencies As New List(Of CurrencyExchange)
    Dim _is_card_enabled As Boolean = False
    Dim _is_check_enabled As Boolean = False
    Dim _are_currencies_enabled As Boolean = False

    CurrencyExchange.GetAllowedCurrencies(True, _national_currency, _currencies)

    For Each _currency As CurrencyExchange In _currencies

      Select Case _currency.Type
        Case CurrencyExchangeType.CARD
          _is_card_enabled = True

        Case CurrencyExchangeType.CHECK
          _is_check_enabled = True

        Case CurrencyExchangeType.CURRENCY
          If Not _are_currencies_enabled Then
            _are_currencies_enabled = True
          End If

      End Select
    Next

    With Me.Grid
      ' Makes invisible bank card columns if they don't have any results
      If Not _is_card_enabled AndAlso total_bank_card = 0 Then
        .Column(GRID_COLUMN_TOTAL_BANK_CARD).Width = 0
        .Column(GRID_COLUMN_COMMISSIONS_BANK_CARD).Width = 0
      Else
        .Column(GRID_COLUMN_TOTAL_BANK_CARD).Width = GRID_WIDTH_AMOUNT_LONG + 400
        .Column(GRID_COLUMN_COMMISSIONS_BANK_CARD).Width = GRID_WIDTH_AMOUNT_LONG + 700
      End If

      ' Makes invisible currency exchange recharges columns if they don't have any results
      If Not _is_check_enabled AndAlso total_currency_exchange = 0 Then
        .Column(GRID_COLUMN_COMMISSIONS_CUR_EXCH).Width = 0
        .Column(GRID_COLUMN_TOTAL_CURRENCY_EXCHANGE).Width = 0
      Else
        .Column(GRID_COLUMN_COMMISSIONS_CUR_EXCH).Width = GRID_WIDTH_AMOUNT_LONG + 700
        .Column(GRID_COLUMN_TOTAL_CURRENCY_EXCHANGE).Width = GRID_WIDTH_AMOUNT_LONG + 300
      End If

      ' Makes invisible check columns if they don't have any results
      If Not _are_currencies_enabled AndAlso total_check_recharges = 0 Then
        .Column(GRID_COLUMN_TOTAL_CHECK_RECHARGES).Width = 0
        .Column(GRID_COLUMN_COMMISSIONS_CHECK).Width = 0
        .Column(GRID_COLUMN_CHECK_BALANCE).Width = 0
        .Column(GRID_COLUMN_CHECK_COLLECTED).Width = 0
        .Column(GRID_COLUMN_CHECK_DIFFERENCE).Width = 0
      Else
        .Column(GRID_COLUMN_TOTAL_CHECK_RECHARGES).Width = GRID_WIDTH_AMOUNT_LONG + 400
        .Column(GRID_COLUMN_COMMISSIONS_CHECK).Width = GRID_WIDTH_AMOUNT_LONG + 400
        .Column(GRID_COLUMN_CHECK_BALANCE).Width = GRID_WIDTH_AMOUNT_LONG + 400
        .Column(GRID_COLUMN_CHECK_COLLECTED).Width = GRID_WIDTH_AMOUNT_LONG + 400
        .Column(GRID_COLUMN_CHECK_DIFFERENCE).Width = GRID_WIDTH_AMOUNT_LONG + 400
      End If

      If (total_amount_money_recharge = 0 And total_amount_bank_card_recharge = 0) Then
        .Column(GRID_COLUMN_CASH_RECHARGE).Width = 0
        .Column(GRID_COLUMN_CREDIT_CARD_RECHARGE).Width = 0
      End If

      If (total_vouchers_count = 0 And total_delivered_vouchers = 0 And total_amount_delivered_vouchers = 0) Then
        .Column(GRID_COLUMN_VOUCHERS_COUNT).Width = 0
        .Column(GRID_COLUMN_DELIVERED_VOUCHERS).Width = 0
        .Column(GRID_COLUMN_AMOUNT_DELIVERED_VOUCHERS).Width = 0
      End If
    End With

  End Sub ' ShowHideCurrencyColumns

  ''' <summary>
  ''' Function to Add Exchange currencies on the grid
  ''' </summary>
  ''' <param name="_column_offset">Number of column</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ExchangeCurrencies(ByVal _column_offset As Integer) As Integer

    With Me.Grid
      If Not m_currency_exchange_iso_codes Is Nothing Then
        _column_offset = 0

        ' Add currency exchange columns
        For _index As Integer = 0 To m_currency_exchange_iso_codes.Count() - 1
          ' Expected column
          Call GUI_AddColumn(Me.Grid, GRID_COLUMNS + _column_offset, m_currency_exchange_iso_description(_index).ToString(), GRID_WIDTH_AMOUNT_LONG, , IIf(opt_movement_session.Checked, _
                                                                                                                                                      GLB_NLS_GUI_PLAYER_TRACKING.GetString(541), _
                                                                                                                                                       GLB_NLS_GUI_PLAYER_TRACKING.GetString(645)))
          _column_offset += 1

          ' Collected column
          Call GUI_AddColumn(Me.Grid, GRID_COLUMNS + _column_offset, m_currency_exchange_iso_description(_index).ToString(), GRID_WIDTH_AMOUNT_LONG, , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2335))

          If Not opt_company_both.Checked Then
            .Column(GRID_COLUMNS + _column_offset).Width = 0
          End If
          _column_offset += 1

          ' Difference column          
          Call GUI_AddColumn(Me.Grid, GRID_COLUMNS + _column_offset, m_currency_exchange_iso_description(_index).ToString(), GRID_WIDTH_AMOUNT_LONG, , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2336))

          If Not opt_company_both.Checked Then
            .Column(GRID_COLUMNS + _column_offset).Width = 0
          End If
          _column_offset += 1
        Next
      End If
    End With

    Return _column_offset

  End Function ' ExchangeCurrencies

  ''' <summary>
  ''' Function to Add Exchange Chips Values on the grid
  ''' </summary>
  ''' <param name="_column_offset">Number of the grid column</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ExchangeChips(ByVal _column_offset As Integer) As Integer

    Dim _description As String
    Dim _iso_code As String

    If Not m_dt_chips Is Nothing Then
      With Me.Grid
        'Add chips by currency
        For Each _dr As DataRow In m_dt_chips.Rows

          _iso_code = " (" & _dr("CHS_ISO_CODE").ToString() & ") "

          If _iso_code = " (X02) " Then
            _iso_code = String.Empty
          End If

          _description = GetChipsGroupDescription(_dr("TYPE").ToString()) & _iso_code

          ' Expected column
          Call GUI_AddColumn(Me.Grid, GRID_COLUMNS + _column_offset, _description, GRID_WIDTH_AMOUNT_LONG, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, IIf(opt_movement_session.Checked, _
                                                                                                                                                          GLB_NLS_GUI_PLAYER_TRACKING.GetString(541), _
                                                                                                                                                          GLB_NLS_GUI_PLAYER_TRACKING.GetString(645)))
          _column_offset += 1

          ' Collected column
          Call GUI_AddColumn(Me.Grid, GRID_COLUMNS + _column_offset, _description, GRID_WIDTH_AMOUNT_LONG, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2335))

          If Not opt_company_both.Checked Then
            .Column(GRID_COLUMNS + _column_offset).Width = 0
          End If
          _column_offset += 1

          ' Difference column          
          Call GUI_AddColumn(Me.Grid, GRID_COLUMNS + _column_offset, _description, GRID_WIDTH_AMOUNT_LONG, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2336))

          If Not opt_company_both.Checked Then
            .Column(GRID_COLUMNS + _column_offset).Width = 0
          End If
          _column_offset += 1
        Next
      End With

    End If

    Return _column_offset

  End Function ' ExchangeChips

  ''' <summary>
  ''' Function to get the chips group description
  ''' </summary>
  ''' <param name="TypeFromQuery"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetChipsGroupDescription(ByVal TypeFromQuery As String) As String
    Dim _group_description As String
    _group_description = String.Empty

    Select Case TypeFromQuery
      Case "1001"
        _group_description = GeneralParam.GetString("FeatureChips", "ValueType.Name")
      Case "1002"
        _group_description = GeneralParam.GetString("FeatureChips", "NonRedeemableType.Name")
      Case "1003"
        _group_description = GeneralParam.GetString("FeatureChips", "ColorType.Name")
    End Select

    Return _group_description
  End Function ' GetChipsGroupDescription

  ''' <summary>
  ''' Function to Insert the chips Balance on the grid
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub ChipsBalance()
    Dim _column_width As Integer

    With Me.Grid

      ' Chips columns depends on gaming tables enabled
      If Not WSI.Common.GamingTableBusinessLogic.IsGamingTablesEnabled Then
        _column_width = 1
      Else
        _column_width = GRID_WIDTH_AMOUNT_LONG + 400
      End If

      ' Chips Balance
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_CHIPS_BALANCE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2941), _column_width, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(645))

      ' Chips Collected
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_CHIPS_COLLECTED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2941), _column_width, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2335))

      ' Chips Difference
      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_CHIPS_DIFFERENCE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2941), _column_width, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2610))

    End With
  End Sub ' ChipsBalance

  ''' <summary>
  ''' Function to insert the split the balance for the both company
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SplitBothCompany()
    With Me.Grid
      If Not m_both_company_selected Then
        .Column(GRID_COLUMN_EXPECTED).Header(1).Text = ""
        .Column(GRID_COLUMN_EXPECTED).Width = 0
        .Column(GRID_COLUMN_EXPECTED).IsColumnPrintable = False

        .Column(GRID_COLUMN_DIFFERENCE_TOTAL).Header(1).Text = ""
        .Column(GRID_COLUMN_DIFFERENCE_TOTAL).Width = 0
        .Column(GRID_COLUMN_DIFFERENCE_TOTAL).IsColumnPrintable = False

        .Column(GRID_COLUMN_COLLECTED).Header(1).Text = ""
        .Column(GRID_COLUMN_COLLECTED).Width = 0
        .Column(GRID_COLUMN_COLLECTED).IsColumnPrintable = False

        .Column(GRID_COLUMN_DIFFERENCE_CASHIER).Header(1).Text = ""
        .Column(GRID_COLUMN_DIFFERENCE_CASHIER).Width = 0
        .Column(GRID_COLUMN_DIFFERENCE_CASHIER).IsColumnPrintable = False

        .Column(GRID_COLUMN_DIFFERENCE_MB).Header(1).Text = ""
        .Column(GRID_COLUMN_DIFFERENCE_MB).Width = 0
        .Column(GRID_COLUMN_DIFFERENCE_MB).IsColumnPrintable = False

        If Not m_card_differentiate_type Then
          .Column(GRID_COLUMN_BANK_CARD_CREDIT_BALANCE).Header(1).Text = ""
          .Column(GRID_COLUMN_BANK_CARD_CREDIT_BALANCE).IsColumnPrintable = False
          .Column(GRID_COLUMN_BANK_CARD_CREDIT_BALANCE).Width = 0

          .Column(GRID_COLUMN_BANK_CARD_DEBIT_BALANCE).Header(1).Text = ""
          .Column(GRID_COLUMN_BANK_CARD_DEBIT_BALANCE).IsColumnPrintable = False
          .Column(GRID_COLUMN_BANK_CARD_DEBIT_BALANCE).Width = 0
        End If

        .Column(GRID_COLUMN_BANK_CARD_COLLECTED).Header(1).Text = ""
        .Column(GRID_COLUMN_BANK_CARD_COLLECTED).IsColumnPrintable = False
        .Column(GRID_COLUMN_BANK_CARD_COLLECTED).Width = 0

        .Column(GRID_COLUMN_BANK_CARD_DIFFERENCE).Header(1).Text = ""
        .Column(GRID_COLUMN_BANK_CARD_DIFFERENCE).IsColumnPrintable = False
        .Column(GRID_COLUMN_BANK_CARD_DIFFERENCE).Width = 0

        .Column(GRID_COLUMN_CHECK_COLLECTED).Header(1).Text = ""
        .Column(GRID_COLUMN_CHECK_COLLECTED).IsColumnPrintable = False
        .Column(GRID_COLUMN_CHECK_COLLECTED).Width = 0

        .Column(GRID_COLUMN_CHECK_DIFFERENCE).Header(1).Text = ""
        .Column(GRID_COLUMN_CHECK_DIFFERENCE).IsColumnPrintable = False
        .Column(GRID_COLUMN_CHECK_DIFFERENCE).Width = 0

        .Column(GRID_COLUMN_CHIPS_COLLECTED).Header(1).Text = ""
        .Column(GRID_COLUMN_CHIPS_COLLECTED).IsColumnPrintable = False
        .Column(GRID_COLUMN_CHIPS_COLLECTED).Width = 0

        .Column(GRID_COLUMN_CHIPS_DIFFERENCE).Header(1).Text = ""
        .Column(GRID_COLUMN_CHIPS_DIFFERENCE).IsColumnPrintable = False
        .Column(GRID_COLUMN_CHIPS_DIFFERENCE).Width = 0

        .Column(GRID_COLUMN_TOTAL_CHIP_SALE_REGISTER).Header(1).Text = ""
        .Column(GRID_COLUMN_TOTAL_CHIP_SALE_REGISTER).Width = 0
        .Column(GRID_COLUMN_TOTAL_CHIP_SALE_REGISTER).IsColumnPrintable = False
      End If
    End With
  End Sub ' SplitBothCompany

  ''' <summary>
  ''' Function to insert to split the balance for the B company
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SplitBCompany()
    With Me.Grid
      If opt_company_b.Checked Then

        .Column(GRID_COLUMN_PENDING_MB).Header(1).Text = ""
        .Column(GRID_COLUMN_PENDING_MB).Width = 0
        .Column(GRID_COLUMN_PENDING_MB).IsColumnPrintable = False

        .Column(GRID_COLUMN_SHORTFALL_EXCESS_MB).Header(1).Text = ""
        .Column(GRID_COLUMN_SHORTFALL_EXCESS_MB).Width = 0
        .Column(GRID_COLUMN_SHORTFALL_EXCESS_MB).IsColumnPrintable = False

        .Column(GRID_COLUMN_SESSION_RESULT).Header(1).Text = ""
        .Column(GRID_COLUMN_SESSION_RESULT).Width = 0
        .Column(GRID_COLUMN_SESSION_RESULT).IsColumnPrintable = False

        .Column(GRID_COLUMN_PROMO_NO_REDEEMABLE).Header(1).Text = ""
        .Column(GRID_COLUMN_PROMO_NO_REDEEMABLE).Width = 0
        .Column(GRID_COLUMN_PROMO_NO_REDEEMABLE).IsColumnPrintable = False

        .Column(GRID_COLUMN_PROMO_REDEEMABLE).Header(1).Text = ""
        .Column(GRID_COLUMN_PROMO_REDEEMABLE).IsColumnPrintable = False
        .Column(GRID_COLUMN_PROMO_REDEEMABLE).Width = 0

        .Column(GRID_COLUMN_NO_REDEEMABLE_TO_REDEEMABLE).Header(1).Text = ""
        .Column(GRID_COLUMN_NO_REDEEMABLE_TO_REDEEMABLE).IsColumnPrintable = False
        .Column(GRID_COLUMN_NO_REDEEMABLE_TO_REDEEMABLE).Width = 0

        .Column(GRID_COLUMN_CS_LIMIT_SALE).Header(1).Text = ""
        .Column(GRID_COLUMN_CS_LIMIT_SALE).IsColumnPrintable = False
        .Column(GRID_COLUMN_CS_LIMIT_SALE).Width = 0

        .Column(GRID_COLUMN_MB_LIMIT_SALE).Header(1).Text = ""
        .Column(GRID_COLUMN_MB_LIMIT_SALE).IsColumnPrintable = False
        .Column(GRID_COLUMN_MB_LIMIT_SALE).Width = 0

        .Column(GRID_COLUMN_TAX_RETURNING).Header(1).Text = ""
        .Column(GRID_COLUMN_TAX_RETURNING).IsColumnPrintable = False
        .Column(GRID_COLUMN_TAX_RETURNING).Width = 0

        .Column(GRID_COLUMN_DECIMAL_ROUNDING).Header(1).Text = ""
        .Column(GRID_COLUMN_DECIMAL_ROUNDING).IsColumnPrintable = False
        .Column(GRID_COLUMN_DECIMAL_ROUNDING).Width = 0

        .Column(GRID_COLUMN_TAXES).Header(1).Text = ""
        .Column(GRID_COLUMN_TAXES).IsColumnPrintable = False
        .Column(GRID_COLUMN_TAXES).Width = 0

        .Column(GRID_COLUMN_CHECK_PAYMENT).Header(1).Text = ""
        .Column(GRID_COLUMN_CHECK_PAYMENT).IsColumnPrintable = False
        .Column(GRID_COLUMN_CHECK_PAYMENT).Width = 0

        .Column(GRID_COLUMN_INPUTS_A).Header(1).Text = ""
        .Column(GRID_COLUMN_INPUTS_A).IsColumnPrintable = False
        .Column(GRID_COLUMN_INPUTS_A).Width = 0

        '.Column(GRID_COLUMN_CARD_USAGE).Header(1).Text = ""
        '.Column(GRID_COLUMN_CARD_USAGE).IsColumnPrintable = False
        '.Column(GRID_COLUMN_CARD_USAGE).Width = 0

        .Column(GRID_COLUMN_OUTPUTS_A).Header(1).Text = ""
        .Column(GRID_COLUMN_OUTPUTS_A).IsColumnPrintable = False
        .Column(GRID_COLUMN_OUTPUTS_A).Width = 0

        .Column(GRID_COLUMN_REFUND_CARD_USAGE).Header(1).Text = ""
        .Column(GRID_COLUMN_REFUND_CARD_USAGE).IsColumnPrintable = False
        .Column(GRID_COLUMN_REFUND_CARD_USAGE).Width = 0

        .Column(GRID_COLUMN_TOTAL_PRIZES).Header(1).Text = ""
        .Column(GRID_COLUMN_TOTAL_PRIZES).IsColumnPrintable = False
        .Column(GRID_COLUMN_TOTAL_PRIZES).Width = 0

        .Column(GRID_COLUMN_DECIMAL_ROUNDING).Header(1).Text = ""
        .Column(GRID_COLUMN_DECIMAL_ROUNDING).IsColumnPrintable = False
        .Column(GRID_COLUMN_DECIMAL_ROUNDING).Width = 0

        .Column(GRID_COLUMN_PRIZES).Header(1).Text = ""
        .Column(GRID_COLUMN_PRIZES).IsColumnPrintable = False
        .Column(GRID_COLUMN_PRIZES).Width = 0

        .Column(GRID_COLUMN_PRIZES_TAXES).Header(1).Text = ""
        .Column(GRID_COLUMN_PRIZES_TAXES).IsColumnPrintable = False
        .Column(GRID_COLUMN_PRIZES_TAXES).Width = 0

        .Column(GRID_COLUMN_TAX_RETURNING).Header(1).Text = ""
        .Column(GRID_COLUMN_TAX_RETURNING).IsColumnPrintable = False
        .Column(GRID_COLUMN_TAX_RETURNING).Width = 0

        .Column(GRID_COLUMN_UNR_KIND1).Header(1).Text = ""
        .Column(GRID_COLUMN_UNR_KIND1).IsColumnPrintable = False
        .Column(GRID_COLUMN_UNR_KIND1).Width = 0

        .Column(GRID_COLUMN_UNR_KIND2).Header(1).Text = ""
        .Column(GRID_COLUMN_UNR_KIND2).IsColumnPrintable = False
        .Column(GRID_COLUMN_UNR_KIND2).Width = 0

        ' DHA 04-JUN-2014: Hide Redeemable as cashout when is company b selected
        .Column(GRID_COLUMN_REDEEMABLE_AS_CASHOUT).Header(1).Text = ""
        .Column(GRID_COLUMN_UNR_KIND2).IsColumnPrintable = False
        .Column(GRID_COLUMN_REDEEMABLE_AS_CASHOUT).Width = 0

        .Column(GRID_COLUMN_HANDPAYS).Header(1).Text = ""
        .Column(GRID_COLUMN_HANDPAYS).IsColumnPrintable = False
        .Column(GRID_COLUMN_HANDPAYS).Width = 0

        .Column(GRID_COLUMN_CANCELATION_HANDPAYS).Header(1).Text = ""
        .Column(GRID_COLUMN_CANCELATION_HANDPAYS).IsColumnPrintable = False
        .Column(GRID_COLUMN_CANCELATION_HANDPAYS).Width = 0

        .Column(GRID_COLUMN_TOTAL_HANDPAYS).Header(1).Text = ""
        .Column(GRID_COLUMN_TOTAL_HANDPAYS).IsColumnPrintable = False
        .Column(GRID_COLUMN_TOTAL_HANDPAYS).Width = 0

        .Column(GRID_COLUMN_CANCEL_PROMO_REDEEMABLE).Header(1).Text = ""
        .Column(GRID_COLUMN_CANCEL_PROMO_REDEEMABLE).IsColumnPrintable = False
        .Column(GRID_COLUMN_CANCEL_PROMO_REDEEMABLE).Width = 0

        .Column(GRID_COLUMN_CANCEL_PROMO_NO_REDEEMABLE).Header(1).Text = ""
        .Column(GRID_COLUMN_CANCEL_PROMO_NO_REDEEMABLE).IsColumnPrintable = False
        .Column(GRID_COLUMN_CANCEL_PROMO_NO_REDEEMABLE).Width = 0

        .Column(GRID_COLUMN_EXPIRATIONS_DEV).Header(1).Text = ""
        .Column(GRID_COLUMN_EXPIRATIONS_DEV).IsColumnPrintable = False
        .Column(GRID_COLUMN_EXPIRATIONS_DEV).Width = 0

        .Column(GRID_COLUMN_EXPIRATIONS_PRIZE).Header(1).Text = ""
        .Column(GRID_COLUMN_EXPIRATIONS_PRIZE).IsColumnPrintable = False
        .Column(GRID_COLUMN_EXPIRATIONS_PRIZE).Width = 0

        .Column(GRID_COLUMN_EXPIRATIONS_PROMO_NR).Header(1).Text = ""
        .Column(GRID_COLUMN_EXPIRATIONS_PROMO_NR).IsColumnPrintable = False
        .Column(GRID_COLUMN_EXPIRATIONS_PROMO_NR).Width = 0

        '27-AUG-2013 FBA  -  Hides Comissions column. No comissions in company B 
        If WSI.Common.Misc.IsCommissionToCompanyBEnabled = False Then
          .Column(GRID_COLUMN_COMMISSIONS_BANK_CARD).Header(1).Text = ""
          .Column(GRID_COLUMN_COMMISSIONS_BANK_CARD).IsColumnPrintable = False
          .Column(GRID_COLUMN_COMMISSIONS_BANK_CARD).Width = 0

          .Column(GRID_COLUMN_COMMISSIONS_CUR_EXCH).Header(1).Text = ""
          .Column(GRID_COLUMN_COMMISSIONS_CUR_EXCH).IsColumnPrintable = False
          .Column(GRID_COLUMN_COMMISSIONS_CUR_EXCH).Width = 0

          .Column(GRID_COLUMN_COMMISSIONS_CHECK).Header(1).Text = ""
          .Column(GRID_COLUMN_COMMISSIONS_CHECK).IsColumnPrintable = False
          .Column(GRID_COLUMN_COMMISSIONS_CHECK).Width = 0
        End If

        .Column(GRID_COLUMN_CASH_ADVANCE).Header(1).Text = ""
        .Column(GRID_COLUMN_CASH_ADVANCE).IsColumnPrintable = False
        .Column(GRID_COLUMN_CASH_ADVANCE).Width = 0
      End If
    End With
  End Sub ' SplitBCompany

#End Region ' Private Functions

#Region " Events "

  Private Sub opt_several_cashiers_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_one_cashier.Click
    Me.cmb_cashier.Enabled = True
    Me.chk_not_show_system_sessions.Enabled = False
  End Sub ' opt_several_cashiers_Click

  Private Sub opt_all_cashiers_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_all_cashiers.Click
    Me.cmb_cashier.Enabled = False

    If Me.opt_one_user.Checked Then
      Me.chk_not_show_system_sessions.Enabled = False
    Else
      Me.chk_not_show_system_sessions.Enabled = True
    End If

  End Sub ' opt_all_cashiers_Click

  Private Sub opt_several_users_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_one_user.Click
    Me.cmb_user.Enabled = True
    Me.chk_show_all.Enabled = True
    Me.chk_not_show_system_sessions.Enabled = False
  End Sub ' opt_several_users_Click

  Private Sub opt_all_users_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_all_users.Click
    Me.cmb_user.Enabled = False
    Me.chk_show_all.Enabled = False

    If Me.opt_one_cashier.Checked Then
      Me.chk_not_show_system_sessions.Enabled = False
    Else
      Me.chk_not_show_system_sessions.Enabled = True
    End If

  End Sub ' opt_all_users_Click

  ' XCD 16-Aug-2012 Fill combo users
  Private Sub chk_show_all_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_show_all.CheckedChanged
    If chk_show_all.Checked Then
      Call SetCombo(Me.cmb_user, "SELECT GU_USER_ID, GU_USERNAME FROM GUI_USERS ORDER BY GU_USERNAME")
    Else
      Call SetCombo(Me.cmb_user, "SELECT GU_USER_ID, GU_USERNAME FROM GUI_USERS WHERE GU_BLOCK_REASON = " & WSI.Common.GUI_USER_BLOCK_REASON.NONE & " ORDER BY GU_USERNAME")
    End If
  End Sub 'chk_show_all

#End Region ' Events

  ' --------------------------------------------------------------------------------------------------------------------------

  ' PURPOSE: Get the defined Query Type
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_QUERY_TYPE
  '
  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM
    'Return ENUM_QUERY_TYPE.QUERY_DATABASE
  End Function ' GUI_GetQueryType

  ' PURPOSE: Executes the query with a command
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '     - 
  Protected Overrides Sub GUI_ExecuteQueryCustom()
    Dim _db_trx As WSI.Common.DB_TRX
    Dim _sql_da As SqlDataAdapter
    Dim _sql_cmd As SqlCommand
    Dim _ds_query As DataSet
    Dim _sessions_results As DataTable
    Dim _row As DataRow
    Dim _dt_table_sessions As DataTable
    Dim _dv_table_sessions As DataView
    Dim _filter_movements As String

    m_not_show_system_sessions = Me.chk_not_show_system_sessions.Checked And Me.chk_not_show_system_sessions.Enabled
    m_str_where = GetSqlWhere()
    _filter_movements = String.Empty

    _db_trx = New WSI.Common.DB_TRX()
    _ds_query = Nothing

    _sql_cmd = New SqlCommand("SP_Cashier_Sessions_Report")

    _sql_cmd.CommandType = CommandType.StoredProcedure

    _sql_cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = IIf(dtp_from.Checked, m_from_date, dtp_from.GetMinDate())
    _sql_cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = IIf(dtp_to.Checked, m_to_date, WGDB.Now.Date.AddDays(1))

    ' RMS - Gaming Day
    If opt_movement_session.Checked Then
      _sql_cmd.Parameters.Add("@pReportMode", SqlDbType.Int).Value = 1
    ElseIf opt_opening_session.Checked Then
      _sql_cmd.Parameters.Add("@pReportMode", SqlDbType.Int).Value = 0
    ElseIf opt_movement_gaming_day.Checked Then
      _sql_cmd.Parameters.Add("@pReportMode", SqlDbType.Int).Value = 2
    End If
    '_sql_cmd.Parameters.Add("@pUseCashierMovements", SqlDbType.Bit).Value = IIf(opt_movement_session.Checked, 1, 0)

    _sql_cmd.Parameters.Add("@pShowSystemSessions", SqlDbType.Bit).Value = IIf(m_not_show_system_sessions, 0, 1)
    _sql_cmd.Parameters.Add("@pSystemUsersTypes", SqlDbType.VarChar).Value = IIf(m_not_show_system_sessions And opt_all_users.Checked, WSI.Common.Misc.SystemUsersListListToString(), String.Empty)
    _sql_cmd.Parameters.Add("@pSessionStatus", SqlDbType.VarChar).Value = GetSessionStatusFilter()
    _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = IIf(opt_all_users.Checked, DBNull.Value, cmb_user.Value)
    _sql_cmd.Parameters.Add("@pCashierId", SqlDbType.Int).Value = IIf(opt_all_cashiers.Checked, DBNull.Value, cmb_cashier.Value)
    _sql_cmd.Parameters.Add("@pCurrencyISOCode", SqlDbType.VarChar).Value = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")

    _sql_da = New SqlDataAdapter(_sql_cmd)
    _ds_query = New DataSet()

    ' Query sessions and movements
    _db_trx.Fill(_sql_da, _ds_query)

    ' DataSet will contains 3 tables, 0 - Sessions Movements , 1 - Sessions with cage info, 2 - Session Currencies information

    _dt_table_sessions = New DataTable()
    _dv_table_sessions = New DataView(_ds_query.Tables(RESULT_TABLE_SESSIONS))

    If WSI.Common.Misc.IsFloorDualCurrencyEnabled Then
      _dv_table_sessions.RowFilter = "SESS_TE_ISO_CODE = '" & cmb_currencies.TextValue & "'"
    End If

    _dt_table_sessions = _dv_table_sessions.ToTable()

    ' Check if there are sessions to report
    If _ds_query.Tables(RESULT_TABLE_SESSIONS).Rows.Count > 0 Then

      _sessions_results = New DataTable()

      ' DHA 09-FEB-2016: Floor Dual Currency filter rows
      If WSI.Common.Misc.IsFloorDualCurrencyEnabled Then
        'For Each _row_filter As DataRow In _ds_query.Tables(RESULT_TABLE_MOVEMENTS).Rows
        '  If cmb_currencies.TextValue <> CurrencyExchange.GetNationalCurrency() And (_row_filter("CM_TYPE") = CInt(CASHIER_MOVEMENT.NA_CASH_IN_SPLIT1) Or _row_filter("CM_TYPE") = CInt(CASHIER_MOVEMENT.NA_CASH_IN_SPLIT2)) Then
        '    _row_filter.Delete()
        '  ElseIf (cmb_currencies.TextValue = CurrencyExchange.GetNationalCurrency() Or String.IsNullOrEmpty(cmb_currencies.TextValue)) And _row_filter("CM_TYPE") = CInt(CASHIER_MOVEMENT.BILL_IN_EXCHANGE_DUAL_CURRENCY) Then
        '    _row_filter.Delete()
        '  End If
        'Next
        _ds_query.Tables(RESULT_TABLE_MOVEMENTS).AcceptChanges()
      End If

      ' Build the cashier session information
      If _ds_query.Tables.Count > 2 Then
        ' Here we have cashier_sessions_currency data
        CSReports.CashierSessionsReports.BuildGroupByConcept(_dt_table_sessions, _
                                                             _ds_query.Tables(RESULT_TABLE_MOVEMENTS), _
                                                             _ds_query.Tables(RESULT_TABLE_CURRENCIES), _
                                                             _sessions_results)
      Else
        ' Currencies calculated with movements information
        CSReports.CashierSessionsReports.BuildGroupByConcept(_dt_table_sessions, _
                                                             _ds_query.Tables(RESULT_TABLE_MOVEMENTS), _
                                                             Nothing, _
                                                             _sessions_results)
      End If

      Call GUI_BeforeFirstRow()

      Dim db_row As CLASS_DB_ROW
      Dim count As Integer
      Dim idx_row As Integer

      If _sessions_results.Rows.Count > 0 Then

        For Each _row In _sessions_results.Rows

          Try
            db_row = New CLASS_DB_ROW(_row)

            If GUI_CheckOutRowBeforeAdd(db_row) Then
              ' Add the db row to the grid
              Me.Grid.AddRow()
              count = count + 1
              idx_row = Me.Grid.NumRows - 1

              If Not GUI_SetupRow(idx_row, db_row) Then
                ' The row can not be added
                Me.Grid.DeleteRowFast(idx_row)
                count = count - 1
              End If
            End If

          Catch ex As OutOfMemoryException
            Throw ex

          Catch exception As Exception
            Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
            Call Trace.WriteLine(exception.ToString())
            Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                                  "frm_cashier_sessions", _
                                  "GUI_SetupRow", _
                                  exception.Message)
            Exit For
          End Try

          db_row = Nothing

          If Not GUI_DoEvents(Me.Grid) Then
            Exit Sub
          End If

        Next

        Call GUI_AfterLastRow()
      End If

    End If

  End Sub

  ' PURPOSE: Generates the session status filter for the query
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '     - String
  Private Function GetSessionStatusFilter() As String
    Dim _status As List(Of String)

    _status = New List(Of String)

    ' Assume all status
    _status.Add(WSI.Common.CASHIER_SESSION_STATUS.OPEN)
    _status.Add(WSI.Common.CASHIER_SESSION_STATUS.OPEN_PENDING)
    _status.Add(WSI.Common.CASHIER_SESSION_STATUS.CLOSED)
    _status.Add(WSI.Common.CASHIER_SESSION_STATUS.PENDING_CLOSING)

    ' If we check any status then refill the status filter with selected items
    If Me.chk_session_open.Checked = True Or Me.chk_session_closed.Checked = True Or Me.chk_session_pending.Checked = True Then

      _status.Clear()

      If Me.chk_session_open.Checked = True Then
        _status.Add(WSI.Common.CASHIER_SESSION_STATUS.OPEN)
        _status.Add(WSI.Common.CASHIER_SESSION_STATUS.OPEN_PENDING)
      End If

      If Me.chk_session_closed.Checked = True Then
        _status.Add(WSI.Common.CASHIER_SESSION_STATUS.CLOSED)
      End If

      If Me.chk_session_pending.Checked = True Then
        _status.Add(WSI.Common.CASHIER_SESSION_STATUS.PENDING_CLOSING)
      End If

    End If

    ' Return the items selected for status filter
    Return String.Join(",", _status.ToArray())
  End Function

  ' PURPOSE: Return the value for a currency type in the provided dictionary
  '
  '  PARAMS:
  '     - INPUT:
  '         - IsoCode
  '         - CodeType
  '         - CurrencyIsoCodeTypeValues
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '     - Value in Decimal
  Private Function GetIsoCodeTypeValue(ByVal IsoCode As String, ByVal CodeType As CurrencyExchangeType, ByVal CurrencyIsoCodeTypeValues As SortedDictionary(Of CurrencyIsoType, Decimal)) As Decimal
    Dim _ct As CurrencyIsoType
    _ct = New CurrencyIsoType()

    _ct.IsoCode = IsoCode
    _ct.Type = CodeType

    Return GetIsoCodeTypeValue(_ct, CurrencyIsoCodeTypeValues)
  End Function

  ' PURPOSE: Return the value for a currency type in the provided dictionary
  '
  '  PARAMS:
  '     - INPUT:
  '         - IsoCode
  '         - CodeType
  '         - CurrencyIsoCodeTypeValues
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '     - Value in Decimal
  Private Function GetIsoCodeTypeValue(ByVal IsoCode As String, ByVal CodeType As CurrencyExchangeType, ByVal CurrencyIsoCodeTypeValues As Dictionary(Of CurrencyIsoType, Decimal)) As Decimal
    Dim _ct As CurrencyIsoType
    _ct = New CurrencyIsoType()

    _ct.IsoCode = IsoCode
    _ct.Type = CodeType

    Return GetIsoCodeTypeValue(_ct, CurrencyIsoCodeTypeValues)
  End Function

  ' PURPOSE: Return the value for a currency type in the provided dictionary
  '
  '  PARAMS:
  '     - INPUT:
  '         - IsoCodeType      
  '         - CurrencyIsoCodeTypeValues
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '     - Value in Decimal
  Private Function GetIsoCodeTypeValue(ByVal IsoCodeType As CurrencyIsoType, ByVal CurrencyIsoCodeTypeValues As SortedDictionary(Of CurrencyIsoType, Decimal)) As Decimal
    If Not CurrencyIsoCodeTypeValues.ContainsKey(IsoCodeType) Then
      Return 0
    End If

    Return CurrencyIsoCodeTypeValues(IsoCodeType)
  End Function

  ' PURPOSE: Return the value for a currency type in the provided dictionary
  '
  '  PARAMS:
  '     - INPUT:
  '         - IsoCodeType      
  '         - CurrencyIsoCodeTypeValues
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '     - Value in Decimal
  Private Function GetIsoCodeTypeValue(ByVal IsoCodeType As CurrencyIsoType, ByVal CurrencyIsoCodeTypeValues As Dictionary(Of CurrencyIsoType, Decimal)) As Decimal
    If Not CurrencyIsoCodeTypeValues.ContainsKey(IsoCodeType) Then
      Return 0
    End If

    Return CurrencyIsoCodeTypeValues(IsoCodeType)
  End Function

  ' PURPOSE : Mantain the correct format of date inputs 
  '
  '  PARAMS :
  '     -  INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '
  Private Sub UpdateDateControlsByOption(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_movement_gaming_day.CheckedChanged, opt_movement_session.CheckedChanged, opt_opening_session.CheckedChanged
    ' Change format and reset values
    Dim _keep_check As Boolean = False
    Dim _open_time As DateTime = WSI.Common.Misc.TodayOpening()

    _keep_check = dtp_from.Checked
    dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                           IIf(opt_movement_gaming_day.Checked, ENUM_FORMAT_TIME.FORMAT_TIME_NONE, ENUM_FORMAT_TIME.FORMAT_HHMMSS))
    dtp_from.Value = New DateTime(dtp_from.Value.Date.Ticks + _open_time.TimeOfDay.Ticks)
    dtp_from.Checked = _keep_check
    dtp_from.Width = IIf(opt_movement_gaming_day.Checked, 166, 222)

    _keep_check = dtp_to.Checked
    dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                         IIf(opt_movement_gaming_day.Checked, ENUM_FORMAT_TIME.FORMAT_TIME_NONE, ENUM_FORMAT_TIME.FORMAT_HHMMSS))
    dtp_to.Value = New DateTime(dtp_to.Value.Date.Ticks + _open_time.TimeOfDay.Ticks)
    dtp_to.Checked = _keep_check
    dtp_from.Width = IIf(opt_movement_gaming_day.Checked, 222, 240)
    dtp_from.Height = 25
    dtp_to.Width = IIf(opt_movement_gaming_day.Checked, 222, 240)
    dtp_to.Height = 25

  End Sub

  ' PURPOSE : Fill currencies combo with defined values
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  Private Sub FillCurrenciesCombo()

    Dim tabla As DataTable

    tabla = Nothing

    WSI.Common.CurrencyExchange.GetFloorDualCurrenciesForCombo(tabla)
    Me.cmb_currencies.Clear()
    Me.cmb_currencies.Add(tabla, "CURRENCY_ID", "CE_CURRENCY_ISO_CODE")

  End Sub

  Private Sub GetComboTerminalsByCurrencies()

    Dim currency As String
    currency = cmb_currencies.TextValue
    ' Set combo with Cashier Terminals
    Call SetComboCashierAlias(Me.cmb_cashier, , , , "'" & currency & "'")

  End Sub

  Private Sub cmb_currencies_ValueChangedEvent() Handles cmb_currencies.ValueChangedEvent
    Call GetComboTerminalsByCurrencies()
  End Sub

End Class

