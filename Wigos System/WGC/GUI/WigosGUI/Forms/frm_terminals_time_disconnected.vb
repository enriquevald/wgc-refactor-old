﻿'-------------------------------------------------------------------
' Copyright © 2007 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_terminals_time_disconnected
' DESCRIPTION:   Form to show the disconnections terminals
' AUTHOR:        David Perelló
' CREATION DATE: 14-DEC-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 14-DEC-2017  DPC    Initial version
'--------------------------------------------------------------------

Imports GUI_Controls
Imports System.Text
Imports GUI_CommonMisc
Imports GUI_CommonOperations.ModuleDateTimeFormats
Imports WSI.Common

Public Class frm_terminals_time_disconnected
  Inherits frm_base_sel

#Region " Members "

  Private m_terminal_id As Int32
  Private m_is_multisite_center As Boolean
  Private m_sum_subtotal_egm_collector As Int32
  Private m_sum_subtotal_center_collector As Int32
  Private m_sum_total_subtotal_egm_collector As Int32
  Private m_sum_total_subtotal_center_collector As Int32

  Private m_filter_date_to As String
  Private m_filter_date_from As String
  Private m_filter_terminal As String
  Private m_filter_site As String
  Private m_filter_show_subtotal As String
  Private m_filter_show_location As String
  Private m_filter_show_only_disconnected As String

#End Region ' Members

#Region " Constants "

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_SITE_ID As Integer = 1
  Private Const GRID_COLUMN_TERMINAL_ID As Integer = 2
  Private Const GRID_COLUMN_PROVIDER_ID As Integer = 3
  Private Const GRID_COLUMN_TERMINAL_NAME As Integer = 4
  Private Const GRID_COLUMN_WORKING_DAY As Integer = 5
  Private Const GRID_COLUMN_COLLECTOR_SECONDS As Integer = 6
  Private Const GRID_COLUMN_COLLECTOR_CENTER_SECONDS As Integer = 7

  ' Grid Columns
  Private Const SQL_COLUMN_SITE_ID As Integer = 0
  Private Const SQL_COLUMN_TERMINAL_ID As Integer = 1
  Private Const SQL_COLUMN_PROVIDER_ID As Integer = 2
  Private Const SQL_COLUMN_TERMINAL_NAME As Integer = 3
  Private Const SQL_COLUMN_WORKING_DAY As Integer = 4
  Private Const SQL_COLUMN_COLLECTOR_SECONDS As Integer = 5
  Private Const SQL_COLUMN_COLLECTOR_CENTER_SECONDS As Integer = 6

  Private GRID_COLUMNS As Integer = 8
  Private Const GRID_HEADER_ROWS As Integer = 1

  Private GRID_INIT_TERMINAL_DATA As Integer = 5

  ' Width
  Private Const GRID_COLUMN_WIDTH_INDEX As Integer = 200
  Private Const GRID_COLUMN_WIDTH_SITE_ID As Integer = 1400
  Private Const GRID_COLUMN_WIDTH_TERMINAL_ID As Integer = 1400
  Private Const GRID_COLUMN_WIDTH_PROVIDER_ID As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_TERMINAL_NAME As Integer = 2200
  Private Const GRID_COLUMN_WIDTH_WORKING_DAY As Integer = 1400
  Private Const GRID_COLUMN_WIDTH_COLLECTOR_SECONDS As Integer = 1800
  Private Const GRID_COLUMN_WIDTH_COLLECTOR_CENTER_SECONDS As Integer = 1800

#End Region ' Constants

#Region " Overrides "

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_FORM.FORM_TERMINALS_TIME_DISCONNECTED
    Call MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId

  ''' <summary>
  ''' Show form
  ''' </summary>
  ''' <param name="MdiParent"></param>
  ''' <remarks></remarks>
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING

    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

  ''' <summary>
  ''' Init controls
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_InitControls()

    Dim _terminal_types As List(Of Int32)

    Call MyBase.GUI_InitControls()

    m_is_multisite_center = WSI.Common.GeneralParam.GetBoolean("MultiSite", "IsCenter", False)

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8812)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False

    Me.chk_show_only_disconnected.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8928)
    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)
    Me.chk_totals.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8833)
    Me.gb_init_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(268)
    Me.dtp_init_from.Text = GLB_NLS_GUI_AUDITOR.GetString(257)
    Me.dtp_init_to.Text = GLB_NLS_GUI_AUDITOR.GetString(258)
    Me.dtp_init_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    Me.dtp_init_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)

    _terminal_types = New List(Of Int32)
    _terminal_types.Add(TerminalTypes.SAS_HOST)

    If m_is_multisite_center Then
      Me.uc_site_select.Visible = True

      Me.gb_init_date.Location = New Point(310, 15)
      Me.uc_pr_list.Location = New Point(556, 11)
      Me.chk_totals.Location = New Point(316, 129)
      Me.chk_terminal_location.Location = New Point(316, 153)
      Me.chk_show_only_disconnected.Location = New Point(316, 175)

      Me.uc_site_select.ShowMultisiteRow = False
      Me.uc_site_select.MultiSelect = True
      Me.uc_site_select.Init()
      Me.uc_site_select.SetDefaultValues(False)

      Call Me.uc_pr_list.Init(_terminal_types.ToArray(), , , True)
    Else
      Call Me.uc_pr_list.Init(_terminal_types.ToArray())
    End If

    m_terminal_id = -1

    Call SetDefaultValues()

    Call GUI_StyleSheet()

  End Sub ' GUI_InitControls

  ''' <summary>
  ''' Get query
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _sql As StringBuilder

    _sql = New StringBuilder

    _sql.AppendLine("   SELECT   TTD_SITE_ID				              ")
    _sql.AppendLine("          , TTD_TERMINAL_ID		              ")
    _sql.AppendLine("          , TE_PROVIDER_ID                   ")
    _sql.AppendLine("          , TE_NAME                          ")
    _sql.AppendLine("          , TTD_WORKING_DAY		              ")
    _sql.AppendLine("          , TTD_EGM_COLLECTOR_SECONDS	      ")
    _sql.AppendLine("          , TTD_COLLECTOR_CENTER_SECONDS     ")
    _sql.AppendLine("     FROM   TERMINALS_TIME_DISCONNECTED      ")
    _sql.AppendLine("    INNER   JOIN    TERMINALS TE             ")
    _sql.AppendLine("       ON   TE_TERMINAL_ID = TTD_TERMINAL_ID ")
    If m_is_multisite_center Then
      _sql.AppendLine("      AND   TE_SITE_ID = TTD_SITE_ID ")
    End If
    _sql.AppendLine("    WHERE   1 = 1                            ")

    If dtp_init_from.Checked Then
      _sql.AppendLine(String.Format("      AND   TTD_WORKING_DAY >= {0}    ", GUI_FormatDateDB(dtp_init_from.Value)))
    End If

    If dtp_init_to.Checked Then
      _sql.AppendLine(String.Format("      AND   TTD_WORKING_DAY < {0}     ", GUI_FormatDateDB(dtp_init_to.Value)))
    End If

    _sql.AppendLine(String.Format("      AND   TTD_TERMINAL_ID IN {0}", Me.uc_pr_list.GetProviderIdListSelected()))

    If m_is_multisite_center AndAlso Me.uc_site_select.GetSitesIdListSelectedAll <> String.Empty Then
      _sql.AppendLine(String.Format("      AND   TTD_SITE_ID IN ({0})", Me.uc_site_select.GetSitesIdListSelectedAll))
    End If

    If (Me.chk_show_only_disconnected.Checked) Then
      _sql.AppendLine("      AND   (TTD_EGM_COLLECTOR_SECONDS > 0 OR TTD_COLLECTOR_CENTER_SECONDS > 0) ")
    End If

    _sql.AppendLine(" ORDER BY TTD_TERMINAL_ID, TTD_WORKING_DAY   ")

    Return _sql.ToString

  End Function ' GUI_FilterGetSqlQuery

  ''' <summary>
  ''' Set rows
  ''' </summary>
  ''' <param name="RowIndex"></param>
  ''' <param name="DbRow"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _terminal_data As List(Of Object)
    Dim _num_columns_location As Integer

    _num_columns_location = 0
    _terminal_data = Nothing

    If chk_terminal_location.Checked Then
      _num_columns_location = TerminalReport.NumColumns(ReportType.Location)
      _terminal_data = TerminalReport.GetReportDataList(DbRow.Value(SQL_COLUMN_TERMINAL_ID), ReportType.Location)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_SITE_ID) Then
      m_terminal_id = DbRow.Value(SQL_COLUMN_TERMINAL_ID)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SITE_ID).Value = DbRow.Value(SQL_COLUMN_SITE_ID)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_TERMINAL_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_ID).Value = DbRow.Value(SQL_COLUMN_TERMINAL_ID)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_PROVIDER_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROVIDER_ID).Value = DbRow.Value(SQL_COLUMN_PROVIDER_ID)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_TERMINAL_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_NAME).Value = DbRow.Value(SQL_COLUMN_TERMINAL_NAME)
    End If

    ' Terminal Report
    If chk_terminal_location.Checked Then
      For _idx As Int32 = 0 To _terminal_data.Count - 1
        If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value Then
          Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
        End If
      Next
    End If

    If Not DbRow.IsNull(SQL_COLUMN_WORKING_DAY) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_WORKING_DAY + _num_columns_location).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_WORKING_DAY),
                                                                                                     ENUM_FORMAT_DATE.FORMAT_DATE_SHORT,
                                                                                                     ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_COLLECTOR_SECONDS) Then
      m_sum_subtotal_egm_collector += DbRow.Value(SQL_COLUMN_COLLECTOR_SECONDS)
      m_sum_total_subtotal_egm_collector += DbRow.Value(SQL_COLUMN_COLLECTOR_SECONDS)

      Me.Grid.Cell(RowIndex, GRID_COLUMN_COLLECTOR_SECONDS + _num_columns_location).Value = TimeSpanToString(TimeSpan.FromSeconds(DbRow.Value(SQL_COLUMN_COLLECTOR_SECONDS)))
    End If

    If Not DbRow.IsNull(SQL_COLUMN_COLLECTOR_CENTER_SECONDS) Then
      m_sum_subtotal_center_collector += DbRow.Value(SQL_COLUMN_COLLECTOR_CENTER_SECONDS)
      m_sum_total_subtotal_center_collector += DbRow.Value(SQL_COLUMN_COLLECTOR_CENTER_SECONDS)

      Me.Grid.Cell(RowIndex, GRID_COLUMN_COLLECTOR_CENTER_SECONDS + _num_columns_location).Value = TimeSpanToString(TimeSpan.FromSeconds(DbRow.Value(SQL_COLUMN_COLLECTOR_CENTER_SECONDS)))
    End If

    Return True

  End Function ' GUI_SetupRow

  ''' <summary>
  ''' Reset filters
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
    Call Me.uc_pr_list.SetDefaultValues()
  End Sub ' GUI_FilterReset

  ''' <summary>
  ''' Set subtotals
  ''' </summary>
  ''' <param name="DbRow"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As CLASS_DB_ROW) As Boolean

    If m_terminal_id = -1 Then

      Return True
    End If

    If Not DbRow.IsNull(SQL_COLUMN_TERMINAL_ID) Then

      If m_terminal_id <> DbRow.Value(SQL_COLUMN_TERMINAL_ID) Then

        AddRowTotal(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1046), TimeSpanToString(TimeSpan.FromSeconds(m_sum_subtotal_egm_collector)), TimeSpanToString(TimeSpan.FromSeconds(m_sum_subtotal_center_collector)), ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

        m_sum_subtotal_egm_collector = 0
        m_sum_subtotal_center_collector = 0
      End If

    End If

    Return True

  End Function ' GUI_CheckOutRowBeforeAdd

  ''' <summary>
  ''' Set the las rows
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_AfterLastRow()

    AddRowTotal(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1046), TimeSpanToString(TimeSpan.FromSeconds(m_sum_subtotal_egm_collector)), TimeSpanToString(TimeSpan.FromSeconds(m_sum_subtotal_center_collector)), ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
    AddRowTotal(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1047), TimeSpanToString(TimeSpan.FromSeconds(m_sum_total_subtotal_egm_collector)), TimeSpanToString(TimeSpan.FromSeconds(m_sum_total_subtotal_center_collector)), ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

  End Sub ' GUI_AfterLastRow

  ''' <summary>
  ''' Filter check
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_FilterCheck() As Boolean

    If Me.dtp_init_from.Checked AndAlso Me.dtp_init_to.Checked AndAlso Me.dtp_init_from.Value > Me.dtp_init_to.Value Then
      Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call Me.dtp_init_to.Focus()

      Return False
    End If

    ' If Multisite then selected site needed
    If m_is_multisite_center Then
      If Not uc_site_select.FilterCheckSites() Then
        Return False
      End If
    End If

    m_terminal_id = -1
    m_sum_subtotal_egm_collector = 0
    m_sum_subtotal_center_collector = 0
    m_sum_total_subtotal_egm_collector = 0
    m_sum_total_subtotal_center_collector = 0

    Return True

  End Function ' GUI_FilterCheck

  ''' <summary>
  ''' Action before first row
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_BeforeFirstRow()

    Call GUI_StyleSheet()

  End Sub ' GUI_BeforeFirstRow

  ''' <summary>
  ''' Report filters.
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_filter_date_from = String.Empty
    m_filter_date_to = String.Empty
    m_filter_terminal = String.Empty
    m_filter_site = String.Empty
    m_filter_show_subtotal = String.Empty
    m_filter_show_location = String.Empty
    m_filter_show_only_disconnected = String.Empty

    If dtp_init_from.Checked Then
      m_filter_date_from = GUI_FormatDate(dtp_init_from.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    If dtp_init_to.Checked Then
      m_filter_date_to = GUI_FormatDate(dtp_init_to.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    m_filter_terminal = Me.uc_pr_list.GetTerminalReportText()

    If Me.chk_totals.Checked Then
      m_filter_show_subtotal = GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_filter_show_subtotal = GLB_NLS_GUI_INVOICING.GetString(480)
    End If

    If Me.chk_terminal_location.Checked Then
      m_filter_show_location = GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_filter_show_location = GLB_NLS_GUI_INVOICING.GetString(480)
    End If

    If Me.chk_show_only_disconnected.Checked Then
      m_filter_show_only_disconnected = GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_filter_show_only_disconnected = GLB_NLS_GUI_INVOICING.GetString(480)
    End If

    If m_is_multisite_center Then
      If Not String.IsNullOrEmpty(Me.uc_site_select.GetSitesIdListSelected()) Then
        m_filter_site = Me.uc_site_select.GetSitesIdListSelected()
      Else
        m_filter_site = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1337)
      End If
    End If

  End Sub ' GUI_ReportUpdateFilters

  ''' <summary>
  ''' Sets the report Filter values for Excel and Print
  ''' </summary>
  ''' <param name="PrintData"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    If m_is_multisite_center Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1803), m_filter_site)
    End If

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5846), m_filter_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5847), m_filter_date_to)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(8833), m_filter_show_subtotal)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237), m_filter_show_location)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(8928), m_filter_show_only_disconnected)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4895), m_filter_terminal)


  End Sub ' GUI_ReportFilter

#End Region ' Overrides

#Region " Private methods "

  ''' <summary>
  ''' Set style grid
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GUI_StyleSheet()

    Dim _terminal_columns As List(Of ColumnSettings)
    Dim _num_columns As Integer
    Dim _num_columns_location As Integer

    _num_columns = GRID_COLUMNS
    _num_columns_location = 0

    If chk_terminal_location.Checked Then
      _num_columns_location = TerminalReport.NumColumns(ReportType.Location)
      _num_columns = GRID_COLUMNS + _num_columns_location
    End If

    With Me.Grid
      Call .Init(_num_columns, GRID_HEADER_ROWS)

      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Header(1).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = GRID_COLUMN_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      'Site Id
      .Column(GRID_COLUMN_SITE_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1927) ' Site ID
      .Column(GRID_COLUMN_SITE_ID).Width = GRID_COLUMN_WIDTH_SITE_ID
      .Column(GRID_COLUMN_SITE_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'Terminal Id
      .Column(GRID_COLUMN_TERMINAL_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1968) ' Terminal id
      .Column(GRID_COLUMN_TERMINAL_ID).Width = GRID_COLUMN_WIDTH_TERMINAL_ID
      .Column(GRID_COLUMN_TERMINAL_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'Provider Id
      .Column(GRID_COLUMN_PROVIDER_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(469) ' Provider id
      .Column(GRID_COLUMN_PROVIDER_ID).Width = GRID_COLUMN_WIDTH_PROVIDER_ID
      .Column(GRID_COLUMN_PROVIDER_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'Terminal name
      .Column(GRID_COLUMN_TERMINAL_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(693) ' Terminal name
      .Column(GRID_COLUMN_TERMINAL_NAME).Width = GRID_COLUMN_WIDTH_TERMINAL_NAME
      .Column(GRID_COLUMN_TERMINAL_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      '  Terminal Report
      If chk_terminal_location.Checked Then
        _terminal_columns = TerminalReport.GetColumnStyles(ReportType.Location)

        For _idx As Int32 = 0 To _terminal_columns.Count - 1
          .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = _terminal_columns(_idx).Header0
          .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = _terminal_columns(_idx).Width
          .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = _terminal_columns(_idx).Alignment
        Next
      End If

      ' Working day
      .Column(GRID_COLUMN_WORKING_DAY + _num_columns_location).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5723) ' Working day
      .Column(GRID_COLUMN_WORKING_DAY + _num_columns_location).Width = GRID_COLUMN_WIDTH_WORKING_DAY
      .Column(GRID_COLUMN_WORKING_DAY + _num_columns_location).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Collector
      .Column(GRID_COLUMN_COLLECTOR_SECONDS + _num_columns_location).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4489) ' Machine - Collector
      .Column(GRID_COLUMN_COLLECTOR_SECONDS + _num_columns_location).Width = GRID_COLUMN_WIDTH_COLLECTOR_SECONDS
      .Column(GRID_COLUMN_COLLECTOR_SECONDS + _num_columns_location).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Collector center
      .Column(GRID_COLUMN_COLLECTOR_CENTER_SECONDS + _num_columns_location).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4488) ' Collector - Center
      .Column(GRID_COLUMN_COLLECTOR_CENTER_SECONDS + _num_columns_location).Width = GRID_COLUMN_WIDTH_COLLECTOR_CENTER_SECONDS
      .Column(GRID_COLUMN_COLLECTOR_CENTER_SECONDS + _num_columns_location).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    End With

  End Sub ' GUI_StyleSheet

  ''' <summary>
  ''' Set defaults values
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SetDefaultValues()

    Dim _closing_time As Integer
    Dim _str_closing_time As String

    _closing_time = GetDefaultClosingTime()
    _str_closing_time = GUI_FormatDate(New DateTime(TimeSpan.FromHours(_closing_time).Ticks), ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMM)

    Me.lbl_from_hour.Text = _str_closing_time
    Me.lbl_to_hour.Text = _str_closing_time

    Me.dtp_init_to.Value = WGDB.Now.Date.AddHours(_closing_time)
    Me.dtp_init_to.Checked = False
    Me.dtp_init_from.Value = Me.dtp_init_to.Value.AddDays(-1)
    Me.dtp_init_from.Checked = True

    Me.chk_totals.Checked = False

    Me.chk_terminal_location.Checked = False

    Me.chk_show_only_disconnected.Checked = True

    If m_is_multisite_center Then
      Me.uc_site_select.SetFirstValue()
    End If

  End Sub ' SetDefaultValues

  ''' <summary>
  ''' Add sobtotal and total row 
  ''' </summary>
  ''' <param name="TextType"></param>
  ''' <param name="TotalEgmCollector"></param>
  ''' <param name="TotalCenterCollector"></param>
  ''' <param name="ColorCell"></param>
  ''' <remarks></remarks>
  Private Sub AddRowTotal(ByVal TextType As String, ByVal TotalEgmCollector As String, ByVal TotalCenterCollector As String, ByRef ColorCell As ENUM_GUI_COLOR)

    Dim prev_redraw As Boolean
    Dim _num_columns_location As Integer

    _num_columns_location = 0

    If Not chk_totals.Checked Then

      Return
    End If

    prev_redraw = Me.Grid.Redraw
    Me.Grid.Redraw = False

    Me.Grid.AddRow()

    Me.Grid.Redraw = False

    Me.Grid.Cell(Grid.NumRows - 1, GRID_COLUMN_INDEX).Value = String.Empty
    Me.Grid.Cell(Grid.NumRows - 1, GRID_COLUMN_SITE_ID).Value = TextType
    Me.Grid.Cell(Grid.NumRows - 1, GRID_COLUMN_TERMINAL_ID).Value = String.Empty
    Me.Grid.Cell(Grid.NumRows - 1, GRID_COLUMN_PROVIDER_ID).Value = String.Empty
    Me.Grid.Cell(Grid.NumRows - 1, GRID_COLUMN_TERMINAL_NAME).Value = String.Empty
    Me.Grid.Cell(Grid.NumRows - 1, GRID_COLUMN_WORKING_DAY).Value = String.Empty

    If chk_terminal_location.Checked Then
      _num_columns_location = TerminalReport.NumColumns(ReportType.Location)

      For _idx As Int16 = 1 To _num_columns_location
        Me.Grid.Cell(Grid.NumRows - 1, GRID_INIT_TERMINAL_DATA + _idx).Value = String.Empty
      Next
    End If

    Me.Grid.Cell(Grid.NumRows - 1, GRID_COLUMN_COLLECTOR_SECONDS + _num_columns_location).Value = TotalEgmCollector
    Me.Grid.Cell(Grid.NumRows - 1, GRID_COLUMN_COLLECTOR_CENTER_SECONDS + _num_columns_location).Value = TotalCenterCollector

    Me.Grid.Row(Grid.NumRows - 1).BackColor = GetColor(ColorCell)

    Me.Grid.Redraw = prev_redraw

  End Sub ' AddRowTotal

#End Region ' Private methods

End Class