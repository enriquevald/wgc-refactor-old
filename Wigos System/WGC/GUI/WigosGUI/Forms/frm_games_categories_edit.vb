﻿'-------------------------------------------------------------------
' Copyright © 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_games_categories_edit.vb
'
' DESCRIPTION : Window for edit categories and items of Games
'               In this window can add and edit items to the selected category,
'               edit title, description and enabled or disabled the item or category.
'
' AUTHOR:        Sergio Daniel Soria
'
' CREATION DATE: 14-SEP-2015
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 22-AUG-2016  SDS    Initial version  
' 28-SEP-2016  AVZ    PBI 18069:Pruebas y Ajustes - Games. Add image to game category 
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off

#Region "Imports"
Imports GUI_Controls
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls.uc_grid
Imports GUI_Controls.uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE
Imports System.Runtime.InteropServices
Imports System.Windows.Forms.UserControl

#End Region  ' Imports

Public Class frm_games_categories_edit
  Inherits frm_base_edit
#Region " Constants "

  Protected Const IMG_MAX_WIDTH As Integer = 1280
  Protected Const IMG_MAX_HEIGH As Integer = 1024

  Private Const LEN_CATEGORY_TITLE As Integer = 100
  Private Const LEN_CATEGORY_DESCRIPTION As Integer = 500

  ' Database codes
  Private Const CATEGORY_STATUS_ENABLED As Integer = 0
  Private Const CATEGORY_STATUS_DISABLED As Integer = 1


#End Region

#Region " Members "

  Private m_category As CLS_GAMES_CATEGORY
  Private m_changed_values As Boolean
  Private m_new_count_id As Integer
  Private m_new_items As Stack



#End Region ' Members

#Region " Overrides "

  Public Overloads Sub ShowEditItem(Optional ByVal OperationId As Int64 = 0)

    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    If OperationId = 0 Then
      Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW
    End If

    Me.DbObjectId = OperationId


    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)

    If Me.DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If Me.DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowEditItem

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_GAME_CATEGORIES_EDIT

    Call MyBase.GUI_SetFormId()
  End Sub 'GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    ' Hide the delete button 
    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False

    If Me.ScreenMode = frm_base_edit.ENUM_SCREEN_MODE.MODE_NEW Then
      Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7526)
      m_changed_values = False
      rb_category_enabled.Checked = True
    Else
      ef_category_title.TextVisible = True
      Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7527)
      m_changed_values = True
    End If

    'Category Code

    'Category Title
    Me.lbl_category_title.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7522)
    Me.ef_category_title.TextVisible = True
    Me.ef_category_title.Value = ""
    Me.ef_category_title.Focus()
    Me.ef_category_title.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_CATEGORY_TITLE)


    'Category Description
    lbl_category_description.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7525)
    tb_description.MaxLength = LEN_CATEGORY_DESCRIPTION

    'Category Status
    gb_category_state.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7523)
    rb_category_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3008)
    rb_category_disabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3359)

    Me.Uc_imageList.Init(uc_image.MAXIMUN_RESOLUTION.x200X150)

  End Sub ' GUI_InitControls

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    '   m_category = Me.DbReadObject

    ' Name
    Me.ef_category_title.Value = m_category.Title

    ' Description
    Me.tb_description.Text = m_category.Description

    ' Enabled
    Me.rb_category_enabled.Checked = m_category.Enabled
    Me.rb_category_disabled.Checked = Not m_category.Enabled

    If Not IsNothing(m_category.Image) Then
      Me.Uc_imageList.Image = m_category.Image
    Else
      Me.Uc_imageList.Image = Nothing
    End If

  End Sub ' GUI_SetScreenData

  ' PURPOSE: Validate the data presented on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - True:  Data is ok
  '     - False: Data is NOT ok
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Dim _all_data_ok As Boolean = True
    Dim _num_category_checked As Integer
    m_category = DbEditedObject
    _num_category_checked = 0

    ' Check title category
    If Not Me.ef_category_title.Value.Length > 0 Then
      _all_data_ok = False
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7528), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(6679)) ' "Field '%1' must not be left blank." param %1-> "Name"
    Else
      If m_category.ExistsCategoryName(Me.ef_category_title.Value, m_category.CategoryId) <> ENUM_STATUS.STATUS_OK Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7529), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , Me.ef_category_title.Value) ' "A caregory named %1 already exists."
        _all_data_ok = False
      End If
    End If

    If (Me.Uc_imageList.Image Is Nothing) Then
      _all_data_ok = False
      ' Must have picture
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7638), ENUM_MB_TYPE.MB_TYPE_WARNING)
    End If

    Return _all_data_ok

  End Function 'GUI_IsScreenDataOk

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_GetScreenData()

    Try
      m_category = Me.DbEditedObject

      ' Name
      m_category.Title = Me.ef_category_title.Value

      ' Enabled
      m_category.Enabled = Me.rb_category_enabled.Checked

      ' Description
      m_category.Description = Me.tb_description.Text

      ' Image
      If Not IsNothing(Me.Uc_imageList.Image) Then
        m_category.Image = Me.Uc_imageList.Image
      Else
        m_category.Image = Nothing
      End If

      'm_category.LastUpdate = WSI.Common.WGDB.Now

    Catch ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, Me.Name, "GUI_GetScreenData", ex.Message)

    End Try

  End Sub ' GUI_GetScreenData

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLS_GAMES_CATEGORY()
        m_category = DbEditedObject
        m_category.CategoryId = 0
        DbStatus = ENUM_STATUS.STATUS_OK
      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7530), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7531), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7531), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE: Button overridable operations. 
  '          Define actions for each button are pressed.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Select Case ButtonId


      Case frm_base_sel.ENUM_BUTTON.BUTTON_OK
        'ACCEPT
        If GUI_IsScreenDataOk() Then
          'Accept changes
          Call MyBase.GUI_ButtonClick(ButtonId)
        End If

      Case Else
        MyBase.GUI_ButtonClick(ButtonId)

    End Select

  End Sub  ' GUI_ButtonClick

#End Region '  Overrides 

#Region " Private Functions "



  
#End Region ' private Functions

#Region "Events"

  


#End Region 'Events


End Class