'-------------------------------------------------------------------
' Copyright � 2003 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_main_cashdesk_draws.vb
'
' DESCRIPTION:   Wigos GUI CashDeskDraws main form..
'
' AUTHOR:        Joaquin Calero Andujar
'
' CREATION DATE: 28-AUG-2013
'
' REVISION HISTORY:
' 
' Date        Author  Description
' ----------- ------  -----------------------------------------------
' 28-AUG-2013  JCA    Initial version
' 02-DEC-2013  RMS    Added app version to form title
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common

Public Class frm_main_cashdesk_draws
  Inherits GUI_Controls.frm_base_mdi

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub
  Friend WithEvents lbl_main_title As GUI_Controls.uc_text_field

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.lbl_main_title = New GUI_Controls.uc_text_field
    Me.SuspendLayout()
    '
    'lbl_main_title
    '
    Me.lbl_main_title.IsReadOnly = True
    Me.lbl_main_title.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_main_title.LabelForeColor = System.Drawing.Color.Blue
    Me.lbl_main_title.Location = New System.Drawing.Point(282, 19)
    Me.lbl_main_title.Name = "lbl_main_title"
    Me.lbl_main_title.Size = New System.Drawing.Size(66, 24)
    Me.lbl_main_title.SufixText = "Sufix Text"
    Me.lbl_main_title.SufixTextVisible = True
    Me.lbl_main_title.TabIndex = 1
    Me.lbl_main_title.Visible = False
    '
    'frm_main
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(496, 272)
    Me.Controls.Add(Me.lbl_main_title)
    Me.Name = "frm_main"
    Me.Text = "xWigos GUI"
    Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    Me.Controls.SetChildIndex(Me.lbl_main_title, 0)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Delegates "

  Private Delegate Sub DelegatesNoParams()

#End Region

#Region " Members "

  ' DHA 27-MAR-2013: Multisite status
  Dim MultisiteStatus As WGDB.MULTISITE_STATUS = WGDB.MULTISITE_STATUS.UNKNOWN

  Dim ProfilesMenuItem As CLASS_MENU_ITEM

  Dim GeneralParamsMenuItem As CLASS_MENU_ITEM

  Dim ExitMenuItem As CLASS_MENU_ITEM

  Dim Auditor As CLASS_MENU_ITEM

  Dim Alarms As CLASS_MENU_ITEM

  Dim Svc_monitor As CLASS_MENU_ITEM


  Dim Sw_download_versions As CLASS_MENU_ITEM
  Dim Misc_sw_build As CLASS_MENU_ITEM
  Dim Licences As CLASS_MENU_ITEM

  Dim Tools As CLASS_MENU_ITEM

  ' JML 19-JUN-2012
  'Dim Configuration_menu As CLASS_MENU_ITEM

  ' HBB 2-AUG-2012
  'Dim Password_Configuration As CLASS_MENU_ITEM



  ' MMG 08-JUL-2013 
  Dim Draws_cashdesk_detail As CLASS_MENU_ITEM

  ' MMG 16-JUL-2013 
  Dim Draws_cashdesk_summary As CLASS_MENU_ITEM

#End Region

#Region " Must Override Methods: frm_base_mdi "

  ' PURPOSE: Set initial GUI permissions.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '       - Perm: Permissions set.
  '
  ' RETURNS:
  Protected Overrides Sub GUI_Permissions(ByRef Perm As GUI_Controls.CLASS_GUI_USER.TYPE_PERMISSIONS)
    Perm.Read = True
    Perm.Write = False
    Perm.Delete = False
    Perm.Execute = False

  End Sub ' GUI_Permissions

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_MAIN

    ' TJG 01-FEB-2005
    ' Set the form icon from the embedded resource (ICO file must be built in the project as "Embedded Resource")
    ' Call could be made as GetManifestResourceStream("GUI_Auditor.GUI_Auditor.ico"))

    '------------------------------------------------
    ' XVV 13/04/2007
    ' Translate tu BASE Form
    ' Me.Icon = New Icon(System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream(Me.GetType(), "WigosGUI.ico"))
    '------------------------------------------------

    '------------------------------------------------
    'XVV 13/04/2007
    'Call Base Form proc
    Call MyBase.GUI_SetFormId()
    '------------------------------------------------

  End Sub ' GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    ' Initialize control to keep form title. A control is used to manage event update
    lbl_main_title.Value = ""

    AddHandler WGDB.OnStatusChanged, AddressOf DatabaseStatusChanged

    ''DHA 27-03-2013:
    'AddHandler WGDB.OnMultiSiteStatusChanged, AddressOf MultiSiteStatusChanged
    ' RCI & MPO 02-APR-2012
    AddHandler Users.AutoLogout, AddressOf OnAutoLogout

    ' Add profiles for all forms on the GUI
    ProfilesMenuItem.Status = CurrentUser.Permissions(ProfilesMenuItem.FormId).Read

    GeneralParamsMenuItem.Status = CurrentUser.Permissions(GeneralParamsMenuItem.FormId).Read
    Svc_monitor.Status = CurrentUser.Permissions(Svc_monitor.FormId).Read

    Auditor.Status = CurrentUser.Permissions(Auditor.FormId).Read

    ' "SW Download"
    Sw_download_versions.Status = CurrentUser.Permissions(Sw_download_versions.FormId).Read
    Misc_sw_build.Status = CurrentUser.Permissions(Misc_sw_build.FormId).Read
    Licences.Status = CurrentUser.Permissions(Licences.FormId).Read

    Alarms.Status = CurrentUser.Permissions(Alarms.FormId).Read

    ' HBB 02-AUG-2012
    'Password_Configuration.Status = CurrentUser.Permissions(Password_Configuration.FormId).Read


    'If Not GeneralParam.GetBoolean("Site", "Configured") Then
    '  m_site_config_Click(Me, Nothing)
    'End If

    'MMG 08-JUL-2013
    Draws_cashdesk_detail.Status = CurrentUser.Permissions(Draws_cashdesk_detail.FormId).Read

    'MMG 16-JUL-2013
    Draws_cashdesk_summary.Status = CurrentUser.Permissions(Draws_cashdesk_summary.FormId).Read

  End Sub ' GUI_InitControls

  ' PURPOSE: First screen activation actions:
  '           - Set form title
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_FirstActivation()

    ' Retrieve Site Identifier and Name from database just once
    'GLB_SiteId = GetSiteId()
    'GLB_SiteName = GetSiteName()

    SetMainTitle()

  End Sub ' GUI_FirstActivation

  ' PURPOSE: Menu creation
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_CreateMenu()
    Dim cls_menu As CLASS_MENU_ITEM
    Dim super_center_flag As String

    '
    ' SYSTEM MENU
    '
    cls_menu = m_mnu.AddItem(GLB_NLS_GUI_AUDITOR.Id(453), ENUM_FORM.FORM_MAIN)
    ProfilesMenuItem = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_CONFIGURATION.Id(467), ENUM_FORM.FORM_USER_PROFILE_SEL, AddressOf m_Click_Profiles)


    ' Configuration menu JML 18-JUN-2012
    'Configuration_menu = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1063), ENUM_FORM.FORM_CURRENCIES, Nothing)
    ' Cashier config

    ' HBB 02-AUG-2012: Password Configuration '
    'Password_Configuration = m_mnu.AddItem(Configuration_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1175), ENUM_FORM.FORM_PASSWORD_CONFIGURATION, AddressOf m_password_configuration_Click)

    '' Player tracking config SLE 14-ENE-2010
    'Player_Tracking_Configuration = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(453), ENUM_FORM.FORM_PLAYER_TRAKING_CONFIGURATION, AddressOf m_player_tracking_configuration_Click)
    GeneralParamsMenuItem = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_AUDITOR.Id(460), ENUM_FORM.FORM_GENERAL_PARAMS, AddressOf m_Click_GeneralParams)
    ' APB 27-NOV-2008
    Svc_monitor = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_SYSTEM_MONITOR.Id(451), ENUM_FORM.FORM_SERVICE_MONITOR, AddressOf m_Svc_monitor_Click)
    ' JGR 17-FEB-2012: Area & Bank

    ' RBG 21-JUN-2013
    ' APB 03-FEB-2011: Sites and Site Operators
    super_center_flag = GetSuperCenterData("Enabled")
    If super_center_flag = "1" Then
      ' Add item to menu only if SuperCenter features are enabled
      'Sites_Operators = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_CONFIGURATION.Id(453), ENUM_FORM.FORM_SITES_OPERATORS_SEL, AddressOf m_Sites_operators_Click)
    End If
    m_mnu.AddBreak(cls_menu)


    m_mnu.AddItem(cls_menu, GLB_NLS_GUI_AUDITOR.Id(452), 0, AddressOf m_Exit_Click)

    '
    ' AUDIT MENU
    '
    cls_menu = m_mnu.AddItem(GLB_NLS_GUI_AUDITOR.Id(459), ENUM_FORM.FORM_MAIN)
    Auditor = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_AUDITOR.Id(451), ENUM_FORM.FORM_GUI_AUDITOR, AddressOf m_Auditor_Click)


    '
    ' EVENTS MENU
    '
    cls_menu = m_mnu.AddItem(GLB_NLS_GUI_AUDITOR.Id(455), ENUM_FORM.FORM_MAIN)
    Alarms = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_ALARMS.Id(454), ENUM_FORM.FORM_ALARMS, AddressOf m_Alarms_Click)

    '
    ' STATISTICS MENU
    '
    cls_menu = m_mnu.AddItem(GLB_NLS_GUI_STATISTICS.Id(201), ENUM_FORM.FORM_MAIN)

    ' MMG 08-JUL-2013   Cash desk draws detail report
    Draws_cashdesk_detail = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(2245), ENUM_FORM.FORM_DRAWS_CASHDESK_DETAIL, AddressOf m_Draws_cashdesk_detail_click)

    ' MMG 16-JUL-2013   Cash desk draws summary report
    Draws_cashdesk_summary = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(2364), ENUM_FORM.FORM_DRAWS_CASHDESK_SUMMARY, AddressOf m_Draws_cashdesk_sumary_click)

    ' 
    ' SW DOWNLOAD MENU -- 451 "SW Download" // TJG 05-SEP-2008
    '
    cls_menu = m_mnu.AddItem(GLB_NLS_GUI_SW_DOWNLOAD.Id(451), ENUM_FORM.FORM_MAIN)
    ' 452 "Versiones de Software"
    Sw_download_versions = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_SW_DOWNLOAD.Id(452), ENUM_FORM.FORM_SW_DOWNLOAD_VERSIONS, AddressOf m_Sw_download_versions_Click)
    ' APB 07-APR-2009
    ' 454 "Control de Versiones"
    Misc_sw_build = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_SW_DOWNLOAD.Id(454), ENUM_FORM.FORM_MISC_SW_BY_BUILD, AddressOf m_Misc_sw_build_Click)
    ' APB 28-APR-2009
    ' 455 "Licencias"
    Licences = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_SW_DOWNLOAD.Id(455), ENUM_FORM.FORM_LICENCE_VERSIONS, AddressOf m_Licence_Click)

  End Sub 'GUI_CreateMenu

  ' PURPOSE: GUI Exit actions.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_Exit()

    MyBase.GUI_Exit()

    'logout the user
    Call GLB_CurrentUser.Logout()

    ' Exit All threads ...
    Environment.Exit(0)

  End Sub ' GUI_Exit

#End Region

#Region " Private Functions "

  ' PURPOSE : Called when the database status has changed.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  ' NOTES:
  Private Sub DatabaseStatusChanged()

    Try
      If GLB_DbServerName <> WGDB.DataSource Then

        GLB_DbServerName = WGDB.DataSource

        If lbl_main_title.InvokeRequired Then
          Call Me.Invoke(New DelegatesNoParams(AddressOf ChangeMainTitle))
        End If
      End If
    Catch ex As Exception

    End Try

  End Sub ' DatabaseStatusChanged

  ' PURPOSE : Called when the multisite status has changed.
  '
  '  PARAMS:
  '     - INPUT:  OldStatus: old status multisite server
  '               NewStatus: new status multisite server
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  ' NOTES:
  Private Sub MultiSiteStatusChanged(ByVal OldStatus As WGDB.MULTISITE_STATUS, ByVal NewStatus As WGDB.MULTISITE_STATUS)

    Try
      MultisiteStatus = NewStatus

      If lbl_main_title.InvokeRequired Then
        Call Me.Invoke(New DelegatesNoParams(AddressOf ChangeMainTitle))
      End If
    Catch _ex As Exception

    End Try
  End Sub 'MultiSiteStatusChanged

  ' PURPOSE : Build main form title.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub SetMainTitle()

    'Dim site_id As String
    'Dim site_name As String

    lbl_main_title.Value = GLB_NLS_GUI_AUDITOR.GetString(475) & Space(2) & "[" & WGDB.GetAppVersion() & "]"

    'site_id = GetSiteId()
    'site_name = GetSiteName()

    If CurrentUser.IsLogged Then
      lbl_main_title.Value = lbl_main_title.Value & " - " & CurrentUser.Name & "@" & GLB_DbServerName '& "     SITE: " & site_id & " - " & site_name
    Else
      If GLB_DbServerName <> "" Then
        lbl_main_title.Value = lbl_main_title.Value & " - @" & GLB_DbServerName '& "     SITE: " & site_id & " - " & site_name
      End If
    End If

    '' DHA 26-MAR-2013: Add MultiSite server status
    'If MultisiteStatus = WGDB.MULTISITE_STATUS.MEMBER_CONNECTED Then
    '  lbl_main_title.Value = lbl_main_title.Value & "     " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1830)
    'ElseIf MultisiteStatus = WGDB.MULTISITE_STATUS.MEMBER_NOT_CONNECTED Then
    '  lbl_main_title.Value = lbl_main_title.Value & "     " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1831)
    'ElseIf MultisiteStatus = WGDB.MULTISITE_STATUS.MEMBER Then
    '  lbl_main_title.Value = lbl_main_title.Value & "     " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1829)
    'End If

    Me.Text = lbl_main_title.Value

  End Sub ' SetMainTitle

  ' PURPOSE : Modify main form title when a database status change.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub ChangeMainTitle()

    SetMainTitle()

  End Sub ' ChangeMainTitle

#End Region

#Region " Menu Selection Events "

  Private Sub m_Click_Profiles(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_user_profiles_sel

    'XVV Replace Me.Cursor.Current to Windows.Forms.cursor 
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance 
    frm = New frm_user_profiles_sel
    Call frm.ShowForEdit(Me)

    'XVV Replace Me.Cursor.Current to Windows.Forms.cursor 
    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Click_GeneralParams(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_genaral_params

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance 
    frm = New frm_genaral_params
    Call frm.ShowForEdit(Me)

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Auditor_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_auditor

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.WaitCursor
    ' Create instance 
    frm = New frm_auditor
    Call frm.ShowForEdit(Me)

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub


  Private Sub m_Exit_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.WaitCursor
    Me.Close()

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Alarms_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_alarms

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_alarms
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  
  Private Sub m_Svc_monitor_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_service_monitor

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_service_monitor
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Sw_download_versions_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_sw_package_versions

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_sw_package_versions
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Misc_sw_build_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_misc_sw_version

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_misc_sw_version
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Licence_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_licence_versions

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_licence_versions
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  'HBB 02-AUG-2012'
  'Private Sub m_password_configuration_Click(ByVal sender As Object, ByVal e As System.EventArgs)
  '  Dim frm As frm_password_configuration

  '  Windows.Forms.Cursor.Current = Cursors.WaitCursor

  '  ' Create instance
  '  frm = New frm_password_configuration()
  '  frm.ShowEditItem(Me)

  '  Windows.Forms.Cursor.Current = Cursors.Default

  'End Sub

  'MMG 08-Jul-2013 
  Private Sub m_Draws_cashdesk_detail_click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_draws_cashdesk_detail_sel

    Windows.Forms.Cursor.Current = Cursors.Default

    ' Create instance
    frm = New frm_draws_cashdesk_detail_sel
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  'MMG 16-Jul-2013 
  Private Sub m_Draws_cashdesk_sumary_click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_draws_cashdesk_summary_sel

    Windows.Forms.Cursor.Current = Cursors.Default

    ' Create instance
    frm = New frm_draws_cashdesk_summary_sel
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  'Private Sub m_Cashdesk_draws_configuration_click(ByVal sender As Object, ByVal e As System.EventArgs)

  '  Dim frm As frm_cashdesk_draws_configuration_edit

  '  Windows.Forms.Cursor.Current = Cursors.Default

  '  ' Create instance
  '  frm = New frm_cashdesk_draws_configuration_edit
  '  frm.ShowForEdit(Me)

  '  Windows.Forms.Cursor.Current = Cursors.Default

  'End Sub

#End Region

#Region " Public Functions "

  Public Sub ShowMainWindow()

    Me.Display(True)

  End Sub

  Public Sub OnAutoLogout(ByVal Title As String, ByVal Message As String)

    If Me.InvokeRequired Then
      Me.Invoke(New Users.AutoLogoutEventHandler(AddressOf OnAutoLogout), New Object() {Title, Message})
    Else
      If frm_message.ShowTimer(Me, 60, Message, Title) = Windows.Forms.DialogResult.None Then

        Users.SetUserLoggedOff(Users.EXIT_CODE.EXPIRED)
        WSI.Common.Log.Close()
        Environment.Exit(0)
      Else
        WSI.Common.Users.SetLastAction("Continue", "")
      End If
    End If

  End Sub

#End Region

End Class
