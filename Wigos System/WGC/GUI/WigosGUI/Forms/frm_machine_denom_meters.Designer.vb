<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_machine_denom_meters
    Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.uc_pr_list = New GUI_Controls.uc_provider()
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox()
    Me.lbl_payout = New System.Windows.Forms.Label()
    Me.lbl_netwin = New System.Windows.Forms.Label()
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.uc_dsl)
    Me.panel_filter.Controls.Add(Me.lbl_payout)
    Me.panel_filter.Controls.Add(Me.lbl_netwin)
    Me.panel_filter.Controls.Add(Me.chk_terminal_location)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Size = New System.Drawing.Size(1226, 205)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_terminal_location, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_netwin, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_payout, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dsl, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 209)
    Me.panel_data.Size = New System.Drawing.Size(1226, 509)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1220, 19)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1220, 4)
    '
    'uc_pr_list
    '
    Me.uc_pr_list.Location = New System.Drawing.Point(286, 3)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(336, 187)
    Me.uc_pr_list.TabIndex = 4
    '
    'chk_terminal_location
    '
    Me.chk_terminal_location.Location = New System.Drawing.Point(10, 100)
    Me.chk_terminal_location.Name = "chk_terminal_location"
    Me.chk_terminal_location.Size = New System.Drawing.Size(248, 16)
    Me.chk_terminal_location.TabIndex = 1
    Me.chk_terminal_location.Text = "xShow terminals location"
    '
    'lbl_payout
    '
    Me.lbl_payout.AutoSize = True
    Me.lbl_payout.ForeColor = System.Drawing.Color.Navy
    Me.lbl_payout.Location = New System.Drawing.Point(7, 133)
    Me.lbl_payout.Name = "lbl_payout"
    Me.lbl_payout.Size = New System.Drawing.Size(53, 13)
    Me.lbl_payout.TabIndex = 2
    Me.lbl_payout.Text = "xPayout"
    '
    'lbl_netwin
    '
    Me.lbl_netwin.AutoSize = True
    Me.lbl_netwin.ForeColor = System.Drawing.Color.Navy
    Me.lbl_netwin.Location = New System.Drawing.Point(7, 155)
    Me.lbl_netwin.Name = "lbl_netwin"
    Me.lbl_netwin.Size = New System.Drawing.Size(52, 13)
    Me.lbl_netwin.TabIndex = 3
    Me.lbl_netwin.Text = "xNetwin"
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.ClosingTimeEnabled = False
    Me.uc_dsl.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.Location = New System.Drawing.Point(10, 7)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.ShowBorder = True
    Me.uc_dsl.Size = New System.Drawing.Size(257, 80)
    Me.uc_dsl.TabIndex = 0
    Me.uc_dsl.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    '
    'frm_machine_denom_meters
    '
    Me.ClientSize = New System.Drawing.Size(1234, 722)
    Me.Name = "frm_machine_denom_meters"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox
    Friend WithEvents lbl_payout As System.Windows.Forms.Label
  Friend WithEvents lbl_netwin As System.Windows.Forms.Label
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector


End Class
