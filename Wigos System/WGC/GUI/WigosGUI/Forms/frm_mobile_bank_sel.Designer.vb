<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_mobile_bank_sel
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_mobile_bank_sel))
    Me.gb_mobile_bank = New System.Windows.Forms.GroupBox()
    Me.rb_mb_one = New System.Windows.Forms.RadioButton()
    Me.rb_mb_all = New System.Windows.Forms.RadioButton()
    Me.cmb_mb = New GUI_Controls.uc_combo()
    Me.gb_blocked = New System.Windows.Forms.GroupBox()
    Me.chk_blocked_no = New System.Windows.Forms.CheckBox()
    Me.chk_blocked_yes = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_mobile_bank.SuspendLayout()
    Me.gb_blocked.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_blocked)
    Me.panel_filter.Controls.Add(Me.gb_mobile_bank)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(974, 83)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_mobile_bank, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_blocked, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 87)
    Me.panel_data.Size = New System.Drawing.Size(974, 431)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(968, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(968, 4)
    '
    'gb_mobile_bank
    '
    Me.gb_mobile_bank.Controls.Add(Me.rb_mb_one)
    Me.gb_mobile_bank.Controls.Add(Me.rb_mb_all)
    Me.gb_mobile_bank.Controls.Add(Me.cmb_mb)
    Me.gb_mobile_bank.Location = New System.Drawing.Point(4, 3)
    Me.gb_mobile_bank.Name = "gb_mobile_bank"
    Me.gb_mobile_bank.Size = New System.Drawing.Size(272, 71)
    Me.gb_mobile_bank.TabIndex = 0
    Me.gb_mobile_bank.TabStop = False
    Me.gb_mobile_bank.Text = "xMobile Bank"
    '
    'rb_mb_one
    '
    Me.rb_mb_one.Location = New System.Drawing.Point(8, 14)
    Me.rb_mb_one.Name = "rb_mb_one"
    Me.rb_mb_one.Size = New System.Drawing.Size(72, 24)
    Me.rb_mb_one.TabIndex = 0
    Me.rb_mb_one.Text = "xOne"
    '
    'rb_mb_all
    '
    Me.rb_mb_all.Checked = True
    Me.rb_mb_all.Location = New System.Drawing.Point(8, 40)
    Me.rb_mb_all.Name = "rb_mb_all"
    Me.rb_mb_all.Size = New System.Drawing.Size(64, 24)
    Me.rb_mb_all.TabIndex = 1
    Me.rb_mb_all.TabStop = True
    Me.rb_mb_all.Text = "xAll"
    '
    'cmb_mb
    '
    Me.cmb_mb.AllowUnlistedValues = False
    Me.cmb_mb.AutoCompleteMode = False
    Me.cmb_mb.IsReadOnly = False
    Me.cmb_mb.Location = New System.Drawing.Point(80, 14)
    Me.cmb_mb.Name = "cmb_mb"
    Me.cmb_mb.SelectedIndex = -1
    Me.cmb_mb.Size = New System.Drawing.Size(184, 24)
    Me.cmb_mb.SufixText = "Sufix Text"
    Me.cmb_mb.SufixTextVisible = True
    Me.cmb_mb.TabIndex = 2
    Me.cmb_mb.TextCombo = Nothing
    Me.cmb_mb.TextVisible = False
    Me.cmb_mb.TextWidth = 0
    '
    'gb_blocked
    '
    Me.gb_blocked.Controls.Add(Me.chk_blocked_no)
    Me.gb_blocked.Controls.Add(Me.chk_blocked_yes)
    Me.gb_blocked.Location = New System.Drawing.Point(282, 3)
    Me.gb_blocked.Name = "gb_blocked"
    Me.gb_blocked.Size = New System.Drawing.Size(92, 71)
    Me.gb_blocked.TabIndex = 1
    Me.gb_blocked.TabStop = False
    Me.gb_blocked.Text = "xEnabled"
    '
    'chk_blocked_no
    '
    Me.chk_blocked_no.AutoSize = True
    Me.chk_blocked_no.Location = New System.Drawing.Point(16, 42)
    Me.chk_blocked_no.Name = "chk_blocked_no"
    Me.chk_blocked_no.Size = New System.Drawing.Size(48, 17)
    Me.chk_blocked_no.TabIndex = 1
    Me.chk_blocked_no.Text = "xNo"
    Me.chk_blocked_no.UseVisualStyleBackColor = True
    '
    'chk_blocked_yes
    '
    Me.chk_blocked_yes.AutoSize = True
    Me.chk_blocked_yes.Location = New System.Drawing.Point(16, 19)
    Me.chk_blocked_yes.Name = "chk_blocked_yes"
    Me.chk_blocked_yes.Size = New System.Drawing.Size(53, 17)
    Me.chk_blocked_yes.TabIndex = 0
    Me.chk_blocked_yes.Text = "xYes"
    Me.chk_blocked_yes.UseVisualStyleBackColor = True
    '
    'frm_mobile_bank_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(984, 522)
    Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
    Me.Name = "frm_mobile_bank_sel"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_mobile_bank_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_mobile_bank.ResumeLayout(False)
    Me.gb_blocked.ResumeLayout(False)
    Me.gb_blocked.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_mobile_bank As System.Windows.Forms.GroupBox
  Friend WithEvents rb_mb_one As System.Windows.Forms.RadioButton
  Friend WithEvents rb_mb_all As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_mb As GUI_Controls.uc_combo
  Friend WithEvents gb_blocked As System.Windows.Forms.GroupBox
  Friend WithEvents chk_blocked_no As System.Windows.Forms.CheckBox
  Friend WithEvents chk_blocked_yes As System.Windows.Forms.CheckBox
End Class
