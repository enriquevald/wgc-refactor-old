'-------------------------------------------------------------------
' Copyright � 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
'   MODULE NAME : frm_cage_safe_keeping_movement_sel
'   DESCRIPTION : Selection safe keeping movements
'        AUTHOR : Samuel Gonz�lez
' CREATION DATE : 14-APR-2015
'
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 14-APR-2015  SGB    Initial version. Backlog Item 687
' 22-APR-2015  SGB    Changes sprint. Backlog Item 1279 Task 1284
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data

Public Class frm_cage_safe_keeping_movement_sel
  Inherits frm_base_sel

#Region " Constants "
  Private Const GRID_FIXED_COLUMNS As Integer = 8

  Private Const GRID_HEADER_ROWS As Integer = 2

  'COLUMN
  'select
  Private Const GRID_COLUMN_SELECT As Integer = 0
  'Safe keeping
  Private Const GRID_COLUMN_SAFE_KEEPING_ID As Integer = 1
  Private Const GRID_COLUMN_SAFE_KEEPING_NAME As Integer = 2
  'Operation
  Private Const GRID_COLUMN_OPERATION_DATE As Integer = 3
  Private Const GRID_COLUMN_OPERATION_AMOUNT As Integer = 4
  Private Const GRID_COLUMN_OPERATION_FINAL_AMOUNT As Integer = 5
  'Cage
  Private Const GRID_COLUMN_CAGE_USER As Integer = 6
  Private Const GRID_COLUMN_CAGE_SESION As Integer = 7

  'WIDTH
  Private Const GRID_COLUMN_WIDTH_SELECT As Integer = 200
  'Safe keeping
  Private Const GRID_WIDTH_SAFE_KEEPING_ID As Integer = 600
  Private Const GRID_WIDTH_SAFE_KEEPING_NAME As Integer = 4000
  'Operation
  Private Const GRID_WIDTH_OPERATION_DATE As Integer = 2000
  Private Const GRID_WIDTH_OPERATION_AMOUNT As Integer = 1500
  Private Const GRID_WIDTH_OPERATION_FINAL_AMOUNT As Integer = 1500
  'Cage
  Private Const GRID_WIDTH_CAGE_USER As Integer = 2000
  Private Const GRID_WIDTH_CAGE_SESION As Integer = 2000

  'SQL
  'Safe keeping
  Private Const SQL_COLUMN_SAFE_KEEPING_ID As Integer = 0
  Private Const SQL_COLUMN_SAFE_KEEPING_NAME As Integer = 1
  'Operation
  Private Const SQL_COLUMN_OPERATION_DATE As Integer = 2
  Private Const SQL_COLUMN_OPERATION_TYPE As Integer = 3
  Private Const SQL_COLUMN_OPERATION_AMOUNT As Integer = 4
  Private Const SQL_COLUMN_OPERATION_FINAL_AMOUNT As Integer = 5
  'Cage
  Private Const SQL_COLUMN_CAGE_USER As Integer = 6
  Private Const SQL_COLUMN_CAGE_SESION As Integer = 7

#End Region 'Constants

#Region " Members "

  'row subtotal
  Private m_last_user_id As Integer
  Private m_subtotal_amount As Double
  Private m_subtotal_final_amount As Double = Nothing

  ' Report filters 
  Private m_id As String
  Private m_name_account As String
  Private m_document As String
  Private m_date_type As String
  Private m_date_from As String
  Private m_date_to As String

#End Region 'Members

#Region " OVERRIDES "

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CAGE_SAFE_KEEPING_MOVEMENT_SEL

    'Call Base Form proc
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6162)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(2)

    ' Date operations
    Me.gb_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6199)
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    Me.gb_count.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6200)
    Me.ef_id.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6164)
    Me.ef_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 20, 0)

    Me.ef_name_account.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6172)
    Me.ef_name_account.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 200)

    Me.ef_document.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6217)
    Me.ef_document.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 200)

    Me.opt_cage_session.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6170)
    Me.opt_operation.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6174)

    Call GUI_StyleSheet()

    Call SetDefaultValues()

  End Sub

  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.ef_id

  End Sub ' GUI_SetInitialFocus

  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()

  End Sub ' GUI_FilterReset

  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Last operation
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    Return True

  End Function ' GUI_FilterCheck

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _sb As System.Text.StringBuilder

    _sb = New System.Text.StringBuilder()

    _sb.AppendLine("     SELECT  SKA.SKA_SAFE_KEEPING_ID                          ")
    _sb.AppendLine("           , SKA.SKA_OWNER_NAME                               ")
    _sb.AppendLine("           , SKO.SKO_DATETIME                                 ")
    _sb.AppendLine("           , SKO.SKO_TYPE                                     ")
    _sb.AppendLine("           , SKO.SKO_AMOUNT                                   ")
    _sb.AppendLine("           , SKO.SKO_FINAL_BALANCE                            ")
    _sb.AppendLine("           , SKO.SKO_USER_ID                                  ")
    _sb.AppendLine("           , CS.CGS_SESSION_NAME                              ")
    _sb.AppendLine("       FROM  SAFE_KEEPING_OPERATIONS SKO                      ")
    _sb.AppendLine(" INNER JOIN  SAFE_KEEPING_ACCOUNTS SKA  ON  SKO.SKO_SAFE_KEEPING_ID = SKA.SKA_SAFE_KEEPING_ID")
    _sb.AppendLine(" INNER JOIN  CAGE_SESSIONS CS           ON  SKO.SKO_CAGE_SESION = CS.CGS_CAGE_SESSION_ID     ")

    _sb.AppendLine(GetSqlWhere())

    _sb.AppendLine("   ORDER BY  SKA.SKA_SAFE_KEEPING_ID, SKO.SKO_DATETIME DESC  ")

    Return _sb.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _value As Double

    If RowIndex = 0 Then
      m_subtotal_final_amount = DbRow.Value(SQL_COLUMN_OPERATION_FINAL_AMOUNT)
    End If

    If m_last_user_id <> DbRow.Value(SQL_COLUMN_SAFE_KEEPING_ID) AndAlso m_last_user_id <> -1 Then
      AddSubtotalRow(RowIndex)
      Me.Grid.AddRow()
      RowIndex += 1
      m_subtotal_final_amount = DbRow.Value(SQL_COLUMN_OPERATION_FINAL_AMOUNT)
    End If

    m_last_user_id = DbRow.Value(SQL_COLUMN_SAFE_KEEPING_ID)

    ' SAFE KEEPING
    ' id
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SAFE_KEEPING_ID).Value = DbRow.Value(SQL_COLUMN_SAFE_KEEPING_ID)
    ' Name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SAFE_KEEPING_NAME).Value = DbRow.Value(SQL_COLUMN_SAFE_KEEPING_NAME)

    ' OPERATION
    ' Date
    If Not DbRow.IsNull(GRID_COLUMN_OPERATION_DATE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_OPERATION_DATE), _
                                                                               ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                               ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If
    ' Amount
    If DbRow.Value(SQL_COLUMN_OPERATION_TYPE) = WSI.Common.OperationCode.SAFE_KEEPING_DEPOSIT Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_AMOUNT).Value = "+" & GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_OPERATION_AMOUNT), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_AMOUNT).ForeColor = Color.Blue
      _value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_OPERATION_AMOUNT)), 0, DbRow.Value(SQL_COLUMN_OPERATION_AMOUNT))
      m_subtotal_amount += _value
    ElseIf DbRow.Value(SQL_COLUMN_OPERATION_TYPE) = WSI.Common.OperationCode.SAFE_KEEPING_WITHDRAW Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_AMOUNT).Value = "-" & GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_OPERATION_AMOUNT), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_AMOUNT).ForeColor = Color.Red
      _value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_OPERATION_AMOUNT)), 0, DbRow.Value(SQL_COLUMN_OPERATION_AMOUNT))
      m_subtotal_amount -= _value
    End If

    ' Final Amount
    Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_FINAL_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_OPERATION_FINAL_AMOUNT), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' CAGE
    ' User
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CAGE_USER).Value = GetUserFullName(DbRow.Value(SQL_COLUMN_CAGE_USER))
    ' Sesion
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CAGE_SESION).Value = DbRow.Value(SQL_COLUMN_CAGE_SESION)

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE : Actions to doing after insert the last row
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Protected Overrides Sub GUI_AfterLastRow()
    Dim _row_index As Integer

    _row_index = Me.Grid.NumRows

    If _row_index > 0 Then
      Me.Grid.AddRow()
      AddSubtotalRow(_row_index)

      _row_index = _row_index + 1
    End If
  End Sub ' GUI_AfterLastRow

  ' PURPOSE : Actions to doing before insert the first row
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Protected Overrides Sub GUI_BeforeFirstRow()
    Call InitSubtotalValues()
  End Sub ' GUI_BeforeFirstRow

#Region " GUI Reports "

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6164), m_id)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6172), m_name_account)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6217), m_document)
    PrintData.SetFilter(m_date_type & " " & LCase(GLB_NLS_GUI_INVOICING.GetString(202)), m_date_from)
    PrintData.SetFilter(m_date_type & " " & LCase(GLB_NLS_GUI_INVOICING.GetString(203)), m_date_to)

  End Sub ' GUI_ReportFilter

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_id = String.Empty
    m_name_account = String.Empty
    m_document = String.Empty
    m_date_from = String.Empty
    m_date_to = String.Empty

    ' ID
    m_id = Me.ef_id.Value

    If String.IsNullOrEmpty(m_id) Then
      ' Name
      m_name_account = Me.ef_name_account.Value
      m_document = Me.ef_document.Value

    End If

    ' Option date
    If Me.opt_operation.Checked Then
      m_date_type = Me.opt_operation.Text
    ElseIf Me.opt_cage_session.Checked Then
      m_date_type = Me.opt_cage_session.Text

    End If

    ' Dates
    If Me.dtp_from.Checked Then
      If Me.opt_operation.Checked Then
        m_date_from = GUI_FormatDate(dtp_from.Value, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
      Else
        m_date_from = GUI_FormatDate(dtp_from.Value, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
      End If
    End If
    If Me.dtp_to.Checked Then
      If Me.opt_operation.Checked Then
        m_date_to = GUI_FormatDate(dtp_to.Value, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
      Else
        m_date_to = GUI_FormatDate(dtp_to.Value, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
      End If
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region 'OVERRIDES

#Region " Public Functions "

  ' PURPOSE : Opens dialog with default settings for edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit
#End Region 'Public Functions

#Region " Private Functions "
  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_FIXED_COLUMNS, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Blank "select" column at the left of the grid
      .Column(GRID_COLUMN_SELECT).Header(0).Text = ""
      .Column(GRID_COLUMN_SELECT).Header(1).Text = ""
      .Column(GRID_COLUMN_SELECT).Width = GRID_COLUMN_WIDTH_SELECT
      .Column(GRID_COLUMN_SELECT).HighLightWhenSelected = False
      .Column(GRID_COLUMN_SELECT).IsColumnPrintable = False

      ' SAFE KEEPING
      .Column(GRID_COLUMN_SAFE_KEEPING_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6163)
      ' Identifier
      .Column(GRID_COLUMN_SAFE_KEEPING_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6173)
      .Column(GRID_COLUMN_SAFE_KEEPING_ID).Width = GRID_WIDTH_SAFE_KEEPING_ID
      .Column(GRID_COLUMN_SAFE_KEEPING_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Safe keeping
      .Column(GRID_COLUMN_SAFE_KEEPING_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6163)
      ' Name
      .Column(GRID_COLUMN_SAFE_KEEPING_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6165)
      .Column(GRID_COLUMN_SAFE_KEEPING_NAME).Width = GRID_WIDTH_SAFE_KEEPING_NAME
      .Column(GRID_COLUMN_SAFE_KEEPING_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' OPERATION
      .Column(GRID_COLUMN_OPERATION_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6166)
      ' Date
      .Column(GRID_COLUMN_OPERATION_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6167)
      .Column(GRID_COLUMN_OPERATION_DATE).Width = GRID_WIDTH_OPERATION_DATE
      .Column(GRID_COLUMN_OPERATION_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Operation
      .Column(GRID_COLUMN_OPERATION_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6166)
      ' Amount
      .Column(GRID_COLUMN_OPERATION_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6168)
      .Column(GRID_COLUMN_OPERATION_AMOUNT).Width = GRID_WIDTH_OPERATION_AMOUNT
      .Column(GRID_COLUMN_OPERATION_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Operation
      .Column(GRID_COLUMN_OPERATION_FINAL_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6166)
      ' Final Amount
      .Column(GRID_COLUMN_OPERATION_FINAL_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6169)
      .Column(GRID_COLUMN_OPERATION_FINAL_AMOUNT).Width = GRID_WIDTH_OPERATION_FINAL_AMOUNT
      .Column(GRID_COLUMN_OPERATION_FINAL_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'Operation
      .Column(GRID_COLUMN_CAGE_USER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6166)
      ' User
      .Column(GRID_COLUMN_CAGE_USER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6171)
      .Column(GRID_COLUMN_CAGE_USER).Width = GRID_WIDTH_CAGE_USER
      .Column(GRID_COLUMN_CAGE_USER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' CAGE
      .Column(GRID_COLUMN_CAGE_SESION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3388)
      ' User
      .Column(GRID_COLUMN_CAGE_SESION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6172)
      .Column(GRID_COLUMN_CAGE_SESION).Width = GRID_WIDTH_CAGE_SESION
      .Column(GRID_COLUMN_CAGE_SESION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE : Set default values to filters
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub SetDefaultValues()
    Dim initial_time As Date

    ' ID
    Me.ef_id.Value = String.Empty

    ' Name
    Me.ef_name_account.Value = String.Empty

    ' Document
    Me.ef_document.Value = String.Empty

    ' Option date
    Me.opt_operation.Checked = True

    ' Date
    initial_time = WSI.Common.Misc.TodayOpening()
    Me.dtp_from.Value = initial_time
    Me.dtp_from.Checked = False
    Me.dtp_to.Value = Me.dtp_from.Value.AddDays(1)
    Me.dtp_to.Checked = False

    Call EnableFilters(True)

  End Sub ' SetDefaultValues

  Private Sub InitSubtotalValues()
    m_subtotal_amount = 0

    m_last_user_id = -1
  End Sub

  ' PURPOSE : Get the SQL WHERE
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Function GetSqlWhere() As String

    Dim _str_where As String
    Dim _str_aux1 As String
    Dim _some_filter As Boolean

    _str_where = String.Empty
    _some_filter = False

    If Me.ef_id.Value <> String.Empty Then
      ' ID
      _str_where += " WHERE  SKA.SKA_SAFE_KEEPING_ID = " & Me.ef_id.Value
      _some_filter = True
    Else
      ' Name
      If Me.ef_name_account.Value <> String.Empty Then
        _str_aux1 = UCase(Me.ef_name_account.Value).Replace("'", "''")
        _str_where += " WHERE  (UPPER(SKA.SKA_OWNER_NAME) LIKE '" & _str_aux1 & "%'  OR"
        _str_where += "         UPPER(SKA.SKA_OWNER_NAME) LIKE '% " & _str_aux1 & "%') "
        _some_filter = True
      End If
    End If

    ' Document
    If Me.ef_document.Value <> String.Empty Then
      _str_where += IIf(_some_filter, " AND ", " WHERE ")
      _str_aux1 = UCase(Me.ef_document.Value).Replace("'", "''")
      _str_where += " ( UPPER(SKA.SKA_OWNER_DOCUMENT_ID) LIKE '%" & _str_aux1 & "%') "
      _some_filter = True
    End If

    ' Date operation
    If Me.opt_operation.Checked Then
      If Me.dtp_from.Checked Then
        _str_where += IIf(_some_filter, " AND ", " WHERE ")
        _str_where += " (SKO.SKO_DATETIME >= " & GUI_FormatDateDB(dtp_from.Value) & ") "
        _some_filter = True
      End If

      If Me.dtp_to.Checked Then
        _str_where += IIf(_some_filter, " AND ", " WHERE ")
        _str_where += " (SKO.SKO_DATETIME < " & GUI_FormatDateDB(dtp_to.Value) & ") "
        _some_filter = True
      End If
    End If

    ' Session date operation
    If Me.opt_cage_session.Checked Then
      If Me.dtp_from.Checked Then
        _str_where += IIf(_some_filter, " AND ", " WHERE ")
        _str_where += " (CS.CGS_WORKING_DAY >= " & GUI_FormatDayDB(dtp_from.Value) & ") "
        _some_filter = True
      End If

      If Me.dtp_to.Checked Then
        _str_where += IIf(_some_filter, " AND ", " WHERE ")
        _str_where += " (CS.CGS_WORKING_DAY < " & GUI_FormatDayDB(dtp_to.Value) & ") "
        _some_filter = True
      End If
    End If

    Return _str_where

  End Function

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - RowIndex: Index of grid
  '           - TypeSubtotal: The type of subtotal, we says that subtotal is, according with order.
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub AddSubtotalRow(ByVal RowIndex As Integer)

    Me.Grid.Cell(RowIndex, GRID_COLUMN_SAFE_KEEPING_ID).Value = Me.Grid.Cell(RowIndex - 1, GRID_COLUMN_SAFE_KEEPING_ID).Value()
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SAFE_KEEPING_NAME).Value = Me.Grid.Cell(RowIndex - 1, GRID_COLUMN_SAFE_KEEPING_NAME).Value()
    Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_DATE).Value = GLB_NLS_GUI_INVOICING.GetString(375)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_AMOUNT).Value = GUI_FormatCurrency(m_subtotal_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_FINAL_AMOUNT).Value = GUI_FormatCurrency(m_subtotal_final_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    CalculateSubTotals(RowIndex)

    Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

    Call InitSubtotalValues()
  End Sub

  ' PURPOSE: Calculate Totals of grid 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub CalculateSubTotals(ByVal RowIndex As Integer)
    m_subtotal_amount += m_subtotal_amount
    m_subtotal_final_amount = Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_FINAL_AMOUNT).Value
  End Sub

  ' PURPOSE : Enable/Disable the filters
  '
  '  PARAMS :
  '     - INPUT  : Boolean indicating if it must enable (true) or disable (false) the filters
  '
  '     - OUTPUT :
  Private Sub EnableFilters(ByVal EnableFilters As Boolean)

    Me.ef_name_account.Enabled = EnableFilters
    Me.ef_document.Enabled = EnableFilters

  End Sub ' EnableFilters

  '----------------------------------------------------------------------------
  ' PURPOSE: Gets gui_user fullname
  '
  ' PARAMS:
  '       - user ID
  '
  ' RETURNS:
  '     - userfullname
  '
  ' NOTES:
  Public Function GetUserFullName(ByVal UserId As String) As String
    Dim _sb As System.Text.StringBuilder
    Dim _obj As Object
    Dim _user_name As String

    _user_name = AUDIT_NONE_STRING

    If UserId <> String.Empty Then

      _sb = New System.Text.StringBuilder

      _sb.AppendLine(" SELECT   GU_FULL_NAME ")
      _sb.AppendLine("   FROM   GUI_USERS ")
      _sb.AppendLine("  WHERE   GU_USER_ID = @pUserId ")

      Try

        Using _db_trx As New WSI.Common.DB_TRX()
          Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

            _cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UserId

            _obj = _cmd.ExecuteScalar()

            If _obj IsNot Nothing AndAlso _obj IsNot DBNull.Value Then
              _user_name = _obj
            End If

          End Using
        End Using

      Catch ex As Exception
        Call Trace.WriteLine(ex.ToString())
        Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                              "cls_cage_control", _
                              "GetUserFullName", _
                              ex.Message)

        Return AUDIT_NONE_STRING
      End Try

    End If

    Return _user_name
  End Function ' GetUserFullName

#End Region 'Private Functions

#Region " Events "
  Private Sub ef_id_EntryFieldValueChanged() Handles ef_id.EntryFieldValueChanged
    EnableFilters(String.IsNullOrEmpty(Me.ef_id.Value))
  End Sub

  Private Sub opt_cage_session_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_cage_session.CheckedChanged
    If opt_cage_session.Checked Then
      Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
      Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
      Me.dtp_from.Width = 230
      Me.dtp_to.Width = 230
    Else
      Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
      Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
      Me.dtp_from.Width = 230
      Me.dtp_to.Width = 230
    End If
  End Sub
#End Region 'Events
End Class