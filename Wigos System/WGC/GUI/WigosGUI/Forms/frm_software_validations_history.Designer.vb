<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_software_validations_history
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.gb_status = New System.Windows.Forms.GroupBox
    Me.lbl_validation_error = New System.Windows.Forms.Label
    Me.chk_validation_error = New System.Windows.Forms.CheckBox
    Me.lbl_validation_ok = New System.Windows.Forms.Label
    Me.chk_validation_ok = New System.Windows.Forms.CheckBox
    Me.btn_uncheck_all = New GUI_Controls.uc_button
    Me.btn_check_all = New GUI_Controls.uc_button
    Me.lbl_unknown = New System.Windows.Forms.Label
    Me.lbl_error = New System.Windows.Forms.Label
    Me.lbl_caneled_or_timeout = New System.Windows.Forms.Label
    Me.lbl_pending = New System.Windows.Forms.Label
    Me.chk_error = New System.Windows.Forms.CheckBox
    Me.chk_unknown = New System.Windows.Forms.CheckBox
    Me.chk_canceled_or_timeout = New System.Windows.Forms.CheckBox
    Me.chk_pending = New System.Windows.Forms.CheckBox
    Me.gb_dates = New System.Windows.Forms.GroupBox
    Me.dtp_created_to = New GUI_Controls.uc_date_picker
    Me.dtp_created_from = New GUI_Controls.uc_date_picker
    Me.uc_pr_list = New GUI_Controls.uc_provider
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_status.SuspendLayout()
    Me.gb_dates.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_status)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Controls.Add(Me.gb_dates)
    Me.panel_filter.Size = New System.Drawing.Size(1099, 195)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_dates, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_status, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 199)
    Me.panel_data.Size = New System.Drawing.Size(1099, 363)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1093, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1093, 4)
    '
    'gb_status
    '
    Me.gb_status.Controls.Add(Me.lbl_validation_error)
    Me.gb_status.Controls.Add(Me.chk_validation_error)
    Me.gb_status.Controls.Add(Me.lbl_validation_ok)
    Me.gb_status.Controls.Add(Me.chk_validation_ok)
    Me.gb_status.Controls.Add(Me.btn_uncheck_all)
    Me.gb_status.Controls.Add(Me.btn_check_all)
    Me.gb_status.Controls.Add(Me.lbl_unknown)
    Me.gb_status.Controls.Add(Me.lbl_error)
    Me.gb_status.Controls.Add(Me.lbl_caneled_or_timeout)
    Me.gb_status.Controls.Add(Me.lbl_pending)
    Me.gb_status.Controls.Add(Me.chk_error)
    Me.gb_status.Controls.Add(Me.chk_unknown)
    Me.gb_status.Controls.Add(Me.chk_canceled_or_timeout)
    Me.gb_status.Controls.Add(Me.chk_pending)
    Me.gb_status.Location = New System.Drawing.Point(592, 6)
    Me.gb_status.Name = "gb_status"
    Me.gb_status.Size = New System.Drawing.Size(196, 189)
    Me.gb_status.TabIndex = 2
    Me.gb_status.TabStop = False
    Me.gb_status.Text = "xStatus"
    '
    'lbl_validation_error
    '
    Me.lbl_validation_error.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_validation_error.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_validation_error.Location = New System.Drawing.Point(6, 60)
    Me.lbl_validation_error.Name = "lbl_validation_error"
    Me.lbl_validation_error.Size = New System.Drawing.Size(16, 16)
    Me.lbl_validation_error.TabIndex = 16
    '
    'chk_validation_error
    '
    Me.chk_validation_error.AutoSize = True
    Me.chk_validation_error.Location = New System.Drawing.Point(28, 60)
    Me.chk_validation_error.Name = "chk_validation_error"
    Me.chk_validation_error.Size = New System.Drawing.Size(118, 17)
    Me.chk_validation_error.TabIndex = 17
    Me.chk_validation_error.Text = "xValidationError"
    Me.chk_validation_error.UseVisualStyleBackColor = True
    '
    'lbl_validation_ok
    '
    Me.lbl_validation_ok.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_validation_ok.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_validation_ok.Location = New System.Drawing.Point(6, 39)
    Me.lbl_validation_ok.Name = "lbl_validation_ok"
    Me.lbl_validation_ok.Size = New System.Drawing.Size(16, 16)
    Me.lbl_validation_ok.TabIndex = 14
    '
    'chk_validation_ok
    '
    Me.chk_validation_ok.AutoSize = True
    Me.chk_validation_ok.Location = New System.Drawing.Point(28, 39)
    Me.chk_validation_ok.Name = "chk_validation_ok"
    Me.chk_validation_ok.Size = New System.Drawing.Size(106, 17)
    Me.chk_validation_ok.TabIndex = 15
    Me.chk_validation_ok.Text = "xValidationOK"
    Me.chk_validation_ok.UseVisualStyleBackColor = True
    '
    'btn_uncheck_all
    '
    Me.btn_uncheck_all.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_uncheck_all.Location = New System.Drawing.Point(102, 152)
    Me.btn_uncheck_all.Name = "btn_uncheck_all"
    Me.btn_uncheck_all.Size = New System.Drawing.Size(90, 30)
    Me.btn_uncheck_all.TabIndex = 13
    Me.btn_uncheck_all.ToolTipped = False
    Me.btn_uncheck_all.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.USER
    '
    'btn_check_all
    '
    Me.btn_check_all.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_check_all.Location = New System.Drawing.Point(4, 152)
    Me.btn_check_all.Name = "btn_check_all"
    Me.btn_check_all.Size = New System.Drawing.Size(90, 30)
    Me.btn_check_all.TabIndex = 12
    Me.btn_check_all.ToolTipped = False
    Me.btn_check_all.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.USER
    '
    'lbl_unknown
    '
    Me.lbl_unknown.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_unknown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_unknown.Location = New System.Drawing.Point(6, 104)
    Me.lbl_unknown.Name = "lbl_unknown"
    Me.lbl_unknown.Size = New System.Drawing.Size(16, 16)
    Me.lbl_unknown.TabIndex = 5
    '
    'lbl_error
    '
    Me.lbl_error.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_error.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_error.Location = New System.Drawing.Point(6, 82)
    Me.lbl_error.Name = "lbl_error"
    Me.lbl_error.Size = New System.Drawing.Size(16, 16)
    Me.lbl_error.TabIndex = 4
    '
    'lbl_caneled_or_timeout
    '
    Me.lbl_caneled_or_timeout.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_caneled_or_timeout.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_caneled_or_timeout.Location = New System.Drawing.Point(6, 126)
    Me.lbl_caneled_or_timeout.Name = "lbl_caneled_or_timeout"
    Me.lbl_caneled_or_timeout.Size = New System.Drawing.Size(16, 16)
    Me.lbl_caneled_or_timeout.TabIndex = 3
    '
    'lbl_pending
    '
    Me.lbl_pending.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_pending.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_pending.Location = New System.Drawing.Point(6, 18)
    Me.lbl_pending.Name = "lbl_pending"
    Me.lbl_pending.Size = New System.Drawing.Size(16, 16)
    Me.lbl_pending.TabIndex = 0
    '
    'chk_error
    '
    Me.chk_error.AutoSize = True
    Me.chk_error.Location = New System.Drawing.Point(28, 82)
    Me.chk_error.Name = "chk_error"
    Me.chk_error.Size = New System.Drawing.Size(62, 17)
    Me.chk_error.TabIndex = 10
    Me.chk_error.Text = "xError"
    Me.chk_error.UseVisualStyleBackColor = True
    '
    'chk_unknown
    '
    Me.chk_unknown.AutoSize = True
    Me.chk_unknown.Location = New System.Drawing.Point(28, 104)
    Me.chk_unknown.Name = "chk_unknown"
    Me.chk_unknown.Size = New System.Drawing.Size(85, 17)
    Me.chk_unknown.TabIndex = 11
    Me.chk_unknown.Text = "xUnknown"
    Me.chk_unknown.UseVisualStyleBackColor = True
    '
    'chk_canceled_or_timeout
    '
    Me.chk_canceled_or_timeout.AutoSize = True
    Me.chk_canceled_or_timeout.Location = New System.Drawing.Point(28, 126)
    Me.chk_canceled_or_timeout.Name = "chk_canceled_or_timeout"
    Me.chk_canceled_or_timeout.Size = New System.Drawing.Size(79, 17)
    Me.chk_canceled_or_timeout.TabIndex = 9
    Me.chk_canceled_or_timeout.Text = "xTimeout"
    Me.chk_canceled_or_timeout.UseVisualStyleBackColor = True
    '
    'chk_pending
    '
    Me.chk_pending.AutoSize = True
    Me.chk_pending.Location = New System.Drawing.Point(28, 18)
    Me.chk_pending.Name = "chk_pending"
    Me.chk_pending.Size = New System.Drawing.Size(78, 17)
    Me.chk_pending.TabIndex = 6
    Me.chk_pending.Text = "xPending"
    Me.chk_pending.UseVisualStyleBackColor = True
    '
    'gb_dates
    '
    Me.gb_dates.Controls.Add(Me.dtp_created_to)
    Me.gb_dates.Controls.Add(Me.dtp_created_from)
    Me.gb_dates.Location = New System.Drawing.Point(6, 6)
    Me.gb_dates.Name = "gb_dates"
    Me.gb_dates.Size = New System.Drawing.Size(244, 72)
    Me.gb_dates.TabIndex = 0
    Me.gb_dates.TabStop = False
    Me.gb_dates.Text = "xCreated"
    '
    'dtp_created_to
    '
    Me.dtp_created_to.Checked = True
    Me.dtp_created_to.IsReadOnly = False
    Me.dtp_created_to.Location = New System.Drawing.Point(6, 38)
    Me.dtp_created_to.Name = "dtp_created_to"
    Me.dtp_created_to.ShowCheckBox = True
    Me.dtp_created_to.ShowUpDown = False
    Me.dtp_created_to.Size = New System.Drawing.Size(230, 24)
    Me.dtp_created_to.SufixText = "Sufix Text"
    Me.dtp_created_to.SufixTextVisible = True
    Me.dtp_created_to.TabIndex = 1
    Me.dtp_created_to.TextWidth = 50
    Me.dtp_created_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_created_from
    '
    Me.dtp_created_from.Checked = True
    Me.dtp_created_from.IsReadOnly = False
    Me.dtp_created_from.Location = New System.Drawing.Point(6, 14)
    Me.dtp_created_from.Name = "dtp_created_from"
    Me.dtp_created_from.ShowCheckBox = True
    Me.dtp_created_from.ShowUpDown = False
    Me.dtp_created_from.Size = New System.Drawing.Size(230, 24)
    Me.dtp_created_from.SufixText = "Sufix Text"
    Me.dtp_created_from.SufixTextVisible = True
    Me.dtp_created_from.TabIndex = 0
    Me.dtp_created_from.TextWidth = 50
    Me.dtp_created_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'uc_pr_list
    '
    Me.uc_pr_list.FilterByCurrency = False
    Me.uc_pr_list.Location = New System.Drawing.Point(255, 3)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(337, 191)
    Me.uc_pr_list.TabIndex = 1
    '
    'frm_software_validations_history
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1107, 566)
    Me.Name = "frm_software_validations_history"
    Me.Text = "frm_software_validations_history"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_status.ResumeLayout(False)
    Me.gb_status.PerformLayout()
    Me.gb_dates.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents gb_dates As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_created_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_created_from As GUI_Controls.uc_date_picker
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents btn_uncheck_all As GUI_Controls.uc_button
  Friend WithEvents btn_check_all As GUI_Controls.uc_button
  Friend WithEvents lbl_unknown As System.Windows.Forms.Label
  Friend WithEvents lbl_error As System.Windows.Forms.Label
  Friend WithEvents lbl_caneled_or_timeout As System.Windows.Forms.Label
  Friend WithEvents lbl_pending As System.Windows.Forms.Label
  Friend WithEvents chk_error As System.Windows.Forms.CheckBox
  Friend WithEvents chk_unknown As System.Windows.Forms.CheckBox
  Friend WithEvents chk_canceled_or_timeout As System.Windows.Forms.CheckBox
  Friend WithEvents chk_pending As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_validation_error As System.Windows.Forms.Label
  Friend WithEvents chk_validation_error As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_validation_ok As System.Windows.Forms.Label
  Friend WithEvents chk_validation_ok As System.Windows.Forms.CheckBox
End Class
