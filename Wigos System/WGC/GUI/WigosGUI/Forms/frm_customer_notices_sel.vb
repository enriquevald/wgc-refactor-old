'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_customer_notices_sel
' DESCRIPTION:   Customer notices selection screen
' AUTHOR:        Xavi Guirado
' CREATION DATE: 05-SEP-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 05-SEP-2017  XGJ    Initial version
' 20-SEP-2017  JBM    WIGOS-4874 Customer notices - GUI - Show original notice in GUI
' 03-OCT-2017  FJC    WIGOS-5570 Customer notices - Several bugs
' 28-MAR-2018  JGC    Bug 32075:[WIGOS-9517]: Notices edition: Incorrect behaviour in Select button
' 28-MAR-2018  JGC    Bug 32091:[WIGOS-9531]: Customer notices - Status field has incorrect values when printing the screen
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Text
Imports WSI.Common

Public Class frm_customer_notices_sel
  Inherits frm_base_sel

#Region "Members"
  ' For report filters 
  Private m_filt_account As String
  Private m_filt_track_data As String
  Private m_filt_holder As String
  Private m_filt_holder_is_vip As String
  Private m_filt_date_from As String
  Private m_filt_date_to As String
  Private m_filt_enabled As Boolean
  Private m_filt_disabled As Boolean
  Private m_filt_current_only As Boolean
  Private m_filt_user As String

  Private m_customer_id As Long
  Private m_priorities As String

  Private m_selected_row As Integer

#End Region

#Region "Constants"

  ' Num grid columns / headers
  Private Const GRID_HEADER_ROWS As Integer = 2
  Private Const GRID_COLUMNS As Integer = 12

  ' DB Columns
  Private Const SQL_COLUMN_NOTICE_ID As Integer = 0
  Private Const SQL_COLUMN_NOTICE_CUSTOMER_ID As Integer = 1
  Private Const SQL_COLUMN_NOTICE_CUSTOMER_NAME As Integer = 2
  Private Const SQL_COLUMN_NOTICE_DATE_CREATION As Integer = 3
  Private Const SQL_COLUMN_NOTICE_USER_CREATION As Integer = 4
  Private Const SQL_COLUMN_NOTICE_USER_CREATION_NAME As Integer = 5
  Private Const SQL_COLUMN_NOTICE_STATUS As Integer = 6
  Private Const SQL_COLUMN_NOTICE_PRIORITY_ID As Integer = 7
  Private Const SQL_COLUMN_NOTICE_PRIORITY_NAME As Integer = 8
  Private Const SQL_COLUMN_NOTICE_TITLE_NAME As Integer = 9
  Private Const SQL_COLUMN_NOTICE_TEXT As Integer = 10

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0  'First column (special)
  Private Const GRID_COLUMN_NOTICE_ID As Integer = 1
  Private Const GRID_COLUMN_NOTICE_CUSTOMER_ID As Integer = 2
  Private Const GRID_COLUMN_NOTICE_CUSTOMER_NAME As Integer = 3
  Private Const GRID_COLUMN_NOTICE_DATE_CREATION As Integer = 4
  Private Const GRID_COLUMN_NOTICE_USER_CREATION_ID As Integer = 5
  Private Const GRID_COLUMN_NOTICE_USER_CREATION_NAME As Integer = 6
  Private Const GRID_COLUMN_NOTICE_STATUS As Integer = 7
  Private Const GRID_COLUMN_NOTICE_PRIORITY_ID As Integer = 8
  Private Const GRID_COLUMN_NOTICE_PRIORITY_NAME As Integer = 9
  Private Const GRID_COLUMN_NOTICE_TITLE_NAME As Integer = 10
  Private Const GRID_COLUMN_NOTICE_TEXT As Integer = 11

  ' Width
  Private Const GRID_WIDTH_INDEX As Integer = 200
  Private Const GRID_WIDTH_NOTICE_ID As Integer = 0
  Private Const GRID_WIDTH_NOTICE_CUSTOMER_ID As Integer = 1500
  Private Const GRID_WIDTH_NOTICE_CUSTOMER_NAME As Integer = 2500
  Private Const GRID_WIDTH_NOTICE_DATE_CREATION As Integer = 1800
  Private Const GRID_WIDTH_NOTICE_USER_CREATION_ID As Integer = 0
  Private Const GRID_WIDTH_NOTICE_USER_CREATION_NAME As Integer = 2000
  Private Const GRID_WIDTH_NOTICE_STATUS As Integer = 1700
  Private Const GRID_WIDTH_NOTICE_PRIORITY_ID As Integer = 0
  Private Const GRID_WIDTH_NOTICE_PRIORITY_NAME As Integer = 1550
  Private Const GRID_WIDTH_NOTICE_TITLE_NAME As Integer = 2500
  Private Const GRID_WIDTH_NOTICE_TEXT As Integer = 7000
#End Region

#Region "Overrrides"
  Dim chk_site As Object

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_FORM.FORM_CUSTOMER_NOTICES_SEL
    MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId 

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8609)

    ' Buttons
    GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)

    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False

    ' show history of notice button
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(466)

    Me.gb_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8715)       ' Date Creation
    Me.dtp_from.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(695)       ' From
    Me.dtp_to.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(696)         ' To

    Me.chk_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4941)   ' Enabled
    Me.chk_disabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4942)  ' Disabled

    Me.gb_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3346)    ' Status
    Me.chk_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8671)   ' Activo 
    Me.chk_disabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8672)  ' No Activo

    ' User
    Me.fra_user.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8629)
    Me.opt_user_one.Text = GLB_NLS_GUI_AUDITOR.GetString(262)
    Me.opt_user_all.Text = GLB_NLS_GUI_AUDITOR.GetString(263)
    Me.chk_show_all.Text = GLB_NLS_GUI_CONFIGURATION.GetString(90)

    ' Other basic functions....
    Call AddUserTableToDataset()
    Call Me.Uc_priority1.Init()

    SetDefaultValues()
    GUI_StyleSheet()

    If m_customer_id <> 0 Then
      Me.dtp_from.Focus()
    Else
      Uc_account_sel1.Focus()
    End If

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()

    Uc_account_sel1.Account = String.Empty
    Uc_account_sel1.Enabled = True

    SetDefaultValues()

  End Sub ' GUI_FilterReset

  Protected Overrides Sub GUI_RowSelectedEvent(ByVal SelectedRow As Integer)
    Dim _dt_history As DataTable
    Dim _str_sql As StringBuilder

    If Not Me.Permissions.Write Then

      Return
    End If

    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False

    If SelectedRow >= 0 And SelectedRow < Me.Grid.NumRows Then

      Try
        _str_sql = New StringBuilder()
        _str_sql.AppendLine("SELECT   CNH_NOTICE_ID             ")
        _str_sql.AppendLine("      ,  CNH_DATETIME              ")
        _str_sql.AppendLine("      ,  CNH_USER_ID               ")
        _str_sql.AppendLine("      ,  CNH_PRIORITY_ID           ")
        _str_sql.AppendLine("      ,  CNH_TEXT                  ")
        _str_sql.AppendLine("FROM     CUSTOMER_NOTICES_HISTORY  ")
        _str_sql.AppendLine("WHERE    CNH_NOTICE_ID = " & Me.Grid.Cell(SelectedRow, GRID_COLUMN_NOTICE_ID).Value)

        _dt_history = GUI_GetTableUsingSQL(_str_sql.ToString(), MAX_RECORDS_LOAD)

        If _dt_history.Rows.Count > 0 Then
          GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = True
          m_selected_row = Me.Grid.Cell(SelectedRow, GRID_COLUMN_NOTICE_ID).Value
        End If

      Catch ex As Exception
        Call Trace.WriteLine(ex.ToString)
      End Try

    End If

  End Sub ' GUI_RowSelectedEvent

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Me.dtp_to.Focus()

        Return False
      End If
    End If

    Return True

  End Function ' GUI_FilterCheck

  ' PURPOSE : Sets initial focus
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  '          
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.Uc_account_sel1
  End Sub ' GUI_SetInitialFocus

  ' PURPOSE : Build an SQL query from conditions set in the filters
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - SQL query text ready to send to the database
  '
  '   NOTES :
  '
  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _str_sql As StringBuilder

    _str_sql = New StringBuilder()
    _str_sql.AppendLine("     SELECT   CN_NOTICE_ID                                                                      ")
    _str_sql.AppendLine("            , CN_CUSTOMER_ID                                                                    ")
    _str_sql.AppendLine("            , AC_HOLDER_NAME                                                                    ")
    _str_sql.AppendLine("            , ISNULL(CN_LAST_UPDATE_DATE, CN_CREATION_DATE)                                     ")
    _str_sql.AppendLine("            , ISNULL(CN_LAST_UPDATE_USER_ID, CN_CREATION_USER_ID)                               ")
    _str_sql.AppendLine("            , GU_USERNAME                                                                       ")
    _str_sql.AppendLine("            , CN_STATUS                                                                         ")
    _str_sql.AppendLine("            , CN_PRIORITY_ID                                                                    ")
    _str_sql.AppendLine("            , CNP_PRIORITY_NAME                                                                 ")
    _str_sql.AppendLine("            , CN_HEADER                                                                         ")
    _str_sql.AppendLine("            , CN_TEXT                                                                           ")
    _str_sql.AppendLine("       FROM   CUSTOMER_NOTICES                                                                  ")
    _str_sql.AppendLine(" INNER JOIN   ACCOUNTS   ON  AC_ACCOUNT_ID = CN_CUSTOMER_ID                                     ")
    _str_sql.AppendLine(" INNER JOIN   GUI_USERS  ON  GU_USER_ID = ISNULL(CN_LAST_UPDATE_USER_ID, CN_CREATION_USER_ID)   ")
    _str_sql.AppendLine(" INNER JOIN   CUSTOMER_NOTICES_PRIORITY  ON  CNP_PRIORITY_ID = CN_PRIORITY_ID                   ")

    _str_sql.AppendLine(GetSqlWhere())

    Return _str_sql.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As CLASS_DB_ROW) As Boolean ' GUI_SetupRow

    Dim _msg_id As Int64
    Dim _customer_id As String
    Dim _date_creation As DateTime
    Dim _user_creation_id As Int64
    Dim _user_creation As String
    Dim _status As String
    Dim _priority_id As Int64
    Dim _priority As String
    Dim _notice_title_name As String
    Dim _notice_text As String
    Dim _customer_name As String

    ' Index
    Me.Grid.Cell(RowIndex, GRID_COLUMN_INDEX).Value = String.Empty

    ' Msg ID
    _msg_id = DbRow.Value(SQL_COLUMN_NOTICE_ID)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NOTICE_ID).Value = _msg_id

    ' Customer id
    _customer_id = DbRow.Value(SQL_COLUMN_NOTICE_CUSTOMER_ID)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NOTICE_CUSTOMER_ID).Value = _customer_id

    ' Customer name
    _customer_name = DbRow.Value(SQL_COLUMN_NOTICE_CUSTOMER_NAME)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NOTICE_CUSTOMER_NAME).Value = _customer_name

    ' Date creation
    _date_creation = DbRow.Value(SQL_COLUMN_NOTICE_DATE_CREATION)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NOTICE_DATE_CREATION).Value = GUI_FormatDate(_date_creation, _
                                                                                    ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                    ENUM_FORMAT_TIME.FORMAT_HHMM)
    ' User creation id
    _user_creation_id = DbRow.Value(SQL_COLUMN_NOTICE_USER_CREATION)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NOTICE_USER_CREATION_ID).Value = _user_creation_id

    ' User  creation name
    _user_creation = DbRow.Value(SQL_COLUMN_NOTICE_USER_CREATION_NAME)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NOTICE_USER_CREATION_NAME).Value = _user_creation

    ' Status
    _status = DbRow.Value(SQL_COLUMN_NOTICE_STATUS)
    If _status Then
      _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8671) ' Activo 
    Else
      _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8672) ' No activo
    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_NOTICE_STATUS).Value = _status

    ' Priority id
    _priority_id = DbRow.Value(SQL_COLUMN_NOTICE_PRIORITY_ID)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NOTICE_PRIORITY_ID).Value = _priority_id

    ' Priority name
    _priority = DbRow.Value(SQL_COLUMN_NOTICE_PRIORITY_NAME)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NOTICE_PRIORITY_NAME).Value = _priority

    ' Header
    _notice_title_name = DbRow.Value(SQL_COLUMN_NOTICE_TITLE_NAME)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NOTICE_TITLE_NAME).Value = _notice_title_name

    ' Text
    _notice_text = DbRow.Value(SQL_COLUMN_NOTICE_TEXT)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NOTICE_TEXT).Value = _notice_text.Replace(vbCrLf, " ")

    Return True
  End Function ' GUI_SetupRow

  ' PURPOSE: Activated when a row of the grid is double clicked or selected, so it can be edited
  '  PARAMS:
  '     - INPUT:
  '               
  '     - OUTPUT:
  '          
  ' RETURNS:
  '     - none
  Protected Overrides Sub GUI_EditSelectedItem()

    Dim _idx_row As Int32
    Dim _notices_id As Integer
    Dim _frm_edit As Object

    'Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    If _idx_row = Me.Grid.NumRows Then
      Return
    End If

    ' Get the Draw ID and Name, and launch the editor
    _notices_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_NOTICE_ID).Value

    _frm_edit = New frm_customer_notices_edit

    Call _frm_edit.ShowEditItem(_notices_id)

    _frm_edit = Nothing
    Call Me.Grid.Focus()

  End Sub 'GUI_EditSelectedItem

  ' PURPOSE: Process clicks on data grid (doible-clicks) and select button
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON) ' GUI_ButtonClick
    Dim _index_sel As Long

    _index_sel = -1

    Select Case ButtonId
      Case frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0 'hist button
        Dim frm_history As frm_customer_notices_history

        frm_history = New frm_customer_notices_history

        frm_history.m_selected_id = m_selected_row
        frm_history.ShowDialog()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_NEW,
           frm_base_sel.ENUM_BUTTON.BUTTON_SELECT ' New button    

        If Not IsNothing(Me.Grid.SelectedRows) AndAlso Me.Grid.SelectedRows.Length > 0 Then
          _index_sel = GetSelectedCustomerNoticesId()
        End If

        If _index_sel <> -1 Then
          If ButtonId = frm_base_sel.ENUM_BUTTON.BUTTON_NEW Then
            If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8667), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_YES Then
              Call GUI_ShowNewNoticesForm(_index_sel, True) ' New Notice with data copied from another
            Else
              Call GUI_ShowNewNoticesForm()                 ' New Notice in Blank
            End If
          Else
            Call GUI_ShowNewNoticesForm(_index_sel)         ' Edit Notice
          End If
        Else
          Call GUI_ShowNewNoticesForm()                     ' New Notice in Blank
        End If
      Case Else
        MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub ' GUI_ButtonClick

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA) ' GUI_ReportFilter

    ' Account
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), m_filt_account)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_filt_track_data)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), m_filt_holder)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4802), m_filt_holder_is_vip)

    ' Current only
    If m_filt_current_only Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(3456), GLB_NLS_GUI_PLAYER_TRACKING.GetString(255))
    Else
      ' Dates
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(8644) & " " & GLB_NLS_GUI_AUDITOR.GetString(257), m_filt_date_from)
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(8644) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_filt_date_to)

      ' Enabled or disabled
      If m_filt_enabled And m_filt_disabled Or Not m_filt_enabled And Not m_filt_disabled Then
        PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(3456), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8671) & " , " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(8672))
      End If

      If m_filt_enabled And Not m_filt_disabled Then
        PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(3456), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8671))
      End If

      If Not m_filt_enabled And m_filt_disabled Then
        PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(3456), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8672))
      End If

      ' User 
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(8645), m_filt_user)

      ' Priorities
      m_priorities = Me.Uc_priority1.GetPrioritiesListText()
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(8630), m_priorities)

    End If
  End Sub ' GUI_ReportFilter

  Protected Overrides Sub GUI_ReportUpdateFilters()
    Dim _string_empty As String

    _string_empty = "---"

    'Account
    m_filt_account = _string_empty
    m_filt_track_data = _string_empty
    m_filt_holder = _string_empty
    m_filt_holder_is_vip = _string_empty

    m_filt_account = Me.Uc_account_sel1.Account()
    m_filt_track_data = Me.Uc_account_sel1.TrackData()
    m_filt_holder = Me.Uc_account_sel1.Holder()
    m_filt_holder_is_vip = Me.Uc_account_sel1.HolderIsVip()

    If Me.gb_date.Enabled Then

      ' Date
      If Me.dtp_from.Checked Then
        m_filt_date_from = GUI_FormatDate(Me.dtp_from.Value, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
      Else
        m_filt_date_from = _string_empty
      End If

      If Me.dtp_to.Checked Then
        m_filt_date_to = GUI_FormatDate(Me.dtp_to.Value, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
      Else
        m_filt_date_to = _string_empty
      End If

    End If

    If Me.opt_user_all.Checked Then
      m_filt_user = Me.opt_user_all.Text
    Else
      m_filt_user = Me.cmb_user.TextValue
    End If

    ' Enable / Disabled
    m_filt_disabled = chk_disabled.Checked
    m_filt_enabled = chk_enabled.Checked

  End Sub ' GUI_ReportUpdateFilters 

#End Region

#Region "Private Functions"

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
      .Sortable = True

      ' Index (hidden)
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Header(1).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Notice ID (hidden)
      .Column(GRID_COLUMN_NOTICE_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_NOTICE_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_NOTICE_ID).Width = GRID_WIDTH_NOTICE_ID
      .Column(GRID_COLUMN_NOTICE_ID).HighLightWhenSelected = False
      .Column(GRID_COLUMN_NOTICE_ID).IsColumnPrintable = False

      ' Customer - Account ID
      .Column(GRID_COLUMN_NOTICE_CUSTOMER_ID).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)               ' Customer
      .Column(GRID_COLUMN_NOTICE_CUSTOMER_ID).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(231)               ' Number
      .Column(GRID_COLUMN_NOTICE_CUSTOMER_ID).Width = GRID_WIDTH_NOTICE_CUSTOMER_ID

      ' Customer - Account name
      .Column(GRID_COLUMN_NOTICE_CUSTOMER_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)             ' Customer
      .Column(GRID_COLUMN_NOTICE_CUSTOMER_NAME).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(235)             ' Holder Name
      .Column(GRID_COLUMN_NOTICE_CUSTOMER_NAME).Width = GRID_WIDTH_NOTICE_CUSTOMER_NAME
      .Column(GRID_COLUMN_NOTICE_CUSTOMER_NAME).IsColumnSortable = True

      ' Notice - Date creation
      .Column(GRID_COLUMN_NOTICE_DATE_CREATION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8608)      ' Notice
      .Column(GRID_COLUMN_NOTICE_DATE_CREATION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8715)      ' Date Creation
      .Column(GRID_COLUMN_NOTICE_DATE_CREATION).Width = GRID_WIDTH_NOTICE_DATE_CREATION
      .Column(GRID_COLUMN_NOTICE_DATE_CREATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_NOTICE_DATE_CREATION).IsColumnSortable = True

      ' Notice - User Creation ID
      .Column(GRID_COLUMN_NOTICE_USER_CREATION_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8608)   ' Notice
      .Column(GRID_COLUMN_NOTICE_USER_CREATION_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8629)   ' User ID
      .Column(GRID_COLUMN_NOTICE_USER_CREATION_ID).Width = GRID_WIDTH_NOTICE_USER_CREATION_ID
      .Column(GRID_COLUMN_NOTICE_USER_CREATION_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Notice - User Creation Name
      .Column(GRID_COLUMN_NOTICE_USER_CREATION_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8608) ' Notice
      .Column(GRID_COLUMN_NOTICE_USER_CREATION_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8629) ' User Name
      .Column(GRID_COLUMN_NOTICE_USER_CREATION_NAME).Width = GRID_WIDTH_NOTICE_USER_CREATION_NAME
      .Column(GRID_COLUMN_NOTICE_USER_CREATION_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_NOTICE_USER_CREATION_NAME).IsColumnSortable = True

      ' Notice - Status
      .Column(GRID_COLUMN_NOTICE_STATUS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8608)             ' Notice
      .Column(GRID_COLUMN_NOTICE_STATUS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3346)             ' Status
      .Column(GRID_COLUMN_NOTICE_STATUS).Width = GRID_WIDTH_NOTICE_STATUS
      .Column(GRID_COLUMN_NOTICE_STATUS).IsColumnSortable = True

      ' Notice - Priotity ID
      .Column(GRID_COLUMN_NOTICE_PRIORITY_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8608)        ' Notice
      .Column(GRID_COLUMN_NOTICE_PRIORITY_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8630)        ' Priority
      .Column(GRID_COLUMN_NOTICE_PRIORITY_ID).Width = GRID_WIDTH_NOTICE_PRIORITY_ID

      ' Notice - Priotity Name
      .Column(GRID_COLUMN_NOTICE_PRIORITY_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8608)      ' Notice
      .Column(GRID_COLUMN_NOTICE_PRIORITY_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8630)      ' Priority
      .Column(GRID_COLUMN_NOTICE_PRIORITY_NAME).Width = GRID_WIDTH_NOTICE_PRIORITY_NAME
      .Column(GRID_COLUMN_NOTICE_PRIORITY_NAME).IsColumnSortable = True

      ' Notice - Header
      .Column(GRID_COLUMN_NOTICE_TITLE_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8608)               ' Notice
      .Column(GRID_COLUMN_NOTICE_TITLE_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8725)               ' Title name
      .Column(GRID_COLUMN_NOTICE_TITLE_NAME).Width = GRID_WIDTH_NOTICE_TITLE_NAME
      .Column(GRID_COLUMN_NOTICE_TITLE_NAME).IsColumnSortable = True

      ' Notice - Text
      .Column(GRID_COLUMN_NOTICE_TEXT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8608)               ' Notice
      .Column(GRID_COLUMN_NOTICE_TEXT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8631)               ' Text
      .Column(GRID_COLUMN_NOTICE_TEXT).Width = GRID_WIDTH_NOTICE_TEXT
      .Column(GRID_COLUMN_NOTICE_TEXT).IsColumnSortable = True
    End With
  End Sub

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub SetDefaultValues()
    Dim closing_time As Integer

    closing_time = GetDefaultClosingTime()

    Me.dtp_from.Value = New DateTime(Now.Year, Now.Month, 1, closing_time, 0, 0)
    Me.dtp_from.Checked = True
    Me.dtp_to.Value = Me.dtp_from.Value.AddMonths(1)
    Me.dtp_to.Checked = False
    Me.opt_user_all.Checked = True
    Me.cmb_user.Enabled = False
    Me.chk_show_all.Checked = False
    Me.chk_show_all.Enabled = False
    Me.chk_enabled.Checked = True
    Me.chk_disabled.Checked = False

    Call Me.Uc_priority1.SetDefaultValues()

    Me.Uc_account_sel1.Clear()

    If m_customer_id <> 0 Then
      Me.Uc_account_sel1.Account = m_customer_id
    End If
  End Sub

  Private Function GetSqlWhere() As String
    Dim _str_account As String
    Dim _str_priority As String

    Dim _lst_filters As List(Of String)
    _lst_filters = New List(Of String)

    _str_account = Me.Uc_account_sel1.GetFilterSQL()
    _str_priority = String.Empty

    'Account
    If (Not String.IsNullOrEmpty(_str_account)) Then
      _lst_filters.Add(_str_account)
    End If

    'Only if one and only one is checked
    If (chk_enabled.Checked Xor chk_disabled.Checked) Then
      If chk_enabled.Checked Then
        _lst_filters.Add("CN_STATUS = '1'")
      End If

      If chk_disabled.Checked Then
        _lst_filters.Add("CN_STATUS = '0'")
      End If
    End If

    'Date
    If Me.dtp_from.Checked Then
      _lst_filters.Add("ISNULL(CN_LAST_UPDATE_DATE, CN_CREATION_DATE) > " & GUI_FormatDateDB(Me.dtp_from.Value))
    End If

    If Me.dtp_to.Checked Then
      _lst_filters.Add("ISNULL(CN_LAST_UPDATE_DATE, CN_CREATION_DATE) < " & GUI_FormatDateDB(Me.dtp_to.Value))
    End If

    ' Add user_Id filter
    If opt_user_one.Checked Then
      _lst_filters.Add("ISNULL(CN_LAST_UPDATE_USER_ID, CN_CREATION_USER_ID) = " & cmb_user.Value)
    End If

    ' Priorities
    _str_priority = Uc_priority1.GetPriorityTypesSelected()
    If (Not String.IsNullOrEmpty(_str_priority)) Then
      _lst_filters.Add(_str_priority)
    End If

    Return mdl_misc.GetFiltersListText(_lst_filters)

  End Function

  ' PURPOSE: Fill the user tbale wuth descendents of logged user
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Sub AddUserTableToDataset()
    Dim string_sql As String
    'XVV Variable not use Dim ds As DataSet
    Dim dt As DataTable

    Call cmb_user.Clear()

    Try
      string_sql = " SELECT DISTINCT(GU_USER_ID) " & _
                        ", GU_USERNAME " & _
                   " FROM   GUI_USERS "

      ' XCD 16-Aug-2012 Discard unsubscribed users
      If Not chk_show_all.Checked Then
        string_sql = string_sql & " WHERE GU_BLOCK_REASON = " & WSI.Common.GUI_USER_BLOCK_REASON.NONE
      End If

      dt = GUI_GetTableUsingSQL(string_sql, MAX_RECORDS_LOAD)

      If Not IsNothing(dt) Then
        Call Fill_combo(cmb_user, dt)
        dt.Dispose()
        dt = Nothing
      End If

    Catch exception As Exception
      Call Trace.WriteLine(exception.ToString())
      'Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
      '                      MAIN_MODULE_NAME, _
      '                      FunctionName, _
      '                      exception.Message)
    End Try
  End Sub 'AddUserTableToDataset

  Private Sub chk_show_all_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_show_all.CheckedChanged
    AddUserTableToDataset()
  End Sub 'chk_show_unsubscribed

  Private Sub opt_user_all_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_user_all.Click
    cmb_user.Enabled = False
    chk_show_all.Enabled = False
  End Sub 'opt_user_all_Click

  Private Sub opt_user_one_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_user_one.Click
    cmb_user.Enabled = True
    chk_show_all.Enabled = True
  End Sub 'opt_user_one_Click

  ' PURPOSE: Fill the combo box user control
  '
  '  PARAMS:
  '     - INPUT:
  '           - table: Tableset from memory
  '     - OUTPUT:
  '           - combo: User control
  '
  ' RETURNS:
  '     - none
  Private Sub Fill_combo(ByVal combo As uc_combo, ByVal table As DataTable)
    Dim idx As Integer
    Dim aux_id As Integer
    Dim aux_name As String
    Dim s As SortedList

    Call combo.Clear()
    s = New SortedList()

    For idx = 0 To table.Rows.Count - 1
      aux_id = CInt(table.Rows.Item(idx).Item(0))
      aux_name = CStr(table.Rows.Item(idx).Item(1))
      s.Add(aux_name, aux_id)
    Next

    For idx = 0 To s.Count - 1
      aux_id = s.GetByIndex(idx)
      aux_name = s.GetKey(idx)
      Call combo.Add(aux_id, aux_name)
    Next

  End Sub 'Fill_combo

  ' PURPOSE : Adds a new notice table to the system (Copy message)
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Sub GUI_NewItemFromSelected()

    Dim _idx_row As Int32
    Dim _notice_id As Integer

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    If _idx_row = Me.Grid.NumRows Then
      Return
    End If

    Call GUI_ShowNewNoticesForm(_notice_id)

  End Sub ' NewPromotion


  ' PURPOSE: Auditor Data
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '  Private Sub AuditChanges(ByVal Message1_L1 As String, ByVal Order1 As Integer, _
  '                            ByVal Message2_L1 As String, ByVal Order2 As Integer)
  '    Dim _curr_auditor As CLASS_AUDITOR_DATA
  '    Dim _orig_auditor As CLASS_AUDITOR_DATA

  '    _curr_auditor = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)
  '    _orig_auditor = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)

  '    Call _curr_auditor.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(7501), "")
  '    Call _orig_auditor.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(7501), "")

  '    Call _curr_auditor.SetField(0, Order1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7502, "'" & Message1_L1 & "' - '" & Message2_L1 & "'"))
  '    Call _orig_auditor.SetField(0, Order2, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7502, "'" & Message1_L1 & "' - '" & Message2_L1 & "'"))

  '    ' Notify
  '    Call _curr_auditor.Notify(CurrentUser.GuiId, _
  '                                  CurrentUser.Id, _
  '                                  CurrentUser.Name, _
  '                                  CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.UPDATE, _
  '                                  0, _
  '                                  _orig_auditor)

  '    _curr_auditor = Nothing
  '    _orig_auditor = Nothing

  '  End Sub


#End Region

#Region "Public Functions"
  ' PURPOSE : Opens dialog with default settings for edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window, Optional ByVal _procedence_promotions As Boolean = False)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window, ByVal account_id As Int64)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING

    Me.m_customer_id = account_id
    Uc_account_sel1.Account = m_customer_id

    Me.MdiParent = MdiParent
    Me.Display(False)

    'Search the customer notices
    If Me.Permissions.Read Then
      Call Application.DoEvents()
      System.Threading.Thread.Sleep(100)
      Call Application.DoEvents()

      Me.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)
    End If

  End Sub ' ShowForEdit


  'Public Sub ShowCustomer(Optional ByVal CustomerId As Long = 0)

  '  m_customer_id = CustomerId
  '  Me.FormId = ENUM_FORM.FORM_CUSTOMER_NOTICES_SEL
  '  MyBase.GUI_SetFormId()

  'End Sub

  ' PURPOSE : Adds a new notice to the system
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Sub GUI_ShowNewNoticesForm(Optional ByVal NoticesCopiedId As Long = -1, Optional ByVal NewCustomerNoticeId As Boolean = False)

    Dim frm_edit As frm_customer_notices_edit
    frm_edit = New frm_customer_notices_edit()
    If NoticesCopiedId <> -1 Then
      frm_edit.ShowNewEditItemNotice(NoticesCopiedId, NewCustomerNoticeId)
    Else
      frm_edit.ShowNewEditItemAccount(IIf(Me.m_customer_id <= 0, -1, Me.m_customer_id))
    End If

    frm_edit = Nothing
    Call Me.Grid.Focus()

  End Sub ' NewPromotion

  Private Function GetSelectedCustomerNoticesId() As Long
    Dim _idx As Integer

    For _idx = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx).IsSelected Then

        Return Me.Grid.Cell(_idx, GRID_COLUMN_NOTICE_ID).Value
      End If
    Next

    Return -1
  End Function

#End Region

#Region "Events"

#End Region

End Class