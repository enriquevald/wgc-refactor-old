'-------------------------------------------------------------------
' Copyright � 2003 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_main.vb
'
' DESCRIPTION:   Wigos GUI main form..
'
' AUTHOR:        Ignasi Ripoll
'
' CREATION DATE: 03-MAY-2004
'
' REVISION HISTORY:
' 
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 03-MAY-2004 IRP    Initial version. 
' 02-APR-2012 RCI & MPO     Added event handler OnAutoLogout.
' 27-MAR-2013 DHA    Form title is updated with the multisite server status.
' 21-JUN-2013 PGJ    Show the Card Writer and Tools menu only for SU user.
' 05-JUL-2013 JCOR   Call to frm_terminals_sel changed to frm_terminals.
' 12-SEP-2013 ANG    Fix bug WIG-199.
' 06-NOV-2013 JAB    Added Cage items.
' 02-DEC-2013 RMS    Added app version to form title
' 16-DEC-2013 JBP    Added game table items.
' 18-DEC-2013 ICS    Added TITO options to menu.
' 24-JAN-2014 JBP    Reorganized Cash Cage, TITO and Gaming Tables menu items
' 19-MAR-2014 DHA    Added form 'Gaming session adjustment'
' 20-MAR-2014 JRM    Added new functionality. Description contained in RequestPinCodeDUT.docx DUT
' 11-APR-2014 DRV    Added permission to forms 'Software Validation Send' and 'Software Validation History'
' 06-MAY-2014 RCI    Added GP to show/hide "Mesas de Juego" functionality
' 03-JUN-2014 RCI    Added GP to show/hide "Tax Report"/"Informe de Impuestos" form
' 29-OCT-2014 DHA    Added "Gambling Table" menu with all gambling table options and "TITO Tickets" menu
' 17-NOV-2014 DCS    Added "Mobile Banks" menu
' 25-NOV-2014 DHA    Redesign GUI menus
' 04-DEC-2014 DHA    Added LCD message form
' 21-JAN-2015 DLL    Fixed Bug WIG-1844: frm_terminal_sas_meters_edit only for TITO
' 28-JAN-2015 DHA    Fixed Bug WIG-1976: Recovered "Disconect" option from "System" menu
' 02-FEB-2015 DHA    Added menu item 'Gaming Table sessions import'
' 12-FEB-2015 FJC    New GUI Menu
' 24-MAR-2015 AMF    Added menu item 'Meter Delta'
' 26-MAR-2015 DRV    Moved menu item 'Change Stacker'
' 16-APR-2015 JBC & RMS   Fixed Buf WIG-2221: Request from cashier to Cage Alarm :Application is closed 
' 09-APR-2015 AMF    Backlog Item 680
' 22-APR-2015 SGB    Backlog Item 1129
' 16-APR-2015 JBC    Fixed Bug WIG-2241: If current user is SU, cage alarms are hidden.
' 04-MAY-2015 SGB    Backlog Item 1314
' 03-SEP-2015 ETP    BUG FIXED Recaudaci�n Terminal/Juego - Error SIN permisos - Muestra el men�
' 04-SEP-2015 YNM    Fixed Bug TFS-4207: exclude terminals movements
' 09-SEP-2015 YNM    Fixed Bug 4021/4022: Error without permission menu options are available
' 14-SEP-2015 ETP    Fixed Bug 4185: New NLS added for alerts.
' 17-SEP-2015 FAV    Fixed Bug 4519: Alert symbol without information (operation with stackers).
' 26-OCT-2015 YNM    Fixed Bug 5530: Alarm botton don't appear
' 01-OCT-2015 ECP    Backlog Item 4158 GUI - Manual pays balance report
' 26-OCT-2015 YNM    Fixed Bug 5530: Alarm botton don't appear
' 05-NOV-2015 SMN    Fixed Bug 6247: SmartFloor menu disabled
' 04-SEP-2015 DLL    Fixed Bug TFS-6510: Permission error at form Multidenomination
' 14-OCT-2015	JRC    Product Backlog Item 4942 LottoRace: Creaci�n grupo InTouchGames
' 22-OCT-2015	JRC    Product Backlog Item 5362 SmartFloor: Reporte de Runners
' 16-NOV-2015 JRC 	 Product Backlog Item 5993 GUI - Creaci�n e Impresi�n C�digos QR
' 02-DEC-2015 MPO    Fixed Bug 7279: Like 5530
' 03-DIC-2015 RGR    Task 6719: Report payments first review changes
' 09-DIC-2015 JRC    Product Backlog Item 5491 Multiple Buckets 
' 30-DEC-2015 RGR    Product Backlog Item 6451:Garcia River-03: equity report
' 11-JAN-2016 RAB    Bug 5355:Se accede a Wigos GUI con un usuario sin permisos
' 13-JAN-2016 YNM    Bug 8346: Error on Visits Report Add Function IsReceptionEnabled
' 14-JAN-2016 JPJ    Bug 8429: TITOTaxWaiver GP is not taken into account
' 28-JAN-2016 FJC    Product Backlog Item 8554:Visitas / Recepci�n: Crear ticket de entrada: GUI precios entrada
' 01-FEB-2016 DHA    Product Backlog Item 8502:Floor Dual Currency: Cuadratura de estad�sticas
' 01-FEB-2016 DDS    Bug 8818:TITO - Acceso Directo Alertas - No se Borra el Men� cuando se han realizado todas las Recaudaciones
' 09-FEB-2016 DHA    Bug 9054: hide form Machine and Game Report for Floor Dual Currency
' 09-FEB-2016 DHA    Product Backlog Item 9051: hide collection from file for Floor Dual Currency
' 16-FEB-2016 LTC    Task 9340: Group chips: Design changes in Group chips
' 25-FEB-2016 LTC    Bug 9582:Mesas: Grupo y MD - NO se puede acceder al formulario Fichas
' 21-MAR-2016 JRC    PBI: 1792 Multiple Buckets: Desactivar Acumulaci�n de Buckets si SPACE is enabled
' 24-MAR-2016 RAB    Product Backlog Item 10942:GUI: summary box TITO Hold vs Win
' 21-MAR-2016 JRC     PBI: 1792 Multiple Buckets: Desactivar Acumulaci�n de Buckets si SPACE is enabled
' 02-MAR-2016 ETP    PBI 10190: Hide multi-denominations menu
' 31-MAR-2016 DDS    PBI 10951:CountR - Cash It Out: GUI - Selection form
' 14-JUN-2016 EOR    Product Backlog Item 13672:Sprint Review 24 Winions - Mex
' 24-JUN-2016 ESE    Product Backlog Item 13581:Generic reports: Add Report Tool to GUI
' 05-AGO-2016 LTC    Product Backlog Item 15199:TPV Televisa: GUI, Banking cards reconciliation
' 12-AUG-2016 ESE    Product Backlog Item 16212:JOP09: Booking terminal
' 15-AUG-2016 EOR    Product Backlog Item 13573: JOP09: Reserved maquines: Report of reserved maquines
' 17-AUG-2016 ESE    Bug 16534:Gaming Tables: Exception when disable gaming table parameter
' 23-AGO-2016 LTC    Product Backlog Item 16742:TPV Televisa: Reconciliation modification of cashier vouchers
' 24-AUG-2016 RGR    Bug 15431: Permission to Hold TITO vs Win is inserted even if not activated
' 25-AGO-2016 ESE    Product Backlog Item 13581:Show all the reports, not only those whit permission
' 14-SEP-2016 ESE    PBI 17115: Draws gamings tables
' 15-SEP-2016 ETP    Fixed bugs 17585 - 17607: Generic reports: add permissions config.
' 23-SEP-2016 FJC    Product Backlog Item 17681:Visitas / Recepci�n: MEJORAS II - Agrupar men� (GUI)
' 27-SEP-2016 FAV    Fixed Bug 8816: Allow request to cage without cage opened.
' 29-SEP-2016 FJC    PBI 17681:Visitas / Recepci�n: MEJORAS II - Agrupar men� (GUI)
' 04-OCT-2016 JBP    PBI 18464:PariPlay - Ajustar pantallas de BonoPlay
' 07-OCT-2016 LTC    Bug 18717:Cash desk draw for gaming tables: Permission is inserted and configuration windows is shown in an environment without gaming tables
' 19-10-2016  ATB    Bug 19280:Televisa: Cuando se desabilitan los GP "BankCard.Conciliate" se sigue mostrando "Conciliaci�n de tarjetas bancarias" en WigosGUI
' 19-10-2016  ATB    Bug 19281:Televisa: Con el Pin pad desactivado en los GP se sigue viendo la opci�n "Hist�rico de transacciones bancarias"
' 20-10-2016  ATB    Bug 19281:Televisa: Con el Pin pad desactivado en los GP se sigue viendo la opci�n "Hist�rico de transacciones bancarias" (hiding the main option)
' 26-10-2016  JRC    PBI 19580:TITO: Pantalla de Log-Seguimiento de Tickets (Cambios Review)
' 31-OCT-2016 RGR    Fixed Bug 19892:GUI If not exist GP-PinPad.Enabled sends an error
' 24-NOV-2016 LTC    PBI 19870:R.Franco: New report Machines and Gaming Tables
' 27-DIC-2016 PDM    Task 21636:GUI - Fix - Asignaci�n de entrada de men� Campa�as publicitarias a Gesti�n de Contenido
' 02-DIC-2016 PDM    Task 22181:GUI - Ajustes menu WinUP - Rearmar Menu
' 30-JAN-2017 FOS    Bug 23862:NLS de permisos poco descriptivas
' 09-FEN-2017 JML    PBI 24378: Reabrir sesiones de caja y mesas de juego: rehacer reapertura y correcci�n de Bugs
' 05-JAN-2017 ESE    PBI 22283:Winpot - concepto de promociones
' 21-FEB-2017 LTC    PBI 22283:Winpot - Change Promotions NLS
' 27-MAR-2017 XGJ    PBI 26247:Sorteo de m�quina - Habilitar / Deshablitar la funcionalidad de sorteo de m�quina
' 18-APR-2017 DMT    WIGOS-888 Dynamic content menu by sections
' 24-APR-2017 DHA    PBI 26812: Drop gaming table information - Global drop hourly report
' 26-APR-2017 MS     PBI 27043: Junkets - Report of junket
' 27-APR-2017 DPC    WIGOS-637: Junkets - Report of flyers
' 07-JUN-2017 JMM    WIGOS-2240: Pantalla "Hist�rico de Contadores" no muestra resultados
' 30-MAR-2017 RLO    Product Backlog Item 26368:WIGOS-67 Jackpots - Jackpot selection
' 18-JUL-2017 ETP    WIGOS-3515 New Jackpots option menu appears in wrong place and with wrong description.
' 18-JUL-2017 ETP    WIGOS-3501 Incorrect menu opcion enabled
' 26-SEP-2017 EOR    PBI 29907: WIGOS-4821 Table games analysis report
' 03-OCT-2017 RGR    WIGOS-4852 Table players report
' 20-NOV-2017 RAB    Bug 30855:WIGOS-6527 [Ticket #10394] e03.007.0012.01 [v3.007.]MKT: No aparece el s�mbolo de Alarma al realizar una recaudaci�n en caja
' 21-NOV-2017 DPC    PBI 30852:[WIGOS-4055]: Payment threshold registration - Threshold movements report
' 30-NOV-2017 RGR    PBI 30932:WIGOS-6393 GUI - Menu: User Interface improvements
' 24-OCT-2017 RGR    PBI 30366:WIGOS-5680 Winpot - Facturation requirements - From 01/12/2017
' 14-DEC-2017 DPC    PBI 31072:[WIGOS-7111] AGG - EGM connection control
' 18-DEC-2017 JBM    PBI 31094:[WIGOS-7005] AGG - EGM Working day
' 04-JAN-2017 RGR    PBI 30366:WIGOS-5680 Winpot - Facturation requirements - From 01/12/2017
' 08-JAN-2018 RGR    PBI 30932:WIGOS-6393 GUI - Menu: User Interface improvements 
' 14-MAR-2018 EOR    Bug 31922:WIGOS-8178 [Ticket #12451] Permiso para Edici�n de Sesi�n de Juego En Mesa Version V03.06.0040 
' 25-ABR-2018 FOS    Bug 32220:WIGOS-10374 When opening the WigosGUI, a warning message appears but when clicking on it, it just dissapears.
' 14-MAY-2018 AGS    Bug 32672:WIGOS-11760 Movibank functionality appears in GUI
' 24-MAY-2018 LQB    Bug 32767:WIGOS-14492 Comment option Meters_Comparison
' 11-JUN-2018 JGC    PBI 32946:WIGOS-10137 Philippines - Create Cashout Receipt Report
' 02-JUL-2018 FOS    Bug 33441:WIGOS-12444 [Ticket #14705] Bajar categor�a socio
' 05-JUL-2018 AGS    Bug 33507:WIGOS-13448 An user with all permissions cannot open 'Imprimir c�digos QR de m�quinas'.
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common
Imports WSI.Common.Junkets
Imports WSI.Common.Jackpot

Public Class frm_main
  Inherits GUI_Controls.frm_base_mdi

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub
  Friend WithEvents lbl_main_title As GUI_Controls.uc_text_field
  Friend WithEvents tmr_MoneyRequestPending As System.Timers.Timer
  Friend WithEvents tmr_TextBlinking As System.Windows.Forms.Timer

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container
    Me.lbl_main_title = New GUI_Controls.uc_text_field
    Me.tmr_MoneyRequestPending = New System.Timers.Timer()
    Me.tmr_TextBlinking = New System.Windows.Forms.Timer(Me.components)
    Me.SuspendLayout()
    '
    'lbl_main_title
    '
    Me.lbl_main_title.IsReadOnly = True
    Me.lbl_main_title.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_main_title.LabelForeColor = System.Drawing.Color.Blue
    Me.lbl_main_title.Location = New System.Drawing.Point(282, 19)
    Me.lbl_main_title.Name = "lbl_main_title"
    Me.lbl_main_title.Size = New System.Drawing.Size(66, 24)
    Me.lbl_main_title.SufixText = "Sufix Text"
    Me.lbl_main_title.SufixTextVisible = True
    Me.lbl_main_title.TabIndex = 1
    Me.lbl_main_title.Visible = False
    '
    'tmr_MoneyRequestPending
    '
    '
    'tmr_TextBlinking
    '
    '
    'frm_main
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(496, 272)
    Me.Controls.Add(Me.lbl_main_title)
    Me.Name = "frm_main"
    Me.Text = "xWigos GUI"
    Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    Me.Controls.SetChildIndex(Me.lbl_main_title, 0)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Delegates "

  Private Delegate Sub DelegatesNoParams()

#End Region

#Region " Members "

  Dim m_elp_mode As Int32

  ' DHA 27-MAR-2013: Multisite status
  Dim MultisiteStatus As WGDB.MULTISITE_STATUS = WGDB.MULTISITE_STATUS.UNKNOWN

  Dim ProfilesMenuItem As CLASS_MENU_ITEM
  Dim TerminalsMenuItem As CLASS_MENU_ITEM
  Dim TerminalsTimeDisconectedMenuItem As CLASS_MENU_ITEM

#If DEBUG Then
  ' This view will replace the existing terminals view
  ' JRM 10-JUN-2013
  Dim TerminalsFormMenuItem As CLASS_MENU_ITEM
#End If

  Dim Terminals3gsMenuItem As CLASS_MENU_ITEM
  Dim TerminalsPendingMenuItem As CLASS_MENU_ITEM
  Dim GeneralParamsMenuItem As CLASS_MENU_ITEM
  Dim TerminalGameRelationMenuItem As CLASS_MENU_ITEM
  ' ETP 03-SET-2015
  Dim TerminalMeterDeltaItem As CLASS_MENU_ITEM

  ' JGR 17-FEB-2012: Area & Bank
  Dim AreaBank As CLASS_MENU_ITEM
  Dim ExitMenuItem As CLASS_MENU_ITEM

  Dim Auditor As CLASS_MENU_ITEM

  Dim EventHistory As CLASS_MENU_ITEM
  Dim Alarms As CLASS_MENU_ITEM

  Dim Statistics As CLASS_MENU_ITEM
  Dim Statistics_games As CLASS_MENU_ITEM
  Dim Statistics_terminals As CLASS_MENU_ITEM
  Dim Statistics_hours As CLASS_MENU_ITEM
  Dim Statistics_cards As CLASS_MENU_ITEM
  Dim Statistics_hours_group As CLASS_MENU_ITEM
  Dim Capacity As CLASS_MENU_ITEM
  Dim CustomerBase As CLASS_MENU_ITEM
  Dim MachineOcupation As CLASS_MENU_ITEM
  ' JGR 25-JAN-2012: Segmentaci�n Cliente-Proveedor
  Dim CustomerSegmentation As CLASS_MENU_ITEM

  Dim Plays_sessions As CLASS_MENU_ITEM
  Dim Plays_sessions_points_assignment As CLASS_MENU_ITEM
  'DHA 17-MAR-2014: Play Sessions Edit
  Dim Plays_sessions_edit As CLASS_MENU_ITEM
  'Dim Plays_plays As CLASS_MENU_ITEM

  Dim Statistics_reportbonoplay As CLASS_MENU_ITEM
  Dim Statistics_reportbonoplay_played As CLASS_MENU_ITEM

  Dim Ws_sessions As CLASS_MENU_ITEM
  Dim Game_meters As CLASS_MENU_ITEM
  Dim Machine_denom_meters As CLASS_MENU_ITEM
  Dim Svc_monitor As CLASS_MENU_ITEM
  Dim Commands_menu As CLASS_MENU_ITEM
  Dim Commands_send As CLASS_MENU_ITEM
  Dim Commands_history As CLASS_MENU_ITEM

  ' RCI 20-DEC-2010: Terminals without activity
  Dim Terminals_without_activity As CLASS_MENU_ITEM

  ' RCI 22-DEC-2010: Automatic Mailing Programming
  Dim Mailing_Programming As CLASS_MENU_ITEM

  ' APB 03-FEB-2011: Sites and Site Operators
  Dim Sites_Operators As CLASS_MENU_ITEM

  Dim Card_summary As CLASS_MENU_ITEM
  Dim Card_movements As CLASS_MENU_ITEM
  Dim Point_movements As CLASS_MENU_ITEM

  Dim Cashier_sessions As CLASS_MENU_ITEM
  Dim Cashier_movements As CLASS_MENU_ITEM

  Dim Acct_taxes As CLASS_MENU_ITEM
  Dim Sales_date_terminal As CLASS_MENU_ITEM
  Dim Provider_day_terminal As CLASS_MENU_ITEM
  Dim Provider_day_terminal_statistics As CLASS_MENU_ITEM
  Dim Menu_Handpays As CLASS_MENU_ITEM
  Dim MB_Movements As CLASS_MENU_ITEM
  Dim Taxes_report As CLASS_MENU_ITEM
  Dim Gap_report As CLASS_MENU_ITEM
  Dim Gap_report_statistics As CLASS_MENU_ITEM
  Dim Tf_Gap_report As CLASS_MENU_ITEM

  ' JML 23-MAY-2012      TODO
  Dim Cash_report As CLASS_MENU_ITEM

  ' JAB 06-MAR-2012: Forms Report
  Dim Form_report As CLASS_MENU_ITEM
  Dim Tickets_report As CLASS_MENU_ITEM
  Dim Detailed_prizes As CLASS_MENU_ITEM

  Dim Tito_holdvswin_report As CLASS_MENU_ITEM


  ' JAB 28-MAR-2012: Voucher Browser
  Dim Voucher_browser As CLASS_MENU_ITEM

  Dim Prize_taxes_report As CLASS_MENU_ITEM

  ' SSC 03-FEB-2012: Terminal money (Accepted Notes)
  Dim Terminal_money As CLASS_MENU_ITEM

  ' SSC 08-FEB-2012: Money pending transfer
  Dim Money_pending_transfer As CLASS_MENU_ITEM

  ' SSC 17-ABR-2012: Money Collections
  Dim Money_Collections As CLASS_MENU_ITEM

  ' SSC 16-ABR-2012: Note Acceptor
  Dim Note_Acceptor As CLASS_MENU_ITEM

  ' DDM 14-MAR-2012: Productivity reports.
  Dim Productivity_reports As CLASS_MENU_ITEM

  ' DDM 05-APR-2012: Productivity reports.
  Dim Segmentation_reports As CLASS_MENU_ITEM

  ' DDM 27-JUN-2012: Promotion reports.
  Dim Promotion_Report As CLASS_MENU_ITEM

  ' RRB 21-AUG-2012: Accounts menu
  Dim Card_details As CLASS_MENU_ITEM

  'YNM 14-DIC-2015
  Dim Customer_Visits As CLASS_MENU_ITEM

  'YNM 16-FEB-2016
  Dim Blacklist_Import As CLASS_MENU_ITEM

  ' XCD 22-AUG-2012: Flags Menu
  Dim Menu_Flags As CLASS_MENU_ITEM
  Dim Account_Flags As CLASS_MENU_ITEM

  'RLO 24-JUL-2015: PromoGame
  Dim Promogame As CLASS_MENU_ITEM

  'JAB 25-MAR-2013_ Report draw tickets menu
  Dim Report_draw_tickets As CLASS_MENU_ITEM

  ' RCI 03-DEC-2010: Not used. Out for now.
  'Dim Class_II_draw_audit As CLASS_MENU_ITEM

  Dim Class_II_jackpot As CLASS_MENU_ITEM
  Dim Jackpot_configuration As CLASS_MENU_ITEM
  Dim Jackpot_history As CLASS_MENU_ITEM
  Dim Jackpot_monitor As CLASS_MENU_ITEM
  Dim Jackpot_promo_messages As CLASS_MENU_ITEM

  Dim Site_jackpot As CLASS_MENU_ITEM
  Dim Site_jackpot_configuration As CLASS_MENU_ITEM
  Dim Site_jackpot_history As CLASS_MENU_ITEM
  Dim Site_bonuses As CLASS_MENU_ITEM

  'Dim Player_tracking_players As CLASS_MENU_ITEM

  Dim Menu_Draws As CLASS_MENU_ITEM
  Dim Menu_Promotions As CLASS_MENU_ITEM
  Dim Menu_Periodical_Promotions As CLASS_MENU_ITEM
  Dim Menu_GiftsCatalog As CLASS_MENU_ITEM
  Dim Menu_GiftsHistory As CLASS_MENU_ITEM

  Dim Sw_download_versions As CLASS_MENU_ITEM
  Dim Terminals_sw_version As CLASS_MENU_ITEM
  Dim Terminals_sw_version_monitor As CLASS_MENU_ITEM
  Dim Misc_sw_build As CLASS_MENU_ITEM
  Dim Licences As CLASS_MENU_ITEM

  Dim Tools As CLASS_MENU_ITEM
  Dim Magnetic_card_writer As CLASS_MENU_ITEM

  Dim Cashier_Configuration As CLASS_MENU_ITEM
  Dim Player_Tracking_Configuration As CLASS_MENU_ITEM
  Dim Catalog_Configuration As CLASS_MENU_ITEM

  'SDS Game Categories & Game Admin
  Dim m_game_admin As CLASS_MENU_ITEM
  ' Dim Game_Admin As CLASS_MENU_ITEM
  Dim Game_Categories As CLASS_MENU_ITEM

  'SDS 15-SEPT-2016: sections schema for App Mobile
  Dim Section_Schema As CLASS_MENU_ITEM
  Dim Winup_Content As CLASS_MENU_ITEM
  Dim Expired_Credits As CLASS_MENU_ITEM

  Dim Advertisement As CLASS_MENU_ITEM
  Dim Games_Admin As CLASS_MENU_ITEM
  Dim Aboutus As CLASS_MENU_ITEM
  Dim HowToGetThere As CLASS_MENU_ITEM
  Dim AdsConfiguration As CLASS_MENU_ITEM

  'SDS 27-OCT-2016 Settings for App Mobile
  Dim Winup_Settings As CLASS_MENU_ITEM

  'PDM 20-ENE-2107 Notifications for App Mobile
  Dim Winup_Notifications As CLASS_MENU_ITEM


  ' JAB 22-JUL-2014: LKT LCD Display Configuration
  Dim LcdConfiguration As CLASS_MENU_ITEM

  ' DHA 01-DEC-2014
  Dim LcdMessageSel As CLASS_MENU_ITEM

  Dim AccountRequestPin As CLASS_MENU_ITEM

  ' JML 14-JUN-2012
  Dim Provider As CLASS_MENU_ITEM

  ' SGB 04-DEC-2015
  Dim Bucket As CLASS_MENU_ITEM
  Dim Bucket_sel As CLASS_MENU_ITEM

  ' XGJ 10-AGO-2016
  Dim Buckets_multiplier_sel As CLASS_MENU_ITEM

  ' XGJ 23-GEN-2017
  'XGJ TODO
  Dim Terminal_Draw_Report As CLASS_MENU_ITEM

  ' JML 19-JUN-2012
  Dim Configuration_menu As CLASS_MENU_ITEM
  Dim Currencies As CLASS_MENU_ITEM

  ' RXM 16-AGO-2012
  Dim Import_menu As CLASS_MENU_ITEM

  ' RXM 01-AGO-2012: Accounts import.
  Dim Accounts_Import As CLASS_MENU_ITEM

  ' JML 28-JUN-2012      TODO
  Dim Peru_Frames As CLASS_MENU_ITEM

  ' HBB 2-AUG-2012
  Dim Password_Configuration As CLASS_MENU_ITEM

  Dim Menu_Preassigned_Promotions As CLASS_MENU_ITEM

  ' DDM 28-AUG-2012: promotion delivered.
  Dim Promotion_Delivered_Report As CLASS_MENU_ITEM

  ' HBB 13-SEP-2012
  Dim Cash_By_Service As CLASS_MENU_ITEM

  ' JML 20-SEP-2012
  Dim Retreats_report As CLASS_MENU_ITEM

  ' AMF 28-JUN-2013
  Dim Service_Charge_report As CLASS_MENU_ITEM

  ' HBB 27-SEP-2012
  Dim Stats_Adjustment As CLASS_MENU_ITEM

#If DEBUG Then
  ' RRR 14-NOV-2014
  Dim FormCharts As CLASS_MENU_ITEM
#End If

  ' HBB 14-JAN-2013
  Dim PaymentOrdersConfig As CLASS_MENU_ITEM

  ' LEM 15-JAN-2013
  Dim Play_preferences As CLASS_MENU_ITEM

  ' HBB 17-JAN-2013
  Dim Payment_Orders As CLASS_MENU_ITEM

  ' RRB 23-JAN-2013
  Dim Cash_Summary As CLASS_MENU_ITEM

  ' QMP 18-FEB-2013
  Dim Operation_Schedule As CLASS_MENU_ITEM

  ' JMM 12-MAR-2013
  Dim MultiSite_Config As CLASS_MENU_ITEM
  Dim MultiSite_Monitor As CLASS_MENU_ITEM

  'SMN 25-MAR-2013
  Dim Machine_Alias As CLASS_MENU_ITEM

  'DMR 30-APR-2013
  Dim Providers_Games As CLASS_MENU_ITEM

  'AMF 30-JUL-2013
  Dim Currency_Configuration As CLASS_MENU_ITEM

  'DLL 18-JUN-2013
  Dim Currency_Exchange_Audit As CLASS_MENU_ITEM

  'RBG 21-JUN-2013
  Dim Menu_Groups As CLASS_MENU_ITEM

  ' RRR 22-JUL-2013
  Dim Money_Laundering_Config As CLASS_MENU_ITEM

  ' RRR 05-AUG-2013
  Dim Player_Edit_Config As CLASS_MENU_ITEM

  ' MMG 08-JUL-2013 
  Dim Draws_cashdesk_detail As CLASS_MENU_ITEM

  'MMG 16-JUL-2013
  Dim Draws_cashdesk_summary As CLASS_MENU_ITEM

  ' ANG 25-JUL-2013
  Dim Money_Laundering_Report As CLASS_MENU_ITEM
  Dim Money_Laundering_Report_Prize As CLASS_MENU_ITEM
  Dim Money_Laundering_Report_Recharge As CLASS_MENU_ITEM

  ' MMG 30-JUL-2013 
  Dim Cashdesk_Draws_Configuration As CLASS_MENU_ITEM
  'XGJ TODO
  Dim Terminal_Draws_Configuration As CLASS_MENU_ITEM

  ' ESE 14-SEP-2016
  Dim Cashdesk_Draws_Tables_Configuration As CLASS_MENU_ITEM

  ' JBC 17-SEP-2013: Points import.
  Dim Points_import As CLASS_MENU_ITEM

  'JPJ 09-OCT-2013
  Dim Menu_TableToPoints As CLASS_MENU_ITEM

  ' RMS 21-OCT-2013
  Dim Account_Card_Changes As CLASS_MENU_ITEM

  ' JAB 06-NOV-2013: Cage forms
  Dim CAGE_Amounts As CLASS_MENU_ITEM
  Dim CAGE_Movements_sel As CLASS_MENU_ITEM
  Dim CAGE_New As CLASS_MENU_ITEM
  Dim CAGE_CashCageCount As CLASS_MENU_ITEM
  Dim CAGE_CashCageStock As CLASS_MENU_ITEM
  Dim CAGE_Source_Target As CLASS_MENU_ITEM
  Dim CAGE_Sessions As CLASS_MENU_ITEM

  'CAGE TEAM
  'AMF 08-APR-2015

  Dim CAGE_Safe_Keeping_menu As CLASS_MENU_ITEM
  Dim CAGE_Safe_Keeping_sel As CLASS_MENU_ITEM

  'SGB 14-APR-2015
  Dim CAGE_Safe_Keeping_movement_sel As CLASS_MENU_ITEM

  'SGB 22-APR-2015
  Dim ACCOUNT_Statement_menu As CLASS_MENU_ITEM
  Dim ACCOUNT_Statement_document_edit As CLASS_MENU_ITEM

  Dim ACCOUNT_Statement_print As CLASS_MENU_ITEM


  ' DLL 14-NOV-2013
  Dim Skipped_Meters_Report As CLASS_MENU_ITEM

  ' YNM 27-AUG-2015
  Dim Meters_Comparison As CLASS_MENU_ITEM

  ' JBP 16-DEC-2013
  Dim Gaming_Tables As CLASS_MENU_ITEM
  Dim Gaming_Tables_Types As CLASS_MENU_ITEM
  Dim Gaming_Tables_Income_Report As CLASS_MENU_ITEM
  Dim Gaming_Tables_Score_Report As CLASS_MENU_ITEM
  Dim Gaming_Tables_Session As CLASS_MENU_ITEM
  ' RMS 09-JAN-2014
  Dim Chips_Operations_Report As CLASS_MENU_ITEM

  ' ICS 18-DEC-2013
  Dim Accounting_TITO_Menu As CLASS_MENU_ITEM
  Dim m_tito_params As CLASS_MENU_ITEM
  Dim m_stacker_collection_sel As CLASS_MENU_ITEM
  Dim m_circulating_cash As CLASS_MENU_ITEM
  Dim m_stacker_sel As CLASS_MENU_ITEM
  Dim m_ticket_tito_report As CLASS_MENU_ITEM
  Dim m_terminals_configuration As CLASS_MENU_ITEM
  Dim m_points_to_credit As CLASS_MENU_ITEM
  Dim m_points_assignment As CLASS_MENU_ITEM
  Dim m_terminal_sas_meters_sel As CLASS_MENU_ITEM
  Dim m_terminal_sas_meters_sel_advanced As CLASS_MENU_ITEM
  Dim m_tito_collection_balance_report As CLASS_MENU_ITEM
  Dim m_tito_summary_sel As CLASS_MENU_ITEM
  Dim m_bonuses_report As CLASS_MENU_ITEM

  ' JBP 24-JAN-2014
  Dim CAGE_Configuration As CLASS_MENU_ITEM
  Dim Gaming_Tables_Configuration As CLASS_MENU_ITEM
  Dim Gaming_Tables_Params As CLASS_MENU_ITEM

  ' SMN 31-JAN-2014
  Dim Gaming_Tables_Cancellations As CLASS_MENU_ITEM

  ' AMF 09-APR-2014
  Dim m_terminal_status As CLASS_MENU_ITEM


  Dim m_software_validation_menu As CLASS_MENU_ITEM
  Dim m_software_validation_send As CLASS_MENU_ITEM
  Dim m_software_validation_history As CLASS_MENU_ITEM

  ' MMG 30-JUL-2013 
  Dim Custom_Alarms As CLASS_MENU_ITEM

  ' HBB 25-APR-2014
  Dim Version_Control As CLASS_MENU_ITEM

  ' JMM 07-MAY-2014
  Dim Patterns_Sel As CLASS_MENU_ITEM

  ' HBB & DRV 12-MAY-2014
  Dim m_massive_collections As CLASS_MENU_ITEM

  ' DRV 14-MAY-2014
  Dim m_stackers_menu As CLASS_MENU_ITEM

  ' DRV 04-JUN-2014
  Dim m_bonuses_meters_comparation As CLASS_MENU_ITEM
  ' JFC 15-MAY-2014
  Dim m_table_play_sessions As CLASS_MENU_ITEM
  Dim m_table_play_sessions_edit As CLASS_MENU_ITEM

  ' DHA 05-FEB-2015
  Dim m_table_play_sessions_import As CLASS_MENU_ITEM

  ' JMM 16-JUN-2014
  Dim m_table_player_movements As CLASS_MENU_ITEM

  ' LTC 16-FEB-2016
  Dim m_chips_sel As CLASS_MENU_ITEM

  ' JMM 05-AUG-2014
  Dim Progressive_jackpots_menu As CLASS_MENU_ITEM
  Dim Progressive_jackpots_sel As CLASS_MENU_ITEM
  Dim Provisions As CLASS_MENU_ITEM
  Dim Provisions_report As CLASS_MENU_ITEM
  Dim Progressive_jackpots_paid_report As CLASS_MENU_ITEM

  ' LEM 08-AUG-2014
  Dim m_terminal_denominations_menu As CLASS_MENU_ITEM

  ' SGB 18-AUG-2014
  Dim m_counter_configuration As CLASS_MENU_ITEM

  'JML 01-SEP-2014
  Dim collection_by_machine_and_denomination As CLASS_MENU_ITEM

  ' DHA 04-SEP-2014
  Dim m_player_tracking_income_report As CLASS_MENU_ITEM

  Dim m_player_tracking_global_drop_hourly As CLASS_MENU_ITEM

  Dim m_table_payers_report As CLASS_MENU_ITEM

  'JML 09-SEP-2014
  Dim collection_by_machine_and_date As CLASS_MENU_ITEM

  ' OPC 16-SEP-2014
  Dim m_cage_sources_target_concepts As CLASS_MENU_ITEM

  ' HBB 18-SEP-2014
  Dim m_Cage_Meters As CLASS_MENU_ITEM

  ' JBC 19-SEP-2014
  Dim m_bank_transaction_data As CLASS_MENU_ITEM

  ' RRR 23-SEP-2014
  Dim m_cage_global_report As CLASS_MENU_ITEM

#If DEBUG Then
  ' HBB 20-OCT-2014
  Dim m_terminal_softcount As CLASS_MENU_ITEM
#End If

  ' FJC 23-OCT-2014
  Dim Short_over_report As CLASS_MENU_ITEM

  ' DRV 20-JUL-2015
  Dim m_cash_monitor As CLASS_MENU_ITEM

  ' DCS 17-NOV-2014
  Dim m_mobile_bank_sel As CLASS_MENU_ITEM

  ' DLL 19-Nov-2014
  'Dim m_terminal_sas_meters_edit As CLASS_MENU_ITEM

  ' DHA 19-NOV-2014
  Dim Terminals_configuration_menu As CLASS_MENU_ITEM
  Dim Services_menu As CLASS_MENU_ITEM
  Dim Alarms_configuration_menu As CLASS_MENU_ITEM
  Dim Statistics_terminals_menu As CLASS_MENU_ITEM
  Dim Statistics_customers_menu As CLASS_MENU_ITEM
  Dim Statistics_play_sessions_menu As CLASS_MENU_ITEM
  Dim Cash_configuration_menu As CLASS_MENU_ITEM
  Dim Customers_configuration_menu As CLASS_MENU_ITEM
  Dim Marketing_promotions_menu As CLASS_MENU_ITEM
  Dim Marketing_raffles_menu As CLASS_MENU_ITEM
  Dim Marketing_loyalty_program_menu As CLASS_MENU_ITEM
  Dim Redeem_Kiosk As CLASS_MENU_ITEM

  ' ATB 24-JUL-2017
  Dim Machine_reservation_menu As CLASS_MENU_ITEM

  Dim m_window_menu As CLASS_WINDOW_MENU

  Dim Tax_report As CLASS_MENU_ITEM

  Dim Cashier_Send As CLASS_MENU_ITEM
  Dim Cashier_Collection As CLASS_MENU_ITEM

  ' DRV 17-DEC-2014
  Dim m_TITO_ticket_out_report As CLASS_MENU_ITEM

  ' JMV 23-DEC-2014
  Dim Machine_and_game_report_menu As CLASS_MENU_ITEM

  ' OPC 23-JAN-2015
  Dim Flash_Report As CLASS_MENU_ITEM
  ' DRV 05-FEB-2015
  Dim m_terminals_with_stacker As CLASS_MENU_ITEM

  'SGB 20-FEB-2015
  'YNM 08-JUL-2015
  Dim one_money_request_menu As CLASS_MENU_ITEM
  Dim more_money_request_menu As CLASS_MENU_ITEM
  Dim one_money_collect_menu As CLASS_MENU_ITEM
  Dim more_money_collect_menu As CLASS_MENU_ITEM

  ' DHA 08-APR-2015
  Dim HistoryMetersEdit As CLASS_MENU_ITEM

  ' FJC 04-MAY-2015
  Dim m_collection_file_sel As CLASS_MENU_ITEM

  ' XCD 11-MAY-2015
  Dim m_pcd_configuration_sel As CLASS_MENU_ITEM

  'FAV 20-MAY-2015
  Dim m_win_control_report As CLASS_MENU_ITEM

  'FOS 16-JUL-2015
  Dim m_purchase_and_sales As CLASS_MENU_ITEM

  'FOS 21-JUL-2014
  Dim m_ticket_configuration As CLASS_MENU_ITEM

  'SGB 07-JUL-2015
  Dim m_terminals_block As CLASS_MENU_ITEM

  'AMF 20-JUL-2015
  Dim m_customers_playing As CLASS_MENU_ITEM

  'ECP 01-OCT-2015
  Dim m_comparation_manual_pays As CLASS_MENU_ITEM

  'RGR 02-OCT-2015
  Dim m_tito_payment_report As CLASS_MENU_ITEM

  'JRC 14-OCT-2015		
  Dim m_gamegateway_terminals As CLASS_MENU_ITEM

  'JRC 22-OCT-2015
  Dim m_smartFloor_menu As CLASS_MENU_ITEM

  Dim m_smartfloor_report_runners As CLASS_MENU_ITEM

  'GDA 27-MAR-2017
  Dim m_intellia_floor_editor As CLASS_MENU_ITEM

  'GDA 27-MAR-2017
  Dim m_intellia_filter_editor As CLASS_MENU_ITEM

  'SGB 07-JUL-2015
  Dim m_smartfloor_report_task As CLASS_MENU_ITEM

  'JRC 16-NOV-2015 
  Dim m_smartfloor_terminals_print_QR_Codes As CLASS_MENU_ITEM

  'RGR 27-NOV-2015
  Dim m_terminal_equity_report As CLASS_MENU_ITEM

  'JRC 09-DIC-2015
  Dim m_report_buckets_by_client As CLASS_MENU_ITEM

  'JCA 28-DIC-2015
  Dim m_gamegateway_menu As CLASS_MENU_ITEM

  'JCA 28-DIC-2015
  Dim m_customer_entrance_price As CLASS_MENU_ITEM

  'ADI 28-JAN-2016
  Dim m_customer_entrance_report_visits As CLASS_MENU_ITEM

  'EOR 09-MAY-2016
  Dim m_activity_tables_report As CLASS_MENU_ITEM

  'YNM 24-MAR-2016
  Dim CountR_Sel As CLASS_MENU_ITEM

  'ADI 30-MAR-2016
  Dim m_countr_sessions As CLASS_MENU_ITEM

  'ESE 15-JUN-2016
  Dim m_generic_reports_system As CLASS_MENU_ITEM
  Dim m_generic_reports_terminals As CLASS_MENU_ITEM
  Dim m_generic_reports_monitor As CLASS_MENU_ITEM
  Dim m_generic_reports_alarms As CLASS_MENU_ITEM
  Dim m_generic_reports_statistics As CLASS_MENU_ITEM
  Dim m_generic_reports_cash As CLASS_MENU_ITEM
  Dim m_generic_reports_cage As CLASS_MENU_ITEM
  Dim m_generic_reports_accounting As CLASS_MENU_ITEM
  Dim m_generic_reports_customers As CLASS_MENU_ITEM
  Dim m_generic_reports_marketing As CLASS_MENU_ITEM
  Dim m_generic_reports_jackpot As CLASS_MENU_ITEM
  Dim m_generic_reports_gaming_table As CLASS_MENU_ITEM
  Dim m_generic_reports_smartfloor As CLASS_MENU_ITEM
  Dim m_generic_reports_gateway As CLASS_MENU_ITEM
  Dim m_generic_reports_software As CLASS_MENU_ITEM
  Dim m_generic_reports_tools As CLASS_MENU_ITEM
  Dim _obj_report_tool As GenericReport.IReportToolConfigService ' Before create the menu check the location/configuration of the report tool

  'JRC 13-JUL-2016
  Dim m_customer_entrance_barred_list As CLASS_MENU_ITEM

  'RGR 14-JUL-2016
  Dim m_banking_report As CLASS_MENU_ITEM

  ' LA 25-JUL-2016
  Dim m_backend_view As CLASS_MENU_ITEM

  ' LTC 05-AGO-2016
  Dim m_banking_reconciliation As CLASS_MENU_ITEM
  Dim bank_transactions_menu As CLASS_MENU_ITEM

  ' ESE 12-AUG-2016
  Dim m_booking_terminals As CLASS_MENU_ITEM

  ' ATB 24-JUL-2017
  Dim m_reservation_terminals_monitor As CLASS_MENU_ITEM

  'EOR 15-AUG-2016
  Dim m_reserved_maquines As CLASS_MENU_ITEM


  'XGJ 09-AGO-2016
  Dim Marketing_buckets_menu As CLASS_MENU_ITEM
  Dim Menu_Buckets As CLASS_MENU_ITEM

  ' FJC 23-SEP-2016
  Dim m_menu_reception As CLASS_MENU_ITEM

  'JRC 26-OCT-2016
  Dim m_ticket_audit_report As CLASS_MENU_ITEM

  ' XGJ 24-GEN-2017
  'XGJ TODO
  Dim Terminal_Draw_Summary As CLASS_MENU_ITEM

  ' MS  14-MAR-2017
  Dim Creditline_menu As CLASS_MENU_ITEM
  Dim Form_Creditline_sel As CLASS_MENU_ITEM
  Dim Form_Creditline_report As CLASS_MENU_ITEM

  ' AMF 30-MAR-2017
  Dim m_menu_junkets As CLASS_MENU_ITEM
  Dim m_junkets As CLASS_MENU_ITEM
  Dim m_junkets_representatives As CLASS_MENU_ITEM
  ' MS  26-APR-2017
  Dim m_junkets_report As CLASS_MENU_ITEM
  ' DPC 26-APR-2017
  Dim m_junkets_report_flyers As CLASS_MENU_ITEM
  ' DPC  09-SEP-2017
  Dim CAGE_FBM_LOG_sel As CLASS_MENU_ITEM

  ' RLO 30-MAR-2017
  Dim m_menu_jackpot As CLASS_MENU_ITEM
  Dim m_menu_jackpot_history_meters As CLASS_MENU_ITEM
  Dim m_menu_jackpot_history As CLASS_MENU_ITEM
  Dim m_menu_jackpot_viewers As CLASS_MENU_ITEM

  ' CSR 01-AGO-2017
  Dim m_winup_dynamic_menu_items As List(Of CLASS_MENU_ITEM)

  'EOR 01-SEP-2017
  Dim m_last_six_months_report As CLASS_MENU_ITEM

  'EOR 26-SEP-2017
  Dim m_tables_analisys_report As CLASS_MENU_ITEM

  'DPC 11-NOV-2017
  Dim Form_threshold_movements As CLASS_MENU_ITEM

  'RGR 28-NOV-2017
  Dim m_software As CLASS_MENU_ITEM
  Dim m_tools As CLASS_MENU_ITEM

  'FGB 15-DEC-2017
  Dim m_lottery_meters_adjustment_meters As CLASS_MENU_ITEM

  ' JBM 18-DEC-2017
  Dim m_billing_menu As CLASS_MENU_ITEM
  Dim m_lottery_meters_adjustment_working_day As CLASS_MENU_ITEM
  Dim m_report_profit_day As CLASS_MENU_ITEM
  Dim m_report_profit_month As CLASS_MENU_ITEM
  Dim m_report_profit_machine_and_day As CLASS_MENU_ITEM

  ' JGC 11-JUN-2018
  Dim m_cash_out_report As CLASS_MENU_ITEM

#End Region

#Region " Must Override Methods: frm_base_mdi "

  ' PURPOSE: Set initial GUI permissions.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '       - Perm: Permissions set.
  '
  ' RETURNS:
  Protected Overrides Sub GUI_Permissions(ByRef Perm As GUI_Controls.CLASS_GUI_USER.TYPE_PERMISSIONS)
    Perm.Read = True
    Perm.Write = False
    Perm.Delete = False
    Perm.Execute = False
  End Sub ' GUI_Permissions

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_MAIN

    ' TJG 01-FEB-2005
    ' Set the form icon from the embedded resource (ICO file must be built in the project as "Embedded Resource")
    ' Call could be made as GetManifestResourceStream("GUI_Auditor.GUI_Auditor.ico"))

    '------------------------------------------------
    ' XVV 13/04/2007
    ' Translate tu BASE Form
    ' Me.Icon = New Icon(System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream(Me.GetType(), "WigosGUI.ico"))
    '------------------------------------------------

    '------------------------------------------------
    'XVV 13/04/2007
    'Call Base Form proc
    Call MyBase.GUI_SetFormId()
    '------------------------------------------------

  End Sub ' GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    ' Must set before MyBase.GUI_InitControls()
    m_elp_mode = GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode")

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    ' Initialize control to keep form title. A control is used to manage event update
    lbl_main_title.Value = ""

    AddHandler WGDB.OnStatusChanged, AddressOf DatabaseStatusChanged

    'DHA 27-03-2013:
    AddHandler WGDB.OnMultiSiteStatusChanged, AddressOf MultiSiteStatusChanged
    ' RCI & MPO 02-APR-2012
    AddHandler Users.AutoLogout, AddressOf OnAutoLogout

    ' Add profiles for all forms on the GUI
    ProfilesMenuItem.Status = CurrentUser.Permissions(ProfilesMenuItem.FormId).Read

    TerminalsMenuItem.Status = CurrentUser.Permissions(TerminalsMenuItem.FormId).Read
    Terminals3gsMenuItem.Status = CurrentUser.Permissions(Terminals3gsMenuItem.FormId).Read
    TerminalsPendingMenuItem.Status = CurrentUser.Permissions(TerminalsPendingMenuItem.FormId).Read
    TerminalGameRelationMenuItem.Status = CurrentUser.Permissions(TerminalGameRelationMenuItem.FormId).Read
    TerminalMeterDeltaItem.Status = CurrentUser.Permissions(TerminalMeterDeltaItem.FormId).Read
    Cashier_Configuration.Status = CurrentUser.Permissions(Cashier_Configuration.FormId).Read
    Catalog_Configuration.Status = CurrentUser.Permissions(Catalog_Configuration.FormId).Read

    'App mobile section

    If WSI.Common.GeneralParam.GetBoolean("Features", "WinUP") Then
      DynamicWinupContentMenu()

      Game_Categories.Status = CurrentUser.Permissions(Game_Categories.FormId).Read
      Games_Admin.Status = CurrentUser.Permissions(Games_Admin.FormId).Read
      Aboutus.Status = CurrentUser.Permissions(Aboutus.FormId).Read
      HowToGetThere.Status = CurrentUser.Permissions(HowToGetThere.FormId).Read
      ' m_backend_view.Status = CurrentUser.Permissions(m_backend_view.FormId).Read

      'SDS 15-SEPT-2016
      Section_Schema.Status = CurrentUser.Permissions(Section_Schema.FormId).Read
      Winup_Content.Status = CurrentUser.Permissions(Winup_Content.FormId).Read
      'SDS  27-OCT-2016
      Winup_Notifications.Status = CurrentUser.Permissions(Winup_Notifications.FormId).Read
      Winup_Settings.Status = CurrentUser.Permissions(Winup_Settings.FormId).Read
    End If

    GeneralParamsMenuItem.Status = CurrentUser.Permissions(GeneralParamsMenuItem.FormId).Read
    Svc_monitor.Status = CurrentUser.Permissions(Svc_monitor.FormId).Read
    ' JGR 17-FEB-2012: Area & Bank
    AreaBank.Status = CurrentUser.Permissions(AreaBank.FormId).Read

    Auditor.Status = CurrentUser.Permissions(Auditor.FormId).Read

    ' 20-NOV-2014 DHA OBSOLETE
    'EventHistory.Status = CurrentUser.Permissions(EventHistory.FormId).Read
    'Alarms.Status = CurrentUser.Permissions(Alarms.FormId).Read

    Statistics.Status = CurrentUser.Permissions(Statistics.FormId).Read
    Statistics_games.Status = CurrentUser.Permissions(Statistics_games.FormId).Read
    Statistics_terminals.Status = CurrentUser.Permissions(Statistics_terminals.FormId).Read
    Statistics_hours_group.Status = CurrentUser.Permissions(Statistics_hours_group.FormId).Read

    'RDS 04-DIC-2015
    If WSI.Common.Misc.IsGameGatewayEnabled() Then
      Statistics_reportbonoplay.Status = CurrentUser.Permissions(Statistics_reportbonoplay.FormId).Read

      Statistics_reportbonoplay_played.Status = CurrentUser.Permissions(Statistics_reportbonoplay_played.FormId).Read
    End If

    ' DMR 30-JUL-2013: Hide the Plays -> Grouped by Account screen.
    'Statistics_cards.Status = CurrentUser.Permissions(Statistics_cards.FormId).Read
    Statistics_hours.Status = CurrentUser.Permissions(Statistics_hours.FormId).Read
    Capacity.Status = CurrentUser.Permissions(Capacity.FormId).Read
    CustomerBase.Status = CurrentUser.Permissions(CustomerBase.FormId).Read
    ' JGR 25-JAN-2012: Segmentaci�n Cliente-Proveedor
    CustomerSegmentation.Status = CurrentUser.Permissions(CustomerSegmentation.FormId).Read

    Plays_sessions.Status = CurrentUser.Permissions(Plays_sessions.FormId).Read
    If m_elp_mode = 0 Then
      Plays_sessions_points_assignment.Status = CurrentUser.Permissions(Plays_sessions_points_assignment.FormId).Read
    End If

    ' DHA 17-MAR-2014
    If Not GeneralParam.GetBoolean("WigosGUI", "HideGamingSessionAdjustment", True) Then
      Plays_sessions_edit.Status = CurrentUser.Permissions(Plays_sessions_edit.FormId).Read
    End If
    'Plays_plays.Status = CurrentUser.Permissions(Plays_plays.FormId).Read

    Ws_sessions.Status = CurrentUser.Permissions(Ws_sessions.FormId).Read
    Game_meters.Status = CurrentUser.Permissions(Game_meters.FormId).Read

    ' DHA 08-APR-2015
    HistoryMetersEdit.Status = CurrentUser.Permissions(HistoryMetersEdit.FormId).Read

    Card_movements.Status = CurrentUser.Permissions(Card_movements.FormId).Read
    Card_summary.Status = CurrentUser.Permissions(Card_summary.FormId).Read
    ' RRB 22-AUG-2012 Permissions to card details
    Card_details.Status = CurrentUser.Permissions(Card_details.FormId).Read

    'YNM 14-DIC-2015
    If WSI.Common.Misc.IsReceptionEnabled() Then
      Customer_Visits.Status = CurrentUser.Permissions(Customer_Visits.FormId).Read
      Blacklist_Import.Status = CurrentUser.Permissions(Blacklist_Import.FormId).Read

      'ADI 28-JAN-2016 Product Backlog Item 6649:Visitas / Recepci�n: Informes
      m_customer_entrance_report_visits.Status = CurrentUser.Permissions(m_customer_entrance_report_visits.FormId).Read

      'FJC 20-JAN-2016 Product Backlog Item 8554:Visitas / Recepci�n: Crear ticket de entrada: GUI precios entrada
      If WSI.Common.Entrances.Entrance.GetReceptionMode = Entrances.Enums.ENUM_RECEPTION_MODE.TicketWithPrice Then
        m_customer_entrance_price.Status = CurrentUser.Permissions(m_customer_entrance_price.FormId).Read
      End If

      'JRC 13-JUL-2106
      m_customer_entrance_barred_list.Status = CurrentUser.Permissions(m_customer_entrance_barred_list.FormId).Read
    End If

    'Machine_denom_meters.Status = CurrentUser.Permissions(Machine_denom_meters.FormId).Read
    Cashier_movements.Status = CurrentUser.Permissions(Cashier_movements.FormId).Read
    Cashier_sessions.Status = CurrentUser.Permissions(Cashier_sessions.FormId).Read
    Acct_taxes.Status = CurrentUser.Permissions(Acct_taxes.FormId).Read
    Sales_date_terminal.Status = CurrentUser.Permissions(Sales_date_terminal.FormId).Read
    Provider_day_terminal.Status = CurrentUser.Permissions(Provider_day_terminal.FormId).Read
    Provider_day_terminal_statistics.Status = CurrentUser.Permissions(Provider_day_terminal_statistics.FormId).Read

#If DEBUG Then
    FormCharts.Status = CurrentUser.Permissions(FormCharts.FormId).Read
#End If

    Expired_Credits.Status = CurrentUser.Permissions(Expired_Credits.FormId).Read

    If GeneralParam.GetBoolean("WinLossStatement", "Enabled", False) Then
      ACCOUNT_Statement_print.Status = CurrentUser.Permissions(ACCOUNT_Statement_print.FormId).Read
      ACCOUNT_Statement_document_edit.Status = CurrentUser.Permissions(ACCOUNT_Statement_document_edit.FormId).Read
    End If

    Menu_Handpays.Status = CurrentUser.Permissions(Menu_Handpays.FormId).Read

    'HBB 07-OCT-2014 if system is in tito mode the mobile banc movement form can not be visible
    If WSI.Common.Misc.IsCashlessMode() Then
      MB_Movements.Status = CurrentUser.Permissions(MB_Movements.FormId).Read
    End If

    ' RCI 03-JUN-2014: Hide TaxReport
    If Not GeneralParam.GetBoolean("WigosGUI", "HideTaxReport", True) Then
      Taxes_report.Status = CurrentUser.Permissions(Taxes_report.FormId).Read
    End If

    ' "Clase II"
    ' RCI 03-DEC-2010: Not used. Out for now.
    'Class_II_draw_audit.Status = CurrentUser.Permissions(Class_II_draw_audit.FormId).Read
    Jackpot_configuration.Status = CurrentUser.Permissions(Jackpot_configuration.FormId).Read
    Jackpot_promo_messages.Status = CurrentUser.Permissions(Jackpot_promo_messages.FormId).Read
    Jackpot_history.Status = CurrentUser.Permissions(Jackpot_history.FormId).Read
    Jackpot_monitor.Status = CurrentUser.Permissions(Jackpot_monitor.FormId).Read

    ' "Site Jackpot"  
    Site_jackpot_configuration.Status = CurrentUser.Permissions(Site_jackpot_configuration.FormId).Read
    Site_jackpot_history.Status = CurrentUser.Permissions(Site_jackpot_history.FormId).Read

    ' "New form to configure Jackpots  with treeview. TODO FOS: to located in the Menu
    If JackpotBusinessLogic.IsJackpotsEnabled() Then
      m_menu_jackpot.Status = CurrentUser.Permissions(m_menu_jackpot.FormId).Read
      m_menu_jackpot_history_meters.Status = CurrentUser.Permissions(m_menu_jackpot_history_meters.FormId).Read
      m_menu_jackpot_history.Status = CurrentUser.Permissions(m_menu_jackpot_history.FormId).Read
      m_menu_jackpot_viewers.Status = CurrentUser.Permissions(m_menu_jackpot_viewers.FormId).Read
    End If

    If WSI.Common.GeneralParam.GetBoolean("SiteJackpot", "Bonusing.Enabled") Then
      Site_bonuses.Status = CurrentUser.Permissions(Site_bonuses.FormId).Read
      m_bonuses_meters_comparation.Status = CurrentUser.Permissions(m_bonuses_meters_comparation.FormId).Read
    End If

    ' "SW Download"
    Sw_download_versions.Status = CurrentUser.Permissions(Sw_download_versions.FormId).Read
    Terminals_sw_version.Status = CurrentUser.Permissions(Terminals_sw_version.FormId).Read
    Terminals_sw_version_monitor.Status = CurrentUser.Permissions(Terminals_sw_version_monitor.FormId).Read
    Misc_sw_build.Status = CurrentUser.Permissions(Misc_sw_build.FormId).Read
    Licences.Status = CurrentUser.Permissions(Licences.FormId).Read

    ' PGJ 21-JUN-2013
    If GLB_CurrentUser.IsSuperUser Then
      ' "Herramientas"
      Magnetic_card_writer.Status = CurrentUser.Permissions(Magnetic_card_writer.FormId).Read
    End If

    ' Player_tracking_players.Status = CurrentUser.Permissions(Player_tracking_players.FormId).Read
    Menu_Promotions.Status = CurrentUser.Permissions(Menu_Promotions.FormId).Read
    ' JMM 11-JUN-2012
    Menu_Periodical_Promotions.Status = CurrentUser.Permissions(Menu_Promotions.FormId).Read
    Menu_Preassigned_Promotions.Status = CurrentUser.Permissions(Menu_Promotions.FormId).Read

    Menu_GiftsCatalog.Status = CurrentUser.Permissions(Menu_GiftsCatalog.FormId).Read
    Menu_GiftsHistory.Status = CurrentUser.Permissions(Menu_GiftsHistory.FormId).Read

    ' XCD 22-AUG-2012
    Menu_Flags.Status = CurrentUser.Permissions(Menu_Flags.FormId).Read
    Account_Flags.Status = CurrentUser.Permissions(Account_Flags.FormId).Read

    ' JML 27-JUL-2012: Exclude porvider
    Provider.Status = CurrentUser.Permissions(Provider.FormId).Read

    ' RCI 10-JUN-2010: Draws
    Menu_Draws.Status = CurrentUser.Permissions(Menu_Draws.FormId).Read

    ' RCI 18-JUN-2010: Commands
    Commands_send.Status = CurrentUser.Permissions(Commands_send.FormId).Read
    Commands_history.Status = CurrentUser.Permissions(Commands_history.FormId).Read

    ' DRV 09-APR-2014: Software Validation
    If WSI.Common.Misc.SystemMode() = SYSTEM_MODE.WASS Then
      m_software_validation_history.Status = CurrentUser.Permissions(m_software_validation_history.FormId).Read
      m_software_validation_send.Status = CurrentUser.Permissions(m_software_validation_send.FormId).Read
    End If

    ' RCI 20-DEC-2010: Terminals without activity
    Terminals_without_activity.Status = CurrentUser.Permissions(Terminals_without_activity.FormId).Read

    'JML 27-JUL-2012: Frame viewer
    If WSI.Common.Misc.ReadGeneralParams("ExternalProtocol", "Enabled") Then
      Peru_Frames.Status = CurrentUser.Permissions(Peru_Frames.FormId).Read
    End If

    ' RCI 22-DEC-2010: Automatic Mailing Programming
    Mailing_Programming.Status = CurrentUser.Permissions(Mailing_Programming.FormId).Read

    ' MBF 13-DEC-2010
    Gap_report.Status = CurrentUser.Permissions(Gap_report.FormId).Read
    Gap_report_statistics.Status = CurrentUser.Permissions(Gap_report_statistics.FormId).Read

    ' JML 23-MAY-2012
    ' DHA 01-FEB-2016: when dual currency is enabled hide form
    If Not WSI.Common.Misc.IsFloorDualCurrencyEnabled() Then
      Cash_report.Status = CurrentUser.Permissions(Cash_report.FormId).Read
    End If

    ' JMM 20-JAN-2012
    Alarms.Status = CurrentUser.Permissions(Alarms.FormId).Read
    Tf_Gap_report.Status = CurrentUser.Permissions(Tf_Gap_report.FormId).Read
    MachineOcupation.Status = CurrentUser.Permissions(MachineOcupation.FormId).Read

    ' JMM 23-JAN-2012
    Prize_taxes_report.Status = CurrentUser.Permissions(Prize_taxes_report.FormId).Read

    ' SSC 09-MAY-2012
    If WSI.Common.Misc.IsNoteAcceptorEnabled() Then
      ' SSC 03-FEB-2012
      Terminal_money.Status = CurrentUser.Permissions(Terminal_money.FormId).Read
      ' SSC 08-FEB-2012
      Money_pending_transfer.Status = CurrentUser.Permissions(Money_pending_transfer.FormId).Read
      ' SSC 17- ABR-2012
      Money_Collections.Status = CurrentUser.Permissions(Money_Collections.FormId).Read
      ' JML 27-JUL-2012 : Currencies aceptator
      ' DHA 12-NOV-2014 : set only when note acceptor is enabled
      Currencies.Status = CurrentUser.Permissions(Currencies.FormId).Read
    End If

    ' JAB 14-MAR-2012
    Form_report.Status = CurrentUser.Permissions(Form_report.FormId).Read

    ' JMV 12-APR-2016
    If Not GeneralParam.GetBoolean("WigosGUI", "HideTITOHoldvsWinReport", True) AndAlso TITO.Utils.IsTitoMode() Then
      Tito_holdvswin_report.Status = CurrentUser.Permissions(Tito_holdvswin_report.FormId).Read
    End If

    ' JAB 28-MAR-2012
    Voucher_browser.Status = CurrentUser.Permissions(Voucher_browser.FormId).Read

    If WSI.Common.Misc.IsCashlessMode() Then
      ' MPO 14-MAR-2012
      Detailed_prizes.Status = CurrentUser.Permissions(Detailed_prizes.FormId).Read
    End If

    ' DDM 14-MAR-2012
    Productivity_reports.Status = CurrentUser.Permissions(Productivity_reports.FormId).Read
    ' DDM 05-APR-2012
    Segmentation_reports.Status = CurrentUser.Permissions(Segmentation_reports.FormId).Read
    ' DDM 27-JUN-2012
    Promotion_Report.Status = CurrentUser.Permissions(Promotion_Report.FormId).Read
    ' DDM 28-AUG-2012
    Promotion_Delivered_Report.Status = CurrentUser.Permissions(Promotion_Delivered_Report.FormId).Read

    ' RXM 01-AGO-2012
    Accounts_Import.Status = CurrentUser.Permissions(Accounts_Import.FormId).Read

    Advertisement.Status = CurrentUser.Permissions(Advertisement.FormId).Read
    AdsConfiguration.Status = CurrentUser.Permissions(AdsConfiguration.FormId).Read

    ' JAB 22-JUL-2014
    LcdConfiguration.Status = CurrentUser.Permissions(LcdConfiguration.FormId).Read

    ' DHA 01-DEC-2014
    LcdMessageSel.Status = CurrentUser.Permissions(LcdMessageSel.FormId).Read

    ' HBB 02-AUG-2012
    Password_Configuration.Status = CurrentUser.Permissions(Password_Configuration.FormId).Read

    If WSI.Common.Misc.IsCashlessMode() Then
      ' RCI 06-JUN-2011
      Tickets_report.Status = CurrentUser.Permissions(Tickets_report.FormId).Read

      ' JBC 26-NOV-2014
      Tax_report.Status = CurrentUser.Permissions(Tax_report.FormId).Read
    End If

    'JAB 25-MAR-2013
    Report_draw_tickets.Status = CurrentUser.Permissions(Report_draw_tickets.FormId).Read

    If WSI.Common.Misc.IsCashlessMode() Then
      ' HBB 13-SEP-2012
      Cash_By_Service.Status = CurrentUser.Permissions(Cash_By_Service.FormId).Read

      ' JML 20-SEP-2012
      Retreats_report.Status = CurrentUser.Permissions(Retreats_report.FormId).Read

      ' AMF 28-JUN-2013
      Service_Charge_report.Status = CurrentUser.Permissions(Service_Charge_report.FormId).Read
    End If

    ' HBB 27-SEP-2012
    Stats_Adjustment.Status = CurrentUser.Permissions(Stats_Adjustment.FormId).Read

    'HBB 14-JAN-2013
    PaymentOrdersConfig.Status = CurrentUser.Permissions(PaymentOrdersConfig.FormId).Read

    ' LEM 15-JAN-2013
    Play_preferences.Status = CurrentUser.Permissions(Play_preferences.FormId).Read

    'HBB 17-JAN-2013
    Payment_Orders.Status = CurrentUser.Permissions(Payment_Orders.FormId).Read

    ' JBC 19-SEP-2014
    m_bank_transaction_data.Status = CurrentUser.Permissions(m_bank_transaction_data.FormId).Read

    'RRB 23-JAN-2013
    Cash_Summary.Status = CurrentUser.Permissions(Cash_Summary.FormId).Read

    ' JAB 06-NOV-2013
    If Cage.IsCageEnabled Then
      CAGE_Amounts.Status = CurrentUser.Permissions(CAGE_Amounts.FormId).Read
      CAGE_Movements_sel.Status = CurrentUser.Permissions(CAGE_Movements_sel.FormId).Read
      CAGE_New.Status = IIf(CurrentUser.Permissions(CAGE_New.FormId).Read And Not GLB_CurrentUser.IsSuperUser, True, False)
      CAGE_Sessions.Status = CurrentUser.Permissions(CAGE_Sessions.FormId).Read
      CAGE_Source_Target.Status = CurrentUser.Permissions(CAGE_Source_Target.FormId).Read
      CAGE_CashCageCount.Status = IIf(CurrentUser.Permissions(CAGE_CashCageCount.FormId).Read And Not GLB_CurrentUser.IsSuperUser, True, False)
      CAGE_CashCageStock.Status = CurrentUser.Permissions(CAGE_CashCageStock.FormId).Read

      ' AMF 10-APR-2015

      CAGE_Safe_Keeping_sel.Status = CurrentUser.Permissions(CAGE_Safe_Keeping_sel.FormId).Read
      CAGE_Safe_Keeping_movement_sel.Status = CurrentUser.Permissions(CAGE_Safe_Keeping_movement_sel.FormId).Read


      ' OPC 16-SEP-2014
      m_cage_sources_target_concepts.Status = CurrentUser.Permissions(m_cage_sources_target_concepts.FormId).Read

      m_Cage_Meters.Status = CurrentUser.Permissions(m_Cage_Meters.FormId).Read
      m_cage_global_report.Status = CurrentUser.Permissions(m_cage_global_report.FormId).Read
    End If

    ' DHA 26-NOV-2014 Added for TITO and Cashless stackers
    If WSI.Common.Misc.IsNoteAcceptorEnabled() Or TITO.Utils.IsTitoMode() Then
      m_stacker_collection_sel.Status = CurrentUser.Permissions(m_stacker_collection_sel.FormId).Read

      ' DHA 01-FEB-2016: when dual currency is enabled hide form
      If Not WSI.Common.Misc.IsFloorDualCurrencyEnabled() Then
        m_collection_file_sel.Status = CurrentUser.Permissions(m_collection_file_sel.FormId).Read
      End If

      ' HBB & DRV 12-MAY-2014
      m_massive_collections.Status = IIf(CurrentUser.Permissions(m_massive_collections.FormId).Read And Not GLB_CurrentUser.IsSuperUser, True, False)

      ' DRV 09-JAN-2014
      m_terminals_with_stacker.Status = CurrentUser.Permissions(m_terminals_with_stacker.FormId).Read
    End If

    ' DLL 14-NOV-2013
    Skipped_Meters_Report.Status = CurrentUser.Permissions(Skipped_Meters_Report.FormId).Read

    ' JBM 05-MAR-2018
    'Meters_Comparison.Status = CurrentUser.Permissions(Meters_Comparison.FormId).Read

    ' QMP 20-FEB-2013
    Operation_Schedule.Status = CurrentUser.Permissions(Operation_Schedule.FormId).Read

    ' JMM 12-MAR-2013
    MultiSite_Config.Status = CurrentUser.Permissions(MultiSite_Config.FormId).Read

    If GeneralParam.GetBoolean("Site", "MultiSiteMember") Then
      MultiSite_Monitor.Status = CurrentUser.Permissions(MultiSite_Monitor.FormId).Read
    End If

    If Not GeneralParam.GetBoolean("Site", "Configured") Then
      m_site_config_Click(Me, Nothing)
    End If

    'SMN 25-MAR-2013
    Machine_Alias.Status = CurrentUser.Permissions(Machine_Alias.FormId).Read

    'DMR 30-APR-2013
    Providers_Games.Status = CurrentUser.Permissions(Providers_Games.FormId).Read

    'AMF 30-JUL-2013
    Currency_Configuration.Status = CurrentUser.Permissions(Currency_Configuration.FormId).Read

    'DLL 19-JUN-2013 
    Currency_Exchange_Audit.Status = CurrentUser.Permissions(Currency_Exchange_Audit.FormId).Read

    'RBG 25-JUN-2013
    Menu_Groups.Status = CurrentUser.Permissions(Menu_Groups.FormId).Read

    If WSI.Common.Misc.IsCashlessMode() Then
      ' RRR 22-JUL-2013
      Money_Laundering_Config.Status = CurrentUser.Permissions(Money_Laundering_Config.FormId).Read
    End If

    ' RRR 05-AUG-2013
    Player_Edit_Config.Status = CurrentUser.Permissions(Player_Edit_Config.FormId).Read

    If WSI.Common.Misc.IsCashlessMode() Then
      'MMG 08-JUL-2013
      Draws_cashdesk_detail.Status = CurrentUser.Permissions(Draws_cashdesk_detail.FormId).Read

      'MMG 16-JUL-2013
      Draws_cashdesk_summary.Status = CurrentUser.Permissions(Draws_cashdesk_summary.FormId).Read

      If Not GeneralParam.GetBoolean("AntiMoneyLaundering", "ExternalControl.Enabled", False) Then
        Money_Laundering_Report_Prize.Status = CurrentUser.Permissions(Money_Laundering_Report_Prize.FormId).Read
        Money_Laundering_Report_Recharge.Status = CurrentUser.Permissions(Money_Laundering_Report_Recharge.FormId).Read
      End If

      'MMG 30-JUL-2013
      Cashdesk_Draws_Configuration.Status = CurrentUser.Permissions(Cashdesk_Draws_Configuration.FormId).Read
      'XGJ TODO
      If (WSI.Common.Misc.IsFeatureTerminalDrawEnabled()) Then
        Terminal_Draws_Configuration.Status = CurrentUser.Permissions(Terminal_Draws_Configuration.FormId).Read
      End If

      'ESE 14-SEP-2016
      'LTC 07-OCT-2016
      If GamingTableBusinessLogic.IsGamingTablesEnabled Then
        Cashdesk_Draws_Tables_Configuration.Status = CurrentUser.Permissions(Cashdesk_Draws_Tables_Configuration.FormId).Read
      End If
    End If

    m_ticket_configuration.Status = CurrentUser.Permissions(m_ticket_configuration.FormId).Read

    'JBC 16-APR-2014
    Custom_Alarms.Status = CurrentUser.Permissions(Custom_Alarms.FormId).Read

    ' JBC 17-SEP-2013
    If Not GeneralParam.GetBoolean("Site", "MultiSiteMember") Then
      Points_import.Status = CurrentUser.Permissions(Points_import.FormId).Read
    End If

    'RMS 21-10-2013
    Account_Card_Changes.Status = CurrentUser.Permissions(Account_Card_Changes.FormId).Read

    'JBP 16-DEC-2013
    'DHA 12-NOV-2014 : modified permissions for gaming table forms
    If GamingTableBusinessLogic.IsFeaturesGamingTablesEnabled Then
      Gaming_Tables_Params.Status = CurrentUser.Permissions(Gaming_Tables_Params.FormId).Read

      If GamingTableBusinessLogic.IsGamingTablesEnabled Then
        Gaming_Tables.Status = CurrentUser.Permissions(Gaming_Tables.FormId).Read
        Gaming_Tables_Types.Status = CurrentUser.Permissions(Gaming_Tables_Types.FormId).Read

        Gaming_Tables_Income_Report.Status = CurrentUser.Permissions(Gaming_Tables_Income_Report.FormId).Read
        Gaming_Tables_Session.Status = CurrentUser.Permissions(Gaming_Tables_Session.FormId).Read
        Chips_Operations_Report.Status = CurrentUser.Permissions(Chips_Operations_Report.FormId).Read           ' RMS 09-JAN-2014
        Gaming_Tables_Cancellations.Status = CurrentUser.Permissions(Gaming_Tables_Cancellations.FormId).Read   ' SMN 31-JAN-2014

        'EOR 14-JUN-2016
#If DEBUG Then
        m_activity_tables_report.Status = CurrentUser.Permissions(m_activity_tables_report.FormId).Read         ' EOR 12-MAY-2016
#End If

        'EOR 26-SEP-2017
        m_tables_analisys_report.Status = CurrentUser.Permissions(m_tables_analisys_report.FormId).Read

        If GamingTableBusinessLogic.IsEnabledGTPlayerTracking Then
          ' JFC 16-MAY-2014
          m_table_play_sessions.Status = CurrentUser.Permissions(m_table_play_sessions.FormId).Read
          m_table_play_sessions_edit.Status = CurrentUser.Permissions(m_table_play_sessions_edit.FormId).Read

          ' DHA 02-FEB-2015
          m_table_play_sessions_import.Status = CurrentUser.Permissions(m_table_play_sessions_import.FormId).Read

          ' JMM 16-JUN-2014
          m_table_player_movements.Status = CurrentUser.Permissions(m_table_player_movements.FormId).Read

          'DCS 04-SEP-2014
          m_player_tracking_income_report.Status = CurrentUser.Permissions(m_player_tracking_income_report.FormId).Read

          m_player_tracking_global_drop_hourly.Status = CurrentUser.Permissions(m_player_tracking_global_drop_hourly.FormId).Read

          If WSI.Common.GamingTableBusinessLogic.IsEnabledGTPlayerTracking() And GamingTableBusinessLogic.IsEnabledGTWinLoss() Then
            Gaming_Tables_Score_Report.Status = CurrentUser.Permissions(Gaming_Tables_Score_Report.FormId).Read
          End If

          m_table_payers_report.Status = CurrentUser.Permissions(m_table_payers_report.FormId).Read
        End If

        ' FOS 16-JUL-2015 
        m_purchase_and_sales.Status = CurrentUser.Permissions(m_purchase_and_sales.FormId).Read

        'LTC 25-FEB-2016
        m_chips_sel.Status = CurrentUser.Permissions(m_chips_sel.FormId).Read
      End If
    End If

    ' ICS 18-DEC-2013
    If TITO.Utils.IsTitoMode And Not WSI.Common.Misc.IsMico2Mode() Then

      'TITO CONFIGURATION
      m_tito_params.Status = CurrentUser.Permissions(m_tito_params.FormId).Read

      'CIRCULATING CASH
      m_circulating_cash.Status = CurrentUser.Permissions(m_circulating_cash.FormId).Read

      'TICKET REPORT
      m_ticket_tito_report.Status = CurrentUser.Permissions(m_ticket_tito_report.FormId).Read

      'STACKER SELECTION
      m_stacker_sel.Status = CurrentUser.Permissions(m_stacker_sel.FormId).Read

      ' SGB 24-FEB-2015: Don't show ticket out report if we are in TITO or General Param is 1
      If Not GeneralParam.GetBoolean("WigosGUI", "HideTicketOutReport", True) Then
        m_TITO_ticket_out_report.Status = CurrentUser.Permissions(m_TITO_ticket_out_report.FormId).Read
      End If

      m_tito_collection_balance_report.Status = CurrentUser.Permissions(m_tito_collection_balance_report.FormId).Read
      m_tito_summary_sel.Status = CurrentUser.Permissions(m_tito_summary_sel.FormId).Read

      'JRC 26-10-2016
      m_ticket_audit_report.Status = CurrentUser.Permissions(m_ticket_audit_report.FormId).Read

#If DEBUG Then
      ' HBB 20-OCT-2014
      m_terminal_softcount.Status = CurrentUser.Permissions(m_terminal_softcount.FormId).Read
#End If

      'm_terminal_sas_meters_edit.Status = CurrentUser.Permissions(m_terminal_sas_meters_edit.FormId).Read
    End If

    ' Request Pin 
    AccountRequestPin.Status = CurrentUser.Permissions(AccountRequestPin.FormId).Read

    m_terminal_sas_meters_sel.Status = CurrentUser.Permissions(m_terminal_sas_meters_sel.FormId).Read

    If Not m_terminal_sas_meters_sel_advanced Is Nothing Then
      m_terminal_sas_meters_sel_advanced.Status = CurrentUser.Permissions(m_terminal_sas_meters_sel.FormId).Read
    End If

    m_terminal_status.Status = CurrentUser.Permissions(m_terminal_status.FormId).Read

    Menu_TableToPoints.Status = CurrentUser.Permissions(Menu_TableToPoints.FormId).Read

    ' JMM 07-MAY-2014
    ' JPJ 02-JUN-2014 They are only active when it's enabled
    If (GeneralParam.GetBoolean("Pattern", "Enabled", True)) Then
      Patterns_Sel.Status = CurrentUser.Permissions(Patterns_Sel.FormId).Read
    End If

    ' JMM 07-AUG-2014
    Progressive_jackpots_sel.Status = CurrentUser.Permissions(Progressive_jackpots_sel.FormId).Read
    Provisions.Status = CurrentUser.Permissions(Provisions.FormId).Read
    Provisions_report.Status = CurrentUser.Permissions(Provisions_report.FormId).Read
    Progressive_jackpots_paid_report.Status = CurrentUser.Permissions(Progressive_jackpots_paid_report.FormId).Read

    If GeneralParam.GetBoolean("Cage", "BillCounter.Enabled", True) AndAlso Cage.IsCageEnabled Then
      m_counter_configuration.Status = CurrentUser.Permissions(m_counter_configuration.FormId).Read
    End If

    ' DPC 08-SEP-2017
    If GeneralParam.GetBoolean("FBM", "Enabled", False) Then
      CAGE_FBM_LOG_sel.Status = CurrentUser.Permissions(CAGE_FBM_LOG_sel.FormId).Read
    End If

    ' LEM 08-AUG-2014
    m_terminal_denominations_menu.Status = CurrentUser.Permissions(m_terminal_denominations_menu.FormId).Read

    If TITO.Utils.IsTitoMode Then
      'JML 01-SEP-2014
      collection_by_machine_and_denomination.Status = CurrentUser.Permissions(collection_by_machine_and_denomination.FormId).Read
      'JML 09-SEP-2014
      collection_by_machine_and_date.Status = CurrentUser.Permissions(collection_by_machine_and_date.FormId).Read
    End If

    ' FJC 23-OCT-2014
    Short_over_report.Status = CurrentUser.Permissions(Short_over_report.FormId).Read

    ' DRV 20-JUL-2015
    m_cash_monitor.Status = CurrentUser.Permissions(m_cash_monitor.FormId).Read

    If (WSI.Common.Misc.IsCashlessMode()) Then
      ' JBM 06-NOV-2017
      If (Not WSI.Common.MobileBankConfig.IsMobileBankLinkedToADevice) Then
        ' DCS 17-NOV-2014
        m_mobile_bank_sel.Status = CurrentUser.Permissions(m_mobile_bank_sel.FormId).Read
      End If
    End If

    If Not Cage.IsCageAutoMode() AndAlso Cage.IsCageEnabled() Then
      Cashier_Send.Status = IIf(CurrentUser.Permissions(Cashier_Send.FormId).Read And Not GLB_CurrentUser.IsSuperUser, True, False)
      Cashier_Collection.Status = IIf(CurrentUser.Permissions(Cashier_Collection.FormId).Read And Not GLB_CurrentUser.IsSuperUser, True, False)
    End If

    ' JMV 23-DEC-2014
    ' LTC 24-NOV-2016
    'If WSI.Common.Misc.ShowMachineAndGameReport() Then
    '  Machine_and_game_report_menu.Status = CurrentUser.Permissions(Machine_and_game_report_menu.FormId).Read
    'End If

    ' OPC 23-JAN-2015
    If Not GeneralParam.GetBoolean("WigosGUI", "HideProviderCashDailyReport", True) Then
      Flash_Report.Status = CurrentUser.Permissions(Flash_Report.FormId).Read
    End If

    m_pcd_configuration_sel.Status = CurrentUser.Permissions(m_pcd_configuration_sel.FormId).Read

    'FGB 15-DEC-2017
    'AGG Lottery
    If (WSI.Common.Misc.IsAGGEnabled()) Then
      m_lottery_meters_adjustment_working_day.Status = CurrentUser.Permissions(m_lottery_meters_adjustment_working_day.FormId).Read
      m_report_profit_day.Status = CurrentUser.Permissions(m_report_profit_day.FormId).Read
      m_report_profit_month.Status = CurrentUser.Permissions(m_report_profit_month.FormId).Read
      m_report_profit_machine_and_day.Status = CurrentUser.Permissions(m_report_profit_machine_and_day.FormId).Read
    End If

    If TITO.Utils.IsTitoMode Then
      ' FAV 20-MAY-2015
      m_win_control_report.Status = CurrentUser.Permissions(m_win_control_report.FormId).Read
    End If

    m_terminals_block.Status = CurrentUser.Permissions(m_terminals_block.FormId).Read

    m_customers_playing.Status = CurrentUser.Permissions(m_customers_playing.FormId).Read

    'EOR 01-SEP-2017
    m_last_six_months_report.Status = CurrentUser.Permissions(m_last_six_months_report.FormId).Read


    If WSI.Common.Misc.IsGameGatewayEnabled() Then
      ' JRC 14-OCT-2015		Feature 4699:LottoRace
      m_gamegateway_terminals.Status = CurrentUser.Permissions(m_gamegateway_terminals.FormId).Read
    End If

    'ECP 27-OCT-2015 Backlog Item 4158 GUI - Manual pays balance report Bug 5594
    m_comparation_manual_pays.Status = CurrentUser.Permissions(m_comparation_manual_pays.FormId).Read

    'RGR 27-OCT-2015 Backlog Item 4809: Permission - "Report of Payments"
    If Tax.EnableTitoTaxWaiver(True) Then
      m_tito_payment_report.Status = CurrentUser.Permissions(m_tito_payment_report.FormId).Read
    End If

    m_window_menu.Status = True

    ' SGB 02-MAR-2015 new menu alert of money request pending
    tmr_MoneyRequestPending.Enabled = True
    tmr_MoneyRequestPending.Interval = 60000
    'tmr_MoneyRequestPending.Interval = GeneralParam.GetInt32("Cage", "TimerToSeeMoneyRequest")
    'tmr_MoneyRequestPending_Event()

    ' JRC 28-OCT-2015		
    If WSI.Common.Misc.IsIntelliaEnabled() Then
      m_intellia_floor_editor.Status = CurrentUser.Permissions(m_intellia_floor_editor.FormId).Read
      m_smartfloor_report_task.Status = CurrentUser.Permissions(m_smartfloor_report_task.FormId).Read
      m_smartfloor_report_runners.Status = CurrentUser.Permissions(m_smartfloor_report_runners.FormId).Read
      'JRC 16-NOV-2015
      m_smartfloor_terminals_print_QR_Codes.Status = CurrentUser.Permissions(m_smartfloor_terminals_print_QR_Codes.FormId).Read
      'PDM 22-JUN-2017
      m_intellia_filter_editor.Status = CurrentUser.Permissions(m_intellia_filter_editor.FormId).Read
    End If

    If (GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode", 0) = 0) Then  'Buckets SPACE
      Bucket_sel.Status = CurrentUser.Permissions(Bucket_sel.FormId).Read
      ' JBM 05-MAR-2018
      Buckets_multiplier_sel.Status = CurrentUser.Permissions(Buckets_multiplier_sel.FormId).Read
    End If

    'RGR 27-NOV-2015  Item 6451:Garcia River-03: Equity report
    If TITO.Utils.IsTitoMode Then
      m_terminal_equity_report.Status = CurrentUser.Permissions(m_terminal_equity_report.FormId).Read

    End If

    'ESE 27-MAY-2016
    '-----------------------------Check if we need to add a new item in the menu
    If _obj_report_tool.ExistLocationMenu(GenericReport.LocationMenu.SystemMenu) Then
      If Not m_generic_reports_system Is Nothing Then
        m_generic_reports_system.Status = CurrentUser.Permissions(m_generic_reports_system.FormId).Read

        If m_generic_reports_system.Status And Not _obj_report_tool.GetChildReportsPermissions(GenericReport.LocationMenu.SystemMenu) Then
          m_generic_reports_system.Status = False
        End If
      End If
    End If

    If _obj_report_tool.ExistLocationMenu(GenericReport.LocationMenu.TerminalsMenu) Then
      If Not m_generic_reports_terminals Is Nothing Then
        m_generic_reports_terminals.Status = CurrentUser.Permissions(m_generic_reports_terminals.FormId).Read

        If m_generic_reports_terminals.Status And Not _obj_report_tool.GetChildReportsPermissions(GenericReport.LocationMenu.TerminalsMenu) Then
          m_generic_reports_terminals.Status = False
        End If
      End If
    End If

    If _obj_report_tool.ExistLocationMenu(GenericReport.LocationMenu.MonitorMenu) Then
      If Not m_generic_reports_monitor Is Nothing Then
        m_generic_reports_monitor.Status = CurrentUser.Permissions(m_generic_reports_monitor.FormId).Read

        If m_generic_reports_monitor.Status And Not _obj_report_tool.GetChildReportsPermissions(GenericReport.LocationMenu.MonitorMenu) Then
          m_generic_reports_monitor.Status = False
        End If
      End If
    End If

    If _obj_report_tool.ExistLocationMenu(GenericReport.LocationMenu.AlarmsMenu) Then
      If Not m_generic_reports_alarms Is Nothing Then
        m_generic_reports_alarms.Status = CurrentUser.Permissions(m_generic_reports_alarms.FormId).Read

        If m_generic_reports_alarms.Status And Not _obj_report_tool.GetChildReportsPermissions(GenericReport.LocationMenu.AlarmsMenu) Then
          m_generic_reports_alarms.Status = False
        End If
      End If
    End If

    If _obj_report_tool.ExistLocationMenu(GenericReport.LocationMenu.StatisticsMenu) Then
      If Not m_generic_reports_statistics Is Nothing Then
        m_generic_reports_statistics.Status = CurrentUser.Permissions(m_generic_reports_statistics.FormId).Read

        If m_generic_reports_statistics.Status And Not _obj_report_tool.GetChildReportsPermissions(GenericReport.LocationMenu.StatisticsMenu) Then
          m_generic_reports_statistics.Status = False
        End If
      End If
    End If

    If _obj_report_tool.ExistLocationMenu(GenericReport.LocationMenu.CashMenu) Then
      If Not m_generic_reports_cash Is Nothing Then
        m_generic_reports_cash.Status = CurrentUser.Permissions(m_generic_reports_cash.FormId).Read

        If m_generic_reports_cash.Status And Not _obj_report_tool.GetChildReportsPermissions(GenericReport.LocationMenu.CashMenu) Then
          m_generic_reports_cash.Status = False
        End If
      End If
    End If

    If _obj_report_tool.ExistLocationMenu(GenericReport.LocationMenu.CageMenu) Then
      If Not m_generic_reports_cage Is Nothing Then
        m_generic_reports_cage.Status = CurrentUser.Permissions(m_generic_reports_cage.FormId).Read

        If m_generic_reports_cage.Status And Not _obj_report_tool.GetChildReportsPermissions(GenericReport.LocationMenu.CageMenu) Then
          m_generic_reports_cage.Status = False
        End If
      End If
    End If

    If _obj_report_tool.ExistLocationMenu(GenericReport.LocationMenu.AccountingMenu) Then
      If Not m_generic_reports_accounting Is Nothing Then
        m_generic_reports_accounting.Status = CurrentUser.Permissions(m_generic_reports_accounting.FormId).Read

        If m_generic_reports_accounting.Status And Not _obj_report_tool.GetChildReportsPermissions(GenericReport.LocationMenu.AccountingMenu) Then
          m_generic_reports_accounting.Status = False
        End If
      End If
    End If

    If _obj_report_tool.ExistLocationMenu(GenericReport.LocationMenu.CustomerMenu) Then
      If Not m_generic_reports_customers Is Nothing Then
        m_generic_reports_customers.Status = CurrentUser.Permissions(m_generic_reports_customers.FormId).Read

        If m_generic_reports_customers.Status And Not _obj_report_tool.GetChildReportsPermissions(GenericReport.LocationMenu.CustomerMenu) Then
          m_generic_reports_customers.Status = False
        End If
      End If
    End If

    If _obj_report_tool.ExistLocationMenu(GenericReport.LocationMenu.MarketingMenu) Then
      If Not m_generic_reports_marketing Is Nothing Then
        m_generic_reports_marketing.Status = CurrentUser.Permissions(m_generic_reports_marketing.FormId).Read

        If m_generic_reports_marketing.Status And Not _obj_report_tool.GetChildReportsPermissions(GenericReport.LocationMenu.MarketingMenu) Then
          m_generic_reports_marketing.Status = False
        End If
      End If
    End If

    If _obj_report_tool.ExistLocationMenu(GenericReport.LocationMenu.JackpotsMenu) Then
      If Not m_generic_reports_jackpot Is Nothing Then
        m_generic_reports_jackpot.Status = CurrentUser.Permissions(m_generic_reports_jackpot.FormId).Read

        If m_generic_reports_jackpot.Status And Not _obj_report_tool.GetChildReportsPermissions(GenericReport.LocationMenu.JackpotsMenu) Then
          m_generic_reports_jackpot.Status = False
        End If
      End If
    End If

    If _obj_report_tool.ExistLocationMenu(GenericReport.LocationMenu.GamingTableMenu) Then
      If Not m_generic_reports_gaming_table Is Nothing Then
        m_generic_reports_gaming_table.Status = CurrentUser.Permissions(m_generic_reports_gaming_table.FormId).Read

        If m_generic_reports_gaming_table.Status And Not _obj_report_tool.GetChildReportsPermissions(GenericReport.LocationMenu.GamingTableMenu) Then
          m_generic_reports_gaming_table.Status = False
        End If
      End If
    End If

    If _obj_report_tool.ExistLocationMenu(GenericReport.LocationMenu.SmartFloorMenu) Then
      If Not m_generic_reports_smartfloor Is Nothing Then
        m_generic_reports_smartfloor.Status = CurrentUser.Permissions(m_generic_reports_smartfloor.FormId).Read

        If m_generic_reports_smartfloor.Status And Not _obj_report_tool.GetChildReportsPermissions(GenericReport.LocationMenu.SmartFloorMenu) Then
          m_generic_reports_smartfloor.Status = False
        End If
      End If
    End If

    If _obj_report_tool.ExistLocationMenu(GenericReport.LocationMenu.GameGatewayMenu) Then
      If Not m_generic_reports_gateway Is Nothing Then
        m_generic_reports_gateway.Status = CurrentUser.Permissions(m_generic_reports_gateway.FormId).Read

        If m_generic_reports_gateway.Status And Not _obj_report_tool.GetChildReportsPermissions(GenericReport.LocationMenu.GameGatewayMenu) Then
          m_generic_reports_gateway.Status = False
        End If
      End If
    End If

    If (GeneralParam.GetBoolean("CreditLine", "Enabled", False)) Then
      Form_Creditline_sel.Status = CurrentUser.Permissions(Form_Creditline_sel.FormId).Read

      Form_Creditline_report.Status = CurrentUser.Permissions(Form_Creditline_report.FormId).Read
    End If

    'RAB 11-JAN-2016
    'Machine_denom_meters.Status = CurrentUser.Permissions(Machine_denom_meters.FormId).Read


    If WSI.Common.Misc.IsCountREnabled Then
      'DDS 31-MAR-2016
      CountR_Sel.Status = CurrentUser.Permissions(CountR_Sel.FormId).Read
      'ADI 30-MAR-2016
      m_countr_sessions.Status = CurrentUser.Permissions(m_countr_sessions.FormId).Read
    End If

    'ATB 19-10-2016
    'RGR 31-10-2016 controlled when not exist GP
    If WSI.Common.GeneralParam.GetBoolean("PinPad", "Enabled", False) Then
      'RGR 14-JUL-2016
      m_banking_report.Status = CurrentUser.Permissions(m_banking_report.FormId).Read

      'ATB 19-10-2016
      'RGR 31-10-2016 controlled when not exist GP
      If GeneralParam.GetBoolean("Cashier.Withdrawal", "BankCard.Conciliate", False) _
      AndAlso GeneralParam.GetBoolean("Cashier.CloseCash", "BankCard.Conciliate", False) Then
        ' LTC 23-AGO-2016
        m_banking_reconciliation.Status = CurrentUser.Permissions(m_banking_reconciliation.FormId).Read
      End If
    End If

#If DEBUG Then
    If WSI.Common.GeneralParam.GetBoolean("Features", "WinUP") Then
      ' m_backend_view.Status = CurrentUser.Permissions(m_backend_view.FormId).Read
    End If
#End If

    If (JunketsBusinessLogic.IsJunketsEnabled) Then

      m_junkets.Status = CurrentUser.Permissions(m_junkets.FormId).Read
      m_junkets_representatives.Status = CurrentUser.Permissions(m_junkets_representatives.FormId).Read
      m_junkets_report.Status = CurrentUser.Permissions(m_junkets_report.FormId).Read
      m_junkets_report_flyers.Status = CurrentUser.Permissions(m_junkets_report_flyers.FormId).Read

    End If

    ' 12-AUG-2016 ESE
    m_booking_terminals.Status = CurrentUser.Permissions(m_booking_terminals.FormId).Read
    ' JBM 05-MAR-2018
    m_reservation_terminals_monitor.Status = CurrentUser.Permissions(m_reservation_terminals_monitor.FormId).Read

    'EOR 15-AUG-2016
    m_reserved_maquines.Status = CurrentUser.Permissions(m_reserved_maquines.FormId).Read

    If Not (GeneralParam.GetBoolean("Client.Registration", "HideThresholdMovementsReport", True)) Then
      Form_threshold_movements.Status = CurrentUser.Permissions(Form_threshold_movements.FormId).Read
    End If

    ' JGC 11-JUN-2018
    If (WSI.Common.AutoPrint.BusinessLogic.AutoPrintConfig.TypesEnabled.Contains(AutoPrint.BusinessLogic.AutoPrintConfig.VoucherType.CashOutReceipt)) Then
      m_cash_out_report.Status = CurrentUser.Permissions(m_cash_out_report.FormId).Read
    End If

    tmr_TextBlinking.Enabled = True
    tmr_TextBlinking.Interval = 1000

    CheckPendingRequest()
    tmr_MoneyRequestPending.Start()

  End Sub ' GUI_InitControls

  ' PURPOSE: First screen activation actions:
  '           - Set form title
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_FirstActivation()

    ' Retrieve Site Identifier and Name from database just once
    'GLB_SiteId = GetSiteId()
    'GLB_SiteName = GetSiteName()

    SetMainTitle()

    Me.KeyPreview = True

  End Sub ' GUI_FirstActivation

  Private Function GetEnterImage(ByVal menu As CLASS_MENU.ENUM_MENU_ICON_NAMES) As Image

    Return CLASS_GUI_RESOURCES.GetImage(String.Format("enter_{0}", menu.ToString.ToLower))

  End Function

  ' PURPOSE: Menu creation
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_CreateMenu()

    Dim _cons_request As GenericReport.ConstructorRequest
    _cons_request = New GenericReport.ConstructorRequest()

    _cons_request.ProfileID = CurrentUser.ProfileId
    _cons_request.ShowAll = CurrentUser.IsSuperUser
    _cons_request.OnlyReportsMailing = False

    _cons_request.ModeType = GetVisibleProfileForms(CurrentUser.GuiId)

    ' ESE 15-JUN-2016
    _obj_report_tool = New GenericReport.ReportToolConfigService(_cons_request) 'Create new object and read the table REPORT_TOOL_CONFIG

    Dim cls_menu As CLASS_MENU_ITEM
    Dim super_center_flag As String

    '
    'WIGOS_LOGO
    ' 
    cls_menu = m_mnu.AddItem(String.Empty, ENUM_FORM.FORM_MAIN, , CLASS_GUI_RESOURCES.GetImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.WIGOS_LOGO.ToString.ToLower), , True)
    '
    ' SYSTEM MENU
    '
    cls_menu = m_mnu.AddItem(GLB_NLS_GUI_AUDITOR.Id(453), ENUM_FORM.FORM_MAIN, , CLASS_GUI_RESOURCES.GetImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.SYSTEM.ToString.ToLower), , True)
    cls_menu.AddEvents(GetEnterImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.SYSTEM))

    ' GUI users and profiles
    ProfilesMenuItem = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_CONFIGURATION.Id(467), ENUM_FORM.FORM_USER_PROFILE_SEL, AddressOf m_Click_Profiles)

    If WSI.Common.Misc.IsCashlessMode Then
      ' DCS 17-NOV-2014
      ' Mobile Banks
      If (Not WSI.Common.MobileBankConfig.IsMobileBankLinkedToADevice) Then
        m_mobile_bank_sel = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5687), ENUM_FORM.FORM_MOBILE_BANK_SEL, AddressOf m_mobile_bank_sel_Click)
      End If
    End If

    m_mnu.AddBreak(cls_menu)

    ' Audit
    Auditor = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_AUDITOR.Id(459), ENUM_FORM.FORM_GUI_AUDITOR, AddressOf m_Auditor_Click)

    ' QMP 18-FEB-2013: Operations schedule
    ' Operations schedule and shifts
    Operation_Schedule = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1663), ENUM_FORM.FORM_OPERATIONS_SCHEDULE, AddressOf m_operation_schedule_Click)

    ' APB 03-FEB-2011: Sites and Site Operators
    super_center_flag = GetSuperCenterData("Enabled")
    If super_center_flag = "1" Then
      ' Add item to menu only if SuperCenter features are enabled
      ' Sites_Operators = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_CONFIGURATION.Id(453), ENUM_FORM.FORM_SITES_OPERATORS_SEL, AddressOf m_Sites_operators_Click)
    End If

    ' Configuration menu JML 18-JUN-2012
    Configuration_menu = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1063), ENUM_FORM.FORM_CURRENCIES, Nothing)

    ' General parameters
    GeneralParamsMenuItem = m_mnu.AddItem(Configuration_menu, GLB_NLS_GUI_AUDITOR.Id(460), ENUM_FORM.FORM_GENERAL_PARAMS, AddressOf m_Click_GeneralParams)

    ' General operations
    Cashier_Configuration = m_mnu.AddItem(Configuration_menu, GLB_NLS_GUI_CONFIGURATION.Id(451), ENUM_FORM.FORM_CASHIER_CONFIGURATION, AddressOf m_cashier_configuration_Click)
    ' Catalogs
    Catalog_Configuration = m_mnu.AddItem(Configuration_menu, GLB_NLS_GUI_AUDITOR.Id(484), ENUM_FORM.FORM_CATALOGS, AddressOf m_catalog_configuration_Click)


    ' RCI 22-DEC-2010: Automatic Mailing Programming
    ' E-mail
    Mailing_Programming = m_mnu.AddItem(Configuration_menu, GLB_NLS_GUI_STATISTICS.Id(451), ENUM_FORM.FORM_MAILING_PROGRAMMING_SEL, AddressOf m_Mailing_programming_Click)

    ' HBB 02-AUG-2012: Password Configuration '
    ' Usernames and passwords
    Password_Configuration = m_mnu.AddItem(Configuration_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1175), ENUM_FORM.FORM_PASSWORD_CONFIGURATION, AddressOf m_password_configuration_Click)

    ' JMM 12-MAR-2013: Multisite Configuration
    ' Site
    MultiSite_Config = m_mnu.AddItem(Configuration_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1773), ENUM_FORM.FORM_MULTISITE_CONFIG, AddressOf m_site_config_Click)

    m_mnu.AddBreak(cls_menu)

    ' 
    ' SW DOWNLOAD MENU -- 451 "SW Download" // TJG 05-SEP-2008
    '
    m_software = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_SW_DOWNLOAD.Id(456), ENUM_FORM.FORM_MAIN)
    ' 452 "Versiones de Software"
    Sw_download_versions = m_mnu.AddItem(m_software, GLB_NLS_GUI_SW_DOWNLOAD.Id(452), ENUM_FORM.FORM_SW_DOWNLOAD_VERSIONS, AddressOf m_Sw_download_versions_Click)
    ' APB 07-APR-2009
    ' 454 "Control de Versiones"
    Version_Control = m_mnu.AddItem(m_software, GLB_NLS_GUI_PLAYER_TRACKING.Id(4893), Nothing)

    Misc_sw_build = m_mnu.AddItem(Version_Control, GLB_NLS_GUI_PLAYER_TRACKING.Id(4894), ENUM_FORM.FORM_MISC_SW_BY_BUILD, AddressOf m_Misc_sw_build_Click)
    ' 453 "Estado de Terminales"   GLB_NLS_GUI_SW_DOWNLOAD.Id(454)
    Terminals_sw_version = m_mnu.AddItem(Version_Control, GLB_NLS_GUI_PLAYER_TRACKING.Id(4895), ENUM_FORM.FORM_TERMINALS_BY_BUILD, AddressOf m_Terminals_sw_version_Click)
    ' APB 28-APR-2009
    ' 455 "Licencias"
    Licences = m_mnu.AddItem(m_software, GLB_NLS_GUI_SW_DOWNLOAD.Id(455), ENUM_FORM.FORM_LICENCE_VERSIONS, AddressOf m_Licence_Click)

    '
    ' TOOLS MENU -- 457 "Herramientas"
    '
    ' PGJ 21-JUN-2013
    If GLB_CurrentUser.IsSuperUser Then
      m_tools = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_CLASS_II.Id(457), ENUM_FORM.FORM_MAIN)
      ' 458 "Grabaci�n de Tarjetas"
      Magnetic_card_writer = m_mnu.AddItem(m_tools, GLB_NLS_GUI_CLASS_II.Id(458), ENUM_FORM.FORM_CARD_RECORD, AddressOf m_magnetic_card_writer_Click)
    End If

    m_mnu.AddBreak(cls_menu)

    ' ANG 11-06
    If GLB_StartedInSelectSiteMode Then
      m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(2051), 0, AddressOf m_DisconnectClick)
    End If

    ' Exit
    m_mnu.AddItem(cls_menu, GLB_NLS_GUI_AUDITOR.Id(452), 0, AddressOf m_Exit_Click)

    'ESE 15-JUN-2016
    'Check if we need to add a new item in the menu
    If _obj_report_tool.ExistLocationMenu(GenericReport.LocationMenu.SystemMenu) Then
      m_mnu.AddBreak(cls_menu)
      m_generic_reports_system = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7333), ENUM_FORM.FORM_REPORT_TOOL_SYSTEM_MENU, AddressOf m_report_tool_Click)
    End If

    '
    ' TERMINALS MENU
    '
    cls_menu = m_mnu.AddItem(GLB_NLS_GUI_AUDITOR.Id(458), ENUM_FORM.FORM_MAIN, , CLASS_GUI_RESOURCES.GetImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.TERMINALS.ToString.ToLower), , True)
    cls_menu.AddEvents(GetEnterImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.TERMINALS))

    ' Terminals
    TerminalsMenuItem = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_AUDITOR.Id(458), ENUM_FORM.FORM_TERMINALS_SELECTION, AddressOf m_Click_Terminals_View)

    If Not GeneralParam.GetBoolean("WigosGUI", "HideTerminalsTimeDisconnected") Then
      TerminalsTimeDisconectedMenuItem = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(8812), ENUM_FORM.FORM_TERMINALS_TIME_DISCONNECTED, AddressOf m_Click_Terminals_Time_Disconnected_View)
    End If

    m_mnu.AddBreak(cls_menu)

    ' Current meters
    Game_meters = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_STATISTICS.Id(395), ENUM_FORM.FORM_GAME_METERS, AddressOf m_Game_meters_Click)

    ' DHA 08-APR-2015
    ' History meters edit
    HistoryMetersEdit = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(6144), ENUM_FORM.FORM_TERMINAL_SAS_METERS_EDIT, AddressOf m_history_meters_edit_Click)

    ' Dll  19-NOV-2014 Add Terminal Sas Meters Edit form
    'If TITO.Utils.IsTitoMode Then
    'm_terminal_sas_meters_edit = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5742), ENUM_FORM.FORM_TERMINAL_SAS_METERS_EDIT, AddressOf m_terminal_sas_meters_edit_Click)
    'End If

    ' ACM 22-NOV-2013 Add Terminal Sas Meters sel form
    ' Meter history
    m_terminal_sas_meters_sel = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1401), ENUM_FORM.FORM_TERMINAL_SAS_METERS_SEL, AddressOf m_terminal_sas_meters_sel_Click)

    If GeneralParam.GetBoolean("SasMeter", "MetersHistory.ShowAdvanced") Then
      m_terminal_sas_meters_sel_advanced = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(8316), ENUM_FORM.FORM_TERMINAL_SAS_METERS_SEL, AddressOf m_terminal_sas_meters_sel_advanced_Click)
    End If

    ' DLL 14-NOV-2013: Skipped meters report
    ' Skipped meters report
    Skipped_Meters_Report = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(2937), ENUM_FORM.FORM_SKIPPED_METERS_REPORT, AddressOf m_skipped_meters_report_Click)

    'YNM 27-AUG-2015: METERS_COMPARISON
    'Meters comparison
    'LQB 24-MAY-2018: Comment this option
    'Meters_Comparison = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(6686), ENUM_FORM.FORM_METERS_COMPARISON, AddressOf m_meters_comparison_Click)

    ''XGJ 23-GEN-2017
    '' Terminal
    ''XGJ TODO
    'Terminal_Draw_Report = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7824), ENUM_FORM.FORM_TERMINAL_DRAW_REPORT, AddressOf m_terminal_draw_report_click)
    'Terminal_Draw_Summary = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7849), ENUM_FORM.FORM_TERMINAL_DRAW_SUMMARY, AddressOf Terminal_Draw_Summary_Click)

    'm_mnu.AddBreak(cls_menu)

    ' ICS 20-DEC-2013 "Estado de Terminales"
    ' Terminal status
    m_terminal_status = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_SW_DOWNLOAD.Id(453), ENUM_FORM.FORM_TERMINALS_STATUS, AddressOf m_terminal_status_Click)

    ' RCI 20-DEC-2010: Terminals without activity
    ' Terminals without activity
    Terminals_without_activity = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_INVOICING.Id(495), ENUM_FORM.FORM_TERMINALS_WITHOUT_ACTIVITY, AddressOf m_Terminal_without_activity_Click)

    ' Machine Reservation menu
    Machine_reservation_menu = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(8535), ENUM_FORM.FORM_MAIN, Nothing)

    ' Terminals Reservation Configuration
    m_booking_terminals = m_mnu.AddItem(Machine_reservation_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7510), ENUM_FORM.FORM_BOOKING_TERMINALS_CONFIGURATION, AddressOf m_booking_terminal_config_Click)

    ' Terminals Reservation Monitor
    m_reservation_terminals_monitor = m_mnu.AddItem(Machine_reservation_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(8534), ENUM_FORM.FORM_TERMINAL_RESERVATION_MONITOR, AddressOf m_terminal_reservation_monitor_Click)

    m_mnu.AddBreak(cls_menu)

    ' Customize menu
    Terminals_configuration_menu = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5743), ENUM_FORM.FORM_MAIN, Nothing)

    ' JGR 17-FEB-2012: Area & Bank
    ' Areas and banks
    AreaBank = m_mnu.AddItem(Terminals_configuration_menu, GLB_NLS_GUI_CONTROLS.Id(425), ENUM_FORM.FORM_AREA_BANK, AddressOf m_AreaBank_Click)

    ' DMR 30-APR-2013
    ' Providers
    Providers_Games = m_mnu.AddItem(Terminals_configuration_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1916), ENUM_FORM.FORM_PROVIDERS_GAMES_SEL, AddressOf m_Providers_Games_Click)

    ' RBG 21-JUN-2013
    ' Terminal groups
    Menu_Groups = m_mnu.AddItem(Terminals_configuration_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5744), ENUM_FORM.FORM_GROUPS_SEL, AddressOf m_Groups_Click)

    ' Terminal denominations
    m_terminal_denominations_menu = m_mnu.AddItem(Terminals_configuration_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5750), ENUM_FORM.FORM_TERMINAL_DENOMINATIONS_CONFIG, AddressOf m_terminal_denominations_config_Click)

    m_mnu.AddBreak(Terminals_configuration_menu)

    ' Pending terminals
    TerminalsPendingMenuItem = m_mnu.AddItem(Terminals_configuration_menu, GLB_NLS_GUI_AUDITOR.Id(463), ENUM_FORM.FORM_TERMINALS_PENDING, AddressOf m_Click_Terminals_Pending)

    ' RCI 13-MAY-2010: Terminals 3GS
    ' 3GS terminals
    Terminals3gsMenuItem = m_mnu.AddItem(Terminals_configuration_menu, GLB_NLS_GUI_AUDITOR.Id(461), ENUM_FORM.FORM_TERMINALS_3GS_EDIT, AddressOf m_Click_Terminals_3GS)

    ' Terminal-game relation
    TerminalGameRelationMenuItem = m_mnu.AddItem(Terminals_configuration_menu, GLB_NLS_GUI_CONFIGURATION.Id(468), ENUM_FORM.FORM_TERMINAL_GAME_RELATION, AddressOf m_Click_Terminal_Game_Relation)

    ' Terminal meter delta
    TerminalMeterDeltaItem = m_mnu.AddItem(Terminals_configuration_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(6061), ENUM_FORM.FORM_TERMINAL_METER_DELTA_SEL, AddressOf m_Click_Terminal_Meter_Delta)

    m_mnu.AddBreak(Terminals_configuration_menu)

    ' Kiosk Config ANG 07-06-2012
    ' PromoBOX
    AdsConfiguration = m_mnu.AddItem(Terminals_configuration_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(722), ENUM_FORM.FORM_ADS_CONFIGURATION, AddressOf m_AdsConfiguration_Click)

    ' TODO: LKT-LCD Display Configuration
    ' LCD display
    LcdConfiguration = m_mnu.AddItem(Terminals_configuration_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5157), ENUM_FORM.FORM_LCD_CONFIGURATION, AddressOf m_LcdConfiguration_Click)

    ' PCD Configuration
    m_pcd_configuration_sel = m_mnu.AddItem(Terminals_configuration_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(6017), ENUM_FORM.FORM_PCD_TERMINALS_CONFIGURATION_SEL, AddressOf m_pcd_configuration_Click)

    'CAGE_TEAM SGB 07-JUL-2015

    m_mnu.AddBreak(Terminals_configuration_menu)
    ' Terminals Block
    m_terminals_block = m_mnu.AddItem(Terminals_configuration_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(6539), ENUM_FORM.FORM_TERMINALS_BLOCK, AddressOf m_terminals_block_Click)

    If (WSI.Common.Misc.IsIntelliaEnabled() OrElse WSI.Common.MobileBankConfig.IsMobileBankLinkedToADevice) Then
      m_mnu.AddBreak(Terminals_configuration_menu)
      m_smartfloor_terminals_print_QR_Codes = m_mnu.AddItem(Terminals_configuration_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(6955), ENUM_FORM.FORM_SMARTFLOOR_TERMINALS_PRINT_QR_CODES, AddressOf m_smartfloor_terminals_print_QR_Codes_Click)
    End If

    'ESE 15-JUN-2016
    'Check if we need to add a new item in the menu
    If _obj_report_tool.ExistLocationMenu(GenericReport.LocationMenu.TerminalsMenu) Then
      m_mnu.AddBreak(cls_menu)
      m_generic_reports_terminals = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7338), ENUM_FORM.FORM_REPORT_TOOL_TERMINALS_MENU, AddressOf m_report_tool_Click)
    End If

    '
    ' MONITOR
    '

    ' Monitor menu
    cls_menu = m_mnu.AddItem(GLB_NLS_GUI_STATISTICS.Id(356), ENUM_FORM.FORM_MAIN, , CLASS_GUI_RESOURCES.GetImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.MONITOR.ToString.ToLower), , True)
    cls_menu.AddEvents(GetEnterImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.MONITOR))

    ' Services menu
    Services_menu = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_SYSTEM_MONITOR.Id(451), ENUM_FORM.FORM_MAIN, Nothing)

    ' APB 27-NOV-2008
    ' Services
    Svc_monitor = m_mnu.AddItem(Services_menu, GLB_NLS_GUI_SYSTEM_MONITOR.Id(205), ENUM_FORM.FORM_SERVICE_MONITOR, AddressOf m_Svc_monitor_Click)

    ' Sessions
    Ws_sessions = m_mnu.AddItem(Services_menu, GLB_NLS_GUI_STATISTICS.Id(355), ENUM_FORM.FORM_WS_SESSIONS, AddressOf m_Ws_sessions_Click)

    ' Terminals version control
    Terminals_sw_version_monitor = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4833), ENUM_FORM.FORM_TERMINALS_BY_BUILD, AddressOf m_Terminals_sw_version_Click)

    ' RCI 18-JUN-2010: Commands
    ' Commands menu
    Commands_menu = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_CONTROLS.Id(451), ENUM_FORM.FORM_COMMANDS_SEND, Nothing)

    ' Send
    Commands_send = m_mnu.AddItem(Commands_menu, GLB_NLS_GUI_CONTROLS.Id(452), ENUM_FORM.FORM_COMMANDS_SEND, AddressOf m_Commands_send_Click)

    ' History
    Commands_history = m_mnu.AddItem(Commands_menu, GLB_NLS_GUI_CONTROLS.Id(453), ENUM_FORM.FORM_COMMANDS_HISTORY, AddressOf m_Commands_history_Click)

    ' WXP Protocol - Frame Viewer
    If WSI.Common.Misc.ReadGeneralParams("ExternalProtocol", "Enabled") Then
      Peru_Frames = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1083), ENUM_FORM.FORM_PERU_FRAMES, AddressOf m_Peru_Frames_Click)
    End If

    If WSI.Common.Misc.SystemMode() = SYSTEM_MODE.WASS Then
      ' Software Validation menu
      m_software_validation_menu = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(4825), ENUM_FORM.FORM_SOFTWARE_VALIDATION_SEND, Nothing)

      ' Send
      m_software_validation_send = m_mnu.AddItem(m_software_validation_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(4816), ENUM_FORM.FORM_SOFTWARE_VALIDATION_SEND, AddressOf m_software_validation_send_Click)

      ' History
      m_software_validation_history = m_mnu.AddItem(m_software_validation_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(4817), ENUM_FORM.FORM_SOFTWARE_VALIDATION_HISTORY, AddressOf m_software_validation_history_Click)
    End If

    ' JMM 12-MAR-2013: Multisite Monitor
    If GeneralParam.GetBoolean("Site", "MultiSiteMember") Then
      ' Connection with MultiSite
      MultiSite_Monitor = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1787), ENUM_FORM.FORM_MULTISITE_MONITOR, AddressOf m_multisite_status_Click)
    End If

    'ESE 15-JUN-2016
    'Check if we need to add a new item in the menu
    If _obj_report_tool.ExistLocationMenu(GenericReport.LocationMenu.MonitorMenu) Then
      m_mnu.AddBreak(cls_menu)
      m_generic_reports_monitor = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7339), ENUM_FORM.FORM_REPORT_TOOL_MONITOR_MENU, AddressOf m_report_tool_Click)
    End If

    '
    ' ALARMS MENU
    '
    cls_menu = m_mnu.AddItem(GLB_NLS_GUI_ALARMS.Id(454), ENUM_FORM.FORM_MAIN, , CLASS_GUI_RESOURCES.GetImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.ALARMS.ToString.ToLower), , True)
    cls_menu.AddEvents(GetEnterImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.ALARMS))

    ' History (OBSOLETE)
    'EventHistory = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_AUDITOR.Id(457), ENUM_FORM.FORM_EVENTS_HISTORY, AddressOf m_EventsHistory_Click)

    ' Alarms
    Alarms = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_ALARMS.Id(454), ENUM_FORM.FORM_ALARMS, AddressOf m_Alarms_Click)

    m_mnu.AddBreak(cls_menu)

    ' Customize menu
    Alarms_configuration_menu = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5743), ENUM_FORM.FORM_MAIN, Nothing)

    ' JBC 16-APR-2013   Custom Alarms
    ' Calculate alarms
    Custom_Alarms = m_mnu.AddItem(Alarms_configuration_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5745), ENUM_FORM.FORM_CUSTOM_ALARMS, AddressOf m_Custom_Alarms_click)

    ' JMM 07-MAY-2014
    ' JPJ 02-JUN-2014 They are only active when it's enabled
    If (GeneralParam.GetBoolean("Pattern", "Enabled", True)) Then
      ' Patterns
      Patterns_Sel = m_mnu.AddItem(Alarms_configuration_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(4924), ENUM_FORM.FORM_PATTERNS_SEL, AddressOf m_Patterns_click)
    End If

    'ESE 15-JUN-2016
    'Check if we need to add a new item in the menu
    If _obj_report_tool.ExistLocationMenu(GenericReport.LocationMenu.AlarmsMenu) Then
      m_mnu.AddBreak(cls_menu)
      m_generic_reports_alarms = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7340), ENUM_FORM.FORM_REPORT_TOOL_ALARMS_MENU, AddressOf m_report_tool_Click)
    End If

    '
    ' STATISTICS MENU
    '
    cls_menu = m_mnu.AddItem(GLB_NLS_GUI_STATISTICS.Id(201), ENUM_FORM.FORM_MAIN, , CLASS_GUI_RESOURCES.GetImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.STATISTICS.ToString.ToLower), , True)
    cls_menu.AddEvents(GetEnterImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.STATISTICS))

    ' Terminals menu
    Statistics_terminals_menu = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_AUDITOR.Id(458), ENUM_FORM.FORM_MAIN, Nothing)

    ' CSR 30-JUL-2015
    ' ETP 02-MAR-2016 PBI 101190 Ocultado.
    'Machine_denom_meters = m_mnu.AddItem(Statistics_terminals_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(6597), ENUM_FORM.FORM_MACHINE_DENOM_METERS, AddressOf m_Machine_denom_meters_Click)

    ' Grouped by date, provider and terminal
    Sales_date_terminal = m_mnu.AddItem(Statistics_terminals_menu, GLB_NLS_GUI_INVOICING.Id(457), ENUM_FORM.FORM_DATE_TERMINAL, AddressOf m_Sales_date_terminal_Click)

    ' Provider activity
    Provider_day_terminal_statistics = m_mnu.AddItem(Statistics_terminals_menu, GLB_NLS_GUI_INVOICING.Id(465), ENUM_FORM.FORM_PROVIDER_DAY_TERMINAL, AddressOf m_Provider_day_terminal_Click)

    m_mnu.AddBreak(Statistics_terminals_menu)

    ' Grouped by terminal
    Statistics_terminals = m_mnu.AddItem(Statistics_terminals_menu, GLB_NLS_GUI_STATISTICS.Id(350), ENUM_FORM.FORM_STATISTICS_TERMINALS, AddressOf m_Statistics_terminals_Click)

    ' Grouped by game
    Statistics_games = m_mnu.AddItem(Statistics_terminals_menu, GLB_NLS_GUI_STATISTICS.Id(351), ENUM_FORM.FORM_STATISTICS_GAMES, AddressOf m_Statistics_games_Click)

    ' Grouped by date
    Statistics = m_mnu.AddItem(Statistics_terminals_menu, GLB_NLS_GUI_STATISTICS.Id(352), ENUM_FORM.FORM_STATISTICS_DATES, AddressOf m_Statistics_general_Click)

    ' Grouped by hour
    Statistics_hours_group = m_mnu.AddItem(Statistics_terminals_menu, GLB_NLS_GUI_STATISTICS.Id(365), ENUM_FORM.FORM_STATISTICS_HOURS_GROUP, AddressOf m_Statistics_hours_group_Click)

    'Statistics_cards = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_STATISTICS.Id(364), ENUM_FORM.FORM_STATISTICS_CARDS, AddressOf m_Statistics_cards_Click) // SLE: 23-ABR-2010 

    ' Accumulated by hour
    Statistics_hours = m_mnu.AddItem(Statistics_terminals_menu, GLB_NLS_GUI_STATISTICS.Id(353), ENUM_FORM.FORM_STATISTICS_HOURS, AddressOf m_Statistics_hours_Click)

    ' LTC 24-NOV-2016
    'If WSI.Common.Misc.ShowMachineAndGameReport() Then
    '  m_mnu.AddBreak(Statistics_terminals_menu)
    '  'JMV 23-DEC-2014 - Reporte de m�quinas y juegos
    '  Machine_and_game_report_menu = m_mnu.AddItem(Statistics_terminals_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5820), ENUM_FORM.FORM_MACHINE_AND_GAME_REPORT, AddressOf m_machine_and_game_report_Click)
    'End If

    m_mnu.AddBreak(Statistics_terminals_menu)

    ' Capacity by provider
    Capacity = m_mnu.AddItem(Statistics_terminals_menu, GLB_NLS_GUI_AUDITOR.Id(470), ENUM_FORM.FORM_CAPACITY, AddressOf m_Capacity_Click)

    ' Machine occupancy
    MachineOcupation = m_mnu.AddItem(Statistics_terminals_menu, GLB_NLS_GUI_STATISTICS.Id(444), ENUM_FORM.FORM_MACHINE_OCCUPATION, AddressOf m_MachineOcupation_Click)

    ' DDM 14-MAR-2012
    ' Productivity report
    Productivity_reports = m_mnu.AddItem(Statistics_terminals_menu, GLB_NLS_GUI_STATISTICS.Id(270), ENUM_FORM.FORM_PRODUCTIVITY_REPORTS, AddressOf m_Productivity_reports_Click)

    If TITO.Utils.IsTitoMode Then
      m_mnu.AddBreak(Statistics_terminals_menu)

      ' JML 1-SEP-2014
      ' Netwin report by denomination and machine
      collection_by_machine_and_denomination = m_mnu.AddItem(Statistics_terminals_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5350), ENUM_FORM.FORM_COLLECTION_BY_MACHINE_AND_DENOMINATION_REPORT, AddressOf m_collection_by_machine_and_denomination_Click)
    End If

    If TITO.Utils.IsTitoMode Then
      ' FAV 20-MAY-2015
      m_win_control_report = m_mnu.AddItem(Statistics_terminals_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(6356), ENUM_FORM.FORM_TITO_WIN_CONTROL_REPORT, AddressOf m_tito_win_control_report_Click)
    End If

    ' Customers menu
    Statistics_customers_menu = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_AUDITOR.Id(472), ENUM_FORM.FORM_MAIN, Nothing)

    ' Customer base
    CustomerBase = m_mnu.AddItem(Statistics_customers_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(551), ENUM_FORM.FORM_CUSTOMER_BASE, AddressOf m_CustomerBase_Click)

    ' LEM 15-JAN-2013
    ' Gaming preferences
    Play_preferences = m_mnu.AddItem(Statistics_customers_menu, GLB_NLS_GUI_STATISTICS.Id(499), ENUM_FORM.FORM_PLAY_PREFERENCES, AddressOf m_Play_preferences_Click)

    ' JGR 25-JAN-2012: Segmentaci�n Cliente-Proveedor
    ' Played by player and provider
    CustomerSegmentation = m_mnu.AddItem(Statistics_customers_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(570), ENUM_FORM.FORM_CUSTOMER_SEGMENTATION, AddressOf m_CustomerSegmentation_Click)

    ' DDM 05-APR-2012
    ' Segmentation report
    Segmentation_reports = m_mnu.AddItem(Statistics_customers_menu, GLB_NLS_GUI_STATISTICS.Id(244), ENUM_FORM.FORM_SEGMENTATION_REPORTS, AddressOf m_Segmentation_reports_Click)

    If WSI.Common.Misc.IsCashlessMode() Then
      ' MPO 14-MAR-2012
      ' Prize detail report
      Detailed_prizes = m_mnu.AddItem(Statistics_customers_menu, GLB_NLS_GUI_STATISTICS.Id(489), ENUM_FORM.FORM_DETAILED_PRIZES, AddressOf m_Detailed_prizes_Click)
    End If

    ' AMF 20-JUL-2015
    m_customers_playing = m_mnu.AddItem(Statistics_customers_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(6609), ENUM_FORM.FORM_CUSTOMERS_PLAYING, AddressOf m_customers_playing_Click)

    'EOR 01-SEP-2017
    m_last_six_months_report = m_mnu.AddItem(Statistics_customers_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(8605), ENUM_FORM.FORM_LAST_SIX_MONTHS_REPORT, AddressOf m_activity_last_six_months_Click)

    ' Gaming sessions menu
    Statistics_play_sessions_menu = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_AUDITOR.Id(96), ENUM_FORM.FORM_MAIN, Nothing)

    ' Gaming sessions
    Plays_sessions = m_mnu.AddItem(Statistics_play_sessions_menu, GLB_NLS_GUI_STATISTICS.Id(357), ENUM_FORM.FORM_PLAYS_SESSIONS, AddressOf m_Plays_sessions_Click)

    If m_elp_mode = 0 Then
      ' Points assignment
      Plays_sessions_points_assignment = m_mnu.AddItem(Statistics_play_sessions_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(2814), ENUM_FORM.FORM_POINTS_ASSIGNMENT, AddressOf m_Points_Assignment_Click)
    End If

    ' DHA 17-MAR-2014
    If Not GeneralParam.GetBoolean("WigosGUI", "HideGamingSessionAdjustment", True) Then
      ' Gaming session adjustment
      Plays_sessions_edit = m_mnu.AddItem(Statistics_play_sessions_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(4739), ENUM_FORM.FORM_PLAYS_SESSIONS_EDIT, AddressOf m_Plays_sessions_edit_Click)
    End If

    m_mnu.AddBreak(cls_menu)

    ' MBF 13-DEC-2010
    ' Statistics balance report
    Gap_report_statistics = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_CONFIGURATION.Id(482), ENUM_FORM.FORM_SITE_GAP_REPORT, AddressOf m_Statistics_providers_gap_Click)

    ' HBB & JCM 27-SEP-2012: Stats Adjustment
    ' Manual adjustment
    Stats_Adjustment = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1371), ENUM_FORM.FORM_STATS_MANUAL_ADJUSTMENT, AddressOf m_stats_manual_adjustment_Click)

#If DEBUG Then

    m_mnu.AddBreak(cls_menu)

    ' RRR 14-NOV-2014
    ' Graphics
    FormCharts = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5717), ENUM_FORM.FORM_CHARTS, AddressOf m_charts_Click)
#End If

    'ESE 15-JUN-2016
    'Check if we need to add a new item in the menu
    If _obj_report_tool.ExistLocationMenu(GenericReport.LocationMenu.StatisticsMenu) Then
      m_mnu.AddBreak(cls_menu)
      m_generic_reports_statistics = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7341), ENUM_FORM.FORM_REPORT_TOOL_STATISTICS_MENU, AddressOf m_report_tool_Click)
    End If

    '
    ' CASH MENU
    '

    'Vertical Separator

    cls_menu = m_mnu.AddItem(GLB_NLS_GUI_INVOICING.Id(393), ENUM_FORM.FORM_MAIN, , CLASS_GUI_RESOURCES.GetImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.CASH.ToString.ToLower), , True)
    cls_menu.AddEvents(GetEnterImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.CASH))

    ' Cash sessions
    Cashier_sessions = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_INVOICING.Id(453), ENUM_FORM.FORM_CASHIER_SESSIONS, AddressOf m_Cashier_sessions_Click)

    ' Cash movements
    Cashier_movements = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_INVOICING.Id(454), ENUM_FORM.FORM_CASHIER_MOVS, AddressOf m_Cashier_movements_Click)

    ' RRB 23-JAN-2013
    ' Cash summary
    Cash_Summary = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1632), ENUM_FORM.FORM_CASH_SUMMARY, AddressOf m_Cash_summary_Click)

    ' Handpays
    Menu_Handpays = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_INVOICING.Id(481), ENUM_FORM.FORM_HANDPAYS, AddressOf m_Handpays_Click)

    'HBB 07-OCT-2014 if system is in tito mode the mobile banc movement form can not be visible
    If WSI.Common.Misc.IsCashlessMode Then
      ' Mobile bank movements
      MB_Movements = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_INVOICING.Id(482), ENUM_FORM.FORM_MOBILE_BANK_MOVEMENTS, AddressOf m_Mobile_Bank_Movements_Click)
    End If

    ' FJC 23-OCT-2014
    ' Shortfall and excess report
    Short_over_report = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5664), ENUM_FORM.FORM_SHORT_OVER_REPORT, AddressOf m_Short_over_report_Click)

    ' DRV 20-JUL-2015
    ' Cash monitor
    m_cash_monitor = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(6612), ENUM_FORM.FORM_CASH_MONITOR, AddressOf m_cash_monitor_Click)

    ' RGR 18-NOV-2015
    ' Report cash payments
    If Tax.EnableTitoTaxWaiver(True) Then
      m_tito_payment_report = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(6783), ENUM_FORM.FORM_TITO_PAYMENT_REPORT_SEL, AddressOf m_tito_payment_report_Click)
    End If

    m_mnu.AddBreak(cls_menu)

    If Not Cage.IsCageAutoMode() AndAlso Cage.IsCageEnabled() Then
      Cashier_Send = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(4376), ENUM_FORM.FORM_CASHIER_SEND, AddressOf m_cashier_send_Click)
      Cashier_Collection = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(4377), ENUM_FORM.FORM_CASHIER_COLLECTION, AddressOf m_cashier_collection_Click)
      m_mnu.AddBreak(cls_menu)
    End If

    If WSI.Common.Misc.IsCountREnabled Then
      'ADI - 09-MAY-2016
      Redeem_Kiosk = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7226), ENUM_FORM.FORM_MAIN, Nothing)
      ' COUNT R SELECTION
      CountR_Sel = m_mnu.AddItem(Redeem_Kiosk, GLB_NLS_GUI_PLAYER_TRACKING.Id(7226), ENUM_FORM.FORM_COUNTR_SEL, AddressOf m_countr_Click)
      m_countr_sessions = m_mnu.AddItem(Redeem_Kiosk, GLB_NLS_GUI_PLAYER_TRACKING.Id(7244), ENUM_FORM.FORM_COUNTR_SESSIONS, AddressOf m_countr_sessions_Click)
      m_mnu.AddBreak(cls_menu)
    End If

    ' JAB 28-MAR-2012
    ' Cash Desk voucher history
    Voucher_browser = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(606), ENUM_FORM.FORM_VOUCHER_BROWSER, AddressOf m_Voucher_browser_Click)

    If WSI.Common.Misc.IsCashlessMode Then
      ' RCI 27-MAY-2011
      ' Cash Desk vouchers report
      Tickets_report = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5746), ENUM_FORM.FORM_TICKETS_REPORT, AddressOf m_Tickets_report_Click)

      ' JBC 26-NOV-2014
      Tax_report = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5764, GeneralParam.GetString("Cashier", "Split.B.Tax.Name")),
                                                                                      ENUM_FORM.FORM_DAILY_TAX_SPLIT_B_SUMMARY, AddressOf m_Tax_report_Click)
    End If

    m_mnu.AddBreak(cls_menu)

    ' HBB 17-JAN-2013
    ' Payment orders
    Payment_Orders = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1563), ENUM_FORM.FORM_PAYMENT_ORDER_SEL, AddressOf m_Payment_orders_Click)

    ' JBC 19-SEP-2014
    ' Bank Data Report
    m_bank_transaction_data = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5490), ENUM_FORM.FORM_BANK_TRANSACTION_DATA, AddressOf m_bank_transaction_data_click)


    ' JGC 11-JUN-2018: Only for Philippines --> Cash Out Receipt Report
    If (WSI.Common.AutoPrint.BusinessLogic.AutoPrintConfig.TypesEnabled.Contains(AutoPrint.BusinessLogic.AutoPrintConfig.VoucherType.CashOutReceipt)) Then
      m_cash_out_report = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(9089), ENUM_FORM.FORM_CASH_OUT_REPORT, AddressOf m_cash_out_report_Click)
    End If

    'ATB 20-10-2016
    'RGR 31-10-2016 controlled when not exist GP
    If GeneralParam.GetBoolean("PinPad", "Enabled", False) Then
      'LTC 05-AGO-2016 Customize menu Bank Transactions 
      bank_transactions_menu = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7407), ENUM_FORM.FORM_MAIN, Nothing)
      'RGR 14-JUL-2016
      m_banking_report = m_mnu.AddItem(bank_transactions_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7518), ENUM_FORM.FORM_BANKING_REPORT, AddressOf m_frm_banking_report_sel_click)
      'ATB 19-10-2016
      'RGR 31-10-2016 controlled when not exist GP
      If GeneralParam.GetBoolean("Cashier.Withdrawal", "BankCard.Conciliate", False) _
      AndAlso GeneralParam.GetBoolean("Cashier.CloseCash", "BankCard.Conciliate", False) Then
        'LTC 05-AGO-2016
        m_banking_reconciliation = m_mnu.AddItem(bank_transactions_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7490), ENUM_FORM.FORM_BANKING_RECONCILIATION, AddressOf m_frm_banking_reconciliation_click)
      End If
    End If

    m_mnu.AddBreak(cls_menu)

    If WSI.Common.Misc.IsCashlessMode Then
      ' MMG 08-JUL-2013   Cash desk draws detail report
      ' Cash Desk draw report
      Draws_cashdesk_detail = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(2245), ENUM_FORM.FORM_DRAWS_CASHDESK_DETAIL, AddressOf m_Draws_cashdesk_detail_click)

      ' MMG 16-JUL-2013   Cash desk draws summary report
      ' Cash Desk draw summary
      Draws_cashdesk_summary = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(2364), ENUM_FORM.FORM_DRAWS_CASHDESK_SUMMARY, AddressOf m_Draws_cashdesk_sumary_click)

      m_mnu.AddBreak(cls_menu)

      'XGJ 23-GEN-2017
      ' Terminal
      'XGJ TODO
      If (WSI.Common.Misc.IsFeatureTerminalDrawEnabled()) Then
        Terminal_Draw_Report = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7824), ENUM_FORM.FORM_TERMINAL_DRAW_REPORT, AddressOf m_terminal_draw_report_click)
        Terminal_Draw_Summary = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7849), ENUM_FORM.FORM_TERMINAL_DRAW_SUMMARY, AddressOf Terminal_Draw_Summary_Click)

        m_mnu.AddBreak(cls_menu)
      End If
    End If

    ' Customize menu
    Cash_configuration_menu = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5743), ENUM_FORM.FORM_MAIN, Nothing)

    If WSI.Common.Misc.IsCashlessMode Then
      ' HBB 13-SEP-2012: charge by service
      ' Service charge
      Cash_By_Service = m_mnu.AddItem(Cash_configuration_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1294), ENUM_FORM.FORM_CASH_SERVICE, AddressOf m_cash_sevice_Click)
    End If

    ' HBB 14-JAN-2013: Payment orders config
    ' Payment orders
    PaymentOrdersConfig = m_mnu.AddItem(Cash_configuration_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1563), ENUM_FORM.FORM_PAYMENT_ORDER_CONFIG, AddressOf m_payment_orders_config_Click)

    ' AMF 30-JUL-2013
    ' Bank card / check / currency recharge
    Currency_Configuration = m_mnu.AddItem(Cash_configuration_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(2543), ENUM_FORM.FORM_CURRENCY_CONFIGURATION, AddressOf m_currency_configuration_Click)

    ' SMN 26-MAR-2013
    ' Cash Desk terminal aliases
    Machine_Alias = m_mnu.AddItem(Cash_configuration_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1811), ENUM_FORM.FORM_MACHINE_ALIAS, AddressOf m_Machine_Alias_Click)

    If WSI.Common.Misc.IsCashlessMode Then
      ' RRR 22-JUL-2013
      ' Money laundering
      Money_Laundering_Config = m_mnu.AddItem(Cash_configuration_menu, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2399, Accounts.getAMLName()), ENUM_FORM.FORM_MONEY_LAUNDERING_CONFIG, AddressOf m_Money_laundering_config_Click)

      ' MMG 09-AUG-2013   Cash desk draws configuration
      ' Cash Desk draw
      Cashdesk_Draws_Configuration = m_mnu.AddItem(Cash_configuration_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(2283), ENUM_FORM.FORM_CASHDESK_DRAWS_CONFIGURATION, AddressOf m_Cashdesk_draws_configuration_click)
      ' LTC 07-OCT-2016
      If GamingTableBusinessLogic.IsGamingTablesEnabled Then
        Cashdesk_Draws_Tables_Configuration = m_mnu.AddItem(Cash_configuration_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7588), ENUM_FORM.FORM_GAMING_TABLES_DRAWS_CONFIGURATION, AddressOf m_GamingTables_draws_configuration_click)
      End If
    End If

    'XGJ TODO
    If (WSI.Common.Misc.IsCashlessMode And WSI.Common.Misc.IsFeatureTerminalDrawEnabled()) Then
      Terminal_Draws_Configuration = m_mnu.AddItem(Cash_configuration_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7822), ENUM_FORM.FORM_TERMINAL_DRAWS_CONFIGURATION, AddressOf m_Machine_draws_configuration_click)
    End If

    ' FOS 21-JUL-2015 
    m_ticket_configuration = m_mnu.AddItem(Cash_configuration_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(6617), ENUM_FORM.FORM_TICKET_PRINT_CONFIGURATION, AddressOf m_ticket_configuration_click)

    'ESE 15-JUN-2016
    'Check if we need to add a new item in the menu
    If _obj_report_tool.ExistLocationMenu(GenericReport.LocationMenu.CashMenu) Then
      m_mnu.AddBreak(cls_menu)
      m_generic_reports_cash = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7342), ENUM_FORM.FORM_REPORT_TOOL_CASH_MENU, AddressOf m_report_tool_Click)
    End If

    ' 
    ' CAGE MENU
    '
    ' JCA 11-DEC-2013
    If Cage.IsCageEnabled Then
      ' Cash cage
      cls_menu = m_mnu.AddItem(GLB_NLS_GUI_PLAYER_TRACKING.Id(2876), ENUM_FORM.FORM_CAGE, , CLASS_GUI_RESOURCES.GetImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.CASH_CAGE.ToString.ToLower), , True)
      cls_menu.AddEvents(GetEnterImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.CASH_CAGE))

      ' RRR 23-SEP-2014
      ' Cage Global Report
      m_cage_global_report = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5531), ENUM_FORM.FORM_CAGE_SESSION_REPORT, AddressOf m_cage_global_report_Click)

      'HBB 18-SEP-2014 Cage meters
      ' Cash cage meters
      m_Cage_Meters = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5478), ENUM_FORM.FORM_CAGE_METERS, AddressOf m_cage_meters_click)

      m_mnu.AddBreak(cls_menu)

      ' Cash cage movements
      CAGE_Movements_sel = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(2999), ENUM_FORM.FORM_CAGE, AddressOf m_CAGE_movement_sel_Click)

      ' New transfer from cash cage
      CAGE_New = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(3001), ENUM_FORM.FORM_CAGE_CONTROL, AddressOf m_CAGE_new_Click)

      m_mnu.AddBreak(cls_menu)

      ' Cash cage sessions
      CAGE_Sessions = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(3379), ENUM_FORM.FORM_CAGE_SESSIONS, AddressOf m_CAGE_Sessions_Click)

      ' Cash cage count
      CAGE_CashCageCount = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(4352), ENUM_FORM.FORM_CAGE_COUNT, AddressOf m_CAGE_CashCageCount_Click)

      ' Cash cage stock
      CAGE_CashCageStock = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(4561), ENUM_FORM.FORM_CAGE_STOCK, AddressOf m_CAGE_CashCageStock_Click)

      m_mnu.AddBreak(cls_menu)

      If WSI.Common.Misc.IsNoteAcceptorEnabled() Or TITO.Utils.IsTitoMode() Then

        ' Stackers menu
        m_stackers_menu = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(4945), ENUM_FORM.FORM_MAIN)

        If WSI.Common.Misc.IsNoteAcceptorEnabled() Then
          ' SSC 03-FEB-2012
          ' Accepted Bills
          Terminal_money = m_mnu.AddItem(m_stackers_menu, GLB_NLS_GUI_INVOICING.Id(448), ENUM_FORM.FORM_TERMINAL_MONEY, AddressOf m_Terminal_money_Click)

          ' SSC 08-FEB-2012
          ' Bill History
          Money_pending_transfer = m_mnu.AddItem(m_stackers_menu, GLB_NLS_GUI_INVOICING.Id(35), ENUM_FORM.FORM_MONEY_PENDING_TRANSFER, AddressOf m_Money_pending_transfer_Click)

          ' SSC 17-ABR-2012
          ' Bill Collection Control
          Money_Collections = m_mnu.AddItem(m_stackers_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(641), ENUM_FORM.FORM_MONEY_COLLECTIONS, AddressOf m_Money_collections_Click)
        End If

        If TITO.Utils.IsTitoMode Then
          ' Stackers
          m_stacker_sel = m_mnu.AddItem(m_stackers_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(2162), ENUM_FORM.FORM_STACKERS_SEL, AddressOf m_stacker_sel_Click)
        End If

        ' HBB & DRV 12-MAY-2014
        ' Collection
        m_massive_collections = m_mnu.AddItem(m_stackers_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(4943), ENUM_FORM.FORM_STACKER_COLLECTION, AddressOf m_massive_collection_Click)

        ' Collection history
        m_stacker_collection_sel = m_mnu.AddItem(m_stackers_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(2222), ENUM_FORM.FORM_STACKER_COLLECTION_SEL, AddressOf m_stacker_collection_sel_Click)

        ' Collection file sel
        ' DHA 01-FEB-2016: when dual currency is enabled hide form
        If Not WSI.Common.Misc.IsFloorDualCurrencyEnabled() Then
          m_collection_file_sel = m_mnu.AddItem(m_stackers_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(6293), ENUM_FORM.FORM_COLLECTION_FILE_SEL, AddressOf m_collection_file_sel_Click)
        End If

        ' DRV 09-JAN-2014
        m_terminals_with_stacker = m_mnu.AddItem(m_stackers_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5827), ENUM_FORM.FORM_TERMINALS_WITH_STACKER, AddressOf m_terminals_with_stacker_Click)

        If TITO.Utils.IsTitoMode Then
          ' JML(4 - DEC - 2013)
          ' Collection balance report
          m_tito_collection_balance_report = m_mnu.AddItem(m_stackers_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(3335), ENUM_FORM.FORM_TITO_COLLECTION_BALANCE_REPORT, AddressOf m_tito_collection_balance_report_Click)

          ' JML 9-SEP-2014
          'Income report by machine and date
          collection_by_machine_and_date = m_mnu.AddItem(m_stackers_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5412), ENUM_FORM.FORM_COLLECTION_BY_MACHINE_AND_DATE, AddressOf m_collection_by_machine_and_date_Click)
        End If

        If WSI.Common.Misc.IsNoteAcceptorEnabled() Then
          m_mnu.AddBreak(m_stackers_menu)

          ' Bill validator Configuration
          Currencies = m_mnu.AddItem(m_stackers_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5712), ENUM_FORM.FORM_CURRENCIES, AddressOf m_currency_click)
        End If

        m_mnu.AddBreak(cls_menu)
      End If

      'AMF 09-APR-2015
      CAGE_Safe_Keeping_menu = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(6101), ENUM_FORM.FORM_MAIN, Nothing)
      CAGE_Safe_Keeping_sel = m_mnu.AddItem(CAGE_Safe_Keeping_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(6102), ENUM_FORM.FORM_CAGE_SAFE_KEEPING_SEL, AddressOf m_CAGE_SafeKeeping_sel_Click)

      'SGB 14-APR-2015
      CAGE_Safe_Keeping_movement_sel = m_mnu.AddItem(CAGE_Safe_Keeping_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(6179), ENUM_FORM.FORM_CAGE_SAFE_KEEPING_MOVEMENT_SEL, AddressOf m_CAGE_SafeKeeping_movement_sel_Click)

      m_mnu.AddBreak(cls_menu)

      ' Customize
      CAGE_Configuration = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5743), ENUM_FORM.FORM_CAGE)

      ' Denominations
      CAGE_Amounts = m_mnu.AddItem(CAGE_Configuration, GLB_NLS_GUI_PLAYER_TRACKING.Id(4374), ENUM_FORM.FORM_CAGE_AMOUNTS, AddressOf m_CAGE_amounts_Click)

      ' Sources and destinations
      CAGE_Source_Target = m_mnu.AddItem(CAGE_Configuration, GLB_NLS_GUI_PLAYER_TRACKING.Id(4375), ENUM_FORM.FORM_CAGE_SOURCE_TARGET, AddressOf m_CAGE_Source_Target_new_Click)

      ' OPC 16-SEP-2014
      ' Cash cage items
      m_cage_sources_target_concepts = m_mnu.AddItem(CAGE_Configuration, GLB_NLS_GUI_PLAYER_TRACKING.Id(5749), ENUM_FORM.FORM_CAGE_SOURCE_TARGET_CONCEPTS, AddressOf m_cage_sources_target_concepts_Click)

      'SGB 18-AUG-2014
      If GeneralParam.GetBoolean("Cage", "BillCounter.Enabled", True) Then

        m_mnu.AddBreak(CAGE_Configuration)

        ' Bill counter
        m_counter_configuration = m_mnu.AddItem(CAGE_Configuration, GLB_NLS_GUI_PLAYER_TRACKING.Id(5267), ENUM_FORM.FORM_COUNTER_CONFIGURATION, AddressOf m_counter_configuration_Click)
      End If

      'ESE 15-JUN-2016
      'Check if we need to add a new item in the menu
      If _obj_report_tool.ExistLocationMenu(GenericReport.LocationMenu.CageMenu) Then
        m_mnu.AddBreak(cls_menu)
        'Generic reports
        m_generic_reports_cage = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7343), ENUM_FORM.FORM_REPORT_TOOL_CAGE_MENU, AddressOf m_report_tool_Click)
      End If

      If GeneralParam.GetBoolean("FBM", "Enabled", False) Then
        m_mnu.AddBreak(cls_menu)
        CAGE_FBM_LOG_sel = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(8666), ENUM_FORM.FORM_FBM_LOG_SEL, AddressOf m_Click_FBM_Log_View)
      End If
    End If

    '
    ' ACCOUNTING MENU
    '
    cls_menu = m_mnu.AddItem(GLB_NLS_GUI_INVOICING.Id(451), ENUM_FORM.FORM_MAIN, , CLASS_GUI_RESOURCES.GetImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.ACCOUNTING.ToString.ToLower), , True)
    cls_menu.AddEvents(GetEnterImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.ACCOUNTING))

    If TITO.Utils.IsTitoMode And Not WSI.Common.Misc.IsMico2Mode() Then

      ' Ticket (TITO)
      Accounting_TITO_Menu = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(2303), ENUM_FORM.FORM_MAIN)

      'Add Circulating Cash form
      m_circulating_cash = m_mnu.AddItem(Accounting_TITO_Menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(3322), ENUM_FORM.FORM_CASH_TITO_SEL, AddressOf m_circulating_cash_Click)

      'Add ticket report form
      m_ticket_tito_report = m_mnu.AddItem(Accounting_TITO_Menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(2124), ENUM_FORM.FORM_TITO_TICKET_REPORT, AddressOf m_ticket_report_Click)

      'JRC 26-10-2016
      m_ticket_audit_report = m_mnu.AddItem(Accounting_TITO_Menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7674), ENUM_FORM.FORM_TICKET_AUDIT_SEL_SEVERAL, AddressOf m_tito_ticket_audit_Click)

      ' ACM 13-DEC-2013 Add tito summary sel form
      ' Activity summary
      m_tito_summary_sel = m_mnu.AddItem(Accounting_TITO_Menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1403), ENUM_FORM.FORM_TITO_SUMMARY_SEL, AddressOf m_tito_summary_sel_Click)

#If DEBUG Then
      'HBB 20-OCT-2014
      ' Soft Count
      m_terminal_softcount = m_mnu.AddItem(Accounting_TITO_Menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5638), ENUM_FORM.FORM_TERMINAL_SOFTCOUNT, AddressOf m_terminal_softcount_Click)
#End If

      ' SGB 24-FEB-2015: Don't show ticket out report if we are in TITO or General Param is 1
      If Not GeneralParam.GetBoolean("WigosGUI", "HideTicketOutReport", True) Then
        m_TITO_ticket_out_report = m_mnu.AddItem(Accounting_TITO_Menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5805), ENUM_FORM.FORM_TICKET_OUT_REPORT, AddressOf m_TITO_ticket_out_report_Click)
      End If

      m_mnu.AddBreak(Accounting_TITO_Menu)

      m_tito_params = m_mnu.AddItem(Accounting_TITO_Menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1063), ENUM_FORM.FORM_TITO_PARAMS_EDIT, AddressOf m_tito_params_Click)

      m_mnu.AddBreak(cls_menu)
    End If

    ' Tax summary
    Acct_taxes = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_INVOICING.Id(452), ENUM_FORM.FORM_ACCT_TAXES, AddressOf m_Acct_taxes_Click)

    ' RCI 03-JUN-2014: Hide TaxReport
    If Not GeneralParam.GetBoolean("WigosGUI", "HideTaxReport", True) Then
      ' Tax report
      Taxes_report = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_INVOICING.Id(346), ENUM_FORM.FORM_TAXES_REPORT, AddressOf m_Taxes_report_Click)
    End If

    ' JMM 23-JAN-2012
    ' Winnings taxes
    Prize_taxes_report = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_INVOICING.Id(449), ENUM_FORM.FORM_PRIZE_TAXES_REPORT, AddressOf m_Prize_taxes_Click)

    If WSI.Common.Misc.IsCashlessMode() Then
      ' JML 20-SEP-2012
      ' Withdrawal report
      Retreats_report = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1360), ENUM_FORM.FORM_RETREATS_REPORT, AddressOf m_Retreats_report_Click)

      ' AMF 28-JUN-2013
      ' Service charge report
      Service_Charge_report = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(2214), ENUM_FORM.FORM_SERVICE_CHARGE_REPORT, AddressOf m_Service_Charge_report_Click)
    End If

    m_mnu.AddBreak(cls_menu)

    ' Provider activity
    Provider_day_terminal = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_INVOICING.Id(465), ENUM_FORM.FORM_PROVIDER_DAY_TERMINAL, AddressOf m_Provider_day_terminal_Click)

    ' RGR 27-NOV-2015
    If TITO.Utils.IsTitoMode Then
      m_terminal_equity_report = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(6958), ENUM_FORM.FORM_TERMINAL_EQUITY_REPORT_SEL, AddressOf m_Terminal_Equity_Report_Click)
    End If

    ' MBF 13-DEC-2010
    ' Statistics balance report
    Gap_report = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_CONFIGURATION.Id(482), ENUM_FORM.FORM_SITE_GAP_REPORT, AddressOf m_Statistics_providers_gap_Click)

    ' Provider / cash activity
    ' DHA 01-FEB-2016: when dual currency is enabled hide form
    If Not WSI.Common.Misc.IsFloorDualCurrencyEnabled() Then
      ' JML 23-MAY-2012
      Cash_report = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_CONFIGURATION.Id(486), ENUM_FORM.FORM_CASH_REPORT, AddressOf m_Cash_report_Click)
    End If

    ' OPC 23-JAN-2015
    ' Flash report
    If Not GeneralParam.GetBoolean("WigosGUI", "HideProviderCashDailyReport", True) Then
      Flash_Report = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5863), ENUM_FORM.FORM_FLASH_REPORT, AddressOf m_flash_report_Click)
    End If

    ' MPO 23-NOV-2011
    ' Machine input and output balance report
    Tf_Gap_report = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_STATISTICS.Id(266), ENUM_FORM.FORM_TF_GAP_REPORT, AddressOf m_Transfering_gap_report_Click)

    'ECP 01-OCT-2015 Backlog Item 4158 GUI - Manual pays balance report
    m_comparation_manual_pays = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(6781), ENUM_FORM.FORM_COMPARATION_MANUAL_PAYS, AddressOf m_comparation_manual_pays_Click)

    m_mnu.AddBreak(cls_menu)

    ' DLL 19-JUN-2013
    ' Exchange rate history
    Currency_Exchange_Audit = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(2082), ENUM_FORM.FORM_CURRENCY_EXCHANGE_AUDIT, AddressOf m_currency_exchange_audit_Click)

    If WSI.Common.Misc.IsCashlessMode() AndAlso Not GeneralParam.GetBoolean("AntiMoneyLaundering", "ExternalControl.Enabled", False) Then
      m_mnu.AddBreak(cls_menu)

      ' Money laundering menu
      Money_Laundering_Report = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2409, Accounts.getAMLName()), ENUM_FORM.FORM_MONEY_LAUNDERING_REPORT_PRIZE)

      ' Prizes
      Money_Laundering_Report_Prize = m_mnu.AddItem(Money_Laundering_Report, GLB_NLS_GUI_PLAYER_TRACKING.Id(2522), ENUM_FORM.FORM_MONEY_LAUNDERING_REPORT_PRIZE, AddressOf m_Money_laundering_report_prize_Click)

      ' Recharges
      Money_Laundering_Report_Recharge = m_mnu.AddItem(Money_Laundering_Report, GLB_NLS_GUI_PLAYER_TRACKING.Id(2521), ENUM_FORM.FORM_MONEY_LAUNDERING_REPORT_RECHARGE, AddressOf m_Money_laundering_report_recharge_Click)
    End If

    m_mnu.AddBreak(cls_menu)

    'CAGE TEAM
    'SGB 22-APR-2015
    'Loss/Win Statement
    If GeneralParam.GetBoolean("WinLossStatement", "Enabled", False) Then
      ACCOUNT_Statement_menu = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(6222), ENUM_FORM.FORM_MAIN, Nothing)

      'RAB 11-JAN-2015
      ACCOUNT_Statement_print = m_mnu.AddItem(ACCOUNT_Statement_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7058), ENUM_FORM.FORM_STATEMENT_PRINT, AddressOf m_ACCOUNT_Statement_print_Click)

      ACCOUNT_Statement_document_edit = m_mnu.AddItem(ACCOUNT_Statement_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7057), ENUM_FORM.FORM_STATEMENT_DOCUMENT_EDIT, AddressOf m_ACCOUNT_Statement_document_edit_Click)

      m_mnu.AddBreak(cls_menu)
    End If

    If WSI.Common.Misc.IsAGGEnabled Then
      m_billing_menu = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(8932), ENUM_FORM.FORM_MAIN, Nothing)
      m_lottery_meters_adjustment_working_day = m_mnu.AddItem(m_billing_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(8932), ENUM_FORM.FORM_LOTTERY_METERS_ADJUSTMENT_WORKING_DAY, AddressOf m_lottery_meters_adjustment_working_day_Click)
      m_mnu.AddBreak(m_billing_menu)
      m_report_profit_day = m_mnu.AddItem(m_billing_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(8955), ENUM_FORM.FORM_LOTTERY_REPORT_PROFIT_DAY, AddressOf m_report_profit_day_Click)
      m_report_profit_month = m_mnu.AddItem(m_billing_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(8956), ENUM_FORM.FORM_LOTTERY_REPORT_PROFIT_MONTH, AddressOf m_report_profit_month_Click)
      m_report_profit_machine_and_day = m_mnu.AddItem(m_billing_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(8976), ENUM_FORM.FORM_LOTTERY_REPORT_PROFIT_MACHINE_DAY, AddressOf m_report_profit_machine_and_day_Click)
      m_mnu.AddBreak(cls_menu)
    End If

    ' Expired credit
    Expired_Credits = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_INVOICING.Id(469), ENUM_FORM.FORM_EXPIRED_CREDITS, AddressOf m_Expired_credits_Click)

    ' JAB 06-MAR-2012
    ' Export...
    Form_report = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_STATISTICS.Id(480), ENUM_FORM.FORM_REPORTS, AddressOf m_Forms_report_Click)

    If Not GeneralParam.GetBoolean("WigosGUI", "HideTITOHoldvsWinReport", True) AndAlso TITO.Utils.IsTitoMode() Then
      Tito_holdvswin_report = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7193), ENUM_FORM.FORM_TITO_HOLD_VS_WIN_REPORT, AddressOf m_tito_holdvswin_report_Click)
      'Machine_and_game_report_menu = m_mnu.AddItem(Statistics_terminals_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5820), ENUM_FORM.FORM_MACHINE_AND_GAME_REPORT, AddressOf m_machine_and_game_report_Click)
    End If

    'ESE 15-JUN-2016
    'Check if we need to add a new item in the menu
    If _obj_report_tool.ExistLocationMenu(GenericReport.LocationMenu.AccountingMenu) Then
      m_mnu.AddBreak(cls_menu)
      m_generic_reports_accounting = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7344), ENUM_FORM.FORM_REPORT_TOOL_ACCOUNTING_MENU, AddressOf m_report_tool_Click)
    End If

    '
    ' CUSTOMER MENU
    '

    cls_menu = m_mnu.AddItem(GLB_NLS_GUI_AUDITOR.Id(472), ENUM_FORM.FORM_MAIN, , CLASS_GUI_RESOURCES.GetImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.CUSTOMERS.ToString.ToLower), , True)
    cls_menu.AddEvents(GetEnterImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.CUSTOMERS))

    ' Account summary
    Card_summary = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_INVOICING.Id(455), ENUM_FORM.FORM_ACCOUNT_SUMMARY, AddressOf m_Card_summary_Click)

    ' Account movements 
    Card_movements = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_INVOICING.Id(456), ENUM_FORM.FORM_ACCOUNT_MOVEMENTS, AddressOf m_Card_movements_Click)

    ' RMS 21-OCT-2013
    ' Player card changes
    Account_Card_Changes = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(2804), ENUM_FORM.FORM_ACCOUNT_CARD_CHANGES, AddressOf m_Account_Card_Changes_Click)

    m_mnu.AddBreak(cls_menu)

    ' Threshold movements report
    If Not (GeneralParam.GetBoolean("Client.Registration", "HideThresholdMovementsReport", True)) Then
      Form_threshold_movements = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(8776), ENUM_FORM.FORM_THRESHOLD_MOVEMENTS, AddressOf m_FormThresholdMovementReport_Click)
    End If

    ' Manual assignment report
    Card_details = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1257), ENUM_FORM.FORM_ACCOUNT_MOVEMENTS_DETAILS, AddressOf m_Card_details_Click)

    m_reserved_maquines = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7497), ENUM_FORM.FORM_RESERVED_MACHINES, AddressOf m_reserved_maquines_Click)

    If WSI.Common.Misc.IsReceptionEnabled() Then

      ' Customize
      m_menu_reception = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7036), ENUM_FORM.FORM_CAGE)

      ' Customer Visit report
      Customer_Visits = m_mnu.AddItem(m_menu_reception, GLB_NLS_GUI_PLAYER_TRACKING.Id(7295), ENUM_FORM.FORM_CUSTOMER_ENTRANCES_REPORT, AddressOf m_customer_visits_Click)

      ' Customer entrances by visits report
      m_customer_entrance_report_visits = m_mnu.AddItem(m_menu_reception, GLB_NLS_GUI_PLAYER_TRACKING.Id(7293), ENUM_FORM.FORM_CUSTOMER_ENTRANCES_REPORT_VISITS, AddressOf m_Customer_Entrance_Visits_Click)

      'Customer entrances Barred List report
      m_customer_entrance_barred_list = m_mnu.AddItem(m_menu_reception, GLB_NLS_GUI_PLAYER_TRACKING.Id(7478), ENUM_FORM.FORM_CUSTOMER_ENTRANCES_BARRED_LIST, AddressOf m_Customer_Entrance_Barred_Report_Click)

      m_mnu.AddBreak(m_menu_reception)

      'Customer Entrance price
      If WSI.Common.Entrances.Entrance.GetReceptionMode = Entrances.Enums.ENUM_RECEPTION_MODE.TicketWithPrice Then
        m_customer_entrance_price = m_mnu.AddItem(m_menu_reception, GLB_NLS_GUI_PLAYER_TRACKING.Id(7294), ENUM_FORM.FORM_CUSTOMER_ENTRANCES_PRICES, AddressOf m_customer_entrance_price_Click)
      End If

    End If

    If (GeneralParam.GetBoolean("CreditLine", "Enabled", False)) Then
      ' MS  14-MAR-2017
      m_mnu.AddBreak(cls_menu)

      'Credit Line
      Creditline_menu = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7945), ENUM_FORM.FORM_CAGE)

      ' Creditline Selection
      Form_Creditline_sel = m_mnu.AddItem(Creditline_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7945), ENUM_FORM.FORM_CREDITLINE_SEL, AddressOf m_FormCreditlineSel_Click) 'AddressOf m_FormCreditlineSel_Click)
      Form_Creditline_report = m_mnu.AddItem(Creditline_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7994), ENUM_FORM.FORM_CREDITLINE_MOVEMENTS, AddressOf m_FormCreditlineMovementReport_Click) 'AddressOf m_FormCreditlineMovementReport_Click)
    End If

    'ADI 30-MAR-2016
    ' m_countr_sessions = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7244), ENUM_FORM.FORM_COUNTR_SESSIONS, AddressOf m_countr_sessions_Click)
    m_mnu.AddBreak(cls_menu)

    ' Import Menu RXM 16-AGO-2012
    ' Import
    Import_menu = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5747), ENUM_FORM.FORM_CURRENCIES, Nothing)

    'RXM 01-AGO-2012
    ' Accounts
    Accounts_Import = m_mnu.AddItem(Import_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1237), ENUM_FORM.FORM_ACCOUNTS_IMPORT, AddressOf m_Accounts_Import_Click)

    If Not GeneralParam.GetBoolean("Site", "MultiSiteMember") OrElse (GeneralParam.GetBoolean("Site", "MultiSiteMember") AndAlso GeneralParam.GetInt32("MultiSite", "PlayerTracking.Mode") = WSI.Common.PlayerTracking_Mode.Site) Then
      ' Points
      Points_import = m_mnu.AddItem(Import_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(205), ENUM_FORM.FORM_POINTS_IMPORT, AddressOf m_Points_Import_Click)
    End If

    'YNM 14-DIC-2015
    If WSI.Common.Misc.IsReceptionEnabled() Then ' Blacklist Import
      Blacklist_Import = m_mnu.AddItem(Import_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7126), ENUM_FORM.FROM_BLACKLIST_IMPORT, AddressOf m_blacklist_import_Click)
    End If

    m_mnu.AddBreak(cls_menu)

    ' Customize
    Customers_configuration_menu = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5743), ENUM_FORM.FORM_CAGE)

    ' RRR 05-AUG-2013
    'Player data
    Player_Edit_Config = m_mnu.AddItem(Customers_configuration_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(2478), ENUM_FORM.FORM_PLAYER_EDIT_CONFIG, AddressOf m_Player_edit_config_Click)

    ' Player PIN request
    AccountRequestPin = m_mnu.AddItem(Customers_configuration_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(4758), ENUM_FORM.FORM_ACCOUNT_REQUEST_PIN, AddressOf m_AccountRequestPin_Click) 'AddressOf m_Click_Profiles)

    'ESE 15-JUN-2016
    'Check if we need to add a new item in the menu
    If _obj_report_tool.ExistLocationMenu(GenericReport.LocationMenu.CustomerMenu) Then
      m_mnu.AddBreak(cls_menu)
      m_generic_reports_customers = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7345), ENUM_FORM.FORM_REPORT_TOOL_CUSTOMER_MENU, AddressOf m_report_tool_Click)
    End If

    ' 
    ' MARKETING MENU // RCI 10-MAY-2010
    '
    cls_menu = m_mnu.AddItem(GLB_NLS_GUI_PLAYER_TRACKING.Id(5748), ENUM_FORM.FORM_MAIN, , CLASS_GUI_RESOURCES.GetImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.MARKETING.ToString.ToLower), , True)
    cls_menu.AddEvents(GetEnterImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.MARKETING))

    ' Promotions menu
    Marketing_promotions_menu = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(461), ENUM_FORM.FORM_MAIN)

    ' Promotions
    Menu_Promotions = m_mnu.AddItem(Marketing_promotions_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(462), ENUM_FORM.FORM_PROMOTIONS, AddressOf m_Promotions_Click)

    ' Recurring promotions
    Menu_Periodical_Promotions = m_mnu.AddItem(Marketing_promotions_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(438), ENUM_FORM.FORM_PROMOTIONS, AddressOf m_Periodical_Promotions_Click)

    ' Preassigned promotions
    Menu_Preassigned_Promotions = m_mnu.AddItem(Marketing_promotions_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1236), ENUM_FORM.FORM_PROMOTIONS, AddressOf m_Preassigned_Promotions_Click)

    m_mnu.AddBreak(Marketing_promotions_menu)

    ' DDM 27-JUN-2012
    ' Promotion report
    Promotion_Report = m_mnu.AddItem(Marketing_promotions_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(808), ENUM_FORM.FORM_PROMOTION_REPORTS, AddressOf m_Promotion_Reports_Click)

    ' DDM 28-AUG-2012: promotion delivered.
    ' Awarded promotion report
    Promotion_Delivered_Report = m_mnu.AddItem(Marketing_promotions_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1271), ENUM_FORM.FORM_PROMOTIONS_DELIVERED_REPORT, AddressOf m_Promotions_Delivered_Report_Click)

    m_mnu.AddBreak(Marketing_promotions_menu)

    ' Flags
    Menu_Flags = m_mnu.AddItem(Marketing_promotions_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1258), ENUM_FORM.FORM_FLAGS_SEL, AddressOf m_Flags_Sel_Click)

    'XCD 14-SEP-2012
    ' Account flag report
    Account_Flags = m_mnu.AddItem(Marketing_promotions_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1259), ENUM_FORM.FORM_ACCOUNT_FLAGS, AddressOf m_Account_Flags_Click)

    ' Raffles menu
    Marketing_raffles_menu = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(463), ENUM_FORM.FORM_MAIN)

    ' Raffles
    Menu_Draws = m_mnu.AddItem(Marketing_raffles_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(463), ENUM_FORM.FORM_DRAWS, AddressOf m_Draws_Click)

    '' PromoGames
    'If (WSI.Common.Misc.IsInTouchGamesEnabled()) Then
    '  m_mnu.AddBreak(Marketing_promotions_menu)

    '  Promogame = m_mnu.AddItem(Marketing_promotions_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(8541), ENUM_FORM.FORM_PROMOGAME, AddressOf m_promogame_Click)
    'End If

    'JAR 25-MAR-2013
    ' Raffle ticket report
    Report_draw_tickets = m_mnu.AddItem(Marketing_raffles_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1810), ENUM_FORM.FORM_REPORT_DRAW_TICKET, AddressOf m_Report_draw_tickets)


    ' Loyalty program menu
    Marketing_loyalty_program_menu = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_CONFIGURATION.Id(292), ENUM_FORM.FORM_MAIN)

    ' SMN 03-MAY-2013
    ' Catalog
    Menu_GiftsCatalog = m_mnu.AddItem(Marketing_loyalty_program_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5751), ENUM_FORM.FORM_GIFTS_SEL, AddressOf m_gifts_catalog_Click)

    ' History 
    Menu_GiftsHistory = m_mnu.AddItem(Marketing_loyalty_program_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5752), ENUM_FORM.FORM_GIFTS_HISTORY, AddressOf m_gifts_history_Click)

    ' JPJ 09-OCT-2013
    ' Credits
    Menu_TableToPoints = m_mnu.AddItem(Marketing_loyalty_program_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5753), ENUM_FORM.FORM_POINTS_TO_CREDITS, AddressOf m_Points_to_Credits_Click)

    '' JRC 09-DIC-2015
    '' Buckets selection
    'If (GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode", 0) = 0) Then 'Buckets SPACE
    '  Bucket_sel = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(6967), ENUM_FORM.FORM_BUCKET_SEL, AddressOf m_bucket_sel_click)
    'End If

    'XGJ 09-AGO-2016
    ' Buckets menu
    If (GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode", 0) = 0) Then 'Buckets SPACE
      Marketing_buckets_menu = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(6968), ENUM_FORM.FORM_MAIN)
      Bucket_sel = m_mnu.AddItem(Marketing_buckets_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(6967), ENUM_FORM.FORM_BUCKET_SEL, AddressOf m_bucket_sel_click)
      Buckets_multiplier_sel = m_mnu.AddItem(Marketing_buckets_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7865), ENUM_FORM.FORM_BUCKETS_MULTIPLIER_SEL, AddressOf m_buckets_multiplier_sel_click)
    End If

    If (JunketsBusinessLogic.IsJunketsEnabled) Then
      m_menu_junkets = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7997), ENUM_FORM.FORM_CAGE)

      m_junkets = m_mnu.AddItem(m_menu_junkets, GLB_NLS_GUI_PLAYER_TRACKING.Id(7997), ENUM_FORM.FORM_JUNKETS_SEL, AddressOf m_FormJunketsSel_Click)
      m_junkets_representatives = m_mnu.AddItem(m_menu_junkets, GLB_NLS_GUI_PLAYER_TRACKING.Id(8089), ENUM_FORM.FORM_JUNKETS_REPRESENTATIVES_SEL, AddressOf m_FormJunketsRepresentativesSel_Click)
      m_junkets_report = m_mnu.AddItem(m_menu_junkets, GLB_NLS_GUI_PLAYER_TRACKING.Id(8268), ENUM_FORM.FORM_JUNKET_REPORT, AddressOf m_FormJunketsReport_Click)
      m_junkets_report_flyers = m_mnu.AddItem(m_menu_junkets, GLB_NLS_GUI_PLAYER_TRACKING.Id(8269), ENUM_FORM.FORM_JUNKET_REPORT_FLYERS, AddressOf m_FormJunketsReportFlyers_Click)
    End If

    m_mnu.AddBreak(cls_menu)

    ' Advertising campaigns
    Advertisement = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(950), ENUM_FORM.FORM_ADVERTISEMENT, AddressOf m_Advertisement_Click)

    ' DHA 01-DEC-2014
    ' LCD messages
    LcdMessageSel = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5770), ENUM_FORM.FORM_LCD_MESSAGE, AddressOf m_Click_lcd_message)

    ' JML 14-JUN-2012      TODO
    ' Providers excluded from non-redeemable credit
    Provider = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1054), ENUM_FORM.FORM_PROVIDER_DATA, AddressOf m_provider_click)

    'ESE 15-JUN-2016
    'Check if we need to add a new item in the menu
    If _obj_report_tool.ExistLocationMenu(GenericReport.LocationMenu.MarketingMenu) Then
      m_mnu.AddBreak(cls_menu)
      m_generic_reports_marketing = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7346), ENUM_FORM.FORM_REPORT_TOOL_MARKETING_MENU, AddressOf m_report_tool_Click)
    End If

    ' JRC 09-DIC-2015
    ' report buckets by client removed temporaly

    ' m_report_buckets_by_client = m_mnu.AddItem(Bucket, GLB_NLS_GUI_PLAYER_TRACKING.Id(6990), ENUM_FORM.FORM_REPORT_BUCKET_BY_CLIENT, AddressOf m_report_buckets_by_client_Click)
    ' TJG 25-JUL-2008
    ' 
    ' JACKPOTS MENU
    '
    cls_menu = m_mnu.AddItem(GLB_NLS_GUI_CLASS_II.Id(453), ENUM_FORM.FORM_MAIN, , CLASS_GUI_RESOURCES.GetImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.JACKPOT.ToString.ToLower), , True)
    cls_menu.AddEvents(GetEnterImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.JACKPOT))

    ' RCI 03-DEC-2010
    ' 466 "Jackpot de Sala"
    Site_jackpot = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_JACKPOT_MGR.Id(466), ENUM_FORM.FORM_SITE_JACKPOT_CONFIGURATION, Nothing)

    ' 468 "Configuraci�n"
    Site_jackpot_configuration = m_mnu.AddItem(Site_jackpot, GLB_NLS_GUI_JACKPOT_MGR.Id(468), ENUM_FORM.FORM_SITE_JACKPOT_CONFIGURATION, AddressOf m_Site_Jackpot_configuration_Click)

    ' 469 "Hist�rico"
    Site_jackpot_history = m_mnu.AddItem(Site_jackpot, GLB_NLS_GUI_JACKPOT_MGR.Id(469), ENUM_FORM.FORM_SITE_JACKPOT_HISTORY, AddressOf m_Site_Jackpot_history_Click)

    If WSI.Common.GeneralParam.GetBoolean("SiteJackpot", "Bonusing.Enabled") Then
      ' 470 "Bonus"
      Site_bonuses = m_mnu.AddItem(Site_jackpot, GLB_NLS_GUI_JACKPOT_MGR.Id(470), ENUM_FORM.FORM_BONUSES, AddressOf m_Site_Bonuses_Click)
      m_bonuses_meters_comparation = m_mnu.AddItem(Site_jackpot, GLB_NLS_GUI_PLAYER_TRACKING.Id(5028), ENUM_FORM.FORM_BONUSES_METERS_COMPARATION, AddressOf m_bonuses_meters_comparation_click)
    End If

    ' 467 "Jackpot WIN"
    Class_II_jackpot = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_JACKPOT_MGR.Id(467), ENUM_FORM.FORM_CLASS_II_JACKPOT_CONFIGURATION, Nothing)

    ' 454 "Configuraci�n"
    Jackpot_configuration = m_mnu.AddItem(Class_II_jackpot, GLB_NLS_GUI_CLASS_II.Id(454), ENUM_FORM.FORM_CLASS_II_JACKPOT_CONFIGURATION, AddressOf m_Jackpot_configuration_Click)

    ' 459 "Mensajes promocionales"
    Jackpot_promo_messages = m_mnu.AddItem(Class_II_jackpot, GLB_NLS_GUI_CLASS_II.Id(459), ENUM_FORM.FORM_CLASS_II_JACKPOT_PROMO_MESSAGE, AddressOf m_Jackpot_promo_messages_Click)

    ' 455 "Hist�rico"
    Jackpot_history = m_mnu.AddItem(Class_II_jackpot, GLB_NLS_GUI_CLASS_II.Id(455), ENUM_FORM.FORM_CLASS_II_JACKPOT_HISTORY, AddressOf m_Jackpot_history_Click)

    ' 456 "Monitor"
    Jackpot_monitor = m_mnu.AddItem(Class_II_jackpot, GLB_NLS_GUI_CLASS_II.Id(456), ENUM_FORM.FORM_CLASS_II_JACKPOT_MONITOR, AddressOf m_Jackpot_monitor_Click)

    ' JMM 05-AUG-2014: Progressive Jackpots
    Progressive_jackpots_menu = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5202), Nothing, Nothing)
    Progressive_jackpots_sel = m_mnu.AddItem(Progressive_jackpots_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5202), ENUM_FORM.FORM_PROGRESSIVE_SEL, AddressOf m_Progressive_click)
    Provisions = m_mnu.AddItem(Progressive_jackpots_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5204), ENUM_FORM.FORM_PROVISIONS_EDIT, AddressOf m_Provisions_click)
    Provisions_report = m_mnu.AddItem(Progressive_jackpots_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5206), ENUM_FORM.FORM_PROVISIONS_REPORT, AddressOf m_Provisions_report_click)
    Progressive_jackpots_paid_report = m_mnu.AddItem(Progressive_jackpots_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5207), ENUM_FORM.FORM_PROGRESSIVE_PAID_REPORT, AddressOf m_Progressive_paid_report_click)

    If JackpotBusinessLogic.IsJackpotsEnabled() Then
      'RLO 30-MAR-2017
      m_menu_jackpot = m_mnu.AddItem(Progressive_jackpots_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7999), ENUM_FORM.FORM_JACKPOT_TREVIEW_EDIT, AddressOf m_FormJackpots_Click)

      'JBP 02-MAY-2017
      m_menu_jackpot_history_meters = m_mnu.AddItem(Progressive_jackpots_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(8235), ENUM_FORM.FORM_JACKPOT_METERS_HISTORY_SEL, AddressOf m_FormJackpotsHistoryMeters_Click)

      'CCG 05-MAY-2017
      m_menu_jackpot_history = m_mnu.AddItem(Progressive_jackpots_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(8265), ENUM_FORM.FORM_JACKPOT_HISTORY_SEL, AddressOf m_FormJackpotsHistory_Click)

      'CCG 16-MAY-2017
      m_menu_jackpot_viewers = m_mnu.AddItem(Progressive_jackpots_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(8286), ENUM_FORM.FORM_JACKPOT_VIEWER_TREEVIEW_EDIT, AddressOf m_FormJackpotViewer_Click)
    End If

    'ESE 15-JUN-2016
    'Check if we need to add a new item in the menu
    If _obj_report_tool.ExistLocationMenu(GenericReport.LocationMenu.JackpotsMenu) Then
      m_mnu.AddBreak(cls_menu)
      m_generic_reports_jackpot = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7347), ENUM_FORM.FORM_REPORT_TOOL_JACKPOTS_MENU, AddressOf m_report_tool_Click)
    End If

    ' DHA 28-OCT-2014
    ' 
    ' GAMING TABLE MENU
    '
    If GamingTableBusinessLogic.IsFeaturesGamingTablesEnabled Then

      ' Gambling tables
      cls_menu = m_mnu.AddItem(GLB_NLS_GUI_PLAYER_TRACKING.Id(5837), ENUM_FORM.FORM_MAIN, , CLASS_GUI_RESOURCES.GetImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.GAMBLING_TABLES.ToString.ToLower), , True)
      cls_menu.AddEvents(GetEnterImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.GAMBLING_TABLES))

      If GamingTableBusinessLogic.IsGamingTablesEnabled Then
        ' Income control report
        Gaming_Tables_Income_Report = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(4471), ENUM_FORM.FORM_GAMING_TABLES_INCOME_REPORT, AddressOf m_gaming_tables_income_report_Click)
        ' Chip purchase/sale report
        Chips_Operations_Report = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(4443), ENUM_FORM.FORM_CHIPS_OPERATIONS_REPORT, AddressOf m_chips_operations_report_Click)
        ' Detail purchases and sales 
        m_purchase_and_sales = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(6578), ENUM_FORM.FORM_PURCHASE_AND_SALES_DETAIL, AddressOf m_purchase_and_sales_Click)
        ' Gambling table sessions
        Gaming_Tables_Session = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(3431), ENUM_FORM.FORM_GAMING_TABLES_SESSIONS_SEL, AddressOf m_gaming_tables_sessions_Click)
        ' Cancelations report
        Gaming_Tables_Cancellations = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(4585), ENUM_FORM.FORM_GAMING_TABLES_CANCELLATIONS, AddressOf m_gaming_tables_cancellations_Click)

        'EOR 14-JUN-2016
#If DEBUG Then
        'EOR 09-MAY-2016 Activity Tables
        m_activity_tables_report = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7308), ENUM_FORM.FORM_ACTIVITY_TABLES, AddressOf m_activity_tables_Click)
#End If

        'EOR 26-SEP-2017
        m_tables_analisys_report = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(8702), ENUM_FORM.FORM_TABLE_ANALISYS_REPORT, AddressOf m_tables_analisys_report_Click)

        m_mnu.AddBreak(cls_menu)

        If WSI.Common.GamingTableBusinessLogic.IsEnabledGTPlayerTracking() Then
          ' Table gaming session report
          m_table_play_sessions = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(4967), ENUM_FORM.FORM_TABLE_PLAY_SESSIONS, AddressOf m_table_play_sessions_Click)
          ' Table gaming session editor
          m_table_play_sessions_edit = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(4989), ENUM_FORM.FORM_TABLE_PLAY_SESSIONS_EDIT, AddressOf m_table_play_sessions_edit_Click)

          ' DHA 05-FEB-2015: Table gaming sessions import
          m_table_play_sessions_import = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5882), ENUM_FORM.FORM_GAMING_TABLE_SESSIONS_IMPORT, AddressOf m_table_play_sessions_import_Click)

          ' Table gaming movements
          m_table_player_movements = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5049), ENUM_FORM.FROM_TABLE_PLAYER_MOVEMENTS, AddressOf m_table_player_movements_Click)
          ' Estimated activity report gaming tables
          m_player_tracking_income_report = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5493), ENUM_FORM.FORM_PLAYER_TRACKING_INCOME_REPORT, AddressOf m_player_tracking_income_report_Click)

          ' Global drop hourly report
          m_player_tracking_global_drop_hourly = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(8186), ENUM_FORM.FORM_GAMING_TABLES_GLOBAL_DROP_HOURLY, AddressOf m_player_tracking_global_drop_hourly_Click)

          ' Score report
          If WSI.Common.GamingTableBusinessLogic.IsEnabledGTPlayerTracking() And GamingTableBusinessLogic.IsEnabledGTWinLoss() Then
            Gaming_Tables_Score_Report = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(8458), ENUM_FORM.FORM_GAMING_TABLES_SCORE_REPORT, AddressOf m_gaming_tables_score_report_Click)
          End If

          ' Table players report
          m_table_payers_report = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(8668), ENUM_FORM.FORM_TABLE_PLAYERS_REPORT, AddressOf m_table_payers_report_Click)

          m_mnu.AddBreak(cls_menu)
        End If
      End If

      ' Customize
      Gaming_Tables_Configuration = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(5743), ENUM_FORM.FORM_GAMING_TABLES, Nothing)

      If GamingTableBusinessLogic.IsGamingTablesEnabled Then
        ' Gambling tables
        Gaming_Tables = m_mnu.AddItem(Gaming_Tables_Configuration, GLB_NLS_GUI_PLAYER_TRACKING.Id(3416), ENUM_FORM.FORM_GAMING_TABLES_SEL, AddressOf m_gaming_tables_Click)

        ' Gambling table types
        Gaming_Tables_Types = m_mnu.AddItem(Gaming_Tables_Configuration, GLB_NLS_GUI_PLAYER_TRACKING.Id(3428), ENUM_FORM.FORM_GAMING_TABLES_TYPES_SEL, AddressOf m_gaming_tables_type_Click)

        ' LTC 16-FEB-2016
        ' Gambling tables Chips 
        m_chips_sel = m_mnu.AddItem(Gaming_Tables_Configuration, GLB_NLS_GUI_PLAYER_TRACKING.Id(6577), ENUM_FORM.FORM_CHIPS_SETS_SEL, AddressOf m_chips_sets_sel_Click)

      End If

      ' Parameters
      Gaming_Tables_Params = m_mnu.AddItem(Gaming_Tables_Configuration, GLB_NLS_GUI_PLAYER_TRACKING.Id(4542), ENUM_FORM.FORM_GAMING_TABLES_CONFIG_EDIT, AddressOf m_gaming_tables_config_Click)

      'ESE 15-JUN-2016
      'Check if we need to add a new item in the menu
      If _obj_report_tool.ExistLocationMenu(GenericReport.LocationMenu.GamingTableMenu) Then
        m_mnu.AddBreak(cls_menu)
        m_generic_reports_gaming_table = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7348), ENUM_FORM.FORM_REPORT_TOOL_GAMINGTABLE_MENU, AddressOf m_report_tool_Click)
      End If
    End If
    'End If

    '
    ' TOURNAMENT MENU  // FAV 07-APR-2017
    ' DLL Tournaments Menu
    'If GamingTablesTournament.GamingTablesTournamentConfig.IsFeaturesGamingTablesTournamentsEnabled Then
    '    cls_menu = m_mnu.AddItem(GLB_NLS_GUI_PLAYER_TRACKING.Id(8070), ENUM_FORM.FORM_MAIN, , CLASS_GUI_RESOURCES.GetImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.TOURNAMENTS.ToString.ToLower), , True)
    '    cls_menu.AddEvents(GetEnterImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.TOURNAMENTS))

    '    m_gaming_table_tournament_cfg = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(8223), ENUM_FORM.FORM_GAMING_TABLES_TOURNAMENTS_CFG, AddressOf m_gaming_tables_tournament_cfg_Click)
    'End If

    'If GamingTablesTournament.GamingTablesTournamentConfig.IsGamingTablesTournamentsEnabled Then
    '    m_gaming_table_tournament = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(8071), ENUM_FORM.FORM_GAMING_TABLES_TOURNAMENTS_SEL, AddressOf m_gaming_tables_tournament_sel_Click)
    '    m_gaming_table_tournament_awards = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(8210), ENUM_FORM.FORM_GAMING_TABLES_TOURNAMENTS_AWARDS, AddressOf m_gaming_tables_tournament_awards_Click)
    '    m_gaming_table_tournament_players = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(8270), ENUM_FORM.FORM_GAMING_TABLES_TOURNAMENTS_PLAYERS, AddressOf m_gaming_tables_tournament_players_Click)
    'End If

    '
    ' SMARTFLOOR MENU //  JRC 22-OCT-2015
    '
    If WSI.Common.Misc.IsIntelliaEnabled() Then
      m_smartFloor_menu = m_mnu.AddItem(GLB_NLS_GUI_PLAYER_TRACKING.Id(6901), ENUM_FORM.FORM_MAIN, , CLASS_GUI_RESOURCES.GetImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.INTELLIA.ToString.ToLower), , True)
      m_smartFloor_menu.AddEvents(GetEnterImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.INTELLIA))

      m_intellia_floor_editor = m_mnu.AddItem(m_smartFloor_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(8002), ENUM_FORM.FORM_INTELLIA_FLOOR_EDITOR, AddressOf m_intellia_floor_editor_Click)
      m_smartfloor_report_runners = m_mnu.AddItem(m_smartFloor_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(6902), ENUM_FORM.FORM_SMARTFLOOR_RUNNERS_REPORT, AddressOf m_smartfloor_report_runners_Click)
      m_smartfloor_report_task = m_mnu.AddItem(m_smartFloor_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(6852), ENUM_FORM.FORM_SMARTFLOOR_REPORT_TASK, AddressOf m_smartfloor_report_task_Click)
      'JRC 16-NOV-2015
      'm_mnu.AddBreak(m_smartFloor_menu)
      'm_smartfloor_terminals_print_QR_Codes = m_mnu.AddItem(m_smartFloor_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(6955), ENUM_FORM.FORM_SMARTFLOOR_TERMINALS_PRINT_QR_CODES, AddressOf m_smartfloor_terminals_print_QR_Codes_Click)

      'PDM 22-JUN-2017
      m_intellia_filter_editor = m_mnu.AddItem(m_smartFloor_menu, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8427), ENUM_FORM.FORM_INTELLIA_FILTER_EDIT, AddressOf m_intellia_filter_editor_Click)

      'ESE 15-JUN-2016
      'Check if we need to add a new item in the menu
      If _obj_report_tool.ExistLocationMenu(GenericReport.LocationMenu.SmartFloorMenu) Then
        m_mnu.AddBreak(cls_menu)
        m_generic_reports_smartfloor = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7349), ENUM_FORM.FORM_REPORT_TOOL_SMARTFLOOR_MENU, AddressOf m_report_tool_Click)
      End If
    End If

    ' 
    ' GAMEGATEWAY MENU //  JCA 28-DEC-2015
    '
    If WSI.Common.Misc.IsGameGatewayMenuEnabled() Then
      m_gamegateway_menu = m_mnu.AddItem(WSI.Common.Resource.String("STR_UC_CARD_BALANCE_RESERVED"), ENUM_FORM.FORM_MAIN, , CLASS_GUI_RESOURCES.GetImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.GAMEGATEWAY.ToString.ToLower), , True)
      m_gamegateway_menu.AddEvents(GetEnterImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.GAMEGATEWAY))

      'm_mnu.AddItem(GeneralParam.GetString("GameGateway", "Name", "GameGateway"), ENUM_FORM.FORM_MAIN, , CLASS_GUI_RESOURCES.GetImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.GAMEGATEWAY.ToString.ToLower))

      ' RDS 19-NOV-2015
      ' Report bonoPLAY
      Statistics_reportbonoplay = m_mnu.AddItem(m_gamegateway_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(6938), ENUM_FORM.FORM_REPORT_BONOPLAY_SEL, AddressOf m_Statistics_bonoplay_Click)
      Statistics_reportbonoplay_played = m_mnu.AddItem(m_gamegateway_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7026), ENUM_FORM.FORM_REPORT_BONOPLAY_PLAYED_SEL, AddressOf m_Statistics_bonoplay_played_Click)

      'JRC 14-OCT-2015		Feature 4699:LottoRace
      If WSI.Common.Misc.IsGameGatewayEnabled() Then
        m_gamegateway_terminals = m_mnu.AddItem(m_gamegateway_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(6835), ENUM_FORM.FORM_GAMEGATEWAY_TERMINALS, AddressOf m_GameGateway_terminals_click)
      End If

      'ESE 15-JUN-2016
      'Check if we need to add a new item in the menu
      If _obj_report_tool.ExistLocationMenu(GenericReport.LocationMenu.GameGatewayMenu) Then
        m_mnu.AddBreak(cls_menu)
        m_generic_reports_gateway = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(7350), ENUM_FORM.FORM_REPORT_TOOL_GAMEGATEWAY_MENU, AddressOf m_report_tool_Click)
      End If
    End If

    '#If DEBUG Then

    ' 
    ' APPMAZING MENU
    ' 
    If WSI.Common.GeneralParam.GetBoolean("Features", "WinUP") Then
      cls_menu = m_mnu.AddItem(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7444), ENUM_FORM.FORM_MAIN, , CLASS_GUI_RESOURCES.GetImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.APPMAZING.ToString.ToLower), , True)
      cls_menu.AddEvents(GetEnterImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.APPMAZING))

      '1-Secciones 
      Dim m_game_sections_schema As GUI_Controls.CLASS_MENU_ITEM
      m_game_sections_schema = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7597), ENUM_FORM.FORM_MAIN, Nothing)
      '1.1 Esquema de secciones
      Section_Schema = m_mnu.AddItem(m_game_sections_schema, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7596), ENUM_FORM.FORM_SECTION_SCHEMA_SEL, AddressOf m_section_schema_Click)

      '2-Contenidos
      Winup_Content = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7819), ENUM_FORM.FORM_MAIN, Nothing)
      '2.4 About Us
      Aboutus = m_mnu.AddItem(Winup_Content, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7592), ENUM_FORM.FORM_ABOUT_US_SEL, AddressOf m_About_Us_Click)
      '2.5 How To Get There
      HowToGetThere = m_mnu.AddItem(Winup_Content, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7627), ENUM_FORM.FORM_HOW_TO_GET_THERE, AddressOf m_how_to_get_there_Click)

      '3-Juegos Online
      Dim m_game_online_games As GUI_Controls.CLASS_MENU_ITEM
      m_game_online_games = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7818), ENUM_FORM.FORM_MAIN, Nothing)
      '3.1 Categorias de Juegos Online
      Game_Categories = m_mnu.AddItem(m_game_online_games, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7533), ENUM_FORM.FORM_GAME_CATEGORIES_SEL, AddressOf m_game_categories_Click)
      '3.2 Administracion de Juegos online
      Games_Admin = m_mnu.AddItem(m_game_online_games, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7532), ENUM_FORM.FORM_GAMES_ADMIN, AddressOf m_Games_Admin_Click)

      '5- Notificaciones
      Winup_Notifications = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7866), ENUM_FORM.FORM_WINUP_NOTIFICATION, AddressOf m_Winup_Notifications_Click)

      '5- Configuracion
      Winup_Settings = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7694), ENUM_FORM.FORM_WINAP_SETTINGS, AddressOf m_Winup_Settings_Click)

    End If

    '#End If

    '' Player tracking config SLE 14-ENE-2010
    'Player_Tracking_Configuration = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(453), ENUM_FORM.FORM_PLAYER_TRAKING_CONFIGURATION, AddressOf m_player_tracking_configuration_Click)

    ' RCI 16-MAR-2012: Hide the plays screen.
    'Plays_plays = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_STATISTICS.Id(354), ENUM_FORM.FORM_PLAYS_PLAYS, AddressOf m_Plays_plays_Click)
    ' DMR 30-JUL-2013: Hide the Plays -> Grouped by Account screen.
    'Statistics_cards = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_STATISTICS.Id(364), ENUM_FORM.FORM_STATISTICS_CARDS, AddressOf m_Statistics_cards_Click)

    ' RRB 31-JAN-2013
    'Terminal_Movements = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1637), ENUM_FORM.FORM_TERMINAL_MOVEMENTS, AddressOf m_Terminal_movements_Click)

    m_window_menu = New CLASS_WINDOW_MENU(AddressOf m_open_forms_menu_Click, AddressOf m_open_forms_item_Click, CLASS_GUI_RESOURCES.GetImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.WINDOW.ToString.ToLower))
    m_mnu.AddPopupItem(m_window_menu)
    m_window_menu.AddEvents(GetEnterImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.WINDOW))

    cls_menu = m_mnu.AddItem(GLB_NLS_GUI_PLAYER_TRACKING.Id(6052), ENUM_FORM.FORM_MAIN, AddressOf m_alarm_request, CLASS_GUI_RESOURCES.GetImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.MONEY_REQUEST.ToString.ToLower), ToolStripItemAlignment.Right, True)
    cls_menu.AddEvents(GetEnterImage(CLASS_MENU.ENUM_MENU_ICON_NAMES.MONEY_REQUEST))
    one_money_request_menu = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(6051), ENUM_FORM.FORM_CAGE_CONTROL, AddressOf m_CAGE_money_request_Click)
    more_money_request_menu = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(6708), ENUM_FORM.FORM_CAGE, AddressOf m_CAGE_money_request_Click)

    one_money_collect_menu = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(6557), ENUM_FORM.FORM_CAGE_CONTROL, AddressOf m_CAGE_money_collect_Click)
    more_money_collect_menu = m_mnu.AddItem(cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(6709), ENUM_FORM.FORM_CAGE, AddressOf m_CAGE_money_collect_Click)

    one_money_request_menu.Visible = False
    more_money_request_menu.Visible = False
    one_money_collect_menu.Visible = False
    more_money_collect_menu.Visible = False

    CheckPendingRequest()

    m_mnu.SetHeight(85)

  End Sub 'GUI_CreateMenu

  Dim m_list As New Dictionary(Of Integer, ToolStripMenuItem)

  Protected Sub loadNodeForMenu(ByVal parent_Id As Integer, ByVal parent_Name As String, ByVal name As String, ByVal m_Content As GUI_Controls.CLASS_MENU_ITEM)

    Dim m_New As GUI_Controls.CLASS_MENU_ITEM
    m_New = Nothing
    Dim dt_Childs = CLASS_WINUP_CONTENT.GetSectionsByParentName(name)
    If (Not dt_Childs Is Nothing) AndAlso dt_Childs.Rows.Count > 0 Then
      m_New = m_mnu.AddItem(m_Content, parent_Name, ENUM_FORM.FORM_MAIN, Nothing)
      For Each row_Section As DataRow In dt_Childs.Rows
        loadNodeForMenu(row_Section("SectionSchemaId"), row_Section("MainTitle").ToString(), row_Section("Name").ToString(), m_New)
      Next
    Else
      m_New = m_mnu.AddItem(m_Content, parent_Name, ENUM_FORM.FORM_MAIN, AddressOf m_content_generic_Click)
      m_list.Add(parent_Id, m_New.MenuItem)
    End If
  End Sub ' GUI_Exit

  Private Sub m_content_generic_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_winup_content_sel
    Dim menu As ToolStripMenuItem
    Dim id As Integer

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    menu = DirectCast(sender, ToolStripMenuItem)

    If m_list.ContainsValue(menu) Then

      Dim pair As KeyValuePair(Of Integer, ToolStripMenuItem)

      For Each pair In m_list
        If pair.Value.Equals(menu) Then
          id = pair.Key
          Exit For
        End If
      Next
    Else
      id = 0
    End If

    frm = New frm_winup_content_sel(id)
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' PURPOSE: GUI Exit actions.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_Exit()

    MyBase.GUI_Exit()

    'logout the user
    Call GLB_CurrentUser.Logout()

    ' Exit All threads ...
    Environment.Exit(0)

  End Sub ' GUI_Exit

  ' PURPOSE: Given a type, the function returns if it needs to be audited
  '
  '  PARAMS:
  '     - INPUT:
  '         - AuditType: AUDIT_FLAGS that wants to be audited
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_HasToBeAudited(ByVal AuditType As AUDIT_FLAGS) As Boolean

    Return False

  End Function
  Private Sub DynamicWinupContentMenu()
    ''Contenido Dinamico **************
    Try
      Dim dt_Sections = CLASS_WINUP_CONTENT.GetSectionsByParentName("Home")
      If Not dt_Sections Is Nothing Then
        For Each row_Section As DataRow In dt_Sections.Rows
          loadNodeForMenu(row_Section("SectionSchemaId"), row_Section("MainTitle").ToString(), row_Section("Name").ToString(), Winup_Content)
        Next
      End If
    Catch _ex As Exception
      WSI.Common.Log.Exception(_ex)
    End Try

    '********************************************************************************************
  End Sub
#End Region

#Region " Private Functions "

  ' PURPOSE : Called when the database status has changed.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  ' NOTES:
  Private Sub DatabaseStatusChanged()

    Try
      If GLB_DbServerName <> WGDB.DataSource Then

        GLB_DbServerName = WGDB.DataSource

        If lbl_main_title.InvokeRequired Then
          Call Me.Invoke(New DelegatesNoParams(AddressOf ChangeMainTitle))
        End If
      End If
    Catch ex As Exception

    End Try

  End Sub ' DatabaseStatusChanged

  ' PURPOSE : Called when the multisite status has changed.
  '
  '  PARAMS:
  '     - INPUT:  OldStatus: old status multisite server
  '               NewStatus: new status multisite server
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  ' NOTES:
  Private Sub MultiSiteStatusChanged(ByVal OldStatus As WGDB.MULTISITE_STATUS, ByVal NewStatus As WGDB.MULTISITE_STATUS)

    Try
      MultisiteStatus = NewStatus

      If lbl_main_title.InvokeRequired Then
        Call Me.Invoke(New DelegatesNoParams(AddressOf ChangeMainTitle))
      End If
    Catch _ex As Exception

    End Try
  End Sub 'MultiSiteStatusChanged

  ' PURPOSE : Build main form title.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub SetMainTitle()

    Dim site_id As String
    Dim site_name As String

    lbl_main_title.Value = GLB_NLS_GUI_AUDITOR.GetString(100) & Space(2) & "[" & WGDB.GetAppVersion() & "]"

    site_id = GetSiteId()
    site_name = GetSiteName()

    If CurrentUser.IsLogged Then
      lbl_main_title.Value = lbl_main_title.Value & " - " & CurrentUser.Name & "@" & GLB_DbServerName & "     SITE: " & site_id & " - " & site_name
    Else
      If GLB_DbServerName <> "" Then
        lbl_main_title.Value = lbl_main_title.Value & " - @" & GLB_DbServerName & "     SITE: " & site_id & " - " & site_name
      End If
    End If

    ' DHA 26-MAR-2013: Add MultiSite server status
    If MultisiteStatus = WGDB.MULTISITE_STATUS.MEMBER_CONNECTED Then
      lbl_main_title.Value = lbl_main_title.Value & "     " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1830)
    ElseIf MultisiteStatus = WGDB.MULTISITE_STATUS.MEMBER_NOT_CONNECTED Then
      lbl_main_title.Value = lbl_main_title.Value & "     " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1831)
    ElseIf MultisiteStatus = WGDB.MULTISITE_STATUS.MEMBER Then
      lbl_main_title.Value = lbl_main_title.Value & "     " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1829)
    End If

    Me.Text = lbl_main_title.Value

  End Sub ' SetMainTitle

  ' PURPOSE : Modify main form title when a database status change.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub ChangeMainTitle()

    SetMainTitle()

  End Sub ' ChangeMainTitle

  Private Sub frm_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
    Dim _selector As frm_screen_selector

    Select Case e.KeyCode
      Case frm_screen_selector.HotKeyCode
        If e.Control AndAlso MdiChildren.Length > 0 Then
          _selector = New frm_screen_selector()
          Call _selector.Open(Me, Not e.Shift)
        End If

    End Select

  End Sub

  ' PURPOSE : Check if pending request cage and hide submenu
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub CheckPendingRequest()
    Dim _dt As DataTable
    Dim _expression As String
    Dim _request_dt() As DataRow
    Dim _collect_dt() As DataRow
    Dim _menu_visible As Boolean

    _menu_visible = False

    If Not GLB_CurrentUser.IsSuperUser AndAlso
       CurrentUser.Permissions(ENUM_FORM.FORM_CAGE_CONTROL).Write Then
      _dt = Cage.CheckPendingMovements(Cage.MovTypeToShow.AllPending)

      If _dt.Rows.Count > 0 Then
        tmr_MoneyRequestPending.Enabled = False

        'Money Request
        _expression = "CGM_TYPE = " & Cage.CageOperationType.RequestOperation
        _request_dt = _dt.Select(_expression)

        If _request_dt.Length > 0 Then
          _menu_visible = True
          If _request_dt.Length = 1 Then
            one_money_request_menu.Visible = True
            more_money_request_menu.Visible = False
          Else
            one_money_request_menu.Visible = False
            more_money_request_menu.Visible = True
          End If
        Else
          one_money_request_menu.Visible = False
          more_money_request_menu.Visible = False
        End If

        'Money Collect
        _expression = "CGM_TYPE IN (" & Cage.CageOperationType.FromCashier _
                                & "," & Cage.CageOperationType.FromCashierClosing _
                                & "," & Cage.CageOperationType.FromGamingTable _
                                & "," & Cage.CageOperationType.FromGamingTableClosing _
                                & "," & Cage.CageOperationType.FromGamingTableDropbox _
                                & "," & Cage.CageOperationType.FromGamingTableDropboxWithExpected & ")"
        _collect_dt = _dt.Select(_expression)

        If _collect_dt.Length > 0 Then
          _menu_visible = True
          If _collect_dt.Length = 1 Then
            one_money_collect_menu.Visible = True
            more_money_collect_menu.Visible = False
          Else
            one_money_collect_menu.Visible = False
            more_money_collect_menu.Visible = True
          End If
        Else
          one_money_collect_menu.Visible = False
          more_money_collect_menu.Visible = False
        End If
      Else
        tmr_MoneyRequestPending.Enabled = True
      End If
    Else
      tmr_MoneyRequestPending.Stop()
    End If

    m_mnu.ItemVisible(GLB_NLS_GUI_PLAYER_TRACKING.Id(6052), _menu_visible)

  End Sub 'CheckPendingRequest

#End Region

#Region " Menu Selection Events "

  Private Sub m_open_forms_menu_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    m_window_menu.OnPopup(Me)
  End Sub

  Private Sub m_open_forms_item_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _tool_strip_item As ToolStripMenuItem
    Dim _tool_strip_menu_item As ToolStripMenuItem
    Dim _idx As Int32

    _tool_strip_menu_item = CType(sender, ToolStripMenuItem)
    _tool_strip_item = _tool_strip_menu_item.OwnerItem

    _idx = _tool_strip_item.DropDownItems.IndexOf(_tool_strip_menu_item)
    m_window_menu.OnClick(_idx, Me)
  End Sub

  Private Sub m_Click_Profiles(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_user_profiles_sel

    'XVV Replace Me.Cursor.Current to Windows.Forms.cursor 
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance 
    frm = New frm_user_profiles_sel
    Call frm.ShowForEdit(Me)

    'XVV Replace Me.Cursor.Current to Windows.Forms.cursor 
    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Click_Terminals(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_terminals_sel

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance 
    frm = New frm_terminals_sel
    Call frm.ShowForEdit(Me)

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' *****
  Private Sub m_Click_Terminals_View(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_terminals

    'JRM 10/06/2013
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance 
    frm = New frm_terminals
    Call frm.ShowForEdit(Me)

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Click_Terminals_Time_Disconnected_View(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_terminals_time_disconnected

    'JRM 10/06/2013
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance 
    frm = New frm_terminals_time_disconnected
    Call frm.ShowForEdit(Me)

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' ***
  Private Sub m_Click_Terminal_Game_Relation(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_terminal_game_relation

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance 
    frm = New frm_terminal_game_relation
    Call frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Click_Terminal_Meter_Delta(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_terminal_meter_delta_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance 
    _frm = New frm_terminal_meter_delta_sel
    Call _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Click_GeneralParams(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_genaral_params

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance 
    frm = New frm_genaral_params
    Call frm.ShowForEdit(Me)

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Auditor_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_auditor

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.WaitCursor
    ' Create instance 
    frm = New frm_auditor
    Call frm.ShowForEdit(Me)

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_AreaBank_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_area_bank_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_area_bank_sel
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_DisconnectClick(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _is_closure_canceled As Boolean

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    '' Try to close all children forms.
    '' Use GUI_Closing instead of Close() to allow 
    '' to user cancell restart if has unsaved changes
    GUI_Closing(_is_closure_canceled)

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.Default

    If Not _is_closure_canceled Then
      '' Dispose current form to avoid restart 
      '' two instances of the same app
      Call Me.Dispose()


      ''Call HtmlPrinter.Stop()
      ''Call FtpVersions.Stop()
      ''Call GeneralParam.Stop()
      ''Call WGDB.Stop()
      Application.Restart()
    End If
  End Sub

  Private Sub m_Exit_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.WaitCursor
    Me.Close()

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_EventsHistory_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_events_history

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_events_history
    frm.ShowForEdit(Me)

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Alarms_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_alarms

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_alarms
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Statistics_terminals_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_statistics_terminals

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_statistics_terminals
    frm.ShowForEdit(Me)

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Statistics_providers_gap_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_statistics_providers

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_statistics_providers
    frm.ShowForEdit(Me)

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Transfering_gap_report_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_comparation_meters

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_comparation_meters
    frm.ShowForEdit(Me)

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' JML 23-MAY-2012
  Private Sub m_Cash_report_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_cash_report

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_cash_report
    frm.ShowForEdit(Me)

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Prize_taxes_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_prize_taxes_report

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_prize_taxes_report
    frm.ShowForEdit(Me)

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_Forms_report_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_reports

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_reports
    frm.ShowForEdit(Me)

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_Voucher_browser_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_voucher_browser

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_voucher_browser
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_Productivity_reports_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_productivity_reports

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_productivity_reports
    frm.ShowForEdit(Me)

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_Segmentation_reports_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_segmentation_reports

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_segmentation_reports
    frm.ShowForEdit(Me)

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_Accounts_Import_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_accounts_import

    Windows.Forms.Cursor.Current = Cursors.WaitCursor
    'RXM 01-AGO-2012 
    ' Create instance
    frm = New frm_accounts_import
    frm.ShowForEdit(Me)

    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub


  Private Sub m_Terminal_money_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_terminal_money

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_terminal_money
    frm.ShowForEdit(Me)

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_Money_pending_transfer_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_money_pending_transfer

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_money_pending_transfer
    frm.ShowForEdit(Me)

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_Money_collections_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_money_collections

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_money_collections
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_Tickets_report_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_tickets_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_tickets_report
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Tax_report_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_tax_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_tax_report
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Detailed_prizes_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_detailed_prizes

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_detailed_prizes
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Statistics_games_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_statistics_games

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_statistics_games
    frm.ShowForEdit(Me)

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Statistics_general_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_statistics_general

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_statistics_general
    frm.ShowForEdit(Me)

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Statistics_hours_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_statistics_hours_accumulated

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_statistics_hours_accumulated
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Statistics_hours_group_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_statistics_hours_group

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_statistics_hours_group
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Plays_sessions_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_plays_sessions

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    'frm = New frm_plays_sessions
    'frm.ShowForEdit(Me)
    frm = New frm_plays_sessions(ENUM_FORM.FORM_PLAYS_SESSIONS)
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub
  Private Sub m_Statistics_bonoplay_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_gamegateway_draws_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    frm = New frm_gamegateway_draws_report()
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Statistics_bonoplay_played_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_gamegateway_draws_prizes_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    frm = New frm_gamegateway_draws_prizes_report()
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Points_Assignment_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_plays_sessions

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_plays_sessions(ENUM_FORM.FORM_POINTS_ASSIGNMENT)
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Plays_sessions_edit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_plays_sessions

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    'frm = New frm_plays_sessions
    'frm.ShowForEdit(Me)
    frm = New frm_plays_sessions(ENUM_FORM.FORM_PLAYS_SESSIONS_EDIT)
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  '''Private Sub m_Plays_plays_Click(ByVal sender As Object, ByVal e As System.EventArgs)
  '''  Dim frm As frm_plays_plays

  '''  Windows.Forms.Cursor.Current = Cursors.WaitCursor

  '''  ' Create instance
  '''  frm = New frm_plays_plays
  '''  frm.ShowForEdit(Me)

  '''  Windows.Forms.Cursor.Current = Cursors.Default

  '''End Sub

  Private Sub m_Ws_sessions_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_wcp_sessions

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_wcp_sessions
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Svc_monitor_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_service_monitor

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_service_monitor
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Cashier_sessions_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_cashier_sessions

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_cashier_sessions
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Taxes_report_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_taxes_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_taxes_report
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Cashier_movements_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_cashier_movements

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_cashier_movements
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Card_summary_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_account_summary

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_account_summary
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Expired_credits_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_expired_credits

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_expired_credits
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Card_movements_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_account_movements

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_account_movements
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' RRB 22-AUG-2012 Card_details
  Private Sub m_Card_details_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_account_movements

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_account_movements(ENUM_FORM.FORM_ACCOUNT_MOVEMENTS_DETAILS)
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  'EOR 15-AUG-2016
  Private Sub m_reserved_maquines_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_reserved_machines_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_reserved_machines_sel()
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  ' YNM 14-DIC-2015 Customer_Visits
  Private Sub m_customer_visits_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_customer_entrances_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_customer_entrances_report()
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' ADI 28-JAN-2016 Customer_Entrance_Visits
  Private Sub m_Customer_Entrance_Visits_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_customers_entrances_visit_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_customers_entrances_visit_sel()
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' ADI 30-MAR-2016 CountR Sessions
  Private Sub m_countr_sessions_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_countr_sessions

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_countr_sessions()
    frm.ShowForEdit(Me)
    'ByVal MdiParent As System.Windows.Forms.IWin32Window
    Windows.Forms.Cursor.Current = Cursors.Default


  End Sub

  ' JRC 13-JUL-2016 Customer_Entrance_Barred_Repor
  Private Sub m_Customer_Entrance_Barred_Report_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_customers_entrances_barred_list_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_customers_entrances_barred_list_sel()
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' FJC 20-JAN-2016 Customer Entrance Prices
  Private Sub m_customer_entrance_price_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_customer_entrance_prices_edit

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_customer_entrance_prices_edit()
    frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' YNM 16-FEB-2016
  Private Sub m_blacklist_import_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_blacklist_import

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_blacklist_import()
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Point_movements_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_points_movements

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_points_movements
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Acct_taxes_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_acct_taxes

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_acct_taxes
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Sales_date_terminal_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_sales_date_terminal

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_sales_date_terminal
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub
  Private Sub m_Capacity_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_capacity

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_capacity
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_CustomerBase_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_customer_base

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_customer_base
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_MachineOcupation_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_machine_occupation

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_machine_occupation
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_CustomerSegmentation_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_customer_segmentation

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_customer_segmentation
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Provider_day_terminal_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_provider_day_terminal

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_provider_day_terminal
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  '04-DEC-2015 RGR 
  Private Sub m_Terminal_Equity_Report_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_terminal_equity_report_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_terminal_equity_report_sel
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub


  Private Sub m_Class_II_draw_audit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_class_II_draw_audit

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_class_II_draw_audit
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Sw_download_versions_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_sw_package_versions

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_sw_package_versions
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Terminals_sw_version_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_terminal_sw_version

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_terminal_sw_version
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_terminal_status_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_terminal_status

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_terminal_status()
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Misc_sw_build_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_misc_sw_version

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_misc_sw_version
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Licence_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_licence_versions

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_licence_versions
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Jackpot_configuration_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_jackpot_configuration

    Windows.Forms.Cursor.Current = Cursors.Default

    ' Create instance
    frm = New frm_jackpot_configuration
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Jackpot_promo_messages_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_jackpot_promo_messages

    Windows.Forms.Cursor.Current = Cursors.Default

    ' Create instance
    frm = New frm_jackpot_promo_messages
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub



  Private Sub m_Jackpot_history_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_jackpot_history

    Windows.Forms.Cursor.Current = Cursors.Default

    ' Create instance
    frm = New frm_jackpot_history
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub


  Private Sub m_Jackpot_monitor_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_jackpot_monitor

    Windows.Forms.Cursor.Current = Cursors.Default

    ' Create instance
    frm = New frm_jackpot_monitor
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  'Private Sub m_Player_tracking_players_Click(ByVal sender As Object, ByVal e As System.EventArgs)

  '  Dim frm As frm_player_tracking_player_sel

  '  Windows.Forms.Cursor.Current = Cursors.Default

  '  ' Create instance
  '  frm = New frm_player_tracking_player_sel
  '  frm.ShowForEdit(Me)

  '  Windows.Forms.Cursor.Current = Cursors.Default

  'End Sub

  Private Sub m_Site_Jackpot_configuration_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_site_jackpot_configuration

    Windows.Forms.Cursor.Current = Cursors.Default

    ' Create instance
    frm = New frm_site_jackpot_configuration
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Site_Jackpot_history_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_site_jackpot_history

    Windows.Forms.Cursor.Current = Cursors.Default

    ' Create instance
    frm = New frm_site_jackpot_history
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Site_Bonuses_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_bonuses

    Windows.Forms.Cursor.Current = Cursors.Default

    ' Create instance
    frm = New frm_bonuses
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_magnetic_card_writer_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_card_writer

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_card_writer
    frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub
  ' JMV 23-12-2014
  Private Sub m_machine_and_game_report_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_machine_and_game_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_machine_and_game_report

    frm.ShowForEdit(Me)


    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_cashier_configuration_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_cashier_configuration

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_cashier_configuration
    frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_catalog_configuration_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_catalogs_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_catalogs_sel
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_game_categories_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_games_categories_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_games_categories_sel
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_section_schema_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_section_schema_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_section_schema_sel
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Game_meters_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_game_meters

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_game_meters
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub
  Private Sub m_Machine_denom_meters_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_machine_denom_meters

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_machine_denom_meters
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_history_meters_edit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_meters_history_edit

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_meters_history_edit
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Promotions_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_promotions_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_promotions_sel
    frm.ShowForEdit(Me, frm_promotions_sel.ENUM_PROMO_TYPE_MODE.TYPE_MANUAL)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Periodical_Promotions_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_promotions_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_promotions_sel
    frm.ShowForEdit(Me, frm_promotions_sel.ENUM_PROMO_TYPE_MODE.TYPE_AUTOMATIC)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Preassigned_Promotions_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_promotions_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_promotions_sel
    frm.ShowForEdit(Me, frm_promotions_sel.ENUM_PROMO_TYPE_MODE.TYPE_PREASSIGNED)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Promotion_Reports_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_automatic_promotions_reports

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_automatic_promotions_reports
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_Promotions_Delivered_Report_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_promotions_delivered_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_promotions_delivered_report
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_gifts_catalog_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_gift_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_gift_sel
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_gifts_history_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_gift_history

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_gift_history
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Click_Terminals_3GS(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_terminals_3gs_edit

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance 
    frm = New frm_terminals_3gs_edit
    Call frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Click_Terminals_Pending(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_terminals_pending

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance 
    frm = New frm_terminals_pending
    Call frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Draws_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_draws_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_draws_sel
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Commands_send_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_command_send

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_command_send
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Commands_history_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_command_history

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_command_history
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Terminal_without_activity_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_terminals_without_activity

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_terminals_without_activity
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Mailing_programming_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_mailing_programming_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_mailing_programming_sel
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Sites_operators_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_sites_operators_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_sites_operators_sel
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub


  Private Sub m_Handpays_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_handpays

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_handpays
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Mobile_Bank_Movements_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_mb_movements

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_mb_movements
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Advertisement_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_advertisement

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_advertisement()
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Games_Admin_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_games_admin_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_games_admin_sel()
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Winup_Settings_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_winup_settings

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_winup_settings
    frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Winup_Notifications_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_winup_notifications_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_winup_notifications_sel
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub


  Private Sub m_About_Us_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_about_us_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_about_us_sel()
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub


  Private Sub m_how_to_get_there_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_how_to_get_there

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_how_to_get_there
    frm.ShowEditItem()

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub


  Private Sub m_AdsConfiguration_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_ads_configuration

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_ads_configuration()
    'frm.ShowForEdit(Me)
    frm.ShowEditItem(Me)


    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_LcdConfiguration_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_lcd_configuration

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_lcd_configuration()
    _frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Click_lcd_message(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_lcd_message_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance 
    frm = New frm_lcd_message_sel
    Call frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_AccountRequestPin_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_account_request_pin

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_account_request_pin()
    Call _frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default


    'Call _frm_edit.ShowEditItem(_id_area_bank)

    'Call RefreshGrid()

    _frm = Nothing


  End Sub

  Private Sub m_FormThresholdMovementReport_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_threshold_movements_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_threshold_movements_report()
    Call _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

    _frm = Nothing

  End Sub

  Private Sub m_FormCreditlineSel_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_creditline_sel


    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_creditline_sel()
    Call _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

    _frm = Nothing


  End Sub

  Private Sub m_FormCreditlineMovementReport_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_creditline_movements_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_creditline_movements_report()
    Call _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

    _frm = Nothing

  End Sub

  Private Sub m_FormJunketsSel_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_junkets_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_junkets_sel()
    Call _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

    _frm = Nothing

  End Sub

  Private Sub m_FormJunketsRepresentativesSel_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_junkets_representatives_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_junkets_representatives_sel()
    Call _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

    _frm = Nothing

  End Sub

  Private Sub m_FormJunketsReport_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_junkets_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_junkets_report()
    Call _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

    _frm = Nothing

  End Sub

  Private Sub m_FormJunketsReportFlyers_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_junkets_flyers_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_junkets_flyers_report()
    Call _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

    _frm = Nothing

  End Sub

  ' JML 23-MAY-2012
  Private Sub m_Recommendation_report_click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_recommendation_report

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_recommendation_report
    frm.ShowForEdit(Me)

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' JML 14-JUN-2012
  Private Sub m_provider_click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_provider_data

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_provider_data
    frm.ShowEditItem(Nothing)

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' SGB 04-DIC-2015
  Private Sub m_bucket_sel_click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_bucket_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_bucket_sel
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' XGJ 10-AGU-2016
  Private Sub m_buckets_multiplier_sel_click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_buckets_multiplier_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_buckets_multiplier_sel
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' XGJ 23-GEN-2017
  Private Sub m_terminal_draw_report_click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_terminal_draw_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_terminal_draw_report
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub



  Private Sub Terminal_Draw_Summary_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_terminal_draw_summary

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_terminal_draw_summary
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' JML 18-JUN-2012
  Private Sub m_currency_click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_currencies

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_currencies
    frm.ShowEditItem(Nothing)

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Peru_Frames_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_peru_frames

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_peru_frames()
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  'HBB 02-AUG-2012'
  Private Sub m_password_configuration_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_password_configuration

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_password_configuration()
    frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  'HBB 13-SEP-2012'
  Private Sub m_cash_sevice_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_service_charge_edit
    'Dim frm As frm_password_configuration

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance 
    frm = New frm_service_charge_edit()
    frm.ShowEditItem(Me)


    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' XCD 14-SEP-2012
  Private Sub m_Flags_Sel_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_flags_sel

    Windows.Forms.Cursor.Current = Cursors.Default

    ' Create instance
    frm = New frm_flags_sel
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' XCD 14-SEP-2012
  Private Sub m_Account_Flags_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_account_flags

    Windows.Forms.Cursor.Current = Cursors.Default

    ' Create instance
    frm = New frm_account_flags
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Report_draw_tickets(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_report_draw_tickets

    Windows.Forms.Cursor.Current = Cursors.Default

    ' Create instance
    frm = New frm_report_draw_tickets
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  'HBB 27-SEP-2012'
  Private Sub m_stats_manual_adjustment_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_stats_manual_adjustment
    'Dim frm As frm_password_configuration

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance 
    frm = New frm_stats_manual_adjustment()
    Call frm.ShowForEdit(Me)


    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  'HBB 14-JAN-2013
  Private Sub m_payment_orders_config_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_payment_orders_config

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    frm = New frm_payment_orders_config()

    Call frm.ShowForConfig()

  End Sub

  'HBB 17-JAN-2013
  Private Sub m_Payment_orders_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_payment_order_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    frm = New frm_payment_order_sel()

    Call frm.ShowForEdit(Me)

  End Sub

  'RRB 23-JAN-2013
  Private Sub m_Cash_summary_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_reports

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    frm = New frm_reports(ENUM_FORM.FORM_CASH_SUMMARY)

    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_Retreats_report_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_retreats_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance

    frm = New frm_retreats_report
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Service_Charge_report_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_retreats_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance

    frm = New frm_retreats_report(ENUM_FORM.FORM_SERVICE_CHARGE_REPORT)
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' LEM 15-JAN-2013
  Private Sub m_Play_preferences_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_play_preferences

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_play_preferences()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' QMP 18-FEB-2013
  Private Sub m_operation_schedule_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_operations_schedule

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance 
    frm = New frm_operations_schedule()
    frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' JMM 12-MAR-2013
  Private Sub m_site_config_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_multisite_configuration

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance 
    frm = New frm_multisite_configuration()
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' JMM 12-MAR-2013
  Private Sub m_multisite_status_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_multisite_monitor

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance 
    frm = New frm_multisite_monitor()
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' SMN 25-MAR-2013
  Private Sub m_Machine_Alias_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_machine_alias

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance 
    frm = New frm_machine_alias()
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' DMR 30-APR-2013
  Private Sub m_Providers_Games_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_providers_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance 
    frm = New frm_providers_sel()
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' AMF 30-JUL-2013
  Private Sub m_currency_configuration_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_currency_configuration

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance 
    frm = New frm_currency_configuration()
    frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' DLL 19-JUN-2013
  Private Sub m_currency_exchange_audit_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_currency_exchange_audit

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance 
    frm = New frm_currency_exchange_audit()
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  'RBG 21-JUN-2013
  Private Sub m_Groups_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_groups_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_groups_sel()
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' RRR 22-JUL-2013
  Private Sub m_Money_laundering_config_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As Object

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    If GeneralParam.GetBoolean("AntiMoneyLaundering", "ExternalControl.Enabled", False) Then
      frm = New frm_money_laundering_external_config()
    Else
      frm = New frm_money_laundering_config()
    End If

    frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' RRR 05-AUG-2013
  Private Sub m_Player_edit_config_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_player_edit_config

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_player_edit_config()
    frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Money_laundering_report_prize_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_money_laundering_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_money_laundering_report(ENUM_FORM.FORM_MONEY_LAUNDERING_REPORT_PRIZE)
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Money_laundering_report_recharge_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_money_laundering_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_money_laundering_report(ENUM_FORM.FORM_MONEY_LAUNDERING_REPORT_RECHARGE)
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  'MMG 08-Jul-2013 
  Private Sub m_Draws_cashdesk_detail_click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_draws_cashdesk_detail_sel

    Windows.Forms.Cursor.Current = Cursors.Default

    ' Create instance
    frm = New frm_draws_cashdesk_detail_sel
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  'MMG 16-Jul-2013 
  Private Sub m_Draws_cashdesk_sumary_click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_draws_cashdesk_summary_sel

    Windows.Forms.Cursor.Current = Cursors.Default

    ' Create instance
    frm = New frm_draws_cashdesk_summary_sel
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  'JBC 17-09-2013
  Private Sub m_Points_Import_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_points_import

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_points_import
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  'JPJ 09-OCT-2013
  Private Sub m_Points_to_Credits_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_points_to_credit

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_points_to_credit
    frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_Machine_draws_configuration_click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_cashdesk_draws_configuration_edit

    Windows.Forms.Cursor.Current = Cursors.Default

    ' Create instance
    frm = New frm_cashdesk_draws_configuration_edit(CLASS_CASHDESK_DRAWS_CONFIGURATION.ENUM_DRAW_CONFIGURATION.DRAW_02)
    frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub


  'ESE 14-SEP-2016
  Private Sub m_GamingTables_draws_configuration_click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_cashdesk_draws_configuration_edit

    Windows.Forms.Cursor.Current = Cursors.Default

    ' Create instance
    frm = New frm_cashdesk_draws_configuration_edit(CLASS_CASHDESK_DRAWS_CONFIGURATION.ENUM_DRAW_CONFIGURATION.DRAW_01)
    frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  'MMG 30-Jul-2013 
  Private Sub m_Cashdesk_draws_configuration_click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_cashdesk_draws_configuration_edit

    Windows.Forms.Cursor.Current = Cursors.Default

    ' Create instance
    frm = New frm_cashdesk_draws_configuration_edit
    frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  'RMS 21-OCT-2013
  Private Sub m_Account_Card_Changes_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_account_card_changes

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_account_card_changes
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub
  ' XMS 11-DEC-2013: Add new Form to Cage Menu
  Private Sub m_CAGE_Source_Target_new_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_cage_source_target_edit

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_cage_source_target_edit
    _frm.ShowEditItem(Nothing)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  ' JAB 06-NOV-2013: Cage
  Private Sub m_CAGE_Sessions_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_cage_sessions

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_cage_sessions()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  ' JAB 06-NOV-2013: Cage
  Private Sub m_CAGE_amounts_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_cage_amounts

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_cage_amounts
    _frm.ShowEditItem(Nothing)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_CAGE_movement_sel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_cage_movements_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_cage_movements_sel
    AddHandler _frm.m_dispose_pending_request, AddressOf CheckPendingRequest
    _frm.ShowForEdit(Me, , , , tmr_MoneyRequestPending)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_cashier_send_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_cage_control

    If Not Cage.IsCageEnabled Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3026), ENUM_MB_TYPE.MB_TYPE_WARNING)
    Else
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      ' Create instance
      _frm = New frm_cage_control
      _frm.m_mode_edit = frm_cage_control.MODE_EDIT.SEND
      _frm.ShowNewItem()

      Windows.Forms.Cursor.Current = Cursors.Default
    End If
  End Sub

  Private Sub m_cashier_collection_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_cage_movements_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_cage_movements_sel
    _frm.m_mode_edit = frm_cage_movements_sel.MODE_EDIT.COLLECTION
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_CAGE_new_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_cage_control

    If Not Cage.IsCageEnabled Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3026), ENUM_MB_TYPE.MB_TYPE_WARNING)
    Else
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      ' Create instance
      _frm = New frm_cage_control
      _frm.ShowNewItem()

      Windows.Forms.Cursor.Current = Cursors.Default
    End If
  End Sub

  Private Sub m_CAGE_CashCageCount_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_cage_control

    If Not Cage.IsCageEnabled Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3026), ENUM_MB_TYPE.MB_TYPE_WARNING)
    Else
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      ' Create instance
      _frm = New frm_cage_control
      _frm.ShowNewItem(CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE)

      Windows.Forms.Cursor.Current = Cursors.Default
    End If
  End Sub

  Private Sub m_CAGE_CashCageStock_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_cage_session_detail
    Dim _frm_only_stock As frm_cage_stock

    Dim _show_only_stock As Boolean
    Dim _ses_detail As cls_cage_session_detail

    _ses_detail = New cls_cage_session_detail()
    _show_only_stock = True
    If Not Cage.IsCageEnabled Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3026), ENUM_MB_TYPE.MB_TYPE_WARNING)
    Else
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      _ses_detail.m_session_id = -1
      _ses_detail.m_session_status = -1
      _ses_detail.m_session_opening_user = AUDIT_NONE_STRING
      _ses_detail.m_session_opening_date = AUDIT_NONE_STRING
      _ses_detail.m_session_closing_user = AUDIT_NONE_STRING
      _ses_detail.m_session_closing_date = AUDIT_NONE_STRING
      _ses_detail.m_session_name = AUDIT_NONE_STRING

      ' Create instance

      If GamingTableBusinessLogic.IsGamingTablesEnabled() Then
        _frm_only_stock = New frm_cage_stock()
        _frm_only_stock.ShowForEdit(Me, _ses_detail, True)
      Else
        _frm = New frm_cage_session_detail()
        _frm.ShowForEdit(Me, _ses_detail, _show_only_stock)
      End If

      Windows.Forms.Cursor.Current = Cursors.Default
    End If
  End Sub

  'AMF 09-APR-2015
  Private Sub m_CAGE_SafeKeeping_sel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_cage_safe_keeping_sel

    If Not Cage.IsCageEnabled Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3026), ENUM_MB_TYPE.MB_TYPE_WARNING)
    Else
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      ' Create instance
      _frm = New frm_cage_safe_keeping_sel
      Call _frm.ShowForEdit(Me)

      Windows.Forms.Cursor.Current = Cursors.Default
    End If
  End Sub

  'SGB 14-APR-2015
  Private Sub m_CAGE_SafeKeeping_movement_sel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_cage_safe_keeping_movement_sel

    If Not Cage.IsCageEnabled Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3026), ENUM_MB_TYPE.MB_TYPE_WARNING)
    Else
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      ' Create instance
      _frm = New frm_cage_safe_keeping_movement_sel
      Call _frm.ShowForEdit(Me)

      Windows.Forms.Cursor.Current = Cursors.Default
    End If
  End Sub

  'SGB 22-APR-2015
  Private Sub m_ACCOUNT_Statement_document_edit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_statement_document_edit

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_statement_document_edit
    Call _frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub


  Private Sub m_ACCOUNT_Statement_print_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_statement_print

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_statement_print
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  ' DLL 14-NOV-2013
  Private Sub m_skipped_meters_report_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_skipped_meters_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_skipped_meters_report
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  'YNM 27-AUG-2015
  Private Sub m_meters_comparison_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_terminal_meters_comparison

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_terminal_meters_comparison
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' JBP 16-DEC-2013
  Private Sub m_gaming_tables_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_gaming_tables_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_gaming_tables_sel
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  ' JBP 16-DEC-2013
  Private Sub m_gaming_tables_type_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_gaming_tables_types_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_gaming_tables_types_sel
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  ' JBP 24-JAN-2014
  Private Sub m_gaming_tables_config_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_gaming_tables_configuration_edit

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_gaming_tables_configuration_edit
    _frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  ' JBP 16-DEC-2013
  Private Sub m_gaming_tables_income_report_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_gaming_tables_income_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_gaming_tables_income_report
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  ' JML 20-JUL-2017
  Private Sub m_gaming_tables_score_report_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_gaming_tables_score_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_gaming_tables_score_report
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  ' JBP 16-DEC-2013
  Private Sub m_gaming_tables_sessions_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_gaming_tables_sessions

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_gaming_tables_sessions
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  ' SMN 3-JAN-2014
  Private Sub m_gaming_tables_cancellations_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_gaming_tables_cancellations

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_gaming_tables_cancellations
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  ' EOR 09-MAY-2016
  Private Sub m_activity_tables_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_activity_tables

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_activity_tables
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  ' EOR 01-SEP-2017
  Private Sub m_activity_last_six_months_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_customer_dashboard_report_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_customer_dashboard_report_sel
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  ' EOR 26-SEP-2016
  Private Sub m_tables_analisys_report_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_gaming_tables_analysis_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_gaming_tables_analysis_report
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub


  ' RMS 09-JAN-2014
  Private Sub m_chips_operations_report_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_chips_operations_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_chips_operations_report
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  ' PURPOSE: Create and show a form's instance.
  '
  '  PARAMS:
  '     - INPUT:
  '         - sender: Object.
  '         - e:      System.EventArgs.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub m_tito_params_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_tito_params_edit
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance 
    _frm = New frm_tito_params_edit()
    _frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_stacker_collection_sel_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_stacker_collections_sel
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_stacker_collections_sel()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' SGB 04-MAY-2015
  Private Sub m_collection_file_sel_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_collection_file_sel
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_collection_file_sel()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' SGB 27-OCT-2015
  Private Sub m_smartfloor_report_task_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_smartfloor_report_task
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_smartfloor_report_task()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_circulating_cash_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_tito_cash_sel
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_tito_cash_sel()
    _frm.Show(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_stacker_sel_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_tito_stackers_sel
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_tito_stackers_sel()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  'RGR 02-OCT-2015
  Private Sub m_tito_payment_report_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_tito_payment_report_sel
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_tito_payment_report_sel()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_ticket_report_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_tito_reports_sel
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_tito_reports_sel()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_currencies_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_currency_configuration
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_currency_configuration()
    _frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_terminal_sas_meters_sel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_terminal_sas_meters_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_terminal_sas_meters_sel()
    _frm.ShowForEdit(Me, frm_terminal_sas_meters_sel.ENUM_FORM_MODE.SIMPLIFIED_MODE)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_terminal_sas_meters_sel_advanced_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_terminal_sas_meters_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_terminal_sas_meters_sel()
    _frm.ShowForEdit(Me, frm_terminal_sas_meters_sel.ENUM_FORM_MODE.ADVANCED_MODE)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_tito_summary_sel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_tito_summary_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_tito_summary_sel()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  'JML 04-DEC-2013
  Private Sub m_tito_collection_balance_report_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_tito_collection_balance_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_tito_collection_balance_report()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  'DRV 04-APR-2013
  Private Sub m_software_validation_send_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_software_validations_send

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_software_validations_send()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  'DRV 04-APR-2013
  Private Sub m_software_validation_history_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_software_validations_history

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_software_validations_history()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_Custom_Alarms_click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_custom_alarms

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_custom_alarms()
    _frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_Patterns_click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_patterns_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_patterns_sel()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  'HBB & DRV 12-MAY-2013
  Private Sub m_massive_collection_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_cage_control

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_cage_control(ENUM_FORM.FORM_STACKER_COLLECTION)
    _frm.ShowNewItem(CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MASSIVE)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_bonuses_meters_comparation_click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_bonuses_meters_comparation

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_bonuses_meters_comparation()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  ' JFC 16-MAY-2014
  Private Sub m_table_play_sessions_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_table_play_sessions

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_table_play_sessions(ENUM_FORM.FORM_TABLE_PLAY_SESSIONS)
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  ' JFC 22-MAY-2014
  Private Sub m_table_play_sessions_edit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_table_play_sessions

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_table_play_sessions(ENUM_FORM.FORM_TABLE_PLAY_SESSIONS_EDIT)
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  ' DHA 02-FEB-2015
  Private Sub m_table_play_sessions_import_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_gaming_tables_session_import

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_gaming_tables_session_import()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  ' JMM 16-JUN-2014
  Private Sub m_table_player_movements_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_table_player_movements

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_table_player_movements()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  'DCS 01-JUL-2014
  Private Sub m_chips_sets_sel_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_chips_sets_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_chips_sets_sel()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Progressive_click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_progressive_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_progressive_sel()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_Provisions_click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_progressive_provision

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_progressive_provision()
    _frm.ShowNewItem()

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_Provisions_report_click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_progressive_provision_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_progressive_provision_report()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_Progressive_paid_report_click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_progressive_jackpots_paid_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_progressive_jackpots_paid_report()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_terminal_denominations_config_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_terminal_denominations_config

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_terminal_denominations_config()
    _frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  'SGB 18-AUG-2014
  Private Sub m_counter_configuration_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_counter_configuration

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_counter_configuration()
    _frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  ' JML 01-SEP-2014
  Private Sub m_collection_by_machine_and_denomination_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_collection_by_machine_and_denomination_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance 
    frm = New frm_collection_by_machine_and_denomination_report()
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' DHA 04-SEP-2014
  Private Sub m_player_tracking_income_report_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_player_tracking_income_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_player_tracking_income_report
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  ' DHA 24-APR-2017
  Private Sub m_player_tracking_global_drop_hourly_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_player_tracking_global_drop_hourly

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_player_tracking_global_drop_hourly
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_table_payers_report_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_table_players_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_table_players_report
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  ' JML 01-SEP-2014
  Private Sub m_collection_by_machine_and_date_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_collection_by_machine_and_date_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance 
    frm = New frm_collection_by_machine_and_date_report()
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' OPC 16-SEP-2014
  Private Sub m_cage_sources_target_concepts_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_cage_source_target_concepts

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_cage_source_target_concepts()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  'HBB 18-SEP-2014'
  Private Sub m_cage_meters_click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_cage_meters

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    frm = New frm_cage_meters()
    Call frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  'JBC 19-SEP-2014'
  Private Sub m_bank_transaction_data_click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_bank_transaction_data_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    frm = New frm_bank_transaction_data_sel()
    Call frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  'RRR 23-SEP-2014'
  Private Sub m_cage_global_report_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_cage_sessions_report = New frm_cage_sessions_report()

    frm.ShowSelectedItem(String.Empty)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

#If DEBUG Then
  Private Sub m_terminal_softcount_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_terminal_softcounts

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    frm = New frm_terminal_softcounts()
    Call frm.ShowNewItem()
    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

#End If

  ' FJC 23-OCT-2014
  Private Sub m_Short_over_report_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_short_over_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    frm = New frm_short_over_report()
    Call frm.ShowForEdit(Me)
    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  'RRR 14-NOV-2014'
  Private Sub m_charts_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_charts

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_charts(GeneralParam.GetString("Charts", "URI"), True)
    Call _frm.ShowDialog()

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  'DCS 03-NOV-2014
  Private Sub m_mobile_bank_sel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_mobile_bank_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    frm = New frm_mobile_bank_sel()
    Call frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  'DCS 03-NOV-2014
  'Private Sub m_terminal_sas_meters_edit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
  '  Dim frm As frm_terminal_sas_meters_edit

  '  Windows.Forms.Cursor.Current = Cursors.WaitCursor

  '  frm = New frm_terminal_sas_meters_edit()
  '  Call frm.ShowForEdit(Me)

  '  Windows.Forms.Cursor.Current = Cursors.Default
  'End Sub

  'DRV 17-DEC-2014
  Private Sub m_TITO_ticket_out_report_Click(ByVal sender As Object, ByVal byvale As System.EventArgs)
    Dim frm As frm_ticket_out_report_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    frm = New frm_ticket_out_report_sel()
    Call frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub




  'JRC 26-OCT-2016
  Private Sub m_tito_ticket_audit_Click(ByVal sender As Object, ByVal byvale As System.EventArgs)
    Dim frm As frm_ticket_audits_sel_several

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    frm = New frm_ticket_audits_sel_several()
    Call frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub



  ' OPC 23-JAN-2015
  Private Sub m_flash_report_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_flash_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    frm = New frm_flash_report
    Call frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  ' DRV 09-JAN-2015
  Private Sub m_terminals_with_stacker_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_terminal_with_stacker

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    frm = New frm_terminal_with_stacker()
    Call frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub


  ' XCD 11-MAY-2015
  Private Sub m_pcd_configuration_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_pcd_configuration_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    frm = New frm_pcd_configuration_sel()
    Call frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  'SGB 07-JUL-2015
  Private Sub m_terminals_block_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_terminals_block

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_terminals_block
    Call _frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_CAGE_money_request_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm_cage As frm_cage_control
    Dim _frm_mov As frm_cage_movements_sel
    Dim _dt As DataTable

    If Not Cage.IsCageEnabled Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3026), ENUM_MB_TYPE.MB_TYPE_WARNING)
    Else
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      _dt = Cage.CheckPendingMovements(Cage.MovTypeToShow.CashierMoneyRequest)

      If _dt.Rows.Count > 0 Then
        ' Create instance
        If _dt.Rows.Count = 1 Then
          If Cage.GetNumberOfOpenedCages() = 0 Then
            NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3378), ENUM_MB_TYPE.MB_TYPE_WARNING)
          Else
            _frm_cage = New frm_cage_control()

            ' The Cashier has requested to Cage (the cage can be closed)
            _frm_cage.ShowWithoutCageID = True

            Call _frm_cage.ShowEditItem(_dt.Rows(0).Item("CGM_MOVEMENT_ID"), CType(CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MOVEMENT, Int16), _dt.Rows(0).Item("CGM_CAGE_SESSION_ID"), True)
          End If
        Else
          _frm_mov = New frm_cage_movements_sel()
          _frm_mov.ShowForEdit(Me, , , Cage.MovTypeToShow.CashierMoneyRequest, tmr_MoneyRequestPending, True)
        End If
      End If

      CheckPendingRequest()

      Windows.Forms.Cursor.Current = Cursors.Default
    End If
  End Sub

  Private Sub m_CAGE_money_collect_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm_cage As frm_cage_control
    Dim _frm_mov As frm_cage_movements_sel
    Dim _dt As DataTable

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _dt = Cage.CheckPendingMovements(Cage.MovTypeToShow.MoneyCollect)

    If _dt.Rows.Count > 0 Then
      If _dt.Rows.Count = 1 Then
        _frm_cage = New frm_cage_control()
        Call _frm_cage.ShowEditItem(_dt.Rows(0).Item("CGM_MOVEMENT_ID"), CType(CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MOVEMENT, Int16), _dt.Rows(0).Item("CGM_CAGE_SESSION_ID"), True)
      Else
        _frm_mov = New frm_cage_movements_sel()
        _frm_mov.ShowForEdit(Me, , , Cage.MovTypeToShow.MoneyCollect, tmr_MoneyRequestPending, True)
      End If
    End If

    CheckPendingRequest()

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_alarm_request(ByVal sender As Object, ByVal e As System.EventArgs)
    CheckPendingRequest()
  End Sub

  'FAV 20-MAY-2015
  Private Sub m_tito_win_control_report_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_tito_win_control_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_tito_win_control_report()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  'FOS 16-JUL-2015
  Private Sub m_purchase_and_sales_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_purchase_and_sales

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_purchase_and_sales()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  'FOS 21-JUL-2015 
  Private Sub m_ticket_configuration_click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_ticket_configuration_edit

    Windows.Forms.Cursor.Current = Cursors.Default

    ' Create instance
    frm = New frm_ticket_configuration_edit()
    frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  'AMF 20-JUL-2015
  Private Sub m_customers_playing_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_customers_playing

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_customers_playing()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  ' DRV 20-JUL-2015
  Private Sub m_cash_monitor_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_cash_monitor

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_cash_monitor()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_comparation_manual_pays_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_tito_win_control_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    'Create instance
    _frm = New frm_tito_win_control_report(ENUM_FORM.FORM_COMPARATION_MANUAL_PAYS)
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  'JRC 14-OCT-2015		
  Private Sub m_GameGateway_terminals_click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_gamegateway_terminals_edit
    Windows.Forms.Cursor.Current = Cursors.WaitCursor
    _frm = New frm_gamegateway_terminals_edit()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  'GDA 27-MAR-2017		
  Private Sub m_intellia_floor_editor_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_intellia_floor_editor
    Windows.Forms.Cursor.Current = Cursors.WaitCursor
    _frm = New frm_intellia_floor_editor()
    _frm.ShowForEdit(Me)
    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  'GDA 27-MAR-2017		
  Private Sub m_intellia_filter_editor_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_intellia_filter_edit
    Windows.Forms.Cursor.Current = Cursors.WaitCursor
    _frm = New frm_intellia_filter_edit()
    _frm.ShowForEdit(Me)
    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub


  'JRC 14-OCT-2015		
  Private Sub m_smartfloor_report_runners_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_smartfloor_report_runner
    Windows.Forms.Cursor.Current = Cursors.WaitCursor
    _frm = New frm_smartfloor_report_runner()
    _frm.ShowForEdit(Me)
    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  'JRC 16-NOV-2015
  Private Sub m_smartfloor_terminals_print_QR_Codes_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_terminals_print_QR_Codes

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_terminals_print_QR_Codes
    Call _frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  'JRC 09-DIC-2015
  Private Sub m_report_buckets_by_client_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_report_buckets_by_client
    Windows.Forms.Cursor.Current = Cursors.WaitCursor
    _frm = New frm_report_buckets_by_client
    Call _frm.ShowForEdit(Me)
    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  ' YNM 24-MAR-2016 CountR
  Private Sub m_countr_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_countr_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_countr_sel()
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' MPO 24-MAR-2016
  Private Sub m_tito_holdvswin_report_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_tito_holdvswin_report

    Windows.Forms.Cursor.Current = Cursors.Default

    ' Create instance
    frm = New frm_tito_holdvswin_report
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  'ESE 15-JUN-2016
  Private Sub m_report_tool_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim aux_title_form As String
    Dim aux_enum_form As ENUM_FORM
    Dim aux_tool_strip As System.Windows.Forms.ToolStripMenuItem
    Dim _aux_request As GenericReport.ReportToolConfigRequest

    'set Sender
    aux_tool_strip = sender
    'create a new ReportToolConfigRequest object
    _aux_request = New GenericReport.ReportToolConfigRequest()
    aux_title_form = ""

    Select Case aux_tool_strip.Text
      Case NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7333))
        _aux_request.LocationMenu = GenericReport.LocationMenu.SystemMenu
        aux_title_form = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7333))
        aux_enum_form = ENUM_FORM.FORM_REPORT_TOOL_SYSTEM_MENU

      Case NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7338))
        _aux_request.LocationMenu = GenericReport.LocationMenu.TerminalsMenu
        aux_title_form = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7338))
        aux_enum_form = ENUM_FORM.FORM_REPORT_TOOL_TERMINALS_MENU

      Case NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7339))
        _aux_request.LocationMenu = GenericReport.LocationMenu.MonitorMenu
        aux_title_form = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7339))
        aux_enum_form = ENUM_FORM.FORM_REPORT_TOOL_MONITOR_MENU

      Case NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7340))
        _aux_request.LocationMenu = GenericReport.LocationMenu.AlarmsMenu
        aux_title_form = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7340))
        aux_enum_form = ENUM_FORM.FORM_REPORT_TOOL_ALARMS_MENU

      Case NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7341))
        _aux_request.LocationMenu = GenericReport.LocationMenu.StatisticsMenu
        aux_title_form = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7341))
        aux_enum_form = ENUM_FORM.FORM_REPORT_TOOL_STATISTICS_MENU

      Case NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7342))
        _aux_request.LocationMenu = GenericReport.LocationMenu.CashMenu
        aux_title_form = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7342))
        aux_enum_form = ENUM_FORM.FORM_REPORT_TOOL_CASH_MENU

      Case NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7343))
        _aux_request.LocationMenu = GenericReport.LocationMenu.CageMenu
        aux_title_form = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7343))
        aux_enum_form = ENUM_FORM.FORM_REPORT_TOOL_CAGE_MENU

      Case NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7344))
        _aux_request.LocationMenu = GenericReport.LocationMenu.AccountingMenu
        aux_title_form = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7344))
        aux_enum_form = ENUM_FORM.FORM_REPORT_TOOL_ACCOUNTING_MENU

      Case NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7345))
        _aux_request.LocationMenu = GenericReport.LocationMenu.CustomerMenu
        aux_title_form = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7345))
        aux_enum_form = ENUM_FORM.FORM_REPORT_TOOL_CUSTOMER_MENU

      Case NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7346))
        _aux_request.LocationMenu = GenericReport.LocationMenu.MarketingMenu
        aux_title_form = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7346))
        aux_enum_form = ENUM_FORM.FORM_REPORT_TOOL_MARKETING_MENU

      Case NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7347))
        _aux_request.LocationMenu = GenericReport.LocationMenu.JackpotsMenu
        aux_title_form = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7347))
        aux_enum_form = ENUM_FORM.FORM_REPORT_TOOL_JACKPOTS_MENU

      Case NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7348))
        _aux_request.LocationMenu = GenericReport.LocationMenu.GamingTableMenu
        aux_title_form = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7348))
        aux_enum_form = ENUM_FORM.FORM_REPORT_TOOL_GAMINGTABLE_MENU

      Case NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7349))
        _aux_request.LocationMenu = GenericReport.LocationMenu.SmartFloorMenu
        aux_title_form = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7349))
        aux_enum_form = ENUM_FORM.FORM_REPORT_TOOL_SMARTFLOOR_MENU

      Case NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7350))
        _aux_request.LocationMenu = GenericReport.LocationMenu.GameGatewayMenu
        aux_title_form = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7350))
        aux_enum_form = ENUM_FORM.FORM_REPORT_TOOL_GAMEGATEWAY_MENU

    End Select

    Dim _frm As frm_generic_report_sel
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_generic_report_sel(_obj_report_tool.GetMenuConfiguration(_aux_request), aux_enum_form)
    _frm.Text = aux_title_form
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  'RGR 14-JUL-2016'
  Private Sub m_frm_banking_report_sel_click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_banking_report_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    frm = New frm_banking_report_sel()
    Call frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  ' LA 25-JUL-2016
  Private Sub m_Backend_View_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_advertisement

    Windows.Forms.Cursor.Current = Cursors.Default

    ' Create instance
    frm = New frm_advertisement(WSI.Common.ADS_TARGET.CasinoOffers)
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  'LTC 05-AGO-2016
  Private Sub m_frm_banking_reconciliation_click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frmUser As frm_bank_reconciliation_user

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    frmUser = New frm_bank_reconciliation_user()
    Call frmUser.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_Click_FBM_Log_View(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_fbm_log

    'JRM 10/06/2013
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance 
    frm = New frm_fbm_log
    Call frm.ShowForEdit(Me)

    'XVV 16/04/2007
    'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  'RLO 30-MAR-2017
  Private Sub m_FormJackpots_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_jackpots_treeview_config

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_jackpots_treeview_config()
    Call _frm.Display(True)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  'JBP 02-MAY-2017
  Private Sub m_FormJackpotsHistoryMeters_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_jackpots_meters_history

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_jackpots_meters_history()
    Call _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

    _frm = Nothing

  End Sub

  'CCG 05-MAY-2017
  Private Sub m_FormJackpotsHistory_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_jackpots_history

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_jackpots_history()
    Call _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

    _frm = Nothing

  End Sub

  'CCG 05-MAY-2017
  Private Sub m_FormJackpotViewer_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_jackpot_viewers_treeview_config

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance 
    _frm = New frm_jackpot_viewers_treeview_config()
    Call _frm.Display(True)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  'ESE 12-AGO-2016
  Private Sub m_booking_terminal_config_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Try
      Dim _frm As frm_booking_terminal_config
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      ' Create instance
      _frm = New frm_booking_terminal_config
      Call _frm.ShowEditItem(Me)

      Windows.Forms.Cursor.Current = Cursors.Default
    Catch ex As Exception

    End Try
  End Sub

  'ATB 24-JUL-2017
  Private Sub m_terminal_reservation_monitor_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Try
      Dim _frm As frm_terminal_reservation_monitor
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      ' Create instance
      _frm = New frm_terminal_reservation_monitor
      Call _frm.ShowForEdit(Me)

      Windows.Forms.Cursor.Current = Cursors.Default
    Catch ex As Exception

    End Try
  End Sub

  Private Sub m_promogame_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_promogame_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_promogame_sel()
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_lottery_meters_adjustment_working_day_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Try
      Dim _frm As frm_lottery_meters_adjustment_working_day
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      ' Create instance
      _frm = New frm_lottery_meters_adjustment_working_day
      Call _frm.ShowForEdit(Me)

      Windows.Forms.Cursor.Current = Cursors.Default
    Catch ex As Exception

    End Try
  End Sub

  Private Sub m_report_profit_day_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Try
      Dim _frm As frm_report_profit_day_and_month
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      ' Create instance
      _frm = New frm_report_profit_day_and_month(frm_report_profit_day_and_month.ENUM_TYPE_FORM.FOR_DAY)
      Call _frm.ShowForEdit(Me)

      Windows.Forms.Cursor.Current = Cursors.Default
    Catch ex As Exception

    End Try
  End Sub

  Private Sub m_report_profit_month_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Try
      Dim _frm As frm_report_profit_day_and_month
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      ' Create instance
      _frm = New frm_report_profit_day_and_month(frm_report_profit_day_and_month.ENUM_TYPE_FORM.FOR_MONTH)
      Call _frm.ShowForEdit(Me)

      Windows.Forms.Cursor.Current = Cursors.Default
    Catch ex As Exception

    End Try
  End Sub

  Private Sub m_report_profit_machine_and_day_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Try
      Dim _frm As frm_report_profit_machine_and_day
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      ' Create instance
      _frm = New frm_report_profit_machine_and_day
      Call _frm.ShowForEdit(Me)

      Windows.Forms.Cursor.Current = Cursors.Default
    Catch ex As Exception

    End Try
  End Sub

  Private Sub m_cash_out_report_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_cash_out_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_cash_out_report
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

#End Region

#Region " Public Functions "

  Public Sub ShowMainWindow()

    Me.Display(True)

  End Sub

  Public Sub OnAutoLogout(ByVal Title As String, ByVal Message As String)

    If Me.InvokeRequired Then
      Me.Invoke(New Users.AutoLogoutEventHandler(AddressOf OnAutoLogout), New Object() {Title, Message})
    Else
      If frm_message.ShowTimer(Me, 60, Message, Title) = Windows.Forms.DialogResult.None Then

        Users.SetUserLoggedOff(Users.EXIT_CODE.EXPIRED)
        WSI.Common.Log.Close()
        Environment.Exit(0)
      Else
        WSI.Common.Users.SetLastAction("Continue", "")
      End If
    End If

  End Sub

#End Region

  Private Sub tmr_MoneyRequestPending_Event(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles tmr_MoneyRequestPending.Elapsed
    CheckPendingRequest()
  End Sub

  Private Sub tmr_TextBlinking_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmr_TextBlinking.Tick
    m_mnu.ItemTextBlinking(GLB_NLS_GUI_PLAYER_TRACKING.Id(6052))

  End Sub


End Class
