<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_progressive_sel
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.gb_progressive_name = New System.Windows.Forms.GroupBox
    Me.ef_progressive_name = New GUI_Controls.uc_entry_field
    Me.chk_enabled = New System.Windows.Forms.CheckBox
    Me.chk_disabled = New System.Windows.Forms.CheckBox
    Me.gb_progressive_status = New System.Windows.Forms.GroupBox
    Me.uc_pr_list = New GUI_Controls.uc_provider
    Me.gb_progressive_detail = New System.Windows.Forms.GroupBox
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox
    Me.opt_terminal = New System.Windows.Forms.RadioButton
    Me.opt_level = New System.Windows.Forms.RadioButton
    Me.opt_progressive = New System.Windows.Forms.RadioButton
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_progressive_name.SuspendLayout()
    Me.gb_progressive_status.SuspendLayout()
    Me.gb_progressive_detail.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_progressive_detail)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Controls.Add(Me.gb_progressive_status)
    Me.panel_filter.Controls.Add(Me.gb_progressive_name)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1002, 183)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_progressive_name, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_progressive_status, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_progressive_detail, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 187)
    Me.panel_data.Size = New System.Drawing.Size(1002, 368)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(996, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(996, 4)
    '
    'gb_progressive_name
    '
    Me.gb_progressive_name.Controls.Add(Me.ef_progressive_name)
    Me.gb_progressive_name.Location = New System.Drawing.Point(7, 6)
    Me.gb_progressive_name.Name = "gb_progressive_name"
    Me.gb_progressive_name.Size = New System.Drawing.Size(321, 60)
    Me.gb_progressive_name.TabIndex = 11
    Me.gb_progressive_name.TabStop = False
    Me.gb_progressive_name.Text = "xProgressiveName"
    '
    'ef_progressive_name
    '
    Me.ef_progressive_name.DoubleValue = 0
    Me.ef_progressive_name.IntegerValue = 0
    Me.ef_progressive_name.IsReadOnly = False
    Me.ef_progressive_name.Location = New System.Drawing.Point(7, 24)
    Me.ef_progressive_name.Name = "ef_progressive_name"
    Me.ef_progressive_name.Size = New System.Drawing.Size(298, 24)
    Me.ef_progressive_name.SufixText = "Sufix Text"
    Me.ef_progressive_name.SufixTextVisible = True
    Me.ef_progressive_name.TabIndex = 0
    Me.ef_progressive_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_progressive_name.TextValue = ""
    Me.ef_progressive_name.Value = ""
    Me.ef_progressive_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_enabled
    '
    Me.chk_enabled.AutoSize = True
    Me.chk_enabled.Location = New System.Drawing.Point(7, 20)
    Me.chk_enabled.Name = "chk_enabled"
    Me.chk_enabled.Size = New System.Drawing.Size(78, 17)
    Me.chk_enabled.TabIndex = 1
    Me.chk_enabled.Text = "xEnabled"
    Me.chk_enabled.UseVisualStyleBackColor = True
    '
    'chk_disabled
    '
    Me.chk_disabled.AutoSize = True
    Me.chk_disabled.Location = New System.Drawing.Point(7, 51)
    Me.chk_disabled.Name = "chk_disabled"
    Me.chk_disabled.Size = New System.Drawing.Size(82, 17)
    Me.chk_disabled.TabIndex = 2
    Me.chk_disabled.Text = "xDisabled"
    Me.chk_disabled.UseVisualStyleBackColor = True
    '
    'gb_progressive_status
    '
    Me.gb_progressive_status.Controls.Add(Me.chk_enabled)
    Me.gb_progressive_status.Controls.Add(Me.chk_disabled)
    Me.gb_progressive_status.Location = New System.Drawing.Point(7, 72)
    Me.gb_progressive_status.Name = "gb_progressive_status"
    Me.gb_progressive_status.Size = New System.Drawing.Size(137, 76)
    Me.gb_progressive_status.TabIndex = 12
    Me.gb_progressive_status.TabStop = False
    Me.gb_progressive_status.Text = "xProgressiveStatus"
    '
    'uc_pr_list
    '
    Me.uc_pr_list.Location = New System.Drawing.Point(334, 3)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(318, 174)
    Me.uc_pr_list.TabIndex = 14
    '
    'gb_progressive_detail
    '
    Me.gb_progressive_detail.Controls.Add(Me.chk_terminal_location)
    Me.gb_progressive_detail.Controls.Add(Me.opt_terminal)
    Me.gb_progressive_detail.Controls.Add(Me.opt_level)
    Me.gb_progressive_detail.Controls.Add(Me.opt_progressive)
    Me.gb_progressive_detail.Location = New System.Drawing.Point(667, 6)
    Me.gb_progressive_detail.Name = "gb_progressive_detail"
    Me.gb_progressive_detail.Size = New System.Drawing.Size(238, 134)
    Me.gb_progressive_detail.TabIndex = 16
    Me.gb_progressive_detail.TabStop = False
    Me.gb_progressive_detail.Text = "xProgressiveDetail"
    '
    'chk_terminal_location
    '
    Me.chk_terminal_location.Location = New System.Drawing.Point(28, 107)
    Me.chk_terminal_location.Name = "chk_terminal_location"
    Me.chk_terminal_location.Size = New System.Drawing.Size(204, 21)
    Me.chk_terminal_location.TabIndex = 17
    Me.chk_terminal_location.Text = "xShow terminals location"
    '
    'opt_terminal
    '
    Me.opt_terminal.AutoSize = True
    Me.opt_terminal.Location = New System.Drawing.Point(17, 84)
    Me.opt_terminal.Name = "opt_terminal"
    Me.opt_terminal.Size = New System.Drawing.Size(89, 17)
    Me.opt_terminal.TabIndex = 3
    Me.opt_terminal.TabStop = True
    Me.opt_terminal.Text = "x_Terminal"
    Me.opt_terminal.UseVisualStyleBackColor = True
    '
    'opt_level
    '
    Me.opt_level.AutoSize = True
    Me.opt_level.Location = New System.Drawing.Point(17, 51)
    Me.opt_level.Name = "opt_level"
    Me.opt_level.Size = New System.Drawing.Size(69, 17)
    Me.opt_level.TabIndex = 2
    Me.opt_level.TabStop = True
    Me.opt_level.Text = "x_Level"
    Me.opt_level.UseVisualStyleBackColor = True
    '
    'opt_progressive
    '
    Me.opt_progressive.AutoSize = True
    Me.opt_progressive.Location = New System.Drawing.Point(17, 20)
    Me.opt_progressive.Name = "opt_progressive"
    Me.opt_progressive.Size = New System.Drawing.Size(106, 17)
    Me.opt_progressive.TabIndex = 1
    Me.opt_progressive.TabStop = True
    Me.opt_progressive.Text = "x_Progressive"
    Me.opt_progressive.UseVisualStyleBackColor = True
    '
    'frm_progressive_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1012, 559)
    Me.Name = "frm_progressive_sel"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_progressive_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_progressive_name.ResumeLayout(False)
    Me.gb_progressive_status.ResumeLayout(False)
    Me.gb_progressive_status.PerformLayout()
    Me.gb_progressive_detail.ResumeLayout(False)
    Me.gb_progressive_detail.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_progressive_name As System.Windows.Forms.GroupBox
  Friend WithEvents ef_progressive_name As GUI_Controls.uc_entry_field
  Friend WithEvents chk_disabled As System.Windows.Forms.CheckBox
  Friend WithEvents chk_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents gb_progressive_status As System.Windows.Forms.GroupBox
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents gb_progressive_detail As System.Windows.Forms.GroupBox
  Friend WithEvents opt_progressive As System.Windows.Forms.RadioButton
  Friend WithEvents opt_terminal As System.Windows.Forms.RadioButton
  Friend WithEvents opt_level As System.Windows.Forms.RadioButton
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox
End Class
