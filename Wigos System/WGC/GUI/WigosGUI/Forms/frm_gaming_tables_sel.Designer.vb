<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_gaming_tables_sel
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.txt_code = New GUI_Controls.uc_entry_field
    Me.txt_name = New GUI_Controls.uc_entry_field
    Me.gb_enabled = New System.Windows.Forms.GroupBox
    Me.chk_no = New System.Windows.Forms.CheckBox
    Me.chk_yes = New System.Windows.Forms.CheckBox
    Me.gb_area_island = New System.Windows.Forms.GroupBox
    Me.cmb_area = New GUI_Controls.uc_combo
    Me.ef_floor_id = New GUI_Controls.uc_entry_field
    Me.cmb_bank = New GUI_Controls.uc_combo
    Me.ef_smoking = New GUI_Controls.uc_entry_field
    Me.gb_table_type = New System.Windows.Forms.GroupBox
    Me.opt_all_types = New System.Windows.Forms.RadioButton
    Me.opt_several_types = New System.Windows.Forms.RadioButton
    Me.dg_filter = New GUI_Controls.uc_grid
    Me.gb_cashier = New System.Windows.Forms.GroupBox
    Me.chk_not_integrated = New System.Windows.Forms.CheckBox
    Me.chk_integrated = New System.Windows.Forms.CheckBox
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_enabled.SuspendLayout()
    Me.gb_area_island.SuspendLayout()
    Me.gb_table_type.SuspendLayout()
    Me.gb_cashier.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_area_island)
    Me.panel_filter.Controls.Add(Me.gb_table_type)
    Me.panel_filter.Controls.Add(Me.gb_cashier)
    Me.panel_filter.Controls.Add(Me.gb_enabled)
    Me.panel_filter.Controls.Add(Me.txt_name)
    Me.panel_filter.Controls.Add(Me.txt_code)
    Me.panel_filter.Size = New System.Drawing.Size(1243, 148)
    Me.panel_filter.Controls.SetChildIndex(Me.txt_code, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.txt_name, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_enabled, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_cashier, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_table_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_area_island, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 152)
    Me.panel_data.Size = New System.Drawing.Size(1243, 405)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1237, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1237, 4)
    '
    'txt_code
    '
    Me.txt_code.DoubleValue = 0
    Me.txt_code.IntegerValue = 0
    Me.txt_code.IsReadOnly = False
    Me.txt_code.Location = New System.Drawing.Point(6, 6)
    Me.txt_code.Name = "txt_code"
    Me.txt_code.Size = New System.Drawing.Size(327, 24)
    Me.txt_code.SufixText = "Sufix Text"
    Me.txt_code.SufixTextVisible = True
    Me.txt_code.TabIndex = 0
    Me.txt_code.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.txt_code.TextValue = ""
    Me.txt_code.TextWidth = 120
    Me.txt_code.Value = ""
    Me.txt_code.ValueForeColor = System.Drawing.Color.Blue
    '
    'txt_name
    '
    Me.txt_name.DoubleValue = 0
    Me.txt_name.IntegerValue = 0
    Me.txt_name.IsReadOnly = False
    Me.txt_name.Location = New System.Drawing.Point(6, 36)
    Me.txt_name.Name = "txt_name"
    Me.txt_name.Size = New System.Drawing.Size(327, 24)
    Me.txt_name.SufixText = "Sufix Text"
    Me.txt_name.SufixTextVisible = True
    Me.txt_name.TabIndex = 1
    Me.txt_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.txt_name.TextValue = ""
    Me.txt_name.TextWidth = 120
    Me.txt_name.Value = ""
    Me.txt_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_enabled
    '
    Me.gb_enabled.Controls.Add(Me.chk_no)
    Me.gb_enabled.Controls.Add(Me.chk_yes)
    Me.gb_enabled.Location = New System.Drawing.Point(128, 66)
    Me.gb_enabled.Name = "gb_enabled"
    Me.gb_enabled.Size = New System.Drawing.Size(78, 71)
    Me.gb_enabled.TabIndex = 2
    Me.gb_enabled.TabStop = False
    Me.gb_enabled.Text = "xEnabled"
    '
    'chk_no
    '
    Me.chk_no.AutoSize = True
    Me.chk_no.Location = New System.Drawing.Point(11, 47)
    Me.chk_no.Name = "chk_no"
    Me.chk_no.Size = New System.Drawing.Size(48, 17)
    Me.chk_no.TabIndex = 1
    Me.chk_no.Text = "xNo"
    Me.chk_no.UseVisualStyleBackColor = True
    '
    'chk_yes
    '
    Me.chk_yes.AutoSize = True
    Me.chk_yes.Location = New System.Drawing.Point(11, 24)
    Me.chk_yes.Name = "chk_yes"
    Me.chk_yes.Size = New System.Drawing.Size(53, 17)
    Me.chk_yes.TabIndex = 0
    Me.chk_yes.Text = "xYes"
    Me.chk_yes.UseVisualStyleBackColor = True
    '
    'gb_area_island
    '
    Me.gb_area_island.Controls.Add(Me.cmb_area)
    Me.gb_area_island.Controls.Add(Me.ef_floor_id)
    Me.gb_area_island.Controls.Add(Me.cmb_bank)
    Me.gb_area_island.Controls.Add(Me.ef_smoking)
    Me.gb_area_island.Location = New System.Drawing.Point(712, 6)
    Me.gb_area_island.Name = "gb_area_island"
    Me.gb_area_island.Size = New System.Drawing.Size(361, 131)
    Me.gb_area_island.TabIndex = 5
    Me.gb_area_island.TabStop = False
    Me.gb_area_island.Text = "xLocation"
    '
    'cmb_area
    '
    Me.cmb_area.AllowUnlistedValues = False
    Me.cmb_area.AutoCompleteMode = False
    Me.cmb_area.IsReadOnly = False
    Me.cmb_area.Location = New System.Drawing.Point(11, 12)
    Me.cmb_area.Name = "cmb_area"
    Me.cmb_area.SelectedIndex = -1
    Me.cmb_area.Size = New System.Drawing.Size(327, 24)
    Me.cmb_area.SufixText = "Sufix Text"
    Me.cmb_area.SufixTextVisible = True
    Me.cmb_area.TabIndex = 0
    Me.cmb_area.TextWidth = 120
    '
    'ef_floor_id
    '
    Me.ef_floor_id.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.ef_floor_id.DoubleValue = 0
    Me.ef_floor_id.IntegerValue = 0
    Me.ef_floor_id.IsReadOnly = False
    Me.ef_floor_id.Location = New System.Drawing.Point(11, 100)
    Me.ef_floor_id.Name = "ef_floor_id"
    Me.ef_floor_id.Size = New System.Drawing.Size(327, 24)
    Me.ef_floor_id.SufixText = "Sufix Text"
    Me.ef_floor_id.SufixTextVisible = True
    Me.ef_floor_id.TabIndex = 3
    Me.ef_floor_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_floor_id.TextValue = ""
    Me.ef_floor_id.TextWidth = 120
    Me.ef_floor_id.Value = ""
    Me.ef_floor_id.ValueForeColor = System.Drawing.Color.Blue
    '
    'cmb_bank
    '
    Me.cmb_bank.AllowUnlistedValues = False
    Me.cmb_bank.AutoCompleteMode = False
    Me.cmb_bank.IsReadOnly = False
    Me.cmb_bank.Location = New System.Drawing.Point(11, 70)
    Me.cmb_bank.Name = "cmb_bank"
    Me.cmb_bank.SelectedIndex = -1
    Me.cmb_bank.Size = New System.Drawing.Size(327, 24)
    Me.cmb_bank.SufixText = "Sufix Text"
    Me.cmb_bank.SufixTextVisible = True
    Me.cmb_bank.TabIndex = 2
    Me.cmb_bank.TextWidth = 120
    '
    'ef_smoking
    '
    Me.ef_smoking.DoubleValue = 0
    Me.ef_smoking.IntegerValue = 0
    Me.ef_smoking.IsReadOnly = False
    Me.ef_smoking.Location = New System.Drawing.Point(11, 40)
    Me.ef_smoking.Name = "ef_smoking"
    Me.ef_smoking.Size = New System.Drawing.Size(327, 24)
    Me.ef_smoking.SufixText = "Sufix Text"
    Me.ef_smoking.SufixTextVisible = True
    Me.ef_smoking.TabIndex = 1
    Me.ef_smoking.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_smoking.TextValue = ""
    Me.ef_smoking.TextWidth = 120
    Me.ef_smoking.Value = ""
    Me.ef_smoking.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_table_type
    '
    Me.gb_table_type.Controls.Add(Me.opt_all_types)
    Me.gb_table_type.Controls.Add(Me.opt_several_types)
    Me.gb_table_type.Controls.Add(Me.dg_filter)
    Me.gb_table_type.Location = New System.Drawing.Point(360, 6)
    Me.gb_table_type.Name = "gb_table_type"
    Me.gb_table_type.Size = New System.Drawing.Size(323, 131)
    Me.gb_table_type.TabIndex = 4
    Me.gb_table_type.TabStop = False
    Me.gb_table_type.Text = "xTableType"
    '
    'opt_all_types
    '
    Me.opt_all_types.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.opt_all_types.Location = New System.Drawing.Point(8, 45)
    Me.opt_all_types.Name = "opt_all_types"
    Me.opt_all_types.Size = New System.Drawing.Size(64, 24)
    Me.opt_all_types.TabIndex = 2
    Me.opt_all_types.Text = "xAll"
    '
    'opt_several_types
    '
    Me.opt_several_types.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.opt_several_types.Location = New System.Drawing.Point(8, 17)
    Me.opt_several_types.Name = "opt_several_types"
    Me.opt_several_types.Size = New System.Drawing.Size(72, 24)
    Me.opt_several_types.TabIndex = 0
    Me.opt_several_types.Text = "xSeveral"
    '
    'dg_filter
    '
    Me.dg_filter.CurrentCol = -1
    Me.dg_filter.CurrentRow = -1
    Me.dg_filter.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_filter.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_filter.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_filter.Location = New System.Drawing.Point(86, 16)
    Me.dg_filter.Name = "dg_filter"
    Me.dg_filter.PanelRightVisible = False
    Me.dg_filter.Redraw = True
    Me.dg_filter.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_filter.Size = New System.Drawing.Size(206, 108)
    Me.dg_filter.Sortable = False
    Me.dg_filter.SortAscending = True
    Me.dg_filter.SortByCol = 0
    Me.dg_filter.TabIndex = 1
    Me.dg_filter.ToolTipped = True
    Me.dg_filter.TopRow = -2
    '
    'gb_cashier
    '
    Me.gb_cashier.Controls.Add(Me.chk_not_integrated)
    Me.gb_cashier.Controls.Add(Me.chk_integrated)
    Me.gb_cashier.Location = New System.Drawing.Point(212, 66)
    Me.gb_cashier.Name = "gb_cashier"
    Me.gb_cashier.Size = New System.Drawing.Size(121, 71)
    Me.gb_cashier.TabIndex = 3
    Me.gb_cashier.TabStop = False
    Me.gb_cashier.Text = "xCashier"
    '
    'chk_not_integrated
    '
    Me.chk_not_integrated.AutoSize = True
    Me.chk_not_integrated.Location = New System.Drawing.Point(11, 47)
    Me.chk_not_integrated.Name = "chk_not_integrated"
    Me.chk_not_integrated.Size = New System.Drawing.Size(108, 17)
    Me.chk_not_integrated.TabIndex = 1
    Me.chk_not_integrated.Text = "xNoIntegrated"
    Me.chk_not_integrated.UseVisualStyleBackColor = True
    '
    'chk_integrated
    '
    Me.chk_integrated.AutoSize = True
    Me.chk_integrated.Location = New System.Drawing.Point(11, 24)
    Me.chk_integrated.Name = "chk_integrated"
    Me.chk_integrated.Size = New System.Drawing.Size(93, 17)
    Me.chk_integrated.TabIndex = 0
    Me.chk_integrated.Text = "xIntegrated"
    Me.chk_integrated.UseVisualStyleBackColor = True
    '
    'frm_gaming_tables_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1251, 561)
    Me.Name = "frm_gaming_tables_sel"
    Me.Text = "gaming_tables_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_enabled.ResumeLayout(False)
    Me.gb_enabled.PerformLayout()
    Me.gb_area_island.ResumeLayout(False)
    Me.gb_table_type.ResumeLayout(False)
    Me.gb_cashier.ResumeLayout(False)
    Me.gb_cashier.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents txt_name As GUI_Controls.uc_entry_field
  Friend WithEvents txt_code As GUI_Controls.uc_entry_field
  Friend WithEvents gb_enabled As System.Windows.Forms.GroupBox
  Friend WithEvents chk_no As System.Windows.Forms.CheckBox
  Friend WithEvents chk_yes As System.Windows.Forms.CheckBox
  Friend WithEvents gb_area_island As System.Windows.Forms.GroupBox
  Friend WithEvents cmb_area As GUI_Controls.uc_combo
  Friend WithEvents ef_floor_id As GUI_Controls.uc_entry_field
  Friend WithEvents cmb_bank As GUI_Controls.uc_combo
  Friend WithEvents ef_smoking As GUI_Controls.uc_entry_field
  Friend WithEvents gb_table_type As System.Windows.Forms.GroupBox
  Friend WithEvents opt_all_types As System.Windows.Forms.RadioButton
  Friend WithEvents opt_several_types As System.Windows.Forms.RadioButton
  Friend WithEvents dg_filter As GUI_Controls.uc_grid
  Friend WithEvents gb_cashier As System.Windows.Forms.GroupBox
  Friend WithEvents chk_not_integrated As System.Windows.Forms.CheckBox
  Friend WithEvents chk_integrated As System.Windows.Forms.CheckBox
End Class
