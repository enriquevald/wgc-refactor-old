'-----------------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd. 
'-----------------------------------------------------------------------------
'
' MODULE NAME:   frm_table_player_movements
' DESCRIPTION:   This screen shows game tables player movements
' AUTHOR:        Javier Molina
' CREATION DATE: 16-JUN-2014
' 
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ ---------------------------------------------------------
' 16-JUN-2014  JMM    Initial version
' 06-AGU-2014  DHA    Fixed Bug #WIG-1157: show virtual accounts
' 16-SEP-2014  DCS    New Show filter with gaming table id
' 30-SEP-2014  DCS    Fixed Bug #WIG-1351: corrected visible info for Unknown accounts and clean sesion id
' 07-OCT-2014  DHA    Fixed Bug WIG-1424: set filters properly
' 25-JUN-2015  FAV    Fixed Bug #WIG-2434: The'Table gaming movements' report loses the filters when the user press the 'search' button again.
' 14-JUL-2016  RAB    Product Backlog Item 15066: GamingTables (Phase 4): Adapt reports to MultiCurrency
' 28-MAY-2018  EAV    WIGOS-12185 Fixing issue in Table Game Movement from table game sessions
' 19-JUN-2018  DMT    WIGOS-13052 Filter by Account does not appear when exporting to excel and when printing report 'Table gaming movements' when coming from 'Table gaming session report' or 'Table gaming session editor'.
'------------------------------------------------------------------------------

#Region "Imports"

Imports System.Text
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions

Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations
Imports WSI.Common
Imports WSI.Common.Terminal
Imports WSI.Common.MultiPromos
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE

#End Region

Public Class frm_table_player_movements
  Inherits frm_base_sel_edit

#Region "Constants"

  Private Const GRID_COLUMNS_COUNT As Integer = 15
  Private Const GRID_HEADERS_COUNT As Integer = 2

  Private Const GRID_COL_ACCOUNT_ID As Integer = 0
  Private Const GRID_COL_ACCOUNT_TRACK_DATA As Integer = 1
  Private Const GRID_COL_ACCOUNT_HOLDER_NAME As Integer = 2
  Private Const GRID_COL_GAME_TABLE_TYPE As Integer = 3
  Private Const GRID_COL_GAME_TABLE As Integer = 4
  Private Const GRID_COL_ATTENDANT As Integer = 5
  Private Const GRID_COL_SEAT As Integer = 6
  Private Const GRID_COL_DATETIME As Integer = 7
  Private Const GRID_COL_DESC As Integer = 8
  Private Const GRID_COL_CASHIER As Integer = 9
  Private Const GRID_COL_OLD_VALUE As Integer = 10
  Private Const GRID_COL_VALUE As Integer = 11
  Private Const GRID_COL_TERMINAL_ID As Integer = 12
  Private Const GRID_COL_PLAY_SESSION As Integer = 13
  Private Const GRID_COL_USER As Integer = 14

  'SQL Columns
  Private Const SQL_COL_DATETIME As Integer = 0
  Private Const SQL_COL_USER_ID As Integer = 1
  Private Const SQL_COL_GAME_TABLE_TYPE As Integer = 2
  Private Const SQL_COL_GAME_TABLE As Integer = 3
  Private Const SQL_COL_ATTENDANT As Integer = 4
  Private Const SQL_COL_SEAT_ID As Integer = 5
  Private Const SQL_COL_CASHIER_ID As Integer = 6
  Private Const SQL_COL_TERMINAL_ID As Integer = 7
  Private Const SQL_COL_PLAY_SESSION_ID As Integer = 8
  Private Const SQL_COL_ACCOUNT_ID As Integer = 9
  Private Const SQL_COL_ACCOUNT_HOLDER_NAME As Integer = 10
  Private Const SQL_COL_ACCOUNT_TYPE As Integer = 11
  Private Const SQL_COL_ACCOUNT_TRACK_DATA As Integer = 12
  Private Const SQL_COL_OLD_VALUE As Integer = 13
  Private Const SQL_COL_VALUE As Integer = 14
  Private Const SQL_COL_NLS_DESC As Integer = 15
  Private Const SQL_COL_ISO_CODE As Integer = 16

  ' Mov. types data grid
  Private Const GRID_MOVS_FILTER_COL_TYPE As Integer = 0
  Private Const GRID_MOVS_FILTER_COL_CHECKED As Integer = 1
  Private Const GRID_MOVS_FILTER_COL_DESC As Integer = 2

  Private Const GRID_MOVS_HEADERS_COUNT As Integer = 0
  Private Const GRID_MOVS_FILTER_COLUMNS As Integer = 3

#End Region

#Region " Enums "

  Public Structure TYPE_DG_FILTER_ELEMENT
    Dim type As Integer
    Dim description As String
  End Structure

#End Region

#Region "Structures"

  Private Structure GamingTablesGroup
    Public Id As Int32
    Public Type As String
    Public Enabled As Boolean
    Public GamingTables As List(Of GamingTable)
  End Structure

  Private Structure GamingTable
    Public Id As Int32
    Public Name As String
  End Structure

#End Region

#Region "Members"

  Private bl_sessions As Boolean = False
  Private m_session_id As Int64
  Private m_account_id As Int64
  Private m_table_session_id As Int64
  Private m_table_name As String

  Private m_all_gaming_tables As List(Of GamingTablesGroup)

  ' For report filters
  Private m_date_from As String
  Private m_date_to As String
  Private m_card_account As String
  Private m_card_track_data As String
  Private m_card_holder_name As String
  Private m_holder_is_vip As String
  Private m_gaming_tables As String
  Private m_cashier As String
  Private m_movement_type As String

  Private m_init_date_from As Date
  Private m_init_date_to As Date

#End Region

#Region "Overridden methods: frm_base_sel_edit"

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId() ' GUI_SetFormId 
    Me.FormId = ENUM_FORM.FROM_TABLE_PLAYER_MOVEMENTS
    MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId 

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5049)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    ' Date
    Me.gb_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4976)
    Me.dtp_from.Text = GLB_NLS_GUI_AUDITOR.GetString(257)
    Me.dtp_to.Text = GLB_NLS_GUI_AUDITOR.GetString(258)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Terminals
    Me.gb_cashier.Visible = True
    Me.gb_cashier.Text = GLB_NLS_GUI_INVOICING.GetString(210)
    Me.opt_one_cashier.Text = GLB_NLS_GUI_INVOICING.GetString(234)
    Me.opt_all_terminals.Text = GLB_NLS_GUI_INVOICING.GetString(224)

    Call GUI_StyleSheet()

    ' Mov. Type Grid Filter
    Me.gb_move_type.Text = GLB_NLS_GUI_INVOICING.GetString(211)
    Me.opt_several_types.Text = GLB_NLS_GUI_INVOICING.GetString(223)
    Me.opt_all_types.Text = GLB_NLS_GUI_INVOICING.GetString(224)
    Call GUI_StyleSheetMovementTypes()

    ' Set combo with Cashier Terminals Alias
    Call SetComboCashierAlias(Me.cmb_cashier)
    Me.cmb_cashier.Enabled = False

    ' Groups
    Me.uc_checked_list_tables.GroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3416)
    Me.m_all_gaming_tables = LoadGamingTablesByType()

    ' TITO Mode
    If WSI.Common.TITO.Utils.IsTitoMode() Then
      uc_account_sel1.InitExtendedQuery(True)
    End If

    SetDefaultValues()

    If Me.bl_sessions = True Then
      Me.uc_checked_list_tables.Enabled = False
      Me.gb_move_type.Enabled = False
      Me.gb_cashier.Enabled = False
      Me.uc_account_sel1.Account = Me.m_account_id.ToString()
      Me.uc_account_sel1.Enabled = False
    End If

    'If m_session_id <> 0 Then
    'Me.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)
    'Me.uc_account_sel1.Enabled = False
    'Me.uc_checked_list_tables.Enabled = False
    'End If

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()

    SetDefaultValues()

  End Sub ' GUI_FilterReset

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Me.dtp_to.Focus()

        Return False
      End If
    End If

    Return True

  End Function ' GUI_FilterCheck

  ' PURPOSE : Sets initial focus
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  '          
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.uc_account_sel1
  End Sub ' GUI_SetInitialFocus

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As CLASS_DB_ROW) As Boolean ' GUI_SetupRow

    Dim _track_data As String
    Dim _account_type As AccountType

    ' Account type
    If Not DbRow.IsNull(SQL_COL_ACCOUNT_TYPE) Then
      _account_type = DbRow.Value(SQL_COL_ACCOUNT_TYPE)    
    End If

    ' Account ID
    If Not DbRow.IsNull(SQL_COL_ACCOUNT_ID) Then
      If _account_type = AccountType.ACCOUNT_VIRTUAL_TERMINAL Then
        Me.Grid.Cell(RowIndex, GRID_COL_ACCOUNT_ID).Value = String.Empty
      Else
        Me.Grid.Cell(RowIndex, GRID_COL_ACCOUNT_ID).Value = DbRow.Value(SQL_COL_ACCOUNT_ID)
      End If
    End If

    ' Card Track Data
    _track_data = ""
    If Not DbRow.IsNull(SQL_COL_ACCOUNT_TRACK_DATA) Then
      CardNumber.VisibleTrackData(_track_data, DbRow.Value(SQL_COL_ACCOUNT_TRACK_DATA), MAGNETIC_CARD_TYPES.CARD_TYPE_PLAYER, _account_type)
      Me.Grid.Cell(RowIndex, GRID_COL_ACCOUNT_TRACK_DATA).Value = _track_data
    End If

    ' Account Holder Name
    If _account_type = AccountType.ACCOUNT_VIRTUAL_TERMINAL Then
      Me.Grid.Cell(RowIndex, GRID_COL_ACCOUNT_HOLDER_NAME).Value = WSI.Common.Resource.String("STR_CASHIER_GAMING_TABLE_PLAYER_UNKNOWN_BUTTON")
    Else
      If Not DbRow.IsNull(SQL_COL_ACCOUNT_HOLDER_NAME) Then
        Me.Grid.Cell(RowIndex, GRID_COL_ACCOUNT_HOLDER_NAME).Value = DbRow.Value(SQL_COL_ACCOUNT_HOLDER_NAME)
      End If
    End If

    ' Datetime
    If Not DbRow.IsNull(SQL_COL_DATETIME) Then
      Me.Grid.Cell(RowIndex, GRID_COL_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COL_DATETIME), _
                                                                       ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                       ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' User
    If Not DbRow.IsNull(SQL_COL_USER_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COL_USER).Value = DbRow.Value(SQL_COL_USER_ID)
    End If

    ' Table Type 
    If Not DbRow.IsNull(SQL_COL_GAME_TABLE_TYPE) Then
      Me.Grid.Cell(RowIndex, GRID_COL_GAME_TABLE_TYPE).Value = DbRow.Value(SQL_COL_GAME_TABLE_TYPE)
    End If

    ' Table name
    If Not DbRow.IsNull(SQL_COL_GAME_TABLE) Then
      Me.Grid.Cell(RowIndex, GRID_COL_GAME_TABLE).Value = DbRow.Value(SQL_COL_GAME_TABLE)
    End If

    ' Attendant
    If Not DbRow.IsNull(SQL_COL_ATTENDANT) Then
      Me.Grid.Cell(RowIndex, GRID_COL_ATTENDANT).Value = DbRow.Value(SQL_COL_ATTENDANT)
    End If

    ' Cashier
    If Not DbRow.IsNull(SQL_COL_CASHIER_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COL_CASHIER).Value = DbRow.Value(SQL_COL_CASHIER_ID)
    End If

    ' Terminal
    If Not DbRow.IsNull(SQL_COL_TERMINAL_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COL_TERMINAL_ID).Value = DbRow.Value(SQL_COL_TERMINAL_ID)
    End If

    ' Play Session
    If Not DbRow.IsNull(SQL_COL_PLAY_SESSION_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COL_PLAY_SESSION).Value = DbRow.Value(SQL_COL_PLAY_SESSION_ID)
    End If

    ' Seat
    If Not DbRow.IsNull(SQL_COL_SEAT_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COL_SEAT).Value = DbRow.Value(SQL_COL_SEAT_ID)
    End If

    ' Account ID
    'If Not DbRow.IsNull(SQL_COL_ACCOUNT_ID) Then
    '  Me.Grid.Cell(RowIndex, GRID_COL_ACCOUNT_ID).Value = DbRow.Value(SQL_COL_ACCOUNT_ID)
    'End If

    '' Account Trackdata
    'If Not DbRow.IsNull(SQL_COL_ACCOUNT_ID) Then
    '  Me.Grid.Cell(RowIndex, GRID_COL_ACCOUNT_ID).Value = DbRow.Value(SQL_COL_ACCOUNT_ID)
    'End If

    '' Account Holder Name
    'If Not DbRow.IsNull(SQL_COL_ACCOUNT_HOLDER_NAME) Then
    '  Me.Grid.Cell(RowIndex, GRID_COL_ACCOUNT_HOLDER_NAME).Value = DbRow.Value(SQL_COL_ACCOUNT_HOLDER_NAME)
    'End If

    ' Old Value
    If Not DbRow.IsNull(SQL_COL_OLD_VALUE) Then

      If DbRow.Value(SQL_COL_OLD_VALUE) > 0 Then

        Select Case (DbRow.Value(SQL_COL_NLS_DESC))
          Case WSI.Common.PlayerTrackingMovementType.PausePlay
          Case WSI.Common.PlayerTrackingMovementType.ContinuePlay
            'Me.Grid.Cell(RowIndex, GRID_COL_OLD_VALUE).Value = GUI_FormatNumber(DbRow.Value(SQL_COL_OLD_VALUE), 0)
          Case WSI.Common.PlayerTrackingMovementType.SwapSeat
            Me.Grid.Cell(RowIndex, GRID_COL_OLD_VALUE).Value = GUI_FormatNumber(DbRow.Value(SQL_COL_OLD_VALUE), 0)
          Case WSI.Common.PlayerTrackingMovementType.PointsWon
            'Me.Grid.Cell(RowIndex, GRID_COL_OLD_VALUE).Value = GUI_FormatNumber(DbRow.Value(SQL_COL_OLD_VALUE), 2)
          Case WSI.Common.PlayerTrackingMovementType.Skill
            Me.Grid.Cell(RowIndex, GRID_COL_OLD_VALUE).Value = GetSkillDescription(DbRow.Value(SQL_COL_OLD_VALUE))
          Case WSI.Common.PlayerTrackingMovementType.Speed
            Me.Grid.Cell(RowIndex, GRID_COL_OLD_VALUE).Value = GetSpeedDescription(DbRow.Value(SQL_COL_OLD_VALUE))
          Case WSI.Common.PlayerTrackingMovementType.ContinueTablePlay
          Case WSI.Common.PlayerTrackingMovementType.PauseTablePlay
          Case WSI.Common.PlayerTrackingMovementType.TableSpeed
            '--
          Case Else
            Me.Grid.Cell(RowIndex, GRID_COL_OLD_VALUE).Value = GUI_FormatCurrency(DbRow.Value(SQL_COL_OLD_VALUE), CurrencySymbol:=False)
        End Select

        'Else
        '  Me.Grid.Cell(RowIndex, GRID_COL_OLD_VALUE).Value = ""
      End If

    End If

    ' Value
    If Not DbRow.IsNull(SQL_COL_VALUE) Then

      If DbRow.Value(SQL_COL_VALUE) > 0 Then

        Select Case (DbRow.Value(SQL_COL_NLS_DESC))
          Case WSI.Common.PlayerTrackingMovementType.PausePlay
          Case WSI.Common.PlayerTrackingMovementType.ContinuePlay
            'Me.Grid.Cell(RowIndex, GRID_COL_VALUE).Value = GUI_FormatNumber(DbRow.Value(SQL_COL_VALUE), 0)
          Case WSI.Common.PlayerTrackingMovementType.SwapSeat
            Me.Grid.Cell(RowIndex, GRID_COL_VALUE).Value = GUI_FormatNumber(DbRow.Value(SQL_COL_VALUE), 0)
          Case WSI.Common.PlayerTrackingMovementType.PointsWon
            Me.Grid.Cell(RowIndex, GRID_COL_VALUE).Value = GUI_FormatNumber(DbRow.Value(SQL_COL_VALUE) - DbRow.Value(SQL_COL_OLD_VALUE), 2)
          Case WSI.Common.PlayerTrackingMovementType.Skill
            Me.Grid.Cell(RowIndex, GRID_COL_VALUE).Value = GetSkillDescription(DbRow.Value(SQL_COL_VALUE))
          Case WSI.Common.PlayerTrackingMovementType.Speed
            Me.Grid.Cell(RowIndex, GRID_COL_VALUE).Value = GetSpeedDescription(DbRow.Value(SQL_COL_VALUE))
          Case WSI.Common.PlayerTrackingMovementType.ContinueTablePlay
          Case WSI.Common.PlayerTrackingMovementType.PauseTablePlay
          Case WSI.Common.PlayerTrackingMovementType.TableSpeed
            '--
          Case Else
            Me.Grid.Cell(RowIndex, GRID_COL_VALUE).Value = GUI_FormatCurrency(DbRow.Value(SQL_COL_VALUE), CurrencySymbol:=False)
        End Select

        'Else
        '  Me.Grid.Cell(RowIndex, GRID_COL_OLD_VALUE).Value = ""
      End If

    End If

    ' Desc
    If Not DbRow.IsNull(SQL_COL_NLS_DESC) Then
      Me.Grid.Cell(RowIndex, GRID_COL_DESC).Value = PlayerTrackingMovements.GetMovementDesc(DbRow.Value(SQL_COL_NLS_DESC), DbRow.Value(SQL_COL_ISO_CODE))
      If DbRow.Value(SQL_COL_NLS_DESC) = WSI.Common.PlayerTrackingMovementType.TableSpeed Then
        If Not DbRow.IsNull(SQL_COL_OLD_VALUE) And Not DbRow.IsNull(SQL_COL_VALUE) Then
          Me.Grid.Cell(RowIndex, GRID_COL_DESC).Value += PlayerTrackingMovements.GetSpeedDesc(DbRow.Value(SQL_COL_OLD_VALUE), DbRow.Value(SQL_COL_VALUE))
        End If
      End If
    End If

    Return True
  End Function ' GUI_SetupRow

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA) ' GUI_ReportFilter

    ' Dates
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_AUDITOR.GetString(257), Me.m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), Me.m_date_to)

    ' Account
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), Me.m_card_account)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), Me.m_card_track_data)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), Me.m_card_holder_name)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4802), Me.m_holder_is_vip)

    'Cashier
    If Me.opt_one_cashier.Checked Then
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(234), m_cashier)
    End If
    If Me.opt_all_terminals.Checked Then
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(251), m_cashier)
    End If

    ' Gaming Tables
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4973), Me.m_gaming_tables)

    ' Movement Type
    If m_movement_type <> "" Then
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(211), m_movement_type)
    End If

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()
    Me.m_date_from = String.Empty
    Me.m_date_to = String.Empty
    Me.m_card_account = String.Empty
    Me.m_card_track_data = String.Empty
    Me.m_card_holder_name = String.Empty
    Me.m_holder_is_vip = String.Empty
    Me.m_gaming_tables = String.Empty
    Me.m_cashier = String.Empty
    Me.m_movement_type = String.Empty

    ' Date 
    If Me.dtp_from.Checked Then
      Me.m_date_from = GUI_FormatDate(Me.dtp_from.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Me.dtp_to.Checked Then
      Me.m_date_to = GUI_FormatDate(Me.dtp_to.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Account
    'DMT 19-JUN-2018
    If Me.uc_account_sel1.Account() = "" Then
      Me.m_card_account = Me.uc_account_sel1.AccountText
    Else
      Me.m_card_account = Me.uc_account_sel1.Account()
    End If

    Me.m_card_track_data = Me.uc_account_sel1.TrackData()
    Me.m_card_holder_name = Me.uc_account_sel1.Holder()
    Me.m_holder_is_vip = Me.uc_account_sel1.HolderIsVip()

    ' Gaming Tables
    If uc_checked_list_tables.SelectedIndexes.Length = uc_checked_list_tables.Count() Then
      Me.m_gaming_tables = GLB_NLS_GUI_PLAYER_TRACKING.GetString(287) ' All
    ElseIf uc_checked_list_tables.SelectedIndexes.Length > 1 Then
      Me.m_gaming_tables = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1808) ' Several
    ElseIf uc_checked_list_tables.SelectedIndexes.Length = 1 Then
      Me.m_gaming_tables = Me.uc_checked_list_tables.SelectedValuesList ' Only one
    End If

    ' Cashier
    If Me.opt_all_terminals.Checked Then
      m_cashier = Me.opt_all_terminals.Text
    End If
    If Me.opt_one_cashier.Checked Then
      m_cashier = Me.cmb_cashier.TextValue
    End If

    'Movement Type Grid
    If Me.opt_all_types.Checked Then
      m_movement_type = Me.opt_all_types.Text
    Else
      Call GetSelectedMovementTypes(m_movement_type)
    End If

  End Sub ' GUI_ReportUpdateFilters 

  ' PURPOSE: Process clicks on data grid (doible-clicks) and select button
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON) ' GUI_ButtonClick
    Select Case ButtonId
      Case Else
        MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub ' GUI_ButtonClick

#End Region

#Region "Private"

  ' PURPOSE: Sets the default date values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()
    Dim _closing_time As Integer
    Dim _initial_time As Date
    Dim _now As Date

    Me.uc_account_sel1.Clear()

    _now = WGDB.Now

    _closing_time = GetDefaultClosingTime()
    _initial_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)
    If _initial_time > _now Then
      _initial_time = _initial_time.AddDays(-1)
    End If

    'FAV 25-JUN-2015
    If Me.bl_sessions Then
      If m_init_date_from <> Nothing Then
        Me.dtp_from.Value = m_init_date_from
        Me.dtp_from.Checked = True
      Else
        Me.dtp_from.Value = _initial_time
        Me.dtp_from.Checked = False
      End If
      If m_init_date_to <> Nothing Then
        Me.dtp_to.Value = m_init_date_to
        Me.dtp_to.Checked = True
      Else
        Me.dtp_to.Value = _initial_time.AddDays(1)
        Me.dtp_to.Checked = False
      End If
    Else
      Me.dtp_from.Value = _initial_time
      Me.dtp_from.Checked = True
      Me.dtp_to.Value = _initial_time.AddDays(1)
      Me.dtp_to.Checked = False
    End If

    Me.opt_all_terminals.Checked = True
    Me.cmb_cashier.Enabled = False

    'Movement Types Filter Data Grid
    Call InitMovementTypesFilter()

    ' Gaming Tables
    uc_checked_list_tables.Clear()
    FillGamingTables()
    'Me.uc_checked_list_tables.SetDefaultValue(True)
    Me.uc_checked_list_tables.CollapseAll()

  End Sub ' SetDefaultValues

  ' PURPOSE: Initialize the movement types filter grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub InitMovementTypesFilter()

    Me.opt_all_types.Checked = True
    Me.dg_filter.Enabled = True

    ' Reset dg selections
    Call FillMovTypesFilterGrid(GetMovTypesFilterGridData())

  End Sub ' InitMovementTypesFilter

  ' PURPOSE: Populate the movement types data grid filter.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub FillMovTypesFilterGrid(ByVal DgList As List(Of TYPE_DG_FILTER_ELEMENT))

    Me.dg_filter.Clear()
    Me.dg_filter.Redraw = False

    ' Get the filters array and insert it into the grid.

    For Each _element As TYPE_DG_FILTER_ELEMENT In DgList

      dg_filter.AddRow()

      dg_filter.Cell(dg_filter.NumRows - 1, GRID_MOVS_FILTER_COL_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
      dg_filter.Cell(dg_filter.NumRows - 1, GRID_MOVS_FILTER_COL_TYPE).Value = _element.type
      dg_filter.Cell(dg_filter.NumRows - 1, GRID_MOVS_FILTER_COL_DESC).Value = _element.description
    Next

    Me.dg_filter.Redraw = True

  End Sub ' FillMovTypesFilterGrid

  ' PURPOSE: Obtain the list of elements that will populate the 
  '         movement types filter grid.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '     - List of elements to be added to the grid
  Private Function GetMovTypesFilterGridData() As List(Of TYPE_DG_FILTER_ELEMENT)

    ' Dimensioned this way to avoid getting an extra element: VB uses the constant to set the upper value, 
    ' not the number of elements
    Dim _dg_filter_rows_list As List(Of TYPE_DG_FILTER_ELEMENT)
    Dim _row As TYPE_DG_FILTER_ELEMENT
    Dim _movement_type As PlayerTrackingMovementType

    _dg_filter_rows_list = New List(Of TYPE_DG_FILTER_ELEMENT)

    For Each _movement_type In System.Enum.GetValues(GetType(PlayerTrackingMovementType))

      _row.type = _movement_type
      _row.description = PlayerTrackingMovements.GetMovementDesc(_movement_type, String.Empty)
      _dg_filter_rows_list.Add(_row)

    Next

    Return _dg_filter_rows_list

  End Function ' GetMovTypesFilterGridData


  ' PURPOSE: Define the layout of the movement types grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheetMovementTypes()
    With Me.dg_filter
      Call .Init(GRID_MOVS_FILTER_COLUMNS, GRID_MOVS_HEADERS_COUNT, True)
      .Counter(0).Visible = False
      .Sortable = False

      ' Hidden columns
      .Column(GRID_MOVS_FILTER_COL_TYPE).Header.Text = ""
      .Column(GRID_MOVS_FILTER_COL_TYPE).WidthFixed = 0

      ' GRID_COL_CHECKBOX
      .Column(GRID_MOVS_FILTER_COL_CHECKED).Header.Text = ""
      .Column(GRID_MOVS_FILTER_COL_CHECKED).WidthFixed = 400
      .Column(GRID_MOVS_FILTER_COL_CHECKED).Fixed = True
      .Column(GRID_MOVS_FILTER_COL_CHECKED).Editable = True
      .Column(GRID_MOVS_FILTER_COL_CHECKED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX

      ' GRID_COL_DESCRIPTION
      .Column(GRID_MOVS_FILTER_COL_DESC).Header.Text = ""
      .Column(GRID_MOVS_FILTER_COL_DESC).WidthFixed = 4000
      .Column(GRID_MOVS_FILTER_COL_DESC).Fixed = True
      .Column(GRID_MOVS_FILTER_COL_DESC).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With

  End Sub 'GUI_StyleSheetMovementTypes

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()

    With Me.Grid
      .Init(GRID_COLUMNS_COUNT, GRID_HEADERS_COUNT, True)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Account Number
      .Column(GRID_COL_ACCOUNT_ID).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COL_ACCOUNT_ID).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COL_ACCOUNT_ID).Width = 1250
      .Column(GRID_COL_ACCOUNT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Account TrackData
      .Column(GRID_COL_ACCOUNT_TRACK_DATA).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COL_ACCOUNT_TRACK_DATA).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(212)
      .Column(GRID_COL_ACCOUNT_TRACK_DATA).Width = 2300
      .Column(GRID_COL_ACCOUNT_TRACK_DATA).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Account Holder Name
      .Column(GRID_COL_ACCOUNT_HOLDER_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COL_ACCOUNT_HOLDER_NAME).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(235)
      .Column(GRID_COL_ACCOUNT_HOLDER_NAME).Width = 2000
      .Column(GRID_COL_ACCOUNT_HOLDER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' User
      .Column(GRID_COL_USER).Header(0).Text = "xUser"
      .Column(GRID_COL_USER).Header(1).Text = "xUser"
      .Column(GRID_COL_USER).Width = 0
      .Column(GRID_COL_USER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Table type
      .Column(GRID_COL_GAME_TABLE_TYPE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4419)
      .Column(GRID_COL_GAME_TABLE_TYPE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(254)
      .Column(GRID_COL_GAME_TABLE_TYPE).Width = 1500
      .Column(GRID_COL_GAME_TABLE_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Table name
      .Column(GRID_COL_GAME_TABLE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4419)
      .Column(GRID_COL_GAME_TABLE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4979)
      .Column(GRID_COL_GAME_TABLE).Width = 1500
      .Column(GRID_COL_GAME_TABLE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Attendant
      .Column(GRID_COL_ATTENDANT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4419)
      .Column(GRID_COL_ATTENDANT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4974)
      .Column(GRID_COL_ATTENDANT).Width = 1500
      .Column(GRID_COL_ATTENDANT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Seat
      .Column(GRID_COL_SEAT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4419)
      .Column(GRID_COL_SEAT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4975)
      .Column(GRID_COL_SEAT).Width = 750
      .Column(GRID_COL_SEAT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Datetime
      .Column(GRID_COL_DATETIME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2466)
      .Column(GRID_COL_DATETIME).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(201)
      .Column(GRID_COL_DATETIME).Width = 2000
      .Column(GRID_COL_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Desc
      .Column(GRID_COL_DESC).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2466)
      .Column(GRID_COL_DESC).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(236)
      .Column(GRID_COL_DESC).Width = 3700
      .Column(GRID_COL_DESC).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Cashier
      .Column(GRID_COL_CASHIER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2466)
      .Column(GRID_COL_CASHIER).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(210)
      .Column(GRID_COL_CASHIER).Width = 2000
      .Column(GRID_COL_CASHIER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Old Value
      .Column(GRID_COL_OLD_VALUE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2466)
      .Column(GRID_COL_OLD_VALUE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4794)
      .Column(GRID_COL_OLD_VALUE).Width = 1000
      .Column(GRID_COL_OLD_VALUE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Value
      .Column(GRID_COL_VALUE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2466)
      .Column(GRID_COL_VALUE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4795)
      .Column(GRID_COL_VALUE).Width = 1000
      .Column(GRID_COL_VALUE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Terminal
      .Column(GRID_COL_TERMINAL_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1097)
      .Column(GRID_COL_TERMINAL_ID).Header(1).Text = " "
      .Column(GRID_COL_TERMINAL_ID).Width = 0
      .Column(GRID_COL_TERMINAL_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Play Session
      .Column(GRID_COL_PLAY_SESSION).Header(0).Text = "xPlaySession"
      .Column(GRID_COL_PLAY_SESSION).Header(1).Text = "xPlaySession"
      .Column(GRID_COL_PLAY_SESSION).Width = 0
      .Column(GRID_COL_PLAY_SESSION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

  End Sub ' GUI_StyleSheet  

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  '
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _sb As System.Text.StringBuilder

    _sb = New System.Text.StringBuilder()

    ' Table
    _sb.AppendLine("   SELECT   GTPM_DATETIME                             ")
    _sb.AppendLine("          , GTPM_USER_ID                              ")
    _sb.AppendLine("          , GTT_NAME                                  ")
    _sb.AppendLine("          , GT_NAME                                   ")
    _sb.AppendLine("          , GU_USERNAME AS ATTENDANT                  ")
    _sb.AppendLine("          , ISNULL(GTS_POSITION, GTPM_SEAT_ID) AS GTPM_SEAT_ID ")
    _sb.AppendLine("          , CT_NAME                                   ")
    _sb.AppendLine("          , TE_NAME                                   ")
    _sb.AppendLine("          , GTPM_PLAY_SESSION_ID                      ")
    _sb.AppendLine("          , GTPM_ACCOUNT_ID                           ")
    _sb.AppendLine("          , AC_HOLDER_NAME                            ")
    _sb.AppendLine("          , AC_TYPE                                   ")
    _sb.AppendLine("          , AC_TRACK_DATA                             ")
    _sb.AppendLine("          , GTPM_OLD_VALUE                            ")
    _sb.AppendLine("          , GTPM_VALUE                                ")
    _sb.AppendLine("          , GTPM_TYPE                                 ")
    _sb.AppendLine("          , ISNULL(GTPM_ISO_CODE, '')                 ")
    _sb.AppendLine("     FROM   GT_PLAYERTRACKING_MOVEMENTS               ")
    _sb.AppendLine("LEFT JOIN   ACCOUNTS                                  ")
    _sb.AppendLine("       ON   GTPM_ACCOUNT_ID = AC_ACCOUNT_ID           ")
    _sb.AppendLine("LEFT JOIN   GAMING_TABLES                             ")
    _sb.AppendLine("       ON   GTPM_GAMING_TABLE_ID = GT_GAMING_TABLE_ID ")
    _sb.AppendLine("LEFT JOIN   GAMING_TABLES_TYPES                       ")
    _sb.AppendLine("       ON   GT_TYPE_ID = GTT_GAMING_TABLE_TYPE_ID     ")
    _sb.AppendLine("LEFT JOIN   GUI_USERS                                 ")
    _sb.AppendLine("       ON   GU_USER_ID = GTPM_USER_ID                 ")
    _sb.AppendLine("LEFT JOIN   TERMINALS                                 ")
    _sb.AppendLine("       ON   TE_TERMINAL_ID = GTPM_TERMINAL_ID         ")
    _sb.AppendLine("LEFT JOIN   CASHIER_TERMINALS                         ")
    _sb.AppendLine("       ON   CT_CASHIER_ID = GTPM_CASHIER_ID           ")
    _sb.AppendLine("LEFT JOIN   GT_SEATS                                  ")
    _sb.AppendLine("       ON   GTS_SEAT_ID = GTPM_SEAT_ID                ")

    _sb.AppendLine(GetSqlWhere())

    _sb.AppendLine("ORDER BY   GTPM_DATETIME DESC       ")

    Return _sb.ToString()
  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the main SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String

    Dim _result As String
    Dim _sb_where As StringBuilder
    Dim _account_filter As String
    Dim _pattern As String
    Dim _rgx As Regex
    Dim idx_row As Integer
    Dim _list_mov_types As String

    _sb_where = New System.Text.StringBuilder()

    If Me.bl_sessions = True Then

      _sb_where.AppendLine(" WHERE GTPM_PLAY_SESSION_ID = " & m_session_id)

    End If

    If m_table_session_id <> 0 Then

      _sb_where.AppendLine(" WHERE GTPM_GAMING_TABLE_SESSION_ID = " & m_table_session_id)

      'Return _sb_where.ToString()
    End If

    'Account filter
    _account_filter = Me.uc_account_sel1.GetFilterSQL()
    If _account_filter <> String.Empty Then
      _sb_where.AppendLine("   AND   " & _account_filter)
    End If

    ' Date filter
    If Me.dtp_from.Checked = True Then
      _sb_where.AppendLine("   AND   (GTPM_DATETIME >= " & GUI_FormatDateDB(dtp_from.Value) & ") ")
    End If

    If Me.dtp_to.Checked = True Then
      _sb_where.AppendLine("   AND   (GTPM_DATETIME < " & GUI_FormatDateDB(dtp_to.Value) & ") ")
    End If

    ' Filter Cashier
    If Me.opt_one_cashier.Checked = True Then
      _sb_where.AppendLine(" AND (GTPM_CASHIER_ID = " & Me.cmb_cashier.Value & ") ")
    End If

    ' Gaming table filter
    If Me.uc_checked_list_tables.SelectedIndexes.Length <> Me.uc_checked_list_tables.Count() Then
      _sb_where.AppendLine("   AND   GTPM_GAMING_TABLE_ID IN (" & Me.uc_checked_list_tables.SelectedIndexesListLevel2 & ") ")
    End If

    ' Movement type
    _list_mov_types = ""

    If opt_several_types.Checked Then
      For idx_row = 0 To Me.dg_filter.NumRows - 1
        If dg_filter.Cell(idx_row, GRID_MOVS_FILTER_COL_CHECKED).Value = uc_grid.GRID_CHK_CHECKED And dg_filter.Cell(idx_row, GRID_MOVS_FILTER_COL_CHECKED).Value <> "" Then
          If _list_mov_types.Length = 0 Then
            _list_mov_types = dg_filter.Cell(idx_row, GRID_MOVS_FILTER_COL_TYPE).Value
          Else
            _list_mov_types = _list_mov_types & ", " & dg_filter.Cell(idx_row, GRID_MOVS_FILTER_COL_TYPE).Value
          End If
        End If
      Next

      If _list_mov_types.Length > 0 Then
        _sb_where.AppendLine(" AND GTPM_TYPE IN (" + _list_mov_types + ")")
      End If
    End If

    ' "AND" first occurrence is replaced by "WHERE"
    _result = _sb_where.ToString()

    If _result <> String.Empty Then
      _pattern = "^\s*AND\b"
      _rgx = New Regex(_pattern, RegexOptions.IgnoreCase)
      _result = _rgx.Replace(_result, " WHERE")
    End If

    Return _result

  End Function ' GetSqlWhere


  ' PURPOSE: Update the checkbox state for all row dependants (for header rows)
  '
  '  PARAMS:
  '     - INPUT:
  '           - FirstRow
  '           - ValueChecked
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ChangeCheckofRows(ByVal FirstRow As Integer, ByVal ValueChecked As String)
    Dim idx As Integer

    For idx = FirstRow + 1 To dg_filter.NumRows - 1
      Me.dg_filter.Cell(idx, GRID_MOVS_FILTER_COL_CHECKED).Value = ValueChecked
    Next

  End Sub ' ChangeCheckofRows

  ' PURPOSE: Fill uc_checked_list control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub FillGamingTables()

    Me.uc_checked_list_tables.SetLevels = 2
    Me.uc_checked_list_tables.ColumnWidth(uc_checked_list.GRID_COLUMN_DESC, 4000)

    ' Fill CheckedList control with groups
    Me.uc_checked_list_tables.Clear()
    Me.uc_checked_list_tables.ReDraw(False)

    For Each _group As GamingTablesGroup In Me.m_all_gaming_tables
      uc_checked_list_tables.Add(1, _group.Id, _group.Type)
      For Each _gaming_table As GamingTable In _group.GamingTables
        If (Me.m_table_name <> _gaming_table.Name) Then
          uc_checked_list_tables.Add(2, _gaming_table.Id, "  " + _gaming_table.Name)
        Else
          uc_checked_list_tables.Add(2, _gaming_table.Id, "  " + _gaming_table.Name, True)
        End If
      Next
    Next

    If Me.uc_checked_list_tables.Count > 0 Then
      Me.uc_checked_list_tables.CurrentRow(0)
      Me.uc_checked_list_tables.ReDraw(True)
      Me.uc_checked_list_tables.CollapseAll()
      If (Me.m_table_session_id = 0) Then
        Me.uc_checked_list_tables.SetDefaultValue(False)
      End If
    End If

    Me.uc_checked_list_tables.ResizeGrid()

  End Sub ' FillGroups

  ' PURPOSE: Get all gaming tables
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: Gaming tables list, order by table type.
  '
  Private Function LoadGamingTablesByType() As List(Of GamingTablesGroup)
    Dim _tables_groups As List(Of GamingTablesGroup)
    Dim _str_sql As StringBuilder
    Dim _dt_counters As DataTable
    Dim _group_id As Integer
    Dim _new_gaming_table As GamingTable
    Dim _new_tables_group As GamingTablesGroup

    _tables_groups = New List(Of GamingTablesGroup)
    _new_tables_group = New GamingTablesGroup()
    _str_sql = New StringBuilder()
    _dt_counters = New DataTable()
    _group_id = 0

    Try

      Using _db_trx As DB_TRX = New DB_TRX()

        _str_sql.AppendLine("     SELECT   GTT_GAMING_TABLE_TYPE_ID              ")
        _str_sql.AppendLine("            , GTT_NAME                              ")
        _str_sql.AppendLine("            , GTT_ENABLED                           ")
        _str_sql.AppendLine("            , GT_GAMING_TABLE_ID                    ")
        _str_sql.AppendLine("            , GT_NAME                               ")
        _str_sql.AppendLine("       FROM   GAMING_TABLES_TYPES                   ")
        _str_sql.AppendLine(" INNER JOIN   GAMING_TABLES                         ")
        _str_sql.AppendLine("         ON   GTT_GAMING_TABLE_TYPE_ID = GT_TYPE_ID ")

        Using _sql_cmd As SqlCommand = New SqlCommand(_str_sql.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          Using _sql_adapter As SqlDataAdapter = New SqlDataAdapter(_sql_cmd)

            If _sql_adapter.Fill(_dt_counters) > 0 Then

              For Each _row As DataRow In _dt_counters.Rows

                If _group_id <> CInt(_row(0)) Then
                  _group_id = _row(0) ' GroupId
                  _new_tables_group = New GamingTablesGroup()
                  _new_tables_group.GamingTables = New List(Of GamingTable)
                  _new_tables_group.Id = _group_id
                  _new_tables_group.Type = _row(1)
                  _new_tables_group.Enabled = _row(2)
                  _tables_groups.Add(_new_tables_group)
                End If

                _new_gaming_table = New GamingTable()
                _new_gaming_table.Id = _row(3)
                _new_gaming_table.Name = _row(4)
                _tables_groups(_tables_groups.Count - 1).GamingTables.Add(_new_gaming_table)

              Next
            End If
          End Using
        End Using
      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return _tables_groups
  End Function ' LoadGamingTablesByType

  ' PURPOSE: Returns a string with the description of all the selected movement types
  '
  '  PARAMS:
  '     - INPUT:
  '           - 
  '     - OUTPUT:
  '           - SelectedMovements
  '
  ' RETURNS:
  '     - None
  Private Sub GetSelectedMovementTypes(ByRef SelectedMovements As String)
    Dim idx As Integer

    SelectedMovements = ""

    For idx = 0 To dg_filter.NumRows - 1
      If Me.dg_filter.Cell(idx, GRID_MOVS_FILTER_COL_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        If SelectedMovements = "" Then
          SelectedMovements = Me.dg_filter.Cell(idx, GRID_MOVS_FILTER_COL_DESC).Value.Trim()
        Else
          SelectedMovements &= ", " & Me.dg_filter.Cell(idx, GRID_MOVS_FILTER_COL_DESC).Value.Trim()
        End If
      End If
    Next
  End Sub

  ' PURPOSE: Returns a string with the description of player skill
  '
  '  PARAMS:
  '     - INPUT:
  '           - 
  '     - OUTPUT:
  '           - Description
  '
  ' RETURNS:
  '     - None
  Private Function GetSkillDescription(ByVal SkillId As Integer) As String
    Dim _description As String

    _description = ""

    Select Case (SkillId)
      Case WSI.Common.GTPlaySessionPlayerSkill.Soft
        _description = Resource.String("STR_FRM_PLAYER_TRACKING_CB_SOFT")
      Case WSI.Common.GTPlaySessionPlayerSkill.Average
        _description = Resource.String("STR_FRM_PLAYER_TRACKING_CB_AVERAGE")
      Case WSI.Common.GTPlaySessionPlayerSkill.Hard
        _description = Resource.String("STR_FRM_PLAYER_TRACKING_CB_HARD")
    End Select

    Return _description

  End Function

  ' PURPOSE: Returns a string with the description of player skill
  '
  '  PARAMS:
  '     - INPUT:
  '           - 
  '     - OUTPUT:
  '           - Description
  '
  ' RETURNS:
  '     - None
  Private Function GetSpeedDescription(ByVal SpeedId As Integer) As String
    Dim _description As String

    _description = ""

    Select Case (SpeedId)
      Case WSI.Common.GTPlayerTrackingSpeed.Slow
        _description = Resource.String("STR_FRM_PLAYER_TRACKING_CB_SLOW")
      Case WSI.Common.GTPlayerTrackingSpeed.Medium
        _description = Resource.String("STR_FRM_PLAYER_TRACKING_CB_MEDIUM")
      Case WSI.Common.GTPlayerTrackingSpeed.Fast
        _description = Resource.String("STR_FRM_PLAYER_TRACKING_CB_FAST")
    End Select

    Return _description

  End Function

  ' PURPOSE: Return a Int64 with the accountId of session
  '
  '  PARAMS:
  '     - INPUT:
  '           - SessionId:
  '
  '     - OUTPUT:
  '           - AccountId
  '
  ' RETURNS:
  '     - none
  Private Function GetAccountIdFromSession(ByVal SessionId As Int64) As Int64
    Dim _str_sql As StringBuilder
    Dim _obj As Object
    Dim _accountId As Int64

    _str_sql = New StringBuilder()
    _accountId = 0

    Try
      _str_sql.AppendLine("     SELECT   GTPS_ACCOUNT_ID                     ")
      _str_sql.AppendLine("     FROM   GT_PLAY_SESSIONS                      ")
      _str_sql.AppendLine("     WHERE GTPS_PLAY_SESSION_ID = @PSESSIONID     ")

      Using _db_trx As New WSI.Common.DB_TRX()
        Using _sql_command As New SqlClient.SqlCommand(_str_sql.ToString, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _sql_command.Parameters.Add("@PSESSIONID", SqlDbType.BigInt).Value = SessionId

          _obj = _sql_command.ExecuteScalar()

          If _obj IsNot Nothing AndAlso _obj IsNot DBNull.Value Then
            _accountId = _obj
          End If

        End Using
      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return _accountId
  End Function

#End Region

#Region "Public"

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    bl_sessions = False
    m_init_date_from = Nothing
    m_init_date_to = Nothing
    Me.MdiParent = MdiParent
    Me.m_session_id = 0
    Me.Display(False)

  End Sub ' ShowForEdit

  ' PURPOSE: Opens dialog with default settings for edit mode with filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - SessionId:
  '           - DateFrom: Movements date from
  '           - DateTo: Movements date to
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowSessionID(ByVal MdiParent As System.Windows.Forms.IWin32Window, _
                         ByVal SessionId As Integer, _
                         Optional ByVal DateFrom As Date = Nothing, _
                         Optional ByVal DateTo As Date = Nothing)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING

    bl_sessions = True
    m_init_date_from = DateFrom
    m_init_date_to = DateTo

    Me.MdiParent = MdiParent
    Me.m_session_id = SessionId
    Me.m_account_id = Me.GetAccountIdFromSession(m_session_id)
    Me.Display(False)

    ' For Show window before filter apply 
    Call Application.DoEvents()
    System.Threading.Thread.Sleep(100)
    Call Application.DoEvents()

    GUI_ButtonClick(GUI_Controls.frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY)

  End Sub

  ' PURPOSE: Opens dialog and apply filters for show only chips movements for the parameter gaming table session
  '
  '  PARAMS:
  '     - INPUT:
  '           - Gaming Table Sesion
  '           - Gaming Table Name
  '           - Parent Forms
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowMovementsChips(ByVal TableSessionId As Int64, ByVal TableName As String, ByVal MdiParent As System.Windows.Forms.IWin32Window)
    Dim _row As TYPE_DG_FILTER_ELEMENT
    Dim _grid_move_types As List(Of TYPE_DG_FILTER_ELEMENT)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.m_table_session_id = TableSessionId
    Me.m_table_name = TableName
    Me.Display(False)

    Me.dtp_from.Checked = False

    opt_several_types.Checked = True

    _grid_move_types = GetMovTypesFilterGridData()

    ' Chips IN
    _row.type = PlayerTrackingMovementType.ChipsIn
    _row.description = PlayerTrackingMovements.GetMovementDesc(PlayerTrackingMovementType.ChipsIn, String.Empty)
    Me.dg_filter.Cell(_grid_move_types.LastIndexOf(_row), GRID_MOVS_FILTER_COL_CHECKED).Value = uc_grid.GRID_CHK_CHECKED

    ' Chips Out
    _row.type = PlayerTrackingMovementType.ChipsOut
    _row.description = PlayerTrackingMovements.GetMovementDesc(PlayerTrackingMovementType.ChipsOut, String.Empty)
    Me.dg_filter.Cell(_grid_move_types.LastIndexOf(_row), GRID_MOVS_FILTER_COL_CHECKED).Value = uc_grid.GRID_CHK_CHECKED

    ' Toal buy in
    _row.type = PlayerTrackingMovementType.SaleChips
    _row.description = PlayerTrackingMovements.GetMovementDesc(PlayerTrackingMovementType.SaleChips, String.Empty)
    Me.dg_filter.Cell(_grid_move_types.LastIndexOf(_row), GRID_MOVS_FILTER_COL_CHECKED).Value = uc_grid.GRID_CHK_CHECKED

    GUI_ButtonClick(GUI_Controls.frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY)

    Me.m_table_session_id = 0
    Me.m_table_name = String.Empty

  End Sub ' ShowMovementsChips

#End Region

#Region "Events"

  Private Sub opt_one_cashier_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_one_cashier.Click
    Me.cmb_cashier.Enabled = True
  End Sub ' opt_one_cashier_Click

  Private Sub opt_one_terminal_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Me.cmb_cashier.Enabled = False
  End Sub ' opt_one_terminal_Click

  Private Sub dg_types_DataSelectedEvent() Handles dg_filter.DataSelectedEvent
    Me.opt_several_types.Checked = True
  End Sub ' dg_types_DataSelectedEvent

  Private Sub dg_types_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer) Handles dg_filter.CellDataChangedEvent
    Me.opt_several_types.Checked = True
  End Sub ' dg_types_CellDataChangedEvent

  Private Sub opt_several_types_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_several_types.CheckedChanged
    If opt_several_types.Checked Then
      Me.opt_all_types.Checked = False
    End If

  End Sub ' opt_several_types_CheckedChanged

  Private Sub opt_all_types_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_all_types.CheckedChanged
    Dim idx As Integer

    If opt_all_types.Checked Then
      Me.dg_filter.Redraw = False

      For idx = 0 To dg_filter.NumRows - 1
        Me.dg_filter.Cell(idx, GRID_MOVS_FILTER_COL_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
      Next

      Me.dg_filter.Redraw = True

      opt_several_types.Checked = False
    End If

  End Sub ' opt_all_types_CheckedChanged

#End Region

End Class