<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_stats_manual_adjustment
  Inherits GUI_Controls.frm_base_print

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.uc_pr_list = New GUI_Controls.uc_provider()
    Me.dg_games = New GUI_Controls.uc_grid()
    Me.dg_hour = New GUI_Controls.uc_grid()
    Me.dg_days = New GUI_Controls.uc_grid()
    Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
    Me.uc_terminal_name = New GUI_Controls.uc_text_field()
    Me.lbl_NetWin1 = New System.Windows.Forms.Label()
    Me.lbl_NetWin2 = New System.Windows.Forms.Label()
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector()
    Me.panel_grids.SuspendLayout()
    Me.panel_filter.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.TableLayoutPanel1.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_grids
    '
    Me.panel_grids.Controls.Add(Me.TableLayoutPanel1)
    Me.panel_grids.Location = New System.Drawing.Point(5, 226)
    Me.panel_grids.Size = New System.Drawing.Size(1222, 478)
    Me.panel_grids.Controls.SetChildIndex(Me.TableLayoutPanel1, 0)
    Me.panel_grids.Controls.SetChildIndex(Me.panel_buttons, 0)
    '
    'panel_buttons
    '
    Me.panel_buttons.Location = New System.Drawing.Point(1131, 0)
    Me.panel_buttons.Size = New System.Drawing.Size(91, 478)
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.uc_dsl)
    Me.panel_filter.Controls.Add(Me.lbl_NetWin2)
    Me.panel_filter.Controls.Add(Me.lbl_NetWin1)
    Me.panel_filter.Controls.Add(Me.uc_terminal_name)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1222, 212)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_terminal_name, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_NetWin1, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_NetWin2, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dsl, 0)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Location = New System.Drawing.Point(5, 216)
    Me.pn_separator_line.Size = New System.Drawing.Size(1222, 10)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1222, 10)
    '
    'uc_pr_list
    '
    Me.uc_pr_list.FilterByCurrency = False
    Me.uc_pr_list.Location = New System.Drawing.Point(6, 6)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(249, 192)
    Me.uc_pr_list.TabIndex = 0
    Me.uc_pr_list.TerminalListHasValues = False
    '
    'dg_games
    '
    Me.dg_games.AutoSize = True
    Me.dg_games.CurrentCol = -1
    Me.dg_games.CurrentRow = -1
    Me.dg_games.Dock = System.Windows.Forms.DockStyle.Fill
    Me.dg_games.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_games.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_games.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_games.Location = New System.Drawing.Point(0, 336)
    Me.dg_games.Margin = New System.Windows.Forms.Padding(0)
    Me.dg_games.Name = "dg_games"
    Me.dg_games.PanelRightVisible = True
    Me.dg_games.Redraw = True
    Me.dg_games.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    Me.dg_games.Size = New System.Drawing.Size(1116, 142)
    Me.dg_games.Sortable = False
    Me.dg_games.SortAscending = True
    Me.dg_games.SortByCol = 0
    Me.dg_games.TabIndex = 12
    Me.dg_games.ToolTipped = True
    Me.dg_games.TopRow = -2
    Me.dg_games.WordWrap = False
    '
    'dg_hour
    '
    Me.dg_hour.AutoSize = True
    Me.dg_hour.CurrentCol = -1
    Me.dg_hour.CurrentRow = -1
    Me.dg_hour.Dock = System.Windows.Forms.DockStyle.Fill
    Me.dg_hour.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_hour.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_hour.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_hour.Location = New System.Drawing.Point(0, 100)
    Me.dg_hour.Margin = New System.Windows.Forms.Padding(0)
    Me.dg_hour.Name = "dg_hour"
    Me.dg_hour.PanelRightVisible = True
    Me.dg_hour.Redraw = True
    Me.dg_hour.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    Me.dg_hour.Size = New System.Drawing.Size(1116, 236)
    Me.dg_hour.Sortable = False
    Me.dg_hour.SortAscending = True
    Me.dg_hour.SortByCol = 0
    Me.dg_hour.TabIndex = 11
    Me.dg_hour.ToolTipped = True
    Me.dg_hour.TopRow = -2
    Me.dg_hour.WordWrap = False
    '
    'dg_days
    '
    Me.dg_days.AutoSize = True
    Me.dg_days.CurrentCol = -1
    Me.dg_days.CurrentRow = -1
    Me.dg_days.Dock = System.Windows.Forms.DockStyle.Fill
    Me.dg_days.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_days.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_days.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_days.Location = New System.Drawing.Point(0, 0)
    Me.dg_days.Margin = New System.Windows.Forms.Padding(0)
    Me.dg_days.Name = "dg_days"
    Me.dg_days.PanelRightVisible = True
    Me.dg_days.Redraw = True
    Me.dg_days.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    Me.dg_days.Size = New System.Drawing.Size(1116, 100)
    Me.dg_days.Sortable = False
    Me.dg_days.SortAscending = True
    Me.dg_days.SortByCol = 0
    Me.dg_days.TabIndex = 10
    Me.dg_days.ToolTipped = True
    Me.dg_days.TopRow = -2
    Me.dg_days.WordWrap = False
    '
    'TableLayoutPanel1
    '
    Me.TableLayoutPanel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.TableLayoutPanel1.ColumnCount = 1
    Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
    Me.TableLayoutPanel1.Controls.Add(Me.dg_days, 0, 0)
    Me.TableLayoutPanel1.Controls.Add(Me.dg_hour, 0, 1)
    Me.TableLayoutPanel1.Controls.Add(Me.dg_games, 0, 2)
    Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
    Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(0)
    Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
    Me.TableLayoutPanel1.RowCount = 3
    Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
    Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 62.5!))
    Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 37.5!))
    Me.TableLayoutPanel1.Size = New System.Drawing.Size(1116, 478)
    Me.TableLayoutPanel1.TabIndex = 13
    '
    'uc_terminal_name
    '
    Me.uc_terminal_name.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.uc_terminal_name.IsReadOnly = True
    Me.uc_terminal_name.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.uc_terminal_name.LabelForeColor = System.Drawing.Color.Blue
    Me.uc_terminal_name.Location = New System.Drawing.Point(3, 187)
    Me.uc_terminal_name.Name = "uc_terminal_name"
    Me.uc_terminal_name.Size = New System.Drawing.Size(460, 24)
    Me.uc_terminal_name.SufixText = "Sufix Text"
    Me.uc_terminal_name.SufixTextVisible = True
    Me.uc_terminal_name.TabIndex = 2
    Me.uc_terminal_name.TextWidth = 130
    '
    'lbl_NetWin1
    '
    Me.lbl_NetWin1.Anchor = System.Windows.Forms.AnchorStyles.Right
    Me.lbl_NetWin1.AutoSize = True
    Me.lbl_NetWin1.Location = New System.Drawing.Point(865, 170)
    Me.lbl_NetWin1.Name = "lbl_NetWin1"
    Me.lbl_NetWin1.Size = New System.Drawing.Size(260, 13)
    Me.lbl_NetWin1.TabIndex = 3
    Me.lbl_NetWin1.Text = "x* Netwin1 = Monto Jugado - Monto Ganado"
    '
    'lbl_NetWin2
    '
    Me.lbl_NetWin2.Anchor = System.Windows.Forms.AnchorStyles.Right
    Me.lbl_NetWin2.AutoSize = True
    Me.lbl_NetWin2.Location = New System.Drawing.Point(865, 187)
    Me.lbl_NetWin2.Name = "lbl_NetWin2"
    Me.lbl_NetWin2.Size = New System.Drawing.Size(260, 13)
    Me.lbl_NetWin2.TabIndex = 4
    Me.lbl_NetWin2.Text = "x* Netwin2 = Monto Jugado - Monto Ganado"
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.ClosingTimeEnabled = True
    Me.uc_dsl.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.Location = New System.Drawing.Point(261, 9)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.ShowBorder = True
    Me.uc_dsl.Size = New System.Drawing.Size(264, 84)
    Me.uc_dsl.TabIndex = 1
    Me.uc_dsl.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    '
    'frm_stats_manual_adjustment
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1232, 708)
    Me.MinimumSize = New System.Drawing.Size(1010, 710)
    Me.Name = "frm_stats_manual_adjustment"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_stats_manual_adjustment"
    Me.panel_grids.ResumeLayout(False)
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.pn_separator_line.ResumeLayout(False)
    Me.TableLayoutPanel1.ResumeLayout(False)
    Me.TableLayoutPanel1.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents dg_games As GUI_Controls.uc_grid
  Friend WithEvents dg_hour As GUI_Controls.uc_grid
  Friend WithEvents dg_days As GUI_Controls.uc_grid
  Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
  Friend WithEvents uc_terminal_name As GUI_Controls.uc_text_field
  Friend WithEvents lbl_NetWin2 As System.Windows.Forms.Label
  Friend WithEvents lbl_NetWin1 As System.Windows.Forms.Label
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector
End Class
