'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_Auditor.vb
' DESCRIPTION:   Interface for filter audit information
' AUTHOR:        Carlos A. Costa
' CREATION DATE: 25-03-2002
'
' REVISION HISTORY:
'
' Date          Author Description
' -----------  ------- ----------------------------------------------
' 25-03-2002   CAC    Initial version
' 13-AUG-2002  JSV    SU can see all users (under his level and in his own level): Change SELECT sentences.
' 30-DEC-2011  MPO    Exclude disabled users (combo)
' 27-FEB-2012  MPO    Defect #169: Change format date of column "Date" and control date_picker controls. From 12h to 24h.
' 16-AUG-2012  XCD    Added show unsubscribed to filter users
' 12-ABR-2013  RBG    Added new column: "Machine"
' 16-APR-2013  DMR    Changing cashier name for its alias
' 25-MAR-2014  JFC    Fixed bug WIG-445: filter GUI must show only relevant values.
' 20-JAN-2015  AMF    Fixed bug WIG-1948: Check if related columns exists
' 26-MAR-2015  ANM    Calling GetProviderIdListSelected always returns a query
'--------------------------------------------------------------------
Option Strict Off
Option Explicit On

Imports System.Data.OleDb
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common


Public Class frm_auditor
  Inherits frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents fra_gui As System.Windows.Forms.GroupBox
  Friend WithEvents fra_audit_code As System.Windows.Forms.GroupBox
  Friend WithEvents cmb_gui As uc_combo
  Friend WithEvents cmb_user As uc_combo
  Friend WithEvents cmb_audit_code As uc_combo
  Friend WithEvents fra_user As System.Windows.Forms.GroupBox
  Friend WithEvents opt_gui_one As System.Windows.Forms.RadioButton
  Friend WithEvents opt_user_one As System.Windows.Forms.RadioButton
  Friend WithEvents opt_audit_code_one As System.Windows.Forms.RadioButton
  Friend WithEvents opt_audit_code_all As System.Windows.Forms.RadioButton
  Friend WithEvents opt_gui_all As System.Windows.Forms.RadioButton
  Friend WithEvents opt_user_all As System.Windows.Forms.RadioButton
  Friend WithEvents fra_date As System.Windows.Forms.GroupBox
  Friend WithEvents dt_date_to As GUI_Controls.uc_date_picker
  Friend WithEvents chk_show_all As System.Windows.Forms.CheckBox
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents dt_date_from As GUI_Controls.uc_date_picker
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_auditor))
    Me.fra_gui = New System.Windows.Forms.GroupBox()
    Me.cmb_gui = New GUI_Controls.uc_combo()
    Me.opt_gui_all = New System.Windows.Forms.RadioButton()
    Me.opt_gui_one = New System.Windows.Forms.RadioButton()
    Me.fra_user = New System.Windows.Forms.GroupBox()
    Me.chk_show_all = New System.Windows.Forms.CheckBox()
    Me.cmb_user = New GUI_Controls.uc_combo()
    Me.opt_user_all = New System.Windows.Forms.RadioButton()
    Me.opt_user_one = New System.Windows.Forms.RadioButton()
    Me.fra_audit_code = New System.Windows.Forms.GroupBox()
    Me.cmb_audit_code = New GUI_Controls.uc_combo()
    Me.opt_audit_code_all = New System.Windows.Forms.RadioButton()
    Me.opt_audit_code_one = New System.Windows.Forms.RadioButton()
    Me.fra_date = New System.Windows.Forms.GroupBox()
    Me.dt_date_to = New GUI_Controls.uc_date_picker()
    Me.dt_date_from = New GUI_Controls.uc_date_picker()
    Me.uc_pr_list = New GUI_Controls.uc_provider()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.fra_gui.SuspendLayout()
    Me.fra_user.SuspendLayout()
    Me.fra_audit_code.SuspendLayout()
    Me.fra_date.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Controls.Add(Me.fra_date)
    Me.panel_filter.Controls.Add(Me.fra_audit_code)
    Me.panel_filter.Controls.Add(Me.fra_user)
    Me.panel_filter.Controls.Add(Me.fra_gui)
    Me.panel_filter.Size = New System.Drawing.Size(1227, 187)
    Me.panel_filter.Controls.SetChildIndex(Me.fra_gui, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.fra_user, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.fra_audit_code, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.fra_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 191)
    Me.panel_data.Size = New System.Drawing.Size(1227, 449)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1221, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1221, 4)
    '
    'fra_gui
    '
    Me.fra_gui.Controls.Add(Me.cmb_gui)
    Me.fra_gui.Controls.Add(Me.opt_gui_all)
    Me.fra_gui.Controls.Add(Me.opt_gui_one)
    Me.fra_gui.Location = New System.Drawing.Point(10, 100)
    Me.fra_gui.Name = "fra_gui"
    Me.fra_gui.Size = New System.Drawing.Size(248, 80)
    Me.fra_gui.TabIndex = 1
    Me.fra_gui.TabStop = False
    Me.fra_gui.Text = "fra_gui"
    '
    'cmb_gui
    '
    Me.cmb_gui.AllowUnlistedValues = False
    Me.cmb_gui.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.cmb_gui.AutoCompleteMode = False
    Me.cmb_gui.IsReadOnly = False
    Me.cmb_gui.Location = New System.Drawing.Point(72, 18)
    Me.cmb_gui.Name = "cmb_gui"
    Me.cmb_gui.SelectedIndex = -1
    Me.cmb_gui.Size = New System.Drawing.Size(170, 25)
    Me.cmb_gui.SufixText = "Sufix Text"
    Me.cmb_gui.SufixTextVisible = True
    Me.cmb_gui.TabIndex = 1
    Me.cmb_gui.TextCombo = Nothing
    Me.cmb_gui.TextVisible = False
    Me.cmb_gui.TextWidth = 0
    '
    'opt_gui_all
    '
    Me.opt_gui_all.Location = New System.Drawing.Point(8, 47)
    Me.opt_gui_all.Name = "opt_gui_all"
    Me.opt_gui_all.Size = New System.Drawing.Size(59, 24)
    Me.opt_gui_all.TabIndex = 2
    Me.opt_gui_all.Text = "opt_gui_all"
    '
    'opt_gui_one
    '
    Me.opt_gui_one.Location = New System.Drawing.Point(8, 19)
    Me.opt_gui_one.Name = "opt_gui_one"
    Me.opt_gui_one.Size = New System.Drawing.Size(59, 24)
    Me.opt_gui_one.TabIndex = 0
    Me.opt_gui_one.Text = "opt_gui_one"
    '
    'fra_user
    '
    Me.fra_user.Controls.Add(Me.chk_show_all)
    Me.fra_user.Controls.Add(Me.cmb_user)
    Me.fra_user.Controls.Add(Me.opt_user_all)
    Me.fra_user.Controls.Add(Me.opt_user_one)
    Me.fra_user.Location = New System.Drawing.Point(264, 6)
    Me.fra_user.Name = "fra_user"
    Me.fra_user.Size = New System.Drawing.Size(277, 80)
    Me.fra_user.TabIndex = 2
    Me.fra_user.TabStop = False
    Me.fra_user.Text = "fra_user"
    '
    'chk_show_all
    '
    Me.chk_show_all.AutoSize = True
    Me.chk_show_all.Location = New System.Drawing.Point(73, 50)
    Me.chk_show_all.Name = "chk_show_all"
    Me.chk_show_all.Size = New System.Drawing.Size(102, 17)
    Me.chk_show_all.TabIndex = 3
    Me.chk_show_all.Text = "chk_show_all"
    Me.chk_show_all.UseVisualStyleBackColor = True
    '
    'cmb_user
    '
    Me.cmb_user.AllowUnlistedValues = False
    Me.cmb_user.AutoCompleteMode = False
    Me.cmb_user.IsReadOnly = False
    Me.cmb_user.Location = New System.Drawing.Point(70, 18)
    Me.cmb_user.Name = "cmb_user"
    Me.cmb_user.SelectedIndex = -1
    Me.cmb_user.Size = New System.Drawing.Size(201, 24)
    Me.cmb_user.SufixText = "Sufix Text"
    Me.cmb_user.SufixTextVisible = True
    Me.cmb_user.TabIndex = 1
    Me.cmb_user.TextCombo = Nothing
    Me.cmb_user.TextVisible = False
    Me.cmb_user.TextWidth = 0
    '
    'opt_user_all
    '
    Me.opt_user_all.Location = New System.Drawing.Point(8, 47)
    Me.opt_user_all.Name = "opt_user_all"
    Me.opt_user_all.Size = New System.Drawing.Size(59, 24)
    Me.opt_user_all.TabIndex = 2
    Me.opt_user_all.Text = "opt_user_all"
    '
    'opt_user_one
    '
    Me.opt_user_one.Location = New System.Drawing.Point(8, 19)
    Me.opt_user_one.Name = "opt_user_one"
    Me.opt_user_one.Size = New System.Drawing.Size(59, 24)
    Me.opt_user_one.TabIndex = 0
    Me.opt_user_one.Text = "opt_user_one"
    '
    'fra_audit_code
    '
    Me.fra_audit_code.Controls.Add(Me.cmb_audit_code)
    Me.fra_audit_code.Controls.Add(Me.opt_audit_code_all)
    Me.fra_audit_code.Controls.Add(Me.opt_audit_code_one)
    Me.fra_audit_code.Location = New System.Drawing.Point(264, 100)
    Me.fra_audit_code.Name = "fra_audit_code"
    Me.fra_audit_code.Size = New System.Drawing.Size(277, 80)
    Me.fra_audit_code.TabIndex = 3
    Me.fra_audit_code.TabStop = False
    Me.fra_audit_code.Text = "fra_sudit_code"
    '
    'cmb_audit_code
    '
    Me.cmb_audit_code.AllowUnlistedValues = False
    Me.cmb_audit_code.AutoCompleteMode = False
    Me.cmb_audit_code.IsReadOnly = False
    Me.cmb_audit_code.Location = New System.Drawing.Point(72, 18)
    Me.cmb_audit_code.Name = "cmb_audit_code"
    Me.cmb_audit_code.SelectedIndex = -1
    Me.cmb_audit_code.Size = New System.Drawing.Size(199, 24)
    Me.cmb_audit_code.SufixText = "Sufix Text"
    Me.cmb_audit_code.SufixTextVisible = True
    Me.cmb_audit_code.TabIndex = 1
    Me.cmb_audit_code.TextCombo = Nothing
    Me.cmb_audit_code.TextVisible = False
    Me.cmb_audit_code.TextWidth = 0
    '
    'opt_audit_code_all
    '
    Me.opt_audit_code_all.Location = New System.Drawing.Point(8, 47)
    Me.opt_audit_code_all.Name = "opt_audit_code_all"
    Me.opt_audit_code_all.Size = New System.Drawing.Size(59, 24)
    Me.opt_audit_code_all.TabIndex = 2
    Me.opt_audit_code_all.Text = "opt_audit_code_one"
    '
    'opt_audit_code_one
    '
    Me.opt_audit_code_one.Location = New System.Drawing.Point(8, 19)
    Me.opt_audit_code_one.Name = "opt_audit_code_one"
    Me.opt_audit_code_one.Size = New System.Drawing.Size(59, 24)
    Me.opt_audit_code_one.TabIndex = 0
    Me.opt_audit_code_one.Text = "opt_audit_code_one"
    '
    'fra_date
    '
    Me.fra_date.Controls.Add(Me.dt_date_to)
    Me.fra_date.Controls.Add(Me.dt_date_from)
    Me.fra_date.Location = New System.Drawing.Point(10, 6)
    Me.fra_date.Name = "fra_date"
    Me.fra_date.Size = New System.Drawing.Size(248, 80)
    Me.fra_date.TabIndex = 0
    Me.fra_date.TabStop = False
    Me.fra_date.Text = "fra_date"
    '
    'dt_date_to
    '
    Me.dt_date_to.Checked = True
    Me.dt_date_to.IsReadOnly = False
    Me.dt_date_to.Location = New System.Drawing.Point(2, 48)
    Me.dt_date_to.Name = "dt_date_to"
    Me.dt_date_to.ShowCheckBox = True
    Me.dt_date_to.ShowUpDown = False
    Me.dt_date_to.Size = New System.Drawing.Size(240, 25)
    Me.dt_date_to.SufixText = "Sufix Text"
    Me.dt_date_to.SufixTextVisible = True
    Me.dt_date_to.TabIndex = 1
    Me.dt_date_to.TextWidth = 50
    Me.dt_date_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dt_date_from
    '
    Me.dt_date_from.Checked = True
    Me.dt_date_from.IsReadOnly = False
    Me.dt_date_from.Location = New System.Drawing.Point(1, 17)
    Me.dt_date_from.Name = "dt_date_from"
    Me.dt_date_from.ShowCheckBox = True
    Me.dt_date_from.ShowUpDown = False
    Me.dt_date_from.Size = New System.Drawing.Size(240, 25)
    Me.dt_date_from.SufixText = "Sufix Text"
    Me.dt_date_from.SufixTextVisible = True
    Me.dt_date_from.TabIndex = 0
    Me.dt_date_from.TextWidth = 50
    Me.dt_date_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'uc_pr_list
    '
    Me.uc_pr_list.FilterByCurrency = False
    Me.uc_pr_list.Location = New System.Drawing.Point(544, 2)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(338, 188)
    Me.uc_pr_list.TabIndex = 4
    Me.uc_pr_list.TerminalListHasValues = False
    '
    'frm_auditor
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(1235, 644)
    Me.Name = "frm_auditor"
    Me.ShowInTaskbar = False
    Me.Text = "frm_auditor"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.fra_gui.ResumeLayout(False)
    Me.fra_user.ResumeLayout(False)
    Me.fra_user.PerformLayout()
    Me.fra_audit_code.ResumeLayout(False)
    Me.fra_date.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Members "

  Private FunctionName As String
  Private Collections_Ids As New CLASS_AUDITOR_DATA(AUDIT_NAME_GENERIC)     ' Gui Ids and Audot Codes

  ' For report filters 
  Private m_gui As String
  Private m_user As String
  Private m_code As String
  Private m_date_from As String
  Private m_date_to As String
  Private m_terminals As String

  Private m_related_exists As Boolean

#End Region ' Members

#Region " Constants "
  Private Const INDENT_LEVEL As String = "    "
  Private Const USER_TABLE_NAME As String = "UserTable"
  Private Const MAX_GUI_USERS As Integer = 1000
  Private Const MAIN_MODULE_NAME As String = "frm_auditor"
  Private Const AGENCY_LEVEL As Integer = 3
  Private Const HEADER_LEVEL As Integer = 0

  Private Const GRID_COL_DATE As Integer = 0
  Private Const GRID_COL_GUI_NAME As Integer = 1
  Private Const GRID_COL_MACHINE As Integer = 2
  Private Const GRID_COL_USER_NAME As Integer = 3
  Private Const GRID_COL_AUDIT_CODE As Integer = 4
  Private Const GRID_COL_MESSAGE As Integer = 5
  Private Const GRID_COL_LEVEL As Integer = 6

  Private Const GRID_NUM_COLUMNS As Integer = 7

#End Region ' Constants

#Region " Overrides Functions "

  ' Initialize controls in the form
  Protected Overrides Sub GUI_InitControls()
    '-------------------------------------------
    ' WARNING: call this procedure first!!
    Call MyBase.GUI_InitControls()
    '-------------------------------------------

    'OC 30/04/04- make this screen an edition one to support collapse and delete.
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION

    Dim _dt As DataTable
    Dim _drs() As DataRow
    Dim _row As DataRow

    m_related_exists = CLASS_AUDITOR_DATA.ColumnsRelatedExists()

    _dt = Collections_Ids.GUI.Table.Copy()
    _drs = _dt.Select("false")

    Select Case GLB_GuiMode

      Case public_globals.ENUM_GUI.WIGOS_GUI
        _drs = _dt.Select("id in (200,201)")

      Case public_globals.ENUM_GUI.MULTISITE_GUI
        _drs = _dt.Select("id in (14, 15, 104 ,201)")

      Case public_globals.ENUM_GUI.CASHDESK_DRAWS_GUI
        _drs = _dt.Select("id in (14, 15, 104, 200)")

    End Select

    For Each _row In _drs
      _dt.Rows.Remove(_row)
    Next

    ' Fill combo box of GUI IDs
    Call Fill_combo(cmb_gui, _dt)

    ' Fill combo box of Audit Code
    Call Fill_combo(cmb_audit_code, Collections_Ids.AuditCode.Table)

    ' Other basic functions....
    Call AddUserTableToDataset()

    ' Initialize default values un the form
    dt_date_from.ShowCheckBox = True
    dt_date_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_SHORT)
    dt_date_to.ShowCheckBox = True
    dt_date_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_TIME_SHORT)

    ' Set NLS Code
    Me.Text = GLB_NLS_GUI_AUDITOR.GetString(3)

    fra_gui.Text = GLB_NLS_GUI_AUDITOR.GetString(253)
    opt_gui_one.Text = GLB_NLS_GUI_AUDITOR.GetString(262)
    opt_gui_all.Text = GLB_NLS_GUI_AUDITOR.GetString(263)
    fra_user.Text = GLB_NLS_GUI_AUDITOR.GetString(254)
    opt_user_one.Text = GLB_NLS_GUI_AUDITOR.GetString(262)
    opt_user_all.Text = GLB_NLS_GUI_AUDITOR.GetString(263)
    fra_audit_code.Text = GLB_NLS_GUI_AUDITOR.GetString(260)
    opt_audit_code_one.Text = GLB_NLS_GUI_AUDITOR.GetString(262)
    opt_audit_code_all.Text = GLB_NLS_GUI_AUDITOR.GetString(263)
    fra_date.Text = GLB_NLS_GUI_AUDITOR.GetString(256)
    dt_date_from.Text = GLB_NLS_GUI_AUDITOR.GetString(257)
    dt_date_to.Text = GLB_NLS_GUI_AUDITOR.GetString(258)
    chk_show_all.Text = GLB_NLS_GUI_CONFIGURATION.GetString(90)

    GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_AUDITOR.GetString(12)

    GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_FILTER_APPLY).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_FILTER_RESET).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_INFO).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False

    ' Providers - Terminals
    If Not GeneralParam.GetBoolean("MultiSite", "IsCenter", False) And m_related_exists Then
      Call Me.uc_pr_list.Init(WSI.Common.Misc.GamingTerminalTypeList())
    Else
      Me.uc_pr_list.Visible = False
    End If
    Call uc_pr_list.SetGroupBoxTitle(GLB_NLS_GUI_STATISTICS.GetString(300))

    ' Set default values
    Call SetDafultValues()

    ' Fill the grid without filter (only header row)
    Call GUI_StyleView()
  End Sub

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY)
  End Sub

  ' Reset filter to the initial to the defalut values
  Protected Overrides Sub GUI_FilterReset()
    Call SetDafultValues()
  End Sub

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_GUI_AUDITOR

    ' TJG 01-FEB-2005
    ' Set the form icon from the embedded resource (ICO file must be built in the project as "Embedded Resource")
    ' Call could be made as GetManifestResourceStream("GUI_Auditor.GUI_Auditor.ico"))

    '------------------------------------------------
    ' XVV 13/04/2007
    ' Translate tu BASE Form
    ' Me.Icon = New Icon(System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream(Me.GetType(), "WigosGUI.ico"))
    '------------------------------------------------

    '------------------------------------------------
    'XVV 13/04/2007
    'Call Base Form proc
    Call MyBase.GUI_SetFormId()
    '------------------------------------------------

  End Sub

  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _string_sql As String
    Dim _string_filter As String
    Dim _string_terminals As String

    _string_filter = ""

    _string_sql = "SELECT   GUI_AUDIT.GA_GUI_ID" & _
                            ", GUI_AUDIT.GA_GUI_USER_ID " & _
                            ", GUI_AUDIT.GA_GUI_USERNAME " & _
                            ", GUI_AUDIT.GA_DATETIME " & _
                            ", GUI_AUDIT.GA_AUDIT_CODE " & _
                            ", GUI_AUDIT.GA_AUDIT_LEVEL " & _
                            ", GUI_AUDIT.GA_NLS_ID " & _
                            ", GUI_AUDIT.GA_NLS_PARAM01 " & _
                            ", GUI_AUDIT.GA_NLS_PARAM02 " & _
                            ", GUI_AUDIT.GA_NLS_PARAM03 " & _
                            ", GUI_AUDIT.GA_NLS_PARAM04 " & _
                            ", GUI_AUDIT.GA_NLS_PARAM05 " & _
                            ", GUI_AUDIT.GA_AUDIT_ID " & _
                            ", GUI_AUDIT.GA_COMPUTER_NAME " & _
                      "FROM    GUI_AUDIT "

    ' Add GUI_Id filter
    If opt_gui_one.Checked = True Then
      _string_filter = "GA_GUI_ID = " & cmb_gui.Value
    End If

    ' Add user_Id filter
    If opt_user_one.Checked = True Then
      If _string_filter.Length > 0 Then
        _string_filter = _string_filter & " AND "
      End If

      _string_filter = _string_filter + " GUI_AUDIT.GA_GUI_USER_ID = " & cmb_user.Value
    End If

    ' Add audit_code filter
    If opt_audit_code_one.Checked = True Then
      If _string_filter.Length > 0 Then
        _string_filter = _string_filter & " AND "
      End If
      _string_filter = _string_filter + "GA_AUDIT_CODE = " & cmb_audit_code.Value
    End If

    ' Add FROM date filter
    If dt_date_from.Checked = True Then
      If _string_filter.Length > 0 Then
        _string_filter = _string_filter & " AND "
      End If

      _string_filter = _string_filter + "GA_DATETIME >= " & GUI_FormatDateDB(dt_date_from.Value)

    End If

    ' Add TO date filter
    If dt_date_to.Checked = True Then
      If _string_filter.Length > 0 Then
        _string_filter = _string_filter & " AND "
      End If

      _string_filter = _string_filter + "GA_DATETIME <= " & GUI_FormatDateDB(dt_date_to.Value)

    End If

    ' Filter Providers - Terminals
    If Not GeneralParam.GetBoolean("MultiSite", "IsCenter", False) And m_related_exists Then
      _string_terminals = Me.uc_pr_list.GetProviderIdListSelected()
      If Me.uc_pr_list.OneTerminalChecked Then
        If _string_filter.Length > 0 Then
          _string_filter = _string_filter & " AND "
        End If
        _string_filter = _string_filter & " GA_RELATED_TYPE = " & CLASS_AUDITOR_DATA.ENUM_AUDITOR_RELATED.TERMINAL & " AND GA_RELATED_ID IN " & _string_terminals
      ElseIf Me.uc_pr_list.SeveralTerminalsChecked Then
        If _string_filter.Length > 0 Then
          _string_filter = _string_filter & " AND "
        End If
        _string_filter = _string_filter & " GA_RELATED_TYPE = " & CLASS_AUDITOR_DATA.ENUM_AUDITOR_RELATED.TERMINAL & " AND GA_RELATED_ID IN " & _string_terminals
      End If
    End If

    If _string_filter.Length > 0 Then
      _string_sql = _string_sql + " WHERE " + _string_filter
    End If

    _string_sql = _string_sql + " ORDER BY   GUI_AUDIT.GA_AUDIT_ID DESC" & _
                                        ", GUI_AUDIT.GA_GUI_ID " & _
                                        ", GUI_AUDIT.GA_AUDIT_CODE " & _
                                        ", GUI_AUDIT.GA_AUDIT_LEVEL ASC " & _
                                        ", GUI_AUDIT.GA_ITEM_ORDER ASC"

    Return _string_sql

  End Function

  Protected Overrides Function GUI_FilterCheck() As Boolean
    GUI_FilterCheck = True

    If Me.dt_date_from.Checked = True And Me.dt_date_to.Checked = True Then
      If Me.dt_date_from.Value > Me.dt_date_to.Value Then
        ' Error en el intervalo de fechas: 
        ' La fecha inicial debe ser menor que la final.
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        Return False
      End If
    End If

    If Me.opt_gui_one.Checked = True Then
      If Me.cmb_gui.SelectedIndex < 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(102), _
                        mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, _
                        mdl_NLS.ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                        GLB_NLS_GUI_AUDITOR.GetString(253))
        Return False
      End If
    End If

    If Me.opt_user_one.Checked = True Then
      If Me.cmb_user.SelectedIndex < 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(102), _
                        mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, _
                        mdl_NLS.ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                        GLB_NLS_GUI_AUDITOR.GetString(254))
        Return False
      End If
    End If

    If opt_audit_code_one.Checked = True Then
      If Me.cmb_audit_code.SelectedIndex < 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(102), _
                        mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, _
                        mdl_NLS.ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                        GLB_NLS_GUI_AUDITOR.GetString(260))
        Return False
      End If
    End If

    Return True
  End Function

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim idx_param As Integer
    Dim nls_param(NLS_MAX_PARAMS) As String
    Dim string_message As String
    Dim prev_redraw As Boolean

    '  GUI Name 
    Me.Grid.Cell(RowIndex, GRID_COL_GUI_NAME).Value = Collections_Ids.GUI.NameFromId(DbRow.Value(0))

    '  GUI Machine Name 
    If DbRow.Value(0) = ENUM_GUI.CASHIER Then
      Me.Grid.Cell(RowIndex, GRID_COL_MACHINE).Value = Computer.Alias(DbRow.Value(13))
    Else
      Me.Grid.Cell(RowIndex, GRID_COL_MACHINE).Value = DbRow.Value(13)
    End If
    '  User Name 
    Me.Grid.Cell(RowIndex, GRID_COL_USER_NAME).Value = DbRow.Value(2)

    '  Date 
    Me.Grid.Cell(RowIndex, GRID_COL_DATE).Value = GUI_FormatDate(DbRow.Value(3), _
                                                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                 ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    '  Audit Code 
    Me.Grid.Cell(RowIndex, GRID_COL_AUDIT_CODE).Value = Collections_Ids.AuditCode.NameFromId(DbRow.Value(4))

    ' Load params for NLS message
    For idx_param = 0 To NLS_MAX_PARAMS - 1
      If DbRow.IsNull(7 + idx_param) Then
        nls_param(idx_param) = ""
      Else
        nls_param(idx_param) = NullTrim(DbRow.Value(7 + idx_param))
      End If
    Next

    ' Replace NLS ID by their equivalence
    string_message = NLS_GetString(DbRow.Value(6), _
                                   nls_param(0), _
                                   nls_param(1), _
                                   nls_param(2), _
                                   nls_param(3), _
                                   nls_param(4))

    Me.Grid.Cell(RowIndex, GRID_COL_MESSAGE).Value = Indent(DbRow.Value(5)) + string_message

    ' OC - 30/04/04 add row level for expand/collapse
    Me.Grid.Cell(RowIndex, GRID_COL_LEVEL).Value = DbRow.Value(5)

    prev_redraw = Me.Grid.Redraw
    Me.Grid.Redraw = False

    If Not RowIsHeader(RowIndex) Then
      Me.Grid.Row(RowIndex).Height = 0
    Else

      'XVV 16/04/2007
      'Change Me.XXX for frm_auditor, compiler generate a warning
      Call Me.Grid.Row(RowIndex).SetSignalAllowExpand(GRID_COL_MESSAGE)

    End If

    Me.Grid.Redraw = prev_redraw

    Return True

  End Function      ' GUI_SetupRow

  ' PURPOSE: OC 30/04/04 Activated when a row of the grid title is double clicked
  '          in order to expand / collapse the forms rows of a specific Gui.
  '  PARAMS:
  '     - INPUT:
  '               
  '     - OUTPUT:
  '          
  ' RETURNS:
  '     - none
  Protected Overrides Sub GUI_EditSelectedItem()

    Dim last_form_row As Integer
    Dim idx As Integer
    Dim prev_redraw As Boolean

    With Me.Grid

      prev_redraw = .Redraw
      .Redraw = False

      If Not RowIsHeader(.CurrentRow) Then

        Exit Sub
      End If

      last_form_row = .CurrentRow

      For idx = .CurrentRow + 1 To Me.Grid.NumRows - 1
        If RowIsHeader(idx) Then

          Exit For
        End If
        last_form_row = idx
      Next

      Call .CollapseExpand(.CurrentRow, last_form_row, GRID_COL_MESSAGE)

      .Redraw = prev_redraw

    End With

  End Sub

#Region " GUI Reports "

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(256) & " " & GLB_NLS_GUI_AUDITOR.GetString(257), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(256) & " " & GLB_NLS_GUI_AUDITOR.GetString(258), m_date_to)
    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(253), m_gui)
    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(254), m_user)
    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(260), m_code)
    If Not GeneralParam.GetBoolean("MultiSite", "IsCenter", False) Then
      PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_terminals)
    End If

    PrintData.FilterHeaderWidth(1) = 2000
    PrintData.FilterHeaderWidth(3) = 2000
  End Sub

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)
    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_gui = ""
    m_user = ""
    m_code = ""
    m_date_from = ""
    m_date_to = ""
    m_terminals = ""

    ' User 
    If Me.opt_user_all.Checked Then
      m_user = Me.opt_user_all.Text
    Else
      m_user = Me.cmb_user.TextValue
    End If

    ' Gui 
    If Me.opt_gui_all.Checked Then
      m_gui = Me.opt_gui_all.Text
    Else
      m_gui = Me.cmb_gui.TextValue
    End If

    ' Audit Code 
    If Me.opt_audit_code_all.Checked Then
      m_code = Me.opt_audit_code_all.Text
    Else
      m_code = Me.cmb_audit_code.TextValue
    End If

    ' Providers - Terminals
    If Not GeneralParam.GetBoolean("MultiSite", "IsCenter", False) And m_related_exists Then
      m_terminals = Me.uc_pr_list.GetTerminalReportText()
    End If

    ' Activation Date 
    If Me.dt_date_from.Checked Then
      m_date_from = GUI_FormatDate(Me.dt_date_from.Value, _
                    ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                    ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_TIME_SHORT)
    End If

    If Me.dt_date_to.Checked Then
      m_date_to = GUI_FormatDate(Me.dt_date_to.Value, _
                  ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                  ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_TIME_SHORT)
    End If

  End Sub

#End Region ' GUI Reports

#End Region ' Overrides Functions

#Region " Private functions "

  ' PURPOSE: Initialize grid view
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - none
  Private Sub GUI_StyleView()
    'XVV Variable not use Dim datagrid_column As DataGridTextBoxColumn

    Me.Grid.Init(GRID_NUM_COLUMNS)

    '  GUI Name 
    Me.Grid.Column(GRID_COL_GUI_NAME).Header.Text = GLB_NLS_GUI_AUDITOR.GetString(253)
    Me.Grid.Column(GRID_COL_GUI_NAME).Width = 1085

    '  GUI Machine Name 
    Me.Grid.Column(GRID_COL_MACHINE).Header.Text = GLB_NLS_GUI_CONFIGURATION.GetString(474)
    Me.Grid.Column(GRID_COL_MACHINE).Width = 2000

    '  User Name 
    Me.Grid.Column(GRID_COL_USER_NAME).Header.Text = GLB_NLS_GUI_AUDITOR.GetString(254)
    Me.Grid.Column(GRID_COL_USER_NAME).Width = 1500

    '  Date 
    Me.Grid.Column(GRID_COL_DATE).Header.Text = GLB_NLS_GUI_AUDITOR.GetString(256)
    Me.Grid.Column(GRID_COL_DATE).Width = 2000
    Me.Grid.Column(GRID_COL_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    '  Audit Code 
    Me.Grid.Column(GRID_COL_AUDIT_CODE).Header.Text = GLB_NLS_GUI_AUDITOR.GetString(264)
    Me.Grid.Column(GRID_COL_AUDIT_CODE).Width = 2250

    '  Message 
    Me.Grid.Column(GRID_COL_MESSAGE).Header.Text = GLB_NLS_GUI_AUDITOR.GetString(252)
    Me.Grid.Column(GRID_COL_MESSAGE).Width = 6880

    'OC 30/04/04  hidden column- row level add for collapse/expand
    Me.Grid.Column(GRID_COL_LEVEL).Header.Text = ""
    Me.Grid.Column(GRID_COL_LEVEL).Width = 0

    ' 19-MAY-2004, Not allowed Sort due to the hidden rows.
    Me.Grid.IsSortable = False

  End Sub 'GUI_StyleView()

  ' PURPOSE: Fix the default values in the orm
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - none
  Private Sub SetDafultValues()
    ' Blank selection
    cmb_gui.SelectedIndex = -1
    cmb_user.SelectedIndex = -1
    cmb_audit_code.SelectedIndex = -1
    cmb_gui.TextValue = ""
    cmb_user.TextValue = ""
    cmb_audit_code.TextValue = ""
    ' Init combos
    opt_gui_all.Checked = True
    cmb_gui.Enabled = False
    opt_user_all.Checked = True
    cmb_user.Enabled = False
    opt_audit_code_all.Checked = True
    cmb_audit_code.Enabled = False
    chk_show_all.Checked = False
    chk_show_all.Enabled = False

    ' Init dates
    dt_date_from.Value = GUI_GetDate()
    dt_date_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    dt_date_from.ShowCheckBox = True
    dt_date_from.Checked = True
    dt_date_to.Value = GUI_GetDate()
    dt_date_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    dt_date_to.ShowCheckBox = True
    dt_date_to.Checked = False

    If Not GeneralParam.GetBoolean("MultiSite", "IsCenter", False) And m_related_exists Then
      Call Me.uc_pr_list.SetDefaultValues()
    End If

  End Sub 'SetDafultValues


  ' PURPOSE: Fill the combo box user control
  '
  '  PARAMS:
  '     - INPUT:
  '           - table: Tableset from memory
  '     - OUTPUT:
  '           - combo: User control
  '
  ' RETURNS:
  '     - none
  Private Sub Fill_combo(ByVal combo As uc_combo, ByVal table As DataTable)
    Dim idx As Integer
    Dim aux_id As Integer
    Dim aux_name As String
    Dim s As SortedList

    Call combo.Clear()
    s = New SortedList()

    For idx = 0 To table.Rows.Count - 1
      aux_id = CInt(table.Rows.Item(idx).Item(0))
      aux_name = CStr(table.Rows.Item(idx).Item(1))
      s.Add(aux_name, aux_id)
    Next

    For idx = 0 To s.Count - 1
      aux_id = s.GetByIndex(idx)
      aux_name = s.GetKey(idx)
      Call combo.Add(aux_id, aux_name)
    Next

  End Sub 'Fill_combo


  ' PURPOSE: Fill the user tbale wuth descendents of logged user
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Sub AddUserTableToDataset()
    Dim string_sql As String
    'XVV Variable not use Dim ds As DataSet
    Dim dt As DataTable

    Call cmb_user.Clear()

    Try
      string_sql = " SELECT DISTINCT(GU_USER_ID) " & _
                        ", GU_USERNAME " & _
                   " FROM   GUI_USERS "

      ' XCD 16-Aug-2012 Discard unsubscribed users
      If Not chk_show_all.Checked Then
        string_sql = string_sql & " WHERE GU_BLOCK_REASON = " & WSI.Common.GUI_USER_BLOCK_REASON.NONE
      End If

      dt = GUI_GetTableUsingSQL(string_sql, MAX_RECORDS_LOAD)

      If Not IsNothing(dt) Then
        Call Fill_combo(cmb_user, dt)
        dt.Dispose()
        dt = Nothing
      End If


    Catch exception As Exception
      Call Trace.WriteLine(exception.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            MAIN_MODULE_NAME, _
                            FunctionName, _
                            exception.Message)
    End Try
  End Sub 'AddUserTableToDataset

  Private Function RowIsHeader(ByVal RowIdx As Integer) As Boolean

    If Grid.Cell(RowIdx, GRID_COL_LEVEL).Value <> HEADER_LEVEL Then

      Return False
    End If

    Return True
  End Function


  ' PURPOSE: Indent the strung message by the GA_AUDIT_LEVEL
  '
  '  PARAMS:
  '     - INPUT:
  '           - idx_row: Row number 
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Blank text of size = INDENT_LEVEL *  GA_AUDIT_LEVEL
  Protected Function Indent(ByVal level As Integer) As String
    Dim idx_start As Integer

    Indent = ""

    ' Indent level message
    For idx_start = 0 To level - 1
      Indent = Indent + INDENT_LEVEL
    Next
  End Function 'Indent

#End Region ' Private Functions

#Region " Public functions "
  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region

#Region " Events "

  Private Sub opt_audit_code_all_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_audit_code_all.Click
    cmb_audit_code.Enabled = False
  End Sub 'opt_audit_code_all_Click

  Private Sub opt_audit_code_one_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_audit_code_one.Click
    cmb_audit_code.Enabled = True
  End Sub 'opt_audit_code_one_Click

  Private Sub opt_user_all_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_user_all.Click
    cmb_user.Enabled = False
    chk_show_all.Enabled = False
  End Sub 'opt_user_all_Click

  Private Sub opt_user_one_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_user_one.Click
    cmb_user.Enabled = True
    chk_show_all.Enabled = True
  End Sub 'opt_user_one_Click

  Private Sub opt_gui_all_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_gui_all.Click
    cmb_gui.Enabled = False
  End Sub 'opt_gui_all_Click

  Private Sub opt_gui_one_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_gui_one.Click
    cmb_gui.Enabled = True
  End Sub 'opt_gui_one_Click

  Private Sub chk_show_all_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_show_all.CheckedChanged
    AddUserTableToDataset()
  End Sub 'chk_show_unsubscribed

#End Region ' Events


End Class