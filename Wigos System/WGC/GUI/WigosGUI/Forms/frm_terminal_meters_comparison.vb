﻿'-------------------------------------------------------------------
' Copyright © 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_terminal_sas_meters_sel
' DESCRIPTION:   SAS Counters
' AUTHOR:        Yineth Nuñez
' CREATION DATE: 27-AGU-2015
'
' REVISION HISTORY:
'
' Date        Author Description
' ------------ ------ -------------------------------------------------------------------------------------
' 27-AUG-20135  YNM   First Release
'---------------------------------------------------------------------------------------------------------
Option Explicit On
Option Strict Off

#Region "Imports"

Imports System.Text
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common
Imports System.Data.SqlClient


#End Region
Public Class frm_terminal_meters_comparison
  Inherits GUI_Controls.frm_base_sel

#Region "Constants"
  ' Grid configuration
  Private GRID_COLUMNS As Integer = 0
  Private Const GRID_HEADER_ROWS As Integer = 0
#End Region

#Region "Members"
  Private m_date As String
  Private m_terminals As String
  Private m_selected_meters As List(Of MetersGroup)
  Private m_all_meters As List(Of MetersGroup)
  Private m_sum_total As List(Of Double)
  Private m_tool_tip As List(Of String)
  Private m_total_columns As Integer
  Private m_multiplier_columns As Integer
  Private m_system_mode As WSI.Common.SYSTEM_MODE
#End Region

#Region "Overrides"

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    Dim _terminal_types() As Integer

    Call MyBase.GUI_InitControls()

    ' Operations
    Me.list_meters_group.GroupBoxText = GLB_NLS_GUI_CONTROLS.GetString(460)
    Me.list_meters_group.ColumnWidth(uc_checked_list.GRID_COLUMN_DESC, 3000)
    Me.list_meters_group.btn_check_all.Width = 120
    Me.list_meters_group.btn_uncheck_all.Width = 120
    Me.list_meters_group.m_resize_width = 290
    Me.list_meters_group.SetLevels = 2

    _terminal_types = {WSI.Common.TerminalTypes.SAS_HOST}
    Call Me.uc_terminals.Init(_terminal_types)

    Call GUI_StyleSheet()
  End Sub

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)
    Dim _print_data As GUI_Reports.CLASS_PRINT_DATA

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CANCEL
        Me.Close()

      Case ENUM_BUTTON.BUTTON_FILTER_APPLY

      Case ENUM_BUTTON.BUTTON_FILTER_RESET

      Case ENUM_BUTTON.BUTTON_INFO

      Case ENUM_BUTTON.BUTTON_NEW

      Case ENUM_BUTTON.BUTTON_PRINT
      Case ENUM_BUTTON.BUTTON_EXCEL
      Case ENUM_BUTTON.BUTTON_SELECT
      Case ENUM_BUTTON.BUTTON_CUSTOM_0
      Case ENUM_BUTTON.BUTTON_CUSTOM_1
      Case ENUM_BUTTON.BUTTON_CUSTOM_2
      Case ENUM_BUTTON.BUTTON_CUSTOM_3
      Case ENUM_BUTTON.BUTTON_CUSTOM_4
      Case ENUM_BUTTON.BUTTON_CUSTOM_5
      Case ENUM_BUTTON.BUTTON_GRID_INFO
      Case Else
        '
    End Select

    If GUI_HasToBeAudited(ButtonId) Then
      _print_data = New GUI_Reports.CLASS_PRINT_DATA
      Call GUI_ReportFilter(_print_data)
      Call MyBase.AuditFormClick(ButtonId, _print_data)
    End If

  End Sub


#End Region

#Region "Public Functions"

  Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.Display(True)

  End Sub


#End Region

#Region "Private Functions"
  Private Sub GUI_StyleSheet()
    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
    End With

  End Sub

#End Region

#Region "Events"

#End Region


End Class