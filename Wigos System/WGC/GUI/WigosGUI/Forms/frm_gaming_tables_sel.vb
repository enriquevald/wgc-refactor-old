'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_gaming_tables_sel.vb
' DESCRIPTION:   This screen allows to view the gaming tables.
' AUTHOR:        Javier Barea
' CREATION DATE: 16-DEC-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 16-DEC-2013  JBP    Initial version
' 18-FEB-2014  RRR    Fixed Bug WIG-643: Search error solved
' 27-MAR-2014  ICS    Fixed Bug WIG-766: Database error when searching for plant ID with an apostrophe
' 15-SEP-2014  DHA    Added functionality to create a new gaming table based on other gaming table
' 30-SEP-2014  DHA    Fixed Bug WIG-1355: Show provider and num seats columns when player tracking is enabled
' 19-NOV-2014  SGB    Fixed Bug WIG-1712: Changes FloorId lenght
' 19-JAN-2016  RGR    Product Backlog Item 4951:Tables: Groups and MD: Groups of chips on the table
' 08-SEP-2016  FOS    PBI 16373: Tables: Split PlayerTracking to GamingTables (Phase 5)
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Text

Public Class frm_gaming_tables_sel
  Inherits frm_base_sel



#Region " Constants "

  ' Fields length
  Private Const MAX_CODE_LEN As Integer = 20
  Private Const MAX_NAME_LEN As Integer = 50

  ' Grid Columns
  Private Const GRID_COLUMN_SENTINEL As Integer = 0
  Private Const GRID_COLUMN_GAME_TABLE_CODE As Integer = 1
  Private Const GRID_COLUMN_GAME_TABLE_TYPE As Integer = 2
  Private Const GRID_COLUMN_GAME_TABLE_NAME As Integer = 3
  Private Const GRID_COLUMN_GAME_TABLE_AREA As Integer = 4
  Private Const GRID_COLUMN_GAME_TABLE_ZONE As Integer = 5
  Private Const GRID_COLUMN_GAME_TABLE_BANK As Integer = 6
  Private Const GRID_COLUMN_GAME_TABLE_FLOOR As Integer = 7
  Private Const GRID_COLUMN_GAME_TABLE_ENABLED As Integer = 8
  Private Const GRID_COLUMN_GAME_TABLE_INTEGRATED As Integer = 9
  Private Const GRID_COLUMN_GAME_TABLE_ID As Integer = 10
  Private Const GRID_COLUMN_GAME_TABLE_NUM_SEATS As Integer = 11
  Private Const GRID_COLUMN_GAME_TABLE_PROVIDER_ID As Integer = 12
  Private Const GRID_COLUMN_GAME_TABLE_PROVIDER_NAME As Integer = 13

  ' Grid Columns Width
  Private Const GRID_COLUMN_SENTINEL_WIDTH As Integer = 200
  Private Const GRID_COLUMN_GAME_TABLE_CODE_WIDTH As Integer = 1560
  Private Const GRID_COLUMN_GAME_TABLE_TYPE_WIDTH As Integer = 1800
  Private Const GRID_COLUMN_GAME_TABLE_NAME_WIDTH As Integer = 2200
  Private Const GRID_COLUMN_GAME_TABLE_AREA_WIDTH As Integer = 1500
  Private Const GRID_COLUMN_GAME_TABLE_ZONE_WIDTH As Integer = 1500
  Private Const GRID_COLUMN_GAME_TABLE_BANK_WIDTH As Integer = 1500
  Private Const GRID_COLUMN_GAME_TABLE_FLOOR_WIDTH As Integer = 1500
  Private Const GRID_COLUMN_GAME_TABLE_ENABLED_WIDTH As Integer = 1100
  Private Const GRID_COLUMN_GAME_TABLE_INTEGRATED_WIDTH As Integer = 2000
  Private Const GRID_COLUMN_GAME_TABLE_ID_WIDTH As Integer = 0
  Private Const GRID_COLUMN_GAME_TABLE_NUM_SEATS_WIDTH As Integer = 1560
  Private Const GRID_COLUMN_GAME_TABLE_PROVIDER_ID_WIDTH As Integer = 0
  Private Const GRID_COLUMN_GAME_TABLE_PROVIDER_NAME_WIDTH As Integer = 2200

  ' Grid setup
  Private Const GRID_HEADER_ROWS As Integer = 1
  Private Const GRID_COLUMNS As Integer = 14

  ' Sql Columns
  Private Const SQL_COLUMN_GAME_TABLE_ID As Integer = 0
  Private Const SQL_COLUMN_GAME_TABLE_CODE As Integer = 1
  Private Const SQL_COLUMN_GAME_TABLE_TYPE As Integer = 2
  Private Const SQL_COLUMN_GAME_TABLE_NAME As Integer = 3
  Private Const SQL_COLUMN_GAME_TABLE_AREA_ID As Integer = 4
  Private Const SQL_COLUMN_GAME_TABLE_AREA_NAME As Integer = 5
  Private Const SQL_COLUMN_GAME_TABLE_BANK As Integer = 6
  Private Const SQL_COLUMN_GAME_TABLE_FLOOR As Integer = 7
  Private Const SQL_COLUMN_GAME_TABLE_ENABLED As Integer = 8
  Private Const SQL_COLUMN_GAME_TABLE_INTEGRATED As Integer = 9
  Private Const SQL_COLUMN_GAME_TABLE_CASHIER_NAME As Integer = 10
  Private Const SQL_COLUMN_GAME_TABLE_NUM_SEATS As Integer = 11
  Private Const SQL_COLUMN_GAME_TABLE_PROVIDER_ID As Integer = 12
  Private Const SQL_COLUMN_GAME_TABLE_PROVIDER_NAME As Integer = 13

  ' Table types data grid
  Private Const GRID_2_COLUMN_CODE As Integer = 0
  Private Const GRID_2_COLUMN_CHECKED As Integer = 1
  Private Const GRID_2_COLUMN_DESC As Integer = 2
  Private Const GRID_2_COLUMN_ROW_TYPE As Integer = 3

  Private Const GRID_2_COLUMNS As Integer = 4

#End Region ' Constants

#Region "Enums"

  Public Structure TYPE_DG_FILTER_ELEMENT
    Dim code As Integer
    Dim description As String
    Dim elem_type As Integer
  End Structure

#End Region ' Enums

#Region " Members "

  ' Event
  Private m_several_click As Boolean

  ' Filter
  Private m_report_name As String
  Private m_report_code As String
  Private m_report_enabled As String
  Private m_report_cashier As String
  Private m_report_area_name As String
  Private m_report_bank_name As String
  Private m_report_floor_id As String
  Private m_report_types As String

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_GAMING_TABLES_SEL

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' -- Form: Gaming Tables-- 
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3416)

    ' -- Buttons --
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(3)

    If WSI.Common.GamingTableBusinessLogic.IsEnabledGTPlayerTracking() AndAlso _
       WSI.Common.GamingTableBusinessLogic.GamingTablesMode() = WSI.Common.GamingTableBusinessLogic.GT_MODE.GUI_AND_CASHIER Then
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4999)  ' 4999 "Dise�o"
    End If

    ' -- Table Code --
    Me.txt_code.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3418)               ' 3418 "C�digo"
    Me.txt_code.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, MAX_CODE_LEN, 0)

    ' -- Table Name --
    Me.txt_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3419)               ' 3419 "Nombre"
    Me.txt_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, MAX_NAME_LEN, 0)

    ' -- Enabled -- 
    Me.gb_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3422)             ' 3422 "Habilitada"
    Me.chk_yes.Text = GLB_NLS_GUI_INVOICING.GetString(479)                       ' "Yes"
    Me.chk_no.Text = GLB_NLS_GUI_INVOICING.GetString(480)                        ' "No"

    ' -- Cashier -- 
    Me.gb_cashier.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3479)             ' 3479 "Cajero"
    Me.chk_integrated.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4339)         ' 4339 "Integrado"
    Me.chk_not_integrated.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4340)     ' 4340 "No Integrado"

    ' -- Table Types --
    Me.gb_table_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3420)          ' "Table type"
    Me.opt_several_types.Text = GLB_NLS_GUI_INVOICING.GetString(223)             ' 272 "Todos"
    Me.opt_all_types.Text = GLB_NLS_GUI_INVOICING.GetString(224)                 ' 273 "Varios"

    Call GUI_StyleSheetTableTypes()

    ' -- Location --
    '   � Area
    Me.gb_area_island.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1166)         ' 1166 "Ubicaci�n"
    Me.cmb_area.Text = GLB_NLS_GUI_CONFIGURATION.GetString(435)                  ' 435 "Area"
    '   � Zone
    Me.ef_smoking.Text = GLB_NLS_GUI_CONFIGURATION.GetString(430)                ' 430 "Zona"
    Me.ef_smoking.IsReadOnly = True
    Me.ef_smoking.TabStop = False
    '   � Bank
    Me.cmb_bank.Text = GLB_NLS_GUI_CONFIGURATION.GetString(431)                  ' 431 "Isla"
    '   � Floor
    Me.ef_floor_id.Text = GLB_NLS_GUI_CONFIGURATION.GetString(432)               ' 432 "Id en planta"
    Me.ef_floor_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 6, 0)

    Call AreaComboFill()
    Call BankComboFill()

    Call AddHandles()

    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls


  ' PURPOSE: Process clicks on data grid (double-clicks) and select button
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Dim _indx_sel As Integer

    _indx_sel = -1

    Select Case ButtonId
      Case frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY
        Call MyBase.GUI_ButtonClick(ButtonId)
        If Me.Grid.NumRows > 0 Then
          Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = True
        Else
          Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False
        End If
      Case ENUM_BUTTON.BUTTON_CUSTOM_0  'Design
        Call GUI_ShowSelectedGamingTableDesign()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_NEW
        If Not IsNothing(Me.Grid.SelectedRows) AndAlso Me.Grid.SelectedRows.Length > 0 Then
          _indx_sel = Me.Grid.SelectedRows(0)
        End If

        If _indx_sel <> -1 Then
          If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5445), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_YES Then
            Call GUI_NewItemFromSelected()
          Else
            Call MyBase.GUI_ButtonClick(ButtonId)
          End If
        Else
          Call MyBase.GUI_ButtonClick(ButtonId)
        End If

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()

    SetDefaultValues()

  End Sub ' GUI_FilterReset  

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    Dim _idx As Integer
    Dim _all_uncheckeds As Boolean

    If Me.opt_several_types.Checked Then
      _all_uncheckeds = True
      For _idx = 0 To dg_filter.NumRows - 1
        If Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
          _all_uncheckeds = False
          Exit For
        End If
      Next
      If _all_uncheckeds Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4351), ENUM_MB_TYPE.MB_TYPE_WARNING)

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck 


  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the
  '          main SQL Query.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Function GetSqlWhere() As String

    Dim _str_where As String = String.Empty

    ' -- Code Filter -- 
    If Not String.IsNullOrEmpty(Me.txt_code.Value) Then
      _str_where = _str_where & " AND (GT_CODE LIKE '%" & DataTable_FilterValue(Me.txt_code.Value) & "%') "
    End If

    ' -- Name Filter -- 
    If Not String.IsNullOrEmpty(Me.txt_name.Value) Then
      _str_where = _str_where & " AND (GT_NAME LIKE '%" & DataTable_FilterValue(Me.txt_name.Value) & "%') "
    End If

    ' -- Enabled Filter -- 
    '   � Only Enableds
    If Me.chk_yes.Checked And Not Me.chk_no.Checked Then
      _str_where = _str_where & " AND (GT_ENABLED = 1) "
    End If
    '   � Only Disableds
    If Not Me.chk_yes.Checked And Me.chk_no.Checked Then
      _str_where = _str_where & " AND (GT_ENABLED = 0) "
    End If

    ' -- Integrated Cashier Filter -- 
    '   � Only With Integrated
    If Me.chk_integrated.Checked And Not Me.chk_not_integrated.Checked Then
      _str_where = _str_where & " AND (GT_HAS_INTEGRATED_CASHIER = 1) "
    End If
    '   � Only Without Integrated
    If Not Me.chk_integrated.Checked And Me.chk_not_integrated.Checked Then
      _str_where = _str_where & " AND (GT_HAS_INTEGRATED_CASHIER = 0) "
    End If

    ' -- Anyone area selected  -
    If Me.cmb_area.SelectedIndex > 0 Then
      _str_where = _str_where & " AND (GT_AREA_ID = " & Me.cmb_area.Value & ") "
      _str_where = _str_where & " AND (GT_BANK_ID = " & Me.cmb_bank.Value & ") "
    End If

    ' -- Floor Id --
    If Not String.IsNullOrEmpty(Me.ef_floor_id.Value) Then
      _str_where = _str_where & " AND " & GUI_FilterField("GT_FLOOR_ID", Me.ef_floor_id.Value, False, False, True)
    End If

    ' -- Gaming Table Type Filter -- 
    _str_where = _str_where & GetSqlWhereTableType("GT_TYPE_ID")

    If Not String.IsNullOrEmpty(_str_where) Then
      _str_where = " WHERE " & _str_where.Remove(_str_where.IndexOf("AND"), 3)
    End If

    Return _str_where
  End Function ' GetSqlWhere


  ' PURPOSE: Build search component to add to the 'where' clause of the 'select' statement
  '          according to the values selected in the data grid filter
  '
  '  PARAMS:
  '     - INPUT:
  '           - DBField: database field name to use in the search condition
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Search condition to add
  Private Function GetSqlWhereTableType(ByVal DBField As String) As String
    Dim str_where As String
    Dim types_list As String

    str_where = ""

    ' Operation Code Type Filter
    If Me.opt_several_types.Checked = True Then
      types_list = GetTypeIdListSelected()
      If types_list <> "" Then
        str_where = " AND (" & DBField & " IN (" & types_list & "))"
      End If
    End If

    Return str_where
  End Function ' GetSqlWhereTableType

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  '
  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _str_sql As StringBuilder

    _str_sql = New StringBuilder()

    _str_sql.AppendLine("   SELECT    GT_GAMING_TABLE_ID        ")
    _str_sql.AppendLine("           , GT_CODE   				        ")
    _str_sql.AppendLine("           , GTT_NAME   				        ")
    _str_sql.AppendLine("           , GT_NAME 				          ")   
    _str_sql.AppendLine("           , AR_AREA_ID       		      ")
    _str_sql.AppendLine("           , AR_NAME          		      ")
    _str_sql.AppendLine("           , BK_NAME          		      ")
    _str_sql.AppendLine("           , GT_FLOOR_ID 		          ")
    _str_sql.AppendLine("           , GT_ENABLED 				        ")
    _str_sql.AppendLine("           , GT_HAS_INTEGRATED_CASHIER ")
    _str_sql.AppendLine("           , CT_NAME                   ")
    _str_sql.AppendLine("           , GT_NUM_SEATS              ")
    _str_sql.AppendLine("           , GT_PROVIDER_ID            ")
    _str_sql.AppendLine("           , PV_NAME                   ")
    _str_sql.AppendLine("     FROM    GAMING_TABLES 			      ")
    _str_sql.AppendLine("     LEFT    JOIN GAMING_TABLES_TYPES	ON  GTT_GAMING_TABLE_TYPE_ID  = GT_TYPE_ID    ")
    _str_sql.AppendLine("     LEFT    JOIN AREAS                ON  GT_AREA_ID                = AR_AREA_ID    ")
    _str_sql.AppendLine("     LEFT    JOIN BANKS                ON  GT_BANK_ID                = BK_BANK_ID    ")
    _str_sql.AppendLine("     LEFT    JOIN CASHIER_TERMINALS    ON  GT_CASHIER_ID             = CT_CASHIER_ID ")
    _str_sql.AppendLine("     LEFT    JOIN PROVIDERS            ON  GT_PROVIDER_ID            = PV_ID         ")

    ' Where 
    _str_sql.AppendLine(GetSqlWhere())

    ' Order
    _str_sql.AppendLine(" ORDER BY    GT_CODE, GT_NAME                      ")

    Return _str_sql.ToString()

  End Function ' GUI_FilterGetSqlQuery 

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  '
  Protected Function GetTableTypes() As DataTable

    Dim _str_sql As StringBuilder

    _str_sql = New StringBuilder()

    _str_sql.AppendLine("   SELECT    GTT_GAMING_TABLE_TYPE_ID  ")
    _str_sql.AppendLine("           , GTT_NAME   				        ")
    _str_sql.AppendLine("     FROM    GAMING_TABLES_TYPES 	    ")
    _str_sql.AppendLine("    WHERE    GTT_ENABLED = 1           ")

    Return GUI_GetTableUsingSQL(_str_sql.ToString(), 5000)

  End Function ' GetTableTypes 


  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  '
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _id As Integer
    Dim _codigo As String
    Dim _type As String
    Dim _name As String
    Dim _area_id As Integer
    Dim _area_name As String
    Dim _zone As String
    Dim _bank_name As String
    Dim _floor_id As String 
    Dim _enabled As Boolean
    Dim _integrated As Boolean
    Dim _cashier_name As String
    Dim _num_seats As Integer
    Dim _provider_id As Integer
    Dim _provider_name As String

    ' Game table code
    _codigo = DbRow.Value(SQL_COLUMN_GAME_TABLE_CODE)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_TABLE_CODE).Value = _codigo

    ' Game table type
    _type = DbRow.Value(SQL_COLUMN_GAME_TABLE_TYPE)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_TABLE_TYPE).Value = _type

    ' Game table name
    _name = DbRow.Value(SQL_COLUMN_GAME_TABLE_NAME)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_TABLE_NAME).Value = _name

    ' Area
    If (Not DbRow.IsNull(SQL_COLUMN_GAME_TABLE_AREA_NAME)) Then
      _area_name = DbRow.Value(SQL_COLUMN_GAME_TABLE_AREA_NAME)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_TABLE_AREA).Value = _area_name

      ' Zone
      If (Not DbRow.IsNull(SQL_COLUMN_GAME_TABLE_AREA_ID)) Then
        _area_id = DbRow.Value(SQL_COLUMN_GAME_TABLE_AREA_ID)

        _zone = GetZoneDescription(_area_id)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_TABLE_ZONE).Value = _zone
      End If
    End If

    ' Bank
    If (Not DbRow.IsNull(SQL_COLUMN_GAME_TABLE_BANK)) Then
      _bank_name = DbRow.Value(SQL_COLUMN_GAME_TABLE_BANK)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_TABLE_BANK).Value = _bank_name
    End If

    ' FloorId
    If (Not DbRow.IsNull(SQL_COLUMN_GAME_TABLE_FLOOR)) Then
      _floor_id = DbRow.Value(SQL_COLUMN_GAME_TABLE_FLOOR)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_TABLE_FLOOR).Value = _floor_id
    End If

    ' Game table enabled
    If (Not DbRow.IsNull(SQL_COLUMN_GAME_TABLE_ENABLED)) Then
      _enabled = DbRow.Value(SQL_COLUMN_GAME_TABLE_ENABLED)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_TABLE_ENABLED).Value = IIf(_enabled, GLB_NLS_GUI_INVOICING.GetString(479), GLB_NLS_GUI_INVOICING.GetString(480))
    End If

    ' Game table Integrated cashier
    If (Not DbRow.IsNull(SQL_COLUMN_GAME_TABLE_INTEGRATED)) Then
      _integrated = DbRow.Value(SQL_COLUMN_GAME_TABLE_INTEGRATED)
      _cashier_name = GLB_NLS_GUI_INVOICING.GetString(480)

      If _integrated Then
        _cashier_name = DbRow.Value(SQL_COLUMN_GAME_TABLE_CASHIER_NAME)
      End If

      Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_TABLE_INTEGRATED).Value = _cashier_name
    End If

    If WSI.Common.GamingTableBusinessLogic.IsEnabledGTPlayerTracking() AndAlso _
       WSI.Common.GamingTableBusinessLogic.GamingTablesMode() = WSI.Common.GamingTableBusinessLogic.GT_MODE.GUI_AND_CASHIER Then
      ' Game table num seats
      _num_seats = 0
      If (Not DbRow.IsNull(SQL_COLUMN_GAME_TABLE_NUM_SEATS)) Then
        _num_seats = DbRow.Value(SQL_COLUMN_GAME_TABLE_NUM_SEATS)
      End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_TABLE_NUM_SEATS).Value = _num_seats

      ' Provider ID
      _provider_id = 0
      If (Not DbRow.IsNull(SQL_COLUMN_GAME_TABLE_PROVIDER_ID)) Then
        _provider_id = DbRow.Value(SQL_COLUMN_GAME_TABLE_PROVIDER_ID)
      End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_TABLE_PROVIDER_ID).Value = _provider_id

      ' Provider Name
      _provider_name = ""
      If (Not DbRow.IsNull(SQL_COLUMN_GAME_TABLE_PROVIDER_NAME)) Then
        _provider_name = DbRow.Value(SQL_COLUMN_GAME_TABLE_PROVIDER_NAME)
      End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_TABLE_PROVIDER_NAME).Value = _provider_name
    End If

    ' Game table ID
    If (Not DbRow.IsNull(SQL_COLUMN_GAME_TABLE_ID)) Then
      _id = DbRow.Value(SQL_COLUMN_GAME_TABLE_ID)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_TABLE_ID).Value = _id
    End If

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Activated when a row of the grid is double clicked or selected, so it can be edited
  '  PARAMS:
  '     - INPUT:
  '               
  '     - OUTPUT:
  '          
  ' RETURNS:
  '     - none
  Protected Overrides Sub GUI_EditSelectedItem()

    Dim _idx_row As Int32
    Dim _gaming_table_id As Integer
    Dim _frm_edit As Object

    'Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    If _idx_row = Me.Grid.NumRows Then
      Return
    End If

    ' Get the Draw ID and Name, and launch the editor
    _gaming_table_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_GAME_TABLE_ID).Value

    _frm_edit = New frm_gaming_tables_edit

    Call _frm_edit.ShowEditItem(_gaming_table_id)

    _frm_edit = Nothing
    Call Me.Grid.Focus()

  End Sub 'GUI_EditSelectedItem

  Protected Overrides Sub GUI_EditNewItem()
    Dim _frm As frm_gaming_tables_edit

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_gaming_tables_edit
    _frm.ShowNewItem()

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub ' GUI_EditNewItem

  Protected Overrides Sub Finalize()
    MyBase.Finalize()
  End Sub ' Finalize

#End Region ' OVERRIDES 

#Region " GUI Reports "

  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    With Me.Grid

    End With

    Call MyBase.GUI_ReportParams(ExcelData)

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    ' Name Filter
    If Not String.IsNullOrEmpty(m_report_name) Then
      PrintData.SetFilter(txt_name.Text, m_report_name)
    End If

    ' Code Filter
    If Not String.IsNullOrEmpty(m_report_code) Then
      PrintData.SetFilter(txt_code.Text, m_report_code)
    End If

    ' Enabled Filter
    If Not String.IsNullOrEmpty(m_report_enabled) Then
      PrintData.SetFilter(gb_enabled.Text, m_report_enabled)
    End If

    ' Integrated Cashier 
    If Not String.IsNullOrEmpty(m_report_cashier) Then
      PrintData.SetFilter(gb_cashier.Text, m_report_cashier)
    End If

    ' Gaming Tables Types Filter
    If Not String.IsNullOrEmpty(m_report_types) Then
      PrintData.SetFilter(gb_table_type.Text, m_report_types)
    End If

    ' Location Filter
    If Not String.IsNullOrEmpty(m_report_area_name) Then
      PrintData.SetFilter(cmb_area.Text, m_report_area_name)
      PrintData.SetFilter(cmb_bank.Text, m_report_bank_name)
    End If

    ' Floor id Filter
    If Not String.IsNullOrEmpty(m_report_floor_id) Then
      PrintData.SetFilter(ef_floor_id.Text, m_report_floor_id)
    End If

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_report_name = String.Empty
    m_report_code = String.Empty
    m_report_enabled = String.Empty
    m_report_cashier = String.Empty
    m_report_area_name = String.Empty
    m_report_bank_name = String.Empty
    m_report_floor_id = String.Empty
    m_report_types = String.Empty

    ' Name filter
    If Not String.IsNullOrEmpty(Me.txt_name.Value) Then
      m_report_name = Me.txt_name.Value
    End If

    ' Code filter
    If Not String.IsNullOrEmpty(Me.txt_code.Value) Then
      m_report_code = Me.txt_code.Value
    End If

    ' Enabled filter
    If Me.chk_yes.Checked Then
      m_report_enabled = Me.chk_yes.Text
    End If

    If Me.chk_no.Checked Then
      If String.IsNullOrEmpty(m_report_enabled) Then
        m_report_enabled = Me.chk_no.Text
      Else
        m_report_enabled = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353) ' All
      End If
    End If

    ' Cashier filter
    If Me.chk_integrated.Checked Then
      m_report_cashier = Me.chk_integrated.Text
    End If

    If Me.chk_not_integrated.Checked Then
      If String.IsNullOrEmpty(m_report_cashier) Then
        m_report_cashier = Me.chk_not_integrated.Text
      Else
        m_report_cashier = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353) ' All
      End If
    End If

    ' Location filter
    If Me.cmb_area.SelectedIndex > 0 Then
      m_report_area_name = Me.cmb_area.TextValue
      m_report_bank_name = Me.cmb_bank.TextValue
    End If

    ' FloorId filter
    If Not String.IsNullOrEmpty(Me.ef_floor_id.Value) Then
      m_report_floor_id = Me.ef_floor_id.Value
    End If

    ' Gaming Table Types filter
    If Me.opt_all_types.Checked Then
      m_report_types = Me.opt_all_types.Text
    ElseIf Me.opt_several_types.Checked Then
      m_report_types = GetTypeIdListSelected(True)
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports


#Region " Private Functions"


  ' PURPOSE: Add Handles
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Private Sub AddHandles()

    AddHandler cmb_area.ValueChangedEvent, AddressOf cmb_area_ValueChangedEvent

  End Sub ' AddHandles

  ' PURPOSE: Option Button checked changed
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Private Sub opt_all_types_CheckedChanged()
    Dim idx As Integer

    For idx = 0 To dg_filter.NumRows - 1
      Me.dg_filter.Cell(idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    Next
  End Sub

  ' PURPOSE: Area selection
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Private Sub cmb_area_ValueChangedEvent()

    'Zone (smoking - no smoking)
    ef_smoking.Value = GetZoneDescription(cmb_area.Value)

    'Banks
    Call BankComboFill()

  End Sub
  ' PURPOSE: Returns zone description
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Private Function GetZoneDescription(ByVal AreaId As Integer) As String
    Dim _table As DataTable
    Dim _returnValue As String

    _table = GUI_GetTableUsingSQL("SELECT AR_SMOKING FROM AREAS WHERE AR_AREA_ID = " & AreaId.ToString(), Integer.MaxValue)

    _returnValue = GLB_NLS_GUI_CONFIGURATION.GetString(437)

    If Not IsNothing(_table) Then
      If _table.Rows.Count() > 0 Then
        If _table.Rows.Item(0).Item("AR_SMOKING") Then
          _returnValue = GLB_NLS_GUI_CONFIGURATION.GetString(436)
        End If
      End If
    End If

    Return _returnValue

  End Function

  ' PURPOSE: Init areas combo box
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Private Sub AreaComboFill()
    Dim _table As DataTable

    _table = GUI_GetTableUsingSQL("SELECT AR_AREA_ID, AR_NAME FROM AREAS INNER JOIN BANKS ON BK_AREA_ID = AR_AREA_ID GROUP BY AR_AREA_ID, AR_NAME ORDER BY AR_NAME", Integer.MaxValue)

    Me.cmb_area.Clear()
    Me.cmb_area.Add(_table)

  End Sub     ' AreaComboFill

  ' PURPOSE: Init BANKS combo box
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Private Sub BankComboFill()
    Dim _table As DataTable

    _table = GUI_GetTableUsingSQL("SELECT BK_BANK_ID, BK_NAME FROM BANKS WHERE BK_AREA_ID = " & cmb_area.Value & " GROUP BY BK_BANK_ID, BK_NAME ORDER BY BK_NAME", Integer.MaxValue)

    Me.cmb_bank.Clear()
    Me.cmb_bank.Add(_table)

  End Sub     ' BankComboFill

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub GUI_StyleSheet()

    ' Grid StyleSheet
    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, False)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
      .Sortable = True

      ' SENTINEL
      .Column(GRID_COLUMN_SENTINEL).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_SENTINEL).Width = GRID_COLUMN_SENTINEL_WIDTH
      .Column(GRID_COLUMN_SENTINEL).HighLightWhenSelected = False
      .Column(GRID_COLUMN_SENTINEL).IsColumnPrintable = False

      ' GAME TABLE CODE
      .Column(GRID_COLUMN_GAME_TABLE_CODE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3418)
      .Column(GRID_COLUMN_GAME_TABLE_CODE).Width = GRID_COLUMN_GAME_TABLE_CODE_WIDTH '1560
      .Column(GRID_COLUMN_GAME_TABLE_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' GAME TABLE TYPE
      .Column(GRID_COLUMN_GAME_TABLE_TYPE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3420)
      .Column(GRID_COLUMN_GAME_TABLE_TYPE).Width = GRID_COLUMN_GAME_TABLE_TYPE_WIDTH '1800
      .Column(GRID_COLUMN_GAME_TABLE_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' GAME TABLE NAME
      .Column(GRID_COLUMN_GAME_TABLE_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3419)
      .Column(GRID_COLUMN_GAME_TABLE_NAME).Width = GRID_COLUMN_GAME_TABLE_NAME_WIDTH '2200
      .Column(GRID_COLUMN_GAME_TABLE_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' GAME TABLE AREA
      .Column(GRID_COLUMN_GAME_TABLE_AREA).Header(0).Text = GLB_NLS_GUI_CONFIGURATION.GetString(435)
      .Column(GRID_COLUMN_GAME_TABLE_AREA).Width = GRID_COLUMN_GAME_TABLE_AREA_WIDTH '1500
      .Column(GRID_COLUMN_GAME_TABLE_AREA).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' GAME TABLE ZONE
      .Column(GRID_COLUMN_GAME_TABLE_ZONE).Header(0).Text = GLB_NLS_GUI_CONFIGURATION.GetString(430)
      .Column(GRID_COLUMN_GAME_TABLE_ZONE).Width = GRID_COLUMN_GAME_TABLE_ZONE_WIDTH '1500
      .Column(GRID_COLUMN_GAME_TABLE_ZONE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' GAME TABLE BANK
      .Column(GRID_COLUMN_GAME_TABLE_BANK).Header(0).Text = GLB_NLS_GUI_CONFIGURATION.GetString(431)
      .Column(GRID_COLUMN_GAME_TABLE_BANK).Width = GRID_COLUMN_GAME_TABLE_BANK_WIDTH '1500
      .Column(GRID_COLUMN_GAME_TABLE_BANK).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' GAME TABLE FLOOR_ID
      .Column(GRID_COLUMN_GAME_TABLE_FLOOR).Header(0).Text = GLB_NLS_GUI_CONFIGURATION.GetString(432)
      .Column(GRID_COLUMN_GAME_TABLE_FLOOR).Width = GRID_COLUMN_GAME_TABLE_FLOOR_WIDTH '1500
      .Column(GRID_COLUMN_GAME_TABLE_FLOOR).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' GAME TABLE ENABLED
      .Column(GRID_COLUMN_GAME_TABLE_ENABLED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3422)
      .Column(GRID_COLUMN_GAME_TABLE_ENABLED).Width = GRID_COLUMN_GAME_TABLE_ENABLED_WIDTH '1100
      .Column(GRID_COLUMN_GAME_TABLE_ENABLED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' GAME TABLE INTEGRATED CASHIER
      .Column(GRID_COLUMN_GAME_TABLE_INTEGRATED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3424)
      .Column(GRID_COLUMN_GAME_TABLE_INTEGRATED).Width = GRID_COLUMN_GAME_TABLE_INTEGRATED_WIDTH '2200
      .Column(GRID_COLUMN_GAME_TABLE_INTEGRATED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      If WSI.Common.GamingTableBusinessLogic.IsEnabledGTPlayerTracking() AndAlso _
         WSI.Common.GamingTableBusinessLogic.GamingTablesMode() = WSI.Common.GamingTableBusinessLogic.GT_MODE.GUI_AND_CASHIER Then
        ' GAME TABLE NUM SEATS
        .Column(GRID_COLUMN_GAME_TABLE_NUM_SEATS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4955)
        .Column(GRID_COLUMN_GAME_TABLE_NUM_SEATS).Width = GRID_COLUMN_GAME_TABLE_NUM_SEATS_WIDTH
        .Column(GRID_COLUMN_GAME_TABLE_NUM_SEATS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

        ' GAME TABLE PROVIDER ID
        .Column(GRID_COLUMN_GAME_TABLE_PROVIDER_ID).Header(0).Text = String.Empty
        .Column(GRID_COLUMN_GAME_TABLE_PROVIDER_ID).Width = GRID_COLUMN_GAME_TABLE_PROVIDER_ID_WIDTH
        .Column(GRID_COLUMN_GAME_TABLE_PROVIDER_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

        ' GAME TABLE PROVIDER NAME
        .Column(GRID_COLUMN_GAME_TABLE_PROVIDER_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5370)
        .Column(GRID_COLUMN_GAME_TABLE_PROVIDER_NAME).Width = GRID_COLUMN_GAME_TABLE_PROVIDER_NAME_WIDTH
        .Column(GRID_COLUMN_GAME_TABLE_PROVIDER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      Else
        ' GAME TABLE NUM SEATS
        .Column(GRID_COLUMN_GAME_TABLE_NUM_SEATS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4955)
        .Column(GRID_COLUMN_GAME_TABLE_NUM_SEATS).Width = 0
        .Column(GRID_COLUMN_GAME_TABLE_NUM_SEATS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

        ' GAME TABLE PROVIDER ID
        .Column(GRID_COLUMN_GAME_TABLE_PROVIDER_ID).Header(0).Text = String.Empty
        .Column(GRID_COLUMN_GAME_TABLE_PROVIDER_ID).Width = 0
        .Column(GRID_COLUMN_GAME_TABLE_PROVIDER_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

        ' GAME TABLE PROVIDER NAME
        .Column(GRID_COLUMN_GAME_TABLE_PROVIDER_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5370)
        .Column(GRID_COLUMN_GAME_TABLE_PROVIDER_NAME).Width = 0
        .Column(GRID_COLUMN_GAME_TABLE_PROVIDER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      End If

      ' GAME TABLE ID (No Visible)
      .Column(GRID_COLUMN_GAME_TABLE_ID).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_GAME_TABLE_ID).Width = GRID_COLUMN_GAME_TABLE_ID_WIDTH '0
      .Column(GRID_COLUMN_GAME_TABLE_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub SetDefaultValues()

    ' Fields
    txt_code.Value = String.Empty
    txt_name.Value = String.Empty

    ' Cashier Integrated
    chk_integrated.Checked = True
    chk_not_integrated.Checked = True

    ' Enabled
    chk_yes.Checked = True
    chk_no.Checked = False

    ' Movement Types Filter Data Grid
    Call InitTableTypesFilter()

    ' Location
    cmb_area.SelectedIndex = 0
    cmb_bank.SelectedIndex = 0
    ef_floor_id.Value = String.Empty
    ef_smoking.Value = String.Empty

  End Sub ' SetDefaultValues

#Region "Table Types"

  ' PURPOSE: Define the layout of the movement types grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheetTableTypes()
    With Me.dg_filter
      Call .Init(GRID_2_COLUMNS, 0, Editable:=True)
      .Counter(0).Visible = False
      .Sortable = False

      ' Hidden columns
      .Column(GRID_2_COLUMN_CODE).Header.Text = String.Empty
      .Column(GRID_2_COLUMN_CODE).WidthFixed = 0

      ' GRID_COL_CHECKBOX
      .Column(GRID_2_COLUMN_CHECKED).Header.Text = String.Empty
      .Column(GRID_2_COLUMN_CHECKED).WidthFixed = 400
      .Column(GRID_2_COLUMN_CHECKED).Fixed = True
      .Column(GRID_2_COLUMN_CHECKED).Editable = True
      .Column(GRID_2_COLUMN_CHECKED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX

      ' GRID_COL_DESCRIPTION
      .Column(GRID_2_COLUMN_DESC).Header.Text = String.Empty
      .Column(GRID_2_COLUMN_DESC).WidthFixed = 2351
      .Column(GRID_2_COLUMN_DESC).Fixed = True
      .Column(GRID_2_COLUMN_DESC).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' GRID_COL_TYPE
      .Column(GRID_2_COLUMN_ROW_TYPE).Header.Text = String.Empty
      .Column(GRID_2_COLUMN_ROW_TYPE).WidthFixed = 0
    End With

  End Sub 'GUI_StyleSheetTableTypes

  ' PURPOSE: Initialize the movement types filter grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub InitTableTypesFilter()

    Me.opt_all_types.Checked = True
    Me.dg_filter.Enabled = True

    ' Reset dg selections
    Call FillTableTypesFilterGrid(GetTableTypesFilterGridData())

  End Sub ' InitTableTypesFilter

  ' PURPOSE : Adds a new gaming table to the system
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Sub GUI_NewItemFromSelected()

    Dim idx_row As Int32
    Dim gaming_table_id As Integer

    ' Search the first row selected
    For idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(idx_row).IsSelected Then
        Exit For
      End If
    Next

    If idx_row = Me.Grid.NumRows Then
      Return
    End If

    ' Get the GamingTable ID, and launch the editor
    gaming_table_id = Me.Grid.Cell(idx_row, GRID_COLUMN_GAME_TABLE_ID).Value

    Call GUI_ShowNewGamingTableForm(gaming_table_id)

  End Sub ' NewPromotion

  ' PURPOSE : Adds a new promotion to the system
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Sub GUI_ShowNewGamingTableForm(Optional ByVal GamingTableCopiedId As Long = 0)

    Dim frm_edit As frm_gaming_tables_edit
    frm_edit = New frm_gaming_tables_edit()
    frm_edit.ShowNewItem(GamingTableCopiedId)
    frm_edit = Nothing

    Call Me.Grid.Focus()

  End Sub ' NewPromotion


  ' PURPOSE: Populate the movement types data grid filter.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub FillTableTypesFilterGrid(ByVal DgList As List(Of TYPE_DG_FILTER_ELEMENT))

    Me.dg_filter.Clear()
    Me.dg_filter.Redraw = False

    ' Get the filters array and insert it into the grid.
    For Each _element As TYPE_DG_FILTER_ELEMENT In DgList
      Call AddOneRowData(_element.code, _element.description)
    Next

    Me.dg_filter.Redraw = True

  End Sub ' FillTableTypesFilterGrid


  ' PURPOSE: Obtain the list of elements that will populate the 
  '         movement types filter grid.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '     - List of elements to be added to the grid
  Private Function GetTableTypesFilterGridData() As List(Of TYPE_DG_FILTER_ELEMENT)

    ' Dimensioned this way to avoid getting an extra element: VB uses the constant to set the upper value, 
    ' not the number of elements
    Dim _table_types As DataTable
    Dim _row As TYPE_DG_FILTER_ELEMENT
    Dim _dg_filter_rows_list As List(Of TYPE_DG_FILTER_ELEMENT)

    _dg_filter_rows_list = New List(Of TYPE_DG_FILTER_ELEMENT)

    _table_types = GetTableTypes()

    For Each _dr As DataRow In _table_types.Rows
      _row = New TYPE_DG_FILTER_ELEMENT
      _row.code = _dr("GTT_GAMING_TABLE_TYPE_ID")
      _row.description = _dr("GTT_NAME")
      _row.elem_type = 1
      _dg_filter_rows_list.Add(_row)
    Next

    Return _dg_filter_rows_list

  End Function ' GetTableTypesFilterGridData

  ' PURPOSE: Get the list of the items selected in the filter data grid
  '          Format list to build query: X, X, X
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String contining id's of the selected rows
  Private Function GetTypeIdListSelected(Optional ByVal GetNames As Boolean = False) As String
    Dim _idx_row As Integer
    Dim _list_mov_types As String
    Dim _field As String

    _list_mov_types = String.Empty

    For _idx_row = 0 To Me.dg_filter.NumRows - 1

      _field = Me.dg_filter.Cell(_idx_row, GRID_2_COLUMN_CODE).Value

      If GetNames Then
        _field = Me.dg_filter.Cell(_idx_row, GRID_2_COLUMN_DESC).Value
      End If

      If dg_filter.Cell(_idx_row, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED And _field <> "" Then
        If _list_mov_types.Length = 0 Then
          _list_mov_types = _field
        Else
          _list_mov_types = _list_mov_types & ", " & _field
        End If
      End If
    Next

    Return _list_mov_types
  End Function 'GetTypeIdListSelected


  ' PURPOSE: Update the checkbox state for all row dependants (for header rows)
  '
  '  PARAMS:
  '     - INPUT:
  '           - FirstRow
  '           - ValueChecked
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ChangeCheckofRows(ByVal FirstRow As Integer, ByVal ValueChecked As String)
    Dim idx As Integer

    For idx = FirstRow + 1 To dg_filter.NumRows - 1

      Me.dg_filter.Cell(idx, GRID_2_COLUMN_CHECKED).Value = ValueChecked

    Next

  End Sub ' ChangeCheckofRows

  ' PURPOSE: Returns a string with the description of all the selected movement types
  '
  '  PARAMS:
  '     - INPUT:
  '           - 
  '     - OUTPUT:
  '           - SelectedMovements
  '
  ' RETURNS:
  '     - None
  Private Sub GetSelectedTableTypes(ByRef SelectedMovements As String)
    Dim idx As Integer

    SelectedMovements = String.Empty

    For idx = 0 To dg_filter.NumRows - 1
      If Me.dg_filter.Cell(idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        If SelectedMovements = String.Empty Then
          SelectedMovements = Me.dg_filter.Cell(idx, GRID_2_COLUMN_DESC).Value.Trim()
        Else
          SelectedMovements &= ", " & Me.dg_filter.Cell(idx, GRID_2_COLUMN_DESC).Value.Trim()
        End If
      End If
    Next
  End Sub


  ' PURPOSE: Add a new data row at the data grid filter
  '
  '  PARAMS:
  '     - INPUT:
  '           - TypeCode: numeric identifier
  '           - TypeDesc: textual description
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub AddOneRowData(ByVal TypeCode As Integer, ByVal TypeDesc As String)
    Dim prev_redraw As Boolean

    prev_redraw = Me.dg_filter.Redraw
    Me.dg_filter.Redraw = False

    dg_filter.AddRow()

    Me.dg_filter.Redraw = False

    dg_filter.Cell(dg_filter.NumRows - 1, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
    dg_filter.Cell(dg_filter.NumRows - 1, GRID_2_COLUMN_CODE).Value = TypeCode
    dg_filter.Cell(dg_filter.NumRows - 1, GRID_2_COLUMN_DESC).Value = TypeDesc
    dg_filter.Cell(dg_filter.NumRows - 1, GRID_2_COLUMN_ROW_TYPE).Value = 1 ' Row type Data. 

    Me.dg_filter.Redraw = prev_redraw
  End Sub ' AddOneRowData


#End Region ' Table Types

#End Region ' Private 

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

  ' PURPOSE: Call to child window to show design for the selected row
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub GUI_ShowSelectedGamingTableDesign()
    Dim _idx_row As Integer
    Dim _gaming_table_id As Integer

    Dim _frm_gaming_table_design As frm_gaming_table_design

    ' Check that it can show the information
    If Not GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Enabled Then
      Return
    End If

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    ' Protect totalizator row
    If Not IsValidDataRow(_idx_row) Then
      Return
    End If

    If Me.Grid.Cell(_idx_row, GRID_COLUMN_GAME_TABLE_PROVIDER_NAME).Value = String.Empty Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5516), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK)
      Return
    End If

    If Me.Grid.Cell(_idx_row, GRID_COLUMN_GAME_TABLE_ID).Value = "" Then
      _gaming_table_id = 0
    Else
      _gaming_table_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_GAME_TABLE_ID).Value
    End If

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm_gaming_table_design = New frm_gaming_table_design()
    Call _frm_gaming_table_design.ShowEditItem(_gaming_table_id)

  End Sub

  Private Function IsValidDataRow(ByVal IdxRow As Integer) As Boolean

    ' Skip Totalizator row!!!
    Return (Me.Grid.Cell(IdxRow, GRID_COLUMN_GAME_TABLE_ID).Value <> "")

  End Function ' IsValidDataRow

#End Region 'Public Functions

#Region " Events "

  Private Sub dg_types_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer) Handles dg_filter.CellDataChangedEvent

    Dim _idx As Integer
    Dim _all_checkeds As Boolean

    m_several_click = False
    _all_checkeds = True

    For _idx = 0 To dg_filter.NumRows - 1
      If Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED Then
        _all_checkeds = False
        Exit For
      End If
    Next

    If _all_checkeds Then
      opt_several_types.Checked = False
      opt_all_types.Checked = True
    Else
      opt_several_types.Checked = True
      opt_all_types.Checked = False
    End If

  End Sub ' dg_types_CellDataChangedEvent

  Private Sub opt_all_types_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_all_types.CheckedChanged
    Dim _idx As Integer
    Dim _check_value As String

    If opt_all_types.Checked Then
      _check_value = uc_grid.GRID_CHK_CHECKED
    Else
      _check_value = uc_grid.GRID_CHK_UNCHECKED
    End If

    If opt_all_types.Checked Or m_several_click Then
      For _idx = 0 To dg_filter.NumRows - 1
        Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = _check_value
      Next
    End If

    m_several_click = True

  End Sub

#End Region ' Events

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

  End Sub

End Class