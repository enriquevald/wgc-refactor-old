'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_command_history
' DESCRIPTION:   This screen allows to view the commands history between:
'                           - two dates 
'                           - for all, severals or one terminal
'                           - for all, severals or one command
'                           - for all, severals or one status
' AUTHOR:        Ra�l Cervera
' CREATION DATE: 15-JUN-2010
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 15-JUN-2010  RCI    Initial version
' 27-FEB-2012  MPO    Defect #189: The control terminals, only it must view the type terminals WIN (1), SAS_HOST (5), SITE_JACKPOT (101) and MOBILE_BANK
' 25-APR-2013  RBG    #746 It makes no sense to work in this form with multiselection
' 30-SEP-2014  LEM    Fixed Bug WIG-1340: Option "Show terminal location" do not work correctly.
' 12-NOV-2014  FJC    Don't show Command: 'SITE_JACKPOT_AWARDED' in the grid
' 30-JAN-2015  ANM    Increased form's width due there is a new column in grid
' 26-MAR-2015  ANM    Calling GetProviderIdListSelected always returns a query
' 16-AUG-2017  MS     Bug 29379:[WIGOS-3650] Missing selection criterion in exported file in Commands history
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports WSI.Common

Public Class frm_command_history
  Inherits frm_base_sel


#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents gb_created As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_created_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_created_from As GUI_Controls.uc_date_picker
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents chk_cmd_restart As System.Windows.Forms.CheckBox
  Friend WithEvents chk_cmd_logger As System.Windows.Forms.CheckBox
  Friend WithEvents chk_status_pending_send As System.Windows.Forms.CheckBox
  Friend WithEvents chk_status_timeout_disconnected As System.Windows.Forms.CheckBox
  Friend WithEvents chk_status_replied_error As System.Windows.Forms.CheckBox
  Friend WithEvents chk_status_replied_ok As System.Windows.Forms.CheckBox
  Friend WithEvents chk_status_pending_reply As System.Windows.Forms.CheckBox
  Friend WithEvents chk_status_timeout_not_reply As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_timeout_disconnected As System.Windows.Forms.Label
  Friend WithEvents lbl_timeout_not_reply As System.Windows.Forms.Label
  Friend WithEvents lbl_replied_error As System.Windows.Forms.Label
  Friend WithEvents lbl_replied_ok As System.Windows.Forms.Label
  Friend WithEvents lbl_pending_reply As System.Windows.Forms.Label
  Friend WithEvents lbl_pending_send As System.Windows.Forms.Label
  Friend WithEvents btn_uncheck_all As GUI_Controls.uc_button
  Friend WithEvents btn_check_all As GUI_Controls.uc_button
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox
  Friend WithEvents gb_command As System.Windows.Forms.GroupBox
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.gb_created = New System.Windows.Forms.GroupBox()
    Me.dtp_created_to = New GUI_Controls.uc_date_picker()
    Me.dtp_created_from = New GUI_Controls.uc_date_picker()
    Me.gb_status = New System.Windows.Forms.GroupBox()
    Me.btn_uncheck_all = New GUI_Controls.uc_button()
    Me.btn_check_all = New GUI_Controls.uc_button()
    Me.lbl_timeout_disconnected = New System.Windows.Forms.Label()
    Me.lbl_timeout_not_reply = New System.Windows.Forms.Label()
    Me.lbl_replied_error = New System.Windows.Forms.Label()
    Me.lbl_replied_ok = New System.Windows.Forms.Label()
    Me.lbl_pending_reply = New System.Windows.Forms.Label()
    Me.lbl_pending_send = New System.Windows.Forms.Label()
    Me.chk_status_timeout_not_reply = New System.Windows.Forms.CheckBox()
    Me.chk_status_timeout_disconnected = New System.Windows.Forms.CheckBox()
    Me.chk_status_replied_error = New System.Windows.Forms.CheckBox()
    Me.chk_status_replied_ok = New System.Windows.Forms.CheckBox()
    Me.chk_status_pending_reply = New System.Windows.Forms.CheckBox()
    Me.chk_status_pending_send = New System.Windows.Forms.CheckBox()
    Me.uc_pr_list = New GUI_Controls.uc_provider()
    Me.gb_command = New System.Windows.Forms.GroupBox()
    Me.chk_cmd_restart = New System.Windows.Forms.CheckBox()
    Me.chk_cmd_logger = New System.Windows.Forms.CheckBox()
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_created.SuspendLayout()
    Me.gb_status.SuspendLayout()
    Me.gb_command.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.chk_terminal_location)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Controls.Add(Me.gb_created)
    Me.panel_filter.Controls.Add(Me.gb_command)
    Me.panel_filter.Controls.Add(Me.gb_status)
    Me.panel_filter.Size = New System.Drawing.Size(956, 195)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_status, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_command, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_created, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_terminal_location, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 199)
    Me.panel_data.Size = New System.Drawing.Size(956, 519)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(950, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(950, 4)
    '
    'gb_created
    '
    Me.gb_created.Controls.Add(Me.dtp_created_to)
    Me.gb_created.Controls.Add(Me.dtp_created_from)
    Me.gb_created.Location = New System.Drawing.Point(8, 8)
    Me.gb_created.Name = "gb_created"
    Me.gb_created.Size = New System.Drawing.Size(250, 72)
    Me.gb_created.TabIndex = 0
    Me.gb_created.TabStop = False
    Me.gb_created.Text = "xCreated"
    '
    'dtp_created_to
    '
    Me.dtp_created_to.Checked = True
    Me.dtp_created_to.IsReadOnly = False
    Me.dtp_created_to.Location = New System.Drawing.Point(6, 38)
    Me.dtp_created_to.Name = "dtp_created_to"
    Me.dtp_created_to.ShowCheckBox = True
    Me.dtp_created_to.ShowUpDown = False
    Me.dtp_created_to.Size = New System.Drawing.Size(240, 25)
    Me.dtp_created_to.SufixText = "Sufix Text"
    Me.dtp_created_to.SufixTextVisible = True
    Me.dtp_created_to.TabIndex = 1
    Me.dtp_created_to.TextWidth = 50
    Me.dtp_created_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_created_from
    '
    Me.dtp_created_from.Checked = True
    Me.dtp_created_from.IsReadOnly = False
    Me.dtp_created_from.Location = New System.Drawing.Point(6, 14)
    Me.dtp_created_from.Name = "dtp_created_from"
    Me.dtp_created_from.ShowCheckBox = True
    Me.dtp_created_from.ShowUpDown = False
    Me.dtp_created_from.Size = New System.Drawing.Size(240, 25)
    Me.dtp_created_from.SufixText = "Sufix Text"
    Me.dtp_created_from.SufixTextVisible = True
    Me.dtp_created_from.TabIndex = 0
    Me.dtp_created_from.TextWidth = 50
    Me.dtp_created_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'gb_status
    '
    Me.gb_status.Controls.Add(Me.btn_uncheck_all)
    Me.gb_status.Controls.Add(Me.btn_check_all)
    Me.gb_status.Controls.Add(Me.lbl_timeout_disconnected)
    Me.gb_status.Controls.Add(Me.lbl_timeout_not_reply)
    Me.gb_status.Controls.Add(Me.lbl_replied_error)
    Me.gb_status.Controls.Add(Me.lbl_replied_ok)
    Me.gb_status.Controls.Add(Me.lbl_pending_reply)
    Me.gb_status.Controls.Add(Me.lbl_pending_send)
    Me.gb_status.Controls.Add(Me.chk_status_timeout_not_reply)
    Me.gb_status.Controls.Add(Me.chk_status_timeout_disconnected)
    Me.gb_status.Controls.Add(Me.chk_status_replied_error)
    Me.gb_status.Controls.Add(Me.chk_status_replied_ok)
    Me.gb_status.Controls.Add(Me.chk_status_pending_reply)
    Me.gb_status.Controls.Add(Me.chk_status_pending_send)
    Me.gb_status.Location = New System.Drawing.Point(596, 8)
    Me.gb_status.Name = "gb_status"
    Me.gb_status.Size = New System.Drawing.Size(201, 180)
    Me.gb_status.TabIndex = 4
    Me.gb_status.TabStop = False
    Me.gb_status.Text = "xStatus"
    '
    'btn_uncheck_all
    '
    Me.btn_uncheck_all.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_uncheck_all.Location = New System.Drawing.Point(100, 147)
    Me.btn_uncheck_all.Name = "btn_uncheck_all"
    Me.btn_uncheck_all.Size = New System.Drawing.Size(90, 30)
    Me.btn_uncheck_all.TabIndex = 7
    Me.btn_uncheck_all.ToolTipped = False
    Me.btn_uncheck_all.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.USER
    '
    'btn_check_all
    '
    Me.btn_check_all.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_check_all.Location = New System.Drawing.Point(5, 147)
    Me.btn_check_all.Name = "btn_check_all"
    Me.btn_check_all.Size = New System.Drawing.Size(90, 30)
    Me.btn_check_all.TabIndex = 6
    Me.btn_check_all.ToolTipped = False
    Me.btn_check_all.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.USER
    '
    'lbl_timeout_disconnected
    '
    Me.lbl_timeout_disconnected.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_timeout_disconnected.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_timeout_disconnected.Location = New System.Drawing.Point(6, 128)
    Me.lbl_timeout_disconnected.Name = "lbl_timeout_disconnected"
    Me.lbl_timeout_disconnected.Size = New System.Drawing.Size(16, 16)
    Me.lbl_timeout_disconnected.TabIndex = 112
    '
    'lbl_timeout_not_reply
    '
    Me.lbl_timeout_not_reply.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_timeout_not_reply.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_timeout_not_reply.Location = New System.Drawing.Point(6, 106)
    Me.lbl_timeout_not_reply.Name = "lbl_timeout_not_reply"
    Me.lbl_timeout_not_reply.Size = New System.Drawing.Size(16, 16)
    Me.lbl_timeout_not_reply.TabIndex = 111
    '
    'lbl_replied_error
    '
    Me.lbl_replied_error.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_replied_error.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_replied_error.Location = New System.Drawing.Point(6, 84)
    Me.lbl_replied_error.Name = "lbl_replied_error"
    Me.lbl_replied_error.Size = New System.Drawing.Size(16, 16)
    Me.lbl_replied_error.TabIndex = 110
    '
    'lbl_replied_ok
    '
    Me.lbl_replied_ok.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_replied_ok.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_replied_ok.Location = New System.Drawing.Point(6, 62)
    Me.lbl_replied_ok.Name = "lbl_replied_ok"
    Me.lbl_replied_ok.Size = New System.Drawing.Size(16, 16)
    Me.lbl_replied_ok.TabIndex = 109
    '
    'lbl_pending_reply
    '
    Me.lbl_pending_reply.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_pending_reply.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_pending_reply.Location = New System.Drawing.Point(6, 40)
    Me.lbl_pending_reply.Name = "lbl_pending_reply"
    Me.lbl_pending_reply.Size = New System.Drawing.Size(16, 16)
    Me.lbl_pending_reply.TabIndex = 108
    '
    'lbl_pending_send
    '
    Me.lbl_pending_send.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_pending_send.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_pending_send.Location = New System.Drawing.Point(6, 18)
    Me.lbl_pending_send.Name = "lbl_pending_send"
    Me.lbl_pending_send.Size = New System.Drawing.Size(16, 16)
    Me.lbl_pending_send.TabIndex = 107
    '
    'chk_status_timeout_not_reply
    '
    Me.chk_status_timeout_not_reply.AutoSize = True
    Me.chk_status_timeout_not_reply.Location = New System.Drawing.Point(28, 106)
    Me.chk_status_timeout_not_reply.Name = "chk_status_timeout_not_reply"
    Me.chk_status_timeout_not_reply.Size = New System.Drawing.Size(130, 17)
    Me.chk_status_timeout_not_reply.TabIndex = 4
    Me.chk_status_timeout_not_reply.Text = "xTimeoutNotReply"
    Me.chk_status_timeout_not_reply.UseVisualStyleBackColor = True
    '
    'chk_status_timeout_disconnected
    '
    Me.chk_status_timeout_disconnected.AutoSize = True
    Me.chk_status_timeout_disconnected.Location = New System.Drawing.Point(28, 128)
    Me.chk_status_timeout_disconnected.Name = "chk_status_timeout_disconnected"
    Me.chk_status_timeout_disconnected.Size = New System.Drawing.Size(155, 17)
    Me.chk_status_timeout_disconnected.TabIndex = 5
    Me.chk_status_timeout_disconnected.Text = "xTimeoutDisconnected"
    Me.chk_status_timeout_disconnected.UseVisualStyleBackColor = True
    '
    'chk_status_replied_error
    '
    Me.chk_status_replied_error.AutoSize = True
    Me.chk_status_replied_error.Location = New System.Drawing.Point(28, 84)
    Me.chk_status_replied_error.Name = "chk_status_replied_error"
    Me.chk_status_replied_error.Size = New System.Drawing.Size(104, 17)
    Me.chk_status_replied_error.TabIndex = 3
    Me.chk_status_replied_error.Text = "xRepliedError"
    Me.chk_status_replied_error.UseVisualStyleBackColor = True
    '
    'chk_status_replied_ok
    '
    Me.chk_status_replied_ok.AutoSize = True
    Me.chk_status_replied_ok.Location = New System.Drawing.Point(28, 62)
    Me.chk_status_replied_ok.Name = "chk_status_replied_ok"
    Me.chk_status_replied_ok.Size = New System.Drawing.Size(91, 17)
    Me.chk_status_replied_ok.TabIndex = 2
    Me.chk_status_replied_ok.Text = "xRepliedOk"
    Me.chk_status_replied_ok.UseVisualStyleBackColor = True
    '
    'chk_status_pending_reply
    '
    Me.chk_status_pending_reply.AutoSize = True
    Me.chk_status_pending_reply.Location = New System.Drawing.Point(28, 40)
    Me.chk_status_pending_reply.Name = "chk_status_pending_reply"
    Me.chk_status_pending_reply.Size = New System.Drawing.Size(110, 17)
    Me.chk_status_pending_reply.TabIndex = 1
    Me.chk_status_pending_reply.Text = "xPendingReply"
    Me.chk_status_pending_reply.UseVisualStyleBackColor = True
    '
    'chk_status_pending_send
    '
    Me.chk_status_pending_send.AutoSize = True
    Me.chk_status_pending_send.Location = New System.Drawing.Point(28, 18)
    Me.chk_status_pending_send.Name = "chk_status_pending_send"
    Me.chk_status_pending_send.Size = New System.Drawing.Size(107, 17)
    Me.chk_status_pending_send.TabIndex = 0
    Me.chk_status_pending_send.Text = "xPendingSend"
    Me.chk_status_pending_send.UseVisualStyleBackColor = True
    '
    'uc_pr_list
    '
    Me.uc_pr_list.FilterByCurrency = False
    Me.uc_pr_list.Location = New System.Drawing.Point(263, 4)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(337, 191)
    Me.uc_pr_list.TabIndex = 3
    Me.uc_pr_list.TerminalListHasValues = False
    '
    'gb_command
    '
    Me.gb_command.Controls.Add(Me.chk_cmd_restart)
    Me.gb_command.Controls.Add(Me.chk_cmd_logger)
    Me.gb_command.Location = New System.Drawing.Point(8, 86)
    Me.gb_command.Name = "gb_command"
    Me.gb_command.Size = New System.Drawing.Size(250, 72)
    Me.gb_command.TabIndex = 1
    Me.gb_command.TabStop = False
    Me.gb_command.Text = "xCommand"
    '
    'chk_cmd_restart
    '
    Me.chk_cmd_restart.AutoSize = True
    Me.chk_cmd_restart.Location = New System.Drawing.Point(6, 44)
    Me.chk_cmd_restart.Name = "chk_cmd_restart"
    Me.chk_cmd_restart.Size = New System.Drawing.Size(74, 17)
    Me.chk_cmd_restart.TabIndex = 1
    Me.chk_cmd_restart.Text = "xRestart"
    Me.chk_cmd_restart.UseVisualStyleBackColor = True
    '
    'chk_cmd_logger
    '
    Me.chk_cmd_logger.AutoSize = True
    Me.chk_cmd_logger.Location = New System.Drawing.Point(6, 21)
    Me.chk_cmd_logger.Name = "chk_cmd_logger"
    Me.chk_cmd_logger.Size = New System.Drawing.Size(72, 17)
    Me.chk_cmd_logger.TabIndex = 0
    Me.chk_cmd_logger.Text = "xLogger"
    Me.chk_cmd_logger.UseVisualStyleBackColor = True
    '
    'chk_terminal_location
    '
    Me.chk_terminal_location.Location = New System.Drawing.Point(14, 165)
    Me.chk_terminal_location.Name = "chk_terminal_location"
    Me.chk_terminal_location.Size = New System.Drawing.Size(238, 20)
    Me.chk_terminal_location.TabIndex = 2
    Me.chk_terminal_location.Text = "xShow terminals location"
    '
    'frm_command_history
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(964, 722)
    Me.Name = "frm_command_history"
    Me.Text = "frm_command_history"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_created.ResumeLayout(False)
    Me.gb_status.ResumeLayout(False)
    Me.gb_status.PerformLayout()
    Me.gb_command.ResumeLayout(False)
    Me.gb_command.PerformLayout()
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Constants "

  Private Const SQL_COLUMN_COMMAND_ID As Integer = 0
  Private Const SQL_COLUMN_CODE As Integer = 1
  Private Const SQL_COLUMN_STATUS As Integer = 2
  Private Const SQL_COLUMN_CREATED_DATE As Integer = 3
  Private Const SQL_COLUMN_CHANGED_DATE As Integer = 4
  Private Const SQL_COLUMN_TERMINAL_NAME As Integer = 5
  Private Const SQL_COLUMN_PROVIDER_NAME As Integer = 6
  Private Const SQL_COLUMN_TERMINAL_ID As Integer = 7

  ' Indicates if Counters are visible or not.
  Private Const COUNTERS_VISIBLE As Boolean = False

  Private Const FORM_MIN_DB_VERSION As Short = 291

#End Region ' Constants

#Region " Members "

  ' For Counters
  Private m_counter_list() As Integer

  ' For report filters 
  Private m_date_from As String
  Private m_date_to As String
  Private m_terminal As String
  Private m_command As String
  Private m_status As String
  Private m_terminal_location As String

  Private WithEvents m_grid As uc_grid

  Private m_refresh_grid As Boolean = False

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_COMMANDS_HISTORY

    Call GUI_SetMinDbVersion(FORM_MIN_DB_VERSION)
    Call MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Dim fore_color As Color
    Dim back_color As Color

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_CONTROLS.GetString(422)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Text = GLB_NLS_GUI_CONTROLS.GetString(26)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    ' Date
    Me.gb_created.Text = GLB_NLS_GUI_CONTROLS.GetString(338)
    Me.dtp_created_from.Text = GLB_NLS_GUI_STATISTICS.GetString(309)
    Me.dtp_created_to.Text = GLB_NLS_GUI_STATISTICS.GetString(310)
    Me.dtp_created_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_created_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Commnad
    Me.gb_command.Text = GLB_NLS_GUI_CONTROLS.GetString(308)
    Me.chk_cmd_logger.Text = GLB_NLS_GUI_CONTROLS.GetString(415)
    Me.chk_cmd_restart.Text = GLB_NLS_GUI_CONTROLS.GetString(416)

    ' Status
    Me.gb_status.Text = GLB_NLS_GUI_CONTROLS.GetString(261)
    ' Status Checks
    Me.chk_status_pending_send.Text = GLB_NLS_GUI_CONTROLS.GetString(402)
    Me.chk_status_pending_reply.Text = GLB_NLS_GUI_CONTROLS.GetString(403)
    Me.chk_status_replied_ok.Text = GLB_NLS_GUI_CONTROLS.GetString(404)
    Me.chk_status_replied_error.Text = GLB_NLS_GUI_CONTROLS.GetString(405)
    Me.chk_status_timeout_disconnected.Text = GLB_NLS_GUI_CONTROLS.GetString(406)
    Me.chk_status_timeout_not_reply.Text = GLB_NLS_GUI_CONTROLS.GetString(407)
    ' Color Status Labels
    WCP_Command_GetUpdateStatusColor(ENUM_WCP_COMMANDS_STATUS.STATUS_PENDING_SEND, fore_color, back_color)
    Me.lbl_pending_send.BackColor = back_color
    Me.lbl_pending_send.ForeColor = fore_color
    WCP_Command_GetUpdateStatusColor(ENUM_WCP_COMMANDS_STATUS.STATUS_PENDING_REPLY, fore_color, back_color)
    Me.lbl_pending_reply.BackColor = back_color
    Me.lbl_pending_reply.ForeColor = fore_color
    WCP_Command_GetUpdateStatusColor(ENUM_WCP_COMMANDS_STATUS.STATUS_OK, fore_color, back_color)
    Me.lbl_replied_ok.BackColor = back_color
    Me.lbl_replied_ok.ForeColor = fore_color
    WCP_Command_GetUpdateStatusColor(ENUM_WCP_COMMANDS_STATUS.STATUS_ERROR, fore_color, back_color)
    Me.lbl_replied_error.BackColor = back_color
    Me.lbl_replied_error.ForeColor = fore_color
    WCP_Command_GetUpdateStatusColor(ENUM_WCP_COMMANDS_STATUS.STATUS_TIMEOUT_DISCONNECTED, fore_color, back_color)
    Me.lbl_timeout_disconnected.BackColor = back_color
    Me.lbl_timeout_disconnected.ForeColor = fore_color
    WCP_Command_GetUpdateStatusColor(ENUM_WCP_COMMANDS_STATUS.STATUS_TIMEOUT_NOT_REPLY, fore_color, back_color)
    Me.lbl_timeout_not_reply.BackColor = back_color
    Me.lbl_timeout_not_reply.ForeColor = fore_color

    btn_check_all.Text = GLB_NLS_GUI_ALARMS.GetString(452)
    btn_uncheck_all.Text = GLB_NLS_GUI_ALARMS.GetString(453)

    Call Me.uc_pr_list.Init(WSI.Common.Misc.WCPTerminalTypes())

    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)

    m_grid = Me.Grid

    mdl_commands.TerminalReportType = ReportType.Provider

    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()

    m_counter_list = WCP_Command_CounterStatusInit()

    If m_refresh_grid Then
      Call GUI_StyleSheet()
    End If

    m_refresh_grid = False

  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()

    WCP_Command_CounterStatusUpdate(Me.Grid, m_counter_list)

  End Sub ' GUI_AfterLastRow

  ' PURPOSE: Activated when a row of the grid is double clicked or selected
  '  PARAMS:
  '     - INPUT:
  '               
  '     - OUTPUT:
  '          
  ' RETURNS:
  '     - none
  Protected Overrides Sub GUI_EditSelectedItem()

    Dim idx_row As Int32
    Dim cmd_id As UInt64
    Dim str_provider As String
    Dim str_terminal As String
    Dim str_created As String
    'Dim previous_top_most As Boolean

    ' Search the first row selected
    For idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(idx_row).IsSelected Then
        Exit For
      End If
    Next

    If idx_row = Me.Grid.NumRows Then
      Return
    End If

    ' Get the Command ID
    cmd_id = WCP_Command_GetRowId(Me.Grid, idx_row)

    If cmd_id > 0 Then
      If Not ShowLogger(cmd_id, Me.MdiParent) Then
        str_provider = ""
        str_terminal = ""
        WCP_Command_GetRowProviderTerminal(Me.Grid, idx_row, str_provider, str_terminal)
        str_created = WCP_Command_GetRowCreatedDate(Me.Grid, idx_row)

        'previous_top_most = LoggerGetTopMost()
        'LoggerSetTopMost(False)
        Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(151), ENUM_MB_TYPE.MB_TYPE_ERROR, , , _
                        str_provider, str_terminal, str_created)
        'LoggerSetTopMost(previous_top_most)
      End If
    Else
      'previous_top_most = LoggerGetTopMost()
      'LoggerSetTopMost(False)
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(153), ENUM_MB_TYPE.MB_TYPE_WARNING)
      'LoggerSetTopMost(previous_top_most)
    End If
    'Me.Grid.Focus()

  End Sub 'GUI_EditSelectedItem

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted

  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.dtp_created_from.Checked And Me.dtp_created_to.Checked Then
      If Me.dtp_created_from.Value > Me.dtp_created_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_created_to.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim str_sql As String

    ' Get Select and from
    str_sql = "SELECT CMD_ID " & _
                  " , CMD_CODE " & _
                  " , CMD_STATUS " & _
                  " , CMD_CREATED " & _
                  " , CMD_STATUS_CHANGED " & _
                  " , TE_NAME " & _
                  " , TE_PROVIDER_ID " & _
                  " , CMD_TERMINAL_ID " & _
              "FROM WCP_COMMANDS WITH (INDEX(IX_wcp_cmd_created)), TERMINALS " & _
              "WHERE TE_TERMINAL_ID = CMD_TERMINAL_ID "

    str_sql = str_sql & GetSqlWhere()
    str_sql = str_sql & "ORDER BY CMD_ID "

    Return str_sql
  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim command As TYPE_COMMAND_ITEM

    command.id = DbRow.Value(SQL_COLUMN_COMMAND_ID)
    If DbRow.IsNull(SQL_COLUMN_PROVIDER_NAME) Then
      command.provider_name = ""
    Else
      command.provider_name = DbRow.Value(SQL_COLUMN_PROVIDER_NAME)
    End If
    command.terminal_name = DbRow.Value(SQL_COLUMN_TERMINAL_NAME)
    command.code = DbRow.Value(SQL_COLUMN_CODE)
    command.status = DbRow.Value(SQL_COLUMN_STATUS)
    command.date_send = DbRow.Value(SQL_COLUMN_CREATED_DATE)
    command.date_status_change = DbRow.Value(SQL_COLUMN_CHANGED_DATE)
    command.terminal_id = DbRow.Value(SQL_COLUMN_TERMINAL_ID)

    If Not WCP_Command_SetupRow(Me.Grid, RowIndex, command) Then
      ' If row can not be added, exit without increment counter status.
      Return False
    End If

    WCP_Command_CounterStatusIncrement(DbRow.Value(SQL_COLUMN_STATUS), m_counter_list)

    Return True
  End Function ' GUI_SetupRow 

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.dtp_created_from

  End Sub ' GUI_SetInitialFocus

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_STATISTICS.GetString(309), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_STATISTICS.GetString(310), m_date_to)
    PrintData.SetFilter(GLB_NLS_GUI_CONTROLS.GetString(308), m_command)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237), m_terminal_location)

    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_terminal)
    PrintData.SetFilter(GLB_NLS_GUI_CONTROLS.GetString(261), m_status)

    PrintData.FilterValueWidth(2) = 10000

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()

    Dim no_one As Boolean
    Dim all As Boolean

    m_status = ""
    m_command = ""
    m_terminal = ""
    m_date_from = ""
    m_date_to = ""
    m_terminal_location = ""

    ' Date 
    If Me.dtp_created_from.Checked Then
      m_date_from = GUI_FormatDate(Me.dtp_created_from.Value, _
                    ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                    ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Me.dtp_created_to.Checked Then
      m_date_to = GUI_FormatDate(Me.dtp_created_to.Value, _
                  ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                  ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Terminals 
    m_terminal = Me.uc_pr_list.GetTerminalReportText()

    ' Status
    no_one = True
    all = True
    If Me.chk_status_pending_send.Checked Then
      m_status = m_status & Me.chk_status_pending_send.Text & ", "
      no_one = False
    Else
      all = False
    End If

    If Me.chk_status_pending_reply.Checked Then
      m_status = m_status & Me.chk_status_pending_reply.Text & ", "
      no_one = False
    Else
      all = False
    End If

    If Me.chk_status_replied_ok.Checked Then
      m_status = m_status & Me.chk_status_replied_ok.Text & ", "
      no_one = False
    Else
      all = False
    End If

    If Me.chk_status_replied_error.Checked Then
      m_status = m_status & Me.chk_status_replied_error.Text & ", "
      no_one = False
    Else
      all = False
    End If

    If Me.chk_status_timeout_disconnected.Checked Then
      m_status = m_status & Me.chk_status_timeout_disconnected.Text & ", "
      no_one = False
    Else
      all = False
    End If

    If Me.chk_status_timeout_not_reply.Checked Then
      m_status = m_status & Me.chk_status_timeout_not_reply.Text & ", "
      no_one = False
    Else
      all = False
    End If

    If m_status.Length <> 0 Then
      m_status = Strings.Left(m_status, Len(m_status) - 2)
    End If

    If no_one Or all Then
      m_status = GLB_NLS_GUI_ALARMS.GetString(277)
    End If

    ' Command
    no_one = True
    all = True
    If Me.chk_cmd_logger.Checked Then
      m_command = m_command & Me.chk_cmd_logger.Text & ", "
      no_one = False
    Else
      all = False
    End If

    If Me.chk_cmd_restart.Checked Then
      m_command = m_command & Me.chk_cmd_restart.Text & ", "
      no_one = False
    Else
      all = False
    End If

    If m_command.Length <> 0 Then
      m_command = Strings.Left(m_command, Len(m_command) - 2)
    End If

    If no_one Or all Then
      m_command = GLB_NLS_GUI_ALARMS.GetString(277)
    End If

    If Me.chk_terminal_location.Checked Then
      m_terminal_location = GLB_NLS_GUI_PLAYER_TRACKING.GetString(698)
    Else
      m_terminal_location = GLB_NLS_GUI_PLAYER_TRACKING.GetString(699)
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region  ' OVERRIDES

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()

    WCP_Command_StyleSheet(Me.Grid, COUNTERS_VISIBLE)
    Me.Grid.SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

  End Sub 'GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Me.dtp_created_from.Value = New DateTime(Now.Year, Now.Month, Now.Day, 0, 0, 0)
    Me.dtp_created_from.Checked = True

    Me.dtp_created_to.Value = Me.dtp_created_from.Value
    Me.dtp_created_to.Checked = False

    Call btn_check_all_Click()

    Me.chk_cmd_logger.Checked = True
    Me.chk_cmd_restart.Checked = True

    Call Me.uc_pr_list.SetDefaultValues()

    Me.chk_terminal_location.Checked = False

  End Sub ' SetDefaultValues

  ' PURPOSE: Get Sql WHERE to build SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String

    Dim str_where As String = ""
    Dim str_list As String

    ' Filter Created Dates
    If Me.dtp_created_from.Checked = True Then
      str_where = str_where & " AND CMD_CREATED >= " & GUI_FormatDateDB(dtp_created_from.Value)
    End If

    If Me.dtp_created_to.Checked = True Then
      str_where = str_where & " AND CMD_CREATED < " & GUI_FormatDateDB(dtp_created_to.Value)
    End If

    ' Filter Providers - Terminals
    str_where = str_where & " AND TE_TERMINAL_ID IN " & Me.uc_pr_list.GetProviderIdListSelected()

    ' Filter Command
    str_list = GetCommandIdListSelected()
    If str_list <> "" Then
      str_where = str_where & " AND CMD_CODE IN (" & str_list & ") "
    End If

    ' FJC 12-NOV-2014
    str_where = str_where & " AND CMD_CODE <> " & WSI.Common.WCP_CommandCode.SiteJackpotAwarded

    ' Filter Status
    str_list = GetStatusIdListSelected()
    If str_list <> "" Then
      str_where = str_where & " AND CMD_STATUS IN (" & str_list & ") "
    End If

    Return str_where
  End Function ' GetSqlWhere

  ' PURPOSE: Get Status Id list for selected status
  '          Format list to build query: X, X, X
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetStatusIdListSelected() As String

    Dim list_type As String = ""

    If Me.chk_status_pending_send.Checked Then
      If list_type.Length = 0 Then
        list_type = ENUM_WCP_COMMANDS_STATUS.STATUS_PENDING_SEND
      Else
        list_type = list_type & ", " & ENUM_WCP_COMMANDS_STATUS.STATUS_PENDING_SEND
      End If
    End If

    If Me.chk_status_pending_reply.Checked Then
      If list_type.Length = 0 Then
        list_type = ENUM_WCP_COMMANDS_STATUS.STATUS_PENDING_REPLY
      Else
        list_type = list_type & ", " & ENUM_WCP_COMMANDS_STATUS.STATUS_PENDING_REPLY
      End If
    End If

    If Me.chk_status_replied_ok.Checked Then
      If list_type.Length = 0 Then
        list_type = ENUM_WCP_COMMANDS_STATUS.STATUS_OK
      Else
        list_type = list_type & ", " & ENUM_WCP_COMMANDS_STATUS.STATUS_OK
      End If
    End If

    If Me.chk_status_replied_error.Checked Then
      If list_type.Length = 0 Then
        list_type = ENUM_WCP_COMMANDS_STATUS.STATUS_ERROR
      Else
        list_type = list_type & ", " & ENUM_WCP_COMMANDS_STATUS.STATUS_ERROR
      End If
    End If

    If Me.chk_status_timeout_disconnected.Checked Then
      If list_type.Length = 0 Then
        list_type = ENUM_WCP_COMMANDS_STATUS.STATUS_TIMEOUT_DISCONNECTED
      Else
        list_type = list_type & ", " & ENUM_WCP_COMMANDS_STATUS.STATUS_TIMEOUT_DISCONNECTED
      End If
    End If

    If Me.chk_status_timeout_not_reply.Checked Then
      If list_type.Length = 0 Then
        list_type = ENUM_WCP_COMMANDS_STATUS.STATUS_TIMEOUT_NOT_REPLY
      Else
        list_type = list_type & ", " & ENUM_WCP_COMMANDS_STATUS.STATUS_TIMEOUT_NOT_REPLY
      End If
    End If

    Return list_type
  End Function 'GetStatusIdListSelected

  ' PURPOSE: Get Command Id list for selected commands
  '          Format list to build query: X, X, X
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetCommandIdListSelected() As String

    Dim list_type As String = ""

    If Me.chk_cmd_logger.Checked Then
      If list_type.Length = 0 Then
        list_type = WSI.Common.WCP_CommandCode.GetLogger
      Else
        list_type = list_type & ", " & WSI.Common.WCP_CommandCode.GetLogger
      End If
    End If

    If Me.chk_cmd_restart.Checked Then
      If list_type.Length = 0 Then
        list_type = WSI.Common.WCP_CommandCode.Restart
      Else
        list_type = list_type & ", " & WSI.Common.WCP_CommandCode.Restart
      End If
    End If

    Return list_type
  End Function 'GetCommandIdListSelected

#End Region  ' Private Functions

#Region " Events"

#Region " Button Events "

  ' PURPOSE: Event handler routine for the CHECK_ALL Button.
  '          Check all status CheckBoxes.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub btn_check_all_Click() Handles btn_check_all.ClickEvent
    Me.chk_status_pending_send.Checked = True
    Me.chk_status_pending_reply.Checked = True
    Me.chk_status_replied_ok.Checked = True
    Me.chk_status_replied_error.Checked = True
    Me.chk_status_timeout_not_reply.Checked = True
    Me.chk_status_timeout_disconnected.Checked = True
  End Sub ' btn_check_all_Click

  ' PURPOSE: Event handler routine for the UNCHECK_ALL Button.
  '          Uncheck all status CheckBoxes.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub btn_uncheck_all_Click() Handles btn_uncheck_all.ClickEvent
    Me.chk_status_pending_send.Checked = False
    Me.chk_status_pending_reply.Checked = False
    Me.chk_status_replied_ok.Checked = False
    Me.chk_status_replied_error.Checked = False
    Me.chk_status_timeout_not_reply.Checked = False
    Me.chk_status_timeout_disconnected.Checked = False
  End Sub ' btn_uncheck_all_Click

#End Region ' Button Events

#Region " DataGrid Events "

  ' PURPOSE: When leaving the DataGrid, set TopMost Property to false for the LogViewer Form.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  'Private Sub grid_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles m_grid.Leave
  'LoggerSetTopMost(False)
  'End Sub ' grid_Leave

#End Region ' DataGrid Events

#Region " Form Events "

  'Private Sub frm_command_history_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
  '  HideLogger()
  'End Sub ' frm_command_history_FormClosed

  'Private Sub frm_command_history_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
  '  LoggerSetTopMost(True)
  '  LoggerSetTopMost(False)
  'End Sub ' frm_command_history_Activated

  Private Sub chk_terminal_location_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminal_location.CheckedChanged
    If chk_terminal_location.Checked Then
      mdl_commands.TerminalReportType = ReportType.Provider + ReportType.Location
    Else
      mdl_commands.TerminalReportType = ReportType.Provider
    End If

    m_refresh_grid = True
  End Sub

#End Region ' Form Events

#End Region 'Events

End Class ' frm_command_history
