﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_junkets_representatives_sel.vb
' DESCRIPTION:   Displays list of representatives of junkets
' AUTHOR:        Alberto Marcos
' CREATION DATE: 30-MAR-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 30-MAR-2017  AMF     Initial version
' 09-AUG-2017  DPC     WIGOS-4018: Junkets: Representative creation date
' -------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_CommonMisc

Public Class frm_junkets_representatives_sel
  Inherits frm_base_sel

#Region " Members "
  Private m_junket As CLASS_JUNKETS_REPRESENTATIVE

  Private m_filter_code As String
  Private m_filter_name As String
  Private m_filter_internal As String
  Private m_filter_status As String

#End Region ' Members

#Region " Constants "

  ' SQL Columns
  Private Const SQL_COLUMN_CODE As Integer = 0
  Private Const SQL_COLUMN_NAME As Integer = 1
  Private Const SQL_COLUMN_INTERNAL As Integer = 2
  Private Const SQL_COLUMN_STATUS As Integer = 3
  Private Const SQL_COLUMN_DEPOSIT_AMOUNT As Integer = 4
  Private Const SQL_COLUMN_CREATION As Integer = 5
  Private Const SQL_COLUMN_ID As Integer = 6

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_CODE As Integer = 1
  Private Const GRID_COLUMN_NAME As Integer = 2
  Private Const GRID_COLUMN_INTERNAL As Integer = 3
  Private Const GRID_COLUMN_STATUS As Integer = 4
  Private Const GRID_COLUMN_DEPOSIT_AMOUNT As Integer = 5
  Private Const GRID_COLUMN_CREATION As Integer = 6
  Private Const GRID_COLUMN_ID As Integer = 7

  Private Const GRID_COLUMNS As Integer = 8

  ' DATAGRID Width
  Private Const GRID_WIDTH_INDEX As Integer = 250
  Private Const GRID_WIDTH_CODE As Integer = 1500
  Private Const GRID_WIDTH_NAME As Integer = 3500
  Private Const GRID_WIDTH_INTERNAL As Integer = 1750
  Private Const GRID_WIDTH_STATUS As Integer = 1300
  Private Const GRID_WIDTH_DEPOSIT_AMOUNT As Integer = 1800
  Private Const GRID_WIDTH_CREATION As Integer = 1650
  Private Const GRID_WIDTH_ID As Integer = 0

#End Region ' Constants

#Region " Properties "

  Private m_is_select_mode As Boolean
  Public Property IsSelectMode() As Boolean
    Get
      Return m_is_select_mode
    End Get
    Set(ByVal value As Boolean)
      m_is_select_mode = value
    End Set
  End Property

  Private m_junket_representative As WigosGUI.CLASS_JUNKETS.TYPE_JUNKET_REPRESENTATIVE
  Public Property JunketRepresentative() As WigosGUI.CLASS_JUNKETS.TYPE_JUNKET_REPRESENTATIVE
    Get
      If m_junket_representative Is Nothing Then m_junket_representative = New CLASS_JUNKETS.TYPE_JUNKET_REPRESENTATIVE
      Return m_junket_representative
    End Get
    Set(ByVal value As WigosGUI.CLASS_JUNKETS.TYPE_JUNKET_REPRESENTATIVE)
      m_junket_representative = value
    End Set
  End Property

#End Region ' Properties

#Region " Public methods"

  ''' <summary>
  ''' Sets the Form Id
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_JUNKETS_REPRESENTATIVES_SEL

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ''' <summary>
  ''' Show the Form
  ''' </summary>
  ''' <param name="MdiParent"></param>
  ''' <remarks></remarks>
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent

    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public methods

#Region " Overrides "

  ''' <summary>
  ''' Initialise all the form controls
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Customer base
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8089)

    Me.ef_code.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5710)
    Me.ef_code.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 10)

    Me.ef_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4399)
    Me.ef_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    Me.gb_internal_employee.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8000)
    Me.chkInternalYes.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
    Me.chkInternalNo.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)

    Me.gb_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(262)
    Me.chkStatusEnabled.Text = GLB_NLS_GUI_CONTROLS.GetString(281)
    Me.chkStatusDisabled.Text = GLB_NLS_GUI_CONTROLS.GetString(282)

    Me.m_junket = New CLASS_JUNKETS_REPRESENTATIVE

    Call SetDefaultValues()

    ' Define layout of all Main Grid Columns
    Call GUI_StyleSheet()

    If m_is_select_mode Then
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_PRINT).Visible = False
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_EXCEL).Visible = False
    End If

  End Sub ' GUI_InitControls

  ''' <summary>
  ''' Sets initial focus
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.ef_code
  End Sub ' GUI_SetInitialFocus

  ''' <summary>
  ''' Set te Form Filters to their Default Values
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()

  End Sub 'GUI_FilterReset

  ''' <summary>
  ''' Perform non-default actions for Button Clicks
  ''' </summary>
  ''' <param name="ButtonId"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case ENUM_BUTTON.BUTTON_NEW

        Call GUI_ShowNewRepresentative()

      Case ENUM_BUTTON.BUTTON_SELECT

        Call GUI_EditSelectedItem()

      Case ENUM_BUTTON.BUTTON_FILTER_APPLY
        Call MyBase.GUI_ButtonClick(ButtonId)

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub 'GUI_ButtonClick

  ''' <summary>
  ''' Finds the first selected grid item and calls the edit form
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_EditSelectedItem()
    Dim _idx_row As Int32
    Dim _frm As frm_junkets_representatives_edit
    Dim _is_enabled As Boolean

    ' Create instance
    _frm = New frm_junkets_representatives_edit()

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    ' If no row is selected
    If _idx_row = Me.Grid.NumRows Then
      Return
    End If

    If Not m_is_select_mode Then
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      _frm.ShowEditItem(Me.Grid.Cell(_idx_row, GRID_COLUMN_CODE).Value)

      Windows.Forms.Cursor.Current = Cursors.Default
      Call Me.Grid.Focus()
    Else

      If CLASS_JUNKETS_REPRESENTATIVE.IsEnabled(Me.Grid.Cell(_idx_row, GRID_COLUMN_ID).Value, _is_enabled) Then
        If Not _is_enabled Then

          If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8093), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO) <> ENUM_MB_RESULT.MB_RESULT_YES Then

            Return
          End If
        End If
      End If

      JunketRepresentative.id = Me.Grid.Cell(_idx_row, GRID_COLUMN_ID).Value
      JunketRepresentative.code = Me.Grid.Cell(_idx_row, GRID_COLUMN_CODE).Value
      JunketRepresentative.name = Me.Grid.Cell(_idx_row, GRID_COLUMN_NAME).Value
      Me.Close()

    End If

  End Sub ' GUI_EditSelectedItem

  ''' <summary>
  ''' Calls the Junkets class to return the SQL Query
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _filter_internal As CLASS_JUNKETS_REPRESENTATIVE.FilterOption
    Dim _filter_status As CLASS_JUNKETS_REPRESENTATIVE.FilterOption

    If chkInternalYes.Checked Then
      _filter_internal = CLASS_JUNKETS_REPRESENTATIVE.FilterOption.Yes
      If chkInternalNo.Checked Then
        _filter_internal = CLASS_JUNKETS_REPRESENTATIVE.FilterOption.Both
      End If
    ElseIf chkInternalNo.Checked Then
      _filter_internal = CLASS_JUNKETS_REPRESENTATIVE.FilterOption.No
    Else
      _filter_internal = CLASS_JUNKETS_REPRESENTATIVE.FilterOption.Both
    End If

    If chkStatusEnabled.Checked Then
      _filter_status = CLASS_JUNKETS_REPRESENTATIVE.FilterOption.Yes
      If chkStatusDisabled.Checked Then
        _filter_status = CLASS_JUNKETS_REPRESENTATIVE.FilterOption.Both
      End If
    ElseIf chkStatusDisabled.Checked Then
      _filter_status = CLASS_JUNKETS_REPRESENTATIVE.FilterOption.No
    Else
      _filter_status = CLASS_JUNKETS_REPRESENTATIVE.FilterOption.Both
    End If

    Return m_junket.GetRepresentatives(ef_code.Value.Trim(), ef_name.Value.Trim(), _filter_internal, _filter_status)

  End Function 'GUI_FilterGetSqlQuery

  ''' <summary>
  ''' Validates the Filter selection where needed
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_FilterCheck() As Boolean

    Return True
  End Function ' GUI_FilterCheck

  ''' <summary>
  ''' Writes the Grid Row reformating where required and adding calculated cell values
  ''' </summary>
  ''' <param name="RowIndex"></param>
  ''' <param name="DbRow"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    If Not DbRow.IsNull(SQL_COLUMN_CODE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CODE).Value = DbRow.Value(SQL_COLUMN_CODE)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NAME).Value = DbRow.Value(SQL_COLUMN_NAME)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_INTERNAL) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_INTERNAL).Value = IIf(DbRow.Value(SQL_COLUMN_INTERNAL), GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279))
    End If

    If Not DbRow.IsNull(SQL_COLUMN_STATUS) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = IIf(DbRow.Value(SQL_COLUMN_STATUS), GLB_NLS_GUI_CONTROLS.GetString(281), GLB_NLS_GUI_CONTROLS.GetString(282))
    End If

    If Not DbRow.IsNull(SQL_COLUMN_DEPOSIT_AMOUNT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DEPOSIT_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_DEPOSIT_AMOUNT), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_CREATION) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CREATION).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_CREATION), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ID).Value = DbRow.Value(SQL_COLUMN_ID)
    End If

    Return True

  End Function ' GUI_SetupRow

  ''' <summary>
  ''' Update filters for reports (Excel and print)
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_filter_code = String.Empty
    m_filter_name = String.Empty
    m_filter_internal = String.Empty
    m_filter_status = String.Empty

    m_filter_code = ef_code.Value
    m_filter_name = ef_name.Value

    If chkInternalYes.Checked Then
      m_filter_internal = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
      If chkInternalNo.Checked Then
        m_filter_internal &= ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)
      End If
    ElseIf chkInternalNo.Checked Then
      m_filter_internal = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)
    End If

    If chkStatusEnabled.Checked Then
      m_filter_status = GLB_NLS_GUI_CONTROLS.GetString(281)
      If chkStatusDisabled.Checked Then
        m_filter_status &= ", " & GLB_NLS_GUI_CONTROLS.GetString(282)
      End If
    ElseIf chkStatusDisabled.Checked Then
      m_filter_status = GLB_NLS_GUI_CONTROLS.GetString(282)
    End If

  End Sub ' GUI_ReportUpdateFilters

  ''' <summary>
  ''' Sets the report Filter values for Excel and Print
  ''' </summary>
  ''' <param name="PrintData"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5710), m_filter_code)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4399), m_filter_name)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(8000), m_filter_internal)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7955), m_filter_status)

  End Sub ' GUI_ReportFilter

#End Region ' Overrides

#Region " Private methods "

  ''' <summary>
  ''' Displayes the Junket representative Edit Form for a New Representative
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GUI_ShowNewRepresentative()

    Dim _frm As frm_junkets_representatives_edit

    Windows.Forms.Cursor.Current = Cursors.WaitCursor


    ' Create instance
    _frm = New frm_junkets_representatives_edit()

    _frm.ShowNewItem()

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub ' GUI_ShowNewRepresentative

  ''' <summary>
  ''' Sets Default Values for the Filters
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SetDefaultValues()

    Me.ef_code.Value = String.Empty
    Me.ef_name.Value = String.Empty

    Me.chkInternalYes.Checked = False
    Me.chkInternalNo.Checked = False

    Me.chkStatusEnabled.Checked = False
    Me.chkStatusDisabled.Checked = False

  End Sub 'SetDefaultValues

  ''' <summary>
  ''' Style Sheet for the look of the form
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GUI_StyleSheet()
    With Me.Grid

      ' Initialize the index of unfixed columns and return the number of columns
      Call .Init(GRID_COLUMNS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Enable sortable in Main Grid Columns
      .Sortable = True

      ' Index
      .Column(GRID_COLUMN_INDEX).Header.Text = String.Empty
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Code
      .Column(GRID_COLUMN_CODE).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5710)
      .Column(GRID_COLUMN_CODE).Width = GRID_WIDTH_CODE
      .Column(GRID_COLUMN_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Name
      .Column(GRID_COLUMN_NAME).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4399)
      .Column(GRID_COLUMN_NAME).Width = GRID_WIDTH_NAME
      .Column(GRID_COLUMN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Internal
      .Column(GRID_COLUMN_INTERNAL).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8000)
      .Column(GRID_COLUMN_INTERNAL).Width = GRID_WIDTH_INTERNAL
      .Column(GRID_COLUMN_INTERNAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Status
      .Column(GRID_COLUMN_STATUS).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(262)
      .Column(GRID_COLUMN_STATUS).Width = GRID_WIDTH_STATUS
      .Column(GRID_COLUMN_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Deposit amount
      .Column(GRID_COLUMN_DEPOSIT_AMOUNT).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8005)
      .Column(GRID_COLUMN_DEPOSIT_AMOUNT).Width = GRID_WIDTH_DEPOSIT_AMOUNT
      .Column(GRID_COLUMN_DEPOSIT_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Creation
      .Column(GRID_COLUMN_CREATION).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6506)
      .Column(GRID_COLUMN_CREATION).Width = GRID_WIDTH_CREATION
      .Column(GRID_COLUMN_CREATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' ID
      .Column(GRID_COLUMN_ID).Width = GRID_WIDTH_ID

    End With
  End Sub ' GUI_StyleSheet

#End Region ' Private methods

End Class
