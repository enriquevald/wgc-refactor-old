'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_groups_sel
'
' DESCRIPTION : Groups selection
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 21-JUN-2013  RBG    Initial version
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Data.SqlClient

Public Class frm_groups_sel
  Inherits frm_base_sel

#Region " Constants "

  ' Grid Columns
  Private Const GRID_COLUMN_ID As Integer = 0
  Private Const GRID_COLUMN_NAME As Integer = 1
  Private Const GRID_COLUMN_DESCRIPTION As Integer = 2
  Private Const GRID_COLUMN_NUM_TERM As Integer = 3
  Private Const GRID_COLUMN_ENABLED As Integer = 4

  Private Const GRID_COLUMNS As Integer = 5
  Private Const GRID_HEADER_ROWS As Integer = 1

  Private Const SQL_COL_ID As Integer = 0
  Private Const SQL_COL_NAME As Integer = 1
  Private Const SQL_COL_DESCR As Integer = 2
  Private Const SQL_COL_NUM_TERM As Integer = 3
  Private Const SQL_COL_ENABLED As Integer = 4

  Private Const FORM_DB_MIN_VERSION As Short = 149
#End Region ' Constants

#Region " Members "

  Private m_name As String
  Private m_status As String

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE : Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_GROUPS_SEL
    Call MyBase.GUI_SetFormId()

    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)
  End Sub ' GUI_SetFormId

  ' PURPOSE : Initialize every form control
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Set form Name
    Me.Text = GLB_NLS_GUI_CONTROLS.GetString(460)

    ' Assign NLS
    ef_name.Text = GLB_NLS_GUI_CONTROLS.GetString(461)
    GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)
    gb_group_status.Text = GLB_NLS_GUI_CONTROLS.GetString(281)
    chk_enabled.Text = GLB_NLS_GUI_CONFIGURATION.GetString(264)
    chk_disabled.Text = GLB_NLS_GUI_CONFIGURATION.GetString(265)

    ' Filters
    Call ef_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, CLASS_GROUP.MAX_NAME_LEN)

    Call GUI_StyleSheet()

    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ' PURPOSE : Activated when a row of the grid is double clicked or selected, so it can be edited
  '
  '  PARAMS :
  '     - INPUT :
  '               
  '     - OUTPUT :
  '          
  ' RETURNS :

  Protected Overrides Sub GUI_EditSelectedItem()

    Dim idx_row As Int32
    Dim group_id As Integer
    Dim group_name As String
    Dim frm_edit As frm_groups_edit

    ' Search the first row selected
    For idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(idx_row).IsSelected Then
        Exit For
      End If
    Next

    If idx_row = Me.Grid.NumRows Then
      Return
    End If

    ' Get the Draw ID and Name, and launch the editor
    group_id = Me.Grid.Cell(idx_row, GRID_COLUMN_ID).Value
    group_name = Me.Grid.Cell(idx_row, GRID_COLUMN_NAME).Value

    frm_edit = New frm_groups_edit()

    Call frm_edit.ShowEditItem(group_id, group_name)


    frm_edit = Nothing
    Call Me.Grid.Focus()

  End Sub ' GUI_EditSelectedItem

  ' PURPOSE: Get the defined Query Type
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_QUERY_TYPE
  '

  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE

    Return ENUM_QUERY_TYPE.QUERY_COMMAND

  End Function ' GUI_GetQueryType

  ' PURPOSE: Build an SQL Command query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL Command query ready to send to the database

  Protected Overrides Function GUI_GetSqlCommand() As SqlCommand

    Dim _sql_command As SqlCommand
    Dim _name As String

    _name = Me.ef_name.Value

    Dim _str_bld As System.Text.StringBuilder
    _str_bld = New System.Text.StringBuilder()

    _str_bld.AppendLine("    SELECT  GR_ID")
    _str_bld.AppendLine("           ,GR_NAME")
    _str_bld.AppendLine("           ,GR_DESCRIPTION")
    _str_bld.AppendLine("           ,(SELECT COUNT(*)")
    _str_bld.AppendLine("             FROM TERMINAL_GROUPS")
    _str_bld.AppendLine("             WHERE TG_ELEMENT_ID = GR_ID AND TG_ELEMENT_TYPE = @pType )")
    _str_bld.AppendLine("             AS GR_NUM_TERMINALS")
    _str_bld.AppendLine("           ,GR_ENABLED ")
    _str_bld.AppendLine("     FROM GROUPS ")

    _str_bld.AppendLine(GetSqlWhere(_name))

    _str_bld.AppendLine(" ORDER BY GR_NAME ")

    _sql_command = New SqlCommand(_str_bld.ToString())

    If _name.Length > 0 Then
      _sql_command.Parameters.Add("@pName", SqlDbType.NVarChar).Value = "%" + _name.Replace("%", "[%]") + "%"
    End If

    _sql_command.Parameters.Add("@pType", SqlDbType.Int).Value = WSI.Common.EXPLOIT_ELEMENT_TYPE.GROUP

    Return _sql_command

  End Function ' GUI_GetSqlCommand

  ' PURPOSE : Initialize all form filters with their default values
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()

  End Sub ' GUI_FilterReset

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Me.Grid.Cell(RowIndex, GRID_COLUMN_ID).Value = DbRow.Value(SQL_COL_ID).ToString()
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NAME).Value = DbRow.Value(SQL_COL_NAME).ToString()
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DESCRIPTION).Value = DbRow.Value(SQL_COL_DESCR).ToString()
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_TERM).Value = IIf(DbRow.Value(SQL_COL_ENABLED), DbRow.Value(SQL_COL_NUM_TERM).ToString(), "---")
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ENABLED).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(IIf(DbRow.Value(SQL_COL_ENABLED), 359, 360))

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE : Process button actions in order to branch to a child screen
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_NEW
        Call ShowNewGroupForm()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        Call GUI_EditSelectedItem()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE : Sets initial focus
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  '          
  ' NOTES
  '
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.ef_name

  End Sub 'GUI_SetInitialFocus


#Region " GUI Reports "

  ' PURPOSE : Set proper values for form filters being sent to the report
  '
  '  PARAMS :
  '     - INPUT :
  '           - PrintData
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    ' Column 1
    PrintData.SetFilter(GLB_NLS_GUI_CONTROLS.GetString(461), m_name)

    ' Column 2
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(280), m_status)

  End Sub ' GUI_ReportFilter

  ' PURPOSE : Set form specific requirements/parameters forthe report
  '
  '  PARAMS :
  '     - INPUT :
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)

    PrintData.Params.Title = GLB_NLS_GUI_CONTROLS.GetString(460)   ' 460 "Grupos"

  End Sub ' GUI_ReportParams

  ' PURPOSE : Set printable filters
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_name = ""
    m_status = ""

    ' Name
    If Me.ef_name.Value.Length > 0 Then
      m_name = Me.ef_name.Value
    End If

    ' Enabled
    If (chk_enabled.Checked And Not chk_disabled.Checked) Then
      m_status = GLB_NLS_GUI_CONTROLS.GetString(281)
    ElseIf (Not chk_enabled.Checked And chk_disabled.Checked) Then
      m_status = GLB_NLS_GUI_CONTROLS.GetString(282)
    Else
      m_status = GLB_NLS_GUI_CONTROLS.GetString(474)
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region ' OVERRIDES

#Region " Public Functions "


  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE : Define layout of all Main Grid Columns 
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Sub GUI_StyleSheet()

    With Me.Grid

      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Columns
      '   - Id
      '   - Name
      '   - Description
      '   - Terminals

      '   - Id
      .Column(GRID_COLUMN_ID).Width = 0
      .Column(GRID_COLUMN_ID).IsColumnPrintable = False

      '   - Name
      .Column(GRID_COLUMN_NAME).Header.Text = GLB_NLS_GUI_CONTROLS.GetString(461)       ' 461 "Nombre"
      .Column(GRID_COLUMN_NAME).Width = 2900
      .Column(GRID_COLUMN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      '   - Description
      .Column(GRID_COLUMN_DESCRIPTION).Header.Text = GLB_NLS_GUI_CONTROLS.GetString(473)       ' 473 "Description"
      .Column(GRID_COLUMN_DESCRIPTION).Width = 7000
      .Column(GRID_COLUMN_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      '   - Terminals
      .Column(GRID_COLUMN_NUM_TERM).Header.Text = GLB_NLS_GUI_CONTROLS.GetString(464)       ' 464 "Terminals"
      .Column(GRID_COLUMN_NUM_TERM).Width = 1200
      .Column(GRID_COLUMN_NUM_TERM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '   - Terminals
      .Column(GRID_COLUMN_ENABLED).Header.Text = GLB_NLS_GUI_CONTROLS.GetString(281)       ' 281 "Enabled"
      .Column(GRID_COLUMN_ENABLED).Width = 1200
      .Column(GRID_COLUMN_ENABLED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    End With

  End Sub 'GUI_StyleSheet

  ' PURPOSE : Activated when new row button is pressed
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Sub ShowNewGroupForm()

    Dim frm_edit As frm_groups_edit

    frm_edit = New frm_groups_edit
    Call frm_edit.ShowNewItem()

    frm_edit = Nothing

    Call Me.Grid.Focus()

  End Sub ' ShowNewAdsForm

  ' PURPOSE: Get SQL WHERE 
  '     
  '  PARAMS:
  '     - INPUT:
  '           - Name: String
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String

  Private Function GetSqlWhere(ByVal Name As String) As String

    Dim _str_bld As System.Text.StringBuilder
    _str_bld = New System.Text.StringBuilder

    If Name <> "" Then
      _str_bld.AppendLine("GR_NAME LIKE @pName")
    End If

    If Me.chk_enabled.Checked = True And Me.chk_disabled.Checked = False Then
      _str_bld.AppendLine(IIf(_str_bld.Length > 0, " AND GR_ENABLED = 1 ", " GR_ENABLED = 1 "))
    End If
    If Me.chk_disabled.Checked = True And Me.chk_enabled.Checked = False Then
      _str_bld.AppendLine(IIf(_str_bld.Length > 0, " AND GR_ENABLED = 0 ", " GR_ENABLED = 0 "))
    End If

    If _str_bld.Length > 0 Then
      _str_bld.Insert(0, " WHERE ")
    End If

    Return _str_bld.ToString()

  End Function ' GetSqlWhere

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub SetDefaultValues()

    Me.ef_name.Value = ""
    Me.chk_enabled.Checked = True
    Me.chk_disabled.Checked = False

  End Sub ' SetDefaultValues

#End Region ' Private Functions

  Public Sub New()

    MyBase.New()
    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.

  End Sub
End Class