'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_promotion_report
' DESCRIPTION:   Promotions Summary
' AUTHOR:        Raul Cervera
' CREATION DATE: 14-SEP-2010
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 14-SEP-2010  RCI    Initial version
' 31-MAY-2011  JAB    Fixed Bug #307: Add Note Acceptor promotions
' 04-JUN-2012  JAB    Eliminate top clause of sql query
' 12-FEB-2013  JMM    uc_account_sel.ValidateFormat added on FilterCheck
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data

Public Class frm_promotion_report
  Inherits frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents uc_account_sel1 As GUI_Controls.uc_account_sel
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector
  Friend WithEvents ef_promo_name As GUI_Controls.uc_entry_field
  Friend WithEvents chk_details As System.Windows.Forms.CheckBox
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.uc_account_sel1 = New GUI_Controls.uc_account_sel
    Me.chk_details = New System.Windows.Forms.CheckBox
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector
    Me.ef_promo_name = New GUI_Controls.uc_entry_field
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.ef_promo_name)
    Me.panel_filter.Controls.Add(Me.uc_dsl)
    Me.panel_filter.Controls.Add(Me.chk_details)
    Me.panel_filter.Controls.Add(Me.uc_account_sel1)
    Me.panel_filter.Size = New System.Drawing.Size(1073, 162)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account_sel1, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_details, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dsl, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_promo_name, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 166)
    Me.panel_data.Size = New System.Drawing.Size(1073, 479)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1067, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1067, 4)
    '
    'uc_account_sel1
    '
    Me.uc_account_sel1.Account = ""
    Me.uc_account_sel1.Anon = False
    Me.uc_account_sel1.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.uc_account_sel1.Holder = ""
    Me.uc_account_sel1.Location = New System.Drawing.Point(270, 5)
    Me.uc_account_sel1.Name = "uc_account_sel1"
    Me.uc_account_sel1.Size = New System.Drawing.Size(309, 108)
    Me.uc_account_sel1.TabIndex = 2
    Me.uc_account_sel1.TrackData = ""
    '
    'chk_details
    '
    Me.chk_details.AutoSize = True
    Me.chk_details.Location = New System.Drawing.Point(7, 134)
    Me.chk_details.Name = "chk_details"
    Me.chk_details.Size = New System.Drawing.Size(72, 17)
    Me.chk_details.TabIndex = 4
    Me.chk_details.Text = "xDetails"
    Me.chk_details.UseVisualStyleBackColor = True
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.FromDate = New Date(2003, 5, 20, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.Location = New System.Drawing.Point(7, 8)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.Size = New System.Drawing.Size(257, 120)
    Me.uc_dsl.TabIndex = 1
    Me.uc_dsl.ToDate = New Date(2003, 5, 20, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    '
    'ef_promo_name
    '
    Me.ef_promo_name.DoubleValue = 0
    Me.ef_promo_name.IntegerValue = 0
    Me.ef_promo_name.IsReadOnly = False
    Me.ef_promo_name.Location = New System.Drawing.Point(583, 18)
    Me.ef_promo_name.Name = "ef_promo_name"
    Me.ef_promo_name.Size = New System.Drawing.Size(241, 24)
    Me.ef_promo_name.SufixText = "Sufix Text"
    Me.ef_promo_name.SufixTextVisible = True
    Me.ef_promo_name.TabIndex = 3
    Me.ef_promo_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_promo_name.TextValue = ""
    Me.ef_promo_name.Value = ""
    '
    'frm_promotion_report
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(1081, 649)
    Me.Name = "frm_promotion_report"
    Me.Text = "frm_promotion_report"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Constants "

  ' DB Columns
  Private Const SQL_COLUMN_ROW_TYPE As Integer = 0
  Private Const SQL_COLUMN_PROMOTION_NAME As Integer = 1
  Private Const SQL_COLUMN_ROW_SUBTYPE As Integer = 2
  Private Const SQL_COLUMN_MIN_CODE As Integer = 3
  Private Const SQL_COLUMN_ACCOUNT_ID As Integer = 4
  Private Const SQL_COLUMN_ACCOUNT_NAME As Integer = 5
  Private Const SQL_COLUMN_POINTS As Integer = 6
  Private Const SQL_COLUMN_CASH_IN As Integer = 7
  Private Const SQL_COLUMN_NON_REDEEMABLE As Integer = 8
  Private Const SQL_COLUMN_REDEEMABLE As Integer = 9
  ' The following column is used to open the detail screen.
  Private Const SQL_COLUMN_PROMOTION_ID As Integer = 10

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_PROMOTION_NAME As Integer = 1
  Private Const GRID_COLUMN_ACCOUNT_ID_NAME As Integer = 2
  Private Const GRID_COLUMN_POINTS As Integer = 3
  Private Const GRID_COLUMN_CASH_IN As Integer = 4
  Private Const GRID_COLUMN_NON_REDEEMABLE As Integer = 5
  Private Const GRID_COLUMN_REDEEMABLE As Integer = 6
  ' The following columns are not visible, they are used to store values needed to open the detail screen.
  Private Const GRID_COLUMN_ROW_SUBTYPE As Integer = 7
  Private Const GRID_COLUMN_PROMOTION_ID As Integer = 8
  Private Const GRID_COLUMN_ACCOUNT_ID As Integer = 9

  Private Const GRID_COLUMNS As Integer = 10
  Private Const GRID_HEADER_ROWS As Integer = 1

  ' Width
  Private Const GRID_WIDTH_INDEX As Integer = 200
  Private Const GRID_WIDTH_PROMOTION_NAME As Integer = 3100
  Private Const GRID_WIDTH_ACCOUNT As Integer = 3000
  Private Const GRID_WIDTH_AMOUNT As Integer = 2000
  Private Const GRID_WIDTH_POINTS As Integer = 1060

  ' Row types
  Private Const ROW_TYPE_TOTAL As String = "A"
  Private Const ROW_TYPE_PROMO As String = "B"
  Private Const ROW_SUBTYPE_PROMO_TOTAL As Integer = 0
  Private Const ROW_SUBTYPE_PROMO_BY_ACCOUNT As Integer = 1

  Private Const CODE_PRIZE As Integer = 7

  Private Const LEN_PROMO_NAME As Integer = 50

#End Region ' Constants

#Region " Members "

  ' For report filters 
  Private m_date_from As String
  Private m_date_to As String
  Private m_account As String
  Private m_track_data As String
  Private m_holder As String
  Private m_promo_name As String

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_PROMOTION_REPORT

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_INVOICING.GetString(345)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(1)

    ' Date time filter
    Me.uc_dsl.Init(GLB_NLS_GUI_STATISTICS.Id(204))

    Me.ef_promo_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(256)
    Me.ef_promo_name.TextVisible = True

    ' Grid
    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

    ' filters
    Me.ef_promo_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_PROMO_NAME)

    ' Details checkbox
    Me.chk_details.Text = GLB_NLS_GUI_STATISTICS.GetString(364)

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted

  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate > Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    End If

    ' Check fields on uc_account_sel are ok
    If Not Me.uc_account_sel1.ValidateFormat Then
      Return False
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database

  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim str_sql As String
    Dim str_top_level_sql As String
    Dim str_top_level_sql2 As String
    Dim str_group_by_account_sql As String
    Dim str_group_by_account_sql2 As String
    Dim str_promo_total_sql As String
    Dim str_promo_total_sql2 As String
    Dim str_total_sql As String
    Dim str_where As String
    Dim _str_promo_deteled As String

    _str_promo_deteled = GLB_NLS_GUI_INVOICING.GetString(353)

    str_top_level_sql = "SELECT " & _
                            "  ROW_TYPE " & _
                            ", PROMOTION_NAME " & _
                            ", ROW_SUBTYPE " & _
                            ", MIN_CODE " & _
                            ", ACCOUNT_ID " & _
                            ", ACCOUNT_NAME " & _
                            ", POINTS " & _
                            ", CASH_IN " & _
                            ", NON_REDEEMABLE " & _
                            ", REDEEMABLE " & _
                            ", PROMOTION_ID " & _
                        " FROM ("

    str_where = GetSqlWhere()

    ' Detailed data grouped by account
    str_group_by_account_sql = " SELECT 'B' ROW_TYPE " & _
                                     ", ISNULL(PM_NAME,'" & _str_promo_deteled & " ' + ISNULL(CAST(X.AO_PROMO_ID AS nvarchar(6)),'') ) PROMOTION_NAME " & _
                                     ", '1' ROW_SUBTYPE " & _
                                     ", MIN_CODE " & _
                                     ", X.AO_PROMO_ID PROMOTION_ID " & _
                                     ", X.AO_ACCOUNT_ID ACCOUNT_ID " & _
                                     ", AC_HOLDER_NAME ACCOUNT_NAME " & _
                                     ", X.SUM_POINTS POINTS " & _
                                     ", X.SUM_AO_CASH_IN CASH_IN " & _
                                     ", X.SUM_AO_NON_REDEEMABLE NON_REDEEMABLE " & _
                                     ", X.SUM_AO_REDEEMABLE REDEEMABLE " & _
                                 " FROM ( SELECT AO_PROMO_ID, AO_ACCOUNT_ID " & _
                                              ", MIN(AO_CODE) MIN_CODE " & _
                                              ", SUM(CASE WHEN AO_CODE = 7 THEN AO_AMOUNT ELSE 0 END) SUM_POINTS " & _
                                              ", SUM(CASE WHEN AO_CODE IN (3, 5, 110) THEN AO_AMOUNT ELSE 0 END) SUM_AO_CASH_IN " & _
                                              ", SUM(CASE WHEN AO_WON_LOCK >= 0 THEN AO_NON_REDEEMABLE ELSE 0 END) SUM_AO_NON_REDEEMABLE " & _
                                              ", SUM(CASE WHEN AO_WON_LOCK <  0 THEN AO_NON_REDEEMABLE ELSE 0 END) SUM_AO_REDEEMABLE " & _
                                          " FROM ACCOUNT_OPERATIONS " & _
                                         " WHERE AO_CODE IN (3, 5, 7, 110) AND AO_PROMO_ID IS NOT NULL "

    str_group_by_account_sql2 = " GROUP BY AO_PROMO_ID, AO_ACCOUNT_ID  ) AS X " & _
                                     " LEFT OUTER JOIN PROMOTIONS AS P " & _
                                "   ON P.PM_PROMOTION_ID = X.AO_PROMO_ID " & _
                              "  INNER JOIN ACCOUNTS AS A " & _
                                "   ON A.AC_ACCOUNT_ID = X.AO_ACCOUNT_ID "

    ' Promo total
    str_promo_total_sql = " SELECT 'B' ROW_TYPE " & _
                                ", ISNULL(PM_NAME,('" & _str_promo_deteled & " ' + ISNULL(CAST(X.AO_PROMO_ID AS nvarchar(6)),'') ) )  PROMOTION_NAME " & _
                                ", '0' ROW_SUBTYPE " & _
                                ", MIN_CODE " & _
                                ", X.AO_PROMO_ID PROMOTION_ID " & _
                                ", NULL ACCOUNT_ID " & _
                                ", NULL ACCOUNT_NAME " & _
                                ", X.SUM_POINTS POINTS " & _
                                ", X.SUM_AO_CASH_IN CASH_IN " & _
                                ", X.SUM_AO_NON_REDEEMABLE NON_REDEEMABLE " & _
                                ", X.SUM_AO_REDEEMABLE REDEEMABLE " & _
                            " FROM ( SELECT AO_PROMO_ID " & _
                                         ", MIN(AO_CODE) MIN_CODE " & _
                                         ", SUM(CASE WHEN AO_CODE = 7 THEN AO_AMOUNT ELSE 0 END) SUM_POINTS " & _
                                         ", SUM(CASE WHEN AO_CODE IN (3, 5, 110) THEN AO_AMOUNT ELSE 0 END) SUM_AO_CASH_IN " & _
                                         ", SUM(CASE WHEN AO_WON_LOCK >= 0 THEN AO_NON_REDEEMABLE ELSE 0 END) SUM_AO_NON_REDEEMABLE " & _
                                         ", SUM(CASE WHEN AO_WON_LOCK <  0 THEN AO_NON_REDEEMABLE ELSE 0 END) SUM_AO_REDEEMABLE " & _
                                     " FROM ACCOUNT_OPERATIONS " & _
                                    " WHERE AO_CODE IN (3, 5, 7, 110) AND AO_PROMO_ID IS NOT NULL "

    str_promo_total_sql2 = " GROUP BY AO_PROMO_ID ) AS X " & _
                                " LEFT OUTER JOIN PROMOTIONS AS P " & _
                           " ON P.PM_PROMOTION_ID = X.AO_PROMO_ID "

    ' Total
    str_total_sql = " SELECT 'A' ROW_TYPE " & _
                          ", NULL PROMOTION_NAME " & _
                          ", '0' ROW_SUBTYPE " & _
                          ", '0' MIN_CODE " & _
                          ", NULL PROMOTION_ID " & _
                          ", NULL ACCOUNT_ID " & _
                          ", NULL ACCOUNT_NAME " & _
                          ", SUM(CASE WHEN AO_CODE = 7 THEN AO_AMOUNT ELSE 0 END) POINTS " & _
                          ", SUM(CASE WHEN AO_CODE IN (3, 5, 110) THEN AO_AMOUNT ELSE 0 END) CASH_IN " & _
                          ", SUM(CASE WHEN AO_WON_LOCK >= 0 THEN AO_NON_REDEEMABLE ELSE 0 END) NON_REDEEMABLE " & _
                          ", SUM(CASE WHEN AO_WON_LOCK <  0 THEN AO_NON_REDEEMABLE ELSE 0 END) REDEEMABLE " & _
                      " FROM ACCOUNT_OPERATIONS " & _
                     " WHERE AO_CODE IN (3, 5, 7, 110) AND AO_PROMO_ID IS NOT NULL "

    str_top_level_sql2 = " ) Z " & _
                      " ORDER BY ROW_TYPE DESC " & _
                              ", PROMOTION_NAME ASC " & _
                              ", ROW_SUBTYPE DESC " & _
                              ", ACCOUNT_ID ASC "

    str_sql = str_top_level_sql

    ' Include grouped by account_id
    If chk_details.Checked Then
      str_sql = str_sql & str_group_by_account_sql & str_where & str_group_by_account_sql2 & " UNION "
    End If

    str_sql = str_sql & str_promo_total_sql & str_where & str_promo_total_sql2 & " UNION " & _
                        str_total_sql & str_where & str_top_level_sql2

    Return str_sql

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    ' Promotion Name
    If Not DbRow.IsNull(SQL_COLUMN_PROMOTION_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTION_NAME).Value = DbRow.Value(SQL_COLUMN_PROMOTION_NAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTION_NAME).Value = ""
    End If

    ' Promotion Id
    If Not DbRow.IsNull(SQL_COLUMN_PROMOTION_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTION_ID).Value = DbRow.Value(SQL_COLUMN_PROMOTION_ID)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTION_ID).Value = 0
    End If

    ' Account Id + Name
    If Not DbRow.IsNull(SQL_COLUMN_ACCOUNT_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_ID)

      If Not DbRow.IsNull(SQL_COLUMN_ACCOUNT_NAME) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID_NAME).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_ID) & " - " & DbRow.Value(SQL_COLUMN_ACCOUNT_NAME)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID_NAME).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_ID)
      End If
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID).Value = 0
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID_NAME).Value = ""
    End If

    ' Default values cash-in / points
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_IN).Value = ""
    Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS).Value = ""

    ' Points
    If DbRow.Value(SQL_COLUMN_MIN_CODE) = 7 Or DbRow.Value(SQL_COLUMN_ROW_TYPE) = ROW_TYPE_TOTAL Then
      If Not DbRow.IsNull(SQL_COLUMN_POINTS) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_POINTS), 0)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS).Value = GUI_FormatNumber(0, 0)
      End If
    End If

    ' Cash-In
    If DbRow.Value(SQL_COLUMN_MIN_CODE) <> 7 Or DbRow.Value(SQL_COLUMN_ROW_TYPE) = ROW_TYPE_TOTAL Then
      If Not DbRow.IsNull(SQL_COLUMN_CASH_IN) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_IN).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_CASH_IN), _
                                                                               ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_IN).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End If
    End If

    ' Non-Redeemable
    If Not DbRow.IsNull(SQL_COLUMN_NON_REDEEMABLE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NON_REDEEMABLE).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_NON_REDEEMABLE), _
                                                                                    ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NON_REDEEMABLE).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' Redeemable
    If Not DbRow.IsNull(SQL_COLUMN_REDEEMABLE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_REDEEMABLE).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_REDEEMABLE), _
                                                                                ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_REDEEMABLE).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_ROW_SUBTYPE).Value = -1

    ' Determine row color and more
    Select Case DbRow.Value(SQL_COLUMN_ROW_TYPE)

      Case ROW_TYPE_PROMO
        Me.Grid.Cell(RowIndex, GRID_COLUMN_ROW_SUBTYPE).Value = DbRow.Value(SQL_COLUMN_ROW_SUBTYPE)

        Select Case DbRow.Value(SQL_COLUMN_ROW_SUBTYPE)

          Case ROW_SUBTYPE_PROMO_BY_ACCOUNT
            Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)

          Case ROW_SUBTYPE_PROMO_TOTAL
            Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_01)
            Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID_NAME).Value = GLB_NLS_GUI_INVOICING.GetString(205)
        End Select

      Case ROW_TYPE_TOTAL
        Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID_NAME).Value = GLB_NLS_GUI_INVOICING.GetString(205)

      Case Else
        '
    End Select

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.uc_dsl
  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Button operations
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)
    Select Case ButtonId
      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        Call GUI_ShowSelectedItem()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub ' GUI_ButtonClick

  ' PURPOSE: Open additional form to show details for the select row
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ShowSelectedItem()

    Dim idx_row As Integer
    Dim from_date As Date
    Dim to_date As Date
    Dim frm_sel As frm_promotion_report_detail

    ' Search the first row selected
    For idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(idx_row).IsSelected Then
        Exit For
      End If
    Next

    ' Skip last totalizator row
    If Me.Grid.Cell(idx_row, GRID_COLUMN_ROW_SUBTYPE).Value = -1 Then
      Return
    End If

    frm_sel = New frm_promotion_report_detail

    from_date = Date.MinValue
    If Me.uc_dsl.FromDateSelected Then
      from_date = Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime)
    End If

    to_date = Date.MaxValue
    If Me.uc_dsl.ToDateSelected Then
      to_date = Me.uc_dsl.ToDate.AddHours(Me.uc_dsl.ClosingTime)
    End If

    frm_sel.ShowForEdit(Me.MdiParent, from_date, to_date, _
                        Me.Grid.Cell(idx_row, GRID_COLUMN_ROW_SUBTYPE).Value, _
                        Me.Grid.Cell(idx_row, GRID_COLUMN_PROMOTION_ID).Value, _
                        Me.Grid.Cell(idx_row, GRID_COLUMN_ACCOUNT_ID).Value, _
                        Me.Grid.Cell(idx_row, GRID_COLUMN_PROMOTION_NAME).Value, _
                        Me.Grid.Cell(idx_row, GRID_COLUMN_ACCOUNT_ID_NAME).Value)

  End Sub ' GUI_ShowSelectedItem

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_STATISTICS.GetString(309), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_STATISTICS.GetString(310), m_date_to)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), m_account)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_track_data)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), m_holder)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(256), m_promo_name)

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_date_from = ""
    m_date_to = ""

    ' Date 
    If Me.uc_dsl.FromDateSelected Then
      m_date_from = GUI_FormatDate(Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime), _
                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    If Me.uc_dsl.ToDateSelected Then
      m_date_to = GUI_FormatDate(Me.uc_dsl.ToDate.AddHours(Me.uc_dsl.ClosingTime), _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    m_account = Me.uc_account_sel1.Account()
    m_track_data = Me.uc_account_sel1.TrackData()
    m_holder = Me.uc_account_sel1.Holder()

    m_promo_name = Me.ef_promo_name.Value

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      .Sortable = False

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Promotion Name
      .Column(GRID_COLUMN_PROMOTION_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(256)
      .Column(GRID_COLUMN_PROMOTION_NAME).Width = GRID_WIDTH_PROMOTION_NAME
      .Column(GRID_COLUMN_PROMOTION_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Account
      .Column(GRID_COLUMN_ACCOUNT_ID_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_ACCOUNT_ID_NAME).Width = GRID_WIDTH_ACCOUNT
      .Column(GRID_COLUMN_ACCOUNT_ID_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Cash-In
      .Column(GRID_COLUMN_CASH_IN).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(42)
      .Column(GRID_COLUMN_CASH_IN).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_CASH_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Points
      .Column(GRID_COLUMN_POINTS).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(305)
      .Column(GRID_COLUMN_POINTS).Width = GRID_WIDTH_POINTS
      .Column(GRID_COLUMN_POINTS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Non-Redeemable
      .Column(GRID_COLUMN_NON_REDEEMABLE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(50)
      .Column(GRID_COLUMN_NON_REDEEMABLE).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_NON_REDEEMABLE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Redeemable
      .Column(GRID_COLUMN_REDEEMABLE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(337)
      .Column(GRID_COLUMN_REDEEMABLE).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_REDEEMABLE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Row Subtype
      .Column(GRID_COLUMN_ROW_SUBTYPE).Header(0).Text = ""
      .Column(GRID_COLUMN_ROW_SUBTYPE).Width = 0
      .Column(GRID_COLUMN_ROW_SUBTYPE).IsColumnPrintable = False

      ' Promotion Id
      .Column(GRID_COLUMN_PROMOTION_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_PROMOTION_ID).Width = 0
      .Column(GRID_COLUMN_PROMOTION_ID).IsColumnPrintable = False

      ' Account Id
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_ACCOUNT_ID).Width = 0
      .Column(GRID_COLUMN_ACCOUNT_ID).IsColumnPrintable = False

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub SetDefaultValues()

    Dim closing_time As Integer
    Dim final_time As Date

    final_time = WSI.Common.Misc.TodayOpening()
    closing_time = final_time.Hour

    final_time = final_time.Date
    Me.uc_dsl.ToDate = final_time
    Me.uc_dsl.ToDateSelected = True
    Me.uc_dsl.FromDate = final_time.AddDays(-1)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.ClosingTime = closing_time

    Me.chk_details.Checked = False

    Me.uc_account_sel1.Clear()

    Me.ef_promo_name.Value = ""

  End Sub ' SetDefaultValues

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the main SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Function GetSqlWhere() As String
    Dim str_where As String = ""
    Dim str_where_account As String = ""

    ' Filter Dates
    str_where = Me.uc_dsl.GetSqlFilterCondition("AO_DATETIME")
    If str_where.Length > 0 Then
      str_where = " AND " & str_where
    End If

    ' Filter Account
    str_where_account = Me.uc_account_sel1.GetFilterSQL()
    If str_where_account <> "" Then
      str_where = str_where & " AND " & _
                  " AO_ACCOUNT_ID IN (SELECT AC_ACCOUNT_ID FROM ACCOUNTS WHERE " & str_where_account & ")"
    End If

    ' Filter Promo Name
    If Me.ef_promo_name.Value <> "" Then
      str_where = str_where & " AND " & _
                  " AO_PROMO_ID IN (SELECT PM_PROMOTION_ID FROM PROMOTIONS WHERE " & GUI_FilterField("PM_NAME", Me.ef_promo_name.Value) & ")"
    End If

    Return str_where
  End Function ' GetSqlWhere

#End Region ' Private Functions

#Region " Events "

#End Region ' Events

End Class ' frm_promotion_report
