'-------------------------------------------------------------------
' Copyright � 2007-2012 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_area_edit
' DESCRIPTION:   New or Edit area item
' AUTHOR:        Susana Sams�
' CREATION DATE: 20-FEB-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 20-FEB-2012  SSC    Initial version.
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient

Public Class frm_area_edit
  Inherits frm_base_edit

#Region " Constants "
  Private Const LEN_AREA_NAME As Integer = 50
#End Region ' Constants

#Region " Members "

  Private m_input_area_name As String
  Private m_delete_operation As Boolean

#End Region ' Members

#Region " Overrides "

  ' PURPOSE : Initializes the form id.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_AREA_EDIT

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE : Form controls initialization.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS  :

  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    ' Set form Name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(573)                      ' 573 "Edici�n de �rea"

    ' Delete button is only available whe editing, not while creating
    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False '(Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT)

    ef_area_name.Text = GLB_NLS_GUI_CONFIGURATION.GetString(435)              ' 435 "Area"
    chk_smoking.Text = GLB_NLS_GUI_CONFIGURATION.GetString(436)               ' 436 "Fumadores"

    ef_area_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_AREA_NAME)


  End Sub ' GUI_InitControls

  ' PURPOSE : Validate the data presented on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    ' The following values must be entered
    '   - Name : Mandator

    If ef_area_name.Value.Length = 0 Then
      ' 123 "Debe introducir un valor."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_area_name.Text)
      Call ef_area_name.Focus()
      Return False

    End If

    Return True

  End Function ' GUI_IsScreenDataOk

  ' PURPOSE : Set initial data on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _area As CLASS_AREA
    _area = DbReadObject

    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT And _area.AreaId = 0 Then
      GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Enabled = False
      GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = False
      ef_area_name.Enabled = False
      Me.chk_smoking.Enabled = False
    End If

    Me.ef_area_name.Value = _area.Name
    Me.chk_smoking.Checked = _area.Smoking


  End Sub ' GUI_SetScreenData

  ' PURPOSE : Get data from the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_GetScreenData()

    Dim _area As CLASS_AREA
 
    _area = DbEditedObject

    _area.Name = ef_area_name.Value
    _area.Smoking = chk_smoking.Checked

  End Sub ' GUI_GetScreenData

  ' PURPOSE : Database overridable operations. 
  '           Define specific DB operation for this form.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Dim _area As CLASS_AREA
    Dim _aux_nls As String
    Dim _rc As mdl_NLS.ENUM_MB_RESULT
    Dim _table As DataTable
    Dim _bln_islands As Boolean

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_AREA
        _area = DbEditedObject
        _area.AreaId = 0

        DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          ' 143 "Se ha producido un error al leer el area %1."
          _area = Me.DbEditedObject
          _aux_nls = m_input_area_name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(143), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          ' 144 "Ya existe una area con el nombre %1."
          _area = Me.DbEditedObject
          _aux_nls = _area.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(144), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
          Call ef_area_name.Focus()
        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          ' 145 "Se ha producido un error al a�adir el area %1."
          _area = Me.DbEditedObject
          _aux_nls = _area.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(145), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          ' 144 "Ya existe un area con el nombre %1."
          _area = Me.DbEditedObject
          _aux_nls = _area.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(144), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
          Call ef_area_name.Focus()

        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          ' 146 "Se ha producido un error al modificar el area %1."
          _area = Me.DbEditedObject
          _aux_nls = _area.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(146), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE
        m_delete_operation = False
        _bln_islands = False
        _area = Me.DbEditedObject
        _aux_nls = _area.Name

        _table = GUI_GetTableUsingSQL("SELECT COUNT(*) FROM BANKS WHERE BK_AREA_ID = " & _area.AreaId, Integer.MaxValue)
        ' 150 "No se puede borrar el �rea %1 porque tiene islas asociadas"
        If Not IsNothing(_table) Then
          If _table.Rows(0).Item(0) > 0 Then
            _rc = NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(150), _
                                     ENUM_MB_TYPE.MB_TYPE_WARNING, _
                                     ENUM_MB_BTN.MB_BTN_OK, _
                                     ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
                                    _aux_nls)
            _bln_islands = True
          End If
        End If

        ' 148 "�Est� seguro que quiere borrar el area %1?"
        If Not _bln_islands Then
          _rc = NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(148), _
                           ENUM_MB_TYPE.MB_TYPE_WARNING, _
                           ENUM_MB_BTN.MB_BTN_YES_NO, _
                           ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
                          _aux_nls)

          If _rc = ENUM_MB_RESULT.MB_RESULT_YES Then
            m_delete_operation = True
          End If
        End If


      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK And m_delete_operation Then
          ' 147 "Se ha producido un error al borrar el area %1."
          _area = Me.DbEditedObject
          _aux_nls = _area.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(147), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_DELETE
        If m_delete_operation Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        Else
          DbStatus = ENUM_STATUS.STATUS_ERROR
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE : Database overridable operations. 
  '           Define specific DB operation for this form.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :

  ' GUI_DB_Operation

  ' PURPOSE : Manage permissions.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)
  End Sub ' GUI_Permissions

  ' PURPOSE : Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.ef_area_name

  End Sub ' GUI_SetInitialFocus

  ' PURPOSE : Init form in new mode
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '

  Public Overloads Sub ShowNewItem(ByVal AreaId As Integer)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    DbObjectId = Nothing
    DbStatus = ENUM_STATUS.STATUS_OK

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowNewItem

  ' PURPOSE : Init form in edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '       - UserId
  '       - Username
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Public Overloads Sub ShowEditItem(ByVal AreaId As Integer)

    Dim _table As DataTable
    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Me.DbObjectId = AreaId

    _table = GUI_GetTableUsingSQL("SELECT AR_NAME FROM AREAS WHERE AR_AREA_ID = " & AreaId, Integer.MaxValue)

    If Not IsNothing(_table) Then
      If _table.Rows.Count() > 0 Then
        Me.m_input_area_name = _table.Rows.Item(0).Item("AR_NAME")
      End If
    End If

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If
  End Sub ' ShowEditItem

#End Region ' Overrides

#Region " Private functions "


#End Region 'Private functions

#Region " Events "


#End Region

End Class