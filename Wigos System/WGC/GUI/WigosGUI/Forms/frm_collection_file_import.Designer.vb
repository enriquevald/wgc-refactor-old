<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_collection_file_import
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_imported = New System.Windows.Forms.GroupBox
    Me.dtp_imported = New GUI_Controls.uc_date_picker
    Me.ef_name = New GUI_Controls.uc_entry_field
    Me.ef_file_2_path = New GUI_Controls.uc_entry_field
    Me.btn_open_file_2_dialog = New System.Windows.Forms.Button
    Me.ef_file_1_path = New GUI_Controls.uc_entry_field
    Me.btn_open_file_1_dialog = New System.Windows.Forms.Button
    Me.gb_output = New System.Windows.Forms.GroupBox
    Me.pgb_import_progress = New System.Windows.Forms.ProgressBar
    Me.tb_output = New System.Windows.Forms.TextBox
    Me.panel_data.SuspendLayout()
    Me.gb_imported.SuspendLayout()
    Me.gb_output.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_output)
    Me.panel_data.Controls.Add(Me.gb_imported)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(669, 505)
    '
    'gb_imported
    '
    Me.gb_imported.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.gb_imported.Controls.Add(Me.dtp_imported)
    Me.gb_imported.Controls.Add(Me.ef_name)
    Me.gb_imported.Controls.Add(Me.ef_file_2_path)
    Me.gb_imported.Controls.Add(Me.btn_open_file_2_dialog)
    Me.gb_imported.Controls.Add(Me.ef_file_1_path)
    Me.gb_imported.Controls.Add(Me.btn_open_file_1_dialog)
    Me.gb_imported.Location = New System.Drawing.Point(3, 3)
    Me.gb_imported.Name = "gb_imported"
    Me.gb_imported.Size = New System.Drawing.Size(662, 150)
    Me.gb_imported.TabIndex = 2
    Me.gb_imported.TabStop = False
    Me.gb_imported.Text = "xImported"
    '
    'dtp_imported
    '
    Me.dtp_imported.Checked = False
    Me.dtp_imported.IsReadOnly = False
    Me.dtp_imported.Location = New System.Drawing.Point(6, 20)
    Me.dtp_imported.Name = "dtp_imported"
    Me.dtp_imported.ShowCheckBox = False
    Me.dtp_imported.ShowUpDown = False
    Me.dtp_imported.Size = New System.Drawing.Size(229, 24)
    Me.dtp_imported.SufixText = "Sufix Text"
    Me.dtp_imported.SufixTextVisible = True
    Me.dtp_imported.TabIndex = 0
    Me.dtp_imported.TextWidth = 120
    Me.dtp_imported.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'ef_name
    '
    Me.ef_name.DoubleValue = 0
    Me.ef_name.IntegerValue = 0
    Me.ef_name.IsReadOnly = False
    Me.ef_name.Location = New System.Drawing.Point(6, 110)
    Me.ef_name.Name = "ef_name"
    Me.ef_name.PlaceHolder = Nothing
    Me.ef_name.Size = New System.Drawing.Size(601, 24)
    Me.ef_name.SufixText = "Sufix Text"
    Me.ef_name.SufixTextVisible = True
    Me.ef_name.TabIndex = 5
    Me.ef_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_name.TextValue = ""
    Me.ef_name.TextWidth = 120
    Me.ef_name.Value = ""
    Me.ef_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_file_2_path
    '
    Me.ef_file_2_path.DoubleValue = 0
    Me.ef_file_2_path.IntegerValue = 0
    Me.ef_file_2_path.IsReadOnly = False
    Me.ef_file_2_path.Location = New System.Drawing.Point(6, 80)
    Me.ef_file_2_path.Name = "ef_file_2_path"
    Me.ef_file_2_path.PlaceHolder = Nothing
    Me.ef_file_2_path.Size = New System.Drawing.Size(601, 24)
    Me.ef_file_2_path.SufixText = "Sufix Text"
    Me.ef_file_2_path.SufixTextVisible = True
    Me.ef_file_2_path.TabIndex = 3
    Me.ef_file_2_path.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_file_2_path.TextValue = ""
    Me.ef_file_2_path.TextWidth = 120
    Me.ef_file_2_path.Value = ""
    Me.ef_file_2_path.ValueForeColor = System.Drawing.Color.Blue
    '
    'btn_open_file_2_dialog
    '
    Me.btn_open_file_2_dialog.Location = New System.Drawing.Point(613, 80)
    Me.btn_open_file_2_dialog.Name = "btn_open_file_2_dialog"
    Me.btn_open_file_2_dialog.Size = New System.Drawing.Size(27, 24)
    Me.btn_open_file_2_dialog.TabIndex = 4
    Me.btn_open_file_2_dialog.Text = "..."
    Me.btn_open_file_2_dialog.UseVisualStyleBackColor = True
    '
    'ef_file_1_path
    '
    Me.ef_file_1_path.DoubleValue = 0
    Me.ef_file_1_path.IntegerValue = 0
    Me.ef_file_1_path.IsReadOnly = False
    Me.ef_file_1_path.Location = New System.Drawing.Point(6, 50)
    Me.ef_file_1_path.Name = "ef_file_1_path"
    Me.ef_file_1_path.PlaceHolder = Nothing
    Me.ef_file_1_path.Size = New System.Drawing.Size(601, 24)
    Me.ef_file_1_path.SufixText = "Sufix Text"
    Me.ef_file_1_path.SufixTextVisible = True
    Me.ef_file_1_path.TabIndex = 1
    Me.ef_file_1_path.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_file_1_path.TextValue = ""
    Me.ef_file_1_path.TextWidth = 120
    Me.ef_file_1_path.Value = ""
    Me.ef_file_1_path.ValueForeColor = System.Drawing.Color.Blue
    '
    'btn_open_file_1_dialog
    '
    Me.btn_open_file_1_dialog.Location = New System.Drawing.Point(613, 50)
    Me.btn_open_file_1_dialog.Name = "btn_open_file_1_dialog"
    Me.btn_open_file_1_dialog.Size = New System.Drawing.Size(27, 24)
    Me.btn_open_file_1_dialog.TabIndex = 2
    Me.btn_open_file_1_dialog.Text = "..."
    Me.btn_open_file_1_dialog.UseVisualStyleBackColor = True
    '
    'gb_output
    '
    Me.gb_output.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.gb_output.Controls.Add(Me.pgb_import_progress)
    Me.gb_output.Controls.Add(Me.tb_output)
    Me.gb_output.Location = New System.Drawing.Point(3, 159)
    Me.gb_output.Name = "gb_output"
    Me.gb_output.Size = New System.Drawing.Size(662, 342)
    Me.gb_output.TabIndex = 4
    Me.gb_output.TabStop = False
    Me.gb_output.Text = "xOutput"
    '
    'pgb_import_progress
    '
    Me.pgb_import_progress.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.pgb_import_progress.Location = New System.Drawing.Point(6, 312)
    Me.pgb_import_progress.Name = "pgb_import_progress"
    Me.pgb_import_progress.Size = New System.Drawing.Size(650, 23)
    Me.pgb_import_progress.TabIndex = 1
    '
    'tb_output
    '
    Me.tb_output.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.tb_output.Location = New System.Drawing.Point(6, 21)
    Me.tb_output.Multiline = True
    Me.tb_output.Name = "tb_output"
    Me.tb_output.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.tb_output.Size = New System.Drawing.Size(650, 291)
    Me.tb_output.TabIndex = 0
    '
    'frm_collection_file_import
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(773, 516)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.MaximumSize = New System.Drawing.Size(779, 544)
    Me.MinimumSize = New System.Drawing.Size(779, 544)
    Me.Name = "frm_collection_file_import"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_collection_file_import"
    Me.panel_data.ResumeLayout(False)
    Me.gb_imported.ResumeLayout(False)
    Me.gb_output.ResumeLayout(False)
    Me.gb_output.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_imported As System.Windows.Forms.GroupBox
  Friend WithEvents ef_file_2_path As GUI_Controls.uc_entry_field
  Friend WithEvents btn_open_file_2_dialog As System.Windows.Forms.Button
  Friend WithEvents ef_file_1_path As GUI_Controls.uc_entry_field
  Friend WithEvents btn_open_file_1_dialog As System.Windows.Forms.Button
  Friend WithEvents ef_name As GUI_Controls.uc_entry_field
  Friend WithEvents dtp_imported As GUI_Controls.uc_date_picker
  Friend WithEvents gb_output As System.Windows.Forms.GroupBox
  Friend WithEvents pgb_import_progress As System.Windows.Forms.ProgressBar
  Friend WithEvents tb_output As System.Windows.Forms.TextBox
End Class
