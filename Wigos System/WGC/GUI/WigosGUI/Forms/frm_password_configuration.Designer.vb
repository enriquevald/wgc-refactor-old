<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_password_configuration
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.ef_number_of_login_attempt = New GUI_Controls.uc_entry_field
    Me.ef_days_without_login = New GUI_Controls.uc_entry_field
    Me.gb_block_account = New System.Windows.Forms.GroupBox
    Me.gb_session_timeout = New System.Windows.Forms.GroupBox
    Me.ef_Cachier_timeout = New GUI_Controls.uc_entry_field
    Me.ef_GUI_timeout = New GUI_Controls.uc_entry_field
    Me.tf_indicate_default_value = New GUI_Controls.uc_text_field
    Me.gb_password_rules = New System.Windows.Forms.GroupBox
    Me.gb_account = New System.Windows.Forms.GroupBox
    Me.ef_change_pass_in_x_days = New GUI_Controls.uc_entry_field
    Me.ef_number_of_password_history = New GUI_Controls.uc_entry_field
    Me.ef_login_number_character = New GUI_Controls.uc_entry_field
    Me.gb_passwod_feaures = New System.Windows.Forms.GroupBox
    Me.ef_number_of_lowercase = New GUI_Controls.uc_entry_field
    Me.ef_passw_number_character = New GUI_Controls.uc_entry_field
    Me.ef_number_of_digits = New GUI_Controls.uc_entry_field
    Me.ef_number_of_uppercase = New GUI_Controls.uc_entry_field
    Me.ef_number_special_character = New GUI_Controls.uc_entry_field
    Me.chk_check_password_rules = New System.Windows.Forms.CheckBox
    Me.panel_data.SuspendLayout()
    Me.gb_block_account.SuspendLayout()
    Me.gb_session_timeout.SuspendLayout()
    Me.gb_password_rules.SuspendLayout()
    Me.gb_account.SuspendLayout()
    Me.gb_passwod_feaures.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_password_rules)
    Me.panel_data.Controls.Add(Me.gb_session_timeout)
    Me.panel_data.Controls.Add(Me.gb_block_account)
    Me.panel_data.Size = New System.Drawing.Size(444, 589)
    '
    'ef_number_of_login_attempt
    '
    Me.ef_number_of_login_attempt.DoubleValue = 0
    Me.ef_number_of_login_attempt.IntegerValue = 0
    Me.ef_number_of_login_attempt.IsReadOnly = False
    Me.ef_number_of_login_attempt.Location = New System.Drawing.Point(26, 15)
    Me.ef_number_of_login_attempt.Name = "ef_number_of_login_attempt"
    Me.ef_number_of_login_attempt.Size = New System.Drawing.Size(372, 24)
    Me.ef_number_of_login_attempt.SufixText = "Sufix Text"
    Me.ef_number_of_login_attempt.SufixTextVisible = True
    Me.ef_number_of_login_attempt.TabIndex = 0
    Me.ef_number_of_login_attempt.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_number_of_login_attempt.TextValue = ""
    Me.ef_number_of_login_attempt.TextWidth = 300
    Me.ef_number_of_login_attempt.Value = ""
    '
    'ef_days_without_login
    '
    Me.ef_days_without_login.DoubleValue = 0
    Me.ef_days_without_login.IntegerValue = 0
    Me.ef_days_without_login.IsReadOnly = False
    Me.ef_days_without_login.Location = New System.Drawing.Point(26, 45)
    Me.ef_days_without_login.Name = "ef_days_without_login"
    Me.ef_days_without_login.Size = New System.Drawing.Size(372, 24)
    Me.ef_days_without_login.SufixText = "Sufix Text"
    Me.ef_days_without_login.SufixTextVisible = True
    Me.ef_days_without_login.TabIndex = 1
    Me.ef_days_without_login.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_days_without_login.TextValue = ""
    Me.ef_days_without_login.TextWidth = 300
    Me.ef_days_without_login.Value = ""
    '
    'gb_block_account
    '
    Me.gb_block_account.Controls.Add(Me.ef_number_of_login_attempt)
    Me.gb_block_account.Controls.Add(Me.ef_days_without_login)
    Me.gb_block_account.Location = New System.Drawing.Point(13, 416)
    Me.gb_block_account.Name = "gb_block_account"
    Me.gb_block_account.Size = New System.Drawing.Size(422, 77)
    Me.gb_block_account.TabIndex = 1
    Me.gb_block_account.TabStop = False
    Me.gb_block_account.Text = "xBlockAccount"
    '
    'gb_session_timeout
    '
    Me.gb_session_timeout.Controls.Add(Me.ef_Cachier_timeout)
    Me.gb_session_timeout.Controls.Add(Me.ef_GUI_timeout)
    Me.gb_session_timeout.Location = New System.Drawing.Point(13, 499)
    Me.gb_session_timeout.Name = "gb_session_timeout"
    Me.gb_session_timeout.Size = New System.Drawing.Size(422, 81)
    Me.gb_session_timeout.TabIndex = 2
    Me.gb_session_timeout.TabStop = False
    Me.gb_session_timeout.Text = "xSessionTimeOut"
    '
    'ef_Cachier_timeout
    '
    Me.ef_Cachier_timeout.DoubleValue = 0
    Me.ef_Cachier_timeout.IntegerValue = 0
    Me.ef_Cachier_timeout.IsReadOnly = False
    Me.ef_Cachier_timeout.Location = New System.Drawing.Point(6, 46)
    Me.ef_Cachier_timeout.Name = "ef_Cachier_timeout"
    Me.ef_Cachier_timeout.Size = New System.Drawing.Size(393, 25)
    Me.ef_Cachier_timeout.SufixText = "Sufix Text"
    Me.ef_Cachier_timeout.SufixTextVisible = True
    Me.ef_Cachier_timeout.TabIndex = 1
    Me.ef_Cachier_timeout.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_Cachier_timeout.TextValue = ""
    Me.ef_Cachier_timeout.TextWidth = 330
    Me.ef_Cachier_timeout.Value = ""
    '
    'ef_GUI_timeout
    '
    Me.ef_GUI_timeout.DoubleValue = 0
    Me.ef_GUI_timeout.IntegerValue = 0
    Me.ef_GUI_timeout.IsReadOnly = False
    Me.ef_GUI_timeout.Location = New System.Drawing.Point(6, 18)
    Me.ef_GUI_timeout.Name = "ef_GUI_timeout"
    Me.ef_GUI_timeout.Size = New System.Drawing.Size(392, 25)
    Me.ef_GUI_timeout.SufixText = "Sufix Text"
    Me.ef_GUI_timeout.SufixTextVisible = True
    Me.ef_GUI_timeout.TabIndex = 0
    Me.ef_GUI_timeout.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_GUI_timeout.TextValue = ""
    Me.ef_GUI_timeout.TextWidth = 330
    Me.ef_GUI_timeout.Value = ""
    '
    'tf_indicate_default_value
    '
    Me.tf_indicate_default_value.AutoSize = True
    Me.tf_indicate_default_value.IsReadOnly = True
    Me.tf_indicate_default_value.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_indicate_default_value.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_indicate_default_value.Location = New System.Drawing.Point(8, 363)
    Me.tf_indicate_default_value.Name = "tf_indicate_default_value"
    Me.tf_indicate_default_value.Size = New System.Drawing.Size(270, 24)
    Me.tf_indicate_default_value.SufixText = "Sufix Text"
    Me.tf_indicate_default_value.SufixTextVisible = True
    Me.tf_indicate_default_value.TabIndex = 0
    Me.tf_indicate_default_value.TabStop = False
    Me.tf_indicate_default_value.TextWidth = 0
    Me.tf_indicate_default_value.Value = "xDefault"
    '
    'gb_password_rules
    '
    Me.gb_password_rules.Controls.Add(Me.gb_account)
    Me.gb_password_rules.Controls.Add(Me.gb_passwod_feaures)
    Me.gb_password_rules.Controls.Add(Me.chk_check_password_rules)
    Me.gb_password_rules.Controls.Add(Me.tf_indicate_default_value)
    Me.gb_password_rules.Location = New System.Drawing.Point(13, 14)
    Me.gb_password_rules.Name = "gb_password_rules"
    Me.gb_password_rules.Size = New System.Drawing.Size(422, 393)
    Me.gb_password_rules.TabIndex = 0
    Me.gb_password_rules.TabStop = False
    Me.gb_password_rules.Text = "xPasswordRules"
    '
    'gb_account
    '
    Me.gb_account.Controls.Add(Me.ef_change_pass_in_x_days)
    Me.gb_account.Controls.Add(Me.ef_number_of_password_history)
    Me.gb_account.Controls.Add(Me.ef_login_number_character)
    Me.gb_account.Location = New System.Drawing.Point(8, 43)
    Me.gb_account.Name = "gb_account"
    Me.gb_account.Size = New System.Drawing.Size(400, 115)
    Me.gb_account.TabIndex = 1
    Me.gb_account.TabStop = False
    Me.gb_account.Text = "xCuenta"
    '
    'ef_change_pass_in_x_days
    '
    Me.ef_change_pass_in_x_days.DoubleValue = 0
    Me.ef_change_pass_in_x_days.IntegerValue = 0
    Me.ef_change_pass_in_x_days.IsReadOnly = False
    Me.ef_change_pass_in_x_days.Location = New System.Drawing.Point(18, 78)
    Me.ef_change_pass_in_x_days.Name = "ef_change_pass_in_x_days"
    Me.ef_change_pass_in_x_days.Size = New System.Drawing.Size(372, 24)
    Me.ef_change_pass_in_x_days.SufixText = "Sufix Text"
    Me.ef_change_pass_in_x_days.SufixTextVisible = True
    Me.ef_change_pass_in_x_days.TabIndex = 2
    Me.ef_change_pass_in_x_days.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_change_pass_in_x_days.TextValue = ""
    Me.ef_change_pass_in_x_days.TextWidth = 300
    Me.ef_change_pass_in_x_days.Value = ""
    '
    'ef_number_of_password_history
    '
    Me.ef_number_of_password_history.DoubleValue = 0
    Me.ef_number_of_password_history.IntegerValue = 0
    Me.ef_number_of_password_history.IsReadOnly = False
    Me.ef_number_of_password_history.Location = New System.Drawing.Point(8, 48)
    Me.ef_number_of_password_history.Name = "ef_number_of_password_history"
    Me.ef_number_of_password_history.Size = New System.Drawing.Size(382, 24)
    Me.ef_number_of_password_history.SufixText = "Sufix Text"
    Me.ef_number_of_password_history.SufixTextVisible = True
    Me.ef_number_of_password_history.TabIndex = 1
    Me.ef_number_of_password_history.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_number_of_password_history.TextValue = ""
    Me.ef_number_of_password_history.TextWidth = 310
    Me.ef_number_of_password_history.Value = ""
    '
    'ef_login_number_character
    '
    Me.ef_login_number_character.DoubleValue = 0
    Me.ef_login_number_character.IntegerValue = 0
    Me.ef_login_number_character.IsReadOnly = False
    Me.ef_login_number_character.Location = New System.Drawing.Point(118, 18)
    Me.ef_login_number_character.Name = "ef_login_number_character"
    Me.ef_login_number_character.Size = New System.Drawing.Size(272, 24)
    Me.ef_login_number_character.SufixText = "Sufix Text"
    Me.ef_login_number_character.SufixTextVisible = True
    Me.ef_login_number_character.TabIndex = 0
    Me.ef_login_number_character.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_login_number_character.TextValue = ""
    Me.ef_login_number_character.TextWidth = 200
    Me.ef_login_number_character.Value = ""
    '
    'gb_passwod_feaures
    '
    Me.gb_passwod_feaures.Controls.Add(Me.ef_number_of_lowercase)
    Me.gb_passwod_feaures.Controls.Add(Me.ef_passw_number_character)
    Me.gb_passwod_feaures.Controls.Add(Me.ef_number_of_digits)
    Me.gb_passwod_feaures.Controls.Add(Me.ef_number_of_uppercase)
    Me.gb_passwod_feaures.Controls.Add(Me.ef_number_special_character)
    Me.gb_passwod_feaures.Location = New System.Drawing.Point(8, 168)
    Me.gb_passwod_feaures.Name = "gb_passwod_feaures"
    Me.gb_passwod_feaures.Size = New System.Drawing.Size(400, 189)
    Me.gb_passwod_feaures.TabIndex = 2
    Me.gb_passwod_feaures.TabStop = False
    Me.gb_passwod_feaures.Text = "xPasswodFeaures"
    '
    'ef_number_of_lowercase
    '
    Me.ef_number_of_lowercase.DoubleValue = 0
    Me.ef_number_of_lowercase.IntegerValue = 0
    Me.ef_number_of_lowercase.IsReadOnly = False
    Me.ef_number_of_lowercase.Location = New System.Drawing.Point(18, 86)
    Me.ef_number_of_lowercase.Name = "ef_number_of_lowercase"
    Me.ef_number_of_lowercase.Size = New System.Drawing.Size(372, 24)
    Me.ef_number_of_lowercase.SufixText = "Sufix Text"
    Me.ef_number_of_lowercase.SufixTextVisible = True
    Me.ef_number_of_lowercase.TabIndex = 2
    Me.ef_number_of_lowercase.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_number_of_lowercase.TextValue = ""
    Me.ef_number_of_lowercase.TextWidth = 300
    Me.ef_number_of_lowercase.Value = ""
    '
    'ef_passw_number_character
    '
    Me.ef_passw_number_character.DoubleValue = 0
    Me.ef_passw_number_character.IntegerValue = 0
    Me.ef_passw_number_character.IsReadOnly = False
    Me.ef_passw_number_character.Location = New System.Drawing.Point(118, 26)
    Me.ef_passw_number_character.Name = "ef_passw_number_character"
    Me.ef_passw_number_character.Size = New System.Drawing.Size(272, 24)
    Me.ef_passw_number_character.SufixText = "Sufix Text"
    Me.ef_passw_number_character.SufixTextVisible = True
    Me.ef_passw_number_character.TabIndex = 0
    Me.ef_passw_number_character.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_passw_number_character.TextValue = ""
    Me.ef_passw_number_character.TextWidth = 200
    Me.ef_passw_number_character.Value = ""
    '
    'ef_number_of_digits
    '
    Me.ef_number_of_digits.DoubleValue = 0
    Me.ef_number_of_digits.IntegerValue = 0
    Me.ef_number_of_digits.IsReadOnly = False
    Me.ef_number_of_digits.Location = New System.Drawing.Point(118, 56)
    Me.ef_number_of_digits.Name = "ef_number_of_digits"
    Me.ef_number_of_digits.Size = New System.Drawing.Size(272, 24)
    Me.ef_number_of_digits.SufixText = "Sufix Text"
    Me.ef_number_of_digits.SufixTextVisible = True
    Me.ef_number_of_digits.TabIndex = 1
    Me.ef_number_of_digits.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_number_of_digits.TextValue = ""
    Me.ef_number_of_digits.TextWidth = 200
    Me.ef_number_of_digits.Value = ""
    '
    'ef_number_of_uppercase
    '
    Me.ef_number_of_uppercase.DoubleValue = 0
    Me.ef_number_of_uppercase.IntegerValue = 0
    Me.ef_number_of_uppercase.IsReadOnly = False
    Me.ef_number_of_uppercase.Location = New System.Drawing.Point(18, 116)
    Me.ef_number_of_uppercase.Name = "ef_number_of_uppercase"
    Me.ef_number_of_uppercase.Size = New System.Drawing.Size(372, 24)
    Me.ef_number_of_uppercase.SufixText = "Sufix Text"
    Me.ef_number_of_uppercase.SufixTextVisible = True
    Me.ef_number_of_uppercase.TabIndex = 3
    Me.ef_number_of_uppercase.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_number_of_uppercase.TextValue = ""
    Me.ef_number_of_uppercase.TextWidth = 300
    Me.ef_number_of_uppercase.Value = ""
    '
    'ef_number_special_character
    '
    Me.ef_number_special_character.DoubleValue = 0
    Me.ef_number_special_character.IntegerValue = 0
    Me.ef_number_special_character.IsReadOnly = False
    Me.ef_number_special_character.Location = New System.Drawing.Point(18, 146)
    Me.ef_number_special_character.Name = "ef_number_special_character"
    Me.ef_number_special_character.Size = New System.Drawing.Size(372, 24)
    Me.ef_number_special_character.SufixText = "Sufix Text"
    Me.ef_number_special_character.SufixTextVisible = True
    Me.ef_number_special_character.TabIndex = 4
    Me.ef_number_special_character.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_number_special_character.TextValue = ""
    Me.ef_number_special_character.TextWidth = 300
    Me.ef_number_special_character.Value = ""
    '
    'chk_check_password_rules
    '
    Me.chk_check_password_rules.Location = New System.Drawing.Point(8, 20)
    Me.chk_check_password_rules.Name = "chk_check_password_rules"
    Me.chk_check_password_rules.Size = New System.Drawing.Size(214, 17)
    Me.chk_check_password_rules.TabIndex = 0
    Me.chk_check_password_rules.Text = "xRules"
    Me.chk_check_password_rules.UseVisualStyleBackColor = True
    '
    'frm_password_configuration
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(546, 598)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_password_configuration"
    Me.Text = "frm_password_configuration"
    Me.panel_data.ResumeLayout(False)
    Me.gb_block_account.ResumeLayout(False)
    Me.gb_session_timeout.ResumeLayout(False)
    Me.gb_password_rules.ResumeLayout(False)
    Me.gb_password_rules.PerformLayout()
    Me.gb_account.ResumeLayout(False)
    Me.gb_passwod_feaures.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_days_without_login As GUI_Controls.uc_entry_field
  Friend WithEvents ef_number_of_login_attempt As GUI_Controls.uc_entry_field
  Friend WithEvents gb_block_account As System.Windows.Forms.GroupBox
  Friend WithEvents gb_session_timeout As System.Windows.Forms.GroupBox
  Friend WithEvents ef_GUI_timeout As GUI_Controls.uc_entry_field
  Friend WithEvents ef_Cachier_timeout As GUI_Controls.uc_entry_field
  Friend WithEvents tf_indicate_default_value As GUI_Controls.uc_text_field
  Friend WithEvents gb_password_rules As System.Windows.Forms.GroupBox
  Friend WithEvents gb_account As System.Windows.Forms.GroupBox
  Friend WithEvents ef_change_pass_in_x_days As GUI_Controls.uc_entry_field
  Friend WithEvents ef_number_of_password_history As GUI_Controls.uc_entry_field
  Friend WithEvents ef_login_number_character As GUI_Controls.uc_entry_field
  Friend WithEvents gb_passwod_feaures As System.Windows.Forms.GroupBox
  Friend WithEvents ef_number_of_lowercase As GUI_Controls.uc_entry_field
  Friend WithEvents ef_passw_number_character As GUI_Controls.uc_entry_field
  Friend WithEvents ef_number_of_digits As GUI_Controls.uc_entry_field
  Friend WithEvents ef_number_of_uppercase As GUI_Controls.uc_entry_field
  Friend WithEvents ef_number_special_character As GUI_Controls.uc_entry_field
  Friend WithEvents chk_check_password_rules As System.Windows.Forms.CheckBox
End Class
