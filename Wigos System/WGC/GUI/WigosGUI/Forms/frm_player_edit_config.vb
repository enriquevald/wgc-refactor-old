'-------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_player_edit_config
' DESCRIPTION:   Configure required fields and limits for account personalization
' AUTHOR:        Jaume Barn�s
' CREATION DATE: 07-JUL-2013
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 07-JUL-2014 JBC    First Release.
' 21-JUL-2014 JBC    Fixed Bug WIG-1100: Without beneficiary, form crash
' 08-SEP-2014 JBC    Fixed Bug WIG-1234: Now AML only allow check RFC
' 23-SEP-2014 SGB    Fixed Bug WIG-1271: check if the beneficiary is active to fill their fields
' 13-AUG-2015 AMF    Backlog Item 3296: Add address country in account
' 22-SEP-2015 GMV    Backlog Item 4535: Bot�n de aceptaci�n deshabilitado en el formulario de configuraci�n de datos de cuenta
' 14-DEC-2015 FOS    Backlog Item 1858: MultiSite MultiCurrency:Accounts Information
' 02-FEB-2016 FOS    Backlog Item 1858: MultiSite MultiCurrency:Accounts Information, enabled ok button when system is not multisite.
' 03-OCT-2017 AMF    WIGOS-5478 Accounts - Change literals
' 13-FEB-2018 EOR    Bug 31515: WIGOS-8032 [Ticket #12229] Formulario de inscripci�n de jugadores en registro por l�mite de pago 
'-------------------------------------------------------------------
Option Strict Off
Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common.Misc
Imports GUI_CommonOperations.CLASS_BASE
Imports WSI.Common.Users
Imports WSI.Common
Imports System.Text
Imports System.Data.SqlClient


Public Class frm_player_edit_config
  Inherits GUI_Controls.frm_base_edit

#Region " Const "
  Private Const FORM_DB_MIN_VERSION As Short = 219

  Private Const GRID_COLUMN_SENTINEL As Integer = 0
  Private Const GRID_COLUMN_ID As Integer = 1
  Private Const GRID_COLUMN_NAME As Integer = 2
  Private Const GRID_COLUMN_VISIBLE As Integer = 3
  Private Const GRID_COLUMN_REQUIRED As Integer = 4
  Private Const GRID_COLUMN_AML As Integer = 5

  Private Const GRID_PRINCIPAL_COLUMN_REQUIRED_SCAN As Integer = 5
  Private Const GRID_PRINCIPAL_COLUMN_REQUIRED_AML As Integer = 6
  Private Const GRID_PRINCIPAL_COLUMN_REQUIRED_AML_SCAN As Integer = 7
  Private Const GRID_BENEF_COLUMN_REQUIRED As Integer = 8
  Private Const GRID_BENEF_COLUMN_REQUIRED_SCAN As Integer = 9
  Private Const GRID_BENEF_COLUMN_REQUIRED_AML As Integer = 10
  Private Const GRID_BENEF_COLUMN_REQUIRED_AML_SCAN As Integer = 11

  Private Const GRID_COLUMNS As Integer = 6
  Private Const GRID_COLUMNS_DOC As Integer = 12

  Private Const GRID_HEADER_ROWS As Integer = 1

  Private Const GRID_PRINCIPAL_TOTAL_ROWS = 14
  Private Const GRID_DOCUMENTS_TOTAL_ROWS = 13
  Private Const GRID_ADDRESS_TOTAL_ROWS = 7
  Private Const GRID_CONTACT_TOTAL_ROWS = 4
  Private Const GRID_BENEFICIARY_TOTAL_ROWS = 7

  Private Const GRID_COLUMN_WIDTH_SENTINEL As Integer = 200
  Private Const GRID_COLUMN_WIDTH_ID As Integer = 0
  Private Const GRID_COLUMN_WIDTH_NAME As Integer = 7000
  Private Const GRID_COLUMN_WIDTH_VISIBLE As Integer = 1000
  Private Const GRID_COLUMN_WIDTH_REQUIRED As Integer = 1000
  Private Const GRID_COLUMN_WIDTH_AML As Integer = 1000
  Private Const GRID_COLUMN_WIDTH_REQUIRED_SCAN As Integer = 1000
  Private Const GRID_COLUMN_WIDTH_AML_SCAN As Integer = 1000

  ' PRINCIPAL GRID
  Private Const GRID_ROW_NAME3 As Integer = 0
  Private Const GRID_ROW_NAME4 As Integer = 1
  Private Const GRID_ROW_NAME1 As Integer = 2
  Private Const GRID_ROW_NAME2 As Integer = 3
  Private Const GRID_ROW_DOCUMENT As Integer = 4
  Private Const GRID_ROW_DOCSCAN As Integer = 5
  Private Const GRID_ROW_BIRTHDATE As Integer = 6
  Private Const GRID_ROW_OCCUPATION As Integer = 7
  Private Const GRID_ROW_GENDER As Integer = 8
  Private Const GRID_ROW_NACIONALITY As Integer = 9
  Private Const GRID_ROW_BIRTHCOUNTRY As Integer = 10
  Private Const GRID_ROW_MARITAL_STATUS As Integer = 11
  Private Const GRID_ROW_WEDDING_DATE As Integer = 12
  Private Const GRID_ROW_PHOTO As Integer = 13
  Private Const GRID_ROW_EXPIRATION_DOCUMENT_DATE As Integer = 14

  ' CONTACT  GRID
  Private Const GRID_ROW_PHONE1 As Integer = 0
  Private Const GRID_ROW_PHONE2 As Integer = 1
  Private Const GRID_ROW_EMAIL1 As Integer = 2
  Private Const GRID_ROW_EMAIL2 As Integer = 3
  Private Const GRID_ROW_TWITTER As Integer = 4

  ' ADDRESS  GRID
  Private Const GRID_ROW_STREET As Integer = 0
  Private Const GRID_ROW_EXT_NUMBER As Integer = 1
  Private Const GRID_ROW_POSTAL_CODE As Integer = 2
  Private Const GRID_ROW_COLONY As Integer = 3
  Private Const GRID_ROW_DELEGATION As Integer = 4
  Private Const GRID_ROW_MUNICIP As Integer = 5
  Private Const GRID_ROW_ADDRESS_COUNTRY As Integer = 6
  Private Const GRID_ROW_FED_ENTITY As Integer = 7

  ' BENEFICIARY  GRID
  Private Const GRID_ROW_BENEFICIARY_NAME3 As Integer = 0
  Private Const GRID_ROW_BENEFICIARY_NAME1 As Integer = 1
  Private Const GRID_ROW_BENEFICIARY_NAME2 As Integer = 2
  Private Const GRID_ROW_BENEFICIARY_DOCUMENT As Integer = 3
  Private Const GRID_ROW_BENEFICIARY_DOCSCAN As Integer = 4
  Private Const GRID_ROW_BENEFICIARY_BIRTHDATE As Integer = 5
  Private Const GRID_ROW_BENEFICIARY_OCCUPATION As Integer = 6
  Private Const GRID_ROW_BENEFICIARY_GENDER As Integer = 7

  ' IDENTIFICATION TYPES COLUMNS
  Private Const SQL_DT_COLUMN_ID As Integer = 0
  Private Const SQL_DT_COLUMN_ENABLED As Integer = 1
  Private Const SQL_DT_COLUMN_NAME As Integer = 2

#End Region

#Region " Members "
  Private m_documents_visible As Dictionary(Of String, String)
  Private m_AML_name As String
  Private m_has_beneficiary As Boolean
  Private m_default_document As String
  Private m_AML_enabled As Boolean
  Private m_disable_rfc_and_curp As Boolean
  Private m_is_multisite_member As Boolean
  Private m_is_multisite_enabled As Boolean

  Private Event CellDataChange(ByVal Row As System.Int32, ByVal Column As System.Int32)
#End Region

#Region "Overrides"

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()
    Dim _iso_code As String

    MyBase.GUI_InitControls()

#If DEBUG Then
    Call WSI.Common.GeneralParam.ReloadGP()
#End If

    _iso_code = WSI.Common.Resource.CountryISOCode2()

    m_disable_rfc_and_curp = IIf(_iso_code = "MX", True, False)

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5084) '"Configuraci�n de datos de cliente"

    m_AML_name = Accounts.getAMLName()

    Me.gb_default_values.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5088) '"Valores por defecto"
    Me.cb_default_country.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5090) '"Pa�s"
    Me.cb_default_document.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5089) '"Documento"

    m_documents_visible = New Dictionary(Of String, String)

    'insert combo empty value
    Me.cb_default_document.Add(0, "")
    Me.cb_default_country.Add(0, "")

    Me.m_has_beneficiary = WSI.Common.Misc.HasBeneficiaryAndNotAMLExternalControl()
    Me.m_AML_enabled = GeneralParam.GetBoolean("AntiMoneyLaundering", "Enabled", False) And Not GeneralParam.GetBoolean("AntiMoneyLaundering", "ExternalControl.Enabled", False)

    If Not Me.m_has_beneficiary Then
      Me.tab_config.TabPages.Remove(Me.xBeneficiary)
    End If

    Call StyleSheet()

    Me.chk_acount_customize_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2552)
    Me.chk_ask_before_print.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2791)

    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False


    ' GMV - 22/09/2015 - defect TFS 4535
    Me.lbl_edition.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2507, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2478)) 'Edicion solo en multisite


    m_is_multisite_member = GeneralParam.GetBoolean("Site", "MultiSiteMember", False)

    If m_is_multisite_member Then
      Me.lbl_edition.Visible = True

      m_is_multisite_enabled = WSI.Common.CurrencyMultisite.IsEnabledMultiCurrency
      GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = m_is_multisite_enabled
    End If

  End Sub

  ' PURPOSE : Override form permissions to disallow writting in MultiSite Member
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As GUI_Controls.CLASS_GUI_USER.TYPE_PERMISSIONS)
    MyBase.GUI_Permissions(AndPerm)


    m_is_multisite_enabled = WSI.Common.CurrencyMultisite.IsEnabledMultiCurrency

    If (m_is_multisite_enabled) Then
      AndPerm.Write = m_is_multisite_enabled
    Else
      m_is_multisite_member = GeneralParam.GetBoolean("Site", "MultiSiteMember", False)
      AndPerm.Write = Not m_is_multisite_member
    End If

  End Sub

  ' PURPOSE : Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_PLAYER_EDIT_CONFIG

    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)

    Call MyBase.GUI_SetFormId()
  End Sub

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)

    Select Case DbOperation

      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLS_REQUIRED_FIELDS()

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2479), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub

  '  PURPOSE: Set the screen data based on Database object
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    Dim _config_data As CLS_REQUIRED_FIELDS

    _config_data = DbEditedObject

    Me.chk_acount_customize_enabled.Checked = IIf(IIf(String.IsNullOrEmpty(_config_data.AccountRequiredField.AccountCustomizeEnabled), "0", _config_data.AccountRequiredField.AccountCustomizeEnabled) > 0, True, False)
    Me.chk_ask_before_print.Checked = IIf(IIf(String.IsNullOrEmpty(_config_data.AccountRequiredField.AccountCustomizeEnabled), "0", _config_data.AccountRequiredField.AccountCustomizeEnabled) = "2", True, False)

    Call PopulateConfigRows(_config_data)
    Call PopulateIdentificationTypes(_config_data)

    ' Populate Combo of Countries
    For Each _column As KeyValuePair(Of String, String) In _config_data.ListOfCountries
      Me.cb_default_country.Add(_column.Key.ToString())
      If _column.Value = _config_data.DefaultCountry Then 'Set combo default value
        Me.cb_default_country.Value = Me.cb_default_country.Count
      End If
    Next

    For Each _column As KeyValuePair(Of String, String) In m_documents_visible
      If _column.Key = _config_data.DefaultDocument Then
        Me.m_default_document = _column.Value
      End If
    Next

    Call RefreshCombo()
    Call ProcessDocumentsCheck()

    For _idx_row As Integer = 0 To Me.dg_principal_documents_config.NumRows - 1
      RaiseEvent CellDataChange(_idx_row, GRID_COLUMN_VISIBLE)
    Next

  End Sub

  '  PURPOSE: Get data in the form
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_GetScreenData()
    Dim _required_field As CLS_REQUIRED_FIELDS

    _required_field = DbEditedObject

    _required_field.DefaultCountry = Me.cb_default_country.TextValue
    _required_field.DefaultDocument = Me.cb_default_document.TextValue

    With Me.dg_principal_config

      _required_field.AccountRequiredField.Name3.Visible = GridValuetoBool(.Cell(GRID_ROW_NAME3, GRID_COLUMN_VISIBLE).Value)
      _required_field.AccountRequiredField.Name3.Required = GridValuetoBool(.Cell(GRID_ROW_NAME3, GRID_COLUMN_REQUIRED).Value)
      _required_field.AccountRequiredField.Name3.AML = GridValuetoBool(.Cell(GRID_ROW_NAME3, GRID_COLUMN_AML).Value)

      _required_field.AccountRequiredField.Name4.Visible = GridValuetoBool(.Cell(GRID_ROW_NAME4, GRID_COLUMN_VISIBLE).Value)
      _required_field.AccountRequiredField.Name4.Required = GridValuetoBool(.Cell(GRID_ROW_NAME4, GRID_COLUMN_REQUIRED).Value)
      _required_field.AccountRequiredField.Name4.AML = GridValuetoBool(.Cell(GRID_ROW_NAME4, GRID_COLUMN_AML).Value)

      _required_field.AccountRequiredField.Name1.Visible = GridValuetoBool(.Cell(GRID_ROW_NAME1, GRID_COLUMN_VISIBLE).Value)
      _required_field.AccountRequiredField.Name1.Required = GridValuetoBool(.Cell(GRID_ROW_NAME1, GRID_COLUMN_REQUIRED).Value)
      _required_field.AccountRequiredField.Name1.AML = GridValuetoBool(.Cell(GRID_ROW_NAME1, GRID_COLUMN_AML).Value)

      _required_field.AccountRequiredField.Name2.Visible = GridValuetoBool(.Cell(GRID_ROW_NAME2, GRID_COLUMN_VISIBLE).Value)
      _required_field.AccountRequiredField.Name2.Required = GridValuetoBool(.Cell(GRID_ROW_NAME2, GRID_COLUMN_REQUIRED).Value)
      _required_field.AccountRequiredField.Name2.AML = GridValuetoBool(.Cell(GRID_ROW_NAME2, GRID_COLUMN_AML).Value)

      _required_field.AccountRequiredField.Document.Visible = GridValuetoBool(.Cell(GRID_ROW_DOCUMENT, GRID_COLUMN_VISIBLE).Value)
      _required_field.AccountRequiredField.Document.Required = GridValuetoBool(.Cell(GRID_ROW_DOCUMENT, GRID_COLUMN_REQUIRED).Value)
      _required_field.AccountRequiredField.Document.AML = GridValuetoBool(.Cell(GRID_ROW_DOCUMENT, GRID_COLUMN_AML).Value)

      _required_field.AccountRequiredField.DocScan.Visible = GridValuetoBool(.Cell(GRID_ROW_DOCSCAN, GRID_COLUMN_VISIBLE).Value)
      _required_field.AccountRequiredField.DocScan.Required = GridValuetoBool(.Cell(GRID_ROW_DOCSCAN, GRID_COLUMN_REQUIRED).Value)
      _required_field.AccountRequiredField.DocScan.AML = GridValuetoBool(.Cell(GRID_ROW_DOCSCAN, GRID_COLUMN_AML).Value)

      _required_field.AccountRequiredField.Birthdate.Visible = GridValuetoBool(.Cell(GRID_ROW_BIRTHDATE, GRID_COLUMN_VISIBLE).Value)
      _required_field.AccountRequiredField.Birthdate.Required = GridValuetoBool(.Cell(GRID_ROW_BIRTHDATE, GRID_COLUMN_REQUIRED).Value)
      _required_field.AccountRequiredField.Birthdate.AML = GridValuetoBool(.Cell(GRID_ROW_BIRTHDATE, GRID_COLUMN_AML).Value)

      _required_field.AccountRequiredField.Occupation.Visible = GridValuetoBool(.Cell(GRID_ROW_OCCUPATION, GRID_COLUMN_VISIBLE).Value)
      _required_field.AccountRequiredField.Occupation.Required = GridValuetoBool(.Cell(GRID_ROW_OCCUPATION, GRID_COLUMN_REQUIRED).Value)
      _required_field.AccountRequiredField.Occupation.AML = GridValuetoBool(.Cell(GRID_ROW_OCCUPATION, GRID_COLUMN_AML).Value)

      _required_field.AccountRequiredField.Gender.Visible = GridValuetoBool(.Cell(GRID_ROW_GENDER, GRID_COLUMN_VISIBLE).Value)
      _required_field.AccountRequiredField.Gender.Required = GridValuetoBool(.Cell(GRID_ROW_GENDER, GRID_COLUMN_REQUIRED).Value)
      _required_field.AccountRequiredField.Gender.AML = GridValuetoBool(.Cell(GRID_ROW_GENDER, GRID_COLUMN_AML).Value)

      _required_field.AccountRequiredField.Nationality.Visible = GridValuetoBool(.Cell(GRID_ROW_NACIONALITY, GRID_COLUMN_VISIBLE).Value)
      _required_field.AccountRequiredField.Nationality.Required = GridValuetoBool(.Cell(GRID_ROW_NACIONALITY, GRID_COLUMN_REQUIRED).Value)
      _required_field.AccountRequiredField.Nationality.AML = GridValuetoBool(.Cell(GRID_ROW_NACIONALITY, GRID_COLUMN_AML).Value)

      _required_field.AccountRequiredField.BirthCountry.Visible = GridValuetoBool(.Cell(GRID_ROW_BIRTHCOUNTRY, GRID_COLUMN_VISIBLE).Value)
      _required_field.AccountRequiredField.BirthCountry.Required = GridValuetoBool(.Cell(GRID_ROW_BIRTHCOUNTRY, GRID_COLUMN_REQUIRED).Value)
      _required_field.AccountRequiredField.BirthCountry.AML = GridValuetoBool(.Cell(GRID_ROW_BIRTHCOUNTRY, GRID_COLUMN_AML).Value)

      _required_field.AccountRequiredField.MaritalStatus.Visible = GridValuetoBool(.Cell(GRID_ROW_MARITAL_STATUS, GRID_COLUMN_VISIBLE).Value)
      _required_field.AccountRequiredField.MaritalStatus.Required = GridValuetoBool(.Cell(GRID_ROW_MARITAL_STATUS, GRID_COLUMN_REQUIRED).Value)
      _required_field.AccountRequiredField.MaritalStatus.AML = GridValuetoBool(.Cell(GRID_ROW_MARITAL_STATUS, GRID_COLUMN_AML).Value)

      _required_field.AccountRequiredField.WeddingDate.Visible = GridValuetoBool(.Cell(GRID_ROW_WEDDING_DATE, GRID_COLUMN_VISIBLE).Value)
      _required_field.AccountRequiredField.WeddingDate.Required = GridValuetoBool(.Cell(GRID_ROW_WEDDING_DATE, GRID_COLUMN_REQUIRED).Value)
      _required_field.AccountRequiredField.WeddingDate.AML = GridValuetoBool(.Cell(GRID_ROW_WEDDING_DATE, GRID_COLUMN_AML).Value)

      _required_field.AccountRequiredField.Photo.Visible = GridValuetoBool(.Cell(GRID_ROW_PHOTO, GRID_COLUMN_VISIBLE).Value)
      _required_field.AccountRequiredField.Photo.Required = GridValuetoBool(.Cell(GRID_ROW_PHOTO, GRID_COLUMN_REQUIRED).Value)
      _required_field.AccountRequiredField.Photo.AML = GridValuetoBool(.Cell(GRID_ROW_PHOTO, GRID_COLUMN_AML).Value)

      _required_field.AccountRequiredField.ExpirationDocumentDate.Visible = GridValuetoBool(.Cell(GRID_ROW_EXPIRATION_DOCUMENT_DATE, GRID_COLUMN_VISIBLE).Value)
      _required_field.AccountRequiredField.ExpirationDocumentDate.Required = GridValuetoBool(.Cell(GRID_ROW_EXPIRATION_DOCUMENT_DATE, GRID_COLUMN_REQUIRED).Value)
      _required_field.AccountRequiredField.ExpirationDocumentDate.AML = GridValuetoBool(.Cell(GRID_ROW_EXPIRATION_DOCUMENT_DATE, GRID_COLUMN_AML).Value)

    End With


    ' CONTACT

    With Me.dg_contact_config
      _required_field.AccountRequiredField.Phone1.Visible = GridValuetoBool(.Cell(GRID_ROW_PHONE1, GRID_COLUMN_VISIBLE).Value)
      _required_field.AccountRequiredField.Phone1.Required = GridValuetoBool(.Cell(GRID_ROW_PHONE1, GRID_COLUMN_REQUIRED).Value)
      _required_field.AccountRequiredField.Phone1.AML = GridValuetoBool(.Cell(GRID_ROW_PHONE1, GRID_COLUMN_AML).Value)

      _required_field.AccountRequiredField.Phone2.Visible = GridValuetoBool(.Cell(GRID_ROW_PHONE2, GRID_COLUMN_VISIBLE).Value)
      _required_field.AccountRequiredField.Phone2.Required = GridValuetoBool(.Cell(GRID_ROW_PHONE2, GRID_COLUMN_REQUIRED).Value)
      _required_field.AccountRequiredField.Phone2.AML = GridValuetoBool(.Cell(GRID_ROW_PHONE2, GRID_COLUMN_AML).Value)

      _required_field.AccountRequiredField.Email1.Visible = GridValuetoBool(.Cell(GRID_ROW_EMAIL1, GRID_COLUMN_VISIBLE).Value)
      _required_field.AccountRequiredField.Email1.Required = GridValuetoBool(.Cell(GRID_ROW_EMAIL1, GRID_COLUMN_REQUIRED).Value)
      _required_field.AccountRequiredField.Email1.AML = GridValuetoBool(.Cell(GRID_ROW_EMAIL1, GRID_COLUMN_AML).Value)

      _required_field.AccountRequiredField.Email2.Visible = GridValuetoBool(.Cell(GRID_ROW_EMAIL2, GRID_COLUMN_VISIBLE).Value)
      _required_field.AccountRequiredField.Email2.Required = GridValuetoBool(.Cell(GRID_ROW_EMAIL2, GRID_COLUMN_REQUIRED).Value)
      _required_field.AccountRequiredField.Email2.AML = GridValuetoBool(.Cell(GRID_ROW_EMAIL2, GRID_COLUMN_AML).Value)

      _required_field.AccountRequiredField.TwitterAccount.Visible = GridValuetoBool(.Cell(GRID_ROW_TWITTER, GRID_COLUMN_VISIBLE).Value)
      _required_field.AccountRequiredField.TwitterAccount.Required = GridValuetoBool(.Cell(GRID_ROW_TWITTER, GRID_COLUMN_REQUIRED).Value)
      _required_field.AccountRequiredField.TwitterAccount.AML = GridValuetoBool(.Cell(GRID_ROW_TWITTER, GRID_COLUMN_AML).Value)
    End With

    '' ADDRESS

    With Me.dg_address_config
      _required_field.AccountRequiredField.Address01.Visible = GridValuetoBool(.Cell(GRID_ROW_STREET, GRID_COLUMN_VISIBLE).Value)
      _required_field.AccountRequiredField.Address01.Required = GridValuetoBool(.Cell(GRID_ROW_STREET, GRID_COLUMN_REQUIRED).Value)
      _required_field.AccountRequiredField.Address01.AML = GridValuetoBool(.Cell(GRID_ROW_STREET, GRID_COLUMN_AML).Value)

      _required_field.AccountRequiredField.ExtNum.Visible = GridValuetoBool(.Cell(GRID_ROW_EXT_NUMBER, GRID_COLUMN_VISIBLE).Value)
      _required_field.AccountRequiredField.ExtNum.Required = GridValuetoBool(.Cell(GRID_ROW_EXT_NUMBER, GRID_COLUMN_REQUIRED).Value)
      _required_field.AccountRequiredField.ExtNum.AML = GridValuetoBool(.Cell(GRID_ROW_EXT_NUMBER, GRID_COLUMN_AML).Value)

      _required_field.AccountRequiredField.Address02.Visible = GridValuetoBool(.Cell(GRID_ROW_COLONY, GRID_COLUMN_VISIBLE).Value)
      _required_field.AccountRequiredField.Address02.Required = GridValuetoBool(.Cell(GRID_ROW_COLONY, GRID_COLUMN_REQUIRED).Value)
      _required_field.AccountRequiredField.Address02.AML = GridValuetoBool(.Cell(GRID_ROW_COLONY, GRID_COLUMN_AML).Value)

      _required_field.AccountRequiredField.Address03.Visible = GridValuetoBool(.Cell(GRID_ROW_DELEGATION, GRID_COLUMN_VISIBLE).Value)
      _required_field.AccountRequiredField.Address03.Required = GridValuetoBool(.Cell(GRID_ROW_DELEGATION, GRID_COLUMN_REQUIRED).Value)
      _required_field.AccountRequiredField.Address03.AML = GridValuetoBool(.Cell(GRID_ROW_DELEGATION, GRID_COLUMN_AML).Value)

      _required_field.AccountRequiredField.City.Visible = GridValuetoBool(.Cell(GRID_ROW_MUNICIP, GRID_COLUMN_VISIBLE).Value)
      _required_field.AccountRequiredField.City.Required = GridValuetoBool(.Cell(GRID_ROW_MUNICIP, GRID_COLUMN_REQUIRED).Value)
      _required_field.AccountRequiredField.City.AML = GridValuetoBool(.Cell(GRID_ROW_MUNICIP, GRID_COLUMN_AML).Value)

      _required_field.AccountRequiredField.Zip.Visible = GridValuetoBool(.Cell(GRID_ROW_POSTAL_CODE, GRID_COLUMN_VISIBLE).Value)
      _required_field.AccountRequiredField.Zip.Required = GridValuetoBool(.Cell(GRID_ROW_POSTAL_CODE, GRID_COLUMN_REQUIRED).Value)
      _required_field.AccountRequiredField.Zip.AML = GridValuetoBool(.Cell(GRID_ROW_POSTAL_CODE, GRID_COLUMN_AML).Value)

      _required_field.AccountRequiredField.AddressCountry.Visible = GridValuetoBool(.Cell(GRID_ROW_ADDRESS_COUNTRY, GRID_COLUMN_VISIBLE).Value)
      _required_field.AccountRequiredField.AddressCountry.Required = GridValuetoBool(.Cell(GRID_ROW_ADDRESS_COUNTRY, GRID_COLUMN_REQUIRED).Value)
      _required_field.AccountRequiredField.AddressCountry.AML = GridValuetoBool(.Cell(GRID_ROW_ADDRESS_COUNTRY, GRID_COLUMN_AML).Value)

      _required_field.AccountRequiredField.FedEntity.Visible = GridValuetoBool(.Cell(GRID_ROW_FED_ENTITY, GRID_COLUMN_VISIBLE).Value)
      _required_field.AccountRequiredField.FedEntity.Required = GridValuetoBool(.Cell(GRID_ROW_FED_ENTITY, GRID_COLUMN_REQUIRED).Value)
      _required_field.AccountRequiredField.FedEntity.AML = GridValuetoBool(.Cell(GRID_ROW_FED_ENTITY, GRID_COLUMN_AML).Value)
    End With

    If m_has_beneficiary Then
      '' BENEFICIARY

      With Me.dg_beneficiary_config
        _required_field.BeneficiaryRequiredField.Name3.Visible = GridValuetoBool(.Cell(GRID_ROW_BENEFICIARY_NAME3, GRID_COLUMN_VISIBLE).Value)
        _required_field.BeneficiaryRequiredField.Name3.Required = GridValuetoBool(.Cell(GRID_ROW_BENEFICIARY_NAME3, GRID_COLUMN_REQUIRED).Value)
        _required_field.BeneficiaryRequiredField.Name3.AML = GridValuetoBool(.Cell(GRID_ROW_BENEFICIARY_NAME3, GRID_COLUMN_AML).Value)

        _required_field.BeneficiaryRequiredField.Name1.Visible = GridValuetoBool(.Cell(GRID_ROW_BENEFICIARY_NAME1, GRID_COLUMN_VISIBLE).Value)
        _required_field.BeneficiaryRequiredField.Name1.Required = GridValuetoBool(.Cell(GRID_ROW_BENEFICIARY_NAME1, GRID_COLUMN_REQUIRED).Value)
        _required_field.BeneficiaryRequiredField.Name1.AML = GridValuetoBool(.Cell(GRID_ROW_BENEFICIARY_NAME1, GRID_COLUMN_AML).Value)

        _required_field.BeneficiaryRequiredField.Name2.Visible = GridValuetoBool(.Cell(GRID_ROW_BENEFICIARY_NAME2, GRID_COLUMN_VISIBLE).Value)
        _required_field.BeneficiaryRequiredField.Name2.Required = GridValuetoBool(.Cell(GRID_ROW_BENEFICIARY_NAME2, GRID_COLUMN_REQUIRED).Value)
        _required_field.BeneficiaryRequiredField.Name2.AML = GridValuetoBool(.Cell(GRID_ROW_BENEFICIARY_NAME2, GRID_COLUMN_AML).Value)

        _required_field.BeneficiaryRequiredField.Document.Visible = GridValuetoBool(.Cell(GRID_ROW_BENEFICIARY_DOCUMENT, GRID_COLUMN_VISIBLE).Value)
        _required_field.BeneficiaryRequiredField.Document.Required = GridValuetoBool(.Cell(GRID_ROW_BENEFICIARY_DOCUMENT, GRID_COLUMN_REQUIRED).Value)
        _required_field.BeneficiaryRequiredField.Document.AML = GridValuetoBool(.Cell(GRID_ROW_BENEFICIARY_DOCUMENT, GRID_COLUMN_AML).Value)

        _required_field.BeneficiaryRequiredField.DocScan.Visible = GridValuetoBool(.Cell(GRID_ROW_BENEFICIARY_DOCSCAN, GRID_COLUMN_VISIBLE).Value)
        _required_field.BeneficiaryRequiredField.DocScan.Required = GridValuetoBool(.Cell(GRID_ROW_BENEFICIARY_DOCSCAN, GRID_COLUMN_REQUIRED).Value)
        _required_field.BeneficiaryRequiredField.DocScan.AML = GridValuetoBool(.Cell(GRID_ROW_BENEFICIARY_DOCSCAN, GRID_COLUMN_AML).Value)

        _required_field.BeneficiaryRequiredField.Birthdate.Visible = GridValuetoBool(.Cell(GRID_ROW_BENEFICIARY_BIRTHDATE, GRID_COLUMN_VISIBLE).Value)
        _required_field.BeneficiaryRequiredField.Birthdate.Required = GridValuetoBool(.Cell(GRID_ROW_BENEFICIARY_BIRTHDATE, GRID_COLUMN_REQUIRED).Value)
        _required_field.BeneficiaryRequiredField.Birthdate.AML = GridValuetoBool(.Cell(GRID_ROW_BENEFICIARY_BIRTHDATE, GRID_COLUMN_AML).Value)

        _required_field.BeneficiaryRequiredField.Occupation.Visible = GridValuetoBool(.Cell(GRID_ROW_BENEFICIARY_OCCUPATION, GRID_COLUMN_VISIBLE).Value)
        _required_field.BeneficiaryRequiredField.Occupation.Required = GridValuetoBool(.Cell(GRID_ROW_BENEFICIARY_OCCUPATION, GRID_COLUMN_REQUIRED).Value)
        _required_field.BeneficiaryRequiredField.Occupation.AML = GridValuetoBool(.Cell(GRID_ROW_BENEFICIARY_OCCUPATION, GRID_COLUMN_AML).Value)

        _required_field.BeneficiaryRequiredField.Gender.Visible = GridValuetoBool(.Cell(GRID_ROW_BENEFICIARY_GENDER, GRID_COLUMN_VISIBLE).Value)
        _required_field.BeneficiaryRequiredField.Gender.Required = GridValuetoBool(.Cell(GRID_ROW_BENEFICIARY_GENDER, GRID_COLUMN_REQUIRED).Value)
        _required_field.BeneficiaryRequiredField.Gender.AML = GridValuetoBool(.Cell(GRID_ROW_BENEFICIARY_GENDER, GRID_COLUMN_AML).Value)
      End With
    End If

    ' Bottom checks
    If Me.chk_acount_customize_enabled.Checked And Me.chk_ask_before_print.Checked Then
      _required_field.AccountRequiredField.AccountCustomizeEnabled = 2 ' "2"
    ElseIf chk_acount_customize_enabled.Checked Then
      _required_field.AccountRequiredField.AccountCustomizeEnabled = 1 ' "1"
    Else
      _required_field.AccountRequiredField.AccountCustomizeEnabled = 0 ' "0"
    End If

    'Fill Documents Data
    Call GetDataFromDocuments(_required_field)

  End Sub

  ' PURPOSE: Validate the data presented on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Dim _document_config As Integer
    Dim _has_id_checked As Boolean

    _has_id_checked = False

    If String.IsNullOrEmpty(Me.cb_default_document.TextValue) Then
      Me.tab_config.SelectedTab = Me.documents
      Me.cb_default_document.Focus()
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.cb_default_document.Text)

      Return False
    End If

    For _document_config = 1 To dg_principal_documents_config.NumRows - 1
      If GridValuetoBool(dg_principal_documents_config.Cell(_document_config, GRID_PRINCIPAL_COLUMN_REQUIRED_SCAN).Value) Then
        _has_id_checked = True

        Exit For
      End If
    Next

    If GridValuetoBool(dg_principal_config.Cell(GRID_ROW_DOCSCAN, GRID_COLUMN_REQUIRED).Value) And Not _has_id_checked Then
      Me.tab_config.SelectedTab = Me.documents
      Me.dg_principal_documents_config.Focus()
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7820), ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.cb_default_document.Text)

      Return False
    End If

    Return True
  End Function ' GUI_IsScreenDataOk

#End Region

#Region " Private Functions "

  ' PURPOSE:  Cast from boolean to uc_grid checked
  '
  '
  ' RETURNS:
  '     - None
  Private Sub StyleSheet()

    Try
      ' TAB PAGES
      Me.tab_config.TabPages(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(749)  'Documentos
      Me.tab_config.TabPages(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1713) 'Principal
      Me.tab_config.TabPages(2).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1714) 'Contacto
      Me.tab_config.TabPages(3).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1715) 'Direcci�n

      ' DOCUMENTS TABPAGE
      Call StyleSheetDocuments(Me.dg_principal_documents_config)

      ' PRINCIPAL,CONTACT AND ADDRESS TABPAGE
      Call StyleSheetConfig(Me.dg_principal_config)
      Call StyleSheetConfig(Me.dg_contact_config)
      Call StyleSheetConfig(Me.dg_address_config)

      If m_has_beneficiary Then
        Call StyleSheetConfig(Me.dg_beneficiary_config)
        Me.dg_beneficiary_config.Column(GRID_COLUMN_VISIBLE).Editable = False
        Me.tab_config.TabPages(4).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2060) '"Direcci�n"
      End If

    Catch _ex As Exception
      Log.Exception(_ex)

    End Try
  End Sub

  ' PURPOSE:  Fill Main, Address, Contact and Beneficiary grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - Data: CLS_REQUIRED_FIELDS
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub PopulateConfigRows(ByVal Data As CLS_REQUIRED_FIELDS)

    For _idx_row As Integer = 0 To GRID_PRINCIPAL_TOTAL_ROWS
      ' PRINCIPAL
      Me.dg_principal_config.AddRow()
    Next

    With Me.dg_principal_config

      .Cell(GRID_ROW_NAME3, GRID_COLUMN_SENTINEL).Value = ""
      .Cell(GRID_ROW_NAME3, GRID_COLUMN_ID).Value = ""
      .Cell(GRID_ROW_NAME3, GRID_COLUMN_NAME).Value = AccountFields.GetName() 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(5032) '"Nombre"
      .Cell(GRID_ROW_NAME3, GRID_COLUMN_VISIBLE).Value = uc_grid.GRID_CHK_CHECKED_DISABLED 'BoolToGridValue(Data.AccountRequiredField.Name3.Visible)
      .Cell(GRID_ROW_NAME3, GRID_COLUMN_REQUIRED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED 'BoolToGridValue(Data.AccountRequiredField.Name3.Required)
      .Cell(GRID_ROW_NAME3, GRID_COLUMN_AML).Value = uc_grid.GRID_CHK_CHECKED_DISABLED 'BoolToGridValue(Data.AccountRequiredField.Name3.AML)

      .Cell(GRID_ROW_NAME4, GRID_COLUMN_SENTINEL).Value = ""
      .Cell(GRID_ROW_NAME4, GRID_COLUMN_ID).Value = ""
      .Cell(GRID_ROW_NAME4, GRID_COLUMN_NAME).Value = AccountFields.GetMiddleName()  'GLB_NLS_GUI_PLAYER_TRACKING.GetString(5087) '"Segundo Nombre"
      .Cell(GRID_ROW_NAME4, GRID_COLUMN_VISIBLE).Value = BoolToGridValue(Data.AccountRequiredField.Name4.Visible)
      .Cell(GRID_ROW_NAME4, GRID_COLUMN_REQUIRED).Value = BoolToGridValue(Data.AccountRequiredField.Name4.Required) 'BoolToGridValue(Data.AccountRequiredField.Name4.Required)
      .Cell(GRID_ROW_NAME4, GRID_COLUMN_AML).Value = uc_grid.GRID_CHK_UNCHECKED_DISABLED 'BoolToGridValue(Data.AccountRequiredField.Name4.AML)

      .Cell(GRID_ROW_NAME1, GRID_COLUMN_SENTINEL).Value = ""
      .Cell(GRID_ROW_NAME1, GRID_COLUMN_ID).Value = ""
      .Cell(GRID_ROW_NAME1, GRID_COLUMN_NAME).Value = AccountFields.GetSurname1() 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(1719) '"Apellido 1"
      .Cell(GRID_ROW_NAME1, GRID_COLUMN_VISIBLE).Value = uc_grid.GRID_CHK_CHECKED_DISABLED 'BoolToGridValue(Data.AccountRequiredField.Name1.Visible)
      .Cell(GRID_ROW_NAME1, GRID_COLUMN_REQUIRED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED 'BoolToGridValue(Data.AccountRequiredField.Name1.Required)
      .Cell(GRID_ROW_NAME1, GRID_COLUMN_AML).Value = uc_grid.GRID_CHK_CHECKED_DISABLED 'BoolToGridValue(Data.AccountRequiredField.Name1.AML)

      .Cell(GRID_ROW_NAME2, GRID_COLUMN_SENTINEL).Value = ""
      .Cell(GRID_ROW_NAME2, GRID_COLUMN_ID).Value = ""
      .Cell(GRID_ROW_NAME2, GRID_COLUMN_NAME).Value = AccountFields.GetSurname2()  'GLB_NLS_GUI_PLAYER_TRACKING.GetString(1720)
      .Cell(GRID_ROW_NAME2, GRID_COLUMN_VISIBLE).Value = BoolToGridValue(Data.AccountRequiredField.Name2.Visible)
      .Cell(GRID_ROW_NAME2, GRID_COLUMN_REQUIRED).Value = BoolToGridValue(Data.AccountRequiredField.Name2.Required)
      .Cell(GRID_ROW_NAME2, GRID_COLUMN_AML).Value = BoolToGridValue(Data.AccountRequiredField.Name2.AML)

      .Cell(GRID_ROW_DOCUMENT, GRID_COLUMN_SENTINEL).Value = ""
      .Cell(GRID_ROW_DOCUMENT, GRID_COLUMN_ID).Value = ""
      .Cell(GRID_ROW_DOCUMENT, GRID_COLUMN_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1566) '"Documento"
      .Cell(GRID_ROW_DOCUMENT, GRID_COLUMN_VISIBLE).Value = uc_grid.GRID_CHK_CHECKED_DISABLED 'BoolToGridValue(Data.AccountRequiredField.Document.Visible)
      .Cell(GRID_ROW_DOCUMENT, GRID_COLUMN_REQUIRED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED 'BoolToGridValue(Data.AccountRequiredField.Document.Required)
      .Cell(GRID_ROW_DOCUMENT, GRID_COLUMN_AML).Value = uc_grid.GRID_CHK_CHECKED_DISABLED 'BoolToGridValue(Data.AccountRequiredField.Document.AML)

      .Cell(GRID_ROW_DOCSCAN, GRID_COLUMN_SENTINEL).Value = ""
      .Cell(GRID_ROW_DOCSCAN, GRID_COLUMN_ID).Value = ""
      .Cell(GRID_ROW_DOCSCAN, GRID_COLUMN_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5092) '"Documentos escaneados"
      .Cell(GRID_ROW_DOCSCAN, GRID_COLUMN_VISIBLE).Value = BoolToGridValue(Data.AccountRequiredField.DocScan.Visible)
      .Cell(GRID_ROW_DOCSCAN, GRID_COLUMN_REQUIRED).Value = BoolToGridValue(Data.AccountRequiredField.DocScan.Required)
      .Cell(GRID_ROW_DOCSCAN, GRID_COLUMN_AML).Value = BoolToGridValue(Data.AccountRequiredField.DocScan.AML)


      .Cell(GRID_ROW_BIRTHDATE, GRID_COLUMN_SENTINEL).Value = ""
      .Cell(GRID_ROW_BIRTHDATE, GRID_COLUMN_ID).Value = ""
      .Cell(GRID_ROW_BIRTHDATE, GRID_COLUMN_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1725) '"Fecha nacimiento"
      .Cell(GRID_ROW_BIRTHDATE, GRID_COLUMN_VISIBLE).Value = uc_grid.GRID_CHK_CHECKED_DISABLED 'BoolToGridValue(Data.AccountRequiredField.Birthdate.Visible)
      .Cell(GRID_ROW_BIRTHDATE, GRID_COLUMN_REQUIRED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED 'BoolToGridValue(Data.AccountRequiredField.Birthdate.Required)
      .Cell(GRID_ROW_BIRTHDATE, GRID_COLUMN_AML).Value = uc_grid.GRID_CHK_CHECKED_DISABLED 'BoolToGridValue(Data.AccountRequiredField.Birthdate.AML)

      .Cell(GRID_ROW_OCCUPATION, GRID_COLUMN_SENTINEL).Value = ""
      .Cell(GRID_ROW_OCCUPATION, GRID_COLUMN_ID).Value = ""
      .Cell(GRID_ROW_OCCUPATION, GRID_COLUMN_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2058) '"Ocupacion"
      .Cell(GRID_ROW_OCCUPATION, GRID_COLUMN_VISIBLE).Value = BoolToGridValue(Data.AccountRequiredField.Occupation.Visible)
      .Cell(GRID_ROW_OCCUPATION, GRID_COLUMN_REQUIRED).Value = BoolToGridValue(Data.AccountRequiredField.Occupation.Required)
      .Cell(GRID_ROW_OCCUPATION, GRID_COLUMN_AML).Value = BoolToGridValue(Data.AccountRequiredField.Occupation.AML)

      .Cell(GRID_ROW_GENDER, GRID_COLUMN_SENTINEL).Value = ""
      .Cell(GRID_ROW_GENDER, GRID_COLUMN_ID).Value = ""
      .Cell(GRID_ROW_GENDER, GRID_COLUMN_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1723) '"Sexo"
      .Cell(GRID_ROW_GENDER, GRID_COLUMN_VISIBLE).Value = uc_grid.GRID_CHK_CHECKED_DISABLED 'BoolToGridValue(Data.AccountRequiredField.Gender.Visible)
      .Cell(GRID_ROW_GENDER, GRID_COLUMN_REQUIRED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED 'BoolToGridValue(Data.AccountRequiredField.Gender.Required)
      .Cell(GRID_ROW_GENDER, GRID_COLUMN_AML).Value = uc_grid.GRID_CHK_CHECKED_DISABLED 'BoolToGridValue(Data.AccountRequiredField.Gender.AML)

      .Cell(GRID_ROW_NACIONALITY, GRID_COLUMN_SENTINEL).Value = ""
      .Cell(GRID_ROW_NACIONALITY, GRID_COLUMN_ID).Value = ""
      .Cell(GRID_ROW_NACIONALITY, GRID_COLUMN_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2089) '"Nacionalidad"
      .Cell(GRID_ROW_NACIONALITY, GRID_COLUMN_VISIBLE).Value = BoolToGridValue(Data.AccountRequiredField.Nationality.Visible)
      .Cell(GRID_ROW_NACIONALITY, GRID_COLUMN_REQUIRED).Value = BoolToGridValue(Data.AccountRequiredField.Nationality.Required)
      .Cell(GRID_ROW_NACIONALITY, GRID_COLUMN_AML).Value = BoolToGridValue(Data.AccountRequiredField.Nationality.AML)

      .Cell(GRID_ROW_BIRTHCOUNTRY, GRID_COLUMN_SENTINEL).Value = ""
      .Cell(GRID_ROW_BIRTHCOUNTRY, GRID_COLUMN_ID).Value = ""
      .Cell(GRID_ROW_BIRTHCOUNTRY, GRID_COLUMN_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2088) '"Pa�s nacimiento"
      .Cell(GRID_ROW_BIRTHCOUNTRY, GRID_COLUMN_VISIBLE).Value = BoolToGridValue(Data.AccountRequiredField.BirthCountry.Visible)
      .Cell(GRID_ROW_BIRTHCOUNTRY, GRID_COLUMN_REQUIRED).Value = BoolToGridValue(Data.AccountRequiredField.BirthCountry.Required)
      .Cell(GRID_ROW_BIRTHCOUNTRY, GRID_COLUMN_AML).Value = BoolToGridValue(Data.AccountRequiredField.BirthCountry.AML)

      .Cell(GRID_ROW_MARITAL_STATUS, GRID_COLUMN_SENTINEL).Value = ""
      .Cell(GRID_ROW_MARITAL_STATUS, GRID_COLUMN_ID).Value = ""
      .Cell(GRID_ROW_MARITAL_STATUS, GRID_COLUMN_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1724) '"Estado civil"
      .Cell(GRID_ROW_MARITAL_STATUS, GRID_COLUMN_VISIBLE).Value = BoolToGridValue(Data.AccountRequiredField.MaritalStatus.Visible)
      .Cell(GRID_ROW_MARITAL_STATUS, GRID_COLUMN_REQUIRED).Value = BoolToGridValue(Data.AccountRequiredField.MaritalStatus.Required)
      .Cell(GRID_ROW_MARITAL_STATUS, GRID_COLUMN_AML).Value = BoolToGridValue(Data.AccountRequiredField.MaritalStatus.AML)

      .Cell(GRID_ROW_WEDDING_DATE, GRID_COLUMN_SENTINEL).Value = ""
      .Cell(GRID_ROW_WEDDING_DATE, GRID_COLUMN_ID).Value = ""
      .Cell(GRID_ROW_WEDDING_DATE, GRID_COLUMN_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1726) '"Fecha boda"
      .Cell(GRID_ROW_WEDDING_DATE, GRID_COLUMN_VISIBLE).Value = BoolToGridValue(Data.AccountRequiredField.WeddingDate.Visible)
      .Cell(GRID_ROW_WEDDING_DATE, GRID_COLUMN_REQUIRED).Value = BoolToGridValue(Data.AccountRequiredField.WeddingDate.Required)
      .Cell(GRID_ROW_WEDDING_DATE, GRID_COLUMN_AML).Value = BoolToGridValue(Data.AccountRequiredField.WeddingDate.AML)

      .Cell(GRID_ROW_PHOTO, GRID_COLUMN_SENTINEL).Value = ""
      .Cell(GRID_ROW_PHOTO, GRID_COLUMN_ID).Value = ""
      .Cell(GRID_ROW_PHOTO, GRID_COLUMN_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7103) '"Photo"
      .Cell(GRID_ROW_PHOTO, GRID_COLUMN_VISIBLE).Value = BoolToGridValue(Data.AccountRequiredField.Photo.Visible)
      .Cell(GRID_ROW_PHOTO, GRID_COLUMN_REQUIRED).Value = BoolToGridValue(Data.AccountRequiredField.Photo.Required)
      .Cell(GRID_ROW_PHOTO, GRID_COLUMN_AML).Value = uc_grid.GRID_CHK_UNCHECKED_DISABLED 'BoolToGridValue(Data.AccountRequiredField.Photo.AML)

      .Cell(GRID_ROW_EXPIRATION_DOCUMENT_DATE, GRID_COLUMN_SENTINEL).Value = ""
      .Cell(GRID_ROW_EXPIRATION_DOCUMENT_DATE, GRID_COLUMN_ID).Value = ""
      .Cell(GRID_ROW_EXPIRATION_DOCUMENT_DATE, GRID_COLUMN_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7950) '"Expiration document date"
      .Cell(GRID_ROW_EXPIRATION_DOCUMENT_DATE, GRID_COLUMN_VISIBLE).Value = BoolToGridValue(Data.AccountRequiredField.ExpirationDocumentDate.Visible)
      .Cell(GRID_ROW_EXPIRATION_DOCUMENT_DATE, GRID_COLUMN_REQUIRED).Value = BoolToGridValue(Data.AccountRequiredField.ExpirationDocumentDate.Required)
      .Cell(GRID_ROW_EXPIRATION_DOCUMENT_DATE, GRID_COLUMN_AML).Value = uc_grid.GRID_CHK_UNCHECKED_DISABLED 'BoolToGridValue(Data.AccountRequiredField.ExpirationDocumentDate.AML)

    End With


    ' CONTACT
    For _idx_row As Integer = 0 To GRID_CONTACT_TOTAL_ROWS
      Me.dg_contact_config.AddRow()
    Next

    With Me.dg_contact_config
      .Cell(GRID_ROW_PHONE1, GRID_COLUMN_SENTINEL).Value = ""
      .Cell(GRID_ROW_PHONE1, GRID_COLUMN_ID).Value = ""
      .Cell(GRID_ROW_PHONE1, GRID_COLUMN_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1727, "1") '"Tel�fono 1"
      .Cell(GRID_ROW_PHONE1, GRID_COLUMN_VISIBLE).Value = BoolToGridValue(Data.AccountRequiredField.Phone1.Visible)
      .Cell(GRID_ROW_PHONE1, GRID_COLUMN_REQUIRED).Value = BoolToGridValue(Data.AccountRequiredField.Phone1.Required)
      .Cell(GRID_ROW_PHONE1, GRID_COLUMN_AML).Value = BoolToGridValue(Data.AccountRequiredField.Phone1.AML)

      .Cell(GRID_ROW_PHONE2, GRID_COLUMN_SENTINEL).Value = ""
      .Cell(GRID_ROW_PHONE2, GRID_COLUMN_ID).Value = ""
      .Cell(GRID_ROW_PHONE2, GRID_COLUMN_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1727, "2")
      .Cell(GRID_ROW_PHONE2, GRID_COLUMN_VISIBLE).Value = BoolToGridValue(Data.AccountRequiredField.Phone2.Visible)
      .Cell(GRID_ROW_PHONE2, GRID_COLUMN_REQUIRED).Value = BoolToGridValue(Data.AccountRequiredField.Phone2.Required)
      .Cell(GRID_ROW_PHONE2, GRID_COLUMN_AML).Value = BoolToGridValue(Data.AccountRequiredField.Phone2.AML)

      .Cell(GRID_ROW_EMAIL1, GRID_COLUMN_SENTINEL).Value = ""
      .Cell(GRID_ROW_EMAIL1, GRID_COLUMN_ID).Value = ""
      .Cell(GRID_ROW_EMAIL1, GRID_COLUMN_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1729, "1") '"Email 1"
      .Cell(GRID_ROW_EMAIL1, GRID_COLUMN_VISIBLE).Value = BoolToGridValue(Data.AccountRequiredField.Email1.Visible)
      .Cell(GRID_ROW_EMAIL1, GRID_COLUMN_REQUIRED).Value = BoolToGridValue(Data.AccountRequiredField.Email1.Required)
      .Cell(GRID_ROW_EMAIL1, GRID_COLUMN_AML).Value = BoolToGridValue(Data.AccountRequiredField.Email1.AML)

      .Cell(GRID_ROW_EMAIL2, GRID_COLUMN_SENTINEL).Value = ""
      .Cell(GRID_ROW_EMAIL2, GRID_COLUMN_ID).Value = ""
      .Cell(GRID_ROW_EMAIL2, GRID_COLUMN_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1729, "2") '"Email 2"
      .Cell(GRID_ROW_EMAIL2, GRID_COLUMN_VISIBLE).Value = BoolToGridValue(Data.AccountRequiredField.Email2.Visible)
      .Cell(GRID_ROW_EMAIL2, GRID_COLUMN_REQUIRED).Value = BoolToGridValue(Data.AccountRequiredField.Email2.Required)
      .Cell(GRID_ROW_EMAIL2, GRID_COLUMN_AML).Value = BoolToGridValue(Data.AccountRequiredField.Email2.AML)

      .Cell(GRID_ROW_TWITTER, GRID_COLUMN_SENTINEL).Value = ""
      .Cell(GRID_ROW_TWITTER, GRID_COLUMN_ID).Value = ""
      .Cell(GRID_ROW_TWITTER, GRID_COLUMN_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1730) '"Cuenta Twitter"
      .Cell(GRID_ROW_TWITTER, GRID_COLUMN_VISIBLE).Value = BoolToGridValue(Data.AccountRequiredField.TwitterAccount.Visible)
      .Cell(GRID_ROW_TWITTER, GRID_COLUMN_REQUIRED).Value = BoolToGridValue(Data.AccountRequiredField.TwitterAccount.Required)
      .Cell(GRID_ROW_TWITTER, GRID_COLUMN_AML).Value = BoolToGridValue(Data.AccountRequiredField.TwitterAccount.AML)
    End With

    ' ADDRESS
    For _idx_row As Integer = 0 To GRID_ADDRESS_TOTAL_ROWS
      Me.dg_address_config.AddRow()
    Next

    With Me.dg_address_config
      .Cell(GRID_ROW_STREET, GRID_COLUMN_SENTINEL).Value = ""
      .Cell(GRID_ROW_STREET, GRID_COLUMN_ID).Value = ""
      .Cell(GRID_ROW_STREET, GRID_COLUMN_NAME).Value = AccountFields.GetAddressName() 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(1731) '"Calle"
      .Cell(GRID_ROW_STREET, GRID_COLUMN_VISIBLE).Value = BoolToGridValue(Data.AccountRequiredField.Address01.Visible)
      .Cell(GRID_ROW_STREET, GRID_COLUMN_REQUIRED).Value = BoolToGridValue(Data.AccountRequiredField.Address01.Required)
      .Cell(GRID_ROW_STREET, GRID_COLUMN_AML).Value = BoolToGridValue(Data.AccountRequiredField.Address01.AML)

      .Cell(GRID_ROW_EXT_NUMBER, GRID_COLUMN_SENTINEL).Value = ""
      .Cell(GRID_ROW_EXT_NUMBER, GRID_COLUMN_ID).Value = ""
      .Cell(GRID_ROW_EXT_NUMBER, GRID_COLUMN_NAME).Value = AccountFields.GetNumExtName() 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(2091) '"N�mero Exterior"
      .Cell(GRID_ROW_EXT_NUMBER, GRID_COLUMN_VISIBLE).Value = BoolToGridValue(Data.AccountRequiredField.ExtNum.Visible)
      .Cell(GRID_ROW_EXT_NUMBER, GRID_COLUMN_REQUIRED).Value = BoolToGridValue(Data.AccountRequiredField.ExtNum.Required)
      .Cell(GRID_ROW_EXT_NUMBER, GRID_COLUMN_AML).Value = BoolToGridValue(Data.AccountRequiredField.ExtNum.AML)

      .Cell(GRID_ROW_POSTAL_CODE, GRID_COLUMN_SENTINEL).Value = ""
      .Cell(GRID_ROW_POSTAL_CODE, GRID_COLUMN_ID).Value = ""
      .Cell(GRID_ROW_POSTAL_CODE, GRID_COLUMN_NAME).Value = AccountFields.GetZipName() 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(1735) '"Codigo Postal"
      .Cell(GRID_ROW_POSTAL_CODE, GRID_COLUMN_VISIBLE).Value = BoolToGridValue(Data.AccountRequiredField.Zip.Visible)
      .Cell(GRID_ROW_POSTAL_CODE, GRID_COLUMN_REQUIRED).Value = BoolToGridValue(Data.AccountRequiredField.Zip.Required)
      .Cell(GRID_ROW_POSTAL_CODE, GRID_COLUMN_AML).Value = BoolToGridValue(Data.AccountRequiredField.Zip.AML)

      .Cell(GRID_ROW_COLONY, GRID_COLUMN_SENTINEL).Value = ""
      .Cell(GRID_ROW_COLONY, GRID_COLUMN_ID).Value = ""
      .Cell(GRID_ROW_COLONY, GRID_COLUMN_NAME).Value = AccountFields.GetColonyName() 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(1732) '"Colonia"
      .Cell(GRID_ROW_COLONY, GRID_COLUMN_VISIBLE).Value = BoolToGridValue(Data.AccountRequiredField.Address02.Visible)
      .Cell(GRID_ROW_COLONY, GRID_COLUMN_REQUIRED).Value = BoolToGridValue(Data.AccountRequiredField.Address02.Required)
      .Cell(GRID_ROW_COLONY, GRID_COLUMN_AML).Value = BoolToGridValue(Data.AccountRequiredField.Address02.AML)

      .Cell(GRID_ROW_DELEGATION, GRID_COLUMN_SENTINEL).Value = ""
      .Cell(GRID_ROW_DELEGATION, GRID_COLUMN_ID).Value = ""
      .Cell(GRID_ROW_DELEGATION, GRID_COLUMN_NAME).Value = AccountFields.GetDelegationName() 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(1733) '"Delegacion"
      .Cell(GRID_ROW_DELEGATION, GRID_COLUMN_VISIBLE).Value = BoolToGridValue(Data.AccountRequiredField.Address03.Visible)
      .Cell(GRID_ROW_DELEGATION, GRID_COLUMN_REQUIRED).Value = BoolToGridValue(Data.AccountRequiredField.Address03.Required)
      .Cell(GRID_ROW_DELEGATION, GRID_COLUMN_AML).Value = BoolToGridValue(Data.AccountRequiredField.Address03.AML)

      .Cell(GRID_ROW_MUNICIP, GRID_COLUMN_SENTINEL).Value = ""
      .Cell(GRID_ROW_MUNICIP, GRID_COLUMN_ID).Value = ""
      .Cell(GRID_ROW_MUNICIP, GRID_COLUMN_NAME).Value = AccountFields.GetTownshipName() 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(1734) '"Municipio"
      .Cell(GRID_ROW_MUNICIP, GRID_COLUMN_VISIBLE).Value = BoolToGridValue(Data.AccountRequiredField.City.Visible)
      .Cell(GRID_ROW_MUNICIP, GRID_COLUMN_REQUIRED).Value = BoolToGridValue(Data.AccountRequiredField.City.Required)
      .Cell(GRID_ROW_MUNICIP, GRID_COLUMN_AML).Value = BoolToGridValue(Data.AccountRequiredField.City.AML)

      .Cell(GRID_ROW_ADDRESS_COUNTRY, GRID_COLUMN_SENTINEL).Value = ""
      .Cell(GRID_ROW_ADDRESS_COUNTRY, GRID_COLUMN_ID).Value = ""
      .Cell(GRID_ROW_ADDRESS_COUNTRY, GRID_COLUMN_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5090) '"Pa�s"
      .Cell(GRID_ROW_ADDRESS_COUNTRY, GRID_COLUMN_VISIBLE).Value = BoolToGridValue(Data.AccountRequiredField.AddressCountry.Visible)
      .Cell(GRID_ROW_ADDRESS_COUNTRY, GRID_COLUMN_REQUIRED).Value = BoolToGridValue(Data.AccountRequiredField.AddressCountry.Required)
      .Cell(GRID_ROW_ADDRESS_COUNTRY, GRID_COLUMN_AML).Value = BoolToGridValue(Data.AccountRequiredField.AddressCountry.AML)

      .Cell(GRID_ROW_FED_ENTITY, GRID_COLUMN_SENTINEL).Value = ""
      .Cell(GRID_ROW_FED_ENTITY, GRID_COLUMN_ID).Value = ""
      .Cell(GRID_ROW_FED_ENTITY, GRID_COLUMN_NAME).Value = AccountFields.GetStateName()
      .Cell(GRID_ROW_FED_ENTITY, GRID_COLUMN_VISIBLE).Value = BoolToGridValue(Data.AccountRequiredField.FedEntity.Visible)
      .Cell(GRID_ROW_FED_ENTITY, GRID_COLUMN_REQUIRED).Value = BoolToGridValue(Data.AccountRequiredField.FedEntity.Required)
      .Cell(GRID_ROW_FED_ENTITY, GRID_COLUMN_AML).Value = BoolToGridValue(Data.AccountRequiredField.FedEntity.AML)
    End With

    If m_has_beneficiary Then
      ' BENEFICIARY
      For _idx_row As Integer = 0 To GRID_BENEFICIARY_TOTAL_ROWS
        Me.dg_beneficiary_config.AddRow()
      Next

      With Me.dg_beneficiary_config
        .Cell(GRID_ROW_BENEFICIARY_NAME3, GRID_COLUMN_SENTINEL).Value = ""
        .Cell(GRID_ROW_BENEFICIARY_NAME3, GRID_COLUMN_ID).Value = ""
        .Cell(GRID_ROW_BENEFICIARY_NAME3, GRID_COLUMN_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5032) '"Nombre"
        .Cell(GRID_ROW_BENEFICIARY_NAME3, GRID_COLUMN_VISIBLE).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
        .Cell(GRID_ROW_BENEFICIARY_NAME3, GRID_COLUMN_REQUIRED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED 'BoolToGridValue(Data.BeneficiaryRequiredField.Name3.Required)
        .Cell(GRID_ROW_BENEFICIARY_NAME3, GRID_COLUMN_AML).Value = uc_grid.GRID_CHK_CHECKED_DISABLED 'BoolToGridValue(Data.BeneficiaryRequiredField.Name3.AML)

        .Cell(GRID_ROW_BENEFICIARY_NAME1, GRID_COLUMN_SENTINEL).Value = ""
        .Cell(GRID_ROW_BENEFICIARY_NAME1, GRID_COLUMN_ID).Value = ""
        .Cell(GRID_ROW_BENEFICIARY_NAME1, GRID_COLUMN_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1719, "1") '"Apellido 1"
        .Cell(GRID_ROW_BENEFICIARY_NAME1, GRID_COLUMN_VISIBLE).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
        .Cell(GRID_ROW_BENEFICIARY_NAME1, GRID_COLUMN_REQUIRED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED 'BoolToGridValue(Data.BeneficiaryRequiredField.Name1.Required)
        .Cell(GRID_ROW_BENEFICIARY_NAME1, GRID_COLUMN_AML).Value = uc_grid.GRID_CHK_CHECKED_DISABLED 'BoolToGridValue(Data.BeneficiaryRequiredField.Name1.AML)

        .Cell(GRID_ROW_BENEFICIARY_NAME2, GRID_COLUMN_SENTINEL).Value = ""
        .Cell(GRID_ROW_BENEFICIARY_NAME2, GRID_COLUMN_ID).Value = ""
        .Cell(GRID_ROW_BENEFICIARY_NAME2, GRID_COLUMN_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1720)
        .Cell(GRID_ROW_BENEFICIARY_NAME2, GRID_COLUMN_VISIBLE).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
        .Cell(GRID_ROW_BENEFICIARY_NAME2, GRID_COLUMN_REQUIRED).Value = BoolToGridValue(Data.BeneficiaryRequiredField.Name2.Required)
        .Cell(GRID_ROW_BENEFICIARY_NAME2, GRID_COLUMN_AML).Value = BoolToGridValue(Data.BeneficiaryRequiredField.Name2.AML)

        .Cell(GRID_ROW_BENEFICIARY_DOCUMENT, GRID_COLUMN_SENTINEL).Value = ""
        .Cell(GRID_ROW_BENEFICIARY_DOCUMENT, GRID_COLUMN_ID).Value = ""
        .Cell(GRID_ROW_BENEFICIARY_DOCUMENT, GRID_COLUMN_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1566) '"Documento"
        .Cell(GRID_ROW_BENEFICIARY_DOCUMENT, GRID_COLUMN_VISIBLE).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
        .Cell(GRID_ROW_BENEFICIARY_DOCUMENT, GRID_COLUMN_REQUIRED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED 'BoolToGridValue(Data.BeneficiaryRequiredField.Document.Required)
        .Cell(GRID_ROW_BENEFICIARY_DOCUMENT, GRID_COLUMN_AML).Value = uc_grid.GRID_CHK_CHECKED_DISABLED 'BoolToGridValue(Data.BeneficiaryRequiredField.Document.AML)

        .Cell(GRID_ROW_BENEFICIARY_DOCSCAN, GRID_COLUMN_SENTINEL).Value = ""
        .Cell(GRID_ROW_BENEFICIARY_DOCSCAN, GRID_COLUMN_ID).Value = ""
        .Cell(GRID_ROW_BENEFICIARY_DOCSCAN, GRID_COLUMN_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5092) '"Documentos escaneados"
        .Cell(GRID_ROW_BENEFICIARY_DOCSCAN, GRID_COLUMN_VISIBLE).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
        .Cell(GRID_ROW_BENEFICIARY_DOCSCAN, GRID_COLUMN_REQUIRED).Value = BoolToGridValue(Data.BeneficiaryRequiredField.DocScan.Required)
        .Cell(GRID_ROW_BENEFICIARY_DOCSCAN, GRID_COLUMN_AML).Value = BoolToGridValue(Data.BeneficiaryRequiredField.DocScan.AML)

        .Cell(GRID_ROW_BENEFICIARY_BIRTHDATE, GRID_COLUMN_SENTINEL).Value = ""
        .Cell(GRID_ROW_BENEFICIARY_BIRTHDATE, GRID_COLUMN_ID).Value = ""
        .Cell(GRID_ROW_BENEFICIARY_BIRTHDATE, GRID_COLUMN_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1725) '"Fecha nacimiento"
        .Cell(GRID_ROW_BENEFICIARY_BIRTHDATE, GRID_COLUMN_VISIBLE).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
        .Cell(GRID_ROW_BENEFICIARY_BIRTHDATE, GRID_COLUMN_REQUIRED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED 'BoolToGridValue(Data.BeneficiaryRequiredField.Birthdate.Required)
        .Cell(GRID_ROW_BENEFICIARY_BIRTHDATE, GRID_COLUMN_AML).Value = uc_grid.GRID_CHK_CHECKED_DISABLED 'BoolToGridValue(Data.BeneficiaryRequiredField.Birthdate.AML)

        .Cell(GRID_ROW_BENEFICIARY_OCCUPATION, GRID_COLUMN_SENTINEL).Value = ""
        .Cell(GRID_ROW_BENEFICIARY_OCCUPATION, GRID_COLUMN_ID).Value = ""
        .Cell(GRID_ROW_BENEFICIARY_OCCUPATION, GRID_COLUMN_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2058) '"Ocupacion"
        .Cell(GRID_ROW_BENEFICIARY_OCCUPATION, GRID_COLUMN_VISIBLE).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
        .Cell(GRID_ROW_BENEFICIARY_OCCUPATION, GRID_COLUMN_REQUIRED).Value = BoolToGridValue(Data.BeneficiaryRequiredField.Occupation.Required)
        .Cell(GRID_ROW_BENEFICIARY_OCCUPATION, GRID_COLUMN_AML).Value = BoolToGridValue(Data.BeneficiaryRequiredField.Occupation.AML)

        .Cell(GRID_ROW_BENEFICIARY_GENDER, GRID_COLUMN_SENTINEL).Value = ""
        .Cell(GRID_ROW_BENEFICIARY_GENDER, GRID_COLUMN_ID).Value = ""
        .Cell(GRID_ROW_BENEFICIARY_GENDER, GRID_COLUMN_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1723) '"Sexo"
        .Cell(GRID_ROW_BENEFICIARY_GENDER, GRID_COLUMN_VISIBLE).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
        .Cell(GRID_ROW_BENEFICIARY_GENDER, GRID_COLUMN_REQUIRED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED 'BoolToGridValue(Data.BeneficiaryRequiredField.Gender.Required)
        .Cell(GRID_ROW_BENEFICIARY_GENDER, GRID_COLUMN_AML).Value = uc_grid.GRID_CHK_CHECKED_DISABLED 'BoolToGridValue(Data.BeneficiaryRequiredField.Gender.AML)
      End With
    End If
  End Sub

  ' PURPOSE:  Grid configuration
  '
  '  PARAMS:
  '     - INPUT:
  '           - Grid: Grid
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub StyleSheetConfig(ByRef Grid As uc_grid)
    Dim arialBold As New Font("Verdana", 8.0F)
    Dim textSize As Size = TextRenderer.MeasureText(m_AML_name, arialBold)

    'GRID1
    With Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, True)

      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
      .Sortable = False

      ' Sentinel
      .Column(GRID_COLUMN_SENTINEL).Header.Text = ""
      .Column(GRID_COLUMN_SENTINEL).Width = GRID_COLUMN_WIDTH_SENTINEL
      .Column(GRID_COLUMN_SENTINEL).HighLightWhenSelected = False
      .Column(GRID_COLUMN_SENTINEL).IsColumnPrintable = False
      .Column(GRID_COLUMN_SENTINEL).Editable = False

      ' GRID ID
      .Column(GRID_COLUMN_ID).Header.Text = ""
      .Column(GRID_COLUMN_ID).Width = GRID_COLUMN_WIDTH_ID

      ' GRID NAME
      .Column(GRID_COLUMN_NAME).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_NAME).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5032) '"Nombre"
      .Column(GRID_COLUMN_NAME).Width = GRID_COLUMN_WIDTH_NAME + 1500
      .Column(GRID_COLUMN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_NAME).Editable = False

      ' GRID VISIBLE
      .Column(GRID_COLUMN_VISIBLE).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_VISIBLE).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(754) '"Visible"
      .Column(GRID_COLUMN_VISIBLE).Width = GRID_COLUMN_WIDTH_VISIBLE
      .Column(GRID_COLUMN_VISIBLE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_VISIBLE).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
      .Column(GRID_COLUMN_VISIBLE).Editable = True

      ' GRID REQUIRED
      .Column(GRID_COLUMN_REQUIRED).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_REQUIRED).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(739) '"Requerido"
      .Column(GRID_COLUMN_REQUIRED).Width = GRID_COLUMN_WIDTH_REQUIRED
      .Column(GRID_COLUMN_REQUIRED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_REQUIRED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
      .Column(GRID_COLUMN_REQUIRED).Editable = True

      '----
      ' GRID AML
      .Column(GRID_COLUMN_AML).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_AML).Header.Text = m_AML_name
      .Column(GRID_COLUMN_AML).Width = IIf(m_AML_enabled, textSize.Width * 15, 0)
      .Column(GRID_COLUMN_AML).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_AML).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
      .Column(GRID_COLUMN_AML).Editable = True

    End With

  End Sub

  ' PURPOSE:  Grid configuration
  '
  '  PARAMS:
  '     - INPUT:
  '           - Grid: Grid
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub StyleSheetDocuments(ByRef Grid As uc_grid)
    Dim arialBold As New Font("Verdana", 8.0F)
    Dim textSize As Size = TextRenderer.MeasureText(m_AML_name, arialBold)

    With Me.dg_principal_documents_config
      If m_has_beneficiary Then
        Call .Init(GRID_COLUMNS_DOC, 2, True)
      Else
        Call .Init(8, 2, True)
      End If

      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
      .Sortable = False

      ' Sentinel
      .Column(GRID_COLUMN_SENTINEL).Header(0).Text = ""
      .Column(GRID_COLUMN_SENTINEL).Header(1).Text = ""
      .Column(GRID_COLUMN_SENTINEL).Width = GRID_COLUMN_WIDTH_SENTINEL
      .Column(GRID_COLUMN_SENTINEL).HighLightWhenSelected = False
      .Column(GRID_COLUMN_SENTINEL).IsColumnPrintable = False
      .Column(GRID_COLUMN_SENTINEL).Editable = False

      ' GRID ID
      .Column(GRID_COLUMN_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_ID).Width = GRID_COLUMN_WIDTH_ID

      ' GRID NAME
      .Column(GRID_COLUMN_NAME).Header(0).Text = ""
      .Column(GRID_COLUMN_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5032) '"Nombre"
      .Column(GRID_COLUMN_NAME).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_NAME).Width = 2500
      .Column(GRID_COLUMN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_NAME).Editable = False

      ' GRID VISIBLE
      .Column(GRID_COLUMN_VISIBLE).Header(0).Text = ""
      .Column(GRID_COLUMN_VISIBLE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(754) '"Visible"
      .Column(GRID_COLUMN_VISIBLE).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_VISIBLE).Width = GRID_COLUMN_WIDTH_VISIBLE
      .Column(GRID_COLUMN_VISIBLE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_VISIBLE).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
      .Column(GRID_COLUMN_VISIBLE).Editable = True

      ' GRID REQUIRED
      .Column(GRID_COLUMN_REQUIRED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1567) '"Principal"
      .Column(GRID_COLUMN_REQUIRED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(739) '"Requerido"
      .Column(GRID_COLUMN_REQUIRED).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_REQUIRED).Width = GRID_COLUMN_WIDTH_REQUIRED
      .Column(GRID_COLUMN_REQUIRED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_REQUIRED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
      .Column(GRID_COLUMN_REQUIRED).Editable = True

      '----
      ' GRID AML
      .Column(GRID_PRINCIPAL_COLUMN_REQUIRED_AML).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1567) & " - " & m_AML_name '"Principal"
      .Column(GRID_PRINCIPAL_COLUMN_REQUIRED_AML).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(739) '"Requerido"
      .Column(GRID_PRINCIPAL_COLUMN_REQUIRED_AML).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_PRINCIPAL_COLUMN_REQUIRED_AML).Width = IIf(Me.m_AML_enabled, textSize.Width * 15, 0)
      .Column(GRID_PRINCIPAL_COLUMN_REQUIRED_AML).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_PRINCIPAL_COLUMN_REQUIRED_AML).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
      .Column(GRID_PRINCIPAL_COLUMN_REQUIRED_AML).Editable = True

      ' GRID REQUIRED SCAN
      .Column(GRID_PRINCIPAL_COLUMN_REQUIRED_SCAN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1567) '"Principal"
      .Column(GRID_PRINCIPAL_COLUMN_REQUIRED_SCAN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5091)
      .Column(GRID_PRINCIPAL_COLUMN_REQUIRED_SCAN).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_PRINCIPAL_COLUMN_REQUIRED_SCAN).Width = GRID_COLUMN_WIDTH_REQUIRED
      .Column(GRID_PRINCIPAL_COLUMN_REQUIRED_SCAN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_PRINCIPAL_COLUMN_REQUIRED_SCAN).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
      .Column(GRID_PRINCIPAL_COLUMN_REQUIRED_SCAN).Editable = True

      ' GRID REQUIRED AML SCAN
      .Column(GRID_PRINCIPAL_COLUMN_REQUIRED_AML_SCAN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1567) & " - " & m_AML_name '"Principal"
      .Column(GRID_PRINCIPAL_COLUMN_REQUIRED_AML_SCAN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5091)
      .Column(GRID_PRINCIPAL_COLUMN_REQUIRED_AML_SCAN).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_PRINCIPAL_COLUMN_REQUIRED_AML_SCAN).Width = IIf(Me.m_AML_enabled, textSize.Width * 15, 0) 'GRID_COLUMN_WIDTH_REQUIRED
      .Column(GRID_PRINCIPAL_COLUMN_REQUIRED_AML_SCAN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_PRINCIPAL_COLUMN_REQUIRED_AML_SCAN).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
      .Column(GRID_PRINCIPAL_COLUMN_REQUIRED_AML_SCAN).Editable = True

      If m_has_beneficiary Then
        ' GRID BENEF REQUIRED
        .Column(GRID_BENEF_COLUMN_REQUIRED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2060) '"Beneficiario"
        .Column(GRID_BENEF_COLUMN_REQUIRED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(739) '"Requerido"
        .Column(GRID_BENEF_COLUMN_REQUIRED).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
        .Column(GRID_BENEF_COLUMN_REQUIRED).Width = GRID_COLUMN_WIDTH_REQUIRED
        .Column(GRID_BENEF_COLUMN_REQUIRED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
        .Column(GRID_BENEF_COLUMN_REQUIRED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
        .Column(GRID_BENEF_COLUMN_REQUIRED).Editable = True

        '----
        ' GRID BENEF AML
        .Column(GRID_BENEF_COLUMN_REQUIRED_SCAN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2060) '"Beneficiario"
        .Column(GRID_BENEF_COLUMN_REQUIRED_SCAN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5091)
        .Column(GRID_BENEF_COLUMN_REQUIRED_SCAN).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
        .Column(GRID_BENEF_COLUMN_REQUIRED_SCAN).Width = GRID_COLUMN_WIDTH_REQUIRED
        .Column(GRID_BENEF_COLUMN_REQUIRED_SCAN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
        .Column(GRID_BENEF_COLUMN_REQUIRED_SCAN).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
        .Column(GRID_BENEF_COLUMN_REQUIRED_SCAN).Editable = True

        ' GRID BENEF REQUIRED SCAN
        .Column(GRID_BENEF_COLUMN_REQUIRED_AML).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2060) & " - " & m_AML_name  '"Beneficiario"
        .Column(GRID_BENEF_COLUMN_REQUIRED_AML).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(739)
        .Column(GRID_BENEF_COLUMN_REQUIRED_AML).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
        .Column(GRID_BENEF_COLUMN_REQUIRED_AML).Width = IIf(Me.m_AML_enabled, (textSize.Width * 15) + 120, 0) 'GRID_COLUMN_WIDTH_REQUIRED + 250
        .Column(GRID_BENEF_COLUMN_REQUIRED_AML).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
        .Column(GRID_BENEF_COLUMN_REQUIRED_AML).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
        .Column(GRID_BENEF_COLUMN_REQUIRED_AML).Editable = True

        ' GRID BENEF REQUIRED AML SCAN
        .Column(GRID_BENEF_COLUMN_REQUIRED_AML_SCAN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2060) & " - " & m_AML_name '"Beneficiario"
        .Column(GRID_BENEF_COLUMN_REQUIRED_AML_SCAN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5091)
        .Column(GRID_BENEF_COLUMN_REQUIRED_AML_SCAN).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
        .Column(GRID_BENEF_COLUMN_REQUIRED_AML_SCAN).Width = IIf(Me.m_AML_enabled, (textSize.Width * 15) + 120, 0) 'GRID_COLUMN_WIDTH_REQUIRED + 250
        .Column(GRID_BENEF_COLUMN_REQUIRED_AML_SCAN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
        .Column(GRID_BENEF_COLUMN_REQUIRED_AML_SCAN).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
        .Column(GRID_BENEF_COLUMN_REQUIRED_AML_SCAN).Editable = True
      End If
    End With

  End Sub

  ' PURPOSE:  Fill Documents grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - Grid: Grid
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub PopulateIdentificationTypes(ByVal Data As CLS_REQUIRED_FIELDS)

    Dim _idx_row As Integer = 0
    Dim _string_code As Integer = 0

    For _idx_ As Integer = 0 To Data.IdentificationTypes.Rows.Count - 1
      Me.dg_principal_documents_config.AddRow()
    Next

    For Each _dr As DataRow In Data.IdentificationTypes.Rows
      With Me.dg_principal_documents_config

        .Cell(_idx_row, GRID_COLUMN_ID).Value = Convert.ToInt16(_dr(SQL_DT_COLUMN_ID)).ToString("000")
        .Cell(_idx_row, GRID_COLUMN_NAME).Value = _dr(SQL_DT_COLUMN_NAME)
        .Cell(_idx_row, GRID_COLUMN_VISIBLE).Value = BoolToGridValue(_dr(SQL_DT_COLUMN_ENABLED))

        _string_code = _dr(SQL_DT_COLUMN_ID).ToString()

        If Data.AccountRequiredField.DocumentTypeList.Contains(_string_code.ToString("000")) Then
          .Cell(_idx_row, GRID_COLUMN_REQUIRED).Value = BoolToGridValue(True)
        End If

        If Data.AccountRequiredField.DocScanTypeList.Contains(_string_code.ToString("000")) Then
          .Cell(_idx_row, GRID_PRINCIPAL_COLUMN_REQUIRED_SCAN).Value = BoolToGridValue(True)
        End If

        ' Now only check RFC. Rest of documents are disabled & unchecked
        'If Data.AccountRequiredField.AMLDocumentTypeList.Contains(_string_code.ToString("000")) Then
        '  .Cell(_idx_row, GRID_PRINCIPAL_COLUMN_REQUIRED_AML).Value = BoolToGridValue(True)
        'End If

        If _string_code = ACCOUNT_HOLDER_ID_TYPE.RFC Then
          .Cell(_idx_row, GRID_PRINCIPAL_COLUMN_REQUIRED_AML).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
          If m_disable_rfc_and_curp Then
            .Cell(_idx_row, GRID_COLUMN_VISIBLE).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
          End If
        Else
          .Cell(_idx_row, GRID_PRINCIPAL_COLUMN_REQUIRED_AML).Value = uc_grid.GRID_CHK_UNCHECKED_DISABLED
        End If

        If Data.AccountRequiredField.AMLDocScanTypeList.Contains(_string_code.ToString("000")) Then
          .Cell(_idx_row, GRID_PRINCIPAL_COLUMN_REQUIRED_AML_SCAN).Value = BoolToGridValue(True)
        End If

        If m_has_beneficiary Then
          If m_disable_rfc_and_curp And (_string_code <> ACCOUNT_HOLDER_ID_TYPE.RFC And _string_code <> ACCOUNT_HOLDER_ID_TYPE.CURP) Then
            .Cell(_idx_row, GRID_BENEF_COLUMN_REQUIRED).Value = uc_grid.GRID_CHK_UNCHECKED_DISABLED
          Else
            If Data.BeneficiaryRequiredField.DocumentTypeList.Contains(_string_code.ToString("000")) Then
              .Cell(_idx_row, GRID_BENEF_COLUMN_REQUIRED).Value = BoolToGridValue(True)
            End If
          End If

          If Data.BeneficiaryRequiredField.DocScanTypeList.Contains(_string_code.ToString("000")) Then
            .Cell(_idx_row, GRID_BENEF_COLUMN_REQUIRED_SCAN).Value = BoolToGridValue(True)
          End If

          'If m_disable_rfc_and_curp And (_string_code <> ACCOUNT_HOLDER_ID_TYPE.RFC And _string_code <> ACCOUNT_HOLDER_ID_TYPE.CURP) Then
          '  .Cell(_idx_row, GRID_BENEF_COLUMN_REQUIRED_AML).Value = uc_grid.GRID_CHK_UNCHECKED_DISABLED
          'Else
          '  If Data.BeneficiaryRequiredField.AMLDocumentTypeList.Contains(_string_code.ToString("000")) Then
          '    .Cell(_idx_row, GRID_BENEF_COLUMN_REQUIRED_AML).Value = BoolToGridValue(True)
          '  End If
          'End If

          If _string_code = ACCOUNT_HOLDER_ID_TYPE.RFC Then
            .Cell(_idx_row, GRID_BENEF_COLUMN_REQUIRED_AML).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
          Else
            .Cell(_idx_row, GRID_BENEF_COLUMN_REQUIRED_AML).Value = uc_grid.GRID_CHK_UNCHECKED_DISABLED
          End If

          If Data.BeneficiaryRequiredField.AMLDocScanTypeList.Contains(_string_code.ToString("000")) Then
            .Cell(_idx_row, GRID_BENEF_COLUMN_REQUIRED_AML_SCAN).Value = BoolToGridValue(True)
          End If
        End If
      End With

      _idx_row += 1
    Next

    Call Data.GetDictionaryOfDocuments(m_documents_visible)

    'Call RefreshCombo()
  End Sub

  ' PURPOSE:  Cast from boolean to uc_grid checked
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function BoolToGridValue(ByVal enabled As Boolean) As String
    If (enabled) Then
      Return uc_grid.GRID_CHK_CHECKED
    Else
      Return uc_grid.GRID_CHK_UNCHECKED
    End If
  End Function 'BoolToGridValue

  ' PURPOSE:  Cast from  uc_grid checked to boolean
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GridValuetoBool(ByVal enabled As String) As Boolean
    If enabled = uc_grid.GRID_CHK_CHECKED Or enabled = uc_grid.GRID_CHK_CHECKED_DISABLED Then
      Return True
    Else
      Return False
    End If
  End Function 'BoolToGridValue

  ' PURPOSE:  Refresh Documents Combo
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub RefreshCombo()

    Try

      If Me.cb_default_document.Count > 0 Then
        If String.IsNullOrEmpty(Me.m_default_document) Then
          Me.m_default_document = Me.cb_default_document.TextValue
        End If
        Me.cb_default_document.Clear()
        'insert combo empty value
        Me.cb_default_document.Add(0, "")
      End If

      For _idx_row As Integer = 0 To Me.dg_principal_documents_config.NumRows - 1

        With Me.dg_principal_documents_config
          If GridValuetoBool(.Cell(_idx_row, GRID_COLUMN_VISIBLE).Value) Then
            If Not .Cell(_idx_row, GRID_COLUMN_ID).Value = ACCOUNT_HOLDER_ID_TYPE.RFC And Not .Cell(_idx_row, GRID_COLUMN_ID).Value = ACCOUNT_HOLDER_ID_TYPE.CURP Then
              If Not .Cell(_idx_row, GRID_COLUMN_NAME).Value = Convert.ToInt16(ACCOUNT_HOLDER_ID_TYPE.RFC).ToString("000") And Not .Cell(_idx_row, GRID_COLUMN_NAME).Value = Convert.ToInt16(ACCOUNT_HOLDER_ID_TYPE.CURP).ToString("000") Then
                Me.cb_default_document.Add(.Cell(_idx_row, GRID_COLUMN_NAME).Value)
              End If
              If .Cell(_idx_row, GRID_COLUMN_NAME).Value = m_default_document Then
                Me.cb_default_document.Value = Me.cb_default_document.Count
              End If
            End If
          End If
        End With

      Next

      If String.IsNullOrEmpty(Me.cb_default_document.TextValue) Then
        Me.m_default_document = String.Empty
      End If

    Catch _ex As Exception

      Log.Exception(_ex)
    End Try
  End Sub

  ' PURPOSE: Get All the info from Documents grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GetDataFromDocuments(ByRef Data As CLS_REQUIRED_FIELDS)
    Dim _types_visible As List(Of String)

    Dim _types_required As List(Of String)
    Dim _types_scan As List(Of String)
    Dim _types_required_aml As List(Of String)
    Dim _types_scan_aml As List(Of String)

    Dim _types_benef_required As List(Of String)
    Dim _types_benef_scan As List(Of String)
    Dim _types_benef_required_aml As List(Of String)
    Dim _types_benef_scan_aml As List(Of String)

    _types_visible = New List(Of String)

    _types_required = New List(Of String)
    _types_scan = New List(Of String)
    _types_required_aml = New List(Of String)
    _types_scan_aml = New List(Of String)

    _types_benef_required = New List(Of String)
    _types_benef_scan = New List(Of String)
    _types_benef_required_aml = New List(Of String)
    _types_benef_scan_aml = New List(Of String)

    With Me.dg_principal_documents_config
      For _idx_row As Integer = 0 To .NumRows - 1
        If GridValuetoBool(.Cell(_idx_row, GRID_COLUMN_VISIBLE).Value) Then

          _types_visible.Add(.Cell(_idx_row, GRID_COLUMN_ID).Value)
        End If

        'PRINCIPAL
        If GridValuetoBool(.Cell(_idx_row, GRID_COLUMN_REQUIRED).Value) Then

          _types_required.Add(.Cell(_idx_row, GRID_COLUMN_ID).Value)
        End If

        If GridValuetoBool(.Cell(_idx_row, GRID_PRINCIPAL_COLUMN_REQUIRED_SCAN).Value) Then

          _types_scan.Add(.Cell(_idx_row, GRID_COLUMN_ID).Value)
        End If

        If GridValuetoBool(.Cell(_idx_row, GRID_PRINCIPAL_COLUMN_REQUIRED_AML).Value) Then

          _types_required_aml.Add(.Cell(_idx_row, GRID_COLUMN_ID).Value)
        End If

        If GridValuetoBool(.Cell(_idx_row, GRID_PRINCIPAL_COLUMN_REQUIRED_AML_SCAN).Value) Then

          _types_scan_aml.Add(.Cell(_idx_row, GRID_COLUMN_ID).Value)
        End If

        If m_has_beneficiary Then
          ' BENEFICIARY
          If GridValuetoBool(.Cell(_idx_row, GRID_BENEF_COLUMN_REQUIRED).Value) Then

            _types_benef_required.Add(.Cell(_idx_row, GRID_COLUMN_ID).Value)
          End If


          If GridValuetoBool(.Cell(_idx_row, GRID_BENEF_COLUMN_REQUIRED_SCAN).Value) Then

            _types_benef_scan.Add(.Cell(_idx_row, GRID_COLUMN_ID).Value)
          End If

          If GridValuetoBool(.Cell(_idx_row, GRID_BENEF_COLUMN_REQUIRED_AML).Value) Then

            _types_benef_required_aml.Add(.Cell(_idx_row, GRID_COLUMN_ID).Value)
          End If

          If GridValuetoBool(.Cell(_idx_row, GRID_BENEF_COLUMN_REQUIRED_AML_SCAN).Value) Then

            _types_benef_scan_aml.Add(.Cell(_idx_row, GRID_COLUMN_ID).Value)
          End If
        End If
      Next
    End With

    If _types_visible.Count > 0 Then
      Data.VisibleDocuments = String.Join(",", _types_visible.ToArray())
    Else
      Data.VisibleDocuments = String.Empty
    End If

    If _types_required.Count > 0 Then
      Data.AccountRequiredField.DocumentTypeList = String.Join(",", _types_required.ToArray())
    Else
      Data.AccountRequiredField.DocumentTypeList = String.Empty
    End If

    If _types_scan.Count > 0 Then
      Data.AccountRequiredField.DocScanTypeList = String.Join(",", _types_scan.ToArray())
    Else
      Data.AccountRequiredField.DocScanTypeList = String.Empty
    End If

    If _types_required_aml.Count > 0 Then
      Data.AccountRequiredField.AMLDocumentTypeList = String.Join(",", _types_required_aml.ToArray())
    Else
      Data.AccountRequiredField.AMLDocumentTypeList = String.Empty
    End If

    If _types_scan_aml.Count > 0 Then
      Data.AccountRequiredField.AMLDocScanTypeList = String.Join(",", _types_scan_aml.ToArray())
    Else
      Data.AccountRequiredField.AMLDocScanTypeList = String.Empty
    End If

    If m_has_beneficiary Then
      If _types_benef_required.Count > 0 Then
        Data.BeneficiaryRequiredField.DocumentTypeList = String.Join(",", _types_benef_required.ToArray())
      Else
        Data.BeneficiaryRequiredField.DocumentTypeList = String.Empty
      End If

      If _types_benef_scan.Count > 0 Then
        Data.BeneficiaryRequiredField.DocScanTypeList = String.Join(",", _types_benef_scan.ToArray())
      Else
        Data.BeneficiaryRequiredField.DocScanTypeList = String.Empty
      End If

      If _types_benef_required_aml.Count > 0 Then
        Data.BeneficiaryRequiredField.AMLDocumentTypeList = String.Join(",", _types_benef_required_aml.ToArray())
      Else
        Data.BeneficiaryRequiredField.AMLDocumentTypeList = String.Empty
      End If

      If _types_scan_aml.Count > 0 Then
        Data.BeneficiaryRequiredField.AMLDocScanTypeList = String.Join(",", _types_benef_scan_aml.ToArray())
      Else
        Data.BeneficiaryRequiredField.AMLDocScanTypeList = String.Empty
      End If
    End If
  End Sub

  '  PURPOSE: Every time that grid documents has a change, we process column to check/uncheck docscan values
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub ProcessDocumentsCheck()
    Dim _check_holder_AML As Boolean
    Dim _check_benef_AML As Boolean
    Dim _check_holder_doscan As Boolean
    Dim _check_benef_doscan As Boolean

    Try
      _check_holder_AML = False
      _check_benef_AML = False
      _check_holder_doscan = False
      _check_benef_doscan = False

      For _idx_rows As Integer = 0 To Me.dg_principal_documents_config.NumRows - 1
        With Me.dg_principal_documents_config
          ' HOLDER
          If GridValuetoBool(.Cell(_idx_rows, GRID_PRINCIPAL_COLUMN_REQUIRED_SCAN).Value) Then
            _check_holder_doscan = True
          End If
          If GridValuetoBool(.Cell(_idx_rows, GRID_PRINCIPAL_COLUMN_REQUIRED_AML_SCAN).Value) Then
            _check_holder_AML = True
          End If

          If m_has_beneficiary Then
            ' BENEFICIARY
            If GridValuetoBool(.Cell(_idx_rows, GRID_BENEF_COLUMN_REQUIRED_SCAN).Value) Then
              _check_benef_doscan = True
            End If
            If GridValuetoBool(.Cell(_idx_rows, GRID_BENEF_COLUMN_REQUIRED_AML_SCAN).Value) Then
              _check_benef_AML = True
            End If
          End If
        End With
      Next

      ' Check/Uncheck docscan grid value from documents data
      Call UpdateGridAfterCheckDocuments(_check_holder_AML, GRID_ROW_DOCSCAN, GRID_COLUMN_AML, Me.dg_principal_config, True)
      Call UpdateGridAfterCheckDocuments(_check_holder_doscan, GRID_ROW_DOCSCAN, GRID_COLUMN_REQUIRED, Me.dg_principal_config, True)

      If m_has_beneficiary Then
        Call UpdateGridAfterCheckDocuments(_check_benef_AML, GRID_ROW_BENEFICIARY_DOCSCAN, GRID_COLUMN_AML, Me.dg_beneficiary_config, False)
        Call UpdateGridAfterCheckDocuments(_check_benef_doscan, GRID_ROW_BENEFICIARY_DOCSCAN, GRID_COLUMN_REQUIRED, Me.dg_beneficiary_config, False)
      End If
    Catch ex As Exception

      Log.Exception(ex)
    End Try

  End Sub

  '  PURPOSE: Every time that grid documents has a change, we process column to check/uncheck docscan values
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub UpdateGridAfterCheckDocuments(ByVal Checked As Boolean, ByVal Row As Integer, ByVal Column As Integer, ByRef Grid As uc_grid, ByVal HasVisible As Boolean)

    If Checked Then
      If (Grid.Cell(Row, Column).Value = uc_grid.GRID_CHK_UNCHECKED) Or (Grid.Cell(Row, Column).Value = uc_grid.GRID_CHK_CHECKED) Then
        Grid.Cell(Row, Column).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
        If HasVisible Then
          Grid.Cell(Row, GRID_COLUMN_VISIBLE).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
        End If
      End If
    Else
      If Grid.Cell(Row, Column).Value = uc_grid.GRID_CHK_CHECKED_DISABLED Then
        Grid.Cell(Row, Column).Value = uc_grid.GRID_CHK_CHECKED

        If HasVisible AndAlso Grid.Cell(Row, GRID_COLUMN_VISIBLE).Value = uc_grid.GRID_CHK_CHECKED_DISABLED _
        And Not Grid.Cell(Row, GRID_COLUMN_REQUIRED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED _
        And Not Grid.Cell(Row, GRID_COLUMN_AML).Value = uc_grid.GRID_CHK_CHECKED_DISABLED Then

          Grid.Cell(Row, GRID_COLUMN_VISIBLE).Value = uc_grid.GRID_CHK_CHECKED

        End If
      End If
    End If

  End Sub
#End Region

#Region " Events "
  ' PURPOSE: Cell Data event changed
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  ' 
  Private Sub dg_contact_CellDataChangedEvent(ByVal Row As System.Int32, ByVal Column As System.Int32) Handles dg_contact_config.CellDataChangedEvent
    Dim _checked As Boolean
    Dim _is_Editable As Boolean

    'if player click on visible check, the rest of row is empty
    If Column = GRID_COLUMN_VISIBLE AndAlso Not GridValuetoBool(Me.dg_contact_config.Cell(Row, GRID_COLUMN_VISIBLE).Value) Then

      For _idx_column As Integer = GRID_COLUMN_REQUIRED To Me.dg_contact_config.NumColumns - 1
        Me.dg_contact_config.Cell(Row, _idx_column).Value = BoolToGridValue(False)
      Next

    Else
      ' here look if the row has any restriction 
      _checked = GridValuetoBool(dg_contact_config.Cell(Row, Column).Value)

      Select Case dg_contact_config.Cell(Row, GRID_COLUMN_VISIBLE).Value
        Case uc_grid.GRID_CHK_CHECKED_DISABLED
        Case uc_grid.GRID_CHK_UNCHECKED_DISABLED
          _is_Editable = False
        Case Else
          _is_Editable = True
      End Select

      If _is_Editable And Not GridValuetoBool(dg_contact_config.Cell(Row, GRID_COLUMN_VISIBLE).Value) Then
        dg_contact_config.Cell(Row, GRID_COLUMN_VISIBLE).Value = BoolToGridValue(_checked)
      End If
    End If
  End Sub

  ' PURPOSE: Cell Data event changed
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  ' 
  Private Sub dg_address_config_CellDataChangedEvent(ByVal Row As System.Int32, ByVal Column As System.Int32) Handles dg_address_config.CellDataChangedEvent
    Dim _checked As Boolean
    Dim _is_Editable As Boolean

    If Column = GRID_COLUMN_VISIBLE AndAlso Not GridValuetoBool(Me.dg_address_config.Cell(Row, GRID_COLUMN_VISIBLE).Value) Then

      For _idx_column As Integer = GRID_COLUMN_REQUIRED To Me.dg_address_config.NumColumns - 1
        Me.dg_address_config.Cell(Row, _idx_column).Value = BoolToGridValue(False)
      Next

    Else
      ' here look if the row has any restriction 
      _checked = GridValuetoBool(dg_address_config.Cell(Row, Column).Value)

      Select Case dg_address_config.Cell(Row, GRID_COLUMN_VISIBLE).Value
        Case uc_grid.GRID_CHK_CHECKED_DISABLED
        Case uc_grid.GRID_CHK_UNCHECKED_DISABLED
          _is_Editable = False
        Case Else
          _is_Editable = True
      End Select

      If _is_Editable And Not GridValuetoBool(dg_address_config.Cell(Row, GRID_COLUMN_VISIBLE).Value) Then
        dg_address_config.Cell(Row, GRID_COLUMN_VISIBLE).Value = BoolToGridValue(_checked)
      End If
    End If
  End Sub

  ' PURPOSE: Cell Data event changed
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  ' 
  Private Sub dg_principal_documents_config_CellDataChangedEvent(ByVal Row As System.Int32, ByVal Column As System.Int32) Handles dg_principal_config.CellDataChangedEvent
    Dim _checked As Boolean
    Dim _is_Editable As Boolean

    ' here look if the row has any restriction 
    _checked = GridValuetoBool(dg_principal_config.Cell(Row, Column).Value)

    Select Case dg_principal_config.Cell(Row, GRID_COLUMN_VISIBLE).Value
      Case uc_grid.GRID_CHK_CHECKED_DISABLED
      Case uc_grid.GRID_CHK_UNCHECKED_DISABLED
        _is_Editable = False
      Case Else
        _is_Editable = True
    End Select

    If _is_Editable Then
      If Column = GRID_COLUMN_VISIBLE AndAlso Not GridValuetoBool(Me.dg_principal_config.Cell(Row, GRID_COLUMN_VISIBLE).Value) Then

        For _idx_column As Integer = GRID_COLUMN_REQUIRED To Me.dg_principal_config.NumColumns - 1
          If Not Me.dg_principal_config.Cell(Row, _idx_column).Value = uc_grid.GRID_CHK_UNCHECKED_DISABLED Then
            Me.dg_principal_config.Cell(Row, _idx_column).Value = BoolToGridValue(False)
          End If
        Next

      Else

        If Not GridValuetoBool(dg_principal_config.Cell(Row, GRID_COLUMN_VISIBLE).Value) Then
          dg_principal_config.Cell(Row, GRID_COLUMN_VISIBLE).Value = BoolToGridValue(_checked)
        End If
      End If
    End If
  End Sub

  ' PURPOSE: Cell Data event changed
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  ' 
  Private Sub dg_principal_config_BeforeStartEditionEvent(ByVal Row As System.Int32, ByVal Column As System.Int32, ByRef EditionCanceled As System.Boolean) Handles dg_principal_config.BeforeStartEditionEvent

    Dim _is_Editable As Boolean = False
    Dim _visible_is_editable As Boolean = False

    If Column > GRID_COLUMN_NAME Then

      Select Case dg_principal_config.Cell(Row, Column).Value
        Case uc_grid.GRID_CHK_CHECKED_DISABLED
        Case uc_grid.GRID_CHK_UNCHECKED_DISABLED
          _is_Editable = False
        Case Else
          _is_Editable = True
      End Select

      EditionCanceled = Not _is_Editable
    End If
  End Sub

  ' PURPOSE: Cell Data event changed
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  ' 
  Private Sub dg_principal_documents_config_CellDataChangedEvent_1(ByVal Row As System.Int32, ByVal Column As System.Int32) Handles dg_principal_documents_config.CellDataChangedEvent, Me.CellDataChange
    Dim _checked As Boolean
    Dim _is_Editable As Boolean

    ' Refresh Documents combo every time that check/uncheck visible column
    If Column = GRID_COLUMN_VISIBLE Then
      Call RefreshCombo()
    End If

    ' When we are unchecking visible column, the remaining columns (editables) should be unchecked.
    If Column = GRID_COLUMN_VISIBLE AndAlso Not GridValuetoBool(Me.dg_principal_documents_config.Cell(Row, GRID_COLUMN_VISIBLE).Value) Then
      For _idx_column As Integer = GRID_COLUMN_REQUIRED To Me.dg_principal_documents_config.NumColumns - 1
        If Me.dg_principal_documents_config.Cell(Row, _idx_column).Value <> uc_grid.GRID_CHK_UNCHECKED_DISABLED Then
          Me.dg_principal_documents_config.Cell(Row, _idx_column).Value = BoolToGridValue(False)
        End If
      Next
      If Not Me.dg_principal_documents_config.Cell(Row, GRID_COLUMN_ID).Value = ACCOUNT_HOLDER_ID_TYPE.RFC And Not Me.dg_principal_documents_config.Cell(Row, GRID_COLUMN_ID).Value = ACCOUNT_HOLDER_ID_TYPE.CURP Then
        Me.dg_principal_documents_config.Cell(Row, GRID_PRINCIPAL_COLUMN_REQUIRED_AML).Value = uc_grid.GRID_CHK_UNCHECKED_DISABLED
        Me.dg_principal_documents_config.Cell(Row, GRID_COLUMN_REQUIRED).Value = uc_grid.GRID_CHK_UNCHECKED_DISABLED
      End If

      If Me.dg_principal_documents_config.Cell(Row, GRID_COLUMN_ID).Value = ACCOUNT_HOLDER_ID_TYPE.RFC Then
        Me.dg_principal_documents_config.Cell(Row, GRID_PRINCIPAL_COLUMN_REQUIRED_AML).Value = uc_grid.GRID_CHK_UNCHECKED_DISABLED
        Me.dg_principal_documents_config.Cell(Row, GRID_BENEF_COLUMN_REQUIRED_AML).Value = uc_grid.GRID_CHK_UNCHECKED_DISABLED
      End If

    Else
      ' else we check if current column is editable
      _checked = GridValuetoBool(dg_principal_documents_config.Cell(Row, Column).Value)

      Select Case dg_principal_documents_config.Cell(Row, GRID_COLUMN_VISIBLE).Value
        Case uc_grid.GRID_CHK_CHECKED_DISABLED
        Case uc_grid.GRID_CHK_UNCHECKED_DISABLED
          _is_Editable = False
        Case Else
          _is_Editable = True
      End Select

      If _is_Editable And Not GridValuetoBool(dg_principal_documents_config.Cell(Row, GRID_COLUMN_VISIBLE).Value) Then
        dg_principal_documents_config.Cell(Row, GRID_COLUMN_VISIBLE).Value = BoolToGridValue(_checked)
      End If
      If Not Me.dg_principal_documents_config.Cell(Row, GRID_COLUMN_ID).Value = ACCOUNT_HOLDER_ID_TYPE.RFC And Not Me.dg_principal_documents_config.Cell(Row, GRID_COLUMN_ID).Value = ACCOUNT_HOLDER_ID_TYPE.CURP Then
        Me.dg_principal_documents_config.Cell(Row, GRID_PRINCIPAL_COLUMN_REQUIRED_AML).Value = uc_grid.GRID_CHK_UNCHECKED_DISABLED 'JBC990
        Me.dg_principal_documents_config.Cell(Row, GRID_COLUMN_REQUIRED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
      End If

      If Me.dg_principal_documents_config.Cell(Row, GRID_COLUMN_ID).Value = ACCOUNT_HOLDER_ID_TYPE.RFC Then
        Me.dg_principal_documents_config.Cell(Row, GRID_PRINCIPAL_COLUMN_REQUIRED_AML).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
        If m_has_beneficiary Then
          Me.dg_principal_documents_config.Cell(Row, GRID_BENEF_COLUMN_REQUIRED_AML).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
        End If
      End If

    End If

    Call ProcessDocumentsCheck()
  End Sub

  ' PURPOSE: Cell Data event changed
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  ' 
  Private Sub dg_principal_documents_config_BeforeStartEditionEvent(ByVal Row As System.Int32, ByVal Column As System.Int32, ByRef EditionCanceled As System.Boolean) Handles dg_principal_documents_config.BeforeStartEditionEvent

    Dim _is_Editable As Boolean = False
    Dim _visible_is_editable As Boolean = False

    If Column > GRID_COLUMN_NAME Then

      Select Case dg_principal_documents_config.Cell(Row, Column).Value
        Case uc_grid.GRID_CHK_CHECKED_DISABLED
        Case uc_grid.GRID_CHK_UNCHECKED_DISABLED
          _is_Editable = False
        Case Else
          _is_Editable = True
      End Select

      EditionCanceled = Not _is_Editable
    End If
  End Sub

  ' PURPOSE: Cell Data event changed
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  ' 
  Private Sub dg_beneficiary_config_BeforeStartEditionEvent(ByVal Row As System.Int32, ByVal Column As System.Int32, ByRef EditionCanceled As System.Boolean) Handles dg_beneficiary_config.BeforeStartEditionEvent

    Dim _is_Editable As Boolean = False
    Dim _visible_is_editable As Boolean = False

    If Column > GRID_COLUMN_NAME Then

      Select Case dg_beneficiary_config.Cell(Row, Column).Value
        Case uc_grid.GRID_CHK_CHECKED_DISABLED
        Case uc_grid.GRID_CHK_UNCHECKED_DISABLED
          _is_Editable = False
        Case Else
          _is_Editable = True
      End Select

      EditionCanceled = Not _is_Editable
    End If
  End Sub

  ' PURPOSE: Cell Data event changed
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  ' 
  Private Sub cb_default_document_ValueChangedEvent() Handles cb_default_document.ValueChangedEvent
    If Me.cb_default_document.Count > 1 Then
      Me.m_default_document = Me.cb_default_document.TextValue
    End If
  End Sub
#End Region
End Class