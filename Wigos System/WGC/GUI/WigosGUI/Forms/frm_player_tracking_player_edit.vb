'-------------------------------------------------------------------
' Copyright � 2007-2009 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_player_tracking_player_edit
' DESCRIPTION:   
' AUTHOR:        Miquel Beltran
' CREATION DATE: 04-DEC-2009
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 04-DEC-2009  MBF    Initial version
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data

Public Class frm_player_tracking_player_edit
  Inherits frm_base_edit

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_FORM.FORM_PLAYER_TRAKING_PLAYER_EDIT

    Call MyBase.GUI_SetFormId()
  End Sub

  'PURPOSE: Executed just before closing form
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)

    ' TODO

    'If DiscardChanges() Then
    '  CloseCanceled = False
    'Else
    '  CloseCanceled = True
    'End If
  End Sub

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' - Form title
   
    ' - Buttons
    ef_player_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_ALPHA_NUMERIC, 50)
    ef_player_name.Text = "Nombre"
    ef_player_name.TextVisible = True

    ef_address_01.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_ALPHA_NUMERIC, 50)
    ef_address_01.Text = "Calle"
    ef_address_01.TextVisible = True

    ef_address_02.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_ALPHA_NUMERIC, 50)
    ef_address_02.Text = "Colonia"
    ef_address_02.TextVisible = True

    ef_address_03.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_ALPHA_NUMERIC, 50)
    ef_address_03.Text = "Delegaci�n"
    ef_address_03.TextVisible = True

    ef_city.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_ALPHA_NUMERIC, 50)
    ef_city.Text = "Municipio"
    ef_city.TextVisible = True

    ef_zip.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_ALPHA_NUMERIC, 10)
    ef_zip.Text = "C�digo postal"
    ef_zip.TextVisible = True

    ef_email_01.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_ALPHA_NUMERIC, 50)
    ef_email_01.Text = "e-mail #1"
    ef_email_01.TextVisible = True

    ef_email_02.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_ALPHA_NUMERIC, 50)
    ef_email_02.Text = "e-mail #2"
    ef_email_02.TextVisible = True

    ef_phone_01.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_ALPHA_NUMERIC, 20)
    ef_phone_01.Text = "Tel�fono m�vil"
    ef_phone_01.TextVisible = True

    ef_phone_02.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_ALPHA_NUMERIC, 20)
    ef_phone_02.Text = "Tel�fono fijo"
    ef_phone_02.TextVisible = True

    ' TODO
    Call dtp_born_date.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)


  End Sub ' GUI_InitControls

  ' PURPOSE: Manage permissions.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)

  End Sub 'GUI_Permissions

  ' PURPOSE: Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetInitialFocus()

    ' TODO Me.ActiveControl = fra_server

  End Sub 'GUI_SetInitialFocus

  ' PURPOSE: Init form in new mode
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overloads Sub ShowNewItem()

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    DbObjectId = Nothing
    DbStatus = ENUM_STATUS.STATUS_OK

    'Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    'Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub 'ShowNewItem

#End Region

End Class