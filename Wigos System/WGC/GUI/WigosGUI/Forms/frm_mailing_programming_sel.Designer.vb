<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_mailing_programming_sel
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_mailing_programming_sel))
    Me.gb_prog_status = New System.Windows.Forms.GroupBox()
    Me.chk_disabled = New System.Windows.Forms.CheckBox()
    Me.chk_enabled = New System.Windows.Forms.CheckBox()
    Me.ef_prog_name = New GUI_Controls.uc_entry_field()
    Me.cmb_prog_type = New GUI_Controls.uc_combo()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_prog_status.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.cmb_prog_type)
    Me.panel_filter.Controls.Add(Me.ef_prog_name)
    Me.panel_filter.Controls.Add(Me.gb_prog_status)
    Me.panel_filter.Size = New System.Drawing.Size(785, 79)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_prog_status, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_prog_name, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.cmb_prog_type, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 83)
    Me.panel_data.Size = New System.Drawing.Size(785, 384)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(779, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(779, 4)
    '
    'gb_prog_status
    '
    Me.gb_prog_status.Controls.Add(Me.chk_disabled)
    Me.gb_prog_status.Controls.Add(Me.chk_enabled)
    Me.gb_prog_status.Location = New System.Drawing.Point(8, 6)
    Me.gb_prog_status.Name = "gb_prog_status"
    Me.gb_prog_status.Size = New System.Drawing.Size(118, 64)
    Me.gb_prog_status.TabIndex = 1
    Me.gb_prog_status.TabStop = False
    Me.gb_prog_status.Text = "xStatus"
    '
    'chk_disabled
    '
    Me.chk_disabled.AutoSize = True
    Me.chk_disabled.Location = New System.Drawing.Point(9, 38)
    Me.chk_disabled.Name = "chk_disabled"
    Me.chk_disabled.Size = New System.Drawing.Size(82, 17)
    Me.chk_disabled.TabIndex = 2
    Me.chk_disabled.Text = "xDisabled"
    Me.chk_disabled.UseVisualStyleBackColor = True
    '
    'chk_enabled
    '
    Me.chk_enabled.AutoSize = True
    Me.chk_enabled.Location = New System.Drawing.Point(9, 16)
    Me.chk_enabled.Name = "chk_enabled"
    Me.chk_enabled.Size = New System.Drawing.Size(78, 17)
    Me.chk_enabled.TabIndex = 1
    Me.chk_enabled.Text = "xEnabled"
    Me.chk_enabled.UseVisualStyleBackColor = True
    '
    'ef_prog_name
    '
    Me.ef_prog_name.DoubleValue = 0.0R
    Me.ef_prog_name.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_prog_name.IntegerValue = 0
    Me.ef_prog_name.IsReadOnly = False
    Me.ef_prog_name.Location = New System.Drawing.Point(132, 16)
    Me.ef_prog_name.Name = "ef_prog_name"
    Me.ef_prog_name.PlaceHolder = Nothing
    Me.ef_prog_name.Size = New System.Drawing.Size(291, 25)
    Me.ef_prog_name.SufixText = "Sufix Text"
    Me.ef_prog_name.SufixTextVisible = True
    Me.ef_prog_name.TabIndex = 2
    Me.ef_prog_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_prog_name.TextValue = ""
    Me.ef_prog_name.TextWidth = 70
    Me.ef_prog_name.Value = ""
    Me.ef_prog_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'cmb_prog_type
    '
    Me.cmb_prog_type.AllowUnlistedValues = False
    Me.cmb_prog_type.AutoCompleteMode = False
    Me.cmb_prog_type.IsReadOnly = False
    Me.cmb_prog_type.Location = New System.Drawing.Point(132, 47)
    Me.cmb_prog_type.Name = "cmb_prog_type"
    Me.cmb_prog_type.SelectedIndex = -1
    Me.cmb_prog_type.Size = New System.Drawing.Size(291, 25)
    Me.cmb_prog_type.SufixText = "Sufix Text"
    Me.cmb_prog_type.SufixTextVisible = True
    Me.cmb_prog_type.TabIndex = 11
    Me.cmb_prog_type.TextCombo = Nothing
    Me.cmb_prog_type.TextWidth = 70
    '
    'frm_mailing_programming_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(793, 471)
    Me.Name = "frm_mailing_programming_sel"
    Me.Text = "frm_mailing_programming_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_prog_status.ResumeLayout(False)
    Me.gb_prog_status.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_prog_status As System.Windows.Forms.GroupBox
  Friend WithEvents ef_prog_name As GUI_Controls.uc_entry_field
  Friend WithEvents chk_disabled As System.Windows.Forms.CheckBox
  Friend WithEvents chk_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents cmb_prog_type As GUI_Controls.uc_combo
End Class
