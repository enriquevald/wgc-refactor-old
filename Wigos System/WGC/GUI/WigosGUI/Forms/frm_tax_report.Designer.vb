<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_tax_report
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.uc_date = New GUI_Controls.uc_daily_session_selector
    Me.chk_rows = New System.Windows.Forms.CheckBox
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.chk_rows)
    Me.panel_filter.Controls.Add(Me.uc_date)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(941, 94)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_rows, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 98)
    Me.panel_data.Size = New System.Drawing.Size(941, 476)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(935, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(935, 4)
    '
    'uc_date
    '
    Me.uc_date.ClosingTime = 0
    Me.uc_date.ClosingTimeEnabled = False
    Me.uc_date.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_date.FromDateSelected = False
    Me.uc_date.Location = New System.Drawing.Point(6, 2)
    Me.uc_date.Name = "uc_date"
    Me.uc_date.ShowBorder = True
    Me.uc_date.Size = New System.Drawing.Size(257, 80)
    Me.uc_date.TabIndex = 11
    Me.uc_date.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_date.ToDateSelected = True
    '
    'chk_rows
    '
    Me.chk_rows.AutoSize = True
    Me.chk_rows.Location = New System.Drawing.Point(269, 7)
    Me.chk_rows.Name = "chk_rows"
    Me.chk_rows.Size = New System.Drawing.Size(63, 17)
    Me.chk_rows.TabIndex = 12
    Me.chk_rows.Text = "xRows"
    Me.chk_rows.UseVisualStyleBackColor = True
    '
    'frm_tax_report
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(951, 578)
    Me.Name = "frm_tax_report"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_tax_report"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_date As GUI_Controls.uc_daily_session_selector
  Friend WithEvents chk_rows As System.Windows.Forms.CheckBox
End Class
