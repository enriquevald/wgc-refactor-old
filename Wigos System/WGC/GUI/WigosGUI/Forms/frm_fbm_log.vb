﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_fbm_log.vb
' DESCRIPTION:   FBM WebService log
' AUTHOR:        Alberto Marcos
' CREATION DATE: 07-SEP-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 07-SEP-2017  AMF     Initial version
' 05-OCT-2017  DPC     [WIGOS-5481]: FBM - WS - CloseSessionConfirmation
' -------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports WSI.Common.Promotion
Imports WSI.Common.Buckets
Imports WSI.Common
Imports GUI_CommonMisc
Imports System.Xml
Imports System.Text
Imports WSI.WCP.WCP_FBM

Public Class frm_fbm_log

  Inherits frm_base_sel

#Region " Constants "

  ' SQL Columns
  Private Const SQL_COLUMN_LOG_ID As Integer = 0
  Private Const SQL_COLUMN_STATUS As Integer = 1
  Private Const SQL_COLUMN_TYPE As Integer = 2
  Private Const SQL_COLUMN_REQUEST As Integer = 3
  Private Const SQL_COLUMN_RESPONSE As Integer = 4
  Private Const SQL_COLUMN_CREATION As Integer = 5
  Private Const SQL_COLUMN_CREATION_USER As Integer = 6

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_TYPE As Integer = 1
  Private Const GRID_COLUMN_STATUS As Integer = 2
  Private Const GRID_COLUMN_CREATION As Integer = 3
  Private Const GRID_COLUMN_CREATION_USER As Integer = 4

  Private Const GRID_COLUMNS As Integer = 5
  Private Const GRID_HEADER_ROWS As Integer = 1

  ' DATAGRID Width
  Private Const GRID_WIDTH_INDEX As Integer = 200
  Private Const GRID_WIDTH_STATUS As Integer = 2500
  Private Const GRID_WIDTH_TYPE As Integer = 2500
  Private Const GRID_WIDTH_USER As Integer = 2000
  Private Const GRID_WIDTH_DATES As Integer = 2000

  Private Const TIME_RETRY As Integer = 30000
  Private Const NUM_RETRIES As Integer = 3

#End Region ' Constants

#Region " Members "

  Private m_unbalance As Boolean

  Private m_session_status_fbm As SessionStatusFBM
  Private m_machine_status_fbm As BlockStatusFBM
  Private m_session_status_gui As SessionStatusFBM
  Private m_machine_status_gui As BlockStatusFBM

  Private m_form_loading As frm_loading

#End Region ' Members 

#Region " Overrides "

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_FBM_LOG_SEL
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()
    Call IniForm()


  End Sub ' GUI_InitControls

  ''' <summary>
  ''' Calls the Creditline class to return the SQL Query
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _sb As StringBuilder

    _sb = New StringBuilder
    _sb.AppendLine(" SELECT TOP 10   FL_ID			      ")
    _sb.AppendLine("               , FL_STATUS		    ")
    _sb.AppendLine("               , FL_TYPE		      ")
    _sb.AppendLine("               , FL_REQUEST		    ")
    _sb.AppendLine("               , FL_RESPONSE	    ")
    _sb.AppendLine("               , FL_CREATION	    ")
    _sb.AppendLine("               , FL_CREATION_USER ")
    _sb.AppendLine("          FROM   FBM_LOG		      ")
    _sb.AppendLine("      ORDER BY   FL_CREATION DESC ")

    Return _sb.ToString

  End Function 'GUI_FilterGetSqlQuery

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <param name="RowIndex"></param>
  ''' <param name="DbRow"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    If Not DbRow.IsNull(SQL_COLUMN_STATUS) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GetStatusDescription(DbRow.Value(SQL_COLUMN_STATUS))
    End If

    If Not DbRow.IsNull(SQL_COLUMN_TYPE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE).Value = GetTypeDescription(DbRow.Value(SQL_COLUMN_TYPE))
    End If

    If Not DbRow.IsNull(SQL_COLUMN_CREATION) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CREATION).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_CREATION), _
                                                                          ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                          ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_CREATION_USER) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CREATION_USER).Value = DbRow.Value(SQL_COLUMN_CREATION_USER)
    End If

    Return True

  End Function ' GUI_SetupRow

  ''' <summary>
  ''' Perform non-default actions for Button Clicks
  ''' </summary>
  ''' <param name="ButtonId"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case ENUM_BUTTON.BUTTON_CUSTOM_0

        Call SetStatus()

      Case ENUM_BUTTON.BUTTON_CUSTOM_1

        If m_unbalance Then
          Call UpdateExpectedStatus(GLB_CurrentUser.Name, m_session_status_gui, m_machine_status_gui)
        Else
          Call OpenCloseSession()
        End If

        Call SetStatus()

        Call GUI_ButtonClick(frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY)

      Case ENUM_BUTTON.BUTTON_SELECT
        Return

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub 'GUI_ButtonClick

#End Region ' Overrides

#Region " Private Methods "

  ''' <summary>
  ''' Set style to grid
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GUI_StyleSheetFbmLog()

    With Me.Grid

      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, True)

      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Provider Name
      .Column(GRID_COLUMN_STATUS).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8649)
      .Column(GRID_COLUMN_STATUS).Width = GRID_WIDTH_STATUS
      .Column(GRID_COLUMN_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      .Column(GRID_COLUMN_TYPE).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1087)
      .Column(GRID_COLUMN_TYPE).Width = GRID_WIDTH_TYPE
      .Column(GRID_COLUMN_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      .Column(GRID_COLUMN_CREATION).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6506)
      .Column(GRID_COLUMN_CREATION).Width = GRID_WIDTH_DATES
      .Column(GRID_COLUMN_CREATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_COLUMN_CREATION_USER).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6676)
      .Column(GRID_COLUMN_CREATION_USER).Width = GRID_WIDTH_USER
      .Column(GRID_COLUMN_CREATION_USER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With

  End Sub ' GUI_StyleSheetProviders

  ''' <summary>
  ''' Get description of type
  ''' </summary>
  ''' <param name="Type"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetTypeDescription(ByVal Type As LogType)

    Dim _description As String

    _description = String.Empty

    Select Case Type
      Case LogType.CloseSession
        _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8661)
      Case LogType.OpenSession
        _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8662)
      Case LogType.BlockMachine
        _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8663)
      Case LogType.UnblockMachine
        _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8664)
      Case LogType.CloseSessionConfirmation
        _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8737)
    End Select

    Return _description

  End Function 'GetTypeDescription

  ''' <summary>
  ''' Get description of status
  ''' </summary>
  ''' <param name="Status"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetStatusDescription(ByVal Status As LogStatus)

    Dim _description As String

    _description = String.Empty

    Select Case Status
      Case LogStatus.Close
        _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8658)
      Case LogStatus.Open
        _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8657)
      Case LogStatus.Blocked
        _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8659)
      Case LogStatus.Unblocked
        _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8660)
    End Select

    Return _description

  End Function ' GetStatusDescription

  ''' <summary>
  ''' Set status
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SetStatus()

    If Not GetExpectedStatus(m_session_status_gui, m_machine_status_gui) Then
      Me.lbl_fbm_machines_status_cont.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8654)
      Me.lbl_fbm_machines_status_cont.BackColor = Color.Yellow
      Me.lbl_fbm_session_status_cont.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8654)
      Me.lbl_fbm_session_status_cont.BackColor = Color.Yellow
    End If

    If m_session_status_gui = SessionStatusFBM.Close Then
      Me.lbl_gui_session_status_cont.Text = GetStatusDescription(LogStatus.Close)
      Me.lbl_gui_session_status_cont.BackColor = Color.Red
    Else
      Me.lbl_gui_session_status_cont.Text = GetStatusDescription(LogStatus.Open)
      Me.lbl_gui_session_status_cont.BackColor = Color.Chartreuse
    End If

    If m_machine_status_gui = BlockStatusFBM.Blocked Then
      Me.lbl_gui_machines_status_cont.Text = GetStatusDescription(LogStatus.Blocked)
      Me.lbl_gui_machines_status_cont.BackColor = Color.Red
    Else
      Me.lbl_gui_machines_status_cont.Text = GetStatusDescription(LogStatus.Unblocked)
      Me.lbl_gui_machines_status_cont.BackColor = Color.Chartreuse
    End If

    If Not GetCurrentStatus(m_session_status_fbm, m_machine_status_fbm) Then
      Me.lbl_fbm_machines_status_cont.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8654)
      Me.lbl_fbm_machines_status_cont.BackColor = Color.Yellow
      Me.lbl_fbm_session_status_cont.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8654)
      Me.lbl_fbm_session_status_cont.BackColor = Color.Yellow

      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = False
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8654)

      Return
    End If

    If m_session_status_fbm = SessionStatusFBM.Close Then
      Me.lbl_fbm_session_status_cont.Text = GetStatusDescription(LogStatus.Close)
      Me.lbl_fbm_session_status_cont.BackColor = Color.Red
    Else
      Me.lbl_fbm_session_status_cont.Text = GetStatusDescription(LogStatus.Open)
      Me.lbl_fbm_session_status_cont.BackColor = Color.Chartreuse
    End If

    If m_machine_status_fbm = BlockStatusFBM.Blocked Then
      Me.lbl_fbm_machines_status_cont.Text = GetStatusDescription(LogStatus.Blocked)
      Me.lbl_fbm_machines_status_cont.BackColor = Color.Red
    Else
      Me.lbl_fbm_machines_status_cont.Text = GetStatusDescription(LogStatus.Unblocked)
      Me.lbl_fbm_machines_status_cont.BackColor = Color.Chartreuse
    End If

    If m_session_status_fbm = m_session_status_gui AndAlso m_machine_status_fbm = m_machine_status_gui Then

      If m_session_status_fbm = SessionStatusFBM.Close Then
        Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(599)
      Else
        Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(600)
      End If

      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = True

      m_unbalance = False
    Else
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8656)
      m_unbalance = True
    End If

  End Sub ' SetStatus

  ''' <summary>
  ''' Make open or close in webservice FBM
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub OpenCloseSession()

    If m_session_status_fbm = SessionStatusFBM.Close Then
      Call OpenSessionFBM(m_session_status_fbm, m_machine_status_gui, GLB_CurrentUser.Name)
    Else
      Call CloseSession()
    End If

  End Sub ' OpenCloseSession

  ''' <summary>
  ''' Form initialization
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub IniForm()

    Call HideButtons()
    Call IniNLS()

    m_unbalance = True

    Call GUI_StyleSheetFbmLog()
    Call SetStatus()

  End Sub ' IniForm

  ''' <summary>
  ''' Hide buttons
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub HideButtons()

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CUSTOM_1).Type = uc_button.ENUM_BUTTON_TYPE.USER
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CUSTOM_1).Size = New System.Drawing.Size(90, 45)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_PRINT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_EXCEL).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_RESET).Visible = False

  End Sub ' HideButtons

  ''' <summary>
  ''' NLS Initialization
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub IniNLS()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8666)

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8655)

    Me.lbl_fbm_session_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8652)
    Me.lbl_gui_session_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8653)
    Me.lbl_fbm_machines_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8652)
    Me.lbl_gui_machines_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8653)

    Me.gb_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8649)
    Me.gb_session.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8650)
    Me.gb_machines.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8651)

  End Sub ' IniNLS


  Private Function CloseSession() As Boolean

    Dim _i As Int32
    Dim _message_cerrando As String
    Dim _thread As Threading.Thread
    Dim _response As FBM.CloseSessionConfirmationResp
    Dim _handled_error As Boolean

    Try

      m_form_loading = New frm_loading()

      _message_cerrando = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8705)

      m_form_loading.Message = _message_cerrando
      m_form_loading.Image = My.Resources.loading

      _thread = New Threading.Thread(AddressOf ShowLoadingMessage)
      _thread.Start()

      If Not CloseSessionFBM(m_session_status_fbm, m_machine_status_gui, GLB_CurrentUser.Name, _handled_error) Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8730), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)

        'Hide message
        Me.Invoke(New MethodInvoker(AddressOf CloseLoadingMessage))

        Return False
      End If

      If _handled_error Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8731), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)

        'Hide message
        Me.Invoke(New MethodInvoker(AddressOf CloseLoadingMessage))

        Return False
      End If

      For _i = 1 To NUM_RETRIES

        Me.Invoke(New MethodInvoker(Sub() m_form_loading.Message = String.Format("{0} - {1} {2}/{3}", _message_cerrando, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8706), _i, NUM_RETRIES)))

        Threading.Thread.Sleep(TIME_RETRY)

        _response = New FBM.CloseSessionConfirmationResp

        If Not CloseSessionConfirmationFBM(GLB_CurrentUser.Name, _response) Then
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8732), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)
        End If

        Select Case CType(_response.StatusCode, CloseSessionConfirmationStatusCode)
          Case CloseSessionConfirmationStatusCode.Ok

            Exit For
          Case CloseSessionConfirmationStatusCode.Retry

            Continue For
          Case CloseSessionConfirmationStatusCode.Error
            NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8733), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, , _response.StatusText)

            Exit For
          Case CloseSessionConfirmationStatusCode.InvalidFBMEstablishmentCode
            NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8734), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, , "InvalidFBMSessionId")

            Exit For
          Case CloseSessionConfirmationStatusCode.InvalidFBMSessionId
            NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8734), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, , "InvalidFBMSessionId")

            Exit For
          Case CloseSessionConfirmationStatusCode.NoCloseSessionInProgress
            NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8735), ENUM_MB_TYPE.MB_TYPE_ERROR)

            Exit For
        End Select

      Next

      Me.Invoke(New MethodInvoker(AddressOf CloseLoadingMessage))

      Return True

    Catch ex As Exception
      Log.Exception(ex)
    End Try

    Return False

  End Function ' CloseSession

  Private Sub ShowLoadingMessage()

    Application.Run(m_form_loading)

  End Sub ' ShowLoadingMessage

  Private Sub CloseLoadingMessage()

    m_form_loading.Hide()
    m_form_loading.Close()

  End Sub ' CloseLoadingMessage

#End Region ' Private Methods

End Class