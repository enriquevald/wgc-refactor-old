﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_customer_dashboard_report_sel
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_customer_dashboard_report_sel))
    Me.gb_gender = New System.Windows.Forms.GroupBox()
    Me.chk_gender_unknown = New System.Windows.Forms.CheckBox()
    Me.chk_gender_male = New System.Windows.Forms.CheckBox()
    Me.chk_gender_female = New System.Windows.Forms.CheckBox()
    Me.gb_level = New System.Windows.Forms.GroupBox()
    Me.chk_level_04 = New System.Windows.Forms.CheckBox()
    Me.chk_level_03 = New System.Windows.Forms.CheckBox()
    Me.chk_level_02 = New System.Windows.Forms.CheckBox()
    Me.chk_level_01 = New System.Windows.Forms.CheckBox()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.dtp_to_creation = New GUI_Controls.uc_date_picker()
    Me.dtp_from_creation = New GUI_Controls.uc_date_picker()
    Me.uc_account_search = New GUI_Controls.uc_account_sel()
    Me.cmb_birthday = New GUI_Controls.uc_combo()
    Me.gb_visits = New System.Windows.Forms.GroupBox()
    Me.pnl_visit = New System.Windows.Forms.Panel()
    Me.cmb_visit = New GUI_Controls.uc_combo()
    Me.opt_last_visit = New System.Windows.Forms.RadioButton()
    Me.dtp_to_visit = New GUI_Controls.uc_date_picker()
    Me.dtp_from_visit = New GUI_Controls.uc_date_picker()
    Me.opt_last_visit_date = New System.Windows.Forms.RadioButton()
    Me.chk_visit = New System.Windows.Forms.CheckBox()
    Me.gb_visit_month = New System.Windows.Forms.GroupBox()
    Me.lbl_description_month = New System.Windows.Forms.Label()
    Me.chk_visit_month_01 = New System.Windows.Forms.CheckBox()
    Me.chk_visit_month_04 = New System.Windows.Forms.CheckBox()
    Me.chk_visit_month_06 = New System.Windows.Forms.CheckBox()
    Me.chk_visit_month_03 = New System.Windows.Forms.CheckBox()
    Me.chk_visit_month_05 = New System.Windows.Forms.CheckBox()
    Me.chk_visit_month_02 = New System.Windows.Forms.CheckBox()
    Me.ef_month_from_01 = New GUI_Controls.uc_entry_field()
    Me.ef_month_to_01 = New GUI_Controls.uc_entry_field()
    Me.ef_month_from_02 = New GUI_Controls.uc_entry_field()
    Me.ef_month_to_02 = New GUI_Controls.uc_entry_field()
    Me.ef_month_from_06 = New GUI_Controls.uc_entry_field()
    Me.ef_month_to_06 = New GUI_Controls.uc_entry_field()
    Me.ef_month_from_05 = New GUI_Controls.uc_entry_field()
    Me.ef_month_to_05 = New GUI_Controls.uc_entry_field()
    Me.ef_month_from_04 = New GUI_Controls.uc_entry_field()
    Me.ef_month_to_04 = New GUI_Controls.uc_entry_field()
    Me.ef_month_from_03 = New GUI_Controls.uc_entry_field()
    Me.ef_month_to_03 = New GUI_Controls.uc_entry_field()
    Me.gb_age_range = New System.Windows.Forms.GroupBox()
    Me.pnl_age_range = New System.Windows.Forms.Panel()
    Me.cmb_age_range = New GUI_Controls.uc_combo()
    Me.opt_range = New System.Windows.Forms.RadioButton()
    Me.opt_range_custom = New System.Windows.Forms.RadioButton()
    Me.ef_age_from = New GUI_Controls.uc_entry_field()
    Me.ef_age_to = New GUI_Controls.uc_entry_field()
    Me.chk_range_age = New System.Windows.Forms.CheckBox()
    Me.gb_visit_week = New System.Windows.Forms.GroupBox()
    Me.lbl_description_week = New System.Windows.Forms.Label()
    Me.chk_day_saturday = New System.Windows.Forms.CheckBox()
    Me.ef_from_saturday = New GUI_Controls.uc_entry_field()
    Me.ef_to_saturday = New GUI_Controls.uc_entry_field()
    Me.chk_day_sunday = New System.Windows.Forms.CheckBox()
    Me.chk_day_wednesday = New System.Windows.Forms.CheckBox()
    Me.chk_day_friday = New System.Windows.Forms.CheckBox()
    Me.chk_day_thuesday = New System.Windows.Forms.CheckBox()
    Me.chk_day_thursday = New System.Windows.Forms.CheckBox()
    Me.chk_day_monday = New System.Windows.Forms.CheckBox()
    Me.ef_from_sunday = New GUI_Controls.uc_entry_field()
    Me.ef_to_sunday = New GUI_Controls.uc_entry_field()
    Me.ef_from_monday = New GUI_Controls.uc_entry_field()
    Me.ef_to_monday = New GUI_Controls.uc_entry_field()
    Me.ef_from_friday = New GUI_Controls.uc_entry_field()
    Me.ef_to_friday = New GUI_Controls.uc_entry_field()
    Me.ef_from_thursday = New GUI_Controls.uc_entry_field()
    Me.ef_to_thursday = New GUI_Controls.uc_entry_field()
    Me.ef_from_wednesday = New GUI_Controls.uc_entry_field()
    Me.ef_to_wednesday = New GUI_Controls.uc_entry_field()
    Me.ef_from_thuesday = New GUI_Controls.uc_entry_field()
    Me.ef_to_thuesday = New GUI_Controls.uc_entry_field()
    Me.gb_games = New System.Windows.Forms.GroupBox()
    Me.lbl_description_game = New System.Windows.Forms.Label()
    Me.pnl_games = New System.Windows.Forms.Panel()
    Me.gb_bet = New System.Windows.Forms.GroupBox()
    Me.lbl_description_bet = New System.Windows.Forms.Label()
    Me.pnl_bet = New System.Windows.Forms.Panel()
    Me.ef_bet_from = New GUI_Controls.uc_entry_field()
    Me.ef_bet_to = New GUI_Controls.uc_entry_field()
    Me.chk_bet = New System.Windows.Forms.CheckBox()
    Me.ef_birthday_day = New GUI_Controls.uc_entry_field()
    Me.gb_birthday = New System.Windows.Forms.GroupBox()
    Me.pnl_birthday = New System.Windows.Forms.Panel()
    Me.chk_birthday = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_gender.SuspendLayout()
    Me.gb_level.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_visits.SuspendLayout()
    Me.pnl_visit.SuspendLayout()
    Me.gb_visit_month.SuspendLayout()
    Me.gb_age_range.SuspendLayout()
    Me.pnl_age_range.SuspendLayout()
    Me.gb_visit_week.SuspendLayout()
    Me.gb_games.SuspendLayout()
    Me.gb_bet.SuspendLayout()
    Me.pnl_bet.SuspendLayout()
    Me.gb_birthday.SuspendLayout()
    Me.pnl_birthday.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_games)
    Me.panel_filter.Controls.Add(Me.gb_birthday)
    Me.panel_filter.Controls.Add(Me.gb_bet)
    Me.panel_filter.Controls.Add(Me.gb_visit_week)
    Me.panel_filter.Controls.Add(Me.gb_age_range)
    Me.panel_filter.Controls.Add(Me.gb_visit_month)
    Me.panel_filter.Controls.Add(Me.gb_visits)
    Me.panel_filter.Controls.Add(Me.uc_account_search)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Controls.Add(Me.gb_level)
    Me.panel_filter.Controls.Add(Me.gb_gender)
    Me.panel_filter.Size = New System.Drawing.Size(1440, 396)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_gender, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_level, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account_search, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_visits, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_visit_month, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_age_range, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_visit_week, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_bet, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_birthday, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_games, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 400)
    Me.panel_data.Size = New System.Drawing.Size(1440, 423)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1434, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1434, 4)
    '
    'gb_gender
    '
    Me.gb_gender.Controls.Add(Me.chk_gender_unknown)
    Me.gb_gender.Controls.Add(Me.chk_gender_male)
    Me.gb_gender.Controls.Add(Me.chk_gender_female)
    Me.gb_gender.Location = New System.Drawing.Point(6, 112)
    Me.gb_gender.Name = "gb_gender"
    Me.gb_gender.Size = New System.Drawing.Size(108, 72)
    Me.gb_gender.TabIndex = 2
    Me.gb_gender.TabStop = False
    Me.gb_gender.Text = "xGender"
    '
    'chk_gender_unknown
    '
    Me.chk_gender_unknown.Location = New System.Drawing.Point(11, 50)
    Me.chk_gender_unknown.Name = "chk_gender_unknown"
    Me.chk_gender_unknown.Size = New System.Drawing.Size(95, 16)
    Me.chk_gender_unknown.TabIndex = 5
    Me.chk_gender_unknown.Text = "xUnknown"
    '
    'chk_gender_male
    '
    Me.chk_gender_male.Location = New System.Drawing.Point(11, 18)
    Me.chk_gender_male.Name = "chk_gender_male"
    Me.chk_gender_male.Size = New System.Drawing.Size(95, 16)
    Me.chk_gender_male.TabIndex = 3
    Me.chk_gender_male.Text = "xMen"
    '
    'chk_gender_female
    '
    Me.chk_gender_female.Location = New System.Drawing.Point(11, 34)
    Me.chk_gender_female.Name = "chk_gender_female"
    Me.chk_gender_female.Size = New System.Drawing.Size(95, 16)
    Me.chk_gender_female.TabIndex = 4
    Me.chk_gender_female.Text = "xWomen"
    '
    'gb_level
    '
    Me.gb_level.Controls.Add(Me.chk_level_04)
    Me.gb_level.Controls.Add(Me.chk_level_03)
    Me.gb_level.Controls.Add(Me.chk_level_02)
    Me.gb_level.Controls.Add(Me.chk_level_01)
    Me.gb_level.Location = New System.Drawing.Point(6, 200)
    Me.gb_level.Name = "gb_level"
    Me.gb_level.Size = New System.Drawing.Size(108, 90)
    Me.gb_level.TabIndex = 6
    Me.gb_level.TabStop = False
    Me.gb_level.Text = "xLevel"
    '
    'chk_level_04
    '
    Me.chk_level_04.AutoSize = True
    Me.chk_level_04.Location = New System.Drawing.Point(12, 16)
    Me.chk_level_04.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_level_04.Name = "chk_level_04"
    Me.chk_level_04.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_04.TabIndex = 7
    Me.chk_level_04.Text = "xLevel 04"
    Me.chk_level_04.UseVisualStyleBackColor = True
    '
    'chk_level_03
    '
    Me.chk_level_03.AutoSize = True
    Me.chk_level_03.Location = New System.Drawing.Point(12, 33)
    Me.chk_level_03.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_level_03.Name = "chk_level_03"
    Me.chk_level_03.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_03.TabIndex = 8
    Me.chk_level_03.Text = "xLevel 03"
    Me.chk_level_03.UseVisualStyleBackColor = True
    '
    'chk_level_02
    '
    Me.chk_level_02.AutoSize = True
    Me.chk_level_02.Location = New System.Drawing.Point(12, 51)
    Me.chk_level_02.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_level_02.Name = "chk_level_02"
    Me.chk_level_02.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_02.TabIndex = 9
    Me.chk_level_02.Text = "xLevel 02"
    Me.chk_level_02.UseVisualStyleBackColor = True
    '
    'chk_level_01
    '
    Me.chk_level_01.AutoSize = True
    Me.chk_level_01.Location = New System.Drawing.Point(12, 68)
    Me.chk_level_01.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_level_01.Name = "chk_level_01"
    Me.chk_level_01.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_01.TabIndex = 10
    Me.chk_level_01.Text = "xLevel 01"
    Me.chk_level_01.UseVisualStyleBackColor = True
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to_creation)
    Me.gb_date.Controls.Add(Me.dtp_from_creation)
    Me.gb_date.Location = New System.Drawing.Point(358, 3)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(224, 74)
    Me.gb_date.TabIndex = 18
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xCreationDate"
    '
    'dtp_to_creation
    '
    Me.dtp_to_creation.Checked = True
    Me.dtp_to_creation.IsReadOnly = False
    Me.dtp_to_creation.Location = New System.Drawing.Point(22, 43)
    Me.dtp_to_creation.Name = "dtp_to_creation"
    Me.dtp_to_creation.ShowCheckBox = True
    Me.dtp_to_creation.ShowUpDown = False
    Me.dtp_to_creation.Size = New System.Drawing.Size(183, 24)
    Me.dtp_to_creation.SufixText = "Sufix Text"
    Me.dtp_to_creation.SufixTextVisible = True
    Me.dtp_to_creation.TabIndex = 20
    Me.dtp_to_creation.TextWidth = 42
    Me.dtp_to_creation.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from_creation
    '
    Me.dtp_from_creation.Checked = True
    Me.dtp_from_creation.IsReadOnly = False
    Me.dtp_from_creation.Location = New System.Drawing.Point(22, 15)
    Me.dtp_from_creation.Name = "dtp_from_creation"
    Me.dtp_from_creation.ShowCheckBox = True
    Me.dtp_from_creation.ShowUpDown = False
    Me.dtp_from_creation.Size = New System.Drawing.Size(183, 24)
    Me.dtp_from_creation.SufixText = "Sufix Text"
    Me.dtp_from_creation.SufixTextVisible = True
    Me.dtp_from_creation.TabIndex = 19
    Me.dtp_from_creation.TextWidth = 42
    Me.dtp_from_creation.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'uc_account_search
    '
    Me.uc_account_search.Account = ""
    Me.uc_account_search.AccountText = ""
    Me.uc_account_search.Anon = False
    Me.uc_account_search.AutoSize = True
    Me.uc_account_search.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_account_search.BirthDate = New Date(CType(0, Long))
    Me.uc_account_search.DisabledHolder = False
    Me.uc_account_search.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.uc_account_search.Holder = ""
    Me.uc_account_search.Location = New System.Drawing.Point(6, 0)
    Me.uc_account_search.MassiveSearchNumbers = ""
    Me.uc_account_search.MassiveSearchNumbersToEdit = ""
    Me.uc_account_search.MassiveSearchType = 0
    Me.uc_account_search.Name = "uc_account_search"
    Me.uc_account_search.SearchTrackDataAsInternal = True
    Me.uc_account_search.ShowMassiveSearch = False
    Me.uc_account_search.ShowVipClients = False
    Me.uc_account_search.Size = New System.Drawing.Size(307, 108)
    Me.uc_account_search.TabIndex = 1
    Me.uc_account_search.Telephone = ""
    Me.uc_account_search.TrackData = ""
    Me.uc_account_search.Vip = False
    Me.uc_account_search.WeddingDate = New Date(CType(0, Long))
    '
    'cmb_birthday
    '
    Me.cmb_birthday.AllowUnlistedValues = False
    Me.cmb_birthday.AutoCompleteMode = False
    Me.cmb_birthday.IsReadOnly = False
    Me.cmb_birthday.Location = New System.Drawing.Point(-4, 29)
    Me.cmb_birthday.Name = "cmb_birthday"
    Me.cmb_birthday.SelectedIndex = -1
    Me.cmb_birthday.Size = New System.Drawing.Size(193, 24)
    Me.cmb_birthday.SufixText = "Sufix Text"
    Me.cmb_birthday.SufixTextVisible = True
    Me.cmb_birthday.TabIndex = 19
    Me.cmb_birthday.TextCombo = Nothing
    '
    'gb_visits
    '
    Me.gb_visits.Controls.Add(Me.pnl_visit)
    Me.gb_visits.Controls.Add(Me.chk_visit)
    Me.gb_visits.Location = New System.Drawing.Point(373, 111)
    Me.gb_visits.Name = "gb_visits"
    Me.gb_visits.Size = New System.Drawing.Size(235, 168)
    Me.gb_visits.TabIndex = 21
    Me.gb_visits.TabStop = False
    '
    'pnl_visit
    '
    Me.pnl_visit.Controls.Add(Me.cmb_visit)
    Me.pnl_visit.Controls.Add(Me.opt_last_visit)
    Me.pnl_visit.Controls.Add(Me.dtp_to_visit)
    Me.pnl_visit.Controls.Add(Me.dtp_from_visit)
    Me.pnl_visit.Controls.Add(Me.opt_last_visit_date)
    Me.pnl_visit.Location = New System.Drawing.Point(20, 20)
    Me.pnl_visit.Name = "pnl_visit"
    Me.pnl_visit.Size = New System.Drawing.Size(209, 142)
    Me.pnl_visit.TabIndex = 18
    '
    'cmb_visit
    '
    Me.cmb_visit.AllowUnlistedValues = False
    Me.cmb_visit.AutoCompleteMode = False
    Me.cmb_visit.IsReadOnly = False
    Me.cmb_visit.Location = New System.Drawing.Point(-14, 29)
    Me.cmb_visit.Name = "cmb_visit"
    Me.cmb_visit.SelectedIndex = -1
    Me.cmb_visit.Size = New System.Drawing.Size(196, 24)
    Me.cmb_visit.SufixText = "Sufix Text"
    Me.cmb_visit.SufixTextVisible = True
    Me.cmb_visit.TabIndex = 23
    Me.cmb_visit.TextCombo = Nothing
    '
    'opt_last_visit
    '
    Me.opt_last_visit.AutoSize = True
    Me.opt_last_visit.Location = New System.Drawing.Point(3, 3)
    Me.opt_last_visit.Name = "opt_last_visit"
    Me.opt_last_visit.Size = New System.Drawing.Size(79, 17)
    Me.opt_last_visit.TabIndex = 22
    Me.opt_last_visit.TabStop = True
    Me.opt_last_visit.Text = "xLastVisit"
    Me.opt_last_visit.UseVisualStyleBackColor = True
    '
    'dtp_to_visit
    '
    Me.dtp_to_visit.Checked = True
    Me.dtp_to_visit.IsReadOnly = False
    Me.dtp_to_visit.Location = New System.Drawing.Point(23, 106)
    Me.dtp_to_visit.Name = "dtp_to_visit"
    Me.dtp_to_visit.ShowCheckBox = False
    Me.dtp_to_visit.ShowUpDown = False
    Me.dtp_to_visit.Size = New System.Drawing.Size(183, 24)
    Me.dtp_to_visit.SufixText = "Sufix Text"
    Me.dtp_to_visit.SufixTextVisible = True
    Me.dtp_to_visit.TabIndex = 26
    Me.dtp_to_visit.TextWidth = 42
    Me.dtp_to_visit.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from_visit
    '
    Me.dtp_from_visit.Checked = True
    Me.dtp_from_visit.IsReadOnly = False
    Me.dtp_from_visit.Location = New System.Drawing.Point(23, 78)
    Me.dtp_from_visit.Name = "dtp_from_visit"
    Me.dtp_from_visit.ShowCheckBox = False
    Me.dtp_from_visit.ShowUpDown = False
    Me.dtp_from_visit.Size = New System.Drawing.Size(183, 24)
    Me.dtp_from_visit.SufixText = "Sufix Text"
    Me.dtp_from_visit.SufixTextVisible = True
    Me.dtp_from_visit.TabIndex = 25
    Me.dtp_from_visit.TextWidth = 42
    Me.dtp_from_visit.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'opt_last_visit_date
    '
    Me.opt_last_visit_date.AutoSize = True
    Me.opt_last_visit_date.Location = New System.Drawing.Point(3, 59)
    Me.opt_last_visit_date.Name = "opt_last_visit_date"
    Me.opt_last_visit_date.Size = New System.Drawing.Size(79, 17)
    Me.opt_last_visit_date.TabIndex = 24
    Me.opt_last_visit_date.TabStop = True
    Me.opt_last_visit_date.Text = "xLastVisit"
    Me.opt_last_visit_date.UseVisualStyleBackColor = True
    '
    'chk_visit
    '
    Me.chk_visit.AutoSize = True
    Me.chk_visit.Checked = True
    Me.chk_visit.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_visit.Location = New System.Drawing.Point(6, 0)
    Me.chk_visit.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_visit.Name = "chk_visit"
    Me.chk_visit.Size = New System.Drawing.Size(57, 17)
    Me.chk_visit.TabIndex = 21
    Me.chk_visit.Text = "xVisit"
    Me.chk_visit.UseVisualStyleBackColor = True
    '
    'gb_visit_month
    '
    Me.gb_visit_month.Controls.Add(Me.lbl_description_month)
    Me.gb_visit_month.Controls.Add(Me.chk_visit_month_01)
    Me.gb_visit_month.Controls.Add(Me.chk_visit_month_04)
    Me.gb_visit_month.Controls.Add(Me.chk_visit_month_06)
    Me.gb_visit_month.Controls.Add(Me.chk_visit_month_03)
    Me.gb_visit_month.Controls.Add(Me.chk_visit_month_05)
    Me.gb_visit_month.Controls.Add(Me.chk_visit_month_02)
    Me.gb_visit_month.Controls.Add(Me.ef_month_from_01)
    Me.gb_visit_month.Controls.Add(Me.ef_month_to_01)
    Me.gb_visit_month.Controls.Add(Me.ef_month_from_02)
    Me.gb_visit_month.Controls.Add(Me.ef_month_to_02)
    Me.gb_visit_month.Controls.Add(Me.ef_month_from_06)
    Me.gb_visit_month.Controls.Add(Me.ef_month_to_06)
    Me.gb_visit_month.Controls.Add(Me.ef_month_from_05)
    Me.gb_visit_month.Controls.Add(Me.ef_month_to_05)
    Me.gb_visit_month.Controls.Add(Me.ef_month_from_04)
    Me.gb_visit_month.Controls.Add(Me.ef_month_to_04)
    Me.gb_visit_month.Controls.Add(Me.ef_month_from_03)
    Me.gb_visit_month.Controls.Add(Me.ef_month_to_03)
    Me.gb_visit_month.Location = New System.Drawing.Point(612, 2)
    Me.gb_visit_month.Name = "gb_visit_month"
    Me.gb_visit_month.Size = New System.Drawing.Size(309, 182)
    Me.gb_visit_month.TabIndex = 27
    Me.gb_visit_month.TabStop = False
    Me.gb_visit_month.Text = "xVisitPerMonth"
    '
    'lbl_description_month
    '
    Me.lbl_description_month.ForeColor = System.Drawing.Color.Navy
    Me.lbl_description_month.Location = New System.Drawing.Point(15, 14)
    Me.lbl_description_month.Name = "lbl_description_month"
    Me.lbl_description_month.Size = New System.Drawing.Size(280, 13)
    Me.lbl_description_month.TabIndex = 71
    Me.lbl_description_month.Text = "xNumberMonth"
    Me.lbl_description_month.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'chk_visit_month_01
    '
    Me.chk_visit_month_01.AutoSize = True
    Me.chk_visit_month_01.Checked = True
    Me.chk_visit_month_01.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_visit_month_01.Location = New System.Drawing.Point(19, 37)
    Me.chk_visit_month_01.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_visit_month_01.Name = "chk_visit_month_01"
    Me.chk_visit_month_01.Size = New System.Drawing.Size(81, 17)
    Me.chk_visit_month_01.TabIndex = 28
    Me.chk_visit_month_01.Text = "xMonth01"
    Me.chk_visit_month_01.UseVisualStyleBackColor = True
    '
    'chk_visit_month_04
    '
    Me.chk_visit_month_04.AutoSize = True
    Me.chk_visit_month_04.Checked = True
    Me.chk_visit_month_04.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_visit_month_04.Location = New System.Drawing.Point(19, 109)
    Me.chk_visit_month_04.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_visit_month_04.Name = "chk_visit_month_04"
    Me.chk_visit_month_04.Size = New System.Drawing.Size(81, 17)
    Me.chk_visit_month_04.TabIndex = 37
    Me.chk_visit_month_04.Text = "xMonth04"
    Me.chk_visit_month_04.UseVisualStyleBackColor = True
    '
    'chk_visit_month_06
    '
    Me.chk_visit_month_06.AutoSize = True
    Me.chk_visit_month_06.Checked = True
    Me.chk_visit_month_06.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_visit_month_06.Location = New System.Drawing.Point(19, 157)
    Me.chk_visit_month_06.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_visit_month_06.Name = "chk_visit_month_06"
    Me.chk_visit_month_06.Size = New System.Drawing.Size(81, 17)
    Me.chk_visit_month_06.TabIndex = 43
    Me.chk_visit_month_06.Text = "xMonth06"
    Me.chk_visit_month_06.UseVisualStyleBackColor = True
    '
    'chk_visit_month_03
    '
    Me.chk_visit_month_03.AutoSize = True
    Me.chk_visit_month_03.Checked = True
    Me.chk_visit_month_03.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_visit_month_03.Location = New System.Drawing.Point(19, 85)
    Me.chk_visit_month_03.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_visit_month_03.Name = "chk_visit_month_03"
    Me.chk_visit_month_03.Size = New System.Drawing.Size(81, 17)
    Me.chk_visit_month_03.TabIndex = 34
    Me.chk_visit_month_03.Text = "xMonth03"
    Me.chk_visit_month_03.UseVisualStyleBackColor = True
    '
    'chk_visit_month_05
    '
    Me.chk_visit_month_05.AutoSize = True
    Me.chk_visit_month_05.Checked = True
    Me.chk_visit_month_05.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_visit_month_05.Location = New System.Drawing.Point(19, 133)
    Me.chk_visit_month_05.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_visit_month_05.Name = "chk_visit_month_05"
    Me.chk_visit_month_05.Size = New System.Drawing.Size(81, 17)
    Me.chk_visit_month_05.TabIndex = 40
    Me.chk_visit_month_05.Text = "xMonth05"
    Me.chk_visit_month_05.UseVisualStyleBackColor = True
    '
    'chk_visit_month_02
    '
    Me.chk_visit_month_02.AutoSize = True
    Me.chk_visit_month_02.Checked = True
    Me.chk_visit_month_02.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_visit_month_02.Location = New System.Drawing.Point(19, 61)
    Me.chk_visit_month_02.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_visit_month_02.Name = "chk_visit_month_02"
    Me.chk_visit_month_02.Size = New System.Drawing.Size(81, 17)
    Me.chk_visit_month_02.TabIndex = 31
    Me.chk_visit_month_02.Text = "xMonth02"
    Me.chk_visit_month_02.UseVisualStyleBackColor = True
    '
    'ef_month_from_01
    '
    Me.ef_month_from_01.DoubleValue = 0.0R
    Me.ef_month_from_01.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_month_from_01.IntegerValue = 0
    Me.ef_month_from_01.IsReadOnly = False
    Me.ef_month_from_01.Location = New System.Drawing.Point(65, 32)
    Me.ef_month_from_01.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.ef_month_from_01.Name = "ef_month_from_01"
    Me.ef_month_from_01.PlaceHolder = Nothing
    Me.ef_month_from_01.ShortcutsEnabled = True
    Me.ef_month_from_01.Size = New System.Drawing.Size(139, 25)
    Me.ef_month_from_01.SufixText = "Sufix Text"
    Me.ef_month_from_01.SufixTextVisible = True
    Me.ef_month_from_01.TabIndex = 29
    Me.ef_month_from_01.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_month_from_01.TextValue = ""
    Me.ef_month_from_01.Value = ""
    Me.ef_month_from_01.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_month_to_01
    '
    Me.ef_month_to_01.DoubleValue = 0.0R
    Me.ef_month_to_01.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_month_to_01.IntegerValue = 0
    Me.ef_month_to_01.IsReadOnly = False
    Me.ef_month_to_01.Location = New System.Drawing.Point(159, 32)
    Me.ef_month_to_01.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.ef_month_to_01.Name = "ef_month_to_01"
    Me.ef_month_to_01.PlaceHolder = Nothing
    Me.ef_month_to_01.ShortcutsEnabled = True
    Me.ef_month_to_01.Size = New System.Drawing.Size(139, 25)
    Me.ef_month_to_01.SufixText = "Sufix Text"
    Me.ef_month_to_01.SufixTextVisible = True
    Me.ef_month_to_01.TabIndex = 30
    Me.ef_month_to_01.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_month_to_01.TextValue = ""
    Me.ef_month_to_01.Value = ""
    Me.ef_month_to_01.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_month_from_02
    '
    Me.ef_month_from_02.DoubleValue = 0.0R
    Me.ef_month_from_02.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_month_from_02.IntegerValue = 0
    Me.ef_month_from_02.IsReadOnly = False
    Me.ef_month_from_02.Location = New System.Drawing.Point(65, 56)
    Me.ef_month_from_02.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.ef_month_from_02.Name = "ef_month_from_02"
    Me.ef_month_from_02.PlaceHolder = Nothing
    Me.ef_month_from_02.ShortcutsEnabled = True
    Me.ef_month_from_02.Size = New System.Drawing.Size(139, 25)
    Me.ef_month_from_02.SufixText = "Sufix Text"
    Me.ef_month_from_02.SufixTextVisible = True
    Me.ef_month_from_02.TabIndex = 32
    Me.ef_month_from_02.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_month_from_02.TextValue = ""
    Me.ef_month_from_02.Value = ""
    Me.ef_month_from_02.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_month_to_02
    '
    Me.ef_month_to_02.DoubleValue = 0.0R
    Me.ef_month_to_02.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_month_to_02.IntegerValue = 0
    Me.ef_month_to_02.IsReadOnly = False
    Me.ef_month_to_02.Location = New System.Drawing.Point(159, 56)
    Me.ef_month_to_02.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.ef_month_to_02.Name = "ef_month_to_02"
    Me.ef_month_to_02.PlaceHolder = Nothing
    Me.ef_month_to_02.ShortcutsEnabled = True
    Me.ef_month_to_02.Size = New System.Drawing.Size(139, 25)
    Me.ef_month_to_02.SufixText = "Sufix Text"
    Me.ef_month_to_02.SufixTextVisible = True
    Me.ef_month_to_02.TabIndex = 33
    Me.ef_month_to_02.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_month_to_02.TextValue = ""
    Me.ef_month_to_02.Value = ""
    Me.ef_month_to_02.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_month_from_06
    '
    Me.ef_month_from_06.DoubleValue = 0.0R
    Me.ef_month_from_06.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_month_from_06.IntegerValue = 0
    Me.ef_month_from_06.IsReadOnly = False
    Me.ef_month_from_06.Location = New System.Drawing.Point(65, 152)
    Me.ef_month_from_06.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.ef_month_from_06.Name = "ef_month_from_06"
    Me.ef_month_from_06.PlaceHolder = Nothing
    Me.ef_month_from_06.ShortcutsEnabled = True
    Me.ef_month_from_06.Size = New System.Drawing.Size(139, 25)
    Me.ef_month_from_06.SufixText = "Sufix Text"
    Me.ef_month_from_06.SufixTextVisible = True
    Me.ef_month_from_06.TabIndex = 44
    Me.ef_month_from_06.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_month_from_06.TextValue = ""
    Me.ef_month_from_06.Value = ""
    Me.ef_month_from_06.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_month_to_06
    '
    Me.ef_month_to_06.DoubleValue = 0.0R
    Me.ef_month_to_06.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_month_to_06.IntegerValue = 0
    Me.ef_month_to_06.IsReadOnly = False
    Me.ef_month_to_06.Location = New System.Drawing.Point(159, 152)
    Me.ef_month_to_06.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.ef_month_to_06.Name = "ef_month_to_06"
    Me.ef_month_to_06.PlaceHolder = Nothing
    Me.ef_month_to_06.ShortcutsEnabled = True
    Me.ef_month_to_06.Size = New System.Drawing.Size(139, 25)
    Me.ef_month_to_06.SufixText = "Sufix Text"
    Me.ef_month_to_06.SufixTextVisible = True
    Me.ef_month_to_06.TabIndex = 45
    Me.ef_month_to_06.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_month_to_06.TextValue = ""
    Me.ef_month_to_06.Value = ""
    Me.ef_month_to_06.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_month_from_05
    '
    Me.ef_month_from_05.DoubleValue = 0.0R
    Me.ef_month_from_05.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_month_from_05.IntegerValue = 0
    Me.ef_month_from_05.IsReadOnly = False
    Me.ef_month_from_05.Location = New System.Drawing.Point(65, 128)
    Me.ef_month_from_05.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.ef_month_from_05.Name = "ef_month_from_05"
    Me.ef_month_from_05.PlaceHolder = Nothing
    Me.ef_month_from_05.ShortcutsEnabled = True
    Me.ef_month_from_05.Size = New System.Drawing.Size(139, 25)
    Me.ef_month_from_05.SufixText = "Sufix Text"
    Me.ef_month_from_05.SufixTextVisible = True
    Me.ef_month_from_05.TabIndex = 41
    Me.ef_month_from_05.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_month_from_05.TextValue = ""
    Me.ef_month_from_05.Value = ""
    Me.ef_month_from_05.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_month_to_05
    '
    Me.ef_month_to_05.DoubleValue = 0.0R
    Me.ef_month_to_05.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_month_to_05.IntegerValue = 0
    Me.ef_month_to_05.IsReadOnly = False
    Me.ef_month_to_05.Location = New System.Drawing.Point(159, 128)
    Me.ef_month_to_05.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.ef_month_to_05.Name = "ef_month_to_05"
    Me.ef_month_to_05.PlaceHolder = Nothing
    Me.ef_month_to_05.ShortcutsEnabled = True
    Me.ef_month_to_05.Size = New System.Drawing.Size(139, 25)
    Me.ef_month_to_05.SufixText = "Sufix Text"
    Me.ef_month_to_05.SufixTextVisible = True
    Me.ef_month_to_05.TabIndex = 42
    Me.ef_month_to_05.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_month_to_05.TextValue = ""
    Me.ef_month_to_05.Value = ""
    Me.ef_month_to_05.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_month_from_04
    '
    Me.ef_month_from_04.DoubleValue = 0.0R
    Me.ef_month_from_04.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_month_from_04.IntegerValue = 0
    Me.ef_month_from_04.IsReadOnly = False
    Me.ef_month_from_04.Location = New System.Drawing.Point(65, 104)
    Me.ef_month_from_04.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.ef_month_from_04.Name = "ef_month_from_04"
    Me.ef_month_from_04.PlaceHolder = Nothing
    Me.ef_month_from_04.ShortcutsEnabled = True
    Me.ef_month_from_04.Size = New System.Drawing.Size(139, 25)
    Me.ef_month_from_04.SufixText = "Sufix Text"
    Me.ef_month_from_04.SufixTextVisible = True
    Me.ef_month_from_04.TabIndex = 38
    Me.ef_month_from_04.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_month_from_04.TextValue = ""
    Me.ef_month_from_04.Value = ""
    Me.ef_month_from_04.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_month_to_04
    '
    Me.ef_month_to_04.DoubleValue = 0.0R
    Me.ef_month_to_04.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_month_to_04.IntegerValue = 0
    Me.ef_month_to_04.IsReadOnly = False
    Me.ef_month_to_04.Location = New System.Drawing.Point(159, 104)
    Me.ef_month_to_04.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.ef_month_to_04.Name = "ef_month_to_04"
    Me.ef_month_to_04.PlaceHolder = Nothing
    Me.ef_month_to_04.ShortcutsEnabled = True
    Me.ef_month_to_04.Size = New System.Drawing.Size(139, 25)
    Me.ef_month_to_04.SufixText = "Sufix Text"
    Me.ef_month_to_04.SufixTextVisible = True
    Me.ef_month_to_04.TabIndex = 39
    Me.ef_month_to_04.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_month_to_04.TextValue = ""
    Me.ef_month_to_04.Value = ""
    Me.ef_month_to_04.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_month_from_03
    '
    Me.ef_month_from_03.DoubleValue = 0.0R
    Me.ef_month_from_03.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_month_from_03.IntegerValue = 0
    Me.ef_month_from_03.IsReadOnly = False
    Me.ef_month_from_03.Location = New System.Drawing.Point(65, 80)
    Me.ef_month_from_03.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.ef_month_from_03.Name = "ef_month_from_03"
    Me.ef_month_from_03.PlaceHolder = Nothing
    Me.ef_month_from_03.ShortcutsEnabled = True
    Me.ef_month_from_03.Size = New System.Drawing.Size(139, 25)
    Me.ef_month_from_03.SufixText = "Sufix Text"
    Me.ef_month_from_03.SufixTextVisible = True
    Me.ef_month_from_03.TabIndex = 35
    Me.ef_month_from_03.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_month_from_03.TextValue = ""
    Me.ef_month_from_03.Value = ""
    Me.ef_month_from_03.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_month_to_03
    '
    Me.ef_month_to_03.DoubleValue = 0.0R
    Me.ef_month_to_03.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_month_to_03.IntegerValue = 0
    Me.ef_month_to_03.IsReadOnly = False
    Me.ef_month_to_03.Location = New System.Drawing.Point(159, 80)
    Me.ef_month_to_03.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.ef_month_to_03.Name = "ef_month_to_03"
    Me.ef_month_to_03.PlaceHolder = Nothing
    Me.ef_month_to_03.ShortcutsEnabled = True
    Me.ef_month_to_03.Size = New System.Drawing.Size(139, 25)
    Me.ef_month_to_03.SufixText = "Sufix Text"
    Me.ef_month_to_03.SufixTextVisible = True
    Me.ef_month_to_03.TabIndex = 36
    Me.ef_month_to_03.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_month_to_03.TextValue = ""
    Me.ef_month_to_03.Value = ""
    Me.ef_month_to_03.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_age_range
    '
    Me.gb_age_range.Controls.Add(Me.pnl_age_range)
    Me.gb_age_range.Controls.Add(Me.chk_range_age)
    Me.gb_age_range.Location = New System.Drawing.Point(122, 112)
    Me.gb_age_range.Name = "gb_age_range"
    Me.gb_age_range.Size = New System.Drawing.Size(245, 134)
    Me.gb_age_range.TabIndex = 11
    Me.gb_age_range.TabStop = False
    '
    'pnl_age_range
    '
    Me.pnl_age_range.Controls.Add(Me.cmb_age_range)
    Me.pnl_age_range.Controls.Add(Me.opt_range)
    Me.pnl_age_range.Controls.Add(Me.opt_range_custom)
    Me.pnl_age_range.Controls.Add(Me.ef_age_from)
    Me.pnl_age_range.Controls.Add(Me.ef_age_to)
    Me.pnl_age_range.Location = New System.Drawing.Point(6, 20)
    Me.pnl_age_range.Name = "pnl_age_range"
    Me.pnl_age_range.Size = New System.Drawing.Size(234, 110)
    Me.pnl_age_range.TabIndex = 18
    '
    'cmb_age_range
    '
    Me.cmb_age_range.AllowUnlistedValues = False
    Me.cmb_age_range.AutoCompleteMode = False
    Me.cmb_age_range.IsReadOnly = False
    Me.cmb_age_range.Location = New System.Drawing.Point(-14, 26)
    Me.cmb_age_range.Name = "cmb_age_range"
    Me.cmb_age_range.SelectedIndex = -1
    Me.cmb_age_range.Size = New System.Drawing.Size(195, 24)
    Me.cmb_age_range.SufixText = "Sufix Text"
    Me.cmb_age_range.SufixTextVisible = True
    Me.cmb_age_range.TabIndex = 13
    Me.cmb_age_range.TextCombo = Nothing
    '
    'opt_range
    '
    Me.opt_range.AutoSize = True
    Me.opt_range.Location = New System.Drawing.Point(17, 3)
    Me.opt_range.Name = "opt_range"
    Me.opt_range.Size = New System.Drawing.Size(90, 17)
    Me.opt_range.TabIndex = 12
    Me.opt_range.TabStop = True
    Me.opt_range.Text = "xRangeAge"
    Me.opt_range.UseVisualStyleBackColor = True
    '
    'opt_range_custom
    '
    Me.opt_range_custom.AutoSize = True
    Me.opt_range_custom.Location = New System.Drawing.Point(17, 58)
    Me.opt_range_custom.Name = "opt_range_custom"
    Me.opt_range_custom.Size = New System.Drawing.Size(134, 17)
    Me.opt_range_custom.TabIndex = 14
    Me.opt_range_custom.TabStop = True
    Me.opt_range_custom.Text = "xRangeAgeCustom"
    Me.opt_range_custom.UseVisualStyleBackColor = True
    '
    'ef_age_from
    '
    Me.ef_age_from.DoubleValue = 0.0R
    Me.ef_age_from.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_age_from.IntegerValue = 0
    Me.ef_age_from.IsReadOnly = False
    Me.ef_age_from.Location = New System.Drawing.Point(-15, 77)
    Me.ef_age_from.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.ef_age_from.Name = "ef_age_from"
    Me.ef_age_from.PlaceHolder = Nothing
    Me.ef_age_from.ShortcutsEnabled = True
    Me.ef_age_from.Size = New System.Drawing.Size(139, 25)
    Me.ef_age_from.SufixText = "Sufix Text"
    Me.ef_age_from.SufixTextVisible = True
    Me.ef_age_from.TabIndex = 15
    Me.ef_age_from.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_age_from.TextValue = ""
    Me.ef_age_from.Value = ""
    Me.ef_age_from.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_age_to
    '
    Me.ef_age_to.DoubleValue = 0.0R
    Me.ef_age_to.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_age_to.IntegerValue = 0
    Me.ef_age_to.IsReadOnly = False
    Me.ef_age_to.Location = New System.Drawing.Point(92, 77)
    Me.ef_age_to.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.ef_age_to.Name = "ef_age_to"
    Me.ef_age_to.PlaceHolder = Nothing
    Me.ef_age_to.ShortcutsEnabled = True
    Me.ef_age_to.Size = New System.Drawing.Size(139, 25)
    Me.ef_age_to.SufixText = "Sufix Text"
    Me.ef_age_to.SufixTextVisible = True
    Me.ef_age_to.TabIndex = 16
    Me.ef_age_to.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_age_to.TextValue = ""
    Me.ef_age_to.Value = ""
    Me.ef_age_to.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_range_age
    '
    Me.chk_range_age.AutoSize = True
    Me.chk_range_age.Checked = True
    Me.chk_range_age.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_range_age.Location = New System.Drawing.Point(6, 0)
    Me.chk_range_age.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_range_age.Name = "chk_range_age"
    Me.chk_range_age.Size = New System.Drawing.Size(69, 17)
    Me.chk_range_age.TabIndex = 11
    Me.chk_range_age.Text = "xRange"
    Me.chk_range_age.UseVisualStyleBackColor = True
    '
    'gb_visit_week
    '
    Me.gb_visit_week.Controls.Add(Me.lbl_description_week)
    Me.gb_visit_week.Controls.Add(Me.chk_day_saturday)
    Me.gb_visit_week.Controls.Add(Me.ef_from_saturday)
    Me.gb_visit_week.Controls.Add(Me.ef_to_saturday)
    Me.gb_visit_week.Controls.Add(Me.chk_day_sunday)
    Me.gb_visit_week.Controls.Add(Me.chk_day_wednesday)
    Me.gb_visit_week.Controls.Add(Me.chk_day_friday)
    Me.gb_visit_week.Controls.Add(Me.chk_day_thuesday)
    Me.gb_visit_week.Controls.Add(Me.chk_day_thursday)
    Me.gb_visit_week.Controls.Add(Me.chk_day_monday)
    Me.gb_visit_week.Controls.Add(Me.ef_from_sunday)
    Me.gb_visit_week.Controls.Add(Me.ef_to_sunday)
    Me.gb_visit_week.Controls.Add(Me.ef_from_monday)
    Me.gb_visit_week.Controls.Add(Me.ef_to_monday)
    Me.gb_visit_week.Controls.Add(Me.ef_from_friday)
    Me.gb_visit_week.Controls.Add(Me.ef_to_friday)
    Me.gb_visit_week.Controls.Add(Me.ef_from_thursday)
    Me.gb_visit_week.Controls.Add(Me.ef_to_thursday)
    Me.gb_visit_week.Controls.Add(Me.ef_from_wednesday)
    Me.gb_visit_week.Controls.Add(Me.ef_to_wednesday)
    Me.gb_visit_week.Controls.Add(Me.ef_from_thuesday)
    Me.gb_visit_week.Controls.Add(Me.ef_to_thuesday)
    Me.gb_visit_week.Location = New System.Drawing.Point(612, 187)
    Me.gb_visit_week.Name = "gb_visit_week"
    Me.gb_visit_week.Size = New System.Drawing.Size(309, 207)
    Me.gb_visit_week.TabIndex = 46
    Me.gb_visit_week.TabStop = False
    Me.gb_visit_week.Text = "xVisitPerWeek"
    '
    'lbl_description_week
    '
    Me.lbl_description_week.ForeColor = System.Drawing.Color.Navy
    Me.lbl_description_week.Location = New System.Drawing.Point(16, 14)
    Me.lbl_description_week.Name = "lbl_description_week"
    Me.lbl_description_week.Size = New System.Drawing.Size(280, 13)
    Me.lbl_description_week.TabIndex = 72
    Me.lbl_description_week.Text = "xNumberDay"
    Me.lbl_description_week.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'chk_day_saturday
    '
    Me.chk_day_saturday.AutoSize = True
    Me.chk_day_saturday.Checked = True
    Me.chk_day_saturday.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_day_saturday.Location = New System.Drawing.Point(19, 181)
    Me.chk_day_saturday.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_day_saturday.Name = "chk_day_saturday"
    Me.chk_day_saturday.Size = New System.Drawing.Size(85, 17)
    Me.chk_day_saturday.TabIndex = 65
    Me.chk_day_saturday.Text = "xSaturday"
    Me.chk_day_saturday.UseVisualStyleBackColor = True
    '
    'ef_from_saturday
    '
    Me.ef_from_saturday.DoubleValue = 0.0R
    Me.ef_from_saturday.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_from_saturday.IntegerValue = 0
    Me.ef_from_saturday.IsReadOnly = False
    Me.ef_from_saturday.Location = New System.Drawing.Point(66, 176)
    Me.ef_from_saturday.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.ef_from_saturday.Name = "ef_from_saturday"
    Me.ef_from_saturday.PlaceHolder = Nothing
    Me.ef_from_saturday.ShortcutsEnabled = True
    Me.ef_from_saturday.Size = New System.Drawing.Size(139, 25)
    Me.ef_from_saturday.SufixText = "Sufix Text"
    Me.ef_from_saturday.SufixTextVisible = True
    Me.ef_from_saturday.TabIndex = 66
    Me.ef_from_saturday.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_from_saturday.TextValue = ""
    Me.ef_from_saturday.Value = ""
    Me.ef_from_saturday.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_to_saturday
    '
    Me.ef_to_saturday.DoubleValue = 0.0R
    Me.ef_to_saturday.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_to_saturday.IntegerValue = 0
    Me.ef_to_saturday.IsReadOnly = False
    Me.ef_to_saturday.Location = New System.Drawing.Point(160, 176)
    Me.ef_to_saturday.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.ef_to_saturday.Name = "ef_to_saturday"
    Me.ef_to_saturday.PlaceHolder = Nothing
    Me.ef_to_saturday.ShortcutsEnabled = True
    Me.ef_to_saturday.Size = New System.Drawing.Size(139, 25)
    Me.ef_to_saturday.SufixText = "Sufix Text"
    Me.ef_to_saturday.SufixTextVisible = True
    Me.ef_to_saturday.TabIndex = 67
    Me.ef_to_saturday.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_to_saturday.TextValue = ""
    Me.ef_to_saturday.Value = ""
    Me.ef_to_saturday.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_day_sunday
    '
    Me.chk_day_sunday.AutoSize = True
    Me.chk_day_sunday.Checked = True
    Me.chk_day_sunday.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_day_sunday.Location = New System.Drawing.Point(19, 37)
    Me.chk_day_sunday.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_day_sunday.Name = "chk_day_sunday"
    Me.chk_day_sunday.Size = New System.Drawing.Size(76, 17)
    Me.chk_day_sunday.TabIndex = 47
    Me.chk_day_sunday.Text = "xSunday"
    Me.chk_day_sunday.UseVisualStyleBackColor = True
    '
    'chk_day_wednesday
    '
    Me.chk_day_wednesday.AutoSize = True
    Me.chk_day_wednesday.Checked = True
    Me.chk_day_wednesday.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_day_wednesday.Location = New System.Drawing.Point(19, 109)
    Me.chk_day_wednesday.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_day_wednesday.Name = "chk_day_wednesday"
    Me.chk_day_wednesday.Size = New System.Drawing.Size(99, 17)
    Me.chk_day_wednesday.TabIndex = 56
    Me.chk_day_wednesday.Text = "xWednesday"
    Me.chk_day_wednesday.UseVisualStyleBackColor = True
    '
    'chk_day_friday
    '
    Me.chk_day_friday.AutoSize = True
    Me.chk_day_friday.Checked = True
    Me.chk_day_friday.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_day_friday.Location = New System.Drawing.Point(19, 157)
    Me.chk_day_friday.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_day_friday.Name = "chk_day_friday"
    Me.chk_day_friday.Size = New System.Drawing.Size(68, 17)
    Me.chk_day_friday.TabIndex = 62
    Me.chk_day_friday.Text = "xFriday"
    Me.chk_day_friday.UseVisualStyleBackColor = True
    '
    'chk_day_thuesday
    '
    Me.chk_day_thuesday.AutoSize = True
    Me.chk_day_thuesday.Checked = True
    Me.chk_day_thuesday.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_day_thuesday.Location = New System.Drawing.Point(19, 85)
    Me.chk_day_thuesday.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_day_thuesday.Name = "chk_day_thuesday"
    Me.chk_day_thuesday.Size = New System.Drawing.Size(88, 17)
    Me.chk_day_thuesday.TabIndex = 53
    Me.chk_day_thuesday.Text = "xThuesday"
    Me.chk_day_thuesday.UseVisualStyleBackColor = True
    '
    'chk_day_thursday
    '
    Me.chk_day_thursday.AutoSize = True
    Me.chk_day_thursday.Checked = True
    Me.chk_day_thursday.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_day_thursday.Location = New System.Drawing.Point(19, 133)
    Me.chk_day_thursday.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_day_thursday.Name = "chk_day_thursday"
    Me.chk_day_thursday.Size = New System.Drawing.Size(86, 17)
    Me.chk_day_thursday.TabIndex = 59
    Me.chk_day_thursday.Text = "xThursday"
    Me.chk_day_thursday.UseVisualStyleBackColor = True
    '
    'chk_day_monday
    '
    Me.chk_day_monday.AutoSize = True
    Me.chk_day_monday.Checked = True
    Me.chk_day_monday.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_day_monday.Location = New System.Drawing.Point(19, 61)
    Me.chk_day_monday.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_day_monday.Name = "chk_day_monday"
    Me.chk_day_monday.Size = New System.Drawing.Size(77, 17)
    Me.chk_day_monday.TabIndex = 50
    Me.chk_day_monday.Text = "xMonday"
    Me.chk_day_monday.UseVisualStyleBackColor = True
    '
    'ef_from_sunday
    '
    Me.ef_from_sunday.DoubleValue = 0.0R
    Me.ef_from_sunday.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_from_sunday.IntegerValue = 0
    Me.ef_from_sunday.IsReadOnly = False
    Me.ef_from_sunday.Location = New System.Drawing.Point(66, 32)
    Me.ef_from_sunday.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.ef_from_sunday.Name = "ef_from_sunday"
    Me.ef_from_sunday.PlaceHolder = Nothing
    Me.ef_from_sunday.ShortcutsEnabled = True
    Me.ef_from_sunday.Size = New System.Drawing.Size(139, 25)
    Me.ef_from_sunday.SufixText = "Sufix Text"
    Me.ef_from_sunday.SufixTextVisible = True
    Me.ef_from_sunday.TabIndex = 48
    Me.ef_from_sunday.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_from_sunday.TextValue = ""
    Me.ef_from_sunday.Value = ""
    Me.ef_from_sunday.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_to_sunday
    '
    Me.ef_to_sunday.DoubleValue = 0.0R
    Me.ef_to_sunday.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_to_sunday.IntegerValue = 0
    Me.ef_to_sunday.IsReadOnly = False
    Me.ef_to_sunday.Location = New System.Drawing.Point(160, 32)
    Me.ef_to_sunday.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.ef_to_sunday.Name = "ef_to_sunday"
    Me.ef_to_sunday.PlaceHolder = Nothing
    Me.ef_to_sunday.ShortcutsEnabled = True
    Me.ef_to_sunday.Size = New System.Drawing.Size(139, 25)
    Me.ef_to_sunday.SufixText = "Sufix Text"
    Me.ef_to_sunday.SufixTextVisible = True
    Me.ef_to_sunday.TabIndex = 49
    Me.ef_to_sunday.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_to_sunday.TextValue = ""
    Me.ef_to_sunday.Value = ""
    Me.ef_to_sunday.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_from_monday
    '
    Me.ef_from_monday.DoubleValue = 0.0R
    Me.ef_from_monday.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_from_monday.IntegerValue = 0
    Me.ef_from_monday.IsReadOnly = False
    Me.ef_from_monday.Location = New System.Drawing.Point(66, 56)
    Me.ef_from_monday.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.ef_from_monday.Name = "ef_from_monday"
    Me.ef_from_monday.PlaceHolder = Nothing
    Me.ef_from_monday.ShortcutsEnabled = True
    Me.ef_from_monday.Size = New System.Drawing.Size(139, 25)
    Me.ef_from_monday.SufixText = "Sufix Text"
    Me.ef_from_monday.SufixTextVisible = True
    Me.ef_from_monday.TabIndex = 51
    Me.ef_from_monday.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_from_monday.TextValue = ""
    Me.ef_from_monday.Value = ""
    Me.ef_from_monday.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_to_monday
    '
    Me.ef_to_monday.DoubleValue = 0.0R
    Me.ef_to_monday.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_to_monday.IntegerValue = 0
    Me.ef_to_monday.IsReadOnly = False
    Me.ef_to_monday.Location = New System.Drawing.Point(160, 56)
    Me.ef_to_monday.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.ef_to_monday.Name = "ef_to_monday"
    Me.ef_to_monday.PlaceHolder = Nothing
    Me.ef_to_monday.ShortcutsEnabled = True
    Me.ef_to_monday.Size = New System.Drawing.Size(139, 25)
    Me.ef_to_monday.SufixText = "Sufix Text"
    Me.ef_to_monday.SufixTextVisible = True
    Me.ef_to_monday.TabIndex = 52
    Me.ef_to_monday.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_to_monday.TextValue = ""
    Me.ef_to_monday.Value = ""
    Me.ef_to_monday.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_from_friday
    '
    Me.ef_from_friday.DoubleValue = 0.0R
    Me.ef_from_friday.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_from_friday.IntegerValue = 0
    Me.ef_from_friday.IsReadOnly = False
    Me.ef_from_friday.Location = New System.Drawing.Point(66, 152)
    Me.ef_from_friday.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.ef_from_friday.Name = "ef_from_friday"
    Me.ef_from_friday.PlaceHolder = Nothing
    Me.ef_from_friday.ShortcutsEnabled = True
    Me.ef_from_friday.Size = New System.Drawing.Size(139, 25)
    Me.ef_from_friday.SufixText = "Sufix Text"
    Me.ef_from_friday.SufixTextVisible = True
    Me.ef_from_friday.TabIndex = 63
    Me.ef_from_friday.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_from_friday.TextValue = ""
    Me.ef_from_friday.Value = ""
    Me.ef_from_friday.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_to_friday
    '
    Me.ef_to_friday.DoubleValue = 0.0R
    Me.ef_to_friday.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_to_friday.IntegerValue = 0
    Me.ef_to_friday.IsReadOnly = False
    Me.ef_to_friday.Location = New System.Drawing.Point(160, 152)
    Me.ef_to_friday.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.ef_to_friday.Name = "ef_to_friday"
    Me.ef_to_friday.PlaceHolder = Nothing
    Me.ef_to_friday.ShortcutsEnabled = True
    Me.ef_to_friday.Size = New System.Drawing.Size(139, 25)
    Me.ef_to_friday.SufixText = "Sufix Text"
    Me.ef_to_friday.SufixTextVisible = True
    Me.ef_to_friday.TabIndex = 64
    Me.ef_to_friday.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_to_friday.TextValue = ""
    Me.ef_to_friday.Value = ""
    Me.ef_to_friday.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_from_thursday
    '
    Me.ef_from_thursday.DoubleValue = 0.0R
    Me.ef_from_thursday.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_from_thursday.IntegerValue = 0
    Me.ef_from_thursday.IsReadOnly = False
    Me.ef_from_thursday.Location = New System.Drawing.Point(66, 128)
    Me.ef_from_thursday.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.ef_from_thursday.Name = "ef_from_thursday"
    Me.ef_from_thursday.PlaceHolder = Nothing
    Me.ef_from_thursday.ShortcutsEnabled = True
    Me.ef_from_thursday.Size = New System.Drawing.Size(139, 25)
    Me.ef_from_thursday.SufixText = "Sufix Text"
    Me.ef_from_thursday.SufixTextVisible = True
    Me.ef_from_thursday.TabIndex = 60
    Me.ef_from_thursday.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_from_thursday.TextValue = ""
    Me.ef_from_thursday.Value = ""
    Me.ef_from_thursday.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_to_thursday
    '
    Me.ef_to_thursday.DoubleValue = 0.0R
    Me.ef_to_thursday.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_to_thursday.IntegerValue = 0
    Me.ef_to_thursday.IsReadOnly = False
    Me.ef_to_thursday.Location = New System.Drawing.Point(160, 128)
    Me.ef_to_thursday.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.ef_to_thursday.Name = "ef_to_thursday"
    Me.ef_to_thursday.PlaceHolder = Nothing
    Me.ef_to_thursday.ShortcutsEnabled = True
    Me.ef_to_thursday.Size = New System.Drawing.Size(139, 25)
    Me.ef_to_thursday.SufixText = "Sufix Text"
    Me.ef_to_thursday.SufixTextVisible = True
    Me.ef_to_thursday.TabIndex = 61
    Me.ef_to_thursday.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_to_thursday.TextValue = ""
    Me.ef_to_thursday.Value = ""
    Me.ef_to_thursday.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_from_wednesday
    '
    Me.ef_from_wednesday.DoubleValue = 0.0R
    Me.ef_from_wednesday.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_from_wednesday.IntegerValue = 0
    Me.ef_from_wednesday.IsReadOnly = False
    Me.ef_from_wednesday.Location = New System.Drawing.Point(66, 104)
    Me.ef_from_wednesday.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.ef_from_wednesday.Name = "ef_from_wednesday"
    Me.ef_from_wednesday.PlaceHolder = Nothing
    Me.ef_from_wednesday.ShortcutsEnabled = True
    Me.ef_from_wednesday.Size = New System.Drawing.Size(139, 25)
    Me.ef_from_wednesday.SufixText = "Sufix Text"
    Me.ef_from_wednesday.SufixTextVisible = True
    Me.ef_from_wednesday.TabIndex = 57
    Me.ef_from_wednesday.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_from_wednesday.TextValue = ""
    Me.ef_from_wednesday.Value = ""
    Me.ef_from_wednesday.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_to_wednesday
    '
    Me.ef_to_wednesday.DoubleValue = 0.0R
    Me.ef_to_wednesday.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_to_wednesday.IntegerValue = 0
    Me.ef_to_wednesday.IsReadOnly = False
    Me.ef_to_wednesday.Location = New System.Drawing.Point(160, 104)
    Me.ef_to_wednesday.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.ef_to_wednesday.Name = "ef_to_wednesday"
    Me.ef_to_wednesday.PlaceHolder = Nothing
    Me.ef_to_wednesday.ShortcutsEnabled = True
    Me.ef_to_wednesday.Size = New System.Drawing.Size(139, 25)
    Me.ef_to_wednesday.SufixText = "Sufix Text"
    Me.ef_to_wednesday.SufixTextVisible = True
    Me.ef_to_wednesday.TabIndex = 58
    Me.ef_to_wednesday.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_to_wednesday.TextValue = ""
    Me.ef_to_wednesday.Value = ""
    Me.ef_to_wednesday.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_from_thuesday
    '
    Me.ef_from_thuesday.DoubleValue = 0.0R
    Me.ef_from_thuesday.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_from_thuesday.IntegerValue = 0
    Me.ef_from_thuesday.IsReadOnly = False
    Me.ef_from_thuesday.Location = New System.Drawing.Point(66, 80)
    Me.ef_from_thuesday.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.ef_from_thuesday.Name = "ef_from_thuesday"
    Me.ef_from_thuesday.PlaceHolder = Nothing
    Me.ef_from_thuesday.ShortcutsEnabled = True
    Me.ef_from_thuesday.Size = New System.Drawing.Size(139, 25)
    Me.ef_from_thuesday.SufixText = "Sufix Text"
    Me.ef_from_thuesday.SufixTextVisible = True
    Me.ef_from_thuesday.TabIndex = 54
    Me.ef_from_thuesday.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_from_thuesday.TextValue = ""
    Me.ef_from_thuesday.Value = ""
    Me.ef_from_thuesday.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_to_thuesday
    '
    Me.ef_to_thuesday.DoubleValue = 0.0R
    Me.ef_to_thuesday.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_to_thuesday.IntegerValue = 0
    Me.ef_to_thuesday.IsReadOnly = False
    Me.ef_to_thuesday.Location = New System.Drawing.Point(160, 80)
    Me.ef_to_thuesday.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.ef_to_thuesday.Name = "ef_to_thuesday"
    Me.ef_to_thuesday.PlaceHolder = Nothing
    Me.ef_to_thuesday.ShortcutsEnabled = True
    Me.ef_to_thuesday.Size = New System.Drawing.Size(139, 25)
    Me.ef_to_thuesday.SufixText = "Sufix Text"
    Me.ef_to_thuesday.SufixTextVisible = True
    Me.ef_to_thuesday.TabIndex = 55
    Me.ef_to_thuesday.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_to_thuesday.TextValue = ""
    Me.ef_to_thuesday.Value = ""
    Me.ef_to_thuesday.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_games
    '
    Me.gb_games.Controls.Add(Me.lbl_description_game)
    Me.gb_games.Controls.Add(Me.pnl_games)
    Me.gb_games.Location = New System.Drawing.Point(926, 1)
    Me.gb_games.Name = "gb_games"
    Me.gb_games.Size = New System.Drawing.Size(417, 183)
    Me.gb_games.TabIndex = 68
    Me.gb_games.TabStop = False
    Me.gb_games.Text = "Tipo de Juego"
    '
    'lbl_description_game
    '
    Me.lbl_description_game.ForeColor = System.Drawing.Color.Navy
    Me.lbl_description_game.Location = New System.Drawing.Point(6, 16)
    Me.lbl_description_game.Name = "lbl_description_game"
    Me.lbl_description_game.Size = New System.Drawing.Size(405, 13)
    Me.lbl_description_game.TabIndex = 73
    Me.lbl_description_game.Text = "xAverageBetType"
    Me.lbl_description_game.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'pnl_games
    '
    Me.pnl_games.AutoScroll = True
    Me.pnl_games.Location = New System.Drawing.Point(6, 31)
    Me.pnl_games.Name = "pnl_games"
    Me.pnl_games.Size = New System.Drawing.Size(405, 147)
    Me.pnl_games.TabIndex = 39
    '
    'gb_bet
    '
    Me.gb_bet.Controls.Add(Me.lbl_description_bet)
    Me.gb_bet.Controls.Add(Me.pnl_bet)
    Me.gb_bet.Controls.Add(Me.chk_bet)
    Me.gb_bet.Location = New System.Drawing.Point(929, 186)
    Me.gb_bet.Name = "gb_bet"
    Me.gb_bet.Size = New System.Drawing.Size(202, 99)
    Me.gb_bet.TabIndex = 39
    Me.gb_bet.TabStop = False
    '
    'lbl_description_bet
    '
    Me.lbl_description_bet.ForeColor = System.Drawing.Color.Navy
    Me.lbl_description_bet.Location = New System.Drawing.Point(4, 17)
    Me.lbl_description_bet.Name = "lbl_description_bet"
    Me.lbl_description_bet.Size = New System.Drawing.Size(190, 13)
    Me.lbl_description_bet.TabIndex = 73
    Me.lbl_description_bet.Text = "xAverageBet"
    Me.lbl_description_bet.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'pnl_bet
    '
    Me.pnl_bet.Controls.Add(Me.ef_bet_from)
    Me.pnl_bet.Controls.Add(Me.ef_bet_to)
    Me.pnl_bet.Location = New System.Drawing.Point(6, 32)
    Me.pnl_bet.Name = "pnl_bet"
    Me.pnl_bet.Size = New System.Drawing.Size(190, 59)
    Me.pnl_bet.TabIndex = 18
    '
    'ef_bet_from
    '
    Me.ef_bet_from.DoubleValue = 0.0R
    Me.ef_bet_from.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_bet_from.IntegerValue = 0
    Me.ef_bet_from.IsReadOnly = False
    Me.ef_bet_from.Location = New System.Drawing.Point(3, 2)
    Me.ef_bet_from.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.ef_bet_from.Name = "ef_bet_from"
    Me.ef_bet_from.PlaceHolder = Nothing
    Me.ef_bet_from.ShortcutsEnabled = True
    Me.ef_bet_from.Size = New System.Drawing.Size(179, 25)
    Me.ef_bet_from.SufixText = "Sufix Text"
    Me.ef_bet_from.SufixTextVisible = True
    Me.ef_bet_from.TabIndex = 101
    Me.ef_bet_from.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_bet_from.TextValue = ""
    Me.ef_bet_from.Value = ""
    Me.ef_bet_from.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_bet_to
    '
    Me.ef_bet_to.DoubleValue = 0.0R
    Me.ef_bet_to.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_bet_to.IntegerValue = 0
    Me.ef_bet_to.IsReadOnly = False
    Me.ef_bet_to.Location = New System.Drawing.Point(3, 30)
    Me.ef_bet_to.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.ef_bet_to.Name = "ef_bet_to"
    Me.ef_bet_to.PlaceHolder = Nothing
    Me.ef_bet_to.ShortcutsEnabled = True
    Me.ef_bet_to.Size = New System.Drawing.Size(179, 25)
    Me.ef_bet_to.SufixText = "Sufix Text"
    Me.ef_bet_to.SufixTextVisible = True
    Me.ef_bet_to.TabIndex = 102
    Me.ef_bet_to.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_bet_to.TextValue = ""
    Me.ef_bet_to.Value = ""
    Me.ef_bet_to.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_bet
    '
    Me.chk_bet.AutoSize = True
    Me.chk_bet.Checked = True
    Me.chk_bet.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_bet.Location = New System.Drawing.Point(6, 0)
    Me.chk_bet.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_bet.Name = "chk_bet"
    Me.chk_bet.Size = New System.Drawing.Size(52, 17)
    Me.chk_bet.TabIndex = 100
    Me.chk_bet.Text = "xBet"
    Me.chk_bet.UseVisualStyleBackColor = True
    '
    'ef_birthday_day
    '
    Me.ef_birthday_day.DoubleValue = 0.0R
    Me.ef_birthday_day.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_birthday_day.IntegerValue = 0
    Me.ef_birthday_day.IsReadOnly = False
    Me.ef_birthday_day.Location = New System.Drawing.Point(-4, 0)
    Me.ef_birthday_day.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.ef_birthday_day.Name = "ef_birthday_day"
    Me.ef_birthday_day.PlaceHolder = Nothing
    Me.ef_birthday_day.ShortcutsEnabled = True
    Me.ef_birthday_day.Size = New System.Drawing.Size(139, 25)
    Me.ef_birthday_day.SufixText = "Sufix Text"
    Me.ef_birthday_day.SufixTextVisible = True
    Me.ef_birthday_day.TabIndex = 18
    Me.ef_birthday_day.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_birthday_day.TextValue = ""
    Me.ef_birthday_day.Value = ""
    Me.ef_birthday_day.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_birthday
    '
    Me.gb_birthday.Controls.Add(Me.pnl_birthday)
    Me.gb_birthday.Controls.Add(Me.chk_birthday)
    Me.gb_birthday.Location = New System.Drawing.Point(120, 252)
    Me.gb_birthday.Name = "gb_birthday"
    Me.gb_birthday.Size = New System.Drawing.Size(247, 83)
    Me.gb_birthday.TabIndex = 70
    Me.gb_birthday.TabStop = False
    '
    'pnl_birthday
    '
    Me.pnl_birthday.Controls.Add(Me.ef_birthday_day)
    Me.pnl_birthday.Controls.Add(Me.cmb_birthday)
    Me.pnl_birthday.Location = New System.Drawing.Point(6, 20)
    Me.pnl_birthday.Name = "pnl_birthday"
    Me.pnl_birthday.Size = New System.Drawing.Size(233, 59)
    Me.pnl_birthday.TabIndex = 18
    '
    'chk_birthday
    '
    Me.chk_birthday.AutoSize = True
    Me.chk_birthday.Checked = True
    Me.chk_birthday.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_birthday.Location = New System.Drawing.Point(6, 0)
    Me.chk_birthday.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_birthday.Name = "chk_birthday"
    Me.chk_birthday.Size = New System.Drawing.Size(81, 17)
    Me.chk_birthday.TabIndex = 17
    Me.chk_birthday.Text = "xBirthday"
    Me.chk_birthday.UseVisualStyleBackColor = True
    '
    'frm_customer_dashboard_report_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1448, 827)
    Me.Name = "frm_customer_dashboard_report_sel"
    Me.Text = "frm_customer_dashboard_report_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_gender.ResumeLayout(False)
    Me.gb_level.ResumeLayout(False)
    Me.gb_level.PerformLayout()
    Me.gb_date.ResumeLayout(False)
    Me.gb_visits.ResumeLayout(False)
    Me.gb_visits.PerformLayout()
    Me.pnl_visit.ResumeLayout(False)
    Me.pnl_visit.PerformLayout()
    Me.gb_visit_month.ResumeLayout(False)
    Me.gb_visit_month.PerformLayout()
    Me.gb_age_range.ResumeLayout(False)
    Me.gb_age_range.PerformLayout()
    Me.pnl_age_range.ResumeLayout(False)
    Me.pnl_age_range.PerformLayout()
    Me.gb_visit_week.ResumeLayout(False)
    Me.gb_visit_week.PerformLayout()
    Me.gb_games.ResumeLayout(False)
    Me.gb_bet.ResumeLayout(False)
    Me.gb_bet.PerformLayout()
    Me.pnl_bet.ResumeLayout(False)
    Me.gb_birthday.ResumeLayout(False)
    Me.gb_birthday.PerformLayout()
    Me.pnl_birthday.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_gender As System.Windows.Forms.GroupBox
  Friend WithEvents chk_gender_female As System.Windows.Forms.CheckBox
  Friend WithEvents chk_gender_male As System.Windows.Forms.CheckBox
  Friend WithEvents gb_level As System.Windows.Forms.GroupBox
  Friend WithEvents chk_level_04 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_03 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_02 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_01 As System.Windows.Forms.CheckBox
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to_creation As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from_creation As GUI_Controls.uc_date_picker
  Friend WithEvents uc_account_search As GUI_Controls.uc_account_sel
  Friend WithEvents cmb_birthday As GUI_Controls.uc_combo
  Friend WithEvents gb_visits As System.Windows.Forms.GroupBox
  Friend WithEvents opt_last_visit As System.Windows.Forms.RadioButton
  Friend WithEvents dtp_to_visit As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from_visit As GUI_Controls.uc_date_picker
  Friend WithEvents opt_last_visit_date As System.Windows.Forms.RadioButton
  Friend WithEvents chk_visit As System.Windows.Forms.CheckBox
  Friend WithEvents pnl_visit As System.Windows.Forms.Panel
  Friend WithEvents gb_visit_month As System.Windows.Forms.GroupBox
  Friend WithEvents chk_visit_month_01 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_visit_month_04 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_visit_month_06 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_visit_month_03 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_visit_month_05 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_visit_month_02 As System.Windows.Forms.CheckBox
  Friend WithEvents gb_age_range As System.Windows.Forms.GroupBox
  Friend WithEvents pnl_age_range As System.Windows.Forms.Panel
  Friend WithEvents opt_range As System.Windows.Forms.RadioButton
  Friend WithEvents opt_range_custom As System.Windows.Forms.RadioButton
  Friend WithEvents chk_range_age As System.Windows.Forms.CheckBox
  Friend WithEvents cmb_age_range As GUI_Controls.uc_combo
  Friend WithEvents ef_age_to As GUI_Controls.uc_entry_field
  Friend WithEvents ef_age_from As GUI_Controls.uc_entry_field
  Friend WithEvents chk_gender_unknown As System.Windows.Forms.CheckBox
  Friend WithEvents cmb_visit As GUI_Controls.uc_combo
  Friend WithEvents ef_month_from_01 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_month_to_01 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_month_from_02 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_month_to_02 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_month_from_06 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_month_to_06 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_month_from_05 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_month_to_05 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_month_from_04 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_month_to_04 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_month_from_03 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_month_to_03 As GUI_Controls.uc_entry_field
  Friend WithEvents gb_visit_week As System.Windows.Forms.GroupBox
  Friend WithEvents chk_day_saturday As System.Windows.Forms.CheckBox
  Friend WithEvents ef_from_saturday As GUI_Controls.uc_entry_field
  Friend WithEvents ef_to_saturday As GUI_Controls.uc_entry_field
  Friend WithEvents chk_day_sunday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_day_wednesday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_day_friday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_day_thuesday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_day_thursday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_day_monday As System.Windows.Forms.CheckBox
  Friend WithEvents ef_from_sunday As GUI_Controls.uc_entry_field
  Friend WithEvents ef_to_sunday As GUI_Controls.uc_entry_field
  Friend WithEvents ef_from_monday As GUI_Controls.uc_entry_field
  Friend WithEvents ef_to_monday As GUI_Controls.uc_entry_field
  Friend WithEvents ef_from_friday As GUI_Controls.uc_entry_field
  Friend WithEvents ef_to_friday As GUI_Controls.uc_entry_field
  Friend WithEvents ef_from_thursday As GUI_Controls.uc_entry_field
  Friend WithEvents ef_to_thursday As GUI_Controls.uc_entry_field
  Friend WithEvents ef_from_wednesday As GUI_Controls.uc_entry_field
  Friend WithEvents ef_to_wednesday As GUI_Controls.uc_entry_field
  Friend WithEvents ef_from_thuesday As GUI_Controls.uc_entry_field
  Friend WithEvents ef_to_thuesday As GUI_Controls.uc_entry_field
  Friend WithEvents gb_games As System.Windows.Forms.GroupBox
  Friend WithEvents pnl_games As System.Windows.Forms.Panel
  Friend WithEvents gb_bet As System.Windows.Forms.GroupBox
  Friend WithEvents pnl_bet As System.Windows.Forms.Panel
  Friend WithEvents ef_bet_from As GUI_Controls.uc_entry_field
  Friend WithEvents ef_bet_to As GUI_Controls.uc_entry_field
  Friend WithEvents chk_bet As System.Windows.Forms.CheckBox
  Friend WithEvents ef_birthday_day As GUI_Controls.uc_entry_field
  Friend WithEvents gb_birthday As System.Windows.Forms.GroupBox
  Friend WithEvents pnl_birthday As System.Windows.Forms.Panel
  Friend WithEvents chk_birthday As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_description_month As System.Windows.Forms.Label
  Friend WithEvents lbl_description_week As System.Windows.Forms.Label
  Friend WithEvents lbl_description_game As System.Windows.Forms.Label
  Friend WithEvents lbl_description_bet As System.Windows.Forms.Label
End Class
