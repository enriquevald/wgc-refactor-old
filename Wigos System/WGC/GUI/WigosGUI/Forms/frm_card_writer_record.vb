'-------------------------------------------------------------------
' Copyright � 2008 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_card_writer_record
'
' DESCRIPTION : Allows to record magnetic cards.
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 22-SEP-2008  RRT    Initial version
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

#Region " Imports "

Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.IO
Imports System.Threading

#End Region

Public Class frm_card_writer_record
  Inherits frm_base

#Region " External Prototypes "

#End Region

#Region " Constants "

  Private Const COLOR_CARD_WRITE_OK_BACK As ENUM_GUI_COLOR = ENUM_GUI_COLOR.GUI_COLOR_BLUE_03
  Private Const COLOR_CARD_WRITE_ERROR_BACK As ENUM_GUI_COLOR = ENUM_GUI_COLOR.GUI_COLOR_RED_00

#End Region

#Region " Structures "

#End Region

#Region " Members "

  Private m_session_last_sequence As Int64
  Private m_total_sequence As Int64
  Private m_device_closed As Boolean
  Private m_initialized As Boolean
  Private auditor_data As CLASS_AUDITOR_DATA
  Private WithEvents m_mcw_api As CLASS_MCW_API

  Public m_form As frm_card_writer_record

#End Region

#Region " Delegates "

  Public Delegate Sub DelegateCardWriteResult(ByVal write_result As CLASS_MCW_API.TYPE_MCW_WRITE_RESULT)
  Private Delegate Sub DelegatesNoParams()

#End Region

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CARD_RECORD

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' - Form
    Text = GLB_NLS_GUI_CLASS_II.GetString(350)

    '- Buttons
    btn_stop.Text = GLB_NLS_GUI_CLASS_II.GetString(2)
    btn_exit.Text = GLB_NLS_GUI_CLASS_II.GetString(3)
    btn_exit.Enabled = False

    ' - Labels

    ' Site data
    lbl_record_site.Text = GLB_NLS_GUI_CLASS_II.GetString(351)
    lbl_record_site.Value = Format(m_mcw_api.SiteId, "0000")

    '   - Swipe card
    lbl_swipe_card.Value = GLB_NLS_GUI_CLASS_II.GetString(354)

    '   - Session sequence
    lbl_session_last_recorded_card.Text = GLB_NLS_GUI_CLASS_II.GetString(355)
    lbl_session_last_recorded_card.Value = "0"
    '   - Total sequence
    lbl_total_sequence.Text = GLB_NLS_GUI_CLASS_II.GetString(373)
    lbl_total_sequence.Value = m_total_sequence.ToString()

    lbl_card_write_status.Text = GLB_NLS_GUI_CLASS_II.GetString(356)
    lbl_card_write_status.Visible = False

    If Not m_initialized Then
      Call NLS_MsgBox(GLB_NLS_GUI_CLASS_II.Id(193), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
    End If

  End Sub 'GUI_InitControls

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetInitialFocus()
    btn_stop.Focus()

    If (m_initialized) Then
      m_mcw_api.Thread_EnableWrite()
    End If
  End Sub

  'PURPOSE: Executed just before closing form.
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)

    auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CARD_WRITER)

    ' Check device has been closed
    If Not m_device_closed Then
      Call btn_stop_ClickEvent()
    End If

    ' - Session of cards recorded
    auditor_data.SetName(GLB_NLS_GUI_CLASS_II.Id(360), GLB_NLS_GUI_CLASS_II.GetString(360))
    '   - Site
    auditor_data.SetField(GLB_NLS_GUI_CLASS_II.Id(369), GUI_FormatNumber(m_mcw_api.SiteId, 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE, ), 0)
    '   - Number of cards recorded
    auditor_data.SetField(GLB_NLS_GUI_CLASS_II.Id(361), GUI_FormatNumber(m_session_last_sequence, 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE, ), 0)
    ' Coercivity
    If m_mcw_api.CardCoercivity = CLASS_MCW_API.MCW_CARD_LO_CO Then
      auditor_data.SetField(GLB_NLS_GUI_CLASS_II.Id(362), GLB_NLS_GUI_CLASS_II.GetString(363))
    Else
      auditor_data.SetField(GLB_NLS_GUI_CLASS_II.Id(362), GLB_NLS_GUI_CLASS_II.GetString(364))
    End If

    If Not auditor_data.Notify(GLB_CurrentUser.GuiId, _
                               GLB_CurrentUser.Id, _
                               GLB_CurrentUser.Name, _
                               CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, _
                               0) Then
      ' Logger message 
    End If
  End Sub

#End Region

#Region " Public Functions "

  ' PURPOSE: Form constructor method.
  '
  '  PARAMS:
  '     - INPUT:
  '         - mdiparent
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Sub New(ByVal Mcw_Api_Obj As CLASS_MCW_API)
    Dim bool_rc As Boolean

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    If (Mcw_Api_Obj Is Nothing) Then
      m_initialized = False
    Else
      m_mcw_api = Mcw_Api_Obj
      m_mcw_api.ResetSessionSequence = 0
      m_total_sequence = 0
      m_session_last_sequence = 0

      bool_rc = m_mcw_api.Site_GetLastSequence(m_mcw_api.SiteId, m_total_sequence)
      If bool_rc = False Then
        Call NLS_MsgBox(GLB_NLS_GUI_CLASS_II.Id(183), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

        m_initialized = False
      Else
        m_initialized = True
      End If
    End If

    m_device_closed = False

  End Sub ' New

  ' PURPOSE: Update the result of the card on the form.
  '
  '  PARAMS:
  '     - INPUT:
  '         - CardWriteResult: Result of the card record.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Sub UpdateCardResult(ByVal CardWriteResult As CLASS_MCW_API.TYPE_MCW_WRITE_RESULT)

    ' The Control.Invoke method allows for the synchronous execution of methods on controls.
    Call Me.Invoke(New DelegateCardWriteResult(AddressOf DisplayCardResult), New Object() {CardWriteResult})

  End Sub ' UpdateCardResult

#End Region

#Region " Private Functions "

  ' PURPOSE: Clear write status 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub ClearCardWriteResult()

    ' lbl_card_write_status.Value = ""

  End Sub ' ClearCardWriteResult

  ' PURPOSE: Update the result of the write operation
  '
  '  PARAMS:
  '     - INPUT:
  '         - CardWriteResult: Data related to card record.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub DisplayCardResult(ByVal CardWriteResult As CLASS_MCW_API.TYPE_MCW_WRITE_RESULT)

    lbl_card_write_status.Visible = True
    lbl_card_write_status.Value = ""

    If Not m_device_closed Then
      If CardWriteResult.write_status = CLASS_MCW_API.MCW_STATUS_ERROR Then
        ' 357 - "Error recording card"
        lbl_card_write_status.LabelForeColor = GetColor(COLOR_CARD_WRITE_ERROR_BACK)
        lbl_card_write_status.Value = GLB_NLS_GUI_CLASS_II.GetString(357)
      Else
        ' 358 - "Card recorded sucessfully"
        lbl_card_write_status.LabelForeColor = GetColor(COLOR_CARD_WRITE_OK_BACK)
        lbl_card_write_status.Value = GLB_NLS_GUI_CLASS_II.GetString(358)
        lbl_session_last_recorded_card.Value = CardWriteResult.session_last_sequence.ToString()
        lbl_total_sequence.Value = CardWriteResult.total_sequence.ToString()
      End If
      m_session_last_sequence = CardWriteResult.session_last_sequence
    Else
      ' 359 - "Recording stopped"
      lbl_card_write_status.LabelForeColor = GetColor(COLOR_CARD_WRITE_OK_BACK)
      lbl_card_write_status.Value = GLB_NLS_GUI_CLASS_II.GetString(359)
    End If

  End Sub ' DisplayCardResult

  ' PURPOSE: Button Stop click event handle.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub btn_stop_ClickEvent() Handles btn_stop.ClickEvent

    m_device_closed = True

    ' Call to stop thread.
    m_mcw_api.Thread_StopRequests()

    ' Set exit buttons
    btn_stop.Enabled = False
    btn_exit.Enabled = True

  End Sub ' btn_stop_ClickEvent


  ' PURPOSE: Close form.
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Sub btn_exit_ClickEvent() Handles btn_exit.ClickEvent

    Me.Close()

  End Sub ' btn_exit_ClickEvent


  ' PURPOSE: Receive and event when a card is recorded.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub mcw_api_CardWriteResultEvent(ByVal CardWriteResult As CLASS_MCW_API.TYPE_MCW_WRITE_RESULT) Handles m_mcw_api.EventCardWriteResult

    Try
      If lbl_card_write_status.InvokeRequired Then
        ' The Control.Invoke method allows for the synchronous execution of methods on controls.
        Call Me.Invoke(New DelegateCardWriteResult(AddressOf DisplayCardResult), New Object() {CardWriteResult})
      End If

    Catch ex As Exception
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "CardWriter", _
                            "CardWriteResultEvent", _
                            "", _
                            "", _
                            "", _
                            ex.Message)
    End Try

  End Sub ' mcw_api_CardWriteResultEvent

#End Region

End Class