'-------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_alarm_inquiry_source.vb
' DESCRIPTION:   Inquiry Source Alarm
' AUTHOR:        Jos� Mart�nez L�pez
' CREATION DATE: 07-MAY-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 07-MAY-2014  JML    Initial version. 
' 04-JUN-2014  JBC    Fixed Bug WIG-994: Now form reset don't search
' 30-JUL-2014  JBC    Fixed Bug WIG-1130: Inquiry serach filters by terminal
' 09-OCT-2014  DRV    Fixed Bug WIG-1453: Alarm Filter idiom it's different from the configured language
'-------------------------------------------------------------------
Option Explicit On
Option Strict Off

#Region " Imports "

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports WSI.Common
Imports WSI.Common.Alarm
Imports System.Text.RegularExpressions

#End Region ' Imports

Public Class frm_alarm_inquiry_source
  Inherits frm_base_sel

#Region " Constants "
  '
  Private Const FORM_DB_MIN_VERSION As Short = 209

  'SQL
  Private Const SQL_COLUMN_SITE_ID As Integer = 0
  Private Const SQL_COLUMN_ALARM_ID As Integer = 1
  Private Const SQL_COLUMN_SOURCE_CODE As Integer = 2
  Private Const SQL_COLUMN_SOURCE_ID As Integer = 3
  Private Const SQL_COLUMN_SOURCE_NAME As Integer = 4
  Private Const SQL_COLUMN_ALARM_CODE As Integer = 5
  Private Const SQL_COLUMN_ALARM_NAME As Integer = 6
  Private Const SQL_COLUMN_ALARM_DESCRIPTION As Integer = 7
  Private Const SQL_COLUMN_SEVERITY As Integer = 8
  Private Const SQL_COLUMN_REPORTED As Integer = 9
  Private Const SQL_COLUMN_DATETIME As Integer = 10
  Private Const SQL_COLUMN_ACK_DATETIME As Integer = 11
  Private Const SQL_COLUMN_ACK_USER_ID As Integer = 12
  Private Const SQL_COLUMN_ACK_USER_NAME As Integer = 13
  Private Const SQL_COLUMN_TIMESTAMP As Integer = 14
  Private Const SQL_COLUMN_PATTERN_ALARM_ID As Integer = 15

  'Grid
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_SITE_ID As Integer = 1
  Private Const GRID_COLUMN_ALARM_ID As Integer = 2
  Private Const GRID_COLUMN_TIMESTAMP As Integer = 3
  Private Const GRID_COLUMN_ALARM_GROUP As Integer = 4
  Private Const GRID_COLUMN_ALARM_CATEGORY As Integer = 5
  Private Const GRID_COLUMN_DATETIME As Integer = 6
  Private Const GRID_COLUMN_SEVERITY As Integer = 7
  Private Const GRID_COLUMN_SOURCE_NAME As Integer = 8
  Private Const GRID_COLUMN_ALARM_DESCRIPTION As Integer = 9
  Private Const GRID_COLUMN_ACK_DATETIME As Integer = 10
  Private Const GRID_COLUMN_ACK_USER_NAME As Integer = 11
  Private Const GRID_COLUMN_SOURCE_CODE As Integer = 12

  Private Const GRID_COLUMNS As Integer = 13
  Private Const GRID_HEADER_ROWS As Integer = 2

  'Counters
  Private Const COUNTER_NORMAL As Integer = 1
  Private Const COUNTER_PATTERN As Integer = 2    'ITEMS WHICH PRODUCED THE ALARM
  Private Const COUNTER_CUSTOM As Integer = 3     'CUSTOM ALARMS (MAY BE DETAILED)

  Private Const MAX_COUNTERS As Integer = 3

  Private Const COUNTER_ALL_VISIBLE As Boolean = True

  Private Const COLOR_NORMAL_BACK = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_NORMAL_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_PATTERN_BACK = ENUM_GUI_COLOR.GUI_COLOR_GREEN_01
  Private Const COLOR_PATTERN_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_CUSTOM_BACK = ENUM_GUI_COLOR.GUI_COLOR_GREEN_00
  Private Const COLOR_CUSTOM_FORE = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00

  'Private Const COLOR_ERROR_BACK = ENUM_GUI_COLOR.GUI_COLOR_RED_02
  'Private Const COLOR_ERROR_FORE = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  'Private Const COLOR_WARNING_BACK = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
  'Private Const COLOR_WARNING_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  'Private Const COLOR_INFO_BACK = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  'Private Const COLOR_INFO_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00

#End Region  ' Constants

#Region " Members "

  Private m_alarm_id As Int64
  'Private m_max_alarm_id As Int64
  'Private m_min_alarm_id As Int64
  Private m_lst_alarm_id As String
  Private m_source_id As Int64

  Private m_is_sites_alarms As Boolean = False
  Private m_site_id As Int64

  Private m_alarms_dic As Dictionary(Of Integer, ALARMS_CATALOG)

  Private m_counter_list(MAX_COUNTERS) As Integer

  Private m_checked_all As String
  Private m_alarm_date As String
  Private m_terminal As String
  Private m_pattern_name As String
  Private m_pattern_description As String
  Private m_alarm_name As String
  Private m_alarm_description As String

#End Region  ' Members

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_ALARM_INQUIRY_SOURCE
    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)

  End Sub 'GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()
    Me.Text = GLB_NLS_GUI_ALARMS.GetString(468)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False

    ' Inquiry Source
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Type = uc_button.ENUM_BUTTON_TYPE.USER
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_ALARMS.GetString(464)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Height += 10

    'Filter 
    Me.cb_all_alarms.Text = GLB_NLS_GUI_ALARMS.GetString(476)

    ' Details 
    Me.ef_alarm_id.Text = "Alarm Id" ' no visible
    Me.lbl_date_text.Value = GLB_NLS_GUI_ALARMS.GetString(470)
    Me.lbl_terminal_text.Value = GLB_NLS_GUI_ALARMS.GetString(471)
    Me.lbl_pattern_name_text.Value = GLB_NLS_GUI_ALARMS.GetString(472)
    Me.lbl_pattern_description_text.Value = GLB_NLS_GUI_ALARMS.GetString(473)
    Me.lbl_alarm_name_text.Value = GLB_NLS_GUI_ALARMS.GetString(474)
    Me.lbl_alarm_description_text.Value = GLB_NLS_GUI_ALARMS.GetString(475)

    Me.lbl_alarms_that_belong_to_pattern.Value = GLB_NLS_GUI_ALARMS.GetString(477)
    Me.lbl_alarms_that_belong_to_pattern_color.Text = ""
    Me.lbl_alarms_that_belong_to_pattern_color.BackColor = GetColor(COLOR_PATTERN_BACK)
    Me.lbl_alarm_generate_by_pattern.Value = GLB_NLS_GUI_ALARMS.GetString(480)
    Me.lbl_alarm_generate_by_pattern_color.Text = ""
    Me.lbl_alarm_generate_by_pattern_color.BackColor = GetColor(COLOR_CUSTOM_BACK)

    Call GUI_StyleSheet()

    Call SetDefaultValues()

    Call GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)

  End Sub ' GUI_InitControls

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _str_sql As String

    _str_sql = "SELECT   "
    If m_is_sites_alarms Then
      _str_sql = _str_sql & " XX_SITE_ID, "
    Else
      _str_sql = _str_sql & " 0 AS XX_SITE_ID, "
    End If

    _str_sql = _str_sql & "XX_ALARM_ID " & _
                "        , XX_SOURCE_CODE " & _
                "        , XX_SOURCE_ID " & _
                "        , XX_SOURCE_NAME " & _
                "        , XX_ALARM_CODE " & _
                "        , XX_ALARM_NAME " & _
                "        , XX_ALARM_DESCRIPTION " & _
                "        , XX_SEVERITY " & _
                "        , XX_REPORTED " & _
                "        , XX_DATETIME " & _
                "        , XX_ACK_DATETIME " & _
                "        , XX_ACK_USER_ID " & _
                "        , XX_ACK_USER_NAME " & _
                "        , CAST(XX_TIMESTAMP AS BIGINT) AS XX_TIMESTAMP " & _
                "        , HGP_AL_ID " & _
                "   FROM   TABLE_ALARM " & _
                "   LEFT   JOIN HISTORICAL_GENERATED_PATTERNS ON XX_ALARM_ID = HGP_AL_ID "

    _str_sql = _str_sql & GetSqlWhere()

    _str_sql = _str_sql & " ORDER BY XX_DATETIME DESC, XX_ALARM_ID DESC "

    If m_is_sites_alarms Then
      _str_sql = Replace(_str_sql, "XX_", "SA_")
      _str_sql = Replace(_str_sql, "TABLE_ALARM", "SITE_ALARMS")
    Else
      _str_sql = Replace(_str_sql, "XX_", "AL_")
      _str_sql = Replace(_str_sql, "TABLE_ALARM", "ALARMS")
    End If

    Return _str_sql

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()
    m_counter_list(COUNTER_NORMAL) = 0
    m_counter_list(COUNTER_PATTERN) = 0
    m_counter_list(COUNTER_CUSTOM) = 0
  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()

    If Grid.Counter(COUNTER_NORMAL).Value <> m_counter_list(COUNTER_NORMAL) Then
      Grid.Counter(COUNTER_NORMAL).Value = m_counter_list(COUNTER_NORMAL)
    End If
    If Grid.Counter(COUNTER_PATTERN).Value <> m_counter_list(COUNTER_PATTERN) Then
      Grid.Counter(COUNTER_PATTERN).Value = m_counter_list(COUNTER_PATTERN)
    End If
    If Grid.Counter(COUNTER_CUSTOM).Value <> m_counter_list(COUNTER_CUSTOM) Then
      Grid.Counter(COUNTER_CUSTOM).Value = m_counter_list(COUNTER_CUSTOM)
    End If

  End Sub ' GUI_AfterLastRow

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _terminal As String
    Dim _split() As String
    Dim _split2() As String
    Dim _pattern As String

    ' Site Id
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SITE_ID).Value = DbRow.Value(SQL_COLUMN_SITE_ID)

    ' Alarm Id
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ALARM_ID).Value = DbRow.Value(SQL_COLUMN_ALARM_ID)

    If m_alarms_dic.ContainsKey(DbRow.Value(SQL_COLUMN_ALARM_CODE)) Then
      ' Alarm Group
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ALARM_GROUP).Value = m_alarms_dic.Item(DbRow.Value(SQL_COLUMN_ALARM_CODE)).GroupName

      ' Alarm Category
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ALARM_CATEGORY).Value = m_alarms_dic.Item(DbRow.Value(SQL_COLUMN_ALARM_CODE)).CategoryName
    Else
      ' Alarm Group
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ALARM_GROUP).Value = ""

      ' Alarm Category
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ALARM_CATEGORY).Value = ""
    End If

    ' Timestamp
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TIMESTAMP).Value = DbRow.Value(SQL_COLUMN_TIMESTAMP)

    ' Generated datetime
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATETIME), _
                                                                        ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                        ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Severity
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SEVERITY).Value = DecodeSeverity(DbRow.Value(SQL_COLUMN_SEVERITY))

    ' Source name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SOURCE_CODE).Value = DbRow.Value(SQL_COLUMN_SOURCE_CODE)

    ' Source name
    _terminal = DbRow.Value(SQL_COLUMN_SOURCE_NAME)
    _split = _terminal.Split("@")

    ' Check if it's a Cashier terminal (user@terminal)
    If (DbRow.Value(SQL_COLUMN_SOURCE_CODE) = AlarmSourceCode.Cashier _
       Or DbRow.Value(SQL_COLUMN_SOURCE_CODE) = AlarmSourceCode.User) _
       AndAlso _split.Length = 2 Then

      'Check if it's like " - Autorizado por"
      _pattern = " - "
      _split2 = Regex.Split(_split(1), _pattern)

      If _split2.Length > 1 Then
        _terminal = _split(0) & "@" & Computer.Alias(_split2(0)) & _pattern
        For _idx As Integer = 1 To _split2.Length - 1
          _terminal &= _split2(_idx)
        Next
      Else
        _terminal = _split(0) & "@" & Computer.Alias(_split(1))
      End If
    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_SOURCE_NAME).Value = _terminal

    ' Alarm description
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ALARM_DESCRIPTION).Value = DbRow.Value(SQL_COLUMN_ALARM_DESCRIPTION)

    ' Ack datetime
    If Not DbRow.IsNull(SQL_COLUMN_ACK_DATETIME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACK_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_ACK_DATETIME), _
                                                                              ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                              ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACK_DATETIME).Value = ""
    End If

    ' Ack user name
    If Not DbRow.IsNull(SQL_COLUMN_ACK_USER_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACK_USER_NAME).Value = DbRow.Value(SQL_COLUMN_ACK_USER_NAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACK_USER_NAME).Value = ""
    End If

    Me.Grid.Row(RowIndex).BackColor = GetColor(COLOR_NORMAL_BACK)
    Me.Grid.Row(RowIndex).ForeColor = GetColor(COLOR_NORMAL_FORE)
    m_counter_list(COUNTER_NORMAL) = m_counter_list(COUNTER_NORMAL) + 1

    If m_lst_alarm_id.Contains(DbRow.Value(SQL_COLUMN_ALARM_ID)) Then

      Me.Grid.Row(RowIndex).BackColor = GetColor(COLOR_PATTERN_BACK)
      Me.Grid.Row(RowIndex).ForeColor = GetColor(COLOR_PATTERN_FORE)
      m_counter_list(COUNTER_NORMAL) = m_counter_list(COUNTER_NORMAL) - 1
      m_counter_list(COUNTER_PATTERN) = m_counter_list(COUNTER_PATTERN) + 1

    ElseIf Not DbRow.IsNull(SQL_COLUMN_PATTERN_ALARM_ID) Then

      Me.Grid.Row(RowIndex).BackColor = GetColor(COLOR_CUSTOM_BACK)
      Me.Grid.Row(RowIndex).ForeColor = GetColor(COLOR_CUSTOM_FORE)
      m_counter_list(COUNTER_NORMAL) = m_counter_list(COUNTER_NORMAL) - 1
      m_counter_list(COUNTER_CUSTOM) = m_counter_list(COUNTER_CUSTOM) + 1

    End If

    'Select Case DbRow.Value(SQL_COLUMN_SEVERITY)
    '  Case WSI.Common.AlarmSeverity.Info
    '    Me.Grid.Cell(RowIndex, GRID_COLUMN_SEVERITY).BackColor = GetColor(COLOR_INFO_BACK)
    '    Me.Grid.Cell(RowIndex, GRID_COLUMN_SEVERITY).ForeColor = GetColor(COLOR_INFO_FORE)

    '  Case WSI.Common.AlarmSeverity.Warning
    '    Me.Grid.Cell(RowIndex, GRID_COLUMN_SEVERITY).BackColor = GetColor(COLOR_WARNING_BACK)
    '    Me.Grid.Cell(RowIndex, GRID_COLUMN_SEVERITY).ForeColor = GetColor(COLOR_WARNING_FORE)

    '  Case WSI.Common.AlarmSeverity.Error
    '    Me.Grid.Cell(RowIndex, GRID_COLUMN_SEVERITY).BackColor = GetColor(COLOR_ERROR_BACK)
    '    Me.Grid.Cell(RowIndex, GRID_COLUMN_SEVERITY).ForeColor = GetColor(COLOR_ERROR_FORE)
    'End Select

    Return True
  End Function ' GUI_SetupRow

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.cb_all_alarms
  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Manage buttons pressed.
  '
  '  PARAMS:
  '     - INPUT:
  '         - ButtonId: Id. of the button clicked.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CUSTOM_1
        Call InquirySourcePatterns()
      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Enable button in selected row
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_RowSelectedEvent(ByVal SelectedRow As Integer)

    Dim _grid_value As Int32

    _grid_value = 0

    If IsNumeric(Me.Grid.Cell(SelectedRow, GRID_COLUMN_SOURCE_CODE).Value) Then
      _grid_value = CInt(Me.Grid.Cell(SelectedRow, GRID_COLUMN_SOURCE_CODE).Value)
    End If

    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = (_grid_value = WSI.Common.AlarmSourceCode.Pattern) And CurrentUser.Permissions(ENUM_FORM.FORM_ALARM_INQUIRY_SOURCE).Read

  End Sub  ' GUI_RowSelectedEvent

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(m_checked_all, " ")
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter(GLB_NLS_GUI_ALARMS.GetString(470), m_alarm_date)
    PrintData.SetFilter(GLB_NLS_GUI_ALARMS.GetString(471), m_terminal)
    PrintData.SetFilter(GLB_NLS_GUI_ALARMS.GetString(472), m_pattern_name)
    PrintData.SetFilter(GLB_NLS_GUI_ALARMS.GetString(473), m_pattern_description)
    PrintData.SetFilter(GLB_NLS_GUI_ALARMS.GetString(474), m_alarm_name)
    PrintData.SetFilter(GLB_NLS_GUI_ALARMS.GetString(475), m_alarm_description)

    'PrintData.FilterValueWidth(1) = 3000
    'PrintData.FilterValueWidth(2) = 4000
  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_checked_all = ""
    m_alarm_date = ""
    m_terminal = ""
    m_pattern_name = ""
    m_pattern_description = ""
    m_alarm_name = ""
    m_alarm_description = ""

    If Me.cb_all_alarms.Checked Then
      m_checked_all = GLB_NLS_GUI_ALARMS.GetString(476)
    Else
      m_checked_all = "--"
    End If

    m_alarm_date = GUI_FormatDate(Me.lbl_date.Text, _
                                         ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                         ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    m_terminal = Me.lbl_terminal.Text
    m_pattern_name = Me.lbl_pattern_name.Text
    m_pattern_description = Me.lbl_pattern_description.Text
    m_alarm_name = Me.lbl_alarm_name.Text
    m_alarm_description = Me.lbl_alarm_description.Text

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window, ByVal AlarmId As Int64, ByVal IsSitesAlarms As Boolean, ByVal SiteId As Int64, ByVal SourceId As Int64)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    m_alarm_id = AlarmId
    m_is_sites_alarms = IsSitesAlarms
    m_site_id = SiteId
    m_source_id = SourceId
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Call GetAlarmData()

    Me.cb_all_alarms.Checked = True

    If Not GetAlarmsCatalog(True, 12, m_alarms_dic, Language.GetWigosGUILanguageId()) Then
      Debug.WriteLine("GetAlarmsCatalog - Not all alarm codes retrieve")

      Return
    End If

  End Sub

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub GUI_StyleSheet()
    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      .Counter(COUNTER_NORMAL).Visible = COUNTER_ALL_VISIBLE
      .Counter(COUNTER_NORMAL).BackColor = GetColor(COLOR_NORMAL_BACK)
      .Counter(COUNTER_NORMAL).ForeColor = GetColor(COLOR_NORMAL_FORE)

      .Counter(COUNTER_CUSTOM).Visible = COUNTER_ALL_VISIBLE
      .Counter(COUNTER_CUSTOM).BackColor = GetColor(COLOR_CUSTOM_BACK)
      .Counter(COUNTER_CUSTOM).ForeColor = GetColor(COLOR_CUSTOM_FORE)

      .Counter(COUNTER_PATTERN).Visible = COUNTER_ALL_VISIBLE
      .Counter(COUNTER_PATTERN).BackColor = GetColor(COLOR_PATTERN_BACK)
      .Counter(COUNTER_PATTERN).ForeColor = GetColor(COLOR_PATTERN_FORE)

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Header(1).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Alarm Id
      .Column(GRID_COLUMN_ALARM_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_ALARM_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_ALARM_ID).Width = 0

      ' Source Code
      .Column(GRID_COLUMN_SOURCE_CODE).Header(0).Text = ""
      .Column(GRID_COLUMN_SOURCE_CODE).Header(1).Text = ""
      .Column(GRID_COLUMN_SOURCE_CODE).Width = 0

      ' Timestamp
      .Column(GRID_COLUMN_TIMESTAMP).Header(0).Text = ""
      .Column(GRID_COLUMN_TIMESTAMP).Header(1).Text = ""
      .Column(GRID_COLUMN_TIMESTAMP).Width = 0

      ' Alarm Id
      If m_is_sites_alarms Then
        .Column(GRID_COLUMN_SITE_ID).Header(0).Text = ""
        .Column(GRID_COLUMN_SITE_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1927) ' SiteId
        .Column(GRID_COLUMN_SITE_ID).Width = 850
        .Column(GRID_COLUMN_SITE_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      Else
        .Column(GRID_COLUMN_SITE_ID).Header(0).Text = ""
        .Column(GRID_COLUMN_SITE_ID).Header(1).Text = ""
        .Column(GRID_COLUMN_SITE_ID).Width = 0
      End If

      ' Alarm group
      .Column(GRID_COLUMN_ALARM_GROUP).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(207)
      .Column(GRID_COLUMN_ALARM_GROUP).Header(1).Text = GLB_NLS_GUI_ALARMS.GetString(462)
      .Column(GRID_COLUMN_ALARM_GROUP).Width = 1500
      .Column(GRID_COLUMN_ALARM_GROUP).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Alarm category
      .Column(GRID_COLUMN_ALARM_CATEGORY).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(207)
      .Column(GRID_COLUMN_ALARM_CATEGORY).Header(1).Text = GLB_NLS_GUI_ALARMS.GetString(463)
      .Column(GRID_COLUMN_ALARM_CATEGORY).Width = 2000
      .Column(GRID_COLUMN_ALARM_CATEGORY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Date Generated
      .Column(GRID_COLUMN_DATETIME).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(207)
      .Column(GRID_COLUMN_DATETIME).Header(1).Text = GLB_NLS_GUI_ALARMS.GetString(443)
      .Column(GRID_COLUMN_DATETIME).Width = 2100
      .Column(GRID_COLUMN_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Severity
      .Column(GRID_COLUMN_SEVERITY).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(207)
      .Column(GRID_COLUMN_SEVERITY).Header(1).Text = GLB_NLS_GUI_ALARMS.GetString(416)
      .Column(GRID_COLUMN_SEVERITY).Width = 1100
      .Column(GRID_COLUMN_SEVERITY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Source
      .Column(GRID_COLUMN_SOURCE_NAME).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(207)
      .Column(GRID_COLUMN_SOURCE_NAME).Header(1).Text = GLB_NLS_GUI_ALARMS.GetString(209)
      .Column(GRID_COLUMN_SOURCE_NAME).Width = 4000
      .Column(GRID_COLUMN_SOURCE_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Description
      .Column(GRID_COLUMN_ALARM_DESCRIPTION).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(207)
      .Column(GRID_COLUMN_ALARM_DESCRIPTION).Header(1).Text = GLB_NLS_GUI_ALARMS.GetString(269)
      .Column(GRID_COLUMN_ALARM_DESCRIPTION).Width = 5200
      .Column(GRID_COLUMN_ALARM_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Ack Datetime
      .Column(GRID_COLUMN_ACK_DATETIME).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(208)
      .Column(GRID_COLUMN_ACK_DATETIME).Header(1).Text = GLB_NLS_GUI_ALARMS.GetString(443)
      .Column(GRID_COLUMN_ACK_DATETIME).Width = 2100
      .Column(GRID_COLUMN_ACK_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Ack Username
      .Column(GRID_COLUMN_ACK_USER_NAME).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(208)
      .Column(GRID_COLUMN_ACK_USER_NAME).Header(1).Text = GLB_NLS_GUI_ALARMS.GetString(267)
      .Column(GRID_COLUMN_ACK_USER_NAME).Width = 2000
      .Column(GRID_COLUMN_ACK_USER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the
  '          main SQL Query.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Function GetSqlWhere() As String

    Dim _str_where As String = ""

    'Sites
    If m_is_sites_alarms Then
      _str_where = _str_where & " AND XX_SITE_ID = " & m_site_id & " "
    End If

    _str_where = _str_where & " AND   AL_SOURCE_ID  = (    " & _
                                "  SELECT   AL_SOURCE_ID   " & _
                                "    FROM   ALARMS         " & _
                                "   WHERE   AL_ALARM_ID=   " & m_alarm_id & ")"

    'Alarm Ids
    If cb_all_alarms.Checked Then
      _str_where = _str_where & " AND XX_DATETIME >= (SELECT MIN(XX_DATETIME) FROM TABLE_ALARM WHERE XX_ALARM_ID IN (" & m_lst_alarm_id & ")) " & _
                                " AND XX_DATETIME <= (SELECT MAX(XX_DATETIME) FROM TABLE_ALARM WHERE XX_ALARM_ID IN (" & m_lst_alarm_id & ")) "
    Else
      _str_where = _str_where & " AND XX_ALARM_ID IN ( " & m_lst_alarm_id & " ) "
    End If

    ' Change first "AND" by "WHERE"
    If _str_where <> "" Then
      _str_where = Strings.Right(_str_where, Len(_str_where) - 5)
      _str_where = " WHERE " & _str_where
    End If

    Return _str_where

  End Function ' GetSqlWhere

  ' PURPOSE : Fill the combo box of levels
  '
  '  PARAMS:
  '     - INPUT:
  '         - Combo: control to be filled
  '     - OUTPUT:
  '
  ' RETURNS :
  Private Sub GetAlarmData()
    Dim _sb As New System.Text.StringBuilder()
    Dim _dt As DataTable
    Dim _ds As DataSet

    m_lst_alarm_id = ""

    _sb.AppendLine("SELECT   HGP_AL_ID  ")
    _sb.AppendLine("       , PT_NAME ")
    _sb.AppendLine("       , PT_DESCRIPTION ")
    _sb.AppendLine("       , HGP_PATTERN_VALUES ")
    _sb.AppendLine("       , HGP_DATE ")
    _sb.AppendLine("       , TE_NAME ")
    _sb.AppendLine("       , HGP_ALARM_NAME ")
    _sb.AppendLine("       , HGP_ALARM_DESCRIPTION ")
    _sb.AppendLine("  FROM   HISTORICAL_GENERATED_PATTERNS ")
    _sb.AppendLine("  LEFT   JOIN PATTERNS ON HGP_PATTERN_ID = PT_ID ")
    _sb.AppendLine("  LEFT   JOIN TERMINALS ON HGP_ELEMENT_ID = TE_TERMINAL_ID ")
    _sb.AppendLine(" WHERE   HGP_AL_ID = @pAlarmId ")

    Try
      Using _db_trx As New DB_TRX()
        Using _cmd As New SqlCommand(_sb.ToString())
          _cmd.Parameters.Add("@pAlarmId", SqlDbType.BigInt).Value = m_alarm_id

          Using _reader As SqlClient.SqlDataReader = _db_trx.ExecuteReader(_cmd)
            If (_reader.Read()) Then
              ef_alarm_id.Value = IIf(IsDBNull(_reader(0)), "", _reader(0).ToString())
              lbl_pattern_name.Text = IIf(IsDBNull(_reader(1)), "", _reader(1).ToString())
              lbl_pattern_description.Text = IIf(IsDBNull(_reader(2)), "", _reader(2).ToString())
              _ds = New DataSet()
              WSI.Common.Misc.DataSetFromXml(_ds, _reader(3), XmlReadMode.Auto)
              _dt = _ds.Tables(0)
              'm_max_alarm_id = _dt.Compute("max(id)", "")
              'm_min_alarm_id = _dt.Compute("min(id)", "")
              For Each row As DataRow In _dt.Rows
                If m_lst_alarm_id.Length > 0 Then
                  m_lst_alarm_id += ", "
                End If
                m_lst_alarm_id += row.Item(0).ToString()
              Next
              lbl_date.Text = IIf(IsDBNull(_reader(4)), "", GUI_FormatDate(_reader(4), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS))
              lbl_terminal.Text = IIf(IsDBNull(_reader(5)), "", _reader(5))
              lbl_alarm_name.Text = IIf(IsDBNull(_reader(6)), "", _reader(6))
              lbl_alarm_description.Text = IIf(IsDBNull(_reader(7)), "", _reader(7))
            End If
          End Using

        End Using
      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

  End Sub ' GetAlarmData

  ' PURPOSE: Return the String representation of an AlarmSeverity.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Id
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Private Function DecodeSeverity(ByVal Id As WSI.Common.AlarmSeverity) As String

    Dim result As String

    result = ""
    Select Case Id
      Case WSI.Common.AlarmSeverity.Info
        result = GLB_NLS_GUI_ALARMS.GetString(206)
      Case WSI.Common.AlarmSeverity.Warning
        result = GLB_NLS_GUI_ALARMS.GetString(358)
      Case WSI.Common.AlarmSeverity.Error
        result = GLB_NLS_GUI_ALARMS.GetString(357)
    End Select

    Return result
  End Function ' DecodeSeverity

  ' PURPOSE: Show new window with the information of the alarms that generated it.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Severity
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub InquirySourcePatterns()
    Dim _idx_row As Short
    Dim _alarm_id As Integer
    Dim _site_id As Int64

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    If _idx_row = Me.Grid.NumRows Then
      Return
    End If

    If Not (Me.Grid.Cell(_idx_row, GRID_COLUMN_SOURCE_CODE).Value = AlarmSourceCode.Pattern) Then
      Call NLS_MsgBox(GLB_NLS_GUI_ALARMS.Id(467), ENUM_MB_TYPE.MB_TYPE_INFO)

      Return
    End If

    If m_is_sites_alarms Then
      _site_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_SITE_ID).Value
    End If
    _alarm_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_ALARM_ID).Value
    m_alarm_id = _alarm_id
    m_site_id = _site_id

    Call SetDefaultValues()

    Call GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)

    Call Me.Grid.Focus()

  End Sub ' InquirySourcePatterns

#End Region ' Private Functions

End Class