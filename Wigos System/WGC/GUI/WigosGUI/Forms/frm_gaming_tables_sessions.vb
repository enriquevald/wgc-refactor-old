'-------------------------------------------------------------------
' Copyright � 2007-2009 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_gaming_tables_sessions
' DESCRIPTION:   This screen allows to view gaming tables sessions:
' AUTHOR:        Sergi Mart�nez
' CREATION DATE: 16-DEC-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 16-DEC-2013  SMN    Initial version.
' 03-JAN-2014  JBP    Refactoriced. 
' 28-JAN-2014  DLL    Added button to show cashier movements from selected gaming table session
' 30-JAN-2014  SMN    Added a new column in Grid
' 31-JAN-2014  RMS    Changed query type to execute a stored procedure GT_Session_information
' 03-FEB-2014  DLL    Fixed bug WIG-579
' 07-OCT-2014  DHA    Fixed Bug WIG-1424: set filters properly
' 25-FEB-2015  FOS    TASK 318. Add Totals Row in grid
' 27-APR-2016  RAB    Product Backlog Item 9758: Tables (Phase 1): Review reports table
' 22-JUL-2016  RAB    Product Backlog Item 15066: GamingTables (Phase 4): Adapt reports to MultiCurrency
' 12-SEP-2016  JML    Bug 17344: Gaming tables: Number of visits is not reported correctly when close the table.
' 21-DIC-2016  FOS    Bug 21895: Move Drop Column position
' 23-DIC-2016  FOS    Bug 21976: Show drop information into tips column
' 30-DIC-2016  FOS    Bug 21976: fix bug when print excel
' 10-JAN-2016  FOS    Bug 22460: Show total tips imports correctly
' 10-JAN-2016  DHA    Bug 22491: hide columns with 0 values
' 02-FEB-2017  JML    Fixed Bug 23999:Failure to report gaming table sessions
' 05-JUN-2018  AGS		Bug 32889:WIGOS-12142, WIGOS-12143, WIGOS-12146, WIGOS-12148 - Drop mesas
' 21-JUN-2018  AGS    Bug 33231:WIGOS-13133 Some columns are hidden in the "Sesiones de mesas de juego" report
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports WSI.Common
Imports System.Text
Imports System.Data.SqlClient


Public Class frm_gaming_tables_sessions
  Inherits frm_base_sel

#Region " Constants "

  ' Grid Setup
  Private Const GRID_HEADER_ROWS As Integer = 2
  Private Const GRID_COLUMNS As Integer = 27

  ' Grid Colums
  Private Const GRID_COLUMN_SESSION_ID As Integer = 0
  Private Const GRID_COLUMN_SESSION_NAME As Integer = 1
  Private Const GRID_COLUMN_SENTINEL As Integer = 2
  Private Const GRID_COLUMN_STATUS As Integer = 3
  Private Const GRID_COLUMN_TABLE_NAME As Integer = 4
  Private Const GRID_COLUMN_TABLE_TYPE As Integer = 5
  Private Const GRID_COLUMN_TABLE_CASHIER As Integer = 6
  Private Const GRID_COLUMN_CASHIER_USER As Integer = 7
  Private Const GRID_COLUMN_OPENING_DATE As Integer = 8
  Private Const GRID_COLUMN_CLOSING_DATE As Integer = 9
  Private Const GRID_COLUMN_CHIPS_PURCHASES As Integer = 10
  Private Const GRID_COLUMN_CHIPS_SALES As Integer = 11
  Private Const GRID_COLUMN_CHIPS_INITIAL_BALANCE As Integer = 12
  Private Const GRID_COLUMN_CHIPS_FILLS_BALANCE As Integer = 13
  Private Const GRID_COLUMN_CHIPS_CREDITS_BALANCE As Integer = 14
  Private Const GRID_COLUMN_CHIPS_FINAL_BALANCE As Integer = 15
  Private Const GRID_COLUMN_DROP As Integer = 16
  Private Const GRID_COLUMN_COPY_DEALER_VALIDATED_AMOUNT As Integer = 17
  Private Const GRID_COLUMN_TIPS As Integer = 18
  Private Const GRID_COLUMN_CHIPS_BALANCE As Integer = 19
  Private Const GRID_COLUMN_CASH_BALANCE As Integer = 20
  Private Const GRID_COLUMN_TOTAL_BALANCE As Integer = 21
  Private Const GRID_COLUMN_VISITS As Integer = 22
  Private Const GRID_COLUMN_AREA As Integer = 23
  Private Const GRID_COLUMN_BANK As Integer = 24
  Private Const GRID_COLUMN_GAMING_TABLE_SESSION_ID As Integer = 25
  Private Const GRID_COLUMN_TOTAL_LOCAL_DROP As Integer = 26

  ' Grid Columns Width
  Private Const GRID_COLUMN_SENTINEL_WIDTH As Integer = 200
  Private Const GRID_COLUMN_STATUS_WIDTH As Integer = 350
  Private Const GRID_COLUMN_TABLE_NAME_WIDTH As Integer = 2500
  Private Const GRID_COLUMN_TABLE_TYPE_WIDTH As Integer = 2000
  Private Const GRID_COLUMN_TABLE_CASHIER_WIDTH As Integer = 2000
  Private Const GRID_COLUMN_CASHIER_USER_WIDTH As Integer = 2000
  Private Const GRID_COLUMN_OPENING_DATE_WIDTH As Integer = 2200
  Private Const GRID_COLUMN_CLOSING_DATE_WIDTH As Integer = 2200
  Private Const GRID_COLUMN_CHIPS_PURCHASES_WIDTH As Integer = 1600
  Private Const GRID_COLUMN_CHIPS_SALES_WIDTH As Integer = 1600
  Private Const GRID_COLUMN_CHIPS_INITIAL_BALANCE_WIDTH As Integer = 1600
  Private Const GRID_COLUMN_CHIPS_FILLS_BALANCE_WIDTH As Integer = 1600
  Private Const GRID_COLUMN_CHIPS_CREDITS_BALANCE_WIDTH As Integer = 1600
  Private Const GRID_COLUMN_CHIPS_FINAL_BALANCE_WIDTH As Integer = 1600
  Private Const GRID_COLUMN_TIPS_WIDTH As Integer = 1600
  Private Const GRID_COLUMN_CHIPS_BALANCE_WIDTH As Integer = 1900
  Private Const GRID_COLUMN_CASH_BALANCE_WIDTH As Integer = 1900
  Private Const GRID_COLUMN_TOTAL_BALANCE_WIDTH As Integer = 1900
  Private Const GRID_COLUMN_VISITS_WIDTH As Integer = 1100
  Private Const GRID_COLUMN_AREA_WIDTH As Integer = 1500
  Private Const GRID_COLUMN_BANK_WIDTH As Integer = 1500
  Private Const GRID_COLUMN_DROP_WIDTH As Integer = 1600
  Private Const GRID_COLUMN_COPY_DEALER_VALIDATED_AMOUNT_WIDTH As Integer = 2200
  Private Const GRID_COLUMN_TOTAL_DROP_WIDTH As Integer = 0

  ' SQL Columns
  Private Const SQL_COLUMN_SESSION_ID As Integer = 0
  Private Const SQL_COLUMN_SESSION_NAME As Integer = 1
  Private Const SQL_COLUMN_STATUS As Integer = 2
  Private Const SQL_COLUMN_TABLE_NAME As Integer = 3
  Private Const SQL_COLUMN_TABLE_TYPE As Integer = 4
  Private Const SQL_COLUMN_TABLE_CASHIER As Integer = 5
  Private Const SQL_COLUMN_TABLE_CASHIER_NAME As Integer = 6
  Private Const SQL_COLUMN_CASHIER_USER As Integer = 7
  Private Const SQL_COLUMN_OPENING_DATE As Integer = 8
  Private Const SQL_COLUMN_CLOSING_DATE As Integer = 9
  Private Const SQL_COLUMN_CHIPS_PURCHASES As Integer = 10
  Private Const SQL_COLUMN_CHIPS_SALES As Integer = 11
  Private Const SQL_COLUMN_CHIPS_INITIAL_BALANCE As Integer = 12
  Private Const SQL_COLUMN_CHIPS_FILLS_BALANCE As Integer = 13
  Private Const SQL_COLUMN_CHIPS_CREDITS_BALANCE As Integer = 14
  Private Const SQL_COLUMN_CHIPS_FINAL_BALANCE As Integer = 15
  Private Const SQL_COLUMN_TIPS As Integer = 16
  Private Const SQL_COLUMN_COLLECTED As Integer = 17
  Private Const SQL_COLUMN_VISITS As Integer = 18
  Private Const SQL_COLUMN_AREA_NAME As Integer = 19
  Private Const SQL_COLUMN_BANK As Integer = 20
  Private Const SQL_COLUMN_GAMING_TABLE_SESSION_ID As Integer = 21
  Private Const SQL_COLUMN_DROP As Integer = 22
  Private Const SQL_COLUMN_ISO_CODE As Integer = 23
  Private Const SQL_COLUMN_TOTAL_LOCAL_DROP As Integer = 24
  Private Const SQL_COLUMN_COPY_DEALER_VALIDATED_AMOUNT As Integer = 25

  ' Table types data grid
  Private Const GRID_2_COLUMN_CODE As Integer = 0
  Private Const GRID_2_COLUMN_CHECKED As Integer = 1
  Private Const GRID_2_COLUMN_DESC As Integer = 2
  Private Const GRID_2_COLUMN_ROW_TYPE As Integer = 3
  Private Const GRID_2_COLUMNS As Integer = 4

  ' Dinamic report constant
  Private Const COLUMNS_TO_CHIPS_GROUP As Integer = 6
  Private Const COLUMNS_TO_TIPS_GROUP As Integer = 1
  Private Const COLUMNS_TO_DROP_GROUP As Integer = 2
  Private Const COLUMNS_TO_BALANCE_CHIPS As Integer = 3
#End Region ' Constants

#Region " Members "

  ' Event
  Private m_several_click As Boolean

  Private m_report_enabled As String
  Private m_report_status As String
  Private m_report_date_type As String
  Private m_report_from As Date
  Private m_report_from_text As String
  Private m_report_to As Date
  Private m_report_to_text As String
  Private m_report_area_name As String
  Private m_report_zone_name As String
  Private m_report_bank_name As String
  Private m_report_types As String
  Private m_is_from_movements As Boolean

  'FOS  25-FEB-2015
  'RAB  27-JUL-2016
  Private m_chips_purchase() As Decimal
  Private m_chips_sales() As Decimal
  Private m_initial_chips_balance() As Decimal
  Private m_fills_chips_balance() As Decimal
  Private m_credits_chips_balance() As Decimal
  Private m_final_chips_balance() As Decimal
  Private m_chips_tips() As Decimal
  Private m_chips_balance() As Decimal
  Private m_currency_balance() As Decimal
  Private m_total_balance() As Decimal
  Private m_drop_amount() As Decimal
  Private m_copy_dealer_validated_amount() As Decimal
  Private m_visits As Integer

  Private m_currencies_accepted As String()
  Private m_current_cashier_session_id As Long
  Private m_row_index As Integer

#End Region

#Region "Enums"

  Public Structure TYPE_DG_FILTER_ELEMENT
    Dim code As Integer
    Dim description As String
    Dim elem_type As Integer
  End Structure

#End Region

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_GAMING_TABLES_SESSIONS_SEL

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3431)                                               ' 3431 "Sesiones de mesas de juego"

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_INFO).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(1)     ' 1    "Salir"

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Text = GLB_NLS_GUI_INVOICING.GetString(2)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Enabled = False

    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = GamingTableBusinessLogic.IsEnableManualConciliation
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6525)       ' Conciliate

    ' -- Date --
    Me.gb_date.Text = GLB_NLS_GUI_INVOICING.GetString(201)                                              ' 201  "Date"
    Me.opt_opening_session.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2101)                           ' 2101 "Buscar por apertura de sesi�n"
    Me.opt_movement_session.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2102)                          ' 2102 "Buscar por movimientos de caja"
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)                                             ' 202  "From"
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)                                               ' 203  "To"
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' -- Table Types --
    Me.gb_table_type.Text = GLB_NLS_GUI_INVOICING.GetString(211)
    Me.opt_several_types.Text = GLB_NLS_GUI_INVOICING.GetString(223)                                    ' 272 "Todos"
    Me.opt_all_types.Text = GLB_NLS_GUI_INVOICING.GetString(224)                                        ' 273 "Varios"

    Call GUI_StyleSheetTableTypes()

    ' -- Enabled -- 
    Me.gb_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3422)                                    ' 3422 "Habilitada"
    Me.chk_yes.Text = GLB_NLS_GUI_INVOICING.GetString(479)                                              ' 479  "Yes"
    Me.chk_no.Text = GLB_NLS_GUI_INVOICING.GetString(480)                                               ' 480  "No"

    ' -- State --
    fra_session_state.Text = GLB_NLS_GUI_INVOICING.GetString(152)                                       ' 152  "Estado  (E)"
    chk_session_open.Text = GLB_NLS_GUI_INVOICING.GetString(153)                                        ' 153  "Abierto (O)"
    chk_session_closed.Text = GLB_NLS_GUI_INVOICING.GetString(154)                                      ' 154  "Cerrado (C)"

    ' -- Location --
    '   � Area
    Me.gb_area_island.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1166)                                ' 1166 "Ubicaci�n"
    Me.cmb_area.Text = GLB_NLS_GUI_CONFIGURATION.GetString(435)                                         ' 435  "Area"
    '   � Zone
    Me.ef_smoking.Text = GLB_NLS_GUI_CONFIGURATION.GetString(430)                                       ' 430  "Zona"
    Me.ef_smoking.IsReadOnly = True
    Me.ef_smoking.TabStop = False
    '   � Bank
    Me.cmb_bank.Text = GLB_NLS_GUI_CONFIGURATION.GetString(431)                                         ' 431  "Isla"

    ' Fill combos
    Call AreaComboFill()
    Call BankComboFill()

    ' Add Handles
    Call AddHandles()

    ' Set currencies accepted to wigos gui system
    m_currencies_accepted = GeneralParam.GetString("RegionalOptions", "CurrenciesAccepted").Split(";")
    m_current_cashier_session_id = 0

    ' Grid
    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ''' <summary>
  ''' Reset dinamic total arrays
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub InitTotalArrays()
    m_chips_purchase = Nothing
    m_chips_sales = Nothing
    m_initial_chips_balance = Nothing
    m_fills_chips_balance = Nothing
    m_credits_chips_balance = Nothing
    m_final_chips_balance = Nothing

    m_drop_amount = Nothing
    m_copy_dealer_validated_amount = Nothing
    m_chips_tips = Nothing

    m_chips_balance = Nothing
    m_currency_balance = Nothing
    m_total_balance = Nothing

    m_visits = 0

    m_chips_purchase = New Decimal(m_currencies_accepted.Length - 1) {}
    m_chips_sales = New Decimal(m_currencies_accepted.Length - 1) {}
    m_initial_chips_balance = New Decimal(m_currencies_accepted.Length - 1) {}
    m_fills_chips_balance = New Decimal(m_currencies_accepted.Length - 1) {}
    m_credits_chips_balance = New Decimal(m_currencies_accepted.Length - 1) {}
    m_final_chips_balance = New Decimal(m_currencies_accepted.Length - 1) {}

    m_drop_amount = New Decimal(m_currencies_accepted.Length - 1) {}
    m_copy_dealer_validated_amount = New Decimal(m_currencies_accepted.Length - 1) {}
    m_chips_tips = New Decimal(m_currencies_accepted.Length - 1) {}

    m_chips_balance = New Decimal(m_currencies_accepted.Length - 1) {}
    m_currency_balance = New Decimal(m_currencies_accepted.Length - 1) {}
    m_total_balance = New Decimal(m_currencies_accepted.Length - 1) {}
  End Sub

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)
    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        Call GUI_ShowSelectedItem()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0
        Call GUI_ShowConciliationDrop()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub ' GUI_ButtonClick

  ' PURPOSE: Call to child window to show details for the selected row
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ShowSelectedItem()

    Dim idx_row As Integer
    Dim session_id As Integer
    Dim session_name As String
    Dim session_date As String
    Dim frm As frm_cashier_movements

    ' Check that it can show the information
    If Not GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Enabled Then

      Return
    End If

    ' Search the first row selected
    For idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(idx_row).IsSelected Then
        Exit For
      End If
    Next

    If Me.Grid.Cell(idx_row, GRID_COLUMN_SESSION_ID).Value = "" Then
      session_id = 0
      session_name = ""
    Else
      session_id = Me.Grid.Cell(idx_row, GRID_COLUMN_SESSION_ID).Value
      session_name = Me.Grid.Cell(idx_row, GRID_COLUMN_SESSION_NAME).Value
    End If

    If Me.Grid.Cell(idx_row, GRID_COLUMN_OPENING_DATE).Value = "" Then
      session_date = ""
    Else
      session_date = Me.Grid.Cell(idx_row, GRID_COLUMN_OPENING_DATE).Value.ToString()
    End If

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    frm = New frm_cashier_movements
    If m_is_from_movements Then
      frm.ShowForEdit(Me.MdiParent, session_id, session_name, m_report_from, m_report_to)
    Else
      frm.ShowForEdit(Me.MdiParent, session_id, session_name)
    End If

  End Sub ' GUI_ShowSelectedItem

  ' PURPOSE: Call to child window to show details for the selected row
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_ShowConciliationDrop()

    Dim _idx_row As Integer
    Dim _session_id As Integer
    Dim _table_name As String
    Dim _session_date_open As String
    Dim _session_date_close As String
    Dim _drop As Decimal
    Dim _columns_sub_to_session_id As Integer
    Dim _columns_sub_to_drop As Integer

    _columns_sub_to_session_id = 2
    _columns_sub_to_drop = 1

    Dim frm As frm_drop_conciliation_edit

    ' Check that it can show the information
    If Not GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled Then

      Return
    End If

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    If Me.Grid.Cell(_idx_row, NumTotalColumns() - _columns_sub_to_session_id).Value = "" Then

      Return
    End If

    _session_id = Me.Grid.Cell(_idx_row, NumTotalColumns() - _columns_sub_to_session_id).Value
    _table_name = Me.Grid.Cell(_idx_row, GRID_COLUMN_TABLE_NAME).Value

    If Me.Grid.Cell(_idx_row, GRID_COLUMN_OPENING_DATE).Value = "" Then
      _session_date_open = ""
    Else
      _session_date_open = Me.Grid.Cell(_idx_row, GRID_COLUMN_OPENING_DATE).Value.ToString()
    End If

    If Me.Grid.Cell(_idx_row, GRID_COLUMN_CLOSING_DATE).Value = "" Then
      _session_date_close = ""
    Else
      _session_date_close = Me.Grid.Cell(_idx_row, GRID_COLUMN_CLOSING_DATE).Value.ToString()
    End If

    'If Me.Grid.Cell(_idx_row, NumTotalColumns() - _columns_sub_to_drop).Value = "" Then
    '  _drop = 0
    'Else
    '  _drop = CDec(Me.Grid.Cell(_idx_row, NumTotalColumns() - _columns_sub_to_drop).Value.ToString())
    'End If

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    frm = New frm_drop_conciliation_edit

    frm.ShowEditItem(_session_id, _table_name, _session_date_open, _drop)

    Call GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)

    frm = Nothing

  End Sub ' GUI_ShowConciliationDrop

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()

  End Sub ' GUI_FilterReset

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.opt_opening_session

  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Set the query method to use
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '     - ENUM_QUERY_TYPE
  Protected Overrides Function GUI_GetQueryType() As GUI_Controls.frm_base_sel.ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM
  End Function

  ' PURPOSE: Executes the query with a command
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '     - 
  Protected Overrides Sub GUI_ExecuteQueryCustom()
    Dim _sql_cmd As SqlCommand
    Dim _table As DataTable
    Dim _row As DataRow
    Dim _status As Integer
    Dim _enabled As Integer

    Call GUI_StyleSheet()
    Call InitTotalArrays()
    m_current_cashier_session_id = 0
    m_row_index = 0

    ' Prepare status parameter
    _status = -1
    If Me.chk_session_open.Checked And Not Me.chk_session_closed.Checked Then
      _status = 0
    End If
    If Not Me.chk_session_open.Checked And Me.chk_session_closed.Checked Then
      _status = 1
    End If

    ' Prepare enabled parameter
    _enabled = -1
    If Me.chk_yes.Checked And Not Me.chk_no.Checked Then
      _enabled = 1
    End If
    If Not Me.chk_yes.Checked And Me.chk_no.Checked Then
      _enabled = 0
    End If

    ' Preparing command
    _sql_cmd = New SqlCommand("GT_Session_Information")
    _sql_cmd.CommandType = CommandType.StoredProcedure

    ' Command parameters
    _sql_cmd.Parameters.Add("@pBaseType", SqlDbType.Int).Value = IIf(Me.opt_opening_session.Checked, 0, 1)
    _sql_cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = IIf(Not Me.dtp_from.Checked, System.DBNull.Value, m_report_from)
    _sql_cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = IIf(Not Me.dtp_to.Checked, System.DBNull.Value, m_report_to)
    _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = _status
    _sql_cmd.Parameters.Add("@pEnabled", SqlDbType.Int).Value = _enabled
    _sql_cmd.Parameters.Add("@pAreaId", SqlDbType.Int).Value = Me.cmb_area.Value
    _sql_cmd.Parameters.Add("@pBankId", SqlDbType.Int).Value = Me.cmb_bank.Value
    _sql_cmd.Parameters.Add("@pChipsISOCode", SqlDbType.VarChar).Value = Cage.CHIPS_ISO_CODE
    _sql_cmd.Parameters.Add("@pChipsCoinsCode", SqlDbType.Int).Value = Cage.COINS_CODE
    _sql_cmd.Parameters.Add("@pValidTypes", SqlDbType.VarChar).Value = GetTypeIdListSelected()

    _table = GUI_GetTableUsingCommand(_sql_cmd, Integer.MaxValue)

    Call GUI_BeforeFirstRow()

    Dim db_row As CLASS_DB_ROW
    Dim idx_row As Integer

    'FOS  25-FEB-2015
    If _table.Rows.Count > 0 Then
      For Each _row In _table.Rows
        Try
          db_row = New CLASS_DB_ROW(_row)

          If GUI_CheckOutRowBeforeAdd(db_row) Then
            idx_row = Me.Grid.NumRows - 1

            GUI_SetupRowAdd(idx_row, db_row)

          End If

        Catch ex As OutOfMemoryException
          Throw ex

        Catch exception As Exception
          Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
          Call Trace.WriteLine(exception.ToString())
          Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                                "frm_gaming_table_sessions", _
                                "GUI_SetupRow", _
                                exception.Message)
          Exit For
        End Try

        db_row = Nothing

        If Not GUI_DoEvents(Me.Grid) Then
          Exit Sub
        End If

      Next

      Call GUI_AfterLastRow()
    End If
  End Sub

  ''' <summary>
  ''' Get index of Iso Code in currencies accepted list
  ''' </summary>
  ''' <param name="RowIsoCode"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetIsoCodeIndexInCurrenciesAccepted(RowIsoCode As String) As Integer
    Dim _idx_currencies_accepted As Integer
    _idx_currencies_accepted = 0

    'Calculate column index to current dbrow
    For Each _currency As String In m_currencies_accepted
      If (m_currencies_accepted(_idx_currencies_accepted) = RowIsoCode) Then

        Exit For
      End If

      _idx_currencies_accepted += 1
    Next

    Return _idx_currencies_accepted
  End Function


  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Public Sub GUI_SetupRowAdd(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW)
    Dim _status As String
    Dim _name As String
    Dim _type As String
    Dim _integrated As Boolean
    Dim _cashier_name As String
    Dim _cashier_user As String
    Dim _open_date As String
    Dim _close_date As String
    Dim _visits As Integer
    Dim _area_name As String
    Dim _bank_name As String
    Dim _chips_purchases As Decimal
    Dim _chips_sales As Decimal
    Dim _chips_initial_balance As Decimal
    Dim _chips_fills_balance As Decimal
    Dim _chips_credits_balance As Decimal
    Dim _chips_final_balance As Decimal
    Dim _chips_balance As Decimal
    Dim _cash_balance As Decimal
    Dim _total_balance As Decimal
    Dim _drop As Decimal
    Dim _copy_dealer_validated_amount As Decimal
    Dim _tips As Decimal
    Dim _show_win As Boolean
    Dim _idx_columns As Integer
    Dim _row_iso_code As String
    Dim _idx_currencies_accepted As Integer
    Dim _has_printed As Boolean
    Dim _current_value As Decimal
    Dim _is_first_row As Boolean

    _chips_purchases = 0
    _chips_sales = 0
    _chips_initial_balance = 0
    _chips_fills_balance = 0
    _chips_credits_balance = 0
    _chips_final_balance = 0
    _chips_balance = 0
    _cash_balance = 0
    _total_balance = 0
    _drop = 0
    _copy_dealer_validated_amount = 0
    _tips = 0
    _show_win = False
    _idx_columns = 0
    _row_iso_code = String.Empty
    _idx_currencies_accepted = 0
    _has_printed = False
    _current_value = 0
    _is_first_row = False

    If Me.Grid.NumRows = 0 Then
      Me.Grid.AddRow()
      _is_first_row = True
      _has_printed = True
    End If


    With Me.Grid
      .AutoSize = True


      If (m_current_cashier_session_id <> DbRow.Value(SQL_COLUMN_SESSION_ID)) Then
        _has_printed = True
        If Not _is_first_row Then
          Me.Grid.AddRow()
        End If
      End If

      If Not _is_first_row Then
        m_row_index = Me.Grid.NumRows - 1
      End If

      ' Session Id
      .Cell(m_row_index, GRID_COLUMN_SESSION_ID).Value = DbRow.Value(SQL_COLUMN_SESSION_ID)
      m_current_cashier_session_id = DbRow.Value(SQL_COLUMN_SESSION_ID)
      .Cell(m_row_index, GRID_COLUMN_SESSION_NAME).Value = DbRow.Value(SQL_COLUMN_SESSION_NAME)

      ' Status
      If Not DbRow.IsNull(SQL_COLUMN_STATUS) Then
        _status = String.Empty

        If DbRow.Value(SQL_COLUMN_STATUS) = CASHIER_SESSION_STATUS.OPEN Then
          _status = GLB_NLS_GUI_INVOICING.GetString(156)
        ElseIf DbRow.Value(SQL_COLUMN_STATUS) = CASHIER_SESSION_STATUS.CLOSED Then
          _status = GLB_NLS_GUI_INVOICING.GetString(157)
          _show_win = True
        Else
          Exit Sub
        End If

        .Cell(m_row_index, GRID_COLUMN_STATUS).Value = _status
      End If

      ' Table Name
      If Not DbRow.IsNull(SQL_COLUMN_TABLE_NAME) Then
        _name = DbRow.Value(SQL_COLUMN_TABLE_NAME)
        .Cell(m_row_index, GRID_COLUMN_TABLE_NAME).Value = _name
      End If

      ' Table Type
      If Not DbRow.IsNull(SQL_COLUMN_TABLE_TYPE) Then
        _type = DbRow.Value(SQL_COLUMN_TABLE_TYPE)
        .Cell(m_row_index, GRID_COLUMN_TABLE_TYPE).Value = _type
      End If

      ' Game table Integrated cashier
      If Not DbRow.IsNull(SQL_COLUMN_TABLE_CASHIER) Then
        _integrated = DbRow.Value(SQL_COLUMN_TABLE_CASHIER)
        _cashier_name = GLB_NLS_GUI_INVOICING.GetString(480)
        _cashier_user = String.Empty

        If _integrated Then
          _cashier_name = DbRow.Value(SQL_COLUMN_TABLE_CASHIER_NAME)
          _cashier_user = DbRow.Value(SQL_COLUMN_CASHIER_USER)
        End If

        .Cell(m_row_index, GRID_COLUMN_TABLE_CASHIER).Value = _cashier_name
        .Cell(m_row_index, GRID_COLUMN_CASHIER_USER).Value = _cashier_user
      End If

      ' Open DateTime
      If Not DbRow.IsNull(SQL_COLUMN_OPENING_DATE) Then
        _open_date = GUI_FormatDate(DbRow.Value(SQL_COLUMN_OPENING_DATE), _
                                    ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                    ENUM_FORMAT_TIME.FORMAT_HHMMSS)

        .Cell(m_row_index, GRID_COLUMN_OPENING_DATE).Value = _open_date
      End If

      ' Close DateTime
      If Not DbRow.IsNull(SQL_COLUMN_CLOSING_DATE) Then
        _close_date = GUI_FormatDate(DbRow.Value(SQL_COLUMN_CLOSING_DATE), _
                                      ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                      ENUM_FORMAT_TIME.FORMAT_HHMMSS)

        .Cell(m_row_index, GRID_COLUMN_CLOSING_DATE).Value = _close_date
      End If

      ' Get row Currency iso code
      _row_iso_code = IIf(DbRow.IsNull(SQL_COLUMN_ISO_CODE), String.Empty, DbRow.Value(SQL_COLUMN_ISO_CODE))

      ' Calculate column index to current dbrow
      _idx_currencies_accepted = GetIsoCodeIndexInCurrenciesAccepted(_row_iso_code)

      If (_idx_currencies_accepted < m_currencies_accepted.Length) Then
        _idx_columns = COLUMNS_TO_CHIPS_GROUP * _idx_currencies_accepted

        ' Chips Purchases
        If Not DbRow.IsNull(SQL_COLUMN_CHIPS_PURCHASES) Then
          'FOS 25-FEB-2015
          'RAB 27_JUL-2016
          _chips_purchases = DbRow.Value(SQL_COLUMN_CHIPS_PURCHASES)
          m_chips_purchase(_idx_currencies_accepted) = m_chips_purchase(_idx_currencies_accepted) + _chips_purchases

          If (.Cell(m_row_index, GRID_COLUMN_CHIPS_PURCHASES + _idx_columns).Value = "") Then
            _current_value = 0
          Else
            _current_value = .Cell(m_row_index, GRID_COLUMN_CHIPS_PURCHASES + _idx_columns).Value
          End If

          .Cell(m_row_index, GRID_COLUMN_CHIPS_PURCHASES + _idx_columns).Value = GUI_FormatCurrency(_current_value + _chips_purchases, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)
        End If

        ' Chips Sales
        If Not DbRow.IsNull(SQL_COLUMN_CHIPS_SALES) Then
          'FOS 25-FEB-2015
          'RAB 27-JUL-2016
          _chips_sales = DbRow.Value(SQL_COLUMN_CHIPS_SALES)
          m_chips_sales(_idx_currencies_accepted) = m_chips_sales(_idx_currencies_accepted) + _chips_sales

          If (.Cell(m_row_index, GRID_COLUMN_CHIPS_SALES + _idx_columns).Value = "") Then
            _current_value = 0
          Else
            _current_value = .Cell(m_row_index, GRID_COLUMN_CHIPS_SALES + _idx_columns).Value
          End If

          .Cell(m_row_index, GRID_COLUMN_CHIPS_SALES + _idx_columns).Value = GUI_FormatCurrency(_current_value + _chips_sales, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)
        End If

        ' Chips Initial Balance
        If Not DbRow.IsNull(SQL_COLUMN_CHIPS_INITIAL_BALANCE) Then
          _chips_initial_balance = DbRow.Value(SQL_COLUMN_CHIPS_INITIAL_BALANCE)
          m_initial_chips_balance(_idx_currencies_accepted) = m_initial_chips_balance(_idx_currencies_accepted) + _chips_initial_balance

          If (.Cell(m_row_index, GRID_COLUMN_CHIPS_INITIAL_BALANCE + _idx_columns).Value = "") Then
            _current_value = 0
          Else
            _current_value = .Cell(m_row_index, GRID_COLUMN_CHIPS_INITIAL_BALANCE + _idx_columns).Value
          End If

          .Cell(m_row_index, GRID_COLUMN_CHIPS_INITIAL_BALANCE + _idx_columns).Value = GUI_FormatCurrency(_current_value + _chips_initial_balance, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)
        End If

        ' Chips Fills Balance
        If Not DbRow.IsNull(SQL_COLUMN_CHIPS_FILLS_BALANCE) Then
          _chips_fills_balance = DbRow.Value(SQL_COLUMN_CHIPS_FILLS_BALANCE)
          m_fills_chips_balance(_idx_currencies_accepted) = m_fills_chips_balance(_idx_currencies_accepted) + _chips_fills_balance

          If (.Cell(m_row_index, GRID_COLUMN_CHIPS_FILLS_BALANCE + _idx_columns).Value = "") Then
            _current_value = 0
          Else
            _current_value = .Cell(m_row_index, GRID_COLUMN_CHIPS_FILLS_BALANCE + _idx_columns).Value
          End If

          .Cell(m_row_index, GRID_COLUMN_CHIPS_FILLS_BALANCE + _idx_columns).Value = GUI_FormatCurrency(_current_value + _chips_fills_balance, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)
        End If

        ' Chips Credits Balance
        If Not DbRow.IsNull(SQL_COLUMN_CHIPS_CREDITS_BALANCE) Then
          _chips_credits_balance = DbRow.Value(SQL_COLUMN_CHIPS_CREDITS_BALANCE)
          m_credits_chips_balance(_idx_currencies_accepted) = m_credits_chips_balance(_idx_currencies_accepted) + _chips_credits_balance

          If (.Cell(m_row_index, GRID_COLUMN_CHIPS_CREDITS_BALANCE + _idx_columns).Value = "") Then
            _current_value = 0
          Else
            _current_value = .Cell(m_row_index, GRID_COLUMN_CHIPS_CREDITS_BALANCE + _idx_columns).Value
          End If

          .Cell(m_row_index, GRID_COLUMN_CHIPS_CREDITS_BALANCE + _idx_columns).Value = GUI_FormatCurrency(_current_value + _chips_credits_balance, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)
        End If

        ' Chips Final Balance
        If Not DbRow.IsNull(SQL_COLUMN_CHIPS_FINAL_BALANCE) Then
          _chips_final_balance = DbRow.Value(SQL_COLUMN_CHIPS_FINAL_BALANCE)
          m_final_chips_balance(_idx_currencies_accepted) = m_final_chips_balance(_idx_currencies_accepted) + _chips_final_balance

          If (.Cell(m_row_index, GRID_COLUMN_CHIPS_FINAL_BALANCE + _idx_columns).Value = "") Then
            _current_value = 0
          Else
            _current_value = .Cell(m_row_index, GRID_COLUMN_CHIPS_FINAL_BALANCE + _idx_columns).Value
          End If

          .Cell(m_row_index, GRID_COLUMN_CHIPS_FINAL_BALANCE + _idx_columns).Value = GUI_FormatCurrency(_current_value + _chips_final_balance, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)
        End If

        _idx_columns = ((COLUMNS_TO_CHIPS_GROUP * m_currencies_accepted.Length) - COLUMNS_TO_CHIPS_GROUP) ' +

        ' Drop
        If GamingTableBusinessLogic.IsEnableManualConciliation Then
          If Not DbRow.IsNull(SQL_COLUMN_DROP) Then
            _drop = DbRow.Value(SQL_COLUMN_DROP)
            m_drop_amount(_idx_currencies_accepted) = m_drop_amount(_idx_currencies_accepted) + _drop

            If (.Cell(m_row_index, GRID_COLUMN_DROP + _idx_columns).Value = "") Then
              _current_value = 0
            Else
              _current_value = .Cell(m_row_index, GRID_COLUMN_DROP + _idx_columns).Value
            End If

            .Cell(m_row_index, GRID_COLUMN_DROP + _idx_columns + _idx_currencies_accepted).Value = GUI_FormatCurrency(_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)
          End If
        End If

        ' COPY_DEALER_VALIDATED_AMOUNT
        If Not DbRow.IsNull(SQL_COLUMN_COPY_DEALER_VALIDATED_AMOUNT) Then
          _copy_dealer_validated_amount = DbRow.Value(SQL_COLUMN_COPY_DEALER_VALIDATED_AMOUNT)
          m_copy_dealer_validated_amount(_idx_currencies_accepted) = m_copy_dealer_validated_amount(_idx_currencies_accepted) + _copy_dealer_validated_amount

          If (.Cell(m_row_index, GRID_COLUMN_COPY_DEALER_VALIDATED_AMOUNT + _idx_columns).Value = "") Then
            _current_value = 0
          Else
            _current_value = .Cell(m_row_index, GRID_COLUMN_COPY_DEALER_VALIDATED_AMOUNT + _idx_columns).Value
          End If

          .Cell(m_row_index, GRID_COLUMN_COPY_DEALER_VALIDATED_AMOUNT + _idx_columns + _idx_currencies_accepted).Value = GUI_FormatCurrency(_copy_dealer_validated_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)
        End If


        _idx_columns = ((COLUMNS_TO_CHIPS_GROUP * m_currencies_accepted.Length) - COLUMNS_TO_CHIPS_GROUP) +
                       ((COLUMNS_TO_DROP_GROUP * m_currencies_accepted.Length) - COLUMNS_TO_DROP_GROUP)



        ' Tips
        If Not DbRow.IsNull(SQL_COLUMN_TIPS) Then
          'FOS 25-FEB-2015
          'RAB 27-JUL-2016
          _tips = DbRow.Value(SQL_COLUMN_TIPS)
          m_chips_tips(_idx_currencies_accepted) = m_chips_tips(_idx_currencies_accepted) + _tips

          If (.Cell(m_row_index, GRID_COLUMN_TIPS + _idx_columns + _idx_currencies_accepted).Value = "") Then
            _current_value = 0
          Else
            _current_value = .Cell(m_row_index, GRID_COLUMN_TIPS + _idx_columns + _idx_currencies_accepted).Value
          End If

          .Cell(m_row_index, GRID_COLUMN_TIPS + _idx_columns + _idx_currencies_accepted).Value = GUI_FormatCurrency(_current_value + _tips, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)

        End If

        _idx_columns = ((COLUMNS_TO_CHIPS_GROUP * m_currencies_accepted.Length) - COLUMNS_TO_CHIPS_GROUP) +
                       ((COLUMNS_TO_TIPS_GROUP * m_currencies_accepted.Length) - COLUMNS_TO_TIPS_GROUP) +
                       ((COLUMNS_TO_DROP_GROUP * m_currencies_accepted.Length) - COLUMNS_TO_DROP_GROUP) +
                        (COLUMNS_TO_BALANCE_CHIPS * _idx_currencies_accepted)

        ' Chips Balance
        _chips_balance = (_chips_final_balance + _chips_credits_balance) - (_chips_fills_balance + _chips_initial_balance)
        m_chips_balance(_idx_currencies_accepted) = m_chips_balance(_idx_currencies_accepted) + _chips_balance

        If (.Cell(m_row_index, GRID_COLUMN_CHIPS_BALANCE + _idx_columns).Value = "") Then
          _current_value = 0
        Else

          _current_value = .Cell(m_row_index, GRID_COLUMN_CHIPS_BALANCE + _idx_columns).Value

        End If

        .Cell(m_row_index, GRID_COLUMN_CHIPS_BALANCE + _idx_columns).Value = GUI_FormatCurrency(_current_value + _chips_balance, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)

        ' Cash Balance (Collected)
        If Not DbRow.IsNull(SQL_COLUMN_COLLECTED) Then
          _cash_balance = Convert.ToDecimal(DbRow.Value(SQL_COLUMN_COLLECTED))
          m_currency_balance(_idx_currencies_accepted) = m_currency_balance(_idx_currencies_accepted) + _cash_balance

          'FOS 25-FEB-2015
          'RAB 27-JUL-2016
          If (.Cell(m_row_index, GRID_COLUMN_CASH_BALANCE + _idx_columns).Value = "") Then
            _current_value = 0
          Else
            _current_value = .Cell(m_row_index, GRID_COLUMN_CASH_BALANCE + _idx_columns).Value
          End If

          .Cell(m_row_index, GRID_COLUMN_CASH_BALANCE + _idx_columns).Value = GUI_FormatCurrency(_current_value + _cash_balance, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)
        End If

        ' Win
        If _show_win Then
          'FOS 25-FEB-2015
          'RAB 27-JUL-2016
          _total_balance = _chips_balance + _cash_balance
          m_total_balance(_idx_currencies_accepted) = m_total_balance(_idx_currencies_accepted) + _total_balance

          If (.Cell(m_row_index, GRID_COLUMN_TOTAL_BALANCE + _idx_columns).Value = "") Then
            _current_value = 0
          Else
            _current_value = .Cell(m_row_index, GRID_COLUMN_TOTAL_BALANCE + _idx_columns).Value
          End If

          .Cell(m_row_index, GRID_COLUMN_TOTAL_BALANCE + _idx_columns).Value = GUI_FormatCurrency(_current_value + _total_balance, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)
        Else
          .Cell(m_row_index, GRID_COLUMN_TOTAL_BALANCE + _idx_columns).Value = String.Empty
        End If
      End If

      'Calculate final index
      _idx_columns = ((COLUMNS_TO_CHIPS_GROUP * m_currencies_accepted.Length) - COLUMNS_TO_CHIPS_GROUP) +
                     ((COLUMNS_TO_DROP_GROUP * m_currencies_accepted.Length) - COLUMNS_TO_DROP_GROUP) +
                     ((COLUMNS_TO_TIPS_GROUP * m_currencies_accepted.Length) - COLUMNS_TO_TIPS_GROUP) +
                     ((COLUMNS_TO_BALANCE_CHIPS * m_currencies_accepted.Length) - COLUMNS_TO_BALANCE_CHIPS)

      ' Visits
      If _has_printed Then
        If Not DbRow.IsNull(SQL_COLUMN_VISITS) Then
          _visits = DbRow.Value(SQL_COLUMN_VISITS)
          m_visits = m_visits + _visits

          .Cell(m_row_index, GRID_COLUMN_VISITS + _idx_columns).Value = GUI_FormatNumber(_visits, 0)
        End If
      End If

      ' Area
      If Not DbRow.IsNull(SQL_COLUMN_AREA_NAME) Then
        _area_name = DbRow.Value(SQL_COLUMN_AREA_NAME)
        .Cell(m_row_index, GRID_COLUMN_AREA + _idx_columns).Value = _area_name
      End If

      ' Bank
      If Not DbRow.IsNull(SQL_COLUMN_BANK) Then
        _bank_name = DbRow.Value(SQL_COLUMN_BANK)
        .Cell(m_row_index, GRID_COLUMN_BANK + _idx_columns).Value = _bank_name
      End If

      ' Gaming table session Id
      .Cell(m_row_index, GRID_COLUMN_GAMING_TABLE_SESSION_ID + _idx_columns).Value = DbRow.Value(SQL_COLUMN_GAMING_TABLE_SESSION_ID)

      ' Total local drop
      .Cell(m_row_index, GRID_COLUMN_TOTAL_LOCAL_DROP + _idx_columns).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TOTAL_LOCAL_DROP), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)

    End With

  End Sub ' GUI_SetupRowAdd

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  '
  Protected Overrides Function GUI_FilterCheck() As Boolean
    Dim _idx As Integer
    Dim _all_uncheckeds As Boolean

    If Me.opt_several_types.Checked Then
      _all_uncheckeds = True
      For _idx = 0 To dg_filter.NumRows - 1
        If Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
          _all_uncheckeds = False
          Exit For
        End If
      Next
      If _all_uncheckeds Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4351), ENUM_MB_TYPE.MB_TYPE_WARNING)

        Return False
      End If
    End If

    ' Date selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    Return True

  End Function ' GUI_FilterCheck 

  Protected Overrides Sub Finalize()
    MyBase.Finalize()

  End Sub

  'FOS  25-FEB-2015
  ' PURPOSE: Perform final processing for the grid data (totalisator row) 
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow() ' GUI_AfterLastRow

    Me.AddTotalRow()

  End Sub ' GUI_AfterLastRow


  ' PURPOSE: Enable button in selected row
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_RowSelectedEvent(ByVal SelectedRow As Integer)

    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False

    If SelectedRow >= 0 And Grid.NumRows > 0 Then
      If Me.Grid.Cell(SelectedRow, GRID_COLUMN_CLOSING_DATE).Value.Length > 0 Then
        GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = True And CurrentUser.Permissions(ENUM_FORM.FORM_DROP_CONCILIATION_EDIT).Read
      End If
    End If

  End Sub ' GUI_RowSelectedEvent

#End Region ' OVERRIDES

#Region " Private Functions "

  ' PURPOSE: Build search component to add to the 'where' clause of the 'select' statement
  '          according to the values selected in the data grid filter
  '
  '  PARAMS:
  '     - INPUT:
  '           - DBField: database field name to use in the search condition
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Search condition to add
  '
  Private Function GetSqlWhereTableType(ByVal DBField As String) As String
    Dim str_where As String
    Dim types_list As String

    str_where = ""

    ' Operation Code Type Filter
    If Me.opt_several_types.Checked = True Then
      types_list = GetTypeIdListSelected()
      If types_list <> "" Then
        str_where = " AND (" & DBField & " IN (" & types_list & "))"
      End If
    End If

    Return str_where

  End Function ' GetSqlWhereTableType

  ' PURPOSE: Add Handles
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub AddHandles()

    AddHandler cmb_area.ValueChangedEvent, AddressOf cmb_area_ValueChangedEvent

  End Sub ' AddHandles

  ' PURPOSE: Option Button checked changed
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub opt_all_types_CheckedChanged()
    Dim idx As Integer

    For idx = 0 To dg_filter.NumRows - 1
      Me.dg_filter.Cell(idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    Next

  End Sub

  ' PURPOSE: Area selection
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub cmb_area_ValueChangedEvent()

    'Zone (smoking - no smoking)
    ef_smoking.Value = GetZoneDescription(cmb_area.Value)

    'Banks
    Call BankComboFill()

  End Sub

  ' PURPOSE: Returns zone description
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  '
  Private Function GetZoneDescription(ByVal AreaId As Integer) As String
    Dim _table As DataTable
    Dim _returnValue As String

    _table = GUI_GetTableUsingSQL("SELECT AR_SMOKING FROM AREAS WHERE AR_AREA_ID = " & AreaId.ToString(), Integer.MaxValue)

    _returnValue = GLB_NLS_GUI_CONFIGURATION.GetString(437)

    If Not IsNothing(_table) Then
      If _table.Rows.Count() > 0 Then
        If _table.Rows.Item(0).Item("AR_SMOKING") Then
          _returnValue = GLB_NLS_GUI_CONFIGURATION.GetString(436)
        End If
      End If
    End If

    Return _returnValue

  End Function

  ' PURPOSE: Init areas combo box
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub AreaComboFill()
    Dim _table As DataTable

    _table = GUI_GetTableUsingSQL("SELECT AR_AREA_ID, AR_NAME FROM AREAS INNER JOIN BANKS ON BK_AREA_ID = AR_AREA_ID GROUP BY AR_AREA_ID, AR_NAME ORDER BY AR_NAME", Integer.MaxValue)

    Me.cmb_area.Clear()
    Me.cmb_area.Add(_table)

  End Sub     ' AreaComboFill

  ' PURPOSE: Init BANKS combo box
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub BankComboFill()
    Dim _table As DataTable

    _table = GUI_GetTableUsingSQL("SELECT BK_BANK_ID, BK_NAME FROM BANKS WHERE BK_AREA_ID = " & cmb_area.Value & " GROUP BY BK_BANK_ID, BK_NAME ORDER BY BK_NAME", Integer.MaxValue)

    Me.cmb_bank.Clear()
    Me.cmb_bank.Add(_table)

  End Sub     ' BankComboFill

  ''' <summary>
  ''' Get total columns
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function NumTotalColumns() As Integer
    Dim _num_currencies_accepted As Integer

    _num_currencies_accepted = m_currencies_accepted.Length

    If (_num_currencies_accepted = 1) Then
      Return GRID_COLUMNS
    Else
      Return GRID_COLUMNS + ((_num_currencies_accepted - 1) * (COLUMNS_TO_CHIPS_GROUP + COLUMNS_TO_DROP_GROUP + COLUMNS_TO_TIPS_GROUP + COLUMNS_TO_BALANCE_CHIPS))
    End If
  End Function

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub GUI_StyleSheet()
    Dim _idx_columns As Integer
    Dim _first_group As Boolean
    _idx_columns = 0
    _first_group = True

    With Me.Grid
      Call .Init(NumTotalColumns(), GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
      .Sortable = True

      ' Session Id
      GUI_AddColumn(Me.Grid, GRID_COLUMN_SESSION_ID, String.Empty, 0, False)

      ' Session Name
      GUI_AddColumn(Me.Grid, GRID_COLUMN_SESSION_NAME, String.Empty, 0, False)

      ' Index      
      .Column(GRID_COLUMN_SENTINEL).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_SENTINEL).Header(1).Text = String.Empty
      .Column(GRID_COLUMN_SENTINEL).Width = GRID_COLUMN_SENTINEL_WIDTH
      .Column(GRID_COLUMN_SENTINEL).HighLightWhenSelected = False
      .Column(GRID_COLUMN_SENTINEL).IsColumnPrintable = False

      ' SESSION STATUS
      GUI_AddColumn(Me.Grid, GRID_COLUMN_STATUS, GLB_NLS_GUI_INVOICING.GetString(155), GRID_COLUMN_STATUS_WIDTH, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER)

      ' GAMING TABLE NAME
      GUI_AddColumn(Me.Grid, GRID_COLUMN_TABLE_NAME, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3410), GRID_COLUMN_TABLE_NAME_WIDTH, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3419))

      ' GAMING TABLE TYPE
      GUI_AddColumn(Me.Grid, GRID_COLUMN_TABLE_TYPE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3410), GRID_COLUMN_TABLE_TYPE_WIDTH, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(254))

      ' GAMING TABLE CASHIER
      GUI_AddColumn(Me.Grid, GRID_COLUMN_TABLE_CASHIER, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3410), GRID_COLUMN_TABLE_CASHIER_WIDTH, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3424))

      ' GAMING CASHIER USER
      GUI_AddColumn(Me.Grid, GRID_COLUMN_CASHIER_USER, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3410), GRID_COLUMN_CASHIER_USER_WIDTH, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3460))

      ' OPEN DATETIME
      GUI_AddColumn(Me.Grid, GRID_COLUMN_OPENING_DATE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(605), GRID_COLUMN_OPENING_DATE_WIDTH, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER, GLB_NLS_GUI_INVOICING.GetString(244))

      ' CLOSE DATETIME
      GUI_AddColumn(Me.Grid, GRID_COLUMN_CLOSING_DATE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(605), GRID_COLUMN_CLOSING_DATE_WIDTH, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER, GLB_NLS_GUI_INVOICING.GetString(266))

      Dim _header1_drop As String
      Dim _width_drop As Integer

      For Each _currency As String In m_currencies_accepted
        ' CHIPS PURCHASES
        GUI_AddColumn(Me.Grid, GRID_COLUMN_CHIPS_PURCHASES + _idx_columns,
                      GLB_NLS_GUI_PLAYER_TRACKING.GetString(2941) & " " & _currency,
                      GRID_COLUMN_CHIPS_PURCHASES_WIDTH,
                      uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT,
                      GLB_NLS_GUI_PLAYER_TRACKING.GetString(2343))

        ' CHIPS SALES
        GUI_AddColumn(Me.Grid, GRID_COLUMN_CHIPS_SALES + _idx_columns,
                      GLB_NLS_GUI_PLAYER_TRACKING.GetString(2941) & " " & _currency,
                      GRID_COLUMN_CLOSING_DATE_WIDTH,
                      uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT,
                      GLB_NLS_GUI_PLAYER_TRACKING.GetString(2344))

        ' CHIPS INITIAL BALANCE
        If opt_movement_session.Checked Then
          GUI_AddColumn(Me.Grid, GRID_COLUMN_CHIPS_INITIAL_BALANCE + _idx_columns,
                        "",
                        0,
                        uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT,
                        GLB_NLS_GUI_PLAYER_TRACKING.GetString(7486))
        Else
          GUI_AddColumn(Me.Grid, GRID_COLUMN_CHIPS_INITIAL_BALANCE + _idx_columns,
                        GLB_NLS_GUI_PLAYER_TRACKING.GetString(2941) & " " & _currency,
                        GRID_COLUMN_CHIPS_INITIAL_BALANCE_WIDTH,
                        uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT,
                        GLB_NLS_GUI_PLAYER_TRACKING.GetString(7486))
        End If

        ' CHIPS FILLS
        GUI_AddColumn(Me.Grid, GRID_COLUMN_CHIPS_FILLS_BALANCE + _idx_columns,
                      GLB_NLS_GUI_PLAYER_TRACKING.GetString(2941) & " " & _currency,
                      GRID_COLUMN_CHIPS_FILLS_BALANCE_WIDTH,
                      uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT,
                      GLB_NLS_GUI_PLAYER_TRACKING.GetString(7487))

        ' CHIPS CREDITS
        GUI_AddColumn(Me.Grid, GRID_COLUMN_CHIPS_CREDITS_BALANCE + _idx_columns,
                      GLB_NLS_GUI_PLAYER_TRACKING.GetString(2941) & " " & _currency,
                      GRID_COLUMN_CHIPS_CREDITS_BALANCE_WIDTH,
                      uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT,
                      GLB_NLS_GUI_PLAYER_TRACKING.GetString(7488))

        ' CHIPS FINAL BALANCE
        If opt_movement_session.Checked Then
          GUI_AddColumn(Me.Grid, GRID_COLUMN_CHIPS_FINAL_BALANCE + _idx_columns,
                        "",
                        0,
                        uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT,
                        GLB_NLS_GUI_PLAYER_TRACKING.GetString(7489))
        Else
          GUI_AddColumn(Me.Grid, GRID_COLUMN_CHIPS_FINAL_BALANCE + _idx_columns,
                        GLB_NLS_GUI_PLAYER_TRACKING.GetString(2941) & " " & _currency,
                        GRID_COLUMN_CHIPS_FINAL_BALANCE_WIDTH,
                        uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT,
                        GLB_NLS_GUI_PLAYER_TRACKING.GetString(7489))
        End If


        _idx_columns = IIf((m_currencies_accepted(m_currencies_accepted.Length - 1) = _currency), _idx_columns, _idx_columns + COLUMNS_TO_CHIPS_GROUP)

      Next

      For Each _currency As String In m_currencies_accepted
        ' DROP
        _header1_drop = String.Empty
        _width_drop = 0

        If GamingTableBusinessLogic.IsEnableManualConciliation Then
          _header1_drop = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3411)
          _width_drop = GRID_COLUMN_DROP_WIDTH
        End If

        GUI_AddColumn(Me.Grid, GRID_COLUMN_DROP + _idx_columns,
                      " ",
                      _width_drop,
                      uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT,
                      GLB_NLS_GUI_PLAYER_TRACKING.GetString(3411) & " (" & _currency & ")")

        ' COPY DEALER VALIDATED AMOUNT
        GUI_AddColumn(Me.Grid, GRID_COLUMN_COPY_DEALER_VALIDATED_AMOUNT + _idx_columns,
                      " ",
                      GRID_COLUMN_COPY_DEALER_VALIDATED_AMOUNT_WIDTH,
                      uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT,
                      GLB_NLS_GUI_PLAYER_TRACKING.GetString(8063) & " (" & _currency & ")")

        _idx_columns = IIf((m_currencies_accepted(m_currencies_accepted.Length - 1) = _currency), _idx_columns, _idx_columns + COLUMNS_TO_DROP_GROUP)
      Next

      For Each _currency As String In m_currencies_accepted
        ' TIPS
        GUI_AddColumn(Me.Grid, GRID_COLUMN_TIPS + _idx_columns,
                      " ",
                      GRID_COLUMN_TIPS_WIDTH,
                      uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT,
                      GLB_NLS_GUI_PLAYER_TRACKING.GetString(3401) & " (" & _currency & ")")

        _idx_columns = IIf((m_currencies_accepted(m_currencies_accepted.Length - 1) = _currency), _idx_columns, _idx_columns + COLUMNS_TO_TIPS_GROUP)
      Next

      For Each _currency As String In m_currencies_accepted
        ' CHIPS BALANCE
        GUI_AddColumn(Me.Grid, GRID_COLUMN_CHIPS_BALANCE + _idx_columns,
                      GLB_NLS_GUI_PLAYER_TRACKING.GetString(4707) & " (" & _currency & ")",
                      GRID_COLUMN_CHIPS_BALANCE_WIDTH,
                      uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT,
                      GLB_NLS_GUI_PLAYER_TRACKING.GetString(2941))

        ' CURRENCY BALANCE
        GUI_AddColumn(Me.Grid, GRID_COLUMN_CASH_BALANCE + _idx_columns,
                      GLB_NLS_GUI_PLAYER_TRACKING.GetString(4707) & " (" & _currency & ")",
                      GRID_COLUMN_CASH_BALANCE_WIDTH,
                      uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT,
                      GLB_NLS_GUI_PLAYER_TRACKING.GetString(4605))

        ' TOTAL BALANCE
        GUI_AddColumn(Me.Grid, GRID_COLUMN_TOTAL_BALANCE + _idx_columns,
                      GLB_NLS_GUI_PLAYER_TRACKING.GetString(4707) & " (" & _currency & ")",
                      GRID_COLUMN_TOTAL_BALANCE_WIDTH,
                      uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT,
                      GLB_NLS_GUI_PLAYER_TRACKING.GetString(5720))

        _idx_columns = IIf((m_currencies_accepted(m_currencies_accepted.Length - 1) = _currency), _idx_columns, _idx_columns + COLUMNS_TO_BALANCE_CHIPS)

      Next

      ' CLIENT VISITS
      GUI_AddColumn(Me.Grid, GRID_COLUMN_VISITS + _idx_columns, String.Empty, GRID_COLUMN_VISITS_WIDTH, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER, GLB_NLS_GUI_PLAYER_TRACKING.GetString(542))

      ' AREA
      GUI_AddColumn(Me.Grid, GRID_COLUMN_AREA + _idx_columns, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1166), GRID_COLUMN_AREA_WIDTH, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7230))

      ' BANK
      GUI_AddColumn(Me.Grid, GRID_COLUMN_BANK + _idx_columns, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1166), GRID_COLUMN_BANK_WIDTH, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7232))

      ' Gaming Table Session Id
      GUI_AddColumn(Me.Grid, GRID_COLUMN_GAMING_TABLE_SESSION_ID + _idx_columns, String.Empty, 0, , String.Empty, False)

      ' Total local drop
      GUI_AddColumn(Me.Grid, GRID_COLUMN_TOTAL_LOCAL_DROP + _idx_columns, String.Empty, GRID_COLUMN_TOTAL_DROP_WIDTH, , String.Empty, False)

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub SetDefaultValues()
    Dim _closing_time As Integer
    Dim _initial_time As Date
    Dim _now As Date

    _now = WGDB.Now
    _closing_time = GetDefaultClosingTime()
    _initial_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)

    ' Enabled
    chk_yes.Checked = True
    chk_no.Checked = False

    ' State
    chk_session_closed.Checked = False
    chk_session_open.Checked = True

    ' Date
    opt_movement_session.Checked = False
    opt_opening_session.Checked = True

    dtp_from.Value = _initial_time
    dtp_from.Checked = True
    dtp_to.Value = _initial_time.AddDays(1)
    dtp_to.Checked = False

    'Movement Types Filter Data Grid
    Call InitTableTypesFilter()

    ' Location
    cmb_area.SelectedIndex = 0
    cmb_bank.SelectedIndex = 0
    ef_smoking.Value = String.Empty

    'Init all total arrays
    Call InitTotalArrays()

  End Sub ' SetDefaultValues

  ' PURPOSE: Checks whether the specified row contains data or not
  '
  '  PARAMS:
  '     - INPUT:
  '           - IdxRow: row to check
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - TRUE: selected row contains data
  '     - FALSE: selected row does not exist or contains no data

  Private Function IsValidDataRow(ByVal IdxRow As Integer) As Boolean

    ' Skip Totalizator row!!!
    If IdxRow < 0 Or IdxRow >= (Me.Grid.NumRows - 1) Then
      Return False
    Else
      Return True
    End If

  End Function ' IsValidDataRow

#Region "Table Types"

  ' PURPOSE: Define the layout of the movement types grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub GUI_StyleSheetTableTypes()

    With Me.dg_filter
      Call .Init(GRID_2_COLUMNS, 0, Editable:=True)
      .Counter(0).Visible = False
      .Sortable = False

      ' Hidden columns
      .Column(GRID_2_COLUMN_CODE).Header.Text = String.Empty
      .Column(GRID_2_COLUMN_CODE).WidthFixed = 0

      ' GRID_COL_CHECKBOX
      .Column(GRID_2_COLUMN_CHECKED).Header.Text = String.Empty
      .Column(GRID_2_COLUMN_CHECKED).WidthFixed = 400
      .Column(GRID_2_COLUMN_CHECKED).Fixed = True
      .Column(GRID_2_COLUMN_CHECKED).Editable = True
      .Column(GRID_2_COLUMN_CHECKED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX

      ' GRID_COL_DESCRIPTION
      .Column(GRID_2_COLUMN_DESC).Header.Text = String.Empty
      .Column(GRID_2_COLUMN_DESC).WidthFixed = 2351
      .Column(GRID_2_COLUMN_DESC).Fixed = True
      .Column(GRID_2_COLUMN_DESC).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' GRID_COL_TYPE
      .Column(GRID_2_COLUMN_ROW_TYPE).Header.Text = String.Empty
      .Column(GRID_2_COLUMN_ROW_TYPE).WidthFixed = 0
    End With

  End Sub 'GUI_StyleSheetTableTypes

  ' PURPOSE: Initialize the movement types filter grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub InitTableTypesFilter()

    Me.opt_all_types.Checked = True
    Me.dg_filter.Enabled = True

    ' Reset dg selections
    Call FillTableTypesFilterGrid(GetTableTypesFilterGridData())

  End Sub ' InitTableTypesFilter

  ' PURPOSE: Populate the movement types data grid filter.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub FillTableTypesFilterGrid(ByVal DgList As List(Of TYPE_DG_FILTER_ELEMENT))

    Me.dg_filter.Clear()
    Me.dg_filter.Redraw = False

    ' Get the filters array and insert it into the grid.
    For Each _element As TYPE_DG_FILTER_ELEMENT In DgList
      Call AddOneRowData(_element.code, _element.description)
    Next

    Me.dg_filter.Redraw = True

  End Sub ' FillTableTypesFilterGrid

  ' PURPOSE: Obtain the list of elements that will populate the 
  '         movement types filter grid.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '     - List of elements to be added to the grid
  '
  Private Function GetTableTypesFilterGridData() As List(Of TYPE_DG_FILTER_ELEMENT)
    ' Dimensioned this way to avoid getting an extra element: VB uses the constant to set the upper value, 
    ' not the number of elements
    Dim _table_types As DataTable
    Dim _row As TYPE_DG_FILTER_ELEMENT
    Dim _dg_filter_rows_list As List(Of TYPE_DG_FILTER_ELEMENT)

    _dg_filter_rows_list = New List(Of TYPE_DG_FILTER_ELEMENT)

    _table_types = GetTableTypes()

    For Each _dr As DataRow In _table_types.Rows
      _row = New TYPE_DG_FILTER_ELEMENT
      _row.code = _dr("GTT_GAMING_TABLE_TYPE_ID")
      _row.description = _dr("GTT_NAME")
      _row.elem_type = 1
      _dg_filter_rows_list.Add(_row)
    Next

    Return _dg_filter_rows_list

  End Function ' GetTableTypesFilterGridData

  ' PURPOSE: Get the list of the items selected in the filter data grid
  '          Format list to build query: X, X, X
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String contining id's of the selected rows
  '
  Private Function GetTypeIdListSelected(Optional ByVal GetNames As Boolean = False) As String
    Dim _idx_row As Integer
    Dim _list_mov_types As String
    Dim _field As String

    _list_mov_types = String.Empty

    For _idx_row = 0 To Me.dg_filter.NumRows - 1

      _field = Me.dg_filter.Cell(_idx_row, GRID_2_COLUMN_CODE).Value

      If GetNames Then
        _field = Me.dg_filter.Cell(_idx_row, GRID_2_COLUMN_DESC).Value
      End If

      If dg_filter.Cell(_idx_row, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED And _field <> "" Then
        If _list_mov_types.Length = 0 Then
          _list_mov_types = _field
        Else
          _list_mov_types = _list_mov_types & ", " & _field
        End If
      End If
    Next

    Return _list_mov_types

  End Function 'GetTypeIdListSelected

  ' PURPOSE: Update the checkbox state for all row dependants (for header rows)
  '
  '  PARAMS:
  '     - INPUT:
  '           - FirstRow
  '           - ValueChecked
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub ChangeCheckofRows(ByVal FirstRow As Integer, ByVal ValueChecked As String)
    Dim idx As Integer

    For idx = FirstRow + 1 To dg_filter.NumRows - 1

      Me.dg_filter.Cell(idx, GRID_2_COLUMN_CHECKED).Value = ValueChecked

    Next

  End Sub ' ChangeCheckofRows

  ' PURPOSE: Returns a string with the description of all the selected movement types
  '
  '  PARAMS:
  '     - INPUT:
  '           - 
  '     - OUTPUT:
  '           - SelectedMovements
  '
  ' RETURNS:
  '     - None
  '
  Private Sub GetSelectedTableTypes(ByRef SelectedMovements As String)
    Dim idx As Integer

    SelectedMovements = String.Empty

    For idx = 0 To dg_filter.NumRows - 1
      If Me.dg_filter.Cell(idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        If SelectedMovements = String.Empty Then
          SelectedMovements = Me.dg_filter.Cell(idx, GRID_2_COLUMN_DESC).Value.Trim()
        Else
          SelectedMovements &= ", " & Me.dg_filter.Cell(idx, GRID_2_COLUMN_DESC).Value.Trim()
        End If
      End If
    Next
  End Sub

  ' PURPOSE: Add a new data row at the data grid filter
  '
  '  PARAMS:
  '     - INPUT:
  '           - TypeCode: numeric identifier
  '           - TypeDesc: textual description
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub AddOneRowData(ByVal TypeCode As Integer, ByVal TypeDesc As String)
    Dim prev_redraw As Boolean

    prev_redraw = Me.dg_filter.Redraw
    Me.dg_filter.Redraw = False

    dg_filter.AddRow()

    Me.dg_filter.Redraw = False

    dg_filter.Cell(dg_filter.NumRows - 1, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
    dg_filter.Cell(dg_filter.NumRows - 1, GRID_2_COLUMN_CODE).Value = TypeCode
    dg_filter.Cell(dg_filter.NumRows - 1, GRID_2_COLUMN_DESC).Value = TypeDesc
    dg_filter.Cell(dg_filter.NumRows - 1, GRID_2_COLUMN_ROW_TYPE).Value = 1 ' Row type Data. 

    Me.dg_filter.Redraw = prev_redraw

  End Sub ' AddOneRowData

#Region " GUI Reports "

  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(ExcelData)

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201), m_report_date_type)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(202), m_report_from_text)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(203), m_report_to_text)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(211), m_report_types)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(152), m_report_status)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(3422), m_report_enabled)
    PrintData.SetFilter(GLB_NLS_GUI_CONFIGURATION.GetString(435), m_report_area_name)
    PrintData.SetFilter(GLB_NLS_GUI_CONFIGURATION.GetString(430), m_report_zone_name)
    PrintData.SetFilter(GLB_NLS_GUI_CONFIGURATION.GetString(431), m_report_bank_name)

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_report_enabled = String.Empty
    m_report_status = String.Empty
    m_report_date_type = String.Empty
    m_report_area_name = String.Empty
    m_report_zone_name = String.Empty
    m_report_bank_name = String.Empty
    m_report_types = String.Empty
    m_report_from_text = String.Empty
    m_report_to_text = String.Empty

    m_report_from = Nothing
    m_report_to = Nothing
    ' Enabled filter
    If Me.chk_yes.Checked Then
      m_report_enabled = Me.chk_yes.Text
    End If

    If Me.chk_no.Checked Then
      If String.IsNullOrEmpty(m_report_enabled) Then
        m_report_enabled = Me.chk_no.Text
      Else
        m_report_enabled = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353) ' All
      End If
    End If

    ' Enabled filter
    If Me.chk_session_open.Checked Then
      m_report_status = Me.chk_session_open.Text
    End If

    ' Status
    If Me.chk_session_closed.Checked Then
      If String.IsNullOrEmpty(m_report_status) Then
        m_report_status = Me.chk_session_closed.Text
      Else
        m_report_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353) ' All
      End If
    End If

    ' Date
    If Me.dtp_from.Checked Or Me.dtp_to.Checked Then

      ' Type
      If Me.opt_opening_session.Checked Then
        m_report_date_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2101)
      Else
        m_report_date_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2102)
      End If

      ' From
      If Me.dtp_from.Checked Then
        m_report_from = Me.dtp_from.Value
        m_report_from_text = GUI_FormatDate(Me.dtp_from.Value, _
                                     ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                     ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      End If

      ' To
      If Me.dtp_to.Checked Then
        m_report_to = Me.dtp_to.Value
        m_report_to_text = GUI_FormatDate(Me.dtp_to.Value, _
                                     ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                     ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      End If
    End If

    ' Location filter
    If Me.cmb_area.SelectedIndex > 0 Then
      m_report_area_name = Me.cmb_area.TextValue
      m_report_zone_name = Me.ef_smoking.Value
      m_report_bank_name = Me.cmb_bank.TextValue
    End If

    ' Gaming Table Types filter
    If Me.opt_all_types.Checked Then
      m_report_types = Me.opt_all_types.Text
    ElseIf Me.opt_several_types.Checked Then
      m_report_types = GetTypeIdListSelected(True)
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region

  'FOS  25-FEB-2015
  Private Sub AddTotalRow()
    Dim _idx_total_row As Integer
    Dim _idx_columns As Integer
    Dim _first_group As Boolean

    _idx_columns = 0
    _first_group = True
    _idx_total_row = Me.Grid.AddRow()

    ' Label - TOTAL
    Me.Grid.Row(_idx_total_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

    ' Session Name
    Me.Grid.Cell(_idx_total_row, GRID_COLUMN_SESSION_NAME).Value = ""
    ' Sentinel
    Me.Grid.Cell(_idx_total_row, GRID_COLUMN_SENTINEL).Value = ""
    ' Status
    Me.Grid.Cell(_idx_total_row, GRID_COLUMN_STATUS).Value = ""
    ' Table Name
    Me.Grid.Cell(_idx_total_row, GRID_COLUMN_TABLE_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1047) & ":"
    Me.Grid.Cell(_idx_total_row, GRID_COLUMN_TABLE_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    ' Table Type
    Me.Grid.Cell(_idx_total_row, GRID_COLUMN_TABLE_TYPE).Value = ""
    ' Cashier user
    Me.Grid.Cell(_idx_total_row, GRID_COLUMN_CASHIER_USER).Value = ""
    ' Opening Date
    Me.Grid.Cell(_idx_total_row, GRID_COLUMN_OPENING_DATE).Value = ""
    ' Closing Date
    Me.Grid.Cell(_idx_total_row, GRID_COLUMN_CLOSING_DATE).Value = ""

    For _idx_currency_total As Integer = 0 To m_currencies_accepted.Length - 1


      ' Total Chips Purchases
      Me.Grid.Cell(_idx_total_row, GRID_COLUMN_CHIPS_PURCHASES + _idx_columns + IIf(_first_group, 0, COLUMNS_TO_CHIPS_GROUP)).Value = GUI_FormatCurrency(m_chips_purchase(_idx_currency_total), _
                                                                                         2, _
                                                                                         ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, _
                                                                                         False)

      _idx_columns = IIf(_first_group, 0, _idx_columns + COLUMNS_TO_CHIPS_GROUP)

      ' CHIPS SALES
      Me.Grid.Cell(_idx_total_row, GRID_COLUMN_CHIPS_SALES + _idx_columns).Value = GUI_FormatCurrency(m_chips_sales(_idx_currency_total), _
                                                                                       2, _
                                                                                       ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, _
                                                                                       False)

      ' CHIPS INITIAL BALANCE
      Me.Grid.Cell(_idx_total_row, GRID_COLUMN_CHIPS_INITIAL_BALANCE + _idx_columns).Value = GUI_FormatCurrency(m_initial_chips_balance(_idx_currency_total), _
                                                                                       2, _
                                                                                       ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, _
                                                                                       False)
      ' CHIPS FILLS BALANCE
      Me.Grid.Cell(_idx_total_row, GRID_COLUMN_CHIPS_FILLS_BALANCE + _idx_columns).Value = GUI_FormatCurrency(m_fills_chips_balance(_idx_currency_total), _
                                                                                       2, _
                                                                                       ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, _
                                                                                       False)
      ' CHIPS CREDITS BALANCE
      Me.Grid.Cell(_idx_total_row, GRID_COLUMN_CHIPS_CREDITS_BALANCE + _idx_columns).Value = GUI_FormatCurrency(m_credits_chips_balance(_idx_currency_total), _
                                                                                         2, _
                                                                                         ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, _
                                                                                         False)

      ' CHIPS FINAL BALANCE
      Me.Grid.Cell(_idx_total_row, GRID_COLUMN_CHIPS_FINAL_BALANCE + _idx_columns).Value = GUI_FormatCurrency(m_final_chips_balance(_idx_currency_total), _
                                                                                         2, _
                                                                                         ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, _
                                                                                         False)



      _first_group = False
    Next



    For _idx_currency_total As Integer = 0 To m_currencies_accepted.Length - 1
      If GamingTableBusinessLogic.IsEnableManualConciliation Then
        'Drop

        Me.Grid.Cell(_idx_total_row, GRID_COLUMN_DROP + _idx_columns).Value = GUI_FormatCurrency(m_drop_amount(_idx_currency_total), _
                                                                                   2, _
                                                                                   ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, _
                                                                                   False)
      End If

      ' Copy dealer validated amount
      ' Hide colum with 0 value
      If m_copy_dealer_validated_amount(_idx_currency_total) = 0 Then
        Me.Grid.Column(GRID_COLUMN_COPY_DEALER_VALIDATED_AMOUNT + _idx_columns).Width = 0
      Else
        Me.Grid.Column(GRID_COLUMN_COPY_DEALER_VALIDATED_AMOUNT + _idx_columns).Width = GRID_COLUMN_COPY_DEALER_VALIDATED_AMOUNT_WIDTH
      End If

      Me.Grid.Cell(_idx_total_row, GRID_COLUMN_COPY_DEALER_VALIDATED_AMOUNT + _idx_columns).Value = GUI_FormatCurrency(m_copy_dealer_validated_amount(_idx_currency_total), _
                                                                                 2, _
                                                                                 ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, _
                                                                                 False)


      _idx_columns = IIf((m_currencies_accepted.Length - 1 = _idx_currency_total), _idx_columns, _idx_columns + COLUMNS_TO_DROP_GROUP)

    Next


    For _idx_currency_total As Integer = 0 To m_currencies_accepted.Length - 1
      ' TIPS

      Me.Grid.Cell(_idx_total_row, GRID_COLUMN_TIPS + _idx_columns).Value = GUI_FormatCurrency(m_chips_tips(_idx_currency_total), _
                                                                                         2, _
                                                                                         ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, _
                                                                                         False)

      _idx_columns = IIf((m_currencies_accepted.Length - 1 = _idx_currency_total), _idx_columns, _idx_columns + COLUMNS_TO_TIPS_GROUP)

    Next

    ' CHIPS BALANCE
    For _idx_currency_total As Integer = 0 To m_currencies_accepted.Length - 1
      Me.Grid.Cell(_idx_total_row, GRID_COLUMN_CHIPS_BALANCE + _idx_columns).Value = GUI_FormatCurrency(m_chips_balance(_idx_currency_total), _
                                                                                         2, _
                                                                                         ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, _
                                                                                         False)

      ' CURRENCY BALANCE
      Me.Grid.Cell(_idx_total_row, GRID_COLUMN_CASH_BALANCE + _idx_columns).Value = GUI_FormatCurrency(m_currency_balance(_idx_currency_total), _
                                                                                         2, _
                                                                                         ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, _
                                                                                         False)

      ' TOTAL BALANCE
      Me.Grid.Cell(_idx_total_row, GRID_COLUMN_TOTAL_BALANCE + _idx_columns).Value = GUI_FormatCurrency(m_total_balance(_idx_currency_total), _
                                                                                         2, _
                                                                                         ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, _
                                                                                         False)

      _idx_columns = IIf((m_currencies_accepted.Length - 1 = _idx_currency_total), _idx_columns, _idx_columns + COLUMNS_TO_BALANCE_CHIPS)
    Next

    ' CLIENT VISITS
    Me.Grid.Cell(_idx_total_row, GRID_COLUMN_VISITS + _idx_columns).Value = GUI_FormatNumber(m_visits, 0)

    ' AREA
    Me.Grid.Cell(_idx_total_row, GRID_COLUMN_AREA + _idx_columns).Value = ""

    ' BANK
    Me.Grid.Cell(_idx_total_row, GRID_COLUMN_BANK + _idx_columns).Value = ""

  End Sub



#End Region ' Private Functions


#Region " Events "

  Private Sub dg_types_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer) Handles dg_filter.CellDataChangedEvent
    Dim _idx As Integer
    Dim _all_checkeds As Boolean

    m_several_click = False
    _all_checkeds = True

    For _idx = 0 To dg_filter.NumRows - 1
      If Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED Then
        _all_checkeds = False
        Exit For
      End If
    Next

    If _all_checkeds Then
      opt_several_types.Checked = False
      opt_all_types.Checked = True
    Else
      opt_several_types.Checked = True
      opt_all_types.Checked = False
    End If

  End Sub ' dg_types_CellDataChangedEvent


  Private Sub opt_all_types_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_all_types.CheckedChanged
    Dim _idx As Integer
    Dim _check_value As String

    If opt_all_types.Checked Then
      _check_value = uc_grid.GRID_CHK_CHECKED
    Else
      _check_value = uc_grid.GRID_CHK_UNCHECKED
    End If

    If opt_all_types.Checked Or m_several_click Then
      For _idx = 0 To dg_filter.NumRows - 1
        Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = _check_value
      Next
    End If

    m_several_click = True

  End Sub

#End Region ' Events

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  '
  Protected Function GetTableTypes() As DataTable

    Dim _str_sql As StringBuilder

    _str_sql = New StringBuilder()

    _str_sql.AppendLine("   SELECT    GTT_GAMING_TABLE_TYPE_ID  ")
    _str_sql.AppendLine("           , GTT_NAME   				        ")
    _str_sql.AppendLine("     FROM    GAMING_TABLES_TYPES 	    ")
    _str_sql.AppendLine("    WHERE    GTT_ENABLED = 1           ")

    Return GUI_GetTableUsingSQL(_str_sql.ToString(), 5000)

  End Function ' GetTableTypes 


End Class