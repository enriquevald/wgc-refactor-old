<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_area_island_edit
  Inherits GUI_Controls.frm_base_print

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_smoking = New System.Windows.Forms.GroupBox
    Me.chk_smoking_no = New System.Windows.Forms.CheckBox
    Me.chk_smoking_yes = New System.Windows.Forms.CheckBox
    Me.ef_area_name = New GUI_Controls.uc_entry_field
    Me.ef_island_name = New GUI_Controls.uc_entry_field
    Me.uc_pr_list = New GUI_Controls.uc_provider
    Me.ef_terminal_name = New GUI_Controls.uc_entry_field
    Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
    Me.tv_areas = New System.Windows.Forms.TreeView
    Me.tv_providers = New System.Windows.Forms.TreeView
    Me.panel_grids.SuspendLayout()
    Me.panel_filter.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_smoking.SuspendLayout()
    Me.SplitContainer1.Panel1.SuspendLayout()
    Me.SplitContainer1.Panel2.SuspendLayout()
    Me.SplitContainer1.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_grids
    '
    Me.panel_grids.Controls.Add(Me.SplitContainer1)
    Me.panel_grids.Location = New System.Drawing.Point(4, 220)
    Me.panel_grids.Size = New System.Drawing.Size(950, 441)
    Me.panel_grids.TabIndex = 1
    Me.panel_grids.Controls.SetChildIndex(Me.SplitContainer1, 0)
    Me.panel_grids.Controls.SetChildIndex(Me.panel_buttons, 0)
    '
    'panel_buttons
    '
    Me.panel_buttons.Location = New System.Drawing.Point(862, 0)
    Me.panel_buttons.Size = New System.Drawing.Size(88, 441)
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.ef_terminal_name)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Controls.Add(Me.ef_island_name)
    Me.panel_filter.Controls.Add(Me.ef_area_name)
    Me.panel_filter.Controls.Add(Me.gb_smoking)
    Me.panel_filter.Size = New System.Drawing.Size(950, 193)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_smoking, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_area_name, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_island_name, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_terminal_name, 0)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Location = New System.Drawing.Point(4, 197)
    Me.pn_separator_line.Size = New System.Drawing.Size(950, 23)
    Me.pn_separator_line.TabIndex = 1
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(950, 4)
    '
    'gb_smoking
    '
    Me.gb_smoking.Controls.Add(Me.chk_smoking_no)
    Me.gb_smoking.Controls.Add(Me.chk_smoking_yes)
    Me.gb_smoking.Location = New System.Drawing.Point(146, 43)
    Me.gb_smoking.Name = "gb_smoking"
    Me.gb_smoking.Size = New System.Drawing.Size(109, 64)
    Me.gb_smoking.TabIndex = 2
    Me.gb_smoking.TabStop = False
    Me.gb_smoking.Text = "xSmoking"
    '
    'chk_smoking_no
    '
    Me.chk_smoking_no.AutoSize = True
    Me.chk_smoking_no.Location = New System.Drawing.Point(28, 42)
    Me.chk_smoking_no.Name = "chk_smoking_no"
    Me.chk_smoking_no.Size = New System.Drawing.Size(41, 17)
    Me.chk_smoking_no.TabIndex = 1
    Me.chk_smoking_no.Text = "No"
    '
    'chk_smoking_yes
    '
    Me.chk_smoking_yes.AutoSize = True
    Me.chk_smoking_yes.Location = New System.Drawing.Point(28, 22)
    Me.chk_smoking_yes.Name = "chk_smoking_yes"
    Me.chk_smoking_yes.Size = New System.Drawing.Size(46, 17)
    Me.chk_smoking_yes.TabIndex = 0
    Me.chk_smoking_yes.Text = "Yes"
    '
    'ef_area_name
    '
    Me.ef_area_name.DoubleValue = 0
    Me.ef_area_name.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_area_name.IntegerValue = 0
    Me.ef_area_name.IsReadOnly = False
    Me.ef_area_name.Location = New System.Drawing.Point(5, 12)
    Me.ef_area_name.Name = "ef_area_name"
    Me.ef_area_name.Size = New System.Drawing.Size(250, 25)
    Me.ef_area_name.SufixText = "Sufix Text"
    Me.ef_area_name.SufixTextVisible = True
    Me.ef_area_name.TabIndex = 0
    Me.ef_area_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_area_name.TextValue = ""
    Me.ef_area_name.TextWidth = 70
    Me.ef_area_name.Value = ""
    '
    'ef_island_name
    '
    Me.ef_island_name.DoubleValue = 0
    Me.ef_island_name.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_island_name.IntegerValue = 0
    Me.ef_island_name.IsReadOnly = False
    Me.ef_island_name.Location = New System.Drawing.Point(5, 119)
    Me.ef_island_name.Name = "ef_island_name"
    Me.ef_island_name.Size = New System.Drawing.Size(250, 25)
    Me.ef_island_name.SufixText = "Sufix Text"
    Me.ef_island_name.SufixTextVisible = True
    Me.ef_island_name.TabIndex = 1
    Me.ef_island_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_island_name.TextValue = ""
    Me.ef_island_name.TextWidth = 70
    Me.ef_island_name.Value = ""
    '
    'uc_pr_list
    '
    Me.uc_pr_list.Location = New System.Drawing.Point(362, 3)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(394, 188)
    Me.uc_pr_list.TabIndex = 11
    '
    'ef_terminal_name
    '
    Me.ef_terminal_name.DoubleValue = 0
    Me.ef_terminal_name.IntegerValue = 0
    Me.ef_terminal_name.IsReadOnly = False
    Me.ef_terminal_name.Location = New System.Drawing.Point(5, 150)
    Me.ef_terminal_name.Name = "ef_terminal_name"
    Me.ef_terminal_name.Size = New System.Drawing.Size(250, 24)
    Me.ef_terminal_name.SufixText = "Sufix Text"
    Me.ef_terminal_name.SufixTextVisible = True
    Me.ef_terminal_name.TabIndex = 12
    Me.ef_terminal_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_terminal_name.TextValue = ""
    Me.ef_terminal_name.TextWidth = 70
    Me.ef_terminal_name.Value = ""
    '
    'SplitContainer1
    '
    Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
    Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
    Me.SplitContainer1.Name = "SplitContainer1"
    '
    'SplitContainer1.Panel1
    '
    Me.SplitContainer1.Panel1.Controls.Add(Me.tv_areas)
    Me.SplitContainer1.Panel1.Padding = New System.Windows.Forms.Padding(10, 10, 3, 10)
    '
    'SplitContainer1.Panel2
    '
    Me.SplitContainer1.Panel2.Controls.Add(Me.tv_providers)
    Me.SplitContainer1.Panel2.Padding = New System.Windows.Forms.Padding(10)
    Me.SplitContainer1.Size = New System.Drawing.Size(950, 441)
    Me.SplitContainer1.SplitterDistance = 431
    Me.SplitContainer1.TabIndex = 9
    '
    'tv_areas
    '
    Me.tv_areas.AllowDrop = True
    Me.tv_areas.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.tv_areas.LabelEdit = True
    Me.tv_areas.Location = New System.Drawing.Point(15, 10)
    Me.tv_areas.Name = "tv_areas"
    Me.tv_areas.Size = New System.Drawing.Size(390, 421)
    Me.tv_areas.TabIndex = 0
    '
    'tv_providers
    '
    Me.tv_providers.AllowDrop = True
    Me.tv_providers.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.tv_providers.Location = New System.Drawing.Point(25, 10)
    Me.tv_providers.Margin = New System.Windows.Forms.Padding(10)
    Me.tv_providers.Name = "tv_providers"
    Me.tv_providers.Size = New System.Drawing.Size(390, 421)
    Me.tv_providers.TabIndex = 1
    '
    'frm_area_island_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(958, 665)
    Me.Name = "frm_area_island_edit"
    Me.Text = "frm_area_island_edit"
    Me.panel_grids.ResumeLayout(False)
    Me.panel_filter.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_smoking.ResumeLayout(False)
    Me.gb_smoking.PerformLayout()
    Me.SplitContainer1.Panel1.ResumeLayout(False)
    Me.SplitContainer1.Panel2.ResumeLayout(False)
    Me.SplitContainer1.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_smoking As System.Windows.Forms.GroupBox
  Friend WithEvents chk_smoking_no As System.Windows.Forms.CheckBox
  Friend WithEvents chk_smoking_yes As System.Windows.Forms.CheckBox
  Friend WithEvents ef_area_name As GUI_Controls.uc_entry_field
  Friend WithEvents ef_island_name As GUI_Controls.uc_entry_field
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents ef_terminal_name As GUI_Controls.uc_entry_field
  Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
  Friend WithEvents tv_areas As System.Windows.Forms.TreeView
  Friend WithEvents tv_providers As System.Windows.Forms.TreeView
End Class
