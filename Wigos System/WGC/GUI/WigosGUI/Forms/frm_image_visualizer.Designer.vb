﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_image_visualizer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.pb_image = New System.Windows.Forms.PictureBox()
    CType(Me.pb_image, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'pb_image
    '
    Me.pb_image.Dock = System.Windows.Forms.DockStyle.Fill
    Me.pb_image.Location = New System.Drawing.Point(0, 0)
    Me.pb_image.Name = "pb_image"
    Me.pb_image.Size = New System.Drawing.Size(275, 469)
    Me.pb_image.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
    Me.pb_image.TabIndex = 0
    Me.pb_image.TabStop = False
    '
    'frm_image_visualizer
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(275, 469)
    Me.Controls.Add(Me.pb_image)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Name = "frm_image_visualizer"
    Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
    Me.Text = "Image visualizer"
    CType(Me.pb_image, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents pb_image As System.Windows.Forms.PictureBox
End Class
