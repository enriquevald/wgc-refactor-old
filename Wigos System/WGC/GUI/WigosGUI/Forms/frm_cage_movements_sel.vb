'-------------------------------------------------------------------
' Copyright � 2007 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_cage_movements_sel
' DESCRIPTION:   This screen allows to view cage_movements:
'                           - between two dates 
'                           - for one or all cashiers
'                           - for one or all users
'                           - for one or all movements types
'                           - for one or all cashier sessions
' AUTHOR:        Francesc Borrell
' CREATION DATE: 07-NOV-2013
'
' REVISION HISTORY:
'
' Date         Author     Description
' -----------  ------     ---------------------------------------------------------------------------------------------------------------------------
' 07-NOV-2013  FBA        Initial version.
' 25-FEB-2014  ICS & FBA  Added support for terminal collection with cage movement.
' 25-FEB-2014  ICS        Fixed bug WIG-678: Super user can't recollect.
' 13-MAR-2014  LEM        Added "Order by" options.
' 20-MAR-2014  JAB        Added functionality to print cage movement details.
' 25-MAR-2014  FBA        Fixed bug WIG-763: When option button Cashier selected must show only cashiers and not gaming tables.
' 03-APR-2014  FBA        Added control for money requests from cash desk.
' 21-MAY-2014  JAB        Added new columns to cancellation data.
' 27-MAY-2014  HBB        Fixed bug WIG-949 In cage movement, if an open or close cage movement is selected the button detail must to be disabled
' 04-SEP-2014  LMRS & JC Added functionality to see reopen sessions.
' 05-SEP-2014  AMF        Progressive Jackpot
' 08-OCT-2014  RRR        Changed cmb_cashier to allow select only custom source/tragets
' 25-NOV-2014  AMF        Fixed bug WIG-1732: Error with Gaming Tables disabled
' 19-FEB-2015  DCS        Fixed Bug WIG-2067: Don't show "Request from cashier" with all "Source/Destination" filters
' 12-MAR-2015  YNM        Fixed Bug WIG-1930: Wrong format on cage session name in export to Excel
' 16-APR-2015  YNM        Fixed Bug WIG-2204: Include Chips in the report
' 24-APR-2015  JBC        Fixed Bug WIG-2245: Request from cashier only for sendings.
' 24-APR-2015  JBC        Fixed Bug WIG-2244: Now open cage_movements by alert disable all filters.
' 09-JUL-2015  YNM        TFS-2074: Alarm any pending collection
' 29-JUL-2015  YNM        TFS-2076: Monitor Mode Configuration
' 07-AUG-2015  YNM        TFS-2076: Monitor Mode Configuration
' 20-AUG-2015  DCS        TFS-2829: Filter movement TYPE doesn't  work properly in combination with filter source/destiny
' 04-SEP-2015  YNM        Fixed Bug TFS-4207: exclude terminals movements
' 02-FEB-2016  DDS        Bug 8894:GUI - Pop-up with 'no data' message when enter the form
' 14-MAR-2016  FAV        Fixed Bug 10373, 10376, 10365: Fixed value showed in Deposits and Withdrawals
' 04-APR-2016  ETP        Fixed Bug 
' 24-MAY-2016  DHA        Product Backlog Item 9848: drop box gaming tables
' 16-JUN-2016  RAB        Product Backlog Item 9853: GamingTables (Phase 3): Fixed Bank (Phase 1)
' 01-JUL-2016  ETP        Fixed Bug 14914: Movimientos de b�veda: No se contabiliza el estado "(Cancelado) Pendiente recaudar"
' 12-JUL-2016  RGR        Product Backlog Item 14626: movements cage: Update Existing screen with total 
' 28-JUN-2016  DHA        Product Backlog Item 15903: Gaming tables (Fase 5): Drop box collection on Cashier
' 27-SEP-2016  FAV        Fixed Bug 8816: Allow request to cage without cage opened.
' 28-SEP-2016  DHA        PBI 17747: cancel gaming tables operations
' 01-DEC-2016  AMF        Bug 20804:Movimientos de b�veda por jornada: error en las cabeceras
' 23-MAR-2017  FOS        Bug 25853:Error showing buttons
' 09-FEN-2017  JML        PBI 24378: Reabrir sesiones de caja y mesas de juego: rehacer reapertura y correcci�n de Bugs
' 13-JUL-2017  RAB        Bug 28654: WIGOS-3561 Wrong column in Cage Movement screen
' 18-JUL-2017  JML        Bug 28817:WIGOS-3701 Missing chips information when collecting them from Cage Movements screen
' 08-AUG-2017  EOR        Bug 29266: WIGOS-3988 - Cash cage movements screen version 20150930q02
' 22-AUG-2017  AMF        Bug 29418:[WIGOS-4263] Most recent cage movements appear at the bottom of the list instead of appearing at the top by default
' 27-NOV-2017  JML        Bug 30908:WIGOS-3561 Wrong column in Cage Movement screen
' 29-NOV-2017  DPC        Bug 30959:[WIGOS-6959]: Cage: cage movements error "Not connected to the database" is displayed after the Seach.
' 14-DEC-2017  DHA        Bug 31089:WIGOS-7195 La b�veda no refleja los montos de los movimientos realizados.
' 13-FEB-2018  DHA        Bug 31509:WIGOS-8162 Egasa Chile Arica: Reporte de b�veda no cuadra con los movimientos realizados
' 26-MAR-2018  AGS        Bug 32059:WIGOS-9422 Database conection error in the screen "Movimientos de boveda"
' 28-MAR-2018  AGS        Bug 32080:WIGOS-9529 Database access error when editing a cash cage movement
' 05-APR-2018  AGS        Fixed Bug 32080:WIGOS-9529 Database access error when editing a cash cage movement
' 03-MAY-2018  JML        Fixed Bug 32514:WIGOS-10640 Cash cage movements: Incorrect visualization of remainder amount
' 16-JUN-2018  MGG        Bug 33101:WIGOS-13000 GUI - Cage movement details: Cancel columns have information when movement is not in a cancel state
' 16-JUN-2018  AGS        Bug 33095:WIGOS-13001 GUI - Cage movements: Amounts are different from the cage details screen and the cage movements screen
' 23-JUN-2018  DPC        Bug 33699:[WIGOS-13820]: Mismatch amount collected when closing Redemption Kiosk session
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common

Public Class frm_cage_movements_sel
  Inherits frm_base_sel

#Region "Enums"

  Public Enum MODE_EDIT
    NONE = 0
    SEND = 1
    COLLECTION = 2
    MONEY_REQUEST = 3
  End Enum

#End Region ' Enums

#Region " Constants "

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_MOVEMENT_ID As Integer = 1
  Private Const GRID_COLUMN_MOVEMENT_DATETIME As Integer = 2
  Private Const GRID_COLUMN_TERMINAL_CAGE_ID As Integer = 3
  Private Const GRID_COLUMN_TERMINAL_CAGE_NAME As Integer = 4
  Private Const GRID_COLUMN_USER_CAGE_NAME As Integer = 5
  Private Const GRID_COLUMN_TERMINAL_CASHIER_ID As Integer = 6
  Private Const GRID_COLUMN_TERMINAL_CASHIER_NAME As Integer = 7
  Private Const GRID_COLUMN_USER_CASHIER_NAME As Integer = 8
  Private Const GRID_COLUMN_OPERATION_TYPE As Integer = 9
  Private Const GRID_COLUMN_OPERATION_TYPE_ID As Integer = 10
  Private Const GRID_COLUMN_STATUS As Integer = 11
  Private Const GRID_COLUMN_STATUS_ID As Integer = 12

  Private Const GRID_COLUMN_GUI_USER_ID As Integer = 13
  Private Const GRID_COLUMN_GU_FULL_NAME As Integer = 14
  Private Const GRID_COLUMN_WORKING_DAY As Integer = 15
  Private Const GRID_COLUMN_GAMBLING_TABLE_ID As Integer = 16
  Private Const GRID_COLUMN_TERMINAL_TYPE As Integer = 17

  Private Const GRID_COLUMN_USER_CAGE_NAME_LAST_MODIFICATION As Integer = 18
  Private Const GRID_COLUMN_MOVEMENT_DATETIME_LAST_MODIFICATION As Integer = 19

  Private Const GRID_COLUMN_RELATED_ID = 20
  Private Const GRID_COLUMN_GAMING_TABLE_SESSION_ID As Integer = 21

  Private Const GRID_COLUMNS As Integer = 22
  Private Const GRID_HEADER_ROWS As Integer = 1
  Private Const GRID_HEADER_ROWS_FROM_SESSIONS As Integer = 2

  ' Width
  Private Const GRID_WIDTH_INDEX As Integer = 150

#If DEBUG Then
  Private Const GRID_WIDTH_COLUMN_MOVEMENT_ID As Integer = 800
  Private Const GRID_WIDTH_COLUMN_GUI_USER_ID As Integer = 800
#Else
  Private Const GRID_WIDTH_COLUMN_MOVEMENT_ID As Integer = 0
  Private Const GRID_WIDTH_COLUMN_GUI_USER_ID As Integer = 0
#End If

  Private Const GRID_WIDTH_COLUMN_MOVEMENT_DATETIME As Integer = 2000
  Private Const GRID_WIDTH_COLUMN_MOVEMENT_DATETIME_LAST_MODIFICATION As Integer = 2000
  Private Const GRID_WIDTH_COLUMN_TERMINAL_CAGE_ID As Integer = 0
  Private Const GRID_WIDTH_COLUMN_TERMINAL_CAGE_NAME As Integer = 0
  Private Const GRID_WIDTH_COLUMN_USER_CAGE_NAME As Integer = 2000
  Private Const GRID_WIDTH_COLUMN_USER_CAGE_NAME_LAST_MODIFICATION As Integer = 1800
  Private Const GRID_WIDTH_COLUMN_TERMINAL_CASHIER_ID As Integer = 0
  Private Const GRID_WIDTH_COLUMN_TERMINAL_CASHIER_NAME As Integer = 2300
  Private Const GRID_WIDTH_COLUMN_USER_CASHIER_NAME As Integer = 2400
  Private Const GRID_WIDTH_COLUMN_OPERATION As Integer = 3800
  Private Const GRID_WIDTH_COLUMN_OPERATION_ID As Integer = 0
  Private Const GRID_WIDTH_COLUMN_STATUS As Integer = 3950
  Private Const GRID_WIDTH_COLUMN_STATUS_ID As Integer = 0
  Private Const GRID_WIDTH_COLUMN_GU_FULL_NAME As Integer = 3950
  Private Const GRID_WIDTH_COLUMN_CGS_CURRENCIES As Integer = 1550
  Private Const GRID_WIDTH_COLUMN_GAMBLING_TABLE_ID As Integer = 0
  Private Const GRID_WIDTH_COLUMN_RELATED_ID = 0
  Private Const GRID_WIDTH_COLUMN_WORKING_DAY As Integer = 1700

  ' QUERY columns
  Private Const SQL_COLUMN_ID As Integer = 0
  Private Const SQL_COLUMN_TERMINAL_CAGE_NAME As Integer = 1
  Private Const SQL_COLUMN_USER_CAGE_NAME As Integer = 2
  Private Const SQL_COLUMN_TERMINAL_CASHIER_NAME As Integer = 3
  Private Const SQL_COLUMN_USER_CASHIER_NAME As Integer = 4
  Private Const SQL_COLUMN_OPERATION_TYPE As Integer = 5
  Private Const SQL_COLUMN_STATUS As Integer = 6
  Private Const SQL_COLUMN_CUSTOM_NAME As Integer = 7
  Private Const SQL_COLUMN_DATETIME As Integer = 8
  Private Const SQL_COLUMN_GAMING_TABLE_ID As Integer = 9
  Private Const SQL_COLUMN_GUI_USER_ID As Integer = 10
  Private Const SQL_COLUMN_GU_FULL_NAME As Integer = 11
  Private Const SQL_COLUMN_GAMBLING_TABLE_ID As Integer = 12
  Private Const SQL_COLUMN_TERMINAL_TYPE As Integer = 13
  Private Const SQL_COLUMN_USER_CAGE_NAME_LAST_MODIFICATION As Integer = 14
  Private Const SQL_COLUMN_DATETIME_LAST_MODIFICATION As Integer = 15
  Private Const SQL_COLUMN_PROGRESSIVE_PROVISION_AMOUNT As Integer = 16
  Private Const SQL_COLUMN_PROGRESSIVE_AWARDED_AMOUNT As Integer = 17
  Private Const SQL_COLUMN_RELATED_ID As Integer = 18
  Private Const SQL_COLUMN_WORKING_DAY As Integer = 19
  Private Const SQL_COLUMN_GAMING_TABLE_SESSION_ID As Integer = 20

  Private Const FORM_DB_MIN_VERSION As Short = 15

  ' Grid counters
  Private Const COUNTER_SENT As Integer = 1
  Private Const COUNTER_OK As Integer = 2
  Private Const COUNTER_DENOMINATION_IMBALANCE As Integer = 3
  Private Const COUNTER_TOTAL_IMBALANCE As Integer = 4
  Private Const COUNTER_CANCELED As Integer = 5
  Private Const COUNTER_OPEN_CLOSE_CAGE As Integer = 6

  ' m_movements_totals columns
  Private Const SQL_COLUMN_CGM_MOVEMENT_ID As Integer = 0
  Private Const SQL_COLUMN_CMD_ISO_CODE As Integer = 1
  Private Const SQL_COLUMN_CMD_DEPOSITS As Integer = 2
  Private Const SQL_COLUMN_CMD_WITHDRAWALS As Integer = 3
  Private Const SQL_COLUMN_CMD_DIFFERENCE As Integer = 4

  ' Timer interval to refresh alarms (in milliseconds)
  Private Const MONITOR_REFRESH_TIME As Integer = 5000

  Private Const MAX_ROWS_IN_MONITOR_MODE As Integer = 100
  Private Const GAMING_TABLE_SYSTEM_USER As String = "SYS-GamingTables"
#End Region ' Constants

#Region " Members "

  Private m_total_columns As Integer
  Private m_init_date_from As Date
  Private m_init_date_to As Date

  ' For currency exchange dynamic columns
  Private m_currency_exchange_iso_codes As List(Of String)
  Private m_currency_exchange_iso_description As List(Of String)
  Private m_movements_totals As DataTable

  ' For report filters 
  Private m_date_from As String
  Private m_date_to As String
  Private m_operation_type As String
  Private m_status As String
  Private m_cashier_name As String
  Private m_user_name As String
  Private m_cage_session As String
  Private m_pending As String
  Private m_order_by As String
  Private m_filter_date_type As String

  'color counter
  Private m_sent_rows As Integer
  Private m_ok_rows As Integer
  Private m_denomination_imbalance_rows As Integer
  Private m_total_imbalance_rows As Integer
  Private m_canceled_rows As Integer
  Private m_open_close_cage_rows As Integer

  Private m_session_id As Int64
  Private m_cls_cage_control As CLASS_CAGE_CONTROL

  Private m_cashier_session_id As Int64
  Private m_cashier_session_name As String

  Private m_opened As Boolean

  Public m_mode_edit As MODE_EDIT = MODE_EDIT.NONE

  Public m_timer As System.Timers.Timer

  Dim m_first_time As Boolean

  Public Event m_dispose_pending_request()

  Private m_filters_read_only As Boolean

  Private m_request_operation_pos As Integer

  Private m_monitor_enabled As Boolean = False

  Private m_table As DataTable

  Private m_selected_row As Integer

  Private m_iso_codes_sql As IDictionary(Of Integer, String)

#End Region ' Members

#Region " Overrides "



  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CAGE

    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.m_iso_codes_sql = New Dictionary(Of Integer, String)

    ' Not from a specific session
    Me.m_session_id = -1
    Me.m_opened = False

    If m_mode_edit = MODE_EDIT.COLLECTION Then
      Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4377)
    Else
      Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2899)
    End If

    ' Buttons
    Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(474)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Enabled = Not GLB_CurrentUser.IsSuperUser
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(1)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2)

    ' Date
    Me.opt_movement_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2466)
    Me.opt_working_day.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5723)
    Me.gb_date.Text = GLB_NLS_GUI_INVOICING.GetString(201)
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Terminals 
    Me.gb_cashier.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3392)
    Me.opt_all_cashiers.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353)
    Me.rb_terminal.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5030)
    Me.rb_custom.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3393)
    Me.rb_gaming_table.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4396)
    Me.rb_gaming_table.Enabled = GamingTableBusinessLogic.IsGamingTablesEnabled
    Me.rb_cashier.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1818)

    ' Users 
    Me.gb_user.Text = GLB_NLS_GUI_INVOICING.GetString(220)
    Me.opt_one_user.Text = GLB_NLS_GUI_INVOICING.GetString(218)
    Me.opt_all_users.Text = GLB_NLS_GUI_INVOICING.GetString(219)
    Me.chk_show_all.Text = GLB_NLS_GUI_CONFIGURATION.GetString(90)

    ' Operation
    Me.gb_operation.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(254)
    Me.opt_from_cashier.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4359)
    Me.opt_from_cage.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4358)
    Me.opt_all.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353)

    ' Cage Session
    Me.gb_sessions.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3388)       ' Sesi�n de B�veda
    Me.opt_one_session.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(286)    ' Una
    Me.opt_all_sessions.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(287)   ' Todas

    ' Order by
    Me.gb_order_by.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4733)             'Order by
    Me.opt_by_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4734)             'Date
    Me.opt_by_cashier_session.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4735)  'Cashier session

    ' Status
    Me.uc_checked_list_status.GroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(262)

    'Status Add
    Call FillCheckedlistFilterGrid(True, False)
    Call Me.uc_checked_list_status.ResizeGrid()
    Call Me.uc_checked_list_status.ColumnWidth(uc_checked_list.GRID_COLUMN_DESC, 4050)

    'Mode
    Me.gb_mode.Text = GLB_NLS_GUI_ALARMS.GetString(203)
    Me.opt_monitor_mode.Text = GLB_NLS_GUI_ALARMS.GetString(204)
    Me.opt_history_mode.Text = GLB_NLS_GUI_ALARMS.GetString(205)

    'Timer Monitor
    Me.tmr_monitor.Interval = MONITOR_REFRESH_TIME

    ' Grid
    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

    ' Set combo with Users (only users with permission to fillout in cashier)
    Call SetCombo(Me.cmb_user, "SELECT GU_USER_ID,  GU_FULL_NAME + ' - [' + GU_USERNAME + ']' AS GU_FULL_NAME FROM GUI_USERS GU INNER JOIN GUI_USER_PROFILES GUP ON GU.GU_PROFILE_ID = GUP.GUP_PROFILE_ID " & _
                " INNER JOIN GUI_PROFILE_FORMS GPF ON GUP.GUP_PROFILE_ID = GPF.GPF_PROFILE_ID WHERE GU_BLOCK_REASON = " & WSI.Common.GUI_USER_BLOCK_REASON.NONE & _
                " AND GU_USER_TYPE = " & WSI.Common.GU_USER_TYPE.USER & " AND GPF.GPF_GUI_ID = " & WSI.Common.ENUM_GUI.CASHIER & _
                " AND GPF.GPF_FORM_ID = " & ENUM_CASHIER_FORM.FORM_MAIN & " AND GPF.GPF_READ_PERM = 1 ORDER BY GU_FULL_NAME ")

    Call SetComboCageSessions(Me.cmb_sessions)
    Call SetDefaultCageSession()

    Me.cmb_user.Enabled = False
    Me.chk_show_all.Enabled = False

    If (m_cashier_session_id > 0) Then
      Me.tf_cashier_session.Location = New System.Drawing.Point(250, 215)
      Me.tf_cashier_session.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4735)
      Me.tf_cashier_session.Value = m_cashier_session_name

      Me.tf_cashier_session.Visible = True
      Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False

      If Me.Permissions.Read Then
        Call Application.DoEvents()
        System.Threading.Thread.Sleep(100)
        Call Application.DoEvents()

        Me.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)
      End If
    Else
      Me.tf_cashier_session.Visible = False
      Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = True
    End If

    If Me.m_mode_edit = MODE_EDIT.COLLECTION Then
      Call Me.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)
    End If

    Me.m_opened = True

    tf_latest_update.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(324)
    tf_latest_update.Value = "0"
    lbl_monitor_msg.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6573, MAX_ROWS_IN_MONITOR_MODE.ToString())

  End Sub 'GUI_InitControls

  Protected Overrides Sub GUI_NoDataFound()
    If m_cashier_session_id > 0 And Not m_opened Then
      Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Enabled = False
    Else
      If Me.m_opened And opt_history_mode.Checked Then
        Call MyBase.GUI_NoDataFound()
      End If
    End If
  End Sub



  ' PURPOSE: To set the cage sessions combo box
  '
  ' PARAMS:
  '       - Combo
  '
  ' RETURNS:
  '      - None
  '
  ' NOTES:
  Private Sub SetComboCageSessions(ByRef Combo As uc_combo)
    Dim _sb As StringBuilder
    Dim _table As DataTable

    _sb = New StringBuilder

    _sb.AppendLine("    SELECT   CGS_CAGE_SESSION_ID  ")
    _sb.AppendLine("           , CGS_SESSION_NAME AS GU_USERNAME ")
    _sb.AppendLine("      FROM   CAGE_SESSIONS ")
    _sb.AppendLine("INNER JOIN   GUI_USERS ")
    _sb.AppendLine("        ON   CGS_OPEN_USER_ID = GU_USER_ID ")
    _sb.AppendLine("  ORDER BY   GU_USERNAME ASC ")

    _table = GUI_GetTableUsingSQL(_sb.ToString(), Integer.MaxValue)

    Call Combo.Clear()
    Call Combo.Add(_table)
    Combo.TextValue = String.Empty
    Combo.SelectedIndex = -1
  End Sub ' SetComboCageSessions

  Private Sub SetDefaultCageSession()
    If Me.cmb_sessions.Count >= 1 Then
      Me.cmb_sessions.SelectedIndex = 0
      Me.cmb_sessions.Enabled = False
    End If
  End Sub

  ' PURPOSE: Process button actions in order to branch to a child screen
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY
        If m_session_id <> -1 Then
          Call GetMovementsTotals()
        End If
        Call MyBase.GUI_ButtonClick(ButtonId)
        m_first_time = False

        ' Recaudar
        If Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2904) Then
          Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Enabled = Not GLB_CurrentUser.IsSuperUser
        End If

      Case frm_base_sel.ENUM_BUTTON.BUTTON_GRID_INFO
        Call GUI_ShowSelectedItem()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        If Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Text <> GLB_NLS_GUI_PLAYER_TRACKING.GetString(2904) Or Not GLB_CurrentUser.IsSuperUser Then
          Call GUI_ShowSelectedItem()
        End If
      Case frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0
        Call GUI_ShowSelectedItem()

      Case ENUM_BUTTON.BUTTON_PRINT
        Call MyBase.GUI_ButtonClick(ButtonId)

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub ' GUI_ButtonClick
  ' PURPOSE: Overrides the clear grid routine
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Boolean
  '
  Protected Overrides Sub GUI_ClearGrid()
    If opt_history_mode.Checked Or (opt_monitor_mode.Checked And m_first_time) Then
      Call MyBase.GUI_ClearGrid()
    End If
  End Sub ' GUI_ClearGrid

  Protected Overrides Sub GUI_EditNewItem()
    Dim _frm As frm_cage_control

    If Not Cage.IsCageEnabled Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3026), ENUM_MB_TYPE.MB_TYPE_WARNING)
    Else
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      If opt_monitor_mode.Checked Then
        tmr_monitor.Stop()
      End If

      _frm = New frm_cage_control
      _frm.ShowNewItem(, Me.m_session_id)

      Windows.Forms.Cursor.Current = Cursors.Default

      If opt_monitor_mode.Checked Then
        Call tmr_monitor_Tick(Nothing, Nothing)
        tmr_monitor.Start()
      End If

    End If
  End Sub ' GUI_EditNewItem

  ' PURPOSE: Call to child window to show details for the selected row
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ShowSelectedItem()

    Dim _frm_edit As frm_cage_control
    Dim _idx_row As Int32
    Dim _movement_id As Int64
    Dim _operation_type_id As Short
    Dim _status As Short
    Dim _cage_session_id As Int32
    Dim _money_collection_id As Int64
    Dim _collection_status As Int32
    Dim _gambling_table_id As Int64
    Dim _has_integrated_cashier As Boolean
    Dim _form_id As ENUM_FORM
    Dim _cancel_mode As CLASS_CAGE_CONTROL.OPEN_MODE
    Dim _collect_mode As CLASS_CAGE_CONTROL.OPEN_MODE
    Dim _terminal_type As TerminalTypes
    Dim _mb_user_type As MB_USER_TYPE
    Dim _gui_user_type As GU_USER_TYPE
    Dim _frm As frm_progressive_provision
    Dim _progressive_provision_id As Integer

    Me.m_cls_cage_control = New CLASS_CAGE_CONTROL

    If opt_monitor_mode.Checked Then
      tmr_monitor.Stop()
    End If

    If Not Cage.IsCageEnabled Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3026), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return
    End If

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    If _idx_row = Me.Grid.NumRows Then
      Return
    End If

    ' Get the Promotion ID and Name, and launch the editor
    _movement_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_MOVEMENT_ID).Value
    _gambling_table_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_GAMBLING_TABLE_ID).Value

    _terminal_type = Me.Grid.Cell(_idx_row, GRID_COLUMN_TERMINAL_TYPE).Value
    Cashier.GetMobileBankAndGUIUserType(_terminal_type, _mb_user_type, _gui_user_type)

    Call GetStatusOperation(_movement_id, _operation_type_id, _status)
    If _operation_type_id = Cage.CageOperationType.FromTerminal Then
      _form_id = ENUM_FORM.FORM_STACKER_COLLECTION
    Else
      _form_id = ENUM_FORM.FORM_CAGE_CONTROL
    End If

    _frm_edit = New frm_cage_control(_form_id)

    _cage_session_id = Me.m_cls_cage_control.GetCageSessionIdByMovement(_movement_id)

    Select Case _operation_type_id
      Case Cage.CageOperationType.ToCashier _
         , Cage.CageOperationType.ToGamingTable
        If _status = Cage.CageStatus.Sent Then
          Call _frm_edit.ShowEditItem(_movement_id, CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT, , True)
        Else
          Call _frm_edit.ShowEditItem(_movement_id, CLASS_CAGE_CONTROL.OPEN_MODE.VIEW_MOVEMENT, , True)
        End If

      Case Cage.CageOperationType.FromCashier _
, Cage.CageOperationType.FromCashierClosing _
         , Cage.CageOperationType.FromGamingTable _
, Cage.CageOperationType.FromGamingTableClosing _
         , Cage.CageOperationType.FromGamingTableDropbox _
         , Cage.CageOperationType.FromGamingTableDropboxWithExpected
        If _status = Cage.CageStatus.Sent Then
          Call _frm_edit.ShowEditItem(_movement_id, CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MOVEMENT, , True)
        Else
          If Me.m_cls_cage_control.HasIntegratedCashier(_gambling_table_id, _has_integrated_cashier) Then

            If _gambling_table_id <> 0 AndAlso Not _has_integrated_cashier Then
              Call _frm_edit.ShowEditItem(_movement_id, CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT_FROM_GAMINGTABLES, , True)
            Else
              Call _frm_edit.ShowEditItem(_movement_id, CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT, , True)
            End If

          End If
        End If

      Case Cage.CageOperationType.FromTerminal
        Me.m_cls_cage_control.GetMoneyCollectionIdByMovement(_movement_id, _money_collection_id, _collection_status)

        If _gui_user_type = GU_USER_TYPE.SYS_TITO Then
          _cancel_mode = CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT
          _collect_mode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MOVEMENT
        Else
          _cancel_mode = CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT_FROM_PROMOBOX_OR_ACCEPTOR
          _collect_mode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_FROM_PROMOBOX_OR_ACCEPTOR
        End If

        If _collection_status = TITO_MONEY_COLLECTION_STATUS.CLOSED_BALANCED OrElse _collection_status = TITO_MONEY_COLLECTION_STATUS.CLOSED_IN_IMBALANCE Then
          Call _frm_edit.ShowEditItem(_movement_id, CType(_cancel_mode, Int16), _cage_session_id, True, _gui_user_type)
        ElseIf TITO_MONEY_COLLECTION_STATUS.PENDING Then
          Call _frm_edit.ShowEditItem(_movement_id, CType(_collect_mode, Int16), _cage_session_id, False, _gui_user_type)
        End If

      Case Cage.CageOperationType.RequestOperation
        If Cage.GetNumberOfOpenedCages() = 0 Then
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3378), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Else
          If _status = Cage.CageStatus.Sent Then ' The Cashier has requested to Cage (the cage can be closed)
            _frm_edit.ShowWithoutCageID = True
            Call _frm_edit.ShowEditItem(_movement_id, CType(CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MOVEMENT, Int16), _cage_session_id, True)
          Else
            Call _frm_edit.ShowEditItem(_movement_id, CType(CLASS_CAGE_CONTROL.OPEN_MODE.VIEW_MOVEMENT, Int16), _cage_session_id, True)
          End If
        End If

      Case Cage.CageOperationType.FromCustom
        If _status = Cage.CageStatus.ReceptionFromCustom Then
          Call _frm_edit.ShowEditItem(_movement_id, CType(CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT, Int16), _cage_session_id, True)
        Else
          Call _frm_edit.ShowEditItem(_movement_id, CLASS_CAGE_CONTROL.OPEN_MODE.VIEW_MOVEMENT, , True)
        End If

      Case Cage.CageOperationType.ToCustom
        If _status = Cage.CageStatus.SentToCustom Then
          Call _frm_edit.ShowEditItem(_movement_id, CType(CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT, Int16), _cage_session_id, True)
        Else
          Call _frm_edit.ShowEditItem(_movement_id, CLASS_CAGE_CONTROL.OPEN_MODE.VIEW_MOVEMENT, , True)
        End If
      Case Cage.CageOperationType.ProgressiveProvision, Cage.CageOperationType.ProgressiveCancelProvision
        _progressive_provision_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_RELATED_ID).Value
        _frm = New frm_progressive_provision()
        _frm.ShowForEdit(_progressive_provision_id, Nothing)

      Case Cage.CageOperationType.CloseCage, Cage.CageOperationType.OpenCage, _
        Cage.CageOperationType.GainQuadrature, Cage.CageOperationType.LostQuadrature
        ' DO NOTHING

      Case Else
        Call _frm_edit.ShowEditItem(_movement_id, CLASS_CAGE_CONTROL.OPEN_MODE.VIEW_MOVEMENT, , True)

    End Select

    _frm_edit = Nothing

    Call Me.Grid.Focus()
    If opt_monitor_mode.Checked Then
      Call tmr_monitor_Tick(Nothing, Nothing)
      tmr_monitor.Start()

    End If
  End Sub ' GUI_ShowSelectedItem
  ' PURPOSE: Indicates if to select the first row of the grid or not
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Boolean
  '
  Protected Overrides Function GUI_SelectFirstRow() As Boolean
    Dim _select_first As Boolean

    _select_first = opt_history_mode.Checked Or (opt_monitor_mode.Checked And m_first_time)
    m_first_time = False

    Return _select_first
  End Function ' GUI_SelectFirstRow
  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub 'GUI_FilterReset

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Date selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    Return True
  End Function 'GUI_FilterCheck

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _sql As StringBuilder

    _sql = New StringBuilder()

    _sql.AppendLine("CREATE TABLE #TABLE_MOVEMENT_CAGE_DATA                                                    ")
    _sql.AppendLine("(                                                                                         ")
    _sql.AppendLine("   CGM_MOVEMENT_ID                 BIGINT,                                                ")
    _sql.AppendLine("   TE_NAME                         VARCHAR(MAX),                                          ")
    _sql.AppendLine("   GU_FULL_NAME                    VARCHAR(MAX),                                          ")
    _sql.AppendLine("   CT_NAME                         VARCHAR(MAX),                                          ")
    _sql.AppendLine("   GU_FULL_NAME_2                  VARCHAR(MAX),                                          ")
    _sql.AppendLine("   CGM_TYPE                        INTEGER,                                               ")
    _sql.AppendLine("   CGM_STATUS                      INTEGER,                                               ")
    _sql.AppendLine("   CST_SOURCE_TARGET_NAME          VARCHAR(MAX),                                          ")
    _sql.AppendLine("   CGM_MOVEMENT_DATETIME           DATETIME,                                              ")
    _sql.AppendLine("   CGM_GAMING_TABLE_ID             INTEGER,                                               ")
    _sql.AppendLine("   CGS_CAGE_SESSION_ID             BIGINT,                                                ")
    _sql.AppendLine("   GU_USERNAME                     VARCHAR(MAX),                                          ")
    _sql.AppendLine("   CGM_GAMING_TABLE_ID_2           INTEGER,                                               ")
    _sql.AppendLine("   TE_TERMINAL_TYPE                INTEGER,                                               ")
    _sql.AppendLine("   GU_FULL_NAME_LAST_MODIFICATION  VARCHAR(MAX),                                          ")
    _sql.AppendLine("   CGM_CANCELLATION_DATETIME       DATETIME,                                              ")
    _sql.AppendLine("   PROGRESSIVE_PROVISION_AMOUNT    MONEY,                                                 ")
    _sql.AppendLine("   PROGRESSIVE_AWARDED_AMOUNT      MONEY,                                                 ")
    _sql.AppendLine("   CGM_RELATED_ID                  BIGINT,                                                ")
    _sql.AppendLine("   CGS_WORKING_DAY                 DATETIME,                                              ")
    _sql.AppendLine("   CMT_COLLECTION_ID               BIGINT,                                                ")
    _sql.AppendLine("   CMT_TERMINAL_ID                 INTEGER,                                               ")
    _sql.AppendLine("   CGM_CASHIER_SESSION_ID          BIGINT,                                                ")
    _sql.AppendLine("   GAMING_TABLE_SESSION_ID         BIGINT                                                 ")
    _sql.AppendLine(")                                                                                         ")
    _sql.AppendLine("INSERT INTO #TABLE_MOVEMENT_CAGE_DATA                                                     ")

    If opt_monitor_mode.Checked Then
      _sql.AppendLine("          SELECT   TOP " & MAX_ROWS_IN_MONITOR_MODE + 1)
    Else
      _sql.AppendLine(" SELECT                                                                      ")
    End If

    _sql.AppendLine("                   CGM_MOVEMENT_ID                                                                   ")
    _sql.AppendLine("                 , TE_NAME                                                                           ")
    _sql.AppendLine("                 , GUV.GU_FULL_NAME                                                                  ")
    _sql.AppendLine("                 , (CASE WHEN CT_NAME IS NULL THEN                                                   ")
    _sql.AppendLine("                      (SELECT TE_NAME FROM TERMINALS WHERE TE_TERMINAL_ID = CGM_TERMINAL_CASHIER_ID) ")
    _sql.AppendLine("                    ELSE                                                                             ")
    _sql.AppendLine("                      CT_NAME                                                                        ")
    _sql.AppendLine("                    END) AS CT_NAME                                                                  ")
    If (rb_gaming_table.Checked) Then
      _sql.AppendLine("                 , GT_NAME                                                                  ")
    Else
      _sql.AppendLine("                 , GUC.GU_FULL_NAME                                                                  ")
    End If
    _sql.AppendLine("                 , CGM_TYPE                                                                          ")
    _sql.AppendLine("                 , CGM_STATUS                                                                        ")
    _sql.AppendLine("                 , CST.CST_SOURCE_TARGET_NAME                                                        ")
    _sql.AppendLine("                 , CGM_MOVEMENT_DATETIME                                                             ")
    _sql.AppendLine("                 , CGM_GAMING_TABLE_ID                                                               ")
    _sql.AppendLine("                 , X.CGS_CAGE_SESSION_ID                                                             ")
    _sql.AppendLine("                 , X.GU_USERNAME                                                                     ")
    _sql.AppendLine("                 , CGM_GAMING_TABLE_ID                                                               ")
    _sql.AppendLine("                 , TE_TERMINAL_TYPE                                                                  ")

    _sql.AppendLine("                 , GUVLM.GU_FULL_NAME as GU_FULL_NAME_LAST_MODIFICATION                              ")
    _sql.AppendLine("                 , CGM_CANCELLATION_DATETIME                                                         ")
    _sql.AppendLine("                 , PGP_CAGE_AMOUNT   AS PROGRESSIVE_PROVISION_AMOUNT                                 ")
    _sql.AppendLine("                 , HP_AMOUNT         AS PROGRESSIVE_AWARDED_AMOUNT                                   ")
    _sql.AppendLine("                 , CGM_RELATED_ID                                                                    ")
    _sql.AppendLine("                 , X.CGS_WORKING_DAY                                                                 ")
    _sql.AppendLine("                 , CGM_MC_COLLECTION_ID                                                              ")
    _sql.AppendLine("                 , CGM_TERMINAL_CASHIER_ID                                                           ")
    _sql.AppendLine("                 , CGM_CASHIER_SESSION_ID                                                            ")
    _sql.AppendLine("                 , ISNULL(GTS_GAMING_TABLE_SESSION_ID, 0) AS GAMING_TABLE_SESSION_ID                 ")
    _sql.AppendLine("            FROM   CAGE_MOVEMENTS                                                                    ")
    If (rb_gaming_table.Checked) Then
      _sql.AppendLine(" INNER JOIN   CASHIER_SESSIONS ON CGM_CASHIER_SESSION_ID = CS_SESSION_ID            ")
      _sql.AppendLine(" INNER JOIN   GAMING_TABLES_SESSIONS ON CS_SESSION_ID = GTS_CASHIER_SESSION_ID            ")
      _sql.AppendLine(" INNER JOIN   GAMING_TABLES ON GTS_GAMING_TABLE_ID = GT_GAMING_TABLE_ID            ")
    Else
      _sql.AppendLine(" LEFT JOIN   CASHIER_SESSIONS ON CGM_CASHIER_SESSION_ID = CS_SESSION_ID            ")
      _sql.AppendLine(" LEFT JOIN   GAMING_TABLES_SESSIONS ON CS_SESSION_ID = GTS_CASHIER_SESSION_ID            ")
    End If
    _sql.AppendLine(" LEFT OUTER JOIN   CASHIER_TERMINALS ON CGM_TERMINAL_CASHIER_ID = CT_CASHIER_ID                      ")
    _sql.AppendLine(" LEFT OUTER JOIN   GUI_USERS GUC ON GUC.GU_USER_ID = CGM_USER_CASHIER_ID                             ")
    _sql.AppendLine(" LEFT OUTER JOIN   TERMINALS ON CGM_TERMINAL_CASHIER_ID = TE_TERMINAL_ID                             ")
    _sql.AppendLine(" LEFT OUTER JOIN   GUI_USERS GUV ON GUV.GU_USER_ID = CGM_USER_CAGE_ID                                ")
    _sql.AppendLine(" LEFT OUTER JOIN   CAGE_SOURCE_TARGET CST ON CST.CST_SOURCE_TARGET_ID = CGM_SOURCE_TARGET_ID         ")
    _sql.AppendLine("      LEFT JOIN   (                                                                                  ")
    _sql.AppendLine("                     SELECT   CGS_CAGE_SESSION_ID                                                    ")
    _sql.AppendLine("               			       , GU_USER_ID                                                             ")
    _sql.AppendLine("               			       , CGS_SESSION_NAME AS GU_USERNAME                                        ")
    _sql.AppendLine("               			       , CGS_WORKING_DAY                                                        ")
    _sql.AppendLine("               		    FROM   CAGE_SESSIONS                                                          ")
    _sql.AppendLine("               	INNER JOIN   GUI_USERS                                                              ")
    _sql.AppendLine("               			    ON   CGS_OPEN_USER_ID = GU_USER_ID                                          ")
    _sql.AppendLine("                   ) AS X ON X.CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID                             ")

    _sql.AppendLine(" LEFT OUTER JOIN   GUI_USERS GUVLM ON GUVLM.GU_USER_ID = CGM_CANCELLATION_USER_ID                    ")
    _sql.AppendLine(" LEFT OUTER JOIN   HANDPAYS ON HP_ID = CGM_RELATED_ID                                                ")
    _sql.AppendLine(" LEFT OUTER JOIN   PROGRESSIVES_PROVISIONS ON PGP_PROVISION_ID = CGM_RELATED_ID                      ")

    _sql.AppendLine(" WHERE 1 = 1 ")

    _sql.AppendLine(Me.GetSqlWhere())

    _sql.AppendLine(" ORDER BY ")
    If opt_by_cashier_session.Checked Then
      _sql.AppendLine(" CGM_CASHIER_SESSION_ID DESC, CGM_MOVEMENT_ID DESC ")
    ElseIf opt_monitor_mode.Checked Then
      _sql.AppendLine(" CGM_MOVEMENT_DATETIME ASC, CGM_MOVEMENT_ID ASC ")
    Else
      _sql.AppendLine(" CGM_MOVEMENT_DATETIME DESC, CGM_MOVEMENT_ID DESC ") ' by default
    End If

    _sql.AppendLine(Me.GetSqlIsoCodesAmount())

    _sql.AppendLine("DROP TABLE #TABLE_MOVEMENT_CAGE_DATA    ")

    Return _sql.ToString()

  End Function 'GUI_FilterGetSqlQuery

  ' PURPOSE: Define the ExecuteQuery customized
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     -
  Protected Overrides Sub GUI_ExecuteQueryCustom()
    Dim _str_sql As String
    Dim _idx_row As Integer
    Dim _idx_grid As Integer
    Dim _count As Integer
    Dim _db_row As CLASS_DB_ROW
    Dim _bln_redraw As Boolean
    Dim _function_name As String
    Dim _more_than_max As Boolean
    Dim _row_step As Integer
    Dim _max_rows As Integer
    Dim _table As DataTable
    Dim _row As DataRow
    Dim _render_only_changes As Boolean
    Dim _has_changes As Boolean
    Dim _rows_count As Integer

    _table = Nothing
    m_table = Nothing
    _rows_count = 0

    _function_name = "GUI_ExecuteQueryCustom"

    _max_rows = GUI_MaxRows()
    _render_only_changes = opt_monitor_mode.Checked

    _has_changes = False
    Try

      tmr_monitor.Stop()

      SaveSelectedIds()

      _str_sql = GUI_FilterGetSqlQuery()

      If String.IsNullOrEmpty(_str_sql) Then
        Return
      End If

      _table = GUI_GetTableUsingSQL(_str_sql, _max_rows)

      If _table Is Nothing Then
        'lbl_no_data.Visible = True
        Me.Grid.Clear()

        Return
      End If

      If _table.Rows.Count = 0 Then
        'lbl_no_data.Visible = True
        Me.Grid.Clear()

        Return
      End If

      If opt_monitor_mode.Checked Then
        If _table.Rows.Count > MAX_ROWS_IN_MONITOR_MODE Then
          lbl_monitor_msg.Visible = True
          _table.Rows.RemoveAt(MAX_ROWS_IN_MONITOR_MODE)
        Else
          lbl_monitor_msg.Visible = False
        End If
      End If

      ' JAB 24-AUG-2012: Control to show huge amounts of records.
      If Not ShowHugeNumberOfRows(_table.Rows.Count) Then
        MyBase.m_user_canceled_data_shows = True
        Exit Sub
      End If

      _bln_redraw = Me.Grid.Redraw
      Me.Grid.Redraw = False

      _more_than_max = False

      Call GetIsoCodeColumns(_table)
      Call GUI_BeforeFirstRow()

      _row_step = _max_rows
      _row_step = _row_step / 20
      If _row_step < MAX_RECORDS_FIRST_LOAD Then
        _row_step = MAX_RECORDS_FIRST_LOAD
      End If

      _count = 0
      _idx_grid = 0
      _idx_row = 0

      While _idx_row < _table.Rows.Count

        Try

          ' JAB 12-JUN-2012: DoEvents run each second.
          If Not GUI_DoEvents(Me.Grid) Then
            Exit Sub
          End If

          _row = _table.Rows(_idx_row)
          _db_row = New CLASS_DB_ROW(_row)

          If _render_only_changes Then

            If DataGridMustChange(_idx_grid, _row) Then
              If Me.Grid.NumRows - 1 < _idx_grid Then
                Me.Grid.AddRow()
              End If
              If GUI_SetupRow(_idx_grid, _db_row) Then
                _has_changes = True
                _count = _count + 1
              End If
            End If
            _idx_grid = _idx_grid + 1
          Else

            ' Add the db row to the grid
            Me.Grid.AddRow()
            _idx_grid = Me.Grid.NumRows - 1

            If GUI_SetupRow(_idx_grid, _db_row) Then
              _count = _count + 1
            End If
          End If
          _idx_row = _idx_row + 1

        Catch exception As Exception
          Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
          Call Trace.WriteLine(exception.ToString())
          Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                                "Cage Movements - Monitor Mode", _
                                _function_name, _
                                exception.Message)
          Exit While

        End Try

        If _count > 0 And _count Mod _row_step = 0 Then
          Me.Grid.Redraw = True
          Call Application.DoEvents()
          Windows.Forms.Cursor.Current = Cursors.WaitCursor
          Me.Grid.Redraw = False
        End If

        If _count >= _max_rows Then
          _more_than_max = True

          Exit While
        End If
      End While

      If _render_only_changes Then
        While Me.Grid.NumRows > _table.Rows.Count
          Me.Grid.DeleteRowFast(Me.Grid.NumRows - 1)
          _has_changes = True
        End While
      End If

      Call GUI_AfterLastRow()

      Me.Grid.Redraw = True

      If _has_changes Then
        SelectLastSelectedRow()
      End If

      If _more_than_max Then
        Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(111), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, , , CStr(_max_rows))
      End If

    Catch ex As Exception
      ' An error has occurred in the query execution
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "Cage Movements - Monitor Mode", _
                            _function_name, _
                            ex.Message)
      Me.Grid.Redraw = True

    Finally
      tmr_monitor.Start()
      ' JAB 01-JUN-2012: Check if disposed.
      If Not Me.IsDisposed Then
        GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = (Me.Grid.NumRows > 0 And Me.Permissions.Write)
      End If

      m_table = New DataTable()
      m_table = _table.Copy()

      If Not _render_only_changes Then
        If _table IsNot Nothing Then
          Call _table.Clear()
        End If
        _table = Nothing
      End If
    End Try

  End Sub ' GUI_ExecuteQueryCustom
  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _column_offset As Int32
    Dim _str_balance_state As String
    Dim _monitor_count As Integer
    Dim _iso_code_column As Integer

    _str_balance_state = ""

    If opt_monitor_mode.Checked Then
      _monitor_count += 1
      If _monitor_count = MAX_ROWS_IN_MONITOR_MODE Then
        _monitor_count = 0
        lbl_monitor_msg.Visible = True
        Return False
      End If
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_ID)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MOVEMENT_ID).Value = DbRow.Value(SQL_COLUMN_ID)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_DATETIME)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MOVEMENT_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATETIME), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_TERMINAL_CAGE_NAME)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_CAGE_NAME).Value = DbRow.Value(SQL_COLUMN_TERMINAL_CAGE_NAME)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_TERMINAL_CASHIER_NAME)) Then
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_OPERATION_TYPE)) AndAlso _
         DbRow.Value(SQL_COLUMN_OPERATION_TYPE) <> Cage.CageOperationType.FromGamingTable And _
DbRow.Value(SQL_COLUMN_OPERATION_TYPE) <> Cage.CageOperationType.FromGamingTableClosing And _
         DbRow.Value(SQL_COLUMN_OPERATION_TYPE) <> Cage.CageOperationType.ToGamingTable And _
         DbRow.Value(SQL_COLUMN_OPERATION_TYPE) <> Cage.CageOperationType.FromGamingTableDropbox And _
         DbRow.Value(SQL_COLUMN_OPERATION_TYPE) <> Cage.CageOperationType.FromGamingTableDropboxWithExpected Then

        Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_CASHIER_NAME).Value = Computer.Alias(DbRow.Value(SQL_COLUMN_TERMINAL_CASHIER_NAME))
      End If
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_USER_CAGE_NAME)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_CAGE_NAME).Value = DbRow.Value(SQL_COLUMN_USER_CAGE_NAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_CAGE_NAME).Value = AUDIT_NONE_STRING
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_USER_CAGE_NAME_LAST_MODIFICATION)) And _
      DbRow.Value(SQL_COLUMN_STATUS) <> Cage.CageStatus.OK And _
      DbRow.Value(SQL_COLUMN_STATUS) <> Cage.CageStatus.OkDenominationImbalance And _
      DbRow.Value(SQL_COLUMN_STATUS) <> Cage.CageStatus.OkAmountImbalance Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_CAGE_NAME_LAST_MODIFICATION).Value = DbRow.Value(SQL_COLUMN_USER_CAGE_NAME_LAST_MODIFICATION)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_CAGE_NAME_LAST_MODIFICATION).Value = AUDIT_NONE_STRING
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_DATETIME_LAST_MODIFICATION)) And _
      DbRow.Value(SQL_COLUMN_STATUS) <> Cage.CageStatus.OK And _
      DbRow.Value(SQL_COLUMN_STATUS) <> Cage.CageStatus.OkDenominationImbalance And _
      DbRow.Value(SQL_COLUMN_STATUS) <> Cage.CageStatus.OkAmountImbalance Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MOVEMENT_DATETIME_LAST_MODIFICATION).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATETIME_LAST_MODIFICATION), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_OPERATION_TYPE)) Then
      Select Case DbRow.Value(SQL_COLUMN_OPERATION_TYPE)
        Case Cage.CageOperationType.ToCashier
          Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4376)
          If Not IsDBNull(DbRow.Value(SQL_COLUMN_USER_CASHIER_NAME)) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_CASHIER_NAME).Value = DbRow.Value(SQL_COLUMN_USER_CASHIER_NAME)
            If DbRow.Value(SQL_COLUMN_GAMING_TABLE_SESSION_ID) > 0 And (Me.rb_gaming_table.Checked Or opt_all_cashiers.Checked) Then
              Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4391) '  Send to gaming tables
            End If
          End If

        Case Cage.CageOperationType.FromCashier
          Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4377)
          If Not IsDBNull(DbRow.Value(SQL_COLUMN_USER_CASHIER_NAME)) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_CASHIER_NAME).Value = DbRow.Value(SQL_COLUMN_USER_CASHIER_NAME)
            If DbRow.Value(SQL_COLUMN_GAMING_TABLE_SESSION_ID) > 0 Then
              Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4392) '  Collection from gaming tables
            End If
          End If

        Case Cage.CageOperationType.FromCashierClosing
          Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7898)
          If Not IsDBNull(DbRow.Value(SQL_COLUMN_USER_CASHIER_NAME)) Then
            If DbRow.Value(SQL_COLUMN_GAMING_TABLE_SESSION_ID) > 0 Then
              Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7900) 'Recepci�n en cierre desde mesa de juego
            End If
            Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_CASHIER_NAME).Value = DbRow.Value(SQL_COLUMN_USER_CASHIER_NAME)
          End If

        Case Cage.CageOperationType.ToCustom
          Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3394)
          If Not IsDBNull(DbRow.Value(SQL_COLUMN_CUSTOM_NAME)) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_CASHIER_NAME).Value = DbRow.Value(SQL_COLUMN_CUSTOM_NAME)
          End If

        Case Cage.CageOperationType.FromCustom
          Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3395)
          If Not IsDBNull(DbRow.Value(SQL_COLUMN_CUSTOM_NAME)) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_CASHIER_NAME).Value = DbRow.Value(SQL_COLUMN_CUSTOM_NAME)
          End If

        Case Cage.CageOperationType.OpenCage
          Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3381)
          If Not IsDBNull(DbRow.Value(SQL_COLUMN_CUSTOM_NAME)) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_CASHIER_NAME).Value = DbRow.Value(SQL_COLUMN_CUSTOM_NAME)
          End If


        Case Cage.CageOperationType.ReOpenCage
          Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5383)
          If Not IsDBNull(DbRow.Value(SQL_COLUMN_CUSTOM_NAME)) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_CASHIER_NAME).Value = DbRow.Value(SQL_COLUMN_CUSTOM_NAME)
          End If

        Case Cage.CageOperationType.CloseCage
          Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3382)
          If Not IsDBNull(DbRow.Value(SQL_COLUMN_CUSTOM_NAME)) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_CASHIER_NAME).Value = DbRow.Value(SQL_COLUMN_CUSTOM_NAME)
          End If

        Case Cage.CageOperationType.CountCage
          Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4352)
          If Not IsDBNull(DbRow.Value(SQL_COLUMN_CUSTOM_NAME)) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_CASHIER_NAME).Value = DbRow.Value(SQL_COLUMN_CUSTOM_NAME)
          End If

        Case Cage.CageOperationType.ToGamingTable
          Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4391) '  Env�o a mesa de juego
          If Not IsDBNull(DbRow.Value(SQL_COLUMN_GAMING_TABLE_ID)) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_CASHIER_NAME).Value = Me.GetGamingTableId(DbRow.Value(SQL_COLUMN_GAMING_TABLE_ID))
          End If

        Case Cage.CageOperationType.FromGamingTable
          Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4392) 'Recepci�n desde mesa de juego
          If Not IsDBNull(DbRow.Value(SQL_COLUMN_GAMING_TABLE_ID)) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_CASHIER_NAME).Value = Me.GetGamingTableId(DbRow.Value(SQL_COLUMN_GAMING_TABLE_ID))
          End If

        Case Cage.CageOperationType.FromGamingTableClosing
          Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7900) 'Recepci�n en cierre desde mesa de juego
          If Not IsDBNull(DbRow.Value(SQL_COLUMN_GAMING_TABLE_ID)) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_CASHIER_NAME).Value = Me.GetGamingTableId(DbRow.Value(SQL_COLUMN_GAMING_TABLE_ID))
          End If

        Case Cage.CageOperationType.FromGamingTableDropbox, _
          Cage.CageOperationType.FromGamingTableDropboxWithExpected
          Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7329) ' Gambling Table drop box collection
          If Not IsDBNull(DbRow.Value(SQL_COLUMN_GAMING_TABLE_ID)) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_CASHIER_NAME).Value = Me.GetGamingTableId(DbRow.Value(SQL_COLUMN_GAMING_TABLE_ID))
          End If

        Case Cage.CageOperationType.FromTerminal
          Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3308) 'Recaudaci�n de tickets

        Case Cage.CageOperationType.ToTerminal
          Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7037) 'Env�o 

        Case Cage.CageOperationType.RequestOperation
          Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4785) 'Solicitud desde cajero
          If Not IsDBNull(DbRow.Value(SQL_COLUMN_USER_CASHIER_NAME)) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_CASHIER_NAME).Value = DbRow.Value(SQL_COLUMN_USER_CASHIER_NAME)
            If DbRow.Value(SQL_COLUMN_GAMING_TABLE_SESSION_ID) > 0 Then
              Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7367) 'Solicitud desde mesa de juego
            End If
          End If

        Case Cage.CageOperationType.ProgressiveProvision
          Dim _amount_str As String
          _amount_str = " "
          If Not IsDBNull(DbRow.Value(SQL_COLUMN_PROGRESSIVE_PROVISION_AMOUNT)) Then
            _amount_str = ": " & FormatCurrency(DbRow.Value(SQL_COLUMN_PROGRESSIVE_PROVISION_AMOUNT))
          End If
          Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5403) & _amount_str 'Provisi�n de progresivo

        Case Cage.CageOperationType.ProgressiveAwarded
          Dim _amount_str As String
          _amount_str = " "
          If Not IsDBNull(DbRow.Value(SQL_COLUMN_PROGRESSIVE_AWARDED_AMOUNT)) Then
            _amount_str = ": " & FormatCurrency(DbRow.Value(SQL_COLUMN_PROGRESSIVE_AWARDED_AMOUNT))
          End If
          Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5404) & _amount_str 'Progresivo otorgado

        Case Cage.CageOperationType.ProgressiveCancelProvision
          Dim _amount_str As String
          _amount_str = " "
          If Not IsDBNull(DbRow.Value(SQL_COLUMN_PROGRESSIVE_PROVISION_AMOUNT)) Then
            _amount_str = ": " & FormatCurrency(DbRow.Value(SQL_COLUMN_PROGRESSIVE_PROVISION_AMOUNT))
          End If
          Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_TYPE).Value = "(" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2786) & ") " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5403) & _amount_str '(Anulaci�n) Provisi�n de progresivo

        Case Cage.CageOperationType.ProgressiveCancelAwarded
          Dim _amount_str As String
          _amount_str = " "
          If Not IsDBNull(DbRow.Value(SQL_COLUMN_PROGRESSIVE_AWARDED_AMOUNT)) Then
            _amount_str = ": " & FormatCurrency(DbRow.Value(SQL_COLUMN_PROGRESSIVE_AWARDED_AMOUNT))
          End If
          Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_TYPE).Value = "(" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2786) & ") " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5404) & _amount_str '(Anulaci�n) Progresivo otorgado

        Case Cage.CageOperationType.GainQuadrature
          Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9019)
          If Not IsDBNull(DbRow.Value(SQL_COLUMN_CUSTOM_NAME)) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_CASHIER_NAME).Value = DbRow.Value(SQL_COLUMN_CUSTOM_NAME)
          End If

        Case Cage.CageOperationType.LostQuadrature
          Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9018)
          If Not IsDBNull(DbRow.Value(SQL_COLUMN_CUSTOM_NAME)) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_CASHIER_NAME).Value = DbRow.Value(SQL_COLUMN_CUSTOM_NAME)
          End If

        Case Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_TYPE).Value = AUDIT_NONE_STRING

      End Select

      Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_TYPE_ID).Value = DbRow.Value(SQL_COLUMN_OPERATION_TYPE)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_STATUS)) Then
      Select Case DbRow.Value(SQL_COLUMN_STATUS)
        Case Cage.CageStatus.Sent

          Select Case DbRow.Value(SQL_COLUMN_OPERATION_TYPE)
            Case Cage.CageOperationType.ToCashier
              Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2891)
            Case Cage.CageOperationType.FromCashier
              Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3011)
            Case Cage.CageOperationType.FromCashierClosing
              Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3011)
            Case Cage.CageOperationType.ToGamingTable
              Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2891)
            Case Cage.CageOperationType.FromGamingTable
              Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3011)
            Case Cage.CageOperationType.FromGamingTableClosing
              Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3011)
            Case Cage.CageOperationType.FromGamingTableDropbox, _
              Cage.CageOperationType.FromGamingTableDropboxWithExpected
              Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3011)
            Case Cage.CageOperationType.FromTerminal
              Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3011)
            Case Cage.CageOperationType.RequestOperation
              Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4786)
            Case Cage.CageOperationType.ToTerminal
              Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3011)
          End Select

          Me.m_sent_rows += 1

        Case Cage.CageStatus.OK

          Select Case DbRow.Value(SQL_COLUMN_OPERATION_TYPE)
            Case Cage.CageOperationType.ToCashier
              Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2892)
            Case Cage.CageOperationType.FromCashier
              Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3305)
            Case Cage.CageOperationType.FromCashierClosing
              Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3305)
            Case Cage.CageOperationType.ToGamingTable
              Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2892)
            Case Cage.CageOperationType.FromGamingTable
              Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3305)
            Case Cage.CageOperationType.FromGamingTableClosing
              Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3305)
            Case Cage.CageOperationType.FromGamingTableDropbox, _
              Cage.CageOperationType.FromGamingTableDropboxWithExpected
              Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3305)
            Case Cage.CageOperationType.FromTerminal
              Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3305)
            Case Cage.CageOperationType.RequestOperation
              Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4788)
            Case Cage.CageOperationType.ProgressiveProvision
              Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2892)
            Case Cage.CageOperationType.ProgressiveAwarded
              Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2892)
            Case Cage.CageOperationType.ProgressiveCancelProvision
              Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2892)
            Case Cage.CageOperationType.ProgressiveCancelAwarded
              Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2892)
            Case Cage.CageOperationType.ToTerminal
              Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2892)
          End Select

          Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREEN_01)
          Me.m_ok_rows += 1

        Case Cage.CageStatus.CanceledDueToImbalanceOrOther
          Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2893)
          Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

        Case Cage.CageStatus.OkDenominationImbalance
          If DbRow.Value(SQL_COLUMN_OPERATION_TYPE) = Cage.CageOperationType.ToCashier Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2944)
          Else
            Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3021)
          End If

          Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_OCHRE_00)
          Me.m_denomination_imbalance_rows += 1

        Case Cage.CageStatus.OkAmountImbalance
          If DbRow.Value(SQL_COLUMN_OPERATION_TYPE) = Cage.CageOperationType.ToCashier Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2894)
          Else
            Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3022)
          End If
          Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_ORANGE_01)
          Me.m_total_imbalance_rows += 1

        Case Cage.CageStatus.Canceled
          If DbRow.Value(SQL_COLUMN_OPERATION_TYPE) = Cage.CageOperationType.RequestOperation Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4787)
          Else
            Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1891)
          End If
          Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_03)
          Me.m_canceled_rows += 1

          'HBB 12-01-2015  -	New Status: �Pendiente recaudar(cancelado)�
        Case Cage.CageStatus.CanceledUncollectedCashierRetirement
          Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5848)
          Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_03)
          Me.m_canceled_rows += 1

        Case Cage.CageStatus.SentToCustom
          Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2892)
          Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREEN_01)
          Me.m_ok_rows += 1

        Case Cage.CageStatus.ReceptionFromCustom
          Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3305)
          Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREEN_01)
          Me.m_ok_rows += 1

        Case Cage.CageStatus.FinishedOpenCloseCage _
           , Cage.CageStatus.FinishedCountCage
          Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2892)
          Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
          Me.m_open_close_cage_rows += 1

        Case Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = "---"

      End Select
      Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_ID).Value = DbRow.Value(SQL_COLUMN_STATUS)
    End If

    ' GUIUser ID
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_GUI_USER_ID)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GUI_USER_ID).Value = DbRow.Value(SQL_COLUMN_GUI_USER_ID)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GUI_USER_ID).Value = AUDIT_NONE_STRING
    End If

    ' GUFull Name
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_GU_FULL_NAME)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GU_FULL_NAME).Value = DbRow.Value(SQL_COLUMN_GU_FULL_NAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GU_FULL_NAME).Value = AUDIT_NONE_STRING
    End If

    ' CGS_WORKING_DAY
    If Not DbRow.IsNull(SQL_COLUMN_WORKING_DAY) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_WORKING_DAY).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_WORKING_DAY), _
                                                                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                   ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_WORKING_DAY).Value = ""
    End If

    ' Gambling table id
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_GAMBLING_TABLE_ID)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GAMBLING_TABLE_ID).Value = DbRow.Value(SQL_COLUMN_GAMBLING_TABLE_ID)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GAMBLING_TABLE_ID).Value = 0
    End If

    ' Progressive provision id
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_RELATED_ID)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_RELATED_ID).Value = DbRow.Value(SQL_COLUMN_RELATED_ID)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_RELATED_ID).Value = 0
    End If

    ' Gaming Table Session Id
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_GAMING_TABLE_SESSION_ID)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GAMING_TABLE_SESSION_ID).Value = DbRow.Value(SQL_COLUMN_GAMING_TABLE_SESSION_ID)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GAMING_TABLE_SESSION_ID).Value = 0
    End If

    ' Terminal Type
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_TERMINAL_TYPE)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_TYPE).Value = DbRow.Value(SQL_COLUMN_TERMINAL_TYPE)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_TYPE).Value = -1
    End If

    ' Add currency exchange columns
    If Not m_currency_exchange_iso_codes Is Nothing And m_session_id <> -1 Then
      ' Currencies
      _column_offset = GRID_COLUMNS
      For _index_session As Int32 = 0 To m_movements_totals.Rows.Count - 1
        If m_movements_totals.Rows(_index_session).Item(SQL_COLUMN_CGM_MOVEMENT_ID) = DbRow.Value(SQL_COLUMN_CGM_MOVEMENT_ID) Then
          For _index As Int32 = 0 To m_currency_exchange_iso_codes.Count() - 1
            _column_offset = GRID_COLUMNS + (_index * 3)

            If m_currency_exchange_iso_codes(_index) = m_movements_totals.Rows(_index_session).Item(SQL_COLUMN_CMD_ISO_CODE) Then

              Me.Grid.Cell(RowIndex, _column_offset).Value = FormatAmount(m_movements_totals.Rows(_index_session).Item(SQL_COLUMN_CMD_DEPOSITS), m_currency_exchange_iso_codes(_index))
              _column_offset += 1
              Me.Grid.Cell(RowIndex, _column_offset).Value = FormatAmount(m_movements_totals.Rows(_index_session).Item(SQL_COLUMN_CMD_WITHDRAWALS), m_currency_exchange_iso_codes(_index))
              _column_offset += 1
              Me.Grid.Cell(RowIndex, _column_offset).Value = FormatAmount(m_movements_totals.Rows(_index_session).Item(SQL_COLUMN_CMD_DIFFERENCE), m_currency_exchange_iso_codes(_index))
              _column_offset += 1

            End If
          Next
        End If
      Next
    End If

    _iso_code_column = m_total_columns - Me.m_iso_codes_sql.Count
    For Each _isoCode As KeyValuePair(Of Integer, String) In Me.m_iso_codes_sql
      Me.Grid.Cell(RowIndex, _iso_code_column).Value = GUI_FormatNumber(DbRow.Value(_isoCode.Key), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      _iso_code_column += 1
    Next

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Enable button in selected row
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_RowSelectedEvent(ByVal SelectedRow As Integer)
    Dim _str_operation_type_id As String
    Dim _str_status_id As String

    If SelectedRow >= 0 And Grid.NumRows > 0 Then
      _str_operation_type_id = Me.Grid.Cell(SelectedRow, GRID_COLUMN_OPERATION_TYPE_ID).Value
      If Not String.IsNullOrEmpty(_str_operation_type_id) Then
        Select Case _str_operation_type_id
          Case Cage.CageOperationType.ToCashier
            Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(474)
            Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Enabled = True
          Case Cage.CageOperationType.OpenCage _
             , Cage.CageOperationType.CloseCage _
             , Cage.CageOperationType.ReOpenCage _
             , Cage.CageOperationType.GainQuadrature _
             , Cage.CageOperationType.LostQuadrature
            Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Enabled = False
          Case Else
            _str_status_id = Me.Grid.Cell(SelectedRow, GRID_COLUMN_STATUS_ID).Value
            If Not String.IsNullOrEmpty(_str_status_id) Then
              If _str_status_id = Cage.CageStatus.Sent And Not _str_operation_type_id = Cage.CageOperationType.RequestOperation Then
                Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2904)   ' Recaudar
                Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Enabled = Not GLB_CurrentUser.IsSuperUser
              Else
                Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(474)    ' Detalle
                Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Enabled = Not _str_operation_type_id = Cage.CageOperationType.RequestOperation Or (Not GLB_CurrentUser.IsSuperUser And _str_operation_type_id = Cage.CageOperationType.RequestOperation)
              End If
            End If
        End Select
      End If
    End If

  End Sub ' GUI_RowSelectedEvent

  ' PURPOSE: Functions after last row
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    If Me.Grid.NumRows > 0 Then
      If Not opt_monitor_mode.Checked Then
        GUI_RowSelectedEvent(0)
      End If
    End If

    Me.Grid.Counter(COUNTER_SENT).Value = IIf(opt_monitor_mode.Checked, Me.Grid.NumRows, Me.m_sent_rows)
    Me.Grid.Counter(COUNTER_OK).Value = Me.m_ok_rows
    Me.Grid.Counter(COUNTER_DENOMINATION_IMBALANCE).Value = Me.m_denomination_imbalance_rows
    Me.Grid.Counter(COUNTER_TOTAL_IMBALANCE).Value = Me.m_total_imbalance_rows
    Me.Grid.Counter(COUNTER_CANCELED).Value = Me.m_canceled_rows
    Me.Grid.Counter(COUNTER_OPEN_CLOSE_CAGE).Value = Me.m_open_close_cage_rows

    Me.m_sent_rows = 0
    Me.m_ok_rows = 0
    Me.m_denomination_imbalance_rows = 0
    Me.m_total_imbalance_rows = 0
    Me.m_canceled_rows = 0
    Me.m_open_close_cage_rows = 0
  End Sub

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.dtp_from
  End Sub 'GUI_SetInitialFocus

  ' PURPOSE: Initialize members and accumulators related to the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  '
  Protected Overrides Sub GUI_BeforeFirstRow()
    If Not opt_monitor_mode.Checked Then
      ' Currency Exchange columns
      LoadIsoCodes(m_currency_exchange_iso_codes, m_currency_exchange_iso_description)
      Call GUI_StyleSheet()

    End If

  End Sub ' GUI_BeforeFirstRow

  ' PURPOSE: Control dispose form
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        Call components.Dispose()
      End If
    End If

    If m_timer IsNot Nothing Then
      RaiseEvent m_dispose_pending_request()
    End If

    Call MyBase.Dispose(disposing)
  End Sub


  ' PURPOSE: Get the defined Query Type
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_QUERY_TYPE
  '
  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM
  End Function ' GUI_GetQueryType

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    PrintData.FilterHeaderWidth(1) = 2000
    PrintData.FilterHeaderWidth(2) = 2000
    PrintData.FilterHeaderWidth(3) = 2000

    ' Date
    PrintData.SetFilter(gb_date.Text, m_filter_date_type)
    PrintData.SetFilter(gb_date.Text & " " & dtp_from.Text, m_date_from)
    PrintData.SetFilter(gb_date.Text & " " & dtp_to.Text, m_date_to)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)

    ' Status
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1090), m_status)

    ' Employee
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(254), m_operation_type)

    If m_cashier_session_id > 0 Then
      PrintData.SetFilter(tf_cashier_session.Text, m_cashier_session_name)
    Else
      PrintData.SetFilter(gb_order_by.Text, m_order_by) 'Order by
      PrintData.SetFilter("", "", True)
      PrintData.SetFilter("", "", True)
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(3392), m_cashier_name)  ' Cashier name
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(220), m_user_name)            ' User name
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(3388), m_cage_session)  ' Cage Session
    End If

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()
    Dim all_checked As Boolean

    m_date_from = ""
    m_date_to = ""
    m_operation_type = ""
    m_status = ""
    m_cashier_name = ""
    m_user_name = ""
    m_cage_session = ""
    m_pending = ""
    m_order_by = ""
    m_filter_date_type = ""

    ' Date 
    If opt_movement_date.Checked Then
      m_filter_date_type = opt_movement_date.Text
    ElseIf opt_working_day.Checked Then
      m_filter_date_type = opt_working_day.Text
    End If
    If Me.dtp_from.Checked Then
      If opt_working_day.Checked Then
        m_date_from = GUI_FormatDate(dtp_from.Value, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
      Else
        m_date_from = GUI_FormatDate(dtp_from.Value, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      End If
    End If
    If Me.dtp_to.Checked Then
      If opt_working_day.Checked Then
        m_date_to = GUI_FormatDate(dtp_to.Value, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
      Else
        m_date_to = GUI_FormatDate(dtp_to.Value, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      End If
    End If

    ' Status
    all_checked = True

    If uc_checked_list_status.SelectedIndexes.Length <> uc_checked_list_status.Count() Then
      all_checked = False
    End If

    If all_checked = True Then
      m_status = GLB_NLS_GUI_AUDITOR.GetString(263)
    Else
      m_status = uc_checked_list_status.SelectedValuesList()
    End If

    ' operation
    If Me.opt_from_cage.Checked Then
      m_operation_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4358)
    End If
    If Me.opt_from_cashier.Checked Then
      m_operation_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4359)
    End If
    If Me.opt_all.Checked Then
      m_operation_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353)
    End If

    ' users
    If Me.opt_all_users.Checked Then
      m_user_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353)
    ElseIf Me.opt_one_user.Checked Then
      m_user_name = Me.cmb_user.TextValue
    End If

    ' Cage Session
    If Me.opt_all_sessions.Checked Then
      m_cage_session = GLB_NLS_GUI_PLAYER_TRACKING.GetString(287)
    ElseIf Me.opt_one_session.Checked Then
      m_cage_session = Me.cmb_sessions.TextValue
    End If

    ' Cashier
    If Me.opt_all_cashiers.Checked Then
      m_cashier_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353)
    Else
      ' ICS 21-FEB-2014
      If rb_cashier.Checked Then
        m_cashier_name = rb_cashier.Text
      ElseIf rb_gaming_table.Checked Then
        m_cashier_name = rb_gaming_table.Text
      ElseIf rb_custom.Checked Then
        m_cashier_name = rb_custom.Text
      ElseIf rb_terminal.Checked Then
        m_cashier_name = rb_terminal.Text
      End If

      m_cashier_name &= ": " & IIf(Me.cmb_cashier.Value = -1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353), cmb_cashier.TextValue)
    End If

    'pending
    m_pending = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2911)

    'Order by

    If opt_by_cashier_session.Checked Then
      m_order_by = opt_by_cashier_session.Text
    Else
      m_order_by = opt_by_date.Text ' by default
    End If

  End Sub ' GUI_ReportUpdateFilters

  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(ExcelData)

#If DEBUG Then
    ExcelData.SetColumnFormat(9, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT) ' Working day
#Else
    ExcelData.SetColumnFormat(7, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT) ' Working day
#End If

    ExcelData.SetColumnFormat(8, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT) ' Cage Session Name

  End Sub ' GUI_ReportParams

#End Region ' GUI Reports

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window, _
                         Optional ByVal SessionId As Int64 = -1, _
                         Optional ByVal SessionClosed As Boolean = False, _
                         Optional ByVal MovType As Cage.MovTypeToShow = Cage.MovTypeToShow.None, _
                         Optional ByRef Timer As System.Timers.Timer = Nothing, _
                         Optional ByVal FiltersReadOnly As Boolean = False)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_SELECTION

    m_init_date_from = Nothing
    m_init_date_to = Nothing
    m_filters_read_only = FiltersReadOnly

    Me.MdiParent = MdiParent
    Me.Display(False)


    If SessionId <> -1 Then
      If Me.Permissions.Read Then
        Call Application.DoEvents()
        System.Threading.Thread.Sleep(100)
        Call Application.DoEvents()

        Me.m_session_id = SessionId
        Me.dtp_from.Checked = False
        Me.opt_all.Checked = True
        Me.gb_sessions.Enabled = False
        Me.cmb_sessions.Value = SessionId
        Me.opt_one_session.Checked = True
        Me.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)
        Me.Text = Me.Text & " - " & Cage.GetCageSessionName(Me.m_session_id)
      End If
    End If

    If Timer IsNot Nothing Then
      m_timer = Timer
    End If


    Select Case MovType
      Case Cage.MovTypeToShow.CashierMoneyRequest
        Me.dtp_from.Checked = False
        Me.opt_from_cage.Checked = True
        FillCheckedlistFilterGrid(True, False)
        m_mode_edit = MODE_EDIT.MONEY_REQUEST
        Me.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)
        Me.uc_checked_list_status.btn_uncheck_all.PerformClick()
        Me.uc_checked_list_status.SetValue(m_request_operation_pos - 1)

      Case Cage.MovTypeToShow.MoneyCollect
        Me.dtp_from.Checked = False
        Me.opt_from_cashier.Checked = True
        FillCheckedlistFilterGrid(False, True)

        Me.uc_checked_list_status.btn_uncheck_all.PerformClick()
        Me.uc_checked_list_status.SetValue(0)
        m_mode_edit = MODE_EDIT.COLLECTION
        Me.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)

      Case Cage.MovTypeToShow.AllPending
      Case Else
        If m_filters_read_only Then
          Me.uc_checked_list_status.btn_uncheck_all.PerformClick()
          Me.uc_checked_list_status.SetValue(m_request_operation_pos - 1)
        End If
    End Select


    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Enabled = Not SessionClosed And Not GLB_CurrentUser.IsSuperUser

    If m_filters_read_only Then
      Me.gb_date.Enabled = False
      Me.gb_operation.Enabled = False
      Me.gb_cashier.Enabled = False
      Me.gb_user.Enabled = False
      Me.gb_sessions.Enabled = False
      Me.gb_mode.Enabled = False
      Me.opt_history_mode.Checked = True
      Me.gb_order_by.Enabled = False
      Me.uc_checked_list_status.Enabled = False

      Me.GUI_Button(ENUM_BUTTON.BUTTON_FILTER_RESET).Enabled = False

    End If

  End Sub

  Public Sub ShowCashierSessionMovements(ByVal CashierSessionId As Int64, ByVal CashierSessionName As String, Optional ByVal DisplayModal As Boolean = False, Optional ByVal MdiParent As System.Windows.Forms.IWin32Window = Nothing)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_SELECTION

    Me.m_cashier_session_id = CashierSessionId
    Me.m_cashier_session_name = CashierSessionName
    Me.m_session_id = -1

    If DisplayModal Then
      Me.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedSingle
      Me.MinimizeBox = False
    Else
      Me.MdiParent = MdiParent
    End If

    Me.Display(DisplayModal)

  End Sub

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()

    Dim _column_offset As Integer
    Dim _header_to_type As Int16
    Dim _iso_code_column As Integer

    m_total_columns = GRID_COLUMNS

    If Not m_currency_exchange_iso_codes Is Nothing And m_session_id <> -1 Then
      m_total_columns += (m_currency_exchange_iso_codes.Count() * 3)
    End If

    m_total_columns += Me.m_iso_codes_sql.Count

    With Me.Grid
      If Not m_currency_exchange_iso_codes Is Nothing And m_session_id <> -1 Then
        Call .Init(m_total_columns, GRID_HEADER_ROWS_FROM_SESSIONS)
        .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
        .Column(GRID_COLUMN_MOVEMENT_ID).Header(0).Text = " "
        .Column(GRID_COLUMN_MOVEMENT_DATETIME).Header(0).Text = " "
        .Column(GRID_COLUMN_TERMINAL_CAGE_ID).Header(0).Text = " "
        .Column(GRID_COLUMN_TERMINAL_CAGE_NAME).Header(0).Text = " "
        .Column(GRID_COLUMN_USER_CAGE_NAME).Header(0).Text = " "
        .Column(GRID_COLUMN_TERMINAL_CASHIER_ID).Header(0).Text = " "
        .Column(GRID_COLUMN_TERMINAL_CASHIER_NAME).Header(0).Text = " "
        .Column(GRID_COLUMN_USER_CASHIER_NAME).Header(0).Text = " "
        .Column(GRID_COLUMN_OPERATION_TYPE).Header(0).Text = " "
        .Column(GRID_COLUMN_OPERATION_TYPE_ID).Header(0).Text = " "
        .Column(GRID_COLUMN_STATUS).Header(0).Text = " "
        .Column(GRID_COLUMN_STATUS_ID).Header(0).Text = " "
        .Column(GRID_COLUMN_GUI_USER_ID).Header(0).Text = " "
        .Column(GRID_COLUMN_GU_FULL_NAME).Header(0).Text = " "
        .Column(GRID_COLUMN_USER_CAGE_NAME_LAST_MODIFICATION).Header(0).Text = " "
        .Column(GRID_COLUMN_MOVEMENT_DATETIME_LAST_MODIFICATION).Header(0).Text = " "
        .Column(GRID_COLUMN_RELATED_ID).Header(0).Text = " "
        .Column(GRID_COLUMN_GAMING_TABLE_SESSION_ID).Header(0).Text = " "
        _header_to_type = 1
      Else
        Call .Init(m_total_columns, GRID_HEADER_ROWS)
        _header_to_type = 0
      End If

      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      SetCounterColors()

      ' HightLight When Selected
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = True

      For _count As Integer = GRID_COLUMN_INDEX + 1 To m_total_columns - 1
        .Column(_count).HighLightWhenSelected = False
      Next

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(_header_to_type).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      'Movement ID
      .Column(GRID_COLUMN_MOVEMENT_ID).Header(_header_to_type).Text = "" 'xMovement ID
      .Column(GRID_COLUMN_MOVEMENT_ID).Width = GRID_WIDTH_COLUMN_MOVEMENT_ID
      .Column(GRID_COLUMN_MOVEMENT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'Terminal ID (cage)
      .Column(GRID_COLUMN_MOVEMENT_DATETIME).Header(_header_to_type).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1050)
      .Column(GRID_COLUMN_MOVEMENT_DATETIME).Width = GRID_WIDTH_COLUMN_MOVEMENT_DATETIME
      .Column(GRID_COLUMN_MOVEMENT_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      'Terminal ID (cage)
      .Column(GRID_COLUMN_TERMINAL_CAGE_ID).Header(_header_to_type).Text = "" 'xTerminal Cage ID"
      .Column(GRID_COLUMN_TERMINAL_CAGE_ID).Width = GRID_WIDTH_COLUMN_TERMINAL_CAGE_ID
      .Column(GRID_COLUMN_TERMINAL_CAGE_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'Terminal (cage)
      .Column(GRID_COLUMN_TERMINAL_CAGE_NAME).Header(_header_to_type).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2895)
      .Column(GRID_COLUMN_TERMINAL_CAGE_NAME).Width = GRID_WIDTH_COLUMN_TERMINAL_CAGE_NAME
      .Column(GRID_COLUMN_TERMINAL_CAGE_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'User (cashier)
      .Column(GRID_COLUMN_USER_CAGE_NAME).Header(_header_to_type).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2896)
      .Column(GRID_COLUMN_USER_CAGE_NAME).Width = GRID_WIDTH_COLUMN_USER_CAGE_NAME
      .Column(GRID_COLUMN_USER_CAGE_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'Terminal ID (cashier)
      .Column(GRID_COLUMN_TERMINAL_CASHIER_ID).Header(_header_to_type).Text = "" 'xTerminal Cashier ID"
      .Column(GRID_COLUMN_TERMINAL_CASHIER_ID).Width = GRID_WIDTH_COLUMN_TERMINAL_CASHIER_ID
      .Column(GRID_COLUMN_TERMINAL_CASHIER_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'Terminal (cashier)
      .Column(GRID_COLUMN_TERMINAL_CASHIER_NAME).Header(_header_to_type).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(492)
      .Column(GRID_COLUMN_TERMINAL_CASHIER_NAME).Width = GRID_WIDTH_COLUMN_TERMINAL_CASHIER_NAME
      .Column(GRID_COLUMN_TERMINAL_CASHIER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'User (cashier) / custom
      If Me.rb_gaming_table.Checked Then
        .Column(GRID_COLUMN_USER_CASHIER_NAME).Header(_header_to_type).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4396)
      ElseIf Me.rb_custom.Checked Then
        .Column(GRID_COLUMN_USER_CASHIER_NAME).Header(_header_to_type).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3393)
      ElseIf Me.rb_cashier.Checked Then
        .Column(GRID_COLUMN_USER_CASHIER_NAME).Header(_header_to_type).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1181)
      ElseIf Me.rb_terminal.Checked Then
        .Column(GRID_COLUMN_USER_CASHIER_NAME).Header(_header_to_type).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5364)
      Else
        .Column(GRID_COLUMN_USER_CASHIER_NAME).Header(_header_to_type).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5479)
      End If
      .Column(GRID_COLUMN_USER_CASHIER_NAME).Width = GRID_WIDTH_COLUMN_USER_CASHIER_NAME
      .Column(GRID_COLUMN_USER_CASHIER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'Operation
      .Column(GRID_COLUMN_OPERATION_TYPE).Header(_header_to_type).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(254)
      .Column(GRID_COLUMN_OPERATION_TYPE).Width = GRID_WIDTH_COLUMN_OPERATION
      .Column(GRID_COLUMN_OPERATION_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'Operation ID
      .Column(GRID_COLUMN_OPERATION_TYPE_ID).Header(_header_to_type).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(254)
      .Column(GRID_COLUMN_OPERATION_TYPE_ID).Width = GRID_WIDTH_COLUMN_OPERATION_ID
      .Column(GRID_COLUMN_OPERATION_TYPE_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'Status
      .Column(GRID_COLUMN_STATUS).Header(_header_to_type).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1090)
      .Column(GRID_COLUMN_STATUS).Width = GRID_WIDTH_COLUMN_STATUS
      .Column(GRID_COLUMN_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'Status ID
      .Column(GRID_COLUMN_STATUS_ID).Header(_header_to_type).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1090)
      .Column(GRID_COLUMN_STATUS_ID).Width = GRID_WIDTH_COLUMN_STATUS_ID
      .Column(GRID_COLUMN_STATUS_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'GUIUser ID
      .Column(GRID_COLUMN_GUI_USER_ID).Header(_header_to_type).Text = ""
      .Column(GRID_COLUMN_GUI_USER_ID).Width = GRID_WIDTH_COLUMN_GUI_USER_ID
      .Column(GRID_COLUMN_GUI_USER_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      'GUFull Name
      .Column(GRID_COLUMN_GU_FULL_NAME).Header(_header_to_type).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3388) ' Sesi�n de b�veda
      .Column(GRID_COLUMN_GU_FULL_NAME).Width = GRID_WIDTH_COLUMN_GU_FULL_NAME
      .Column(GRID_COLUMN_GU_FULL_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Working Day
      .Column(GRID_COLUMN_WORKING_DAY).Header(0).Text = " "
      .Column(GRID_COLUMN_WORKING_DAY).Header(_header_to_type).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5723) ' Working day
      .Column(GRID_COLUMN_WORKING_DAY).Width = GRID_WIDTH_COLUMN_WORKING_DAY
      .Column(GRID_COLUMN_WORKING_DAY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      'Gambling Table ID
      .Column(GRID_COLUMN_GAMBLING_TABLE_ID).Header(_header_to_type).Text = ""
      .Column(GRID_COLUMN_GAMBLING_TABLE_ID).Width = GRID_WIDTH_COLUMN_GAMBLING_TABLE_ID
      .Column(GRID_COLUMN_GAMBLING_TABLE_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'Progressive provision ID
      .Column(GRID_COLUMN_RELATED_ID).Header(_header_to_type).Text = ""
      .Column(GRID_COLUMN_RELATED_ID).Width = GRID_WIDTH_COLUMN_RELATED_ID
      .Column(GRID_COLUMN_RELATED_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'Gaming Table Session ID
      .Column(GRID_COLUMN_GAMING_TABLE_SESSION_ID).Header(_header_to_type).Text = ""
      .Column(GRID_COLUMN_GAMING_TABLE_SESSION_ID).Width = 0

      ' Terminal Type
      .Column(GRID_COLUMN_TERMINAL_TYPE).Header(_header_to_type).Text = ""
      .Column(GRID_COLUMN_TERMINAL_TYPE).Width = 0

      If Not m_currency_exchange_iso_codes Is Nothing And m_session_id <> -1 Then
        _column_offset = 0

        ' Add currency exchange columns
        For _index As Integer = 0 To m_currency_exchange_iso_codes.Count() - 1
          ' Expected column
          .Column(GRID_COLUMNS + _column_offset).Header(0).Text = m_currency_exchange_iso_description(_index).ToString() & _
                                                                  " [" & m_currency_exchange_iso_codes(_index).ToString() & "]"
          .Column(GRID_COLUMNS + _column_offset).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(496)                  ' Dep�sitos
          .Column(GRID_COLUMNS + _column_offset).Width = GRID_WIDTH_COLUMN_CGS_CURRENCIES
          .Column(GRID_COLUMNS + _column_offset).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          _column_offset += 1

          ' Collected column
          .Column(GRID_COLUMNS + _column_offset).Header(0).Text = m_currency_exchange_iso_description(_index).ToString() & _
                                                                  " [" & m_currency_exchange_iso_codes(_index).ToString() & "]"
          .Column(GRID_COLUMNS + _column_offset).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(497)                  ' Retiros
          .Column(GRID_COLUMNS + _column_offset).Width = GRID_WIDTH_COLUMN_CGS_CURRENCIES
          .Column(GRID_COLUMNS + _column_offset).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          _column_offset += 1

          ' Difference column          
          .Column(GRID_COLUMNS + _column_offset).Header(0).Text = m_currency_exchange_iso_description(_index).ToString() & _
                                                                  " [" & m_currency_exchange_iso_codes(_index).ToString() & "]"
          .Column(GRID_COLUMNS + _column_offset).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(499)            ' Diferencia
          .Column(GRID_COLUMNS + _column_offset).Width = GRID_WIDTH_COLUMN_CGS_CURRENCIES
          .Column(GRID_COLUMNS + _column_offset).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          _column_offset += 1

        Next
      End If

      'User (last modification)
      .Column(GRID_COLUMN_USER_CAGE_NAME_LAST_MODIFICATION).Header(_header_to_type).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4613)
      .Column(GRID_COLUMN_USER_CAGE_NAME_LAST_MODIFICATION).Width = GRID_WIDTH_COLUMN_USER_CAGE_NAME
      .Column(GRID_COLUMN_USER_CAGE_NAME_LAST_MODIFICATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'Movement datetime last modification
      .Column(GRID_COLUMN_MOVEMENT_DATETIME_LAST_MODIFICATION).Header(_header_to_type).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4614)
      .Column(GRID_COLUMN_MOVEMENT_DATETIME_LAST_MODIFICATION).Width = GRID_WIDTH_COLUMN_MOVEMENT_DATETIME_LAST_MODIFICATION
      .Column(GRID_COLUMN_MOVEMENT_DATETIME_LAST_MODIFICATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      _iso_code_column = m_total_columns - Me.m_iso_codes_sql.Count
      For Each _isoCode As KeyValuePair(Of Integer, String) In Me.m_iso_codes_sql
        If _header_to_type = 1 Then
          .Column(_iso_code_column).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2078)
        End If
        .Column(_iso_code_column).Header(_header_to_type).Text = _isoCode.Value
        .Column(_iso_code_column).Width = GRID_WIDTH_COLUMN_CGS_CURRENCIES
        .Column(_iso_code_column).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
        _iso_code_column = _iso_code_column + 1
      Next

    End With

  End Sub 'GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Dim _closing_time As Integer
    Dim _initial_time As Date
    Dim _now As Date

    _now = WGDB.Now

    Me.opt_movement_date.Checked = True

    _closing_time = GetDefaultClosingTime()
    _initial_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)

    If _initial_time > _now Then
      _initial_time = _initial_time.AddDays(-1)
    End If

    If m_init_date_from <> Nothing Then
      Me.dtp_from.Value = m_init_date_from
      Me.dtp_from.Checked = True
    Else
      Me.dtp_from.Value = _initial_time
      Me.dtp_from.Checked = True
    End If
    If m_init_date_to <> Nothing Then
      Me.dtp_to.Value = m_init_date_to
      Me.dtp_to.Checked = True
    Else
      Me.dtp_to.Value = _initial_time.AddDays(1)
      Me.dtp_to.Checked = False
    End If

    Me.opt_all_users.Checked = True
    Me.cmb_user.Enabled = False
    Me.chk_show_all.Checked = False
    Me.chk_show_all.Enabled = False

    Me.opt_from_cage.Checked = False
    If Me.m_mode_edit = MODE_EDIT.COLLECTION Then
      Me.opt_from_cashier.Checked = True
      Me.opt_all.Checked = False
      Me.opt_from_cage.Enabled = False
      Me.opt_all.Enabled = False
      Call Me.uc_checked_list_status.SetDefaultValue(False)
      Call Me.uc_checked_list_status.SetValue(0)
      Me.uc_checked_list_status.Enabled = False
      Me.rb_cashier.Checked = True
      Call Me.rb_custom_gaming_cashier_terminal_click(Nothing, Nothing)
      Me.cmb_cashier.Enabled = True
      Me.opt_all_cashiers.Enabled = False
      Me.rb_gaming_table.Enabled = False
      Me.rb_custom.Enabled = False
      Me.rb_terminal.Enabled = False
    Else
      Me.opt_from_cashier.Checked = False
      Me.opt_all.Checked = True
      Me.opt_from_cage.Enabled = True
      Me.opt_all.Enabled = True
      Call Me.uc_checked_list_status.SetDefaultValue(True)
      Me.opt_all_cashiers.Checked = True
      Me.cmb_cashier.Enabled = False
    End If

    Me.opt_all_sessions.Checked = True

    Me.opt_by_date.Checked = True

    If m_cashier_session_id > 0 Then
      Me.dtp_from.Checked = False
      Me.dtp_to.Checked = False
      Me.gb_sessions.Visible = False
      Me.gb_user.Visible = False
      Me.gb_cashier.Visible = False
      Me.gb_order_by.Visible = False
    End If

    Me.opt_history_mode.Checked = True

  End Sub 'SetDefaultValues


  ' PURPOSE: Get query Sql with iso codes total amount 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - Sring
  '
  ' RETURNS:
  '     - String
  Private Function GetSqlIsoCodesAmount() As String
    Dim _sql As StringBuilder

    _sql = New StringBuilder()

    _sql.AppendLine("DECLARE @_count INTEGER	                             ")
    _sql.AppendLine("DECLARE @_count_aux INTEGER	                         ")
    _sql.AppendLine("DECLARE @_isoCodeLocal AS NVARCHAR(MAX)               ")
    _sql.AppendLine("DECLARE @cols AS NVARCHAR(MAX)                        ")
    _sql.AppendLine("DECLARE @cols_type_1 AS NVARCHAR(MAX)                 ")
    _sql.AppendLine("DECLARE @cols_type_2 AS NVARCHAR(MAX)                 ")
    _sql.AppendLine("DECLARE @colsFormat AS NVARCHAR(MAX)                  ")
    _sql.AppendLine("DECLARE @query  AS NVARCHAR(MAX)                      ")
    _sql.AppendLine("DECLARE @_counter INTEGER                             ")
    _sql.AppendLine("DECLARE @_isoCode NVARCHAR(MAX)                       ")
    _sql.AppendLine("DECLARE @_isoCodeColumn NVARCHAR(MAX)                 ")
    _sql.AppendLine("DECLARE @_dataType NVARCHAR(4)                        ")
    _sql.AppendLine("DECLARE @_queryColumns NVARCHAR(MAX)                  ")
    _sql.AppendLine("DECLARE @cols_update_1 AS NVARCHAR(MAX)               ")
    _sql.AppendLine("DECLARE @cols_update_2 AS NVARCHAR(MAX)               ")
    _sql.AppendLine("DECLARE @_activeDualCurrency NVARCHAR(1)              ")

    _sql.AppendLine("SET @_isoCodeLocal = (SELECT GP.GP_KEY_VALUE                                                                     ")
    _sql.AppendLine("                        FROM GENERAL_PARAMS AS GP                                                                ")
    _sql.AppendLine("                        WHERE GP.GP_GROUP_KEY = 'RegionalOptions' AND GP.GP_SUBJECT_KEY = 'CurrencyISOCode')     ")

    _sql.AppendLine("SET @_activeDualCurrency = (SELECT CAST(GP.GP_KEY_VALUE AS BIT)                                                  ")
    _sql.AppendLine("                            FROM GENERAL_PARAMS AS GP                                                            ")
    _sql.AppendLine("                            WHERE GP.GP_GROUP_KEY = 'FloorDualCurrency' AND GP.GP_SUBJECT_KEY = 'Enabled')       ")

    _sql.AppendLine(" CREATE TABLE #TABLE_ISO_CODE_DATA ")
    _sql.AppendLine(" (                                 ")
    _sql.AppendLine("   ID INT IDENTITY(1,1),           ")
    _sql.AppendLine("   ISO_CODE_COLUMN VARCHAR(7),     ")
    _sql.AppendLine("   ISO_CODE VARCHAR(3),            ")
    _sql.AppendLine("   DATA_TYPE INTEGER,              ")
    _sql.AppendLine("   CUR_ORDER INTEGER               ")
    _sql.AppendLine(" )                                 ")
    _sql.AppendLine("     INSERT INTO #TABLE_ISO_CODE_DATA                                                                             ")
    _sql.AppendLine("    SELECT   CASE WHEN CD.CMD_ISO_CODE='X01' THEN @_isoCodeLocal ELSE CD.CMD_ISO_CODE END AS CMD_ISO_CODE_COLUMN  ")
    _sql.AppendLine("           , CASE WHEN CD.CMD_ISO_CODE='X01' THEN @_isoCodeLocal ELSE CD.CMD_ISO_CODE END AS CMD_ISO_CODE         ")
    _sql.AppendLine("           , CASE WHEN CD.CMD_CAGE_CURRENCY_TYPE > 1000                                                           ")
    _sql.AppendLine("                  THEN CD.CMD_CAGE_CURRENCY_TYPE                                                                  ")
    _sql.AppendLine("                  ELSE 0 END AS DATA_TYPE                                                                         ")
    _sql.AppendLine("           , ISNULL(CE_CURRENCY_ORDER, CASE WHEN CD.CMD_ISO_CODE='X02' THEN 9999 ELSE 0 END) AS CUR_ORDER         ")
    _sql.AppendLine("      FROM   CAGE_MOVEMENTS AS CM                                                                                 ")
    _sql.AppendLine(" LEFT JOIN   CAGE_MOVEMENT_DETAILS AS CD ON CM.CGM_MOVEMENT_ID = CD.CMD_MOVEMENT_ID                               ")
    _sql.AppendLine(" LEFT JOIN   CURRENCY_EXCHANGE ON CE_CURRENCY_ISO_CODE =  CMD_ISO_CODE AND CE_TYPE = 0                            ")
    _sql.AppendLine("     WHERE   CM.CGM_MOVEMENT_ID IN(SELECT CGM_MOVEMENT_ID FROM #TABLE_MOVEMENT_CAGE_DATA)                         ")
    _sql.AppendLine("       AND   CD.CMD_ISO_CODE IS NOT NULL                                                                          ")
    _sql.AppendLine("       AND   (CD.CMD_QUANTITY > 0 OR CD.CMD_QUANTITY IN (-1, -2, -100))                                           ")
    _sql.AppendLine("       AND   CM.CGM_TYPE NOT IN(200)                                                                              ")
    _sql.AppendLine("  GROUP BY   ISNULL(ce_currency_order, CASE WHEN CD.CMD_ISO_CODE='X02' THEN 9999 ELSE 0 END)                      ")
    _sql.AppendLine("           , CASE WHEN CD.CMD_ISO_CODE='X01' THEN @_isoCodeLocal ELSE CD.CMD_ISO_CODE END                         ")
    _sql.AppendLine("           , CASE WHEN CD.CMD_CAGE_CURRENCY_TYPE > 1000                                                           ")
    _sql.AppendLine("                  THEN CD.CMD_CAGE_CURRENCY_TYPE                                                                  ")
    _sql.AppendLine("                  ELSE 0 END                                                                                      ")
    _sql.AppendLine(" UNION                                                                                                            ")
    _sql.AppendLine("    SELECT   CASE WHEN CD.CMD_ISO_CODE='X01' THEN @_isoCodeLocal ELSE CD.CMD_ISO_CODE  END AS CMD_ISO_CODE_COLUMN ")
    _sql.AppendLine("           , CASE WHEN CD.CMD_ISO_CODE='X01' THEN @_isoCodeLocal ELSE CD.CMD_ISO_CODE END AS CMD_ISO_CODE         ")
    _sql.AppendLine("           , CASE WHEN CD.CMD_CAGE_CURRENCY_TYPE > 1000                                                           ")
    _sql.AppendLine("                  THEN CD.CMD_CAGE_CURRENCY_TYPE                                                                  ")
    _sql.AppendLine("                  ELSE 0 END AS DATA_TYPE                                                                         ")
    _sql.AppendLine("           , CASE WHEN CD.CMD_ISO_CODE='X02' THEN 9999 ELSE 0 END  AS CUR_ORDER                                   ")
    _sql.AppendLine("      FROM   CAGE_MOVEMENTS AS CM                                                                                 ")
    _sql.AppendLine(" LEFT JOIN   CAGE_MOVEMENT_DETAILS AS CD ON CM.CGM_MOVEMENT_ID = CD.CMD_MOVEMENT_ID                               ")
    _sql.AppendLine("     WHERE   CM.CGM_MOVEMENT_ID IN(SELECT CGM_MOVEMENT_ID FROM #TABLE_MOVEMENT_CAGE_DATA)                         ")
    _sql.AppendLine("       AND   CD.CMD_ISO_CODE IS NOT NULL                                                                          ")
    _sql.AppendLine("       AND   CD.CMD_ISO_CODE NOT IN ('X01')                                                                       ")
    _sql.AppendLine("       AND   CD.CMD_QUANTITY IN(-1, -2, -100)                                                                     ")
    _sql.AppendLine("       AND   CM.CGM_TYPE NOT IN(200)                                                                              ")
    _sql.AppendLine("       AND   NOT EXISTS(SELECT 1 FROM CURRENCY_EXCHANGE WHERE CE_CURRENCY_ISO_CODE =  CMD_ISO_CODE AND CE_TYPE = 0) ")
    _sql.AppendLine("  GROUP BY   CASE WHEN CD.CMD_ISO_CODE='X02' THEN 9999 ELSE 0 END                                                 ")
    _sql.AppendLine("           , CD.CMD_ISO_CODE                                                                                      ")
    _sql.AppendLine("           , CASE WHEN CD.CMD_CAGE_CURRENCY_TYPE > 1000                                                           ")
    _sql.AppendLine("                  THEN CD.CMD_CAGE_CURRENCY_TYPE                                                                  ")
    _sql.AppendLine("                  ELSE 0 END                                                                                      ")
    _sql.AppendLine("  ORDER BY   4, 2                                                                                                 ")

    _sql.AppendLine("SET @_counter = 1		                                                  ")
    _sql.AppendLine("SET @_count = (SELECT COUNT(*) FROM #TABLE_ISO_CODE_DATA) + 1          ")
    _sql.AppendLine("SET @_queryColumns = ''                                                ")
    _sql.AppendLine("SET @cols = ''                                                         ")
    _sql.AppendLine("SET @cols_type_1 = ''                                                  ")
    _sql.AppendLine("SET @cols_type_2 = ''                                                  ")
    _sql.AppendLine("SET @colsFormat =''                                                    ")
    _sql.AppendLine("SET @cols_update_1 = ''                                                ")
    _sql.AppendLine("SET @cols_update_2 = ''                                                ")

    _sql.AppendLine("WHILE NOT @_counter = @_count                                                                                                  ")
    _sql.AppendLine("    BEGIN                                                                                                                      ")
    _sql.AppendLine("   SELECT                                                                                                                      ")
    _sql.AppendLine("     @_isoCode  =  TIC.ISO_CODE ,                                                                                              ")
    _sql.AppendLine("     @_isoCodeColumn = TIC.ISO_CODE_COLUMN ,                                                                                   ")
    _sql.AppendLine("     @_dataType = cast(TIC.DATA_TYPE as nvarchar(4))")
    _sql.AppendLine("   FROM #TABLE_ISO_CODE_DATA AS TIC                                                                                            ")
    _sql.AppendLine("   WHERE TIC.id = @_counter                                                                                                    ")
    _sql.AppendLine("   SET @_queryColumns = 'ALTER TABLE #TABLE_MOVEMENT_CAGE_DATA ADD [' + @_isoCodeColumn + '_' +  @_dataType + '] MONEY '       ")
    _sql.AppendLine("   EXEC SP_EXECUTESQL @_queryColumns                                                                                           ")
    _sql.AppendLine("   IF @_dataType <> 9999                                                                                                       ")
    _sql.AppendLine("     BEGIN                                                                                                                     ")
    _sql.AppendLine("         SET @cols = @cols + ',AM.'+ @_isoCode + '_' +  @_dataType                                                             ")
    _sql.AppendLine("         IF @cols_type_1 = ''                                                                                                  ")
    _sql.AppendLine("           BEGIN                                                                                                               ")
    _sql.AppendLine("             SET @cols_type_1 = @_isoCode + '_' +  @_dataType                                                                  ")
    _sql.AppendLine("             SET @cols_update_1 = 'CM.'+@_isoCodeColumn + '_' +  @_dataType+' = AM.'+@_isoCode + '_' +  @_dataType+''          ")
    _sql.AppendLine("           END                                                                                                                 ")
    _sql.AppendLine("         ELSE                                                                                                                  ")
    _sql.AppendLine("           BEGIN                                                                                                               ")
    _sql.AppendLine("             SET @cols_type_1 = @cols_type_1 + ',' + @_isoCode + '_' +  @_dataType                                             ")
    _sql.AppendLine("             SET @cols_update_1 = @cols_update_1 +','+'CM.'+@_isoCodeColumn + '_' +  @_dataType+' = AM.'+@_isoCode + '_' +  @_dataType+''          ")
    _sql.AppendLine("           END                                                                                                                 ")
    _sql.AppendLine("         SET @colsFormat = @colsFormat + ',ISNULL('+@_isoCodeColumn + '_' +  @_dataType+', 0) AS '+@_isoCode + '_' +  @_dataType+''                ")
    _sql.AppendLine("     END                                                                                                                       ")
    _sql.AppendLine("   IF @_dataType = 9999                                                                                                        ")
    _sql.AppendLine("     BEGIN                                                                                                                     ")
    _sql.AppendLine("         SET @cols = @cols + ',AZ.'+@_isoCode+''                                                                               ")
    _sql.AppendLine("         IF @cols_type_2 = ''                                                                                                  ")
    _sql.AppendLine("           BEGIN                                                                                                               ")
    _sql.AppendLine("             SET @cols_type_2 = @_isoCode                                                                                      ")
    _sql.AppendLine("             SET @cols_update_2 = 'CM.'+@_isoCodeColumn+' = AZ.'+@_isoCode+''                                                  ")
    _sql.AppendLine("           END                                                                                                                 ")
    _sql.AppendLine("         ELSE                                                                                                                  ")
    _sql.AppendLine("           BEGIN                                                                                                               ")
    _sql.AppendLine("             SET @cols_type_2 = @cols_type_2 + ',' + @_isoCode                                                                 ")
    _sql.AppendLine("             SET @cols_update_2 = @cols_update_2 +','+'CM.'+@_isoCodeColumn+' = AZ.'+@_isoCode+''                              ")
    _sql.AppendLine("           END                                                                                                                 ")
    _sql.AppendLine("         SET @colsFormat = @colsFormat + ',ISNULL('+@_isoCodeColumn+', 0) AS ''Otros ('+@_isoCode+')'''                        ")
    _sql.AppendLine("     END                                                                                                                       ")
    _sql.AppendLine("     SET @_counter = @_counter + 1                                                                                             ")
    _sql.AppendLine("    END                                                                                                                        ")


    _sql.AppendLine("IF @cols_type_1 <> ''                                                                                                          ")
    _sql.AppendLine("  BEGIN                                                                                                                        ")
    _sql.AppendLine("    SET @_queryColumns = 'UPDATE CM                                                                                            ")
    _sql.AppendLine("                          SET                                                                                                  ")
    _sql.AppendLine("                            '+@cols_update_1+'                                                                                 ")
    _sql.AppendLine("                          FROM #TABLE_MOVEMENT_CAGE_DATA AS CM                                                                 ")
    _sql.AppendLine("                          INNER JOIN (                                                                                         ")
    _sql.AppendLine("                          SELECT * FROM(                                                                                       ")
    _sql.AppendLine("                                         SELECT                                                                                ")
    _sql.AppendLine("                                            CM.CGM_MOVEMENT_ID                                                                 ")
    _sql.AppendLine("                                           ,CASE                                                                               ")
    _sql.AppendLine("                                              WHEN CM.CGM_TYPE IN(0, 1, 3, 4, 5, 99) AND CD.CMD_CAGE_CURRENCY_TYPE = 1003 THEN(CD.CMD_QUANTITY * -1)                         ")
    _sql.AppendLine("                                              WHEN CM.CGM_TYPE IN(0, 1, 3, 4, 5, 99) AND CD.CMD_CAGE_CURRENCY_TYPE = 99 THEN(CD.CMD_DENOMINATION * -1)                       ")
    _sql.AppendLine("                                              WHEN CM.CGM_TYPE IN(0, 1, 3, 4, 5, 99) AND CD.CMD_CAGE_CURRENCY_TYPE <> 1003 THEN (CD.CMD_QUANTITY * CD.CMD_DENOMINATION * -1) ")
    _sql.AppendLine("                                              WHEN CD.CMD_CAGE_CURRENCY_TYPE = 1003 THEN(CD.CMD_QUANTITY)                         ")
    _sql.AppendLine("                                              WHEN CD.CMD_CAGE_CURRENCY_TYPE = 99 THEN(CD.CMD_DENOMINATION)                       ")
    '_sql.AppendLine("                                              WHEN CD.CMD_CAGE_CURRENCY_TYPE = 0 AND CM.CGM_TYPE = 100 THEN(CD.CMD_DENOMINATION)  ")
    _sql.AppendLine("                                              WHEN CD.CMD_QUANTITY < 0 THEN(CD.CMD_DENOMINATION) ")
    _sql.AppendLine("                                              WHEN CD.CMD_CAGE_CURRENCY_TYPE <> 1003 THEN (CD.CMD_QUANTITY * CD.CMD_DENOMINATION) ")
    _sql.AppendLine("                                              ELSE (CD.CMD_QUANTITY * CD.CMD_DENOMINATION)                                     ")
    _sql.AppendLine("                                              END AS CD_AMOUNT                                                                 ")
    _sql.AppendLine("                                           ,CASE WHEN CD.CMD_ISO_CODE=''X01'' THEN ''' + @_isoCodeLocal + ''' ELSE CD.CMD_ISO_CODE END + ''_'' + CAST(   ")
    _sql.AppendLine("                                                CASE WHEN CD.CMD_CAGE_CURRENCY_TYPE > 1000                                      ")
    _sql.AppendLine("                                                     THEN CD.CMD_CAGE_CURRENCY_TYPE                                             ")
    _sql.AppendLine("                                                     WHEN CD.CMD_CAGE_CURRENCY_TYPE = 99 AND CD.CMD_QUANTITY IN (-1, -2, -100)  ")
    _sql.AppendLine("                                                     THEN 0                                                                     ")
    _sql.AppendLine("                                                     ELSE CASE WHEN CD.CMD_ISO_CODE=''X01'' THEN 1001 ELSE 0 END END as nvarchar(4))    as CMD_ISO_CODE  ")
    _sql.AppendLine("                                         FROM CAGE_MOVEMENTS AS CM                                                             ")
    _sql.AppendLine("                                              LEFT JOIN CAGE_MOVEMENT_DETAILS AS CD                                            ")
    _sql.AppendLine("                                              ON CM.CGM_MOVEMENT_ID = CD.CMD_MOVEMENT_ID                                       ")
    _sql.AppendLine("                                         WHERE CD.CMD_ISO_CODE IS NOT NULL AND CM.CGM_TYPE NOT IN(200) AND CD.CMD_QUANTITY NOT IN (-200)                      ")
    _sql.AppendLine("                                       ) AS PU                                                                                 ")
    _sql.AppendLine("                                       PIVOT(                                                                                  ")
    _sql.AppendLine("                                          SUM(CD_AMOUNT)                                                                       ")
    _sql.AppendLine("                                          FOR CMD_ISO_CODE IN(' + @cols_type_1 + ')                                            ")
    _sql.AppendLine("                                       ) AS PVT                                                                                ")
    _sql.AppendLine("                          ) AS AM ON CM.CGM_MOVEMENT_ID = AM.CGM_MOVEMENT_ID                                                   ")
    _sql.AppendLine("                         '                                                                                                     ")
    _sql.AppendLine("    EXEC SP_EXECUTESQL @_queryColumns                                                                                          ")
    _sql.AppendLine("  END                                                                                                                          ")

    _sql.AppendLine("IF @cols_type_2 <> ''                                                                                                          ")
    _sql.AppendLine("    BEGIN                                                                                                                      ")
    _sql.AppendLine("      SET @_queryColumns = 'UPDATE CM                                                                                          ")
    _sql.AppendLine("                            SET                                                                                                ")
    _sql.AppendLine("                             '+@cols_update_2+'                                                                                ")
    _sql.AppendLine("                            FROM #TABLE_MOVEMENT_CAGE_DATA AS CM                                                               ")
    _sql.AppendLine("                            INNER JOIN (                                                                                       ")
    _sql.AppendLine("                              SELECT * FROM(                                                                                   ")
    _sql.AppendLine("                                     SELECT                                                                                    ")
    _sql.AppendLine("                                       CM.CGM_MOVEMENT_ID                                                                      ")
    _sql.AppendLine("                                       ,CASE                                                                                   ")
    _sql.AppendLine("                                           WHEN CM.CGM_TYPE IN(0, 1, 3, 4, 5, 99) AND CD.CMD_CAGE_CURRENCY_TYPE = 1003 THEN(CD.CMD_QUANTITY * -1)       ")
    _sql.AppendLine("                                           WHEN CM.CGM_TYPE IN(0, 1, 3, 4, 5, 99) AND CD.CMD_CAGE_CURRENCY_TYPE <> 1003 THEN (CD.CMD_DENOMINATION * -1) ")
    _sql.AppendLine("                                           WHEN CD.CMD_CAGE_CURRENCY_TYPE = 1003 THEN(CD.CMD_QUANTITY)                         ")
    _sql.AppendLine("                                           WHEN CD.CMD_CAGE_CURRENCY_TYPE <> 1003 THEN (CD.CMD_DENOMINATION)                   ")
    _sql.AppendLine("                                           ELSE CD.CMD_DENOMINATION                                                            ")
    _sql.AppendLine("                                           END AS CD_AMOUNT                                                                    ")
    _sql.AppendLine("                                       ,CD.CMD_ISO_CODE                                                                        ")
    _sql.AppendLine("                                        FROM CAGE_MOVEMENTS AS CM                                                              ")
    _sql.AppendLine("                                        LEFT JOIN CAGE_MOVEMENT_DETAILS AS CD                                                  ")
    _sql.AppendLine("                                        ON CM.CGM_MOVEMENT_ID = CD.CMD_MOVEMENT_ID                                             ")
    _sql.AppendLine("                                         WHERE  CD.CMD_ISO_CODE IS NOT NULL AND CD.CMD_QUANTITY IN(-1, -2, -100)               ")
    _sql.AppendLine("                                                AND CD.CMD_ISO_CODE NOT IN (''X01'')                                           ")
    _sql.AppendLine("                                      ) AS PU                                                                                  ")
    _sql.AppendLine("                                      PIVOT(                                                                                   ")
    _sql.AppendLine("                                          SUM(CD_AMOUNT)                                                                       ")
    _sql.AppendLine("                                          FOR CMD_ISO_CODE IN('+@cols_type_2+')                                                ")
    _sql.AppendLine("                                      ) AS PVT                                                                                 ")
    _sql.AppendLine("                              ) AS AZ ON CM.CGM_MOVEMENT_ID = AZ.CGM_MOVEMENT_ID                                               ")
    _sql.AppendLine("                            '                                                                                                  ")
    _sql.AppendLine("      EXEC SP_EXECUTESQL @_queryColumns                                                                                        ")
    _sql.AppendLine("   END                                                                                                                         ")

    _sql.AppendLine("CREATE TABLE #TABLE_MACHINE_ISO_CODE_DATA                                                                                      ")
    _sql.AppendLine("(                                                                                                                              ")
    _sql.AppendLine("   id INT Identity(1,1),                                                                                                       ")
    _sql.AppendLine("   iso_code VARCHAR(5)                                                                                                         ")
    _sql.AppendLine(")                                                                                                                              ")

    _sql.AppendLine("INSERT INTO #TABLE_MACHINE_ISO_CODE_DATA                                                                                       ")
    _sql.AppendLine("SELECT TE.TE_ISO_CODE FROM(SELECT                                                                                              ")
    _sql.AppendLine("    CM.CGM_MOVEMENT_ID                                                                                                         ")
    _sql.AppendLine("   ,CASE                                                                                                                       ")
    _sql.AppendLine("       WHEN @_activeDualCurrency = '1'                                                                                         ")
    _sql.AppendLine("         THEN ISNULL(TE_ISO_CODE,@_isoCodeLocal)                                                                               ")
    _sql.AppendLine("       ELSE @_isoCodeLocal                                                                                                     ")
    _sql.AppendLine("    END AS TE_ISO_CODE                                                                                                         ")
    _sql.AppendLine("    FROM #TABLE_MOVEMENT_CAGE_DATA  AS CM                                                                                      ")
    _sql.AppendLine("    INNER JOIN MONEY_COLLECTIONS AS MCU                                                                                        ")
    _sql.AppendLine("    ON CM.CMT_COLLECTION_ID = MCU.MC_COLLECTION_ID AND CM.CGM_TYPE  IN (5, 104)                                                ")
    _sql.AppendLine("    LEFT JOIN TERMINALS AS TE                                                                                                  ")
    _sql.AppendLine("    ON CM.CMT_TERMINAL_ID = TE.TE_TERMINAL_ID                                                                                  ")
    _sql.AppendLine("    WHERE((MCU.MC_COLLECTED_BILL_AMOUNT Is Not NULL And MCU.MC_COLLECTED_BILL_AMOUNT > 0) OR                                   ")
    _sql.AppendLine("                          (MCU.MC_EXPECTED_BILL_AMOUNT Is Not NULL And MCU.MC_EXPECTED_BILL_AMOUNT > 0) OR                     ")
    _sql.AppendLine("                          (MCU.MC_COLLECTED_COIN_AMOUNT Is Not NULL And MCU.MC_COLLECTED_COIN_AMOUNT > 0) OR                   ")
    _sql.AppendLine("                          (MCU.MC_EXPECTED_COIN_AMOUNT Is Not NULL And MCU.MC_EXPECTED_COIN_AMOUNT > 0))                       ")
    _sql.AppendLine(")AS TE                                                                                                                         ")
    _sql.AppendLine(" GROUP BY TE.TE_ISO_CODE                                                                                                       ")
    _sql.AppendLine("SET @_counter = 1		                                                                                                          ")
    _sql.AppendLine("SET @_count_aux = (SELECT COUNT(*) FROM #TABLE_MACHINE_ISO_CODE_DATA) + 1                                                      ")
    _sql.AppendLine("WHILE NOT @_counter = @_count_aux                                                                                              ")
    _sql.AppendLine("  BEGIN                                                                                                                        ")
    _sql.AppendLine("    SELECT                                                                                                                     ")
    _sql.AppendLine("      @_isoCode  =  TIC.iso_code                                                                                               ")
    _sql.AppendLine("    FROM #TABLE_MACHINE_ISO_CODE_DATA AS TIC                                                                                   ")
    _sql.AppendLine("    WHERE TIC.id = @_counter                                                                                                   ")
    _sql.AppendLine("    SET @_count =(SELECT COUNT(*) FROM #TABLE_ISO_CODE_DATA AS tic                                                             ")
    _sql.AppendLine("                        WHERE tic.iso_code = @_isoCode AND tic.data_type = 0)                                                  ")
    _sql.AppendLine("    IF @_count = 0                                                                                                             ")
    _sql.AppendLine("      BEGIN                                                                                                                    ")
    _sql.AppendLine("        SET @_queryColumns = 'ALTER TABLE #TABLE_MOVEMENT_CAGE_DATA ADD [' + @_isoCode + '] MONEY '                            ")
    _sql.AppendLine("        EXEC SP_EXECUTESQL @_queryColumns                                                                                      ")
    _sql.AppendLine("        SET @colsFormat = @colsFormat + ',ISNULL('+@_isoCode+', 0) AS '+@_isoCode + ''                                         ")
    _sql.AppendLine("      END                                                                                                                      ")
    _sql.AppendLine("    SET @_queryColumns ='                                                                                                      ")
    _sql.AppendLine("    UPDATE                                                                                                                     ")
    _sql.AppendLine("      MTP                                                                                                                      ")
    _sql.AppendLine("      SET                                                                                                                      ")
    _sql.AppendLine("        MTP.'+@_isoCode+'_'+@_dataType+' = (CASE WHEN MTP.CGM_TYPE = 104 THEN MCU.COLLECTED_AMOUNT                                          ")
    _sql.AppendLine("                                                ELSE MCU.EXPECTED_AMOUNT END)                                                  ")
    _sql.AppendLine("    FROM #TABLE_MOVEMENT_CAGE_DATA AS MTP                                                                                      ")
    _sql.AppendLine("    INNER JOIN (SELECT                                                                                                         ")
    _sql.AppendLine("                 CM.CGM_MOVEMENT_ID                                                                                            ")
    _sql.AppendLine("                ,MC_EXPECTED_BILL_AMOUNT + MC_EXPECTED_COIN_AMOUNT AS EXPECTED_AMOUNT                                          ")
    _sql.AppendLine("                ,MC_COLLECTED_BILL_AMOUNT + MC_COLLECTED_COIN_AMOUNT AS COLLECTED_AMOUNT                                       ")
    _sql.AppendLine("                ,CASE                                                                                                          ")
    _sql.AppendLine("                   WHEN '''+@_activeDualCurrency+''' = ''1''                                                                   ")
    _sql.AppendLine("                     THEN ISNULL(TE_ISO_CODE,'''+@_isoCodeLocal+''')                                                           ")
    _sql.AppendLine("                   ELSE '''+@_isoCodeLocal+'''                                                                                 ")
    _sql.AppendLine("                 END AS TE_ISO_CODE                                                                                            ")
    _sql.AppendLine("                FROM #TABLE_MOVEMENT_CAGE_DATA  AS CM                                                                          ")
    _sql.AppendLine("                INNER JOIN MONEY_COLLECTIONS AS MCU                                                                            ")
    _sql.AppendLine("                ON CM.CMT_COLLECTION_ID = MCU.MC_COLLECTION_ID AND CM.CGM_TYPE IN (5, 104)                                     ")
    _sql.AppendLine("                LEFT JOIN TERMINALS AS TE                                                                                      ")
    _sql.AppendLine("                ON CM.CMT_TERMINAL_ID = TE.TE_TERMINAL_ID                                                                      ")
    _sql.AppendLine("                WHERE((MCU.MC_COLLECTED_BILL_AMOUNT Is Not NULL And MCU.MC_COLLECTED_BILL_AMOUNT > 0) Or                       ")
    _sql.AppendLine("                      (MCU.MC_EXPECTED_BILL_AMOUNT Is Not NULL And MCU.MC_EXPECTED_BILL_AMOUNT > 0) Or                         ")
    _sql.AppendLine("                      (MCU.MC_COLLECTED_COIN_AMOUNT Is Not NULL And MCU.MC_COLLECTED_COIN_AMOUNT > 0) Or                       ")
    _sql.AppendLine("                      (MCU.MC_EXPECTED_COIN_AMOUNT Is Not NULL And MCU.MC_EXPECTED_COIN_AMOUNT > 0))                           ")
    _sql.AppendLine("    )AS MCU ON MTP.CGM_MOVEMENT_ID = MCU.CGM_MOVEMENT_ID AND MCU.TE_ISO_CODE = '''+@_isoCode+'''                               ")
    _sql.AppendLine("    '                                                                                                                          ")
    _sql.AppendLine("    EXEC SP_EXECUTESQL @_queryColumns                                                                                          ")
    _sql.AppendLine("    SET @_counter = @_counter + 1                                                                                              ")
    _sql.AppendLine("  END                                                                                                                          ")

    _sql.AppendLine("SET @_queryColumns ='                                                                                                          ")
    _sql.AppendLine("    SELECT                                                                                                                     ")
    _sql.AppendLine("      CGM_MOVEMENT_ID                                                                                                          ")
    _sql.AppendLine("     ,TE_NAME                                                                                                                  ")
    _sql.AppendLine("     ,GU_FULL_NAME                                                                                                             ")
    _sql.AppendLine("     ,CT_NAME                                                                                                                  ")
    _sql.AppendLine("     ,GU_FULL_NAME_2                                                                                                           ")
    _sql.AppendLine("     ,CGM_TYPE                                                                                                                 ")
    _sql.AppendLine("     ,CGM_STATUS                                                                                                               ")
    _sql.AppendLine("     ,CST_SOURCE_TARGET_NAME                                                                                                   ")
    _sql.AppendLine("     ,CGM_MOVEMENT_DATETIME                                                                                                    ")
    _sql.AppendLine("     ,CGM_GAMING_TABLE_ID                                                                                                      ")
    _sql.AppendLine("     ,CGS_CAGE_SESSION_ID                                                                                                      ")
    _sql.AppendLine("     ,GU_USERNAME                                                                                                              ")
    _sql.AppendLine("     ,CGM_GAMING_TABLE_ID_2                                                                                                    ")
    _sql.AppendLine("     ,TE_TERMINAL_TYPE                                                                                                         ")
    _sql.AppendLine("     ,GU_FULL_NAME_LAST_MODIFICATION                                                                                           ")
    _sql.AppendLine("     ,CGM_CANCELLATION_DATETIME                                                                                                ")
    _sql.AppendLine("     ,PROGRESSIVE_PROVISION_AMOUNT                                                                                             ")
    _sql.AppendLine("     ,PROGRESSIVE_AWARDED_AMOUNT                                                                                               ")
    _sql.AppendLine("     ,CGM_RELATED_ID                                                                                                           ")
    _sql.AppendLine("     ,CGS_WORKING_DAY                                                                                                          ")
    _sql.AppendLine("     ,GAMING_TABLE_SESSION_ID                                                                                                  ")
    _sql.AppendLine("     '+@colsFormat+'                                                                                                           ")
    _sql.AppendLine("     FROM #TABLE_MOVEMENT_CAGE_DATA  AS CMT                                                                                    ")
    _sql.AppendLine(" ORDER BY ")
    If opt_by_cashier_session.Checked Then
      _sql.AppendLine(" CGM_CASHIER_SESSION_ID DESC, CGM_MOVEMENT_ID DESC ")
    ElseIf opt_monitor_mode.Checked Then
      _sql.AppendLine(" CGM_MOVEMENT_DATETIME ASC, CGM_MOVEMENT_ID ASC ")
    Else
      _sql.AppendLine(" CGM_MOVEMENT_DATETIME DESC, CGM_MOVEMENT_ID DESC ") ' by default
    End If
    _sql.AppendLine("  '                                                                                                                            ")
    _sql.AppendLine("EXEC SP_EXECUTESQL @_queryColumns                                                                                              ")

    _sql.AppendLine("DROP TABLE #TABLE_ISO_CODE_DATA                                                                                                ")
    _sql.AppendLine("DROP TABLE #TABLE_MACHINE_ISO_CODE_DATA                                                                                        ")
    Return _sql.ToString()
  End Function




  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the main SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String
    Dim _str_where As String
    Dim _date_from As String
    Dim _date_to As String

    _str_where = ""
    _date_from = ""
    _date_to = ""

    ' Filter Dates
    If Me.dtp_from.Checked Then
      If Me.opt_working_day.Checked Then
        _date_from = GUI_FormatDayDB(dtp_from.Value)
      Else
        _date_from = GUI_FormatDateDB(dtp_from.Value)
      End If
    End If
    If Me.dtp_to.Checked Then
      If Me.opt_working_day.Checked Then
        _date_to = GUI_FormatDayDB(dtp_to.Value)
      Else
        _date_to = GUI_FormatDateDB(dtp_to.Value)
      End If
    End If

    If opt_movement_date.Checked Then
      _str_where = _str_where & WSI.Common.Misc.DateSQLFilter("CGM_MOVEMENT_DATETIME", _date_from, _date_to, False)
    ElseIf opt_working_day.Checked Then
      _str_where = _str_where & WSI.Common.Misc.DateSQLFilter("CGS_WORKING_DAY", _date_from, _date_to, False)
    End If

    ' Filter operation
    If Me.m_mode_edit = MODE_EDIT.MONEY_REQUEST Then
      _str_where = _str_where & "AND (CGM_STATUS = 0 AND CGM_TYPE = 200) "
    Else
      _str_where = _str_where & GetSqlWhereOperation()
    End If


    ' Filter Terminal
    If Me.opt_one_user.Checked = True Then
      _str_where = _str_where & " AND (CGM_USER_CASHIER_ID = " & Me.cmb_user.Value & " OR CGM_USER_CAGE_ID = " & Me.cmb_user.Value & ") "
    End If

    ' Filter status
    _str_where = _str_where & GetSqlWhereStatus()


    If m_session_id <> -1 Then
      _str_where = _str_where & " AND (CGM_CAGE_SESSION_ID = " & m_session_id & ") "
    Else
      If opt_one_session.Checked Then
        _str_where = _str_where & " AND (CGM_CAGE_SESSION_ID = " & Me.cmb_sessions.Value & ") "
      End If
    End If

    If m_cashier_session_id > 0 Then
      _str_where = _str_where & " AND (CGM_CASHIER_SESSION_ID = " & m_cashier_session_id & ") "

    End If

    Return _str_where

  End Function ' GetSqlWhere

  ' PURPOSE: Build the variable part of the WHERE for status filter
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhereStatus() As String
    Dim _aux_sql_request As String

    _aux_sql_request = String.Empty
    GetSqlWhereStatus = String.Empty

    If opt_monitor_mode.Checked Then
      GetSqlWhereStatus = "AND CGM_STATUS IN (" & Cage.CageStatus.Sent & ")"

    Else
      GetSqlWhereStatus = Me.uc_checked_list_status.SelectedIndexesList()

      If InStr(GetSqlWhereStatus, Cage.CageStatus.FinishedOpenCloseCage) > 0 Then
        GetSqlWhereStatus &= ", " & Cage.CageStatus.FinishedCountCage
      End If

      If InStr(GetSqlWhereStatus, Cage.CageOperationType.RequestOperation) > 0 Then
        _aux_sql_request = " (CGM_STATUS IN (" & Cage.CageStatus.Sent & ", " & Cage.CageStatus.Canceled & ", " & Cage.CageStatus.OK _
                                              & ") AND CGM_TYPE = " & Cage.CageOperationType.RequestOperation & ")"
      End If

      GetSqlWhereStatus = Replace(GetSqlWhereStatus, ", " & Cage.CageOperationType.RequestOperation, String.Empty)

      If GetSqlWhereStatus <> "" Then
        GetSqlWhereStatus = " AND (CGM_STATUS IN (" & GetSqlWhereStatus & ")" & IIf(_aux_sql_request <> "", " OR " & _aux_sql_request, " AND CGM_TYPE <> " & Cage.CageOperationType.RequestOperation) & ")"
      ElseIf _aux_sql_request <> "" Then
        GetSqlWhereStatus = " AND " & _aux_sql_request
      End If
    End If

    Return GetSqlWhereStatus
  End Function ' GetSqlWhereStatus

  ' PURPOSE: Build the variable part of the WHERE for operation filter
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - GetSqlWhereOperation
  Private Function GetSqlWhereOperation() As String
    Dim _str_where As String

    _str_where = String.Empty


    If Me.rb_gaming_table.Checked Then
      If Me.cmb_cashier.Value <> -1 Then
        _str_where = _str_where & " AND (GTS_GAMING_TABLE_ID = " & Me.cmb_cashier.Value & ") "
      End If
    Else
      If Me.cmb_cashier.Value = -1 Or Not Me.cmb_cashier.Enabled Then
        _str_where = _str_where & " AND (CGM_TYPE IN ("
      End If
    End If

    If opt_all_cashiers.Checked Then
      ' Gaming table selected
      If Me.cmb_cashier.Enabled = False Or Me.cmb_cashier.Value = -1 Then
        ' Movement Type = Cage send
        If opt_from_cage.Checked Or opt_all.Checked Then
          _str_where = _str_where & Cage.CageOperationType.ToGamingTable & ","
        End If
        ' Movement Type = Cage collect
        If opt_from_cashier.Checked Or opt_all.Checked Then
          _str_where = _str_where & Cage.CageOperationType.FromGamingTable & "," & Cage.CageOperationType.FromGamingTableClosing & ","
        End If
        ' Movement Type = Cage collect - dropbox
        If opt_from_cashier.Checked Or opt_all.Checked Then
          _str_where = _str_where & Cage.CageOperationType.FromGamingTableDropbox & "," & Cage.CageOperationType.FromGamingTableDropboxWithExpected & ","
        End If
      End If
    End If


    If Me.rb_custom.Checked Or opt_all_cashiers.Checked Then

      ' Custom selected
      If Me.cmb_cashier.Enabled = False Or Me.cmb_cashier.Value = -1 Then
        ' Movement Type = Cage send
        If opt_from_cage.Checked Or opt_all.Checked Then
          _str_where = _str_where & Cage.CageOperationType.ToCustom & ","
        End If
        ' Movement Type = Cage collect
        If opt_from_cashier.Checked Or opt_all.Checked Then
          _str_where = _str_where & Cage.CageOperationType.FromCustom & ","
        End If
      Else
        ' Selected specific source / destiny
        _str_where = _str_where & " AND (CGM_SOURCE_TARGET_ID = " & Me.cmb_cashier.Value & ") "
      End If
    End If

    If Me.rb_cashier.Checked Or opt_all_cashiers.Checked Then


      'Cashier selected
      If Me.cmb_cashier.Enabled = False Or Me.cmb_cashier.Value = -1 Then
        ' Movement Type = Cage send
        If opt_from_cage.Checked Or opt_all.Checked Then
          _str_where = _str_where & Cage.CageOperationType.ToCashier & "," & Cage.CageOperationType.RequestOperation & ","
        End If
        ' Movement Type = Cage collect
        If opt_from_cashier.Checked Or opt_all.Checked Then
          _str_where = _str_where & Cage.CageOperationType.FromCashier & "," & Cage.CageOperationType.FromCashierClosing & ","
        End If
      Else
        ' Selected specific source / destiny
        _str_where = _str_where & " AND (CGM_TERMINAL_CASHIER_ID = " & Me.cmb_cashier.Value & ") "
      End If
    End If

    If Me.rb_terminal.Checked Or opt_all_cashiers.Checked Then

      ' Terminal Selected
      If Me.cmb_cashier.Enabled = False Or Me.cmb_cashier.Value = -1 Then
        ' Movement Type = Cage send
        If opt_from_cage.Checked Or opt_all.Checked Then
          _str_where = _str_where & Cage.CageOperationType.ToTerminal & ", " & Cage.CageOperationType.ProgressiveProvision & ", " & Cage.CageOperationType.ProgressiveAwarded & ", " & Cage.CageOperationType.ProgressiveCancelProvision & ", " & Cage.CageOperationType.ProgressiveCancelAwarded & ","
        End If
        ' Movement Type = Cage collect
        If (opt_from_cashier.Checked Or opt_all.Checked) And (m_mode_edit <> MODE_EDIT.COLLECTION) Then
          _str_where = _str_where & Cage.CageOperationType.FromTerminal & ","
        End If
      Else
        ' Selected specific source / destiny
        _str_where = _str_where & " AND (CGM_TERMINAL_CASHIER_ID IN ( SELECT TE_TERMINAL_ID FROM TERMINALS WHERE TE_MASTER_ID IN (" & Me.cmb_cashier.Value & "))  "
        _str_where = _str_where & " OR CT_TERMINAL_ID IN ( SELECT TE_TERMINAL_ID FROM TERMINALS WHERE TE_MASTER_ID IN (" & Me.cmb_cashier.Value & ")) ) "
      End If

    End If

    If Me.opt_all_cashiers.Checked And Not Me.opt_monitor_mode.Checked Then
      ' Only movements which is possible in historic mode 
      _str_where = _str_where & Cage.CageOperationType.OpenCage & ", " & Cage.CageOperationType.CloseCage & ", " & Cage.CageOperationType.ReOpenCage & ", " & Cage.CageOperationType.CountCage & ", " & Cage.CageOperationType.LostQuadrature & ", " & Cage.CageOperationType.GainQuadrature & _
                         ", " & Cage.CageOperationType.ProgressiveProvision & ", " & Cage.CageOperationType.ProgressiveAwarded & ", " & Cage.CageOperationType.ProgressiveCancelProvision & ", " & Cage.CageOperationType.ProgressiveCancelAwarded & ","

    End If

    If Me.opt_all_cashiers.Checked Or (Not Me.rb_gaming_table.Checked And Me.cmb_cashier.Value = -1) Then
      _str_where = _str_where.Remove(_str_where.Length - 1, 1)
      _str_where = _str_where & " )) "
    End If

    If Me.rb_cashier.Checked Then
      _str_where = _str_where & " AND (GTS_GAMING_TABLE_SESSION_ID IS NULL) "
    End If


    Return _str_where
  End Function ' GetSqlWhereOperation

  ' PURPOSE: Fill the filter Grid with data
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub FillCheckedlistFilterGrid(ByVal Sent As Boolean, ByVal Collect As Boolean)
    Dim _str As String
    Dim _str_ok As String
    Dim _str_ok_denomination As String

    _str = ""
    _str_ok = ""
    _str_ok_denomination = ""

    Call Me.uc_checked_list_status.Clear()
    Call Me.uc_checked_list_status.ReDraw(False)

    Call Me.uc_checked_list_status.ColumnWidth(uc_checked_list.GRID_COLUMN_COLOR, 150)

    If opt_from_cage.Checked Then
      _str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2891)
      _str_ok = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2892)
      _str_ok_denomination = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2944)
    ElseIf opt_from_cashier.Checked Then
      _str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3011)
      _str_ok = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3305)
      _str_ok_denomination = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3021)
    Else
      _str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3012)
      _str_ok = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3318)
      _str_ok_denomination = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3319)
    End If

    Me.uc_checked_list_status.Add(Cage.CageStatus.Sent, _str)
    Me.uc_checked_list_status.Add(Cage.CageStatus.OK, _str_ok, ENUM_GUI_COLOR.GUI_COLOR_GREEN_01)
    Me.uc_checked_list_status.Add(Cage.CageStatus.OkDenominationImbalance, _str_ok_denomination, ENUM_GUI_COLOR.GUI_COLOR_OCHRE_00)

    If Collect Then
      Me.uc_checked_list_status.Add(Cage.CageStatus.OkAmountImbalance, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3022), ENUM_GUI_COLOR.GUI_COLOR_ORANGE_01)
      Me.uc_checked_list_status.Add(Cage.CageStatus.ReceptionFromCustom, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3395), ENUM_GUI_COLOR.GUI_COLOR_GREEN_01)
      Me.uc_checked_list_status.Add(Cage.CageStatus.CanceledUncollectedCashierRetirement, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5848), ENUM_GUI_COLOR.GUI_COLOR_GREY_03)
    End If

    If Sent Then
      Me.uc_checked_list_status.Add(Cage.CageStatus.Canceled, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1891), ENUM_GUI_COLOR.GUI_COLOR_GREY_03)
      Me.uc_checked_list_status.Add(Cage.CageStatus.SentToCustom, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3394), ENUM_GUI_COLOR.GUI_COLOR_GREEN_01)
    End If


    '    uc_checked_list_status.Add(Cage.CageStatus.CanceledDueToImbalanceOrOther, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2893))
    Me.uc_checked_list_status.Add(Cage.CageStatus.FinishedOpenCloseCage, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3445), ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

    If Sent Then
      Me.uc_checked_list_status.Add(Cage.CageOperationType.RequestOperation, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4785))
      m_request_operation_pos = Me.uc_checked_list_status.Count
    End If

    If Me.uc_checked_list_status.Count() > 0 Then
      Call Me.uc_checked_list_status.CurrentRow(0)
    End If

    Call Me.uc_checked_list_status.ReDraw(True)

  End Sub 'FillCheckedlistFilterGrid

  ' PURPOSE: Sets counter colors for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetCounterColors()
    With Me.Grid
      Me.m_sent_rows = 0
      .Counter(COUNTER_SENT).Visible = True
      .Counter(COUNTER_SENT).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
      .Counter(COUNTER_SENT).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)

      Me.m_ok_rows = 0
      .Counter(COUNTER_OK).Visible = True
      .Counter(COUNTER_OK).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREEN_01)
      .Counter(COUNTER_OK).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)

      Me.m_denomination_imbalance_rows = 0
      .Counter(COUNTER_DENOMINATION_IMBALANCE).Visible = True
      .Counter(COUNTER_DENOMINATION_IMBALANCE).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_OCHRE_00)
      .Counter(COUNTER_DENOMINATION_IMBALANCE).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)

      Me.m_total_imbalance_rows = 0
      .Counter(COUNTER_TOTAL_IMBALANCE).Visible = True
      .Counter(COUNTER_TOTAL_IMBALANCE).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_ORANGE_01)
      .Counter(COUNTER_TOTAL_IMBALANCE).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)

      Me.m_canceled_rows = 0
      .Counter(COUNTER_CANCELED).Visible = True
      .Counter(COUNTER_CANCELED).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_03)
      .Counter(COUNTER_CANCELED).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)

      Me.m_open_close_cage_rows = 0
      .Counter(COUNTER_OPEN_CLOSE_CAGE).Visible = True
      .Counter(COUNTER_OPEN_CLOSE_CAGE).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
      .Counter(COUNTER_OPEN_CLOSE_CAGE).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)

    End With
  End Sub ' SetCounterColors

  ' PURPOSE: gets the status and the operation
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GetStatusOperation(ByVal MovementId As Long, ByRef OperationTypeId As Short, ByRef Status As Cage.CageStatus)
    Dim _sb As StringBuilder
    Dim _sql_trx As SqlClient.SqlTransaction

    _sql_trx = Nothing

    _sb = New StringBuilder

    _sb.AppendLine("SELECT    CGM_STATUS ")
    _sb.AppendLine("         ,CGM_TYPE ")
    _sb.AppendLine("  FROM    CAGE_MOVEMENTS ")
    _sb.AppendLine(" WHERE    CGM_MOVEMENT_ID = @pMovementId ")

    If GUI_BeginSQLTransaction(_sql_trx) Then
      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), _sql_trx.Connection, _sql_trx)

        _cmd.Parameters.Add("@pMovementId", SqlDbType.NVarChar).Value = MovementId

        Using _sql_reader As SqlClient.SqlDataReader = _cmd.ExecuteReader()
          If _sql_reader.Read() Then
            Status = CInt(_sql_reader.GetInt32(0))
            OperationTypeId = CInt(_sql_reader.GetInt32(1))
          End If
        End Using
      End Using
      GUI_EndSQLTransaction(_sql_trx, False)
    End If

  End Sub

  ' PURPOSE: Get Iso Codes of Currency Exchange columns
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - List of string with the Iso Codes
  '
  Private Function LoadIsoCodes(ByRef IsoCodes As List(Of String), ByRef IsoDescriptions As List(Of String)) As Boolean
    Dim _str_sql As StringBuilder
    Dim _national_currency As String

    _str_sql = New StringBuilder()
    IsoCodes = New List(Of String)
    IsoDescriptions = New List(Of String)
    _national_currency = CurrencyExchange.GetNationalCurrency()

    Try
      Using _db_trx As DB_TRX = New DB_TRX()

        _str_sql.AppendLine(" SELECT DISTINCT   cmd_iso_code, ce_description ")
        _str_sql.AppendLine("         FROM cage_movement_details ")
        _str_sql.AppendLine(" INNER JOIN   currency_exchange ON cmd_iso_code = ce_currency_iso_code ")
        _str_sql.AppendLine(" INNER JOIN   general_params ON ce_currency_iso_code = gp_key_value AND gp_group_key = 'RegionalOptions' AND gp_subject_key = 'CurrencyISOCode' ")
        _str_sql.AppendLine("         WHERE ce_type IN (0,3) ")
        _str_sql.AppendLine("         UNION ")
        _str_sql.AppendLine(" SELECT DISTINCT   cmd_iso_code, ce_description ")
        _str_sql.AppendLine("         FROM cage_movement_details ")
        _str_sql.AppendLine(" INNER JOIN   currency_exchange ON cmd_iso_code = ce_currency_iso_code ")
        _str_sql.AppendLine("         WHERE ce_type IN (0,3) ")


        Using _cmd As SqlCommand = New SqlCommand(_str_sql.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          Using _reader As SqlDataReader = _cmd.ExecuteReader()

            While _reader.Read()
              If Not _reader.IsDBNull(0) Then
                IsoCodes.Add(_reader.GetString(0))
                IsoDescriptions.Add(_reader.GetString(1))
              End If
            End While
          End Using
        End Using
      End Using

    Catch ex As Exception
      Return False
    End Try

    Return True

  End Function 'LoadCurrenciesExchange

  ' PURPOSE: .
  '
  '  PARAMS:
  '     - INPUT:
  '           - GuiId As Integer
  '           - DbTrx As DB_TRX
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - DataTable
  '
  Private Sub GetMovementsTotals()
    '17-DEC-2013 JCA  If modified this query, check if is necessary update in frm_cage_sessions_details. Thanks
    Dim _str_sql As StringBuilder
    Dim _str_where As String

    m_movements_totals = New DataTable()
    _str_sql = New StringBuilder()

    Try
      _str_sql.AppendLine("     SELECT   X.cgm_movement_id                                                                  ")
      _str_sql.AppendLine("		         , X.cmd_iso_code                                                                     ")
      _str_sql.AppendLine("		         , SUM(X.cmd_deposits) AS cmd_deposits                                                ")
      _str_sql.AppendLine("		         , SUM(X.cmd_withdrawals) AS cmd_withdrawals                                          ")
      _str_sql.AppendLine("		         , SUM(X.cmd_difference) AS cmd_difference                                            ")
      _str_sql.AppendLine("       FROM   (                                                                                  ")
      _str_sql.AppendLine("	              SELECT                                                                            ")
      _str_sql.AppendLine("                        cgm_movement_id                                                          ")
      _str_sql.AppendLine("		                   , cmd_iso_code                                                             ")
      _str_sql.AppendLine("		                   , cmd_denomination                                                         ")
      _str_sql.AppendLine("		                   , CASE                                                                     ")

      _str_sql.AppendLine("		                      WHEN cgm_type = 3 THEN                                                  ") 'Arqueo de b�veda: Valores introducidos por el �suario
      _str_sql.AppendLine("				                       (  SELECT ISNULL(SUM(CASE                                          ")
      _str_sql.AppendLine("								                              WHEN CMD.cmd_quantity < 0                           ")
      _str_sql.AppendLine("									                            THEN CMD.cmd_denomination                           ")
      _str_sql.AppendLine("								                            ELSE CMD.cmd_quantity * CMD.cmd_denomination          ")
      _str_sql.AppendLine("								                            END), 0)                                              ")
      _str_sql.AppendLine("								           FROM CAGE_MOVEMENTS AUX                                                ")
      _str_sql.AppendLine("								    INNER JOIN   CAGE_MOVEMENT_DETAILS CMD ON                                     ")
      _str_sql.AppendLine("		                           AUX.CGM_MOVEMENT_ID   =  CMD.CMD_MOVEMENT_ID                       ")
      _str_sql.AppendLine("								          AND AUX.CGM_RELATED_MOVEMENT_ID = cage_movements.cgm_movement_id        ")
      _str_sql.AppendLine("								         WHERE AUX.CGM_TYPE =         199                                         ")
      _str_sql.AppendLine("								           AND CAGE_MOVEMENT_DETAILS.cmd_iso_code  = CMD.cmd_iso_code             ")
      _str_sql.AppendLine("								           AND CAGE_MOVEMENT_DETAILS.cmd_denomination  = CMD.cmd_denomination     ")

      _str_sql.AppendLine("								           )              ")

      _str_sql.AppendLine("			                    WHEN cgm_type >= 100                                                    ")
      _str_sql.AppendLine("				                    THEN ISNULL(SUM(CASE                                                  ")
      _str_sql.AppendLine("								                              WHEN cmd_quantity < 0                               ")
      _str_sql.AppendLine("									                            THEN cmd_denomination                               ")
      _str_sql.AppendLine("								                            ELSE cmd_quantity * cmd_denomination                  ")
      _str_sql.AppendLine("								                            END), 0)                                              ")
      _str_sql.AppendLine("			                      ELSE 0                                                                ")
      _str_sql.AppendLine("			                    END AS cmd_deposits                                                     ")
      _str_sql.AppendLine("		                   , CASE                                                                     ")
      _str_sql.AppendLine("		                      WHEN cgm_type = 3 THEN                                                  ") 'Arqueo de b�veda: Valores introducidos por el �suario
      _str_sql.AppendLine("				                       (  SELECT ISNULL(SUM(CASE                                          ")
      _str_sql.AppendLine("								                              WHEN CMD.cmd_quantity < 0                           ")
      _str_sql.AppendLine("									                            THEN CMD.cmd_denomination                           ")
      _str_sql.AppendLine("								                            ELSE CMD.cmd_quantity * CMD.cmd_denomination          ")
      _str_sql.AppendLine("								                            END), 0)                                              ")
      _str_sql.AppendLine("								           FROM CAGE_MOVEMENTS AUX                                                ")
      _str_sql.AppendLine("								    INNER JOIN   CAGE_MOVEMENT_DETAILS CMD ON                                     ")
      _str_sql.AppendLine("		                           AUX.CGM_MOVEMENT_ID   =  CMD_MOVEMENT_ID                           ")
      _str_sql.AppendLine("								           AND AUX.CGM_RELATED_MOVEMENT_ID = cage_movements.cgm_movement_id       ")
      _str_sql.AppendLine("								         WHERE AUX.CGM_TYPE = 99                                                  ")
      _str_sql.AppendLine("								           AND CAGE_MOVEMENT_DETAILS.cmd_iso_code  = CMD.cmd_iso_code             ")
      _str_sql.AppendLine("								           AND CAGE_MOVEMENT_DETAILS.cmd_denomination  = CMD.cmd_denomination     ")
      _str_sql.AppendLine("                                                                                  )              ")

      _str_sql.AppendLine("			                    WHEN cgm_type < 100                                                     ")
      _str_sql.AppendLine("				                    THEN ISNULL(SUM(CASE                                                  ")
      _str_sql.AppendLine("								                              WHEN cmd_quantity < 0                               ")
      _str_sql.AppendLine("									                            THEN cmd_denomination                               ")
      _str_sql.AppendLine("								                            ELSE cmd_quantity * cmd_denomination                  ")
      _str_sql.AppendLine("								                            END), 0)                                              ")
      _str_sql.AppendLine("			                      ELSE 0                                                                ")
      _str_sql.AppendLine("			                    END AS cmd_withdrawals                                                  ")
      _str_sql.AppendLine("		                   , CASE                                                                     ")

      _str_sql.AppendLine("		                      WHEN cgm_type = 3 THEN                                                  ")

      _str_sql.AppendLine("				                       (   select ISNULL(SUM(CASE                                         ")
      _str_sql.AppendLine("						                              WHEN CMD.cmd_quantity < 0  AND AUX.CGM_TYPE = 199       ")
      _str_sql.AppendLine("							                            THEN CMD.cmd_denomination                               ")
      _str_sql.AppendLine("							                          WHEN cmd_quantity > 0  AND AUX.CGM_TYPE = 199             ")
      _str_sql.AppendLine("							                            THEN CMD.cmd_quantity * CMD.cmd_denomination            ")
      _str_sql.AppendLine("							                          WHEN   CMD.cmd_quantity < 0 AND AUX.CGM_TYPE = 99         ")
      _str_sql.AppendLine("							                            THEN -1 * CMD.cmd_denomination                          ")
      _str_sql.AppendLine("						                              ELSE -1 * CMD.cmd_quantity * CMD.cmd_denomination       ")
      _str_sql.AppendLine("						                            END), 0)                                                  ")

      _str_sql.AppendLine("								           FROM CAGE_MOVEMENTS AUX                                                ")
      _str_sql.AppendLine("								     INNER JOIN   CAGE_MOVEMENT_DETAILS CMD ON                                    ")
      _str_sql.AppendLine("		                              AUX.CGM_MOVEMENT_ID   =  CMD_MOVEMENT_ID                        ")
      _str_sql.AppendLine("								            AND   AUX.CGM_RELATED_MOVEMENT_ID = cage_movements.cgm_movement_id    ")
      _str_sql.AppendLine("								          WHERE   AUX.CGM_TYPE IN (99,199)                                        ")
      _str_sql.AppendLine("	                          AND   CAGE_MOVEMENT_DETAILS.cmd_iso_code  = CMD.cmd_iso_code          ")
      _str_sql.AppendLine("								            AND   CAGE_MOVEMENT_DETAILS.cmd_denomination  = CMD.cmd_denomination  ")
      _str_sql.AppendLine("                                                                                  )              ")


      _str_sql.AppendLine("			                    WHEN cgm_type >= 100                                                    ")
      _str_sql.AppendLine("				                    THEN ISNULL(SUM(CASE                                                  ")
      _str_sql.AppendLine("								                              WHEN cmd_quantity < 0                               ")
      _str_sql.AppendLine("									                            THEN cmd_denomination                               ")
      _str_sql.AppendLine("								                            ELSE cmd_quantity * cmd_denomination                  ")
      _str_sql.AppendLine("								                            END), 0)                                              ")
      _str_sql.AppendLine("			                      ELSE - 1 * ISNULL(SUM(CASE                                            ")
      _str_sql.AppendLine("							                                      WHEN cmd_quantity < 0                         ")
      _str_sql.AppendLine("								                                    THEN cmd_denomination                         ")
      _str_sql.AppendLine("							                                    ELSE cmd_quantity * cmd_denomination            ")
      _str_sql.AppendLine("							                                    END), 0)                                        ")
      _str_sql.AppendLine("			                      END AS cmd_difference                                                 ")
      _str_sql.AppendLine("	                FROM   cage_movements                                                          ")
      _str_sql.AppendLine("	          INNER JOIN   cage_movement_details                                                    ")
      _str_sql.AppendLine("                   ON   cage_movements.cgm_movement_id = cage_movement_details.cmd_movement_id   ")
      _str_sql.AppendLine("               WHERE    1 = 1 ")
      _str_where = GetSqlWhere()
      _str_sql.AppendLine(_str_where)
      _str_sql.AppendLine("	            GROUP BY   cgm_movement_id                                                          ")
      _str_sql.AppendLine("		                   , cmd_iso_code                                                             ")
      _str_sql.AppendLine("		                   , cgm_type                                                                 ")
      _str_sql.AppendLine("		                   , cmd_denomination                                                         ")
      _str_sql.AppendLine("	             ) AS X                                                                             ")
      _str_sql.AppendLine("   GROUP BY   x.cgm_movement_id                                                                  ")
      _str_sql.AppendLine("	           , x.cmd_iso_code                                                                     ")
      _str_sql.AppendLine("   ORDER BY   X.cgm_movement_id                                                                  ")

      Using _db_trx As New DB_TRX()
        Using _sql_cmd As New SqlCommand(_str_sql.ToString())
          Using _sql_da As New SqlDataAdapter(_sql_cmd)
            _db_trx.Fill(_sql_da, m_movements_totals)
          End Using
        End Using
      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

  End Sub ' GetSessonsTotals

  ' PURPOSE: Formats amount
  '
  '  PARAMS:
  '     - INPUT:
  '           - Amount
  '           - IsDenomination
  '           - IsoCode
  '           - ShowZeros
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - FormatAmount
  '
  Private Function FormatAmount(ByVal Amount As Decimal, _
                                ByVal IsoCode As String) As String

    Dim _currency_exchange As WSI.Common.CurrencyExchangeProperties
    FormatAmount = ""

    If Not Amount = Nothing Or Amount = 0 Then
      _currency_exchange = WSI.Common.CurrencyExchangeProperties.GetProperties(IsoCode)

      If Not _currency_exchange Is Nothing Then
        If Amount < 0 Then
          FormatAmount = "-" & _currency_exchange.FormatCurrency(Amount * -1) ' s'hauria d'implementar a currency exchange la possiblitat de -$n
        Else
          FormatAmount = _currency_exchange.FormatCurrency(Amount)
        End If
      Else
        FormatAmount = Currency.Format(Amount, IsoCode)
      End If
    End If

    Return FormatAmount
  End Function ' FormatAmount

  ' PURPOSE: To get the gaming table name
  '
  '  PARAMS:
  '     - INPUT:
  '           - Amount
  '           - IsDenomination
  '           - IsoCode
  '           - ShowZeros
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - FormatAmount
  '
  Private Function GetGamingTableId(ByVal GamingTableId As Integer) As String
    Dim _sb As StringBuilder
    Dim _sql_trx As SqlTransaction

    GetGamingTableId = String.Empty
    _sb = New StringBuilder
    _sql_trx = Nothing

    _sb.AppendLine("   SELECT   GT_NAME + ' - [' + GT_CODE + ']' AS NAME ")
    _sb.AppendLine("     FROM   GAMING_TABLES ")
    _sb.AppendLine("    WHERE   GT_GAMING_TABLE_ID = " & GamingTableId)

    If GUI_BeginSQLTransaction(_sql_trx) Then
      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), _sql_trx.Connection, _sql_trx)
        Using _sql_reader As SqlDataReader = _cmd.ExecuteReader()
          If _sql_reader.Read() Then
            GetGamingTableId = _sql_reader.GetString(0)
          End If
        End Using
      End Using
    End If

    Return GetGamingTableId
  End Function ' GetGamingTableId

  ' PURPOSE: Enable/Disable mode related controls
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub EnableMonitorOrHistory()

    If opt_monitor_mode.Checked Then
      gb_date.Enabled = False
      m_monitor_enabled = True
      tf_latest_update.Value = WSI.Common.Format.CustomFormatTime(WSI.Common.WGDB.Now, True)
      tf_latest_update.Visible = True
      lbl_monitor_msg.Visible = False
      Me.Grid.Sortable = False
      m_first_time = True

      Me.dtp_from.Checked = False
      'If m_mode_edit <> MODE_EDIT.COLLECTION Then
      '  Me.opt_all.Checked = True
      'End If

      Me.gb_date.Enabled = False
      Me.gb_sessions.Enabled = False
      Me.gb_order_by.Enabled = False
      FillCheckedlistFilterGrid(True, True)

      Call tmr_monitor.Start()
      Me.Grid.Redraw = False

      Me.GUI_Button(ENUM_BUTTON.BUTTON_FILTER_RESET).Enabled = False

      Me.uc_checked_list_status.Enabled = True
      Me.uc_checked_list_status.btn_uncheck_all.PerformClick()
      Me.uc_checked_list_status.SetValue(0)

      Call tmr_monitor_Tick(Nothing, Nothing)

      Me.Grid.Redraw = True
      Me.uc_checked_list_status.Enabled = False

    ElseIf opt_history_mode.Checked AndAlso gb_mode.Enabled Then
      gb_date.Enabled = True
      m_monitor_enabled = False
      tf_latest_update.Visible = False
      lbl_monitor_msg.Visible = False
      Me.Grid.Sortable = True
      'm_first_time = False
      Call tmr_monitor.Stop()
      Me.gb_date.Enabled = True
      Me.gb_operation.Enabled = True
      Me.gb_cashier.Enabled = True
      Me.gb_user.Enabled = True
      Me.gb_sessions.Enabled = True
      Me.gb_order_by.Enabled = True
      Me.uc_checked_list_status.Enabled = True

      Me.GUI_Button(ENUM_BUTTON.BUTTON_FILTER_RESET).Enabled = True

    End If

  End Sub ' EnableMonitorOrHistory

#End Region ' Private Functions

#Region " Events "

  Private Sub rb_custom_gaming_cashier_terminal_click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
  Handles rb_cashier.Click, rb_custom.Click, rb_gaming_table.Click, rb_terminal.Click

    Me.cmb_cashier.Enabled = True
    If Me.rb_cashier.Checked Then
      Call SetComboCashierAlias(Me.cmb_cashier, True, True)
    ElseIf Me.rb_custom.Checked Then
      ' 08-OCT-2014 RRR Changed to select only custom source/tragets
      Call SetCombo(Me.cmb_cashier, "SELECT CST_SOURCE_TARGET_ID, CST_SOURCE_TARGET_NAME FROM   CAGE_SOURCE_TARGET WHERE CST_SOURCE_TARGET_ID >= 1000", True)
    ElseIf Me.rb_gaming_table.Checked Then
      Call SetCombo(Me.cmb_cashier, "SELECT GT_GAMING_TABLE_ID, GT_NAME + ' - [' + GT_CODE + ']' AS NAME " & _
                                    "FROM   GAMING_TABLES WHERE   GT_ENABLED = 1 AND   GT_CASHIER_ID IS NOT NULL ORDER BY   GT_CODE ASC ", True)
    ElseIf Me.rb_terminal.Checked Then
      Call SetCombo(Me.cmb_cashier, "SELECT TE_MASTER_ID, TE_BASE_NAME  FROM TERMINALS " & _
                                                                " INNER JOIN TERMINALS_LAST_CHANGED  ON TE_TERMINAL_ID = TLC_TERMINAL_ID " & _
                                                                     " WHERE TE_TERMINAL_TYPE IN ( " & WSI.Common.Misc.TerminalTypeListToString(WSI.Common.Misc.AcceptorTerminalTypeList()) & " ) " & _
                                                                      "ORDER BY TE_MASTER_ID ASC ", True)
    End If

  End Sub ' rb_custom_gaming_cashier_click

  Private Sub opt_all_cashiers_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_all_cashiers.Click

    Me.rb_custom.Enabled = True
    Me.rb_gaming_table.Enabled = GamingTableBusinessLogic.IsGamingTablesEnabled
    Me.rb_cashier.Enabled = True
    Me.rb_custom.Enabled = True
    Me.cmb_cashier.Clear()
    Me.cmb_cashier.Enabled = False

  End Sub

  Private Sub opt_several_users_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_one_user.Click
    Me.cmb_user.Enabled = True
    Me.chk_show_all.Enabled = True
  End Sub ' opt_several_users_Click

  Private Sub opt_all_users_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_all_users.Click
    Me.cmb_user.Enabled = False
    Me.chk_show_all.Enabled = False
  End Sub ' opt_all_users_Click

  Private Sub chk_show_all_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_show_all.CheckedChanged
    If chk_show_all.Checked Then
      Call SetCombo(Me.cmb_user, "SELECT GU_USER_ID, GU_FULL_NAME + ' - [' + GU_USERNAME + ']' AS GU_FULL_NAME FROM GUI_USERS WHERE GU_USER_TYPE = " & WSI.Common.GU_USER_TYPE.USER & " ORDER BY GU_USERNAME")
    Else
      Call SetCombo(Me.cmb_user, "SELECT GU_USER_ID, GU_FULL_NAME + ' - [' + GU_USERNAME + ']' AS GU_FULL_NAME FROM GUI_USERS GU INNER JOIN GUI_USER_PROFILES GUP ON GU.GU_PROFILE_ID = GUP.GUP_PROFILE_ID " & _
                  " INNER JOIN GUI_PROFILE_FORMS GPF ON GUP.GUP_PROFILE_ID = GPF.GPF_PROFILE_ID WHERE GU_BLOCK_REASON = " & WSI.Common.GUI_USER_BLOCK_REASON.NONE & _
                  " AND GU_USER_TYPE = " & WSI.Common.GU_USER_TYPE.USER & " AND GPF.GPF_GUI_ID = " & WSI.Common.ENUM_GUI.CASHIER & _
                  " AND GPF.GPF_FORM_ID = " & ENUM_CASHIER_FORM.FORM_MAIN & " AND GPF.GPF_READ_PERM = 1 ORDER BY GU_FULL_NAME ")
    End If

  End Sub ' chk_show_all_CheckedChanged

  Private Sub opt_from_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) _
  Handles opt_from_cage.CheckedChanged, opt_from_cashier.CheckedChanged, opt_all.CheckedChanged

    If opt_from_cage.Checked Then
      FillCheckedlistFilterGrid(True, False)
      If opt_monitor_mode.Checked Then
        Me.uc_checked_list_status.Enabled = True
        Me.uc_checked_list_status.btn_uncheck_all.PerformClick()
        Me.uc_checked_list_status.SetValue(0)
        Me.uc_checked_list_status.SetValue(m_request_operation_pos - 1)
        Me.uc_checked_list_status.Enabled = False
      End If
    ElseIf opt_from_cashier.Checked Then
      FillCheckedlistFilterGrid(False, True)
      If opt_monitor_mode.Checked Then
        Me.uc_checked_list_status.Enabled = True
        Me.uc_checked_list_status.btn_uncheck_all.PerformClick()
        Me.uc_checked_list_status.SetValue(0)
        Me.uc_checked_list_status.Enabled = False
      End If
    Else
      FillCheckedlistFilterGrid(True, True)
      If opt_monitor_mode.Checked Then
        Me.uc_checked_list_status.Enabled = True
        Me.uc_checked_list_status.btn_uncheck_all.PerformClick()
        Me.uc_checked_list_status.SetValue(m_request_operation_pos - 1)
        Me.uc_checked_list_status.SetValue(0)
        Me.uc_checked_list_status.Enabled = False
      End If
    End If

  End Sub 'opt_from_CheckedChanged

  Private Sub opt_all_sessions_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_all_sessions.CheckedChanged
    Me.cmb_sessions.Enabled = Not opt_all_sessions.Checked
  End Sub ' opt_all_sessions_CheckedChanged

  Private Sub opt_working_day_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_working_day.CheckedChanged
    If Me.opt_working_day.Checked Then
      Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
      Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
      Me.dtp_from.Width = 240
      Me.dtp_to.Width = 240
    Else
      Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      Me.dtp_from.Width = 240
      Me.dtp_to.Width = 240
    End If
  End Sub ' opt_working_day_CheckedChanged

  Private Sub opt_monitor_mode_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_monitor_mode.CheckedChanged
    If opt_monitor_mode.Checked Then
      Call EnableMonitorOrHistory()
    End If

  End Sub ' opt_monitor_mode_CheckedChanged

  Private Sub opt_history_mode_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_history_mode.CheckedChanged
    If opt_history_mode.Checked Then
      Call EnableMonitorOrHistory()
    End If
  End Sub ' opt_history_mode_CheckedChanged

  Private Sub tmr_monitor_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmr_monitor.Tick
    If opt_monitor_mode.Checked Then
      If m_first_time Then
        Call GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)
      Else
        Call Me.GUI_ExecuteQueryCustom()
        WSI.Common.Users.SetLastAction(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7880), Me.Text) 'EOR 08-AUG-2017
      End If
      tf_latest_update.Value = WSI.Common.Format.CustomFormatTime(WSI.Common.WGDB.Now, True)
    End If
  End Sub ' tmr_monitor_Tick

#End Region ' Events

  Private Sub SaveSelectedIds()
    Dim idx As Integer

    If m_selected_row <> 0 Then
      m_selected_row = 0
      m_selected_row = Nothing
    End If

    idx = 0
    While idx < Me.Grid.NumRows
      If Me.Grid.Row(idx).IsSelected Then
        m_selected_row = idx
      End If
      idx = idx + 1
    End While
  End Sub

  Private Function DataGridMustChange(ByVal IdxGrid As Integer, ByVal Row As DataRow) As Boolean
    If Me.Grid.NumRows - 1 < IdxGrid Then
      Return True
    End If

    ' Check status field
    If Me.Grid.Cell(IdxGrid, GRID_COLUMN_MOVEMENT_ID).Value <> Row(SQL_COLUMN_ID).ToString() Then
      Return True
    End If

    Return False
  End Function

  Private Sub SelectLastSelectedRow()
    Dim idx As Integer
    Dim any_selected As Boolean

    If Me.Grid.NumRows = 0 Then
      Return
    End If

    any_selected = False
    Me.Grid.Redraw = False

    Try
      idx = 0
      While idx < Me.Grid.NumRows
        Me.Grid.Row(idx).IsSelected = False
        Call Me.Grid.RepaintRow(idx)
        idx = idx + 1
      End While

      idx = 0
      While idx < Me.Grid.NumRows
        If m_selected_row = idx Then
          any_selected = True
          Me.Grid.Row(idx).IsSelected = True
          Call Me.Grid.RepaintRow(idx)
        End If
        idx = idx + 1
      End While

      If Not any_selected And Me.Grid.CurrentRow >= 0 Then
        Me.Grid.Row(Me.Grid.CurrentRow).IsSelected = True
        Call Me.Grid.RepaintRow(Me.Grid.CurrentRow)
      End If

    Finally
      Me.Grid.Redraw = True
    End Try
  End Sub

  Private Sub GetIsoCodeColumns(table As DataTable)

    Me.m_iso_codes_sql.Clear()

    For _i As Integer = SQL_COLUMN_WORKING_DAY + 1 To table.Columns.Count - 1
      If table.Columns(_i).ColumnName.Contains(CurrencyExchangeType.CASINO_CHIP_RE) Then
        Me.m_iso_codes_sql.Add(_i, table.Columns(_i).ColumnName.Substring(0, 3) & "-" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2941) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(3320))
      ElseIf table.Columns(_i).ColumnName.Contains(CurrencyExchangeType.CASINO_CHIP_NRE) Then
        Me.m_iso_codes_sql.Add(_i, table.Columns(_i).ColumnName.Substring(0, 3) & "-" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2941) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(3321))
      ElseIf table.Columns(_i).ColumnName.Contains(CurrencyExchangeType.CASINO_CHIP_COLOR) Then
        Me.m_iso_codes_sql.Add(_i, GeneralParam.GetString("FeatureChips", "ColorType.Name", GLB_NLS_GUI_PLAYER_TRACKING.GetString(2941)))
      ElseIf table.Columns(_i).ColumnName.Contains(CurrencyExchangeType.CURRENCY) Then
        Me.m_iso_codes_sql.Add(_i, table.Columns(_i).ColumnName.Substring(0, 3))
      Else
        Me.m_iso_codes_sql.Add(_i, table.Columns(_i).ColumnName.Replace("Otros", GLB_NLS_GUI_PLAYER_TRACKING.GetString(2346)))
      End If
    Next

  End Sub

End Class
