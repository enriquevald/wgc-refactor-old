'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_statistics_providers.vb
' DESCRIPTION:   Check statistics against provider activity
' AUTHOR:        Miquel Beltran
' CREATION DATE: 26-NOV-2010
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 26-NOV-2010  MBF    Initial Draft.
' 10-MAY-2011  LCM    Added "Print" and "Export excel" buttons.
' 26-OCT-2011  RCI    Handpays queries are grouped by current provider not the provider when the handpay was entered.
' 19-JAN-2012  MPO & RCI   Fixed huge size when saving Excel reports.
' 24-JAN-2012  RCI    Don't clear format columns. Apply format only in necessary columns.
'                     Also, some minor bug fixes about GRID_COLUMN_SENTINEL and show/hide GRID_COLUMN_TERMINAL.
' 27-FEB-2012  RCI    Use non_redeemable2 when calculating account BalanceParts.
' 22-MAR-2012  RCI    When Print/Excel operations, set the PrintDateTime when FILTER_APPLY button is clicked.
' 26-MAR-2012  JAB    Fixed Error, happened to enter "'" in "provider_name".
' 03-MAY-2012  JAB    Fixed Bug #290: Check the date filter.
' 23-MAY-2012  JMM    Fixed Bug #304: Read cashier session data filtering by movements date instead of cashier session opening date.
' 30-MAY-2013  RRR    Fixed Bug #809: Escaped '%', '*', '[' and ']' characters in provider's names.
' 08-AUG-2013  ICS & RCI    Fixed Bug WIG-103: Imbalance due bank card commissions.
' 29-NOV-2013  LJM    Button event actions encapsulated, GUI_ButtonClick created, audit called.
' 17-JAN-2013  JML    Fixed Bug WIGOSTITO-994.
' 07-FEB-2014  AMF    Fixed Bug WIG-493: Don't compatible with the installed version.
' 29-MAY-2014  JAB    Added permissions: To buttons "Excel" and "Print".
' 02-SEP-2014  AMF    Progressive Jackpot
' 01-OCT-2014  ACC    Exclude HP progressive jackpots
' 17-OCT-2014  ACC    NOT Exclude HP progressive jackpots. Now insert statistics when pay progressive and matches with sessions.
' 23-OCT-2014  FJC    Fixed Bug WIG-1566: Add Provision to Progressive in statistics (column L)
' 17-NOV-2014  LEM    Fixed Bug WIG-1695: Don't opens manual adjustment for unbalanced terminals.
' 15-DEC-2014  XCD    ONLY IN CASHLESS -- Delete D column (play sessions opened)
' 19-DEC-2014  RCI    Fixed Bug WIG-1862: Open PlaySessions (column D) still in SQL query (must remove in Cashless)
' 20-JAN-2015  FJC    Set features form (size and autoscroll panels) when resolutions in 1024x768px or below 
' 20-JAN-2015  FJC    Set features form (size and autoscroll panels) when size's form exceeds desktop area
' 27-MAR-2015  ANM    Calling GetProviderIdListSelected always returns a query
' 01-FEB-2016  DHA    Product Backlog Item 8502:Floor Dual Currency: Cuadratura de estad�sticas
' 16-FEB-2015  FAV    Fixed Bug 9347 Added row with machine tickets voided
' 14-SEP-2016  FAV    Fixed Bug 17641 Timeout in request
' 09-DEC-2016  XGJ    Bug 18155:Ajuste manual de estad�sticas no accesible desde Cuadratura de estad�sticas
' 20-DEC-2016  FJC    Bug 12402:Actividad de proveedores / estad�sticas: no aparece el bot�n de ajuste manual de estad�sticas
' 16-FEB-2017  MS     Bug 24682:Wigos GUI: Cuadratura de Estadisticas displays Terminal ID in the Denomination Column
' 28-FEB-2017  XGJ    Bug 25178:Sorteo de m�quina - Totales dep�sitos en Resumen de Sorteos de m�quina
' 04-OCT-2017  DHA    Bug 30060:WIGOS-5491 [Ticket #9174] Cuadratura de Estad�stica - Denominaci�n Contable Excel V03.06.0023
'--------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_Controls.uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE
Imports GUI_CommonMisc
Imports System.Data.SqlClient
Imports WSI.Common

Public Class frm_statistics_providers
  Inherits frm_base

#Region "Constants"
  Private TERMINAL_DATA_COLUMNS As Int32

  ' Grid Columns
  Private Const GRID_HEADER_ROWS As Integer = 2

  Private GRID_COLUMN_SENTINEL As Integer
  Private GRID_INIT_TERMINAL_DATA As Integer
  Private GRID_COLUMN_PROVIDER As Integer
  Private GRID_COLUMN_TERMINAL As Integer
  Private GRID_COLUMN_TERMINAL_ID As Integer
  Private GRID_COLUMN_STATISTICS_NW As Integer
  Private GRID_COLUMN_PROVIDER_NW As Integer
  Private GRID_COLUMN_PLAYSESSION_NW As Integer
  Private GRID_COLUMN_HP_NOT_TRANSFERED As Integer
  Private GRID_COLUMN_HP_MANUAL As Integer
  Private GRID_COLUMN_GAP As Integer

  Private GRID_COLUMNS As Integer

  ' Cashier descuadre
  Private Const GRID_COLUMN_LETTER = 1
  Private Const GRID_COLUMN_CONCEPT = 2
  Private Const GRID_COLUMN_VALUE = 3
  Private Const GRID_CASHIER_COLUMNS As Integer = 4
  Private Const GRID_CASHIER_HEADER_ROWS As Integer = 1

  Private Const SQL_COLUMN_PROVIDER = 0
  ' For statistics query
  Private Const SQL_COLUMN_STATISTICS_NW = 1
  ' For Activity providers query
  Private Const SQL_COLUMN_PROVIDER_NW = 1
  ' For Open Playsessions
  Private Const SQL_COLUMN_PLAYSESSIONS_NW = 1
  ' For Handpays query
  Private Const SQL_COLUMN_HP_NOT_TRANSFERED = 1
  Private Const SQL_COLUMN_HP_MANUAL = 1

  Private Const SQL_COLUMN_TERMINAL = 2
  Private Const SQL_COLUMN_TERMINAL_ID = 3


  ' Grid counters
  Private Const COUNTER_PROVIDER_OK As Integer = 1
  Private Const COUNTER_PROVIDER_GAP As Integer = 2

  Private Const GRID_WIDTH_PROVIDER As Integer = 1900
  Private Const GRID_WIDTH_TERMINAL As Integer = 2400
  Private Const GRID_WIDTH_AMOUNT_LONG As Integer = 2100
  Private Const GRID_WIDTH_AMOUNT_SHORT As Integer = 2100
  Private Const GRID_WIDTH_CONCEPT As Integer = 5250
  Private Const GRID_WIDTH_TERMINAL_ID As Integer = 0

  Private Const FORM_DB_MIN_VERSION As Short = 183

#End Region

#Region "Enums"

#End Region ' Enums

#Region "Members"

  Private m_by_terminal As Boolean
  Private m_by_provider As Boolean
  Private m_only_unbalance As Boolean
  Private m_terminal_location As Boolean


  Private m_print_data As New GUI_Reports.CLASS_PRINT_DATA()
  Private m_excel_data As GUI_Reports.CLASS_EXCEL_DATA

  ' For report filters 
  Private m_type As String
  Private m_terminal As String
  Private m_date_from As String
  Private m_date_to As String
  Private m_closing_hour As String
  Private m_print_datetime As Date

  Private m_gaming_table_enabled As Boolean
  Private m_is_tito_mode As Boolean

  Private m_terminal_report_type As ReportType = ReportType.Provider
  Private m_refresh_grid As Boolean = False

#End Region ' Members

#Region "Overrides"

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  '
  Private Function GUI_FilterCheck(ByVal TabIndex As Integer) As Boolean
    ' Dates selection
    Select Case TabIndex
      Case 0
        If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
          If Me.uc_dsl.FromDate > Me.uc_dsl.ToDate Then
            Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
            Call Me.uc_dsl.Focus()

            Return False
          End If
        End If
      Case 1
        If Me.uc_dsl_cashier.FromDateSelected And Me.uc_dsl_cashier.ToDateSelected Then
          If Me.uc_dsl_cashier.FromDate > Me.uc_dsl_cashier.ToDate Then
            Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
            Call Me.uc_dsl.Focus()

            Return False
          End If
        End If
    End Select

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Set the internal Form Id
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_SITE_GAP_REPORT

    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)

    'Call Base Form proc
    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Initialize all form controls
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    m_gaming_table_enabled = GamingTableBusinessLogic.IsGamingTablesEnabled()
    m_is_tito_mode = WSI.Common.TITO.Utils.IsTitoMode()

    ' Form Title
    Me.Text = GLB_NLS_GUI_CONFIGURATION.GetString(482)

    Me.btn_print.Text = GLB_NLS_GUI_CONTROLS.GetString(5)
    Me.btn_excel.Text = GLB_NLS_GUI_CONTROLS.GetString(27)
    Me.btn_exit.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(2)
    Me.btn_select.Text = GLB_NLS_GUI_CONTROLS.GetString(9)

    ' Date time filter
    Me.uc_dsl.Init(GLB_NLS_GUI_INVOICING.Id(201))
    Me.uc_dsl.ShowBorder = True
    Me.uc_dsl_cashier.Init(GLB_NLS_GUI_INVOICING.Id(201))
    Me.uc_dsl_cashier.ShowBorder = True


    Me.btn_filter_apply.Text = GLB_NLS_GUI_CONTROLS.GetString(8)
    Me.btn_filter_reset.Text = GLB_NLS_GUI_CONTROLS.GetString(7)

    'Me.opt_all_providers.Text = GLB_NLS_GUI_STATISTICS.GetString(338)
    Me.chk_only_unbalance.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7187)
    Me.opt_by_provider.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7186)
    Me.opt_by_terminal.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7184)

    Me.lbl_a.Text = "A - " & GLB_NLS_GUI_STATISTICS.GetString(318)
    Me.lbl_b.Text = "B - " & GLB_NLS_GUI_STATISTICS.GetString(319)
    Me.lbl_c.Text = "C - " & GLB_NLS_GUI_STATISTICS.GetString(344)
    Me.lbl_d.Text = "D - " & GLB_NLS_GUI_STATISTICS.GetString(320)
    Me.lbl_e.Text = "E - " & GLB_NLS_GUI_STATISTICS.GetString(321)

    If m_is_tito_mode Then
      Me.lbl_note.Visible = False
    Else
      Me.lbl_note.Visible = True
      Me.lbl_note.Text = "*: " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5798)
    End If

    Me.TabPage1.Text = GLB_NLS_GUI_STATISTICS.GetString(331)
    Me.TabPage2.Text = GLB_NLS_GUI_STATISTICS.GetString(332)

    ' DHA 01-FEB-2016: Hidde tab when is dual currency enabled
    If WSI.Common.Misc.IsFloorDualCurrencyEnabled() Then
      Me.tb_control.TabPages.Remove(Me.TabPage2)
    End If

    Me.gb_labels.Text = GLB_NLS_GUI_STATISTICS.GetString(331)

    btn_excel.Enabled = False
    btn_print.Enabled = False
    btn_select.Enabled = False

    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7185)

    Call SetDefaultValues()
    Call GUI_StyleSheet()
    Call GUI_StyleSheetCashier()

  End Sub ' GUI_InitControls

#End Region ' Overrides

#Region "Private Functions"

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Dim closing_time As Integer
    Dim final_time As Date
    Dim _now As Date

    _now = WSI.Common.WGDB.Now

    ' Providers - Terminals
    Call Me.uc_pr_list.Init(WSI.Common.Misc.GamingTerminalTypeList())

    Call Me.uc_pr_list.SetDefaultValues()

    closing_time = GetDefaultClosingTime()
    final_time = New DateTime(_now.Year, _now.Month, _now.Day, closing_time, 0, 0)
    If final_time > _now Then
      final_time = final_time.AddDays(-1)
    End If

    final_time = final_time.Date
    Me.uc_dsl.ToDate = final_time
    Me.uc_dsl.ToDateSelected = True
    Me.uc_dsl.FromDate = final_time.AddDays(-1)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.ClosingTime = closing_time

    Me.uc_dsl_cashier.ToDate = final_time
    Me.uc_dsl_cashier.ToDateSelected = True
    Me.uc_dsl_cashier.FromDate = final_time.AddDays(-1)
    Me.uc_dsl_cashier.FromDateSelected = True
    Me.uc_dsl_cashier.ClosingTime = closing_time

    Me.opt_by_terminal.Checked = True
    Me.opt_by_provider.Checked = False
    'Me.opt_all_providers.Checked = True

    Me.chk_terminal_location.Checked = False
    Me.chk_terminal_location.Enabled = opt_by_terminal.Checked

    Me.chk_only_unbalance.Checked = False

  End Sub ' SetDefaultValues

  ' PURPOSE: Refresh grid for cashier gap problems
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Private Sub RefreshGridCashier()

    Dim _total_redeemable As Decimal
    Dim _totat_not_redeemable As Decimal
    Dim _from_date As Date
    Dim _to_date As Date
    Dim _str_calcul As String

    Dim _cash_days_data As TYPE_PROVIDER_CASH_ITEM
    Dim _closing_time As String

    _closing_time = Me.uc_dsl.ClosingTime.ToString("00")

    Try

      If Me.uc_dsl.FromDateSelected Then
        _from_date = Me.uc_dsl_cashier.FromDate.AddHours(Me.uc_dsl_cashier.ClosingTime)
      Else
        _from_date = New Date(2007, 1, 1, 0, 0, 0)  ' 17-01-2013   JML    Fixed defect WIGOSTITO-994
      End If

      If Me.uc_dsl.ToDateSelected Then
        _to_date = Me.uc_dsl_cashier.ToDate.AddHours(Me.uc_dsl_cashier.ClosingTime)
      Else
        _to_date = WSI.Common.WGDB.Now.AddDays(1)
      End If

      _cash_days_data.cash_date = GLB_NLS_GUI_STATISTICS.GetString(203)

      Using _db_trx As New WSI.Common.DB_TRX()
        GetProvCashData(_from_date, _to_date, _closing_time, _db_trx.SqlTransaction, _cash_days_data)

        CalculateTotalAssets(_total_redeemable, _totat_not_redeemable, _db_trx.SqlTransaction)
      End Using

      ' Actividad de proveedores [total]
      AddRowCashier("A", GLB_NLS_GUI_STATISTICS.GetString(330), _cash_days_data.provider_activity_total, False)

      ' Actividad de proveedores [no redimible]
      AddRowCashier("B", GLB_NLS_GUI_STATISTICS.GetString(329), _cash_days_data.provider_activity_no_redeemable, False)

      ' Actividad de proveedores [redimible]
      AddRowCashier("A-B", GLB_NLS_GUI_STATISTICS.GetString(327), _cash_days_data.provider_activity_redeemable, True)

      Me.dg_list_cashier.AddRow()

      ' Actividad de proveedores [redimible]
      AddRowCashier("C", GLB_NLS_GUI_STATISTICS.GetString(327), _cash_days_data.provider_activity_redeemable, False)

      ' Sesiones de juego activas
      If m_is_tito_mode Then
        AddRowCashier("D", GLB_NLS_GUI_STATISTICS.GetString(343), _cash_days_data.play_sessions_open, False)
      End If

      ' Resultado de caja
      AddRowCashier(IIf(m_is_tito_mode, "E", "D"), GLB_NLS_GUI_STATISTICS.GetString(326), _cash_days_data.cash_results, False)

      ' Otros ingresos: Depositos tarjetas + comisiones divisas y tarjetas bancarias
      AddRowCashier(IIf(m_is_tito_mode, "F", "E"), GLB_NLS_GUI_STATISTICS.GetString(342), _cash_days_data.other_income, False)

      ' Creditos caducados
      AddRowCashier(IIf(m_is_tito_mode, "G", "F"), GLB_NLS_GUI_STATISTICS.GetString(325), _cash_days_data.credits_expired, False)

      ' Credito no redimible convertido a redimible
      AddRowCashier(IIf(m_is_tito_mode, "H", "G"), GLB_NLS_GUI_STATISTICS.GetString(328), _cash_days_data.converted_credit, False)

      ' Jackpot de sala
      AddRowCashier(IIf(m_is_tito_mode, "I", "H"), GLB_NLS_GUI_STATISTICS.GetString(324), _cash_days_data.hall_payments, False)

      If m_is_tito_mode Then
        _str_calcul = "-C-D+E-F-G+H+I"

        If m_gaming_table_enabled Then
          _str_calcul &= "+J-K-L"
          ' Gaming Tables
          AddRowCashier("J", GLB_NLS_GUI_STATISTICS.GetString(348), _cash_days_data.gaming_table_open, False)
          AddRowCashier("K", GLB_NLS_GUI_STATISTICS.GetString(349), _cash_days_data.gaming_table_circulation, False)
          'Progressive Provision
          AddRowCashier("L", GLB_NLS_GUI_PLAYER_TRACKING.GetString(5799), _cash_days_data.progressive_provision, False)
        Else
          _str_calcul &= "-L"
          'Progressive Provision
          AddRowCashier("L", GLB_NLS_GUI_PLAYER_TRACKING.GetString(5799), _cash_days_data.progressive_provision, False)
        End If

        ' Tickets de maquina anulados
        _str_calcul &= "-M"
        AddRowCashier("M", GLB_NLS_GUI_PLAYER_TRACKING.GetString(7123), _cash_days_data.machine_tickets_voided, False)

      Else
        _str_calcul = "-C+D-E-F+G+H"
        If m_gaming_table_enabled Then
          _str_calcul &= "+I-J-K"
          ' Gaming Tables
          AddRowCashier("I", GLB_NLS_GUI_STATISTICS.GetString(348), _cash_days_data.gaming_table_open, False)
          AddRowCashier("J", GLB_NLS_GUI_STATISTICS.GetString(349), _cash_days_data.gaming_table_circulation, False)
          'Progressive Provision
          AddRowCashier("K", GLB_NLS_GUI_PLAYER_TRACKING.GetString(5799), _cash_days_data.progressive_provision, False)
        Else
          _str_calcul &= "-K"
          'Progressive Provision
          AddRowCashier("K", GLB_NLS_GUI_PLAYER_TRACKING.GetString(5799), _cash_days_data.progressive_provision, False)
        End If
      End If

      ' Descuadre
      AddRowCashier(_str_calcul, GLB_NLS_GUI_STATISTICS.GetString(323), _cash_days_data.redeemable_circulation_increase, True)

      Me.dg_list_cashier.AddRow()

      AddRowCashier("", GLB_NLS_GUI_STATISTICS.GetString(340) & " (" & GUI_FormatDate(WSI.Common.WGDB.Now, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS) & ")", _total_redeemable, False)
      AddRowCashier("", GLB_NLS_GUI_STATISTICS.GetString(341) & " (" & GUI_FormatDate(WSI.Common.WGDB.Now, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS) & ")", _totat_not_redeemable, False)

    Catch ex As Exception

      Me.dg_list_cashier.Clear()

      Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(333), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ex.Message)

      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "frm_statistics_providers", _
                            "RefreshGridCashier", _
                            ex.Message)
    End Try


  End Sub ' RefreshGridCashier

  Private Sub AddRowCashier(ByVal Letter As String, ByVal Concept As String, ByVal Value As Decimal, ByVal IsSum As Boolean)
    Me.dg_list_cashier.Redraw = False
    Me.dg_list_cashier.AddRow()

    Me.dg_list_cashier.Cell(Me.dg_list_cashier.NumRows - 1, GRID_COLUMN_LETTER).Value = Letter
    Me.dg_list_cashier.Cell(Me.dg_list_cashier.NumRows - 1, GRID_COLUMN_CONCEPT).Value = Concept
    Me.dg_list_cashier.Cell(Me.dg_list_cashier.NumRows - 1, GRID_COLUMN_VALUE).Value = GUI_FormatCurrency(Value)

    If IsSum Then
      Me.dg_list_cashier.Row(Me.dg_list_cashier.NumRows - 1).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
      Me.dg_list_cashier.Row(Me.dg_list_cashier.NumRows - 1).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
    End If

    Me.dg_list_cashier.Redraw = True
  End Sub

  ' PURPOSE: Refresh data in the grid with fresh info from database
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     

  Private Sub RefreshGrid()

    Dim data_table As DataTable
    Dim row_index As Integer
    Dim data_row As DataRow

    Dim _total_sta_nw As Double
    Dim _total_pro_nw As Double
    Dim _total_ps_nw As Double
    Dim _total_hp_man As Double
    Dim _total_hp_not As Double
    Dim _gap As Double
    Dim _gap_count_ok As Integer
    Dim _gap_count As Integer
    Dim _total_gap As Double


    Dim provider_name As String

    Try

      m_by_terminal = opt_by_terminal.Checked
      m_by_provider = opt_by_provider.Checked

      m_only_unbalance = chk_only_unbalance.Checked
      m_terminal_location = chk_terminal_location.Checked

      Call GUI_StyleSheet()

      Me.dg_list.Update()

      ' ====== Get provider-statistics START ======

      ' Get provider list
      If Not m_by_terminal Then
        data_table = GUI_GetTableUsingSQL(GetSqlQueryProviderList(), 5000)
        If IsNothing(data_table) Then
          Me.dg_list.Clear()
          Exit Sub
        End If

        row_index = 0

        ' New rows to add
        For Each data_row In data_table.Rows
          Me.dg_list.Redraw = False
          Me.dg_list.AddRow()
          InitRow(row_index)

          UpdateRowProviderTerminalData(data_row, row_index)

          row_index += 1
          Me.dg_list.Redraw = True
        Next
      End If

      ' Get provider-statistics
      data_table = GUI_GetTableUsingSQL(GetSqlQueryProviderStatistics(), 5000, , , 60)
      If IsNothing(data_table) Then
        Me.dg_list.Clear()
        Exit Sub
      End If

      row_index = 0


      ' update existing rows
      While row_index < Me.dg_list.NumRows

        Dim found_rows() As Data.DataRow

        ' Search for terminal
        If m_by_terminal Then
          provider_name = Me.dg_list.Cell(row_index, GRID_COLUMN_TERMINAL).Value
          found_rows = data_table.Select("Terminal Like '" + DataTable_FilterValue(provider_name) + "'") 'provider_name.Replace("'", "''").Replace("%", "[%]") + "'")
        Else
          provider_name = Me.dg_list.Cell(row_index, GRID_COLUMN_PROVIDER).Value
          found_rows = data_table.Select("Provider Like '" + DataTable_FilterValue(provider_name) + "'") 'provider_name.Replace("'", "''").Replace("%", "[%]") + "'")
        End If

        If found_rows.Length > 0 Then

          ' Update fields
          Me.dg_list.Redraw = False
          UpdateRowStatistics(found_rows(0), row_index)
          Me.dg_list.Redraw = True

          ' mark row from datatable to be deleted
          found_rows(0).Delete()


          row_index = row_index + 1

        Else
          ' Terminal not found, but do nothing
          row_index = row_index + 1
        End If

      End While

      data_table.AcceptChanges()

      ' New rows to add
      For Each data_row In data_table.Rows
        Me.dg_list.Redraw = False
        Me.dg_list.AddRow()
        InitRow(row_index)

        UpdateRowStatistics(data_row, row_index)

        row_index += 1
        Me.dg_list.Redraw = True
      Next

      ' ====== Get provider-statistics END ======

      ' ====== Get provider-activity START ======

      ' Get provider-activuty
      data_table = GUI_GetTableUsingSQL(GetSqlQueryProviderActivity(), 5000, , , 60)
      If IsNothing(data_table) Then
        Me.dg_list.Clear()
        Exit Sub
      End If

      row_index = 0

      ' update existing rows
      While row_index < Me.dg_list.NumRows

        Dim found_rows() As Data.DataRow

        ' Search for terminal
        If m_by_terminal Then
          provider_name = Me.dg_list.Cell(row_index, GRID_COLUMN_TERMINAL).Value
          found_rows = data_table.Select("Terminal Like '" + DataTable_FilterValue(provider_name) + "'") 'provider_name.Replace("'", "''").Replace("%", "[%]") + "'")
        Else
          provider_name = Me.dg_list.Cell(row_index, GRID_COLUMN_PROVIDER).Value
          found_rows = data_table.Select("Provider Like '" + DataTable_FilterValue(provider_name) + "'") 'provider_name.Replace("'", "''").Replace("%", "[%]") + "'")
        End If

        If found_rows.Length > 0 Then

          ' Update fields
          Me.dg_list.Redraw = False
          UpdateRowActivity(found_rows(0), row_index)
          Me.dg_list.Redraw = True

          ' mark row from datatable to be deleted
          found_rows(0).Delete()


          row_index = row_index + 1

        Else
          ' Terminal not found, but do nothing
          row_index = row_index + 1
        End If

      End While

      data_table.AcceptChanges()

      ' New rows to add
      For Each data_row In data_table.Rows
        Me.dg_list.Redraw = False
        Me.dg_list.AddRow()
        InitRow(row_index)

        UpdateRowActivity(data_row, row_index)

        row_index += 1
        Me.dg_list.Redraw = True
      Next

      ' ====== Get provider-activity END ======

      ' ====== Get provider-activity START ======

      ' Get provider-activuty
      data_table = GUI_GetTableUsingSQL(GetSqlQueryOpenPlaySessions(), 5000, , , 60)
      If IsNothing(data_table) Then
        Me.dg_list.Clear()
        Exit Sub
      End If

      row_index = 0

      ' update existing rows
      While row_index < Me.dg_list.NumRows

        Dim found_rows() As Data.DataRow

        ' Search for terminal
        If m_by_terminal Then
          provider_name = Me.dg_list.Cell(row_index, GRID_COLUMN_TERMINAL).Value
          found_rows = data_table.Select("Terminal Like '" + DataTable_FilterValue(provider_name) + "'") 'provider_name.Replace("'", "''").Replace("%", "[%]") + "'")
        Else
          provider_name = Me.dg_list.Cell(row_index, GRID_COLUMN_PROVIDER).Value
          found_rows = data_table.Select("Provider Like '" + DataTable_FilterValue(provider_name) + "'") 'provider_name.Replace("'", "''").Replace("%", "[%]") + "'")
        End If

        If found_rows.Length > 0 Then

          ' Update fields
          Me.dg_list.Redraw = False
          UpdateRowPlaySessions(found_rows(0), row_index)
          Me.dg_list.Redraw = True

          ' mark row from datatable to be deleted
          found_rows(0).Delete()


          row_index = row_index + 1

        Else
          ' Terminal not found, but do nothing
          row_index = row_index + 1
        End If

      End While

      data_table.AcceptChanges()

      ' New rows to add
      For Each data_row In data_table.Rows
        Me.dg_list.Redraw = False
        Me.dg_list.AddRow()
        InitRow(row_index)

        UpdateRowPlaySessions(data_row, row_index)

        row_index += 1
        Me.dg_list.Redraw = True
      Next

      ' ====== Get provider-activity END ======

      ' ====== Get Handpays Manual START ======

      ' Get provider-activuty
      data_table = GUI_GetTableUsingSQL(GetSqlQueryHandpaysManual(), 5000, , , 60)
      If IsNothing(data_table) Then
        Me.dg_list.Clear()
        Exit Sub
      End If

      row_index = 0

      ' update existing rows
      While row_index < Me.dg_list.NumRows

        Dim found_rows() As Data.DataRow

        ' Search for terminal
        If m_by_terminal Then
          provider_name = Me.dg_list.Cell(row_index, GRID_COLUMN_TERMINAL).Value
          found_rows = data_table.Select("Terminal Like '" + DataTable_FilterValue(provider_name) + "'") 'provider_name.Replace("'", "''").Replace("%", "[%]") + "'")
        Else
          provider_name = Me.dg_list.Cell(row_index, GRID_COLUMN_PROVIDER).Value
          found_rows = data_table.Select("Provider Like '" + DataTable_FilterValue(provider_name) + "'") 'provider_name.Replace("'", "''").Replace("%", "[%]") + "'")
        End If

        If found_rows.Length > 0 Then

          ' Update fields
          Me.dg_list.Redraw = False
          UpdateRowHandpaysManual(found_rows(0), row_index)
          Me.dg_list.Redraw = True

          ' mark row from datatable to be deleted
          found_rows(0).Delete()


          row_index = row_index + 1

        Else
          ' Terminal not found, but do nothing
          row_index = row_index + 1
        End If

      End While

      data_table.AcceptChanges()

      ' New rows to add
      For Each data_row In data_table.Rows
        Me.dg_list.Redraw = False
        Me.dg_list.AddRow()
        InitRow(row_index)

        UpdateRowHandpaysManual(data_row, row_index)

        row_index += 1
        Me.dg_list.Redraw = True
      Next

      ' ====== Get Handpays Manual END ======


      ' ====== Get Handpays Not Transfered START ======

      ' Get provider-activuty
      data_table = GUI_GetTableUsingSQL(GetSqlQueryHandpaysNotTransfered(), 5000, , , 60)
      If IsNothing(data_table) Then
        Me.dg_list.Clear()
        Exit Sub
      End If

      row_index = 0

      ' update existing rows
      While row_index < Me.dg_list.NumRows

        Dim found_rows() As Data.DataRow

        ' Search for terminal
        If m_by_terminal Then
          provider_name = Me.dg_list.Cell(row_index, GRID_COLUMN_TERMINAL).Value
          found_rows = data_table.Select("Terminal Like '" + DataTable_FilterValue(provider_name) + "'") 'provider_name.Replace("'", "''").Replace("%", "[%]") + "'")
        Else
          provider_name = Me.dg_list.Cell(row_index, GRID_COLUMN_PROVIDER).Value
          found_rows = data_table.Select("Provider Like '" + DataTable_FilterValue(provider_name) + "'") 'provider_name.Replace("'", "''").Replace("%", "[%]") + "'")
        End If

        If found_rows.Length > 0 Then

          ' Update fields
          Me.dg_list.Redraw = False
          UpdateRowHandpaysNotTransfered(found_rows(0), row_index)
          Me.dg_list.Redraw = True

          ' mark row from datatable to be deleted
          found_rows(0).Delete()


          row_index = row_index + 1

        Else
          ' Terminal not found, but do nothing
          row_index = row_index + 1
        End If

      End While

      data_table.AcceptChanges()

      ' New rows to add
      For Each data_row In data_table.Rows
        Me.dg_list.Redraw = False
        Me.dg_list.AddRow()
        InitRow(row_index)

        UpdateRowHandpaysNotTransfered(data_row, row_index)

        row_index += 1
        Me.dg_list.Redraw = True
      Next

      ' ====== Get Handpays Not Transfered  END ======

      ' ====== Get totals and counts ======

      row_index = 0

      While row_index < Me.dg_list.NumRows

        _gap = GUI_ParseCurrency(Me.dg_list.Cell(row_index, GRID_COLUMN_GAP).Value)

        If _gap = 0 Then
          'If m_by_terminal Or m_by_provider Then
          If m_only_unbalance Then
            Me.dg_list.Row(row_index).Height = 0
            row_index += 1
            Continue While
          End If
          _gap_count_ok += 1
        Else
          _gap_count += 1
        End If

        _total_sta_nw += GUI_ParseCurrency(Me.dg_list.Cell(row_index, GRID_COLUMN_STATISTICS_NW).Value)
        _total_pro_nw += GUI_ParseCurrency(Me.dg_list.Cell(row_index, GRID_COLUMN_PROVIDER_NW).Value)
        _total_ps_nw += GUI_ParseCurrency(Me.dg_list.Cell(row_index, GRID_COLUMN_PLAYSESSION_NW).Value)
        _total_hp_man += GUI_ParseCurrency(Me.dg_list.Cell(row_index, GRID_COLUMN_HP_MANUAL).Value)
        _total_hp_not += GUI_ParseCurrency(Me.dg_list.Cell(row_index, GRID_COLUMN_HP_NOT_TRANSFERED).Value)

        _total_gap += _gap

        row_index += 1

      End While

      Me.dg_list.Redraw = False
      Me.dg_list.AddRow()
      InitRow(row_index)

      Me.dg_list.Cell(row_index, GRID_COLUMN_PROVIDER).Value = "Total:"
      Me.dg_list.Cell(row_index, GRID_COLUMN_STATISTICS_NW).Value = GUI_FormatCurrency(_total_sta_nw)
      Me.dg_list.Cell(row_index, GRID_COLUMN_PROVIDER_NW).Value = GUI_FormatCurrency(_total_pro_nw)
      Me.dg_list.Cell(row_index, GRID_COLUMN_PLAYSESSION_NW).Value = GUI_FormatCurrency(_total_ps_nw)
      Me.dg_list.Cell(row_index, GRID_COLUMN_HP_MANUAL).Value = GUI_FormatCurrency(_total_hp_man)
      Me.dg_list.Cell(row_index, GRID_COLUMN_HP_NOT_TRANSFERED).Value = GUI_FormatCurrency(_total_hp_not)
      Me.dg_list.Cell(row_index, GRID_COLUMN_GAP).Value = GUI_FormatCurrency(_total_gap)

      Me.dg_list.Row(row_index).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)

      For _col_index As Integer = 0 To Me.dg_list.NumColumns - 1
        Me.dg_list.Cell(row_index, _col_index).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
      Next

      If _total_gap <> 0 Then
        Me.dg_list.Cell(row_index, GRID_COLUMN_GAP).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
        Me.dg_list.Cell(row_index, GRID_COLUMN_GAP).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_RED_00)
      End If

      'Me.dg_list.Counter(COUNTER_PROVIDER_GAP).Value = _gap_count
      'Me.dg_list.Counter(COUNTER_PROVIDER_OK).Value = _gap_count_ok

      Me.dg_list.Redraw = True

    Catch ex As Exception

      Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(333), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ex.Message)

      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "frm_statistics_providers", _
                            "RefreshGrid", _
                            ex.Message)

    End Try


  End Sub ' RefreshGrid

  ' PURPOSE: Init an empty row
  '
  '  PARAMS:
  '     - INPUT:
  '           - RowIndex
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '   
  Private Sub InitRow(ByVal RowIndex As Integer)

    Me.dg_list.Cell(RowIndex, GRID_COLUMN_STATISTICS_NW).Value = GUI_FormatCurrency(0)
    Me.dg_list.Cell(RowIndex, GRID_COLUMN_PROVIDER_NW).Value = GUI_FormatCurrency(0)
    Me.dg_list.Cell(RowIndex, GRID_COLUMN_PLAYSESSION_NW).Value = GUI_FormatCurrency(0)
    Me.dg_list.Cell(RowIndex, GRID_COLUMN_HP_MANUAL).Value = GUI_FormatCurrency(0)
    Me.dg_list.Cell(RowIndex, GRID_COLUMN_HP_NOT_TRANSFERED).Value = GUI_FormatCurrency(0)
    Me.dg_list.Cell(RowIndex, GRID_COLUMN_GAP).Value = GUI_FormatCurrency(0)

  End Sub ' InitRow

  ' PURPOSE: Update or insert data in grid - Handpay not transfered
  '
  '  PARAMS:
  '     - INPUT:
  '           - DataRow
  '           - RowIndex
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '   
  Private Function UpdateRowHandpaysNotTransfered(ByVal Row As DataRow, ByVal RowIndex As Integer) As Integer

    Dim fore_color As Color
    Dim back_color As Color

    Call UpdateRowProviderTerminalData(Row, RowIndex)

    Me.dg_list.Cell(RowIndex, GRID_COLUMN_HP_NOT_TRANSFERED).Value = GUI_FormatCurrency(Row.Item(SQL_COLUMN_HP_NOT_TRANSFERED))

    Me.dg_list.Cell(RowIndex, GRID_COLUMN_GAP).Value = GUI_FormatCurrency(GUI_ParseCurrency(Me.dg_list.Cell(RowIndex, GRID_COLUMN_GAP).Value) + GUI_ParseCurrency(Me.dg_list.Cell(RowIndex, GRID_COLUMN_HP_NOT_TRANSFERED).Value))

    GetUpdateStatusColor(GUI_ParseCurrency(Me.dg_list.Cell(RowIndex, GRID_COLUMN_GAP).Value), fore_color, back_color)
    Me.dg_list.Row(RowIndex).ForeColor = fore_color
    Me.dg_list.Row(RowIndex).BackColor = back_color

    Return 1
  End Function ' UpdateRowHandpaysNotTransfered

  ' PURPOSE: Update or insert data in grid - Handpay manual
  '
  '  PARAMS:
  '     - INPUT:
  '           - DataRow
  '           - RowIndex
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '   
  Private Function UpdateRowHandpaysManual(ByVal Row As DataRow, ByVal RowIndex As Integer) As Integer

    Dim fore_color As Color
    Dim back_color As Color

    Call UpdateRowProviderTerminalData(Row, RowIndex)

    Me.dg_list.Cell(RowIndex, GRID_COLUMN_HP_MANUAL).Value = GUI_FormatCurrency(Row.Item(SQL_COLUMN_HP_MANUAL))


    Me.dg_list.Cell(RowIndex, GRID_COLUMN_GAP).Value = GUI_FormatCurrency(GUI_ParseCurrency(Me.dg_list.Cell(RowIndex, GRID_COLUMN_GAP).Value) - GUI_ParseCurrency(Me.dg_list.Cell(RowIndex, GRID_COLUMN_HP_MANUAL).Value))

    GetUpdateStatusColor(GUI_ParseCurrency(Me.dg_list.Cell(RowIndex, GRID_COLUMN_GAP).Value), fore_color, back_color)
    Me.dg_list.Row(RowIndex).ForeColor = fore_color
    Me.dg_list.Row(RowIndex).BackColor = back_color

    Return 1
  End Function ' UpdateRowHandpaysManual


  ' PURPOSE: Update or insert data in grid - Provider name
  '
  '  PARAMS:
  '     - INPUT:
  '           - DataRow
  '           - RowIndex
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '   
  Private Function UpdateRowProviderTerminalData(ByVal Row As DataRow, ByVal RowIndex As Integer) As Integer
    Dim _terminal_data As List(Of Object)

    If m_by_terminal Then
      If String.IsNullOrEmpty(Me.dg_list.Cell(RowIndex, GRID_INIT_TERMINAL_DATA).Value) Then
        Me.dg_list.Cell(RowIndex, GRID_COLUMN_TERMINAL_ID).Value = Row.Item(SQL_COLUMN_TERMINAL_ID)
        _terminal_data = TerminalReport.GetReportDataList(Row.Item(SQL_COLUMN_TERMINAL_ID), m_terminal_report_type)
        For _idx As Int32 = 0 To _terminal_data.Count - 1
          If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value Then
            Me.dg_list.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
          End If
        Next
      End If
    Else
      If (Not IsDBNull(Row.Item(SQL_COLUMN_PROVIDER))) Then
        Me.dg_list.Cell(RowIndex, GRID_COLUMN_PROVIDER).Value = Row.Item(SQL_COLUMN_PROVIDER)
      Else
        Me.dg_list.Cell(RowIndex, GRID_COLUMN_PROVIDER).Value = ""
      End If
    End If

    Return 1

  End Function ' UpdateRowProviderTerminalData

  ' PURPOSE: Update or insert data in grid - Statistics net win
  '
  '  PARAMS:
  '     - INPUT:
  '           - DataRow
  '           - RowIndex
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '   
  Private Function UpdateRowStatistics(ByVal Row As DataRow, ByVal RowIndex As Integer) As Integer

    Dim fore_color As Color
    Dim back_color As Color

    Call UpdateRowProviderTerminalData(Row, RowIndex)

    Me.dg_list.Cell(RowIndex, GRID_COLUMN_STATISTICS_NW).Value = GUI_FormatCurrency(Row.Item(SQL_COLUMN_STATISTICS_NW))

    Me.dg_list.Cell(RowIndex, GRID_COLUMN_GAP).Value = Me.dg_list.Cell(RowIndex, GRID_COLUMN_STATISTICS_NW).Value

    GetUpdateStatusColor(GUI_ParseCurrency(Me.dg_list.Cell(RowIndex, GRID_COLUMN_GAP).Value), fore_color, back_color)
    Me.dg_list.Row(RowIndex).ForeColor = fore_color
    Me.dg_list.Row(RowIndex).BackColor = back_color

    Return 1
  End Function ' UpdateRow


  ' PURPOSE: Update or insert data in grid - Provider activity net win
  '
  '  PARAMS:
  '     - INPUT:
  '           - DataRow
  '           - RowIndex
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '   
  Private Function UpdateRowActivity(ByVal Row As DataRow, ByVal RowIndex As Integer) As Integer

    Dim fore_color As Color
    Dim back_color As Color

    Call UpdateRowProviderTerminalData(Row, RowIndex)

    Me.dg_list.Cell(RowIndex, GRID_COLUMN_PROVIDER_NW).Value = GUI_FormatCurrency(Row.Item(SQL_COLUMN_PROVIDER_NW))

    Me.dg_list.Cell(RowIndex, GRID_COLUMN_GAP).Value = GUI_FormatCurrency(GUI_ParseCurrency(Me.dg_list.Cell(RowIndex, GRID_COLUMN_GAP).Value) - GUI_ParseCurrency(Me.dg_list.Cell(RowIndex, GRID_COLUMN_PROVIDER_NW).Value))

    GetUpdateStatusColor(GUI_ParseCurrency(Me.dg_list.Cell(RowIndex, GRID_COLUMN_GAP).Value), fore_color, back_color)
    Me.dg_list.Row(RowIndex).ForeColor = fore_color
    Me.dg_list.Row(RowIndex).BackColor = back_color

    Return 1
  End Function ' UpdateRowActivity

  ' PURPOSE: Update or insert data in grid - Open Playsessions net win
  '
  '  PARAMS:
  '     - INPUT:
  '           - DataRow
  '           - RowIndex
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '   
  Private Function UpdateRowPlaySessions(ByVal Row As DataRow, ByVal RowIndex As Integer) As Integer

    Dim fore_color As Color
    Dim back_color As Color

    Call UpdateRowProviderTerminalData(Row, RowIndex)

    Me.dg_list.Cell(RowIndex, GRID_COLUMN_PLAYSESSION_NW).Value = GUI_FormatCurrency(Row.Item(SQL_COLUMN_PLAYSESSIONS_NW))

    Me.dg_list.Cell(RowIndex, GRID_COLUMN_GAP).Value = GUI_FormatCurrency(GUI_ParseCurrency(Me.dg_list.Cell(RowIndex, GRID_COLUMN_GAP).Value) - GUI_ParseCurrency(Me.dg_list.Cell(RowIndex, GRID_COLUMN_PLAYSESSION_NW).Value))

    GetUpdateStatusColor(GUI_ParseCurrency(Me.dg_list.Cell(RowIndex, GRID_COLUMN_GAP).Value), fore_color, back_color)
    Me.dg_list.Row(RowIndex).ForeColor = fore_color
    Me.dg_list.Row(RowIndex).BackColor = back_color

    Return 1
  End Function ' UpdateRowPlaySessions

  ' PURPOSE: Build the SQL query to obtain the relevant data from database
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '     
  Private Function GetSqlQueryOpenPlaySessions() As String

    Dim _str_sql As String
    Dim _date_filter As String

    _date_filter = Me.uc_dsl.GetSqlFilterCondition("PS_STARTED")

    ' Get Select and from
    _str_sql = " SELECT Provider, SUM(played - won) as Netwin " & _
               IIf(m_by_terminal, " , Terminal, Terminal_id ", "") & _
              " FROM (SELECT SUM(ps_total_cash_in) AS played " & _
              "            , SUM(ps_total_cash_out) AS won " & _
              "            , te_provider_id AS Provider " & _
              "            , te_name AS Terminal, PS_terminal_id as terminal_id " & _
              "        FROM PLAY_SESSIONS  WITH (INDEX(IX_ps_started)), terminals " & _
              "       WHERE te_terminal_id = PLAY_SESSIONS.PS_terminal_id " & _
              "         AND " & IIf(_date_filter.Length > 0, _date_filter, "1 = 1 ") & _
              "         AND PS_STATUS = 0 " & _
              "         AND PS_PROMO = 0 "

    ' Filter Providers - Terminals
    _str_sql = _str_sql & " AND PS_terminal_id IN " & Me.uc_pr_list.GetProviderIdListSelected()

    _str_sql = _str_sql & "GROUP BY PS_terminal_id, terminals.te_provider_id, terminals.te_name) AS x "
    _str_sql = _str_sql & "GROUP BY Provider " & IIf(m_by_terminal, " , Terminal, Terminal_id ", "")

    Return _str_sql

  End Function ' GetSqlQueryOpenPlaySessions


  ' PURPOSE: Build the SQL query to obtain the relevant data from database
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '     
  Private Function GetSqlQueryHandpaysNotTransfered() As String

    Dim _str_sql As String
    Dim _date_filter As String

    ' Get Select and from
    ' RCI 26-OCT-2011: Group by current provider (TE_PROVIDER_ID) not the provider
    '                  when the handpay was entered (HP_TE_PROVIDER_ID).
    _str_sql = "SELECT TE_PROVIDER_ID AS Provider " & _
              " , sum ( HP_AMOUNT ) " & _
              IIf(m_by_terminal, " , TE_NAME As Terminal, TE_TERMINAL_ID AS TERMINAL_ID ", "") & _
              " FROM HANDPAYS " & _
              "   LEFT OUTER JOIN ACCOUNT_MOVEMENTS " & _
              "     ON HP_MOVEMENT_ID = AM_MOVEMENT_ID " & _
              "   LEFT OUTER JOIN TERMINALS " & _
              "     ON HP_TERMINAL_ID = TE_TERMINAL_ID " & _
              " WHERE AM_ACCOUNT_ID IS NULL " & _
              "   AND HP_TYPE <> " & WSI.Common.HANDPAY_TYPE.SPECIAL_PROGRESSIVE & " "

    ' Filter Providers - Terminals
    _str_sql = _str_sql & " AND HP_TERMINAL_ID IN " & Me.uc_pr_list.GetProviderIdListSelected()

    _date_filter = Me.uc_dsl.GetSqlFilterCondition("HP_DATETIME")

    If _date_filter.Length > 0 Then
      _str_sql = _str_sql & " AND " & _date_filter
    End If

    _str_sql = _str_sql & " GROUP BY TE_PROVIDER_ID " & IIf(m_by_terminal, " , TE_NAME, TE_TERMINAL_ID ", "")

    Return _str_sql

  End Function ' GetSqlQueryHandpaysNotTransfered


  ' PURPOSE: Build the SQL query to obtain the relevant data from database
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '     
  Private Function GetSqlQueryHandpaysManual() As String

    Dim _str_sql As String
    Dim _date_filter As String
    Dim _manual_handpays_filter As String

    _manual_handpays_filter = " HP_TYPE IN (" & WSI.Common.HANDPAY_TYPE.MANUAL & ", " & _
                                                WSI.Common.HANDPAY_TYPE.MANUAL_CANCELLED_CREDITS & ", " & _
                                                WSI.Common.HANDPAY_TYPE.MANUAL_JACKPOT & ", " & _
                                                WSI.Common.HANDPAY_TYPE.MANUAL_OTHERS & ") "

    ' Get Select and from
    ' RCI 26-OCT-2011: Group by current provider (TE_PROVIDER_ID) not the provider
    '                  when the handpay was entered (HP_TE_PROVIDER_ID).
    _str_sql = "SELECT TE_PROVIDER_ID AS Provider " & _
              " , sum ( HP_AMOUNT )" & _
              IIf(m_by_terminal, " , TE_NAME As Terminal, TE_TERMINAL_ID AS TERMINAL_ID ", "") & _
              " FROM HANDPAYS " & _
              "   LEFT OUTER JOIN TERMINALS " & _
              "     ON HP_TERMINAL_ID = TE_TERMINAL_ID " & _
              " WHERE " & _manual_handpays_filter

    ' Filter Providers - Terminals
    _str_sql = _str_sql & " AND HP_TERMINAL_ID IN " & Me.uc_pr_list.GetProviderIdListSelected()

    _date_filter = Me.uc_dsl.GetSqlFilterCondition("HP_DATETIME")

    If _date_filter.Length > 0 Then
      _str_sql = _str_sql & " AND " & _date_filter
    End If

    _str_sql = _str_sql & " GROUP BY TE_PROVIDER_ID " & IIf(m_by_terminal, " , TE_NAME, TE_TERMINAL_ID ", "")


    Return _str_sql

  End Function ' GetSqlQueryHandpays

  Private Function GetSqlQueryProviderList() As String

    Dim str_sql As String

    ' Get Select and from
    str_sql = " SELECT TE_PROVIDER_ID AS Provider " & _
              "   FROM TERMINALS " & _
              "  WHERE TE_TYPE = 1 "

    ' Filter Terminal
    str_sql = str_sql & " AND TE_TERMINAL_ID IN " & Me.uc_pr_list.GetProviderIdListSelected()
    str_sql = str_sql & " GROUP BY TE_PROVIDER_ID "

    Return str_sql

  End Function



  ' PURPOSE: Build the SQL query to obtain the relevant data from database
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '     
  Private Function GetSqlQueryProviderStatistics() As String
    Dim _str_sql As String
    Dim _date_filter As String

    _str_sql = "SELECT Provider, " & _
                "  sum(PlayedAmount-(WonAmount +  ProgressiveProvisionAmount - ProgressiveJackpotAmount0)) as Netwin " & _
                IIf(m_by_terminal, " , Terminal, Terminal_Id ", "") & _
                " FROM " & _
                " (SELECT te_provider_id AS Provider, " & _
                " sum(SPH_PLAYED_AMOUNT)as PlayedAmount, " & _
                " sum(SPH_WON_AMOUNT) AS WonAmount, " & _
                " sum(ISNULL(SPH_PROGRESSIVE_PROVISION_AMOUNT,0)) AS ProgressiveProvisionAmount, " & _
                " sum(ISNULL(SPH_PROGRESSIVE_JACKPOT_AMOUNT_0,0)) AS ProgressiveJackpotAmount0, " & _
                " te_name AS Terminal, " & _
                " sph_terminal_id AS Terminal_Id " & _
                " FROM SALES_PER_HOUR_V2 WITH (INDEX(PK_sales_per_hour)), terminals " & _
                " WHERE te_terminal_id = SALES_PER_HOUR_V2.SPH_terminal_id "

    ' Filter Providers - Terminals
    _str_sql = _str_sql & " AND SPH_terminal_id IN " & Me.uc_pr_list.GetProviderIdListSelected()

    _date_filter = Me.uc_dsl.GetSqlFilterCondition("SPH_BASE_HOUR")

    If _date_filter.Length > 0 Then
      _str_sql = _str_sql & " AND " & _date_filter
    End If

    _str_sql = _str_sql & " GROUP BY sph_terminal_id, terminals.te_provider_id, terminals.te_name) x GROUP BY Provider " & IIf(m_by_terminal, " , Terminal, Terminal_Id ", "") & " ORDER BY Provider " & IIf(m_by_terminal, " , Terminal ", "")

    Return _str_sql

  End Function


  ' PURPOSE: Build the SQL query to obtain the relevant data from database
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '     
  Private Function GetSqlQueryProviderActivity() As String

    Dim _str_sql As String
    Dim _date_filter As String

    ' Get Select and from
    _str_sql = " SELECT Provider, SUM(played - won) as Netwin " & _
               IIf(m_by_terminal, " , Terminal, Terminal_id ", "") & _
              " FROM (SELECT SUM(ps_total_cash_in) AS played " & _
              "            , SUM(ps_total_cash_out) AS won " & _
              "            , te_provider_id AS Provider " & _
              "            , te_name AS Terminal, PS_terminal_id AS TERMINAL_ID " & _
              "        FROM PLAY_SESSIONS WITH (INDEX(IX_ps_finished)), terminals   " & _
              "       WHERE te_terminal_id = PLAY_SESSIONS.PS_terminal_id "

    ' Filter Providers - Terminals
    _str_sql = _str_sql & " AND PS_terminal_id IN " & Me.uc_pr_list.GetProviderIdListSelected()

    _date_filter = Me.uc_dsl.GetSqlFilterCondition("PS_FINISHED")

    If _date_filter.Length > 0 Then
      _str_sql = _str_sql & " AND " & _date_filter
    End If

    'XGJ 28-FEB-2017
    If (WSI.Common.Misc.IsTerminalDrawEnabled()) Then
      _str_sql = _str_sql & "         AND PS_STATUS NOT IN (0,10,11)  " & _
                            "         AND PS_PROMO = 0                " & _
                            "         AND PS_TYPE <> 201              "
    Else

      _str_sql = _str_sql & "         AND PS_STATUS <> 0 " & _
                            "         AND PS_PROMO = 0 "
    End If

    _str_sql = _str_sql & "GROUP BY PS_terminal_id, terminals.te_provider_id, terminals.te_name) AS x "
    _str_sql = _str_sql & "GROUP BY Provider " & IIf(m_by_terminal, " , Terminal, Terminal_id ", "")

    Return _str_sql

  End Function


  ' PURPOSE: Format appearance of the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     

  Private Sub GUI_StyleSheet()

    'Dim fore_color As Color
    'Dim back_color As Color
    Dim _terminal_columns As List(Of ColumnSettings)

    Call GridColumnReIndex()

    With Me.dg_list
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, False)

      .Button(BUTTON_TYPE_ADD).Visible = False
      .Button(BUTTON_TYPE_DELETE).Visible = False

      .Sortable = True

      'GetUpdateStatusColor(0, fore_color, back_color)
      '.Counter(COUNTER_PROVIDER_OK).Visible = True
      '.Counter(COUNTER_PROVIDER_OK).BackColor = back_color
      '.Counter(COUNTER_PROVIDER_OK).ForeColor = fore_color

      'GetUpdateStatusColor(1, fore_color, back_color)
      '.Counter(COUNTER_PROVIDER_GAP).Visible = True
      '.Counter(COUNTER_PROVIDER_GAP).BackColor = back_color
      '.Counter(COUNTER_PROVIDER_GAP).ForeColor = fore_color

      ' Sentinel
      .Column(GRID_COLUMN_SENTINEL).Header(0).Text = ""
      .Column(GRID_COLUMN_SENTINEL).Header(1).Text = ""
      .Column(GRID_COLUMN_SENTINEL).Width = 200
      .Column(GRID_COLUMN_SENTINEL).HighLightWhenSelected = False
      ' RCI 24-JAN-2012: Hide SENTINEL column from the reports.
      .Column(GRID_COLUMN_SENTINEL).IsColumnPrintable = False

      '  Terminal Report
      _terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)
      For _idx As Int32 = 0 To Math.Min(_terminal_columns.Count - 1, TERMINAL_DATA_COLUMNS - 1)
        '.Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = _terminal_columns(_idx).Header0
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = ""
        If m_by_terminal Then
          .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = _terminal_columns(_idx).Width
        Else
          .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = IIf(_idx > 0, 0, _terminal_columns(_idx).Width)
        End If
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = _terminal_columns(_idx).Alignment
        If _terminal_columns(_idx).Column = ReportColumn.Provider Then
          GRID_COLUMN_PROVIDER = GRID_INIT_TERMINAL_DATA + _idx
        End If
        If _terminal_columns(_idx).Column = ReportColumn.Terminal Then
          GRID_COLUMN_TERMINAL = GRID_INIT_TERMINAL_DATA + _idx
        End If
      Next

      ' Terminal_Id
      .Column(GRID_COLUMN_TERMINAL_ID).Header(1).Text = ""
      If m_by_terminal Then
        .Column(GRID_COLUMN_TERMINAL_ID).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(300)
        .Column(GRID_COLUMN_TERMINAL_ID).Width = GRID_WIDTH_TERMINAL_ID
      Else
        ' RCI 24-JAN-2012: Avoid the strange character in header(0) when TERMINAL column is hidden.
        .Column(GRID_COLUMN_TERMINAL_ID).Header(0).Text = ""
        .Column(GRID_COLUMN_TERMINAL_ID).Width = 0
      End If
      .Column(GRID_COLUMN_TERMINAL_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' NW Statistics
      .Column(GRID_COLUMN_STATISTICS_NW).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(334)
      .Column(GRID_COLUMN_STATISTICS_NW).Header(1).Text = "A"
      .Column(GRID_COLUMN_STATISTICS_NW).Width = GRID_WIDTH_AMOUNT_SHORT
      .Column(GRID_COLUMN_STATISTICS_NW).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' NW Providers Activity
      .Column(GRID_COLUMN_PROVIDER_NW).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(335)
      .Column(GRID_COLUMN_PROVIDER_NW).Header(1).Text = "B"
      .Column(GRID_COLUMN_PROVIDER_NW).Width = GRID_WIDTH_AMOUNT_SHORT
      .Column(GRID_COLUMN_PROVIDER_NW).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' NW Current Playsessions
      .Column(GRID_COLUMN_PLAYSESSION_NW).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(345)
      .Column(GRID_COLUMN_PLAYSESSION_NW).Header(1).Text = "C"
      .Column(GRID_COLUMN_PLAYSESSION_NW).Width = GRID_WIDTH_AMOUNT_SHORT
      .Column(GRID_COLUMN_PLAYSESSION_NW).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' HP Not transfered
      .Column(GRID_COLUMN_HP_NOT_TRANSFERED).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(336)
      .Column(GRID_COLUMN_HP_NOT_TRANSFERED).Header(1).Text = "D"
      .Column(GRID_COLUMN_HP_NOT_TRANSFERED).Width = GRID_WIDTH_AMOUNT_SHORT
      .Column(GRID_COLUMN_HP_NOT_TRANSFERED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' HP Manual
      .Column(GRID_COLUMN_HP_MANUAL).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(337)
      .Column(GRID_COLUMN_HP_MANUAL).Header(1).Text = "E"
      .Column(GRID_COLUMN_HP_MANUAL).Width = GRID_WIDTH_AMOUNT_SHORT
      .Column(GRID_COLUMN_HP_MANUAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Gap
      .Column(GRID_COLUMN_GAP).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(322)
      .Column(GRID_COLUMN_GAP).Header(1).Text = "(A-B-C+D-E)"
      .Column(GRID_COLUMN_GAP).Width = GRID_WIDTH_AMOUNT_LONG
      .Column(GRID_COLUMN_GAP).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

  End Sub 'GUI_StyleSheet

  ' PURPOSE: Format appearance of the data grid for cashier
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     

  Private Sub GUI_StyleSheetCashier()
    Dim _width_total As Integer

    With Me.dg_list_cashier
      Call .Init(GRID_CASHIER_COLUMNS, GRID_CASHIER_HEADER_ROWS, False)

      .Button(BUTTON_TYPE_ADD).Visible = False
      .Button(BUTTON_TYPE_DELETE).Visible = False

      .Sortable = True

      ' Sentinel
      .Column(GRID_COLUMN_SENTINEL).Header.Text = ""
      .Column(GRID_COLUMN_SENTINEL).Width = 200
      .Column(GRID_COLUMN_SENTINEL).HighLightWhenSelected = False
      ' RCI 24-JAN-2012: Hide SENTINEL column from the reports.
      .Column(GRID_COLUMN_SENTINEL).IsColumnPrintable = False

      ' Letter
      _width_total = 2100
      If m_gaming_table_enabled Then
        _width_total = 2400
      End If

      .Column(GRID_COLUMN_LETTER).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_LETTER).Header.Text = ""
      .Column(GRID_COLUMN_LETTER).Width = _width_total
      .Column(GRID_COLUMN_LETTER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Item
      .Column(GRID_COLUMN_CONCEPT).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_CONCEPT).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1404)
      .Column(GRID_COLUMN_CONCEPT).Width = GRID_WIDTH_CONCEPT
      .Column(GRID_COLUMN_CONCEPT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Value
      .Column(GRID_COLUMN_VALUE).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_VALUE).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1405)
      .Column(GRID_COLUMN_VALUE).Width = GRID_WIDTH_AMOUNT_SHORT
      .Column(GRID_COLUMN_VALUE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

  End Sub 'GUI_StyleSheet

  ' PURPOSE: Get color by SERVICE status
  '
  '  PARAMS:
  '     - INPUT:
  '           - Status:  ENUM_SERVICE_STATUS
  '     - OUTPUT:
  '           - ForeColor: Foreground color
  '           - BackColor: Background color
  '
  ' RETURNS:
  '     - 
  Private Sub GetUpdateStatusColor(ByVal Status As Double, _
                                    ByRef ForeColor As Color, _
                                    ByRef BackColor As Color)
    If Not m_by_terminal And Not m_by_provider Then
      If Status = 0 Then
        ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
        BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
      Else
        ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
        BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_RED_02)
      End If
    Else
      ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
      BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
    End If

  End Sub 'GetUpdateStatusColor


  Private Sub CopyTabs(ByVal DLSFrom As uc_daily_session_selector, ByVal DLSTo As uc_daily_session_selector)

    DLSTo.ToDate = DLSFrom.ToDate
    DLSTo.ToDateSelected = DLSFrom.ToDateSelected
    DLSTo.FromDate = DLSFrom.FromDate
    DLSTo.FromDateSelected = DLSFrom.FromDateSelected
    DLSTo.ClosingTime = DLSFrom.ClosingTime

  End Sub

  Private Sub excel_click_action()
    Windows.Forms.Cursor.Current = Cursors.WaitCursor
    Cursor = Cursors.WaitCursor

    Call GUI_ExcelResultList()

    Cursor = Cursors.Default
    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub print_click_action()
    Windows.Forms.Cursor.Current = Cursors.WaitCursor
    Cursor = Cursors.WaitCursor

    '''Call GUI_PrintResultList()   ' CrystalReports
    Call GUI_ExcelResultList(True)  ' SpreadSheetGear2012

    Cursor = Cursors.Default
    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub filter_apply_action()
    ' JAB 03-MAY-2012: Check the date filter
    If Not GUI_FilterCheck(tb_control.SelectedIndex) Then
      Return
    End If

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    Try
      Me.dg_list.Clear()
      Me.dg_list_cashier.Clear()

      If Me.tb_control.SelectedTab Is TabPage1 Then
        CopyTabs(uc_dsl, uc_dsl_cashier)
      ElseIf Me.tb_control.SelectedTab Is TabPage2 Then
        CopyTabs(uc_dsl_cashier, uc_dsl)
      End If

      Me.RefreshGrid()
      Me.RefreshGridCashier()

      If tb_control.SelectedIndex = 0 Then
        btn_excel.Enabled = CBool(dg_list.NumRows > 0) And CurrentUser.Permissions(ENUM_FORM.FORM_EXCEL_ENABLED).Read
        btn_print.Enabled = CBool(dg_list.NumRows > 0) And CurrentUser.Permissions(ENUM_FORM.FORM_PRINT_ENABLED).Read
      ElseIf tb_control.SelectedIndex = 1 Then
        btn_excel.Enabled = CBool(dg_list_cashier.NumRows > 0) And CurrentUser.Permissions(ENUM_FORM.FORM_EXCEL_ENABLED).Read
        btn_print.Enabled = CBool(dg_list_cashier.NumRows > 0) And CurrentUser.Permissions(ENUM_FORM.FORM_PRINT_ENABLED).Read
      End If
      btn_select.Enabled = GetEnabledStatusButtonSelect()

      m_print_datetime = frm_base_sel.GUI_GetPrintDateTime()
      GUI_ReportUpdateFilters()

      'If Me.tb_control.SelectedTab Is TabPage1 Then
      '  Me.dg_list.Clear()
      '  Me.RefreshGrid()
      'ElseIf Me.tb_control.SelectedTab Is TabPage2 Then
      '  Me.dg_list_cashier.Clear()
      '  Me.RefreshGridCashier()
      'End If

    Finally
      Windows.Forms.Cursor.Current = Cursors.Default
    End Try


  End Sub

  Private Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)
    Dim _print_data As GUI_Reports.CLASS_PRINT_DATA

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_FILTER_APPLY
        Call filter_apply_action()

      Case ENUM_BUTTON.BUTTON_EXCEL
        Call excel_click_action()

      Case ENUM_BUTTON.BUTTON_PRINT
        Call print_click_action()

      Case Else
        '
    End Select

    If GUI_HasToBeAudited(ButtonId) Then
      _print_data = New GUI_Reports.CLASS_PRINT_DATA
      Call Me.GUI_ReportFilter(_print_data)
      Call MyBase.AuditFormClick(ButtonId, _print_data)
    End If
  End Sub

  Private Sub GridColumnReIndex()
    If m_by_terminal Then
      TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(m_terminal_report_type)
    Else
      TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(ReportType.Provider)
    End If

    GRID_COLUMN_SENTINEL = 0
    GRID_INIT_TERMINAL_DATA = 1
    GRID_COLUMN_PROVIDER = GRID_INIT_TERMINAL_DATA
    GRID_COLUMN_TERMINAL = GRID_INIT_TERMINAL_DATA + 1
    GRID_COLUMN_TERMINAL_ID = TERMINAL_DATA_COLUMNS + 1
    GRID_COLUMN_STATISTICS_NW = TERMINAL_DATA_COLUMNS + 2
    GRID_COLUMN_PROVIDER_NW = TERMINAL_DATA_COLUMNS + 3
    GRID_COLUMN_PLAYSESSION_NW = TERMINAL_DATA_COLUMNS + 4
    GRID_COLUMN_HP_NOT_TRANSFERED = TERMINAL_DATA_COLUMNS + 5
    GRID_COLUMN_HP_MANUAL = TERMINAL_DATA_COLUMNS + 6
    GRID_COLUMN_GAP = TERMINAL_DATA_COLUMNS + 7

    GRID_COLUMNS = TERMINAL_DATA_COLUMNS + 8
  End Sub

  ''' <summary>
  ''' Show Statistics Manual Adjust Form
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub ShowStatisticsManualAdjust()
    If dg_list.CurrentRow < 0 Or dg_list.NumRows < 0 Then
      Return
    End If

    If String.IsNullOrEmpty(dg_list.Cell(dg_list.CurrentRow, GRID_COLUMN_TERMINAL_ID).Value) Then
      Return
    End If
    ' Only when search is: by terminal with mismatch, then the GRID_COLUMN_TERMINAL_ID has value
    Form_Statics_Adjust()
  End Sub

  ''' <summary>
  ''' Get Status Enabled for Select button
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetEnabledStatusButtonSelect() As Boolean
    If dg_list.CurrentRow >= 0 AndAlso _
       Not String.IsNullOrEmpty(dg_list.Cell(dg_list.CurrentRow, GRID_COLUMN_TERMINAL_ID).Value) AndAlso _
       Me.tb_control.SelectedTab Is TabPage1 Then

      Return True
    Else
      Return False
    End If
  End Function
#End Region ' Private Functions

#Region "Events"

  ' PURPOSE: Execute proper actions when user presses the 'Exit' button
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     

  Private Sub btn_cancel_ClickEvent() Handles btn_exit.ClickEvent
    Me.Close()
  End Sub

  Private Sub frm_statistics_providers_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
    Dim handled As Boolean

    handled = False
    If Me.dg_list.ContainsFocus Then
      handled = Me.dg_list.KeyPressed(sender, e)
    End If

    If Me.dg_list_cashier.ContainsFocus Then
      handled = Me.dg_list_cashier.KeyPressed(sender, e)
    End If

    If Not handled Then
      Select Case e.KeyChar
        Case Chr(13)
          ' ACC 18-JUN-2002 The select event is done in uc_grid control
          If Not Me.dg_list.ContainsFocus Then
            Call btn_filter_apply_ClickEvent()
          End If

        Case Chr(27)
          Call btn_cancel_ClickEvent()
      End Select

    End If

  End Sub

  Private Sub btn_filter_apply_ClickEvent() Handles btn_filter_apply.ClickEvent

    Call GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)

    '' JAB 03-MAY-2012: Check the date filter
    'If Not GUI_FilterCheck(tb_control.SelectedIndex) Then
    '  Return
    'End If

    'Windows.Forms.Cursor.Current = Cursors.WaitCursor

    'Try
    '  Me.dg_list.Clear()
    '  Me.dg_list_cashier.Clear()

    '  If Me.tb_control.SelectedTab Is TabPage1 Then
    '    CopyTabs(uc_dsl, uc_dsl_cashier)
    '  ElseIf Me.tb_control.SelectedTab Is TabPage2 Then
    '    CopyTabs(uc_dsl_cashier, uc_dsl)
    '  End If

    '  Me.RefreshGrid()
    '  Me.RefreshGridCashier()

    '  If tb_control.SelectedIndex = 0 Then
    '    btn_excel.Enabled = CBool(dg_list.NumRows > 0)
    '    btn_print.Enabled = CBool(dg_list.NumRows > 0)
    '  ElseIf tb_control.SelectedIndex = 1 Then
    '    btn_excel.Enabled = CBool(dg_list_cashier.NumRows > 0)
    '    btn_print.Enabled = CBool(dg_list_cashier.NumRows > 0)
    '  End If

    '  m_print_datetime = frm_base_sel.GUI_GetPrintDateTime()
    '  GUI_ReportUpdateFilters()

    '  'If Me.tb_control.SelectedTab Is TabPage1 Then
    '  '  Me.dg_list.Clear()
    '  '  Me.RefreshGrid()
    '  'ElseIf Me.tb_control.SelectedTab Is TabPage2 Then
    '  '  Me.dg_list_cashier.Clear()
    '  '  Me.RefreshGridCashier()
    '  'End If

    'Finally
    '  Windows.Forms.Cursor.Current = Cursors.Default
    'End Try

  End Sub

  Private Sub btn_filter_reset_ClickEvent() Handles btn_filter_reset.ClickEvent

    ' Set filter default values
    Call SetDefaultValues()

  End Sub


  Private Sub tb_control_Selected(ByVal sender As Object, ByVal e As System.Windows.Forms.TabControlEventArgs) Handles tb_control.Selected

    Dim _tab As TabPage

    _tab = e.TabPage

    If _tab Is TabPage1 Then

      CopyTabs(uc_dsl_cashier, uc_dsl)

      btn_excel.Enabled = CBool(dg_list.NumRows > 0)
      btn_print.Enabled = CBool(dg_list.NumRows > 0)

    ElseIf _tab Is TabPage2 Then

      CopyTabs(uc_dsl, uc_dsl_cashier)

      btn_excel.Enabled = CBool(dg_list_cashier.NumRows > 0)
      btn_print.Enabled = CBool(dg_list_cashier.NumRows > 0)

    End If

    btn_select.Enabled = GetEnabledStatusButtonSelect()
  End Sub

  Private Sub btn_print_ClickEvent() Handles btn_print.ClickEvent
    Call GUI_ButtonClick(ENUM_BUTTON.BUTTON_PRINT)
    'Windows.Forms.Cursor.Current = Cursors.WaitCursor
    'Cursor = Cursors.WaitCursor

    ''''Call GUI_PrintResultList()   ' CrystalReports
    'Call GUI_ExcelResultList(True)  ' SpreadSheetGear2012

    'Cursor = Cursors.Default
    'Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub btn_excel_ClickEvent() Handles btn_excel.ClickEvent

    Call GUI_ButtonClick(ENUM_BUTTON.BUTTON_EXCEL)
    'Windows.Forms.Cursor.Current = Cursors.WaitCursor
    'Cursor = Cursors.WaitCursor

    'Call GUI_ExcelResultList()

    'Cursor = Cursors.Default
    'Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub dg_list_DataSelectedEvent() Handles dg_list.DataSelectedEvent
    Call ShowStatisticsManualAdjust()
  End Sub

  Private Sub opt_by_terminal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_by_terminal.CheckedChanged
    chk_terminal_location.Enabled = opt_by_terminal.Checked
    uc_pr_list.BlockTerminals(Not opt_by_terminal.Checked)
  End Sub

  Private Sub chk_terminal_location_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminal_location.CheckedChanged
    If chk_terminal_location.Checked Then
      m_terminal_report_type = ReportType.Provider + ReportType.Location
    Else
      m_terminal_report_type = ReportType.Provider
    End If

    m_refresh_grid = True
  End Sub

  Private Sub btn_select_ClickEvent() Handles btn_select.ClickEvent
    Call ShowStatisticsManualAdjust()
  End Sub

  Private Sub dg_list_RowSelectedEvent(SelectedRow As Integer) Handles dg_list.RowSelectedEvent
    btn_select.Enabled = GetEnabledStatusButtonSelect()
  End Sub
#End Region ' Events

#Region "Public Functions"

  ' PURPOSE: Entry point for the screen
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    Me.MdiParent = MdiParent

    Call Me.Display(False)

  End Sub

#End Region ' Public Functions

  Private Sub GUI_PrintResultList()

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    If Not IsNothing(m_print_data) Then
      m_print_data = Nothing

    End If

    m_print_data = New GUI_Reports.CLASS_PRINT_DATA()

    'Call GUI_ReportUpdateFilters()

    Call GUI_ReportFilter(m_print_data)

    Call frm_base_sel.GUI_ReportDataX(IIf(tb_control.SelectedIndex = 0, dg_list, dg_list_cashier), m_print_data)

    Call frm_base_sel.GUI_ReportParamsX(IIf(tb_control.SelectedIndex = 0, dg_list, dg_list_cashier), m_print_data, _
                                        IIf(tb_control.SelectedIndex = 0, GLB_NLS_GUI_STATISTICS.GetString(331), GLB_NLS_GUI_STATISTICS.GetString(332)), _
                                        m_print_datetime)

    'Call GUI_ReportFooter(m_print_data)

    m_print_data.Preview()

  End Sub

  ' Called when row is selected
  Private Sub Form_Statics_Adjust()
    Dim _date_from As Date
    Dim _date_to As Date
    Dim _terminal_id As Integer
    Dim _frm_stats_adjust As frm_stats_manual_adjustment
    Dim _today_opening As Date

    _terminal_id = dg_list.Cell(dg_list.CurrentRow, GRID_COLUMN_TERMINAL_ID).Value

    ' Terminal Id can't be Id = 0
    If _terminal_id <= 0 Then
      Return
    End If

    _today_opening = WSI.Common.Misc.TodayOpening()

    If String.IsNullOrEmpty(m_date_from) Then
      _date_from = New Date(2007, 1, 1, _today_opening.Hour, _today_opening.Minute, 0)
    Else
      _date_from = CDate(m_date_from)

    End If
    If String.IsNullOrEmpty(m_date_to) Then
      _date_to = _today_opening.AddDays(1)
    Else
      _date_to = CDate(m_date_to)
    End If


    _frm_stats_adjust = New frm_stats_manual_adjustment()

    _frm_stats_adjust.ShowForEdit(Me.MdiParent, _terminal_id, _date_from, _date_to)

  End Sub

  Private Sub GUI_ExcelResultList(Optional ByVal OnlyPreview As Boolean = False)

    Try

      If Not IsNothing(m_excel_data) Then
        m_excel_data = Nothing

      End If

      m_excel_data = New GUI_Reports.CLASS_EXCEL_DATA(OnlyPreview, OnlyPreview)

      If m_excel_data.IsNull() Then
        Return
      End If

      If Not m_excel_data.Cancelled Then
        frm_base_sel.GUI_ReportParamsX(IIf(tb_control.SelectedIndex = 0, dg_list, dg_list_cashier), m_excel_data, _
                                       IIf(tb_control.SelectedIndex = 0, GLB_NLS_GUI_STATISTICS.GetString(331), GLB_NLS_GUI_STATISTICS.GetString(332)), _
                                       m_print_datetime)
      End If

      If Not m_excel_data.Cancelled Then

        ' Get ReportFilters from PrintData
        If Not IsNothing(m_print_data) Then
          m_print_data = Nothing
        End If

        m_print_data = New GUI_Reports.CLASS_PRINT_DATA()

        'Call GUI_ReportUpdateFilters()

        Call GUI_ReportFilter(m_print_data)

        ' Insert filter data
        m_excel_data.UpdateReportFilters(m_print_data.Filter.ItemArray)

        If tb_control.SelectedIndex = 0 Then
          m_excel_data.SetColumnFormat(GRID_COLUMN_PROVIDER, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)
        End If
        
      End If

      If Not m_excel_data.Cancelled Then
        frm_base_sel.GUI_ReportDataX(IIf(tb_control.SelectedIndex = 0, dg_list, dg_list_cashier), m_excel_data)
      End If

      If Not m_excel_data.Cancelled Then
        'GUI_ReportFooter(m_excel_data)
      End If

      If Not m_excel_data.Cancelled Then
        Call m_excel_data.Preview(OnlyPreview)
      End If

    Catch ex As Exception

      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(146), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            GLB_ApplicationName, _
                            "frm_base_sel.GUI_ExcelResultList()", _
                            "", _
                            "", _
                            "", _
                           "Error in Excel data export.")
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            GLB_ApplicationName, _
                            "Exception", _
                            "", _
                            "", _
                            "", _
                            ex.Message)
    Finally
      If m_excel_data Is Nothing Then
      Else
        m_excel_data.Dispose()
        m_excel_data = Nothing
      End If

    End Try

  End Sub

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Sub GUI_ReportFilter(ByRef PrintData As GUI_Reports.CLASS_PRINT_DATA)

    If tb_control.SelectedIndex = 0 Then

      PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_STATISTICS.GetString(309), m_date_from)
      PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_STATISTICS.GetString(310), m_date_to)
      PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(471), m_type)

      If (m_type = GLB_NLS_GUI_STATISTICS.GetString(383)) Then
        PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7185), IIf(m_terminal_location, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)))
      End If

      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7187), IIf(m_only_unbalance, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)))



      PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_terminal)


      PrintData.FilterValueWidth(1) = 3000
      PrintData.FilterValueWidth(2) = 3000

    ElseIf tb_control.SelectedIndex = 1 Then

      PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_STATISTICS.GetString(309), m_date_from)
      PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_STATISTICS.GetString(310), m_date_to)

      PrintData.FilterValueWidth(1) = 3000

    End If

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Sub GUI_ReportParams(ByRef PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    'Call GUI_ReportUpdateFilters()

    'Call MyBase.GUI_ReportParams(PrintData)
    'frm_base_sel.GUI_ReportParamsX(dg_list, m_print_data, Me.Text)

    PrintData.Params.Title = GLB_NLS_GUI_PLAYER_TRACKING.GetString(211)

    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Sub GUI_ReportUpdateFilters()

    m_type = ""
    m_terminal = ""
    m_date_from = ""
    m_date_to = ""

    If tb_control.SelectedIndex = 0 Then

      ' Date 
      If Me.uc_dsl.FromDateSelected Then
        m_date_from = GUI_FormatDate(Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime), _
                                     ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                     ENUM_FORMAT_TIME.FORMAT_HHMM)
      End If

      If Me.uc_dsl.ToDateSelected Then
        m_date_to = GUI_FormatDate(Me.uc_dsl.ToDate.AddHours(Me.uc_dsl.ClosingTime), _
                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ENUM_FORMAT_TIME.FORMAT_HHMM)
      End If

      ' Providers - Terminals
      m_terminal = Me.uc_pr_list.GetTerminalReportText()

      'If opt_all_providers.Checked = True Then
      'm_type = GLB_NLS_GUI_STATISTICS.GetString(338)
      'Else
      If opt_by_provider.Checked = True Then
        m_type = GLB_NLS_GUI_STATISTICS.GetString(446)
      ElseIf opt_by_terminal.Checked = True Then
        m_type = GLB_NLS_GUI_STATISTICS.GetString(383)
      Else

      End If

    ElseIf tb_control.SelectedIndex = 1 Then

      ' Date 
      If Me.uc_dsl_cashier.FromDateSelected Then
        m_date_from = GUI_FormatDate(Me.uc_dsl_cashier.FromDate.AddHours(Me.uc_dsl_cashier.ClosingTime), _
                                     ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                     ENUM_FORMAT_TIME.FORMAT_HHMM)
      End If

      If Me.uc_dsl_cashier.ToDateSelected Then
        m_date_to = GUI_FormatDate(Me.uc_dsl_cashier.ToDate.AddHours(Me.uc_dsl_cashier.ClosingTime), _
                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ENUM_FORMAT_TIME.FORMAT_HHMM)
      End If

    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

End Class '