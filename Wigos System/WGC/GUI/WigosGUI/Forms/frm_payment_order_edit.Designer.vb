<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_payment_order_edit
    Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_payment_order_edit))
    Me.tab_order_viewer = New System.Windows.Forms.TabControl()
    Me.TabPage1 = New System.Windows.Forms.TabPage()
    Me.gb_room_info = New System.Windows.Forms.GroupBox()
    Me.ef_name2 = New GUI_Controls.uc_entry_field()
    Me.ef_name1 = New GUI_Controls.uc_entry_field()
    Me.ef_title2 = New GUI_Controls.uc_entry_field()
    Me.ef_title1 = New GUI_Controls.uc_entry_field()
    Me.ef_check_payment = New GUI_Controls.uc_entry_field()
    Me.ef_cash_payment = New GUI_Controls.uc_entry_field()
    Me.ef_full_payment = New GUI_Controls.uc_entry_field()
    Me.ef_order_date = New GUI_Controls.uc_entry_field()
    Me.ef_witholding_tax1 = New GUI_Controls.uc_entry_field()
    Me.ef_witholding_tax2 = New GUI_Controls.uc_entry_field()
    Me.ef_witholding_tax3 = New GUI_Controls.uc_entry_field()
    Me.ef_prize = New GUI_Controls.uc_entry_field()
    Me.ef_return_balance = New GUI_Controls.uc_entry_field()
    Me.gb_bank_info = New System.Windows.Forms.GroupBox()
    Me.cb_bank_sel = New GUI_Controls.uc_combo()
    Me.dtp_application_date = New GUI_Controls.uc_date_picker()
    Me.ef_account_number = New GUI_Controls.uc_entry_field()
    Me.ef_customer_number = New GUI_Controls.uc_entry_field()
    Me.ef_efective_days = New GUI_Controls.uc_entry_field()
    Me.ef_payment_type = New GUI_Controls.uc_entry_field()
    Me.ef_bank_name = New GUI_Controls.uc_entry_field()
    Me.gb_player_info = New System.Windows.Forms.GroupBox()
    Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
    Me.ef_player_doc_id2 = New GUI_Controls.uc_entry_field()
    Me.ef_player_name3 = New GUI_Controls.uc_entry_field()
    Me.ef_player_doc_id1 = New GUI_Controls.uc_entry_field()
    Me.ef_player_name4 = New GUI_Controls.uc_entry_field()
    Me.cb_player_doc_type2 = New GUI_Controls.uc_combo()
    Me.cb_player_doc_type1 = New GUI_Controls.uc_combo()
    Me.ef_player_name1 = New GUI_Controls.uc_entry_field()
    Me.ef_player_name2 = New GUI_Controls.uc_entry_field()
    Me.ef_player_doc_type1 = New GUI_Controls.uc_entry_field()
    Me.ef_player_doc_type2 = New GUI_Controls.uc_entry_field()
    Me.panel_data.SuspendLayout()
    Me.tab_order_viewer.SuspendLayout()
    Me.TabPage1.SuspendLayout()
    Me.gb_room_info.SuspendLayout()
    Me.gb_bank_info.SuspendLayout()
    Me.gb_player_info.SuspendLayout()
    Me.TableLayoutPanel1.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.tab_order_viewer)
    Me.panel_data.Size = New System.Drawing.Size(859, 618)
    '
    'tab_order_viewer
    '
    Me.tab_order_viewer.Controls.Add(Me.TabPage1)
    Me.tab_order_viewer.Dock = System.Windows.Forms.DockStyle.Fill
    Me.tab_order_viewer.Location = New System.Drawing.Point(0, 0)
    Me.tab_order_viewer.Name = "tab_order_viewer"
    Me.tab_order_viewer.SelectedIndex = 0
    Me.tab_order_viewer.Size = New System.Drawing.Size(859, 618)
    Me.tab_order_viewer.TabIndex = 0
    '
    'TabPage1
    '
    Me.TabPage1.Controls.Add(Me.gb_room_info)
    Me.TabPage1.Controls.Add(Me.gb_bank_info)
    Me.TabPage1.Controls.Add(Me.gb_player_info)
    Me.TabPage1.Location = New System.Drawing.Point(4, 22)
    Me.TabPage1.Name = "TabPage1"
    Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage1.Size = New System.Drawing.Size(851, 592)
    Me.TabPage1.TabIndex = 0
    Me.TabPage1.Text = "TabPage1"
    Me.TabPage1.UseVisualStyleBackColor = True
    '
    'gb_room_info
    '
    Me.gb_room_info.Controls.Add(Me.ef_name2)
    Me.gb_room_info.Controls.Add(Me.ef_name1)
    Me.gb_room_info.Controls.Add(Me.ef_title2)
    Me.gb_room_info.Controls.Add(Me.ef_title1)
    Me.gb_room_info.Controls.Add(Me.ef_check_payment)
    Me.gb_room_info.Controls.Add(Me.ef_cash_payment)
    Me.gb_room_info.Controls.Add(Me.ef_full_payment)
    Me.gb_room_info.Controls.Add(Me.ef_order_date)
    Me.gb_room_info.Controls.Add(Me.ef_witholding_tax1)
    Me.gb_room_info.Controls.Add(Me.ef_witholding_tax2)
    Me.gb_room_info.Controls.Add(Me.ef_witholding_tax3)
    Me.gb_room_info.Controls.Add(Me.ef_prize)
    Me.gb_room_info.Controls.Add(Me.ef_return_balance)
    Me.gb_room_info.Dock = System.Windows.Forms.DockStyle.Top
    Me.gb_room_info.Location = New System.Drawing.Point(3, 356)
    Me.gb_room_info.Name = "gb_room_info"
    Me.gb_room_info.Size = New System.Drawing.Size(845, 235)
    Me.gb_room_info.TabIndex = 2
    Me.gb_room_info.TabStop = False
    Me.gb_room_info.Text = "Datos de Pago"
    '
    'ef_name2
    '
    Me.ef_name2.DoubleValue = 0.0R
    Me.ef_name2.IntegerValue = 0
    Me.ef_name2.IsReadOnly = False
    Me.ef_name2.Location = New System.Drawing.Point(440, 172)
    Me.ef_name2.Name = "ef_name2"
    Me.ef_name2.OnlyUpperCase = True
    Me.ef_name2.PlaceHolder = Nothing
    Me.ef_name2.Size = New System.Drawing.Size(350, 24)
    Me.ef_name2.SufixText = "Sufix Text"
    Me.ef_name2.SufixTextVisible = True
    Me.ef_name2.TabIndex = 5
    Me.ef_name2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_name2.TextValue = ""
    Me.ef_name2.TextWidth = 150
    Me.ef_name2.Value = ""
    Me.ef_name2.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_name1
    '
    Me.ef_name1.DoubleValue = 0.0R
    Me.ef_name1.IntegerValue = 0
    Me.ef_name1.IsReadOnly = False
    Me.ef_name1.Location = New System.Drawing.Point(440, 110)
    Me.ef_name1.Name = "ef_name1"
    Me.ef_name1.OnlyUpperCase = True
    Me.ef_name1.PlaceHolder = Nothing
    Me.ef_name1.Size = New System.Drawing.Size(350, 24)
    Me.ef_name1.SufixText = "Sufix Text"
    Me.ef_name1.SufixTextVisible = True
    Me.ef_name1.TabIndex = 3
    Me.ef_name1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_name1.TextValue = ""
    Me.ef_name1.TextWidth = 150
    Me.ef_name1.Value = ""
    Me.ef_name1.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_title2
    '
    Me.ef_title2.DoubleValue = 0.0R
    Me.ef_title2.IntegerValue = 0
    Me.ef_title2.IsReadOnly = False
    Me.ef_title2.Location = New System.Drawing.Point(440, 142)
    Me.ef_title2.Name = "ef_title2"
    Me.ef_title2.OnlyUpperCase = True
    Me.ef_title2.PlaceHolder = Nothing
    Me.ef_title2.Size = New System.Drawing.Size(350, 24)
    Me.ef_title2.SufixText = "Sufix Text"
    Me.ef_title2.SufixTextVisible = True
    Me.ef_title2.TabIndex = 4
    Me.ef_title2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_title2.TextValue = ""
    Me.ef_title2.TextWidth = 150
    Me.ef_title2.Value = ""
    Me.ef_title2.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_title1
    '
    Me.ef_title1.DoubleValue = 0.0R
    Me.ef_title1.IntegerValue = 0
    Me.ef_title1.IsReadOnly = False
    Me.ef_title1.Location = New System.Drawing.Point(440, 80)
    Me.ef_title1.Name = "ef_title1"
    Me.ef_title1.OnlyUpperCase = True
    Me.ef_title1.PlaceHolder = Nothing
    Me.ef_title1.Size = New System.Drawing.Size(350, 24)
    Me.ef_title1.SufixText = "Sufix Text"
    Me.ef_title1.SufixTextVisible = True
    Me.ef_title1.TabIndex = 2
    Me.ef_title1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_title1.TextValue = ""
    Me.ef_title1.TextWidth = 150
    Me.ef_title1.Value = ""
    Me.ef_title1.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_check_payment
    '
    Me.ef_check_payment.DoubleValue = 0.0R
    Me.ef_check_payment.IntegerValue = 0
    Me.ef_check_payment.IsReadOnly = False
    Me.ef_check_payment.Location = New System.Drawing.Point(460, 20)
    Me.ef_check_payment.Name = "ef_check_payment"
    Me.ef_check_payment.PlaceHolder = Nothing
    Me.ef_check_payment.Size = New System.Drawing.Size(330, 24)
    Me.ef_check_payment.SufixText = "Sufix Text"
    Me.ef_check_payment.SufixTextVisible = True
    Me.ef_check_payment.TabIndex = 11
    Me.ef_check_payment.TabStop = False
    Me.ef_check_payment.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_check_payment.TextValue = ""
    Me.ef_check_payment.TextWidth = 130
    Me.ef_check_payment.Value = ""
    Me.ef_check_payment.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_cash_payment
    '
    Me.ef_cash_payment.DoubleValue = 0.0R
    Me.ef_cash_payment.IntegerValue = 0
    Me.ef_cash_payment.IsReadOnly = False
    Me.ef_cash_payment.Location = New System.Drawing.Point(6, 202)
    Me.ef_cash_payment.Name = "ef_cash_payment"
    Me.ef_cash_payment.PlaceHolder = Nothing
    Me.ef_cash_payment.Size = New System.Drawing.Size(330, 24)
    Me.ef_cash_payment.SufixText = "Sufix Text"
    Me.ef_cash_payment.SufixTextVisible = True
    Me.ef_cash_payment.TabIndex = 10
    Me.ef_cash_payment.TabStop = False
    Me.ef_cash_payment.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_cash_payment.TextValue = ""
    Me.ef_cash_payment.TextWidth = 130
    Me.ef_cash_payment.Value = ""
    Me.ef_cash_payment.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_full_payment
    '
    Me.ef_full_payment.DoubleValue = 0.0R
    Me.ef_full_payment.IntegerValue = 0
    Me.ef_full_payment.IsReadOnly = False
    Me.ef_full_payment.Location = New System.Drawing.Point(6, 172)
    Me.ef_full_payment.Name = "ef_full_payment"
    Me.ef_full_payment.PlaceHolder = Nothing
    Me.ef_full_payment.Size = New System.Drawing.Size(330, 24)
    Me.ef_full_payment.SufixText = "Sufix Text"
    Me.ef_full_payment.SufixTextVisible = True
    Me.ef_full_payment.TabIndex = 9
    Me.ef_full_payment.TabStop = False
    Me.ef_full_payment.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_full_payment.TextValue = ""
    Me.ef_full_payment.TextWidth = 130
    Me.ef_full_payment.Value = ""
    Me.ef_full_payment.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_order_date
    '
    Me.ef_order_date.DoubleValue = 0.0R
    Me.ef_order_date.IntegerValue = 0
    Me.ef_order_date.IsReadOnly = False
    Me.ef_order_date.Location = New System.Drawing.Point(460, 50)
    Me.ef_order_date.Name = "ef_order_date"
    Me.ef_order_date.PlaceHolder = Nothing
    Me.ef_order_date.Size = New System.Drawing.Size(330, 24)
    Me.ef_order_date.SufixText = "Sufix Text"
    Me.ef_order_date.SufixTextVisible = True
    Me.ef_order_date.TabIndex = 11
    Me.ef_order_date.TabStop = False
    Me.ef_order_date.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_order_date.TextValue = ""
    Me.ef_order_date.TextWidth = 130
    Me.ef_order_date.Value = ""
    Me.ef_order_date.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_witholding_tax1
    '
    Me.ef_witholding_tax1.DoubleValue = 0.0R
    Me.ef_witholding_tax1.IntegerValue = 0
    Me.ef_witholding_tax1.IsReadOnly = False
    Me.ef_witholding_tax1.Location = New System.Drawing.Point(6, 80)
    Me.ef_witholding_tax1.Name = "ef_witholding_tax1"
    Me.ef_witholding_tax1.PlaceHolder = Nothing
    Me.ef_witholding_tax1.Size = New System.Drawing.Size(330, 24)
    Me.ef_witholding_tax1.SufixText = "Sufix Text"
    Me.ef_witholding_tax1.SufixTextVisible = True
    Me.ef_witholding_tax1.TabIndex = 8
    Me.ef_witholding_tax1.TabStop = False
    Me.ef_witholding_tax1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_witholding_tax1.TextValue = ""
    Me.ef_witholding_tax1.TextWidth = 130
    Me.ef_witholding_tax1.Value = ""
    Me.ef_witholding_tax1.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_witholding_tax2
    '
    Me.ef_witholding_tax2.DoubleValue = 0.0R
    Me.ef_witholding_tax2.IntegerValue = 0
    Me.ef_witholding_tax2.IsReadOnly = False
    Me.ef_witholding_tax2.Location = New System.Drawing.Point(6, 110)
    Me.ef_witholding_tax2.Name = "ef_witholding_tax2"
    Me.ef_witholding_tax2.PlaceHolder = Nothing
    Me.ef_witholding_tax2.Size = New System.Drawing.Size(330, 24)
    Me.ef_witholding_tax2.SufixText = "Sufix Text"
    Me.ef_witholding_tax2.SufixTextVisible = True
    Me.ef_witholding_tax2.TabIndex = 8
    Me.ef_witholding_tax2.TabStop = False
    Me.ef_witholding_tax2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_witholding_tax2.TextValue = ""
    Me.ef_witholding_tax2.TextWidth = 130
    Me.ef_witholding_tax2.Value = ""
    Me.ef_witholding_tax2.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_witholding_tax3
    '
    Me.ef_witholding_tax3.DoubleValue = 0.0R
    Me.ef_witholding_tax3.IntegerValue = 0
    Me.ef_witholding_tax3.IsReadOnly = False
    Me.ef_witholding_tax3.Location = New System.Drawing.Point(6, 140)
    Me.ef_witholding_tax3.Name = "ef_witholding_tax3"
    Me.ef_witholding_tax3.PlaceHolder = Nothing
    Me.ef_witholding_tax3.Size = New System.Drawing.Size(330, 24)
    Me.ef_witholding_tax3.SufixText = "Sufix Text"
    Me.ef_witholding_tax3.SufixTextVisible = True
    Me.ef_witholding_tax3.TabIndex = 8
    Me.ef_witholding_tax3.TabStop = False
    Me.ef_witholding_tax3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_witholding_tax3.TextValue = ""
    Me.ef_witholding_tax3.TextWidth = 130
    Me.ef_witholding_tax3.Value = ""
    Me.ef_witholding_tax3.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_prize
    '
    Me.ef_prize.DoubleValue = 0.0R
    Me.ef_prize.IntegerValue = 0
    Me.ef_prize.IsReadOnly = False
    Me.ef_prize.Location = New System.Drawing.Point(6, 50)
    Me.ef_prize.Name = "ef_prize"
    Me.ef_prize.PlaceHolder = Nothing
    Me.ef_prize.Size = New System.Drawing.Size(330, 24)
    Me.ef_prize.SufixText = "Sufix Text"
    Me.ef_prize.SufixTextVisible = True
    Me.ef_prize.TabIndex = 9
    Me.ef_prize.TabStop = False
    Me.ef_prize.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_prize.TextValue = ""
    Me.ef_prize.TextWidth = 130
    Me.ef_prize.Value = ""
    Me.ef_prize.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_return_balance
    '
    Me.ef_return_balance.DoubleValue = 0.0R
    Me.ef_return_balance.IntegerValue = 0
    Me.ef_return_balance.IsReadOnly = False
    Me.ef_return_balance.Location = New System.Drawing.Point(6, 20)
    Me.ef_return_balance.Name = "ef_return_balance"
    Me.ef_return_balance.PlaceHolder = Nothing
    Me.ef_return_balance.Size = New System.Drawing.Size(330, 24)
    Me.ef_return_balance.SufixText = "Sufix Text"
    Me.ef_return_balance.SufixTextVisible = True
    Me.ef_return_balance.TabIndex = 12
    Me.ef_return_balance.TabStop = False
    Me.ef_return_balance.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_return_balance.TextValue = ""
    Me.ef_return_balance.TextWidth = 130
    Me.ef_return_balance.Value = ""
    Me.ef_return_balance.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_bank_info
    '
    Me.gb_bank_info.Controls.Add(Me.cb_bank_sel)
    Me.gb_bank_info.Controls.Add(Me.dtp_application_date)
    Me.gb_bank_info.Controls.Add(Me.ef_account_number)
    Me.gb_bank_info.Controls.Add(Me.ef_customer_number)
    Me.gb_bank_info.Controls.Add(Me.ef_efective_days)
    Me.gb_bank_info.Controls.Add(Me.ef_payment_type)
    Me.gb_bank_info.Controls.Add(Me.ef_bank_name)
    Me.gb_bank_info.Dock = System.Windows.Forms.DockStyle.Top
    Me.gb_bank_info.Location = New System.Drawing.Point(3, 211)
    Me.gb_bank_info.Name = "gb_bank_info"
    Me.gb_bank_info.Size = New System.Drawing.Size(845, 145)
    Me.gb_bank_info.TabIndex = 1
    Me.gb_bank_info.TabStop = False
    Me.gb_bank_info.Text = "Datos Bancarios"
    '
    'cb_bank_sel
    '
    Me.cb_bank_sel.AllowUnlistedValues = False
    Me.cb_bank_sel.AutoCompleteMode = False
    Me.cb_bank_sel.IsReadOnly = False
    Me.cb_bank_sel.Location = New System.Drawing.Point(460, 20)
    Me.cb_bank_sel.Name = "cb_bank_sel"
    Me.cb_bank_sel.SelectedIndex = -1
    Me.cb_bank_sel.Size = New System.Drawing.Size(330, 24)
    Me.cb_bank_sel.SufixText = "Sufix Text"
    Me.cb_bank_sel.SufixTextVisible = True
    Me.cb_bank_sel.TabIndex = 0
    Me.cb_bank_sel.TextCombo = Nothing
    Me.cb_bank_sel.TextWidth = 130
    '
    'dtp_application_date
    '
    Me.dtp_application_date.Checked = True
    Me.dtp_application_date.IsReadOnly = False
    Me.dtp_application_date.Location = New System.Drawing.Point(460, 50)
    Me.dtp_application_date.Name = "dtp_application_date"
    Me.dtp_application_date.ShowCheckBox = False
    Me.dtp_application_date.ShowUpDown = False
    Me.dtp_application_date.Size = New System.Drawing.Size(260, 24)
    Me.dtp_application_date.SufixText = "Sufix Text"
    Me.dtp_application_date.SufixTextVisible = True
    Me.dtp_application_date.TabIndex = 1
    Me.dtp_application_date.TextWidth = 130
    Me.dtp_application_date.Value = New Date(2013, 1, 17, 17, 29, 40, 0)
    '
    'ef_account_number
    '
    Me.ef_account_number.DoubleValue = 0.0R
    Me.ef_account_number.IntegerValue = 0
    Me.ef_account_number.IsReadOnly = False
    Me.ef_account_number.Location = New System.Drawing.Point(5, 110)
    Me.ef_account_number.Name = "ef_account_number"
    Me.ef_account_number.PlaceHolder = Nothing
    Me.ef_account_number.Size = New System.Drawing.Size(400, 24)
    Me.ef_account_number.SufixText = "Sufix Text"
    Me.ef_account_number.SufixTextVisible = True
    Me.ef_account_number.TabIndex = 4
    Me.ef_account_number.TabStop = False
    Me.ef_account_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_account_number.TextValue = ""
    Me.ef_account_number.TextWidth = 130
    Me.ef_account_number.Value = ""
    Me.ef_account_number.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_customer_number
    '
    Me.ef_customer_number.DoubleValue = 0.0R
    Me.ef_customer_number.IntegerValue = 0
    Me.ef_customer_number.IsReadOnly = False
    Me.ef_customer_number.Location = New System.Drawing.Point(5, 80)
    Me.ef_customer_number.Name = "ef_customer_number"
    Me.ef_customer_number.PlaceHolder = Nothing
    Me.ef_customer_number.Size = New System.Drawing.Size(330, 24)
    Me.ef_customer_number.SufixText = "Sufix Text"
    Me.ef_customer_number.SufixTextVisible = True
    Me.ef_customer_number.TabIndex = 3
    Me.ef_customer_number.TabStop = False
    Me.ef_customer_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_customer_number.TextValue = ""
    Me.ef_customer_number.TextWidth = 130
    Me.ef_customer_number.Value = ""
    Me.ef_customer_number.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_efective_days
    '
    Me.ef_efective_days.DoubleValue = 0.0R
    Me.ef_efective_days.IntegerValue = 0
    Me.ef_efective_days.IsReadOnly = False
    Me.ef_efective_days.Location = New System.Drawing.Point(460, 80)
    Me.ef_efective_days.Name = "ef_efective_days"
    Me.ef_efective_days.PlaceHolder = Nothing
    Me.ef_efective_days.Size = New System.Drawing.Size(260, 24)
    Me.ef_efective_days.SufixText = "Sufix Text"
    Me.ef_efective_days.SufixTextVisible = True
    Me.ef_efective_days.TabIndex = 6
    Me.ef_efective_days.TabStop = False
    Me.ef_efective_days.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_efective_days.TextValue = ""
    Me.ef_efective_days.TextWidth = 130
    Me.ef_efective_days.Value = ""
    Me.ef_efective_days.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_payment_type
    '
    Me.ef_payment_type.DoubleValue = 0.0R
    Me.ef_payment_type.IntegerValue = 0
    Me.ef_payment_type.IsReadOnly = False
    Me.ef_payment_type.Location = New System.Drawing.Point(5, 50)
    Me.ef_payment_type.Name = "ef_payment_type"
    Me.ef_payment_type.PlaceHolder = Nothing
    Me.ef_payment_type.Size = New System.Drawing.Size(330, 24)
    Me.ef_payment_type.SufixText = "Sufix Text"
    Me.ef_payment_type.SufixTextVisible = True
    Me.ef_payment_type.TabIndex = 2
    Me.ef_payment_type.TabStop = False
    Me.ef_payment_type.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_payment_type.TextValue = ""
    Me.ef_payment_type.TextWidth = 130
    Me.ef_payment_type.Value = ""
    Me.ef_payment_type.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_bank_name
    '
    Me.ef_bank_name.DoubleValue = 0.0R
    Me.ef_bank_name.IntegerValue = 0
    Me.ef_bank_name.IsReadOnly = False
    Me.ef_bank_name.Location = New System.Drawing.Point(5, 20)
    Me.ef_bank_name.Name = "ef_bank_name"
    Me.ef_bank_name.PlaceHolder = Nothing
    Me.ef_bank_name.Size = New System.Drawing.Size(400, 24)
    Me.ef_bank_name.SufixText = "Sufix Text"
    Me.ef_bank_name.SufixTextVisible = True
    Me.ef_bank_name.TabIndex = 1
    Me.ef_bank_name.TabStop = False
    Me.ef_bank_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_bank_name.TextValue = ""
    Me.ef_bank_name.TextWidth = 130
    Me.ef_bank_name.Value = ""
    Me.ef_bank_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_player_info
    '
    Me.gb_player_info.Controls.Add(Me.TableLayoutPanel1)
    Me.gb_player_info.Dock = System.Windows.Forms.DockStyle.Top
    Me.gb_player_info.Location = New System.Drawing.Point(3, 3)
    Me.gb_player_info.Name = "gb_player_info"
    Me.gb_player_info.Size = New System.Drawing.Size(845, 208)
    Me.gb_player_info.TabIndex = 0
    Me.gb_player_info.TabStop = False
    Me.gb_player_info.Text = "Datos del Jugador"
    '
    'TableLayoutPanel1
    '
    Me.TableLayoutPanel1.ColumnCount = 2
    Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
    Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
    Me.TableLayoutPanel1.Controls.Add(Me.ef_player_doc_id2, 1, 5)
    Me.TableLayoutPanel1.Controls.Add(Me.ef_player_name3, 0, 0)
    Me.TableLayoutPanel1.Controls.Add(Me.ef_player_doc_id1, 1, 4)
    Me.TableLayoutPanel1.Controls.Add(Me.ef_player_name4, 0, 1)
    Me.TableLayoutPanel1.Controls.Add(Me.cb_player_doc_type2, 0, 5)
    Me.TableLayoutPanel1.Controls.Add(Me.cb_player_doc_type1, 0, 4)
    Me.TableLayoutPanel1.Controls.Add(Me.ef_player_name1, 0, 2)
    Me.TableLayoutPanel1.Controls.Add(Me.ef_player_name2, 0, 3)
    Me.TableLayoutPanel1.Controls.Add(Me.ef_player_doc_type1, 1, 2)
    Me.TableLayoutPanel1.Controls.Add(Me.ef_player_doc_type2, 1, 3)
    Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
    Me.TableLayoutPanel1.Location = New System.Drawing.Point(3, 17)
    Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
    Me.TableLayoutPanel1.RowCount = 6
    Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.TableLayoutPanel1.Size = New System.Drawing.Size(839, 188)
    Me.TableLayoutPanel1.TabIndex = 10
    '
    'ef_player_doc_id2
    '
    Me.ef_player_doc_id2.DoubleValue = 0.0R
    Me.ef_player_doc_id2.IntegerValue = 0
    Me.ef_player_doc_id2.IsReadOnly = False
    Me.ef_player_doc_id2.Location = New System.Drawing.Point(409, 153)
    Me.ef_player_doc_id2.Name = "ef_player_doc_id2"
    Me.ef_player_doc_id2.OnlyUpperCase = True
    Me.ef_player_doc_id2.PlaceHolder = Nothing
    Me.ef_player_doc_id2.Size = New System.Drawing.Size(300, 24)
    Me.ef_player_doc_id2.SufixText = "Sufix Text"
    Me.ef_player_doc_id2.SufixTextVisible = True
    Me.ef_player_doc_id2.TabIndex = 6
    Me.ef_player_doc_id2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_player_doc_id2.TextValue = ""
    Me.ef_player_doc_id2.TextWidth = 100
    Me.ef_player_doc_id2.Value = ""
    Me.ef_player_doc_id2.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_player_name3
    '
    Me.ef_player_name3.DoubleValue = 0.0R
    Me.ef_player_name3.IntegerValue = 0
    Me.ef_player_name3.IsReadOnly = False
    Me.ef_player_name3.Location = New System.Drawing.Point(3, 3)
    Me.ef_player_name3.Name = "ef_player_name3"
    Me.ef_player_name3.OnlyUpperCase = True
    Me.ef_player_name3.PlaceHolder = Nothing
    Me.ef_player_name3.Size = New System.Drawing.Size(400, 24)
    Me.ef_player_name3.SufixText = "Sufix Text"
    Me.ef_player_name3.SufixTextVisible = True
    Me.ef_player_name3.TabIndex = 0
    Me.ef_player_name3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_player_name3.TextValue = ""
    Me.ef_player_name3.TextWidth = 130
    Me.ef_player_name3.Value = ""
    Me.ef_player_name3.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_player_doc_id1
    '
    Me.ef_player_doc_id1.DoubleValue = 0.0R
    Me.ef_player_doc_id1.IntegerValue = 0
    Me.ef_player_doc_id1.IsReadOnly = False
    Me.ef_player_doc_id1.Location = New System.Drawing.Point(409, 123)
    Me.ef_player_doc_id1.Name = "ef_player_doc_id1"
    Me.ef_player_doc_id1.OnlyUpperCase = True
    Me.ef_player_doc_id1.PlaceHolder = Nothing
    Me.ef_player_doc_id1.Size = New System.Drawing.Size(300, 24)
    Me.ef_player_doc_id1.SufixText = "Sufix Text"
    Me.ef_player_doc_id1.SufixTextVisible = True
    Me.ef_player_doc_id1.TabIndex = 4
    Me.ef_player_doc_id1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_player_doc_id1.TextValue = ""
    Me.ef_player_doc_id1.TextWidth = 100
    Me.ef_player_doc_id1.Value = ""
    Me.ef_player_doc_id1.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_player_name4
    '
    Me.ef_player_name4.DoubleValue = 0.0R
    Me.ef_player_name4.IntegerValue = 0
    Me.ef_player_name4.IsReadOnly = False
    Me.ef_player_name4.Location = New System.Drawing.Point(3, 33)
    Me.ef_player_name4.Name = "ef_player_name4"
    Me.ef_player_name4.OnlyUpperCase = True
    Me.ef_player_name4.PlaceHolder = Nothing
    Me.ef_player_name4.Size = New System.Drawing.Size(400, 24)
    Me.ef_player_name4.SufixText = "Sufix Text"
    Me.ef_player_name4.SufixTextVisible = True
    Me.ef_player_name4.TabIndex = 9
    Me.ef_player_name4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_player_name4.TextValue = ""
    Me.ef_player_name4.TextWidth = 130
    Me.ef_player_name4.Value = ""
    Me.ef_player_name4.ValueForeColor = System.Drawing.Color.Blue
    '
    'cb_player_doc_type2
    '
    Me.cb_player_doc_type2.AllowUnlistedValues = False
    Me.cb_player_doc_type2.AutoCompleteMode = False
    Me.cb_player_doc_type2.IsReadOnly = False
    Me.cb_player_doc_type2.Location = New System.Drawing.Point(3, 153)
    Me.cb_player_doc_type2.Name = "cb_player_doc_type2"
    Me.cb_player_doc_type2.SelectedIndex = -1
    Me.cb_player_doc_type2.Size = New System.Drawing.Size(330, 24)
    Me.cb_player_doc_type2.SufixText = "Sufix Text"
    Me.cb_player_doc_type2.SufixTextVisible = True
    Me.cb_player_doc_type2.TabIndex = 5
    Me.cb_player_doc_type2.TextCombo = Nothing
    Me.cb_player_doc_type2.TextWidth = 130
    '
    'cb_player_doc_type1
    '
    Me.cb_player_doc_type1.AllowUnlistedValues = False
    Me.cb_player_doc_type1.AutoCompleteMode = False
    Me.cb_player_doc_type1.IsReadOnly = False
    Me.cb_player_doc_type1.Location = New System.Drawing.Point(3, 123)
    Me.cb_player_doc_type1.Name = "cb_player_doc_type1"
    Me.cb_player_doc_type1.SelectedIndex = -1
    Me.cb_player_doc_type1.Size = New System.Drawing.Size(330, 24)
    Me.cb_player_doc_type1.SufixText = "Sufix Text"
    Me.cb_player_doc_type1.SufixTextVisible = True
    Me.cb_player_doc_type1.TabIndex = 3
    Me.cb_player_doc_type1.TextCombo = Nothing
    Me.cb_player_doc_type1.TextWidth = 130
    '
    'ef_player_name1
    '
    Me.ef_player_name1.DoubleValue = 0.0R
    Me.ef_player_name1.IntegerValue = 0
    Me.ef_player_name1.IsReadOnly = False
    Me.ef_player_name1.Location = New System.Drawing.Point(3, 63)
    Me.ef_player_name1.Name = "ef_player_name1"
    Me.ef_player_name1.OnlyUpperCase = True
    Me.ef_player_name1.PlaceHolder = Nothing
    Me.ef_player_name1.Size = New System.Drawing.Size(400, 24)
    Me.ef_player_name1.SufixText = "Sufix Text"
    Me.ef_player_name1.SufixTextVisible = True
    Me.ef_player_name1.TabIndex = 1
    Me.ef_player_name1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_player_name1.TextValue = ""
    Me.ef_player_name1.TextWidth = 130
    Me.ef_player_name1.Value = ""
    Me.ef_player_name1.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_player_name2
    '
    Me.ef_player_name2.DoubleValue = 0.0R
    Me.ef_player_name2.IntegerValue = 0
    Me.ef_player_name2.IsReadOnly = False
    Me.ef_player_name2.Location = New System.Drawing.Point(3, 93)
    Me.ef_player_name2.Name = "ef_player_name2"
    Me.ef_player_name2.OnlyUpperCase = True
    Me.ef_player_name2.PlaceHolder = Nothing
    Me.ef_player_name2.Size = New System.Drawing.Size(400, 24)
    Me.ef_player_name2.SufixText = "Sufix Text"
    Me.ef_player_name2.SufixTextVisible = True
    Me.ef_player_name2.TabIndex = 2
    Me.ef_player_name2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_player_name2.TextValue = ""
    Me.ef_player_name2.TextWidth = 130
    Me.ef_player_name2.Value = ""
    Me.ef_player_name2.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_player_doc_type1
    '
    Me.ef_player_doc_type1.DoubleValue = 0.0R
    Me.ef_player_doc_type1.IntegerValue = 0
    Me.ef_player_doc_type1.IsReadOnly = False
    Me.ef_player_doc_type1.Location = New System.Drawing.Point(409, 63)
    Me.ef_player_doc_type1.Name = "ef_player_doc_type1"
    Me.ef_player_doc_type1.PlaceHolder = Nothing
    Me.ef_player_doc_type1.Size = New System.Drawing.Size(330, 24)
    Me.ef_player_doc_type1.SufixText = "Sufix Text"
    Me.ef_player_doc_type1.SufixTextVisible = True
    Me.ef_player_doc_type1.TabIndex = 10
    Me.ef_player_doc_type1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_player_doc_type1.TextValue = ""
    Me.ef_player_doc_type1.TextWidth = 130
    Me.ef_player_doc_type1.Value = ""
    Me.ef_player_doc_type1.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_player_doc_type2
    '
    Me.ef_player_doc_type2.DoubleValue = 0.0R
    Me.ef_player_doc_type2.IntegerValue = 0
    Me.ef_player_doc_type2.IsReadOnly = False
    Me.ef_player_doc_type2.Location = New System.Drawing.Point(409, 93)
    Me.ef_player_doc_type2.Name = "ef_player_doc_type2"
    Me.ef_player_doc_type2.PlaceHolder = Nothing
    Me.ef_player_doc_type2.Size = New System.Drawing.Size(330, 24)
    Me.ef_player_doc_type2.SufixText = "Sufix Text"
    Me.ef_player_doc_type2.SufixTextVisible = True
    Me.ef_player_doc_type2.TabIndex = 10
    Me.ef_player_doc_type2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_player_doc_type2.TextValue = ""
    Me.ef_player_doc_type2.TextWidth = 130
    Me.ef_player_doc_type2.Value = ""
    Me.ef_player_doc_type2.ValueForeColor = System.Drawing.Color.Blue
    '
    'frm_payment_order_edit
    '
    Me.ClientSize = New System.Drawing.Size(961, 627)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_payment_order_edit"
    Me.panel_data.ResumeLayout(False)
    Me.tab_order_viewer.ResumeLayout(False)
    Me.TabPage1.ResumeLayout(False)
    Me.gb_room_info.ResumeLayout(False)
    Me.gb_bank_info.ResumeLayout(False)
    Me.gb_player_info.ResumeLayout(False)
    Me.TableLayoutPanel1.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents tab_order_viewer As System.Windows.Forms.TabControl
  Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
  Friend WithEvents gb_room_info As System.Windows.Forms.GroupBox
  Friend WithEvents gb_bank_info As System.Windows.Forms.GroupBox
  Friend WithEvents gb_player_info As System.Windows.Forms.GroupBox
  Friend WithEvents ef_player_doc_id1 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_player_name1 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_player_doc_id2 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_order_date As GUI_Controls.uc_entry_field
  Friend WithEvents cb_player_doc_type2 As GUI_Controls.uc_combo
  Friend WithEvents cb_player_doc_type1 As GUI_Controls.uc_combo
  Friend WithEvents ef_payment_type As GUI_Controls.uc_entry_field
  Friend WithEvents ef_bank_name As GUI_Controls.uc_entry_field
  Friend WithEvents ef_account_number As GUI_Controls.uc_entry_field
  Friend WithEvents ef_customer_number As GUI_Controls.uc_entry_field
  Friend WithEvents ef_efective_days As GUI_Controls.uc_entry_field
  Friend WithEvents dtp_application_date As GUI_Controls.uc_date_picker
  Friend WithEvents ef_witholding_tax1 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_prize As GUI_Controls.uc_entry_field
  Friend WithEvents ef_return_balance As GUI_Controls.uc_entry_field
  Friend WithEvents ef_witholding_tax2 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_witholding_tax3 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_check_payment As GUI_Controls.uc_entry_field
  Friend WithEvents ef_cash_payment As GUI_Controls.uc_entry_field
  Friend WithEvents ef_full_payment As GUI_Controls.uc_entry_field
  Friend WithEvents cb_bank_sel As GUI_Controls.uc_combo
  Friend WithEvents ef_player_name3 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_player_name2 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_title2 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_title1 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_name2 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_name1 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_player_name4 As GUI_Controls.uc_entry_field
  Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
  Friend WithEvents ef_player_doc_type1 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_player_doc_type2 As GUI_Controls.uc_entry_field

End Class
