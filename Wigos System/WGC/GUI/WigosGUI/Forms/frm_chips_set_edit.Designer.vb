<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_chips_set_edit
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_chips_set_edit))
    Me.chk_enabled = New System.Windows.Forms.CheckBox()
    Me.ef_chips_sets_name = New GUI_Controls.uc_entry_field()
    Me.dg_chips = New GUI_Controls.uc_grid()
    Me.cd_button_color = New System.Windows.Forms.ColorDialog()
    Me.btn_add = New GUI_Controls.uc_button()
    Me.btn_del = New GUI_Controls.uc_button()
    Me.gb_setConfig = New System.Windows.Forms.GroupBox()
    Me.cmb_chip_type = New GUI_Controls.uc_combo()
    Me.cmb_iso_code = New GUI_Controls.uc_combo()
    Me.gb_grid = New System.Windows.Forms.GroupBox()
    Me.lbl_disabled_group = New System.Windows.Forms.Label()
    Me.lbl_max_chips_for_set = New System.Windows.Forms.Label()
    Me.panel_data.SuspendLayout()
    Me.gb_setConfig.SuspendLayout()
    Me.gb_grid.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_setConfig)
    Me.panel_data.Controls.Add(Me.gb_grid)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(619, 500)
    '
    'chk_enabled
    '
    Me.chk_enabled.AutoSize = True
    Me.chk_enabled.Checked = True
    Me.chk_enabled.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_enabled.Location = New System.Drawing.Point(504, 54)
    Me.chk_enabled.Name = "chk_enabled"
    Me.chk_enabled.Size = New System.Drawing.Size(78, 17)
    Me.chk_enabled.TabIndex = 3
    Me.chk_enabled.Text = "xEnabled"
    '
    'ef_chips_sets_name
    '
    Me.ef_chips_sets_name.DoubleValue = 0.0R
    Me.ef_chips_sets_name.IntegerValue = 0
    Me.ef_chips_sets_name.IsReadOnly = False
    Me.ef_chips_sets_name.Location = New System.Drawing.Point(6, 20)
    Me.ef_chips_sets_name.Name = "ef_chips_sets_name"
    Me.ef_chips_sets_name.PlaceHolder = Nothing
    Me.ef_chips_sets_name.Size = New System.Drawing.Size(341, 24)
    Me.ef_chips_sets_name.SufixText = "Sufix Text"
    Me.ef_chips_sets_name.SufixTextVisible = True
    Me.ef_chips_sets_name.TabIndex = 0
    Me.ef_chips_sets_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_chips_sets_name.TextValue = ""
    Me.ef_chips_sets_name.TextWidth = 100
    Me.ef_chips_sets_name.Value = ""
    Me.ef_chips_sets_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'dg_chips
    '
    Me.dg_chips.CurrentCol = -1
    Me.dg_chips.CurrentRow = -1
    Me.dg_chips.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_chips.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_chips.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_chips.Location = New System.Drawing.Point(6, 18)
    Me.dg_chips.Name = "dg_chips"
    Me.dg_chips.PanelRightVisible = False
    Me.dg_chips.Redraw = True
    Me.dg_chips.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    Me.dg_chips.Size = New System.Drawing.Size(595, 348)
    Me.dg_chips.Sortable = False
    Me.dg_chips.SortAscending = True
    Me.dg_chips.SortByCol = 0
    Me.dg_chips.TabIndex = 0
    Me.dg_chips.ToolTipped = True
    Me.dg_chips.TopRow = -2
    Me.dg_chips.WordWrap = False
    '
    'cd_button_color
    '
    Me.cd_button_color.SolidColorOnly = True
    '
    'btn_add
    '
    Me.btn_add.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_add.Location = New System.Drawing.Point(495, 372)
    Me.btn_add.Name = "btn_add"
    Me.btn_add.Size = New System.Drawing.Size(50, 21)
    Me.btn_add.TabIndex = 1
    Me.btn_add.ToolTipped = False
    Me.btn_add.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.ADD_DELETE
    '
    'btn_del
    '
    Me.btn_del.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_del.Location = New System.Drawing.Point(551, 372)
    Me.btn_del.Name = "btn_del"
    Me.btn_del.Size = New System.Drawing.Size(50, 21)
    Me.btn_del.TabIndex = 2
    Me.btn_del.ToolTipped = False
    Me.btn_del.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.ADD_DELETE
    '
    'gb_setConfig
    '
    Me.gb_setConfig.Controls.Add(Me.cmb_chip_type)
    Me.gb_setConfig.Controls.Add(Me.ef_chips_sets_name)
    Me.gb_setConfig.Controls.Add(Me.chk_enabled)
    Me.gb_setConfig.Controls.Add(Me.cmb_iso_code)
    Me.gb_setConfig.Location = New System.Drawing.Point(3, 3)
    Me.gb_setConfig.Name = "gb_setConfig"
    Me.gb_setConfig.Size = New System.Drawing.Size(609, 86)
    Me.gb_setConfig.TabIndex = 0
    Me.gb_setConfig.TabStop = False
    '
    'cmb_chip_type
    '
    Me.cmb_chip_type.AllowUnlistedValues = False
    Me.cmb_chip_type.AutoCompleteMode = False
    Me.cmb_chip_type.IsReadOnly = False
    Me.cmb_chip_type.Location = New System.Drawing.Point(6, 51)
    Me.cmb_chip_type.Name = "cmb_chip_type"
    Me.cmb_chip_type.SelectedIndex = -1
    Me.cmb_chip_type.Size = New System.Drawing.Size(341, 24)
    Me.cmb_chip_type.SufixText = "Sufix Text"
    Me.cmb_chip_type.SufixTextVisible = True
    Me.cmb_chip_type.TabIndex = 1
    Me.cmb_chip_type.TextCombo = Nothing
    Me.cmb_chip_type.TextWidth = 100
    '
    'cmb_iso_code
    '
    Me.cmb_iso_code.AllowUnlistedValues = False
    Me.cmb_iso_code.AutoCompleteMode = False
    Me.cmb_iso_code.IsReadOnly = False
    Me.cmb_iso_code.Location = New System.Drawing.Point(398, 20)
    Me.cmb_iso_code.Name = "cmb_iso_code"
    Me.cmb_iso_code.SelectedIndex = -1
    Me.cmb_iso_code.Size = New System.Drawing.Size(184, 24)
    Me.cmb_iso_code.SufixText = "Sufix Text"
    Me.cmb_iso_code.SufixTextVisible = True
    Me.cmb_iso_code.TabIndex = 2
    Me.cmb_iso_code.TextCombo = Nothing
    Me.cmb_iso_code.TextWidth = 100
    '
    'gb_grid
    '
    Me.gb_grid.Controls.Add(Me.lbl_disabled_group)
    Me.gb_grid.Controls.Add(Me.lbl_max_chips_for_set)
    Me.gb_grid.Controls.Add(Me.btn_del)
    Me.gb_grid.Controls.Add(Me.dg_chips)
    Me.gb_grid.Controls.Add(Me.btn_add)
    Me.gb_grid.Location = New System.Drawing.Point(3, 86)
    Me.gb_grid.Name = "gb_grid"
    Me.gb_grid.Size = New System.Drawing.Size(609, 399)
    Me.gb_grid.TabIndex = 1
    Me.gb_grid.TabStop = False
    '
    'lbl_disabled_group
    '
    Me.lbl_disabled_group.AutoSize = True
    Me.lbl_disabled_group.ForeColor = System.Drawing.Color.Red
    Me.lbl_disabled_group.Location = New System.Drawing.Point(203, 374)
    Me.lbl_disabled_group.Name = "lbl_disabled_group"
    Me.lbl_disabled_group.Size = New System.Drawing.Size(0, 13)
    Me.lbl_disabled_group.TabIndex = 4
    '
    'lbl_max_chips_for_set
    '
    Me.lbl_max_chips_for_set.AutoSize = True
    Me.lbl_max_chips_for_set.Location = New System.Drawing.Point(6, 374)
    Me.lbl_max_chips_for_set.Name = "lbl_max_chips_for_set"
    Me.lbl_max_chips_for_set.Size = New System.Drawing.Size(33, 13)
    Me.lbl_max_chips_for_set.TabIndex = 3
    Me.lbl_max_chips_for_set.Text = "1/20"
    '
    'frm_chips_set_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(723, 511)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.MinimumSize = New System.Drawing.Size(545, 499)
    Me.Name = "frm_chips_set_edit"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_chips_set_edit"
    Me.panel_data.ResumeLayout(False)
    Me.gb_setConfig.ResumeLayout(False)
    Me.gb_setConfig.PerformLayout()
    Me.gb_grid.ResumeLayout(False)
    Me.gb_grid.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents chk_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents ef_chips_sets_name As GUI_Controls.uc_entry_field
  Friend WithEvents dg_chips As GUI_Controls.uc_grid
  Friend WithEvents cd_button_color As System.Windows.Forms.ColorDialog
  Friend WithEvents btn_add As GUI_Controls.uc_button
  Friend WithEvents btn_del As GUI_Controls.uc_button
  Friend WithEvents gb_setConfig As System.Windows.Forms.GroupBox
  Friend WithEvents gb_grid As System.Windows.Forms.GroupBox
  Friend WithEvents cmb_chip_type As GUI_Controls.uc_combo
  Friend WithEvents cmb_iso_code As GUI_Controls.uc_combo
  Friend WithEvents lbl_max_chips_for_set As System.Windows.Forms.Label
  Friend WithEvents lbl_disabled_group As System.Windows.Forms.Label
End Class
