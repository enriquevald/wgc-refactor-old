﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_countr_sel
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.gb_area_island = New System.Windows.Forms.GroupBox()
    Me.ef_smoking = New GUI_Controls.uc_entry_field()
    Me.cmb_area = New GUI_Controls.uc_combo()
    Me.ef_floor_id = New GUI_Controls.uc_entry_field()
    Me.cmb_bank = New GUI_Controls.uc_combo()
    Me.gb_status = New System.Windows.Forms.GroupBox()
    Me.chk_offline = New System.Windows.Forms.CheckBox()
    Me.chk_online = New System.Windows.Forms.CheckBox()
    Me.gb_CountR = New System.Windows.Forms.GroupBox()
    Me.txt_name = New GUI_Controls.uc_entry_field()
    Me.txt_code = New GUI_Controls.uc_entry_field()
    Me.gb_enabled = New System.Windows.Forms.GroupBox()
    Me.chk_no = New System.Windows.Forms.CheckBox()
    Me.chk_yes = New System.Windows.Forms.CheckBox()
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_area_island.SuspendLayout()
    Me.gb_status.SuspendLayout()
    Me.gb_CountR.SuspendLayout()
    Me.gb_enabled.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.chk_terminal_location)
    Me.panel_filter.Controls.Add(Me.gb_enabled)
    Me.panel_filter.Controls.Add(Me.gb_CountR)
    Me.panel_filter.Controls.Add(Me.gb_status)
    Me.panel_filter.Controls.Add(Me.gb_area_island)
    Me.panel_filter.Size = New System.Drawing.Size(1197, 163)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_area_island, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_status, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_CountR, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_enabled, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_terminal_location, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 167)
    Me.panel_data.Size = New System.Drawing.Size(1197, 560)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1191, 21)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1191, 5)
    '
    'gb_area_island
    '
    Me.gb_area_island.Controls.Add(Me.ef_smoking)
    Me.gb_area_island.Controls.Add(Me.cmb_area)
    Me.gb_area_island.Controls.Add(Me.ef_floor_id)
    Me.gb_area_island.Controls.Add(Me.cmb_bank)
    Me.gb_area_island.Location = New System.Drawing.Point(399, 8)
    Me.gb_area_island.Name = "gb_area_island"
    Me.gb_area_island.Size = New System.Drawing.Size(360, 131)
    Me.gb_area_island.TabIndex = 3
    Me.gb_area_island.TabStop = False
    Me.gb_area_island.Text = "xLocation"
    '
    'ef_smoking
    '
    Me.ef_smoking.DoubleValue = 0.0R
    Me.ef_smoking.IntegerValue = 0
    Me.ef_smoking.IsReadOnly = False
    Me.ef_smoking.Location = New System.Drawing.Point(11, 40)
    Me.ef_smoking.Name = "ef_smoking"
    Me.ef_smoking.PlaceHolder = Nothing
    Me.ef_smoking.Size = New System.Drawing.Size(327, 24)
    Me.ef_smoking.SufixText = "Sufix Text"
    Me.ef_smoking.SufixTextVisible = True
    Me.ef_smoking.TabIndex = 1
    Me.ef_smoking.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_smoking.TextValue = ""
    Me.ef_smoking.TextWidth = 120
    Me.ef_smoking.Value = ""
    Me.ef_smoking.ValueForeColor = System.Drawing.Color.Blue
    '
    'cmb_area
    '
    Me.cmb_area.AllowUnlistedValues = False
    Me.cmb_area.AutoCompleteMode = False
    Me.cmb_area.IsReadOnly = False
    Me.cmb_area.Location = New System.Drawing.Point(11, 12)
    Me.cmb_area.Name = "cmb_area"
    Me.cmb_area.SelectedIndex = -1
    Me.cmb_area.Size = New System.Drawing.Size(327, 24)
    Me.cmb_area.SufixText = "Sufix Text"
    Me.cmb_area.SufixTextVisible = True
    Me.cmb_area.TabIndex = 0
    Me.cmb_area.TextCombo = Nothing
    Me.cmb_area.TextWidth = 120
    '
    'ef_floor_id
    '
    Me.ef_floor_id.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.ef_floor_id.DoubleValue = 0.0R
    Me.ef_floor_id.IntegerValue = 0
    Me.ef_floor_id.IsReadOnly = False
    Me.ef_floor_id.Location = New System.Drawing.Point(11, 100)
    Me.ef_floor_id.Name = "ef_floor_id"
    Me.ef_floor_id.PlaceHolder = Nothing
    Me.ef_floor_id.Size = New System.Drawing.Size(327, 24)
    Me.ef_floor_id.SufixText = "Sufix Text"
    Me.ef_floor_id.SufixTextVisible = True
    Me.ef_floor_id.TabIndex = 3
    Me.ef_floor_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_floor_id.TextValue = ""
    Me.ef_floor_id.TextWidth = 120
    Me.ef_floor_id.Value = ""
    Me.ef_floor_id.ValueForeColor = System.Drawing.Color.Blue
    '
    'cmb_bank
    '
    Me.cmb_bank.AllowUnlistedValues = False
    Me.cmb_bank.AutoCompleteMode = False
    Me.cmb_bank.IsReadOnly = False
    Me.cmb_bank.Location = New System.Drawing.Point(11, 70)
    Me.cmb_bank.Name = "cmb_bank"
    Me.cmb_bank.SelectedIndex = -1
    Me.cmb_bank.Size = New System.Drawing.Size(327, 24)
    Me.cmb_bank.SufixText = "Sufix Text"
    Me.cmb_bank.SufixTextVisible = True
    Me.cmb_bank.TabIndex = 2
    Me.cmb_bank.TextCombo = Nothing
    Me.cmb_bank.TextWidth = 120
    '
    'gb_status
    '
    Me.gb_status.Controls.Add(Me.chk_offline)
    Me.gb_status.Controls.Add(Me.chk_online)
    Me.gb_status.Location = New System.Drawing.Point(6, 96)
    Me.gb_status.Name = "gb_status"
    Me.gb_status.Size = New System.Drawing.Size(277, 43)
    Me.gb_status.TabIndex = 1
    Me.gb_status.TabStop = False
    Me.gb_status.Text = "xEnabled"
    '
    'chk_offline
    '
    Me.chk_offline.AutoSize = True
    Me.chk_offline.Location = New System.Drawing.Point(128, 19)
    Me.chk_offline.Name = "chk_offline"
    Me.chk_offline.Size = New System.Drawing.Size(70, 17)
    Me.chk_offline.TabIndex = 1
    Me.chk_offline.Text = "xOffline"
    Me.chk_offline.UseVisualStyleBackColor = True
    '
    'chk_online
    '
    Me.chk_online.AutoSize = True
    Me.chk_online.Location = New System.Drawing.Point(16, 19)
    Me.chk_online.Name = "chk_online"
    Me.chk_online.Size = New System.Drawing.Size(69, 17)
    Me.chk_online.TabIndex = 0
    Me.chk_online.Text = "xOnline"
    Me.chk_online.UseVisualStyleBackColor = True
    '
    'gb_CountR
    '
    Me.gb_CountR.Controls.Add(Me.txt_name)
    Me.gb_CountR.Controls.Add(Me.txt_code)
    Me.gb_CountR.Location = New System.Drawing.Point(6, 8)
    Me.gb_CountR.Name = "gb_CountR"
    Me.gb_CountR.Size = New System.Drawing.Size(277, 82)
    Me.gb_CountR.TabIndex = 0
    Me.gb_CountR.TabStop = False
    Me.gb_CountR.Text = "GroupBox1"
    '
    'txt_name
    '
    Me.txt_name.DoubleValue = 0.0R
    Me.txt_name.IntegerValue = 0
    Me.txt_name.IsReadOnly = False
    Me.txt_name.Location = New System.Drawing.Point(6, 48)
    Me.txt_name.Name = "txt_name"
    Me.txt_name.PlaceHolder = Nothing
    Me.txt_name.Size = New System.Drawing.Size(265, 24)
    Me.txt_name.SufixText = "Sufix Text"
    Me.txt_name.SufixTextVisible = True
    Me.txt_name.TabIndex = 1
    Me.txt_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.txt_name.TextValue = ""
    Me.txt_name.TextWidth = 120
    Me.txt_name.Value = ""
    Me.txt_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'txt_code
    '
    Me.txt_code.AutoSize = True
    Me.txt_code.DoubleValue = 0.0R
    Me.txt_code.IntegerValue = 0
    Me.txt_code.IsReadOnly = False
    Me.txt_code.Location = New System.Drawing.Point(6, 20)
    Me.txt_code.Name = "txt_code"
    Me.txt_code.PlaceHolder = Nothing
    Me.txt_code.Size = New System.Drawing.Size(265, 24)
    Me.txt_code.SufixText = "Sufix Text"
    Me.txt_code.SufixTextVisible = True
    Me.txt_code.TabIndex = 0
    Me.txt_code.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.txt_code.TextValue = ""
    Me.txt_code.TextWidth = 120
    Me.txt_code.Value = ""
    Me.txt_code.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_enabled
    '
    Me.gb_enabled.Controls.Add(Me.chk_no)
    Me.gb_enabled.Controls.Add(Me.chk_yes)
    Me.gb_enabled.Location = New System.Drawing.Point(289, 8)
    Me.gb_enabled.Name = "gb_enabled"
    Me.gb_enabled.Size = New System.Drawing.Size(104, 82)
    Me.gb_enabled.TabIndex = 2
    Me.gb_enabled.TabStop = False
    Me.gb_enabled.Text = "xEnabled"
    '
    'chk_no
    '
    Me.chk_no.AutoSize = True
    Me.chk_no.Location = New System.Drawing.Point(11, 55)
    Me.chk_no.Name = "chk_no"
    Me.chk_no.Size = New System.Drawing.Size(48, 17)
    Me.chk_no.TabIndex = 1
    Me.chk_no.Text = "xNo"
    Me.chk_no.UseVisualStyleBackColor = True
    '
    'chk_yes
    '
    Me.chk_yes.AutoSize = True
    Me.chk_yes.Location = New System.Drawing.Point(11, 24)
    Me.chk_yes.Name = "chk_yes"
    Me.chk_yes.Size = New System.Drawing.Size(53, 17)
    Me.chk_yes.TabIndex = 0
    Me.chk_yes.Text = "xYes"
    Me.chk_yes.UseVisualStyleBackColor = True
    '
    'chk_terminal_location
    '
    Me.chk_terminal_location.Location = New System.Drawing.Point(6, 145)
    Me.chk_terminal_location.Name = "chk_terminal_location"
    Me.chk_terminal_location.Size = New System.Drawing.Size(199, 16)
    Me.chk_terminal_location.TabIndex = 18
    Me.chk_terminal_location.Text = "xShow terminals location"
    '
    'frm_countr_sel
    '
    Me.ClientSize = New System.Drawing.Size(1205, 731)
    Me.Name = "frm_countr_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_area_island.ResumeLayout(False)
    Me.gb_status.ResumeLayout(False)
    Me.gb_status.PerformLayout()
    Me.gb_CountR.ResumeLayout(False)
    Me.gb_CountR.PerformLayout()
    Me.gb_enabled.ResumeLayout(False)
    Me.gb_enabled.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_area_island As System.Windows.Forms.GroupBox
  Friend WithEvents cmb_area As GUI_Controls.uc_combo
  Friend WithEvents ef_floor_id As GUI_Controls.uc_entry_field
  Friend WithEvents cmb_bank As GUI_Controls.uc_combo
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents chk_online As System.Windows.Forms.CheckBox
  Friend WithEvents gb_CountR As System.Windows.Forms.GroupBox
  Friend WithEvents txt_name As GUI_Controls.uc_entry_field
  Friend WithEvents txt_code As GUI_Controls.uc_entry_field
  Friend WithEvents ef_smoking As GUI_Controls.uc_entry_field
  Friend WithEvents chk_offline As System.Windows.Forms.CheckBox
  Friend WithEvents gb_enabled As System.Windows.Forms.GroupBox
  Friend WithEvents chk_no As System.Windows.Forms.CheckBox
  Friend WithEvents chk_yes As System.Windows.Forms.CheckBox
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox

End Class
