'-------------------------------------------------------------------
' Copyright � 2007 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_statistics_terminals
' DESCRIPTION:   This screen allows to view the terminals statistics between:
'                           - two dates 
'                           - for all terminals or one terminal
' AUTHOR:        Merc� Torrelles Minguella
' CREATION DATE: 13-APR-2007
'
' REVISION HISTORY: 
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 13-APR-2007  MTM     Initial version
' 15-DIC-2008  SLE     Modified query to avoid timeouts 
' 04-APR-2012  JCM     NLS String "Varios" changed to NLS String "Multijuego"
' 09-MAY-2012  JAB     Fixed Bug #110 & #101: The time in the filters doesn't work properly.
' 18-OCT-2012  RRB     Now show Netwin % in excel.
' 03-DEC-2012  RRB     Add Average Bet column.
' 28-MAR-2013  RCI & HBB    Added column SPH_THEORETICAL_WON_AMOUNT in table SALES_PER_HOUR. Needed to calculate the theoretical payout %. Also added a column to the grid with the theoretical payout %
' 25-MAY-2013  JCA    Change to use new table providers_games
' 18-AUG-2014  AMF     Progressive Jackpot
' 03-SEP-2014  LEM     Added functionality to show terminal location data.
' 17-OCT-2014  JMV     Fixed WIG-1431
' 27-MAR-2015  ANM     Calling GetProviderIdListSelected always returns a query
' 14-APR-2015  FJC     BackLog Item 966
' 13-MAY-2015  FOS     WIG 2336:Error filter by Terminal
' 10-JUN-2015  DHA     Error when bigger numbers of money type
' 17-JUN-2015  FAV     WIG-2461: Errors in the log
' 11-NOV-2015  FOS     Product Backlog Item 5839:Garcia River change NLS
' 25-OCT-2016  LTC     Bug 6269:Statistics Group by Terminal: Error on filters - Terminal Offline
' 24-NOV-2016  ETP     Bug 20935: GUI: Error en el reporte terminales agrupadas por terminal.
' 08-MAR-2017  DHA     Bug 25341: error calculating TOTAL NETWIN
' 24-MAY-2017  LTC     Bug 26822: Error ON Statistics by Provider Report
' 15-JUN-2018 GDA    Bug 33063 - WIGOS-12243 - [Ticket #14517] Fallo � Reporte de Estad�stica agrupada por fecha, proveedor, terminal- Jugadas Version V03.08.0001
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Text
Imports WSI.Common

Public Class frm_statistics_terminals
  Inherits frm_base_sel


#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector
  Friend WithEvents lbl_netwin As System.Windows.Forms.Label
  Friend WithEvents lbl_payout As System.Windows.Forms.Label
  Friend WithEvents Uc_combo_games As GUI_Controls.uc_combo_games
  Friend WithEvents chk_exclude_rows_without_activity As System.Windows.Forms.CheckBox
  Friend WithEvents gb_payout_variance_threshold As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_payout_variance_threshold_color As System.Windows.Forms.Label
  Friend WithEvents chk_payout_variance_threshold As System.Windows.Forms.CheckBox
  Friend WithEvents ef_payout_variance_threshold As GUI_Controls.uc_entry_field
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.uc_pr_list = New GUI_Controls.uc_provider()
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector()
    Me.lbl_netwin = New System.Windows.Forms.Label()
    Me.lbl_payout = New System.Windows.Forms.Label()
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox()
    Me.Uc_combo_games = New GUI_Controls.uc_combo_games()
    Me.chk_exclude_rows_without_activity = New System.Windows.Forms.CheckBox()
    Me.gb_payout_variance_threshold = New System.Windows.Forms.GroupBox()
    Me.lbl_payout_variance_threshold_color = New System.Windows.Forms.Label()
    Me.chk_payout_variance_threshold = New System.Windows.Forms.CheckBox()
    Me.ef_payout_variance_threshold = New GUI_Controls.uc_entry_field()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_payout_variance_threshold.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_payout_variance_threshold)
    Me.panel_filter.Controls.Add(Me.chk_exclude_rows_without_activity)
    Me.panel_filter.Controls.Add(Me.Uc_combo_games)
    Me.panel_filter.Controls.Add(Me.chk_terminal_location)
    Me.panel_filter.Controls.Add(Me.lbl_payout)
    Me.panel_filter.Controls.Add(Me.lbl_netwin)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Controls.Add(Me.uc_dsl)
    Me.panel_filter.Size = New System.Drawing.Size(1226, 190)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dsl, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_netwin, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_payout, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_terminal_location, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.Uc_combo_games, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_exclude_rows_without_activity, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_payout_variance_threshold, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 194)
    Me.panel_data.Size = New System.Drawing.Size(1226, 514)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1220, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1220, 4)
    '
    'uc_pr_list
    '
    Me.uc_pr_list.Location = New System.Drawing.Point(270, 5)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(338, 188)
    Me.uc_pr_list.TabIndex = 1
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.ClosingTimeEnabled = True
    Me.uc_dsl.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.Location = New System.Drawing.Point(7, 8)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.ShowBorder = True
    Me.uc_dsl.Size = New System.Drawing.Size(257, 80)
    Me.uc_dsl.TabIndex = 0
    Me.uc_dsl.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    '
    'lbl_netwin
    '
    Me.lbl_netwin.AutoSize = True
    Me.lbl_netwin.ForeColor = System.Drawing.Color.Navy
    Me.lbl_netwin.Location = New System.Drawing.Point(610, 166)
    Me.lbl_netwin.Name = "lbl_netwin"
    Me.lbl_netwin.Size = New System.Drawing.Size(52, 13)
    Me.lbl_netwin.TabIndex = 13
    Me.lbl_netwin.Text = "xNetwin"
    '
    'lbl_payout
    '
    Me.lbl_payout.AutoSize = True
    Me.lbl_payout.ForeColor = System.Drawing.Color.Navy
    Me.lbl_payout.Location = New System.Drawing.Point(610, 147)
    Me.lbl_payout.Name = "lbl_payout"
    Me.lbl_payout.Size = New System.Drawing.Size(53, 13)
    Me.lbl_payout.TabIndex = 14
    Me.lbl_payout.Text = "xPayout"
    '
    'chk_terminal_location
    '
    Me.chk_terminal_location.Location = New System.Drawing.Point(614, 84)
    Me.chk_terminal_location.Name = "chk_terminal_location"
    Me.chk_terminal_location.Size = New System.Drawing.Size(296, 16)
    Me.chk_terminal_location.TabIndex = 4
    Me.chk_terminal_location.Text = "xShow terminals location"
    '
    'Uc_combo_games
    '
    Me.Uc_combo_games.Location = New System.Drawing.Point(601, 6)
    Me.Uc_combo_games.Name = "Uc_combo_games"
    Me.Uc_combo_games.Size = New System.Drawing.Size(272, 72)
    Me.Uc_combo_games.TabIndex = 3
    '
    'chk_exclude_rows_without_activity
    '
    Me.chk_exclude_rows_without_activity.AutoSize = True
    Me.chk_exclude_rows_without_activity.Location = New System.Drawing.Point(614, 106)
    Me.chk_exclude_rows_without_activity.Name = "chk_exclude_rows_without_activity"
    Me.chk_exclude_rows_without_activity.Size = New System.Drawing.Size(194, 17)
    Me.chk_exclude_rows_without_activity.TabIndex = 5
    Me.chk_exclude_rows_without_activity.Text = "xExcludeRowsWithOutActivity"
    Me.chk_exclude_rows_without_activity.UseVisualStyleBackColor = True
    '
    'gb_payout_variance_threshold
    '
    Me.gb_payout_variance_threshold.Controls.Add(Me.lbl_payout_variance_threshold_color)
    Me.gb_payout_variance_threshold.Controls.Add(Me.chk_payout_variance_threshold)
    Me.gb_payout_variance_threshold.Controls.Add(Me.ef_payout_variance_threshold)
    Me.gb_payout_variance_threshold.Location = New System.Drawing.Point(7, 98)
    Me.gb_payout_variance_threshold.Name = "gb_payout_variance_threshold"
    Me.gb_payout_variance_threshold.Size = New System.Drawing.Size(257, 62)
    Me.gb_payout_variance_threshold.TabIndex = 15
    Me.gb_payout_variance_threshold.TabStop = False
    Me.gb_payout_variance_threshold.Text = "GroupBox1"
    '
    'lbl_payout_variance_threshold_color
    '
    Me.lbl_payout_variance_threshold_color.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
    Me.lbl_payout_variance_threshold_color.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_payout_variance_threshold_color.Location = New System.Drawing.Point(28, 26)
    Me.lbl_payout_variance_threshold_color.Name = "lbl_payout_variance_threshold_color"
    Me.lbl_payout_variance_threshold_color.Size = New System.Drawing.Size(16, 16)
    Me.lbl_payout_variance_threshold_color.TabIndex = 4
    '
    'chk_payout_variance_threshold
    '
    Me.chk_payout_variance_threshold.AutoSize = True
    Me.chk_payout_variance_threshold.Location = New System.Drawing.Point(7, 26)
    Me.chk_payout_variance_threshold.Name = "chk_payout_variance_threshold"
    Me.chk_payout_variance_threshold.Size = New System.Drawing.Size(15, 14)
    Me.chk_payout_variance_threshold.TabIndex = 2
    Me.chk_payout_variance_threshold.UseVisualStyleBackColor = True
    '
    'ef_payout_variance_threshold
    '
    Me.ef_payout_variance_threshold.DoubleValue = 0.0R
    Me.ef_payout_variance_threshold.IntegerValue = 0
    Me.ef_payout_variance_threshold.IsReadOnly = False
    Me.ef_payout_variance_threshold.Location = New System.Drawing.Point(50, 20)
    Me.ef_payout_variance_threshold.Name = "ef_payout_variance_threshold"
    Me.ef_payout_variance_threshold.PlaceHolder = Nothing
    Me.ef_payout_variance_threshold.Size = New System.Drawing.Size(201, 24)
    Me.ef_payout_variance_threshold.SufixText = "Sufix Text"
    Me.ef_payout_variance_threshold.SufixTextVisible = True
    Me.ef_payout_variance_threshold.TabIndex = 3
    Me.ef_payout_variance_threshold.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_payout_variance_threshold.TextValue = ""
    Me.ef_payout_variance_threshold.TextWidth = 150
    Me.ef_payout_variance_threshold.Value = ""
    Me.ef_payout_variance_threshold.ValueForeColor = System.Drawing.Color.Blue
    '
    'frm_statistics_terminals
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(1234, 712)
    Me.Name = "frm_statistics_terminals"
    Me.Text = "frm_statistics_terminals"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_payout_variance_threshold.ResumeLayout(False)
    Me.gb_payout_variance_threshold.PerformLayout()
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Members "

  Private m_refreshing_grid As Boolean

  ' For report filters 
  Private m_game As String
  Private m_terminal As String
  Private m_date_from As String
  Private m_date_to As String
  Private m_rows As String
  Private m_variance_threshold As String

  ' For amount grid
  Dim total_played_amount As Decimal
  Dim total_won_amount As Decimal
  Dim total_terminal_count As Integer
  Dim total_played_count As Decimal
  Dim total_won_count As Decimal
  Dim total_theoretical_won_amount As Decimal
  Dim total_progressive_provision_amount As Decimal
  Dim total_non_progressive_jackpot_amount As Decimal
  Dim total_progressive_jackpot_amount As Decimal
  Dim total_progressive_jackpot_amount_0 As Decimal

  Private m_terminal_report_type As ReportType = ReportType.Provider
  Private m_refresh_grid As Boolean = False

#End Region ' Members

#Region " Constants "

  Private Const SQL_COLUMN_TERMINAL_ID As Integer = 0
  Private Const SQL_COLUMN_TERMINAL_NAME As Integer = 1
  Private Const SQL_COLUMN_PLAYED_AMOUNT As Integer = 2
  Private Const SQL_COLUMN_WON_AMOUNT As Integer = 3
  Private Const SQL_COLUMN_AMOUNT_PER_CENT As Integer = 4
  Private Const SQL_COLUMN_NETWIN As Integer = 5
  Private Const SQL_COLUMN_NETWIN_PER_CENT As Integer = 6
  Private Const SQL_COLUMN_PLAYED_COUNT As Integer = 7
  Private Const SQL_COLUMN_WON_COUNT As Integer = 8
  Private Const SQL_COLUMN_COUNT_PER_CENT As Integer = 9
  Private Const SQL_COLUMN_PROVIDER_NAME As Integer = 10
  Private Const SQL_COLUMN_MIN_GAME_ID As Integer = 11
  Private Const SQL_COLUMN_MAX_GAME_ID As Integer = 12
  Private Const SQL_COLUMN_MIN_GAME_NAME As Integer = 13
  Private Const SQL_COLUMN_THEORICAL_AMOUNT_PER_CENT As Integer = 14
  Private Const SQL_COLUMN_THEORICAL_AMOUNT As Integer = 15
  Private Const SQL_COLUMN_PROGRESSIVE_PROVISION_AMOUNT As Integer = 16
  Private Const SQL_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT As Integer = 17
  Private Const SQL_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT As Integer = 18
  Private Const SQL_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT_0 As Integer = 19
  Private Const SQL_COLUMN_TERM_DAY As Integer = 20
  Private Const SQL_COLUMN_TERM_DAY_MASTER As Integer = 21
  Private Const SQL_COLUMN_EXCLUDE_TERMINAL_ID As Integer = 22

  Private TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(m_terminal_report_type)

  Private GRID_COLUMN_INDEX As Integer
  Private GRID_COLUMN_DATE_FROM As Integer
  Private GRID_COLUMN_DATE_TO As Integer

  Private GRID_INIT_TERMINAL_DATA As Integer

  Private GRID_COLUMN_GAME_NAME As Integer
  Private GRID_COLUMN_TERM_DAY As Integer
  Private GRID_COLUMN_PLAYED_AMOUNT As Integer
  Private GRID_COLUMN_WON_AMOUNT As Integer
  Private GRID_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT As Integer
  Private GRID_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT As Integer
  Private GRID_COLUMN_PROGRESSIVE_PROVISION_AMOUNT As Integer
  Private GRID_COLUMN_AMOUNT_PER_CENT As Integer
  Private GRID_COLUMN_THEORICAL_AMOUNT_PER_CENT As Integer
  Private GRID_COLUMN_THEORICAL_AMOUNT_VARIATION As Integer
  Private GRID_COLUMN_NETWIN As Integer
  Private GRID_COLUMN_NETWIN_PER_CENT As Integer
  Private GRID_COLUMN_PLAYED_COUNT As Integer
  Private GRID_COLUMN_WON_COUNT As Integer
  Private GRID_COLUMN_COUNT_PER_CENT As Integer
  Private GRID_COLUMN_AVERAGE_BET As Integer
  Private GRID_COLUMN_NW_PER_TERMINAL As Integer
  Private GRID_COLUMN_PLAYED_PER_TERMINAL As Integer

  Private GRID_COLUMNS As Integer
  Private Const GRID_HEADER_ROWS As Integer = 2

  'Counters
  Private Const COUNTER_DEVICE_ERROR As Integer = 1
  Private Const COUNTER_DEVICE_WARNING As Integer = 2
  Private Const COUNTER_DEVICE_OK As Integer = 3
  Private Const COUNTER_OPERATIONS As Integer = 4

  Private Const COLOR_DEVICE_OK_BACK = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_DEVICE_OK_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_DEVICE_ERROR_BACK = ENUM_GUI_COLOR.GUI_COLOR_RED_02
  Private Const COLOR_DEVICE_ERROR_FORE = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_DEVICE_WARNING_BACK = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
  Private Const COLOR_DEVICE_WARNING_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_OPERATION_BACK = ENUM_GUI_COLOR.GUI_COLOR_GREY_01
  Private Const COLOR_OPERATION_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00

#End Region ' Constants

#Region " OVERRIDES "

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_STATISTICS_TERMINALS

    ' TJG 01-FEB-2005
    ' Set the form icon from the embedded resource (ICO file must be built in the project as "Embedded Resource")
    ' Call could be made as GetManifestResourceStream("GUI_Auditor.GUI_Auditor.ico"))
    'Me.Icon = New Icon(System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream(Me.GetType(), "WigosGUI.ico"))

    '------------------------------------------------
    'XVV 13/04/2007
    'Call Base Form proc
    Call MyBase.GUI_SetFormId()
    '------------------------------------------------

  End Sub ' GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_STATISTICS.GetString(201) + " " + GLB_NLS_GUI_STATISTICS.GetString(350) ' Estad�sticas Agrupadas por Terminal

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    ' Date time filter
    Me.uc_dsl.Init(GLB_NLS_GUI_INVOICING.Id(201), True)
    Me.uc_dsl.ClosingTimeEnabled = False

    ' Progressives
    Me.lbl_payout.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5400)
    Me.lbl_netwin.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5349)

    ' Theoretical payout variation limit
    Me.lbl_payout_variance_threshold_color.BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_01)
    Me.gb_payout_variance_threshold.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6921)
    Me.ef_payout_variance_threshold.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6922)
    Me.ef_payout_variance_threshold.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 9)

    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

    ' Set combo with Games
    Call Me.Uc_combo_games.Init()

    ' Providers - Terminals
    Call Me.uc_pr_list.Init(WSI.Common.Misc.GamingTerminalTypeList())

    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)

    Me.chk_exclude_rows_without_activity.Text = GLB_NLS_GUI_INVOICING.GetString(464)

  End Sub ' GUI_InitControls

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
    Call Me.uc_pr_list.SetDefaultValues()
  End Sub ' GUI_FilterReset

  Protected Overrides Sub GUI_BeforeFirstRow()

    total_played_amount = 0
    total_won_amount = 0
    total_terminal_count = 0
    total_played_count = 0
    total_won_count = 0
    total_theoretical_won_amount = 0
    total_progressive_provision_amount = 0
    total_non_progressive_jackpot_amount = 0
    total_progressive_jackpot_amount = 0
    total_progressive_jackpot_amount_0 = 0

    If m_refresh_grid Then
      Call GUI_StyleSheet()
    End If

    m_refresh_grid = False

  End Sub ' GUI_BeforeFirsRow

  Protected Overrides Sub GUI_AfterLastRow()

    Me.Grid.AddRow()

    Dim _idx_row As Integer = Me.Grid.NumRows - 1
    Dim _amount_per_cent As Decimal
    Dim _netwin_per_cent As Decimal
    Dim _netwin As Decimal = total_played_amount - (total_won_amount + total_progressive_provision_amount - total_progressive_jackpot_amount_0)
    Dim _count_per_cent As Decimal
    Dim _total_average_bet As Decimal
    Dim _theoretical_amount_per_cent As Decimal

    _total_average_bet = 0

    ' Terminal
    Me.Grid.Cell(_idx_row, GRID_COLUMN_DATE_FROM).Value = GLB_NLS_GUI_STATISTICS.GetString(203)

    ' Played Amount
    Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_AMOUNT).Value = GUI_FormatCurrency(total_played_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Won Amount
    Me.Grid.Cell(_idx_row, GRID_COLUMN_WON_AMOUNT).Value = GUI_FormatCurrency(total_won_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Non progressive Jackpot amount
    Me.Grid.Cell(_idx_row, GRID_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT).Value = GUI_FormatCurrency(total_non_progressive_jackpot_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Progressive Jackpot amount
    Me.Grid.Cell(_idx_row, GRID_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT).Value = GUI_FormatCurrency(total_progressive_jackpot_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Progressive provision amount
    Me.Grid.Cell(_idx_row, GRID_COLUMN_PROGRESSIVE_PROVISION_AMOUNT).Value = GUI_FormatCurrency(total_progressive_provision_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Amount Per cent
    If total_played_amount = 0 Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_AMOUNT_PER_CENT).Value = ""
    Else
      _amount_per_cent = ((total_won_amount + total_progressive_provision_amount - total_progressive_jackpot_amount_0) / total_played_amount) * 100
      Me.Grid.Cell(_idx_row, GRID_COLUMN_AMOUNT_PER_CENT).Value = GUI_FormatNumber(_amount_per_cent, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"
    End If

    ' Theoretical Amount Per cent
    If total_theoretical_won_amount = 0 Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_THEORICAL_AMOUNT_PER_CENT).Value = ""
    Else
      If total_played_amount = 0 Then
        Me.Grid.Cell(_idx_row, GRID_COLUMN_THEORICAL_AMOUNT_PER_CENT).Value = ""
      Else
        _theoretical_amount_per_cent = (total_theoretical_won_amount / total_played_amount) * 100
        Me.Grid.Cell(_idx_row, GRID_COLUMN_THEORICAL_AMOUNT_PER_CENT).Value = GUI_FormatNumber(_theoretical_amount_per_cent, 2) & "%"
      End If
    End If

    ' Netwin
    Me.Grid.Cell(_idx_row, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(_netwin, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Netwin per cent
    If total_played_amount = 0 Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_NETWIN_PER_CENT).Value = ""
    Else
      _netwin_per_cent = _netwin * 100 / total_played_amount
      Me.Grid.Cell(_idx_row, GRID_COLUMN_NETWIN_PER_CENT).Value = GUI_FormatNumber(_netwin_per_cent, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"
    End If

    ' Played Count
    Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_COUNT).Value = GUI_FormatNumber(total_played_count, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)

    ' Won Count
    Me.Grid.Cell(_idx_row, GRID_COLUMN_WON_COUNT).Value = GUI_FormatNumber(total_won_count, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)

    ' Count per cent
    If total_played_count = 0 Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_COUNT_PER_CENT).Value = ""
    Else
      _count_per_cent = (total_won_count / total_played_count) * 100
      Me.Grid.Cell(_idx_row, GRID_COLUMN_COUNT_PER_CENT).Value = GUI_FormatNumber(_count_per_cent, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"
    End If

    ' Average bet
    If total_played_count = 0 Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_AVERAGE_BET).Value = ""
    Else
      _total_average_bet = Math.Round(total_played_amount / total_played_count, 2, MidpointRounding.AwayFromZero)

      If _total_average_bet = 0 Then
        Me.Grid.Cell(_idx_row, GRID_COLUMN_AVERAGE_BET).Value = ""
      Else
        Me.Grid.Cell(_idx_row, GRID_COLUMN_AVERAGE_BET).Value = GUI_FormatCurrency(_total_average_bet, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End If
    End If

    ' Term x Day
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TERM_DAY).Value = GUI_FormatNumber(total_terminal_count, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)

    ' Count Netwin per terminal    
    If total_terminal_count = 0 Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_NW_PER_TERMINAL).Value = GUI_FormatCurrency(_netwin / 1, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)   ' LTC 2017-MAY-24
    Else
      Me.Grid.Cell(_idx_row, GRID_COLUMN_NW_PER_TERMINAL).Value = GUI_FormatCurrency(_netwin / total_terminal_count, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' Count Played per terminal
    If total_terminal_count = 0 Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_PER_TERMINAL).Value = GUI_FormatCurrency(total_played_amount / 1, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)   ' LTC 2017-MAY-24
    Else
      Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_PER_TERMINAL).Value = GUI_FormatCurrency(total_played_amount / total_terminal_count, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' Color Row
    Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

  End Sub ' GUI_AfterLastRow

  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate > Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _str_sql As StringBuilder

    _str_sql = New StringBuilder()

    _str_sql.AppendLine(GetSqlQueryTerminalsConnected())
    'Filter PayoutVarianceThreshold
    If (chk_payout_variance_threshold.Checked AndAlso Not String.IsNullOrEmpty(ef_payout_variance_threshold.Value.Trim())) Then
      _str_sql.AppendLine("SELECT * FROM ( ")
    End If
    _str_sql.AppendLine(" SELECT   SPH_TERMINAL_ID, ")
    _str_sql.AppendLine("          Terminal, ")
    _str_sql.AppendLine("          PlayedAmount,")
    _str_sql.AppendLine("          WonAmount, ")
    _str_sql.AppendLine("          CASE x.PlayedAmount WHEN 0 THEN 0 ELSE ((WonAmount +  ProgressiveProvisionAmount - ProgressiveJackpotAmount0)*100/x.PlayedAmount) END as AmountPerCent, ")
    _str_sql.AppendLine("          PlayedAmount-(WonAmount +  ProgressiveProvisionAmount - ProgressiveJackpotAmount0) as Netwin, ")
    _str_sql.AppendLine("          CASE x.PlayedAmount WHEN 0 THEN 0 ELSE ((PlayedAmount-(WonAmount +  ProgressiveProvisionAmount - ProgressiveJackpotAmount0))*100/x.PlayedAmount) END as NetwinPc, ")
    _str_sql.AppendLine("          PlayedCount, ")
    _str_sql.AppendLine("          WonCount, ")
    _str_sql.AppendLine("          CASE x.PlayedCount WHEN 0 THEN 0 ELSE((x.WonCount* 100.0)/x.PlayedCount) END as CountPerCent, ")
    _str_sql.AppendLine("          Provider, ")
    _str_sql.AppendLine("          ISNULL(MinGameId,-1) AS MinGameId, ")
    _str_sql.AppendLine("          ISNULL(MaxGameId,-1) AS MaxGameId, ")
    _str_sql.AppendLine("          MinGameName, ")
    _str_sql.AppendLine("          CASE x.PlayedAmount WHEN 0 THEN 0 ELSE(x.TheoreticalWonAmount*100/PlayedAmount) END AS TheoreticalAmountPc, ")
    _str_sql.AppendLine("          TheoreticalWonAmount, ")
    _str_sql.AppendLine("          ProgressiveProvisionAmount, ")
    _str_sql.AppendLine("          NonProgressiveJackpotAmount, ")
    _str_sql.AppendLine("          ProgressiveJackpotAmount, ")
    _str_sql.AppendLine("          ProgressiveJackpotAmount0, ")
    _str_sql.AppendLine("          TerminalCount, ")
    _str_sql.AppendLine("          TerminalCountMaster, ")
    _str_sql.AppendLine("          TE_TERMINAL_ID ")
    _str_sql.AppendLine("   FROM ")
    _str_sql.AppendLine("     (SELECT   TE_TERMINAL_ID, ")
    _str_sql.AppendLine("               SALES_PER_HOUR_V2.SPH_TERMINAL_ID,")
    _str_sql.AppendLine("               sum(CAST(SPH_PLAYED_AMOUNT AS DECIMAL(22,4)))as PlayedAmount, ")
    _str_sql.AppendLine("               sum(CAST(SPH_WON_AMOUNT AS DECIMAL(22,4))) AS WonAmount, ")
    _str_sql.AppendLine("               CASE WHEN SUM(SPH_WON_COUNT)>SUM(SPH_PLAYED_COUNT) THEN SUM(SPH_WON_COUNT) ELSE SUM(SPH_PLAYED_COUNT) END as PlayedCount, ")
    _str_sql.AppendLine("               sum(SPH_WON_COUNT) AS WonCount, ")
    _str_sql.AppendLine("               sum(ISNULL(SPH_PROGRESSIVE_PROVISION_AMOUNT,0)) AS ProgressiveProvisionAmount, ")
    _str_sql.AppendLine("               sum(ISNULL(SPH_JACKPOT_AMOUNT,0) - ISNULL(SPH_PROGRESSIVE_JACKPOT_AMOUNT,0)) AS NonProgressiveJackpotAmount, ")
    _str_sql.AppendLine("               sum(ISNULL(SPH_PROGRESSIVE_JACKPOT_AMOUNT,0)) AS ProgressiveJackpotAmount, ")
    _str_sql.AppendLine("               sum(ISNULL(SPH_PROGRESSIVE_JACKPOT_AMOUNT_0,0)) AS ProgressiveJackpotAmount0, ")
    _str_sql.AppendLine("               TE_NAME AS Terminal, ")
    _str_sql.AppendLine("               TE_PROVIDER_ID AS Provider, ")
    _str_sql.AppendLine("               min(SPH_GAME_ID) AS MinGameId, ")
    _str_sql.AppendLine("               max(SPH_GAME_ID) AS MaxGameId, ")
    _str_sql.AppendLine("               ISNULL((SELECT PG_GAME_NAME AS GM_NAME FROM PROVIDERS_GAMES WHERE PG_GAME_ID = min(SPH_GAME_ID)),'UNKNOWN') AS MinGameName, ")
    _str_sql.AppendLine("               sum(SPH_THEORETICAL_WON_AMOUNT) AS TheoreticalWonAmount,  ")
    _str_sql.AppendLine("               TMP_NUM_CONNECTED AS TerminalCount, ")
    _str_sql.AppendLine("               CASE WHEN TMP_NUM_CONNECTED IS NULL                                  ")
    _str_sql.AppendLine("                    	    THEN CASE WHEN TC.TMP_MASTER_ID IS NULL THEN 0 ELSE 1 END    ")
    _str_sql.AppendLine("                    	    ELSE TMP_NUM_CONNECTED                                         ")
    _str_sql.AppendLine("                     END AS TerminalCountMaster   ")
    _str_sql.AppendLine("        FROM   TERMINALS ")
    _str_sql.AppendLine(GetSqlWhere())
    'Filter PayoutVarianceThreshold
    If (chk_payout_variance_threshold.Checked AndAlso Not String.IsNullOrEmpty(ef_payout_variance_threshold.Value.Trim())) Then
      _str_sql.AppendLine(" ) t  WHERE ABS(AmountPerCent - TheoreticalAmountPc) > " + ef_payout_variance_threshold.Value)
    End If
    _str_sql.AppendLine("  ORDER BY   Provider, Terminal")


    _str_sql.AppendLine(" ")
    _str_sql.AppendLine(" DROP TABLE #TMP_CONNECTED ")

    Return _str_sql.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim min_game_id As Integer
    Dim max_game_id As Integer
    Dim min_game_name As String
    Dim _average_bet As Decimal
    Dim _payout_variance_threshold As Decimal
    Dim _terminal_data As List(Of Object)

    _average_bet = 0

    ' Date from
    If m_date_from <> "" Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_FROM).Value = GUI_FormatDate(m_date_from, _
                                                                           ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                           ENUM_FORMAT_TIME.FORMAT_HHMM)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_FROM).Value = ""
    End If

    ' Date to
    If m_date_to <> "" Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_TO).Value = GUI_FormatDate(m_date_to, _
                                                                         ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                         ENUM_FORMAT_TIME.FORMAT_HHMM)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_TO).Value = ""
    End If

    ' Terminal Report
    If Not DbRow.IsNull(SQL_COLUMN_EXCLUDE_TERMINAL_ID) Then
      _terminal_data = TerminalReport.GetReportDataList(DbRow.Value(SQL_COLUMN_EXCLUDE_TERMINAL_ID), m_terminal_report_type)
    Else
      _terminal_data = New List(Of Object)()
    End If
    For _idx As Int32 = 0 To _terminal_data.Count - 1
      If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value Then
        Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
      End If
    Next

    ' Term x Day
    If Not DbRow.IsNull(SQL_COLUMN_TERM_DAY) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERM_DAY).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_TERM_DAY), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
      total_terminal_count += DbRow.Value(SQL_COLUMN_TERM_DAY)
    ElseIf Not DbRow.IsNull(SQL_COLUMN_TERM_DAY_MASTER) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERM_DAY).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_TERM_DAY_MASTER), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
    End If

    ' Include/exclude rows without activity
    If Not DbRow.IsNull(SQL_COLUMN_TERMINAL_ID) Then
      ' Game
      min_game_id = DbRow.Value(SQL_COLUMN_MIN_GAME_ID)
      max_game_id = DbRow.Value(SQL_COLUMN_MAX_GAME_ID)
      min_game_name = DbRow.Value(SQL_COLUMN_MIN_GAME_NAME)

      If min_game_id = max_game_id Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_NAME).Value = min_game_name
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(634) '"Multijuego"
      End If

      ' PlayedAmount
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT))
      total_played_amount += DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT)

      ' WonAmount
      Me.Grid.Cell(RowIndex, GRID_COLUMN_WON_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_WON_AMOUNT))
      total_won_amount += DbRow.Value(SQL_COLUMN_WON_AMOUNT)

      ' AmountPerCent
      If DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT) = 0.0 Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_AMOUNT_PER_CENT).Value = String.Empty
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_AMOUNT_PER_CENT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_AMOUNT_PER_CENT), _
                                                                                     ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"
      End If

      'TheoricalAmount
      total_theoretical_won_amount += DbRow.Value(SQL_COLUMN_THEORICAL_AMOUNT)

      'TheoricalAmountPerCent
      Me.Grid.Cell(RowIndex, GRID_COLUMN_THEORICAL_AMOUNT_PER_CENT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_THEORICAL_AMOUNT_PER_CENT), _
                                                                                                         ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"

      'PayoutVarianceThreshold
      If DbRow.IsNull(SQL_COLUMN_AMOUNT_PER_CENT) Or DbRow.IsNull(SQL_COLUMN_THEORICAL_AMOUNT_PER_CENT) Then
        _payout_variance_threshold = 0
        Me.Grid.Cell(RowIndex, GRID_COLUMN_THEORICAL_AMOUNT_VARIATION).Value = ""
        Me.Grid.Cell(RowIndex, GRID_COLUMN_THEORICAL_AMOUNT_VARIATION).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_THEORICAL_AMOUNT_VARIATION).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
      Else
        _payout_variance_threshold = DbRow.Value(SQL_COLUMN_AMOUNT_PER_CENT) - DbRow.Value(SQL_COLUMN_THEORICAL_AMOUNT_PER_CENT)

        If Not Me.chk_payout_variance_threshold.Checked And (Math.Abs(_payout_variance_threshold) > GUI_ParseNumber(ef_payout_variance_threshold.Value)) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_THEORICAL_AMOUNT_VARIATION).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_01)
          Me.Grid.Cell(RowIndex, GRID_COLUMN_INDEX).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_01)
          Me.Grid.Cell(RowIndex, GRID_COLUMN_THEORICAL_AMOUNT_VARIATION).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_INDEX).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
          Me.Grid.Cell(RowIndex, GRID_COLUMN_THEORICAL_AMOUNT_VARIATION).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
          Me.Grid.Cell(RowIndex, GRID_COLUMN_THEORICAL_AMOUNT_VARIATION).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
        End If

        Me.Grid.Cell(RowIndex, GRID_COLUMN_THEORICAL_AMOUNT_VARIATION).Value = GUI_FormatNumber(_payout_variance_threshold, _
                                                                                                      ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"
      End If




      ' Non progressive Jackpot amount
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT))
      total_non_progressive_jackpot_amount += DbRow.Value(SQL_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT)

      ' Progressive Jackpot amount
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT))
      total_progressive_jackpot_amount += DbRow.Value(SQL_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT)
      total_progressive_jackpot_amount_0 += DbRow.Value(SQL_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT_0)


      ' Progressive provision amount
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROGRESSIVE_PROVISION_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PROGRESSIVE_PROVISION_AMOUNT))
      total_progressive_provision_amount += DbRow.Value(SQL_COLUMN_PROGRESSIVE_PROVISION_AMOUNT)

      ' Netwin
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_NETWIN))

      ' Netwin PerCent
      If DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT) = 0.0 Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN_PER_CENT).Value = String.Empty
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN_PER_CENT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_NETWIN_PER_CENT), _
                                                                                     ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"
      End If

      ' PlayedCount
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED_COUNT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_PLAYED_COUNT), _
                                                                                ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
      total_played_count += DbRow.Value(SQL_COLUMN_PLAYED_COUNT)

      ' WonCount
      Me.Grid.Cell(RowIndex, GRID_COLUMN_WON_COUNT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_WON_COUNT), _
                                                                             ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
      total_won_count += DbRow.Value(SQL_COLUMN_WON_COUNT)

      ' CountPerCent
      If DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT) = 0.0 Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_COUNT_PER_CENT).Value = String.Empty
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_COUNT_PER_CENT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_COUNT_PER_CENT), _
                                                                                    ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"
      End If

      ' Average bet
      If Not DbRow.IsNull(SQL_COLUMN_PLAYED_COUNT) And Not DbRow.IsNull(SQL_COLUMN_PLAYED_AMOUNT) Then
        If DbRow.Value(SQL_COLUMN_PLAYED_COUNT) = 0 Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_AVERAGE_BET).Value = ""
        Else
          _average_bet = Math.Round(DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT) / DbRow.Value(SQL_COLUMN_PLAYED_COUNT), 2, MidpointRounding.AwayFromZero)

          If _average_bet = 0 Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_AVERAGE_BET).Value = ""
          Else
            Me.Grid.Cell(RowIndex, GRID_COLUMN_AVERAGE_BET).Value = GUI_FormatCurrency(_average_bet, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
          End If
        End If
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_AVERAGE_BET).Value = ""
      End If

      ' Netwin per terminals
      If Not DbRow.IsNull(SQL_COLUMN_NETWIN) AndAlso Me.Grid.Cell(RowIndex, GRID_COLUMN_TERM_DAY).Value <> String.Empty Then
        If Me.Grid.Cell(RowIndex, GRID_COLUMN_TERM_DAY).Value = 0 Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_NW_PER_TERMINAL).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_NETWIN) / 1)    ' LTC 2017-MAY-24
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_NW_PER_TERMINAL).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_NETWIN) / Me.Grid.Cell(RowIndex, GRID_COLUMN_TERM_DAY).Value)
        End If
      End If

      ' Played per terminals
      If Not DbRow.IsNull(SQL_COLUMN_PLAYED_AMOUNT) AndAlso Me.Grid.Cell(RowIndex, GRID_COLUMN_TERM_DAY).Value <> String.Empty Then
        If Me.Grid.Cell(RowIndex, GRID_COLUMN_TERM_DAY).Value = 0 Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED_PER_TERMINAL).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT) / 1)   ' LTC 2017-MAY-24
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED_PER_TERMINAL).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT) / Me.Grid.Cell(RowIndex, GRID_COLUMN_TERM_DAY).Value)
        End If
      End If

    End If
    Return True

  End Function ' GUI_SetupRow 

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.uc_dsl
  End Sub ' GUI_SetInitialFocus

#Region " GUI Reports "

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_STATISTICS.GetString(309), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_STATISTICS.GetString(310), m_date_to)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_terminal)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(215), m_game)
    PrintData.SetFilter(chk_exclude_rows_without_activity.Text, m_rows)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6923), m_variance_threshold)

    PrintData.FilterValueWidth(1) = 3000
    PrintData.FilterValueWidth(2) = 3000

  End Sub ' GUI_ReportFilter

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)

    PrintData.Params.Title = GLB_NLS_GUI_STATISTICS.GetString(359)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_game = ""
    m_terminal = ""
    m_date_from = ""
    m_date_to = ""
    m_rows = ""
    m_variance_threshold = ""

    ' Date
    If Me.uc_dsl.FromDateSelected Then
      m_date_from = GUI_FormatDate(Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime), _
                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    If Me.uc_dsl.ToDateSelected Then
      m_date_to = GUI_FormatDate(Me.uc_dsl.ToDate.AddHours(Me.uc_dsl.ClosingTime), _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    ' Terminals 
    m_terminal = Me.uc_pr_list.GetTerminalReportText()

    ' Game
    If Me.Uc_combo_games.GetSelectedId() = String.Empty Then
      m_game = Me.Uc_combo_games.opt_all_game.Text
    Else
      m_game = Me.Uc_combo_games.GetSelectedValue
    End If

    ' Rows
    If chk_exclude_rows_without_activity.Checked Then
      m_rows = GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_rows = GLB_NLS_GUI_INVOICING.GetString(480)
    End If

    m_variance_threshold = ef_payout_variance_threshold.Value

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region  ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS: 
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    Dim _terminal_columns As List(Of ColumnSettings)

    Call GridColumnReIndex()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      .Counter(COUNTER_DEVICE_OK).Visible = False
      .Counter(COUNTER_DEVICE_OK).BackColor = GetColor(COLOR_DEVICE_OK_BACK)

      .Counter(COUNTER_DEVICE_ERROR).Visible = False
      .Counter(COUNTER_DEVICE_ERROR).BackColor = GetColor(COLOR_DEVICE_ERROR_BACK)
      .Counter(COUNTER_DEVICE_ERROR).ForeColor = GetColor(COLOR_DEVICE_ERROR_FORE)

      .Counter(COUNTER_DEVICE_WARNING).Visible = False
      .Counter(COUNTER_DEVICE_WARNING).BackColor = GetColor(COLOR_DEVICE_WARNING_BACK)

      .Counter(COUNTER_OPERATIONS).Visible = False
      .Counter(COUNTER_OPERATIONS).BackColor = GetColor(COLOR_OPERATION_BACK)

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Date from
      .Column(GRID_COLUMN_DATE_FROM).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(204)
      .Column(GRID_COLUMN_DATE_FROM).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(309)
      .Column(GRID_COLUMN_DATE_FROM).Width = 1700
      .Column(GRID_COLUMN_DATE_FROM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Date to
      .Column(GRID_COLUMN_DATE_TO).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(204)
      .Column(GRID_COLUMN_DATE_TO).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(310)
      .Column(GRID_COLUMN_DATE_TO).Width = 1700
      .Column(GRID_COLUMN_DATE_TO).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Terminal Report
      _terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)
      For _idx As Int32 = 0 To _terminal_columns.Count - 1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(300)
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = _terminal_columns(_idx).Header1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = _terminal_columns(_idx).Width
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = _terminal_columns(_idx).Alignment
      Next

      '  Game Name
      .Column(GRID_COLUMN_GAME_NAME).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(300)
      .Column(GRID_COLUMN_GAME_NAME).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(215)
      .Column(GRID_COLUMN_GAME_NAME).Width = 1950
      .Column(GRID_COLUMN_GAME_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Term x Day
      .Column(GRID_COLUMN_TERM_DAY).Header(0).Text = ""
      .Column(GRID_COLUMN_TERM_DAY).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(269)
      .Column(GRID_COLUMN_TERM_DAY).Width = 1200
      .Column(GRID_COLUMN_TERM_DAY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      '  Played Amount
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Header(0).Text = ""
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6275) 'Coin In
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Width = 1250
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Won Amount
      .Column(GRID_COLUMN_WON_AMOUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(302)
      .Column(GRID_COLUMN_WON_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5391)
      .Column(GRID_COLUMN_WON_AMOUNT).Width = 1950
      .Column(GRID_COLUMN_WON_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Non progressive Jackpot amount
      .Column(GRID_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(307)
      .Column(GRID_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5293)
      .Column(GRID_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT).Width = 1600
      .Column(GRID_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Progressive Jackpot amount
      .Column(GRID_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(307)
      .Column(GRID_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5294)
      .Column(GRID_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT).Width = 1250
      .Column(GRID_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Progressive provision amount
      .Column(GRID_COLUMN_PROGRESSIVE_PROVISION_AMOUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(307)
      .Column(GRID_COLUMN_PROGRESSIVE_PROVISION_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5292)
      .Column(GRID_COLUMN_PROGRESSIVE_PROVISION_AMOUNT).Width = 1250
      .Column(GRID_COLUMN_PROGRESSIVE_PROVISION_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Amount Per Cent
      .Column(GRID_COLUMN_AMOUNT_PER_CENT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1924)
      .Column(GRID_COLUMN_AMOUNT_PER_CENT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(303)
      .Column(GRID_COLUMN_AMOUNT_PER_CENT).Width = 1250
      .Column(GRID_COLUMN_AMOUNT_PER_CENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Theorical Amount PerCent
      .Column(GRID_COLUMN_THEORICAL_AMOUNT_PER_CENT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1924)
      .Column(GRID_COLUMN_THEORICAL_AMOUNT_PER_CENT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1809)
      .Column(GRID_COLUMN_THEORICAL_AMOUNT_PER_CENT).Width = GRID_COLUMN_THEORICAL_PAYOUT_WIDTH
      .Column(GRID_COLUMN_THEORICAL_AMOUNT_PER_CENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Theorical Payout  PerCent variance  
      .Column(GRID_COLUMN_THEORICAL_AMOUNT_VARIATION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1924)
      .Column(GRID_COLUMN_THEORICAL_AMOUNT_VARIATION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6923)
      .Column(GRID_COLUMN_THEORICAL_AMOUNT_VARIATION).Width = GRID_COLUMN_THEORICAL_PAYOUT_WIDTH
      .Column(GRID_COLUMN_THEORICAL_AMOUNT_VARIATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT


      ' Netwin
      .Column(GRID_COLUMN_NETWIN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(539)
      .Column(GRID_COLUMN_NETWIN).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(311)
      .Column(GRID_COLUMN_NETWIN).Width = 1250
      .Column(GRID_COLUMN_NETWIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Netwin Per Cent
      .Column(GRID_COLUMN_NETWIN_PER_CENT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(539)
      .Column(GRID_COLUMN_NETWIN_PER_CENT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(312)
      .Column(GRID_COLUMN_NETWIN_PER_CENT).Width = 1250
      .Column(GRID_COLUMN_NETWIN_PER_CENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Played Count
      .Column(GRID_COLUMN_PLAYED_COUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(308)
      .Column(GRID_COLUMN_PLAYED_COUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(304)
      .Column(GRID_COLUMN_PLAYED_COUNT).Width = 950
      .Column(GRID_COLUMN_PLAYED_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Won Count
      .Column(GRID_COLUMN_WON_COUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(308)
      .Column(GRID_COLUMN_WON_COUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(305)
      .Column(GRID_COLUMN_WON_COUNT).Width = 950
      .Column(GRID_COLUMN_WON_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Count Per Cent
      .Column(GRID_COLUMN_COUNT_PER_CENT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(308)
      .Column(GRID_COLUMN_COUNT_PER_CENT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(316)
      .Column(GRID_COLUMN_COUNT_PER_CENT).Width = 820
      .Column(GRID_COLUMN_COUNT_PER_CENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Average bet
      .Column(GRID_COLUMN_AVERAGE_BET).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(308)
      .Column(GRID_COLUMN_AVERAGE_BET).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(347)
      .Column(GRID_COLUMN_AVERAGE_BET).Width = 1400
      .Column(GRID_COLUMN_AVERAGE_BET).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' NetWin Per Terminal
      .Column(GRID_COLUMN_NW_PER_TERMINAL).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(346)
      .Column(GRID_COLUMN_NW_PER_TERMINAL).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(311)
      .Column(GRID_COLUMN_NW_PER_TERMINAL).Width = 1250
      .Column(GRID_COLUMN_NW_PER_TERMINAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Played Per Terminal
      .Column(GRID_COLUMN_PLAYED_PER_TERMINAL).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(346)
      .Column(GRID_COLUMN_PLAYED_PER_TERMINAL).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(301)
      .Column(GRID_COLUMN_PLAYED_PER_TERMINAL).Width = 1250
      .Column(GRID_COLUMN_PLAYED_PER_TERMINAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

  End Sub 'GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Dim _closing_time As Integer
    Dim _final_time As Date
    Dim _now As Date

    _now = WSI.Common.WGDB.Now

    _closing_time = GetDefaultClosingTime()
    _final_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)
    If _final_time > _now Then
      _final_time = _final_time.AddDays(-1)
    End If

    _final_time = _final_time.Date
    Me.uc_dsl.ToDate = _final_time
    Me.uc_dsl.ToDateSelected = True
    Me.uc_dsl.FromDate = _final_time.AddDays(-1)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.ClosingTime = _closing_time

    Call Me.Uc_combo_games.SetDefaultValues()

    Me.chk_terminal_location.Checked = False
    Me.chk_exclude_rows_without_activity.Checked = True

    Me.chk_payout_variance_threshold.Checked = False
    Me.ef_payout_variance_threshold.Value = GeneralParam.GetDecimal("PlayerTracking", "PayoutVarianceThreshold", 3)

  End Sub ' SetDefaultValues

  ' PURPOSE: Get Sql WHERE to build SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String

    Dim _str_sql As StringBuilder
    Dim _str_dates As String = ""

    _str_sql = New StringBuilder()

    _str_sql.Append(IIf(Me.chk_exclude_rows_without_activity.Checked, " INNER JOIN ", " LEFT JOIN "))
    _str_sql.AppendLine(" SALES_PER_HOUR_V2 ON SALES_PER_HOUR_V2.SPH_TERMINAL_ID = TE_TERMINAL_ID ")

    ' Filter Dates
    _str_dates = Me.uc_dsl.GetSqlFilterCondition("SPH_BASE_HOUR")
    If _str_dates.Length > 0 Then
      _str_sql.AppendLine(" AND " & _str_dates)
    End If

    ' Filter Game
    If Me.Uc_combo_games.GetSelectedId <> String.Empty Then
      _str_sql.AppendLine(" AND SPH_GAME_ID = " & Me.Uc_combo_games.GetSelectedId)
    End If

    _str_sql.AppendLine(" LEFT JOIN  #TMP_CONNECTED ON TE_TERMINAL_ID = TMP_TERMINAL_ID  ")
    _str_sql.AppendLine(" LEFT JOIN	(SELECT	DISTINCT TMP_MASTER_ID FROM  #TMP_CONNECTED) AS TC ON TE_MASTER_ID = TC.TMP_MASTER_ID ")

    ' Filter Providers - Terminals
    _str_sql.AppendLine(" WHERE  TE_TERMINAL_ID IN " & Me.uc_pr_list.GetProviderIdListSelected())

    _str_sql.AppendLine(" GROUP BY TE_TERMINAL_ID, TE_NAME, TE_PROVIDER_ID, TMP_NUM_CONNECTED, TC.TMP_MASTER_ID, SALES_PER_HOUR_V2.SPH_TERMINAL_ID ) x")

    Return _str_sql.ToString()

  End Function ' GetSqlWhere

  ' PURPOSE: Get terminals connected
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlQueryTerminalsConnected() As String

    Dim _str_sql As StringBuilder

    _str_sql = New StringBuilder()

    _str_sql.AppendLine(" CREATE TABLE #TMP_CONNECTED ")
    _str_sql.AppendLine(" ( ")
    _str_sql.AppendLine("    TMP_TERMINAL_ID BIGINT, ")
    _str_sql.AppendLine("    TMP_NUM_CONNECTED BIGINT, ")
    _str_sql.AppendLine("    TMP_MASTER_ID BIGINT ")
    _str_sql.AppendLine(" ) ")
    _str_sql.AppendLine(" CREATE NONCLUSTERED INDEX IX_TMP_TC_ID ON #TMP_CONNECTED(TMP_TERMINAL_ID) ")
    _str_sql.AppendLine(" ")
    _str_sql.AppendLine(" INSERT INTO   #TMP_CONNECTED ")
    _str_sql.AppendLine("      SELECT   TC_TERMINAL_ID, ")
    _str_sql.AppendLine("               SUM(1), ")
    _str_sql.AppendLine("               TC_MASTER_ID ")
    _str_sql.AppendLine("        FROM   TERMINALS_CONNECTED ")
    _str_sql.AppendLine("       WHERE ")

    ' Filter date
    _str_sql.AppendLine(Me.uc_dsl.GetSqlFilterCondition("TC_DATE", False)) ' LTC 25-OCT-20126

    ' Filter Providers - Terminals
    _str_sql.AppendLine("   AND   TC_TERMINAL_ID IN  " & Me.uc_pr_list.GetProviderIdListSelected())

    _str_sql.AppendLine("   AND   TC_CONNECTED = 1 ")
    _str_sql.AppendLine("GROUP BY TC_TERMINAL_ID, TC_MASTER_ID ")

    Return _str_sql.ToString()

  End Function ' GetSqlQueryTerminalsConnected

  Private Sub GridColumnReIndex()

    TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(m_terminal_report_type)

    GRID_COLUMN_INDEX = 0
    GRID_COLUMN_DATE_FROM = 1
    GRID_COLUMN_DATE_TO = 2
    GRID_INIT_TERMINAL_DATA = 3
    GRID_COLUMN_GAME_NAME = TERMINAL_DATA_COLUMNS + 3
    GRID_COLUMN_TERM_DAY = TERMINAL_DATA_COLUMNS + 4
    GRID_COLUMN_PLAYED_AMOUNT = TERMINAL_DATA_COLUMNS + 5
    GRID_COLUMN_WON_AMOUNT = TERMINAL_DATA_COLUMNS + 6
    GRID_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT = TERMINAL_DATA_COLUMNS + 7
    GRID_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT = TERMINAL_DATA_COLUMNS + 8
    GRID_COLUMN_PROGRESSIVE_PROVISION_AMOUNT = TERMINAL_DATA_COLUMNS + 9
    GRID_COLUMN_AMOUNT_PER_CENT = TERMINAL_DATA_COLUMNS + 10
    GRID_COLUMN_THEORICAL_AMOUNT_PER_CENT = TERMINAL_DATA_COLUMNS + 11
    GRID_COLUMN_THEORICAL_AMOUNT_VARIATION = TERMINAL_DATA_COLUMNS + 12
    GRID_COLUMN_NETWIN = TERMINAL_DATA_COLUMNS + 13
    GRID_COLUMN_NETWIN_PER_CENT = TERMINAL_DATA_COLUMNS + 14
    GRID_COLUMN_PLAYED_COUNT = TERMINAL_DATA_COLUMNS + 15
    GRID_COLUMN_WON_COUNT = TERMINAL_DATA_COLUMNS + 16
    GRID_COLUMN_COUNT_PER_CENT = TERMINAL_DATA_COLUMNS + 17
    GRID_COLUMN_AVERAGE_BET = TERMINAL_DATA_COLUMNS + 18
    GRID_COLUMN_NW_PER_TERMINAL = TERMINAL_DATA_COLUMNS + 19
    GRID_COLUMN_PLAYED_PER_TERMINAL = TERMINAL_DATA_COLUMNS + 20

    GRID_COLUMNS = TERMINAL_DATA_COLUMNS + 21

  End Sub

#End Region  ' Private Functions

#Region " Events"

  Private Sub chk_terminal_location_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminal_location.CheckedChanged
    If chk_terminal_location.Checked Then
      m_terminal_report_type = ReportType.Provider + ReportType.Location
    Else
      m_terminal_report_type = ReportType.Provider
    End If

    m_refresh_grid = True
  End Sub

#End Region 'Events

End Class
