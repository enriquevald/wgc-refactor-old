<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_note_counter_events
  Inherits GUI_Controls.frm_base

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.dg_note_counter_event = New GUI_Controls.uc_grid
    Me.btn_exit = New GUI_Controls.uc_button
    Me.SuspendLayout()
    '
    'dg_note_counter_event
    '
    Me.dg_note_counter_event.CurrentCol = -1
    Me.dg_note_counter_event.CurrentRow = -1
    Me.dg_note_counter_event.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_note_counter_event.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_note_counter_event.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_note_counter_event.Location = New System.Drawing.Point(7, 7)
    Me.dg_note_counter_event.Name = "dg_note_counter_event"
    Me.dg_note_counter_event.PanelRightVisible = True
    Me.dg_note_counter_event.Redraw = True
    Me.dg_note_counter_event.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_note_counter_event.Size = New System.Drawing.Size(616, 356)
    Me.dg_note_counter_event.Sortable = False
    Me.dg_note_counter_event.SortAscending = True
    Me.dg_note_counter_event.SortByCol = 0
    Me.dg_note_counter_event.TabIndex = 0
    Me.dg_note_counter_event.ToolTipped = True
    Me.dg_note_counter_event.TopRow = -2
    '
    'btn_exit
    '
    Me.btn_exit.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_exit.Location = New System.Drawing.Point(629, 333)
    Me.btn_exit.Name = "btn_exit"
    Me.btn_exit.Size = New System.Drawing.Size(90, 30)
    Me.btn_exit.TabIndex = 1
    Me.btn_exit.ToolTipped = False
    Me.btn_exit.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'frm_note_counter_events
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(726, 368)
    Me.Controls.Add(Me.btn_exit)
    Me.Controls.Add(Me.dg_note_counter_event)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.KeyPreview = True
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Name = "frm_note_counter_events"
    Me.Text = "frm_note_counter_events"
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents dg_note_counter_event As GUI_Controls.uc_grid
  Friend WithEvents btn_exit As GUI_Controls.uc_button
End Class
