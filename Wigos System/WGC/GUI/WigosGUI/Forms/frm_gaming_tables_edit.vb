'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_gaming_tables_edit.vb
' DESCRIPTION:   Edit gaming tables
' AUTHOR:        Javier Barea
' CREATION DATE: 16-DEC-2013
'
' REVISION HISTORY:
'
' Date         Author  Description
' -----------  ------  -----------------------------------------------
' 16-DEC-2013  JBP     Initial version
' 24-FEB-2014  RRR     Added ValidateFormat
' 08-JUL-2014  DHA     Added gambling table design parameters
' 23-JUL-2014  DHA     Fixed bug #WIG-1116: Min bet can not be greater than max bet
' 23-JUL-2014  DHA     Fixed bug #WIG-1116: Min bet can not be greater than max bet
' 29-SEP-2014  LRS     Fixed bug #WIG-1235
' 30-SEP-2014  DHA     Fixed Bug #WIG-1348: error when created new gaming table
' 30-SEP-2014  DCS&DHA Fixed Bug #WIG-1355: error when edit gaming table
' 06-OCT-2014  DHA     Fixed Bug #WIG-1418: error when validate idle and speed fields
' 06-OCT-2014  DHA     Fixed Bug #WIG-1421: error when set enable/disable PlayerTracking, some fields are erased
' 09-OCT-2014  DHA     Fixed Bug #WIG-1454: validate if currency is selected
' 19-NOV-2014  SGB     Fixed Bug WIG-1712: Changes FloorId lenght
' 25-JAN-2016  RGR     Product Backlog Item 4951:Tables: Groups and MD: Groups of chips on the table
' 16-FEB-2016  EOR     Task 4951: Change Text GroupBox From Chips Groups to Chips
'                      Filter Change Name to Group
'                      Delete Number of Field Color
' 22-FEB-2016  RGR     Product Backlog Item 9339:Sprint Review 18 Winions - Mex
' 30-MAR-2016  LTC     Product Backlog Item 10936:Tables (Phase 1): Diferences Adjustment in Chips Screen
' 05-MAY-2016  RAB     PBI 9848: Management drop box (Phase 1)
' 13-JUN-2016  FOS     Product Backlog Item 13403:Mesas (Fase 3): GUI Modification (Fase 1)
' 29-JUN-2016  FOS     Product Backlog Item 13403:Tables: GUI modifications (Phase 1)
' 06-JUL-2016  FOS     Product Backlog Item 15070: Tables: GUI configuration (Phase 4)
' 25-JUL-2016  FOS     Product Backlog Item 15094: Tables: GUI configuration (Phase 4)
' 24-AGO-2016  FOS     Fixed Bug 16991: Tables: Min Bet and Max Bet fields without symbol.
' 08-SEP-2016  FOS     PBI 16373: Tables: Split PlayerTracking to GamingTables (Phase 5)
' 23-SEP-2016  FOS     Fixed Bug 17597: Tables: Error to create a new table 
' 27-SEP-2016  FOS     Fixed Bug 18126: Tables: Doesn't save changes when player tracking is disabled
' 29-SEP-2016  FOS     Fixed Bug 18126: Tables: Doesn't save changes when player tracking is disabled
' 30-SEP-2016  JML     Fixed Bug 17877: Gaming tables: First time do not save the template
' 10-OCT-2016  EOR     Fixed Bug 18666: Gaming tables: Exception Press button Cancel
' 16-DEC-2016  FOS     Fixed Bug 1689: Add Cashier name in the field of auditor data
' 06-FEB-2017  ATB     Fixed Bug 24227: Tables: an uncontrolled exception appears  when pressing Cancel button after a cashier selection at table's edition.
' 05-SEP-2017  DHA     PBI 28629:WIGOS-3350 MES22 - Linked gaming table configuration
' 22-JUN-2018  LQB     Bug 33282:WIGOS-13055 Tables with integrated cashier saved without seats 
' 04-JUL-2018  AGS     Bug 33430:WIGOS-12990 Gaming Tables edition - Confirmation pop up should not appear when clicking on Cancel button without changes made.
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonMisc
Imports System.Text
Imports WSI.Common

Public Class frm_gaming_tables_edit
  Inherits frm_base_edit

#Region "Constants"

  ' Fields length
  Private Const MAX_CODE_LEN As Integer = 20
  Private Const MAX_NAME_LEN As Integer = 50
  Private Const MAX_DESC_LEN As Integer = 512

  ' Tabs Grid Constants
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_NAME As Integer = 1
  Private Const GRID_COLUMN_ALLOWED As Integer = 2

  Private Const GRID_WIDTH_INDEX As Integer = 0
  Private Const GRID_WIDTH_NAME As Integer = 3360
  Private Const GRID_WIDTH_ALLOWED As Integer = 1200
  Private Const GRID_WIDTH_GAMING_TABLE_ID As Integer = 0

  Private Const GRID_COLUMNS As Integer = 3
  Private Const GRID_HEADER_ROWS As Integer = 1

  ' Private enabled chips datatable Constants
  Private Const DT_COLUMN_ID As Integer = 0
  Private Const DT_COLUMN_TYPE As Integer = 1
  Private Const DT_COLUMN_ISO_CODE As Integer = 2
  Private Const DT_COLUMN_NAME As Integer = 3
  Private Const DT_COLUMN_ALLOWED As Integer = 4
  Private Const DT_COLUMN_GAMING_TABLE_ID As Integer = 5
  Private Const OLD_CHIPS_DENOMINATION As String = "X01"

#End Region   ' Constants

#Region "Members"

  Private m_current_virtual_cashier_id As Integer
  Private WithEvents m_gridsList As List(Of GUI_Controls.uc_grid)
  Private m_chip_types_enabled As DataTable
  Private m_chip_sets As IDictionary(Of FeatureChips.ChipType, String)
  Private m_gaming_table As CLASS_GAMING_TABLE
  Private m_dt_bet_currencies As DataTable
#End Region     ' Members

#Region "Properties"

  Public Property VirtualCashierID() As Integer
    Get
      Return m_current_virtual_cashier_id
    End Get

    Set(ByVal value As Integer)
      Me.m_current_virtual_cashier_id = value
    End Set
  End Property

  Public ReadOnly Property SelectedCashierID() As Integer
    Get
      If Me.chk_integrated.Checked Then
        Return Me.cmb_cashiers.SelectedValue
      Else
        Return Me.VirtualCashierID
      End If

    End Get

  End Property

#End Region  ' Properties

#Region "Privates"

  ' PURPOSE: Cashier session status change when windows is open. 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Function CheckGamingTableStatus() As Boolean

    Dim _session_id As Int64
    m_gaming_table = Me.DbReadObject

    If Me.chk_enabled.Checked <> Me.m_gaming_table.Enabled _
     Or Me.chk_integrated.Checked <> Me.m_gaming_table.IntegratedCashier _
     Or Me.SelectedCashierID <> Me.m_gaming_table.CashierID Then

      Using DB_TRX As New DB_TRX
        ' Check Cage Pending Movements
        If m_gaming_table.ExistCagePendingMovements(DB_TRX.SqlTransaction) Then
          ' Warning Message
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4601), ENUM_MB_TYPE.MB_TYPE_WARNING)

          Return False

        End If

        ' Check Terminal sessions open
        If Cashier.ExistCashierTerminalSessionOpen(Me.m_gaming_table.CashierID, _session_id, DB_TRX.SqlTransaction) Then
          ' Warning Message
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4602), ENUM_MB_TYPE.MB_TYPE_WARNING)

          Return False

        End If

      End Using

    End If

    Return True

  End Function ' CheckGamingTableStatus

  ' PURPOSE: Combo Integrated Cashier checks selection
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Function CheckCashierSelected() As Boolean

    Dim _session_id As Int64
    m_gaming_table = Me.DbReadObject

    If Me.SelectedCashierID <> Me.m_gaming_table.CashierID Then

      Using DB_TRX As New DB_TRX
        ' Check Cage Pending Movements
        If m_gaming_table.ExistCagePendingMovements(DB_TRX.SqlTransaction) Then
          ' Warning Message
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4363), ENUM_MB_TYPE.MB_TYPE_WARNING)

          Return False

        End If
        ' Check Terminal sessions open
        If Cashier.ExistCashierTerminalSessionOpen(Me.cmb_cashiers.SelectedValue, _session_id, DB_TRX.SqlTransaction) Then
          ' Warning Message
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3438), ENUM_MB_TYPE.MB_TYPE_WARNING)

          Return False

        End If

      End Using

    End If

    Return True

  End Function ' CheckCashierSelected

  ' PURPOSE: Set Cashier
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub SetCashier()

    Call RemoveHandles()

    ' Integrated Cashier
    Me.chk_integrated.Checked = m_gaming_table.IntegratedCashier

    ' Cashier
    Call EnableDisableCmbCashiers()

    Call AddHandles()

  End Sub ' SetCashier

  ' PURPOSE: Add Handles
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub AddHandles()

    AddHandler Me.cmb_area.ValueChangedEvent, AddressOf cmb_area_ValueChangedEvent
    AddHandler Me.chk_integrated.CheckedChanged, AddressOf chk_integrated_CheckedChanged

  End Sub ' AddHandles

  ' PURPOSE: Remove Handles
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub RemoveHandles()

    RemoveHandler Me.cmb_area.ValueChangedEvent, AddressOf cmb_area_ValueChangedEvent
    RemoveHandler Me.chk_integrated.CheckedChanged, AddressOf chk_integrated_CheckedChanged

  End Sub ' RemoveHandles

  ' PURPOSE: Init table type combo box
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub DisableContorlsIfCashierSessionIsOpen()
    Dim _session_id As Int64
    m_gaming_table = Me.DbReadObject

    Me.chk_enabled.Enabled = True
    Me.gb_cashier_terminal.Enabled = True
    Me.lbl_status.Text = "---"
    Me.cmb_provider.Enabled = True
    Me.gb_area_island.Enabled = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = True
    Me.chk_management_drop_box.Enabled = True

    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT Then

      Me.lbl_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4409)  ' 4409 "Cerrada"

      Using _db_trx As New DB_TRX()
        If Cashier.ExistCashierTerminalSessionOpen(m_gaming_table.CashierID, _session_id, _db_trx.SqlTransaction) Then
          Me.chk_enabled.Enabled = False
          Me.gb_cashier_terminal.Enabled = False
          Me.cmb_provider.Enabled = False
          Me.gb_area_island.Enabled = False
          Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False
          Me.lbl_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4408)  ' 4408 "Abierta"
          Me.chk_management_drop_box.Enabled = False
        End If
      End Using
    End If
  End Sub ' DisableContorlsIfCashierSessionIsOpen

  ' PURPOSE: Init table type combo box
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub TableTypeComboFill()
    Dim _table As DataTable
    Dim _sb As StringBuilder

    _sb = New StringBuilder()

    _sb.AppendLine("  SELECT    GTT_GAMING_TABLE_TYPE_ID  ")
    _sb.AppendLine("          , GTT_NAME                  ")
    _sb.AppendLine("    FROM    GAMING_TABLES_TYPES       ")
    _sb.AppendLine("   WHERE    GTT_ENABLED  = 1          ")

    _table = GUI_GetTableUsingSQL(_sb.ToString(), Integer.MaxValue)

    Call InsertTableDefaultValue(_table, False)

    Me.cmb_table_type.Add(_table)

  End Sub     ' TableTypeComboFill

  ' PURPOSE: Init table type combo box
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub CashierComboFill()
    Dim _table As DataTable
    Dim _table_id As String
    Dim _cashier_id As String
    Dim _sb As StringBuilder

    m_gaming_table = Me.DbReadObject
    _table_id = m_gaming_table.GamingTableID.ToString()
    _cashier_id = m_gaming_table.CashierID.ToString()

    _sb = New StringBuilder()

    _sb.AppendLine("  SELECT    CT_CASHIER_ID                                                                       ")
    _sb.AppendLine("          , CT_NAME                                                                             ")
    _sb.AppendLine("    FROM    CASHIER_TERMINALS                                                                   ")
    _sb.AppendLine("   WHERE    CT_TERMINAL_ID IS NULL                                                              ")
    _sb.AppendLine("     AND    CT_GAMING_TABLE_ID IS NULL                                                          ")
    _sb.AppendLine("     AND    CT_CASHIER_ID NOT IN ( SELECT ISNULL(GT_CASHIER_ID, 0) FROM GAMING_TABLES           ")
    _sb.AppendLine("                                    WHERE GT_GAMING_TABLE_ID != " & _table_id & "               ")
    _sb.AppendLine("                                   UNION                                                        ")
    _sb.AppendLine("                                   SELECT ISNULL(GT_LINKED_CASHIER_ID, 0) FROM GAMING_TABLES    ")
    _sb.AppendLine("                                    WHERE GT_GAMING_TABLE_ID != " & _table_id & " )             ")
    _sb.AppendLine("ORDER BY    CT_NAME                                                                             ")

    _table = GUI_GetTableUsingSQL(_sb.ToString(), Integer.MaxValue)

    For _idx As Integer = 0 To _table.Rows.Count - 1
      _table.Rows(_idx)(1) = Computer.Alias(_table.Rows(_idx)(1))
    Next

    Call InsertTableDefaultValue(_table, False)

    Me.cmb_cashiers.DataSource = _table
    Me.cmb_cashiers.DisplayMember = "CT_NAME"
    Me.cmb_cashiers.ValueMember = "CT_CASHIER_ID"

  End Sub     ' CashierComboFill

  ' PURPOSE: Insert Table Default value
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub InsertTableDefaultValue(ByRef Table As DataTable, ByVal IsSimple As Boolean)
    Dim _dr As DataRow
    Dim _arr() As Object

    ' First combo item (Void)
    _dr = Table.NewRow()

    If IsSimple Then
      _arr = New Object() {""}
    Else
      _arr = New Object() {-1, ""}
    End If

    _dr.ItemArray() = _arr
    Table.Rows.InsertAt(_dr, 0)

  End Sub     ' InsertTableDefaultValue

  ' PURPOSE: Init areas combo box
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub AreaComboFill()
    Dim _table As DataTable

    _table = GUI_GetTableUsingSQL("SELECT AR_AREA_ID, AR_NAME FROM AREAS INNER JOIN BANKS ON BK_AREA_ID = AR_AREA_ID GROUP BY AR_AREA_ID, AR_NAME ORDER BY AR_NAME", Integer.MaxValue)

    Me.cmb_area.Clear()
    Me.cmb_area.Add(_table)

  End Sub     ' AreaComboFill

  ' PURPOSE: Init BANKS combo box
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub BankComboFill()
    Dim _table As DataTable

    _table = GUI_GetTableUsingSQL("SELECT BK_BANK_ID, BK_NAME FROM BANKS WHERE BK_AREA_ID = " & cmb_area.Value & " GROUP BY BK_BANK_ID, BK_NAME ORDER BY BK_NAME", Integer.MaxValue)

    Me.cmb_bank.Clear()
    Me.cmb_bank.Add(_table)

  End Sub     ' BankComboFill

  ' PURPOSE: Init providers combo box
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub ProvidersComboFill()
    Dim _table As DataTable

    _table = GUI_GetTableUsingSQL("SELECT PV_ID,  PV_NAME FROM PROVIDERS WHERE PV_HIDE = 0 AND NOT PV_NAME = 'UNKNOWN' ORDER BY PV_NAME", Integer.MaxValue)

    Me.cmb_provider.Clear()
    Me.cmb_provider.Add(_table)

  End Sub     ' ProvidersComboFill

  Private Sub OperationTypeComboFill()
    Dim _str_value As String
    _str_value = String.Empty
    For Each item As Object In [Enum].GetValues(GetType(WSI.Common.ClosingStocks.ClosingStockType))
      Select Case item
        Case WSI.Common.ClosingStocks.ClosingStockType.NONE
          _str_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7358)
        Case WSI.Common.ClosingStocks.ClosingStockType.FIXED
          _str_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7356)
        Case WSI.Common.ClosingStocks.ClosingStockType.ROLLING
          _str_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7357)

      End Select

      cmb_operation_type.Add(CType(item, Int32), _str_value)
    Next

  End Sub

  ' PURPOSE: Validates controls content
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: Validation result
  '
  Private Function ValidateFormat() As Boolean

    If Not Me.txt_code.ValidateFormat() Then
      Return False
    End If

    If Not Me.txt_name.ValidateFormat() Then
      Return False
    End If

    Return True

  End Function  ' ValidateFormat

  ''' <summary>
  ''' Returns if we must show the table design
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetShowTableDesign() As Boolean
    Dim _show_table_design As Boolean

    Select Case WSI.Common.GamingTableBusinessLogic.GamingTablesMode
      Case WSI.Common.GamingTableBusinessLogic.GT_MODE.GUI
        _show_table_design = False
      Case WSI.Common.GamingTableBusinessLogic.GT_MODE.GUI_AND_CASHIER
        _show_table_design = True
      Case Else
        _show_table_design = False
    End Select

    Return _show_table_design
  End Function

  ' PURPOSE: Initialize special config view (enable or disable Player tracking)
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: Validation result
  '
  Private Sub InitialLayoutConfiguration()
    Dim _PT_enabled As Boolean

    If WSI.Common.GamingTableBusinessLogic.GamingTablesMode = GamingTableBusinessLogic.GT_MODE.GUI Then
      Me.cmb_provider.Visible = False
      Me.gb_table_idle.Visible = False
      Me.gb_table_speed.Visible = False
      Me.panel_table_design.Visible = False
      Me.gb_table_bet.Visible = False
      Me.ef_table_num_seats.Visible = False

      Me.gb_operational_type.Location = New Point(Me.gb_table_bet.Location)
      Me.gb_chips_config.Location = New Point(Me.gb_operational_type.Location.X, Me.gb_operational_type.Height + Me.gb_operational_type.Location.Y + 5)
      Me.ef_theoric_hold.Location = New Point(Me.ef_table_num_seats.Location)
      Me.gb_gaming_table.Size = New Size(Me.gb_gaming_table.Size.Width, 190)
      Me.chk_management_drop_box.Location = New Point(Me.chk_enabled.Location.X, (Me.cmb_provider.Location.Y - 2))
      Me.gb_gaming_table.Size = New Size(Me.gb_gaming_table.Size.Width, 195)
      Me.gb_description.Location = New Point(Me.gb_gaming_table.Location.X, 200)
      Me.gb_description.Size = New Size(Me.gb_gaming_table.Size.Width, Me.gb_chips_config.Location.Y + Me.gb_chips_config.Height - Me.gb_description.Location.Y)
      Me.Size = New Size(Me.Size.Width + 45, 520)

      Me.gb_gaming_table.TabIndex = 0
      Me.gb_description.TabIndex = 1
      Me.gb_area_island.TabIndex = 2
      Me.gb_cashier_terminal.TabIndex = 3
      Me.gb_operational_type.TabIndex = 4
      Me.gb_chips_config.TabIndex = 5
    End If

    If WSI.Common.GamingTableBusinessLogic.GamingTablesMode = GamingTableBusinessLogic.GT_MODE.GUI_AND_CASHIER Then
      _PT_enabled = WSI.Common.GamingTableBusinessLogic.IsEnabledGTPlayerTracking()

      Me.gb_table_idle.Visible = _PT_enabled
      Me.gb_table_idle.Enabled = _PT_enabled
      Me.gb_table_speed.Visible = _PT_enabled
      Me.gb_table_speed.Enabled = _PT_enabled

      Me.panel_table_design.Visible = True
      Me.panel_table_design.Enabled = True

      Me.gb_gaming_table.TabIndex = 0
      Me.gb_table_idle.TabIndex = 1
      Me.gb_table_speed.TabIndex = 2
      Me.gb_area_island.TabIndex = 3
      Me.gb_cashier_terminal.TabIndex = 4
      Me.gb_table_bet.TabIndex = 5
      Me.gb_operational_type.TabIndex = 6
      Me.gb_chips_config.TabIndex = 7
      Me.gb_description.TabIndex = 8

      If (_PT_enabled) Then
        'Set panel_table_design just under gb_table_speed
        Me.panel_table_design.Location = New Point(Me.gb_table_speed.Location.X, Me.gb_table_speed.Location.Y + Me.gb_table_speed.Size.Height + 10)
      Else
        'Set panel_table_design at gb_table_idle.Location
        Me.panel_table_design.Location = New Point(Me.gb_table_idle.Location.X, Me.gb_table_idle.Location.Y)
        'Move txt_description under panel_table_design
        Me.gb_description.Location = New Point(Me.panel_table_design.Location.X, Me.panel_table_design.Location.Y + Me.panel_table_design.Size.Height + 10)
        Me.gb_description.Size = New Size(Me.panel_table_design.Size.Width, Me.gb_chips_config.Location.Y + Me.gb_chips_config.Height - Me.gb_description.Location.Y)
        Me.gb_description.TabIndex = 3
        'Resize form
        Me.Size = New Size(Me.Size.Width, Me.gb_chips_config.Location.Y + Me.gb_chips_config.Height + 50)
      End If

      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = True
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4999)  'Design
    End If

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7362)  'Templates

  End Sub ' InitialLayoutConfiguration

  ' PURPOSE: Call to child window to show design for the selected row
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_ShowSelectedGamingTableDesign()
    Dim _gaming_table_id As Integer
    Dim _gaming_table_design_xml As String
    Dim _frm_gaming_table_design As frm_gaming_table_design

    _gaming_table_id = Me.m_gaming_table.GamingTableID
    _gaming_table_design_xml = ""

    panel_table_design.GetXMLGamingTable(_gaming_table_design_xml)
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm_gaming_table_design = New frm_gaming_table_design()

    Call _frm_gaming_table_design.ShowEditItem(_gaming_table_id, False, _gaming_table_design_xml, txt_name.Value)

    panel_table_design.LoadGamingTable(_gaming_table_design_xml)
    ef_table_num_seats.Value = panel_table_design.NumSeats

  End Sub

  Private Sub GUI_CageControlTemplates()
    Dim _frm As frm_cage_control
    Dim _is_template_editable As Boolean

    m_gaming_table = Me.DbEditedObject

    _is_template_editable = False

    If (Me.cmb_operation_type.SelectedIndex = WSI.Common.ClosingStocks.ClosingStockType.FIXED) Then
      _is_template_editable = True
    End If
    ' Create instance
    _frm = New frm_cage_control
    _frm.m_mode_edit = frm_cage_control.MODE_EDIT.SEND
    _frm.ShowNewItem(CLASS_CAGE_CONTROL.OPEN_MODE.TEMPLATE_MAINTENANCE, 0, 0, Me.m_gaming_table.CashierID, Me.m_gaming_table.Name, _is_template_editable, Me.m_gaming_table.ClosingStock) ', _closing_stocks_dt) 'de la tabla gamingtables.

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' PURPOSE: Fill the Datatable which contains all chip types (color chips and value chips) and which of them are enabled in this gaming table
  '
  '  PARAMS:
  '     - INPUT:
  '       - gaming table id
  '
  '     - OUTPUT:
  '
  Private Sub GetDataTableChipTypes(ByVal GamingTableID As Integer)
    Dim _sb As StringBuilder
    Dim _chips_types As List(Of String)

    _sb = New StringBuilder()

    If Not FeatureChips.IsChipsTypeEnabledGP(FeatureChips.ChipType.RE) And _
        Not FeatureChips.IsChipsTypeEnabledGP(FeatureChips.ChipType.NR) And _
        Not FeatureChips.IsChipsTypeEnabledGP(FeatureChips.ChipType.COLOR) Then
      Return
    End If

    _chips_types = New List(Of String)()

    If FeatureChips.IsChipsTypeEnabledGP(FeatureChips.ChipType.RE) Then
      _chips_types.Add(FeatureChips.ChipType.RE)
    End If

    If FeatureChips.IsChipsTypeEnabledGP(FeatureChips.ChipType.NR) Then
      _chips_types.Add(FeatureChips.ChipType.NR)
    End If

    If FeatureChips.IsChipsTypeEnabledGP(FeatureChips.ChipType.COLOR) Then
      _chips_types.Add(FeatureChips.ChipType.COLOR)
    End If

    _sb.AppendLine("        SELECT   CHS_CHIP_SET_ID			                                        ")
    _sb.AppendLine("             , CHS_CHIP_TYPE			                                            ")
    _sb.AppendLine("             , CHS_ISO_CODE		                                            	  ")
    _sb.AppendLine("             , CHS_NAME	                                                		  ")
    _sb.AppendLine("             , CASE 			                                                    ")
    _sb.AppendLine("                   WHEN  GTCS_CHIPS_SET_ID IS NULL			                      ")
    _sb.AppendLine(" 						             THEN 0			                                          ")
    _sb.AppendLine(" 				           ELSE  1			                                              ")
    _sb.AppendLine(" 			         END AS GTCS_CHIP_SET_ALLOWED			                              ")
    _sb.AppendLine("        FROM   CHIPS_SETS			                                                ")
    _sb.AppendLine("        LEFT JOIN   (SELECT   GTCS_CHIPS_SET_ID , GTCS_GAMING_TABLE_ID			  ")
    _sb.AppendLine(" 				          FROM   GAMING_TABLE_CHIPS_SETS			                        ")
    _sb.AppendLine(" 			           WHERE   GTCS_GAMING_TABLE_ID = " & Me.m_gaming_table.GamingTableID)
    _sb.AppendLine(" 			         ) GTCS			                                                    ")
    _sb.AppendLine("        ON   CHS_CHIP_SET_ID  = GTCS_CHIPS_SET_ID			                        ")
    _sb.AppendLine("        WHERE   CHS_ALLOWED = 1 AND CHS_CHIP_TYPE IN ( " & String.Join(",", _chips_types.ToArray()) & ")")


    _sb.AppendLine("        ORDER BY  CHS_CHIP_TYPE, CHS_CHIP_SET_ID, CHS_ISO_CODE	                                              ")
    m_chip_types_enabled = GUI_GetTableUsingSQL(_sb.ToString(), 500)

  End Sub ' GetDataChipTypes

  ' PURPOSE: Set chip type allowed in datatable and mantain datarowversion.
  '          If current change undo old changes, final state will be without changes.
  '
  '  PARAMS:
  '     - INPUT:
  '       - chip id
  '       - Satatus: Allowed/ Disallowed
  '
  '     - OUTPUT:
  '
  Private Function SetDataTableChipTypes(ByVal ChipId As Integer, ByVal Status As Boolean) As Boolean

    If m_chip_types_enabled.Select("CHS_CHIP_SET_ID = " & ChipId).Length > 1 Then
      Return False
    Else
      If m_chip_types_enabled.Select("CHS_CHIP_SET_ID = " & ChipId)(0).Item(DT_COLUMN_ALLOWED, DataRowVersion.Original) = Status Then
        m_chip_types_enabled.Select("CHS_CHIP_SET_ID = " & ChipId)(0).RejectChanges()
      Else
        m_chip_types_enabled.Select("CHS_CHIP_SET_ID = " & ChipId)(0).Item(DT_COLUMN_ALLOWED) = Status
      End If
    End If

    Return True

  End Function ' SetDataChipTypes

  ' PURPOSE: Checked Allowed Grid in Tabs
  '
  '  PARAMS:
  '     - INPUT:
  '       - chipId
  '
  '     - OUTPUT:
  '
  Private Sub SetGridInTabs(ByVal chipId As Integer)
    Dim _grid As uc_grid

    For _index As Integer = 0 To Me.tab_chipsSets.TabCount - 1
      _grid = m_gridsList.Item(_index)
      With _grid
        For _row As Integer = 0 To .NumRows - 1
          If .Cell(_row, GRID_COLUMN_INDEX).Value = chipId Then
            .Cell(_row, GRID_COLUMN_ALLOWED).Value = uc_grid.GRID_CHK_CHECKED
          End If
        Next
      End With

    Next

  End Sub

  ' PURPOSE: Fill the tab control, created so many dynamic tabs as types of chips
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  Private Sub TabControlFill()
    Dim _tabsChips As IDictionary(Of Integer, String)
    Dim _grid As uc_grid

    m_gridsList = New List(Of uc_grid)
    _tabsChips = Me.GetTabs()

    For Each _item As KeyValuePair(Of Integer, String) In _tabsChips
      'Create Grid 
      _grid = New GUI_Controls.uc_grid()

      'Add grid to tab
      Call CreateDataGridInTabControl(_grid, _item.Value, _item.Key, False, String.Empty, Me.tab_chipsSets)

      'Format the grid
      Call GUI_StyleSheetTab(_grid)

      'Fill grid 
      Call DataGridFill(_grid, _item.Key)

      ' Add event to mantain internal datatable chips sets
      AddHandler _grid.CellDataChangedEvent, AddressOf grid_CellDataChanged

      m_gridsList.Add(_grid)

    Next

  End Sub ' TabControlFill

  ' PURPOSE: Fill the  grid included in tab control, one grid in each tab. By default select chip  with ID = 1.
  '          Include all color chips, and value chips which currency was the same to gaming table
  '
  '  PARAMS:
  '     - INPUT:
  '       - Datagrid to fill (control in tab)
  '       - String ISO CODE (name of tab)
  '
  '     - OUTPUT:
  '
  Private Sub DataGridFill(ByVal dg_chips As GUI_Controls.uc_grid, ByVal chipType As Integer)
    Dim _selected_dr() As DataRow
    Dim _indexRow As Integer

    _selected_dr = m_chip_types_enabled.Select(String.Format("CHS_CHIP_TYPE = {0}", chipType))
    For Each _dr As DataRow In _selected_dr
      _indexRow = dg_chips.AddRow()

      dg_chips.Cell(_indexRow, GRID_COLUMN_INDEX).Value = _dr.Item(DT_COLUMN_ID)

      If chipType = FeatureChips.ChipType.COLOR Then
        dg_chips.Cell(_indexRow, GRID_COLUMN_NAME).Value = _dr.Item(DT_COLUMN_NAME)
      Else
        dg_chips.Cell(_indexRow, GRID_COLUMN_NAME).Value = String.Format("{0} - {1}", _dr.Item(DT_COLUMN_NAME), _dr.Item(DT_COLUMN_ISO_CODE))
      End If
      If _dr.Item(DT_COLUMN_ALLOWED) > 0 Then
        dg_chips.Cell(_indexRow, GRID_COLUMN_ALLOWED).Value = uc_grid.GRID_CHK_CHECKED
      Else
        dg_chips.Cell(_indexRow, GRID_COLUMN_ALLOWED).Value = uc_grid.GRID_CHK_UNCHECKED
      End If

    Next

  End Sub ' DataGridFill

  ' PURPOSE: Define layout of all Grid Columns of grids included in tab control
  '
  '  PARAMS:
  '     - INPUT:
  '       - Datagrid to fill (control in tab)
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub GUI_StyleSheetTab(ByVal dg_chips As GUI_Controls.uc_grid)

    With dg_chips
      ' .Init (Cols, Rows, IsGridEditable)
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, True)

      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header.Text = " "
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' NAME
      .Column(GRID_COLUMN_NAME).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2420) '  "Grupo"
      .Column(GRID_COLUMN_NAME).Width = GRID_WIDTH_NAME
      .Column(GRID_COLUMN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' ALLOWED
      .Column(GRID_COLUMN_ALLOWED).Header.Text = GLB_NLS_GUI_CONTROLS.GetString(281) '   "Allowed"
      .Column(GRID_COLUMN_ALLOWED).Width = GRID_WIDTH_ALLOWED
      .Column(GRID_COLUMN_ALLOWED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_ALLOWED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
      .Column(GRID_COLUMN_ALLOWED).Editable = True

    End With

  End Sub ' GUI_StyleSheetTab

  ' PURPOSE: Get tabs to display on screen
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '            
  ' RETURNS: IDictionary 
  '
  Private Function GetTabs() As IDictionary
    Dim _tabs As Dictionary(Of Integer, String)

    _tabs = New Dictionary(Of Integer, String)()

    If (m_chip_types_enabled IsNot Nothing) Then
      For Each _item As DataRow In m_chip_types_enabled.Rows
        If Not _tabs.ContainsKey(_item("CHS_CHIP_TYPE")) Then
          _tabs.Add(_item("CHS_CHIP_TYPE"), Me.GetNameChipType(_item("CHS_CHIP_TYPE")))
        End If
      Next
    End If
    Return _tabs
  End Function ' GetTabs

  ' PURPOSE: Get the description chips type
  '
  '  PARAMS:
  '     - INPUT: chipType
  '
  '     - OUTPUT:
  '           
  ' RETURNS: String
  '
  Private Function GetNameChipType(chipType As FeatureChips.ChipType) As String
    Dim _name As String

    _name = m_chip_sets.Item(chipType)

    Return _name
  End Function

  ' PURPOSE: Update datatable in class gaming table contains allowed chips for this table
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub UpdateChipsGamingTable()

    Dim _rows As DataRow()
    Dim _search As DataRow()
    Dim _newRow As DataRow

    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
      m_gaming_table.ChipsAllowed.Clear()
    End If


    For Each _item As DataRow In m_gaming_table.ChipsAllowed.Rows
      _search = m_chip_types_enabled.Select(String.Format("GTCS_CHIP_SET_ALLOWED = {0} AND CHS_CHIP_SET_ID = {1}", 1, _item("GTCS_CHIPS_SET_ID")))
      If _search.Length = 0 Then
        If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT Then
          _item.Delete()
        End If
      End If
    Next

    _rows = m_chip_types_enabled.Select(String.Format("GTCS_CHIP_SET_ALLOWED = {0}", 1))
    For Each _item As DataRow In _rows
      _search = m_gaming_table.ChipsAllowed.Select(String.Format("GTCS_CHIPS_SET_ID = {0}", _item("CHS_CHIP_SET_ID")))
      If (_search.Length = 0) Then
        _newRow = m_gaming_table.ChipsAllowed.NewRow()
        _newRow("GTCS_GAMING_TABLE_ID") = m_gaming_table.GamingTableID
        _newRow("GTCS_CHIPS_SET_ID") = _item("CHS_CHIP_SET_ID")
        m_gaming_table.ChipsAllowed.Rows.Add(_newRow)
      End If
    Next

  End Sub ' SetChipsSets

  ' PURPOSE: Set descriptions in tabs for currencies
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub SetTabsCurrencies()
    Dim _nationalCurrency As New CurrencyExchange()
    Dim _allowedCurrencies As New List(Of CurrencyExchange)
    Dim _types As New List(Of CurrencyExchangeType)

    Call AddCurrenciesTab()


  End Sub ' SetTabsCurrencies

  ''' <summary>
  ''' Enable/disable template button 
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub cmb_operation_type_ValueChangedEvent() Handles cmb_operation_type.ValueChangedEvent
    Dim _selected_item As Object

    _selected_item = cmb_operation_type.SelectedIndex

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = False

    If _selected_item = 0 Then
      chk_operation_type.Enabled = False
      chk_operation_type.Checked = False
    ElseIf _selected_item = 1 Then
      chk_operation_type.Enabled = True
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = True
    Else
      chk_operation_type.Enabled = False
      chk_operation_type.Checked = True
    End If

  End Sub

  ''' <summary>
  ''' Add one tab for each allowed currency in the system
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub AddCurrenciesTab()
    Dim _currency_accepted As String
    Dim _currency_array As String()
    Dim _str_one_word As String = String.Empty


    If WSI.Common.Misc.IsMultiCurrencyExchangeEnabled() Then
      _currency_accepted = GeneralParam.GetString("RegionalOptions", "CurrenciesAccepted")
      _currency_array = _currency_accepted.Split(";")

      For Each _str_one_word In _currency_array
        If (Not _str_one_word.Equals(OLD_CHIPS_DENOMINATION)) Then
          Call AddTab(_str_one_word)
        End If
      Next

    Else
      Call AddTab(CurrencyExchange.GetNationalCurrency())
    End If


    Call FillTabValues()

  End Sub

  ''' <summary>
  ''' Create the structure for each tab of aallowed currency
  ''' </summary>
  ''' <param name="name"></param>
  ''' <remarks></remarks>
  Private Sub AddTab(ByRef name As String)
    Dim _tab As Windows.Forms.TabPage
    Dim _ef_table_min_bet As uc_entry_field
    Dim _ef_table_max_bet As uc_entry_field

    _tab = New Windows.Forms.TabPage
    _ef_table_min_bet = New uc_entry_field()
    _ef_table_max_bet = New uc_entry_field()

    _ef_table_min_bet.Size = New Size(240, 25)
    _ef_table_min_bet.TextWidth = 100
    _ef_table_min_bet.Location = New System.Drawing.Point(_ef_table_min_bet.Location.X + 20, _ef_table_min_bet.Location.Y + 10)
    _ef_table_min_bet.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5070)
    _ef_table_min_bet.SufixText = ""
    _ef_table_min_bet.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 2)

    _ef_table_max_bet.Size = New Size(240, 25)
    _ef_table_max_bet.TextWidth = 100
    _ef_table_max_bet.Location = New System.Drawing.Point(_ef_table_min_bet.Location.X, _ef_table_min_bet.Location.Y + 35)
    _ef_table_max_bet.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5071)
    _ef_table_max_bet.SufixText = ""
    _ef_table_max_bet.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 2)

    _tab.Text = name
    _tab.Controls.Add(_ef_table_min_bet)
    _tab.Controls.Add(_ef_table_max_bet)
    Me.tab_currencies.TabPages.Add(_tab)

  End Sub

  ''' <summary>
  ''' Fill the currencies values from database.
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub FillTabValues()
    Dim _default As uc_entry_field
    Dim _bet_currencies As DataTable

    _bet_currencies = New DataTable()
    _bet_currencies = m_gaming_table.BetByCurrencies

    For Each _tab As TabPage In tab_currencies.TabPages
      _default = New uc_entry_field()
      For Each item As DataRow In _bet_currencies.Rows
        If (item(0).ToString() = _tab.Text) Then
          _default = _tab.Controls.Item(0)
          _default.TextValue = GUI_FormatNumber(item(1).ToString(), 2)
          _default = _tab.Controls.Item(1)
          _default.TextValue = GUI_FormatNumber(item(2).ToString(), 2)
        End If
      Next
    Next

  End Sub

  ''' <summary>
  ''' Obtain the Values to bet from database
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetBetCurrenciesValues() As DataTable
    Dim _default As uc_entry_field
    Dim _new_row As DataRow
    Dim _dt_bet_currencies As DataTable

    _dt_bet_currencies = New DataTable()
    _dt_bet_currencies = CreateDatatable(1)

    For Each _tab As TabPage In tab_currencies.TabPages
      _default = New uc_entry_field()
      _new_row = _dt_bet_currencies.NewRow()
      _new_row(0) = _tab.Text
      _default = _tab.Controls.Item(0)
      _new_row(2) = IIf(String.IsNullOrEmpty(_default.Value), 0, _default.Value)
      _default = _tab.Controls.Item(1)
      _new_row(1) = IIf(String.IsNullOrEmpty(_default.Value), 0, _default.Value)
      _dt_bet_currencies.Rows.Add(_new_row)
    Next

    Return _dt_bet_currencies
  End Function

#End Region    ' Privates

#Region "Public "

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    m_chip_sets = CLS_CHIPS_SET.GetNamesChipType()

  End Sub

#End Region     ' Public

#Region "Overrides"

  ' PURPOSE : Init form in edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '       - UserId
  '       - Username
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '
  Public Overloads Sub ShowEditItem(ByVal OperationId As Int64)

    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
    Me.DbObjectId = OperationId

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)

    If Me.DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If Me.DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub

  ' PURPOSE: Init form in new mode
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overloads Sub ShowNewItem(Optional ByVal GamingTableCopiedId As Long = 0)
    Dim _gaming_table As CLASS_GAMING_TABLE

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    DbObjectId = Nothing
    DbStatus = ENUM_STATUS.STATUS_OK

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)

    If GamingTableCopiedId > 0 Then
      _gaming_table = DbEditedObject
      DbObjectId = GamingTableCopiedId
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
      DbObjectId = Nothing

      Call _gaming_table.ResetIdValues()
    End If

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowNewItem

  ' PURPOSE: Process clicks on data grid (double-clicks) and select button
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CUSTOM_0  'Design
        Call GUI_ShowSelectedGamingTableDesign()

      Case ENUM_BUTTON.BUTTON_CUSTOM_1 'Templates
        Call GUI_CageControlTemplates()

      Case Else

        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Sub EnableDisableCmbCashiers()
    m_gaming_table = Me.DbReadObject

    If TITO.Utils.IsTitoMode Then
      If Me.chk_integrated.Checked And m_gaming_table.CashierID <> Nothing _
        And m_gaming_table.CashierID > 0 Then
        Me.cmb_cashiers.SelectedValue = m_gaming_table.CashierID
        Me.cmb_cashiers.Enabled = True
      Else
        Me.cmb_cashiers.SelectedValue = -1
        Me.cmb_cashiers.Enabled = False
      End If
    Else
      If Me.chk_integrated.Checked And m_gaming_table.CashierID <> Nothing _
        And m_gaming_table.CashierID > 0 Then
        Me.cmb_cashiers.SelectedValue = m_gaming_table.CashierID
      ElseIf Not Me.chk_integrated.Checked And m_gaming_table.LinkedCashierid <> Nothing _
        And m_gaming_table.LinkedCashierid > 0 Then
        Me.cmb_cashiers.SelectedValue = m_gaming_table.LinkedCashierid
      Else
        Me.cmb_cashiers.SelectedValue = -1
      End If
    End If

  End Sub

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_GAMING_TABLES_EDIT
    Call MyBase.GUI_SetFormId()

  End Sub

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    ' Initialize special config view
    Call InitialLayoutConfiguration()

    ' Gaming Tables 
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3417)                         ' 3417 "Edici�n de mesa de juego"

    'Controls:
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_DELETE).Visible = False

    ' Gaming Table Data
    Me.gb_gaming_table.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4406)         ' 4406 "Gaming Table"

    ' Table Code
    Me.txt_code.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3418)                ' 3418 "C�digo"
    Me.txt_code.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, MAX_CODE_LEN, 0)

    ' Table Name
    Me.txt_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3419)                ' 3419 "Nombre"
    Me.txt_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, MAX_NAME_LEN, 0)

    ' Table Types
    Me.cmb_table_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3420)          ' 3420 "Tipo de Mesa"

    ' Area
    Me.gb_area_island.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1166)          ' 1166 "Ubicaci�n"
    Me.cmb_area.Text = GLB_NLS_GUI_CONFIGURATION.GetString(435)                   ' 435 "Area"

    ' Zone
    Me.ef_smoking.Text = GLB_NLS_GUI_CONFIGURATION.GetString(430)                 ' 430 "Zona"
    Me.ef_smoking.IsReadOnly = True
    Me.ef_smoking.TabStop = False

    ' Bank
    Me.cmb_bank.Text = GLB_NLS_GUI_CONFIGURATION.GetString(431)                   ' 431 "Isla"

    ' Floor
    Me.ef_floor_id.Text = GLB_NLS_GUI_CONFIGURATION.GetString(432)                ' 432 "Id en planta"
    Me.ef_floor_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 6, 0)

    ' Enabled
    Me.chk_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3422)             ' 3422 "Habilitada"

    ' Integrated Cashier
    Me.gb_cashier_terminal.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3479)     ' 3479 "Cajero"
    Me.lbl_linked.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8482) ' 8480 "Linked"
    Me.chk_integrated.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4339)          ' 4339 "Integrado"

    ' Description
    Me.gb_description.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3423)          ' 3423 "Descripci�n"
    Me.txt_description.MaxLength = MAX_DESC_LEN

    ' Table Speed
    Me.gb_table_speed.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5354)          ' 5354 "Speed table"
    Me.lbl_speed_time.Text = GLB_NLS_GUI_CONTROLS.GetString(442)
    Me.lbl_speed_info.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5355)          ' 5355 "Time of a complete play"

    ' Table Idle
    Me.gb_table_idle.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5587)
    Me.ef_table_idle_plays.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2, 0)
    Me.lbl_idle_plays.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5588)
    Me.lbl_idle_info.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5589, "--")

    '  Slow
    Me.lbl_speed_slow.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5021)          ' 5021 "Slow"
    Me.ef_table_speed_slow.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 4, 0)

    '  Normal
    Me.lbl_speed_normal.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5022)        ' 5022 "Normal"
    Me.ef_table_speed_normal.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 4, 0)

    '  Fast
    Me.lbl_speed_fast.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5023)          ' 5023 "Fast"
    Me.ef_table_speed_fast.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 4, 0)

    ' Table Num Seats
    Me.ef_table_num_seats.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4955)      ' 4955 "No. of seats"
    Me.ef_table_num_seats.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2, 0)
    Me.ef_table_num_seats.IsReadOnly = True

    ' Table Bets
    Me.gb_table_bet.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5072)            ' 5072 "Table bets"

    ' Theoric Hold
    Me.ef_theoric_hold.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5629)         ' 5629 "Hold Teorico %"
    Me.ef_theoric_hold.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 6, 2)

    ' Management drop box
    Me.chk_management_drop_box.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7298) ' 7298 "Gesti�n drop box"

    ' Session Status
    Me.lbl_session_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4407)      ' 4407 "Estado Sesi�n:"

    ' Provider
    Me.cmb_provider.Text = GLB_NLS_GUI_AUDITOR.GetString(341)                     ' 341 "Proveedor"

    ' Chips Sets
    'EOR 16-02-2016 Change Text Group 
    Me.gb_chips_config.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6577)         ' 6577 "Chips"
    Me.lbl_info_tab_chips.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7122)      ' 7122 "The table should operate with at least one group of Value chips"

    Call Me.SetTabsCurrencies()

    Call Me.GetDataTableChipTypes(Me.m_gaming_table.GamingTableID)

    ' Check Cashier Session is Open
    Call DisableContorlsIfCashierSessionIsOpen()

    ' Fill Combos
    Call TableTypeComboFill()
    Call CashierComboFill()
    Call AreaComboFill()
    Call BankComboFill()
    Call ProvidersComboFill()
    Call TabControlFill()
    Call OperationTypeComboFill()

    Me.cmb_operation_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7359)
    Me.gb_operational_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7360)
    Me.chk_operation_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7361)
    Me.chk_operation_type.Enabled = False
    Me.chk_operation_type.Checked = False

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = False

    ' Add Handles
    Call AddHandles()

  End Sub ' GUI_InitControls

  ''' <summary>
  ''' Checks table speed values
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CheckTableSpeed() As Boolean
    If WSI.Common.GamingTableBusinessLogic.GamingTablesMode = GamingTableBusinessLogic.GT_MODE.GUI Then
      Return True
    End If

    If WSI.Common.GamingTableBusinessLogic.GamingTablesMode = GamingTableBusinessLogic.GT_MODE.GUI_AND_CASHIER Then
      Dim _PT_enabled As Boolean
      _PT_enabled = WSI.Common.GamingTableBusinessLogic.IsEnabledGTPlayerTracking()

      If (Not _PT_enabled) Then
        Return True
      End If

      ' Table Speed Slow empty
      If String.IsNullOrEmpty(Me.ef_table_speed_slow.Value) Then
        ' "%1 field must not be left blank."
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(114), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_speed_slow.Text)
        Call Me.ef_table_speed_slow.Focus()

        Return False
      End If

      ' Table Speed Normal empty
      If String.IsNullOrEmpty(Me.ef_table_speed_normal.Value) Then
        ' "%1 field must not be left blank."
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(114), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_speed_normal.Text)
        Call Me.ef_table_speed_normal.Focus()

        Return False
      End If

      ' Table Speed Fast empty
      If String.IsNullOrEmpty(Me.ef_table_speed_fast.Value) Then
        ' "%1 field must not be left blank."
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(114), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_speed_fast.Text)
        Call Me.ef_table_speed_fast.Focus()

        Return False
      End If

      ' Table Speed Slow is higher than 0
      If GUI_ParseNumber(Me.ef_table_speed_slow.Value) <= 0 Then
        ' "You must enter a value for %1 greater than 0."
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_speed_slow.Text)
        Call Me.ef_table_speed_slow.Focus()
        Return False
      End If

      ' Table Speed Normal is higher than 0
      If GUI_ParseNumber(Me.ef_table_speed_normal.Value) <= 0 Then
        ' "You must enter a value for %1 greater than 0."
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_speed_normal.Text)
        Call Me.ef_table_speed_normal.Focus()
        Return False
      End If

      ' Table Speed Fast is higher than 0
      If GUI_ParseNumber(Me.ef_table_speed_fast.Value) <= 0 Then
        ' "You must enter a value for %1 greater than 0."
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_speed_fast.Text)
        Call Me.ef_table_speed_fast.Focus()
        Return False
      End If

      ' Table Speed Slow is higher than normal speed
      If GUI_ParseNumber(Me.ef_table_speed_slow.Value) < GUI_ParseNumber(Me.ef_table_speed_normal.Value) Then
        ' "The '%1' field cannot be greater than %2."
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7505), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_speed_normal.Text, Me.lbl_speed_slow.Text)
        Call Me.ef_table_speed_normal.Focus()
        Return False
      End If

      ' Table Speed normal is higher than fast speed
      If GUI_ParseNumber(Me.ef_table_speed_normal.Value) < GUI_ParseNumber(Me.ef_table_speed_fast.Value) Then
        ' "The '%1' field cannot be greater than %2."
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7505), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_speed_fast.Text, Me.lbl_speed_normal.Text)
        Call Me.ef_table_speed_fast.Focus()
        Return False
      End If
    End If

    Return True
  End Function

  ''' <summary>
  ''' Checks table idle value
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CheckTableIdle() As Boolean
    If WSI.Common.GamingTableBusinessLogic.GamingTablesMode = GamingTableBusinessLogic.GT_MODE.GUI Then
      Return True
    End If

    If WSI.Common.GamingTableBusinessLogic.GamingTablesMode = GamingTableBusinessLogic.GT_MODE.GUI_AND_CASHIER Then
      Dim _PT_enabled As Boolean
      _PT_enabled = WSI.Common.GamingTableBusinessLogic.IsEnabledGTPlayerTracking()

      If (Not _PT_enabled) Then
        Return True
      End If

      If String.IsNullOrEmpty(Me.ef_table_idle_plays.Value) Then
        ' "%1 field must not be left blank."
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(114), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.gb_table_idle.Text)
        Call Me.ef_table_idle_plays.Focus()
        Return False
      End If

      ' Table idle plays is higher than 0
      If GUI_ParseNumber(Me.ef_table_idle_plays.Value) <= 0 Then
        ' "You must enter a value for %1 greater than 0."
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.gb_table_idle.Text)
        Call Me.ef_table_idle_plays.Focus()
        Return False
      End If
    End If

    Return True
  End Function

  ''' <summary>
  ''' Checks table number of seats
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CheckTableNumberOfSeats() As Boolean
    If WSI.Common.GamingTableBusinessLogic.GamingTablesMode = GamingTableBusinessLogic.GT_MODE.GUI Then
      Return True
    End If

    ' Table Num Seats
    If String.IsNullOrEmpty(Me.ef_table_num_seats.Value) Then
      ' "%1 field must not be left blank."
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(114), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.ef_table_num_seats.Text)
      Call Me.ef_table_num_seats.Focus()

      Return False
    End If

    ' Table Num Seats exceeded
    If GUI_ParseNumber(Me.ef_table_num_seats.Value) > GeneralParam.GetInt32("GamingTables", "Seats.MaxAllowedSeats", 10) Then
      ' "The number of enabled seats exceeds the maximum allowed."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4956), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.ef_table_num_seats.Text)
      Call Me.ef_table_num_seats.Focus()

      Return False
    End If

    Return True
  End Function

  ''' <summary>
  ''' Checks provider value
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CheckProvider() As Boolean
    If WSI.Common.GamingTableBusinessLogic.GamingTablesMode = GamingTableBusinessLogic.GT_MODE.GUI Then
      Return True
    End If

    If String.IsNullOrEmpty(Me.cmb_provider.TextValue) Then
      ' "%1 field must not be left blank."
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(114), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.cmb_provider.Text)
      Call Me.cmb_provider.Focus()
      Return False
    End If

    Return True
  End Function

  ''' <summary>
  ''' Check theoric hold value
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CheckTheoricHold() As Boolean
    ' Theoric Hold incorrect format
    If String.IsNullOrEmpty(Me.ef_theoric_hold.Value) Then
      ' "The format of the field %1 is not correct."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.ef_theoric_hold.Text)
      Call Me.ef_theoric_hold.Focus()
      Return False
    End If

    ' Theoric hold between o and 100
    If Me.ef_theoric_hold.Value < 0 Or Me.ef_theoric_hold.Value > 100 Then
      ' "El campo %1 debe de estar entre el valor %2 y %3."
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(131), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.ef_theoric_hold.Text, 0, 100)
      Call Me.ef_theoric_hold.Focus()
      Return False
    End If

    Return True
  End Function

  ''' <summary>
  ''' Checks table bets
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CheckTableBets() As Boolean
    Dim _ef_max_Bet As uc_entry_field
    Dim _ef_min_Bet As uc_entry_field

    If WSI.Common.GamingTableBusinessLogic.GamingTablesMode = GamingTableBusinessLogic.GT_MODE.GUI Then
      Return True
    End If

    For Each _tab As TabPage In tab_currencies.TabPages
      _ef_max_Bet = New uc_entry_field()
      _ef_min_Bet = New uc_entry_field()

      _ef_min_Bet = _tab.Controls.Item(0)
      _ef_max_Bet = _tab.Controls.Item(1)

      ' DHA: 23-JUL-2014 Min bet can not be greater thane max bet
      ' Table min bet wrong format
      If Not String.IsNullOrEmpty(_ef_min_Bet.Value) And GUI_ParseNumber(_ef_min_Bet.Value) < 0 Then
        ' "The format of the field %1 is not correct."
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _ef_min_Bet.Text)
        Call _ef_min_Bet.Focus()
        Return False
      End If

      ' Table max bet wrong format
      If Not String.IsNullOrEmpty(_ef_max_Bet.Value) And GUI_ParseNumber(_ef_max_Bet.Value) < 0 Then
        ' "The format of the field %1 is not correct."
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _ef_max_Bet.Text)
        Call _ef_max_Bet.Focus()
        Return False
      End If

      If Not String.IsNullOrEmpty(_ef_min_Bet.Value) And Not String.IsNullOrEmpty(_ef_max_Bet.Value) Then
        If GUI_ParseNumber(_ef_max_Bet.Value) < GUI_ParseNumber(_ef_min_Bet.Value) Then
          ' "The '%1' field cannot be greater than %2."
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1421), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _ef_min_Bet.Text, _ef_max_Bet.Text)
          Call tab_currencies.SelectTab(_tab.TabIndex())
          Call _ef_max_Bet.Focus()
          Return False
        End If
      End If
    Next

    Return True
  End Function

  ''' <summary>
  ''' Checks Chips Sets
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Function CheckChipsSets() As Integer
    Dim _count_chip_sets As Integer

    _count_chip_sets = 0
    For _index As Integer = 0 To Me.tab_chipsSets.TabCount - 1
      _count_chip_sets += Me.CountCheckGrid(_index)
    Next

    If _count_chip_sets = 0 Then
      ' "You must enable at least one chip group."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7061), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK)
      Me.tab_chipsSets.Focus()
      Return False
    End If

    Return True
  End Function


  ' PURPOSE: Validate the data presented on the screen. The following values are not empty: Holder Name, RFC, CURP
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - True:  Data is ok
  '     - False: Data is NOT ok
  '
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Dim nls_param As String

    ' Empty Code
    If String.IsNullOrEmpty(Me.txt_code.Value) Then
      ' 3441 "El campo 'C�digo' es obligatorio"
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3441), _
                                ENUM_MB_TYPE.MB_TYPE_ERROR, _
                                ENUM_MB_BTN.MB_BTN_OK)
      Call Me.txt_code.Focus()

      Return False
    End If

    ' Too long Code
    If Me.txt_code.Value.Length > MAX_CODE_LEN Then
      ' 3442 "El campo 'C�digo' debe tener un m�ximo de %1 car�cteres."
      nls_param = MAX_CODE_LEN.ToString()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3442), _
                                ENUM_MB_TYPE.MB_TYPE_ERROR, _
                                ENUM_MB_BTN.MB_BTN_OK, _
                                nls_param)
      Call Me.txt_code.Focus()

      Return False
    End If

    ' Empty Name
    If String.IsNullOrEmpty(Me.txt_name.Value) Then
      ' 3443 "El campo 'Nombre' es obligatorio"
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3443), _
                                ENUM_MB_TYPE.MB_TYPE_ERROR, _
                                ENUM_MB_BTN.MB_BTN_OK)
      Call Me.txt_name.Focus()

      Return False
    End If

    ' Too long Name
    If Me.txt_name.Value.Length > MAX_NAME_LEN Then
      ' 3442 "El campo 'Nombre' debe tener un m�ximo de %1 car�cteres."
      nls_param = MAX_NAME_LEN.ToString()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3444), _
                                ENUM_MB_TYPE.MB_TYPE_ERROR, _
                                ENUM_MB_BTN.MB_BTN_OK, _
                                nls_param)
      Call Me.txt_name.Focus()

      Return False
    End If

    ' Too long Description
    If Me.txt_description.Text.Length > MAX_DESC_LEN Then
      ' 3448 "El campo 'Descripci�n' debe tener un m�ximo de %1 car�cteres."
      nls_param = MAX_DESC_LEN.ToString()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3448), _
                                ENUM_MB_TYPE.MB_TYPE_ERROR, _
                                ENUM_MB_BTN.MB_BTN_OK, _
                                nls_param)
      Call Me.txt_description.Focus()

      Return False
    End If

    ' Empty Table Type
    If Me.cmb_table_type.SelectedIndex < 1 Then
      ' 4383 "Se debe seleccionar un tipo de mesa."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4383), _
                                ENUM_MB_TYPE.MB_TYPE_ERROR, _
                                ENUM_MB_BTN.MB_BTN_OK)
      Call Me.cmb_table_type.Focus()

      Return False
    End If

    ' Empty Cashier
    If Me.chk_integrated.Checked _
    And Not Me.cmb_cashiers.SelectedIndex > 0 Then
      ' 3448 "Debe seleccionar un cajero."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3478), _
                                ENUM_MB_TYPE.MB_TYPE_ERROR, _
                                ENUM_MB_BTN.MB_BTN_OK)
      Call Me.cmb_cashiers.Focus()

      Return False
    End If

    ' Cashier session status change when windows is open. 
    If Not CheckGamingTableStatus() Then
      Return False
    End If

    ' Cashier disponibility
    If Not CheckCashierSelected() Then

      Me.cmb_cashiers.SelectedValue = 0 ' Set default value
      If Me.m_gaming_table.CashierID <> Nothing Then
        Me.cmb_cashiers.SelectedValue = Me.m_gaming_table.CashierID ' Set original value
      End If

      Call Me.cmb_cashiers.Focus()

      Return False
    End If

    If (ef_table_num_seats.Value = "0") Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_PLAYER_TRACKING.GetString(4994))
      Return False
    End If

    If Not ValidateFormat() Then
      Return False
    End If

    If (Not CheckProvider()) Then
      Return False
    End If

    If (Not CheckTableNumberOfSeats()) Then
      Return False
    End If

    If (Not CheckTheoricHold()) Then
      Return False
    End If

    If (Not CheckTableIdle()) Then
      Return False
    End If

    If (Not CheckTableSpeed()) Then
      Return False
    End If

    If (Not CheckTableBets()) Then
      Return False
    End If

    If (Not CheckChipsSets()) Then
      Return False
    End If

    Return True

  End Function 'GUI_IsScreenDataOk

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_GetScreenData()

    Dim _gaming_table_new As CLASS_GAMING_TABLE
    Dim _gaming_table_old As CLASS_GAMING_TABLE

    Dim _closing_stock_new As DataTable
    Dim _closing_stock_old As DataTable
    Dim _rows_to_delete As DataRow()

    m_gaming_table = Me.DbEditedObject

    ' Codigo
    m_gaming_table.Code = Me.txt_code.Value

    ' Name
    m_gaming_table.Name = Me.txt_name.Value

    ' Table Type
    If Me.cmb_table_type.SelectedIndex > 0 Then
      m_gaming_table.TypeID = Me.cmb_table_type.Value
      m_gaming_table.TypeName = Me.cmb_table_type.TextValue
    End If

    ' Enabled
    m_gaming_table.Enabled = Me.chk_enabled.Checked

    ' Integrated Cashier
    m_gaming_table.IntegratedCashier = Me.chk_integrated.Checked

    ' Cashier
    ' ATB 06-FEB-2017
    ' If there's no option selected, don't evaluate it
    If Me.cmb_cashiers.SelectedValue <> Nothing AndAlso m_gaming_table.IntegratedCashier Then
      m_gaming_table.CashierID = Me.cmb_cashiers.SelectedValue
      m_gaming_table.CashierName = Me.cmb_cashiers.SelectedItem.Row.ItemArray(1)
      m_gaming_table.LinkedCashierid = Me.cmb_cashiers.SelectedValue
    ElseIf Me.cmb_cashiers.SelectedValue <> Nothing AndAlso Not m_gaming_table.IntegratedCashier Then
      m_gaming_table.LinkedCashierid = Me.cmb_cashiers.SelectedValue
      m_gaming_table.CashierName = Me.cmb_cashiers.SelectedItem.Row.ItemArray(1)
    End If

    'If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT _
    'Or Me.cmb_area.SelectedIndex > 0 Then
    ' Area
    m_gaming_table.AreaID = Me.cmb_area.Value
    m_gaming_table.AreaName = Me.cmb_area.TextValue

    ' Bank
    m_gaming_table.BankID = Me.cmb_bank.Value
    m_gaming_table.BankName = Me.cmb_bank.TextValue
    'End If

    ' FloorId
    m_gaming_table.FloorID = Me.ef_floor_id.Value

    ' Description
    m_gaming_table.Description = Me.txt_description.Text

    ' Theoric hold
    m_gaming_table.TheoricHold = Me.ef_theoric_hold.Value

    ' Management drop box
    m_gaming_table.DropBoxEnabled = Me.chk_management_drop_box.Checked


    If (tab_chipsSets.TabCount > 0) Then
      ' Update Chips Allowed
      UpdateChipsGamingTable()
    End If

    If WSI.Common.GamingTableBusinessLogic.IsGamingTablesEnabledAndPlayerTracking() Then
      ' Table Speed Slow
      Int32.TryParse(Me.ef_table_speed_slow.Value, m_gaming_table.TableSpeedSlow)

      ' Table Speed Normal
      Int32.TryParse(Me.ef_table_speed_normal.Value, m_gaming_table.TableSpeedNormal)

      ' Table Speed Fast
      Int32.TryParse(Me.ef_table_speed_fast.Value, m_gaming_table.TableSpeedFast)
    End If

    _gaming_table_new = DbEditedObject
    _gaming_table_old = DbReadObject

    m_gaming_table.ClosingStock.CashierId = Me.SelectedCashierID
    m_gaming_table.ClosingStock.SleepsOnTable = Me.chk_operation_type.Checked
    m_gaming_table.ClosingStock.Type = Me.cmb_operation_type.SelectedIndex
    If (m_gaming_table.ClosingStock.Type = ClosingStocks.ClosingStockType.NONE) Then
      m_gaming_table.ClosingStock.CurrenciesStocks = New DataTable()
    Else
      _closing_stock_new = _gaming_table_new.ClosingStock.CurrenciesStocks
      _closing_stock_old = _gaming_table_old.ClosingStock.CurrenciesStocks

      If SelectedCashierID = 0 Then
        _rows_to_delete = _gaming_table_new.ClosingStock.CurrenciesStocks.Select("QUANTITY IS NULL")
        For Each row As DataRow In _rows_to_delete
          row.Delete()
        Next
      End If

      ValidateTables(_closing_stock_old, _closing_stock_new, "ISO_CODE,DENOMINATION")
    End If

    _gaming_table_new.BetByCurrencies = GetBetCurrenciesValues()

    ValidateTables(_gaming_table_old.BetByCurrencies, _gaming_table_new.BetByCurrencies, "GTB_ISO_CODE", 1)

    ' Provider
    If Me.cmb_provider.SelectedIndex >= 0 Then
      m_gaming_table.ProviderID = Me.cmb_provider.Value
      m_gaming_table.ProviderName = Me.cmb_provider.TextValue
    End If

    ' Table Num Seats
    m_gaming_table.TableNumSeats = Me.ef_table_num_seats.Value

    ' Table idle plays
    m_gaming_table.TableIdlePlays = Me.ef_table_idle_plays.Value

    If GetShowTableDesign() Then
      panel_table_design.GetXMLGamingTable(m_gaming_table.TableDesignXml)
    End If


  End Sub 'GUI_GetScreenData


  ''' <summary>
  '''  Function to validate the AllowedCurrencies tables for audit
  ''' </summary>
  ''' <param name="DataTableRead"></param>
  ''' <param name="DataTableEdit"></param>
  ''' <param name="Sort"></param>
  ''' <param name="TypeTable"></param>
  ''' <remarks></remarks>
  Private Sub ValidateTables(DataTableRead As DataTable, DataTableEdit As DataTable, Sort As String, Optional TypeTable As Integer = 0)
    Dim _dt As DataTable
    Dim _dbReadObject_count As Integer
    Dim _dbEditObject_count As Integer

    _dbReadObject_count = DataTableRead.Rows.Count
    _dbEditObject_count = DataTableEdit.Rows.Count

    ' If both tables are null, exit from function
    If (_dbReadObject_count = 0) AndAlso (_dbEditObject_count = 0) Then
      Exit Sub
    End If

    If (_dbReadObject_count <> _dbEditObject_count) Then

      _dt = CompareDataTables(DataTableRead, DataTableEdit, Sort, TypeTable)

      If (_dbReadObject_count < _dbEditObject_count) Then
        If _dbReadObject_count = 0 OrElse _dt.Rows.Count = 0 Then
          Exit Sub
        End If
        DataTableRead.Clear()
        DataTableRead.Merge(_dt, False, MissingSchemaAction.Add)
      Else
        DataTableEdit.Clear()
        DataTableEdit.Merge(_dt, False, MissingSchemaAction.Add)
      End If

    End If
  End Sub

  ''' <summary>
  ''' Function to compare the edit and read allowedCurrencies datatables.
  ''' </summary>
  ''' <param name="dtRead"></param>
  ''' <param name="dtEdit"></param>
  ''' <param name="sort"></param>
  ''' <param name="TypeTable"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function CompareDataTables(dtRead As DataTable, dtEdit As DataTable, sort As String, Optional TypeTable As Integer = 0) As DataTable
    Dim _dt_merge As DataTable
    Dim _dt_small As DataTable
    Dim _dt_big As DataTable
    Dim _dv_small As DataView
    Dim _dv_big As DataView
    Dim _is_insert_row As Boolean
    Dim _dr_new_datatable As DataRow
    Dim _row_table_1 As Object
    Dim _row_table_2 As Object
    Dim _less_row_edit As Boolean

    Try

      _dt_merge = CreateDatatable(TypeTable)
      _dt_small = New DataTable()
      _dt_big = New DataTable()


      If dtRead.Rows.Count > dtEdit.Rows.Count Then
        _dt_big = dtEdit.Copy()
        _dt_small = dtEdit.Copy()
        _less_row_edit = True
      Else
        _dt_big = dtEdit.Copy()
        _dt_small = dtRead.Copy()
        _less_row_edit = False
      End If

      _dv_small = _dt_small.DefaultView
      _dv_small.Sort = sort & " DESC"
      _dt_small = _dv_small.ToTable()

      _dv_big = _dt_big.DefaultView
      _dv_big.Sort = sort & " DESC"
      _dt_big = _dv_big.ToTable()

      For Each _row_big As DataRow In _dt_big.Rows
        _is_insert_row = False
        _dr_new_datatable = _dt_merge.NewRow()
        For Each _row_small As DataRow In _dt_small.Rows
          _row_table_1 = _row_big.ItemArray
          _row_table_2 = _row_small.ItemArray
          If CompareItems(_row_table_1, _row_table_2, _dt_big.Columns.Count) Then
            _dr_new_datatable = _row_small
            _dt_merge.ImportRow(_dr_new_datatable)
            _is_insert_row = True
            Exit For
          End If
        Next
        If Not _is_insert_row Then
          If (TypeTable = 0) Then
            _dr_new_datatable.Item(0) = _row_big.Item(0)
            _dr_new_datatable.Item(1) = _row_big.Item(1)
            _dr_new_datatable.Item(2) = _row_big.Item(2)
            _dr_new_datatable.Item(3) = _row_big.Item(3)
            _dr_new_datatable.Item(4) = 0
            _dr_new_datatable.Item(5) = 0
            _dt_merge.Rows.Add(_dr_new_datatable)
          Else

            If (_less_row_edit) Then
              _dr_new_datatable.Item(0) = _row_big.Item(0)
              _dr_new_datatable.Item(1) = _row_big.Item(1)
              _dr_new_datatable.Item(2) = _row_big.Item(2)
            Else
              _dr_new_datatable.Item(0) = _row_big.Item(0)
              _dr_new_datatable.Item(1) = 0
              _dr_new_datatable.Item(2) = 0
            End If
            _dt_merge.Rows.Add(_dr_new_datatable)
          End If

        End If

      Next

      Return _dt_merge

    Catch exception As Exception
      _dt_merge = New DataTable()

      Return _dt_merge
    End Try
  End Function

  ''' <summary>
  ''' Function to compare each item of allowed currencies datatables.
  ''' </summary>
  ''' <param name="array1"></param>
  ''' <param name="array2"></param>
  ''' <param name="limitColumns"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function CompareItems(ByVal array1 As Object, ByVal array2 As Object, ByVal limitColumns As Integer) As Boolean
    Dim _counter As Integer

    For _counter = 0 To 3
      If (_counter >= limitColumns) Then
        Exit For
      End If
      If Not (array1(_counter).Equals(array2(_counter))) Then
        Return False
      End If

    Next

    Return True
  End Function

  ''' <summary>
  ''' Function to create a datatable with the structure of bet Currencies or closing stocks
  ''' </summary>
  ''' <param name="TypeTable"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function CreateDatatable(Optional TypeTable As Integer = 0) As DataTable
    Dim _default_table As DataTable

    If (TypeTable = 0) Then
      _default_table = New DataTable("CLOSING_STOCKS")
      _default_table.Columns.Add("ISO_CODE", GetType(System.String))
      _default_table.Columns.Add("DENOMINATION", GetType(System.Decimal))
      _default_table.Columns.Add("CAGE_CURRENCY_TYPE", GetType(CageCurrencyType))
      _default_table.Columns.Add("CHIP_ID", GetType(System.Int64))
      _default_table.Columns.Add("QUANTITY", GetType(System.Int32))
      _default_table.Columns.Add("TOTAL", GetType(System.Decimal))
    Else
      _default_table = New DataTable("Table")
      _default_table.Columns.Add("GTB_ISO_CODE", GetType(System.String))
      _default_table.Columns.Add("GTB_MAX_BET", GetType(System.Decimal))
      _default_table.Columns.Add("GTB_MIN_BET", GetType(System.Decimal))
    End If

    Return _default_table
  End Function


  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    m_gaming_table = Me.DbReadObject

    ' Set virtual cashier ID
    Me.VirtualCashierID = Cashier.ReadTerminalId(m_gaming_table.Name, GU_USER_TYPE.SYS_GAMING_TABLE)

    ' Code
    Me.txt_code.Value = m_gaming_table.Code

    ' Name
    Me.txt_name.Value = m_gaming_table.Name

    ' Table Types
    If Not String.IsNullOrEmpty(m_gaming_table.TypeName) Then
      Me.cmb_table_type.TextValue = m_gaming_table.TypeName
    Else
      Me.cmb_table_type.SelectedIndex = 0
    End If

    ' Zone
    Me.ef_smoking.Value = GLB_NLS_GUI_CONFIGURATION.GetString(437)

    ' Area and Bank id 0 always exist with text value '---'
    ' Area
    Me.cmb_area.SelectedIndex = 0
    If m_gaming_table.AreaID <> Nothing Then
      Me.cmb_area.Value = m_gaming_table.AreaID
    End If
    m_gaming_table.AreaName = Me.cmb_area.TextValue

    ' Bank 
    Me.cmb_bank.SelectedIndex = 0
    If m_gaming_table.BankID <> Nothing Then
      Me.cmb_bank.Value = m_gaming_table.BankID
    End If
    m_gaming_table.BankName = Me.cmb_bank.TextValue

    ' Floor Id
    Me.ef_floor_id.Value = m_gaming_table.FloorID

    ' Enabled
    Me.chk_enabled.Checked = m_gaming_table.Enabled

    'Set Cashier 
    Call SetCashier()

    ' Description
    Me.txt_description.Text = m_gaming_table.Description

    ' Table Speed Slow
    Me.ef_table_speed_slow.Value = m_gaming_table.TableSpeedSlow

    ' Table Speed Normal
    Me.ef_table_speed_normal.Value = m_gaming_table.TableSpeedNormal

    ' Table Speed Fast
    Me.ef_table_speed_fast.Value = m_gaming_table.TableSpeedFast


    ' Management drop box
    If Me.chk_integrated.Checked Then
      chk_management_drop_box.Enabled = False
    End If
    Me.chk_management_drop_box.Checked = m_gaming_table.DropBoxEnabled

    ' Table Num Seats
    If m_gaming_table.GamingTableID <> 0 Then
      Me.ef_table_num_seats.IsReadOnly = True
    End If

    Me.ef_table_num_seats.Value = m_gaming_table.TableNumSeats

    ' ProviderID 
    If m_gaming_table.ProviderID >= 0 Then
      Me.cmb_provider.Value = m_gaming_table.ProviderID
    Else
      Me.cmb_provider.TextValue = String.Empty
    End If

    'Table Idle Plays
    Me.ef_table_idle_plays.Value = m_gaming_table.TableIdlePlays
    Me.lbl_idle_info.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5589, Me.ef_table_idle_plays.Value)

    ' Theoric Hold
    Me.ef_theoric_hold.Value = m_gaming_table.TheoricHold

    ' Preview table design
    If GetShowTableDesign() Then
      panel_table_design.LoadGamingTable(m_gaming_table.TableDesignXml)
      panel_table_design.GetXMLGamingTable(m_gaming_table.TableDesignXml)
    End If


    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then

      For Each _row As DataRow In m_gaming_table.ChipsAllowed.Rows
        Call SetDataTableChipTypes(_row("GTCS_CHIPS_SET_ID"), True)
        Call SetGridInTabs(_row("GTCS_CHIPS_SET_ID"))
      Next
    End If

    Me.cmb_operation_type.SelectedIndex = m_gaming_table.ClosingStock.Type
    Me.chk_operation_type.Checked = m_gaming_table.ClosingStock.SleepsOnTable
    Me.m_dt_bet_currencies = m_gaming_table.BetByCurrencies

  End Sub 'GUI_SetScreenData

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        Me.DbEditedObject = New CLASS_GAMING_TABLE
        m_gaming_table = DbEditedObject
        m_gaming_table.ProviderID = -1

        Me.DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          If Me.DbStatus = ENUM_STATUS.STATUS_NOT_FOUND Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4302), ENUM_MB_TYPE.MB_TYPE_ERROR)
          Else
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4301), ENUM_MB_TYPE.MB_TYPE_ERROR)
          End If

        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          If Me.DbStatus = ENUM_STATUS.STATUS_DEPENDENCIES Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4385), ENUM_MB_TYPE.MB_TYPE_ERROR)
          ElseIf DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4353), ENUM_MB_TYPE.MB_TYPE_ERROR)
          ElseIf Me.DbStatus = ENUM_STATUS.STATUS_NOT_SUPPORTED Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3432), ENUM_MB_TYPE.MB_TYPE_ERROR)
          Else
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4303), ENUM_MB_TYPE.MB_TYPE_ERROR)
          End If
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          If Me.DbStatus = ENUM_STATUS.STATUS_DEPENDENCIES Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4385), ENUM_MB_TYPE.MB_TYPE_ERROR)
          ElseIf Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4353), ENUM_MB_TYPE.MB_TYPE_ERROR)
          ElseIf Me.DbStatus = ENUM_STATUS.STATUS_NOT_FOUND Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4302), ENUM_MB_TYPE.MB_TYPE_ERROR)
          ElseIf Me.DbStatus = ENUM_STATUS.STATUS_NOT_SUPPORTED Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3432), ENUM_MB_TYPE.MB_TYPE_ERROR)
          ElseIf Me.DbStatus = ENUM_STATUS.STATUS_ALREADY_EXISTS Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4307), ENUM_MB_TYPE.MB_TYPE_WARNING)
          Else
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4304), ENUM_MB_TYPE.MB_TYPE_ERROR)
          End If
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4305), ENUM_MB_TYPE.MB_TYPE_ERROR)
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)
    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE: Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.txt_code

  End Sub 'GUI_SetInitialFocus

  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As GUI_Controls.CLASS_GUI_USER.TYPE_PERMISSIONS)
    MyBase.GUI_Permissions(AndPerm)

    ' TODO
    'Dim _gaming_table As CLASS_GAMING_TABLE
    '_gaming_table = DbReadObject

    AndPerm.Write = chk_enabled.Enabled
  End Sub

#End Region   ' Overrides

#Region "Events"

  ' PURPOSE: Area selection
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub cmb_area_ValueChangedEvent()
    Dim _table As DataTable

    'Zone (smoking - no smoking)
    Me.ef_smoking.Value = GLB_NLS_GUI_CONFIGURATION.GetString(437)

    _table = GUI_GetTableUsingSQL("SELECT AR_SMOKING FROM AREAS WHERE AR_AREA_ID = " & cmb_area.Value, Integer.MaxValue)

    If Not IsNothing(_table) Then
      If _table.Rows.Count() > 0 Then
        If _table.Rows.Item(0).Item("AR_SMOKING") Then
          Me.ef_smoking.Value = GLB_NLS_GUI_CONFIGURATION.GetString(436)
        End If
      End If
    End If

    'Banks
    Call BankComboFill()

  End Sub

  ' PURPOSE: Integrated Cashier selection
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub chk_integrated_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    Call CheckInegratedCheckedChanged()

  End Sub

  ' PURPOSE: Integrated Cashier checks selection
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub CheckInegratedCheckedChanged()
    ' Check Cage Pending Movements
    m_gaming_table = Me.DbReadObject

    Call RemoveHandles()

    Using DB_TRX As New DB_TRX

      If m_gaming_table.ExistCagePendingMovements(DB_TRX.SqlTransaction) Then
        ' Warning Message
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4363), ENUM_MB_TYPE.MB_TYPE_WARNING)
        ' Set original value
        If Me.m_gaming_table.IntegratedCashier Then
          Me.chk_integrated.Checked = True
        Else
          Me.chk_integrated.Checked = False
        End If
      Else
        'Call EnableDisableCmbCashiers()
        If TITO.Utils.IsTitoMode Then
          If Me.chk_integrated.Checked Then
            Me.cmb_cashiers.Enabled = True
          Else
            Me.cmb_cashiers.SelectedValue = -1
            Me.cmb_cashiers.Enabled = False
          End If
        End If
      End If

    End Using

    If Me.chk_integrated.Checked Then
      chk_management_drop_box.Enabled = False
      chk_management_drop_box.Checked = False
    Else
      chk_management_drop_box.Enabled = True
    End If

    Call AddHandles()

  End Sub

  ' PURPOSE: Ignore [ENTER] key when editing comments
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    If e.KeyChar = Chr(Keys.Enter) And Me.txt_description.ContainsFocus Then
      Return True
    End If

    Return MyBase.GUI_KeyPress(sender, e)

  End Function ' GUI_KeyPress

  Private Sub ef_table_num_seats_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Dim _max_seats As Int32
    If Not String.IsNullOrEmpty(ef_table_num_seats.Value) And GUI_ParseNumber(ef_table_num_seats.Value) >= 0 Then
      _max_seats = GeneralParam.GetInt32("GamingTables", "Seats.MaxAllowedSeats", 10)
      If ef_table_num_seats.Value > _max_seats Then
        ef_table_num_seats.Value = _max_seats
      End If
      panel_table_design.UpdateSeatsTable(ef_table_num_seats.Value)
    End If
  End Sub

  Private Sub ef_table_idle_plays_EntryFieldValueChanged()
    Me.lbl_idle_info.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5589, Me.ef_table_idle_plays.Value)
  End Sub

  ' PURPOSE: Count check grid active for chip sets
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Function CountCheckGrid(ByVal tabPageIndex) As Integer
    Dim _count As Integer
    Dim _grid As uc_grid

    _count = 0
    _grid = m_gridsList.Item(tabPageIndex)

    With _grid
      For _index As Integer = 0 To .NumRows - 1
        If (.Cell(_index, GRID_COLUMN_ALLOWED).Value = uc_grid.GRID_CHK_CHECKED) Then
          _count = _count + 1
        End If
      Next
    End With

    Return _count
  End Function

  ' PURPOSE: Handler to control that restrictions are followed
  '
  '  PARAMS:
  '     - INPUT:
  '       - Row To Change
  '       - Col To Change
  '       - EditionCanceled Boolean to aplly change or not
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub grid_CellDataChanged(ByVal Row As Integer, ByVal Col As Integer)
    If (m_gridsList(tab_chipsSets.SelectedIndex).Cell(Row, GRID_COLUMN_ALLOWED).Value = uc_grid.GRID_CHK_CHECKED) Then
      SetDataTableChipTypes(m_gridsList(tab_chipsSets.SelectedIndex).Cell(Row, GRID_COLUMN_INDEX).Value, True)
    Else
      SetDataTableChipTypes(m_gridsList(tab_chipsSets.SelectedIndex).Cell(Row, GRID_COLUMN_INDEX).Value, False)
    End If

  End Sub ' grid_CellDataChanged

#End Region      ' Events 



End Class