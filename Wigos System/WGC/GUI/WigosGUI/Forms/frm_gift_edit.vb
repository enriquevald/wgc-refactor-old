'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_gift_edit
'
' DESCRIPTION : New or Edit Gift item 
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 23-SEP-2010  TJG    Initial version
' 18-OCT-2010  TJG    Gift stock 
' 28-OCT-2010  TJG    Exchange points for draw numbers
' 10-MAY-2012  MPO    Added the image and description of the Gift
' 3-MAY-2013   MAR    Took away the option for Draw Numbers
' 6-MAY-2013   MAR    Added daily and monthly clicne tand global limits to gift giving
' 18-SEP-2013  DRV    Added Points for level
' 09-OCT-2013  ANG    Support for gift which  belongs to 'redemption table'
' 14-OCT-2013  ANG    Fix bug WIG-64
' 07-APR-2014  JBP    Added only VIP property.
' 20-JAN-2015  FJC    Set features form (size and autoscroll panels) when size's form exceeds desktop area
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports WSI.Common
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data

Public Class frm_gift_edit
  Inherits frm_base_edit

#Region " Constants "
  Private Const LEN_PROMO_DIGITS As Integer = 8
  Private Const FORM_DB_MIN_VERSION As Short = 149 ''' TODO CHECK FOR RIGHT DB VERSION!!!

  Private Const LEN_DRAW_PROMOBOX_TEXT_NAME As Integer = 50
#End Region ' Constants

#Region " Members "

  Private m_input_gift_name As String
  Private m_delete_operation As Boolean

#End Region ' Members

#Region " Overrides "

  ' PURPOSE : Initializes the form id.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_GIFTS_EDIT
    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE : Form controls initialization.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS  :

  Protected Overrides Sub GUI_InitControls()
    ' Required by the base class
    Call MyBase.GUI_InitControls()

    ' Set form Name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(352)                        ' 352 "Edici�n de Regalo"

    ' Delete button is only available whe editing, not while creating
    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = (Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT)

    ' Gift Fields
    '   - Name
    '   - Points
    '   - Type
    '   - Status
    '   - Current Stock

    '   - Name
    ef_gift_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(353)              ' 353 "Nombre"
    ef_gift_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, CLASS_GIFT.MAX_NAME_LEN)

    '   - Description and Image
    tab_description.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(688)           ' 688 "Description"
    txt_description.MaxLength = CLASS_GIFT.MAX_DESCRIPTION_LEN

    '   - PromoBox
    tab_promobox.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(722) ' 722 "PromoBOX"
    chk_redeem_on_promobox.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4917)  ' 4917 "Canjeable"
    ef_text_on_promobox.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2870)     ' 2870 "Display text"
    ef_text_on_promobox.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_DRAW_PROMOBOX_TEXT_NAME)


    '   - Images
    tab_icon.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(719)                  ' 719 "Icono" 
    tab_image.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(687)                 ' 687 "Imagen"       
    img_icon_gift.Init(uc_image.MAXIMUN_RESOLUTION.x200X150)
    img_image_gift.Init(uc_image.MAXIMUN_RESOLUTION.x400X300)


    lbl_belongs_to_points_to_credit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2716) '' 2716 "Belongs to redemption table"
    '   - Points
    ' 2622 "Puntos por Nivel"
    gb_points.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2622)

    chk_points_level1.Text = GeneralParam.GetString("PlayerTracking", "Level01.Name")
    If chk_points_level1.Text.Length > 9 Then
      chk_points_level1.Text = chk_points_level1.Text.Remove(9) & "..."
    End If
    chk_points_level2.Text = GeneralParam.GetString("PlayerTracking", "Level02.Name")
    If chk_points_level2.Text.Length > 9 Then
      chk_points_level2.Text = chk_points_level2.Text.Remove(9) & "..."
    End If
    chk_points_level3.Text = GeneralParam.GetString("PlayerTracking", "Level03.Name")
    If chk_points_level3.Text.Length > 9 Then
      chk_points_level3.Text = chk_points_level3.Text.Remove(9) & "..."
    End If
    chk_points_level4.Text = GeneralParam.GetString("PlayerTracking", "Level04.Name")
    If chk_points_level4.Text.Length > 9 Then
      chk_points_level4.Text = chk_points_level4.Text.Remove(9) & "..."
    End If

    ef_points_level1.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8)
    ef_points_level2.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8)
    ef_points_level3.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8)
    ef_points_level4.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8)

    lbl_info_points_level.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2633)

    '   - Type
    gb_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(355)                   ' 355 "Tipo"
    opt_type_object.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(356)           ' 356 "Objeto"
    opt_type_nrc.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(357)              ' 357 "Cr�ditos No Redimibles"
    ef_nrc.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)
    opt_type_redeem_credit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(313)    ' 313 "Cr�ditos Redimibles"
    ef_redeem_credit.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)
    opt_type_services.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(396)         ' 396 "Servicios"

    '   - Status
    gb_available.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(358)              ' 358 "Disponible"
    opt_status_available.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(359)      ' 359 "S�"
    opt_status_not_available.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(360)  ' 360 "No"

    ' JBP 07-APR-2014: Gift for VIP's
    Me.chk_only_vip.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4804)          ' 4804 "Exclusivo para clientes VIP" 

    '   - Current Stock
    ef_current_stock.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(381)            ' 381 "Stock"
    ef_current_stock.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 6, 0)

    Me.lbl_icon_optimun_size.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(733, "200", "150")
    Me.lbl_image_optimun_size.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(733, "400", "300")

    ' tab Promotion limits
    tab_by_customer.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1928)              ' L�mite Cliente
    tab_by_promotion.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1929)             ' L�mite Regalo

    lbl_daily_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1932)             ' Sin l�mite por cliente y jornada
    lbl_monthly_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1933)           ' Sin l�mite por cliente y mes
    lbl_promotion_daily_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1934)   ' Sin l�mite por jornada y regalo
    lbl_promotion_monthly_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1935) ' Sin l�mite por mes y regalo

    ef_gift_limit_day.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1930)            ' L�mite por jornada
    ef_gift_limit_month.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1931)          ' L�mite mensual
    ef_daily_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1930)               ' L�mite por jornada
    ef_monthly_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1931)             ' L�mite mensual

    ef_gift_limit_day.TextVisible = True
    ef_gift_limit_month.TextVisible = True
    ef_daily_limit.TextVisible = True
    ef_monthly_limit.TextVisible = True

    ef_gift_limit_day.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, LEN_PROMO_DIGITS)
    ef_gift_limit_month.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, LEN_PROMO_DIGITS)
    ef_daily_limit.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, LEN_PROMO_DIGITS)
    ef_monthly_limit.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, LEN_PROMO_DIGITS)

  End Sub ' GUI_InitControls

  ' PURPOSE : Validate the data presented on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    ' The following values must be entered
    '   - Name : Mandatory
    '   - Points : Mandatory
    '   - Not Redeemable Credits : Only when type is NRC
    '   - Stock : Only when type is OBJECT

    '   - Name : Mandatory
    If ef_gift_name.Value = "" Then
      ' 118 "Debe introducir un valor para %1."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_gift_name.Text)
      Call ef_gift_name.Focus()

      Return False
    End If


    If Not chk_points_level1.Checked And Not chk_points_level2.Checked And Not chk_points_level3.Checked And Not chk_points_level4.Checked Then
      ' 118 "Debe introducir un valor para %1."
      ' 2622 "Puntos por Nivel"
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2622))
      ef_points_level1.Focus()

      Return False
    End If

    If chk_points_level1.Checked And GUI_ParseNumber(ef_points_level1.Value) <= 0 Then
      ' 118 "Debe introducir un valor para %1."
      ' 2624 "Puntos Nivel %1"
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2624, GeneralParam.GetString("PlayerTracking", "Level01.Name")))
      ef_points_level1.Focus()

      Return False
    End If
    If chk_points_level2.Checked And GUI_ParseNumber(ef_points_level2.Value) <= 0 Then
      ' 118 "Debe introducir un valor para %1."
      ' 2624 "Puntos Nivel %1"
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2624, GeneralParam.GetString("PlayerTracking", "Level02.Name")))
      ef_points_level2.Focus()

      Return False
    End If
    If chk_points_level3.Checked And GUI_ParseNumber(ef_points_level3.Value) <= 0 Then
      ' 118 "Debe introducir un valor para %1."
      ' 2624 "Puntos Nivel %1"
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2624, GeneralParam.GetString("PlayerTracking", "Level03.Name")))
      ef_points_level3.Focus()

      Return False
    End If
    If chk_points_level4.Checked And GUI_ParseNumber(ef_points_level4.Value) <= 0 Then
      ' 118 "Debe introducir un valor para %1."
      ' 2624 "Puntos Nivel %1"
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2624, GeneralParam.GetString("PlayerTracking", "Level04.Name")))
      ef_points_level4.Focus()

      Return False
    End If

    '   - Points : Mandatory 
    If GUI_ParseNumber(ef_points_level1.Value) < 0 And GUI_ParseNumber(ef_points_level2.Value) < 0 And _
                GUI_ParseNumber(ef_points_level3.Value) < 0 And GUI_ParseNumber(ef_points_level4.Value) < 0 Then
      ' 118 "Debe introducir un valor para %1."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2622))
      Call ef_points_level1.Focus()

      Return False
    End If

    '   - Not Redeemable Credits : Only when type is NRC
    If opt_type_nrc.Checked And GUI_ParseNumber(ef_nrc.Value) <= 0 Then
      ' 118 "Debe introducir un valor para %1."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , opt_type_nrc.Text)
      Call ef_nrc.Focus()

      Return False
    End If

    '   - Redeemable Credits : Only when type is RedeemCredit
    If opt_type_redeem_credit.Checked And GUI_ParseNumber(ef_redeem_credit.Value) <= 0 Then
      ' 118 "Debe introducir un valor para %1."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , opt_type_redeem_credit.Text)
      Call ef_redeem_credit.Focus()

      Return False
    End If

    Return True

  End Function ' GUI_IsScreenDataOk

  ' PURPOSE : Set initial data on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _gift As CLASS_GIFT

    _gift = DbReadObject

    ef_gift_name.Value = _gift.Name

    lbl_belongs_to_points_to_credit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2716) '' 2716 "Belongs to redemption table"
    lbl_belongs_to_points_to_credit.Value += " " & _gift.ChangeTableName
    lbl_belongs_to_points_to_credit.Visible = _gift.BelongsToRedemptionTable

    If _gift.BelongsToRedemptionTable Then
      Call DiallowControlsOnRedemptionTableGift()
    End If

    'if points for level = -1 it means that the gift it's not available for that level, in the DB it's saved as NULL
    If (_gift.PointsLevel1 = -1) Then
      ef_points_level1.Value = ""
      chk_points_level1.Checked = False
    Else
      ef_points_level1.Value = _gift.PointsLevel1
      chk_points_level1.Checked = True
    End If
    ef_points_level1.Enabled = chk_points_level1.Checked

    If (_gift.PointsLevel2 = -1) Then
      ef_points_level2.Value = ""
      chk_points_level2.Checked = False
    Else
      ef_points_level2.Value = _gift.PointsLevel2
      chk_points_level2.Checked = True
    End If
    ef_points_level2.Enabled = chk_points_level2.Checked

    If (_gift.PointsLevel3 = -1) Then
      ef_points_level3.Value = ""
      chk_points_level3.Checked = False
    Else
      ef_points_level3.Value = _gift.PointsLevel3
      chk_points_level3.Checked = True
    End If
    ef_points_level3.Enabled = chk_points_level3.Checked

    If (_gift.PointsLevel4 = -1) Then
      ef_points_level4.Value = ""
      chk_points_level4.Checked = False
    Else
      ef_points_level4.Value = _gift.PointsLevel4
      chk_points_level4.Checked = True
    End If
    ef_points_level4.Enabled = chk_points_level4.Checked

    Select Case _gift.Type
      Case CLASS_GIFT.ENUM_GIFT_TYPE.OBJECT_GIFT

        ef_current_stock.IsReadOnly = False
        ef_current_stock.TabStop = True
        ef_current_stock.Value = _gift.CurrentStock

        ef_nrc.IsReadOnly = True
        ef_nrc.TabStop = False
        ef_nrc.Value = ""

        ef_redeem_credit.IsReadOnly = True
        ef_redeem_credit.TabStop = False
        ef_redeem_credit.Value = ""

        opt_type_object.Checked = True

      Case CLASS_GIFT.ENUM_GIFT_TYPE.NOT_REDEEMABLE_CREDIT

        ef_nrc.IsReadOnly = False
        ef_nrc.TabStop = True
        ef_nrc.Value = _gift.ConversionToNRC

        ef_redeem_credit.IsReadOnly = True
        ef_redeem_credit.TabStop = False
        ef_redeem_credit.Value = ""

        ef_current_stock.IsReadOnly = True
        ef_current_stock.TabStop = False
        ef_current_stock.Value = ""

        opt_type_nrc.Checked = True

      Case CLASS_GIFT.ENUM_GIFT_TYPE.REDEEMABLE_CREDIT

        ef_nrc.IsReadOnly = True
        ef_nrc.TabStop = False
        ef_nrc.Value = ""

        ef_redeem_credit.IsReadOnly = False
        ef_redeem_credit.TabStop = True
        ef_redeem_credit.Value = _gift.ConversionToNRC

        ef_current_stock.IsReadOnly = True
        ef_current_stock.TabStop = False
        ef_current_stock.Value = ""

        opt_type_redeem_credit.Checked = True

      Case CLASS_GIFT.ENUM_GIFT_TYPE.DRAW_NUMBERS

        ef_nrc.IsReadOnly = True
        ef_nrc.TabStop = False
        ef_nrc.Value = ""

        ef_redeem_credit.IsReadOnly = True
        ef_redeem_credit.TabStop = False
        ef_redeem_credit.Value = ""

        ef_current_stock.IsReadOnly = True
        ef_current_stock.TabStop = False
        ef_current_stock.Value = ""

      Case CLASS_GIFT.ENUM_GIFT_TYPE.SERVICES

        ef_nrc.IsReadOnly = True
        ef_nrc.TabStop = False
        ef_nrc.Value = ""

        ef_redeem_credit.IsReadOnly = True
        ef_redeem_credit.TabStop = False
        ef_redeem_credit.Value = ""

        ef_current_stock.IsReadOnly = True
        ef_current_stock.TabStop = False
        ef_current_stock.Value = ""

        opt_type_services.Checked = True

      Case Else
        chk_points_level1.Enabled = False
        chk_points_level2.Enabled = False
        chk_points_level3.Enabled = False
        chk_points_level4.Enabled = False
        ef_points_level1.Enabled = False
        ef_points_level2.Enabled = False
        ef_points_level3.Enabled = False
        ef_points_level4.Enabled = False
        ef_points_level1.Value = ""
        ef_points_level2.Value = ""
        ef_points_level3.Value = ""
        ef_points_level4.Value = ""

    End Select

    opt_status_available.Checked = _gift.Available
    opt_status_not_available.Checked = Not _gift.Available

    img_icon_gift.Image = _gift.Icon
    img_image_gift.Image = _gift.Image
    txt_description.Text = _gift.Description

    ef_daily_limit.Value = CStr(_gift.AccountDailyLimit)
    ef_monthly_limit.Value = CStr(_gift.AccountMonthlyLimit)
    ef_gift_limit_day.Value = CStr(_gift.GlobalDailyLimit)
    ef_gift_limit_month.Value = CStr(_gift.GlobalMonthlyLimit)

    opt_type_nrc.Enabled = False
    ef_nrc.Enabled = False

    opt_type_redeem_credit.Enabled = False
    ef_redeem_credit.Enabled = False

    If (GUI_FormatNumber(GUI_ParseNumber(ef_daily_limit.Value)) > 0) Then
      lbl_daily_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1939, GUI_FormatNumber(GUI_ParseNumber(ef_daily_limit.Value), 0))
    Else
      lbl_daily_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1932)
    End If

    If (GUI_FormatNumber(GUI_ParseNumber(ef_monthly_limit.Value)) > 0) Then
      lbl_monthly_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1940, GUI_FormatNumber(GUI_ParseNumber(ef_monthly_limit.Value), 0))
    Else
      lbl_monthly_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1933)
    End If

    If (GUI_FormatNumber(GUI_ParseNumber(ef_gift_limit_day.Value)) > 0) Then
      lbl_promotion_daily_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1941, GUI_FormatNumber(GUI_ParseNumber(ef_gift_limit_day.Value), 0))
    Else
      lbl_promotion_daily_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1934)
    End If

    If (GUI_FormatNumber(GUI_ParseNumber(ef_gift_limit_month.Value)) > 0) Then
      lbl_promotion_monthly_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1942, GUI_FormatNumber(GUI_ParseNumber(ef_gift_limit_month.Value), 0))
    Else
      lbl_promotion_monthly_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1935)
    End If

    chk_redeem_on_promobox.Checked = _gift.AwardOnPromoBOX
    ef_text_on_promobox.Value = _gift.TextOnPromoBOX

    ' JBP 07-APR-2014: Gift for Vips
    Me.chk_only_vip.Checked = _gift.IsVip

  End Sub ' GUI_SetScreenData

  ' PURPOSE : Get data from the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_GetScreenData()

    Dim _gift As CLASS_GIFT

    _gift = DbEditedObject

    _gift.Name = ef_gift_name.Value

    If opt_type_object.Checked Then
      _gift.Type = CLASS_GIFT.ENUM_GIFT_TYPE.OBJECT_GIFT
      _gift.ConversionToNRC = 0
      _gift.CurrentStock = IIf(ef_current_stock.Value = "", 0, ef_current_stock.Value)
    ElseIf opt_type_nrc.Checked Then
      _gift.Type = CLASS_GIFT.ENUM_GIFT_TYPE.NOT_REDEEMABLE_CREDIT
      _gift.ConversionToNRC = IIf(ef_nrc.Value = "", 0, ef_nrc.Value)
      _gift.CurrentStock = 0
    ElseIf opt_type_redeem_credit.Checked Then
      _gift.Type = CLASS_GIFT.ENUM_GIFT_TYPE.REDEEMABLE_CREDIT
      _gift.ConversionToNRC = IIf(ef_redeem_credit.Value = "", 0, ef_redeem_credit.Value)
      _gift.CurrentStock = 0
    ElseIf opt_type_services.Checked Then
      _gift.Type = CLASS_GIFT.ENUM_GIFT_TYPE.SERVICES
      _gift.ConversionToNRC = 0
      _gift.CurrentStock = 0
    End If

    'if points for level = -1 it means that the gift it's not available for that level in the DB it's saved as NULL
    _gift.PointsLevel1 = IIf(chk_points_level1.Checked And ef_points_level1.Value <> "", ef_points_level1.Value, -1)
    _gift.PointsLevel2 = IIf(chk_points_level2.Checked And ef_points_level2.Value <> "", ef_points_level2.Value, -1)
    _gift.PointsLevel3 = IIf(chk_points_level3.Checked And ef_points_level3.Value <> "", ef_points_level3.Value, -1)
    _gift.PointsLevel4 = IIf(chk_points_level4.Checked And ef_points_level4.Value <> "", ef_points_level4.Value, -1)

    _gift.Available = opt_status_available.Checked
    If opt_status_available.Checked Then
      _gift.Available = CLASS_GIFT.ENUM_GIFT_STATUS.AVAILABLE
    Else
      _gift.Available = CLASS_GIFT.ENUM_GIFT_STATUS.NOT_AVAILABLE
    End If

    _gift.Icon = img_icon_gift.Image
    _gift.Image = img_image_gift.Image
    _gift.Description = txt_description.Text

    _gift.AccountDailyLimit = IIf(ef_daily_limit.Value = "", 0, ef_daily_limit.Value)
    _gift.AccountMonthlyLimit = IIf(ef_monthly_limit.Value = "", 0, ef_monthly_limit.Value)
    _gift.GlobalDailyLimit = IIf(ef_gift_limit_day.Value = "", 0, ef_gift_limit_day.Value)
    _gift.GlobalMonthlyLimit = IIf(ef_gift_limit_month.Value = "", 0, ef_gift_limit_month.Value)

    ' JMM 30-APR-2014
    ' Redeemable on PromoBOX
    _gift.AwardOnPromoBOX = Me.chk_redeem_on_promobox.Checked
    ' Name on PromoBOX
    _gift.TextOnPromoBOX = Me.ef_text_on_promobox.Value

    ' JBP 07-APR-2014: Gift for Vips
    _gift.IsVip = Me.chk_only_vip.Checked

  End Sub ' GUI_GetScreenData

  ' PURPOSE : Database overridable operations. 
  '           Define specific DB operation for this form.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Dim _gift As CLASS_GIFT
    Dim _aux_nls As String
    Dim _rc As mdl_NLS.ENUM_MB_RESULT

    If Me.DbStatus = ENUM_STATUS.STATUS_NOT_SUPPORTED Then
      '' Min database version popup alerdy showed. Don't show any message again
      Exit Sub
    End If

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_GIFT
        _gift = DbEditedObject
        _gift.GiftId = 0
        _gift.Type = CLASS_GIFT.ENUM_GIFT_TYPE.OBJECT_GIFT
        _gift.Available = True

        DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          ' 127 "Se ha producido un error al leer el regalo %1."
          _gift = Me.DbEditedObject
          _aux_nls = m_input_gift_name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(127), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          ' 125 "Ya existe una regalo con el nombre %1."
          _gift = Me.DbEditedObject
          _aux_nls = _gift.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(125), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
          Call ef_gift_name.Focus()
        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          ' 128 "Se ha producido un error al a�adir el regalo %1."
          _gift = Me.DbEditedObject
          _aux_nls = _gift.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(128), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          ' 125 "Ya existe una regalo con el nombre %1."
          _gift = Me.DbEditedObject
          _aux_nls = _gift.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(125), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
          Call ef_gift_name.Focus()

        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          ' 126 "Se ha producido un error al modificar el regalo %1."
          _gift = Me.DbEditedObject
          _aux_nls = _gift.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(126), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE
        m_delete_operation = False
        ' 130 "�Est� seguro que quiere borrar el regalo %1?"
        _gift = Me.DbEditedObject
        _aux_nls = _gift.Name
        _rc = NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(130), _
                         ENUM_MB_TYPE.MB_TYPE_WARNING, _
                         ENUM_MB_BTN.MB_BTN_YES_NO, _
                         ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
                        _aux_nls)

        If _rc = ENUM_MB_RESULT.MB_RESULT_YES Then
          m_delete_operation = True
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus = ENUM_STATUS.STATUS_DEPENDENCIES And m_delete_operation Then
          ' 124 "No se puede borrar el regalo %1 porque tiene entregas pendientes."
          _gift = Me.DbEditedObject
          _aux_nls = _gift.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(124), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)

        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK And m_delete_operation Then
          ' 129 "Se ha producido un error al borrar el regalo %1."
          _gift = Me.DbEditedObject
          _aux_nls = _gift.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(129), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_DELETE
        If m_delete_operation Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        Else
          DbStatus = ENUM_STATUS.STATUS_ERROR
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE : Manage permissions.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)
  End Sub ' GUI_Permissions

  ' PURPOSE : Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.ef_gift_name

  End Sub ' GUI_SetInitialFocus

  ' PURPOSE : Init form in new mode
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '
  Public Overloads Sub ShowNewItem()

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    DbObjectId = Nothing
    DbStatus = ENUM_STATUS.STATUS_OK

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowNewItem

  ' PURPOSE : Init form in edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '       - UserId
  '       - Username
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Public Overloads Sub ShowEditItem(ByVal GiftId As Integer, ByVal GiftName As String)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Me.DbObjectId = GiftId
    Me.m_input_gift_name = GiftName

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If
  End Sub ' ShowEditItem

  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean
    If Me.txt_description.Focused Then
      Return True

    Else

      Return False
    End If

    '
  End Function

#End Region ' Overrides

#Region " Private "

  ' PURPOSE : Disallow all controls and Control event handler when gift belongs to RedemptionTable
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub DiallowControlsOnRedemptionTableGift()

    '' Controls to keep Enabled.
    Dim _exception_list As New List(Of String)(New String() {"lbl_belongs_to_points_to_credit" _
                                                                , "TabControl1" _
                                                                , "btn_delete" _
                                                                , "btn_ok" _
                                                                , "btn_cancel" _
                                                                , "TabControl2" _
                                                                , "txt_description" _
                                                                , "lbl_description" _
                                                                , "chk_redeem_on_promobox" _
                                                                , "chk_vip"})
    DisallowAllControls(Me, _exception_list)

    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
    '' Disallow Check event handlers to avoid
    '' changes in gift name ( is not editable )
    RemoveHandler opt_type_object.CheckedChanged, AddressOf opt_type_object_CheckedChanged
    RemoveHandler opt_type_nrc.CheckedChanged, AddressOf opt_type_nrc_CheckedChanged
    RemoveHandler opt_type_redeem_credit.CheckedChanged, AddressOf opt_type_redeem_credit_CheckedChanged

    Me.gb_available.Visible = False

  End Sub

  ' PURPOSE : Disallow all controls not present in exception List
  '           ( Recursive Function )
  '  PARAMS :
  '     - INPUT :
  '         Main control
  '         Exception List
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub DisallowAllControls(ByRef Control As Control, ByVal ExceptionList As List(Of String))

    For Each _control As Control In Control.Controls

      If Not ExceptionList.Contains(_control.Name) Then

        If Not _control.Controls Is Nothing AndAlso _control.Controls.Count > 0 Then
          Call DisallowAllControls(_control, ExceptionList)
        Else
          _control.Enabled = False
        End If
      End If


    Next
  End Sub

#End Region ' Private

#Region " Events "

  Private Sub opt_type_object_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_type_object.CheckedChanged

    If opt_type_object.Checked Then
      ef_gift_name.IsReadOnly = False
      ef_gift_name.TabStop = True

      ef_current_stock.IsReadOnly = False
      ef_current_stock.TabStop = True
      ef_nrc.IsReadOnly = True
      ef_nrc.TabStop = False
      ef_nrc.Value = ""
      ef_redeem_credit.IsReadOnly = True
      ef_redeem_credit.TabStop = False
      ef_redeem_credit.Value = ""
    End If

  End Sub

  Private Sub opt_type_nrc_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_type_nrc.CheckedChanged

    If opt_type_nrc.Checked Then
      ' 345 "$XXX Cr�ditos no redimibles"
      ef_gift_name.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(345, GUI_FormatCurrency(GUI_ParseNumber(ef_nrc.Value)))
      ef_gift_name.IsReadOnly = True
      ef_gift_name.TabStop = False

      ef_current_stock.IsReadOnly = True
      ef_current_stock.TabStop = False
      ef_current_stock.Value = ""
      ef_nrc.IsReadOnly = False
      ef_nrc.TabStop = True
      ef_redeem_credit.IsReadOnly = True
      ef_redeem_credit.TabStop = False
      ef_redeem_credit.Value = ""
    End If

  End Sub

  Private Sub opt_type_redeem_credit_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_type_redeem_credit.CheckedChanged

    If opt_type_redeem_credit.Checked Then
      ' 310 "$XXX Cr�ditos redimibles"
      ef_gift_name.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(310, GUI_FormatCurrency(GUI_ParseNumber(ef_redeem_credit.Value)))
      ef_gift_name.IsReadOnly = True
      ef_gift_name.TabStop = False

      ef_current_stock.IsReadOnly = True
      ef_current_stock.TabStop = False
      ef_current_stock.Value = ""
      ef_nrc.IsReadOnly = True
      ef_nrc.TabStop = False
      ef_nrc.Value = ""
      ef_redeem_credit.IsReadOnly = False
      ef_redeem_credit.TabStop = True
    End If

  End Sub

  Private Sub opt_type_services_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_type_services.CheckedChanged

    If opt_type_services.Checked Then
      ef_gift_name.IsReadOnly = False
      ef_gift_name.TabStop = True

      ef_current_stock.IsReadOnly = True
      ef_current_stock.TabStop = False
      ef_current_stock.Value = ""
      ef_nrc.IsReadOnly = True
      ef_nrc.TabStop = False
      ef_nrc.Value = ""
      ef_redeem_credit.IsReadOnly = True
      ef_redeem_credit.TabStop = False
      ef_redeem_credit.Value = ""
    End If

  End Sub

  Private Sub ef_nrc_EntryFieldValueChanged() Handles ef_nrc.EntryFieldValueChanged

    If opt_type_nrc.Checked Then
      ef_gift_name.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(345, GUI_FormatCurrency(GUI_ParseNumber(ef_nrc.Value))) ' "$XXX Cr�ditos no redimibles"
    End If

  End Sub

  Private Sub ef_redeem_credit_EntryFieldValueChanged() Handles ef_redeem_credit.EntryFieldValueChanged

    If opt_type_redeem_credit.Checked Then
      ef_gift_name.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(310, GUI_FormatCurrency(GUI_ParseNumber(ef_redeem_credit.Value))) ' "$XXX Cr�ditos redimibles"
    End If

  End Sub

  Private Sub ef_daily_limit_EntryFieldValueChanged() Handles ef_daily_limit.EntryFieldValueChanged
    If (GUI_FormatNumber(GUI_ParseNumber(ef_daily_limit.Value)) > 0) Then
      lbl_daily_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1939, GUI_FormatNumber(GUI_ParseNumber(ef_daily_limit.Value), 0))
    Else
      lbl_daily_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1932)
    End If
  End Sub

  Private Sub ef_monthly_limit_EntryFieldValueChanged() Handles ef_monthly_limit.EntryFieldValueChanged
    If (GUI_FormatNumber(GUI_ParseNumber(ef_monthly_limit.Value)) > 0) Then
      lbl_monthly_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1940, GUI_FormatNumber(GUI_ParseNumber(ef_monthly_limit.Value), 0))
    Else
      lbl_monthly_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1933)
    End If
  End Sub

  Private Sub ef_promotion_limit_day_EntryFieldValueChanged() Handles ef_gift_limit_day.EntryFieldValueChanged
    If (GUI_FormatCurrency(GUI_ParseCurrency(ef_gift_limit_day.Value)) > 0) Then
      lbl_promotion_daily_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1941, GUI_FormatNumber(GUI_ParseCurrency(ef_gift_limit_day.Value), 0))
    Else
      lbl_promotion_daily_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1934)
    End If
  End Sub

  Private Sub ef_promotion_limit_month_EntryFieldValueChanged() Handles ef_gift_limit_month.EntryFieldValueChanged
    If (GUI_FormatCurrency(GUI_ParseCurrency(ef_gift_limit_month.Value)) > 0) Then
      lbl_promotion_monthly_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1942, GUI_FormatNumber(GUI_ParseCurrency(ef_gift_limit_month.Value), 0))
    Else
      lbl_promotion_monthly_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1935)
    End If
  End Sub

  Private Sub chk_points_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_points_level4.CheckedChanged, chk_points_level3.CheckedChanged, chk_points_level2.CheckedChanged, chk_points_level1.CheckedChanged
    Select Case sender.Name
      Case "chk_points_level1"
        If chk_points_level1.Checked Then
          ef_points_level1.Enabled = True
        Else
          ef_points_level1.Enabled = False
          ef_points_level1.Value = ""
        End If
      Case "chk_points_level2"
        If chk_points_level2.Checked Then
          ef_points_level2.Enabled = True
        Else
          ef_points_level2.Enabled = False
          ef_points_level2.Value = ""
        End If
      Case "chk_points_level3"
        If chk_points_level3.Checked Then
          ef_points_level3.Enabled = True
        Else
          ef_points_level3.Enabled = False
          ef_points_level3.Value = ""
        End If
      Case "chk_points_level4"
        If chk_points_level4.Checked Then
          ef_points_level4.Enabled = True
        Else
          ef_points_level4.Enabled = False
          ef_points_level4.Value = ""
        End If
    End Select

  End Sub

#End Region ' Events

End Class
