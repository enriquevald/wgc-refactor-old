<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_operations_schedule
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.tab_control = New System.Windows.Forms.TabControl()
    Me.tab_schedule = New System.Windows.Forms.TabPage()
    Me.lbl_disable_machines = New System.Windows.Forms.Label()
    Me.chk_disable_machines = New System.Windows.Forms.CheckBox()
    Me.chk_schedule_enabled = New System.Windows.Forms.CheckBox()
    Me.cmb_closing_time = New GUI_Controls.uc_combo()
    Me.gb_schedule = New System.Windows.Forms.GroupBox()
    Me.btn_all = New GUI_Controls.uc_button()
    Me.dtp_sunday_time2_to = New GUI_Controls.uc_date_picker()
    Me.dtp_saturday_time2_to = New GUI_Controls.uc_date_picker()
    Me.dtp_friday_time2_to = New GUI_Controls.uc_date_picker()
    Me.dtp_thursday_time2_to = New GUI_Controls.uc_date_picker()
    Me.dtp_wednesday_time2_to = New GUI_Controls.uc_date_picker()
    Me.dtp_tuesday_time2_to = New GUI_Controls.uc_date_picker()
    Me.dtp_monday_time2_to = New GUI_Controls.uc_date_picker()
    Me.dtp_sunday_time2_from = New GUI_Controls.uc_date_picker()
    Me.dtp_saturday_time2_from = New GUI_Controls.uc_date_picker()
    Me.dtp_friday_time2_from = New GUI_Controls.uc_date_picker()
    Me.dtp_thursday_time2_from = New GUI_Controls.uc_date_picker()
    Me.dtp_wednesday_time2_from = New GUI_Controls.uc_date_picker()
    Me.dtp_tuesday_time2_from = New GUI_Controls.uc_date_picker()
    Me.dtp_monday_time2_from = New GUI_Controls.uc_date_picker()
    Me.chk_sunday_time2 = New System.Windows.Forms.CheckBox()
    Me.chk_saturday_time2 = New System.Windows.Forms.CheckBox()
    Me.chk_friday_time2 = New System.Windows.Forms.CheckBox()
    Me.chk_thursday_time2 = New System.Windows.Forms.CheckBox()
    Me.chk_wednesday_time2 = New System.Windows.Forms.CheckBox()
    Me.chk_tuesday_time2 = New System.Windows.Forms.CheckBox()
    Me.chk_monday_time2 = New System.Windows.Forms.CheckBox()
    Me.dtp_sunday_time1_to = New GUI_Controls.uc_date_picker()
    Me.dtp_saturday_time1_to = New GUI_Controls.uc_date_picker()
    Me.dtp_friday_time1_to = New GUI_Controls.uc_date_picker()
    Me.dtp_thursday_time1_to = New GUI_Controls.uc_date_picker()
    Me.dtp_wednesday_time1_to = New GUI_Controls.uc_date_picker()
    Me.dtp_tuesday_time1_to = New GUI_Controls.uc_date_picker()
    Me.dtp_monday_time1_to = New GUI_Controls.uc_date_picker()
    Me.dtp_sunday_time1_from = New GUI_Controls.uc_date_picker()
    Me.dtp_saturday_time1_from = New GUI_Controls.uc_date_picker()
    Me.dtp_friday_time1_from = New GUI_Controls.uc_date_picker()
    Me.dtp_thursday_time1_from = New GUI_Controls.uc_date_picker()
    Me.dtp_wednesday_time1_from = New GUI_Controls.uc_date_picker()
    Me.dtp_tuesday_time1_from = New GUI_Controls.uc_date_picker()
    Me.dtp_monday_time1_from = New GUI_Controls.uc_date_picker()
    Me.chk_sunday = New System.Windows.Forms.CheckBox()
    Me.chk_saturday = New System.Windows.Forms.CheckBox()
    Me.chk_friday = New System.Windows.Forms.CheckBox()
    Me.chk_thursday = New System.Windows.Forms.CheckBox()
    Me.chk_wednesday = New System.Windows.Forms.CheckBox()
    Me.chk_tuesday = New System.Windows.Forms.CheckBox()
    Me.chk_monday = New System.Windows.Forms.CheckBox()
    Me.tab_exceptions = New System.Windows.Forms.TabPage()
    Me.gb_new_exception = New System.Windows.Forms.GroupBox()
    Me.dtp_new_exception_date_to = New GUI_Controls.uc_date_picker()
    Me.chk_new_exception_date_range = New System.Windows.Forms.CheckBox()
    Me.chk_new_exception_allow = New System.Windows.Forms.CheckBox()
    Me.dtp_new_exception_date_from = New GUI_Controls.uc_date_picker()
    Me.btn_add_exception = New GUI_Controls.uc_button()
    Me.dtp_new_exception_time2_to = New GUI_Controls.uc_date_picker()
    Me.dtp_new_exception_time2_from = New GUI_Controls.uc_date_picker()
    Me.chk_new_exception_time2 = New System.Windows.Forms.CheckBox()
    Me.dtp_new_exception_time1_to = New GUI_Controls.uc_date_picker()
    Me.dtp_new_exception_time1_from = New GUI_Controls.uc_date_picker()
    Me.btn_delete_exceptions = New GUI_Controls.uc_button()
    Me.dg_exceptions = New GUI_Controls.uc_grid()
    Me.tab_calendar = New System.Windows.Forms.TabPage()
    Me.lbl_calendar_instructions = New System.Windows.Forms.Label()
    Me.lbl_calendar_error = New System.Windows.Forms.Label()
    Me.lbl_calendar_schedule = New System.Windows.Forms.Label()
    Me.calendar = New System.Windows.Forms.MonthCalendar()
    Me.tab_notifications = New System.Windows.Forms.TabPage()
    Me.gb_notifications_recipients = New System.Windows.Forms.GroupBox()
    Me.dg_recipients = New GUI_Controls.uc_grid()
    Me.btn_delete_recipients = New GUI_Controls.uc_button()
    Me.gb_new_recipient = New System.Windows.Forms.GroupBox()
    Me.btn_add_recipient = New GUI_Controls.uc_button()
    Me.ef_new_recipient = New GUI_Controls.uc_entry_field()
    Me.ef_subject = New GUI_Controls.uc_entry_field()
    Me.chk_notifications_enabled = New System.Windows.Forms.CheckBox()
    Me.tab_shifts = New System.Windows.Forms.TabPage()
    Me.gb_daily_cash_sessions = New System.Windows.Forms.GroupBox()
    Me.lbl_info_permission = New System.Windows.Forms.Label()
    Me.ef_max_daily_cash_openings = New GUI_Controls.uc_entry_field()
    Me.gb_work_shifts = New System.Windows.Forms.GroupBox()
    Me.ef_work_shift_duration = New GUI_Controls.uc_entry_field()
    Me.chk_enable_shift_duration = New System.Windows.Forms.CheckBox()
    Me.panel_data.SuspendLayout()
    Me.tab_control.SuspendLayout()
    Me.tab_schedule.SuspendLayout()
    Me.gb_schedule.SuspendLayout()
    Me.tab_exceptions.SuspendLayout()
    Me.gb_new_exception.SuspendLayout()
    Me.tab_calendar.SuspendLayout()
    Me.tab_notifications.SuspendLayout()
    Me.gb_notifications_recipients.SuspendLayout()
    Me.gb_new_recipient.SuspendLayout()
    Me.tab_shifts.SuspendLayout()
    Me.gb_daily_cash_sessions.SuspendLayout()
    Me.gb_work_shifts.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.tab_control)
    Me.panel_data.Size = New System.Drawing.Size(668, 433)
    '
    'tab_control
    '
    Me.tab_control.Controls.Add(Me.tab_schedule)
    Me.tab_control.Controls.Add(Me.tab_exceptions)
    Me.tab_control.Controls.Add(Me.tab_calendar)
    Me.tab_control.Controls.Add(Me.tab_notifications)
    Me.tab_control.Controls.Add(Me.tab_shifts)
    Me.tab_control.Location = New System.Drawing.Point(3, 3)
    Me.tab_control.Name = "tab_control"
    Me.tab_control.SelectedIndex = 0
    Me.tab_control.Size = New System.Drawing.Size(662, 428)
    Me.tab_control.TabIndex = 0
    '
    'tab_schedule
    '
    Me.tab_schedule.Controls.Add(Me.lbl_disable_machines)
    Me.tab_schedule.Controls.Add(Me.chk_disable_machines)
    Me.tab_schedule.Controls.Add(Me.chk_schedule_enabled)
    Me.tab_schedule.Controls.Add(Me.cmb_closing_time)
    Me.tab_schedule.Controls.Add(Me.gb_schedule)
    Me.tab_schedule.Location = New System.Drawing.Point(4, 22)
    Me.tab_schedule.Name = "tab_schedule"
    Me.tab_schedule.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_schedule.Size = New System.Drawing.Size(654, 402)
    Me.tab_schedule.TabIndex = 0
    Me.tab_schedule.Text = "xSchedule"
    Me.tab_schedule.UseVisualStyleBackColor = True
    '
    'lbl_disable_machines
    '
    Me.lbl_disable_machines.ForeColor = System.Drawing.Color.Blue
    Me.lbl_disable_machines.Location = New System.Drawing.Point(46, 67)
    Me.lbl_disable_machines.Name = "lbl_disable_machines"
    Me.lbl_disable_machines.Size = New System.Drawing.Size(582, 30)
    Me.lbl_disable_machines.TabIndex = 23
    Me.lbl_disable_machines.Text = """Las m�quinas se bloquear�n 15 minutos antes del final del horario de operaciones" & _
    " y se desbloquear�n 15 minutos despu�s del inicio."""
    '
    'chk_disable_machines
    '
    Me.chk_disable_machines.AutoSize = True
    Me.chk_disable_machines.Location = New System.Drawing.Point(27, 37)
    Me.chk_disable_machines.Name = "chk_disable_machines"
    Me.chk_disable_machines.Size = New System.Drawing.Size(133, 17)
    Me.chk_disable_machines.TabIndex = 1
    Me.chk_disable_machines.Text = "xDisable machines"
    Me.chk_disable_machines.UseVisualStyleBackColor = True
    '
    'chk_schedule_enabled
    '
    Me.chk_schedule_enabled.AutoSize = True
    Me.chk_schedule_enabled.Location = New System.Drawing.Point(27, 14)
    Me.chk_schedule_enabled.Name = "chk_schedule_enabled"
    Me.chk_schedule_enabled.Size = New System.Drawing.Size(193, 17)
    Me.chk_schedule_enabled.TabIndex = 0
    Me.chk_schedule_enabled.Text = "xEnable Operations Schedule"
    Me.chk_schedule_enabled.UseVisualStyleBackColor = True
    '
    'cmb_closing_time
    '
    Me.cmb_closing_time.AllowUnlistedValues = False
    Me.cmb_closing_time.AutoCompleteMode = False
    Me.cmb_closing_time.IsReadOnly = False
    Me.cmb_closing_time.Location = New System.Drawing.Point(440, 10)
    Me.cmb_closing_time.Name = "cmb_closing_time"
    Me.cmb_closing_time.SelectedIndex = -1
    Me.cmb_closing_time.Size = New System.Drawing.Size(188, 24)
    Me.cmb_closing_time.SufixText = "Sufix Text"
    Me.cmb_closing_time.SufixTextVisible = True
    Me.cmb_closing_time.TabIndex = 2
    Me.cmb_closing_time.TextVisible = False
    Me.cmb_closing_time.TextWidth = 115
    '
    'gb_schedule
    '
    Me.gb_schedule.Controls.Add(Me.btn_all)
    Me.gb_schedule.Controls.Add(Me.dtp_sunday_time2_to)
    Me.gb_schedule.Controls.Add(Me.dtp_saturday_time2_to)
    Me.gb_schedule.Controls.Add(Me.dtp_friday_time2_to)
    Me.gb_schedule.Controls.Add(Me.dtp_thursday_time2_to)
    Me.gb_schedule.Controls.Add(Me.dtp_wednesday_time2_to)
    Me.gb_schedule.Controls.Add(Me.dtp_tuesday_time2_to)
    Me.gb_schedule.Controls.Add(Me.dtp_monday_time2_to)
    Me.gb_schedule.Controls.Add(Me.dtp_sunday_time2_from)
    Me.gb_schedule.Controls.Add(Me.dtp_saturday_time2_from)
    Me.gb_schedule.Controls.Add(Me.dtp_friday_time2_from)
    Me.gb_schedule.Controls.Add(Me.dtp_thursday_time2_from)
    Me.gb_schedule.Controls.Add(Me.dtp_wednesday_time2_from)
    Me.gb_schedule.Controls.Add(Me.dtp_tuesday_time2_from)
    Me.gb_schedule.Controls.Add(Me.dtp_monday_time2_from)
    Me.gb_schedule.Controls.Add(Me.chk_sunday_time2)
    Me.gb_schedule.Controls.Add(Me.chk_saturday_time2)
    Me.gb_schedule.Controls.Add(Me.chk_friday_time2)
    Me.gb_schedule.Controls.Add(Me.chk_thursday_time2)
    Me.gb_schedule.Controls.Add(Me.chk_wednesday_time2)
    Me.gb_schedule.Controls.Add(Me.chk_tuesday_time2)
    Me.gb_schedule.Controls.Add(Me.chk_monday_time2)
    Me.gb_schedule.Controls.Add(Me.dtp_sunday_time1_to)
    Me.gb_schedule.Controls.Add(Me.dtp_saturday_time1_to)
    Me.gb_schedule.Controls.Add(Me.dtp_friday_time1_to)
    Me.gb_schedule.Controls.Add(Me.dtp_thursday_time1_to)
    Me.gb_schedule.Controls.Add(Me.dtp_wednesday_time1_to)
    Me.gb_schedule.Controls.Add(Me.dtp_tuesday_time1_to)
    Me.gb_schedule.Controls.Add(Me.dtp_monday_time1_to)
    Me.gb_schedule.Controls.Add(Me.dtp_sunday_time1_from)
    Me.gb_schedule.Controls.Add(Me.dtp_saturday_time1_from)
    Me.gb_schedule.Controls.Add(Me.dtp_friday_time1_from)
    Me.gb_schedule.Controls.Add(Me.dtp_thursday_time1_from)
    Me.gb_schedule.Controls.Add(Me.dtp_wednesday_time1_from)
    Me.gb_schedule.Controls.Add(Me.dtp_tuesday_time1_from)
    Me.gb_schedule.Controls.Add(Me.dtp_monday_time1_from)
    Me.gb_schedule.Controls.Add(Me.chk_sunday)
    Me.gb_schedule.Controls.Add(Me.chk_saturday)
    Me.gb_schedule.Controls.Add(Me.chk_friday)
    Me.gb_schedule.Controls.Add(Me.chk_thursday)
    Me.gb_schedule.Controls.Add(Me.chk_wednesday)
    Me.gb_schedule.Controls.Add(Me.chk_tuesday)
    Me.gb_schedule.Controls.Add(Me.chk_monday)
    Me.gb_schedule.Location = New System.Drawing.Point(11, 111)
    Me.gb_schedule.Name = "gb_schedule"
    Me.gb_schedule.Size = New System.Drawing.Size(617, 285)
    Me.gb_schedule.TabIndex = 1
    Me.gb_schedule.TabStop = False
    Me.gb_schedule.Text = "xSchedule"
    '
    'btn_all
    '
    Me.btn_all.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_all.Location = New System.Drawing.Point(518, 23)
    Me.btn_all.Name = "btn_all"
    Me.btn_all.Size = New System.Drawing.Size(63, 21)
    Me.btn_all.TabIndex = 6
    Me.btn_all.ToolTipped = False
    Me.btn_all.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.SMALL
    '
    'dtp_sunday_time2_to
    '
    Me.dtp_sunday_time2_to.Checked = True
    Me.dtp_sunday_time2_to.IsReadOnly = False
    Me.dtp_sunday_time2_to.Location = New System.Drawing.Point(415, 252)
    Me.dtp_sunday_time2_to.Name = "dtp_sunday_time2_to"
    Me.dtp_sunday_time2_to.ShowCheckBox = False
    Me.dtp_sunday_time2_to.ShowUpDown = True
    Me.dtp_sunday_time2_to.Size = New System.Drawing.Size(91, 24)
    Me.dtp_sunday_time2_to.SufixText = "Sufix Text"
    Me.dtp_sunday_time2_to.SufixTextVisible = True
    Me.dtp_sunday_time2_to.TabIndex = 42
    Me.dtp_sunday_time2_to.TextVisible = False
    Me.dtp_sunday_time2_to.TextWidth = 20
    Me.dtp_sunday_time2_to.Value = New Date(2013, 2, 22, 0, 0, 0, 0)
    '
    'dtp_saturday_time2_to
    '
    Me.dtp_saturday_time2_to.Checked = True
    Me.dtp_saturday_time2_to.IsReadOnly = False
    Me.dtp_saturday_time2_to.Location = New System.Drawing.Point(415, 214)
    Me.dtp_saturday_time2_to.Name = "dtp_saturday_time2_to"
    Me.dtp_saturday_time2_to.ShowCheckBox = False
    Me.dtp_saturday_time2_to.ShowUpDown = True
    Me.dtp_saturday_time2_to.Size = New System.Drawing.Size(91, 24)
    Me.dtp_saturday_time2_to.SufixText = "Sufix Text"
    Me.dtp_saturday_time2_to.SufixTextVisible = True
    Me.dtp_saturday_time2_to.TabIndex = 36
    Me.dtp_saturday_time2_to.TextVisible = False
    Me.dtp_saturday_time2_to.TextWidth = 20
    Me.dtp_saturday_time2_to.Value = New Date(2013, 2, 22, 0, 0, 0, 0)
    '
    'dtp_friday_time2_to
    '
    Me.dtp_friday_time2_to.Checked = True
    Me.dtp_friday_time2_to.IsReadOnly = False
    Me.dtp_friday_time2_to.Location = New System.Drawing.Point(415, 176)
    Me.dtp_friday_time2_to.Name = "dtp_friday_time2_to"
    Me.dtp_friday_time2_to.ShowCheckBox = False
    Me.dtp_friday_time2_to.ShowUpDown = True
    Me.dtp_friday_time2_to.Size = New System.Drawing.Size(91, 24)
    Me.dtp_friday_time2_to.SufixText = "Sufix Text"
    Me.dtp_friday_time2_to.SufixTextVisible = True
    Me.dtp_friday_time2_to.TabIndex = 30
    Me.dtp_friday_time2_to.TextVisible = False
    Me.dtp_friday_time2_to.TextWidth = 20
    Me.dtp_friday_time2_to.Value = New Date(2013, 2, 22, 0, 0, 0, 0)
    '
    'dtp_thursday_time2_to
    '
    Me.dtp_thursday_time2_to.Checked = True
    Me.dtp_thursday_time2_to.IsReadOnly = False
    Me.dtp_thursday_time2_to.Location = New System.Drawing.Point(415, 137)
    Me.dtp_thursday_time2_to.Name = "dtp_thursday_time2_to"
    Me.dtp_thursday_time2_to.ShowCheckBox = False
    Me.dtp_thursday_time2_to.ShowUpDown = True
    Me.dtp_thursday_time2_to.Size = New System.Drawing.Size(91, 24)
    Me.dtp_thursday_time2_to.SufixText = "Sufix Text"
    Me.dtp_thursday_time2_to.SufixTextVisible = True
    Me.dtp_thursday_time2_to.TabIndex = 24
    Me.dtp_thursday_time2_to.TextVisible = False
    Me.dtp_thursday_time2_to.TextWidth = 20
    Me.dtp_thursday_time2_to.Value = New Date(2013, 2, 22, 0, 0, 0, 0)
    '
    'dtp_wednesday_time2_to
    '
    Me.dtp_wednesday_time2_to.Checked = True
    Me.dtp_wednesday_time2_to.IsReadOnly = False
    Me.dtp_wednesday_time2_to.Location = New System.Drawing.Point(415, 99)
    Me.dtp_wednesday_time2_to.Name = "dtp_wednesday_time2_to"
    Me.dtp_wednesday_time2_to.ShowCheckBox = False
    Me.dtp_wednesday_time2_to.ShowUpDown = True
    Me.dtp_wednesday_time2_to.Size = New System.Drawing.Size(91, 24)
    Me.dtp_wednesday_time2_to.SufixText = "Sufix Text"
    Me.dtp_wednesday_time2_to.SufixTextVisible = True
    Me.dtp_wednesday_time2_to.TabIndex = 18
    Me.dtp_wednesday_time2_to.TextVisible = False
    Me.dtp_wednesday_time2_to.TextWidth = 20
    Me.dtp_wednesday_time2_to.Value = New Date(2013, 2, 22, 0, 0, 0, 0)
    '
    'dtp_tuesday_time2_to
    '
    Me.dtp_tuesday_time2_to.Checked = True
    Me.dtp_tuesday_time2_to.IsReadOnly = False
    Me.dtp_tuesday_time2_to.Location = New System.Drawing.Point(415, 61)
    Me.dtp_tuesday_time2_to.Name = "dtp_tuesday_time2_to"
    Me.dtp_tuesday_time2_to.ShowCheckBox = False
    Me.dtp_tuesday_time2_to.ShowUpDown = True
    Me.dtp_tuesday_time2_to.Size = New System.Drawing.Size(91, 24)
    Me.dtp_tuesday_time2_to.SufixText = "Sufix Text"
    Me.dtp_tuesday_time2_to.SufixTextVisible = True
    Me.dtp_tuesday_time2_to.TabIndex = 12
    Me.dtp_tuesday_time2_to.TextVisible = False
    Me.dtp_tuesday_time2_to.TextWidth = 20
    Me.dtp_tuesday_time2_to.Value = New Date(2013, 2, 22, 0, 0, 0, 0)
    '
    'dtp_monday_time2_to
    '
    Me.dtp_monday_time2_to.Checked = True
    Me.dtp_monday_time2_to.IsReadOnly = False
    Me.dtp_monday_time2_to.Location = New System.Drawing.Point(415, 22)
    Me.dtp_monday_time2_to.Name = "dtp_monday_time2_to"
    Me.dtp_monday_time2_to.ShowCheckBox = False
    Me.dtp_monday_time2_to.ShowUpDown = True
    Me.dtp_monday_time2_to.Size = New System.Drawing.Size(91, 24)
    Me.dtp_monday_time2_to.SufixText = "Sufix Text"
    Me.dtp_monday_time2_to.SufixTextVisible = True
    Me.dtp_monday_time2_to.TabIndex = 5
    Me.dtp_monday_time2_to.TextVisible = False
    Me.dtp_monday_time2_to.TextWidth = 20
    Me.dtp_monday_time2_to.Value = New Date(2013, 2, 22, 0, 0, 0, 0)
    '
    'dtp_sunday_time2_from
    '
    Me.dtp_sunday_time2_from.Checked = True
    Me.dtp_sunday_time2_from.IsReadOnly = False
    Me.dtp_sunday_time2_from.Location = New System.Drawing.Point(345, 252)
    Me.dtp_sunday_time2_from.Name = "dtp_sunday_time2_from"
    Me.dtp_sunday_time2_from.ShowCheckBox = False
    Me.dtp_sunday_time2_from.ShowUpDown = True
    Me.dtp_sunday_time2_from.Size = New System.Drawing.Size(71, 24)
    Me.dtp_sunday_time2_from.SufixText = "Sufix Text"
    Me.dtp_sunday_time2_from.SufixTextVisible = True
    Me.dtp_sunday_time2_from.TabIndex = 41
    Me.dtp_sunday_time2_from.TextVisible = False
    Me.dtp_sunday_time2_from.TextWidth = 0
    Me.dtp_sunday_time2_from.Value = New Date(2013, 2, 22, 0, 0, 0, 0)
    '
    'dtp_saturday_time2_from
    '
    Me.dtp_saturday_time2_from.Checked = True
    Me.dtp_saturday_time2_from.IsReadOnly = False
    Me.dtp_saturday_time2_from.Location = New System.Drawing.Point(345, 214)
    Me.dtp_saturday_time2_from.Name = "dtp_saturday_time2_from"
    Me.dtp_saturday_time2_from.ShowCheckBox = False
    Me.dtp_saturday_time2_from.ShowUpDown = True
    Me.dtp_saturday_time2_from.Size = New System.Drawing.Size(71, 24)
    Me.dtp_saturday_time2_from.SufixText = "Sufix Text"
    Me.dtp_saturday_time2_from.SufixTextVisible = True
    Me.dtp_saturday_time2_from.TabIndex = 35
    Me.dtp_saturday_time2_from.TextVisible = False
    Me.dtp_saturday_time2_from.TextWidth = 0
    Me.dtp_saturday_time2_from.Value = New Date(2013, 2, 22, 0, 0, 0, 0)
    '
    'dtp_friday_time2_from
    '
    Me.dtp_friday_time2_from.Checked = True
    Me.dtp_friday_time2_from.IsReadOnly = False
    Me.dtp_friday_time2_from.Location = New System.Drawing.Point(345, 176)
    Me.dtp_friday_time2_from.Name = "dtp_friday_time2_from"
    Me.dtp_friday_time2_from.ShowCheckBox = False
    Me.dtp_friday_time2_from.ShowUpDown = True
    Me.dtp_friday_time2_from.Size = New System.Drawing.Size(71, 24)
    Me.dtp_friday_time2_from.SufixText = "Sufix Text"
    Me.dtp_friday_time2_from.SufixTextVisible = True
    Me.dtp_friday_time2_from.TabIndex = 29
    Me.dtp_friday_time2_from.TextVisible = False
    Me.dtp_friday_time2_from.TextWidth = 0
    Me.dtp_friday_time2_from.Value = New Date(2013, 2, 22, 0, 0, 0, 0)
    '
    'dtp_thursday_time2_from
    '
    Me.dtp_thursday_time2_from.Checked = True
    Me.dtp_thursday_time2_from.IsReadOnly = False
    Me.dtp_thursday_time2_from.Location = New System.Drawing.Point(345, 137)
    Me.dtp_thursday_time2_from.Name = "dtp_thursday_time2_from"
    Me.dtp_thursday_time2_from.ShowCheckBox = False
    Me.dtp_thursday_time2_from.ShowUpDown = True
    Me.dtp_thursday_time2_from.Size = New System.Drawing.Size(71, 24)
    Me.dtp_thursday_time2_from.SufixText = "Sufix Text"
    Me.dtp_thursday_time2_from.SufixTextVisible = True
    Me.dtp_thursday_time2_from.TabIndex = 23
    Me.dtp_thursday_time2_from.TextVisible = False
    Me.dtp_thursday_time2_from.TextWidth = 0
    Me.dtp_thursday_time2_from.Value = New Date(2013, 2, 22, 0, 0, 0, 0)
    '
    'dtp_wednesday_time2_from
    '
    Me.dtp_wednesday_time2_from.Checked = True
    Me.dtp_wednesday_time2_from.IsReadOnly = False
    Me.dtp_wednesday_time2_from.Location = New System.Drawing.Point(345, 99)
    Me.dtp_wednesday_time2_from.Name = "dtp_wednesday_time2_from"
    Me.dtp_wednesday_time2_from.ShowCheckBox = False
    Me.dtp_wednesday_time2_from.ShowUpDown = True
    Me.dtp_wednesday_time2_from.Size = New System.Drawing.Size(71, 24)
    Me.dtp_wednesday_time2_from.SufixText = "Sufix Text"
    Me.dtp_wednesday_time2_from.SufixTextVisible = True
    Me.dtp_wednesday_time2_from.TabIndex = 17
    Me.dtp_wednesday_time2_from.TextVisible = False
    Me.dtp_wednesday_time2_from.TextWidth = 0
    Me.dtp_wednesday_time2_from.Value = New Date(2013, 2, 22, 0, 0, 0, 0)
    '
    'dtp_tuesday_time2_from
    '
    Me.dtp_tuesday_time2_from.Checked = True
    Me.dtp_tuesday_time2_from.IsReadOnly = False
    Me.dtp_tuesday_time2_from.Location = New System.Drawing.Point(345, 61)
    Me.dtp_tuesday_time2_from.Name = "dtp_tuesday_time2_from"
    Me.dtp_tuesday_time2_from.ShowCheckBox = False
    Me.dtp_tuesday_time2_from.ShowUpDown = True
    Me.dtp_tuesday_time2_from.Size = New System.Drawing.Size(71, 24)
    Me.dtp_tuesday_time2_from.SufixText = "Sufix Text"
    Me.dtp_tuesday_time2_from.SufixTextVisible = True
    Me.dtp_tuesday_time2_from.TabIndex = 11
    Me.dtp_tuesday_time2_from.TextVisible = False
    Me.dtp_tuesday_time2_from.TextWidth = 0
    Me.dtp_tuesday_time2_from.Value = New Date(2013, 2, 22, 0, 0, 0, 0)
    '
    'dtp_monday_time2_from
    '
    Me.dtp_monday_time2_from.Checked = True
    Me.dtp_monday_time2_from.IsReadOnly = False
    Me.dtp_monday_time2_from.Location = New System.Drawing.Point(345, 22)
    Me.dtp_monday_time2_from.Name = "dtp_monday_time2_from"
    Me.dtp_monday_time2_from.ShowCheckBox = False
    Me.dtp_monday_time2_from.ShowUpDown = True
    Me.dtp_monday_time2_from.Size = New System.Drawing.Size(71, 24)
    Me.dtp_monday_time2_from.SufixText = "Sufix Text"
    Me.dtp_monday_time2_from.SufixTextVisible = True
    Me.dtp_monday_time2_from.TabIndex = 4
    Me.dtp_monday_time2_from.TextVisible = False
    Me.dtp_monday_time2_from.TextWidth = 0
    Me.dtp_monday_time2_from.Value = New Date(2013, 2, 22, 0, 0, 0, 0)
    '
    'chk_sunday_time2
    '
    Me.chk_sunday_time2.AutoSize = True
    Me.chk_sunday_time2.Location = New System.Drawing.Point(299, 256)
    Me.chk_sunday_time2.Name = "chk_sunday_time2"
    Me.chk_sunday_time2.Size = New System.Drawing.Size(55, 17)
    Me.chk_sunday_time2.TabIndex = 40
    Me.chk_sunday_time2.Text = "xAnd"
    Me.chk_sunday_time2.UseVisualStyleBackColor = True
    '
    'chk_saturday_time2
    '
    Me.chk_saturday_time2.AutoSize = True
    Me.chk_saturday_time2.Location = New System.Drawing.Point(299, 218)
    Me.chk_saturday_time2.Name = "chk_saturday_time2"
    Me.chk_saturday_time2.Size = New System.Drawing.Size(55, 17)
    Me.chk_saturday_time2.TabIndex = 34
    Me.chk_saturday_time2.Text = "xAnd"
    Me.chk_saturday_time2.UseVisualStyleBackColor = True
    '
    'chk_friday_time2
    '
    Me.chk_friday_time2.AutoSize = True
    Me.chk_friday_time2.Location = New System.Drawing.Point(299, 179)
    Me.chk_friday_time2.Name = "chk_friday_time2"
    Me.chk_friday_time2.Size = New System.Drawing.Size(55, 17)
    Me.chk_friday_time2.TabIndex = 28
    Me.chk_friday_time2.Text = "xAnd"
    Me.chk_friday_time2.UseVisualStyleBackColor = True
    '
    'chk_thursday_time2
    '
    Me.chk_thursday_time2.AutoSize = True
    Me.chk_thursday_time2.Location = New System.Drawing.Point(299, 141)
    Me.chk_thursday_time2.Name = "chk_thursday_time2"
    Me.chk_thursday_time2.Size = New System.Drawing.Size(55, 17)
    Me.chk_thursday_time2.TabIndex = 22
    Me.chk_thursday_time2.Text = "xAnd"
    Me.chk_thursday_time2.UseVisualStyleBackColor = True
    '
    'chk_wednesday_time2
    '
    Me.chk_wednesday_time2.AutoSize = True
    Me.chk_wednesday_time2.Location = New System.Drawing.Point(299, 103)
    Me.chk_wednesday_time2.Name = "chk_wednesday_time2"
    Me.chk_wednesday_time2.Size = New System.Drawing.Size(55, 17)
    Me.chk_wednesday_time2.TabIndex = 16
    Me.chk_wednesday_time2.Text = "xAnd"
    Me.chk_wednesday_time2.UseVisualStyleBackColor = True
    '
    'chk_tuesday_time2
    '
    Me.chk_tuesday_time2.AutoSize = True
    Me.chk_tuesday_time2.Location = New System.Drawing.Point(299, 65)
    Me.chk_tuesday_time2.Name = "chk_tuesday_time2"
    Me.chk_tuesday_time2.Size = New System.Drawing.Size(55, 17)
    Me.chk_tuesday_time2.TabIndex = 10
    Me.chk_tuesday_time2.Text = "xAnd"
    Me.chk_tuesday_time2.UseVisualStyleBackColor = True
    '
    'chk_monday_time2
    '
    Me.chk_monday_time2.AutoSize = True
    Me.chk_monday_time2.Location = New System.Drawing.Point(299, 26)
    Me.chk_monday_time2.Name = "chk_monday_time2"
    Me.chk_monday_time2.Size = New System.Drawing.Size(55, 17)
    Me.chk_monday_time2.TabIndex = 3
    Me.chk_monday_time2.Text = "xAnd"
    Me.chk_monday_time2.UseVisualStyleBackColor = True
    '
    'dtp_sunday_time1_to
    '
    Me.dtp_sunday_time1_to.Checked = True
    Me.dtp_sunday_time1_to.IsReadOnly = False
    Me.dtp_sunday_time1_to.Location = New System.Drawing.Point(193, 252)
    Me.dtp_sunday_time1_to.Name = "dtp_sunday_time1_to"
    Me.dtp_sunday_time1_to.ShowCheckBox = False
    Me.dtp_sunday_time1_to.ShowUpDown = True
    Me.dtp_sunday_time1_to.Size = New System.Drawing.Size(91, 24)
    Me.dtp_sunday_time1_to.SufixText = "Sufix Text"
    Me.dtp_sunday_time1_to.SufixTextVisible = True
    Me.dtp_sunday_time1_to.TabIndex = 39
    Me.dtp_sunday_time1_to.TextVisible = False
    Me.dtp_sunday_time1_to.TextWidth = 20
    Me.dtp_sunday_time1_to.Value = New Date(2013, 2, 22, 0, 0, 0, 0)
    '
    'dtp_saturday_time1_to
    '
    Me.dtp_saturday_time1_to.Checked = True
    Me.dtp_saturday_time1_to.IsReadOnly = False
    Me.dtp_saturday_time1_to.Location = New System.Drawing.Point(193, 214)
    Me.dtp_saturday_time1_to.Name = "dtp_saturday_time1_to"
    Me.dtp_saturday_time1_to.ShowCheckBox = False
    Me.dtp_saturday_time1_to.ShowUpDown = True
    Me.dtp_saturday_time1_to.Size = New System.Drawing.Size(91, 24)
    Me.dtp_saturday_time1_to.SufixText = "Sufix Text"
    Me.dtp_saturday_time1_to.SufixTextVisible = True
    Me.dtp_saturday_time1_to.TabIndex = 33
    Me.dtp_saturday_time1_to.TextVisible = False
    Me.dtp_saturday_time1_to.TextWidth = 20
    Me.dtp_saturday_time1_to.Value = New Date(2013, 2, 22, 0, 0, 0, 0)
    '
    'dtp_friday_time1_to
    '
    Me.dtp_friday_time1_to.Checked = True
    Me.dtp_friday_time1_to.IsReadOnly = False
    Me.dtp_friday_time1_to.Location = New System.Drawing.Point(193, 176)
    Me.dtp_friday_time1_to.Name = "dtp_friday_time1_to"
    Me.dtp_friday_time1_to.ShowCheckBox = False
    Me.dtp_friday_time1_to.ShowUpDown = True
    Me.dtp_friday_time1_to.Size = New System.Drawing.Size(91, 24)
    Me.dtp_friday_time1_to.SufixText = "Sufix Text"
    Me.dtp_friday_time1_to.SufixTextVisible = True
    Me.dtp_friday_time1_to.TabIndex = 27
    Me.dtp_friday_time1_to.TextVisible = False
    Me.dtp_friday_time1_to.TextWidth = 20
    Me.dtp_friday_time1_to.Value = New Date(2013, 2, 22, 0, 0, 0, 0)
    '
    'dtp_thursday_time1_to
    '
    Me.dtp_thursday_time1_to.Checked = True
    Me.dtp_thursday_time1_to.IsReadOnly = False
    Me.dtp_thursday_time1_to.Location = New System.Drawing.Point(193, 137)
    Me.dtp_thursday_time1_to.Name = "dtp_thursday_time1_to"
    Me.dtp_thursday_time1_to.ShowCheckBox = False
    Me.dtp_thursday_time1_to.ShowUpDown = True
    Me.dtp_thursday_time1_to.Size = New System.Drawing.Size(91, 24)
    Me.dtp_thursday_time1_to.SufixText = "Sufix Text"
    Me.dtp_thursday_time1_to.SufixTextVisible = True
    Me.dtp_thursday_time1_to.TabIndex = 21
    Me.dtp_thursday_time1_to.TextVisible = False
    Me.dtp_thursday_time1_to.TextWidth = 20
    Me.dtp_thursday_time1_to.Value = New Date(2013, 2, 22, 0, 0, 0, 0)
    '
    'dtp_wednesday_time1_to
    '
    Me.dtp_wednesday_time1_to.Checked = True
    Me.dtp_wednesday_time1_to.IsReadOnly = False
    Me.dtp_wednesday_time1_to.Location = New System.Drawing.Point(193, 99)
    Me.dtp_wednesday_time1_to.Name = "dtp_wednesday_time1_to"
    Me.dtp_wednesday_time1_to.ShowCheckBox = False
    Me.dtp_wednesday_time1_to.ShowUpDown = True
    Me.dtp_wednesday_time1_to.Size = New System.Drawing.Size(91, 24)
    Me.dtp_wednesday_time1_to.SufixText = "Sufix Text"
    Me.dtp_wednesday_time1_to.SufixTextVisible = True
    Me.dtp_wednesday_time1_to.TabIndex = 15
    Me.dtp_wednesday_time1_to.TextVisible = False
    Me.dtp_wednesday_time1_to.TextWidth = 20
    Me.dtp_wednesday_time1_to.Value = New Date(2013, 2, 22, 0, 0, 0, 0)
    '
    'dtp_tuesday_time1_to
    '
    Me.dtp_tuesday_time1_to.Checked = True
    Me.dtp_tuesday_time1_to.IsReadOnly = False
    Me.dtp_tuesday_time1_to.Location = New System.Drawing.Point(193, 61)
    Me.dtp_tuesday_time1_to.Name = "dtp_tuesday_time1_to"
    Me.dtp_tuesday_time1_to.ShowCheckBox = False
    Me.dtp_tuesday_time1_to.ShowUpDown = True
    Me.dtp_tuesday_time1_to.Size = New System.Drawing.Size(91, 24)
    Me.dtp_tuesday_time1_to.SufixText = "Sufix Text"
    Me.dtp_tuesday_time1_to.SufixTextVisible = True
    Me.dtp_tuesday_time1_to.TabIndex = 9
    Me.dtp_tuesday_time1_to.TextVisible = False
    Me.dtp_tuesday_time1_to.TextWidth = 20
    Me.dtp_tuesday_time1_to.Value = New Date(2013, 2, 22, 0, 0, 0, 0)
    '
    'dtp_monday_time1_to
    '
    Me.dtp_monday_time1_to.Checked = True
    Me.dtp_monday_time1_to.IsReadOnly = False
    Me.dtp_monday_time1_to.Location = New System.Drawing.Point(193, 22)
    Me.dtp_monday_time1_to.Name = "dtp_monday_time1_to"
    Me.dtp_monday_time1_to.ShowCheckBox = False
    Me.dtp_monday_time1_to.ShowUpDown = True
    Me.dtp_monday_time1_to.Size = New System.Drawing.Size(91, 24)
    Me.dtp_monday_time1_to.SufixText = "Sufix Text"
    Me.dtp_monday_time1_to.SufixTextVisible = True
    Me.dtp_monday_time1_to.TabIndex = 2
    Me.dtp_monday_time1_to.TextVisible = False
    Me.dtp_monday_time1_to.TextWidth = 20
    Me.dtp_monday_time1_to.Value = New Date(2013, 2, 22, 0, 0, 0, 0)
    '
    'dtp_sunday_time1_from
    '
    Me.dtp_sunday_time1_from.Checked = True
    Me.dtp_sunday_time1_from.IsReadOnly = False
    Me.dtp_sunday_time1_from.Location = New System.Drawing.Point(123, 252)
    Me.dtp_sunday_time1_from.Name = "dtp_sunday_time1_from"
    Me.dtp_sunday_time1_from.ShowCheckBox = False
    Me.dtp_sunday_time1_from.ShowUpDown = True
    Me.dtp_sunday_time1_from.Size = New System.Drawing.Size(71, 24)
    Me.dtp_sunday_time1_from.SufixText = "Sufix Text"
    Me.dtp_sunday_time1_from.SufixTextVisible = True
    Me.dtp_sunday_time1_from.TabIndex = 38
    Me.dtp_sunday_time1_from.TextVisible = False
    Me.dtp_sunday_time1_from.TextWidth = 0
    Me.dtp_sunday_time1_from.Value = New Date(2013, 2, 22, 0, 0, 0, 0)
    '
    'dtp_saturday_time1_from
    '
    Me.dtp_saturday_time1_from.Checked = True
    Me.dtp_saturday_time1_from.IsReadOnly = False
    Me.dtp_saturday_time1_from.Location = New System.Drawing.Point(123, 214)
    Me.dtp_saturday_time1_from.Name = "dtp_saturday_time1_from"
    Me.dtp_saturday_time1_from.ShowCheckBox = False
    Me.dtp_saturday_time1_from.ShowUpDown = True
    Me.dtp_saturday_time1_from.Size = New System.Drawing.Size(71, 24)
    Me.dtp_saturday_time1_from.SufixText = "Sufix Text"
    Me.dtp_saturday_time1_from.SufixTextVisible = True
    Me.dtp_saturday_time1_from.TabIndex = 32
    Me.dtp_saturday_time1_from.TextVisible = False
    Me.dtp_saturday_time1_from.TextWidth = 0
    Me.dtp_saturday_time1_from.Value = New Date(2013, 2, 22, 0, 0, 0, 0)
    '
    'dtp_friday_time1_from
    '
    Me.dtp_friday_time1_from.Checked = True
    Me.dtp_friday_time1_from.IsReadOnly = False
    Me.dtp_friday_time1_from.Location = New System.Drawing.Point(123, 176)
    Me.dtp_friday_time1_from.Name = "dtp_friday_time1_from"
    Me.dtp_friday_time1_from.ShowCheckBox = False
    Me.dtp_friday_time1_from.ShowUpDown = True
    Me.dtp_friday_time1_from.Size = New System.Drawing.Size(71, 24)
    Me.dtp_friday_time1_from.SufixText = "Sufix Text"
    Me.dtp_friday_time1_from.SufixTextVisible = True
    Me.dtp_friday_time1_from.TabIndex = 26
    Me.dtp_friday_time1_from.TextVisible = False
    Me.dtp_friday_time1_from.TextWidth = 0
    Me.dtp_friday_time1_from.Value = New Date(2013, 2, 22, 0, 0, 0, 0)
    '
    'dtp_thursday_time1_from
    '
    Me.dtp_thursday_time1_from.Checked = True
    Me.dtp_thursday_time1_from.IsReadOnly = False
    Me.dtp_thursday_time1_from.Location = New System.Drawing.Point(123, 137)
    Me.dtp_thursday_time1_from.Name = "dtp_thursday_time1_from"
    Me.dtp_thursday_time1_from.ShowCheckBox = False
    Me.dtp_thursday_time1_from.ShowUpDown = True
    Me.dtp_thursday_time1_from.Size = New System.Drawing.Size(71, 24)
    Me.dtp_thursday_time1_from.SufixText = "Sufix Text"
    Me.dtp_thursday_time1_from.SufixTextVisible = True
    Me.dtp_thursday_time1_from.TabIndex = 20
    Me.dtp_thursday_time1_from.TextVisible = False
    Me.dtp_thursday_time1_from.TextWidth = 0
    Me.dtp_thursday_time1_from.Value = New Date(2013, 2, 22, 0, 0, 0, 0)
    '
    'dtp_wednesday_time1_from
    '
    Me.dtp_wednesday_time1_from.Checked = True
    Me.dtp_wednesday_time1_from.IsReadOnly = False
    Me.dtp_wednesday_time1_from.Location = New System.Drawing.Point(123, 99)
    Me.dtp_wednesday_time1_from.Name = "dtp_wednesday_time1_from"
    Me.dtp_wednesday_time1_from.ShowCheckBox = False
    Me.dtp_wednesday_time1_from.ShowUpDown = True
    Me.dtp_wednesday_time1_from.Size = New System.Drawing.Size(71, 24)
    Me.dtp_wednesday_time1_from.SufixText = "Sufix Text"
    Me.dtp_wednesday_time1_from.SufixTextVisible = True
    Me.dtp_wednesday_time1_from.TabIndex = 14
    Me.dtp_wednesday_time1_from.TextVisible = False
    Me.dtp_wednesday_time1_from.TextWidth = 0
    Me.dtp_wednesday_time1_from.Value = New Date(2013, 2, 22, 0, 0, 0, 0)
    '
    'dtp_tuesday_time1_from
    '
    Me.dtp_tuesday_time1_from.Checked = True
    Me.dtp_tuesday_time1_from.IsReadOnly = False
    Me.dtp_tuesday_time1_from.Location = New System.Drawing.Point(123, 61)
    Me.dtp_tuesday_time1_from.Name = "dtp_tuesday_time1_from"
    Me.dtp_tuesday_time1_from.ShowCheckBox = False
    Me.dtp_tuesday_time1_from.ShowUpDown = True
    Me.dtp_tuesday_time1_from.Size = New System.Drawing.Size(71, 24)
    Me.dtp_tuesday_time1_from.SufixText = "Sufix Text"
    Me.dtp_tuesday_time1_from.SufixTextVisible = True
    Me.dtp_tuesday_time1_from.TabIndex = 8
    Me.dtp_tuesday_time1_from.TextVisible = False
    Me.dtp_tuesday_time1_from.TextWidth = 0
    Me.dtp_tuesday_time1_from.Value = New Date(2013, 2, 22, 0, 0, 0, 0)
    '
    'dtp_monday_time1_from
    '
    Me.dtp_monday_time1_from.Checked = True
    Me.dtp_monday_time1_from.IsReadOnly = False
    Me.dtp_monday_time1_from.Location = New System.Drawing.Point(123, 22)
    Me.dtp_monday_time1_from.Name = "dtp_monday_time1_from"
    Me.dtp_monday_time1_from.ShowCheckBox = False
    Me.dtp_monday_time1_from.ShowUpDown = True
    Me.dtp_monday_time1_from.Size = New System.Drawing.Size(71, 24)
    Me.dtp_monday_time1_from.SufixText = "Sufix Text"
    Me.dtp_monday_time1_from.SufixTextVisible = True
    Me.dtp_monday_time1_from.TabIndex = 1
    Me.dtp_monday_time1_from.TextVisible = False
    Me.dtp_monday_time1_from.TextWidth = 0
    Me.dtp_monday_time1_from.Value = New Date(2013, 2, 22, 0, 0, 0, 0)
    '
    'chk_sunday
    '
    Me.chk_sunday.AutoSize = True
    Me.chk_sunday.Location = New System.Drawing.Point(20, 256)
    Me.chk_sunday.Name = "chk_sunday"
    Me.chk_sunday.Size = New System.Drawing.Size(76, 17)
    Me.chk_sunday.TabIndex = 37
    Me.chk_sunday.Text = "xSunday"
    Me.chk_sunday.UseVisualStyleBackColor = True
    '
    'chk_saturday
    '
    Me.chk_saturday.AutoSize = True
    Me.chk_saturday.Location = New System.Drawing.Point(20, 218)
    Me.chk_saturday.Name = "chk_saturday"
    Me.chk_saturday.Size = New System.Drawing.Size(85, 17)
    Me.chk_saturday.TabIndex = 31
    Me.chk_saturday.Text = "xSaturday"
    Me.chk_saturday.UseVisualStyleBackColor = True
    '
    'chk_friday
    '
    Me.chk_friday.AutoSize = True
    Me.chk_friday.Location = New System.Drawing.Point(20, 179)
    Me.chk_friday.Name = "chk_friday"
    Me.chk_friday.Size = New System.Drawing.Size(68, 17)
    Me.chk_friday.TabIndex = 25
    Me.chk_friday.Text = "xFriday"
    Me.chk_friday.UseVisualStyleBackColor = True
    '
    'chk_thursday
    '
    Me.chk_thursday.AutoSize = True
    Me.chk_thursday.Location = New System.Drawing.Point(20, 141)
    Me.chk_thursday.Name = "chk_thursday"
    Me.chk_thursday.Size = New System.Drawing.Size(86, 17)
    Me.chk_thursday.TabIndex = 19
    Me.chk_thursday.Text = "xThursday"
    Me.chk_thursday.UseVisualStyleBackColor = True
    '
    'chk_wednesday
    '
    Me.chk_wednesday.AutoSize = True
    Me.chk_wednesday.Location = New System.Drawing.Point(20, 103)
    Me.chk_wednesday.Name = "chk_wednesday"
    Me.chk_wednesday.Size = New System.Drawing.Size(99, 17)
    Me.chk_wednesday.TabIndex = 13
    Me.chk_wednesday.Text = "xWednesday"
    Me.chk_wednesday.UseVisualStyleBackColor = True
    '
    'chk_tuesday
    '
    Me.chk_tuesday.AutoSize = True
    Me.chk_tuesday.Location = New System.Drawing.Point(20, 65)
    Me.chk_tuesday.Name = "chk_tuesday"
    Me.chk_tuesday.Size = New System.Drawing.Size(81, 17)
    Me.chk_tuesday.TabIndex = 7
    Me.chk_tuesday.Text = "xTuesday"
    Me.chk_tuesday.UseVisualStyleBackColor = True
    '
    'chk_monday
    '
    Me.chk_monday.AutoSize = True
    Me.chk_monday.Location = New System.Drawing.Point(20, 26)
    Me.chk_monday.Name = "chk_monday"
    Me.chk_monday.Size = New System.Drawing.Size(77, 17)
    Me.chk_monday.TabIndex = 0
    Me.chk_monday.Text = "xMonday"
    Me.chk_monday.UseVisualStyleBackColor = True
    '
    'tab_exceptions
    '
    Me.tab_exceptions.Controls.Add(Me.gb_new_exception)
    Me.tab_exceptions.Controls.Add(Me.btn_delete_exceptions)
    Me.tab_exceptions.Controls.Add(Me.dg_exceptions)
    Me.tab_exceptions.Location = New System.Drawing.Point(4, 22)
    Me.tab_exceptions.Name = "tab_exceptions"
    Me.tab_exceptions.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_exceptions.Size = New System.Drawing.Size(654, 402)
    Me.tab_exceptions.TabIndex = 1
    Me.tab_exceptions.Text = "xExceptions"
    Me.tab_exceptions.UseVisualStyleBackColor = True
    '
    'gb_new_exception
    '
    Me.gb_new_exception.Controls.Add(Me.dtp_new_exception_date_to)
    Me.gb_new_exception.Controls.Add(Me.chk_new_exception_date_range)
    Me.gb_new_exception.Controls.Add(Me.chk_new_exception_allow)
    Me.gb_new_exception.Controls.Add(Me.dtp_new_exception_date_from)
    Me.gb_new_exception.Controls.Add(Me.btn_add_exception)
    Me.gb_new_exception.Controls.Add(Me.dtp_new_exception_time2_to)
    Me.gb_new_exception.Controls.Add(Me.dtp_new_exception_time2_from)
    Me.gb_new_exception.Controls.Add(Me.chk_new_exception_time2)
    Me.gb_new_exception.Controls.Add(Me.dtp_new_exception_time1_to)
    Me.gb_new_exception.Controls.Add(Me.dtp_new_exception_time1_from)
    Me.gb_new_exception.Location = New System.Drawing.Point(7, 244)
    Me.gb_new_exception.Name = "gb_new_exception"
    Me.gb_new_exception.Size = New System.Drawing.Size(641, 100)
    Me.gb_new_exception.TabIndex = 2
    Me.gb_new_exception.TabStop = False
    Me.gb_new_exception.Text = "GroupBox1"
    '
    'dtp_new_exception_date_to
    '
    Me.dtp_new_exception_date_to.Checked = True
    Me.dtp_new_exception_date_to.IsReadOnly = False
    Me.dtp_new_exception_date_to.Location = New System.Drawing.Point(60, 58)
    Me.dtp_new_exception_date_to.Name = "dtp_new_exception_date_to"
    Me.dtp_new_exception_date_to.ShowCheckBox = False
    Me.dtp_new_exception_date_to.ShowUpDown = False
    Me.dtp_new_exception_date_to.Size = New System.Drawing.Size(106, 24)
    Me.dtp_new_exception_date_to.SufixText = "Sufix Text"
    Me.dtp_new_exception_date_to.TabIndex = 2
    Me.dtp_new_exception_date_to.TextVisible = False
    Me.dtp_new_exception_date_to.TextWidth = 0
    Me.dtp_new_exception_date_to.Value = New Date(2013, 2, 25, 0, 0, 0, 0)
    '
    'chk_new_exception_date_range
    '
    Me.chk_new_exception_date_range.AutoSize = True
    Me.chk_new_exception_date_range.Location = New System.Drawing.Point(21, 62)
    Me.chk_new_exception_date_range.Name = "chk_new_exception_date_range"
    Me.chk_new_exception_date_range.Size = New System.Drawing.Size(47, 17)
    Me.chk_new_exception_date_range.TabIndex = 1
    Me.chk_new_exception_date_range.Text = "xTo"
    Me.chk_new_exception_date_range.UseVisualStyleBackColor = True
    '
    'chk_new_exception_allow
    '
    Me.chk_new_exception_allow.AutoSize = True
    Me.chk_new_exception_allow.Checked = True
    Me.chk_new_exception_allow.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_new_exception_allow.Location = New System.Drawing.Point(174, 35)
    Me.chk_new_exception_allow.Name = "chk_new_exception_allow"
    Me.chk_new_exception_allow.Size = New System.Drawing.Size(129, 17)
    Me.chk_new_exception_allow.TabIndex = 3
    Me.chk_new_exception_allow.Text = "xAllow Operations"
    Me.chk_new_exception_allow.UseVisualStyleBackColor = True
    '
    'dtp_new_exception_date_from
    '
    Me.dtp_new_exception_date_from.Checked = True
    Me.dtp_new_exception_date_from.IsReadOnly = False
    Me.dtp_new_exception_date_from.Location = New System.Drawing.Point(60, 28)
    Me.dtp_new_exception_date_from.Name = "dtp_new_exception_date_from"
    Me.dtp_new_exception_date_from.ShowCheckBox = False
    Me.dtp_new_exception_date_from.ShowUpDown = False
    Me.dtp_new_exception_date_from.Size = New System.Drawing.Size(106, 24)
    Me.dtp_new_exception_date_from.SufixText = "Sufix Text"
    Me.dtp_new_exception_date_from.TabIndex = 0
    Me.dtp_new_exception_date_from.TextVisible = False
    Me.dtp_new_exception_date_from.TextWidth = 0
    Me.dtp_new_exception_date_from.Value = New Date(2013, 2, 25, 0, 0, 0, 0)
    '
    'btn_add_exception
    '
    Me.btn_add_exception.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_add_exception.Location = New System.Drawing.Point(562, 59)
    Me.btn_add_exception.Name = "btn_add_exception"
    Me.btn_add_exception.Size = New System.Drawing.Size(63, 21)
    Me.btn_add_exception.TabIndex = 9
    Me.btn_add_exception.ToolTipped = False
    Me.btn_add_exception.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.SMALL
    '
    'dtp_new_exception_time2_to
    '
    Me.dtp_new_exception_time2_to.Checked = True
    Me.dtp_new_exception_time2_to.IsReadOnly = False
    Me.dtp_new_exception_time2_to.Location = New System.Drawing.Point(451, 58)
    Me.dtp_new_exception_time2_to.Name = "dtp_new_exception_time2_to"
    Me.dtp_new_exception_time2_to.ShowCheckBox = False
    Me.dtp_new_exception_time2_to.ShowUpDown = True
    Me.dtp_new_exception_time2_to.Size = New System.Drawing.Size(91, 24)
    Me.dtp_new_exception_time2_to.SufixText = "Sufix Text"
    Me.dtp_new_exception_time2_to.TabIndex = 8
    Me.dtp_new_exception_time2_to.TextVisible = False
    Me.dtp_new_exception_time2_to.TextWidth = 20
    Me.dtp_new_exception_time2_to.Value = New Date(2013, 2, 22, 0, 0, 0, 0)
    '
    'dtp_new_exception_time2_from
    '
    Me.dtp_new_exception_time2_from.Checked = True
    Me.dtp_new_exception_time2_from.IsReadOnly = False
    Me.dtp_new_exception_time2_from.Location = New System.Drawing.Point(381, 58)
    Me.dtp_new_exception_time2_from.Name = "dtp_new_exception_time2_from"
    Me.dtp_new_exception_time2_from.ShowCheckBox = False
    Me.dtp_new_exception_time2_from.ShowUpDown = True
    Me.dtp_new_exception_time2_from.Size = New System.Drawing.Size(71, 24)
    Me.dtp_new_exception_time2_from.SufixText = "Sufix Text"
    Me.dtp_new_exception_time2_from.TabIndex = 7
    Me.dtp_new_exception_time2_from.TextVisible = False
    Me.dtp_new_exception_time2_from.TextWidth = 0
    Me.dtp_new_exception_time2_from.Value = New Date(2013, 2, 22, 0, 0, 0, 0)
    '
    'chk_new_exception_time2
    '
    Me.chk_new_exception_time2.AutoSize = True
    Me.chk_new_exception_time2.Location = New System.Drawing.Point(341, 62)
    Me.chk_new_exception_time2.Name = "chk_new_exception_time2"
    Me.chk_new_exception_time2.Size = New System.Drawing.Size(55, 17)
    Me.chk_new_exception_time2.TabIndex = 6
    Me.chk_new_exception_time2.Text = "xAnd"
    Me.chk_new_exception_time2.UseVisualStyleBackColor = True
    '
    'dtp_new_exception_time1_to
    '
    Me.dtp_new_exception_time1_to.Checked = True
    Me.dtp_new_exception_time1_to.IsReadOnly = False
    Me.dtp_new_exception_time1_to.Location = New System.Drawing.Point(242, 58)
    Me.dtp_new_exception_time1_to.Name = "dtp_new_exception_time1_to"
    Me.dtp_new_exception_time1_to.ShowCheckBox = False
    Me.dtp_new_exception_time1_to.ShowUpDown = True
    Me.dtp_new_exception_time1_to.Size = New System.Drawing.Size(91, 24)
    Me.dtp_new_exception_time1_to.SufixText = "Sufix Text"
    Me.dtp_new_exception_time1_to.TabIndex = 5
    Me.dtp_new_exception_time1_to.TextVisible = False
    Me.dtp_new_exception_time1_to.TextWidth = 20
    Me.dtp_new_exception_time1_to.Value = New Date(2013, 2, 22, 0, 0, 0, 0)
    '
    'dtp_new_exception_time1_from
    '
    Me.dtp_new_exception_time1_from.Checked = True
    Me.dtp_new_exception_time1_from.IsReadOnly = False
    Me.dtp_new_exception_time1_from.Location = New System.Drawing.Point(172, 58)
    Me.dtp_new_exception_time1_from.Name = "dtp_new_exception_time1_from"
    Me.dtp_new_exception_time1_from.ShowCheckBox = False
    Me.dtp_new_exception_time1_from.ShowUpDown = True
    Me.dtp_new_exception_time1_from.Size = New System.Drawing.Size(71, 24)
    Me.dtp_new_exception_time1_from.SufixText = "Sufix Text"
    Me.dtp_new_exception_time1_from.TabIndex = 4
    Me.dtp_new_exception_time1_from.TextVisible = False
    Me.dtp_new_exception_time1_from.TextWidth = 0
    Me.dtp_new_exception_time1_from.Value = New Date(2013, 2, 22, 0, 0, 0, 0)
    '
    'btn_delete_exceptions
    '
    Me.btn_delete_exceptions.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_delete_exceptions.Location = New System.Drawing.Point(569, 214)
    Me.btn_delete_exceptions.Name = "btn_delete_exceptions"
    Me.btn_delete_exceptions.Size = New System.Drawing.Size(63, 21)
    Me.btn_delete_exceptions.TabIndex = 1
    Me.btn_delete_exceptions.ToolTipped = False
    Me.btn_delete_exceptions.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.SMALL
    '
    'dg_exceptions
    '
    Me.dg_exceptions.CurrentCol = -1
    Me.dg_exceptions.CurrentRow = -1
    Me.dg_exceptions.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_exceptions.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_exceptions.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_exceptions.Location = New System.Drawing.Point(7, 7)
    Me.dg_exceptions.Name = "dg_exceptions"
    Me.dg_exceptions.PanelRightVisible = False
    Me.dg_exceptions.Redraw = True
    Me.dg_exceptions.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_exceptions.Size = New System.Drawing.Size(556, 228)
    Me.dg_exceptions.Sortable = False
    Me.dg_exceptions.SortAscending = True
    Me.dg_exceptions.SortByCol = 0
    Me.dg_exceptions.TabIndex = 0
    Me.dg_exceptions.ToolTipped = True
    Me.dg_exceptions.TopRow = -2
    '
    'tab_calendar
    '
    Me.tab_calendar.Controls.Add(Me.lbl_calendar_instructions)
    Me.tab_calendar.Controls.Add(Me.lbl_calendar_error)
    Me.tab_calendar.Controls.Add(Me.lbl_calendar_schedule)
    Me.tab_calendar.Controls.Add(Me.calendar)
    Me.tab_calendar.Location = New System.Drawing.Point(4, 22)
    Me.tab_calendar.Name = "tab_calendar"
    Me.tab_calendar.Size = New System.Drawing.Size(654, 402)
    Me.tab_calendar.TabIndex = 2
    Me.tab_calendar.Text = "xCalendar"
    Me.tab_calendar.UseVisualStyleBackColor = True
    '
    'lbl_calendar_instructions
    '
    Me.lbl_calendar_instructions.Location = New System.Drawing.Point(96, 28)
    Me.lbl_calendar_instructions.Name = "lbl_calendar_instructions"
    Me.lbl_calendar_instructions.Size = New System.Drawing.Size(452, 42)
    Me.lbl_calendar_instructions.TabIndex = 3
    Me.lbl_calendar_instructions.Text = "xInstructions"
    '
    'lbl_calendar_error
    '
    Me.lbl_calendar_error.AutoSize = True
    Me.lbl_calendar_error.ForeColor = System.Drawing.Color.Red
    Me.lbl_calendar_error.Location = New System.Drawing.Point(93, 280)
    Me.lbl_calendar_error.Name = "lbl_calendar_error"
    Me.lbl_calendar_error.Size = New System.Drawing.Size(99, 13)
    Me.lbl_calendar_error.TabIndex = 2
    Me.lbl_calendar_error.Text = "xSchedule error"
    '
    'lbl_calendar_schedule
    '
    Me.lbl_calendar_schedule.AutoSize = True
    Me.lbl_calendar_schedule.Location = New System.Drawing.Point(93, 261)
    Me.lbl_calendar_schedule.Name = "lbl_calendar_schedule"
    Me.lbl_calendar_schedule.Size = New System.Drawing.Size(168, 13)
    Me.lbl_calendar_schedule.TabIndex = 1
    Me.lbl_calendar_schedule.Text = "xOperations Schedule string"
    '
    'calendar
    '
    Me.calendar.CalendarDimensions = New System.Drawing.Size(2, 1)
    Me.calendar.Location = New System.Drawing.Point(90, 82)
    Me.calendar.MaxSelectionCount = 1
    Me.calendar.Name = "calendar"
    Me.calendar.TabIndex = 0
    '
    'tab_notifications
    '
    Me.tab_notifications.Controls.Add(Me.gb_notifications_recipients)
    Me.tab_notifications.Controls.Add(Me.gb_new_recipient)
    Me.tab_notifications.Controls.Add(Me.ef_subject)
    Me.tab_notifications.Controls.Add(Me.chk_notifications_enabled)
    Me.tab_notifications.Location = New System.Drawing.Point(4, 22)
    Me.tab_notifications.Name = "tab_notifications"
    Me.tab_notifications.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_notifications.Size = New System.Drawing.Size(654, 402)
    Me.tab_notifications.TabIndex = 3
    Me.tab_notifications.Text = "xNotifications"
    Me.tab_notifications.UseVisualStyleBackColor = True
    '
    'gb_notifications_recipients
    '
    Me.gb_notifications_recipients.Controls.Add(Me.dg_recipients)
    Me.gb_notifications_recipients.Controls.Add(Me.btn_delete_recipients)
    Me.gb_notifications_recipients.Location = New System.Drawing.Point(60, 77)
    Me.gb_notifications_recipients.Name = "gb_notifications_recipients"
    Me.gb_notifications_recipients.Size = New System.Drawing.Size(519, 207)
    Me.gb_notifications_recipients.TabIndex = 2
    Me.gb_notifications_recipients.TabStop = False
    Me.gb_notifications_recipients.Text = "xRecipients"
    '
    'dg_recipients
    '
    Me.dg_recipients.CurrentCol = -1
    Me.dg_recipients.CurrentRow = -1
    Me.dg_recipients.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_recipients.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_recipients.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_recipients.Location = New System.Drawing.Point(6, 20)
    Me.dg_recipients.Name = "dg_recipients"
    Me.dg_recipients.PanelRightVisible = False
    Me.dg_recipients.Redraw = True
    Me.dg_recipients.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_recipients.Size = New System.Drawing.Size(438, 180)
    Me.dg_recipients.Sortable = False
    Me.dg_recipients.SortAscending = True
    Me.dg_recipients.SortByCol = 0
    Me.dg_recipients.TabIndex = 0
    Me.dg_recipients.ToolTipped = True
    Me.dg_recipients.TopRow = -2
    '
    'btn_delete_recipients
    '
    Me.btn_delete_recipients.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_delete_recipients.Location = New System.Drawing.Point(450, 179)
    Me.btn_delete_recipients.Name = "btn_delete_recipients"
    Me.btn_delete_recipients.Size = New System.Drawing.Size(63, 21)
    Me.btn_delete_recipients.TabIndex = 1
    Me.btn_delete_recipients.ToolTipped = False
    Me.btn_delete_recipients.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.SMALL
    '
    'gb_new_recipient
    '
    Me.gb_new_recipient.Controls.Add(Me.btn_add_recipient)
    Me.gb_new_recipient.Controls.Add(Me.ef_new_recipient)
    Me.gb_new_recipient.Location = New System.Drawing.Point(60, 290)
    Me.gb_new_recipient.Name = "gb_new_recipient"
    Me.gb_new_recipient.Size = New System.Drawing.Size(519, 55)
    Me.gb_new_recipient.TabIndex = 3
    Me.gb_new_recipient.TabStop = False
    Me.gb_new_recipient.Text = "xNew Recipient"
    '
    'btn_add_recipient
    '
    Me.btn_add_recipient.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_add_recipient.Location = New System.Drawing.Point(450, 24)
    Me.btn_add_recipient.Name = "btn_add_recipient"
    Me.btn_add_recipient.Size = New System.Drawing.Size(63, 21)
    Me.btn_add_recipient.TabIndex = 1
    Me.btn_add_recipient.ToolTipped = False
    Me.btn_add_recipient.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.SMALL
    '
    'ef_new_recipient
    '
    Me.ef_new_recipient.DoubleValue = 0.0R
    Me.ef_new_recipient.IntegerValue = 0
    Me.ef_new_recipient.IsReadOnly = False
    Me.ef_new_recipient.Location = New System.Drawing.Point(6, 20)
    Me.ef_new_recipient.Name = "ef_new_recipient"
    Me.ef_new_recipient.PlaceHolder = Nothing
    Me.ef_new_recipient.Size = New System.Drawing.Size(438, 24)
    Me.ef_new_recipient.SufixText = "Sufix Text"
    Me.ef_new_recipient.TabIndex = 0
    Me.ef_new_recipient.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_new_recipient.TextValue = ""
    Me.ef_new_recipient.TextVisible = False
    Me.ef_new_recipient.TextWidth = 60
    Me.ef_new_recipient.Value = ""
    Me.ef_new_recipient.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_subject
    '
    Me.ef_subject.DoubleValue = 0.0R
    Me.ef_subject.IntegerValue = 0
    Me.ef_subject.IsReadOnly = False
    Me.ef_subject.Location = New System.Drawing.Point(60, 47)
    Me.ef_subject.Name = "ef_subject"
    Me.ef_subject.PlaceHolder = Nothing
    Me.ef_subject.Size = New System.Drawing.Size(519, 24)
    Me.ef_subject.SufixText = "Sufix Text"
    Me.ef_subject.TabIndex = 1
    Me.ef_subject.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_subject.TextValue = ""
    Me.ef_subject.TextVisible = False
    Me.ef_subject.TextWidth = 66
    Me.ef_subject.Value = ""
    Me.ef_subject.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_notifications_enabled
    '
    Me.chk_notifications_enabled.Location = New System.Drawing.Point(56, 5)
    Me.chk_notifications_enabled.Name = "chk_notifications_enabled"
    Me.chk_notifications_enabled.Size = New System.Drawing.Size(519, 39)
    Me.chk_notifications_enabled.TabIndex = 0
    Me.chk_notifications_enabled.Text = "xEnable notifications"
    Me.chk_notifications_enabled.UseVisualStyleBackColor = True
    '
    'tab_shifts
    '
    Me.tab_shifts.Controls.Add(Me.gb_daily_cash_sessions)
    Me.tab_shifts.Controls.Add(Me.gb_work_shifts)
    Me.tab_shifts.Location = New System.Drawing.Point(4, 22)
    Me.tab_shifts.Name = "tab_shifts"
    Me.tab_shifts.Size = New System.Drawing.Size(654, 402)
    Me.tab_shifts.TabIndex = 4
    Me.tab_shifts.Text = "xShifts"
    Me.tab_shifts.UseVisualStyleBackColor = True
    '
    'gb_daily_cash_sessions
    '
    Me.gb_daily_cash_sessions.Controls.Add(Me.lbl_info_permission)
    Me.gb_daily_cash_sessions.Controls.Add(Me.ef_max_daily_cash_openings)
    Me.gb_daily_cash_sessions.Location = New System.Drawing.Point(112, 115)
    Me.gb_daily_cash_sessions.Name = "gb_daily_cash_sessions"
    Me.gb_daily_cash_sessions.Size = New System.Drawing.Size(439, 133)
    Me.gb_daily_cash_sessions.TabIndex = 1
    Me.gb_daily_cash_sessions.TabStop = False
    Me.gb_daily_cash_sessions.Text = "xDaily Cash Sessions"
    '
    'lbl_info_permission
    '
    Me.lbl_info_permission.Font = New System.Drawing.Font("Courier New", 8.25!)
    Me.lbl_info_permission.ForeColor = System.Drawing.Color.Navy
    Me.lbl_info_permission.Location = New System.Drawing.Point(21, 63)
    Me.lbl_info_permission.Name = "lbl_info_permission"
    Me.lbl_info_permission.Size = New System.Drawing.Size(393, 57)
    Me.lbl_info_permission.TabIndex = 1
    Me.lbl_info_permission.Text = "xTo open additional cash sessions in the same day..."
    '
    'ef_max_daily_cash_openings
    '
    Me.ef_max_daily_cash_openings.DoubleValue = 0.0R
    Me.ef_max_daily_cash_openings.IntegerValue = 0
    Me.ef_max_daily_cash_openings.IsReadOnly = False
    Me.ef_max_daily_cash_openings.Location = New System.Drawing.Point(21, 26)
    Me.ef_max_daily_cash_openings.Name = "ef_max_daily_cash_openings"
    Me.ef_max_daily_cash_openings.PlaceHolder = Nothing
    Me.ef_max_daily_cash_openings.Size = New System.Drawing.Size(393, 24)
    Me.ef_max_daily_cash_openings.SufixText = "Sufix Text"
    Me.ef_max_daily_cash_openings.TabIndex = 0
    Me.ef_max_daily_cash_openings.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_max_daily_cash_openings.TextValue = ""
    Me.ef_max_daily_cash_openings.TextVisible = False
    Me.ef_max_daily_cash_openings.TextWidth = 350
    Me.ef_max_daily_cash_openings.Value = ""
    Me.ef_max_daily_cash_openings.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_work_shifts
    '
    Me.gb_work_shifts.Controls.Add(Me.ef_work_shift_duration)
    Me.gb_work_shifts.Controls.Add(Me.chk_enable_shift_duration)
    Me.gb_work_shifts.Location = New System.Drawing.Point(112, 13)
    Me.gb_work_shifts.Name = "gb_work_shifts"
    Me.gb_work_shifts.Size = New System.Drawing.Size(439, 94)
    Me.gb_work_shifts.TabIndex = 0
    Me.gb_work_shifts.TabStop = False
    Me.gb_work_shifts.Text = "xWork Shifts"
    '
    'ef_work_shift_duration
    '
    Me.ef_work_shift_duration.DoubleValue = 0.0R
    Me.ef_work_shift_duration.IntegerValue = 0
    Me.ef_work_shift_duration.IsReadOnly = False
    Me.ef_work_shift_duration.Location = New System.Drawing.Point(36, 48)
    Me.ef_work_shift_duration.Name = "ef_work_shift_duration"
    Me.ef_work_shift_duration.PlaceHolder = Nothing
    Me.ef_work_shift_duration.Size = New System.Drawing.Size(204, 24)
    Me.ef_work_shift_duration.SufixText = "Sufix Text"
    Me.ef_work_shift_duration.SufixTextWidth = 50
    Me.ef_work_shift_duration.TabIndex = 1
    Me.ef_work_shift_duration.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_work_shift_duration.TextValue = ""
    Me.ef_work_shift_duration.TextVisible = False
    Me.ef_work_shift_duration.TextWidth = 115
    Me.ef_work_shift_duration.Value = ""
    Me.ef_work_shift_duration.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_enable_shift_duration
    '
    Me.chk_enable_shift_duration.AutoSize = True
    Me.chk_enable_shift_duration.Location = New System.Drawing.Point(21, 25)
    Me.chk_enable_shift_duration.Name = "chk_enable_shift_duration"
    Me.chk_enable_shift_duration.Size = New System.Drawing.Size(177, 17)
    Me.chk_enable_shift_duration.TabIndex = 0
    Me.chk_enable_shift_duration.Text = "xLimit Work Shift Duration"
    Me.chk_enable_shift_duration.UseVisualStyleBackColor = True
    '
    'frm_operations_schedule
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(770, 442)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_operations_schedule"
    Me.Text = "frm_operations_schedule"
    Me.panel_data.ResumeLayout(False)
    Me.tab_control.ResumeLayout(False)
    Me.tab_schedule.ResumeLayout(False)
    Me.tab_schedule.PerformLayout()
    Me.gb_schedule.ResumeLayout(False)
    Me.gb_schedule.PerformLayout()
    Me.tab_exceptions.ResumeLayout(False)
    Me.gb_new_exception.ResumeLayout(False)
    Me.gb_new_exception.PerformLayout()
    Me.tab_calendar.ResumeLayout(False)
    Me.tab_calendar.PerformLayout()
    Me.tab_notifications.ResumeLayout(False)
    Me.gb_notifications_recipients.ResumeLayout(False)
    Me.gb_new_recipient.ResumeLayout(False)
    Me.tab_shifts.ResumeLayout(False)
    Me.gb_daily_cash_sessions.ResumeLayout(False)
    Me.gb_work_shifts.ResumeLayout(False)
    Me.gb_work_shifts.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents tab_control As System.Windows.Forms.TabControl
  Friend WithEvents tab_schedule As System.Windows.Forms.TabPage
  Friend WithEvents tab_exceptions As System.Windows.Forms.TabPage
  Friend WithEvents tab_calendar As System.Windows.Forms.TabPage
  Friend WithEvents gb_schedule As System.Windows.Forms.GroupBox
  Friend WithEvents chk_monday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_sunday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_saturday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_friday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_thursday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_wednesday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_tuesday As System.Windows.Forms.CheckBox
  Friend WithEvents dtp_monday_time1_from As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_sunday_time1_from As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_saturday_time1_from As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_friday_time1_from As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_thursday_time1_from As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_wednesday_time1_from As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_tuesday_time1_from As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_sunday_time1_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_saturday_time1_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_friday_time1_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_thursday_time1_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_wednesday_time1_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_tuesday_time1_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_monday_time1_to As GUI_Controls.uc_date_picker
  Friend WithEvents chk_sunday_time2 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_saturday_time2 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_friday_time2 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_thursday_time2 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_wednesday_time2 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_tuesday_time2 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_monday_time2 As System.Windows.Forms.CheckBox
  Friend WithEvents dtp_sunday_time2_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_saturday_time2_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_friday_time2_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_thursday_time2_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_wednesday_time2_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_tuesday_time2_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_monday_time2_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_sunday_time2_from As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_saturday_time2_from As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_friday_time2_from As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_thursday_time2_from As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_wednesday_time2_from As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_tuesday_time2_from As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_monday_time2_from As GUI_Controls.uc_date_picker
  Friend WithEvents btn_all As GUI_Controls.uc_button
  Friend WithEvents btn_delete_exceptions As GUI_Controls.uc_button
  Friend WithEvents dg_exceptions As GUI_Controls.uc_grid
  Friend WithEvents gb_new_exception As System.Windows.Forms.GroupBox
  Friend WithEvents chk_new_exception_allow As System.Windows.Forms.CheckBox
  Friend WithEvents dtp_new_exception_date_from As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_new_exception_date_to As GUI_Controls.uc_date_picker
  Friend WithEvents btn_add_exception As GUI_Controls.uc_button
  Friend WithEvents dtp_new_exception_time2_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_new_exception_time2_from As GUI_Controls.uc_date_picker
  Friend WithEvents chk_new_exception_time2 As System.Windows.Forms.CheckBox
  Friend WithEvents dtp_new_exception_time1_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_new_exception_time1_from As GUI_Controls.uc_date_picker
  Friend WithEvents chk_new_exception_date_range As System.Windows.Forms.CheckBox
  Friend WithEvents calendar As System.Windows.Forms.MonthCalendar
  Friend WithEvents lbl_calendar_schedule As System.Windows.Forms.Label
  Friend WithEvents lbl_calendar_error As System.Windows.Forms.Label
  Friend WithEvents lbl_calendar_instructions As System.Windows.Forms.Label
  Friend WithEvents tab_notifications As System.Windows.Forms.TabPage
  Friend WithEvents chk_notifications_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents ef_subject As GUI_Controls.uc_entry_field
  Friend WithEvents dg_recipients As GUI_Controls.uc_grid
  Friend WithEvents gb_notifications_recipients As System.Windows.Forms.GroupBox
  Friend WithEvents btn_delete_recipients As GUI_Controls.uc_button
  Friend WithEvents gb_new_recipient As System.Windows.Forms.GroupBox
  Friend WithEvents btn_add_recipient As GUI_Controls.uc_button
  Friend WithEvents ef_new_recipient As GUI_Controls.uc_entry_field
  Friend WithEvents cmb_closing_time As GUI_Controls.uc_combo
  Friend WithEvents chk_schedule_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents tab_shifts As System.Windows.Forms.TabPage
  Friend WithEvents gb_work_shifts As System.Windows.Forms.GroupBox
  Friend WithEvents chk_enable_shift_duration As System.Windows.Forms.CheckBox
  Friend WithEvents ef_work_shift_duration As GUI_Controls.uc_entry_field
  Friend WithEvents gb_daily_cash_sessions As System.Windows.Forms.GroupBox
  Friend WithEvents ef_max_daily_cash_openings As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_info_permission As System.Windows.Forms.Label
  Friend WithEvents chk_disable_machines As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_disable_machines As System.Windows.Forms.Label
End Class
