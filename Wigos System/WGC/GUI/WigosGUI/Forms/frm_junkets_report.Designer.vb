﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_junkets_report
    Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.uc_account_sel = New GUI_Controls.uc_account_sel()
    Me.gb_report_type = New System.Windows.Forms.GroupBox()
    Me.opt_representative = New System.Windows.Forms.RadioButton()
    Me.opt_working_day = New System.Windows.Forms.RadioButton()
    Me.opt_customers = New System.Windows.Forms.RadioButton()
    Me.gb_junkets = New System.Windows.Forms.GroupBox()
    Me.opt_all_junkets = New System.Windows.Forms.RadioButton()
    Me.opt_several_junkets = New System.Windows.Forms.RadioButton()
    Me.dg_filter_junkets = New GUI_Controls.uc_grid()
    Me.gb_representatives = New System.Windows.Forms.GroupBox()
    Me.opt_all_representatives = New System.Windows.Forms.RadioButton()
    Me.opt_several_representatives = New System.Windows.Forms.RadioButton()
    Me.dg_filter_representatives = New GUI_Controls.uc_grid()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.lbl_to_hour = New System.Windows.Forms.Label()
    Me.lbl_from_hour = New System.Windows.Forms.Label()
    Me.chk_show_without_activity = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_report_type.SuspendLayout()
    Me.gb_junkets.SuspendLayout()
    Me.gb_representatives.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.chk_show_without_activity)
    Me.panel_filter.Controls.Add(Me.gb_junkets)
    Me.panel_filter.Controls.Add(Me.gb_representatives)
    Me.panel_filter.Controls.Add(Me.gb_report_type)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Controls.Add(Me.uc_account_sel)
    Me.panel_filter.Size = New System.Drawing.Size(1147, 234)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account_sel, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_report_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_representatives, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_junkets, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_show_without_activity, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 238)
    Me.panel_data.Size = New System.Drawing.Size(1147, 450)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1141, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1141, 4)
    '
    'uc_account_sel
    '
    Me.uc_account_sel.Account = ""
    Me.uc_account_sel.AccountText = ""
    Me.uc_account_sel.Anon = False
    Me.uc_account_sel.AutoSize = True
    Me.uc_account_sel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_account_sel.BirthDate = New Date(CType(0, Long))
    Me.uc_account_sel.DisabledHolder = False
    Me.uc_account_sel.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_account_sel.Holder = ""
    Me.uc_account_sel.Location = New System.Drawing.Point(3, 95)
    Me.uc_account_sel.MassiveSearchNumbers = ""
    Me.uc_account_sel.MassiveSearchNumbersToEdit = ""
    Me.uc_account_sel.MassiveSearchType = 0
    Me.uc_account_sel.Name = "uc_account_sel"
    Me.uc_account_sel.SearchTrackDataAsInternal = True
    Me.uc_account_sel.ShowMassiveSearch = False
    Me.uc_account_sel.ShowVipClients = True
    Me.uc_account_sel.Size = New System.Drawing.Size(307, 134)
    Me.uc_account_sel.TabIndex = 1
    Me.uc_account_sel.Telephone = ""
    Me.uc_account_sel.TrackData = ""
    Me.uc_account_sel.Vip = False
    Me.uc_account_sel.WeddingDate = New Date(CType(0, Long))
    '
    'gb_report_type
    '
    Me.gb_report_type.Controls.Add(Me.opt_representative)
    Me.gb_report_type.Controls.Add(Me.opt_working_day)
    Me.gb_report_type.Controls.Add(Me.opt_customers)
    Me.gb_report_type.Location = New System.Drawing.Point(320, 174)
    Me.gb_report_type.Name = "gb_report_type"
    Me.gb_report_type.Size = New System.Drawing.Size(530, 55)
    Me.gb_report_type.TabIndex = 4
    Me.gb_report_type.TabStop = False
    Me.gb_report_type.Text = "xReportType"
    '
    'opt_representative
    '
    Me.opt_representative.AutoSize = True
    Me.opt_representative.Location = New System.Drawing.Point(349, 22)
    Me.opt_representative.Name = "opt_representative"
    Me.opt_representative.Size = New System.Drawing.Size(177, 17)
    Me.opt_representative.TabIndex = 2
    Me.opt_representative.TabStop = True
    Me.opt_representative.Text = "xJunket by Representative"
    Me.opt_representative.UseVisualStyleBackColor = True
    '
    'opt_working_day
    '
    Me.opt_working_day.AutoSize = True
    Me.opt_working_day.Location = New System.Drawing.Point(17, 22)
    Me.opt_working_day.Name = "opt_working_day"
    Me.opt_working_day.Size = New System.Drawing.Size(164, 17)
    Me.opt_working_day.TabIndex = 0
    Me.opt_working_day.TabStop = True
    Me.opt_working_day.Text = "xJunket by Working Day"
    Me.opt_working_day.UseVisualStyleBackColor = True
    '
    'opt_customers
    '
    Me.opt_customers.AutoSize = True
    Me.opt_customers.Location = New System.Drawing.Point(187, 22)
    Me.opt_customers.Name = "opt_customers"
    Me.opt_customers.Size = New System.Drawing.Size(153, 17)
    Me.opt_customers.TabIndex = 1
    Me.opt_customers.TabStop = True
    Me.opt_customers.Text = "xJunket by Customers"
    Me.opt_customers.UseVisualStyleBackColor = True
    '
    'gb_junkets
    '
    Me.gb_junkets.Controls.Add(Me.opt_all_junkets)
    Me.gb_junkets.Controls.Add(Me.opt_several_junkets)
    Me.gb_junkets.Controls.Add(Me.dg_filter_junkets)
    Me.gb_junkets.Location = New System.Drawing.Point(672, 9)
    Me.gb_junkets.Name = "gb_junkets"
    Me.gb_junkets.Size = New System.Drawing.Size(346, 159)
    Me.gb_junkets.TabIndex = 3
    Me.gb_junkets.TabStop = False
    Me.gb_junkets.Text = "xJunkets"
    '
    'opt_all_junkets
    '
    Me.opt_all_junkets.Location = New System.Drawing.Point(8, 45)
    Me.opt_all_junkets.Name = "opt_all_junkets"
    Me.opt_all_junkets.Size = New System.Drawing.Size(64, 24)
    Me.opt_all_junkets.TabIndex = 1
    Me.opt_all_junkets.Text = "xAll"
    '
    'opt_several_junkets
    '
    Me.opt_several_junkets.Location = New System.Drawing.Point(8, 17)
    Me.opt_several_junkets.Name = "opt_several_junkets"
    Me.opt_several_junkets.Size = New System.Drawing.Size(72, 24)
    Me.opt_several_junkets.TabIndex = 0
    Me.opt_several_junkets.Text = "xSeveral"
    '
    'dg_filter_junkets
    '
    Me.dg_filter_junkets.CurrentCol = -1
    Me.dg_filter_junkets.CurrentRow = -1
    Me.dg_filter_junkets.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_filter_junkets.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_filter_junkets.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_filter_junkets.Location = New System.Drawing.Point(86, 16)
    Me.dg_filter_junkets.Name = "dg_filter_junkets"
    Me.dg_filter_junkets.PanelRightVisible = False
    Me.dg_filter_junkets.Redraw = True
    Me.dg_filter_junkets.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_filter_junkets.Size = New System.Drawing.Size(249, 132)
    Me.dg_filter_junkets.Sortable = False
    Me.dg_filter_junkets.SortAscending = True
    Me.dg_filter_junkets.SortByCol = 0
    Me.dg_filter_junkets.TabIndex = 2
    Me.dg_filter_junkets.TabStop = False
    Me.dg_filter_junkets.ToolTipped = True
    Me.dg_filter_junkets.TopRow = -2
    Me.dg_filter_junkets.WordWrap = False
    '
    'gb_representatives
    '
    Me.gb_representatives.Controls.Add(Me.opt_all_representatives)
    Me.gb_representatives.Controls.Add(Me.opt_several_representatives)
    Me.gb_representatives.Controls.Add(Me.dg_filter_representatives)
    Me.gb_representatives.Location = New System.Drawing.Point(320, 9)
    Me.gb_representatives.Name = "gb_representatives"
    Me.gb_representatives.Size = New System.Drawing.Size(346, 159)
    Me.gb_representatives.TabIndex = 2
    Me.gb_representatives.TabStop = False
    Me.gb_representatives.Text = "xRepresentatives"
    '
    'opt_all_representatives
    '
    Me.opt_all_representatives.Location = New System.Drawing.Point(8, 45)
    Me.opt_all_representatives.Name = "opt_all_representatives"
    Me.opt_all_representatives.Size = New System.Drawing.Size(64, 24)
    Me.opt_all_representatives.TabIndex = 1
    Me.opt_all_representatives.Text = "xAll"
    '
    'opt_several_representatives
    '
    Me.opt_several_representatives.Location = New System.Drawing.Point(8, 17)
    Me.opt_several_representatives.Name = "opt_several_representatives"
    Me.opt_several_representatives.Size = New System.Drawing.Size(72, 24)
    Me.opt_several_representatives.TabIndex = 0
    Me.opt_several_representatives.Text = "xSeveral"
    '
    'dg_filter_representatives
    '
    Me.dg_filter_representatives.CurrentCol = -1
    Me.dg_filter_representatives.CurrentRow = -1
    Me.dg_filter_representatives.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_filter_representatives.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_filter_representatives.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_filter_representatives.Location = New System.Drawing.Point(86, 16)
    Me.dg_filter_representatives.Name = "dg_filter_representatives"
    Me.dg_filter_representatives.PanelRightVisible = False
    Me.dg_filter_representatives.Redraw = True
    Me.dg_filter_representatives.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_filter_representatives.Size = New System.Drawing.Size(249, 132)
    Me.dg_filter_representatives.Sortable = False
    Me.dg_filter_representatives.SortAscending = True
    Me.dg_filter_representatives.SortByCol = 0
    Me.dg_filter_representatives.TabIndex = 2
    Me.dg_filter_representatives.TabStop = False
    Me.dg_filter_representatives.ToolTipped = True
    Me.dg_filter_representatives.TopRow = -2
    Me.dg_filter_representatives.WordWrap = False
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 20)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(197, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 50)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(197, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.lbl_to_hour)
    Me.gb_date.Controls.Add(Me.lbl_from_hour)
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(8, 9)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(302, 87)
    Me.gb_date.TabIndex = 0
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'lbl_to_hour
    '
    Me.lbl_to_hour.AutoSize = True
    Me.lbl_to_hour.ForeColor = System.Drawing.Color.Blue
    Me.lbl_to_hour.Location = New System.Drawing.Point(228, 56)
    Me.lbl_to_hour.Name = "lbl_to_hour"
    Me.lbl_to_hour.Size = New System.Drawing.Size(40, 13)
    Me.lbl_to_hour.TabIndex = 13
    Me.lbl_to_hour.Text = "00:00"
    '
    'lbl_from_hour
    '
    Me.lbl_from_hour.AutoSize = True
    Me.lbl_from_hour.ForeColor = System.Drawing.Color.Blue
    Me.lbl_from_hour.Location = New System.Drawing.Point(228, 28)
    Me.lbl_from_hour.Name = "lbl_from_hour"
    Me.lbl_from_hour.Size = New System.Drawing.Size(40, 13)
    Me.lbl_from_hour.TabIndex = 12
    Me.lbl_from_hour.Text = "00:00"
    '
    'chk_show_without_activity
    '
    Me.chk_show_without_activity.AutoSize = True
    Me.chk_show_without_activity.Checked = True
    Me.chk_show_without_activity.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_show_without_activity.Location = New System.Drawing.Point(856, 211)
    Me.chk_show_without_activity.Name = "chk_show_without_activity"
    Me.chk_show_without_activity.Size = New System.Drawing.Size(154, 17)
    Me.chk_show_without_activity.TabIndex = 5
    Me.chk_show_without_activity.Text = "xShow without activity"
    Me.chk_show_without_activity.UseVisualStyleBackColor = True
    '
    'frm_junkets_report
    '
    Me.ClientSize = New System.Drawing.Size(1155, 692)
    Me.Name = "frm_junkets_report"
    Me.Text = "frm_junkets_report"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_report_type.ResumeLayout(False)
    Me.gb_report_type.PerformLayout()
    Me.gb_junkets.ResumeLayout(False)
    Me.gb_representatives.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gb_date.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_account_sel As GUI_Controls.uc_account_sel
  Friend WithEvents gb_report_type As System.Windows.Forms.GroupBox
  Friend WithEvents opt_representative As System.Windows.Forms.RadioButton
  Friend WithEvents opt_customers As System.Windows.Forms.RadioButton
  Friend WithEvents opt_working_day As System.Windows.Forms.RadioButton
  Friend WithEvents gb_junkets As System.Windows.Forms.GroupBox
  Friend WithEvents opt_all_junkets As System.Windows.Forms.RadioButton
  Friend WithEvents opt_several_junkets As System.Windows.Forms.RadioButton
  Friend WithEvents dg_filter_junkets As GUI_Controls.uc_grid
  Friend WithEvents gb_representatives As System.Windows.Forms.GroupBox
  Friend WithEvents opt_all_representatives As System.Windows.Forms.RadioButton
  Friend WithEvents opt_several_representatives As System.Windows.Forms.RadioButton
  Friend WithEvents dg_filter_representatives As GUI_Controls.uc_grid
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Protected WithEvents lbl_from_hour As System.Windows.Forms.Label
  Protected WithEvents lbl_to_hour As System.Windows.Forms.Label
  Friend WithEvents chk_show_without_activity As System.Windows.Forms.CheckBox

End Class
