﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_junkets_flyers_report.vb
' DESCRIPTION:   Report of flyers
' AUTHOR:        David Perelló
' CREATION DATE: 27-APR-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 27-APR-2017  DPC    Initial version
' 17-MAY-2017  DPC    WIGOS-1933 - Junkets - Changes request 3
' 21-JUL-2017  ETP    Bug 28899:[WIGOS-3890] Junkets - Gui is displaying wrong information about the "Reporte de Flyers"
' -------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_CommonMisc
Imports System.Text
Imports System.Data.SqlClient
Imports WSI.Common

Public Class frm_junkets_flyers_report

  Inherits frm_base_sel

#Region " Constants "

  ' Filter grid
  Private Const GRID_FILTER_COLUMN_ID As Integer = 0
  Private Const GRID_FILTER_COLUMN_CHECKED As Integer = 1
  Private Const GRID_FILTER_COLUMN_DESC As Integer = 2

  Private Const GRID_FILTER_HEADER = 0
  Private Const GRID_FILTER_DATA = 1

  Private Const GRID_FILTER_COLUMNS As Integer = 3
  Private Const GRID_FILTER_HEADER_ROWS As Integer = 1

  ' SQL Columns
  Private Const SQL_COLUMN_JUNKET_ID As Integer = 0
  Private Const SQL_COLUMN_JUNKET_NAME As Integer = 1
  Private Const SQL_COLUMN_FLYER_ID As Integer = 2
  Private Const SQL_COLUMN_FLYER_CODE As Integer = 3
  Private Const SQL_COLUMN_FLYER_NUM_CREATED As Integer = 4
  Private Const SQL_COLUMN_NUMS_USES As Integer = 5
  Private Const SQL_COLUMN_PCT As Integer = 6
  Private Const SQL_COLUMN_REPRESENTATIVE_NAME As Integer = 7
  Private Const SQL_COLUMN_JUNKET_CODE As Integer = 8

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_JUNKET_ID As Integer = 1
  Private Const GRID_COLUMN_JUNKET_NAME As Integer = 2
  Private Const GRID_COLUMN_JUNKET_CODE As Integer = 3
  Private Const GRID_COLUMN_REPRESENTATIVE_NAME As Integer = 4
  Private Const GRID_COLUMN_FLYER_ID As Integer = 5
  Private Const GRID_COLUMN_FLYER_CODE As Integer = 6
  Private Const GRID_COLUMN_FLYER_NUM_CREATED As Integer = 7
  Private Const GRID_COLUMN_NUM_USES As Integer = 8
  Private Const GRID_COLUMN_PCT As Integer = 9
  Private Const GRID_COLUMN_TYPE As Integer = 10

  Private Const GRID_COLUMNS As Integer = 11
  Private Const GRID_HEADER_COLUMNS As Integer = 2

  ' DATAGRID Width
  Private Const GRID_WIDTH_INDEX As Integer = 250
  Private Const GRID_WIDTH_JUNKET_ID As Integer = 0
  Private Const GRID_WIDTH_JUNKET_NAME As Integer = 3550
  Private Const GRID_WIDTH_JUNKET_CODE As Integer = 1300
  Private Const GRID_WIDTH_REPRESENTATIVE_NAME As Integer = 3550
  Private Const GRID_WIDTH_FLYER_ID As Integer = 0
  Private Const GRID_WIDTH_FLYER_CODE As Integer = 1300
  Private Const GRID_WIDTH_FLYER_NUM_CREATED As Integer = 1700
  Private Const GRID_WIDTH_NUM_USES As Integer = 1200
  Private Const GRID_WIDTH_PCT As Integer = 1500
  Private Const GRID_WIDTH_TYPE As Integer = 0

#End Region ' Constants

#Region " Enums "

  Private Enum Type
    JUNKET = 0
    FLYER = 1
  End Enum

#End Region ' Enums

#Region " Members "

  Private m_str_junket_name As String = String.Empty
  Private m_filter_representatives As String
  Private m_filter_junkets As String
  Private m_filter_code_flyer As String
  Private m_junket_row As Int16
  Private m_created As Int32
  Private m_uses As Int32

#End Region ' Members

#Region " Overrides "

  ''' <summary>
  ''' Sets the Form Id
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_JUNKET_REPORT_FLYERS

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ''' <summary>
  ''' Show the Form
  ''' </summary>
  ''' <param name="MdiParent"></param>
  ''' <remarks></remarks>
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent

    Me.Display(False)

  End Sub ' ShowForEdit

  ''' <summary>
  ''' Initialise all the form controls
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Customer base
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8269)

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False

    Me.gb_representatives.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8089)
    Me.opt_several_representatives.Text = GLB_NLS_GUI_INVOICING.GetString(223)
    Me.opt_all_representatives.Text = GLB_NLS_GUI_INVOICING.GetString(224)

    Me.gb_junkets.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7997)
    Me.opt_several_junkets.Text = GLB_NLS_GUI_INVOICING.GetString(223)
    Me.opt_all_junkets.Text = GLB_NLS_GUI_INVOICING.GetString(224)

    Me.gb_flyer.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8062)
    Me.ef_flyer_code.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5710)
    Me.ef_flyer_code.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 6)

    Call GUI_StyleSheetFilter(dg_filter_representatives)
    Call GUI_StyleSheetFilter(dg_filter_junkets)
    Call GUI_StyleSheet()

    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ''' <summary>
  ''' Calls the Creditline class to return the SQL Query
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Return CLASS_JUNKETS_FLYER.GetSQLFilter(GetGridFilter(dg_filter_representatives, opt_all_representatives.Checked), _
                                            GetGridFilter(dg_filter_junkets, opt_all_junkets.Checked), _
                                            Me.ef_flyer_code.Value)

  End Function 'GUI_FilterGetSqlQuery

  ''' <summary>
  ''' Validates the Filter selection where needed
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_FilterCheck() As Boolean

    Return True
  End Function ' GUI_FilterCheck

  ''' <summary>
  ''' Add row of junket
  ''' </summary>
  ''' <param name="DbRow"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As CLASS_DB_ROW) As Boolean

    If Not DbRow.IsNull(SQL_COLUMN_JUNKET_NAME) Then
      If m_str_junket_name <> DbRow.Value(SQL_COLUMN_JUNKET_NAME) Then
        AddRowJunket(DbRow)
        m_str_junket_name = DbRow.Value(SQL_COLUMN_JUNKET_NAME)
      End If
    End If

    Return True

  End Function ' GUI_CheckOutRowBeforeAdd

  ''' <summary>
  ''' Writes the Grid Row reformating where required and adding calculated cell values
  ''' </summary>
  ''' <param name="RowIndex"></param>
  ''' <param name="DbRow"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _div_pct As Decimal

    Me.Grid.Cell(RowIndex, GRID_COLUMN_JUNKET_ID).Value = String.Empty

    Me.Grid.Cell(RowIndex, GRID_COLUMN_JUNKET_NAME).Value = String.Empty

    If Not DbRow.IsNull(SQL_COLUMN_FLYER_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_FLYER_ID).Value = DbRow.Value(SQL_COLUMN_FLYER_ID)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_FLYER_CODE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_FLYER_CODE).Value = DbRow.Value(SQL_COLUMN_FLYER_CODE)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_FLYER_NUM_CREATED) Then

      Me.Grid.Cell(m_junket_row, GRID_COLUMN_FLYER_NUM_CREATED).Value += DbRow.Value(SQL_COLUMN_FLYER_NUM_CREATED)

      m_created += DbRow.Value(SQL_COLUMN_FLYER_NUM_CREATED)

      Me.Grid.Cell(RowIndex, GRID_COLUMN_FLYER_NUM_CREATED).Value = IIf(DbRow.Value(SQL_COLUMN_FLYER_NUM_CREATED) = 0, _
                                                                        String.Empty, _
                                                                        DbRow.Value(SQL_COLUMN_FLYER_NUM_CREATED))
    End If

    If Not DbRow.IsNull(SQL_COLUMN_NUMS_USES) Then

      Me.Grid.Cell(m_junket_row, GRID_COLUMN_NUM_USES).Value += DbRow.Value(SQL_COLUMN_NUMS_USES)

      m_uses += DbRow.Value(SQL_COLUMN_NUMS_USES)

      Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_USES).Value = IIf(DbRow.Value(SQL_COLUMN_NUMS_USES) = 0, _
                                                               String.Empty, _
                                                               DbRow.Value(SQL_COLUMN_NUMS_USES))
    End If

    If Not DbRow.IsNull(SQL_COLUMN_PCT) Then

      _div_pct = IIf(Me.Grid.Cell(m_junket_row, GRID_COLUMN_FLYER_NUM_CREATED).Value > 0, (Me.Grid.Cell(m_junket_row, GRID_COLUMN_NUM_USES).Value / Me.Grid.Cell(m_junket_row, GRID_COLUMN_FLYER_NUM_CREATED).Value) * 100, 0)

      Me.Grid.Cell(m_junket_row, GRID_COLUMN_PCT).Value = String.Format("{0}%", GUI_FormatNumber(_div_pct, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT))

      Me.Grid.Cell(RowIndex, GRID_COLUMN_PCT).Value = IIf(DbRow.Value(SQL_COLUMN_PCT) = 0, _
                                                          String.Empty, _
                                                          String.Format("{0}%", GUI_FormatNumber(DbRow.Value(SQL_COLUMN_PCT), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)))
    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE).Value = Int16.Parse(Type.FLYER)

    Return True

  End Function ' GUI_SetupRow

  ''' <summary>
  ''' After last row initialize the variable
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_AfterLastRow()

    AddRowTotal()
    m_str_junket_name = String.Empty

  End Sub ' GUI_AfterLastRow

  ''' <summary>
  ''' Finds the first selected grid item and calls the edit form
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_EditSelectedItem()

    Dim _idx_row As Int32

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    ' If no row is selected
    If _idx_row = Me.Grid.NumRows Then
      Return
    End If

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    If Me.Grid.Cell(_idx_row, GRID_COLUMN_TYPE).Value = Int16.Parse(Type.JUNKET) Then
      OpenFormJunket(_idx_row)
    Else
      OpenFormFlyer(_idx_row)
    End If

    Windows.Forms.Cursor.Current = Cursors.Default
    Call Me.Grid.Focus()

  End Sub ' GUI_EditSelectedItem

  ''' <summary>
  ''' Perform non-default actions for Button Clicks
  ''' </summary>
  ''' <param name="ButtonId"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case ENUM_BUTTON.BUTTON_SELECT

        Call GUI_EditSelectedItem()

      Case ENUM_BUTTON.BUTTON_FILTER_APPLY
        Call MyBase.GUI_ButtonClick(ButtonId)

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub 'GUI_ButtonClick

  ''' <summary>
  ''' Update filters for reports (Excel and print)
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_filter_representatives = String.Empty
    m_filter_junkets = String.Empty
    m_filter_code_flyer = String.Empty

    m_filter_representatives = GetDescListSelected(dg_filter_representatives, opt_all_representatives.Checked)
    If (String.IsNullOrEmpty(m_filter_representatives)) Then
      m_filter_representatives = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6774)
    End If

    m_filter_junkets = GetDescListSelected(dg_filter_junkets, opt_all_junkets.Checked)
    If (String.IsNullOrEmpty(m_filter_junkets)) Then
      m_filter_junkets = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6774)
    End If

    If Not String.IsNullOrEmpty(Me.ef_flyer_code.Value) Then
      m_filter_code_flyer = Me.ef_flyer_code.Value
    End If

  End Sub ' GUI_ReportUpdateFilters

  ''' <summary>
  ''' Sets the report Filter values for Excel and Print
  ''' </summary>
  ''' <param name="PrintData"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(8089), m_filter_representatives)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7997), m_filter_junkets)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(8207), m_filter_code_flyer)

  End Sub ' GUI_ReportFilter

  ''' <summary>
  ''' GUI_FilterReset
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub 'GUI_FilterReset

#End Region ' Overrides

#Region " Private methods "

  ''' <summary>
  ''' Sets Default Values for the Filters
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SetDefaultValues()

    Dim _ds_result As DataSet

    _ds_result = New DataSet

    opt_all_representatives.Checked = True
    opt_all_junkets.Checked = True

    If CLASS_JUNKETS_FLYER.GetDataForFilters(_ds_result) Then
      LoadFilterGrid(dg_filter_representatives, _ds_result.Tables("JUNKETS_REPRESENTATIVES"))
      LoadFilterGrid(dg_filter_junkets, _ds_result.Tables("JUNKETS"))
    End If

    ef_flyer_code.Value = String.Empty

    m_created = 0
    m_uses = 0

  End Sub ' SetDefaultValues

  ''' <summary>
  ''' Set the style of grids filter
  ''' </summary>
  ''' <param name="Grid"></param>
  ''' <remarks></remarks>
  Private Sub GUI_StyleSheetFilter(ByRef Grid As uc_grid)

    With Grid
      Call .Init(GRID_FILTER_COLUMNS, GRID_FILTER_HEADER_ROWS, True)
      .Counter(0).Visible = False
      .Sortable = False

      ' Hidden columns
      .Column(GRID_FILTER_COLUMN_ID).Header.Text = ""
      .Column(GRID_FILTER_COLUMN_ID).WidthFixed = 0

      ' GRID_COL_CHECKBOX
      .Column(GRID_FILTER_COLUMN_CHECKED).Header.Text = ""
      .Column(GRID_FILTER_COLUMN_CHECKED).WidthFixed = 400
      .Column(GRID_FILTER_COLUMN_CHECKED).Fixed = True
      .Column(GRID_FILTER_COLUMN_CHECKED).Editable = True
      .Column(GRID_FILTER_COLUMN_CHECKED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX

      ' GRID_COL_DESCRIPTION
      .Column(GRID_FILTER_COLUMN_DESC).Header.Text = ""
      .Column(GRID_FILTER_COLUMN_DESC).WidthFixed = 3000
      .Column(GRID_FILTER_COLUMN_DESC).Fixed = True
      .Column(GRID_FILTER_COLUMN_DESC).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With

  End Sub ' GUI_StyleSheetFilter

  ''' <summary>
  ''' Set the style of results grid
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GUI_StyleSheet()

    With Me.Grid

      ' Initialize the index of unfixed columns and return the number of columns
      Call .Init(GRID_COLUMNS, GRID_HEADER_COLUMNS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Enable sortable in Main Grid Columns
      .Sortable = False

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_INDEX).Header(1).Text = String.Empty
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Junket id
      .Column(GRID_COLUMN_JUNKET_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8014)
      .Column(GRID_COLUMN_JUNKET_ID).Header(1).Text = String.Empty
      .Column(GRID_COLUMN_JUNKET_ID).Width = GRID_WIDTH_JUNKET_ID

      ' Junket name
      .Column(GRID_COLUMN_JUNKET_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8014)
      .Column(GRID_COLUMN_JUNKET_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1186)
      .Column(GRID_COLUMN_JUNKET_NAME).Width = GRID_WIDTH_JUNKET_NAME
      .Column(GRID_COLUMN_JUNKET_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Junket code
      .Column(GRID_COLUMN_JUNKET_CODE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8014)
      .Column(GRID_COLUMN_JUNKET_CODE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5710)
      .Column(GRID_COLUMN_JUNKET_CODE).Width = GRID_WIDTH_JUNKET_CODE
      .Column(GRID_COLUMN_JUNKET_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Representative
      .Column(GRID_COLUMN_REPRESENTATIVE_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8014)
      .Column(GRID_COLUMN_REPRESENTATIVE_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8015)
      .Column(GRID_COLUMN_REPRESENTATIVE_NAME).Width = GRID_WIDTH_REPRESENTATIVE_NAME
      .Column(GRID_COLUMN_REPRESENTATIVE_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Flyer id
      .Column(GRID_COLUMN_FLYER_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8203)
      .Column(GRID_COLUMN_FLYER_ID).Header(1).Text = String.Empty
      .Column(GRID_COLUMN_FLYER_ID).Width = GRID_WIDTH_FLYER_ID

      ' Flyer
      .Column(GRID_COLUMN_FLYER_CODE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8203)
      .Column(GRID_COLUMN_FLYER_CODE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5710)
      .Column(GRID_COLUMN_FLYER_CODE).Width = GRID_WIDTH_FLYER_CODE
      .Column(GRID_COLUMN_FLYER_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Created
      .Column(GRID_COLUMN_FLYER_NUM_CREATED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8203)
      .Column(GRID_COLUMN_FLYER_NUM_CREATED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2636)
      .Column(GRID_COLUMN_FLYER_NUM_CREATED).Width = GRID_WIDTH_FLYER_NUM_CREATED
      .Column(GRID_COLUMN_FLYER_NUM_CREATED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Used
      .Column(GRID_COLUMN_NUM_USES).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8203)
      .Column(GRID_COLUMN_NUM_USES).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8204)
      .Column(GRID_COLUMN_NUM_USES).Width = GRID_WIDTH_NUM_USES
      .Column(GRID_COLUMN_NUM_USES).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Pct
      .Column(GRID_COLUMN_PCT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8203)
      .Column(GRID_COLUMN_PCT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(368)
      .Column(GRID_COLUMN_PCT).Width = GRID_WIDTH_PCT
      .Column(GRID_COLUMN_PCT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Type
      .Column(GRID_COLUMN_TYPE).Width = GRID_WIDTH_TYPE

    End With

  End Sub ' GUI_StyleSheet

  ''' <summary>
  ''' Set results on grid
  ''' </summary>
  ''' <param name="Grid"></param>
  ''' <param name="Results"></param>
  ''' <remarks></remarks>
  Private Sub LoadFilterGrid(ByRef Grid As uc_grid, ByRef Results As DataTable)

    Grid.Clear()

    For Each _row As DataRow In Results.Rows
      AddRowDataFilter(Grid, _row)
    Next

  End Sub ' LoadFilterGrid

  ''' <summary>
  ''' Add row in grid
  ''' </summary>
  ''' <param name="Grid"></param>
  ''' <param name="Row"></param>
  ''' <remarks></remarks>
  Private Sub AddRowDataFilter(ByRef Grid As uc_grid, ByRef Row As DataRow)

    Dim prev_redraw As Boolean

    prev_redraw = Me.Grid.Redraw
    Grid.Redraw = False

    Grid.AddRow()

    Grid.Redraw = False

    Grid.Cell(Grid.NumRows - 1, GRID_FILTER_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    Grid.Cell(Grid.NumRows - 1, GRID_FILTER_COLUMN_ID).Value = Row("ID")
    Grid.Cell(Grid.NumRows - 1, GRID_FILTER_COLUMN_DESC).Value = Row("DESCRIPTION")

    Grid.Redraw = prev_redraw

  End Sub ' AddRowDataFilter

  ''' <summary>
  ''' Add row junket (parent) in results grid
  ''' </summary>
  ''' <param name="JunketID"></param>
  ''' <param name="JunketName"></param>
  ''' <remarks></remarks>
  Private Sub AddRowJunket(ByVal DbRow As CLASS_DB_ROW)

    Dim prev_redraw As Boolean

    prev_redraw = Me.Grid.Redraw
    Me.Grid.Redraw = False

    Me.Grid.AddRow()

    Me.Grid.Redraw = False

    Grid.Cell(Grid.NumRows - 1, GRID_COLUMN_JUNKET_ID).Value = DbRow.Value(SQL_COLUMN_JUNKET_ID)
    Grid.Cell(Grid.NumRows - 1, GRID_COLUMN_JUNKET_NAME).Value = DbRow.Value(SQL_COLUMN_JUNKET_NAME)
    Grid.Cell(Grid.NumRows - 1, GRID_COLUMN_JUNKET_CODE).Value = DbRow.Value(SQL_COLUMN_JUNKET_CODE)
    Grid.Cell(Grid.NumRows - 1, GRID_COLUMN_REPRESENTATIVE_NAME).Value = DbRow.Value(SQL_COLUMN_REPRESENTATIVE_NAME)
    Grid.Cell(Grid.NumRows - 1, GRID_COLUMN_FLYER_ID).Value = String.Empty
    Grid.Cell(Grid.NumRows - 1, GRID_COLUMN_FLYER_CODE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1047)
    Grid.Cell(Grid.NumRows - 1, GRID_COLUMN_FLYER_NUM_CREATED).Value = 0
    Grid.Cell(Grid.NumRows - 1, GRID_COLUMN_NUM_USES).Value = 0
    Grid.Cell(Grid.NumRows - 1, GRID_COLUMN_PCT).Value = "0%"
    Grid.Cell(Grid.NumRows - 1, GRID_COLUMN_TYPE).Value = Int16.Parse(Type.JUNKET)

    Grid.Row(Grid.NumRows - 1).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

    Me.Grid.Redraw = prev_redraw

    m_junket_row = Grid.NumRows - 1

  End Sub ' AddRowJunket

  ''' <summary>
  ''' Add total row
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub AddRowTotal()

    Dim prev_redraw As Boolean

    prev_redraw = Me.Grid.Redraw
    Me.Grid.Redraw = False

    Me.Grid.AddRow()

    Me.Grid.Redraw = False

    Grid.Cell(Grid.NumRows - 1, GRID_COLUMN_JUNKET_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2271)

    Grid.Cell(Grid.NumRows - 1, GRID_COLUMN_FLYER_NUM_CREATED).Value = m_created
    Grid.Cell(Grid.NumRows - 1, GRID_COLUMN_NUM_USES).Value = m_uses
    Grid.Cell(Grid.NumRows - 1, GRID_COLUMN_PCT).Value = String.Format("{0}%", GUI_FormatNumber(IIf(m_created > 0, 100 * (m_uses / m_created), 0), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT))

    Grid.Row(Grid.NumRows - 1).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

    Me.Grid.Redraw = prev_redraw

    m_created = 0
    m_uses = 0

  End Sub ' AddRowTotal

  ''' <summary>
  ''' Get selecteds rows in filter grids
  ''' </summary>
  ''' <param name="Grid"></param>
  ''' <param name="GetAll"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetGridFilter(ByRef Grid As uc_grid, GetAll As Boolean) As String

    Dim _list_id As List(Of String)

    If GetAll Then Return String.Empty

    _list_id = New List(Of String)

    For i As Int32 = 0 To Grid.NumRows - 1
      If Grid.Cell(i, GRID_FILTER_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        _list_id.Add(Grid.Cell(i, GRID_FILTER_COLUMN_ID).Value.ToString)
      End If
    Next

    Return String.Join(", ", _list_id.ToArray())

  End Function ' GetGridFilter

  ''' <summary>
  ''' Open edit form junket
  ''' </summary>
  ''' <param name="IndexRow"></param>
  ''' <remarks></remarks>
  Private Sub OpenFormJunket(IndexRow As Int32)

    Dim _frm As frm_junkets_edit

    _frm = New frm_junkets_edit()

    _frm.ShowEditItem(Me.Grid.Cell(IndexRow, GRID_COLUMN_JUNKET_ID).Value.ToString)

  End Sub ' OpenFormJunket

  ''' <summary>
  ''' Open edit form flyer
  ''' </summary>
  ''' <param name="IndexRow"></param>
  ''' <remarks></remarks>
  Private Sub OpenFormFlyer(IndexRow As Int32)

    Dim _frm As frm_junkets_flyer_edit
    Dim _info_form_flyer As CLASS_JUNKETS_FLYER.InfoFormFlyer

    _frm = New frm_junkets_flyer_edit()

    _info_form_flyer = New CLASS_JUNKETS_FLYER.InfoFormFlyer
    _info_form_flyer.flyer_id = Me.Grid.Cell(IndexRow, GRID_COLUMN_FLYER_ID).Value.ToString

    _frm.InfoFormFlyer = _info_form_flyer

    _frm.ShowEditItem(Nothing)

  End Sub ' OpenFormFlyer

  ''' <summary>
  ''' Get filter Description list
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetDescListSelected(ByRef Grid As uc_grid, GetAll As Boolean) As String

    Dim _idx_row As Integer
    Dim _list As List(Of String)

    _list = New List(Of String)

    For _idx_row = 0 To Grid.NumRows - 1
      If Not GetAll Then
        If (Grid.Cell(_idx_row, GRID_FILTER_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED) Then
          _list.Add(Grid.Cell(_idx_row, GRID_FILTER_COLUMN_DESC).Value)
        End If
      End If
    Next

    Return String.Join(", ", _list.ToArray())

  End Function ' GetTypeDescListSelected

  ''' <summary>
  ''' CellDataChangedEvent
  ''' </summary>
  ''' <param name="Row"></param>
  ''' <param name="Column"></param>
  ''' <remarks></remarks>
  Private Sub dg_filter_representatives_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer) Handles dg_filter_representatives.CellDataChangedEvent

    ' Update radio buttons accordingly
    If Me.opt_several_representatives.Checked = False Then
      Me.opt_several_representatives.Checked = True
    End If

  End Sub ' dg_filter_representatives_CellDataChangedEvent

  ''' <summary>
  ''' CellDataChangedEvent
  ''' </summary>
  ''' <param name="Row"></param>
  ''' <param name="Column"></param>
  ''' <remarks></remarks>
  Private Sub dg_filter_junkets_representatives_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer) Handles dg_filter_junkets.CellDataChangedEvent

    ' Update radio buttons accordingly
    If Me.opt_several_junkets.Checked = False Then
      Me.opt_several_junkets.Checked = True
    End If

  End Sub ' dg_filter_junkets_representatives_CellDataChangedEvent

#End Region ' Private methods

#Region " Events "

  Private Sub opt_all_representatives_CheckedChanged(sender As Object, e As EventArgs) Handles opt_all_representatives.CheckedChanged
    Dim idx As Integer

    If opt_all_junkets.Checked Then
      For idx = 0 To dg_filter_representatives.NumRows - 1
        Me.dg_filter_representatives.Cell(idx, GRID_FILTER_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
      Next
    End If
  End Sub ' opt_all_representatives_CheckedChanged

  Private Sub opt_all_junkets_CheckedChanged(sender As Object, e As EventArgs) Handles opt_all_junkets.CheckedChanged
    Dim idx As Integer

    If opt_all_junkets.Checked Then
      For idx = 0 To dg_filter_junkets.NumRows - 1
        Me.dg_filter_junkets.Cell(idx, GRID_FILTER_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
      Next
    End If
  End Sub ' opt_all_junkets_CheckedChanged

#End Region ' Events

End Class
