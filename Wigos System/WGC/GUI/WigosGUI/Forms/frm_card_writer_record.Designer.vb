<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_card_writer_record
  Inherits GUI_Controls.frm_base

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.lbl_swipe_card = New GUI_Controls.uc_text_field
    Me.lbl_session_last_recorded_card = New GUI_Controls.uc_text_field
    Me.btn_stop = New GUI_Controls.uc_button
    Me.btn_exit = New GUI_Controls.uc_button
    Me.panel_data = New System.Windows.Forms.Panel
    Me.lbl_record_site = New GUI_Controls.uc_text_field
    Me.lbl_card_write_status = New GUI_Controls.uc_text_field
    Me.panel_buttons = New System.Windows.Forms.Panel
    Me.lbl_total_sequence = New GUI_Controls.uc_text_field
    Me.panel_data.SuspendLayout()
    Me.panel_buttons.SuspendLayout()
    Me.SuspendLayout()
    '
    'lbl_swipe_card
    '
    Me.lbl_swipe_card.Font = New System.Drawing.Font("Verdana", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_swipe_card.IsReadOnly = True
    Me.lbl_swipe_card.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_CENTER
    Me.lbl_swipe_card.LabelForeColor = System.Drawing.Color.DarkTurquoise
    Me.lbl_swipe_card.Location = New System.Drawing.Point(15, 49)
    Me.lbl_swipe_card.Name = "lbl_swipe_card"
    Me.lbl_swipe_card.Size = New System.Drawing.Size(263, 41)
    Me.lbl_swipe_card.SufixText = "Sufix Text"
    Me.lbl_swipe_card.SufixTextVisible = True
    Me.lbl_swipe_card.TabIndex = 2
    Me.lbl_swipe_card.TextVisible = False
    Me.lbl_swipe_card.TextWidth = 0
    Me.lbl_swipe_card.Value = "xSWIPE CARDx"
    '
    'lbl_session_last_recorded_card
    '
    Me.lbl_session_last_recorded_card.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_session_last_recorded_card.IsReadOnly = True
    Me.lbl_session_last_recorded_card.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_session_last_recorded_card.LabelForeColor = System.Drawing.Color.Blue
    Me.lbl_session_last_recorded_card.Location = New System.Drawing.Point(6, 146)
    Me.lbl_session_last_recorded_card.Name = "lbl_session_last_recorded_card"
    Me.lbl_session_last_recorded_card.Size = New System.Drawing.Size(233, 25)
    Me.lbl_session_last_recorded_card.SufixText = "Sufix Text"
    Me.lbl_session_last_recorded_card.SufixTextVisible = True
    Me.lbl_session_last_recorded_card.TabIndex = 3
    Me.lbl_session_last_recorded_card.TextWidth = 186
    Me.lbl_session_last_recorded_card.Value = "12345"
    '
    'btn_stop
    '
    Me.btn_stop.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_stop.Location = New System.Drawing.Point(5, 151)
    Me.btn_stop.Name = "btn_stop"
    Me.btn_stop.Size = New System.Drawing.Size(90, 30)
    Me.btn_stop.TabIndex = 4
    Me.btn_stop.ToolTipped = False
    Me.btn_stop.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_exit
    '
    Me.btn_exit.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_exit.Location = New System.Drawing.Point(5, 187)
    Me.btn_exit.Name = "btn_exit"
    Me.btn_exit.Size = New System.Drawing.Size(90, 30)
    Me.btn_exit.TabIndex = 5
    Me.btn_exit.ToolTipped = False
    Me.btn_exit.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.lbl_total_sequence)
    Me.panel_data.Controls.Add(Me.lbl_record_site)
    Me.panel_data.Controls.Add(Me.lbl_card_write_status)
    Me.panel_data.Controls.Add(Me.lbl_swipe_card)
    Me.panel_data.Controls.Add(Me.lbl_session_last_recorded_card)
    Me.panel_data.Dock = System.Windows.Forms.DockStyle.Fill
    Me.panel_data.Location = New System.Drawing.Point(4, 4)
    Me.panel_data.Name = "panel_data"
    Me.panel_data.Size = New System.Drawing.Size(391, 222)
    Me.panel_data.TabIndex = 6
    '
    'lbl_record_site
    '
    Me.lbl_record_site.IsReadOnly = True
    Me.lbl_record_site.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_record_site.LabelForeColor = System.Drawing.Color.Blue
    Me.lbl_record_site.Location = New System.Drawing.Point(7, 9)
    Me.lbl_record_site.Name = "lbl_record_site"
    Me.lbl_record_site.Size = New System.Drawing.Size(207, 24)
    Me.lbl_record_site.SufixText = "Sufix Text"
    Me.lbl_record_site.SufixTextVisible = True
    Me.lbl_record_site.TabIndex = 5
    Me.lbl_record_site.TextWidth = 140
    Me.lbl_record_site.Value = "xxxx"
    '
    'lbl_card_write_status
    '
    Me.lbl_card_write_status.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_card_write_status.IsReadOnly = True
    Me.lbl_card_write_status.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_CENTER
    Me.lbl_card_write_status.LabelForeColor = System.Drawing.Color.Blue
    Me.lbl_card_write_status.Location = New System.Drawing.Point(3, 97)
    Me.lbl_card_write_status.Name = "lbl_card_write_status"
    Me.lbl_card_write_status.Size = New System.Drawing.Size(289, 30)
    Me.lbl_card_write_status.SufixText = "Sufix Text"
    Me.lbl_card_write_status.SufixTextVisible = True
    Me.lbl_card_write_status.TabIndex = 4
    Me.lbl_card_write_status.TextVisible = False
    Me.lbl_card_write_status.TextWidth = 0
    Me.lbl_card_write_status.Value = "xCard Recorded Successfullyx"
    '
    'panel_buttons
    '
    Me.panel_buttons.Controls.Add(Me.btn_stop)
    Me.panel_buttons.Controls.Add(Me.btn_exit)
    Me.panel_buttons.Dock = System.Windows.Forms.DockStyle.Right
    Me.panel_buttons.Location = New System.Drawing.Point(297, 4)
    Me.panel_buttons.Name = "panel_buttons"
    Me.panel_buttons.Size = New System.Drawing.Size(98, 222)
    Me.panel_buttons.TabIndex = 7
    '
    'lbl_total_sequence
    '
    Me.lbl_total_sequence.IsReadOnly = True
    Me.lbl_total_sequence.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_total_sequence.LabelForeColor = System.Drawing.Color.Blue
    Me.lbl_total_sequence.Location = New System.Drawing.Point(5, 188)
    Me.lbl_total_sequence.Name = "lbl_total_sequence"
    Me.lbl_total_sequence.Size = New System.Drawing.Size(183, 25)
    Me.lbl_total_sequence.SufixText = "Sufix Text"
    Me.lbl_total_sequence.TabIndex = 6
    Me.lbl_total_sequence.TextWidth = 75
    Me.lbl_total_sequence.Value = "123456789"
    '
    'frm_card_writer_record
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(399, 230)
    Me.ControlBox = False
    Me.Controls.Add(Me.panel_buttons)
    Me.Controls.Add(Me.panel_data)
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Name = "frm_card_writer_record"
    Me.Text = "xCard Writer Record"
    Me.panel_data.ResumeLayout(False)
    Me.panel_buttons.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents lbl_swipe_card As GUI_Controls.uc_text_field
  Friend WithEvents lbl_session_last_recorded_card As GUI_Controls.uc_text_field
  Friend WithEvents btn_stop As GUI_Controls.uc_button
  Friend WithEvents btn_exit As GUI_Controls.uc_button
  Friend WithEvents panel_data As System.Windows.Forms.Panel
  Friend WithEvents panel_buttons As System.Windows.Forms.Panel
  Friend WithEvents lbl_card_write_status As GUI_Controls.uc_text_field
  Friend WithEvents lbl_record_site As GUI_Controls.uc_text_field
  Friend WithEvents lbl_total_sequence As GUI_Controls.uc_text_field
End Class
