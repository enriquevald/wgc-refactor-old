'-------------------------------------------------------------------
' Copyright © 2007-2009 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_customer_base
' DESCRIPTION:   Information various from the movements of account and session playing 
' AUTHOR:        Marcos Piedra
' CREATION DATE: 31-OCT-2010
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 31-OCT-2011  MPO    Initial version.
' 24-NOV-2011  MPO    Chash in = movements type 44 (prize coupon) + movements type 1 (chash in)
' 01-DEC-2011  JMM    Performance improvement
' 23-JAN-2012  MPO    Specific error: Catch the exception out of memory.
' 25-JAN-2012  MPO    Change order ac_holder_level.
' 26-JAN-2012  MPO    Fixed bug: #213 and #214
'                     #213: Netwin Total = (Play sessions) - (Site Jackpots)
'                     #214: If it doesn't have play sessions but it have account movements, It must show the register.
' 31-JAN-2012  MPO    #213: Change Netwin Total = PS_INITIAL_BALANCE + PS_CASH_IN - PS_FINAL_BALANCE
' 01-FEB-2012  MPO    Date of account creation is empty in some cases. No activiy --> Empty if don't has play sessions
'                     Column GRID_COLUMN_TOTAL_PLAYED_SEC align center
' 07-JUN-2012  JML    Add filter by days without activity.
' 21-AUG-2012  XCD    Added column Coin in redeemable.
' 24-AUG-2012  JAB    Control to show huge amounts of records & DoEvents run each second.
' 17-SEP-2012  JAB    Timeout Exception.
' 21-SEP-2012  JAB    Fixed bug. Error when assign a decimal in a int32 variable.
' 03-DEC-2012  RRB    Fixed bug. Error when maxium days is empty and add Played Count and Average Bet columns.
' 08-JAN-2013  RCI    Fixed bug #400
' 12-FEB-2013  JMM    uc_account_sel.ValidateFormat added on FilterCheck
' 14-FEB-2013  ANG    Call to mdl_account_for_report to get Account Holder data.
' 12-MAR-2013  LEM    Fixed Bug #638: Query very slow.
' 24-APR-2013  SMN    Hide Player Tracking data if exists an External Loyalty
' 03-MAY-2013  RBG    Fixed Bug #746: It makes no sense to work in this form with multiselection
' 10-JUN-2013  RBG    Fixed Bug #834: Force format in excel columns
' 17-JUN-2013  NMR    Fixed Bug #863: Don't show message when user stops operation
' 11-JUL-2013  ACM    Added "Print Player Data" buton.
' 21-AUG-2013  JMM    Fixed Defect WIG-141: Several filters errors
' 28-AUG-2013  QMP    Added account flag filter
' 02-SEP-2013  CCG    Added account massive search in the query
' 26-SEP-2013  CCG    Added Multitype massive search
' 04-NOV-2013  CCG    Fixed Defect WIG-370: Initialize flags grid with all unchecked and disable flags grid when there is one account in the filter
' 07-DEC-2014  JFC    Inproved filters, deactivating other filters when id account is informed.
' 06-MAY-2014  JBP    Added Vip client filter at reports.
' 27-JUN-2014  LEM & RCI    When card is recycled, the trackdata is shown empty and must be CardNumber.INVALID_TRACKDATA.
' 21-AUG-2014  DLL    Fixed Defect WIG-1194: missing creation date on grid
' 29-SEP-2014  SGB    Fixed Defect WIG-1303: Error when entering wrong card in search filter
' 10-DEC-2014  FJC    · Change NLS only in mode TITO 
'                         Recargas  ==> Cash Inputs
'                         Retiros   ==> Cash Outputs
'                     · Add concept "Bill In" (Billetes introducidos) in column Cash Inputs (before: Recargas), only in TITO mode.
' 28-JAN-2015  DCS    In TITO mode anonymous accounts don't displayed
' 11-FEB-2015  DCS    Fixed Defect WIG-2024: Error when execute query, changed subtotals data type to Decimal
' 30-APR-2015  JBC    Fixed Bug WIG-2261: Excel Date
' 21-AGO-2015  FJC    Product BackLog Item: 3702
' 06-OCT-2015  FAV    Fixed Bug 4927: A incorrect default filter when the user clean the Account textbox or Card 
' 13-OCT-2015  FAV    Fixed Bug 5143: Unhandled exception with Flags table with records
' 30-DIC-2015  SGB    Product Backlog Item 7889: Multiple Buckets
' 06-JAN-2016  SGB    Backlog Item 7910: Change column AC_POINTS to bucket 1.
' 25-FEB-2016  ETP    Bug 9565: Resetear el filtro Puntos
' 21-MAR-2016  JRC    PBI: 1792 Multiple Buckets: Desactivar Acumulación de Buckets si SPACE is enabled 
' 09-JUN-2016  ETP    Bug 14343:Buckets: Redondeo al transferir promociones redimibles y No redimibles
' 10-JUN-2016  FAV    Fixed Bug 14086: Enable or disable "Imprimir datos cliente" button using permissions.
' 19-DIC-2016  ETP    Fixed Bug 21608: Played column has to be visible in TITO mode.
' 23-DIC-2016  ETP    Fixed Bug 21982: Missing Netwin/ Cash out columns in TITO mode.
' 20-ENE-2017  DPC    Bug 8873:Ref. 16323: Base de Clientes en TITO no calcula el NetWin
' 01-FEB-2017  DPC    Bug 9252:Estadísticas Clientes. Base Clientes. En TITO ocultar columnas Netwin, CashOut, Pagado
' 22-MAY-2017  FAV    Bug 27490:Base de Clientes no muestra resultados (time-out)
' 08-JUN-2017  RGR    Fixed Bug 27408: Client Base - Date format in column 'Generated - Level Points' when exporting to excel
' 19-JUL-2017  ETP    Fixed Bug 27094: [Wigos-3242] Recarga no aparece en el reporte de Base de clientes
' 19-SEP-2017  RAB    Bug 29818: WIGOS-4990 [Ticket #8714] Antara - Customer base report: wrong export to excel
' 26-MAR-2018  RGR    Bug 32064:WIGOS-7079 [Ticket #11090] Customer Base Report 
' 28-MAY-2018  AGS    Bug 32802:WIGOS-12156 [Ticket #14469] Fallo – Resumen de Cuenta – Última Actividad - Versión V03.08.0001 
' 27-JUN-2018  AGS    Bug 33354:WIGOS-13061 GUI - Clients base: DB access error when selecting to show bucket data
' 27-JUN-2018  AGS    Bug 33374:WIGOS-13062 GUI - Clients base: terminal raffle results not accurate
' 09-JUL-2018  MS     Bug 33525:[WIGOS-13409] GUI - Clients Base: Buckets columns are empty
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Text
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports WSI.Common


Public Class frm_customer_base
  Inherits frm_base_sel

#Region " Constants "

  ' Index Columns of SQL
  Private Const SQL_COLUMN_HOLDERNAME As Integer = 0
  Private Const SQL_COLUMN_TRACKDATA As Integer = 1
  Private Const SQL_COLUMN_ACCOUNT As Integer = 2
  Private Const SQL_COLUMN_HOLDER_LEVEL As Integer = 3
  Private Const SQL_COLUMN_ACCOUNT_TYPE As Integer = 4
  Private Const SQL_INIT_COLUMNS_HOLDER_DATA As Integer = 5

  Private Const SQL_FINISH_COLUMNS_HOLDER_DATA = SQL_INIT_COLUMNS_HOLDER_DATA + mdl_account_for_report.SQL_NUM_COLUMNS_HOLDER_DATA

  Private Const SQL_COLUMN_COIN_IN As Integer = SQL_FINISH_COLUMNS_HOLDER_DATA
  Private Const SQL_COLUMN_PLAYED_COUNT As Integer = SQL_FINISH_COLUMNS_HOLDER_DATA + 1
  Private Const SQL_COLUMN_COIN_IN_REDEEM As Integer = SQL_FINISH_COLUMNS_HOLDER_DATA + 2
  Private Const SQL_COLUMN_CASH_IN As Integer = SQL_FINISH_COLUMNS_HOLDER_DATA + 3
  Private Const SQL_COLUMN_NETWIN As Integer = SQL_FINISH_COLUMNS_HOLDER_DATA + 4
  Private Const SQL_COLUMN_WITHDRAW As Integer = SQL_FINISH_COLUMNS_HOLDER_DATA + 5
  Private Const SQL_COLUMN_BALANCE As Integer = SQL_FINISH_COLUMNS_HOLDER_DATA + 6
  Private Const SQL_COLUMN_VISITS As Integer = SQL_FINISH_COLUMNS_HOLDER_DATA + 7
  Private Const SQL_COLUMN_ACCOUNT_CREATION As Integer = SQL_FINISH_COLUMNS_HOLDER_DATA + 8
  Private Const SQL_COLUMN_LAST_LEVEL_DATE As Integer = SQL_FINISH_COLUMNS_HOLDER_DATA + 9
  Private Const SQL_COLUMN_OLD_LEVEL As Integer = SQL_FINISH_COLUMNS_HOLDER_DATA + 10
  Private Const SQL_COLUMN_NEW_LEVEL As Integer = SQL_FINISH_COLUMNS_HOLDER_DATA + 11
  Private Const SQL_COLUMN_TOTAL_PLAYED_SEC As Integer = SQL_FINISH_COLUMNS_HOLDER_DATA + 12
  Private Const SQL_COLUMN_NO_ACTIVITY As Integer = SQL_FINISH_COLUMNS_HOLDER_DATA + 13
  Private Const SQL_COLUMN_POINTS_AWARDED As Integer = SQL_FINISH_COLUMNS_HOLDER_DATA + 14
  Private Const SQL_COLUMN_POINTS_DISCRETIONAL_LEVEL As Integer = SQL_FINISH_COLUMNS_HOLDER_DATA + 15
  Private Const SQL_COLUMN_POINTS_DISCRETIONAL_CHANGE As Integer = SQL_FINISH_COLUMNS_HOLDER_DATA + 16
  Private Const SQL_COLUMN_POINTS_PROMOTIONAL As Integer = SQL_FINISH_COLUMNS_HOLDER_DATA + 17
  Private Const SQL_COLUMN_POINTS_EXPIRED As Integer = SQL_FINISH_COLUMNS_HOLDER_DATA + 18
  Private Const SQL_COLUMN_LAST_VISIT_DATE As Integer = SQL_FINISH_COLUMNS_HOLDER_DATA + 19
  Private Const SQL_COLUMN_CURRENT_BALANCE As Integer = SQL_FINISH_COLUMNS_HOLDER_DATA + 20
  Private Const SQL_COLUMN_CURRENT_POINTS As Integer = SQL_FINISH_COLUMNS_HOLDER_DATA + 21
  Private Const SQL_COLUMN_CURRENT_STATE As Integer = SQL_FINISH_COLUMNS_HOLDER_DATA + 22
  Private Const SQL_COLUMN_CONTAIN_AM As Integer = SQL_FINISH_COLUMNS_HOLDER_DATA + 23
  Private Const SQL_COLUMN_CONTAIN_PS As Integer = SQL_FINISH_COLUMNS_HOLDER_DATA + 24
  Private Const SQL_COLUMN_LASTLEVELMOV As Integer = SQL_FINISH_COLUMNS_HOLDER_DATA + 25
  Private Const SQL_COLUMN_GT_VISITS As Integer = SQL_FINISH_COLUMNS_HOLDER_DATA + 26
  Private Const SQL_COLUMN_GT_LASTVISIT As Integer = SQL_FINISH_COLUMNS_HOLDER_DATA + 27
  Private Const SQL_COLUMN_GT_TOTALPLAYINGSECONDS As Integer = SQL_FINISH_COLUMNS_HOLDER_DATA + 28
  Private Const SQL_COLUMN_GT_NOACTIVITY As Integer = SQL_FINISH_COLUMNS_HOLDER_DATA + 29
  Private Const SQL_COLUMN_GT_TOTALPLAYED As Integer = SQL_FINISH_COLUMNS_HOLDER_DATA + 30
  Private Const SQL_COLUMN_GT_TOTALPLAYED_COUNT As Integer = SQL_FINISH_COLUMNS_HOLDER_DATA + 31
  Private Const SQL_COLUMN_GT_TOTALPLAYED_REDEEM As Integer = SQL_FINISH_COLUMNS_HOLDER_DATA + 32
  Private Const SQL_COLUMN_GT_NETWIN As Integer = SQL_FINISH_COLUMNS_HOLDER_DATA + 33
  Private Const SQL_COLUMN_GT_CONTAINS_PS As Integer = SQL_FINISH_COLUMNS_HOLDER_DATA + 34
  Private Const SQL_COLUMN_GT_CHIPS_IN As Integer = SQL_FINISH_COLUMNS_HOLDER_DATA + 35

  '' Index Columns Grids
  Private Const GRID_COLUMN_INDEX As Integer = 0
  ' Account
  Private Const GRID_COLUMN_LEVEL As Integer = 1
  Private Const GRID_COLUMN_ACCOUNT As Integer = 2
  Private Const GRID_COLUMN_TRACK_DATA As Integer = 3
  Private Const GRID_COLUMN_HOLDER_NAME As Integer = 4
  Private Const GRID_COLUMN_ACCOUNT_CREATION As Integer = 5

  ' ACCOUNT BALANCE
  Private Const GRID_COLUMN_BALANCE_AMOUNT As Integer = 6
  Private Const GRID_COLUMN_WITHDRAW_AMOUNT As Integer = 7

  ' TERMINAL PLAY SESSIONS
  Private Const GRID_COLUMN_COININ_AMOUNT As Integer = 8
  Private Const GRID_COLUMN_PLAYED_COUNT As Integer = 9
  Private Const GRID_COLUMN_AVERAGE_BET As Integer = 10
  Private Const GRID_COLUMN_COININ_REDEEM_AMOUNT As Integer = 11
  Private Const GRID_COLUMN_CASHIN_AMOUNT As Integer = 12
  Private Const GRID_COLUMN_NETWIN_AMOUNT As Integer = 13
  ' TERMINAL STATISTICS
  Private Const GRID_COLUMN_VISITS As Integer = 14
  Private Const GRID_COLUMN_LAST_VISIT_DATE As Integer = 15
  Private Const GRID_COLUMN_TOTAL_PLAYED_SEC As Integer = 16
  Private Const GRID_COLUMN_NO_ACTIVITY As Integer = 17
  ' GAMBLING TABLE SESSIONS
  Private Const GRID_COLUMN_GT_COININ_AMOUNT As Integer = 18
  Private Const GRID_COLUMN_GT_PLAYED_COUNT As Integer = 19
  Private Const GRID_COLUMN_GT_AVERAGE_BET As Integer = 20
  Private Const GRID_COLUMN_GT_COININ_REDEEM_AMOUNT As Integer = 21
  Private Const GRID_COLUMN_GT_CASHIN_AMOUNT As Integer = 22
  Private Const GRID_COLUMN_GT_NETWIN_AMOUNT As Integer = 23
  ' GAMBLING TABLE STATISTICS
  Private Const GRID_COLUMN_GT_VISITS As Integer = 24
  Private Const GRID_COLUMN_GT_LAST_VISIT_DATE As Integer = 25
  Private Const GRID_COLUMN_GT_TOTAL_PLAYED_SEC As Integer = 26
  Private Const GRID_COLUMN_GT_NO_ACTIVITY As Integer = 27

  ' Point Program
  Private Const GRID_COLUMN_LAST_LEVEL_DATE As Integer = 28
  Private Const GRID_COLUMN_OLD_LEVEL As Integer = 29
  Private Const GRID_COLUMN_NEW_LEVEL As Integer = 30
  Private Const GRID_COLUMN_POINTS_AWARDED As Integer = 31
  Private Const GRID_COLUMN_POINTS_DISCRETIONAL_LEVEL As Integer = 32
  Private Const GRID_COLUMN_POINTS_DISCRETIONAL_CHANGE As Integer = 33
  Private Const GRID_COLUMN_POINTS_PROMOTIONAL As Integer = 34
  Private Const GRID_COLUMN_POINTS_EXPIRED As Integer = 35

  ' Current situation of the account
  Private GRID_COLUMN_CURRENT_BALANCE As Integer
  Private GRID_COLUMN_CURRENT_POINTS As Integer
  Private GRID_COLUMN_CURRENT_STATE As Integer

  Private Const GRID_COLUMNS As Integer = 36
  Private Const GRID_COLUMNS_CURRENT_DATA As Integer = 3
  Private Const GRID_HEADER_ROWS As Integer = 2

  ' Width
  Private Const GRID_WIDTH_CARD_HOLDER_NAME As Integer = 2500
  Private Const GRID_WIDTH_CARD_TRACK_DATA As Integer = 2300
  Private Const GRID_WIDTH_ACCOUNT As Integer = 1150
  Private Const GRID_WIDTH_LEVEL As Integer = 1500
  Private Const GRID_WIDTH_AMOUNT As Integer = 1800
  Private Const GRID_WIDTH_NUMBER As Integer = 1000
  Private Const GRID_WIDTH_DATE_CREATION As Integer = 1300
  Private Const GRID_WIDTH_DATE As Integer = 1500
  Private Const GRID_WIDTH_SHORT_DATE As Integer = 1000
  Private Const GRID_WIDTH_TOTAL_PLAYED_SEC As Integer = 1900
  Private Const GRID_WIDTH_NO_ACTIVITY As Integer = 1500
  Private Const GRID_WIDTH_POINTS As Integer = 1600
  Private Const GRID_WIDTH_LEVEL_CHANGE As Integer = 1000

  ' Text format columns
  Private Const EXCEL_COLUMN_TELEPHONE_NUMBER_1 As Integer = 48
  Private Const EXCEL_COLUMN_TELEPHONE_NUMBER_2 As Integer = 49
  Private Const EXCEL_COLUMN_POSTAL_CODE As Integer = 41
  Private Const EXCEL_COLUMN_EXTERNAL_NUMER As Integer = 40
  Private Const EXCEL_COLUMN_DATE_REGISTER As Integer = 37
  Private Const EXCEL_COLUMN_BIRTHDATE As Integer = 4
  Private Const EXCEL_COLUMN_LAST_VISIT As Integer = 14
  Private Const EXCEL_COLUMN_GT_LAST_VISIT As Integer = 24
  Private Const EXCEL_COLUMN_DATE_LEVEL As Integer = 27
  Private Const EXCEL_COLUMN_WEDDING_DATE As Integer = 38

  ' QMP 02-JUL-2013: Account flag filter
  Private Const GRID_SELECT_FLAG_IDX As Integer = 0
  Private Const GRID_SELECT_FLAG_CHECKED As Integer = 1
  Private Const GRID_SELECT_FLAG_COLOR As Integer = 2
  Private Const GRID_SELECT_FLAG_NAME As Integer = 3
  Private Const GRID_SELECT_FLAG_COLUMNS As Integer = 4
  Private Const GRID_SELECT_FLAG_HEADER_ROWS As Integer = 0

  Private Const FORM_DB_MIN_VERSION As Short = 160

#End Region

#Region " Structures "

  Public Structure TYPE_TOTAL_COUNTERS
    Dim level As String
    Dim num_rows As Integer
    Dim coin_in_amount As Decimal
    Dim played_count_amount As Decimal
    Dim coin_in_redeem_amount As Decimal
    Dim cash_in_amount As Decimal
    Dim net_win_amount As Decimal
    Dim with_draw_amount As Decimal
    Dim balance_amount As Decimal
    Dim total_played As Decimal
    Dim visits As Decimal
    Dim points As Decimal
    Dim points_discretional_level As Decimal
    Dim points_discretional_change As Decimal
    Dim points_promotional As Decimal
    Dim points_expired As Decimal
    Dim total_balance As Decimal
    Dim total_points As Decimal
  End Structure

#End Region ' Structures

#Region "Enums"

  Private Enum TERMINAL_DRAW_FILTER As Integer
    NONE = 0
    ONLY_WITH = 1
    ONLY_WITHOUT = 2
  End Enum

#End Region


#Region " Member"

  Private m_date_from As String
  Private m_date_to As String
  Private m_date_creation_from As String
  Private m_date_creation_to As String
  Private m_current_filter As String
  Private m_holder_data_filter As String
  Private m_bucket_filter As String
  Private m_bucket_data As Boolean

  Private m_level As String
  Private m_gender As String
  Private m_only_anonymous As String

  Private m_account As String
  Private m_track_data As String
  Private m_holder As String
  Private m_holder_is_vip As String

  Private m_holder_data As Boolean
  Private m_current_data As Boolean
  Private m_change_col As Boolean

  Private m_no_activity As String
  Private m_no_activity_filter As String

  Private m_allow_totals As Boolean

  Private m_total_level As TYPE_TOTAL_COUNTERS
  Private m_total As TYPE_TOTAL_COUNTERS

  ' QMP 02-JUL-2013: Account flag filter
  Private m_flag As String

  Private m_cell_disabled_color As System.Drawing.Color = System.Drawing.Color.FromArgb(240, 240, 240)
  Private m_cell_text_disabled_color As System.Drawing.Color = System.Drawing.Color.FromArgb(109, 109, 109)
  Private m_cell_enable_color As System.Drawing.Color = Drawing.Color.White
  Private m_cell_text_enable_color As System.Drawing.Color = Drawing.Color.Black

  'FJC 10-DIC-2014: Changes in NLS columns when is TITO mode
  Private m_is_tito_mode As Boolean

  Private m_welcome_draw_filter As TERMINAL_DRAW_FILTER
#End Region

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CUSTOMER_BASE

    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Is TITO mode?
    m_is_tito_mode = TITO.Utils.IsTitoMode

    ' Customer base
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(551)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Text = GLB_NLS_GUI_STATISTICS.GetString(137)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(3)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(775) 'Print player data
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Type = uc_button.ENUM_BUTTON_TYPE.USER
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Height += 20

    ' Date time filter
    Me.uc_dsl.Init(GLB_NLS_GUI_PLAYER_TRACKING.Id(830), True)

    ' Gender
    Me.gb_gender.Text = GLB_NLS_GUI_INVOICING.GetString(403)
    Me.chk_gender_male.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(521)
    Me.chk_gender_female.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(522)

    ' Level
    ' TO DO : Should call mdl_account_for_report.GetLevelNames()
    Me.gb_level.Text = GLB_NLS_GUI_INVOICING.GetString(381)
    Me.chk_level_01.Text = GetCashierPlayerTrackingData("Level01.Name")
    If Me.chk_level_01.Text = "" Then
      Me.chk_level_01.Text = GLB_NLS_GUI_CONFIGURATION.GetString(289)
    End If
    Me.chk_level_02.Text = GetCashierPlayerTrackingData("Level02.Name")
    If chk_level_02.Text = "" Then
      Me.chk_level_02.Text = GLB_NLS_GUI_CONFIGURATION.GetString(290)
    End If
    Me.chk_level_03.Text = GetCashierPlayerTrackingData("Level03.Name")
    If Me.chk_level_03.Text = "" Then
      Me.chk_level_03.Text = GLB_NLS_GUI_CONFIGURATION.GetString(291)
    End If
    Me.chk_level_04.Text = GetCashierPlayerTrackingData("Level04.Name")
    If Me.chk_level_04.Text = "" Then
      Me.chk_level_04.Text = GLB_NLS_GUI_CONFIGURATION.GetString(332)
    End If

    ' Only anonymous
    Me.chk_only_anonymous.Text = GLB_NLS_GUI_CONFIGURATION.GetString(66)
    If WSI.Common.TITO.Utils.IsTitoMode() Then
      Me.chk_only_anonymous.Visible = False
    End If
    Me.chk_current.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(559)

    'CCG Show Massive Search
    Me.uc_account_sel1.ShowMassiveSearch = True

    ' Creation Date
    Me.gb_date.Text = GLB_NLS_GUI_INVOICING.GetString(468)
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)

    ' Show Holder Data
    Me.chk_holder_data.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(552)

    'show buckets
    Me.chk_show_bucket.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6970)


    If Not (GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode", 0) = 0) Then 'Buckets SPACE	
      Me.chk_show_bucket.Visible = False
    End If


    Me.m_change_col = False
    Me.m_holder_data = False
    Me.m_current_data = True
    Me.m_bucket_data = False

    ' No activity
    Me.gb_activity.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(831)

    Me.rb_all.Text = GLB_NLS_GUI_STATISTICS.GetString(206) ' 206 todos 218 todas
    Me.rb_activity.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(843)

    Me.pgb_maximum.Text = ""
    Me.rb_without_maximum.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(844)
    Me.rb_maximum.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(845)

    Me.ef_days_activity.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(846)
    Me.ef_days_activity.Enabled = True

    Call Me.ef_days_activity.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)

    ' QMP 02-JUL-2013: Account flag filter
    Me.gb_flag.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286)
    Me.btn_select_all_flags.Text = GLB_NLS_GUI_ALARMS.GetString(452)
    Me.btn_select_none_flags.Text = GLB_NLS_GUI_ALARMS.GetString(453)

    Me.chk_only_with_welcome_draw.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8706)
    Me.chk_only_without_welcome_draw.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8707)

    Call InitializeGridSelectFlags()

    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ' PURPOSE : Manage permissions.
  '
  '  PARAMS :
  '     - INPUT :
  '           - AndPerm TYPE_PERMISSIONS
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As GUI_Controls.CLASS_GUI_USER.TYPE_PERMISSIONS)

    MyBase.GUI_Permissions(AndPerm)

    ' GroupBox Permissions
    Me.chk_holder_data.Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_SHOW_HOLDER_DATA).Read


  End Sub ' GUI_Permissions

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _str_sql As System.Text.StringBuilder
    Dim _activity_filter As Boolean
    Dim _no_activity_days As Int32
    Dim _flags As String
    Dim _ps_terminal_draw_type As String

    _activity_filter = Me.rb_activity.Checked And Not Me.uc_account_sel1.DisabledHolder
    _ps_terminal_draw_type = PlaySessionType.TERMINAL_DRAW.GetHashCode.ToString
    _str_sql = New System.Text.StringBuilder()

    ' CCG 02-SEP-2013: Massive Search
    If Not String.IsNullOrEmpty(Me.uc_account_sel1.MassiveSearchNumbers) Then
      _str_sql.AppendLine(Me.uc_account_sel1.CreateAndInsertAccountData())
    End If

    _str_sql.AppendLine(" DECLARE @INI_DATE AS DATETIME  ")
    _str_sql.AppendLine(" DECLARE @FIN_DATE AS DATETIME  ")
    _str_sql.AppendLine(" SET @INI_DATE =  " & GUI_FormatDateDB(GetFromDate))
    _str_sql.AppendLine(" SET @FIN_DATE =  " & GUI_FormatDateDB(GetToTime))

    If _activity_filter Then
      If Me.rb_maximum.Checked Then
        _no_activity_days = Me.ef_days_activity.Value
      ElseIf Me.rb_without_maximum.Checked Then
        If Me.uc_dsl.ToDateSelected Then
          _no_activity_days = DateDiff(DateInterval.Day, Me.uc_dsl.FromDate, Me.uc_dsl.ToDate)
        Else
          _no_activity_days = DateDiff(DateInterval.Day, Me.uc_dsl.FromDate, WSI.Common.WGDB.Now)
        End If
      End If
      _str_sql.AppendLine(" DECLARE @pDaysNoActivity AS Int ")
      _str_sql.AppendLine(" SET @pDaysNoActivity = " & _no_activity_days)
    End If

    _str_sql.AppendLine(" ")
    _str_sql.AppendLine(" SELECT   SUBSTRING(GP_SUBJECT_KEY, 6,2) AS ACCOUNT_LEVEL ")
    _str_sql.AppendLine("        , GP_KEY_VALUE AS LEVEL_NAME ")
    _str_sql.AppendLine("   INTO   #GENERAL_PARAMS_LEVELS ")
    _str_sql.AppendLine("   FROM   GENERAL_PARAMS ")
    _str_sql.AppendLine("  WHERE   GP_GROUP_KEY = 'PlayerTracking'  ")
    _str_sql.AppendLine("    AND   GP_SUBJECT_KEY like 'Level%.Name'  ")
    _str_sql.AppendLine(" ")

    _str_sql.AppendLine("     SELECT   PS_ACCOUNT_ID                                                                                                              ")
    _str_sql.AppendLine("            , COUNT(DISTINCT  DATEDIFF(HOUR, @INI_DATE, PS_FINISHED) / 24 )             VISITS                                          ")
    _str_sql.AppendLine("            , DATEADD(DAY, DATEDIFF(HOUR, @INI_DATE, MAX(PS_FINISHED)) / 24, @INI_DATE) LASTVISIT                                       ")
    _str_sql.AppendLine("            , SUM(DATEDIFF (SECOND, PS_STARTED, ISNULL(PS_FINISHED, GETDATE())))        TOTALPLAYINGSECONDS                             ")
    _str_sql.AppendLine("            , (DATEDIFF (HOUR, DATEADD(DAY, DATEDIFF(HOUR, @INI_DATE, MAX(PS_FINISHED)) / 24, @INI_DATE), @FIN_DATE) / 24)-1 NOACTIVITY ")
    _str_sql.AppendLine("            , SUM(PS_PLAYED_AMOUNT)                                                   TOTALPLAYED                                       ")
    _str_sql.AppendLine("            , SUM(PS_PLAYED_COUNT)                                                    TOTALPLAYED_COUNT                                 ")
    _str_sql.AppendLine("            , SUM(PS_REDEEMABLE_PLAYED)                                               TOTALPLAYED_REDEEM                                ")
    _str_sql.AppendLine("            , SUM(PS_PLAYED_AMOUNT - PS_WON_AMOUNT)                                   NETWINPLAYEDWON                                   ")
    _str_sql.AppendLine("            , SUM(PS_INITIAL_BALANCE + PS_CASH_IN - PS_FINAL_BALANCE)                 NETWININOUT                                       ")
    _str_sql.AppendLine("            , 1                                                                       CONTAINS_PS                                       ")
    _str_sql.AppendLine("            , SUM(PS_CASH_IN)                                                         BILLIN                                            ")
    _str_sql.AppendLine("       INTO   #PLAY_SESSIONS                                                                                                            ")
    _str_sql.AppendLine("       FROM   PLAY_SESSIONS        WITH(INDEX(IX_PS_FINISHED_STATUS) )                                                                  ")
    _str_sql.AppendLine("      WHERE   PS_FINISHED >= @INI_DATE                                                                                                  ")
    _str_sql.AppendLine("        AND   PS_FINISHED <  @FIN_DATE                                                                                                  ")
    _str_sql.AppendLine("        AND   PS_STATUS   <> 0                                                                                                          ")  ' 0 is PlaySession.Opened
    If Me.m_welcome_draw_filter = TERMINAL_DRAW_FILTER.ONLY_WITH Then
      _str_sql.AppendLine("  			AND  PS_TYPE =  " + _ps_terminal_draw_type)
    ElseIf Me.m_welcome_draw_filter = TERMINAL_DRAW_FILTER.ONLY_WITHOUT Then
      _str_sql.AppendLine("  			AND  PS_TYPE <>  " + _ps_terminal_draw_type)
    End If
    _str_sql.AppendLine("      GROUP   BY PS_ACCOUNT_ID                                                                                                          ")
    _str_sql.AppendLine(" ")

    _str_sql.AppendLine("     SELECT  GTPS_ACCOUNT_ID                                                                               GT_ACCOUNT_ID                   ")
    _str_sql.AppendLine("           , COUNT(DISTINCT  DATEDIFF(HOUR, @INI_DATE, gtps_finished) / 24 )                               GT_VISITS                       ")
    _str_sql.AppendLine("           , DATEADD(DAY, DATEDIFF(HOUR, @INI_DATE, MAX(gtps_finished)) / 24, @INI_DATE)                   GT_LASTVISIT                    ")
    _str_sql.AppendLine("           , SUM(DATEDIFF (SECOND, gtps_started, ISNULL(gtps_finished, GETDATE())))                        GT_TOTALPLAYINGSECONDS          ")
    _str_sql.AppendLine("           , (DATEDIFF(HOUR, DATEADD(DAY, DATEDIFF(HOUR, @INI_DATE, MAX(gtps_finished)) / 24, @INI_DATE), @FIN_DATE) / 24)-1 GT_NOACTIVITY ")
    _str_sql.AppendLine("           , SUM(gtps_played_amount)                                                                       GT_TOTALPLAYED                  ")
    _str_sql.AppendLine("           , SUM(gtps_plays)                                                                               GT_TOTALPLAYED_COUNT            ")
    _str_sql.AppendLine("           , SUM(gtps_played_amount)                                                                       GT_TOTALPLAYED_REDEEM           ")
    _str_sql.AppendLine("           , SUM(gtps_netwin)                                                                              GT_NETWIN                       ")
    _str_sql.AppendLine("           , 1                                                                                             GT_CONTAINS_PS                  ")
    _str_sql.AppendLine("           , SUM(gtps_chips_in)                                                                            GT_CHIPS_IN                     ")
    _str_sql.AppendLine("       INTO   #GT_PLAY_SESSIONS                                                                                                            ")
    _str_sql.AppendLine("       FROM   gt_play_sessions                                                                                                             ")
    _str_sql.AppendLine("      WHERE   gtps_finished >= @INI_DATE                                                                                                   ")
    _str_sql.AppendLine("        AND   gtps_finished <  @FIN_DATE                                                                                                   ")
    _str_sql.AppendLine("        AND   gtps_status   <> 0                                                                                                           ")
    _str_sql.AppendLine("      GROUP   BY gtps_account_id                                                                                                           ")
    _str_sql.AppendLine(" ")

    _str_sql.AppendLine("      SELECT   AM_ACCOUNT_ID, AM_TYPE                                                                                                   ")
    _str_sql.AppendLine("             , SUM(AM_ADD_AMOUNT) as AM_ADD_AMOUNT                                                                                      ")
    _str_sql.AppendLine("             , SUM(AM_SUB_AMOUNT) as [AM_SUB_AMOUNT]                                                                                    ")
    _str_sql.AppendLine("             , MAX(CASE WHEN AM_TYPE IN (43, 62) THEN AM_DATETIME ELSE NULL END) [AM_DATETIME]                                          ")
    _str_sql.AppendLine("             , MAX(CASE WHEN AM_TYPE IN (43, 62) THEN AM_MOVEMENT_ID ELSE NULL END) [AM_MOVEMENT_ID]                                    ")
    _str_sql.AppendLine("        INTO   #ACCOUNT_MOVEMENTS                                                                                                       ")
    _str_sql.AppendLine("        FROM   ACCOUNT_MOVEMENTS          WITH (INDEX (IX_TYPE_DATE_ACCOUNT) )                                                          ")
    _str_sql.AppendLine("       WHERE   AM_TYPE IN (1,2,3,36,40,43,44,62,63,64,65, 50,60,61,68,71,72,73, 1102, 1202, 1302, 1402, 1502,1101, 1201,1301,1401,1501) ")
    _str_sql.AppendLine("         AND   AM_DATETIME >= @INI_DATE                                                                                                 ")
    _str_sql.AppendLine("         AND   AM_DATETIME <  @FIN_DATE                                                                                                 ")
    _str_sql.AppendLine("    GROUP BY   AM_ACCOUNT_ID, AM_TYPE                                                                                                   ")
    _str_sql.AppendLine(" ")

    _str_sql.AppendLine(" SELECT   HOLDERNAME                           ") '0
    _str_sql.AppendLine("        , [TRACKDATA]                          ") '1
    _str_sql.AppendLine("        , ACC_ID                  AS [ACCOUNT] ") '2
    _str_sql.AppendLine("        , [HOLDERLEVEL]                        ") '3
    _str_sql.AppendLine("        , [ACCOUNTTYPE]                        ") '4

    '' ADD COLUMN ALIAS FOR HOLDER DATA
    _str_sql.AppendLine(mdl_account_for_report.AccountFieldsSql(False, True))

    _str_sql.AppendLine("       , TOTALPLAYED              AS [COIN IN] ")
    _str_sql.AppendLine("       , TOTALPLAYED_COUNT        AS [PLAYED COUNT] ")
    _str_sql.AppendLine("       , TOTALPLAYED_REDEEM       AS [COIN IN REDEEM] ")

    If m_is_tito_mode Then
      ' Add Bill in (billetes introducidos) to 'Cash In' concept
      _str_sql.AppendLine("       , ISNULL([PRIZE COUPON],0) + ISNULL([DEPOSIT A],0)  + ISNULL([BILLIN],0)  AS [CASH IN]")
      _str_sql.AppendLine("       , (ISNULL([PRIZE COUPON],0) + ISNULL([DEPOSIT A],0)  + ISNULL([BILLIN],0)) - ( ISNULL([REFUNDS],0) + ISNULL([PRIZES],0) - ISNULL([TAX],0) + ISNULL([TAXRETURNING],0) + ISNULL([DECIMALROUNDING],0) - ISNULL([SERVICECHARGE],0)) AS [NETWIN] ")
    Else
      _str_sql.AppendLine("       , ISNULL([PRIZE COUPON],0) + ISNULL([DEPOSIT A],0)                        AS [CASH IN]")
      _str_sql.AppendLine("       , ISNULL(NETWININOUT,0)     AS [NETWIN] ")
    End If

    _str_sql.AppendLine("       , ISNULL([REFUNDS],0) + ISNULL([PRIZES],0) - ISNULL([TAX],0) + ISNULL([TAXRETURNING],0) + ISNULL([DECIMALROUNDING],0) - ISNULL([SERVICECHARGE],0) AS [WITHDRAW] ") '21
    _str_sql.AppendLine("       , ISNULL([REFUNDS],0) + ISNULL([PRIZES],0)           AS [CBALANCE] ")
    _str_sql.AppendLine("       , [VISITS]                   ")
    _str_sql.AppendLine("       , [CREATED]                  ")
    _str_sql.AppendLine("       , [LASTLEVEL]                ")
    _str_sql.AppendLine("       , [OLD_LEVEL]                ")
    _str_sql.AppendLine("       , [NEW_LEVEL]                ")
    _str_sql.AppendLine("       , [TOTALPLAYINGSECONDS]      ")
    _str_sql.AppendLine("       , [NOACTIVITY]               ")
    _str_sql.AppendLine("       , [POINTSAWARDED]            ")
    _str_sql.AppendLine("       , [POINTSDISCRETIONALLEVEL]  ")
    _str_sql.AppendLine("       , [POINTSDISCRETIONALCHANGE] ")
    _str_sql.AppendLine("       , [POINTSPROMOTION]          ")
    _str_sql.AppendLine("       , [POINTSEXPIRED]            ")
    _str_sql.AppendLine("       , [LASTVISIT]                ")
    _str_sql.AppendLine("       , [BALANCE]                  ")
    _str_sql.AppendLine("       , [POINTS]                   ")
    _str_sql.AppendLine("       , [BLOCKED]                  ")
    _str_sql.AppendLine("       , CONTAINS_AM                ")
    _str_sql.AppendLine("       , CONTAINS_PS                ")
    _str_sql.AppendLine("       , LASTLEVELMOV               ")
    _str_sql.AppendLine("       , GT_VISITS                  ")
    _str_sql.AppendLine("       , GT_LASTVISIT               ")
    _str_sql.AppendLine("       , GT_TOTALPLAYINGSECONDS     ")
    _str_sql.AppendLine("       , GT_NOACTIVITY              ")
    _str_sql.AppendLine("       , GT_TOTALPLAYED             ")
    _str_sql.AppendLine("       , GT_TOTALPLAYED_COUNT       ")
    _str_sql.AppendLine("       , GT_TOTALPLAYED_REDEEM      ")
    _str_sql.AppendLine("       , GT_NETWIN                  ")
    _str_sql.AppendLine("       , GT_CONTAINS_PS             ")
    _str_sql.AppendLine("       , GT_CHIPS_IN                ")
    _str_sql.AppendLine(" ")

    _str_sql.AppendLine(mdl_bucket_for_report.BucketsFieldsSql(m_bucket_data))

    _str_sql.AppendLine(" INTO #CUSTOMER_TABLE ")

    _str_sql.AppendLine("  FROM ( ")

    _str_sql.AppendLine("    SELECT   AC_ACCOUNT_ID [ACC_ID]       ")
    _str_sql.AppendLine("           , AC_HOLDER_NAME [HOLDERNAME]     ")
    _str_sql.AppendLine("           , AC_TRACK_DATA [TRACKDATA]       ")
    _str_sql.AppendLine("           , AC_HOLDER_LEVEL                 ")
    _str_sql.AppendLine("           , AC_TYPE [ACCOUNTTYPE]           ")
    _str_sql.AppendLine("           , ISNULL((SELECT   LEVEL_NAME     ")
    _str_sql.AppendLine("                       FROM   #GENERAL_PARAMS_LEVELS  ")
    _str_sql.AppendLine("                      WHERE   ACCOUNT_LEVEL = RIGHT('0' + CAST(AC_HOLDER_LEVEL AS NVARCHAR), 2) ")
    _str_sql.AppendLine("                   ), '') [HOLDERLEVEL]     ")
    _str_sql.AppendLine("           , AC_CREATED [CREATED]            ")
    _str_sql.AppendLine("           , AC_BALANCE [BALANCE]            ")

    If m_bucket_data Then
      _str_sql.AppendLine("           , ISNULL([1], 0) AS POINTS                     ")
    Else
      _str_sql.AppendLine("           , DBO.GETBUCKETVALUE(" & Buckets.BucketType.RedemptionPoints & ",AC_ACCOUNT_ID) AS POINTS ")
    End If

    _str_sql.AppendLine("           , AC_BLOCKED [BLOCKED]            ")

    ' Add account Info
    _str_sql.AppendLine(mdl_account_for_report.AccountFieldsSql())

    _str_sql.AppendLine(mdl_bucket_for_report.BucketsFieldsSql(m_bucket_data))

    _str_sql.AppendLine("    FROM ACCOUNTS                ")

    _str_sql.AppendLine(mdl_bucket_for_report.BucketsJoinSql(m_bucket_data))

    _str_sql.AppendLine("    WHERE AC_CREATED < @FIN_DATE ")

    _str_sql.AppendLine(GetSqlWhereAccount())

    _str_sql.AppendLine("     GROUP BY AC_ACCOUNT_ID,AC_HOLDER_NAME,AC_TRACK_DATA,AC_HOLDER_LEVEL ")
    _str_sql.AppendLine("        ,AC_HOLDER_ZIP,AC_CREATED,AC_BALANCE,AC_BLOCKED, AC_TYPE ")

    '' ADD HOLDER DATA TO GROUP BY
    _str_sql.AppendLine(mdl_account_for_report.AccountFieldsSql(True, False))

    _str_sql.AppendLine(mdl_bucket_for_report.BucketsFieldsSql(m_bucket_data))

    _str_sql.AppendLine("    ) ACCOUNT_INFO LEFT JOIN ")

    _str_sql.AppendLine("       ( ")
    _str_sql.AppendLine("      SELECT   PS_ACCOUNT_ID                                     ")
    _str_sql.AppendLine("             , VISITS                                            ")
    _str_sql.AppendLine("             , LASTVISIT                                         ")
    _str_sql.AppendLine("             , TOTALPLAYINGSECONDS                               ")
    _str_sql.AppendLine("             , NOACTIVITY                                        ")
    _str_sql.AppendLine("             , TOTALPLAYED                                       ")
    _str_sql.AppendLine("             , TOTALPLAYED_COUNT                                 ")
    _str_sql.AppendLine("             , TOTALPLAYED_REDEEM                                ")
    _str_sql.AppendLine("             , NETWINPLAYEDWON                                   ")
    _str_sql.AppendLine("             , NETWININOUT                                       ")
    _str_sql.AppendLine("             , CONTAINS_PS                                       ")
    If m_is_tito_mode Then
      _str_sql.AppendLine("             , BILLIN                                          ")
    End If
    _str_sql.AppendLine("        FROM #PLAY_SESSIONS                                      ")
    _str_sql.AppendLine(" ")

    _str_sql.AppendLine("      ) PLAY_SES ON PLAY_SES.PS_ACCOUNT_ID = ACCOUNT_INFO.ACC_ID ")

    _str_sql.AppendLine("   LEFT JOIN   ( ")
    _str_sql.AppendLine("      SELECT   GT_ACCOUNT_ID                                     ")
    _str_sql.AppendLine("             , GT_VISITS                                         ")
    _str_sql.AppendLine("             , GT_LASTVISIT                                      ")
    _str_sql.AppendLine("             , GT_TOTALPLAYINGSECONDS                            ")
    _str_sql.AppendLine("             , GT_NOACTIVITY                                     ")
    _str_sql.AppendLine("             , GT_TOTALPLAYED                                    ")
    _str_sql.AppendLine("             , GT_TOTALPLAYED_COUNT                              ")
    _str_sql.AppendLine("             , GT_TOTALPLAYED_REDEEM                             ")
    _str_sql.AppendLine("             , GT_NETWIN                                         ")
    _str_sql.AppendLine("             , GT_CONTAINS_PS                                    ")
    _str_sql.AppendLine("             , GT_CHIPS_IN                                       ")
    _str_sql.AppendLine("        FROM #GT_PLAY_SESSIONS                                   ")
    _str_sql.AppendLine(" ")

    _str_sql.AppendLine("      ) GT_PLAY_SES ON GT_PLAY_SES.GT_ACCOUNT_ID = ACCOUNT_INFO.ACC_ID ")

    _str_sql.AppendLine("   LEFT JOIN   ( ")
    _str_sql.AppendLine("        SELECT   AM_ACCOUNT_ID                                                                 ")
    _str_sql.AppendLine("               , SUM(CASE AM_TYPE WHEN  1 THEN AM_ADD_AMOUNT ELSE 0 END) [DEPOSIT A]           ")
    _str_sql.AppendLine("               , SUM(CASE AM_TYPE WHEN 44 THEN AM_ADD_AMOUNT ELSE 0 END) [PRIZE COUPON]        ")
    _str_sql.AppendLine("               , SUM(CASE AM_TYPE WHEN  3 THEN AM_SUB_AMOUNT ELSE 0 END) [REFUNDS]             ")
    _str_sql.AppendLine("               , SUM(CASE AM_TYPE WHEN  2 THEN AM_SUB_AMOUNT ELSE 0 END) [PRIZES]              ")
    _str_sql.AppendLine("               , SUM(CASE AM_TYPE WHEN  2 THEN AM_ADD_AMOUNT ELSE 0 END) [TAX]                 ")
    _str_sql.AppendLine("               , MAX(CASE WHEN AM_TYPE IN (43, 62) THEN AM_DATETIME ELSE NULL END) [LASTLEVEL] ")

    ' we tried to replace these 2 lines with DBO.GETBUCKETVALUE 8 and 9, but those numbers are not date filtered and this query is date filtered
    _str_sql.AppendLine("               , SUM(CASE WHEN AM_TYPE IN (36, 1102, 1202) THEN AM_ADD_AMOUNT-AM_SUB_AMOUNT ELSE 0 END) [POINTSAWARDED]                         ")
    _str_sql.AppendLine("               , SUM(CASE WHEN AM_TYPE IN (68,72,73, 1302, 1402, 1502) THEN AM_ADD_AMOUNT - AM_SUB_AMOUNT ELSE 0 END) [POINTSDISCRETIONALLEVEL] ")

    _str_sql.AppendLine("               , SUM(CASE WHEN AM_TYPE IN (50,71,1301,1401,1501) THEN AM_ADD_AMOUNT - AM_SUB_AMOUNT ELSE 0 END) [POINTSDISCRETIONALCHANGE]      ")
    _str_sql.AppendLine("               , SUM(CASE WHEN AM_TYPE IN (60,61, 1101) THEN AM_ADD_AMOUNT-AM_SUB_AMOUNT ELSE 0 END) [POINTSPROMOTION]  ")
    _str_sql.AppendLine("               , SUM(CASE WHEN AM_TYPE IN (40,1201) THEN AM_SUB_AMOUNT ELSE 0 END) [POINTSEXPIRED]        ")
    _str_sql.AppendLine("               , SUM(CASE AM_TYPE WHEN 63 THEN AM_ADD_AMOUNT ELSE 0 END) [TAXRETURNING]    ")
    _str_sql.AppendLine("               , SUM(CASE AM_TYPE WHEN 64 THEN AM_ADD_AMOUNT ELSE 0 END) [DECIMALROUNDING] ")
    _str_sql.AppendLine("               , SUM(CASE AM_TYPE WHEN 65 THEN AM_SUB_AMOUNT ELSE 0 END) [SERVICECHARGE]   ")
    _str_sql.AppendLine("               , 1 CONTAINS_AM                            ")
    _str_sql.AppendLine("               , cast(null as nvarchar(20))   [OLD_LEVEL] ")
    _str_sql.AppendLine("               , cast(null as nvarchar(20))   [NEW_LEVEL] ")
    _str_sql.AppendLine("               , MAX(CASE WHEN AM_TYPE IN (43, 62) THEN AM_MOVEMENT_ID ELSE NULL END) [LASTLEVELMOV] ")
    _str_sql.AppendLine("          FROM   #ACCOUNT_MOVEMENTS ")
    _str_sql.AppendLine("      GROUP BY   AM_ACCOUNT_ID ")

    _str_sql.AppendLine("        ) ACCOUNT_MOV ON ACCOUNT_MOV.AM_ACCOUNT_ID = ACCOUNT_INFO.ACC_ID ")

    ' QMP 02-JUL-2013: Account flag filter
    _flags = ""

    If String.IsNullOrEmpty(Me.uc_account_sel1.Account) Then
      _flags = String.Join(",", Me.GetSelectedFlagsId.ToArray())
    End If

    If Not String.IsNullOrEmpty(_flags) Then
      _str_sql.AppendLine(" INNER JOIN ( SELECT   DISTINCT(AF_ACCOUNT_ID) AS AF_ACC_ID                  ")
      _str_sql.AppendLine("                FROM   ACCOUNT_FLAGS                                         ")
      _str_sql.AppendLine("               WHERE   AF_FLAG_ID IN (" & _flags & ")                        ")
      _str_sql.AppendLine("                 AND   AF_STATUS   = " & ACCOUNT_FLAG_STATUS.ACTIVE)
      _str_sql.AppendLine("            ) ACCOUNT_FLAGS ON ACCOUNT_FLAGS.AF_ACC_ID = ACCOUNT_INFO.ACC_ID ")
    End If
    _str_sql.AppendLine(" ")

    'CCG 02-SET-2013: Add the account numbers selected in the massive search form.
    If Not String.IsNullOrEmpty(Me.uc_account_sel1.MassiveSearchNumbers) Then
      If Me.uc_account_sel1.MassiveSearchType = GUI_Controls.uc_account_sel.ENUM_MASSIVE_SEARCH_TYPE.ID_ACCOUNT Then
        _str_sql.AppendLine(Me.uc_account_sel1.GetInnerJoinAccountMassiveSearch("ACCOUNT_INFO.ACC_ID"))
      Else
        _str_sql.AppendLine(Me.uc_account_sel1.GetInnerJoinAccountMassiveSearch("TRACKDATA"))
      End If
    End If

    _str_sql.AppendLine(" ")
    _str_sql.AppendLine("     UPDATE   #CUSTOMER_TABLE ")
    _str_sql.AppendLine("        SET   #CUSTOMER_TABLE.[OLD_LEVEL] = ISNULL((SELECT   LEVEL_NAME       ")
    _str_sql.AppendLine("                                                      FROM   #GENERAL_PARAMS_LEVELS     ")
    _str_sql.AppendLine("                                                     WHERE   ACCOUNT_LEVEL = RIGHT('0' + CAST(CAST(AM_SUB_AMOUNT AS BIGINT) AS NVARCHAR), 2)), '') ")
    _str_sql.AppendLine("            , #CUSTOMER_TABLE.[NEW_LEVEL] = ISNULL((SELECT   LEVEL_NAME ")
    _str_sql.AppendLine("                                                      FROM   #GENERAL_PARAMS_LEVELS     ")
    _str_sql.AppendLine("                                                     WHERE   ACCOUNT_LEVEL = RIGHT('0' + CAST(CAST(AM_ADD_AMOUNT AS BIGINT) AS NVARCHAR), 2)), '') ")
    _str_sql.AppendLine("       FROM   #CUSTOMER_TABLE AS #CUSTOMER_TABLE ")
    _str_sql.AppendLine(" INNER JOIN   ACCOUNT_MOVEMENTS AS Table_B ON #CUSTOMER_TABLE.LASTLEVELMOV = Table_B.am_movement_id ")
    _str_sql.AppendLine("      WHERE   ISNULL(#CUSTOMER_TABLE.LASTLEVELMOV, 0) > 0 ")

    _str_sql.AppendLine(" ")
    _str_sql.AppendLine("     CREATE   NONCLUSTERED INDEX IX_NOACTIVITY ON #CUSTOMER_TABLE(NOACTIVITY, [CASH IN]) ")
    _str_sql.AppendLine(" ")
    _str_sql.AppendLine("     SELECT * FROM #CUSTOMER_TABLE             ")

    If _activity_filter Then

      If Not rb_without_maximum.Checked Then
        _str_sql.AppendLine("      WHERE   ([NOACTIVITY] < @pDaysNoActivity) ")

      Else
        _str_sql.AppendLine("      WHERE   ( [CASH IN] > 0 )                 ")

      End If

    End If

    _str_sql.AppendLine("   ORDER BY   HOLDERLEVEL DESC , ACCOUNT       ")

    _str_sql.AppendLine(" ")
    _str_sql.AppendLine(" DROP TABLE   #CUSTOMER_TABLE                  ")
    _str_sql.AppendLine(" DROP TABLE   #GENERAL_PARAMS_LEVELS ")
    _str_sql.AppendLine(" DROP TABLE   #ACCOUNT_MOVEMENTS")
    _str_sql.AppendLine(" DROP TABLE   #PLAY_SESSIONS")
    _str_sql.AppendLine(" DROP TABLE   #GT_PLAY_SESSIONS")

    ' CCG 02-SEP-2013: Delete the temporary table
    If Not String.IsNullOrEmpty(Me.uc_account_sel1.MassiveSearchNumbers) Then
      _str_sql.AppendLine(Me.uc_account_sel1.DropTableAccountMassiveSearch())
    End If

    Return _str_sql.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE: Get the defined Query Type
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_QUERY_TYPE

  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM
  End Function ' GUI_GetQueryType

  ' PURPOSE: Define the ExecuteQuery customized
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     -

  Protected Overrides Sub GUI_ExecuteQueryCustom()
    Dim _str_sql As String
    Dim _db_trx As WSI.Common.DB_TRX
    Dim _sql_da As SqlDataAdapter
    Dim _dataset As DataSet = Nothing
    Dim _datatable As DataTable
    Dim _datarow As System.Data.DataRow
    Dim _n As Int32

    Try

      ' Obtain the SqlQuery
      _str_sql = GUI_FilterGetSqlQuery()

      If String.IsNullOrEmpty(_str_sql) Then
        Return
      End If

      _db_trx = New WSI.Common.DB_TRX()
      _sql_da = New SqlDataAdapter(New SqlCommand(_str_sql))
      _sql_da.SelectCommand.CommandTimeout = 5 * 60 '5 min
      _dataset = New DataSet()

      _db_trx.Fill(_sql_da, _dataset)

      _datatable = _dataset.Tables(0)

      Me.Grid.Redraw = False

      Call GUI_BeforeFirstRow()

      ' JAB 24-AUG-2012: Control to show huge amounts of records.
      If Not ShowHugeNumberOfRows(_datatable.Rows.Count) Then
        MyBase.m_user_canceled_data_shows = True
        Exit Sub
      End If

      For _n = 0 To _datatable.Rows.Count - 1

        ' JAB 24-AUG-2012: DoEvents run each second.
        If Not GUI_DoEvents(Me.Grid) Then
          Exit Sub
        End If

        _datarow = _datatable.Rows(_n Mod _datatable.Rows.Count)

        Call GUI_CheckOutRowBeforeAdd(New CLASS_DB_ROW(_datarow))

        Me.Grid.AddRow()
        Call GUI_SetupRow(Me.Grid.NumRows - 1, New CLASS_DB_ROW(_datarow))

      Next _n

      Call GUI_AfterLastRow()

      _sql_da.Dispose()
      _db_trx.Dispose()

    Catch _ex_sql As SqlException
      ' JAB 17-SEP-2012: Timeout Exception.
      If _ex_sql.Number = -2 Then
        Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(144), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Else
        Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      End If

    Catch ex As OutOfMemoryException
      ' Specific error: Out of memory catch the exception.
      Me.Grid.Redraw = True
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(150), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "Customer base", _
                            "GUI_ExecuteQueryCustom", _
                            ex.Message)
    Catch ex As Exception
      ' An error has occurred in the query execution
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "Customer base", _
                            "GUI_ExecuteQueryCustom", _
                            ex.Message)

    Finally
      If Not Me.IsDisposed Then
        Me.Grid.Redraw = True
      End If

      If _dataset IsNot Nothing Then
        Call _dataset.Clear()
        Call _dataset.Dispose()
        _dataset = Nothing
      End If
    End Try

  End Sub

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _track_data As String
    Dim _double As Double
    Dim _time As TimeSpan
    Dim _number As Object
    Dim _average_bet As Double

    ' Holder Name
    If Not DbRow.IsNull(SQL_COLUMN_HOLDERNAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_NAME).Value = DbRow.Value(SQL_COLUMN_HOLDERNAME)
    End If

    ' Card Track Data
    _track_data = ""
    If Not DbRow.IsNull(SQL_COLUMN_TRACKDATA) Then
      CardNumber.VisibleTrackData(_track_data, DbRow.Value(SQL_COLUMN_TRACKDATA), MAGNETIC_CARD_TYPES.CARD_TYPE_PLAYER, _
                                    CType(DbRow.Value(SQL_COLUMN_ACCOUNT_TYPE), AccountType))
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TRACK_DATA).Value = _track_data
    End If

    ' Card Account
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT).Value = DbRow.Value(SQL_COLUMN_ACCOUNT).ToString

    ' Level
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_LEVEL) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_LEVEL).Value = DbRow.Value(SQL_COLUMN_HOLDER_LEVEL)
    End If

    If m_holder_data Then

      Call mdl_account_for_report.SetupRowHolderData(Me.Grid, DbRow, RowIndex, GRID_COLUMNS, SQL_INIT_COLUMNS_HOLDER_DATA)

    End If

    If m_bucket_data Then

      mdl_bucket_for_report.SetupRowBucketData(Me.Grid, DbRow, RowIndex, InitColumnGrid(), SQL_COLUMN_GT_CONTAINS_PS + 2)

    End If

    If Me.m_current_data Then

      Me.Grid.Cell(RowIndex, GRID_COLUMN_CURRENT_BALANCE).Value = GUI_FormatCurrency(Math.Truncate(DbRow.Value(SQL_COLUMN_CURRENT_BALANCE) * 100) / 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      Me.Grid.Cell(RowIndex, GRID_COLUMN_CURRENT_POINTS).Value = GUI_FormatNumber(Math.Truncate(DbRow.Value(SQL_COLUMN_CURRENT_POINTS)), 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      If DbRow.Value(SQL_COLUMN_CURRENT_STATE) = 0 Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CURRENT_STATE).Value = ""
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CURRENT_STATE).Value = GLB_NLS_GUI_INVOICING.GetString(419)
      End If

    End If

    ' TERMINAL PLAY SESSIONS

    _double = GetValNum(DbRow, SQL_COLUMN_COIN_IN)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_COININ_AMOUNT).Value = GUI_FormatCurrency(_double, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' PlayedCount
    If DbRow.IsNull(SQL_COLUMN_PLAYED_COUNT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED_COUNT).Value = ""
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED_COUNT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_PLAYED_COUNT), _
                                                                                ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
    End If

    ' Average bet
    If Not DbRow.IsNull(SQL_COLUMN_PLAYED_COUNT) And Not DbRow.IsNull(SQL_COLUMN_COIN_IN) Then
      If DbRow.Value(SQL_COLUMN_PLAYED_COUNT) = 0 Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_AVERAGE_BET).Value = ""
      Else
        _average_bet = Math.Round(DbRow.Value(SQL_COLUMN_COIN_IN) / DbRow.Value(SQL_COLUMN_PLAYED_COUNT), 2, MidpointRounding.AwayFromZero)

        If _average_bet = 0 Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_AVERAGE_BET).Value = ""
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_AVERAGE_BET).Value = GUI_FormatCurrency(_average_bet, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If
      End If
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_AVERAGE_BET).Value = ""
    End If

    _double = GetValNum(DbRow, SQL_COLUMN_COIN_IN_REDEEM)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_COININ_REDEEM_AMOUNT).Value = GUI_FormatCurrency(_double, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    _double = GetValNum(DbRow, SQL_COLUMN_CASH_IN)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CASHIN_AMOUNT).Value = GUI_FormatCurrency(_double, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    _double = GetValNum(DbRow, SQL_COLUMN_NETWIN)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN_AMOUNT).Value = GUI_FormatCurrency(_double, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)


    ' GAMBLING TABLES PLAY SESSIONS
    _double = GetValNum(DbRow, SQL_COLUMN_GT_TOTALPLAYED)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_GT_COININ_AMOUNT).Value = GUI_FormatCurrency(_double, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' PlayedCount
    If DbRow.IsNull(SQL_COLUMN_GT_TOTALPLAYED_COUNT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GT_PLAYED_COUNT).Value = ""
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GT_PLAYED_COUNT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_GT_TOTALPLAYED), _
                                                                                ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
    End If

    ' Average bet
    If Not DbRow.IsNull(SQL_COLUMN_GT_TOTALPLAYED_COUNT) And Not DbRow.IsNull(SQL_COLUMN_GT_TOTALPLAYED) Then
      If DbRow.Value(SQL_COLUMN_GT_TOTALPLAYED_COUNT) = 0 Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_GT_AVERAGE_BET).Value = ""
      Else
        _average_bet = Math.Round(DbRow.Value(SQL_COLUMN_GT_TOTALPLAYED) / DbRow.Value(SQL_COLUMN_GT_TOTALPLAYED_COUNT), 2, MidpointRounding.AwayFromZero)

        If _average_bet = 0 Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_GT_AVERAGE_BET).Value = ""
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_GT_AVERAGE_BET).Value = GUI_FormatCurrency(_average_bet, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If
      End If
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GT_AVERAGE_BET).Value = ""
    End If

    _double = GetValNum(DbRow, SQL_COLUMN_GT_TOTALPLAYED_REDEEM)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_GT_COININ_REDEEM_AMOUNT).Value = GUI_FormatCurrency(_double, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    _double = GetValNum(DbRow, SQL_COLUMN_GT_TOTALPLAYED)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_GT_CASHIN_AMOUNT).Value = GUI_FormatCurrency(_double, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    _double = GetValNum(DbRow, SQL_COLUMN_GT_NETWIN)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_GT_NETWIN_AMOUNT).Value = GUI_FormatCurrency(_double, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    _double = GetValNum(DbRow, SQL_COLUMN_WITHDRAW)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_WITHDRAW_AMOUNT).Value = GUI_FormatCurrency(_double, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    _double = GetValNum(DbRow, SQL_COLUMN_BALANCE)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_BALANCE_AMOUNT).Value = GUI_FormatCurrency(_double, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    _number = GetValNum(DbRow, SQL_COLUMN_VISITS)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_VISITS).Value = GUI_FormatNumber(_number, 0)

    ' Date of last visit TERMINALS

    If Not DbRow.IsNull(SQL_COLUMN_LAST_VISIT_DATE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_LAST_VISIT_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_LAST_VISIT_DATE), _
                                                                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                 ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    End If

    _number = GetValNum(DbRow, SQL_COLUMN_GT_VISITS)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_GT_VISITS).Value = GUI_FormatNumber(_number, 0)

    ' Date of last visit GAMBLING TABLES

    If Not DbRow.IsNull(SQL_COLUMN_GT_LASTVISIT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GT_LAST_VISIT_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_GT_LASTVISIT), _
                                                                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                 ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    End If

    ' Date of put the last level
    If Not DbRow.IsNull(SQL_COLUMN_LAST_LEVEL_DATE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_LAST_LEVEL_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_LAST_LEVEL_DATE), _
                                                                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                 ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    End If

    ' Old level
    If Not DbRow.IsNull(SQL_COLUMN_OLD_LEVEL) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_OLD_LEVEL).Value = DbRow.Value(SQL_COLUMN_OLD_LEVEL)
    End If
    ' New level
    If Not DbRow.IsNull(SQL_COLUMN_NEW_LEVEL) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NEW_LEVEL).Value = DbRow.Value(SQL_COLUMN_NEW_LEVEL)
    End If

    ' Date of creation of the account
    If Not DbRow.IsNull(SQL_COLUMN_ACCOUNT_CREATION) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_CREATION).Value = GUI_FormatDate(WSI.Common.Misc.Opening(GUI_FormatDate(DbRow.Value(SQL_COLUMN_ACCOUNT_CREATION), _
                                                                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                 ENUM_FORMAT_TIME.FORMAT_HHMMSS)), _
                                                                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                 ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    End If


    'TERMINAL PLAY SESSION
    ' Played session / visits
    Dim _number2 As Object
    _number = GetValNum(DbRow, SQL_COLUMN_VISITS)
    _number2 = GetValNum(DbRow, SQL_COLUMN_TOTAL_PLAYED_SEC)
    If _number > 0 And _number2 > 0 Then

      _time = New TimeSpan(0, 0, _number2 / _number)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_PLAYED_SEC).Value = TimeSpanToString(_time)

    Else

      Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_PLAYED_SEC).Value = ""

    End If

    ' Days no activity if contain play sessions
    If Not DbRow.IsNull(SQL_COLUMN_CONTAIN_PS) AndAlso DbRow.Value(SQL_COLUMN_CONTAIN_PS) = 1 Then
      _number = GetValNum(DbRow, SQL_COLUMN_NO_ACTIVITY)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NO_ACTIVITY).Value = GUI_FormatNumber(_number, 0)
    Else

      If Not DbRow.IsNull(GRID_COLUMN_NO_ACTIVITY) AndAlso GetValNum(DbRow, SQL_COLUMN_NO_ACTIVITY) > 0 Then
        _number = GetValNum(DbRow, SQL_COLUMN_NO_ACTIVITY)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_NO_ACTIVITY).Value = GUI_FormatNumber(_number, 0)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_NO_ACTIVITY).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1048)
      End If

    End If

    'GAMBLING TABLES PLAY SESSIONS
    ' Played session / visits
    _number = GetValNum(DbRow, SQL_COLUMN_GT_VISITS)
    _number2 = GetValNum(DbRow, SQL_COLUMN_GT_TOTALPLAYINGSECONDS)
    If _number > 0 And _number2 > 0 Then
      _time = New TimeSpan(0, 0, _number2 / _number)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GT_TOTAL_PLAYED_SEC).Value = TimeSpanToString(_time)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GT_TOTAL_PLAYED_SEC).Value = ""
    End If

    ' Days no activity 
    If Not DbRow.IsNull(SQL_COLUMN_GT_CONTAINS_PS) AndAlso DbRow.Value(SQL_COLUMN_GT_CONTAINS_PS) = 1 Then
      _number = GetValNum(DbRow, SQL_COLUMN_GT_NOACTIVITY)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GT_NO_ACTIVITY).Value = GUI_FormatNumber(_number, 0)
    Else

      If Not DbRow.IsNull(GRID_COLUMN_GT_NO_ACTIVITY) AndAlso GetValNum(DbRow, SQL_COLUMN_GT_NOACTIVITY) > 0 Then
        _number = GetValNum(DbRow, SQL_COLUMN_GT_NOACTIVITY)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_GT_NO_ACTIVITY).Value = GUI_FormatNumber(_number, 0)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_GT_NO_ACTIVITY).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1048)
      End If

    End If

    ' Points awarded
    If Not DbRow.IsNull(SQL_COLUMN_POINTS_AWARDED) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS_AWARDED).Value = GUI_FormatNumber(Math.Truncate(DbRow.Value(SQL_COLUMN_POINTS_AWARDED)), 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS_AWARDED).Value = 0
    End If

    ' Discretional points level
    If Not DbRow.IsNull(SQL_COLUMN_POINTS_DISCRETIONAL_LEVEL) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS_DISCRETIONAL_LEVEL).Value = GUI_FormatNumber(Math.Truncate(DbRow.Value(SQL_COLUMN_POINTS_DISCRETIONAL_LEVEL)), 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS_DISCRETIONAL_LEVEL).Value = 0
    End If

    ' Discretional points change
    If Not DbRow.IsNull(SQL_COLUMN_POINTS_DISCRETIONAL_CHANGE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS_DISCRETIONAL_CHANGE).Value = GUI_FormatNumber(Math.Truncate(DbRow.Value(SQL_COLUMN_POINTS_DISCRETIONAL_CHANGE)), 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS_DISCRETIONAL_CHANGE).Value = 0
    End If

    ' Promotional points
    If Not DbRow.IsNull(SQL_COLUMN_POINTS_PROMOTIONAL) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS_PROMOTIONAL).Value = GUI_FormatNumber(Math.Truncate(DbRow.Value(SQL_COLUMN_POINTS_PROMOTIONAL)), 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS_PROMOTIONAL).Value = 0
    End If

    ' Points expired
    If Not DbRow.IsNull(SQL_COLUMN_POINTS_EXPIRED) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS_EXPIRED).Value = GUI_FormatNumber(Math.Truncate(DbRow.Value(SQL_COLUMN_POINTS_EXPIRED)), 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS_EXPIRED).Value = 0
    End If

    If m_bucket_data Then

      mdl_bucket_for_report.UpdateTotalizers(DbRow, SQL_COLUMN_GT_CONTAINS_PS + 2)

    End If

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.uc_dsl

  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted

  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Creation date selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    ' Dates selection 
    If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate > Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    End If

    ' No activity days
    'If chk_without_activity.Checked Then
    '  If ef_days_activity.Value < 0 Then
    '    Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(159), ENUM_MB_TYPE.MB_TYPE_WARNING)
    '    Call Me.ef_days_activity.Focus()

    '    Return False
    '  End If
    'End If

    ' Maximum days
    If rb_activity.Checked And Me.rb_maximum.Checked Then
      If String.IsNullOrEmpty(ef_days_activity.Value) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_WARNING, , , rb_maximum.Text & " " & ef_days_activity.SufixText)
        Call Me.ef_days_activity.Focus()

        Return False
      End If
    End If

    ' Check fields on uc_account_sel are ok
    If Not Me.uc_account_sel1.ValidateFormat Then
      Return False
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Process clicks on data grid (doible-clicks) and select button
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case ENUM_BUTTON.BUTTON_FILTER_APPLY
        If m_change_col Then
          m_change_col = False
          m_holder_data = chk_holder_data.Checked
          m_current_data = chk_current.Checked
          m_bucket_data = chk_show_bucket.Checked
          Call GUI_StyleSheet()
        End If
        Call MyBase.GUI_ButtonClick(ButtonId)

        'TO DO: It opens the Play Preferences form
      Case ENUM_BUTTON.BUTTON_SELECT
        Call ShowPlayPreferences()

      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        Call PrintData()
      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub ' GUI_ButtonClick

  ' PURPOSE : Checks out the DB row before adding it to the grid
  '              and process counters (& totals rows)
  '
  '  PARAMS :
  '     - INPUT :
  '           - DataRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the data row can be added) or False (the data row can not be added)
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As CLASS_DB_ROW) As Boolean

    Call ProcessCounters(DbRow)

    Return True

  End Function ' GUI_CheckOutRowBeforeAdd


  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_BeforeFirstRow()

    mdl_bucket_for_report.BucketsBueforeFirstRow()

    ResetTotalCounters(m_total_level)
    ResetTotalCounters(m_total)

    m_allow_totals = False

  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_AfterLastRow()

    'Last level
    If m_total_level.num_rows > 0 Then
      Call ProcessSubTotal()
    End If

    'Totals
    Call ProcessTotal()

  End Sub ' GUI_AfterLastRow

  ' PURPOSE: Set the tool tip text for a given row and column
  '
  '  PARAMS:
  '     - INPUT:
  '           - RowIndex As Integer
  '           - ColIndex As Integer
  '     - OUTPUT:
  '           - ToolTipTxt As String
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_SetToolTipText(ByVal RowIndex As Integer, _
                                             ByVal ColIndex As Integer, _
                                             ByRef ToolTipTxt As String)

    If ColIndex = GRID_COLUMN_NO_ACTIVITY Then
      ToolTipTxt = GLB_NLS_GUI_PLAYER_TRACKING.GetString(537)
    End If

  End Sub ' GUI_SetToolTipText

  Protected Overrides Sub GUI_RowSelectedEvent(ByVal SelectedRow As Integer)

    If Not IsValidDataRow(SelectedRow) Then

      Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Enabled = False
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False
    Else

      Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Enabled = True

      ' "Print Player Data" disabled for anonymous accounts.
      If IsNotAnonymous(SelectedRow) And CurrentUser.Permissions(ENUM_FORM.FORM_SHOW_HOLDER_DATA).Read Then
        Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = True
      Else
        Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False
      End If

    End If

  End Sub ' GUI_RowSelectedEvent

#Region " GUI Reports "

  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    With Me.Grid

      .Column(GRID_COLUMN_LEVEL).IsColumnPrintable = True
      .Column(GRID_COLUMN_LAST_LEVEL_DATE).IsColumnPrintable = True
      .Column(GRID_COLUMN_OLD_LEVEL).IsColumnPrintable = True
      .Column(GRID_COLUMN_NEW_LEVEL).IsColumnPrintable = True
      .Column(GRID_COLUMN_POINTS_AWARDED).IsColumnPrintable = True
      .Column(GRID_COLUMN_POINTS_EXPIRED).IsColumnPrintable = True

      If m_holder_data Then

        Call mdl_account_for_report.HolderColumnsPrintable(Me.Grid, GRID_COLUMNS, True)

      End If

      If m_bucket_data Then

        Call mdl_bucket_for_report.BucketsColumnsPrintable(Me.Grid, InitColumnGrid() + mdl_bucket_for_report.GRID_NUM_COLUMNS_BUCKETS, True)

      End If

      If m_current_data Then

        .Column(GRID_COLUMN_CURRENT_BALANCE).IsColumnPrintable = True
        .Column(GRID_COLUMN_CURRENT_POINTS).IsColumnPrintable = True
        .Column(GRID_COLUMN_CURRENT_STATE).IsColumnPrintable = True

      End If

    End With

    Call MyBase.GUI_ReportParams(ExcelData)

    ExcelData.SetColumnFormat(EXCEL_COLUMN_DATE_LEVEL, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.DATE)

    ' Set specific column formats.
    If (m_holder_data) Then

      ExcelData.SetColumnFormat(EXCEL_COLUMN_TELEPHONE_NUMBER_1, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)
      ExcelData.SetColumnFormat(EXCEL_COLUMN_TELEPHONE_NUMBER_2, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)
      ExcelData.SetColumnFormat(EXCEL_COLUMN_POSTAL_CODE, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)
      ExcelData.SetColumnFormat(EXCEL_COLUMN_EXTERNAL_NUMER, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)
      ExcelData.SetColumnFormat(EXCEL_COLUMN_DATE_REGISTER, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.DATE)
      ExcelData.SetColumnFormat(EXCEL_COLUMN_BIRTHDATE, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.DATE)
      ExcelData.SetColumnFormat(EXCEL_COLUMN_LAST_VISIT, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.DATE)
      ExcelData.SetColumnFormat(EXCEL_COLUMN_GT_LAST_VISIT, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.DATE)
      ExcelData.SetColumnFormat(EXCEL_COLUMN_WEDDING_DATE, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.DATE)
    Else
      ExcelData.SetColumnFormat(EXCEL_COLUMN_LAST_VISIT, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.DATE)
      ExcelData.SetColumnFormat(EXCEL_COLUMN_GT_LAST_VISIT, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.DATE)
      ExcelData.SetColumnFormat(EXCEL_COLUMN_BIRTHDATE, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.DATE)
    End If

  End Sub


  '' PURPOSE: Set form specific requirements/parameters forthe report
  ''
  ''  PARAMS:
  ''     - INPUT:
  ''           - PrintData
  ''           - FirstColIndex
  ''     - OUTPUT:
  ''           - None
  ''
  '' RETURNS:
  ''     - None

  'Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
  '                                         Optional ByVal FirstColIndex As Integer = 0)

  '  With Me.Grid

  '    .Column(GRID_COLUMN_ACCOUNT).PrintWidth = GRID_WIDTH_ACCOUNT

  '    .Column(GRID_COLUMN_TRACK_DATA).IsColumnPrintable = False
  '    .Column(GRID_COLUMN_LAST_LEVEL_DATE).IsColumnPrintable = False
  '    .Column(GRID_COLUMN_OLD_LEVEL).IsColumnPrintable = False
  '    .Column(GRID_COLUMN_NEW_LEVEL).IsColumnPrintable = False
  '    .Column(GRID_COLUMN_POINTS_AWARDED).IsColumnPrintable = False
  '    .Column(GRID_COLUMN_POINTS_DISCRETIONAL_LEVEL).IsColumnPrintable = False
  '    .Column(GRID_COLUMN_POINTS_DISCRETIONAL_CHANGE).IsColumnPrintable = False
  '    .Column(GRID_COLUMN_POINTS_EXPIRED).IsColumnPrintable = False

  '    If m_holder_data Then

  '      mdl_account_for_report.HolderColumnsPrintable(Me.Grid, GRID_COLUMNS, False)

  '    End If

  '    If m_bucket_data Then

  '      Call mdl_bucket_for_report.BucketsColumnsPrintable(Me.Grid, InitColumnGrid(), False)

  '    End If

  '    If m_current_data Then

  '      .Column(GRID_COLUMN_CURRENT_BALANCE).IsColumnPrintable = False
  '      .Column(GRID_COLUMN_CURRENT_POINTS).IsColumnPrintable = False
  '      .Column(GRID_COLUMN_CURRENT_STATE).IsColumnPrintable = False

  '    End If

  '  End With

  '  Call MyBase.GUI_ReportParams(PrintData)
  '  PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  'End Sub ' GUI_ReportParams

  '' PURPOSE: Set proper values for form filters being sent to the report
  ''
  ''  PARAMS:
  ''     - INPUT:
  ''           - PrintData
  ''     - OUTPUT:
  ''           - None
  ''
  '' RETURNS:
  ''     - None

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    Dim _terminal_draw_filter As String

    _terminal_draw_filter = "---"

    Call MyBase.GUI_ReportFilter(PrintData)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(830) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(830) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), m_account)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_track_data)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), m_holder)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4802), m_holder_is_vip)

    If Me.chk_only_anonymous.Checked Then
      PrintData.SetFilter(GLB_NLS_GUI_CONFIGURATION.GetString(66), m_only_anonymous)
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286), m_flag)
    Else
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(403), m_gender)
      ' Hide filter if exists an External Loyalty
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(381), m_level)
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286), m_flag)

    End If

    If String.IsNullOrEmpty(m_date_creation_from) Then
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(468) & " " & GLB_NLS_GUI_INVOICING.GetString(202), String.Empty)
    Else
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(468) & " " & GLB_NLS_GUI_INVOICING.GetString(202), GUI_FormatDate(m_date_creation_from, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE))
    End If

    If String.IsNullOrEmpty(m_date_creation_to) Then
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(468) & " " & GLB_NLS_GUI_INVOICING.GetString(203), String.Empty)
    Else
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(468) & " " & GLB_NLS_GUI_INVOICING.GetString(203), GUI_FormatDate(m_date_creation_to, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE))
    End If

    PrintData.SetFilter(Me.chk_holder_data.Text, m_holder_data_filter)
    PrintData.SetFilter(Me.chk_show_bucket.Text, m_bucket_filter)
    PrintData.SetFilter(Me.chk_current.Text, m_current_filter)

    If Not String.IsNullOrEmpty(m_no_activity) And Not String.IsNullOrEmpty(m_no_activity_filter) Then
      PrintData.SetFilter(m_no_activity, m_no_activity_filter)
    End If

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(8706), IIf(Me.m_welcome_draw_filter = TERMINAL_DRAW_FILTER.ONLY_WITH, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)))
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(8707), IIf(Me.m_welcome_draw_filter = TERMINAL_DRAW_FILTER.ONLY_WITHOUT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)))

    PrintData.FilterValueWidth(1) = 3000
    PrintData.FilterHeaderWidth(2) = 1800
    PrintData.FilterHeaderWidth(3) = 3200
    PrintData.FilterValueWidth(3) = 2500

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportUpdateFilters()

    Dim _date As Date

    m_date_from = ""
    m_date_to = ""
    m_date_creation_from = ""
    m_date_creation_to = ""
    m_current_filter = ""
    m_holder_data_filter = ""
    m_bucket_filter = ""

    m_level = ""
    m_gender = ""
    m_only_anonymous = ""

    ' Card Account number
    m_account = Me.uc_account_sel1.Account()
    m_track_data = Me.uc_account_sel1.TrackData()
    m_holder = Me.uc_account_sel1.Holder()
    m_holder_is_vip = Me.uc_account_sel1.HolderIsVip()

    ' Date
    _date = GetFromDate()
    m_date_from = GUI_FormatDate(_date, _
                                   ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    _date = GetToTime()
    m_date_to = GUI_FormatDate(_date, _
                                 ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    ' Date creation
    If Me.dtp_from.Checked Then
      m_date_creation_from = GUI_FormatDate(dtp_from.Value, _
                                   ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If
    If Me.dtp_to.Checked Then
      m_date_creation_to = GUI_FormatDate(dtp_to.Value, _
                                 ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    ' Level
    m_level = GetLevelsSelected(False)

    ' Gender
    m_gender = GetGendersSelected(False)

    ' Only anonymous
    m_only_anonymous = IIf(Me.chk_only_anonymous.Checked, GLB_NLS_GUI_PLAYER_TRACKING.GetString(359), GLB_NLS_GUI_PLAYER_TRACKING.GetString(360))

    If Me.m_current_data Then
      m_current_filter = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
    Else
      m_current_filter = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)
    End If

    If Me.m_holder_data Then
      m_holder_data_filter = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
    Else
      m_holder_data_filter = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)
    End If

    If Me.m_bucket_data Then
      m_bucket_filter = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
    Else
      m_bucket_filter = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)
    End If

    If Me.rb_all.Checked Then
      m_no_activity = GLB_NLS_GUI_PLAYER_TRACKING.GetString(831)
      m_no_activity_filter = GLB_NLS_GUI_STATISTICS.GetString(206)
    End If

    If Me.rb_activity.Checked Then
      m_no_activity = GLB_NLS_GUI_PLAYER_TRACKING.GetString(843)
      If Me.rb_without_maximum.Checked Then
        m_no_activity_filter = GLB_NLS_GUI_PLAYER_TRACKING.GetString(844)
      End If
      If Me.rb_maximum.Checked Then
        m_no_activity_filter = GLB_NLS_GUI_PLAYER_TRACKING.GetString(845) & " " & Me.ef_days_activity.Value & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(846)
      End If
    End If

    ' QMP 02-JUL-2013: Account flag filter
    m_flag = Me.GetSelectedFlagsTitles()

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region

#Region " Private "

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub GUI_StyleSheet()

    Dim _num_column As Integer

    With Me.Grid

      .Redraw = False

      'INITIALIZE THE INDEX OF UNFIXED COLUMNS AND RETURN THE NUMBER OF COLUMNS
      _num_column = InitColumnGrid()

      'SGB 29-DEC-2015: Integrate buckets in grid
      mdl_bucket_for_report.CountColumnsBuckets(m_bucket_data)

      Call .Init(_num_column + mdl_bucket_for_report.GRID_NUM_COLUMNS_BUCKETS, GRID_HEADER_ROWS, False)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_COLUMN_INDEX).WidthFixed = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Player Tracking - Level 
      .Column(GRID_COLUMN_LEVEL).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_LEVEL).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(381)
      .Column(GRID_COLUMN_LEVEL).Width = GRID_WIDTH_LEVEL
      .Column(GRID_COLUMN_LEVEL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Account - Number
      .Column(GRID_COLUMN_ACCOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_ACCOUNT).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COLUMN_ACCOUNT).Width = GRID_WIDTH_ACCOUNT
      .Column(GRID_COLUMN_ACCOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Account - Card
      .Column(GRID_COLUMN_TRACK_DATA).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_TRACK_DATA).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(212)
      .Column(GRID_COLUMN_TRACK_DATA).Width = GRID_WIDTH_CARD_TRACK_DATA
      .Column(GRID_COLUMN_TRACK_DATA).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Account - Holder Name
      .Column(GRID_COLUMN_HOLDER_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_HOLDER_NAME).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(235)
      .Column(GRID_COLUMN_HOLDER_NAME).Width = GRID_WIDTH_CARD_HOLDER_NAME
      .Column(GRID_COLUMN_HOLDER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Account - Creation
      'NLS_ID_GUI_PLAYER_TRACKING(544)   "Account created"                     // 
      .Column(GRID_COLUMN_ACCOUNT_CREATION).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_ACCOUNT_CREATION).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(468)
      .Column(GRID_COLUMN_ACCOUNT_CREATION).Width = GRID_WIDTH_DATE_CREATION
      .Column(GRID_COLUMN_ACCOUNT_CREATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Terminal play sessions
      ' Balance - Coin in
      'NLS_ID_GUI_PLAYER_TRACKING(537)   "Coin in"                             // 
      .Column(GRID_COLUMN_COININ_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7926) ' "Terminales "
      .Column(GRID_COLUMN_COININ_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(537)
      .Column(GRID_COLUMN_COININ_AMOUNT).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_COININ_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Played Count
      .Column(GRID_COLUMN_PLAYED_COUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7926) ' "Terminales "
      .Column(GRID_COLUMN_PLAYED_COUNT).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(261)
      .Column(GRID_COLUMN_PLAYED_COUNT).Width = 950
      .Column(GRID_COLUMN_PLAYED_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Average bet
      .Column(GRID_COLUMN_AVERAGE_BET).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7926) ' "Terminales "
      .Column(GRID_COLUMN_AVERAGE_BET).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(347)
      .Column(GRID_COLUMN_AVERAGE_BET).Width = 1400
      .Column(GRID_COLUMN_AVERAGE_BET).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Balance - Coin in redeemable
      'NLS_ID_GUI_PLAYER_TRACKING(1250)   "Coin in redeemable"                             // 
      .Column(GRID_COLUMN_COININ_REDEEM_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7926) ' "Terminales "
      .Column(GRID_COLUMN_COININ_REDEEM_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1250)
      .Column(GRID_COLUMN_COININ_REDEEM_AMOUNT).Width = 1900
      .Column(GRID_COLUMN_COININ_REDEEM_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Balance - Cash in
      'NLS_ID_GUI_PLAYER_TRACKING(538)   "Cash in"                                        // 
      .Column(GRID_COLUMN_CASHIN_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7926) ' "Terminales "
      If m_is_tito_mode Then
        .Column(GRID_COLUMN_CASHIN_AMOUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(398)        'Cash In
      Else
        .Column(GRID_COLUMN_CASHIN_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(538)  'Recargas
      End If
      .Column(GRID_COLUMN_CASHIN_AMOUNT).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_CASHIN_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Balance - Net Win
      'NLS_ID_GUI_PLAYER_TRACKING(539)   "Net win"                             // 
      .Column(GRID_COLUMN_NETWIN_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7926) ' "Terminales "
      .Column(GRID_COLUMN_NETWIN_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(539)

      If m_is_tito_mode Then
        .Column(GRID_COLUMN_NETWIN_AMOUNT).Width = 0
      Else
        .Column(GRID_COLUMN_NETWIN_AMOUNT).Width = GRID_WIDTH_AMOUNT
      End If

      .Column(GRID_COLUMN_NETWIN_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Gambling tables play sessions
      ' Balance - Coin in
      'NLS_ID_GUI_PLAYER_TRACKING(537)   "Coin in"                             // 
      .Column(GRID_COLUMN_GT_COININ_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7621)  '  "Mesas de juego"
      .Column(GRID_COLUMN_GT_COININ_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(537)
      If GamingTableBusinessLogic.IsGamingTablesEnabled Then
        .Column(GRID_COLUMN_GT_COININ_AMOUNT).Width = GRID_WIDTH_AMOUNT
      Else
        .Column(GRID_COLUMN_GT_COININ_AMOUNT).Width = 0
      End If
      .Column(GRID_COLUMN_GT_COININ_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Played Count
      .Column(GRID_COLUMN_GT_PLAYED_COUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7621)  '  "Mesas de juego"
      .Column(GRID_COLUMN_GT_PLAYED_COUNT).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(261)
      If GamingTableBusinessLogic.IsGamingTablesEnabled Then
        .Column(GRID_COLUMN_GT_PLAYED_COUNT).Width = 950
      Else
        .Column(GRID_COLUMN_GT_PLAYED_COUNT).Width = 0
      End If
      .Column(GRID_COLUMN_GT_PLAYED_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Average bet
      .Column(GRID_COLUMN_GT_AVERAGE_BET).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7621)  '  "Mesas de juego"
      .Column(GRID_COLUMN_GT_AVERAGE_BET).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(347)
      If GamingTableBusinessLogic.IsGamingTablesEnabled Then
        .Column(GRID_COLUMN_GT_AVERAGE_BET).Width = 1400
      Else
        .Column(GRID_COLUMN_GT_AVERAGE_BET).Width = 0
      End If
      .Column(GRID_COLUMN_GT_AVERAGE_BET).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Balance - Coin in redeemable
      'NLS_ID_GUI_PLAYER_TRACKING(1250)   "Coin in redeemable"                             // 
      .Column(GRID_COLUMN_GT_COININ_REDEEM_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7621)  '  "Mesas de juego"
      .Column(GRID_COLUMN_GT_COININ_REDEEM_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1250)
      If GamingTableBusinessLogic.IsGamingTablesEnabled Then
        .Column(GRID_COLUMN_GT_COININ_REDEEM_AMOUNT).Width = 1900
      Else
        .Column(GRID_COLUMN_GT_COININ_REDEEM_AMOUNT).Width = 0
      End If
      .Column(GRID_COLUMN_GT_COININ_REDEEM_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Balance - Cash in
      'NLS_ID_GUI_PLAYER_TRACKING(538)   "Cash in"                                        // 
      .Column(GRID_COLUMN_GT_CASHIN_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7621)  '  "Mesas de juego"
      If m_is_tito_mode Then
        .Column(GRID_COLUMN_GT_CASHIN_AMOUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(398)        'Cash In
      Else
        .Column(GRID_COLUMN_GT_CASHIN_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(538)  'Recargas
      End If
      If GamingTableBusinessLogic.IsGamingTablesEnabled Then
        .Column(GRID_COLUMN_GT_CASHIN_AMOUNT).Width = GRID_WIDTH_AMOUNT
      Else
        .Column(GRID_COLUMN_GT_CASHIN_AMOUNT).Width = 0
      End If
      .Column(GRID_COLUMN_GT_CASHIN_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Balance - Net Win
      'NLS_ID_GUI_PLAYER_TRACKING(539)   "Net win"                             // 
      .Column(GRID_COLUMN_GT_NETWIN_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7621)  '  "Mesas de juego"
      .Column(GRID_COLUMN_GT_NETWIN_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(539)

      If m_is_tito_mode Or Not GamingTableBusinessLogic.IsGamingTablesEnabled() Then
        .Column(GRID_COLUMN_GT_NETWIN_AMOUNT).Width = 0
      Else
        .Column(GRID_COLUMN_GT_NETWIN_AMOUNT).Width = GRID_WIDTH_AMOUNT
      End If

      .Column(GRID_COLUMN_GT_NETWIN_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT


      ' Balance - Total
      'NLS_ID_GUI_PLAYER_TRACKING(541)   "Balance"  'RETIROS=>COBRADO=>Cash Out                     // 
      .Column(GRID_COLUMN_BALANCE_AMOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(240)
      If m_is_tito_mode Then
        .Column(GRID_COLUMN_BALANCE_AMOUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(399)       'Cash Out
        .Column(GRID_COLUMN_BALANCE_AMOUNT).Width = 0
      Else
        .Column(GRID_COLUMN_BALANCE_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(540) 'Retiros
        .Column(GRID_COLUMN_BALANCE_AMOUNT).Width = GRID_WIDTH_AMOUNT
      End If

      .Column(GRID_COLUMN_BALANCE_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT


      ' Balance - Withdraw
      'NLS_ID_GUI_PLAYER_TRACKING(540)   "Withdraw"                            // 
      .Column(GRID_COLUMN_WITHDRAW_AMOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(240)
      .Column(GRID_COLUMN_WITHDRAW_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1349)

      If m_is_tito_mode Then
        .Column(GRID_COLUMN_WITHDRAW_AMOUNT).Width = 0
      Else
        .Column(GRID_COLUMN_WITHDRAW_AMOUNT).Width = GRID_WIDTH_AMOUNT
      End If

      .Column(GRID_COLUMN_WITHDRAW_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Player Tracking - Date of level
      'NLS_ID_GUI_PLAYER_TRACKING(545)   "Date level"                          // 
      .Column(GRID_COLUMN_LAST_LEVEL_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(545)
      .Column(GRID_COLUMN_LAST_LEVEL_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1050)
      .Column(GRID_COLUMN_LAST_LEVEL_DATE).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_LAST_LEVEL_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Player Tracking - Old Level
      'NLS_ID_GUI_PLAYER_TRACKING()   "Old Level"                          // 
      .Column(GRID_COLUMN_OLD_LEVEL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(545)
      .Column(GRID_COLUMN_OLD_LEVEL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1051)
      .Column(GRID_COLUMN_OLD_LEVEL).Width = GRID_WIDTH_LEVEL_CHANGE
      .Column(GRID_COLUMN_OLD_LEVEL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Player Tracking - New level
      'NLS_ID_GUI_PLAYER_TRACKING()   "New level"                          // 
      .Column(GRID_COLUMN_NEW_LEVEL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(545)
      .Column(GRID_COLUMN_NEW_LEVEL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1052)
      .Column(GRID_COLUMN_NEW_LEVEL).Width = GRID_WIDTH_LEVEL_CHANGE
      .Column(GRID_COLUMN_NEW_LEVEL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Player Tracking - Points awarded
      'NLS_ID_GUI_PLAYER_TRACKING(547)   "Generated - Level points"
      .Column(GRID_COLUMN_POINTS_AWARDED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1883)
      .Column(GRID_COLUMN_POINTS_AWARDED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1885)
      .Column(GRID_COLUMN_POINTS_AWARDED).Width = GRID_WIDTH_POINTS
      .Column(GRID_COLUMN_POINTS_AWARDED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Player Tracking - Discretional points level
      'NLS_ID_GUI_PLAYER_TRACKING()   "Discretionals - Level points" 
      .Column(GRID_COLUMN_POINTS_DISCRETIONAL_LEVEL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1884)
      .Column(GRID_COLUMN_POINTS_DISCRETIONAL_LEVEL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1885)
      .Column(GRID_COLUMN_POINTS_DISCRETIONAL_LEVEL).Width = GRID_WIDTH_POINTS
      .Column(GRID_COLUMN_POINTS_DISCRETIONAL_LEVEL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Player Tracking - Discretional points change
      'NLS_ID_GUI_PLAYER_TRACKING()   "Discretionals - Change points"
      .Column(GRID_COLUMN_POINTS_DISCRETIONAL_CHANGE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1884)
      .Column(GRID_COLUMN_POINTS_DISCRETIONAL_CHANGE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1886)
      .Column(GRID_COLUMN_POINTS_DISCRETIONAL_CHANGE).Width = GRID_WIDTH_POINTS
      .Column(GRID_COLUMN_POINTS_DISCRETIONAL_CHANGE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Promotional points change
      'NLS_ID_GUI_PLAYER_TRACKING()   "Promotional - Change points"
      .Column(GRID_COLUMN_POINTS_PROMOTIONAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1889)
      .Column(GRID_COLUMN_POINTS_PROMOTIONAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1886)
      .Column(GRID_COLUMN_POINTS_PROMOTIONAL).Width = GRID_WIDTH_POINTS
      .Column(GRID_COLUMN_POINTS_PROMOTIONAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Player Tracking - Points expired
      'NLS_ID_GUI_PLAYER_TRACKING(1049)   "Points expired"                      // 
      .Column(GRID_COLUMN_POINTS_EXPIRED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1887)
      .Column(GRID_COLUMN_POINTS_EXPIRED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1888)
      .Column(GRID_COLUMN_POINTS_EXPIRED).Width = GRID_WIDTH_POINTS
      .Column(GRID_COLUMN_POINTS_EXPIRED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Terminals
      ' Statistics - Visits
      'NLS_ID_GUI_PLAYER_TRACKING(542)   "Visits"                              // 
      .Column(GRID_COLUMN_VISITS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(549) & " (" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7926) & ")" ' "Terminales "
      .Column(GRID_COLUMN_VISITS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(542)
      .Column(GRID_COLUMN_VISITS).Width = GRID_WIDTH_NUMBER
      .Column(GRID_COLUMN_VISITS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Statistics - Date of last visit
      ' NLS_ID_GUI_PLAYER_TRACKING(543)   "Last visit"                          // 
      .Column(GRID_COLUMN_LAST_VISIT_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(549) & " (" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7926) & ")"  ' "Terminales "
      .Column(GRID_COLUMN_LAST_VISIT_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(543)
      .Column(GRID_COLUMN_LAST_VISIT_DATE).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_LAST_VISIT_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Statistics - Total played seconds
      'NLS_ID_GUI_PLAYER_TRACKING(546)   "Played seconds/Visits"                // 
      .Column(GRID_COLUMN_TOTAL_PLAYED_SEC).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(549) & " (" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7926) & ")"  ' "Terminales "
      .Column(GRID_COLUMN_TOTAL_PLAYED_SEC).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(546)
      .Column(GRID_COLUMN_TOTAL_PLAYED_SEC).Width = GRID_WIDTH_TOTAL_PLAYED_SEC
      .Column(GRID_COLUMN_TOTAL_PLAYED_SEC).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Statistics - No activity
      'NLS_ID_GUI_PLAYER_TRACKING(548)   "No activity"                         //
      .Column(GRID_COLUMN_NO_ACTIVITY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(549) & " (" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7926) & ")"  ' "Terminales "
      .Column(GRID_COLUMN_NO_ACTIVITY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(548)
      .Column(GRID_COLUMN_NO_ACTIVITY).Width = GRID_WIDTH_NO_ACTIVITY
      .Column(GRID_COLUMN_NO_ACTIVITY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'Gambling tables
      ' Statistics - Visits
      'NLS_ID_GUI_PLAYER_TRACKING(542)   "Visits"                              // 
      .Column(GRID_COLUMN_GT_VISITS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(549) & " (" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7621) & ")"   '  "Mesas de juego"
      .Column(GRID_COLUMN_GT_VISITS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(542)
      If GamingTableBusinessLogic.IsGamingTablesEnabled Then
        .Column(GRID_COLUMN_GT_VISITS).Width = GRID_WIDTH_NUMBER
      Else
        .Column(GRID_COLUMN_GT_VISITS).Width = 0
      End If
      .Column(GRID_COLUMN_GT_VISITS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Statistics - Date of last visit
      ' NLS_ID_GUI_PLAYER_TRACKING(543)   "Last visit"                          // 
      .Column(GRID_COLUMN_GT_LAST_VISIT_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(549) & " (" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7621) & ")"   '  "Mesas de juego"
      .Column(GRID_COLUMN_GT_LAST_VISIT_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(543)
      If GamingTableBusinessLogic.IsGamingTablesEnabled Then
        .Column(GRID_COLUMN_GT_LAST_VISIT_DATE).Width = GRID_WIDTH_DATE
      Else
        .Column(GRID_COLUMN_GT_LAST_VISIT_DATE).Width = 0
      End If
      .Column(GRID_COLUMN_GT_LAST_VISIT_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Statistics - Total played seconds
      'NLS_ID_GUI_PLAYER_TRACKING(546)   "Played seconds/Visits"                // 
      .Column(GRID_COLUMN_GT_TOTAL_PLAYED_SEC).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(549) & " (" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7621) & ")"   '  "Mesas de juego"
      .Column(GRID_COLUMN_GT_TOTAL_PLAYED_SEC).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(546)
      If GamingTableBusinessLogic.IsGamingTablesEnabled Then
        .Column(GRID_COLUMN_GT_TOTAL_PLAYED_SEC).Width = GRID_WIDTH_TOTAL_PLAYED_SEC
      Else
        .Column(GRID_COLUMN_GT_TOTAL_PLAYED_SEC).Width = 0
      End If
      .Column(GRID_COLUMN_GT_TOTAL_PLAYED_SEC).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Statistics - No activity
      'NLS_ID_GUI_PLAYER_TRACKING(548)   "No activity"                         //
      .Column(GRID_COLUMN_GT_NO_ACTIVITY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(549) & " (" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7621) & ")"   '  "Mesas de juego"
      .Column(GRID_COLUMN_GT_NO_ACTIVITY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(548)
      If GamingTableBusinessLogic.IsGamingTablesEnabled Then
        .Column(GRID_COLUMN_GT_NO_ACTIVITY).Width = GRID_WIDTH_NO_ACTIVITY
      Else
        .Column(GRID_COLUMN_GT_NO_ACTIVITY).Width = 0
      End If
      .Column(GRID_COLUMN_GT_NO_ACTIVITY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      If m_holder_data Then

        Call mdl_account_for_report.StyleSheetHolderData(Grid, GRID_COLUMNS, , m_holder_data)
      End If

      If m_current_data Then

        '' Current Situation - Balance
        .Column(GRID_COLUMN_CURRENT_BALANCE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(557)
        .Column(GRID_COLUMN_CURRENT_BALANCE).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(240)
        .Column(GRID_COLUMN_CURRENT_BALANCE).Width = GRID_WIDTH_AMOUNT
        .Column(GRID_COLUMN_CURRENT_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        '' Current Situation - Points
        .Column(GRID_COLUMN_CURRENT_POINTS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(557)
        .Column(GRID_COLUMN_CURRENT_POINTS).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(305)
        .Column(GRID_COLUMN_CURRENT_POINTS).Width = GRID_WIDTH_NUMBER
        .Column(GRID_COLUMN_CURRENT_POINTS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        '' Current Situation - State
        .Column(GRID_COLUMN_CURRENT_STATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(557)
        .Column(GRID_COLUMN_CURRENT_STATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(558)
        .Column(GRID_COLUMN_CURRENT_STATE).Width = GRID_WIDTH_NUMBER
        .Column(GRID_COLUMN_CURRENT_STATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      End If

      If m_bucket_data Then

        Call mdl_bucket_for_report.StyleSheetBucketData(Grid, _num_column, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7112), m_bucket_data)

      End If

      .Redraw = True
      .Sortable = False

    End With

  End Sub

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()
    SetDefaultValues(True)
  End Sub


  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - ClearUcAccountSel
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues(ClearUcAccountSel As Boolean)

    Dim [date] As DateTime
    Dim closing_time As Integer
    Dim initial_time As Date

    initial_time = WSI.Common.Misc.TodayOpening()
    closing_time = GetDefaultClosingTime()

    [date] = New DateTime(Now.Year, Now.Month, 1)
    Me.uc_dsl.FromDate = [date].AddMonths(-1)
    Me.uc_dsl.FromDateSelected = True

    Me.uc_dsl.ToDate = Me.uc_dsl.FromDate.AddMonths(1)
    Me.uc_dsl.ToDateSelected = True

    Me.uc_dsl.ClosingTime = closing_time

    Me.dtp_from.Value = initial_time
    Me.dtp_from.Checked = False
    Me.dtp_to.Value = Me.dtp_from.Value.AddDays(1)
    Me.dtp_to.Checked = False

    If (ClearUcAccountSel) Then
      Me.uc_account_sel1.Clear()
    End If

    Me.chk_level_01.Checked = False
    Me.chk_level_02.Checked = False
    Me.chk_level_03.Checked = False
    Me.chk_level_04.Checked = False

    Me.chk_gender_male.Checked = False
    Me.chk_gender_female.Checked = False

    Me.chk_only_anonymous.Checked = False
    Me.chk_holder_data.Checked = False
    Me.chk_current.Checked = True

    Me.rb_activity.Checked = True
    Me.rb_maximum.Checked = True

    Me.ef_days_activity.Value = DateDiff(DateInterval.Day, Me.uc_dsl.FromDate, Me.uc_dsl.ToDate)  '1 month aprox.

    ' QMP 02-JUL-2013: Account flag filter
    Call UncheckAllFlags()
    Me.gb_flag.Enabled = True

    Me.chk_show_bucket.Checked = False

  End Sub ' SetDefaultValues

  ' PURPOSE: Get Level list for selected levels
  '          Format list to build query: X, X, X
  '          X is the Id if GetIds = True
  '          otherwise, X is the Name of the control
  '  PARAMS:
  '     - INPUT:
  '           - GetIds As Boolean
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String

  Private Function GetLevelsSelected(Optional ByVal GetIds As Boolean = True) As String
    Dim list_type As String

    list_type = ""
    If Me.chk_level_01.Checked Then
      list_type = IIf(GetIds, "1", Me.chk_level_01.Text)
    End If
    If Me.chk_level_02.Checked Then
      If list_type.Length > 0 Then
        list_type = list_type & ", "
      End If
      list_type = list_type & IIf(GetIds, "2", Me.chk_level_02.Text)
    End If
    If Me.chk_level_03.Checked Then
      If list_type.Length > 0 Then
        list_type = list_type & ", "
      End If
      list_type = list_type & IIf(GetIds, "3", Me.chk_level_03.Text)
    End If
    If Me.chk_level_04.Checked Then
      If list_type.Length > 0 Then
        list_type = list_type & ", "
      End If
      list_type = list_type & IIf(GetIds, "4", Me.chk_level_04.Text)
    End If

    Return list_type
  End Function ' GetLevelsSelected

  ' PURPOSE: Get Gender list for selected genders
  '          Format list to build query: X, X, X
  '          X is the Id if GetIds = True
  '          otherwise, X is the Name of the control
  '  PARAMS:
  '     - INPUT:
  '           - GetIds As Boolean
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String

  Private Function GetGendersSelected(Optional ByVal GetIds As Boolean = True) As String
    Dim list_type As String

    list_type = ""
    If chk_gender_male.Checked Then
      list_type = IIf(GetIds, "1", Me.chk_gender_male.Text)
    End If
    If chk_gender_female.Checked Then
      If list_type.Length > 0 Then
        list_type = list_type & ", "
      End If
      list_type = list_type & IIf(GetIds, "2", Me.chk_gender_female.Text)
    End If

    Return list_type
  End Function ' GetGendersSelected

  ' PURPOSE: 
  '     
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String

  Private Function GetSqlWhereAccount() As String

    Dim str_where As String = ""

    str_where = Me.uc_account_sel1.GetFilterSQL()

    If Not String.IsNullOrEmpty(str_where) Then str_where = " AND " & str_where

    If Not Me.uc_account_sel1.DisabledHolder AndAlso String.IsNullOrEmpty(Me.uc_account_sel1.Account) Then

      ' Filter Dates - Creation
      If Me.dtp_from.Checked = True Then
        str_where = str_where & " AND (AC_CREATED >= " & GUI_FormatDateDB(dtp_from.Value) & ") "
      End If

      If Me.dtp_to.Checked = True Then
        str_where = str_where & " AND (AC_CREATED < " & GUI_FormatDateDB(dtp_to.Value) & ") "
      End If

      ' Level
      If Not Me.chk_only_anonymous.Checked Then
        ' Ignore filters if exists an External Loyalty
        If Me.chk_level_01.Checked Or _
             Me.chk_level_02.Checked Or _
             Me.chk_level_03.Checked Or _
             Me.chk_level_04.Checked Then

          str_where = str_where & " AND AC_HOLDER_LEVEL IN (" & GetLevelsSelected() & ") "
        End If

        ' Gender
        If Me.chk_gender_male.Checked Or _
           Me.chk_gender_female.Checked Then

          str_where = str_where & " AND AC_HOLDER_GENDER IN (" & GetGendersSelected() & ") "
        End If
      Else
        str_where = str_where & " AND ( AC_USER_TYPE IS NULL OR AC_USER_TYPE = 0 ) "
      End If

    End If

    If String.IsNullOrEmpty(str_where) Then
      Return " "
    Else
      'Return " WHERE " & str_where.Trim.TrimStart(New Char() {"A", "N", "D"})
      Return str_where
    End If

  End Function

  ' PURPOSE: Ge to time from user control 
  '     
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String

  Private Function GetToTime() As Date

    Dim _to_time As Date = New Date(Me.uc_dsl.ToDate.Year, Me.uc_dsl.ToDate.Month, Me.uc_dsl.ToDate.Day, Me.uc_dsl.ClosingTime, 0, 0)
    Dim _to_date_default As Date = WSI.Common.Misc.TodayOpening().AddDays(1)

    If Me.uc_dsl.ToDateSelected Then
      If _to_time > _to_date_default Then
        _to_time = _to_date_default
      End If
    Else
      _to_time = _to_date_default
    End If

    Return _to_time

  End Function ' GetToTime

  ' PURPOSE: Ge from time from user control 
  '     
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String

  Private Function GetFromDate() As Date

    Return New Date(Me.uc_dsl.FromDate.Year, Me.uc_dsl.FromDate.Month, Me.uc_dsl.FromDate.Day, Me.uc_dsl.ClosingTime, 0, 0)

  End Function ' GetFromDate

  ' PURPOSE: Prepare columns of the grid
  '     
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String

  Private Function InitColumnGrid() As Integer

    Dim _acols As Integer


    _acols = GRID_COLUMNS

    If m_holder_data Then

      _acols += mdl_account_for_report.GRID_NUM_COLUMNS_HOLDER_DATA

    End If

    If m_current_data Then
      ' Current situation of the account
      GRID_COLUMN_CURRENT_BALANCE = _acols + 0
      GRID_COLUMN_CURRENT_POINTS = _acols + 1
      GRID_COLUMN_CURRENT_STATE = _acols + 2

      _acols += GRID_COLUMNS_CURRENT_DATA
    End If

    Return _acols

  End Function ' InitColumnGrid

  ' PURPOSE: Returns a column value in row, if it null then return 0
  '     
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String

  Private Function GetValNum(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW, ByVal Col As Integer) As Object

    If DbRow.IsNull(Col) Then
      Return 0
    Else
      Return DbRow.Value(Col)
    End If

  End Function

  ' PURPOSE : Reset Total Counters variable
  '
  '  PARAMS :
  '     - INPUT : 
  '           - Total As TYPE_TOTAL_COUNTERS
  '           - Key As String
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub ResetTotalCounters(ByRef Total As TYPE_TOTAL_COUNTERS)

    Total.level = ""
    Total.num_rows = 0
    Total.coin_in_amount = 0
    Total.played_count_amount = 0
    Total.coin_in_redeem_amount = 0
    Total.cash_in_amount = 0
    Total.net_win_amount = 0
    Total.with_draw_amount = 0
    Total.balance_amount = 0
    Total.total_played = 0
    Total.visits = 0
    Total.points = 0
    Total.points_discretional_level = 0
    Total.points_discretional_change = 0
    Total.points_promotional = 0
    Total.points_expired = 0
    Total.total_balance = 0
    Total.total_points = 0

  End Sub ' ResetTotalCounters

  ' PURPOSE : Update total counters.
  '
  '  PARAMS :
  '     - INPUT :
  '           - Total As TYPE_TOTAL_COUNTERS
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub UpdateTotalCounters(ByRef Total As TYPE_TOTAL_COUNTERS, ByVal DbRow As CLASS_DB_ROW)

    Total.level = DbRow.Value(SQL_COLUMN_HOLDER_LEVEL)
    Total.num_rows += 1

    Total.total_balance += GetValNum(DbRow, SQL_COLUMN_CURRENT_BALANCE)
    Total.total_points += Math.Truncate(GetValNum(DbRow, SQL_COLUMN_CURRENT_POINTS))

    Total.coin_in_amount += GetValNum(DbRow, SQL_COLUMN_COIN_IN)
    Total.played_count_amount += GetValNum(DbRow, SQL_COLUMN_PLAYED_COUNT)
    Total.coin_in_redeem_amount += GetValNum(DbRow, SQL_COLUMN_COIN_IN_REDEEM)
    Total.cash_in_amount += GetValNum(DbRow, SQL_COLUMN_CASH_IN)
    Total.net_win_amount += GetValNum(DbRow, SQL_COLUMN_NETWIN)
    Total.with_draw_amount += GetValNum(DbRow, SQL_COLUMN_WITHDRAW)
    Total.balance_amount += GetValNum(DbRow, SQL_COLUMN_BALANCE)
    Total.visits += GetValNum(DbRow, SQL_COLUMN_VISITS)
    Total.total_played += GetValNum(DbRow, SQL_COLUMN_TOTAL_PLAYED_SEC)
    Total.points += Math.Truncate(GetValNum(DbRow, SQL_COLUMN_POINTS_AWARDED))
    Total.points_discretional_level += Math.Truncate(GetValNum(DbRow, SQL_COLUMN_POINTS_DISCRETIONAL_LEVEL))
    Total.points_discretional_change += Math.Truncate(GetValNum(DbRow, SQL_COLUMN_POINTS_DISCRETIONAL_CHANGE))
    Total.points_promotional += Math.Truncate(GetValNum(DbRow, SQL_COLUMN_POINTS_PROMOTIONAL))
    Total.points_expired += Math.Truncate(GetValNum(DbRow, SQL_COLUMN_POINTS_EXPIRED))

  End Sub ' UpdateTotalCounters

  ' PURPOSE : Process total counters: Draw row of totals, update and reset total counters.
  '
  '  PARAMS :
  '     - INPUT : 
  '           - DbRow As CLASS_DB_ROW
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub ProcessCounters(ByVal DbRow As CLASS_DB_ROW)

    Dim _holder_level As String

    _holder_level = DbRow.Value(SQL_COLUMN_HOLDER_LEVEL)

    If m_allow_totals AndAlso DbRow.Value(SQL_COLUMN_HOLDER_LEVEL) <> m_total_level.level Then
      Call ProcessSubTotal()
      Call ResetTotalCounters(m_total_level)
      If (Me.chk_show_bucket.Checked) Then
        mdl_bucket_for_report.InitSubTotal()
      End If
    End If

    UpdateTotalCounters(m_total, DbRow)
    UpdateTotalCounters(m_total_level, DbRow)

    m_allow_totals = True

  End Sub ' ProcessCounters

  ' PURPOSE : Process sub-total data for DrawRow
  '
  '  PARAMS :
  '     - INPUT : 
  '           - DbRow As CLASS_DB_ROW
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub ProcessSubTotal()
    Dim _color As Integer

    m_total_level.level = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1046) & " " & m_total_level.level
    _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01
    DrawRow(m_total_level, _color)

  End Sub ' ProcessSubTotal

  ' PURPOSE : Process total data for DrawRow
  '
  '  PARAMS :
  '     - INPUT : 
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub ProcessTotal()
    Dim _color As Integer

    m_total.level = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1047).ToUpper
    _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
    DrawRow(m_total, _color)

  End Sub ' ProcessTotal

  ' PURPOSE: Show results to screen
  '
  '  PARAMS:
  '     - INPUT:
  '           - Provider items with data
  '           - color of row
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Function DrawRow(ByVal Total As TYPE_TOTAL_COUNTERS, ByVal Color As Integer) As Boolean

    Dim idx_row As Integer
    Dim _total_average_bet As Double

    With Me.Grid
      .AddRow()
      idx_row = Me.Grid.NumRows - 1

      .Cell(idx_row, GRID_COLUMN_LEVEL).Value = Total.level

      ' Coin in
      .Cell(idx_row, GRID_COLUMN_COININ_AMOUNT).Value = GUI_FormatCurrency(Total.coin_in_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      ' Played count
      .Cell(idx_row, GRID_COLUMN_PLAYED_COUNT).Value = GUI_FormatNumber(Total.played_count_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)

      ' Average bet
      If Total.played_count_amount = 0 Then
        .Cell(idx_row, GRID_COLUMN_AVERAGE_BET).Value = ""
      Else
        _total_average_bet = Math.Round(Total.coin_in_amount / Total.played_count_amount, 2, MidpointRounding.AwayFromZero)
        .Cell(idx_row, GRID_COLUMN_AVERAGE_BET).Value = GUI_FormatCurrency(_total_average_bet, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End If

      ' Coin in redeem
      .Cell(idx_row, GRID_COLUMN_COININ_REDEEM_AMOUNT).Value = GUI_FormatCurrency(Total.coin_in_redeem_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      ' Chash in
      .Cell(idx_row, GRID_COLUMN_CASHIN_AMOUNT).Value = GUI_FormatCurrency(Total.cash_in_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      ' Netwin
      .Cell(idx_row, GRID_COLUMN_NETWIN_AMOUNT).Value = GUI_FormatCurrency(Total.net_win_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      ' Withdraw
      .Cell(idx_row, GRID_COLUMN_WITHDRAW_AMOUNT).Value = GUI_FormatCurrency(Total.with_draw_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      ' Balance
      .Cell(idx_row, GRID_COLUMN_BALANCE_AMOUNT).Value = GUI_FormatCurrency(Total.balance_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      ' Total played / visits
      If Total.visits > 0 Then
        .Cell(idx_row, GRID_COLUMN_TOTAL_PLAYED_SEC).Value = TimeSpanToString(New TimeSpan(0, 0, Total.total_played / Total.visits))
      End If

      ' Visits
      .Cell(idx_row, GRID_COLUMN_VISITS).Value = GUI_FormatNumber(Total.visits, 0)

      ' Wins
      .Cell(idx_row, GRID_COLUMN_POINTS_AWARDED).Value = GUI_FormatNumber(Total.points, 0)

      ' Discertional points level
      .Cell(idx_row, GRID_COLUMN_POINTS_DISCRETIONAL_LEVEL).Value = GUI_FormatNumber(Total.points_discretional_level, 0)

      ' Discretional points change
      .Cell(idx_row, GRID_COLUMN_POINTS_DISCRETIONAL_CHANGE).Value = GUI_FormatNumber(Total.points_discretional_change, 0)

      ' Promotional points
      .Cell(idx_row, GRID_COLUMN_POINTS_PROMOTIONAL).Value = GUI_FormatNumber(Total.points_promotional, 0)

      ' Points expired
      .Cell(idx_row, GRID_COLUMN_POINTS_EXPIRED).Value = GUI_FormatNumber(Total.points_expired, 0)

      If Me.m_current_data Then

        ' Total Balance
        .Cell(idx_row, GRID_COLUMN_CURRENT_BALANCE).Value = GUI_FormatCurrency(Total.total_balance, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        ' Total Points
        .Cell(idx_row, GRID_COLUMN_CURRENT_POINTS).Value = GUI_FormatNumber(Total.total_points, 0)

      End If

      .Row(idx_row).BackColor = GetColor(Color)

    End With

    mdl_bucket_for_report.BucketsAfterLastRow(Me.Grid, idx_row, InitColumnGrid(), Total.level = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1047).ToUpper)

    Return True

  End Function ' DrawRow

  ' PURPOSE: Enable/disble controls of activity box
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub CheckActivityControls()

    If Me.rb_all.Checked Then
      Me.rb_without_maximum.Enabled = False
      Me.rb_maximum.Enabled = False
      Me.ef_days_activity.Enabled = False
    ElseIf Me.rb_activity.Enabled Then
      Me.rb_without_maximum.Enabled = True
      Me.rb_maximum.Enabled = True
      Me.ef_days_activity.Enabled = True
    End If

    If Me.rb_without_maximum.Checked Then
      Me.ef_days_activity.Enabled = False
    ElseIf Me.rb_maximum.Enabled Then
      Me.ef_days_activity.Enabled = True
    End If

  End Sub ' CheckActivityControls

  Private Sub ShowPlayPreferences()
    Dim _sel_id As Integer
    Dim _frm As frm_play_preferences
    Dim _from As Date
    Dim _to As Date

    If Date.TryParse(m_date_from, _from) And Date.TryParse(m_date_to, _to) Then

      If Me.Grid.SelectedRows.Length > 0 And Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_ACCOUNT).Value <> "" Then
        _sel_id = Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_ACCOUNT).Value
        _frm = New frm_play_preferences()
        _frm.ShowForEdit(Me.MdiParent, _sel_id, _from, True, _to, True)
      End If

    End If

  End Sub

  ' PURPOSE: Print a PDF file with Player data
  '     
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  Private Sub PrintData()
    Dim _account_id As Integer
    Dim _player As CardData
    Dim _frm As frm_print_account_info
    Dim _holder_name As String
    Dim _print As Boolean
    Dim _include_scanned_documents As Boolean

    _player = New CardData()

    Try
      If Me.Grid.SelectedRows.Length > 0 AndAlso _
         Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_ACCOUNT).Value <> "" Then

        _account_id = Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_ACCOUNT).Value

        If CardData.DB_CardGetPersonalData(_account_id, _player) Then
          _holder_name = _player.AccountId & " - " & _player.PlayerTracking.HolderName
          _frm = New frm_print_account_info(_holder_name)
          Call _frm.Show(_print, _include_scanned_documents)

          If Not _print Then
            Return
          End If
          Call PrintPlayerData(_include_scanned_documents, _player)

        End If

      End If

    Catch ex As Exception

      Log.Exception(ex)
    End Try

  End Sub ' PrintData

  Private Function IsValidDataRow(ByVal IdxRow As Integer) As Boolean

    If IdxRow >= 0 And IdxRow < Me.Grid.NumRows Then
      ' All rows with data have a non-empty value in column ACCOUNT.
      Return Not String.IsNullOrEmpty(Me.Grid.Cell(IdxRow, GRID_COLUMN_ACCOUNT).Value)
    End If

    Return False
  End Function ' IsValidDataRow

  Private Function IsNotAnonymous(ByVal IdxRow As Integer) As Boolean

    ' All anonymous cards have empty value in column LEVEL
    Return Not String.IsNullOrEmpty(Me.Grid.Cell(IdxRow, GRID_COLUMN_LEVEL).Value)

  End Function ' IsNotAnonymous

  ' PURPOSE: Initializes the account flag grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub InitializeGridSelectFlags()

    Call Me.dg_select_flag.Init(GRID_SELECT_FLAG_COLUMNS, GRID_SELECT_FLAG_HEADER_ROWS, True)

    ' FLAG_ID ( NOT VISIBLE )
    With Me.dg_select_flag.Column(GRID_SELECT_FLAG_IDX)
      .Width = 0
      .IsColumnPrintable = False
    End With

    ' FLAG COLOR
    With Me.dg_select_flag.Column(GRID_SELECT_FLAG_COLOR)
      .Width = 200
      .HighLightWhenSelected = False
    End With

    ' FLAG CHECKED
    With Me.dg_select_flag.Column(GRID_SELECT_FLAG_CHECKED)
      .Header.Text = ""
      .WidthFixed = 300
      .Fixed = True
      .Editable = True
      .EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
    End With

    ' COLUMN_FLAG_NAME
    With Me.dg_select_flag.Column(GRID_SELECT_FLAG_NAME)
      .Width = 3000
      .Editable = False
      .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    End With

  End Sub ' InitializeGridSelectFlags

  ' PURPOSE : Populates the account flag grid
  '
  '  PARAMS :
  '     - INPUT : Grid as uc_grid
  '     - OUTPUT :
  '
  ' RETURNS :
  '
  Private Sub FillFlags(ByRef Grid As uc_grid)

    Dim _table As DataTable
    Dim _idx As Integer
    Dim _flag_color As Color
    Dim _flag_id As Integer
    Dim _has_previous_data As Boolean
    Dim _previous_unselected_flags As List(Of Int64)

    _previous_unselected_flags = New List(Of Int64)
    _table = CLASS_FLAG.GetAllFlags(True)

    ' Get previous unselected flags to keep flag selection
    If (Me.dg_select_flag.NumRows > 0) Then
      For _idx = 0 To dg_select_flag.NumRows - 1
        If Me.dg_select_flag.Cell(_idx, GRID_SELECT_FLAG_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED Then
          _flag_id = dg_select_flag.Cell(_idx, GRID_SELECT_FLAG_IDX).Value
          _previous_unselected_flags.Add(_flag_id)
        End If
      Next
      _has_previous_data = True
    End If

    Me.dg_select_flag.Clear()

    For _idx = 0 To _table.Rows.Count - 1

      Call dg_select_flag.AddRow()

      _flag_id = _table.Rows.Item(_idx).Item("FL_FLAG_ID")
      _flag_color = Color.FromArgb(_table.Rows.Item(_idx).Item("FL_COLOR"))

      Me.dg_select_flag.Cell(_idx, GRID_SELECT_FLAG_IDX).Value = _flag_id

      ' Trick that allow set cell backcolor as Black
      Call ProcessColor(_flag_color)
      Me.dg_select_flag.Cell(_idx, GRID_SELECT_FLAG_COLOR).BackColor = _flag_color
      Me.dg_select_flag.Cell(_idx, GRID_SELECT_FLAG_COLOR).Value = String.Empty
      Me.dg_select_flag.Cell(_idx, GRID_SELECT_FLAG_NAME).Value = _table.Rows.Item(_idx).Item("FL_NAME").ToString()

      If _has_previous_data Then
        Me.dg_select_flag.Cell(_idx, GRID_SELECT_FLAG_CHECKED).Value = IIf(_previous_unselected_flags.Contains(_flag_id), _
                                                                              uc_grid.GRID_CHK_UNCHECKED, _
                                                                              uc_grid.GRID_CHK_CHECKED)
      Else
        Me.dg_select_flag.Cell(_idx, GRID_SELECT_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
      End If

    Next
  End Sub ' FillFlags

  ' PURPOSE : Reads account flag data from database
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS : DataTable (ID, Name)
  '
  Private Function DB_GetFlagsData() As DataTable

    Dim _table As DataTable
    Dim _str_bld As StringBuilder
    _str_bld = New StringBuilder()

    _str_bld.AppendLine("SELECT   FL_FLAG_ID ")
    _str_bld.AppendLine("       , FL_COLOR   ")
    _str_bld.AppendLine("       , FL_NAME    ")
    _str_bld.AppendLine("  FROM   FLAGS      ")
    _str_bld.AppendLine(" ORDER   BY FL_NAME ")

    _table = GUI_GetTableUsingSQL(_str_bld.ToString(), Int32.MaxValue)

    Return _table

  End Function ' DB_GetFlagsData

  ' PURPOSE : Trick to set cell backround color to Black
  '
  '  PARAMS :
  '     - INPUT : Color
  '     - OUTPUT : Color
  '
  ' RETURNS : 
  '
  Private Sub ProcessColor(ByRef FlagColor)

    If (FlagColor.A = Color.Black.A And _
        FlagColor.R = Color.Black.R And _
        FlagColor.G = Color.Black.G And _
        FlagColor.B = Color.Black.B) Then

      FlagColor = Color.FromArgb(FlagColor.A, FlagColor.R, FlagColor.G, FlagColor.B + 1)

    End If

  End Sub ' ProcessColor

  ' PURPOSE : Gets selected flag IDs from account flag filter
  '
  '  PARAMS :
  '     - INPUT : 
  '     - OUTPUT : 
  '
  ' RETURNS : String
  '
  Private Function GetSelectedFlagsId() As List(Of String)

    Dim _list As List(Of String)
    Dim _idx As Int32

    _list = New List(Of String)

    If Me.gb_flag.Enabled Then
      For _idx = 0 To Me.dg_select_flag.NumRows - 1

        If (Me.dg_select_flag.Cell(_idx, GRID_SELECT_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED) Then
          _list.Add(Me.dg_select_flag.Cell(_idx, GRID_SELECT_FLAG_IDX).Value)
        End If

      Next
    End If

    Return _list

  End Function ' GetSelectedFlagsId

  ' PURPOSE : Gets selected flag names from account flag filter
  '
  '  PARAMS :
  '     - INPUT : 
  '     - OUTPUT : 
  '
  ' RETURNS : String
  '
  Private Function GetSelectedFlagsTitles(Optional ByRef Separator As String = ",") As String

    Dim _idx As Int32
    Dim _str_bld As StringBuilder
    Dim _str_out As String
    Dim _flag_count As Integer

    _str_bld = New StringBuilder
    _flag_count = 0

    For _idx = 0 To (Me.dg_select_flag.NumRows - 1)
      If (Me.dg_select_flag.Cell(_idx, GRID_SELECT_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED) Then
        _str_bld.Append(Me.dg_select_flag.Cell(_idx, GRID_SELECT_FLAG_NAME).Value & ",")
        _flag_count += 1
      End If
    Next

    ' All or flag list
    _str_out = IIf(_flag_count = Me.dg_select_flag.NumRows, GLB_NLS_GUI_PLAYER_TRACKING.GetString(287), _str_bld.ToString())

    If (_str_out.Length > 0 AndAlso _str_out.EndsWith(",")) Then

      _str_out = _str_out.Substring(0, _str_out.Length - 1)

    End If

    Return _str_out

  End Function 'GetSelectedFlagsTitles

  ' PURPOSE: To set the color of flags grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - BackColor: To set the Back color of the cell
  '           - ForeColor: To set the Fore color of the cell
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetGridColor(ByVal BackColor As Drawing.Color, ByVal ForeColor As Drawing.Color)
    Dim _idx_row As Integer

    For _idx_row = 0 To Me.dg_select_flag.NumRows - 1
      Me.dg_select_flag.Row(_idx_row).BackColor = BackColor
      Me.dg_select_flag.Row(_idx_row).ForeColor = ForeColor
    Next
  End Sub ' SetGridColor

  ' PURPOSE : Enable/Disable the filters
  '
  '  PARAMS :
  '     - INPUT : Boolean indicating if it must enable (true) or disable (false) the filters
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - 
  Private Sub EnableFilters(ByVal EnableFilters As Boolean)
    Me.gb_activity.Enabled = EnableFilters
    Me.gb_level.Enabled = EnableFilters
    Me.gb_gender.Enabled = EnableFilters
    Me.chk_only_anonymous.Enabled = EnableFilters
    Me.gb_flag.Enabled = EnableFilters
    Me.gb_date.Enabled = EnableFilters

    If Not EnableFilters Then
      Me.rb_all.Checked = True
      Me.dg_select_flag.ClearSelection()
      Call UncheckAllFlags()

    End If

  End Sub ' EnableFilters

  Private Sub UncheckAllFlags()
    If (dg_select_flag.NumColumns > 0) Then
      Me.dg_select_flag.Clear()
      FillFlags(Me.dg_select_flag)
      handle_select_flags_Click(Me.btn_select_none_flags, Nothing)
      Call Me.SetGridColor(m_cell_enable_color, m_cell_text_enable_color)
    End If
  End Sub

  Private Sub RemoveChkWelcomeFilterHandlers()
    RemoveHandler chk_only_with_welcome_draw.CheckedChanged, AddressOf WelcomeDrawFilterHandler
    RemoveHandler chk_only_without_welcome_draw.CheckedChanged, AddressOf WelcomeDrawFilterHandler
  End Sub

  Private Sub AddChkWelcomeFilterHandlers()
    AddHandler chk_only_with_welcome_draw.CheckedChanged, AddressOf WelcomeDrawFilterHandler
    AddHandler chk_only_without_welcome_draw.CheckedChanged, AddressOf WelcomeDrawFilterHandler
  End Sub

#End Region

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region

#Region " Events "

  Private Sub chk_only_anonymous_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_only_anonymous.CheckedChanged
    gb_level.Enabled = Not chk_only_anonymous.Checked
    gb_gender.Enabled = Not chk_only_anonymous.Checked
    chk_holder_data.Enabled = Not chk_only_anonymous.Checked AndAlso CurrentUser.Permissions(ENUM_FORM.FORM_SHOW_HOLDER_DATA).Read
    If chk_only_anonymous.Checked Then
      chk_level_01.Checked = False
      chk_level_02.Checked = False
      chk_level_03.Checked = False
      chk_level_04.Checked = False

      chk_gender_male.Checked = False
      chk_gender_female.Checked = False
      chk_holder_data.Checked = False
    End If
  End Sub

  Private Sub chk_holder_data_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_holder_data.CheckedChanged
    m_change_col = True
  End Sub

  Private Sub chk_current_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_current.CheckedChanged
    m_change_col = True
  End Sub

  Private Sub chk_show_bucket_CheckedChanged(sender As Object, e As EventArgs) Handles chk_show_bucket.CheckedChanged
    m_change_col = True
  End Sub

  Private Sub rb_all_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_all.CheckedChanged
    Call CheckActivityControls()
  End Sub

  Private Sub rb_activity_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_activity.CheckedChanged
    Call CheckActivityControls()
  End Sub

  Private Sub rb_without_maximum_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_without_maximum.CheckedChanged
    Call CheckActivityControls()
  End Sub

  Private Sub rb_maximum_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_maximum.CheckedChanged
    Call CheckActivityControls()
  End Sub

  Private Sub uc_account_sel1_OneAccountFilterChanged() Handles uc_account_sel1.OneAccountFilterChanged
    Dim _enable_filters As Boolean

    _enable_filters = String.IsNullOrEmpty(Me.uc_account_sel1.Account) AndAlso Not uc_account_sel1.DisabledHolder
    Call EnableFilters(_enable_filters)

    If (_enable_filters And Me.uc_account_sel1.AccountText = String.Empty And Me.uc_account_sel1.TrackData = String.Empty) Then
      Call SetDefaultValues(False)
    End If

    If Me.uc_account_sel1.DisabledHolder _
        And Me.uc_account_sel1.MassiveSearchNumbers Is String.Empty Then
      Call SetGridColor(m_cell_disabled_color, m_cell_text_disabled_color)
    Else
      Call Me.SetGridColor(m_cell_enable_color, m_cell_text_enable_color)
    End If

  End Sub

  Private Sub handle_select_flags_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
              Handles btn_select_all_flags.Click, btn_select_none_flags.Click

    Dim _button As Button
    Dim _mark As String
    Dim idx_rows As Integer

    _button = CType(sender, Button)

    _mark = IIf(_button.Text = Me.btn_select_all_flags.Text, _
                uc_grid.GRID_CHK_CHECKED, _
                uc_grid.GRID_CHK_UNCHECKED)

    For idx_rows = 0 To Me.dg_select_flag.NumRows - 1
      Me.dg_select_flag.Cell(idx_rows, GRID_SELECT_FLAG_CHECKED).Value = _mark
    Next

  End Sub

  Private Sub chk_only_with_welcome_draw_CheckedChanged(sender As Object, e As EventArgs) Handles chk_only_with_welcome_draw.CheckedChanged
    Call RemoveChkWelcomeFilterHandlers()

    If Me.chk_only_with_welcome_draw.Checked Then
      Me.m_welcome_draw_filter = TERMINAL_DRAW_FILTER.ONLY_WITH
      Me.chk_only_without_welcome_draw.Checked = False
    End If

    Call WelcomeDrawFilterHandler()
    Call AddChkWelcomeFilterHandlers()
  End Sub

  Private Sub chk_only_without_welcome_draw_CheckedChanged(sender As Object, e As EventArgs) Handles chk_only_without_welcome_draw.CheckedChanged
    Call RemoveChkWelcomeFilterHandlers()

    If Me.chk_only_without_welcome_draw.Checked Then
      Me.m_welcome_draw_filter = TERMINAL_DRAW_FILTER.ONLY_WITHOUT
      Me.chk_only_with_welcome_draw.Checked = False
    End If

    Call WelcomeDrawFilterHandler()
    Call AddChkWelcomeFilterHandlers()
  End Sub

  Private Sub WelcomeDrawFilterHandler()

    If Not Me.chk_only_with_welcome_draw.Checked And Not Me.chk_only_without_welcome_draw.Checked Then
      Me.m_welcome_draw_filter = TERMINAL_DRAW_FILTER.NONE
    End If

  End Sub

#End Region ' Events

End Class
