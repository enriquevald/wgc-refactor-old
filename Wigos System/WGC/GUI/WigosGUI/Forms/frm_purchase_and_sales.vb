'-------------------------------------------------------------------
' Copyright � 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_purchase_and_sales
' DESCRIPTION:   Purchase and sales report
' AUTHOR:        Ferran Ortner
' CREATION DATE: 16-JUL-2015
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------------------------------   
' 16-JUL-2015  FOS    Initial version
' 06-AUG-2015  DCS    Refactoring and add last changes
' 27-AUG-2015  FOS    Add checks type operation
' 31-AUG-2015  FOS    Fixed bug 4008: Error when filter by num folio.
' 08-SEP-2015  YNM    Fixed bug 4261: Error when filter by num folio and voucher.
' 29-SEP-2015  FAV    Fixed bug 4261 (reopened): Error when filter by num folio and voucher.
' 15-OCT-2015  FAV    Fixed bug 4261: Error when filter by num folio and voucher. Error in Where clause
' 05-NOV-2015  MPO    Fixed bug 6139: buy/sales operation: "Recibo" column wrong formatted for excel
' 10-NOV-2015  GMV    Fixed bug 6288: Num folio filter/columns isn't necessary
' 02-FEB-2016  RGR    Product Backlog Item 7357: Table-45: Operation buy / sell tabs: Changes columns and filters
' 22-FEB-2016  ESE    Task 9698:7357 Operation buy/sell chips: Change columns and filters
' 26-APR-2016  RAB    Product Backlog Item 9758: Table (Phase 1): Review reports table
' 28-MAR-2016  EOR    Product Backlog Item 10981:Sprint Review 21 Winions - Mex
' 11-JUL-2016  RAB    Product Backlog Item 15066: GamingTables (Phase 4): Adapt reports to multicurrency.
' 21-JUL-2016  RAB    Product Backlog Item 15066: GamingTables (Phase 4): Adapt reports to multicurrency.
' 22-MAR-2017  ATB    Product Backlog Item 25951: Third TAX - Reports
' 03-APR-2017  FGB    Product Backlog Item 25737: Third TAX - Report columns
' 14-JUL-2017  RAB    PBI 28424: WIGOS-2957 Rounding - Reports - Add Remining amount in two reports
' 21-FEB-2018  EOR    Bug 31640: WIGOS-652 Missing 'Charge for service' column at Operations sales/purchases of chips report 
'--------------------------------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common
Imports System.Text
Imports System.Data.SqlClient

Public Class frm_purchase_and_sales
  Inherits frm_base_sel

#Region " Constants "

  Private Const GRID_TOTAL_COLUMNS As Integer = 17
  Private Const GRID_HEADER_ROWS As Integer = 2

  ' DB Columns
  Private Const SQL_COLUMN_OPERATION_ID As Integer = 0
  Private Const SQL_COLUMN_OPERATION_TYPE As Integer = 3
  Private Const SQL_COLUMN_DATETIME As Integer = 1
  Private Const SQL_COLUMN_CURRENCY_ISO_CODE As Integer = 60
  Private Const SQL_COLUMN_CAGE_CURRENCY_TYPE As Integer = 59
  Private Const SQL_COLUMN_PURCHASE_AMOUNT As Integer = 58
  Private Const SQL_COLUMN_SALES_AMOUNT As Integer = 57
  Private Const SQL_COLUMN_FEDERAL_TAXES As Integer = 12
  Private Const SQL_COLUMN_ESTATAL_TAXES As Integer = 13
  Private Const SQL_COLUMN_COUNCIL_TAXES As Integer = 68
  Private Const SQL_COLUMN_SERVICE_CHARGE As Integer = 31
  Private Const SQL_COLUMN_ROUND_DECIMALS As Integer = 32
  Private Const SQL_COLUMN_ROUND_RETENTION As Integer = 30
  Private Const SQL_COLUMN_PAY_NET As Integer = 9
  Private Const SQL_COLUMN_PRIZE As Integer = 11
  Private Const SQL_COLUMN_PURCHASE_AMOUNT_CURRENCY As Integer = 61
  Private Const SQL_COLUMN_SALES_AMOUNT_CURRENCY As Integer = 62
  Private Const SQL_COLUMN_RESIDUAL_AMOUNT As Integer = 69

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_OPERATION_ID As Integer = 1
  Private Const GRID_COLUMN_OPERATION_TYPE As Integer = 2
  Private Const GRID_COLUMN_DATETIME As Integer = 3
  Private Const GRID_COLUMN_PURCHASE_AMOUNT_CURRENCY As Integer = 4
  Private Const GRID_COLUMN_PURCHASE_AMOUNT As Integer = 5
  Private Const GRID_COLUMN_SALES_AMOUNT_CURRENCY As Integer = 6
  Private Const GRID_COLUMN_SALES_AMOUNT As Integer = 7
  Private Const GRID_COLUMN_RESIDUAL_AMOUNT As Integer = 8
  Private Const GRID_COLUMN_PRIZE As Integer = 9
  Private Const GRID_COLUMN_FEDERAL_TAXES As Integer = 10
  Private Const GRID_COLUMN_ESTATAL_TAXES As Integer = 11
  Private Const GRID_COLUMN_COUNCIL_TAXES As Integer = 12
  Private Const GRID_COLUMN_SERVICE_CHARGE As Integer = 13
  Private Const GRID_COLUMN_ROUND_DECIMALS As Integer = 14
  Private Const GRID_COLUMN_ROUND_RETENTION As Integer = 15
  Private Const GRID_COLUMN_PAY_NET As Integer = 16

  Private Const GRID_TOTAL_PURCHASE_AMOUNT_CURRENCY As Integer = 0
  Private Const GRID_TOTAL_PURCHASE_AMOUNT As Integer = 1
  Private Const GRID_TOTAL_SALES_AMOUNT_CURRENCY As Integer = 2
  Private Const GRID_TOTAL_SALES_AMOUNT As Integer = 3
  Private Const GRID_TOTAL_FEDERAL_TAXES As Integer = 4
  Private Const GRID_TOTAL_ESTATAL_TAXES As Integer = 5
  Private Const GRID_TOTAL_COUNCIL_TAXES As Integer = 6
  Private Const GRID_TOTAL_SERVICE_CHARGE As Integer = 7
  Private Const GRID_TOTAL_ROUND_DECIMALS As Integer = 8
  Private Const GRID_TOTAL_ROUND_RETENTION As Integer = 9
  Private Const GRID_TOTAL_PAY_NET As Integer = 10
  Private Const GRID_TOTAL_PRIZE As Integer = 11
  Private Const GRID_TOTAL_RESIDUAL_AMOUNT As Integer = 12

  Private Const QUANTITY_OF_TOTAL_AMOUNTS As Byte = 13

  ' Grid width
  Private Const GRID_COLUMN_WIDTH_TAX3 As Integer = 1800
#End Region ' Constants

#Region " Members "
  Private m_totals_list As List(Of Decimal)
  Private m_option_checked As String
  Private m_operation_id As String
  Private m_init_date_from As String
  Private m_init_date_to As String
  Private m_operations As String
  Private m_sequence_visible As Boolean
  Private m_selected_currency As String
#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_PURCHASE_AND_SALES_DETAIL

    Call MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId

  ' PURPOSE : Checks out the DB row before adding it to the grid
  '              and process counters (& totals rows)
  '
  '  PARAMS :
  '     - INPUT :
  '           - DataRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the data row can be added) or False (the data row can not be added)
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    'Checking empty rows
    If DbRow.Value(SQL_COLUMN_PURCHASE_AMOUNT) = 0 AndAlso DbRow.Value(SQL_COLUMN_SALES_AMOUNT) = 0 AndAlso _
       DbRow.Value(SQL_COLUMN_ROUND_DECIMALS) = 0 AndAlso DbRow.Value(SQL_COLUMN_PAY_NET) = 0 AndAlso _
       DbRow.Value(SQL_COLUMN_FEDERAL_TAXES) = 0 AndAlso DbRow.Value(SQL_COLUMN_PRIZE) = 0 Then

      Return False
    Else

      Return True
    End If

  End Function ' GUI_CheckOutRowBeforeAdd

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(3)

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_INFO).Visible = False

    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False

    ' Date time filter
    Me.uc_date.Init(GLB_NLS_GUI_INVOICING.Id(201), True)

    ' Voucher / sequence / date filter
    Me.opt_by_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(268)
    Me.opt_by_operation.Text = GLB_NLS_GUI_INVOICING.GetString(350)
    Me.ef_operation_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, 18)

    AddHandler Me.opt_by_date.CheckedChanged, AddressOf opt_CheckedChanged
    AddHandler Me.opt_by_operation.CheckedChanged, AddressOf opt_CheckedChanged

    ' Set filter default values
    Call SetDefaultValues()

    ' Grid
    Call GUI_StyleSheet()

    ' Operation types
    Me.gb_operation_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6685)
    Me.chk_purchase.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4472)
    Me.chk_purchase_anulation.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6684)
    Me.chk_sales.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1177)
    Me.chk_sales_anulation.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6683)

    'Set form title
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6579)

    m_selected_currency = String.Empty

  End Sub ' GUI_InitControls

  ' PURPOSE: Execute the command associated to the button ButtonId.
  '
  '  PARAMS:
  '     - INPUT:
  '           - ButtonId As ENUM_BUTTON
  '     - OUTPUT:
  '           -
  '
  ' RETURNS:
  '     -
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base.ENUM_BUTTON)
    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_FILTER_RESET
        Call SetDefaultValues()

      Case Else
        MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub ' GUI_ButtonClick

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_FilterReset

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()
    Dim _idx_totals As Byte

    m_totals_list = New List(Of Decimal)

    For _idx_totals = 1 To QUANTITY_OF_TOTAL_AMOUNTS
      m_totals_list.Add(0)
    Next

  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    Dim _idx_row As Integer

    Me.Grid.AddRow()
    _idx_row = Me.Grid.NumRows - 1

    Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

    Me.Grid.Cell(_idx_row, GRID_COLUMN_OPERATION_TYPE).Value = GLB_NLS_GUI_INVOICING.GetString(205)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_PURCHASE_AMOUNT_CURRENCY).Value = GUI_FormatNumber(m_totals_list(GRID_TOTAL_PURCHASE_AMOUNT_CURRENCY)) & " " & m_selected_currency
    Me.Grid.Cell(_idx_row, GRID_COLUMN_PURCHASE_AMOUNT).Value = GUI_FormatNumber(m_totals_list(GRID_TOTAL_PURCHASE_AMOUNT)) & " " & CurrencyExchange.GetNationalCurrency()
    Me.Grid.Cell(_idx_row, GRID_COLUMN_SALES_AMOUNT_CURRENCY).Value = GUI_FormatNumber(m_totals_list(GRID_TOTAL_SALES_AMOUNT_CURRENCY)) & " " & m_selected_currency
    Me.Grid.Cell(_idx_row, GRID_COLUMN_SALES_AMOUNT).Value = GUI_FormatNumber(m_totals_list(GRID_TOTAL_SALES_AMOUNT)) & " " & CurrencyExchange.GetNationalCurrency()
    Me.Grid.Cell(_idx_row, GRID_COLUMN_RESIDUAL_AMOUNT).Value = GUI_FormatNumber(m_totals_list(GRID_TOTAL_RESIDUAL_AMOUNT)) & " " & CurrencyExchange.GetNationalCurrency()
    Me.Grid.Cell(_idx_row, GRID_COLUMN_PRIZE).Value = GUI_FormatNumber(m_totals_list(GRID_TOTAL_PRIZE)) & " " & CurrencyExchange.GetNationalCurrency()
    Me.Grid.Cell(_idx_row, GRID_COLUMN_FEDERAL_TAXES).Value = GUI_FormatNumber(m_totals_list(GRID_TOTAL_FEDERAL_TAXES)) & " " & CurrencyExchange.GetNationalCurrency()
    Me.Grid.Cell(_idx_row, GRID_COLUMN_ESTATAL_TAXES).Value = GUI_FormatNumber(m_totals_list(GRID_TOTAL_ESTATAL_TAXES)) & " " & CurrencyExchange.GetNationalCurrency()
    Me.Grid.Cell(_idx_row, GRID_COLUMN_COUNCIL_TAXES).Value = GUI_FormatNumber(m_totals_list(GRID_TOTAL_COUNCIL_TAXES)) & " " & CurrencyExchange.GetNationalCurrency()
    Me.Grid.Cell(_idx_row, GRID_COLUMN_SERVICE_CHARGE).Value = GUI_FormatNumber(m_totals_list(GRID_TOTAL_SERVICE_CHARGE)) & " " & CurrencyExchange.GetNationalCurrency()
    Me.Grid.Cell(_idx_row, GRID_COLUMN_ROUND_DECIMALS).Value = GUI_FormatNumber(m_totals_list(GRID_TOTAL_ROUND_DECIMALS)) & " " & CurrencyExchange.GetNationalCurrency()
    Me.Grid.Cell(_idx_row, GRID_COLUMN_ROUND_RETENTION).Value = GUI_FormatNumber(m_totals_list(GRID_TOTAL_ROUND_RETENTION)) & " " & CurrencyExchange.GetNationalCurrency()
    Me.Grid.Cell(_idx_row, GRID_COLUMN_PAY_NET).Value = GUI_FormatNumber(m_totals_list(GRID_TOTAL_PAY_NET)) & " " & CurrencyExchange.GetNationalCurrency()

    'Third tax column must be shown when Third tax is active, or there are record with third data
    SetThirdColumnWidth(GRID_COLUMN_COUNCIL_TAXES, (GetThirdTaxIsActive() OrElse (m_totals_list(GRID_TOTAL_COUNCIL_TAXES) > 0)))
  End Sub ' GUI_AfterLastRow

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    If opt_by_date.Checked Then
      ' Dates selection 
      If Me.uc_date.ToDateSelected Then
        If Me.uc_date.FromDate > Me.uc_date.ToDate Then
          Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
          Call Me.uc_date.Focus()

          Return False
        End If
      End If

    ElseIf (Me.opt_by_operation.Checked And ef_operation_id.Value() = "") Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.opt_by_operation.Text)
      Me.ef_operation_id.Focus()

      Return False
    End If

    Return True
  End Function 'GUI_FilterCheck

  ' PURPOSE: Get the defined Query Type
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_QUERY_TYPE
  '
  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE

    Return ENUM_QUERY_TYPE.CUSTOM_DATATABLE

  End Function ' GUI_GetQueryType

  ' PURPOSE: Get customized datatable for show in grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - DataTable
  '
  Protected Overrides Function GUI_GetCustomDataTable() As System.Data.DataTable
    Dim _dt As DataTable
    Dim _dv As DataView
    Dim _date_to As DateTime
    Dim _query_str As String

    'EOR 28-MAR-2016
    Dim _query_type_operations As String

    _dt = Nothing
    _dv = Nothing

    _query_str = String.Empty
    _query_type_operations = String.Empty

    If opt_by_date.Checked Then
      ' Filter by date

      If (String.IsNullOrEmpty(m_init_date_to)) Then
        _date_to = WGDB.Now
        m_init_date_to = GUI_FormatDate(_date_to, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
        Call WSI.Common.SharedBL02Common.GetSalesAndPaymentsBetweenDatesDirect(Me.uc_date.FromDate.AddHours(Me.uc_date.ClosingTime), _date_to, _dt)
      Else
        Call WSI.Common.SharedBL02Common.GetSalesAndPaymentsBetweenDatesDirect(Me.uc_date.FromDate.AddHours(Me.uc_date.ClosingTime), Me.uc_date.ToDate.AddHours(Me.uc_date.ClosingTime), _dt)
      End If
    Else
      ' Filter by Voucher or sequence
      Call WSI.Common.SharedBL02Common.GetSalesAndPaymentsByOperationListDirect(GetOperation(), _dt)
    End If

    'Set local selected currency
    m_selected_currency = uc_multicurrency.SelectedCurrency

    'Filter to selected iso code
    _dv = _dt.DefaultView
    _dv.RowFilter = "CurrencyIsoCode in ('" + m_selected_currency + "')"
    _dt = _dv.ToTable()

    If chk_purchase.Checked Then
      If Not String.IsNullOrEmpty(_query_str) Then
        _query_str += " or "
      End If

      _query_str += " FichasABalance > 0 "
    End If

    If chk_sales.Checked Then
      If Not String.IsNullOrEmpty(_query_str) Then
        _query_str += " or "
      End If

      _query_str += " BalanceAFichas > 0 "
    End If

    If chk_purchase_anulation.Checked Then
      If Not String.IsNullOrEmpty(_query_str) Then
        _query_str += " or "
      End If

      _query_str += " FichasABalance < 0 "
    End If

    If chk_sales_anulation.Checked Then
      If Not String.IsNullOrEmpty(_query_str) Then
        _query_str += " or "
      End If

      _query_str += " BalanceAFichas < 0 "
    End If

    'EOR 28-MAR-2016 Typ Operation for Chips Swap
    If GeneralParam.GetInt32("TITA", "Enabled") <> 0 Then
      _query_type_operations = " TipoOperacion IN ('13','14','15','16','141') "
    Else
      _query_type_operations = " TipoOperacion IN ('13','14','15','16') "
    End If

    If String.IsNullOrEmpty(_query_str) Then
      _query_str = _query_type_operations
    Else
      _query_str = _query_type_operations + " And (" + _query_str + ")"
    End If

    _dt = New DataView(_dt, _query_str, String.Empty, DataViewRowState.CurrentRows).ToTable

    Return _dt

  End Function ' GUI_GetCustomDataTable

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    If m_option_checked = opt_by_date.Text Then
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_AUDITOR.GetString(257), m_init_date_from)
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_AUDITOR.GetString(258), m_init_date_to)
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6685), m_operations)
    Else
      PrintData.SetFilter(m_option_checked, m_operation_id)
    End If

    ' Currency type
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7382), m_selected_currency)
  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()
    m_init_date_from = ""
    m_init_date_to = ""
    m_option_checked = ""
    m_operation_id = ""
    m_operations = ""

    If opt_by_date.Checked Then
      ' Init Date From
      m_init_date_from = GUI_FormatDate(Me.uc_date.FromDate.AddHours(Me.uc_date.ClosingTime), _
                                        ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

      ' Init Date To
      If Me.uc_date.ToDateSelected Then
        m_init_date_to = GUI_FormatDate(Me.uc_date.ToDate.AddHours(Me.uc_date.ClosingTime), _
                                        ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      End If

      m_option_checked = opt_by_date.Text
    Else
      m_option_checked = opt_by_operation.Text
      m_operation_id = ef_operation_id.TextValue
    End If

    If chk_purchase.Checked AndAlso chk_purchase_anulation.Checked AndAlso chk_sales.Checked AndAlso chk_sales_anulation.Checked Then
      m_operations = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353)
    Else
      If chk_purchase.Checked Then
        If Not String.IsNullOrEmpty(m_operations) Then
          m_operations += " , "
        End If

        m_operations += GLB_NLS_GUI_PLAYER_TRACKING.GetString(4472)
      End If

      If chk_sales.Checked Then
        If Not String.IsNullOrEmpty(m_operations) Then
          m_operations += " , "
        End If

        m_operations += GLB_NLS_GUI_PLAYER_TRACKING.GetString(1177)
      End If

      If chk_purchase_anulation.Checked Then
        If Not String.IsNullOrEmpty(m_operations) Then
          m_operations += " , "
        End If

        m_operations += GLB_NLS_GUI_PLAYER_TRACKING.GetString(6684)
      End If

      If chk_sales_anulation.Checked Then
        If Not String.IsNullOrEmpty(m_operations) Then
          m_operations += " , "
        End If

        m_operations += GLB_NLS_GUI_PLAYER_TRACKING.GetString(6683)
      End If

      If chk_purchase.Checked AndAlso chk_purchase_anulation.Checked AndAlso chk_sales.Checked AndAlso chk_sales_anulation.Checked Then
        m_operations = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353)
      End If
    End If

  End Sub ' GUI_ReportUpdateFilters

  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(ExcelData)

    ExcelData.SetColumnFormat(0, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.DATE)

  End Sub ' GUI_ReportParams

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _is_anulation As Boolean
    Dim _anulation_prefix As String

    _is_anulation = False
    _anulation_prefix = String.Empty

    Call CalculateTotal(DbRow)

    ' Operation ID
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_OPERATION_ID)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_ID).Value = DbRow.Value(SQL_COLUMN_OPERATION_ID).ToString()
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_ID).Value = String.Empty
    End If

    ' Date and time
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_DATETIME)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATETIME), _
                                                                                      ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                      ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DATETIME).Value = String.Empty
    End If

    ' Purchase amount currency
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_PURCHASE_AMOUNT_CURRENCY)) AndAlso DbRow.Value(SQL_COLUMN_PURCHASE_AMOUNT_CURRENCY) <> 0 Then
      If DbRow.Value(SQL_COLUMN_PURCHASE_AMOUNT_CURRENCY) < 0 Then
        _is_anulation = True
      End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PURCHASE_AMOUNT_CURRENCY).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_PURCHASE_AMOUNT_CURRENCY)) & " " & m_selected_currency
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PURCHASE_AMOUNT_CURRENCY).Value = String.Empty
    End If

    ' Purchase amount
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_PURCHASE_AMOUNT)) AndAlso DbRow.Value(SQL_COLUMN_PURCHASE_AMOUNT) <> 0 Then
      If DbRow.Value(SQL_COLUMN_PURCHASE_AMOUNT) < 0 Then
        _is_anulation = True
      End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PURCHASE_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PURCHASE_AMOUNT), CurrencySymbol:=False) & " " & CurrencyExchange.GetNationalCurrency()
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PURCHASE_AMOUNT).Value = String.Empty
    End If

    ' Sales amount currency
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_SALES_AMOUNT_CURRENCY)) AndAlso DbRow.Value(SQL_COLUMN_SALES_AMOUNT_CURRENCY) <> 0 Then
      If DbRow.Value(SQL_COLUMN_SALES_AMOUNT_CURRENCY) < 0 Then
        _is_anulation = True
      End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SALES_AMOUNT_CURRENCY).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_SALES_AMOUNT_CURRENCY)) & " " & m_selected_currency
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SALES_AMOUNT_CURRENCY).Value = String.Empty
    End If

    ' Sales amount
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_SALES_AMOUNT)) AndAlso DbRow.Value(SQL_COLUMN_SALES_AMOUNT) <> 0 Then
      If DbRow.Value(SQL_COLUMN_SALES_AMOUNT) < 0 Then
        _is_anulation = True
      End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SALES_AMOUNT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_SALES_AMOUNT)) & " " & CurrencyExchange.GetNationalCurrency()
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SALES_AMOUNT).Value = String.Empty
    End If

    ' Residual amount
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_RESIDUAL_AMOUNT)) AndAlso DbRow.Value(SQL_COLUMN_RESIDUAL_AMOUNT) <> 0 Then
      If DbRow.Value(SQL_COLUMN_RESIDUAL_AMOUNT) < 0 Then
        _is_anulation = True
      End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_RESIDUAL_AMOUNT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_RESIDUAL_AMOUNT)) & " " & CurrencyExchange.GetNationalCurrency()
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_RESIDUAL_AMOUNT).Value = String.Empty
    End If

    ' Prize amount
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_PRIZE)) AndAlso DbRow.Value(SQL_COLUMN_PRIZE) <> 0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PRIZE).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_PRIZE)) & " " & CurrencyExchange.GetNationalCurrency()
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PRIZE).Value = String.Empty
    End If

    ' Federal Tax
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_FEDERAL_TAXES)) AndAlso DbRow.Value(SQL_COLUMN_FEDERAL_TAXES) <> 0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_FEDERAL_TAXES).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_FEDERAL_TAXES)) & " " & CurrencyExchange.GetNationalCurrency()
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_FEDERAL_TAXES).Value = String.Empty
    End If

    ' State tax
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_ESTATAL_TAXES)) AndAlso DbRow.Value(SQL_COLUMN_ESTATAL_TAXES) <> 0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ESTATAL_TAXES).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_ESTATAL_TAXES)) & " " & CurrencyExchange.GetNationalCurrency()
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ESTATAL_TAXES).Value = String.Empty
    End If

    ' Council tax
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_COUNCIL_TAXES)) AndAlso DbRow.Value(SQL_COLUMN_COUNCIL_TAXES) <> 0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_COUNCIL_TAXES).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_COUNCIL_TAXES)) & " " & CurrencyExchange.GetNationalCurrency()
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_COUNCIL_TAXES).Value = String.Empty
    End If

    'Service Charge
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_SERVICE_CHARGE)) AndAlso DbRow.Value(SQL_COLUMN_SERVICE_CHARGE) <> 0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SERVICE_CHARGE).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_SERVICE_CHARGE)) & " " & CurrencyExchange.GetNationalCurrency()
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SERVICE_CHARGE).Value = String.Empty
    End If

    ' Decimal round
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_ROUND_DECIMALS)) AndAlso DbRow.Value(SQL_COLUMN_ROUND_DECIMALS) <> 0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ROUND_DECIMALS).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_ROUND_DECIMALS)) & " " & CurrencyExchange.GetNationalCurrency()
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ROUND_DECIMALS).Value = String.Empty
    End If

    ' Round retention
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_ROUND_RETENTION)) AndAlso DbRow.Value(SQL_COLUMN_ROUND_RETENTION) <> 0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ROUND_RETENTION).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_ROUND_RETENTION)) & " " & CurrencyExchange.GetNationalCurrency()
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ROUND_RETENTION).Value = String.Empty
    End If

    ' Pay net
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_PAY_NET)) AndAlso DbRow.Value(SQL_COLUMN_PAY_NET) <> 0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PAY_NET).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_PAY_NET)) & " " & CurrencyExchange.GetNationalCurrency()
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PAY_NET).Value = String.Empty
    End If

    ' Operation Type
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_OPERATION_TYPE)) Then
      Dim _operation_type_str As String
      _operation_type_str = String.Empty

      If _is_anulation = True Then
        _anulation_prefix = "(" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2786) & ") "
      End If

      _operation_type_str = TranslateOperationType(DbRow.Value(SQL_COLUMN_OPERATION_TYPE).ToString(), DbRow.Value(SQL_COLUMN_CAGE_CURRENCY_TYPE).ToString(), DbRow.Value(SQL_COLUMN_CURRENCY_ISO_CODE).ToString())

      Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_TYPE).Value = _anulation_prefix & _operation_type_str
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_OPERATION_TYPE).Value = String.Empty
    End If

    Return True
  End Function ' GUI_SetupRow

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    Dim _column_width As Integer
    _column_width = 0

    With Me.Grid

      Call .Init(GRID_TOTAL_COLUMNS, GRID_HEADER_ROWS)

      ' Index
      GUI_AddColumn(Me.Grid, GRID_COLUMN_INDEX, " ", 150, , " ", False)

      ' Operation ID
      GUI_AddColumn(Me.Grid, GRID_COLUMN_OPERATION_ID, GLB_NLS_GUI_INVOICING.GetString(350), 1100, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1407))

      ' Operation Type
      GUI_AddColumn(Me.Grid, GRID_COLUMN_OPERATION_TYPE, GLB_NLS_GUI_INVOICING.GetString(350), 5200, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6601))

      ' Date and time
      GUI_AddColumn(Me.Grid, GRID_COLUMN_DATETIME, GLB_NLS_GUI_INVOICING.GetString(350), 2200, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6602))

      ' Purchases currency
      GUI_AddColumn(Me.Grid, GRID_COLUMN_PURCHASE_AMOUNT_CURRENCY, GLB_NLS_GUI_INVOICING.GetString(287), 1500, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5668))

      ' Purchases
      GUI_AddColumn(Me.Grid, GRID_COLUMN_PURCHASE_AMOUNT, GLB_NLS_GUI_INVOICING.GetString(287), 1500, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6603))

      ' Sales currency
      GUI_AddColumn(Me.Grid, GRID_COLUMN_SALES_AMOUNT_CURRENCY, GLB_NLS_GUI_INVOICING.GetString(287), 1500, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5668))

      ' Sales
      GUI_AddColumn(Me.Grid, GRID_COLUMN_SALES_AMOUNT, GLB_NLS_GUI_INVOICING.GetString(287), 1500, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6604))

      ' Residual amount
      If (GamingTableBusinessLogic.IsEnableRoundingSaleChips()) Then
        _column_width = 1500
      End If

      GUI_AddColumn(Me.Grid, GRID_COLUMN_RESIDUAL_AMOUNT, GLB_NLS_GUI_INVOICING.GetString(287), _column_width, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8477))

      ' Prize
      GUI_AddColumn(Me.Grid, GRID_COLUMN_PRIZE, GLB_NLS_GUI_INVOICING.GetString(287), 1500, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6608))

      ' Federal tax
      GUI_AddColumn(Me.Grid, GRID_COLUMN_FEDERAL_TAXES, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6605), 1800, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GetCashierData("Tax.OnPrize.1.Name"))

      ' Statal tax
      GUI_AddColumn(Me.Grid, GRID_COLUMN_ESTATAL_TAXES, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6605), 1800, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GetCashierData("Tax.OnPrize.2.Name"))

      ' Council tax
      GUI_AddColumn(Me.Grid, GRID_COLUMN_COUNCIL_TAXES, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6605), 1800, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GetCashierData("Tax.OnPrize.3.Name"))
      'Third tax column must be shown when Third tax is active, or there are record with third data
      SetThirdColumnWidth(GRID_COLUMN_COUNCIL_TAXES, GetThirdTaxIsActive())

      ' Service Charge
      GUI_AddColumn(Me.Grid, GRID_COLUMN_SERVICE_CHARGE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6605), 1800, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1127))

      ' Decimal round
      GUI_AddColumn(Me.Grid, GRID_COLUMN_ROUND_DECIMALS, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6606), 2250, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1126))

      ' Retention round
      GUI_AddColumn(Me.Grid, GRID_COLUMN_ROUND_RETENTION, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6606), 2250, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_INVOICING.GetString(483))

      ' Pay net
      GUI_AddColumn(Me.Grid, GRID_COLUMN_PAY_NET, "", 1500, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_INVOICING.GetString(432))
    End With

  End Sub 'GUI_StyleSheet

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()
    Dim _now As DateTime
    Dim _initial_time As DateTime
    Dim _closing_time As Integer

    _now = WGDB.Now
    _closing_time = GetDefaultClosingTime()
    _initial_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)

    If _initial_time > _now Then
      _initial_time = _initial_time.AddDays(-1)
    End If

    Me.uc_date.ClosingTime = _closing_time
    Me.uc_date.FromDate = _initial_time
    Me.uc_date.ToDate = Me.uc_date.FromDate.AddDays(1)
    Me.uc_date.ToDateSelected = True
    Me.opt_by_date.Checked = True

    Me.ef_operation_id.Value = String.Empty

    Me.chk_purchase.Checked = True
    Me.chk_purchase_anulation.Checked = True
    Me.chk_sales.Checked = True
    Me.chk_sales_anulation.Checked = True

    Me.m_sequence_visible = GeneralParam.GetBoolean("Cashier.Voucher", "Voucher.A.ShowSequence") Or GeneralParam.GetBoolean("Cashier.Voucher", "Voucher.B.ShowSequence")

    Call opt_CheckedChanged()

    'uc_multi_currency_site_sel
    uc_multicurrency.GroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7382)
    uc_multicurrency.OpenModeControl = uc_multi_currency_site_sel.OPEN_MODE.OnlyCurrency
    Me.uc_multicurrency.Init()

  End Sub ' SetDefaultValues

  ' PURPOSE: Calculate grid total amounts
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub CalculateTotal(ByVal DbRow As CLASS_DB_ROW)

    m_totals_list(GRID_TOTAL_PURCHASE_AMOUNT_CURRENCY) += DbRow.Value(SQL_COLUMN_PURCHASE_AMOUNT_CURRENCY)
    m_totals_list(GRID_TOTAL_PURCHASE_AMOUNT) += DbRow.Value(SQL_COLUMN_PURCHASE_AMOUNT)
    m_totals_list(GRID_TOTAL_SALES_AMOUNT_CURRENCY) += DbRow.Value(SQL_COLUMN_SALES_AMOUNT_CURRENCY)
    m_totals_list(GRID_TOTAL_SALES_AMOUNT) += DbRow.Value(SQL_COLUMN_SALES_AMOUNT)
    m_totals_list(GRID_TOTAL_FEDERAL_TAXES) += DbRow.Value(SQL_COLUMN_FEDERAL_TAXES)
    m_totals_list(GRID_TOTAL_ESTATAL_TAXES) += DbRow.Value(SQL_COLUMN_ESTATAL_TAXES)
    m_totals_list(GRID_TOTAL_COUNCIL_TAXES) += DbRow.Value(SQL_COLUMN_COUNCIL_TAXES)
    m_totals_list(GRID_TOTAL_SERVICE_CHARGE) += DbRow.Value(SQL_COLUMN_SERVICE_CHARGE)
    m_totals_list(GRID_TOTAL_ROUND_DECIMALS) += DbRow.Value(SQL_COLUMN_ROUND_DECIMALS)
    m_totals_list(GRID_TOTAL_ROUND_RETENTION) += DbRow.Value(SQL_COLUMN_ROUND_RETENTION)
    m_totals_list(GRID_TOTAL_PAY_NET) += DbRow.Value(SQL_COLUMN_PAY_NET)
    m_totals_list(GRID_TOTAL_PRIZE) += DbRow.Value(SQL_COLUMN_PRIZE)
    m_totals_list(GRID_TOTAL_RESIDUAL_AMOUNT) += DbRow.Value(SQL_COLUMN_RESIDUAL_AMOUNT)

  End Sub ' CalculateTotal

  ' PURPOSE: Gets the movement name and options of each kind of movement type
  '
  '  PARAMS:
  '     - INPUT:
  '           - operation_type: the Code of the operation
  '           - CageCurrencyType: Type of currency
  '           - CurrencyIsoCode: Code of the iso currency
  '     - OUTPUT:
  '           - unrecognized_movement: true if the movement doesn't have a personalized title
  '
  ' RETURNS:
  '     - A string with the name of the operation type
  Private Function TranslateOperationType(ByVal operation_type As Integer, ByVal CageCurrencyType As String, ByVal CurrencyIsoCode As String) As String
    Dim _str_type As String
    Dim _chip_description As String

    _str_type = ""
    _chip_description = FeatureChips.GetChipTypeDescription(CType(CageCurrencyType, CurrencyExchangeType), CurrencyIsoCode)

    Select Case operation_type
      'EOR 28-MAR-2016 Add Operation Code Chips Swap
      Case WSI.Common.OperationCode.CHIPS_SALE, WSI.Common.OperationCode.CHIPS_SWAP
        _str_type = IIf(_chip_description <> "", GLB_NLS_GUI_PLAYER_TRACKING.GetString(7282, _chip_description), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6600))

      Case WSI.Common.OperationCode.CHIPS_SALE_WITH_RECHARGE
        _str_type = IIf(_chip_description <> "", GLB_NLS_GUI_PLAYER_TRACKING.GetString(7283, _chip_description), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6607))

      Case WSI.Common.OperationCode.CHIPS_PURCHASE
        _str_type = IIf(_chip_description <> "", GLB_NLS_GUI_PLAYER_TRACKING.GetString(7284, _chip_description), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6610))

      Case WSI.Common.OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT
        _str_type = IIf(_chip_description <> "", GLB_NLS_GUI_PLAYER_TRACKING.GetString(7285, _chip_description), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6611))

        ' LA 12-04-2016
      Case WSI.Common.OperationCode.PROMOTION_WITH_TAXES
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7272)

      Case WSI.Common.OperationCode.PRIZE_PAYOUT
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7273)

      Case WSI.Common.OperationCode.PRIZE_PAYOUT_AND_RECHARGE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7274)

    End Select
    Return _str_type
  End Function 'TranslateOperationType

  ' PURPOSE: Gets the operation id from a voucher id or sequence
  '
  '  PARAMS:
  '     - INPUT:
  '           
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Operation ID
  Private Function GetOperation() As String
    Dim _sb As StringBuilder
    Dim _dt_result As DataTable
    Dim _operation_ids As String
    Dim _tita_enabled As Boolean

    _operation_ids = String.Empty

    _sb = New StringBuilder
    _sb.AppendLine(" SELECT   CV_OPERATION_ID                     ")
    _sb.AppendLine("   FROM   CASHIER_VOUCHERS                    ")
    _sb.AppendLine("  WHERE   CV_OPERATION_ID =  @pOperationID    ")

    'EOR 28-MAR-2016
    _tita_enabled = (GeneralParam.GetInt32("TITA", "Enabled") <> 0)

    If _tita_enabled Then
      _sb.AppendLine("    AND   CV_TYPE IN (@pVoucherType_CashInA, @pVoucherType_CashOutA, @pVoucherType_ChipsBuy, @pVoucherType_ChipsSale, @pVoucherType_ChipsSwap)")
    Else
      _sb.AppendLine("    AND   CV_TYPE IN (@pVoucherType_CashInA, @pVoucherType_CashOutA, @pVoucherType_ChipsBuy, @pVoucherType_ChipsSale)")
    End If

    Try
      Using _db_trx As New DB_TRX()
        Using _cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _cmd.Parameters.Add("@pOperationID", SqlDbType.BigInt).Value = m_operation_id
          _cmd.Parameters.Add("@pVoucherType_CashInA", SqlDbType.Int).Value = CashierVoucherType.CashIn_A
          _cmd.Parameters.Add("@pVoucherType_CashOutA", SqlDbType.Int).Value = CashierVoucherType.CashOut_A
          _cmd.Parameters.Add("@pVoucherType_ChipsBuy", SqlDbType.Int).Value = CashierVoucherType.ChipsBuy
          _cmd.Parameters.Add("@pVoucherType_ChipsSale", SqlDbType.Int).Value = CashierVoucherType.ChipsSale

          If _tita_enabled Then
            _cmd.Parameters.Add("@pVoucherType_ChipsSwap", SqlDbType.Int).Value = CashierVoucherType.ChipsSwap
          End If

          _dt_result = GUI_GetTableUsingCommand(_cmd, 100)

        End Using
      End Using

      For Each _dr As DataRow In _dt_result.Select()
        _operation_ids += _dr(0) & ","
      Next

      _operation_ids = _operation_ids.TrimEnd(",")
    Catch ex As Exception

    End Try

    If (String.IsNullOrEmpty(_operation_ids)) Then
      _operation_ids = m_operation_id
    End If

    Return _operation_ids

  End Function ' GetOperation

  ''' <summary>
  ''' Is third tax is active
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetThirdTaxIsActive() As Boolean
    Dim _third_tax_pct As Decimal
    Decimal.TryParse(GetCashierData("Tax.OnPrize.3.Pct"), _third_tax_pct)

    Return (_third_tax_pct > 0)
  End Function

  ''' <summary>
  ''' Set width of third tax column 
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SetThirdColumnWidth(ThirdTaxColumnIndex As Integer, ShowThirdTaxColumn As Boolean)
    If ShowThirdTaxColumn Then
      Me.Grid.Column(ThirdTaxColumnIndex).Fixed = False
      Me.Grid.Column(ThirdTaxColumnIndex).Width = GRID_COLUMN_WIDTH_TAX3
    Else
      Me.Grid.Column(ThirdTaxColumnIndex).Width = 0
    End If
  End Sub

#End Region ' Private Functions

#Region " Events "

  ' PURPOSE: Generic Handler for radio button options sequence / voucher / date
  '          Enable or disable entry fieald and user control date
  '
  '  PARAMS:
  '     - INPUT:
  '           - 
  '     - OUTPUT:
  '           - 
  '
  ' RETURNS:
  '     - 
  Private Sub opt_CheckedChanged()
    Me.uc_date.Enabled = opt_by_date.Checked
    Me.ef_operation_id.Enabled = opt_by_operation.Checked
    Me.ef_operation_id.Value = String.Empty

    If opt_by_operation.Checked Then
      chk_purchase.Enabled = False
      chk_sales.Enabled = False
      chk_purchase_anulation.Enabled = False
      chk_sales_anulation.Enabled = False
      gb_operation_type.Enabled = False
    Else
      chk_purchase.Enabled = True
      chk_sales.Enabled = True
      chk_purchase_anulation.Enabled = True
      chk_sales_anulation.Enabled = True
      gb_operation_type.Enabled = True
    End If

    If opt_by_date.Checked Then
      m_option_checked = opt_by_date.Text
    ElseIf opt_by_operation.Checked Then
      m_option_checked = opt_by_operation.Text
    End If

  End Sub ' opt_CheckedChanged

#End Region ' Events
End Class