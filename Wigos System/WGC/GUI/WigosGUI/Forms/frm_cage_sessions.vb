'-------------------------------------------------------------------
' Copyright � 2007-2009 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_cage_sessions
'
' DESCRIPTION:   This screen allows to view cage sessions:
'                           - between two dates 
'                           - for one or all cashiers
'                           - for one or all users
'                           - for one or all movements types
'
' AUTHOR:        Jes�s �ngel Blanco Blanco
'
' CREATION DATE: 16-DEC-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 13-DEC-2013  JAB    Initial version.
' 19-FEB-2014  FBA    Fixed bug WIG-652
' 24-MAR-2014  CCG    Fixed Bug WIGOSTITO-1164: Reorder currency columns to put in first place the national currency.
' 20-MAR-2014  CCG-JCA Fixed bug WIGOSTITO-1158: No show TITO tickets or Bank Card in money columns
' 09-ABR-2014  JCA     Fixed bug WIG-817: Its not possible to close last vault session
' 25-APR-2014  DHA & JML Rename TITO Cashier/Account Movements
' 29-APR-2014  JCA & DHA Fixed Bug WIG-871: last cage session can be closed if any terminal sesion is open with played tickets
' 01-SEP-2014  JBC    Fixed Bug WIG-1222: Can do new movements in closed sessions. 
' 04-SEP-2014  LRS    Added functionality to reopen sessions.
' 23-SEP-2014  RRR    Added Cage Meters functionalities
' 30-SEP-2014  LRS    Fixed Bug WIG-1339
' 30-SEP-2014  LRS    Fixed Bug WIG-1339 (new specification)
' 10-OCT-2014  JAB    Fixed Bug WIG-1191: Cage movements canceled should not be counted.
' 17-NOV-2014  RRR    Fixed Bug WIG-1693: Incorrect multi-session cage report values
' 05-DIC-2014  LRS    Fixed Bug WIG-1812: Can save CHIPS
' 05-DIC-2014  LRS    Fixed Bug WIG-1813: Change currency grids
' 10-FEB-2015  OPC    Fixed Bug WIG-1902: Added new permission.
' 09-APR-2015  RMS    Gaming Day
' 12-MAY-2015  JBC    Fixed Bug WIG-2285: GamingDay
' 12-MAY-2015  JBC    Fixed Bug WIG-2289: GamingDay
' 28-MAY-2015  TPF    Fixed Bug WIG-2394: Change vault session date 
' 14-SEP-2015  ETP    Fixed Bug 3824: Fixed close one session if another session is opened.
' 03-DEC-2015  JPJ    Fixed Bug 6764: MXN Collection was only taken into account.
' 08-MAR-2016  FAV    Fixed Bug 10373, 10376, 10365: Fixed value showed in Deposits and Withdrawals
' 26-APR-2016  FOS    Fixed Bug 12340: exchange currency values appear on national currency values
' 24-NOV-2016  ATB    Bug 20049: Cage Sessions: the request and cancellation from cashier it's accounted as deposit and withdrawal
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations
Imports WSI.Common
Imports GUI_CommonMisc
Imports System.Text
Imports System.Data.SqlClient

Public Class frm_cage_sessions
  Inherits frm_base_sel

#Region " MEMBERS "

  Public m_is_tito_mode As Boolean

  Private mc_ses_detail As cls_cage_session_detail

  Private m_sessions_totals As DataTable

  ' For report filters 
  Private m_filter_user_type As String
  Private m_filter_user As String
  Private m_filter_date_type As String
  Private m_filter_date_from As String
  Private m_filter_date_to As String
  Private m_filter_session_status As List(Of String)
  Private m_from_date As Date
  Private m_to_date As Date

  Private m_selection_mode As Boolean
  Public m_cage_session_id As Long
  Public m_cage_sessions_in_grid As String
  Public m_cage_session_name As String
  Public m_cage_session_status As Int32
  Public m_first_session_name As String
  Public m_last_session_name As String

  Private m_gaming_day As Boolean = False

#End Region

#Region " CONSTANTS "

  ' DB Columns
  Private Const SQL_COLUMN_CGS_CAGE_SESSION_ID As Integer = 0
  Private Const SQL_COLUMN_CGS_OPEN_USER_ID As Integer = 1
  Private Const SQL_COLUMN_CGS_OPEN_USER_NAME As Integer = 2
  Private Const SQL_COLUMN_CGS_OPEN_DATETIME As Integer = 3
  Private Const SQL_COLUMN_CGS_CLOSE_USER_ID As Integer = 4
  Private Const SQL_COLUMN_CGS_CLOSE_USER_NAME As Integer = 5
  Private Const SQL_COLUMN_CGS_CLOSE_DATETIME As Integer = 6
  Private Const SQL_COLUMN_CGS_STATUS As Integer = 7
  Private Const SQL_COLUMN_CGS_SESSION_NAME As Integer = 8
  Private Const SQL_COLUMN_CGS_WORKING_DAY As Integer = 9

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_CGS_CAGE_SESSION_ID As Integer = 1
  Private Const GRID_COLUMN_CGS_OPEN_USER_ID As Integer = 2
  Private Const GRID_COLUMN_CGS_SESSION_NAME As Integer = 3
  Private Const GRID_COLUMN_CGS_STATUS As Integer = 4
  Private Const GRID_COLUMN_CGS_OPEN_USER_NAME As Integer = 5
  Private Const GRID_COLUMN_CGS_WORKING_DAY As Integer = 6
  Private Const GRID_COLUMN_CGS_OPEN_DATETIME As Integer = 7
  Private Const GRID_COLUMN_CGS_CLOSE_USER_ID As Integer = 8
  Private Const GRID_COLUMN_CGS_CLOSE_USER_NAME As Integer = 9
  Private Const GRID_COLUMN_CGS_CLOSE_DATETIME As Integer = 10
  Private Const GRID_COLUMN_CGS_STATUS_ID As Integer = 11

  ' Grid Width
  Private Const WIDTH_GRID_COLUMN_INDEX As Integer = 200
  Private Const WIDTH_GRID_COLUMN_CGS_CAGE_SESSION_ID As Integer = 0
  Private Const WIDTH_GRID_COLUMN_CGS_OPEN_USER_ID As Integer = 0
  Private Const WIDTH_GRID_COLUMN_CGS_SESSION_NAME As Integer = 3725
  Private Const WIDTH_GRID_COLUMN_CGS_OPEN_USER_NAME As Integer = 3750
  Private Const WIDTH_GRID_COLUMN_CGS_OPEN_DATETIME As Integer = 1700
  Private Const WIDTH_GRID_COLUMN_CGS_WORKING_DAY As Integer = 1700
  Private Const WIDTH_GRID_COLUMN_CGS_CLOSE_USER_ID As Integer = 0
  Private Const WIDTH_GRID_COLUMN_CGS_CLOSE_USER_NAME As Integer = 3750
  Private Const WIDTH_GRID_COLUMN_CGS_CLOSE_DATETIME As Integer = 1700
  Private Const WIDTH_GRID_COLUMN_CGS_STATUS As Integer = 300
  Private Const WIDTH_GRID_COLUMN_CGS_CURRENCIES As Integer = 1550

  Private Const GRID_COLUMNS As Integer = 12
  Private Const GRID_HEADER_ROWS As Integer = 2

  ' m_sessons_totals columns
  Private Const SQL_COLUMN_CMD_ISO_CODE As Integer = 1
  Private Const SQL_COLUMN_CMD_DEPOSITS As Integer = 2
  Private Const SQL_COLUMN_CMD_WITHDRAWALS As Integer = 3
  Private Const SQL_COLUMN_CMD_DIFFERENCE As Integer = 4

#End Region

#Region " OVERRIDES "

  ' PURPOSE: Set the internal Form Id
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CAGE_SESSIONS

    'Call Base Form proc
    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    Call MyBase.GUI_InitControls()

    mc_ses_detail = New cls_cage_session_detail()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3379) ' Sesiones de B�veda.

    Me.m_is_tito_mode = TITO.Utils.IsTitoMode()

    Me.Grid.SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(1)   ' Salir

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_INFO).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_INFO).Text = GLB_NLS_GUI_INVOICING.GetString(2)     ' Movimientos
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_INFO).Enabled = False

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = CurrentUser.Permissions(ENUM_FORM.FORM_CAGE_SESSIONS_DETAIL).Read
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Text = GLB_NLS_GUI_INVOICING.GetString(5)   ' Detalle
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Enabled = False

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4352) ' Arqueo de b�veda
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Type = uc_button.ENUM_BUTTON_TYPE.USER
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Size = New System.Drawing.Size(90, 45)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = Not GLB_CurrentUser.IsSuperUser

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4561) ' Stock de b�veda
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Type = uc_button.ENUM_BUTTON_TYPE.USER
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Size = New System.Drawing.Size(90, 45)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = Not GLB_CurrentUser.IsSuperUser

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3426) ' Cerrar sesi�n
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Type = uc_button.ENUM_BUTTON_TYPE.USER
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Size = New System.Drawing.Size(90, 45)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = Not GLB_CurrentUser.IsSuperUser

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_3).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_3).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3425) ' Abrir sesi�n
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_3).Type = uc_button.ENUM_BUTTON_TYPE.USER
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_3).Size = New System.Drawing.Size(90, 45)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_3).Enabled = Not GLB_CurrentUser.IsSuperUser

    ' RRR 18-AUG-2014
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_4).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_4).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5568) '"Informe Global"
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_4).Type = uc_button.ENUM_BUTTON_TYPE.USER
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_4).Size = New System.Drawing.Size(90, 45)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_4).Enabled = False

    ' RRR 22-SEP-2014
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_5).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_5).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5539) '"Informe de Sesi�n"
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_5).Type = uc_button.ENUM_BUTTON_TYPE.USER
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_5).Size = New System.Drawing.Size(90, 45)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_5).Enabled = False

    ' Date
    Me.gb_date.Text = GLB_NLS_GUI_INVOICING.GetString(201)
    Me.opt_opening_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2101)  ' Buscar por apertura de sesi�n
    Me.opt_closing_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3402)  ' Buscar por cierre de sesi�n
    Me.opt_working_day.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5723)   ' Buscar por jornada
    Me.opt_opening_user.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3403)  ' Buscar por usuario de apertura
    Me.opt_closing_user.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3404)  ' Buscar por usuario de cierre
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Users 
    Me.gb_user.Text = GLB_NLS_GUI_INVOICING.GetString(220)
    Me.opt_one_user.Text = GLB_NLS_GUI_INVOICING.GetString(218)
    Me.opt_all_users.Text = GLB_NLS_GUI_INVOICING.GetString(219)
    Me.chk_show_all.Text = GLB_NLS_GUI_CONFIGURATION.GetString(90)

    ' State 
    fra_session_state.Text = GLB_NLS_GUI_INVOICING.GetString(152)
    chk_session_open.Text = GLB_NLS_GUI_INVOICING.GetString(153)
    chk_session_closed.Text = GLB_NLS_GUI_INVOICING.GetString(154)

    ' Grid
    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

    ' Set combo with Users
    Call SetCombo(Me.cmb_user, "SELECT GU_USER_ID, GU_USERNAME FROM GUI_USERS WHERE GU_BLOCK_REASON = " & _
                  WSI.Common.GUI_USER_BLOCK_REASON.NONE & " ORDER BY GU_USERNAME")
    Me.cmb_user.Enabled = False
    Me.chk_show_all.Enabled = False

    ' Is selection mode
    If Me.m_selection_mode Then
      Call Me.SetLayoutForSelectionMode()
    End If

    ' Gaming Day
    Me.m_gaming_day = WSI.Common.Misc.IsGamingDayEnabled()

  End Sub ' GUI_InitControls

  ' PURPOSE: Process button actions in order to branch to a child screen
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)
    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY
        Call GetSessionsTotals()
        Call MyBase.GUI_ButtonClick(ButtonId)

        Me.GetCageSessionId(False)

        'If Me.m_cage_session_id <> -1 Then
        Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_4).Enabled = Me.m_cage_session_id <> -1
        Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_5).Enabled = Me.m_cage_session_id <> -1
        'End If

      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        If (CurrentUser.Permissions(ENUM_FORM.FORM_CAGE_SESSIONS_DETAIL).Read) Then
          If Not Me.m_selection_mode Then
            Call ShowCageBalance()
          Else ' Select a cash cage session
            Call Me.GetCageSessionId(True)
          End If
        End If

      Case frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0
        Call GUI_CountCageSession()


      Case frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1
        Call ShowCageBalance(True)

      Case frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2
        If Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3426) Then
          Call GUI_CloseCageSession()
        Else ' si el texto no es cerrar reabre
          Call GUI_ReOpenCageSession()
        End If

      Case frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_3
        If Not Permissions.Write Then
          Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

          Exit Sub
        End If
        Call GUI_OpenCageSession()

        ' RRR 22-SEP-2014
      Case frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_4
        Call Me.ShowCageSelectionGlobalReport()

        ' RRR 18-AUG-2014
      Case frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_5
        Call Me.ShowCageSessionGlobalReport()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  ' 
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _str_sql As StringBuilder

    ' Sessions
    _str_sql = New System.Text.StringBuilder()
    _str_sql.AppendLine("    SELECT   cgs_cage_session_id                                                                                         ")
    _str_sql.AppendLine("           , cgs_open_user_id                                                                                            ")
    _str_sql.AppendLine("           , (SELECT  (gu_full_name + ' [' + gu_username + ']')                                                          ")
    _str_sql.AppendLine("                       FROM  gui_users                                                                                   ")
    _str_sql.AppendLine("                      WHERE  gui_users.gu_user_id = cgs_open_user_id) AS cgs_open_user_name                              ")
    _str_sql.AppendLine("           , cgs_open_datetime                                                                                           ")
    _str_sql.AppendLine("           , cgs_close_user_id                                                                                           ")
    _str_sql.AppendLine("           , (SELECT  (gu_full_name + ' [' + gu_username + ']')                                                          ")
    _str_sql.AppendLine("                       FROM  gui_users                                                                                   ")
    _str_sql.AppendLine("                      WHERE  gui_users.gu_user_id = cgs_close_user_id) AS cgs_close_user_name                            ")
    _str_sql.AppendLine("           , cgs_close_datetime                                                                                          ")
    _str_sql.AppendLine("           , CASE                                                                                                        ")
    _str_sql.AppendLine("               WHEN cgs_close_datetime IS NULL                                                                           ")
    _str_sql.AppendLine("                 THEN 0                                                                                                  ")
    _str_sql.AppendLine("                 ELSE 1                                                                                                  ")
    _str_sql.AppendLine("               END AS cgs_status                                                                                         ")
    _str_sql.AppendLine("           , CGS_SESSION_NAME                                                                                            ")
    _str_sql.AppendLine("           , CGS_WORKING_DAY                                                                                             ")
    _str_sql.AppendLine("      FROM   cage_sessions                                                                                               ")
    _str_sql.AppendLine(GetSqlWhereSessions())

    ' Status values.
    ' CASHIER_SESSION_STATUS.OPEN   --> 0
    ' CASHIER_SESSION_STATUS.CLOSED --> 1

    Return _str_sql.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  '
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _column_offset As Int32

    ' Session Id
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CGS_CAGE_SESSION_ID).Value = DbRow.Value(SQL_COLUMN_CGS_CAGE_SESSION_ID)

    ' CGS_OPEN_USER_ID
    If Not DbRow.IsNull(SQL_COLUMN_CGS_OPEN_USER_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CGS_OPEN_USER_ID).Value = DbRow.Value(SQL_COLUMN_CGS_OPEN_USER_ID)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CGS_OPEN_USER_ID).Value = ""
    End If

    ' CGS_SESSION_NAME
    If Not DbRow.IsNull(SQL_COLUMN_CGS_SESSION_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CGS_SESSION_NAME).Value = DbRow.Value(SQL_COLUMN_CGS_SESSION_NAME).ToString()
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CGS_SESSION_NAME).Value = ""
    End If

    ' CGS_OPEN_USER_NAME
    If Not DbRow.IsNull(SQL_COLUMN_CGS_OPEN_USER_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CGS_OPEN_USER_NAME).Value = DbRow.Value(SQL_COLUMN_CGS_OPEN_USER_NAME).ToString()
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CGS_OPEN_USER_NAME).Value = ""
    End If

    ' CGS_WORKING_DAY
    If Not DbRow.IsNull(SQL_COLUMN_CGS_WORKING_DAY) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CGS_WORKING_DAY).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_CGS_WORKING_DAY), _
                                                                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                   ENUM_FORMAT_TIME.FORMAT_TIME_NONE)

    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CGS_WORKING_DAY).Value = ""
    End If

    ' CGS_OPEN_DATETIME
    If Not DbRow.IsNull(SQL_COLUMN_CGS_OPEN_DATETIME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CGS_OPEN_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_CGS_OPEN_DATETIME), _
                                                                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                   ENUM_FORMAT_TIME.FORMAT_HHMM)

    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CGS_OPEN_DATETIME).Value = ""
    End If

    ' CGS_CLOSE_USER_ID
    If Not DbRow.IsNull(SQL_COLUMN_CGS_CLOSE_USER_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CGS_CLOSE_USER_ID).Value = DbRow.Value(SQL_COLUMN_CGS_CLOSE_USER_ID)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CGS_CLOSE_USER_ID).Value = ""
    End If

    ' CGS_CLOSE_USER_NAME
    If Not DbRow.IsNull(SQL_COLUMN_CGS_CLOSE_USER_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CGS_CLOSE_USER_NAME).Value = DbRow.Value(SQL_COLUMN_CGS_CLOSE_USER_NAME).ToString()
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CGS_CLOSE_USER_NAME).Value = ""
    End If

    ' CGS_CLOSE_DATETIME
    If Not DbRow.IsNull(SQL_COLUMN_CGS_CLOSE_DATETIME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CGS_CLOSE_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_CGS_CLOSE_DATETIME), _
                                                                                    ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                    ENUM_FORMAT_TIME.FORMAT_HHMM)

    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CGS_CLOSE_DATETIME).Value = ""
    End If

    ' Status

    If Not DbRow.IsNull(SQL_COLUMN_CGS_STATUS) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CGS_STATUS_ID).Value = DbRow.Value(SQL_COLUMN_CGS_STATUS)
      Select Case DbRow.Value(SQL_COLUMN_CGS_STATUS)
        Case WSI.Common.CASHIER_SESSION_STATUS.OPEN
          Me.Grid.Cell(RowIndex, GRID_COLUMN_CGS_STATUS).Value = GLB_NLS_GUI_INVOICING.GetString(156) ' A
          Me.Grid.Cell(RowIndex, GRID_COLUMN_CGS_STATUS).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
          Me.Grid.Cell(RowIndex, GRID_COLUMN_INDEX).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)


        Case WSI.Common.CASHIER_SESSION_STATUS.CLOSED
          Me.Grid.Cell(RowIndex, GRID_COLUMN_CGS_STATUS).Value = GLB_NLS_GUI_INVOICING.GetString(157) ' C
      End Select
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CGS_STATUS).Value = ""
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CGS_STATUS_ID).Value = -1000
    End If


    ' Add currency exchange columns

    If Not mc_ses_detail.m_currency_exchange_iso_codes Is Nothing Then
      ' Currencies
      _column_offset = GRID_COLUMNS
      For _index_session As Int32 = 0 To m_sessions_totals.Rows.Count - 1
        If m_sessions_totals.Rows(_index_session).Item(SQL_COLUMN_CGS_CAGE_SESSION_ID) = DbRow.Value(SQL_COLUMN_CGS_CAGE_SESSION_ID) Then
          For _index As Int32 = 0 To mc_ses_detail.m_currency_exchange_iso_codes.Count() - 1
            _column_offset = GRID_COLUMNS + (_index * 3)

            If m_sessions_totals.Rows(_index_session).Item(SQL_COLUMN_CMD_ISO_CODE) = mc_ses_detail.m_currency_exchange_iso_codes(_index) Then
              Me.Grid.Cell(RowIndex, _column_offset).Value = mc_ses_detail.FormatAmount(m_sessions_totals.Rows(_index_session).Item(SQL_COLUMN_CMD_DEPOSITS), mc_ses_detail.m_currency_exchange_iso_codes(_index))
              _column_offset += 1
              Me.Grid.Cell(RowIndex, _column_offset).Value = mc_ses_detail.FormatAmount(m_sessions_totals.Rows(_index_session).Item(SQL_COLUMN_CMD_WITHDRAWALS), mc_ses_detail.m_currency_exchange_iso_codes(_index))
              _column_offset += 1
              Me.Grid.Cell(RowIndex, _column_offset).Value = mc_ses_detail.FormatAmount(m_sessions_totals.Rows(_index_session).Item(SQL_COLUMN_CMD_DIFFERENCE), mc_ses_detail.m_currency_exchange_iso_codes(_index))
              _column_offset += 1
            End If

          Next
        End If
      Next




    End If

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()

  End Sub ' GUI_FilterReset

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.FilterHeaderWidth(1) = 2000
    PrintData.SetFilter(gb_date.Text, m_filter_date_type)
    PrintData.SetFilter(gb_date.Text & " " & dtp_from.Text, m_filter_date_from)
    PrintData.SetFilter(gb_date.Text & " " & dtp_to.Text, m_filter_date_to)

    PrintData.FilterHeaderWidth(2) = 2000
    PrintData.SetFilter(gb_user.Text, m_filter_user_type)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(220), m_filter_user)

    PrintData.FilterValueWidth(3) = 4000
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(152), String.Join(",", m_filter_session_status.ToArray()))

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_filter_user_type = ""
    m_filter_user = ""
    m_filter_date_type = ""
    m_filter_date_from = ""
    m_filter_date_to = ""
    m_filter_session_status = New List(Of String)

    m_from_date = Nothing
    m_to_date = Nothing

    'Date 
    If opt_opening_date.Checked Then
      m_filter_date_type = opt_opening_date.Text
    ElseIf opt_closing_date.Checked Then
      m_filter_date_type = opt_closing_date.Text
    ElseIf opt_working_day.Checked Then
      m_filter_date_type = opt_working_day.Text
    End If
    If Me.dtp_from.Checked Then
      m_from_date = dtp_from.Value
      If opt_working_day.Checked Then
        m_filter_date_from = GUI_FormatDate(dtp_from.Value, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
      Else
        m_filter_date_from = GUI_FormatDate(dtp_from.Value, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      End If
    End If
    If Me.dtp_to.Checked Then
      m_to_date = dtp_to.Value
      If opt_working_day.Checked Then
        m_filter_date_to = GUI_FormatDate(dtp_to.Value, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
      Else
        m_filter_date_to = GUI_FormatDate(dtp_to.Value, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      End If
    End If

    ' Users
    If opt_opening_user.Checked Then
      m_filter_user_type = opt_opening_user.Text
    ElseIf opt_closing_user.Checked Then
      m_filter_user_type = opt_closing_user.Text
    End If
    If Me.opt_all_users.Checked Then
      m_filter_user = Me.opt_all_users.Text
    Else
      m_filter_user = Me.cmb_user.TextValue
    End If

    'State
    If Me.chk_session_open.Checked = True Then
      m_filter_session_status.Add(GLB_NLS_GUI_INVOICING.GetString(153))
    End If
    If chk_session_closed.Checked = True Then
      m_filter_session_status.Add(GLB_NLS_GUI_INVOICING.GetString(154))
    End If

  End Sub ' GUI_ReportUpdateFilters

  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(ExcelData)

    ExcelData.SetColumnFormat(3, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT) ' Working day

  End Sub ' GUI_ReportParams

  ' PURPOSE: Call to child window to show details for the selected row
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ShowSelectedItem()

    If Me.Grid Is Nothing OrElse Me.Grid.NumRows = 0 Then
      Return
    End If

    Dim _idx_row As Integer
    Dim _session_id As Int64
    Dim _frm_cage_movements_sel As frm_cage_movements_sel
    Dim _session_closed As Boolean

    _session_closed = False

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    ' Protect totalizator row
    ' TODO: Poner cuando est� el registro de totales
    '''If Not IsValidDataRow(idx_row) Then
    '''  Return
    '''End If

    ' Session
    If Me.Grid.Cell(_idx_row, GRID_COLUMN_CGS_CAGE_SESSION_ID).Value = "" Then
      _session_id = 0
    Else
      _session_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_CGS_CAGE_SESSION_ID).Value
    End If

    Select Case Me.Grid.Cell(_idx_row, GRID_COLUMN_CGS_STATUS).Value
      Case GLB_NLS_GUI_INVOICING.GetString(156)
        _session_closed = False
      Case GLB_NLS_GUI_INVOICING.GetString(157)
        _session_closed = True
      Case Else
        _session_closed = False
    End Select

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm_cage_movements_sel = New frm_cage_movements_sel
    _frm_cage_movements_sel.ShowForEdit(Me.MdiParent, _session_id, _session_closed)

  End Sub ' GUI_ShowSelectedItem

  ' PURPOSE: Change text button_custom_2 to reopen session
  '
  '  PARAMS:
  '     - INPUT:
  '           - SelectedRow
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_RowSelectedEvent(ByVal SelectedRow As Integer)

    If Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled Then ' podria mirar si es superusuario, pero si en el futuro cambia la condicion seria un lio, mejor comprobar enabled

      If IsNumeric(Me.Grid.Cell(SelectedRow, GRID_COLUMN_CGS_STATUS_ID).Value) AndAlso Me.Grid.Cell(SelectedRow, GRID_COLUMN_CGS_STATUS_ID).Value = CASHIER_SESSION_STATUS.CLOSED Then
        ' pongo texto de reapertura
        Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5388)
      Else
        ' pongo texto original
        Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3426) ' Cerrar sesi�n
      End If

    End If


  End Sub
#End Region

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent

    Me.Display(False)

  End Sub ' ShowForEdit

  ' PURPOSE: Opens dialog with selection mode settings
  '
  '  PARAMS:
  '     - INPUT:
  '           - CageSessionId
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Public Sub ShowForEdit(ByRef CageSessionId As Long)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.m_selection_mode = True

    Me.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedSingle
    Me.MinimizeBox = False

    Me.Display(True)

    CageSessionId = Me.m_cage_session_id

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub GUI_StyleSheet()
    Dim _total_columns As Integer
    Dim _column_offset As Integer

    _total_columns = GRID_COLUMNS

    If Not mc_ses_detail.m_currency_exchange_iso_codes Is Nothing Then
      ' Three new columns for each iso code: Expected, collected and difference
      _total_columns += (mc_ses_detail.m_currency_exchange_iso_codes.Count() * 3)

      Call Me.ReorderIsoCodeList()
    End If

    With Me.Grid
      Call .Init(_total_columns, GRID_HEADER_ROWS)

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = WIDTH_GRID_COLUMN_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Open Session Id
      .Column(GRID_COLUMN_CGS_CAGE_SESSION_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_CGS_CAGE_SESSION_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_CGS_CAGE_SESSION_ID).Width = WIDTH_GRID_COLUMN_CGS_CAGE_SESSION_ID
      .Column(GRID_COLUMN_CGS_CAGE_SESSION_ID).IsColumnPrintable = False

      ' Open User Id
      .Column(GRID_COLUMN_CGS_OPEN_USER_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(629) ' Sesi�n de Caja.
      .Column(GRID_COLUMN_CGS_OPEN_USER_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_CGS_OPEN_USER_ID).Width = WIDTH_GRID_COLUMN_CGS_OPEN_USER_ID
      .Column(GRID_COLUMN_CGS_OPEN_USER_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Session name
      .Column(GRID_COLUMN_CGS_SESSION_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(629)
      .Column(GRID_COLUMN_CGS_SESSION_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4594) ' Session Name
      .Column(GRID_COLUMN_CGS_SESSION_NAME).Width = WIDTH_GRID_COLUMN_CGS_SESSION_NAME
      .Column(GRID_COLUMN_CGS_SESSION_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Open User Name
      .Column(GRID_COLUMN_CGS_OPEN_USER_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(629)
      .Column(GRID_COLUMN_CGS_OPEN_USER_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4450) ' Opening user
      .Column(GRID_COLUMN_CGS_OPEN_USER_NAME).Width = WIDTH_GRID_COLUMN_CGS_OPEN_USER_NAME
      .Column(GRID_COLUMN_CGS_OPEN_USER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Working Day
      .Column(GRID_COLUMN_CGS_WORKING_DAY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(629)
      .Column(GRID_COLUMN_CGS_WORKING_DAY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5723) ' Working day
      .Column(GRID_COLUMN_CGS_WORKING_DAY).Width = WIDTH_GRID_COLUMN_CGS_WORKING_DAY
      .Column(GRID_COLUMN_CGS_WORKING_DAY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Open Date
      .Column(GRID_COLUMN_CGS_OPEN_DATETIME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(629)
      .Column(GRID_COLUMN_CGS_OPEN_DATETIME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4452) ' Opening date
      .Column(GRID_COLUMN_CGS_OPEN_DATETIME).Width = WIDTH_GRID_COLUMN_CGS_OPEN_DATETIME
      .Column(GRID_COLUMN_CGS_OPEN_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Close User Id
      .Column(GRID_COLUMN_CGS_CLOSE_USER_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(629)
      .Column(GRID_COLUMN_CGS_CLOSE_USER_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_CGS_CLOSE_USER_ID).Width = WIDTH_GRID_COLUMN_CGS_CLOSE_USER_ID
      .Column(GRID_COLUMN_CGS_CLOSE_USER_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Close User Name
      .Column(GRID_COLUMN_CGS_CLOSE_USER_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(629)
      .Column(GRID_COLUMN_CGS_CLOSE_USER_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4451) ' Closing user
      .Column(GRID_COLUMN_CGS_CLOSE_USER_NAME).Width = WIDTH_GRID_COLUMN_CGS_CLOSE_USER_NAME
      .Column(GRID_COLUMN_CGS_CLOSE_USER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Close Date
      .Column(GRID_COLUMN_CGS_CLOSE_DATETIME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(629)
      .Column(GRID_COLUMN_CGS_CLOSE_DATETIME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4453) ' Closing date
      .Column(GRID_COLUMN_CGS_CLOSE_DATETIME).Width = WIDTH_GRID_COLUMN_CGS_CLOSE_DATETIME
      .Column(GRID_COLUMN_CGS_CLOSE_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' State
      .Column(GRID_COLUMN_CGS_STATUS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(629)
      .Column(GRID_COLUMN_CGS_STATUS).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(155)
      .Column(GRID_COLUMN_CGS_STATUS).Width = WIDTH_GRID_COLUMN_CGS_STATUS
      .Column(GRID_COLUMN_CGS_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_COLUMN_CGS_STATUS_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(629)
      .Column(GRID_COLUMN_CGS_STATUS_ID).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(155)
      .Column(GRID_COLUMN_CGS_STATUS_ID).Width = 0
      .Column(GRID_COLUMN_CGS_STATUS_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER


      If Not mc_ses_detail.m_currency_exchange_iso_codes Is Nothing Then
        _column_offset = 0

        ' Add currency exchange columns
        For _index As Integer = 0 To mc_ses_detail.m_currency_exchange_iso_codes.Count() - 1
          ' Expected column
          .Column(GRID_COLUMNS + _column_offset).Header(0).Text = mc_ses_detail.m_currency_exchange_iso_description(_index).ToString() & _
                                                                  " [" & mc_ses_detail.m_currency_exchange_iso_codes(_index).ToString() & "]"
          .Column(GRID_COLUMNS + _column_offset).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(496)                  ' Dep�sitos
          .Column(GRID_COLUMNS + _column_offset).Width = WIDTH_GRID_COLUMN_CGS_CURRENCIES
          .Column(GRID_COLUMNS + _column_offset).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          _column_offset += 1

          ' Collected column
          .Column(GRID_COLUMNS + _column_offset).Header(0).Text = mc_ses_detail.m_currency_exchange_iso_description(_index).ToString() & _
                                                                  " [" & mc_ses_detail.m_currency_exchange_iso_codes(_index).ToString() & "]"
          .Column(GRID_COLUMNS + _column_offset).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(497)                  ' Retiros
          .Column(GRID_COLUMNS + _column_offset).Width = WIDTH_GRID_COLUMN_CGS_CURRENCIES
          .Column(GRID_COLUMNS + _column_offset).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          _column_offset += 1

          ' Difference column          
          .Column(GRID_COLUMNS + _column_offset).Header(0).Text = mc_ses_detail.m_currency_exchange_iso_description(_index).ToString() & _
                                                                  " [" & mc_ses_detail.m_currency_exchange_iso_codes(_index).ToString() & "]"
          .Column(GRID_COLUMNS + _column_offset).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(499)            ' Diferencia
          .Column(GRID_COLUMNS + _column_offset).Width = WIDTH_GRID_COLUMN_CGS_CURRENCIES
          .Column(GRID_COLUMNS + _column_offset).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          _column_offset += 1

        Next
      End If

    End With

    Me.Grid.Width += System.Windows.Forms.SystemInformation.VerticalScrollBarWidth

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Reorder the Iso Code/Description list to put in first position the national currency.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub ReorderIsoCodeList()
    Dim _currency_iso_code As String
    Dim _idx As Integer
    Dim _temp As String

    _currency_iso_code = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode", "MXN")

    For _idx = 0 To mc_ses_detail.m_currency_exchange_iso_codes.Count - 1
      If mc_ses_detail.m_currency_exchange_iso_codes(_idx).Equals(_currency_iso_code) Then
        ' Switch IsoCodes
        _temp = mc_ses_detail.m_currency_exchange_iso_codes(0)
        mc_ses_detail.m_currency_exchange_iso_codes(0) = mc_ses_detail.m_currency_exchange_iso_codes(_idx)
        mc_ses_detail.m_currency_exchange_iso_codes(_idx) = _temp

        'Switch Descriptions
        _temp = mc_ses_detail.m_currency_exchange_iso_description(0)
        mc_ses_detail.m_currency_exchange_iso_description(0) = mc_ses_detail.m_currency_exchange_iso_description(_idx)
        mc_ses_detail.m_currency_exchange_iso_description(_idx) = _temp

        Exit For
      End If
    Next

  End Sub ' ReorderIsoCodeList

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub SetDefaultValues()

    Dim _closing_time As Integer
    Dim _initial_time As Date
    Dim _now As Date

    _now = WGDB.Now

    opt_opening_date.Checked = True
    opt_opening_user.Checked = True

    _closing_time = GetDefaultClosingTime()
    _initial_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)

    If _initial_time > _now Then
      _initial_time = _initial_time.AddDays(-1)
    End If

    Me.dtp_from.Value = _initial_time
    Me.dtp_from.Checked = False

    Me.dtp_to.Value = _initial_time.AddDays(1)
    Me.dtp_to.Checked = False

    Me.opt_all_users.Checked = True
    Me.cmb_user.Enabled = False
    Me.chk_show_all.Checked = False
    Me.chk_show_all.Enabled = False
    Me.chk_session_closed.Checked = False
    Me.chk_session_open.Checked = True

  End Sub ' SetDefaultValues

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the main SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - SessionFilter: Filter sessions when it's searching by movement
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Function GetSqlWhereSessions() As String
    Dim _str_where As String
    Dim _status As List(Of String)
    Dim _date_from As String
    Dim _date_to As String

    _str_where = ""
    _date_from = ""
    _date_to = ""

    ' Filter Dates by OpeningDate
    If Me.dtp_from.Checked Then
      If Me.opt_working_day.Checked Then
        _date_from = GUI_FormatDayDB(dtp_from.Value)
      Else
        _date_from = GUI_FormatDateDB(dtp_from.Value)
      End If
    End If
    If Me.dtp_to.Checked Then
      If Me.opt_working_day.Checked Then
        _date_to = GUI_FormatDayDB(dtp_to.Value)
      Else
        _date_to = GUI_FormatDateDB(dtp_to.Value)
      End If
    End If

    If opt_opening_date.Checked Then
      _str_where = _str_where & WSI.Common.Misc.DateSQLFilter("cgs_open_datetime", _date_from, _date_to, False)
    ElseIf opt_closing_date.Checked Then
      _str_where = _str_where & WSI.Common.Misc.DateSQLFilter("cgs_close_datetime", _date_from, _date_to, False)
    ElseIf opt_working_day.Checked Then
      _str_where = _str_where & WSI.Common.Misc.DateSQLFilter("cgs_working_day", _date_from, _date_to, False)
    End If

    ' Filter User
    If Me.opt_one_user.Checked = True Then
      If opt_opening_user.Checked Then
        _str_where = _str_where & " AND (cgs_open_user_id = " & Me.cmb_user.Value & ") "
      ElseIf opt_closing_user.Checked Then
        _str_where = _str_where & " AND (cgs_close_user_id = " & Me.cmb_user.Value & ") "
      End If
    End If

    'Filter State
    _status = New List(Of String)

    If Me.chk_session_open.Checked = True Then
      _status.Add(WSI.Common.CASHIER_SESSION_STATUS.OPEN)
    End If

    If Me.chk_session_closed.Checked = True Then
      _status.Add(WSI.Common.CASHIER_SESSION_STATUS.CLOSED)
    End If

    If _status.Count > 0 Then
      _str_where = _str_where & " AND (CASE WHEN cgs_close_datetime IS NULL THEN 0 ELSE 1 END IN ( " & String.Join(",", _status.ToArray()) & " )) "
    End If

    ' Final processing
    If Len(_str_where) > 0 Then
      ' Discard the leading ' AND ' and place 'Where' instead
      _str_where = Strings.Right(_str_where, Len(_str_where) - 5)
      _str_where = " WHERE " & _str_where
    End If

    Return _str_where
  End Function ' GetSqlWhere

  ''' <summary>
  ''' Builds an SQL "not in" string in sql format 
  ''' for a collection of types, separated by comma
  ''' </summary>
  ''' <returns>Returns a comma separated list of types that must not shown</returns>
  ''' <remarks></remarks>
  Private Function GetSqlTypeNotIn()
    Dim _str_sql_type_not_in As StringBuilder
    _str_sql_type_not_in = New StringBuilder()

    _str_sql_type_not_in.Append(Convert.ToInt32(Cage.CageOperationType.RequestOperation).ToString())
    _str_sql_type_not_in.Append(", " & Convert.ToInt32(Cage.CageOperationType.CountCage).ToString())

    Return _str_sql_type_not_in.ToString()
  End Function 'GetSqlTypeNotIn

  ''' <summary>
  ''' Builds an SQL "in" string in sql format 
  ''' for a collection of status, separated by comma
  ''' </summary>
  ''' <returns>Returns a comma separated list of status that must shown</returns>
  ''' <remarks></remarks>
  Private Function GetSqlStatusIn()
    Dim _str_sql_status_in As StringBuilder
    _str_sql_status_in = New StringBuilder()

    _str_sql_status_in.Append(TITO_MONEY_COLLECTION_STATUS.CLOSED_BALANCED)
    _str_sql_status_in.Append(", " & TITO_MONEY_COLLECTION_STATUS.CLOSED_IN_IMBALANCE)

    Return _str_sql_status_in.ToString()
  End Function

  ' PURPOSE: Get Iso Codes of Currency Exchange columns
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - List of string with the Iso Codes
  '
  Private Function LoadIsoCodes(ByRef IsoCodes As List(Of String), ByRef IsoDescriptions As List(Of String)) As Boolean
    Dim _str_sql As StringBuilder
    Dim _national_currency As String

    _str_sql = New StringBuilder()
    IsoCodes = New List(Of String)
    IsoDescriptions = New List(Of String)
    _national_currency = CurrencyExchange.GetNationalCurrency()

    Try
      Using _db_trx As DB_TRX = New DB_TRX()

        _str_sql.AppendLine(" SELECT DISTINCT   cmd_iso_code, ce_description                              ")
        _str_sql.AppendLine("            FROM   cage_movement_details                                     ")
        _str_sql.AppendLine("      INNER JOIN   currency_exchange ON cmd_iso_code = ce_currency_iso_code  ")
        _str_sql.AppendLine("           WHERE   ce_type = 0                                               ")

        Using _cmd As SqlCommand = New SqlCommand(_str_sql.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          Using _reader As SqlDataReader = _cmd.ExecuteReader()

            While _reader.Read()
              If Not _reader.IsDBNull(0) Then
                IsoCodes.Add(_reader.GetString(0))
                IsoDescriptions.Add(_reader.GetString(1))
              End If
            End While
          End Using
        End Using
      End Using

    Catch ex As Exception
      Return False
    End Try

    Return True

  End Function 'LoadCurrenciesExchange

  ' PURPOSE: .
  '
  '  PARAMS:
  '     - INPUT:
  '           - GuiId As Integer
  '           - DbTrx As DB_TRX
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - DataTable
  '
  Private Sub GetSessionsTotals()
    Dim _str_sql As StringBuilder
    Dim _str_sql_in_options As StringBuilder
    Dim _national_currency As String

    m_sessions_totals = New DataTable()
    _str_sql = New StringBuilder()
    _str_sql_in_options = New StringBuilder()
    _national_currency = CurrencyExchange.GetNationalCurrency()

    Try
      _str_sql.AppendLine("    SELECT   cgs_cage_session_id  AS cgs_id                                              ")
      _str_sql.AppendLine("      INTO   #TMP_CAGE_SESSION         																									")
      _str_sql.AppendLine("      FROM   cage_sessions  																								              ")
      _str_sql.AppendLine("    " & GetSqlWhereSessions() & "                                                        ")
      _str_sql.AppendLine("                                                                                         ")
      _str_sql.AppendLine("   SELECT   X.cgs_id																								                      ")
      _str_sql.AppendLine("		       , X.cmd_iso_code                                                               ")
      _str_sql.AppendLine("		       , SUM(X.cmd_deposits)    AS cmd_deposits                                       ")
      _str_sql.AppendLine("		       , SUM(X.cmd_withdrawals) AS cmd_withdrawals																		")
      _str_sql.AppendLine("     INTO   #TMP_CAGE_MONEY                                                              ")
      _str_sql.AppendLine("     FROM   (                                                                            ")
      _str_sql.AppendLine("    	       SELECT   cgs_id																								              ")
      _str_sql.AppendLine("    		            , cgm_movement_id                                                     ")
      _str_sql.AppendLine("    		            , cmd_iso_code                                                        ")
      _str_sql.AppendLine("    		            , cmd_denomination                                                    ")
      _str_sql.AppendLine("    		            , CASE                                                                ")
      _str_sql.AppendLine("    			             WHEN cgm_type >= 100                                               ")
      _str_sql.AppendLine("    				             THEN ISNULL(SUM(CASE 																						")
      _str_sql.AppendLine("    								                      WHEN cmd_quantity < 0                           ")
      _str_sql.AppendLine("    									                      THEN cmd_denomination                         ")
      _str_sql.AppendLine("    								                        ELSE cmd_quantity * cmd_denomination          ")
      _str_sql.AppendLine("    								                      END), 0)                                        ")
      _str_sql.AppendLine("    			               ELSE 0                                                           ")
      _str_sql.AppendLine("    			             END AS cmd_deposits                                                ")
      _str_sql.AppendLine("    		            , CASE                                                                ")
      _str_sql.AppendLine("    			             WHEN cgm_type < 100                                                ")
      _str_sql.AppendLine("    				             THEN ISNULL(SUM(CASE                                             ")
      _str_sql.AppendLine("    								                      WHEN cmd_quantity < 0                           ")
      _str_sql.AppendLine("    									                      THEN cmd_denomination                         ")
      _str_sql.AppendLine("    								                        ELSE cmd_quantity * cmd_denomination					")
      _str_sql.AppendLine("    								                      END), 0)                                        ")
      _str_sql.AppendLine("    			               ELSE 0                                                           ")
      _str_sql.AppendLine("    			             END AS cmd_withdrawals                                             ")
      _str_sql.AppendLine("    	         FROM   cage_movements                                                      ")
      _str_sql.AppendLine("    	   INNER JOIN   #TMP_CAGE_SESSION ON cgs_id = cgm_cage_session_id                   ")
      _str_sql.AppendLine("    	   INNER JOIN   cage_movement_details ON cgm_movement_id = cmd_movement_id          ")
      _str_sql.AppendLine("    	        WHERE   cmd_quantity <> -200                                                ")
      _str_sql.AppendLine("    	          AND   cmd_quantity <> -1                                                  ")
      ' ATB 24-NOV-2016
      ' Checking if it's not a request and if it's not a cancelled cashier retirement
      _str_sql.AppendLine("    	          AND   cgm_type NOT IN(" & GetSqlTypeNotIn() & ")                          ")
      _str_sql.AppendLine("    	          AND   cgm_status <> @pCanceledUncollectedCashierRetirement                ")
      _str_sql.AppendLine("    	     GROUP BY   cgs_id                                                              ")
      _str_sql.AppendLine("    		            , cgm_movement_id                                                     ")
      _str_sql.AppendLine("    		            , cmd_iso_code                                                        ")
      _str_sql.AppendLine("    		            , cgm_type                                                            ")
      _str_sql.AppendLine("    		            , cgm_status																									        ")
      _str_sql.AppendLine("    		            , cmd_denomination																									  ")
      _str_sql.AppendLine("    	       ) AS X                                                                       ")
      _str_sql.AppendLine("  GROUP BY   X.cgs_id                                                                    ")
      _str_sql.AppendLine("	          , X.cmd_iso_code                                                              ")
      _str_sql.AppendLine("")
      _str_sql.AppendLine("   INSERT INTO   #TMP_CAGE_MONEY                                                         ")
      _str_sql.AppendLine("        SELECT   cgm_cage_session_id                                                     ")
      If Not WSI.Common.Misc.IsFloorDualCurrencyEnabled() Then
        _str_sql.AppendLine("               , @pIsoCode AS cmd_iso_code                                             ")
      Else
        _str_sql.AppendLine("               , te_iso_code AS cmd_iso_code                                           ")
      End If
      _str_sql.AppendLine("               , SUM(ISNULL(MC_COLLECTED_BILL_AMOUNT, 0)                                 ")
      _str_sql.AppendLine("                 + ISNULL(MC_COLLECTED_COIN_AMOUNT, 0)) AS cmd_deposits                  ")
      _str_sql.AppendLine("   	          , 0 AS cmd_withdrawals                                                    ")
      _str_sql.AppendLine("          FROM   money_collections                                                       ")
      _str_sql.AppendLine("    INNER JOIN   cage_movements ON cgm_mc_collection_id = mc_collection_id               ")
      _str_sql.AppendLine("    INNER JOIN   #TMP_CAGE_SESSION ON cgm_cage_session_id = cgs_id                       ")
      If WSI.Common.Misc.IsFloorDualCurrencyEnabled() Then
        _str_sql.AppendLine("  INNER JOIN    terminals ON te_terminal_id = cgm_terminal_cashier_id                  ")
      End If
      _str_sql.AppendLine("         WHERE   cgm_type = @pFromTerminal                                               ")
      _str_sql.AppendLine("   	      AND   mc_status IN (" & GetSqlStatusIn() & ")                                 ")
      If Not WSI.Common.Misc.IsFloorDualCurrencyEnabled() Then
        _str_sql.AppendLine("      GROUP BY   cgm_cage_session_id                                                   ")
      Else
        _str_sql.AppendLine("      GROUP BY   cgm_cage_session_id, te_iso_code                                      ")
      End If
      _str_sql.AppendLine("                                                                                         ")
      _str_sql.AppendLine("   SELECT   X.cgs_id                                                                     ")
      _str_sql.AppendLine("   	     , X.cmd_iso_code                                                               ")
      _str_sql.AppendLine("   	     , X.cmd_deposits                                                               ")
      _str_sql.AppendLine("   	     , X.cmd_withdrawals                                                            ")
      _str_sql.AppendLine("   	     , (X.cmd_deposits - X.cmd_withdrawals) AS cmd_difference                       ")
      _str_sql.AppendLine("     FROM (                                                                              ")
      _str_sql.AppendLine("               SELECT   cgs_id                                                           ")
      _str_sql.AppendLine("                      , cmd_iso_code                                                     ")
      _str_sql.AppendLine("                      , SUM(ISNULL(cmd_deposits, 0)) AS cmd_deposits                     ")
      _str_sql.AppendLine("                      , SUM(ISNULL(cmd_withdrawals, 0)) AS cmd_withdrawals               ")
      _str_sql.AppendLine("                 FROM   #TMP_CAGE_MONEY                                                  ")
      _str_sql.AppendLine("             GROUP BY   cgs_id                                                           ")
      _str_sql.AppendLine("                      , cmd_iso_code                                                     ")
      _str_sql.AppendLine("           ) AS X                                                                        ")
      _str_sql.AppendLine("                                                                                         ")
      _str_sql.AppendLine("DROP TABLE  #TMP_CAGE_SESSION																								            ")
      _str_sql.AppendLine("DROP TABLE  #TMP_CAGE_MONEY															 										            ")

      Using _db_trx As New DB_TRX()
        Using _sql_cmd As New SqlCommand(_str_sql.ToString())
          _sql_cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).Value = _national_currency
          _sql_cmd.Parameters.Add("@pCanceledUncollectedCashierRetirement", SqlDbType.Int).Value = Convert.ToInt32(Cage.CageStatus.CanceledUncollectedCashierRetirement).ToString()
          _sql_cmd.Parameters.Add("@pFromTerminal", SqlDbType.Int).Value = Cage.CageOperationType.FromTerminal
          Using _sql_da As New SqlDataAdapter(_sql_cmd)
            _db_trx.Fill(_sql_da, m_sessions_totals)
          End Using
        End Using
      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

  End Sub ' GetSessonsTotals

  ' PURPOSE: Checks whether the specified row contains data or not
  '
  '  PARAMS:
  '     - INPUT:
  '           - IdxRow: row to check
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - TRUE: selected row contains data
  '     - FALSE: selected row does not exist or contains no data
  '
  Private Function IsValidDataRow(ByVal IdxRow As Integer) As Boolean

    ' Skip Totalizator row!!!
    If IdxRow < 0 Or IdxRow >= (Me.Grid.NumRows - 1) Then
      Return False
    Else
      Return True
    End If

  End Function ' IsValidDataRow

  ' PURPOSE: Show cage balance.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Private Sub ShowCageBalance(Optional ByVal ShowOnlyStock As Boolean = False)
    Dim _frm_cage_session_detail As frm_cage_session_detail

    If (Me.Grid Is Nothing OrElse Me.Grid.NumRows = 0) Then
      If ShowOnlyStock Then
        Windows.Forms.Cursor.Current = Cursors.WaitCursor

        mc_ses_detail.m_session_id = -1
        mc_ses_detail.m_session_status = -1
        mc_ses_detail.m_session_opening_user = AUDIT_NONE_STRING
        mc_ses_detail.m_session_opening_date = AUDIT_NONE_STRING
        mc_ses_detail.m_session_closing_user = AUDIT_NONE_STRING
        mc_ses_detail.m_session_opening_user_id = AUDIT_NONE_STRING
        mc_ses_detail.m_session_closing_date = AUDIT_NONE_STRING
        mc_ses_detail.m_session_name = AUDIT_NONE_STRING
        mc_ses_detail.m_session_working_day = AUDIT_NONE_STRING

        _frm_cage_session_detail = New frm_cage_session_detail(ENUM_FORM.FORM_CAGE_SESSIONS_DETAIL)
        _frm_cage_session_detail.ShowForEdit(Me.MdiParent, mc_ses_detail, ShowOnlyStock)
        Return
      Else
        Return
      End If
    End If

    Dim _idx_row As Integer
    Dim _session_id As Int64

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    ' Protect totalizator row
    ' TODO: Poner cuando est� el registro de totales
    '''If Not IsValidDataRow(idx_row) Then
    '''  Return
    '''End If

    ' Session
    If Me.Grid.Cell(_idx_row, GRID_COLUMN_CGS_CAGE_SESSION_ID).Value = "" Then
      _session_id = 0
    Else
      _session_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_CGS_CAGE_SESSION_ID).Value
    End If

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    mc_ses_detail.m_session_id = _session_id
    mc_ses_detail.m_session_status = Me.Grid.Cell(_idx_row, GRID_COLUMN_CGS_STATUS_ID).Value
    mc_ses_detail.m_session_opening_user = Me.Grid.Cell(_idx_row, GRID_COLUMN_CGS_OPEN_USER_NAME).Value
    mc_ses_detail.m_session_opening_user_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_CGS_OPEN_USER_ID).Value
    mc_ses_detail.m_session_opening_date = Me.Grid.Cell(_idx_row, GRID_COLUMN_CGS_OPEN_DATETIME).Value
    mc_ses_detail.m_session_closing_user = Me.Grid.Cell(_idx_row, GRID_COLUMN_CGS_CLOSE_USER_NAME).Value
    mc_ses_detail.m_session_closing_date = Me.Grid.Cell(_idx_row, GRID_COLUMN_CGS_CLOSE_DATETIME).Value
    mc_ses_detail.m_session_name = Me.Grid.Cell(_idx_row, GRID_COLUMN_CGS_SESSION_NAME).Value
    mc_ses_detail.m_session_working_day = Me.Grid.Cell(_idx_row, GRID_COLUMN_CGS_WORKING_DAY).Value

    _frm_cage_session_detail = New frm_cage_session_detail(ENUM_FORM.FORM_CAGE_SESSIONS_DETAIL)
    _frm_cage_session_detail.ShowForEdit(Me.MdiParent, mc_ses_detail, ShowOnlyStock)

  End Sub 'ShowCageBalance

  ' PURPOSE: Session has open sessions.
  '
  '  PARAMS:
  '     - INPUT:
  '         - IdxRow: Int32
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Private Function HasPendingSessions(ByVal IdxRow As Int32) As Boolean
    Dim _result As Boolean
    Dim _sb_query As StringBuilder
    Dim _result_data As Object

    _result = False

    ' JCA & DHA 29-APR-2014
    _sb_query = New StringBuilder()
    _sb_query.AppendLine(" SELECT   COUNT(*)     ")
    _sb_query.AppendLine("   FROM  (             ")

    ' cashier sessions with integrated cashier
    '_sb_query.AppendLine("                SELECT   COUNT(DISTINCT (CS_SESSION_ID)) AS OPENSESSIONS                            ")
    _sb_query.AppendLine("                SELECT   DISTINCT (CS_SESSION_ID) AS OPENSESSIONS                                   ")
    _sb_query.AppendLine("                  FROM   CASHIER_SESSIONS                                                           ")
    _sb_query.AppendLine("                 INNER   JOIN CAGE_MOVEMENTS                                                        ")
    _sb_query.AppendLine("                    ON   (CASHIER_SESSIONS.CS_USER_ID = CAGE_MOVEMENTS.CGM_USER_CASHIER_ID)         ")
    ' cashier sessions w/o integrated cashier    
    _sb_query.AppendLine("                    OR   (CAGE_MOVEMENTS.CGM_TERMINAL_CASHIER_ID = CASHIER_SESSIONS.CS_CASHIER_ID)  ")
    '''''''''''''''''''''''''''''''''''''''''
    _sb_query.AppendLine("                 INNER   JOIN CAGE_SESSIONS                                                         ")
    _sb_query.AppendLine("                    ON   CAGE_MOVEMENTS.CGM_CAGE_SESSION_ID = CAGE_SESSIONS.CGS_CAGE_SESSION_ID     ")
    _sb_query.AppendLine("                 WHERE   CGS_CAGE_SESSION_ID = @pSessionId                                          ")
    _sb_query.AppendLine("                   AND   CAGE_MOVEMENTS.CGM_STATUS <> 1                                             ")
    _sb_query.AppendLine("                   AND   CASHIER_SESSIONS.CS_STATUS = 0                                             ")
    _sb_query.AppendLine("                   AND   CASHIER_SESSIONS.CS_USER_ID NOT IN(                                        ")
    _sb_query.AppendLine("                        SELECT  GU_USER_ID  FROM  GUI_USERS  WHERE  GU_USER_TYPE = @pUserUserType)  ")

    _sb_query.AppendLine("          ) AS X                                                                    ")
    _sb_query.AppendLine(" INNER JOIN   CAGE_MOVEMENTS ON CGM_CASHIER_SESSION_ID = X.OPENSESSIONS             ")
    _sb_query.AppendLine("      WHERE   CGM_CAGE_SESSION_ID = @pSessionId                                     ")

    Try
      Using _db_trx As New DB_TRX()
        Using _sql_cmd As New SqlCommand(_sb_query.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = Me.Grid.Cell(IdxRow, GRID_COLUMN_CGS_CAGE_SESSION_ID).Value
          _sql_cmd.Parameters.Add("@pUserUserType", SqlDbType.Int).Value = GU_USER_TYPE.SYS_SYSTEM
          _result_data = _sql_cmd.ExecuteScalar()

          If Not IsNothing(_result_data) Then
            'If CInt(_result_data) > 0 And Not GeneralParam.GetBoolean("Cage", "AllowClosingWithOpenCashSessions", False) Then
            If CInt(_result_data) > 0 Then
              _result = True
            Else
              _result = False
            End If

          End If

        End Using
      End Using

    Catch _ex As Exception
      _result = True
    End Try

    Return _result
  End Function 'HasPendingSessions


  ' PURPOSE: Session has open sessions.
  '
  '  PARAMS:
  '     - INPUT:
  '           '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Private Function HasPendingSessionsToCollect(Optional ByVal GamingDay As DateTime = Nothing) As Boolean
    Dim _sb_query As StringBuilder
    Dim _obj As Object

    ' JCA & DHA 29-APR-2014: Added played tickets types
    _sb_query = New StringBuilder()
    _sb_query.AppendLine("SELECT   TOP 1 CS_SESSION_ID ")
    _sb_query.AppendLine("  FROM   CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID ")
    _sb_query.AppendLine(" INNER   JOIN CASHIER_SESSIONS ON CM_SESSION_ID = CS_SESSION_ID ")
    _sb_query.AppendLine("	AND    CS_STATUS   = @pStatusOpen ")
    _sb_query.AppendLine("	AND    CM_TYPE    IN (@pFillerIn,@pFillerOut,@pCashInSplit1,@pCashIn,@pTicketInCashable,@pTicketInPromoRE,@pTicketInPromoNR) ")
    _sb_query.AppendLine("	AND    CM_SUB_TYPE = 0 ")

    If GamingDay <> Nothing Then
      ' Check only for pendins sessions of the indicated gaming day
      _sb_query.AppendLine("	AND    CS_GAMING_DAY = " + GUI_FormatDateDB(GamingDay))
    End If

    Try
      Using _db_trx As New DB_TRX()
        Using _sql_cmd As New SqlCommand(_sb_query.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _sql_cmd.Parameters.Add("@pStatusOpen", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN
          _sql_cmd.Parameters.Add("@pFillerIn", SqlDbType.Int).Value = CASHIER_MOVEMENT.FILLER_IN
          _sql_cmd.Parameters.Add("@pFillerOut", SqlDbType.Int).Value = CASHIER_MOVEMENT.FILLER_OUT
          _sql_cmd.Parameters.Add("@pCashInSplit1", SqlDbType.Int).Value = CASHIER_MOVEMENT.NA_CASH_IN_SPLIT1
          _sql_cmd.Parameters.Add("@pCashIn", SqlDbType.Int).Value = CASHIER_MOVEMENT.CASH_IN
          _sql_cmd.Parameters.Add("@pTicketInCashable", SqlDbType.Int).Value = CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PLAYED_CASHABLE
          _sql_cmd.Parameters.Add("@pTicketInPromoRE", SqlDbType.Int).Value = CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PLAYED_PROMO_REDEEMABLE
          _sql_cmd.Parameters.Add("@pTicketInPromoNR", SqlDbType.Int).Value = CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PLAYED_PROMO_NOT_REDEEMABLE

          _obj = _sql_cmd.ExecuteScalar()

          Return Not IsNothing(_obj)

        End Using
      End Using

    Catch _ex As Exception
    End Try

    ' On error return true -> Can't close cage session
    Return True
  End Function 'HasPendingSessionsToCollect

  ' PURPOSE: Session has pending movements.
  '
  '  PARAMS:
  '     - INPUT:
  '         - IdxRow: Int32
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Private Function HasPendingMovements(ByVal IdxRow As Int32) As Boolean
    Dim _sb_query As StringBuilder
    Dim _result_data As Object



    _sb_query = New StringBuilder()
    _sb_query.AppendLine("                SELECT   COUNT(*) AS PENDINGMOVEMENTS                                             ")
    _sb_query.AppendLine("                  FROM   CAGE_PENDING_MOVEMENTS                                                   ")
    _sb_query.AppendLine("            INNER JOIN   CAGE_MOVEMENTS                                                           ")
    _sb_query.AppendLine("                    ON   CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_PENDING_MOVEMENTS.CPM_MOVEMENT_ID  ")
    _sb_query.AppendLine("            INNER JOIN   CAGE_SESSIONS                                                            ")
    _sb_query.AppendLine("                    ON   CAGE_MOVEMENTS.CGM_CAGE_SESSION_ID = CAGE_SESSIONS.CGS_CAGE_SESSION_ID   ")
    _sb_query.AppendLine("                 WHERE   CGS_CAGE_SESSION_ID = @pSessionId                                        ")

    Try
      Using _db_trx As New DB_TRX()
        Using _sql_cmd As New SqlCommand(_sb_query.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = Me.Grid.Cell(IdxRow, GRID_COLUMN_CGS_CAGE_SESSION_ID).Value
          _result_data = _sql_cmd.ExecuteScalar()

          If Not IsNothing(_result_data) Then
            If CInt(_result_data) > 0 Then
              Return True
            End If

          End If
        End Using
      End Using

    Catch _ex As Exception
    End Try

    Return False
  End Function

  ' PURPOSE: Checks if there is only 1 opened cage session
  '
  '  PARAMS:
  '     - INPUT:
  '         - IdxRow: Int32
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '     - true if its the last open cage session
  '
  Private Function CheckIfLastOpenCageSession() As Boolean
    Dim _result As Boolean
    Dim _sb_query As StringBuilder
    Dim _result_data As Object

    _result = False

    _sb_query = New StringBuilder()
    _sb_query.AppendLine("      SELECT   COUNT(CGS_CAGE_SESSION_ID)   ")
    _sb_query.AppendLine("        FROM   CAGE_SESSIONS                ")
    _sb_query.AppendLine("       WHERE   CGS_CLOSE_DATETIME IS NULL   ")


    Try
      Using _db_trx As New DB_TRX()
        Using _sql_cmd As New SqlCommand(_sb_query.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _result_data = _sql_cmd.ExecuteScalar()

          If Not IsNothing(_result_data) Then
            _result = CInt(_result_data) <= 1
          End If

        End Using
      End Using
    Catch _ex As Exception
      _result = True
    End Try

    Return _result
  End Function ' CheckIfLastOpenCageSession

  ' PURPOSE: Generate Auditor Data
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Public Function GUI_AuditorData(ByVal NLSID As Int16, ByVal SessionName As String, ByVal WorkingDay As String, ByVal OpenSession As Boolean) As Boolean

    Dim _auditor_data As CLASS_AUDITOR_DATA

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_CAGE)

    _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(2876), _
                      GLB_NLS_GUI_PLAYER_TRACKING.GetString(2876) & ": " & String.Format(GLB_NLS_GUI_PLAYER_TRACKING.GetString(NLSID), SessionName), _
                      SessionName)

    If OpenSession Then
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(253), SessionName)
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5723), WorkingDay)
    End If

    If _auditor_data.Notify(GLB_CurrentUser.GuiId, _
                           GLB_CurrentUser.Id, _
                           GLB_CurrentUser.Name, _
                           CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, _
                           0) Then
      Return True
    End If

    Return False

  End Function



  ' PURPOSE: Generate Auditor Data Extended
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '

  Public Function GUI_AuditorDataExtended(ByVal NLSID As Int16, ByRef Row As Integer) As Boolean

    Dim _auditor_data As CLASS_AUDITOR_DATA

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_CAGE)

    ' Audit data
    '     - Account
    _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(2876), _
                         GLB_NLS_GUI_PLAYER_TRACKING.GetString(2876) & ": " & _
                         GLB_NLS_GUI_PLAYER_TRACKING.GetString(NLSID))

    _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4594), Me.Grid.Cell(Row, GRID_COLUMN_CGS_SESSION_NAME).Value)
    _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4452), Me.Grid.Cell(Row, GRID_COLUMN_CGS_OPEN_DATETIME).Value)

    If _auditor_data.Notify(GLB_CurrentUser.GuiId, _
                               GLB_CurrentUser.Id, _
                               GLB_CurrentUser.Name, _
                               CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, _
                              0) Then

      Return True
    End If

    Return False

  End Function

  ' PURPOSE: count cage session
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Private Sub GUI_CountCageSession()
    Dim _frm_edit As frm_cage_control
    Dim _idx_row As Integer

    If Me.Grid Is Nothing OrElse Me.Grid.NumRows = 0 Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4454), ENUM_MB_TYPE.MB_TYPE_WARNING) ' You must select an open cash cage session

      Return
    End If

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    If Me.Grid.Cell(_idx_row, GRID_COLUMN_CGS_STATUS_ID).Value = CASHIER_SESSION_STATUS.CLOSED Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4454), ENUM_MB_TYPE.MB_TYPE_WARNING) ' You must select an open cash cage session
    Else
      _frm_edit = New frm_cage_control

      Call _frm_edit.ShowNewItem(CLASS_CAGE_CONTROL.OPEN_MODE.COUNT_CAGE, Me.Grid.Cell(_idx_row, GRID_COLUMN_CGS_CAGE_SESSION_ID).Value)

    End If

    _frm_edit = Nothing
  End Sub

  ' PURPOSE: open cage session
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Private Sub GUI_OpenCageSession()

    Dim _class_cage_control As CLASS_CAGE_CONTROL
    Dim _cage_sessions_opened As Integer
    Dim _nls As Integer
    Dim _frm_edit As frm_cage_session_open
    Dim _session_name As String
    Dim _working_day As Date

    _cage_sessions_opened = Cage.GetNumberOfOpenedCages()
    _session_name = String.Empty
    _working_day = Now

    Using _db_trx As New DB_TRX()

      If _cage_sessions_opened > 0 Then

        If _cage_sessions_opened = 1 Then
          _nls = 6346
        Else
          _nls = 4588
        End If

        If CheckIfAlreadyOpenedCageSession(_db_trx) Then
          _nls = 4593
        End If

        If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(_nls), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, _
           , IIf(_nls = 4593, CurrentUser.Name, _cage_sessions_opened)) = ENUM_MB_RESULT.MB_RESULT_YES Then

          _frm_edit = New frm_cage_session_open

          Call _frm_edit.ShowNewItem(0, _db_trx)

          If _frm_edit.Canceled = True Then

            Exit Sub
          End If

          _session_name = _frm_edit.SessionName
          _working_day = _frm_edit.WorkingDay

          Call Me.OpenCage(_session_name, _working_day, _db_trx)

        End If
      Else
        If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4477), _
                              ENUM_MB_TYPE.MB_TYPE_WARNING, _
                              ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_YES Then

          _frm_edit = New frm_cage_session_open

          Call _frm_edit.ShowNewItem(0, _db_trx)

          If _frm_edit.Canceled = True Then

            Exit Sub
          End If

          _session_name = _frm_edit.SessionName
          _working_day = _frm_edit.WorkingDay

          Call Me.OpenCage(_session_name, _working_day, _db_trx)

        End If
      End If

    End Using

    _class_cage_control = Nothing
  End Sub

  ' PURPOSE: open cage session
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Private Sub OpenCage(ByVal SessionName As String, ByVal WorkingDay As Date, ByVal DbTrx As DB_TRX)
    Dim _class_cage_control As CLASS_CAGE_CONTROL

    _class_cage_control = New CLASS_CAGE_CONTROL

    If _class_cage_control.OpenCageSessionDB(SessionName, WorkingDay, DbTrx.SqlTransaction) Then

      _class_cage_control.OperationType = Cage.CageOperationType.OpenCage
      _class_cage_control.Status = Cage.CageStatus.FinishedOpenCloseCage

      If _class_cage_control.InsertMovement(DbTrx.SqlTransaction) Then

        If Not GUI_AuditorData(5724, SessionName, WorkingDay, True) Then
          Call DbTrx.Rollback()
        Else
          If Not CageMeters.OpenCage_CreateCageSessionMeters(_class_cage_control.CageSessionId, DbTrx.SqlTransaction) Then
            Call DbTrx.Rollback()
            NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3387), ENUM_MB_TYPE.MB_TYPE_WARNING)
          Else
            Call DbTrx.Commit()
            NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3386), ENUM_MB_TYPE.MB_TYPE_INFO)
          End If
        End If

      Else
        Call DbTrx.Rollback()
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3387), ENUM_MB_TYPE.MB_TYPE_WARNING)
      End If

    Else

      Call DbTrx.Rollback()
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3387), ENUM_MB_TYPE.MB_TYPE_WARNING)

    End If

  End Sub ' OpenCage


  ' PURPOSE: close cage session
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Private Sub GUI_CloseCageSession()

    Dim _idx_row As Integer
    Dim _class_cage_control As CLASS_CAGE_CONTROL
    Dim _can_close As Boolean
    Dim _cage_session_gaming_day As DateTime = Nothing
    Dim _opened_sessions As Integer

    If Me.Grid Is Nothing OrElse Me.Grid.NumRows = 0 Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4454), ENUM_MB_TYPE.MB_TYPE_WARNING) ' You must select an open cash cage session

      Return
    End If

    If Not Permissions.Write Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK) 'the user must have permissions

      Return
    End If

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    _can_close = True

    If Me.Grid.Cell(_idx_row, GRID_COLUMN_CGS_STATUS_ID).Value = CASHIER_SESSION_STATUS.CLOSED Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4454), ENUM_MB_TYPE.MB_TYPE_WARNING) ' You must select an open cash cage session
      _can_close = False
    Else

      If Me.m_gaming_day Then
        ' When gaming day active, pick the cage session working day
        Using _db_trx As New DB_TRX()
          _cage_session_gaming_day = Cage.GetCageSessionWorkingDayById(Me.Grid.Cell(_idx_row, GRID_COLUMN_CGS_CAGE_SESSION_ID).Value, _db_trx.SqlTransaction)
        End Using
      End If

      _opened_sessions = Cage.GetNumberOfOpenedCages(_cage_session_gaming_day)

      'If CheckIfLastOpenCageSession() Then
      '  NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4597), _
      '            ENUM_MB_TYPE.MB_TYPE_ERROR, _
      '            ENUM_MB_BTN.MB_BTN_OK)
      '  _can_close = False
      'ElseIf HasPendingMovements(_idx_row) Then
      If HasPendingMovements(_idx_row) And
        ((_opened_sessions = 1 And m_gaming_day) Or
         Not m_gaming_day) Then

        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4709), _
                  ENUM_MB_TYPE.MB_TYPE_ERROR, _
                  ENUM_MB_BTN.MB_BTN_OK)
        _can_close = False

      ElseIf HasPendingSessions(_idx_row) And
        ((_opened_sessions = 1 And m_gaming_day) Or
         Not m_gaming_day) Then

        If Not GeneralParam.GetBoolean("Cage", "AllowClosingWithOpenCashSessions", False) OrElse Cage.GetNumberOfOpenedCages(_cage_session_gaming_day) = 1 Then
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4579), _
                    ENUM_MB_TYPE.MB_TYPE_ERROR, _
                    ENUM_MB_BTN.MB_BTN_OK)
          _can_close = False
        Else
          'PREGUNTA SI CERRAR
          If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4719), _
                                  ENUM_MB_TYPE.MB_TYPE_WARNING, _
                                  ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_YES Then
            'SI PREGUNTA ES QUE SI QUIERE CERRAR
            _can_close = True
          Else
            'SI PREGUNTA ES QUE NO QUIERE CERRAR
            _can_close = False
          End If
        End If

      ElseIf Not m_gaming_day Then

        If _opened_sessions = 1 AndAlso HasPendingSessionsToCollect() Then
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4579), _
                             ENUM_MB_TYPE.MB_TYPE_ERROR, _
                             ENUM_MB_BTN.MB_BTN_OK)
          _can_close = False
        End If

      Else

        If _opened_sessions = 1 And (Cage.GamingDayHasOpenedCashierSessions(_cage_session_gaming_day) Or HasPendingSessionsToCollect(_cage_session_gaming_day)) Then
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4579), _
                             ENUM_MB_TYPE.MB_TYPE_ERROR, _
                             ENUM_MB_BTN.MB_BTN_OK)
          _can_close = False
        End If

      End If

    End If

    If _can_close Then
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4478), _
                        ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_YES Then

        Using _db_trx As New DB_TRX()

          _class_cage_control = New CLASS_CAGE_CONTROL
          _class_cage_control.CageSessionId = Me.Grid.Cell(_idx_row, GRID_COLUMN_CGS_CAGE_SESSION_ID).Value

          If _class_cage_control.CloseCageSession(_db_trx.SqlTransaction) Then
            If Not GUI_AuditorData(5725, Me.Grid.Cell(_idx_row, GRID_COLUMN_CGS_SESSION_NAME).Value, Me.Grid.Cell(_idx_row, GRID_COLUMN_CGS_WORKING_DAY).Value, False) Then
              Call _db_trx.Rollback()
            Else
              Call _db_trx.Commit()
              NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3383), ENUM_MB_TYPE.MB_TYPE_INFO)
            End If
          Else
            Call _db_trx.Rollback()
            NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3384), ENUM_MB_TYPE.MB_TYPE_WARNING)
          End If

        End Using

      End If
    End If

  End Sub

  ' PURPOSE: checks if there is already an opened cage session for this user
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Private Function CheckIfAlreadyOpenedCageSession(ByVal DB_Trx As DB_TRX) As Boolean
    Dim _sb As StringBuilder

    _sb = New StringBuilder
    _sb.AppendLine(" SELECT   TOP 1 *                                ")
    _sb.AppendLine("   FROM   CAGE_SESSIONS                          ")
    _sb.AppendLine("  WHERE   CGS_OPEN_USER_ID  = @pUserID           ")
    _sb.AppendLine("    AND   CGS_CLOSE_DATETIME IS NULL             ")

    Using _cmd As New SqlCommand(_sb.ToString())

      _cmd.Parameters.Add("@pUserID", SqlDbType.Int).Value = CurrentUser.Id

      Using _reader As SqlDataReader = DB_Trx.ExecuteReader(_cmd)
        If _reader.Read() Then
          Return True
        End If
      End Using
    End Using

    Return False

  End Function

  ' PURPOSE: To get the selected cage session id
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Private Sub GetCageSessionId(ByVal CloseCurrentForm As Boolean)
    Dim _idx_row As Integer

    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    'LMRS 30/09/2014 WIG-1339
    Me.m_cage_session_id = -1
    If Me.Grid.NumRows > 0 AndAlso Not Me.Grid.Cell(_idx_row, GRID_COLUMN_CGS_CAGE_SESSION_ID).Value = "" Then
      Me.m_cage_session_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_CGS_CAGE_SESSION_ID).Value
      If CloseCurrentForm Then
        Call Me.Close()
      End If
    End If


  End Sub ' GetCageSessionId

  ' PURPOSE: To get the selected cage session name and status
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Private Sub GetCageSessionNameAndStatus()
    Dim _idx_row As Integer

    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    If Me.Grid.Cell(_idx_row, GRID_COLUMN_CGS_CAGE_SESSION_ID).Value = "" Then
      Me.m_cage_session_name = String.Empty
      Me.m_cage_session_status = 0
    Else
      Me.m_cage_session_name = Me.Grid.Cell(_idx_row, GRID_COLUMN_CGS_SESSION_NAME).Value

      If Me.Grid.Cell(_idx_row, GRID_COLUMN_CGS_STATUS).Value = GLB_NLS_GUI_INVOICING.GetString(156) Then
        Me.m_cage_session_status = 0
      ElseIf Me.Grid.Cell(_idx_row, GRID_COLUMN_CGS_STATUS).Value = GLB_NLS_GUI_INVOICING.GetString(157) Then
        Me.m_cage_session_status = 1
      End If

    End If

  End Sub ' GetCageSessionNameAndStatus

  ' PURPOSE: To get the first and las cage session name in screen
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Private Sub GetCageFirstAndLastSessionName()

    Me.m_first_session_name = Me.Grid.Cell(0, GRID_COLUMN_CGS_SESSION_NAME).Value
    Me.m_last_session_name = Me.Grid.Cell(Me.Grid.NumRows - 1, GRID_COLUMN_CGS_SESSION_NAME).Value

  End Sub ' GetCageFirstAndLastSessionName

  ' PURPOSE: To get all cage session id
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Private Sub GetCageSessionIdsInGrid()
    Dim _idx_row As Integer

    Me.m_cage_sessions_in_grid = String.Empty

    For _idx_row = 0 To Me.Grid.NumRows - 1

      m_cage_sessions_in_grid = m_cage_sessions_in_grid & Me.Grid.Cell(_idx_row, GRID_COLUMN_CGS_CAGE_SESSION_ID).Value.ToString()

      If _idx_row < Me.Grid.NumRows - 1 Then
        m_cage_sessions_in_grid = m_cage_sessions_in_grid & ","
      End If

    Next

  End Sub ' GetCageSessionId

  ' PURPOSE: To set the form layout for Selection mode
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Private Sub SetLayoutForSelectionMode()
    Me.m_cage_session_id = -1
    Me.gb_date.Enabled = False
    Me.gb_user.Enabled = False
    Me.fra_session_state.Enabled = False

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_RESET).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_INFO).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_3).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_EXCEL).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_PRINT).Visible = False

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = CurrentUser.Permissions(ENUM_FORM.FORM_CAGE_SESSIONS_DETAIL).Read
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(723)   ' Seleccionar
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Enabled = True

    ' Search
    Call GetSessionsTotals()
    Call MyBase.GUI_ButtonClick(frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY)
  End Sub ' SetLayoutForSelectionMode

  ' PURPOSE: Show cage global report.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Private Sub GUI_ReOpenCageSession()

    Dim _idx_row As Integer

    If Me.Grid Is Nothing OrElse Me.Grid.NumRows = 0 Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5389), ENUM_MB_TYPE.MB_TYPE_WARNING) ' You must select an open cash cage session

      Return
    End If

    If Not Permissions.Write Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK) 'the user must have permissions

      Return
    End If

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then

        Exit For
      End If
    Next

    If Me.Grid.Cell(_idx_row, GRID_COLUMN_CGS_STATUS_ID).Value <> CASHIER_SESSION_STATUS.CLOSED Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5389), ENUM_MB_TYPE.MB_TYPE_WARNING) ' You must select an open cash cage session

      Return
    End If


    'preguntar solo si se quiere reabrir la sesi�n de b�veda XXXXXXX
    If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5386), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, _
           , Me.Grid.Cell(_idx_row, GRID_COLUMN_CGS_SESSION_NAME).Value) = ENUM_MB_RESULT.MB_RESULT_YES Then


      Call Me.ReOpenCage(_idx_row)

    Else

      Return
    End If
  End Sub


  ' PURPOSE: reopen cage session
  '
  '  PARAMS:
  '     - INPUT: db, row
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Private Sub ReOpenCage(ByVal Row As Integer)
    Dim _class_cage_control As New CLASS_CAGE_CONTROL
    Dim _source_name As String
    Dim _description As String
    Dim _message As String ' acumula el mensaje de salida
    Dim _type As Integer  ' acumula el tipo de mensaje de salida

    _class_cage_control.CageSessionId = Me.Grid.Cell(Row, GRID_COLUMN_CGS_CAGE_SESSION_ID).Value
    _class_cage_control.OperationType = Cage.CageOperationType.ReOpenCage
    _class_cage_control.Status = Cage.CageStatus.FinishedOpenCloseCage

    Using DbTrx As New DB_TRX()
      If _class_cage_control.ReOpenCageSessionDB(DbTrx.SqlTransaction) Then
        If Not GUI_AuditorDataExtended(5383, Row) Then
          Call DbTrx.Rollback() ' Rollback if error
          _message = GLB_NLS_GUI_PLAYER_TRACKING.Id(5385)
          _type = ENUM_MB_TYPE.MB_TYPE_WARNING


        Else
          ' Register Alarms - Saving the alarm using the Alarm.Register
          _source_name = GLB_CurrentUser.Name & "@" & Environment.MachineName
          _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5392) & Me.Grid.Cell(Row, GRID_COLUMN_CGS_SESSION_NAME).Value

          Call Alarm.Register(AlarmSourceCode.User, GLB_CurrentUser.Id, _source_name, _
                              AlarmCode.User_CageSessionReopened, _
                             _description, AlarmSeverity.Warning, DateTime.MinValue, DbTrx.SqlTransaction)

          Call DbTrx.Commit()
          _message = GLB_NLS_GUI_PLAYER_TRACKING.Id(5384)
          _type = ENUM_MB_TYPE.MB_TYPE_INFO

        End If

      Else
        Call DbTrx.Rollback() ' Rollback if error

        _message = GLB_NLS_GUI_PLAYER_TRACKING.Id(5385)
        _type = ENUM_MB_TYPE.MB_TYPE_WARNING

      End If

    End Using
    ' mostramos el resultado por pantalla
    NLS_MsgBox(_message, _type)
  End Sub ' ReOpenCage

  Private Sub ShowCageSessionGlobalReport()
    Me.GetCageSessionId(False)
    Me.GetCageSessionNameAndStatus()

    Dim frm As frm_cage_sessions_report = New frm_cage_sessions_report(Me.m_cage_session_name, Me.m_cage_session_status)

    frm.ShowSelectedItem(Me.m_cage_session_id.ToString())

  End Sub

  Private Sub ShowCageSelectionGlobalReport()

    Me.GetCageFirstAndLastSessionName()
    Me.GetCageSessionIdsInGrid()

    Dim frm As frm_cage_sessions_report = New frm_cage_sessions_report(Me.m_first_session_name, Me.m_last_session_name)

    frm.ShowSelectedItem(Me.m_cage_sessions_in_grid)

  End Sub

#End Region ' Private Functions

#Region " Filters Behaviors "

  Private Sub opt_one_user_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_one_user.Click
    Me.cmb_user.Enabled = True
    Me.chk_show_all.Enabled = True
  End Sub ' opt_one_user_Click

  Private Sub opt_all_users_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_all_users.Click
    Me.cmb_user.Enabled = False
    Me.chk_show_all.Enabled = False
  End Sub ' opt_all_users_Click

  Private Sub chk_show_all_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_show_all.CheckedChanged
    If chk_show_all.Checked Then
      Call SetCombo(Me.cmb_user, "SELECT GU_USER_ID, GU_USERNAME FROM GUI_USERS ORDER BY GU_USERNAME")
    Else
      Call SetCombo(Me.cmb_user, "SELECT GU_USER_ID, GU_USERNAME FROM GUI_USERS WHERE GU_BLOCK_REASON = " & WSI.Common.GUI_USER_BLOCK_REASON.NONE & " ORDER BY GU_USERNAME")
    End If
  End Sub ' chk_show_all_CheckedChanged

  Private Sub opt_working_day_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_working_day.CheckedChanged
    If Me.opt_working_day.Checked Then
      Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
      Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
      Me.dtp_from.Width = 240
      Me.dtp_to.Width = 240
    Else
      Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      Me.dtp_from.Width = 240
      Me.dtp_to.Width = 240
    End If
  End Sub ' opt_working_day_CheckedChanged

#End Region ' Filters Behaviors

End Class