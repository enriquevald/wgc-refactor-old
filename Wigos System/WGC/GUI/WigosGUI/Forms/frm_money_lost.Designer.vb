<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_money_lost
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.btn_ok = New GUI_Controls.uc_button
    Me.btn_cancel = New GUI_Controls.uc_button
    Me.gb_num_lost_notes = New System.Windows.Forms.GroupBox
    Me.dg_money_lost = New System.Windows.Forms.DataGrid
    Me.panel_data.SuspendLayout()
    Me.gb_num_lost_notes.SuspendLayout()
    CType(Me.dg_money_lost, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_num_lost_notes)
    Me.panel_data.Size = New System.Drawing.Size(579, 129)
    '
    'btn_ok
    '
    Me.btn_ok.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_ok.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.btn_ok.Location = New System.Drawing.Point(0, 12)
    Me.btn_ok.Name = "btn_ok"
    Me.btn_ok.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_ok.Size = New System.Drawing.Size(90, 30)
    Me.btn_ok.TabIndex = 3
    Me.btn_ok.ToolTipped = False
    Me.btn_ok.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_cancel
    '
    Me.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btn_cancel.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.btn_cancel.Location = New System.Drawing.Point(0, 42)
    Me.btn_cancel.Name = "btn_cancel"
    Me.btn_cancel.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_cancel.Size = New System.Drawing.Size(90, 30)
    Me.btn_cancel.TabIndex = 4
    Me.btn_cancel.ToolTipped = False
    Me.btn_cancel.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'gb_num_lost_notes
    '
    Me.gb_num_lost_notes.Controls.Add(Me.dg_money_lost)
    Me.gb_num_lost_notes.Enabled = False
    Me.gb_num_lost_notes.Location = New System.Drawing.Point(4, 3)
    Me.gb_num_lost_notes.Name = "gb_num_lost_notes"
    Me.gb_num_lost_notes.Size = New System.Drawing.Size(575, 126)
    Me.gb_num_lost_notes.TabIndex = 11
    Me.gb_num_lost_notes.TabStop = False
    Me.gb_num_lost_notes.Text = "xNumLostNotes"
    '
    'dg_money_lost
    '
    Me.dg_money_lost.AllowDrop = True
    Me.dg_money_lost.AllowNavigation = False
    Me.dg_money_lost.CaptionFont = New System.Drawing.Font("Verdana", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.dg_money_lost.CaptionVisible = False
    Me.dg_money_lost.DataMember = ""
    Me.dg_money_lost.Dock = System.Windows.Forms.DockStyle.Fill
    Me.dg_money_lost.HeaderFont = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.dg_money_lost.HeaderForeColor = System.Drawing.SystemColors.ControlText
    Me.dg_money_lost.Location = New System.Drawing.Point(3, 17)
    Me.dg_money_lost.Name = "dg_money_lost"
    Me.dg_money_lost.Size = New System.Drawing.Size(569, 106)
    Me.dg_money_lost.TabIndex = 9
    '
    'frm_money_lost
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(682, 137)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_money_lost"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_money_lost"
    Me.panel_data.ResumeLayout(False)
    Me.gb_num_lost_notes.ResumeLayout(False)
    CType(Me.dg_money_lost, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)

  End Sub
  Private WithEvents btn_ok As GUI_Controls.uc_button
  Private WithEvents btn_cancel As GUI_Controls.uc_button
  Friend WithEvents gb_num_lost_notes As System.Windows.Forms.GroupBox
  Friend WithEvents dg_money_lost As System.Windows.Forms.DataGrid
End Class
