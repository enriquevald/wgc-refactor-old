<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_play_sessions_to_revise
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.dg_list_accounts = New GUI_Controls.uc_grid
    Me.dg_list_play_sessions = New GUI_Controls.uc_grid
    Me.ef_points_accounts = New GUI_Controls.uc_entry_field
    Me.btn_apply_accounts = New GUI_Controls.uc_button
    Me.panel_data.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.btn_apply_accounts)
    Me.panel_data.Controls.Add(Me.ef_points_accounts)
    Me.panel_data.Controls.Add(Me.dg_list_play_sessions)
    Me.panel_data.Controls.Add(Me.dg_list_accounts)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(615, 596)
    '
    'dg_list_accounts
    '
    Me.dg_list_accounts.CurrentCol = -1
    Me.dg_list_accounts.CurrentRow = -1
    Me.dg_list_accounts.Location = New System.Drawing.Point(0, 57)
    Me.dg_list_accounts.Name = "dg_list_accounts"
    Me.dg_list_accounts.PanelRightVisible = True
    Me.dg_list_accounts.Redraw = True
    Me.dg_list_accounts.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_list_accounts.Size = New System.Drawing.Size(592, 160)
    Me.dg_list_accounts.Sortable = False
    Me.dg_list_accounts.SortAscending = True
    Me.dg_list_accounts.SortByCol = 0
    Me.dg_list_accounts.TabIndex = 0
    Me.dg_list_accounts.ToolTipped = True
    Me.dg_list_accounts.TopRow = -2
    '
    'dg_list_play_sessions
    '
    Me.dg_list_play_sessions.CurrentCol = -1
    Me.dg_list_play_sessions.CurrentRow = -1
    Me.dg_list_play_sessions.Location = New System.Drawing.Point(3, 244)
    Me.dg_list_play_sessions.Name = "dg_list_play_sessions"
    Me.dg_list_play_sessions.PanelRightVisible = True
    Me.dg_list_play_sessions.Redraw = True
    Me.dg_list_play_sessions.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_list_play_sessions.Size = New System.Drawing.Size(589, 349)
    Me.dg_list_play_sessions.Sortable = False
    Me.dg_list_play_sessions.SortAscending = True
    Me.dg_list_play_sessions.SortByCol = 0
    Me.dg_list_play_sessions.TabIndex = 1
    Me.dg_list_play_sessions.ToolTipped = True
    Me.dg_list_play_sessions.TopRow = -2
    '
    'ef_points_accounts
    '
    Me.ef_points_accounts.DoubleValue = 0
    Me.ef_points_accounts.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_points_accounts.IntegerValue = 0
    Me.ef_points_accounts.IsReadOnly = False
    Me.ef_points_accounts.Location = New System.Drawing.Point(252, 16)
    Me.ef_points_accounts.Margin = New System.Windows.Forms.Padding(3, 3, 3, 2)
    Me.ef_points_accounts.Name = "ef_points_accounts"
    Me.ef_points_accounts.Size = New System.Drawing.Size(178, 25)
    Me.ef_points_accounts.SufixText = "Sufix Text"
    Me.ef_points_accounts.SufixTextVisible = True
    Me.ef_points_accounts.TabIndex = 3
    Me.ef_points_accounts.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_points_accounts.TextValue = ""
    Me.ef_points_accounts.Value = ""
    '
    'btn_apply_accounts
    '
    Me.btn_apply_accounts.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_apply_accounts.Location = New System.Drawing.Point(436, 16)
    Me.btn_apply_accounts.Name = "btn_apply_accounts"
    Me.btn_apply_accounts.Size = New System.Drawing.Size(90, 30)
    Me.btn_apply_accounts.TabIndex = 5
    Me.btn_apply_accounts.ToolTipped = False
    Me.btn_apply_accounts.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'frm_play_sessions_to_revise
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(719, 604)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_play_sessions_to_revise"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_play_sessions_to_revise"
    Me.panel_data.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents dg_list_play_sessions As GUI_Controls.uc_grid
  Friend WithEvents dg_list_accounts As GUI_Controls.uc_grid
  Friend WithEvents ef_points_accounts As GUI_Controls.uc_entry_field
  Friend WithEvents btn_apply_accounts As GUI_Controls.uc_button
End Class
