'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_gift_sel
'
' DESCRIPTION : Gifts Selection
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 22-SEP-2010  TJG    Initial version
' 18-OCT-2010  TJG    Gift stock 
' 04-JUN-2012  JAB    Eliminate top clause of sql query
' 25-APR-2013  RBG    #746 It makes no sense to work in this form with multiselection
' 3-MAY-2013   MAR    Took away the option for Draw Numbers
' 22-MAY-2013  ACM    Fixed Bug #794: Filter any occurrence in description. Not just the first characters
' 05-JUN-2013  DHA    Fixed Bug #819: Modifications in the query for filters with character '%'
' 11-JUN-2013  RBG    Fixed Bug #840: Add he anotati�n for column Requests in excel and print.
' 18-SEP-2013  DRV    Added Points for level
' 25-OCT-2013  MMG    Added Redemption Table filter
' 18-NOV-2013  MMG    Added code for disabling the rest of filters when the Redemption Table Name filter is being used.
' 19-FEB-2014  JPJ    Defect WIG-153: Added filter check in the points and the stock
' 07-APR-2014  JBP    Added only VIP filter.
' 07-MAY-2014  DRV    Fixed Bug WIG-902: Theres's no selection column
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data

Public Class frm_gift_sel
  Inherits frm_base_sel

#Region " Constants "

  ' DB Columns
  Private Const SQL_COLUMN_GIFT_ID As Integer = 0
  Private Const SQL_COLUMN_NAME As Integer = 1
  Private Const SQL_COLUMN_VIP As Integer = 2
  Private Const SQL_COLUMN_POINTS_LEVEL1 As Integer = 3
  Private Const SQL_COLUMN_POINTS_LEVEL2 As Integer = 4
  Private Const SQL_COLUMN_POINTS_LEVEL3 As Integer = 5
  Private Const SQL_COLUMN_POINTS_LEVEL4 As Integer = 6
  Private Const SQL_COLUMN_CONVERSION_TO_NRC As Integer = 7
  Private Const SQL_COLUMN_ENABLED As Integer = 8
  Private Const SQL_COLUMN_TYPE As Integer = 9
  Private Const SQL_COLUMN_CURRENT_STOCK As Integer = 10
  Private Const SQL_COLUMN_REQUEST_COUNTER As Integer = 11
  Private Const SQL_COLUMN_DELIVERY_COUNTER As Integer = 12
  Private Const SQL_COLUMN_CHANJE_TABLE As Integer = 13

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_CHANJE_TABLE As Integer = 1
  Private Const GRID_COLUMN_GIFT_ID As Integer = 2
  Private Const GRID_COLUMN_NAME As Integer = 3
  Private Const GRID_COLUMN_TYPE As Integer = 4
  Private Const GRID_COLUMN_STATUS As Integer = 5
  Private Const GRID_COLUMN_CURRENT_STOCK As Integer = 6
  Private Const GRID_COLUMN_REQUEST_COUNTER As Integer = 7
  Private Const GRID_COLUMN_DELIVERY_COUNTER As Integer = 8
  Private Const GRID_COLUMN_POINTS_LEVEL1 As Integer = 9
  Private Const GRID_COLUMN_POINTS_LEVEL2 As Integer = 10
  Private Const GRID_COLUMN_POINTS_LEVEL3 As Integer = 11
  Private Const GRID_COLUMN_POINTS_LEVEL4 As Integer = 12
  Private Const GRID_COLUMN_VIP As Integer = 13

  Private Const GRID_COLUMNS As Integer = 14
  Private Const GRID_HEADER_ROWS As Integer = 2

  Private Const FORM_DB_MIN_VERION As Short = 204
#End Region ' Constants

#Region " Members "

  ' For report filters 
  Private m_filter_name As String
  Private m_filter_redemption_table As String
  Private m_filter_status As String
  Private m_filter_points_from As String
  Private m_filter_points_to As String
  Private m_filter_stock_from As String
  Private m_filter_stock_to As String
  Private m_filter_type As String

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE : Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_GIFTS_SEL

    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERION)

    Call MyBase.GUI_SetFormId()
  End Sub

  ' PURPOSE : Initialize every form control
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Set form Name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(349)                      ' 349 "Cat�logo de Regalos"

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2)    ' New
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6) ' Cancel

    ' Gift Filters
    '   - Redemption Table
    '   - Name
    '   - Points
    '   - Stock
    '   - Type
    '   - Status
    '   - Legend

    '   - Redemption Table    
    Me.ef_redemption_table_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2826)
    Me.ef_redemption_table_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    '   - Name
    Me.ef_gift_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(339)         ' 339 "Regalo"
    Me.ef_gift_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    '   - Points
    Me.gb_points.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(340)            ' 340 "Rango de Puntos"
    Me.ef_points_from.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(341)       ' 341 "Desde"
    Me.ef_points_from.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8)
    Me.ef_points_to.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(342)         ' 342 "Hasta"
    Me.ef_points_to.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8)

    '   - Stock
    Me.gb_stock.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(383)             ' 383 "Nivel de Stock"
    Me.ef_stock_from.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(384)        ' 384 "Superior a"
    Me.ef_stock_from.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8)
    Me.ef_stock_to.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(385)          ' 385 "Inferior a"
    Me.ef_stock_to.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8)

    '   - Type                                                            
    Me.gb_gift_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(343)               ' 343 "Tipo"
    Me.chk_type_object.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(344)            ' 344 "Objeto"
    Me.chk_type_nrc.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(357)               ' 357 "Cr�ditos No Redimibles"
    Me.chk_type_redeem_credit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(313)     ' 313 "Cr�ditos Redimibles"
    Me.chk_type_services.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(396)          ' 396 "Servicios"

    '   - Status
    Me.gb_gift_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(346)             ' 346 "Estado"
    Me.chk_status_available.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(347)       ' 347 "Seleccionable"
    Me.chk_status_not_available.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(348)   ' 348 "No Seleccionable"

    '   - Legend
    Me.lbl_legend_01.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(389)        ' 389 "* Stock Disponible = Stock - Pedidos"
    Me.lbl_legend_02.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(390)        ' 390 "** Pedidos = Regalos en stock pendientes de ser entregados"

    ' VIP
    Me.chk_only_vip.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4804)         ' 4804 "Exclusivo para clientes VIP" 

    ' Grid
    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ' PURPOSE : Initialize all form filters with their default values
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()

  End Sub ' GUI_FilterReset

  ' PURPOSE : Activated when a row of the grid is double clicked or selected, so it can be edited
  '
  '  PARAMS :
  '     - INPUT :
  '               
  '     - OUTPUT :
  '          
  ' RETURNS :

  Protected Overrides Sub GUI_EditSelectedItem()

    Dim idx_row As Int32
    Dim gift_id As Integer
    Dim gift_name As String
    Dim frm_edit As Object

    ' Search the first row selected
    For idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(idx_row).IsSelected Then
        Exit For
      End If
    Next

    If idx_row = Me.Grid.NumRows Then
      Return
    End If

    ' Get the Promotion ID and Name, and launch the editor
    gift_id = Me.Grid.Cell(idx_row, GRID_COLUMN_GIFT_ID).Value
    gift_name = Me.Grid.Cell(idx_row, GRID_COLUMN_NAME).Value

    frm_edit = New frm_gift_edit

    Call frm_edit.ShowEditItem(gift_id, gift_name)

    frm_edit = Nothing

    Call Me.Grid.Focus()

  End Sub ' GUI_EditSelectedItem

  ' PURPOSE : Check for consistency values provided for every filter
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted

  Protected Overrides Function GUI_FilterCheck() As Boolean
    ' 19-FEB-2014  JPJ    Defect WIG-153: Added filter check in the points and the stock

    'We verify the point ranges
    If Not String.IsNullOrEmpty(ef_points_from.Value.ToString) AndAlso _
       Not String.IsNullOrEmpty(ef_points_to.Value.ToString) AndAlso _
       CType(ef_points_from.Value, Integer) > CType(ef_points_to.Value, Integer) Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1896), ENUM_MB_TYPE.MB_TYPE_INFO, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, ef_points_from.Text, ef_points_to.Text)
      ef_points_from.Focus()

      Return False
    End If

    'We verify the stock ranges
    If Not String.IsNullOrEmpty(ef_stock_from.Value.ToString) AndAlso _
       Not String.IsNullOrEmpty(ef_stock_to.Value.ToString) AndAlso _
       CType(ef_stock_from.Value, Integer) > CType(ef_stock_to.Value, Integer) Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1896), ENUM_MB_TYPE.MB_TYPE_INFO, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, ef_stock_from.Text, ef_stock_to.Text)
      ef_stock_from.Focus()

      Return False
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE : Build an SQL query from conditions set in the filters
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - SQL query text ready to send to the database

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _sql_query As String

    _sql_query = "SELECT   GI_GIFT_ID" _
                     + " , GI_NAME" _
                     + " , GI_VIP" _
                     + " , GI_POINTS_LEVEL1" _
                     + " , GI_POINTS_LEVEL2" _
                     + " , GI_POINTS_LEVEL3" _
                     + " , GI_POINTS_LEVEL4" _
                     + " , GI_CONVERSION_TO_NRC" _
                     + " , GI_AVAILABLE" _
                     + " , GI_TYPE" _
                     + " , GI_CURRENT_STOCK" _
                     + " , GI_REQUEST_COUNTER" _
                     + " , GI_DELIVERY_COUNTER" _
                     + " , PTC_NAME" _
                + " FROM   GIFTS" _
                + " LEFT   JOIN POINTS_TO_CREDITS ON PTC_POINTS_TO_CREDITS_ID = GI_POINTS_TO_CREDITS_ID"

    _sql_query = _sql_query + GetSqlWhere()
    _sql_query = _sql_query + " ORDER BY GI_POINTS_LEVEL1 ASC, GI_NAME ASC"

    Return _sql_query

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _gift_type As CLASS_GIFT.ENUM_GIFT_TYPE
    Dim _aux_str As String = ""
    Dim _aux_dec As Decimal
    Dim _yes_text As String
    Dim _no_text As String

    _yes_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
    _no_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)

    ' Assign Mapped columns (search for 'mapping' string in this file)
    Call MyBase.GUI_SetupRow(RowIndex, DbRow)

    Me.Grid.Cell(RowIndex, GRID_COLUMN_GIFT_ID).Value = DbRow.Value(SQL_COLUMN_GIFT_ID)

    If IsDBNull(DbRow.Value(SQL_COLUMN_CHANJE_TABLE)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CHANJE_TABLE).Value = String.Empty
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CHANJE_TABLE).Value = DbRow.Value(SQL_COLUMN_CHANJE_TABLE)
    End If

    '   - Type
    _gift_type = DbRow.Value(SQL_COLUMN_TYPE)

    '   - Points
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_POINTS_LEVEL1)) Then
      _aux_dec = DbRow.Value(SQL_COLUMN_POINTS_LEVEL1)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS_LEVEL1).Value = _aux_dec.ToString("#,##0")
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS_LEVEL1).Value = ""
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_POINTS_LEVEL2)) Then
      _aux_dec = DbRow.Value(SQL_COLUMN_POINTS_LEVEL2)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS_LEVEL2).Value = _aux_dec.ToString("#,##0")
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS_LEVEL2).Value = ""
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_POINTS_LEVEL3)) Then
      _aux_dec = DbRow.Value(SQL_COLUMN_POINTS_LEVEL3)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS_LEVEL3).Value = _aux_dec.ToString("#,##0")
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS_LEVEL3).Value = ""
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_POINTS_LEVEL4)) Then
      _aux_dec = DbRow.Value(SQL_COLUMN_POINTS_LEVEL4)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS_LEVEL4).Value = _aux_dec.ToString("#,##0")
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS_LEVEL4).Value = ""
    End If

    ' VIP
    Me.Grid.Cell(RowIndex, GRID_COLUMN_VIP).Value = _no_text

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_VIP)) Then
      If DbRow.Value(SQL_COLUMN_VIP) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_VIP).Value = _yes_text
      End If
    End If

    '   - Name
    Select Case _gift_type
      Case CLASS_GIFT.ENUM_GIFT_TYPE.OBJECT_GIFT _
         , CLASS_GIFT.ENUM_GIFT_TYPE.NOT_REDEEMABLE_CREDIT _
         , CLASS_GIFT.ENUM_GIFT_TYPE.REDEEMABLE_CREDIT _
         , CLASS_GIFT.ENUM_GIFT_TYPE.DRAW_NUMBERS _
         , CLASS_GIFT.ENUM_GIFT_TYPE.SERVICES
        ' AJQ & MBF - 06-OCT-2010 - the gift name is set on the GUI (code deleted)
        _aux_str = DbRow.Value(SQL_COLUMN_NAME).ToString()

      Case Else
        _aux_str = "UNKNOWN " + DbRow.Value(SQL_COLUMN_NAME).ToString() + " ???"

    End Select

    Me.Grid.Cell(RowIndex, GRID_COLUMN_NAME).Value = _aux_str

    '   - Type Name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE).Value = GetTypeName(_gift_type)

    '   - Enabled
    If DbRow.Value(SQL_COLUMN_ENABLED) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(347)   ' 347 "Disponible"
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(348)   ' 348 "No Disponible"
    End If

    '   - Stock (only for objects)
    _aux_str = ""
    If _gift_type = CLASS_GIFT.ENUM_GIFT_TYPE.OBJECT_GIFT Then
      _aux_dec = DbRow.Value(SQL_COLUMN_CURRENT_STOCK)
      _aux_str = _aux_dec.ToString("#,##0")
    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_CURRENT_STOCK).Value = _aux_str

    '   - Request counter (only for objects and services)
    _aux_str = ""
    If _gift_type = CLASS_GIFT.ENUM_GIFT_TYPE.OBJECT_GIFT _
       Or _gift_type = CLASS_GIFT.ENUM_GIFT_TYPE.SERVICES Then
      _aux_dec = DbRow.Value(SQL_COLUMN_REQUEST_COUNTER)
      _aux_str = _aux_dec.ToString("#,##0")
    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_REQUEST_COUNTER).Value = _aux_str

    '   - Delivery counter
    _aux_dec = DbRow.Value(SQL_COLUMN_DELIVERY_COUNTER)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DELIVERY_COUNTER).Value = _aux_dec.ToString("#,##0")

    Return True

  End Function ' GUI_SetupRow


  ' PURPOSE : Set focus to a control when first entering the form
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.ef_redemption_table_name

  End Sub 'GUI_SetInitialFocus

  ' PURPOSE : Process button actions in order to branch to a child screen
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_NEW
        Call ShowNewGiftForm()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        Call GUI_EditSelectedItem()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

#Region " GUI Reports "

  ' PURPOSE : Set proper values for form filters being sent to the report
  '
  '  PARAMS :
  '     - INPUT :
  '           - PrintData
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(339), m_filter_name)                ' 339 "Regalo"
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2826), m_filter_redemption_table)   ' 2826 "Redemption Table"
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(343), m_filter_type)                ' 343 "Tipo"
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(346), m_filter_status)              ' 346 "Estado"
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(382), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2050)) ' 382 "Pedidos"

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(377), m_filter_points_from)   ' 377 "Puntos Desde"
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(378), m_filter_points_to)     ' 378 "Puntos Hasta"

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(386), m_filter_stock_from)    ' 386 "Stock Desde"
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(387), m_filter_stock_to)      ' 387 "Stock Hasta"

    PrintData.FilterValueWidth(1) = 5000

  End Sub ' GUI_ReportFilter

  ' PURPOSE : Set form specific requirements/parameters forthe report
  '
  '  PARAMS :
  '     - INPUT :
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Params.Title = GLB_NLS_GUI_PLAYER_TRACKING.GetString(349)   ' 349 "Cat�logo de Regalos"

    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  ' PURPOSE : Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_ReportUpdateFilters()

    ' Gift Filters
    '   - Name
    '   - Redemption Table
    '   - Type
    '   - Points 
    '   - Stock
    '   - Status

    m_filter_name = ""
    m_filter_redemption_table = ""
    m_filter_type = ""
    m_filter_points_from = ""
    m_filter_points_to = ""
    m_filter_status = ""
    m_filter_stock_from = ""
    m_filter_stock_to = ""

    ' (If the Redemption Table filter is not empty, the other filters must be empty on the report)
    If Not String.IsNullOrEmpty(Me.ef_redemption_table_name.Value) Then
      '   - Redemption Table
      m_filter_redemption_table = Me.ef_redemption_table_name.Value

      '   - Name
      m_filter_name = ""

      '   - Type
      m_filter_type = ""

      '   - Points       
      m_filter_points_from = ""
      m_filter_points_to = ""

      '   - Stock
      m_filter_stock_from = ""
      m_filter_stock_to = ""

      '   - Status    
      m_filter_status = ""

    Else

      '   - Name
      If Me.ef_gift_name.Value <> "" Then
        m_filter_name = Me.ef_gift_name.Value
      End If

      '   - Type
      m_filter_type = SetGiftTypeFilter(False)

      '   - Points 
      If Me.ef_points_from.Value <> "" Then
        m_filter_points_from = Me.ef_points_from.Value
      End If

      If Me.ef_points_to.Value <> "" Then
        m_filter_points_to = Me.ef_points_to.Value
      End If

      '   - Stock
      If Me.ef_stock_from.Value <> "" Then
        m_filter_stock_from = Me.ef_stock_from.Value
      End If

      If Me.ef_stock_to.Value <> "" Then
        m_filter_stock_to = Me.ef_stock_to.Value
      End If

      '   - Status
      Me.m_filter_status = SetGiftSatusFilter(False)

    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE : Opens dialog with default settings for edit mode
  '
  '  PARAMS :
  '     - INPUT :

  '     - OUTPUT :

  '
  ' RETURNS :


  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE : Define layout of all Main Grid Columns 
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Sub GUI_StyleSheet()
    With Me.Grid

      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Columns
      '   - Index
      '   - Gift Id
      '   - Gift Name
      '   - Points
      '   - Gift Status
      '   - Current stock
      '   - Request Counter
      '   - Delivery Counter

      '   - Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      '   - Gift Id
      .Column(GRID_COLUMN_GIFT_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(339)   ' 339 "Regalo"
      .Column(GRID_COLUMN_GIFT_ID).Header(1).Text = " "
      .Column(GRID_COLUMN_GIFT_ID).Width = 0
      .Column(GRID_COLUMN_GIFT_ID).Mapping = SQL_COLUMN_GIFT_ID

      '   - Gift Name
      .Column(GRID_COLUMN_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(339)      ' 339 "Regalo"
      .Column(GRID_COLUMN_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(353)      ' 353 "Nom"
      .Column(GRID_COLUMN_NAME).Width = 4750
      .Column(GRID_COLUMN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      '   - Gift Type
      .Column(GRID_COLUMN_TYPE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(339)
      .Column(GRID_COLUMN_TYPE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(343)      ' 343 "Tipo"
      .Column(GRID_COLUMN_TYPE).Width = 2150
      .Column(GRID_COLUMN_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      '   - Redemption table
      .Column(GRID_COLUMN_CHANJE_TABLE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(339)
      .Column(GRID_COLUMN_CHANJE_TABLE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2826)      ' 343 "Tabla de canje"
      .Column(GRID_COLUMN_CHANJE_TABLE).Width = 2150
      .Column(GRID_COLUMN_CHANJE_TABLE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      '   - Points Level1
      .Column(GRID_COLUMN_POINTS_LEVEL1).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2622)     ' 2622 "Puntos por Nivel"
      .Column(GRID_COLUMN_POINTS_LEVEL1).Header(1).Text = GeneralParam.GetString("PlayerTracking", "Level01.Name")
      .Column(GRID_COLUMN_POINTS_LEVEL1).Width = 1290
      .Column(GRID_COLUMN_POINTS_LEVEL1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      '   - Points Level2
      .Column(GRID_COLUMN_POINTS_LEVEL2).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2622)     ' 2622 "Puntos por Nivel"
      .Column(GRID_COLUMN_POINTS_LEVEL2).Header(1).Text = GeneralParam.GetString("PlayerTracking", "Level02.Name")
      .Column(GRID_COLUMN_POINTS_LEVEL2).Width = 1290
      .Column(GRID_COLUMN_POINTS_LEVEL2).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      '   - Points Level3
      .Column(GRID_COLUMN_POINTS_LEVEL3).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2622)    ' 2622 "Puntos por Nivel"
      .Column(GRID_COLUMN_POINTS_LEVEL3).Header(1).Text = GeneralParam.GetString("PlayerTracking", "Level03.Name")
      .Column(GRID_COLUMN_POINTS_LEVEL3).Width = 1290
      .Column(GRID_COLUMN_POINTS_LEVEL3).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      '   - Points Level1
      .Column(GRID_COLUMN_POINTS_LEVEL4).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2622)    ' 2622 "Puntos por Nivel"
      .Column(GRID_COLUMN_POINTS_LEVEL4).Header(1).Text = GeneralParam.GetString("PlayerTracking", "Level04.Name")
      .Column(GRID_COLUMN_POINTS_LEVEL4).Width = 1290
      .Column(GRID_COLUMN_POINTS_LEVEL4).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      '   - VIP
      .Column(GRID_COLUMN_VIP).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_VIP).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4805)               ' 4805 "VIP"
      .Column(GRID_COLUMN_VIP).Width = 800
      .Column(GRID_COLUMN_VIP).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '   - Gift Status
      .Column(GRID_COLUMN_STATUS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(339)
      .Column(GRID_COLUMN_STATUS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(346)    ' 346 "Estado"
      .Column(GRID_COLUMN_STATUS).Width = 1500
      .Column(GRID_COLUMN_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '   - Current stock
      .Column(GRID_COLUMN_CURRENT_STOCK).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(339)
      .Column(GRID_COLUMN_CURRENT_STOCK).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(351)     ' 351 "Stock"
      .Column(GRID_COLUMN_CURRENT_STOCK).Width = 1300
      .Column(GRID_COLUMN_CURRENT_STOCK).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      '   - Request Counter
      .Column(GRID_COLUMN_REQUEST_COUNTER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(339)
      .Column(GRID_COLUMN_REQUEST_COUNTER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(382)   ' 382 "Pedidos"
      .Column(GRID_COLUMN_REQUEST_COUNTER).Width = 1300
      .Column(GRID_COLUMN_REQUEST_COUNTER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      '   - Delivery Counter
      .Column(GRID_COLUMN_DELIVERY_COUNTER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(339)
      .Column(GRID_COLUMN_DELIVERY_COUNTER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(388)  ' 388 "Entregados"
      .Column(GRID_COLUMN_DELIVERY_COUNTER).Width = 1300
      .Column(GRID_COLUMN_DELIVERY_COUNTER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

  End Sub 'GUI_StyleSheet

  ' PURPOSE : Set default values to filters
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Sub SetDefaultValues()

    Me.ef_gift_name.Value = ""

    Me.ef_redemption_table_name.Value = ""

    Me.ef_points_from.Value = ""
    Me.ef_points_to.Value = ""

    Me.ef_stock_from.Value = ""
    Me.ef_stock_to.Value = ""

    Me.chk_type_object.Checked = True
    Me.chk_type_nrc.Checked = True
    Me.chk_type_redeem_credit.Checked = True
    Me.chk_type_services.Checked = True

    Me.chk_status_available.Checked = True
    Me.chk_status_not_available.Checked = False
    Me.chk_only_vip.Checked = False

    Me.EnableFilters(True)

  End Sub ' SetDefaultValues

  ' PURPOSE : Set filter related to gift type
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Function SetGiftTypeFilter(ByVal SqlFilter As Boolean) As String

    Dim idx_item As Int32
    Dim idx_count As Int32
    Dim filter_string(0 To 4) As String
    Dim result_string As String

    idx_count = 0

    If Me.chk_type_object.Checked Then
      filter_string(idx_count) = IIf(SqlFilter, "GI_TYPE = " + CLASS_GIFT.ENUM_GIFT_TYPE.OBJECT_GIFT.ToString("D") _
                                              , GLB_NLS_GUI_PLAYER_TRACKING.GetString(344))      ' 344 "Objeto"
      idx_count = idx_count + 1
    End If

    If Me.chk_type_nrc.Checked Then
      filter_string(idx_count) = IIf(SqlFilter, "GI_TYPE = " + CLASS_GIFT.ENUM_GIFT_TYPE.NOT_REDEEMABLE_CREDIT.ToString("D") _
                                              , GLB_NLS_GUI_PLAYER_TRACKING.GetString(357))      ' 357 "Cr�ditos No Redimibles"
      idx_count = idx_count + 1
    End If

    If Me.chk_type_redeem_credit.Checked Then
      filter_string(idx_count) = IIf(SqlFilter, "GI_TYPE = " + CLASS_GIFT.ENUM_GIFT_TYPE.REDEEMABLE_CREDIT.ToString("D") _
                                              , GLB_NLS_GUI_PLAYER_TRACKING.GetString(313))      ' 313 "Cr�ditos Redimibles"
      idx_count = idx_count + 1
    End If

    If Me.chk_type_services.Checked Then
      filter_string(idx_count) = IIf(SqlFilter, "GI_TYPE = " + CLASS_GIFT.ENUM_GIFT_TYPE.SERVICES.ToString("D") _
                                              , GLB_NLS_GUI_PLAYER_TRACKING.GetString(396))      ' 396 "Servicios"
      idx_count = idx_count + 1
    End If

    If idx_count < 1 Or idx_count > 4 Then
      Return ""
    End If

    If SqlFilter Then
      result_string = " AND (" + filter_string(0)

      For idx_item = 1 To idx_count - 1
        result_string = result_string + " OR " + filter_string(idx_item)
      Next

      result_string = result_string + ")"
    Else
      result_string = filter_string(0)

      For idx_item = 1 To idx_count - 1
        result_string = result_string + ", " + filter_string(idx_item)
      Next
    End If

    Return result_string

  End Function ' SetGiftTypeFilter

  ' PURPOSE : Set filter related to gift status
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Function SetGiftSatusFilter(ByVal SqlFilter As Boolean) As String

    Dim idx_item As Int32
    Dim idx_count As Int32
    Dim filter_string(0 To 2) As String
    Dim result_string As String

    idx_count = 0

    If Me.chk_status_available.Checked Then
      filter_string(idx_count) = IIf(SqlFilter, "GI_AVAILABLE = " + CLASS_GIFT.ENUM_GIFT_STATUS.AVAILABLE.ToString("D") _
                                              , GLB_NLS_GUI_PLAYER_TRACKING.GetString(347))      ' Available
      idx_count = idx_count + 1
    End If

    If Me.chk_status_not_available.Checked Then
      filter_string(idx_count) = IIf(SqlFilter, "GI_AVAILABLE = " + CLASS_GIFT.ENUM_GIFT_STATUS.NOT_AVAILABLE.ToString("D") _
                                              , GLB_NLS_GUI_PLAYER_TRACKING.GetString(348))      ' Not available
      idx_count = idx_count + 1
    End If

    If idx_count < 1 Or idx_count > 2 Then
      Return ""
    End If

    If SqlFilter Then
      result_string = " AND (" + filter_string(0)

      For idx_item = 1 To idx_count - 1
        result_string = result_string + " OR " + filter_string(idx_item)
      Next

      result_string = result_string + ")"
    Else
      result_string = filter_string(0)

      For idx_item = 1 To idx_count - 1
        result_string = result_string + ", " + filter_string(idx_item)
      Next
    End If

    Return result_string

  End Function ' SetGiftSatusFilter

  ' PURPOSE : Build the variable part of the WHERE clause (the one that depends on filter values) for the
  '          main SQL Query.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Function GetSqlWhere() As String

    Dim str_where As String = ""

    ' Gift Filters
    '   - Redemption Table
    '   - Name
    '   - Points
    '   - Available Stock
    '   - Type
    '   - Status

    '   - Redemption Table (If this filter is active, don't filter by any other filter)
    If Not String.IsNullOrEmpty(Me.ef_redemption_table_name.Value) Then
      str_where = str_where + " AND " & GUI_FilterField("PTC_NAME", Me.ef_redemption_table_name.Value, False, False, True)

    Else

      '   - Name
      If Me.ef_gift_name.Value <> "" Then
        str_where = str_where + " AND " & GUI_FilterField("GI_NAME", Me.ef_gift_name.Value, False, False, True)
      End If

      '   - Vip
      If Me.chk_only_vip.Checked Then
        str_where = str_where + " AND GI_VIP = 1"
      End If

      '   - Points
      If Me.ef_points_from.Value <> "" And Me.ef_points_to.Value <> "" Then
        str_where = str_where & " AND (  GI_POINTS_LEVEL1 >= " & Me.ef_points_from.Value & " AND GI_POINTS_LEVEL1 <= " & Me.ef_points_to.Value & " OR " & _
                                "        GI_POINTS_LEVEL2 >= " & Me.ef_points_from.Value & " AND GI_POINTS_LEVEL2 <= " & Me.ef_points_to.Value & " OR " & _
                                "        GI_POINTS_LEVEL3 >= " & Me.ef_points_from.Value & " AND GI_POINTS_LEVEL3 <= " & Me.ef_points_to.Value & " OR " & _
                                "        GI_POINTS_LEVEL4 >= " & Me.ef_points_from.Value & " AND GI_POINTS_LEVEL4 <= " & Me.ef_points_to.Value & " )"
      Else
        If Me.ef_points_from.Value <> "" Then
          str_where = str_where & " AND ( GI_POINTS_LEVEL1 >= " & Me.ef_points_from.Value.ToString & " OR " & _
                                  "       GI_POINTS_LEVEL2 >= " & Me.ef_points_from.Value.ToString & " OR " & _
                                  "       GI_POINTS_LEVEL3 >= " & Me.ef_points_from.Value.ToString & " OR " & _
                                  "       GI_POINTS_LEVEL4 >= " & Me.ef_points_from.Value.ToString & " )  "

        End If

        If Me.ef_points_to.Value <> "" Then
          str_where = str_where & " AND ( GI_POINTS_LEVEL1 <= " & Me.ef_points_to.Value.ToString & " OR " & _
                                  "       GI_POINTS_LEVEL2 <= " & Me.ef_points_to.Value.ToString & " OR " & _
                                  "       GI_POINTS_LEVEL3 <= " & Me.ef_points_to.Value.ToString & " OR " & _
                                  "       GI_POINTS_LEVEL4 <= " & Me.ef_points_to.Value.ToString & " )  "
        End If
      End If
      '   - Available Stock
      If Me.ef_stock_from.Value <> "" Then
        str_where = str_where + " AND ( GI_CURRENT_STOCK - GI_REQUEST_COUNTER ) >= " + Me.ef_stock_from.Value.ToString
      End If

      If Me.ef_stock_to.Value <> "" Then
        str_where = str_where + " AND ( GI_CURRENT_STOCK - GI_REQUEST_COUNTER ) <= " + Me.ef_stock_to.Value.ToString
      End If

      '   - Type
      str_where = str_where + SetGiftTypeFilter(True)

      '   - Status
      If Me.chk_status_available.Checked And Not Me.chk_status_not_available.Checked Then
        str_where = str_where + " AND GI_AVAILABLE = " + CLASS_GIFT.ENUM_GIFT_STATUS.AVAILABLE.ToString("D")
      End If

      If Not Me.chk_status_available.Checked And Me.chk_status_not_available.Checked Then
        str_where = str_where + " AND GI_AVAILABLE = " + CLASS_GIFT.ENUM_GIFT_STATUS.NOT_AVAILABLE.ToString("D")
      End If

    End If

    ' Final processing
    If Len(str_where) > 0 Then
      ' Repleace the leading ' AND ' by ' WHERE ' 
      str_where = " WHERE " + Strings.Right(str_where, Len(str_where) - 5)
    End If

    Return str_where
  End Function ' GetSqlWhere

  ' PURPOSE : Adds a new promotion to the system
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Sub ShowNewGiftForm()

    Dim frm_edit As frm_gift_edit

    frm_edit = New frm_gift_edit
    Call frm_edit.ShowNewItem()

    frm_edit = Nothing

    Call Me.Grid.Focus()

  End Sub ' ShowNewGiftForm

  ' PURPOSE : Return the type name string
  '
  '  PARAMS :
  '     - INPUT : Type enum
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - String with the type name

  Private Function GetTypeName(ByVal Type As CLASS_GIFT.ENUM_GIFT_TYPE) As String
    Dim type_name As String

    type_name = "UNKNOWN"

    Select Case Type
      Case CLASS_GIFT.ENUM_GIFT_TYPE.OBJECT_GIFT
        type_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(344) ' 344 "Objeto"
      Case CLASS_GIFT.ENUM_GIFT_TYPE.NOT_REDEEMABLE_CREDIT
        type_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(357) ' 357 "Cr�ditos No Redimibles"
      Case CLASS_GIFT.ENUM_GIFT_TYPE.REDEEMABLE_CREDIT
        type_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(313) ' 313 "Cr�ditos Redimibles"
      Case CLASS_GIFT.ENUM_GIFT_TYPE.DRAW_NUMBERS
        type_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(391) ' 391 "Boletos de Sorteo"
      Case CLASS_GIFT.ENUM_GIFT_TYPE.SERVICES
        type_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(396) ' 396 "Servicios"
    End Select

    Return type_name

  End Function ' GetTypeName

  ' PURPOSE : Enable/Disable the filters (except the Redeption Table Name filter)
  '
  '  PARAMS :
  '     - INPUT : Boolean indicating if it must enable (true) or disable (false) the filters
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - String with the type name

  Private Sub EnableFilters(ByVal EnableFilters As Boolean)
    Me.ef_gift_name.Enabled = EnableFilters
    Me.gb_gift_status.Enabled = EnableFilters
    Me.gb_gift_type.Enabled = EnableFilters
    Me.gb_points.Enabled = EnableFilters
    Me.gb_stock.Enabled = EnableFilters

  End Sub ' EnableFilters



#End Region ' Private Functions

#Region "Events"

  Private Sub ef_redemption_table_name_EntryFieldValueChanged() Handles ef_redemption_table_name.EntryFieldValueChanged
    Dim _enable_filters As Boolean

    _enable_filters = True

    If Not String.IsNullOrEmpty(Me.ef_redemption_table_name.TextValue) Then
      _enable_filters = False

    End If

    Call EnableFilters(_enable_filters)

  End Sub

#End Region ' Events

End Class