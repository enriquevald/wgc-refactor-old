<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_gaming_tables_sessions
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_gaming_tables_sessions))
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.opt_opening_session = New System.Windows.Forms.RadioButton()
    Me.opt_movement_session = New System.Windows.Forms.RadioButton()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.fra_session_state = New System.Windows.Forms.GroupBox()
    Me.chk_session_closed = New System.Windows.Forms.CheckBox()
    Me.chk_session_open = New System.Windows.Forms.CheckBox()
    Me.gb_table_type = New System.Windows.Forms.GroupBox()
    Me.opt_all_types = New System.Windows.Forms.RadioButton()
    Me.opt_several_types = New System.Windows.Forms.RadioButton()
    Me.dg_filter = New GUI_Controls.uc_grid()
    Me.gb_area_island = New System.Windows.Forms.GroupBox()
    Me.cmb_area = New GUI_Controls.uc_combo()
    Me.cmb_bank = New GUI_Controls.uc_combo()
    Me.ef_smoking = New GUI_Controls.uc_entry_field()
    Me.gb_enabled = New System.Windows.Forms.GroupBox()
    Me.chk_no = New System.Windows.Forms.CheckBox()
    Me.chk_yes = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.fra_session_state.SuspendLayout()
    Me.gb_table_type.SuspendLayout()
    Me.gb_area_island.SuspendLayout()
    Me.gb_enabled.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_enabled)
    Me.panel_filter.Controls.Add(Me.gb_area_island)
    Me.panel_filter.Controls.Add(Me.gb_table_type)
    Me.panel_filter.Controls.Add(Me.fra_session_state)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Size = New System.Drawing.Size(1196, 148)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.fra_session_state, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_table_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_area_island, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_enabled, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 152)
    Me.panel_data.Size = New System.Drawing.Size(1196, 497)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1190, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1190, 4)
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.opt_opening_session)
    Me.gb_date.Controls.Add(Me.opt_movement_session)
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(6, 6)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(257, 131)
    Me.gb_date.TabIndex = 0
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'opt_opening_session
    '
    Me.opt_opening_session.Location = New System.Drawing.Point(18, 14)
    Me.opt_opening_session.Name = "opt_opening_session"
    Me.opt_opening_session.Size = New System.Drawing.Size(210, 24)
    Me.opt_opening_session.TabIndex = 0
    Me.opt_opening_session.Text = "xSearchByOpeningDate"
    '
    'opt_movement_session
    '
    Me.opt_movement_session.Location = New System.Drawing.Point(18, 40)
    Me.opt_movement_session.Name = "opt_movement_session"
    Me.opt_movement_session.Size = New System.Drawing.Size(210, 24)
    Me.opt_movement_session.TabIndex = 1
    Me.opt_movement_session.Text = "xSearchByMovementsDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 100)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(240, 25)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 3
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 76)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(240, 25)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 2
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'fra_session_state
    '
    Me.fra_session_state.Controls.Add(Me.chk_session_closed)
    Me.fra_session_state.Controls.Add(Me.chk_session_open)
    Me.fra_session_state.Location = New System.Drawing.Point(576, 6)
    Me.fra_session_state.Name = "fra_session_state"
    Me.fra_session_state.Size = New System.Drawing.Size(153, 72)
    Me.fra_session_state.TabIndex = 2
    Me.fra_session_state.TabStop = False
    Me.fra_session_state.Text = "xState"
    '
    'chk_session_closed
    '
    Me.chk_session_closed.Location = New System.Drawing.Point(18, 45)
    Me.chk_session_closed.Name = "chk_session_closed"
    Me.chk_session_closed.Size = New System.Drawing.Size(129, 16)
    Me.chk_session_closed.TabIndex = 1
    Me.chk_session_closed.Text = "xClosed"
    '
    'chk_session_open
    '
    Me.chk_session_open.Location = New System.Drawing.Point(18, 19)
    Me.chk_session_open.Name = "chk_session_open"
    Me.chk_session_open.Size = New System.Drawing.Size(129, 16)
    Me.chk_session_open.TabIndex = 0
    Me.chk_session_open.Text = "xOpen"
    '
    'gb_table_type
    '
    Me.gb_table_type.Controls.Add(Me.opt_all_types)
    Me.gb_table_type.Controls.Add(Me.opt_several_types)
    Me.gb_table_type.Controls.Add(Me.dg_filter)
    Me.gb_table_type.Location = New System.Drawing.Point(269, 6)
    Me.gb_table_type.Name = "gb_table_type"
    Me.gb_table_type.Size = New System.Drawing.Size(301, 131)
    Me.gb_table_type.TabIndex = 1
    Me.gb_table_type.TabStop = False
    Me.gb_table_type.Text = "xTableType"
    '
    'opt_all_types
    '
    Me.opt_all_types.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.opt_all_types.Location = New System.Drawing.Point(8, 45)
    Me.opt_all_types.Name = "opt_all_types"
    Me.opt_all_types.Size = New System.Drawing.Size(64, 24)
    Me.opt_all_types.TabIndex = 1
    Me.opt_all_types.Text = "xAll"
    '
    'opt_several_types
    '
    Me.opt_several_types.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.opt_several_types.Location = New System.Drawing.Point(8, 17)
    Me.opt_several_types.Name = "opt_several_types"
    Me.opt_several_types.Size = New System.Drawing.Size(72, 24)
    Me.opt_several_types.TabIndex = 0
    Me.opt_several_types.Text = "xSeveral"
    '
    'dg_filter
    '
    Me.dg_filter.CurrentCol = -1
    Me.dg_filter.CurrentRow = -1
    Me.dg_filter.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_filter.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_filter.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_filter.Location = New System.Drawing.Point(86, 16)
    Me.dg_filter.Name = "dg_filter"
    Me.dg_filter.PanelRightVisible = False
    Me.dg_filter.Redraw = True
    Me.dg_filter.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_filter.Size = New System.Drawing.Size(206, 108)
    Me.dg_filter.Sortable = False
    Me.dg_filter.SortAscending = True
    Me.dg_filter.SortByCol = 0
    Me.dg_filter.TabIndex = 2
    Me.dg_filter.ToolTipped = True
    Me.dg_filter.TopRow = -2
    Me.dg_filter.WordWrap = False
    '
    'gb_area_island
    '
    Me.gb_area_island.Controls.Add(Me.cmb_area)
    Me.gb_area_island.Controls.Add(Me.cmb_bank)
    Me.gb_area_island.Controls.Add(Me.ef_smoking)
    Me.gb_area_island.Location = New System.Drawing.Point(735, 6)
    Me.gb_area_island.Name = "gb_area_island"
    Me.gb_area_island.Size = New System.Drawing.Size(351, 100)
    Me.gb_area_island.TabIndex = 4
    Me.gb_area_island.TabStop = False
    Me.gb_area_island.Text = "xLocation"
    '
    'cmb_area
    '
    Me.cmb_area.AllowUnlistedValues = False
    Me.cmb_area.AutoCompleteMode = False
    Me.cmb_area.IsReadOnly = False
    Me.cmb_area.Location = New System.Drawing.Point(11, 12)
    Me.cmb_area.Name = "cmb_area"
    Me.cmb_area.SelectedIndex = -1
    Me.cmb_area.Size = New System.Drawing.Size(327, 24)
    Me.cmb_area.SufixText = "Sufix Text"
    Me.cmb_area.SufixTextVisible = True
    Me.cmb_area.TabIndex = 0
    Me.cmb_area.TextCombo = Nothing
    Me.cmb_area.TextWidth = 120
    '
    'cmb_bank
    '
    Me.cmb_bank.AllowUnlistedValues = False
    Me.cmb_bank.AutoCompleteMode = False
    Me.cmb_bank.IsReadOnly = False
    Me.cmb_bank.Location = New System.Drawing.Point(11, 70)
    Me.cmb_bank.Name = "cmb_bank"
    Me.cmb_bank.SelectedIndex = -1
    Me.cmb_bank.Size = New System.Drawing.Size(327, 24)
    Me.cmb_bank.SufixText = "Sufix Text"
    Me.cmb_bank.SufixTextVisible = True
    Me.cmb_bank.TabIndex = 2
    Me.cmb_bank.TextCombo = Nothing
    Me.cmb_bank.TextWidth = 120
    '
    'ef_smoking
    '
    Me.ef_smoking.DoubleValue = 0.0R
    Me.ef_smoking.IntegerValue = 0
    Me.ef_smoking.IsReadOnly = False
    Me.ef_smoking.Location = New System.Drawing.Point(11, 40)
    Me.ef_smoking.Name = "ef_smoking"
    Me.ef_smoking.PlaceHolder = Nothing
    Me.ef_smoking.Size = New System.Drawing.Size(327, 24)
    Me.ef_smoking.SufixText = "Sufix Text"
    Me.ef_smoking.SufixTextVisible = True
    Me.ef_smoking.TabIndex = 1
    Me.ef_smoking.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_smoking.TextValue = ""
    Me.ef_smoking.TextWidth = 120
    Me.ef_smoking.Value = ""
    Me.ef_smoking.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_enabled
    '
    Me.gb_enabled.Controls.Add(Me.chk_no)
    Me.gb_enabled.Controls.Add(Me.chk_yes)
    Me.gb_enabled.Location = New System.Drawing.Point(576, 84)
    Me.gb_enabled.Name = "gb_enabled"
    Me.gb_enabled.Size = New System.Drawing.Size(153, 53)
    Me.gb_enabled.TabIndex = 3
    Me.gb_enabled.TabStop = False
    Me.gb_enabled.Text = "xEnabled"
    '
    'chk_no
    '
    Me.chk_no.AutoSize = True
    Me.chk_no.Location = New System.Drawing.Point(79, 24)
    Me.chk_no.Name = "chk_no"
    Me.chk_no.Size = New System.Drawing.Size(48, 17)
    Me.chk_no.TabIndex = 3
    Me.chk_no.Text = "xNo"
    Me.chk_no.UseVisualStyleBackColor = True
    '
    'chk_yes
    '
    Me.chk_yes.AutoSize = True
    Me.chk_yes.Location = New System.Drawing.Point(11, 24)
    Me.chk_yes.Name = "chk_yes"
    Me.chk_yes.Size = New System.Drawing.Size(52, 17)
    Me.chk_yes.TabIndex = 2
    Me.chk_yes.Text = "xYes"
    Me.chk_yes.UseVisualStyleBackColor = True
    '
    'frm_gaming_tables_sessions
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1204, 653)
    Me.Name = "frm_gaming_tables_sessions"
    Me.Text = "frm_gamingtables_sessions"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.fra_session_state.ResumeLayout(False)
    Me.gb_table_type.ResumeLayout(False)
    Me.gb_area_island.ResumeLayout(False)
    Me.gb_enabled.ResumeLayout(False)
    Me.gb_enabled.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents opt_opening_session As System.Windows.Forms.RadioButton
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents fra_session_state As System.Windows.Forms.GroupBox
  Friend WithEvents chk_session_closed As System.Windows.Forms.CheckBox
  Friend WithEvents chk_session_open As System.Windows.Forms.CheckBox
  Friend WithEvents opt_movement_session As System.Windows.Forms.RadioButton
  Friend WithEvents gb_table_type As System.Windows.Forms.GroupBox
  Friend WithEvents opt_several_types As System.Windows.Forms.RadioButton
  Friend WithEvents dg_filter As GUI_Controls.uc_grid
  Friend WithEvents gb_area_island As System.Windows.Forms.GroupBox
  Friend WithEvents cmb_area As GUI_Controls.uc_combo
  Friend WithEvents cmb_bank As GUI_Controls.uc_combo
  Friend WithEvents ef_smoking As GUI_Controls.uc_entry_field
  Friend WithEvents gb_enabled As System.Windows.Forms.GroupBox
  Friend WithEvents chk_no As System.Windows.Forms.CheckBox
  Friend WithEvents chk_yes As System.Windows.Forms.CheckBox
  Friend WithEvents opt_all_types As System.Windows.Forms.RadioButton
End Class
