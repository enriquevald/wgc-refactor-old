﻿
'-------------------------------------------------------------------
' Copyright © 2007-2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_report_task
' DESCRIPTION:   Report task to smartfloor
' AUTHOR:        Samuel González
' CREATION DATE: 22-OCT-2015
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 22-OCT-2015  SGB    Initial version
' 11-NOV-2015  SGB    Backlog Item 6322: Changes columns
' 13-NOV-2015  SGB    Backlog Item 6322: Add new columns
' 16-NOV-2015  SGB    Backlog Item 6322: NLS's change

Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common
Imports System.Text
Imports System.Runtime.InteropServices
Imports System.Data
Imports System.Data.SqlClient
Imports WSI.Common.CollectionImport
Imports WSI.Common.Misc

Public Class frm_smartfloor_report_task
  Inherits frm_base_sel

#Region " Enums "

#End Region ' Enums

#Region " Constants "
  Private Const GRID_COLUMNS As Integer = 27
  Private Const GRID_HEADER_ROWS As Integer = 2

  'DB Columns Alarm
  Private Const SQL_COLUMN_ALARM_SECTION_TYPE As Integer = 0
  Private Const SQL_COLUMN_ALARM_CATEGORY_NAME As Integer = 1
  Private Const SQL_COLUMN_ALARM_GROUP_NAME As Integer = 2
  Private Const SQL_COLUMN_ALARM_CODE As Integer = 3

  'DB Columns
  Private Const SQL_COLUMN_ALARM_ID As Integer = 0
  Private Const SQL_COLUMN_RANGE_ID As Integer = 1
  Private Const SQL_COLUMN_DATE_CREATED As Integer = 2
  Private Const SQL_COLUMN_ALARM_DESC As Integer = 3
  Private Const SQL_COLUMN_ALARM_SOURCE As Integer = 4
  Private Const SQL_COLUMN_STATUS As Integer = 5
  Private Const SQL_COLUMN_SEVERITY As Integer = 6
  Private Const SQL_COLUMN_ASIGNED As Integer = 7
  Private Const SQL_COLUMN_ACCEPTED As Integer = 8
  Private Const SQL_COLUMN_SOLVED As Integer = 9
  Private Const SQL_COLUMN_VALIDATE As Integer = 10
  Private Const SQL_COLUMN_TIME_ASIGNED As Integer = 11
  Private Const SQL_COLUMN_TIME_ACCEPTED As Integer = 12
  Private Const SQL_COLUMN_TIME_SOLVED As Integer = 13
  Private Const SQL_COLUMN_TIME_VALIDATED As Integer = 14
  Private Const SQL_COLUMN_TIME_TOTAL As Integer = 15
  Private Const SQL_COLUMN_USER_NAME_SOURCE As Integer = 16
  Private Const SQL_COLUMN_USER_NAME_STATUS As Integer = 17

  'Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_ALARM_ID As Integer = 1
  Private Const GRID_COLUMN_ALARM_DESC As Integer = 2
  Private Const GRID_COLUMN_ALARM_SOURCE As Integer = 3
  Private Const GRID_COLUMN_STATUS As Integer = 4
  Private Const GRID_COLUMN_USER_NAME As Integer = 5
  Private Const GRID_COLUMN_SEVERITY As Integer = 6
  Private Const GRID_COLUMN_DATE_CREATED As Integer = 7
  Private Const GRID_COLUMN_ASIGNED As Integer = 8
  Private Const GRID_COLUMN_ACCEPTED As Integer = 9
  Private Const GRID_COLUMN_SOLVED As Integer = 10
  Private Const GRID_COLUMN_VALIDATE As Integer = 11
  Private Const GRID_COLUMN_TIME_ASIGNED As Integer = 12
  Private Const GRID_COLUMN_TIME_ACCEPTED As Integer = 13
  Private Const GRID_COLUMN_TIME_SOLVED As Integer = 14
  Private Const GRID_COLUMN_TIME_VALIDATED As Integer = 15
  Private Const GRID_COLUMN_TIME_TOTAL As Integer = 16
  Private Const GRID_COLUMN_COUNT_TIME_ASIGNED As Integer = 17
  Private Const GRID_COLUMN_AVG_TIME_ASIGNED As Integer = 18
  Private Const GRID_COLUMN_COUNT_TIME_ACCEPTED As Integer = 19
  Private Const GRID_COLUMN_AVG_TIME_ACCEPTED As Integer = 20
  Private Const GRID_COLUMN_COUNT_TIME_SOLVED As Integer = 21
  Private Const GRID_COLUMN_AVG_TIME_SOLVED As Integer = 22
  Private Const GRID_COLUMN_COUNT_TIME_VALIDATED As Integer = 23
  Private Const GRID_COLUMN_AVG_TIME_VALIDATED As Integer = 24
  Private Const GRID_COLUMN_COUNT_TIME_TOTAL As Integer = 25
  Private Const GRID_COLUMN_AVG_TIME_TOTAL As Integer = 26

  ' Width
  Private Const GRID_WIDTH_ALARM_ID As Integer = 0
  Private Const GRID_WIDTH_ALARM_DESC As Integer = 3200
  Private Const GRID_WIDTH_DATE_CREATED As Integer = 1950
  Private Const GRID_WIDTH_ALARM_SOURCE As Integer = 1200
  Private Const GRID_WIDTH_STATUS As Integer = 1200
  Private Const GRID_WIDTH_USER_NAME As Integer = 2400
  Private Const GRID_WIDTH_SEVERITY As Integer = 1100
  Private Const GRID_WIDTH_ASIGNED As Integer = 1950
  Private Const GRID_WIDTH_ACCEPTED As Integer = 1950
  Private Const GRID_WIDTH_SOLVED As Integer = 1950
  Private Const GRID_WIDTH_VALIDATE As Integer = 1950
  Private Const GRID_WIDTH_TIME_ASIGNED As Integer = 1400
  Private Const GRID_WIDTH_TIME_ACCEPTED As Integer = 1400
  Private Const GRID_WIDTH_TIME_SOLVED As Integer = 1400
  Private Const GRID_WIDTH_TIME_VALIDATED As Integer = 1400
  Private Const GRID_WIDTH_TIME_TOTAL As Integer = 1400
  Private Const GRID_WIDTH_AVG_TIME_ASIGNED As Integer = 1400
  Private Const GRID_WIDTH_AVG_TIME_ACCEPTED As Integer = 1400
  Private Const GRID_WIDTH_AVG_TIME_SOLVED As Integer = 1400
  Private Const GRID_WIDTH_AVG_TIME_VALIDATED As Integer = 1400
  Private Const GRID_WIDTH_AVG_TIME_TOTAL As Integer = 1400
  Private Const GRID_WIDTH_COUNT_TIME_ASIGNED As Integer = 700
  Private Const GRID_WIDTH_COUNT_TIME_ACCEPTED As Integer = 700
  Private Const GRID_WIDTH_COUNT_TIME_SOLVED As Integer = 700
  Private Const GRID_WIDTH_COUNT_TIME_VALIDATED As Integer = 700
  Private Const GRID_WIDTH_COUNT_TIME_TOTAL As Integer = 700

  Private Const START_RANGE_GAME_ALARMS As String = "135169"
  Private Const END_RANGE_GAME_ALARMS As String = "139263"

  'DT Columns Totals
  Private Const DT_COLUMN_TOTALS_GROUP_NAME As Integer = 0
  Private Const DT_COLUMN_TOTALS_ASIGNED As Integer = 1
  Private Const DT_COLUMN_TOTALS_ACCEPTED As Integer = 2
  Private Const DT_COLUMN_TOTALS_SOLVED As Integer = 3
  Private Const DT_COLUMN_TOTALS_VALIDATED As Integer = 4
  Private Const DT_COLUMN_TOTALS_TOTAL As Integer = 5
  Private Const DT_COLUMN_COUNT_ASIGNED As Integer = 6
  Private Const DT_COLUMN_COUNT_ACCEPTED As Integer = 7
  Private Const DT_COLUMN_COUNT_SOLVED As Integer = 8
  Private Const DT_COLUMN_COUNT_VALIDATED As Integer = 9
  Private Const DT_COLUMN_COUNT_TOTAL As Integer = 10
  Private Const DT_COLUMN_AVG_ASIGNED As Integer = 11
  Private Const DT_COLUMN_AVG_ACCEPTED As Integer = 12
  Private Const DT_COLUMN_AVG_SOLVED As Integer = 13
  Private Const DT_COLUMN_AVG_VALIDATED As Integer = 14
  Private Const DT_COLUMN_AVG_TOTAL As Integer = 15

#End Region ' Constants

#Region " Members "

  ' reportFilter
  Private m_date_by As String
  Private m_filter_from As String
  Private m_filter_to As String

  Private m_show_all As String

  Private m_selected_alarms As String

  Private m_severity As List(Of String)
  Private m_state As List(Of String)

  ' For grid totalizators
  Dim m_total_points As Double
  Dim m_totals_data_table As DataTable

  'Show all
  Dim m_show_all_grid As Boolean

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_SMARTFLOOR_REPORT_TASK
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6852)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_INFO).Visible = False

    'Filter
    ' Date time filter
    Me.uc_dsl.Init(GLB_NLS_GUI_INVOICING.Id(201), True)

    'Date
    Me.gb_date.Text = GLB_NLS_GUI_INVOICING.GetString(201)
    Me.opt_date_resolution.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6855)
    Me.opt_date_creation.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6856)

    'Alarms catalog
    uc_checked_list_alarms_catalog.GroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6874)
    uc_checked_list_alarms_catalog.SetLevels = 3
    uc_checked_list_alarms_catalog.btn_check_all.Width = 100
    uc_checked_list_alarms_catalog.btn_uncheck_all.Width = 115

    'show all
    Me.chk_show_all.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6861)

    'Severity
    Me.gb_severity.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6864)
    Me.chk_severity_low.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6865)
    Me.chk_severity_medium.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6866)
    Me.chk_severity_high.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6867)

    'State
    Me.gb_state.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6868)
    Me.chk_state_not_pending.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6869)
    Me.chk_state_pending.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6870)
    Me.chk_state_progress.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6871)
    Me.chk_state_solved.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6872)
    Me.chk_state_scaled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6890)
    Me.chk_state_validated.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6873)
    Me.chk_state_pending_all.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6887)

    ' Grid
    Call GUI_StyleSheet()

    'Set filter Alarms info
    Call FillAlarmsCatalog()

    ' Set filter default values
    Call SetDefaultValues()

    m_show_all_grid = chk_show_all.Checked

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()

  End Sub ' GUI_FilterReset

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '   - TRUE: filter values are accepted
  '   - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    Dim to_date As DateTime

    ' Dates selection 
    If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate > Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    Else
      to_date = WSI.Common.WGDB.Now
      to_date = New DateTime(to_date.Year, to_date.Month, to_date.Day, Me.uc_dsl.ClosingTime, 0, 0)
      If Me.uc_dsl.FromDate > to_date Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _result As Boolean
    Dim _time As TimeSpan

    _result = True

    'alarm id
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ALARM_ID).Value = DbRow.Value(SQL_COLUMN_ALARM_ID)

    'date created
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_CREATED).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATE_CREATED), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    'alarm desc
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ALARM_DESC).Value = DbRow.Value(SQL_COLUMN_ALARM_DESC)

    'alarm source
    If Not DbRow.IsNull(SQL_COLUMN_ALARM_SOURCE) Then
      Select Case DbRow.Value(SQL_COLUMN_ALARM_SOURCE)

        Case SMARTFLOOR_ALARM_SOURCE.APP
          If Not DbRow.IsNull(SQL_COLUMN_USER_NAME_SOURCE) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_ALARM_SOURCE).Value = DbRow.Value(SQL_COLUMN_USER_NAME_SOURCE)
          Else
            Me.Grid.Cell(RowIndex, GRID_COLUMN_ALARM_SOURCE).Value = String.Empty
          End If

          'Me.Grid.Cell(RowIndex, GRID_COLUMN_ALARM_SOURCE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6857) 'App

        Case SMARTFLOOR_ALARM_SOURCE.SYSTEM
          Me.Grid.Cell(RowIndex, GRID_COLUMN_ALARM_SOURCE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6858) 'System

      End Select

    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ALARM_SOURCE).Value = String.Empty
    End If

    'status
    If Not DbRow.IsNull(SQL_COLUMN_STATUS) Then
      Select Case DbRow.Value(SQL_COLUMN_STATUS)

        Case SMARTFLOOR_TASK_STATE.PENDING
          Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6870) 'Pending

        Case SMARTFLOOR_TASK_STATE.PROGRESS
          Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6871) 'Progress

        Case SMARTFLOOR_TASK_STATE.SOLVED
          Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6872) 'Solved

        Case SMARTFLOOR_TASK_STATE.SCALED
          Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6890) 'Scaled

        Case SMARTFLOOR_TASK_STATE.VALIDATED
          Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6873) 'Validated
      End Select

    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6869) 'Not pending
    End If

    'severity
    If Not DbRow.IsNull(SQL_COLUMN_SEVERITY) Then
      Select Case DbRow.Value(SQL_COLUMN_SEVERITY)

        Case SMARTFLOOR_TASK_SEVERITY.LOW
          Me.Grid.Cell(RowIndex, GRID_COLUMN_SEVERITY).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6865) 'Low

        Case SMARTFLOOR_TASK_SEVERITY.MEDIUM
          Me.Grid.Cell(RowIndex, GRID_COLUMN_SEVERITY).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6866) 'Medium

        Case SMARTFLOOR_TASK_SEVERITY.HIGH
          Me.Grid.Cell(RowIndex, GRID_COLUMN_SEVERITY).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6867) 'High

      End Select

    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SEVERITY).Value = String.Empty
    End If

    'asigned
    If Not DbRow.IsNull(SQL_COLUMN_ASIGNED) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ASIGNED).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_ASIGNED), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ASIGNED).Value = String.Empty
    End If

    'acepted
    If Not DbRow.IsNull(SQL_COLUMN_ACCEPTED) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCEPTED).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_ACCEPTED), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCEPTED).Value = String.Empty
    End If

    'solved
    If Not DbRow.IsNull(SQL_COLUMN_SOLVED) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SOLVED).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_SOLVED), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SOLVED).Value = String.Empty
    End If

    'validate
    If Not DbRow.IsNull(SQL_COLUMN_VALIDATE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_VALIDATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_VALIDATE), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_VALIDATE).Value = String.Empty
    End If

    'time asigned
    If Not DbRow.IsNull(SQL_COLUMN_TIME_ASIGNED) AndAlso DbRow.Value(SQL_COLUMN_TIME_ASIGNED) > 0 Then
      _time = New TimeSpan(0, 0, DbRow.Value(SQL_COLUMN_TIME_ASIGNED))
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TIME_ASIGNED).Value = TimeSpanToString(_time)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TIME_ASIGNED).Value = String.Empty
    End If

    'time accepted
    If Not DbRow.IsNull(SQL_COLUMN_TIME_ACCEPTED) AndAlso DbRow.Value(SQL_COLUMN_TIME_ACCEPTED) > 0 Then
      _time = New TimeSpan(0, 0, DbRow.Value(SQL_COLUMN_TIME_ACCEPTED))
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TIME_ACCEPTED).Value = TimeSpanToString(_time)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TIME_ACCEPTED).Value = String.Empty
    End If

    'time solved
    If Not DbRow.IsNull(SQL_COLUMN_TIME_SOLVED) AndAlso DbRow.Value(SQL_COLUMN_TIME_SOLVED) > 0 Then
      _time = New TimeSpan(0, 0, DbRow.Value(SQL_COLUMN_TIME_SOLVED))
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TIME_SOLVED).Value = TimeSpanToString(_time)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TIME_SOLVED).Value = String.Empty
    End If

    'time validated
    If Not DbRow.IsNull(SQL_COLUMN_TIME_VALIDATED) AndAlso DbRow.Value(SQL_COLUMN_TIME_VALIDATED) > 0 Then
      _time = New TimeSpan(0, 0, DbRow.Value(SQL_COLUMN_TIME_VALIDATED))
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TIME_VALIDATED).Value = TimeSpanToString(_time)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TIME_VALIDATED).Value = String.Empty
    End If

    'time total
    If Not DbRow.IsNull(SQL_COLUMN_TIME_TOTAL) AndAlso DbRow.Value(SQL_COLUMN_TIME_TOTAL) > 0 Then
      _time = New TimeSpan(0, 0, DbRow.Value(SQL_COLUMN_TIME_TOTAL))
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TIME_TOTAL).Value = TimeSpanToString(_time)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TIME_TOTAL).Value = String.Empty
    End If

    'user name
    If Not DbRow.IsNull(SQL_COLUMN_USER_NAME_STATUS) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_NAME).Value = DbRow.Value(SQL_COLUMN_USER_NAME_STATUS)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_NAME).Value = String.Empty
    End If


    _result = UpdateTotals(DbRow)

    Return _result

  End Function ' GUI_SetupRow

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _sb As System.Text.StringBuilder

    _sb = New System.Text.StringBuilder()

    _sb.AppendLine("    SELECT   LSA_ALARM_ID AS ALARM_ID                                           ")
    _sb.AppendLine("           , ALARM_RANGE_ID                                                     ")
    _sb.AppendLine("           , LSA_DATE_CREATED                                                   ")
    _sb.AppendLine("           , ALARM_DESC                                                         ")
    _sb.AppendLine("           , LSA_ALARM_SOURCE                                                   ")
    _sb.AppendLine("           , LST_STATUS                                                         ")
    _sb.AppendLine("           , LST_SEVERITY                                                       ")
    _sb.AppendLine("           , LST_ASSIGNED                                                       ")
    _sb.AppendLine("           , LST_ACCEPTED                                                       ")
    _sb.AppendLine("           , LST_SOLVED                                                         ")
    _sb.AppendLine("           , LST_VALIDATE                                                       ")
    _sb.AppendLine("           , DATEDIFF (SECOND, LSA_DATE_CREATED, LST_ASSIGNED) AS TIME_ASSIGNED ")
    _sb.AppendLine("           , DATEDIFF (SECOND, LST_ASSIGNED, LST_ACCEPTED ) AS TIME_ACCEPTED    ")
    _sb.AppendLine("           , DATEDIFF (SECOND, LST_ACCEPTED, LST_SOLVED) AS TIME_SOLVED         ")
    _sb.AppendLine("           , DATEDIFF (SECOND, LST_SOLVED, LST_VALIDATE) AS TIME_VALIDATED      ")
    _sb.AppendLine("           , DATEDIFF (SECOND, LSA_DATE_CREATED, LST_VALIDATE) AS TIME_TOTAL    ")
    _sb.AppendLine("           , ISNULL(GU_USERNAME,'') AS GU_USERNAME                              ")
    _sb.AppendLine("           , (SELECT  ISNULL(GU.GU_USERNAME,' ')                                ")
    _sb.AppendLine("               FROM  GUI_USERS GU                                               ")
    _sb.AppendLine("              WHERE  GU.GU_USER_ID = CASE WHEN T.LST_STATUS = 1 THEN	-1   -- PENDING                     ")
    _sb.AppendLine("           						                    WHEN T.LST_STATUS = 2 THEN	LST_ACCEPTED_USER_ID   -- PROGRESS  ")
    _sb.AppendLine("             						                  WHEN T.LST_STATUS = 3 THEN	LST_SOLVED_USER_ID     -- SOLVED    ")
    _sb.AppendLine("                                          WHEN T.LST_STATUS = 4 THEN	LST_SCALE_FROM_USER_ID -- SCALED    ")
    _sb.AppendLine("                                          WHEN T.LST_STATUS = 5 THEN	LST_VALIDATE_USER_ID   -- VALIDATED ")
    _sb.AppendLine("                                          ELSE -1 END) GU_USER_STATUS           ")
    _sb.AppendLine("      FROM   LAYOUT_SITE_ALARMS A                                               ")
    _sb.AppendLine(" LEFT JOIN   LAYOUT_SITE_TASKS T ON T.LST_ID = A.LSA_TASK_ID                    ")
    _sb.AppendLine(" LEFT JOIN   GUI_USERS ON LSA_USER_CREATED = GU_USER_ID                         ")
    _sb.AppendLine("INNER JOIN                                                                      ")
    _sb.AppendLine("   (SELECT   LR_NAME + ' - ' + LRL_LABEL AS [ALARM_DESC]                        ")
    _sb.AppendLine("           , LRL_ID AS [ALARM_RANGE_ID]                                         ")
    _sb.AppendLine("           , CAST((LR_FIELD * 1000) + LRL_VALUE1 AS INT) AS [SUBCATEGORY]       ")
    _sb.AppendLine("      FROM   LAYOUT_RANGES AS R                                                 ")
    _sb.AppendLine("INNER JOIN   LAYOUT_RANGES_LEGENDS AS L ON R.LR_ID = L.LRL_RANGE_ID             ")
    _sb.AppendLine("     WHERE   LR_SECTION_ID IN (41,42,43)                                           ")
    _sb.AppendLine("             ) AS N ON N.SUBCATEGORY = A.LSA_ALARM_ID                           ")
    _sb.AppendLine(GetSqlWhere())
    _sb.AppendLine("ORDER BY LSA_DATE_CREATED                                                       ")

    Return _sb.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE: Initialize members and accumulators related to the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()

    Me.m_total_points = 0
    Call GUI_StyleSheet()

    Call InitTotalsDataTable()

  End Sub 'GUI_BeforeFirstRow

  ' PURPOSE: Print totalizator row in the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    Dim _idx_row As Int32
    Dim _time As TimeSpan
    Dim _row As DataRow
    Dim _all_rows() As DataRow

    _all_rows = m_totals_data_table.Select("", "GROUP_NAME")

    For Each _row In _all_rows

      _idx_row = Me.Grid.AddRow()

      'total group name
      Me.Grid.Cell(_idx_row, GRID_COLUMN_ALARM_DESC).Value = _row.Item(DT_COLUMN_TOTALS_GROUP_NAME)

      'time asigned
      _time = New TimeSpan(0, 0, _row.Item(DT_COLUMN_TOTALS_ASIGNED))
      Me.Grid.Cell(_idx_row, GRID_COLUMN_TIME_ASIGNED).Value = IIf(_row.Item(DT_COLUMN_TOTALS_ASIGNED) > 0, TimeSpanToString(_time), "")

      'time accepted
      _time = New TimeSpan(0, 0, _row.Item(DT_COLUMN_TOTALS_ACCEPTED))
      Me.Grid.Cell(_idx_row, GRID_COLUMN_TIME_ACCEPTED).Value = IIf(_row.Item(DT_COLUMN_TOTALS_ACCEPTED) > 0, TimeSpanToString(_time), "")

      'time solved
      _time = New TimeSpan(0, 0, _row.Item(DT_COLUMN_TOTALS_SOLVED))
      Me.Grid.Cell(_idx_row, GRID_COLUMN_TIME_SOLVED).Value = IIf(_row.Item(DT_COLUMN_TOTALS_SOLVED) > 0, TimeSpanToString(_time), "")

      'time validated
      _time = New TimeSpan(0, 0, _row.Item(DT_COLUMN_TOTALS_VALIDATED))
      Me.Grid.Cell(_idx_row, GRID_COLUMN_TIME_VALIDATED).Value = IIf(_row.Item(DT_COLUMN_TOTALS_VALIDATED) > 0, TimeSpanToString(_time), "")

      'time total
      _time = New TimeSpan(0, 0, _row.Item(DT_COLUMN_TOTALS_TOTAL))
      Me.Grid.Cell(_idx_row, GRID_COLUMN_TIME_TOTAL).Value = IIf(_row.Item(DT_COLUMN_TOTALS_TOTAL) > 0, TimeSpanToString(_time), "")

      'avg asigned
      _time = New TimeSpan(0, 0, _row.Item(DT_COLUMN_AVG_ASIGNED))
      Me.Grid.Cell(_idx_row, GRID_COLUMN_AVG_TIME_ASIGNED).Value = IIf(_row.Item(DT_COLUMN_AVG_ASIGNED) > 0, TimeSpanToString(_time), "")

      'avg accepted
      _time = New TimeSpan(0, 0, _row.Item(DT_COLUMN_AVG_ACCEPTED))
      Me.Grid.Cell(_idx_row, GRID_COLUMN_AVG_TIME_ACCEPTED).Value = IIf(_row.Item(DT_COLUMN_AVG_ACCEPTED) > 0, TimeSpanToString(_time), "")

      'avg solved
      _time = New TimeSpan(0, 0, _row.Item(DT_COLUMN_AVG_SOLVED))
      Me.Grid.Cell(_idx_row, GRID_COLUMN_AVG_TIME_SOLVED).Value = IIf(_row.Item(DT_COLUMN_AVG_SOLVED) > 0, TimeSpanToString(_time), "")

      'avg validated
      _time = New TimeSpan(0, 0, _row.Item(DT_COLUMN_AVG_VALIDATED))
      Me.Grid.Cell(_idx_row, GRID_COLUMN_AVG_TIME_VALIDATED).Value = IIf(_row.Item(DT_COLUMN_AVG_VALIDATED) > 0, TimeSpanToString(_time), "")

      'avg total
      _time = New TimeSpan(0, 0, _row.Item(DT_COLUMN_AVG_TOTAL))
      Me.Grid.Cell(_idx_row, GRID_COLUMN_AVG_TIME_TOTAL).Value = IIf(_row.Item(DT_COLUMN_AVG_TOTAL) > 0, TimeSpanToString(_time), "")

      'count asigned
      Me.Grid.Cell(_idx_row, GRID_COLUMN_COUNT_TIME_ASIGNED).Value = IIf(_row.Item(DT_COLUMN_AVG_ASIGNED) > 0, _row.Item(DT_COLUMN_COUNT_ASIGNED), "")

      'count accepted
      Me.Grid.Cell(_idx_row, GRID_COLUMN_COUNT_TIME_ACCEPTED).Value = IIf(_row.Item(DT_COLUMN_AVG_ACCEPTED) > 0, _row.Item(DT_COLUMN_COUNT_ACCEPTED), "")

      'count solved
      Me.Grid.Cell(_idx_row, GRID_COLUMN_COUNT_TIME_SOLVED).Value = IIf(_row.Item(DT_COLUMN_AVG_SOLVED) > 0, _row.Item(DT_COLUMN_COUNT_SOLVED), "")

      'count validated
      Me.Grid.Cell(_idx_row, GRID_COLUMN_COUNT_TIME_VALIDATED).Value = IIf(_row.Item(DT_COLUMN_AVG_VALIDATED) > 0, _row.Item(DT_COLUMN_COUNT_VALIDATED), "")

      'count total
      Me.Grid.Cell(_idx_row, GRID_COLUMN_COUNT_TIME_TOTAL).Value = IIf(_row.Item(DT_COLUMN_AVG_TOTAL) > 0, _row.Item(DT_COLUMN_COUNT_TOTAL), "")

      ' Color Row
      Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
    Next

  End Sub ' GUI_AfterLastRow

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.uc_dsl
  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Dispose form
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub Finalize()
    MyBase.Finalize() ' TODO Hace falta?
  End Sub ' Finalize

#Region " GUI Reports "

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()
    m_date_by = String.Empty
    m_filter_from = String.Empty
    m_filter_to = String.Empty
    m_show_all = String.Empty
    m_selected_alarms = String.Empty
    m_severity = New List(Of String)
    m_state = New List(Of String)

    'Date
    Dim aux_date As Date

    m_filter_from = GUI_FormatDate(Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime), _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMM)

    If Me.uc_dsl.ToDateSelected Then
      m_filter_to = GUI_FormatDate(Me.uc_dsl.ToDate.AddHours(Me.uc_dsl.ClosingTime), _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMM)
    Else
      If Now.Hour >= Me.uc_dsl.ClosingTime Then
        aux_date = New Date(Now.Year, Now.Month, Now.Day, Me.uc_dsl.ClosingTime, 0, 0)
        aux_date = aux_date.AddDays(1)
      Else
        aux_date = New Date(Now.Year, Now.Month, Now.Day, Me.uc_dsl.ClosingTime, 0, 0)
      End If
      m_filter_to = GUI_FormatDate(aux_date, _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    If opt_date_creation.Checked Then
      m_date_by = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6856)
    ElseIf opt_date_resolution.Checked Then
      m_date_by = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6855)
    End If

    ' Show all
    If chk_show_all.Checked Then
      m_show_all = True
    Else
      m_show_all = False
    End If

    ' Groups
    If uc_checked_list_alarms_catalog.SelectedIndexes.Length = uc_checked_list_alarms_catalog.Count() Then
      m_selected_alarms = GLB_NLS_GUI_AUDITOR.GetString(263) 'All
    Else
      m_selected_alarms = uc_checked_list_alarms_catalog.SelectedValuesList()
    End If

    'Severity
    If Me.chk_severity_high.Checked Then
      m_severity.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6867))
    End If
    If Me.chk_severity_medium.Checked Then
      m_severity.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6866))
    End If
    If Me.chk_severity_low.Checked Then
      m_severity.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6865))
    End If

    'State
    If Me.chk_state_not_pending.Checked Then
      m_state.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6869))
    End If
    If Me.chk_state_pending.Checked Then
      m_state.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6870))
    End If
    If Me.chk_state_progress.Checked Then
      m_state.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6871))
    End If
    If Me.chk_state_solved.Checked Then
      m_state.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6872))
    End If
    If Me.chk_state_validated.Checked Then
      m_state.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6873))
    End If

  End Sub ' GUI_ReportUpdateFilters

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201), m_date_by)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_filter_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_filter_to)

    If Not String.IsNullOrEmpty(m_show_all) Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6861), m_show_all)
    End If

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6874), m_selected_alarms)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6864), String.Join(",", m_severity.ToArray()))

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6868), String.Join(",", m_state.ToArray()))

    PrintData.FilterValueWidth(1) = 3000
    PrintData.FilterValueWidth(2) = 4000

  End Sub ' GUI_ReportFilter

#End Region

#End Region ' OVERRIDES

#Region " Public Functions "

  Public Sub FillAlarmsGroupsControl(ByVal DataTableAlarms As DataTable, ByRef AlarmsCheckedList As GUI_Controls.uc_checked_list)
    Dim _group As Integer = 0
    Dim _category As String = ""
    Dim _name_id As String

    _name_id = ""

    Try

      ' Fill CheckedList control with groups
      Call AlarmsCheckedList.Clear()
      Call AlarmsCheckedList.ReDraw(False)

      For Each row As DataRow In DataTableAlarms.Rows

        If row.Item(SQL_COLUMN_ALARM_SECTION_TYPE) <> _group Then
          If row.Item(SQL_COLUMN_ALARM_SECTION_TYPE) = SMARTFLOOR_ALARM_TYPE.CLIENT Then
            AlarmsCheckedList.Add(1, 0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6862))
          End If
          If row.Item(SQL_COLUMN_ALARM_SECTION_TYPE) = SMARTFLOOR_ALARM_TYPE.TERMINAL Then
            AlarmsCheckedList.Add(1, 0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6863))
          End If
          _group = row.Item(SQL_COLUMN_ALARM_SECTION_TYPE)
        End If

        If row.Item(SQL_COLUMN_ALARM_CATEGORY_NAME) <> _category Then
          AlarmsCheckedList.Add(2, 0, row.Item(SQL_COLUMN_ALARM_CATEGORY_NAME))
          _category = row.Item(SQL_COLUMN_ALARM_CATEGORY_NAME)
        End If

        AlarmsCheckedList.Add(3, row.Item(SQL_COLUMN_ALARM_CODE), row.Item(SQL_COLUMN_ALARM_GROUP_NAME))

      Next

      If AlarmsCheckedList.Count > 0 Then
        Call AlarmsCheckedList.CurrentRow(0)
        Call AlarmsCheckedList.ReDraw(True)
      End If

    Catch ex As Exception

      Debug.WriteLine(ex.Message)

    End Try

  End Sub ' FillAlarmsGroupsControl

  ' PURPOSE : Opens dialog with default settings for edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    Dim _total_columns As Integer

    _total_columns = GRID_COLUMNS


    With Me.Grid
      Call .Init(_total_columns, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Header(1).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      'Id
      .Column(GRID_COLUMN_ALARM_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_ALARM_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_ALARM_ID).Width = GRID_WIDTH_ALARM_ID
      .Column(GRID_COLUMN_ALARM_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'Description
      .Column(GRID_COLUMN_ALARM_DESC).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6891)
      .Column(GRID_COLUMN_ALARM_DESC).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6874)
      .Column(GRID_COLUMN_ALARM_DESC).Width = GRID_WIDTH_ALARM_DESC
      .Column(GRID_COLUMN_ALARM_DESC).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'Source
      .Column(GRID_COLUMN_ALARM_SOURCE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6891)
      .Column(GRID_COLUMN_ALARM_SOURCE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6885)
      .Column(GRID_COLUMN_ALARM_SOURCE).Width = IIf(m_show_all_grid, GRID_WIDTH_ALARM_SOURCE, 0)
      .Column(GRID_COLUMN_ALARM_SOURCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'Status
      .Column(GRID_COLUMN_STATUS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6891)
      .Column(GRID_COLUMN_STATUS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6868)
      .Column(GRID_COLUMN_STATUS).Width = GRID_WIDTH_STATUS
      .Column(GRID_COLUMN_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'Runner
      .Column(GRID_COLUMN_USER_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6891)
      .Column(GRID_COLUMN_USER_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6905) ' Runner
      .Column(GRID_COLUMN_USER_NAME).Width = GRID_WIDTH_USER_NAME
      .Column(GRID_COLUMN_USER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT


      'Severity
      .Column(GRID_COLUMN_SEVERITY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6891)
      .Column(GRID_COLUMN_SEVERITY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6864)
      .Column(GRID_COLUMN_SEVERITY).Width = GRID_WIDTH_SEVERITY
      .Column(GRID_COLUMN_SEVERITY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'Date creation
      .Column(GRID_COLUMN_DATE_CREATED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6854)
      .Column(GRID_COLUMN_DATE_CREATED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6856)
      .Column(GRID_COLUMN_DATE_CREATED).Width = GRID_WIDTH_DATE_CREATED
      .Column(GRID_COLUMN_DATE_CREATED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'Date asigned
      .Column(GRID_COLUMN_ASIGNED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6854)
      .Column(GRID_COLUMN_ASIGNED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6879)
      .Column(GRID_COLUMN_ASIGNED).Width = IIf(m_show_all_grid, GRID_WIDTH_ASIGNED, 0)
      .Column(GRID_COLUMN_ASIGNED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'Date accepted
      .Column(GRID_COLUMN_ACCEPTED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6854)
      .Column(GRID_COLUMN_ACCEPTED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6880)
      .Column(GRID_COLUMN_ACCEPTED).Width = IIf(m_show_all_grid, GRID_WIDTH_ACCEPTED, 0)
      .Column(GRID_COLUMN_ACCEPTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'Date solved
      .Column(GRID_COLUMN_SOLVED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6854)
      .Column(GRID_COLUMN_SOLVED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6881)
      .Column(GRID_COLUMN_SOLVED).Width = GRID_WIDTH_SOLVED
      .Column(GRID_COLUMN_SOLVED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'Date validate
      .Column(GRID_COLUMN_VALIDATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6854)
      .Column(GRID_COLUMN_VALIDATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6882)
      .Column(GRID_COLUMN_VALIDATE).Width = IIf(m_show_all_grid, GRID_WIDTH_VALIDATE, 0)
      .Column(GRID_COLUMN_VALIDATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'Time asigned
      .Column(GRID_COLUMN_TIME_ASIGNED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6884)
      .Column(GRID_COLUMN_TIME_ASIGNED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6879)
      .Column(GRID_COLUMN_TIME_ASIGNED).Width = IIf(m_show_all_grid, GRID_WIDTH_TIME_ASIGNED, 0)
      .Column(GRID_COLUMN_TIME_ASIGNED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'Time accepted
      .Column(GRID_COLUMN_TIME_ACCEPTED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6884)
      .Column(GRID_COLUMN_TIME_ACCEPTED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6880)
      .Column(GRID_COLUMN_TIME_ACCEPTED).Width = IIf(m_show_all_grid, GRID_WIDTH_TIME_ACCEPTED, 0)
      .Column(GRID_COLUMN_TIME_ACCEPTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'Time solved
      .Column(GRID_COLUMN_TIME_SOLVED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6884)
      .Column(GRID_COLUMN_TIME_SOLVED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6881)
      .Column(GRID_COLUMN_TIME_SOLVED).Width = GRID_WIDTH_TIME_SOLVED
      .Column(GRID_COLUMN_TIME_SOLVED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'Time validated
      .Column(GRID_COLUMN_TIME_VALIDATED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6884)
      .Column(GRID_COLUMN_TIME_VALIDATED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6882)
      .Column(GRID_COLUMN_TIME_VALIDATED).Width = IIf(m_show_all_grid, GRID_WIDTH_TIME_VALIDATED, 0)
      .Column(GRID_COLUMN_TIME_VALIDATED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'Time total
      .Column(GRID_COLUMN_TIME_TOTAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6884)
      .Column(GRID_COLUMN_TIME_TOTAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6883)
      .Column(GRID_COLUMN_TIME_TOTAL).Width = IIf(m_show_all_grid, GRID_WIDTH_TIME_TOTAL, 0)
      .Column(GRID_COLUMN_TIME_TOTAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'Time avg asigned
      .Column(GRID_COLUMN_AVG_TIME_ASIGNED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6879)
      .Column(GRID_COLUMN_AVG_TIME_ASIGNED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6930)
      .Column(GRID_COLUMN_AVG_TIME_ASIGNED).Width = IIf(m_show_all_grid, GRID_WIDTH_AVG_TIME_ASIGNED, 0)
      .Column(GRID_COLUMN_AVG_TIME_ASIGNED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'Time avg accepted
      .Column(GRID_COLUMN_AVG_TIME_ACCEPTED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6880)
      .Column(GRID_COLUMN_AVG_TIME_ACCEPTED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6930)
      .Column(GRID_COLUMN_AVG_TIME_ACCEPTED).Width = IIf(m_show_all_grid, GRID_WIDTH_AVG_TIME_ACCEPTED, 0)
      .Column(GRID_COLUMN_AVG_TIME_ACCEPTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'Time avg solved
      .Column(GRID_COLUMN_AVG_TIME_SOLVED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6881)
      .Column(GRID_COLUMN_AVG_TIME_SOLVED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6930)
      .Column(GRID_COLUMN_AVG_TIME_SOLVED).Width = GRID_WIDTH_AVG_TIME_SOLVED
      .Column(GRID_COLUMN_AVG_TIME_SOLVED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'Time avg validated
      .Column(GRID_COLUMN_AVG_TIME_VALIDATED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6882)
      .Column(GRID_COLUMN_AVG_TIME_VALIDATED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6930)
      .Column(GRID_COLUMN_AVG_TIME_VALIDATED).Width = IIf(m_show_all_grid, GRID_WIDTH_AVG_TIME_VALIDATED, 0)
      .Column(GRID_COLUMN_AVG_TIME_VALIDATED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'Time avg total
      .Column(GRID_COLUMN_AVG_TIME_TOTAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6883)
      .Column(GRID_COLUMN_AVG_TIME_TOTAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6930)
      .Column(GRID_COLUMN_AVG_TIME_TOTAL).Width = IIf(m_show_all_grid, GRID_WIDTH_AVG_TIME_TOTAL, 0)
      .Column(GRID_COLUMN_AVG_TIME_TOTAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'Time count asigned
      .Column(GRID_COLUMN_COUNT_TIME_ASIGNED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6879)
      .Column(GRID_COLUMN_COUNT_TIME_ASIGNED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3302)
      .Column(GRID_COLUMN_COUNT_TIME_ASIGNED).Width = IIf(m_show_all_grid, GRID_WIDTH_COUNT_TIME_ASIGNED, 0)
      .Column(GRID_COLUMN_COUNT_TIME_ASIGNED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'Time count accepted
      .Column(GRID_COLUMN_COUNT_TIME_ACCEPTED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6880)
      .Column(GRID_COLUMN_COUNT_TIME_ACCEPTED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3302)
      .Column(GRID_COLUMN_COUNT_TIME_ACCEPTED).Width = IIf(m_show_all_grid, GRID_WIDTH_COUNT_TIME_ACCEPTED, 0)
      .Column(GRID_COLUMN_COUNT_TIME_ACCEPTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'Time count solved
      .Column(GRID_COLUMN_COUNT_TIME_SOLVED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6881)
      .Column(GRID_COLUMN_COUNT_TIME_SOLVED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3302)
      .Column(GRID_COLUMN_COUNT_TIME_SOLVED).Width = GRID_WIDTH_COUNT_TIME_SOLVED
      .Column(GRID_COLUMN_COUNT_TIME_SOLVED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'Time count validated 
      .Column(GRID_COLUMN_COUNT_TIME_VALIDATED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6882)
      .Column(GRID_COLUMN_COUNT_TIME_VALIDATED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3302)
      .Column(GRID_COLUMN_COUNT_TIME_VALIDATED).Width = IIf(m_show_all_grid, GRID_WIDTH_COUNT_TIME_VALIDATED, 0)
      .Column(GRID_COLUMN_COUNT_TIME_VALIDATED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'Time count total     
      .Column(GRID_COLUMN_COUNT_TIME_TOTAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6883)
      .Column(GRID_COLUMN_COUNT_TIME_TOTAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3302)
      .Column(GRID_COLUMN_COUNT_TIME_TOTAL).Width = IIf(m_show_all_grid, GRID_WIDTH_COUNT_TIME_TOTAL, 0)
      .Column(GRID_COLUMN_COUNT_TIME_TOTAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With


    Me.Grid.Width += System.Windows.Forms.SystemInformation.VerticalScrollBarWidth

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Dim initial_time As Date

    initial_time = WSI.Common.Misc.TodayOpening()
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.ToDateSelected = True
    Me.uc_dsl.ToDate = initial_time
    Me.uc_dsl.FromDate = Me.uc_dsl.ToDate.AddDays(-1)
    Me.uc_dsl.ClosingTime = initial_time.Hour

    ' Catalog Filter
    Call Me.uc_checked_list_alarms_catalog.CollapseAll()
    Call Me.uc_checked_list_alarms_catalog.SetDefaultValue(False)

    Me.opt_date_creation.Checked = True

    Me.chk_severity_high.Checked = False
    Me.chk_severity_medium.Checked = False
    Me.chk_severity_low.Checked = False

    Me.chk_state_not_pending.Checked = False
    Me.chk_state_pending.Checked = False
    Me.chk_state_progress.Checked = False
    Me.chk_state_scaled.Checked = False
    Me.chk_state_solved.Checked = False
    Me.chk_state_validated.Checked = False
    Me.chk_show_all.Checked = False
    Me.chk_state_pending_all.Checked = False

  End Sub ' SetDefaultValues

  ' PURPOSE: Read Data base data from alarms
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - DataTable
  Private Function GetAlarmDataTable() As DataTable

    Dim _sb As StringBuilder
    Dim _adapter As SqlDataAdapter
    Dim _table As DataTable

    _adapter = New SqlDataAdapter()
    _table = New DataTable()
    _sb = New StringBuilder()

    Try
      Using _db_trx As DB_TRX = New DB_TRX()

        _sb.AppendLine("     SELECT   LR_SECTION_ID AS SECTIONTYPE       ")
        _sb.AppendLine("            , LR_NAME       AS CATEGORYNAME      ")
        _sb.AppendLine("            , LRL_LABEL     AS GROUPNAME         ")
        _sb.AppendLine("            , LRL_ID        AS ALARMCODE         ")
        _sb.AppendLine("       FROM   LAYOUT_RANGES AS R                 ")
        _sb.AppendLine(" INNER JOIN   LAYOUT_RANGES_LEGENDS AS L         ")
        _sb.AppendLine("         ON   R.LR_ID = L.LRL_RANGE_ID           ")
        _sb.AppendLine("      WHERE   LR_SECTION_ID IN (" + CStr(SMARTFLOOR_ALARM_TYPE.CLIENT) + "," + CStr(SMARTFLOOR_ALARM_TYPE.TERMINAL) + ") ")
        _sb.AppendLine("   ORDER BY   LR_SECTION_ID                      ")

        Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _adapter = New SqlDataAdapter(_cmd)
          _adapter.Fill(_table)
        End Using
      End Using

      Return _table

    Catch ex As Exception
      Return Nothing
    End Try

  End Function 'GetAlarmData

  ' PURPOSE: Fill uc_checked_list control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub FillAlarmsCatalog()

    Try

      Call FillAlarmsGroupsControl(GetAlarmDataTable(), uc_checked_list_alarms_catalog)

      Call uc_checked_list_alarms_catalog.ResizeGrid()
    Catch ex As Exception

      Debug.WriteLine(ex.Message)

    End Try

  End Sub ' FillAlarmsCatalog

  ' PURPOSE: Build the variable part of the WHERE clause for the main SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String

    Dim _str_where As String = String.Empty
    Dim _date_from As String = String.Empty
    Dim _date_to As String = String.Empty

    If uc_dsl.FromDateSelected Then
      _date_from = GUI_FormatDateDB(uc_dsl.FromDate)
    End If
    If uc_dsl.ToDateSelected Then
      _date_to = GUI_FormatDateDB(uc_dsl.ToDate)
    End If

    If opt_date_creation.Checked Then
      _str_where = _str_where & WSI.Common.Misc.DateSQLFilter("LSA_DATE_CREATED", _date_from, _date_to, False)
    Else
      _str_where = _str_where & WSI.Common.Misc.DateSQLFilter("LST_SOLVED", _date_from, _date_to, False)
    End If

    _str_where += GetSeveritySelect()

    _str_where += GetStateSelect()

    _str_where += GetAlarmSelect()

    ' Final processing
    If Len(_str_where) > 0 Then
      ' Discard the leading ' AND ' and place 'Where' instead
      _str_where = Strings.Right(_str_where, Len(_str_where) - 5)
      _str_where = " WHERE " & _str_where
    End If

    Return _str_where

  End Function ' GetSqlWhere

  ' PURPOSE: Get severity select to column LST_SEVERITY of query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Private Function GetSeveritySelect() As String

    Dim _str_severity As String = ""

    If Me.chk_severity_low.Checked Then
      _str_severity = CStr(SMARTFLOOR_TASK_SEVERITY.LOW)
    End If

    If Me.chk_severity_medium.Checked Then
      _str_severity += IIf(String.IsNullOrEmpty(_str_severity), CStr(SMARTFLOOR_TASK_SEVERITY.MEDIUM), "," + CStr(SMARTFLOOR_TASK_SEVERITY.MEDIUM))
    End If

    If Me.chk_severity_high.Checked Then
      _str_severity += IIf(String.IsNullOrEmpty(_str_severity), CStr(SMARTFLOOR_TASK_SEVERITY.HIGH), "," + CStr(SMARTFLOOR_TASK_SEVERITY.HIGH))
    End If

    If Not String.IsNullOrEmpty(_str_severity) Then
      Return " AND LST_SEVERITY IN (" + _str_severity + ")"
    Else
      Return ""
    End If

  End Function ' GetSeveritySelect

  ' PURPOSE: Get State select to column LST_STATUS of query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Private Function GetStateSelect() As String

    Dim _str_state As String = ""
    Dim _not_pending As Boolean = False

    If Me.chk_state_not_pending.Checked Then
      _not_pending = True
    End If

    If Me.chk_state_pending.Checked Then
      _str_state += IIf(String.IsNullOrEmpty(_str_state), CStr(SMARTFLOOR_TASK_STATE.PENDING), "," + CStr(SMARTFLOOR_TASK_STATE.PENDING))
    End If

    If Me.chk_state_progress.Checked Then
      _str_state += IIf(String.IsNullOrEmpty(_str_state), CStr(SMARTFLOOR_TASK_STATE.PROGRESS), "," + CStr(SMARTFLOOR_TASK_STATE.PROGRESS))
    End If

    If Me.chk_state_solved.Checked Then
      _str_state += IIf(String.IsNullOrEmpty(_str_state), CStr(SMARTFLOOR_TASK_STATE.SOLVED), "," + CStr(SMARTFLOOR_TASK_STATE.SOLVED))
    End If

    If Me.chk_state_scaled.Checked Then
      _str_state += IIf(String.IsNullOrEmpty(_str_state), CStr(SMARTFLOOR_TASK_STATE.SCALED), "," + CStr(SMARTFLOOR_TASK_STATE.SCALED))
    End If

    If Me.chk_state_validated.Checked Then
      _str_state += IIf(String.IsNullOrEmpty(_str_state), CStr(SMARTFLOOR_TASK_STATE.VALIDATED), "," + CStr(SMARTFLOOR_TASK_STATE.VALIDATED))
    End If

    If Not String.IsNullOrEmpty(_str_state) And _not_pending Then
      Return " AND ( LST_STATUS IN (" + _str_state + ") OR LST_STATUS IS NULL )"
    ElseIf String.IsNullOrEmpty(_str_state) And _not_pending Then
      Return " AND ( LST_STATUS IS NULL )"
    ElseIf Not String.IsNullOrEmpty(_str_state) And Not _not_pending Then
      Return " AND LST_STATUS IN (" + _str_state + ") "
    Else
      Return ""
    End If

  End Function ' GetStateSelect

  ' PURPOSE: Get Alarms select to column ALARM_RANGE_ID of query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Private Function GetAlarmSelect() As String
    Dim _alarm_codes As String = String.Empty
    Dim _str_alarm As String = ""

    If Me.uc_checked_list_alarms_catalog.Enabled Then
      _alarm_codes = Me.uc_checked_list_alarms_catalog.SelectedIndexesList()
    End If

    If _alarm_codes.Length > 0 Then
      If _alarm_codes.Contains(START_RANGE_GAME_ALARMS) Then
        _str_alarm = _str_alarm & " AND ( ALARM_RANGE_ID IN ( " & _alarm_codes & " ) " & _
                                  " OR ( ALARM_RANGE_ID >= " & START_RANGE_GAME_ALARMS & " AND ALARM_RANGE_ID <= " & END_RANGE_GAME_ALARMS & " ))"
      Else
        _str_alarm = _str_alarm & " AND ALARM_RANGE_ID IN ( " & _alarm_codes & " ) "
      End If
    End If

    Return _str_alarm

  End Function ' GetAlarmSelect

  ' PURPOSE: Set the counter and the sub and add amount cells of each total row
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row: the row to process
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function UpdateTotals(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _total_row As DataRow
    Dim _alarm_type As String

    _alarm_type = DbRow.Value(SQL_COLUMN_ALARM_DESC)

    _total_row = GetRowFromTotalsByType(_alarm_type)

    _total_row.Item(DT_COLUMN_TOTALS_GROUP_NAME) = _alarm_type

    'Asigned
    _total_row.Item(DT_COLUMN_TOTALS_ASIGNED) = _total_row.Item(DT_COLUMN_TOTALS_ASIGNED) + _
                                                IIf(DbRow.IsNull(SQL_COLUMN_TIME_ASIGNED), 0, DbRow.Value(SQL_COLUMN_TIME_ASIGNED))

    _total_row.Item(DT_COLUMN_COUNT_ASIGNED) = _total_row.Item(DT_COLUMN_COUNT_ASIGNED) + _
                                                    IIf(DbRow.IsNull(SQL_COLUMN_TIME_ASIGNED), 0, 1)

    _total_row.Item(DT_COLUMN_AVG_ASIGNED) = IIf(_total_row.Item(DT_COLUMN_COUNT_ASIGNED) <> 0, _total_row.Item(DT_COLUMN_TOTALS_ASIGNED) / _total_row.Item(DT_COLUMN_COUNT_ASIGNED), 0)

    'Accepted
    _total_row.Item(DT_COLUMN_TOTALS_ACCEPTED) = _total_row.Item(DT_COLUMN_TOTALS_ACCEPTED) + _
                                                IIf(DbRow.IsNull(SQL_COLUMN_TIME_ACCEPTED), 0, DbRow.Value(SQL_COLUMN_TIME_ACCEPTED))

    _total_row.Item(DT_COLUMN_COUNT_ACCEPTED) = _total_row.Item(DT_COLUMN_COUNT_ACCEPTED) + _
                                                    IIf(DbRow.IsNull(SQL_COLUMN_TIME_ACCEPTED), 0, 1)

    _total_row.Item(DT_COLUMN_AVG_ACCEPTED) = IIf(_total_row.Item(DT_COLUMN_COUNT_ACCEPTED) <> 0, _total_row.Item(DT_COLUMN_TOTALS_ACCEPTED) / _total_row.Item(DT_COLUMN_COUNT_ACCEPTED), 0)

    'Solved
    _total_row.Item(DT_COLUMN_TOTALS_SOLVED) = _total_row.Item(DT_COLUMN_TOTALS_SOLVED) + _
                                                IIf(DbRow.IsNull(SQL_COLUMN_TIME_SOLVED), 0, DbRow.Value(SQL_COLUMN_TIME_SOLVED))

    _total_row.Item(DT_COLUMN_COUNT_SOLVED) = _total_row.Item(DT_COLUMN_COUNT_SOLVED) + _
                                                IIf(DbRow.IsNull(SQL_COLUMN_TIME_SOLVED), 0, 1)

    _total_row.Item(DT_COLUMN_AVG_SOLVED) = IIf(_total_row.Item(DT_COLUMN_COUNT_SOLVED) <> 0, _total_row.Item(DT_COLUMN_TOTALS_SOLVED) / _total_row.Item(DT_COLUMN_COUNT_SOLVED), 0)

    'Validated
    _total_row.Item(DT_COLUMN_TOTALS_VALIDATED) = _total_row.Item(DT_COLUMN_TOTALS_VALIDATED) + _
                                                IIf(DbRow.IsNull(SQL_COLUMN_TIME_VALIDATED), 0, DbRow.Value(SQL_COLUMN_TIME_VALIDATED))

    _total_row.Item(DT_COLUMN_COUNT_VALIDATED) = _total_row.Item(DT_COLUMN_COUNT_VALIDATED) + _
                                                IIf(DbRow.IsNull(SQL_COLUMN_TIME_VALIDATED), 0, 1)

    _total_row.Item(DT_COLUMN_AVG_VALIDATED) = IIf(_total_row.Item(DT_COLUMN_COUNT_VALIDATED) <> 0, _total_row.Item(DT_COLUMN_TOTALS_VALIDATED) / _total_row.Item(DT_COLUMN_COUNT_VALIDATED), 0)

    'Total
    _total_row.Item(DT_COLUMN_TOTALS_TOTAL) = _total_row.Item(DT_COLUMN_TOTALS_TOTAL) + _
                                                IIf(DbRow.IsNull(SQL_COLUMN_TIME_TOTAL), 0, DbRow.Value(SQL_COLUMN_TIME_TOTAL))

    _total_row.Item(DT_COLUMN_COUNT_TOTAL) = _total_row.Item(DT_COLUMN_COUNT_TOTAL) + _
                                                IIf(DbRow.IsNull(SQL_COLUMN_TIME_TOTAL), 0, 1)

    _total_row.Item(DT_COLUMN_AVG_TOTAL) = IIf(_total_row.Item(DT_COLUMN_COUNT_TOTAL) <> 0, _total_row.Item(DT_COLUMN_TOTALS_TOTAL) / _total_row.Item(DT_COLUMN_COUNT_TOTAL), 0)

    Return True

  End Function ' UpdateTotals

  ' PURPOSE : Obtain the row from DataTable m_totals_data_table according to AlarmType.
  '           If it doesn't exist, add a new row for the AlarmType.
  '
  '  PARAMS :
  '     - INPUT :
  '           - AlarmType
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '           - A DataRow for the AlarmType totals.
  '
  Private Function GetRowFromTotalsByType(ByVal AlarmType As String) As DataRow

    Dim _selected_rows() As DataRow
    Dim _filter As String
    Dim _row_new_type As DataRow

    _filter = ""

    _filter = "GROUP_NAME LIKE '" + AlarmType + "'"

    _selected_rows = m_totals_data_table.Select(_filter)

    If _selected_rows.Length = 0 Then
      _row_new_type = m_totals_data_table.NewRow
      _row_new_type.Item(DT_COLUMN_TOTALS_GROUP_NAME) = AlarmType
      _row_new_type.Item(DT_COLUMN_TOTALS_ASIGNED) = 0
      _row_new_type.Item(DT_COLUMN_TOTALS_ACCEPTED) = 0
      _row_new_type.Item(DT_COLUMN_TOTALS_SOLVED) = 0
      _row_new_type.Item(DT_COLUMN_TOTALS_VALIDATED) = 0
      _row_new_type.Item(DT_COLUMN_TOTALS_TOTAL) = 0
      _row_new_type.Item(DT_COLUMN_COUNT_ASIGNED) = 0
      _row_new_type.Item(DT_COLUMN_COUNT_ACCEPTED) = 0
      _row_new_type.Item(DT_COLUMN_COUNT_SOLVED) = 0
      _row_new_type.Item(DT_COLUMN_COUNT_VALIDATED) = 0
      _row_new_type.Item(DT_COLUMN_COUNT_TOTAL) = 0
      _row_new_type.Item(DT_COLUMN_AVG_ASIGNED) = 0
      _row_new_type.Item(DT_COLUMN_AVG_ACCEPTED) = 0
      _row_new_type.Item(DT_COLUMN_AVG_SOLVED) = 0
      _row_new_type.Item(DT_COLUMN_AVG_VALIDATED) = 0
      _row_new_type.Item(DT_COLUMN_AVG_TOTAL) = 0
      m_totals_data_table.Rows.Add(_row_new_type)
    Else
      _row_new_type = _selected_rows(0)
    End If

    Return _row_new_type

  End Function ' GetRowFromTotalsByType

  ' PURPOSE : Create the data table for the totals by alarms type.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub InitTotalsDataTable()
    Dim _column As DataColumn

    If m_totals_data_table IsNot Nothing Then
      m_totals_data_table.Dispose()
      m_totals_data_table = Nothing
    End If

    m_totals_data_table = New DataTable("TOTALS_BY_ALARM_TYPE")

    _column = New DataColumn()
    _column.DataType = System.Type.GetType("System.String")
    _column.ColumnName = "GROUP_NAME"
    _column.ReadOnly = False
    m_totals_data_table.Columns.Add(_column)

    _column = New DataColumn()
    _column.DataType = System.Type.GetType("System.Int32")
    _column.ColumnName = "ASIGNED"
    _column.ReadOnly = False
    m_totals_data_table.Columns.Add(_column)

    _column = New DataColumn()
    _column.DataType = System.Type.GetType("System.Int32")
    _column.ColumnName = "ACCEPTED"
    _column.ReadOnly = False
    m_totals_data_table.Columns.Add(_column)

    _column = New DataColumn()
    _column.DataType = System.Type.GetType("System.Int32")
    _column.ColumnName = "SOLVED"
    _column.ReadOnly = False
    m_totals_data_table.Columns.Add(_column)

    _column = New DataColumn()
    _column.DataType = System.Type.GetType("System.Int32")
    _column.ColumnName = "VALIDATED"
    _column.ReadOnly = False
    m_totals_data_table.Columns.Add(_column)

    _column = New DataColumn()
    _column.DataType = System.Type.GetType("System.Int32")
    _column.ColumnName = "TOTAL"
    _column.ReadOnly = False
    m_totals_data_table.Columns.Add(_column)

    _column = New DataColumn()
    _column.DataType = System.Type.GetType("System.Int32")
    _column.ColumnName = "COUNT_ASIGNED"
    _column.ReadOnly = False
    m_totals_data_table.Columns.Add(_column)

    _column = New DataColumn()
    _column.DataType = System.Type.GetType("System.Int32")
    _column.ColumnName = "COUNT_ACCEPTED"
    _column.ReadOnly = False
    m_totals_data_table.Columns.Add(_column)

    _column = New DataColumn()
    _column.DataType = System.Type.GetType("System.Int32")
    _column.ColumnName = "COUNT_SOLVED"
    _column.ReadOnly = False
    m_totals_data_table.Columns.Add(_column)

    _column = New DataColumn()
    _column.DataType = System.Type.GetType("System.Int32")
    _column.ColumnName = "COUNT_VALIDATED"
    _column.ReadOnly = False
    m_totals_data_table.Columns.Add(_column)

    _column = New DataColumn()
    _column.DataType = System.Type.GetType("System.Int32")
    _column.ColumnName = "COUNT_TOTAL"
    _column.ReadOnly = False
    m_totals_data_table.Columns.Add(_column)

    _column = New DataColumn()
    _column.DataType = System.Type.GetType("System.Int32")
    _column.ColumnName = "AVG_ASIGNED"
    _column.ReadOnly = False
    m_totals_data_table.Columns.Add(_column)

    _column = New DataColumn()
    _column.DataType = System.Type.GetType("System.Int32")
    _column.ColumnName = "AVG_ACCEPTED"
    _column.ReadOnly = False
    m_totals_data_table.Columns.Add(_column)

    _column = New DataColumn()
    _column.DataType = System.Type.GetType("System.Int32")
    _column.ColumnName = "AVG_SOLVED"
    _column.ReadOnly = False
    m_totals_data_table.Columns.Add(_column)

    _column = New DataColumn()
    _column.DataType = System.Type.GetType("System.Int32")
    _column.ColumnName = "AVG_VALIDATED"
    _column.ReadOnly = False
    m_totals_data_table.Columns.Add(_column)

    _column = New DataColumn()
    _column.DataType = System.Type.GetType("System.Int32")
    _column.ColumnName = "AVG_TOTAL"
    _column.ReadOnly = False
    m_totals_data_table.Columns.Add(_column)

  End Sub ' InitTotalsDataTable

#End Region ' Private Functions

#Region " Events "
  Private Sub chk_show_all_CheckedChanged(sender As Object, e As EventArgs) Handles chk_show_all.CheckedChanged

    m_show_all_grid = chk_show_all.Checked

  End Sub ' chk_show_all_CheckedChanged

  Private Sub chk_state_pending_all_CheckedChanged(sender As Object, e As EventArgs) Handles chk_state_pending_all.CheckedChanged
    Dim _checkbox As CheckBox

    _checkbox = sender

    chk_state_not_pending.Checked = _checkbox.Checked
    chk_state_pending.Checked = _checkbox.Checked
    chk_state_progress.Checked = _checkbox.Checked

  End Sub ' chk_state_pending_all_CheckedChanged

  Private Sub chk_state_CheckedChanged(sender As Object, e As EventArgs) Handles chk_state_not_pending.CheckedChanged, chk_state_progress.CheckedChanged, chk_state_pending.CheckedChanged
    Dim _checkbox As CheckBox

    _checkbox = sender

    If Not _checkbox.Checked Then
      RemoveHandler chk_state_pending_all.CheckedChanged, AddressOf Me.chk_state_pending_all_CheckedChanged
      chk_state_pending_all.Checked = False
      AddHandler chk_state_pending_all.CheckedChanged, AddressOf Me.chk_state_pending_all_CheckedChanged
    End If

  End Sub ' chk_state_not_pending_CheckedChanged

#End Region ' Events

End Class