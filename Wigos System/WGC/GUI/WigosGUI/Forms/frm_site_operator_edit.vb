'-------------------------------------------------------------------
' Copyright � 2011 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_site_operator_edit.vb
' DESCRIPTION:   Site Operator edition form
' AUTHOR:        Agust� Poch
' CREATION DATE: 04-FEB-2011
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 04-FEB-2011  APB    Initial version.
'
'--------------------------------------------------------------------

Option Explicit On 

#Region " Imports "

Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE

#End Region ' Imports

Public Class frm_site_operator_edit
  Inherits frm_base_edit

#Region " Constants "

  Private Const LEN_OPERATOR_NAME As Integer = 30

  Private Const GRID_COLUMN_SITE_ID As Integer = 0
  Private Const GRID_COLUMN_SITE_NAME As Integer = 1
  Private Const GRID_COLUMNS As Integer = 2
  Private Const GRID_HEADER_ROWS As Integer = 1

#End Region ' Constants

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer
  Friend WithEvents gb_sites As System.Windows.Forms.GroupBox
  Friend WithEvents dg_sites As GUI_Controls.uc_grid

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents ef_operator_name As GUI_Controls.uc_entry_field
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.ef_operator_name = New GUI_Controls.uc_entry_field
    Me.gb_sites = New System.Windows.Forms.GroupBox
    Me.dg_sites = New GUI_Controls.uc_grid
    Me.panel_data.SuspendLayout()
    Me.gb_sites.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_sites)
    Me.panel_data.Controls.Add(Me.ef_operator_name)
    Me.panel_data.Size = New System.Drawing.Size(391, 318)
    '
    'ef_operator_name
    '
    Me.ef_operator_name.DoubleValue = 0
    Me.ef_operator_name.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_operator_name.IntegerValue = 0
    Me.ef_operator_name.IsReadOnly = False
    Me.ef_operator_name.Location = New System.Drawing.Point(16, 20)
    Me.ef_operator_name.Name = "ef_operator_name"
    Me.ef_operator_name.Size = New System.Drawing.Size(363, 25)
    Me.ef_operator_name.SufixText = "Sufix Text"
    Me.ef_operator_name.SufixTextVisible = True
    Me.ef_operator_name.TabIndex = 0
    Me.ef_operator_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_operator_name.TextValue = ""
    Me.ef_operator_name.Value = ""
    '
    'gb_sites
    '
    Me.gb_sites.Controls.Add(Me.dg_sites)
    Me.gb_sites.Location = New System.Drawing.Point(3, 56)
    Me.gb_sites.Name = "gb_sites"
    Me.gb_sites.Size = New System.Drawing.Size(376, 261)
    Me.gb_sites.TabIndex = 4
    Me.gb_sites.TabStop = False
    Me.gb_sites.Text = "xSalas Asociadas"
    '
    'dg_sites
    '
    Me.dg_sites.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.dg_sites.CurrentCol = -1
    Me.dg_sites.CurrentRow = -1
    Me.dg_sites.Location = New System.Drawing.Point(13, 20)
    Me.dg_sites.Name = "dg_sites"
    Me.dg_sites.PanelRightVisible = False
    Me.dg_sites.Redraw = True
    Me.dg_sites.Size = New System.Drawing.Size(351, 235)
    Me.dg_sites.Sortable = False
    Me.dg_sites.SortAscending = True
    Me.dg_sites.SortByCol = 0
    Me.dg_sites.TabIndex = 2
    Me.dg_sites.ToolTipped = True
    Me.dg_sites.TopRow = -2
    '
    'frm_site_operator_edit
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(493, 326)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_site_operator_edit"
    Me.Text = "xEdici�n de Operador de Salas"
    Me.panel_data.ResumeLayout(False)
    Me.gb_sites.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Members "

  Private m_input_operator_name As String
  Private DeleteOperation As Boolean

#End Region ' Members

#Region " Override Functions "

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_SITE_OPERATOR_EDIT

    ' Call Base Form procedure
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    ' Hide the delete button if ScreenMode is New
    If Me.ScreenMode = frm_base_edit.ENUM_SCREEN_MODE.MODE_NEW Then
      GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
    Else
      GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = True
    End If

    ' Add any initialization after the MyBase.GUI_InitControls() call
    Me.Text = GLB_NLS_GUI_CONFIGURATION.GetString(352)

    ef_operator_name.Text = GLB_NLS_GUI_CONFIGURATION.GetString(210)
    Call ef_operator_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_OPERATOR_NAME)

    gb_sites.Text = GLB_NLS_GUI_CONFIGURATION.GetString(382)
    Call GUI_StyleView()

  End Sub ' GUI_InitControls

  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim operator_item As CLASS_SITE_OPERATOR

    operator_item = DbReadObject
    ef_operator_name.Value = operator_item.OperatorName
    Call RefreshGrid(operator_item)

  End Sub ' GUI_SetScreenData

  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Dim nls_param1 As String

    ' blank field
    If ef_operator_name.Value = "" Then
      nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(381)
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), _
                      mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
                      , _
                      , _
                      nls_param1)
      Call ef_operator_name.Focus()

      Return False
    End If

    Return True

  End Function ' GUI_IsScreenDataOk

  Protected Overrides Sub GUI_GetScreenData()

    Dim operator_item As CLASS_SITE_OPERATOR

    operator_item = DbEditedObject
    operator_item.OperatorName = ef_operator_name.Value

  End Sub ' GUI_GetScreenData

  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Dim operator_class As CLASS_SITE_OPERATOR
    Dim nls_param1 As String
    Dim nls_param2 As String
    Dim rc As mdl_NLS.ENUM_MB_RESULT

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_SITE_OPERATOR
        operator_class = DbEditedObject
        operator_class.OperatorId = 0
        DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          operator_class = Me.DbEditedObject
          nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(381)
          nls_param2 = m_input_operator_name
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(102), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1, _
                          nls_param2)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_INSERT
        operator_class = Me.DbEditedObject
        Call MyBase.GUI_DB_Operation(frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_INSERT)

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_INSERT
        Call MyBase.GUI_DB_Operation(DbOperation)

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          operator_class = Me.DbEditedObject
          nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(381)
          nls_param2 = operator_class.OperatorName
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(110), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1, _
                          nls_param2)
          Call ef_operator_name.Focus()

        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          operator_class = Me.DbEditedObject
          nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(381)
          nls_param2 = operator_class.OperatorName
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(104), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1, _
                          nls_param2)

        ElseIf Me.DbStatus = ENUM_STATUS.STATUS_OK Then
          Call MyBase.GUI_DB_Operation(frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE
        operator_class = Me.DbEditedObject
        Call MyBase.GUI_DB_Operation(frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE)

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_UPDATE
        Call MyBase.GUI_DB_Operation(DbOperation)

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          operator_class = Me.DbEditedObject
          nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(381)
          nls_param2 = operator_class.OperatorName
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(110), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1, _
                          nls_param2)
          Call ef_operator_name.Focus()

        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          operator_class = Me.DbEditedObject
          nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(381)
          nls_param2 = operator_class.OperatorName
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(106), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1, _
                          nls_param2)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE
        DeleteOperation = False
        operator_class = Me.DbEditedObject
        nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(381)
        nls_param2 = operator_class.OperatorName
        rc = NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(111), _
                        ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        ENUM_MB_BTN.MB_BTN_YES_NO, _
                        ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
                        nls_param1, _
                        nls_param2)
        If rc = ENUM_MB_RESULT.MB_RESULT_YES Then
          DeleteOperation = True
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_DELETE
        If DeleteOperation Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        Else
          DbStatus = ENUM_STATUS.STATUS_ERROR
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK And DeleteOperation Then
          operator_class = Me.DbEditedObject
          nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(381)
          nls_param2 = operator_class.OperatorName
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(115), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1, _
                          nls_param2)
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)

    Dim super_center_flag As String

    ' Protect against access when not running in SuperCenter Mode
    super_center_flag = GetSuperCenterData("Enabled")
    If super_center_flag = "1" Then
      AndPerm.Write = True
      AndPerm.Delete = True
      AndPerm.Read = True
    Else
      AndPerm.Write = False
      AndPerm.Delete = False
      AndPerm.Read = False
    End If

  End Sub ' GUI_Permissions

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.ef_operator_name
  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Init form in new mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Overloads Sub ShowNewItem()

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    DbObjectId = Nothing
    DbStatus = ENUM_STATUS.STATUS_OK

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowNewItem

  ' PURPOSE: Init form in edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - ProfileId
  '           - ProfileName
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Overloads Sub ShowEditItem(ByVal OperatorId As Integer, ByVal OperatorName As String)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Me.DbObjectId = OperatorId
    Me.m_input_operator_name = OperatorName

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If
  End Sub ' ShowEditItem

#End Region ' Override Functions

#Region " Private Functions "

  ' PURPOSE: Configure Grid columns
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  '
  Private Sub GUI_StyleView()

    Call Me.dg_sites.Init(GRID_COLUMNS, GRID_HEADER_ROWS)

    ' ST_SITE_ID
    Me.dg_sites.Column(GRID_COLUMN_SITE_ID).Header.Text = GLB_NLS_GUI_CONFIGURATION.GetString(209)
    Me.dg_sites.Column(GRID_COLUMN_SITE_ID).Width = 1100
    Me.dg_sites.Column(GRID_COLUMN_SITE_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    ' ST_NAME
    Me.dg_sites.Column(GRID_COLUMN_SITE_NAME).Header.Text = GLB_NLS_GUI_CONFIGURATION.GetString(210)
    Me.dg_sites.Column(GRID_COLUMN_SITE_NAME).Width = 3750
    Me.dg_sites.Column(GRID_COLUMN_SITE_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

  End Sub ' GUI_StyleView

  ' PURPOSE: Populate sites list
  '
  '  PARAMS:
  '     - INPUT:
  '           - Operator object which contains array of assigned sites
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  '
  Private Sub RefreshGrid(ByVal operator_item As CLASS_SITE_OPERATOR)

    Dim idx_site As Integer

    Call Me.dg_sites.Clear()
    Me.dg_sites.Redraw = False

    For idx_site = 0 To operator_item.NumSites - 1
      idx_site = Me.dg_sites.AddRow()
      Me.dg_sites.Cell(idx_site, GRID_COLUMN_SITE_ID).Value = operator_item.SiteList(idx_site).site_id
      Me.dg_sites.Cell(idx_site, GRID_COLUMN_SITE_NAME).Value = operator_item.SiteList(idx_site).site_name
    Next

    Me.dg_sites.Redraw = True

  End Sub ' RefreshGrid

#End Region ' Private Functions 

End Class