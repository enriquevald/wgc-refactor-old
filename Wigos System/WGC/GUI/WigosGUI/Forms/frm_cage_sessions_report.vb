
'-------------------------------------------------------------------
' Copyright � 2009 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_cage_sessions_report.vb
'
' DESCRIPTION:   Show full details for a cage session 
'
' AUTHOR:        Rub�n Rodr�guez
'
' CREATION DATE: 30-JUL-2014
'
' REVISION HISTORY:
'
' Date         Author     Description
' -----------  ---------  ---------------------------------------------------
' 30-JUL-2014  RRR        Initial version.
' 05-NOV-2014  SGB        Fixed Bug WIG-1649: Not shown decimal denomination in Excel.
' 11-NOV-2014  RRR        Fixed Bug WIG-1665: Error with Excel.
' 20-JAN-2015  FJC        Set features form (size and autoscroll panels) when size's form exceeds desktop area
' 10-FEB-2015  DRV        Fixed Bug WIG-1665: differences between the data displayed and printed
' 10-MAR-2015  YNM        Fixed Bug WIG-2137: Stock on Close Session is different that Current Stock.
' 16-JUN-2015  YNM        Fixed Bug WIG-2444: Cage General Report: incorrect data when export to excel
' 31-AUG-2015  FAV        Fixed Bug TFS-4097: Use the "Cashier.PlayerCard-ConceptName" general param for the "Deposito de tarjeta" concept.
' 26-FEB-2016  RAB        Bug 8508:Reporte de b�veda: Al exportar a Excel, cambio el campo "Resto" por "Monedas"
' 02-MAY-2016  JML        Product Backlog Item 10825:Gaming Tables (Fase 1): Items, tickets & chips (Cage) 
' 19-DEC-2016  RAB        Bug 11169: REF. 17075 - Report Cage: Correct string "Rest" when exporting to Excel.
' 23-AUG-2017  EOR        Bug 29438: WIGOS-3913 CAGE - There are fields in GUI but NOT in excel report
'--------------------------------------------------------------------
Option Strict Off
Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_Reports
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Imports WSI.Common
Imports WSI.Common.TITO


Public Class frm_cage_sessions_report
  Inherits frm_base_edit

#Region " Constants "
  Public Const NONE_DATA_STRING As String = "---"
  Private Const OUTPUT_FOLDER As String = "Reports"
  Private Const FORM_DB_MIN_VERSION As Short = 183
#End Region ' Constants

#Region " Enums"

  Public Enum ENUM_OPEN_MODE
    SESSION = 0
    MULTI_SESSION = 1
    CAGE_GLOBAL = 2
  End Enum

#End Region ' Enums

#Region " Members "

  Private m_date_from As Date
  Private m_date_to As Date
  Private m_first_session As String
  Private m_last_session As String
  Private m_cage_session_name As String
  Private m_cage_session_status As Int32

  Private m_gui_user_type As GU_USER_TYPE

  Private m_open_mode As ENUM_OPEN_MODE

  ' Excel
  Private m_currencies_stock As SortedDictionary(Of VoucherCageSessionsReport.CageSummaryKey, Decimal)
  Private m_chips_stock_summary As Decimal

  ' Progress bar
  Private m_progress As frm_excel_progress = Nothing

  Private m_session_data As New WSI.Common.CageSessionReport.TYPE_CAGE_GLOBAL_SESSION_REPORT

  Dim m_national_currency As String

#End Region ' Members

#Region "Overrides"

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CAGE_SESSION_REPORT

    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    Dim _split_data As New WSI.Common.TYPE_SPLITS

    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    WSI.Common.Split.ReadSplitParameters(_split_data)

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5538) ' "Informe de B�veda"

    '- Buttons
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_PRINT).Visible = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_PRINT).Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_PRINT_ENABLED).Read

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Visible = False 'False

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Visible = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Text = GLB_NLS_GUI_CONTROLS.GetString(27)

    ' ICS 12-MAR-2013: Enable Excel button if user has read permission
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Enabled = Me.Permissions.Read And CurrentUser.Permissions(ENUM_FORM.FORM_EXCEL_ENABLED).Read

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Visible = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(1)

    ' - Labels
    Select Case Me.m_open_mode

      Case ENUM_OPEN_MODE.SESSION
        Me.lbl_cg_name.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5591) & Me.m_cage_session_name

        If Me.m_cage_session_status = 0 Then
          Me.tf_cg_status.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5540) '"Abierta"
        Else
          Me.tf_cg_status.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5541) '"Cerrada"
        End If

      Case ENUM_OPEN_MODE.MULTI_SESSION
        Me.lbl_cg_name.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5542, Me.m_first_session, Me.m_last_session) ' "Informe de las sesiones de b�veda comprendidas entre " & Me.m_first_session & " y " & Me.m_last_session
        Me.tf_cg_status.Visible = False

      Case Else
        Me.lbl_cg_name.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5543) '"Informe general de b�veda"
        Me.tf_cg_status.Visible = False

    End Select

    ' - Header
    Me.tf_cg_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6507) '"Estado"

    m_national_currency = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")

  End Sub 'GUI_InitControls

  ' PURPOSE: Check for screen data.
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    ' Nothing to check
    Return True

  End Function ' GUI_IsScreenDataOk

  ' PURPOSE: Sets database permissions.
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _cage As New WSI.Common.CageSessionReport()
    Dim _parameters_overview As VoucherCageSessionsReport.ParametersCageSessionsReport
    Dim _voucher_file_name As String
    Dim _voucher As VoucherCageSessionsReport
    Dim _cage_session_status As Boolean

    Try

      Using _db_trx As New DB_TRX()

        If Me.m_open_mode = ENUM_OPEN_MODE.CAGE_GLOBAL Then
          m_session_data = _cage.ReadCageGlobalReport()
        Else
          m_session_data = _cage.ReadCageGlobalSessionsReport(Me.DbObjectId, _cage_session_status)
        End If

        If (Not _cage_session_status) Then
          Me.m_cage_session_status = 0
          Me.tf_cg_status.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5540) '"Abierta"
        Else
          Me.m_cage_session_status = 1
          Me.tf_cg_status.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5541) '"Cerrada"
        End If

        _parameters_overview = New VoucherCageSessionsReport.ParametersCageSessionsReport()

        _parameters_overview.UserType = Me.m_gui_user_type
        _parameters_overview.BackColor = Me.BackColor
        _parameters_overview.CageSessionId = Me.DbObjectId

        _voucher = New VoucherCageSessionsReport(m_session_data, PrintMode.Preview, _parameters_overview, _db_trx.SqlTransaction)
        _voucher_file_name = VoucherManager.SaveHTMLVoucherOnFile("REPRINT", _voucher.VoucherHTML)
        web_browser.Navigate(_voucher_file_name)

      End Using ' _db_trx

      'Header


    Catch _ex As Exception

      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Log.Exception(_ex)

      Me.Close()

    End Try

  End Sub ' GUI_SetScreenData

  ' PURPOSE: Sets database permissions.
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)

  End Sub  'GUI_Permissions

  ' PURPOSE: Sets the initial focus
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Focus()
  End Sub

  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

  End Sub 'GUI_DB_Operation

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Select Case ButtonId

      Case ENUM_BUTTON.BUTTON_OK
        Call MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_EXCEL)

      Case ENUM_BUTTON.BUTTON_PRINT
        Call MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_PRINT)

      Case Else
        ' Set button pressed as Cancel - Even when 'Enter' key is pressed.
        ' (Base edit assumes when key Enter is pressed 
        ButtonId = ENUM_BUTTON.BUTTON_CANCEL
        Call MyBase.GUI_ButtonClick(ButtonId)

    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Generate Excel report
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ExcelResultList()
    ReportFile(False)
  End Sub

  ' PURPOSE: Print report
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_PrintResultList()
    ReportFile(True)
  End Sub

#End Region ' Overrides


#Region "Private Functions"

  ' PURPOSE: Generate the report file
  '
  '    - INPUT:
  '         
  '    - OUTPUT:
  '
  ' RETURNS:
  Public Sub GenerateReportFile(Optional ByVal Session1 As String = "", Optional ByVal Session2 As String = "", _
                                Optional ByVal ShowFileExportedForm As Boolean = True, _
                                Optional ByVal OnlyPreview As Boolean = False)

    Dim _session_reader As New WSI.Common.CageSessionReport()
    Dim _dt_cage_session_details As DataTable
    Dim _dr_cage_session_detail As DataRow
    Dim _col1_name As String
    Dim _col2_name As String
    Dim _col3_name As String
    Dim _col4_name As String
    Dim _col5_name As String
    Dim _col6_name As String
    Dim _col7_name As String
    Dim _col8_name As String
    Dim _col9_name As String
    Dim _idx_row As Int32

    Dim _params As Dictionary(Of String, String)
    Dim _result As ExcelConversion.ExcelFileLocation
    Dim _national_currency As String
    Dim _file_path As String
    Dim _excel As GUI_Reports.CLASS_EXCEL_DATA
    Dim _workbook As SpreadsheetGear.IWorkbook

    _national_currency = CurrencyExchange.GetNationalCurrency()
    _params = New Dictionary(Of String, String)

    _params.Add(GLB_NLS_GUI_CONTROLS.GetString(377), GUI_FormatDate(WGDB.Now, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS))
    _params.Add(GLB_NLS_GUI_CONTROLS.GetString(378), CurrentUser.Name & "@" & Environment.MachineName)

    Select Case Me.m_open_mode
      Case ENUM_OPEN_MODE.SESSION
        _params.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(3459), Session1)
      Case ENUM_OPEN_MODE.MULTI_SESSION
        _params.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(297), Session1)
        _params.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(298), Session2)
    End Select

    _params.Add("", "")

    _dt_cage_session_details = New DataTable()
    _dr_cage_session_detail = _dt_cage_session_details.NewRow()

    _col1_name = "TITLE"
    _col2_name = "DESCRIPTION"
    _col3_name = "VALUE1"
    _col4_name = "VALUE2"
    _col5_name = "VALUE3"
    _col6_name = "VALUE4"
    _col7_name = "VALUE5"
    _col8_name = "VALUE6"
    _col9_name = "TYPE"

    Try

      _file_path = System.Environment.CurrentDirectory & Path.DirectorySeparatorChar & ".." & Path.DirectorySeparatorChar & OUTPUT_FOLDER

      If Not Directory.Exists(_file_path) Then
        _file_path = System.Environment.CurrentDirectory
      End If

      _result = New ExcelConversion.ExcelFileLocation(_file_path)

      If Not ExcelConversion.HasPermissionsOverDirectory(_file_path) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2736), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _file_path)
        Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                                          GLB_ApplicationName, _
                                          "frm_cage_sessions_report.vb", _
                                          "", _
                                          "", _
                                          "", _
                                         GLB_NLS_GUI_PLAYER_TRACKING.GetString(5545, _file_path))
      Else
        'Using _db_trx As New DB_TRX()

        With _dt_cage_session_details
          .Columns.Add(_col1_name, Type.GetType("System.String")).AllowDBNull = True
          .Columns.Add(_col2_name, Type.GetType("System.String")).AllowDBNull = True
          .Columns.Add(_col3_name, Type.GetType("System.String")).AllowDBNull = True
          .Columns.Add(_col4_name, Type.GetType("System.String")).AllowDBNull = True
          .Columns.Add(_col5_name, Type.GetType("System.String")).AllowDBNull = True
          .Columns.Add(_col6_name, Type.GetType("System.String")).AllowDBNull = True
          .Columns.Add(_col7_name, Type.GetType("System.String")).AllowDBNull = True
          .Columns.Add(_col8_name, Type.GetType("System.String")).AllowDBNull = True
          .Columns.Add(_col9_name, Type.GetType("System.Int32")).AllowDBNull = True
        End With

        Select Case Me.m_open_mode
          Case ENUM_OPEN_MODE.SESSION
            _dt_cage_session_details.TableName = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5621)
          Case ENUM_OPEN_MODE.MULTI_SESSION
            _dt_cage_session_details.TableName = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5622)
          Case Else
            _dt_cage_session_details.TableName = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5543)
        End Select


        ' RESULTADO DE B�VEDA

        ' Header section
        _dr_cage_session_detail = _dt_cage_session_details.NewRow()
        _dr_cage_session_detail(_col1_name) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5547)   '"Resultado de B�veda"
        _dr_cage_session_detail(_col2_name) = DBNull.Value
        _dr_cage_session_detail(_col3_name) = DBNull.Value
        _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.BOLD

        _dt_cage_session_details.Rows.Add(_dr_cage_session_detail)

        ' Blank Line
        _dr_cage_session_detail = _dt_cage_session_details.NewRow()
        _dr_cage_session_detail(_col1_name) = DBNull.Value
        _dr_cage_session_detail(_col2_name) = DBNull.Value
        _dr_cage_session_detail(_col3_name) = DBNull.Value
        _dr_cage_session_detail(_col4_name) = DBNull.Value
        _dr_cage_session_detail(_col5_name) = DBNull.Value
        _dr_cage_session_detail(_col6_name) = DBNull.Value
        _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.TOPBORDER

        _dt_cage_session_details.Rows.Add(_dr_cage_session_detail)

        ' Depositos/Retiros
        Call InsertCageReportResult(m_session_data, _dt_cage_session_details)

        ' Blank Line
        _dr_cage_session_detail = _dt_cage_session_details.NewRow()
        _dr_cage_session_detail(_col1_name) = DBNull.Value
        _dr_cage_session_detail(_col2_name) = DBNull.Value
        _dr_cage_session_detail(_col3_name) = DBNull.Value
        _dr_cage_session_detail(_col4_name) = DBNull.Value
        _dr_cage_session_detail(_col5_name) = DBNull.Value
        _dr_cage_session_detail(_col6_name) = DBNull.Value
        _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.DEFAULT

        _dt_cage_session_details.Rows.Add(_dr_cage_session_detail)

        ' Other system concepts
        Call InsertOtherSystemConcepts(m_session_data, _dt_cage_session_details)

        ' Blank Line
        _dr_cage_session_detail = _dt_cage_session_details.NewRow()
        _dr_cage_session_detail(_col1_name) = DBNull.Value
        _dr_cage_session_detail(_col2_name) = DBNull.Value
        _dr_cage_session_detail(_col3_name) = DBNull.Value
        _dr_cage_session_detail(_col4_name) = DBNull.Value
        _dr_cage_session_detail(_col5_name) = DBNull.Value
        _dr_cage_session_detail(_col6_name) = DBNull.Value
        _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.DEFAULT

        _dt_cage_session_details.Rows.Add(_dr_cage_session_detail)

        ' Provisiones
        Call InsertCageReportLiabilities(m_session_data, _dt_cage_session_details)

        ' Blank Line
        _dr_cage_session_detail = _dt_cage_session_details.NewRow()
        _dr_cage_session_detail(_col1_name) = DBNull.Value
        _dr_cage_session_detail(_col2_name) = DBNull.Value
        _dr_cage_session_detail(_col3_name) = DBNull.Value
        _dr_cage_session_detail(_col4_name) = DBNull.Value
        _dr_cage_session_detail(_col5_name) = DBNull.Value
        _dr_cage_session_detail(_col6_name) = DBNull.Value
        _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.DEFAULT

        _dt_cage_session_details.Rows.Add(_dr_cage_session_detail)


        ' RESUMEN DE B�VEDA

        ' Header section
        _dr_cage_session_detail = _dt_cage_session_details.NewRow()
        _dr_cage_session_detail(_col1_name) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5619)  '"Resumen de B�veda"
        _dr_cage_session_detail(_col2_name) = DBNull.Value
        _dr_cage_session_detail(_col3_name) = DBNull.Value
        _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.BOLD

        _dt_cage_session_details.Rows.Add(_dr_cage_session_detail)

        ' Blank row
        _dr_cage_session_detail = _dt_cage_session_details.NewRow()
        _dr_cage_session_detail(_col1_name) = DBNull.Value
        _dr_cage_session_detail(_col2_name) = DBNull.Value
        _dr_cage_session_detail(_col3_name) = DBNull.Value
        _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.TOPBORDER

        _dt_cage_session_details.Rows.Add(_dr_cage_session_detail)

        ' Header Line
        _dr_cage_session_detail = _dt_cage_session_details.NewRow()
        _dr_cage_session_detail(_col1_name) = DBNull.Value
        _dr_cage_session_detail(_col2_name) = DBNull.Value
        _dr_cage_session_detail(_col3_name) = DBNull.Value
        _dr_cage_session_detail(_col4_name) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5550) '"(Entradas - Salidas)"
        _dr_cage_session_detail(_col5_name) = "Arqueos"
        _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.BOLD_ALIGN_RIGHT

        _dt_cage_session_details.Rows.Add(_dr_cage_session_detail)

        ' Summary
        Call InsertCageReportSummary(m_session_data, _dt_cage_session_details)

        ' Blank Line
        _dr_cage_session_detail = _dt_cage_session_details.NewRow()
        _dr_cage_session_detail(_col1_name) = DBNull.Value
        _dr_cage_session_detail(_col2_name) = DBNull.Value
        _dr_cage_session_detail(_col3_name) = DBNull.Value
        _dr_cage_session_detail(_col4_name) = DBNull.Value
        _dr_cage_session_detail(_col5_name) = DBNull.Value
        _dr_cage_session_detail(_col6_name) = DBNull.Value
        _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.DEFAULT

        _dt_cage_session_details.Rows.Add(_dr_cage_session_detail)


        ' TRANSACCIONES DE B�VEDA

        ' Header section
        _dr_cage_session_detail = _dt_cage_session_details.NewRow()
        _dr_cage_session_detail(_col1_name) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5562)   '"Transacciones de B�veda"
        _dr_cage_session_detail(_col2_name) = DBNull.Value
        _dr_cage_session_detail(_col3_name) = DBNull.Value
        _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.BOLD

        _dt_cage_session_details.Rows.Add(_dr_cage_session_detail)

        ' Blank row
        _dr_cage_session_detail = _dt_cage_session_details.NewRow()
        _dr_cage_session_detail(_col1_name) = DBNull.Value
        _dr_cage_session_detail(_col2_name) = DBNull.Value
        _dr_cage_session_detail(_col3_name) = DBNull.Value
        _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.TOPBORDER

        _dt_cage_session_details.Rows.Add(_dr_cage_session_detail)

        ' Header Line
        _dr_cage_session_detail = _dt_cage_session_details.NewRow()
        _dr_cage_session_detail(_col1_name) = DBNull.Value
        _dr_cage_session_detail(_col2_name) = DBNull.Value
        _dr_cage_session_detail(_col3_name) = DBNull.Value
        _dr_cage_session_detail(_col4_name) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5548)   '"Entradas"
        _dr_cage_session_detail(_col5_name) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5549)   '"Salidas"
        _dr_cage_session_detail(_col6_name) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5557)   '"Total"
        _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.BOLD_ALIGN_RIGHT

        _dt_cage_session_details.Rows.Add(_dr_cage_session_detail)

        ' Cashier
        Call InsertCageReportTransaction(m_session_data, CageSessionReport.CAGE_SOURCE_TARGETS.CASHIERS, _dt_cage_session_details)

        ' GamingTables
        If GamingTableBusinessLogic.IsGamingTablesEnabled() Then
          Call InsertCageReportTransaction(m_session_data, CageSessionReport.CAGE_SOURCE_TARGETS.GAMING_TABLES, _dt_cage_session_details)
        End If

        ' Stackers
        Call InsertCageReportTransaction(m_session_data, CageSessionReport.CAGE_SOURCE_TARGETS.TERMINALS, _dt_cage_session_details)

        ' Custom
        Call InsertCageReportTransaction(m_session_data, CageSessionReport.CAGE_SOURCE_TARGETS.CUSTOM, _dt_cage_session_details)


        ' STOCK DE B�VEDA

        ' Header section
        _dr_cage_session_detail = _dt_cage_session_details.NewRow()
        _dr_cage_session_detail(_col1_name) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5553)   '"Stock de B�veda"
        _dr_cage_session_detail(_col2_name) = DBNull.Value
        _dr_cage_session_detail(_col3_name) = DBNull.Value
        _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.BOLD

        _dt_cage_session_details.Rows.Add(_dr_cage_session_detail)

        ' Blank row
        _dr_cage_session_detail = _dt_cage_session_details.NewRow()
        _dr_cage_session_detail(_col1_name) = DBNull.Value
        _dr_cage_session_detail(_col2_name) = DBNull.Value
        _dr_cage_session_detail(_col3_name) = DBNull.Value
        _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.TOPBORDER

        _dt_cage_session_details.Rows.Add(_dr_cage_session_detail)

        ' Header Line
        _dr_cage_session_detail = _dt_cage_session_details.NewRow()
        _dr_cage_session_detail(_col1_name) = DBNull.Value
        _dr_cage_session_detail(_col2_name) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5554)   '"Divisa"
        _dr_cage_session_detail(_col3_name) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5555)   '"Denominaci�n"
        _dr_cage_session_detail(_col4_name) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5678)   '"Tipo"
        _dr_cage_session_detail(_col5_name) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4979)   '"Nombre"
        _dr_cage_session_detail(_col6_name) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6924)   '"Dibujo"
        _dr_cage_session_detail(_col7_name) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5556)   '"Cantidad"
        _dr_cage_session_detail(_col8_name) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5557)   '"Total"
        _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.BOLD_ALIGN_RIGHT

        _dt_cage_session_details.Rows.Add(_dr_cage_session_detail)

        ' Stock
        Call Me.InsertCageStock(m_session_data, _dt_cage_session_details)

        ' Blank row
        _dr_cage_session_detail = _dt_cage_session_details.NewRow()
        _dr_cage_session_detail(_col1_name) = DBNull.Value
        _dr_cage_session_detail(_col2_name) = DBNull.Value
        _dr_cage_session_detail(_col3_name) = DBNull.Value
        _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.DEFAULT

        _dt_cage_session_details.Rows.Add(_dr_cage_session_detail)


        ' RESUMEN DE STOCK

        ' Header section
        _dr_cage_session_detail = _dt_cage_session_details.NewRow()
        _dr_cage_session_detail(_col1_name) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5611)  ' "Resumen de Stock de B�veda"
        _dr_cage_session_detail(_col2_name) = DBNull.Value
        _dr_cage_session_detail(_col3_name) = DBNull.Value
        _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.BOLD

        _dt_cage_session_details.Rows.Add(_dr_cage_session_detail)

        ' Blank row
        _dr_cage_session_detail = _dt_cage_session_details.NewRow()
        _dr_cage_session_detail(_col1_name) = DBNull.Value
        _dr_cage_session_detail(_col2_name) = DBNull.Value
        _dr_cage_session_detail(_col3_name) = DBNull.Value
        _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.TOPBORDER

        _dt_cage_session_details.Rows.Add(_dr_cage_session_detail)

        Call Me.InsertCageStockSummary(m_session_data, _dt_cage_session_details)

        ' Shows the progress bar
        If ShowFileExportedForm Then
          If OnlyPreview Then
            m_progress = New frm_excel_progress(GLB_NLS_GUI_CONTROLS.GetString(5)) ' Imprimir
          Else
            m_progress = New frm_excel_progress()
          End If
          m_progress.Show()

          For _idx_row = 0 To _dt_cage_session_details.Rows.Count - 1
            UpdateProgressBar(_idx_row, _dt_cage_session_details.Rows.Count())
          Next
        End If

        _workbook = Nothing

        If ExcelConversion.CageSessionReportToExcel(_dt_cage_session_details, String.Empty, _params, True, _result, OnlyPreview, _workbook) Then

          If OnlyPreview Then
            _excel = New GUI_Reports.CLASS_EXCEL_DATA(True, True, _workbook, _dt_cage_session_details.Rows.Count + 10, , _dt_cage_session_details.Columns.Count)

            Try
              _excel.Title = _dt_cage_session_details.TableName
              _excel.Site = GetSiteOrMultiSiteName()
              _excel.PrintDateTime = GUI_FormatDate(WGDB.Now, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
              _excel.PrintUser = GLB_CurrentUser.Name & "@" & Environment.MachineName

              If m_progress IsNot Nothing Then
                m_progress.Close()
                m_progress.Dispose()
                m_progress = Nothing
              End If

              _excel.Print(True, True)

            Finally
              _excel.Dispose()
            End Try

          End If

        End If

        UpdateProgressBar(_dt_cage_session_details.Rows.Count(), _dt_cage_session_details.Rows.Count())

      End If

      ' Close the progress bar
      Try
        If m_progress IsNot Nothing Then
          m_progress.Close()
          m_progress.Dispose()
          m_progress = Nothing
        End If
      Catch ex As Exception
      End Try

      ' Show path file dialog popUp.
      If Not _result.ShowFile And Not String.IsNullOrEmpty(_result.Filepath) And Not String.IsNullOrEmpty(_result.Filename) And _result.ShowFileExportedForm Then
        Dim _frm As frm_file_exported

        _frm = New frm_file_exported(_result.Filepath, _result.Filename, _result.NlsId)
        _frm.ShowDialog()
      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub IncludeArray(ByVal MyArray() As DataRow, ByRef OtherArray() As DataRow)
    Dim StartIndex As Integer = OtherArray.Length
    Dim Count As Integer = MyArray.Length

    ReDim Preserve OtherArray(OtherArray.Length + MyArray.Length - 1)

    System.Array.Copy(MyArray, 0, OtherArray, StartIndex, Count)
  End Sub

  Private Sub InsertCageReportResult(ByVal SessionData As WSI.Common.CageSessionReport.TYPE_CAGE_GLOBAL_SESSION_REPORT _
                                        , ByRef OutDataTable As DataTable)
    Dim _drs() As DataRow

    Dim _source_target_name As String
    Dim _concept_description As String
    Dim _where As String

    Dim _cage_transaction As SortedDictionary(Of VoucherCageSessionsReport.CageTransactionsKey, VoucherCageSessionsReport.CageTransactionsClass)

    _cage_transaction = New SortedDictionary(Of VoucherCageSessionsReport.CageTransactionsKey, VoucherCageSessionsReport.CageTransactionsClass)(New VoucherCageSessionsReport.CageTransactionsKey.SortComparer())

    'CASHIER
    _where = String.Format("CM_SOURCE_TARGET_ID IN({0}) AND (CM_CONCEPT_ID = 0)", CType(WSI.Common.CageSessionReport.CAGE_SOURCE_TARGETS.CASHIERS, Integer))
    _drs = SessionData.CageDtTransactions.Select(_where, "ORDEN")

    'GAMMING TABLE
    If (GamingTableBusinessLogic.IsGamingTablesEnabled()) Then
      _where = String.Format("CM_SOURCE_TARGET_ID IN({0}) AND (CM_CONCEPT_ID = 0)", CType(WSI.Common.CageSessionReport.CAGE_SOURCE_TARGETS.GAMING_TABLES, Integer))
      Call IncludeArray(SessionData.CageDtTransactions.Select(_where, "ORDEN"), _drs)
    End If    

    'TERMINALS
    _where = String.Format("CM_SOURCE_TARGET_ID IN({0}) AND (CM_CONCEPT_ID = 0)", CType(WSI.Common.CageSessionReport.CAGE_SOURCE_TARGETS.TERMINALS, Integer))
    Call IncludeArray(SessionData.CageDtTransactions.Select(_where, "ORDEN"), _drs)

    'CUSTOMS
    _where = String.Format("CM_SOURCE_TARGET_ID NOT IN({0}, {1}, {2}) AND (CM_CONCEPT_ID = 0)",
                           CType(WSI.Common.CageSessionReport.CAGE_SOURCE_TARGETS.CASHIERS, Integer),
                           CType(WSI.Common.CageSessionReport.CAGE_SOURCE_TARGETS.GAMING_TABLES, Integer),
                           CType(WSI.Common.CageSessionReport.CAGE_SOURCE_TARGETS.TERMINALS, Integer))
    Call IncludeArray(SessionData.CageDtTransactions.Select(_where, "ORDEN"), _drs)


    For Each _dr As DataRow In _drs

      If _dr("CST_SOURCE_TARGET_NAME") Is DBNull.Value Then
        _source_target_name = String.Empty
      Else
        _source_target_name = _dr("CST_SOURCE_TARGET_NAME")
      End If

      _concept_description = _dr("CC_DESCRIPTION").ToString()

      Select Case CType(_dr("CM_SOURCE_TARGET_ID"), CageMeters.CageSystemSourceTarget)

        Case CageMeters.CageSystemSourceTarget.Cashiers
          _source_target_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5563)   '"Cajas"
        Case CageMeters.CageSystemSourceTarget.GamingTables
          _source_target_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5564)   '"Mesas"
        Case CageMeters.CageSystemSourceTarget.Terminals
          _source_target_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5565)   '"Terminales"

      End Select

      Dim _key As VoucherCageSessionsReport.CageTransactionsKey = New VoucherCageSessionsReport.CageTransactionsKey()
      _key.IsoCode = _dr("CM_ISO_CODE").ToString()
      _key.CageCurrencyType = CType(_dr("CAGE_CURRENCY_TYPE"), CageCurrencyType)
      _key.SourceTargetName = _source_target_name
      _key.ConceptDescription = _concept_description
      _key.SourceTargetId = CType(_dr("CM_SOURCE_TARGET_ID").ToString(), Long)


      If Not _cage_transaction.ContainsKey(_key) Then
        _cage_transaction.Add(_key, New VoucherCageSessionsReport.CageTransactionsClass())
      End If

      _cage_transaction(_key).Outputs = CType(_dr("CM_VALUE_OUT"), Decimal)
      _cage_transaction(_key).Inputs = CType(_dr("CM_VALUE_IN"), Decimal)

    Next

    Call Me.CreateReportSummaryFromDictionary(_cage_transaction, OutDataTable)

  End Sub

  Private Sub CreateReportSummaryFromDictionary(ByVal CageTransaction As SortedDictionary(Of VoucherCageSessionsReport.CageTransactionsKey, VoucherCageSessionsReport.CageTransactionsClass), ByRef OutDataTable As DataTable)

    Dim _dr_cage_session_detail As DataRow
    Dim _national_currency As String

    Dim _currency_iso_type As CurrencyIsoType

    Dim _currencies_in As SortedDictionary(Of CurrencyIsoType, Decimal)
    Dim _currencies_out As SortedDictionary(Of CurrencyIsoType, Decimal)
    Dim _currencies_diff As SortedDictionary(Of CurrencyIsoType, Decimal)

    Dim _col1_name As String = "TITLE"
    Dim _col2_name As String = "DESCRIPTION"
    Dim _col3_name As String = "VALUE1"
    Dim _col4_name As String = "VALUE2"
    Dim _col5_name As String = "VALUE3"
    Dim _col6_name As String = "VALUE4"
    Dim _col9_name As String = "TYPE"

    _national_currency = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")

    _currencies_in = New SortedDictionary(Of CurrencyIsoType, Decimal)(New CurrencyIsoType.SortComparer)
    _currencies_out = New SortedDictionary(Of CurrencyIsoType, Decimal)(New CurrencyIsoType.SortComparer)
    _currencies_diff = New SortedDictionary(Of CurrencyIsoType, Decimal)(New CurrencyIsoType.SortComparer)

    For Each _cage_trans As KeyValuePair(Of VoucherCageSessionsReport.CageTransactionsKey, VoucherCageSessionsReport.CageTransactionsClass) In CageTransaction
      _currency_iso_type = New CurrencyIsoType()
      _currency_iso_type.IsoCode = _cage_trans.Key.IsoCode
      _currency_iso_type.Type = _cage_trans.Key.CageCurrencyType

      If _cage_trans.Key.ConceptId = 0 Then
        If _currencies_in.ContainsKey(_currency_iso_type) Then
          _currencies_in(_currency_iso_type) = _currencies_in(_currency_iso_type) + _cage_trans.Value.Inputs
          _currencies_out(_currency_iso_type) = _currencies_out(_currency_iso_type) + _cage_trans.Value.Outputs
          _currencies_diff(_currency_iso_type) = _currencies_diff(_currency_iso_type) + _cage_trans.Value.Difference
        Else
          _currencies_in.Add(_currency_iso_type, _cage_trans.Value.Inputs)
          _currencies_out.Add(_currency_iso_type, _cage_trans.Value.Outputs)
          _currencies_diff.Add(_currency_iso_type, _cage_trans.Value.Difference)
        End If
      End If

    Next

    ' Entradas
    _currency_iso_type = New CurrencyIsoType()
    _currency_iso_type.IsoCode = _national_currency
    _currency_iso_type.Type = CageCurrencyType.Bill

    _dr_cage_session_detail = OutDataTable.NewRow()
    _dr_cage_session_detail(_col1_name) = DBNull.Value
    _dr_cage_session_detail(_col2_name) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5548) '"Entradas"
    _dr_cage_session_detail(_col4_name) = _national_currency
    _dr_cage_session_detail(_col5_name) = CType(_currencies_in(_currency_iso_type), Currency).ToString(False)
    _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.BOLD

    OutDataTable.Rows.Add(_dr_cage_session_detail)

    For Each _cage_summary As KeyValuePair(Of CurrencyIsoType, Decimal) In _currencies_in
      _currency_iso_type = New CurrencyIsoType()
      _currency_iso_type.IsoCode = _national_currency
      If _cage_summary.Key.IsoCode <> _currency_iso_type.IsoCode Or _cage_summary.Key.Type <> _currency_iso_type.Type Then
        _dr_cage_session_detail = OutDataTable.NewRow()
        _dr_cage_session_detail(_col1_name) = DBNull.Value
        If _cage_summary.Key.IsoCode = Cage.CHIPS_ISO_CODE Then
          _dr_cage_session_detail(_col4_name) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2941)
        ElseIf FeatureChips.IsCageCurrencyTypeChips(_cage_summary.Key.Type) Then
          _dr_cage_session_detail(_col4_name) = FeatureChips.GetChipTypeDescription(_cage_summary.Key)
        Else
          _dr_cage_session_detail(_col4_name) = _cage_summary.Key.IsoCode
        End If
        _dr_cage_session_detail(_col5_name) = CType(_cage_summary.Value, Currency).ToString(False)
        _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.DEFAULT

        OutDataTable.Rows.Add(_dr_cage_session_detail)
      End If
    Next

    ' Salidas
    _currency_iso_type.IsoCode = _national_currency
    _currency_iso_type.Type = CageCurrencyType.Bill

    _dr_cage_session_detail = OutDataTable.NewRow()
    _dr_cage_session_detail(_col1_name) = DBNull.Value
    _dr_cage_session_detail(_col2_name) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5549) '"Salidas"
    _dr_cage_session_detail(_col4_name) = _national_currency
    _dr_cage_session_detail(_col5_name) = CType(_currencies_out(_currency_iso_type), Currency).ToString(False)
    _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.BOLD

    OutDataTable.Rows.Add(_dr_cage_session_detail)

    For Each _cage_summary As KeyValuePair(Of CurrencyIsoType, Decimal) In _currencies_out
      _currency_iso_type = New CurrencyIsoType()
      _currency_iso_type.IsoCode = _national_currency
      If _cage_summary.Key.IsoCode <> _currency_iso_type.IsoCode Or _cage_summary.Key.Type <> _currency_iso_type.Type Then
        _dr_cage_session_detail = OutDataTable.NewRow()
        _dr_cage_session_detail(_col1_name) = DBNull.Value
        If _cage_summary.Key.IsoCode = Cage.CHIPS_ISO_CODE Then
          _dr_cage_session_detail(_col4_name) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2941)
        ElseIf FeatureChips.IsCageCurrencyTypeChips(_cage_summary.Key.Type) Then
          _dr_cage_session_detail(_col4_name) = FeatureChips.GetChipTypeDescription(_cage_summary.Key)
        Else
          _dr_cage_session_detail(_col4_name) = _cage_summary.Key.IsoCode
        End If
        _dr_cage_session_detail(_col5_name) = CType(_cage_summary.Value, Currency).ToString(False)
        _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.DEFAULT

        OutDataTable.Rows.Add(_dr_cage_session_detail)
      End If
    Next

    ' Blank Line
    _dr_cage_session_detail = OutDataTable.NewRow()
    _dr_cage_session_detail(_col1_name) = DBNull.Value
    _dr_cage_session_detail(_col2_name) = DBNull.Value
    _dr_cage_session_detail(_col3_name) = DBNull.Value
    _dr_cage_session_detail(_col4_name) = DBNull.Value
    _dr_cage_session_detail(_col5_name) = DBNull.Value
    _dr_cage_session_detail(_col6_name) = DBNull.Value
    _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.DEFAULT

    OutDataTable.Rows.Add(_dr_cage_session_detail)

    ' (Entradas - Salidas)
    _currency_iso_type.IsoCode = _national_currency
    _currency_iso_type.Type = CageCurrencyType.Bill

    _dr_cage_session_detail = OutDataTable.NewRow()
    _dr_cage_session_detail(_col1_name) = DBNull.Value
    _dr_cage_session_detail(_col2_name) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5550) '"(Entradas - Salidas)"
    _dr_cage_session_detail(_col4_name) = _national_currency
    _dr_cage_session_detail(_col5_name) = CType(_currencies_diff(_currency_iso_type), Currency).ToString(False)
    _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.BOLD

    OutDataTable.Rows.Add(_dr_cage_session_detail)

    For Each _cage_summary As KeyValuePair(Of CurrencyIsoType, Decimal) In _currencies_diff
      _currency_iso_type = New CurrencyIsoType()
      _currency_iso_type.IsoCode = _national_currency
      If _cage_summary.Key.IsoCode <> _currency_iso_type.IsoCode Or _cage_summary.Key.Type <> _currency_iso_type.Type Then
        _dr_cage_session_detail = OutDataTable.NewRow()
        _dr_cage_session_detail(_col1_name) = DBNull.Value
        If _cage_summary.Key.IsoCode = Cage.CHIPS_ISO_CODE Then
          _dr_cage_session_detail(_col4_name) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2941)
        ElseIf FeatureChips.IsCageCurrencyTypeChips(_cage_summary.Key.Type) Then
          _dr_cage_session_detail(_col4_name) = FeatureChips.GetChipTypeDescription(_cage_summary.Key)
        Else
          _dr_cage_session_detail(_col4_name) = _cage_summary.Key.IsoCode
        End If
        _dr_cage_session_detail(_col5_name) = CType(_cage_summary.Value, Currency).ToString(False)
        _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.BOLD

        OutDataTable.Rows.Add(_dr_cage_session_detail)
      End If
    Next

  End Sub

  Private Sub InsertCageReportLiabilities(ByVal SessionData As WSI.Common.CageSessionReport.TYPE_CAGE_GLOBAL_SESSION_REPORT _
                                        , ByRef OutDataTable As DataTable)

    Dim _dr_cage_session_detail As DataRow

    Dim _old_liability As Int64
    Dim _current_liability As Int64
    Dim _currency_iso_type As CurrencyIsoType
    Dim _currency_description As String

    Dim _col1_name As String = "TITLE"
    Dim _col2_name As String = "DESCRIPTION"
    Dim _col3_name As String = "VALUE1"
    Dim _col4_name As String = "VALUE2"
    Dim _col5_name As String = "VALUE3"
    Dim _col6_name As String = "VALUE4"
    Dim _col9_name As String = "TYPE"

    _dr_cage_session_detail = OutDataTable.NewRow()
    _dr_cage_session_detail(_col1_name) = DBNull.Value
    _dr_cage_session_detail(_col2_name) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5551) '"Provisiones"
    _dr_cage_session_detail(_col4_name) = DBNull.Value
    _dr_cage_session_detail(_col5_name) = DBNull.Value
    _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.BOLD

    OutDataTable.Rows.Add(_dr_cage_session_detail)

    _currency_iso_type = New CurrencyIsoType()

    For Each _dr As DataRow In SessionData.CageDtCustomLiabilities.Rows

      _currency_iso_type.IsoCode = _dr("LIABILITY_ISO_CODE").ToString()
      _currency_iso_type.Type = CType(_dr("LIABILITY_CAGE_CURRENCY_TYPE"), CageCurrencyType)
      _currency_description = _currency_iso_type.IsoCode
      If _currency_iso_type.IsoCode = Cage.CHIPS_ISO_CODE Then
        _currency_description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2941)
      ElseIf FeatureChips.IsCurrencyExchangeTypeChips(_currency_iso_type.Type) Then
        _currency_description = FeatureChips.GetChipTypeDescription(_currency_iso_type)
      End If

      _current_liability = CType(_dr("LIABILITY_CONCEPT_ID"), Int64)

      _dr_cage_session_detail = OutDataTable.NewRow()
      _dr_cage_session_detail(_col1_name) = DBNull.Value
      If Not _old_liability = _current_liability Then
        If _current_liability = 5 Then
          _dr_cage_session_detail(_col2_name) = WSI.Common.Misc.GetCardPlayerConceptName()  'ID = 5 is "Dep�sito de Tarjetas" (CAGE_CONCEPTS table). But we will use the value found in the general params
        Else
          _dr_cage_session_detail(_col2_name) = _dr("LIABILITY_NAME").ToString()
        End If
      End If
      _dr_cage_session_detail(_col4_name) = _currency_description
      _dr_cage_session_detail(_col5_name) = CType(CType(_dr("LIABILITY_VALUE"), Decimal), Currency).ToString(False)
      _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.DEFAULT

      OutDataTable.Rows.Add(_dr_cage_session_detail)

      _old_liability = _current_liability

    Next

  End Sub

  Private Sub InsertCageReportSummary(ByVal SessionData As WSI.Common.CageSessionReport.TYPE_CAGE_GLOBAL_SESSION_REPORT _
                                          , ByRef OutDataTable As DataTable)

    Dim _dr_cage_session_detail As DataRow
    Dim _payment_method As String

    Dim _old_iso_type_liability As CurrencyIsoType
    Dim _current_iso_type_liability As CurrencyIsoType
    Dim _current_iso_type_description As String

    Dim _total_liability As Decimal

    Dim _cage_count_total_amount As Decimal
    Dim _cage_count_amount As Decimal

    Dim _col1_name As String = "TITLE"
    Dim _col2_name As String = "DESCRIPTION"
    Dim _col3_name As String = "VALUE1"
    Dim _col4_name As String = "VALUE2"
    Dim _col5_name As String = "VALUE3"
    Dim _col6_name As String = "VALUE4"
    Dim _col9_name As String = "TYPE"

    _old_iso_type_liability = New CurrencyIsoType()
    _current_iso_type_liability = New CurrencyIsoType()
    _total_liability = 0

    For Each _dr As DataRow In SessionData.CageDtSessionSummary.Rows

      _current_iso_type_liability.IsoCode = _dr("ISO_CODE").ToString()
      _current_iso_type_liability.Type = CType(_dr("CAGE_CURRENCY_TYPE"), CageCurrencyType)

      ' Cage Count
      _cage_count_amount = 0
      _cage_count_total_amount = 0

      If SessionData.CageDtCounts.Select("ORIGIN = " & CType(_dr("ORIGIN"), Int32) & " AND ISO_CODE = '" & _current_iso_type_liability.IsoCode & "' AND CAGE_CURRENCY_TYPE = " & CInt(_current_iso_type_liability.Type) & " ").Length > 0 Then
        _cage_count_amount = CType(SessionData.CageDtCounts.Compute("SUM(DEPOSITS)", "ORIGIN = " & CType(_dr("ORIGIN"), Int32) & " AND ISO_CODE = '" & _current_iso_type_liability.IsoCode & "' AND CAGE_CURRENCY_TYPE = " & CInt(_current_iso_type_liability.Type) & " "), Decimal)
      End If

      If Not _old_iso_type_liability.Equals(_current_iso_type_liability) Then

        If Not String.IsNullOrEmpty(_old_iso_type_liability.IsoCode) Then

          _cage_count_total_amount = 0
          If SessionData.CageDtCounts.Select("ORIGIN IS NULL AND ISO_CODE = '" & _old_iso_type_liability.IsoCode & "' AND CAGE_CURRENCY_TYPE = " & CInt(_old_iso_type_liability.Type) & " ").Length > 0 Then
            _cage_count_total_amount = CType(SessionData.CageDtCounts.Compute("SUM(DEPOSITS)", "ORIGIN IS NULL AND ISO_CODE = '" & _old_iso_type_liability.IsoCode + "' AND CAGE_CURRENCY_TYPE = " & CInt(_old_iso_type_liability.Type) & " "), Decimal)
          End If

          _dr_cage_session_detail = OutDataTable.NewRow()
          _dr_cage_session_detail(_col1_name) = DBNull.Value
          _dr_cage_session_detail(_col2_name) = "   " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5557) ' "Total"
          _dr_cage_session_detail(_col4_name) = CType(_total_liability, Currency).ToString(False)
          _dr_cage_session_detail(_col5_name) = CType(_cage_count_total_amount, Currency).ToString(False)
          _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.BOLD

          OutDataTable.Rows.Add(_dr_cage_session_detail)

          ' Blank row
          _dr_cage_session_detail = OutDataTable.NewRow()
          _dr_cage_session_detail(_col1_name) = DBNull.Value
          _dr_cage_session_detail(_col2_name) = DBNull.Value
          _dr_cage_session_detail(_col3_name) = DBNull.Value
          _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.DEFAULT

          OutDataTable.Rows.Add(_dr_cage_session_detail)

          _total_liability = 0

        End If

        If _current_iso_type_liability.IsoCode = Cage.CHIPS_ISO_CODE Then
          _current_iso_type_description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2941)
        ElseIf FeatureChips.IsCurrencyExchangeTypeChips(_current_iso_type_liability.Type) Then
          _current_iso_type_description = FeatureChips.GetChipTypeDescription(_current_iso_type_liability)
        Else
          _current_iso_type_description = _current_iso_type_liability.IsoCode
        End If

        _dr_cage_session_detail = OutDataTable.NewRow()
        _dr_cage_session_detail(_col1_name) = DBNull.Value
        _dr_cage_session_detail(_col2_name) = _current_iso_type_description
        _dr_cage_session_detail(_col4_name) = DBNull.Value
        _dr_cage_session_detail(_col5_name) = DBNull.Value
        _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.BOLD

        OutDataTable.Rows.Add(_dr_cage_session_detail)

        _total_liability = _total_liability + (CType(_dr("DEPOSITS"), Decimal) - CType(_dr("WITHDRAWALS"), Decimal))

      Else
        _total_liability = _total_liability + (CType(_dr("DEPOSITS"), Decimal) - CType(_dr("WITHDRAWALS"), Decimal))
      End If

      _dr_cage_session_detail = OutDataTable.NewRow()
      _dr_cage_session_detail(_col1_name) = DBNull.Value

      Select Case CType(_dr("ORIGIN"), Int32)

        Case -1
          _payment_method = Resource.String("STR_VOUCHER_CAGESESSIONREPORT_BANC_CARD") ' "Tarjetas Bancarias"
        Case -2
          _payment_method = Resource.String("STR_VOUCHER_CAGESESSIONREPORT_CHECK") ' "Cheques"
        Case -100
          _payment_method = Resource.String("STR_CAGE_COINS") ' "Resto"
        Case -200
          _payment_method = Resource.String("STR_FRM_TITO_HANDPAYS_TICKETS") ' "Tickets"
        Case Else
          _payment_method = Resource.String("STR_REFUND_CASH") ' "Efectivo"
          If FeatureChips.IsCurrencyExchangeTypeChips(_current_iso_type_liability.Type) Then
            _payment_method = FeatureChips.GetChipTypeDescription(_current_iso_type_liability)  ' "Fichas"
          End If

      End Select

      _dr_cage_session_detail(_col2_name) = "   " & _payment_method
      _dr_cage_session_detail(_col4_name) = CType(CType(_dr("DEPOSITS"), Decimal) - CType(_dr("WITHDRAWALS"), Decimal), Currency).ToString(False)
      _dr_cage_session_detail(_col5_name) = CType(_cage_count_amount, Currency).ToString(False)
      _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.DEFAULT

      OutDataTable.Rows.Add(_dr_cage_session_detail)

      _old_iso_type_liability.IsoCode = _dr("ISO_CODE").ToString()
      _old_iso_type_liability.Type = CType(_dr("CAGE_CURRENCY_TYPE"), CageCurrencyType)

    Next

    _cage_count_total_amount = 0
    If SessionData.CageDtCounts.Select("ORIGIN IS NULL AND ISO_CODE = '" & _current_iso_type_liability.IsoCode & "' AND CAGE_CURRENCY_TYPE = " & CInt(_current_iso_type_liability.Type) & " ").Length > 0 Then
      _cage_count_total_amount = CType(SessionData.CageDtCounts.Compute("SUM(DEPOSITS)", "ORIGIN IS NULL AND ISO_CODE = '" & _current_iso_type_liability.IsoCode & "' AND CAGE_CURRENCY_TYPE = " & CInt(_current_iso_type_liability.Type) & " "), Decimal)
    End If

    _dr_cage_session_detail = OutDataTable.NewRow()
    _dr_cage_session_detail(_col1_name) = DBNull.Value
    _dr_cage_session_detail(_col2_name) = "   " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5557) ' "Total"
    _dr_cage_session_detail(_col4_name) = CType(_total_liability, Currency).ToString(False)
    _dr_cage_session_detail(_col5_name) = CType(_cage_count_total_amount, Currency).ToString(False)
    _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.BOLD

    OutDataTable.Rows.Add(_dr_cage_session_detail)

  End Sub



  Private Sub InsertCageReportTransaction(ByVal SessionData As WSI.Common.CageSessionReport.TYPE_CAGE_GLOBAL_SESSION_REPORT _
                                        , ByVal SourceTargetId As CageSessionReport.CAGE_SOURCE_TARGETS _
                                        , ByRef OutDataTable As DataTable)

    Dim _where As String
    Dim _drs() As DataRow
    Dim _currency_iso_type As CurrencyIsoType
    Dim _currency_description As String
    Dim _source_target_name As String
    Dim _concept_description As String
    Dim _concept_id As Int64

    Dim _cage_transaction As Dictionary(Of VoucherCageSessionsReport.CageTransactionsKey, VoucherCageSessionsReport.CageTransactionsClass)

    _cage_transaction = New Dictionary(Of VoucherCageSessionsReport.CageTransactionsKey, VoucherCageSessionsReport.CageTransactionsClass) '(New VoucherCageSessionsReport.CageTransactionsKey.SortComparer())

    _currency_iso_type = New CurrencyIsoType()

    _currency_description = ""


    If SourceTargetId <> CageSessionReport.CAGE_SOURCE_TARGETS.CUSTOM Then
      _where = String.Format("CM_SOURCE_TARGET_ID IN({0}) AND (CM_VALUE_IN <> 0 OR CM_VALUE_OUT <> 0 OR CM_CONCEPT_ID = 0)", SourceTargetId.GetHashCode())
    Else
      _where = String.Format("CM_SOURCE_TARGET_ID NOT IN({0}, {1}, {2}) AND (CM_VALUE_IN <> 0 OR CM_VALUE_OUT <> 0 OR CM_CONCEPT_ID = 0) " _
                              , CageSessionReport.CAGE_SOURCE_TARGETS.CASHIERS.GetHashCode() _
                              , CageSessionReport.CAGE_SOURCE_TARGETS.GAMING_TABLES.GetHashCode() _
                              , CageSessionReport.CAGE_SOURCE_TARGETS.TERMINALS.GetHashCode())
    End If

    _drs = SessionData.CageDtTransactions.Select(_where, "ORDEN")

    For Each _dr As DataRow In _drs

      _currency_iso_type.IsoCode = _dr("CM_ISO_CODE").ToString()
      _currency_iso_type.Type = CType(_dr("CAGE_CURRENCY_TYPE"), CageCurrencyType)


      If _dr("CST_SOURCE_TARGET_NAME") Is DBNull.Value Then
        _source_target_name = String.Empty
      Else
        _source_target_name = _dr("CST_SOURCE_TARGET_NAME")
      End If

      _concept_id = CType(_dr("CM_CONCEPT_ID"), Int64)
      _concept_description = _dr("CC_DESCRIPTION").ToString()

      Select Case CType(_dr("CM_SOURCE_TARGET_ID"), CageMeters.CageSystemSourceTarget)

        Case CageMeters.CageSystemSourceTarget.Cashiers
          _source_target_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5563)   '"Cajas"
        Case CageMeters.CageSystemSourceTarget.GamingTables
          _source_target_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5564)   '"Mesas"
        Case CageMeters.CageSystemSourceTarget.Terminals
          _source_target_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5565)   '"Terminales"

      End Select

      Dim _key As VoucherCageSessionsReport.CageTransactionsKey = New VoucherCageSessionsReport.CageTransactionsKey()
      _key.IsoCode = _currency_iso_type.IsoCode
      _key.CageCurrencyType = _currency_iso_type.Type
      _key.SourceTargetName = _source_target_name
      _key.ConceptId = _concept_id
      _key.ConceptDescription = _concept_description

      If Not _cage_transaction.ContainsKey(_key) Then

        _cage_transaction.Add(_key, New VoucherCageSessionsReport.CageTransactionsClass())

      End If

      _cage_transaction(_key).Outputs = CType(_dr("CM_VALUE_OUT"), Decimal)
      _cage_transaction(_key).Inputs = CType(_dr("CM_VALUE_IN"), Decimal)

    Next

    Call Me.CreateReportRowsFromDictionary(_cage_transaction, OutDataTable)

  End Sub

  Private Sub CreateReportRowsFromDictionary(ByVal CageTransaction As Dictionary(Of VoucherCageSessionsReport.CageTransactionsKey, VoucherCageSessionsReport.CageTransactionsClass), ByRef OutDataTable As DataTable)

    Dim _dr_cage_session_detail As DataRow
    Dim _is_same_source_target As Boolean
    Dim _is_same_concept_id As Boolean
    Dim _source_target As String
    Dim _concept_description As String
    Dim _concept_id As Int64
    Dim _source_title As String
    Dim _source_title_old As String
    Dim _source_target_old As String
    Dim _concept_id_old As String
    Dim _first_time As Boolean
    Dim _total_input As Decimal
    Dim _total_output As Decimal
    Dim _national_currency As String
    Dim _show_zeros As Boolean
    Dim _currency_iso_type As CurrencyIsoType

    Dim _col1_name As String = "TITLE"
    Dim _col2_name As String = "DESCRIPTION"
    Dim _col3_name As String = "VALUE1"
    Dim _col4_name As String = "VALUE2"
    Dim _col5_name As String = "VALUE3"
    Dim _col6_name As String = "VALUE4"
    Dim _col9_name As String = "TYPE"

    _national_currency = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")
    _source_target_old = String.Empty
    _concept_id_old = 0
    _source_title = String.Empty
    _source_title_old = String.Empty
    _first_time = True
    _total_input = 0
    _total_output = 0
    _show_zeros = GeneralParam.GetBoolean("Cage", "Meters.Report.ShowItemsWithZeroAmount", True)

    _currency_iso_type = New CurrencyIsoType()
    _currency_iso_type.IsoCode = _national_currency

    For Each _cage_trans As KeyValuePair(Of VoucherCageSessionsReport.CageTransactionsKey, VoucherCageSessionsReport.CageTransactionsClass) In CageTransaction

      _is_same_source_target = False
      _is_same_concept_id = False

      _source_target = _cage_trans.Key.SourceTargetName
      _concept_description = _cage_trans.Key.ConceptDescription
      _concept_id = _cage_trans.Key.ConceptId

      _currency_iso_type.IsoCode = _cage_trans.Key.IsoCode
      _currency_iso_type.Type = FeatureChips.ConvertToCurrencyExchangeType(_cage_trans.Key.CageCurrencyType)

      If _concept_id <> 0 And Not String.IsNullOrEmpty(_concept_description) Then
        _source_title = "   " & _concept_description
      ElseIf Not String.IsNullOrEmpty(_source_target) Then
        _source_title = _source_target
      Else
        _source_title = "???"
      End If

      If _first_time Or _source_target <> _source_target_old Then

        ' Blank Line
        _dr_cage_session_detail = OutDataTable.NewRow()
        _dr_cage_session_detail(_col1_name) = DBNull.Value
        _dr_cage_session_detail(_col2_name) = DBNull.Value
        _dr_cage_session_detail(_col3_name) = DBNull.Value
        _dr_cage_session_detail(_col4_name) = DBNull.Value
        _dr_cage_session_detail(_col5_name) = DBNull.Value
        _dr_cage_session_detail(_col6_name) = DBNull.Value
        _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.DEFAULT

        OutDataTable.Rows.Add(_dr_cage_session_detail)

        _is_same_source_target = False
        _is_same_concept_id = False
      ElseIf _concept_id <> _concept_id_old Then
        _is_same_source_target = True
        _is_same_concept_id = False
      Else
        _source_title = String.Empty
        _is_same_source_target = True
        _is_same_concept_id = True
      End If

      If ((_show_zeros) Or (_cage_trans.Value.Inputs > 0 Or _cage_trans.Value.Outputs > 0) Or (_first_time) Or Not _is_same_source_target) Then
        _dr_cage_session_detail = OutDataTable.NewRow()
        _dr_cage_session_detail(_col1_name) = DBNull.Value
        _dr_cage_session_detail(_col2_name) = _source_title

        If _cage_trans.Key.IsoCode = Cage.CHIPS_ISO_CODE Then
          _dr_cage_session_detail(_col3_name) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2941)
        ElseIf FeatureChips.IsCageCurrencyTypeChips(_cage_trans.Key.CageCurrencyType) Then
          _dr_cage_session_detail(_col3_name) = FeatureChips.GetChipTypeDescription(_currency_iso_type)
        Else
          _dr_cage_session_detail(_col3_name) = _cage_trans.Key.IsoCode
        End If

        _dr_cage_session_detail(_col4_name) = _cage_trans.Value.Inputs
        _dr_cage_session_detail(_col5_name) = _cage_trans.Value.Outputs
        _dr_cage_session_detail(_col6_name) = _cage_trans.Value.Difference

        If _concept_id = 0 Then
          _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.BOLD
        Else
          _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.DEFAULT
        End If

        OutDataTable.Rows.Add(_dr_cage_session_detail)

        _total_input += _cage_trans.Value.Inputs
        _total_output += _cage_trans.Value.Outputs

      End If

      _source_target_old = _source_target
      _concept_id_old = _concept_id
      _source_title_old = _source_title
      _first_time = False

    Next

    If CageTransaction.Count > 0 Then

      ' Blank Line
      _dr_cage_session_detail = OutDataTable.NewRow()
      _dr_cage_session_detail(_col1_name) = DBNull.Value
      _dr_cage_session_detail(_col2_name) = DBNull.Value
      _dr_cage_session_detail(_col3_name) = DBNull.Value
      _dr_cage_session_detail(_col4_name) = DBNull.Value
      _dr_cage_session_detail(_col5_name) = DBNull.Value
      _dr_cage_session_detail(_col6_name) = DBNull.Value
      _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.DEFAULT

      OutDataTable.Rows.Add(_dr_cage_session_detail)

      _total_input = 0
      _total_output = 0

    End If

  End Sub

  Private Sub InsertCageStock(ByVal SessionData As WSI.Common.CageSessionReport.TYPE_CAGE_GLOBAL_SESSION_REPORT _
                                       , ByRef OutDataTable As DataTable)

    Dim _dr_cage_session_detail As DataRow
    Dim _cage_currency_type As CurrencyIsoType
    Dim _last_cage_currency_type As CurrencyIsoType
    Dim _denomination As Decimal
    Dim _quantity As Decimal
    Dim _total_currency As Decimal
    Dim _currency_type_description As String
    Dim _key As VoucherCageSessionsReport.CageSummaryKey
    Dim _name As String
    Dim _drawing As String

    m_currencies_stock = New SortedDictionary(Of VoucherCageSessionsReport.CageSummaryKey, Decimal)(New VoucherCageSessionsReport.CageSummaryKey.SortComparer())

    m_chips_stock_summary = 0

    Dim _col1_name As String = "TITLE"
    Dim _col2_name As String = "DESCRIPTION"
    Dim _col3_name As String = "VALUE1"
    Dim _col4_name As String = "VALUE2"
    Dim _col5_name As String = "VALUE3"
    Dim _col6_name As String = "VALUE4"
    Dim _col7_name As String = "VALUE5"
    Dim _col8_name As String = "VALUE6"
    Dim _col9_name As String = "TYPE"

    _cage_currency_type = New CurrencyIsoType()
    _last_cage_currency_type = New CurrencyIsoType()
    _currency_type_description = ""
    _total_currency = 0
    For Each _dr As DataRow In SessionData.CageDtStock.Rows
      _key = New VoucherCageSessionsReport.CageSummaryKey()

      _cage_currency_type.IsoCode = _dr("CGS_ISO_CODE").ToString()
      _cage_currency_type.Type = CType(_dr("CGS_CAGE_CURRENCY_TYPE"), CageCurrencyType)
      _denomination = CType(_dr("cgs_denomination"), Decimal)

      If _denomination < 0 Then
        _cage_currency_type.Type = CurrencyExchangeType.CURRENCY
      End If

      If _denomination <> Cage.TICKETS_CODE Then

        If Not _last_cage_currency_type.Equals(_cage_currency_type) Then

          If Not String.IsNullOrEmpty(_last_cage_currency_type.IsoCode) Then
            ' Total Currency
            _dr_cage_session_detail = OutDataTable.NewRow()
            _dr_cage_session_detail(_col1_name) = DBNull.Value
            _dr_cage_session_detail(_col3_name) = "Total"   '"Total"
            _dr_cage_session_detail(_col4_name) = DBNull.Value
            If _last_cage_currency_type.Type = CurrencyExchangeType.CASINO_CHIP_COLOR Then
              _dr_cage_session_detail(_col7_name) = Math.Round(_total_currency, 0).ToString()
              _dr_cage_session_detail(_col8_name) = DBNull.Value
            Else
              _dr_cage_session_detail(_col7_name) = DBNull.Value
              _dr_cage_session_detail(_col8_name) = _total_currency
            End If

            _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.BOLD_ALIGN_RIGHT

            OutDataTable.Rows.Add(_dr_cage_session_detail)

            _total_currency = 0
          End If

          ' Blank row
          _dr_cage_session_detail = OutDataTable.NewRow()
          _dr_cage_session_detail(_col1_name) = DBNull.Value
          _dr_cage_session_detail(_col2_name) = DBNull.Value
          _dr_cage_session_detail(_col3_name) = DBNull.Value
          _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.DEFAULT

          OutDataTable.Rows.Add(_dr_cage_session_detail)

          _dr_cage_session_detail = OutDataTable.NewRow()
          _dr_cage_session_detail(_col1_name) = DBNull.Value
          If _cage_currency_type.IsoCode = Cage.CHIPS_ISO_CODE Then
            _dr_cage_session_detail(_col2_name) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2941)
          ElseIf FeatureChips.IsCurrencyExchangeTypeChips(_cage_currency_type.Type) Then
            _dr_cage_session_detail(_col2_name) = FeatureChips.GetChipTypeDescription(_cage_currency_type)
          Else
            _dr_cage_session_detail(_col2_name) = _cage_currency_type.IsoCode
          End If

        Else
          _dr_cage_session_detail = OutDataTable.NewRow()
          _dr_cage_session_detail(_col1_name) = DBNull.Value
          _dr_cage_session_detail(_col2_name) = DBNull.Value
        End If

        _quantity = CType(_dr("cgs_quantity"), Decimal)
        If _cage_currency_type.IsoCode = Cage.CHIPS_ISO_CODE Then
          _currency_type_description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2941)
        ElseIf FeatureChips.IsCurrencyExchangeTypeChips(_cage_currency_type.Type) Then
          _currency_type_description = FeatureChips.GetChipTypeDescription(_cage_currency_type)
        Else
          _currency_type_description = CLASS_CAGE_CONTROL.GetCurrencyTypeDescription(CType(_dr("cgs_cage_currency_type"), CageCurrencyType))
        End If

        If _dr("CE_CURRENCY_ORDER") Is DBNull.Value Then
          _key.Order = -1
        Else
          _key.Order = CType(_dr("CE_CURRENCY_ORDER"), Int32)
        End If

        _key.IsoCode = _cage_currency_type.IsoCode
        _key.CageCurrencyType = _cage_currency_type.Type

        Select Case Decimal.ToInt32(_denomination)

          Case Cage.COINS_CODE
            _key.CageCurrencyType = CageCurrencyType.Coin
            If _key.IsoCode = m_national_currency Then
              _key.CurrencyTypeDescription = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5558)
            Else
              _key.CurrencyTypeDescription = String.Empty
            End If

            If _cage_currency_type.IsoCode <> Cage.CHIPS_ISO_CODE Then
              _total_currency = _total_currency + Math.Round(_quantity, 2)
              SetStockSummaryValues(_key, Math.Round(_quantity, 2))
              _dr_cage_session_detail(_col3_name) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2952)   '"Resto"              
              _dr_cage_session_detail(_col4_name) = DBNull.Value
              _dr_cage_session_detail(_col7_name) = DBNull.Value
              _dr_cage_session_detail(_col8_name) = Math.Round(_quantity, 2).ToString()
            End If

          Case Cage.BANK_CARD_CODE
            _key.CageCurrencyType = CageCurrencyType.Bill
            _key.CurrencyTypeDescription = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5560)   '"Tarjeta Bancaria"

            _total_currency = _total_currency + Math.Round(_quantity, 2)
            SetStockSummaryValues(_key, Math.Round(_quantity, 2))
            _dr_cage_session_detail(_col3_name) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5560)   '"Tarjeta Bancaria"
            _dr_cage_session_detail(_col4_name) = DBNull.Value
            _dr_cage_session_detail(_col7_name) = DBNull.Value
            _dr_cage_session_detail(_col8_name) = Math.Round(_quantity, 2).ToString()

          Case Cage.CHECK_CODE
            _key.CageCurrencyType = CageCurrencyType.Bill
            _key.CurrencyTypeDescription = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5561)   '"Cheque"

            _total_currency = _total_currency + Math.Round(_quantity, 2)
            SetStockSummaryValues(_key, Math.Round(_quantity, 2))
            _dr_cage_session_detail(_col3_name) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5561)   '"Cheque"
            _dr_cage_session_detail(_col4_name) = DBNull.Value
            _dr_cage_session_detail(_col7_name) = DBNull.Value
            _dr_cage_session_detail(_col8_name) = Math.Round(_quantity, 2).ToString()

          Case Else
            _key.CageCurrencyType = CageCurrencyType.Bill
            _key.CurrencyTypeDescription = ""

            If FeatureChips.IsCurrencyExchangeTypeChips(_cage_currency_type.Type) Then
              _key.CageCurrencyType = FeatureChips.ConvertToCageCurrencyType(_cage_currency_type.Type)
              _key.CurrencyTypeDescription = FeatureChips.GetChipTypeDescription(_cage_currency_type)
            End If

            If _cage_currency_type.Type = CageCurrencyType.ChipsColor Then
              _total_currency = _total_currency + _quantity
              SetStockSummaryValues(_key, _quantity)
              ' SGB 05-NOV-2014 Fixed Bug WIG-1649:Add two decimals show denominations
              _dr_cage_session_detail(_col3_name) = DBNull.Value
              _dr_cage_session_detail(_col4_name) = _currency_type_description
              _dr_cage_session_detail(_col7_name) = Math.Round(_quantity, 0).ToString()
              _dr_cage_session_detail(_col8_name) = DBNull.Value
            Else
              _total_currency = _total_currency + _denomination * _quantity
              SetStockSummaryValues(_key, _denomination * _quantity)
              ' SGB 05-NOV-2014 Fixed Bug WIG-1649:Add two decimals show denominations
              _dr_cage_session_detail(_col3_name) = Math.Round(_denomination, 2).ToString()
              _dr_cage_session_detail(_col4_name) = _currency_type_description
              _dr_cage_session_detail(_col7_name) = Math.Round(_quantity, 0).ToString()
              _dr_cage_session_detail(_col8_name) = _denomination * _quantity
            End If

        End Select

        _name = _dr("NAME").ToString()
        _drawing = _dr("DRAWING").ToString()

        _dr_cage_session_detail(_col5_name) = _name
        _dr_cage_session_detail(_col6_name) = _drawing

        _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.ALIGN_RIGHT

        OutDataTable.Rows.Add(_dr_cage_session_detail)

      End If

      _last_cage_currency_type.IsoCode = _dr("CGS_ISO_CODE").ToString()
      _last_cage_currency_type.Type = CType(_dr("CGS_CAGE_CURRENCY_TYPE"), CageCurrencyType)
      If _denomination < 0 Then
        _last_cage_currency_type.Type = CurrencyExchangeType.CURRENCY
      End If

    Next

    ' Total Currency
    _dr_cage_session_detail = OutDataTable.NewRow()
    _dr_cage_session_detail(_col1_name) = DBNull.Value
    _dr_cage_session_detail(_col3_name) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1340)   '"Total"
    _dr_cage_session_detail(_col4_name) = DBNull.Value
    If _last_cage_currency_type.Type = CurrencyExchangeType.CASINO_CHIP_COLOR Then
      _dr_cage_session_detail(_col7_name) = Math.Round(_total_currency, 0).ToString()
      _dr_cage_session_detail(_col8_name) = DBNull.Value
    Else
      _dr_cage_session_detail(_col7_name) = DBNull.Value
      _dr_cage_session_detail(_col8_name) = _total_currency
    End If

    _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.BOLD_ALIGN_RIGHT

    OutDataTable.Rows.Add(_dr_cage_session_detail)


  End Sub

  Private Sub SetStockSummaryValues(ByVal Key As VoucherCageSessionsReport.CageSummaryKey, ByVal Value As Decimal)

    If Key.IsoCode <> m_national_currency And Key.CageCurrencyType = CageCurrencyType.Coin Then
      Key.CageCurrencyType = CageCurrencyType.Bill
    End If

    If Key.IsoCode = Cage.CHIPS_ISO_CODE Then
      m_chips_stock_summary = m_chips_stock_summary + Value
    Else
      If m_currencies_stock.ContainsKey(Key) Then
        m_currencies_stock(Key) = m_currencies_stock(Key) + Value
      Else
        m_currencies_stock.Add(Key, Value)
      End If
    End If

  End Sub

  Private Sub InsertCageStockSummary(ByVal SessionData As WSI.Common.CageSessionReport.TYPE_CAGE_GLOBAL_SESSION_REPORT _
                                       , ByRef OutDataTable As DataTable)
    Dim _dr_cage_session_detail As DataRow
    Dim _national_currency As String
    Dim _last_currency As String
    Dim _total_currency As Decimal

    Dim _col1_name As String = "TITLE"
    Dim _col2_name As String = "DESCRIPTION"
    Dim _col3_name As String = "VALUE1"
    Dim _col4_name As String = "VALUE2"
    Dim _col5_name As String = "VALUE3"
    Dim _col6_name As String = "VALUE4"
    Dim _col9_name As String = "TYPE"

    _national_currency = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")
    _last_currency = String.Empty

    For Each _key As VoucherCageSessionsReport.CageSummaryKey In m_currencies_stock.Keys

      If _last_currency <> String.Empty And _last_currency <> _key.IsoCode And _last_currency = Cage.CHIPS_COLOR Then
        ' Blank row
        _dr_cage_session_detail = OutDataTable.NewRow()
        _dr_cage_session_detail(_col1_name) = DBNull.Value
        _dr_cage_session_detail(_col2_name) = DBNull.Value
        _dr_cage_session_detail(_col3_name) = DBNull.Value
        _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.DEFAULT

        OutDataTable.Rows.Add(_dr_cage_session_detail)

        ' Total
        _dr_cage_session_detail = OutDataTable.NewRow()
        _dr_cage_session_detail(_col1_name) = DBNull.Value
        _dr_cage_session_detail(_col2_name) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1340) ' "Total"
        _dr_cage_session_detail(_col4_name) = DBNull.Value
        If _last_currency = _national_currency Then
          _dr_cage_session_detail(_col5_name) = CType(_total_currency, Currency)
        ElseIf _last_currency = Cage.CHIPS_COLOR Then
          _dr_cage_session_detail(_col5_name) = CType(_total_currency, Currency) ' Int64)
        Else
          _dr_cage_session_detail(_col5_name) = CType(_total_currency, Decimal)
        End If
        _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.BOLD

        OutDataTable.Rows.Add(_dr_cage_session_detail)
      End If

      If _last_currency <> String.Empty And _last_currency <> _key.IsoCode And _last_currency <> _national_currency Then

        ' Blank row
        _dr_cage_session_detail = OutDataTable.NewRow()
        _dr_cage_session_detail(_col1_name) = DBNull.Value
        _dr_cage_session_detail(_col2_name) = DBNull.Value
        _dr_cage_session_detail(_col3_name) = DBNull.Value
        _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.DEFAULT

        OutDataTable.Rows.Add(_dr_cage_session_detail)

        _total_currency = 0
      End If

      If _last_currency <> String.Empty And _last_currency <> _key.IsoCode And _last_currency = Cage.CHIPS_COLOR Then

        _dr_cage_session_detail = OutDataTable.NewRow()
        _dr_cage_session_detail(_col1_name) = DBNull.Value
        _dr_cage_session_detail(_col2_name) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5618)  ' "Otras Divisas"
        _dr_cage_session_detail(_col3_name) = DBNull.Value
        _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.BOLD
        OutDataTable.Rows.Add(_dr_cage_session_detail)
      End If

      _dr_cage_session_detail = OutDataTable.NewRow()
      _dr_cage_session_detail(_col1_name) = DBNull.Value
      If _key.IsoCode = m_national_currency And _key.CageCurrencyType < CageCurrencyType.ChipsRedimible Then
        _dr_cage_session_detail(_col2_name) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1340) + " " + _key.CurrencyTypeDescription
      ElseIf _key.IsoCode = m_national_currency Then
        _dr_cage_session_detail(_col2_name) = _key.CurrencyTypeDescription
      Else
        _dr_cage_session_detail(_col2_name) = "  " + _key.CurrencyTypeDescription
      End If
      If _key.CageCurrencyType = CageCurrencyType.Bill And _key.CurrencyTypeDescription = String.Empty And _key.IsoCode = _national_currency Then
        _dr_cage_session_detail(_col2_name) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5612) '"Total Efectivo"
      ElseIf _key.CageCurrencyType = CageCurrencyType.Bill And _key.CurrencyTypeDescription = String.Empty Then
        _dr_cage_session_detail(_col2_name) = "  " & _key.IsoCode
      ElseIf _key.CageCurrencyType = CageCurrencyType.Coin OrElse _key.CageCurrencyType = CageCurrencyType.Others Then
        _dr_cage_session_detail(_col2_name) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1340) + " " + GLB_NLS_GUI_PLAYER_TRACKING.GetString(2952) '"Total Resto"        
      End If
      _dr_cage_session_detail(_col4_name) = DBNull.Value
      If _key.IsoCode = _national_currency Or _key.IsoCode = Cage.CHIPS_ISO_CODE Then
        _dr_cage_session_detail(_col5_name) = CType(m_currencies_stock(_key), Currency)
        _total_currency = _total_currency + m_currencies_stock(_key)
      ElseIf _key.IsoCode = Cage.CHIPS_COLOR Then
        _dr_cage_session_detail(_col5_name) = CType(m_currencies_stock(_key), Int64)
        '_total_currency = _total_currency + m_currencies_stock(_key)
      Else
        _dr_cage_session_detail(_col5_name) = CType(m_currencies_stock(_key), Decimal)
        _total_currency = _total_currency + m_currencies_stock(_key)
      End If
      _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.DEFAULT

      OutDataTable.Rows.Add(_dr_cage_session_detail)
      _last_currency = _key.IsoCode

    Next

    ' Blank row
    _dr_cage_session_detail = OutDataTable.NewRow()
    _dr_cage_session_detail(_col1_name) = DBNull.Value
    _dr_cage_session_detail(_col2_name) = DBNull.Value
    _dr_cage_session_detail(_col3_name) = DBNull.Value
    _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.DEFAULT

    OutDataTable.Rows.Add(_dr_cage_session_detail)

  End Sub

  Private Sub InsertOtherSystemConcepts(ByVal SessionData As WSI.Common.CageSessionReport.TYPE_CAGE_GLOBAL_SESSION_REPORT _
                                      , ByRef OutDataTable As DataTable)

    Dim _dr_cage_session_detail As DataRow

    Dim _old_concept As Int64
    Dim _current_concept As Int64
    Dim _currency_iso_type As CurrencyIsoType
    Dim _currency_description As String

    Dim _col1_name As String = "TITLE"
    Dim _col2_name As String = "DESCRIPTION"
    Dim _col3_name As String = "VALUE1"
    Dim _col4_name As String = "VALUE2"
    Dim _col5_name As String = "VALUE3"
    Dim _col6_name As String = "VALUE4"
    Dim _col9_name As String = "TYPE"

    _old_concept = 0

    For Each _dr As DataRow In SessionData.CageDtSessionOtherSystemConcepts.Rows
      _currency_iso_type = New CurrencyIsoType()
      _currency_iso_type.IsoCode = _dr("CM_ISO_CODE").ToString()
      _currency_iso_type.Type = CType(_dr("CAGE_CURRENCY_TYPE"), CageCurrencyType)
      _currency_description = _currency_iso_type.IsoCode
      If _currency_iso_type.IsoCode = Cage.CHIPS_ISO_CODE Then
        _currency_description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2941)
      ElseIf FeatureChips.IsCageCurrencyTypeChips(_currency_iso_type.Type) Then
        _currency_description = FeatureChips.GetChipTypeDescription(_currency_iso_type)
      End If

      _current_concept = CType(_dr("CM_CONCEPT_ID"), Int64)

      _dr_cage_session_detail = OutDataTable.NewRow()
      _dr_cage_session_detail(_col1_name) = DBNull.Value
      If Not _old_concept = _current_concept Then
        _dr_cage_session_detail(_col2_name) = _dr("CM_DESCRIPTION").ToString()
      End If
      _dr_cage_session_detail(_col4_name) = _currency_description
      _dr_cage_session_detail(_col5_name) = _dr("CM_VALUE").ToString()
      If Not _old_concept = _current_concept Then
        _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.BOLD
      Else
        _dr_cage_session_detail(_col9_name) = ExcelConversion.EnumCashierSessionDetailsType.DEFAULT
      End If
      OutDataTable.Rows.Add(_dr_cage_session_detail)

      _old_concept = _current_concept

    Next

  End Sub



  Private Sub ReportFile(ByVal OnlyPreview As Boolean)

    Select Case Me.m_open_mode
      Case ENUM_OPEN_MODE.SESSION
        Call GenerateReportFile(Me.m_cage_session_name, String.Empty, , OnlyPreview)
      Case ENUM_OPEN_MODE.MULTI_SESSION
        Call GenerateReportFile(Me.m_first_session, Me.m_last_session, , OnlyPreview)
      Case Else
        Call GenerateReportFile(String.Empty, String.Empty, , OnlyPreview)
    End Select
  End Sub

  Private Function GetDateValueString(ByVal DateValue As Date) As String

    If DateValue <> Date.MinValue And DateValue <> Date.MaxValue Then
      Return GUI_FormatDate(DateValue, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    Return NONE_DATA_STRING

  End Function

#End Region ' Private Functions

#Region "Public Functions"

  ' PURPOSE: Update the progress bar.
  '
  '    - INPUT:
  '         - Count As Int32
  '         - Max As Int32
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Public Sub UpdateProgressBar(ByVal Count As Int32, ByVal Max As Int32)

    If m_progress IsNot Nothing Then
      m_progress.SetValues(Count, Max)
    End If

  End Sub ' UpdateProgressBar

#End Region ' Public Functions

#Region "Events"

  Private Sub frm_cage_sessions_report_SizeChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.SizeChanged

    Dim _difference_x As Int32
    Dim _difference_y As Int32

    _difference_x = 1340 - Me.Size.Width
    '_difference_y = 791 - Me.Size.Height
    _difference_y = Me.Height - _
                    Me.panel_data.Location.Y - _
                    System.Windows.Forms.SystemInformation.CaptionHeight - 10


    'Me.web_browser.Size = New Size(1222 - _difference_x, 676 - _difference_y)
    Me.panel_data.Size = New Size(1222 - _difference_x, _difference_y)

  End Sub

#End Region ' Events

  Public Sub New(ByVal SessionName As String, ByVal SessionStatus As Int32)

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    m_gui_user_type = GU_USER_TYPE.NOT_ASSIGNED
    m_open_mode = ENUM_OPEN_MODE.SESSION

    Me.m_cage_session_name = SessionName
    Me.m_cage_session_status = SessionStatus

  End Sub

  Public Sub New(ByVal FirstSession As String, ByVal LastSession As String)

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    m_gui_user_type = GU_USER_TYPE.NOT_ASSIGNED
    m_open_mode = ENUM_OPEN_MODE.MULTI_SESSION

    Me.m_first_session = FirstSession
    Me.m_last_session = LastSession

  End Sub

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    m_gui_user_type = GU_USER_TYPE.NOT_ASSIGNED
    m_open_mode = ENUM_OPEN_MODE.CAGE_GLOBAL

  End Sub

End Class