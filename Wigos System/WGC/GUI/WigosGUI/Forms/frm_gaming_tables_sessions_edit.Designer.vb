<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_gaming_tables_sessions_edit
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.ef_initial_amount = New GUI_Controls.uc_entry_field
    Me.ef_client_visits = New GUI_Controls.uc_entry_field
    Me.ef_collected_amount = New GUI_Controls.uc_entry_field
    Me.cmb_gaming_table = New GUI_Controls.uc_combo
    Me.dtp_open_datetime = New GUI_Controls.uc_date_picker
    Me.dtp_close_datetime = New GUI_Controls.uc_date_picker
    Me.panel_data.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.dtp_close_datetime)
    Me.panel_data.Controls.Add(Me.dtp_open_datetime)
    Me.panel_data.Controls.Add(Me.cmb_gaming_table)
    Me.panel_data.Controls.Add(Me.ef_collected_amount)
    Me.panel_data.Controls.Add(Me.ef_client_visits)
    Me.panel_data.Controls.Add(Me.ef_initial_amount)
    Me.panel_data.Location = New System.Drawing.Point(5, 7)
    Me.panel_data.Size = New System.Drawing.Size(784, 336)
    '
    'ef_initial_amount
    '
    Me.ef_initial_amount.DoubleValue = 0
    Me.ef_initial_amount.IntegerValue = 0
    Me.ef_initial_amount.IsReadOnly = False
    Me.ef_initial_amount.Location = New System.Drawing.Point(14, 83)
    Me.ef_initial_amount.Name = "ef_initial_amount"
    Me.ef_initial_amount.Size = New System.Drawing.Size(338, 24)
    Me.ef_initial_amount.SufixText = "Sufix Text"
    Me.ef_initial_amount.SufixTextVisible = True
    Me.ef_initial_amount.TabIndex = 1
    Me.ef_initial_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_initial_amount.TextValue = ""
    Me.ef_initial_amount.Value = ""
    '
    'ef_client_visits
    '
    Me.ef_client_visits.DoubleValue = 0
    Me.ef_client_visits.IntegerValue = 0
    Me.ef_client_visits.IsReadOnly = False
    Me.ef_client_visits.Location = New System.Drawing.Point(14, 113)
    Me.ef_client_visits.Name = "ef_client_visits"
    Me.ef_client_visits.Size = New System.Drawing.Size(338, 24)
    Me.ef_client_visits.SufixText = "Sufix Text"
    Me.ef_client_visits.SufixTextVisible = True
    Me.ef_client_visits.TabIndex = 1
    Me.ef_client_visits.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_client_visits.TextValue = ""
    Me.ef_client_visits.Value = ""
    '
    'ef_collected_amount
    '
    Me.ef_collected_amount.DoubleValue = 0
    Me.ef_collected_amount.IntegerValue = 0
    Me.ef_collected_amount.IsReadOnly = False
    Me.ef_collected_amount.Location = New System.Drawing.Point(14, 143)
    Me.ef_collected_amount.Name = "ef_collected_amount"
    Me.ef_collected_amount.Size = New System.Drawing.Size(338, 24)
    Me.ef_collected_amount.SufixText = "Sufix Text"
    Me.ef_collected_amount.SufixTextVisible = True
    Me.ef_collected_amount.TabIndex = 1
    Me.ef_collected_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_collected_amount.TextValue = ""
    Me.ef_collected_amount.Value = ""
    '
    'cmb_gaming_table
    '
    Me.cmb_gaming_table.AllowUnlistedValues = False
    Me.cmb_gaming_table.AutoCompleteMode = False
    Me.cmb_gaming_table.IsReadOnly = False
    Me.cmb_gaming_table.Location = New System.Drawing.Point(390, 24)
    Me.cmb_gaming_table.Name = "cmb_gaming_table"
    Me.cmb_gaming_table.SelectedIndex = -1
    Me.cmb_gaming_table.Size = New System.Drawing.Size(320, 24)
    Me.cmb_gaming_table.SufixText = "Sufix Text"
    Me.cmb_gaming_table.SufixTextVisible = True
    Me.cmb_gaming_table.TabIndex = 2
    Me.cmb_gaming_table.TextWidth = 90
    '
    'dtp_open_datetime
    '
    Me.dtp_open_datetime.Checked = True
    Me.dtp_open_datetime.IsReadOnly = False
    Me.dtp_open_datetime.Location = New System.Drawing.Point(67, 23)
    Me.dtp_open_datetime.Name = "dtp_open_datetime"
    Me.dtp_open_datetime.ShowCheckBox = False
    Me.dtp_open_datetime.ShowUpDown = False
    Me.dtp_open_datetime.Size = New System.Drawing.Size(285, 24)
    Me.dtp_open_datetime.SufixText = "Sufix Text"
    Me.dtp_open_datetime.SufixTextVisible = True
    Me.dtp_open_datetime.TabIndex = 3
    Me.dtp_open_datetime.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_close_datetime
    '
    Me.dtp_close_datetime.Checked = True
    Me.dtp_close_datetime.IsReadOnly = False
    Me.dtp_close_datetime.Location = New System.Drawing.Point(67, 53)
    Me.dtp_close_datetime.Name = "dtp_close_datetime"
    Me.dtp_close_datetime.ShowCheckBox = False
    Me.dtp_close_datetime.ShowUpDown = False
    Me.dtp_close_datetime.Size = New System.Drawing.Size(285, 24)
    Me.dtp_close_datetime.SufixText = "Sufix Text"
    Me.dtp_close_datetime.SufixTextVisible = True
    Me.dtp_close_datetime.TabIndex = 3
    Me.dtp_close_datetime.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'frm_gaming_tables_sessions_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(903, 347)
    Me.Name = "frm_gaming_tables_sessions_edit"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_gaming_tables_sessions_edit"
    Me.panel_data.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_collected_amount As GUI_Controls.uc_entry_field
  Friend WithEvents ef_client_visits As GUI_Controls.uc_entry_field
  Friend WithEvents ef_initial_amount As GUI_Controls.uc_entry_field
  Friend WithEvents cmb_gaming_table As GUI_Controls.uc_combo
  Friend WithEvents dtp_close_datetime As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_open_datetime As GUI_Controls.uc_date_picker
End Class
