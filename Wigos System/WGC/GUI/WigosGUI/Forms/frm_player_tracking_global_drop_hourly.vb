﻿'-------------------------------------------------------------------
' Copyright © 2007-2009 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_player_tracking_global_drop_hourly
' DESCRIPTION:   
' AUTHOR:        David Hernández
' CREATION DATE: 24-APR-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 24-APR-2017  DHA    Initial version
' 05-MAY-2017  DHA    PBI 27175:Drop gaming table information - Global drop hourly report (Filter)
' 08-AUG-2017  RAB    Bug 29234:WIGOS-4269 Tables - Hourly drop report: The currency symbol is shown only on "Total" gaming table line
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports WSI.Common
Imports System.Data.SqlClient
Imports System.Text

Public Class frm_player_tracking_global_drop_hourly
  Inherits frm_base_sel


#Region " Constants "

  ' Grid Columns
  Private Const GRID_COLUMN_SENTINEL As Integer = 0
  Private Const GRID_COLUMN_TABLE_DATE_RANGE As Integer = 1
  Private Const GRID_COLUMN_TABLE_SESSION_ID As Integer = 2
  Private Const GRID_COLUMN_TABLE_SESSION_NAME As Integer = 3
  Private Const GRID_COLUMN_TABLE_NAME As Integer = 4
  Private Const GRID_COLUMN_TABLE_ISO_CODE As Integer = 5
  Private Const GRID_COLUMN_TABLE_DROP_AMOUNT As Integer = 6

  ' Grid Columns Width
  Private Const GRID_COLUMN_SENTINEL_WIDTH As Integer = 200
  Private Const GRID_COLUMN_TABLE_SESSION_ID_WIDTH As Integer = 0
  Private Const GRID_COLUMN_TABLE_DATE_RANGE_WIDTH As Integer = 1800
  Private Const GRID_COLUMN_TABLE_SESSION_NAME_WIDTH As Integer = 2500
  Private Const GRID_COLUMN_TABLE_NAME_WIDTH As Integer = 1800
  Private Const GRID_COLUMN_TABLE_ISO_CODE_WIDTH As Integer = 1000
  Private Const GRID_COLUMN_TABLE_DROP_AMOUNT_WIDTH As Integer = 2200

  ' Grid setup
  Private Const GRID_HEADER_ROWS As Integer = 1
  Private Const GRID_COLUMNS As Integer = 7

  ' Sql Columns
  Private Const SQL_COLUMN_GAME_TABLE_SESSION_ID As Integer = 0
  Private Const SQL_COLUMN_GAME_TABLE_DATETIME_RANGE As Integer = 1
  Private Const SQL_COLUMN_GAME_TABLE_ISO_CODE As Integer = 2
  Private Const SQL_COLUMN_GAME_TABLE_DROP_AMOUNT As Integer = 3
  Private Const SQL_COLUMN_GAME_TABLE_SESSION_NAME As Integer = 4
  Private Const SQL_COLUMN_GAME_TABLE_NAME As Integer = 5

#End Region ' Constants

#Region "Structures"

  Private Structure GamingTablesGroup
    Public Id As Int32
    Public Type As String
    Public Enabled As Boolean
    Public GamingTables As List(Of GamingTable)
  End Structure

  Private Structure GamingTable
    Public Id As Int32
    Public Name As String
  End Structure

#End Region

#Region "Enums"

  ' Report activity
  Private Enum GAMING_TABLES_REPORT_ACTIVITY
    GT_REPORT_ACTIVITY_ALL = 0
    GT_REPORT_ACTIVITY_ONLY = 1
  End Enum

#End Region ' Enums

#Region " Members "

  Private m_all_gaming_tables As List(Of GamingTablesGroup)

  ' Total row
  Private m_total_drop_amount As Double

  ' SubTotal row
  Private m_subtotal_session_name As String
  Private m_subtotal_drop_amount As Double

  Private m_previous_session_id As Int64

  ' Filter
  Private m_from_date As DateTime?
  Private m_to_date As DateTime?
  Private m_report_only_activity As GAMING_TABLES_REPORT_ACTIVITY
  Private m_gaming_tables As String
  Private m_selected_currency As String

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_GAMING_TABLES_SEL

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.m_all_gaming_tables = LoadGamingTablesByType()

    ' -- Form: Gaming Tables-- 
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8180)

    ' Date
    Me.gb_date.Text = GLB_NLS_GUI_INVOICING.GetString(201)
    Me.dtp_from.Text = GLB_NLS_GUI_AUDITOR.GetString(257)
    Me.dtp_to.Text = GLB_NLS_GUI_AUDITOR.GetString(258)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HOURS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HOURS)

    chb_only_with_activity.Text = GLB_NLS_GUI_INVOICING.GetString(464)

    ' Groups
    uc_checked_list_groups.GroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3416)
    uc_checked_list_groups.btn_check_all.Width = 115
    uc_checked_list_groups.btn_uncheck_all.Width = 115

    uc_multicurrency.GroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7382)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2) ' Exit

    ' Buttons
    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()

    SetDefaultValues()

  End Sub ' GUI_FilterReset  

  Protected Overrides Sub GUI_ExecuteQueryCustom()
    Dim _sql_cmd As SqlCommand
    Dim _table As DataTable
    Dim _row As DataRow

    Call GUI_StyleSheet()

    m_selected_currency = uc_multicurrency.SelectedCurrency

    ' Preparing command
    _sql_cmd = New SqlCommand("GT_HourlyDrop")
    _sql_cmd.CommandType = CommandType.StoredProcedure

    ' Command parameters
    _sql_cmd.Parameters.Add("@pGamingTableSessionId", SqlDbType.BigInt).Value = DBNull.Value
    _sql_cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = IIf(Me.m_from_date Is Nothing, DBNull.Value, Me.m_from_date)
    _sql_cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = IIf(Me.m_to_date Is Nothing, DBNull.Value, Me.m_to_date)
    _sql_cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).Value = m_selected_currency
    _sql_cmd.Parameters.Add("@pSaleChipsMov", SqlDbType.Int).Value = PlayerTrackingMovementType.SaleChips
    _sql_cmd.Parameters.Add("@pValCopyDealerMov", SqlDbType.Int).Value = PlayerTrackingMovementType.DealerCopyValidate
    _sql_cmd.Parameters.Add("@pRedeemableChipsType", SqlDbType.Int).Value = FeatureChips.ChipType.RE
    _sql_cmd.Parameters.Add("@pGamingTablesId", SqlDbType.NVarChar).Value = Me.uc_checked_list_groups.SelectedIndexesListLevel2

    _table = GUI_GetTableUsingCommand(_sql_cmd, Integer.MaxValue)

    Call GUI_BeforeFirstRow()

    Dim db_row As CLASS_DB_ROW
    Dim idx_row As Integer
    Dim count As Integer

    If _table.Rows.Count > 0 Then

      For Each _row In _table.Rows

        Try
          db_row = New CLASS_DB_ROW(_row)

          If GUI_CheckOutRowBeforeAdd(db_row) Then
            ' Add the db row to the grid
            Me.Grid.AddRow()
            count = count + 1
            idx_row = Me.Grid.NumRows - 1

            If Not GUI_SetupRow(idx_row, db_row) Then
              ' The row can not be added
              Me.Grid.DeleteRowFast(idx_row)
              count = count - 1
            End If
          End If

        Catch ex As OutOfMemoryException
          Throw ex

        Catch exception As Exception
          Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
          Call Trace.WriteLine(exception.ToString())
          Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                                "frm_player_tracking_global_drop_hourly", _
                                "GUI_SetupRow", _
                                exception.Message)
          Exit For
        End Try

        db_row = Nothing

        If Not GUI_DoEvents(Me.Grid) Then
          Exit Sub
        End If

      Next

    End If

    Call GUI_AfterLastRow()

  End Sub

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_BeforeFirstRow()

    m_previous_session_id = 0

    m_total_drop_amount = 0

    m_subtotal_drop_amount = 0


  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Overrides Sub GUI_AfterLastRow()

    ' Add sub total row
    AddSubTotalRow()

    Me.Grid.AddRow()

    Dim _idx_row As Integer = Me.Grid.NumRows - 1

    ' Label - TOTAL:
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TABLE_DATE_RANGE).Value = GLB_NLS_GUI_INVOICING.GetString(205)

    ' Iso Code
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TABLE_ISO_CODE).Value = m_selected_currency

    ' Drop Amount
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TABLE_DROP_AMOUNT).Value = GUI_FormatCurrency(m_total_drop_amount, _
                                                                                ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)

    Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)


  End Sub ' GUI_AfterLastRow

  ' PURPOSE : Checks out the DB row before adding it to the grid
  '              and process counters (& totals rows)
  '
  '  PARAMS :
  '     - INPUT :
  '           - DataRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the data row can be added) or False (the data row can not be added)
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    If m_previous_session_id = 0 Then
      m_previous_session_id = DbRow.Value(SQL_COLUMN_GAME_TABLE_SESSION_ID)
      m_subtotal_session_name = DbRow.Value(SQL_COLUMN_GAME_TABLE_SESSION_NAME)
    End If
    If DbRow.Value(SQL_COLUMN_GAME_TABLE_SESSION_ID) <> m_previous_session_id And m_previous_session_id <> 0 Then

      AddSubTotalRow()

      m_previous_session_id = DbRow.Value(SQL_COLUMN_GAME_TABLE_SESSION_ID)
      m_subtotal_session_name = DbRow.Value(SQL_COLUMN_GAME_TABLE_SESSION_NAME)
    End If

    If DbRow.Value(SQL_COLUMN_GAME_TABLE_ISO_CODE) <> m_selected_currency Then
      Return False
    End If

    If m_report_only_activity = GAMING_TABLES_REPORT_ACTIVITY.GT_REPORT_ACTIVITY_ONLY AndAlso _
      DbRow.Value(SQL_COLUMN_GAME_TABLE_DROP_AMOUNT) = 0 Then
      Return False
    End If

    Return True

  End Function

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Me.dtp_to.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck 

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  '
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _gt_session_id As Int64
    Dim _gt_session_name As String
    Dim _gt_table_name As String
    Dim _gt_date_range As DateTime
    Dim _gt_drop_iso_code As String
    Dim _gt_drop_amount As Decimal

    ' Gaming table session id
    _gt_session_id = DbRow.Value(SQL_COLUMN_GAME_TABLE_SESSION_ID)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TABLE_SESSION_ID).Value = _gt_session_id

    ' Date range
    _gt_date_range = DbRow.Value(SQL_COLUMN_GAME_TABLE_DATETIME_RANGE)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TABLE_DATE_RANGE).Value = GUI_FormatDate(_gt_date_range, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    ' Gaming table session name
    _gt_session_name = DbRow.Value(SQL_COLUMN_GAME_TABLE_SESSION_NAME)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TABLE_SESSION_NAME).Value = _gt_session_name

    ' Gaming table  name
    _gt_table_name = DbRow.Value(SQL_COLUMN_GAME_TABLE_NAME)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TABLE_NAME).Value = _gt_table_name

    ' Drop Iso Code
    _gt_drop_iso_code = DbRow.Value(SQL_COLUMN_GAME_TABLE_ISO_CODE)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TABLE_ISO_CODE).Value = _gt_drop_iso_code

    ' Drop Amount
    _gt_drop_amount = DbRow.Value(SQL_COLUMN_GAME_TABLE_DROP_AMOUNT)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TABLE_DROP_AMOUNT).Value = GUI_FormatCurrency(_gt_drop_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)

    m_total_drop_amount += _gt_drop_amount

    m_subtotal_drop_amount += _gt_drop_amount

    Return True

  End Function ' GUI_SetupRow


  Protected Overrides Sub Finalize()
    MyBase.Finalize()
  End Sub ' Finalize

#End Region ' OVERRIDES 

#Region " GUI Reports "

  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    With Me.Grid

    End With

    Call MyBase.GUI_ReportParams(ExcelData)

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    'Dates
    If Me.m_from_date Is Nothing Then
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), String.Empty)
    Else
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), GUI_FormatDate(Me.m_from_date, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS))
    End If

    If Me.m_to_date Is Nothing Then
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), String.Empty)
    Else
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), GUI_FormatDate(Me.m_to_date, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS))
    End If

    ' Activity
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(464), IIf(m_report_only_activity, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)))

    ' Gaming Tables
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4973), Me.m_gaming_tables)

    ' Currency type
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7382), m_selected_currency)


  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportUpdateFilters()

    ' Date
    If Me.dtp_from.Checked Then
      Me.m_from_date = dtp_from.Value
    Else
      Me.m_from_date = Nothing
    End If

    If Me.dtp_to.Checked Then
      Me.m_to_date = Me.dtp_to.Value
    Else
      Me.m_to_date = Nothing
    End If

    ' Only Activity
    m_report_only_activity = GAMING_TABLES_REPORT_ACTIVITY.GT_REPORT_ACTIVITY_ALL
    If chb_only_with_activity.Checked Then
      m_report_only_activity = GAMING_TABLES_REPORT_ACTIVITY.GT_REPORT_ACTIVITY_ONLY
    End If

    ' Gaming Tables
    If uc_checked_list_groups.SelectedIndexes.Length = uc_checked_list_groups.Count() Then
      m_gaming_tables = GLB_NLS_GUI_PLAYER_TRACKING.GetString(287) ' All
    ElseIf uc_checked_list_groups.SelectedIndexes.Length > 1 Then
      m_gaming_tables = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1808) ' Several
    ElseIf uc_checked_list_groups.SelectedIndexes.Length = 1 Then
      m_gaming_tables = Me.uc_checked_list_groups.SelectedValuesList ' Only one
    End If

  End Sub ' GUI_ReportUpdateFilters

  ' PURPOSE: Set the query method to use
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '     - ENUM_QUERY_TYPE
  Protected Overrides Function GUI_GetQueryType() As GUI_Controls.frm_base_sel.ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM
  End Function

#End Region ' GUI Reports


#Region " Private Functions"


  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub GUI_StyleSheet()

    ' Grid StyleSheet
    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, False)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
      .Sortable = True

      ' SENTINEL
      .Column(GRID_COLUMN_SENTINEL).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_SENTINEL).Width = GRID_COLUMN_SENTINEL_WIDTH
      .Column(GRID_COLUMN_SENTINEL).HighLightWhenSelected = False
      .Column(GRID_COLUMN_SENTINEL).IsColumnPrintable = False

      ' GAME TABLE SESSION ID
      GUI_AddColumn(Me.Grid, GRID_COLUMN_TABLE_SESSION_ID, String.Empty, 0, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT, False)

      ' DATE
      GUI_AddColumn(Me.Grid, GRID_COLUMN_TABLE_DATE_RANGE, GLB_NLS_GUI_INVOICING.GetString(201), GRID_COLUMN_TABLE_DATE_RANGE_WIDTH, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER)

      ' SESSION NAME
      GUI_AddColumn(Me.Grid, GRID_COLUMN_TABLE_SESSION_NAME, GLB_NLS_GUI_INVOICING.GetString(208), GRID_COLUMN_TABLE_SESSION_NAME_WIDTH, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT)

      ' NAME TABLE
      GUI_AddColumn(Me.Grid, GRID_COLUMN_TABLE_NAME, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3410), GRID_COLUMN_TABLE_SESSION_NAME_WIDTH, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT)

      ' ISO CODE
      GUI_AddColumn(Me.Grid, GRID_COLUMN_TABLE_ISO_CODE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5554), GRID_COLUMN_TABLE_ISO_CODE_WIDTH, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER)

      ' DROP AMOUNT
      GUI_AddColumn(Me.Grid, GRID_COLUMN_TABLE_DROP_AMOUNT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3411), GRID_COLUMN_TABLE_DROP_AMOUNT_WIDTH, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, )

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub SetDefaultValues()

    Dim _closing_time As Integer
    Dim _initial_time As Date
    Dim _now As DateTime

    _now = WGDB.Now

    _closing_time = GetDefaultClosingTime()
    _initial_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)
    If _initial_time > _now Then
      _initial_time = _initial_time.AddDays(-1)
    End If

    Me.dtp_from.Value = _initial_time
    Me.dtp_from.Checked = True
    Me.dtp_to.Value = _initial_time.AddDays(1)
    Me.dtp_to.Checked = True

    Me.chb_only_with_activity.Checked = False

    uc_checked_list_groups.Clear()
    FillGroups()
    Me.uc_checked_list_groups.SetDefaultValue(True)
    Me.uc_checked_list_groups.CollapseAll()

    uc_multicurrency.OpenModeControl = uc_multi_currency_site_sel.OPEN_MODE.OnlyCurrency
    Me.uc_multicurrency.Init()

  End Sub ' SetDefaultValues

  Protected Sub AddSubTotalRow()

    If m_previous_session_id <> 0 And (m_subtotal_drop_amount > 0 And chb_only_with_activity.Checked Or _
      Not chb_only_with_activity.Checked) Then

      Me.Grid.AddRow()

      Dim _idx_row As Integer = Me.Grid.NumRows - 1


      Me.Grid.Cell(_idx_row, GRID_COLUMN_TABLE_SESSION_NAME).Value = m_subtotal_session_name

      ' Label - TOTAL:
      Me.Grid.Cell(_idx_row, GRID_COLUMN_TABLE_NAME).Value = GLB_NLS_GUI_INVOICING.GetString(205)

      ' Iso Code
      Me.Grid.Cell(_idx_row, GRID_COLUMN_TABLE_ISO_CODE).Value = m_selected_currency

      ' Drop Amount
      Me.Grid.Cell(_idx_row, GRID_COLUMN_TABLE_DROP_AMOUNT).Value = GUI_FormatCurrency(m_subtotal_drop_amount, _
                                                                                  ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)

      Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

    End If

    m_subtotal_drop_amount = 0


  End Sub ' GUI_AfterLastRow

  ' PURPOSE: Fill uc_checked_list control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS
  '
  Private Sub FillGroups()

    ' Fill CheckedList control with groups
    Me.uc_checked_list_groups.Clear()
    Me.uc_checked_list_groups.ReDraw(False)

    For Each _group As GamingTablesGroup In Me.m_all_gaming_tables
      uc_checked_list_groups.Add(1, _group.Id, _group.Type)
      For Each _gaming_table As GamingTable In _group.GamingTables
        uc_checked_list_groups.Add(2, _gaming_table.Id, "  " + _gaming_table.Name)
      Next
    Next

    If Me.uc_checked_list_groups.Count > 0 Then
      Me.uc_checked_list_groups.ColumnWidth(3, 4450)
      Me.uc_checked_list_groups.CurrentRow(0)
      Me.uc_checked_list_groups.ReDraw(True)
    End If

  End Sub ' FillGroups

  ' PURPOSE: Get all gaming tables
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: Gaming tables list, order by table type.
  '
  Private Function LoadGamingTablesByType() As List(Of GamingTablesGroup)
    Dim _tables_groups As List(Of GamingTablesGroup)
    Dim _str_sql As StringBuilder
    Dim _dt_counters As DataTable
    Dim _group_id As Integer
    Dim _new_gaming_table As GamingTable
    Dim _new_tables_group As GamingTablesGroup

    _tables_groups = New List(Of GamingTablesGroup)
    _new_tables_group = New GamingTablesGroup()
    _str_sql = New StringBuilder()
    _dt_counters = New DataTable()
    _group_id = 0

    Try

      Using _db_trx As DB_TRX = New DB_TRX()

        _str_sql.AppendLine("     SELECT   GTT_GAMING_TABLE_TYPE_ID              ")
        _str_sql.AppendLine("            , GTT_NAME                              ")
        _str_sql.AppendLine("            , GTT_ENABLED                           ")
        _str_sql.AppendLine("            , GT_GAMING_TABLE_ID                    ")
        _str_sql.AppendLine("            , GT_NAME                               ")
        _str_sql.AppendLine("       FROM   GAMING_TABLES_TYPES                   ")
        _str_sql.AppendLine(" INNER JOIN   GAMING_TABLES                         ")
        _str_sql.AppendLine("         ON   GTT_GAMING_TABLE_TYPE_ID = GT_TYPE_ID ")

        Using _sql_cmd As SqlCommand = New SqlCommand(_str_sql.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          Using _sql_adapter As SqlDataAdapter = New SqlDataAdapter(_sql_cmd)

            If _sql_adapter.Fill(_dt_counters) > 0 Then

              For Each _row As DataRow In _dt_counters.Rows

                If _group_id <> CInt(_row(0)) Then
                  _group_id = _row(0) ' GroupId
                  _new_tables_group = New GamingTablesGroup()
                  _new_tables_group.GamingTables = New List(Of GamingTable)
                  _new_tables_group.Id = _group_id
                  _new_tables_group.Type = _row(1)
                  _new_tables_group.Enabled = _row(2)
                  _tables_groups.Add(_new_tables_group)
                End If

                _new_gaming_table = New GamingTable()
                _new_gaming_table.Id = _row(3)
                _new_gaming_table.Name = _row(4)
                _tables_groups(_tables_groups.Count - 1).GamingTables.Add(_new_gaming_table)

              Next
            End If
          End Using
        End Using
      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return _tables_groups
  End Function ' LoadGamingTablesByType


#End Region ' Private 

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit


#End Region 'Public Functions

#Region " Events "

#End Region ' Events

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

  End Sub

End Class