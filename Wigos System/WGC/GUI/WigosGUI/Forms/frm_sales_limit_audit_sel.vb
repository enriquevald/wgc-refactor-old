'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_sales_limit-audit_sel.vb
' DESCRIPTION:   Interface for filter sales limit audit information
' AUTHOR:        Rub�n Rodr�guez
' CREATION DATE: 25-06-2013
'
' REVISION HISTORY:
'
' Date          Author Description
' -----------  ------- ----------------------------------------------
' 25-06-2013   RRR    Initial version
'--------------------------------------------------------------------
Option Strict Off
Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common
Imports System.Data.SqlClient

Public Class frm_sales_limit_audit_sel
  Inherits frm_base_sel

#Region " Constants "

  ' DB Columns
  Private Const SQL_COLUMN_OLD_SALES_LIMIT As Integer = 1
  Private Const SQL_COLUMN_OLD_MB_SALES_LIMIT As Integer = 2
  Private Const SQL_COLUMN_NEW_SALES_LIMIT As Integer = 3
  Private Const SQL_COLUMN_NEW_MB_SALES_LIMIT As Integer = 4
  Private Const SQL_COLUMN_USER_NAME As Integer = 5
  Private Const SQL_COLUMN_DATETIME As Integer = 6

  ' Grid Columns
  Private Const GRID_COL_DATE As Integer = 0
  Private Const GRID_COL_USER_NAME As Integer = 1
  Private Const GRID_COL_CASHIER_OLD As Integer = 2
  Private Const GRID_COL_CASHIER_NEW As Integer = 3
  Private Const GRID_COL_MB_OLD As Integer = 4
  Private Const GRID_COL_MB_NEW As Integer = 5

  Private Const GRID_NUM_COLUMNS As Integer = 6
  Private Const GRID_HEADER_ROWS As Integer = 2

#End Region ' Constants

#Region " Members "

  Private FunctionName As String

  ' For report filters 
  Private m_user As String
  Private m_date_from As String
  Private m_date_to As String
  Private m_terminal_type As String

#End Region ' Members

#Region " Overrides Functions "

  ' PURPOSE : Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_SALES_LIMIT_AUDIT_SEL

    Call MyBase.GUI_SetFormId()
  End Sub

  ' PURPOSE : Initialize every form control
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_InitControls()
    '-------------------------------------------
    ' WARNING: call this procedure first!!
    Call MyBase.GUI_InitControls()
    '-------------------------------------------

    ' Add user names to combo
    Call AddUserTableToDataset()

    ' Initialize default values un the form
    dt_date_from.ShowCheckBox = True
    dt_date_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_SHORT)
    dt_date_to.ShowCheckBox = True
    dt_date_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_TIME_SHORT)

    ' Set NLS Code
    Me.Text = GLB_NLS_GUI_AUDITOR.GetString(474)

    fra_user.Text = GLB_NLS_GUI_AUDITOR.GetString(254)
    opt_user_one.Text = GLB_NLS_GUI_AUDITOR.GetString(262)
    opt_user_all.Text = GLB_NLS_GUI_AUDITOR.GetString(263)
    fra_date.Text = GLB_NLS_GUI_AUDITOR.GetString(256)
    dt_date_from.Text = GLB_NLS_GUI_AUDITOR.GetString(257)
    dt_date_to.Text = GLB_NLS_GUI_AUDITOR.GetString(258)
    chk_show_all.Text = GLB_NLS_GUI_CONFIGURATION.GetString(90)
    fra_terminal_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2115)
    chk_cashier.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(491)
    chk_mobile_bank.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1182)

    GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_AUDITOR.GetString(12)

    GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_FILTER_APPLY).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_FILTER_RESET).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_INFO).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False

    ' Set default values
    Call SetDefaultValues()

    ' Fill the grid without filter (only header row)
    Call GUI_StyleView()

  End Sub

  ' Reset filter to the initial to the defalut values
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub

  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE

    Return ENUM_QUERY_TYPE.QUERY_COMMAND

  End Function ' GUI_GetQueryType

  ' PURPOSE : Build an SQL query from conditions set in the filters
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - SQL query text ready to send to the database

  Protected Overrides Function GUI_GetSqlCommand() As SqlCommand
    Dim _sql_command As SqlCommand
    Dim _sb As System.Text.StringBuilder

    _sb = New System.Text.StringBuilder()

    _sb.AppendLine("SELECT   SLA_ID ")
    _sb.AppendLine("       , SLA_OLD_SALES_LIMIT ")
    _sb.AppendLine("       , SLA_OLD_MB_SALES_LIMIT ")
    _sb.AppendLine("       , SLA_NEW_SALES_LIMIT ")
    _sb.AppendLine("       , SLA_NEW_MB_SALES_LIMIT ")
    _sb.AppendLine("       , GU_USERNAME  ")
    _sb.AppendLine("       , SLA_DATETIME ")
    _sb.AppendLine(" FROM   SALES_LIMIT_AUDIT ")
    _sb.AppendLine("        INNER JOIN   GUI_USERS ON GU_USER_ID = SLA_USER_ID")

    _sb.AppendLine(GetSqlWhere())

    _sb.AppendLine(" ORDER BY   SLA_DATETIME DESC ")

    _sql_command = New SqlCommand(_sb.ToString())
    Return _sql_command

  End Function ' GUI_GetSqlCommand

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Call MyBase.GUI_SetupRow(RowIndex, DbRow)

    Dim _str_old_cashier_limit As String
    Dim _str_new_cashier_limit As String
    Dim _str_old_mb_limit As String
    Dim _str_new_mb_limit As String

    ' Date Time
    Me.Grid.Cell(RowIndex, GRID_COL_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATETIME), _
                 ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    ' User Name
    Me.Grid.Cell(RowIndex, GRID_COL_USER_NAME).Value = DbRow.Value(SQL_COLUMN_USER_NAME)

    ' Cashier and MB limits
    If DbRow.Value(SQL_COLUMN_OLD_SALES_LIMIT).ToString() <> String.Empty Then
      _str_old_cashier_limit = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_OLD_SALES_LIMIT), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Else
      _str_old_cashier_limit = "---"
    End If

    If DbRow.Value(SQL_COLUMN_OLD_MB_SALES_LIMIT).ToString() <> String.Empty Then
      _str_old_mb_limit = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_OLD_MB_SALES_LIMIT), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Else
      _str_old_mb_limit = "---"
    End If

    If DbRow.Value(SQL_COLUMN_NEW_SALES_LIMIT).ToString() <> String.Empty Then
      _str_new_cashier_limit = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_NEW_SALES_LIMIT), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Else
      _str_new_cashier_limit = "---"
    End If

    If DbRow.Value(SQL_COLUMN_NEW_MB_SALES_LIMIT).ToString() <> String.Empty Then
      _str_new_mb_limit = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_NEW_MB_SALES_LIMIT), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Else
      _str_new_mb_limit = "---"
    End If

    ' Cashier
    If _str_old_cashier_limit <> _str_new_cashier_limit Then
      Me.Grid.Cell(RowIndex, GRID_COL_CASHIER_OLD).Value = _str_old_cashier_limit
      Me.Grid.Cell(RowIndex, GRID_COL_CASHIER_NEW).Value = _str_new_cashier_limit
    End If

    ' MB
    If _str_old_mb_limit <> _str_new_mb_limit Then
      Me.Grid.Cell(RowIndex, GRID_COL_MB_OLD).Value = _str_old_mb_limit
      Me.Grid.Cell(RowIndex, GRID_COL_MB_NEW).Value = _str_new_mb_limit
    End If

    Return True

  End Function ' GUI_SetupRow

#End Region ' Overrides Functions

#Region " GUI Reports "

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(256) & " " & GLB_NLS_GUI_AUDITOR.GetString(257), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(256) & " " & GLB_NLS_GUI_AUDITOR.GetString(258), m_date_to)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(254), m_user)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2115), m_terminal_type)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)

    PrintData.FilterHeaderWidth(1) = 2000
    PrintData.FilterHeaderWidth(3) = 2000

  End Sub

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_user = ""
    m_date_from = ""
    m_date_to = ""
    m_terminal_type = ""

    ' User 
    If Me.opt_user_all.Checked Then
      m_user = Me.opt_user_all.Text
    Else
      m_user = Me.cmb_user.TextValue
    End If

    ' Activation Date 
    If Me.dt_date_from.Checked Then
      m_date_from = GUI_FormatDate(Me.dt_date_from.Value, _
                    ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                    ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_TIME_SHORT)
    End If

    If Me.dt_date_to.Checked Then
      m_date_to = GUI_FormatDate(Me.dt_date_to.Value, _
                  ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                  ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_TIME_SHORT)
    End If

    ' Terminal Type

    Dim _all_selected As Boolean

    _all_selected = False
    If Not Me.chk_cashier.Checked And Not Me.chk_mobile_bank.Checked Then
      _all_selected = True
    End If

    If Me.chk_cashier.Checked Or _all_selected = True Then
      If Not String.IsNullOrEmpty(m_terminal_type) Then
        m_terminal_type = m_terminal_type & ", "
      End If
      m_terminal_type = m_terminal_type & GLB_NLS_GUI_PLAYER_TRACKING.GetString(491)
    End If

    If Me.chk_mobile_bank.Checked Or _all_selected = True Then
      If Not String.IsNullOrEmpty(m_terminal_type) Then
        m_terminal_type = m_terminal_type & ", "
      End If
      m_terminal_type = m_terminal_type & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1182)
    End If

  End Sub


#End Region ' GUI Reports

#Region " Private Functions "

  ' PURPOSE: Fix the default values in the orm
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - none
  Private Sub SetDefaultValues()
    ' Blank selection
    cmb_user.SelectedIndex = -1
    cmb_user.TextValue = ""
    ' Init combos
    opt_user_all.Checked = True
    cmb_user.Enabled = False
    chk_show_all.Checked = False
    chk_show_all.Enabled = False

    ' Init dates
    dt_date_from.Value = GUI_GetDate()
    dt_date_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    dt_date_from.ShowCheckBox = True
    dt_date_from.Checked = True
    dt_date_to.Value = GUI_GetDate()
    dt_date_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    dt_date_to.ShowCheckBox = True
    dt_date_to.Checked = False

    ' Init checks
    chk_cashier.Checked = True
    chk_mobile_bank.Checked = True

  End Sub 'SetDafultValues

  ' PURPOSE: Initialize grid view
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - none
  Private Sub GUI_StyleView()
    'XVV Variable not use Dim datagrid_column As DataGridTextBoxColumn

    Me.Grid.Init(GRID_NUM_COLUMNS, GRID_HEADER_ROWS)

    '  User Name 
    Me.Grid.Column(GRID_COL_USER_NAME).Header(0).Text = " "
    Me.Grid.Column(GRID_COL_USER_NAME).Header(1).Text = GLB_NLS_GUI_AUDITOR.GetString(254)
    Me.Grid.Column(GRID_COL_USER_NAME).Width = 2000

    '  Date 
    Me.Grid.Column(GRID_COL_DATE).Header(0).Text = " "
    Me.Grid.Column(GRID_COL_DATE).Header(1).Text = GLB_NLS_GUI_AUDITOR.GetString(256)
    Me.Grid.Column(GRID_COL_DATE).Width = 2000
    Me.Grid.Column(GRID_COL_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    ' Cashier Old
    Me.Grid.Column(GRID_COL_CASHIER_OLD).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(491)
    Me.Grid.Column(GRID_COL_CASHIER_OLD).Header(1).Text = "Anterior"
    Me.Grid.Column(GRID_COL_CASHIER_OLD).Width = 1500
    Me.Grid.Column(GRID_COL_CASHIER_OLD).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    ' Cashier New
    Me.Grid.Column(GRID_COL_CASHIER_NEW).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(491)
    Me.Grid.Column(GRID_COL_CASHIER_NEW).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2)
    Me.Grid.Column(GRID_COL_CASHIER_NEW).Width = 1500
    Me.Grid.Column(GRID_COL_CASHIER_NEW).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    ' MB Old
    Me.Grid.Column(GRID_COL_MB_OLD).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1182)
    Me.Grid.Column(GRID_COL_MB_OLD).Header(1).Text = "Anterior"
    Me.Grid.Column(GRID_COL_MB_OLD).Width = 1500
    Me.Grid.Column(GRID_COL_MB_OLD).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    ' MB New
    Me.Grid.Column(GRID_COL_MB_NEW).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1182)
    Me.Grid.Column(GRID_COL_MB_NEW).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2)
    Me.Grid.Column(GRID_COL_MB_NEW).Width = 1500
    Me.Grid.Column(GRID_COL_MB_NEW).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    ' Not allowed Sort due to the hidden rows.
    Me.Grid.IsSortable = False

  End Sub 'GUI_StyleView()

  ' PURPOSE: Fill the user tbale wuth descendents of logged user
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Sub AddUserTableToDataset()
    Const MAIN_MODULE_NAME As String = "frm_sales_limit_audit"

    Dim string_sql As String
    'XVV Variable not use Dim ds As DataSet
    Dim dt As DataTable

    Call cmb_user.Clear()

    Try
      string_sql = " SELECT DISTINCT(GU_USER_ID) " & _
                        ", GU_USERNAME " & _
                   " FROM   GUI_USERS "

      ' XCD 16-Aug-2012 Discard unsubscribed users
      If Not chk_show_all.Checked Then
        string_sql = string_sql & " WHERE GU_BLOCK_REASON = " & WSI.Common.GUI_USER_BLOCK_REASON.NONE
      End If

      dt = GUI_GetTableUsingSQL(string_sql, MAX_RECORDS_LOAD)

      If Not IsNothing(dt) Then
        Call Fill_combo(cmb_user, dt)
        dt.Dispose()
        dt = Nothing
      End If


    Catch exception As Exception
      Call Trace.WriteLine(exception.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            MAIN_MODULE_NAME, _
                            FunctionName, _
                            exception.Message)
    End Try
  End Sub 'AddUserTableToDataset

  ' PURPOSE: Fill the combo box user control
  '
  '  PARAMS:
  '     - INPUT:
  '           - table: Tableset from memory
  '     - OUTPUT:
  '           - combo: User control
  '
  ' RETURNS:
  '     - none
  Private Sub Fill_combo(ByVal combo As uc_combo, ByVal table As DataTable)
    Dim idx As Integer
    Dim aux_id As Integer
    Dim aux_name As String
    Dim s As SortedList

    Call combo.Clear()
    s = New SortedList()

    For idx = 0 To table.Rows.Count - 1
      aux_id = CInt(table.Rows.Item(idx).Item(0))
      aux_name = CStr(table.Rows.Item(idx).Item(1))
      s.Add(aux_name, aux_id)
    Next

    For idx = 0 To s.Count - 1
      aux_id = s.GetByIndex(idx)
      aux_name = s.GetKey(idx)
      Call combo.Add(aux_id, aux_name)
    Next

  End Sub 'Fill_combo

  ' PURPOSE : Build the variable part of the WHERE clause (the one that depends on filter values) for the
  '          main SQL Query.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Function GetSqlWhere() As String

    Dim _sb_where As System.Text.StringBuilder

    _sb_where = New System.Text.StringBuilder()

    ' Gift Filters
    '   - User Id
    '   - Date From
    '   - Date To
    '   - Only Cashier Limit Changed
    '   - Only MB Limit Changed

    ' User Id
    If opt_user_one.Checked = True Then
      _sb_where.AppendLine("  AND SLA_USER_ID = " & cmb_user.Value)
    End If

    ' Date From
    If dt_date_from.Checked = True Then
      _sb_where.AppendLine("  AND SLA_DATETIME >= " & GUI_FormatDateDB(dt_date_from.Value))
    End If

    ' Date To
    If dt_date_to.Checked = True Then
      _sb_where.AppendLine("  AND SLA_DATETIME <= " & GUI_FormatDateDB(dt_date_to.Value))
    End If

    ' Only Cashier Limit Changed
    If chk_cashier.Checked = True And chk_mobile_bank.Checked = False Then
      _sb_where.AppendLine("  AND ISNULL(SLA_OLD_SALES_LIMIT, 0) != ISNULL(SLA_NEW_SALES_LIMIT, 0) ")
    End If

    ' Only MB Limit Changed
    If chk_mobile_bank.Checked = True And chk_cashier.Checked = False Then
      _sb_where.AppendLine("  AND ISNULL(SLA_OLD_MB_SALES_LIMIT, 0) != ISNULL(SLA_NEW_MB_SALES_LIMIT, 0) ")
    End If

    _sb_where = New System.Text.StringBuilder(_sb_where.ToString().TrimStart(New Char() {" ", " ", "A", "N", "D"}))

    If _sb_where.Length > 0 Then
      _sb_where.Insert(0, " WHERE ")
    End If

    Return _sb_where.ToString()
  End Function ' GetSqlWhere

#End Region ' Private Functions

#Region " Public Functions "

  ' PURPOSE : Opens dialog with default settings for edit mode
  '
  '  PARAMS :
  '     - INPUT :

  '     - OUTPUT :

  '
  ' RETURNS :


  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Events "

  Private Sub opt_user_all_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_user_all.Click
    cmb_user.Enabled = False
    chk_show_all.Enabled = False
  End Sub 'opt_user_all_Click

  Private Sub opt_user_one_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_user_one.Click
    cmb_user.Enabled = True
    chk_show_all.Enabled = True
  End Sub 'opt_user_one_Click

  Private Sub chk_show_all_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_show_all.CheckedChanged
    AddUserTableToDataset()
  End Sub 'chk_show_all_CheckedChanged

#End Region ' Events

End Class