'-------------------------------------------------------------------
' Copyright � 2011 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_gaming_tables_income_report
' DESCRIPTION:   Gaming tabels income
' AUTHOR:        David Lasdiez
' CREATION DATE: 16-DEC-2013
'
' REVISION HISTORY:
'
' Date         Author   Description
' -----------  ------   -----------------------------------------------
' 16-DEC-2013  DLL      Initial version.
' 20-DEC-2013  RMS      Required Stored Procedure and functions installed in DB
' 30-JAN-2014  RMS      Added table type subtotals in table reports
' 18-FEB-2014  RRR      Fixed Bug WIG-645: : Search error solved
' 20-FEB-2014  RMS      Added note about only gaming tables closed se
' 26-MAR-2014  DHA      Fixed Bug WIG-770: set filters properly (Date and Gaming Table Type)
' 07-OCT-2014  DHA      Fixed Bug WIG-1424: set filters properly
' 08-OCT-2014  JML      Fixed Bug WIG-1439: PlayerTracking: Report columns with wrong format
' 12-FEB-2015  OPC      Fixed Bug WIG-1921: incorrect date format.
' 26-FEB-2015  ANM      Toggle duration time by average time in grid
' 05-MAR-2015  ANM      Fixed Bug Sprint1-453: rounded to minutes
' 19-NOV-2015  CPC      Added column "Win + Drop" before "Win"
' 08-JUN-2016  FOS      Product Backlog Item 13462:Split drop in cashier and gambling tables drop
' 20-JUL-2016  RAB      Product Backlog Item 15066: GamingTables (Phase 4): Adapt reports to multicurrency.
' 26-OCT-2016  ESE      Bug 1709: The report shows wrong values, the total are not matching
' 01-DIC-2016  EOR      Bug 19913: Reporte de control de ingresos de mesas de juego: no se muestran valores en la columna "drop" de las mesas
'----------------------------------------------------------------------
Option Explicit On
Option Strict Off

#Region " Imports "

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports WSI.Common

#End Region

Public Class frm_gaming_tables_income_report
  Inherits GUI_Controls.frm_base_sel

#Region " Members "

  ' Members for totals and subtotals
  Private m_type As String
  Private m_validated_tickets As Decimal
  Private m_drop As Decimal
  Private m_drop_cashier As Decimal
  Private m_drop_gambling_tables As Decimal
  Private m_win As Decimal
  Private m_tips As Decimal
  Private m_win_tips As Decimal
  Private m_seconds As Decimal
  Private m_average As Decimal
  Private m_validated_tickets_total As Decimal
  Private m_drop_total As Decimal
  Private m_drop_total_cashier As Decimal
  Private m_drop_total_gambling_tables As Decimal
  Private m_win_total As Decimal
  Private m_tips_total As Decimal
  Private m_seconds_total As Decimal
  Private m_avg_total As Decimal
  Private m_current_interval As String
  Private m_current_interval_aux As String
  Private m_calculated_drop_total As Decimal
  Private m_calculated_drop_subtotal As Decimal
  Private m_calculated_drop_subtotal_aux As Decimal

  ' Table type subtotals members
  Private m_validated_tickets_aux As Decimal
  Private m_drop_aux As Decimal
  Private m_drop_gambling_tables_aux As Decimal
  Private m_drop_cashier_aux As Decimal
  Private m_win_aux As Decimal
  Private m_tips_aux As Decimal
  Private m_win_tips_aux As Decimal
  Private m_seconds_aux As Decimal
  Private m_avg_aux As Decimal

  ' Filter members
  Private m_date_from As String
  Private m_date_to As String
  Private m_report_base_type As GAMING_TABLES_REPORT_TYPE
  Private m_report_interval As GAMING_TABLES_REPORT_TIME_INTERVAL
  Private m_report_order As GAMING_TABLES_REPORT_ORDER
  Private m_report_only_activity As GAMING_TABLES_REPORT_ACTIVITY
  Private m_report_selected_types As String
  Private m_report_selected_types_names As String
  Private m_table_type_totals As Boolean

  ' Grid tooltip
  Private m_tool_tip As List(Of String)

  ' Currency iso code
  Private m_selected_currency As String
  Private m_selected_option As Integer
  Private m_selected_option_excel_print As String

#End Region

#Region " Constants "

  ' SQL Columns

  Private Const SQL_COLUMN_TABLE_ID As Integer = 0
  Private Const SQL_COLUMN_TABLE_NAME As Integer = 1
  Private Const SQL_COLUMN_TABLE_TYPE_ID As Integer = 2
  Private Const SQL_COLUMN_TABLE_TYPE_NAME As Integer = 3
  Private Const SQL_COLUMN_VALIDATED_TICKETS As Integer = 4
  Private Const SQL_COLUMN_DROP_GAMBLING_TABLES As Integer = 5
  Private Const SQL_COLUMN_DROP_CASHIER As Integer = 6
  Private Const SQL_COLUMN_TOTAL_DROP As Integer = 7
  Private Const SQL_COLUMN_WIN As Integer = 8
  Private Const SQL_COLUMN_TIPS As Integer = 9
  Private Const SQL_COLUMN_WIN_DROP As Integer = 10
  Private Const SQL_COLUMN_THEORIC_HOLD As Integer = 11
  Private Const SQL_COLUMN_TIPS_DROP As Integer = 12
  Private Const SQL_COLUMN_WIN_TIPS As Integer = 13
  Private Const SQL_COLUMN_FIRST_OPEN_SESSION As Integer = 14
  Private Const SQL_COLUMN_LAST_CLOSE_SESSION As Integer = 15
  Private Const SQL_COLUMN_SESSION_IS_CLOSED As Integer = 16
  Private Const SQL_COLUMN_MINUTES_OPEN As Integer = 17
  Private Const SQL_COLUMN_MINUTES_AVG As Integer = 18
  Private Const SQL_COLUMN_INTERVAL_DATE As Integer = 19

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_DATE As Integer = 1
  Private Const GRID_COLUMN_TABLE_NAME As Integer = 2
  Private Const GRID_COLUMN_VALIDATED_TICKETS As Integer = 3
  Private Const GRID_COLUMN_DROP_CASHIER As Integer = 4
  Private Const GRID_COLUMN_DROP_GAMING_TABLES As Integer = 5
  Private Const GRID_COLUMN_DROP_TOTAL As Integer = 6
  Private Const GRID_COLUMN_WIN As Integer = 7
  Private Const GRID_COLUMN_WIN_DROP_PER_CENT As Integer = 8
  Private Const GRID_COLUMN_THEORIC_HOLD As Integer = 9
  Private Const GRID_COLUMN_TIPS As Integer = 10
  Private Const GRID_COLUMN_TIPS_DROP_PER_CENT As Integer = 11
  Private Const GRID_COLUMN_WIN_TIPS As Integer = 12
  Private Const GRID_COLUMN_SESSION_HOURS As Integer = 13
  Private Const GRID_COLUMN_SESSION_OPEN As Integer = 14
  Private Const GRID_COLUMN_SESSION_CLOSE As Integer = 15

  Private Const GRID_COLUMNS As Integer = 16
  Private Const GRID_HEADER_ROWS As Integer = 1

  ' Excel
  Private Const EXCEL_COLUMN_DATE As Integer = 0

  ' Amount columns Width
  Private Const AMOUNT_COLUMNS_WIDTH As Integer = 1700

#End Region

#Region " Enums "

  ' Report Types
  Private Enum GAMING_TABLES_REPORT_TYPE
    GT_REPORT_TYPE_TOTAL = 0              ' By Table type
    GT_REPORT_TYPE_DETAIL = 1             ' By Table
  End Enum

  ' Report Time Interval
  Private Enum GAMING_TABLES_REPORT_TIME_INTERVAL
    GT_REPORT_TIME_INTERVAL_NONE = -1
    GT_REPORT_TIME_INTERVAL_BY_DAY = 0
    GT_REPORT_TIME_INTERVAL_BY_MONTH = 1
    GT_REPORT_TIME_INTERVAL_BY_YEAR = 2
  End Enum

  ' Report Order by
  Private Enum GAMING_TABLES_REPORT_ORDER
    GT_REPORT_ORDER_BY_DATE = 0
    GT_REPORT_ORDER_BY_TABLE = 1
  End Enum

  ' Report activity
  Private Enum GAMING_TABLES_REPORT_ACTIVITY
    GT_REPORT_ACTIVITY_ALL = 0
    GT_REPORT_ACTIVITY_ONLY = 1
  End Enum

#End Region

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_GAMING_TABLES_INCOME_REPORT

    Call MyBase.GUI_SetFormId()
  End Sub

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Set form Name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3408)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    ' Date
    Me.uc_dsl.Init(GLB_NLS_GUI_ALARMS.Id(443), True)

    ' Meters
    Me.uc_checked_list_table_type.GroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3409)

    ' Report Based On
    gb_report_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4417)
    rb_groupped_type_total.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4418)
    rb_grouped_type_detail.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4419)

    ' Report Options
    gb_report_options.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4421)
    chb_by_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4422)
    rb_interval_day.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4423)
    rb_interval_month.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4424)
    rb_interval_year.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4425)
    chb_order_by_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4426)
    chb_only_with_activity.Text = GLB_NLS_GUI_INVOICING.GetString(464)
    chb_table_type_totals.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4578)

    ' Note
    lbl_note1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4718) + Environment.NewLine + GLB_NLS_GUI_PLAYER_TRACKING.GetString(7816)

    ' Hold = Win/drop *100
    lbl_hold.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3414) & " = " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(3412) & " / " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(3411) & " * 100"

    ' Fill operators
    Call Me.uc_checked_list_table_type.ColumnWidth(uc_checked_list.GRID_COLUMN_DESC, 4250)
    Call FillTableTypeFilterGrid()

    ' Set filter default values
    Call SetDefaultValues()

    ' Grid
    Call GUI_StyleSheet()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Date selection 
    If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate > Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE : Checks out the DB row before adding it to the grid
  '              and process counters (& totals rows)
  '
  '  PARAMS :
  '     - INPUT :
  '           - DataRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the data row can be added) or False (the data row can not be added)
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    If m_report_interval <> GAMING_TABLES_REPORT_TIME_INTERVAL.GT_REPORT_TIME_INTERVAL_NONE Then
      If m_report_order = GAMING_TABLES_REPORT_ORDER.GT_REPORT_ORDER_BY_DATE Then
        If m_current_interval <> "" AndAlso m_current_interval <> CType(DbRow.Value(SQL_COLUMN_INTERVAL_DATE), Date).ToShortDateString() Then
          Call DrawRow(False)
          m_current_interval = CType(DbRow.Value(SQL_COLUMN_INTERVAL_DATE), Date).ToShortDateString()
          Call ResetGlobal()
        Else
          m_current_interval = CType(DbRow.Value(SQL_COLUMN_INTERVAL_DATE), Date).ToShortDateString()
        End If
      ElseIf m_report_order = GAMING_TABLES_REPORT_ORDER.GT_REPORT_ORDER_BY_TABLE Then
        If m_current_interval <> "" AndAlso m_current_interval <> DbRow.Value(SQL_COLUMN_TABLE_NAME) Then
          Call DrawRow(False)
          m_current_interval = DbRow.Value(SQL_COLUMN_TABLE_NAME)

          Call ResetGlobal()
        Else
          m_current_interval = DbRow.Value(SQL_COLUMN_TABLE_NAME)

        End If
      End If
    End If

    ' Set table type subtotals
    If m_report_base_type = GAMING_TABLES_REPORT_TYPE.GT_REPORT_TYPE_DETAIL AndAlso m_table_type_totals Then
      If m_current_interval_aux <> "" AndAlso m_current_interval_aux <> DbRow.Value(SQL_COLUMN_TABLE_TYPE_NAME) Then
        Call DrawRow(False, True)
        m_current_interval_aux = DbRow.Value(SQL_COLUMN_TABLE_TYPE_NAME)

        Call ResetGlobal(True)
      Else
        m_current_interval_aux = DbRow.Value(SQL_COLUMN_TABLE_TYPE_NAME)

      End If

      m_win_aux += DbRow.Value(SQL_COLUMN_WIN)
      m_drop_aux += DbRow.Value(SQL_COLUMN_TOTAL_DROP)
      m_drop_gambling_tables_aux += DbRow.Value(SQL_COLUMN_DROP_GAMBLING_TABLES)
      m_drop_cashier_aux += DbRow.Value(SQL_COLUMN_DROP_CASHIER)
      m_validated_tickets_aux += DbRow.Value(SQL_COLUMN_VALIDATED_TICKETS)
      m_tips_aux += DbRow.Value(SQL_COLUMN_TIPS)
      m_seconds_aux += DbRow.Value(SQL_COLUMN_MINUTES_OPEN)
      m_avg_aux += DbRow.Value(SQL_COLUMN_MINUTES_AVG)
      If Not DbRow.Value(SQL_COLUMN_TOTAL_DROP) Is DBNull.Value Then
        m_calculated_drop_subtotal_aux += GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TOTAL_DROP), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)
      End If

    End If

    Call ProcessCounters(DbRow)

    Return True
  End Function

  ' PURPOSE: 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - 
  Protected Overrides Sub GUI_AfterLastRow()

    If m_report_interval <> GAMING_TABLES_REPORT_TIME_INTERVAL.GT_REPORT_TIME_INTERVAL_NONE AndAlso Me.Grid.NumRows > 0 Then

      Call DrawRow(False)

      If m_report_base_type = GAMING_TABLES_REPORT_TYPE.GT_REPORT_TYPE_DETAIL AndAlso m_table_type_totals Then

        Call DrawRow(False, True)
      End If

    Else
      If m_report_base_type = GAMING_TABLES_REPORT_TYPE.GT_REPORT_TYPE_DETAIL AndAlso m_table_type_totals Then

        Call DrawRow(False, True)
      End If

    End If

    ' Draw totals
    Call DrawRow(True)

    If m_validated_tickets_total > 0 Then
      Me.Grid.Column(GRID_COLUMN_VALIDATED_TICKETS).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8063)
      Me.Grid.Column(GRID_COLUMN_VALIDATED_TICKETS).Width = AMOUNT_COLUMNS_WIDTH
    End If

  End Sub

  ' PURPOSE: Hide columns depending selected mode
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()
    ResetGlobal()
    ResetGlobal(True)

    m_drop_total = 0
    m_win_total = 0
    m_tips_total = 0
    m_seconds_total = 0
    m_avg_total = 0
    m_validated_tickets = 0
    m_drop_cashier = 0
    m_drop_gambling_tables = 0
    m_drop_total_cashier = 0
    m_drop_total_gambling_tables = 0
    m_calculated_drop_total = 0
    m_calculated_drop_subtotal = 0
    m_validated_tickets_total = 0

    m_current_interval = ""
    m_current_interval_aux = ""
  End Sub ' GUI_AfterLastRow

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _time As TimeSpan
    Dim _win As Decimal
    Dim _drop As Decimal
    Dim _windrop As Decimal

    _win = 0
    _drop = 0
    _windrop = 0

    With Me.Grid

      .Cell(RowIndex, GRID_COLUMN_INDEX).Value = ""

      ' Date
      Select Case m_report_interval
        Case GAMING_TABLES_REPORT_TIME_INTERVAL.GT_REPORT_TIME_INTERVAL_BY_DAY
          .Cell(RowIndex, GRID_COLUMN_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_INTERVAL_DATE), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)
        Case GAMING_TABLES_REPORT_TIME_INTERVAL.GT_REPORT_TIME_INTERVAL_BY_MONTH
          .Cell(RowIndex, GRID_COLUMN_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_INTERVAL_DATE), ENUM_FORMAT_DATE.FORMAT_DATE_MMYEAR)
        Case GAMING_TABLES_REPORT_TIME_INTERVAL.GT_REPORT_TIME_INTERVAL_BY_YEAR
          .Cell(RowIndex, GRID_COLUMN_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_INTERVAL_DATE), ENUM_FORMAT_DATE.FORMAT_DATE_YEAR)

      End Select

      ' Table / Table type name
      .Cell(RowIndex, GRID_COLUMN_TABLE_NAME).Value = DbRow.Value(SQL_COLUMN_TABLE_NAME)

      .Cell(RowIndex, GRID_COLUMN_VALIDATED_TICKETS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_VALIDATED_TICKETS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)

      .Cell(RowIndex, GRID_COLUMN_DROP_CASHIER).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_DROP_CASHIER), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)

      .Cell(RowIndex, GRID_COLUMN_DROP_GAMING_TABLES).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_DROP_GAMBLING_TABLES), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)

      .Cell(RowIndex, GRID_COLUMN_DROP_TOTAL).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TOTAL_DROP), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)

      .Cell(RowIndex, GRID_COLUMN_WIN).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_WIN), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)

      .Cell(RowIndex, GRID_COLUMN_WIN_DROP_PER_CENT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_WIN_DROP), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & Application.CurrentCulture.NumberFormat.PercentSymbol

      .Cell(RowIndex, GRID_COLUMN_TIPS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TIPS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)

      .Cell(RowIndex, GRID_COLUMN_TIPS_DROP_PER_CENT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_TIPS_DROP), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & Application.CurrentCulture.NumberFormat.PercentSymbol

      .Cell(RowIndex, GRID_COLUMN_WIN_TIPS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_WIN_TIPS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)

      .Cell(RowIndex, GRID_COLUMN_THEORIC_HOLD).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_THEORIC_HOLD), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & Application.CurrentCulture.NumberFormat.PercentSymbol

      '
      If Not DbRow.Value(SQL_COLUMN_FIRST_OPEN_SESSION) Is DBNull.Value Then
        .Cell(RowIndex, GRID_COLUMN_SESSION_OPEN).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_FIRST_OPEN_SESSION), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      End If

      '
      If Not DbRow.Value(SQL_COLUMN_LAST_CLOSE_SESSION) Is DBNull.Value Then
        .Cell(RowIndex, GRID_COLUMN_SESSION_CLOSE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_LAST_CLOSE_SESSION), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      End If

      If DbRow.Value(SQL_COLUMN_MINUTES_AVG) <> 0 Then
        _time = TimeSpan.FromSeconds(DbRow.Value(SQL_COLUMN_MINUTES_OPEN) / DbRow.Value(SQL_COLUMN_MINUTES_AVG))
      Else
        _time = TimeSpan.FromSeconds(0)
      End If
      .Cell(RowIndex, GRID_COLUMN_SESSION_HOURS).Value = TimeSpanToString(_time)

    End With

    Return True
  End Function

  ' PURPOSE : Set member values for filters
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_date_from = String.Empty
    m_date_to = String.Empty

    ' Dates filter checks
    If Me.uc_dsl.FromDateSelected Then
      m_date_from = GUI_FormatDate(Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime), _
                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If
    If Me.uc_dsl.ToDateSelected Then
      m_date_to = GUI_FormatDate(Me.uc_dsl.ToDate.AddHours(Me.uc_dsl.ClosingTime), _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Selected report type
    If rb_grouped_type_detail.Checked Then
      m_report_base_type = GAMING_TABLES_REPORT_TYPE.GT_REPORT_TYPE_DETAIL
    ElseIf rb_groupped_type_total.Checked Then
      m_report_base_type = GAMING_TABLES_REPORT_TYPE.GT_REPORT_TYPE_TOTAL
    End If

    ' Interval
    m_report_interval = GAMING_TABLES_REPORT_TIME_INTERVAL.GT_REPORT_TIME_INTERVAL_NONE
    If chb_by_date.Enabled AndAlso chb_by_date.Checked Then
      If rb_interval_day.Checked Then
        m_report_interval = GAMING_TABLES_REPORT_TIME_INTERVAL.GT_REPORT_TIME_INTERVAL_BY_DAY
      ElseIf rb_interval_month.Checked Then
        m_report_interval = GAMING_TABLES_REPORT_TIME_INTERVAL.GT_REPORT_TIME_INTERVAL_BY_MONTH
      ElseIf rb_interval_year.Checked Then
        m_report_interval = GAMING_TABLES_REPORT_TIME_INTERVAL.GT_REPORT_TIME_INTERVAL_BY_YEAR
      End If
    End If

    ' Order
    m_report_order = GAMING_TABLES_REPORT_ORDER.GT_REPORT_ORDER_BY_TABLE
    If chb_order_by_name.Enabled AndAlso Not chb_order_by_name.Checked Then
      m_report_order = GAMING_TABLES_REPORT_ORDER.GT_REPORT_ORDER_BY_DATE
    End If

    ' Only Activity
    m_report_only_activity = GAMING_TABLES_REPORT_ACTIVITY.GT_REPORT_ACTIVITY_ALL
    If chb_only_with_activity.Checked Then
      m_report_only_activity = GAMING_TABLES_REPORT_ACTIVITY.GT_REPORT_ACTIVITY_ONLY
    End If

    ' Selected table types
    m_report_selected_types = Me.uc_checked_list_table_type.SelectedIndexesList()
    m_report_selected_types_names = Me.uc_checked_list_table_type.SelectedValuesList()

    m_table_type_totals = Me.chb_table_type_totals.Enabled AndAlso Me.chb_table_type_totals.Checked

  End Sub

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS: 
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    Dim _report_type_value As String
    Dim _report_groupped_by As String

    _report_type_value = String.Empty
    _report_groupped_by = String.Empty

    Select Case m_report_base_type
      Case GAMING_TABLES_REPORT_TYPE.GT_REPORT_TYPE_DETAIL
        _report_type_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4419)
      Case GAMING_TABLES_REPORT_TYPE.GT_REPORT_TYPE_TOTAL
        _report_type_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4418)
    End Select

    If m_report_interval <> GAMING_TABLES_REPORT_TIME_INTERVAL.GT_REPORT_TIME_INTERVAL_NONE Then
      If m_report_order = GAMING_TABLES_REPORT_ORDER.GT_REPORT_ORDER_BY_TABLE Then
        Select Case m_report_base_type
          Case GAMING_TABLES_REPORT_TYPE.GT_REPORT_TYPE_DETAIL
            _report_groupped_by = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4427)
          Case GAMING_TABLES_REPORT_TYPE.GT_REPORT_TYPE_TOTAL
            _report_groupped_by = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4426)
        End Select
      End If
      If _report_groupped_by <> String.Empty Then
        _report_groupped_by &= ", "
      End If
      Select Case m_report_interval
        Case GAMING_TABLES_REPORT_TIME_INTERVAL.GT_REPORT_TIME_INTERVAL_BY_DAY
          _report_groupped_by &= GLB_NLS_GUI_PLAYER_TRACKING.GetString(4423)
        Case GAMING_TABLES_REPORT_TIME_INTERVAL.GT_REPORT_TIME_INTERVAL_BY_MONTH
          _report_groupped_by &= GLB_NLS_GUI_PLAYER_TRACKING.GetString(4424)
        Case GAMING_TABLES_REPORT_TIME_INTERVAL.GT_REPORT_TIME_INTERVAL_BY_YEAR
          _report_groupped_by &= GLB_NLS_GUI_PLAYER_TRACKING.GetString(4425)
      End Select
    End If

    ' Dates filter checks
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & Space(1) & GLB_NLS_GUI_AUDITOR.GetString(257), _
                        m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & Space(1) & GLB_NLS_GUI_AUDITOR.GetString(258), _
                        m_date_to)

    ' Space
    PrintData.SetFilter(String.Empty, String.Empty, True)

    ' Options
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4417), _report_type_value)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4421), _report_groupped_by)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(464), IIf(m_report_only_activity, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)))
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4578), IIf(m_table_type_totals, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)))

    ' Gaming Table Types
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(3409), m_report_selected_types_names)
    ' Note
    PrintData.SetFilter(String.Empty, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4718) + Environment.NewLine + GLB_NLS_GUI_PLAYER_TRACKING.GetString(7816), False)
    ' Space
    PrintData.SetFilter(String.Empty, String.Empty, True)
    PrintData.SetFilter(String.Empty, String.Empty, True)

    ' Width from header filter
    PrintData.Settings.FilterHeaderWidth(1) = 1600
    PrintData.Settings.FilterHeaderWidth(2) = 1000
    PrintData.Settings.FilterHeaderWidth(3) = 1400

    ' Currency type
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7382), m_selected_option_excel_print)

  End Sub

  ' PURPOSE: Set the query method to use
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '     - ENUM_QUERY_TYPE
  Protected Overrides Function GUI_GetQueryType() As GUI_Controls.frm_base_sel.ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM
  End Function

  ' PURPOSE: Executes the query with a command
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '     - 
  Protected Overrides Sub GUI_ExecuteQueryCustom()
    Dim _sql_cmd As SqlCommand
    Dim _table As DataTable
    Dim _row As DataRow

    Call GUI_StyleSheet()

    m_selected_option = uc_multicurrency.SelectedOption
    If (m_selected_option = Convert.ToInt32(uc_multi_currency_site_sel.MULTICURRENCY_OPTION.TotalToIsoCode)) Then
      m_selected_currency = GeneralParam.GetString("RegionalOptions", "CurrenciesAccepted").Replace(";", ",")
    Else
      m_selected_currency = uc_multicurrency.SelectedCurrency
    End If

    _sql_cmd = New SqlCommand("GT_Base_Report_Data")
    _sql_cmd.CommandType = CommandType.StoredProcedure

    _sql_cmd.Parameters.Add("@pBaseType", SqlDbType.Int).Value = m_report_base_type
    _sql_cmd.Parameters.Add("@pTimeInterval", SqlDbType.Int).Value = m_report_interval
    _sql_cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = IIf(Not Me.uc_dsl.FromDateSelected, System.DBNull.Value, m_date_from)
    _sql_cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = IIf(Not Me.uc_dsl.ToDateSelected, System.DBNull.Value, m_date_to)
    _sql_cmd.Parameters.Add("@pOnlyActivity", SqlDbType.Int).Value = m_report_only_activity
    _sql_cmd.Parameters.Add("@pOrderBy", SqlDbType.Int).Value = m_report_order
    _sql_cmd.Parameters.Add("@pValidTypes", SqlDbType.VarChar).Value = m_report_selected_types
    _sql_cmd.Parameters.Add("@pSelectedCurrency", SqlDbType.NVarChar).Value = m_selected_currency
    _sql_cmd.Parameters.Add("@pTotalToSelectedCurrency", SqlDbType.Int).Value = m_selected_option

    _table = GUI_GetTableUsingCommand(_sql_cmd, Integer.MaxValue)

    Call GUI_BeforeFirstRow()

    Dim db_row As CLASS_DB_ROW
    Dim count As Integer
    Dim idx_row As Integer

    If _table.Rows.Count > 0 Then
      For Each _row In _table.Rows

        Try
          db_row = New CLASS_DB_ROW(_row)

          If GUI_CheckOutRowBeforeAdd(db_row) Then
            ' Add the db row to the grid
            Me.Grid.AddRow()
            count = count + 1
            idx_row = Me.Grid.NumRows - 1

            If Not GUI_SetupRow(idx_row, db_row) Then
              ' The row can not be added
              Me.Grid.DeleteRowFast(idx_row)
              count = count - 1
            End If
          End If

        Catch ex As OutOfMemoryException
          Throw ex

        Catch exception As Exception
          Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
          Call Trace.WriteLine(exception.ToString())
          Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                                "frm_gaming_tables_income_report", _
                                "GUI_SetupRow", _
                                exception.Message)
          Exit For
        End Try

        db_row = Nothing

        If Not GUI_DoEvents(Me.Grid) Then
          Exit Sub
        End If

      Next

      Call GUI_AfterLastRow()
    End If

    'Set m_multicurrency_selected_option when GUI_ExecuteQueryCustom function its ok.
    If (uc_multicurrency.SelectedOption = uc_multi_currency_site_sel.MULTICURRENCY_OPTION.TotalToIsoCode) Then
      m_selected_option_excel_print = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7397, CurrencyExchange.GetNationalCurrency())
    ElseIf (uc_multicurrency.SelectedOption = uc_multi_currency_site_sel.MULTICURRENCY_OPTION.ByCurrency) Then
      m_selected_option_excel_print = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7398) & ": " & m_selected_currency
    End If

  End Sub

#End Region

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Fill the filter Grid with data
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub FillTableTypeFilterGrid()
    Dim _data_table_tables_type As DataTable

    Call Me.uc_checked_list_table_type.Clear()
    Call Me.uc_checked_list_table_type.ReDraw(False)

    _data_table_tables_type = GamingTablesType_DBRead()
    Me.uc_checked_list_table_type.Add(_data_table_tables_type, "ID", "NAME")

    If Me.uc_checked_list_table_type.Count() > 0 Then
      Call Me.uc_checked_list_table_type.CurrentRow(0)
    End If

    Call Me.uc_checked_list_table_type.ReDraw(True)

  End Sub 'FillTableTypeFilterGrid

  ' PURPOSE: Read the data from gaming tables types
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - DataTable: collected data
  Private Function GamingTablesType_DBRead() As DataTable
    Dim _str_sql As String
    Dim _data_table_tables_type As DataTable

    _str_sql = "SELECT   gtt_gaming_table_type_id AS ID" & _
              "        , GTT_NAME  AS NAME" & _
              "   FROM   GAMING_TABLES_TYPES " & _
              "  WHERE   GTT_ENABLED =  " & 1 & ""

    _data_table_tables_type = GUI_GetTableUsingSQL(_str_sql, 5000)

    Return _data_table_tables_type
  End Function

  ' PURPOSE: Set the tool tip text for a given row and column
  '
  '  PARAMS:
  '     - INPUT:
  '           - RowIndex As Integer
  '           - ColIndex As Integer
  '     - OUTPUT:
  '           - ToolTipTxt As String
  '
  ' RETURNS:

  Protected Overrides Sub GUI_SetToolTipText(ByVal RowIndex As Integer, _
                                             ByVal ColIndex As Integer, _
                                             ByRef ToolTipTxt As String)


    If RowIndex = -3 Then
      Select Case ColIndex
        Case GRID_COLUMN_DROP_CASHIER
          ToolTipTxt = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7812)

        Case GRID_COLUMN_DROP_GAMING_TABLES
          ToolTipTxt = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7813)

        Case GRID_COLUMN_DROP_TOTAL
          ToolTipTxt = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7814)

        Case GRID_COLUMN_WIN
          ToolTipTxt = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7815)

        Case GRID_COLUMN_TIPS_DROP_PER_CENT
          ' ToolTip for T-D%
          ToolTipTxt = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3415)
      End Select
    End If

  End Sub ' GUI_SetToolTipText

  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)
    Call MyBase.GUI_ReportParams(ExcelData)

    ExcelData.SetColumnFormat(EXCEL_COLUMN_DATE, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.DATE)

    If m_report_interval = GAMING_TABLES_REPORT_TIME_INTERVAL.GT_REPORT_TIME_INTERVAL_BY_MONTH Then
      ExcelData.KeepDateFormat = True
    End If

  End Sub ' GUI_ReportParams

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    With Me.Grid

      .IsSortable = False
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      ' Index      
      GUI_AddColumn(Me.Grid, GRID_COLUMN_INDEX, " ", 200, , " ", False)
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False

      ' TODO: DATE
      Dim _column_printable As Boolean
      Dim _column_width As Integer

      Select Case m_report_interval
        Case GAMING_TABLES_REPORT_TIME_INTERVAL.GT_REPORT_TIME_INTERVAL_NONE
          _column_width = 0
          _column_printable = False
        Case Else
          _column_width = 1250
          _column_printable = True
      End Select

      GUI_AddColumn(Me.Grid, GRID_COLUMN_DATE, " ", _column_width, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER, GLB_NLS_GUI_JACKPOT_MGR.GetString(297), _column_printable)

      ' Table Name
      Dim _header1 As String
      _header1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3410)

      Select Case m_report_base_type
        Case GAMING_TABLES_REPORT_TYPE.GT_REPORT_TYPE_DETAIL
          .Column(GRID_COLUMN_TABLE_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3410)
        Case GAMING_TABLES_REPORT_TYPE.GT_REPORT_TYPE_TOTAL
          .Column(GRID_COLUMN_TABLE_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4418)
      End Select

      GUI_AddColumn(Me.Grid, GRID_COLUMN_TABLE_NAME, _header1, 3000)

      ' DROP
      GUI_AddColumn(Me.Grid, GRID_COLUMN_VALIDATED_TICKETS, "", 1, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT)

      GUI_AddColumn(Me.Grid, GRID_COLUMN_DROP_CASHIER, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7337), AMOUNT_COLUMNS_WIDTH, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT)

      GUI_AddColumn(Me.Grid, GRID_COLUMN_DROP_GAMING_TABLES, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7336), AMOUNT_COLUMNS_WIDTH, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT)

      GUI_AddColumn(Me.Grid, GRID_COLUMN_DROP_TOTAL, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3411), AMOUNT_COLUMNS_WIDTH, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT)

      ' WIN
      GUI_AddColumn(Me.Grid, GRID_COLUMN_WIN, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3412), AMOUNT_COLUMNS_WIDTH, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT)

      ' THEORIC HOLD
      Select Case m_report_base_type

        Case GAMING_TABLES_REPORT_TYPE.GT_REPORT_TYPE_TOTAL
          _column_width = 0
          _column_printable = False

        Case GAMING_TABLES_REPORT_TYPE.GT_REPORT_TYPE_DETAIL
          _column_width = AMOUNT_COLUMNS_WIDTH
          _column_printable = True

      End Select

      GUI_AddColumn(Me.Grid, GRID_COLUMN_THEORIC_HOLD, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5629), _column_width, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, _column_printable)

      ' W-D %
      GUI_AddColumn(Me.Grid, GRID_COLUMN_WIN_DROP_PER_CENT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3414), 1150, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT)

      ' TIP
      GUI_AddColumn(Me.Grid, GRID_COLUMN_TIPS, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3413), AMOUNT_COLUMNS_WIDTH, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT)

      ' T-D %
      GUI_AddColumn(Me.Grid, GRID_COLUMN_TIPS_DROP_PER_CENT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4710), 1150, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT)

      ' W-T %
      GUI_AddColumn(Me.Grid, GRID_COLUMN_WIN_TIPS, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3412) + "+" + GLB_NLS_GUI_PLAYER_TRACKING.GetString(3413), AMOUNT_COLUMNS_WIDTH, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT)

      ' Session Open
      GUI_AddColumn(Me.Grid, GRID_COLUMN_SESSION_OPEN, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3462), 0, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, False)

      ' Session Close
      GUI_AddColumn(Me.Grid, GRID_COLUMN_SESSION_CLOSE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3463), 0, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, False)

      ' Session Hours
      GUI_AddColumn(Me.Grid, GRID_COLUMN_SESSION_HOURS, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4981), 1300, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT)

    End With
  End Sub ' GUI_StyleSheet


  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()
    Dim _current_date As DateTime

    _current_date = WSI.Common.WGDB.Now

    Me.uc_dsl.ClosingTime = GetDefaultClosingTime()

    Me.uc_dsl.FromDate = New DateTime(_current_date.Year, _current_date.Month, _current_date.Day, 0, 0, 0)

    Me.uc_dsl.FromDateSelected = True

    Me.uc_dsl.ToDate = Me.uc_dsl.FromDate.AddDays(1)

    Me.uc_dsl.ToDateSelected = False

    Me.rb_groupped_type_total.Checked = True

    Call Me.uc_checked_list_table_type.SetDefaultValue(True)

    chb_by_date.CheckState = CheckState.Unchecked
    rb_interval_month.Checked = True
    chb_only_with_activity.CheckState = CheckState.Unchecked
    chb_order_by_name.CheckState = CheckState.Unchecked
    chb_table_type_totals.Enabled = Me.rb_grouped_type_detail.Checked
    chb_table_type_totals.Checked = True

    Call GUI_ReportUpdateFilters()

    'uc_multi_currency_site_sel
    uc_multicurrency.OpenModeControl = uc_multi_currency_site_sel.OPEN_MODE.AllControls
    Me.uc_multicurrency.Init()

  End Sub ' SetDefaultValues

  ' PURPOSE : Process total counters: Draw row of totals, update and reset total counters.
  '
  '  PARAMS :
  '     - INPUT : 
  '           - DbRow As CLASS_DB_ROW
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  Private Sub ProcessCounters(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW)

    m_drop += DbRow.Value(SQL_COLUMN_TOTAL_DROP)
    m_drop_cashier += DbRow.Value(SQL_COLUMN_DROP_CASHIER)
    m_drop_gambling_tables += DbRow.Value(SQL_COLUMN_DROP_GAMBLING_TABLES)
    m_validated_tickets += DbRow.Value(SQL_COLUMN_VALIDATED_TICKETS)
    m_tips += DbRow.Value(SQL_COLUMN_TIPS)
    m_win += DbRow.Value(SQL_COLUMN_WIN)
    m_seconds += DbRow.Value(SQL_COLUMN_MINUTES_OPEN)
    m_average += DbRow.Value(SQL_COLUMN_MINUTES_AVG)

    m_drop_total += DbRow.Value(SQL_COLUMN_TOTAL_DROP)
    m_drop_total_cashier += DbRow.Value(SQL_COLUMN_DROP_CASHIER)
    m_drop_total_gambling_tables += DbRow.Value(SQL_COLUMN_DROP_GAMBLING_TABLES)
    m_validated_tickets_total += DbRow.Value(SQL_COLUMN_VALIDATED_TICKETS)
    m_tips_total += DbRow.Value(SQL_COLUMN_TIPS)
    m_win_total += DbRow.Value(SQL_COLUMN_WIN)
    m_seconds_total += DbRow.Value(SQL_COLUMN_MINUTES_OPEN)
    m_avg_total += DbRow.Value(SQL_COLUMN_MINUTES_AVG)

    If Not DbRow.Value(SQL_COLUMN_TOTAL_DROP) Is DBNull.Value Then
      m_calculated_drop_total += GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TOTAL_DROP), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)
      m_calculated_drop_subtotal += GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TOTAL_DROP), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)
    End If

  End Sub

  ' PURPOSE: Show results to screen drawing the total counter data in a new row with the specified color
  '
  '  PARAMS:
  '     - INPUT:
  '          - LastTotal: Indicates the kind of total
  '     - OUTPUT:
  '
  ' RETURNS:
  '   - True
  Private Function DrawRow(ByVal LastTotal As Boolean, Optional ByVal IsTableTypeSubTotal As Boolean = False) As Boolean
    Dim _idx_row As Integer
    Dim _row_back_color As ENUM_GUI_COLOR
    Dim _validated_tickets As Decimal
    Dim _drop As Decimal
    Dim _drop_cashier As Decimal
    Dim _drop_gambling_tables As Decimal
    Dim _tips As Decimal
    Dim _win As Decimal
    Dim _win_tips As Decimal
    Dim _seconds As Decimal
    Dim _average As Decimal
    Dim _time As TimeSpan
    Dim _calculated_drop As Decimal

    ' Default row back color for subtotal 
    _row_back_color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01

    If LastTotal Then
      m_type = GLB_NLS_GUI_INVOICING.GetString(205) ' TOTAL:
      _drop = m_drop_total
      _tips = m_tips_total
      _win = m_win_total
      _win_tips = m_tips_total + m_win_total
      _seconds = m_seconds_total
      _average = m_avg_total
      _drop_cashier = m_drop_total_cashier
      _drop_gambling_tables = m_drop_total_gambling_tables
      _calculated_drop = m_calculated_drop_total
      _validated_tickets = m_validated_tickets_total

      ' Color for total
      _row_back_color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
    Else

      _drop = m_drop
      _tips = m_tips
      _win = m_win
      _win_tips = m_tips + m_win
      _seconds = m_seconds
      _average = m_average
      _drop_cashier = m_drop_cashier
      _drop_gambling_tables = m_drop_gambling_tables
      _calculated_drop = m_calculated_drop_subtotal
      _validated_tickets = m_validated_tickets

      Select Case m_report_order
        Case GAMING_TABLES_REPORT_ORDER.GT_REPORT_ORDER_BY_DATE
          Select Case m_report_interval
            Case GAMING_TABLES_REPORT_TIME_INTERVAL.GT_REPORT_TIME_INTERVAL_BY_DAY
              m_type = GUI_FormatDate(Date.Parse(m_current_interval), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)
            Case GAMING_TABLES_REPORT_TIME_INTERVAL.GT_REPORT_TIME_INTERVAL_BY_MONTH
              m_type = GUI_FormatDate(Date.Parse(m_current_interval), ENUM_FORMAT_DATE.FORMAT_DATE_MMYEAR)
            Case GAMING_TABLES_REPORT_TIME_INTERVAL.GT_REPORT_TIME_INTERVAL_BY_YEAR
              m_type = GUI_FormatDate(Date.Parse(m_current_interval), ENUM_FORMAT_DATE.FORMAT_DATE_YEAR)
          End Select

        Case GAMING_TABLES_REPORT_ORDER.GT_REPORT_ORDER_BY_TABLE
          m_type = m_current_interval
          _row_back_color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01
      End Select

      If IsTableTypeSubTotal Then
        m_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3409) & Space(1) & m_current_interval_aux ' Table type: X
        _row_back_color = ENUM_GUI_COLOR.GUI_COLOR_BLUE_02

        _drop = m_drop_aux
        _tips = m_tips_aux
        _win = m_win_aux
        _win_tips = m_tips_aux + m_win_aux
        _seconds = m_seconds_aux
        _average = m_avg_aux
        _drop_cashier = m_drop_cashier_aux
        _drop_gambling_tables = m_drop_gambling_tables_aux
        _calculated_drop = m_calculated_drop_subtotal_aux
        _validated_tickets = m_validated_tickets_aux

      End If

    End If

    ' Add the totals row & data
    With Me.Grid
      .AddRow()
      _idx_row = Me.Grid.NumRows - 1

      .Cell(_idx_row, GRID_COLUMN_INDEX).Value = String.Empty

      If m_report_order = GAMING_TABLES_REPORT_ORDER.GT_REPORT_ORDER_BY_DATE AndAlso Not LastTotal AndAlso Not IsTableTypeSubTotal Then
        .Cell(_idx_row, GRID_COLUMN_DATE).Value = m_type
      Else
        .Cell(_idx_row, GRID_COLUMN_TABLE_NAME).Value = m_type
      End If

      .Cell(_idx_row, GRID_COLUMN_VALIDATED_TICKETS).Value = GUI_FormatCurrency(_validated_tickets, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)
      .Cell(_idx_row, GRID_COLUMN_DROP_CASHIER).Value = GUI_FormatCurrency(_drop_cashier, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)
      .Cell(_idx_row, GRID_COLUMN_DROP_GAMING_TABLES).Value = GUI_FormatCurrency(_drop_gambling_tables, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)
      .Cell(_idx_row, GRID_COLUMN_DROP_TOTAL).Value = GUI_FormatCurrency(_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)

      .Cell(_idx_row, GRID_COLUMN_WIN).Value = GUI_FormatCurrency(_win, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)

      .Cell(_idx_row, GRID_COLUMN_TIPS).Value = GUI_FormatCurrency(_tips, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)

      ' Prevent divide by 0
      If _drop = 0 Then
        .Cell(_idx_row, GRID_COLUMN_WIN_DROP_PER_CENT).Value = GUI_FormatNumber(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & Application.CurrentCulture.NumberFormat.PercentSymbol

      Else
        .Cell(_idx_row, GRID_COLUMN_WIN_DROP_PER_CENT).Value = GUI_FormatNumber((_win / _drop) * 100, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & Application.CurrentCulture.NumberFormat.PercentSymbol

      End If

      ' Prevent divide by 0
      If _calculated_drop = 0 Then
        .Cell(_idx_row, GRID_COLUMN_TIPS_DROP_PER_CENT).Value = GUI_FormatNumber(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & Application.CurrentCulture.NumberFormat.PercentSymbol

      Else
        Dim _aux_tip_drop As Decimal
        _aux_tip_drop = (_tips / _calculated_drop) * 100
        .Cell(_idx_row, GRID_COLUMN_TIPS_DROP_PER_CENT).Value = GUI_FormatNumber(_aux_tip_drop, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & Application.CurrentCulture.NumberFormat.PercentSymbol

        'Dim _aux_split As String()
        '_aux_split = _aux_tip_drop.ToString().Split(".")
        'Dim _aux_total As String
        '_aux_total = _aux_split(0) + "." + _aux_split(1).Remove(2)

        '.Cell(_idx_row, GRID_COLUMN_TIPS_DROP_PER_CENT).Value = GUI_FormatNumber(Decimal.Parse(_aux_total), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & Application.CurrentCulture.NumberFormat.PercentSymbol

      End If

      .Cell(_idx_row, GRID_COLUMN_WIN_TIPS).Value = GUI_FormatCurrency(_win_tips, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)

      If _average <> 0 Then
        _time = TimeSpan.FromSeconds(_seconds / _average)
      Else
        _time = TimeSpan.FromSeconds(0)
      End If
      .Cell(_idx_row, GRID_COLUMN_SESSION_HOURS).Value = TimeSpanToString(_time)

      .Row(_idx_row).BackColor = GetColor(_row_back_color)

    End With

    Return True
  End Function ' DrawRow

  ' PURPOSE: Initialice the subtotal and total data
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ResetGlobal(Optional ByVal IsTableTypeSubTotal As Boolean = False)

    If IsTableTypeSubTotal Then
      m_drop_aux = 0
      m_win_aux = 0
      m_tips_aux = 0
      m_win_tips_aux = 0
      m_seconds_aux = 0
      m_avg_aux = 0
      m_drop_cashier_aux = 0
      m_drop_gambling_tables_aux = 0
      m_calculated_drop_subtotal_aux = 0
      m_validated_tickets_aux = 0

    Else
      m_type = ""
      m_drop = 0
      m_win = 0
      m_tips = 0
      m_win_tips = 0
      m_seconds = 0
      m_average = 0
      m_calculated_drop_subtotal = 0
      m_drop_cashier = 0
      m_drop_gambling_tables = 0
      m_validated_tickets = 0

    End If

  End Sub

#End Region

#Region " Events "

  ' PURPOSE: Wen checked state changes on grouped by date interval
  '
  '  PARAMS:
  '     - INPUT:
  '           - sender
  '           - e
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - None
  Private Sub chb_by_date_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chb_by_date.CheckedChanged
    rb_interval_day.Enabled = chb_by_date.Checked
    rb_interval_month.Enabled = chb_by_date.Checked
    rb_interval_year.Enabled = chb_by_date.Checked
    chb_order_by_name.Enabled = chb_by_date.Checked
  End Sub

  Private Sub rb_groupped_type_total_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_groupped_type_total.CheckedChanged, rb_grouped_type_detail.CheckedChanged
    If rb_groupped_type_total.Checked Then
      chb_order_by_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4426)
    ElseIf rb_grouped_type_detail.Checked Then
      chb_order_by_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4427)
    End If
    chb_table_type_totals.Enabled = rb_grouped_type_detail.Checked
  End Sub

#End Region

End Class