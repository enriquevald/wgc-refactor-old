'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_password_configuration
' DESCRIPTION:   Configure/Cash by service
' AUTHOR:        Humberto Braojos
' CREATION DATE: 13-Sep-2012
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 13-Sep-2012 HBB    First Release
' 22-SEP-2016 EOR    PBI 17963: Televisa - Service Charge: Modifications Voucher / Report / Settings
'-------------------------------------------------------------------

Option Strict Off
Option Explicit On


Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common.Misc
Imports System.Data.SqlClient
Imports GUI_CommonOperations.CLASS_BASE
Imports WSI.Common.Users

Public Class frm_service_charge_edit
  Inherits frm_base_edit

#Region " Members"

  Private Const m_code_ascii As Int32 = 65    ' 22-SEP-2016 EOR

#End Region

#Region " Overrides "

  'PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()
    MyBase.GUI_InitControls()
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1294)

    'setting text to the control
    Me.chk_enable_service_charge.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1295)
    Me.ef_concept.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1296)
    Me.ef_percent_over_devolution_lower_amount.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1297)
    Me.ef_percent_over_devolution_greater_amount.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1297)
    Me.ef_percent_over_award_amount.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1298)
    Me.gb_amount_lower.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1299)
    Me.gb_amount_greater.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1300)
    Me.cmb_awardcalculation.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1316)
    Me.cmb_on_company.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7623)    ' 22-SEP-2016 EOR


    'setting the entry fields format
    Me.ef_concept.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 40)
    Me.ef_percent_over_devolution_lower_amount.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 5, 2)
    Me.ef_percent_over_devolution_greater_amount.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 5, 2)
    Me.ef_percent_over_award_amount.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 5, 2)

    Me.ef_percent_over_devolution_lower_amount.IsReadOnly = False
    Me.ef_percent_over_devolution_greater_amount.IsReadOnly = False
    Me.ef_percent_over_award_amount.IsReadOnly = False

    'adding values to the combobox
    Me.cmb_awardcalculation.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1317))
    Me.cmb_awardcalculation.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1318))

    'EOR 21-SEP-2016 adding values to the combobox company
    Me.cmb_on_company.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6747))
    Me.cmb_on_company.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6748))

    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False

    AddHandler cmb_awardcalculation.ValueChangedEvent, AddressOf cmb_awardcalculation_ValueChangedEvent
  End Sub

  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_FORM.FORM_CASH_SERVICE
    Call MyBase.GUI_SetFormId()
  End Sub

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)
    Dim _service_charge As SERVICE_CHARGE

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New SERVICE_CHARGE
        _service_charge = DbEditedObject

      Case ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE
        DbReadObject = DbEditedObject.Duplicate()

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Me.DbStatus = ENUM_STATUS.STATUS_OK

        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select
  End Sub

  ' PURPOSE: Validate the data presented on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    If ef_concept.Value = "" Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1296))
      ef_concept.Focus()

      Return False
    End If

    If Not CheckEntryData(ef_percent_over_devolution_lower_amount.Value, 0, 100) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1305), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1297), "0.00", "100.00")
      ef_percent_over_devolution_lower_amount.Focus()

      Return False
    End If

    If Not CheckEntryData(ef_percent_over_devolution_greater_amount.Value, 0, 100) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1305), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1297), "0.00", "100.00")
      ef_percent_over_devolution_greater_amount.Focus()

      Return False
    End If

    If Not CheckEntryData(ef_percent_over_award_amount.Value, 0, 100) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1305), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1298), "0.00", "100.00")
      ef_percent_over_award_amount.Focus()

      Return False
    End If

    If chk_enable_service_charge.Checked And cmb_awardcalculation.SelectedIndex = -1 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1321), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1295), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1316))

      cmb_awardcalculation.Focus()

      Return False
    End If

    Return True

  End Function

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _service_charge As SERVICE_CHARGE

    _service_charge = DbReadObject

    chk_enable_service_charge.Checked = _service_charge.ServiceChargeEnabled

    ef_concept.Value = _service_charge.Concept

    'it is checked if the values are greater or equal than zero because they could be negatives 
    'and it does not have sense a negative percent. The entry fields does not let you type negative values but 
    'they could be changed in the general params window
    If _service_charge.PercentOverLowerDevolution >= 0 Then
      ef_percent_over_devolution_lower_amount.Value = _service_charge.PercentOverLowerDevolution
    Else
      ef_percent_over_devolution_lower_amount.Value = ""
      _service_charge.PercentOverLowerDevolution = -1
    End If

    If _service_charge.PercentOverGreaterDevolution >= 0 Then
      ef_percent_over_devolution_greater_amount.Value = _service_charge.PercentOverGreaterDevolution
    Else
      ef_percent_over_devolution_greater_amount.Value = ""
      _service_charge.PercentOverGreaterDevolution = -1
    End If

    If _service_charge.PercentOverAward >= 0 Then
      ef_percent_over_award_amount.Value = _service_charge.PercentOverAward
    Else
      ef_percent_over_award_amount.Value = ""
      _service_charge.PercentOverAward = -1
    End If

    If _service_charge.CalculationOfAward < 1 Then
      cmb_awardcalculation.SelectedIndex = 0
      _service_charge.CalculationOfAward = 1
      Call cmb_awardcalculation_ValueChangedEvent()
    Else
      cmb_awardcalculation.SelectedIndex = _service_charge.CalculationOfAward - 1
      Call cmb_awardcalculation_ValueChangedEvent()
    End If

    ' 22-SEP-2016 EOR
    cmb_on_company.SelectedIndex = Convert.ToInt32(_service_charge.AmountAttributedCompany) - m_code_ascii

  End Sub

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_GetScreenData()

    Dim _service_charge As SERVICE_CHARGE
    _service_charge = DbEditedObject

    _service_charge.ServiceChargeEnabled = chk_enable_service_charge.Checked
    _service_charge.Concept = ef_concept.Value

    If Not Decimal.TryParse(ef_percent_over_devolution_lower_amount.Value, _service_charge.PercentOverLowerDevolution) Then
      _service_charge.PercentOverLowerDevolution = -1
    End If

    If Not Decimal.TryParse(ef_percent_over_devolution_greater_amount.Value, _service_charge.PercentOverGreaterDevolution) Then
      _service_charge.PercentOverGreaterDevolution = -1
    End If

    If Not Decimal.TryParse(ef_percent_over_award_amount.Value, _service_charge.PercentOverAward) Then
      _service_charge.PercentOverAward = -1
    End If

    _service_charge.CalculationOfAward = cmb_awardcalculation.SelectedIndex + 1

    ' 22-SEP-2016 EOR
    _service_charge.AmountAttributedCompany = Strings.Chr(m_code_ascii + cmb_on_company.SelectedIndex).ToString()


  End Sub
#End Region

#Region "Privates methods"

  Private Function CheckEntryData(ByVal EntryValue As String, ByVal MinPosibleValue As Double, ByVal MaxPosibleValue As Double) As Boolean

    Dim _entryvalueafterparse As Double

    _entryvalueafterparse = 0

    If Double.TryParse(EntryValue, _entryvalueafterparse) And _entryvalueafterparse >= MinPosibleValue And _entryvalueafterparse <= MaxPosibleValue Then
      Return True
    Else
      Return False
    End If


  End Function

#End Region

#Region "Public methods"

#End Region

#Region "Events"

  Private Sub cmb_awardcalculation_ValueChangedEvent()

    If cmb_awardcalculation.SelectedIndex = 1 Then
      Me.lbl_note.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1301)
    Else
      Me.lbl_note.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1322)
    End If

  End Sub 'cmb_area_ValueChangedEvent

#End Region
End Class