<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_service_monitor
  Inherits GUI_Controls.frm_base

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container
    Me.Service_Grid = New GUI_Controls.uc_grid
    Me.btn_exit = New GUI_Controls.uc_button
    Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
    Me.gb_status = New System.Windows.Forms.GroupBox
    Me.lbl_inactive = New System.Windows.Forms.Label
    Me.tf_inactive = New GUI_Controls.uc_text_field
    Me.lbl_standby = New System.Windows.Forms.Label
    Me.tf_standby = New GUI_Controls.uc_text_field
    Me.lbl_running = New System.Windows.Forms.Label
    Me.tf_running = New GUI_Controls.uc_text_field
    Me.Database_Grid = New GUI_Controls.uc_grid
    Me.gb_status.SuspendLayout()
    Me.SuspendLayout()
    '
    'Service_Grid
    '
    Me.Service_Grid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Service_Grid.CurrentCol = -1
    Me.Service_Grid.CurrentRow = -1
    Me.Service_Grid.Location = New System.Drawing.Point(9, 191)
    Me.Service_Grid.Name = "Service_Grid"
    Me.Service_Grid.PanelRightVisible = False
    Me.Service_Grid.Redraw = True
    Me.Service_Grid.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.Service_Grid.Size = New System.Drawing.Size(590, 203)
    Me.Service_Grid.Sortable = False
    Me.Service_Grid.SortAscending = True
    Me.Service_Grid.SortByCol = 0
    Me.Service_Grid.TabIndex = 1
    Me.Service_Grid.ToolTipped = True
    Me.Service_Grid.TopRow = -2
    '
    'btn_exit
    '
    Me.btn_exit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btn_exit.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btn_exit.Location = New System.Drawing.Point(605, 364)
    Me.btn_exit.Name = "btn_exit"
    Me.btn_exit.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_exit.Size = New System.Drawing.Size(90, 30)
    Me.btn_exit.TabIndex = 4
    Me.btn_exit.ToolTipped = False
    Me.btn_exit.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'Timer1
    '
    '
    'gb_status
    '
    Me.gb_status.Controls.Add(Me.lbl_inactive)
    Me.gb_status.Controls.Add(Me.tf_inactive)
    Me.gb_status.Controls.Add(Me.lbl_standby)
    Me.gb_status.Controls.Add(Me.tf_standby)
    Me.gb_status.Controls.Add(Me.lbl_running)
    Me.gb_status.Controls.Add(Me.tf_running)
    Me.gb_status.Location = New System.Drawing.Point(9, 13)
    Me.gb_status.Name = "gb_status"
    Me.gb_status.Size = New System.Drawing.Size(123, 86)
    Me.gb_status.TabIndex = 109
    Me.gb_status.TabStop = False
    Me.gb_status.Text = "xStatus"
    '
    'lbl_inactive
    '
    Me.lbl_inactive.BackColor = System.Drawing.SystemColors.GrayText
    Me.lbl_inactive.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_inactive.Location = New System.Drawing.Point(10, 63)
    Me.lbl_inactive.Name = "lbl_inactive"
    Me.lbl_inactive.Size = New System.Drawing.Size(16, 16)
    Me.lbl_inactive.TabIndex = 110
    '
    'tf_inactive
    '
    Me.tf_inactive.IsReadOnly = True
    Me.tf_inactive.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_inactive.LabelForeColor = System.Drawing.Color.Black
    Me.tf_inactive.Location = New System.Drawing.Point(4, 59)
    Me.tf_inactive.Name = "tf_inactive"
    Me.tf_inactive.Size = New System.Drawing.Size(116, 24)
    Me.tf_inactive.SufixText = "Sufix Text"
    Me.tf_inactive.SufixTextVisible = True
    Me.tf_inactive.TabIndex = 109
    Me.tf_inactive.TabStop = False
    Me.tf_inactive.TextVisible = False
    Me.tf_inactive.TextWidth = 30
    Me.tf_inactive.Value = "xInactive"
    '
    'lbl_standby
    '
    Me.lbl_standby.BackColor = System.Drawing.SystemColors.GrayText
    Me.lbl_standby.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_standby.Location = New System.Drawing.Point(10, 41)
    Me.lbl_standby.Name = "lbl_standby"
    Me.lbl_standby.Size = New System.Drawing.Size(16, 16)
    Me.lbl_standby.TabIndex = 107
    '
    'tf_standby
    '
    Me.tf_standby.IsReadOnly = True
    Me.tf_standby.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_standby.LabelForeColor = System.Drawing.Color.Black
    Me.tf_standby.Location = New System.Drawing.Point(4, 37)
    Me.tf_standby.Name = "tf_standby"
    Me.tf_standby.Size = New System.Drawing.Size(116, 24)
    Me.tf_standby.SufixText = "Sufix Text"
    Me.tf_standby.SufixTextVisible = True
    Me.tf_standby.TabIndex = 108
    Me.tf_standby.TabStop = False
    Me.tf_standby.TextVisible = False
    Me.tf_standby.TextWidth = 30
    Me.tf_standby.Value = "xStandby"
    '
    'lbl_running
    '
    Me.lbl_running.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_running.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_running.Location = New System.Drawing.Point(10, 19)
    Me.lbl_running.Name = "lbl_running"
    Me.lbl_running.Size = New System.Drawing.Size(16, 16)
    Me.lbl_running.TabIndex = 106
    '
    'tf_running
    '
    Me.tf_running.IsReadOnly = True
    Me.tf_running.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_running.LabelForeColor = System.Drawing.Color.Black
    Me.tf_running.Location = New System.Drawing.Point(4, 15)
    Me.tf_running.Name = "tf_running"
    Me.tf_running.Size = New System.Drawing.Size(114, 24)
    Me.tf_running.SufixText = "Sufix Text"
    Me.tf_running.SufixTextVisible = True
    Me.tf_running.TabIndex = 104
    Me.tf_running.TabStop = False
    Me.tf_running.TextVisible = False
    Me.tf_running.TextWidth = 30
    Me.tf_running.Value = "xRunning"
    '
    'Database_Grid
    '
    Me.Database_Grid.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Database_Grid.CurrentCol = -1
    Me.Database_Grid.CurrentRow = -1
    Me.Database_Grid.Location = New System.Drawing.Point(9, 112)
    Me.Database_Grid.Name = "Database_Grid"
    Me.Database_Grid.PanelRightVisible = False
    Me.Database_Grid.Redraw = True
    Me.Database_Grid.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.Database_Grid.Size = New System.Drawing.Size(590, 70)
    Me.Database_Grid.Sortable = False
    Me.Database_Grid.SortAscending = True
    Me.Database_Grid.SortByCol = 0
    Me.Database_Grid.TabIndex = 110
    Me.Database_Grid.ToolTipped = True
    Me.Database_Grid.TopRow = -2
    '
    'frm_service_monitor
    '
    Me.CancelButton = Me.btn_exit
    Me.ClientSize = New System.Drawing.Size(702, 401)
    Me.Controls.Add(Me.Database_Grid)
    Me.Controls.Add(Me.gb_status)
    Me.Controls.Add(Me.btn_exit)
    Me.Controls.Add(Me.Service_Grid)
    Me.Name = "frm_service_monitor"
    Me.Text = "xfrm_service_monitor"
    Me.gb_status.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents Service_Grid As GUI_Controls.uc_grid

  Private Sub Grid_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Service_Grid.Load

  End Sub
  Friend WithEvents btn_exit As GUI_Controls.uc_button
  Friend WithEvents Timer1 As System.Windows.Forms.Timer
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_inactive As System.Windows.Forms.Label
  Friend WithEvents tf_inactive As GUI_Controls.uc_text_field
  Friend WithEvents lbl_standby As System.Windows.Forms.Label
  Friend WithEvents tf_standby As GUI_Controls.uc_text_field
  Friend WithEvents lbl_running As System.Windows.Forms.Label
  Friend WithEvents tf_running As GUI_Controls.uc_text_field
  Friend WithEvents Database_Grid As GUI_Controls.uc_grid
End Class
