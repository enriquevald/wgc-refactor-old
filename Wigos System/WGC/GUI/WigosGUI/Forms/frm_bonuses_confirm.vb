'-------------------------------------------------------------------
' Copyright © 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_bonuses_confirm.vb
' DESCRIPTION:   Bonuses confirm
' AUTHOR:        Javier Molina Moral
' CREATION DATE: 19-FEB-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 19-FEB-2014  JMM    Initial version
' -------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Data

Public Class frm_bonuses_confirm
  Inherits GUI_Controls.frm_base_edit

#Region " Constants "

#End Region ' Constants

#Region " Members "

  Private m_default_value As frm_bonuses.BONUS_STATUS

#End Region ' Members

#Region " Properties "

  Public Property DefaultValue() As frm_bonuses.BONUS_STATUS
    Get
      Return Me.m_default_value
    End Get
    Set(ByVal value As frm_bonuses.BONUS_STATUS)
      Me.m_default_value = value
    End Set
  End Property

#End Region

#Region " Overrides"

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_BONUSES_CONFIRM

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()
    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4674) '4674 "Confirmación de bonus"

    ' Enable / Disable controls
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = False
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Visible = False

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Visible = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Enabled = True

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Visible = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Enabled = True

    'Bonus group box
    Me.gb_bonus_info.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4675) '4675 "Bonus"

    'Date
    Me.ef_datetime.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(268) '268 "Fecha"

    'Provider
    Me.ef_provider_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(469) '469 "Provider"

    'Terminal
    Me.ef_terminal_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1097) '1097 "Terminal"

    'Transferred amount
    Me.ef_transferred.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3406) '3406 "Transferred"

    'Current status
    Me.ef_current_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4676) '4676 "Current status"

    'To transfer amount
    Me.ef_to_transfer.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3405) '3405 "To transfer"

    ' It looks like redundant code, but it's necessary.
    Me.ef_datetime.IsReadOnly = True
    Me.ef_provider_name.IsReadOnly = True
    Me.ef_terminal_name.IsReadOnly = True
    Me.ef_transferred.IsReadOnly = True
    Me.ef_current_status.IsReadOnly = True
    Me.ef_to_transfer.IsReadOnly = True

    'Status Combo
    Me.cmb_new_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4677) '4677 "Set bonus status as"  

    If Me.DefaultValue = frm_bonuses.BONUS_STATUS.UNKNOWN Then
      Me.cmb_new_status.Add(-1, "")
    End If

    Me.cmb_new_status.Add(frm_bonuses.BONUS_STATUS.CONFIRMED, GLB_NLS_GUI_PLAYER_TRACKING.Id(frm_bonuses.GetStatusNLS(frm_bonuses.BONUS_STATUS.CONFIRMED)))
    Me.cmb_new_status.Add(frm_bonuses.BONUS_STATUS.CANCELED, GLB_NLS_GUI_PLAYER_TRACKING.Id(frm_bonuses.GetStatusNLS(frm_bonuses.BONUS_STATUS.CANCELED)))

    If Me.DefaultValue <> frm_bonuses.BONUS_STATUS.UNKNOWN Then
      Me.cmb_new_status.Value = Me.DefaultValue
    End If

  End Sub 'GUI_InitControls

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    Dim _bonus As CLASS_BONUS
    Dim _bonus_status As frm_bonuses.BONUS_STATUS

    _bonus = DbReadObject

    _bonus_status = frm_bonuses.TranslateWCPStatus(_bonus.CurrentWCPStatus)

    If _bonus_status <> frm_bonuses.BONUS_STATUS.IN_DOUBT Then
      DbReadObject = Nothing
      Me.Close()
    End If

    'Bonus date
    Me.ef_datetime.Value = _bonus.BonusDate

    'Provider
    Me.ef_provider_name.Value = _bonus.ProviderName

    'Terminal
    Me.ef_terminal_name.Value = _bonus.TerminalName

    'Transferido
    Me.ef_transferred.Value = GUI_FormatCurrency(_bonus.Transferred)

    'Current status
    Me.ef_current_status.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(frm_bonuses.GetStatusNLS(_bonus_status))

    'To Transfer
    Me.ef_to_transfer.Value = GUI_FormatCurrency(_bonus.ToTransfer)

  End Sub 'GUI_SetScreenData

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_GetScreenData()
    Dim _bonus As CLASS_BONUS

    _bonus = DbEditedObject

    ' Current Status
    If cmb_new_status.Value = frm_bonuses.BONUS_STATUS.CONFIRMED Then
      _bonus.CurrentWCPStatus = BonusTransferStatus.Confirmed
      _bonus.Transferred = _bonus.ToTransfer
    ElseIf cmb_new_status.Value = frm_bonuses.BONUS_STATUS.CANCELED Then
      _bonus.CurrentWCPStatus = BonusTransferStatus.CanceledTimeout
    End If

  End Sub 'GUI_GetScreenData

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Dim _bonus As CLASS_BONUS
    Dim _bonus_status As frm_bonuses.BONUS_STATUS

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE

        Me.DbEditedObject = New CLASS_BONUS
        Me.DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

        _bonus = DbEditedObject

        _bonus_status = frm_bonuses.TranslateWCPStatus(_bonus.CurrentWCPStatus)

        If _bonus_status <> frm_bonuses.BONUS_STATUS.IN_DOUBT Then
          DbReadObject = Nothing
          DbStatus = ENUM_STATUS.STATUS_ERROR
          Me.Close()
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If
      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_UPDATE
        Call MyBase.GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_UPDATE)
      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)
    End Select

  End Sub 'GUI_DB_Operation

  ' PURPOSE : Validate the data presented on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Select Case cmb_new_status.Value
      Case frm_bonuses.BONUS_STATUS.CONFIRMED, frm_bonuses.BONUS_STATUS.CANCELED
      Case Else
        '4678 "You must select a value for the '%1' field."
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4678), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_PLAYER_TRACKING.GetString(4677))
        Return False
    End Select

    Return True
  End Function 'GUI_IsScreenDataOk()

  ' PURPOSE: Handle Form buttons cliks
  '
  '  PARAMS:
  '     - INPUT: Clicked button object
  '         -  
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)
    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        ' Save without close form
        MyBase.CloseOnOkClick = False
        Call MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_OK)
        MyBase.CloseOnOkClick = True

      Case ENUM_BUTTON.BUTTON_CANCEL
        DbReadObject = Nothing
        Me.Close()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Perform any clean-up , release used memory in image cache
  '
  '  PARAMS:
  '     - INPUT:
  '         -  
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '
  Protected Overrides Sub GUI_Exit()
    Call MyBase.GUI_Exit()
  End Sub ' GUI_Exit

#End Region ' Overrides

#Region " Public Functions "

  Public Sub New()
    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    Me.m_default_value = frm_bonuses.BONUS_STATUS.UNKNOWN
  End Sub

#End Region ' Public Functions

End Class