<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_charts
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.WebBrowser1 = New System.Windows.Forms.WebBrowser
    Me.web_browser = New System.Windows.Forms.WebBrowser
    Me.SuspendLayout()
    '
    'WebBrowser1
    '
    Me.WebBrowser1.Dock = System.Windows.Forms.DockStyle.Fill
    Me.WebBrowser1.Location = New System.Drawing.Point(0, 0)
    Me.WebBrowser1.MinimumSize = New System.Drawing.Size(20, 20)
    Me.WebBrowser1.Name = "WebBrowser1"
    Me.WebBrowser1.Size = New System.Drawing.Size(1424, 752)
    Me.WebBrowser1.TabIndex = 0
    '
    'web_browser
    '
    Me.web_browser.Location = New System.Drawing.Point(0, 0)
    Me.web_browser.MinimumSize = New System.Drawing.Size(20, 20)
    Me.web_browser.Name = "web_browser"
    Me.web_browser.ScriptErrorsSuppressed = True
    Me.web_browser.ScrollBarsEnabled = False
    Me.web_browser.Size = New System.Drawing.Size(1324, 752)
    Me.web_browser.TabIndex = 207
    '
    'frm_charts
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1424, 752)
    Me.Controls.Add(Me.web_browser)
    Me.Controls.Add(Me.WebBrowser1)
    Me.Name = "frm_charts"
    Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
    Me.Text = "frm_charts"
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents WebBrowser1 As System.Windows.Forms.WebBrowser
  Friend WithEvents web_browser As System.Windows.Forms.WebBrowser
End Class
