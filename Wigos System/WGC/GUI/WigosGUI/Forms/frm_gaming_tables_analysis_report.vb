﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
'   MODULE NAME : frm_gaming_tables_analysis_report_sel
'   DESCRIPTION : Table games analysis report
'        AUTHOR : Ervin Olvera
' CREATION DATE : 22-SEP-2017
'
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 26-SEP-2017  EOR    Initial version.
' 07-NOV-2017  EOR    Bug 30623: WIGOS-6203 Report Table games analysis does not work
' 10-NOV-2017  EOR    WIGOS-6482 Table games analysis report: wrong currency symbol
' 16-NOV-2017  EOR    WIGOS-6482 Table games analysis report: wrong currency symbol
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

#Region " Imports"
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports WSI.Common
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports System.Xml
Imports System.Globalization
#End Region


Public Class frm_gaming_tables_analysis_report
  Inherits frm_base_sel

#Region " Enums"

  Private Enum TypeRowTotal
    RowSubTotal = 0
    RowGranTotal = 1
  End Enum

  Private Enum TypeAccumulated
    Monthly = 0
    Yearly = 1
  End Enum

#End Region

#Region " Members"

  Private m_date As DateTime
  Private m_type_acum As TypeAccumulated = TypeAccumulated.Monthly
  Private m_data_table_query As DataTable
  Private m_previous_type_table As Integer
  Private m_previous_type_table_name As String
  Private m_show_subtotals As Boolean
  Private m_types_tables As String
  Private m_types_tables_names As String
  Private m_iso_code As String

#End Region

#Region " Constants"
  'Grid Setup
  Private Const GRID_HEADER_ROWS As Integer = 2
  Private Const GRID_COLUMNS As Integer = 10

  ' Grid Columns
  Private Const GRID_COLUMN_ID As Integer = 0
  Private Const GRID_COLUMN_TABLE_NAME As Integer = 1
  Private Const GRID_COLUMN_DROP_CASHIER As Integer = 2
  Private Const GRID_COLUMN_DROP_GAMING_TABLE As Integer = 3
  Private Const GRID_COLUMN_DROP_TOTAL As Integer = 4
  Private Const GRID_COLUMN_CHIPS_RESULT As Integer = 5
  Private Const GRID_COLUMN_WIN As Integer = 6
  Private Const GRID_COLUMN_HOLD As Integer = 7
  Private Const GRID_COLUMN_WIN_ACUM As Integer = 8
  Private Const GRID_COLUMN_HOLD_ACUM As Integer = 9

  'SQL Columns
  Private Const SQL_COLUMN_TABLE_TYPE As Integer = 0
  Private Const SQL_COLUMN_TABLE_TYPE_NAME As Integer = 1
  Private Const SQL_COLUMN_TABLE_ID As Integer = 2
  Private Const SQL_COLUMN_TABLE_NAME As Integer = 3
  Private Const SQL_COLUMN_DROP_GAMING_TABLE As Integer = 4
  Private Const SQL_COLUMN_DROP_CASHIER As Integer = 5
  Private Const SQL_COLUMN_DROP_TOTAL As Integer = 6
  Private Const SQL_COLUMN_CHIPS_RESULT As Integer = 7
  Private Const SQL_COLUMN_WIN As Integer = 8
  Private Const SQL_COLUMN_HOLD As Integer = 9
  Private Const SQL_COLUMN_WIN_ACUM As Integer = 10
  Private Const SQL_COLUMN_DROP_TOTAL_ACUM As Integer = 11
  Private Const SQL_COLUMN_HOLD_ACUM As Integer = 12

  ' Width Columns
  Private Const GRID_WIDTH_ID As Integer = 200
  Private Const GRID_WIDTH_TABLE_NAME As Integer = 3000
  Private Const GRID_WIDTH_AMOUNT As Integer = 1700

#End Region

#Region " Overrides"

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    Call MyBase.GUI_InitControls()

    'Buttons
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    'Set form Name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8702)

    'Date
    Me.dtp_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1574)
    Me.dtp_date.SetRange(New Date(1999, 1, 1), WSI.Common.Misc.TodayOpening())

    'Accumulated
    Me.lbl_accumulated.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6975)
    Me.opt_acum_monthly.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4424)
    Me.opt_acum_yearly.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4425)

    'Subtotal
    Me.chk_type_table_subtotal.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4578)

    'Tables
    Me.uc_checked_list_table_type.GroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3409)
    Me.uc_checked_list_table_type.ColumnWidth(uc_checked_list.GRID_COLUMN_DESC, 4250)
    Call FillTypeTableFilterGrid()

    'Multicurrency
    Me.uc_multicurrency.GroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7382)
    Me.uc_multicurrency.OpenModeControl = uc_multi_currency_site_sel.OPEN_MODE.OnlyCurrency

    Call GUI_StyleSheet()

    Call SetDefaultValues()
  End Sub 'GUI_InitControls

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_FORM.FORM_TABLE_ANALISYS_REPORT
    Call MyBase.GUI_SetFormId()
  End Sub 'GUI_SetFormId

  ' PURPOSE: Call function to reset Filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub 'GUI_FilterReset

  ' PURPOSE: Process button actions in order to branch to a child screen
  '
  '  PARAMS:
  '     - INPUT:
  '           - ButtonId
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)
    MyBase.GUI_ButtonClick(ButtonId)
  End Sub ' GUI_ButtonClick

  Protected Overrides Function GUI_GetQueryType() As frm_base_sel.ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM
  End Function

  ' PURPOSE : Sets the values of a row after show in datagrid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As CLASS_DB_ROW) As Boolean
    Call MyBase.GUI_SetupRow(RowIndex, DbRow)

    If m_previous_type_table <> 0 And m_previous_type_table <> DbRow.Value(SQL_COLUMN_TABLE_TYPE) And m_show_subtotals Then
      Call SetRowTotalOrSubtotalGrid(TypeRowTotal.RowSubTotal, m_previous_type_table_name, m_previous_type_table)
      Me.Grid.AddRow()
      RowIndex = Me.Grid.NumRows - 1
    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_TABLE_NAME).Value = DbRow.Value(SQL_COLUMN_TABLE_NAME)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DROP_GAMING_TABLE).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_DROP_GAMING_TABLE), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, False)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DROP_CASHIER).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_DROP_CASHIER), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, False)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DROP_TOTAL).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_DROP_TOTAL), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, False)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CHIPS_RESULT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_CHIPS_RESULT), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, False)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_WIN).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_WIN), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, False)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLD).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_HOLD), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & Application.CurrentCulture.NumberFormat.PercentSymbol
    Me.Grid.Cell(RowIndex, GRID_COLUMN_WIN_ACUM).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_WIN_ACUM), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, False)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLD_ACUM).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_HOLD_ACUM), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & Application.CurrentCulture.NumberFormat.PercentSymbol


    m_previous_type_table = DbRow.Value(SQL_COLUMN_TABLE_TYPE)
    m_previous_type_table_name = DbRow.Value(SQL_COLUMN_TABLE_TYPE_NAME)
    Return True
  End Function 'GUI_SetupRow

  ' PURPOSE : Sets initial focus
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS:
  '     - none       
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.dtp_date
  End Sub 'GUI_SetInitialFocus

  ' PURPOSE: Print totalizator row in the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    If m_show_subtotals Then
      Me.Grid.AddRow()
      Call SetRowTotalOrSubtotalGrid(TypeRowTotal.RowSubTotal, m_previous_type_table_name, m_previous_type_table)
    End If

    Me.Grid.AddRow()
    Call SetRowTotalOrSubtotalGrid(TypeRowTotal.RowGranTotal, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1340), 0)
  End Sub ' GUI_AfterLastRow

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()
    m_date = dtp_date.Value
    m_show_subtotals = chk_type_table_subtotal.Checked

    If opt_acum_monthly.Checked Then
      m_type_acum = TypeAccumulated.Monthly
    End If

    If opt_acum_yearly.Checked Then
      m_type_acum = TypeAccumulated.Yearly
    End If

    m_types_tables = Me.uc_checked_list_table_type.SelectedIndexesList()
    m_types_tables_names = Me.uc_checked_list_table_type.SelectedValuesList()

    m_iso_code = Me.uc_multicurrency.SelectedCurrency()
  End Sub 'GUI_ReportUpdateFilters

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1574), GUI_FormatDate(m_date, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS))
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6975), IIf(m_type_acum = TypeAccumulated.Monthly, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4424), GLB_NLS_GUI_PLAYER_TRACKING.GetString(4425)))
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4578), IIf(m_show_subtotals, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)))
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(3409), m_types_tables_names)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7382), m_iso_code)
  End Sub ' GUI_ReportFilter

  Protected Overrides Function GUI_FilterCheck() As Boolean
    Return True
  End Function

  Protected Overrides Sub GUI_ExecuteQueryCustom()
    Dim _sql_command As SqlCommand = New SqlCommand()
    Dim _db_row As CLASS_DB_ROW
    Dim _query_as_datatable As DataTable
    Dim _row As DataRow
    Dim _idx_row As Integer

    Call GUI_StyleSheet()

    _sql_command.CommandText = "GetGamingTableAnalisys"
    _sql_command.CommandType = CommandType.StoredProcedure
    _sql_command.Parameters.Add("@pDate", SqlDbType.DateTime).Value = m_date
    _sql_command.Parameters.Add("@pTypeAcum", SqlDbType.Bit).Value = m_type_acum
    _sql_command.Parameters.Add("@pTypeTables", SqlDbType.VarChar).Value = m_types_tables
    _sql_command.Parameters.Add("@pIsoCode", SqlDbType.VarChar).Value = m_iso_code

    _query_as_datatable = GUI_GetTableUsingCommand(_sql_command, Integer.MaxValue)

    If _query_as_datatable.Rows.Count > 0 Then

      m_previous_type_table = 0
      m_data_table_query = New DataTable()
      m_data_table_query = _query_as_datatable

      For Each _row In _query_as_datatable.Rows()
        Try
          _db_row = New CLASS_DB_ROW(_row)

          Me.Grid.AddRow()
          _idx_row = Me.Grid.NumRows - 1

          If Not GUI_SetupRow(_idx_row, _db_row) Then
            Me.Grid.DeleteRowFast(_idx_row)
          End If
        Catch ex As OutOfMemoryException
          Throw
        Catch exception As Exception
          Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
          Call Trace.WriteLine(exception.ToString())
          Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                                "frm_activity_tables", _
                                "GUI_SetupRow", _
                                exception.Message)
          Exit For
        End Try

        _db_row = Nothing

        If Not GUI_DoEvents(Me.Grid) Then
          Exit Sub
        End If

      Next

      Call GUI_AfterLastRow()
    End If
  End Sub

  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)
    Call MyBase.GUI_ReportParams(ExcelData)
  End Sub ' GUI_ReportParams

#End Region

#Region " Privates"

  Private Sub SetDefaultValues()
    Me.dtp_date.Value = WSI.Common.Misc.TodayOpening().AddDays(-1)
    Me.lbl_hour.Text = dtp_date.Value.ToString("HH:mm")

    Me.opt_acum_monthly.Checked = True

    Me.chk_type_table_subtotal.Checked = True

    Me.uc_checked_list_table_type.SetDefaultValue(True)

    Me.uc_multicurrency.Init()
  End Sub

  Private Sub GUI_StyleSheet()
    With Me.Grid
      ' Initialize Grid
      .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' GRID INDEX
      .Column(GRID_COLUMN_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_ID).Width = GRID_WIDTH_ID
      .Column(GRID_COLUMN_ID).HighLightWhenSelected = False
      .Column(GRID_COLUMN_ID).IsColumnPrintable = False

      ' TABLE NAME 
      .Column(GRID_COLUMN_TABLE_NAME).Header(0).Text = " "
      .Column(GRID_COLUMN_TABLE_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3419)
      .Column(GRID_COLUMN_TABLE_NAME).Width = GRID_WIDTH_TABLE_NAME
      .Column(GRID_COLUMN_TABLE_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' DROP GAMING TABLE
      .Column(GRID_COLUMN_DROP_GAMING_TABLE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6975) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6216).ToLower()
      .Column(GRID_COLUMN_DROP_GAMING_TABLE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7336)
      .Column(GRID_COLUMN_DROP_GAMING_TABLE).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_DROP_GAMING_TABLE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' DROP CASHIER
      .Column(GRID_COLUMN_DROP_CASHIER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6975) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6216).ToLower()
      .Column(GRID_COLUMN_DROP_CASHIER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7337)
      .Column(GRID_COLUMN_DROP_CASHIER).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_DROP_CASHIER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' DROP TOTAL
      .Column(GRID_COLUMN_DROP_TOTAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6975) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6216).ToLower()
      .Column(GRID_COLUMN_DROP_TOTAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3411)
      .Column(GRID_COLUMN_DROP_TOTAL).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_DROP_TOTAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' RESULT CHIPS
      .Column(GRID_COLUMN_CHIPS_RESULT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6975) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6216).ToLower()
      .Column(GRID_COLUMN_CHIPS_RESULT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8701)
      .Column(GRID_COLUMN_CHIPS_RESULT).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_CHIPS_RESULT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' WIN
      .Column(GRID_COLUMN_WIN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6975) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6216).ToLower()
      .Column(GRID_COLUMN_WIN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3412)
      .Column(GRID_COLUMN_WIN).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_WIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' HOLD
      .Column(GRID_COLUMN_HOLD).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6975) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6216).ToLower()
      .Column(GRID_COLUMN_HOLD).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7197) & " %"
      .Column(GRID_COLUMN_HOLD).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_HOLD).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' WIN ACUM 
      .Column(GRID_COLUMN_WIN_ACUM).Header(0).Text = IIf(m_type_acum = TypeAccumulated.Monthly, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8699), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8700))
      .Column(GRID_COLUMN_WIN_ACUM).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3412)
      .Column(GRID_COLUMN_WIN_ACUM).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_WIN_ACUM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' HOLD ACUM
      .Column(GRID_COLUMN_HOLD_ACUM).Header(0).Text = IIf(m_type_acum = TypeAccumulated.Monthly, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8699), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8700))
      .Column(GRID_COLUMN_HOLD_ACUM).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7197) & " %"
      .Column(GRID_COLUMN_HOLD_ACUM).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_HOLD_ACUM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With
  End Sub

  Private Sub SetRowTotalOrSubtotalGrid(ByVal TypeRow As TypeRowTotal, ByVal TextValue As String, ByVal TypeTable As Integer)
    Dim _idx_row As Integer
    Dim _amount As Double
    Dim _amount_drop_total As Double
    Dim _filter As String
    Dim _text_value As String

    _idx_row = Me.Grid.NumRows - 1

    If TypeRow = TypeRowTotal.RowSubTotal Then
      Me.Grid.Row(_idx_row).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLUE_02)
      _filter = "TABLE_TYPE = " & TypeTable.ToString()
      _text_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3409) & " " & TextValue
    Else
      Me.Grid.Row(_idx_row).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
      _filter = ""
      _text_value = TextValue
    End If

    Me.Grid.Cell(_idx_row, GRID_COLUMN_TABLE_NAME).Value = _text_value

    _amount = m_data_table_query.Compute("SUM(DROP_GAMBLING_TABLE)", _filter)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_DROP_GAMING_TABLE).Value = GUI_FormatCurrency(_amount, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, False)

    _amount = m_data_table_query.Compute("SUM(DROP_CASHIER)", _filter)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_DROP_CASHIER).Value = GUI_FormatCurrency(_amount, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, False)

    _amount = m_data_table_query.Compute("SUM(DROP_TOTAL)", _filter)
    _amount_drop_total = _amount
    Me.Grid.Cell(_idx_row, GRID_COLUMN_DROP_TOTAL).Value = GUI_FormatCurrency(_amount, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, False)

    _amount = m_data_table_query.Compute("SUM(CHIPS_RESULT)", _filter)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_CHIPS_RESULT).Value = GUI_FormatCurrency(_amount, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, False)

    _amount = m_data_table_query.Compute("SUM(WIN)", _filter)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_WIN).Value = GUI_FormatCurrency(_amount, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, False)

    If _amount_drop_total = 0 Then
      _amount = 0
    Else
      _amount = (_amount / _amount_drop_total) * 100
    End If
    Me.Grid.Cell(_idx_row, GRID_COLUMN_HOLD).Value = GUI_FormatNumber(_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & Application.CurrentCulture.NumberFormat.PercentSymbol

    _amount = m_data_table_query.Compute("SUM(WIN_ACUM)", _filter)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_WIN_ACUM).Value = GUI_FormatCurrency(_amount, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, False)

    _amount_drop_total = m_data_table_query.Compute("SUM(DROP_TOTAL_ACUM)", _filter)
    If _amount_drop_total = 0 Then
      _amount = 0
    Else
      _amount = (_amount / _amount_drop_total) * 100
    End If
    Me.Grid.Cell(_idx_row, GRID_COLUMN_HOLD_ACUM).Value = GUI_FormatNumber(_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & Application.CurrentCulture.NumberFormat.PercentSymbol
  End Sub

  Private Sub FillTypeTableFilterGrid()
    Dim _data_table_tables_type As DataTable
    Dim _sql As StringBuilder

    _sql = New StringBuilder()
    _sql.AppendLine("SELECT  GTT_GAMING_TABLE_TYPE_ID ")
    _sql.AppendLine("      , GTT_NAME                 ")
    _sql.AppendLine("FROM GAMING_TABLES_TYPES         ")
    _sql.AppendLine("WHERE GTT_ENABLED = 1            ")

    _data_table_tables_type = GUI_GetTableUsingSQL(_sql.ToString(), Integer.MaxValue)

    Me.uc_checked_list_table_type.Clear()
    Me.uc_checked_list_table_type.ReDraw(False)

    Me.uc_checked_list_table_type.Add(_data_table_tables_type, "GTT_GAMING_TABLE_TYPE_ID", "GTT_NAME")

    If Me.uc_checked_list_table_type.Count() > 0 Then
      Call Me.uc_checked_list_table_type.CurrentRow(0)
    End If

    Call Me.uc_checked_list_table_type.ReDraw(True)

  End Sub

#End Region

#Region " Publics"

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)
  End Sub ' ShowForEdit

#End Region

End Class