'-------------------------------------------------------------------
' Copyright © 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_account_block.vb
' DESCRIPTION:   Account block / unblock
' AUTHOR:        Javi Barea
' CREATION DATE: 07-MAY-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 07-MAY-2013  JBP    Initial version.
' 12-JUN-2013  RCI    Fixed Bug #842/#843: Block account: No permission
' 22-OCT-2014  SMN    Added new functionality: if block reason is EXTERNAL_SYSTEM_CANCELED cannot unlock account
' 21-MAY-2015  RCI    Fixed Bug WIG-2375: Don't allow to unlock accounts if they are locked by External PLD.
' 18-SEP-2015  ETP    Fixed Bug 4554: m_account_id must be Int64 for data consistency. (avoid application crash)
' 07-JUN-2016  JBP    PBI 13253:C2GO - Implementación métodos WS Wigos
' 14-JUL-2016  PDM    PBI 15448:Visitas / Recepción: MEJORAS - Cajero & GUI - Nuevo motivo de bloqueo
'--------------------------------------------------------------------

Option Explicit On

#Region " Imports "

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common

#End Region

Public Class frm_account_block
  Inherits frm_base_edit

#Region " Constants "

  Private Const AC_BLOCKED_ID = 0
  Private Const AC_BLOCK_REASON_ID = 1
  Private Const AC_BLOCK_DESCRIPTION_ID = 2
  Private Const AC_HOLDER_NAME_ID = 3

  Private Const BLOCK_ACCOUNT_Y_POSITION_SIZE_BALANCE As Integer = 187

#End Region

#Region " Members "

  Dim m_account_id As Int64
  Private WithEvents lbl_account_header As System.Windows.Forms.Label
  Friend WithEvents txt_block_description As System.Windows.Forms.TextBox
  Private WithEvents lbl_block_reason As System.Windows.Forms.Label
  Friend WithEvents chk_blacklist As System.Windows.Forms.CheckBox
  Dim m_card_data As CardData

#End Region ' Members

#Region " Windows Form Designer generated code "

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  Friend WithEvents txt_reason As System.Windows.Forms.TextBox
  Private WithEvents lbl_block_unblock_description As System.Windows.Forms.Label
  Private WithEvents lbl_unblock_description As System.Windows.Forms.Label

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.txt_reason = New System.Windows.Forms.TextBox()
    Me.lbl_block_unblock_description = New System.Windows.Forms.Label()
    Me.lbl_unblock_description = New System.Windows.Forms.Label()
    Me.lbl_account_header = New System.Windows.Forms.Label()
    Me.txt_block_description = New System.Windows.Forms.TextBox()
    Me.lbl_block_reason = New System.Windows.Forms.Label()
    Me.chk_blacklist = New System.Windows.Forms.CheckBox()
    Me.panel_data.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.chk_blacklist)
    Me.panel_data.Controls.Add(Me.txt_block_description)
    Me.panel_data.Controls.Add(Me.lbl_block_reason)
    Me.panel_data.Controls.Add(Me.lbl_account_header)
    Me.panel_data.Controls.Add(Me.lbl_block_unblock_description)
    Me.panel_data.Controls.Add(Me.lbl_unblock_description)
    Me.panel_data.Controls.Add(Me.txt_reason)
    Me.panel_data.Size = New System.Drawing.Size(555, 509)
    '
    'txt_reason
    '
    Me.txt_reason.Location = New System.Drawing.Point(14, 307)
    Me.txt_reason.MaxLength = 255
    Me.txt_reason.Multiline = True
    Me.txt_reason.Name = "txt_reason"
    Me.txt_reason.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.txt_reason.Size = New System.Drawing.Size(526, 159)
    Me.txt_reason.TabIndex = 4
    '
    'lbl_block_unblock_description
    '
    Me.lbl_block_unblock_description.AutoSize = True
    Me.lbl_block_unblock_description.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold)
    Me.lbl_block_unblock_description.Location = New System.Drawing.Point(11, 288)
    Me.lbl_block_unblock_description.Name = "lbl_block_unblock_description"
    Me.lbl_block_unblock_description.Size = New System.Drawing.Size(223, 13)
    Me.lbl_block_unblock_description.TabIndex = 3
    Me.lbl_block_unblock_description.Text = "xDescriptionBlockUnblockReason"
    '
    'lbl_unblock_description
    '
    Me.lbl_unblock_description.AutoSize = True
    Me.lbl_unblock_description.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold)
    Me.lbl_unblock_description.Location = New System.Drawing.Point(11, 98)
    Me.lbl_unblock_description.Name = "lbl_unblock_description"
    Me.lbl_unblock_description.Size = New System.Drawing.Size(217, 13)
    Me.lbl_unblock_description.TabIndex = 1
    Me.lbl_unblock_description.Text = "xDescriptionBlockUnblockAction"
    '
    'lbl_account_header
    '
    Me.lbl_account_header.Cursor = System.Windows.Forms.Cursors.Default
    Me.lbl_account_header.Font = New System.Drawing.Font("Verdana", 9.25!)
    Me.lbl_account_header.ForeColor = System.Drawing.SystemColors.ControlText
    Me.lbl_account_header.Location = New System.Drawing.Point(11, 9)
    Me.lbl_account_header.Name = "lbl_account_header"
    Me.lbl_account_header.Size = New System.Drawing.Size(529, 37)
    Me.lbl_account_header.TabIndex = 0
    Me.lbl_account_header.Text = "xAccountHeader"
    Me.lbl_account_header.UseMnemonic = False
    '
    'txt_block_description
    '
    Me.txt_block_description.Location = New System.Drawing.Point(14, 115)
    Me.txt_block_description.MaxLength = 255
    Me.txt_block_description.Multiline = True
    Me.txt_block_description.Name = "txt_block_description"
    Me.txt_block_description.ReadOnly = True
    Me.txt_block_description.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.txt_block_description.Size = New System.Drawing.Size(526, 159)
    Me.txt_block_description.TabIndex = 2
    Me.txt_block_description.Text = "xBlockDescription"
    '
    'lbl_block_reason
    '
    Me.lbl_block_reason.Cursor = System.Windows.Forms.Cursors.Default
    Me.lbl_block_reason.Font = New System.Drawing.Font("Verdana", 9.25!)
    Me.lbl_block_reason.ForeColor = System.Drawing.SystemColors.ControlText
    Me.lbl_block_reason.Location = New System.Drawing.Point(11, 46)
    Me.lbl_block_reason.Name = "lbl_block_reason"
    Me.lbl_block_reason.Size = New System.Drawing.Size(529, 52)
    Me.lbl_block_reason.TabIndex = 0
    Me.lbl_block_reason.Text = "xBlockReason"
    Me.lbl_block_reason.UseMnemonic = False
    '
    'chk_blacklist
    '
    Me.chk_blacklist.AutoSize = True
    Me.chk_blacklist.Checked = True
    Me.chk_blacklist.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_blacklist.Location = New System.Drawing.Point(14, 477)
    Me.chk_blacklist.Name = "chk_blacklist"
    Me.chk_blacklist.Size = New System.Drawing.Size(91, 17)
    Me.chk_blacklist.TabIndex = 5
    Me.chk_blacklist.Text = "CheckBox1"
    Me.chk_blacklist.UseVisualStyleBackColor = True
    '
    'frm_account_block
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(657, 515)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_account_block"
    Me.Text = "#5272 "
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Publics "

  Public Sub New(ByVal AccountId As Int64)
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call
    m_account_id = AccountId

  End Sub

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.Display(True)

  End Sub ' ShowForEdit

#End Region

#Region " Overrides "

  ' PURPOSE: Process clicks on data grid (double-clicks) and select button
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)
    Dim _block_reason_found As AccountBlockReason
    Dim _msg_id As Integer

    If Not CardData.DB_CardGetPersonalData(m_card_data.AccountId, m_card_data) Then
      Me.DialogResult = Windows.Forms.DialogResult.Abort
      Me.Close()

      Return
    End If

    Me.DialogResult = Windows.Forms.DialogResult.OK

    If ButtonId = frm_base_edit.ENUM_BUTTON.BUTTON_OK Then
      If Accounts.ExistsBlockReasonInMask(New List(Of AccountBlockReason)(New AccountBlockReason() _
                                                     {AccountBlockReason.EXTERNAL_SYSTEM_CANCELED, AccountBlockReason.EXTERNAL_AML}), _
                                          Accounts.ConvertOldBlockReason(CType(m_card_data.BlockReason, Int32)), _block_reason_found) Then

        _msg_id = 0
        Select Case _block_reason_found
          Case AccountBlockReason.EXTERNAL_SYSTEM_CANCELED
            _msg_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(5648)
          Case AccountBlockReason.EXTERNAL_AML
            _msg_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(6365)
        End Select

        Me.DialogResult = Windows.Forms.DialogResult.Abort

        If _msg_id > 0 Then
          Call NLS_MsgBox(_msg_id, ENUM_MB_TYPE.MB_TYPE_INFO)
        End If
      Else
        If Not BlockUnblockAccount() Then

          ' TODO: Show Error
          Log.Error("Block Button: BlockUnblockAccount. Error block / unblock account.")

          Me.DialogResult = Windows.Forms.DialogResult.Abort

        End If
      End If
    Else
      Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End If

    Me.Close()

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_ACCOUNT_BLOCK

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    Dim _header As String
    Dim _coma As String

    _coma = String.Empty

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    Me.m_card_data = New CardData()
    CardData.DB_CardGetPersonalData(m_account_id, m_card_data)

    ' Set account header
    _header = m_account_id.ToString()

    If Not String.IsNullOrEmpty(m_card_data.PlayerTracking.HolderName) Then
      _header = _header & " - " & m_card_data.PlayerTracking.HolderName
    End If

    If Not m_card_data.Blocked Then

      Dim btn_ok As uc_button

      btn_ok = Me.GUI_Button(ENUM_BUTTON.BUTTON_OK)

      ' - Hide unblock labels
      lbl_unblock_description.Visible = False
      txt_block_description.Visible = False


      ' - Set Sizes and Positions
      Me.Size = New Size(Me.Size.Width, Me.Size.Height - BLOCK_ACCOUNT_Y_POSITION_SIZE_BALANCE)

      Me.lbl_block_unblock_description.Location = New Point(Me.lbl_block_unblock_description.Location.X, Me.lbl_block_unblock_description.Location.Y - BLOCK_ACCOUNT_Y_POSITION_SIZE_BALANCE)
      Me.txt_reason.Location = New Point(Me.txt_reason.Location.X, Me.txt_reason.Location.Y - BLOCK_ACCOUNT_Y_POSITION_SIZE_BALANCE)

      ' 1 - Set Form Title
      ' 2 - Set Label Description
      ' 3 - Set Button Ok text

      Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1950)
      Me.lbl_account_header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1947) & _header
      lbl_block_unblock_description.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1954)
      Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1952)
      lbl_block_reason.Text = WSI.Common.Resource.String("STR_UC_CARD_BLOCK_STATUS_UNLOCKED") & "."

      Me.chk_blacklist.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7410)


      Me.chk_blacklist.Location = New Point(Me.txt_reason.Location.X, Me.txt_reason.Location.Y + Me.txt_reason.Height + 10)

    Else

      ' - Set description text
      Dim _description As String

      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1956)

      If Not String.IsNullOrEmpty(m_card_data.BlockDescription) Then
        _description = m_card_data.BlockDescription
      End If

      ' - Show unblock labels
      lbl_unblock_description.Visible = True
      txt_block_description.Visible = True

      ' 1 - Set Form Title
      ' 2 - Set Label Description
      ' 3 - Set Button Ok text

      Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1951)
      Me.lbl_account_header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1948) & _header
      lbl_block_unblock_description.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1949)
      txt_block_description.Text = _description
      lbl_unblock_description.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1955)
      Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1953)

      Me.chk_blacklist.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7411)

      lbl_block_reason.Text = WSI.Common.Resource.String("STR_UC_CARD_BLOCK_STATUS_LOCKED") & " "
      For Each _block_reason_ As KeyValuePair(Of AccountBlockReason, String) In Accounts.GetBlockReason(CType(Me.m_card_data.BlockReason, Int32))
        lbl_block_reason.Text &= _coma & _block_reason_.Value()
        _coma = ", "
      Next

    End If

    Me.chk_blacklist.Visible = WSI.Common.Misc.IsReceptionEnabled
    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False

  End Sub 'GUI_InitControls


  ' PURPOSE: Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.txt_reason

  End Sub ' GUI_SetInitialFocus

#End Region

#Region " Private Functions "

  ' PURPOSE: Block account data base implementation
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Public Function BlockUnblockAccount() As Boolean

    Dim _card As CardData
    Dim _session_info As CashierSessionInfo
    Dim _account_block_unblock As AccountBlockUnblock
    Try

      _card = New CardData()


      ' It's not a real cashier session. Don't open a cashier session for this movement.
      _session_info = New CashierSessionInfo()
      _session_info.AuthorizedByUserName = CurrentUser.Name
      _session_info.TerminalId = 0
      _session_info.TerminalName = Environment.MachineName
      _session_info.CashierSessionId = 0

      If Me.chk_blacklist.Checked Then

        Using _db_trx As New DB_TRX

          _account_block_unblock = New AccountBlockUnblock(m_card_data, AccountBlockUnblockSource.GUI, txt_reason.Text, True)

          If Not _account_block_unblock.ProceedToBlockUnblockAccount(_session_info, Not m_card_data.Blocked, _db_trx.SqlTransaction, _card) Then

            Return False
          End If

          If WSI.Common.Misc.IsReceptionEnabled() Then
            If _card.Blocked Then
              WSI.Common.Entrances.Customers.AddToBlacklist(m_card_data, _session_info, WSI.Common.Misc.TodayOpening(), WSI.Common.Entrances.Enums.ENUM_BLACKLIST_REASON.BLOCK, txt_reason.Text, _db_trx.SqlTransaction)
            Else
              WSI.Common.Entrances.Customers.RemoveFromBlacklist(m_card_data, _session_info, _db_trx.SqlTransaction)
            End If
          End If


          _db_trx.Commit()

        End Using
      Else

        _account_block_unblock = New AccountBlockUnblock(m_card_data, AccountBlockUnblockSource.GUI, txt_reason.Text, False)

        If Not _account_block_unblock.ProceedToBlockUnblockAccount(_session_info, _card) Then
          Return False
        End If
      End If

      ' update card data with changes
      m_card_data.Blocked = _card.Blocked
      m_card_data.BlockReason = _card.BlockReason
      m_card_data.BlockDescription = _card.BlockDescription

      If Not GUI_AuditorData() Then

        Return False

      End If

      Return True

    Catch ex As Exception
      Log.Exception(ex)
    End Try

    Return False

  End Function ' BlockUnblockAccount

  ' PURPOSE: Block account data base implementation
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Public Function GUI_AuditorData() As Boolean

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _result_audit As Boolean

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_ACCOUNT)
    _result_audit = False


    ' Audit data
    '     - Account
    _auditor_data.SetName(GLB_NLS_GUI_STATISTICS.Id(207), _
                         GLB_NLS_GUI_STATISTICS.GetString(207) & " " & IIf(m_card_data.Blocked, _
                         GLB_NLS_GUI_INVOICING.GetString(238), GLB_NLS_GUI_INVOICING.GetString(239)) & _
                         ": " & m_card_data.AccountId.ToString() & " - " & m_card_data.PlayerTracking.HolderName)

    If _auditor_data.Notify(GLB_CurrentUser.GuiId, _
                              GLB_CurrentUser.Id, _
                              GLB_CurrentUser.Name, _
                              CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, _
                              0) Then

      _result_audit = True

    End If

    If Me.chk_blacklist.Checked And _result_audit = True Then

      _result_audit = False

      _auditor_data.SetName(GLB_NLS_GUI_STATISTICS.Id(207), _
                     GLB_NLS_GUI_STATISTICS.GetString(207) & " " & IIf(m_card_data.Blocked, _
                     GLB_NLS_GUI_INVOICING.GetString(510), GLB_NLS_GUI_INVOICING.GetString(511)) & _
                     ": " & m_card_data.AccountId.ToString() & " - " & m_card_data.PlayerTracking.HolderName)

      If _auditor_data.Notify(GLB_CurrentUser.GuiId, _
                              GLB_CurrentUser.Id, _
                              GLB_CurrentUser.Name, _
                              CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, _
                              0) Then

        _result_audit = True

      End If

    End If

    Return _result_audit

  End Function

#End Region

#Region " Events "

  ' PURPOSE: Ignore [ENTER] key when editing comments
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    If e.KeyChar = Chr(Keys.Enter) And Me.txt_reason.ContainsFocus Then
      Return True
    End If

    Return MyBase.GUI_KeyPress(sender, e)

  End Function ' GUI_KeyPress

#End Region

End Class
