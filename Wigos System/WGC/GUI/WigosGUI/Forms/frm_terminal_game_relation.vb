'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_terminal_game_relation
' DESCRIPTION:   Edit terminal_game_relation
' AUTHOR:        Alberto Cuesta
' CREATION DATE: 11-JAN-2010
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 11-JAN-2010 ACC    Initial draft.
' 19-DEC-2012 ICS    Added columns: Creation Date and Update Date.
' 11-MAR-2013 AMF    Fixed Bug #636: DB error when terminal or game name has an apostrophe.
' 21-JUN-2013 DMR    Added Excel Functionality.
' 26-JUN-2013 DMR    Added UNKNOWN option in the Displayed Game combobox.
' 17-SEP-2013 ANG    Fixed Bug #WIG-208.
' 29-MAY-2014 JAB    Added permissions: To buttons "Excel" and "Print".
' 03-SEP-2014 LEM    Added functionality to show terminal location data.
' 30-SEP-2014 LEM    Fixed Bug WIG-1340: Option "Show terminal location" do not work correctly.
' 27-MAR-2015 ANM    Calling GetProviderIdListSelected always returns a query
' 07-SEP-2016 ETP    Fixed Bug 17390: Relaci�n Terminal-Juego: excepciones no controladas al guardar
' 13-SEP-2016 ETP    Fixed Bug 17608: Excepci�n no controlada al dar de alta un terminal de juego
' 09-NOV-2016 JBP    Bug 20284:Terminales 3GS: Al tratar de guardar el "Juego" de un terminal 3GS salta una excepci�n no controlada
' 01-FEB-2017 FOS    Bug 23945:Not controlled exception in datagrid.
' 20-APR-2017 JML    Fixed bug 26701: WIGOS-1100 - Error on Terminal Game Relation report (v03.005.0037)
' 02-MAY-2017 JML    Fixed bug 26701: WIGOS-1100 - Error on Terminal Game Relation report (v03.005.0037) (reopen)
'-------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_Reports.PrintDataset
Imports System.Data.SqlClient
Imports System.Data
Imports System.Drawing
Imports System.Text
Imports WSI.Common

Public Class frm_terminal_game_relation
  Inherits GUI_Controls.frm_base_print

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents Splitter1 As System.Windows.Forms.Splitter
  Friend WithEvents chk_only_without_translation As System.Windows.Forms.CheckBox
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox
  Friend WithEvents dg_terminal_game As System.Windows.Forms.DataGrid
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.uc_pr_list = New GUI_Controls.uc_provider
    Me.chk_only_without_translation = New System.Windows.Forms.CheckBox
    Me.dg_terminal_game = New System.Windows.Forms.DataGrid
    Me.Splitter1 = New System.Windows.Forms.Splitter
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox
    Me.panel_grids.SuspendLayout()
    Me.panel_filter.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    CType(Me.dg_terminal_game, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'panel_grids
    '
    Me.panel_grids.Controls.Add(Me.dg_terminal_game)
    Me.panel_grids.Controls.Add(Me.Splitter1)
    Me.panel_grids.Location = New System.Drawing.Point(4, 217)
    Me.panel_grids.Size = New System.Drawing.Size(1264, 501)
    Me.panel_grids.Controls.SetChildIndex(Me.panel_buttons, 0)
    Me.panel_grids.Controls.SetChildIndex(Me.Splitter1, 0)
    Me.panel_grids.Controls.SetChildIndex(Me.dg_terminal_game, 0)
    '
    'panel_buttons
    '
    Me.panel_buttons.Location = New System.Drawing.Point(1176, 0)
    Me.panel_buttons.Size = New System.Drawing.Size(88, 501)
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.chk_terminal_location)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Controls.Add(Me.chk_only_without_translation)
    Me.panel_filter.Size = New System.Drawing.Size(1264, 190)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_only_without_translation, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_terminal_location, 0)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Location = New System.Drawing.Point(4, 194)
    Me.pn_separator_line.Size = New System.Drawing.Size(1264, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1264, 4)
    '
    'uc_pr_list
    '
    Me.uc_pr_list.FilterByCurrency = False
    Me.uc_pr_list.Location = New System.Drawing.Point(3, 5)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(338, 188)
    Me.uc_pr_list.TabIndex = 0
    '
    'chk_only_without_translation
    '
    Me.chk_only_without_translation.Location = New System.Drawing.Point(371, 28)
    Me.chk_only_without_translation.Name = "chk_only_without_translation"
    Me.chk_only_without_translation.Size = New System.Drawing.Size(296, 16)
    Me.chk_only_without_translation.TabIndex = 1
    Me.chk_only_without_translation.Text = "xShow only games without translation"
    '
    'dg_terminal_game
    '
    Me.dg_terminal_game.AllowDrop = True
    Me.dg_terminal_game.AllowNavigation = False
    Me.dg_terminal_game.AllowSorting = False
    Me.dg_terminal_game.CaptionFont = New System.Drawing.Font("Verdana", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.dg_terminal_game.CaptionVisible = False
    Me.dg_terminal_game.DataMember = ""
    Me.dg_terminal_game.Dock = System.Windows.Forms.DockStyle.Fill
    Me.dg_terminal_game.HeaderFont = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.dg_terminal_game.HeaderForeColor = System.Drawing.SystemColors.ControlText
    Me.dg_terminal_game.Location = New System.Drawing.Point(0, 8)
    Me.dg_terminal_game.Name = "dg_terminal_game"
    Me.dg_terminal_game.Size = New System.Drawing.Size(1176, 493)
    Me.dg_terminal_game.TabIndex = 8
    '
    'Splitter1
    '
    Me.Splitter1.Dock = System.Windows.Forms.DockStyle.Top
    Me.Splitter1.Location = New System.Drawing.Point(0, 0)
    Me.Splitter1.Name = "Splitter1"
    Me.Splitter1.Size = New System.Drawing.Size(1176, 8)
    Me.Splitter1.TabIndex = 7
    Me.Splitter1.TabStop = False
    '
    'chk_terminal_location
    '
    Me.chk_terminal_location.Location = New System.Drawing.Point(371, 67)
    Me.chk_terminal_location.Name = "chk_terminal_location"
    Me.chk_terminal_location.Size = New System.Drawing.Size(296, 16)
    Me.chk_terminal_location.TabIndex = 2
    Me.chk_terminal_location.Text = "xShow terminals location"
    '
    'frm_terminal_game_relation
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(1272, 722)
    Me.Name = "frm_terminal_game_relation"
    Me.Text = "Terminal-Game Relation"
    Me.panel_grids.ResumeLayout(False)
    Me.panel_filter.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    CType(Me.dg_terminal_game, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Enums "

#End Region

#Region " Constants "

  Private m_terminal_report_type As ReportType = ReportType.Provider
  Private TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(m_terminal_report_type)

  ' Grid Columns
  Public GRID_COLUMN_TE_PROV_ID As Integer = 0
  Public GRID_COLUMN_GAME_ID As Integer = 1
  Public GRID_INIT_TERMINAL_DATA As Integer = 2
  Public GRID_COLUMN_TGT_PAYOUT_IDX As Integer = TERMINAL_DATA_COLUMNS + 2
  Public GRID_COLUMN_SOURCE_GAME_NAME As Integer = TERMINAL_DATA_COLUMNS + 3
  Public GRID_COLUMN_TARGET_GAME_NAME As Integer = TERMINAL_DATA_COLUMNS + 4
  Public GRID_COLUMN_PAYOUT As Integer = TERMINAL_DATA_COLUMNS + 5
  Public GRID_COLUMN_CREATED As Integer = TERMINAL_DATA_COLUMNS + 6
  Public GRID_COLUMN_UPDATED As Integer = TERMINAL_DATA_COLUMNS + 7

  Public GRID_LAST_INDEX As Integer = TERMINAL_DATA_COLUMNS + 8

#End Region

#Region " Members "

  Private m_sql_commandbuilder As SqlCommandBuilder
  Shared m_sql_conn As SqlConnection
  Private m_sql_adap As SqlDataAdapter
  Private m_sql_dataset As DataSet
  Private m_data_view_terminal_game_relation As DataView

  ' For report filters 
  Private m_report_terminals As String
  Private m_report_without_translation As String
  Private m_refresh_grid As Boolean = True

#End Region

#Region " Overrides Functions "

  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_FORM.FORM_TERMINAL_GAME_RELATION

    Call MyBase.GUI_SetFormId()

  End Sub

  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)

    '' Force to grid to lost focus
    '' to allow to detect changes on uncommited cells
    Me.ActiveControl = Nothing

    If DiscardChanges() Then
      CloseCanceled = False
    Else
      CloseCanceled = True
    End If
  End Sub

  Protected Overrides Sub GUI_InitControls()
    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_CONFIGURATION.GetString(468)                        ' 468 "Relaci�n Terminal-Juego"

    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_CONTROLS.GetString(13)

    GUI_Button(ENUM_BUTTON.BUTTON_INFO).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Enabled = False
    GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Enabled = False
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False

    ' Providers - Terminals
    Call Me.uc_pr_list.Init(WSI.Common.Misc.GamingTerminalTypeList())

    ' 473 "Mostrar s�lo juegos reportados sin renombrar"
    Me.chk_only_without_translation.Text = GLB_NLS_GUI_CONFIGURATION.GetString(473)
    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)

    Call SetDefaultValues()

    dg_terminal_game.Enabled = False

  End Sub

  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)

  End Sub 'GUI_Permissions

  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = uc_pr_list

  End Sub 'GUI_SetInitialFocus

#End Region

#Region " Private Functions "

  ' PURPOSE: Manage buttons pressed.
  '
  '  PARAMS:
  '     - INPUT:
  '         - ButtonId: Id. of the button clicked.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        ' sle 09/11/09
        Call GUI_SaveChanges()

      Case ENUM_BUTTON.BUTTON_CUSTOM_1
      Case ENUM_BUTTON.BUTTON_CUSTOM_2

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  Private Sub GUI_SaveChanges()

    Dim data_table_changes_terminal_game As New DataTable
    Dim idx_change As Integer
    Dim terminal_id As Integer
    Dim source_game_id As Integer
    Dim target_game_id As Integer
    Dim game_payout_id As Integer
    Dim reload_games_list As Boolean
    Dim num_saved_changes As Integer
    Dim modified_index As Integer

    reload_games_list = False

    Try
      If Not IsNothing(m_data_view_terminal_game_relation) Then
        Call TrimDataValues(m_sql_dataset.Tables(0).Select("", "", DataViewRowState.ModifiedCurrent Or DataViewRowState.Added), True)
        data_table_changes_terminal_game = m_data_view_terminal_game_relation.Table.GetChanges
      End If

      If IsNothing(data_table_changes_terminal_game) Then
        Exit Sub
      End If

      '
      ' UPDATE 
      '
      num_saved_changes = 0

      For idx_change = 0 To data_table_changes_terminal_game.Rows.Count - 1

        terminal_id = GetTerminalId(data_table_changes_terminal_game.Rows(idx_change).Item("TERMINAL_NAME"))
        source_game_id = GetGameId(data_table_changes_terminal_game.Rows(idx_change).Item("SOURCE_GAME_NAME"))

        'SET -1 for insert DBNULL in PAYOUT
        game_payout_id = IIf(String.IsNullOrEmpty(data_table_changes_terminal_game.Rows(idx_change).Item("TGT_PAYOUT_IDX").ToString()), -1, data_table_changes_terminal_game.Rows(idx_change).Item("TGT_PAYOUT_IDX"))

        If terminal_id = 0 Or source_game_id = 0 Then
          Continue For
        End If

        ' Check whether the game name exists
        target_game_id = IIf(String.IsNullOrEmpty(data_table_changes_terminal_game.Rows(idx_change).Item("GAME_ID").ToString()), -1, data_table_changes_terminal_game.Rows(idx_change).Item("GAME_ID"))

        ' Don't insert a game name at database if it's unknown
        If data_table_changes_terminal_game.Rows(idx_change).Item("TARGET_GAME_NAME").ToString = "UNKNOWN" And target_game_id = 0 Then
          target_game_id = -1
        End If

        If target_game_id = 0 Then
          ' Game name does not exist => A new game should be created using the new name

          ' Ask for confirmation before proceeding
          ' 197 "El nombre de juego %1 introducido para %2 no existe. �Desea crear un nuevo juego con este nombre?"

          If NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(197), _
                        ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        ENUM_MB_BTN.MB_BTN_YES_NO, _
                        ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
                        data_table_changes_terminal_game.Rows(idx_change).Item("TARGET_GAME_NAME").ToString.ToUpper, _
                        data_table_changes_terminal_game.Rows(idx_change).Item("SOURCE_GAME_NAME").ToString) = ENUM_MB_RESULT.MB_RESULT_YES Then
            ' Register (create) a new game using the new name
            target_game_id = RegisterGame(data_table_changes_terminal_game.Rows(idx_change).Item("TARGET_GAME_NAME").ToString.ToUpper)

          Else
            modified_index = data_table_changes_terminal_game.Rows(idx_change)("NUM_ROW")
            m_data_view_terminal_game_relation.Table.Rows(modified_index).RejectChanges()

          End If


          If target_game_id = 0 Then
            ' Skip the game and rollback the name update 
            data_table_changes_terminal_game.Rows(idx_change).Item("TARGET_GAME_NAME") = data_table_changes_terminal_game.Rows(idx_change).Item("TARGET_GAME_NAME_BIS")

            Continue For
          End If

          ' A new game name was created => Game lists should be reloaded
          reload_games_list = True

        End If

        UpdateTerminalGameTranslation(terminal_id, source_game_id, target_game_id, game_payout_id)

        num_saved_changes += 1
      Next

      If num_saved_changes > 0 Then
        ' Auditory
        GUI_WriteAuditoryChanges(True, data_table_changes_terminal_game)

        m_sql_dataset.AcceptChanges()
      End If

      If reload_games_list Then
        Dim combobox_column As DataGridComboBoxColumn
        Dim str_sql As String

        ' A new game name was created => Game lists should be reloaded in the grid's combo
        combobox_column = dg_terminal_game.TableStyles(0).GridColumnStyles("TARGET_GAME_NAME")
        'str_sql = SELECT_GAMES
        str_sql = SELECT_GAMES_V2
        combobox_column.Frozen = True
        combobox_column.ColumnComboBox.DataSource = New DataView(GUI_GetTableUsingSQL(str_sql, 1000))
        combobox_column.Frozen = False
      End If

      If num_saved_changes > 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(108), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO)
      End If

    Catch ex As Exception
      m_sql_dataset.AcceptChanges()
      Debug.WriteLine(ex.Message)
      GUI_ClearGrid()

      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ex.Message)

    Finally
    End Try

  End Sub


  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()

  End Sub 'GUI_FilterReset

  Protected Overrides Function GUI_FilterCheck() As Boolean

    Return True
  End Function 'GUI_FilterCheck

  Private Sub GUI_ClearGrid()
    dg_terminal_game.DataSource = Nothing
  End Sub 'GUI_ClearGrid

  Protected Overrides Sub GUI_ExecuteQuery()
    Dim data_table_changes_terminal_game As New DataTable

    Try
      ' Check the filter
      If GUI_FilterCheck() Then

        'XVV 16/04/2007
        'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
        Windows.Forms.Cursor.Current = Cursors.WaitCursor

        ' If pending changes MsgBox
        If Not IsNothing(m_data_view_terminal_game_relation) Then
          data_table_changes_terminal_game = m_data_view_terminal_game_relation.Table.GetChanges

          If Not IsNothing(data_table_changes_terminal_game) Then
            If data_table_changes_terminal_game.Rows.Count > 0 Then
              If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(122), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then
                Return
              End If
            End If
          End If
        End If

        ' Invalidate datagrid
        GUI_ClearGrid()

        ' Execute the query        
        Call ExecuteQuery()

        ' For report filters
        Call GUI_ReportUpdateFilters(Me.dg_terminal_game)

        GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = (dg_terminal_game.VisibleRowCount > 0) _
                                                      And Me.Permissions.Write

        GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Enabled = dg_terminal_game.VisibleRowCount > 0 And CurrentUser.Permissions(ENUM_FORM.FORM_PRINT_ENABLED).Read
        GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Enabled = dg_terminal_game.VisibleRowCount > 0 And CurrentUser.Permissions(ENUM_FORM.FORM_EXCEL_ENABLED).Read

      End If

    Catch exception As Exception
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "TerminalsEdit", _
                            "FilterApply", _
                            "", _
                            "", _
                            "", _
                            exception.Message)

    Finally

      'XVV 16/04/2007
      'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
      Windows.Forms.Cursor.Current = Cursors.Default
    End Try

  End Sub 'GUI_ExecuteQuery

  Private Sub ExecuteQuery()

    Dim table_style As DataGridTableStyle
    Dim sql_query_terminal_game As StringBuilder
    Dim sql_where_payout_idx As StringBuilder
    Dim combobox_column As DataGridComboBoxColumn
    Dim combobox_column_bis As DataGridComboBoxColumnBis
    Dim text_column As DataGridTextBoxColumn
    Dim str_sql As String
    Dim _terminal_columns As List(Of ColumnSettings)
    Dim _columns_style_created As Boolean

    ' Initialize DataView for database
    Try
      m_sql_conn = Nothing
      m_sql_conn = NewSQLConnection()

      TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(m_terminal_report_type)

      GRID_COLUMN_TGT_PAYOUT_IDX = TERMINAL_DATA_COLUMNS + 2
      GRID_COLUMN_SOURCE_GAME_NAME = TERMINAL_DATA_COLUMNS + 3
      GRID_COLUMN_TARGET_GAME_NAME = TERMINAL_DATA_COLUMNS + 4
      GRID_COLUMN_PAYOUT = TERMINAL_DATA_COLUMNS + 5
      GRID_COLUMN_CREATED = TERMINAL_DATA_COLUMNS + 6
      GRID_COLUMN_UPDATED = TERMINAL_DATA_COLUMNS + 7

      GRID_LAST_INDEX = TERMINAL_DATA_COLUMNS + 8


      If Not (m_sql_conn Is Nothing) Then
        ' Connected
        If m_sql_conn.State <> ConnectionState.Open Then
          ' Connecting ..
          m_sql_conn.Open()
        End If

        If Not m_sql_conn.State = ConnectionState.Open Then
          Exit Try
        End If
      End If

      m_sql_dataset = New DataSet

      ' Build Query -------------------------------------------------
      sql_query_terminal_game = New StringBuilder()
      sql_where_payout_idx = New StringBuilder()

      sql_where_payout_idx.AppendLine("              WHERE (PG_PV_ID = TE_PROV_ID)  ")
      'sql_where_payout_idx.AppendLine("                AND (PG_GAME_ID = TERMINAL_GAME_TRANSLATION.TGT_TARGET_GAME_ID) )")
      sql_where_payout_idx.AppendLine("                AND (PG_GAME_ID = TERMINAL_GAME_TRANSLATION.TGT_TRANSLATED_GAME_ID) )")

      'sql_query_terminal_game.AppendLine(GetOldQuery())

      sql_query_terminal_game.AppendLine(" SELECT   TE_PROV_ID                                                        ")
      'sql_query_terminal_game.AppendLine("        , TGT_TARGET_GAME_ID    GAME_ID                                                           ")
      sql_query_terminal_game.AppendLine("        , ISNULL(TGT_TRANSLATED_GAME_ID,-1)    GAME_ID                                 ")
      sql_query_terminal_game.AppendLine("        , TE_PROVIDER_ID        PROVIDER_ID                                 ")
      sql_query_terminal_game.AppendLine("        , ISNULL(TGT_PAYOUT_IDX, -1)      TGT_PAYOUT_IDX                            ")
      sql_query_terminal_game.AppendLine("        , TE_NAME						 TERMINAL_NAME                                  ")
      sql_query_terminal_game.AppendLine("        , GAMES.GM_NAME				 SOURCE_GAME_NAME                             ")
      sql_query_terminal_game.AppendLine("        , ISNULL (PROVIDERS_GAMES.PG_GAME_NAME, 'UNKNOWN')  TARGET_GAME_NAME   ")
      sql_query_terminal_game.AppendLine("        , GAMES.GM_NAME				 TARGET_GAME_NAME_BIS                         ")
      sql_query_terminal_game.AppendLine("        , TGT_CREATED					 CREATED                                      ")
      sql_query_terminal_game.AppendLine("        , TGT_UPDATED					 UPDATED                                      ")
      sql_query_terminal_game.AppendLine("        , 0							 NUM_ROW                                            ")
      sql_query_terminal_game.AppendLine("      , CASE TGT_PAYOUT_IDX                  ")
      sql_query_terminal_game.AppendLine("          WHEN 1 THEN                        ")
      sql_query_terminal_game.AppendLine("            (SELECT  PG_PAYOUT_1             ")
      sql_query_terminal_game.AppendLine("               FROM PROVIDERS_GAMES        ")
      sql_query_terminal_game.AppendLine(sql_where_payout_idx.ToString())
      sql_query_terminal_game.AppendLine("          WHEN 2 THEN                        ")
      sql_query_terminal_game.AppendLine("            (SELECT  PG_PAYOUT_2             ")
      sql_query_terminal_game.AppendLine("               FROM PROVIDERS_GAMES        ")
      sql_query_terminal_game.AppendLine(sql_where_payout_idx.ToString())
      sql_query_terminal_game.AppendLine("          WHEN 3 THEN                        ")
      sql_query_terminal_game.AppendLine("            (SELECT  PG_PAYOUT_3             ")
      sql_query_terminal_game.AppendLine("               FROM PROVIDERS_GAMES        ")
      sql_query_terminal_game.AppendLine(sql_where_payout_idx.ToString())
      sql_query_terminal_game.AppendLine("          WHEN 4 THEN                        ")
      sql_query_terminal_game.AppendLine("            (SELECT  PG_PAYOUT_4             ")
      sql_query_terminal_game.AppendLine("               FROM PROVIDERS_GAMES        ")
      sql_query_terminal_game.AppendLine(sql_where_payout_idx.ToString())
      sql_query_terminal_game.AppendLine("          WHEN 5 THEN                        ")
      sql_query_terminal_game.AppendLine("            (SELECT  PG_PAYOUT_5             ")
      sql_query_terminal_game.AppendLine("               FROM PROVIDERS_GAMES        ")
      sql_query_terminal_game.AppendLine(sql_where_payout_idx.ToString())
      sql_query_terminal_game.AppendLine("          WHEN 6 THEN                        ")
      sql_query_terminal_game.AppendLine("            (SELECT  PG_PAYOUT_6             ")
      sql_query_terminal_game.AppendLine("               FROM PROVIDERS_GAMES        ")
      sql_query_terminal_game.AppendLine(sql_where_payout_idx.ToString())
      sql_query_terminal_game.AppendLine("          WHEN 7 THEN                        ")
      sql_query_terminal_game.AppendLine("            (SELECT  PG_PAYOUT_7             ")
      sql_query_terminal_game.AppendLine("               FROM PROVIDERS_GAMES        ")
      sql_query_terminal_game.AppendLine(sql_where_payout_idx.ToString())
      sql_query_terminal_game.AppendLine("          WHEN 8 THEN                        ")
      sql_query_terminal_game.AppendLine("            (SELECT  PG_PAYOUT_8             ")
      sql_query_terminal_game.AppendLine("               FROM PROVIDERS_GAMES        ")
      sql_query_terminal_game.AppendLine(sql_where_payout_idx.ToString())
      sql_query_terminal_game.AppendLine("          WHEN 9 THEN                        ")
      sql_query_terminal_game.AppendLine("            (SELECT  PG_PAYOUT_9             ")
      sql_query_terminal_game.AppendLine("               FROM PROVIDERS_GAMES        ")
      sql_query_terminal_game.AppendLine(sql_where_payout_idx.ToString())
      sql_query_terminal_game.AppendLine("          WHEN 10 THEN                       ")
      sql_query_terminal_game.AppendLine("            (SELECT  PG_PAYOUT_10            ")
      sql_query_terminal_game.AppendLine("               FROM PROVIDERS_GAMES        ")
      sql_query_terminal_game.AppendLine(sql_where_payout_idx.ToString())
      sql_query_terminal_game.AppendLine("          ELSE ' '                           ")
      sql_query_terminal_game.AppendLine("        END AS PAYOUT                        ")
      sql_query_terminal_game.AppendLine("   , TGT_TERMINAL_ID AS TE_TERMINAL_ID ")
      sql_query_terminal_game.AppendLine("   FROM      TERMINAL_GAME_TRANSLATION                                                          ")

      sql_query_terminal_game.AppendLine("   LEFT JOIN TERMINALS ON TE_TERMINAL_ID = TGT_TERMINAL_ID                                      ")
      sql_query_terminal_game.AppendLine("   LEFT JOIN GAMES ON GM_GAME_ID = TGT_SOURCE_GAME_ID                                           ")
      sql_query_terminal_game.AppendLine("   LEFT JOIN PROVIDERS_GAMES ON PG_PV_ID = TE_PROV_ID AND PG_GAME_ID = TGT_TRANSLATED_GAME_ID    ")
      sql_query_terminal_game.AppendLine("  WHERE TE_TYPE            = 1  															                                   ")

      ' Filters
      If Me.chk_only_without_translation.Checked Then
        sql_query_terminal_game.AppendLine(" AND TGT_TRANSLATED_GAME_ID IS NULL")
      End If

      sql_query_terminal_game.AppendLine(" AND TGT_TERMINAL_ID IN " & Me.uc_pr_list.GetProviderIdListSelected())

      ' Order by
      sql_query_terminal_game.AppendLine(" ORDER BY PROVIDER_ID, TERMINAL_NAME, SOURCE_GAME_NAME ")

      If Not IsNothing(m_sql_adap) Then
        m_sql_adap.Dispose()
        m_sql_adap = Nothing
      End If

      m_sql_adap = New SqlDataAdapter(sql_query_terminal_game.ToString(), m_sql_conn)
      m_sql_adap.FillSchema(m_sql_dataset, SchemaType.Source)
      m_sql_adap.Fill(m_sql_dataset)

      'hacer un ciclo para poner los indices al datatable

      TerminalReport.GetReportDataTable(m_sql_dataset.Tables(0), m_terminal_report_type)

      m_sql_dataset.Tables(0).Columns("NUM_ROW").ReadOnly = False

      For _idx As Integer = 0 To m_sql_dataset.Tables(0).Rows.Count - 1
        m_sql_dataset.Tables(0).Rows(_idx)("NUM_ROW") = _idx
      Next

      m_sql_dataset.Tables(0).AcceptChanges()

      ' Default Values for Table Servers
      m_sql_dataset.Tables(0).Columns("PROVIDER_ID").ReadOnly = True
      m_sql_dataset.Tables(0).Columns("TERMINAL_NAME").ReadOnly = True
      m_sql_dataset.Tables(0).Columns("SOURCE_GAME_NAME").ReadOnly = True

      m_sql_dataset.Tables(0).Columns("CREATED").ReadOnly = True
      m_sql_dataset.Tables(0).Columns("UPDATED").ReadOnly = True
      m_sql_dataset.Tables(0).Columns("PAYOUT").ReadOnly = False
      m_sql_dataset.Tables(0).Columns("TGT_PAYOUT_IDX").ReadOnly = False
      m_sql_dataset.Tables(0).Columns("TARGET_GAME_NAME").ReadOnly = False
      m_sql_dataset.Tables(0).Columns("GAME_ID").ReadOnly = False

      m_sql_commandbuilder = New SqlCommandBuilder(m_sql_adap)

      m_data_view_terminal_game_relation = New DataView(m_sql_dataset.Tables(0))
      m_data_view_terminal_game_relation.AllowNew = False
      m_data_view_terminal_game_relation.AllowDelete = False
      m_data_view_terminal_game_relation.Table.CaseSensitive = False

      dg_terminal_game.Enabled = True
      dg_terminal_game.DataSource = m_data_view_terminal_game_relation
      dg_terminal_game.AllowNavigation = False
      dg_terminal_game.CaptionVisible = False

      ' Delete Permissions
      dg_terminal_game.AllowDrop = False

      ' Columns Style
      _columns_style_created = Not IsNothing(dg_terminal_game.TableStyles.Item(m_data_view_terminal_game_relation.Table.TableName))
      If Not _columns_style_created OrElse m_refresh_grid Then

        table_style = New DataGridTableStyle
        table_style.AllowSorting = False
        table_style.MappingName = m_data_view_terminal_game_relation.Table.TableName

        For i As Integer = 0 To GRID_LAST_INDEX
          Select Case i
            Case GRID_COLUMN_TE_PROV_ID
              text_column = New DataGridTextBoxColumn
              text_column.MappingName = "TE_PROV_ID"
              text_column.ReadOnly = True
              text_column.Width = 0
              table_style.GridColumnStyles.Add(text_column)

            Case GRID_COLUMN_GAME_ID
              text_column = New DataGridTextBoxColumn
              text_column.MappingName = "GAME_ID"
              text_column.ReadOnly = True
              text_column.Width = 0
              table_style.GridColumnStyles.Add(text_column)

            Case GRID_INIT_TERMINAL_DATA
              _terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)
              For Each _column As ColumnSettings In _terminal_columns
                text_column = New DataGridTextBoxColumn
                text_column.MappingName = _column.MappingName
                text_column.ReadOnly = True
                text_column.HeaderText = _column.Header0
                text_column.Width = _column.Width / 15
                table_style.GridColumnStyles.Add(text_column)
              Next

            Case GRID_COLUMN_TGT_PAYOUT_IDX
              text_column = New DataGridTextBoxColumn
              text_column.MappingName = "TGT_PAYOUT_IDX"
              text_column.ReadOnly = True
              text_column.Width = 0
              table_style.GridColumnStyles.Add(text_column)


            Case GRID_COLUMN_SOURCE_GAME_NAME
              text_column = New DataGridTextBoxColumn
              text_column.MappingName = "SOURCE_GAME_NAME"
              text_column.ReadOnly = True
              text_column.HeaderText = GLB_NLS_GUI_CONFIGURATION.GetString(471)       ' 471 "Juego Reportado"
              text_column.Width = 230
              table_style.GridColumnStyles.Add(text_column)

            Case GRID_COLUMN_TARGET_GAME_NAME
              str_sql = SELECT_GAMES_V2
              combobox_column = New DataGridComboBoxColumn()
              combobox_column.MappingName = "TARGET_GAME_NAME"
              combobox_column.ReadOnly = True
              combobox_column.HeaderText = GLB_NLS_GUI_CONFIGURATION.GetString(472)   ' "Juego a Mostrar"
              combobox_column.Width = 230
              combobox_column.ColumnComboBox.DataSource = New DataView(GUI_GetTableUsingSQL(str_sql, 1000))
              combobox_column.ColumnComboBox.DisplayMember = "TARGET_GAME_NAME"
              combobox_column.ColumnComboBox.ValueMember = "TARGET_GAME_NAME"

              table_style.PreferredRowHeight = combobox_column.ColumnComboBox.Height
              table_style.GridColumnStyles.Add(combobox_column)

            Case GRID_COLUMN_CREATED
              ' Date Created
              text_column = New DataGridTextBoxColumn()
              text_column.MappingName = "CREATED"
              text_column.ReadOnly = True
              text_column.HeaderText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(455)
              text_column.Width = 140
              text_column.Format = GUI_PatternDate(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
              table_style.GridColumnStyles.Add(text_column)

            Case GRID_COLUMN_UPDATED
              ' Date Updated
              text_column = New DataGridTextBoxColumn()
              text_column.MappingName = "UPDATED"
              text_column.ReadOnly = True
              text_column.HeaderText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(456)
              text_column.Width = 155
              text_column.Format = GUI_PatternDate(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
              table_style.GridColumnStyles.Add(text_column)

            Case GRID_COLUMN_PAYOUT
              ' Payout
              combobox_column_bis = New DataGridComboBoxColumnBis()
              combobox_column_bis.MappingName = "PAYOUT"
              combobox_column_bis.ReadOnly = False
              combobox_column_bis.HeaderText = GLB_NLS_GUI_STATISTICS.GetString(303)       ' 303 "Payout %"
              combobox_column_bis.Width = 75
              combobox_column_bis.ColumnComboBoxBis.DisplayMember = "PAYOUT"
              combobox_column_bis.Format = "0.00'%'"
              combobox_column_bis.NullText = "0.00%"
              table_style.PreferredRowHeight = combobox_column_bis.ColumnComboBoxBis.Height
              table_style.GridColumnStyles.Add(combobox_column_bis)

          End Select
        Next

        If _columns_style_created Then
          dg_terminal_game.TableStyles.Remove(dg_terminal_game.TableStyles.Item(table_style.MappingName))
        End If

        dg_terminal_game.TableStyles.Add(table_style)
      Else
        ' Refresh Combobox values
        combobox_column = dg_terminal_game.TableStyles(0).GridColumnStyles("TARGET_GAME_NAME")
        'str_sql = SELECT_GAMES
        str_sql = SELECT_GAMES_V2
        combobox_column.Frozen = True
        combobox_column.ColumnComboBox.DataSource = New DataView(GUI_GetTableUsingSQL(str_sql, 1000))
        combobox_column.Frozen = False

      End If

      m_refresh_grid = False

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)

    Finally
    End Try

  End Sub

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Me.chk_only_without_translation.Checked = False

    Call Me.uc_pr_list.SetDefaultValues()

    Me.chk_terminal_location.Checked = False

  End Sub 'SetDefaultValues

  Private Function DiscardChanges() As Boolean
    Dim data_table_changes_terminal_game As New DataTable

    If Not IsNothing(m_data_view_terminal_game_relation) Then
      data_table_changes_terminal_game = m_data_view_terminal_game_relation.Table.GetChanges

      If Not IsNothing(data_table_changes_terminal_game) Then
        If data_table_changes_terminal_game.Rows.Count > 0 Then
          If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then
            Return False
          End If
        End If
      End If
    End If

    Return True
  End Function

  Private Sub GUI_WriteAuditoryChanges(ByVal Server As Boolean, ByVal ChangesDT As DataTable)

    Dim original_modified_dv As DataView
    Dim auditor_data As CLASS_AUDITOR_DATA
    Dim original_auditor_data As CLASS_AUDITOR_DATA
    Dim idx_change As Integer
    Dim idx_original As Integer
    Dim str_value As StringBuilder

    If IsNothing(ChangesDT) Then
      Exit Sub
    End If

    original_modified_dv = New DataView(ChangesDT, "", "", DataViewRowState.ModifiedOriginal)

    '---------------------------------------------
    ' Insert MODIFIED items
    '---------------------------------------------
    auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINAL_GAME_RELATION)
    original_auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINAL_GAME_RELATION)

    ' Set Parameter
    Call auditor_data.SetName(GLB_NLS_GUI_AUDITOR.Id(73), " ")
    Call original_auditor_data.SetName(GLB_NLS_GUI_AUDITOR.Id(73), " ")

    For idx_change = 0 To ChangesDT.Rows.Count - 1

      '
      ' Details for current rows
      '

      str_value = New StringBuilder()

      str_value.Append(IIf(IsDBNull(ChangesDT.Rows(idx_change).Item("PROVIDER_ID")), AUDIT_NONE_STRING, ChangesDT.Rows(idx_change).Item("PROVIDER_ID") & "."))
      str_value.Append(IIf(IsDBNull(ChangesDT.Rows(idx_change).Item("TERMINAL_NAME")), AUDIT_NONE_STRING, ChangesDT.Rows(idx_change).Item("TERMINAL_NAME") & " "))
      str_value.Append(IIf(IsDBNull(ChangesDT.Rows(idx_change).Item("TARGET_GAME_NAME")), "UNKNOWN", ChangesDT.Rows(idx_change).Item("TARGET_GAME_NAME")))
      str_value.AppendLine(IIf(IsDBNull(ChangesDT.Rows(idx_change).Item("PAYOUT")), AUDIT_NONE_STRING, "[" & ChangesDT.Rows(idx_change).Item("PAYOUT") & "]"))
      Call auditor_data.SetField(0, str_value.ToString())


      ' Search original row
      For idx_original = 0 To original_modified_dv.Count - 1
        If original_modified_dv(idx_original).Item("TERMINAL_NAME") = ChangesDT.Rows(idx_change).Item("TERMINAL_NAME") _
          And original_modified_dv(idx_original).Item("SOURCE_GAME_NAME") = ChangesDT.Rows(idx_change).Item("SOURCE_GAME_NAME") Then
          Exit For
        End If
      Next

      ' Critical Error: exit
      If idx_original = original_modified_dv.Count Then
        ' Logger message 

        Exit Sub
      End If

      '
      ' Details for original rows
      '

      str_value.Length = 0

      str_value.Append(IIf(IsDBNull(original_modified_dv(idx_original).Item("PROVIDER_ID")), AUDIT_NONE_STRING, original_modified_dv(idx_original).Item("PROVIDER_ID") & "."))
      str_value.Append(IIf(IsDBNull(original_modified_dv(idx_original).Item("TERMINAL_NAME")), AUDIT_NONE_STRING, original_modified_dv(idx_original).Item("TERMINAL_NAME") & " "))
      str_value.Append(IIf(IsDBNull(original_modified_dv(idx_original).Item("TARGET_GAME_NAME")), AUDIT_NONE_STRING, original_modified_dv(idx_original).Item("TARGET_GAME_NAME")))
      str_value.Append(IIf(IsDBNull(original_modified_dv(idx_original).Item("PAYOUT")), AUDIT_NONE_STRING, "[" & original_modified_dv(idx_original).Item("PAYOUT") & "]"))
      Call original_auditor_data.SetField(0, str_value.ToString())

    Next

    If Not auditor_data.Notify(GLB_CurrentUser.GuiId, _
                               GLB_CurrentUser.Id, _
                               GLB_CurrentUser.Name, _
                               CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.UPDATE, _
                               0, _
                               original_auditor_data) Then
      ' Logger message 
    End If

    auditor_data = Nothing
    original_auditor_data = Nothing

  End Sub

  ' PURPOSE: Get Terminal Id from Terminal Name
  '
  '  PARAMS:
  '     - INPUT:
  '           - Terminal Name
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - Terminal Id
  '
  Private Function GetTerminalId(ByVal TerminalName As String) As Integer
    Dim _str_sql As String
    Dim _obj As Object

    Try
      Using _db_trx As New DB_TRX()
        _str_sql = "SELECT TE_TERMINAL_ID FROM TERMINALS WHERE TE_TYPE = 1 AND TE_NAME = @pTerminalName "
        Using _cmd As New SqlCommand(_str_sql)
          _cmd.Parameters.Add("@pTerminalName", SqlDbType.NVarChar).Value = TerminalName
          _obj = _db_trx.ExecuteScalar(_cmd)

          If _obj IsNot Nothing AndAlso _obj IsNot DBNull.Value Then
            Return CInt(_obj)
          End If

        End Using
      End Using

    Catch _ex As Exception
    End Try

    Return 0
  End Function ' GetTerminalId

  ' PURPOSE: Get Game Id from Game Name
  '
  '  PARAMS:
  '     - INPUT:
  '           - Game Name
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - Game Id
  '
  Private Function GetGameId(ByVal GameName As String) As Integer
    Dim _str_sql As String
    Dim _obj As Object

    Try
      Using _db_trx As New DB_TRX()
        _str_sql = "SELECT GM_GAME_ID FROM GAMES WHERE GM_NAME = @pGameName "
        Using _cmd As New SqlCommand(_str_sql)
          _cmd.Parameters.Add("@pGameName", SqlDbType.NVarChar).Value = GameName
          _obj = _db_trx.ExecuteScalar(_cmd)

          If _obj IsNot Nothing AndAlso _obj IsNot DBNull.Value Then
            Return CInt(_obj)
          End If

        End Using
      End Using

    Catch _ex As Exception
    End Try

    Return 0
  End Function ' GetGameId


  Private Function GetGameIdFromProvider(ByVal ProviderId As Integer, ByVal Gamename As String) As Integer
    Dim _str_sql As StringBuilder
    Dim _obj As Object

    _str_sql = New StringBuilder()

    Try
      Using _db_trx As New DB_TRX()
        _str_sql.AppendLine(" SELECT PG_GAME_ID ")
        _str_sql.AppendLine("   FROM PROVIDERS_GAMES")
        _str_sql.AppendLine("  WHERE PG_GAME_NAME = @pGameName ")
        _str_sql.AppendLine("    AND PG_PV_ID = @pProviderId ")
        Using _cmd As New SqlCommand(_str_sql.ToString)
          _cmd.Parameters.Add("@pGameName", SqlDbType.NVarChar).Value = Gamename
          _cmd.Parameters.Add("@pProviderId", SqlDbType.BigInt).Value = ProviderId
          _obj = _db_trx.ExecuteScalar(_cmd)

          If _obj IsNot Nothing AndAlso _obj IsNot DBNull.Value Then
            Return CInt(_obj)
          End If

        End Using
      End Using

    Catch _ex As Exception
    End Try

    Return 0

  End Function

  ' PURPOSE: Register New Game with Game Name
  '
  '  PARAMS:
  '     - INPUT:
  '           - Game Name
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Game Id

  Private Function RegisterGame(ByVal GameName As String) As Integer

    Dim _str_sql As String
    Dim SqlTrans As System.Data.SqlClient.SqlTransaction = Nothing
    Dim commit_trx As Boolean
    Dim sql_command As SqlCommand
    Dim rc As Integer
    Dim _obj As Object

    Try

      If Not GUI_BeginSQLTransaction(SqlTrans) Then
        Exit Function
      End If

      commit_trx = False

      _str_sql = "INSERT INTO GAMES ( GM_NAME ) VALUES ( @p1 )"

      sql_command = New SqlCommand(_str_sql)
      sql_command.Connection = SqlTrans.Connection
      sql_command.Transaction = SqlTrans

      sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 50).Value = GameName

      rc = sql_command.ExecuteNonQuery()

      If rc = 1 Then
        commit_trx = True
      End If

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)

    Finally
      ' Either commit or rollback
      If Not (SqlTrans.Connection Is Nothing) Then
        If commit_trx Then
          SqlTrans.Commit()
        Else
          SqlTrans.Rollback()
        End If
      End If

      SqlTrans.Dispose()
      SqlTrans = Nothing

    End Try

    Try
      Using _db_trx As New DB_TRX()
        _str_sql = "SELECT GM_GAME_ID FROM GAMES WHERE GM_NAME = @pGameName "
        Using _cmd As New SqlCommand(_str_sql)
          _cmd.Parameters.Add("@pGameName", SqlDbType.NVarChar).Value = GameName
          _obj = _db_trx.ExecuteScalar(_cmd)

          If _obj IsNot Nothing AndAlso _obj IsNot DBNull.Value Then
            Return CInt(_obj)
          End If

        End Using
      End Using

    Catch _ex As Exception
    End Try

    Return 0
  End Function ' RegisterGame

  ' PURPOSE: Update Terminal Game Translation
  '
  '  PARAMS:
  '     - INPUT:
  '           - TerminalId
  '           - SourceGameId
  '           - TargetGameId
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '
  Private Sub UpdateTerminalGameTranslation(ByVal TerminalId As Integer, ByVal SourceGameId As Integer, ByVal TargetGameId As Integer, ByVal GamePayoutId As Integer)
    Dim str_sql As String
    Dim SqlTrans As System.Data.SqlClient.SqlTransaction = Nothing
    Dim commit_trx As Boolean
    Dim sql_command As SqlCommand
    Dim rc As Integer
    Dim idx As Integer = 0

    Try

      If Not GUI_BeginSQLTransaction(SqlTrans) Then
        Exit Sub
      End If

      commit_trx = False

      str_sql = " UPDATE TERMINAL_GAME_TRANSLATION " + _
                "    SET TGT_TRANSLATED_GAME_ID = @p1 " + _
                "      , TGT_UPDATED        = GETDATE() " + _
                "      , TGT_PAYOUT_IDX     = @p4 " + _
                "  WHERE TGT_TERMINAL_ID    = @p2 " + _
                "    AND TGT_SOURCE_GAME_ID = @p3 "

      sql_command = New SqlCommand(str_sql)
      sql_command.Connection = SqlTrans.Connection
      sql_command.Transaction = SqlTrans

      If (TargetGameId = -1) Then
        sql_command.Parameters.Add("@p1", SqlDbType.Int).Value = DBNull.Value
      Else
        sql_command.Parameters.Add("@p1", SqlDbType.Int).Value = TargetGameId
      End If

      sql_command.Parameters.Add("@p2", SqlDbType.Int).Value = TerminalId
      sql_command.Parameters.Add("@p3", SqlDbType.Int).Value = SourceGameId
      If (GamePayoutId = -1) Then
        sql_command.Parameters.Add("@p4", SqlDbType.Int).Value = DBNull.Value
      Else
        sql_command.Parameters.Add("@p4", SqlDbType.Int).Value = GamePayoutId
      End If
      rc = sql_command.ExecuteNonQuery()

      If rc = 1 Then
        commit_trx = True
      End If

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)

    Finally
      ' Either commit or rollback
      If Not (SqlTrans.Connection Is Nothing) Then
        If commit_trx Then
          SqlTrans.Commit()
        Else
          SqlTrans.Rollback()
        End If
      End If

      SqlTrans.Dispose()
      SqlTrans = Nothing

    End Try

  End Sub ' UpdateTerminalGameTranslation

#End Region

#Region " Public functions "
  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region

#Region " Options Events "

  Private Sub chk_terminal_location_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminal_location.CheckedChanged
    If chk_terminal_location.Checked Then
      m_terminal_report_type = ReportType.Provider + ReportType.Location
    Else
      m_terminal_report_type = ReportType.Provider
    End If

    m_refresh_grid = True
  End Sub

#End Region

#Region " DataGrid Events "

  Private Sub dg_terminal_game_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dg_terminal_game.Enter
    Me.CancelButton = Nothing
  End Sub

  Private Sub dg_terminal_game_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dg_terminal_game.Leave
    Me.CancelButton = GUI_Button(ENUM_BUTTON.BUTTON_CANCEL)
  End Sub

#End Region

#Region " GUI Reports "

  ' PURPOSE: Initialize report filters.
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData As GUI_Reports.CLASS_PRINT_DATA
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_report_terminals)
    PrintData.SetFilter(GLB_NLS_GUI_CONFIGURATION.GetString(473), m_report_without_translation)
    PrintData.FilterValueWidth(1) = 3000
    PrintData.FilterHeaderWidth(2) = 4000

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Update report filters.
  '
  '  PARAMS:
  '     - INPUT:
  '           - MyDataGrid As DataGrid
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters(ByVal MyDataGrid As DataGrid)

    Call MyBase.GUI_ReportUpdateFilters(MyDataGrid)

    If Me.chk_only_without_translation.Checked Then
      m_report_without_translation = GLB_NLS_GUI_AUDITOR.GetString(336)
    Else
      m_report_without_translation = GLB_NLS_GUI_AUDITOR.GetString(337)
    End If

    ' Providers - Terminals
    m_report_terminals = Me.uc_pr_list.GetTerminalReportText()

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#Region "OldQuerys"
  Private Sub GetOldQuery()
    'sql_query_terminal_game.AppendLine(" SELECT TE_PROV_ID                                                           ")
    'sql_query_terminal_game.AppendLine("      , TERMINAL_GAME_TRANSLATION.TGT_TARGET_GAME_ID    GAME_ID              ")
    'sql_query_terminal_game.AppendLine("      , TE_PROVIDER_ID  PROVIDER_ID          ")
    'sql_query_terminal_game.AppendLine("      , TGT_PAYOUT_IDX                       ")
    'sql_query_terminal_game.AppendLine("      , TE_NAME         TERMINAL_NAME        ")
    'sql_query_terminal_game.AppendLine("      , X.GM_NAME       SOURCE_GAME_NAME     ")
    'sql_query_terminal_game.AppendLine("      , Y.GM_NAME       TARGET_GAME_NAME     ")
    'sql_query_terminal_game.AppendLine("      , Y.GM_NAME       TARGET_GAME_NAME_BIS ")
    'sql_query_terminal_game.AppendLine("      , TGT_CREATED     CREATED              ")
    'sql_query_terminal_game.AppendLine("      , TGT_UPDATED     UPDATED              ")
    'sql_query_terminal_game.AppendLine("      , 0               NUM_ROW              ")
    'sql_query_terminal_game.AppendLine("      , CASE TGT_PAYOUT_IDX                  ")
    'sql_query_terminal_game.AppendLine("          WHEN 1 THEN                        ")
    'sql_query_terminal_game.AppendLine("            (SELECT  PG_PAYOUT_1             ")
    'sql_query_terminal_game.AppendLine("               FROM PROVIDERS_GAMES        ")
    'sql_query_terminal_game.AppendLine(sql_where_payout_idx.ToString())
    'sql_query_terminal_game.AppendLine("          WHEN 2 THEN                        ")
    'sql_query_terminal_game.AppendLine("            (SELECT  PG_PAYOUT_2             ")
    'sql_query_terminal_game.AppendLine("               FROM PROVIDERS_GAMES        ")
    'sql_query_terminal_game.AppendLine(sql_where_payout_idx.ToString())
    'sql_query_terminal_game.AppendLine("          WHEN 3 THEN                        ")
    'sql_query_terminal_game.AppendLine("            (SELECT  PG_PAYOUT_3             ")
    'sql_query_terminal_game.AppendLine("               FROM PROVIDERS_GAMES        ")
    'sql_query_terminal_game.AppendLine(sql_where_payout_idx.ToString())
    'sql_query_terminal_game.AppendLine("          WHEN 4 THEN                        ")
    'sql_query_terminal_game.AppendLine("            (SELECT  PG_PAYOUT_4             ")
    'sql_query_terminal_game.AppendLine("               FROM PROVIDERS_GAMES        ")
    'sql_query_terminal_game.AppendLine(sql_where_payout_idx.ToString())
    'sql_query_terminal_game.AppendLine("          WHEN 5 THEN                        ")
    'sql_query_terminal_game.AppendLine("            (SELECT  PG_PAYOUT_5             ")
    'sql_query_terminal_game.AppendLine("               FROM PROVIDERS_GAMES        ")
    'sql_query_terminal_game.AppendLine(sql_where_payout_idx.ToString())
    'sql_query_terminal_game.AppendLine("          WHEN 6 THEN                        ")
    'sql_query_terminal_game.AppendLine("            (SELECT  PG_PAYOUT_6             ")
    'sql_query_terminal_game.AppendLine("               FROM PROVIDERS_GAMES        ")
    'sql_query_terminal_game.AppendLine(sql_where_payout_idx.ToString())
    'sql_query_terminal_game.AppendLine("          WHEN 7 THEN                        ")
    'sql_query_terminal_game.AppendLine("            (SELECT  PG_PAYOUT_7             ")
    'sql_query_terminal_game.AppendLine("               FROM PROVIDERS_GAMES        ")
    'sql_query_terminal_game.AppendLine(sql_where_payout_idx.ToString())
    'sql_query_terminal_game.AppendLine("          WHEN 8 THEN                        ")
    'sql_query_terminal_game.AppendLine("            (SELECT  PG_PAYOUT_8             ")
    'sql_query_terminal_game.AppendLine("               FROM PROVIDERS_GAMES        ")
    'sql_query_terminal_game.AppendLine(sql_where_payout_idx.ToString())
    'sql_query_terminal_game.AppendLine("          WHEN 9 THEN                        ")
    'sql_query_terminal_game.AppendLine("            (SELECT  PG_PAYOUT_9             ")
    'sql_query_terminal_game.AppendLine("               FROM PROVIDERS_GAMES        ")
    'sql_query_terminal_game.AppendLine(sql_where_payout_idx.ToString())
    'sql_query_terminal_game.AppendLine("          WHEN 10 THEN                       ")
    'sql_query_terminal_game.AppendLine("            (SELECT  PG_PAYOUT_10            ")
    'sql_query_terminal_game.AppendLine("               FROM PROVIDERS_GAMES        ")
    'sql_query_terminal_game.AppendLine(sql_where_payout_idx.ToString())
    'sql_query_terminal_game.AppendLine("          ELSE ' '                           ")
    'sql_query_terminal_game.AppendLine("        END AS PAYOUT                        ")

    'sql_query_terminal_game.AppendLine("   FROM TERMINAL_GAME_TRANSLATION            ")
    'sql_query_terminal_game.AppendLine("      , TERMINALS                            ")
    'sql_query_terminal_game.AppendLine("      , GAMES X                              ")
    'sql_query_terminal_game.AppendLine("      , GAMES Y                              ")

    'sql_query_terminal_game.AppendLine("  WHERE TGT_TERMINAL_ID    = TE_TERMINAL_ID  ")
    'sql_query_terminal_game.AppendLine("    AND TE_TYPE            = 1               ")
    'sql_query_terminal_game.AppendLine("    AND TGT_SOURCE_GAME_ID = X.GM_GAME_ID    ")
    'sql_query_terminal_game.AppendLine("    AND TGT_TARGET_GAME_ID = Y.GM_GAME_ID    ")
  End Sub

#End Region

End Class

Public Class DataGridComboBoxColumn

#Region " Declarations "

  Inherits DataGridTextBoxColumn

  Public ColumnComboBox As System.Windows.Forms.ComboBox
  Public Frozen As Boolean

  Private mcmSource As System.Windows.Forms.CurrencyManager
  Private mintRowNum As Integer
  Private mblnEditing As Boolean

#End Region

#Region " Constants "

#End Region ' Constants

#Region " Constructor "

  Public Sub New()

    mcmSource = Nothing
    mblnEditing = False
    Frozen = False

    ColumnComboBox = New System.Windows.Forms.ComboBox()
    ColumnComboBox.DropDownStyle = ComboBoxStyle.DropDown

    AddHandler ColumnComboBox.Leave, AddressOf LeaveComboBox
    AddHandler ColumnComboBox.SelectionChangeCommitted, AddressOf ComboStartEditing
    AddHandler ColumnComboBox.TextChanged, AddressOf ComboStartEditing
    AddHandler ColumnComboBox.Enter, AddressOf ComboEnter

  End Sub

#End Region

#Region " Private Methods "

  Private Sub HandleScroll(ByVal sender As Object, ByVal e As EventArgs)

    If ColumnComboBox.Visible Then
      ColumnComboBox.Hide()
    End If

  End Sub

  Private Sub ComboStartEditing(ByVal sender As Object, ByVal e As EventArgs)

    If Frozen Then
      Exit Sub
    End If

    If Not mblnEditing Then
      mblnEditing = True
      MyBase.ColumnStartedEditing(sender)
    End If

  End Sub

  Private Sub ComboEnter(ByVal sender As Object, ByVal e As EventArgs)

    If Frozen Then
      Exit Sub
    End If

    Dim str_sql As StringBuilder
    Dim provider_id As String
    Dim game_id As String
    Dim combo As ComboBox
    Dim parent As DataGrid
    Dim table As DataTable
    Dim _unknown_row As DataRow()

    combo = sender
    parent = combo.Parent

    If combo.Tag = ENUM_FORM.FORM_TERMINALS_PENDING Or (combo.Tag = ENUM_FORM.FORM_TERMINALS_3GS_EDIT And combo.DisplayMember <> "GM_NAME") Then
      combo.DropDownStyle = ComboBoxStyle.DropDownList
      Exit Sub
    End If

    'combo.AutoCompleteMode = AutoCompleteMode.Suggest
    combo.DropDownStyle = ComboBoxStyle.DropDownList

    If combo.DisplayMember <> "PAYOUT" Then
      If Not String.IsNullOrEmpty(parent.Item(mintRowNum, 0).ToString()) Then

        provider_id = parent.Item(mintRowNum, 0)
        str_sql = New StringBuilder()

        str_sql.AppendLine(" SELECT PG_GAME_NAME GM_NAME FROM PROVIDERS_GAMES WHERE PG_PV_ID = " & provider_id & "ORDER BY PG_GAME_NAME")

        table = New DataTable()
        table = GUI_GetTableUsingSQL(str_sql.ToString, 1000)

        _unknown_row = table.Select("GM_NAME = 'UNKNOWN'")

        'Add UNKNOWN Game Name as an option at the combobox
        If _unknown_row.Length = 0 Then
          table.Rows.Add("UNKNOWN")
        End If

        ColumnComboBox.DataSource = New DataView(table)
        ColumnComboBox.DisplayMember = "GM_NAME"
        ColumnComboBox.ValueMember = "GM_NAME"

        Dim text_grid As String = MyBase.GetColumnValueAtRow(mcmSource, mintRowNum).ToString()
        For index As Integer = 0 To ColumnComboBox.Items.Count - 1
          Dim drview As DataRowView
          drview = ColumnComboBox.Items(index)

          If drview.Row("GM_NAME") = text_grid Then
            ColumnComboBox.SelectedIndex = index
            Exit For
          End If
          'If ColumnComboBox.Items(index) = combo.Text Then
          '  ColumnComboBox.SelectedIndex = index
          'End If

        Next
      Else
        table = New DataTable()
        table.Columns.Add("GM_NAME")
        ColumnComboBox.DataSource = table.DefaultView()
        ColumnComboBox.DisplayMember = "GM_NAME"
        ColumnComboBox.ValueMember = "GM_NAME"

        Exit Sub

      End If
    Else

      provider_id = parent.Item(mintRowNum, 0)
      game_id = parent.Item(mintRowNum, 1)
      'game_id = 255
      str_sql = New StringBuilder()


      str_sql.AppendLine(" SELECT * FROM PROVIDERS_GAMES WHERE PG_PV_ID = " & provider_id & " AND PG_GAME_ID = " & game_id)

      Dim table_combo As DataTable
      Dim row As DataRow

      table_combo = New DataTable()
      table = New DataTable()

      table = GUI_GetTableUsingSQL(str_sql.ToString, 1000)

      row = table.Rows(0)
      table_combo.Columns.Add("INDEX")
      table_combo.Columns.Add("GAME_PAYOUT")

      For i As Integer = 0 To 9
        If Not String.Equals(row(3 + i).ToString(), String.Empty) Then
          table_combo.Rows.Add(i + 1, row(3 + i))
        End If
      Next

      ColumnComboBox.DataSource = table_combo.DefaultView()
      ColumnComboBox.DisplayMember = "GAME_PAYOUT"
      ColumnComboBox.ValueMember = "INDEX"
    End If

    If Not mblnEditing Then
      mblnEditing = True
      MyBase.ColumnStartedEditing(sender)
    End If

  End Sub


  Private Sub LeaveComboBox(ByVal sender As Object, ByVal e As EventArgs)

    Dim edit_value As String
    Dim _combo As ComboBox
    
    _combo = sender


    If ColumnComboBox.SelectedValue IsNot Nothing Then

      edit_value = ColumnComboBox.SelectedValue().ToString()
    Else
      If _combo.ValueMember = "GM_NAME" Then
        edit_value = "UNKNOWN"
      Else
        edit_value = ""
      End If
    End If

      ' Check whether the value is in the ComboBox or not
      'If ColumnComboBox.FindStringExact(edit_value) < 0 Then
      '' The value was NOT in the ComboBox => let's add it to make the browsing (SetColumnValueAtRow) work

      'Dim data_view As DataView
      'Dim data_view_row As DataRowView

      '' Get the combobox column's dataview
      'data_view = ColumnComboBox.DataSource

      '' Add a new item to the dataview 
      'data_view.AllowNew = True
      'data_view_row = data_view.AddNew
      'data_view_row("GM_NAME") = edit_value
      'data_view.AllowNew = False

      'ColumnComboBox.DataSource = data_view
      'End If

      If mblnEditing Then
        SetColumnValueAtRow(mcmSource, mintRowNum, edit_value)
        mblnEditing = False
        Invalidate()
      End If

      ColumnComboBox.Hide()
      AddHandler Me.DataGridTableStyle.DataGrid.Scroll, New EventHandler(AddressOf HandleScroll)

  End Sub

  Private Function GetGameIdFromProvider(ByVal ProviderId As Integer, ByVal Gamename As String) As Integer
    Dim _str_sql As StringBuilder
    Dim _obj As Object

    _str_sql = New StringBuilder()

    Try
      Using _db_trx As New DB_TRX()
        _str_sql.AppendLine(" SELECT PG_GAME_ID ")
        _str_sql.AppendLine("   FROM PROVIDERS_GAMES")
        _str_sql.AppendLine("  WHERE PG_GAME_NAME = @pGameName ")
        _str_sql.AppendLine("    AND PG_PV_ID = @pProviderId ")
        Using _cmd As New SqlCommand(_str_sql.ToString)
          _cmd.Parameters.Add("@pGameName", SqlDbType.NVarChar).Value = Gamename
          _cmd.Parameters.Add("@pProviderId", SqlDbType.BigInt).Value = ProviderId
          _obj = _db_trx.ExecuteScalar(_cmd)

          If _obj IsNot Nothing AndAlso _obj IsNot DBNull.Value Then
            Return CInt(_obj)
          End If

        End Using
      End Using

    Catch _ex As Exception
    End Try

    Return 0

  End Function

  Private Function GetProviderId(ByVal ProviderName As String) As Integer
    Dim _str_sql As StringBuilder
    Dim _obj As Object

    _str_sql = New StringBuilder()

    Try
      Using _db_trx As New DB_TRX()
        _str_sql.AppendLine(" SELECT PV_ID ")
        _str_sql.AppendLine("   FROM PROVIDERS")
        _str_sql.AppendLine("  WHERE PV_NAME = @pProviderName ")
        Using _cmd As New SqlCommand(_str_sql.ToString)
          _cmd.Parameters.Add("@pProviderName", SqlDbType.NVarChar).Value = ProviderName

          _obj = _db_trx.ExecuteScalar(_cmd)

          If _obj IsNot Nothing AndAlso _obj IsNot DBNull.Value Then
            Return CInt(_obj)
          End If

        End Using
      End Using

    Catch _ex As Exception
    End Try

    Return 0

  End Function

  ''' <summary>
  ''' Set data grid value of terminal game relation
  ''' </summary>
  ''' <param name="Parent"></param>
  ''' <param name="RowNum"></param>
  ''' <param name="game_id"></param>
  ''' <remarks></remarks>
  Private Sub SetTerminalGameRelationGridData(ByVal Parent As DataGrid, ByVal RowNum As Integer, ByVal game_id As Integer)
    Dim form As frm_terminal_game_relation = Parent.Parent.Parent
    Dim Payout As Integer = form.GRID_COLUMN_PAYOUT
    Dim Payout_idx As Integer = form.GRID_COLUMN_TGT_PAYOUT_IDX

    Parent.Item(RowNum, form.GRID_COLUMN_GAME_ID) = game_id

    Parent.Item(RowNum, Payout) = 0
    Parent.Item(RowNum, Payout_idx) = -1
  End Sub ' SetTerminalGameRelationGridData

#End Region

#Region " Overridden Methods "

  Protected Overloads Overrides Sub Edit( _
              ByVal [source] As System.Windows.Forms.CurrencyManager, _
              ByVal rowNum As Integer, _
              ByVal bounds As System.Drawing.Rectangle, _
              ByVal [readOnly] As Boolean, _
              ByVal instantText As String, _
              ByVal cellIsVisible As Boolean)

    MyBase.Edit([source], rowNum, bounds, [readOnly], instantText, cellIsVisible)

    mintRowNum = rowNum
    mcmSource = [source]

    With ColumnComboBox
      .Parent = Me.TextBox.Parent
      .Location = Me.TextBox.Location
      .Size = New Size(Me.TextBox.Size.Width, .Size.Height)
      .SelectedIndex = .FindStringExact(Me.TextBox.Text)
      .Text = Me.TextBox.Text

      Me.TextBox.Visible = False
      .Visible = True

      AddHandler Me.DataGridTableStyle.DataGrid.Scroll, AddressOf HandleScroll

      .BringToFront()
      .Focus()
    End With

  End Sub

  Protected Overrides Function Commit( _
              ByVal dataSource As System.Windows.Forms.CurrencyManager, _
              ByVal rowNum As Integer) As Boolean

    If mblnEditing Then
      mblnEditing = False
      SetColumnValueAtRow(dataSource, rowNum, ColumnComboBox.Text)
    End If

    Return True

  End Function

  Protected Overrides Sub ConcedeFocus()

    Console.WriteLine("ConcedeFocus")
    MyBase.ConcedeFocus()

  End Sub

  Protected Overrides Function GetColumnValueAtRow( _
              ByVal [source] As System.Windows.Forms.CurrencyManager, _
              ByVal rowNum As Integer) As Object

    Dim s As Object = MyBase.GetColumnValueAtRow([source], rowNum)
    Dim dv As DataView = CType(Me.ColumnComboBox.DataSource, DataView)
    Dim rowCount As Integer
    Dim i As Integer = 0
    Dim obj As Object

    If Not dv.Table.Columns.Contains(Me.ColumnComboBox.ValueMember) Then
      Return s
    End If
    '= dv.Count
    If dv IsNot Nothing Then
      rowCount = dv.Count
    Else
      rowCount = 0

    End If


    While i < rowCount
      obj = dv(i)(Me.ColumnComboBox.ValueMember)
      If (Not obj Is DBNull.Value) AndAlso _
         (Not s Is DBNull.Value) AndAlso _
         (s = obj) Then

        Exit While
      End If

      i += 1
    End While

    If i < rowCount Then
      Return dv(i)(Me.ColumnComboBox.DisplayMember)
    Else
      Return s
    End If

    Return DBNull.Value

  End Function

  Protected Overrides Sub SetColumnValueAtRow(ByVal [source] As System.Windows.Forms.CurrencyManager, _
                                              ByVal rowNum As Integer, _
                                              ByVal value As Object)

    Dim s As Object = value
    Dim dv As DataView = CType(Me.ColumnComboBox.DataSource, DataView)
    Dim rowCount As Integer
    Dim i As Integer = 0
    Dim obj As Object
    Dim str_display_member As String = Me.ColumnComboBox.DisplayMember
    Dim parent As DataGrid



    If IsNothing(dv) Then

      Return
    End If
    rowCount = dv.Count

    If str_display_member = "GAME_PAYOUT" Then
      str_display_member = "INDEX"
    End If

    While i < rowCount
      obj = dv(i)(str_display_member) '(Me.ColumnComboBox.DisplayMember)

      If (Not obj Is DBNull.Value) AndAlso _
          (s = obj) Then
        Exit While
      End If

      i += 1
    End While

    If i < rowCount Then
      s = dv(i)(Me.ColumnComboBox.ValueMember)
    Else
      's = DBNull.Value
      s = value
    End If

    If MyBase.GetColumnValueAtRow([source], rowNum) = s Then
      Exit Sub
    End If

    If str_display_member = "PG_GAME_NAME" Or str_display_member = "GM_NAME" Then
      If String.IsNullOrEmpty(s) Then
        s = "UNKNOWN"
      End If
    End If
    MyBase.SetColumnValueAtRow([source], rowNum, s)

    parent = Me.ColumnComboBox.Parent

    If str_display_member = "PG_GAME_NAME" Or str_display_member = "GM_NAME" Then
      Dim game_id As Integer

      If parent IsNot Nothing Then
        If Not String.IsNullOrEmpty(parent.Item(rowNum, 0).ToString()) Then
          game_id = GetGameIdFromProvider(parent.Item(rowNum, 0), s)

          If Me.ColumnComboBox.Tag = ENUM_FORM.FORM_TERMINALS_3GS_EDIT Then
            parent.Item(rowNum, frm_terminals_3gs_edit.GRID_COLUMN_3GS_GAME_ID) = game_id

          ElseIf Me.ColumnComboBox.Tag <> ENUM_FORM.FORM_TERMINALS_3GS_EDIT AndAlso Me.ColumnComboBox.Tag <> ENUM_FORM.FORM_TERMINALS_PENDING Then
            SetTerminalGameRelationGridData(parent, rowNum, game_id)
          End If
        End If
      End If
    ElseIf str_display_member = "PV_NAME" Then
      Dim provider_id As Integer

      Try
        'If Not IsNothing(parent.Parent.Parent) And parent.Parent.Parent.Name = "frm_terminals_3gs_edit" Then
        If Me.ColumnComboBox.Tag = ENUM_FORM.FORM_TERMINALS_3GS_EDIT Then
          provider_id = GetProviderId(s)
          parent.Item(rowNum, 0) = provider_id

          parent.Item(rowNum, frm_terminal_game_relation.GRID_COLUMN_GAME_ID) = 0
          parent.Item(rowNum, 7) = "UNKNOWN" 'Game name
        Else
          parent.Item(rowNum, 0) = s
        End If

      Catch ex As Exception
        parent.Item(rowNum, 0) = s
      End Try
      'provider_id = GetProviderId(s)
      'parent.Item(rowNum, 0) = provider_id
      'parent.Item(rowNum, 0) = s
    End If
  End Sub

#End Region

End Class





Public Class DataGridComboBoxColumnBis

#Region " Declarations "

  Inherits DataGridTextBoxColumn

  Public ColumnComboBoxBis As System.Windows.Forms.ComboBox
  Public Frozen As Boolean

  Private mcmSource As System.Windows.Forms.CurrencyManager
  Private mintRowNum As Integer
  Private mblnEditing As Boolean

#End Region

#Region " Constants "

  'Private Const SELECT_GAMES As String = " SELECT GM_NAME FROM GAMES, TERMINAL_GAME_TRANSLATION WHERE TGT_TARGET_GAME_ID = GM_GAME_ID AND TGT_SOURCE_GAME_ID <> TGT_TARGET_GAME_ID ORDER BY GM_NAME "

#End Region ' Constants

#Region " Constructor "

  Public Sub New()

    mcmSource = Nothing
    mblnEditing = False
    Frozen = False

    ColumnComboBoxBis = New System.Windows.Forms.ComboBox()
    ColumnComboBoxBis.DropDownStyle = ComboBoxStyle.DropDown

    AddHandler ColumnComboBoxBis.Leave, AddressOf LeaveComboBox
    AddHandler ColumnComboBoxBis.SelectionChangeCommitted, AddressOf ComboStartEditing
    AddHandler ColumnComboBoxBis.TextChanged, AddressOf ComboStartEditing
    AddHandler ColumnComboBoxBis.Enter, AddressOf ComboEnter
    AddHandler ColumnComboBoxBis.SelectedIndexChanged, AddressOf ComboSelectedIndexChanged
  End Sub

#End Region

#Region " Private Methods "

  Private Sub HandleScroll(ByVal sender As Object, ByVal e As EventArgs)

    If ColumnComboBoxBis.Visible Then
      ColumnComboBoxBis.Hide()
    End If

  End Sub
  Private Sub ComboSelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

  End Sub

  Private Sub ComboStartEditing(ByVal sender As Object, ByVal e As EventArgs)

    If Frozen Then
      Exit Sub
    End If

    If Not mblnEditing Then
      mblnEditing = True
      MyBase.ColumnStartedEditing(sender)
    End If

  End Sub

  Private Sub ComboEnter(ByVal sender As Object, ByVal e As EventArgs)

    If Frozen Then
      Exit Sub
    End If

    Dim str_sql As StringBuilder
    Dim provider_id As String
    Dim game_id As String
    Dim combo As ComboBox
    Dim parent As DataGrid
    Dim rows_affected As Integer
    Dim table As DataTable
    Dim table_combo As DataTable
    Dim row As DataRow
    Dim _sql_trx As System.Data.SqlClient.SqlTransaction = Nothing
    Dim obj As Object
    Dim _payout_selected As Single = 0

    combo = sender
    parent = combo.Parent

    If combo.Tag = ENUM_FORM.FORM_TERMINALS_PENDING Or combo.Tag = ENUM_FORM.FORM_TERMINALS_3GS_EDIT Then
      Exit Sub
    End If


    combo.DropDownStyle = ComboBoxStyle.DropDownList

    provider_id = parent.Item(mintRowNum, 0)
    game_id = "-1"

    obj = parent.Item(mintRowNum, 1)

    If obj IsNot Nothing And Not String.IsNullOrEmpty(obj.ToString()) Then
      game_id = obj.ToString()
    End If


    str_sql = New StringBuilder()

    GUI_BeginSQLTransaction(_sql_trx)

    str_sql.AppendLine(" SELECT * FROM PROVIDERS_GAMES WHERE PG_PV_ID = " & provider_id & " AND PG_GAME_ID = " & game_id)

    'Using cmd As New SqlClient.SqlCommand(str_sql.ToString(), _sql_trx.Connection, _sql_trx)
    '  rows_affected = cmd.ExecuteNonQuery()
    'End Using

    table = GUI_GetTableUsingSQL(str_sql.ToString, 1000)
    rows_affected = table.Rows.Count

    table_combo = New DataTable()
    table_combo.Columns.Add("INDEX")
    table_combo.Columns.Add("GAME_PAYOUT")

    'table = New DataTable()
    If rows_affected > 0 Then
      'table = GUI_GetTableUsingSQL(str_sql.ToString, 1000)
      row = table.Rows(0)
      For i As Integer = 0 To 9
        If Not String.Equals(row(3 + i).ToString(), String.Empty) Then
          table_combo.Rows.Add(i + 1, GUI_FormatNumber(row(3 + i), 2))
        End If
      Next
    Else
      'table_combo.Rows.Add(1, GUI_FormatNumber(0, 2))
    End If

    If (table_combo.Rows.Count = 0) Then
      table_combo.Rows.Add("-1", "0.00")
    End If
    ColumnComboBoxBis.DataSource = table_combo.DefaultView()
    ColumnComboBoxBis.DisplayMember = "GAME_PAYOUT"
    ColumnComboBoxBis.ValueMember = "INDEX"


    If (MyBase.GetColumnValueAtRow(mcmSource, mintRowNum) IsNot Nothing AndAlso MyBase.GetColumnValueAtRow(mcmSource, mintRowNum) IsNot DBNull.Value) Then
      _payout_selected = Convert.ToSingle(MyBase.GetColumnValueAtRow(mcmSource, mintRowNum))
    End If

    For index As Integer = 0 To ColumnComboBoxBis.Items.Count - 1
      Dim drview As DataRowView
      drview = ColumnComboBoxBis.Items(index)


      If drview.Row("GAME_PAYOUT") = _payout_selected.ToString("0.00") Then
        ColumnComboBoxBis.SelectedIndex = index
        Exit For
      End If
      'If ColumnComboBox.Items(index) = combo.Text Then
      '  ColumnComboBox.SelectedIndex = index
      'End If

    Next

    If Not mblnEditing Then
      mblnEditing = True
      MyBase.ColumnStartedEditing(sender)
    End If

  End Sub


  Private Sub LeaveComboBox(ByVal sender As Object, ByVal e As EventArgs)

    Dim edit_value As String

    If Not String.IsNullOrEmpty(ColumnComboBoxBis.SelectedValue()) Then
      If ColumnComboBoxBis.SelectedValue().ToString() <> "-1" Then
        edit_value = ColumnComboBoxBis.Text
      Else
        edit_value = 0 'ColumnComboBoxBis.SelectedValue().ToString()
      End If
    Else
      edit_value = 0 'ColumnComboBoxBis.SelectedValue().ToString()
    End If
    '' Check whether the value is in the ComboBox or not
    'If ColumnComboBoxBis.FindStringExact(edit_value) < 0 Then
    '  ' The value was NOT in the ComboBox => let's add it to make the browsing (SetColumnValueAtRow) work

    '  Dim data_view As DataView
    '  Dim data_view_row As DataRowView

    '  ' Get the combobox column's dataview
    '  data_view = ColumnComboBoxBis.DataSource

    '  ' Add a new item to the dataview 
    '  data_view.AllowNew = True
    '  data_view_row = data_view.AddNew
    '  data_view_row("INDEX") = 9999
    '  data_view_row("GAME_PAYOUT") = edit_value
    '  data_view.AllowNew = False

    '  ColumnComboBoxBis.DataSource = data_view
    'End If

    If mblnEditing Then
      SetColumnValueAtRow(mcmSource, mintRowNum, edit_value)
      mblnEditing = False
      Invalidate()
    End If

    ColumnComboBoxBis.Hide()
    AddHandler Me.DataGridTableStyle.DataGrid.Scroll, New EventHandler(AddressOf HandleScroll)

  End Sub

#End Region

#Region " Overridden Methods "

  Protected Overloads Overrides Sub Edit( _
              ByVal [source] As System.Windows.Forms.CurrencyManager, _
              ByVal rowNum As Integer, _
              ByVal bounds As System.Drawing.Rectangle, _
              ByVal [readOnly] As Boolean, _
              ByVal instantText As String, _
              ByVal cellIsVisible As Boolean)

    MyBase.Edit([source], rowNum, bounds, [readOnly], instantText, cellIsVisible)

    mintRowNum = rowNum
    mcmSource = [source]

    With ColumnComboBoxBis
      .Parent = Me.TextBox.Parent
      .Location = Me.TextBox.Location
      .Size = New Size(Me.TextBox.Size.Width, .Size.Height)
      .SelectedIndex = .FindStringExact(Me.TextBox.Text)
      .Text = Me.TextBox.Text

      Me.TextBox.Visible = False
      .Visible = True

      AddHandler Me.DataGridTableStyle.DataGrid.Scroll, AddressOf HandleScroll

      .BringToFront()
      .Focus()
    End With

  End Sub

  Protected Overrides Function Commit( _
              ByVal dataSource As System.Windows.Forms.CurrencyManager, _
              ByVal rowNum As Integer) As Boolean

    If mblnEditing Then
      mblnEditing = False
      SetColumnValueAtRow(dataSource, rowNum, ColumnComboBoxBis.Text)
    End If

    Return True

  End Function

  Protected Overrides Sub ConcedeFocus()

    Console.WriteLine("ConcedeFocus")
    MyBase.ConcedeFocus()

  End Sub

  Protected Overrides Function GetColumnValueAtRow( _
              ByVal [source] As System.Windows.Forms.CurrencyManager, _
              ByVal rowNum As Integer) As Object

    Dim s As Object = MyBase.GetColumnValueAtRow([source], rowNum)
    'Dim dv As DataView = CType(Me.ColumnComboBoxBis.DataSource, DataView)
    'Dim rowCount As Integer
    'Dim i As Integer = 0
    'Dim obj As Object

    ''= dv.Count
    'If dv IsNot Nothing Then
    ' owCount = dv.Count
    'Else
    ' owCount = 0

    'End If


    'While i < rowCount
    ' bj = dv(i)(Me.ColumnComboBoxBis.ValueMember)
    ' f (Not obj Is DBNull.Value) AndAlso _
    '   (Not s Is DBNull.Value) AndAlso _
    '   (s = obj) Then

    ' xit While
    ' nd If

    '  += 1
    'End While

    'If i < rowCount Then
    ' eturn dv(i)(Me.ColumnComboBoxBis.DisplayMember)
    'Else
    Return s
    'End If

    'Return DBNull.Value

  End Function

  Protected Overrides Sub SetColumnValueAtRow(ByVal [source] As System.Windows.Forms.CurrencyManager, _
                                              ByVal rowNum As Integer, _
                                              ByVal value As Object)

    Dim s As Object = value
    Dim dv As DataView = CType(Me.ColumnComboBoxBis.DataSource, DataView)
    Dim rowCount As Integer
    Dim i As Integer = 0
    Dim obj As Object
    Dim str_display_member As String = Me.ColumnComboBoxBis.DisplayMember
    Dim str_display_member_original As String = Me.ColumnComboBoxBis.DisplayMember
    Dim _result As Integer

    If IsNothing(dv) Then

      Return
    End If
    rowCount = dv.Count

    If str_display_member = "GAME_PAYOUT" Then
      str_display_member = "INDEX"
    End If
    While i < rowCount
      obj = dv(i)(str_display_member) '(Me.ColumnComboBox.DisplayMember)

      If (Not obj Is DBNull.Value) AndAlso _
          (s = obj) Then
        Exit While
      End If

      i += 1
    End While

    If i < rowCount Then
      s = dv(i)(Me.ColumnComboBoxBis.ValueMember)
    Else
      's = DBNull.Value
      s = value
    End If


    If str_display_member_original = "GAME_PAYOUT" Then
      Dim parent As DataGrid

      _result = 0

      parent = Me.ColumnComboBoxBis.Parent
      If Me.ColumnComboBoxBis.SelectedValue IsNot Nothing AndAlso Int32.TryParse(Me.ColumnComboBoxBis.SelectedValue.ToString(), _result) Then
        parent.Item(rowNum, frm_terminal_game_relation.GRID_COLUMN_TGT_PAYOUT_IDX) = Me.ColumnComboBoxBis.SelectedValue
      Else
        parent.Item(rowNum, frm_terminal_game_relation.GRID_COLUMN_TGT_PAYOUT_IDX) = 0
      End If

      If String.IsNullOrEmpty(s.ToString().Trim()) Then
        s = 0
      End If
    End If


    Dim _obj_value As String = MyBase.GetColumnValueAtRow([source], rowNum).ToString()
    If Not IsNothing(_obj_value) And Not String.IsNullOrEmpty(_obj_value) Then
      If MyBase.GetColumnValueAtRow([source], rowNum) = s Then
        Exit Sub
      End If
    End If

    If String.IsNullOrEmpty(s.ToString().Trim()) And str_display_member_original = "GAME_PAYOUT" Then
      s = -1
    End If

    MyBase.SetColumnValueAtRow([source], rowNum, s)


  End Sub

#End Region

End Class
