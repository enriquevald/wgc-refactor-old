'-------------------------------------------------------------------
' Copyright © 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_money_laundering_external_config
' DESCRIPTION:   Configure accounts required fields and limits for money laundering external actions
' AUTHOR:        Alberto Marcos
' CREATION DATE: 17-MAR-2015
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 17-MAR-2015 AMF    First Release.
'-------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common.Misc
Imports GUI_CommonOperations.CLASS_BASE
Imports WSI.Common.Users
Imports WSI.Common
Imports System.Text
Imports System.Data.SqlClient

Public Class frm_money_laundering_external_config
  Inherits frm_base_edit

#Region " Members "

  Private m_base_name As String

#End Region ' Members

#Region " Enums "

  Public Enum CUSTOM_TICKET_PRINT_MODE
    DONT_PRINT = 0
    PRINT = 1
    ASK_BEFORE_PRINT = 2
  End Enum

#End Region

#Region " Constants "

  Private Const FORM_DB_MIN_VERSION As Short = 183

#End Region

#Region " Overrides "

  ' PURPOSE : Override form permissions to disallow writting in MultiSite Member
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As GUI_Controls.CLASS_GUI_USER.TYPE_PERMISSIONS)
    MyBase.GUI_Permissions(AndPerm)

    Dim _is_multisite_member As Boolean

    _is_multisite_member = GeneralParam.GetBoolean("Site", "MultiSiteMember")

    AndPerm.Write = Not _is_multisite_member

  End Sub

  ' PURPOSE : Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_MONEY_LAUNDERING_CONFIG

    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)

    Call MyBase.GUI_SetFormId()
  End Sub

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()
    Dim _account_holder_info As Dictionary(Of Int32, String)

    MyBase.GUI_InitControls()

    'setting the window name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2948, Accounts.getAMLName()) ' NLS: Configuración antilavado de dinero
    Me.lbl_info.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2507, Accounts.getAMLName()) 'NLS: La Configuración antilavado de dinero solo puede editarse en el MultiSite
    Me.lbl_info.Visible = GeneralParam.GetBoolean("Site", "MultiSiteMember")
    Me.ef_aml_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(367)

    Me.ef_aml_expiration_days.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6053)
    Me.lbl_days.Text = GLB_NLS_GUI_CONFIGURATION.GetString(204)
    Me.gb_prize_ident_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6055)
    Me.gb_rchg_ident_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6054)
    Me.gb_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(282) ' Límites

    _account_holder_info = IdentificationTypes.GetIdentificationTypes()

    'setting text to the labels
    Me.chk_active_aml.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2386, Accounts.getAMLName()) ' NLS: Activar control antilavado de dinero

    Me.lbl_rchg_ident_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(288) ' NLS: Límite identificación
    Me.lbl_rchg_ident_msg.Text = GLB_NLS_GUI_AUDITOR.GetString(252) ' NLS: A las
    Me.lbl_prize_ident_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(288) ' NLS: Límite identificación
    Me.lbl_prize_ident_msg.Text = GLB_NLS_GUI_AUDITOR.GetString(252) ' NLS: A las

    'setting properties to the entry fields: lenght
    Me.ef_minimum_wage.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 6, 2)
    Me.ef_rchg_ident_limit.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 2)
    Me.ef_prize_ident_limit.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 2)
    Me.ef_aml_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    Me.ef_aml_expiration_days.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 5)

    'making entry fields editable
    Me.ef_minimum_wage.IsReadOnly = False
    Me.ef_minimum_wage.TextVisible = True
    Me.ef_rchg_ident_limit.IsReadOnly = False
    Me.ef_prize_ident_limit.IsReadOnly = False

    Me.ef_minimum_wage.Focus()

    GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = Me.Permissions.Write
    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False

  End Sub

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)

    Select Case DbOperation

      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLS_MONEY_LAUNDERING_CONFIG

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2479), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub

  ' PURPOSE: Validate the data presented on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    If Me.IsEmptyOrZero(Me.ef_minimum_wage.Value) Then
      Me.ef_minimum_wage.Focus()
      ' NLS: Personalización de Cuenta
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _
                        Me.ef_minimum_wage.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    If Me.IsEmptyOrZero(Me.ef_rchg_ident_limit.Value) Then
      Me.ef_rchg_ident_limit.Focus()
      ' NLS: Personalización de Cuenta
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _
                        Me.lbl_rchg_ident_limit.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    If String.IsNullOrEmpty(Me.ef_aml_name.Value.Trim()) Then
      Me.ef_aml_name.Focus()
      ' NLS: Personalización de Cuenta
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _
                        Me.ef_aml_name.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    If String.IsNullOrEmpty(Me.tb_rchg_ident_msg.Text.Trim()) Then
      Me.tb_rchg_ident_msg.Focus()
      ' NLS: Personalización de Cuenta
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _
                        Me.lbl_rchg_ident_msg.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If
    '

    If Me.IsEmptyOrZero(Me.ef_prize_ident_limit.Value) Then
      Me.ef_prize_ident_limit.Focus()
      ' NLS: Personalización de Cuenta
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _
                        Me.lbl_prize_ident_limit.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    If String.IsNullOrEmpty(Me.tb_prize_ident_msg.Text.Trim()) Then
      Me.tb_prize_ident_msg.Focus()
      ' NLS: Personalización de Cuenta
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _
                        Me.lbl_prize_ident_msg.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    Return True

  End Function

  '  PURPOSE: Ignore [ENTER] key when editing comments
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    If e.KeyChar = Chr(Keys.Enter) Then
      If Me.tb_rchg_ident_msg.ContainsFocus _
        Or Me.tb_prize_ident_msg.ContainsFocus Then

        Return True
      End If
    End If

    Return MyBase.GUI_KeyPress(sender, e)

  End Function

  '  PURPOSE: Set the screen data based on Database object
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _ml_config As CLS_MONEY_LAUNDERING_CONFIG

    _ml_config = DbReadObject


    Me.ef_minimum_wage.Text = _ml_config.BaseName
    Me.ef_aml_name.Value = _ml_config.Name
    Me.lbl_minwage_desc.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2400) & " " & _ml_config.BaseName
    m_base_name = _ml_config.BaseName

    Me.chk_active_aml.Checked = _ml_config.Enabled

    Me.ef_minimum_wage.Value = _ml_config.BaseAmount

    Me.ef_rchg_ident_limit.Value = _ml_config.RechargeIdentification.Limit
    Me.tb_rchg_ident_msg.Text = _ml_config.RechargeIdentification.Message

    Me.ef_prize_ident_limit.Value = _ml_config.PrizeIdentification.Limit
    Me.tb_prize_ident_msg.Text = _ml_config.PrizeIdentification.Message

    Me.ef_aml_expiration_days.Value = _ml_config.ExpirationDays

    Me.SetAbsoluteValues()

  End Sub

  '  PURPOSE: Get data in the form
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_GetScreenData()

    Dim _ml_config As CLS_MONEY_LAUNDERING_CONFIG

    _ml_config = DbEditedObject

    _ml_config.Name = Me.ef_aml_name.Value
    _ml_config.Enabled = Me.chk_active_aml.Checked
    _ml_config.BaseAmount = IIf(Me.IsEmptyOrZero(Me.ef_minimum_wage.Value), 0, Me.ef_minimum_wage.Value)

    _ml_config.RechargeIdentification.Limit = IIf(Me.IsEmptyOrZero(Me.ef_rchg_ident_limit.Value), 0, Me.ef_rchg_ident_limit.Value)
    _ml_config.RechargeIdentification.Message = Me.tb_rchg_ident_msg.Text

    _ml_config.PrizeIdentification.Limit = IIf(Me.IsEmptyOrZero(Me.ef_prize_ident_limit.Value), 0, Me.ef_prize_ident_limit.Value)
    _ml_config.PrizeIdentification.Message = Me.tb_prize_ident_msg.Text

    _ml_config.ExpirationDays = IIf(Me.IsEmptyOrZero(Me.ef_aml_expiration_days.Value), 0, Me.ef_aml_expiration_days.Value)

  End Sub

#End Region ' Overrides

#Region "User Controls Handlers"

  ' PURPOSE : Controls entry fields value change
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT : 
  '
  ' RETURNS :
  Private Sub ef_EntryFieldValueChanged() Handles ef_minimum_wage.EntryFieldValueChanged, ef_prize_ident_limit.EntryFieldValueChanged, ef_rchg_ident_limit.EntryFieldValueChanged
    Me.SetAbsoluteValues()
  End Sub

#End Region ' User Controls Handlers

#Region "Private Functions"

  ' PURPOSE : Converts a bolean value into "1" or "0"
  '
  '  PARAMS :
  '     - INPUT : 
  '             - BoolValue
  '     - OUTPUT :
  '
  ' RETURNS : The result of the conversion
  Private Function Get_String_from_Bool(ByVal BoolValue As Boolean) As String
    If BoolValue Then
      Return "1"
    Else
      Return "0"
    End If
  End Function

  ' PURPOSE : Sets absolute values fields text
  '
  '  PARAMS :
  '     - INPUT : 
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub SetAbsoluteValues()
    Dim _base_amount As Decimal
    Dim _recharge_limit1_value As Decimal
    Dim _prize_limit1_value As Decimal

    _base_amount = 0
    _recharge_limit1_value = 0
    _prize_limit1_value = 0

    If Me.ef_minimum_wage.TextValue.Trim() <> String.Empty Then
      _base_amount = Convert.ToDecimal(Me.ef_minimum_wage.Value.Trim())
    End If

    If Me.ef_rchg_ident_limit.TextValue.Trim <> String.Empty Then
      _recharge_limit1_value = Convert.ToDecimal(Me.ef_rchg_ident_limit.Value.Trim())
    End If

    If Me.ef_prize_ident_limit.TextValue.Trim <> String.Empty Then
      _prize_limit1_value = Convert.ToDecimal(Me.ef_prize_ident_limit.Value.Trim())
    End If

    Me.lbl_rchg_ident_absolute.Value = m_base_name & " (" & GUI_FormatCurrency(_base_amount * _recharge_limit1_value) & ")"
    Me.lbl_prize_ident_absolute.Value = m_base_name & " (" & GUI_FormatCurrency(_base_amount * _prize_limit1_value) & ")"

  End Sub

  ' PURPOSE : Determines if a numeric istring is empty or 0
  '
  '  PARAMS :
  '     - INPUT : 
  '             - Value
  '     - OUTPUT :
  '
  ' RETURNS : TRUE if is empty or zero, FALSE otherwise
  Private Function IsEmptyOrZero(ByVal Value As String) As Boolean

    If String.IsNullOrEmpty(Value) Then
      Return True
    ElseIf Value = 0 Then
      Return True
    End If

  End Function

#End Region ' Private Functions

#Region " Public Functions "

  ' PURPOSE : Overloads ShowEditItem function to pass the 'show mode'
  '
  '  PARAMS :
  '     - INPUT : 
  '             - RequiredFieldsMode
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Overloads Sub ShowEditItem(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    MyBase.ShowEditItem(0)

  End Sub

#End Region ' Public Functions

End Class