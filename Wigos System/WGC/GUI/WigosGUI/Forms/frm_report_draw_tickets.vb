'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_report_draw_ticketds
' DESCRIPTION:   Report draw tickets
' AUTHOR:        Juli Arnalot
' CREATION DATE: 27-MAR-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 27-MAR-2013  JAR    Initial version
' 24-APR-2013  SMN    Hide Player Tracking data if exists an External Loyalty 
' 05-JUN-2013  DHA    Fixed Bug #819: Modifications in the query for filters with character '%'
' 05-JUN-2013  NMR    Replacing string in MessageBox to clarify the message
' 13-JUN-2013  DHA    Fixed Bug #848: Wrong date format on field First Ticket from Excel
' 16-SEP-2013  CCG    Added account massive search in the query
' 24-FEB-2014  JFC    Fixec bug WIG-334: FilterCheck modified.
' 07-MAY-2014  DHA    Fixed Bug WIG-900: timeout due to virtual accounts filter
'--------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_Controls
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports WSI.Common

Public Class frm_report_draw_tickets
  Inherits frm_base_sel

#Region " Constants "
  Private Const LEN_DRAW_NAME As Integer = 50

  ' DB Columns
  Private Const SQL_COL_HOLDER_LEVEL As Integer = 0
  Private Const SQL_COL_ACCOUNT_ID As Integer = 1
  Private Const SQL_COL_HOLDER_NAME As Integer = 2
  Private Const SQL_COL_DRAW_ID As Integer = 3
  Private Const SQL_COL_DRAW_NAME As Integer = 4
  Private Const SQL_COL_DRAW_TYPE As Integer = 5
  Private Const SQL_COL_NUM_TICKETS As Integer = 6
  Private Const SQL_COL_NUM_NUMBERS As Integer = 7
  Private Const SQL_COL_FIRST_TICKET As Integer = 8
  Private Const SQL_COL_LAST_TICKET As Integer = 9

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private GRID_COL_HOLDER_LEVEL As Integer = 1
  Private GRID_COL_ACCOUNT_ID As Integer = 2
  Private GRID_COL_HOLDER_NAME As Integer = 3
  Private GRID_COL_DRAW_ID As Integer = 4
  Private GRID_COL_DRAW_NAME As Integer = 5
  Private GRID_COL_DRAW_TYPE As Integer = 6
  Private Const GRID_COL_NUM_TICKETS As Integer = 7
  Private Const GRID_COL_NUM_NUMBERS As Integer = 8
  Private Const GRID_COL_FIRST_TICKET As Integer = 9
  Private Const GRID_COL_LAST_TICKET As Integer = 10

  ' Width Columns
  Private Const GRID_WIDTH_HOLDER_LEVEL As Integer = 1330
  Private Const GRID_WIDTH_ACCOUNT_ID As Integer = 1180
  Private Const GRID_WIDTH_HOLDER_NAME As Integer = 3170
  Private Const GRID_WIDTH_DRAW_ID As Integer = 0
  Private Const GRID_WIDTH_DRAW_NAME As Integer = 3100
  Private Const GRID_WIDTH_DRAW_TYPE As Integer = 3100
  Private Const GRID_WIDTH_NUM_TICKETS As Integer = 1300
  Private Const GRID_WIDTH_NUM_NUMBERS As Integer = 1500
  Private Const GRID_WIDTH_FIRST_TICKET As Integer = 2040
  Private Const GRID_WIDTH_LAST_TICKET As Integer = 2040

  Private Const GRID_COLUMNS_COUNT As Integer = 11
  Private Const GRID_HEADER_ROWS As Integer = 2

  Private Const FORM_DB_MIN_VERSION As Short = 149
#End Region ' Constants

#Region " Members "

  Private m_level_names As Dictionary(Of Integer, String)

  ' For subtotals
  Private m_previous_level As Integer
  Private m_previous_draw As String

  ' For report filters
  Private m_date_from As String
  Private m_date_to As String
  Private m_account As String
  Private m_track_data As String
  Private m_holder As String
  Private m_draw_name As String
  Private m_order_name As String

  ' For grid totalizators
  Dim m_total_tickets_level As Double
  Dim m_total_number_level As Double
  Dim m_total_tickets_draw As Double
  Dim m_total_number_draw As Double
#End Region ' Members

#Region " Overrides "
  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_REPORT_DRAW_TICKET

    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    Try
      ' Dates selection 
      If Me.dtp_from.Checked AndAlso Me.dtp_to.Checked Then
        If Me.dtp_from.Value > Me.dtp_to.Value Then
          Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
          Call Me.dtp_to.Focus()

          Return False
        End If
      End If

      ' Account track data format
      If Not uc_account_filter.ValidateFormat() Then
        Return False
      End If

      ' Check mandatory params
      If opt_order_draw.Checked Then
        If opt_draw_name.Checked Then
          If String.IsNullOrEmpty(ef_draw_name.Value) Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1893))
            Return False
          End If
          'ElseIf opt_draw_list.Checked Then
          '  Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_PLAYER_TRACKING.GetString(319))
          '  Return False
        End If
      Else
        If String.IsNullOrEmpty(uc_account_filter.GetMassiveSearchFilterSQL()) Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_INVOICING.GetString(115))
          Return False
        End If
      End If

      Return True

    Catch _ex As Exception
      ' An error has occurred in the query execution
      Call Trace.WriteLine(_ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, "frm_report_draw_tickets", "GUI_FilterCheck", _ex.Message)
    End Try

    Return False
  End Function ' GUI_FilterCheck

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()
    m_total_number_level = 0
    m_total_tickets_level = 0
    m_total_tickets_draw = 0
    m_total_number_draw = 0
    Call GUI_StyleSheet()
  End Sub

  ' PURPOSE: Print totalizator row in the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    Dim _text_subtotal As String
    Dim _text_total As String

    _text_subtotal = GLB_NLS_GUI_INVOICING.GetString(375)
    _text_total = GLB_NLS_GUI_INVOICING.GetString(205)

    ProcesSubTotal(GRID_COL_HOLDER_LEVEL, m_total_tickets_level, m_total_number_level, ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01, _text_subtotal)
    ProcesSubTotal(GRID_COL_DRAW_NAME, m_total_tickets_draw, m_total_number_draw, ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00, _text_total)

    ResetTotalCounters()
  End Sub ' GUI_AfterLastRow

  ' PURPOSE : Checks out the DB row before adding it to the grid
  '              and process counters (& totals rows)
  '
  '  PARAMS :
  '     - INPUT :
  '           - DataRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the data row can be added) or False (the data row can not be added)
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As CLASS_DB_ROW) As Boolean

    Call ProcessCounters(DbRow)

    Return True
  End Function ' GUI_CheckOutRowBeforeAdd

  ' PURPOSE : Define the ExecuteQuery customized
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_ExecuteQueryCustom()
    Dim _sql_cmd As SqlCommand
    Dim _sql_where_account As String
    Dim _table As DataTable
    Dim _row As DataRow
    Dim _sort As String
    Dim _show_totals As Boolean

    _sql_cmd = New SqlCommand("ReportDrawTickets")
    _sql_cmd.CommandType = CommandType.StoredProcedure

    ' CCG 16-SEP-2013: Get ID ACCOUNT for single or massive search
    _sql_where_account = Me.uc_account_filter.GetMassiveSearchFilterSQL()

    _sql_cmd.Parameters.Add("@pDrawId", SqlDbType.BigInt).Value = IIf(Not opt_draw_list.Checked, System.DBNull.Value, cmd_draw_name.SelectedValue)
    _sql_cmd.Parameters.Add("@pDrawName", SqlDbType.NVarChar).Value = IIf(String.IsNullOrEmpty(ef_draw_name.Value), System.DBNull.Value, ef_draw_name.Value.Replace("%", "\%"))
    _sql_cmd.Parameters.Add("@pSqlAccount", SqlDbType.NVarChar).Value = IIf(String.IsNullOrEmpty(_sql_where_account), System.DBNull.Value, "WHERE " & _sql_where_account)
    _sql_cmd.Parameters.Add("@pTicketFrom", SqlDbType.DateTime).Value = IIf(Not dtp_from.Checked, System.DBNull.Value, dtp_from.Value)
    _sql_cmd.Parameters.Add("@pTicketTo", SqlDbType.DateTime).Value = IIf(Not dtp_to.Checked, System.DBNull.Value, dtp_to.Value)

    _table = GUI_GetTableUsingCommand(_sql_cmd, Integer.MaxValue)

    If opt_order_account.Checked Then
      _sort = "AC_HOLDER_LEVEL, AC_HOLDER_NAME"
      _show_totals = False
    Else
      _sort = "DR_NAME, AC_HOLDER_LEVEL"
      _show_totals = True
    End If

    Call GUI_BeforeFirstRow()

    Dim rows() As DataRow = _table.Select("", _sort)

    Dim db_row As CLASS_DB_ROW
    Dim count As Integer
    Dim idx_row As Integer

    For Each _row In rows

      Try
        db_row = New CLASS_DB_ROW(_row)

        If GUI_CheckOutRowBeforeAdd(db_row) Then
          ' Add the db row to the grid
          Me.Grid.AddRow()
          count = count + 1
          idx_row = Me.Grid.NumRows - 1

          If Not GUI_SetupRow(idx_row, db_row) Then
            ' The row can not be added
            Me.Grid.DeleteRowFast(idx_row)
            count = count - 1
          End If
        End If

        m_previous_level = db_row.Value(SQL_COL_HOLDER_LEVEL)
        m_previous_draw = db_row.Value(SQL_COL_DRAW_NAME)
      Catch ex As OutOfMemoryException
        Throw ex

      Catch exception As Exception
        Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        Call Trace.WriteLine(exception.ToString())
        Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                              "frm_report_draw_name", _
                              "GUI_SetupRow", _
                              exception.Message)
        Exit For
      End Try

      db_row = Nothing

      If Not GUI_DoEvents(Me.Grid) Then
        Exit Sub
      End If

    Next

    Call GUI_AfterLastRow()
  End Sub ' GUI_ExecuteQueryCustom

  ' PURPOSE: Get the defined Query Type
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_QUERY_TYPE
  '
  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM
  End Function ' GUI_GetQueryType

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    Call MyBase.GUI_InitControls()

    ' Form name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1810)

    ' Style datagrid
    Call GUI_StyleSheet()

    ' Buttons
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_INFO).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY).Text = GLB_NLS_GUI_CONTROLS.GetString(8)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_RESET).Text = GLB_NLS_GUI_CONTROLS.GetString(7)

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(2)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_EXCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(27)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_PRINT).Text = GLB_NLS_GUI_CONTROLS.GetString(5)

    ' Holder level constants
    m_level_names = mdl_account_for_report.GetLevelNames()

    ' Order filter
    opt_order_account.Text = GLB_NLS_GUI_INVOICING.GetString(115)
    opt_order_draw.Text = GLB_NLS_GUI_INVOICING.GetString(116)
    gb_order.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1892)
    opt_order_draw.Checked = True

    ' Date
    Me.gb_ending_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1574)
    Me.dtp_from.Text = GLB_NLS_GUI_AUDITOR.GetString(257)
    Me.dtp_to.Text = GLB_NLS_GUI_AUDITOR.GetString(258)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Set filter default values
    Call SetDefaultValues()

    ' Draw Name
    Me.ef_draw_name.Text = ""
    Me.ef_draw_name.TextVisible = True
    Me.ef_draw_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_DRAW_NAME)

    opt_draw_name.Checked = True
    opt_draw_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1893)
    opt_draw_list.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1894)

    gb_sorteo.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(494)

    ' Combobox
    InitializeCombobox()

    m_previous_level = -1
    m_previous_draw = ""

    ' DHA 07-MAY-2014
    uc_account_filter.InitExtendedQuery(True)

    'CCG Show Massive Search
    Me.uc_account_filter.ShowMassiveSearch = True

  End Sub ' GUI_InitControls

  ' PURPOSE: Call GUI_ButtonClick with the Cursor in Wait Mode. Finally restore Cursor to Default.
  '
  '  PARAMS:
  '     - INPUT:
  '           - ButtonId As ENUM_BUTTON
  '     - OUTPUT:
  '           -
  '
  ' RETURNS:
  '     -
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId
      Case frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY
        Call MyBase.GUI_ButtonClick(ButtonId)

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub ' GUI_ButtonClick

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS: 
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1892), m_order_name)
    'PrintData.SetFilter("", "", True)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(319), m_draw_name)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), m_account)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_track_data)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), m_holder)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1328), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1329), m_date_to)
    'PrintData.SetFilter("", "", True)
    'PrintData.SetFilter("", "", True)
    'PrintData.SetFilter("", "", True)


    PrintData.FilterHeaderWidth(2) = 1500
    PrintData.FilterValueWidth(2) = 4000

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()
    m_date_from = ""
    m_date_to = ""
    m_draw_name = ""
    m_account = ""
    m_track_data = ""
    m_holder = ""

    ' Search by
    m_order_name = IIf(opt_order_draw.Checked, GLB_NLS_GUI_INVOICING.GetString(116), GLB_NLS_GUI_INVOICING.GetString(115))

    ' Date
    If Me.dtp_from.Checked Then
      m_date_from = GUI_FormatDate(Me.dtp_from.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Me.dtp_to.Checked Then
      m_date_to = GUI_FormatDate(Me.dtp_to.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Account
    m_account = uc_account_filter.Account
    m_track_data = uc_account_filter.TrackData
    m_holder = uc_account_filter.Holder

    ' Draw Name
    m_draw_name = IIf(opt_draw_name.Checked, ef_draw_name.Value, cmd_draw_name.Text)

  End Sub 'GUI_ReportUpdateFilters

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS : 
  '     - True (the row should be added) or False (the row can not be added)
  '
  '   NOTES :
  '
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    ' Account
    Me.Grid.Cell(RowIndex, GRID_COL_ACCOUNT_ID).Value = DbRow.Value(SQL_COL_ACCOUNT_ID)

    ' Card Holder Name
    If Not DbRow.IsNull(SQL_COL_HOLDER_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COL_HOLDER_NAME).Value = DbRow.Value(SQL_COL_HOLDER_NAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_COL_HOLDER_NAME).Value = ""
    End If

    ' Level
    If m_level_names.ContainsKey(DbRow.Value(SQL_COL_HOLDER_LEVEL)) Then
      Me.Grid.Cell(RowIndex, GRID_COL_HOLDER_LEVEL).Value = m_level_names(DbRow.Value(SQL_COL_HOLDER_LEVEL))
    End If

    ' Draw Name
    If Not DbRow.IsNull(SQL_COL_DRAW_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COL_DRAW_NAME).Value = DbRow.Value(SQL_COL_DRAW_NAME)
    End If

    ' Draw Type
    If Not DbRow.IsNull(SQL_COL_DRAW_TYPE) Then
      Me.Grid.Cell(RowIndex, GRID_COL_DRAW_TYPE).Value = GetTypeString(DbRow.Value(SQL_COL_DRAW_TYPE))
    End If

    ' Tickets
    If Not DbRow.IsNull(SQL_COL_NUM_TICKETS) Then
      Me.Grid.Cell(RowIndex, GRID_COL_NUM_TICKETS).Value = GUI_FormatNumber(DbRow.Value(SQL_COL_NUM_TICKETS), 0)

      Me.m_total_tickets_level += GUI_FormatNumber(DbRow.Value(SQL_COL_NUM_TICKETS), 0)
      Me.m_total_tickets_draw += GUI_FormatNumber(DbRow.Value(SQL_COL_NUM_TICKETS), 0)
    End If

    ' Numbers
    If Not DbRow.IsNull(SQL_COL_NUM_NUMBERS) Then
      Me.Grid.Cell(RowIndex, GRID_COL_NUM_NUMBERS).Value = GUI_FormatNumber(DbRow.Value(SQL_COL_NUM_NUMBERS), 0)

      Me.m_total_number_level += GUI_FormatNumber(DbRow.Value(SQL_COL_NUM_NUMBERS), 0)
      Me.m_total_number_draw += GUI_FormatNumber(DbRow.Value(SQL_COL_NUM_NUMBERS), 0)
    End If

    ' First ticket
    If Not DbRow.IsNull(SQL_COL_FIRST_TICKET) Then
      Me.Grid.Cell(RowIndex, GRID_COL_FIRST_TICKET).Value = GUI_FormatDate(DbRow.Value(SQL_COL_FIRST_TICKET), _
                                                                                    ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                    ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Last ticket
    If Not DbRow.IsNull(SQL_COL_LAST_TICKET) Then
      Me.Grid.Cell(RowIndex, GRID_COL_LAST_TICKET).Value = GUI_FormatDate(DbRow.Value(SQL_COL_LAST_TICKET), _
                                                                                    ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                    ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If
    Return True

  End Function ' GUI_SetupRow

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public functions

#Region " Private Functions "
  ' PURPOSE : Reset Total Counters variable
  '
  '  PARAMS :
  '     - INPUT : 
  '           - Total As TYPE_TOTAL_COUNTERS
  '           - Key As String
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub ResetTotalCounters()
    m_total_number_level = 0
    m_total_tickets_level = 0
  End Sub ' ResetTotalCounters

  ' PURPOSE: Initialize combobox
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub InitializeCombobox()
    'Dim _col_autocomplete As AutoCompleteStringCollection
    Dim _sql As StringBuilder

    ' _col_autocomplete = New AutoCompleteStringCollection

    _sql = New StringBuilder()

    _sql.AppendLine(" SELECT   DR_ID")
    _sql.AppendLine("        , DR_NAME ")
    _sql.AppendLine("   FROM   DRAWS")
    _sql.AppendLine("  WHERE   DR_STATUS = 0 ")
    _sql.AppendLine("     OR   (DR_STATUS = 1 AND DATEDIFF(DAY, DR_ENDING_DATE, GETDATE()) < 30) ")
    _sql.AppendLine("  ORDER BY DR_NAME ")

    Dim table As DataTable = GUI_GetTableUsingSQL(_sql.ToString(), 10000)

    'For Each row As DataRow In table.Rows
    '  _col_autocomplete.Add(row(1))
    'Next

    cmd_draw_name.DataSource = New DataView(table)
    cmd_draw_name.DisplayMember = "DR_NAME"
    cmd_draw_name.ValueMember = "DR_ID"
    cmd_draw_name.DropDownStyle = ComboBoxStyle.DropDownList

    'cmd_draw_name.AutoCompleteCustomSource = _col_autocomplete

  End Sub ' InitializeCombobox

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    With Me.Grid
      Call .Init(GRID_COLUMNS_COUNT, GRID_HEADER_ROWS)

      If opt_order_draw.Checked Then
        GRID_COL_DRAW_ID = 1
        GRID_COL_DRAW_NAME = 2
        GRID_COL_DRAW_TYPE = 3
        GRID_COL_HOLDER_LEVEL = 4
        GRID_COL_ACCOUNT_ID = 5
        GRID_COL_HOLDER_NAME = 6
      Else
        GRID_COL_HOLDER_LEVEL = 1
        GRID_COL_ACCOUNT_ID = 2
        GRID_COL_HOLDER_NAME = 3
        GRID_COL_DRAW_ID = 4
        GRID_COL_DRAW_NAME = 5
        GRID_COL_DRAW_TYPE = 6
      End If

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' ACCOUNT_ID
      .Column(GRID_COL_ACCOUNT_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1564)
      .Column(GRID_COL_ACCOUNT_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1579)
      .Column(GRID_COL_ACCOUNT_ID).Width = GRID_WIDTH_ACCOUNT_ID
      .Column(GRID_COL_ACCOUNT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COL_ACCOUNT_ID).Mapping = SQL_COL_ACCOUNT_ID
      .Column(GRID_COL_ACCOUNT_ID).IsMerged = True


      ' HOLDER_NAME
      .Column(GRID_COL_HOLDER_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1564)
      .Column(GRID_COL_HOLDER_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(794)
      .Column(GRID_COL_HOLDER_NAME).Width = GRID_WIDTH_HOLDER_NAME
      .Column(GRID_COL_HOLDER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COL_HOLDER_NAME).Mapping = SQL_COL_HOLDER_NAME
      .Column(GRID_COL_HOLDER_NAME).IsMerged = True

      ' HOLDER_LEVEL
      .Column(GRID_COL_HOLDER_LEVEL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1564)
      .Column(GRID_COL_HOLDER_LEVEL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(443)
      .Column(GRID_COL_HOLDER_LEVEL).Width = GRID_WIDTH_HOLDER_LEVEL
      .Column(GRID_COL_HOLDER_LEVEL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COL_HOLDER_LEVEL).Mapping = SQL_COL_HOLDER_LEVEL
      .Column(GRID_COL_HOLDER_LEVEL).IsMerged = True

      ' DRAW_ID
      .Column(GRID_COL_DRAW_ID).Header(0).Text = " "
      .Column(GRID_COL_DRAW_ID).Header(1).Text = " "
      .Column(GRID_COL_DRAW_ID).Width = GRID_WIDTH_DRAW_ID
      .Column(GRID_COL_DRAW_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COL_DRAW_ID).Mapping = SQL_COL_DRAW_ID

      ' DRAW NAME
      .Column(GRID_COL_DRAW_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(319)
      .Column(GRID_COL_DRAW_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(367)
      .Column(GRID_COL_DRAW_NAME).Width = GRID_WIDTH_DRAW_NAME
      .Column(GRID_COL_DRAW_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COL_DRAW_NAME).Mapping = SQL_COL_DRAW_NAME

      ' DRAW TYPE 
      .Column(GRID_COL_DRAW_TYPE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(319)
      .Column(GRID_COL_DRAW_TYPE).Header(1).Text = GLB_NLS_GUI_CONFIGURATION.GetString(299)
      .Column(GRID_COL_DRAW_TYPE).Width = GRID_WIDTH_DRAW_TYPE
      .Column(GRID_COL_DRAW_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COL_DRAW_TYPE).Mapping = SQL_COL_DRAW_TYPE

      ' NUM_TICKETS
      .Column(GRID_COL_NUM_TICKETS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(324)
      .Column(GRID_COL_NUM_TICKETS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1825)
      .Column(GRID_COL_NUM_TICKETS).Width = GRID_WIDTH_NUM_TICKETS
      .Column(GRID_COL_NUM_TICKETS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COL_NUM_TICKETS).Mapping = SQL_COL_NUM_TICKETS

      ' NUM_NUMBERS
      .Column(GRID_COL_NUM_NUMBERS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(324)
      .Column(GRID_COL_NUM_NUMBERS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1826)
      .Column(GRID_COL_NUM_NUMBERS).Width = GRID_WIDTH_NUM_NUMBERS
      .Column(GRID_COL_NUM_NUMBERS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COL_NUM_NUMBERS).Mapping = SQL_COL_NUM_NUMBERS

      ' FIRST_TICKET
      .Column(GRID_COL_FIRST_TICKET).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(324)
      .Column(GRID_COL_FIRST_TICKET).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1827)
      .Column(GRID_COL_FIRST_TICKET).Width = GRID_WIDTH_FIRST_TICKET
      .Column(GRID_COL_FIRST_TICKET).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COL_FIRST_TICKET).Mapping = SQL_COL_FIRST_TICKET

      ' LAST_TICKET
      .Column(GRID_COL_LAST_TICKET).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(324)
      .Column(GRID_COL_LAST_TICKET).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1828)
      .Column(GRID_COL_LAST_TICKET).Width = GRID_WIDTH_LAST_TICKET
      .Column(GRID_COL_LAST_TICKET).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COL_LAST_TICKET).Mapping = SQL_COL_LAST_TICKET

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Process subtoal value 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ProcesSubTotal(ByVal IdxTotalColumn As Integer, ByVal TotalTickets As Integer, ByVal TotalNumber As Integer, ByVal ENUM_GUI As ControlColor.ENUM_GUI_COLOR, ByVal TextTotal As String)
    If Me.opt_order_draw.Checked Then
      If Me.Grid.NumRows Then
        Dim idx_row As Integer = Me.Grid.NumRows

        Me.Grid.AddRow()

        ' Label - TOTAL
        Me.Grid.Cell(idx_row, IdxTotalColumn).Value = TextTotal

        ' Total - Tickets
        Me.Grid.Cell(idx_row, GRID_COL_NUM_TICKETS).Value = TotalTickets

        ' Total - Numbers
        Me.Grid.Cell(idx_row, GRID_COL_NUM_NUMBERS).Value = TotalNumber

        ' Color Row
        Me.Grid.Row(idx_row).BackColor = GetColor(ENUM_GUI)
      End If
    End If
  End Sub ' ProcesSubTotal

  ' PURPOSE: Process draw type text 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Draw type text
  Private Function GetTypeString(ByVal Type As Integer) As String

    Select Case Type
      Case WSI.Common.DrawType.BY_PLAYED_CREDIT
        Return GLB_NLS_GUI_CONFIGURATION.GetString(302)
      Case WSI.Common.DrawType.BY_REDEEMABLE_SPENT
        Return GLB_NLS_GUI_CONFIGURATION.GetString(303)
      Case WSI.Common.DrawType.BY_TOTAL_SPENT
        Return GLB_NLS_GUI_CONFIGURATION.GetString(313)
      Case WSI.Common.DrawType.BY_POINTS
        Return GLB_NLS_GUI_CONFIGURATION.GetString(329)
      Case WSI.Common.DrawType.BY_CASH_IN
        Return GLB_NLS_GUI_CONFIGURATION.GetString(330)
      Case WSI.Common.DrawType.BY_CASH_IN_POSTERIORI
        Return GLB_NLS_GUI_CONFIGURATION.GetString(422)
      Case Else
        Return ""
    End Select
  End Function ' GetTypeString

  ' PURPOSE : Process total counters: Draw row of totals, update and reset total counters.
  '
  '  PARAMS :
  '     - INPUT : 
  '           - DbRow As CLASS_DB_ROW
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub ProcessCounters(ByVal DbRow As CLASS_DB_ROW)
    If opt_order_draw.Checked AndAlso (m_previous_level <> -1 AndAlso m_previous_level <> DbRow.Value(SQL_COL_HOLDER_LEVEL)) OrElse (Not String.IsNullOrEmpty(m_previous_draw) AndAlso Not m_previous_draw.Equals(DbRow.Value(SQL_COL_DRAW_NAME))) Then
      Dim _text_subtotal As String
      _text_subtotal = GLB_NLS_GUI_INVOICING.GetString(375)

      ProcesSubTotal(GRID_COL_HOLDER_LEVEL, m_total_tickets_level, m_total_number_level, ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01, _text_subtotal)

      ' Reset values after row has been inserted
      m_total_tickets_level = 0
      m_total_number_level = 0
    End If

    If opt_order_draw.Checked AndAlso Not String.IsNullOrEmpty(m_previous_draw) AndAlso Not m_previous_draw.Equals(DbRow.Value(SQL_COL_DRAW_NAME)) Then
      Dim _text_total As String
      _text_total = GLB_NLS_GUI_INVOICING.GetString(205)

      ProcesSubTotal(GRID_COL_DRAW_NAME, m_total_tickets_draw, m_total_number_draw, ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00, _text_total)

      ' Reset values after row has been inserted
      m_total_tickets_draw = 0
      m_total_number_draw = 0
      m_previous_level = -1
      m_previous_draw = -1
    End If
  End Sub ' ProcessCounters

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()
    ' Dates
    Me.dtp_from.Value = New DateTime(Now.Year, Now.Month, Now.Day, 0, 0, 0)
    Me.dtp_from.Checked = False
    Me.dtp_to.Value = Me.dtp_from.Value.AddDays(1)
    Me.dtp_to.Checked = False

    ' Account
    uc_account_filter.Clear()

    ' Draw Name
    ef_draw_name.Value = String.Empty

    If cmd_draw_name.Items.Count > 0 Then
      cmd_draw_name.SelectedIndex = 0
    End If

    opt_draw_name.Checked = True
    opt_order_draw.Checked = True

  End Sub ' SetDefaultValues

  Private Sub cmd_draw_name_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd_draw_name.TextChanged
    cmd_draw_name.DroppedDown = False
  End Sub ' cmd_draw_name_TextChanged

  Private Sub EnableDrawFields()
    If opt_draw_list.Checked Then
      ef_draw_name.Enabled = False
      cmd_draw_name.Enabled = True
    ElseIf opt_draw_name.Checked Then
      ef_draw_name.Enabled = True
      cmd_draw_name.Enabled = False
    End If
  End Sub

  Private Sub rdDrawList_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_draw_list.CheckedChanged
    EnableDrawFields()
  End Sub

  Private Sub rdDrawName_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_draw_name.CheckedChanged
    EnableDrawFields()
  End Sub
#End Region ' Private Functions
End Class