'-------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME :       frm_mobile_bank_edit_device.vb
'
' DESCRIPTION :       Window for edit a mobile bank
'
' AUTHOR:             Joan Benito
'
' CREATION DATE:      10-NOV-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 03-NOV-2014  JBM    Initial version   
'--------------------------------------------------------------------

Option Strict Off
Option Explicit On

#Region "Imports"
Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_CommonOperations.CLASS_BASE
Imports WSI.Common
#End Region ' Imports

Public Class frm_mobile_bank_edit_device
  Inherits frm_base_edit

#Region " Constants "
  Private Const LEN_EMPLOYEE_CODE As Integer = 40

#End Region ' Constants

#Region " Members "
  Private m_mobile_bank As New cls_mobile_bank
  Private m_mobile_bank_previous As New cls_mobile_bank
  Private m_sql_ctx As Integer
  Private m_barcode_handler As KbdMsgFilterHandler
  Dim m_kbdmsg_disabled As Boolean = False
  Dim m_kbdmsg_received As Boolean = False

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_MOBILE_BANK_EDIT

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5688) '  "Edit mobile bank"

    ' PIN
    Me.ef_pin.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(747) '  "PIN"
    Me.ef_pin.Password = True
    Me.ef_pin.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, 4, 0)
    Me.ef_pin.TextAlign = HorizontalAlignment.Right

    ' CARD
    Me.ef_card_mb.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6743) ' "Tarjeta"
    Me.ef_card_mb.TextAlign = HorizontalAlignment.Right
    'SDS 29-05-2018 bug:WIGOS-12268
    Me.ef_card_mb.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TRACK_DATA, TRACK_DATA_LENGTH_WIN)

    ' Group Limits
    Me.gb_limits.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5689) ' 5689 - L�mites m�ximos de recarga

    ' Limit by recharge
    Me.ef_limit_recharge.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 12, 2)
    Me.chk_limit_recharge.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5690) ' 5690 "Maximum recharge"
    Me.chk_limit_recharge.Checked = False
    Me.ef_limit_recharge.Enabled = False

    ' Limit by number of recharge
    Me.ef_limit_num_recharges.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 6, 0)
    Me.chk_limit_num_recharges.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5691) ' 5691 - Number of recharges by session
    Me.chk_limit_num_recharges.Checked = False
    Me.ef_limit_num_recharges.Enabled = False

    ' Limit by session
    Me.ef_limit_by_session.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 12, 2)
    Me.chk_limit_by_session.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5698) ' 5698 - Total recharging by session
    Me.chk_limit_by_session.Checked = False
    Me.ef_limit_by_session.Enabled = False

    ' Lock mobile bank
    Me.chk_lock_mobile_bank.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8999) ' Lock mobile bank"
    Me.chk_lock_mobile_bank.Checked = False

    AddHandler chk_limit_by_session.CheckedChanged, AddressOf checked_event
    AddHandler chk_limit_num_recharges.CheckedChanged, AddressOf checked_event
    AddHandler chk_limit_recharge.CheckedChanged, AddressOf checked_event

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Visible = False

    'SDS 29-05-2018 bug:WIGOS-12268
    If Not m_kbdmsg_disabled Then
      m_barcode_handler = AddressOf Me.BarcodeEvent
      KeyboardMessageFilter.RegisterHandler(Me, BarcodeType.Account, m_barcode_handler)
    End If

  End Sub ' GUI_InitControls

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    m_sql_ctx = SqlCtx

    ' Editable fields
    If ((Not String.IsNullOrEmpty(m_mobile_bank.NumberOfRechargesLimit)) AndAlso (GUI_FormatNumber(m_mobile_bank.NumberOfRechargesLimit, 0))) Then
      Me.ef_limit_num_recharges.Value = GUI_FormatNumber(m_mobile_bank.NumberOfRechargesLimit, 0)
      Me.chk_limit_num_recharges.Checked = True
      Me.ef_limit_num_recharges.Enabled = True
    End If

    If ((Not m_mobile_bank.RechargeLimit.SqlMoney.IsNull) AndAlso (GUI_FormatCurrency(m_mobile_bank.RechargeLimit, 2) > 0)) Then
      Me.ef_limit_recharge.Value = GUI_FormatCurrency(m_mobile_bank.RechargeLimit, 2)
      Me.ef_limit_recharge.Enabled = True
      Me.chk_limit_recharge.Checked = True
    End If

    If ((Not m_mobile_bank.TotalLimit.SqlMoney.IsNull) AndAlso (GUI_FormatCurrency(m_mobile_bank.TotalLimit, 2) > 0)) Then
      Me.ef_limit_by_session.Value = GUI_FormatCurrency(m_mobile_bank.TotalLimit, 2)
      Me.ef_limit_by_session.Enabled = True
      Me.chk_limit_by_session.Checked = True
    End If

    Me.ef_pin.Value = m_mobile_bank.Pin
    Me.ef_card_mb.Value = m_mobile_bank.TrackData

    Me.chk_lock_mobile_bank.Checked = m_mobile_bank.Blocked

    'Events
    AddHandler chk_limit_by_session.CheckedChanged, AddressOf checked_event
    AddHandler chk_limit_num_recharges.CheckedChanged, AddressOf checked_event
    AddHandler chk_limit_recharge.CheckedChanged, AddressOf checked_event
  End Sub ' GUI_SetScreenData

  ' PURPOSE: Process button actions in order to branch to a child screen
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)
    Select Case ButtonId
      Case frm_base_sel.ENUM_BUTTON.BUTTON_OK
        'ACCEPT
        Me.m_mobile_bank_previous = Me.m_mobile_bank.Duplicate()

        If GUI_IsScreenDataOk() Then
          'Accept changes
          Call Me.GUI_GetScreenData()

          If (Me.CardInUse()) AndAlso (Me.m_mobile_bank_previous.TrackData <> Me.m_mobile_bank.TrackData) Then
            Me.m_mobile_bank = Me.m_mobile_bank_previous
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8801), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , )
          Else
            DbReadObject = Me.m_mobile_bank
            DbEditedObject = Me.m_mobile_bank
            MyBase.Close()
          End If
        End If

      Case frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL
        'Cancel
        Me.m_mobile_bank_previous = Me.m_mobile_bank.Duplicate()

        Call Me.GUI_GetScreenData()
        'Notice client that he will lost data
        If (m_mobile_bank.AuditorData.IsEqual(m_mobile_bank_previous.AuditorData)) Then
          MyBase.DbReadObject = Nothing
          MyBase.DbEditedObject = Nothing
        Else
          MyBase.DbReadObject = Me.m_mobile_bank
          MyBase.DbEditedObject = Me.m_mobile_bank_previous
        End If

        MyBase.Close()
        Me.m_mobile_bank = Me.m_mobile_bank_previous

      Case Else
        MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Get data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_GetScreenData()
    If (Me.chk_limit_num_recharges.Checked AndAlso (Not String.IsNullOrEmpty(Me.ef_limit_num_recharges.Value))) Then
      m_mobile_bank.NumberOfRechargesLimit = GUI_FormatNumber(Me.ef_limit_num_recharges.Value, 0)
    Else
      m_mobile_bank.NumberOfRechargesLimit = GUI_FormatNumber(0, 0)
    End If

    If (Me.chk_limit_recharge.Checked AndAlso (Not String.IsNullOrEmpty(Me.ef_limit_recharge.Value))) Then
      m_mobile_bank.RechargeLimit = GUI_FormatCurrency(Me.ef_limit_recharge.Value, 2)
    Else
      m_mobile_bank.RechargeLimit = GUI_FormatCurrency(0, 2)
    End If

    If (Me.chk_limit_by_session.Checked AndAlso (Not String.IsNullOrEmpty(Me.ef_limit_by_session.Value))) Then
      m_mobile_bank.TotalLimit = GUI_FormatCurrency(Me.ef_limit_by_session.Value, 2)
    Else
      m_mobile_bank.TotalLimit = GUI_FormatCurrency(0, 2)
    End If

    m_mobile_bank.Pin = Me.ef_pin.Value
    m_mobile_bank.TrackData = Me.ef_card_mb.Value
    m_mobile_bank.Blocked = Me.chk_lock_mobile_bank.Checked
  End Sub ' GUI_GetScreenData

  ' PURPOSE: Check all data in the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - True if all is OK / False if any field is KO
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    ' PIN is not empty and is numeric and has 4 digits
    If String.IsNullOrEmpty(Me.ef_pin.Value) Then
      ' 1302  "Field '%1' must not be left blank."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.ef_pin.Text)
      Call ef_pin.Focus()
      Return False
    End If

    If Not WSI.Common.ValidateFormat.RawNumber(Me.ef_pin.Value) Then
      ' "The format of the field %1 is not correct."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.ef_pin.Text)
      Call Me.ef_pin.Focus()
      Return False
    End If

    If Not Me.ef_pin.Value.Length = 4 Then
      ' 5699 - �PIN code must be @p0 digits long.�
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5699), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , 4)
      Call Me.ef_pin.Focus()
      Return False
    End If

    ' Limit global is not empty and > 0 and > Limit recharge
    If Me.chk_limit_by_session.Checked Then
      If String.IsNullOrEmpty(Me.ef_limit_by_session.Value) Then
        ' 1302  "Field '%1' must not be left blank."
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.chk_limit_by_session.Text)
        Call ef_limit_by_session.Focus()
        Return False
      End If

      If GUI_ParseCurrency(Me.ef_limit_by_session.Value) <= 0 Then
        ' 125  "You must enter a %1 value greater than 0."
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(125), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.chk_limit_by_session.Text, 0)
        Call ef_limit_by_session.Focus()
        Return False
      End If

      If Me.chk_limit_recharge.Checked Then
        If GUI_ParseCurrency(Me.ef_limit_by_session.Value) < GUI_ParseCurrency(Me.ef_limit_recharge.Value) Then
          ' 1195  '%1' field value must be greater or equal than %2.
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1195), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.chk_limit_by_session.Text, Me.chk_limit_recharge.Text)
          Call ef_limit_by_session.Focus()
          Return False
        End If
      End If
    End If

    ' Limit for recharge is not empty  and > 0
    If Me.chk_limit_recharge.Checked Then
      If String.IsNullOrEmpty(Me.ef_limit_recharge.Value) Then
        ' 1302  "Field '%1' must not be left blank."
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.chk_limit_recharge.Text)
        Call ef_limit_recharge.Focus()
        Return False
      End If

      If GUI_ParseCurrency(Me.ef_limit_recharge.Value) <= 0 Then
        ' 125  "You must enter a %1 value greater than 0."
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(125), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.chk_limit_recharge.Text, 0)
        Call ef_limit_recharge.Focus()
        Return False
      End If
    End If

    ' Limit for num of recharge is not empty  and > 0
    If Me.chk_limit_num_recharges.Checked Then
      If String.IsNullOrEmpty(Me.ef_limit_num_recharges.Value) Then
        ' 1302  "Field '%1' must not be left blank."
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.chk_limit_num_recharges.Text)
        Call ef_limit_num_recharges.Focus()
        Return False
      End If

      If GUI_ParseNumber(Me.ef_limit_num_recharges.Value) <= 0 Then
        ' 125  "You must enter a %1 value greater than 0."
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(125), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.chk_limit_num_recharges.Text, 0)
        Call ef_limit_num_recharges.Focus()
        Return False
      End If
    End If

    If (Me.ef_card_mb.Value.Length <> 0 AndAlso Me.ef_card_mb.Value.Length <> 20) Then
      ' invalid track data number
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5579), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , )
      Call ef_card_mb.Focus()
      Return False
    End If


    If (Me.ef_card_mb.Value.Length <> 0) Then
      If Not m_mobile_bank.IsTrackDataValid(Me.ef_card_mb.Value) Then
        ' invalid track data number
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5579), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , )
        Call ef_card_mb.Focus()
        Return False
      End If
    End If

    Return True
  End Function ' GUI_IsScreenDataOk

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = m_mobile_bank

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

#End Region ' OVERRIDES

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode with filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - MdiParent
  '           - Mobile Bank ID
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window, _
                         ByVal ProfileId As Integer, ByVal username As String, ByRef MobileBank As cls_mobile_bank)

    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
    Me.m_mobile_bank_previous = New cls_mobile_bank

    Me.m_mobile_bank = MobileBank
    Me.DbObjectId = Me.m_mobile_bank.ID

    Dim _simulatedCurrentUser As CLASS_GUI_USER
    _simulatedCurrentUser = New CLASS_GUI_USER

    If (Me.m_mobile_bank.UserId <> 0) Then
      Call _simulatedCurrentUser.GetUserInDb(username, m_sql_ctx)
    End If

    'Call _simulatedCurrentUser.GetSpecificUserPermissions(ENUM_GUI.SMARTFLOOR, m_sql_ctx)
    'Call _simulatedCurrentUser.GetSpecificProfilePermissions(ENUM_GUI.CASHIER, ProfileId, m_sql_ctx)
    Call _simulatedCurrentUser.GetSpecificProfilePermissions(ENUM_GUI.WIGOS_GUI, ProfileId, m_sql_ctx)

    'If (Not _simulatedCurrentUser.PermissionsOtherUser(ENUM_CASHIER_FORM.FORM_MOBIBANK_ACCESS).Read) Then
    If (Not _simulatedCurrentUser.PermissionsOtherUser(ENUM_FORM.FORM_MOBIBANK_USER_CONFIG).Read) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8770), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , )

      Return
    End If

    Call Me.Display(True)
    MobileBank = Me.m_mobile_bank
  End Sub ' ShowForEdit

  Public Sub ReadMobileBank(ByRef MobileBank As cls_mobile_bank)

    Me.m_mobile_bank = MobileBank
    Me.DbObjectId = Me.m_mobile_bank.ID

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)

    If Me.DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    MobileBank = Me.m_mobile_bank
  End Sub

  Public Sub InsertMobileBank(ByRef MobileBank As cls_mobile_bank)
    Me.m_mobile_bank = MobileBank
    Me.DbObjectId = Me.m_mobile_bank.ID

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_INSERT)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_INSERT)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT)
  End Sub

  Public Sub UpdateMobileBank(ByRef MobileBank As cls_mobile_bank)

    Me.m_mobile_bank = MobileBank
    Me.DbObjectId = Me.m_mobile_bank.ID

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_UPDATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE)
  End Sub

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: know if card is in use
  '
  ' RETURNS:
  '     - Boolean: with card is in use or not
  Private Function CardInUse() As Boolean

    Return WSI.Common.MobileBank.CardInUse(Me.m_mobile_bank.TrackData)

  End Function

#End Region ' Private Functions

#Region " Events"

  ' PURPOSE: Handler to enable or disable entry filed whose checkbox is checked
  '
  '  PARAMS:
  '     - INPUT:
  '           - Sender Object
  '           - Event Args
  '     - OUTPUT:
  '           - None
  ' RETURNS:
  '     
  Private Sub checked_event(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _object As CheckBox = sender

    ' Get check box
    Select Case _object.Text
      Case Me.chk_limit_by_session.Text
        Me.ef_limit_by_session.Enabled = Me.chk_limit_by_session.Checked
        If String.IsNullOrEmpty(Me.ef_limit_by_session.Value) Then
          Me.ef_limit_by_session.Value = 0
        End If

      Case Me.chk_limit_num_recharges.Text
        Me.ef_limit_num_recharges.Enabled = Me.chk_limit_num_recharges.Checked
        If String.IsNullOrEmpty(Me.ef_limit_num_recharges.Value) Then
          Me.ef_limit_num_recharges.Value = 0
        End If

      Case Me.chk_limit_recharge.Text
        Me.ef_limit_recharge.Enabled = Me.chk_limit_recharge.Checked
        If String.IsNullOrEmpty(Me.ef_limit_recharge.Value) Then
          Me.ef_limit_recharge.Value = 0
        End If

      Case Else
        ' Do nothing - Unknown check
    End Select

  End Sub ' rb_checked_event
  'SDS 29-05-2018 bug:WIGOS-12268
  Public Sub BarcodeEvent(ByVal e As KbdMsgEvent, ByVal Data As Object)
    Try
      If Not Data Is Nothing Then
        Dim _data_read As WSI.Common.Barcode = DirectCast(Data, WSI.Common.Barcode)
        Me.ef_card_mb.TextValue = _data_read.ExternalCode
        m_kbdmsg_received = True
        Me.Focus()
      End If
    Catch ex As Exception
    End Try
  End Sub

  'SDS 29-05-2018 bug:WIGOS-12268
  Private Sub frm_mobile_bank_edit_device_Closing(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    If Not m_kbdmsg_disabled Then
      KeyboardMessageFilter.UnregisterHandler(Me, m_barcode_handler)
    End If
  End Sub
#End Region ' Events


End Class