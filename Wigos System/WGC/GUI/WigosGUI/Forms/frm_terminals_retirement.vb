'-------------------------------------------------------------------
' Copyright � 2011 Win Systems Intl.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_terminals_retirement.vb
'
' DESCRIPTION : Multiple terminals retirement dialog
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 08-MAR-2011  TJG    First version
' 09-JAN-2014  ICS    Close open play sessions when terminal is retired
' 04-MAY-2015  FJC    Fixed Bug WIG-2270:
' 09-MAR-2016  FAV   Fixed Bug 9552: Validate that the money collection is not open when the Terminal is going to be to retired
' 16-MAR-2016  FAV   Fixed Bug 9557: The cashier session for the pending money collection should change to pending closing
'-------------------------------------------------------------------
Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports WSI.Common

Public Class frm_terminals_retirement
  Inherits GUI_Controls.frm_base_edit

#Region " Members "

  Private m_terminals_list As New CLASS_TERMINAL_LIST
  Private m_play_sessions_to_close As List(Of CLASS_PLAY_SESSION.TYPE_PLAY_SESSION)
  Private m_terminals_to_pending As List(Of Int32)

#End Region ' Members

#Region " Constants "

  Private Const GRID_COL_TERMINAL_ID As Integer = 0
  Private Const GRID_COL_STATUS As Integer = 1
  Private Const GRID_COL_RETIREMENT_DATE As Integer = 2

#End Region

#Region " Properties"
#End Region ' Properties

#Region " Overrides "

  ' PURPOSE : Initializes the form id.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_TERMINALS_RETIREMENT

    Call MyBase.GUI_SetFormId()

  End Sub

  ' PURPOSE : Form controls initialization.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_AUDITOR.GetString(469)                          ' 469 "Desconectar Terminales"

    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False

    ' RCI 17-JUN-2011: Needs Write AND Delete permissions to retire terminals.
    GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = Me.Permissions.Write And Me.Permissions.Delete

    dtp_retirement_date.Text = GLB_NLS_GUI_CONFIGURATION.GetString(455)   ' 455 "Fecha de Retirada"
    dtp_retirement_date.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, 0)

    With uc_grid_terminals
      Call .Init(3, 1)

      ' Index
      .Column(GRID_COL_TERMINAL_ID).Header(0).Text = GLB_NLS_GUI_AUDITOR.GetString(333)             ' 333 "Terminal"
      .Column(GRID_COL_TERMINAL_ID).Header(0).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COL_TERMINAL_ID).Width = 2600
      .Column(GRID_COL_TERMINAL_ID).HighLightWhenSelected = False
      .Column(GRID_COL_TERMINAL_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      .Column(GRID_COL_STATUS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(262)          ' 262 "Estado"
      .Column(GRID_COL_STATUS).Header(0).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COL_STATUS).Width = 1600
      .Column(GRID_COL_STATUS).HighLightWhenSelected = False
      .Column(GRID_COL_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      .Column(GRID_COL_RETIREMENT_DATE).Header(0).Text = GLB_NLS_GUI_CONFIGURATION.GetString(455)   ' 455 "Fecha de Retirada"
      .Column(GRID_COL_RETIREMENT_DATE).Header(0).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COL_RETIREMENT_DATE).Width = 1790
      .Column(GRID_COL_RETIREMENT_DATE).HighLightWhenSelected = False
      .Column(GRID_COL_RETIREMENT_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    End With

    m_terminals_to_pending = New List(Of Int32)()

  End Sub ' GUI_InitControls

  ' PURPOSE : Validate the data presented on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - True :  Data is ok
  '     - False : Data is NOT ok

  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Dim _num_play_sessions As Integer
    Dim _idx_terminal As Integer
    Dim _read_list As CLASS_TERMINAL_LIST
    Dim _aux_date As Date
    Dim _max_started As Date
    Dim _max_finished As Date

    _aux_date = WSI.Common.Misc.TodayOpening.AddDays(1)

    ' Selected retirement date can not be later than tomorrow's working day
    If dtp_retirement_date.Value > _aux_date Then
      ' 198 "La fecha de retirada seleccionada no puede ser posterior a la jornada de ma�ana."
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(198), _
                      ENUM_MB_TYPE.MB_TYPE_WARNING, _
                      ENUM_MB_BTN.MB_BTN_OK, _
                      ENUM_MB_DEF_BTN.MB_DEF_BTN_1)

      Return False
    End If

    ' Check the retirement date for all selected terminals
    '     - Selected retirement date can not be later than previous terminal's retirement date
    '     - The terminal can not have play sessions after the working day related to the selected retirement date

    _read_list = DbReadObject

    For _idx_terminal = 0 To _read_list.Count - 1
      ' Get the retirement date selected for the terminal
      If dtp_retirement_date.Checked Then
        _aux_date = dtp_retirement_date.Value
      Else
        _aux_date = uc_grid_terminals.Cell(_idx_terminal, GRID_COL_RETIREMENT_DATE).Value()
      End If

      '   - Selected retirement date can not be later than previous terminal's retirement date
      If _read_list.Item(_idx_terminal).Status = WSI.Common.TerminalStatus.RETIRED _
     And _read_list.Item(_idx_terminal).RetirementDate <> Nothing Then
        If _aux_date > _read_list.Item(_idx_terminal).RetirementDate Then
          ' 199 "La fecha de retirada seleccionada debe ser anterior a la fecha de retirada (%2) del terminal %1."
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(199), _
                          ENUM_MB_TYPE.MB_TYPE_WARNING, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _read_list.Item(_idx_terminal).Name, _
                          _read_list.Item(_idx_terminal).RetirementDate)

          Return False
        End If
      End If

      '   - The terminal can not have play sessions after the working day related to the selected retirement date
      _num_play_sessions = CountPlaySessions(_read_list.Item(_idx_terminal).TerminalId, _
                                             WSI.Common.Misc.Opening(_aux_date.AddDays(1)), _
                                             _max_started, _
                                             _max_finished)
      If _num_play_sessions > 0 Then
        ' 200 "La fecha de retirada seleccionada debe ser posterior a la �ltima sesi�n de juego del terminal %1."
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(200), _
                    ENUM_MB_TYPE.MB_TYPE_WARNING, _
                    ENUM_MB_BTN.MB_BTN_OK, _
                    ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                    _read_list.Item(_idx_terminal).Name)

        Return False
      End If

      'FAV 09-MAR-2016 It checks if there is a money_collection in Open status for the Terminal
      Dim _current_money_collection As WSI.Common.TITO.MoneyCollection
      Using _db_trx As New WSI.Common.DB_TRX()
        _current_money_collection = WSI.Common.TITO.MoneyCollection.DB_ReadCurrentMoneyCollection(_read_list.Item(_idx_terminal).TerminalId, _db_trx.SqlTransaction)

        If (Not _current_money_collection Is Nothing) Then
          If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7180), _
                      ENUM_MB_TYPE.MB_TYPE_WARNING, _
                      ENUM_MB_BTN.MB_BTN_YES_NO, _
                      ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                      _current_money_collection.StackerId, _
                      _read_list.Item(_idx_terminal).Name) = ENUM_MB_RESULT.MB_RESULT_YES Then

            m_terminals_to_pending.Add(_read_list.Item(_idx_terminal).TerminalId)
          Else
            Return False
          End If
        End If
      End Using
    Next

    Return True

  End Function ' GUI_IsScreenDataOk

  ' PURPOSE : Get data from the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_GetScreenData()

    Dim _idx_terminal As Integer
    Dim _edit_list As CLASS_TERMINAL_LIST
    Dim _terminal_open_play_sessions As List(Of CLASS_PLAY_SESSION.TYPE_PLAY_SESSION)

    _edit_list = DbEditedObject

    m_play_sessions_to_close = New List(Of CLASS_PLAY_SESSION.TYPE_PLAY_SESSION)()

    ' Terminals were inserted in uc_grid_terminals in the same order than in the list of terminals
    For _idx_terminal = 0 To _edit_list.Count - 1
      _edit_list.Item(_idx_terminal).Status = WSI.Common.TerminalStatus.RETIRED
      _edit_list.Item(_idx_terminal).ExternalId = ""
      _edit_list.Item(_idx_terminal).MachineSerialNumber = String.Empty
      _edit_list.Item(_idx_terminal).SerialNumber = String.Empty
      _edit_list.Item(_idx_terminal).AssetNumber = -1
      _edit_list.Item(_idx_terminal).MachineAssetNumber = -1

      If dtp_retirement_date.Checked Then
        _edit_list.Item(_idx_terminal).RetirementDate = dtp_retirement_date.Value
      Else
        _edit_list.Item(_idx_terminal).RetirementDate = uc_grid_terminals.Cell(_idx_terminal, GRID_COL_RETIREMENT_DATE).Value()
      End If

      _edit_list.Item(_idx_terminal).RetirementRequested = Nothing       ' Force the update of TE_RETIREMENT_REQUESTED

      _edit_list.Item(_idx_terminal).RetirementPlaySessions = CLASS_TERMINAL.ENUM_RETIREMENT_PLAY_SESSIONS_STATUS.NONE

      ' Check if there are still open play sessions
      _terminal_open_play_sessions = GetOpenPlaySessions(_edit_list.Item(_idx_terminal).TerminalId)
      If _terminal_open_play_sessions.Count > 0 Then
        If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4441), _
                      ENUM_MB_TYPE.MB_TYPE_WARNING, _
                      ENUM_MB_BTN.MB_BTN_YES_NO, _
                      ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                      _edit_list.Item(_idx_terminal).Name) = ENUM_MB_RESULT.MB_RESULT_YES Then

          m_play_sessions_to_close.AddRange(_terminal_open_play_sessions)
          _edit_list.Item(_idx_terminal).RetirementPlaySessions = CLASS_TERMINAL.ENUM_RETIREMENT_PLAY_SESSIONS_STATUS.CLOSE
        Else
          _edit_list.Item(_idx_terminal).RetirementPlaySessions = CLASS_TERMINAL.ENUM_RETIREMENT_PLAY_SESSIONS_STATUS.REMAIN_OPEN
        End If
      End If

    Next

  End Sub ' GUI_GetScreenData

  ' PURPOSE : Set initial data on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _read_list As CLASS_TERMINAL_LIST
    Dim _idx_terminal As Integer
    Dim _idx_row As Integer
    Dim _retirement_date As DateTime
    Dim _common_retirement_date As DateTime

    _read_list = DbReadObject

    _retirement_date = Nothing
    _common_retirement_date = Nothing

    For _idx_terminal = 0 To _read_list.Count - 1
      uc_grid_terminals.AddRow()

      _idx_row = uc_grid_terminals.NumRows - 1

      ' Terminals are inserted in uc_grid_terminals in the same order than in the list of terminals
      uc_grid_terminals.Cell(_idx_row, GRID_COL_TERMINAL_ID).Value = _read_list.Item(_idx_terminal).Name
      uc_grid_terminals.Cell(_idx_row, GRID_COL_STATUS).Value = CLASS_TERMINAL.StatusName(_read_list.Item(_idx_terminal).Status)

      If _read_list.Item(_idx_terminal).Status = WSI.Common.TerminalStatus.RETIRED Then
        If _read_list.Item(_idx_terminal).RetirementDate = Nothing Then
          _retirement_date = SuggestedRetirementDate(_read_list.Item(_idx_terminal).TerminalId)
        Else
          _retirement_date = CDate(_read_list.Item(_idx_terminal).RetirementDate.ToShortDateString())
        End If
      Else
        _retirement_date = SuggestedRetirementDate(_read_list.Item(_idx_terminal).TerminalId)
      End If

      uc_grid_terminals.Cell(_idx_row, GRID_COL_RETIREMENT_DATE).Value = _retirement_date

      If _common_retirement_date = Nothing Then
        _common_retirement_date = _retirement_date
      Else
        If Date.Compare(_retirement_date, _common_retirement_date) < 0 Then
          _common_retirement_date = _retirement_date
        End If
      End If
    Next

    ' Set a date with no time
    dtp_retirement_date.Value = CDate(_common_retirement_date.ToShortDateString())
    dtp_retirement_date.Checked = False

  End Sub ' GUI_SetScreenData

  ' PURPOSE : Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)
    Dim _idx_terminal As Integer

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = m_terminals_list.Duplicate

        DbStatus = ENUM_STATUS.STATUS_OK

        'Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ
        'Case ENUM_DB_OPERATION.DB_OPERATION_READ
        'Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        'Case ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE
        'Case ENUM_DB_OPERATION.DB_OPERATION_UPDATE
        'Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If m_play_sessions_to_close.Count > 0 Then
          ClosePlaySessions(m_play_sessions_to_close)
        End If

        'FAV 09-MAR-2016 It changes status to Pending
        If (m_terminals_to_pending.Count > 0) Then
          Using _db_trx As New WSI.Common.DB_TRX()
            For _idx_terminal = 0 To m_terminals_to_pending.Count - 1

              If (WSI.Common.TITO.TITO_ChangeStacker.ChangeStackerInTitoMode(m_terminals_to_pending(_idx_terminal), Nothing, 0, False, _db_trx.SqlTransaction) _ 
                  AND Cashier.WCPCashierSessionPendingClosing(m_terminals_to_pending(_idx_terminal), _db_trx.SqlTransaction) <> MB_CASHIER_SESSION_CLOSE_STATUS.ERROR) Then
                _db_trx.Commit()
              Else
                Log.Error(String.Format("The operation to change the Stacker to Pending to collect has returned an error. TerminalId [{0}]", m_terminals_to_pending(_idx_terminal)))
              End If
            Next
          End Using
        End If

        Call MyBase.GUI_DB_Operation(DbOperation)

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub 'GUI_DB_Operation

  ' PURPOSE : Manage permissions.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)

  End Sub 'GUI_Permissions

  ' PURPOSE : Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.dtp_retirement_date

  End Sub 'GUI_SetInitialFocus

  ' PURPOSE : Init form in edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '         - SelectedCollection 
  '         - SelectedTerminals 
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Public Overloads Sub ShowEditItem(ByRef SelectedCollection As Collection, _
                                    ByRef SelectedTerminals As DataView)
    Dim _idx_row As Integer
    Dim _sel_row As DataRowView

    ' Add any initialization after the InitializeComponent() call

    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    ' Load the list of Terminal Ids
    For Each _idx_row In SelectedCollection
      _sel_row = SelectedTerminals(_idx_row)
      m_terminals_list.AddId(_sel_row.Item("TE_TERMINAL_ID"))
    Next

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub

#End Region ' Overrides

#Region " Public Functions "
#End Region

#Region " Private functions "
#End Region

End Class