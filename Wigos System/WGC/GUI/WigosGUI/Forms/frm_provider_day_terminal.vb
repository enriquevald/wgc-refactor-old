'-------------------------------------------------------------------
' Copyright � 2007 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_provider_day_terminal
' DESCRIPTION:   This screen allows to view the providers activity:
'                           
'
' AUTHOR:        Santi Lopez
' CREATION DATE: 18-NOV-2008
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 18-NOV-2008  SLE    Initial version
' 04-AUG-2011  RCI    Added the possibility to show individual terminal activity
' 08-JUN-2012  JAB    DoEvents run each second.
' 12-JUN-2012  JAB    DoEvents run each second (use common routine).
' 17-JUL-2012  RCI    Fixed Bug #321: The columns Provider and Terminal must be sorted by name.
' 24-AUG-2012  JAB    DoEvents run each second.
' 19-MAR-2013  AMF    Fixed Bug #649: Error when terminal name has an apostrophe
' 19-APR-2013  RBG    Eliminated credit type filter and included information in new columns.
' 06-MAY-2013  RBG    Fixed Bug #766: Export shows Credit Type Filter and it doesn't exists.  
' 08-AUG-2013  FBA    Forces use of one INDEX or another depending on the number of terminals selected.
' 08-JAN-2014  RMS    Added support for gaming tables with a virtual provider
' 13-FEB-2014  DLL    Fixed Bug WIG-618: gambling tables not showed at providers activity
' 19-FEB-2014  AMF    Fixed Bug WIG-647: Control minimum version
' 27-MAR-2014  RCI & DDM    Fixed Bug WIGOSTITO-1174: PlaySession data is not correct.
' 24-APR-2014  DLL    Fixed Bug WIG-849: Error in ApplyExchange function, need one parameter more
' 16-MAY-2014  DLL    Fixed Bug WIG-924: Delete ApplyExchange function. Values save in CM_AUX_AMOUNT
' 03-SEP-2014  LEM    Added functionality to show terminal location data.
' 30-SEP-2014  LEM    Fixed Bug WIG-1340: Option "Show terminal location" do not work correctly.
' 30-SEP-2014  LEM    Fixed Bug WIG-1490: Wrong query with "Include gaming tables" activated.
' 30-SEP-2014  LEM    Fixed Bug WIG-1493: Shows "Gaming tables" row on grid with Gaming Table module deactivated.
' 28-JAN-2015  ANM    Don't show terminal name nor id floor if terminals check button isn't checked
' 26-MAR-2015  ANM    Calling GetProviderIdListSelected always returns a query
' 23-JUN-2015  DCS    Fixed Bug #WIG-2454: Statistics reports: Error displaying cloned terminals
' 25-JaN-2016  ETP    Fixed Bug 8572: NR credits transfer, added PS_AUX_FT_NR_CASH_IN
' 03-FEB-2016  RCI & DDM    Fixed Bug 8957: The solution for cloned terminals in ONE day, breaks the usual case (not cloned).
' 11-FEB-2016  JMV    Fixed Bug 9231:Actividad de proveedores
' 03-MAY-2016  RAB    Product Backlog Item 9758: Tables (Phase 1): Review reports table
' 03-JUN-2016  RAB    Product Backlog Item 9758: Tables (Phase 1): Review reports table (form: activity of suppliers)
' 15-SEP-2016  FAV    Fixed Bug 17686 : Timeout exception
' 28-JUL-2017  MS     Bug 29043 : "Daily total" text appears truncated in first column of Providers activity accounting window
' 04-SEP-2017  RGR    Bug 29542:WIGOS-3920 Replaced terminal not computed for "Terminal x Day" column in Provider Activity window
' 13-OCT-2017  EOR    Bug 30166:WIGOS-5508 [Ticket #9176] Reporte Actividad de Proveedores - Error Filas sin Actividad V03.06.0023
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient

Public Class frm_provider_day_terminal
  Inherits frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents gb_options As System.Windows.Forms.GroupBox
  Friend WithEvents chk_empty_rows As System.Windows.Forms.CheckBox
  Friend WithEvents chk_detailed_day As System.Windows.Forms.CheckBox
  Friend WithEvents chk_total_day As System.Windows.Forms.CheckBox
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents chk_show_gaming_tables As System.Windows.Forms.CheckBox
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox
  Friend WithEvents chk_terminals As System.Windows.Forms.CheckBox
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.gb_options = New System.Windows.Forms.GroupBox
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox
    Me.chk_show_gaming_tables = New System.Windows.Forms.CheckBox
    Me.chk_terminals = New System.Windows.Forms.CheckBox
    Me.chk_empty_rows = New System.Windows.Forms.CheckBox
    Me.chk_detailed_day = New System.Windows.Forms.CheckBox
    Me.chk_total_day = New System.Windows.Forms.CheckBox
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector
    Me.uc_pr_list = New GUI_Controls.uc_provider
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_options.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Controls.Add(Me.uc_dsl)
    Me.panel_filter.Controls.Add(Me.gb_options)
    Me.panel_filter.Size = New System.Drawing.Size(1239, 188)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_options, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dsl, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 192)
    Me.panel_data.Size = New System.Drawing.Size(1239, 548)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1233, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1233, 4)
    '
    'gb_options
    '
    Me.gb_options.AutoSize = True
    Me.gb_options.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.gb_options.Controls.Add(Me.chk_terminal_location)
    Me.gb_options.Controls.Add(Me.chk_show_gaming_tables)
    Me.gb_options.Controls.Add(Me.chk_terminals)
    Me.gb_options.Controls.Add(Me.chk_empty_rows)
    Me.gb_options.Controls.Add(Me.chk_detailed_day)
    Me.gb_options.Controls.Add(Me.chk_total_day)
    Me.gb_options.Location = New System.Drawing.Point(611, 8)
    Me.gb_options.Name = "gb_options"
    Me.gb_options.Size = New System.Drawing.Size(279, 172)
    Me.gb_options.TabIndex = 2
    Me.gb_options.TabStop = False
    Me.gb_options.Text = "xOptions"
    '
    'chk_terminal_location
    '
    Me.chk_terminal_location.Location = New System.Drawing.Point(26, 43)
    Me.chk_terminal_location.Name = "chk_terminal_location"
    Me.chk_terminal_location.Size = New System.Drawing.Size(247, 17)
    Me.chk_terminal_location.TabIndex = 5
    Me.chk_terminal_location.Text = "xShow terminals location"
    '
    'chk_show_gaming_tables
    '
    Me.chk_show_gaming_tables.AutoSize = True
    Me.chk_show_gaming_tables.Location = New System.Drawing.Point(6, 135)
    Me.chk_show_gaming_tables.Name = "chk_show_gaming_tables"
    Me.chk_show_gaming_tables.Size = New System.Drawing.Size(145, 17)
    Me.chk_show_gaming_tables.TabIndex = 4
    Me.chk_show_gaming_tables.Text = "xShowGamingTables"
    Me.chk_show_gaming_tables.UseVisualStyleBackColor = True
    Me.chk_show_gaming_tables.Visible = False
    '
    'chk_terminals
    '
    Me.chk_terminals.AutoSize = True
    Me.chk_terminals.Location = New System.Drawing.Point(6, 20)
    Me.chk_terminals.Name = "chk_terminals"
    Me.chk_terminals.Size = New System.Drawing.Size(124, 17)
    Me.chk_terminals.TabIndex = 0
    Me.chk_terminals.Text = "xShow Terminals"
    '
    'chk_empty_rows
    '
    Me.chk_empty_rows.AutoSize = True
    Me.chk_empty_rows.Location = New System.Drawing.Point(6, 112)
    Me.chk_empty_rows.Name = "chk_empty_rows"
    Me.chk_empty_rows.Size = New System.Drawing.Size(148, 17)
    Me.chk_empty_rows.TabIndex = 3
    Me.chk_empty_rows.Text = "xExclude empty rows"
    '
    'chk_detailed_day
    '
    Me.chk_detailed_day.AutoSize = True
    Me.chk_detailed_day.Location = New System.Drawing.Point(6, 66)
    Me.chk_detailed_day.Name = "chk_detailed_day"
    Me.chk_detailed_day.Size = New System.Drawing.Size(66, 17)
    Me.chk_detailed_day.TabIndex = 1
    Me.chk_detailed_day.Text = "xDetail"
    '
    'chk_total_day
    '
    Me.chk_total_day.AutoSize = True
    Me.chk_total_day.Enabled = False
    Me.chk_total_day.Location = New System.Drawing.Point(26, 89)
    Me.chk_total_day.Name = "chk_total_day"
    Me.chk_total_day.Size = New System.Drawing.Size(61, 17)
    Me.chk_total_day.TabIndex = 2
    Me.chk_total_day.Text = "xTotal"
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.ClosingTimeEnabled = True
    Me.uc_dsl.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.Location = New System.Drawing.Point(8, 8)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.Size = New System.Drawing.Size(257, 120)
    Me.uc_dsl.TabIndex = 0
    Me.uc_dsl.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    '
    'uc_pr_list
    '
    Me.uc_pr_list.Location = New System.Drawing.Point(272, 5)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(337, 191)
    Me.uc_pr_list.TabIndex = 1
    '
    'frm_provider_day_terminal
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(1247, 744)
    Me.Name = "frm_provider_day_terminal"
    Me.Text = "frm_provider_day_terminal"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_options.ResumeLayout(False)
    Me.gb_options.PerformLayout()
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Constants "
  Private TERMINAL_DATA_COLUMNS As Int32

  Private Const SQL_COLUMN_ORDERED As Integer = 0
  Private Const SQL_COLUMN_PROVIDER As Integer = 1
  Private Const SQL_COLUMN_TERMINAL As Integer = 2
  Private Const SQL_COLUMN_NUM_TERMINALS As Integer = 3
  Private Const SQL_COLUMN_TERMINAL_ID As Integer = 4
  Private Const SQL_COLUMN_RE_PLAYED As Integer = 5
  Private Const SQL_COLUMN_RE_WON As Integer = 6
  Private Const SQL_COLUMN_NR_PLAYED As Integer = 7
  Private Const SQL_COLUMN_NR_WON As Integer = 8
  Private Const SQL_COLUMN_PLAYED As Integer = 9
  Private Const SQL_COLUMN_WON As Integer = 10
  Private Const SQL_COLUMN_TERMINAL_MASTER_ID As Integer = 11

  Private GRID_COLUMN_INDEX As Integer
  Private GRID_COLUMN_DAY_FROM As Integer
  Private GRID_COLUMN_DAY_TO As Integer
  Private GRID_INIT_TERMINAL_DATA As Integer
  Private GRID_COLUMN_PROVIDER As Integer
  Private GRID_COLUMN_NUM_TERMINALS As Integer
  Private GRID_COLUMN_RE_PLAYED As Integer
  Private GRID_COLUMN_RE_WON As Integer
  Private GRID_COLUMN_RE_NETWIN As Integer
  Private GRID_COLUMN_RE_NETWIN_TERMINALS As Integer
  Private GRID_COLUMN_NR_PLAYED As Integer
  Private GRID_COLUMN_NR_WON As Integer
  Private GRID_COLUMN_NR_NETWIN As Integer
  Private GRID_COLUMN_NR_NETWIN_TERMINALS As Integer
  Private GRID_COLUMN_PLAYED As Integer
  Private GRID_COLUMN_WON As Integer
  Private GRID_COLUMN_NETWIN As Integer
  Private GRID_COLUMN_NETWIN_TERMINALS As Integer

  Private GRID_COLUMNS As Integer
  Private Const GRID_HEADER_ROWS As Integer = 2

  Private Const TABLE_PROVIDERS_TERMINALS As Integer = 0
  Private Const TABLE_TERMINALS_DATA As Integer = 1
  Private Const TABLE_GAMING_TABLES_DATA As Integer = 2

  Private Const FORM_DB_MIN_VERSION As Short = 183

#End Region ' Constants

#Region " Enums "

  Private Enum ENUM_TYPE_TABLE
    PROVIDER = 1
    TERMINAL = 2
  End Enum

#End Region ' Enums

#Region " Structures "

  Private Structure TYPE_PROVIDER_ITEM
    Dim name As String
    Dim terminal As String
    Dim terminal_id As Int32
    Dim num_terminals As Integer
    Dim played As Decimal
    Dim won As Decimal
    Dim re_played As Decimal
    Dim re_won As Decimal
    Dim nr_played As Decimal
    Dim nr_won As Decimal
    Dim terminal_any_master_id_is_connected As Boolean
  End Structure

#End Region ' Structures

#Region " Members "

  ' For report filters 
  Private m_date_from As String
  Private m_date_to As String
  Private m_terminals As String
  Private m_options_value As String
  Private m_gaming_tables As String
  Private m_gaming_tables_provider_name As String

  Private m_date_filter As Date

  Private m_terminal_report_type As ReportType = ReportType.Provider
  Private m_refresh_grid As Boolean = False
  Private m_banks_situation As DataTable

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_PROVIDER_DAY_TERMINAL
    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)
    Call MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_INVOICING.GetString(465)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(1)

    ' Date time filter
    Me.uc_dsl.Init(GLB_NLS_GUI_INVOICING.Id(201), True)

    ' Activity
    Me.gb_options.Text = GLB_NLS_GUI_INVOICING.GetString(369)
    Me.chk_terminals.Text = GLB_NLS_GUI_INVOICING.GetString(427)
    Me.chk_empty_rows.Text = GLB_NLS_GUI_INVOICING.GetString(464)
    Me.chk_total_day.Text = GLB_NLS_GUI_INVOICING.GetString(278)
    Me.chk_detailed_day.Text = GLB_NLS_GUI_INVOICING.GetString(277)

    ' Gaming tables option
    Me.chk_show_gaming_tables.Visible = GamingTableBusinessLogic.IsGamingTablesEnabled()
    Me.chk_show_gaming_tables.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4440, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3416)) ' "'Incluir mesas de juego'"
    m_gaming_tables_provider_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3416) ' "* Mesas de Juego"

    ' Providers - Terminals
    Call Me.uc_pr_list.Init(WSI.Common.Misc.GamingTerminalTypeList())

    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)

    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  '
  Protected Overrides Function GUI_FilterCheck() As Boolean

    Dim to_date As DateTime

    ' Dates selection 
    If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate > Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    Else
      to_date = WSI.Common.WGDB.Now
      to_date = New DateTime(to_date.Year, to_date.Month, to_date.Day, Me.uc_dsl.ClosingTime, 0, 0)
      If Me.uc_dsl.FromDate > to_date Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  '' PURPOSE: Set a different value for the maximum number of rows that can be showed
  ''
  ''  PARAMS:
  ''     - INPUT:
  ''
  ''     - OUTPUT:
  ''
  '' RETURNS:
  ''
  ''Protected Overrides Function GUI_MaxRows() As Integer
  ''  Return 10000
  ''End Function ' GUI_MaxRows

  ' PURPOSE: Get the defined Query Type
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_QUERY_TYPE
  '
  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM
  End Function ' GUI_GetQueryType

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  '
  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _query As String

    _query = GetQueryConnected() & GetQueryAmount()

    If chk_show_gaming_tables.Checked Then
      _query &= GetQueryGamingTables()      ' Gaming tables information
    End If

    Return _query
  End Function ' GUI_FilterGetSqlQuery

  Private Function AddRowData(ByVal Row As DataRow, _
                              ByVal ProviderDay As DataTable, ByVal ProviderTotal As DataTable, _
                              ByVal TerminalDay As DataTable, ByVal TerminalTotal As DataTable) As Boolean

    Try

      ' JAB 12-JUN-2012: DoEvents run each second.
      If Not GUI_DoEvents(Me.Grid) Then
        Return False
      End If

      AddTerminals(ProviderDay, Row, ENUM_TYPE_TABLE.PROVIDER)
      AddTerminals(ProviderTotal, Row, ENUM_TYPE_TABLE.PROVIDER)

      If Me.chk_terminals.Checked Then
        AddTerminals(TerminalDay, Row, ENUM_TYPE_TABLE.TERMINAL)
        AddTerminals(TerminalTotal, Row, ENUM_TYPE_TABLE.TERMINAL)
      End If

      Return True

    Catch exception As Exception
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Trace.WriteLine(exception.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "ProviderActivity", _
                            "GUI_ExecuteQueryCustom", _
                            exception.Message)
    End Try

    Return False
  End Function

  ' PURPOSE: Define the ExecuteQuery customized
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     -
  Protected Overrides Sub GUI_ExecuteQueryCustom()

    Dim _count As Integer
    Dim _function_name As String
    Dim _more_than_max As Boolean
    Dim _date_from_custom As Date
    Dim _date_to_custom As Date
    Dim _total_days As Integer

    Dim _db_trx As WSI.Common.DB_TRX
    Dim _sql_da As SqlDataAdapter
    Dim _ds_query As DataSet
    Dim _table As DataTable
    Dim _table_amount As DataTable
    Dim _row As DataRow

    Dim _data_set As DataSet
    Dim _dt_prov_day As DataTable
    Dim _dt_term_day As DataTable
    Dim _dt_prov_total As DataTable
    Dim _dt_term_total As DataTable
    Dim _rows As DataRow()

    Dim _table_gaming_tables As DataTable
    Dim _num_expected_tables As Integer

    Dim _list_rows_empty As List(Of DataRow) 'EOR 13-OCT-2017

    ' Prepare the expected number of tables obtained
    _num_expected_tables = 2
    ' if gaming tables information is requested, then we wait 1 more table
    If chk_show_gaming_tables.Checked Then
      _num_expected_tables += 1
    End If

    _function_name = "GUI_ExecuteQueryCustom"

    _date_from_custom = m_date_from
    _date_to_custom = m_date_to
    m_date_filter = m_date_from

    _db_trx = Nothing
    _sql_da = Nothing
    _ds_query = Nothing
    _data_set = Nothing

    Try
      Call Me.Grid.Clear()
      Me.Grid.Redraw = False
      _count = 0

      _dt_prov_day = CreateTableActivity("PROV_DAY", ENUM_TYPE_TABLE.PROVIDER)
      _dt_prov_total = CreateTableActivity("PROV_TOTAL", ENUM_TYPE_TABLE.PROVIDER)
      _dt_term_day = CreateTableActivity("TERM_DAY", ENUM_TYPE_TABLE.TERMINAL)
      _dt_term_total = CreateTableActivity("TERM_TOTAL", ENUM_TYPE_TABLE.TERMINAL)

      _data_set = New DataSet("ACTIVITY")
      _data_set.Tables.Add(_dt_prov_day)
      _data_set.Tables.Add(_dt_prov_total)
      _data_set.Tables.Add(_dt_term_day)
      _data_set.Tables.Add(_dt_term_total)
      _data_set.Relations.Add("PT_DAY", _dt_prov_day.Columns(SQL_COLUMN_PROVIDER), _dt_term_day.Columns(SQL_COLUMN_PROVIDER), True)
      _data_set.Relations.Add("PT_TOTAL", _dt_prov_total.Columns(SQL_COLUMN_PROVIDER), _dt_term_total.Columns(SQL_COLUMN_PROVIDER), True)

      LoadProviders(_dt_prov_total)
      LoadProviders(_dt_prov_day)

      ' If show daily details, do one SELECT for every day,
      ' else do only one SELECT for all the period.
      If Me.chk_detailed_day.Checked Then
        _total_days = DateDiff(DateInterval.Day, _date_from_custom, _date_to_custom)
      Else
        _total_days = 1
      End If

      _db_trx = New WSI.Common.DB_TRX()
      _sql_da = New SqlDataAdapter(New SqlCommand())
      _ds_query = Nothing

      For _num_days As Integer = 1 To _total_days

        ' JAB 12-JUN-2012: DoEvents run each second.
        If Not GUI_DoEvents(Me.Grid) Then
          Exit Sub
        End If

        _sql_da.SelectCommand.CommandText = GUI_FilterGetSqlQuery()
        _sql_da.SelectCommand.CommandTimeout = 60

        If _ds_query IsNot Nothing Then
          _ds_query.Dispose()
        End If
        _ds_query = New DataSet()

        _db_trx.Fill(_sql_da, _ds_query)

        If _ds_query.Tables.Count <> _num_expected_tables Then
          Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

          Return
        End If

        _table = _ds_query.Tables(TABLE_PROVIDERS_TERMINALS)
        _table_amount = _ds_query.Tables(TABLE_TERMINALS_DATA)
        _table.PrimaryKey = New DataColumn() {_table.Columns(SQL_COLUMN_PROVIDER), _table.Columns(SQL_COLUMN_TERMINAL)}
        _table_amount.PrimaryKey = New DataColumn() {_table_amount.Columns(SQL_COLUMN_PROVIDER), _table_amount.Columns(SQL_COLUMN_TERMINAL)}
        _table_amount.Merge(_table)

        _table_amount.Columns("TerminalID").ColumnName = "TerminalIdOld"
        _table_amount.Columns("TerminalMasterID").ColumnName = "TerminalMasterIDOld"
        _table_amount.Columns.Add("TerminalID", Type.GetType(_table_amount.Columns("TerminalIdOld").DataType.FullName), "ISNULL(TerminalIdOld,TE_TERMINAL_ID)")
        _table_amount.Columns.Add("TerminalMasterID", Type.GetType(_table_amount.Columns("TerminalMasterIDOld").DataType.FullName), "ISNULL(TerminalMasterIDOld,TE_MASTER_ID)")
        _table_amount.Columns("NumTerminals").SetOrdinal(SQL_COLUMN_NUM_TERMINALS)
        _table_amount.Columns("TerminalID").SetOrdinal(SQL_COLUMN_TERMINAL_ID)
        _table_amount.Columns("TerminalMasterID").SetOrdinal(SQL_COLUMN_TERMINAL_MASTER_ID)

        ResetProviderTable(_dt_prov_day)
        _dt_term_day.Clear()

        ' EOR 13-OCT-2017 Remove rows empty
        If chk_empty_rows.Checked Then
          _list_rows_empty = New List(Of DataRow)

          For Each _dr As DataRow In _table_amount.Rows
            If IIf(IsDBNull(_dr(SQL_COLUMN_RE_PLAYED)), 0, _dr(SQL_COLUMN_RE_PLAYED)) = 0 And _
              IIf(IsDBNull(_dr(SQL_COLUMN_RE_WON)), 0, _dr(SQL_COLUMN_RE_WON)) = 0 And _
              IIf(IsDBNull(_dr(SQL_COLUMN_NR_PLAYED)), 0, _dr(SQL_COLUMN_NR_PLAYED)) = 0 And _
              IIf(IsDBNull(_dr(SQL_COLUMN_NR_WON)), 0, _dr(SQL_COLUMN_NR_WON)) = 0 And _
              IIf(IsDBNull(_dr(SQL_COLUMN_PLAYED)), 0, _dr(SQL_COLUMN_PLAYED)) = 0 And _
              IIf(IsDBNull(_dr(SQL_COLUMN_WON)), 0, _dr(SQL_COLUMN_WON)) = 0 Then

              _list_rows_empty.Add(_dr)
            End If
          Next

          For Each _dr As DataRow In _list_rows_empty
            _table_amount.Rows.Remove(_dr)
          Next
        End If


        ' Column Terminal must be sorted by name.
        _rows = _table_amount.Select("", "Terminal")

        For Each _row In _rows
          AddRowData(_row, _dt_prov_day, _dt_prov_total, _dt_term_day, _dt_term_total)
        Next ' For Each _row

        If m_refresh_grid Then
          Call GUI_StyleSheet()
          m_refresh_grid = False
        End If

        ' If requested gaming tables information then prepare the data
        If chk_show_gaming_tables.Checked Then
          _table_gaming_tables = _ds_query.Tables(TABLE_GAMING_TABLES_DATA)
          _table_gaming_tables.PrimaryKey = New DataColumn() {_table_gaming_tables.Columns(SQL_COLUMN_PROVIDER), _table_gaming_tables.Columns(SQL_COLUMN_TERMINAL)}

          ' Column Terminal must be sorted by name.
          _rows = _table_gaming_tables.Select("", "Terminal")

          For Each _row In _rows
            AddRowData(_row, _dt_prov_day, _dt_prov_total, _dt_term_day, _dt_term_total)
          Next ' For Each _row
        End If

        If Me.chk_detailed_day.Checked Then
          _more_than_max = DrawGrid(_count, _dt_prov_day, _dt_term_day, "D", m_date_filter)
        End If

        If _more_than_max Then
          Exit For
        End If

        m_date_filter = DateAdd(DateInterval.Day, 1, m_date_filter)
      Next ' For _num_days

      _more_than_max = DrawGrid(_count, _dt_prov_total, _dt_term_total, "T", m_date_filter)

      Me.Grid.Redraw = True
      If _more_than_max Then
        Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(111), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, , , CStr(GUI_MaxRows()))
      End If

    Catch ex As Exception
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "ProviderActivity", _
                            _function_name, _
                            ex.Message)
      Me.Grid.Redraw = True

    Finally
      If _sql_da IsNot Nothing Then
        _sql_da.Dispose()
      End If
      If _db_trx IsNot Nothing Then
        _db_trx.Dispose()
      End If
      If _ds_query IsNot Nothing Then
        _ds_query.Dispose()
      End If
      If _data_set IsNot Nothing Then
        _data_set.Dispose()
      End If
    End Try

  End Sub ' GUI_ExecuteQueryCustom

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.uc_dsl
  End Sub ' GUI_SetInitialFocus

#Region " GUI Reports "

  ' PURPOSE: Set form specific requirements/parameters for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    With Me.Grid
      .Column(GRID_COLUMN_RE_PLAYED).IsColumnPrintable = False
      .Column(GRID_COLUMN_RE_WON).IsColumnPrintable = False
      .Column(GRID_COLUMN_RE_NETWIN).IsColumnPrintable = False
      .Column(GRID_COLUMN_RE_NETWIN_TERMINALS).IsColumnPrintable = False

    End With

    Call MyBase.GUI_ReportParams(PrintData)

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set form specific requirements/parameters for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_EXCEL_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    With Me.Grid
      .Column(GRID_COLUMN_RE_PLAYED).IsColumnPrintable = True
      .Column(GRID_COLUMN_RE_WON).IsColumnPrintable = True
      .Column(GRID_COLUMN_RE_NETWIN).IsColumnPrintable = True
      .Column(GRID_COLUMN_RE_NETWIN_TERMINALS).IsColumnPrintable = True

    End With

    Call MyBase.GUI_ReportParams(PrintData)

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)

    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_terminals)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(369), m_options_value)

    If GamingTableBusinessLogic.IsGamingTablesEnabled Then
      PrintData.SetFilter("", "", True)
      PrintData.SetFilter(chk_show_gaming_tables.Text, m_gaming_tables)
    End If

    PrintData.FilterHeaderWidth(2) = 1500
    PrintData.FilterValueWidth(2) = 3000
    PrintData.FilterHeaderWidth(3) = 800
    PrintData.FilterValueWidth(3) = 3300

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()

    Dim aux_date As Date

    m_date_from = ""
    m_date_to = ""
    m_terminals = ""

    'Date
    m_date_from = GUI_FormatDate(Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime), _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMM)

    If Me.uc_dsl.ToDateSelected Then
      m_date_to = GUI_FormatDate(Me.uc_dsl.ToDate.AddHours(Me.uc_dsl.ClosingTime), _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMM)
    Else
      If Now.Hour >= Me.uc_dsl.ClosingTime Then
        aux_date = New Date(Now.Year, Now.Month, Now.Day, Me.uc_dsl.ClosingTime, 0, 0)
        aux_date = aux_date.AddDays(1)
      Else
        aux_date = New Date(Now.Year, Now.Month, Now.Day, Me.uc_dsl.ClosingTime, 0, 0)
      End If
      m_date_to = GUI_FormatDate(aux_date, _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    ' Options
    ' Show Terminals
    m_options_value = chk_terminals.Text & ": "
    If Me.chk_terminals.Checked Then
      m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(480)
    End If
    ' Show Detailed Days
    m_options_value = m_options_value & ", " & chk_detailed_day.Text & ": "
    If Me.chk_detailed_day.Checked Then
      m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(479)
      m_options_value = m_options_value & ", " & chk_total_day.Text & ": "
      ' Show Daily Totals
      If chk_total_day.Checked Then
        m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(479)
      Else
        m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(480)
      End If
    Else
      m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(480)
    End If
    ' Exclude empty rows (providers without activity)
    m_options_value = m_options_value & ", " & chk_empty_rows.Text & ": "
    If Me.chk_empty_rows.Checked Then
      m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(480)
    End If

    ' Providers - Terminals
    m_terminals = Me.uc_pr_list.GetTerminalReportText()

    ' Gaming tables
    m_gaming_tables = GLB_NLS_GUI_INVOICING.GetString(IIf(chk_show_gaming_tables.Checked, 479, 480))

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  Private Function GetQueryGamingTables() As String
    Dim _sb As System.Text.StringBuilder
    Dim _date_from_custom As Date
    Dim _date_to_custom As Date
    Dim _closing_time As String
    Dim _currency_iso_code As String
    Dim _chip_RE As String
    Dim _chip_NRE As String
    Dim _chip_color As String

    _sb = New System.Text.StringBuilder()
    _currency_iso_code = CurrencyExchange.GetNationalCurrency()
    _chip_RE = Convert.ToString(CurrencyExchangeType.CASINO_CHIP_RE)
    _chip_NRE = Convert.ToString(CurrencyExchangeType.CASINO_CHIP_NRE)
    _chip_color = Convert.ToString(CurrencyExchangeType.CASINO_CHIP_COLOR)

    ' If show daily details, do one SELECT for every day,
    ' else do only one SELECT for all the period.
    If Me.chk_detailed_day.Checked Then
      _date_from_custom = m_date_filter.Date
      _date_to_custom = DateAdd(DateInterval.Day, 1, m_date_filter.Date)
    Else
      _date_from_custom = m_date_from
      _date_from_custom = _date_from_custom.Date
      _date_to_custom = m_date_to
      _date_to_custom = _date_to_custom.Date
    End If

    _closing_time = Me.uc_dsl.ClosingTime.ToString("00")

    _sb.AppendLine("SELECT   1 AS ORDERED ")
    _sb.AppendLine("       , '" & m_gaming_tables_provider_name & "' AS Provider ")
    _sb.AppendLine("       , Terminal ")
    _sb.AppendLine("       , COUNT(*)                            AS NumTerminals ")
    _sb.AppendLine("       , 0                                   AS TerminalId   ")
    _sb.AppendLine("       , SUM(CHIPS_RE_IN  + NOT_INTEGRATED_AMOUNT_IN  + CHIPS_RE_SALE)     AS RE_IN ")
    _sb.AppendLine("       , SUM(CHIPS_RE_OUT + NOT_INTEGRATED_AMOUNT_OUT + CHIPS_RE_PURCHASE) AS RE_OUT ")
    _sb.AppendLine("       , SUM(CHIPS_NRE_IN  + NOT_INTEGRATED_AMOUNT_IN  + CHIPS_NRE_SALE)     AS NR_IN ")
    _sb.AppendLine("       , SUM(CHIPS_NRE_OUT + NOT_INTEGRATED_AMOUNT_OUT + CHIPS_NRE_PURCHASE) AS NR_OUT ")
    _sb.AppendLine("       , SUM(CHIPS_RE_IN  + NOT_INTEGRATED_AMOUNT_IN  + CHIPS_RE_SALE)     + SUM(CHIPS_NRE_IN  + NOT_INTEGRATED_AMOUNT_IN  + CHIPS_NRE_SALE)     AS TTL_IN ")
    _sb.AppendLine("       , SUM(CHIPS_RE_OUT + NOT_INTEGRATED_AMOUNT_OUT + CHIPS_RE_PURCHASE) + SUM(CHIPS_NRE_OUT + NOT_INTEGRATED_AMOUNT_OUT + CHIPS_NRE_PURCHASE) AS TTL_OUT ")
    _sb.AppendLine("       , -1 AS TERMINAL_MASTER_ID ")

    _sb.AppendLine("         FROM ( SELECT   CT_NAME AS Terminal ")

    _sb.AppendLine("                , SUM(CASE WHEN CM_CURRENCY_ISO_CODE = '" & Cage.CHIPS_ISO_CODE & "' OR (CM_CURRENCY_ISO_CODE = '" + _currency_iso_code + "' AND CM_CAGE_CURRENCY_TYPE = " + _chip_RE + ") THEN ")
    _sb.AppendLine("                           CASE CM_TYPE ")
    _sb.AppendLine("                           WHEN " & CASHIER_MOVEMENT.FILLER_OUT & "   THEN CM_SUB_AMOUNT ")
    _sb.AppendLine("                           WHEN " & CASHIER_MOVEMENT.CAGE_CLOSE_SESSION & " THEN CM_INITIAL_BALANCE ")
    _sb.AppendLine("                           WHEN " & CASHIER_MOVEMENT.FILLER_OUT_CLOSING_STOCK & "   THEN CM_SUB_AMOUNT ")
    _sb.AppendLine("                           ELSE 0   END  ")
    _sb.AppendLine("                      ELSE 0   END) AS CHIPS_RE_IN ")
    _sb.AppendLine("                , SUM(CASE WHEN CM_CURRENCY_ISO_CODE = '" & Cage.CHIPS_ISO_CODE & "' OR (CM_CURRENCY_ISO_CODE = '" + _currency_iso_code + "' AND CM_CAGE_CURRENCY_TYPE = " + _chip_NRE + ") THEN ")
    _sb.AppendLine("                           CASE CM_TYPE ")
    _sb.AppendLine("                           WHEN " & CASHIER_MOVEMENT.FILLER_OUT & "   THEN CM_SUB_AMOUNT ")
    _sb.AppendLine("                           WHEN " & CASHIER_MOVEMENT.CAGE_CLOSE_SESSION & " THEN CM_INITIAL_BALANCE ")
    _sb.AppendLine("                           WHEN " & CASHIER_MOVEMENT.FILLER_OUT_CLOSING_STOCK & "   THEN CM_SUB_AMOUNT ")
    _sb.AppendLine("                           ELSE 0   END  ")
    _sb.AppendLine("                      ELSE 0   END) AS CHIPS_NRE_IN  ")
    _sb.AppendLine("                , SUM(CASE WHEN CM_CURRENCY_ISO_CODE = '" & Cage.CHIPS_ISO_CODE & "' OR (CM_CURRENCY_ISO_CODE = '" + _currency_iso_code + "' AND CM_CAGE_CURRENCY_TYPE = " + _chip_RE + ") THEN ")
    _sb.AppendLine("                           CASE CM_TYPE ")
    _sb.AppendLine("                           WHEN " & CASHIER_MOVEMENT.FILLER_IN & " THEN CM_ADD_AMOUNT ")
    _sb.AppendLine("                           WHEN " & CASHIER_MOVEMENT.FILLER_IN_CLOSING_STOCK & " THEN CM_ADD_AMOUNT ")
    _sb.AppendLine("                           ELSE 0   END ")
    _sb.AppendLine("                      ELSE 0   END) AS CHIPS_RE_OUT ")
    _sb.AppendLine("                , SUM(CASE WHEN CM_CURRENCY_ISO_CODE = '" & Cage.CHIPS_ISO_CODE & "' OR (CM_CURRENCY_ISO_CODE = '" + _currency_iso_code + "' AND CM_CAGE_CURRENCY_TYPE = " + _chip_NRE + ") THEN ")
    _sb.AppendLine("                           CASE CM_TYPE ")
    _sb.AppendLine("                           WHEN " & CASHIER_MOVEMENT.FILLER_IN & " THEN CM_ADD_AMOUNT ")
    _sb.AppendLine("                           WHEN " & CASHIER_MOVEMENT.FILLER_IN_CLOSING_STOCK & " THEN CM_ADD_AMOUNT ")
    _sb.AppendLine("                           ELSE 0   END ")
    _sb.AppendLine("                      ELSE 0   END) AS CHIPS_NRE_OUT ")

    _sb.AppendLine("                , SUM(CASE WHEN GT_HAS_INTEGRATED_CASHIER = 0 AND ((CM_CURRENCY_ISO_CODE <> '" & Cage.CHIPS_ISO_CODE & "' OR CM_CURRENCY_ISO_CODE IS NULL ) AND ")
    _sb.AppendLine("					                     (CM_CAGE_CURRENCY_TYPE NOT IN (" + _chip_RE + ", " + _chip_NRE + ", " + _chip_color + "))) THEN")
    _sb.AppendLine("                           CASE CM_TYPE ")
    _sb.AppendLine("                           WHEN " & CASHIER_MOVEMENT.FILLER_OUT & " THEN ISNULL(CM_AUX_AMOUNT, CM_SUB_AMOUNT) ")
    _sb.AppendLine("                           WHEN " & CASHIER_MOVEMENT.CAGE_CLOSE_SESSION & " THEN ISNULL(CM_AUX_AMOUNT, CM_INITIAL_BALANCE) ")
    _sb.AppendLine("                           WHEN " & CASHIER_MOVEMENT.FILLER_OUT_CLOSING_STOCK & " THEN ISNULL(CM_AUX_AMOUNT, CM_SUB_AMOUNT) ")
    _sb.AppendLine("                           ELSE 0   END ")
    _sb.AppendLine("                      ELSE 0   END) AS NOT_INTEGRATED_AMOUNT_IN  ")
    _sb.AppendLine("                , SUM(CASE WHEN GT_HAS_INTEGRATED_CASHIER = 0 AND ((CM_CURRENCY_ISO_CODE <> '" & Cage.CHIPS_ISO_CODE & "' OR CM_CURRENCY_ISO_CODE IS NULL ) AND ")
    _sb.AppendLine("					                     (CM_CAGE_CURRENCY_TYPE NOT IN (" + _chip_RE + ", " + _chip_NRE + ", " + _chip_color + "))) THEN")
    _sb.AppendLine("                           CASE CM_TYPE ")
    _sb.AppendLine("                           WHEN " & CASHIER_MOVEMENT.FILLER_IN & "   THEN ISNULL(CM_AUX_AMOUNT, CM_ADD_AMOUNT) ")
    _sb.AppendLine("                           WHEN " & CASHIER_MOVEMENT.FILLER_IN_CLOSING_STOCK & "   THEN ISNULL(CM_AUX_AMOUNT, CM_ADD_AMOUNT) ")
    _sb.AppendLine("                           ELSE 0   END ")
    _sb.AppendLine("                      ELSE 0   END) AS NOT_INTEGRATED_AMOUNT_OUT ")

    _sb.AppendLine("                , SUM(CASE WHEN GT_HAS_INTEGRATED_CASHIER = 1 AND CM_CURRENCY_ISO_CODE = '" & Cage.CHIPS_ISO_CODE & "' OR (CM_CURRENCY_ISO_CODE = '" + _currency_iso_code + "' AND CM_CAGE_CURRENCY_TYPE = " + _chip_RE + ") THEN ")
    _sb.AppendLine("                           CASE CM_TYPE ")
    _sb.AppendLine("                           WHEN " & CASHIER_MOVEMENT.CHIPS_SALE_TOTAL & " THEN CM_SUB_AMOUNT ")
    _sb.AppendLine("                           ELSE 0   END ")
    _sb.AppendLine("                      ELSE 0   END) AS CHIPS_RE_SALE  ")
    _sb.AppendLine("                , SUM(CASE WHEN GT_HAS_INTEGRATED_CASHIER = 1 AND CM_CURRENCY_ISO_CODE = '" & Cage.CHIPS_ISO_CODE & "' OR (CM_CURRENCY_ISO_CODE = '" + _currency_iso_code + "' AND CM_CAGE_CURRENCY_TYPE = " + _chip_NRE + ") THEN ")
    _sb.AppendLine("                           CASE CM_TYPE ")
    _sb.AppendLine("                           WHEN " & CASHIER_MOVEMENT.CHIPS_SALE_TOTAL & " THEN CM_SUB_AMOUNT ")
    _sb.AppendLine("                           ELSE 0   END ")
    _sb.AppendLine("                      ELSE 0   END) AS CHIPS_NRE_SALE  ")
    _sb.AppendLine("                , SUM(CASE WHEN GT_HAS_INTEGRATED_CASHIER = 1 AND CM_CURRENCY_ISO_CODE = '" & Cage.CHIPS_ISO_CODE & "' OR (CM_CURRENCY_ISO_CODE = '" + _currency_iso_code + "' AND CM_CAGE_CURRENCY_TYPE = " + _chip_RE + ") THEN ")
    _sb.AppendLine("                           CASE CM_TYPE ")
    _sb.AppendLine("                           WHEN " & CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL & " THEN CM_ADD_AMOUNT ")
    _sb.AppendLine("                           ELSE 0   END ")
    _sb.AppendLine("                      ELSE 0   END) AS CHIPS_RE_PURCHASE ")
    _sb.AppendLine("                , SUM(CASE WHEN GT_HAS_INTEGRATED_CASHIER = 1 AND CM_CURRENCY_ISO_CODE = '" & Cage.CHIPS_ISO_CODE & "' OR (CM_CURRENCY_ISO_CODE = '" + _currency_iso_code + "' AND CM_CAGE_CURRENCY_TYPE = " + _chip_NRE + ") THEN ")
    _sb.AppendLine("                           CASE CM_TYPE ")
    _sb.AppendLine("                           WHEN " & CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL & " THEN CM_ADD_AMOUNT ")
    _sb.AppendLine("                           ELSE 0   END ")
    _sb.AppendLine("                      ELSE 0   END) AS CHIPS_NRE_PURCHASE ")

    _sb.AppendLine("                  FROM   CASHIER_MOVEMENTS WITH (INDEX(IX_cm_date_type))")
    _sb.AppendLine("            INNER JOIN   CASHIER_TERMINALS      ON CM_CASHIER_ID = CT_CASHIER_ID ")
    _sb.AppendLine("            INNER JOIN   GAMING_TABLES_SESSIONS ON CM_SESSION_ID = GTS_CASHIER_SESSION_ID ")
    _sb.AppendLine("            INNER JOIN   GAMING_TABLES          ON GT_CASHIER_ID = CM_CASHIER_ID ")
    _sb.AppendLine("            INNER JOIN   CASHIER_SESSIONS       ON CS_SESSION_ID = CM_SESSION_ID ")
    _sb.AppendLine("                 WHERE   CS_STATUS IN (" & WSI.Common.CASHIER_SESSION_STATUS.CLOSED & ", " & WSI.Common.CASHIER_SESSION_STATUS.PENDING_CLOSING & ") ")
    _sb.AppendLine("                   AND   CM_TYPE   IN (" & CASHIER_MOVEMENT.FILLER_IN & ", " & CASHIER_MOVEMENT.FILLER_OUT & ", " & CASHIER_MOVEMENT.CAGE_CLOSE_SESSION & ", ")
    _sb.AppendLine("                                       " & CASHIER_MOVEMENT.CHIPS_SALE_TOTAL & ", " & CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL & ", ")
    _sb.AppendLine("                                       " & CASHIER_MOVEMENT.FILLER_OUT_CLOSING_STOCK & ", " & CASHIER_MOVEMENT.FILLER_IN_CLOSING_STOCK & " ) ")
    _sb.AppendLine("                   AND   CM_DATE   >= " & GUI_FormatDateDB(_date_from_custom) & " ")
    _sb.AppendLine("                   AND   CM_DATE   <= " & GUI_FormatDateDB(_date_to_custom) & " ")
    _sb.AppendLine("              GROUP BY   CT_NAME ")
    _sb.AppendLine("                       , DATEDIFF(HOUR, '01-01-2007 " & _closing_time & ":00:00', DATEADD(HOUR, " & _closing_time & ", CM_DATE)) / 24")
    _sb.AppendLine("                  ) x ")
    _sb.AppendLine(" GROUP BY x.Terminal ")

    Return _sb.ToString()
  End Function

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    Dim _terminal_columns As List(Of ColumnSettings)

    Call GridColumnReIndex()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 150
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Date
      ' Day from
      .Column(GRID_COLUMN_DAY_FROM).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(201)
      .Column(GRID_COLUMN_DAY_FROM).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(202)

      .Column(GRID_COLUMN_DAY_FROM).Width = 1700

      .Column(GRID_COLUMN_DAY_FROM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Day to
      .Column(GRID_COLUMN_DAY_TO).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(201)
      .Column(GRID_COLUMN_DAY_TO).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(203)
      .Column(GRID_COLUMN_DAY_TO).Width = 1700
      .Column(GRID_COLUMN_DAY_TO).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '  Terminal Report
      _terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)
      For _idx As Int32 = 0 To _terminal_columns.Count - 1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(253)
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = _terminal_columns(_idx).Header1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = _terminal_columns(_idx).Width
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = _terminal_columns(_idx).Alignment
        If _terminal_columns(_idx).Column = ReportColumn.Provider Then
          GRID_COLUMN_PROVIDER = GRID_INIT_TERMINAL_DATA + _idx
        Else
          'Don't show terminal name nor id floor if terminals check button isn't checked
          If chk_terminals.Checked = False Then
            .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = 0
          End If
        End If
      Next

      ' No title
      ' NumTerminals
      .Column(GRID_COLUMN_NUM_TERMINALS).Header(0).Text = " "
      .Column(GRID_COLUMN_NUM_TERMINALS).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(269)
      .Column(GRID_COLUMN_NUM_TERMINALS).Width = 1200
      .Column(GRID_COLUMN_NUM_TERMINALS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Redeemable
      ' Played
      .Column(GRID_COLUMN_RE_PLAYED).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(337)
      .Column(GRID_COLUMN_RE_PLAYED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3136)
      .Column(GRID_COLUMN_RE_PLAYED).Width = 1750
      .Column(GRID_COLUMN_RE_PLAYED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Won
      .Column(GRID_COLUMN_RE_WON).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(337)
      .Column(GRID_COLUMN_RE_WON).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3109)
      .Column(GRID_COLUMN_RE_WON).Width = 1750
      .Column(GRID_COLUMN_RE_WON).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Netwin
      .Column(GRID_COLUMN_RE_NETWIN).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(337)
      .Column(GRID_COLUMN_RE_NETWIN).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(259)
      .Column(GRID_COLUMN_RE_NETWIN).Width = 1600
      .Column(GRID_COLUMN_RE_NETWIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Netwin / Terminals
      .Column(GRID_COLUMN_RE_NETWIN_TERMINALS).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(337)
      .Column(GRID_COLUMN_RE_NETWIN_TERMINALS).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(279)
      .Column(GRID_COLUMN_RE_NETWIN_TERMINALS).Width = 2000
      .Column(GRID_COLUMN_RE_NETWIN_TERMINALS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Non-Redeemable
      ' Played
      .Column(GRID_COLUMN_NR_PLAYED).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(338)
      .Column(GRID_COLUMN_NR_PLAYED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3136)
      .Column(GRID_COLUMN_NR_PLAYED).Width = 1750
      .Column(GRID_COLUMN_NR_PLAYED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Won
      .Column(GRID_COLUMN_NR_WON).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(338)
      .Column(GRID_COLUMN_NR_WON).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3109)
      .Column(GRID_COLUMN_NR_WON).Width = 1750
      .Column(GRID_COLUMN_NR_WON).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Netwin
      .Column(GRID_COLUMN_NR_NETWIN).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(338)
      .Column(GRID_COLUMN_NR_NETWIN).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(259)
      .Column(GRID_COLUMN_NR_NETWIN).Width = 1600
      .Column(GRID_COLUMN_NR_NETWIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Netwin / Terminals
      .Column(GRID_COLUMN_NR_NETWIN_TERMINALS).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(338)
      .Column(GRID_COLUMN_NR_NETWIN_TERMINALS).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(279)
      .Column(GRID_COLUMN_NR_NETWIN_TERMINALS).Width = 2000
      .Column(GRID_COLUMN_NR_NETWIN_TERMINALS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Total
      ' Played
      .Column(GRID_COLUMN_PLAYED).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
      .Column(GRID_COLUMN_PLAYED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3136)
      .Column(GRID_COLUMN_PLAYED).Width = 1750
      .Column(GRID_COLUMN_PLAYED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Won
      .Column(GRID_COLUMN_WON).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
      .Column(GRID_COLUMN_WON).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3109)
      .Column(GRID_COLUMN_WON).Width = 1750
      .Column(GRID_COLUMN_WON).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Netwin
      .Column(GRID_COLUMN_NETWIN).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
      .Column(GRID_COLUMN_NETWIN).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(259)
      .Column(GRID_COLUMN_NETWIN).Width = 1600
      .Column(GRID_COLUMN_NETWIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Netwin / Terminals
      .Column(GRID_COLUMN_NETWIN_TERMINALS).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
      .Column(GRID_COLUMN_NETWIN_TERMINALS).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(279)
      .Column(GRID_COLUMN_NETWIN_TERMINALS).Width = 2000
      .Column(GRID_COLUMN_NETWIN_TERMINALS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Sortable = False
    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()
    Dim initial_time As Date

    initial_time = WSI.Common.Misc.TodayOpening()
    Me.uc_dsl.ToDateSelected = True
    Me.uc_dsl.ToDate = initial_time
    Me.uc_dsl.FromDate = Me.uc_dsl.ToDate.AddDays(-1)
    Me.uc_dsl.ClosingTime = initial_time.Hour

    Call Me.uc_pr_list.SetDefaultValues()

    Me.chk_terminals.Checked = False
    Me.chk_empty_rows.Checked = False
    Me.chk_detailed_day.Checked = False
    Me.chk_total_day.Checked = False
    Me.chk_total_day.Enabled = False

    Me.chk_show_gaming_tables.Checked = chk_show_gaming_tables.Visible

    Me.chk_terminal_location.Checked = False
    Me.chk_terminal_location.Enabled = chk_terminals.Checked

  End Sub ' SetDefaultValues 

  ' PURPOSE: Get Sql Query to build connected terminals per provider and terminal
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - String with SQL sentence
  '
  Private Function GetQueryConnected() As String
    Dim _str_sql As String
    Dim _date_from_custom As Date
    Dim _date_to_custom As Date

    ' If show daily details, do one SELECT for every day,
    ' else do only one SELECT for all the period.
    If Me.chk_detailed_day.Checked Then
      _date_from_custom = m_date_filter.Date
      _date_to_custom = DateAdd(DateInterval.Day, 1, m_date_filter.Date)
    Else
      _date_from_custom = m_date_from
      _date_from_custom = _date_from_custom.Date
      _date_to_custom = m_date_to
      _date_to_custom = _date_to_custom.Date
    End If

    ' Get Select and from
    _str_sql = "SELECT   0 AS ORDERED " & _
              "        , ISNULL(TE_PROVIDER_ID, '') AS Provider " & _
              "        , ISNULL(TE_NAME, '')        AS Terminal " & _
              "        , COUNT(*)                   AS NumTerminals " & _
              "        , TC_TERMINAL_ID             AS TerminalId " & _
              "        , TC_MASTER_ID             AS TerminalMasterId " & _
              "   FROM   TERMINALS_CONNECTED " & _
              "        , TERMINALS " & _
              "  WHERE   TC_DATE        >= " & GUI_FormatDateDB(_date_from_custom) & _
              "    AND   TC_DATE        <  " & GUI_FormatDateDB(_date_to_custom) & _
              "    AND   TC_CONNECTED   = 1 " & _
              "    AND   TC_TERMINAL_ID = TE_TERMINAL_ID " & _
              "    AND   TE_TYPE        = 1 "

    ' Filter Terminal
    _str_sql = _str_sql & " AND TE_TERMINAL_ID IN " & Me.uc_pr_list.GetProviderIdListSelected()

    _str_sql = _str_sql & " GROUP BY TE_PROVIDER_ID, TE_NAME, TC_MASTER_ID, TC_TERMINAL_ID "
    _str_sql = _str_sql & " ORDER BY TE_PROVIDER_ID, TE_NAME, TC_MASTER_ID, TC_TERMINAL_ID "
    _str_sql = _str_sql & " ; "

    Return _str_sql
  End Function ' GetQueryConnected

  ' PURPOSE: Get Sql Query to build played and won amount per provider and terminal
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String with SQL sentence
  Private Function GetQueryAmount() As String

    Dim _str_sql As String
    Dim _with_index As String

    ' FBA 08-AUG-2013
    ' Forces use of one INDEX or another depending on the number of terminals selected.
    If Me.uc_pr_list.OneTerminalChecked Then
      _with_index = " WITH (INDEX(IX_ps_finished)) "
    Else
      _with_index = " WITH (INDEX(IX_ps_finished_status)) "
    End If

    ' ACC on 18-FEB-2010
    ' Change Provider activity by Play Sessions instead sales per hour table.

    ' Get Select and from
    _str_sql = ""
    _str_sql = _str_sql & " SELECT   0 AS ORDERED "
    _str_sql = _str_sql & "        , ISNULL(TE_PROVIDER_ID, '') as Provider "
    _str_sql = _str_sql & "        , ISNULL(TE_NAME, '') as Terminal "
    _str_sql = _str_sql & "        , RE_IN "
    _str_sql = _str_sql & "        , RE_OUT "
    _str_sql = _str_sql & "        , NR_IN "
    _str_sql = _str_sql & "        , NR_OUT "
    _str_sql = _str_sql & "        , TTL_IN "
    _str_sql = _str_sql & "        , TTL_OUT "
    _str_sql = _str_sql & "        , ISNULL(TE_TERMINAL_ID, '') AS TE_TERMINAL_ID "
    _str_sql = _str_sql & "        , ISNULL(TE_MASTER_ID, '')   AS TE_MASTER_ID "
    _str_sql = _str_sql & "   FROM ( SELECT   PS_TERMINAL_ID "
    _str_sql = _str_sql & "                 , SUM(ISNULL(PS_REDEEMABLE_CASH_IN, 0))      AS RE_IN   "
    _str_sql = _str_sql & "                 , SUM(ISNULL(PS_REDEEMABLE_CASH_OUT, 0))     AS RE_OUT  "
    _str_sql = _str_sql & "                 , SUM(ISNULL(PS_NON_REDEEMABLE_CASH_IN, 0) + ISNULL(PS_AUX_FT_NR_CASH_IN, 0))  AS NR_IN   "
    _str_sql = _str_sql & "                 , SUM(ISNULL(PS_NON_REDEEMABLE_CASH_OUT, 0)) AS NR_OUT  "
    _str_sql = _str_sql & "                 , SUM(PS_TOTAL_CASH_IN)                      AS TTL_IN  "
    _str_sql = _str_sql & "                 , SUM(PS_TOTAL_CASH_OUT)                     AS TTL_OUT "
    _str_sql = _str_sql & "            FROM   PLAY_SESSIONS  " & _with_index & " "
    _str_sql = _str_sql & "           WHERE "
    _str_sql = _str_sql & GetSqlWhereAmount(_with_index)
    _str_sql = _str_sql & "        GROUP BY   PS_TERMINAL_ID) AS X "
    _str_sql = _str_sql & "  INNER   JOIN TERMINALS ON PS_TERMINAL_ID = TE_TERMINAL_ID "
    _str_sql = _str_sql & "  ORDER   BY Provider, Terminal "
    _str_sql = _str_sql & " ; "

    Return _str_sql
  End Function ' GetQueryAmount

  ' PURPOSE: Get Sql WHERE to build SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String with WHERE options
  Private Function GetSqlWhereAmount(IndexString As String) As String

    Dim _str_where As String
    Dim _date_from_custom As Date
    Dim _date_to_custom As Date

    ' If show daily details, do one SELECT for every day,
    ' else do only one SELECT for all the period.
    If Me.chk_detailed_day.Checked Then
      _date_from_custom = m_date_filter
      _date_to_custom = DateAdd(DateInterval.Day, 1, m_date_filter)
    Else
      _date_from_custom = m_date_from
      _date_to_custom = m_date_to
    End If

    _str_where = ""

    If (IndexString.Contains("IX_ps_finished")) Then
      ' Filter Terminal
      _str_where = _str_where & " PS_TERMINAL_ID IN " & Me.uc_pr_list.GetProviderIdListSelected() & " "

      ' Filter Dates
      _str_where = _str_where & " AND (PS_FINISHED >= " & GUI_FormatDateDB(_date_from_custom) & ") "
      _str_where = _str_where & " AND (PS_FINISHED  < " & GUI_FormatDateDB(_date_to_custom) & ") "

      ' Fileter Status
      _str_where = _str_where & " AND PS_STATUS <> 0 AND PS_PROMO = 0 " ' Not Started - SLE 29-ABR-2010: Not Promotional 
    Else
      ' Filter Dates
      _str_where = _str_where & " (PS_FINISHED >= " & GUI_FormatDateDB(_date_from_custom) & ") "
      _str_where = _str_where & " AND (PS_FINISHED  < " & GUI_FormatDateDB(_date_to_custom) & ") "

      ' Fileter Status
      _str_where = _str_where & " AND PS_STATUS <> 0 AND PS_PROMO = 0 " ' Not Started - SLE 29-ABR-2010: Not Promotional 

      ' Filter Terminal
      _str_where = _str_where & " AND PS_TERMINAL_ID IN " & Me.uc_pr_list.GetProviderIdListSelected() & " "

    End If

    Return _str_where
  End Function ' GetSqlWhereAmount

  ' PURPOSE: Puts number of terminals, played and won = 0 into provider table
  '
  '  PARAMS:
  '     - INPUT:
  '           - ProvTable
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - None
  Private Sub ResetProviderTable(ByVal ProvTable As DataTable)
    For Each _row As DataRow In ProvTable.Rows
      _row.BeginEdit()
      _row(SQL_COLUMN_NUM_TERMINALS) = 0
      _row(SQL_COLUMN_PLAYED) = 0
      _row(SQL_COLUMN_WON) = 0
      _row(SQL_COLUMN_RE_PLAYED) = 0
      _row(SQL_COLUMN_RE_WON) = 0
      _row(SQL_COLUMN_NR_PLAYED) = 0
      _row(SQL_COLUMN_NR_WON) = 0
      _row.EndEdit()
    Next
  End Sub ' ResetProviderTable

  ' PURPOSE: Add number of terminals to the provider/terminal row
  '
  '  PARAMS:
  '     - INPUT:
  '           - ProvTable
  '           - Row
  '           - Type
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub AddTerminals(ByVal ProvTable As DataTable, _
                           ByVal Row As DataRow, _
                           ByVal Type As ENUM_TYPE_TABLE)
    Dim _rows As DataRow()
    Dim _row As DataRow
    Dim _aux_text As String
    Dim _counter As Integer

    _row = Nothing

    Select Case Type
      Case ENUM_TYPE_TABLE.PROVIDER
        _rows = ProvTable.Select("Provider = '" + CStr(Row(SQL_COLUMN_PROVIDER)).Replace("'", "''") + "'")
        If _rows.Length > 0 Then
          _row = _rows(0)
        End If

      Case ENUM_TYPE_TABLE.TERMINAL
        _rows = ProvTable.Select("Provider = '" + CStr(Row(SQL_COLUMN_PROVIDER)).Replace("'", "''") + "' and Terminal = '" + CStr(Row(SQL_COLUMN_TERMINAL)).Replace("'", "''") + "'")
        If _rows.Length > 0 Then
          _row = _rows(0)
        Else
          _row = ProvTable.NewRow()
          _row(SQL_COLUMN_PROVIDER) = Row(SQL_COLUMN_PROVIDER)
          _row(SQL_COLUMN_TERMINAL) = Row(SQL_COLUMN_TERMINAL)
          _row(SQL_COLUMN_TERMINAL_ID) = Row(SQL_COLUMN_TERMINAL_ID)
          _row(SQL_COLUMN_TERMINAL_MASTER_ID) = Row(SQL_COLUMN_TERMINAL_MASTER_ID)
          ProvTable.Rows.Add(_row)
        End If
    End Select

    If _row Is Nothing Then
      _aux_text = "*** NOT ADDED *** Provider: [" + Row(SQL_COLUMN_PROVIDER) + "]" _
                + " (NumTerminals: " + Row(SQL_COLUMN_NUM_TERMINALS).ToString() + ") was NOT found in the providers table"
      Call Trace.WriteLine(_aux_text)
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_USER_MESSAGE, _
                            "ProviderActivity", _
                            "AddTerminals", , , , _
                            _aux_text)

      Return
    End If


    _counter = IIf(Row.IsNull(SQL_COLUMN_NUM_TERMINALS), 0, Row(SQL_COLUMN_NUM_TERMINALS))

    _row(SQL_COLUMN_ORDERED) = IIf(Row.IsNull(SQL_COLUMN_ORDERED), 0, Row(SQL_COLUMN_ORDERED))
    _row(SQL_COLUMN_NUM_TERMINALS) += IIf(_counter = 0, 1, _counter)
    _row(SQL_COLUMN_PLAYED) += IIf(Row.IsNull(SQL_COLUMN_PLAYED), 0, Row(SQL_COLUMN_PLAYED))
    _row(SQL_COLUMN_WON) += IIf(Row.IsNull(SQL_COLUMN_WON), 0, Row(SQL_COLUMN_WON))
    _row(SQL_COLUMN_RE_PLAYED) += IIf(Row.IsNull(SQL_COLUMN_RE_PLAYED), 0, Row(SQL_COLUMN_RE_PLAYED))
    _row(SQL_COLUMN_RE_WON) += IIf(Row.IsNull(SQL_COLUMN_RE_WON), 0, Row(SQL_COLUMN_RE_WON))
    _row(SQL_COLUMN_NR_PLAYED) += IIf(Row.IsNull(SQL_COLUMN_NR_PLAYED), 0, Row(SQL_COLUMN_NR_PLAYED))
    _row(SQL_COLUMN_NR_WON) += IIf(Row.IsNull(SQL_COLUMN_NR_WON), 0, Row(SQL_COLUMN_NR_WON))

  End Sub ' AddTerminals

  ' PURPOSE: Load Provider name into provider table
  '
  '  PARAMS:
  '     - INPUT:
  '           - ProvTable
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub LoadProviders(ByVal ProvTable As DataTable)
    Dim _str_sql As String
    Dim _table As DataTable
    Dim _row As DataRow
    Dim _new_row As DataRow

    _str_sql = " SELECT DISTINCT TE_PROVIDER_ID FROM TERMINALS WHERE TE_TERMINAL_ID IN " & _
               Me.uc_pr_list.GetProviderIdListSelected() & " AND TE_TYPE = 1 "

    _table = GUI_GetTableUsingSQL(_str_sql, GUI_MaxRows())

    For Each _row In _table.Rows
      _new_row = ProvTable.NewRow()
      _new_row(SQL_COLUMN_ORDERED) = 0
      _new_row(SQL_COLUMN_PROVIDER) = IIf(_row.IsNull(0), GLB_NLS_GUI_STATISTICS.GetString(213), _row(0))
      ProvTable.Rows.Add(_new_row)
    Next

    ' Add the virtual Gaming Tables provider if requested
    If chk_show_gaming_tables.Checked Then
      _new_row = ProvTable.NewRow()
      _new_row(SQL_COLUMN_ORDERED) = 1
      _new_row(SQL_COLUMN_PROVIDER) = m_gaming_tables_provider_name
      ProvTable.Rows.Add(_new_row)
    End If

  End Sub ' LoadProviders

  ' PURPOSE: Prepare data before draw rows
  '
  '  PARAMS:
  '     - INPUT:
  '           - ProvTable
  '           - TermTable
  '           - Type: "T" for total activity; "D" for daily activity
  '           - FilterDate: Day of row
  '     - OUTPUT:
  '           - Counter of rows
  '
  ' RETURNS:
  '     - Boolean: Indicates if there are more than max rows.
  '
  Private Function DrawGrid(ByRef Count As Integer, _
                            ByVal ProvTable As DataTable, _
                            ByVal TermTable As DataTable, _
                            ByVal Type As String, _
                            ByVal FilterDate As Date) As Boolean
    Dim color_total As Integer
    Dim more_than_max As Boolean
    Dim day_string1 As String
    Dim day_string2 As String
    Dim day_string_total As String
    Dim total_item As TYPE_PROVIDER_ITEM
    Dim _child_rows As DataRow()
    Dim _rows As DataRow()
    Dim _terminal_any_master_id_is_connected As Boolean


    more_than_max = False

    m_banks_situation = New DataTable()
    GamingTableBusinessLogic.GetGamingTablesSituation(m_banks_situation)

    With Me.Grid
      If (Me.chk_detailed_day.Checked And Me.chk_total_day.Checked) Then
        .Column(GRID_COLUMN_DAY_FROM).Width = 2200
      Else
        .Column(GRID_COLUMN_DAY_FROM).Width = 1700
      End If
    End With

    If Type = "T" Then
      day_string1 = m_date_from
      day_string2 = m_date_to
      day_string_total = GLB_NLS_GUI_INVOICING.GetString(205)
      color_total = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
      ' Head-board Final Summary
      Me.Grid.AddRow()
      Me.Grid.Row(Me.Grid.NumRows - 1).BackColor = GetColor(color_total)
      Count = Count + 1
    Else
      day_string1 = GUI_FormatDate(FilterDate, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
      day_string2 = GUI_FormatDate(FilterDate.AddDays(1), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
      day_string_total = GLB_NLS_GUI_INVOICING.GetString(280, New Date(FilterDate.Year, FilterDate.Month, FilterDate.Day))
      color_total = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01
    End If

    total_item.name = ""
    total_item.terminal = ""
    total_item.num_terminals = 0
    total_item.played = 0
    total_item.won = 0
    total_item.re_played = 0
    total_item.re_won = 0
    total_item.nr_played = 0
    total_item.nr_won = 0

    ' Column Provider must be sorted by name.
    _rows = ProvTable.Select("", "ORDERED, Provider")

    For Each _row As DataRow In _rows
      ' Show terminals if checked
      If Me.chk_terminals.Checked Then
        _child_rows = _row.GetChildRows(IIf(Type = "T", "PT_TOTAL", "PT_DAY"))

        For Each _row_term As DataRow In _child_rows
          _terminal_any_master_id_is_connected = False
          ' If not is gaming table  and exists other terminal with same master id which are connected mark the property to true for not paint in red and not recalculated
          If _row_term(SQL_COLUMN_TERMINAL_MASTER_ID) <> -1 And TermTable.Select("TerminalMasterID = " & _row_term.Item(SQL_COLUMN_TERMINAL_MASTER_ID) & "AND NumTerminals = " & _row_term.Item(SQL_COLUMN_NUM_TERMINALS) & " > 0").LongLength Then
            _terminal_any_master_id_is_connected = True
          End If

          If Not SetupRow(day_string1, day_string2, _row_term, Count, more_than_max, _terminal_any_master_id_is_connected) Then
            Continue For
          End If
        Next
      End If

      If Not SetupRow(day_string1, day_string2, _row, Count, more_than_max, False) Then
        Continue For
      End If

      total_item.num_terminals += IIf(IsDBNull(_row(SQL_COLUMN_NUM_TERMINALS)), 0, _row(SQL_COLUMN_NUM_TERMINALS))
      total_item.played += _row(SQL_COLUMN_PLAYED)
      total_item.won += _row(SQL_COLUMN_WON)
      total_item.re_played += _row(SQL_COLUMN_RE_PLAYED)
      total_item.re_won += _row(SQL_COLUMN_RE_WON)
      total_item.nr_played += _row(SQL_COLUMN_NR_PLAYED)
      total_item.nr_won += _row(SQL_COLUMN_NR_WON)
    Next

    ' Totals
    If Me.chk_total_day.Checked Or Type = "T" Then
      If Type = "D" And Me.chk_empty_rows.Checked And total_item.num_terminals = 0 Then
        If total_item.played <> 0 Or total_item.won <> 0 Then
          more_than_max = DrawRow(day_string_total, "", total_item, color_total, Count)
        End If
      Else
        more_than_max = DrawRow(day_string_total, "", total_item, color_total, Count)
      End If
    End If

    Return more_than_max
  End Function ' DrawGrid

  ' PURPOSE: Prepare and show results to screen
  '
  '  PARAMS:
  '     - INPUT:
  '           - DayString1
  '           - DayString2
  '           - Row
  '     - OUTPUT:
  '           - Count
  '           - MoreThanMax
  '
  ' RETURNS:
  '     - True (the row has been added) or False (the row has not been added)
  '
  Private Function SetupRow(ByVal DayString1 As String, _
                            ByVal DayString2 As String, _
                            ByVal Row As DataRow, _
                            ByRef Count As Integer, _
                            ByRef MoreThanMax As Boolean, _
                            ByRef TerminalAnyMasterIdIsConnected As Boolean) As Boolean
    Dim item_provider As TYPE_PROVIDER_ITEM
    Dim color As Integer

    item_provider.name = Row(SQL_COLUMN_PROVIDER)
    item_provider.terminal = Row(SQL_COLUMN_TERMINAL)
    item_provider.terminal_id = IIf(Row.IsNull(SQL_COLUMN_TERMINAL_ID), 0, Row(SQL_COLUMN_TERMINAL_ID))
    item_provider.terminal_any_master_id_is_connected = TerminalAnyMasterIdIsConnected
    item_provider.num_terminals = IIf(Row.IsNull(SQL_COLUMN_NUM_TERMINALS), 0, Row(SQL_COLUMN_NUM_TERMINALS))
    item_provider.played = Row(SQL_COLUMN_PLAYED)
    item_provider.won = Row(SQL_COLUMN_WON)
    item_provider.re_played = Row(SQL_COLUMN_RE_PLAYED)
    item_provider.re_won = Row(SQL_COLUMN_RE_WON)
    item_provider.nr_played = Row(SQL_COLUMN_NR_PLAYED)
    item_provider.nr_won = Row(SQL_COLUMN_NR_WON)

    If Me.chk_empty_rows.Checked Then
      If item_provider.num_terminals <= 0 And item_provider.played = 0 And item_provider.won = 0 Then
        Return False
      End If
    End If

    If (item_provider.num_terminals = 0 _
        And (String.IsNullOrEmpty(item_provider.terminal) Or Not item_provider.terminal_any_master_id_is_connected)) _
        And (item_provider.played <> 0 Or item_provider.won <> 0) Then
      If Me.chk_terminals.Checked And String.IsNullOrEmpty(item_provider.terminal) Then
        color = ENUM_GUI_COLOR.GUI_COLOR_ORANGE_01
      Else
        color = ENUM_GUI_COLOR.GUI_COLOR_RED_01
      End If
    Else
      If Me.chk_terminals.Checked And String.IsNullOrEmpty(item_provider.terminal) Then
        color = ENUM_GUI_COLOR.GUI_COLOR_GREEN_01
      Else
        color = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
      End If
    End If

    MoreThanMax = DrawRow(DayString1, DayString2, item_provider, color, Count)

    Return True
  End Function ' SetupRow

  ' PURPOSE: Show results to screen
  '
  '  PARAMS:
  '     - INPUT:
  '           - DayString1
  '           - DayString2
  '           - Provider items with data
  '           - color of row
  '     - OUTPUT:
  '           - Counter of rows
  '
  ' RETURNS:
  '     - Boolean: Indicates if there are more than max rows.
  '
  Private Function DrawRow(ByVal DayString1 As String, ByVal DayString2 As String, ByVal ProviderItem As TYPE_PROVIDER_ITEM, ByVal Color As Integer, ByRef Count As Integer) As Boolean

    Dim idx_row, row_step As Integer
    Dim netwin As Decimal
    Dim re_netwin As Decimal
    Dim nr_netwin As Decimal
    Dim netwin_terminals As Decimal
    Dim re_netwin_terminals As Decimal
    Dim nr_netwin_terminals As Decimal
    Dim more_than_max, line_error As Boolean
    Dim _terminal_data As List(Of Object)
    Dim _gaming_table_row() As DataRow

    more_than_max = False
    line_error = False
    row_step = GUI_MaxRows()
    row_step = row_step / 20
    If row_step < MAX_RECORDS_FIRST_LOAD Then
      row_step = MAX_RECORDS_FIRST_LOAD
    End If

    netwin = 0
    re_netwin = 0
    nr_netwin = 0
    netwin_terminals = 0
    re_netwin_terminals = 0
    nr_netwin_terminals = 0

    netwin = ProviderItem.played - ProviderItem.won
    re_netwin = ProviderItem.re_played - ProviderItem.re_won
    nr_netwin = ProviderItem.nr_played - ProviderItem.nr_won

    ' Fixed Bug 8957: RCI & DDM 03-FEB-2016: The solution for cloned terminals in ONE day, breaks the usual case (not cloned).
    '                                        Commnet this code and has to be studied later.
    'If Not ProviderItem.terminal_any_master_id_is_connected Then
    If ProviderItem.num_terminals = 0 Then
      line_error = True
    Else
      line_error = False
      netwin_terminals = netwin / ProviderItem.num_terminals
      re_netwin_terminals = re_netwin / ProviderItem.num_terminals
      nr_netwin_terminals = nr_netwin / ProviderItem.num_terminals
    End If
    'Else
    'line_error = False
    'netwin_terminals = netwin
    're_netwin_terminals = re_netwin
    'nr_netwin_terminals = nr_netwin
    'End If

    With Me.Grid
      .AddRow()
      idx_row = Me.Grid.NumRows - 1

        .Cell(idx_row, GRID_COLUMN_DAY_FROM).Value = DayString1
        .Cell(idx_row, GRID_COLUMN_DAY_TO).Value = DayString2
        .Cell(idx_row, GRID_COLUMN_PROVIDER).Value = ProviderItem.name
      If (ProviderItem.name = m_gaming_tables_provider_name) Then
        'Gaming Tables
        If (chk_terminals.Checked) Then ' Showing details
          If (ProviderItem.terminal <> "") Then
            'Detail
            .Cell(idx_row, GRID_COLUMN_PROVIDER).Value = ""
            _gaming_table_row = m_banks_situation.Select("GT_NAME='" + ProviderItem.terminal + "'")
            .Cell(idx_row, GRID_COLUMN_PROVIDER + 1).Value = ProviderItem.terminal
            If (chk_terminal_location.Checked) Then
              If (_gaming_table_row.Length > 0) Then
                .Cell(idx_row, GRID_COLUMN_PROVIDER + 3).Value = _gaming_table_row(0)(1).ToString()
                .Cell(idx_row, GRID_COLUMN_PROVIDER + 4).Value = _gaming_table_row(0)(2).ToString()
              Else
                .Cell(idx_row, GRID_COLUMN_PROVIDER + 3).Value = "--"
                .Cell(idx_row, GRID_COLUMN_PROVIDER + 4).Value = "--"
              End If
            End If
          End If

        End If
      Else
        'Terminals
        If ProviderItem.terminal_id <> 0 Then
        ' Terminal Report
        _terminal_data = TerminalReport.GetReportDataList(ProviderItem.terminal_id, m_terminal_report_type)
        For _idx As Int32 = 0 To _terminal_data.Count - 1
          If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value Then
            .Cell(idx_row, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
          End If
        Next
        .Cell(idx_row, GRID_COLUMN_PROVIDER).Value = String.Empty
      End If
      End If

      ' Number of terminals
      If ProviderItem.num_terminals = 0 And ProviderItem.terminal_any_master_id_is_connected Then
        .Cell(idx_row, GRID_COLUMN_NUM_TERMINALS).Value = 1
      Else
        .Cell(idx_row, GRID_COLUMN_NUM_TERMINALS).Value = GUI_FormatNumber(ProviderItem.num_terminals, 0)
      End If

      ' Total Played
      .Cell(idx_row, GRID_COLUMN_PLAYED).Value = GUI_FormatCurrency(ProviderItem.played, 2)
      ' Total Won
      .Cell(idx_row, GRID_COLUMN_WON).Value = GUI_FormatCurrency(ProviderItem.won, 2)
      ' Total NetWin
      .Cell(idx_row, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(netwin, 2)

      ' Redeemable Played
      .Cell(idx_row, GRID_COLUMN_RE_PLAYED).Value = GUI_FormatCurrency(ProviderItem.re_played, 2)
      ' Redeemable Won
      .Cell(idx_row, GRID_COLUMN_RE_WON).Value = GUI_FormatCurrency(ProviderItem.re_won, 2)
      ' Redeemable NetWin
      .Cell(idx_row, GRID_COLUMN_RE_NETWIN).Value = GUI_FormatCurrency(re_netwin, 2)

      ' Non-Redeemable Played
      .Cell(idx_row, GRID_COLUMN_NR_PLAYED).Value = GUI_FormatCurrency(ProviderItem.nr_played, 2)
      ' Non-Redeemable Won
      .Cell(idx_row, GRID_COLUMN_NR_WON).Value = GUI_FormatCurrency(ProviderItem.nr_won, 2)
      ' Non-Redeemable NetWin
      .Cell(idx_row, GRID_COLUMN_NR_NETWIN).Value = GUI_FormatCurrency(nr_netwin, 2)

      ' NetWin / Terminals
      If line_error = False Then
        .Cell(idx_row, GRID_COLUMN_NETWIN_TERMINALS).Value = GUI_FormatCurrency(netwin_terminals, 2)

        .Cell(idx_row, GRID_COLUMN_RE_NETWIN_TERMINALS).Value = GUI_FormatCurrency(re_netwin_terminals, 2)

        .Cell(idx_row, GRID_COLUMN_NR_NETWIN_TERMINALS).Value = GUI_FormatCurrency(nr_netwin_terminals, 2)
      Else
        .Cell(idx_row, GRID_COLUMN_NETWIN_TERMINALS).Value = ""
        .Cell(idx_row, GRID_COLUMN_RE_NETWIN_TERMINALS).Value = ""
        .Cell(idx_row, GRID_COLUMN_NR_NETWIN_TERMINALS).Value = ""
      End If

      .Row(idx_row).BackColor = GetColor(Color)

      Count = Count + 1

      If Count Mod row_step = 0 Then
        .Redraw = True
        Call Application.DoEvents()
        Windows.Forms.Cursor.Current = Cursors.WaitCursor
        .Redraw = False
      End If

    End With

    If Count >= GUI_MaxRows() Then
      more_than_max = True
    End If

    Return more_than_max
  End Function ' DrawRow

  ' PURPOSE: Create and return a DataTable to save provider/terminal activity
  '
  '  PARAMS:
  '     - INPUT:
  '           - TableName
  '           - Type
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - DataTable
  '
  Private Function CreateTableActivity(ByVal TableName As String, ByVal Type As ENUM_TYPE_TABLE) As DataTable
    Dim table As DataTable

    table = New DataTable(TableName)
    table.Columns.Add("ORDERED", System.Type.GetType("System.Int32"))
    table.Columns.Add("Provider", System.Type.GetType("System.String"))
    table.Columns.Add("Terminal", System.Type.GetType("System.String"))
    table.Columns.Add("NumTerminals", System.Type.GetType("System.Int32")).DefaultValue = 0
    table.Columns.Add("TerminalId", System.Type.GetType("System.Int32")).DefaultValue = 0
    table.Columns.Add("PlayedAmount", System.Type.GetType("System.Decimal")).DefaultValue = 0
    table.Columns.Add("WonAmount", System.Type.GetType("System.Decimal")).DefaultValue = 0
    table.Columns.Add("RedeemablePlayedAmount", System.Type.GetType("System.Decimal")).DefaultValue = 0
    table.Columns.Add("RedeemableWonAmount", System.Type.GetType("System.Decimal")).DefaultValue = 0
    table.Columns.Add("NonRedeemablePlayedAmount", System.Type.GetType("System.Decimal")).DefaultValue = 0
    table.Columns.Add("NonRedeemableWonAmount", System.Type.GetType("System.Decimal")).DefaultValue = 0
    table.Columns.Add("TerminalMasterId", System.Type.GetType("System.Int32")).DefaultValue = 0

    Select Case Type
      Case ENUM_TYPE_TABLE.PROVIDER
        table.PrimaryKey = New DataColumn() {table.Columns(SQL_COLUMN_PROVIDER)}
        table.Columns(SQL_COLUMN_TERMINAL).DefaultValue = ""

      Case ENUM_TYPE_TABLE.TERMINAL
        table.PrimaryKey = New DataColumn() {table.Columns(SQL_COLUMN_PROVIDER), table.Columns(SQL_COLUMN_TERMINAL)}
    End Select

    Return table
  End Function ' CreateTableActivity

  Private Sub GridColumnReIndex()
    If chk_terminals.Checked Then
      TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(m_terminal_report_type)
    Else
      TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(ReportType.Provider)
    End If

    GRID_COLUMN_INDEX = 0
    GRID_COLUMN_DAY_FROM = 1
    GRID_COLUMN_DAY_TO = 2
    GRID_INIT_TERMINAL_DATA = 3
    GRID_COLUMN_PROVIDER = GRID_INIT_TERMINAL_DATA
    GRID_COLUMN_NUM_TERMINALS = TERMINAL_DATA_COLUMNS + 3
    GRID_COLUMN_RE_PLAYED = TERMINAL_DATA_COLUMNS + 4
    GRID_COLUMN_RE_WON = TERMINAL_DATA_COLUMNS + 5
    GRID_COLUMN_RE_NETWIN = TERMINAL_DATA_COLUMNS + 6
    GRID_COLUMN_RE_NETWIN_TERMINALS = TERMINAL_DATA_COLUMNS + 7
    GRID_COLUMN_NR_PLAYED = TERMINAL_DATA_COLUMNS + 8
    GRID_COLUMN_NR_WON = TERMINAL_DATA_COLUMNS + 9
    GRID_COLUMN_NR_NETWIN = TERMINAL_DATA_COLUMNS + 10
    GRID_COLUMN_NR_NETWIN_TERMINALS = TERMINAL_DATA_COLUMNS + 11
    GRID_COLUMN_PLAYED = TERMINAL_DATA_COLUMNS + 12
    GRID_COLUMN_WON = TERMINAL_DATA_COLUMNS + 13
    GRID_COLUMN_NETWIN = TERMINAL_DATA_COLUMNS + 14
    GRID_COLUMN_NETWIN_TERMINALS = TERMINAL_DATA_COLUMNS + 15

    GRID_COLUMNS = TERMINAL_DATA_COLUMNS + 16
  End Sub

#End Region ' Private Functions

#Region " Events "

  Private Sub opt_detailed_day_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk_detailed_day.CheckedChanged
    If Me.chk_detailed_day.Checked Then
      Me.chk_total_day.Enabled = True
    Else
      Me.chk_total_day.Checked = False
      Me.chk_total_day.Enabled = False
    End If
  End Sub

  Private Sub chk_terminal_location_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminal_location.CheckedChanged
    If chk_terminal_location.Checked Then
      m_terminal_report_type = ReportType.Provider + ReportType.Location
    Else
      m_terminal_report_type = ReportType.Provider
    End If

    m_refresh_grid = True
  End Sub

  Private Sub chk_terminals_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminals.CheckedChanged
    Me.chk_terminal_location.Enabled = chk_terminals.Checked

    m_refresh_grid = True
  End Sub

#End Region ' Events

End Class ' frm_provider_day_terminal
