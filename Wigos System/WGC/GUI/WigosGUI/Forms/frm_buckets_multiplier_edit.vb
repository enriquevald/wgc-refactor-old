'-------------------------------------------------------------------
' Copyright © 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_buckets_multiplier_edit
' DESCRIPTION:   New or Edit buckets multiplier
' AUTHOR:        Xavi Guirado Jornet
' CREATION DATE: 12-AGU-2016
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 12-AGU-2016  XGJ    Initial version.
' 27-SET-2016  SMN    Fixed Bug 18241:Multiplicador de buckets: no se pueden realizar modificaciones
' 11-GEN-2017  XGJ    PBI 22544: Buckets - Multiplicador: Niveles
' 29-NOV-2017  RAB    Bug 30936:WIGOS-6344 [Ticket #10080] Fallo Bucket Multiplicadores V03.06.0035
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Text
Imports WSI.Common
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE

Public Class frm_buckets_multiplier_edit
  Inherits frm_base_edit

#Region "Members"
  Private m_is_center As Boolean = GeneralParam.GetBoolean("MultiSite", "IsCenter", False)
  Private m_elp_mode As Integer = GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode")
  Private m_buckets As CLASS_BUCKETS_MULTIPLIER
  Private m_datatable_buckets As DataTable
  Private m_datatable_levels As DataTable
#End Region

#Region "Constants"
  Private Const LEN_MAX_DESCRIPTION = 50
  Private Const LEN_MAX_MULTIPLIER = 5
  Private Const LEN_MAX_DECIMALS = 2

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_LEVEL_DESCRIPTION As Integer = 1
  Private Const GRID_COLUMN_LEVEL_CHECK As Integer = 2

  Private Const GRID_WIDTH_INDEX As Integer = 200
  Private Const GRID_WIDTH_ID As Integer = 0
  Private Const GRID_WIDTH_LEVEL_DESCRIPTION As Integer = 2000
  Private Const GRID_WIDTH_LEVEL_CHECK As Integer = 500

  Private Const GRID_COLUMNS As Integer = 3
  Private Const GRID_HEADER_ROWS As Integer = 1
#End Region

#Region "Enums"

#End Region

#Region "Structures"

#End Region

#Region "Overrides"
  Dim ef As String

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_FORM.FORM_BUCKETS_MULTIPLIER_EDIT

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    ' Form
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7508)

    ' Buttons
    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False

    Me.gb_bucket.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6969)

    ' Buckets
    Me.cmb_buckets.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6969) ' Bucket
    m_datatable_buckets = GUI_GetTableUsingSQL("SELECT BU_BUCKET_ID, BU_NAME FROM BUCKETS WHERE BU_MULTIPLIER_ENABLED =1 ORDER BY BU_NAME", Integer.MaxValue)
    Me.cmb_buckets.Add(m_datatable_buckets)

    ' Description
    Me.ef_description.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7192)
    Me.ef_description.TextVisible = True
    Me.ef_description.Value = ""
    Me.ef_description.Focus()
    Me.ef_description.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_MAX_DESCRIPTION)

    ' Multiplier
    Me.ef_multiplier.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(568)
    Me.ef_multiplier.TextVisible = True
    Me.ef_multiplier.Value = ""
    Me.ef_multiplier.Focus()
    Me.ef_multiplier.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, LEN_MAX_MULTIPLIER, LEN_MAX_DECIMALS)

    ' Enabled
    Me.chk_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5218)

    ' Providers
    If m_is_center Then
      Me.uc_terminals_group_filter.SetGroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5819)
      Me.uc_terminals_group_filter.ShowGroups = New List(Of WSI.Common.Groups.GROUP_NODE_ELEMENT)
      Me.uc_terminals_group_filter.ShowGroups.Add(WSI.Common.Groups.GROUP_NODE_ELEMENT.PROVIDER)
    Else
      Me.uc_terminals_group_filter.SetGroupBoxText = GLB_NLS_GUI_CONTROLS.GetString(477)
    End If
    Me.uc_terminals_group_filter.SetTerminalList(New TerminalList())

    ' uc_Schedule control
    uc_schedule.SetDefaults()
    uc_schedule.ShowCheckBoxDateTo = True
    uc_schedule.LabelDateFrom = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5797)

    ' Sites
    If m_is_center Then
      Me.uc_site_select.ShowMultisiteRow = False
      Me.uc_site_select.Init()
      Me.uc_site_select.SetDefaultValues(False)
    Else
      Me.uc_site_select.Visible = False
    End If

    ' Group Box Aplica a niveles
    Me.gp_apply_levels.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5210)
    Me.dg_apply.PanelRightVisible = False

    EnableControls(True)

    If Not m_is_center Then
      Me.Size = New Size(Me.Width, Me.Height - 150)
    End If

    ' XGJ 12-GEN-2017
    GUI_dg_apply_StyleSheet()
    dg_aply_load()

    GUI_SetInitialFocus()
  End Sub

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    m_buckets = DbReadObject

    ' Description
    Me.ef_description.TextValue = m_buckets.Description

    'Multiplier
    Me.ef_multiplier.Value = m_buckets.Multiplier

    ' Enabled
    chk_enabled.Checked = m_buckets.Enabled

    ' Terminals
    uc_terminals_group_filter.SetTerminalList(m_buckets.TerminalList)

    uc_schedule.DateFrom = m_buckets.ScheduleStart

    If m_buckets.ScheduleEnd = Nothing Then
      uc_schedule.CheckedDateTo = False
    Else
      uc_schedule.DateTo = m_buckets.ScheduleEnd
    End If

    If m_buckets.ScheduleWeekday = Nothing Then
      Me.uc_schedule.Weekday = Me.uc_schedule.AllWeekDaysSelected
    Else
      uc_schedule.Weekday = m_buckets.ScheduleWeekday
    End If
    uc_schedule.TimeFrom = m_buckets.Schedule1TimeFrom
    uc_schedule.TimeTo = m_buckets.Schedule1TimeTo
    uc_schedule.SecondTime = m_buckets.Schedule2Enabled
    uc_schedule.SecondTimeFrom = m_buckets.Schedule2TimeFrom
    uc_schedule.SecondTimeTo = m_buckets.Schedule2TimeTo

    ' Sites
    If m_is_center Then
      uc_site_select.SetSitesIdListSelected(m_buckets.SiteList)
    End If

    ' Bucket levels afected
    For _index As Integer = 0 To m_buckets.BucketLevels.Length - 1
      If (m_buckets.BucketLevels(_index) = "1") Then
        dg_apply.Cell(_index, GRID_COLUMN_LEVEL_CHECK).Value = BoolToGridValue(True)
      Else
        dg_apply.Cell(_index, GRID_COLUMN_LEVEL_CHECK).Value = BoolToGridValue(False)
      End If
    Next

  End Sub ' GUI_SetScreenData

  ' PURPOSE: Validate the data presented on the screen. The following values are not empty: Holder Name, RFC, CURP
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - True:  Data is ok
  '     - False: Data is NOT ok
  '
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Dim _nls_param1 As String

    ' Empty Description
    If (String.IsNullOrEmpty(Me.ef_description.TextValue)) Then
      ' 3441 "El campo 'Descripción' es obligatorio"
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , Me.ef_description.Text)
      Call Me.ef_description.Focus()
      Return False
    End If

    If Not (String.IsNullOrEmpty(Me.ef_description.TextValue)) Then
      Using _db_trx As WSI.Common.DB_TRX = New WSI.Common.DB_TRX()

        If CkeckBucketMultiplierDuplicate(ef_description.TextValue, m_buckets.MsgId, _db_trx.SqlTransaction) Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7618), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , Me.ef_description.TextValue)
          Return False
        End If

        _db_trx.Commit()

      End Using
    End If

    ' Empty Bucket
    If (String.IsNullOrEmpty(Me.cmb_buckets.TextValue)) Then
      ' 3441 "El campo 'Bucket' es obligatorio"
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , Me.cmb_buckets.Text)
      Call Me.cmb_buckets.Focus()
      Return False
    End If

    ' Empty Multiplier
    If (String.IsNullOrEmpty(Me.ef_multiplier.TextValue) Or (Convert.ToDecimal(Me.ef_multiplier.Value) = 0)) Then
      ' 3441 "El campo 'Multiplier' es obligatorio"
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5335), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , Me.ef_multiplier.Text, "0")
      Call Me.ef_multiplier.Focus()
      Return False
    End If

    ' Empty Multiplier
    If (Me.ef_multiplier.Value >= 10) Then
      ' 3441 "El campo 'Multiplier' es obligatorio"
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7582), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , Me.ef_multiplier.Text, "10")
      Call Me.ef_multiplier.Focus()
      Return False
    End If

    If Not m_is_center Then
      ' Providers (if listed or not listed must have at least one selected)
      If Not Me.uc_terminals_group_filter.CheckAtLeastOneSelected() Then
        _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(172)
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
        Call uc_terminals_group_filter.Focus()

        Return False
      End If
    End If

    If m_is_center Then
      ' You must select at least one option from: %1
      If String.IsNullOrEmpty(Me.uc_site_select.GetSitesIdListSelectedAll()) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1292), ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1803))
        Call uc_site_select.Focus()

        Return False
      End If
    End If

    ' Expiration dates range
    If uc_schedule.DateFrom > uc_schedule.DateTo Then
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call uc_schedule.SetFocus(1)

      Return False
    End If

    ' No Week days selected
    If uc_schedule.Weekday = uc_schedule.NoWeekDaysSelected() Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(110), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call uc_schedule.SetFocus(4)

      Return False
    End If

    Return True

  End Function

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_GetScreenData()
    Dim _levels As String

    _levels = ""
    m_buckets = Me.DbEditedObject

    ' Enabled
    m_buckets.Enabled = chk_enabled.Checked

    ' Description
    m_buckets.Description = Me.ef_description.TextValue

    ' Multiplier  
    If (Me.ef_multiplier.Value.ToString() = "") Then
      m_buckets.Multiplier = 0
    Else
      m_buckets.Multiplier = Me.ef_multiplier.Value
    End If

    ' ScheduleStart
    m_buckets.ScheduleStart = uc_schedule.DateFrom

    ' ScheduleEnd
    If uc_schedule.CheckedDateTo Then
      m_buckets.ScheduleEnd = uc_schedule.DateTo
    Else
      m_buckets.ScheduleEnd = Nothing
    End If

    ' ScheduleWeekday
    m_buckets.ScheduleWeekday = uc_schedule.Weekday

    ' Schedule1TimeFrom
    m_buckets.Schedule1TimeFrom = uc_schedule.TimeFrom

    ' Schedule1TimeTo
    m_buckets.Schedule1TimeTo = uc_schedule.TimeTo

    ' Schedule2Enabled
    m_buckets.Schedule2Enabled = uc_schedule.SecondTime

    ' Schedule2TimeFrom
    m_buckets.Schedule2TimeFrom = uc_schedule.SecondTimeFrom

    ' Schedule2TimeTo
    m_buckets.Schedule2TimeTo = uc_schedule.SecondTimeTo

    ' Terminals
    uc_terminals_group_filter.GetTerminalsFromControl(m_buckets.TerminalList)

    ' Sites
    If m_is_center Then
      m_buckets.SiteList = uc_site_select.GetSitesIdListSelectedAll()
    End If

    ' Bucked afected
    m_buckets.BucketAfectedId = Me.cmb_buckets.Value
    m_buckets.BucketName = Me.cmb_buckets.TextValue

    ' Bucket levels afected
    For _index As Integer = 0 To Me.dg_apply.NumRows - 1
      If (Me.dg_apply.Cell(_index, GRID_COLUMN_LEVEL_CHECK).Value) Then
        _levels = _levels + "0"
      Else
        _levels = _levels + "1"
      End If
    Next
    m_buckets.BucketLevels = _levels

  End Sub

  ' PURPOSE : Init form in edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '       - UserId
  '       - Username
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '
  Public Overloads Sub ShowEditItem(ByVal MessageId As Int64)
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Me.DbObjectId = MessageId

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)

    If Me.DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If Me.DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub

  ' PURPOSE : Override form permissions to disallow writting in MultiSite Member
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As GUI_Controls.CLASS_GUI_USER.TYPE_PERMISSIONS)

    MyBase.GUI_Permissions(AndPerm)

    'If Not m_is_center And m_buckets.MasterSequenceId <> 0 Then
    '  AndPerm.Write = False
    'End If

  End Sub

  ' PURPOSE: Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.cmb_buckets

  End Sub ' GUI_SetInitialFocus

#End Region

#Region "Private"
  ' PURPOSE: Enable/disable controls
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub EnableControls(ByVal Enable As Boolean)

    Me.ef_description.Enabled = Enable
    Me.ef_multiplier.Enabled = Enable
    cmb_buckets.Enabled = Enable
    chk_enabled.Enabled = Enable
    If Enable Then
      uc_terminals_group_filter.Enabled = True
    Else
      uc_terminals_group_filter.Enabled = True
      Me.uc_terminals_group_filter.ThreeStateCheckMode = False
    End If
    uc_schedule.Enabled = Enable
    uc_site_select.Enabled = Enable
    gb_bucket.Enabled = Enable

  End Sub

  ''' <summary>
  ''' Return
  ''' </summary>
  ''' <param name="MultiplierDescription"></param>
  ''' <param name="BmBucketId"></param>
  ''' <param name="SqlTrans"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CkeckBucketMultiplierDuplicate(ByVal MultiplierDescription As String, ByVal BmBucketId As Integer, ByVal SqlTrans As SqlClient.SqlTransaction) As Boolean
    Dim _sb As StringBuilder
    Dim _count_multipliers As Integer

    Try

      _sb = New StringBuilder()

      _sb.AppendLine("  SELECT  COUNT(*)                                 ")
      _sb.AppendLine("    FROM  BUCKETS_MULTIPLIER_SCHEDULE              ")
      _sb.AppendLine("   WHERE  BM_BUCKET_DESCRIPTION = @pMultiplierDesc ")
      _sb.AppendLine("     AND  BM_BUCKET_ID <> @pBmBucketId             ")


      Using _cmd As New SqlClient.SqlCommand(_sb.ToString, SqlTrans.Connection, SqlTrans)
        _cmd.Parameters.Add("@pMultiplierDesc", SqlDbType.NVarChar, 250).Value = MultiplierDescription
        _cmd.Parameters.Add("@pBmBucketId", SqlDbType.BigInt).Value = BmBucketId

        _count_multipliers = _cmd.ExecuteScalar()

        If _count_multipliers = 0 Then

          Return False
        End If

      End Using

    Catch ex As Exception
    End Try

    Return True
  End Function ' CkeckBucketMultiplierDuplicate

  ' PURPOSE: Configure grid of levels
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub GUI_dg_apply_StyleSheet()

    With Me.dg_apply
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, True)

      ' INDEX 
      .Column(GRID_COLUMN_INDEX).Header.Text = ""
      .Column(GRID_COLUMN_INDEX).WidthFixed = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Level
      .Column(GRID_COLUMN_LEVEL_DESCRIPTION).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(443)
      .Column(GRID_COLUMN_LEVEL_DESCRIPTION).Width = GRID_WIDTH_LEVEL_DESCRIPTION
      .Column(GRID_COLUMN_LEVEL_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Check
      .Column(GRID_COLUMN_LEVEL_CHECK).Header.Text = ""
      .Column(GRID_COLUMN_LEVEL_CHECK).Width = GRID_WIDTH_LEVEL_CHECK
      .Column(GRID_COLUMN_LEVEL_CHECK).EditionControl.Type = CONTROL_TYPE_CHECK_BOX
      .Column(GRID_COLUMN_LEVEL_CHECK).Editable = True
      .Column(GRID_COLUMN_LEVEL_CHECK).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
    End With
  End Sub

  ' PURPOSE: Init grid of levels
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub dg_aply_load()
    Dim _count As Integer

    m_datatable_levels = GUI_GetTableUsingSQL("SELECT gp_key_value FROM general_params WHERE gp_group_key = 'PlayerTracking' and gp_subject_key like 'Level%.name'", Integer.MaxValue)

    For index As Integer = 0 To m_datatable_levels.Rows.Count - 1
      _count = dg_apply.AddRow()
      dg_apply.Cell(_count, GRID_COLUMN_LEVEL_DESCRIPTION).Value = m_datatable_levels.Rows.Item(index).Item(0).ToString()

    Next


  End Sub

  ' PURPOSE:  Cast from boolean to uc_grid checked
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function BoolToGridValue(ByVal enabled As Boolean) As String

    Return IIf(enabled, uc_grid.GRID_CHK_CHECKED, uc_grid.GRID_CHK_UNCHECKED)

  End Function     ' BoolToGridValue

#End Region

#Region "Public"

  ' PURPOSE: Init form in new mode
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overloads Sub ShowNewItem(Optional ByVal MultiplierCopiedId As Long = 0)
    Dim _bucket As CLASS_BUCKETS_MULTIPLIER

    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    DbObjectId = Nothing
    DbStatus = ENUM_STATUS.STATUS_OK

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    If MultiplierCopiedId > 0 Then
      _bucket = DbEditedObject
      DbObjectId = MultiplierCopiedId
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
      DbObjectId = Nothing

      Call _bucket.ResetIdValues()
    End If

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowNewItem

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)
    Dim _aux_date As DateTime

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        Me.DbEditedObject = New CLASS_BUCKETS_MULTIPLIER
        m_buckets = DbEditedObject
        _aux_date = Now.AddMonths(1)
        m_buckets.Enabled = True
        m_buckets.ScheduleStart = New DateTime(_aux_date.Year, _aux_date.Month, 1, GetDefaultClosingTime(), 0, 0)
        m_buckets.ScheduleEnd = m_buckets.ScheduleStart.AddMonths(1)
        m_buckets.Schedule1TimeFrom = 0     ' 00:00:00
        m_buckets.Schedule1TimeTo = 0       ' 00:00:00
        m_buckets.Schedule2Enabled = False
        m_buckets.Schedule2TimeFrom = 0     ' 00:00:00
        m_buckets.Schedule2TimeTo = 0       ' 00:00:00
        m_buckets.Description = ""
        m_buckets.Multiplier = 0
        m_buckets.ScheduleWeekday = uc_schedule.AllWeekDaysSelected() ' all days
        m_buckets.BucketName = Me.cmb_buckets.TextValue
        m_buckets.BucketLevels = ""

        Me.DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          If Me.DbStatus = ENUM_STATUS.STATUS_NOT_FOUND Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7514), ENUM_MB_TYPE.MB_TYPE_ERROR)
          Else
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7515), ENUM_MB_TYPE.MB_TYPE_ERROR)
          End If

        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7516), ENUM_MB_TYPE.MB_TYPE_ERROR)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          If Me.DbStatus = ENUM_STATUS.STATUS_NOT_FOUND Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7514), ENUM_MB_TYPE.MB_TYPE_ERROR)
          Else
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7517), ENUM_MB_TYPE.MB_TYPE_ERROR)
          End If
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        ' Not used
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4305), ENUM_MB_TYPE.MB_TYPE_ERROR)
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)
    End Select

  End Sub ' GUI_DB_Operation


#End Region

#Region "Events"


#End Region


End Class