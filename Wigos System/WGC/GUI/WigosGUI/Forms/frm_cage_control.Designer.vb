<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_cage_control
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_cage_control))
    Me.Label1 = New System.Windows.Forms.Label()
    Me.Uc_text_field1 = New GUI_Controls.uc_text_field()
    Me.Label2 = New System.Windows.Forms.Label()
    Me.Uc_text_field2 = New GUI_Controls.uc_text_field()
    Me.Label3 = New System.Windows.Forms.Label()
    Me.Uc_text_field3 = New GUI_Controls.uc_text_field()
    Me.gb_cashier = New System.Windows.Forms.GroupBox()
    Me.ef_cashier_date = New GUI_Controls.uc_entry_field()
    Me.panel_cashier_type = New System.Windows.Forms.Panel()
    Me.rb_to_terminal = New System.Windows.Forms.RadioButton()
    Me.rb_to_user = New System.Windows.Forms.RadioButton()
    Me.cmb_terminals = New GUI_Controls.uc_combo()
    Me.ef_cashier_name = New GUI_Controls.uc_entry_field()
    Me.ef_user_terminals_name = New GUI_Controls.uc_entry_field()
    Me.cmb_user = New GUI_Controls.uc_combo()
    Me.cmb_gaming_table = New GUI_Controls.uc_combo()
    Me.panel_gaming_table_type = New System.Windows.Forms.Panel()
    Me.ef_gaming_table_visits = New GUI_Controls.uc_entry_field()
    Me.cb_close_gaming_table = New System.Windows.Forms.CheckBox()
    Me.gb_custom = New System.Windows.Forms.GroupBox()
    Me.ef_custom = New GUI_Controls.uc_entry_field()
    Me.cmb_custom = New GUI_Controls.uc_combo()
    Me.btn_load_gaming_values = New GUI_Controls.uc_button()
    Me.gb_operation = New System.Windows.Forms.GroupBox()
    Me.ef_date_cage = New GUI_Controls.uc_entry_field()
    Me.ef_status_movement = New GUI_Controls.uc_entry_field()
    Me.ef_cage_user = New GUI_Controls.uc_entry_field()
    Me.ef_operation = New GUI_Controls.uc_entry_field()
    Me.gb_total = New System.Windows.Forms.GroupBox()
    Me.dg_total = New GUI_Controls.uc_grid()
    Me.gb_movement_type = New System.Windows.Forms.GroupBox()
    Me.rb_reception = New System.Windows.Forms.RadioButton()
    Me.rb_send = New System.Windows.Forms.RadioButton()
    Me.gb_source_target = New System.Windows.Forms.GroupBox()
    Me.rb_terminal = New System.Windows.Forms.RadioButton()
    Me.rb_gaming_table = New System.Windows.Forms.RadioButton()
    Me.rb_cashier = New System.Windows.Forms.RadioButton()
    Me.rb_custom = New System.Windows.Forms.RadioButton()
    Me.gb_gaming_table = New System.Windows.Forms.GroupBox()
    Me.lb_gt_without_integrated_cashier = New System.Windows.Forms.Label()
    Me.ef_gaming_table = New GUI_Controls.uc_entry_field()
    Me.panel_aux = New System.Windows.Forms.Panel()
    Me.tb_concepts = New System.Windows.Forms.TabControl()
    Me.TabPage1 = New System.Windows.Forms.TabPage()
    Me.dg_concepts = New GUI_Controls.uc_grid()
    Me.tb_currency = New System.Windows.Forms.TabControl()
    Me.lbl_error1 = New System.Windows.Forms.Label()
    Me.lbl_ok1 = New System.Windows.Forms.Label()
    Me.gb_collection_params = New System.Windows.Forms.GroupBox()
    Me.ef_floor_id = New GUI_Controls.uc_entry_field()
    Me.ef_stacker_id = New GUI_Controls.uc_entry_field()
    Me.lbl_notes = New System.Windows.Forms.Label()
    Me.tb_notes = New System.Windows.Forms.TextBox()
    Me.ef_extraction_date = New GUI_Controls.uc_entry_field()
    Me.ef_insertion_date = New GUI_Controls.uc_entry_field()
    Me.ef_employee = New GUI_Controls.uc_entry_field()
    Me.ef_terminal = New GUI_Controls.uc_entry_field()
    Me.lb_note_counter_status = New System.Windows.Forms.Label()
    Me.btn_open_events = New GUI_Controls.uc_button()
    Me.lbl_counter = New System.Windows.Forms.Label()
    Me.lb_error_messages = New System.Windows.Forms.Label()
    Me.tab_ticket = New System.Windows.Forms.TabPage()
    Me.ef_num_of_tickets = New GUI_Controls.uc_entry_field()
    Me.gb_status1 = New System.Windows.Forms.GroupBox()
    Me.lbl_pending = New System.Windows.Forms.Label()
    Me.tf_pending = New GUI_Controls.uc_text_field()
    Me.lbl_error = New System.Windows.Forms.Label()
    Me.tf_error1 = New GUI_Controls.uc_text_field()
    Me.Label5 = New System.Windows.Forms.Label()
    Me.tf_ok1 = New GUI_Controls.uc_text_field()
    Me.dg_collection_tickets = New GUI_Controls.uc_grid()
    Me.ef_ticket = New GUI_Controls.uc_entry_field()
    Me.btn_save_tickets = New GUI_Controls.uc_button()
    Me.tb_collections = New System.Windows.Forms.TabControl()
    Me.cmb_cage_sessions = New GUI_Controls.uc_combo()
    Me.gb_cage_sessions = New System.Windows.Forms.GroupBox()
    Me.ef_cage_session = New GUI_Controls.uc_entry_field()
    Me.cmb_cage_template = New GUI_Controls.uc_combo()
    Me.gb_template = New System.Windows.Forms.GroupBox()
    Me.tmr_note_counter_working = New System.Windows.Forms.Timer(Me.components)
    Me.gb_note_counter_status = New System.Windows.Forms.GroupBox()
    Me.gb_note_counter_events = New System.Windows.Forms.GroupBox()
    Me.panel_data.SuspendLayout()
    Me.gb_cashier.SuspendLayout()
    Me.panel_cashier_type.SuspendLayout()
    Me.panel_gaming_table_type.SuspendLayout()
    Me.gb_custom.SuspendLayout()
    Me.gb_operation.SuspendLayout()
    Me.gb_total.SuspendLayout()
    Me.gb_movement_type.SuspendLayout()
    Me.gb_source_target.SuspendLayout()
    Me.gb_gaming_table.SuspendLayout()
    Me.panel_aux.SuspendLayout()
    Me.tb_concepts.SuspendLayout()
    Me.gb_collection_params.SuspendLayout()
    Me.tab_ticket.SuspendLayout()
    Me.gb_status1.SuspendLayout()
    Me.tb_collections.SuspendLayout()
    Me.gb_cage_sessions.SuspendLayout()
    Me.gb_template.SuspendLayout()
    Me.gb_note_counter_status.SuspendLayout()
    Me.gb_note_counter_events.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_template)
    Me.panel_data.Controls.Add(Me.gb_note_counter_events)
    Me.panel_data.Controls.Add(Me.gb_note_counter_status)
    Me.panel_data.Controls.Add(Me.gb_cage_sessions)
    Me.panel_data.Controls.Add(Me.gb_collection_params)
    Me.panel_data.Controls.Add(Me.tb_collections)
    Me.panel_data.Controls.Add(Me.panel_aux)
    Me.panel_data.Controls.Add(Me.gb_gaming_table)
    Me.panel_data.Controls.Add(Me.gb_source_target)
    Me.panel_data.Controls.Add(Me.gb_movement_type)
    Me.panel_data.Controls.Add(Me.gb_cashier)
    Me.panel_data.Controls.Add(Me.gb_custom)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(980, 918)
    '
    'Label1
    '
    Me.Label1.BackColor = System.Drawing.SystemColors.GrayText
    Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Label1.Location = New System.Drawing.Point(10, 63)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(16, 16)
    Me.Label1.TabIndex = 110
    '
    'Uc_text_field1
    '
    Me.Uc_text_field1.IsReadOnly = True
    Me.Uc_text_field1.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.Uc_text_field1.LabelForeColor = System.Drawing.Color.Black
    Me.Uc_text_field1.Location = New System.Drawing.Point(4, 59)
    Me.Uc_text_field1.Name = "Uc_text_field1"
    Me.Uc_text_field1.Size = New System.Drawing.Size(116, 24)
    Me.Uc_text_field1.SufixText = "Sufix Text"
    Me.Uc_text_field1.SufixTextVisible = True
    Me.Uc_text_field1.TabIndex = 109
    Me.Uc_text_field1.TabStop = False
    Me.Uc_text_field1.TextVisible = False
    Me.Uc_text_field1.TextWidth = 30
    Me.Uc_text_field1.Value = "xInactive"
    '
    'Label2
    '
    Me.Label2.BackColor = System.Drawing.SystemColors.GrayText
    Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Label2.Location = New System.Drawing.Point(10, 41)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(16, 16)
    Me.Label2.TabIndex = 107
    '
    'Uc_text_field2
    '
    Me.Uc_text_field2.IsReadOnly = True
    Me.Uc_text_field2.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.Uc_text_field2.LabelForeColor = System.Drawing.Color.Black
    Me.Uc_text_field2.Location = New System.Drawing.Point(4, 37)
    Me.Uc_text_field2.Name = "Uc_text_field2"
    Me.Uc_text_field2.Size = New System.Drawing.Size(116, 24)
    Me.Uc_text_field2.SufixText = "Sufix Text"
    Me.Uc_text_field2.SufixTextVisible = True
    Me.Uc_text_field2.TabIndex = 108
    Me.Uc_text_field2.TabStop = False
    Me.Uc_text_field2.TextVisible = False
    Me.Uc_text_field2.TextWidth = 30
    Me.Uc_text_field2.Value = "xStandby"
    '
    'Label3
    '
    Me.Label3.BackColor = System.Drawing.SystemColors.Window
    Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Label3.Location = New System.Drawing.Point(10, 19)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(16, 16)
    Me.Label3.TabIndex = 106
    '
    'Uc_text_field3
    '
    Me.Uc_text_field3.IsReadOnly = True
    Me.Uc_text_field3.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.Uc_text_field3.LabelForeColor = System.Drawing.Color.Black
    Me.Uc_text_field3.Location = New System.Drawing.Point(4, 15)
    Me.Uc_text_field3.Name = "Uc_text_field3"
    Me.Uc_text_field3.Size = New System.Drawing.Size(114, 24)
    Me.Uc_text_field3.SufixText = "Sufix Text"
    Me.Uc_text_field3.SufixTextVisible = True
    Me.Uc_text_field3.TabIndex = 104
    Me.Uc_text_field3.TabStop = False
    Me.Uc_text_field3.TextVisible = False
    Me.Uc_text_field3.TextWidth = 30
    Me.Uc_text_field3.Value = "xRunning"
    '
    'gb_cashier
    '
    Me.gb_cashier.Controls.Add(Me.ef_cashier_date)
    Me.gb_cashier.Controls.Add(Me.panel_cashier_type)
    Me.gb_cashier.Controls.Add(Me.cmb_terminals)
    Me.gb_cashier.Controls.Add(Me.ef_cashier_name)
    Me.gb_cashier.Controls.Add(Me.ef_user_terminals_name)
    Me.gb_cashier.Controls.Add(Me.cmb_user)
    Me.gb_cashier.Location = New System.Drawing.Point(3, 98)
    Me.gb_cashier.Name = "gb_cashier"
    Me.gb_cashier.Size = New System.Drawing.Size(956, 113)
    Me.gb_cashier.TabIndex = 5
    Me.gb_cashier.TabStop = False
    Me.gb_cashier.Text = "xCashier"
    '
    'ef_cashier_date
    '
    Me.ef_cashier_date.DoubleValue = 0.0R
    Me.ef_cashier_date.IntegerValue = 0
    Me.ef_cashier_date.IsReadOnly = True
    Me.ef_cashier_date.Location = New System.Drawing.Point(486, 46)
    Me.ef_cashier_date.Name = "ef_cashier_date"
    Me.ef_cashier_date.PlaceHolder = Nothing
    Me.ef_cashier_date.Size = New System.Drawing.Size(460, 24)
    Me.ef_cashier_date.SufixText = "Sufix Text"
    Me.ef_cashier_date.SufixTextVisible = True
    Me.ef_cashier_date.TabIndex = 4
    Me.ef_cashier_date.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_cashier_date.TextValue = ""
    Me.ef_cashier_date.TextWidth = 100
    Me.ef_cashier_date.Value = ""
    Me.ef_cashier_date.ValueForeColor = System.Drawing.Color.Blue
    '
    'panel_cashier_type
    '
    Me.panel_cashier_type.Controls.Add(Me.rb_to_terminal)
    Me.panel_cashier_type.Controls.Add(Me.rb_to_user)
    Me.panel_cashier_type.Location = New System.Drawing.Point(590, 16)
    Me.panel_cashier_type.Name = "panel_cashier_type"
    Me.panel_cashier_type.Size = New System.Drawing.Size(341, 24)
    Me.panel_cashier_type.TabIndex = 3
    '
    'rb_to_terminal
    '
    Me.rb_to_terminal.AutoSize = True
    Me.rb_to_terminal.Location = New System.Drawing.Point(165, 3)
    Me.rb_to_terminal.Name = "rb_to_terminal"
    Me.rb_to_terminal.Size = New System.Drawing.Size(94, 17)
    Me.rb_to_terminal.TabIndex = 1
    Me.rb_to_terminal.TabStop = True
    Me.rb_to_terminal.Text = "xToTerminal"
    Me.rb_to_terminal.UseVisualStyleBackColor = True
    '
    'rb_to_user
    '
    Me.rb_to_user.AutoSize = True
    Me.rb_to_user.Location = New System.Drawing.Point(18, 3)
    Me.rb_to_user.Name = "rb_to_user"
    Me.rb_to_user.Size = New System.Drawing.Size(71, 17)
    Me.rb_to_user.TabIndex = 0
    Me.rb_to_user.TabStop = True
    Me.rb_to_user.Text = "xToUser"
    Me.rb_to_user.UseVisualStyleBackColor = True
    '
    'cmb_terminals
    '
    Me.cmb_terminals.AllowUnlistedValues = False
    Me.cmb_terminals.AutoCompleteMode = False
    Me.cmb_terminals.IsReadOnly = False
    Me.cmb_terminals.Location = New System.Drawing.Point(6, 78)
    Me.cmb_terminals.Name = "cmb_terminals"
    Me.cmb_terminals.SelectedIndex = -1
    Me.cmb_terminals.Size = New System.Drawing.Size(450, 24)
    Me.cmb_terminals.SufixText = "Sufix Text"
    Me.cmb_terminals.SufixTextVisible = True
    Me.cmb_terminals.TabIndex = 2
    Me.cmb_terminals.TextCombo = Nothing
    Me.cmb_terminals.TextWidth = 100
    '
    'ef_cashier_name
    '
    Me.ef_cashier_name.DoubleValue = 0.0R
    Me.ef_cashier_name.IntegerValue = 0
    Me.ef_cashier_name.IsReadOnly = True
    Me.ef_cashier_name.Location = New System.Drawing.Point(6, 46)
    Me.ef_cashier_name.Name = "ef_cashier_name"
    Me.ef_cashier_name.PlaceHolder = Nothing
    Me.ef_cashier_name.Size = New System.Drawing.Size(460, 24)
    Me.ef_cashier_name.SufixText = "Sufix Text"
    Me.ef_cashier_name.SufixTextVisible = True
    Me.ef_cashier_name.TabIndex = 1
    Me.ef_cashier_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_cashier_name.TextValue = ""
    Me.ef_cashier_name.TextWidth = 100
    Me.ef_cashier_name.Value = ""
    Me.ef_cashier_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_user_terminals_name
    '
    Me.ef_user_terminals_name.DoubleValue = 0.0R
    Me.ef_user_terminals_name.IntegerValue = 0
    Me.ef_user_terminals_name.IsReadOnly = True
    Me.ef_user_terminals_name.Location = New System.Drawing.Point(486, 78)
    Me.ef_user_terminals_name.Name = "ef_user_terminals_name"
    Me.ef_user_terminals_name.PlaceHolder = Nothing
    Me.ef_user_terminals_name.Size = New System.Drawing.Size(460, 24)
    Me.ef_user_terminals_name.SufixText = "Sufix Text"
    Me.ef_user_terminals_name.SufixTextVisible = True
    Me.ef_user_terminals_name.TabIndex = 5
    Me.ef_user_terminals_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_user_terminals_name.TextValue = ""
    Me.ef_user_terminals_name.TextWidth = 100
    Me.ef_user_terminals_name.Value = ""
    Me.ef_user_terminals_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'cmb_user
    '
    Me.cmb_user.AllowUnlistedValues = False
    Me.cmb_user.AutoCompleteMode = False
    Me.cmb_user.IsReadOnly = False
    Me.cmb_user.Location = New System.Drawing.Point(6, 16)
    Me.cmb_user.Name = "cmb_user"
    Me.cmb_user.SelectedIndex = -1
    Me.cmb_user.Size = New System.Drawing.Size(450, 24)
    Me.cmb_user.SufixText = "Sufix Text"
    Me.cmb_user.SufixTextVisible = True
    Me.cmb_user.TabIndex = 0
    Me.cmb_user.TextCombo = Nothing
    Me.cmb_user.TextWidth = 100
    '
    'cmb_gaming_table
    '
    Me.cmb_gaming_table.AllowUnlistedValues = False
    Me.cmb_gaming_table.AutoCompleteMode = False
    Me.cmb_gaming_table.IsReadOnly = False
    Me.cmb_gaming_table.Location = New System.Drawing.Point(6, 23)
    Me.cmb_gaming_table.Name = "cmb_gaming_table"
    Me.cmb_gaming_table.SelectedIndex = -1
    Me.cmb_gaming_table.Size = New System.Drawing.Size(450, 24)
    Me.cmb_gaming_table.SufixText = "Sufix Text"
    Me.cmb_gaming_table.SufixTextVisible = True
    Me.cmb_gaming_table.TabIndex = 0
    Me.cmb_gaming_table.TextCombo = Nothing
    Me.cmb_gaming_table.TextWidth = 100
    '
    'panel_gaming_table_type
    '
    Me.panel_gaming_table_type.Controls.Add(Me.ef_gaming_table_visits)
    Me.panel_gaming_table_type.Controls.Add(Me.cb_close_gaming_table)
    Me.panel_gaming_table_type.Location = New System.Drawing.Point(552, 10)
    Me.panel_gaming_table_type.Name = "panel_gaming_table_type"
    Me.panel_gaming_table_type.Size = New System.Drawing.Size(226, 48)
    Me.panel_gaming_table_type.TabIndex = 2
    '
    'ef_gaming_table_visits
    '
    Me.ef_gaming_table_visits.DoubleValue = 0.0R
    Me.ef_gaming_table_visits.IntegerValue = 0
    Me.ef_gaming_table_visits.IsReadOnly = False
    Me.ef_gaming_table_visits.Location = New System.Drawing.Point(3, 19)
    Me.ef_gaming_table_visits.Name = "ef_gaming_table_visits"
    Me.ef_gaming_table_visits.PlaceHolder = Nothing
    Me.ef_gaming_table_visits.Size = New System.Drawing.Size(209, 24)
    Me.ef_gaming_table_visits.SufixText = "Sufix Text"
    Me.ef_gaming_table_visits.SufixTextVisible = True
    Me.ef_gaming_table_visits.TabIndex = 1
    Me.ef_gaming_table_visits.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_gaming_table_visits.TextValue = ""
    Me.ef_gaming_table_visits.TextWidth = 60
    Me.ef_gaming_table_visits.Value = ""
    Me.ef_gaming_table_visits.ValueForeColor = System.Drawing.Color.Blue
    '
    'cb_close_gaming_table
    '
    Me.cb_close_gaming_table.AutoSize = True
    Me.cb_close_gaming_table.Location = New System.Drawing.Point(14, 2)
    Me.cb_close_gaming_table.Name = "cb_close_gaming_table"
    Me.cb_close_gaming_table.Size = New System.Drawing.Size(100, 17)
    Me.cb_close_gaming_table.TabIndex = 0
    Me.cb_close_gaming_table.Text = "xCerrarMesa"
    Me.cb_close_gaming_table.UseVisualStyleBackColor = True
    '
    'gb_custom
    '
    Me.gb_custom.Controls.Add(Me.ef_custom)
    Me.gb_custom.Controls.Add(Me.cmb_custom)
    Me.gb_custom.Location = New System.Drawing.Point(3, 300)
    Me.gb_custom.Name = "gb_custom"
    Me.gb_custom.Size = New System.Drawing.Size(956, 61)
    Me.gb_custom.TabIndex = 7
    Me.gb_custom.TabStop = False
    Me.gb_custom.Text = "xCustom"
    '
    'ef_custom
    '
    Me.ef_custom.DoubleValue = 0.0R
    Me.ef_custom.IntegerValue = 0
    Me.ef_custom.IsReadOnly = True
    Me.ef_custom.Location = New System.Drawing.Point(6, 23)
    Me.ef_custom.Name = "ef_custom"
    Me.ef_custom.PlaceHolder = Nothing
    Me.ef_custom.Size = New System.Drawing.Size(450, 24)
    Me.ef_custom.SufixText = "Sufix Text"
    Me.ef_custom.SufixTextVisible = True
    Me.ef_custom.TabIndex = 0
    Me.ef_custom.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_custom.TextValue = ""
    Me.ef_custom.TextWidth = 100
    Me.ef_custom.Value = ""
    Me.ef_custom.ValueForeColor = System.Drawing.Color.Blue
    '
    'cmb_custom
    '
    Me.cmb_custom.AllowUnlistedValues = False
    Me.cmb_custom.AutoCompleteMode = False
    Me.cmb_custom.IsReadOnly = False
    Me.cmb_custom.Location = New System.Drawing.Point(262, 24)
    Me.cmb_custom.Name = "cmb_custom"
    Me.cmb_custom.SelectedIndex = -1
    Me.cmb_custom.Size = New System.Drawing.Size(450, 24)
    Me.cmb_custom.SufixText = "Sufix Text"
    Me.cmb_custom.SufixTextVisible = True
    Me.cmb_custom.TabIndex = 1
    Me.cmb_custom.TextCombo = Nothing
    Me.cmb_custom.TextWidth = 100
    '
    'btn_load_gaming_values
    '
    Me.btn_load_gaming_values.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_load_gaming_values.Location = New System.Drawing.Point(784, 20)
    Me.btn_load_gaming_values.Name = "btn_load_gaming_values"
    Me.btn_load_gaming_values.Size = New System.Drawing.Size(90, 30)
    Me.btn_load_gaming_values.TabIndex = 0
    Me.btn_load_gaming_values.ToolTipped = False
    Me.btn_load_gaming_values.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'gb_operation
    '
    Me.gb_operation.Controls.Add(Me.ef_date_cage)
    Me.gb_operation.Controls.Add(Me.ef_status_movement)
    Me.gb_operation.Controls.Add(Me.ef_cage_user)
    Me.gb_operation.Controls.Add(Me.ef_operation)
    Me.gb_operation.Location = New System.Drawing.Point(0, 3)
    Me.gb_operation.Name = "gb_operation"
    Me.gb_operation.Size = New System.Drawing.Size(956, 87)
    Me.gb_operation.TabIndex = 0
    Me.gb_operation.TabStop = False
    Me.gb_operation.Text = "xOperation"
    '
    'ef_date_cage
    '
    Me.ef_date_cage.DoubleValue = 0.0R
    Me.ef_date_cage.IntegerValue = 0
    Me.ef_date_cage.IsReadOnly = True
    Me.ef_date_cage.Location = New System.Drawing.Point(486, 51)
    Me.ef_date_cage.Name = "ef_date_cage"
    Me.ef_date_cage.PlaceHolder = Nothing
    Me.ef_date_cage.Size = New System.Drawing.Size(450, 24)
    Me.ef_date_cage.SufixText = "Sufix Text"
    Me.ef_date_cage.SufixTextVisible = True
    Me.ef_date_cage.TabIndex = 3
    Me.ef_date_cage.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_date_cage.TextValue = ""
    Me.ef_date_cage.TextWidth = 100
    Me.ef_date_cage.Value = ""
    Me.ef_date_cage.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_status_movement
    '
    Me.ef_status_movement.DoubleValue = 0.0R
    Me.ef_status_movement.IntegerValue = 0
    Me.ef_status_movement.IsReadOnly = True
    Me.ef_status_movement.Location = New System.Drawing.Point(486, 20)
    Me.ef_status_movement.Name = "ef_status_movement"
    Me.ef_status_movement.PlaceHolder = Nothing
    Me.ef_status_movement.Size = New System.Drawing.Size(450, 24)
    Me.ef_status_movement.SufixText = "Sufix Text"
    Me.ef_status_movement.SufixTextVisible = True
    Me.ef_status_movement.TabIndex = 2
    Me.ef_status_movement.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_status_movement.TextValue = ""
    Me.ef_status_movement.TextWidth = 100
    Me.ef_status_movement.Value = ""
    Me.ef_status_movement.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_cage_user
    '
    Me.ef_cage_user.DoubleValue = 0.0R
    Me.ef_cage_user.IntegerValue = 0
    Me.ef_cage_user.IsReadOnly = True
    Me.ef_cage_user.Location = New System.Drawing.Point(6, 51)
    Me.ef_cage_user.Name = "ef_cage_user"
    Me.ef_cage_user.PlaceHolder = Nothing
    Me.ef_cage_user.Size = New System.Drawing.Size(450, 24)
    Me.ef_cage_user.SufixText = "Sufix Text"
    Me.ef_cage_user.SufixTextVisible = True
    Me.ef_cage_user.TabIndex = 1
    Me.ef_cage_user.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_cage_user.TextValue = ""
    Me.ef_cage_user.TextWidth = 100
    Me.ef_cage_user.Value = ""
    Me.ef_cage_user.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_operation
    '
    Me.ef_operation.DoubleValue = 0.0R
    Me.ef_operation.IntegerValue = 0
    Me.ef_operation.IsReadOnly = True
    Me.ef_operation.Location = New System.Drawing.Point(6, 20)
    Me.ef_operation.Name = "ef_operation"
    Me.ef_operation.PlaceHolder = Nothing
    Me.ef_operation.Size = New System.Drawing.Size(450, 24)
    Me.ef_operation.SufixText = "Sufix Text"
    Me.ef_operation.SufixTextVisible = True
    Me.ef_operation.TabIndex = 0
    Me.ef_operation.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_operation.TextValue = ""
    Me.ef_operation.TextWidth = 100
    Me.ef_operation.Value = ""
    Me.ef_operation.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_total
    '
    Me.gb_total.Controls.Add(Me.dg_total)
    Me.gb_total.Location = New System.Drawing.Point(0, 93)
    Me.gb_total.Name = "gb_total"
    Me.gb_total.Size = New System.Drawing.Size(956, 153)
    Me.gb_total.TabIndex = 1
    Me.gb_total.TabStop = False
    Me.gb_total.Text = "xTotal"
    '
    'dg_total
    '
    Me.dg_total.CurrentCol = -1
    Me.dg_total.CurrentRow = -1
    Me.dg_total.Dock = System.Windows.Forms.DockStyle.Fill
    Me.dg_total.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_total.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_total.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_total.Location = New System.Drawing.Point(3, 17)
    Me.dg_total.Name = "dg_total"
    Me.dg_total.PanelRightVisible = False
    Me.dg_total.Redraw = True
    Me.dg_total.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    Me.dg_total.Size = New System.Drawing.Size(950, 133)
    Me.dg_total.Sortable = False
    Me.dg_total.SortAscending = True
    Me.dg_total.SortByCol = 0
    Me.dg_total.TabIndex = 0
    Me.dg_total.ToolTipped = True
    Me.dg_total.TopRow = -2
    Me.dg_total.WordWrap = False
    '
    'gb_movement_type
    '
    Me.gb_movement_type.Controls.Add(Me.rb_reception)
    Me.gb_movement_type.Controls.Add(Me.rb_send)
    Me.gb_movement_type.Location = New System.Drawing.Point(3, 52)
    Me.gb_movement_type.Name = "gb_movement_type"
    Me.gb_movement_type.Size = New System.Drawing.Size(475, 47)
    Me.gb_movement_type.TabIndex = 2
    Me.gb_movement_type.TabStop = False
    Me.gb_movement_type.Text = "xMovementType"
    '
    'rb_reception
    '
    Me.rb_reception.AutoSize = True
    Me.rb_reception.Location = New System.Drawing.Point(278, 18)
    Me.rb_reception.Name = "rb_reception"
    Me.rb_reception.Size = New System.Drawing.Size(88, 17)
    Me.rb_reception.TabIndex = 1
    Me.rb_reception.TabStop = True
    Me.rb_reception.Text = "xReception"
    Me.rb_reception.UseVisualStyleBackColor = True
    '
    'rb_send
    '
    Me.rb_send.AutoSize = True
    Me.rb_send.Location = New System.Drawing.Point(137, 18)
    Me.rb_send.Name = "rb_send"
    Me.rb_send.Size = New System.Drawing.Size(58, 17)
    Me.rb_send.TabIndex = 0
    Me.rb_send.TabStop = True
    Me.rb_send.Text = "xSent"
    Me.rb_send.UseVisualStyleBackColor = True
    '
    'gb_source_target
    '
    Me.gb_source_target.Controls.Add(Me.rb_terminal)
    Me.gb_source_target.Controls.Add(Me.rb_gaming_table)
    Me.gb_source_target.Controls.Add(Me.rb_cashier)
    Me.gb_source_target.Controls.Add(Me.rb_custom)
    Me.gb_source_target.Location = New System.Drawing.Point(484, 52)
    Me.gb_source_target.Name = "gb_source_target"
    Me.gb_source_target.Size = New System.Drawing.Size(475, 47)
    Me.gb_source_target.TabIndex = 3
    Me.gb_source_target.TabStop = False
    Me.gb_source_target.Text = "xSoruceTarget"
    '
    'rb_terminal
    '
    Me.rb_terminal.AutoSize = True
    Me.rb_terminal.Location = New System.Drawing.Point(367, 18)
    Me.rb_terminal.Name = "rb_terminal"
    Me.rb_terminal.Size = New System.Drawing.Size(81, 17)
    Me.rb_terminal.TabIndex = 3
    Me.rb_terminal.Text = "xTerminal"
    Me.rb_terminal.UseVisualStyleBackColor = True
    '
    'rb_gaming_table
    '
    Me.rb_gaming_table.AutoSize = True
    Me.rb_gaming_table.Location = New System.Drawing.Point(113, 18)
    Me.rb_gaming_table.Name = "rb_gaming_table"
    Me.rb_gaming_table.Size = New System.Drawing.Size(116, 17)
    Me.rb_gaming_table.TabIndex = 1
    Me.rb_gaming_table.Text = "xGamblingTable"
    Me.rb_gaming_table.UseVisualStyleBackColor = True
    '
    'rb_cashier
    '
    Me.rb_cashier.AutoSize = True
    Me.rb_cashier.Location = New System.Drawing.Point(12, 18)
    Me.rb_cashier.Name = "rb_cashier"
    Me.rb_cashier.Size = New System.Drawing.Size(76, 17)
    Me.rb_cashier.TabIndex = 0
    Me.rb_cashier.Text = "xCashier"
    Me.rb_cashier.UseVisualStyleBackColor = True
    '
    'rb_custom
    '
    Me.rb_custom.AutoSize = True
    Me.rb_custom.Checked = True
    Me.rb_custom.Location = New System.Drawing.Point(256, 18)
    Me.rb_custom.Name = "rb_custom"
    Me.rb_custom.Size = New System.Drawing.Size(76, 17)
    Me.rb_custom.TabIndex = 2
    Me.rb_custom.TabStop = True
    Me.rb_custom.Text = "xCustom"
    Me.rb_custom.UseVisualStyleBackColor = True
    '
    'gb_gaming_table
    '
    Me.gb_gaming_table.Controls.Add(Me.lb_gt_without_integrated_cashier)
    Me.gb_gaming_table.Controls.Add(Me.ef_gaming_table)
    Me.gb_gaming_table.Controls.Add(Me.btn_load_gaming_values)
    Me.gb_gaming_table.Controls.Add(Me.cmb_gaming_table)
    Me.gb_gaming_table.Controls.Add(Me.panel_gaming_table_type)
    Me.gb_gaming_table.Location = New System.Drawing.Point(3, 211)
    Me.gb_gaming_table.Name = "gb_gaming_table"
    Me.gb_gaming_table.Size = New System.Drawing.Size(956, 90)
    Me.gb_gaming_table.TabIndex = 6
    Me.gb_gaming_table.TabStop = False
    Me.gb_gaming_table.Text = "xGamingTable"
    '
    'lb_gt_without_integrated_cashier
    '
    Me.lb_gt_without_integrated_cashier.AutoSize = True
    Me.lb_gt_without_integrated_cashier.Location = New System.Drawing.Point(564, 65)
    Me.lb_gt_without_integrated_cashier.Name = "lb_gt_without_integrated_cashier"
    Me.lb_gt_without_integrated_cashier.Size = New System.Drawing.Size(174, 13)
    Me.lb_gt_without_integrated_cashier.TabIndex = 1
    Me.lb_gt_without_integrated_cashier.Text = "xGtWithoutIntegratedCashier"
    Me.lb_gt_without_integrated_cashier.Visible = False
    '
    'ef_gaming_table
    '
    Me.ef_gaming_table.DoubleValue = 0.0R
    Me.ef_gaming_table.IntegerValue = 0
    Me.ef_gaming_table.IsReadOnly = True
    Me.ef_gaming_table.Location = New System.Drawing.Point(6, 53)
    Me.ef_gaming_table.Name = "ef_gaming_table"
    Me.ef_gaming_table.PlaceHolder = Nothing
    Me.ef_gaming_table.Size = New System.Drawing.Size(460, 24)
    Me.ef_gaming_table.SufixText = "Sufix Text"
    Me.ef_gaming_table.SufixTextVisible = True
    Me.ef_gaming_table.TabIndex = 1
    Me.ef_gaming_table.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_gaming_table.TextValue = ""
    Me.ef_gaming_table.TextWidth = 100
    Me.ef_gaming_table.Value = ""
    Me.ef_gaming_table.ValueForeColor = System.Drawing.Color.Blue
    '
    'panel_aux
    '
    Me.panel_aux.Controls.Add(Me.tb_concepts)
    Me.panel_aux.Controls.Add(Me.dg_concepts)
    Me.panel_aux.Controls.Add(Me.tb_currency)
    Me.panel_aux.Controls.Add(Me.gb_operation)
    Me.panel_aux.Controls.Add(Me.gb_total)
    Me.panel_aux.Location = New System.Drawing.Point(3, 367)
    Me.panel_aux.Name = "panel_aux"
    Me.panel_aux.Size = New System.Drawing.Size(965, 554)
    Me.panel_aux.TabIndex = 8
    '
    'tb_concepts
    '
    Me.tb_concepts.Alignment = System.Windows.Forms.TabAlignment.Right
    Me.tb_concepts.Controls.Add(Me.TabPage1)
    Me.tb_concepts.Location = New System.Drawing.Point(937, 281)
    Me.tb_concepts.Multiline = True
    Me.tb_concepts.Name = "tb_concepts"
    Me.tb_concepts.SelectedIndex = 0
    Me.tb_concepts.Size = New System.Drawing.Size(20, 78)
    Me.tb_concepts.TabIndex = 5
    '
    'TabPage1
    '
    Me.TabPage1.Location = New System.Drawing.Point(4, 4)
    Me.TabPage1.Name = "TabPage1"
    Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage1.Size = New System.Drawing.Size(0, 70)
    Me.TabPage1.TabIndex = 0
    Me.TabPage1.Text = "xConcepts"
    Me.TabPage1.UseVisualStyleBackColor = True
    '
    'dg_concepts
    '
    Me.dg_concepts.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.dg_concepts.CurrentCol = -1
    Me.dg_concepts.CurrentRow = -1
    Me.dg_concepts.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_concepts.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_concepts.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_concepts.Location = New System.Drawing.Point(0, 281)
    Me.dg_concepts.Name = "dg_concepts"
    Me.dg_concepts.PanelRightVisible = False
    Me.dg_concepts.Redraw = True
    Me.dg_concepts.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    Me.dg_concepts.Size = New System.Drawing.Size(933, 266)
    Me.dg_concepts.Sortable = False
    Me.dg_concepts.SortAscending = True
    Me.dg_concepts.SortByCol = 0
    Me.dg_concepts.TabIndex = 4
    Me.dg_concepts.ToolTipped = True
    Me.dg_concepts.TopRow = -2
    Me.dg_concepts.Visible = False
    Me.dg_concepts.WordWrap = False
    '
    'tb_currency
    '
    Me.tb_currency.Location = New System.Drawing.Point(0, 256)
    Me.tb_currency.Name = "tb_currency"
    Me.tb_currency.SelectedIndex = 0
    Me.tb_currency.Size = New System.Drawing.Size(936, 292)
    Me.tb_currency.TabIndex = 2
    '
    'lbl_error1
    '
    Me.lbl_error1.BackColor = System.Drawing.SystemColors.GrayText
    Me.lbl_error1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_error1.Location = New System.Drawing.Point(10, 44)
    Me.lbl_error1.Name = "lbl_error1"
    Me.lbl_error1.Size = New System.Drawing.Size(16, 16)
    Me.lbl_error1.TabIndex = 110
    '
    'lbl_ok1
    '
    Me.lbl_ok1.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_ok1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_ok1.Location = New System.Drawing.Point(10, 19)
    Me.lbl_ok1.Name = "lbl_ok1"
    Me.lbl_ok1.Size = New System.Drawing.Size(16, 16)
    Me.lbl_ok1.TabIndex = 106
    '
    'gb_collection_params
    '
    Me.gb_collection_params.Controls.Add(Me.ef_floor_id)
    Me.gb_collection_params.Controls.Add(Me.ef_stacker_id)
    Me.gb_collection_params.Controls.Add(Me.lbl_notes)
    Me.gb_collection_params.Controls.Add(Me.tb_notes)
    Me.gb_collection_params.Controls.Add(Me.ef_extraction_date)
    Me.gb_collection_params.Controls.Add(Me.ef_insertion_date)
    Me.gb_collection_params.Controls.Add(Me.ef_employee)
    Me.gb_collection_params.Controls.Add(Me.ef_terminal)
    Me.gb_collection_params.Location = New System.Drawing.Point(1151, 70)
    Me.gb_collection_params.Name = "gb_collection_params"
    Me.gb_collection_params.Size = New System.Drawing.Size(733, 160)
    Me.gb_collection_params.TabIndex = 4
    Me.gb_collection_params.TabStop = False
    Me.gb_collection_params.Text = "xParametros"
    '
    'ef_floor_id
    '
    Me.ef_floor_id.DoubleValue = 0.0R
    Me.ef_floor_id.IntegerValue = 0
    Me.ef_floor_id.IsReadOnly = False
    Me.ef_floor_id.Location = New System.Drawing.Point(499, 20)
    Me.ef_floor_id.Name = "ef_floor_id"
    Me.ef_floor_id.PlaceHolder = Nothing
    Me.ef_floor_id.Size = New System.Drawing.Size(220, 24)
    Me.ef_floor_id.SufixText = "Sufix Text"
    Me.ef_floor_id.SufixTextVisible = True
    Me.ef_floor_id.TabIndex = 1
    Me.ef_floor_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_floor_id.TextValue = ""
    Me.ef_floor_id.TextWidth = 100
    Me.ef_floor_id.Value = ""
    Me.ef_floor_id.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_stacker_id
    '
    Me.ef_stacker_id.DoubleValue = 0.0R
    Me.ef_stacker_id.IntegerValue = 0
    Me.ef_stacker_id.IsReadOnly = False
    Me.ef_stacker_id.Location = New System.Drawing.Point(7, 20)
    Me.ef_stacker_id.Name = "ef_stacker_id"
    Me.ef_stacker_id.PlaceHolder = Nothing
    Me.ef_stacker_id.Size = New System.Drawing.Size(220, 24)
    Me.ef_stacker_id.SufixText = "Sufix Text"
    Me.ef_stacker_id.SufixTextVisible = True
    Me.ef_stacker_id.TabIndex = 0
    Me.ef_stacker_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_stacker_id.TextValue = ""
    Me.ef_stacker_id.TextWidth = 100
    Me.ef_stacker_id.Value = ""
    Me.ef_stacker_id.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_notes
    '
    Me.lbl_notes.AutoSize = True
    Me.lbl_notes.Location = New System.Drawing.Point(58, 118)
    Me.lbl_notes.Name = "lbl_notes"
    Me.lbl_notes.Size = New System.Drawing.Size(46, 13)
    Me.lbl_notes.TabIndex = 3
    Me.lbl_notes.Text = "xNotes"
    '
    'tb_notes
    '
    Me.tb_notes.Location = New System.Drawing.Point(110, 115)
    Me.tb_notes.MaxLength = 199
    Me.tb_notes.Multiline = True
    Me.tb_notes.Name = "tb_notes"
    Me.tb_notes.Size = New System.Drawing.Size(609, 36)
    Me.tb_notes.TabIndex = 6
    '
    'ef_extraction_date
    '
    Me.ef_extraction_date.DoubleValue = 0.0R
    Me.ef_extraction_date.IntegerValue = 0
    Me.ef_extraction_date.IsReadOnly = False
    Me.ef_extraction_date.Location = New System.Drawing.Point(499, 81)
    Me.ef_extraction_date.Name = "ef_extraction_date"
    Me.ef_extraction_date.PlaceHolder = Nothing
    Me.ef_extraction_date.Size = New System.Drawing.Size(220, 24)
    Me.ef_extraction_date.SufixText = "Sufix Text"
    Me.ef_extraction_date.SufixTextVisible = True
    Me.ef_extraction_date.TabIndex = 5
    Me.ef_extraction_date.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_extraction_date.TextValue = ""
    Me.ef_extraction_date.TextWidth = 100
    Me.ef_extraction_date.Value = ""
    Me.ef_extraction_date.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_insertion_date
    '
    Me.ef_insertion_date.DoubleValue = 0.0R
    Me.ef_insertion_date.IntegerValue = 0
    Me.ef_insertion_date.IsReadOnly = False
    Me.ef_insertion_date.Location = New System.Drawing.Point(499, 50)
    Me.ef_insertion_date.Name = "ef_insertion_date"
    Me.ef_insertion_date.PlaceHolder = Nothing
    Me.ef_insertion_date.Size = New System.Drawing.Size(220, 24)
    Me.ef_insertion_date.SufixText = "Sufix Text"
    Me.ef_insertion_date.SufixTextVisible = True
    Me.ef_insertion_date.TabIndex = 3
    Me.ef_insertion_date.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_insertion_date.TextValue = ""
    Me.ef_insertion_date.TextWidth = 100
    Me.ef_insertion_date.Value = ""
    Me.ef_insertion_date.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_employee
    '
    Me.ef_employee.DoubleValue = 0.0R
    Me.ef_employee.IntegerValue = 0
    Me.ef_employee.IsReadOnly = False
    Me.ef_employee.Location = New System.Drawing.Point(7, 80)
    Me.ef_employee.Name = "ef_employee"
    Me.ef_employee.PlaceHolder = Nothing
    Me.ef_employee.Size = New System.Drawing.Size(450, 24)
    Me.ef_employee.SufixText = "Sufix Text"
    Me.ef_employee.SufixTextVisible = True
    Me.ef_employee.TabIndex = 4
    Me.ef_employee.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_employee.TextValue = ""
    Me.ef_employee.TextWidth = 100
    Me.ef_employee.Value = ""
    Me.ef_employee.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_terminal
    '
    Me.ef_terminal.DoubleValue = 0.0R
    Me.ef_terminal.IntegerValue = 0
    Me.ef_terminal.IsReadOnly = False
    Me.ef_terminal.Location = New System.Drawing.Point(7, 50)
    Me.ef_terminal.Name = "ef_terminal"
    Me.ef_terminal.PlaceHolder = Nothing
    Me.ef_terminal.Size = New System.Drawing.Size(450, 24)
    Me.ef_terminal.SufixText = "Sufix Text"
    Me.ef_terminal.SufixTextVisible = True
    Me.ef_terminal.TabIndex = 2
    Me.ef_terminal.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_terminal.TextValue = ""
    Me.ef_terminal.TextWidth = 100
    Me.ef_terminal.Value = ""
    Me.ef_terminal.ValueForeColor = System.Drawing.Color.Blue
    '
    'lb_note_counter_status
    '
    Me.lb_note_counter_status.BackColor = System.Drawing.Color.Red
    Me.lb_note_counter_status.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lb_note_counter_status.Location = New System.Drawing.Point(10, 21)
    Me.lb_note_counter_status.Name = "lb_note_counter_status"
    Me.lb_note_counter_status.Size = New System.Drawing.Size(16, 16)
    Me.lb_note_counter_status.TabIndex = 111
    '
    'btn_open_events
    '
    Me.btn_open_events.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_open_events.Location = New System.Drawing.Point(470, 13)
    Me.btn_open_events.Name = "btn_open_events"
    Me.btn_open_events.Size = New System.Drawing.Size(90, 30)
    Me.btn_open_events.TabIndex = 17
    Me.btn_open_events.ToolTipped = False
    Me.btn_open_events.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'lbl_counter
    '
    Me.lbl_counter.AutoSize = True
    Me.lbl_counter.Location = New System.Drawing.Point(38, 22)
    Me.lbl_counter.Name = "lbl_counter"
    Me.lbl_counter.Size = New System.Drawing.Size(59, 13)
    Me.lbl_counter.TabIndex = 15
    Me.lbl_counter.Text = "Available"
    Me.lbl_counter.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'lb_error_messages
    '
    Me.lb_error_messages.ForeColor = System.Drawing.Color.Red
    Me.lb_error_messages.Location = New System.Drawing.Point(6, 13)
    Me.lb_error_messages.Name = "lb_error_messages"
    Me.lb_error_messages.Size = New System.Drawing.Size(457, 30)
    Me.lb_error_messages.TabIndex = 14
    Me.lb_error_messages.Text = "xError"
    '
    'tab_ticket
    '
    Me.tab_ticket.Controls.Add(Me.ef_num_of_tickets)
    Me.tab_ticket.Controls.Add(Me.gb_status1)
    Me.tab_ticket.Controls.Add(Me.dg_collection_tickets)
    Me.tab_ticket.Controls.Add(Me.ef_ticket)
    Me.tab_ticket.Controls.Add(Me.btn_save_tickets)
    Me.tab_ticket.Location = New System.Drawing.Point(4, 22)
    Me.tab_ticket.Name = "tab_ticket"
    Me.tab_ticket.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_ticket.Size = New System.Drawing.Size(948, 286)
    Me.tab_ticket.TabIndex = 1
    Me.tab_ticket.Text = "TabPage2"
    Me.tab_ticket.UseVisualStyleBackColor = True
    '
    'ef_num_of_tickets
    '
    Me.ef_num_of_tickets.DoubleValue = 0.0R
    Me.ef_num_of_tickets.IntegerValue = 0
    Me.ef_num_of_tickets.IsReadOnly = False
    Me.ef_num_of_tickets.Location = New System.Drawing.Point(795, 116)
    Me.ef_num_of_tickets.Name = "ef_num_of_tickets"
    Me.ef_num_of_tickets.PlaceHolder = Nothing
    Me.ef_num_of_tickets.Size = New System.Drawing.Size(122, 24)
    Me.ef_num_of_tickets.SufixText = "Sufix Text"
    Me.ef_num_of_tickets.SufixTextVisible = True
    Me.ef_num_of_tickets.TabIndex = 114
    Me.ef_num_of_tickets.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    Me.ef_num_of_tickets.TextValue = ""
    Me.ef_num_of_tickets.TextWidth = 76
    Me.ef_num_of_tickets.Value = ""
    Me.ef_num_of_tickets.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_status1
    '
    Me.gb_status1.Controls.Add(Me.lbl_pending)
    Me.gb_status1.Controls.Add(Me.tf_pending)
    Me.gb_status1.Controls.Add(Me.lbl_error)
    Me.gb_status1.Controls.Add(Me.tf_error1)
    Me.gb_status1.Controls.Add(Me.Label5)
    Me.gb_status1.Controls.Add(Me.tf_ok1)
    Me.gb_status1.Location = New System.Drawing.Point(795, 6)
    Me.gb_status1.Name = "gb_status1"
    Me.gb_status1.Size = New System.Drawing.Size(123, 105)
    Me.gb_status1.TabIndex = 111
    Me.gb_status1.TabStop = False
    Me.gb_status1.Text = "xStatus"
    '
    'lbl_pending
    '
    Me.lbl_pending.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(94, Byte), Integer))
    Me.lbl_pending.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_pending.Location = New System.Drawing.Point(10, 69)
    Me.lbl_pending.Name = "lbl_pending"
    Me.lbl_pending.Size = New System.Drawing.Size(16, 16)
    Me.lbl_pending.TabIndex = 112
    '
    'tf_pending
    '
    Me.tf_pending.IsReadOnly = True
    Me.tf_pending.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_pending.LabelForeColor = System.Drawing.Color.Black
    Me.tf_pending.Location = New System.Drawing.Point(5, 65)
    Me.tf_pending.Name = "tf_pending"
    Me.tf_pending.Size = New System.Drawing.Size(116, 24)
    Me.tf_pending.SufixText = "Sufix Text"
    Me.tf_pending.SufixTextVisible = True
    Me.tf_pending.TabIndex = 111
    Me.tf_pending.TabStop = False
    Me.tf_pending.TextVisible = False
    Me.tf_pending.TextWidth = 30
    Me.tf_pending.Value = "xPending"
    '
    'lbl_error
    '
    Me.lbl_error.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
    Me.lbl_error.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_error.Location = New System.Drawing.Point(10, 44)
    Me.lbl_error.Name = "lbl_error"
    Me.lbl_error.Size = New System.Drawing.Size(16, 16)
    Me.lbl_error.TabIndex = 110
    '
    'tf_error1
    '
    Me.tf_error1.IsReadOnly = True
    Me.tf_error1.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_error1.LabelForeColor = System.Drawing.Color.Black
    Me.tf_error1.Location = New System.Drawing.Point(4, 40)
    Me.tf_error1.Name = "tf_error1"
    Me.tf_error1.Size = New System.Drawing.Size(116, 24)
    Me.tf_error1.SufixText = "Sufix Text"
    Me.tf_error1.SufixTextVisible = True
    Me.tf_error1.TabIndex = 109
    Me.tf_error1.TabStop = False
    Me.tf_error1.TextVisible = False
    Me.tf_error1.TextWidth = 30
    Me.tf_error1.Value = "xInactive"
    '
    'Label5
    '
    Me.Label5.BackColor = System.Drawing.SystemColors.Window
    Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Label5.Location = New System.Drawing.Point(10, 19)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(16, 16)
    Me.Label5.TabIndex = 106
    '
    'tf_ok1
    '
    Me.tf_ok1.IsReadOnly = True
    Me.tf_ok1.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_ok1.LabelForeColor = System.Drawing.Color.Black
    Me.tf_ok1.Location = New System.Drawing.Point(4, 15)
    Me.tf_ok1.Name = "tf_ok1"
    Me.tf_ok1.Size = New System.Drawing.Size(114, 24)
    Me.tf_ok1.SufixText = "Sufix Text"
    Me.tf_ok1.SufixTextVisible = True
    Me.tf_ok1.TabIndex = 104
    Me.tf_ok1.TabStop = False
    Me.tf_ok1.TextVisible = False
    Me.tf_ok1.TextWidth = 30
    Me.tf_ok1.Value = "xRunning"
    '
    'dg_collection_tickets
    '
    Me.dg_collection_tickets.CurrentCol = -1
    Me.dg_collection_tickets.CurrentRow = -1
    Me.dg_collection_tickets.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_collection_tickets.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_collection_tickets.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_collection_tickets.Location = New System.Drawing.Point(10, 42)
    Me.dg_collection_tickets.Name = "dg_collection_tickets"
    Me.dg_collection_tickets.PanelRightVisible = False
    Me.dg_collection_tickets.Redraw = True
    Me.dg_collection_tickets.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_collection_tickets.Size = New System.Drawing.Size(779, 238)
    Me.dg_collection_tickets.Sortable = False
    Me.dg_collection_tickets.SortAscending = True
    Me.dg_collection_tickets.SortByCol = 0
    Me.dg_collection_tickets.TabIndex = 1
    Me.dg_collection_tickets.ToolTipped = True
    Me.dg_collection_tickets.TopRow = -2
    Me.dg_collection_tickets.WordWrap = False
    '
    'ef_ticket
    '
    Me.ef_ticket.DoubleValue = 0.0R
    Me.ef_ticket.IntegerValue = 0
    Me.ef_ticket.IsReadOnly = False
    Me.ef_ticket.Location = New System.Drawing.Point(6, 6)
    Me.ef_ticket.Name = "ef_ticket"
    Me.ef_ticket.PlaceHolder = Nothing
    Me.ef_ticket.Size = New System.Drawing.Size(572, 24)
    Me.ef_ticket.SufixText = "Sufix Text"
    Me.ef_ticket.SufixTextVisible = True
    Me.ef_ticket.TabIndex = 0
    Me.ef_ticket.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_ticket.TextValue = ""
    Me.ef_ticket.TextVisible = False
    Me.ef_ticket.TextWidth = 200
    Me.ef_ticket.Value = ""
    Me.ef_ticket.ValueForeColor = System.Drawing.Color.Blue
    '
    'btn_save_tickets
    '
    Me.btn_save_tickets.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_save_tickets.Location = New System.Drawing.Point(699, 6)
    Me.btn_save_tickets.Name = "btn_save_tickets"
    Me.btn_save_tickets.Size = New System.Drawing.Size(90, 30)
    Me.btn_save_tickets.TabIndex = 1
    Me.btn_save_tickets.ToolTipped = False
    Me.btn_save_tickets.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'tb_collections
    '
    Me.tb_collections.Controls.Add(Me.tab_ticket)
    Me.tb_collections.Location = New System.Drawing.Point(1151, 448)
    Me.tb_collections.Name = "tb_collections"
    Me.tb_collections.SelectedIndex = 0
    Me.tb_collections.Size = New System.Drawing.Size(956, 312)
    Me.tb_collections.TabIndex = 9
    '
    'cmb_cage_sessions
    '
    Me.cmb_cage_sessions.AllowUnlistedValues = False
    Me.cmb_cage_sessions.AutoCompleteMode = False
    Me.cmb_cage_sessions.IsReadOnly = False
    Me.cmb_cage_sessions.Location = New System.Drawing.Point(6, 16)
    Me.cmb_cage_sessions.Name = "cmb_cage_sessions"
    Me.cmb_cage_sessions.SelectedIndex = -1
    Me.cmb_cage_sessions.Size = New System.Drawing.Size(450, 24)
    Me.cmb_cage_sessions.SufixText = "Sufix Text"
    Me.cmb_cage_sessions.SufixTextVisible = True
    Me.cmb_cage_sessions.TabIndex = 0
    Me.cmb_cage_sessions.TextCombo = Nothing
    Me.cmb_cage_sessions.TextWidth = 100
    '
    'gb_cage_sessions
    '
    Me.gb_cage_sessions.Controls.Add(Me.cmb_cage_sessions)
    Me.gb_cage_sessions.Controls.Add(Me.ef_cage_session)
    Me.gb_cage_sessions.Location = New System.Drawing.Point(3, 3)
    Me.gb_cage_sessions.Name = "gb_cage_sessions"
    Me.gb_cage_sessions.Size = New System.Drawing.Size(475, 50)
    Me.gb_cage_sessions.TabIndex = 1
    Me.gb_cage_sessions.TabStop = False
    Me.gb_cage_sessions.Text = "xCageSessions"
    '
    'ef_cage_session
    '
    Me.ef_cage_session.DoubleValue = 0.0R
    Me.ef_cage_session.IntegerValue = 0
    Me.ef_cage_session.IsReadOnly = True
    Me.ef_cage_session.Location = New System.Drawing.Point(13, 14)
    Me.ef_cage_session.Name = "ef_cage_session"
    Me.ef_cage_session.PlaceHolder = Nothing
    Me.ef_cage_session.Size = New System.Drawing.Size(450, 24)
    Me.ef_cage_session.SufixText = "Sufix Text"
    Me.ef_cage_session.SufixTextVisible = True
    Me.ef_cage_session.TabIndex = 1
    Me.ef_cage_session.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_cage_session.TextValue = ""
    Me.ef_cage_session.TextWidth = 100
    Me.ef_cage_session.Value = ""
    Me.ef_cage_session.ValueForeColor = System.Drawing.Color.Blue
    '
    'cmb_cage_template
    '
    Me.cmb_cage_template.AllowUnlistedValues = False
    Me.cmb_cage_template.AutoCompleteMode = False
    Me.cmb_cage_template.IsReadOnly = False
    Me.cmb_cage_template.Location = New System.Drawing.Point(5, 16)
    Me.cmb_cage_template.Name = "cmb_cage_template"
    Me.cmb_cage_template.SelectedIndex = -1
    Me.cmb_cage_template.Size = New System.Drawing.Size(450, 24)
    Me.cmb_cage_template.SufixText = "Sufix Text"
    Me.cmb_cage_template.SufixTextVisible = True
    Me.cmb_cage_template.TabIndex = 2
    Me.cmb_cage_template.TextCombo = Nothing
    Me.cmb_cage_template.TextWidth = 100
    '
    'gb_template
    '
    Me.gb_template.Controls.Add(Me.cmb_cage_template)
    Me.gb_template.Location = New System.Drawing.Point(484, 3)
    Me.gb_template.Name = "gb_template"
    Me.gb_template.Size = New System.Drawing.Size(475, 50)
    Me.gb_template.TabIndex = 10
    Me.gb_template.TabStop = False
    Me.gb_template.Text = "xCageTemplate"
    '
    'tmr_note_counter_working
    '
    Me.tmr_note_counter_working.Interval = 500
    '
    'gb_note_counter_status
    '
    Me.gb_note_counter_status.Controls.Add(Me.lb_note_counter_status)
    Me.gb_note_counter_status.Controls.Add(Me.lbl_counter)
    Me.gb_note_counter_status.Location = New System.Drawing.Point(1151, 288)
    Me.gb_note_counter_status.Name = "gb_note_counter_status"
    Me.gb_note_counter_status.Size = New System.Drawing.Size(161, 50)
    Me.gb_note_counter_status.TabIndex = 11
    Me.gb_note_counter_status.TabStop = False
    Me.gb_note_counter_status.Text = "xNoteCounterStatus"
    '
    'gb_note_counter_events
    '
    Me.gb_note_counter_events.Controls.Add(Me.btn_open_events)
    Me.gb_note_counter_events.Controls.Add(Me.lb_error_messages)
    Me.gb_note_counter_events.Location = New System.Drawing.Point(1270, 288)
    Me.gb_note_counter_events.Name = "gb_note_counter_events"
    Me.gb_note_counter_events.Size = New System.Drawing.Size(566, 50)
    Me.gb_note_counter_events.TabIndex = 12
    Me.gb_note_counter_events.TabStop = False
    Me.gb_note_counter_events.Text = "xNoteCounterEvents"
    '
    'frm_cage_control
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1084, 921)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_cage_control"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_CAGE_control"
    Me.panel_data.ResumeLayout(False)
    Me.gb_cashier.ResumeLayout(False)
    Me.panel_cashier_type.ResumeLayout(False)
    Me.panel_cashier_type.PerformLayout()
    Me.panel_gaming_table_type.ResumeLayout(False)
    Me.panel_gaming_table_type.PerformLayout()
    Me.gb_custom.ResumeLayout(False)
    Me.gb_operation.ResumeLayout(False)
    Me.gb_total.ResumeLayout(False)
    Me.gb_movement_type.ResumeLayout(False)
    Me.gb_movement_type.PerformLayout()
    Me.gb_source_target.ResumeLayout(False)
    Me.gb_source_target.PerformLayout()
    Me.gb_gaming_table.ResumeLayout(False)
    Me.gb_gaming_table.PerformLayout()
    Me.panel_aux.ResumeLayout(False)
    Me.tb_concepts.ResumeLayout(False)
    Me.gb_collection_params.ResumeLayout(False)
    Me.gb_collection_params.PerformLayout()
    Me.tab_ticket.ResumeLayout(False)
    Me.gb_status1.ResumeLayout(False)
    Me.tb_collections.ResumeLayout(False)
    Me.gb_cage_sessions.ResumeLayout(False)
    Me.gb_template.ResumeLayout(False)
    Me.gb_note_counter_status.ResumeLayout(False)
    Me.gb_note_counter_status.PerformLayout()
    Me.gb_note_counter_events.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents Label1 As System.Windows.Forms.Label
  Friend WithEvents Uc_text_field1 As GUI_Controls.uc_text_field
  Friend WithEvents Label2 As System.Windows.Forms.Label
  Friend WithEvents Uc_text_field2 As GUI_Controls.uc_text_field
  Friend WithEvents Label3 As System.Windows.Forms.Label
  Friend WithEvents Uc_text_field3 As GUI_Controls.uc_text_field
  Friend WithEvents gb_cashier As System.Windows.Forms.GroupBox
  Friend WithEvents gb_operation As System.Windows.Forms.GroupBox
  Friend WithEvents gb_total As System.Windows.Forms.GroupBox
  Friend WithEvents dg_total As GUI_Controls.uc_grid
  Friend WithEvents cmb_user As GUI_Controls.uc_combo
  Friend WithEvents ef_cashier_name As GUI_Controls.uc_entry_field
  Friend WithEvents ef_user_terminals_name As GUI_Controls.uc_entry_field
  Friend WithEvents ef_operation As GUI_Controls.uc_entry_field
  Friend WithEvents ef_cage_user As GUI_Controls.uc_entry_field
  Friend WithEvents ef_status_movement As GUI_Controls.uc_entry_field
  Friend WithEvents ef_date_cage As GUI_Controls.uc_entry_field
  Friend WithEvents gb_movement_type As System.Windows.Forms.GroupBox
  Friend WithEvents rb_send As System.Windows.Forms.RadioButton
  Friend WithEvents gb_source_target As System.Windows.Forms.GroupBox
  Friend WithEvents rb_cashier As System.Windows.Forms.RadioButton
  Friend WithEvents rb_custom As System.Windows.Forms.RadioButton
  Friend WithEvents rb_reception As System.Windows.Forms.RadioButton
  Friend WithEvents gb_custom As System.Windows.Forms.GroupBox
  Friend WithEvents cmb_custom As GUI_Controls.uc_combo
  Friend WithEvents ef_custom As GUI_Controls.uc_entry_field
  Friend WithEvents panel_cashier_type As System.Windows.Forms.Panel
  Friend WithEvents rb_to_terminal As System.Windows.Forms.RadioButton
  Friend WithEvents rb_to_user As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_terminals As GUI_Controls.uc_combo
  Friend WithEvents rb_gaming_table As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_gaming_table As GUI_Controls.uc_combo
  Friend WithEvents btn_load_gaming_values As GUI_Controls.uc_button
  Friend WithEvents gb_gaming_table As System.Windows.Forms.GroupBox
  Friend WithEvents panel_aux As System.Windows.Forms.Panel
  Friend WithEvents ef_gaming_table As GUI_Controls.uc_entry_field
  Friend WithEvents ef_gaming_table_visits As GUI_Controls.uc_entry_field
  Friend WithEvents cb_close_gaming_table As System.Windows.Forms.CheckBox
  Public WithEvents panel_gaming_table_type As System.Windows.Forms.Panel
  Friend WithEvents rb_terminal As System.Windows.Forms.RadioButton
  Friend WithEvents lbl_error1 As System.Windows.Forms.Label
  Friend WithEvents lbl_ok1 As System.Windows.Forms.Label
  Friend WithEvents gb_collection_params As System.Windows.Forms.GroupBox
  Friend WithEvents tb_notes As System.Windows.Forms.TextBox
  Friend WithEvents ef_extraction_date As GUI_Controls.uc_entry_field
  Friend WithEvents ef_insertion_date As GUI_Controls.uc_entry_field
  Friend WithEvents ef_employee As GUI_Controls.uc_entry_field
  Friend WithEvents ef_terminal As GUI_Controls.uc_entry_field
  Friend WithEvents tb_collections As System.Windows.Forms.TabControl
  Friend WithEvents tab_ticket As System.Windows.Forms.TabPage
  Friend WithEvents gb_status1 As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_error As System.Windows.Forms.Label
  Friend WithEvents tf_error1 As GUI_Controls.uc_text_field
  Friend WithEvents Label5 As System.Windows.Forms.Label
  Friend WithEvents tf_ok1 As GUI_Controls.uc_text_field
  Friend WithEvents dg_collection_tickets As GUI_Controls.uc_grid
  Friend WithEvents ef_ticket As GUI_Controls.uc_entry_field
  Friend WithEvents btn_save_tickets As GUI_Controls.uc_button
  Friend WithEvents cmb_cage_sessions As GUI_Controls.uc_combo
  Friend WithEvents gb_cage_sessions As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_notes As System.Windows.Forms.Label
  Friend WithEvents ef_cage_session As GUI_Controls.uc_entry_field
  Friend WithEvents ef_cashier_date As GUI_Controls.uc_entry_field
  Friend WithEvents lb_gt_without_integrated_cashier As System.Windows.Forms.Label
  Friend WithEvents ef_stacker_id As GUI_Controls.uc_entry_field
  Friend WithEvents gb_template As System.Windows.Forms.GroupBox
  Friend WithEvents cmb_cage_template As GUI_Controls.uc_combo
  Friend WithEvents lb_error_messages As System.Windows.Forms.Label
  Friend WithEvents lbl_counter As System.Windows.Forms.Label
  Friend WithEvents ef_floor_id As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_pending As System.Windows.Forms.Label
  Friend WithEvents tf_pending As GUI_Controls.uc_text_field
  Friend WithEvents btn_open_events As GUI_Controls.uc_button
  Friend WithEvents lb_note_counter_status As System.Windows.Forms.Label
  Friend WithEvents tmr_note_counter_working As System.Windows.Forms.Timer
  Friend WithEvents gb_note_counter_status As System.Windows.Forms.GroupBox
  Friend WithEvents gb_note_counter_events As System.Windows.Forms.GroupBox
  Friend WithEvents dg_concepts As GUI_Controls.uc_grid
  Friend WithEvents tb_concepts As System.Windows.Forms.TabControl
  Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
  Friend WithEvents tb_currency As System.Windows.Forms.TabControl
  Friend WithEvents ef_num_of_tickets As GUI_Controls.uc_entry_field
End Class
