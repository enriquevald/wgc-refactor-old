<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_table_play_sessions
  Inherits GUI_Controls.frm_base_sel_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_table_play_sessions))
    Me.gb_user = New System.Windows.Forms.GroupBox()
    Me.chk_show_all = New System.Windows.Forms.CheckBox()
    Me.opt_one_user = New System.Windows.Forms.RadioButton()
    Me.opt_all_users = New System.Windows.Forms.RadioButton()
    Me.cmb_user = New GUI_Controls.uc_combo()
    Me.uc_account_sel1 = New GUI_Controls.uc_account_sel()
    Me.uc_checked_list_groups = New GUI_Controls.uc_checked_list()
    Me.gb_average_bet = New System.Windows.Forms.GroupBox()
    Me.ef_current_bet_to = New GUI_Controls.uc_entry_field()
    Me.ef_current_bet_from = New GUI_Controls.uc_entry_field()
    Me.game_status = New GUI_Controls.uc_combo_one_all()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.rb_finish_date = New System.Windows.Forms.RadioButton()
    Me.rb_ini_date = New System.Windows.Forms.RadioButton()
    Me.rb_historic = New System.Windows.Forms.RadioButton()
    Me.rb_open_sessions = New System.Windows.Forms.RadioButton()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.gb_manual_entry = New System.Windows.Forms.GroupBox()
    Me.lbl_color_not_imported = New System.Windows.Forms.Label()
    Me.lbl_color_imported = New System.Windows.Forms.Label()
    Me.chk_manual_entry_no = New System.Windows.Forms.CheckBox()
    Me.chk_manual_entry_yes = New System.Windows.Forms.CheckBox()
    Me.uc_multicurrency = New GUI_Controls.uc_multi_currency_site_sel()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_user.SuspendLayout()
    Me.gb_average_bet.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_manual_entry.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.uc_multicurrency)
    Me.panel_filter.Controls.Add(Me.gb_manual_entry)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Controls.Add(Me.gb_average_bet)
    Me.panel_filter.Controls.Add(Me.gb_user)
    Me.panel_filter.Controls.Add(Me.game_status)
    Me.panel_filter.Controls.Add(Me.uc_checked_list_groups)
    Me.panel_filter.Controls.Add(Me.uc_account_sel1)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1230, 237)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account_sel1, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_checked_list_groups, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.game_status, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_user, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_average_bet, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_manual_entry, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_multicurrency, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 241)
    Me.panel_data.Size = New System.Drawing.Size(1230, 437)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1224, 22)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1224, 4)
    '
    'gb_user
    '
    Me.gb_user.Controls.Add(Me.chk_show_all)
    Me.gb_user.Controls.Add(Me.opt_one_user)
    Me.gb_user.Controls.Add(Me.opt_all_users)
    Me.gb_user.Controls.Add(Me.cmb_user)
    Me.gb_user.Location = New System.Drawing.Point(310, 7)
    Me.gb_user.Name = "gb_user"
    Me.gb_user.Size = New System.Drawing.Size(307, 72)
    Me.gb_user.TabIndex = 3
    Me.gb_user.TabStop = False
    Me.gb_user.Text = "xUser"
    '
    'chk_show_all
    '
    Me.chk_show_all.AutoSize = True
    Me.chk_show_all.Location = New System.Drawing.Point(104, 49)
    Me.chk_show_all.Name = "chk_show_all"
    Me.chk_show_all.Size = New System.Drawing.Size(102, 17)
    Me.chk_show_all.TabIndex = 3
    Me.chk_show_all.Text = "chk_show_all"
    Me.chk_show_all.UseVisualStyleBackColor = True
    '
    'opt_one_user
    '
    Me.opt_one_user.Location = New System.Drawing.Point(12, 17)
    Me.opt_one_user.Name = "opt_one_user"
    Me.opt_one_user.Size = New System.Drawing.Size(72, 24)
    Me.opt_one_user.TabIndex = 0
    Me.opt_one_user.Text = "xOne"
    '
    'opt_all_users
    '
    Me.opt_all_users.Location = New System.Drawing.Point(15, 45)
    Me.opt_all_users.Name = "opt_all_users"
    Me.opt_all_users.Size = New System.Drawing.Size(64, 24)
    Me.opt_all_users.TabIndex = 1
    Me.opt_all_users.Text = "xAll"
    '
    'cmb_user
    '
    Me.cmb_user.AllowUnlistedValues = False
    Me.cmb_user.AutoCompleteMode = False
    Me.cmb_user.IsReadOnly = False
    Me.cmb_user.Location = New System.Drawing.Point(101, 17)
    Me.cmb_user.Name = "cmb_user"
    Me.cmb_user.SelectedIndex = -1
    Me.cmb_user.Size = New System.Drawing.Size(200, 24)
    Me.cmb_user.SufixText = "Sufix Text"
    Me.cmb_user.SufixTextVisible = True
    Me.cmb_user.TabIndex = 2
    Me.cmb_user.TextCombo = Nothing
    Me.cmb_user.TextVisible = False
    Me.cmb_user.TextWidth = 0
    '
    'uc_account_sel1
    '
    Me.uc_account_sel1.Account = ""
    Me.uc_account_sel1.AccountText = ""
    Me.uc_account_sel1.Anon = False
    Me.uc_account_sel1.AutoSize = True
    Me.uc_account_sel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_account_sel1.BirthDate = New Date(CType(0, Long))
    Me.uc_account_sel1.DisabledHolder = False
    Me.uc_account_sel1.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.uc_account_sel1.Holder = ""
    Me.uc_account_sel1.Location = New System.Drawing.Point(308, 82)
    Me.uc_account_sel1.MassiveSearchNumbers = ""
    Me.uc_account_sel1.MassiveSearchNumbersToEdit = ""
    Me.uc_account_sel1.MassiveSearchType = 0
    Me.uc_account_sel1.Name = "uc_account_sel1"
    Me.uc_account_sel1.SearchTrackDataAsInternal = True
    Me.uc_account_sel1.ShowMassiveSearch = False
    Me.uc_account_sel1.ShowVipClients = True
    Me.uc_account_sel1.Size = New System.Drawing.Size(307, 134)
    Me.uc_account_sel1.TabIndex = 4
    Me.uc_account_sel1.Telephone = ""
    Me.uc_account_sel1.TrackData = ""
    Me.uc_account_sel1.Vip = False
    Me.uc_account_sel1.WeddingDate = New Date(CType(0, Long))
    '
    'uc_checked_list_groups
    '
    Me.uc_checked_list_groups.GroupBoxText = "xCheckedList"
    Me.uc_checked_list_groups.Location = New System.Drawing.Point(618, 6)
    Me.uc_checked_list_groups.m_resize_width = 346
    Me.uc_checked_list_groups.multiChoice = True
    Me.uc_checked_list_groups.Name = "uc_checked_list_groups"
    Me.uc_checked_list_groups.SelectedIndexes = New Integer(-1) {}
    Me.uc_checked_list_groups.SelectedIndexesList = ""
    Me.uc_checked_list_groups.SelectedIndexesListLevel2 = ""
    Me.uc_checked_list_groups.SelectedValuesArray = New String(-1) {}
    Me.uc_checked_list_groups.SelectedValuesList = ""
    Me.uc_checked_list_groups.SetLevels = 2
    Me.uc_checked_list_groups.Size = New System.Drawing.Size(346, 153)
    Me.uc_checked_list_groups.TabIndex = 5
    Me.uc_checked_list_groups.ValuesArray = New String(-1) {}
    '
    'gb_average_bet
    '
    Me.gb_average_bet.Controls.Add(Me.ef_current_bet_to)
    Me.gb_average_bet.Controls.Add(Me.ef_current_bet_from)
    Me.gb_average_bet.Location = New System.Drawing.Point(618, 160)
    Me.gb_average_bet.Name = "gb_average_bet"
    Me.gb_average_bet.Size = New System.Drawing.Size(214, 75)
    Me.gb_average_bet.TabIndex = 6
    Me.gb_average_bet.TabStop = False
    Me.gb_average_bet.Text = "xCurrentBet"
    '
    'ef_current_bet_to
    '
    Me.ef_current_bet_to.DoubleValue = 0.0R
    Me.ef_current_bet_to.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_current_bet_to.IntegerValue = 0
    Me.ef_current_bet_to.IsReadOnly = False
    Me.ef_current_bet_to.Location = New System.Drawing.Point(14, 45)
    Me.ef_current_bet_to.Margin = New System.Windows.Forms.Padding(3, 3, 3, 2)
    Me.ef_current_bet_to.Name = "ef_current_bet_to"
    Me.ef_current_bet_to.PlaceHolder = Nothing
    Me.ef_current_bet_to.Size = New System.Drawing.Size(192, 25)
    Me.ef_current_bet_to.SufixText = "xDesde"
    Me.ef_current_bet_to.SufixTextVisible = True
    Me.ef_current_bet_to.TabIndex = 1
    Me.ef_current_bet_to.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_current_bet_to.TextValue = ""
    Me.ef_current_bet_to.TextWidth = 50
    Me.ef_current_bet_to.Value = ""
    Me.ef_current_bet_to.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_current_bet_from
    '
    Me.ef_current_bet_from.DoubleValue = 0.0R
    Me.ef_current_bet_from.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_current_bet_from.IntegerValue = 0
    Me.ef_current_bet_from.IsReadOnly = False
    Me.ef_current_bet_from.Location = New System.Drawing.Point(14, 16)
    Me.ef_current_bet_from.Margin = New System.Windows.Forms.Padding(3, 3, 3, 2)
    Me.ef_current_bet_from.Name = "ef_current_bet_from"
    Me.ef_current_bet_from.PlaceHolder = Nothing
    Me.ef_current_bet_from.Size = New System.Drawing.Size(192, 25)
    Me.ef_current_bet_from.SufixText = "xDesde"
    Me.ef_current_bet_from.SufixTextVisible = True
    Me.ef_current_bet_from.TabIndex = 0
    Me.ef_current_bet_from.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_current_bet_from.TextValue = ""
    Me.ef_current_bet_from.TextWidth = 50
    Me.ef_current_bet_from.Value = ""
    Me.ef_current_bet_from.ValueForeColor = System.Drawing.Color.Blue
    '
    'game_status
    '
    Me.game_status.AutoSize = True
    Me.game_status.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.game_status.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.game_status.Location = New System.Drawing.Point(1, 154)
    Me.game_status.Name = "game_status"
    Me.game_status.SelectOne = -1
    Me.game_status.Size = New System.Drawing.Size(306, 81)
    Me.game_status.TabIndex = 2
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.rb_finish_date)
    Me.gb_date.Controls.Add(Me.rb_ini_date)
    Me.gb_date.Controls.Add(Me.rb_historic)
    Me.gb_date.Controls.Add(Me.rb_open_sessions)
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(1, 6)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(306, 147)
    Me.gb_date.TabIndex = 0
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'rb_finish_date
    '
    Me.rb_finish_date.AutoCheck = False
    Me.rb_finish_date.Location = New System.Drawing.Point(59, 72)
    Me.rb_finish_date.Name = "rb_finish_date"
    Me.rb_finish_date.Size = New System.Drawing.Size(144, 24)
    Me.rb_finish_date.TabIndex = 3
    Me.rb_finish_date.Text = "xFinishDate"
    '
    'rb_ini_date
    '
    Me.rb_ini_date.AutoCheck = False
    Me.rb_ini_date.Location = New System.Drawing.Point(59, 51)
    Me.rb_ini_date.Name = "rb_ini_date"
    Me.rb_ini_date.Size = New System.Drawing.Size(144, 24)
    Me.rb_ini_date.TabIndex = 2
    Me.rb_ini_date.Text = "xIniDate"
    '
    'rb_historic
    '
    Me.rb_historic.AutoCheck = False
    Me.rb_historic.Location = New System.Drawing.Point(25, 33)
    Me.rb_historic.Name = "rb_historic"
    Me.rb_historic.Size = New System.Drawing.Size(144, 24)
    Me.rb_historic.TabIndex = 1
    Me.rb_historic.Text = "xHistoric"
    '
    'rb_open_sessions
    '
    Me.rb_open_sessions.AutoCheck = False
    Me.rb_open_sessions.Location = New System.Drawing.Point(25, 13)
    Me.rb_open_sessions.Name = "rb_open_sessions"
    Me.rb_open_sessions.Size = New System.Drawing.Size(144, 24)
    Me.rb_open_sessions.TabIndex = 0
    Me.rb_open_sessions.Text = "xOpenSessions"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(55, 118)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(240, 25)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 5
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(55, 94)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = False
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(240, 25)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 4
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'gb_manual_entry
    '
    Me.gb_manual_entry.Controls.Add(Me.lbl_color_not_imported)
    Me.gb_manual_entry.Controls.Add(Me.lbl_color_imported)
    Me.gb_manual_entry.Controls.Add(Me.chk_manual_entry_no)
    Me.gb_manual_entry.Controls.Add(Me.chk_manual_entry_yes)
    Me.gb_manual_entry.Location = New System.Drawing.Point(833, 160)
    Me.gb_manual_entry.Name = "gb_manual_entry"
    Me.gb_manual_entry.Size = New System.Drawing.Size(131, 75)
    Me.gb_manual_entry.TabIndex = 7
    Me.gb_manual_entry.TabStop = False
    Me.gb_manual_entry.Text = "xManualEntry"
    '
    'lbl_color_not_imported
    '
    Me.lbl_color_not_imported.BackColor = System.Drawing.Color.White
    Me.lbl_color_not_imported.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_color_not_imported.Location = New System.Drawing.Point(108, 45)
    Me.lbl_color_not_imported.Name = "lbl_color_not_imported"
    Me.lbl_color_not_imported.Size = New System.Drawing.Size(16, 16)
    Me.lbl_color_not_imported.TabIndex = 106
    '
    'lbl_color_imported
    '
    Me.lbl_color_imported.BackColor = System.Drawing.Color.White
    Me.lbl_color_imported.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_color_imported.Location = New System.Drawing.Point(108, 16)
    Me.lbl_color_imported.Name = "lbl_color_imported"
    Me.lbl_color_imported.Size = New System.Drawing.Size(16, 16)
    Me.lbl_color_imported.TabIndex = 105
    '
    'chk_manual_entry_no
    '
    Me.chk_manual_entry_no.Location = New System.Drawing.Point(10, 45)
    Me.chk_manual_entry_no.Name = "chk_manual_entry_no"
    Me.chk_manual_entry_no.Size = New System.Drawing.Size(107, 17)
    Me.chk_manual_entry_no.TabIndex = 1
    Me.chk_manual_entry_no.Text = "xNo"
    '
    'chk_manual_entry_yes
    '
    Me.chk_manual_entry_yes.Location = New System.Drawing.Point(10, 16)
    Me.chk_manual_entry_yes.Name = "chk_manual_entry_yes"
    Me.chk_manual_entry_yes.Size = New System.Drawing.Size(107, 17)
    Me.chk_manual_entry_yes.TabIndex = 0
    Me.chk_manual_entry_yes.Text = "xYes"
    '
    'uc_multicurrency
    '
    Me.uc_multicurrency.GroupBoxText = Nothing
    Me.uc_multicurrency.HeightControl = 0
    Me.uc_multicurrency.Location = New System.Drawing.Point(967, 6)
    Me.uc_multicurrency.Name = "uc_multicurrency"
    Me.uc_multicurrency.OpenModeControl = GUI_Controls.uc_multi_currency_site_sel.OPEN_MODE.OnlyCurrency
    Me.uc_multicurrency.SelectedCurrency = ""
    Me.uc_multicurrency.SelectedOption = GUI_Controls.uc_multi_currency_site_sel.MULTICURRENCY_OPTION.TotalToIsoCode
    Me.uc_multicurrency.Size = New System.Drawing.Size(166, 77)
    Me.uc_multicurrency.TabIndex = 8
    Me.uc_multicurrency.WidthControl = 0
    '
    'frm_table_play_sessions
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1240, 682)
    Me.Name = "frm_table_play_sessions"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_table_play_sessions"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_user.ResumeLayout(False)
    Me.gb_user.PerformLayout()
    Me.gb_average_bet.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gb_manual_entry.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_user As System.Windows.Forms.GroupBox
  Friend WithEvents chk_show_all As System.Windows.Forms.CheckBox
  Friend WithEvents opt_one_user As System.Windows.Forms.RadioButton
  Friend WithEvents opt_all_users As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_user As GUI_Controls.uc_combo
  Friend WithEvents uc_account_sel1 As GUI_Controls.uc_account_sel
  Friend WithEvents uc_checked_list_groups As GUI_Controls.uc_checked_list
  Friend WithEvents gb_average_bet As System.Windows.Forms.GroupBox
  Friend WithEvents ef_current_bet_from As GUI_Controls.uc_entry_field
  Friend WithEvents game_status As GUI_Controls.uc_combo_one_all
  Friend WithEvents ef_current_bet_to As GUI_Controls.uc_entry_field
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents rb_finish_date As System.Windows.Forms.RadioButton
  Friend WithEvents rb_ini_date As System.Windows.Forms.RadioButton
  Friend WithEvents rb_historic As System.Windows.Forms.RadioButton
  Friend WithEvents rb_open_sessions As System.Windows.Forms.RadioButton
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents gb_manual_entry As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_color_not_imported As System.Windows.Forms.Label
  Friend WithEvents lbl_color_imported As System.Windows.Forms.Label
  Friend WithEvents chk_manual_entry_no As System.Windows.Forms.CheckBox
  Friend WithEvents chk_manual_entry_yes As System.Windows.Forms.CheckBox
  Friend WithEvents uc_multicurrency As GUI_Controls.uc_multi_currency_site_sel
End Class
