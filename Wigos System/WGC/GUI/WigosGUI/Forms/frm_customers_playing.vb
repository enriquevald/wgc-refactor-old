﻿'-------------------------------------------------------------------
' Copyright © 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
'   MODULE NAME : frm_customers_playing
'   DESCRIPTION : Selection customers playing
'        AUTHOR : Alberto Marcos
' CREATION DATE : 20-JUL-2015
'
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 20-JUL-2015  AMF    Initial version.
' 21-APR-2016  ETP    Fixed Bug 12198: Added Terminal filter for dual currency.
' 07-JUN-2016  DLL    Fixed Bug 12926: Error dividing by zero.
' 06-SEP-2016  ETP    Fixed Bug 17353: Incorrect Floor ID and Bank.
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common
Imports System.Runtime.InteropServices
Imports System.Data
Imports System.Data.SqlClient
Imports WSI.Common.CollectionImport


Public Class frm_customers_playing
  Inherits frm_base_sel

#Region "Enumerates"

  Private Enum DISPLAY_MODE
    MODE_ACCOUNT_TERMINAL = 0
    MODE_TERMINAL_ACCOUNT = 1
  End Enum

#End Region

#Region " Constants "

  'GRID
  Private Const GRID_FIXED_COLUMNS As Integer = 21
  Private Const GRID_HEADER_ROWS As Integer = 2

  Private Const GRID_COLUMN_SELECT As Integer = 0

  ' Account
  Private GRID_COLUMN_ACCOUNT_ID As Integer = 1
  Private GRID_COLUMN_HOLDER_NAME As Integer = 2
  Private GRID_COLUMN_LEVEL As Integer = 3
  Private GRID_COLUMN_IS_VIP As Integer = 4

  ' Terminal
  Private GRID_COLUMN_TERMINAL_ID As Integer = 5
  Private GRID_COLUMN_PROVIDER As Integer = 6
  Private GRID_COLUMN_TERMINAL_NAME As Integer = 7
  Private GRID_COLUMN_FLOOR_ID As Integer = 8
  Private GRID_COLUMN_AREA As Integer = 9
  Private GRID_COLUMN_BANK As Integer = 10
  Private GRID_COLUMN_POSITION As Integer = 11

  ' Session
  Private Const GRID_COLUMN_STARTED As Integer = 12
  Private Const GRID_COLUMN_DURATION As Integer = 13
  Private Const GRID_COLUMN_PLAYED_COUNT As Integer = 14
  Private Const GRID_COLUMN_PLAYED_AMOUNT As Integer = 15
  Private Const GRID_COLUMN_AVERAGE_BET As Integer = 16

  ' Working day
  Private Const GRID_COLUMN_WORKING_DAY_DURATION As Integer = 17
  Private Const GRID_COLUMN_WORKING_DAY_PLAYED_COUNT As Integer = 18
  Private Const GRID_COLUMN_WORKING_DAY_PLAYED_AMOUNT As Integer = 19
  Private Const GRID_COLUMN_WORKING_DAY_AVERAGE_BET As Integer = 20

  'SQL
  Private Const SQL_COLUMN_TERMINAL_ID As Integer = 0
  Private Const SQL_COLUMN_PROVIDER As Integer = 2
  Private Const SQL_COLUMN_TERMINAL_NAME As Integer = 3
  Private Const SQL_COLUMN_FLOOR_ID As Integer = 4
  Private Const SQL_COLUMN_AREA As Integer = 6
  Private Const SQL_COLUMN_BANK As Integer = 8
  Private Const SQL_COLUMN_POSITION As Integer = 9

  Private Const SQL_COLUMN_ACCOUNT_ID As Integer = 10
  Private Const SQL_COLUMN_HOLDER_NAME As Integer = 11
  Private Const SQL_COLUMN_TRACK_DATA As Integer = 12
  Private Const SQL_COLUMN_LEVEL As Integer = 14
  Private Const SQL_COLUMN_IS_VIP As Integer = 15

  Private Const SQL_COLUMN_STARTED As Integer = 17
  Private Const SQL_COLUMN_DURATION As Integer = 18
  Private Const SQL_COLUMN_PLAYED_COUNT As Integer = 19
  Private Const SQL_COLUMN_PLAYED_AMOUNT As Integer = 20
  Private Const SQL_COLUMN_AVERAGE_BET As Integer = 21

  Private Const SQL_COLUMN_WORKING_DAY_DURATION As Integer = 22
  Private Const SQL_COLUMN_WORKING_DAY_PLAYED_COUNT As Integer = 23
  Private Const SQL_COLUMN_WORKING_DAY_PLAYED_AMOUNT As Integer = 24
  Private Const SQL_COLUMN_WORKING_DAY_AVERAGE_BET As Integer = 25
  Private Const SQL_COLUMN_ACCOUNT_TYPE As Integer = 26

  ' Colums Grid Width
  Private Const GRID_WIDTH_COL_PROVIDER As Integer = 2000
  Private Const GRID_WIDTH_COL_TERMINAL_ID As Integer = 1400
  Private Const GRID_WIDTH_COL_TERMINAL As Integer = 2200
  Private Const GRID_WIDTH_COL_FLOOR As Integer = 1300
  Private Const GRID_WIDTH_COL_AREA As Integer = 1900
  Private Const GRID_WIDTH_COL_BANK As Integer = 1400
  Private Const GRID_WIDTH_COL_POSITION As Integer = 1000
  Private Const GRID_WIDTH_COL_ACCOUNT_ID As Integer = 1150
  Private Const GRID_WIDTH_COL_HOLDER_NAME As Integer = 2500
  Private Const GRID_WIDTH_COL_LEVEL As Integer = 1150
  Private Const GRID_WIDTH_HOLDER_IS_VIP As Integer = 800
  Private Const GRID_WIDTH_COL_STARTED As Integer = 1900
  Private Const GRID_WIDTH_COL_DURATION As Integer = 1500
  Private Const GRID_WIDTH_COL_PLAYED_COUNT As Integer = 1000
  Private Const GRID_WIDTH_COL_PLAYED_AMOUNT As Integer = 1800
  Private Const GRID_WIDTH_COL_AVERAGE_BET As Integer = 1400

  'XML
  Private Const SQL_XML_DATA As Integer = 0
  Private Const SQL_XML_SCHEMA As Integer = 1
  Private Const SQL_TABLE As Integer = 0

#End Region 'Constants

#Region " Members "

  ' Report filters 
  Private m_account As String
  Private m_track_data As String
  Private m_holder As String
  Private m_holder_is_vip As String
  Private m_order As String
  Private m_level As String
  Private m_duration_filter As String
  Private m_average_bet As String
  Private m_option As String

  ' Totals
  Private m_duration As Long
  Private m_played_count As Integer
  Private m_played_amount As Decimal
  Private m_working_day_duration As Long
  Private m_working_day_played_count As Integer
  Private m_working_day_played_amount As Decimal

  Private m_first_time As Boolean = True
  Private m_display_mode As DISPLAY_MODE
  Private m_selected_ids As List(Of Integer)

  Private m_first_time_search As Boolean

#End Region 'Members

#Region " Public Functions "

  ' PURPOSE: Create a new instance
  '         
  ' PARAMS:
  '    - INPUT:
  '       TempFiles
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub 'New

  ' PURPOSE : Opens dialog with default settings for edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region 'Public Functions

#Region " Overrides "

  ' PURPOSE: GUI form Id
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: None
  '
  ' NOTES
  ' 
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CUSTOMERS_PLAYING

    'Call Base Form proc
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    m_first_time_search = True

    ' Form title 
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6609)

    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(3)

    ' Update info
    Me.tf_last_update.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(324)
    Me.tf_last_update.Value = "0"
    Me.tf_last_update.SufixText = GLB_NLS_GUI_JACKPOT_MGR.GetString(310)

    ' Level
    gb_level.Text = GLB_NLS_GUI_INVOICING.GetString(381)
    chk_level_01.Text = GetCashierPlayerTrackingData("Level01.Name")
    If chk_level_01.Text = "" Then
      chk_level_01.Text = GLB_NLS_GUI_CONFIGURATION.GetString(289)
    End If
    chk_level_02.Text = GetCashierPlayerTrackingData("Level02.Name")
    If chk_level_02.Text = "" Then
      chk_level_02.Text = GLB_NLS_GUI_CONFIGURATION.GetString(290)
    End If
    chk_level_03.Text = GetCashierPlayerTrackingData("Level03.Name")
    If chk_level_03.Text = "" Then
      chk_level_03.Text = GLB_NLS_GUI_CONFIGURATION.GetString(291)
    End If
    chk_level_04.Text = GetCashierPlayerTrackingData("Level04.Name")
    If chk_level_04.Text = "" Then
      chk_level_04.Text = GLB_NLS_GUI_CONFIGURATION.GetString(332)
    End If

    ' Session/Working Day
    Me.opt_session.Text = GLB_NLS_GUI_STATISTICS.GetString(393)
    Me.opt_working_day.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5723)
    ' Duration
    lbl_duration.Text = GLB_NLS_GUI_STATISTICS.GetString(382)
    dtp_duration.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    ' Average Bet
    uc_entry_average_bet.Text = GLB_NLS_GUI_STATISTICS.GetString(347)
    uc_entry_average_bet.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 5, 2)

    ' Order By
    gb_order_by.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(792)
    opt_account.Text = GLB_NLS_GUI_STATISTICS.GetString(207)
    opt_date_started.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6656)
    opt_provider_terminal.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6657)

    Me.tmr_monitor.Start()

    Call GUI_StyleSheet()

    Call Me.uc_pr_list.Init(WSI.Common.Misc.GamingTerminalTypeList())

    Call SetDefaultValues()

    GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)

  End Sub ' GUI_InitControls

  ' PURPOSE: Called to be set the initial focus.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.uc_account_sel1

  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()

  End Sub ' GUI_FilterReset

  ' PURPOSE: Get the defined Query Type
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_QUERY_TYPE
  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM
  End Function 'GUI_GetQueryType

  ' PURPOSE: Get custom DataTable
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - DataTable
  Protected Overrides Function GUI_GetCustomDataTable() As DataTable

    Dim _ds As DataSet
    Dim _dw As DataView
    Dim _sb As System.Text.StringBuilder
    Dim _value As String
    Dim _account_filter As String
    Dim _terminal_filter As String
    Dim _account_sort As String
    Dim _sr_xml As IO.StringReader
    Dim _sr_schema As IO.StringReader

    _ds = New DataSet()
    _dw = New DataView()
    _sb = New System.Text.StringBuilder()
    _value = String.Empty
    _account_filter = String.Empty
    _account_sort = String.Empty
    _sr_schema = Nothing
    _sr_xml = Nothing

    Try

      _sb.AppendLine(" SELECT   MD_DATA      ")
      _sb.AppendLine("        , MD_SCHEMA    ")
      _sb.AppendLine("   FROM   MONITOR_DATA ")
      _sb.AppendLine("  WHERE   MD_TYPE = 1  ")

      Using _db_trx As DB_TRX = New DB_TRX()
        Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          Using _data_reader As SqlDataReader = _cmd.ExecuteReader()
            If _data_reader.Read() Then
              _sr_xml = New IO.StringReader(_data_reader.GetString(SQL_XML_DATA))
              _sr_schema = New IO.StringReader(_data_reader.GetString(SQL_XML_SCHEMA))
            End If
          End Using

          If _sr_schema IsNot Nothing And _sr_xml IsNot Nothing Then
            _ds.ReadXmlSchema(_sr_schema)
            _ds.ReadXml(_sr_xml, XmlReadMode.Auto)
            _dw = _ds.Tables(SQL_TABLE).DefaultView

            ' Order
            If opt_account.Checked Then
              _account_sort = "AC_ACCOUNT_ID"
            ElseIf opt_date_started.Checked Then
              _account_sort = "PS_STARTED DESC"
            ElseIf opt_provider_terminal.Checked Then
              _account_sort = "PV_NAME, TE_NAME"
            End If
            _dw.Sort = _account_sort

            ' Filter user
            _account_filter = Me.uc_account_sel1.GetFilterSQL(False)
            If _account_filter <> String.Empty Then
              _dw.RowFilter = _account_filter
            End If

            _terminal_filter = uc_pr_list.GetTerminalRowFilter()

            If (_terminal_filter) <> String.Empty Then

              _dw.RowFilter += IIf(_dw.RowFilter <> String.Empty, " AND ", "") & "TE_TERMINAL_ID IN (" & _terminal_filter & ") "
            End If


            ' Level
            If Me.chk_level_01.Checked Or _
               Me.chk_level_02.Checked Or _
               Me.chk_level_03.Checked Or _
               Me.chk_level_04.Checked Then

              _dw.RowFilter += IIf(_dw.RowFilter <> String.Empty, " AND ", "") & " AC_HOLDER_LEVEL IN (" & GetLevelsSelected() & ") "

            End If

            ' Duration
            If Me.dtp_duration.Value.TimeOfDay.TotalSeconds > 0 Then
              If Me.opt_session.Checked Then
                _dw.RowFilter += IIf(_dw.RowFilter <> String.Empty, " AND ", "") & " PS1PS_DURATION >= " & Me.dtp_duration.Value.TimeOfDay.TotalSeconds.ToString()
              ElseIf Me.opt_working_day.Checked Then
                _dw.RowFilter += IIf(_dw.RowFilter <> String.Empty, " AND ", "") & " WD_DURATION >= " & Me.dtp_duration.Value.TimeOfDay.TotalSeconds.ToString()
              End If
            End If

            ' Average Bet
            If Me.uc_entry_average_bet.Value <> String.Empty Then
              If Me.opt_session.Checked Then
                _dw.RowFilter += IIf(_dw.RowFilter <> String.Empty, " AND ", "") & " PS_AVERAGE_BET >= " & Me.uc_entry_average_bet.Value.ToString()
              ElseIf Me.opt_working_day.Checked Then
                _dw.RowFilter += IIf(_dw.RowFilter <> String.Empty, " AND ", "") & " WD_AVERAGE_BET >= " & Me.uc_entry_average_bet.Value.ToString()
              End If
            End If

            Return _dw.ToTable()
          End If

        End Using
      End Using

    Catch _ex As Exception

      Log.Exception(_ex)
    End Try

    Return New DataTable()

  End Function 'GUI_GetCustomDataTable

  ' PURPOSE: Define the ExecuteQuery customized
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     -
  Protected Overrides Sub GUI_ExecuteQueryCustom()
    Dim _function_name As String
    Dim _max_rows As Integer
    Dim _table As DataTable
    Dim _idx_row As Integer
    Dim _count As Integer
    Dim _more_than_max As Boolean
    Dim _db_row As CLASS_DB_ROW
    Dim _grid_rows As Integer
    Dim _has_changed As Boolean

    _table = Nothing
    _function_name = "GUI_ExecuteQueryCustom"
    _has_changed = False
    _max_rows = GUI_MaxRows()

    Try

      Me.tmr_monitor.Stop()

      SaveSelectedGridIds()

      _table = GUI_GetCustomDataTable()

      Call GUI_ReportUpdateFilters()

      If _table Is Nothing Then

        Return
      End If

      _grid_rows = _table.Rows.Count

      _more_than_max = False

      Call GUI_BeforeFirstRow()

      _count = 0
      _idx_row = 0

      For _idx_row = 0 To _table.Rows.Count - 1

        If _idx_row >= Me.Grid.NumRows Then
          Me.Grid.AddRow()
        End If

        _db_row = New CLASS_DB_ROW(_table.Rows(_idx_row))
        Me.Grid.Redraw = False

        If GUI_SetupRow(_idx_row, _db_row) Then
          Me.Grid.Redraw = True
        End If

      Next

      While _idx_row < Me.Grid.NumRows
        Me.Grid.DeleteRow(_idx_row)
      End While

      Call GUI_AfterLastRow()

      SelectLastSelectedRow()

      If _more_than_max Then
        Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(111), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, , , CStr(_max_rows))
      End If

      Me.tmr_monitor.Start()

      Me.tf_last_update.Value = GUI_FormatDate(WGDB.Now, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    Catch ex As Exception
      ' An error has occurred in the query execution
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "Customers playing", _
                            _function_name, _
                            ex.Message)
      Me.Grid.Redraw = True

    End Try

  End Sub ' GUI_ExecuteQueryCustom

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()

    Dim _display_mode As DISPLAY_MODE

    If opt_account.Checked OrElse opt_date_started.Checked Then
      _display_mode = DISPLAY_MODE.MODE_ACCOUNT_TERMINAL
    Else
      _display_mode = DISPLAY_MODE.MODE_TERMINAL_ACCOUNT
    End If

    If _display_mode <> m_display_mode Then
      Call GUI_StyleSheet()
    End If

    m_duration = 0
    m_played_count = 0
    m_played_amount = 0
    m_working_day_duration = 0
    m_working_day_played_count = 0
    m_working_day_played_amount = 0

  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE: Indicates if to select the first row of the grid or not
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Boolean
  '
  Protected Overrides Function GUI_SelectFirstRow() As Boolean
    Dim _select_first As Boolean

    _select_first = m_first_time
    m_first_time = False

    Return _select_first
  End Function ' GUI_SelectFirstRow

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()

    Dim _idx_row As Integer
    Dim _time As TimeSpan
    Dim _total_average_bet As Double

    _idx_row = Me.Grid.NumRows

    Me.Grid.AddRow()

    Me.Grid.Counter(0).Value -= 1

    ' Total
    Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_ID).Value = GLB_NLS_GUI_INVOICING.GetString(205)

    ' Duration
    _time = New TimeSpan(0, 0, m_duration)
    If _time.TotalSeconds > 0 Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_DURATION).Value = TimeSpanToString(_time)
    End If

    ' Played Count
    Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_COUNT).Value = GUI_FormatNumber(m_played_count, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)

    ' Played Amount
    Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_AMOUNT).Value = GUI_FormatCurrency(m_played_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Average Bet
    If m_played_amount = 0 Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_AVERAGE_BET).Value = ""
    Else
      _total_average_bet = Math.Round(m_played_amount / IIf(m_played_count <> 0, m_played_count, 1), 2, MidpointRounding.AwayFromZero)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_AVERAGE_BET).Value = GUI_FormatCurrency(_total_average_bet, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' Working Day Duration
    _time = New TimeSpan(0, 0, m_working_day_duration)
    If _time.TotalSeconds > 0 Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_WORKING_DAY_DURATION).Value = TimeSpanToString(_time)
    End If

    ' Working Day Played Count
    Me.Grid.Cell(_idx_row, GRID_COLUMN_WORKING_DAY_PLAYED_COUNT).Value = GUI_FormatNumber(m_working_day_played_count, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)

    ' Working Day Played Amount
    Me.Grid.Cell(_idx_row, GRID_COLUMN_WORKING_DAY_PLAYED_AMOUNT).Value = GUI_FormatCurrency(m_working_day_played_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Working Day Average Bet
    If m_working_day_played_amount = 0 Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_WORKING_DAY_AVERAGE_BET).Value = ""
    Else
      _total_average_bet = Math.Round(m_working_day_played_amount / IIf(m_working_day_played_count <> 0, m_working_day_played_count, 1), 2, MidpointRounding.AwayFromZero)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_WORKING_DAY_AVERAGE_BET).Value = GUI_FormatCurrency(_total_average_bet, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' Color Row
    Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

  End Sub ' GUI_AfterLastRow

  ' PURPOSE: Overrides the clear grid routine
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Boolean
  '
  Protected Overrides Sub GUI_ClearGrid()
    If m_first_time Then
      Call MyBase.GUI_ClearGrid()
    End If
  End Sub ' GUI_ClearGrid

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _time As TimeSpan

    Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)

    ' Account Id
    If Not DbRow.IsNull(SQL_COLUMN_ACCOUNT_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_ID)
    End If

    ' Holder Name
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_NAME).Value = DbRow.Value(SQL_COLUMN_HOLDER_NAME)
    End If

    ' Level
    If Not DbRow.IsNull(SQL_COLUMN_LEVEL) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_LEVEL).Value = DbRow.Value(SQL_COLUMN_LEVEL)
    End If

    ' VIP
    Grid.Cell(RowIndex, GRID_COLUMN_IS_VIP).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_IS_VIP)) Then
      If DbRow.Value(SQL_COLUMN_IS_VIP) Then
        Grid.Cell(RowIndex, GRID_COLUMN_IS_VIP).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
      End If
    End If

    ' Terminal Id
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_ID).Value = DbRow.Value(SQL_COLUMN_TERMINAL_ID)

    ' Provider
    If Not DbRow.IsNull(SQL_COLUMN_PROVIDER) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROVIDER).Value = DbRow.Value(SQL_COLUMN_PROVIDER)
    End If

    ' Terminal Name
    If Not DbRow.IsNull(SQL_COLUMN_TERMINAL_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_NAME).Value = DbRow.Value(SQL_COLUMN_TERMINAL_NAME)
    End If

    ' Floor ID
    If Not DbRow.IsNull(SQL_COLUMN_FLOOR_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_FLOOR_ID).Value = DbRow.Value(SQL_COLUMN_FLOOR_ID)
    End If

    ' Area
    If Not DbRow.IsNull(SQL_COLUMN_AREA) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_AREA).Value = DbRow.Value(SQL_COLUMN_AREA)
    End If

    ' Bank
    If Not DbRow.IsNull(SQL_COLUMN_BANK) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_BANK).Value = DbRow.Value(SQL_COLUMN_BANK)
    End If

    ' Position
    If Not DbRow.IsNull(SQL_COLUMN_POSITION) AndAlso DbRow.Value(SQL_COLUMN_POSITION) >= 0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_POSITION).Value = DbRow.Value(SQL_COLUMN_POSITION)
    End If

    ' Started
    If Not DbRow.IsNull(SQL_COLUMN_STARTED) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_STARTED).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_STARTED), _
                                                                         ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                         ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Duration
    If Not DbRow.IsNull(SQL_COLUMN_DURATION) Then
      _time = New TimeSpan(0, 0, DbRow.Value(SQL_COLUMN_DURATION))
      If _time.TotalSeconds > 0 Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DURATION).Value = TimeSpanToString(_time)
        m_duration += DbRow.Value(SQL_COLUMN_DURATION)
      End If
    End If

    ' Played Count
    If Not DbRow.IsNull(SQL_COLUMN_PLAYED_COUNT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED_COUNT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_PLAYED_COUNT), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
      m_played_count += DbRow.Value(SQL_COLUMN_PLAYED_COUNT)
    End If

    ' Played Amount
    If Not DbRow.IsNull(SQL_COLUMN_PLAYED_AMOUNT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      m_played_amount += DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT)
    End If

    ' Average Bet
    If Not DbRow.IsNull(SQL_COLUMN_AVERAGE_BET) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_AVERAGE_BET).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_AVERAGE_BET), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' Working Day Duration
    If Not DbRow.IsNull(SQL_COLUMN_WORKING_DAY_DURATION) Then
      _time = New TimeSpan(0, 0, DbRow.Value(SQL_COLUMN_WORKING_DAY_DURATION))
      If _time.TotalSeconds > 0 Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_WORKING_DAY_DURATION).Value = TimeSpanToString(_time)
        m_working_day_duration += DbRow.Value(SQL_COLUMN_WORKING_DAY_DURATION)
      End If
    End If

    ' Working Day Played Count
    If Not DbRow.IsNull(SQL_COLUMN_WORKING_DAY_PLAYED_COUNT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_WORKING_DAY_PLAYED_COUNT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_WORKING_DAY_PLAYED_COUNT), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
      m_working_day_played_count += DbRow.Value(SQL_COLUMN_WORKING_DAY_PLAYED_COUNT)
    End If

    ' Working Day Played Amount
    If Not DbRow.IsNull(SQL_COLUMN_WORKING_DAY_PLAYED_AMOUNT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_WORKING_DAY_PLAYED_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_WORKING_DAY_PLAYED_AMOUNT), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      m_working_day_played_amount += DbRow.Value(SQL_COLUMN_WORKING_DAY_PLAYED_AMOUNT)
    End If

    ' Working Day Average Bet
    If Not DbRow.IsNull(SQL_COLUMN_WORKING_DAY_AVERAGE_BET) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_WORKING_DAY_AVERAGE_BET).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_WORKING_DAY_AVERAGE_BET), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Given a type, the function returns if it needs to be audited
  '
  '  PARAMS:
  '     - INPUT:
  '         - AuditType: AUDIT_FLAGS that wants to be audited
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_HasToBeAudited(ByVal AuditType As AUDIT_FLAGS) As Boolean
    If m_first_time_search Then
      If AuditType = AUDIT_FLAGS.ACCESS Then
        m_first_time_search = False
      End If

      Return True
    Else
      Return AuditType <> AUDIT_FLAGS.SEARCH
    End If

  End Function ' GUI

#Region " GUI Reports "

  ' PURPOSE: Overridable routine to initialize report filters.
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData As GUI_Reports.CLASS_PRINT_DATA
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    ' Account
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), m_account)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_track_data)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), m_holder)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4802), m_holder_is_vip)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(381), m_level)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(382) + " " + m_option, m_duration_filter)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(347) + " " + m_option, m_average_bet)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(792), m_order)

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()

    Dim _time As TimeSpan

    ' Empty
    m_account = String.Empty
    m_holder = String.Empty
    m_track_data = String.Empty
    m_holder_is_vip = String.Empty
    m_order = String.Empty
    m_level = String.Empty
    m_duration_filter = String.Empty
    m_average_bet = String.Empty
    m_option = String.Empty

    ' Account
    m_account = Me.uc_account_sel1.Account()
    m_track_data = Me.uc_account_sel1.TrackData()
    m_holder = Me.uc_account_sel1.Holder()
    m_holder_is_vip = Me.uc_account_sel1.HolderIsVip

    ' Level
    m_level = GetLevelsSelected(False)

    ' Option
    If Me.opt_session.Checked Then
      m_option = Me.opt_session.Text.ToLower
    ElseIf Me.opt_working_day.Checked Then
      m_option = Me.opt_working_day.Text.ToLower
    End If

    ' Duration
    If Me.dtp_duration.Value.TimeOfDay.TotalSeconds > 0 Then
      _time = New TimeSpan(0, 0, Me.dtp_duration.Value.TimeOfDay.TotalSeconds)
      m_duration_filter = Strings.Left(TimeSpanToString(_time), 8)
    End If

    ' Average Bet
    If uc_entry_average_bet.Value <> String.Empty Then
      m_average_bet = GUI_FormatCurrency(uc_entry_average_bet.Value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' Order By
    If opt_account.Checked Then
      m_order = opt_account.Text
    ElseIf opt_date_started.Checked Then
      m_order = opt_date_started.Text
    ElseIf opt_provider_terminal.Checked Then
      m_order = opt_provider_terminal.Text
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region ' Overrides

#Region " Private Functions "

  ' PURPOSE : Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS :
  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_FIXED_COLUMNS, GRID_HEADER_ROWS)

      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE

      ' Order By
      If opt_account.Checked OrElse opt_date_started.Checked Then
        ' Account
        GRID_COLUMN_ACCOUNT_ID = 1
        GRID_COLUMN_HOLDER_NAME = 2
        GRID_COLUMN_LEVEL = 3
        GRID_COLUMN_IS_VIP = 4

        ' Terminal
        GRID_COLUMN_TERMINAL_ID = 5
        GRID_COLUMN_PROVIDER = 6
        GRID_COLUMN_TERMINAL_NAME = 7
        GRID_COLUMN_FLOOR_ID = 8
        GRID_COLUMN_AREA = 9
        GRID_COLUMN_BANK = 10
        GRID_COLUMN_POSITION = 11

        m_display_mode = DISPLAY_MODE.MODE_ACCOUNT_TERMINAL
      ElseIf opt_provider_terminal.Checked Then
        ' Terminal
        GRID_COLUMN_TERMINAL_ID = 1
        GRID_COLUMN_PROVIDER = 2
        GRID_COLUMN_TERMINAL_NAME = 3
        GRID_COLUMN_FLOOR_ID = 4
        GRID_COLUMN_AREA = 5
        GRID_COLUMN_BANK = 6
        GRID_COLUMN_POSITION = 7
        ' Account
        GRID_COLUMN_ACCOUNT_ID = 8
        GRID_COLUMN_HOLDER_NAME = 9
        GRID_COLUMN_LEVEL = 10
        GRID_COLUMN_IS_VIP = 11

        m_display_mode = DISPLAY_MODE.MODE_TERMINAL_ACCOUNT
      End If

      ' Blank "select" column at the left of the grid
      .Column(GRID_COLUMN_SELECT).Header(0).Text = ""
      .Column(GRID_COLUMN_SELECT).Header(1).Text = ""
      .Column(GRID_COLUMN_SELECT).Width = 150
      .Column(GRID_COLUMN_SELECT).HighLightWhenSelected = True
      .Column(GRID_COLUMN_SELECT).IsColumnPrintable = False

      ' Account Id
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COLUMN_ACCOUNT_ID).Width = GRID_WIDTH_COL_ACCOUNT_ID
      .Column(GRID_COLUMN_ACCOUNT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Holder Name
      .Column(GRID_COLUMN_HOLDER_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_HOLDER_NAME).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(235)
      .Column(GRID_COLUMN_HOLDER_NAME).Width = GRID_WIDTH_COL_HOLDER_NAME
      .Column(GRID_COLUMN_HOLDER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Level
      .Column(GRID_COLUMN_LEVEL).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_LEVEL).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(381)
      .Column(GRID_COLUMN_LEVEL).Width = GRID_WIDTH_COL_LEVEL
      .Column(GRID_COLUMN_LEVEL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' VIP
      .Column(GRID_COLUMN_IS_VIP).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_IS_VIP).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4805)
      .Column(GRID_COLUMN_IS_VIP).Width = GRID_WIDTH_HOLDER_IS_VIP
      .Column(GRID_COLUMN_IS_VIP).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Terminal Id
      .Column(GRID_COLUMN_TERMINAL_ID).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(232)
      .Column(GRID_COLUMN_TERMINAL_ID).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COLUMN_TERMINAL_ID).Width = GRID_WIDTH_COL_TERMINAL_ID
      .Column(GRID_COLUMN_TERMINAL_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Provider
      .Column(GRID_COLUMN_PROVIDER).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(232)
      .Column(GRID_COLUMN_PROVIDER).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(268)
      .Column(GRID_COLUMN_PROVIDER).Width = GRID_WIDTH_COL_PROVIDER
      .Column(GRID_COLUMN_PROVIDER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Terminal name
      .Column(GRID_COLUMN_TERMINAL_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(232)
      .Column(GRID_COLUMN_TERMINAL_NAME).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(248)
      .Column(GRID_COLUMN_TERMINAL_NAME).Width = GRID_WIDTH_COL_TERMINAL
      .Column(GRID_COLUMN_TERMINAL_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Floor ID
      .Column(GRID_COLUMN_FLOOR_ID).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(232)
      .Column(GRID_COLUMN_FLOOR_ID).Header(1).Text = GLB_NLS_GUI_CONFIGURATION.GetString(432)
      .Column(GRID_COLUMN_FLOOR_ID).Width = GRID_WIDTH_COL_FLOOR
      .Column(GRID_COLUMN_FLOOR_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Area
      .Column(GRID_COLUMN_AREA).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(232)
      .Column(GRID_COLUMN_AREA).Header(1).Text = GLB_NLS_GUI_CONFIGURATION.GetString(435)
      .Column(GRID_COLUMN_AREA).Width = GRID_WIDTH_COL_AREA
      .Column(GRID_COLUMN_AREA).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Bank 
      .Column(GRID_COLUMN_BANK).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(232)
      .Column(GRID_COLUMN_BANK).Header(1).Text = GLB_NLS_GUI_CONFIGURATION.GetString(431)
      .Column(GRID_COLUMN_BANK).Width = GRID_WIDTH_COL_BANK
      .Column(GRID_COLUMN_BANK).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Position
      .Column(GRID_COLUMN_POSITION).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(232)
      .Column(GRID_COLUMN_POSITION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5222)
      .Column(GRID_COLUMN_POSITION).Width = GRID_WIDTH_COL_POSITION
      .Column(GRID_COLUMN_POSITION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Started
      .Column(GRID_COLUMN_STARTED).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(393)
      .Column(GRID_COLUMN_STARTED).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(381)
      .Column(GRID_COLUMN_STARTED).Width = GRID_WIDTH_COL_STARTED
      .Column(GRID_COLUMN_STARTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Duration
      .Column(GRID_COLUMN_DURATION).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(393)
      .Column(GRID_COLUMN_DURATION).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(382)
      .Column(GRID_COLUMN_DURATION).Width = GRID_WIDTH_COL_DURATION
      .Column(GRID_COLUMN_DURATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Played Count
      .Column(GRID_COLUMN_PLAYED_COUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(393)
      .Column(GRID_COLUMN_PLAYED_COUNT).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(261)
      .Column(GRID_COLUMN_PLAYED_COUNT).Width = GRID_WIDTH_COL_PLAYED_COUNT
      .Column(GRID_COLUMN_PLAYED_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Played Amount
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(393)
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(385)
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Width = GRID_WIDTH_COL_PLAYED_AMOUNT
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Average Bet
      .Column(GRID_COLUMN_AVERAGE_BET).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(393)
      .Column(GRID_COLUMN_AVERAGE_BET).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(347)
      .Column(GRID_COLUMN_AVERAGE_BET).Width = GRID_WIDTH_COL_AVERAGE_BET
      .Column(GRID_COLUMN_AVERAGE_BET).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Working Day Duration
      .Column(GRID_COLUMN_WORKING_DAY_DURATION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5723)
      .Column(GRID_COLUMN_WORKING_DAY_DURATION).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(382)
      .Column(GRID_COLUMN_WORKING_DAY_DURATION).Width = GRID_WIDTH_COL_DURATION
      .Column(GRID_COLUMN_WORKING_DAY_DURATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Working Day Played Count
      .Column(GRID_COLUMN_WORKING_DAY_PLAYED_COUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5723)
      .Column(GRID_COLUMN_WORKING_DAY_PLAYED_COUNT).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(261)
      .Column(GRID_COLUMN_WORKING_DAY_PLAYED_COUNT).Width = GRID_WIDTH_COL_PLAYED_COUNT
      .Column(GRID_COLUMN_WORKING_DAY_PLAYED_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Working Day Played Amount
      .Column(GRID_COLUMN_WORKING_DAY_PLAYED_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5723)
      .Column(GRID_COLUMN_WORKING_DAY_PLAYED_AMOUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(385)
      .Column(GRID_COLUMN_WORKING_DAY_PLAYED_AMOUNT).Width = GRID_WIDTH_COL_PLAYED_AMOUNT
      .Column(GRID_COLUMN_WORKING_DAY_PLAYED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Working Day Average Bet
      .Column(GRID_COLUMN_WORKING_DAY_AVERAGE_BET).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5723)
      .Column(GRID_COLUMN_WORKING_DAY_AVERAGE_BET).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(347)
      .Column(GRID_COLUMN_WORKING_DAY_AVERAGE_BET).Width = GRID_WIDTH_COL_AVERAGE_BET
      .Column(GRID_COLUMN_WORKING_DAY_AVERAGE_BET).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE : Set default values to filters
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub SetDefaultValues()

    ' Account
    uc_account_sel1.Clear()

    ' Level
    Me.chk_level_01.Checked = False
    Me.chk_level_02.Checked = False
    Me.chk_level_03.Checked = False
    Me.chk_level_04.Checked = False

    ' Option Session/Working Day
    Me.opt_session.Checked = True

    ' Duration
    dtp_duration.Enabled = True
    dtp_duration.Value = "00:00"

    ' Average Bet
    uc_entry_average_bet.Value = String.Empty

    ' Order By
    opt_account.Checked = True

    'Terminals list
    Call uc_pr_list.SetDefaultValues()

  End Sub ' SetDefaultValues

  ' PURPOSE: Save the selected Sessions Ids from the Grid.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub SaveSelectedGridIds()
    Dim _idx As Integer
    Dim _row_id As Integer

    If m_selected_ids IsNot Nothing Then
      m_selected_ids.Clear()
      m_selected_ids = Nothing
    End If

    m_selected_ids = New List(Of Integer)
    _idx = 0
    While _idx < Me.Grid.NumRows
      If Me.Grid.Row(_idx).IsSelected Then
        _row_id = New Integer
        If Integer.TryParse(Me.Grid.Cell(_idx, GRID_COLUMN_TERMINAL_ID).Value, _row_id) Then
          m_selected_ids.Add(_row_id)
        ElseIf _idx = Me.Grid.NumRows - 1 Then
          m_selected_ids.Add(-1) ' Total
        End If
      End If
      _idx = _idx + 1
    End While

  End Sub ' SaveSelectedIds

  ' PURPOSE: Select in the Grid the latest selected Alarm Ids that have been saved.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub SelectLastSelectedRow()
    Dim _idx As Integer
    Dim _any_selected As Boolean
    Dim _row_id As Integer

    If Me.Grid.NumRows = 0 Then
      Return
    End If
    If m_selected_ids Is Nothing Then
      Return
    End If
    If m_selected_ids.Count = 0 Then
      For _idx = 0 To Me.Grid.NumRows - 1
        If Me.Grid.Row(_idx).IsSelected Then
          Me.Grid.Row(_idx).IsSelected = False
          Call Me.Grid.RepaintRow(_idx)
        End If
      Next
      Return
    End If

    _any_selected = False
    Me.Grid.Redraw = False

    Try
      _idx = 0
      While _idx < Me.Grid.NumRows
        Me.Grid.Row(_idx).IsSelected = False
        Call Me.Grid.RepaintRow(_idx)
        _idx = _idx + 1
      End While

      _idx = 0
      While _idx < Me.Grid.NumRows
        If Not Integer.TryParse(Me.Grid.Cell(_idx, GRID_COLUMN_TERMINAL_ID).Value, _row_id) Then
          _idx = _idx + 1
          Continue While
        End If

        If m_selected_ids.Contains(_row_id) Then
          _any_selected = True
          Me.Grid.Row(_idx).IsSelected = True
          Call Me.Grid.RepaintRow(_idx)
        End If
        _idx = _idx + 1
      End While

      'Total
      If m_selected_ids.Contains(-1) Then
        _any_selected = True
        Me.Grid.Row(Me.Grid.NumRows - 1).IsSelected = True
        Call Me.Grid.RepaintRow(Me.Grid.NumRows - 1)
      End If

      If Not _any_selected And Me.Grid.CurrentRow >= 0 Then
        Me.Grid.Row(Me.Grid.CurrentRow).IsSelected = True
        Call Me.Grid.RepaintRow(Me.Grid.CurrentRow)
      End If

    Finally
      Me.Grid.Redraw = True
    End Try
  End Sub ' SelectLastSelectedRow

  ' PURPOSE: Get Level list for selected levels
  '          Format list to build query: X, X, X
  '          X is the Id if GetIds = True
  '          otherwise, X is the Name of the control
  '  PARAMS:
  '     - INPUT:
  '           - GetIds As Boolean
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Private Function GetLevelsSelected(Optional ByVal GetIds As Boolean = True) As String
    Dim list_type As String

    list_type = ""

    If Me.chk_level_01.Checked Then
      list_type = IIf(GetIds, "1", Me.chk_level_01.Text)
    End If

    If Me.chk_level_02.Checked Then
      If list_type.Length > 0 Then
        list_type = list_type & ", "
      End If
      list_type = list_type & IIf(GetIds, "2", Me.chk_level_02.Text)
    End If

    If Me.chk_level_03.Checked Then
      If list_type.Length > 0 Then
        list_type = list_type & ", "
      End If
      list_type = list_type & IIf(GetIds, "3", Me.chk_level_03.Text)
    End If

    If Me.chk_level_04.Checked Then
      If list_type.Length > 0 Then
        list_type = list_type & ", "
      End If
      list_type = list_type & IIf(GetIds, "4", Me.chk_level_04.Text)
    End If

    Return list_type
  End Function ' GetLevelsSelected

#End Region ' Private Functions

#Region " Events "

  Private Sub tmr_monitor_Tick(sender As Object, e As EventArgs) Handles tmr_monitor.Tick
    Call GUI_ExecuteQueryCustom()
  End Sub ' tmr_monitor_Tick

#End Region ' Events

  Private Sub panel_filter_Paint(sender As Object, e As PaintEventArgs) Handles panel_filter.Paint

  End Sub
End Class