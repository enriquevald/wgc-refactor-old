﻿
'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_winup_content_edit
' DESCRIPTION:   WinUp content edit
' AUTHOR:        Pablo Molina
' CREATION DATE: 19-ENE-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 02-MAR-2017  PDM    Initial version
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports WSI.Common
Imports GUI_Reports
Imports System.IO

Public Class frm_winup_content_edit
  Inherits frm_base_edit

#Region "Members"

  Private m_class_base As CLASS_WINUP_CONTENT
  Private m_sectionschematype As Integer
  Private m_winup_settings As CLASS_WINUP_SETTINGS

#End Region

#Region "Constructor"

  Public Sub New()
    Me.New(0)
  End Sub

  Public Sub New(ByVal SectionSchemaType As Integer)
    m_sectionschematype = SectionSchemaType
    m_class_base = New CLASS_WINUP_CONTENT()
    Me.InitializeComponent()
  End Sub

#End Region

#Region " Overrides "

  ' PURPOSE : Initializes the form id.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_WINUP_CONTENT_EDIT

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId


  ' PURPOSE : Form controls initialization.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS  :
  Protected Overrides Sub GUI_InitControls()

    Dim _imageMaxSize As Size

    Me.Text = CLASS_WINUP_CONTENT.GetSectionSchemaNameById(m_sectionschematype)

    chk_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(532)
    lbl_title.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7522)

    Me.ef_title.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 100)

    Me.lbl_description.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7935) ' Label description

    Me.lbl_description_detail.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7930)
    Me.ef_order.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7449)
    Me.ef_order.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)

    Me.tab_app_images.TabPages.Item(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7450) 'imagen de lista
    Me.tab_app_images.TabPages.Item(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7451) 'imagen de detalle

    Me.m_winup_settings = New CLASS_WINUP_SETTINGS
    _imageMaxSize = Me.m_winup_settings.GetImageMaxSize()
    Me.Uc_imageList.Init(_imageMaxSize.Height, _imageMaxSize.Width)
    Me.Uc_imageDetail.Init(_imageMaxSize.Height, _imageMaxSize.Width)

    Me.ef_order.TextVisible = True

    tab_html.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7567)
    tab_preview.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7568)
    chk_add_style.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7569)
    btn_insert_tag.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7570)

    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = (Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT)

    cmbTag.Items.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7571)) 'titulo 1
    cmbTag.Items.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7572)) 'titulo 2
    cmbTag.Items.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7573)) 'titulo 3
    cmbTag.Items.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7574)) 'div
    cmbTag.Items.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7575)) 'span
    cmbTag.Items.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7576)) 'list
    cmbTag.Items.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7577)) 'bold
    cmbTag.Items.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7578)) 'italic
    cmbTag.Items.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7579)) 'underline
    cmbTag.Items.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7580)) 'paragraph

    Call MyBase.GUI_InitControls()

  End Sub

  ' PURPOSE : Set initial data on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Me.ef_title.TextValue = m_class_base.Title
    Me.txt_message.Text = m_class_base.Description
    Me.chk_enabled.Checked = m_class_base.Status

    Me.txt_abstract.Text = m_class_base.DescriptionDetails

    Me.ef_order.Value = m_class_base.Order

    If Not IsNothing(m_class_base.DetailImage) Then
      Me.Uc_imageDetail.Image = m_class_base.DetailImage
    Else
      Me.Uc_imageDetail.Image = Nothing
    End If

    If Not IsNothing(m_class_base.ListImage) Then
      Me.Uc_imageList.Image = m_class_base.ListImage
    Else
      Me.Uc_imageList.Image = Nothing
    End If

  End Sub

  ' PURPOSE : Get data from the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_GetScreenData()

    m_class_base.Title = ef_title.Value
    m_class_base.Description = txt_message.Text
    m_class_base.Status = chk_enabled.Checked

    m_class_base.DescriptionDetails = Me.txt_abstract.Text.Replace(Chr(34), Chr(39))
    m_class_base.Order = IIf(String.IsNullOrEmpty(Me.ef_order.Value), 0, Convert.ToInt32(Me.ef_order.Value))

    If Not Me.Uc_imageDetail.Image Is Nothing Then
      m_class_base.DetailImage = Me.Uc_imageDetail.Image
    Else
      m_class_base.DetailImage = Nothing
    End If

    If Not Me.Uc_imageList.Image Is Nothing Then
      m_class_base.ListImage = Me.Uc_imageList.Image
    Else
      m_class_base.ListImage = Nothing
    End If

    m_class_base.SectionSchemaId = m_sectionschematype

  End Sub

  ' PURPOSE : Validate the data presented on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    If String.IsNullOrEmpty(Me.ef_title.Value) Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_title.Text)
      Call ef_title.Focus()

      Return False

    End If

    If (Me.Uc_imageList.Image IsNot Nothing) Then
      Dim _image_weight = Me.Uc_imageList.GetImageKBWeight()
      Dim _image_maxsize = Me.m_winup_settings.GetImageMaxWeightKB()

      If (_image_weight > 0) And (_image_weight > _image_maxsize) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7802), ENUM_MB_TYPE.MB_TYPE_WARNING, , , " " & _image_maxsize & " KB", , )
        Return False
      End If
    End If
    If (Me.Uc_imageDetail.Image IsNot Nothing) Then
      Dim _image_weight = Me.Uc_imageDetail.GetImageKBWeight()
      Dim _image_maxsize = Me.m_winup_settings.GetImageMaxWeightKB()

      If (_image_weight > 0) And (_image_weight > _image_maxsize) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7802), ENUM_MB_TYPE.MB_TYPE_WARNING, , , " " & _image_maxsize & " KB", , )
        Return False
      End If
    End If

    Return True
  End Function

  ' PURPOSE : Init form in edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '       - UserId
  '       - Username
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Overloads Sub ShowEditItem(ByVal AdsId As Integer)

    ' Sets the screen mode
    Me.DbEditedObject = m_class_base

    Call MyBase.ShowEditItem(AdsId)

    'Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
    'Me.DbEditedObject = m_class_base
    'Me.DbObjectId = AdsId


    ' ''Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    'Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    'Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    'Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    'If DbStatus = ENUM_STATUS.STATUS_OK Then
    '  Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    'End If

    'If DbStatus = ENUM_STATUS.STATUS_OK Then
    '  Call Me.Display(True)
    'End If
  End Sub ' ShowEditItem

  ' PURPOSE : Database overridable operations. 
  '           Define specific DB operation for this form.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        Me.DbEditedObject = m_class_base
        DbStatus = ENUM_STATUS.STATUS_OK
      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)

    Call MyBase.GUI_ButtonClick(ButtonId)

  End Sub

  ' PURPOSE : Override default behavior when user press key
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean
    Return False
  End Function

#End Region

#Region "Events"

  Private Sub cmbTag_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTag.SelectedIndexChanged
    chk_add_style.Checked = False
  End Sub

  Private Sub btn_insert_tag_Click(sender As Object, e As EventArgs) Handles btn_insert_tag.Click

    Select Case cmbTag.Text
      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(7571)
        AddControl("H1")

      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(7572)
        AddControl("H2")
      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(7573)
        AddControl("H3")
      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(7577)
        AddControl("B")
      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(7578)
        AddControl("I")

      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(7579)
        AddControl("U")
      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(7576)
        txt_abstract.SelectionColor = Color.DarkGreen
        If chk_add_style.Checked = True Then
          txt_abstract.AppendText(vbCrLf + "<UL style= ''>")
          txt_abstract.AppendText(vbCrLf + "<LI style= ''>")
          txt_abstract.SelectionColor = Color.Black
          txt_abstract.AppendText("item1")
          txt_abstract.SelectionColor = Color.DarkGreen
          txt_abstract.AppendText("</LI>")
          txt_abstract.AppendText(vbCrLf + "</UL>")
        Else
          txt_abstract.AppendText(vbCrLf + "<UL>")
          txt_abstract.AppendText(vbCrLf + "<LI>")
          txt_abstract.SelectionColor = Color.Black
          txt_abstract.AppendText("item2")
          txt_abstract.SelectionColor = Color.DarkGreen
          txt_abstract.AppendText("</LI>")
          txt_abstract.AppendText(vbCrLf + "</UL>")
        End If


      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(7574)
        AddControl("DIV")
      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(7575)
        AddControl("SPAN")
      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(7580)
        AddControl("P")
    End Select
  End Sub
  'SDS 27-08-2016 PBI 17065 html description
  Private Sub AddControl(control As String)
    txt_abstract.SelectionColor = Color.DarkGreen
    If chk_add_style.Checked = True Then
      txt_abstract.AppendText(vbCrLf + "<" + control + " style= '' " + ">")
    Else
      txt_abstract.AppendText(vbCrLf + "<" + control + ">")
    End If
    txt_abstract.SelectionColor = Color.Black
    txt_abstract.AppendText("texto")
    txt_abstract.SelectionColor = Color.DarkGreen
    txt_abstract.AppendText("</" + control + ">")
  End Sub

  Private Sub tab_control_HTML_SelectedIndexChanged(sender As Object, e As EventArgs) Handles tab_control_HTML.SelectedIndexChanged
    Dim current As TabPage = TryCast(sender, TabControl).SelectedTab
    If current.Name = "tab_preview" Then
      web_browser_html.DocumentText = vbCrLf + txt_abstract.Text
    End If
  End Sub

  Private Sub Uc_imageList_ImageSelected(ByRef [Continue] As Boolean, ImageSelected As Image) Handles Uc_imageList.ImageSelected
    m_class_base.ListImageName = String.Empty
  End Sub

  Private Sub Uc_imageDetail_ImageSelected(ByRef [Continue] As Boolean, ImageSelected As Image) Handles Uc_imageDetail.ImageSelected
    m_class_base.DetailImageName = String.Empty
  End Sub

#End Region

  

End Class