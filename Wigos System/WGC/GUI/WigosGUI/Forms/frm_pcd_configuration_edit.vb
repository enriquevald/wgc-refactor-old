'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_pcd_configuration_edit.vb
' DESCRIPTION:   Flags edition form
' AUTHOR:        Xavier Cots
' CREATION DATE: 27-AUG-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 27-AUG-2012  XCD    Initial version.
' 07-DEC-2015  ETP & JML Fixed bug TFS 7493
' 10-DEC-2015  ETP modified combo1 to Combobox filter
'--------------------------------------------------------------------

Option Explicit On

#Region " Imports "

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports WSI.Common

#End Region

Public Class frm_pcd_configuration_edit
  Inherits frm_base_edit

#Region " Constants "

  Private ReadOnly FIXED_INPUTS_COUNTERS_CODES As Integer() = {0, 1, 2, 3, 5, 6, 8, 9}
  Private ReadOnly FIXED_OUTPUTS_CODES As Integer() = {0, 1}
  ' Input controls
  Private Const INPUT_FIRST_CONTROL_METER As Integer = 1
  Private Const INPUT_LAST_CONTROL_METER As Integer = 8
  Private Const INPUT_FIRST_CONTROL_DOOR As Integer = 1
  Private Const INPUT_LAST_CONTROL_DOOR As Integer = 8
  Private Const EMPTY_VALUE_NOT_SELECT As Integer = -2

  Friend WithEvents tb_description As System.Windows.Forms.TextBox
  Friend WithEvents lbl_description As System.Windows.Forms.Label
  Friend WithEvents lbl_denom_right As System.Windows.Forms.Label
  Friend WithEvents lbl_port_right As System.Windows.Forms.Label
  Friend WithEvents lbl_counter_left As System.Windows.Forms.Label
  Friend WithEvents lbl_denom_left As System.Windows.Forms.Label
  Friend WithEvents lbl_port_left As System.Windows.Forms.Label
  Friend WithEvents lbl_counter_right As System.Windows.Forms.Label
  Friend WithEvents cmb_port_3 As GUI_Controls.uc_dual_combo
  Friend WithEvents cmb_port_2 As GUI_Controls.uc_dual_combo
  Friend WithEvents cmb_port_1 As GUI_Controls.uc_dual_combo
  Friend WithEvents cmb_port_6 As GUI_Controls.uc_dual_combo
  Friend WithEvents cmb_port_5 As GUI_Controls.uc_dual_combo
  Friend WithEvents cmb_port_4 As GUI_Controls.uc_dual_combo
  Friend WithEvents cmb_port_8 As GUI_Controls.uc_dual_combo
  Friend WithEvents cmb_port_7 As GUI_Controls.uc_dual_combo
  Friend WithEvents cmb_out_port_1 As GUI_Controls.uc_dual_combo
  Friend WithEvents cmb_out_port_2 As GUI_Controls.uc_dual_combo
  Friend WithEvents lbl_outputs As System.Windows.Forms.Label
  Friend WithEvents btn_cmb_out_2 As System.Windows.Forms.Button
  Friend WithEvents btn_cmb_out_1 As System.Windows.Forms.Button
  Friend WithEvents lbl_t0 As System.Windows.Forms.Label
  Friend WithEvents ef_cmb_out_2_t1 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_cmb_out_2_t0 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_cmb_out_1_t1 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_cmb_out_1_t0 As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_t1 As System.Windows.Forms.Label
  Friend WithEvents lbl_title_output As System.Windows.Forms.Label
  Friend WithEvents lbl_title_counters As System.Windows.Forms.Label
  Friend WithEvents lbl_output_mult As System.Windows.Forms.Label
  Friend WithEvents cmb_port_btn_8 As GUI_Controls.uc_dual_combo_with_button
  Friend WithEvents cmb_port_btn_7 As GUI_Controls.uc_dual_combo_with_button
  Friend WithEvents cmb_port_btn_6 As GUI_Controls.uc_dual_combo_with_button
  Friend WithEvents cmb_port_btn_5 As GUI_Controls.uc_dual_combo_with_button
  Friend WithEvents cmb_port_btn_4 As GUI_Controls.uc_dual_combo_with_button
  Friend WithEvents cmb_port_btn_3 As GUI_Controls.uc_dual_combo_with_button
  Friend WithEvents cmb_port_btn_2 As GUI_Controls.uc_dual_combo_with_button
  Friend WithEvents cmb_port_btn_1 As GUI_Controls.uc_dual_combo_with_button
  Friend WithEvents lbl_title_doors As System.Windows.Forms.Label
  Friend WithEvents lbl_output_port As System.Windows.Forms.Label

#End Region

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub
  Friend WithEvents ef_name As GUI_Controls.uc_entry_field

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.ef_name = New GUI_Controls.uc_entry_field
    Me.lbl_description = New System.Windows.Forms.Label
    Me.tb_description = New System.Windows.Forms.TextBox
    Me.lbl_port_left = New System.Windows.Forms.Label
    Me.lbl_denom_left = New System.Windows.Forms.Label
    Me.lbl_counter_left = New System.Windows.Forms.Label
    Me.lbl_denom_right = New System.Windows.Forms.Label
    Me.lbl_port_right = New System.Windows.Forms.Label
    Me.lbl_counter_right = New System.Windows.Forms.Label
    Me.cmb_port_1 = New GUI_Controls.uc_dual_combo
    Me.cmb_port_2 = New GUI_Controls.uc_dual_combo
    Me.cmb_port_3 = New GUI_Controls.uc_dual_combo
    Me.cmb_port_6 = New GUI_Controls.uc_dual_combo
    Me.cmb_port_5 = New GUI_Controls.uc_dual_combo
    Me.cmb_port_4 = New GUI_Controls.uc_dual_combo
    Me.cmb_port_8 = New GUI_Controls.uc_dual_combo
    Me.cmb_port_7 = New GUI_Controls.uc_dual_combo
    Me.cmb_out_port_1 = New GUI_Controls.uc_dual_combo
    Me.cmb_out_port_2 = New GUI_Controls.uc_dual_combo
    Me.lbl_outputs = New System.Windows.Forms.Label
    Me.btn_cmb_out_1 = New System.Windows.Forms.Button
    Me.btn_cmb_out_2 = New System.Windows.Forms.Button
    Me.ef_cmb_out_1_t0 = New GUI_Controls.uc_entry_field
    Me.ef_cmb_out_1_t1 = New GUI_Controls.uc_entry_field
    Me.ef_cmb_out_2_t1 = New GUI_Controls.uc_entry_field
    Me.ef_cmb_out_2_t0 = New GUI_Controls.uc_entry_field
    Me.lbl_t0 = New System.Windows.Forms.Label
    Me.lbl_t1 = New System.Windows.Forms.Label
    Me.lbl_title_counters = New System.Windows.Forms.Label
    Me.lbl_title_output = New System.Windows.Forms.Label
    Me.lbl_output_mult = New System.Windows.Forms.Label
    Me.lbl_output_port = New System.Windows.Forms.Label
    Me.cmb_port_btn_1 = New GUI_Controls.uc_dual_combo_with_button
    Me.cmb_port_btn_2 = New GUI_Controls.uc_dual_combo_with_button
    Me.cmb_port_btn_3 = New GUI_Controls.uc_dual_combo_with_button
    Me.cmb_port_btn_4 = New GUI_Controls.uc_dual_combo_with_button
    Me.cmb_port_btn_5 = New GUI_Controls.uc_dual_combo_with_button
    Me.cmb_port_btn_6 = New GUI_Controls.uc_dual_combo_with_button
    Me.cmb_port_btn_7 = New GUI_Controls.uc_dual_combo_with_button
    Me.cmb_port_btn_8 = New GUI_Controls.uc_dual_combo_with_button
    Me.lbl_title_doors = New System.Windows.Forms.Label
    Me.panel_data.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.lbl_title_doors)
    Me.panel_data.Controls.Add(Me.cmb_port_btn_8)
    Me.panel_data.Controls.Add(Me.cmb_port_btn_7)
    Me.panel_data.Controls.Add(Me.cmb_port_btn_6)
    Me.panel_data.Controls.Add(Me.cmb_port_btn_5)
    Me.panel_data.Controls.Add(Me.cmb_port_btn_4)
    Me.panel_data.Controls.Add(Me.cmb_port_btn_3)
    Me.panel_data.Controls.Add(Me.cmb_port_btn_2)
    Me.panel_data.Controls.Add(Me.cmb_port_btn_1)
    Me.panel_data.Controls.Add(Me.lbl_output_mult)
    Me.panel_data.Controls.Add(Me.lbl_output_port)
    Me.panel_data.Controls.Add(Me.lbl_title_output)
    Me.panel_data.Controls.Add(Me.lbl_title_counters)
    Me.panel_data.Controls.Add(Me.lbl_t1)
    Me.panel_data.Controls.Add(Me.lbl_t0)
    Me.panel_data.Controls.Add(Me.ef_cmb_out_2_t1)
    Me.panel_data.Controls.Add(Me.ef_cmb_out_2_t0)
    Me.panel_data.Controls.Add(Me.ef_cmb_out_1_t1)
    Me.panel_data.Controls.Add(Me.ef_cmb_out_1_t0)
    Me.panel_data.Controls.Add(Me.btn_cmb_out_2)
    Me.panel_data.Controls.Add(Me.btn_cmb_out_1)
    Me.panel_data.Controls.Add(Me.lbl_outputs)
    Me.panel_data.Controls.Add(Me.cmb_out_port_2)
    Me.panel_data.Controls.Add(Me.cmb_out_port_1)
    Me.panel_data.Controls.Add(Me.cmb_port_8)
    Me.panel_data.Controls.Add(Me.cmb_port_7)
    Me.panel_data.Controls.Add(Me.cmb_port_6)
    Me.panel_data.Controls.Add(Me.cmb_port_5)
    Me.panel_data.Controls.Add(Me.cmb_port_4)
    Me.panel_data.Controls.Add(Me.cmb_port_3)
    Me.panel_data.Controls.Add(Me.cmb_port_2)
    Me.panel_data.Controls.Add(Me.cmb_port_1)
    Me.panel_data.Controls.Add(Me.lbl_counter_right)
    Me.panel_data.Controls.Add(Me.lbl_denom_right)
    Me.panel_data.Controls.Add(Me.lbl_port_right)
    Me.panel_data.Controls.Add(Me.lbl_counter_left)
    Me.panel_data.Controls.Add(Me.lbl_denom_left)
    Me.panel_data.Controls.Add(Me.lbl_port_left)
    Me.panel_data.Controls.Add(Me.tb_description)
    Me.panel_data.Controls.Add(Me.lbl_description)
    Me.panel_data.Controls.Add(Me.ef_name)
    Me.panel_data.Location = New System.Drawing.Point(5, 7)
    Me.panel_data.Size = New System.Drawing.Size(802, 543)
    '
    'ef_name
    '
    Me.ef_name.DoubleValue = 0
    Me.ef_name.IntegerValue = 0
    Me.ef_name.IsReadOnly = False
    Me.ef_name.Location = New System.Drawing.Point(18, 8)
    Me.ef_name.Name = "ef_name"
    Me.ef_name.PlaceHolder = Nothing
    Me.ef_name.Size = New System.Drawing.Size(278, 24)
    Me.ef_name.SufixText = "Sufix Text"
    Me.ef_name.SufixTextVisible = True
    Me.ef_name.TabIndex = 0
    Me.ef_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_name.TextValue = ""
    Me.ef_name.Value = ""
    Me.ef_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_description
    '
    Me.lbl_description.AutoSize = True
    Me.lbl_description.Location = New System.Drawing.Point(349, 13)
    Me.lbl_description.Name = "lbl_description"
    Me.lbl_description.Size = New System.Drawing.Size(78, 13)
    Me.lbl_description.TabIndex = 17
    Me.lbl_description.Text = "xDescription"
    '
    'tb_description
    '
    Me.tb_description.Location = New System.Drawing.Point(429, 10)
    Me.tb_description.Multiline = True
    Me.tb_description.Name = "tb_description"
    Me.tb_description.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.tb_description.Size = New System.Drawing.Size(359, 62)
    Me.tb_description.TabIndex = 1
    '
    'lbl_port_left
    '
    Me.lbl_port_left.Location = New System.Drawing.Point(346, 106)
    Me.lbl_port_left.Name = "lbl_port_left"
    Me.lbl_port_left.Size = New System.Drawing.Size(51, 13)
    Me.lbl_port_left.TabIndex = 19
    Me.lbl_port_left.Text = "xPuerto"
    '
    'lbl_denom_left
    '
    Me.lbl_denom_left.Location = New System.Drawing.Point(235, 106)
    Me.lbl_denom_left.Name = "lbl_denom_left"
    Me.lbl_denom_left.Size = New System.Drawing.Size(102, 13)
    Me.lbl_denom_left.TabIndex = 20
    Me.lbl_denom_left.Text = "xMultiplicador"
    Me.lbl_denom_left.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_counter_left
    '
    Me.lbl_counter_left.AutoSize = True
    Me.lbl_counter_left.Location = New System.Drawing.Point(12, 106)
    Me.lbl_counter_left.Name = "lbl_counter_left"
    Me.lbl_counter_left.Size = New System.Drawing.Size(67, 13)
    Me.lbl_counter_left.TabIndex = 21
    Me.lbl_counter_left.Text = "xContador"
    '
    'lbl_denom_right
    '
    Me.lbl_denom_right.Location = New System.Drawing.Point(648, 106)
    Me.lbl_denom_right.Name = "lbl_denom_right"
    Me.lbl_denom_right.Size = New System.Drawing.Size(69, 13)
    Me.lbl_denom_right.TabIndex = 23
    Me.lbl_denom_right.Text = "xAbierta"
    Me.lbl_denom_right.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_port_right
    '
    Me.lbl_port_right.Location = New System.Drawing.Point(733, 106)
    Me.lbl_port_right.Name = "lbl_port_right"
    Me.lbl_port_right.Size = New System.Drawing.Size(51, 13)
    Me.lbl_port_right.TabIndex = 22
    Me.lbl_port_right.Text = "xPuerto"
    '
    'lbl_counter_right
    '
    Me.lbl_counter_right.AutoSize = True
    Me.lbl_counter_right.Location = New System.Drawing.Point(418, 106)
    Me.lbl_counter_right.Name = "lbl_counter_right"
    Me.lbl_counter_right.Size = New System.Drawing.Size(55, 13)
    Me.lbl_counter_right.TabIndex = 24
    Me.lbl_counter_right.Text = "xAlarma"
    '
    'cmb_port_1
    '
    Me.cmb_port_1.BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_1.Checkbox1Checked = False
    Me.cmb_port_1.Combo1AutoCompleteMode = False
    Me.cmb_port_1.Combo1BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_1.Combo1IsReadOnly = False
    Me.cmb_port_1.Combo1TextValue = Nothing
    Me.cmb_port_1.Combo1Value = 0
    Me.cmb_port_1.Combo2AutoCompleteMode = False
    Me.cmb_port_1.Combo2BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_1.Combo2IsReadOnly = False
    Me.cmb_port_1.Combo2TextValue = Nothing
    Me.cmb_port_1.Combo2Value = 0
    Me.cmb_port_1.ControlMode = 0
    Me.cmb_port_1.EntryField1Enabled = False
    Me.cmb_port_1.EntryField1ReadOnly = False
    Me.cmb_port_1.EntryField1Text = "Text"
    Me.cmb_port_1.EntryField1Value = ""
    Me.cmb_port_1.Location = New System.Drawing.Point(3, 125)
    Me.cmb_port_1.Name = "cmb_port_1"
    Me.cmb_port_1.Size = New System.Drawing.Size(400, 31)
    Me.cmb_port_1.TabIndex = 2
    '
    'cmb_port_2
    '
    Me.cmb_port_2.BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_2.Checkbox1Checked = False
    Me.cmb_port_2.Combo1AutoCompleteMode = False
    Me.cmb_port_2.Combo1BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_2.Combo1IsReadOnly = False
    Me.cmb_port_2.Combo1TextValue = Nothing
    Me.cmb_port_2.Combo1Value = 0
    Me.cmb_port_2.Combo2AutoCompleteMode = False
    Me.cmb_port_2.Combo2BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_2.Combo2IsReadOnly = False
    Me.cmb_port_2.Combo2TextValue = Nothing
    Me.cmb_port_2.Combo2Value = 0
    Me.cmb_port_2.ControlMode = 0
    Me.cmb_port_2.EntryField1Enabled = False
    Me.cmb_port_2.EntryField1ReadOnly = False
    Me.cmb_port_2.EntryField1Text = "Text"
    Me.cmb_port_2.EntryField1Value = ""
    Me.cmb_port_2.Location = New System.Drawing.Point(3, 162)
    Me.cmb_port_2.Name = "cmb_port_2"
    Me.cmb_port_2.Size = New System.Drawing.Size(400, 31)
    Me.cmb_port_2.TabIndex = 3
    '
    'cmb_port_3
    '
    Me.cmb_port_3.BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_3.Checkbox1Checked = False
    Me.cmb_port_3.Combo1AutoCompleteMode = False
    Me.cmb_port_3.Combo1BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_3.Combo1IsReadOnly = False
    Me.cmb_port_3.Combo1TextValue = Nothing
    Me.cmb_port_3.Combo1Value = 0
    Me.cmb_port_3.Combo2AutoCompleteMode = False
    Me.cmb_port_3.Combo2BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_3.Combo2IsReadOnly = False
    Me.cmb_port_3.Combo2TextValue = Nothing
    Me.cmb_port_3.Combo2Value = 0
    Me.cmb_port_3.ControlMode = 0
    Me.cmb_port_3.EntryField1Enabled = False
    Me.cmb_port_3.EntryField1ReadOnly = False
    Me.cmb_port_3.EntryField1Text = "Text"
    Me.cmb_port_3.EntryField1Value = ""
    Me.cmb_port_3.Location = New System.Drawing.Point(3, 199)
    Me.cmb_port_3.Name = "cmb_port_3"
    Me.cmb_port_3.Size = New System.Drawing.Size(400, 31)
    Me.cmb_port_3.TabIndex = 4
    '
    'cmb_port_6
    '
    Me.cmb_port_6.BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_6.Checkbox1Checked = False
    Me.cmb_port_6.Combo1AutoCompleteMode = False
    Me.cmb_port_6.Combo1BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_6.Combo1IsReadOnly = False
    Me.cmb_port_6.Combo1TextValue = Nothing
    Me.cmb_port_6.Combo1Value = 0
    Me.cmb_port_6.Combo2AutoCompleteMode = False
    Me.cmb_port_6.Combo2BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_6.Combo2IsReadOnly = False
    Me.cmb_port_6.Combo2TextValue = Nothing
    Me.cmb_port_6.Combo2Value = 0
    Me.cmb_port_6.ControlMode = 0
    Me.cmb_port_6.EntryField1Enabled = False
    Me.cmb_port_6.EntryField1ReadOnly = False
    Me.cmb_port_6.EntryField1Text = "Text"
    Me.cmb_port_6.EntryField1Value = ""
    Me.cmb_port_6.Location = New System.Drawing.Point(3, 310)
    Me.cmb_port_6.Name = "cmb_port_6"
    Me.cmb_port_6.Size = New System.Drawing.Size(400, 31)
    Me.cmb_port_6.TabIndex = 7
    '
    'cmb_port_5
    '
    Me.cmb_port_5.BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_5.Checkbox1Checked = False
    Me.cmb_port_5.Combo1AutoCompleteMode = False
    Me.cmb_port_5.Combo1BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_5.Combo1IsReadOnly = False
    Me.cmb_port_5.Combo1TextValue = Nothing
    Me.cmb_port_5.Combo1Value = 0
    Me.cmb_port_5.Combo2AutoCompleteMode = False
    Me.cmb_port_5.Combo2BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_5.Combo2IsReadOnly = False
    Me.cmb_port_5.Combo2TextValue = Nothing
    Me.cmb_port_5.Combo2Value = 0
    Me.cmb_port_5.ControlMode = 0
    Me.cmb_port_5.EntryField1Enabled = False
    Me.cmb_port_5.EntryField1ReadOnly = False
    Me.cmb_port_5.EntryField1Text = "Text"
    Me.cmb_port_5.EntryField1Value = ""
    Me.cmb_port_5.Location = New System.Drawing.Point(3, 273)
    Me.cmb_port_5.Name = "cmb_port_5"
    Me.cmb_port_5.Size = New System.Drawing.Size(400, 31)
    Me.cmb_port_5.TabIndex = 6
    '
    'cmb_port_4
    '
    Me.cmb_port_4.BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_4.Checkbox1Checked = False
    Me.cmb_port_4.Combo1AutoCompleteMode = False
    Me.cmb_port_4.Combo1BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_4.Combo1IsReadOnly = False
    Me.cmb_port_4.Combo1TextValue = Nothing
    Me.cmb_port_4.Combo1Value = 0
    Me.cmb_port_4.Combo2AutoCompleteMode = False
    Me.cmb_port_4.Combo2BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_4.Combo2IsReadOnly = False
    Me.cmb_port_4.Combo2TextValue = Nothing
    Me.cmb_port_4.Combo2Value = 0
    Me.cmb_port_4.ControlMode = 0
    Me.cmb_port_4.EntryField1Enabled = False
    Me.cmb_port_4.EntryField1ReadOnly = False
    Me.cmb_port_4.EntryField1Text = "Text"
    Me.cmb_port_4.EntryField1Value = ""
    Me.cmb_port_4.Location = New System.Drawing.Point(3, 236)
    Me.cmb_port_4.Name = "cmb_port_4"
    Me.cmb_port_4.Size = New System.Drawing.Size(400, 31)
    Me.cmb_port_4.TabIndex = 5
    '
    'cmb_port_8
    '
    Me.cmb_port_8.BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_8.Checkbox1Checked = False
    Me.cmb_port_8.Combo1AutoCompleteMode = False
    Me.cmb_port_8.Combo1BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_8.Combo1IsReadOnly = False
    Me.cmb_port_8.Combo1TextValue = Nothing
    Me.cmb_port_8.Combo1Value = 0
    Me.cmb_port_8.Combo2AutoCompleteMode = False
    Me.cmb_port_8.Combo2BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_8.Combo2IsReadOnly = False
    Me.cmb_port_8.Combo2TextValue = Nothing
    Me.cmb_port_8.Combo2Value = 0
    Me.cmb_port_8.ControlMode = 0
    Me.cmb_port_8.EntryField1Enabled = False
    Me.cmb_port_8.EntryField1ReadOnly = False
    Me.cmb_port_8.EntryField1Text = "Text"
    Me.cmb_port_8.EntryField1Value = ""
    Me.cmb_port_8.Location = New System.Drawing.Point(3, 384)
    Me.cmb_port_8.Name = "cmb_port_8"
    Me.cmb_port_8.Size = New System.Drawing.Size(400, 31)
    Me.cmb_port_8.TabIndex = 9
    '
    'cmb_port_7
    '
    Me.cmb_port_7.BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_7.Checkbox1Checked = False
    Me.cmb_port_7.Combo1AutoCompleteMode = False
    Me.cmb_port_7.Combo1BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_7.Combo1IsReadOnly = False
    Me.cmb_port_7.Combo1TextValue = Nothing
    Me.cmb_port_7.Combo1Value = 0
    Me.cmb_port_7.Combo2AutoCompleteMode = False
    Me.cmb_port_7.Combo2BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_7.Combo2IsReadOnly = False
    Me.cmb_port_7.Combo2TextValue = Nothing
    Me.cmb_port_7.Combo2Value = 0
    Me.cmb_port_7.ControlMode = 0
    Me.cmb_port_7.EntryField1Enabled = False
    Me.cmb_port_7.EntryField1ReadOnly = False
    Me.cmb_port_7.EntryField1Text = "Text"
    Me.cmb_port_7.EntryField1Value = ""
    Me.cmb_port_7.Location = New System.Drawing.Point(3, 347)
    Me.cmb_port_7.Name = "cmb_port_7"
    Me.cmb_port_7.Size = New System.Drawing.Size(400, 31)
    Me.cmb_port_7.TabIndex = 8
    '
    'cmb_out_port_1
    '
    Me.cmb_out_port_1.BackColor = System.Drawing.SystemColors.Control
    Me.cmb_out_port_1.Checkbox1Checked = False
    Me.cmb_out_port_1.Combo1AutoCompleteMode = False
    Me.cmb_out_port_1.Combo1BackColor = System.Drawing.SystemColors.Control
    Me.cmb_out_port_1.Combo1IsReadOnly = False
    Me.cmb_out_port_1.Combo1TextValue = Nothing
    Me.cmb_out_port_1.Combo1Value = 0
    Me.cmb_out_port_1.Combo2AutoCompleteMode = False
    Me.cmb_out_port_1.Combo2BackColor = System.Drawing.SystemColors.Control
    Me.cmb_out_port_1.Combo2IsReadOnly = False
    Me.cmb_out_port_1.Combo2TextValue = Nothing
    Me.cmb_out_port_1.Combo2Value = 0
    Me.cmb_out_port_1.ControlMode = 0
    Me.cmb_out_port_1.EntryField1Enabled = False
    Me.cmb_out_port_1.EntryField1ReadOnly = False
    Me.cmb_out_port_1.EntryField1Text = "Text"
    Me.cmb_out_port_1.EntryField1Value = ""
    Me.cmb_out_port_1.Location = New System.Drawing.Point(3, 467)
    Me.cmb_out_port_1.Name = "cmb_out_port_1"
    Me.cmb_out_port_1.Size = New System.Drawing.Size(400, 30)
    Me.cmb_out_port_1.TabIndex = 18
    '
    'cmb_out_port_2
    '
    Me.cmb_out_port_2.BackColor = System.Drawing.SystemColors.Control
    Me.cmb_out_port_2.Checkbox1Checked = False
    Me.cmb_out_port_2.Combo1AutoCompleteMode = False
    Me.cmb_out_port_2.Combo1BackColor = System.Drawing.SystemColors.Control
    Me.cmb_out_port_2.Combo1IsReadOnly = False
    Me.cmb_out_port_2.Combo1TextValue = Nothing
    Me.cmb_out_port_2.Combo1Value = 0
    Me.cmb_out_port_2.Combo2AutoCompleteMode = False
    Me.cmb_out_port_2.Combo2BackColor = System.Drawing.SystemColors.Control
    Me.cmb_out_port_2.Combo2IsReadOnly = False
    Me.cmb_out_port_2.Combo2TextValue = Nothing
    Me.cmb_out_port_2.Combo2Value = 0
    Me.cmb_out_port_2.ControlMode = 0
    Me.cmb_out_port_2.EntryField1Enabled = False
    Me.cmb_out_port_2.EntryField1ReadOnly = False
    Me.cmb_out_port_2.EntryField1Text = "Text"
    Me.cmb_out_port_2.EntryField1Value = ""
    Me.cmb_out_port_2.Location = New System.Drawing.Point(3, 505)
    Me.cmb_out_port_2.Name = "cmb_out_port_2"
    Me.cmb_out_port_2.Size = New System.Drawing.Size(400, 30)
    Me.cmb_out_port_2.TabIndex = 21
    '
    'lbl_outputs
    '
    Me.lbl_outputs.AutoSize = True
    Me.lbl_outputs.Location = New System.Drawing.Point(9, 450)
    Me.lbl_outputs.Name = "lbl_outputs"
    Me.lbl_outputs.Size = New System.Drawing.Size(55, 13)
    Me.lbl_outputs.TabIndex = 28
    Me.lbl_outputs.Text = "xSalidas"
    '
    'btn_cmb_out_1
    '
    Me.btn_cmb_out_1.BackgroundImage = Global.WigosGUI.My.Resources.Resources.PulseUp
    Me.btn_cmb_out_1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
    Me.btn_cmb_out_1.Location = New System.Drawing.Point(411, 466)
    Me.btn_cmb_out_1.Name = "btn_cmb_out_1"
    Me.btn_cmb_out_1.Size = New System.Drawing.Size(75, 32)
    Me.btn_cmb_out_1.TabIndex = 29
    Me.btn_cmb_out_1.TabStop = False
    Me.btn_cmb_out_1.Tag = ""
    Me.btn_cmb_out_1.UseVisualStyleBackColor = True
    '
    'btn_cmb_out_2
    '
    Me.btn_cmb_out_2.BackgroundImage = Global.WigosGUI.My.Resources.Resources.PulseUp
    Me.btn_cmb_out_2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
    Me.btn_cmb_out_2.Location = New System.Drawing.Point(411, 504)
    Me.btn_cmb_out_2.Name = "btn_cmb_out_2"
    Me.btn_cmb_out_2.Size = New System.Drawing.Size(75, 32)
    Me.btn_cmb_out_2.TabIndex = 30
    Me.btn_cmb_out_2.TabStop = False
    Me.btn_cmb_out_2.Tag = "1"
    Me.btn_cmb_out_2.UseVisualStyleBackColor = True
    '
    'ef_cmb_out_1_t0
    '
    Me.ef_cmb_out_1_t0.DoubleValue = 0
    Me.ef_cmb_out_1_t0.IntegerValue = 0
    Me.ef_cmb_out_1_t0.IsReadOnly = False
    Me.ef_cmb_out_1_t0.Location = New System.Drawing.Point(492, 470)
    Me.ef_cmb_out_1_t0.Name = "ef_cmb_out_1_t0"
    Me.ef_cmb_out_1_t0.PlaceHolder = Nothing
    Me.ef_cmb_out_1_t0.Size = New System.Drawing.Size(120, 24)
    Me.ef_cmb_out_1_t0.SufixText = "Sufix Text"
    Me.ef_cmb_out_1_t0.SufixTextVisible = True
    Me.ef_cmb_out_1_t0.TabIndex = 19
    Me.ef_cmb_out_1_t0.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_cmb_out_1_t0.TextValue = ""
    Me.ef_cmb_out_1_t0.TextWidth = 0
    Me.ef_cmb_out_1_t0.Value = ""
    Me.ef_cmb_out_1_t0.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_cmb_out_1_t1
    '
    Me.ef_cmb_out_1_t1.DoubleValue = 0
    Me.ef_cmb_out_1_t1.IntegerValue = 0
    Me.ef_cmb_out_1_t1.IsReadOnly = False
    Me.ef_cmb_out_1_t1.Location = New System.Drawing.Point(618, 470)
    Me.ef_cmb_out_1_t1.Name = "ef_cmb_out_1_t1"
    Me.ef_cmb_out_1_t1.PlaceHolder = Nothing
    Me.ef_cmb_out_1_t1.Size = New System.Drawing.Size(120, 24)
    Me.ef_cmb_out_1_t1.SufixText = "Sufix Text"
    Me.ef_cmb_out_1_t1.SufixTextVisible = True
    Me.ef_cmb_out_1_t1.TabIndex = 20
    Me.ef_cmb_out_1_t1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_cmb_out_1_t1.TextValue = ""
    Me.ef_cmb_out_1_t1.TextWidth = 0
    Me.ef_cmb_out_1_t1.Value = ""
    Me.ef_cmb_out_1_t1.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_cmb_out_2_t1
    '
    Me.ef_cmb_out_2_t1.DoubleValue = 0
    Me.ef_cmb_out_2_t1.IntegerValue = 0
    Me.ef_cmb_out_2_t1.IsReadOnly = False
    Me.ef_cmb_out_2_t1.Location = New System.Drawing.Point(618, 508)
    Me.ef_cmb_out_2_t1.Name = "ef_cmb_out_2_t1"
    Me.ef_cmb_out_2_t1.PlaceHolder = Nothing
    Me.ef_cmb_out_2_t1.Size = New System.Drawing.Size(120, 24)
    Me.ef_cmb_out_2_t1.SufixText = "Sufix Text"
    Me.ef_cmb_out_2_t1.SufixTextVisible = True
    Me.ef_cmb_out_2_t1.TabIndex = 23
    Me.ef_cmb_out_2_t1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_cmb_out_2_t1.TextValue = ""
    Me.ef_cmb_out_2_t1.TextWidth = 0
    Me.ef_cmb_out_2_t1.Value = ""
    Me.ef_cmb_out_2_t1.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_cmb_out_2_t0
    '
    Me.ef_cmb_out_2_t0.DoubleValue = 0
    Me.ef_cmb_out_2_t0.IntegerValue = 0
    Me.ef_cmb_out_2_t0.IsReadOnly = False
    Me.ef_cmb_out_2_t0.Location = New System.Drawing.Point(492, 508)
    Me.ef_cmb_out_2_t0.Name = "ef_cmb_out_2_t0"
    Me.ef_cmb_out_2_t0.PlaceHolder = Nothing
    Me.ef_cmb_out_2_t0.Size = New System.Drawing.Size(120, 24)
    Me.ef_cmb_out_2_t0.SufixText = "Sufix Text"
    Me.ef_cmb_out_2_t0.SufixTextVisible = True
    Me.ef_cmb_out_2_t0.TabIndex = 22
    Me.ef_cmb_out_2_t0.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_cmb_out_2_t0.TextValue = ""
    Me.ef_cmb_out_2_t0.TextWidth = 0
    Me.ef_cmb_out_2_t0.Value = ""
    Me.ef_cmb_out_2_t0.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_t0
    '
    Me.lbl_t0.Location = New System.Drawing.Point(492, 450)
    Me.lbl_t0.Name = "lbl_t0"
    Me.lbl_t0.Size = New System.Drawing.Size(120, 13)
    Me.lbl_t0.TabIndex = 35
    Me.lbl_t0.Text = "T0 (ms)"
    Me.lbl_t0.TextAlign = System.Drawing.ContentAlignment.TopCenter
    '
    'lbl_t1
    '
    Me.lbl_t1.Location = New System.Drawing.Point(618, 450)
    Me.lbl_t1.Name = "lbl_t1"
    Me.lbl_t1.Size = New System.Drawing.Size(120, 13)
    Me.lbl_t1.TabIndex = 36
    Me.lbl_t1.Text = "T1 (ms)"
    Me.lbl_t1.TextAlign = System.Drawing.ContentAlignment.TopCenter
    '
    'lbl_title_counters
    '
    Me.lbl_title_counters.BackColor = System.Drawing.Color.DarkSeaGreen
    Me.lbl_title_counters.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_title_counters.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
    Me.lbl_title_counters.Location = New System.Drawing.Point(0, 80)
    Me.lbl_title_counters.Name = "lbl_title_counters"
    Me.lbl_title_counters.Size = New System.Drawing.Size(403, 20)
    Me.lbl_title_counters.TabIndex = 37
    Me.lbl_title_counters.Text = "INPUT COUNTERS"
    Me.lbl_title_counters.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_title_output
    '
    Me.lbl_title_output.BackColor = System.Drawing.Color.SandyBrown
    Me.lbl_title_output.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_title_output.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
    Me.lbl_title_output.Location = New System.Drawing.Point(0, 424)
    Me.lbl_title_output.Name = "lbl_title_output"
    Me.lbl_title_output.Size = New System.Drawing.Size(801, 20)
    Me.lbl_title_output.TabIndex = 38
    Me.lbl_title_output.Text = "OUTPUT"
    Me.lbl_title_output.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_output_mult
    '
    Me.lbl_output_mult.Location = New System.Drawing.Point(232, 450)
    Me.lbl_output_mult.Name = "lbl_output_mult"
    Me.lbl_output_mult.Size = New System.Drawing.Size(102, 13)
    Me.lbl_output_mult.TabIndex = 40
    Me.lbl_output_mult.Text = "xMultiplicador"
    Me.lbl_output_mult.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_output_port
    '
    Me.lbl_output_port.Location = New System.Drawing.Point(343, 450)
    Me.lbl_output_port.Name = "lbl_output_port"
    Me.lbl_output_port.Size = New System.Drawing.Size(51, 13)
    Me.lbl_output_port.TabIndex = 39
    Me.lbl_output_port.Text = "xPuerto"
    '
    'cmb_port_btn_1
    '
    Me.cmb_port_btn_1.BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_btn_1.Button1BackImage = Nothing
    Me.cmb_port_btn_1.Button1Tag = ""
    Me.cmb_port_btn_1.Checkbox1Checked = False
    Me.cmb_port_btn_1.Combo1AutoCompleteMode = False
    Me.cmb_port_btn_1.Combo1BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_btn_1.Combo1IsReadOnly = False
    Me.cmb_port_btn_1.Combo1TextValue = Nothing
    Me.cmb_port_btn_1.Combo1Value = 0
    Me.cmb_port_btn_1.Combo2AutoCompleteMode = False
    Me.cmb_port_btn_1.Combo2BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_btn_1.Combo2IsReadOnly = False
    Me.cmb_port_btn_1.Combo2TextValue = Nothing
    Me.cmb_port_btn_1.Combo2Value = 0
    Me.cmb_port_btn_1.ControlMode = 0
    Me.cmb_port_btn_1.Location = New System.Drawing.Point(409, 122)
    Me.cmb_port_btn_1.Name = "cmb_port_btn_1"
    Me.cmb_port_btn_1.Size = New System.Drawing.Size(383, 37)
    Me.cmb_port_btn_1.TabIndex = 10
    '
    'cmb_port_btn_2
    '
    Me.cmb_port_btn_2.BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_btn_2.Button1BackImage = Nothing
    Me.cmb_port_btn_2.Button1Tag = ""
    Me.cmb_port_btn_2.Checkbox1Checked = False
    Me.cmb_port_btn_2.Combo1AutoCompleteMode = False
    Me.cmb_port_btn_2.Combo1BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_btn_2.Combo1IsReadOnly = False
    Me.cmb_port_btn_2.Combo1TextValue = Nothing
    Me.cmb_port_btn_2.Combo1Value = 0
    Me.cmb_port_btn_2.Combo2AutoCompleteMode = False
    Me.cmb_port_btn_2.Combo2BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_btn_2.Combo2IsReadOnly = False
    Me.cmb_port_btn_2.Combo2TextValue = Nothing
    Me.cmb_port_btn_2.Combo2Value = 0
    Me.cmb_port_btn_2.ControlMode = 0
    Me.cmb_port_btn_2.Location = New System.Drawing.Point(409, 159)
    Me.cmb_port_btn_2.Name = "cmb_port_btn_2"
    Me.cmb_port_btn_2.Size = New System.Drawing.Size(383, 37)
    Me.cmb_port_btn_2.TabIndex = 11
    '
    'cmb_port_btn_3
    '
    Me.cmb_port_btn_3.BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_btn_3.Button1BackImage = Nothing
    Me.cmb_port_btn_3.Button1Tag = ""
    Me.cmb_port_btn_3.Checkbox1Checked = False
    Me.cmb_port_btn_3.Combo1AutoCompleteMode = False
    Me.cmb_port_btn_3.Combo1BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_btn_3.Combo1IsReadOnly = False
    Me.cmb_port_btn_3.Combo1TextValue = Nothing
    Me.cmb_port_btn_3.Combo1Value = 0
    Me.cmb_port_btn_3.Combo2AutoCompleteMode = False
    Me.cmb_port_btn_3.Combo2BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_btn_3.Combo2IsReadOnly = False
    Me.cmb_port_btn_3.Combo2TextValue = Nothing
    Me.cmb_port_btn_3.Combo2Value = 0
    Me.cmb_port_btn_3.ControlMode = 0
    Me.cmb_port_btn_3.Location = New System.Drawing.Point(409, 196)
    Me.cmb_port_btn_3.Name = "cmb_port_btn_3"
    Me.cmb_port_btn_3.Size = New System.Drawing.Size(383, 37)
    Me.cmb_port_btn_3.TabIndex = 12
    '
    'cmb_port_btn_4
    '
    Me.cmb_port_btn_4.BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_btn_4.Button1BackImage = Nothing
    Me.cmb_port_btn_4.Button1Tag = ""
    Me.cmb_port_btn_4.Checkbox1Checked = False
    Me.cmb_port_btn_4.Combo1AutoCompleteMode = False
    Me.cmb_port_btn_4.Combo1BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_btn_4.Combo1IsReadOnly = False
    Me.cmb_port_btn_4.Combo1TextValue = Nothing
    Me.cmb_port_btn_4.Combo1Value = 0
    Me.cmb_port_btn_4.Combo2AutoCompleteMode = False
    Me.cmb_port_btn_4.Combo2BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_btn_4.Combo2IsReadOnly = False
    Me.cmb_port_btn_4.Combo2TextValue = Nothing
    Me.cmb_port_btn_4.Combo2Value = 0
    Me.cmb_port_btn_4.ControlMode = 0
    Me.cmb_port_btn_4.Location = New System.Drawing.Point(409, 233)
    Me.cmb_port_btn_4.Name = "cmb_port_btn_4"
    Me.cmb_port_btn_4.Size = New System.Drawing.Size(383, 37)
    Me.cmb_port_btn_4.TabIndex = 13
    '
    'cmb_port_btn_5
    '
    Me.cmb_port_btn_5.BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_btn_5.Button1BackImage = Nothing
    Me.cmb_port_btn_5.Button1Tag = ""
    Me.cmb_port_btn_5.Checkbox1Checked = False
    Me.cmb_port_btn_5.Combo1AutoCompleteMode = False
    Me.cmb_port_btn_5.Combo1BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_btn_5.Combo1IsReadOnly = False
    Me.cmb_port_btn_5.Combo1TextValue = Nothing
    Me.cmb_port_btn_5.Combo1Value = 0
    Me.cmb_port_btn_5.Combo2AutoCompleteMode = False
    Me.cmb_port_btn_5.Combo2BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_btn_5.Combo2IsReadOnly = False
    Me.cmb_port_btn_5.Combo2TextValue = Nothing
    Me.cmb_port_btn_5.Combo2Value = 0
    Me.cmb_port_btn_5.ControlMode = 0
    Me.cmb_port_btn_5.Location = New System.Drawing.Point(409, 270)
    Me.cmb_port_btn_5.Name = "cmb_port_btn_5"
    Me.cmb_port_btn_5.Size = New System.Drawing.Size(383, 37)
    Me.cmb_port_btn_5.TabIndex = 14
    '
    'cmb_port_btn_6
    '
    Me.cmb_port_btn_6.BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_btn_6.Button1BackImage = Nothing
    Me.cmb_port_btn_6.Button1Tag = ""
    Me.cmb_port_btn_6.Checkbox1Checked = False
    Me.cmb_port_btn_6.Combo1AutoCompleteMode = False
    Me.cmb_port_btn_6.Combo1BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_btn_6.Combo1IsReadOnly = False
    Me.cmb_port_btn_6.Combo1TextValue = Nothing
    Me.cmb_port_btn_6.Combo1Value = 0
    Me.cmb_port_btn_6.Combo2AutoCompleteMode = False
    Me.cmb_port_btn_6.Combo2BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_btn_6.Combo2IsReadOnly = False
    Me.cmb_port_btn_6.Combo2TextValue = Nothing
    Me.cmb_port_btn_6.Combo2Value = 0
    Me.cmb_port_btn_6.ControlMode = 0
    Me.cmb_port_btn_6.Location = New System.Drawing.Point(409, 307)
    Me.cmb_port_btn_6.Name = "cmb_port_btn_6"
    Me.cmb_port_btn_6.Size = New System.Drawing.Size(383, 37)
    Me.cmb_port_btn_6.TabIndex = 15
    '
    'cmb_port_btn_7
    '
    Me.cmb_port_btn_7.BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_btn_7.Button1BackImage = Nothing
    Me.cmb_port_btn_7.Button1Tag = ""
    Me.cmb_port_btn_7.Checkbox1Checked = False
    Me.cmb_port_btn_7.Combo1AutoCompleteMode = False
    Me.cmb_port_btn_7.Combo1BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_btn_7.Combo1IsReadOnly = False
    Me.cmb_port_btn_7.Combo1TextValue = Nothing
    Me.cmb_port_btn_7.Combo1Value = 0
    Me.cmb_port_btn_7.Combo2AutoCompleteMode = False
    Me.cmb_port_btn_7.Combo2BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_btn_7.Combo2IsReadOnly = False
    Me.cmb_port_btn_7.Combo2TextValue = Nothing
    Me.cmb_port_btn_7.Combo2Value = 0
    Me.cmb_port_btn_7.ControlMode = 0
    Me.cmb_port_btn_7.Location = New System.Drawing.Point(409, 344)
    Me.cmb_port_btn_7.Name = "cmb_port_btn_7"
    Me.cmb_port_btn_7.Size = New System.Drawing.Size(383, 37)
    Me.cmb_port_btn_7.TabIndex = 16
    '
    'cmb_port_btn_8
    '
    Me.cmb_port_btn_8.BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_btn_8.Button1BackImage = Nothing
    Me.cmb_port_btn_8.Button1Tag = ""
    Me.cmb_port_btn_8.Checkbox1Checked = False
    Me.cmb_port_btn_8.Combo1AutoCompleteMode = False
    Me.cmb_port_btn_8.Combo1BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_btn_8.Combo1IsReadOnly = False
    Me.cmb_port_btn_8.Combo1TextValue = Nothing
    Me.cmb_port_btn_8.Combo1Value = 0
    Me.cmb_port_btn_8.Combo2AutoCompleteMode = False
    Me.cmb_port_btn_8.Combo2BackColor = System.Drawing.SystemColors.Control
    Me.cmb_port_btn_8.Combo2IsReadOnly = False
    Me.cmb_port_btn_8.Combo2TextValue = Nothing
    Me.cmb_port_btn_8.Combo2Value = 0
    Me.cmb_port_btn_8.ControlMode = 0
    Me.cmb_port_btn_8.Location = New System.Drawing.Point(409, 381)
    Me.cmb_port_btn_8.Name = "cmb_port_btn_8"
    Me.cmb_port_btn_8.Size = New System.Drawing.Size(383, 37)
    Me.cmb_port_btn_8.TabIndex = 17
    '
    'lbl_title_doors
    '
    Me.lbl_title_doors.BackColor = System.Drawing.Color.DarkSeaGreen
    Me.lbl_title_doors.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_title_doors.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
    Me.lbl_title_doors.Location = New System.Drawing.Point(409, 80)
    Me.lbl_title_doors.Name = "lbl_title_doors"
    Me.lbl_title_doors.Size = New System.Drawing.Size(392, 20)
    Me.lbl_title_doors.TabIndex = 49
    Me.lbl_title_doors.Text = "INPUT DOORS"
    Me.lbl_title_doors.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'frm_pcd_configuration_edit
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(904, 554)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_pcd_configuration_edit"
    Me.Text = "#5272 "
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Members "

  Private m_input_configuration_name As String
  Private DeleteOperation As Boolean
  ' input functionalities
  Private m_dt_input_sas_meters As DataTable
  Private m_dt_input_door_meters As DataTable
  ' output functionalities
  Private m_dt_output_tasks As DataTable
  Private m_dt_pcd_input_ports As DataTable
  Private m_dt_pcd_output_ports As DataTable

#End Region

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_PCD_TERMINALS_CONFIGURATION_EDIT

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    Dim _dual_control As uc_dual_combo
    Dim _dual_control_btn As uc_dual_combo_with_button
    Dim _control_ef As uc_entry_field
    Dim _control_button As Button
    Dim _sas_meters As cls_terminal_sas_meters
    Dim _hex_code As String

    'GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Enabled = True
    'GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Visible = True

    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
      Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
    End If

    '
    ' SET LABELS
    '
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6019)
    ef_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6018)
    lbl_description.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6026)
    lbl_counter_left.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6021) ' Contador
    lbl_counter_right.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6209) ' Puerta
    lbl_port_left.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6020) ' Port
    lbl_port_right.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6020)
    lbl_output_port.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6020)
    lbl_denom_left.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6022) ' Multiplicador
    lbl_output_mult.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6022)
    lbl_denom_right.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6207) ' Flag / VOltage
    lbl_outputs.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6208) ' Acci�n
    lbl_title_counters.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6205) ' COUNTER
    lbl_title_output.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6326) ' EXIT
    lbl_title_doors.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6206) ' DOOR

    tb_description.MaxLength = CLASS_PCD_CONFIGURATION.MAX_DESCRIPTION_LEN

    '
    ' Input datatables initialize
    '

    ' Ports
    m_dt_pcd_input_ports = New DataTable()
    m_dt_pcd_input_ports.Columns.Add("Id", Type.GetType("System.Int32"))
    m_dt_pcd_input_ports.Columns.Add("PCD IO Number")

    For _idx_port As Integer = 1 To CLASS_PCD_CONFIGURATION.PCD_NUM_INPUT_PORTS
      m_dt_pcd_input_ports.Rows.Add(_idx_port, _idx_port.ToString())
    Next

    ' Sas counters
    _sas_meters = New cls_terminal_sas_meters()

    m_dt_input_sas_meters = New DataTable()
    m_dt_input_sas_meters.Columns.Add("Id", Type.GetType("System.Int32"))
    m_dt_input_sas_meters.Columns.Add("Description")

    'm_dt_input_sas_meters.Rows.Add(0, Hex(0).PadLeft(2, "0") + " - " + GLB_NLS_GUI_PLAYER_TRACKING.GetString(6327))
    'm_dt_input_sas_meters.Rows.Add(1, Hex(1).PadLeft(2, "0") + " - " + GLB_NLS_GUI_PLAYER_TRACKING.GetString(6328))
    'm_dt_input_sas_meters.Rows.Add(2, Hex(2).PadLeft(2, "0") + " - " + GLB_NLS_GUI_PLAYER_TRACKING.GetString(6329))
    'm_dt_input_sas_meters.Rows.Add(3, Hex(3).PadLeft(2, "0") + " - " + GLB_NLS_GUI_PLAYER_TRACKING.GetString(6330))
    'm_dt_input_sas_meters.Rows.Add(5, Hex(5).PadLeft(2, "0") + " - " + GLB_NLS_GUI_PLAYER_TRACKING.GetString(6331))
    'm_dt_input_sas_meters.Rows.Add(6, Hex(6).PadLeft(2, "0") + " - " + GLB_NLS_GUI_PLAYER_TRACKING.GetString(6332))
    'm_dt_input_sas_meters.Rows.Add(8, Hex(8).PadLeft(2, "0") + " - " + GLB_NLS_GUI_PLAYER_TRACKING.GetString(6333))
    'm_dt_input_sas_meters.Rows.Add(9, Hex(9).PadLeft(2, "0") + " - " + GLB_NLS_GUI_PLAYER_TRACKING.GetString(6334))

    m_dt_input_sas_meters.Rows.Add(EMPTY_VALUE_NOT_SELECT.ToString(), "")
    For Each _meter As cls_terminal_sas_meters.Meter In _sas_meters.MeterList
      _hex_code = _meter.HexCode.PadLeft(2, "0")
      m_dt_input_sas_meters.Rows.Add(_meter.Code, _hex_code + " - " + _meter.CatalogName)
    Next

    ' Door alarms
    m_dt_input_door_meters = New DataTable()
    m_dt_input_door_meters.Columns.Add("Sas code", Type.GetType("System.Int64"))
    m_dt_input_door_meters.Columns.Add("Description")

    m_dt_input_door_meters.Rows.Add(SmibProtocols.PCDProtocolConfiguration.DoorCodes.SLOT_DOOR, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4492)) '	Slot door - was opened
    m_dt_input_door_meters.Rows.Add(SmibProtocols.PCDProtocolConfiguration.DoorCodes.DROP_DOOR, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4493)) '	Drop door - was opened
    m_dt_input_door_meters.Rows.Add(SmibProtocols.PCDProtocolConfiguration.DoorCodes.CARD_CAGE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4495)) '	Card cage - was opened
    'm_dt_input_door_meters.Rows.Add(65559, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4023)) '	AC power was applied to gaming machine
    m_dt_input_door_meters.Rows.Add(SmibProtocols.PCDProtocolConfiguration.DoorCodes.CASHBOX_DOOR, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4494)) '	Cashbox door - was opened
    m_dt_input_door_meters.Rows.Add(SmibProtocols.PCDProtocolConfiguration.DoorCodes.BELLY_DOOR, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4491)) '	Belly door - was opened

    '
    ' Output settings init
    '

    ' Ports
    m_dt_pcd_output_ports = New DataTable()
    m_dt_pcd_output_ports.Columns.Add("Id", Type.GetType("System.Int32"))
    m_dt_pcd_output_ports.Columns.Add("Port number")

    For _idx_port As Integer = 1 To CLASS_PCD_CONFIGURATION.PCD_NUM_OUTPUT_PORTS
      m_dt_pcd_output_ports.Rows.Add(_idx_port, _idx_port.ToString())
    Next

    ' Commands
    m_dt_output_tasks = New DataTable()
    m_dt_output_tasks.Columns.Add("Id", Type.GetType("System.Int32"))
    m_dt_output_tasks.Columns.Add("Description")

    m_dt_output_tasks.Rows.Add(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6210)) 'Shutdown
    m_dt_output_tasks.Rows.Add(1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6211)) '"Send cash in"

    '
    ' Setting controls values
    '

    ' Input Ports
    For _idx_port As Integer = INPUT_FIRST_CONTROL_METER To INPUT_LAST_CONTROL_METER
      _dual_control = panel_data.Controls("cmb_port_" + _idx_port.ToString())

      If _idx_port <= FIXED_INPUTS_COUNTERS_CODES.Length Then
        _dual_control.Combo1Add(m_dt_input_sas_meters, False, EMPTY_VALUE_NOT_SELECT)
        '_dual_control.Combo1Add(m_dt_input_sas_meters, True, FIXED_INPUTS_COUNTERS_CODES(_idx_port - 1))
        _dual_control.Combo2Add(m_dt_pcd_input_ports, False, _idx_port)
      Else
        _dual_control.Combo1Text = Nothing
        _dual_control.Combo1Add(m_dt_input_sas_meters, False, EMPTY_VALUE_NOT_SELECT)
        '_dual_control.Combo1AutoCompleteMode = True
        _dual_control.Combo2Add(m_dt_pcd_input_ports, True)
        ' only fixed codes
        _dual_control.Enabled = False
      End If

      _dual_control.EntryField1Text = Nothing
      _dual_control.EntryField1SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 9, 4)
      ' Raise checkbox checked event
      _dual_control.Checkbox1Checked = True
      _dual_control.Checkbox1Checked = False
    Next

    For _idx_port As Integer = INPUT_FIRST_CONTROL_DOOR To INPUT_LAST_CONTROL_DOOR
      _dual_control_btn = panel_data.Controls("cmb_port_btn_" + _idx_port.ToString())
      _dual_control_btn.Combo1Text = Nothing
      _dual_control_btn.Combo1Add(m_dt_input_door_meters)
      _dual_control_btn.Combo1Value = 1
      '_dual_control_btn.Combo1AutoCompleteMode = True
      _dual_control_btn.Combo2Add(m_dt_pcd_input_ports, False, _idx_port + INPUT_LAST_CONTROL_METER)
      _dual_control_btn.Button1Tag = CLASS_PCD_CONFIGURATION.PCD_IO_TYPE_PULSE_UP
      TypeButtonTagChanged(_dual_control_btn.Controls("btn_1"), False)
      ' Raise checkbox checked event
      _dual_control_btn.Checkbox1Checked = True
      _dual_control_btn.Checkbox1Checked = False
    Next

    ' Output Ports
    For _idx_port As Integer = 1 To CLASS_PCD_CONFIGURATION.PCD_NUM_OUTPUT_PORTS
      _dual_control = panel_data.Controls("cmb_out_port_" + _idx_port.ToString())

      If _idx_port <= FIXED_OUTPUTS_CODES.Length Then
        _dual_control.Combo1Add(m_dt_output_tasks, True, FIXED_OUTPUTS_CODES(_idx_port - 1))
      Else
        _dual_control.Combo1Text = Nothing
        _dual_control.Combo1Add(m_dt_output_tasks)
        _dual_control.Combo1AutoCompleteMode = True
      End If
      _dual_control.Combo2Add(m_dt_pcd_output_ports, False, _idx_port)
      _dual_control.EntryField1Text = Nothing
      _dual_control.EntryField1SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 9, 4)
      ' T0 And T1
      _control_ef = panel_data.Controls("ef_cmb_out_" + _idx_port.ToString() + "_t0")
      _control_ef.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, CLASS_PCD_CONFIGURATION.MAX_OUTPUT_MS_LEN)
      _control_ef.Enabled = False
      _control_ef = panel_data.Controls("ef_cmb_out_" + _idx_port.ToString() + "_t1")
      _control_ef.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, CLASS_PCD_CONFIGURATION.MAX_OUTPUT_MS_LEN)
      _control_ef.Enabled = False
      ' Button output type
      _control_button = panel_data.Controls("btn_cmb_out_" + _idx_port.ToString())
      If FIXED_OUTPUTS_CODES(_idx_port - 1) = 0 Then
        ' Shutdown
        _dual_control.EntryField1ReadOnly = True
        _control_button.Tag = CLASS_PCD_CONFIGURATION.PCD_IO_TYPE_PULSE_UP
      ElseIf FIXED_OUTPUTS_CODES(_idx_port - 1) = 1 Then
        ' Cash in
        _control_button.Tag = CLASS_PCD_CONFIGURATION.PCD_IO_TYPE_SIGNAL_UP
      Else
        _control_button.Tag = CLASS_PCD_CONFIGURATION.PCD_IO_TYPE_PULSE_UP
      End If
      TypeButtonTagChanged(_control_button)
    Next

    ' set visible/enabled/color config
    ControlValueChangedEvent()

    ' Filters
    Call ef_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, CLASS_PCD_CONFIGURATION.MAX_NAME_LEN)

  End Sub 'GUI_InitControls

  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)
    Dim _pcd_configuration_class As CLASS_PCD_CONFIGURATION
    Dim _nls_param1 As String
    Dim _nls_param2 As String
    Dim _rc As mdl_NLS.ENUM_MB_RESULT

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_PCD_CONFIGURATION
        _pcd_configuration_class = DbEditedObject

        DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          _pcd_configuration_class = Me.DbEditedObject
          _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6017)
          _nls_param2 = _pcd_configuration_class.ConfigurationName
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(103), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _nls_param1, _
                          _nls_param2)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          _pcd_configuration_class = Me.DbEditedObject
          _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6017)
          _nls_param2 = _pcd_configuration_class.ConfigurationId
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(114), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _nls_param1, _
                          _nls_param2)
          Call ef_name.Focus()
        ElseIf Me.DbStatus = ENUM_STATUS.STATUS_ALREADY_EXISTS Then
          _pcd_configuration_class = Me.DbEditedObject
          _nls_param1 = _pcd_configuration_class.ConfigurationName
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6023), _
                          ENUM_MB_TYPE.MB_TYPE_WARNING, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _nls_param1)
          Call ef_name.Focus()
        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          _pcd_configuration_class = Me.DbEditedObject
          _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6017)
          _nls_param2 = _pcd_configuration_class.ConfigurationName
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(105), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _nls_param1, _
                          _nls_param2)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus = ENUM_STATUS.STATUS_ALREADY_EXISTS Then
          Call ef_name.Focus()
        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          _pcd_configuration_class = Me.DbEditedObject
          _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6017)
          _nls_param2 = _pcd_configuration_class.ConfigurationName
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(107), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _nls_param1, _
                          _nls_param2)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus = ENUM_STATUS.STATUS_DEPENDENCIES And DeleteOperation Then
          _pcd_configuration_class = Me.DbEditedObject
          _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6017)
          _nls_param2 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4895) 'Terminales
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(23), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _nls_param1.ToLower, _
                          _nls_param2.ToLower)

        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK And DeleteOperation Then
          _pcd_configuration_class = Me.DbEditedObject
          _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6017)
          _nls_param2 = m_input_configuration_name
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(116), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _nls_param1, _
                          _nls_param2)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE
        DeleteOperation = False
        _pcd_configuration_class = Me.DbEditedObject
        _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6017)
        _nls_param2 = _pcd_configuration_class.ConfigurationName
        _rc = NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(112), _
                        ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        ENUM_MB_BTN.MB_BTN_YES_NO, _
                        ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
                        _nls_param1, _
                        _nls_param2)
        If _rc = ENUM_MB_RESULT.MB_RESULT_YES Then
          DeleteOperation = True
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_DELETE
        If DeleteOperation Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        Else
          DbStatus = ENUM_STATUS.STATUS_ERROR
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select
  End Sub

  Public Overloads Sub ShowNewItem()

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    DbObjectId = Nothing

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub 'ShowNewItem

  Public Overloads Sub ShowEditItem(ByVal ConfigurationId As Integer, ByVal ConfigurationName As String)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Me.DbObjectId = ConfigurationId
    Me.m_input_configuration_name = ConfigurationName

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If
  End Sub ' ShowEditItem

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _configuration_item As CLASS_PCD_CONFIGURATION
    Dim _control_dual As uc_dual_combo
    Dim _control_dual_btn As uc_dual_combo_with_button
    Dim _control_btn As Button
    Dim _control_ef0 As uc_entry_field
    Dim _control_ef1 As uc_entry_field

    _configuration_item = DbReadObject

    ' configuration name
    ef_name.Value = _configuration_item.ConfigurationName
    tb_description.Text = _configuration_item.ConfigurationDescription


    ' Input Ports
    For _idx_port As Integer = 1 To CLASS_PCD_CONFIGURATION.PCD_NUM_INPUT_PORTS
      If _configuration_item.ConfigurationInputPort(_idx_port - 1).egm_number = CLASS_PCD_CONFIGURATION.PCD_EGM_NULL_VALUE Then
        Continue For
      End If

      If _configuration_item.ConfigurationInputPort(_idx_port - 1).egm_number > CLASS_PCD_CONFIGURATION.PCD_INPUT_TYPE_DOOR_MASK Then
        ' Door
        For _idx_control As Integer = INPUT_FIRST_CONTROL_DOOR To INPUT_LAST_CONTROL_DOOR
          _control_dual_btn = panel_data.Controls("cmb_port_btn_" + _idx_control.ToString())
          If _control_dual_btn.Checkbox1Checked Then
            Continue For
          End If
          _control_dual_btn.Checkbox1Checked = True
          _control_dual_btn.Combo1Value = _configuration_item.ConfigurationInputPort(_idx_port - 1).egm_number
          _control_dual_btn.Combo2Value = _configuration_item.ConfigurationInputPort(_idx_port - 1).pcd_io_number
          _control_dual_btn.Button1Tag = _configuration_item.ConfigurationInputPort(_idx_port - 1).pulse_type
          TypeButtonTagChanged(_control_dual_btn.Controls("btn_1"))
          Exit For
        Next

      Else
        ' Meter
        For _idx_control As Integer = INPUT_FIRST_CONTROL_METER To INPUT_LAST_CONTROL_METER
          _control_dual = panel_data.Controls("cmb_port_" + _idx_control.ToString())
          If _control_dual.Checkbox1Checked Then
            Continue For
          End If
          '  If _idx_control <= FIXED_INPUTS_COUNTERS_CODES.Length Then
          '    If _control_dual.Combo1Value = _configuration_item.ConfigurationInputPort(_idx_port - 1).egm_number Then
          '      _control_dual.Checkbox1Checked = True
          '      _control_dual.EntryField1Value = _configuration_item.ConfigurationInputPort(_idx_port - 1).egm_number_multiplier.ToString()
          '      _control_dual.Combo2Value = _configuration_item.ConfigurationInputPort(_idx_port - 1).pcd_io_number
          '      Exit For
          '    End If
          '  Else
          _control_dual.Checkbox1Checked = True
          _control_dual.EntryField1Value = _configuration_item.ConfigurationInputPort(_idx_port - 1).egm_number_multiplier.ToString()
          _control_dual.Combo1Value = _configuration_item.ConfigurationInputPort(_idx_port - 1).egm_number
          _control_dual.Combo2Value = _configuration_item.ConfigurationInputPort(_idx_port - 1).pcd_io_number
          Exit For
          'End If
        Next
      End If
    Next

    ' Output Ports
    For _idx_port As Integer = 1 To CLASS_PCD_CONFIGURATION.PCD_NUM_OUTPUT_PORTS
      If _configuration_item.ConfigurationOutputPort(_idx_port - 1).egm_number = CLASS_PCD_CONFIGURATION.PCD_EGM_NULL_VALUE Then
        Continue For
      End If

      For _idx_control As Integer = 1 To CLASS_PCD_CONFIGURATION.PCD_NUM_OUTPUT_PORTS
        _control_dual = panel_data.Controls("cmb_out_port_" + _idx_control.ToString())
        _control_btn = panel_data.Controls("btn_cmb_out_" + _idx_control.ToString())
        If _control_dual.Checkbox1Checked Then
          Continue For
        End If

        If _idx_control <= FIXED_OUTPUTS_CODES.Length Then
          If _control_dual.Combo1Value = _configuration_item.ConfigurationOutputPort(_idx_port - 1).egm_number Then
            _control_dual.Checkbox1Checked = True
            _control_dual.EntryField1Value = IIf(_configuration_item.ConfigurationOutputPort(_idx_port - 1).egm_number_multiplier <= 0, Nothing, _configuration_item.ConfigurationOutputPort(_idx_port - 1).egm_number_multiplier.ToString())
            _control_dual.Combo2Value = _configuration_item.ConfigurationOutputPort(_idx_port - 1).pcd_io_number
            _control_btn.Tag = _configuration_item.ConfigurationOutputPort(_idx_port - 1).pulse_type

            TypeButtonTagChanged(_control_btn)

            _control_ef0 = panel_data.Controls("ef_cmb_out_" + _idx_control.ToString() + "_t0")
            _control_ef1 = panel_data.Controls("ef_cmb_out_" + _idx_control.ToString() + "_t1")

            If _control_btn.Tag = CLASS_PCD_CONFIGURATION.PCD_IO_TYPE_SIGNAL_DOWN _
            Or _control_btn.Tag = CLASS_PCD_CONFIGURATION.PCD_IO_TYPE_SIGNAL_UP Then
              _control_ef0.Value = _configuration_item.ConfigurationOutputPort(_idx_port - 1).time_pulse_down_ms.ToString()
              _control_ef1.Value = _configuration_item.ConfigurationOutputPort(_idx_port - 1).time_pulse_up_ms.ToString()
            Else
              _control_ef0.Enabled = False
              _control_ef1.Enabled = False
            End If

            Exit For
          End If
        Else
          _control_dual.Checkbox1Checked = True
          _control_dual.EntryField1Value = _configuration_item.ConfigurationOutputPort(_idx_port - 1).egm_number_multiplier.ToString()
          _control_dual.Combo1Value = _configuration_item.ConfigurationOutputPort(_idx_port - 1).egm_number
          _control_dual.Combo2Value = _configuration_item.ConfigurationOutputPort(_idx_port - 1).pcd_io_number
          _control_btn.Tag = _configuration_item.ConfigurationOutputPort(_idx_port - 1).pulse_type

          TypeButtonTagChanged(_control_btn)

          _control_ef0 = panel_data.Controls("ef_cmb_out_" + _idx_port.ToString() + "_t0")
          _control_ef1 = panel_data.Controls("ef_cmb_out_" + _idx_port.ToString() + "_t1")

          If _control_btn.Tag = CLASS_PCD_CONFIGURATION.PCD_IO_TYPE_SIGNAL_DOWN _
          Or _control_btn.Tag = CLASS_PCD_CONFIGURATION.PCD_IO_TYPE_SIGNAL_UP Then
            _control_ef0.Value = _configuration_item.ConfigurationOutputPort(_idx_port - 1).time_pulse_down_ms.ToString()
            _control_ef1.Value = _configuration_item.ConfigurationOutputPort(_idx_port - 1).time_pulse_up_ms.ToString()
          End If

          Exit For
        End If
      Next
    Next

    ControlValueChangedEvent()
  End Sub

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_GetScreenData()

    Dim _configuration_item As CLASS_PCD_CONFIGURATION
    Dim _control_dual As uc_dual_combo
    Dim _control_dual_btn As uc_dual_combo_with_button
    Dim _control_btn As Button
    Dim _control_ef0 As uc_entry_field
    Dim _control_ef1 As uc_entry_field
    Dim _port_denom As Decimal
    Dim _time_in_ms As Integer
    Dim _idx_port_to_save As Integer

    _configuration_item = DbEditedObject

    _configuration_item.ConfigurationName = ef_name.Value
    _configuration_item.ConfigurationDescription = tb_description.Text.Trim()

    _idx_port_to_save = 0
    ' Meters
    For _idx_port As Integer = INPUT_FIRST_CONTROL_METER To INPUT_LAST_CONTROL_METER
      _control_dual = panel_data.Controls("cmb_port_" + _idx_port.ToString())
      If _control_dual.Checkbox1Checked Then
        Decimal.TryParse(_control_dual.EntryField1Value, _port_denom)
        _configuration_item.ConfigurationInputPort(_idx_port_to_save).egm_number_multiplier = _port_denom
        _configuration_item.ConfigurationInputPort(_idx_port_to_save).egm_number = _control_dual.Combo1Value
        _configuration_item.ConfigurationInputPort(_idx_port_to_save).pcd_io_number = _control_dual.Combo2Value
        _idx_port_to_save += 1
      End If
    Next

    ' Doors
    For _idx_port As Integer = INPUT_FIRST_CONTROL_DOOR To INPUT_LAST_CONTROL_DOOR
      _control_dual_btn = panel_data.Controls("cmb_port_btn_" + _idx_port.ToString())
      If _control_dual_btn.Checkbox1Checked Then
        _configuration_item.ConfigurationInputPort(_idx_port_to_save).egm_number = _control_dual_btn.Combo1Value
        _configuration_item.ConfigurationInputPort(_idx_port_to_save).pcd_io_number = _control_dual_btn.Combo2Value
        _configuration_item.ConfigurationInputPort(_idx_port_to_save).pulse_type = _control_dual_btn.Button1Tag
        _idx_port_to_save += 1
      End If
    Next

    While _idx_port_to_save < CLASS_PCD_CONFIGURATION.PCD_NUM_INPUT_PORTS
      _configuration_item.InitInputPort(_idx_port_to_save)
      _idx_port_to_save += 1
    End While

    _idx_port_to_save = 0
    For _idx_port As Integer = 1 To CLASS_PCD_CONFIGURATION.PCD_NUM_OUTPUT_PORTS
      _control_dual = panel_data.Controls("cmb_out_port_" + _idx_port.ToString())
      _control_btn = panel_data.Controls("btn_cmb_out_" + _idx_port.ToString())
      If _control_dual.Checkbox1Checked Then
        Decimal.TryParse(_control_dual.EntryField1Value, _port_denom)
        _configuration_item.ConfigurationOutputPort(_idx_port_to_save).egm_number_multiplier = _port_denom
        _configuration_item.ConfigurationOutputPort(_idx_port_to_save).egm_number = _control_dual.Combo1Value
        _configuration_item.ConfigurationOutputPort(_idx_port_to_save).pcd_io_number = _control_dual.Combo2Value
        _configuration_item.ConfigurationOutputPort(_idx_port_to_save).pulse_type = _control_btn.Tag

        If _control_btn.Tag = CLASS_PCD_CONFIGURATION.PCD_IO_TYPE_SIGNAL_DOWN _
        Or _control_btn.Tag = CLASS_PCD_CONFIGURATION.PCD_IO_TYPE_SIGNAL_UP Then
          _control_ef0 = panel_data.Controls("ef_cmb_out_" + _idx_port.ToString() + "_t0")
          _control_ef1 = panel_data.Controls("ef_cmb_out_" + _idx_port.ToString() + "_t1")
          If Integer.TryParse(_control_ef0.Value, _time_in_ms) Then
            _configuration_item.ConfigurationOutputPort(_idx_port_to_save).time_pulse_down_ms = _time_in_ms
          End If
          If Integer.TryParse(_control_ef1.Value, _time_in_ms) Then
            _configuration_item.ConfigurationOutputPort(_idx_port_to_save).time_pulse_up_ms = _time_in_ms
          End If
        End If
        _idx_port_to_save += 1
      End If
    Next

    While _idx_port_to_save < CLASS_PCD_CONFIGURATION.PCD_NUM_OUTPUT_PORTS
      ' Clean unused ports
      _configuration_item.InitOutputPort(_idx_port_to_save)
      _idx_port_to_save += 1
    End While

  End Sub

  ' PURPOSE: Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.ef_name

  End Sub ' GUI_SetInitialFocus

  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Dim _nls_param1 As String
    Dim _control As uc_dual_combo
    Dim _control_btn As uc_dual_combo_with_button
    Dim _ef_control As uc_entry_field
    Dim _sas_meters_values As List(Of Integer)
    Dim _ports_list As List(Of Integer)

    _sas_meters_values = New List(Of Integer)
    _ports_list = New List(Of Integer)

    ' name blank field
    If ef_name.Value = "" Then
      _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6018)
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
      Call ef_name.Focus()

      Return False
    End If

    ' METERS
    For _idx_port As Integer = INPUT_FIRST_CONTROL_METER To INPUT_LAST_CONTROL_METER 'Meters 
      _control = panel_data.Controls("cmb_port_" + _idx_port.ToString())
      If Not _control.Checkbox1Checked Then
        Continue For
      End If
      If _sas_meters_values.Contains(_control.Combo1Value) Then
        _nls_param1 = _control.Combo1TextValue
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6023), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
        Call _control.Combo1Focus()
        Return False
      ElseIf String.IsNullOrEmpty(_control.EntryField1Value) Then
        _nls_param1 = _idx_port.ToString()
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6025), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
        Call _control.EntryField1Focus()
        Return False
      ElseIf _ports_list.Contains(_control.Combo2Value) Then
        _nls_param1 = _control.Combo2TextValue
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6340), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
        Call _control.Combo2Focus()
        Return False
      ElseIf (_control.Combo1Value = EMPTY_VALUE_NOT_SELECT) Then
        _nls_param1 = _idx_port.ToString()
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6339), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
        Call _control.Combo1Focus()
        Return False
      Else
        _sas_meters_values.Add(_control.Combo1Value)
        _ports_list.Add(_control.Combo2Value)
      End If
    Next

    ' DOORS
    For _idx_port As Integer = INPUT_FIRST_CONTROL_DOOR To INPUT_LAST_CONTROL_DOOR 'Meters 
      _control_btn = panel_data.Controls("cmb_port_btn_" + _idx_port.ToString())
      If Not _control_btn.Checkbox1Checked Then
        Continue For
      End If
      If _control_btn.Combo1Value = 0 Then
        _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6209)
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
        Call _control_btn.Combo1Focus()
        Return False
      End If
      If _sas_meters_values.Contains(_control_btn.Combo1Value) Then
        _nls_param1 = _control_btn.Combo1TextValue
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6023), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
        Call _control_btn.Combo1Focus()
        Return False
      ElseIf _ports_list.Contains(_control_btn.Combo2Value) Then
        _nls_param1 = _control_btn.Combo2TextValue
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6340), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
        Call _control_btn.Combo2Focus()
        Return False
      Else
        _sas_meters_values.Add(_control_btn.Combo1Value)
        _ports_list.Add(_control_btn.Combo2Value)
      End If
    Next

    ' OUTPUTS
    _ports_list.Clear()
    For _idx_port As Integer = 1 To CLASS_PCD_CONFIGURATION.PCD_NUM_OUTPUT_PORTS
      _control = panel_data.Controls("cmb_out_port_" + _idx_port.ToString())
      If Not _control.Checkbox1Checked Then
        Continue For
      End If

      If _control.EntryField1Enabled Then
        If String.IsNullOrEmpty(_control.EntryField1Value) Then
          _nls_param1 = _idx_port.ToString()
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6025), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
          Call _control.EntryField1Focus()
          Return False
        End If
      End If

      _ef_control = panel_data.Controls("ef_cmb_out_" + _idx_port.ToString() + "_t0")
      If _ef_control.Enabled Then
        If String.IsNullOrEmpty(_ef_control.Value) Then
          _nls_param1 = "T0"
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
          Call _ef_control.Focus()
          Return False
        End If
      End If

      _ef_control = panel_data.Controls("ef_cmb_out_" + _idx_port.ToString() + "_t1")
      If _ef_control.Enabled Then
        If String.IsNullOrEmpty(_ef_control.Value) Then
          _nls_param1 = "T1"
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
          Call _ef_control.Focus()
          Return False
        End If
      End If
    Next

    Return True

  End Function

#End Region

#Region " Private Functions "

  Private Sub TypeButtonTagChanged(ByRef SenderButton As Button, Optional ByVal HasEntryFields As Boolean = True)

    Dim _enable_efs As Boolean

    Select Case SenderButton.Tag
      Case CLASS_PCD_CONFIGURATION.PCD_IO_TYPE_SIGNAL_DOWN
        SenderButton.BackgroundImage = My.Resources.PulsesUp
        SenderButton.BackgroundImage.RotateFlip(RotateFlipType.Rotate180FlipX)
        _enable_efs = True
      Case CLASS_PCD_CONFIGURATION.PCD_IO_TYPE_SIGNAL_UP
        SenderButton.BackgroundImage = My.Resources.PulsesUp
        _enable_efs = True
      Case CLASS_PCD_CONFIGURATION.PCD_IO_TYPE_PULSE_DOWN
        SenderButton.BackgroundImage = My.Resources.PulseUp
        SenderButton.BackgroundImage.RotateFlip(RotateFlipType.Rotate180FlipX)
        _enable_efs = False
      Case CLASS_PCD_CONFIGURATION.PCD_IO_TYPE_PULSE_UP
        SenderButton.BackgroundImage = My.Resources.PulseUp
        _enable_efs = False
      Case Else
        _enable_efs = False
    End Select

    If HasEntryFields Then
      If SenderButton.Name.Contains("out_1") Then
        ef_cmb_out_1_t0.Enabled = _enable_efs
        ef_cmb_out_1_t1.Enabled = _enable_efs
      ElseIf SenderButton.Name.Contains("out_2") Then
        ef_cmb_out_2_t0.Enabled = _enable_efs
        ef_cmb_out_2_t1.Enabled = _enable_efs
      End If
    End If
  End Sub

#End Region

#Region " Events "

  ' PURPOSE: Shows in red incorrect configuration controls
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub ControlValueChangedEvent() Handles cmb_port_1.ControlValueChangedEvent, cmb_port_2.ControlValueChangedEvent, cmb_port_3.ControlValueChangedEvent, cmb_port_4.ControlValueChangedEvent, cmb_port_5.ControlValueChangedEvent, cmb_port_6.ControlValueChangedEvent, cmb_port_7.ControlValueChangedEvent, cmb_port_8.ControlValueChangedEvent, cmb_port_btn_1.ControlValueChangedEvent, cmb_port_btn_2.ControlValueChangedEvent, cmb_port_btn_3.ControlValueChangedEvent, cmb_port_btn_4.ControlValueChangedEvent, cmb_port_btn_5.ControlValueChangedEvent, cmb_port_btn_6.ControlValueChangedEvent, cmb_port_btn_7.ControlValueChangedEvent, cmb_port_btn_8.ControlValueChangedEvent

    Dim _list_ports As List(Of Integer)
    Dim _list_ports_error As List(Of Integer)
    Dim _list_sas_meters As List(Of Integer)
    Dim _list_sas_meters_error As List(Of Integer)
    Dim _control_dual As uc_dual_combo
    Dim _control_dual_btn As uc_dual_combo_with_button

    _list_ports = New List(Of Integer)
    _list_sas_meters = New List(Of Integer)

    _list_ports_error = New List(Of Integer)
    _list_sas_meters_error = New List(Of Integer)

    For _idx_port As Integer = INPUT_FIRST_CONTROL_METER To INPUT_LAST_CONTROL_METER
      _control_dual = panel_data.Controls("cmb_port_" + _idx_port.ToString())
      If _control_dual.Checkbox1Checked Then
        ' Check Port
        If _list_ports.Contains(_control_dual.Combo2Value) Then
          _list_ports_error.Add(_control_dual.Combo2Value)
        Else
          _list_ports.Add(_control_dual.Combo2Value)
        End If

        ' Check sas meter
        If _list_sas_meters.Contains(_control_dual.Combo1Value) Then
          _list_sas_meters_error.Add(_control_dual.Combo1Value)
        ElseIf (_control_dual.Combo1Value = EMPTY_VALUE_NOT_SELECT) Then
          _list_sas_meters_error.Add(_control_dual.Combo1Value)
        Else
          _list_sas_meters.Add(_control_dual.Combo1Value)
        End If
      End If
    Next

    For _idx_port As Integer = INPUT_FIRST_CONTROL_DOOR To INPUT_LAST_CONTROL_DOOR
      _control_dual_btn = panel_data.Controls("cmb_port_btn_" + _idx_port.ToString())
      If _control_dual_btn.Checkbox1Checked Then
        ' Check Port
        If _list_ports.Contains(_control_dual_btn.Combo2Value) Then
          _list_ports_error.Add(_control_dual_btn.Combo2Value)
        Else
          _list_ports.Add(_control_dual_btn.Combo2Value)
        End If

        ' Check sas meter
        If _control_dual_btn.Combo1Value = 0 Then
          Continue For
        ElseIf _list_sas_meters.Contains(_control_dual_btn.Combo1Value) Then
          _list_sas_meters_error.Add(_control_dual_btn.Combo1Value)
        Else
          _list_sas_meters.Add(_control_dual_btn.Combo1Value)
        End If
      End If
    Next

    ' To mark error values first value
    For _idx_port As Integer = INPUT_FIRST_CONTROL_METER To INPUT_LAST_CONTROL_METER
      _control_dual = panel_data.Controls("cmb_port_" + _idx_port.ToString())

      If _control_dual.Checkbox1Checked Then
        ' Check no selected value
        If _list_ports_error.Contains(_control_dual.Combo2Value) Then
          _control_dual.Combo2BackColor = Color.Red
          Continue For
        End If
        ' Check sas meter
        If _list_sas_meters_error.Contains(_control_dual.Combo1Value) Then
          _control_dual.Combo1BackColor = Color.Red
          Continue For
        End If

        If _control_dual.EntryField1Value.Length = 0 Then
          _control_dual.Combo1BackColor = Color.Red
          Continue For
        End If
      End If
      _control_dual.Combo1BackColor = Color.Transparent
      _control_dual.Combo2BackColor = Color.Transparent
    Next

    For _idx_port As Integer = INPUT_FIRST_CONTROL_DOOR To INPUT_LAST_CONTROL_DOOR
      _control_dual_btn = panel_data.Controls("cmb_port_btn_" + _idx_port.ToString())

      If _control_dual_btn.Checkbox1Checked Then
        ' Check Port
        If _list_ports_error.Contains(_control_dual_btn.Combo2Value) Then
          _control_dual_btn.Combo2BackColor = Color.Red
          Continue For
        End If
        If _control_dual_btn.Combo1Value = 0 Then
          _control_dual_btn.Combo1BackColor = Color.Red
          Continue For
        End If
        ' Check sas meter
        If _list_sas_meters_error.Contains(_control_dual_btn.Combo1Value) Then
          _control_dual_btn.Combo1BackColor = Color.Red
          Continue For
        End If
      End If
      _control_dual_btn.Combo1BackColor = Color.Transparent
      _control_dual_btn.Combo2BackColor = Color.Transparent
    Next

  End Sub

  ' PURPOSE: Change button pulse
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub ButtonPulsesClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cmb_out_1.Click, btn_cmb_out_2.Click, cmb_port_btn_1.Button1ClickEvent, cmb_port_btn_2.Button1ClickEvent, cmb_port_btn_3.Button1ClickEvent, cmb_port_btn_4.Button1ClickEvent, cmb_port_btn_5.Button1ClickEvent, cmb_port_btn_6.Button1ClickEvent, cmb_port_btn_7.Button1ClickEvent, cmb_port_btn_8.Button1ClickEvent

    Dim _btn As Button
    _btn = sender

    Select Case _btn.Tag

      Case CLASS_PCD_CONFIGURATION.PCD_IO_TYPE_PULSE_UP
        _btn.Tag = CLASS_PCD_CONFIGURATION.PCD_IO_TYPE_PULSE_DOWN
      Case CLASS_PCD_CONFIGURATION.PCD_IO_TYPE_PULSE_DOWN
        _btn.Tag = CLASS_PCD_CONFIGURATION.PCD_IO_TYPE_PULSE_UP
      Case CLASS_PCD_CONFIGURATION.PCD_IO_TYPE_SIGNAL_UP
        _btn.Tag = CLASS_PCD_CONFIGURATION.PCD_IO_TYPE_SIGNAL_DOWN
      Case CLASS_PCD_CONFIGURATION.PCD_IO_TYPE_SIGNAL_DOWN
        _btn.Tag = CLASS_PCD_CONFIGURATION.PCD_IO_TYPE_SIGNAL_UP
      Case Else
        ' Default value
        _btn.BackgroundImage = My.Resources.PulseUp
        _btn.Tag = CLASS_PCD_CONFIGURATION.PCD_IO_TYPE_PULSE_UP
    End Select


    If _btn.Name.Contains("btn_cmb_out") Then
      ' Output
      TypeButtonTagChanged(_btn, True)
    ElseIf _btn.Name.Contains("btn_1") Then
      ' Door Input uc_dual_combo_with_button
      TypeButtonTagChanged(_btn, False)
    End If

  End Sub

#End Region

End Class
