<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_chips_sets_sel
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_group_status = New System.Windows.Forms.GroupBox()
    Me.chk_enabled = New System.Windows.Forms.CheckBox()
    Me.chk_disabled = New System.Windows.Forms.CheckBox()
    Me.gb_chips_types = New System.Windows.Forms.GroupBox()
    Me.chk_chip_type_NR = New System.Windows.Forms.CheckBox()
    Me.chk_chip_type_RE = New System.Windows.Forms.CheckBox()
    Me.chk_chip_type_Color = New System.Windows.Forms.CheckBox()
    Me.ef_chips_sets_name = New GUI_Controls.uc_entry_field()
    Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_group_status.SuspendLayout()
    Me.gb_chips_types.SuspendLayout()
    Me.TableLayoutPanel1.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.ef_chips_sets_name)
    Me.panel_filter.Controls.Add(Me.gb_chips_types)
    Me.panel_filter.Controls.Add(Me.gb_group_status)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(724, 102)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_group_status, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_chips_types, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_chips_sets_name, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 106)
    Me.panel_data.Size = New System.Drawing.Size(724, 401)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Padding = New System.Windows.Forms.Padding(0, 4, 0, 4)
    Me.pn_separator_line.Size = New System.Drawing.Size(718, 17)
    '
    'pn_line
    '
    Me.pn_line.Location = New System.Drawing.Point(0, 4)
    Me.pn_line.Size = New System.Drawing.Size(718, 4)
    '
    'gb_group_status
    '
    Me.gb_group_status.Controls.Add(Me.chk_enabled)
    Me.gb_group_status.Controls.Add(Me.chk_disabled)
    Me.gb_group_status.Location = New System.Drawing.Point(524, 6)
    Me.gb_group_status.Name = "gb_group_status"
    Me.gb_group_status.Size = New System.Drawing.Size(84, 90)
    Me.gb_group_status.TabIndex = 2
    Me.gb_group_status.TabStop = False
    Me.gb_group_status.Text = "xEnabled"
    '
    'chk_enabled
    '
    Me.chk_enabled.AutoSize = True
    Me.chk_enabled.Location = New System.Drawing.Point(13, 19)
    Me.chk_enabled.Name = "chk_enabled"
    Me.chk_enabled.Size = New System.Drawing.Size(48, 17)
    Me.chk_enabled.TabIndex = 0
    Me.chk_enabled.Text = "xNo"
    Me.chk_enabled.UseVisualStyleBackColor = True
    '
    'chk_disabled
    '
    Me.chk_disabled.AutoSize = True
    Me.chk_disabled.Location = New System.Drawing.Point(13, 42)
    Me.chk_disabled.Name = "chk_disabled"
    Me.chk_disabled.Size = New System.Drawing.Size(53, 17)
    Me.chk_disabled.TabIndex = 1
    Me.chk_disabled.Text = "xYes"
    Me.chk_disabled.UseVisualStyleBackColor = True
    '
    'gb_chips_types
    '
    Me.gb_chips_types.Controls.Add(Me.TableLayoutPanel1)
    Me.gb_chips_types.Location = New System.Drawing.Point(320, 6)
    Me.gb_chips_types.Name = "gb_chips_types"
    Me.gb_chips_types.Size = New System.Drawing.Size(192, 90)
    Me.gb_chips_types.TabIndex = 1
    Me.gb_chips_types.TabStop = False
    Me.gb_chips_types.Text = "xChipsType"
    '
    'chk_chip_type_NR
    '
    Me.chk_chip_type_NR.AutoSize = True
    Me.chk_chip_type_NR.Location = New System.Drawing.Point(3, 26)
    Me.chk_chip_type_NR.Name = "chk_chip_type_NR"
    Me.chk_chip_type_NR.Size = New System.Drawing.Size(84, 17)
    Me.chk_chip_type_NR.TabIndex = 1
    Me.chk_chip_type_NR.Text = "xType_NR"
    Me.chk_chip_type_NR.UseVisualStyleBackColor = True
    '
    'chk_chip_type_RE
    '
    Me.chk_chip_type_RE.AutoSize = True
    Me.chk_chip_type_RE.Location = New System.Drawing.Point(3, 3)
    Me.chk_chip_type_RE.Name = "chk_chip_type_RE"
    Me.chk_chip_type_RE.Size = New System.Drawing.Size(83, 17)
    Me.chk_chip_type_RE.TabIndex = 0
    Me.chk_chip_type_RE.Text = "xType_RE"
    Me.chk_chip_type_RE.UseVisualStyleBackColor = True
    '
    'chk_chip_type_Color
    '
    Me.chk_chip_type_Color.AutoSize = True
    Me.chk_chip_type_Color.Location = New System.Drawing.Point(3, 49)
    Me.chk_chip_type_Color.Name = "chk_chip_type_Color"
    Me.chk_chip_type_Color.Size = New System.Drawing.Size(99, 17)
    Me.chk_chip_type_Color.TabIndex = 2
    Me.chk_chip_type_Color.Text = "xType_Color"
    Me.chk_chip_type_Color.UseVisualStyleBackColor = True
    '
    'ef_chips_sets_name
    '
    Me.ef_chips_sets_name.DoubleValue = 0.0R
    Me.ef_chips_sets_name.IntegerValue = 0
    Me.ef_chips_sets_name.IsReadOnly = False
    Me.ef_chips_sets_name.Location = New System.Drawing.Point(6, 25)
    Me.ef_chips_sets_name.Name = "ef_chips_sets_name"
    Me.ef_chips_sets_name.PlaceHolder = Nothing
    Me.ef_chips_sets_name.Size = New System.Drawing.Size(290, 24)
    Me.ef_chips_sets_name.SufixText = "Sufix Text"
    Me.ef_chips_sets_name.SufixTextVisible = True
    Me.ef_chips_sets_name.TabIndex = 0
    Me.ef_chips_sets_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_chips_sets_name.TextValue = ""
    Me.ef_chips_sets_name.Value = ""
    Me.ef_chips_sets_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'TableLayoutPanel1
    '
    Me.TableLayoutPanel1.ColumnCount = 1
    Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
    Me.TableLayoutPanel1.Controls.Add(Me.chk_chip_type_Color, 0, 2)
    Me.TableLayoutPanel1.Controls.Add(Me.chk_chip_type_NR, 0, 1)
    Me.TableLayoutPanel1.Controls.Add(Me.chk_chip_type_RE, 0, 0)
    Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
    Me.TableLayoutPanel1.Location = New System.Drawing.Point(3, 17)
    Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
    Me.TableLayoutPanel1.RowCount = 3
    Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
    Me.TableLayoutPanel1.Size = New System.Drawing.Size(186, 70)
    Me.TableLayoutPanel1.TabIndex = 11
    '
    'frm_chips_sets_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(734, 511)
    Me.Name = "frm_chips_sets_sel"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_chips_sets"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_group_status.ResumeLayout(False)
    Me.gb_group_status.PerformLayout()
    Me.gb_chips_types.ResumeLayout(False)
    Me.TableLayoutPanel1.ResumeLayout(False)
    Me.TableLayoutPanel1.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_group_status As System.Windows.Forms.GroupBox
  Friend WithEvents chk_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents chk_disabled As System.Windows.Forms.CheckBox
  Friend WithEvents gb_chips_types As System.Windows.Forms.GroupBox
  Friend WithEvents chk_chip_type_RE As System.Windows.Forms.CheckBox
  Friend WithEvents chk_chip_type_Color As System.Windows.Forms.CheckBox
  Friend WithEvents chk_chip_type_NR As System.Windows.Forms.CheckBox
  Friend WithEvents ef_chips_sets_name As GUI_Controls.uc_entry_field
  Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
End Class
