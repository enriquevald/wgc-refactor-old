'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_user_edit.vb
' DESCRIPTION:   User edition form
' AUTHOR:        Jaume Sala
' CREATION DATE: 04-JUL-2002
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 04-JUL-2002  JSV    Initial version.
' 07-AUG-2012  JCM    Added Cashier Sesion Sales Limit, Cashier Sesion all MB Sales Limit
' 16-AUG-2012  XCD    Added block reason status
' 17-AUG-2012  HBB    Added Check password rules in IsScreenDataOk
' 20-AUG-2012  HBB    Added Check login rules in IsScreenDataOk
' 29-AUG-2012  HBB    Get default value from GP(Change password in N days or not)
' 29-AUG-2012  JCM    Added non alpha numeric chars for password 
'                     Added case sensitive password
' 07-SEP-2012  JAB    Can created terminated users
' 13-FEB-2013  QMP    "Unlock" button is now associated to Execute permission
' 05-MAR-2013  ICS    Added a format validation for entry fields
' 06-MAR-2013  ICS    Added a new feature to limit the number of users per profile.
' 18-MAR-2013  ICS    Fixed bug #651: When it's a new user mustn't check that selected profile is different than previous
' 06-JUN-2013  DHA    Fixed Bug #830: Changed the length from the field 'Nombre Completo'
' 10-JUL-2013  DHA    Changed the length from the field 'Password' from 15 to 22 characters
' 13-FEB-2014  LJM    Added Employee code to user
' 22-SEP-2014  HBB & XIT Added Change Stacker card code to user
' 25-SEP-2014  SGB    Added Change Technician card code to user
' 25-SEP-2014  SGB    Fixed Bug #WIG-1277 & #WIG-1279: multisite control input because Stacker card & Technician card columns do not exist in BD
' 30-SEP-2014  DRV    Added kbdMsgEvent
' 19-MAY-2015  JML    Multisite-Multicurrency, User sales limit disabled
' 17-NOV-2015  JBC    SmartFloor Manager and Runners properties.
' 28-NOV-2016  FAV    PBI 20347:EGASA: Login with card
' 28-NOV-2016  FAV    PBI 20374:EGASA: Logic card assignment
' 29-NOV-2016  FAV    PBI 20347:EGASA: New LOGIN card in CASHIER
' 25-JAN-2017  FOS    Fixed Bug 23413:When an user is update, change password is always required
' 09-AUG-2017  AMF    Bug 29280:[WIGOS-4057] Multisite - Untranslated string in the user edition screen
' 28-SEP-2017  ETP    Fixed Bug 29947:[WIGOS-5438] wrong label NLS for "Habilitar empleado" control in "Edici�n de usuario" screen for Multisite
' 20-APR-2018  XGJ    WIGOS-10166: Movibank - An error is appear when trying to create user with DEFAULT_PROFILE.
'--------------------------------------------------------------------

Option Explicit On

#Region " Imports "

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports WSI.Common
Imports System.Text.RegularExpressions
Imports System.Data.SqlClient
Imports System.Text

#End Region

Public Class frm_user_edit
  Inherits frm_base_edit

#Region " Constants "

  Private Const LEN_FULL_NAME As Integer = 50
  Private Const LEN_PROFILE As Integer = 20
  Private Const LEN_EMPLOYEE_CODE As Integer = 40

  Friend WithEvents ef_cashier_sales_limit As GUI_Controls.uc_entry_field
  Friend WithEvents gb_sales_limit As System.Windows.Forms.GroupBox
  Friend WithEvents ef_mb_sales_limit As GUI_Controls.uc_entry_field
  Friend WithEvents gb_state As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_blocked As System.Windows.Forms.Label
  Friend WithEvents cb_unsubscribed As System.Windows.Forms.CheckBox
  Friend WithEvents cb_disabled As System.Windows.Forms.CheckBox
  Friend WithEvents btn_unlock As System.Windows.Forms.Button
  Friend WithEvents chklst_block_reasons As System.Windows.Forms.CheckedListBox
  Friend WithEvents chk_master As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_master_info As System.Windows.Forms.Label
  Friend WithEvents ef_employee_code As GUI_Controls.uc_entry_field
  Friend WithEvents gb_change_stacker As System.Windows.Forms.GroupBox
  Friend WithEvents ef_card_stacker_change As GUI_Controls.uc_entry_field
  Friend WithEvents chk_enable_stacker_change As System.Windows.Forms.CheckBox
  Friend WithEvents ef_card_technician As GUI_Controls.uc_entry_field
  Friend WithEvents chk_enable_technician As System.Windows.Forms.CheckBox
  Friend WithEvents gb_smartfloor As System.Windows.Forms.GroupBox
  Friend WithEvents chk_sf_manager As System.Windows.Forms.CheckBox
  Friend WithEvents chk_sf_runner As System.Windows.Forms.CheckBox
  Friend WithEvents ef_card_employee As GUI_Controls.uc_entry_field
  Friend WithEvents chk_enable_employee As System.Windows.Forms.CheckBox
  Friend WithEvents btn_restore_PIN As System.Windows.Forms.Button
  Friend WithEvents pnl_employee As System.Windows.Forms.Panel
  Private Const LEN_PASSWORD_EXP As Integer = 3

#End Region

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    Me.FormId = ENUM_FORM.FORM_USER_EDIT

    Call MyBase.GUI_SetFormId()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call
  End Sub

  Public Sub New(ByVal FormId As ENUM_FORM)
    MyBase.New()

    Me.FormId = FormId
    Me.m_master = True

    Call MyBase.GUI_SetFormId()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call
  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If

    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.
  'Do not modify it using the code editor.
  Friend WithEvents ef_full_name As GUI_Controls.uc_entry_field
  Friend WithEvents gb_login As System.Windows.Forms.GroupBox
  Friend WithEvents chk_password_req As System.Windows.Forms.CheckBox
  Friend WithEvents chk_not_expired As System.Windows.Forms.CheckBox
  Friend WithEvents dtp_password_date As GUI_Controls.uc_date_picker
  Friend WithEvents ef_password_exp As GUI_Controls.uc_entry_field
  Friend WithEvents ef_verify_password As GUI_Controls.uc_entry_field
  Friend WithEvents ef_password As GUI_Controls.uc_entry_field
  Friend WithEvents ef_profile As GUI_Controls.uc_entry_field
  Friend WithEvents bt_select_profile As GUI_Controls.uc_button
  Friend WithEvents ef_username As GUI_Controls.uc_entry_field

  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.ef_username = New GUI_Controls.uc_entry_field()
    Me.ef_full_name = New GUI_Controls.uc_entry_field()
    Me.gb_login = New System.Windows.Forms.GroupBox()
    Me.ef_card_employee = New GUI_Controls.uc_entry_field()
    Me.btn_restore_PIN = New System.Windows.Forms.Button()
    Me.chk_enable_employee = New System.Windows.Forms.CheckBox()
    Me.chk_password_req = New System.Windows.Forms.CheckBox()
    Me.chk_not_expired = New System.Windows.Forms.CheckBox()
    Me.dtp_password_date = New GUI_Controls.uc_date_picker()
    Me.ef_password_exp = New GUI_Controls.uc_entry_field()
    Me.ef_verify_password = New GUI_Controls.uc_entry_field()
    Me.ef_password = New GUI_Controls.uc_entry_field()
    Me.ef_profile = New GUI_Controls.uc_entry_field()
    Me.bt_select_profile = New GUI_Controls.uc_button()
    Me.ef_cashier_sales_limit = New GUI_Controls.uc_entry_field()
    Me.gb_sales_limit = New System.Windows.Forms.GroupBox()
    Me.ef_mb_sales_limit = New GUI_Controls.uc_entry_field()
    Me.gb_state = New System.Windows.Forms.GroupBox()
    Me.chklst_block_reasons = New System.Windows.Forms.CheckedListBox()
    Me.btn_unlock = New System.Windows.Forms.Button()
    Me.lbl_blocked = New System.Windows.Forms.Label()
    Me.cb_unsubscribed = New System.Windows.Forms.CheckBox()
    Me.cb_disabled = New System.Windows.Forms.CheckBox()
    Me.chk_master = New System.Windows.Forms.CheckBox()
    Me.lbl_master_info = New System.Windows.Forms.Label()
    Me.ef_employee_code = New GUI_Controls.uc_entry_field()
    Me.gb_change_stacker = New System.Windows.Forms.GroupBox()
    Me.ef_card_technician = New GUI_Controls.uc_entry_field()
    Me.chk_enable_technician = New System.Windows.Forms.CheckBox()
    Me.ef_card_stacker_change = New GUI_Controls.uc_entry_field()
    Me.chk_enable_stacker_change = New System.Windows.Forms.CheckBox()
    Me.gb_smartfloor = New System.Windows.Forms.GroupBox()
    Me.chk_sf_runner = New System.Windows.Forms.CheckBox()
    Me.chk_sf_manager = New System.Windows.Forms.CheckBox()
    Me.pnl_employee = New System.Windows.Forms.Panel()
    Me.panel_data.SuspendLayout()
    Me.gb_login.SuspendLayout()
    Me.gb_sales_limit.SuspendLayout()
    Me.gb_state.SuspendLayout()
    Me.gb_change_stacker.SuspendLayout()
    Me.gb_smartfloor.SuspendLayout()
    Me.pnl_employee.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_smartfloor)
    Me.panel_data.Controls.Add(Me.ef_profile)
    Me.panel_data.Controls.Add(Me.gb_change_stacker)
    Me.panel_data.Controls.Add(Me.ef_employee_code)
    Me.panel_data.Controls.Add(Me.lbl_master_info)
    Me.panel_data.Controls.Add(Me.chk_master)
    Me.panel_data.Controls.Add(Me.gb_state)
    Me.panel_data.Controls.Add(Me.gb_sales_limit)
    Me.panel_data.Controls.Add(Me.bt_select_profile)
    Me.panel_data.Controls.Add(Me.ef_full_name)
    Me.panel_data.Controls.Add(Me.ef_username)
    Me.panel_data.Controls.Add(Me.gb_login)
    Me.panel_data.Size = New System.Drawing.Size(787, 413)
    '
    'ef_username
    '
    Me.ef_username.DoubleValue = 0.0R
    Me.ef_username.IntegerValue = 0
    Me.ef_username.IsReadOnly = False
    Me.ef_username.Location = New System.Drawing.Point(89, 15)
    Me.ef_username.Name = "ef_username"
    Me.ef_username.OnlyUpperCase = True
    Me.ef_username.PlaceHolder = Nothing
    Me.ef_username.Size = New System.Drawing.Size(272, 24)
    Me.ef_username.SufixText = "Sufix Text"
    Me.ef_username.SufixTextVisible = True
    Me.ef_username.TabIndex = 0
    Me.ef_username.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_username.TextValue = ""
    Me.ef_username.Value = ""
    Me.ef_username.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_full_name
    '
    Me.ef_full_name.DoubleValue = 0.0R
    Me.ef_full_name.IntegerValue = 0
    Me.ef_full_name.IsReadOnly = False
    Me.ef_full_name.Location = New System.Drawing.Point(49, 45)
    Me.ef_full_name.Name = "ef_full_name"
    Me.ef_full_name.PlaceHolder = Nothing
    Me.ef_full_name.Size = New System.Drawing.Size(410, 24)
    Me.ef_full_name.SufixText = "Sufix Text"
    Me.ef_full_name.SufixTextVisible = True
    Me.ef_full_name.TabIndex = 1
    Me.ef_full_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_full_name.TextValue = ""
    Me.ef_full_name.TextWidth = 120
    Me.ef_full_name.Value = ""
    Me.ef_full_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_login
    '
    Me.gb_login.Controls.Add(Me.pnl_employee)
    Me.gb_login.Controls.Add(Me.chk_password_req)
    Me.gb_login.Controls.Add(Me.chk_not_expired)
    Me.gb_login.Controls.Add(Me.dtp_password_date)
    Me.gb_login.Controls.Add(Me.ef_password_exp)
    Me.gb_login.Controls.Add(Me.ef_verify_password)
    Me.gb_login.Controls.Add(Me.ef_password)
    Me.gb_login.Location = New System.Drawing.Point(8, 135)
    Me.gb_login.Name = "gb_login"
    Me.gb_login.Size = New System.Drawing.Size(328, 275)
    Me.gb_login.TabIndex = 7
    Me.gb_login.TabStop = False
    Me.gb_login.Text = "xLogin"
    '
    'ef_card_employee
    '
    Me.ef_card_employee.DoubleValue = 0.0R
    Me.ef_card_employee.IntegerValue = 0
    Me.ef_card_employee.IsReadOnly = False
    Me.ef_card_employee.Location = New System.Drawing.Point(6, 32)
    Me.ef_card_employee.Name = "ef_card_employee"
    Me.ef_card_employee.PlaceHolder = Nothing
    Me.ef_card_employee.Size = New System.Drawing.Size(209, 24)
    Me.ef_card_employee.SufixText = "Sufix Text"
    Me.ef_card_employee.SufixTextVisible = True
    Me.ef_card_employee.TabIndex = 7
    Me.ef_card_employee.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_card_employee.TextValue = ""
    Me.ef_card_employee.TextWidth = 50
    Me.ef_card_employee.Value = ""
    Me.ef_card_employee.ValueForeColor = System.Drawing.Color.Blue
    '
    'btn_restore_PIN
    '
    Me.btn_restore_PIN.Location = New System.Drawing.Point(219, 32)
    Me.btn_restore_PIN.Name = "btn_restore_PIN"
    Me.btn_restore_PIN.Size = New System.Drawing.Size(100, 23)
    Me.btn_restore_PIN.TabIndex = 8
    Me.btn_restore_PIN.Text = "xReiniciar PIN"
    Me.btn_restore_PIN.UseVisualStyleBackColor = True
    '
    'chk_enable_employee
    '
    Me.chk_enable_employee.AutoSize = True
    Me.chk_enable_employee.Location = New System.Drawing.Point(15, 11)
    Me.chk_enable_employee.Name = "chk_enable_employee"
    Me.chk_enable_employee.Size = New System.Drawing.Size(140, 17)
    Me.chk_enable_employee.TabIndex = 6
    Me.chk_enable_employee.Text = "xHabilitar empleado"
    Me.chk_enable_employee.UseVisualStyleBackColor = True
    '
    'chk_password_req
    '
    Me.chk_password_req.Location = New System.Drawing.Point(16, 170)
    Me.chk_password_req.Name = "chk_password_req"
    Me.chk_password_req.Size = New System.Drawing.Size(296, 17)
    Me.chk_password_req.TabIndex = 5
    Me.chk_password_req.Text = "Requiere cambio de clave en el pr�ximo login"
    '
    'chk_not_expired
    '
    Me.chk_not_expired.Location = New System.Drawing.Point(16, 147)
    Me.chk_not_expired.Name = "chk_not_expired"
    Me.chk_not_expired.Size = New System.Drawing.Size(216, 17)
    Me.chk_not_expired.TabIndex = 4
    Me.chk_not_expired.Text = "Clave nunca expira"
    '
    'dtp_password_date
    '
    Me.dtp_password_date.Checked = True
    Me.dtp_password_date.IsReadOnly = False
    Me.dtp_password_date.Location = New System.Drawing.Point(16, 21)
    Me.dtp_password_date.Name = "dtp_password_date"
    Me.dtp_password_date.ShowCheckBox = False
    Me.dtp_password_date.ShowUpDown = False
    Me.dtp_password_date.Size = New System.Drawing.Size(274, 24)
    Me.dtp_password_date.SufixText = "Sufix Text"
    Me.dtp_password_date.SufixTextVisible = True
    Me.dtp_password_date.TabIndex = 0
    Me.dtp_password_date.TextWidth = 170
    Me.dtp_password_date.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'ef_password_exp
    '
    Me.ef_password_exp.DoubleValue = 0.0R
    Me.ef_password_exp.IntegerValue = 0
    Me.ef_password_exp.IsReadOnly = False
    Me.ef_password_exp.Location = New System.Drawing.Point(16, 114)
    Me.ef_password_exp.Name = "ef_password_exp"
    Me.ef_password_exp.PlaceHolder = Nothing
    Me.ef_password_exp.Size = New System.Drawing.Size(255, 24)
    Me.ef_password_exp.SufixText = "xdias"
    Me.ef_password_exp.SufixTextVisible = True
    Me.ef_password_exp.SufixTextWidth = 45
    Me.ef_password_exp.TabIndex = 3
    Me.ef_password_exp.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_password_exp.TextValue = ""
    Me.ef_password_exp.TextWidth = 170
    Me.ef_password_exp.Value = ""
    Me.ef_password_exp.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_verify_password
    '
    Me.ef_verify_password.DoubleValue = 0.0R
    Me.ef_verify_password.IntegerValue = 0
    Me.ef_verify_password.IsReadOnly = False
    Me.ef_verify_password.Location = New System.Drawing.Point(16, 79)
    Me.ef_verify_password.Name = "ef_verify_password"
    Me.ef_verify_password.PlaceHolder = Nothing
    Me.ef_verify_password.Size = New System.Drawing.Size(304, 24)
    Me.ef_verify_password.SufixText = "Sufix Text"
    Me.ef_verify_password.SufixTextVisible = True
    Me.ef_verify_password.TabIndex = 2
    Me.ef_verify_password.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_verify_password.TextValue = ""
    Me.ef_verify_password.TextWidth = 170
    Me.ef_verify_password.Value = ""
    Me.ef_verify_password.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_password
    '
    Me.ef_password.DoubleValue = 0.0R
    Me.ef_password.IntegerValue = 0
    Me.ef_password.IsReadOnly = False
    Me.ef_password.Location = New System.Drawing.Point(16, 49)
    Me.ef_password.Name = "ef_password"
    Me.ef_password.PlaceHolder = Nothing
    Me.ef_password.Size = New System.Drawing.Size(304, 24)
    Me.ef_password.SufixText = "Sufix Text"
    Me.ef_password.SufixTextVisible = True
    Me.ef_password.TabIndex = 1
    Me.ef_password.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_password.TextValue = ""
    Me.ef_password.TextWidth = 170
    Me.ef_password.Value = ""
    Me.ef_password.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_profile
    '
    Me.ef_profile.DoubleValue = 0.0R
    Me.ef_profile.IntegerValue = 0
    Me.ef_profile.IsReadOnly = True
    Me.ef_profile.Location = New System.Drawing.Point(443, 15)
    Me.ef_profile.Name = "ef_profile"
    Me.ef_profile.PlaceHolder = Nothing
    Me.ef_profile.Size = New System.Drawing.Size(291, 24)
    Me.ef_profile.SufixText = "Sufix Text"
    Me.ef_profile.SufixTextVisible = True
    Me.ef_profile.TabIndex = 3
    Me.ef_profile.TabStop = False
    Me.ef_profile.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_profile.TextValue = ""
    Me.ef_profile.Value = ""
    Me.ef_profile.ValueForeColor = System.Drawing.Color.Blue
    '
    'bt_select_profile
    '
    Me.bt_select_profile.DialogResult = System.Windows.Forms.DialogResult.None
    Me.bt_select_profile.Location = New System.Drawing.Point(740, 15)
    Me.bt_select_profile.Name = "bt_select_profile"
    Me.bt_select_profile.Size = New System.Drawing.Size(30, 29)
    Me.bt_select_profile.TabIndex = 4
    Me.bt_select_profile.ToolTipped = False
    Me.bt_select_profile.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.SELECTION
    '
    'ef_cashier_sales_limit
    '
    Me.ef_cashier_sales_limit.DoubleValue = 0.0R
    Me.ef_cashier_sales_limit.IntegerValue = 0
    Me.ef_cashier_sales_limit.IsReadOnly = False
    Me.ef_cashier_sales_limit.Location = New System.Drawing.Point(13, 15)
    Me.ef_cashier_sales_limit.Name = "ef_cashier_sales_limit"
    Me.ef_cashier_sales_limit.PlaceHolder = Nothing
    Me.ef_cashier_sales_limit.Size = New System.Drawing.Size(205, 24)
    Me.ef_cashier_sales_limit.SufixText = "Sufix Text"
    Me.ef_cashier_sales_limit.SufixTextVisible = True
    Me.ef_cashier_sales_limit.TabIndex = 0
    Me.ef_cashier_sales_limit.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_cashier_sales_limit.TextValue = ""
    Me.ef_cashier_sales_limit.Value = ""
    Me.ef_cashier_sales_limit.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_sales_limit
    '
    Me.gb_sales_limit.Controls.Add(Me.ef_mb_sales_limit)
    Me.gb_sales_limit.Controls.Add(Me.ef_cashier_sales_limit)
    Me.gb_sales_limit.Location = New System.Drawing.Point(551, 135)
    Me.gb_sales_limit.Name = "gb_sales_limit"
    Me.gb_sales_limit.Size = New System.Drawing.Size(224, 76)
    Me.gb_sales_limit.TabIndex = 9
    Me.gb_sales_limit.TabStop = False
    Me.gb_sales_limit.Text = "xL�mtes de Ventas"
    '
    'ef_mb_sales_limit
    '
    Me.ef_mb_sales_limit.DoubleValue = 0.0R
    Me.ef_mb_sales_limit.IntegerValue = 0
    Me.ef_mb_sales_limit.IsReadOnly = False
    Me.ef_mb_sales_limit.Location = New System.Drawing.Point(13, 45)
    Me.ef_mb_sales_limit.Name = "ef_mb_sales_limit"
    Me.ef_mb_sales_limit.PlaceHolder = Nothing
    Me.ef_mb_sales_limit.Size = New System.Drawing.Size(205, 24)
    Me.ef_mb_sales_limit.SufixText = "Sufix Text"
    Me.ef_mb_sales_limit.SufixTextVisible = True
    Me.ef_mb_sales_limit.TabIndex = 1
    Me.ef_mb_sales_limit.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_mb_sales_limit.TextValue = ""
    Me.ef_mb_sales_limit.Value = ""
    Me.ef_mb_sales_limit.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_state
    '
    Me.gb_state.Controls.Add(Me.chklst_block_reasons)
    Me.gb_state.Controls.Add(Me.btn_unlock)
    Me.gb_state.Controls.Add(Me.lbl_blocked)
    Me.gb_state.Controls.Add(Me.cb_unsubscribed)
    Me.gb_state.Controls.Add(Me.cb_disabled)
    Me.gb_state.Location = New System.Drawing.Point(342, 135)
    Me.gb_state.Name = "gb_state"
    Me.gb_state.Size = New System.Drawing.Size(203, 275)
    Me.gb_state.TabIndex = 8
    Me.gb_state.TabStop = False
    Me.gb_state.Text = "xEstado"
    '
    'chklst_block_reasons
    '
    Me.chklst_block_reasons.BackColor = System.Drawing.SystemColors.Control
    Me.chklst_block_reasons.BorderStyle = System.Windows.Forms.BorderStyle.None
    Me.chklst_block_reasons.ForeColor = System.Drawing.SystemColors.ControlText
    Me.chklst_block_reasons.Location = New System.Drawing.Point(6, 99)
    Me.chklst_block_reasons.Name = "chklst_block_reasons"
    Me.chklst_block_reasons.SelectionMode = System.Windows.Forms.SelectionMode.None
    Me.chklst_block_reasons.Size = New System.Drawing.Size(190, 64)
    Me.chklst_block_reasons.TabIndex = 3
    '
    'btn_unlock
    '
    Me.btn_unlock.Location = New System.Drawing.Point(60, 174)
    Me.btn_unlock.Name = "btn_unlock"
    Me.btn_unlock.Size = New System.Drawing.Size(96, 23)
    Me.btn_unlock.TabIndex = 4
    Me.btn_unlock.Text = "xDesbloquear"
    Me.btn_unlock.UseVisualStyleBackColor = True
    '
    'lbl_blocked
    '
    Me.lbl_blocked.BackColor = System.Drawing.Color.Red
    Me.lbl_blocked.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_blocked.ForeColor = System.Drawing.Color.White
    Me.lbl_blocked.Location = New System.Drawing.Point(6, 17)
    Me.lbl_blocked.Name = "lbl_blocked"
    Me.lbl_blocked.Size = New System.Drawing.Size(190, 23)
    Me.lbl_blocked.TabIndex = 0
    Me.lbl_blocked.Text = "BLOQUEADO"
    Me.lbl_blocked.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'cb_unsubscribed
    '
    Me.cb_unsubscribed.AutoSize = True
    Me.cb_unsubscribed.Location = New System.Drawing.Point(7, 76)
    Me.cb_unsubscribed.Name = "cb_unsubscribed"
    Me.cb_unsubscribed.Size = New System.Drawing.Size(59, 17)
    Me.cb_unsubscribed.TabIndex = 2
    Me.cb_unsubscribed.Text = "xBaja"
    Me.cb_unsubscribed.UseVisualStyleBackColor = True
    '
    'cb_disabled
    '
    Me.cb_disabled.AutoSize = True
    Me.cb_disabled.Location = New System.Drawing.Point(7, 53)
    Me.cb_disabled.Name = "cb_disabled"
    Me.cb_disabled.Size = New System.Drawing.Size(110, 17)
    Me.cb_disabled.TabIndex = 1
    Me.cb_disabled.Text = "xDeshabilitado"
    Me.cb_disabled.UseVisualStyleBackColor = True
    '
    'chk_master
    '
    Me.chk_master.AutoSize = True
    Me.chk_master.Location = New System.Drawing.Point(505, 60)
    Me.chk_master.Name = "chk_master"
    Me.chk_master.Size = New System.Drawing.Size(121, 17)
    Me.chk_master.TabIndex = 5
    Me.chk_master.Text = "xUsuarioMaestro"
    Me.chk_master.UseVisualStyleBackColor = True
    '
    'lbl_master_info
    '
    Me.lbl_master_info.AutoSize = True
    Me.lbl_master_info.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.lbl_master_info.ForeColor = System.Drawing.Color.Navy
    Me.lbl_master_info.Location = New System.Drawing.Point(5, 110)
    Me.lbl_master_info.Name = "lbl_master_info"
    Me.lbl_master_info.Size = New System.Drawing.Size(78, 13)
    Me.lbl_master_info.TabIndex = 6
    Me.lbl_master_info.Text = "xMasterUser"
    Me.lbl_master_info.Visible = False
    '
    'ef_employee_code
    '
    Me.ef_employee_code.DoubleValue = 0.0R
    Me.ef_employee_code.IntegerValue = 0
    Me.ef_employee_code.IsReadOnly = False
    Me.ef_employee_code.Location = New System.Drawing.Point(39, 75)
    Me.ef_employee_code.Name = "ef_employee_code"
    Me.ef_employee_code.PlaceHolder = Nothing
    Me.ef_employee_code.Size = New System.Drawing.Size(322, 24)
    Me.ef_employee_code.SufixText = "Sufix Text"
    Me.ef_employee_code.SufixTextVisible = True
    Me.ef_employee_code.TabIndex = 2
    Me.ef_employee_code.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_employee_code.TextValue = ""
    Me.ef_employee_code.TextWidth = 130
    Me.ef_employee_code.Value = ""
    Me.ef_employee_code.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_change_stacker
    '
    Me.gb_change_stacker.Controls.Add(Me.ef_card_technician)
    Me.gb_change_stacker.Controls.Add(Me.chk_enable_technician)
    Me.gb_change_stacker.Controls.Add(Me.ef_card_stacker_change)
    Me.gb_change_stacker.Controls.Add(Me.chk_enable_stacker_change)
    Me.gb_change_stacker.Location = New System.Drawing.Point(551, 217)
    Me.gb_change_stacker.Name = "gb_change_stacker"
    Me.gb_change_stacker.Size = New System.Drawing.Size(225, 193)
    Me.gb_change_stacker.TabIndex = 10
    Me.gb_change_stacker.TabStop = False
    Me.gb_change_stacker.Text = "xCambio de Stacker"
    '
    'ef_card_technician
    '
    Me.ef_card_technician.DoubleValue = 0.0R
    Me.ef_card_technician.IntegerValue = 0
    Me.ef_card_technician.IsReadOnly = False
    Me.ef_card_technician.Location = New System.Drawing.Point(10, 96)
    Me.ef_card_technician.Name = "ef_card_technician"
    Me.ef_card_technician.OnlyUpperCase = True
    Me.ef_card_technician.PlaceHolder = Nothing
    Me.ef_card_technician.Size = New System.Drawing.Size(209, 24)
    Me.ef_card_technician.SufixText = "Sufix Text"
    Me.ef_card_technician.SufixTextVisible = True
    Me.ef_card_technician.TabIndex = 3
    Me.ef_card_technician.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_card_technician.TextValue = ""
    Me.ef_card_technician.TextWidth = 50
    Me.ef_card_technician.Value = ""
    Me.ef_card_technician.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_enable_technician
    '
    Me.chk_enable_technician.AutoSize = True
    Me.chk_enable_technician.Location = New System.Drawing.Point(10, 78)
    Me.chk_enable_technician.Name = "chk_enable_technician"
    Me.chk_enable_technician.Size = New System.Drawing.Size(124, 17)
    Me.chk_enable_technician.TabIndex = 2
    Me.chk_enable_technician.Text = "xHabilitar tecnico"
    Me.chk_enable_technician.UseVisualStyleBackColor = True
    '
    'ef_card_stacker_change
    '
    Me.ef_card_stacker_change.DoubleValue = 0.0R
    Me.ef_card_stacker_change.IntegerValue = 0
    Me.ef_card_stacker_change.IsReadOnly = False
    Me.ef_card_stacker_change.Location = New System.Drawing.Point(10, 38)
    Me.ef_card_stacker_change.Name = "ef_card_stacker_change"
    Me.ef_card_stacker_change.OnlyUpperCase = True
    Me.ef_card_stacker_change.PlaceHolder = Nothing
    Me.ef_card_stacker_change.Size = New System.Drawing.Size(209, 24)
    Me.ef_card_stacker_change.SufixText = "Sufix Text"
    Me.ef_card_stacker_change.SufixTextVisible = True
    Me.ef_card_stacker_change.TabIndex = 1
    Me.ef_card_stacker_change.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_card_stacker_change.TextValue = ""
    Me.ef_card_stacker_change.TextWidth = 50
    Me.ef_card_stacker_change.Value = ""
    Me.ef_card_stacker_change.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_enable_stacker_change
    '
    Me.chk_enable_stacker_change.AutoSize = True
    Me.chk_enable_stacker_change.Location = New System.Drawing.Point(10, 20)
    Me.chk_enable_stacker_change.Name = "chk_enable_stacker_change"
    Me.chk_enable_stacker_change.Size = New System.Drawing.Size(189, 17)
    Me.chk_enable_stacker_change.TabIndex = 0
    Me.chk_enable_stacker_change.Text = "xHabilitar cambio de stacker"
    Me.chk_enable_stacker_change.UseVisualStyleBackColor = True
    '
    'gb_smartfloor
    '
    Me.gb_smartfloor.Controls.Add(Me.chk_sf_runner)
    Me.gb_smartfloor.Controls.Add(Me.chk_sf_manager)
    Me.gb_smartfloor.Location = New System.Drawing.Point(551, 83)
    Me.gb_smartfloor.Name = "gb_smartfloor"
    Me.gb_smartfloor.Size = New System.Drawing.Size(224, 46)
    Me.gb_smartfloor.TabIndex = 11
    Me.gb_smartfloor.TabStop = False
    Me.gb_smartfloor.Text = "xSmartFloor"
    '
    'chk_sf_runner
    '
    Me.chk_sf_runner.AutoSize = True
    Me.chk_sf_runner.Location = New System.Drawing.Point(136, 20)
    Me.chk_sf_runner.Name = "chk_sf_runner"
    Me.chk_sf_runner.Size = New System.Drawing.Size(74, 17)
    Me.chk_sf_runner.TabIndex = 4
    Me.chk_sf_runner.Text = "xRunner"
    Me.chk_sf_runner.UseVisualStyleBackColor = True
    '
    'chk_sf_manager
    '
    Me.chk_sf_manager.AutoSize = True
    Me.chk_sf_manager.Location = New System.Drawing.Point(10, 19)
    Me.chk_sf_manager.Name = "chk_sf_manager"
    Me.chk_sf_manager.Size = New System.Drawing.Size(82, 17)
    Me.chk_sf_manager.TabIndex = 3
    Me.chk_sf_manager.Text = "xManager"
    Me.chk_sf_manager.UseVisualStyleBackColor = True
    '
    'pnl_employee
    '
    Me.pnl_employee.Controls.Add(Me.chk_enable_employee)
    Me.pnl_employee.Controls.Add(Me.ef_card_employee)
    Me.pnl_employee.Controls.Add(Me.btn_restore_PIN)
    Me.pnl_employee.Location = New System.Drawing.Point(1, 195)
    Me.pnl_employee.Name = "pnl_employee"
    Me.pnl_employee.Size = New System.Drawing.Size(322, 76)
    Me.pnl_employee.TabIndex = 10
    '
    'frm_user_edit
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(889, 421)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_user_edit"
    Me.Text = "#5272 "
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.gb_login.ResumeLayout(False)
    Me.gb_sales_limit.ResumeLayout(False)
    Me.gb_state.ResumeLayout(False)
    Me.gb_state.PerformLayout()
    Me.gb_change_stacker.ResumeLayout(False)
    Me.gb_change_stacker.PerformLayout()
    Me.gb_smartfloor.ResumeLayout(False)
    Me.gb_smartfloor.PerformLayout()
    Me.pnl_employee.ResumeLayout(False)
    Me.pnl_employee.PerformLayout()
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Members "
  Private m_input_username As String
  Private m_initial_profile_id As Integer = 0
  Private m_profile_id_selected As Integer = -1
  Private DeleteOperation As Boolean
  Private m_password_changed As Boolean = False
  Private m_master As Boolean
  Private m_barcode_handler As KbdMsgFilterHandler
  Private m_continue_operation As Boolean
  Private m_user_id As Integer
#End Region

#Region " Properties "

#End Region ' Properties

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()
    'Me.FormId = ENUM_FORM.FORM_USER_EDIT

    ' TJG 01-FEB-2005
    ' Set the form icon from the embedded resource (ICO file must be built in the project as "Embedded Resource")
    ' Call could be made as GetManifestResourceStream("GUI_Configuration.GUI_Configuration.ico"))

    '------------------------------------------------
    ' XVV 13/04/2007
    ' Translate tu BASE Form
    ' Me.Icon = New Icon(System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream(Me.GetType(), "WigosGUI.ico"))
    '------------------------------------------------

    '------------------------------------------------
    ' XVV 13/04/2007
    ' Call Base Form proc
    ' Call MyBase.GUI_SetFormId()
    '------------------------------------------------
  End Sub 'GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    ' Hide the delete button if ScreenMode is New
    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = (Me.ScreenMode <> frm_base_edit.ENUM_SCREEN_MODE.MODE_NEW)

    If (WSI.Common.MobileBankConfig.IsMobileBankLinkedToADevice) Then
      GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
      GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = True
      GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9016)  'MoviBank
    End If

    'Add any initialization after the MyBase.GUI_InitControls() call

    If Me.m_master Then
      Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2360)
      GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False

      If WSI.Common.GeneralParam.GetBoolean("Site", "MultiSiteMember", False) Then
        GUI_Button(ENUM_BUTTON.BUTTON_OK).Visible = False
        GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)
        ef_full_name.Enabled = False
        gb_login.Enabled = False
        gb_state.Enabled = False
        gb_sales_limit.Enabled = False
        bt_select_profile.Enabled = False
        lbl_master_info.Visible = True
        lbl_master_info.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2361)
      End If
    Else
      If Not WSI.Common.GeneralParam.GetBoolean("MultiSite", "IsCenter", False) Then
        chk_master.Visible = False
      End If

      Me.Text = GLB_NLS_GUI_CONFIGURATION.GetString(325)
    End If

    chk_master.Enabled = GLB_CurrentUser.Permissions(ENUM_FORM.FORM_MASTER_USER_EDIT).Write

    ef_username.Text = GLB_NLS_GUI_CONFIGURATION.GetString(326)
    ef_full_name.Text = GLB_NLS_GUI_CONFIGURATION.GetString(327)
    ef_employee_code.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4704)
    ef_profile.Text = GLB_NLS_GUI_CONFIGURATION.GetString(319)
    ef_password.Text = GLB_NLS_GUI_CONFIGURATION.GetString(305)
    ef_verify_password.Text = GLB_NLS_GUI_CONFIGURATION.GetString(306)
    ef_password_exp.Text = GLB_NLS_GUI_CONFIGURATION.GetString(241)
    ef_password_exp.SufixText = GLB_NLS_GUI_CONFIGURATION.GetString(204)
    chk_master.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2359)
    gb_login.Text = GLB_NLS_GUI_CONFIGURATION.GetString(304)
    gb_state.Text = GLB_NLS_GUI_CONFIGURATION.GetString(259)

    chk_not_expired.Text = GLB_NLS_GUI_CONFIGURATION.GetString(309)
    chk_password_req.Text = GLB_NLS_GUI_CONFIGURATION.GetString(310)

    dtp_password_date.Text = GLB_NLS_GUI_CONFIGURATION.GetString(307)
    Call dtp_password_date.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    dtp_password_date.Value = GUI_GetDate()

    ' User status
    cb_disabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1220)
    cb_unsubscribed.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1223)
    btn_unlock.Text = GLB_NLS_GUI_CONFIGURATION.GetString(89)
    btn_unlock.Enabled = False
    lbl_blocked.Text = ""
    lbl_blocked.Hide()

    ' User status - System block reasons
    chklst_block_reasons.Items.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1221), False) ' Wrong password
    chklst_block_reasons.Items.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1222), False) ' Inactive account

    ' filters
    ' Now, employee code is managed by a regular expresion (general params, user, EmployeeCode.ValidationRegex
    Call Me.ef_employee_code.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_EMPLOYEE_CODE)

    Call ef_username.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_ALPHA_NUMERIC, WSI.Common.PasswordPolicy.MAX_LOGIN_LENGTH_CONST)
    Call ef_full_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_BASE, LEN_FULL_NAME)
    Call ef_profile.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_PROFILE)

    Call ef_password.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, WSI.Common.PasswordPolicy.MAX_PASSWORD_LENGTH_CONST)
    ef_password.Password = True
    ef_password.OnlyUpperCase = False

    Call ef_verify_password.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, WSI.Common.PasswordPolicy.MAX_PASSWORD_LENGTH_CONST)
    ef_verify_password.Password = True
    ef_verify_password.OnlyUpperCase = False

    Call ef_password_exp.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, LEN_PASSWORD_EXP)

    ' Disable username entry field in edition mode.
    ' Note: In edition mode we set onlyUpperCase to false because existing users or users
    ' created or altered out of this form could be in lowercase and then there will be
    ' a change in the username which will be audited but not updated in database.
    If (Me.ScreenMode = frm_base_edit.ENUM_SCREEN_MODE.MODE_EDIT) Then
      ef_username.IsReadOnly = True
      ef_username.OnlyUpperCase = False
    Else
      ef_username.IsReadOnly = False
      ef_username.OnlyUpperCase = True
    End If

    gb_sales_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1180)

    ef_cashier_sales_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1181)
    ef_cashier_sales_limit.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9, 0)

    ef_mb_sales_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1182)
    ef_mb_sales_limit.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9, 0)

    'QMP 13-FEB-2012 If user has only Execute permission (not Write), disable all controls
    '                except for Unlock and Accept buttons (only when in MODE_EDIT)
    '
    'IMPORTANT NOTE
    '-------------------------------------------------------------------------------------------
    'If a control is added or modified, please make sure it is properly disabled when
    'ScreenMode is MODE_EDIT, and user has Execute permission but no Write permission. In this
    'scenario, user is only allowed to Unlock accounts, so make sure no other value changes in any way.

    If Me.Permissions.Execute And Not Me.Permissions.Write And Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT Then
      For Each _control As Control In Me.panel_data.Controls
        Call DisableControls(_control)
      Next

      Me.btn_unlock.Enabled = True
      GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = True
    End If

    If Not GeneralParam.GetBoolean("MultiSite", "IsCenter") AndAlso _
     (Not GeneralParam.GetBoolean("CashDesk.Draw", "IsCashDeskDraw")) Then
      'HBB 17/09/2014: Change Stacker permission
      gb_change_stacker.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5467)
      chk_enable_stacker_change.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5468)
      ef_card_stacker_change.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5469)
      Call ef_card_stacker_change.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TRACK_DATA, TRACK_DATA_LENGTH_WIN)

      'SGB 25/09/2014: Technical permission
      chk_enable_technician.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5574)
      ef_card_technician.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5469)
      Call ef_card_technician.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TRACK_DATA, TRACK_DATA_LENGTH_WIN)

      'OVS 02/11/2016: Employee permission
      Me.btn_restore_PIN.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7790)
      Me.chk_enable_employee.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2224)
      Me.ef_card_employee.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5469)
      Call ef_card_employee.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TRACK_DATA, TRACK_DATA_LENGTH_WIN)

      KeyboardMessageFilter.Init()
      m_barcode_handler = AddressOf Me.BarcodeEvent
      KeyboardMessageFilter.RegisterHandler(Me, BarcodeType.Account, m_barcode_handler)
    Else
      gb_change_stacker.Visible = False
      pnl_employee.Visible = False
    End If

    Me.gb_smartfloor.Visible = WSI.Common.Misc.IsIntelliaEnabled()
    Me.gb_smartfloor.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6851)
    Me.chk_sf_manager.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6850)
    Me.chk_sf_runner.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6934)

    m_password_changed = False

  End Sub 'GUI_InitControls

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim user_item As CLASS_USER
    'XVV Variable not use Dim h_user_button_disabled As Boolean
    'XVV Variable not use Dim profile_name As String
    'XVV Variable not use Dim rc As Integer

    user_item = DbReadObject

    If ((GeneralParam.GetBoolean("MultiSite", "IsCenter", False) Or chk_master.Checked) And WSI.Common.CurrencyMultisite.IsEnabledMultiCurrency) Then
      user_item.SalesLimitCashier = ""
      user_item.SalesLimitMB = ""
    End If

    m_user_id = user_item.UserId
    ef_username.Value = user_item.UserName
    ef_full_name.Value = user_item.FullName
    ef_employee_code.Value = user_item.EmployeeCode
    chk_master.Checked = user_item.GupMaster
    chk_master.Enabled = Not chk_master.Checked

    ' Load profile is defined.
    If (user_item.ProfileId <> -1) Then
      ef_profile.Value = CLASS_PROFILE.GetProfileNameForId(user_item.ProfileId, SqlCtx)
      user_item.ProfileName = ef_profile.Value
    End If

    ' Protect against non-existent items
    If ef_profile.Value <> "" Then
      ' Save profile ID into a member and if user selects another profile, this member is updated.
      ' When GetScreenData is called the value for profile id is obtained from this member.
      m_profile_id_selected = user_item.ProfileId
    End If

    If Me.ScreenMode = frm_base_edit.ENUM_SCREEN_MODE.MODE_NEW Then
      ef_password.Value = ""
      ef_verify_password.Value = ""
    Else
      ef_password.Value = CLASS_USER.UNDEFINED_PWD_STRING
      ef_verify_password.Value = CLASS_USER.UNDEFINED_PWD_STRING
    End If

    dtp_password_date.Value = user_item.PasswordDate

    If user_item.PasswordExp = 0 Then
      ef_password_exp.Value = ""
      chk_not_expired.Checked = True
    Else
      ef_password_exp.Value = user_item.PasswordExp
      chk_not_expired.Checked = False
    End If

    ef_password_exp.Enabled = (chk_not_expired.Checked)
    btn_restore_PIN.Enabled = (Not String.IsNullOrEmpty(user_item.CardEmployee))

    chk_password_req.Checked = (user_item.PasswordReq = CLASS_USER.USER_PASSWORD_REQUIRED)

    ' XCD 14-Aug-2012 Set checked block reasons

    If (user_item.BlockReason And GUI_USER_BLOCK_REASON.WRONG_PASSWORD).Equals(GUI_USER_BLOCK_REASON.WRONG_PASSWORD) Then
      chklst_block_reasons.SetItemChecked(chklst_block_reasons.Items.IndexOf(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1221)), True)
    Else
      chklst_block_reasons.SetItemChecked(chklst_block_reasons.Items.IndexOf(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1221)), False)
    End If

    If (user_item.BlockReason And GUI_USER_BLOCK_REASON.INACTIVITY).Equals(GUI_USER_BLOCK_REASON.INACTIVITY) Then
      chklst_block_reasons.SetItemChecked(chklst_block_reasons.Items.IndexOf(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1222)), True)
    Else
      chklst_block_reasons.SetItemChecked(chklst_block_reasons.Items.IndexOf(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1222)), False)
    End If

    btn_unlock.Enabled = (chklst_block_reasons.CheckedItems.Count > 0)
    cb_disabled.Checked = ((user_item.BlockReason And GUI_USER_BLOCK_REASON.DISABLED).Equals(GUI_USER_BLOCK_REASON.DISABLED))
    cb_unsubscribed.Checked = ((user_item.BlockReason And GUI_USER_BLOCK_REASON.UNSUBSCRIBED).Equals(GUI_USER_BLOCK_REASON.UNSUBSCRIBED))

    ' Sets Text and Show/Hide to status label
    UpdateStatusLabel()

    If Not String.IsNullOrEmpty(user_item.SalesLimitCashier) Then
      ef_cashier_sales_limit.Value = user_item.SalesLimitCashier
    Else
      ef_cashier_sales_limit.Value = ""
    End If

    If Not String.IsNullOrEmpty(user_item.SalesLimitMB) Then
      ef_mb_sales_limit.Value = user_item.SalesLimitMB
    Else
      ef_mb_sales_limit.Value = ""
    End If

    chk_enable_stacker_change.Checked = Not String.IsNullOrEmpty(user_item.CardNumber)
    ef_card_stacker_change.Enabled = chk_enable_stacker_change.Checked
    If Not String.IsNullOrEmpty(user_item.CardNumber) Then
      ef_card_stacker_change.TextValue = user_item.CardNumber
    End If

    chk_enable_technician.Checked = Not String.IsNullOrEmpty(user_item.CardTechnician)
    ef_card_technician.Enabled = chk_enable_technician.Checked
    If Not String.IsNullOrEmpty(user_item.CardTechnician) Then
      ef_card_technician.TextValue = user_item.CardTechnician
    End If

    chk_enable_employee.Checked = Not String.IsNullOrEmpty(user_item.CardEmployee)
    ef_card_employee.Enabled = chk_enable_employee.Checked
    If Not String.IsNullOrEmpty(user_item.CardEmployee) Then
      ef_card_employee.TextValue = user_item.CardEmployee
    End If

    gb_sales_limit.Visible = Not ((GeneralParam.GetBoolean("MultiSite", "IsCenter", False) Or chk_master.Checked) And WSI.Common.CurrencyMultisite.IsEnabledMultiCurrency)

    If WSI.Common.Misc.IsIntelliaEnabled() Then
      Me.chk_sf_manager.Checked = user_item.IntelliaManager
      Me.chk_sf_runner.Checked = user_item.IntelliaRunner
    End If
  End Sub 'GUI_SetScreenData

  ' PURPOSE: Validate the data presented on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - True:  Data is ok
  '     - False: Data is NOT ok
  '
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Dim nls_param1 As String
    Dim _pwd_policy As PasswordPolicy
    Dim user_item As CLASS_USER
    Dim user_unsubscribed As Boolean
    Static Dim first_time As Boolean = True
    Dim _user_id As Int32
    Dim _max_users As Integer
    Dim _employee_code_wrong As Boolean
    Dim _pattern As String

    _pwd_policy = New PasswordPolicy()

    user_item = DbReadObject

    ' Detect changes in unsubscribed status.
    ' When field values were restarted, unsubscribed status is false
    user_unsubscribed = IIf(first_time, IIf((user_item.BlockReason() And GUI_USER_BLOCK_REASON.UNSUBSCRIBED).Equals(GUI_USER_BLOCK_REASON.UNSUBSCRIBED), _
                                True, False), _
                              False)

    ' Enter only when unsubscribed status changes
    If user_unsubscribed Xor cb_unsubscribed.Checked() Then
      If cb_unsubscribed.Checked Then
        If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(158), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, , ef_username.Value) = ENUM_MB_RESULT.MB_RESULT_NO Then
          cb_unsubscribed.Checked = False
          UpdateStatusLabel()

          Return False
        End If
      Else
        If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(159), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, , ef_username.Value) = ENUM_MB_RESULT.MB_RESULT_YES Then

          'Delete password
          ef_password.Value = ""
          ef_verify_password.Value = ""
          'dtp_password_date.Value = Date.Now()

          'chk_not_expired.Checked = True
          'chk_password_req.Checked = False

          'cb_disabled.Checked = False
          'chklst_block_reasons.SetItemChecked(chklst_block_reasons.Items.IndexOf(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1221)), False) ' Wrong password
          'chklst_block_reasons.SetItemChecked(chklst_block_reasons.Items.IndexOf(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1222)), False) ' Inactive account

          'ef_cashier_sales_limit.Value = ""
          'ef_mb_sales_limit.Value = ""

          ' Next validation fails and don't need to reset User values
          first_time = False
        Else
          cb_unsubscribed.Checked = True
          UpdateStatusLabel()

          Return False
        End If
      End If
    End If

    ' Update Label
    UpdateStatusLabel()

    ' blank field
    If ef_username.Value = "" Then
      nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(326)
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , nls_param1)
      Call ef_username.Focus()

      Return False
    End If

    ' blank field
    If ef_full_name.Value = "" Then
      nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(327)
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , nls_param1)
      Call ef_full_name.Focus()

      Return False
    End If

    'valid format full name
    If Not ef_full_name.ValidateFormat() Then
      Call ef_full_name.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.ef_full_name.Text)
      Return False
    End If

    ' blank field
    ' Do not get value from screen because entry field only hold the name and not the id.
    ' Every time the entry fiels is changed, the member m_profile_selected is updated
    If Not m_profile_id_selected > 0 Then
      nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(319)
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , nls_param1)
      Call bt_select_profile.Focus()

      Return False
    End If

    ' blank field
    If ef_password.TextValueWithoutTrim = "" Then
      nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(305)
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , nls_param1)
      Call ef_password.Focus()

      Return False
    End If

    ' verify password
    If ef_password.TextValueWithoutTrim <> ef_verify_password.TextValueWithoutTrim Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(121), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call ef_password.Focus()

      Return False
    End If

    If CInt(GUI_ParseNumber(ef_password_exp.Value)) = 0 And Not chk_not_expired.Checked Then
      nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(308)
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(125), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , nls_param1)
      Call ef_password_exp.Focus()

      Return False
    End If

    If Me.ScreenMode = frm_base_edit.ENUM_SCREEN_MODE.MODE_NEW And Not _pwd_policy.CheckLoginRules(ef_username.TextValue) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
      ef_username.Focus()

      Return False
    End If

    If Me.ScreenMode = frm_base_edit.ENUM_SCREEN_MODE.MODE_NEW Then
      _user_id = -1
    Else
      _user_id = user_item.UserId
    End If

    If (m_password_changed Or Me.ScreenMode = frm_base_edit.ENUM_SCREEN_MODE.MODE_NEW) And _
        Not _pwd_policy.CheckPasswordRules(Environment.MachineName, ef_username.TextValue, _user_id, ef_password.TextValueWithoutTrim) And _
        Not chk_password_req.Checked Then

      Call NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1218), _pwd_policy.MessagePasswordPolicyInvalid(), ENUM_MB_TYPE.MB_TYPE_WARNING)

      ef_password.Focus()

      Return False
    End If

    ' Validate format of the fields
    If Not ef_username.ValidateFormat() Then
      Call ef_username.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_username.Text)
      Return False
    End If

    If Not ef_full_name.ValidateFormat() Then
      Call ef_full_name.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_full_name.Text)
      Return False
    End If

    If Not ef_cashier_sales_limit.ValidateFormat() Then
      Call ef_cashier_sales_limit.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_cashier_sales_limit.Text)
      Return False
    End If

    If Not ef_mb_sales_limit.ValidateFormat() Then
      Call ef_mb_sales_limit.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_mb_sales_limit.Text)
      Return False
    End If

    'Validate if can select this profile
    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW OrElse _
      (Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT And user_item.ProfileId <> m_profile_id_selected) Then
      If Not CLASS_PROFILE.CanAssignMoreUsers(Me.m_profile_id_selected, _max_users) Then
        Call Me.ef_profile.Focus()
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1762), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _max_users.ToString, ef_profile.Value)

        Return False
      End If
    End If

    'Master User with Master Profile
    If chk_master.Checked Then
      If Not CLASS_PROFILE.GetGupMaster(Me.m_profile_id_selected) Then
        Call bt_select_profile.Focus()
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2367), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)

        Return False
      End If
    End If

    If Not String.IsNullOrEmpty(Me.ef_employee_code.Value) Then
      _employee_code_wrong = False
      Try
        _pattern = WSI.Common.GeneralParam.GetString("User", "EmployeeCode.ValidationRegex")
        If Not String.IsNullOrEmpty(_pattern) Then
          _employee_code_wrong = Not ValidateFormat.CheckRegularExpression(Me.ef_employee_code.Value, _pattern)
        End If
      Catch
        _employee_code_wrong = True
      End Try

      If _employee_code_wrong Then
        ef_employee_code.Focus()
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4705), ENUM_MB_TYPE.MB_TYPE_WARNING)

        Return False
      End If
    End If

    If chk_enable_stacker_change.Checked AndAlso String.IsNullOrEmpty(ef_card_stacker_change.TextValue) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5579), ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(5468).ToLower)
      ef_card_stacker_change.Focus()
      Return False
    End If

    If chk_enable_technician.Checked AndAlso String.IsNullOrEmpty(ef_card_technician.TextValue) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5579), ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(5574).ToLower)
      ef_card_technician.Focus()
      Return False
    End If

    If chk_enable_employee.Checked AndAlso String.IsNullOrEmpty(ef_card_employee.TextValue) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5579), ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2224).ToLower)
      ef_card_employee.Focus()
      Return False
    End If

    If (Not String.IsNullOrEmpty(ef_card_technician.TextValue)) And (Not String.IsNullOrEmpty(ef_card_stacker_change.TextValue)) And _
      (ef_card_technician.TextValue = ef_card_stacker_change.TextValue) Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7796), ENUM_MB_TYPE.MB_TYPE_WARNING)
      ef_card_stacker_change.Focus()
      Return False
    End If

    Return True

  End Function 'GUI_IsScreenDataOk

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_GetScreenData()

    Dim user_item As CLASS_USER

    user_item = DbEditedObject

    user_item.UserName = ef_username.Value
    user_item.FullName = ef_full_name.Value
    user_item.EmployeeCode = ef_employee_code.Value
    user_item.GupMaster = chk_master.Checked

    ' Do not get value from screen because entry field only hold the name and not the id.
    ' Every time the entry fiels is changed, the member m_profile_selected is updated
    user_item.ProfileId = m_profile_id_selected

    user_item.ProfileName = ef_profile.Value

    ' If paswword is not changed, it is not assigned
    If ef_password.TextValueWithoutTrim = CLASS_USER.UNDEFINED_PWD_STRING Then
      user_item.Password = ""
    Else
      user_item.Password = ef_password.TextValueWithoutTrim
    End If

    ' Password date
    user_item.PasswordDate = dtp_password_date.Value

    ' Password not expires
    If chk_not_expired.Checked Then
      user_item.PasswordExp = 0
    Else
      user_item.PasswordExp = CInt(GUI_ParseNumber(ef_password_exp.Value))
      If user_item.PasswordExp = 0 Then
        user_item.PasswordExp = -1
      End If
    End If

    ' Require password change
    If chk_password_req.Checked Then
      user_item.PasswordReq = CLASS_USER.USER_PASSWORD_REQUIRED
    Else
      user_item.PasswordReq = CLASS_USER.USER_PASSWORD_NOT_REQUIRED
    End If

    ' Block reason
    user_item.BlockReason = GUI_USER_BLOCK_REASON.NONE

    If cb_disabled.Checked Then
      user_item.BlockReason += GUI_USER_BLOCK_REASON.DISABLED
    End If

    If cb_unsubscribed.Checked Then
      user_item.BlockReason += GUI_USER_BLOCK_REASON.UNSUBSCRIBED
    End If

    If chklst_block_reasons.CheckedItems.Contains(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1221)) Then
      user_item.BlockReason += GUI_USER_BLOCK_REASON.WRONG_PASSWORD
    End If

    If chklst_block_reasons.CheckedItems.Contains(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1222)) Then
      user_item.BlockReason += GUI_USER_BLOCK_REASON.INACTIVITY
    End If

    If Not String.IsNullOrEmpty(ef_cashier_sales_limit.Value) Then
      user_item.SalesLimitCashier = ef_cashier_sales_limit.Value
    Else
      user_item.SalesLimitCashier = ""
    End If

    If Not String.IsNullOrEmpty(ef_mb_sales_limit.Value) Then
      user_item.SalesLimitMB = ef_mb_sales_limit.Value
    Else
      user_item.SalesLimitMB = ""
    End If

    If chk_enable_stacker_change.Checked And Not String.IsNullOrEmpty(ef_card_stacker_change.TextValue) Then
      user_item.CardNumber = ef_card_stacker_change.TextValue
    Else
      user_item.CardNumber = ""
    End If

    If chk_enable_technician.Checked And Not String.IsNullOrEmpty(ef_card_technician.TextValue) Then
      user_item.CardTechnician = ef_card_technician.TextValue
    Else
      user_item.CardTechnician = ""
    End If

    If chk_enable_employee.Checked And Not String.IsNullOrEmpty(ef_card_employee.TextValue) Then
      user_item.CardEmployee = ef_card_employee.TextValue
    Else
      user_item.CardEmployee = ""
    End If

    If WSI.Common.Misc.IsIntelliaEnabled() Then
      user_item.IntelliaManager = Me.chk_sf_manager.Checked
      user_item.IntelliaRunner = Me.chk_sf_runner.Checked
    End If

  End Sub 'GUI_GetScreenData

  ' PURPOSE: Database overridable operations.
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)
    Dim user_class As CLASS_USER
    Dim _user_card As UserCard
    Dim nls_param1 As String
    Dim nls_param2 As String
    Dim aux_date As Date
    Dim rc As mdl_NLS.ENUM_MB_RESULT
    Dim _pwd_policy As PasswordPolicy

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_USER()
        user_class = DbEditedObject

        If Me.ScreenMode = frm_base_edit.ENUM_SCREEN_MODE.MODE_NEW Then
          _pwd_policy = New PasswordPolicy()

          If _pwd_policy.ChangeInNDays <> 0 Then
            user_class.PasswordExp = _pwd_policy.ChangeInNDays
          End If

          user_class.PasswordReq = 1
        End If

        aux_date = GUI_GetDate()

        user_class.UserId = 0
        user_class.ProfileId = m_initial_profile_id
        user_class.BlockReason = GUI_USER_BLOCK_REASON.NONE
        user_class.PasswordDate = aux_date
        user_class.GupMaster = Me.m_master

        DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          user_class = Me.DbEditedObject
          nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(326)
          nls_param2 = m_input_username
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(102), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1, _
                          nls_param2)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          user_class = Me.DbEditedObject
          nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(326)
          nls_param2 = user_class.UserName
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(110), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1, _
                          nls_param2)
          Call ef_username.Focus()
        ElseIf Me.DbStatus = ENUM_STATUS.STATUS_PASSWORD Then
          user_class = Me.DbEditedObject
          nls_param1 = user_class.UserName
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(126), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1)
          Call ef_password.Focus()
        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK And m_continue_operation Then
          user_class = Me.DbEditedObject
          nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(326)
          nls_param2 = user_class.UserName
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(104), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1, _
                          nls_param2)
          'ElseIf Me.DbStatus = ENUM_STATUS.STATUS_OK Then

          '  ' only for insert a new mobile bank
          '  Dim _user_reload As CLASS_GUI_USER
          '  _user_reload = New CLASS_GUI_USER

          '  _user_reload.GetUserInDb(Me.ef_username.Value, 0)


          '  Me.m_mobile_bank.UserId = _user_reload.Id
          '  Me.m_mobile_bank.HolderName = Me.ef_username.Value
          '  If Permissions.Write Then
          '    Call insertMobileBankDeviceMode()
          '  End If
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_INSERT,
           frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE
        Dim _assigned_message As String = String.Empty

        m_continue_operation = True

        user_class = Me.DbEditedObject

        If chk_enable_stacker_change.Checked Then
          _assigned_message = GetUsersWithTheSameTrackData(ef_card_stacker_change.TextValue, user_class)
        End If

        If chk_enable_technician.Checked _
          And ef_card_technician.TextValue <> ef_card_stacker_change.TextValue Then
          _assigned_message &= GetUsersWithTheSameTrackData(ef_card_technician.TextValue, user_class)
        End If

        If chk_enable_employee.Checked _
          And ef_card_employee.TextValue <> ef_card_stacker_change.TextValue And ef_card_employee.TextValue <> ef_card_technician.TextValue Then
          _assigned_message &= GetUsersWithTheSameTrackData(ef_card_employee.TextValue, user_class)
        End If

        If Not String.IsNullOrEmpty(_assigned_message) Then
          If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7779), _
                        ENUM_MB_TYPE.MB_TYPE_WARNING,
                        ENUM_MB_BTN.MB_BTN_YES_NO, _
                        ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
                        _assigned_message) = ENUM_MB_RESULT.MB_RESULT_NO Then

            m_continue_operation = False
          End If
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_INSERT,
           frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_UPDATE
        If m_continue_operation Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        Else
          DbStatus = ENUM_STATUS.STATUS_ERROR
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus = ENUM_STATUS.STATUS_OK Then
          user_class = DbEditedObject

          ' Reset num incorrect pin errors if user have a employeee card
          If (Not String.IsNullOrEmpty(user_class.CardEmployee)) Then
            _user_card = New UserCard(user_class.UserId, user_class.CardEmployee, WCP_CardTypes.CARD_TYPE_EMPLOYEE)

            If _user_card.Status = UserCardStatus.Ok Then
              _user_card.ResetNumTrys()
            End If
          End If

        ElseIf Me.DbStatus = ENUM_STATUS.STATUS_PASSWORD Then
          _pwd_policy = New PasswordPolicy()

          If _pwd_policy.RulesEnabled And chk_password_req.Checked Then
            _pwd_policy.DisableRule()
          End If

          _pwd_policy.MessagePasswordPolicyInvalid()
          Call NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1218), _pwd_policy.MessagePasswordPolicyInvalid(), ENUM_MB_TYPE.MB_TYPE_WARNING)

          Call ef_password.Focus()
        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK And m_continue_operation Then
          user_class = Me.DbEditedObject
          nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(326)
          nls_param2 = user_class.UserName
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(106), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1, _
                          nls_param2)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus = ENUM_STATUS.STATUS_DEPENDENCIES And DeleteOperation Then
          user_class = Me.DbEditedObject
          nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(326)
          nls_param2 = user_class.UserName
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(146), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1)

        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK And DeleteOperation Then
          user_class = Me.DbEditedObject
          nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(326)
          nls_param2 = user_class.UserName
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(115), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1, _
                          nls_param2)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE
        DeleteOperation = False
        user_class = Me.DbEditedObject
        nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(326)
        nls_param2 = user_class.UserName
        rc = NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(111), _
                        ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        ENUM_MB_BTN.MB_BTN_YES_NO, _
                        ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
                        nls_param1, _
                        nls_param2)
        If rc = ENUM_MB_RESULT.MB_RESULT_YES Then
          DeleteOperation = True
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_DELETE
        If DeleteOperation Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        Else
          DbStatus = ENUM_STATUS.STATUS_ERROR
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub 'GUI_DB_Operation

  ' PURPOSE: Manage permissions.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_Permissions(ByRef Perm As CLASS_GUI_USER.TYPE_PERMISSIONS)

    Dim user_item As CLASS_USER

    user_item = DbReadObject

    Me.m_master = user_item.GupMaster

    If WSI.Common.GeneralParam.GetBoolean("Site", "MultiSiteMember", False) Then
      Perm.Write = Perm.Write And Not user_item.GupMaster
      Perm.Delete = Perm.Delete And Not user_item.GupMaster
    End If

    If Me.m_master Then
      Perm.Delete = False
    End If

  End Sub 'GUI_Permissions

  ' PURPOSE: Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetInitialFocus()
    If Me.ScreenMode = frm_base_edit.ENUM_SCREEN_MODE.MODE_NEW Then
      Me.ActiveControl = Me.ef_username
    Else
      Me.ActiveControl = Me.ef_full_name
    End If
  End Sub 'GUI_SetInitialFocus

  ' PURPOSE: Init form in new mode
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overloads Sub ShowNewItem(Optional ByVal ProfileId As Integer = CLASS_PROFILE.INVALID_PROFILE_ID)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    Me.m_master = False

    DbObjectId = Nothing
    m_initial_profile_id = ProfileId
    DbStatus = ENUM_STATUS.STATUS_OK

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub 'ShowNewItem

  ' PURPOSE: Init form in new master mode
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Sub ShowNewMaster(Optional ByVal ProfileId As Integer = CLASS_PROFILE.INVALID_PROFILE_ID)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    Me.m_master = True

    DbObjectId = Nothing
    m_initial_profile_id = ProfileId
    DbStatus = ENUM_STATUS.STATUS_OK

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub 'ShowNewMaster

  ' PURPOSE: Init form in edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '       - UserId
  '       - Username
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overloads Sub ShowEditItem(ByVal UserId As Integer, ByVal Username As String)
    Dim user_item As CLASS_USER

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Me.DbObjectId = UserId
    Me.m_input_username = Username

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    user_item = DbReadObject
    Me.m_master = user_item.GupMaster

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If
  End Sub 'ShowEditItem

  ' PURPOSE: Overrides the GUI_ButtonClick to allow the user to Unlock accounts
  '          when they have Execute permission but no Write permission.
  '
  '  PARAMS:
  '     - INPUT:
  '       - ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)
    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_OK
        Call SaveUser()

      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        Call openMobileBankDeviceMode() 'MoviBank

      Case ENUM_BUTTON.BUTTON_CANCEL
        'If m_is_mobile_bank_edited Then
        '  If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8784), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_YES Then
        '    Call MyBase.GUI_ButtonClick(ButtonId)
        '  End If
        'Else
        Call MyBase.GUI_ButtonClick(ButtonId)
        'End If

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)

    End Select
  End Sub ' GUI_ButtonClick

  ''' <summary>
  ''' Save user data
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SaveUser()
    'When user has Execute permission but not Write permission, allow only to Unlock
    If Permissions.Execute And (Not Permissions.Write) And Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT Then
      Me.ActiveControl = Me.AcceptButton

      Call GUI_GetScreenData()

      If DbReadObject.AuditorData.IsEqual(DbEditedObject.AuditorData) Then
        If Me.CloseOnOkClick Then
          Call Me.Close()
        End If

        Exit Sub
      End If

      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE)
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_UPDATE)
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE)
      If DbStatus = ENUM_STATUS.STATUS_OK Then
        Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AUDIT_UPDATE)
      End If
    Else
      If ActiveControl.Name = ef_password.Name Or ActiveControl.Name = ef_verify_password.Name Then
        System.Windows.Forms.SendKeys.Send("{TAB}")
      Else
        Call MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_OK)

        If DbStatus = ENUM_STATUS.STATUS_OK Then
          'Save mmobile data
          Dim _user_edited_item As CLASS_USER
          _user_edited_item = DbEditedObject

          Dim _user_read_item As CLASS_USER
          _user_read_item = DbReadObject

          If (Permissions.Write AndAlso (_user_edited_item.MobileBank.AuditorData.Equals(_user_read_item.MobileBank.AuditorData))) Then
            Call SaveMobileBankDataInDeviceMode(_user_edited_item.MobileBank)
          End If
        End If
      End If
    End If
  End Sub

  'PURPOSE: Executed just before closing form
  '
  ' PARAMS:
  '    - INPUT :
  '
  '    - OUTPUT :
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)
    KeyboardMessageFilter.UnregisterHandler(Me, m_barcode_handler)
    MyBase.GUI_Closing(CloseCanceled)
  End Sub

#End Region

#Region " Private Functions "

  ' PURPOSE: Update label with current status
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Sub UpdateStatusLabel()
    lbl_blocked.Hide()

    If (chklst_block_reasons.CheckedItems.Count > 0) Then
      lbl_blocked.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(558).ToUpper()
      lbl_blocked.Show()
    End If

    If cb_disabled.Checked() Then
      lbl_blocked.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1220).ToUpper()
      lbl_blocked.Show()
    End If

    If cb_unsubscribed.Checked() Then
      lbl_blocked.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1223).ToUpper()
      lbl_blocked.Show()
    End If
  End Sub

  ' PURPOSE: Disable all controls in the form (putting them in
  '          ReadOnly mode if possible)
  '
  '  PARAMS:
  '     - INPUT:
  '           - Container as Control
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - none
  Private Sub DisableControls(ByVal Container As Control)

    Dim _control As Control

    If TypeOf (Container) Is uc_entry_field Then
      CType(Container, uc_entry_field).IsReadOnly = True
    ElseIf TypeOf (Container) Is uc_date_picker Then
      CType(Container, uc_date_picker).IsReadOnly = True
    ElseIf Container.HasChildren Then
      For Each _control In Container.Controls
        Call DisableControls(_control)
      Next
    Else
      If TypeOf (Container) Is CheckBox Then
        CType(Container, CheckBox).Enabled = False
      ElseIf TypeOf (Container) Is CheckedListBox Then
        CType(Container, CheckedListBox).Enabled = False
      ElseIf TypeOf (Container) Is Button Then
        CType(Container, Button).Enabled = False
      End If
    End If
  End Sub ' DisableControls

  Private Function GetUsersWithTheSameTrackData(TrackData As String, User As CLASS_USER) As String
    Dim _users As List(Of UserCard)
    Dim _message As StringBuilder
    Dim _user_card_operation As New UserCardOperation

    _users = New List(Of UserCard)
    _message = New StringBuilder()
    _user_card_operation = New UserCardOperation()

    _user_card_operation.CardLinkedToAnotherUsers(TrackData, User.UserId, _users)

    If _users.Count > 0 Then
      For Each _user_linked As UserCard In _users
        _message.AppendLine(String.Format("{0} ({1}: {2}): {3}", _user_linked.HolderName, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7415), GetCardTypeName(_user_linked.CardType), _user_linked.TrackData))
      Next
    End If

    Return _message.ToString()
  End Function

  Private Function GetCardTypeName(CardType As Integer)
    Select Case (CardType)
      Case WCP_CardTypes.CARD_TYPE_EMPLOYEE
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(2224)

      Case WCP_CardTypes.CARD_TYPE_TECH
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(5574)

      Case WCP_CardTypes.CARD_TYPE_CHANGE_STACKER
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(5468)

      Case Else
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(5020) 'Unknown
    End Select
  End Function

  ''' <summary>
  ''' Opne form to edit mobile bank in device mode
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub openMobileBankDeviceMode()
    Dim _user_item As CLASS_USER
    Dim _old_cursor As Windows.Forms.Cursor

    _user_item = DbEditedObject

    _old_cursor = Windows.Forms.Cursor.Current
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    Try
      'Edit bank mobile
      Using _frm_edit_mobile_bank = New frm_mobile_bank_edit_device
        _frm_edit_mobile_bank.ShowForEdit(Me, m_profile_id_selected, m_input_username, _user_item.MobileBank)
      End Using
    Finally
      Windows.Forms.Cursor.Current = _old_cursor
    End Try
  End Sub

  ''' <summary>
  ''' Save mobile bank data
  ''' </summary>
  ''' <param name="MobileBankData"></param>
  ''' <remarks></remarks>
  Private Sub SaveMobileBankDataInDeviceMode(MobileBankData As cls_mobile_bank)
    Dim frm As frm_mobile_bank_edit_device
    frm = New frm_mobile_bank_edit_device

    If (MobileBankData Is Nothing) Then
      Return
    End If

    If (MobileBankData.ID = 0) Then
      frm.InsertMobileBank(MobileBankData)
    Else
      frm.UpdateMobileBank(MobileBankData)
    End If
  End Sub

#End Region

#Region " Events "

  ' PURPOSE: When user set 'password never expires' disables the 'number of days password is valid'
  '
  '  PARAMS:
  '     - INPUT:
  '           - sender
  '           - event args
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Sub chk_not_expired_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk_not_expired.Click
    ef_password_exp.Enabled = (Not chk_not_expired.Checked)
  End Sub

  ' PURPOSE: Launch profile selection form and get a profile id
  '
  '  PARAMS:
  '     - INPUT:
  '           - sender
  '           - event args
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Sub bt_select_profile_ClickEvent() Handles bt_select_profile.ClickEvent
    Dim profiles_selected() As Integer
    Dim frm_p As frm_user_profiles_sel

    Dim ctx As Integer

    Try
      'XVV 17/04/2007
      'Comment this function, because are obsolete (Oracle version)
      'GUI_SqlCtxAlloc(ctx)

      frm_p = New frm_user_profiles_sel
      profiles_selected = frm_p.ShowForSelect(frm_user_profiles_sel.ENUM_RESULT_TYPE.RT_PROFILE, chk_master.Checked)
      If Not IsNothing(profiles_selected) Then
        If profiles_selected.Length > 0 Then
          ' get profile name from database
          ef_profile.Value = CLASS_PROFILE.GetProfileNameForId(profiles_selected(0), ctx)
          ' Save ID into a member to get it (not the name) when GetScreenData function is called
          m_profile_id_selected = profiles_selected(0)
        End If
      End If
    Finally
      'XVV 17/04/2007
      'Comment this function, because are obsolete (Oracle version)
      'GUI_SqlCtxRelease(ctx)
    End Try
  End Sub

  ' PURPOSE: Set systems block reasons form to false
  '
  '  PARAMS:
  '     - INPUT:
  '           - sender
  '           - event args
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Sub btn_unlock_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_unlock.Click
    chklst_block_reasons.SetItemChecked(chklst_block_reasons.Items.IndexOf(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1221)), False) ' Wrong password
    chklst_block_reasons.SetItemChecked(chklst_block_reasons.Items.IndexOf(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1222)), False) ' Inactive account
    btn_unlock.Enabled = False
    UpdateStatusLabel()
  End Sub

  ' PURPOSE: Manages barcode received events
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Sub BarcodeEvent(ByVal e As KbdMsgEvent, ByVal Data As Object)
    Try
      If Not Data Is Nothing Then
        Dim _data_read As WSI.Common.Barcode = DirectCast(Data, WSI.Common.Barcode)

        If ef_password.IsFocused Then
          ef_password.TextValue = _data_read.ExternalCode
        End If

        If ef_verify_password.IsFocused Then
          ef_verify_password.TextValue = _data_read.ExternalCode
        End If

        If ef_card_stacker_change.IsFocused Or (chk_enable_stacker_change.Checked And chk_enable_stacker_change.Focused) Then
          ef_card_stacker_change.TextValue = _data_read.ExternalCode
        End If

        If ef_card_technician.IsFocused Or (chk_enable_technician.Checked And chk_enable_technician.Focused) Then
          ef_card_technician.TextValue = _data_read.ExternalCode
        End If

        If ef_card_employee.IsFocused Or (chk_enable_employee.Checked And chk_enable_employee.Focused) Then
          ef_card_employee.TextValue = _data_read.ExternalCode
        End If
        If chk_enable_technician.Checked Xor chk_enable_stacker_change.Checked Xor chk_enable_employee.Checked Then
          If chk_enable_stacker_change.Checked Then
            ef_card_stacker_change.TextValue = _data_read.ExternalCode
          ElseIf chk_enable_technician.Checked Then
            ef_card_technician.TextValue = _data_read.ExternalCode
          Else
            ef_card_employee.TextValue = _data_read.ExternalCode
          End If
        End If

      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub cb_disabled_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cb_disabled.Click
    UpdateStatusLabel()
  End Sub

  Private Sub cb_unsubscribed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cb_unsubscribed.Click
    UpdateStatusLabel()
  End Sub

  Private Sub ef_password_EntryFieldValueChanged() Handles ef_password.EntryFieldValueChanged
    m_password_changed = True
  End Sub

  Private Sub chk_enable_stacker_change_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_enable_stacker_change.Click
    ef_card_stacker_change.Enabled = chk_enable_stacker_change.Checked
    If Not ef_card_stacker_change.Enabled Then
      ef_card_stacker_change.TextValue = ""
    End If
  End Sub

  Private Sub chk_enable_technician_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_enable_technician.Click
    ef_card_technician.Enabled = chk_enable_technician.Checked
    If Not ef_card_technician.Enabled Then
      ef_card_technician.TextValue = ""
    End If
  End Sub

  Private Sub chk_enable_employee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_enable_employee.Click
    ef_card_employee.Enabled = chk_enable_employee.Checked
    If Not ef_card_employee.Enabled Then
      ef_card_employee.TextValue = ""
      btn_restore_PIN.Enabled = False
    Else
      btn_restore_PIN.Enabled = True
    End If
  End Sub

  ' PURPOSE: Reset to PIN for employee card
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub btn_restore_PIN_Click(sender As Object, e As EventArgs) Handles btn_restore_PIN.Click

    Dim _user_card As UserCard
    Dim _track_data As String
    Dim _user_id As Object

    Try
      _track_data = ef_card_employee.Value

      _user_id = Me.DbObjectId
      _user_card = New UserCard(_user_id, _track_data, WCP_CardTypes.CARD_TYPE_EMPLOYEE)

      Select Case _user_card.Status
        Case UserCardStatus.Ok
          If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7795), ENUM_MB_TYPE.MB_TYPE_WARNING, _
              ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) <> ENUM_MB_RESULT.MB_RESULT_NO Then
            If Not _user_card.UpdatePin(String.Empty) Then
              ' Error
              Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6185), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, , , , , , )
            Else
              ' Pin reset OK
              Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7791), ENUM_MB_TYPE.MB_TYPE_INFO, ENUM_MB_BTN.MB_BTN_OK, , , , , , )
            End If
          End If

        Case UserCardStatus.NoExist
          ' Card not found
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7792), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, , _track_data, , , , )

        Case Else
      End Select
    Catch ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, Me.Name, "GUI_GetScreenData", ex.Message)
    End Try

  End Sub
#End Region

End Class
