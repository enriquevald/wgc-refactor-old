<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_gaming_tables_types_edit
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.chk_enabled = New System.Windows.Forms.CheckBox()
    Me.txt_name = New GUI_Controls.uc_entry_field()
    Me.cb_AFIP_types = New System.Windows.Forms.ComboBox()
    Me.lbl_afip_type = New System.Windows.Forms.Label()
    Me.panel_data.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.lbl_afip_type)
    Me.panel_data.Controls.Add(Me.cb_AFIP_types)
    Me.panel_data.Controls.Add(Me.chk_enabled)
    Me.panel_data.Controls.Add(Me.txt_name)
    Me.panel_data.Size = New System.Drawing.Size(418, 160)
    '
    'chk_enabled
    '
    Me.chk_enabled.AutoSize = True
    Me.chk_enabled.Location = New System.Drawing.Point(144, 68)
    Me.chk_enabled.Name = "chk_enabled"
    Me.chk_enabled.Size = New System.Drawing.Size(78, 17)
    Me.chk_enabled.TabIndex = 20
    Me.chk_enabled.Text = "xEnabled"
    '
    'txt_name
    '
    Me.txt_name.DoubleValue = 0.0R
    Me.txt_name.IntegerValue = 0
    Me.txt_name.IsReadOnly = False
    Me.txt_name.Location = New System.Drawing.Point(21, 31)
    Me.txt_name.Name = "txt_name"
    Me.txt_name.PlaceHolder = Nothing
    Me.txt_name.Size = New System.Drawing.Size(384, 24)
    Me.txt_name.SufixText = "Sufix Text"
    Me.txt_name.SufixTextVisible = True
    Me.txt_name.TabIndex = 19
    Me.txt_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.txt_name.TextValue = ""
    Me.txt_name.TextWidth = 120
    Me.txt_name.Value = ""
    Me.txt_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'cb_AFIP_types
    '
    Me.cb_AFIP_types.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.cb_AFIP_types.FormattingEnabled = True
    Me.cb_AFIP_types.Location = New System.Drawing.Point(144, 109)
    Me.cb_AFIP_types.Name = "cb_AFIP_types"
    Me.cb_AFIP_types.Size = New System.Drawing.Size(253, 21)
    Me.cb_AFIP_types.TabIndex = 21
    '
    'lbl_afip_type
    '
    Me.lbl_afip_type.AutoSize = True
    Me.lbl_afip_type.Location = New System.Drawing.Point(74, 113)
    Me.lbl_afip_type.Name = "lbl_afip_type"
    Me.lbl_afip_type.Size = New System.Drawing.Size(65, 13)
    Me.lbl_afip_type.TabIndex = 22
    Me.lbl_afip_type.Text = "AFIP Type"
    Me.lbl_afip_type.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'frm_gaming_tables_types_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(520, 169)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_gaming_tables_types_edit"
    Me.Text = "frm_gaming_tables_types_edit"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents chk_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents txt_name As GUI_Controls.uc_entry_field
  Friend WithEvents cb_AFIP_types As System.Windows.Forms.ComboBox
  Friend WithEvents lbl_afip_type As System.Windows.Forms.Label
End Class
