﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_reserved_machines_sel
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.uc_terminals = New GUI_Controls.uc_provider()
    Me.uc_accounts = New GUI_Controls.uc_account_sel()
    Me.chk_terminals_location = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.chk_terminals_location)
    Me.panel_filter.Controls.Add(Me.uc_accounts)
    Me.panel_filter.Controls.Add(Me.uc_terminals)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1030, 228)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_terminals, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_accounts, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_terminals_location, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 232)
    Me.panel_data.Size = New System.Drawing.Size(1030, 394)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1024, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1024, 4)
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(2, 2)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(303, 92)
    Me.gb_date.TabIndex = 12
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 45)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(258, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 18)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(258, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'uc_terminals
    '
    Me.uc_terminals.FilterByCurrency = False
    Me.uc_terminals.Location = New System.Drawing.Point(316, -2)
    Me.uc_terminals.Name = "uc_terminals"
    Me.uc_terminals.Size = New System.Drawing.Size(393, 211)
    Me.uc_terminals.TabIndex = 13
    Me.uc_terminals.TerminalListHasValues = False
    '
    'uc_accounts
    '
    Me.uc_accounts.Account = ""
    Me.uc_accounts.AccountText = ""
    Me.uc_accounts.Anon = False
    Me.uc_accounts.AutoSize = True
    Me.uc_accounts.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_accounts.BirthDate = New Date(CType(0, Long))
    Me.uc_accounts.DisabledHolder = False
    Me.uc_accounts.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.uc_accounts.Holder = ""
    Me.uc_accounts.Location = New System.Drawing.Point(-2, 93)
    Me.uc_accounts.MassiveSearchNumbers = ""
    Me.uc_accounts.MassiveSearchNumbersToEdit = ""
    Me.uc_accounts.MassiveSearchType = 0
    Me.uc_accounts.Name = "uc_accounts"
    Me.uc_accounts.SearchTrackDataAsInternal = True
    Me.uc_accounts.ShowMassiveSearch = False
    Me.uc_accounts.ShowVipClients = False
    Me.uc_accounts.Size = New System.Drawing.Size(307, 108)
    Me.uc_accounts.TabIndex = 14
    Me.uc_accounts.Telephone = ""
    Me.uc_accounts.TrackData = ""
    Me.uc_accounts.Vip = False
    Me.uc_accounts.WeddingDate = New Date(CType(0, Long))
    '
    'chk_terminals_location
    '
    Me.chk_terminals_location.AutoSize = True
    Me.chk_terminals_location.Location = New System.Drawing.Point(334, 184)
    Me.chk_terminals_location.Name = "chk_terminals_location"
    Me.chk_terminals_location.Size = New System.Drawing.Size(136, 17)
    Me.chk_terminals_location.TabIndex = 5
    Me.chk_terminals_location.Text = "xTerminals location"
    Me.chk_terminals_location.UseVisualStyleBackColor = True
    '
    'frm_reserved_machines_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1040, 630)
    Me.Name = "frm_reserved_machines_sel"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_reserved_machines_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents uc_terminals As GUI_Controls.uc_provider
  Friend WithEvents uc_accounts As GUI_Controls.uc_account_sel
  Friend WithEvents chk_terminals_location As System.Windows.Forms.CheckBox
End Class
