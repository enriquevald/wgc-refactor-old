﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_customers_entrances_visit_sel
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.grp_box_date = New System.Windows.Forms.GroupBox()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.chk_visits_by_customer = New System.Windows.Forms.CheckBox()
    Me.chk_total_entries_by_month = New System.Windows.Forms.CheckBox()
    Me.grp_box_accum = New System.Windows.Forms.GroupBox()
    Me.opt_only_visits = New System.Windows.Forms.RadioButton()
    Me.opt_visits_entries = New System.Windows.Forms.RadioButton()
    Me.lbl_numero_total_entradas = New System.Windows.Forms.Label()
    Me.lbl_total_entradas = New System.Windows.Forms.Label()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.grp_box_date.SuspendLayout()
    Me.grp_box_accum.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.lbl_numero_total_entradas)
    Me.panel_filter.Controls.Add(Me.lbl_total_entradas)
    Me.panel_filter.Controls.Add(Me.grp_box_accum)
    Me.panel_filter.Controls.Add(Me.chk_total_entries_by_month)
    Me.panel_filter.Controls.Add(Me.chk_visits_by_customer)
    Me.panel_filter.Controls.Add(Me.grp_box_date)
    Me.panel_filter.Size = New System.Drawing.Size(1305, 132)
    Me.panel_filter.TabIndex = 1
    Me.panel_filter.Controls.SetChildIndex(Me.grp_box_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_visits_by_customer, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_total_entries_by_month, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.grp_box_accum, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_total_entradas, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_numero_total_entradas, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 136)
    Me.panel_data.Size = New System.Drawing.Size(1305, 579)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1299, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1299, 4)
    '
    'grp_box_date
    '
    Me.grp_box_date.Controls.Add(Me.dtp_to)
    Me.grp_box_date.Controls.Add(Me.dtp_from)
    Me.grp_box_date.Location = New System.Drawing.Point(16, 17)
    Me.grp_box_date.Name = "grp_box_date"
    Me.grp_box_date.Size = New System.Drawing.Size(210, 100)
    Me.grp_box_date.TabIndex = 1
    Me.grp_box_date.TabStop = False
    Me.grp_box_date.Text = "GroupBox1"
    '
    'dtp_to
    '
    Me.dtp_to.AutoSize = True
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(10, 53)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(190, 25)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 13
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2010, 5, 10, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(10, 20)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = False
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(190, 25)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 12
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2010, 5, 10, 0, 0, 0, 0)
    '
    'chk_visits_by_customer
    '
    Me.chk_visits_by_customer.AutoSize = True
    Me.chk_visits_by_customer.Location = New System.Drawing.Point(236, 46)
    Me.chk_visits_by_customer.Name = "chk_visits_by_customer"
    Me.chk_visits_by_customer.Size = New System.Drawing.Size(91, 17)
    Me.chk_visits_by_customer.TabIndex = 8
    Me.chk_visits_by_customer.Text = "CheckBox1"
    Me.chk_visits_by_customer.UseVisualStyleBackColor = True
    '
    'chk_total_entries_by_month
    '
    Me.chk_total_entries_by_month.AutoSize = True
    Me.chk_total_entries_by_month.Location = New System.Drawing.Point(236, 72)
    Me.chk_total_entries_by_month.Name = "chk_total_entries_by_month"
    Me.chk_total_entries_by_month.Size = New System.Drawing.Size(91, 17)
    Me.chk_total_entries_by_month.TabIndex = 9
    Me.chk_total_entries_by_month.Text = "CheckBox2"
    Me.chk_total_entries_by_month.UseVisualStyleBackColor = True
    '
    'grp_box_accum
    '
    Me.grp_box_accum.Controls.Add(Me.opt_only_visits)
    Me.grp_box_accum.Controls.Add(Me.opt_visits_entries)
    Me.grp_box_accum.Location = New System.Drawing.Point(233, 17)
    Me.grp_box_accum.Name = "grp_box_accum"
    Me.grp_box_accum.Size = New System.Drawing.Size(200, 100)
    Me.grp_box_accum.TabIndex = 11
    Me.grp_box_accum.TabStop = False
    Me.grp_box_accum.Text = "GroupBox1"
    '
    'opt_only_visits
    '
    Me.opt_only_visits.AutoSize = True
    Me.opt_only_visits.Location = New System.Drawing.Point(13, 67)
    Me.opt_only_visits.Name = "opt_only_visits"
    Me.opt_only_visits.Size = New System.Drawing.Size(101, 17)
    Me.opt_only_visits.TabIndex = 1
    Me.opt_only_visits.TabStop = True
    Me.opt_only_visits.Text = "RadioButton2"
    Me.opt_only_visits.UseVisualStyleBackColor = True
    '
    'opt_visits_entries
    '
    Me.opt_visits_entries.AutoSize = True
    Me.opt_visits_entries.Location = New System.Drawing.Point(13, 29)
    Me.opt_visits_entries.Name = "opt_visits_entries"
    Me.opt_visits_entries.Size = New System.Drawing.Size(101, 17)
    Me.opt_visits_entries.TabIndex = 0
    Me.opt_visits_entries.TabStop = True
    Me.opt_visits_entries.Text = "RadioButton1"
    Me.opt_visits_entries.UseVisualStyleBackColor = True
    '
    'lbl_numero_total_entradas
    '
    Me.lbl_numero_total_entradas.AutoSize = True
    Me.lbl_numero_total_entradas.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_numero_total_entradas.Location = New System.Drawing.Point(1121, 105)
    Me.lbl_numero_total_entradas.Name = "lbl_numero_total_entradas"
    Me.lbl_numero_total_entradas.Size = New System.Drawing.Size(19, 18)
    Me.lbl_numero_total_entradas.TabIndex = 18
    Me.lbl_numero_total_entradas.Text = "0"
    '
    'lbl_total_entradas
    '
    Me.lbl_total_entradas.AutoSize = True
    Me.lbl_total_entradas.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_total_entradas.Location = New System.Drawing.Point(995, 105)
    Me.lbl_total_entradas.Name = "lbl_total_entradas"
    Me.lbl_total_entradas.Size = New System.Drawing.Size(117, 18)
    Me.lbl_total_entradas.TabIndex = 17
    Me.lbl_total_entradas.Text = "xTotalEntradas"
    '
    'frm_customers_entrances_visit_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1313, 719)
    Me.Name = "frm_customers_entrances_visit_sel"
    Me.Text = "frm_customers_entrances_visit_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.grp_box_date.ResumeLayout(False)
    Me.grp_box_date.PerformLayout()
    Me.grp_box_accum.ResumeLayout(False)
    Me.grp_box_accum.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents chk_total_entries_by_month As System.Windows.Forms.CheckBox
  Friend WithEvents chk_visits_by_customer As System.Windows.Forms.CheckBox
  Friend WithEvents grp_box_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents grp_box_accum As System.Windows.Forms.GroupBox
  Friend WithEvents opt_only_visits As System.Windows.Forms.RadioButton
  Friend WithEvents opt_visits_entries As System.Windows.Forms.RadioButton
  Friend WithEvents lbl_numero_total_entradas As System.Windows.Forms.Label
  Friend WithEvents lbl_total_entradas As System.Windows.Forms.Label
End Class
