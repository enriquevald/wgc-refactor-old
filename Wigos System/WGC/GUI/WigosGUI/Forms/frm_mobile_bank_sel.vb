'-------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME :       frm_mobile_bank_sel.vb
'
' DESCRIPTION :       Window for view mobile bank and sesion which are joined
'
' AUTHOR:             Didac Campanals
'
' CREATION DATE:      03-NOV-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 03-NOV-2014  DCS    Initial version   
' 24-NOV-2014  DCS    Fixed Bug WIG-1736 - Index out of range in filter
'--------------------------------------------------------------------

Option Strict Off
Option Explicit On

#Region " Imports "
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Text
#End Region ' Imports

Public Class frm_mobile_bank_sel
  Inherits frm_base_sel

#Region " Constants "

  ' SQL Columns
  Private Const SQL_COLUMN_MB_ID As Integer = 0
  Private Const SQL_COLUMN_MB_TYPE As Integer = 1
  Private Const SQL_COLUMN_MB_NAME As Integer = 2
  Private Const SQL_COLUMN_MB_EMPLOYEE_CODE As Integer = 3
  Private Const SQL_COLUMN_MB_ALLOWED As Integer = 4
  Private Const SQL_COLUMN_MB_LIMIT_BY_SESSION As Integer = 5
  Private Const SQL_COLUMN_MB_LIMIT_RECHARGE As Integer = 6
  Private Const SQL_COLUMN_MB_LIMIT_NUM_RECHARGE As Integer = 7

  ' Grid Columns
  Private Const GRID_COLUMN_ID As Integer = 0
  Private Const GRID_COLUMN_MB_ID As Integer = 1
  Private Const GRID_COLUMN_MB_NAME As Integer = 2
  Private Const GRID_COLUMN_MB_EMPLOYEE_CODE As Integer = 4
  Private Const GRID_COLUMN_MB_ALLOWED As Integer = 3
  Private Const GRID_COLUMN_MB_LIMIT_BY_SESSION As Integer = 5
  Private Const GRID_COLUMN_MB_LIMIT_RECHARGE As Integer = 6
  Private Const GRID_COLUMN_MB_LIMIT_NUM_RECHARGE As Integer = 7

  ' Width Columns
  Private Const GRID_WIDTH_ID As Integer = 200
  Private Const GRID_WIDTH_MB_ID As Integer = 600
  Private Const GRID_WIDTH_MB_NAME As Integer = 2250
  Private Const GRID_WIDTH_MB_ALLOWED As Integer = 1000
  Private Const GRID_WIDTH_MB_EMPLOYEE_CODE As Integer = 2000
  Private Const GRID_WIDTH_MB_LIMIT_RECHARGE As Integer = 1850
  Private Const GRID_WIDTH_MB_LIMIT_NUM_RECHARGE As Integer = 2100
  Private Const GRID_WIDTH_MB_LIMIT_BY_SESSION As Integer = 1900

  ' Other Constants
  Private Const GRID_COLUMNS As Integer = 8
  Private Const GRID_HEADER_ROWS As Integer = 2

#End Region ' Constants

#Region " Members "
  ' Report filters
  Private m_mobile_bank As String
  Private m_blocked As String
#End Region ' Members.

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_MOBILE_BANK_SEL

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False

    ' Form Name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5687) ' 5987 - "Bancos m�viles"

    ' Mobile Bank Selector
    Me.gb_mobile_bank.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1182) ' Mobile Bank
    Me.rb_mb_all.Text = GLB_NLS_GUI_INVOICING.GetString(219) ' All
    Me.rb_mb_one.Text = GLB_NLS_GUI_INVOICING.GetString(218) ' One

    ' Mobile Bank Status
    Me.gb_blocked.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(558)      ' Locked
    Me.chk_blocked_yes.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(698) ' Yes
    Me.chk_blocked_no.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(699)  ' No

    Call FillCombos()

    'Initialize Controls
    Me.gb_mobile_bank.Focus()

    ' Initialize Grid
    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

    ' Add  handlers
    AddHandler rb_mb_all.CheckedChanged, AddressOf rb_checked_event
    AddHandler rb_mb_one.CheckedChanged, AddressOf rb_checked_event

  End Sub ' GUI_InitControls

  ' PURPOSE: Call function to reset Filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()

  End Sub ' GUI_FilterReset

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _sb As StringBuilder
    _sb = New StringBuilder()

    _sb.AppendLine(" SELECT   MB_ACCOUNT_ID                                                                                            ")
    _sb.AppendLine("        , MB_ACCOUNT_TYPE                                                                                          ")
    _sb.AppendLine("        , MB_HOLDER_NAME                                                                                           ")
    _sb.AppendLine("        , MB_EMPLOYEE_CODE                                                                                         ")
    _sb.AppendLine("        , MB_BLOCKED                                                                                               ")
    _sb.AppendLine("        , ISNULL(MB_TOTAL_LIMIT, 0) AS MB_TOTAL_LIMIT                                                              ")
    _sb.AppendLine("        , ISNULL(MB_RECHARGE_LIMIT, 0) AS MB_RECHARGE_LIMIT                                                        ")
    _sb.AppendLine("        , ISNULL(MB_NUMBER_OF_RECHARGES_LIMIT, 0) AS MB_NUMBER_OF_RECHARGES_LIMIT                                  ")
    _sb.AppendLine("   FROM   MOBILE_BANKS                                                                                             ")
    _sb.AppendLine("  WHERE   MB_ACCOUNT_TYPE IN (" & WSI.Common.MB_USER_TYPE.ANONYMOUS & ", " & WSI.Common.MB_USER_TYPE.PERSONAL & ") ")
    _sb.AppendLine("          " & GetSqlWhere())
    _sb.AppendLine("  ORDER   BY  MB_ACCOUNT_ID                                                                                        ")

    Return _sb.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    If Not DbRow.IsNull(SQL_COLUMN_MB_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_ID).Value = DbRow.Value(SQL_COLUMN_MB_ID)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_MB_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_NAME).Value = DbRow.Value(SQL_COLUMN_MB_NAME)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_MB_EMPLOYEE_CODE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_EMPLOYEE_CODE).Value = DbRow.Value(SQL_COLUMN_MB_EMPLOYEE_CODE)
    End If

    If DbRow.Value(SQL_COLUMN_MB_ALLOWED) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_ALLOWED).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(558) ' Locked
    End If

    If Not DbRow.IsNull(SQL_COLUMN_MB_LIMIT_RECHARGE) AndAlso DbRow.Value(SQL_COLUMN_MB_LIMIT_RECHARGE) > 0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_LIMIT_RECHARGE).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_MB_LIMIT_RECHARGE), 2)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_LIMIT_RECHARGE).Value = String.Empty
    End If

    If Not DbRow.IsNull(SQL_COLUMN_MB_LIMIT_NUM_RECHARGE) AndAlso DbRow.Value(SQL_COLUMN_MB_LIMIT_NUM_RECHARGE) > 0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_LIMIT_NUM_RECHARGE).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_MB_LIMIT_NUM_RECHARGE), 0)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_LIMIT_NUM_RECHARGE).Value = String.Empty
    End If

    If Not DbRow.IsNull(SQL_COLUMN_MB_LIMIT_BY_SESSION) AndAlso DbRow.Value(SQL_COLUMN_MB_LIMIT_BY_SESSION) > 0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_LIMIT_BY_SESSION).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_MB_LIMIT_BY_SESSION), 2)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_LIMIT_BY_SESSION).Value = String.Empty
    End If

    Return True
  End Function ' GUI_SetupRow

  ' PURPOSE: Call to child window to show details for the selected row
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ShowSelectedItem()
    Dim _idx_row As Integer
    Dim _mobile_bank_id As Integer
    Dim _frm As frm_mobile_bank_edit

    _mobile_bank_id = -1

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    If Not String.IsNullOrEmpty(Me.Grid.Cell(_idx_row, GRID_COLUMN_MB_ID).Value) Then
      _mobile_bank_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_MB_ID).Value
    End If

    ' When not valid id exit
    If _mobile_bank_id < 0 Then
      Return
    End If

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_mobile_bank_edit
    _frm.ShowForEdit(Me.MdiParent, _mobile_bank_id)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub ' GUI_ShowSelectedItem

  ' PURPOSE: Process button actions in order to branch to a child screen
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)
    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY
        Call MyBase.GUI_ButtonClick(ButtonId)

      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        Call GUI_ShowSelectedItem()

      Case ENUM_BUTTON.BUTTON_PRINT
        Call MyBase.GUI_ButtonClick(ButtonId)

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

#End Region ' OVERRIDES

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    ' Filter mobile bank
    PrintData.SetFilter(Me.gb_mobile_bank.Text, m_mobile_bank)

    ' Filter blocked
    PrintData.SetFilter(Me.gb_blocked.Text, m_blocked)

    PrintData.FilterHeaderWidth(2) = 1600
    PrintData.FilterValueWidth(2) = 2000
    PrintData.FilterHeaderWidth(4) = 500
    PrintData.FilterValueWidth(4) = 1800
  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()
    ' Filter mobile bank
    If Me.rb_mb_one.Checked Then
      m_mobile_bank = Me.cmb_mb.TextValue
    Else
      m_mobile_bank = GLB_NLS_GUI_INVOICING.GetString(219)
    End If

    ' Filter blocked
    If Me.chk_blocked_yes.Checked And Not Me.chk_blocked_no.Checked Then
      m_blocked = Me.chk_blocked_yes.Text
    End If

    If Not Me.chk_blocked_yes.Checked And Me.chk_blocked_no.Checked Then
      m_blocked = Me.chk_blocked_no.Text
    End If

    If (Not Me.chk_blocked_yes.Checked And Not Me.chk_blocked_no.Checked) Or (Me.chk_blocked_yes.Checked And Me.chk_blocked_no.Checked) Then
      m_blocked = Me.chk_blocked_yes.Text & " / " & Me.chk_blocked_no.Text ' Yes / NO
    End If
  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - MdiParent
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_SELECTION

    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    With Me.Grid
      ' Initialize Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' MB INDEX
      .Column(GRID_COLUMN_ID).Header(0).Text = " "
      .Column(GRID_COLUMN_ID).Header(1).Text = " "
      .Column(GRID_COLUMN_ID).Width = GRID_WIDTH_ID
      .Column(GRID_COLUMN_ID).HighLightWhenSelected = False
      .Column(GRID_COLUMN_ID).IsColumnPrintable = False

      ' MB INDEX
      .Column(GRID_COLUMN_MB_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1182) ' "Mobile Bank"
      .Column(GRID_COLUMN_MB_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2285) ' "ID"
      .Column(GRID_COLUMN_MB_ID).Width = GRID_WIDTH_MB_ID
      .Column(GRID_COLUMN_MB_ID).Mapping = SQL_COLUMN_MB_ID

      ' MB NAME
      .Column(GRID_COLUMN_MB_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1182) ' "Mobile Bank"
      .Column(GRID_COLUMN_MB_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3419) ' "Nombre"
      .Column(GRID_COLUMN_MB_NAME).Width = GRID_WIDTH_MB_NAME
      .Column(GRID_COLUMN_MB_NAME).Mapping = SQL_COLUMN_MB_NAME

      ' MB EMPLOYEE CODE
      .Column(GRID_COLUMN_MB_EMPLOYEE_CODE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1182) ' "Mobile Bank"
      .Column(GRID_COLUMN_MB_EMPLOYEE_CODE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4704) ' "Employee code"
      .Column(GRID_COLUMN_MB_EMPLOYEE_CODE).Width = GRID_WIDTH_MB_EMPLOYEE_CODE
      .Column(GRID_COLUMN_MB_EMPLOYEE_CODE).Mapping = SQL_COLUMN_MB_EMPLOYEE_CODE

      ' MB ALLOWED
      .Column(GRID_COLUMN_MB_ALLOWED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1182) ' "Mobile Bank"
      .Column(GRID_COLUMN_MB_ALLOWED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(558)  ' "Locked"
      .Column(GRID_COLUMN_MB_ALLOWED).Width = GRID_WIDTH_MB_ALLOWED
      .Column(GRID_COLUMN_MB_ALLOWED).Mapping = SQL_COLUMN_MB_ALLOWED

      ' MB RECHARGE LIMIT
      .Column(GRID_COLUMN_MB_LIMIT_RECHARGE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5689)  ' 5689 - "Maximum limits for recharge"
      .Column(GRID_COLUMN_MB_LIMIT_RECHARGE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5690)  ' 5690 - "Maximum recharge"
      .Column(GRID_COLUMN_MB_LIMIT_RECHARGE).Width = GRID_WIDTH_MB_LIMIT_RECHARGE
      .Column(GRID_COLUMN_MB_LIMIT_RECHARGE).Mapping = SQL_COLUMN_MB_LIMIT_RECHARGE

      ' MB NUM RECHARGE LIMIT
      .Column(GRID_COLUMN_MB_LIMIT_NUM_RECHARGE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5689)  ' 5689 - "Maximum limits for recharge"
      .Column(GRID_COLUMN_MB_LIMIT_NUM_RECHARGE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5691)  ' 5691 - "Number of recharges by session"
      .Column(GRID_COLUMN_MB_LIMIT_NUM_RECHARGE).Width = GRID_WIDTH_MB_LIMIT_NUM_RECHARGE
      .Column(GRID_COLUMN_MB_LIMIT_NUM_RECHARGE).Mapping = SQL_COLUMN_MB_LIMIT_NUM_RECHARGE

      ' MB BY SESSION LIMIT
      .Column(GRID_COLUMN_MB_LIMIT_BY_SESSION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5689)  ' 5689 - "Maximum limits for recharge"
      .Column(GRID_COLUMN_MB_LIMIT_BY_SESSION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5698)  ' 5698 - "Total recharging by session"
      .Column(GRID_COLUMN_MB_LIMIT_BY_SESSION).Width = GRID_WIDTH_MB_LIMIT_BY_SESSION
      .Column(GRID_COLUMN_MB_LIMIT_BY_SESSION).Mapping = SQL_COLUMN_MB_LIMIT_BY_SESSION
    End With
  End Sub 'GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()
    Me.rb_mb_all.Checked = True
    Me.cmb_mb.Enabled = False
    Me.chk_blocked_yes.Checked = False
    Me.chk_blocked_no.Checked = False
  End Sub ' SetDefaultValues

  ' PURPOSE: Fill combo values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub FillCombos()
    Dim _sb As StringBuilder
    _sb = New StringBuilder()

    ' Fill combo mobile banks
    _sb.AppendLine("   SELECT   MB_ACCOUNT_ID                                                         ")
    _sb.AppendLine("          , (CASE WHEN MB_HOLDER_NAME IS NULL                                     ")
    _sb.AppendLine("                   THEN CAST(MB_ACCOUNT_ID as nvarchar)                           ")
    _sb.AppendLine("                   ELSE CAST(MB_ACCOUNT_ID as nvarchar) + ' - ' + MB_HOLDER_NAME  ")
    _sb.AppendLine("             END) AS MB_NAME                                                      ")
    _sb.AppendLine("     FROM   MOBILE_BANKS                                                          ")
    _sb.AppendLine("    WHERE   MB_ACCOUNT_TYPE <> " & WSI.Common.MB_USER_TYPE.SYS_ACCEPTOR)
    _sb.AppendLine("      AND   MB_ACCOUNT_TYPE <> " & WSI.Common.MB_USER_TYPE.SYS_PROMOBOX)
    _sb.AppendLine(" ORDER BY   MB_ACCOUNT_ID, MB_HOLDER_NAME                                         ")

    Call SetCombo(Me.cmb_mb, _sb.ToString)
    _sb.Length = 0

  End Sub ' FillCombos

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the main SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  ' NOTES:
  '     - Can't filter by uc_account_sel1 when IsCashInOperation is FALSE
  Private Function GetSqlWhere() As String
    Dim _str_where As String = ""

    If Me.rb_mb_one.Checked = True Then
      _str_where = _str_where & " AND MB_ACCOUNT_ID = " & Me.cmb_mb.Value & " "
    End If

    If Me.chk_blocked_yes.Checked = True And Not Me.chk_blocked_no.Checked Then
      _str_where = _str_where & " AND MB_BLOCKED = " & 1 & " "
    End If

    If Not Me.chk_blocked_yes.Checked = True And Me.chk_blocked_no.Checked Then
      _str_where = _str_where & " AND MB_BLOCKED = " & 0 & " "
    End If

    Return _str_where

  End Function ' GetSqlWhere

#End Region ' Private Functions

#Region " Events"

  ' PURPOSE: Handler to enable or disable combos when "all" radio button is checked
  '
  '  PARAMS:
  '     - INPUT:
  '           - Sender Object
  '           - Event Args
  '     - OUTPUT:
  '           - None
  ' RETURNS:
  '     
  Private Sub rb_checked_event(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _object As RadioButton = sender

    ' Get GroupBox
    Select Case _object.Parent.Text
      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(1182) ' Mobile Bank
        If (_object.Text = GLB_NLS_GUI_INVOICING.GetString(219)) Then
          cmb_mb.Enabled = False
        Else
          cmb_mb.Enabled = True
        End If

      Case Else
        ' Do nothing - Unknown button
    End Select

  End Sub ' rb_checked_event

#End Region ' Events

End Class