'-------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_gaming_tables_configuration_edit.vb
' DESCRIPTION:   Configuration of gaming tables 
' AUTHOR:        Javier Barea
' CREATION DATE: 24-JAN-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 24-JAN-2014  JBP    Initial version.
' 28-JAN-2014  DLL    Added controls
' 06-MAY-2014  DLL    Fixed Bug WIG-885 check if all gaming table session are closed
' 29-MAY-2014  SMN    Add new General Param
' 05-OCT-2015  ETP    Fixed Bug 1688: Automatic distribution with negative value are not allowed. Don't save distribution if is disabled.
' 02-NOV-2015  ECP    Backlog Item 5638 - Task 5723 Gambling Table Parameters, eliminate functionality: chip's automatic distribuition configuration
' 29-JUN-2016  FOS    Product Backlog Item 13403: Tables: GUI modifications (Phase 1)
' 06-JUL-2016  FOS    Product Backlog Item 15070: Tables: GUI configuration (Phase 4)
' 30-AGO-2016  ESE    Bug 17124: Parameters are not saved correctly
' 08-SEP-2016  FOS    PBI 16373: Tables: Split PlayerTracking to GamingTables (Phase 5)
' 22-MAR-2017  JML    PBI 25975:MES10 Ticket validation - Configuration
' 27-MAR-2017  JML    PBI 25975:MES10 Ticket validation - Configuration  -  Fixed bug:   WIGOS-683 GUI -Gaming Tables: delaer copy option is enabled when player tracking gaming table parameters is disabled
' 14-JUN-2017  DHA    PBI 27996:WIGOS-2482 - Dealer copy ticket - Validation in gaming table
' 05-JUL-2017  RAB    PBI 28421: WIGOS-1425 MES10 Ticket validation - Dealer copy tickets configuration
' 14-JUL-2017  DHA    Bug 28794:WIGOS-3230 [Ticket #6556] Doesn't satisfy recharge limit
' 04-SEP-2017  RAB    PBI 28942: WIGOS-3412 MES16 - Points assignment: Configuration
'--------------------------------------------------------------------

#Region " Imports "

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_Controls.frm_base_sel
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE
Imports GUI_CommonMisc
Imports System.Text
Imports WSI.Common

#End Region

Public Class frm_gaming_tables_configuration_edit
  Inherits frm_base_edit

#Region "Constants"
  Private Const FORM_DB_MIN_VERSION As Short = 173
  Private Const OLD_CHIPS_DENOMINATION As String = "X01"
#End Region             'Constants

#Region "Members"
  Private m_gaming_table_config As CLASS_GAMING_TABLES_CONFIGURATION
  Private m_dt_allowed_currencies As DataTable

#End Region               'Members

#Region "Properties"

#End Region              'Properties

#Region "Overrides"

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_GAMING_TABLES_CONFIG_EDIT
    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)
    Call MyBase.GUI_SetFormId()

  End Sub

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    ' Gaming Tables Configuration
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2901) ' Gambling table parameters

    ' Gaming Tables Mode
    Me.cmb_gaming_tables_mode.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4556)
    Me.cmb_gaming_tables_mode.Add(WSI.Common.GamingTableBusinessLogic.GT_MODE.DISABLED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7562))
    Me.cmb_gaming_tables_mode.Add(WSI.Common.GamingTableBusinessLogic.GT_MODE.GUI_AND_CASHIER, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7563))
    Me.cmb_gaming_tables_mode.Add(WSI.Common.GamingTableBusinessLogic.GT_MODE.GUI, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7564))
    Me.chk_player_tracking.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7566)
    Me.gb_params.Text = GLB_NLS_GUI_CONFIGURATION.GetString(452) ' Configuration
    Me.chk_stock_control.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4550)
    Me.chk_insert_visits.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4552)
    Me.ef_max_allowed_purchase.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4546)
    Me.ef_max_allowed_purchase.SufixText = ""
    Me.ef_max_allowed_purchase.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 2)
    Me.ef_max_allowed_sale.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4545)
    Me.ef_max_allowed_sale.SufixText = ""
    Me.ef_max_allowed_sale.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 2)
    Me.chk_dealercopy_barcode.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4919)
    Me.chk_dealercopy_signature.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(658)
    Me.ef_dealercopy_title.Text = String.Empty
    Me.ef_dealercopy_title.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50, 0)
    Me.lbl_dealercopy_title.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(392)
    Me.gb_cashiermode.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4918)
    Me.cmb_cashier_mode.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4920)
    Me.cmb_cashier_mode.Add(CLASS_GAMING_TABLES_CONFIGURATION.CASHIER_MODE_PURCHASE.CHIPS, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2941))
    Me.cmb_cashier_mode.Add(CLASS_GAMING_TABLES_CONFIGURATION.CASHIER_MODE_PURCHASE.TICKET, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2109))

    Me.chk_cashier_concilition_enable.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7978)
    Me.chk_cashier_concilition_enable.Visible = Not WSI.Common.TITO.Utils.IsTitoMode()

    Me.chk_cashier_concilition_check_account.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8394)
    Me.chk_cashier_concilition_check_account.Visible = Not WSI.Common.TITO.Utils.IsTitoMode()

    'Controls:
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_DELETE).Visible = False
    Me.btn_ticket_configuration.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8473) ' Config.

#If DEBUG Then
    Me.btn_ticket_configuration.Visible = WSI.Common.TITO.Utils.IsTitoMode()
#Else
    Me.btn_ticket_configuration.Visible = False
#End If

    Me.cmb_gaming_tables_points_assignment_mode.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8607) ' Points assignment mode
    Me.cmb_gaming_tables_points_assignment_mode.Add(CLASS_GAMING_TABLES_CONFIGURATION.ASSIGNMENT_POINTS_MODE.AUTOMATIC, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8020)) ' Automatic mode. Default value
    Me.cmb_gaming_tables_points_assignment_mode.Add(CLASS_GAMING_TABLES_CONFIGURATION.ASSIGNMENT_POINTS_MODE.MANUAL, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8019)) ' Manual mode
    Me.lbl_gaming_tables_points_assignment_days.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7310) ' Days
    Me.pnl_gaming_tables_points_assignment_mode.Visible = False

    Call AddCurrenciesTab()

  End Sub ' GUI_InitControls

  Private Function IsGamingTablesEnabled() As Boolean
    Return (cmb_gaming_tables_mode.Value <> WSI.Common.GamingTableBusinessLogic.GT_MODE.DISABLED)
  End Function

  ' PURPOSE: Validate the data presented on the screen. The following values are not empty: Holder Name, RFC, CURP
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - True:  Data is ok
  '     - False: Data is NOT ok
  '
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    If (IsGamingTablesEnabled()) Then

      If GUI_ParseNumber(ef_max_allowed_purchase.Value) < 0 Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2951), ENUM_MB_TYPE.MB_TYPE_ERROR, , , ef_max_allowed_purchase.Text)
        Call Me.ef_max_allowed_purchase.Focus()

        Return False
      End If

      If GUI_ParseNumber(ef_max_allowed_sale.Value) < 0 Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2951), ENUM_MB_TYPE.MB_TYPE_ERROR, , , ef_max_allowed_sale.Text)
        Call Me.ef_max_allowed_sale.Focus()

        Return False
      End If
    Else
      If Not m_gaming_table_config.IsAllGamingTableSessionClossed() Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4923), ENUM_MB_TYPE.MB_TYPE_WARNING)

        Return False
      End If
    End If

    Return True

  End Function 'GUI_IsScreenDataOk

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_GetScreenData()
    Try
      If Me.IsDisposed Then
        Return
      End If
      Dim _gaming_table_new As CLASS_GAMING_TABLES_CONFIGURATION
      Dim _gaming_table_old As CLASS_GAMING_TABLES_CONFIGURATION

      m_gaming_table_config = Me.DbEditedObject

      Call GetAllowedCurrenciesValues()

      m_gaming_table_config.Mode = Me.cmb_gaming_tables_mode.Value
      m_gaming_table_config.PlayerTracking = Me.chk_player_tracking.Checked

      m_gaming_table_config.ChipsStockControl = Me.chk_stock_control.Checked
      m_gaming_table_config.EnabledInsertVisits = Me.chk_insert_visits.Checked


      m_gaming_table_config.DealercopyMode = Me.cmb_cashier_mode.Value
      m_gaming_table_config.DealercopyBarcode = Me.chk_dealercopy_barcode.Checked
      m_gaming_table_config.DealercopySignature = Me.chk_dealercopy_signature.Checked
      m_gaming_table_config.DealercopyTitle = Me.ef_dealercopy_title.Value

      m_gaming_table_config.CashierConcilitionEnable = Me.chk_cashier_concilition_enable.Checked
      m_gaming_table_config.CashierConciliationCheckAccount = Me.chk_cashier_concilition_check_account.Checked

      _gaming_table_new = DbEditedObject
      _gaming_table_old = DbReadObject

      ValidateTables(_gaming_table_old, _gaming_table_new)

      If Me.cmb_gaming_tables_points_assignment_mode.SelectedIndex = CLASS_GAMING_TABLES_CONFIGURATION.ASSIGNMENT_POINTS_MODE.MANUAL Then
        m_gaming_table_config.AssignmentPointsMode = Int32.Parse(Me.txt_gaming_tables_points_assignment_days.Text)
      ElseIf Me.cmb_gaming_tables_points_assignment_mode.SelectedIndex = CLASS_GAMING_TABLES_CONFIGURATION.ASSIGNMENT_POINTS_MODE.AUTOMATIC Then
        m_gaming_table_config.AssignmentPointsMode = 0
      End If

    Catch ex As Exception
      ' Save error in log
      Log.Error("Gaming tables configuration: Error when save configuration.")
    End Try

  End Sub 'GUI_GetScreenData

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _dealer_mode_enabled As Boolean

    If Me.IsDisposed Then
      Return
    End If

    If (m_gaming_table_config.Mode = GamingTableBusinessLogic.GT_MODE.DISABLED) Then
      Me.gb_params.Enabled = False
      Me.gb_cashiermode.Enabled = False ' 14-OCT-2014 SMN
    End If

    Me.cmb_gaming_tables_mode.Value = m_gaming_table_config.Mode
    Me.chk_player_tracking.Checked = m_gaming_table_config.PlayerTracking

    Me.chk_stock_control.Checked = m_gaming_table_config.ChipsStockControl
    Me.chk_insert_visits.Checked = m_gaming_table_config.EnabledInsertVisits

    Me.ef_max_allowed_purchase.Value = m_gaming_table_config.ChipsMaxAllowedPurchases
    Me.ef_max_allowed_sale.Value = m_gaming_table_config.ChipsMaxAllowedSales

    _dealer_mode_enabled = m_gaming_table_config.DealercopyMode = CLASS_GAMING_TABLES_CONFIGURATION.CASHIER_MODE_PURCHASE.TICKET

    Me.cmb_cashier_mode.Value = m_gaming_table_config.DealercopyMode
    Me.chk_dealercopy_barcode.Checked = m_gaming_table_config.DealercopyBarcode
    Me.chk_dealercopy_signature.Checked = m_gaming_table_config.DealercopySignature
    Me.ef_dealercopy_title.Value = m_gaming_table_config.DealercopyTitle
    Me.chk_dealercopy_barcode.Enabled = _dealer_mode_enabled
    Me.chk_dealercopy_signature.Enabled = _dealer_mode_enabled
    Me.ef_dealercopy_title.Enabled = _dealer_mode_enabled
    Me.lbl_dealercopy_title.Enabled = _dealer_mode_enabled
    Me.m_dt_allowed_currencies = m_gaming_table_config.CurrenciesAllowed

    Me.chk_cashier_concilition_enable.Enabled = _dealer_mode_enabled And Me.chk_player_tracking.Checked
    Me.chk_cashier_concilition_enable.Checked = m_gaming_table_config.CashierConcilitionEnable And Me.chk_cashier_concilition_enable.Enabled

    Me.chk_cashier_concilition_check_account.Enabled = Me.chk_cashier_concilition_enable.Checked
    Me.chk_cashier_concilition_check_account.Checked = m_gaming_table_config.CashierConciliationCheckAccount

    If m_gaming_table_config.AssignmentPointsMode > 0 Then
      cmb_gaming_tables_points_assignment_mode.SelectedIndex = CLASS_GAMING_TABLES_CONFIGURATION.ASSIGNMENT_POINTS_MODE.MANUAL
      Me.txt_gaming_tables_points_assignment_days.Text = m_gaming_table_config.AssignmentPointsMode
    Else
      cmb_gaming_tables_points_assignment_mode.SelectedIndex = CLASS_GAMING_TABLES_CONFIGURATION.ASSIGNMENT_POINTS_MODE.AUTOMATIC
      Me.txt_gaming_tables_points_assignment_days.Text = 0
    End If

    AddHandler cmb_gaming_tables_mode.ValueChangedEvent, AddressOf uc_gaming_tables_mode_ValueChangedEvent
    AddHandler cmb_cashier_mode.ValueChangedEvent, AddressOf cmb_cashier_mode_ValueChangedEvent

    Call uc_gaming_tables_mode_ValueChangedEvent()
    Call cmb_cashier_mode_ValueChangedEvent()

  End Sub 'GUI_SetScreenData

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    If Me.DbStatus = ENUM_STATUS.STATUS_NOT_SUPPORTED Then
      '''Wrong DbVersion message already shown, do not show again.
      Exit Sub
    End If

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        Me.DbEditedObject = New CLASS_GAMING_TABLES_CONFIGURATION
        m_gaming_table_config = DbEditedObject
        Me.DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), ENUM_MB_TYPE.MB_TYPE_ERROR)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), ENUM_MB_TYPE.MB_TYPE_ERROR)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), ENUM_MB_TYPE.MB_TYPE_ERROR)
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE: Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.cmb_gaming_tables_mode

  End Sub 'GUI_SetInitialFocus

  ' PURPOSE: Keypress 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    Select Case e.KeyChar
      Case Chr(Keys.Enter)

        Return False

      Case Chr(Keys.Escape)

      Case Else
        Return MyBase.GUI_KeyPress(sender, e)

    End Select
  End Function ' GUI_KeyPress

#End Region               'Overrides

#Region "Events"

  Private Sub uc_gaming_tables_mode_ValueChangedEvent()
    Me.gb_params.Enabled = (Me.cmb_gaming_tables_mode.Value <> GamingTableBusinessLogic.GT_MODE.DISABLED)
    Me.gb_cashiermode.Enabled = (Me.cmb_gaming_tables_mode.Value <> GamingTableBusinessLogic.GT_MODE.DISABLED)
    Me.chk_player_tracking.Enabled = (Me.cmb_gaming_tables_mode.Value = GamingTableBusinessLogic.GT_MODE.GUI_AND_CASHIER)
    If Not Me.chk_player_tracking.Enabled Then
      Me.chk_player_tracking.Checked = False
    End If
  End Sub

  Private Sub cmb_cashier_mode_ValueChangedEvent()
    Dim _cashier_mode_ticket As Boolean

    _cashier_mode_ticket = (cmb_cashier_mode.Value = CLASS_GAMING_TABLES_CONFIGURATION.CASHIER_MODE_PURCHASE.TICKET)
    chk_dealercopy_barcode.Enabled = _cashier_mode_ticket
    chk_dealercopy_signature.Enabled = _cashier_mode_ticket
    ef_dealercopy_title.Enabled = _cashier_mode_ticket
    lbl_dealercopy_title.Enabled = _cashier_mode_ticket
    chk_cashier_concilition_enable.Enabled = _cashier_mode_ticket And Me.chk_player_tracking.Checked
    btn_ticket_configuration.Enabled = _cashier_mode_ticket

    If Me.chk_player_tracking.Checked Then
      Me.chk_cashier_concilition_enable.Enabled = _cashier_mode_ticket
    Else
      Me.chk_cashier_concilition_enable.Enabled = False
    End If

    Me.chk_cashier_concilition_enable.Checked = Me.chk_cashier_concilition_enable.Checked And Me.chk_cashier_concilition_enable.Enabled

  End Sub
#End Region 'Events 

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.

  End Sub

  ''' <summary>
  ''' Function to add one tab for each allowed currency  in  the system
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub AddCurrenciesTab()
    Dim _currency_accepted As String
    Dim _currency_array As String()
    Dim _str_one_word As String = String.Empty

    _currency_accepted = GeneralParam.GetString("RegionalOptions", "CurrenciesAccepted")
    _currency_array = _currency_accepted.Split(";")

    For Each _str_one_word In _currency_array
      If (Not _str_one_word.Equals(OLD_CHIPS_DENOMINATION)) Then
        Call AddTab(_str_one_word)
      End If
    Next

    Call FillTabValues()

  End Sub

  ''' <summary>
  ''' Function that add the structure for each currency tab.
  ''' </summary>
  ''' <param name="name"></param>
  ''' <remarks></remarks>
  Private Sub AddTab(ByRef name As String)
    Dim _tab As Windows.Forms.TabPage
    Dim _ef_max_allowed_purchase As uc_entry_field
    Dim _ef_max_allowed_sale As uc_entry_field

    _tab = New Windows.Forms.TabPage

    _ef_max_allowed_purchase = New uc_entry_field()
    _ef_max_allowed_sale = New uc_entry_field()

    _ef_max_allowed_purchase.Size = New Size(240, 25)
    _ef_max_allowed_purchase.TextWidth = 100
    _ef_max_allowed_purchase.Location = New System.Drawing.Point(_ef_max_allowed_purchase.Location.X + 20, _ef_max_allowed_purchase.Location.Y + 10)
    _ef_max_allowed_purchase.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4546)
    _ef_max_allowed_purchase.SufixText = ""
    _ef_max_allowed_purchase.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 2)

    _ef_max_allowed_sale.Size = New Size(240, 25)
    _ef_max_allowed_sale.TextWidth = 100
    _ef_max_allowed_sale.Location = New System.Drawing.Point(_ef_max_allowed_purchase.Location.X, _ef_max_allowed_purchase.Location.Y + 35)
    _ef_max_allowed_sale.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4545)
    _ef_max_allowed_sale.SufixText = ""
    _ef_max_allowed_sale.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 2)

    _tab.Text = name
    _tab.Controls.Add(_ef_max_allowed_purchase)
    _tab.Controls.Add(_ef_max_allowed_sale)
    Me.tb_currencies.TabPages.Add(_tab)

  End Sub

  ''' <summary>
  ''' Function to fill database values in the form
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub FillTabValues()
    Dim _default As uc_entry_field
    Dim _allowed_currency As DataTable

    _allowed_currency = New DataTable()
    _allowed_currency = m_gaming_table_config.CurrenciesAllowed

    For Each _tab As TabPage In tb_currencies.TabPages
      _default = New uc_entry_field()
      For Each item As DataRow In _allowed_currency.Rows
        If (item(0).ToString() = _tab.Text) Then
          _default = _tab.Controls.Item(0)
          _default.TextValue = GUI_FormatNumber(item(1).ToString(), 2)
          _default = _tab.Controls.Item(1)
          _default.TextValue = GUI_FormatNumber(item(2).ToString(), 2)
        End If
      Next
    Next
  End Sub

  ''' <summary>
  ''' Function to get all allowed values from database
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GetAllowedCurrenciesValues()
    Try
      Dim _default As uc_entry_field
      Dim _new_row As DataRow

      If m_dt_allowed_currencies Is Nothing Then
        m_dt_allowed_currencies = CreateDatatable()
      Else
        m_dt_allowed_currencies.Clear()
      End If

      For Each _tab As TabPage In tb_currencies.TabPages
        _default = New uc_entry_field()
        _new_row = m_dt_allowed_currencies.NewRow()
        _new_row(0) = _tab.Text
        _default = _tab.Controls.Item(0)
        _new_row(1) = IIf(String.IsNullOrEmpty(_default.TextValue), 0, _default.TextValue)
        _default = _tab.Controls.Item(1)
        _new_row(2) = IIf(String.IsNullOrEmpty(_default.TextValue), 0, _default.TextValue)
        m_dt_allowed_currencies.Rows.Add(_new_row)
      Next
    Catch ex As Exception
      Log.Error(ex.Message)
    End Try
  End Sub

  ''' <summary>
  ''' Function to create a datatable with the structure of allowed Currencies
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function CreateDatatable() As DataTable
    Dim _currencies_allowed As DataTable

    _currencies_allowed = New DataTable("ALLOWEDCURRENCIES")
    _currencies_allowed.Columns.Add("GTC_ISO_CODE", GetType(System.String))
    _currencies_allowed.Columns.Add("GTC_MAX_ALLOWED_PURCHASE", GetType(System.Decimal))
    _currencies_allowed.Columns.Add("GTC_MAX_ALLOWED_SALES", GetType(System.Decimal))
    Return _currencies_allowed
  End Function

  ''' <summary>
  ''' Function to validate the AllowedCurrencies tables for audit
  ''' </summary>
  ''' <param name="TablesRead"></param>
  ''' <param name="TablesEdit"></param>
  ''' <remarks></remarks>
  Private Sub ValidateTables(TablesRead As CLASS_GAMING_TABLES_CONFIGURATION, TablesEdit As CLASS_GAMING_TABLES_CONFIGURATION)
    Dim _rows() As DataRow
    Dim _dbReadObject_count As Integer
    Dim _dbEditObject_count As Integer

    _dbReadObject_count = TablesRead.CurrenciesAllowed.Rows.Count
    _dbEditObject_count = TablesEdit.CurrenciesAllowed.Rows.Count

    ' If both tables are null, exit from function
    If (_dbReadObject_count = 0) AndAlso (_dbEditObject_count = 0) Then
      Exit Sub
    End If

    If (_dbReadObject_count <> _dbEditObject_count) Then

      For Each _row As DataRow In TablesRead.CurrenciesAllowed.Rows
        _rows = TablesEdit.CurrenciesAllowed.Select(String.Format("GTC_ISO_CODE = '{0}'", _row("GTC_ISO_CODE")))

        If _rows.Length = 0 Then
          TablesEdit.CurrenciesAllowed.Rows.Add(_row("GTC_ISO_CODE"), 0, 0)
        End If
      Next

      For Each _row As DataRow In TablesEdit.CurrenciesAllowed.Rows
        _rows = TablesRead.CurrenciesAllowed.Select(String.Format("GTC_ISO_CODE = '{0}'", _row("GTC_ISO_CODE")))

        If _rows.Length = 0 Then
          TablesRead.CurrenciesAllowed.Rows.Add(_row("GTC_ISO_CODE"), 0, 0)
        End If
      Next

    End If
  End Sub

  ''' <summary>
  ''' Function to compare the edit and read allowedCurrencies datatables.
  ''' </summary>
  ''' <param name="dtRead"></param>
  ''' <param name="dtEdit"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function CompareDataTables(dtRead As DataTable, dtEdit As DataTable) As DataTable
    Dim _dt_merge As DataTable
    Dim _dt_small As DataTable
    Dim _dt_big As DataTable
    Dim _dv_small As DataView
    Dim _dv_big As DataView
    Dim _is_insert_row As Boolean
    Dim _dr_new_datatable As DataRow
    Dim _row_table_1 As Object
    Dim _row_table_2 As Object
    Try

      _dt_merge = CreateDatatable()
      _dt_small = New DataTable()
      _dt_big = New DataTable()
      If dtRead.Rows.Count > dtEdit.Rows.Count Then
        _dt_big = dtRead.Copy()
        _dt_small = dtEdit.Copy()
      Else
        _dt_big = dtEdit.Copy()
        _dt_small = dtRead.Copy()
      End If

      _dv_small = _dt_small.DefaultView
      _dv_small.Sort = "GTC_ISO_CODE" & " DESC"
      _dt_small = _dv_small.ToTable()

      _dv_big = _dt_big.DefaultView
      _dv_big.Sort = "GTC_ISO_CODE" & " DESC"
      _dt_big = _dv_big.ToTable()

      For Each _row_big As DataRow In _dt_big.Rows
        _is_insert_row = False
        _dr_new_datatable = _dt_merge.NewRow()

        For Each _row_small As DataRow In _dt_small.Rows
          _row_table_1 = _row_big.ItemArray
          _row_table_2 = _row_small.ItemArray

          If CompareItems(_row_table_1, _row_table_2) Then
            _dr_new_datatable = _row_small
            _dt_merge.ImportRow(_dr_new_datatable)
            _is_insert_row = True
            Exit For
          End If
        Next
        If Not _is_insert_row Then
          _dr_new_datatable.Item(0) = _row_big.Item(0)
          _dr_new_datatable.Item(1) = _row_big.Item(1)
          _dr_new_datatable.Item(2) = _row_big.Item(2)
          _dt_merge.Rows.Add(_dr_new_datatable)
        End If

      Next

      Return _dt_merge

    Catch exception As Exception
      _dt_merge = New DataTable()

      Return _dt_merge
    End Try
  End Function

  ''' <summary>
  ''' Function to compare each item of allowed currencies datatables.
  ''' </summary>
  ''' <param name="array1"></param>
  ''' <param name="array2"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function CompareItems(ByVal ArrayBigTable As Object, ByVal ArraySmallTable As Object) As Boolean
    Dim _counter As Integer

    If ArrayBigTable(0).Equals(ArraySmallTable(0)) Then 'Compare if iso_code are equals.

      For _counter = 0 To 2

        If Not (ArrayBigTable(_counter).Equals(ArraySmallTable(_counter))) Then

          Return False
        End If

      Next

      Return True
    Else

      Return False
    End If
  End Function

  Private Sub chk_player_tracking_CheckedChanged(sender As Object, e As EventArgs) Handles chk_player_tracking.CheckedChanged
    If Me.chk_player_tracking.Checked Then
      Me.chk_cashier_concilition_enable.Enabled = (cmb_cashier_mode.Value = CLASS_GAMING_TABLES_CONFIGURATION.CASHIER_MODE_PURCHASE.TICKET)
    Else
      Me.chk_cashier_concilition_enable.Enabled = False
    End If

    Me.chk_cashier_concilition_enable.Checked = Me.chk_cashier_concilition_enable.Checked And Me.chk_cashier_concilition_enable.Enabled

  End Sub

  Private Sub chk_cashier_concilition_enable_CheckedChanged(sender As Object, e As EventArgs) Handles chk_cashier_concilition_enable.CheckedChanged
    Me.chk_cashier_concilition_check_account.Enabled = Me.chk_cashier_concilition_enable.Checked
  End Sub

  Private Sub btn_ticket_configuration_ClickEvent() Handles btn_ticket_configuration.ClickEvent

    Dim _frm_dealer_copy_configuration_edit As frm_dealer_copy_configuration_edit

    _frm_dealer_copy_configuration_edit = New frm_dealer_copy_configuration_edit

    _frm_dealer_copy_configuration_edit.ShowEditItem(Me)

  End Sub

  Private Sub cmb_gaming_tables_points_assignment_mode_ValueChangedEvent() Handles cmb_gaming_tables_points_assignment_mode.ValueChangedEvent

    If (cmb_gaming_tables_points_assignment_mode.SelectedIndex = CLASS_GAMING_TABLES_CONFIGURATION.ASSIGNMENT_POINTS_MODE.AUTOMATIC) Then
      pnl_gaming_tables_points_assignment_mode.Visible = False
      txt_gaming_tables_points_assignment_days.Text = 0
    ElseIf (cmb_gaming_tables_points_assignment_mode.SelectedIndex = CLASS_GAMING_TABLES_CONFIGURATION.ASSIGNMENT_POINTS_MODE.MANUAL) Then
      pnl_gaming_tables_points_assignment_mode.Visible = True
    End If

  End Sub

  Private Sub txt_gaming_tables_points_assignment_days_TextChanged(sender As Object, e As EventArgs) Handles txt_gaming_tables_points_assignment_days.TextChanged
    Dim _point_assignment_days As Integer

    If Int32.TryParse(txt_gaming_tables_points_assignment_days.Text, _point_assignment_days) = False And txt_gaming_tables_points_assignment_days.Text <> String.Empty Then
      txt_gaming_tables_points_assignment_days.Text = txt_gaming_tables_points_assignment_days.Text.Substring(0, txt_gaming_tables_points_assignment_days.TextLength - 1)
      txt_gaming_tables_points_assignment_days.Select(txt_gaming_tables_points_assignment_days.Text.Length, 0)
    End If
  End Sub
End Class