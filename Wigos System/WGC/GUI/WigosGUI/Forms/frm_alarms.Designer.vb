<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_alarms
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_alarms))
    Me.gb_severity = New System.Windows.Forms.GroupBox()
    Me.lbl_color_error = New System.Windows.Forms.Label()
    Me.lbl_color_warning = New System.Windows.Forms.Label()
    Me.chk_error = New System.Windows.Forms.CheckBox()
    Me.lbl_color_info = New System.Windows.Forms.Label()
    Me.chk_warning = New System.Windows.Forms.CheckBox()
    Me.chk_info = New System.Windows.Forms.CheckBox()
    Me.gb_sources = New System.Windows.Forms.GroupBox()
    Me.btn_uncheck_all_sources = New GUI_Controls.uc_button()
    Me.btn_check_all_sources = New GUI_Controls.uc_button()
    Me.dg_sources = New GUI_Controls.uc_grid()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.gb_mode = New System.Windows.Forms.GroupBox()
    Me.opt_history_mode = New System.Windows.Forms.RadioButton()
    Me.opt_monitor_mode = New System.Windows.Forms.RadioButton()
    Me.lbl_latest_alarms = New GUI_Controls.uc_text_field()
    Me.tmr_monitor = New System.Windows.Forms.Timer(Me.components)
    Me.lbl_no_data = New GUI_Controls.uc_text_field()
    Me.uc_site_select = New GUI_Controls.uc_sites_sel()
    Me.ef_alarm_desc = New GUI_Controls.uc_entry_field()
    Me.uc_checked_list_alarms_catalog = New GUI_Controls.uc_checked_list()
    Me.uc_pr_list = New GUI_Controls.uc_provider()
    Me.ef_alarm_id = New GUI_Controls.uc_entry_field()
    Me.gb_user = New System.Windows.Forms.GroupBox()
    Me.chk_show_all = New System.Windows.Forms.CheckBox()
    Me.chk_user_filter = New System.Windows.Forms.CheckBox()
    Me.opt_none = New System.Windows.Forms.RadioButton()
    Me.opt_one_user = New System.Windows.Forms.RadioButton()
    Me.opt_all_users = New System.Windows.Forms.RadioButton()
    Me.cmb_user = New GUI_Controls.uc_combo()
    Me.tf_last_update = New GUI_Controls.uc_text_field()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_severity.SuspendLayout()
    Me.gb_sources.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_mode.SuspendLayout()
    Me.gb_user.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.tf_last_update)
    Me.panel_filter.Controls.Add(Me.uc_checked_list_alarms_catalog)
    Me.panel_filter.Controls.Add(Me.ef_alarm_desc)
    Me.panel_filter.Controls.Add(Me.ef_alarm_id)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Controls.Add(Me.uc_site_select)
    Me.panel_filter.Controls.Add(Me.lbl_no_data)
    Me.panel_filter.Controls.Add(Me.lbl_latest_alarms)
    Me.panel_filter.Controls.Add(Me.gb_mode)
    Me.panel_filter.Controls.Add(Me.gb_severity)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Controls.Add(Me.gb_sources)
    Me.panel_filter.Controls.Add(Me.gb_user)
    Me.panel_filter.Size = New System.Drawing.Size(1268, 232)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_user, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_sources, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_severity, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_mode, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_latest_alarms, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_no_data, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_site_select, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_alarm_id, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_alarm_desc, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_checked_list_alarms_catalog, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.tf_last_update, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 236)
    Me.panel_data.Size = New System.Drawing.Size(1268, 522)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1262, 22)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1262, 4)
    '
    'gb_severity
    '
    Me.gb_severity.Controls.Add(Me.lbl_color_error)
    Me.gb_severity.Controls.Add(Me.lbl_color_warning)
    Me.gb_severity.Controls.Add(Me.chk_error)
    Me.gb_severity.Controls.Add(Me.lbl_color_info)
    Me.gb_severity.Controls.Add(Me.chk_warning)
    Me.gb_severity.Controls.Add(Me.chk_info)
    Me.gb_severity.Location = New System.Drawing.Point(294, 101)
    Me.gb_severity.Name = "gb_severity"
    Me.gb_severity.Size = New System.Drawing.Size(125, 88)
    Me.gb_severity.TabIndex = 3
    Me.gb_severity.TabStop = False
    Me.gb_severity.Text = "xSeverity"
    '
    'lbl_color_error
    '
    Me.lbl_color_error.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_color_error.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_color_error.Location = New System.Drawing.Point(6, 18)
    Me.lbl_color_error.Name = "lbl_color_error"
    Me.lbl_color_error.Size = New System.Drawing.Size(16, 16)
    Me.lbl_color_error.TabIndex = 0
    '
    'lbl_color_warning
    '
    Me.lbl_color_warning.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_color_warning.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_color_warning.Location = New System.Drawing.Point(6, 40)
    Me.lbl_color_warning.Name = "lbl_color_warning"
    Me.lbl_color_warning.Size = New System.Drawing.Size(16, 16)
    Me.lbl_color_warning.TabIndex = 2
    '
    'chk_error
    '
    Me.chk_error.AutoSize = True
    Me.chk_error.Location = New System.Drawing.Point(27, 18)
    Me.chk_error.Name = "chk_error"
    Me.chk_error.Size = New System.Drawing.Size(62, 17)
    Me.chk_error.TabIndex = 1
    Me.chk_error.Text = "xError"
    Me.chk_error.UseVisualStyleBackColor = True
    '
    'lbl_color_info
    '
    Me.lbl_color_info.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_color_info.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_color_info.Location = New System.Drawing.Point(6, 63)
    Me.lbl_color_info.Name = "lbl_color_info"
    Me.lbl_color_info.Size = New System.Drawing.Size(16, 16)
    Me.lbl_color_info.TabIndex = 4
    '
    'chk_warning
    '
    Me.chk_warning.AutoSize = True
    Me.chk_warning.Location = New System.Drawing.Point(27, 40)
    Me.chk_warning.Name = "chk_warning"
    Me.chk_warning.Size = New System.Drawing.Size(79, 17)
    Me.chk_warning.TabIndex = 3
    Me.chk_warning.Text = "xWarning"
    Me.chk_warning.UseVisualStyleBackColor = True
    '
    'chk_info
    '
    Me.chk_info.AutoSize = True
    Me.chk_info.Location = New System.Drawing.Point(27, 63)
    Me.chk_info.Name = "chk_info"
    Me.chk_info.Size = New System.Drawing.Size(100, 17)
    Me.chk_info.TabIndex = 5
    Me.chk_info.Text = "xInformative"
    Me.chk_info.UseVisualStyleBackColor = True
    '
    'gb_sources
    '
    Me.gb_sources.Controls.Add(Me.btn_uncheck_all_sources)
    Me.gb_sources.Controls.Add(Me.btn_check_all_sources)
    Me.gb_sources.Controls.Add(Me.dg_sources)
    Me.gb_sources.Location = New System.Drawing.Point(805, 6)
    Me.gb_sources.Name = "gb_sources"
    Me.gb_sources.Size = New System.Drawing.Size(307, 150)
    Me.gb_sources.TabIndex = 7
    Me.gb_sources.TabStop = False
    Me.gb_sources.Text = "xSources"
    '
    'btn_uncheck_all_sources
    '
    Me.btn_uncheck_all_sources.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_uncheck_all_sources.Location = New System.Drawing.Point(3, 55)
    Me.btn_uncheck_all_sources.Name = "btn_uncheck_all_sources"
    Me.btn_uncheck_all_sources.Size = New System.Drawing.Size(90, 30)
    Me.btn_uncheck_all_sources.TabIndex = 1
    Me.btn_uncheck_all_sources.ToolTipped = False
    Me.btn_uncheck_all_sources.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.USER
    '
    'btn_check_all_sources
    '
    Me.btn_check_all_sources.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_check_all_sources.Location = New System.Drawing.Point(3, 20)
    Me.btn_check_all_sources.Name = "btn_check_all_sources"
    Me.btn_check_all_sources.Size = New System.Drawing.Size(90, 30)
    Me.btn_check_all_sources.TabIndex = 0
    Me.btn_check_all_sources.ToolTipped = False
    Me.btn_check_all_sources.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.USER
    '
    'dg_sources
    '
    Me.dg_sources.CurrentCol = -1
    Me.dg_sources.CurrentRow = -1
    Me.dg_sources.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_sources.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_sources.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_sources.Location = New System.Drawing.Point(95, 13)
    Me.dg_sources.Name = "dg_sources"
    Me.dg_sources.PanelRightVisible = False
    Me.dg_sources.Redraw = True
    Me.dg_sources.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_sources.Size = New System.Drawing.Size(208, 130)
    Me.dg_sources.Sortable = False
    Me.dg_sources.SortAscending = True
    Me.dg_sources.SortByCol = 0
    Me.dg_sources.TabIndex = 2
    Me.dg_sources.ToolTipped = True
    Me.dg_sources.TopRow = -2
    Me.dg_sources.WordWrap = False
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(294, 6)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(252, 66)
    Me.gb_date.TabIndex = 1
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 38)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(240, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 40
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 14)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(240, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 40
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'gb_mode
    '
    Me.gb_mode.Controls.Add(Me.opt_history_mode)
    Me.gb_mode.Controls.Add(Me.opt_monitor_mode)
    Me.gb_mode.Location = New System.Drawing.Point(424, 120)
    Me.gb_mode.Name = "gb_mode"
    Me.gb_mode.Size = New System.Drawing.Size(120, 69)
    Me.gb_mode.TabIndex = 5
    Me.gb_mode.TabStop = False
    Me.gb_mode.Text = "xMode"
    '
    'opt_history_mode
    '
    Me.opt_history_mode.AutoSize = True
    Me.opt_history_mode.Location = New System.Drawing.Point(6, 42)
    Me.opt_history_mode.Name = "opt_history_mode"
    Me.opt_history_mode.Size = New System.Drawing.Size(72, 17)
    Me.opt_history_mode.TabIndex = 1
    Me.opt_history_mode.Text = "xHistory"
    Me.opt_history_mode.UseVisualStyleBackColor = True
    '
    'opt_monitor_mode
    '
    Me.opt_monitor_mode.AutoSize = True
    Me.opt_monitor_mode.Location = New System.Drawing.Point(6, 19)
    Me.opt_monitor_mode.Name = "opt_monitor_mode"
    Me.opt_monitor_mode.Size = New System.Drawing.Size(74, 17)
    Me.opt_monitor_mode.TabIndex = 0
    Me.opt_monitor_mode.Text = "xMonitor"
    Me.opt_monitor_mode.UseVisualStyleBackColor = True
    '
    'lbl_latest_alarms
    '
    Me.lbl_latest_alarms.AutoSize = True
    Me.lbl_latest_alarms.IsReadOnly = True
    Me.lbl_latest_alarms.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_latest_alarms.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_latest_alarms.Location = New System.Drawing.Point(299, 73)
    Me.lbl_latest_alarms.Name = "lbl_latest_alarms"
    Me.lbl_latest_alarms.Size = New System.Drawing.Size(210, 24)
    Me.lbl_latest_alarms.SufixText = "Sufix Text"
    Me.lbl_latest_alarms.SufixTextVisible = True
    Me.lbl_latest_alarms.TabIndex = 2
    Me.lbl_latest_alarms.TabStop = False
    Me.lbl_latest_alarms.TextWidth = 0
    Me.lbl_latest_alarms.Value = "xLatest alarms"
    '
    'tmr_monitor
    '
    '
    'lbl_no_data
    '
    Me.lbl_no_data.AutoSize = True
    Me.lbl_no_data.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_no_data.IsReadOnly = True
    Me.lbl_no_data.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_no_data.LabelForeColor = System.Drawing.Color.Red
    Me.lbl_no_data.Location = New System.Drawing.Point(421, 95)
    Me.lbl_no_data.Name = "lbl_no_data"
    Me.lbl_no_data.Size = New System.Drawing.Size(100, 25)
    Me.lbl_no_data.SufixText = "Sufix Text"
    Me.lbl_no_data.SufixTextVisible = True
    Me.lbl_no_data.TabIndex = 4
    Me.lbl_no_data.TabStop = False
    Me.lbl_no_data.TextWidth = 0
    Me.lbl_no_data.Value = "xNo data"
    '
    'uc_site_select
    '
    Me.uc_site_select.Location = New System.Drawing.Point(6, 3)
    Me.uc_site_select.MultiSelect = True
    Me.uc_site_select.Name = "uc_site_select"
    Me.uc_site_select.ShowIsoCode = False
    Me.uc_site_select.ShowMultisiteRow = True
    Me.uc_site_select.Size = New System.Drawing.Size(286, 161)
    Me.uc_site_select.TabIndex = 0
    '
    'ef_alarm_desc
    '
    Me.ef_alarm_desc.DoubleValue = 0.0R
    Me.ef_alarm_desc.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_alarm_desc.IntegerValue = 0
    Me.ef_alarm_desc.IsReadOnly = False
    Me.ef_alarm_desc.Location = New System.Drawing.Point(156, 196)
    Me.ef_alarm_desc.Name = "ef_alarm_desc"
    Me.ef_alarm_desc.PlaceHolder = Nothing
    Me.ef_alarm_desc.Size = New System.Drawing.Size(305, 25)
    Me.ef_alarm_desc.SufixText = "Sufix Text"
    Me.ef_alarm_desc.SufixTextVisible = True
    Me.ef_alarm_desc.TabIndex = 9
    Me.ef_alarm_desc.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_alarm_desc.TextValue = ""
    Me.ef_alarm_desc.TextWidth = 75
    Me.ef_alarm_desc.Value = ""
    Me.ef_alarm_desc.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_checked_list_alarms_catalog
    '
    Me.uc_checked_list_alarms_catalog.GroupBoxText = "xCheckedList"
    Me.uc_checked_list_alarms_catalog.Location = New System.Drawing.Point(550, 6)
    Me.uc_checked_list_alarms_catalog.m_resize_width = 249
    Me.uc_checked_list_alarms_catalog.multiChoice = True
    Me.uc_checked_list_alarms_catalog.Name = "uc_checked_list_alarms_catalog"
    Me.uc_checked_list_alarms_catalog.SelectedIndexes = New Integer(-1) {}
    Me.uc_checked_list_alarms_catalog.SelectedIndexesList = ""
    Me.uc_checked_list_alarms_catalog.SelectedIndexesListLevel2 = ""
    Me.uc_checked_list_alarms_catalog.SelectedValuesArray = New String(-1) {}
    Me.uc_checked_list_alarms_catalog.SelectedValuesList = ""
    Me.uc_checked_list_alarms_catalog.SelectedValuesListLevel2 = ""
    Me.uc_checked_list_alarms_catalog.SetLevels = 2
    Me.uc_checked_list_alarms_catalog.Size = New System.Drawing.Size(249, 183)
    Me.uc_checked_list_alarms_catalog.TabIndex = 6
    Me.uc_checked_list_alarms_catalog.ValuesArray = New String(-1) {}
    '
    'uc_pr_list
    '
    Me.uc_pr_list.FilterByCurrency = False
    Me.uc_pr_list.Location = New System.Drawing.Point(1114, 2)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(307, 174)
    Me.uc_pr_list.TabIndex = 9
    Me.uc_pr_list.TerminalListHasValues = False
    '
    'ef_alarm_id
    '
    Me.ef_alarm_id.DoubleValue = 0.0R
    Me.ef_alarm_id.IntegerValue = 0
    Me.ef_alarm_id.IsReadOnly = False
    Me.ef_alarm_id.Location = New System.Drawing.Point(-53, 197)
    Me.ef_alarm_id.Name = "ef_alarm_id"
    Me.ef_alarm_id.PlaceHolder = Nothing
    Me.ef_alarm_id.Size = New System.Drawing.Size(200, 24)
    Me.ef_alarm_id.SufixText = "Sufix Text"
    Me.ef_alarm_id.SufixTextVisible = True
    Me.ef_alarm_id.TabIndex = 9
    Me.ef_alarm_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_alarm_id.TextValue = ""
    Me.ef_alarm_id.TextWidth = 75
    Me.ef_alarm_id.Value = ""
    Me.ef_alarm_id.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_user
    '
    Me.gb_user.Controls.Add(Me.chk_show_all)
    Me.gb_user.Controls.Add(Me.chk_user_filter)
    Me.gb_user.Controls.Add(Me.opt_none)
    Me.gb_user.Controls.Add(Me.opt_one_user)
    Me.gb_user.Controls.Add(Me.opt_all_users)
    Me.gb_user.Controls.Add(Me.cmb_user)
    Me.gb_user.Location = New System.Drawing.Point(805, 160)
    Me.gb_user.Name = "gb_user"
    Me.gb_user.Size = New System.Drawing.Size(307, 72)
    Me.gb_user.TabIndex = 8
    Me.gb_user.TabStop = False
    Me.gb_user.Text = "xUser"
    '
    'chk_show_all
    '
    Me.chk_show_all.AutoSize = True
    Me.chk_show_all.Location = New System.Drawing.Point(204, 46)
    Me.chk_show_all.Name = "chk_show_all"
    Me.chk_show_all.Size = New System.Drawing.Size(78, 17)
    Me.chk_show_all.TabIndex = 9
    Me.chk_show_all.Text = "xShowAll"
    Me.chk_show_all.UseVisualStyleBackColor = True
    '
    'chk_user_filter
    '
    Me.chk_user_filter.AutoSize = True
    Me.chk_user_filter.Location = New System.Drawing.Point(6, -1)
    Me.chk_user_filter.Name = "chk_user_filter"
    Me.chk_user_filter.Size = New System.Drawing.Size(85, 17)
    Me.chk_user_filter.TabIndex = 8
    Me.chk_user_filter.Text = "xchk_user"
    Me.chk_user_filter.UseVisualStyleBackColor = True
    '
    'opt_none
    '
    Me.opt_none.Location = New System.Drawing.Point(82, 42)
    Me.opt_none.Name = "opt_none"
    Me.opt_none.Size = New System.Drawing.Size(112, 24)
    Me.opt_none.TabIndex = 3
    Me.opt_none.Text = "xNone"
    '
    'opt_one_user
    '
    Me.opt_one_user.Location = New System.Drawing.Point(8, 16)
    Me.opt_one_user.Name = "opt_one_user"
    Me.opt_one_user.Size = New System.Drawing.Size(72, 24)
    Me.opt_one_user.TabIndex = 0
    Me.opt_one_user.Text = "xOne"
    '
    'opt_all_users
    '
    Me.opt_all_users.Location = New System.Drawing.Point(8, 42)
    Me.opt_all_users.Name = "opt_all_users"
    Me.opt_all_users.Size = New System.Drawing.Size(64, 24)
    Me.opt_all_users.TabIndex = 2
    Me.opt_all_users.Text = "xAll"
    '
    'cmb_user
    '
    Me.cmb_user.AllowUnlistedValues = False
    Me.cmb_user.AutoCompleteMode = False
    Me.cmb_user.IsReadOnly = False
    Me.cmb_user.Location = New System.Drawing.Point(78, 16)
    Me.cmb_user.Name = "cmb_user"
    Me.cmb_user.SelectedIndex = -1
    Me.cmb_user.Size = New System.Drawing.Size(234, 24)
    Me.cmb_user.SufixText = "Sufix Text"
    Me.cmb_user.SufixTextVisible = True
    Me.cmb_user.TabIndex = 1
    Me.cmb_user.TextCombo = Nothing
    Me.cmb_user.TextVisible = False
    Me.cmb_user.TextWidth = 0
    '
    'tf_last_update
    '
    Me.tf_last_update.IsReadOnly = True
    Me.tf_last_update.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_last_update.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_last_update.Location = New System.Drawing.Point(500, 195)
    Me.tf_last_update.Name = "tf_last_update"
    Me.tf_last_update.Size = New System.Drawing.Size(305, 24)
    Me.tf_last_update.SufixText = "Sufix Text"
    Me.tf_last_update.SufixTextVisible = True
    Me.tf_last_update.TabIndex = 126
    Me.tf_last_update.TabStop = False
    Me.tf_last_update.TextWidth = 140
    Me.tf_last_update.Value = "xLastUpdate"
    '
    'frm_alarms
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1276, 762)
    Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
    Me.Name = "frm_alarms"
    Me.Text = "frm_alarms"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_severity.ResumeLayout(False)
    Me.gb_severity.PerformLayout()
    Me.gb_sources.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gb_mode.ResumeLayout(False)
    Me.gb_mode.PerformLayout()
    Me.gb_user.ResumeLayout(False)
    Me.gb_user.PerformLayout()
    Me.ResumeLayout(False)

End Sub
  Friend WithEvents gb_severity As System.Windows.Forms.GroupBox
  Friend WithEvents chk_error As System.Windows.Forms.CheckBox
  Friend WithEvents chk_warning As System.Windows.Forms.CheckBox
  Friend WithEvents chk_info As System.Windows.Forms.CheckBox
  Friend WithEvents gb_sources As System.Windows.Forms.GroupBox
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents gb_mode As System.Windows.Forms.GroupBox
  Friend WithEvents opt_history_mode As System.Windows.Forms.RadioButton
  Friend WithEvents opt_monitor_mode As System.Windows.Forms.RadioButton
  Friend WithEvents lbl_latest_alarms As GUI_Controls.uc_text_field
  Friend WithEvents tmr_monitor As System.Windows.Forms.Timer
  Friend WithEvents lbl_no_data As GUI_Controls.uc_text_field
  Friend WithEvents lbl_color_info As System.Windows.Forms.Label
  Friend WithEvents lbl_color_warning As System.Windows.Forms.Label
  Friend WithEvents lbl_color_error As System.Windows.Forms.Label
  Friend WithEvents btn_uncheck_all_sources As GUI_Controls.uc_button
  Friend WithEvents btn_check_all_sources As GUI_Controls.uc_button
  Friend WithEvents dg_sources As GUI_Controls.uc_grid
  Friend WithEvents uc_site_select As GUI_Controls.uc_sites_sel
  Friend WithEvents ef_alarm_desc As GUI_Controls.uc_entry_field
  Friend WithEvents uc_checked_list_alarms_catalog As GUI_Controls.uc_checked_list
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents ef_alarm_id As GUI_Controls.uc_entry_field
  Friend WithEvents gb_user As System.Windows.Forms.GroupBox
  Friend WithEvents opt_one_user As System.Windows.Forms.RadioButton
  Friend WithEvents opt_all_users As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_user As GUI_Controls.uc_combo
  Friend WithEvents opt_none As System.Windows.Forms.RadioButton
  Friend WithEvents chk_user_filter As System.Windows.Forms.CheckBox
  Friend WithEvents chk_show_all As System.Windows.Forms.CheckBox
  Friend WithEvents tf_last_update As GUI_Controls.uc_text_field
End Class
