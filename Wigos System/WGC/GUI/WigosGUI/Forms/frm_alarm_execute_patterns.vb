'-------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_alarm_execute_patterns.vb
' DESCRIPTION:   Execute patterns alarms
' AUTHOR:        Jos� Mart�nez L�pez
' CREATION DATE: 07-MAY-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 07-MAY-2014  JML    Initial version.
' 04-JUN-2014  JBC    Fixed Bug WIG-994: Now form reset doesn't search
' 04-JUN-2014  JPJ    Fixed Bug WIG-1001: Pattern execute can't find some patterns
' 05-JUN-2014  JPJ    Fixed Bug WIG-1002: Some recursive patterns don't show up
' 06-JUN-2014  JPJ    Fixed Bug WIG-1004: Terminal restriction was not included (It was reopened due to assigning groups without terminals)
' 02-OCT-2014  JPJ    Fixed Bug WIG-1388: Open patterns don't show up
' 04-DEC-2014  DRV    Fixed Bug WIG-1806: Pattern not insereted on alarms table
'-------------------------------------------------------------------
Option Explicit On

#Region " Imports "

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports System.Text.RegularExpressions
Imports WSI.Common
Imports WSI.Common.Alarm
Imports WSI.Common.Patterns

#End Region

Public Class frm_alarm_execute_patterns
  Inherits frm_base_sel

#Region " Constants "
  '
  Private Const FORM_DB_MIN_VERSION As Short = 209

  ' Table Alarms generated
  Private Const SQL_COLUMN_SITE_ID As Integer = 0
  Private Const SQL_COLUMN_ALARM_ID As Integer = 1
  Private Const SQL_COLUMN_SOURCE_CODE As Integer = 2
  Private Const SQL_COLUMN_SOURCE_ID As Integer = 3
  Private Const SQL_COLUMN_SOURCE_NAME As Integer = 4
  Private Const SQL_COLUMN_ALARM_CODE As Integer = 5
  Private Const SQL_COLUMN_ALARM_NAME As Integer = 6
  Private Const SQL_COLUMN_ALARM_DESCRIPTION As Integer = 7
  Private Const SQL_COLUMN_SEVERITY As Integer = 8
  Private Const SQL_COLUMN_REPORTED As Integer = 9
  Private Const SQL_COLUMN_DATETIME As Integer = 10
  Private Const SQL_COLUMN_ACK_DATETIME As Integer = 11
  Private Const SQL_COLUMN_ACK_USER_ID As Integer = 12
  Private Const SQL_COLUMN_ACK_USER_NAME As Integer = 13
  Private Const SQL_COLUMN_TIMESTAMP As Integer = 14
  Private Const SQL_COLUMN_ELEMENT_VALUES As Integer = 15
  Private Const SQL_COLUMN_PATTERN_ID As Integer = 16
  Private Const SQL_COLUMN_PATTERN_NAME As Integer = 17
  Private Const SQL_COLUMN_STATE As Integer = 18

  'Table alarms
  Private Const SQL_ALARMS_COLUMN_SITE_ID As Integer = 0
  Private Const SQL_ALARMS_COLUMN_ALARM_ID As Integer = 1
  Private Const SQL_ALARMS_COLUMN_SOURCE_CODE As Integer = 2
  Private Const SQL_ALARMS_COLUMN_SOURCE_ID As Integer = 3
  Private Const SQL_ALARMS_COLUMN_SOURCE_NAME As Integer = 4
  Private Const SQL_ALARMS_COLUMN_ALARM_CODE As Integer = 5
  Private Const SQL_ALARMS_COLUMN_DESCRIPTION As Integer = 6
  Private Const SQL_ALARMS_COLUMN_ALARM_NAME As Integer = 7
  Private Const SQL_ALARMS_COLUMN_ALARM_DESCRIPTION As Integer = 8
  Private Const SQL_ALARMS_COLUMN_SEVERITY As Integer = 9
  Private Const SQL_ALARMS_COLUMN_REPORTED As Integer = 10
  Private Const SQL_ALARMS_COLUMN_DATETIME As Integer = 11
  Private Const SQL_ALARMS_COLUMN_ACK_DATETIME As Integer = 12
  Private Const SQL_ALARMS_COLUMN_ACK_USER_ID As Integer = 13
  Private Const SQL_ALARMS_COLUMN_ACK_USER_NAME As Integer = 14
  Private Const SQL_ALARMS_COLUMN_TIMESTAMP As Integer = 15

  'Grid
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_SITE_ID As Integer = 1
  Private Const GRID_COLUMN_ALARM_ID As Integer = 2
  Private Const GRID_COLUMN_TIMESTAMP As Integer = 3
  Private Const GRID_COLUMN_PATTERN_NAME As Integer = 4
  Private Const GRID_COLUMN_ALARM_GROUP As Integer = 5
  Private Const GRID_COLUMN_ALARM_CATEGORY As Integer = 6
  Private Const GRID_COLUMN_DATETIME As Integer = 7
  Private Const GRID_COLUMN_SEVERITY As Integer = 8
  Private Const GRID_COLUMN_SOURCE_NAME As Integer = 9
  Private Const GRID_COLUMN_ALARM_DESCRIPTION As Integer = 10
  Private Const GRID_COLUMN_ACK_DATETIME As Integer = 11
  Private Const GRID_COLUMN_ACK_USER_NAME As Integer = 12
  Private Const GRID_COLUMN_SOURCE_CODE As Integer = 13

  Private Const GRID_COLUMNS As Integer = 14
  Private Const GRID_HEADER_ROWS As Integer = 2

  ' PATTERN COLUMNS
  Private Const PATTERN_COLUMN_ALARM_ID As Integer = 0
  Private Const PATTERN_COLUMN_IDENTIFIER As Integer = 1
  Private Const PATTERN_COLUMN_IDENTIFIER_NAME As Integer = 2
  Private Const PATTERN_COLUMN_ALARM_CODE As Integer = 3
  Private Const PATTERN_COLUMN_ALARM_NAME As Integer = 4
  Private Const PATTERN_COLUMN_ALARM_REPORTED As Integer = 5
  Private Const PATTERN_COLUMN_ALARM_DATETIME As Integer = 6

  'Minimizable
  Private Const TEXT_PLUS As String = "[+]"
  Private Const TEXT_MINOR As String = "[-]"

  'Counters
  Private Const COUNTER_PATTERN_CLOSE As Integer = 1
  Private Const COUNTER_PATTERN_OPEN As Integer = 2
  Private Const COUNTER_ALARM As Integer = 3

  Private Const MAX_COUNTERS As Integer = 3

  Private Const COUNTER_ALL_VISIBLE As Boolean = True

  Private Const COLOR_ALARM_BACK = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_ALARM_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_PATTERN_CLOSE_BACK = ENUM_GUI_COLOR.GUI_COLOR_OCHRE_00
  Private Const COLOR_PATTERN_CLOSE_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_PATTERN_OPEN_BACK = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01
  Private Const COLOR_PATTERN_OPEN_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00

#End Region

#Region " Members "

  Private m_is_center As Boolean = GeneralParam.GetBoolean("MultiSite", "IsCenter", False)
  Private m_is_cashdeskdraw As Boolean = GeneralParam.GetBoolean("CashDesk.Draw", "IsCashDeskDraw", False)

  Private m_is_sites_alarms As Boolean = False
  Private m_site_id As Int64
  Private m_alarms As DataTable

  Dim m_pattern_template_dic As Dictionary(Of Int64, PatternTemplate)
  Dim m_patern_struct As PatternTemplate

  Private m_counter_list(MAX_COUNTERS) As Integer

  Dim m_alarms_dic As Dictionary(Of Integer, ALARMS_CATALOG)

  Dim m_selected_row As Int32
  Dim m_row_height As Int32

  'Report
  Dim m_selected_patterns As String
  Dim m_show_open_patterns As String

#End Region  ' Members

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_ALARM_EXECUTE_PATTERNS
    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)

  End Sub 'GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()
    Me.Text = GLB_NLS_GUI_ALARMS.GetString(469)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False

    ' Filtro
    Me.uc_checked_list_patterns.GroupBoxText = GLB_NLS_GUI_ALARMS.GetString(478)
    Me.uc_checked_list_patterns.SetLevels = 1
    Me.uc_checked_list_patterns.btn_check_all.Width = 100
    Me.uc_checked_list_patterns.btn_uncheck_all.Width = 115
    Me.uc_checked_list_patterns.ColumnWidth(3, 5900)

    Me.cb_show_open_patterns.Text = GLB_NLS_GUI_ALARMS.GetString(479)

    Me.lbl_closed_pattern.Value = GLB_NLS_GUI_ALARMS.GetString(481)
    Me.lbl_closed_pattern_color.Text = ""
    Me.lbl_closed_pattern_color.BackColor = GetColor(COLOR_PATTERN_CLOSE_BACK)
    Me.lbl_in_process_pattern.Value = GLB_NLS_GUI_ALARMS.GetString(482)
    Me.lbl_in_process_pattern_color.Text = ""
    Me.lbl_in_process_pattern_color.BackColor = GetColor(COLOR_PATTERN_OPEN_BACK)

    Call GUI_StyleSheet()

    Call FillPatternsControl()

    Call SetDefaultValues()

    Call GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)

  End Sub ' GUI_InitControls

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.uc_checked_list_patterns
  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()
    m_counter_list(COUNTER_PATTERN_CLOSE) = 0
    m_counter_list(COUNTER_PATTERN_OPEN) = 0
    m_counter_list(COUNTER_ALARM) = 0
  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()

    If Grid.Counter(COUNTER_PATTERN_CLOSE).Value <> m_counter_list(COUNTER_PATTERN_CLOSE) Then
      Grid.Counter(COUNTER_PATTERN_CLOSE).Value = m_counter_list(COUNTER_PATTERN_CLOSE)
    End If
    If Grid.Counter(COUNTER_PATTERN_OPEN).Value <> m_counter_list(COUNTER_PATTERN_OPEN) Then
      Grid.Counter(COUNTER_PATTERN_OPEN).Value = m_counter_list(COUNTER_PATTERN_OPEN)
    End If
    If Grid.Counter(COUNTER_ALARM).Value <> m_counter_list(COUNTER_ALARM) Then
      Grid.Counter(COUNTER_ALARM).Value = m_counter_list(COUNTER_ALARM)
    End If

  End Sub ' GUI_AfterLastRow

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _terminal As String
    Dim _split() As String
    Dim _split2() As String
    Dim _pattern As String


    'INDEX : show if it is minimizable
    If Not DbRow.IsNull(SQL_COLUMN_PATTERN_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_INDEX).Value = TEXT_MINOR
    End If

    ' Site Id
    If Not DbRow.IsNull(SQL_COLUMN_SITE_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SITE_ID).Value = DbRow.Value(SQL_COLUMN_SITE_ID)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SITE_ID).Value = ""
    End If

    ' Alarm Id
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ALARM_ID).Value = DbRow.Value(SQL_COLUMN_ALARM_ID)

    ' Pattern name
    If Not DbRow.IsNull(SQL_COLUMN_PATTERN_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PATTERN_NAME).Value = DbRow.Value(SQL_COLUMN_PATTERN_NAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PATTERN_NAME).Value = ""
    End If

    If m_alarms_dic.ContainsKey(DbRow.Value(SQL_COLUMN_ALARM_CODE)) Then
      ' Alarm Group
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ALARM_GROUP).Value = m_alarms_dic.Item(DbRow.Value(SQL_COLUMN_ALARM_CODE)).GroupName

      ' Alarm Category
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ALARM_CATEGORY).Value = m_alarms_dic.Item(DbRow.Value(SQL_COLUMN_ALARM_CODE)).CategoryName
    Else
      ' Alarm Group
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ALARM_GROUP).Value = ""

      ' Alarm Category
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ALARM_CATEGORY).Value = ""
    End If

    ' Timestamp
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TIMESTAMP).Value = DbRow.Value(SQL_COLUMN_TIMESTAMP)

    ' Generated datetime
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATETIME), _
                                                                        ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                        ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Severity
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SEVERITY).Value = DecodeSeverity(DbRow.Value(SQL_COLUMN_SEVERITY))

    ' Source name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SOURCE_CODE).Value = DbRow.Value(SQL_COLUMN_SOURCE_CODE)

    ' Source name
    _terminal = DbRow.Value(SQL_COLUMN_SOURCE_NAME)
    _split = _terminal.Split("@")

    ' Check if it's a Cashier terminal (user@terminal)
    If (DbRow.Value(SQL_COLUMN_SOURCE_CODE) = AlarmSourceCode.Cashier _
       Or DbRow.Value(SQL_COLUMN_SOURCE_CODE) = AlarmSourceCode.User) _
       AndAlso _split.Length = 2 Then

      'Check if it's like " - Autorizado por"
      _pattern = " - "
      _split2 = Regex.Split(_split(1), _pattern)

      If _split2.Length > 1 Then
        _terminal = _split(0) & "@" & Computer.Alias(_split2(0)) & _pattern
        For _idx As Integer = 1 To _split2.Length - 1
          _terminal &= _split2(_idx)
        Next
      Else
        _terminal = _split(0) & "@" & Computer.Alias(_split(1))
      End If
    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_SOURCE_NAME).Value = _terminal

    ' Alarm description
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ALARM_DESCRIPTION).Value = DbRow.Value(SQL_COLUMN_ALARM_DESCRIPTION)

    ' Ack datetime
    If Not DbRow.IsNull(SQL_COLUMN_ACK_DATETIME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACK_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_ACK_DATETIME), _
                                                                              ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                              ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACK_DATETIME).Value = ""
    End If

    ' Ack user name
    If Not DbRow.IsNull(SQL_COLUMN_ACK_USER_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACK_USER_NAME).Value = DbRow.Value(SQL_COLUMN_ACK_USER_NAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACK_USER_NAME).Value = ""
    End If

    Select Case DbRow.Value(SQL_COLUMN_STATE)

      Case WSI.Common.Patterns.PATTERN_STATE.CLOSED
        Me.Grid.Row(RowIndex).BackColor = GetColor(COLOR_PATTERN_CLOSE_BACK)
        Me.Grid.Row(RowIndex).ForeColor = GetColor(COLOR_PATTERN_CLOSE_FORE)
        m_counter_list(COUNTER_PATTERN_CLOSE) = m_counter_list(COUNTER_PATTERN_CLOSE) + 1

      Case WSI.Common.Patterns.PATTERN_STATE.CREATED
        Me.Grid.Row(RowIndex).BackColor = GetColor(COLOR_PATTERN_OPEN_BACK)
        Me.Grid.Row(RowIndex).ForeColor = GetColor(COLOR_PATTERN_OPEN_FORE)
        m_counter_list(COUNTER_PATTERN_OPEN) = m_counter_list(COUNTER_PATTERN_OPEN) + 1

      Case Else
        Me.Grid.Row(RowIndex).BackColor = GetColor(COLOR_ALARM_BACK)
        Me.Grid.Row(RowIndex).ForeColor = GetColor(COLOR_ALARM_FORE)
        m_counter_list(COUNTER_ALARM) = m_counter_list(COUNTER_ALARM) + 1

    End Select

    m_row_height = Me.Grid.Row(RowIndex).Height

    Return True
  End Function ' GUI_SetupRow

  ' PURPOSE: Get the defined Query Type
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_QUERY_TYPE
  '
  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM
  End Function ' GUI_GetQueryType

  ' PURPOSE: Define the ExecuteQuery customized
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     -
  Protected Overrides Sub GUI_ExecuteQueryCustom()
    Dim _function_name As String
    Dim _bln_redraw As Boolean
    Dim _alarms_to_pattern As DataTable
    Dim _rows_alarms As DataRow()
    Dim _new_row As DataRow
    Dim _pattern_list_dic As Dictionary(Of String, PendingPattern)
    Dim _table_patterns As DataTable
    Dim _idx_row As Integer
    Dim _row As DataRow
    Dim _db_row As CLASS_DB_ROW
    Dim _idx_grid As Integer
    Dim _has_changes As Boolean
    Dim _count As Integer
    Dim _row_step As Integer
    Dim _max_rows As Integer
    Dim _more_than_max As Boolean
    Dim _pattern_template_dic As Dictionary(Of Int64, PatternTemplate)
    Dim _patterns_selected As Int32()
    Dim _ordered_list As Stack(Of String)
    Dim _element As PendingPattern

    _function_name = "GUI_ExecuteQueryCustom"

    _max_rows = GUI_MaxRows()

    _has_changes = False

    Try

      If Me.uc_checked_list_patterns.Count = 0 Then
        Call GUI_Exit()

        Return
      End If

      If m_alarms Is Nothing Then
        Call GUI_Exit()

        Return
      End If

      If m_alarms.Rows.Count = 0 Then
        Call GUI_Exit()

        Return
      End If

      Me.Grid.Clear()

      _alarms_to_pattern = New DataTable()
      _alarms_to_pattern.Columns.Add("ALARM_ID", System.Type.GetType("System.Int64"))
      _alarms_to_pattern.Columns.Add("ELEMENT_IDENTIFIER", System.Type.GetType("System.Int64"))
      _alarms_to_pattern.Columns.Add("ELEMENT_IDENTIFIER_NAME", System.Type.GetType("System.String"))
      _alarms_to_pattern.Columns.Add("ALARM_CODE", System.Type.GetType("System.Int32"))
      _alarms_to_pattern.Columns.Add("ALARM_NAME", System.Type.GetType("System.String"))
      _alarms_to_pattern.Columns.Add("ALARM_REPORTED", System.Type.GetType("System.DateTime"))
      _alarms_to_pattern.Columns.Add("ALARM_DATETIME", System.Type.GetType("System.DateTime"))

      ' 04-JUN-2014  JPJ    Fixed Bug WIG-1001: Pattern execute can't find some patterns
      If m_is_sites_alarms Then
        _rows_alarms = m_alarms.Select("SA_ALARM_ID > 0", "SA_SITE_ID, SA_DATETIME ASC, SA_ALARM_ID")
      Else
        _rows_alarms = m_alarms.Select("AL_ALARM_ID > 0", "Al_DATETIME ASC, AL_ALARM_ID")
      End If
      For Each _row_alarm As DataRow In _rows_alarms
        _new_row = _alarms_to_pattern.NewRow()
        _new_row(PATTERN_COLUMN_ALARM_ID) = _row_alarm(SQL_ALARMS_COLUMN_ALARM_ID)           ' AL_ALARM_ID
        _new_row(PATTERN_COLUMN_IDENTIFIER) = _row_alarm(SQL_ALARMS_COLUMN_SOURCE_ID)        ' AL_SOURCE_ID
        _new_row(PATTERN_COLUMN_IDENTIFIER_NAME) = _row_alarm(SQL_ALARMS_COLUMN_SOURCE_NAME) ' AL_SOURCE_NAME
        _new_row(PATTERN_COLUMN_ALARM_CODE) = _row_alarm(SQL_ALARMS_COLUMN_ALARM_CODE)       ' AL_ALARM_CODE
        _new_row(PATTERN_COLUMN_ALARM_NAME) = _row_alarm(SQL_ALARMS_COLUMN_DESCRIPTION)      ' AL_ALARM_NAME
        ' 05-JUN-2014  JPJ    Fixed Bug WIG-1002: Some recursive patterns don't show up
        _new_row(PATTERN_COLUMN_ALARM_REPORTED) = _row_alarm(SQL_ALARMS_COLUMN_REPORTED)     ' AL_REPORTED
        _new_row(PATTERN_COLUMN_ALARM_DATETIME) = _row_alarm(SQL_ALARMS_COLUMN_DATETIME)     ' AL_DATETIME
        _alarms_to_pattern.Rows.Add(_new_row)
      Next

      ' JAB 24-AUG-2012: Control to show huge amounts of records.
      If Not ShowHugeNumberOfRows(_alarms_to_pattern.Rows.Count) Then
        MyBase.m_user_canceled_data_shows = True
        Exit Sub
      End If

      _bln_redraw = Me.Grid.Redraw
      Me.Grid.Redraw = False

      Call GUI_BeforeFirstRow()

      _row_step = _max_rows
      _row_step = _row_step / 20
      If _row_step < MAX_RECORDS_FIRST_LOAD Then
        _row_step = MAX_RECORDS_FIRST_LOAD
      End If

      _count = 0
      _idx_grid = 0
      _idx_row = 0

      _pattern_template_dic = New Dictionary(Of Int64, PatternTemplate)

      _patterns_selected = Me.uc_checked_list_patterns.SelectedIndexes()

      For _idx As Integer = 0 To _patterns_selected.Length - 1
        m_patern_struct = m_pattern_template_dic(_patterns_selected(_idx))
        _pattern_template_dic.Add(m_patern_struct.pattern_id, m_patern_struct)
      Next

      _pattern_list_dic = New Dictionary(Of String, PendingPattern)
      ' 05-JUN-2014  JPJ    Fixed Bug WIG-1001: Pattern execute can't find some patterns
      _alarms_to_pattern = New DataView(_alarms_to_pattern, String.Empty, "ALARM_ID ASC", DataViewRowState.CurrentRows).ToTable

      _ordered_list = New Stack(Of String)
      Call PatternLookUp(_alarms_to_pattern, _pattern_template_dic, _pattern_list_dic, _ordered_list)

      If _pattern_list_dic.Count = 0 Then
        Return
      End If

      _table_patterns = New DataTable()
      Call DefineTableGenerateAlarmPattern(_table_patterns)

      If cb_show_open_patterns.Checked Then
        'These are the closed and open patterns, but we only take the open ones
        For Each _element_temp As PendingPattern In _pattern_list_dic.Values
          If _element_temp.State <> PATTERN_STATE.CLOSED Then
            PatternToGrid(_table_patterns, _element_temp, _pattern_template_dic)
          End If
        Next
      End If

      'These are the closed patterns
      For Each _key As String In _ordered_list
        _element = New PendingPattern
        _element = _pattern_list_dic(_key)

        If _element.State <> PATTERN_STATE.CLOSED Then

          Continue For
        End If

        PatternToGrid(_table_patterns, _element, _pattern_template_dic)
      Next

      While _idx_row < _table_patterns.Rows.Count
        Try
          _row = _table_patterns.Rows(_idx_row)
          _db_row = New CLASS_DB_ROW(_row)

          ' Add the db row to the grid
          Me.Grid.AddRow()
          _idx_grid = Me.Grid.NumRows - 1

          If GUI_SetupRow(_idx_grid, _db_row) Then
            _count = _count + 1
          End If

          _idx_row = _idx_row + 1

        Catch exception As Exception
          Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
          Call Trace.WriteLine(exception.ToString())
          Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                                "Alarms", _
                                _function_name, _
                                exception.Message)
          Exit While
        End Try

        ' RCI 22-DEC-2010: Need count > 0, otherwise it's always doing Redraw for 0 rows!!
        If _count > 0 And _count Mod _row_step = 0 Then
          Me.Grid.Redraw = True
          Call Application.DoEvents()
          Windows.Forms.Cursor.Current = Cursors.WaitCursor
          Me.Grid.Redraw = False
        End If

        If _count >= _max_rows Then
          _more_than_max = True

          Exit While
        End If
      End While

      Call GUI_AfterLastRow()

      Me.Grid.Redraw = True

      If _more_than_max Then
        Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(111), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, , , CStr(_max_rows))
      End If

    Catch ex As Exception
      ' An error has occurred in the query execution
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "Alarms", _
                            _function_name, _
                            ex.Message)
      Me.Grid.Redraw = True

    Finally
      ' JAB 01-JUN-2012: Check if disposed.
      If Not Me.IsDisposed Then
        GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = (Me.Grid.NumRows > 0 And Me.Permissions.Write)
      End If

    End Try

  End Sub ' GUI_ExecuteQueryCustom

  ' PURPOSE: Enable button in selected row
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_RowSelectedEvent(ByVal SelectedRow As Integer)

    m_selected_row = SelectedRow

    'Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = True ' (Me.Grid.Cell(SelectedRow, GRID_COLUMN_SOURCE_CODE).Value = AlarmSourceCode.Pattern) & CurrentUser.Permissions(ENUM_FORM.FORM_ALARM_INQUIRY_SOURCE).Read

  End Sub  ' GUI_RowSelectedEvent

  ' PURPOSE: Process clicks on data grid (double-clicks) and select button
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId
      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        Call ExpandContractPattern()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)

    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    If Me.uc_checked_list_patterns.Count = 0 Then
      Return False
    End If

    If Me.uc_checked_list_patterns.SelectedIndexes().Length = 0 Then
      Return False
    End If

    Return True
  End Function ' GUI_FilterCheck


#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_ALARMS.GetString(479), m_show_open_patterns)
    PrintData.SetFilter(GLB_NLS_GUI_ALARMS.GetString(478), m_selected_patterns)
    PrintData.FilterValueWidth(1) = 3000
    PrintData.FilterValueWidth(2) = 4000
  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_selected_patterns = ""
    'm_alarm_desc = ""

    ' Patterns
    If uc_checked_list_patterns.SelectedIndexes.Length = uc_checked_list_patterns.Count() Then
      m_selected_patterns = GLB_NLS_GUI_AUDITOR.GetString(263) 'All
    Else
      m_selected_patterns = uc_checked_list_patterns.SelectedValuesList()
    End If

    If Me.cb_show_open_patterns.Checked Then
      m_show_open_patterns = GLB_NLS_GUI_ALARMS.GetString(318)
    Else
      m_show_open_patterns = GLB_NLS_GUI_ALARMS.GetString(319)
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window _
                       , ByVal IsSitesAlarms As Boolean _
                       , ByVal Alarms As DataTable _
                       , ByVal AlarmDic As Dictionary(Of Integer, ALARMS_CATALOG))

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    m_is_sites_alarms = IsSitesAlarms

    Try

      If IsSitesAlarms Then
        m_alarms = New DataView(Alarms, String.Empty, "SA_DATETIME ASC, SA_ALARM_ID ASC", DataViewRowState.CurrentRows).ToTable
      Else
        m_alarms = New DataView(Alarms, String.Empty, "AL_DATETIME ASC, AL_ALARM_ID ASC", DataViewRowState.CurrentRows).ToTable
      End If
    Catch _ex As Exception

      Debug.WriteLine(_ex.Message)

    End Try

    m_alarms_dic = AlarmDic
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    cb_show_open_patterns.Checked = False
    Me.uc_checked_list_patterns.SetDefaultValue(True)

  End Sub

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub GUI_StyleSheet()
    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      .Counter(COUNTER_PATTERN_CLOSE).Visible = COUNTER_ALL_VISIBLE
      .Counter(COUNTER_PATTERN_CLOSE).BackColor = GetColor(COLOR_PATTERN_CLOSE_BACK)
      .Counter(COUNTER_PATTERN_CLOSE).ForeColor = GetColor(COLOR_PATTERN_CLOSE_FORE)

      .Counter(COUNTER_PATTERN_OPEN).Visible = COUNTER_ALL_VISIBLE
      .Counter(COUNTER_PATTERN_OPEN).BackColor = GetColor(COLOR_PATTERN_OPEN_BACK)
      .Counter(COUNTER_PATTERN_OPEN).ForeColor = GetColor(COLOR_PATTERN_OPEN_FORE)

      .Counter(COUNTER_ALARM).Visible = COUNTER_ALL_VISIBLE
      .Counter(COUNTER_ALARM).BackColor = GetColor(COLOR_ALARM_BACK)
      .Counter(COUNTER_ALARM).ForeColor = GetColor(COLOR_ALARM_FORE)

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Header(1).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = 380
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False
      .Column(GRID_COLUMN_INDEX).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Alarm Id
      .Column(GRID_COLUMN_ALARM_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_ALARM_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_ALARM_ID).Width = 0

      ' Source Code
      .Column(GRID_COLUMN_SOURCE_CODE).Header(0).Text = ""
      .Column(GRID_COLUMN_SOURCE_CODE).Header(1).Text = ""
      .Column(GRID_COLUMN_SOURCE_CODE).Width = 0

      ' Timestamp
      .Column(GRID_COLUMN_TIMESTAMP).Header(0).Text = ""
      .Column(GRID_COLUMN_TIMESTAMP).Header(1).Text = ""
      .Column(GRID_COLUMN_TIMESTAMP).Width = 0

      ' Alarm Id
      If m_is_sites_alarms Then
        .Column(GRID_COLUMN_SITE_ID).Header(0).Text = ""
        .Column(GRID_COLUMN_SITE_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1927) ' SiteId
        .Column(GRID_COLUMN_SITE_ID).Width = 850
        .Column(GRID_COLUMN_SITE_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      Else
        .Column(GRID_COLUMN_SITE_ID).Header(0).Text = ""
        .Column(GRID_COLUMN_SITE_ID).Header(1).Text = ""
        .Column(GRID_COLUMN_SITE_ID).Width = 0
      End If

      'Pattern name
      .Column(GRID_COLUMN_PATTERN_NAME).Header(0).Text = " "
      .Column(GRID_COLUMN_PATTERN_NAME).Header(1).Text = GLB_NLS_GUI_ALARMS.GetString(472)
      .Column(GRID_COLUMN_PATTERN_NAME).Width = 2500
      .Column(GRID_COLUMN_PATTERN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Alarm group
      .Column(GRID_COLUMN_ALARM_GROUP).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(207)
      .Column(GRID_COLUMN_ALARM_GROUP).Header(1).Text = GLB_NLS_GUI_ALARMS.GetString(462)
      .Column(GRID_COLUMN_ALARM_GROUP).Width = 1500
      .Column(GRID_COLUMN_ALARM_GROUP).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Alarm category
      .Column(GRID_COLUMN_ALARM_CATEGORY).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(207)
      .Column(GRID_COLUMN_ALARM_CATEGORY).Header(1).Text = GLB_NLS_GUI_ALARMS.GetString(463)
      .Column(GRID_COLUMN_ALARM_CATEGORY).Width = 2000
      .Column(GRID_COLUMN_ALARM_CATEGORY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Date Generated
      .Column(GRID_COLUMN_DATETIME).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(207)
      .Column(GRID_COLUMN_DATETIME).Header(1).Text = GLB_NLS_GUI_ALARMS.GetString(443)
      .Column(GRID_COLUMN_DATETIME).Width = 2100
      .Column(GRID_COLUMN_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Severity
      .Column(GRID_COLUMN_SEVERITY).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(207)
      .Column(GRID_COLUMN_SEVERITY).Header(1).Text = GLB_NLS_GUI_ALARMS.GetString(416)
      .Column(GRID_COLUMN_SEVERITY).Width = 1100
      .Column(GRID_COLUMN_SEVERITY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Source
      .Column(GRID_COLUMN_SOURCE_NAME).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(207)
      .Column(GRID_COLUMN_SOURCE_NAME).Header(1).Text = GLB_NLS_GUI_ALARMS.GetString(209)
      .Column(GRID_COLUMN_SOURCE_NAME).Width = 4000
      .Column(GRID_COLUMN_SOURCE_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Description
      .Column(GRID_COLUMN_ALARM_DESCRIPTION).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(207)
      .Column(GRID_COLUMN_ALARM_DESCRIPTION).Header(1).Text = GLB_NLS_GUI_ALARMS.GetString(269)
      .Column(GRID_COLUMN_ALARM_DESCRIPTION).Width = 5200
      .Column(GRID_COLUMN_ALARM_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Ack Datetime
      .Column(GRID_COLUMN_ACK_DATETIME).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(208)
      .Column(GRID_COLUMN_ACK_DATETIME).Header(1).Text = GLB_NLS_GUI_ALARMS.GetString(443)
      .Column(GRID_COLUMN_ACK_DATETIME).Width = 2100
      .Column(GRID_COLUMN_ACK_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Ack Username
      .Column(GRID_COLUMN_ACK_USER_NAME).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(208)
      .Column(GRID_COLUMN_ACK_USER_NAME).Header(1).Text = GLB_NLS_GUI_ALARMS.GetString(267)
      .Column(GRID_COLUMN_ACK_USER_NAME).Width = 2000
      .Column(GRID_COLUMN_ACK_USER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Return the String representation of an AlarmSeverity.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Id
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Private Function DecodeSeverity(ByVal Id As WSI.Common.AlarmSeverity) As String

    Dim result As String

    result = ""
    Select Case Id
      Case WSI.Common.AlarmSeverity.Info
        result = GLB_NLS_GUI_ALARMS.GetString(206)
      Case WSI.Common.AlarmSeverity.Warning
        result = GLB_NLS_GUI_ALARMS.GetString(358)
      Case WSI.Common.AlarmSeverity.Error
        result = GLB_NLS_GUI_ALARMS.GetString(357)
    End Select

    Return result
  End Function ' DecodeSeverity

  ' PURPOSE: Show new window with the information of the alarms that generated it.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Severity
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub InquirySourcePatterns()
    Dim _idx_row As Short
    Dim _alarm_id As Integer
    Dim _site_id As Int64

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    If _idx_row = Me.Grid.NumRows Then
      Return
    End If

    If Not (Me.Grid.Cell(_idx_row, GRID_COLUMN_SOURCE_CODE).Value = 14) Then
      Call NLS_MsgBox(GLB_NLS_GUI_ALARMS.Id(467), ENUM_MB_TYPE.MB_TYPE_INFO)

      Return
    End If

    If m_is_sites_alarms Then
      _site_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_SITE_ID).Value
    End If
    _alarm_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_ALARM_ID).Value
    ' m_alarm_id = _alarm_id
    m_site_id = _site_id

    Call SetDefaultValues()

    '    Call Me.Grid.Focus()

  End Sub ' InquirySourcePatterns

  ' PURPOSE: For convert Int64 array to String array
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Function ConvertToString(ByVal Input As Int64) As String
    Dim _output As String = ""

    _output = Input.ToString()

    Return _output
  End Function ' ConvertToString

  ' PURPOSE: Define table: generate alarm pattern
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub DefineTableGenerateAlarmPattern(ByVal Table As DataTable)

    Table.Columns.Add("SITE_ID", System.Type.GetType("System.Int32"))
    Table.Columns.Add("ALARM_ID", System.Type.GetType("System.Int32"))
    Table.Columns.Add("SOURCE_CODE", System.Type.GetType("System.Int32"))
    Table.Columns.Add("SOURCE_ID", System.Type.GetType("System.Int32"))
    Table.Columns.Add("SOURCE_NAME", System.Type.GetType("System.String"))
    Table.Columns.Add("ALARM_CODE", System.Type.GetType("System.Int32"))
    Table.Columns.Add("ALARM_NAME", System.Type.GetType("System.String"))
    Table.Columns.Add("ALARM_DESCRIPTION", System.Type.GetType("System.String"))
    Table.Columns.Add("SEVERITY", System.Type.GetType("System.Int32"))
    Table.Columns.Add("REPORTED", System.Type.GetType("System.DateTime"))
    Table.Columns.Add("DATETIME", System.Type.GetType("System.DateTime"))
    Table.Columns.Add("ACK_DATETIME", System.Type.GetType("System.DateTime"))
    Table.Columns.Add("ACK_USER_ID", System.Type.GetType("System.Int64"))
    Table.Columns.Add("ACK_USER_NAME", System.Type.GetType("System.String"))
    Table.Columns.Add("TIMESTAMP", System.Type.GetType("System.Int64"))
    Table.Columns.Add("ELEMENT_VALUES", System.Type.GetType("System.String"))
    Table.Columns.Add("PATTERN_ID", System.Type.GetType("System.Int64"))
    Table.Columns.Add("PATTERN_NAME", System.Type.GetType("System.String"))
    Table.Columns.Add("STATE", System.Type.GetType("System.Int32"))


  End Sub

  Private Sub FillPatternsControl()
    Dim _sb As StringBuilder
    Dim _index As Int32
    Dim _name As String
    Dim _dt As DataTable
    Dim _pattern As Int64()
    Dim _select As String
    Dim _life_time As Int32
    Dim _terminals As TerminalList

    _pattern = Nothing
    _dt = New DataTable()

    ' Fill uc_checked_list_patterns control with groups
    Call Me.uc_checked_list_patterns.Clear()
    Call Me.uc_checked_list_patterns.ReDraw(False)

    m_pattern_template_dic = New Dictionary(Of Int64, PatternTemplate)

    _life_time = GeneralParam.GetInt32("Pattern", "Pattern.LifeTime", PATTERN_LIFE_TIME, PATTERN_LIFE_TIME_MIN, PATTERN_LIFE_TIME_MAX)

    Try
      _sb = New StringBuilder()

      _sb.AppendLine("SELECT   PT_ID ")
      _sb.AppendLine("       , PT_NAME ")
      _sb.AppendLine("       , PT_PATTERN ")
      _sb.AppendLine("       , PT_AL_CODE ")
      _sb.AppendLine("       , PT_AL_NAME ")
      _sb.AppendLine("       , PT_AL_DESCRIPTION ")
      _sb.AppendLine("       , PT_AL_SEVERITY ")
      _sb.AppendLine("       , PT_TYPE ")
      _sb.AppendLine("       , PT_RESTRICTED_TO_TERMINAL_LIST ")
      _sb.AppendLine("       , PT_SCHEDULE_TIME_FROM ")
      _sb.AppendLine("       , PT_SCHEDULE_TIME_TO ")
      _sb.AppendLine("       , PT_LIFE_TIME ")
      _sb.AppendLine("       , PT_RESTRICTED_TO_TERMINAL_LIST")
      _sb.AppendLine("  FROM   PATTERNS ")
      _sb.AppendLine(" WHERE   PT_PATTERN IS NOT NULL ")
      _sb.AppendLine("   AND   CAST(PT_PATTERN AS NVARCHAR(MAX)) <> N'<Pattern/>' ")
      _sb.AppendLine(" ORDER   BY PT_NAME ")

      Using _db_trx As New WSI.Common.DB_TRX()
        Using _sql_command As New SqlClient.SqlCommand(_sb.ToString, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          If Not m_is_center Then
            Call GetPatternTerminals(_db_trx.SqlTransaction, _dt)
          End If

          Using _reader As SqlDataReader = _sql_command.ExecuteReader()
            While _reader.Read()

              _index = _reader.GetInt64(0)
              _name = _reader.GetString(1)
              Me.uc_checked_list_patterns.Add(1, _index, _name)

              m_patern_struct = New PatternTemplate()
              m_patern_struct.pattern_id = _reader.GetInt64(0)                                       ' Indicates which pattern is
              m_patern_struct.pattern_name = _reader.GetString(1)                                    ' Pattern name
              m_patern_struct.pattern_elements = m_patern_struct.toArray(_reader(2).ToString())      ' Pattern template
              m_patern_struct.alarm_code = _reader.GetInt32(3)                                       ' Identificaction code of the alarm to generate when pattern is found
              m_patern_struct.alarm_name = _reader.GetString(4)                                      ' Name of the alarm to generate when pattern is found
              m_patern_struct.alarm_description = _reader.GetString(5)                               ' Descripction of the found pattern
              m_patern_struct.alarm_severity = _reader.GetInt32(6)                                   ' severity of the alarm
              m_patern_struct.pattern_type = _reader.GetInt32(7)                                     ' How the pattern is processed
              m_patern_struct.time_from = IIf(IsDBNull(_reader(9)), 0, _reader(9))                   ' Pattern start time
              m_patern_struct.time_to = IIf(IsDBNull(_reader(10)), 0, _reader(10))                   ' Pattern end time
              m_patern_struct.life_time = IIf(IsDBNull(_reader(11)), 0, _reader(11))                 ' Pattern life time
              ' 06-JUN-2014  JPJ    Fixed Bug WIG-1004: Terminal restriction was not included (It was reopened due to assigning groups without terminals)
              m_patern_struct.terminal_list_type = 0
              If Not IsDBNull(_reader(12)) Then
                _terminals = New TerminalList()
                _terminals.FromXml(_reader(12))
                m_patern_struct.terminal_list_type = _terminals.ListType                             ' Terminal list type
              End If

              If (m_patern_struct.life_time > _life_time) Then
                m_patern_struct.life_time = _life_time
              End If

              If _dt.Rows.Count > 0 Then
                _select = TERMINAL_COLUMN_PATTERN_ID + " = " + m_patern_struct.pattern_id.ToString() ' Terminals are associated with the pattern
                ' 04-JUN-2014 JPJ    Fixed Bug WIG-1004: Terminal restriction was not included
                m_patern_struct.terminal_list = New DataView(_dt, _select, TERMINAL_COLUMN_TERMINAL_ID + " ASC", DataViewRowState.CurrentRows).ToTable
              Else
                m_patern_struct.terminal_list = New DataTable()
              End If
              m_pattern_template_dic.Add(m_patern_struct.pattern_id, m_patern_struct)

            End While
          End Using
        End Using
      End Using

      If uc_checked_list_patterns.Count > 0 Then
        Call uc_checked_list_patterns.CurrentRow(0)
        Call uc_checked_list_patterns.ReDraw(True)
        Call uc_checked_list_patterns.ResizeGrid()
      End If

      Me.uc_checked_list_patterns.CollapseAll()
      Me.uc_checked_list_patterns.SetDefaultValue(False)

    Catch ex As Exception

      Debug.WriteLine(ex.Message)

    End Try

  End Sub ' FillPatternsControl

  Private Sub ExpandContractPattern()
    Dim _height As Int32
    Dim _is_pattern As Boolean

    _is_pattern = False

    If Me.Grid.Cell(m_selected_row, GRID_COLUMN_INDEX).Value = TEXT_PLUS Then
      _height = m_row_height
      Me.Grid.Cell(m_selected_row, GRID_COLUMN_INDEX).Value = TEXT_MINOR
      _is_pattern = True
    ElseIf Me.Grid.Cell(m_selected_row, GRID_COLUMN_INDEX).Value = TEXT_MINOR Then
      _height = 0
      Me.Grid.Cell(m_selected_row, GRID_COLUMN_INDEX).Value = TEXT_PLUS
      _is_pattern = True
    End If

    If _is_pattern Then
      For _idx As Int32 = m_selected_row + 1 To Me.Grid.NumRows - 1
        If Me.Grid.Cell(_idx, GRID_COLUMN_INDEX).Value <> "" Then
          Exit For
        End If
        Me.Grid.Row(_idx).Height = _height
      Next
    End If

  End Sub  ' ExpandContractPattern

  ' PURPOSE: Prepares a pattern alarm row
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Function PatternElementToRow(ByVal Table As DataTable, ByVal Element As PendingPattern, _
                                       ByVal PatternName As String, ByVal ElementValues As String) As DataRow
    Dim _new_row As DataRow

    _new_row = Table.NewRow()
    _new_row(SQL_COLUMN_SITE_ID) = 0
    _new_row(SQL_COLUMN_ALARM_ID) = Element.temp_alarm_id
    _new_row(SQL_COLUMN_ALARM_ID) = Element.alarm_id
    _new_row(SQL_COLUMN_SOURCE_CODE) = AlarmSourceCode.Pattern
    _new_row(SQL_COLUMN_SOURCE_ID) = Element.element_identifier
    _new_row(SQL_COLUMN_SOURCE_NAME) = Element.element_identifier_name
    _new_row(SQL_COLUMN_ALARM_CODE) = Element.alarm_code
    _new_row(SQL_COLUMN_ALARM_NAME) = Element.alarm_name
    _new_row(SQL_COLUMN_ALARM_DESCRIPTION) = Element.alarm_description
    _new_row(SQL_COLUMN_SEVERITY) = Element.alarm_severity
    _new_row(SQL_COLUMN_REPORTED) = Element.last_element_time
    _new_row(SQL_COLUMN_DATETIME) = Element.last_element_time
    _new_row(SQL_COLUMN_ACK_DATETIME) = DBNull.Value
    _new_row(SQL_COLUMN_ACK_USER_ID) = 0
    _new_row(SQL_COLUMN_ACK_USER_NAME) = ""
    _new_row(SQL_COLUMN_TIMESTAMP) = 0
    _new_row(SQL_COLUMN_ELEMENT_VALUES) = ElementValues
    _new_row(SQL_COLUMN_PATTERN_ID) = Element.pattern_id
    _new_row(SQL_COLUMN_PATTERN_NAME) = PatternName
    _new_row(SQL_COLUMN_STATE) = Element.State

    Return _new_row
  End Function  ' PatternElementToRow

  ' PURPOSE: Generates the alarms that meets a pattern
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub GenerateChildRows(ByVal TablePatterns As DataTable, ByVal AlarmRows() As DataRow)
    Dim _new_row As DataRow

    For Each _row_alarm As DataRow In AlarmRows
      _new_row = TablePatterns.NewRow()
      _new_row(SQL_COLUMN_SITE_ID) = _row_alarm(SQL_ALARMS_COLUMN_SITE_ID)
      _new_row(SQL_COLUMN_ALARM_ID) = _row_alarm(SQL_ALARMS_COLUMN_ALARM_ID)
      _new_row(SQL_COLUMN_SOURCE_CODE) = _row_alarm(SQL_ALARMS_COLUMN_SOURCE_CODE)
      _new_row(SQL_COLUMN_SOURCE_ID) = _row_alarm(SQL_ALARMS_COLUMN_SOURCE_ID)
      _new_row(SQL_COLUMN_SOURCE_NAME) = _row_alarm(SQL_ALARMS_COLUMN_SOURCE_NAME)
      _new_row(SQL_COLUMN_ALARM_CODE) = _row_alarm(SQL_ALARMS_COLUMN_ALARM_CODE)
      _new_row(SQL_COLUMN_ALARM_NAME) = _row_alarm(SQL_ALARMS_COLUMN_ALARM_NAME)
      _new_row(SQL_COLUMN_ALARM_DESCRIPTION) = _row_alarm(SQL_ALARMS_COLUMN_ALARM_DESCRIPTION)
      _new_row(SQL_COLUMN_SEVERITY) = _row_alarm(SQL_ALARMS_COLUMN_SEVERITY)
      _new_row(SQL_COLUMN_REPORTED) = _row_alarm(SQL_ALARMS_COLUMN_REPORTED)
      _new_row(SQL_COLUMN_DATETIME) = _row_alarm(SQL_ALARMS_COLUMN_DATETIME)
      _new_row(SQL_COLUMN_ACK_DATETIME) = _row_alarm(SQL_ALARMS_COLUMN_ACK_DATETIME)
      _new_row(SQL_COLUMN_ACK_USER_ID) = _row_alarm(SQL_ALARMS_COLUMN_ACK_USER_ID)
      _new_row(SQL_COLUMN_ACK_USER_NAME) = _row_alarm(SQL_ALARMS_COLUMN_ACK_USER_NAME)
      _new_row(SQL_COLUMN_TIMESTAMP) = _row_alarm(SQL_ALARMS_COLUMN_TIMESTAMP)
      _new_row(SQL_COLUMN_ELEMENT_VALUES) = _row_alarm(SQL_ALARMS_COLUMN_ALARM_ID)
      _new_row(SQL_COLUMN_PATTERN_ID) = DBNull.Value
      _new_row(SQL_COLUMN_PATTERN_NAME) = DBNull.Value
      _new_row(SQL_COLUMN_STATE) = -1
      TablePatterns.Rows.Add(_new_row)
    Next

  End Sub  ' GenerateChildRows

  ' PURPOSE: Prepares an alarm from a pattern of patterns
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub AlarmOfAPattern(ByVal Element As PendingPattern)
    Dim _new_row As DataRow

    _new_row = m_alarms.NewRow()
    _new_row(SQL_ALARMS_COLUMN_SITE_ID) = 0
    _new_row(SQL_ALARMS_COLUMN_ALARM_ID) = Element.temp_alarm_id
    _new_row(SQL_ALARMS_COLUMN_ALARM_ID) = Element.temp_alarm_id
    _new_row(SQL_ALARMS_COLUMN_SOURCE_CODE) = AlarmSourceCode.Pattern
    _new_row(SQL_ALARMS_COLUMN_SOURCE_ID) = Element.element_identifier
    _new_row(SQL_ALARMS_COLUMN_SOURCE_NAME) = Element.element_identifier_name
    _new_row(SQL_ALARMS_COLUMN_ALARM_CODE) = Element.alarm_code
    _new_row(SQL_ALARMS_COLUMN_ALARM_NAME) = Element.alarm_name
    _new_row(SQL_ALARMS_COLUMN_ALARM_DESCRIPTION) = Element.alarm_description
    _new_row(SQL_ALARMS_COLUMN_SEVERITY) = Element.alarm_severity
    _new_row(SQL_ALARMS_COLUMN_REPORTED) = Element.last_element_time
    _new_row(SQL_ALARMS_COLUMN_DATETIME) = Element.last_element_time
    _new_row(SQL_ALARMS_COLUMN_ACK_DATETIME) = DBNull.Value
    _new_row(SQL_ALARMS_COLUMN_ACK_USER_ID) = 0
    _new_row(SQL_ALARMS_COLUMN_ACK_USER_NAME) = ""
    _new_row(SQL_ALARMS_COLUMN_TIMESTAMP) = 0

    m_alarms.Rows.Add(_new_row)

  End Sub  ' AlarmOfAPattern

  ' PURPOSE: Prepares the patterns to be inserted in the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub PatternToGrid(ByVal TablePatterns As DataTable, _
                            ByVal Element As PendingPattern, _
                            ByVal _pattern_template_dic As Dictionary(Of Int64, PatternTemplate))
    Dim _element_values_str() As String
    Dim _element_values_string As String
    Dim _exist_pattern_alarm As Int16
    Dim _rows_alarms As DataRow()

    _element_values_str = Array.ConvertAll(Element.element_values, New Converter(Of Int64, String)(AddressOf ConvertToString))
    _element_values_string = String.Join(", ", _element_values_str)
    TablePatterns.Rows.Add(PatternElementToRow(TablePatterns, Element, _
                                               _pattern_template_dic(Element.pattern_id).pattern_name, _
                                               _element_values_string))

    If m_is_sites_alarms Then
      _exist_pattern_alarm = m_alarms.Select("SA_ALARM_ID = " & Element.temp_alarm_id & " ", "SA_SITE_ID, SA_DATETIME DESC, SA_ALARM_ID").Length
    Else
      _exist_pattern_alarm = m_alarms.Select("AL_ALARM_ID = " & Element.temp_alarm_id & " ", "Al_DATETIME DESC, AL_ALARM_ID").Length
    End If

    If _exist_pattern_alarm = 0 AndAlso Element.temp_alarm_id <> 0 Then
      Call AlarmOfAPattern(Element)
    End If

    If m_is_sites_alarms Then
      _rows_alarms = m_alarms.Select("SA_ALARM_ID IN (" & _element_values_string & ")", "SA_SITE_ID, SA_DATETIME DESC, SA_ALARM_ID")
    Else
      _rows_alarms = m_alarms.Select("AL_ALARM_ID IN (" & _element_values_string & ")", "AL_DATETIME DESC, AL_ALARM_ID")
    End If
    If _rows_alarms.Length = 0 Then

      Return
    End If

    Call GenerateChildRows(TablePatterns, _rows_alarms)

  End Sub  ' AlarmOfAPattern

#End Region ' Private Functions

End Class