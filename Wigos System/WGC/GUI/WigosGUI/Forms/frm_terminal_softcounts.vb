'-------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_terminal_softcount.vb
'
' DESCRIPTION :
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 21-OCT-2014  HBB    Initial version
' 26-NOV-2014  SGB    Fixed Bug WIG-1761: Error to open this form when not is configured counter
'--------------------------------------------------------------------


Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common
Imports System.Data.SqlClient
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_Reports.PrintDataset
Imports System.Text
Imports System.Runtime.InteropServices
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports WSI.Common.TITO
Imports WSI.Common.NoteScanner

Public Class frm_terminal_softcounts
  Inherits frm_base_edit

#Region "Constants"
  ' total grid, count of headers and columns
  Private Const GRID_TOTAL_COLUMN_COUNT As Int32 = 4
  Private Const GRID_TOTAL_HEADERS_COUNT As Int32 = 1

  ' Total grid columns
  Private Const GRID_TOTAL_COLUMN_CURRENCY As Int32 = 0
  Private Const GRID_TOTAL_COLUMN_COUNT_OF_BILLS As Int32 = 1
  Private Const GRID_TOTAL_COLUMN_AMOUNT As Int32 = 2
  Private Const GRID_TOTAL_COLUMN_AMOUNT_WITH_SIMBOL As Int32 = 3

  ' Total grid widthes
  Private Const GRID_TOTAL_CURRENCY_WIDTH As Int32 = 2500
  Private Const GRID_TOTAL_CURRENCY_WIDTH_WITH_SIMBOL As Int32 = 2500
  Private Const GRID_TOTAL_COUNT_OF_BILLS_WIDTH As Int32 = 3250
  Private Const GRID_TOTAL_AMOUNT_WIDTH_WITH_SIMBOL As Int32 = 3250
  Private Const GRID_TOTAL_AMOUNT_WIDTH = 0

  'currencies grids, count of headers and columns
  Private Const GRID_CURRENCIES_COLUMN_COUNT As Int32 = 6
  Private Const GRID_CURRENCIES_HEADERS_COUNT As Int32 = 1

  Private Const GRID_CURRENCIES_COLUMN_ISOCODE As Int32 = 0
  Private Const GRID_CURRENCIES_COLUMN_CURRENCY As Int32 = 1
  Private Const GRID_CURRENCIES_COLUMN_CURRENCY_WITH_SIMBOL As Int32 = 2
  Private Const GRID_CURRENCIES_COLUMN_COUNT_OF_BILLS As Int32 = 3
  Private Const GRID_CURRENCIES_COLUMN_AMOUNT As Int32 = 4
  Private Const GRID_CURRENCIES_COLUMN_AMOUNT_WITH_SIMBOL As Int32 = 5

  Private Const GRID_CURRENCY_ISOCODE_WIDTH = 0
  Private Const GRID_CURRENCY_WIDTH As Int32 = 0
  Private Const GRID_CURRENCY_WIDTH_WITH_SIMBOL As Int32 = 2500
  Private Const GRID_CURRENCY_COUNT_OF_BILLS_WIDTH As Int32 = 3250
  Private Const GRID_CURRENCY_AMOUNT_WIDTH_WITH_SIMBOL As Int32 = 3250
  Private Const GRID_CURRENCY_AMOUNT_WIDTH = 0

  'ticket grid, count of headers and columns
  Private Const GRID_TICKET_COLUMN_COUNT As Int32 = 5
  Private Const GRID_TICKET_HEADERS_COUNT As Int32 = 2

  ' tickets columns
  Private Const GRID_TICKET_COLUMN_TICKET_DENOMINATION = 0
  Private Const GRID_TICKET_COLUMN_TICKET_ID As Int32 = 1
  Private Const GRID_TICKET_VALIDATION_COLUMN_NUMBER_ORIGINAL_FORMAT As Int32 = 2
  Private Const GRID_TICKET_COLUMN_VALIDATION_NUMBER As Int32 = 3
  Private Const GRID_TICKET_COLUMN_VALUE As Int32 = 4

  'ticket sql row
  Private Const SQL_TICKET_ID As Int32 = 0
  Private Const SQL_TICKET_VALUE As Int32 = 1

  'Const GRID_COLUMN_TICKET_NUMBER_CORRECT_FORMAT As Int32 = 0
  'Const GRID_COLUMN_TICKET_TYPE As Int32 = 5
  'Const GRID_COLUMN_TICKET_STATUS As Int32 = 6
  'Const GRID_COLUMN_TICKET_BELONG As Int32 = 7

  ' tickets width
  Private Const GRID_TICKET_WIDTH_VALIDATION_NUMBER = 3610
  Private Const GRID_TICKET_WIDTH_VALUE = 2635

  Private m_count_of_bills As Int64


#End Region

#Region "Members"
  Private WithEvents m_scanner_manager As ScannerManager
  Private m_frm_counter_event As frm_note_counter_events
  Public m_list_of_event As List(Of String)
  Private m_frm_note_counter_events As frm_note_counter_events
  Private m_scanner_initialized As Boolean = False
  Private m_first_error As Boolean
  Private m_second_error As Boolean
  Private m_softcount_started As Boolean
#End Region

#Region "Overrides Functions"

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_TERMINAL_SOFTCOUNT
    Call MyBase.GUI_SetFormId()

  End Sub

  Protected Overrides Sub GUI_InitControls()

    MyBase.GUI_InitControls()
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5638)

    Me.gb_collection_params.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2256)
    Me.ef_insertion_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2251)
    Me.ef_extraction_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2263)
    Me.ef_stacker_id.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2252)
    Me.ef_floor_id.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5200)
    Me.ef_employee.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2896)
    Me.ef_terminal.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2254)
    Me.lbl_notes.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2255)
    Me.gb_total.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2936)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5208)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_CONTROLS.GetString(7)
    Me.ef_tickets.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, 18)
    Me.btn_save_tickets.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2262)
    Me.tf_ok.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1972)
    Me.tf_error.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1974)
    Me.tf_pending_tickets.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5238)
    Me.gb_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1971)
    Me.tb_currency.TabPages("tab_tickets").Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4600)
    Me.btn_insert_ticket.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2262)
    Me.gb_note_counter_events.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5452)
    Me.gb_note_counter_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5451)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5346)

    Me.ef_insertion_date.IsReadOnly = True
    Me.ef_extraction_date.IsReadOnly = True
    Me.ef_employee.IsReadOnly = True
    Me.ef_terminal.IsReadOnly = True

    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True

    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = False


    Me.ef_employee.TextValue = CurrentUser.Name

    Me.ef_stacker_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 18)

    m_list_of_event = New List(Of String)
    m_frm_counter_event = New frm_note_counter_events(m_list_of_event)

    Call GUI_StyleSheetTotalGrid()
    Call InitTotalGrid()
    Call LoadCurrenciesTab()
    Call GUI_StyleSheetTicketGrid()
    Call InitializeScanner()

  End Sub

  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)

    Dim _terminal_softcount As CLASS_SOFTCOUNT

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_SOFTCOUNT()
        _terminal_softcount = DbEditedObject
        _terminal_softcount.SofctountType = ENUM_SOFTCOUNT_TYPE.TYPE_TERMINAL
        _terminal_softcount.InsertionDate = Nothing
        _terminal_softcount.ExtractionDate = Nothing
        _terminal_softcount.FloorId = ""
        _terminal_softcount.StackerId = 0

        _terminal_softcount.GetCurrenciesList()
        _terminal_softcount.GetDenominationsByCurrencies()
      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select
  End Sub

  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    If Not m_softcount_started Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5680), ENUM_MB_TYPE.MB_TYPE_ERROR)
      ef_stacker_id.Focus()
      Return False
    End If

    If Not CkeckCountOfTickets() Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5681), ENUM_MB_TYPE.MB_TYPE_ERROR)
      Return False
    End If

    Return True
  End Function

  Protected Overrides Sub GUI_GetScreenData()
    Dim _softcount As CLASS_SOFTCOUNT

    _softcount = DbEditedObject

    If GUI_IsScreenDataOk() Then
      Call CreateDataSet()
    End If


  End Sub

  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    Dim _softcount As CLASS_SOFTCOUNT
    Dim _insertion_date_str As String
    Dim _extraction_dare_str As String

    _softcount = DbEditedObject

    _insertion_date_str = IIf(_softcount.InsertionDate <> Nothing, GUI_FormatDate(_softcount.InsertionDate, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM), "")
    _extraction_dare_str = IIf(_softcount.ExtractionDate <> Nothing, GUI_FormatDate(_softcount.ExtractionDate, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM), "")
    Me.ef_terminal.Value = _softcount.TerminalName
    Me.ef_insertion_date.TextValue = _insertion_date_str
    Me.ef_extraction_date.TextValue = _extraction_dare_str

  End Sub

  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean
    Dim _datagrid As uc_grid

    Select Case e.KeyChar
      Case Chr(Keys.Enter)
        If Me.ActiveControl.Name = ef_stacker_id.Name Then
          Call LoadSoftCountByStackerOrFloor(ef_stacker_id.TextValue, ENUM_SOFTCOUNT_FIELD.FIELD_STACKER)
          Return True
        ElseIf Me.ActiveControl.Name = ef_floor_id.Name Then
          Call LoadSoftCountByStackerOrFloor(ef_floor_id.TextValue, ENUM_SOFTCOUNT_FIELD.FIELD_FLOORID)
          Return True
        End If

        If Me.ActiveControl.Name.Equals("uc_grid") Then
          _datagrid = Me.ActiveControl
          _datagrid.KeyPressed(sender, e)
          Return True
        End If
      Case Else
        Return MyBase.GUI_KeyPress(sender, e)

    End Select
  End Function

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CUSTOM_1
        Call CleanSoftCountGrids()
      Case ENUM_BUTTON.BUTTON_CUSTOM_2
        m_frm_counter_event.ShowForEdit(Me.Parent)
      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        Call ResetSoftcount()
      Case Else
        MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub

  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)

    If m_scanner_manager IsNot Nothing Then
      m_scanner_manager.CloseScanner()
      RemoveHandler m_scanner_manager.NoteCountReceivedEvent, AddressOf CatchMessageFromScanner
      'tmr_note_counter_working.Stop()
    End If

  End Sub

#End Region

#Region "Private Functions"

  Private Sub LoadSoftCountByStackerOrFloor(ByVal Id As String, ByVal SoftCountField As ENUM_SOFTCOUNT_FIELD)
    Dim _softcount As CLASS_SOFTCOUNT

    _softcount = DbEditedObject

    Select Case SoftCountField
      Case ENUM_SOFTCOUNT_FIELD.FIELD_FLOORID
        _softcount.FloorId = Id
        m_softcount_started = True

      Case ENUM_SOFTCOUNT_FIELD.FIELD_STACKER
        If Not Int64.TryParse(Id, _softcount.StackerId) Then
          _softcount.StackerId = 0 'mostrar por pantalla error
        Else
          m_softcount_started = True
        End If
    End Select

    Me.DbObjectId = Id

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)

    Select Case DbStatus
      Case ENUM_STATUS.STATUS_OK
        Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
        Call GUI_SetScreenData(0)
        ef_stacker_id.Enabled = False
        ef_floor_id.Enabled = False
        Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = True

      Case ENUM_STATUS.STATUS_NOT_FOUND
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4944), _
                                                  mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
                                                  ENUM_MB_BTN.MB_BTN_OK, , )

    End Select

  End Sub

  Private Sub InitTotalGrid()
    Dim _softcount As CLASS_SOFTCOUNT
    Dim _total_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4708)

    _softcount = DbReadObject

    dg_total.Clear()

    ' set the currencies to the total grid
    For Each _str As String In _softcount.ListOfCurrencies
      Call SetUpRowTotalGrid(_str)
    Next

    Call SetUpRowTotalGrid(_total_str)

  End Sub

  Private Sub SetUpRowTotalGrid(ByVal Currency As String)

    Dim _ticket_tito_str As String = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4600)
    Dim _idx_row As Integer

    Me.dg_total.AddRow()
    _idx_row = Me.dg_total.NumRows - 1
    Me.dg_total.Cell(_idx_row, GRID_TOTAL_COLUMN_CURRENCY).Value = Currency
    Me.dg_total.Cell(_idx_row, GRID_TOTAL_COLUMN_COUNT_OF_BILLS).Value = 0
    Me.dg_total.Cell(_idx_row, GRID_TOTAL_COLUMN_AMOUNT).Value = 0

    If Currency <> _ticket_tito_str Then
      Me.dg_total.Cell(_idx_row, GRID_TOTAL_COLUMN_AMOUNT_WITH_SIMBOL).Value = GUI_FormatCurrency(0)
    Else

    End If


  End Sub

  Private Sub LoadCurrenciesTab()

    Dim _softcount As CLASS_SOFTCOUNT
    Dim _tickets_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4600)
    Dim _index As Int32

    _softcount = DbReadObject
    _index = 0

    For Each _str As String In _softcount.ListOfCurrencies
      If _str <> _tickets_str Then
        Call AddTabPageAndGrid(_str, _index)
        _index = _index + 1
      End If
    Next
    tb_currency.SelectTab(0)
  End Sub

  Private Sub AddTabPageAndGrid(ByVal ISOCode As String, ByVal Index As Int32)

    Dim _tab_page As TabPage
    Dim _grid As uc_grid

    _tab_page = New TabPage(ISOCode)
    _grid = New uc_grid()

    AddHandler _grid.CellDataChangedEvent, AddressOf CellDataChangeHandler

    _grid.Width = tb_currency.Width - 6
    _grid.Height = tb_currency.Height
    _grid.PanelRightVisible = False

    _tab_page.Controls.Add(_grid)

    tb_currency.TabPages.Insert(Index, _tab_page)
    Call GUI_StyleSheetCurrenciesGrid(_grid)
    LoadCurrencies(_grid, ISOCode)

  End Sub

  Private Sub LoadCurrencies(ByRef Grid As uc_grid, ByVal ISOCode As String)
    Dim _softcount As CLASS_SOFTCOUNT
    Dim _rows As DataRow()
    Dim _idx As Int32

    _softcount = DbReadObject
    _idx = 0
    _rows = _softcount.TableDenominations().Select("ISO_CODE = '" & ISOCode & "'")

    For Each _row As DataRow In _rows
      With Grid
        .AddRow()
        .Cell(_idx, GRID_CURRENCIES_COLUMN_ISOCODE).Value() = ISOCode
        .Cell(_idx, GRID_CURRENCIES_COLUMN_CURRENCY).Value() = _row(1).ToString()
        .Cell(_idx, GRID_CURRENCIES_COLUMN_CURRENCY_WITH_SIMBOL).Value = GUI_FormatCurrency(_row(1).ToString())
        .Cell(_idx, GRID_CURRENCIES_COLUMN_COUNT_OF_BILLS).Value = 0
        .Cell(_idx, GRID_CURRENCIES_COLUMN_AMOUNT).Value = 0
        .Cell(_idx, GRID_CURRENCIES_COLUMN_AMOUNT_WITH_SIMBOL).Value = GUI_FormatCurrency(0)
      End With

      _idx = _idx + 1
    Next

  End Sub

  Private Sub GUI_StyleSheetTotalGrid()

    With Me.dg_total
      Call .Init(GRID_TOTAL_COLUMN_COUNT, GRID_TOTAL_HEADERS_COUNT, True)

      .Column(GRID_TOTAL_COLUMN_CURRENCY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2270)
      .Column(GRID_TOTAL_COLUMN_CURRENCY).Width = GRID_TOTAL_CURRENCY_WIDTH
      .Column(GRID_TOTAL_COLUMN_CURRENCY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      .Column(GRID_TOTAL_COLUMN_COUNT_OF_BILLS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2953)
      .Column(GRID_TOTAL_COLUMN_COUNT_OF_BILLS).Width = GRID_TOTAL_COUNT_OF_BILLS_WIDTH
      .Column(GRID_TOTAL_COLUMN_COUNT_OF_BILLS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_TOTAL_COLUMN_COUNT_OF_BILLS).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_TOTAL_COLUMN_COUNT_OF_BILLS).EditionControl.EntryField.IsReadOnly = False
      .Column(GRID_TOTAL_COLUMN_COUNT_OF_BILLS).EditionControl.EntryField.SetFilter(FORMAT_NUMBER, 20)
      .Column(GRID_TOTAL_COLUMN_COUNT_OF_BILLS).Editable = True


      .Column(GRID_TOTAL_COLUMN_AMOUNT).Width = 0

      .Column(GRID_TOTAL_COLUMN_AMOUNT_WITH_SIMBOL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2936)
      .Column(GRID_TOTAL_COLUMN_AMOUNT_WITH_SIMBOL).Width = GRID_TOTAL_COUNT_OF_BILLS_WIDTH
      .Column(GRID_TOTAL_COLUMN_AMOUNT_WITH_SIMBOL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With
  End Sub

  Private Sub GUI_StyleSheetCurrenciesGrid(ByRef DataGrid As uc_grid)

    With DataGrid
      Call .Init(GRID_CURRENCIES_COLUMN_COUNT, GRID_CURRENCIES_HEADERS_COUNT, True)

      .Column(GRID_CURRENCIES_COLUMN_ISOCODE).Width = GRID_CURRENCY_ISOCODE_WIDTH

      .Column(GRID_CURRENCIES_COLUMN_CURRENCY).Width = GRID_CURRENCY_WIDTH

      .Column(GRID_CURRENCIES_COLUMN_CURRENCY_WITH_SIMBOL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2270)
      .Column(GRID_CURRENCIES_COLUMN_CURRENCY_WITH_SIMBOL).Width = GRID_CURRENCY_WIDTH_WITH_SIMBOL
      .Column(GRID_CURRENCIES_COLUMN_CURRENCY_WITH_SIMBOL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_CURRENCIES_COLUMN_COUNT_OF_BILLS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2953)
      .Column(GRID_CURRENCIES_COLUMN_COUNT_OF_BILLS).Width = GRID_CURRENCY_COUNT_OF_BILLS_WIDTH
      .Column(GRID_CURRENCIES_COLUMN_COUNT_OF_BILLS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_CURRENCIES_COLUMN_COUNT_OF_BILLS).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_CURRENCIES_COLUMN_COUNT_OF_BILLS).EditionControl.EntryField.IsReadOnly = False
      .Column(GRID_CURRENCIES_COLUMN_COUNT_OF_BILLS).EditionControl.EntryField.SetFilter(FORMAT_NUMBER, 20)
      .Column(GRID_CURRENCIES_COLUMN_COUNT_OF_BILLS).Editable = True

      .Column(GRID_CURRENCIES_COLUMN_AMOUNT_WITH_SIMBOL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2936)
      .Column(GRID_CURRENCIES_COLUMN_AMOUNT_WITH_SIMBOL).Width = GRID_CURRENCY_AMOUNT_WIDTH_WITH_SIMBOL
      .Column(GRID_CURRENCIES_COLUMN_AMOUNT_WITH_SIMBOL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_CURRENCIES_COLUMN_AMOUNT).Width = GRID_CURRENCY_AMOUNT_WIDTH

    End With
  End Sub

  Private Sub GUI_StyleSheetTicketGrid()

    With Me.dg_inserted_tickets
      Call .Init(GRID_TICKET_COLUMN_COUNT, GRID_TICKET_HEADERS_COUNT, True)

      .Column(GRID_TICKET_COLUMN_TICKET_DENOMINATION).Width = 0

      .Column(GRID_TICKET_COLUMN_TICKET_ID).Width = 0
      .Column(GRID_TICKET_VALIDATION_COLUMN_NUMBER_ORIGINAL_FORMAT).Width = 0

      ' Ticket number
      .Column(GRID_TICKET_COLUMN_VALIDATION_NUMBER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2269)
      .Column(GRID_TICKET_COLUMN_VALIDATION_NUMBER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2259)
      .Column(GRID_TICKET_COLUMN_VALIDATION_NUMBER).Width = GRID_TICKET_WIDTH_VALIDATION_NUMBER
      .Column(GRID_TICKET_COLUMN_VALIDATION_NUMBER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Ticket value
      .Column(GRID_TICKET_COLUMN_VALUE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2269)
      .Column(GRID_TICKET_COLUMN_VALUE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2260)
      .Column(GRID_TICKET_COLUMN_VALUE).Width = GRID_TICKET_WIDTH_VALUE
      .Column(GRID_TICKET_COLUMN_VALUE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With
  End Sub

  Private Sub CalculateCountAndAmount(ByVal Grid As uc_grid, ByRef Count As Int32, ByRef Amount As Decimal)

    Dim _current_count As Int32

    For _idx As Int32 = 0 To Grid.NumRows - 1
      _current_count = CType(Grid.Cell(_idx, GRID_CURRENCIES_COLUMN_COUNT_OF_BILLS).Value, Int32)
      Count = Count + _current_count
      Amount = Amount + (_current_count * CType(Grid.Cell(_idx, GRID_CURRENCIES_COLUMN_CURRENCY).Value, Decimal))
    Next

  End Sub

  Private Sub UpdateCurrencyRowOfGridTotals(ByVal ISOCode As String, ByVal Count As Int32, ByVal Amount As Decimal)

    For _idx As Int32 = 0 To dg_total.NumRows - 2
      If dg_total.Cell(_idx, GRID_TOTAL_COLUMN_CURRENCY).Value = ISOCode Then
        dg_total.Cell(_idx, GRID_TOTAL_COLUMN_COUNT_OF_BILLS).Value = Count
        dg_total.Cell(_idx, GRID_TOTAL_COLUMN_AMOUNT_WITH_SIMBOL).Value = GUI_FormatCurrency(Amount)
        dg_total.Cell(_idx, GRID_TOTAL_COLUMN_AMOUNT).Value = Amount
      End If
    Next

  End Sub

  Private Sub UpdateTotalRowOfGridTotal()

    Dim _count As Int32
    Dim _amount As Decimal

    _count = 0
    _amount = 0

    For _idx As Int32 = 0 To dg_total.NumRows - 2
      _count = _count + dg_total.Cell(_idx, GRID_TOTAL_COLUMN_COUNT_OF_BILLS).Value
      _amount = _amount + dg_total.Cell(_idx, GRID_TOTAL_COLUMN_AMOUNT).Value
    Next

    dg_total.Cell(dg_total.NumRows - 1, GRID_TOTAL_COLUMN_COUNT_OF_BILLS).Value = _count
    dg_total.Cell(dg_total.NumRows - 1, GRID_TOTAL_COLUMN_AMOUNT).Value = _amount
    dg_total.Cell(dg_total.NumRows - 1, GRID_TOTAL_COLUMN_AMOUNT_WITH_SIMBOL).Value = GUI_FormatCurrency(_amount)

  End Sub

  Private Sub UpdateTicketRowOfGridTotals(ByVal TicketRowName As String, ByVal TicketValue As Decimal)

    Dim _count_of_tickets As Int32
    Dim _amount As Decimal

    _count_of_tickets = 0
    _amount = 0

    For _idx As Int32 = 0 To dg_total.NumRows - 2
      If dg_total.Cell(_idx, GRID_TOTAL_COLUMN_CURRENCY).Value = TicketRowName Then
        _count_of_tickets = dg_total.Cell(_idx, GRID_TOTAL_COLUMN_COUNT_OF_BILLS).Value
        _amount = dg_total.Cell(_idx, GRID_TOTAL_COLUMN_COUNT_OF_BILLS).Value

        dg_total.Cell(_idx, GRID_TOTAL_COLUMN_COUNT_OF_BILLS).Value = _count_of_tickets + 1
        dg_total.Cell(_idx, GRID_TOTAL_COLUMN_AMOUNT).Value = _amount + TicketValue
        dg_total.Cell(_idx, GRID_TOTAL_COLUMN_AMOUNT_WITH_SIMBOL).Value = GUI_FormatCurrency(_amount + TicketValue)
      End If
    Next

  End Sub

  Private Function CreateDatatableFromTotal() As DataTable
    Dim _dt As DataTable
    Dim _row As DataRow

    _dt = New DataTable()

    _dt.Columns.Add("Denominations", Type.GetType("System.String"))
    _dt.Columns.Add("Count", Type.GetType("System.Int32"))
    _dt.Columns.Add("Amount", Type.GetType("System.Decimal"))

    For _idx As Int32 = 0 To dg_total.NumRows - 1
      _row = _dt.NewRow()
      _row("Denominations") = dg_total.Cell(_idx, GRID_TOTAL_COLUMN_CURRENCY).Value
      _row("Count") = dg_total.Cell(_idx, GRID_TOTAL_COLUMN_COUNT_OF_BILLS).Value
      _row("Amount") = dg_total.Cell(_idx, GRID_TOTAL_COLUMN_AMOUNT).Value
      _dt.Rows.Add(_row)
    Next

    Return _dt
  End Function

  Private Function CreateDatatableFromTickets(ByVal Grid As uc_grid) As DataTable

    Dim _dt As DataTable
    Dim _row As DataRow

    _dt = New DataTable()

    _dt.Columns.Add("TicketId", Type.GetType("System.Int64"))

    For _idx As Int32 = 0 To Grid.NumRows - 1
      _row = _dt.NewRow()

      _row("TicketId") = Grid.Cell(_idx, GRID_TICKET_COLUMN_TICKET_ID).Value

      _dt.Rows.Add(_row)
    Next

    Return _dt
  End Function

  Private Function CreateDatatableFromCurrency(ByVal Grid As uc_grid) As DataTable

    Dim _dt As DataTable
    Dim _row As DataRow

    _dt = New DataTable()
    _dt.Columns.Add("Deniminations", Type.GetType("System.Decimal"))
    _dt.Columns.Add("Count", Type.GetType("System.Int32"))
    _dt.Columns.Add("Amount", Type.GetType("System.Decimal"))

    For _idx As Int32 = 0 To Grid.NumRows - 1
      _row = _dt.NewRow()
      _row(0) = Grid.Cell(_idx, GRID_CURRENCIES_COLUMN_CURRENCY).Value
      _row(1) = Grid.Cell(_idx, GRID_CURRENCIES_COLUMN_COUNT_OF_BILLS).Value
      _row(2) = Grid.Cell(_idx, GRID_CURRENCIES_COLUMN_AMOUNT).Value
      _dt.Rows.Add(_row)
    Next

    Return _dt
  End Function

  Private Function CreateDataSet() As DataSet
    Dim _ds As DataSet
    Dim _dt As DataTable
    Dim _grid As uc_grid

    _ds = New DataSet("Softcounts")

    _dt = CreateDatatableFromTotal()
    _ds.Tables.Add(_dt)

    For _idx As Int32 = 0 To tb_currency.TabPages.Count - 1 'hay que escapar que el grid no sea el de tickets
      _grid = GetGridFromTabPage(Me.tb_currency.TabPages(_idx))

      If _grid.NumRows > 0 AndAlso _grid.Cell(0, 0).Value = Cage.TICKETS_CODE.ToString() Then
        _dt = CreateDatatableFromTickets(_grid)
      Else
        _dt = CreateDatatableFromCurrency(_grid)
      End If
      _ds.Tables.Add(_dt)
    Next



    Return _ds
  End Function

  Private Function GetGridFromTabPage(ByVal Tab As TabPage) As uc_grid
    Dim _datagrid As uc_grid

    _datagrid = Nothing

    For Each _control As Control In Tab.Controls
      If TypeOf _control Is uc_grid Then
        _datagrid = _control
        Exit For
      End If
    Next

    Return _datagrid
  End Function

  Private Sub SetTicketToGrid(ByVal Row As DataRow, ByVal ValidationNumber As String)

    Dim _row As Int32
    Dim _ticket_value As Decimal

    dg_inserted_tickets.AddRow()
    _row = dg_inserted_tickets.NumRows - 1
    _ticket_value = CType(Row(SQL_TICKET_VALUE), Decimal)

    dg_inserted_tickets.Cell(_row, GRID_TICKET_COLUMN_TICKET_DENOMINATION).Value = Cage.TICKETS_CODE
    dg_inserted_tickets.Cell(_row, GRID_TICKET_COLUMN_TICKET_ID).Value = Row(SQL_TICKET_ID)
    dg_inserted_tickets.Cell(_row, GRID_TICKET_VALIDATION_COLUMN_NUMBER_ORIGINAL_FORMAT).Value = ValidationNumber
    dg_inserted_tickets.Cell(_row, GRID_TICKET_COLUMN_VALIDATION_NUMBER).Value = ValidationNumberManager.FormatValidationNumber(ValidationNumber, 1, False)
    dg_inserted_tickets.Cell(_row, GRID_TICKET_COLUMN_VALUE).Value = GUI_FormatCurrency(_ticket_value)

    ef_tickets.TextValue = ""

    Call UpdateTicketRowOfGridTotals(tab_tickets.Text, _ticket_value)
    Call UpdateTotalRowOfGridTotal()

  End Sub

  Private Sub CleanSoftCountGrids()

    Call InitTotalGrid()
    Call ClearCurrenciesGrids()
    Call ClearTicketGrid()

  End Sub

  Private Sub ResetSoftcount()

    Me.ef_stacker_id.TextValue = ""
    Me.ef_floor_id.TextValue = ""
    Me.ef_insertion_date.TextValue = ""
    Me.ef_extraction_date.TextValue = ""
    Me.ef_terminal.TextValue = ""
    Me.ef_stacker_id.Enabled = True
    Me.ef_floor_id.Enabled = True

    DbObjectId = Nothing

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    Call InitTotalGrid()
    Call ClearCurrenciesGrids()
    Call ClearTicketGrid()

  End Sub

  Private Sub ClearCurrenciesGrids()

    Dim _softcount As CLASS_SOFTCOUNT
    Dim _grid As uc_grid

    _softcount = DbReadObject

    For Each _tab As TabPage In tb_currency.TabPages
      _grid = GetGridFromTabPage(_tab)
      ' this IF answer if the current grid is contained in a curency tab. the first column has the ISOCODE
      If _grid IsNot Nothing AndAlso _grid.NumRows > 0 _
                             AndAlso _softcount.ListOfCurrencies.Contains(_grid.Cell(0, 0).Value) Then
        For _idx As Int32 = 0 To _grid.NumRows - 1
          With _grid
            .Cell(_idx, GRID_CURRENCIES_COLUMN_COUNT_OF_BILLS).Value = 0
            .Cell(_idx, GRID_CURRENCIES_COLUMN_AMOUNT).Value = 0
            .Cell(_idx, GRID_CURRENCIES_COLUMN_AMOUNT_WITH_SIMBOL).Value = GUI_FormatCurrency(0)
          End With
        Next
      End If
    Next

  End Sub

  Private Sub ClearTicketGrid()
    dg_inserted_tickets.Clear()
  End Sub

  Private Sub InitializeScanner()
    Dim _note_counter_info As NoteCounterInfo
    Dim _application_id As Int32

    m_first_error = True
    m_second_error = True
    If CommonScannerFunctions.GetApplicationId(_application_id) Then
      Try
        If m_scanner_manager Is Nothing Then
          _note_counter_info = CommonScannerFunctions.GetScannerInfo(_application_id)

          If Not _note_counter_info Is Nothing Then
            m_scanner_manager = New ScannerManager(_note_counter_info)
            AddHandler m_scanner_manager.NoteCountReceivedEvent, AddressOf CatchMessageFromScanner

            m_list_of_event = New List(Of String)
            m_frm_counter_event = New frm_note_counter_events(m_list_of_event)
            If m_scanner_manager.InitializeScanner() Then

              lb_error_messages.ForeColor = Color.Blue
              lb_error_messages.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5196)
              lb_note_counter_status.BackColor = Color.Green
              'm_note_counter_flag = False
              'm_lbl_counter_status_update = 0
              lbl_counter.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5448)
              'tmr_note_counter_working.Start()
              m_scanner_initialized = True
            Else
              lb_error_messages.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5189)
              lbl_counter.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5450)
              'm_lbl_counter_status_update = 7
            End If
          Else
            lb_error_messages.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5189)
            lbl_counter.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5450)
          End If

        End If
      Catch _ex As Exception
      End Try
    End If
  End Sub

  Private Sub CatchMessageFromScanner(ByVal MessageReceived As IScanned)

    Try
      Select Case MessageReceived.GetScannedType()
        Case ScannedType.Currencies
          SetScreenScannerNotes(CType(MessageReceived, NotesReceived))
        Case ScannedType.Tickets
          If Me.tab_tickets.InvokeRequired AndAlso tb_currency.Contains(tab_tickets) Then
            'm_error_received = False
            Me.tab_tickets.Invoke(New InvokeTicketGrid(AddressOf AddTicket), New Object() {CType(MessageReceived, TicketReceived).ValidationNumber.ToString(), 0D, True, 0})
          End If
        Case ScannedType.Error
        Case ScannedType.Information
          If lb_error_messages.InvokeRequired() Then
            ' m_error_received = True
            Me.lb_error_messages.Invoke(New InvokeError(AddressOf CounterErrorReceived), New Object() {MessageReceived})
          End If

      End Select
    Catch ex As InvalidOperationException
      Log.Error(ex.Message())
    End Try

  End Sub

  Private Sub SetErrorToLabel(ByVal ErrorText As String)

    m_frm_counter_event.SetEventToGrid(ErrorText)
    m_list_of_event.Add(ErrorText)

    If m_first_error Then
      lb_error_messages.Text = ErrorText
      m_first_error = False
    ElseIf m_second_error Then
      lb_error_messages.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5188)
      m_second_error = False
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = True

    End If

  End Sub

  Private Sub SetScreenScannerNotes(ByVal NotesReceived As NotesReceived)
    Dim _currency As NoteReceived

    If Me.tb_currency.InvokeRequired Then
      For Each _currency In NotesReceived.Notes
        Me.tb_currency.Invoke(New InvokeCurrencyGrid(AddressOf AddCurrencyToCollection), New Object() {_currency, NotesReceived.IsoCode})
      Next
    End If
  End Sub

  Private Sub AddCurrencyToCollection(ByVal Currency As NoteReceived, ByVal IsoCode As String)

    Dim _datagrid As uc_grid
    Dim _total_count As Int32
    Dim _total_amount As Decimal
    Dim _current_count As Int32

    _total_amount = 0
    _total_count = 0

    For Each _tab As TabPage In tb_currency.TabPages
      _datagrid = GetGridFromTabPage(_tab)

      ' this IF answer if the current grid is contained in a curency tab. the first column has the ISOCODE
      If _datagrid IsNot Nothing AndAlso _datagrid.NumRows > 0 AndAlso IsoCode = _datagrid.Cell(0, 0).Value Then
        For _idx As Int32 = 0 To _datagrid.NumRows - 1
          If _datagrid.Cell(_idx, GRID_CURRENCIES_COLUMN_CURRENCY).Value = Currency.Denomination Then
            _current_count = CType(_datagrid.Cell(_idx, GRID_CURRENCIES_COLUMN_COUNT_OF_BILLS).Value, Int32)
            _datagrid.Cell(_idx, GRID_CURRENCIES_COLUMN_COUNT_OF_BILLS).Value = Currency.Quantity + _current_count  ' add the new count of bill + colleted previusly
            _datagrid.Cell(_idx, GRID_CURRENCIES_COLUMN_AMOUNT_WITH_SIMBOL).Value = GUI_FormatCurrency((Currency.Quantity + _current_count) * Currency.Denomination)
            _datagrid.Cell(_idx, GRID_CURRENCIES_COLUMN_AMOUNT).Value = Currency.Quantity * Currency.Denomination

            Call CalculateCountAndAmount(_datagrid, _total_count, _total_amount) 'calculate the count of bills and the amount for the current grid
            Call UpdateCurrencyRowOfGridTotals(IsoCode, _total_count, _total_amount)
            Call UpdateTotalRowOfGridTotal()

          End If
        Next
        Exit For

      Else
        Call SetErrorToLabel(GLB_NLS_GUI_PLAYER_TRACKING.GetString(3031, GUI_FormatCurrency(Currency.Denomination)))

      End If
    Next

  End Sub

  Private Sub CounterErrorReceived(ByVal MessageReceived As IScanned)

    Dim _text As String
    Dim _denom() As String

    Select Case MessageReceived.GetScannedType()
      Case ScannedType.Error
        Dim _error As ErrorReceived
        _error = CType(MessageReceived, ErrorReceived)
        _text = GetCounterErrorDescription(_error.AlarmCode)
        lbl_counter.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5454)
        Call SetErrorToLabel(_text)
      Case ScannedType.Information

        Dim _info As InformationReceived
        _info = CType(MessageReceived, InformationReceived)
        Select Case _info.InformationType
          Case InformationType.NOTECOUNTER_INITIALIZED
            Call SetErrorToLabel(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5196))

          Case InformationType.UNDEFINED_CURRENCY
            _denom = _info.Info.Split("-")
            Call SetErrorToLabel(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5496, _denom(1), _denom(0)))

          Case InformationType.LABEL_RECEIVED
            Call SetErrorToLabel(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5537))
        End Select


    End Select
  End Sub

  Private Sub AddTicket(ByVal ValidationNumber As String, Optional ByVal TicketValue As Decimal = 0, Optional ByVal ReadByScanner As Boolean = False, Optional ByVal TicketId As Int64 = 0)

    Dim _tickets As DataTable
    Dim _softcount As CLASS_SOFTCOUNT
    Dim _error_description As String

    _tickets = New DataTable()
    _softcount = DbReadObject

    _tickets = _softcount.GetTicketData(ValidationNumber)

    Select Case _tickets.Rows.Count

      Case 0
        If ReadByScanner Then
          _error_description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5152, ValidationNumber)
          Call SetErrorToLabel(_error_description)
          Return
        Else
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2280), _
                                                    mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
                                                    ENUM_MB_BTN.MB_BTN_OK, , ValidationNumber)
          ef_tickets.TextValue = ""
          Return
        End If

      Case 1
        Call SetTicketToGrid(_tickets.Rows(0), ValidationNumber)

      Case Is > 1

    End Select
  End Sub

  Private Sub SetStatusOkToLabel(ByVal StatusOkTextText As String)

    lb_note_counter_status.BackColor = Color.Green
    m_first_error = True
    m_second_error = True

  End Sub

  Private Function CkeckCountOfTickets() As Boolean

    Dim _count_of_tickets_in_grid As Int32
    Dim _ticket_iso_code As String
    Dim _correct_count_of_ticket As Boolean

    _correct_count_of_ticket = True

    _ticket_iso_code = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4600)

    _count_of_tickets_in_grid = dg_collection_tickets.NumRows

    If _count_of_tickets_in_grid > 0 Then
      For _idx As Int32 = 0 To dg_total.NumRows - 2
        If dg_total.Cell(_idx, GRID_TOTAL_COLUMN_CURRENCY).Value = _ticket_iso_code Then
          _correct_count_of_ticket = (_count_of_tickets_in_grid = dg_total.Cell(_idx, GRID_TOTAL_COLUMN_COUNT_OF_BILLS).Value)
        End If
      Next
    End If

    Return _correct_count_of_ticket
  End Function

#End Region

#Region "Events"
  Private Sub CellDataChangeHandler(ByVal Row As Integer, ByVal Column As Integer)

    Dim _datagrid As uc_grid
    Dim _count As Int32
    Dim _amount As Decimal
    Dim _total_count As Int32
    Dim _total_amount As Decimal
    Dim _iso_code As String

    _count = 0
    _total_amount = 0
    _total_count = 0
    _amount = 0
    _iso_code = ""

    _datagrid = Me.tb_currency.SelectedTab.Controls.Item(0)
    _iso_code = tb_currency.SelectedTab().Text

    If _datagrid.Cell(Row, Column) IsNot Nothing Then
      _count = CType(_datagrid.Cell(Row, Column).Value, Int64)
    Else
      _datagrid.Cell(Row, Column).Value = 0
      _count = 0
    End If

    _datagrid.Cell(Row, GRID_CURRENCIES_COLUMN_AMOUNT_WITH_SIMBOL).Value = GUI_FormatCurrency(_count * CType(_datagrid.Cell(Row, GRID_CURRENCIES_COLUMN_CURRENCY).Value, Decimal))
    _datagrid.Cell(Row, GRID_CURRENCIES_COLUMN_AMOUNT).Value = _count * CType(_datagrid.Cell(Row, GRID_CURRENCIES_COLUMN_CURRENCY).Value, Decimal)

    Call CalculateCountAndAmount(_datagrid, _total_count, _total_amount) 'calculate the count of bills and the amount for the current grid
    Call UpdateCurrencyRowOfGridTotals(_iso_code, _total_count, _total_amount)
    Call UpdateTotalRowOfGridTotal()

  End Sub

  Private Function GetCounterErrorDescription(ByVal ErrorCode As AlarmCode) As String
    Dim _text As String

    Select Case ErrorCode
      Case AlarmCode.Reader_Chain_Detected
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5174)
      Case AlarmCode.Reader_Changing_Operating_Mode
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5178)
      Case AlarmCode.Reader_Combined_Denomination_Change_Strap_Limit
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5176)
      Case AlarmCode.Reader_Comunication_Error
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5177)
      Case AlarmCode.Reader_Configuration
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5175)
      Case AlarmCode.Reader_Denomination_Changed
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5165)
      Case AlarmCode.Reader_Double_Detected
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5169)
      Case AlarmCode.Reader_Facing_Error
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5167)
      Case AlarmCode.Reader_Jam_Detected
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5171)
      Case AlarmCode.Reader_No_Call
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5173)
      Case AlarmCode.Reader_Orientation_Error
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5168)
      Case AlarmCode.Reader_Stacker_Full
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5172)
      Case AlarmCode.Reader_Stranger_Detected
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5164)
      Case AlarmCode.Reader_Strap_Limit
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5166)
      Case AlarmCode.Reader_Suspect_Document
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5170)
      Case AlarmCode.Reader_Ticket_Already_Collected
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5153)
      Case AlarmCode.Reader_Ticket_Sent_Two_Times
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5155)
      Case AlarmCode.Reader_Ticket_With_Duplicated_Number
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5154)
      Case AlarmCode.Reder_Invalid_Ticket
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5152)
      Case AlarmCode.Reader_Undefined_Elements
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5491)
      Case AlarmCode.Reader_No_Count
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5518)
      Case Else
        _text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5198)

    End Select

    Return _text
  End Function

  Private Sub btn_insert_ticket_ClickEvent() Handles btn_insert_ticket.ClickEvent
    Call Me.AddTicket(ef_tickets.Value)
  End Sub

  Private Sub dg_total_BeforeStartEditionEvent(ByVal Row As System.Int32, ByVal Column As System.Int32, ByRef EditionCanceled As System.Boolean) Handles dg_total.BeforeStartEditionEvent

    Dim _ticket_iso_code As String

    _ticket_iso_code = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4600)

    EditionCanceled = True

    If dg_total.Cell(Row, GRID_TOTAL_COLUMN_CURRENCY).Value = _ticket_iso_code Then
      EditionCanceled = False
    End If

  End Sub

#End Region


End Class