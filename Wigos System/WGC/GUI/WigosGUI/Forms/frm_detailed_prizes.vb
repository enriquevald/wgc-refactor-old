'-------------------------------------------------------------------
' Copyright � 2011 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_detail_prizes.vb
' DESCRIPTION:   Details of prizes generated or cashed
' AUTHOR:        Marcos Piedra Osuna
' CREATION DATE: 12-MAR-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 12-MAR-2012  MPO    Initial version
' 30-MAR-2012  JCM    The interval between from_date and to_date must be less than 31 days
' 17-APR-2012  JCM    Convert "Multigame" literal string in parameter
' 11-JUL-2014  XIT    Added Multilanguage Support
' 03-SEP-2014  LEM    Added functionality to show terminal location data.
' -------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Data.SqlClient
Imports WSI.Common

Public Class frm_detailed_prizes
  Inherits frm_base_sel

#Region " Members "

  Private m_num_won As Integer
  Private m_sum_won As Decimal

  Private m_total_num_won As Integer
  Private m_total_sum_won As Decimal
  Private m_total_num_paid As Integer
  Private m_total_sum_paid As Decimal

  Private m_current_acc_id As Integer
  Private m_current_num_won As Integer

  Private m_count_account As Integer

  Private m_date_from As String
  Private m_date_to As String

  Private m_account_id As String
  Private m_account_holder_name As String
  Private m_level As String
  Private m_subtotal As Boolean

  Private m_terminal_report_type As ReportType = ReportType.Provider
  Private m_refresh_grid As Boolean = False

#End Region

#Region " CONSTANT "

  Private TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(m_terminal_report_type)

  Private GRID_COLUMN_INDEX As Integer = 0
  Private GRID_COL_ACCOUNT_ID As Integer = 1
  Private GRID_COL_ACCOUNT_HOLDER_NAME As Integer = 2
  Private GRID_COL_LEVEL As Integer = 3
  Private GRID_INIT_TERMINAL_DATA As Integer = 4
  Private GRID_COL_GAME As Integer = TERMINAL_DATA_COLUMNS + 4
  Private GRID_COL_NUM_WON As Integer = TERMINAL_DATA_COLUMNS + 5
  Private GRID_COL_SUM_WON As Integer = TERMINAL_DATA_COLUMNS + 6
  Private GRID_COL_NUM_PAID As Integer = TERMINAL_DATA_COLUMNS + 7
  Private GRID_COL_SUM_PAID As Integer = TERMINAL_DATA_COLUMNS + 8

  Private Const SQL_COL_ACCOUNT_ID As Integer = 0
  Private Const SQL_COL_ACCOUNT_HOLDER_NAME As Integer = 1
  Private Const SQL_COL_LEVEL As Integer = 2
  Private Const SQL_COL_PROVIDER As Integer = 3
  Private Const SQL_COL_TERMINAL As Integer = 4
  Private Const SQL_COL_GAME As Integer = 5
  Private Const SQL_COL_NUM_WON As Integer = 6
  Private Const SQL_COL_SUM_WON As Integer = 7
  Private Const SQL_COL_NUM_PAID As Integer = 8
  Private Const SQL_COL_SUM_PAID As Integer = 9
  Private Const SQL_COL_CONTAIN_PAID As Integer = 10
  Private Const SQL_COL_CONTAIN_WON As Integer = 11
  Private Const SQL_COL_TERMINAL_ID As Integer = 12

  Private GRID_COLUMNS As Integer = TERMINAL_DATA_COLUMNS + 9
  Private Const GRID_HEADER_ROWS As Integer = 2

  Private Const GRID_WIDTH_INDEX As Integer = 200
  Private Const GRID_WIDTH_AMOUNT As Integer = 1700
  Private Const GRID_WIDTH_NUMBER As Integer = 900
  Private Const GRID_WIDTH_LEVEL As Integer = 1500

  Private Const COUNTER_ACCOUNT As Integer = 1
  Private Const FORM_DB_MIN_VERSION As Short = 219
#End Region

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_DETAILED_PRIZES
    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Detailed prizes
    Me.Text = GLB_NLS_GUI_STATISTICS.GetString(489)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(3)

    ' Date
    Me.gb_date.Text = GLB_NLS_GUI_INVOICING.GetString(201)
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)

    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset  

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.dtp_from

  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted

  Protected Overrides Function GUI_FilterCheck() As Boolean
    ' Date selection 
    If Me.dtp_from.Value > Me.dtp_to.Value Then
      Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call Me.dtp_to.Focus()

      Return False
    End If

    ' The interval between from_date and to_date must be less than 31 days
    If Me.dtp_to.Value > Me.dtp_from.Value.AddDays(31) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(587), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call Me.dtp_to.Focus()

      Return False
    End If

    Return True
  End Function ' GUI_FilterCheck  

  ' PURPOSE: Build an SQL Command query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL Command query ready to send to the database

  Protected Overrides Function GUI_GetSqlCommand() As SqlCommand
    Dim _sql_command As SqlCommand

    _sql_command = New SqlCommand(SqlQuery.CommandText("DetailedPrizes"))
    _sql_command.Parameters.Add("@pInitDate", SqlDbType.DateTime).Value = dtp_from.Value
    _sql_command.Parameters.Add("@pFinDate", SqlDbType.DateTime).Value = dtp_to.Value
    _sql_command.Parameters.Add("@pMultiGame", SqlDbType.NVarChar).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(634)
    _sql_command.Parameters.Add("@pGamingTerminalTypeList", SqlDbType.NVarChar).Value = WSI.Common.Misc.GamingTerminalTypeListToString()
    _sql_command.Parameters.Add("@pLanguageId", SqlDbType.Int).Value = Resource.LanguageId

    Return _sql_command

  End Function

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _row_total_account As Boolean
    Dim _add_total As Boolean
    Dim _idx_row As Integer
    Dim _terminal_data As List(Of Object)

    _add_total = False
    _row_total_account = False
    m_subtotal = False

    Try

      m_account_id = DbRow.Value(SQL_COL_ACCOUNT_ID).ToString()
      m_account_holder_name = IsNull(DbRow.Value(SQL_COL_ACCOUNT_HOLDER_NAME), "").ToString()
      m_level = IsNull(DbRow.Value(SQL_COL_LEVEL), "").ToString()

      Me.Grid.Cell(RowIndex, GRID_COL_ACCOUNT_ID).Value = m_account_id
      Me.Grid.Cell(RowIndex, GRID_COL_ACCOUNT_HOLDER_NAME).Value = m_account_holder_name
      Me.Grid.Cell(RowIndex, GRID_COL_LEVEL).Value = m_level

      If DbRow.IsNull(SQL_COL_PROVIDER) Then

        _row_total_account = True

      Else
        If Not DbRow.IsNull(SQL_COL_TERMINAL_ID) Then
          _terminal_data = TerminalReport.GetReportDataList(DbRow.Value(SQL_COL_TERMINAL_ID), m_terminal_report_type)
          For _idx As Int32 = 0 To _terminal_data.Count - 1
            If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value Then
              Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
            End If
          Next
        End If

        Me.Grid.Cell(RowIndex, GRID_COL_GAME).Value = DbRow.Value(SQL_COL_GAME)
        Me.Grid.Cell(RowIndex, GRID_COL_NUM_WON).Value = GUI_FormatNumber(DbRow.Value(SQL_COL_NUM_WON), 0)
        Me.Grid.Cell(RowIndex, GRID_COL_SUM_WON).Value = GUI_FormatCurrency(DbRow.Value(SQL_COL_SUM_WON), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        m_num_won += Me.Grid.Cell(RowIndex, GRID_COL_NUM_WON).Value
        m_sum_won += Me.Grid.Cell(RowIndex, GRID_COL_SUM_WON).Value
        m_total_num_won += Me.Grid.Cell(RowIndex, GRID_COL_NUM_WON).Value
        m_total_sum_won += Me.Grid.Cell(RowIndex, GRID_COL_SUM_WON).Value

        m_current_num_won += 1

        If DbRow.Value(SQL_COL_CONTAIN_PAID) = 0 Then
          If DbRow.Value(SQL_COL_CONTAIN_WON) = 1 Or DbRow.Value(SQL_COL_CONTAIN_WON) = m_current_num_won Then
            _row_total_account = True
          End If
          _add_total = True
        End If

      End If

      If _row_total_account Then

        _idx_row = RowIndex

        If _add_total Then
          Me.Grid.AddRow()
          _idx_row = Me.Grid.NumRows - 1
          Me.Grid.Cell(_idx_row, GRID_COL_ACCOUNT_ID).Value = m_account_id
          Me.Grid.Cell(_idx_row, GRID_COL_ACCOUNT_HOLDER_NAME).Value = m_account_holder_name
          Me.Grid.Cell(_idx_row, GRID_COL_LEVEL).Value = m_level
        End If

        If m_num_won > 0 Then Me.Grid.Cell(_idx_row, GRID_COL_NUM_WON).Value = GUI_FormatNumber(m_num_won, 0)
        If m_sum_won > 0 Then Me.Grid.Cell(_idx_row, GRID_COL_SUM_WON).Value = GUI_FormatCurrency(m_sum_won, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        If Not DbRow.IsNull(SQL_COL_NUM_PAID) AndAlso DbRow.Value(SQL_COL_NUM_PAID) > 0 Then
          Me.Grid.Cell(_idx_row, GRID_COL_NUM_PAID).Value = GUI_FormatNumber(DbRow.Value(SQL_COL_NUM_PAID), 0)
          m_total_num_paid += Me.Grid.Cell(_idx_row, GRID_COL_NUM_PAID).Value
        End If
        If Not DbRow.IsNull(SQL_COL_SUM_PAID) AndAlso DbRow.Value(SQL_COL_SUM_PAID) > 0 Then
          Me.Grid.Cell(_idx_row, GRID_COL_SUM_PAID).Value = GUI_FormatCurrency(DbRow.Value(SQL_COL_SUM_PAID), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
          m_total_sum_paid += Me.Grid.Cell(_idx_row, GRID_COL_SUM_PAID).Value
        End If
        Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

        m_subtotal = True

        m_num_won = 0
        m_sum_won = 0
        m_current_num_won = 0
        m_count_account += 1

      End If

      m_current_acc_id = Me.Grid.Cell(RowIndex, GRID_COL_ACCOUNT_ID).Value

    Catch ex As Exception
      Dim a As Integer = 1
    End Try


    Return True

  End Function

  ' PURPOSE: Get the defined Query Type
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_QUERY_TYPE
  '

  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_COMMAND
  End Function ' GUI_GetQueryType

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_BeforeFirstRow()

    m_current_acc_id = -1

    m_num_won = 0
    m_sum_won = 0

    m_total_num_won = 0
    m_total_sum_won = 0
    m_total_num_paid = 0
    m_total_sum_paid = 0

    m_count_account = 0

    If m_refresh_grid Then
      Call GUI_StyleSheet()
    End If

    m_refresh_grid = False

  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_AfterLastRow()

    Dim _idx_row As Integer = Me.Grid.NumRows - 1

    If (Me.Grid.NumRows > 0) Then
      GUI_RowSelectedEvent(0)

      If Not m_subtotal Then
        Me.Grid.AddRow()
        _idx_row = Me.Grid.NumRows - 1
        Me.Grid.Cell(_idx_row, GRID_COL_ACCOUNT_ID).Value = m_account_id
        Me.Grid.Cell(_idx_row, GRID_COL_ACCOUNT_HOLDER_NAME).Value = m_account_holder_name
        Me.Grid.Cell(_idx_row, GRID_COL_LEVEL).Value = m_level

        If m_num_won > 0 Then Me.Grid.Cell(_idx_row, GRID_COL_NUM_WON).Value = GUI_FormatNumber(m_num_won, 0)
        If m_sum_won > 0 Then Me.Grid.Cell(_idx_row, GRID_COL_SUM_WON).Value = GUI_FormatCurrency(m_sum_won, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

      End If

    Else
      GUI_RowSelectedEvent(-1)
    End If

    Me.Grid.AddRow()
    _idx_row = Me.Grid.NumRows - 1
    Me.Grid.Cell(_idx_row, 2).Value = GLB_NLS_GUI_INVOICING.GetString(205)
    Me.Grid.Cell(_idx_row, GRID_COL_NUM_WON).Value = GUI_FormatNumber(m_total_num_won, 0)
    Me.Grid.Cell(_idx_row, GRID_COL_SUM_WON).Value = GUI_FormatCurrency(m_total_sum_won, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(_idx_row, GRID_COL_NUM_PAID).Value = GUI_FormatNumber(m_total_num_paid, 0)
    Me.Grid.Cell(_idx_row, GRID_COL_SUM_PAID).Value = GUI_FormatCurrency(m_total_sum_paid, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

    Me.Grid.Counter(COUNTER_ACCOUNT).Value = m_count_account

  End Sub ' GUI_AfterLastRow

#Region " GUI Reports "

  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    With Me.Grid

    End With

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)

    PrintData.FilterValueWidth(1) = 3000

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportUpdateFilters()

    ' Date
    m_date_from = GUI_FormatDate(Me.dtp_from.Value, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    m_date_to = GUI_FormatDate(Me.dtp_to.Value, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region

#Region " Private Function "

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub GUI_StyleSheet()
    Dim _terminal_columns As List(Of ColumnSettings)

    Call GridColumnReIndex()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .Sortable = False

      .Counter(COUNTER_ACCOUNT).Visible = True
      .Counter(COUNTER_ACCOUNT).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Account
      ' AccountID
      .Column(GRID_COL_ACCOUNT_ID).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COL_ACCOUNT_ID).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COL_ACCOUNT_ID).Width = 1150
      .Column(GRID_COL_ACCOUNT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER

      ' Account
      ' Holder name
      .Column(GRID_COL_ACCOUNT_HOLDER_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COL_ACCOUNT_HOLDER_NAME).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(235)
      .Column(GRID_COL_ACCOUNT_HOLDER_NAME).Width = 3200
      .Column(GRID_COL_ACCOUNT_HOLDER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT_CENTER

      ' Account
      ' Level
      .Column(GRID_COL_LEVEL).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COL_LEVEL).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(381)
      .Column(GRID_COL_LEVEL).Width = GRID_WIDTH_LEVEL
      .Column(GRID_COL_LEVEL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Terminal Report
      _terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)
      For _idx As Int32 = 0 To _terminal_columns.Count - 1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(300)
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = _terminal_columns(_idx).Header1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = _terminal_columns(_idx).Width
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = _terminal_columns(_idx).Alignment
      Next

      ' Terminal
      ' Game
      .Column(GRID_COL_GAME).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(300)
      .Column(GRID_COL_GAME).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(215)
      .Column(GRID_COL_GAME).Width = 2500
      .Column(GRID_COL_GAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Prize Generated
      ' Number
      .Column(GRID_COL_NUM_WON).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(483)
      .Column(GRID_COL_NUM_WON).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(485)
      .Column(GRID_COL_NUM_WON).Width = GRID_WIDTH_NUMBER
      .Column(GRID_COL_NUM_WON).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Prize Generated
      ' Total
      .Column(GRID_COL_SUM_WON).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(483)
      .Column(GRID_COL_SUM_WON).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(486)
      .Column(GRID_COL_SUM_WON).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COL_SUM_WON).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Prize Paid
      ' Number
      .Column(GRID_COL_NUM_PAID).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(484)
      .Column(GRID_COL_NUM_PAID).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(485)
      .Column(GRID_COL_NUM_PAID).Width = GRID_WIDTH_NUMBER
      .Column(GRID_COL_NUM_PAID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Prize Paid
      ' Total
      .Column(GRID_COL_SUM_PAID).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(484)
      .Column(GRID_COL_SUM_PAID).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(486)
      .Column(GRID_COL_SUM_PAID).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COL_SUM_PAID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub SetDefaultValues()

    Dim _initial_time As Date

    _initial_time = WSI.Common.Misc.TodayOpening()

    ' Set at current month's first day 
    Me.dtp_from.Value = _initial_time.AddDays(-_initial_time.Day + 1)
    Me.dtp_from.Checked = True
    Me.dtp_to.Value = _initial_time.AddDays(1)
    Me.dtp_to.Checked = False

    Me.chk_terminal_location.Checked = False

  End Sub ' SetDefaultValues

  Private Function IsNull(ByVal Obj As Object, ByVal Value As String) As Object

    If IsDBNull(Obj) Then
      Return Value
    Else
      Return Obj
    End If

  End Function

  Private Sub GridColumnReIndex()

    TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(m_terminal_report_type)

    GRID_COLUMN_INDEX = 0
    GRID_COL_ACCOUNT_ID = 1
    GRID_COL_ACCOUNT_HOLDER_NAME = 2
    GRID_COL_LEVEL = 3
    GRID_INIT_TERMINAL_DATA = 4
    GRID_COL_GAME = TERMINAL_DATA_COLUMNS + 4
    GRID_COL_NUM_WON = TERMINAL_DATA_COLUMNS + 5
    GRID_COL_SUM_WON = TERMINAL_DATA_COLUMNS + 6
    GRID_COL_NUM_PAID = TERMINAL_DATA_COLUMNS + 7
    GRID_COL_SUM_PAID = TERMINAL_DATA_COLUMNS + 8

    GRID_COLUMNS = TERMINAL_DATA_COLUMNS + 9

  End Sub

#End Region

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent

    Me.Display(False)

  End Sub ' ShowForEdit

#End Region

#Region " Events "

  Private Sub chk_terminal_location_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminal_location.CheckedChanged
    If chk_terminal_location.Checked Then
      m_terminal_report_type = ReportType.Provider + ReportType.Location
    Else
      m_terminal_report_type = ReportType.Provider
    End If

    m_refresh_grid = True
  End Sub

#End Region

End Class