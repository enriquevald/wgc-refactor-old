'-------------------------------------------------------------------
' Copyright � 2011 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_tickets_report
' DESCRIPTION:   Tickets report
' AUTHOR:        Julio Andr�s
' CREATION DATE: 30-MAY-2011
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 30-MAY-2011  JAA    Initial version
' 18-JAN-2013  QMP    Company B Tax column now reads header name from GeneralParams
' 16-APR-2013  DMR    Changing cashier name for its alias
' 24-DEC-2013  JPJ    Hide B firm if it's not active
' 23-DEC-2014  JBC    Fixed Bug WIG-1892: Service Charge wrong hidden.
' 13-FEB-2015  JML & DHA Added cage concepts (Generic & Entry)
' 19-FEB-2015  AMF    Fixed Bug WIG-2090: Operation type filter
' 20-JUL-2015  DCS    Fixed Bug WIG-2593: Don't show properly cage concepts
' 20-JUL-2015  DCS    Adapt for development Pay card player to company B
' 10-NOV-2015  GMV    Fixed bug 6288: Num folio filter/columns isn't necessary
' 26-APR-2016  DLL    Fixed Bug 12352: Error searching
' 03-APR-2017  FGB    PBI 25737: Third TAX - Report columns
' 20-OCT-2017  RAB    Bug 30338:WIGOS-5198 [Ticket #8867] Reporte de Tickets de caja
' 22-FEB-2018  FGB    WIGOS-7946: [Ticket #12065] reporte ticket de caja
'                       When the report is grouped by and showing 'Sale Note' we added the column concept
' 23-MAY-2018  AGS    Bug 32759:WIGOS-12144 [Ticket #14453] Fallo � Reporte de Ticket de Caja - Reintegro - version V03.08.001
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Text
Imports WSI.Common

Public Class frm_tickets_report
  Inherits frm_base_sel

#Region " Constants "

  ' DB Columns
  ' Detailed SQL columns
  Private Const SQL_COLUMN_DETAIL_RECEIPT As Integer = 0
  Private Const SQL_COLUMN_DETAIL_FOLIO As Integer = 1
  Private Const SQL_COLUMN_DETAIL_DATETIME As Integer = 2
  Private Const SQL_COLUMN_DETAIL_AMOUNT As Integer = 3
  Private Const SQL_COLUMN_DETAIL_TAX As Integer = 4
  Private Const SQL_COLUMN_DETAIL_TOTAL As Integer = 5
  Private Const SQL_COLUMN_DETAIL_CONCEPT As Integer = 6
  Private Const SQL_COLUMN_DETAIL_CASHIER_USER As Integer = 7
  Private Const SQL_COLUMN_DETAIL_CASHIER_NAME As Integer = 8
  Private Const SQL_COLUMN_DETAIL_CANCELLED_FOLIO As Integer = 9

  Private Const SQL_COLUMN_DETAIL_CHARGE_RECEIPT As Integer = 0
  Private Const SQL_COLUMN_DETAIL_CHARGE_FOLIO As Integer = 1
  Private Const SQL_COLUMN_DETAIL_CHARGE_DATETIME As Integer = 2
  Private Const SQL_COLUMN_DETAIL_CHARGE_DEPOSIT As Integer = 3
  Private Const SQL_COLUMN_DETAIL_CHARGE_PROMOTION As Integer = 4
  Private Const SQL_COLUMN_DETAIL_CHARGE_TOTAL_DEPOSIT As Integer = 5
  Private Const SQL_COLUMN_DETAIL_CHARGE_ACCOUNT_ID As Integer = 6
  Private Const SQL_COLUMN_DETAIL_CHARGE_CLIENT_NAME As Integer = 7
  Private Const SQL_COLUMN_DETAIL_CHARGE_CASHIER_USER As Integer = 8
  Private Const SQL_COLUMN_DETAIL_CHARGE_CASHIER_NAME As Integer = 9

  Private Const SQL_COLUMN_DETAIL_WITHDRAW_RECEIPT As Integer = 0
  Private Const SQL_COLUMN_DETAIL_WITHDRAW_FOLIO As Integer = 1
  Private Const SQL_COLUMN_DETAIL_WITHDRAW_DATETIME As Integer = 2
  Private Const SQL_COLUMN_DETAIL_WITHDRAW_INITIAL_BALANCE As Integer = 3
  Private Const SQL_COLUMN_DETAIL_WITHDRAW_WITHDRAW As Integer = 4
  Private Const SQL_COLUMN_DETAIL_WITHDRAW_DEVOLUTION As Integer = 5
  Private Const SQL_COLUMN_DETAIL_WITHDRAW_PRIZE As Integer = 6
  Private Const SQL_COLUMN_DETAIL_WITHDRAW_TAX1 As Integer = 7
  Private Const SQL_COLUMN_DETAIL_WITHDRAW_TAX2 As Integer = 8
  'Private Const SQL_COLUMN_DETAIL_WITHDRAW_TAX3 As Integer = 9
  Private Const SQL_COLUMN_DETAIL_WITHDRAW_PAID_AMOUNT As Integer = 9
  Private Const SQL_COLUMN_DETAIL_WITHDRAW_FINAL_BALANCE As Integer = 10
  Private Const SQL_COLUMN_DETAIL_WITHDRAW_ACCOUNT_ID As Integer = 11
  Private Const SQL_COLUMN_DETAIL_WITHDRAW_CLIENT_NAME As Integer = 12
  Private Const SQL_COLUMN_DETAIL_WITHDRAW_CASHIER_USER As Integer = 13
  Private Const SQL_COLUMN_DETAIL_WITHDRAW_CASHIER_NAME As Integer = 14

  Private Const SQL_COLUMN_DETAIL_CONCEPT_RECEIPT As Integer = 0
  Private Const SQL_COLUMN_DETAIL_CONCEPT_FOLIO As Integer = 1
  Private Const SQL_COLUMN_DETAIL_CONCEPT_DATETIME As Integer = 2
  Private Const SQL_COLUMN_DETAIL_CONCEPT_DEPOSIT As Integer = 3
  Private Const SQL_COLUMN_DETAIL_CONCEPT_CASHIER_USER As Integer = 4
  Private Const SQL_COLUMN_DETAIL_CONCEPT_CASHIER_NAME As Integer = 5

  ' Grouped SQL columns
  Private Const SQL_COLUMN_GROUPED_DAY As Integer = 0
  Private Const SQL_COLUMN_GROUPED_AMOUNT As Integer = 1
  Private Const SQL_COLUMN_GROUPED_TAX As Integer = 2
  Private Const SQL_COLUMN_GROUPED_TOTAL As Integer = 3
  Private Const SQL_COLUMN_GROUPED_TRANSACTIONS As Integer = 4
  Private Const SQL_COLUMN_GROUPED_CONCEPT As Integer = 5

  Private Const SQL_COLUMN_GROUPED_CHARGE_DAY As Integer = 0
  Private Const SQL_COLUMN_GROUPED_CHARGE_DEPOSIT As Integer = 1
  Private Const SQL_COLUMN_GROUPED_CHARGE_PROMOTION As Integer = 2
  Private Const SQL_COLUMN_GROUPED_CHARGE_TOTAL_DEPOSIT As Integer = 3
  Private Const SQL_COLUMN_GROUPED_CHARGE_TRANSACTIONS As Integer = 4

  Private Const SQL_COLUMN_GROUPED_WITHDRAW_DAY As Integer = 0
  Private Const SQL_COLUMN_GROUPED_WITHDRAW_INITIAL_BALANCE As Integer = 1
  Private Const SQL_COLUMN_GROUPED_WITHDRAW_WITHDRAW As Integer = 2
  Private Const SQL_COLUMN_GROUPED_WITHDRAW_DEVOLUTION As Integer = 3
  Private Const SQL_COLUMN_GROUPED_WITHDRAW_PRIZE As Integer = 4
  Private Const SQL_COLUMN_GROUPED_WITHDRAW_TAX1 As Integer = 5
  Private Const SQL_COLUMN_GROUPED_WITHDRAW_TAX2 As Integer = 6
  'Private Const SQL_COLUMN_GROUPED_WITHDRAW_TAX3 As Integer = 7
  Private Const SQL_COLUMN_GROUPED_WITHDRAW_PAID_AMOUNT As Integer = 7
  Private Const SQL_COLUMN_GROUPED_WITHDRAW_FINAL_BALANCE As Integer = 8
  Private Const SQL_COLUMN_GROUPED_WITHDRAW_TRANSACTIONS As Integer = 9

  Private Const SQL_COLUMN_GROUPED_CONCEPT_DAY As Integer = 0
  Private Const SQL_COLUMN_GROUPED_CONCEPT_DEPOSIT As Integer = 1
  Private Const SQL_COLUMN_GROUPED_CONCEPT_TRANSACTIONS As Integer = 2

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0

  'Detailed columns
  Private Const GRID_COLUMN_DETAIL_RECEIPT As Integer = 1
  Private Const GRID_COLUMN_DETAIL_FOLIO As Integer = 2
  Private Const GRID_COLUMN_DETAIL_DATETIME As Integer = 3
  Private Const GRID_COLUMN_DETAIL_CONCEPT As Integer = 4
  Private Const GRID_COLUMN_DETAIL_AMOUNT As Integer = 5
  Private Const GRID_COLUMN_DETAIL_TAX As Integer = 6
  Private Const GRID_COLUMN_DETAIL_TOTAL As Integer = 7
  Private Const GRID_COLUMN_DETAIL_CASHIER_USER As Integer = 8
  Private Const GRID_COLUMN_DETAIL_CASHIER_NAME As Integer = 9
  Private Const GRID_COLUMN_DETAIL_CANCELLED_FOLIO As Integer = 10

  Private Const GRID_COLUMN_DETAIL_CHARGE_RECEIPT As Integer = 1
  Private Const GRID_COLUMN_DETAIL_CHARGE_FOLIO As Integer = 2
  Private Const GRID_COLUMN_DETAIL_CHARGE_DATETIME As Integer = 3
  Private Const GRID_COLUMN_DETAIL_CHARGE_DEPOSIT As Integer = 4
  Private Const GRID_COLUMN_DETAIL_CHARGE_PROMOTION As Integer = 5
  Private Const GRID_COLUMN_DETAIL_CHARGE_TOTAL_DEPOSIT As Integer = 6
  Private Const GRID_COLUMN_DETAIL_CHARGE_ACCOUNT_ID As Integer = 7
  Private Const GRID_COLUMN_DETAIL_CHARGE_CLIENT_NAME As Integer = 8
  Private Const GRID_COLUMN_DETAIL_CHARGE_CASHIER_USER As Integer = 9
  Private Const GRID_COLUMN_DETAIL_CHARGE_CASHIER_NAME As Integer = 10

  Private Const GRID_COLUMN_DETAIL_WITHDRAW_RECEIPT As Integer = 1
  Private Const GRID_COLUMN_DETAIL_WITHDRAW_FOLIO As Integer = 2
  Private Const GRID_COLUMN_DETAIL_WITHDRAW_DATETIME As Integer = 3
  Private Const GRID_COLUMN_DETAIL_WITHDRAW_INITIAL_BALANCE As Integer = 4
  Private Const GRID_COLUMN_DETAIL_WITHDRAW_WITHDRAW As Integer = 5
  Private Const GRID_COLUMN_DETAIL_WITHDRAW_DEVOLUTION As Integer = 6
  Private Const GRID_COLUMN_DETAIL_WITHDRAW_PRIZE As Integer = 7
  Private Const GRID_COLUMN_DETAIL_WITHDRAW_TAX1 As Integer = 8
  Private Const GRID_COLUMN_DETAIL_WITHDRAW_TAX2 As Integer = 9
  'Private Const GRID_COLUMN_DETAIL_WITHDRAW_TAX3 As Integer = 10
  Private Const GRID_COLUMN_DETAIL_WITHDRAW_PAID_AMOUNT As Integer = 10
  Private Const GRID_COLUMN_DETAIL_WITHDRAW_FINAL_BALANCE As Integer = 11
  Private Const GRID_COLUMN_DETAIL_WITHDRAW_ACCOUNT_ID As Integer = 12
  Private Const GRID_COLUMN_DETAIL_WITHDRAW_CLIENT_NAME As Integer = 13
  Private Const GRID_COLUMN_DETAIL_WITHDRAW_CASHIER_USER As Integer = 14
  Private Const GRID_COLUMN_DETAIL_WITHDRAW_CASHIER_NAME As Integer = 15

  'Grouped columns
  Private Const GRID_COLUMN_GROUPED_DAY As Integer = 1
  Private Const GRID_COLUMN_GROUPED_CONCEPT As Integer = 2
  Private Const GRID_COLUMN_GROUPED_AMOUNT As Integer = 3
  Private Const GRID_COLUMN_GROUPED_TAX As Integer = 4
  Private Const GRID_COLUMN_GROUPED_TOTAL As Integer = 5
  Private Const GRID_COLUMN_GROUPED_TRANSACTIONS As Integer = 6

  Private Const GRID_COLUMN_GROUPED_CHARGE_DAY As Integer = 1
  Private Const GRID_COLUMN_GROUPED_CHARGE_DEPOSIT As Integer = 2
  Private Const GRID_COLUMN_GROUPED_CHARGE_PROMOTION As Integer = 3
  Private Const GRID_COLUMN_GROUPED_CHARGE_TOTAL_DEPOSIT As Integer = 4
  Private Const GRID_COLUMN_GROUPED_CHARGE_TRANSACTIONS As Integer = 5

  Private Const GRID_COLUMN_GROUPED_WITHDRAW_DAY As Integer = 1
  Private Const GRID_COLUMN_GROUPED_WITHDRAW_INITIAL_BALANCE As Integer = 2
  Private Const GRID_COLUMN_GROUPED_WITHDRAW_WITHDRAW As Integer = 3
  Private Const GRID_COLUMN_GROUPED_WITHDRAW_DEVOLUTION As Integer = 4
  Private Const GRID_COLUMN_GROUPED_WITHDRAW_PRIZE As Integer = 5
  Private Const GRID_COLUMN_GROUPED_WITHDRAW_TAX1 As Integer = 6
  Private Const GRID_COLUMN_GROUPED_WITHDRAW_TAX2 As Integer = 7
  'Private Const GRID_COLUMN_GROUPED_WITHDRAW_TAX3 As Integer = 8
  Private Const GRID_COLUMN_GROUPED_WITHDRAW_PAID_AMOUNT As Integer = 8
  Private Const GRID_COLUMN_GROUPED_WITHDRAW_FINAL_BALANCE As Integer = 9
  Private Const GRID_COLUMN_GROUPED_WITHDRAW_TRANSACTIONS As Integer = 10

  'Num of columns
  Private Const GRID_NUM_COLUMNS_DETAIL As Integer = 11
  Private Const GRID_NUM_COLUMNS_DETAIL_CHARGE As Integer = 11
  Private Const GRID_NUM_COLUMNS_DETAIL_WITHDRAW As Integer = 16
  Private Const GRID_NUM_COLUMNS_GROUPED As Integer = 7
  Private Const GRID_NUM_COLUMNS_GROUPED_CHARGE As Integer = 6
  Private Const GRID_NUM_COLUMNS_GROUPED_WITHDRAW As Integer = 11
  Private Const GRID_NUM_HEADER_ROWS As Integer = 1

  'Width for the third tax
  Private Const GRID_COLUMN_WIDTH_WITHDRAW_TAX3 As Integer = 1600

#End Region ' Constants

#Region " Members "

  ' For report filters 
  Private m_option As String
  Private m_operation_type As String
  Private m_init_date_from As String
  Private m_init_date_to As String

  Private m_series As String

  ' For total amount row
  Private m_total_amount As Double
  Private m_total_tax As Double
  Private m_total_total As Double

  Private m_total_charge_deposit As Double
  Private m_total_charge_promotion As Double
  Private m_total_charge_total_deposit As Double

  Private m_total_withdraw_initial_balance As Double
  Private m_total_withdraw_withdraw As Double
  Private m_total_withdraw_devolution As Double
  Private m_total_withdraw_prize As Double
  Private m_total_withdraw_tax1 As Double
  Private m_total_withdraw_tax2 As Double
  Private m_total_withdraw_tax3 As Double
  Private m_total_withdraw_paid_amount As Double
  Private m_total_withdraw_final_balance As Double

  Private m_total_transactions As Integer

  Private m_firm_b_enabled As Boolean 'Indicates if B firm is active.
  Private m_third_tax_is_active As Boolean

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_TICKETS_REPORT

    Call MyBase.GUI_SetFormId()
  End Sub

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    Dim _split_data As New WSI.Common.TYPE_SPLITS

    WSI.Common.Split.ReadSplitParameters(_split_data)

    Call MyBase.GUI_InitControls()

    ' Set form Name
    Me.Text = GLB_NLS_GUI_INVOICING.GetString(384)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    ' Date time filter
    Me.uc_dsl.Init(GLB_NLS_GUI_INVOICING.Id(201))
    Me.uc_dsl.ClosingTimeEnabled = False

    ' Options
    Me.gb_options.Text = GLB_NLS_GUI_INVOICING.GetString(369)
    Me.opt_daily_detail.Text = GLB_NLS_GUI_INVOICING.GetString(277)
    Me.opt_daily_grouped.Text = GLB_NLS_GUI_INVOICING.GetString(385)

    'Operation Type
    Me.gb_operation_type.Text = GLB_NLS_GUI_INVOICING.GetString(386)

    Me.lbl_business_a.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1285) & " A"

    Me.opt_charge.Text = WSI.Common.GeneralParam.Value("Cashier", "Split.A.CashInVoucherTitle")
    If String.IsNullOrEmpty(opt_charge.Text) Then
      opt_charge.Text = "[" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(607) & "]"
    End If

    Me.opt_withdraw.Text = WSI.Common.GeneralParam.Value("Cashier", "Split.A.CashOutVoucherTitle")
    If String.IsNullOrEmpty(opt_withdraw.Text) Then
      opt_withdraw.Text = "[" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(608) & "]"
    End If

    opt_entry.Text = WSI.Common.Cage.GetConceptDescription(1)

    Me.lbl_business_b.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1285) & " B"

    Me.opt_sale_note.Text = WSI.Common.GeneralParam.Value("Cashier", "Split.B.CashInVoucherTitle")

    If String.IsNullOrEmpty(opt_sale_note.Text) Then
      opt_sale_note.Text = "[" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(607) & "]"
    End If

    Me.chk_charge_sale_note.Text = WSI.Common.GeneralParam.Value("Cashier", "Split.B.Name")
    Me.chk_cancel_charge_sale_note.Text = WSI.Common.GeneralParam.Value("Cashier", "Split.B.Name")
    Me.chk_service_charge_sale_note.Text = WSI.Common.GeneralParam.Value("Cashier.ServiceCharge", "Text")
    Me.chk_card_commission.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5821)

    Me.opt_charge_cancel_sale_note.Text = WSI.Common.GeneralParam.Value("Cashier", "Split.B.CashOutVoucherTitle")
    If String.IsNullOrEmpty(opt_charge_cancel_sale_note.Text) Then
      opt_charge_cancel_sale_note.Text = "[" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(608) & "]"
    End If

    Me.chk_player_card_pay.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5767)
    Me.chk_cancel_player_card_pay.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5767)

    m_firm_b_enabled = _split_data.enabled_b

    ' DCS 17-JUL-2015
    Call InitialLayoutConfiguration()

    ' Set filter default values
    Call SetDefaultValues()

    ' Grid
    Call GUI_StyleSheet()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()

    If (Me.opt_charge.Checked OrElse Me.opt_withdraw.Checked) Then
      m_series = WSI.Common.Misc.ReadGeneralParams("Cashier.Voucher.Mode.01", "Series.A")
      Me.lbl_series.Text = GLB_NLS_GUI_INVOICING.GetString(397) & ": " & m_series
    ElseIf (Me.opt_sale_note.Checked OrElse Me.opt_charge_cancel_sale_note.Checked) Then
      m_series = WSI.Common.Misc.ReadGeneralParams("Cashier.Voucher.Mode.01", "Series.B")
      Me.lbl_series.Text = GLB_NLS_GUI_INVOICING.GetString(397) & ": " & m_series
    Else
      m_series = ""
      Me.lbl_series.Text = GLB_NLS_GUI_INVOICING.GetString(397) & ":"
    End If

    m_total_amount = 0
    m_total_tax = 0
    m_total_total = 0

    m_total_charge_deposit = 0
    m_total_charge_promotion = 0
    m_total_charge_total_deposit = 0

    m_total_withdraw_initial_balance = 0
    m_total_withdraw_withdraw = 0
    m_total_withdraw_devolution = 0
    m_total_withdraw_prize = 0
    m_total_withdraw_tax1 = 0
    m_total_withdraw_tax2 = 0
    m_total_withdraw_tax3 = 0
    m_total_withdraw_paid_amount = 0
    m_total_withdraw_final_balance = 0

    m_total_transactions = 0

  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()

    Me.Grid.AddRow()

    Dim idx_row As Integer = Me.Grid.NumRows - 1

    ' TOTAL
    Me.Grid.Cell(idx_row, 1).Value = GLB_NLS_GUI_INVOICING.GetString(205)

    If opt_daily_detail.Checked Then

      If opt_sale_note.Checked OrElse opt_charge_cancel_sale_note.Checked Then
        Me.Grid.Cell(idx_row, GRID_COLUMN_DETAIL_AMOUNT).Value = GUI_FormatCurrency(m_total_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Me.Grid.Cell(idx_row, GRID_COLUMN_DETAIL_TAX).Value = GUI_FormatCurrency(m_total_tax, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Me.Grid.Cell(idx_row, GRID_COLUMN_DETAIL_TOTAL).Value = GUI_FormatCurrency(m_total_total, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        Me.Grid.Cell(idx_row, GRID_COLUMN_DETAIL_DATETIME).Value = GLB_NLS_GUI_INVOICING.GetString(398, GUI_FormatNumber(m_total_transactions, 0))

      ElseIf opt_charge.Checked OrElse opt_entry.Checked Then
        Me.Grid.Cell(idx_row, GRID_COLUMN_DETAIL_CHARGE_DEPOSIT).Value = GUI_FormatCurrency(m_total_charge_deposit, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Me.Grid.Cell(idx_row, GRID_COLUMN_DETAIL_CHARGE_PROMOTION).Value = GUI_FormatCurrency(m_total_charge_promotion, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Me.Grid.Cell(idx_row, GRID_COLUMN_DETAIL_CHARGE_TOTAL_DEPOSIT).Value = GUI_FormatCurrency(m_total_charge_total_deposit, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        Me.Grid.Cell(idx_row, GRID_COLUMN_DETAIL_CHARGE_DATETIME).Value = GLB_NLS_GUI_INVOICING.GetString(398, GUI_FormatNumber(m_total_transactions, 0))

      ElseIf opt_withdraw.Checked Then
        Me.Grid.Cell(idx_row, GRID_COLUMN_DETAIL_WITHDRAW_INITIAL_BALANCE).Value = GUI_FormatCurrency(m_total_withdraw_initial_balance, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Me.Grid.Cell(idx_row, GRID_COLUMN_DETAIL_WITHDRAW_WITHDRAW).Value = GUI_FormatCurrency(m_total_withdraw_withdraw, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Me.Grid.Cell(idx_row, GRID_COLUMN_DETAIL_WITHDRAW_DEVOLUTION).Value = GUI_FormatCurrency(m_total_withdraw_devolution, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Me.Grid.Cell(idx_row, GRID_COLUMN_DETAIL_WITHDRAW_PRIZE).Value = GUI_FormatCurrency(m_total_withdraw_prize, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Me.Grid.Cell(idx_row, GRID_COLUMN_DETAIL_WITHDRAW_TAX1).Value = GUI_FormatCurrency(m_total_withdraw_tax1, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Me.Grid.Cell(idx_row, GRID_COLUMN_DETAIL_WITHDRAW_TAX2).Value = GUI_FormatCurrency(m_total_withdraw_tax2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        'Me.Grid.Cell(idx_row, GRID_COLUMN_DETAIL_WITHDRAW_TAX3).Value = GUI_FormatCurrency(m_total_withdraw_tax3, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        'Third tax column must be shown when Third tax is active, or there are record with third data
        'SetThirdColumnWidth(GRID_COLUMN_DETAIL_WITHDRAW_TAX3, (m_third_tax_is_active OrElse (m_total_withdraw_tax3 > 0)))

        Me.Grid.Cell(idx_row, GRID_COLUMN_DETAIL_WITHDRAW_PAID_AMOUNT).Value = GUI_FormatCurrency(m_total_withdraw_paid_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Me.Grid.Cell(idx_row, GRID_COLUMN_DETAIL_WITHDRAW_FINAL_BALANCE).Value = GUI_FormatCurrency(m_total_withdraw_final_balance, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        Me.Grid.Cell(idx_row, GRID_COLUMN_DETAIL_WITHDRAW_DATETIME).Value = GLB_NLS_GUI_INVOICING.GetString(398, GUI_FormatNumber(m_total_transactions, 0))

      End If
    ElseIf opt_daily_grouped.Checked Then

      If opt_sale_note.Checked OrElse opt_charge_cancel_sale_note.Checked Then
        Me.Grid.Cell(idx_row, GRID_COLUMN_GROUPED_AMOUNT).Value = GUI_FormatCurrency(m_total_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Me.Grid.Cell(idx_row, GRID_COLUMN_GROUPED_CONCEPT).Value = String.Empty
        Me.Grid.Cell(idx_row, GRID_COLUMN_GROUPED_TAX).Value = GUI_FormatCurrency(m_total_tax, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Me.Grid.Cell(idx_row, GRID_COLUMN_GROUPED_TOTAL).Value = GUI_FormatCurrency(m_total_total, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        Me.Grid.Cell(idx_row, GRID_COLUMN_GROUPED_TRANSACTIONS).Value = GUI_FormatNumber(m_total_transactions, 0)

      ElseIf opt_charge.Checked OrElse opt_entry.Checked Then
        Me.Grid.Cell(idx_row, GRID_COLUMN_GROUPED_CHARGE_DEPOSIT).Value = GUI_FormatCurrency(m_total_charge_deposit, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Me.Grid.Cell(idx_row, GRID_COLUMN_GROUPED_CHARGE_PROMOTION).Value = GUI_FormatCurrency(m_total_charge_promotion, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Me.Grid.Cell(idx_row, GRID_COLUMN_GROUPED_CHARGE_TOTAL_DEPOSIT).Value = GUI_FormatCurrency(m_total_charge_total_deposit, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        Me.Grid.Cell(idx_row, GRID_COLUMN_GROUPED_CHARGE_TRANSACTIONS).Value = GUI_FormatNumber(m_total_transactions, 0)

      ElseIf opt_withdraw.Checked Then
        Me.Grid.Cell(idx_row, GRID_COLUMN_GROUPED_WITHDRAW_INITIAL_BALANCE).Value = GUI_FormatCurrency(m_total_withdraw_initial_balance, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Me.Grid.Cell(idx_row, GRID_COLUMN_GROUPED_WITHDRAW_WITHDRAW).Value = GUI_FormatCurrency(m_total_withdraw_withdraw, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Me.Grid.Cell(idx_row, GRID_COLUMN_GROUPED_WITHDRAW_DEVOLUTION).Value = GUI_FormatCurrency(m_total_withdraw_devolution, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Me.Grid.Cell(idx_row, GRID_COLUMN_GROUPED_WITHDRAW_PRIZE).Value = GUI_FormatCurrency(m_total_withdraw_prize, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Me.Grid.Cell(idx_row, GRID_COLUMN_GROUPED_WITHDRAW_TAX1).Value = GUI_FormatCurrency(m_total_withdraw_tax1, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Me.Grid.Cell(idx_row, GRID_COLUMN_GROUPED_WITHDRAW_TAX2).Value = GUI_FormatCurrency(m_total_withdraw_tax2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        'Me.Grid.Cell(idx_row, GRID_COLUMN_GROUPED_WITHDRAW_TAX3).Value = GUI_FormatCurrency(m_total_withdraw_tax3, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        'Third tax column must be shown when Third tax is active, or there are record with third data
        'SetThirdColumnWidth(GRID_COLUMN_GROUPED_WITHDRAW_TAX3, (m_third_tax_is_active OrElse (m_total_withdraw_tax3 > 0)))

        Me.Grid.Cell(idx_row, GRID_COLUMN_GROUPED_WITHDRAW_PAID_AMOUNT).Value = GUI_FormatCurrency(m_total_withdraw_paid_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Me.Grid.Cell(idx_row, GRID_COLUMN_GROUPED_WITHDRAW_FINAL_BALANCE).Value = GUI_FormatCurrency(m_total_withdraw_final_balance, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        Me.Grid.Cell(idx_row, GRID_COLUMN_GROUPED_WITHDRAW_TRANSACTIONS).Value = GUI_FormatNumber(m_total_transactions, 0)

      End If
    End If

    Me.Grid.Row(idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

  End Sub ' GUI_AfterLastRow

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted

  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate > Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    End If

    ' Option selection 
    If Me.opt_sale_note.Checked Then
      If Not (Me.chk_charge_sale_note.Checked OrElse Me.chk_service_charge_sale_note.Checked OrElse (WSI.Common.Misc.IsCommissionToCompanyBEnabled AndAlso Me.chk_card_commission.Checked) OrElse (WSI.Common.Misc.IsCardPlayerToCompanyB() AndAlso Me.chk_player_card_pay.Checked)) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1292), ENUM_MB_TYPE.MB_TYPE_ERROR, , , Me.opt_sale_note.Text)
        Call Me.chk_charge_sale_note.Focus()

        Return False
      End If
    End If

    If WSI.Common.Misc.IsCardPlayerToCompanyB() And Me.opt_charge_cancel_sale_note.Checked Then
      If Not (Me.chk_cancel_player_card_pay.Checked OrElse Me.chk_cancel_charge_sale_note.Checked) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1292), ENUM_MB_TYPE.MB_TYPE_ERROR, , , Me.opt_charge_cancel_sale_note.Text)
        Call Me.chk_cancel_charge_sale_note.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database

  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    If opt_daily_detail.Checked Then
      'Details
      Return GetSQLQueryForDetail()
    ElseIf opt_daily_grouped.Checked Then
      'Grouped by day
      Return GetSQLQueryForGroupedByDay()
    Else
      'Should not happen
      Return String.Empty
    End If
  End Function ' GUI_FilterGetSqlQuery

  ''' <summary>
  ''' Get SQL for detail
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetSQLQueryForDetail() As String
    Dim _sb As StringBuilder
    _sb = New StringBuilder()

    If (opt_sale_note.Checked OrElse opt_charge_cancel_sale_note.Checked) Then
      _sb.AppendLine("SELECT   CV_VOUCHER_ID [Recibo]")
      _sb.AppendLine("       , CV_SEQUENCE [Folio]")
      _sb.AppendLine("       , CV_DATETIME [Fecha y Hora]")
      _sb.AppendLine("       , CV_M01_BASE [Importe]")
      _sb.AppendLine("       , CV_M01_TAX1 [IVA]")
      _sb.AppendLine("       , CV_M01_BASE + CV_M01_TAX1 [Total]")
      _sb.AppendLine("       , ISNULL(GP_KEY_VALUE, CV_TYPE) [Concepto]")
      _sb.AppendLine("       , ( SELECT GU_FULL_NAME FROM GUI_USERS WHERE GU_USER_ID = CV_USER_ID) [Cajero]")
      _sb.AppendLine("       , ( SELECT CT_NAME FROM CASHIER_TERMINALS WHERE CT_CASHIER_ID = CV_CASHIER_ID) [Caja] ")
      _sb.AppendLine("       , CV_M01_CANCELLED_SEQUENCE [Folio cancelado]")
      _sb.AppendLine("  FROM   CASHIER_VOUCHERS ")
      _sb.AppendLine("  LEFT   OUTER JOIN  GENERAL_PARAMS ON (")
      _sb.AppendLine("                                         (GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Split.B.Name' AND CV_TYPE NOT IN  (" & GetStringForListOfCVTypes(GetSalesNoteListOfCVTypesNotToIncludeInGroupReport()) & ") )")
      _sb.AppendLine("                                      OR (GP_GROUP_KEY = 'Cashier.PaymentMethod' AND GP_SUBJECT_KEY = 'CommissionToCompanyB.BankCard.VoucherTitle' AND CV_TYPE = " & WSI.Common.CashierVoucherType.Commission_B & ")")

      If WSI.Common.Misc.IsCommissionToCompanyBEnabled Then
        _sb.AppendLine("                                      OR (GP_GROUP_KEY = 'Cashier.PaymentMethod' AND GP_SUBJECT_KEY = 'CommissionToCompanyB.BankCard.VoucherTitle' AND CV_TYPE = " & WSI.Common.CashierVoucherType.Commission_B & ")")
        _sb.AppendLine("                                      OR (GP_GROUP_KEY = 'Cashier.PaymentMethod' AND GP_SUBJECT_KEY = 'CommissionToCompanyB.Check.VoucherTitle' AND CV_TYPE = " & WSI.Common.CashierVoucherType.CheckCommissionB & ")")
        _sb.AppendLine("                                      OR (GP_GROUP_KEY = 'Cashier.PaymentMethod' AND GP_SUBJECT_KEY = 'CommissionToCompanyB.Currency.VoucherTitle' AND CV_TYPE = " & WSI.Common.CashierVoucherType.ExchangeCommissionB & ")")
      End If

      _sb.AppendLine("                                      OR (GP_GROUP_KEY = 'Cashier.ServiceCharge' AND GP_SUBJECT_KEY = 'Text' AND CV_TYPE = " & WSI.Common.CashierVoucherType.ServiceCharge_B & ")")

      _sb.AppendLine("                                       ) ")

    ElseIf opt_charge.Checked Then
      _sb.AppendLine("SELECT   CV_VOUCHER_ID [Recibo]")
      _sb.AppendLine("       , CV_SEQUENCE [Folio]")
      _sb.AppendLine("       , CV_DATETIME [Fecha y Hora]")
      _sb.AppendLine("       , CV_M01_DEV  [Dep�sito]")
      _sb.AppendLine("       , CV_M01_BASE [Promoci�n]")
      _sb.AppendLine("       , CV_M01_DEV + CV_M01_BASE [Total Dep�sito]")
      _sb.AppendLine("       , CV_ACCOUNT_ID [Cuenta]")
      _sb.AppendLine("       , ( SELECT AC_HOLDER_NAME FROM ACCOUNTS WHERE AC_ACCOUNT_ID = CV_ACCOUNT_ID) [Nombre Cliente]")
      _sb.AppendLine("       , ( SELECT GU_FULL_NAME FROM GUI_USERS WHERE GU_USER_ID = CV_USER_ID) [Cajero]")
      _sb.AppendLine("       , ( SELECT CT_NAME FROM CASHIER_TERMINALS WHERE CT_CASHIER_ID = CV_CASHIER_ID) [Caja] ")
      _sb.AppendLine("  FROM   CASHIER_VOUCHERS ")

    ElseIf opt_withdraw.Checked Then
      _sb.AppendLine("SELECT   CV_VOUCHER_ID [Recibo]")
      _sb.AppendLine("       , CV_SEQUENCE   [Folio]")
      _sb.AppendLine("       , CV_DATETIME   [Fecha y Hora]")
      _sb.AppendLine("       , CV_M01_FINAL + CV_M01_DEV  + CV_M01_BASE [Saldo Inicial]")
      _sb.AppendLine("       , CV_M01_DEV   + CV_M01_BASE [Retiro]")
      _sb.AppendLine("       , CV_M01_DEV    [Devoluci�n]")
      _sb.AppendLine("       , CV_M01_BASE   [Premio]")
      _sb.AppendLine("       , CV_M01_TAX1   [Retenci�n 1]")
      _sb.AppendLine("       , CV_M01_TAX2   [Retenci�n 2]")
      _sb.AppendLine("       , CV_M01_DEV   + CV_M01_BASE - CV_M01_TAX1 - CV_M01_TAX2 [Cantidad Pagada]")
      _sb.AppendLine("       , CV_M01_FINAL  [Saldo Final]")
      _sb.AppendLine("       , CV_ACCOUNT_ID [Cuenta]")
      _sb.AppendLine("       , ( SELECT AC_HOLDER_NAME FROM ACCOUNTS WHERE AC_ACCOUNT_ID = CV_ACCOUNT_ID) [Nombre Cliente]")
      _sb.AppendLine("       , ( SELECT GU_FULL_NAME FROM GUI_USERS WHERE GU_USER_ID = CV_USER_ID) [Cajero]")
      _sb.AppendLine("       , ( SELECT CT_NAME FROM CASHIER_TERMINALS WHERE CT_CASHIER_ID = CV_CASHIER_ID) [Caja] ")
      _sb.AppendLine("  FROM   CASHIER_VOUCHERS ")

    ElseIf opt_entry.Checked Then
      _sb.AppendLine("SELECT   CV_VOUCHER_ID [Recibo]")
      _sb.AppendLine("       , CV_SEQUENCE [Folio]")
      _sb.AppendLine("       , CV_DATETIME [Fecha y Hora]")
      _sb.AppendLine("       , CV_M01_DEV  [Dep�sito]")
      _sb.AppendLine("       , ( SELECT GU_FULL_NAME FROM GUI_USERS WHERE GU_USER_ID = CV_USER_ID) [Cajero]")
      _sb.AppendLine("       , ( SELECT CT_NAME FROM CASHIER_TERMINALS WHERE CT_CASHIER_ID = CV_CASHIER_ID) [Caja] ")
      _sb.AppendLine("  FROM   CASHIER_VOUCHERS ")

    End If

    _sb.AppendLine(GetSqlWhere())
    _sb.AppendLine(" ORDER BY 2 ASC ")

    Return _sb.ToString()
  End Function

  ''' <summary>
  ''' Get SQL for grouped by day
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetSQLQueryForGroupedByDay() As String
    Dim _sb As StringBuilder
    _sb = New StringBuilder()

    Dim _base_date As String = "'01/01/2007 " & Me.uc_dsl.ClosingTime.ToString("00") & ":00:00'"

    If (opt_sale_note.Checked OrElse opt_charge_cancel_sale_note.Checked) Then
      _sb.AppendLine("SELECT   DATEADD (DAY, DATEDIFF(HOUR, " & _base_date & ", CV_DATETIME) / 24, " & _base_date & ")  [Jornada]")
      _sb.AppendLine("       , SUM ( CV_M01_BASE ) [Importe]")
      _sb.AppendLine("       , SUM ( CV_M01_TAX1 ) [IVA]")
      _sb.AppendLine("       , SUM ( CV_M01_BASE + CV_M01_TAX1 ) [Total]")
      _sb.AppendLine("       , SUM ( 1 ) [Transacciones] ")
      'Concepto
      _sb.AppendLine("       , ISNULL((CASE WHEN (CV_TYPE NOT IN ( " & GetStringForListOfCVTypes(GetSalesNoteListOfCVTypesNotToIncludeInGroupReport()) & ") ) THEN  (SELECT MIN(GP_KEY_VALUE) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Split.B.Name')")

      If WSI.Common.Misc.IsCommissionToCompanyBEnabled Then
        _sb.AppendLine("                      WHEN (CV_TYPE = " & WSI.Common.CashierVoucherType.Commission_B & ") THEN  (SELECT MIN(GP_KEY_VALUE) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.PaymentMethod' AND GP_SUBJECT_KEY = 'CommissionToCompanyB.BankCard.VoucherTitle')")
        _sb.AppendLine("                      WHEN (CV_TYPE = " & WSI.Common.CashierVoucherType.CheckCommissionB & ") THEN  (SELECT MIN(GP_KEY_VALUE) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.PaymentMethod' AND GP_SUBJECT_KEY = 'CommissionToCompanyB.Check.VoucherTitle')")
        _sb.AppendLine("                      WHEN (CV_TYPE = " & WSI.Common.CashierVoucherType.ExchangeCommissionB & ") THEN  (SELECT MIN(GP_KEY_VALUE) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.PaymentMethod' AND GP_SUBJECT_KEY = 'CommissionToCompanyB.Currency.VoucherTitle')")
      End If

      _sb.AppendLine("                      WHEN (CV_TYPE = " & WSI.Common.CashierVoucherType.ServiceCharge_B & ") THEN  (SELECT MIN(GP_KEY_VALUE) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.ServiceCharge' AND GP_SUBJECT_KEY = 'Text')")
      _sb.AppendLine("                      ELSE CAST(CV_TYPE AS NVARCHAR(50))")

      _sb.AppendLine("                 END), CAST(CV_TYPE AS NVARCHAR(50)))  [Concepto] ")
    ElseIf opt_charge.Checked Then
      _sb.AppendLine("SELECT   DATEADD (DAY, DATEDIFF(HOUR, " & _base_date & ", CV_DATETIME) / 24, " & _base_date & ")   [Jornada]")
      _sb.AppendLine("       , SUM ( CV_M01_DEV  )              [Dep�sito]")
      _sb.AppendLine("       , SUM ( CV_M01_BASE )              [Promoci�n]")
      _sb.AppendLine("       , SUM ( CV_M01_DEV + CV_M01_BASE ) [Total Dep�sito]")
      _sb.AppendLine("       , SUM ( 1 ) [Transacciones] ")
    ElseIf opt_withdraw.Checked Then
      _sb.AppendLine("SELECT   DATEADD (DAY, DATEDIFF(HOUR, " & _base_date & ", CV_DATETIME) / 24, " & _base_date & ")   [Jornada]")
      _sb.AppendLine("       , SUM ( CV_M01_FINAL + CV_M01_DEV  + CV_M01_BASE + CV_M01_TAX1 + CV_M01_TAX2 ) [Saldo Inicial]")
      _sb.AppendLine("       , SUM ( CV_M01_DEV   + CV_M01_BASE + CV_M01_TAX1 + CV_M01_TAX2 )               [Retiro]")
      _sb.AppendLine("       , SUM ( CV_M01_DEV )   [Devoluci�n]")
      _sb.AppendLine("       , SUM ( CV_M01_BASE )  [Premio]")
      _sb.AppendLine("       , SUM ( CV_M01_TAX1 )  [Retenci�n 1]")
      _sb.AppendLine("       , SUM ( CV_M01_TAX2 )  [Retenci�n 2]")
      _sb.AppendLine("       , SUM ( CV_M01_DEV + CV_M01_BASE - CV_M01_TAX1 - CV_M01_TAX2 ) [Cantidad Pagada]")
      _sb.AppendLine("       , SUM ( CV_M01_FINAL ) [Saldo Final]")
      _sb.AppendLine("       , SUM ( 1 ) [Transacciones] ")
    ElseIf opt_entry.Checked Then
      _sb.AppendLine("SELECT   DATEADD (DAY, DATEDIFF(HOUR, " & _base_date & ", CV_DATETIME) / 24, " & _base_date & ")   [Jornada]")
      _sb.AppendLine("       , SUM ( CV_M01_DEV ) [Dep�sito]")
      _sb.AppendLine("       , SUM ( 1 ) [Transacciones] ")
    End If

    _sb.AppendLine("  FROM   CASHIER_VOUCHERS ")

    _sb.AppendLine(GetSqlWhere())

    'Set order
    If (opt_sale_note.Checked OrElse opt_charge_cancel_sale_note.Checked) Then
      _sb.AppendLine(" GROUP BY  DATEADD (DAY, DATEDIFF(HOUR, " & _base_date & ", CV_DATETIME) / 24, " & _base_date & "), CV_TYPE")
      _sb.AppendLine(" ORDER BY  1 ASC, 6 ASC ")
    Else
      _sb.AppendLine(" GROUP BY  DATEADD (DAY, DATEDIFF(HOUR, " & _base_date & ", CV_DATETIME) / 24, " & _base_date & ")")
      _sb.AppendLine(" ORDER BY  1 ASC ")
    End If

    Return _sb.ToString()
  End Function

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _value As Double

    ' Assign Mapped columns (search for 'mapping' string in this file)
    Call MyBase.GUI_SetupRow(RowIndex, DbRow)

    If opt_daily_detail.Checked Then

      m_total_transactions += 1

      If opt_sale_note.Checked OrElse opt_charge_cancel_sale_note.Checked Then

        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_RECEIPT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_DETAIL_RECEIPT), 0)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_FOLIO).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_DETAIL_FOLIO), 0)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DETAIL_DATETIME), _
                                                                              ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                              ENUM_FORMAT_TIME.FORMAT_HHMM)

        _value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_DETAIL_AMOUNT)), 0, DbRow.Value(SQL_COLUMN_DETAIL_AMOUNT))
        m_total_amount += _value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_AMOUNT).Value = GUI_FormatCurrency(_value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        _value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_DETAIL_TAX)), 0, DbRow.Value(SQL_COLUMN_DETAIL_TAX))
        m_total_tax += _value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_TAX).Value = GUI_FormatCurrency(_value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        _value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_DETAIL_TOTAL)), 0, DbRow.Value(SQL_COLUMN_DETAIL_TOTAL))
        m_total_total += _value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_TOTAL).Value = GUI_FormatCurrency(_value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_CONCEPT).Value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_DETAIL_CONCEPT)), " ", DbRow.Value(SQL_COLUMN_DETAIL_CONCEPT))
        Call SetConceptColumnValueForCardPlayerToCompanyB(RowIndex, GRID_COLUMN_DETAIL_CONCEPT)

        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_CASHIER_USER).Value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_DETAIL_CASHIER_USER)), " ", DbRow.Value(SQL_COLUMN_DETAIL_CASHIER_USER))

        If IsDBNull(DbRow.Value(SQL_COLUMN_DETAIL_CASHIER_NAME)) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_CASHIER_NAME).Value = " "
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_CASHIER_NAME).Value = Computer.Alias(DbRow.Value(SQL_COLUMN_DETAIL_CASHIER_NAME))
        End If

        If IsDBNull(DbRow.Value(SQL_COLUMN_DETAIL_CANCELLED_FOLIO)) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_CANCELLED_FOLIO).Value = ""
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_CANCELLED_FOLIO).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_DETAIL_CANCELLED_FOLIO), 0)
        End If

      ElseIf opt_entry.Checked Then

        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_RECEIPT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_DETAIL_CONCEPT_RECEIPT), 0)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_FOLIO).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_DETAIL_CONCEPT_FOLIO), 0)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DETAIL_CONCEPT_DATETIME), _
                                                                              ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                              ENUM_FORMAT_TIME.FORMAT_HHMM)

        _value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_DETAIL_CONCEPT_DEPOSIT)), 0, DbRow.Value(SQL_COLUMN_DETAIL_CONCEPT_DEPOSIT))
        m_total_charge_deposit += _value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_CHARGE_DEPOSIT).Value = GUI_FormatCurrency(_value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_CHARGE_CASHIER_USER).Value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_DETAIL_CONCEPT_CASHIER_USER)), " ", DbRow.Value(SQL_COLUMN_DETAIL_CONCEPT_CASHIER_USER))

        If IsDBNull(DbRow.Value(SQL_COLUMN_DETAIL_CONCEPT_CASHIER_NAME)) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_CHARGE_CASHIER_NAME).Value = " "
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_CHARGE_CASHIER_NAME).Value = Computer.Alias(DbRow.Value(SQL_COLUMN_DETAIL_CONCEPT_CASHIER_NAME))
        End If

      ElseIf opt_charge.Checked Then

        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_CHARGE_RECEIPT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_DETAIL_CHARGE_RECEIPT), 0)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_CHARGE_FOLIO).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_DETAIL_CHARGE_FOLIO), 0)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_CHARGE_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DETAIL_CHARGE_DATETIME), _
                                                                              ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                              ENUM_FORMAT_TIME.FORMAT_HHMM)

        _value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_DETAIL_CHARGE_DEPOSIT)), 0, DbRow.Value(SQL_COLUMN_DETAIL_CHARGE_DEPOSIT))
        m_total_charge_deposit += _value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_CHARGE_DEPOSIT).Value = GUI_FormatCurrency(_value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        _value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_DETAIL_CHARGE_PROMOTION)), 0, DbRow.Value(SQL_COLUMN_DETAIL_CHARGE_PROMOTION))
        m_total_charge_promotion += _value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_CHARGE_PROMOTION).Value = GUI_FormatCurrency(_value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        _value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_DETAIL_CHARGE_TOTAL_DEPOSIT)), 0, DbRow.Value(SQL_COLUMN_DETAIL_CHARGE_TOTAL_DEPOSIT))
        m_total_charge_total_deposit += _value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_CHARGE_TOTAL_DEPOSIT).Value = GUI_FormatCurrency(_value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_CHARGE_ACCOUNT_ID).Value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_DETAIL_CHARGE_ACCOUNT_ID)), " ", DbRow.Value(SQL_COLUMN_DETAIL_CHARGE_ACCOUNT_ID))
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_CHARGE_CLIENT_NAME).Value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_DETAIL_CHARGE_CLIENT_NAME)), " ", DbRow.Value(SQL_COLUMN_DETAIL_CHARGE_CLIENT_NAME))
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_CHARGE_CASHIER_USER).Value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_DETAIL_CHARGE_CASHIER_USER)), " ", DbRow.Value(SQL_COLUMN_DETAIL_CHARGE_CASHIER_USER))
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_CHARGE_CASHIER_NAME).Value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_DETAIL_CHARGE_CASHIER_NAME)), " ", DbRow.Value(SQL_COLUMN_DETAIL_CHARGE_CASHIER_NAME))

      ElseIf opt_withdraw.Checked Then

        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_WITHDRAW_RECEIPT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_DETAIL_WITHDRAW_RECEIPT), 0)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_WITHDRAW_FOLIO).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_DETAIL_WITHDRAW_FOLIO), 0)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_WITHDRAW_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DETAIL_WITHDRAW_DATETIME), _
                                                                                      ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                      ENUM_FORMAT_TIME.FORMAT_HHMM)

        _value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_DETAIL_WITHDRAW_INITIAL_BALANCE)), 0, DbRow.Value(SQL_COLUMN_DETAIL_WITHDRAW_INITIAL_BALANCE))
        m_total_withdraw_initial_balance += _value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_WITHDRAW_INITIAL_BALANCE).Value = GUI_FormatCurrency(_value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        _value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_DETAIL_WITHDRAW_WITHDRAW)), 0, DbRow.Value(SQL_COLUMN_DETAIL_WITHDRAW_WITHDRAW))
        m_total_withdraw_withdraw += _value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_WITHDRAW_WITHDRAW).Value = GUI_FormatCurrency(_value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        _value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_DETAIL_WITHDRAW_DEVOLUTION)), 0, DbRow.Value(SQL_COLUMN_DETAIL_WITHDRAW_DEVOLUTION))
        m_total_withdraw_devolution += _value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_WITHDRAW_DEVOLUTION).Value = GUI_FormatCurrency(_value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        _value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_DETAIL_WITHDRAW_PRIZE)), 0, DbRow.Value(SQL_COLUMN_DETAIL_WITHDRAW_PRIZE))
        m_total_withdraw_prize += _value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_WITHDRAW_PRIZE).Value = GUI_FormatCurrency(_value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        _value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_DETAIL_WITHDRAW_TAX1)), 0, DbRow.Value(SQL_COLUMN_DETAIL_WITHDRAW_TAX1))
        m_total_withdraw_tax1 += _value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_WITHDRAW_TAX1).Value = GUI_FormatCurrency(_value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        _value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_DETAIL_WITHDRAW_TAX2)), 0, DbRow.Value(SQL_COLUMN_DETAIL_WITHDRAW_TAX2))
        m_total_withdraw_tax2 += _value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_WITHDRAW_TAX2).Value = GUI_FormatCurrency(_value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        '_value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_DETAIL_WITHDRAW_TAX3)), 0, DbRow.Value(SQL_COLUMN_DETAIL_WITHDRAW_TAX3))
        'm_total_withdraw_tax3 += _value
        'Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_WITHDRAW_TAX3).Value = GUI_FormatCurrency(_value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        _value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_DETAIL_WITHDRAW_PAID_AMOUNT)), 0, DbRow.Value(SQL_COLUMN_DETAIL_WITHDRAW_PAID_AMOUNT))
        m_total_withdraw_paid_amount += _value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_WITHDRAW_PAID_AMOUNT).Value = GUI_FormatCurrency(_value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        _value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_DETAIL_WITHDRAW_FINAL_BALANCE)), 0, DbRow.Value(SQL_COLUMN_DETAIL_WITHDRAW_FINAL_BALANCE))
        m_total_withdraw_final_balance += _value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_WITHDRAW_FINAL_BALANCE).Value = GUI_FormatCurrency(_value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_WITHDRAW_ACCOUNT_ID).Value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_DETAIL_WITHDRAW_ACCOUNT_ID)), " ", DbRow.Value(SQL_COLUMN_DETAIL_WITHDRAW_ACCOUNT_ID))
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_WITHDRAW_CLIENT_NAME).Value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_DETAIL_WITHDRAW_CLIENT_NAME)), " ", DbRow.Value(SQL_COLUMN_DETAIL_WITHDRAW_CLIENT_NAME))
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_WITHDRAW_CASHIER_USER).Value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_DETAIL_WITHDRAW_CASHIER_USER)), " ", DbRow.Value(SQL_COLUMN_DETAIL_WITHDRAW_CASHIER_USER))
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAIL_WITHDRAW_CASHIER_NAME).Value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_DETAIL_WITHDRAW_CASHIER_NAME)), " ", DbRow.Value(SQL_COLUMN_DETAIL_WITHDRAW_CASHIER_NAME))

      End If
    ElseIf opt_daily_grouped.Checked Then

      If opt_sale_note.Checked OrElse opt_charge_cancel_sale_note.Checked Then

        Me.Grid.Cell(RowIndex, GRID_COLUMN_GROUPED_DAY).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_GROUPED_DAY), _
                                                                                      ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                      ENUM_FORMAT_TIME.FORMAT_TIME_NONE)

        Me.Grid.Cell(RowIndex, GRID_COLUMN_GROUPED_CONCEPT).Value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_GROUPED_CONCEPT)), " ", DbRow.Value(SQL_COLUMN_GROUPED_CONCEPT))
        Call SetConceptColumnValueForCardPlayerToCompanyB(RowIndex, GRID_COLUMN_GROUPED_CONCEPT)

        _value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_GROUPED_AMOUNT)), 0, DbRow.Value(SQL_COLUMN_GROUPED_AMOUNT))
        m_total_amount += _value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_GROUPED_AMOUNT).Value = GUI_FormatCurrency(_value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        _value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_GROUPED_TAX)), 0, DbRow.Value(SQL_COLUMN_GROUPED_TAX))
        m_total_tax += _value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_GROUPED_TAX).Value = GUI_FormatCurrency(_value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        _value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_GROUPED_TOTAL)), 0, DbRow.Value(SQL_COLUMN_GROUPED_TOTAL))
        m_total_total += _value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_GROUPED_TOTAL).Value = GUI_FormatCurrency(_value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        _value = DbRow.Value(SQL_COLUMN_GROUPED_TRANSACTIONS)
        m_total_transactions += _value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_GROUPED_TRANSACTIONS).Value = GUI_FormatNumber(_value, 0)

      ElseIf opt_charge.Checked Then

        Me.Grid.Cell(RowIndex, GRID_COLUMN_GROUPED_CHARGE_DAY).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_GROUPED_CHARGE_DAY), _
                                                                                              ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                              ENUM_FORMAT_TIME.FORMAT_TIME_NONE)

        _value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_GROUPED_CHARGE_DEPOSIT)), 0, DbRow.Value(SQL_COLUMN_GROUPED_CHARGE_DEPOSIT))
        m_total_charge_deposit += _value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_GROUPED_CHARGE_DEPOSIT).Value = GUI_FormatCurrency(_value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        _value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_GROUPED_CHARGE_PROMOTION)), 0, DbRow.Value(SQL_COLUMN_GROUPED_CHARGE_PROMOTION))
        m_total_charge_promotion += _value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_GROUPED_CHARGE_PROMOTION).Value = GUI_FormatCurrency(_value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        _value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_GROUPED_CHARGE_TOTAL_DEPOSIT)), 0, DbRow.Value(SQL_COLUMN_GROUPED_CHARGE_TOTAL_DEPOSIT))
        m_total_charge_total_deposit += _value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_GROUPED_CHARGE_TOTAL_DEPOSIT).Value = GUI_FormatCurrency(_value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        _value = DbRow.Value(SQL_COLUMN_GROUPED_CHARGE_TRANSACTIONS)
        m_total_transactions += _value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_GROUPED_CHARGE_TRANSACTIONS).Value = GUI_FormatNumber(_value, 0)

      ElseIf opt_entry.Checked Then

        Me.Grid.Cell(RowIndex, GRID_COLUMN_GROUPED_CHARGE_DAY).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_GROUPED_CONCEPT_DAY), _
                                                                                              ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                              ENUM_FORMAT_TIME.FORMAT_TIME_NONE)

        _value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_GROUPED_CONCEPT_DEPOSIT)), 0, DbRow.Value(SQL_COLUMN_GROUPED_CONCEPT_DEPOSIT))
        m_total_charge_deposit += _value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_GROUPED_CHARGE_DEPOSIT).Value = GUI_FormatCurrency(_value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        _value = DbRow.Value(SQL_COLUMN_GROUPED_CONCEPT_TRANSACTIONS)
        m_total_transactions += _value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_GROUPED_CHARGE_TRANSACTIONS).Value = GUI_FormatNumber(_value, 0)

      ElseIf opt_withdraw.Checked Then

        Me.Grid.Cell(RowIndex, GRID_COLUMN_GROUPED_WITHDRAW_DAY).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_GROUPED_WITHDRAW_DAY), _
                                                                                                      ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                                      ENUM_FORMAT_TIME.FORMAT_TIME_NONE)

        _value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_GROUPED_WITHDRAW_INITIAL_BALANCE)), 0, DbRow.Value(SQL_COLUMN_GROUPED_WITHDRAW_INITIAL_BALANCE))
        m_total_withdraw_initial_balance += _value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_GROUPED_WITHDRAW_INITIAL_BALANCE).Value = GUI_FormatCurrency(_value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        _value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_GROUPED_WITHDRAW_WITHDRAW)), 0, DbRow.Value(SQL_COLUMN_GROUPED_WITHDRAW_WITHDRAW))
        m_total_withdraw_withdraw += _value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_GROUPED_WITHDRAW_WITHDRAW).Value = GUI_FormatCurrency(_value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        _value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_GROUPED_WITHDRAW_DEVOLUTION)), 0, DbRow.Value(SQL_COLUMN_GROUPED_WITHDRAW_DEVOLUTION))
        m_total_withdraw_devolution += _value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_GROUPED_WITHDRAW_DEVOLUTION).Value = GUI_FormatCurrency(_value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        _value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_GROUPED_WITHDRAW_PRIZE)), 0, DbRow.Value(SQL_COLUMN_GROUPED_WITHDRAW_PRIZE))
        m_total_withdraw_prize += _value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_GROUPED_WITHDRAW_PRIZE).Value = GUI_FormatCurrency(_value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        _value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_GROUPED_WITHDRAW_TAX1)), 0, DbRow.Value(SQL_COLUMN_GROUPED_WITHDRAW_TAX1))
        m_total_withdraw_tax1 += _value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_GROUPED_WITHDRAW_TAX1).Value = GUI_FormatCurrency(_value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        _value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_GROUPED_WITHDRAW_TAX2)), 0, DbRow.Value(SQL_COLUMN_GROUPED_WITHDRAW_TAX2))
        m_total_withdraw_tax2 += _value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_GROUPED_WITHDRAW_TAX2).Value = GUI_FormatCurrency(_value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        '_value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_GROUPED_WITHDRAW_TAX3)), 0, DbRow.Value(SQL_COLUMN_GROUPED_WITHDRAW_TAX3))
        'm_total_withdraw_tax3 += _value
        'Me.Grid.Cell(RowIndex, GRID_COLUMN_GROUPED_WITHDRAW_TAX3).Value = GUI_FormatCurrency(_value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        _value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_GROUPED_WITHDRAW_PAID_AMOUNT)), 0, DbRow.Value(SQL_COLUMN_GROUPED_WITHDRAW_PAID_AMOUNT))
        m_total_withdraw_paid_amount += _value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_GROUPED_WITHDRAW_PAID_AMOUNT).Value = GUI_FormatCurrency(_value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        _value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_GROUPED_WITHDRAW_FINAL_BALANCE)), 0, DbRow.Value(SQL_COLUMN_GROUPED_WITHDRAW_FINAL_BALANCE))
        m_total_withdraw_final_balance += _value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_GROUPED_WITHDRAW_FINAL_BALANCE).Value = GUI_FormatCurrency(_value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        _value = DbRow.Value(SQL_COLUMN_GROUPED_WITHDRAW_TRANSACTIONS)
        m_total_transactions += _value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_GROUPED_WITHDRAW_TRANSACTIONS).Value = GUI_FormatNumber(_value, 0)

      End If
    End If

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.uc_dsl
  End Sub 'GUI_SetInitialFocus

  ' PURPOSE: Process button actions in order to branch to a child screen
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    If ButtonId = ENUM_BUTTON.BUTTON_FILTER_APPLY Then
      Call GUI_StyleSheet()
    End If

    Call MyBase.GUI_ButtonClick(ButtonId)

  End Sub ' GUI_ButtonClick

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_AUDITOR.GetString(257), m_init_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_AUDITOR.GetString(258), m_init_date_to)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(397), m_series)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(369), m_option)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(386), m_operation_type)

    PrintData.FilterHeaderWidth(2) = 2000

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_option = ""
    m_operation_type = ""
    m_init_date_from = ""
    m_init_date_to = ""

    ' Init Date From
    If Me.uc_dsl.FromDateSelected Then
      m_init_date_from = GUI_FormatDate(Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime), _
                                        ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    ' Init Date To
    If Me.uc_dsl.ToDateSelected Then
      m_init_date_to = GUI_FormatDate(Me.uc_dsl.ToDate.AddHours(Me.uc_dsl.ClosingTime), _
                                      ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    ' Option
    If opt_daily_detail.Checked Then
      m_option = opt_daily_detail.Text
    ElseIf opt_daily_grouped.Checked Then
      m_option = opt_daily_grouped.Text
    Else
      m_option = GLB_NLS_GUI_CONTROLS.GetString(408)
    End If

    ' Operation type
    If opt_sale_note.Checked Then
      m_operation_type = opt_sale_note.Text
      If Me.chk_charge_sale_note.Checked Then
        m_operation_type = m_operation_type & ": " & chk_charge_sale_note.Text
      End If
      If Me.chk_service_charge_sale_note.Checked Then
        If Me.chk_charge_sale_note.Checked Then
          m_operation_type = m_operation_type & ","
        Else
          m_operation_type = m_operation_type & ":"
        End If
        m_operation_type = m_operation_type & " " & chk_service_charge_sale_note.Text
      End If
      If WSI.Common.Misc.IsCommissionToCompanyBEnabled And Me.chk_card_commission.Checked Then
        If m_operation_type.Contains(":") Then
          m_operation_type = m_operation_type & "," & chk_card_commission.Text
        Else
          m_operation_type = m_operation_type & ":" & chk_card_commission.Text
        End If
      End If
      If WSI.Common.Misc.IsCardPlayerToCompanyB() And Me.chk_player_card_pay.Checked Then
        If m_operation_type.Contains(":") Then
          m_operation_type = m_operation_type & "," & chk_player_card_pay.Text
        Else
          m_operation_type = m_operation_type & ":" & chk_player_card_pay.Text
        End If
      End If
    ElseIf opt_charge.Checked Then
      m_operation_type = opt_charge.Text
    ElseIf opt_charge_cancel_sale_note.Checked Then
      If WSI.Common.Misc.IsCardPlayerToCompanyB() Then

        If Me.chk_cancel_charge_sale_note.Checked Then
          m_operation_type = opt_charge_cancel_sale_note.Text & ":" & Me.chk_cancel_charge_sale_note.Text
        End If
        If Me.chk_cancel_player_card_pay.Checked Then
          If m_operation_type.Length > 0 Then
            m_operation_type = m_operation_type & "," & chk_cancel_player_card_pay.Text
          Else
            m_operation_type = opt_charge_cancel_sale_note.Text & ":" & chk_cancel_player_card_pay.Text
          End If
        End If
      Else
        m_operation_type = opt_charge_cancel_sale_note.Text
      End If

    ElseIf opt_withdraw.Checked Then
      m_operation_type = opt_withdraw.Text
    ElseIf opt_entry.Checked Then
      m_operation_type = opt_entry.Text
    Else
      m_operation_type = GLB_NLS_GUI_CONTROLS.GetString(408)
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()

    Dim _mode As Integer
    Dim _sequence_visible As Boolean

    _sequence_visible = GeneralParam.GetBoolean("Cashier.Voucher", "Voucher.A.ShowSequence") OrElse GeneralParam.GetBoolean("Cashier.Voucher", "Voucher.B.ShowSequence")

    m_third_tax_is_active = GetThirdTaxIsActive()

    With Me.Grid

      If opt_daily_detail.Checked Then

        If Not Integer.TryParse(WSI.Common.Misc.ReadGeneralParams("Cashier.Voucher", "Mode"), _mode) Then
          _mode = 0
        End If

        If opt_sale_note.Checked OrElse opt_charge_cancel_sale_note.Checked Then

          Call .Init(GRID_NUM_COLUMNS_DETAIL, GRID_NUM_HEADER_ROWS)

          .Column(GRID_COLUMN_INDEX).Header.Text = " "
          .Column(GRID_COLUMN_INDEX).Width = 200
          .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
          .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

          Select Case _mode
            Case 1
              .Column(GRID_COLUMN_DETAIL_RECEIPT).Header.Text = ""
              .Column(GRID_COLUMN_DETAIL_RECEIPT).Width = 0
            Case Else
              .Column(GRID_COLUMN_DETAIL_RECEIPT).Header.Text = GLB_NLS_GUI_INVOICING.GetString(167)
              .Column(GRID_COLUMN_DETAIL_RECEIPT).Width = 1000
          End Select
          .Column(GRID_COLUMN_DETAIL_RECEIPT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_DETAIL_FOLIO).Header.Text = GLB_NLS_GUI_INVOICING.GetString(389)
          .Column(GRID_COLUMN_DETAIL_FOLIO).Width = If(_sequence_visible, 1500, 0)
          .Column(GRID_COLUMN_DETAIL_FOLIO).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_DETAIL_DATETIME).Header.Text = GLB_NLS_GUI_INVOICING.GetString(390)
          .Column(GRID_COLUMN_DETAIL_DATETIME).Width = 1800
          .Column(GRID_COLUMN_DETAIL_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

          .Column(GRID_COLUMN_DETAIL_AMOUNT).Header.Text = GLB_NLS_GUI_INVOICING.GetString(391)
          .Column(GRID_COLUMN_DETAIL_AMOUNT).Width = 1400
          .Column(GRID_COLUMN_DETAIL_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_DETAIL_TAX).Header.Text = GetCashierData("Split.B.Tax.Name")
          If m_firm_b_enabled Then
            .Column(GRID_COLUMN_DETAIL_TAX).Width = 1100
          Else
            .Column(GRID_COLUMN_DETAIL_TAX).Width = 0
          End If
          .Column(GRID_COLUMN_DETAIL_TAX).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_DETAIL_TOTAL).Header.Text = GLB_NLS_GUI_INVOICING.GetString(488)
          .Column(GRID_COLUMN_DETAIL_TOTAL).Width = 1400
          .Column(GRID_COLUMN_DETAIL_TOTAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_DETAIL_CONCEPT).Header.Text = GLB_NLS_GUI_INVOICING.GetString(348)
          .Column(GRID_COLUMN_DETAIL_CONCEPT).Width = 2950
          .Column(GRID_COLUMN_DETAIL_CONCEPT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

          .Column(GRID_COLUMN_DETAIL_CASHIER_USER).Header.Text = GLB_NLS_GUI_INVOICING.GetString(210)
          .Column(GRID_COLUMN_DETAIL_CASHIER_USER).Width = 1500
          .Column(GRID_COLUMN_DETAIL_CASHIER_USER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

          .Column(GRID_COLUMN_DETAIL_CASHIER_NAME).Header.Text = GLB_NLS_GUI_INVOICING.GetString(393)
          .Column(GRID_COLUMN_DETAIL_CASHIER_NAME).Width = 3000
          .Column(GRID_COLUMN_DETAIL_CASHIER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

          If opt_sale_note.Checked Then
            _mode = 1
          End If

          Select Case _mode
            Case 1
              .Column(GRID_COLUMN_DETAIL_CANCELLED_FOLIO).Header.Text = ""
              .Column(GRID_COLUMN_DETAIL_CANCELLED_FOLIO).Width = 0
            Case Else
              .Column(GRID_COLUMN_DETAIL_CANCELLED_FOLIO).Header.Text = GLB_NLS_GUI_INVOICING.GetString(168)
              .Column(GRID_COLUMN_DETAIL_CANCELLED_FOLIO).Width = If(_sequence_visible, 1500, 0)
          End Select
          .Column(GRID_COLUMN_DETAIL_CANCELLED_FOLIO).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ElseIf opt_charge.Checked Then

          Call .Init(GRID_NUM_COLUMNS_DETAIL_CHARGE, GRID_NUM_HEADER_ROWS)

          .Column(GRID_COLUMN_INDEX).Header.Text = " "
          .Column(GRID_COLUMN_INDEX).Width = 200
          .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
          .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

          Select Case _mode
            Case 1
              .Column(GRID_COLUMN_DETAIL_CHARGE_RECEIPT).Header.Text = ""
              .Column(GRID_COLUMN_DETAIL_CHARGE_RECEIPT).Width = 0
            Case Else
              .Column(GRID_COLUMN_DETAIL_CHARGE_RECEIPT).Header.Text = GLB_NLS_GUI_INVOICING.GetString(167)
              .Column(GRID_COLUMN_DETAIL_CHARGE_RECEIPT).Width = 1000
          End Select
          .Column(GRID_COLUMN_DETAIL_CHARGE_RECEIPT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_DETAIL_CHARGE_FOLIO).Header.Text = GLB_NLS_GUI_INVOICING.GetString(389)
          .Column(GRID_COLUMN_DETAIL_CHARGE_FOLIO).Width = If(_sequence_visible, 1500, 0)
          .Column(GRID_COLUMN_DETAIL_CHARGE_FOLIO).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_DETAIL_CHARGE_DATETIME).Header.Text = GLB_NLS_GUI_INVOICING.GetString(390)
          .Column(GRID_COLUMN_DETAIL_CHARGE_DATETIME).Width = 1800
          .Column(GRID_COLUMN_DETAIL_CHARGE_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

          .Column(GRID_COLUMN_DETAIL_CHARGE_DEPOSIT).Header.Text = GLB_NLS_GUI_INVOICING.GetString(395)
          .Column(GRID_COLUMN_DETAIL_CHARGE_DEPOSIT).Width = 1400
          .Column(GRID_COLUMN_DETAIL_CHARGE_DEPOSIT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_DETAIL_CHARGE_PROMOTION).Header.Text = GLB_NLS_GUI_INVOICING.GetString(311)
          .Column(GRID_COLUMN_DETAIL_CHARGE_PROMOTION).Width = 1400
          .Column(GRID_COLUMN_DETAIL_CHARGE_PROMOTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_DETAIL_CHARGE_TOTAL_DEPOSIT).Header.Text = GLB_NLS_GUI_INVOICING.GetString(363)
          .Column(GRID_COLUMN_DETAIL_CHARGE_TOTAL_DEPOSIT).Width = 1400
          .Column(GRID_COLUMN_DETAIL_CHARGE_TOTAL_DEPOSIT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_DETAIL_CHARGE_ACCOUNT_ID).Header.Text = GLB_NLS_GUI_INVOICING.GetString(230)
          .Column(GRID_COLUMN_DETAIL_CHARGE_ACCOUNT_ID).Width = 1000
          .Column(GRID_COLUMN_DETAIL_CHARGE_ACCOUNT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_DETAIL_CHARGE_CLIENT_NAME).Header.Text = GLB_NLS_GUI_INVOICING.GetString(394)
          .Column(GRID_COLUMN_DETAIL_CHARGE_CLIENT_NAME).Width = 3000
          .Column(GRID_COLUMN_DETAIL_CHARGE_CLIENT_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

          .Column(GRID_COLUMN_DETAIL_CHARGE_CASHIER_USER).Header.Text = GLB_NLS_GUI_INVOICING.GetString(210)
          .Column(GRID_COLUMN_DETAIL_CHARGE_CASHIER_USER).Width = 1500
          .Column(GRID_COLUMN_DETAIL_CHARGE_CASHIER_USER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

          .Column(GRID_COLUMN_DETAIL_CHARGE_CASHIER_NAME).Header.Text = GLB_NLS_GUI_INVOICING.GetString(393)
          .Column(GRID_COLUMN_DETAIL_CHARGE_CASHIER_NAME).Width = 1600
          .Column(GRID_COLUMN_DETAIL_CHARGE_CASHIER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

        ElseIf opt_withdraw.Checked Then

          Call .Init(GRID_NUM_COLUMNS_DETAIL_WITHDRAW, GRID_NUM_HEADER_ROWS)

          .Column(GRID_COLUMN_INDEX).Header.Text = " "
          .Column(GRID_COLUMN_INDEX).Width = 200
          .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
          .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

          Select Case _mode
            Case 1
              .Column(GRID_COLUMN_DETAIL_WITHDRAW_RECEIPT).Header.Text = ""
              .Column(GRID_COLUMN_DETAIL_WITHDRAW_RECEIPT).Width = 0
            Case Else
              .Column(GRID_COLUMN_DETAIL_WITHDRAW_RECEIPT).Header.Text = GLB_NLS_GUI_INVOICING.GetString(167)
              .Column(GRID_COLUMN_DETAIL_WITHDRAW_RECEIPT).Width = 1000
          End Select
          .Column(GRID_COLUMN_DETAIL_WITHDRAW_RECEIPT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_DETAIL_WITHDRAW_FOLIO).Header.Text = GLB_NLS_GUI_INVOICING.GetString(389)
          .Column(GRID_COLUMN_DETAIL_WITHDRAW_FOLIO).Width = If(_sequence_visible, 1500, 0)
          .Column(GRID_COLUMN_DETAIL_WITHDRAW_FOLIO).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_DETAIL_WITHDRAW_DATETIME).Header.Text = GLB_NLS_GUI_INVOICING.GetString(390)
          .Column(GRID_COLUMN_DETAIL_WITHDRAW_DATETIME).Width = 1800
          .Column(GRID_COLUMN_DETAIL_WITHDRAW_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

          .Column(GRID_COLUMN_DETAIL_WITHDRAW_INITIAL_BALANCE).Header.Text = GLB_NLS_GUI_INVOICING.GetString(396)
          .Column(GRID_COLUMN_DETAIL_WITHDRAW_INITIAL_BALANCE).Width = 1400
          .Column(GRID_COLUMN_DETAIL_WITHDRAW_INITIAL_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_DETAIL_WITHDRAW_WITHDRAW).Header.Text = GLB_NLS_GUI_INVOICING.GetString(489)
          .Column(GRID_COLUMN_DETAIL_WITHDRAW_WITHDRAW).Width = 1400
          .Column(GRID_COLUMN_DETAIL_WITHDRAW_WITHDRAW).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_DETAIL_WITHDRAW_DEVOLUTION).Header.Text = GLB_NLS_GUI_INVOICING.GetString(31)
          .Column(GRID_COLUMN_DETAIL_WITHDRAW_DEVOLUTION).Width = 1400
          .Column(GRID_COLUMN_DETAIL_WITHDRAW_DEVOLUTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_DETAIL_WITHDRAW_PRIZE).Header.Text = GLB_NLS_GUI_INVOICING.GetString(33)
          .Column(GRID_COLUMN_DETAIL_WITHDRAW_PRIZE).Width = 1400
          .Column(GRID_COLUMN_DETAIL_WITHDRAW_PRIZE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_DETAIL_WITHDRAW_TAX1).Header.Text = GetCashierData("Tax.OnPrize.1.Name")
          .Column(GRID_COLUMN_DETAIL_WITHDRAW_TAX1).Width = 1600
          .Column(GRID_COLUMN_DETAIL_WITHDRAW_TAX1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_DETAIL_WITHDRAW_TAX2).Header.Text = GetCashierData("Tax.OnPrize.2.Name")
          .Column(GRID_COLUMN_DETAIL_WITHDRAW_TAX2).Width = 1600
          .Column(GRID_COLUMN_DETAIL_WITHDRAW_TAX2).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          '.Column(GRID_COLUMN_DETAIL_WITHDRAW_TAX3).Header.Text = GetCashierData("Tax.OnPrize.3.Name")
          'SetThirdColumnWidth(GRID_COLUMN_DETAIL_WITHDRAW_TAX3, m_third_tax_is_active)
          '.Column(GRID_COLUMN_DETAIL_WITHDRAW_TAX3).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_DETAIL_WITHDRAW_PAID_AMOUNT).Header.Text = GLB_NLS_GUI_INVOICING.GetString(399)
          .Column(GRID_COLUMN_DETAIL_WITHDRAW_PAID_AMOUNT).Width = 1600
          .Column(GRID_COLUMN_DETAIL_WITHDRAW_PAID_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_DETAIL_WITHDRAW_FINAL_BALANCE).Header.Text = GLB_NLS_GUI_INVOICING.GetString(400)
          .Column(GRID_COLUMN_DETAIL_WITHDRAW_FINAL_BALANCE).Width = 1400
          .Column(GRID_COLUMN_DETAIL_WITHDRAW_FINAL_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_DETAIL_WITHDRAW_ACCOUNT_ID).Header.Text = GLB_NLS_GUI_INVOICING.GetString(230)
          .Column(GRID_COLUMN_DETAIL_WITHDRAW_ACCOUNT_ID).Width = 1000
          .Column(GRID_COLUMN_DETAIL_WITHDRAW_ACCOUNT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_DETAIL_WITHDRAW_CLIENT_NAME).Header.Text = GLB_NLS_GUI_INVOICING.GetString(394)
          .Column(GRID_COLUMN_DETAIL_WITHDRAW_CLIENT_NAME).Width = 3000
          .Column(GRID_COLUMN_DETAIL_WITHDRAW_CLIENT_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

          .Column(GRID_COLUMN_DETAIL_WITHDRAW_CASHIER_USER).Header.Text = GLB_NLS_GUI_INVOICING.GetString(210)
          .Column(GRID_COLUMN_DETAIL_WITHDRAW_CASHIER_USER).Width = 1500
          .Column(GRID_COLUMN_DETAIL_WITHDRAW_CASHIER_USER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

          .Column(GRID_COLUMN_DETAIL_WITHDRAW_CASHIER_NAME).Header.Text = GLB_NLS_GUI_INVOICING.GetString(393)
          .Column(GRID_COLUMN_DETAIL_WITHDRAW_CASHIER_NAME).Width = 1600
          .Column(GRID_COLUMN_DETAIL_WITHDRAW_CASHIER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

        ElseIf opt_entry.Checked Then

          Call .Init(GRID_NUM_COLUMNS_DETAIL_CHARGE, GRID_NUM_HEADER_ROWS)

          .Column(GRID_COLUMN_INDEX).Header.Text = " "
          .Column(GRID_COLUMN_INDEX).Width = 200
          .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
          .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

          Select Case _mode
            Case 1
              .Column(GRID_COLUMN_DETAIL_CHARGE_RECEIPT).Header.Text = ""
              .Column(GRID_COLUMN_DETAIL_CHARGE_RECEIPT).Width = 0
            Case Else
              .Column(GRID_COLUMN_DETAIL_CHARGE_RECEIPT).Header.Text = GLB_NLS_GUI_INVOICING.GetString(167)
              .Column(GRID_COLUMN_DETAIL_CHARGE_RECEIPT).Width = 1000
          End Select
          .Column(GRID_COLUMN_DETAIL_CHARGE_RECEIPT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_DETAIL_CHARGE_FOLIO).Header.Text = GLB_NLS_GUI_INVOICING.GetString(389)
          .Column(GRID_COLUMN_DETAIL_CHARGE_FOLIO).Width = If(_sequence_visible, 1500, 0)
          .Column(GRID_COLUMN_DETAIL_CHARGE_FOLIO).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_DETAIL_CHARGE_DATETIME).Header.Text = GLB_NLS_GUI_INVOICING.GetString(390)
          .Column(GRID_COLUMN_DETAIL_CHARGE_DATETIME).Width = 1800
          .Column(GRID_COLUMN_DETAIL_CHARGE_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

          .Column(GRID_COLUMN_DETAIL_CHARGE_DEPOSIT).Header.Text = GLB_NLS_GUI_INVOICING.GetString(391)
          .Column(GRID_COLUMN_DETAIL_CHARGE_DEPOSIT).Width = 1400
          .Column(GRID_COLUMN_DETAIL_CHARGE_DEPOSIT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_DETAIL_CHARGE_PROMOTION).Header.Text = GLB_NLS_GUI_INVOICING.GetString(311)
          .Column(GRID_COLUMN_DETAIL_CHARGE_PROMOTION).Width = 0
          .Column(GRID_COLUMN_DETAIL_CHARGE_PROMOTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_DETAIL_CHARGE_TOTAL_DEPOSIT).Header.Text = GLB_NLS_GUI_INVOICING.GetString(363)
          .Column(GRID_COLUMN_DETAIL_CHARGE_TOTAL_DEPOSIT).Width = 0
          .Column(GRID_COLUMN_DETAIL_CHARGE_TOTAL_DEPOSIT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_DETAIL_CHARGE_ACCOUNT_ID).Header.Text = GLB_NLS_GUI_INVOICING.GetString(230)
          .Column(GRID_COLUMN_DETAIL_CHARGE_ACCOUNT_ID).Width = 0
          .Column(GRID_COLUMN_DETAIL_CHARGE_ACCOUNT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_DETAIL_CHARGE_CLIENT_NAME).Header.Text = GLB_NLS_GUI_INVOICING.GetString(394)
          .Column(GRID_COLUMN_DETAIL_CHARGE_CLIENT_NAME).Width = 0
          .Column(GRID_COLUMN_DETAIL_CHARGE_CLIENT_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

          .Column(GRID_COLUMN_DETAIL_CHARGE_CASHIER_USER).Header.Text = GLB_NLS_GUI_INVOICING.GetString(210)
          .Column(GRID_COLUMN_DETAIL_CHARGE_CASHIER_USER).Width = 1500
          .Column(GRID_COLUMN_DETAIL_CHARGE_CASHIER_USER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

          .Column(GRID_COLUMN_DETAIL_CHARGE_CASHIER_NAME).Header.Text = GLB_NLS_GUI_INVOICING.GetString(393)
          .Column(GRID_COLUMN_DETAIL_CHARGE_CASHIER_NAME).Width = 1600
          .Column(GRID_COLUMN_DETAIL_CHARGE_CASHIER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

        End If
      ElseIf opt_daily_grouped.Checked Then
        If opt_sale_note.Checked OrElse opt_charge_cancel_sale_note.Checked Then

          Call .Init(GRID_NUM_COLUMNS_GROUPED, GRID_NUM_HEADER_ROWS)

          .Column(GRID_COLUMN_INDEX).Header.Text = " "
          .Column(GRID_COLUMN_INDEX).Width = 200
          .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
          .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

          .Column(GRID_COLUMN_GROUPED_DAY).Header.Text = GLB_NLS_GUI_INVOICING.GetString(401)
          .Column(GRID_COLUMN_GROUPED_DAY).Width = 1200
          .Column(GRID_COLUMN_GROUPED_DAY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

          .Column(GRID_COLUMN_GROUPED_CONCEPT).Header.Text = GLB_NLS_GUI_INVOICING.GetString(348)
          .Column(GRID_COLUMN_GROUPED_CONCEPT).Width = 5000
          .Column(GRID_COLUMN_GROUPED_CONCEPT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

          .Column(GRID_COLUMN_GROUPED_AMOUNT).Header.Text = GLB_NLS_GUI_INVOICING.GetString(391)
          .Column(GRID_COLUMN_GROUPED_AMOUNT).Width = 1500
          .Column(GRID_COLUMN_GROUPED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_GROUPED_TAX).Header.Text = GetCashierData("Split.B.Tax.Name")
          If m_firm_b_enabled Then
            .Column(GRID_COLUMN_GROUPED_TAX).Width = 1500
          Else
            .Column(GRID_COLUMN_GROUPED_TAX).Width = 0
          End If
          .Column(GRID_COLUMN_GROUPED_TAX).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_GROUPED_TOTAL).Header.Text = GLB_NLS_GUI_INVOICING.GetString(488)
          .Column(GRID_COLUMN_GROUPED_TOTAL).Width = 1500
          .Column(GRID_COLUMN_GROUPED_TOTAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_GROUPED_TRANSACTIONS).Header.Text = GLB_NLS_GUI_INVOICING.GetString(402)
          .Column(GRID_COLUMN_GROUPED_TRANSACTIONS).Width = 1500
          .Column(GRID_COLUMN_GROUPED_TRANSACTIONS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ElseIf opt_charge.Checked Then

          Call .Init(GRID_NUM_COLUMNS_GROUPED_CHARGE, GRID_NUM_HEADER_ROWS)

          .Column(GRID_COLUMN_INDEX).Header.Text = " "
          .Column(GRID_COLUMN_INDEX).Width = 200
          .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
          .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

          .Column(GRID_COLUMN_GROUPED_CHARGE_DAY).Header.Text = GLB_NLS_GUI_INVOICING.GetString(401)
          .Column(GRID_COLUMN_GROUPED_CHARGE_DAY).Width = 1200
          .Column(GRID_COLUMN_GROUPED_CHARGE_DAY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

          .Column(GRID_COLUMN_GROUPED_CHARGE_DEPOSIT).Header.Text = GLB_NLS_GUI_INVOICING.GetString(395)
          .Column(GRID_COLUMN_GROUPED_CHARGE_DEPOSIT).Width = 1400
          .Column(GRID_COLUMN_GROUPED_CHARGE_DEPOSIT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_GROUPED_CHARGE_PROMOTION).Header.Text = GLB_NLS_GUI_INVOICING.GetString(311)
          .Column(GRID_COLUMN_GROUPED_CHARGE_PROMOTION).Width = 1400
          .Column(GRID_COLUMN_GROUPED_CHARGE_PROMOTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_GROUPED_CHARGE_TOTAL_DEPOSIT).Header.Text = GLB_NLS_GUI_INVOICING.GetString(363)
          .Column(GRID_COLUMN_GROUPED_CHARGE_TOTAL_DEPOSIT).Width = 1400
          .Column(GRID_COLUMN_GROUPED_CHARGE_TOTAL_DEPOSIT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_GROUPED_CHARGE_TRANSACTIONS).Header.Text = GLB_NLS_GUI_INVOICING.GetString(402)
          .Column(GRID_COLUMN_GROUPED_CHARGE_TRANSACTIONS).Width = 1400
          .Column(GRID_COLUMN_GROUPED_CHARGE_TRANSACTIONS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ElseIf opt_withdraw.Checked Then

          Call .Init(GRID_NUM_COLUMNS_GROUPED_WITHDRAW, GRID_NUM_HEADER_ROWS)

          .Column(GRID_COLUMN_INDEX).Header.Text = " "
          .Column(GRID_COLUMN_INDEX).Width = 200
          .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
          .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

          .Column(GRID_COLUMN_GROUPED_WITHDRAW_DAY).Header.Text = GLB_NLS_GUI_INVOICING.GetString(401)
          .Column(GRID_COLUMN_GROUPED_WITHDRAW_DAY).Width = 1200
          .Column(GRID_COLUMN_GROUPED_WITHDRAW_DAY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

          .Column(GRID_COLUMN_GROUPED_WITHDRAW_INITIAL_BALANCE).Header.Text = GLB_NLS_GUI_INVOICING.GetString(396)
          .Column(GRID_COLUMN_GROUPED_WITHDRAW_INITIAL_BALANCE).Width = 1400
          .Column(GRID_COLUMN_GROUPED_WITHDRAW_INITIAL_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_GROUPED_WITHDRAW_WITHDRAW).Header.Text = GLB_NLS_GUI_INVOICING.GetString(489)
          .Column(GRID_COLUMN_GROUPED_WITHDRAW_WITHDRAW).Width = 1400
          .Column(GRID_COLUMN_GROUPED_WITHDRAW_WITHDRAW).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_GROUPED_WITHDRAW_DEVOLUTION).Header.Text = GLB_NLS_GUI_INVOICING.GetString(31)
          .Column(GRID_COLUMN_GROUPED_WITHDRAW_DEVOLUTION).Width = 1400
          .Column(GRID_COLUMN_GROUPED_WITHDRAW_DEVOLUTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_GROUPED_WITHDRAW_PRIZE).Header.Text = GLB_NLS_GUI_INVOICING.GetString(33)
          .Column(GRID_COLUMN_GROUPED_WITHDRAW_PRIZE).Width = 1400
          .Column(GRID_COLUMN_GROUPED_WITHDRAW_PRIZE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_GROUPED_WITHDRAW_TAX1).Header.Text = GetCashierData("Tax.OnPrize.1.Name")
          .Column(GRID_COLUMN_GROUPED_WITHDRAW_TAX1).Width = 1600
          .Column(GRID_COLUMN_GROUPED_WITHDRAW_TAX1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_GROUPED_WITHDRAW_TAX2).Header.Text = GetCashierData("Tax.OnPrize.2.Name")
          .Column(GRID_COLUMN_GROUPED_WITHDRAW_TAX2).Width = 1600
          .Column(GRID_COLUMN_GROUPED_WITHDRAW_TAX2).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          '.Column(GRID_COLUMN_GROUPED_WITHDRAW_TAX3).Header.Text = GetCashierData("Tax.OnPrize.3.Name")
          'SetThirdColumnWidth(GRID_COLUMN_GROUPED_WITHDRAW_TAX3, m_third_tax_is_active)
          '.Column(GRID_COLUMN_GROUPED_WITHDRAW_TAX3).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_GROUPED_WITHDRAW_PAID_AMOUNT).Header.Text = GLB_NLS_GUI_INVOICING.GetString(399)
          .Column(GRID_COLUMN_GROUPED_WITHDRAW_PAID_AMOUNT).Width = 1600
          .Column(GRID_COLUMN_GROUPED_WITHDRAW_PAID_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_GROUPED_WITHDRAW_FINAL_BALANCE).Header.Text = GLB_NLS_GUI_INVOICING.GetString(400)
          .Column(GRID_COLUMN_GROUPED_WITHDRAW_FINAL_BALANCE).Width = 1400
          .Column(GRID_COLUMN_GROUPED_WITHDRAW_FINAL_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_GROUPED_WITHDRAW_TRANSACTIONS).Header.Text = GLB_NLS_GUI_INVOICING.GetString(402)
          .Column(GRID_COLUMN_GROUPED_WITHDRAW_TRANSACTIONS).Width = 1400
          .Column(GRID_COLUMN_GROUPED_WITHDRAW_TRANSACTIONS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ElseIf opt_entry.Checked Then

          Call .Init(GRID_NUM_COLUMNS_GROUPED_CHARGE, GRID_NUM_HEADER_ROWS)

          .Column(GRID_COLUMN_INDEX).Header.Text = " "
          .Column(GRID_COLUMN_INDEX).Width = 200
          .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
          .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

          .Column(GRID_COLUMN_GROUPED_CHARGE_DAY).Header.Text = GLB_NLS_GUI_INVOICING.GetString(401)
          .Column(GRID_COLUMN_GROUPED_CHARGE_DAY).Width = 1200
          .Column(GRID_COLUMN_GROUPED_CHARGE_DAY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

          .Column(GRID_COLUMN_GROUPED_CHARGE_DEPOSIT).Header.Text = GLB_NLS_GUI_INVOICING.GetString(391)
          .Column(GRID_COLUMN_GROUPED_CHARGE_DEPOSIT).Width = 1400
          .Column(GRID_COLUMN_GROUPED_CHARGE_DEPOSIT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_GROUPED_CHARGE_PROMOTION).Header.Text = GLB_NLS_GUI_INVOICING.GetString(311)
          .Column(GRID_COLUMN_GROUPED_CHARGE_PROMOTION).Width = 0
          .Column(GRID_COLUMN_GROUPED_CHARGE_PROMOTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_GROUPED_CHARGE_TOTAL_DEPOSIT).Header.Text = GLB_NLS_GUI_INVOICING.GetString(363)
          .Column(GRID_COLUMN_GROUPED_CHARGE_TOTAL_DEPOSIT).Width = 0
          .Column(GRID_COLUMN_GROUPED_CHARGE_TOTAL_DEPOSIT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

          .Column(GRID_COLUMN_GROUPED_CHARGE_TRANSACTIONS).Header.Text = GLB_NLS_GUI_INVOICING.GetString(402)
          .Column(GRID_COLUMN_GROUPED_CHARGE_TRANSACTIONS).Width = 1500
          .Column(GRID_COLUMN_GROUPED_CHARGE_TRANSACTIONS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
        End If
      End If
    End With

  End Sub 'GUI_StyleSheet

  ' PURPOSE: Set initial visualization config
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub InitialLayoutConfiguration()

    ' Enabled or disabled all options
    Me.lbl_business_b.Visible = m_firm_b_enabled
    Me.opt_sale_note.Visible = m_firm_b_enabled
    Me.chk_charge_sale_note.Visible = m_firm_b_enabled
    Me.opt_charge_cancel_sale_note.Visible = m_firm_b_enabled
    Me.chk_service_charge_sale_note.Visible = m_firm_b_enabled And GeneralParam.GetBoolean("Cashier.ServiceCharge", "Enabled")
    Me.opt_entry.Visible = Not String.IsNullOrEmpty(GeneralParam.GetString("Cashier.Voucher", "Concept.Name.1", "")) And Cage.IsCageEnabled
    Me.chk_card_commission.Visible = m_firm_b_enabled And GeneralParam.GetBoolean("Cashier.PaymentMethod", "CommissionToCompanyB.Enabled")
    Me.chk_player_card_pay.Visible = m_firm_b_enabled And WSI.Common.Misc.IsCardPlayerToCompanyB()
    Me.chk_cancel_player_card_pay.Visible = m_firm_b_enabled And WSI.Common.Misc.IsCardPlayerToCompanyB()
    Me.chk_cancel_charge_sale_note.Visible = m_firm_b_enabled And WSI.Common.Misc.IsCardPlayerToCompanyB()

    ' Company B disabled
    If Not m_firm_b_enabled Then
      Me.gb_operation_type.Width = Me.lbl_business_b.Width

      If Not Me.opt_entry.Visible Then
        Me.gb_operation_type.Height = Me.opt_withdraw.Bottom + 5
      Else
        Me.gb_operation_type.Height = Me.opt_entry.Bottom + 5
      End If

      Me.panel_filter.AutoSize = True
    Else
      ' Company B enabled

      ' Service charge disabled
      If Not Me.chk_service_charge_sale_note.Visible Then
        Me.chk_card_commission.Top = Me.chk_charge_sale_note.Bottom
        Me.chk_player_card_pay.Top = Me.chk_card_commission.Bottom
        Me.opt_charge_cancel_sale_note.Top = Me.chk_player_card_pay.Bottom

        If chk_cancel_player_card_pay.Visible Then
          Me.chk_cancel_charge_sale_note.Top = Me.opt_charge_cancel_sale_note.Bottom
          Me.chk_cancel_player_card_pay.Top = Me.chk_cancel_charge_sale_note.Bottom
          Me.gb_operation_type.Height = Me.chk_cancel_player_card_pay.Bottom + 5
        Else
          Me.gb_operation_type.Height = Me.opt_charge_cancel_sale_note.Bottom + 5
        End If
      End If

      ' Card comission disabled
      If Not Me.chk_card_commission.Visible Then
        If Me.chk_service_charge_sale_note.Visible Then
          Me.chk_player_card_pay.Top = Me.chk_service_charge_sale_note.Bottom
        Else
          Me.chk_player_card_pay.Top = Me.chk_charge_sale_note.Bottom
        End If

        Me.opt_charge_cancel_sale_note.Top = Me.chk_player_card_pay.Bottom

        If chk_cancel_player_card_pay.Visible Then
          Me.chk_cancel_charge_sale_note.Top = Me.opt_charge_cancel_sale_note.Bottom
          Me.chk_cancel_player_card_pay.Top = Me.chk_cancel_charge_sale_note.Bottom
          Me.gb_operation_type.Height = Me.chk_cancel_player_card_pay.Bottom + 5
        Else
          Me.gb_operation_type.Height = Me.opt_charge_cancel_sale_note.Bottom + 5
        End If
      End If

      ' Pay to company B player card disabled
      If Not Me.chk_player_card_pay.Visible Then
        If Me.chk_card_commission.Visible Then
          Me.opt_charge_cancel_sale_note.Top = Me.chk_card_commission.Bottom
        ElseIf Me.chk_service_charge_sale_note.Visible Then
          Me.opt_charge_cancel_sale_note.Top = Me.chk_service_charge_sale_note.Bottom
        Else
          Me.opt_charge_cancel_sale_note.Top = Me.chk_charge_sale_note.Bottom
        End If

        If chk_cancel_player_card_pay.Visible Then
          Me.chk_cancel_charge_sale_note.Top = Me.opt_charge_cancel_sale_note.Bottom
          Me.chk_cancel_player_card_pay.Top = Me.chk_cancel_charge_sale_note.Bottom
          Me.gb_operation_type.Height = Me.chk_cancel_player_card_pay.Bottom + 5
        Else
          Me.gb_operation_type.Height = Me.opt_charge_cancel_sale_note.Bottom + 5
        End If
      End If

      If Me.chk_cancel_player_card_pay.Visible Then
        Me.chk_cancel_charge_sale_note.Top = Me.opt_charge_cancel_sale_note.Bottom
        Me.chk_cancel_player_card_pay.Top = Me.chk_cancel_charge_sale_note.Bottom
      End If

      ' Adjust Groupbox Height to cage concepts if all concepts in company B are disabled
      If Me.gb_operation_type.Height < Me.opt_entry.Bottom Then
        Me.gb_operation_type.Height = Me.opt_entry.Bottom + 5
      End If
    End If

    Me.panel_filter.AutoSize = True

  End Sub

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Dim _closing_time As Integer

    Me.opt_daily_detail.Checked = True

    If m_firm_b_enabled Then
      Me.opt_sale_note.Checked = True
      Me.chk_charge_sale_note.Checked = True
      Me.chk_service_charge_sale_note.Checked = True
      Me.chk_card_commission.Checked = True
      Me.chk_player_card_pay.Checked = True
      Me.chk_cancel_player_card_pay.Checked = True
      Me.chk_cancel_charge_sale_note.Checked = True
    Else
      Me.opt_withdraw.Checked = True
    End If

    Me.lbl_series.Text = GLB_NLS_GUI_INVOICING.GetString(397) & ":"

    _closing_time = GetDefaultClosingTime()
    Me.uc_dsl.FromDate = New DateTime(Now.Year, Now.Month, 1, _closing_time, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.ToDate = Me.uc_dsl.FromDate.AddMonths(1)
    Me.uc_dsl.ToDateSelected = True
    Me.uc_dsl.ClosingTime = _closing_time

  End Sub ' SetDefaultValues

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the
  '          main SQL Query.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String
    Dim _list_of_cv_types As List(Of Integer)

    Dim _str_aux As String
    Dim _list_if_filters As List(Of String)

    _list_if_filters = New List(Of String)

    _list_if_filters.Add("(CV_SEQUENCE IS NOT NULL)")

    ' Filter INIT Dates
    If Me.uc_dsl.FromDateSelected Then
      _list_if_filters.Add("(CV_DATETIME >= " & GUI_FormatDateDB(uc_dsl.FromDate.AddHours(uc_dsl.ClosingTime)) & ")")
    End If

    If Me.uc_dsl.ToDateSelected Then
      _list_if_filters.Add("(CV_DATETIME < " & GUI_FormatDateDB(uc_dsl.ToDate.AddHours(uc_dsl.ClosingTime)) & ")")
    End If

    ' Filter Operation type
    If Me.opt_charge.Checked Then
      _list_if_filters.Add("(CV_TYPE = " & WSI.Common.CashierVoucherType.CashIn_A & ")")
    ElseIf Me.opt_withdraw.Checked Then
      _list_if_filters.Add("(CV_TYPE = " & WSI.Common.CashierVoucherType.CashOut_A & ")")
    ElseIf Me.opt_entry.Checked Then
      _list_if_filters.Add("(CV_TYPE >= " & WSI.Common.CashierVoucherType.CageConcepts & " AND CV_TYPE <= " & WSI.Common.CashierVoucherType.CageConcepts_Last & ")")
    ElseIf Me.opt_sale_note.Checked Then
      _list_of_cv_types = GetSalesNoteListOfCVTypesToIncludeInDetailReport()

      _str_aux = GetWhereForListOfCVTypes(_list_of_cv_types)
      If (Not String.IsNullOrEmpty(_str_aux)) Then
        _list_if_filters.Add(_str_aux)
      End If
    ElseIf Me.opt_charge_cancel_sale_note.Checked Then

      If Not WSI.Common.Misc.IsCardPlayerToCompanyB() Then
        _list_if_filters.Add("(CV_TYPE = " & WSI.Common.CashierVoucherType.Cancel_B & ")")
      Else
        If Me.chk_cancel_charge_sale_note.Checked And Me.chk_cancel_player_card_pay.Checked Then
          _list_if_filters.Add("(CV_TYPE IN (" & WSI.Common.CashierVoucherType.Cancel_B & ", " & WSI.Common.CashierVoucherType.CardDepositOut_B & "))")
        ElseIf Me.chk_cancel_player_card_pay.Checked Then
          _list_if_filters.Add("(CV_TYPE = " & WSI.Common.CashierVoucherType.CardDepositOut_B & ")")
        ElseIf Me.chk_cancel_charge_sale_note.Checked Then
          _list_if_filters.Add("(CV_TYPE = " & WSI.Common.CashierVoucherType.Cancel_B & ")")
        End If
      End If
    End If

    ' Final processing
    If (_list_if_filters.Count > 0) Then
      _str_aux = String.Join(" AND ", _list_if_filters.ToArray())
      Return " WHERE " & _str_aux
    End If

    'Does not have filters
    Return String.Empty
  End Function ' GetSqlWhere

  ''' <summary>
  ''' Get list of CV types for Sales Notes
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetSalesNoteListOfCVTypesToIncludeInDetailReport() As List(Of Integer)
    Dim _list_of_cv_types As List(Of Integer)
    _list_of_cv_types = New List(Of Integer)

    If Me.chk_charge_sale_note.Checked Then
      _list_of_cv_types.Add(WSI.Common.CashierVoucherType.CashIn_B)
    End If

    If Me.chk_service_charge_sale_note.Checked Then
      _list_of_cv_types.Add(WSI.Common.CashierVoucherType.ServiceCharge_B)
    End If

    If WSI.Common.Misc.IsCommissionToCompanyBEnabled Then
      If Me.chk_card_commission.Checked Then
        _list_of_cv_types.Add(WSI.Common.CashierVoucherType.Commission_B)
        _list_of_cv_types.Add(WSI.Common.CashierVoucherType.CheckCommissionB)
        _list_of_cv_types.Add(WSI.Common.CashierVoucherType.ExchangeCommissionB)
      End If
    End If

    If WSI.Common.Misc.IsCardPlayerToCompanyB() Then
      If Me.chk_player_card_pay.Checked Then
        _list_of_cv_types.Add(WSI.Common.CashierVoucherType.CardDepositIn_B)
        _list_of_cv_types.Add(WSI.Common.CashierVoucherType.CardReplacement_B)
      End If
    End If

    Return _list_of_cv_types
  End Function

  ''' <summary>
  ''' Get list of CV Types not to include in group report
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetSalesNoteListOfCVTypesNotToIncludeInGroupReport() As List(Of Integer)
    Dim _list_of_cv_types As List(Of Integer)
    _list_of_cv_types = New List(Of Integer)

    _list_of_cv_types.Add(WSI.Common.CashierVoucherType.ServiceCharge_B)
    _list_of_cv_types.Add(WSI.Common.CashierVoucherType.Commission_B)
    _list_of_cv_types.Add(WSI.Common.CashierVoucherType.CheckCommissionB)
    _list_of_cv_types.Add(WSI.Common.CashierVoucherType.ExchangeCommissionB)
    _list_of_cv_types.Add(WSI.Common.CashierVoucherType.CardDepositIn_B)
    _list_of_cv_types.Add(WSI.Common.CashierVoucherType.CardReplacement_B)
    _list_of_cv_types.Add(WSI.Common.CashierVoucherType.CardDepositOut_B)

    Return _list_of_cv_types
  End Function

  ''' <summary>
  ''' Get string with the concatenated elements in the CV types list
  ''' </summary>
  ''' <param name="ListOfCVTypes"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetStringForListOfCVTypes(ByVal ListOfCVTypes As List(Of Integer)) As String
    If (ListOfCVTypes.Count = 0) Then
      Return String.Empty
    End If

    'Convert to array of string
    Dim _list_cv_types_aux = ListOfCVTypes.ConvertAll(Of String)(Function(i As Integer) i.ToString())

    Dim _str_types = String.Join(", ", _list_cv_types_aux.ToArray())

    Return _str_types
  End Function

  ''' <summary>
  ''' Return filter from list of CV types
  ''' </summary>
  ''' <param name="ListOfCVTypes"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetWhereForListOfCVTypes(ByVal ListOfCVTypes As List(Of Integer)) As String
    Dim _str_list_of_cv_types As String

    _str_list_of_cv_types = GetStringForListOfCVTypes(ListOfCVTypes)
    If (String.IsNullOrEmpty(_str_list_of_cv_types)) Then
      Return String.Empty
    End If

    Return "(CV_TYPE IN (" & _str_list_of_cv_types & "))"
  End Function

  ''' <summary>
  ''' Set column value if it is a numeric value (CV_TYPE) and is not filled with a text
  ''' </summary>
  ''' <param name="RowIndex"></param>
  ''' <param name="ColumnIndex"></param>
  ''' <remarks></remarks>
  Private Sub SetConceptColumnValueForCardPlayerToCompanyB(ByRef RowIndex As Integer, ByRef ColumnIndex As Integer)
    If WSI.Common.Misc.IsCardPlayerToCompanyB() Then
      If IsNumeric(Me.Grid.Cell(RowIndex, ColumnIndex).Value) Then
        Select Case (Me.Grid.Cell(RowIndex, ColumnIndex).Value)
          Case WSI.Common.CashierVoucherType.CardDepositIn_B
            Me.Grid.Cell(RowIndex, ColumnIndex).Value = GLB_NLS_GUI_INVOICING.GetString(51)

          Case WSI.Common.CashierVoucherType.CardReplacement_B
            Me.Grid.Cell(RowIndex, ColumnIndex).Value = GLB_NLS_GUI_INVOICING.GetString(70)

          Case WSI.Common.CashierVoucherType.CardDepositOut_B
            Me.Grid.Cell(RowIndex, ColumnIndex).Value = GLB_NLS_GUI_INVOICING.GetString(52)
        End Select
      End If
    End If
  End Sub

  ' PURPOSE:   Control selected options in Operation Type
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub SelectedOptions()
    If Me.opt_sale_note.Checked Then
      Me.chk_charge_sale_note.Enabled = True
      Me.chk_service_charge_sale_note.Enabled = True
      Me.chk_card_commission.Enabled = True
      Me.chk_player_card_pay.Enabled = True

      If Not (Me.chk_charge_sale_note.Checked OrElse Me.chk_service_charge_sale_note.Checked OrElse Me.chk_card_commission.Checked OrElse Me.chk_player_card_pay.Checked) Then
        Me.chk_charge_sale_note.Checked = True
        Me.chk_service_charge_sale_note.Checked = True
        Me.chk_card_commission.Checked = True
        Me.chk_player_card_pay.Checked = True
      End If
    Else
      Me.chk_charge_sale_note.Enabled = False
      Me.chk_service_charge_sale_note.Enabled = False
      Me.chk_card_commission.Enabled = False
      Me.chk_player_card_pay.Enabled = False
    End If

    If Me.opt_charge_cancel_sale_note.Checked Then
      Me.chk_cancel_player_card_pay.Enabled = True
      Me.chk_cancel_charge_sale_note.Enabled = True

      If Not (Me.chk_cancel_player_card_pay.Checked OrElse Me.chk_cancel_charge_sale_note.Checked) Then
        Me.chk_cancel_player_card_pay.Checked = True
        Me.chk_cancel_charge_sale_note.Checked = True
      End If
    Else
      Me.chk_cancel_player_card_pay.Enabled = False
      Me.chk_cancel_charge_sale_note.Enabled = False
    End If

  End Sub ' OptionsSelected

  ''' <summary>
  ''' Is third tax is active
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetThirdTaxIsActive() As Boolean
    Dim _third_tax_pct As Decimal
    Decimal.TryParse(GetCashierData("Tax.OnPrize.3.Pct"), _third_tax_pct)

    Return (_third_tax_pct > 0)
  End Function

  ''' <summary>
  ''' Set width of third tax column 
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SetThirdColumnWidth(ThirdTaxColumnIndex As Integer, ShowThirdTaxColumn As Boolean)
    If ShowThirdTaxColumn Then
      Me.Grid.Column(ThirdTaxColumnIndex).Fixed = False
      Me.Grid.Column(ThirdTaxColumnIndex).Width = GRID_COLUMN_WIDTH_WITHDRAW_TAX3
    Else
      Me.Grid.Column(ThirdTaxColumnIndex).Width = 0
    End If
  End Sub

#End Region ' Private Functions

#Region "Events"

  Private Sub opt_charge_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_charge.CheckedChanged
    SelectedOptions()
  End Sub

  Private Sub opt_withdraw_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_withdraw.CheckedChanged
    SelectedOptions()
  End Sub

  Private Sub opt_sale_note_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_sale_note.CheckedChanged
    SelectedOptions()
  End Sub

  Private Sub opt_charge_cancel_sale_note_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_charge_cancel_sale_note.CheckedChanged
    SelectedOptions()
  End Sub

#End Region ' Events

End Class