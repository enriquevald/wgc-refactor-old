﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_intellia_floor_editor
  Inherits GUI_Controls.frm_base

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.wb_floor_editor = New System.Windows.Forms.WebBrowser()
    Me.SuspendLayout()
    '
    'wb_floor_editor
    '
    Me.wb_floor_editor.AllowWebBrowserDrop = False
    Me.wb_floor_editor.CausesValidation = False
    Me.wb_floor_editor.Dock = System.Windows.Forms.DockStyle.Fill
    Me.wb_floor_editor.Location = New System.Drawing.Point(4, 4)
    Me.wb_floor_editor.MinimumSize = New System.Drawing.Size(20, 20)
    Me.wb_floor_editor.Name = "wb_floor_editor"
    Me.wb_floor_editor.ScriptErrorsSuppressed = True
    Me.wb_floor_editor.Size = New System.Drawing.Size(988, 496)
    Me.wb_floor_editor.TabIndex = 1
    Me.wb_floor_editor.WebBrowserShortcutsEnabled = False
    '
    'frm_intellia_floor_editor
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(996, 504)
    Me.Controls.Add(Me.wb_floor_editor)
    Me.Name = "frm_intellia_floor_editor"
    Me.Text = "frm_intellia_floor_editor"
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents wb_floor_editor As System.Windows.Forms.WebBrowser
End Class
