'-------------------------------------------------------------------
' Copyright � 2008 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_jackpot_promo_messages.vb
'
' DESCRIPTION:   Jackpot configuration form
'
' AUTHOR:        Armando Alva
'
' CREATION DATE: 25-AUG-2008
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 25-AUG-2008  AAV    Initial Draft.
' 02-SEP-2008  AAV    Initial Release.
'--------------------------------------------------------------------
Option Strict Off
Option Explicit On

Imports System.Data.OleDb
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Math
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE

Public Class frm_jackpot_promo_messages
  Inherits frm_base_edit


#Region " Constants "

  'Match with constant in CommonData.dll
  Private AUDIT_NAME_JACKPOT_PARAMETERS As Integer = 17
#End Region 'Constants

#Region "Overrides"

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CLASS_II_JACKPOT_PROMO_MESSAGE

    ' Set the form icon from the embedded resource (ICO file must be built in the project as "Embedded Resource")
    ' Call could be made as GetManifestResourceStream("GUI_JackpotManager.GUI_JackpotManager.ico"))
    ' Me.Icon = New Icon(System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream(Me.GetType(), "GUI_JackpotManager.ico"))

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    ' Initialize Form Controls

    ' - Form
    Me.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(235)

    '- Buttons

    ' - Labels
    '   - Jackpot system 

    ' Enable / Disable controls
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = False
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Visible = False
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Enabled = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Enabled = True

    Me.gb_promo_message.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(321)
    Me.lbl_message_1.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(322) + " 1 "
    Me.lbl_message_2.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(322) + " 2 "
    Me.lbl_message_3.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(322) + " 3 "

  End Sub 'GUI_InitControls

  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Return True

  End Function ' GUI_IsScreenDataOk

  Protected Overrides Sub GUI_GetScreenData()

    Dim jp_params_edit As CLASS_JACKPOT_PARAMS
    Dim jp_params_read As CLASS_JACKPOT_PARAMS

    jp_params_edit = Me.DbEditedObject
    jp_params_read = Me.DbReadObject

    jp_params_edit.PromoMess(0) = txt_message_1.Text
    jp_params_edit.PromoMess(1) = txt_message_2.Text
    jp_params_edit.PromoMess(2) = txt_message_3.Text

  End Sub ' GUI_GetScreenData

  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    Dim jackpot_parameters As CLASS_JACKPOT_PARAMS
    jackpot_parameters = Me.DbReadObject

    Me.txt_message_1.Text = jackpot_parameters.PromoMess(0)
    Me.txt_message_2.Text = jackpot_parameters.PromoMess(1)
    Me.txt_message_3.Text = jackpot_parameters.PromoMess(2)

  End Sub ' GUI_SetScreenData

  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)

  End Sub  'GUI_Permissions

  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)
    Dim jackpot_parameters As CLASS_JACKPOT_PARAMS

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_JACKPOT_PARAMS
        jackpot_parameters = DbEditedObject
        jackpot_parameters.Enabled = False

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        Else
          ' Call Me.DB_CheckAllCounters() DO NOTHING
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)



    End Select

  End Sub ' GUI_DB_Operation

  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    ' The keypress event is done in uc_grid control
    Return MyBase.GUI_KeyPress(sender, e)

  End Function ' GUI_KeyPress

#End Region

#Region "Private Functions"

#End Region

#Region "Public Functions"

  ' PURPOSE: Init form in edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - mdiparent
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Overloads Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
    Me.MdiParent = MdiParent
    Me.DbObjectId = Nothing

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(False)
    End If

  End Sub ' ShowEditItem

#End Region

#Region "Events"

  Private Sub dg_jackpot_list_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer)
    ' DO NOTHING
  End Sub  ' dg_jackpot_list_CellDataChangedEvent

#End Region

  Private Sub panel_data_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles panel_data.Paint

  End Sub

  Private Sub frm_jackpot_promo_messages_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

  End Sub

  Private Sub dtp_working_from_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)

  End Sub

  Private Sub cmb_block_mode_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)

  End Sub

End Class