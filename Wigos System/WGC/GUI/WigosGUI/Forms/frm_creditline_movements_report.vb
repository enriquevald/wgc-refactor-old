﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_creditline_sel.vb
' DESCRIPTION:   Displays list of Clients with credit lines movements
' AUTHOR:        Enric Tomas
' CREATION DATE: 28-MAR-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 28-MAR-2017  ETP     Initial version
' 03-APR-2017  ETP     WIGOS-116: CreditLines - Multisite.
' 25-JUL-2017  DPC     WIGOS-3928: Minor errors in Credit lines report
' 26-SEP-2017  ETP     Fixed Bug 29921:[WIGOS-4564] Multisite: wrong site number in grids
' -------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_CommonMisc
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports WSI.Common
Imports WSI.Common.Currency
Imports WSI.Common.CreditLines
Imports WigosGUI.CLASS_CREDITLINE_MOVEMENTS

Public Class frm_creditline_movements_report
  Inherits frm_base_sel

#Region " Members "
  Private m_init_date_from As Date
  Private m_credit_line_movements As CLASS_CREDITLINE_MOVEMENTS
  Private m_totals_data_table As DataTable

  Private m_filter_account As String
  Private m_filter_card As String
  Private m_filter_holder As String
  Private m_filter_vip As String
  Private m_filter_movements_types As String
  Private m_filter_from As String
  Private m_filter_to As String
  Private m_site As String
  Private m_is_center As Boolean
  Private m_refreshing_grid As Boolean

#End Region ' Members

#Region " Constants "

  ' SQL Columns
  Private Const SQL_COLUMN_ACCOUNT_ID As Integer = 0
  Private Const SQL_COLUMN_ACCOUNT_NAME As Integer = 1
  Private Const SQL_COLUMN_CREDIT_ID As Integer = 2
  Private Const SQL_COLUMN_OPERATION_ID As Integer = 3
  Private Const SQL_COLUMN_TYPE As Integer = 4
  Private Const SQL_COLUMN_OLD_VALUE As Integer = 5
  Private Const SQL_COLUMN_NEW_VALUE As Integer = 6
  Private Const SQL_COLUMN_CREATION As Integer = 7
  Private Const SQL_COLUMN_CREATION_USER As Integer = 8
  Private Const SQL_COLUMN_SITE As Integer = 9

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_SITE As Integer = 1
  Private Const GRID_COLUMN_CREATION As Integer = 2
  Private Const GRID_COLUMN_CREATION_USER As Integer = 3
  Private Const GRID_COLUMN_ACCOUNT_ID As Integer = 4
  Private Const GRID_COLUMN_ACCOUNT_NAME As Integer = 5
  Private Const GRID_COLUMN_TYPE As Integer = 6
  Private Const GRID_COLUMN_OLD_VALUE As Integer = 7
  Private Const GRID_COLUMN_NEW_VALUE As Integer = 8
  Private Const GRID_COLUMN_TYPE_ID As Integer = 9

  Private Const GRID_COLUMNS As Integer = 10
  Private Const GRID_HEADER_ROWS As Integer = 2
  Private Const GRID_TAB As String = "     "

  ' Mov. types data grid
  Private Const GRID_2_COLUMN_CODE As Integer = 0
  Private Const GRID_2_COLUMN_CHECKED As Integer = 1
  Private Const GRID_2_COLUMN_DESC As Integer = 2
  Private Const GRID_2_COLUMN_ROW_TYPE As Integer = 3

  Private Const GRID_2_ROW_TYPE_HEADER = 0
  Private Const GRID_2_ROW_TYPE_DATA = 1

  ' Left padding for data rows in the mov. types data grid
  Private Const GRID_2_TAB As String = "     "

  Private Const GRID_2_COLUMNS As Integer = 4
  Private Const GRID_2_HEADER_ROWS As Integer = 1

  ' DATAGRID Width
  Private Const GRID_WIDTH_INDEX As Integer = 200
  Private Const GRID_WIDTH_SITE As Integer = 0
  Private Const GRID_WIDTH_HOLDER As Integer = 3000
  Private Const GRID_WIDTH_USER As Integer = 1400
  Private Const GRID_WIDTH_VALUE As Integer = 2740
  Private Const GRID_WIDTH_TYPE As Integer = 3255
  Private Const GRID_WIDTH_DATES As Integer = 1740
  Private Const GRID_WIDTH_DEFAULT As Integer = 1000

  ' Excel columns
  Private Const EXCEL_COLUMN_SITE_ID As Integer = 0

#End Region ' Constants

#Region " Constructor "
  Public Sub New()

    ' This call is required by the designer.
    InitializeComponent()

    m_is_center = GeneralParam.GetBoolean("MultiSite", "IsCenter", False)

  End Sub ' New
#End Region 'Constructor

#Region " Public Methods "

  ''' <summary>
  ''' Sets the Form Id
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CREDITLINE_MOVEMENTS

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ''' <summary>
  ''' Show the Form
  ''' </summary>
  ''' <param name="MdiParent"></param>
  ''' <remarks></remarks>
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION
    Me.MdiParent = MdiParent

    m_init_date_from = Nothing

    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Methods

#Region " Overrides "

  ''' <summary>
  ''' Initialise all the form controls
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False

    ' Customer base
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7994)

    ' Date Range
    Me.gb_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(268)

    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_from.Value = GUI_GetDate().AddHours(GetDefaultClosingTime())
    Me.dtp_from.Checked = False

    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_to.Value = GUI_GetDate().AddHours(GetDefaultClosingTime())
    Me.dtp_to.Checked = False

    ' Credit Line Movement Types
    Me.gb_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3391)
    Me.opt_several_types.Text = GLB_NLS_GUI_INVOICING.GetString(223)
    Me.opt_all_types.Text = GLB_NLS_GUI_INVOICING.GetString(224)

    ' Multisite
    Me.uc_sites_sel.Visible = m_is_center
    If m_is_center Then
      InitializeMultiSiteControls()
    End If

    Me.m_credit_line_movements = New CLASS_CREDITLINE_MOVEMENTS

    Call GUI_StyleSheetMovementTypes()

    Call FillMovementTypeGrid()

    Call SetDefaultValues()

    ' Define layout of all Main Grid Columns
    Call GUI_StyleSheet()

  End Sub ' GUI_InitControls

  ''' <summary>
  ''' Set te Form Filters to their Default Values
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub 'GUI_FilterReset

  ''' <summary>
  ''' Perform non-default actions for Button Clicks
  ''' </summary>
  ''' <param name="ButtonId"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_FILTER_APPLY
        Call MyBase.GUI_ButtonClick(ButtonId)
      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub 'GUI_ButtonClick

  ''' <summary>
  ''' Calls the Creditline class to return the SQL Query
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim str_sql As String

    str_sql = ""
    str_sql = m_credit_line_movements.getCreditLinesMovements(dtp_from, dtp_to, GetTypeIdListSelected(), uc_account_sel1, uc_sites_sel)

    Return str_sql
  End Function 'GUI_FilterGetSqlQuery

  ''' <summary>
  ''' Validates the Filter selection where needed
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_FilterCheck() As Boolean

    'Date selecteciton
    If Me.dtp_from.Checked AndAlso Me.dtp_to.Checked Then
      If dtp_from.Value > dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    ' Account selection
    If Not Me.uc_account_sel1.ValidateFormat() Then
      Return False
    End If

    If m_is_center Then
      ' Multisite: Sites selection
      If Not Me.uc_sites_sel.FilterCheckSites() Then

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  ''' <summary>
  ''' Writes the Grid Row reformating where required and adding calculated cell values
  ''' </summary>
  ''' <param name="RowIndex"></param>
  ''' <param name="DbRow"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As CLASS_DB_ROW) As Boolean

    Dim _str_type As String

    _str_type = String.Empty

    If m_is_center AndAlso Not DbRow.IsNull(SQL_COLUMN_SITE) Then

      Me.Grid.Cell(RowIndex, GRID_COLUMN_SITE).Value = String.Format("{0:000}", DbRow.Value(SQL_COLUMN_SITE))
    End If

    If Not DbRow.IsNull(SQL_COLUMN_CREATION) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CREATION).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_CREATION), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_CREATION_USER) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CREATION_USER).Value = DbRow.Value(SQL_COLUMN_CREATION_USER)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_ACCOUNT_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_ID)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_ACCOUNT_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_NAME).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_NAME)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_OPERATION_ID) Then

      _str_type = MarkUndoneMovement(DbRow.Value(SQL_COLUMN_TYPE), DbRow.Value(SQL_COLUMN_NEW_VALUE))

      Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE).Value = String.Format("{0}{1}", _str_type, CreditLineMovements.MovementTypeToString(Int32.Parse(DbRow.Value(SQL_COLUMN_TYPE))))
    End If

    If Not DbRow.IsNull(SQL_COLUMN_OLD_VALUE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_OLD_VALUE).Value = m_credit_line_movements.XmlToData(DbRow.Value(SQL_COLUMN_OLD_VALUE),
                                                                                              DbRow.Value(SQL_COLUMN_TYPE),
                                                                                              DbRow.Value(SQL_COLUMN_TYPE) <> CreditLineMovements.CreditLineMovementType.Create)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_NEW_VALUE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NEW_VALUE).Value = m_credit_line_movements.XmlToData(DbRow.Value(SQL_COLUMN_NEW_VALUE),
                                                                                              DbRow.Value(SQL_COLUMN_TYPE),
                                                                                              DbRow.Value(SQL_COLUMN_TYPE) <> CreditLineMovements.CreditLineMovementType.Create)
    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE_ID).Value = GRID_2_ROW_TYPE_HEADER

    If DbRow.Value(SQL_COLUMN_TYPE) = CreditLineMovements.CreditLineMovementType.Create Then
      Call Me.Grid.Row(RowIndex).SetSignalAllowExpand(GRID_COLUMN_TYPE)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE).Value = String.Format("{0} - {1}: {2}", Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE).Value,
                                                                     GLB_NLS_GUI_PLAYER_TRACKING.GetString(5323),
                                                                     m_credit_line_movements.XmlToData(DbRow.Value(SQL_COLUMN_NEW_VALUE), DbRow.Value(SQL_COLUMN_TYPE), True))

      AddCreateRows(RowIndex, DbRow)
    End If

    Return True

  End Function ' GUI_SetupRow

  ''' <summary>
  ''' GUI_EditSelectedItem for expanding rows.
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_EditSelectedItem()

    Dim last_form_row As Integer
    Dim idx As Integer
    Dim prev_redraw As Boolean

    With Me.Grid

      prev_redraw = .Redraw
      .Redraw = False

      If Not RowIsHeaderGrid(.CurrentRow) Then

        Exit Sub
      End If

      last_form_row = .CurrentRow

      For idx = .CurrentRow + 1 To Me.Grid.NumRows - 1
        If RowIsHeaderGrid(idx) Then

          Exit For
        End If
        last_form_row = idx
      Next

      Call .CollapseExpand(.CurrentRow, last_form_row, GRID_COLUMN_TYPE)

      .Redraw = prev_redraw

    End With

  End Sub ' GUI_EditSelectedItem

  ''' <summary>
  ''' Update filters for report.
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_filter_account = String.Empty
    m_filter_card = String.Empty
    m_filter_holder = String.Empty
    m_filter_movements_types = String.Empty
    m_filter_from = String.Empty
    m_filter_to = String.Empty

    If Not String.IsNullOrEmpty(uc_account_sel1.Account) Then
      m_filter_account = uc_account_sel1.Account
    End If

    If Not String.IsNullOrEmpty(uc_account_sel1.TrackData) Then
      m_filter_card = uc_account_sel1.TrackData
    End If

    If Not String.IsNullOrEmpty(uc_account_sel1.Holder) Then
      m_filter_holder = uc_account_sel1.Holder
    End If

    If Not String.IsNullOrEmpty(uc_account_sel1.HolderIsVip) Then
      m_filter_vip = uc_account_sel1.HolderIsVip
    End If

    If dtp_from.Checked Then
      m_filter_from = dtp_from.Value
    End If

    If dtp_to.Checked Then
      m_filter_to = dtp_to.Value
    End If

    m_filter_movements_types = GetTypeDescListSelected()
    If (String.IsNullOrEmpty(m_filter_movements_types)) Then
      m_filter_movements_types = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1807)
    End If

    If m_is_center Then
      Me.uc_sites_sel.GetSitesIdListSelectedAll()
      m_site = Me.uc_sites_sel.GetSitesIdListSelectedToPrint()
    End If

  End Sub ' GUI_ReportUpdateFilters

  ''' <summary>
  ''' Sets the report Filter values for Excel and Print
  ''' </summary>
  ''' <param name="PrintData"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(202), m_filter_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(203), m_filter_to)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), m_filter_account)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_filter_card)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), m_filter_holder)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4802), m_filter_vip)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(3391), m_filter_movements_types)
    If (m_is_center) Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1803), m_site)
    End If
  End Sub ' GUI_ReportFilter

  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(ExcelData)

    ' Set specific column formats.
    ExcelData.SetColumnFormat(EXCEL_COLUMN_SITE_ID, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)

  End Sub

#End Region ' Overrides

#Region " Private Methods "

  ''' <summary>
  ''' Sets Default Values for the Filters
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SetDefaultValues()

    Dim _initial_time As Date

    ' Dates From and To
    _initial_time = WSI.Common.Misc.TodayOpening()

    Me.dtp_from.Value = _initial_time
    Me.dtp_from.Checked = False
    Me.dtp_to.Value = Me.dtp_from.Value.AddDays(1)
    Me.dtp_to.Checked = True

    ' Clear Account, Card and Name
    Me.uc_account_sel1.Clear()

    ' Reset Status Grid
    opt_all_types.Checked = True

    Call FillMovementTypeGrid()

    If m_is_center Then
      Me.uc_sites_sel.SetDefaultValues(True)
    End If

  End Sub 'SetDefaultValues

  ''' <summary>
  ''' Populate the Combo Grid for Statuses
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub FillMovementTypeGrid()
    ' Fill CheckedList control with groups
    FillMovTypesFilterGrid(m_credit_line_movements.GetMovTypesFilterGridData())

  End Sub ' FillStatusGrid

  ''' <summary>
  ''' Fill MovTypes Filter Grid
  ''' </summary>
  ''' <param name="DgList"></param>
  ''' <remarks></remarks>
  Private Sub FillMovTypesFilterGrid(ByVal DgList As List(Of TYPE_DG_FILTER_ELEMENT))

    Me.dg_filter.Clear()
    Me.dg_filter.Redraw = False

    ' Get the filters array and insert it into the grid.

    For Each _element As TYPE_DG_FILTER_ELEMENT In DgList
      If (_element.elem_type = GRID_2_ROW_TYPE_DATA) Then
        Call AddOneRowData(_element.code, _element.description)
      Else
        If (_element.elem_type = GRID_2_ROW_TYPE_HEADER) Then
          Call AddOneRowHeader(_element.description)
        End If
      End If
    Next

    Me.dg_filter.Redraw = True

  End Sub ' FillMovTypesFilterGrid

  ''' <summary>
  ''' Add row to data grid
  ''' </summary>
  ''' <param name="TypeCode"></param>
  ''' <param name="TypeDesc"></param>
  ''' <remarks></remarks>
  Private Sub AddOneRowData(ByVal TypeCode As Integer, ByVal TypeDesc As String)
    Dim prev_redraw As Boolean

    prev_redraw = Me.dg_filter.Redraw
    Me.dg_filter.Redraw = False

    dg_filter.AddRow()

    Me.dg_filter.Redraw = False

    dg_filter.Cell(dg_filter.NumRows - 1, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    dg_filter.Cell(dg_filter.NumRows - 1, GRID_2_COLUMN_CODE).Value = TypeCode
    dg_filter.Cell(dg_filter.NumRows - 1, GRID_2_COLUMN_DESC).Value = GRID_2_TAB & TypeDesc
    dg_filter.Cell(dg_filter.NumRows - 1, GRID_2_COLUMN_ROW_TYPE).Value = GRID_2_ROW_TYPE_DATA

    Me.dg_filter.Row(dg_filter.NumRows - 1).Height = 0

    Me.dg_filter.Redraw = prev_redraw
  End Sub ' AddOneRowData

  ''' <summary>
  ''' Add header to data grid
  ''' </summary>
  ''' <param name="HeaderMsg"></param>
  ''' <remarks></remarks>
  Private Sub AddOneRowHeader(ByVal HeaderMsg As String)

    dg_filter.AddRow()

    dg_filter.Cell(dg_filter.NumRows - 1, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    dg_filter.Cell(dg_filter.NumRows - 1, GRID_2_COLUMN_DESC).Value = HeaderMsg
    dg_filter.Cell(dg_filter.NumRows - 1, GRID_2_COLUMN_ROW_TYPE).Value = GRID_2_ROW_TYPE_HEADER

    Call dg_filter.Row(dg_filter.NumRows - 1).SetSignalAllowExpand(GRID_2_COLUMN_DESC)
  End Sub ' AddOneRowHeader

  ''' <summary>
  ''' Style Sheet for the look of the form
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GUI_StyleSheet()
    With Me.Grid

      ' Initialize the index of unfixed columns and return the number of columns
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Enable sortable in Main Grid Columns
      .Sortable = False

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Header(1).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Site
      If m_is_center Then
        .Column(GRID_COLUMN_SITE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5033)
        .Column(GRID_COLUMN_SITE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1927) ' SiteId
        .Column(GRID_COLUMN_SITE).Width = 850
        .Column(GRID_COLUMN_SITE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      Else
        .Column(GRID_COLUMN_SITE).Header(0).Text = ""
        .Column(GRID_COLUMN_SITE).Header(1).Text = ""
        .Column(GRID_COLUMN_SITE).Width = GRID_WIDTH_SITE
        .Column(GRID_COLUMN_SITE).HighLightWhenSelected = False
        .Column(GRID_COLUMN_SITE).IsColumnPrintable = False
      End If

      ' Movement date
      .Column(GRID_COLUMN_CREATION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5033)
      .Column(GRID_COLUMN_CREATION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2287)
      .Column(GRID_COLUMN_CREATION).Width = GRID_WIDTH_DATES
      .Column(GRID_COLUMN_CREATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' User
      .Column(GRID_COLUMN_CREATION_USER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5033)
      .Column(GRID_COLUMN_CREATION_USER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1278)
      .Column(GRID_COLUMN_CREATION_USER).Width = GRID_WIDTH_USER
      .Column(GRID_COLUMN_CREATION_USER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Account Number
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COLUMN_ACCOUNT_ID).Width = GRID_WIDTH_DEFAULT
      .Column(GRID_COLUMN_ACCOUNT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Account Holder
      .Column(GRID_COLUMN_ACCOUNT_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_ACCOUNT_NAME).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(235)
      .Column(GRID_COLUMN_ACCOUNT_NAME).Width = GRID_WIDTH_HOLDER
      .Column(GRID_COLUMN_ACCOUNT_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Operation Type
      .Column(GRID_COLUMN_TYPE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2466)
      .Column(GRID_COLUMN_TYPE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4544)
      .Column(GRID_COLUMN_TYPE).Width = GRID_WIDTH_TYPE
      .Column(GRID_COLUMN_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Operation Type ID
      .Column(GRID_COLUMN_TYPE_ID).Width = 0
      .Column(GRID_COLUMN_TYPE_ID).HighLightWhenSelected = False
      .Column(GRID_COLUMN_TYPE_ID).IsColumnPrintable = False

      ' Old Value
      .Column(GRID_COLUMN_OLD_VALUE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2466)
      .Column(GRID_COLUMN_OLD_VALUE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7995)
      .Column(GRID_COLUMN_OLD_VALUE).Width = GRID_WIDTH_VALUE
      .Column(GRID_COLUMN_OLD_VALUE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' New Value
      .Column(GRID_COLUMN_NEW_VALUE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2466S)
      .Column(GRID_COLUMN_NEW_VALUE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7996)
      .Column(GRID_COLUMN_NEW_VALUE).Width = GRID_WIDTH_VALUE
      .Column(GRID_COLUMN_NEW_VALUE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With

  End Sub ' GUI_StyleSheet

  ''' <summary>
  ''' Style Sheet for the look of filter
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GUI_StyleSheetMovementTypes()

    With Me.dg_filter
      Call .Init(GRID_2_COLUMNS, GRID_2_HEADER_ROWS, True)
      .Counter(0).Visible = False
      .Sortable = False

      ' Hidden columns
      .Column(GRID_2_COLUMN_CODE).Header.Text = ""
      .Column(GRID_2_COLUMN_CODE).WidthFixed = 0

      ' GRID_COL_CHECKBOX
      .Column(GRID_2_COLUMN_CHECKED).Header.Text = ""
      .Column(GRID_2_COLUMN_CHECKED).WidthFixed = 400
      .Column(GRID_2_COLUMN_CHECKED).Fixed = True
      .Column(GRID_2_COLUMN_CHECKED).Editable = True
      .Column(GRID_2_COLUMN_CHECKED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX

      ' GRID_COL_DESCRIPTION
      .Column(GRID_2_COLUMN_DESC).Header.Text = ""
      .Column(GRID_2_COLUMN_DESC).WidthFixed = 3000
      .Column(GRID_2_COLUMN_DESC).Fixed = True
      .Column(GRID_2_COLUMN_DESC).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' GRID_COL_TYPE
      .Column(GRID_2_COLUMN_ROW_TYPE).Header.Text = ""
      .Column(GRID_2_COLUMN_ROW_TYPE).WidthFixed = 0
    End With

  End Sub ' GUI_StyleSheetMovementTypes

  ''' <summary>
  ''' Initialize MultiSite Controls
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub InitializeMultiSiteControls()
    'Resize panel
    Me.panel_filter.Height = 246

    'Resize Controls:
    Me.gb_date.Size = New System.Drawing.Size(304, 85)

    'Allocate controls:
    Me.uc_account_sel1.Location = New System.Drawing.Point(3, 104)
    Me.gb_type.Location = New System.Drawing.Point(326, 15)
    Me.uc_sites_sel.Location = New System.Drawing.Point(683, 13)

    Me.dtp_from.Location = New System.Drawing.Point(39, 21)
    Me.dtp_to.Location = New System.Drawing.Point(39, 51)

    Me.uc_sites_sel.ShowMultisiteRow = False
    Me.uc_sites_sel.Init()
    Me.uc_sites_sel.SetDefaultValues(True)
  End Sub ' InitializeMultiSiteControls

  ''' <summary>
  ''' Data selected event.
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub dg_types_DataSelectedEvent() Handles dg_filter.DataSelectedEvent

    Dim _idx As Integer
    Dim _last_row As Integer
    Dim _row_is_header As Boolean

    If Me.opt_several_types.Checked = False Then
      Me.opt_several_types.Checked = True
    End If

    With Me.dg_filter
      _row_is_header = RowIsHeaderFilter(.CurrentRow)

      If _row_is_header And .CurrentCol <> GRID_2_COLUMN_CHECKED Then
        _last_row = .CurrentRow
        For _idx = .CurrentRow + 1 To .NumRows - 1
          If RowIsHeaderFilter(_idx) Then

            Exit For
          End If
          _last_row = _idx
        Next

        Me.dg_filter.CollapseExpand(.CurrentRow, _last_row, GRID_2_COLUMN_DESC)
      End If
    End With

  End Sub ' dg_types_DataSelectedEvent

  ''' <summary>
  ''' CellDataChangedEvent
  ''' </summary>
  ''' <param name="Row"></param>
  ''' <param name="Column"></param>
  ''' <remarks></remarks>
  Private Sub dg_types_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer) Handles dg_filter.CellDataChangedEvent

    Dim current_row As Integer

    ' Prevent recursive calls
    If m_refreshing_grid = True Then

      Exit Sub
    End If

    ' Update radio buttons accordingly
    If Me.opt_several_types.Checked = False Then
      Me.opt_several_types.Checked = True
    End If

    With Me.dg_filter

      current_row = .CurrentRow
      m_refreshing_grid = True

      If Me.dg_filter.Cell(.CurrentRow, GRID_2_COLUMN_ROW_TYPE).Value = GRID_2_ROW_TYPE_HEADER Then
        Call ChangeCheckofRows(.CurrentRow, .Cell(.CurrentRow, GRID_2_COLUMN_CHECKED).Value)
      Else
        If .Cell(.CurrentRow, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED Then
          Call TryToUncheckHeader(.CurrentRow)
        Else
          Call TryToCheckHeader(.CurrentRow)
        End If
      End If

      m_refreshing_grid = False
      .CurrentRow = current_row

    End With
  End Sub ' dg_types_CellDataChangedEvent

  ''' <summary>
  ''' Check if row is header
  ''' </summary>
  ''' <param name="RowIdx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function RowIsHeaderFilter(ByVal RowIdx As Integer) As Boolean

    If Me.dg_filter.Cell(RowIdx, GRID_2_COLUMN_ROW_TYPE).Value = GRID_2_ROW_TYPE_HEADER Then
      Return True
    Else
      Return False
    End If

  End Function ' RowIsHeader

  ''' <summary>
  ''' Check if row is header
  ''' </summary>
  ''' <param name="RowIdx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function RowIsHeaderGrid(ByVal RowIdx As Integer) As Boolean

    If Me.Grid.Cell(RowIdx, GRID_COLUMN_TYPE_ID).Value = GRID_2_ROW_TYPE_HEADER Then
      Return True
    Else
      Return False
    End If

  End Function ' RowIsHeader

  ''' <summary>
  ''' Update the checkbox state for all row dependants (for header rows)
  ''' </summary>
  ''' <param name="FirstRow"></param>
  ''' <param name="ValueChecked"></param>
  ''' <remarks></remarks>
  Private Sub ChangeCheckofRows(ByVal FirstRow As Integer, ByVal ValueChecked As String)

    Dim idx As Integer

    For idx = FirstRow + 1 To dg_filter.NumRows - 1
      If Me.dg_filter.Cell(idx, GRID_2_COLUMN_ROW_TYPE).Value = GRID_2_ROW_TYPE_HEADER Then

        Exit Sub
      End If

      Me.dg_filter.Cell(idx, GRID_2_COLUMN_CHECKED).Value = ValueChecked

    Next

  End Sub ' ChangeCheckofRows

  ''' <summary>
  ''' Update the check status of a header row when one of its dependant rows has been unchecked
  ''' </summary>
  ''' <param name="FirstRow"></param>
  ''' <remarks></remarks>
  Private Sub TryToUncheckHeader(ByVal FirstRow As Integer)

    Dim _idx As Integer
    Dim _idx_header As Integer
    Dim _idx_bottom_header As Integer
    Dim _checked As Boolean

    _idx_bottom_header = -1
    _idx_header = -1
    ' Go look for the header from current row upwards
    For _idx = FirstRow - 1 To 0 Step -1
      If RowIsHeaderFilter(_idx) And _idx_header = -1 Then
        _idx_header = _idx
      End If

      If Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED And _idx_header = -1 Then
        ' One or more upper rows are already unchecked
        _checked = True
      End If
    Next

    ' Go look for the bottom header from current row downwards
    For _idx = FirstRow + 1 To Me.dg_filter.NumRows - 1
      If RowIsHeaderFilter(_idx) And _idx_bottom_header = -1 Then
        _idx_bottom_header = _idx
      End If
      If _idx_bottom_header = -1 And Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        _checked = True
      End If
    Next

    'update de check status of the header row when some or all the dependant rows has been checked
    If Not _checked Then
      Me.dg_filter.Cell(_idx_header, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    Else
      Me.dg_filter.Cell(_idx_header, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
    End If

  End Sub ' TryToUncheckHeader

  ''' <summary>
  ''' Update the check status of a header row when one of its dependant rows has been checked
  ''' </summary>
  ''' <param name="FirstRow"></param>
  ''' <remarks></remarks>
  Private Sub TryToCheckHeader(ByVal FirstRow As Integer)

    Dim _idx As Integer
    Dim _idx_header As Integer
    Dim _idx_bottom_header As Integer
    Dim _is_unchecked As Boolean

    _is_unchecked = False
    _idx_header = -1
    _idx_bottom_header = -1

    ' Go look for the header from current row upwards usually controls if any item is unchecked
    For _idx = FirstRow - 1 To 0 Step -1
      If RowIsHeaderFilter(_idx) And _idx_header = -1 Then
        _idx_header = _idx
      End If
      If _idx_header = -1 And Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED Then
        _is_unchecked = True
      End If
    Next

    ' Go look for the bottom header from current row downwards
    For _idx = FirstRow + 1 To Me.dg_filter.NumRows - 1
      If RowIsHeaderFilter(_idx) And _idx_bottom_header = -1 Then
        _idx_bottom_header = _idx
      End If
      If _idx_bottom_header = -1 And Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED Then
        _is_unchecked = True
      End If
    Next

    ' Updates the header check status if all rows are checked
    If _is_unchecked = False Then
      Me.dg_filter.Cell(_idx_header, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
    Else
      Me.dg_filter.Cell(_idx_header, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
    End If

  End Sub ' TryToCheckHeader

  ''' <summary>
  ''' Get filter movement Id list
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetTypeIdListSelected() As String

    Dim _idx_row As Integer
    Dim _list_mov_types As List(Of String)

    _list_mov_types = New List(Of String)

    If (opt_all_types.Checked) Then
      Return String.Empty
    End If

    ' Add checked filters
    For _idx_row = 0 To Me.dg_filter.NumRows - 1
      If (dg_filter.Cell(_idx_row, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED _
        And dg_filter.Cell(_idx_row, GRID_2_COLUMN_ROW_TYPE).Value = CStr(GRID_2_ROW_TYPE_DATA)) Then

        _list_mov_types.Add(dg_filter.Cell(_idx_row, GRID_2_COLUMN_CODE).Value)

      End If
    Next

    ' Return string concatenated
    Return String.Join(", ", _list_mov_types.ToArray())

  End Function 'GetTypeIdListSelected

  ''' <summary>
  ''' Get filter movement Description list
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetTypeDescListSelected() As String

    Dim _idx_row As Integer
    Dim _list_mov_types As List(Of String)

    _list_mov_types = New List(Of String)

    ' Add checked filters
    For _idx_row = 0 To Me.dg_filter.NumRows - 1
      If (dg_filter.Cell(_idx_row, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED _
        And dg_filter.Cell(_idx_row, GRID_2_COLUMN_ROW_TYPE).Value = CStr(GRID_2_ROW_TYPE_DATA)) Then

        _list_mov_types.Add(dg_filter.Cell(_idx_row, GRID_2_COLUMN_DESC).Value)

      End If
    Next

    ' Return string concatenated
    Return String.Join(", ", _list_mov_types.ToArray())

  End Function ' GetTypeDescListSelected

  ''' <summary>
  ''' Mark movement as Undone
  ''' </summary>
  ''' <param name="ColumnType"></param>
  ''' <param name="ColumnValue"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function MarkUndoneMovement(ByVal ColumnType As CreditLineMovements.CreditLineMovementType, ByVal ColumnValue As String) As String

    Dim _str_value As String

    Try

      _str_value = m_credit_line_movements.XmlToData(ColumnValue, ColumnType)

      If ColumnType = CreditLineMovements.CreditLineMovementType.GetMarker OrElse _
         ColumnType = CreditLineMovements.CreditLineMovementType.GetMarkerCardReplacement OrElse _
         ColumnType = CreditLineMovements.CreditLineMovementType.GetMarkerChips Then

        If Not String.IsNullOrEmpty(_str_value) AndAlso GUI_ParseCurrency(_str_value) < 0 Then
          Return String.Format("({0}) ", GLB_NLS_GUI_PLAYER_TRACKING.GetString(2786))
        End If

      End If

    Catch _ex As Exception
      Log.Error(_ex.Message())
    End Try

    Return String.Empty

  End Function ' MarkUndoneMovement

  ''' <summary>
  ''' Add Details Rows for create movements
  ''' </summary>
  ''' <param name="HeaderRowIndex"></param>
  ''' <param name="DbRow"></param>
  ''' <remarks></remarks>
  Private Sub AddCreateRows(ByVal HeaderRowIndex As Integer, ByVal DbRow As CLASS_DB_ROW)

    Dim _current_row As Integer
    Dim _xml_to_row As List(Of NEW_ROW_GRID)

    _current_row = HeaderRowIndex
    _xml_to_row = m_credit_line_movements.XmlToRow(DbRow.Value(SQL_COLUMN_NEW_VALUE))

    For Each _data As NEW_ROW_GRID In _xml_to_row
      Me.Grid.AddRow()
      _current_row = _current_row + 1

      If Not DbRow.IsNull(SQL_COLUMN_NEW_VALUE) Then
        Me.Grid.Cell(_current_row, GRID_COLUMN_NEW_VALUE).Value = _data.value
      End If

      Me.Grid.Cell(_current_row, GRID_COLUMN_TYPE).Value = String.format("{0}{1}", GRID_TAB, _data.type_description)
      Me.Grid.Cell(_current_row, GRID_COLUMN_TYPE_ID).Value = GRID_2_ROW_TYPE_DATA

      Me.Grid.Row(_current_row).Height = 0
    Next

  End Sub ' AddCreateRows

#End Region ' Private Methods

#Region " Events "

  Private Sub opt_all_types_CheckedChanged(sender As Object, e As EventArgs) Handles opt_all_types.CheckedChanged

    Dim idx As Integer

    If opt_all_types.Checked Then
      For idx = 0 To dg_filter.NumRows - 1
        Me.dg_filter.Cell(idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
      Next
    End If

  End Sub ' opt_all_types_CheckedChanged

#End Region ' Events

End Class
