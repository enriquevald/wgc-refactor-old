'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_base_sel_db_edit
' DESCRIPTION:   Selection base class
' AUTHOR:        Alberto Cuesta
' CREATION DATE: 08-MAR-2007
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 08-MAR-2007 ACC    Add AllowSorting property
' 08-JUN-2010 RCI    Add Print functionality
'-------------------------------------------------------------------

Option Strict Off
Option Explicit On 

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_Reports.PrintDataset
Imports System.Data.SqlClient

Public Class frm_terminals_edit
  Inherits GUI_Controls.frm_base_print

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents dg_servers As System.Windows.Forms.DataGrid
  Friend WithEvents Splitter1 As System.Windows.Forms.Splitter
  Friend WithEvents dg_terminals As System.Windows.Forms.DataGrid
  Friend WithEvents chk_only_terminals As System.Windows.Forms.CheckBox
  Friend WithEvents fra_server As System.Windows.Forms.GroupBox
  Friend WithEvents ef_server_name As GUI_Controls.uc_entry_field
  Friend WithEvents fra_server_blocked As System.Windows.Forms.GroupBox
  Friend WithEvents chk_server_no As System.Windows.Forms.CheckBox
  Friend WithEvents chk_server_yes As System.Windows.Forms.CheckBox
  Friend WithEvents fra_terminal As System.Windows.Forms.GroupBox
  Friend WithEvents fra_terminal_blocked As System.Windows.Forms.GroupBox
  Friend WithEvents chk_terminal_no As System.Windows.Forms.CheckBox
  Friend WithEvents chk_terminal_yes As System.Windows.Forms.CheckBox
  Friend WithEvents ef_terminal_name As GUI_Controls.uc_entry_field
  Friend WithEvents ef_server_id As GUI_Controls.uc_entry_field
  Friend WithEvents ef_terminal_id As GUI_Controls.uc_entry_field
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.fra_terminal = New System.Windows.Forms.GroupBox
    Me.ef_terminal_id = New GUI_Controls.uc_entry_field
    Me.fra_terminal_blocked = New System.Windows.Forms.GroupBox
    Me.chk_terminal_no = New System.Windows.Forms.CheckBox
    Me.chk_terminal_yes = New System.Windows.Forms.CheckBox
    Me.ef_terminal_name = New GUI_Controls.uc_entry_field
    Me.fra_server = New System.Windows.Forms.GroupBox
    Me.ef_server_id = New GUI_Controls.uc_entry_field
    Me.fra_server_blocked = New System.Windows.Forms.GroupBox
    Me.chk_server_no = New System.Windows.Forms.CheckBox
    Me.chk_server_yes = New System.Windows.Forms.CheckBox
    Me.ef_server_name = New GUI_Controls.uc_entry_field
    Me.chk_only_terminals = New System.Windows.Forms.CheckBox
    Me.dg_terminals = New System.Windows.Forms.DataGrid
    Me.Splitter1 = New System.Windows.Forms.Splitter
    Me.dg_servers = New System.Windows.Forms.DataGrid
    Me.panel_grids.SuspendLayout()
    Me.panel_filter.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.fra_terminal.SuspendLayout()
    Me.fra_terminal_blocked.SuspendLayout()
    Me.fra_server.SuspendLayout()
    Me.fra_server_blocked.SuspendLayout()
    CType(Me.dg_terminals, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.dg_servers, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'panel_grids
    '
    Me.panel_grids.Controls.Add(Me.dg_terminals)
    Me.panel_grids.Controls.Add(Me.Splitter1)
    Me.panel_grids.Controls.Add(Me.dg_servers)
    Me.panel_grids.Location = New System.Drawing.Point(4, 215)
    Me.panel_grids.Size = New System.Drawing.Size(1126, 533)
    Me.panel_grids.Controls.SetChildIndex(Me.panel_buttons, 0)
    Me.panel_grids.Controls.SetChildIndex(Me.dg_servers, 0)
    Me.panel_grids.Controls.SetChildIndex(Me.Splitter1, 0)
    Me.panel_grids.Controls.SetChildIndex(Me.dg_terminals, 0)
    '
    'panel_buttons
    '
    Me.panel_buttons.Location = New System.Drawing.Point(1038, 0)
    Me.panel_buttons.Size = New System.Drawing.Size(88, 533)
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.fra_terminal)
    Me.panel_filter.Controls.Add(Me.fra_server)
    Me.panel_filter.Controls.Add(Me.chk_only_terminals)
    Me.panel_filter.Size = New System.Drawing.Size(1126, 188)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_only_terminals, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.fra_server, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.fra_terminal, 0)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Location = New System.Drawing.Point(4, 192)
    Me.pn_separator_line.Size = New System.Drawing.Size(1126, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1126, 4)
    '
    'fra_terminal
    '
    Me.fra_terminal.Controls.Add(Me.ef_terminal_id)
    Me.fra_terminal.Controls.Add(Me.fra_terminal_blocked)
    Me.fra_terminal.Controls.Add(Me.ef_terminal_name)
    Me.fra_terminal.Location = New System.Drawing.Point(270, 8)
    Me.fra_terminal.Name = "fra_terminal"
    Me.fra_terminal.Size = New System.Drawing.Size(256, 152)
    Me.fra_terminal.TabIndex = 2
    Me.fra_terminal.TabStop = False
    Me.fra_terminal.Text = "xTerminal"
    '
    'ef_terminal_id
    '
    Me.ef_terminal_id.DoubleValue = 0
    Me.ef_terminal_id.IntegerValue = 0
    Me.ef_terminal_id.IsReadOnly = False
    Me.ef_terminal_id.Location = New System.Drawing.Point(6, 46)
    Me.ef_terminal_id.Name = "ef_terminal_id"
    Me.ef_terminal_id.Size = New System.Drawing.Size(242, 24)
    Me.ef_terminal_id.SufixText = "Sufix Text"
    Me.ef_terminal_id.SufixTextVisible = True
    Me.ef_terminal_id.TabIndex = 1
    Me.ef_terminal_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_terminal_id.TextValue = ""
    Me.ef_terminal_id.Value = ""
    '
    'fra_terminal_blocked
    '
    Me.fra_terminal_blocked.Controls.Add(Me.chk_terminal_no)
    Me.fra_terminal_blocked.Controls.Add(Me.chk_terminal_yes)
    Me.fra_terminal_blocked.Location = New System.Drawing.Point(15, 80)
    Me.fra_terminal_blocked.Name = "fra_terminal_blocked"
    Me.fra_terminal_blocked.Size = New System.Drawing.Size(123, 64)
    Me.fra_terminal_blocked.TabIndex = 2
    Me.fra_terminal_blocked.TabStop = False
    Me.fra_terminal_blocked.Text = "xBlocked"
    '
    'chk_terminal_no
    '
    Me.chk_terminal_no.Location = New System.Drawing.Point(28, 42)
    Me.chk_terminal_no.Name = "chk_terminal_no"
    Me.chk_terminal_no.Size = New System.Drawing.Size(72, 16)
    Me.chk_terminal_no.TabIndex = 1
    Me.chk_terminal_no.Text = "No"
    '
    'chk_terminal_yes
    '
    Me.chk_terminal_yes.Location = New System.Drawing.Point(28, 22)
    Me.chk_terminal_yes.Name = "chk_terminal_yes"
    Me.chk_terminal_yes.Size = New System.Drawing.Size(72, 16)
    Me.chk_terminal_yes.TabIndex = 0
    Me.chk_terminal_yes.Text = "Yes"
    '
    'ef_terminal_name
    '
    Me.ef_terminal_name.DoubleValue = 0
    Me.ef_terminal_name.IntegerValue = 0
    Me.ef_terminal_name.IsReadOnly = False
    Me.ef_terminal_name.Location = New System.Drawing.Point(16, 20)
    Me.ef_terminal_name.Name = "ef_terminal_name"
    Me.ef_terminal_name.Size = New System.Drawing.Size(232, 24)
    Me.ef_terminal_name.SufixText = "Sufix Text"
    Me.ef_terminal_name.SufixTextVisible = True
    Me.ef_terminal_name.TabIndex = 0
    Me.ef_terminal_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_terminal_name.TextValue = ""
    Me.ef_terminal_name.TextWidth = 70
    Me.ef_terminal_name.Value = ""
    '
    'fra_server
    '
    Me.fra_server.Controls.Add(Me.ef_server_id)
    Me.fra_server.Controls.Add(Me.fra_server_blocked)
    Me.fra_server.Controls.Add(Me.ef_server_name)
    Me.fra_server.Location = New System.Drawing.Point(8, 8)
    Me.fra_server.Name = "fra_server"
    Me.fra_server.Size = New System.Drawing.Size(256, 152)
    Me.fra_server.TabIndex = 1
    Me.fra_server.TabStop = False
    Me.fra_server.Text = "xServer"
    '
    'ef_server_id
    '
    Me.ef_server_id.DoubleValue = 0
    Me.ef_server_id.IntegerValue = 0
    Me.ef_server_id.IsReadOnly = False
    Me.ef_server_id.Location = New System.Drawing.Point(6, 46)
    Me.ef_server_id.Name = "ef_server_id"
    Me.ef_server_id.Size = New System.Drawing.Size(242, 24)
    Me.ef_server_id.SufixText = "Sufix Text"
    Me.ef_server_id.SufixTextVisible = True
    Me.ef_server_id.TabIndex = 1
    Me.ef_server_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_server_id.TextValue = ""
    Me.ef_server_id.Value = ""
    '
    'fra_server_blocked
    '
    Me.fra_server_blocked.Controls.Add(Me.chk_server_no)
    Me.fra_server_blocked.Controls.Add(Me.chk_server_yes)
    Me.fra_server_blocked.Location = New System.Drawing.Point(15, 80)
    Me.fra_server_blocked.Name = "fra_server_blocked"
    Me.fra_server_blocked.Size = New System.Drawing.Size(123, 64)
    Me.fra_server_blocked.TabIndex = 2
    Me.fra_server_blocked.TabStop = False
    Me.fra_server_blocked.Text = "xBlocked"
    '
    'chk_server_no
    '
    Me.chk_server_no.Location = New System.Drawing.Point(28, 42)
    Me.chk_server_no.Name = "chk_server_no"
    Me.chk_server_no.Size = New System.Drawing.Size(72, 16)
    Me.chk_server_no.TabIndex = 1
    Me.chk_server_no.Text = "No"
    '
    'chk_server_yes
    '
    Me.chk_server_yes.Location = New System.Drawing.Point(28, 22)
    Me.chk_server_yes.Name = "chk_server_yes"
    Me.chk_server_yes.Size = New System.Drawing.Size(72, 16)
    Me.chk_server_yes.TabIndex = 0
    Me.chk_server_yes.Text = "Yes"
    '
    'ef_server_name
    '
    Me.ef_server_name.DoubleValue = 0
    Me.ef_server_name.IntegerValue = 0
    Me.ef_server_name.IsReadOnly = False
    Me.ef_server_name.Location = New System.Drawing.Point(16, 20)
    Me.ef_server_name.Name = "ef_server_name"
    Me.ef_server_name.Size = New System.Drawing.Size(232, 24)
    Me.ef_server_name.SufixText = "Sufix Text"
    Me.ef_server_name.SufixTextVisible = True
    Me.ef_server_name.TabIndex = 0
    Me.ef_server_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_server_name.TextValue = ""
    Me.ef_server_name.TextWidth = 70
    Me.ef_server_name.Value = ""
    '
    'chk_only_terminals
    '
    Me.chk_only_terminals.Checked = True
    Me.chk_only_terminals.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_only_terminals.Location = New System.Drawing.Point(11, 166)
    Me.chk_only_terminals.Name = "chk_only_terminals"
    Me.chk_only_terminals.Size = New System.Drawing.Size(221, 20)
    Me.chk_only_terminals.TabIndex = 3
    Me.chk_only_terminals.Text = "xOnlyTerminals"
    '
    'dg_terminals
    '
    Me.dg_terminals.AllowDrop = True
    Me.dg_terminals.CaptionFont = New System.Drawing.Font("Verdana", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.dg_terminals.CaptionText = "Terminals of Server ---"
    Me.dg_terminals.DataMember = ""
    Me.dg_terminals.Dock = System.Windows.Forms.DockStyle.Fill
    Me.dg_terminals.HeaderFont = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.dg_terminals.HeaderForeColor = System.Drawing.SystemColors.ControlText
    Me.dg_terminals.Location = New System.Drawing.Point(0, 148)
    Me.dg_terminals.Name = "dg_terminals"
    Me.dg_terminals.Size = New System.Drawing.Size(1038, 385)
    Me.dg_terminals.TabIndex = 5
    '
    'Splitter1
    '
    Me.Splitter1.Dock = System.Windows.Forms.DockStyle.Top
    Me.Splitter1.Location = New System.Drawing.Point(0, 140)
    Me.Splitter1.MinExtra = 0
    Me.Splitter1.MinSize = 0
    Me.Splitter1.Name = "Splitter1"
    Me.Splitter1.Size = New System.Drawing.Size(1038, 8)
    Me.Splitter1.TabIndex = 7
    Me.Splitter1.TabStop = False
    '
    'dg_servers
    '
    Me.dg_servers.CaptionFont = New System.Drawing.Font("Verdana", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.dg_servers.CaptionText = "Servers"
    Me.dg_servers.DataMember = ""
    Me.dg_servers.Dock = System.Windows.Forms.DockStyle.Top
    Me.dg_servers.HeaderFont = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.dg_servers.HeaderForeColor = System.Drawing.SystemColors.ControlText
    Me.dg_servers.Location = New System.Drawing.Point(0, 0)
    Me.dg_servers.Name = "dg_servers"
    Me.dg_servers.Size = New System.Drawing.Size(1038, 0)
    Me.dg_servers.TabIndex = 4
    '
    'frm_terminals_edit
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(1134, 752)
    Me.Name = "frm_terminals_edit"
    Me.Text = "frm_terminals_edit"
    Me.panel_grids.ResumeLayout(False)
    Me.panel_filter.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.fra_terminal.ResumeLayout(False)
    Me.fra_terminal_blocked.ResumeLayout(False)
    Me.fra_server.ResumeLayout(False)
    Me.fra_server_blocked.ResumeLayout(False)
    CType(Me.dg_terminals, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.dg_servers, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Enums "

  Private TERMINAL_TYPE_3GS As Integer = 5

#End Region

#Region " Members "

  Private m_sql_commandbuilder As SqlCommandBuilder
  Shared m_sql_conn As SqlConnection
  Private m_sql_adap As SqlDataAdapter
  Private m_sql_dataset As DataSet
  Private m_data_view_servers As DataView
  Private m_data_view_terminals As DataView
  Private m_data_row_view As DataRowView
  Private m_data_relation As DataRelation
  Private m_last_server_row As Integer = -1
  Private m_last_server_column As Integer = -1
  Private m_last_terminal_column As Integer = -1

  ' For report filters 
  Private m_report_server_name As String
  Private m_report_terminal_name As String
  Private m_report_terminal_id As String
  Private m_report_terminal_blocked As String

  ' For terminal selection
  Private m_selection_mode As Boolean
  Private m_selected_terminal_id As Integer

#End Region

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_FORM.FORM_TERMINALS_EDIT

    ' TJG 01-FEB-2005
    ' Set the form icon from the embedded resource (ICO file must be built in the project as "Embedded Resource")
    ' Call could be made as GetManifestResourceStream("GUI_Auditor.GUI_Auditor.ico"))

    '------------------------------------------------
    ' XVV 13/04/2007
    ' Translate tu BASE Form
    'Me.Icon = New Icon(System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream(Me.GetType(), "WigosGUI.ico"))
    '------------------------------------------------

    '------------------------------------------------
    'XVV 13/04/2007
    'Call Base Form proc
    Call MyBase.GUI_SetFormId()
    '------------------------------------------------
  End Sub

  'PURPOSE: Executed just before closing form
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)
    If DiscardChanges() Then
      CloseCanceled = False
    Else
      CloseCanceled = True
    End If
  End Sub

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' - Form title
    Me.Text = GLB_NLS_GUI_AUDITOR.GetString(328)

    ' - Buttons
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_CONTROLS.GetString(13)
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_AUDITOR.GetString(466)

    GUI_Button(ENUM_BUTTON.BUTTON_INFO).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Visible = Not m_selection_mode
    GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Enabled = False
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = Not m_selection_mode
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = Not m_selection_mode
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = False
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = m_selection_mode
    GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False

    chk_only_terminals.Text = GLB_NLS_GUI_AUDITOR.GetString(340)
    chk_server_yes.Text = GLB_NLS_GUI_AUDITOR.GetString(336)
    chk_server_no.Text = GLB_NLS_GUI_AUDITOR.GetString(337)
    chk_terminal_yes.Text = GLB_NLS_GUI_AUDITOR.GetString(336)
    chk_terminal_no.Text = GLB_NLS_GUI_AUDITOR.GetString(337)
    fra_server_blocked.Text = GLB_NLS_GUI_AUDITOR.GetString(332)
    fra_terminal_blocked.Text = GLB_NLS_GUI_AUDITOR.GetString(332)
    fra_server.Text = GLB_NLS_GUI_AUDITOR.GetString(338)
    fra_terminal.Text = GLB_NLS_GUI_AUDITOR.GetString(339)
    ef_server_name.Text = GLB_NLS_GUI_AUDITOR.GetString(330)
    ef_terminal_name.Text = GLB_NLS_GUI_AUDITOR.GetString(330)
    ef_server_id.Text = GLB_NLS_GUI_AUDITOR.GetString(331)
    ef_terminal_id.Text = GLB_NLS_GUI_AUDITOR.GetString(331)

    dg_terminals.CaptionText = GLB_NLS_GUI_AUDITOR.GetString(458)

    Call Me.ef_server_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    Call Me.ef_server_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 40)
    Call Me.ef_terminal_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    Call Me.ef_terminal_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 40)

    dg_servers.Enabled = False
    dg_terminals.Enabled = False

  End Sub ' GUI_InitControls

  ' PURPOSE: Manage permissions.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)

  End Sub 'GUI_Permissions

  ' PURPOSE: Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = fra_server

  End Sub 'GUI_SetInitialFocus

#End Region

#Region " Private Functions "
  

  ' PURPOSE: Manage buttons pressed.
  '
  '  PARAMS:
  '     - INPUT:
  '         - ButtonId: Id. of the button clicked.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        Call GUI_SaveChanges()

      Case ENUM_BUTTON.BUTTON_SELECT
        Call GUI_SelectTerminal()

      Case ENUM_BUTTON.BUTTON_CUSTOM_1
        Call GUI_DisconnectTerminal()

      Case ENUM_BUTTON.BUTTON_CUSTOM_2

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Disconnect EBOX from system.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub GUI_DisconnectTerminal()

    Dim rc As Boolean

    Try
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      ' Execute assign        
      rc = DisconnectTerminal()

      GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = m_data_view_terminals.Count > 0 And Me.Permissions.Write

    Catch ex As Exception
      Debug.WriteLine(ex.Message)
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(113), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ex.Message)

    Finally
      Windows.Forms.Cursor.Current = Cursors.Default
    End Try

  End Sub ' GUI_DisconnectTerminal

  Private Function DisconnectTerminal() As Boolean


    Dim row_selected As DataRowView
    Dim idx_row As Integer
    Dim select_idx As Collection
    Dim has_errors As Boolean
    Dim str_terminal_names As String
    ' Split str_terminal_names in 40 chars params.
    Dim idx_char As Integer
    Dim param(5) As String

    ' If CurrentRowIndex = -1, there is no row selected.
    If dg_terminals.CurrentRowIndex = -1 Then
      Return True
    End If

    select_idx = New Collection()
    has_errors = False
    str_terminal_names = ""

    Try

      ' Get selected terminal
      For idx_row = m_data_view_terminals.Count - 1 To 0 Step -1
        If dg_terminals.IsSelected(idx_row) Then
          select_idx.Add(idx_row)
        End If
      Next

      ' Nothing selected, show error
      If select_idx.Count = 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)
        Return True
      End If

      ' Check if terminal 3G selected
      For Each idx_row In select_idx
        row_selected = m_data_view_terminals(idx_row)
        If Is3GSTerminal(row_selected.Item("TE_TERMINAL_ID")) Then
          str_terminal_names = str_terminal_names & row_selected.Item("TE_NAME")
          Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , str_terminal_names)
          Return True
        End If
      Next

      For Each idx_row In select_idx
        row_selected = m_data_view_terminals(idx_row)
        str_terminal_names = str_terminal_names & row_selected.Item("TE_NAME") & ", "
      Next

      ' Nothing selected, show error
      If select_idx.Count = 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)
        Return True
      End If

      param(0) = ""
      param(1) = ""
      param(2) = ""
      param(3) = ""
      param(4) = ""

      For idx_char = 0 To str_terminal_names.Length - 1
        If (idx_char >= (40 * 5)) Then
          Exit For
        End If
        param(idx_char \ 40) &= str_terminal_names(idx_char)
      Next

      ' MSG confirmation!
      If NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(464), _
                    ENUM_MB_TYPE.MB_TYPE_WARNING, _
                    ENUM_MB_BTN.MB_BTN_YES_NO, _
                    ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
                    param(0), param(1), param(2), param(3), param(4)) = ENUM_MB_RESULT.MB_RESULT_NO Then
        Return True
      End If

      For Each idx_row In select_idx
        row_selected = m_data_view_terminals(idx_row)

        row_selected.Item("TE_EXTERNAL_ID") = DBNull.Value
        row_selected.EndEdit()
      Next

      Return Not has_errors

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)
      has_errors = True
      Return False

    Finally
      If has_errors Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(119), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)
      End If
    End Try

  End Function ' DisconnectTerminal

  Public Function Is3GSTerminal(ByVal TerminalId As Integer) As Boolean

    Dim sql_query As String
    Dim data_table As DataTable

    sql_query = "SELECT COUNT(*) FROM TERMINALS_3GS WHERE T3GS_TERMINAL_ID = " & TerminalId.ToString()

    data_table = GUI_GetTableUsingSQL(sql_query, 5000)
    If IsNothing(data_table) Then
      Return False
    End If

    Return (data_table.Rows(0).Item(0) > 0)

  End Function ' Is3GSTerminal


  ' PURPOSE: 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub GUI_SaveChanges()
    Dim data_table_changes_servers As New DataTable
    Dim data_table_changes_terminals As New DataTable

    Try
      If Not IsNothing(m_data_view_servers) Then
        data_table_changes_servers = m_data_view_servers.Table.GetChanges
      End If

      If Not IsNothing(m_data_view_terminals) Then
        data_table_changes_terminals = m_data_view_terminals.Table.GetChanges
      End If

      '
      ' UPDATE 
      '
      ' Added rows
      m_sql_adap.Update(m_sql_dataset.Tables(0).Select("", "", DataViewRowState.Added))
      m_sql_adap.Update(m_sql_dataset.Tables(1).Select("", "", DataViewRowState.Added))
      ' Modified rows
      m_sql_adap.Update(m_sql_dataset.Tables(0).Select("", "", DataViewRowState.ModifiedCurrent))
      m_sql_adap.Update(m_sql_dataset.Tables(1).Select("", "", DataViewRowState.ModifiedCurrent))
      ' Deleted rows
      m_sql_adap.Update(m_sql_dataset.Tables(1).Select("", "", DataViewRowState.Deleted))
      m_sql_adap.Update(m_sql_dataset.Tables(0).Select("", "", DataViewRowState.Deleted))

      ' Auditory
      GUI_WriteAuditoryChanges(True, data_table_changes_servers)
      GUI_WriteAuditoryChanges(False, data_table_changes_terminals)

      m_sql_dataset.AcceptChanges()

      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(108), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO)

    Catch ex As Exception
      m_sql_dataset.AcceptChanges()
      Debug.WriteLine(ex.Message)
      GUI_ClearGrid()

      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ex.Message)

    Finally
    End Try

  End Sub

  Protected Overrides Sub GUI_FilterReset()

    chk_only_terminals.Checked = True
    ef_server_name.Value = ""
    ef_server_id.Value = ""
    ef_terminal_name.Value = ""
    ef_terminal_id.Value = ""
    chk_server_yes.Checked = False
    chk_server_no.Checked = False
    chk_terminal_yes.Checked = False
    chk_terminal_no.Checked = False

  End Sub 'GUI_FilterReset

  Protected Overrides Function GUI_FilterCheck() As Boolean

    Return True
  End Function 'GUI_FilterCheck

  Private Sub GUI_ClearGrid()

    dg_servers.DataSource = Nothing
    dg_terminals.DataSource = Nothing

  End Sub 'GUI_ClearGrid

  Protected Overrides Sub GUI_ExecuteQuery()
    Dim user_msg As Boolean
    Dim data_table_changes_servers As New DataTable
    Dim data_table_changes_terminals As New DataTable

    Try
      ' Check the filter
      If GUI_FilterCheck() Then

        'XVV 16/04/2007
        'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
        Windows.Forms.Cursor.Current = Cursors.WaitCursor

        ' If pending changes MsgBox
        user_msg = False
        If Not IsNothing(m_data_view_servers) Then
          data_table_changes_servers = m_data_view_servers.Table.GetChanges

          If Not IsNothing(data_table_changes_servers) Then
            If data_table_changes_servers.Rows.Count > 0 Then
              If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(122), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then
                Return
              End If
              user_msg = True
            End If
          End If
        End If

        If Not IsNothing(m_data_view_terminals) And Not user_msg Then
          data_table_changes_terminals = m_data_view_terminals.Table.GetChanges

          If Not IsNothing(data_table_changes_terminals) Then
            If data_table_changes_terminals.Rows.Count > 0 Then
              If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(122), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then
                Return
              End If
            End If
          End If
        End If

        ' Invalidate datagrid
        GUI_ClearGrid()

        ' Execute the query        
        Call ExecuteQuery()

        ' For report filters
        Call GUI_ReportUpdateFilters(Me.dg_terminals)

        GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = Me.Permissions.Write

        GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = Me.Permissions.Write

        GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Enabled = Me.Permissions.Write

        ' Must be greater than 1, because the property AllowNew of DataGrid makes always appear a last row for adding data.
        GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Enabled = dg_terminals.VisibleRowCount > 1

      End If

    Catch exception As Exception
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "TerminalsEdit", _
                            "FilterApply", _
                            "", _
                            "", _
                            "", _
                            exception.Message)

    Finally

      'XVV 16/04/2007
      'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
      Windows.Forms.Cursor.Current = Cursors.Default
    End Try

  End Sub 'GUI_ExecuteQuery

  Private Sub ExecuteQuery()
    Dim column_parent As DataColumn
    Dim column_child As DataColumn
    Dim table_style As DataGridTableStyle
    Dim sql_query As String
    Dim sql_query_servers As String
    Dim sql_query_terminals As String
    'XVV Variable not use Dim sql_command As SqlCommand

    ' Initialize DataView for database
    Try
      m_sql_conn = Nothing
      m_sql_conn = NewSQLConnection()

      If Not (m_sql_conn Is Nothing) Then
        ' Connected
        If m_sql_conn.State <> ConnectionState.Open Then
          ' Connecting ..
          m_sql_conn.Open()
        End If

        If Not m_sql_conn.State = ConnectionState.Open Then
          Exit Try
        End If
      End If

      m_sql_dataset = New DataSet

      ' Build Query -------------------------------------------------
      sql_query_servers = "SELECT * FROM TERMINALS WHERE TE_TYPE = 0 "
      sql_query_terminals = "SELECT * FROM TERMINALS, TERMINAL_TYPES WHERE TE_TYPE = 1 AND TT_TYPE = TE_TERMINAL_TYPE"

      If chk_only_terminals.Checked = True Then
        sql_query_servers = sql_query_servers & " AND TE_SERVER_ID IS NOT NULL"
        sql_query_terminals = sql_query_terminals & " AND TE_SERVER_ID IS NULL"
      Else
        If Len(ef_server_name.Value) > 0 Then
          sql_query_servers = sql_query_servers & " AND " & GUI_FilterField("TE_NAME", ef_server_name.Value)
          sql_query_terminals = sql_query_terminals & " AND TE_SERVER_ID IN (SELECT TE_TERMINAL_ID " & _
                                                                              "FROM TERMINALS " & _
                                                                             "WHERE TE_TYPE = 0 AND " & GUI_FilterField("TE_NAME", ef_server_name.Value) & ")"
        End If

        If Len(ef_server_id.Value) > 0 Then
          sql_query_servers = sql_query_servers & " AND " & GUI_FilterField("TE_EXTERNAL_ID", ef_server_id.Value)
          sql_query_terminals = sql_query_terminals & " AND TE_SERVER_ID IN (SELECT TE_TERMINAL_ID " & _
                                                                              "FROM TERMINALS " & _
                                                                             "WHERE TE_TYPE = 0 AND " & GUI_FilterField("TE_EXTERNAL_ID", ef_server_id.Value) & ")"
        End If

        If chk_server_yes.Checked = True And chk_server_no.Checked = False Then
          sql_query_servers = sql_query_servers & " AND TE_BLOCKED = 1 "
          sql_query_terminals = sql_query_terminals & " AND TE_SERVER_ID IN (SELECT TE_TERMINAL_ID " & _
                                                                              "FROM TERMINALS " & _
                                                                             "WHERE TE_TYPE = 0 AND TE_BLOCKED = 1 )"
        End If

        If chk_server_yes.Checked = False And chk_server_no.Checked = True Then
          sql_query_servers = sql_query_servers & " AND TE_BLOCKED = 0 "
          sql_query_terminals = sql_query_terminals & " AND TE_SERVER_ID IN (SELECT TE_TERMINAL_ID " & _
                                                                              "FROM TERMINALS " & _
                                                                             "WHERE TE_TYPE = 0 AND TE_BLOCKED = 0 )"
        End If
      End If

      If Len(ef_terminal_name.Value) > 0 Then
        sql_query_servers = sql_query_servers & " AND TE_TERMINAL_ID IN (SELECT TE_SERVER_ID " & _
                                                                          "FROM TERMINALS " & _
                                                                         "WHERE TE_TYPE = 1 AND " & GUI_FilterField("TE_NAME", ef_terminal_name.Value) & ")"
        sql_query_terminals = sql_query_terminals & " AND " & GUI_FilterField("TE_NAME", ef_terminal_name.Value)
      End If

      If Len(ef_terminal_id.Value) > 0 Then
        sql_query_servers = sql_query_servers & " AND TE_TERMINAL_ID IN (SELECT TE_SERVER_ID " & _
                                                                          "FROM TERMINALS " & _
                                                                         "WHERE TE_TYPE = 1 AND " & GUI_FilterField("TE_EXTERNAL_ID", ef_terminal_id.Value) & ")"
        sql_query_terminals = sql_query_terminals & " AND " & GUI_FilterField("TE_EXTERNAL_ID", ef_terminal_id.Value)
      End If

      If chk_terminal_yes.Checked = True And chk_terminal_no.Checked = False Then
        sql_query_servers = sql_query_servers & " AND TE_TERMINAL_ID IN (SELECT TE_SERVER_ID " & _
                                                                          "FROM TERMINALS " & _
                                                                         "WHERE TE_TYPE = 1 AND TE_BLOCKED = 1 )"
        sql_query_terminals = sql_query_terminals & " AND TE_BLOCKED = 1 "
      End If
      If chk_terminal_yes.Checked = False And chk_terminal_no.Checked = True Then
        sql_query_servers = sql_query_servers & " AND TE_TERMINAL_ID IN (SELECT TE_SERVER_ID " & _
                                                                          "FROM TERMINALS " & _
                                                                         "WHERE TE_TYPE = 1 AND TE_BLOCKED = 0 )"
        sql_query_terminals = sql_query_terminals & " AND TE_BLOCKED = 0 "
      End If

      sql_query = sql_query_servers & ";" & sql_query_terminals
      ' End Build Query -------------------------------------------------

      If Not IsNothing(m_sql_adap) Then
        m_sql_adap.Dispose()
        m_sql_adap = Nothing
      End If

      m_sql_adap = New SqlDataAdapter(sql_query, m_sql_conn)
      m_sql_adap.FillSchema(m_sql_dataset, SchemaType.Source)
      m_sql_adap.Fill(m_sql_dataset)

      ' Default Values for Table Servers
      m_sql_dataset.Tables(0).Columns("TE_TYPE").DefaultValue = 0
      m_sql_dataset.Tables(0).Columns("TE_ACTIVE").DefaultValue = 1
      m_sql_dataset.Tables(0).Columns("TE_BLOCKED").DefaultValue = 0
      m_sql_dataset.Tables(0).Columns("TE_TERMINAL_ID").AutoIncrement = True
      m_sql_dataset.Tables(0).Columns("TE_TERMINAL_ID").AutoIncrementStep = -1
      m_sql_dataset.Tables(0).Columns("TE_TERMINAL_ID").AutoIncrementSeed = -1
      m_sql_dataset.Tables(0).Columns("TE_TERMINAL_ID").ReadOnly = False

      ' Default Values for Table Terminals
      m_sql_dataset.Tables(1).Columns("TE_TYPE").DefaultValue = 1
      m_sql_dataset.Tables(1).Columns("TE_ACTIVE").DefaultValue = 1
      m_sql_dataset.Tables(1).Columns("TE_BLOCKED").DefaultValue = 0
      m_sql_dataset.Tables(1).Columns("TE_TERMINAL_ID").AutoIncrement = True
      m_sql_dataset.Tables(1).Columns("TE_TERMINAL_ID").AutoIncrementStep = -1
      m_sql_dataset.Tables(1).Columns("TE_TERMINAL_ID").AutoIncrementSeed = -1
      m_sql_dataset.Tables(1).Columns("TE_TERMINAL_ID").ReadOnly = False
      m_sql_dataset.Tables(1).Columns("TE_TERMINAL_TYPE").DefaultValue = 1
      m_sql_dataset.Tables(1).Columns("TT_TYPE").DefaultValue = 1
      m_sql_dataset.Tables(1).Columns("TT_NAME").DefaultValue = "LKT WIN"

      m_sql_commandbuilder = New SqlCommandBuilder(m_sql_adap)
      m_sql_adap.UpdateCommand = m_sql_commandbuilder.GetUpdateCommand
      m_sql_adap.DeleteCommand = m_sql_commandbuilder.GetDeleteCommand
      m_sql_adap.InsertCommand = m_sql_commandbuilder.GetInsertCommand

      ' Add Event get new te_terminal_id in insert statement
      AddHandler m_sql_adap.RowUpdated, New SqlRowUpdatedEventHandler(AddressOf OnRowUpdated)

      If chk_only_terminals.Checked = True Then
        m_data_view_servers = New DataView(m_sql_dataset.Tables(0))
        m_data_view_terminals = New DataView(m_sql_dataset.Tables(1))
      Else
        If m_sql_dataset.Tables(0).Rows.Count = 0 Then
          Return
        End If
        column_parent = m_sql_dataset.Tables(0).Columns("TE_TERMINAL_ID")
        column_child = m_sql_dataset.Tables(1).Columns("TE_SERVER_ID")
        m_data_relation = New DataRelation("Terminals", column_parent, column_child, True)
        m_sql_dataset.Relations.Add(m_data_relation)

        m_data_view_servers = New DataView(m_sql_dataset.Tables(0))
        m_data_row_view = m_data_view_servers(0)
        m_data_view_terminals = m_data_row_view.CreateChildView(m_data_relation)
        m_data_view_terminals.Table = m_sql_dataset.Tables(1)
      End If

      ' Enable Data Grids
      If chk_only_terminals.Checked = True Then
        dg_servers.Enabled = False
        dg_terminals.Enabled = True
        m_sql_dataset.Tables(1).Columns("TE_SERVER_ID").DefaultValue = Nothing
      Else
        dg_servers.Enabled = True
        dg_terminals.Enabled = True
      End If

      dg_servers.DataSource = m_data_view_servers
      dg_terminals.DataSource = m_data_view_terminals

      dg_servers.AllowNavigation = False

      If chk_only_terminals.Checked = True Then
        dg_servers.CaptionText = "---"
        dg_terminals.CaptionText = GLB_NLS_GUI_AUDITOR.GetString(458)
        m_report_server_name = ""
      Else
        dg_servers.CaptionText = GLB_NLS_GUI_AUDITOR.GetString(334)
        dg_terminals.CaptionText = GLB_NLS_GUI_AUDITOR.GetString(335) & " " & m_data_view_servers(0).Item("TE_NAME")
        m_report_server_name = m_data_view_servers(0).Item("TE_NAME")
      End If

      ' Delete Permissions
      dg_terminals.AllowDrop = Me.Permissions.Delete
      dg_servers.AllowDrop = Me.Permissions.Delete

      ' Columns for Servers
      If IsNothing(dg_servers.TableStyles.Item(m_data_view_servers.Table.TableName)) Then
        table_style = New DataGridTableStyle
        table_style.MappingName = m_data_view_servers.Table.TableName
        dg_servers.TableStyles.Add(table_style)
        ' Header Texts

        dg_servers.TableStyles(0).GridColumnStyles("TE_TERMINAL_ID").HeaderText = GLB_NLS_GUI_AUDITOR.GetString(329) & " ."
        dg_servers.TableStyles(0).GridColumnStyles("TE_NAME").HeaderText = GLB_NLS_GUI_AUDITOR.GetString(330)
        dg_servers.TableStyles(0).GridColumnStyles("TE_EXTERNAL_ID").HeaderText = GLB_NLS_GUI_AUDITOR.GetString(331)
        dg_servers.TableStyles(0).GridColumnStyles("TE_BLOCKED").HeaderText = GLB_NLS_GUI_AUDITOR.GetString(332)
        dg_servers.TableStyles(0).GridColumnStyles("TE_PROVIDER_ID").HeaderText = GLB_NLS_GUI_AUDITOR.GetString(341)
        dg_servers.TableStyles(0).GridColumnStyles("TE_TERMINAL_TYPE").HeaderText = GLB_NLS_GUI_SW_DOWNLOAD.GetString(322)
        dg_servers.TableStyles(0).GridColumnStyles("TE_BUILD_ID").HeaderText = GLB_NLS_GUI_AUDITOR.GetString(342)
        dg_servers.TableStyles(0).GridColumnStyles("TE_CLIENT_ID").HeaderText = GLB_NLS_GUI_AUDITOR.GetString(343)
        dg_servers.TableStyles(0).GridColumnStyles("TE_VENDOR_ID").HeaderText = GLB_NLS_GUI_AUDITOR.GetString(344)

        dg_servers.TableStyles(0).GridColumnStyles("TE_TERMINAL_ID").Width = 0
        dg_servers.TableStyles(0).GridColumnStyles("TE_TYPE").Width = 0
        dg_servers.TableStyles(0).GridColumnStyles("TE_SERVER_ID").Width = 0
        dg_servers.TableStyles(0).GridColumnStyles("TE_NAME").Width = 200
        dg_servers.TableStyles(0).GridColumnStyles("TE_EXTERNAL_ID").Width = 190
        dg_servers.TableStyles(0).GridColumnStyles("TE_BLOCKED").Width = 70
        dg_servers.TableStyles(0).GridColumnStyles("TE_ACTIVE").Width = 0
        dg_servers.TableStyles(0).GridColumnStyles("TE_TIMESTAMP").Width = 0
        dg_servers.TableStyles(0).GridColumnStyles("TE_PROVIDER_ID").Width = 90
        dg_servers.TableStyles(0).GridColumnStyles("TE_TERMINAL_TYPE").Width = 0
        dg_servers.TableStyles(0).GridColumnStyles("TE_VENDOR_ID").Width = 90
        ' Alignments
        dg_servers.TableStyles(0).GridColumnStyles("TE_TERMINAL_ID").Alignment = HorizontalAlignment.Right
        ' Read only
        dg_servers.TableStyles(0).GridColumnStyles("TE_EXTERNAL_ID").ReadOnly = True
      End If

      ' Columns for Terminals
      If IsNothing(dg_terminals.TableStyles.Item(m_data_view_terminals.Table.TableName)) Then
        table_style = New DataGridTableStyle
        table_style.MappingName = m_data_view_terminals.Table.TableName
        dg_terminals.TableStyles.Add(table_style)
        ' Header Text
        ' - Server
        dg_terminals.TableStyles(0).GridColumnStyles("TE_SERVER_ID").HeaderText = GLB_NLS_GUI_AUDITOR.GetString(329)
        ' - Terminal
        dg_terminals.TableStyles(0).GridColumnStyles("TE_TERMINAL_ID").HeaderText = GLB_NLS_GUI_AUDITOR.GetString(333)
        ' - Name
        dg_terminals.TableStyles(0).GridColumnStyles("TE_NAME").HeaderText = GLB_NLS_GUI_AUDITOR.GetString(330)
        ' - External Id.
        dg_terminals.TableStyles(0).GridColumnStyles("TE_EXTERNAL_ID").HeaderText = GLB_NLS_GUI_AUDITOR.GetString(331)
        ' - Blocked
        dg_terminals.TableStyles(0).GridColumnStyles("TE_BLOCKED").HeaderText = GLB_NLS_GUI_AUDITOR.GetString(332)
        ' - Provider
        dg_terminals.TableStyles(0).GridColumnStyles("TE_PROVIDER_ID").HeaderText = GLB_NLS_GUI_AUDITOR.GetString(341)
        ' - Type
        dg_terminals.TableStyles(0).GridColumnStyles("TT_NAME").HeaderText = GLB_NLS_GUI_SW_DOWNLOAD.GetString(322)
        ' - Build Id
        dg_terminals.TableStyles(0).GridColumnStyles("TE_BUILD_ID").HeaderText = GLB_NLS_GUI_AUDITOR.GetString(342)
        ' - Client Id
        dg_terminals.TableStyles(0).GridColumnStyles("TE_CLIENT_ID").HeaderText = GLB_NLS_GUI_AUDITOR.GetString(343)

        dg_terminals.TableStyles(0).GridColumnStyles("TE_VENDOR_ID").HeaderText = GLB_NLS_GUI_AUDITOR.GetString(344)



        ' Width
        dg_terminals.TableStyles(0).GridColumnStyles("TE_TERMINAL_ID").Width = 100
        dg_terminals.TableStyles(0).GridColumnStyles("TE_TYPE").Width = 0
        dg_terminals.TableStyles(0).GridColumnStyles("TE_SERVER_ID").Width = 0
        dg_terminals.TableStyles(0).GridColumnStyles("TE_NAME").Width = 200
        dg_terminals.TableStyles(0).GridColumnStyles("TE_EXTERNAL_ID").Width = 190
        dg_terminals.TableStyles(0).GridColumnStyles("TE_BLOCKED").Width = 70
        dg_terminals.TableStyles(0).GridColumnStyles("TE_ACTIVE").Width = 0
        dg_terminals.TableStyles(0).GridColumnStyles("TE_TIMESTAMP").Width = 0
        dg_terminals.TableStyles(0).GridColumnStyles("TE_PROVIDER_ID").Width = 90
        dg_terminals.TableStyles(0).GridColumnStyles("TE_TERMINAL_TYPE").Width = 0
        dg_terminals.TableStyles(0).GridColumnStyles("TT_NAME").Width = 90
        dg_terminals.TableStyles(0).GridColumnStyles("TT_TYPE").Width = 0
        dg_terminals.TableStyles(0).GridColumnStyles("TE_VENDOR_ID").Width = 90
        dg_terminals.TableStyles(0).GridColumnStyles("TE_UNIQUE_EXTERNAL_ID").Width = 0
        ' Alignments
        'dg_terminals.TableStyles(0).GridColumnStyles("TE_TERMINAL_ID").Alignment = HorizontalAlignment.Right

        ' Read only
        'dg_terminals.TableStyles(0).GridColumnStyles("TE_EXTERNAL_ID").ReadOnly = True
        dg_terminals.TableStyles(0).GridColumnStyles("TT_NAME").ReadOnly = True
        dg_terminals.TableStyles(0).GridColumnStyles("TT_TYPE").ReadOnly = True
        dg_terminals.TableStyles(0).GridColumnStyles("TE_TIMESTAMP").ReadOnly = True
        dg_terminals.TableStyles(0).GridColumnStyles("TE_ACTIVE").ReadOnly = True
        dg_terminals.TableStyles(0).GridColumnStyles("TE_SERVER_ID").ReadOnly = True
        dg_terminals.TableStyles(0).GridColumnStyles("TE_TYPE").ReadOnly = True
        dg_terminals.TableStyles(0).GridColumnStyles("TE_BUILD_ID").ReadOnly = True
        dg_terminals.TableStyles(0).GridColumnStyles("TE_CLIENT_ID").ReadOnly = True
        dg_terminals.TableStyles(0).GridColumnStyles("TE_TERMINAL_ID").ReadOnly = True


      End If

      m_last_server_row = -1
      m_last_server_column = -1
      m_last_terminal_column = -1

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)

    Finally
    End Try

  End Sub

  Private Function DiscardChanges() As Boolean
    Dim data_table_changes_servers As New DataTable
    Dim data_table_changes_terminals As New DataTable

    ' If pending changes MsgBox
    If Not IsNothing(m_data_view_servers) Then
      data_table_changes_servers = m_data_view_servers.Table.GetChanges

      If Not IsNothing(data_table_changes_servers) Then
        If data_table_changes_servers.Rows.Count > 0 Then
          If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then
            Return False
          End If
        End If
      End If
    End If

    If Not IsNothing(m_data_view_terminals) Then
      data_table_changes_terminals = m_data_view_terminals.Table.GetChanges

      If Not IsNothing(data_table_changes_terminals) Then
        If data_table_changes_terminals.Rows.Count > 0 Then
          If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then
            Return False
          End If
        End If
      End If
    End If

    Return True
  End Function

  Private Function GUI_CheckChanges(ByVal Server As Boolean, ByVal ChangesDT As DataTable) As Boolean
    Dim idx_change As Integer
    Dim idx_terminal As Integer
    Dim original_deleted_dv As DataView

    If IsNothing(ChangesDT) Then
      Return True
    End If

    If Not Server Then
      Return True
    End If

    original_deleted_dv = New DataView(ChangesDT, "", "", DataViewRowState.Deleted)

    For idx_change = 0 To original_deleted_dv.Count - 1
      For idx_terminal = 0 To m_sql_dataset.Tables(1).Rows.Count - 1
        If original_deleted_dv(idx_change).Item("TE_TERMINAL_ID") = m_sql_dataset.Tables(1).Rows(idx_terminal).Item("TE_SERVER_ID") Then
          Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(110), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO)

          Return False
        End If
      Next
    Next

    Return True
  End Function

  Private Sub GUI_WriteAuditoryChanges(ByVal Server As Boolean, ByVal ChangesDT As DataTable)
    Dim original_modified_dv As DataView
    Dim original_deleted_dv As DataView
    Dim auditor_data As CLASS_AUDITOR_DATA
    Dim original_auditor_data As CLASS_AUDITOR_DATA
    Dim idx_change As Integer
    Dim idx_original As Integer
    Dim value_string As String

    If IsNothing(ChangesDT) Then
      Exit Sub
    End If

    original_modified_dv = New DataView(ChangesDT, "", "", DataViewRowState.ModifiedOriginal)
    original_deleted_dv = New DataView(ChangesDT, "", "", DataViewRowState.Deleted)

    '---------------------------------------------
    ' Insert MODIFIED and ADDED items
    '---------------------------------------------
    For idx_change = 0 To ChangesDT.Rows.Count - 1
      auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)
      original_auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)

      If ChangesDT.Rows(idx_change).RowState <> DataRowState.Deleted Then

        ' Set Name and Identifier
        If Server Then
          Call auditor_data.SetName(GLB_NLS_GUI_AUDITOR.Id(329), ChangesDT.Rows(idx_change).Item("TE_NAME"))
          Call original_auditor_data.SetName(GLB_NLS_GUI_AUDITOR.Id(329), ChangesDT.Rows(idx_change).Item("TE_NAME"))
        Else
          Call auditor_data.SetName(GLB_NLS_GUI_AUDITOR.Id(333), ChangesDT.Rows(idx_change).Item("TE_NAME"))
          Call original_auditor_data.SetName(GLB_NLS_GUI_AUDITOR.Id(333), ChangesDT.Rows(idx_change).Item("TE_NAME"))
        End If

        '
        ' Details for current rows
        '

        ' Name
        value_string = IIf(IsDBNull(ChangesDT.Rows(idx_change).Item("TE_NAME")), "", ChangesDT.Rows(idx_change).Item("TE_NAME"))
        Call auditor_data.SetField(GLB_NLS_GUI_AUDITOR.Id(330), IIf(value_string = "", AUDIT_NONE_STRING, value_string))

        ' External Id
        value_string = IIf(IsDBNull(ChangesDT.Rows(idx_change).Item("TE_EXTERNAL_ID")), "", ChangesDT.Rows(idx_change).Item("TE_EXTERNAL_ID"))
        Call auditor_data.SetField(GLB_NLS_GUI_AUDITOR.Id(331), IIf(value_string = "", AUDIT_NONE_STRING, value_string))

        ' Blocked
        value_string = IIf(IsDBNull(ChangesDT.Rows(idx_change).Item("TE_BLOCKED")), "", ChangesDT.Rows(idx_change).Item("TE_BLOCKED"))
        Call auditor_data.SetField(GLB_NLS_GUI_AUDITOR.Id(332), IIf(value_string = "", AUDIT_NONE_STRING, value_string))

        ' Search original row
        If ChangesDT.Rows(idx_change).RowState = DataRowState.Modified Then
          For idx_original = 0 To original_modified_dv.Count - 1
            If original_modified_dv(idx_original).Item("TE_TERMINAL_ID") = ChangesDT.Rows(idx_change).Item("TE_TERMINAL_ID") Then
              Exit For
            End If
          Next

          ' Critical Error: exit
          If idx_original = original_modified_dv.Count Then
            ' Logger message 

            Exit Sub
          End If

          '
          ' Details for original rows
          '

          ' Name
          value_string = IIf(IsDBNull(original_modified_dv(idx_original).Item("TE_NAME")), "", original_modified_dv(idx_original).Item("TE_NAME"))
          Call original_auditor_data.SetField(GLB_NLS_GUI_AUDITOR.Id(330), IIf(value_string = "", AUDIT_NONE_STRING, value_string))

          ' External Id
          value_string = IIf(IsDBNull(original_modified_dv(idx_original).Item("TE_EXTERNAL_ID")), "", original_modified_dv(idx_original).Item("TE_EXTERNAL_ID"))
          Call original_auditor_data.SetField(GLB_NLS_GUI_AUDITOR.Id(331), IIf(value_string = "", AUDIT_NONE_STRING, value_string))

          ' Blocked
          value_string = IIf(IsDBNull(original_modified_dv(idx_original).Item("TE_BLOCKED")), "", original_modified_dv(idx_original).Item("TE_BLOCKED"))
          Call original_auditor_data.SetField(GLB_NLS_GUI_AUDITOR.Id(332), IIf(value_string = "", AUDIT_NONE_STRING, value_string))

        End If

        Select Case ChangesDT.Rows(idx_change).RowState
          Case DataRowState.Modified
            If Not auditor_data.Notify(GLB_CurrentUser.GuiId, _
                                       GLB_CurrentUser.Id, _
                                       GLB_CurrentUser.Name, _
                                       CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.UPDATE, _
                                       0, _
                                       original_auditor_data) Then
              ' Logger message 
            End If

          Case DataRowState.Added
            If Not auditor_data.Notify(GLB_CurrentUser.GuiId, _
                                       GLB_CurrentUser.Id, _
                                       GLB_CurrentUser.Name, _
                                       CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.INSERT, _
                                       0) Then
              ' Logger message 
            End If

          Case Else
            ' Logger message

        End Select

        auditor_data = Nothing
        original_auditor_data = Nothing
      End If
    Next

    '---------------------------------------------
    ' Insert DELETED items
    '---------------------------------------------
    For idx_original = 0 To original_deleted_dv.Count - 1
      auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)

      ' Set Name and Identifier
      If Server Then
        Call auditor_data.SetName(GLB_NLS_GUI_AUDITOR.Id(329), original_deleted_dv(idx_original).Item("TE_NAME"))
      Else
        Call auditor_data.SetName(GLB_NLS_GUI_AUDITOR.Id(333), original_deleted_dv(idx_original).Item("TE_NAME"))
      End If

      '
      ' Details for original rows
      '

      ' Name
      value_string = IIf(IsDBNull(original_deleted_dv(idx_original).Item("TE_NAME")), "", original_deleted_dv(idx_original).Item("TE_NAME"))
      Call auditor_data.SetField(GLB_NLS_GUI_AUDITOR.Id(330), IIf(value_string = "", AUDIT_NONE_STRING, value_string))

      ' External Id
      value_string = IIf(IsDBNull(original_deleted_dv(idx_original).Item("TE_EXTERNAL_ID")), "", original_deleted_dv(idx_original).Item("TE_EXTERNAL_ID"))
      Call auditor_data.SetField(GLB_NLS_GUI_AUDITOR.Id(331), IIf(value_string = "", AUDIT_NONE_STRING, value_string))

      ' Blocked
      value_string = IIf(IsDBNull(original_deleted_dv(idx_original).Item("TE_BLOCKED")), "", original_deleted_dv(idx_original).Item("TE_BLOCKED"))
      Call auditor_data.SetField(GLB_NLS_GUI_AUDITOR.Id(332), IIf(value_string = "", AUDIT_NONE_STRING, value_string))

      If Not auditor_data.Notify(GLB_CurrentUser.GuiId, _
                                 GLB_CurrentUser.Id, _
                                 GLB_CurrentUser.Name, _
                                 CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.DELETE, _
                                 0) Then
        ' Logger message 
      End If

      auditor_data = Nothing
      original_auditor_data = Nothing
    Next

  End Sub

  ' PURPOSE: Select terminal from list and return
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_SelectTerminal()

    Dim idx_row As Integer
    Dim select_idx As Collection
    Dim data_row As DataRowView

    select_idx = New Collection()

    ' Get selected terminal
    For idx_row = m_data_view_terminals.Count - 1 To 0 Step -1
      If dg_terminals.IsSelected(idx_row) Then
        data_row = m_data_view_terminals(idx_row)

        ' Read internal ID
        ' Save internal ID in m_selected_terminal_id
        m_selected_terminal_id = data_row.Item(0)
        Exit For

      End If
    Next

    ' close screen (as in Cancel)
    Call GUI_ButtonClick(ENUM_BUTTON.BUTTON_CANCEL)

  End Sub ' GUI_SelectTerminal

#End Region

#Region " Public functions "
  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

  ' PURPOSE: Opens dialog with default settings for select mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - SelResType: Fix filter to this type of results
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - List of ProfileId or UserId selected
  Public Function ShowForSelect() As Integer

    m_selection_mode = True

    Me.Display(True)

    m_selection_mode = False

    Return m_selected_terminal_id

  End Function

#End Region

#Region " DataGrid Events "

  Private Sub dg_servers_CurrentCellChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dg_servers.CurrentCellChanged
    Dim grid_cell As DataGridCell


    If dg_servers.CurrentRowIndex <> -1 Then

      grid_cell = dg_servers.CurrentCell()
      If grid_cell.ColumnNumber = 0 Or _
         grid_cell.ColumnNumber = 1 Or _
         grid_cell.ColumnNumber = 2 Or _
         grid_cell.ColumnNumber = 6 Then

        If m_last_server_column > grid_cell.ColumnNumber Then
          If grid_cell.ColumnNumber > 0 Then
            grid_cell.ColumnNumber = grid_cell.ColumnNumber - 1
          Else
            grid_cell.ColumnNumber = m_last_server_column
          End If
        Else
          If grid_cell.ColumnNumber < 6 Then
            grid_cell.ColumnNumber = grid_cell.ColumnNumber + 1
          Else
            grid_cell.ColumnNumber = m_last_server_column
          End If

        End If

        dg_servers.CurrentCell = grid_cell

      End If

      m_last_server_column = dg_servers.CurrentCell.ColumnNumber

      If m_last_server_row <> dg_servers.CurrentRowIndex Then
        m_data_row_view = m_data_view_servers(dg_servers.CurrentRowIndex)
        m_data_view_terminals = m_data_row_view.CreateChildView(m_data_relation)
        m_data_view_terminals.Table = m_sql_dataset.Tables(1)
        dg_terminals.DataSource = m_data_view_terminals
        dg_terminals.CaptionText = GLB_NLS_GUI_AUDITOR.GetString(335) & " " & m_data_view_servers(dg_servers.CurrentRowIndex).Item("TE_NAME")
        m_report_server_name = m_data_view_servers(dg_servers.CurrentRowIndex).Item("TE_NAME")
      End If
    Else
      dg_terminals.DataSource = Nothing
      dg_terminals.CaptionText = "Nothing"
    End If

    m_last_server_row = dg_servers.CurrentRowIndex

  End Sub

  Private Sub dg_terminals_CurrentCellChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dg_terminals.CurrentCellChanged
    Dim grid_cell As DataGridCell

    If dg_servers.CurrentRowIndex <> -1 Then

      grid_cell = dg_terminals.CurrentCell()
      If grid_cell.ColumnNumber = 0 Or _
         grid_cell.ColumnNumber = 1 Or _
         grid_cell.ColumnNumber = 2 Or _
         grid_cell.ColumnNumber = 6 Then

        If m_last_server_column > grid_cell.ColumnNumber Then
          If grid_cell.ColumnNumber > 0 Then
            grid_cell.ColumnNumber = grid_cell.ColumnNumber - 1
          Else
            grid_cell.ColumnNumber = m_last_server_column
          End If
        Else
          If grid_cell.ColumnNumber < 6 Then
            grid_cell.ColumnNumber = grid_cell.ColumnNumber + 1
          Else
            grid_cell.ColumnNumber = m_last_server_column
          End If

        End If

        dg_terminals.CurrentCell = grid_cell

      End If

      m_last_terminal_column = dg_terminals.CurrentCell.ColumnNumber

    End If

  End Sub

  Private Sub dg_servers_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dg_servers.KeyDown
    Dim idx_terminal As Integer

    If e.KeyCode = Keys.Delete And e.KeyData = Keys.Delete Then
      If dg_servers.CurrentRowIndex <> -1 Then
        For idx_terminal = 0 To m_data_view_terminals.Count - 1

          If m_data_view_terminals(idx_terminal).Item("TE_SERVER_ID") = m_data_view_servers(dg_servers.CurrentRowIndex).Item("TE_TERMINAL_ID") Then
            Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(110), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO)

            e.Handled = True
          End If
        Next
      End If
    End If

    e.Handled = False

  End Sub

#End Region

#Region " Sql Server Events "

  Private Shared Sub OnRowUpdated(ByVal sender As Object, ByVal args As SqlRowUpdatedEventArgs)
    Dim new_id As Integer = 0
    Dim db_identity_cmd As SqlCommand = New SqlCommand("SELECT @@IDENTITY", m_sql_conn)

    If args.StatementType = StatementType.Insert Then
      ' Retrieve the identity value and store it in the CategoryID column.
      new_id = CInt(db_identity_cmd.ExecuteScalar())
      args.Row("TE_TERMINAL_ID") = new_id
    End If

  End Sub

#End Region

#Region " CheckBox Events "

  Private Sub chk_only_terminals_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk_only_terminals.CheckStateChanged

    If chk_only_terminals.Checked = True Then
      fra_server.Enabled = False
      Me.dg_servers.Size = New System.Drawing.Size(1038, 0)
    Else
      fra_server.Enabled = True
      Me.dg_servers.Size = New System.Drawing.Size(1038, 140)
    End If

  End Sub

#End Region

#Region " DataGrid Events "

  Private Sub dg_servers_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dg_servers.Enter
    Me.CancelButton = Nothing
  End Sub

  Private Sub dg_servers_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dg_servers.Leave
    Me.CancelButton = GUI_Button(ENUM_BUTTON.BUTTON_CANCEL)
  End Sub

  Private Sub dg_terminals_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dg_terminals.Enter
    Me.CancelButton = Nothing
  End Sub

  Private Sub dg_terminals_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dg_terminals.Leave
    Me.CancelButton = GUI_Button(ENUM_BUTTON.BUTTON_CANCEL)
  End Sub

#End Region ' DataGrid Events

#Region " GUI Reports "

  ' PURPOSE: Initialize report filters.
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData As GUI_Reports.CLASS_PRINT_DATA
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(339), m_report_terminal_name)
    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(331), m_report_terminal_id)
    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(332), m_report_terminal_blocked)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)

    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(338), m_report_server_name)

    PrintData.FilterValueWidth(1) = 3000
    PrintData.FilterValueWidth(2) = 3000

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Update report filters.
  '
  '  PARAMS:
  '     - INPUT:
  '           - MyDataGrid As DataGrid
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters(ByVal MyDataGrid As DataGrid)

    Call MyBase.GUI_ReportUpdateFilters(MyDataGrid)

    m_report_terminal_name = Me.ef_terminal_name.Value
    m_report_terminal_id = Me.ef_terminal_id.Value

    m_report_terminal_blocked = ""
    If Me.chk_terminal_yes.Checked Then
      m_report_terminal_blocked = GLB_NLS_GUI_AUDITOR.GetString(336)
    ElseIf Me.chk_terminal_no.Checked Then
      m_report_terminal_blocked = GLB_NLS_GUI_AUDITOR.GetString(337)
    End If

    ' m_report_server_name is also updated in other places in the code...

    If chk_only_terminals.Checked Then
      m_report_server_name = "(" & GLB_NLS_GUI_AUDITOR.GetString(340) & ")"
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

End Class
