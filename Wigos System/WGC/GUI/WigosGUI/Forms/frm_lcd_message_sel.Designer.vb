<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_lcd_message_sel
  Inherits GUI_Controls.frm_base_sel_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.gb_enabled = New System.Windows.Forms.GroupBox
    Me.chk_current = New System.Windows.Forms.CheckBox
    Me.chk_disabled = New System.Windows.Forms.CheckBox
    Me.chk_enabled = New System.Windows.Forms.CheckBox
    Me.gb_date = New System.Windows.Forms.GroupBox
    Me.dtp_to = New GUI_Controls.uc_date_picker
    Me.dtp_from = New GUI_Controls.uc_date_picker
    Me.gb_results = New System.Windows.Forms.GroupBox
    Me.lbl_color_site = New System.Windows.Forms.Label
    Me.lbl_color_multisite = New System.Windows.Forms.Label
    Me.chk_site = New System.Windows.Forms.CheckBox
    Me.chk_multisite = New System.Windows.Forms.CheckBox
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_enabled.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_results.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_results)
    Me.panel_filter.Controls.Add(Me.gb_enabled)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1117, 92)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_enabled, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_results, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 96)
    Me.panel_data.Size = New System.Drawing.Size(1117, 536)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1111, 27)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1111, 5)
    '
    'gb_enabled
    '
    Me.gb_enabled.Controls.Add(Me.chk_current)
    Me.gb_enabled.Controls.Add(Me.chk_disabled)
    Me.gb_enabled.Controls.Add(Me.chk_enabled)
    Me.gb_enabled.Location = New System.Drawing.Point(256, 1)
    Me.gb_enabled.Name = "gb_enabled"
    Me.gb_enabled.Size = New System.Drawing.Size(161, 88)
    Me.gb_enabled.TabIndex = 12
    Me.gb_enabled.TabStop = False
    '
    'chk_current
    '
    Me.chk_current.AutoSize = True
    Me.chk_current.Location = New System.Drawing.Point(26, 17)
    Me.chk_current.Name = "chk_current"
    Me.chk_current.Size = New System.Drawing.Size(77, 17)
    Me.chk_current.TabIndex = 2
    Me.chk_current.Text = "xCurrent"
    Me.chk_current.UseVisualStyleBackColor = True
    '
    'chk_disabled
    '
    Me.chk_disabled.AutoSize = True
    Me.chk_disabled.Location = New System.Drawing.Point(26, 63)
    Me.chk_disabled.Name = "chk_disabled"
    Me.chk_disabled.Size = New System.Drawing.Size(82, 17)
    Me.chk_disabled.TabIndex = 1
    Me.chk_disabled.Text = "xDisabled"
    Me.chk_disabled.UseVisualStyleBackColor = True
    '
    'chk_enabled
    '
    Me.chk_enabled.AutoSize = True
    Me.chk_enabled.Location = New System.Drawing.Point(26, 40)
    Me.chk_enabled.Name = "chk_enabled"
    Me.chk_enabled.Size = New System.Drawing.Size(78, 17)
    Me.chk_enabled.TabIndex = 0
    Me.chk_enabled.Text = "xEnabled"
    Me.chk_enabled.UseVisualStyleBackColor = True
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(6, 1)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(244, 88)
    Me.gb_date.TabIndex = 11
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(7, 51)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(222, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 7
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(7, 19)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(222, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 6
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'gb_results
    '
    Me.gb_results.Controls.Add(Me.lbl_color_site)
    Me.gb_results.Controls.Add(Me.lbl_color_multisite)
    Me.gb_results.Controls.Add(Me.chk_site)
    Me.gb_results.Controls.Add(Me.chk_multisite)
    Me.gb_results.Location = New System.Drawing.Point(425, 1)
    Me.gb_results.Name = "gb_results"
    Me.gb_results.Size = New System.Drawing.Size(131, 88)
    Me.gb_results.TabIndex = 4
    Me.gb_results.TabStop = False
    '
    'lbl_color_site
    '
    Me.lbl_color_site.BackColor = System.Drawing.Color.White
    Me.lbl_color_site.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_color_site.Location = New System.Drawing.Point(108, 53)
    Me.lbl_color_site.Name = "lbl_color_site"
    Me.lbl_color_site.Size = New System.Drawing.Size(16, 16)
    Me.lbl_color_site.TabIndex = 106
    '
    'lbl_color_multisite
    '
    Me.lbl_color_multisite.BackColor = System.Drawing.Color.White
    Me.lbl_color_multisite.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_color_multisite.Location = New System.Drawing.Point(108, 25)
    Me.lbl_color_multisite.Name = "lbl_color_multisite"
    Me.lbl_color_multisite.Size = New System.Drawing.Size(16, 16)
    Me.lbl_color_multisite.TabIndex = 105
    '
    'chk_site
    '
    Me.chk_site.Location = New System.Drawing.Point(10, 53)
    Me.chk_site.Name = "chk_site"
    Me.chk_site.Size = New System.Drawing.Size(107, 17)
    Me.chk_site.TabIndex = 1
    Me.chk_site.Text = "xSite"
    '
    'chk_multisite
    '
    Me.chk_multisite.Location = New System.Drawing.Point(10, 25)
    Me.chk_multisite.Name = "chk_multisite"
    Me.chk_multisite.Size = New System.Drawing.Size(107, 17)
    Me.chk_multisite.TabIndex = 0
    Me.chk_multisite.Text = "xMultiSite"
    '
    'frm_lcd_message_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1127, 636)
    Me.Name = "frm_lcd_message_sel"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_lcd_message_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_enabled.ResumeLayout(False)
    Me.gb_enabled.PerformLayout()
    Me.gb_date.ResumeLayout(False)
    Me.gb_results.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_enabled As System.Windows.Forms.GroupBox
  Friend WithEvents chk_disabled As System.Windows.Forms.CheckBox
  Friend WithEvents chk_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents gb_results As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_color_site As System.Windows.Forms.Label
  Friend WithEvents lbl_color_multisite As System.Windows.Forms.Label
  Friend WithEvents chk_site As System.Windows.Forms.CheckBox
  Friend WithEvents chk_multisite As System.Windows.Forms.CheckBox
  Friend WithEvents chk_current As System.Windows.Forms.CheckBox
End Class
