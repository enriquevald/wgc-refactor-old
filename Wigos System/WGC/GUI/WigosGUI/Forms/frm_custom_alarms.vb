'-------------------------------------------------------------------
' Copyright © 2007-2014 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_custom_alarms
' DESCRIPTION:   Set config of alarms
' AUTHOR:        Jaume Barnés
' CREATION DATE: 10-APR-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 10-APR-2014  JBC    Initial version.
' 29-APR-2014  AMF    Fixed Bug WIG-868: Several errors solved
' 06-MAY-2014  JBC    Fixed Bug WIG-868: Several errors solved
' 20-OCT-2015  GMV    TFS 5275 : HighRollers GUI Configuración
' 11-NOV-2015  GMV    TFS 5275 : HighRollers Extended definition
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common
Imports System.Text

Public Class frm_custom_alarms
  Inherits frm_base_edit

#Region " Constants "
  Private Const PAYOUT_MAX_VALUE As Int32 = 100
  Private Const DAY_HOURS As Int32 = 24
  Private Const MIN_HOURS As Int32 = 1
  Private Const MAX_HOURS As Int32 = 1440
#End Region

#Region " Members "
  Private WithEvents m_timer As Timer
#End Region

#Region " Overrides "
  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    m_timer = New Timer()

    m_timer.Enabled = True
    m_timer.Interval = 100

    m_timer.Start()

    'PLAYED
    Me.chk_played_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(537) '"Jugado"
    Me.cb_played_pus_unit.Add(1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2582).ToLower)
    Me.cb_played_pus_unit.Add(2, GLB_NLS_GUI_PLAYER_TRACKING.GetString(299).ToLower)
    Me.lbl_played_pus_text.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4854)
    Me.chk_played_quantity.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4844)
    Me.chk_played_amount.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2916)
    Me.ef_played_pus_value.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 4)
    Me.ef_played_quantity_value.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 7)
    Me.ef_played_amount_value.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 10, 2)
    Me.ef_played_pus_value.Text = String.Empty
    Me.ef_played_pus_value.Value = String.Empty
    Me.ef_played_quantity_value.Value = String.Empty
    Me.ef_played_amount_value.Value = String.Empty

    'WON
    Me.chk_won_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(798)
    Me.cb_won_pus_unit.Add(1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2582).ToLower)
    Me.cb_won_pus_unit.Add(2, GLB_NLS_GUI_PLAYER_TRACKING.GetString(299).ToLower)
    Me.chk_won_quantity.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4844)
    Me.lbl_won_pus_text.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4854)
    Me.chk_won_amount.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2916)
    Me.ef_won_quantity_value.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 7)
    Me.ef_won_pus_value.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 4)
    Me.ef_won_amount_value.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 10, 2)
    Me.ef_won_quantity_value.Value = String.Empty
    Me.ef_won_pus_value.Text = String.Empty
    Me.ef_won_pus_value.Value = String.Empty
    Me.ef_won_amount_value.Value = String.Empty

    'JACKPOT
    Me.chk_jackpot_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4850)
    Me.lbl_jackpot_pus_text.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4854)
    Me.cb_jackpot_pus_unit.Add(1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2582).ToLower)
    Me.cb_jackpot_pus_unit.Add(2, GLB_NLS_GUI_PLAYER_TRACKING.GetString(299).ToLower)
    Me.chk_jackpot_quantity.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4844) 'Jackpot Number
    Me.chk_jackpot_amount.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2916)
    Me.ef_jackpot_pus_value.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 4)
    Me.ef_jackpot_quantity_value.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 7)
    Me.ef_jackpot_amount_value.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 10, 2)
    Me.ef_jackpot_pus_value.Text = String.Empty
    Me.ef_jackpot_pus_value.Value = String.Empty
    Me.ef_jackpot_quantity_value.Value = String.Empty
    Me.ef_jackpot_amount_value.Value = String.Empty

    'CANCELED CREDITS
    Me.chk_canceled_credits_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4857) 'Canceled credits
    Me.lbl_cc_pus_text.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4854)
    Me.cb_canceled_credits_pus_unit.Add(1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2582).ToLower)
    Me.cb_canceled_credits_pus_unit.Add(2, GLB_NLS_GUI_PLAYER_TRACKING.GetString(299).ToLower)
    Me.chk_canceled_credits_quantity.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4844)
    Me.chk_canceled_credits_amount.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2916)
    Me.ef_canceled_credits_pus_value.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 4)
    Me.ef_canceled_credits_quantity_value.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 7)
    Me.ef_canceled_credits_amount_value.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 10, 2)
    Me.ef_canceled_credits_pus_value.Text = String.Empty
    Me.ef_canceled_credits_pus_value.Value = String.Empty
    Me.ef_canceled_credits_quantity_value.Value = String.Empty
    Me.ef_canceled_credits_amount_value.Value = String.Empty

    'PAYOUT
    Me.chk_payout_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1924) '"Payout"
    Me.lbl_payout_pus_text.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4854)
    Me.cb_payout_pus_unit.Add(1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2582).ToLower)
    Me.cb_payout_pus_unit.Add(2, GLB_NLS_GUI_PLAYER_TRACKING.GetString(299).ToLower)
    Me.lbl_payout_limit_lower.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4841)
    Me.lbl_payout_limit_upper.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4842)
    Me.ef_payout_pus_value.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 4)
    Me.ef_payout_limit_lower.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 5, 2)
    Me.ef_payout_limit_upper.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 5, 2)
    Me.ef_payout_limit_upper.Text = String.Empty
    Me.ef_payout_limit_upper.Value = String.Empty
    Me.ef_payout_limit_lower.Value = String.Empty
    Me.ef_payout_pus_value.Text = String.Empty
    Me.ef_payout_pus_value.Value = String.Empty
    Me.ef_payout_limit_lower.Text = String.Empty

    'COUNTERFEITS
    Me.chk_counterfeits_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4843)
    Me.lbl_counterfeits_pus_text.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4854)
    Me.cb_counterfeits_pus_unit.Add(1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2582).ToLower)
    Me.cb_counterfeits_pus_unit.Add(2, GLB_NLS_GUI_PLAYER_TRACKING.GetString(299).ToLower)
    Me.lbl_counterfeits_quantity_value.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4844)
    Me.ef_counterfeits_quantity_value.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 7)
    Me.ef_counterfeits_pus_value.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 4)
    Me.ef_counterfeits_pus_value.Text = String.Empty
    Me.ef_counterfeits_pus_value.Value = String.Empty
    Me.ef_counterfeits_quantity_value.Text = String.Empty
    Me.ef_counterfeits_quantity_value.Value = String.Empty

    'TERMINAL ACTIVITY
    Me.chk_terminal_activity_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4852)
    Me.lbl_terminal_activity_pus_text.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4854)
    Me.ef_terminal_activity_pus_value.Text = String.Empty
    Me.ef_terminal_activity_pus_value.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 4)
    Me.cb_terminal_activity_pus_unit.Add(1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2582).ToLower)
    Me.cb_terminal_activity_pus_unit.Add(2, GLB_NLS_GUI_PLAYER_TRACKING.GetString(299).ToLower)


    'HIGHROLLER
    Me.chk_highroller_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6892)
    Me.chk_highroller_betavg.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6893)
    Me.ef_highroller_bet_average.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 10, 2)
    Me.ef_highroller_pus_value.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 4)
    Me.cb_highroller_pus_unit.Add(3, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6895).ToLower)
    Me.lbl_highroller_pus_text.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4854)
    Me.ef_highroller_coinin.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 10, 2)
    Me.chk_highroller_coinin.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1387)
    Me.chk_highroller_include_personal.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6896)
    Me.ef_highroller_pus_value.Text = String.Empty
    Me.ef_highroller_bet_average.Text = String.Empty
    Me.ef_highroller_coinin.Text = String.Empty


    Me.gb_played.Enabled = False
    Me.gb_won.Enabled = False
    Me.gb_jackpot.Enabled = False
    Me.gb_canceled_credits.Enabled = False
    Me.gb_payout.Enabled = False
    Me.gb_counterfeits.Enabled = False
    Me.gb_terminal_activity.Enabled = False
    Me.gb_highroller.Enabled = False


    Call SetDefaultValues()

    GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(2)
    GUI_Button(ENUM_BUTTON.BUTTON_OK).Text = GLB_NLS_GUI_CONTROLS.GetString(1)
    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False

  End Sub

  ' PURPOSE: Set form default values
  '
  ' RETURNS:
  Private Sub SetDefaultValues()

    Me.cb_played_pus_unit.Value = 0
    Me.cb_jackpot_pus_unit.Value = 0
    Me.cb_canceled_credits_pus_unit.Value = 0
    Me.cb_payout_pus_unit.Value = 0
    Me.cb_counterfeits_pus_unit.Value = 0
    Me.cb_terminal_activity_pus_unit.Value = 0
    Me.cb_highroller_pus_unit.Value = 3

    Me.ef_played_amount_value.Value = String.Empty
    Me.ef_won_amount_value.Value = String.Empty
    Me.ef_jackpot_amount_value.Value = String.Empty
    Me.ef_canceled_credits_amount_value.Value = String.Empty
    Me.ef_payout_limit_lower.Value = String.Empty
    Me.ef_payout_limit_upper.Value = String.Empty
    Me.ef_highroller_bet_average.Value = String.Empty
    Me.ef_highroller_coinin.Value = String.Empty

  End Sub

  ' PURPOSE : Check if screen data is ok before saving
  '
  '  PARAMS :
  '     -  INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '
  '   NOTES :
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Try

      'PLAYED
      If Me.chk_played_enabled.Checked Then

        If Not Me.chk_played_quantity.Checked And Not Me.chk_played_amount.Checked Then
          Me.chk_played_quantity.Focus()
          NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), GLB_NLS_GUI_PLAYER_TRACKING.GetString(4862, _
                            Me.chk_played_enabled.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

          Return False
        End If

        If Me.chk_played_quantity.Checked Then
          If String.IsNullOrEmpty(Me.ef_played_quantity_value.Value) OrElse Me.ef_played_quantity_value.Value < 0 Then
            Me.ef_played_quantity_value.Focus()
            NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_VALID_MSG_BLINK_GENERIC", _
                              Me.chk_played_quantity.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

            Return False
          End If
        End If

        'PERIOD UNDER STUDY
        If String.IsNullOrEmpty(Me.ef_played_pus_value.Value) OrElse Me.ef_played_pus_value.Value <= 0 Then
          Me.ef_played_pus_value.Focus()
          NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_VALID_MSG_BLINK_GENERIC", _
                            Me.lbl_played_pus_text.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

          Return False
        Else
          If String.IsNullOrEmpty(Me.cb_played_pus_unit.Value) OrElse Me.cb_played_pus_unit.Value = 0 Then
            Me.cb_played_pus_unit.Focus()
            NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_VALID_MSG_BLINK_GENERIC", _
                              Me.lbl_played_pus_text.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)


            Return False
          End If
        End If

        If Not CheckPUS(Me.cb_played_pus_unit.Value, Me.ef_played_pus_value.Value) Then
          Me.cb_played_pus_unit.Focus()
          NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1190, _
                            Me.lbl_played_pus_text.Text, "1 " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(259), _
                            "60 " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(258)), ENUM_MB_TYPE.MB_TYPE_WARNING)

          Return False
        End If 'PERIOD UNDER STUDY

          If Me.chk_played_amount.Checked Then
            If String.IsNullOrEmpty(Me.ef_played_amount_value.Value) OrElse Me.ef_played_amount_value.Value < 0 Then
              Me.chk_played_amount.Focus()
              NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), GLB_NLS_GUI_PLAYER_TRACKING.GetString(4862, _
                                Me.chk_played_amount.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

              Return False
            End If
          End If 'Me.chk_played_enabled.Checked 
        End If

        'WON
        If Me.chk_won_enabled.Checked Then

          If Not Me.chk_won_quantity.Checked And Not Me.chk_won_amount.Checked Then
            Me.chk_played_quantity.Focus()
            NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), GLB_NLS_GUI_PLAYER_TRACKING.GetString(4862, _
                              Me.chk_won_enabled.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

            Return False
          End If

          'PERIOD UNDER STUDY
          If String.IsNullOrEmpty(Me.ef_won_pus_value.Value) OrElse Me.ef_won_pus_value.Value <= 0 Then
            Me.ef_won_pus_value.Focus()
            NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_VALID_MSG_BLINK_GENERIC", _
                              Me.lbl_won_pus_text.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

            Return False
          Else
            If String.IsNullOrEmpty(Me.cb_won_pus_unit.Value) OrElse Me.cb_won_pus_unit.Value = 0 Then
              Me.cb_won_pus_unit.Focus()
              NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_VALID_MSG_BLINK_GENERIC", _
                                Me.lbl_won_pus_text.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)


              Return False
            End If
          End If

        If Not CheckPUS(Me.cb_won_pus_unit.Value, Me.ef_won_pus_value.Value) Then
          Me.cb_won_pus_unit.Focus()
          NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1190, _
                            Me.lbl_won_pus_text.Text, "1 " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(259), _
                            "60 " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(258)), ENUM_MB_TYPE.MB_TYPE_WARNING)

          Return False
        End If 'PERIOD UNDER STUDY

        If Me.chk_won_quantity.Checked Then
          If String.IsNullOrEmpty(Me.ef_won_quantity_value.Value) OrElse Me.ef_won_quantity_value.Value < 0 Then
            Me.ef_won_quantity_value.Focus()
            NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_VALID_MSG_BLINK_GENERIC", _
                              Me.chk_won_quantity.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

            Return False
          End If
        End If

        If Me.chk_won_amount.Checked Then
          If String.IsNullOrEmpty(Me.ef_won_amount_value.Value) OrElse Me.ef_won_amount_value.Value < 0 Then
            Me.ef_won_amount_value.Focus()
            NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_VALID_MSG_BLINK_GENERIC", _
                              Me.chk_won_amount.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

            Return False
          End If
        End If 'Me.chk_won_enabled.Checked
      End If

      'JACKPOT
      If Me.chk_jackpot_enabled.Checked Then

        If Not Me.chk_jackpot_quantity.Checked And Not Me.chk_jackpot_amount.Checked Then
          Me.chk_played_quantity.Focus()
          NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), GLB_NLS_GUI_PLAYER_TRACKING.GetString(4862, _
                            Me.chk_jackpot_enabled.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

          Return False
        End If

        'PERIOD UNDER STUDY
        If String.IsNullOrEmpty(Me.ef_jackpot_pus_value.Value) OrElse Me.ef_jackpot_pus_value.Value <= 0 Then
          Me.ef_jackpot_pus_value.Focus()
          NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_VALID_MSG_BLINK_GENERIC", _
                            Me.lbl_jackpot_pus_text.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

          Return False
        Else
          If String.IsNullOrEmpty(Me.cb_jackpot_pus_unit.Value) OrElse Me.cb_jackpot_pus_unit.Value = 0 Then
            Me.cb_jackpot_pus_unit.Focus()
            NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_VALID_MSG_BLINK_GENERIC", _
                              Me.lbl_jackpot_pus_text.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)


            Return False
          End If
        End If

        If Not CheckPUS(Me.cb_jackpot_pus_unit.Value, Me.ef_jackpot_pus_value.Value) Then
          Me.cb_jackpot_pus_unit.Focus()
          NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1190, _
                            Me.lbl_jackpot_pus_text.Text, "1 " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(259), _
                            "60 " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(258)), ENUM_MB_TYPE.MB_TYPE_WARNING)

          Return False
        End If        'PERIOD UNDER STUDY

        If Me.chk_jackpot_quantity.Checked Then
          If String.IsNullOrEmpty(Me.ef_jackpot_quantity_value.Value) OrElse Me.ef_jackpot_quantity_value.Value < 0 Then
            Me.ef_jackpot_quantity_value.Focus()
            NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_VALID_MSG_BLINK_GENERIC", _
                              Me.chk_jackpot_quantity.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

            Return False
          End If
        End If

        If Me.chk_jackpot_amount.Checked Then
          If String.IsNullOrEmpty(Me.ef_jackpot_amount_value.Value) OrElse Me.ef_jackpot_amount_value.Value < 0 Then
            Me.ef_jackpot_amount_value.Focus()
            NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_VALID_MSG_BLINK_GENERIC", _
                              Me.chk_jackpot_amount.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

            Return False
          End If
        End If 'Me.chk_jackpot_enabled.Checked
      End If

      'CANCELED CREDITS
      If Me.chk_canceled_credits_enabled.Checked Then
        If Not Me.chk_canceled_credits_quantity.Checked And Not Me.chk_canceled_credits_amount.Checked Then
          Me.chk_canceled_credits_quantity.Focus()
          NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), GLB_NLS_GUI_PLAYER_TRACKING.GetString(4862, _
                            Me.chk_canceled_credits_enabled.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

          Return False
        End If

        If String.IsNullOrEmpty(Me.ef_canceled_credits_pus_value.Value) OrElse Me.ef_canceled_credits_pus_value.Value <= 0 Then
          Me.ef_canceled_credits_pus_value.Focus()
          NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_VALID_MSG_BLINK_GENERIC", _
                            Me.lbl_counterfeits_pus_text.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

          Return False
        Else
          If String.IsNullOrEmpty(Me.cb_canceled_credits_pus_unit.Value) OrElse Me.cb_canceled_credits_pus_unit.Value = 0 Then
            Me.cb_canceled_credits_pus_unit.Focus()
            NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_VALID_MSG_BLINK_GENERIC", _
                              Me.lbl_counterfeits_pus_text.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

            Return False
          End If
        End If

        If Not CheckPUS(Me.cb_canceled_credits_pus_unit.Value, Me.ef_canceled_credits_pus_value.Value) Then
          Me.cb_canceled_credits_pus_unit.Focus()
          NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1190, _
                            Me.lbl_counterfeits_pus_text.Text, "1 " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(259), _
                            "60 " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(258)), ENUM_MB_TYPE.MB_TYPE_WARNING)

          Return False
        End If

        If Me.chk_canceled_credits_quantity.Checked Then
          If String.IsNullOrEmpty(Me.ef_canceled_credits_quantity_value.Value) OrElse Me.ef_canceled_credits_quantity_value.Value < 0 Then
            Me.ef_canceled_credits_quantity_value.Focus()
            NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_VALID_MSG_BLINK_GENERIC", _
                              Me.chk_canceled_credits_quantity.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

            Return False
          End If
        End If

        If Me.chk_canceled_credits_amount.Checked Then
          If String.IsNullOrEmpty(Me.ef_canceled_credits_amount_value.Value) OrElse Me.ef_canceled_credits_amount_value.Value < 0 Then
            Me.ef_canceled_credits_amount_value.Focus()
            NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_VALID_MSG_BLINK_GENERIC", _
                              Me.chk_canceled_credits_amount.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

            Return False
          End If
        End If 'Me.chk_canceled_credits_enabled.Checked
      End If

      'PAYOUT
      If Me.chk_payout_enabled.Checked Then
        'PERIOD UNDER STUDY
        If String.IsNullOrEmpty(Me.ef_payout_pus_value.Value) OrElse Me.ef_payout_pus_value.Value <= 0 Then
          Me.ef_payout_pus_value.Focus()
          NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_VALID_MSG_BLINK_GENERIC", _
                            Me.lbl_payout_pus_text.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

          Return False
        Else
          If String.IsNullOrEmpty(Me.cb_payout_pus_unit.Value) OrElse Me.cb_payout_pus_unit.Value = 0 Then
            Me.cb_payout_pus_unit.Focus()
            NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_VALID_MSG_BLINK_GENERIC", _
                              Me.lbl_payout_pus_text.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)


            Return False
          End If
        End If

        If Not CheckPUS(Me.cb_payout_pus_unit.Value, Me.ef_payout_pus_value.Value) Then
          Me.cb_payout_pus_unit.Focus()
          NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1190, _
                            Me.lbl_payout_pus_text.Text, "1 " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(259), _
                            "60 " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(258)), ENUM_MB_TYPE.MB_TYPE_WARNING)

          Return False
        End If 'PERIOD UNDER STUDY

        If String.IsNullOrEmpty(Me.ef_payout_limit_lower.Value) OrElse Me.ef_payout_limit_lower.Value <= 0 Then
          Me.ef_payout_limit_lower.Focus()
          NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_VALID_MSG_BLINK_GENERIC", _
                            Me.lbl_payout_limit_lower.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

          Return False
        End If

        If String.IsNullOrEmpty(Me.ef_payout_limit_upper.Value) OrElse Me.ef_payout_limit_upper.Value <= 0 Then
          Me.ef_payout_limit_upper.Focus()
          NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_VALID_MSG_BLINK_GENERIC", _
                            Me.lbl_payout_limit_upper.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

          Return False
        End If

        If GUI_FormatCurrency(Me.ef_payout_limit_upper.Value) > PAYOUT_MAX_VALUE Then
          Me.ef_payout_limit_upper.Focus()
          NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1421, Me.chk_payout_enabled.Text & " " & Me.lbl_payout_limit_upper.Text, CStr(PAYOUT_MAX_VALUE)), _
                             ENUM_MB_TYPE.MB_TYPE_WARNING)

          Return False
        End If

        If GUI_FormatCurrency(Me.ef_payout_limit_lower.Value) > PAYOUT_MAX_VALUE Then
          Me.ef_payout_limit_lower.Focus()
          NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1421, Me.chk_payout_enabled.Text & " " & Me.lbl_payout_limit_lower.Text, CStr(PAYOUT_MAX_VALUE)), _
                             ENUM_MB_TYPE.MB_TYPE_WARNING)

          Return False
        End If

        If Me.ef_payout_limit_lower.Value > GeneralParam.GetDecimal("PlayerTracking", "TerminalDefaultPayout") Then
          Me.ef_payout_limit_lower.Focus()
          NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2316, Me.lbl_payout_limit_lower.Text, CStr(GeneralParam.GetDecimal("PlayerTracking", "TerminalDefaultPayout"))), _
                             ENUM_MB_TYPE.MB_TYPE_WARNING)

          Return False
        End If
      End If 'chk_payout_enabled.Checked

      'COUNTERFEITS
      If Me.chk_counterfeits_enabled.Checked Then

        If String.IsNullOrEmpty(Me.ef_counterfeits_pus_value.Value) OrElse Me.ef_counterfeits_pus_value.Value <= 0 Then
          Me.ef_counterfeits_pus_value.Focus()
          NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_VALID_MSG_BLINK_GENERIC", _
                          Me.lbl_counterfeits_pus_text.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

          Return False
        Else
          If String.IsNullOrEmpty(Me.cb_counterfeits_pus_unit.Value) OrElse Me.cb_counterfeits_pus_unit.Value = 0 Then
            Me.cb_counterfeits_pus_unit.Focus()
            NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_VALID_MSG_BLINK_GENERIC", _
                              Me.lbl_counterfeits_pus_text.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

            Return False
          End If
        End If

        If Not CheckPUS(Me.cb_counterfeits_pus_unit.Value, Me.ef_counterfeits_pus_value.Value) Then
          Me.cb_counterfeits_pus_unit.Focus()
          NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1190, _
                            Me.lbl_counterfeits_pus_text.Text, "1 " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(259), _
                            "60 " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(258)), ENUM_MB_TYPE.MB_TYPE_WARNING)

          Return False
        End If


        If String.IsNullOrEmpty(Me.ef_counterfeits_quantity_value.Value) OrElse Me.ef_counterfeits_quantity_value.Value < 0 Then
          Me.ef_counterfeits_quantity_value.Focus()
          NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_VALID_MSG_BLINK_GENERIC", _
                            Me.lbl_counterfeits_quantity_value.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

          Return False
        End If
      End If 'chk_counterfeits_enabled.Checked


      'TERMINAL ACTIVITY
      If Me.chk_terminal_activity_enabled.Checked Then
        If String.IsNullOrEmpty(Me.ef_terminal_activity_pus_value.Value) OrElse Me.ef_terminal_activity_pus_value.Value <= 0 Then
          Me.ef_terminal_activity_pus_value.Focus()
          NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_VALID_MSG_BLINK_GENERIC", _
                            Me.lbl_terminal_activity_pus_text.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

          Return False
        Else
          If String.IsNullOrEmpty(Me.cb_terminal_activity_pus_unit.Value) OrElse Me.cb_terminal_activity_pus_unit.Value = 0 Then
            Me.cb_terminal_activity_pus_unit.Focus()
            NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_VALID_MSG_BLINK_GENERIC", _
                              Me.lbl_terminal_activity_pus_text.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

            Return False
          End If
        End If

        If Not CheckPUS(Me.cb_terminal_activity_pus_unit.Value, Me.ef_terminal_activity_pus_value.Value) Then
          Me.cb_terminal_activity_pus_unit.Focus()
          NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1190, _
                            Me.lbl_terminal_activity_pus_text.Text, "1 " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(259), _
                            "60 " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(258)), ENUM_MB_TYPE.MB_TYPE_WARNING)

          Return False
        End If
      End If

      'HIGHROLLER
      If Me.chk_highroller_enabled.Checked Then
          If (Me.chk_highroller_betavg.Checked And (String.IsNullOrEmpty(Me.ef_highroller_bet_average.Value) OrElse Me.ef_highroller_bet_average.Value <= 0)) Then
            Me.ef_highroller_bet_average.Focus()
            NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_VALID_MSG_BLINK_GENERIC", _
                              Me.chk_highroller_betavg.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)
            Return False
          End If

          If (Me.chk_highroller_coinin.Checked And (String.IsNullOrEmpty(Me.ef_highroller_coinin.Value) OrElse Me.ef_highroller_coinin.Value <= 0)) Then
            Me.ef_highroller_coinin.Focus()
            NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_VALID_MSG_BLINK_GENERIC", _
                              Me.chk_highroller_coinin.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)
            Return False
          End If

          If (Not Me.chk_highroller_betavg.Checked) And (Not Me.chk_highroller_coinin.Checked) Then
            Me.ef_highroller_bet_average.Focus()
            NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), GLB_NLS_GUI_PLAYER_TRACKING.GetString(4862, _
                            Me.chk_highroller_enabled.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)
            Return False
          End If

      End If



      Return True
    Catch _Ex As Exception

      Log.Exception(_Ex)
    End Try

    Return False

  End Function

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_GetScreenData()

    Dim _cls_custom_alarms As CLS_CUSTOM_ALARMS

    Try
      _cls_custom_alarms = DbEditedObject

      'PLAYED
      If (Me.chk_played_enabled.Checked) Then
        _cls_custom_alarms.Played_Enabled = True

        _cls_custom_alarms.Played_PUS_Unit = GUI_ParseNumber(Me.cb_played_pus_unit.Value)
        _cls_custom_alarms.Played_PUS_Value = GUI_ParseNumber(Me.ef_played_pus_value.Value)

        If (chk_played_quantity.Checked) Then
          _cls_custom_alarms.Played_Limit_Quantity = GUI_ParseNumber(Me.ef_played_quantity_value.Value)
        Else
          _cls_custom_alarms.Played_Limit_Quantity = -1
        End If

        If (chk_played_amount.Checked) Then
          _cls_custom_alarms.Played_Limit_Amount = GUI_ParseNumber(Me.ef_played_amount_value.Value)
        Else
          _cls_custom_alarms.Played_Limit_Amount = -1
        End If
      Else
        _cls_custom_alarms.Played_Enabled = False
        _cls_custom_alarms.Played_Limit_Quantity = -1
        _cls_custom_alarms.Played_Limit_Amount = -1
        _cls_custom_alarms.Played_PUS_Unit = 0
        _cls_custom_alarms.Played_PUS_Value = 0
      End If

      'WON
      If (Me.chk_won_enabled.Checked) Then
        _cls_custom_alarms.Won_Enabled = True

        _cls_custom_alarms.Won_PUS_Unit = GUI_ParseNumber(Me.cb_won_pus_unit.Value)
        _cls_custom_alarms.Won_PUS_Value = GUI_ParseNumber(Me.ef_won_pus_value.Value)

        If (chk_won_quantity.Checked) Then
          _cls_custom_alarms.Won_Limit_Quantity = GUI_ParseNumber(Me.ef_won_quantity_value.Value)
        Else
          _cls_custom_alarms.Won_Limit_Quantity = -1
        End If

        If (chk_won_amount.Checked) Then
          _cls_custom_alarms.Won_Limit_Amount = GUI_ParseNumber(Me.ef_won_amount_value.Value)
        Else
          _cls_custom_alarms.Won_Limit_Amount = -1
        End If
      Else
        _cls_custom_alarms.Won_PUS_Unit = 0
        _cls_custom_alarms.Won_PUS_Value = 0
        _cls_custom_alarms.Won_Enabled = False
        _cls_custom_alarms.Won_Limit_Quantity = -1
        _cls_custom_alarms.Won_Limit_Amount = -1
      End If

      'JACKPOT
      If (Me.chk_jackpot_enabled.Checked) Then
        _cls_custom_alarms.Jackpot_Enabled = True

        If (chk_jackpot_quantity.Checked) Then
          _cls_custom_alarms.Jackpot_Limit_Quantity = GUI_ParseNumber(Me.ef_jackpot_quantity_value.Value)
        Else
          _cls_custom_alarms.Jackpot_Limit_Quantity = -1
        End If

        If (chk_jackpot_amount.Checked) Then
          _cls_custom_alarms.Jackpot_Limit_Amount = GUI_ParseNumber(Me.ef_jackpot_amount_value.Value)
        Else
          _cls_custom_alarms.Jackpot_Limit_Amount = -1
        End If
        _cls_custom_alarms.Jackpot_PUS_Unit = GUI_ParseNumber(Me.cb_jackpot_pus_unit.Value)
        _cls_custom_alarms.Jackpot_PUS_Value = GUI_ParseNumber(Me.ef_jackpot_pus_value.Value)

      Else
        _cls_custom_alarms.Jackpot_Enabled = False
        _cls_custom_alarms.Jackpot_Limit_Quantity = -1
        _cls_custom_alarms.Jackpot_Limit_Amount = -1
        _cls_custom_alarms.Jackpot_PUS_Unit = 0
        _cls_custom_alarms.Jackpot_PUS_Value = 0
      End If

      'CANCELED CREDITS
      If (Me.chk_canceled_credits_enabled.Checked) Then
        _cls_custom_alarms.Canceled_Credit_Enabled = True

        If (chk_canceled_credits_quantity.Checked) Then
          _cls_custom_alarms.Canceled_Credit_Limit_Quantity = GUI_ParseNumber(Me.ef_canceled_credits_quantity_value.Value)
        Else
          _cls_custom_alarms.Canceled_Credit_Limit_Quantity = -1
        End If

        If (chk_canceled_credits_amount.Checked) Then
          _cls_custom_alarms.Canceled_Credit_Limit_Amount = GUI_ParseNumber(Me.ef_canceled_credits_amount_value.Value)
        Else
          _cls_custom_alarms.Canceled_Credit_Limit_Amount = -1
        End If
        _cls_custom_alarms.Canceled_Credit_PUS_Unit = GUI_ParseNumber(Me.cb_canceled_credits_pus_unit.Value)
        _cls_custom_alarms.Canceled_Credit_PUS_Value = GUI_ParseNumber(Me.ef_canceled_credits_pus_value.Value)
      Else
        _cls_custom_alarms.Canceled_Credit_Enabled = False
        _cls_custom_alarms.Canceled_Credit_Limit_Quantity = -1
        _cls_custom_alarms.Canceled_Credit_Limit_Amount = -1
        _cls_custom_alarms.Canceled_Credit_PUS_Unit = 0
        _cls_custom_alarms.Canceled_Credit_PUS_Value = 0
      End If

      'PAYOUT
      If (Me.chk_payout_enabled.Checked) Then
        _cls_custom_alarms.Payout = True
        _cls_custom_alarms.Payout_Limit_Lower = GUI_ParseNumber(Me.ef_payout_limit_lower.Value)
        _cls_custom_alarms.Payout_Limit_Upper = GUI_ParseNumber(Me.ef_payout_limit_upper.Value)
        _cls_custom_alarms.Payout_PUS_Unit = GUI_ParseNumber(Me.cb_payout_pus_unit.Value)
        _cls_custom_alarms.Payout_PUS_Value = GUI_ParseNumber(Me.ef_payout_pus_value.Value)
      Else
        _cls_custom_alarms.Payout = False
        _cls_custom_alarms.Payout_PUS_Unit = 0
        _cls_custom_alarms.Payout_PUS_Value = 0
        _cls_custom_alarms.Payout_Limit_Lower = 0
        _cls_custom_alarms.Payout_Limit_Upper = 0
      End If

      'COUNTERFEITS
      If (Me.chk_counterfeits_enabled.Checked) Then
        _cls_custom_alarms.Counterfeit_Enabled = True
        _cls_custom_alarms.Counterfeit_PUS_Unit = Me.cb_counterfeits_pus_unit.Value
        _cls_custom_alarms.Counterfeit_PUS_Value = GUI_ParseNumber(Me.ef_counterfeits_pus_value.Value)
        _cls_custom_alarms.Counterfeit_Limit_Quantity = GUI_ParseNumber(Me.ef_counterfeits_quantity_value.Value)
      Else
        _cls_custom_alarms.Counterfeit_Enabled = False
        _cls_custom_alarms.Counterfeit_PUS_Unit = 0
        _cls_custom_alarms.Counterfeit_PUS_Value = 0
        _cls_custom_alarms.Counterfeit_Limit_Quantity = -1
      End If

      If (Me.chk_terminal_activity_enabled.Checked) Then
        _cls_custom_alarms.Terminal_Activity_Enabled = True
        _cls_custom_alarms.Terminal_Activity_PUS_Unit = Me.cb_terminal_activity_pus_unit.Value
        _cls_custom_alarms.Terminal_Activity_PUS_Value = GUI_ParseNumber(Me.ef_terminal_activity_pus_value.Value)
      Else
        _cls_custom_alarms.Terminal_Activity_Enabled = False
        _cls_custom_alarms.Terminal_Activity_PUS_Unit = 0
        _cls_custom_alarms.Terminal_Activity_PUS_Value = 0
      End If

      'HIGHROLLER
      If (Me.chk_highroller_enabled.Checked) Then
        _cls_custom_alarms.Highroller_Enabled = True
        _cls_custom_alarms.Highroller_PUS_Unit = Me.cb_highroller_pus_unit.Value
        _cls_custom_alarms.Highroller_PUS_Value = GUI_ParseNumber(Me.ef_highroller_pus_value.Value)
        _cls_custom_alarms.Highroller_Bet_Average_Amount = IIf(chk_highroller_betavg.Checked, GUI_ParseNumber(ef_highroller_bet_average.Value), 0)
        _cls_custom_alarms.Highroller_Coin_In_Amount = IIf(chk_highroller_coinin.Checked, GUI_ParseNumber(ef_highroller_coinin.Value), 0)
        _cls_custom_alarms.Highroller_ProcessAnonymous = Not chk_highroller_include_personal.Checked
      Else
        _cls_custom_alarms.Highroller_Enabled = False
        _cls_custom_alarms.Highroller_PUS_Unit = 0
        _cls_custom_alarms.Highroller_PUS_Value = 0
        _cls_custom_alarms.Highroller_Bet_Average_Amount = 0
        _cls_custom_alarms.Highroller_Coin_In_Amount = 0
        _cls_custom_alarms.Highroller_ProcessAnonymous = True
      End If

    Catch _Ex As Exception

      Log.Exception(_Ex)
    End Try

  End Sub

  ' PURPOSE: sET data to the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _cls_custom_alarms As CLS_CUSTOM_ALARMS

    Try
      _cls_custom_alarms = DbReadObject

      'PLAYED
      Me.chk_played_enabled.Checked = _cls_custom_alarms.Played_Enabled

      Me.ef_played_pus_value.Value = IIf(_cls_custom_alarms.Played_PUS_Value > 0, _cls_custom_alarms.Played_PUS_Value, String.Empty)
      Me.cb_played_pus_unit.Value = _cls_custom_alarms.Played_PUS_Unit

      If (_cls_custom_alarms.Played_Limit_Quantity >= 0) Then
        Me.chk_played_quantity.Checked = True
        Me.ef_played_quantity_value.Value = _cls_custom_alarms.Played_Limit_Quantity
      Else
        Me.chk_played_quantity.Checked = False
        Me.ef_played_quantity_value.Enabled = False
      End If

      If (_cls_custom_alarms.Played_Limit_Amount >= 0) Then
        Me.chk_played_amount.Checked = True
        Me.ef_played_amount_value.Value = GUI_FormatCurrency(_cls_custom_alarms.Played_Limit_Amount)
      Else
        Me.chk_played_amount.Checked = False
        Me.ef_played_amount_value.Enabled = False
      End If

      'WON
      Me.chk_won_enabled.Checked = _cls_custom_alarms.Won_Enabled

      Me.ef_won_pus_value.Value = IIf(_cls_custom_alarms.Won_PUS_Value > 0, _cls_custom_alarms.Won_PUS_Value, String.Empty)
      Me.cb_won_pus_unit.Value = _cls_custom_alarms.Won_PUS_Unit

      If (_cls_custom_alarms.Won_Limit_Quantity >= 0) Then
        Me.chk_won_quantity.Checked = True
        Me.ef_won_quantity_value.Value = _cls_custom_alarms.Won_Limit_Quantity
      Else
        Me.chk_won_quantity.Checked = False
        Me.ef_won_quantity_value.Enabled = False
      End If

      If (_cls_custom_alarms.Won_Limit_Amount >= 0) Then
        Me.chk_won_amount.Checked = True
        Me.ef_won_amount_value.Value = _cls_custom_alarms.Won_Limit_Amount
      Else
        Me.chk_won_amount.Checked = False
        Me.ef_won_amount_value.Enabled = False
      End If

      If Me.chk_played_enabled.Checked Or Me.chk_won_enabled.Checked Then
        Me.ef_played_pus_value.Enabled = True
        Me.cb_played_pus_unit.Enabled = True
      Else
        Me.ef_played_pus_value.Enabled = False
        Me.cb_played_pus_unit.Enabled = False
      End If

      'JACKPOT
      Me.chk_jackpot_enabled.Checked = _cls_custom_alarms.Jackpot_Enabled

      Me.ef_jackpot_pus_value.Value = IIf(_cls_custom_alarms.Jackpot_PUS_Value > 0, _cls_custom_alarms.Jackpot_PUS_Value, String.Empty)
      Me.cb_jackpot_pus_unit.Value = _cls_custom_alarms.Jackpot_PUS_Unit

      If (_cls_custom_alarms.Jackpot_Limit_Quantity >= 0) Then
        Me.chk_jackpot_quantity.Checked = True
        Me.ef_jackpot_quantity_value.Value = _cls_custom_alarms.Jackpot_Limit_Quantity
      Else
        Me.chk_jackpot_quantity.Checked = False
        Me.ef_jackpot_quantity_value.Enabled = False
      End If

      If (_cls_custom_alarms.Jackpot_Limit_Amount >= 0) Then
        Me.chk_jackpot_amount.Checked = True
        Me.ef_jackpot_amount_value.Value = _cls_custom_alarms.Jackpot_Limit_Amount
      Else
        Me.chk_jackpot_amount.Checked = False
        Me.ef_jackpot_amount_value.Enabled = False
      End If

      'CANCELED CREDITS
      Me.chk_canceled_credits_enabled.Checked = _cls_custom_alarms.Canceled_Credit_Enabled

      Me.ef_canceled_credits_pus_value.Value = IIf(_cls_custom_alarms.Canceled_Credit_PUS_Value > 0, _cls_custom_alarms.Canceled_Credit_PUS_Value, String.Empty)
      Me.cb_canceled_credits_pus_unit.Value = _cls_custom_alarms.Canceled_Credit_PUS_Unit

      If (_cls_custom_alarms.Canceled_Credit_Limit_Quantity >= 0) Then
        Me.chk_canceled_credits_quantity.Checked = True
        Me.ef_canceled_credits_quantity_value.Value = _cls_custom_alarms.Canceled_Credit_Limit_Quantity
      Else
        Me.chk_canceled_credits_quantity.Checked = False
        Me.ef_canceled_credits_quantity_value.Enabled = False
      End If

      If (_cls_custom_alarms.Canceled_Credit_Limit_Amount >= 0) Then
        Me.chk_canceled_credits_amount.Checked = True
        Me.ef_canceled_credits_amount_value.Value = _cls_custom_alarms.Canceled_Credit_Limit_Amount
      Else
        Me.chk_canceled_credits_amount.Checked = False
        Me.ef_canceled_credits_amount_value.Enabled = False
      End If

      'PAYOUT
      Me.chk_payout_enabled.Checked = _cls_custom_alarms.Payout

      Me.ef_payout_pus_value.Value = IIf(_cls_custom_alarms.Payout_PUS_Value > 0, _cls_custom_alarms.Payout_PUS_Value, String.Empty)
      Me.cb_payout_pus_unit.Value = _cls_custom_alarms.Payout_PUS_Unit

      Me.ef_payout_limit_lower.Value = _cls_custom_alarms.Payout_Limit_Lower
      Me.ef_payout_limit_upper.Value = _cls_custom_alarms.Payout_Limit_Upper

      'COUNTERFEITS
      Me.chk_counterfeits_enabled.Checked = _cls_custom_alarms.Counterfeit_Enabled

      Me.ef_counterfeits_pus_value.Value = IIf(_cls_custom_alarms.Counterfeit_PUS_Value > 0, _cls_custom_alarms.Counterfeit_PUS_Value, String.Empty)
      Me.cb_counterfeits_pus_unit.Value = _cls_custom_alarms.Counterfeit_PUS_Unit

      Me.ef_counterfeits_quantity_value.Value = IIf(_cls_custom_alarms.Counterfeit_Limit_Quantity >= 0, _cls_custom_alarms.Counterfeit_Limit_Quantity, String.Empty)

      'TERMINAL ACTIVITY
      Me.chk_terminal_activity_enabled.Checked = _cls_custom_alarms.Terminal_Activity_Enabled

      Me.ef_terminal_activity_pus_value.Value = IIf(_cls_custom_alarms.Terminal_Activity_PUS_Value > 0, _cls_custom_alarms.Terminal_Activity_PUS_Value, String.Empty)
      Me.cb_terminal_activity_pus_unit.Value = _cls_custom_alarms.Terminal_Activity_PUS_Unit

      'HIGHROLLER
      Me.chk_highroller_enabled.Checked = _cls_custom_alarms.Highroller_Enabled

      Me.ef_highroller_pus_value.Value = IIf(_cls_custom_alarms.Highroller_PUS_Value > 0, _cls_custom_alarms.Highroller_PUS_Value, String.Empty)
      Me.cb_highroller_pus_unit.Value = IIf(_cls_custom_alarms.Highroller_PUS_Unit = 0, 3, _cls_custom_alarms.Highroller_PUS_Unit)
      Me.ef_highroller_bet_average.Value = IIf(_cls_custom_alarms.Highroller_Bet_Average_Amount > 0, _cls_custom_alarms.Highroller_Bet_Average_Amount, String.Empty)
      Me.ef_highroller_coinin.Value = IIf(_cls_custom_alarms.Highroller_Coin_In_Amount > 0, _cls_custom_alarms.Highroller_Coin_In_Amount, String.Empty)
      Me.chk_highroller_include_personal.Checked = Not _cls_custom_alarms.Highroller_ProcessAnonymous
      Me.chk_highroller_coinin.Checked = _cls_custom_alarms.Highroller_Coin_In_Amount > 0
      Me.chk_highroller_betavg.Checked = _cls_custom_alarms.Highroller_Bet_Average_Amount > 0
      Me.ef_highroller_bet_average.Enabled = _cls_custom_alarms.Highroller_Bet_Average_Amount > 0
      Me.ef_highroller_coinin.Enabled = _cls_custom_alarms.Highroller_Coin_In_Amount > 0

    Catch _Ex As Exception

      Log.Exception(_Ex)
    End Try

  End Sub

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)

    Select Case DbOperation

      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLS_CUSTOM_ALARMS

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub

#End Region

#Region " Public Functions "

  ' PURPOSE : Overloads ShowEditItem function to pass the 'show mode'
  '
  '  PARAMS :
  '     - INPUT : 
  '             - RequiredFieldsMode
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Overloads Sub ShowEditItem(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4859)

    MyBase.ShowEditItem(0)

  End Sub

  ' PURPOSE: Manage permissions.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)

  End Sub 'GUI_Permissions

  ' PURPOSE : Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CUSTOM_ALARMS

    Call MyBase.GUI_SetFormId()

  End Sub

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Translate id period to string
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Function getPeriodFromInt(ByVal Value As Int32) As String

    Select Case Value
      Case 1
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(259).ToLower()
      Case 2
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(299).ToLower()
      Case 3
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(6895).ToLower()
      Case Else
        Return ""

    End Select

  End Function

  ' PURPOSE: Translate yes/no to string
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Function getYesNoFromInt(ByVal Value As Int32) As String

    Select Case Value
      Case 0
        Return GLB_NLS_GUI_INVOICING.GetString(2686)

      Case 1
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(484)

      Case Else
        Return GLB_NLS_GUI_INVOICING.GetString(2686)

    End Select

  End Function

  ' PURPOSE: Refresh example labels every tick count from timer
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub RefreshExampleLabels()

    Dim _payout_value As Decimal
    Dim _payout_lower As Decimal
    Dim _payout_upper As Decimal
    Dim _amount_value As Decimal
    Dim _str_message As String

    _str_message = String.Empty

    _amount_value = 0

    'PLAYED
    _amount_value = IIf(String.IsNullOrEmpty(Me.ef_played_amount_value.Value), 0, Me.ef_played_amount_value.Value)

    If Me.chk_played_enabled.Checked Then
      _str_message = AUDIT_NONE_STRING

      If Me.chk_played_quantity.Checked And Me.chk_played_amount.Checked Then

        _str_message = Resource.String("STR_GUI_ALARMS_CONFIGURATION_1")
        _str_message = _str_message.Replace("@p0", GLB_NLS_GUI_PLAYER_TRACKING.GetString(4849))
        _str_message = _str_message.Replace("@p1", GetLiteralFromUnity(Me.cb_played_pus_unit.Value))
        _str_message = _str_message.Replace("@p2", Me.ef_played_pus_value.Value)
        _str_message = _str_message.Replace("@p3", getPeriodFromInt(Me.cb_played_pus_unit.Value))
        _str_message = _str_message.Replace("@p4", Me.ef_played_quantity_value.Value)
        _str_message = _str_message.Replace("@p5", GLB_NLS_GUI_PLAYER_TRACKING.GetString(4891))
        _str_message = _str_message.Replace("@p6", GUI_FormatCurrency(_amount_value))

      Else
        If Me.chk_played_quantity.Checked And Not Me.chk_played_amount.Checked Then

          _str_message = Resource.String("STR_GUI_ALARMS_CONFIGURATION_2")
          _str_message = _str_message.Replace("@p0", GLB_NLS_GUI_PLAYER_TRACKING.GetString(4849))
          _str_message = _str_message.Replace("@p1", GetLiteralFromUnity(Me.cb_played_pus_unit.Value))
          _str_message = _str_message.Replace("@p2", Me.ef_played_pus_value.Value)
          _str_message = _str_message.Replace("@p3", getPeriodFromInt(Me.cb_played_pus_unit.Value))
          _str_message = _str_message.Replace("@p4", Me.ef_played_quantity_value.Value)
          _str_message = _str_message.Replace("@p5", GLB_NLS_GUI_PLAYER_TRACKING.GetString(4891))

        ElseIf Not Me.chk_played_quantity.Checked And Me.chk_played_amount.Checked Then

          _str_message = Resource.String("STR_GUI_ALARMS_CONFIGURATION_3")
          _str_message = _str_message.Replace("@p0", GLB_NLS_GUI_PLAYER_TRACKING.GetString(4849))
          _str_message = _str_message.Replace("@p1", GetLiteralFromUnity(Me.cb_played_pus_unit.Value))
          _str_message = _str_message.Replace("@p2", Me.ef_played_pus_value.Value)
          _str_message = _str_message.Replace("@p3", getPeriodFromInt(Me.cb_played_pus_unit.Value))
          _str_message = _str_message.Replace("@p4", GUI_FormatCurrency(_amount_value))

        End If
      End If
    Else
      _str_message = AUDIT_NONE_STRING
    End If

    Me.lbl_played_example.Text = _str_message

    'WON
    _amount_value = IIf(String.IsNullOrEmpty(Me.ef_won_amount_value.Value), 0, Me.ef_won_amount_value.Value)

    If Me.chk_won_enabled.Checked Then
      _str_message = AUDIT_NONE_STRING

      If Me.chk_won_quantity.Checked And Me.chk_won_amount.Checked Then

        _str_message = Resource.String("STR_GUI_ALARMS_CONFIGURATION_1")
        _str_message = _str_message.Replace("@p0", GLB_NLS_GUI_PLAYER_TRACKING.GetString(4849))
        _str_message = _str_message.Replace("@p1", GetLiteralFromUnity(Me.cb_won_pus_unit.Value))
        _str_message = _str_message.Replace("@p2", Me.ef_won_pus_value.Value)
        _str_message = _str_message.Replace("@p3", getPeriodFromInt(Me.cb_won_pus_unit.Value))
        _str_message = _str_message.Replace("@p4", Me.ef_won_quantity_value.Value)
        _str_message = _str_message.Replace("@p5", GLB_NLS_GUI_PLAYER_TRACKING.GetString(4845))
        _str_message = _str_message.Replace("@p6", GUI_FormatCurrency(_amount_value))

      Else
        If Me.chk_won_quantity.Checked And Not Me.chk_won_amount.Checked Then

          _str_message = Resource.String("STR_GUI_ALARMS_CONFIGURATION_2")
          _str_message = _str_message.Replace("@p0", GLB_NLS_GUI_PLAYER_TRACKING.GetString(4849))
          _str_message = _str_message.Replace("@p1", GetLiteralFromUnity(Me.cb_won_pus_unit.Value))
          _str_message = _str_message.Replace("@p2", Me.ef_won_pus_value.Value)
          _str_message = _str_message.Replace("@p3", getPeriodFromInt(Me.cb_won_pus_unit.Value))
          _str_message = _str_message.Replace("@p4", Me.ef_won_quantity_value.Value)
          _str_message = _str_message.Replace("@p5", GLB_NLS_GUI_PLAYER_TRACKING.GetString(4845))

        ElseIf Not Me.chk_won_quantity.Checked And Me.chk_won_amount.Checked Then

          _str_message = Resource.String("STR_GUI_ALARMS_CONFIGURATION_3")
          _str_message = _str_message.Replace("@p0", GLB_NLS_GUI_PLAYER_TRACKING.GetString(4849))
          _str_message = _str_message.Replace("@p1", GetLiteralFromUnity(Me.cb_won_pus_unit.Value))
          _str_message = _str_message.Replace("@p2", Me.ef_won_pus_value.Value)
          _str_message = _str_message.Replace("@p3", getPeriodFromInt(Me.cb_won_pus_unit.Value))
          _str_message = _str_message.Replace("@p4", GUI_FormatCurrency(_amount_value))
        End If
      End If
    Else
      _str_message = AUDIT_NONE_STRING
    End If

    Me.lbl_won_example.Text = _str_message

    'JACKPOT
    _amount_value = IIf(String.IsNullOrEmpty(Me.ef_jackpot_amount_value.Value), 0, Me.ef_jackpot_amount_value.Value)

    If Me.chk_jackpot_enabled.Checked Then
      _str_message = AUDIT_NONE_STRING

      If Me.chk_jackpot_quantity.Checked And Me.chk_jackpot_amount.Checked Then

        _str_message = Resource.String("STR_GUI_ALARMS_CONFIGURATION_1")
        _str_message = _str_message.Replace("@p0", GLB_NLS_GUI_PLAYER_TRACKING.GetString(4888))
        _str_message = _str_message.Replace("@p1", GetLiteralFromUnity(Me.cb_jackpot_pus_unit.Value))
        _str_message = _str_message.Replace("@p2", Me.ef_jackpot_pus_value.Value)
        _str_message = _str_message.Replace("@p3", getPeriodFromInt(Me.cb_jackpot_pus_unit.Value))
        _str_message = _str_message.Replace("@p4", Me.ef_jackpot_quantity_value.Value)
        _str_message = _str_message.Replace("@p5", GLB_NLS_GUI_PLAYER_TRACKING.GetString(4846))
        _str_message = _str_message.Replace("@p6", GUI_FormatCurrency(_amount_value))

      Else
        If Me.chk_jackpot_quantity.Checked And Not Me.chk_jackpot_amount.Checked Then

          _str_message = Resource.String("STR_GUI_ALARMS_CONFIGURATION_2")
          _str_message = _str_message.Replace("@p0", GLB_NLS_GUI_PLAYER_TRACKING.GetString(4888))
          _str_message = _str_message.Replace("@p1", GetLiteralFromUnity(Me.cb_jackpot_pus_unit.Value))
          _str_message = _str_message.Replace("@p2", Me.ef_jackpot_pus_value.Value)
          _str_message = _str_message.Replace("@p3", getPeriodFromInt(Me.cb_jackpot_pus_unit.Value))
          _str_message = _str_message.Replace("@p4", Me.ef_jackpot_quantity_value.Value)
          _str_message = _str_message.Replace("@p5", GLB_NLS_GUI_PLAYER_TRACKING.GetString(4846))

        ElseIf Not Me.chk_jackpot_quantity.Checked And Me.chk_jackpot_amount.Checked Then

          _str_message = Resource.String("STR_GUI_ALARMS_CONFIGURATION_3")
          _str_message = _str_message.Replace("@p0", GLB_NLS_GUI_PLAYER_TRACKING.GetString(4888))
          _str_message = _str_message.Replace("@p1", GetLiteralFromUnity(Me.cb_jackpot_pus_unit.Value))
          _str_message = _str_message.Replace("@p2", Me.ef_jackpot_pus_value.Value)
          _str_message = _str_message.Replace("@p3", getPeriodFromInt(Me.cb_jackpot_pus_unit.Value))
          _str_message = _str_message.Replace("@p4", GUI_FormatCurrency(_amount_value))

        End If
      End If
    Else
      _str_message = AUDIT_NONE_STRING
    End If

    Me.lbl_jackpot_example.Text = _str_message

    'cc
    _amount_value = IIf(String.IsNullOrEmpty(Me.ef_canceled_credits_amount_value.Value), 0, Me.ef_canceled_credits_amount_value.Value)

    If Me.chk_canceled_credits_enabled.Checked Then
      _str_message = AUDIT_NONE_STRING

      If Me.chk_canceled_credits_quantity.Checked And Me.chk_canceled_credits_amount.Checked Then
        _str_message = Resource.String("STR_GUI_ALARMS_CONFIGURATION_1")
        _str_message = _str_message.Replace("@p0", GLB_NLS_GUI_PLAYER_TRACKING.GetString(4889))
        _str_message = _str_message.Replace("@p1", GetLiteralFromUnity(Me.cb_canceled_credits_pus_unit.Value))
        _str_message = _str_message.Replace("@p2", Me.ef_canceled_credits_pus_value.Value)
        _str_message = _str_message.Replace("@p3", getPeriodFromInt(Me.cb_canceled_credits_pus_unit.Value))
        _str_message = _str_message.Replace("@p4", Me.ef_canceled_credits_quantity_value.Value)
        _str_message = _str_message.Replace("@p5", GLB_NLS_GUI_PLAYER_TRACKING.GetString(4847))
        _str_message = _str_message.Replace("@p6", GUI_FormatCurrency(_amount_value))

      Else
        If Me.chk_canceled_credits_quantity.Checked And Not Me.chk_canceled_credits_amount.Checked Then
          _str_message = Resource.String("STR_GUI_ALARMS_CONFIGURATION_2")
          _str_message = _str_message.Replace("@p0", GLB_NLS_GUI_PLAYER_TRACKING.GetString(4889))
          _str_message = _str_message.Replace("@p1", GetLiteralFromUnity(Me.cb_canceled_credits_pus_unit.Value))
          _str_message = _str_message.Replace("@p2", Me.ef_canceled_credits_pus_value.Value)
          _str_message = _str_message.Replace("@p3", getPeriodFromInt(Me.cb_canceled_credits_pus_unit.Value))
          _str_message = _str_message.Replace("@p4", Me.ef_canceled_credits_quantity_value.Value)
          _str_message = _str_message.Replace("@p5", GLB_NLS_GUI_PLAYER_TRACKING.GetString(4847))

        ElseIf Not Me.chk_canceled_credits_quantity.Checked And Me.chk_canceled_credits_amount.Checked Then

          _str_message = Resource.String("STR_GUI_ALARMS_CONFIGURATION_3")
          _str_message = _str_message.Replace("@p0", GLB_NLS_GUI_PLAYER_TRACKING.GetString(4889))
          _str_message = _str_message.Replace("@p1", GetLiteralFromUnity(Me.cb_canceled_credits_pus_unit.Value))
          _str_message = _str_message.Replace("@p2", Me.ef_canceled_credits_pus_value.Value)
          _str_message = _str_message.Replace("@p3", getPeriodFromInt(Me.cb_canceled_credits_pus_unit.Value))
          _str_message = _str_message.Replace("@p4", GUI_FormatCurrency(_amount_value))

        End If
      End If
    Else
      _str_message = AUDIT_NONE_STRING
    End If

    Me.lbl_canceled_credits_example.Text = _str_message

    If Me.chk_payout_enabled.Checked Then

      'PAYOUT
      _payout_value = GeneralParam.GetDecimal("PlayerTracking", "TerminalDefaultPayout")
      _payout_lower = _payout_value - IIf(String.IsNullOrEmpty(Me.ef_payout_limit_lower.Value), 0, Me.ef_payout_limit_lower.Value)

      If _payout_lower < 0 Then
        _payout_lower = 0
      End If

      _payout_upper = _payout_value + IIf(String.IsNullOrEmpty(Me.ef_payout_limit_upper.Value), 0, Me.ef_payout_limit_upper.Value)
      _str_message = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4892)
      _str_message = _str_message.Replace("@p0", _payout_value & "%")
      _str_message = _str_message.Replace("@p1", GetLiteralFromUnity(Me.cb_payout_pus_unit.Value))
      _str_message = _str_message.Replace("@p2", Me.ef_payout_pus_value.Value)
      _str_message = _str_message.Replace("@p3", getPeriodFromInt(Me.cb_payout_pus_unit.Value))
      _str_message = _str_message.Replace("@p4", GUI_FormatNumber(_payout_lower, 2) & "%")
      _str_message = _str_message.Replace("@p5", GUI_FormatNumber(_payout_upper, 2) & "%")
    Else
      _str_message = AUDIT_NONE_STRING
    End If

    Me.lbl_payout_example.Text = _str_message

    'COUNTERFEITS
    If Me.chk_counterfeits_enabled.Checked Then
      _str_message = Resource.String("STR_GUI_ALARMS_CONFIGURATION_2")
      _str_message = _str_message.Replace("@p0", GLB_NLS_GUI_PLAYER_TRACKING.GetString(4890))
      _str_message = _str_message.Replace("@p1", GetLiteralFromUnity(Me.cb_counterfeits_pus_unit.Value))
      _str_message = _str_message.Replace("@p2", Me.ef_counterfeits_pus_value.Value)
      _str_message = _str_message.Replace("@p3", getPeriodFromInt(Me.cb_counterfeits_pus_unit.Value))
      _str_message = _str_message.Replace("@p4", Me.ef_counterfeits_quantity_value.Value)
      _str_message = _str_message.Replace("@p5", GLB_NLS_GUI_PLAYER_TRACKING.GetString(4848))
    Else
      _str_message = AUDIT_NONE_STRING
    End If

    Me.lbl_counterfeits_example.Text = _str_message

    'TERMINAL ACTIVITY
    If Me.chk_terminal_activity_enabled.Checked Then
      _str_message = Resource.String("STR_GUI_ALARMS_CONFIGURATION_5")
      _str_message = _str_message.Replace("@p1", GetLiteralFromUnity(Me.cb_terminal_activity_pus_unit.Value))
      _str_message = _str_message.Replace("@p2", Me.ef_terminal_activity_pus_value.Value)
      _str_message = _str_message.Replace("@p3", getPeriodFromInt(Me.cb_terminal_activity_pus_unit.Value))
    Else
      _str_message = AUDIT_NONE_STRING
    End If

    Me.lbl_terminal_activity_example.Text = _str_message

    'HIGHROLLER
    If Me.chk_highroller_enabled.Checked Then

      _str_message = AUDIT_NONE_STRING

      If (chk_highroller_betavg.Checked) Then
          If (chk_highroller_coinin.Checked) Then
            _str_message = Resource.String("STR_GUI_ALARMS_CONFIGURATION_6")
            _str_message = _str_message.Replace("@p0", GLB_NLS_GUI_PLAYER_TRACKING.GetString(4849))
            _str_message = _str_message.Replace("@p1", GetLiteralFromUnity(Me.cb_highroller_pus_unit.Value))
            _str_message = _str_message.Replace("@p2", Me.ef_highroller_pus_value.Value)
            _str_message = _str_message.Replace("@p3", GLB_NLS_GUI_PLAYER_TRACKING.GetString(6895))
            _str_message = _str_message.Replace("@p4", GUI_FormatCurrency(IIf(String.IsNullOrEmpty(Me.ef_highroller_bet_average.Value), 0, Me.ef_highroller_bet_average.Value)))
            _str_message = _str_message.Replace("@p5", GUI_FormatCurrency(IIf(String.IsNullOrEmpty(Me.ef_highroller_coinin.Value), 0, Me.ef_highroller_coinin.Value)))
          Else
            _str_message = Resource.String("STR_GUI_ALARMS_CONFIGURATION_7")
            _str_message = _str_message.Replace("@p0", GLB_NLS_GUI_PLAYER_TRACKING.GetString(4849))
            _str_message = _str_message.Replace("@p1", GetLiteralFromUnity(Me.cb_highroller_pus_unit.Value))
            _str_message = _str_message.Replace("@p2", Me.ef_highroller_pus_value.Value)
            _str_message = _str_message.Replace("@p3", GLB_NLS_GUI_PLAYER_TRACKING.GetString(6895))
            _str_message = _str_message.Replace("@p4", GUI_FormatCurrency(IIf(String.IsNullOrEmpty(Me.ef_highroller_bet_average.Value), 0, Me.ef_highroller_bet_average.Value)))
            _str_message = _str_message.Replace("@p5", "")
          End If
      ElseIf (chk_highroller_coinin.Checked) Then
            _str_message = Resource.String("STR_GUI_ALARMS_CONFIGURATION_8")
            _str_message = _str_message.Replace("@p0", GLB_NLS_GUI_PLAYER_TRACKING.GetString(4849))
            _str_message = _str_message.Replace("@p1", GetLiteralFromUnity(Me.cb_highroller_pus_unit.Value))
            _str_message = _str_message.Replace("@p2", Me.ef_highroller_pus_value.Value)
            _str_message = _str_message.Replace("@p3", GLB_NLS_GUI_PLAYER_TRACKING.GetString(6895))
            _str_message = _str_message.Replace("@p4", GUI_FormatCurrency(IIf(String.IsNullOrEmpty(Me.ef_highroller_coinin.Value), 0, Me.ef_highroller_coinin.Value)))
            _str_message = _str_message.Replace("@p5", "")
      End If
      If (Not _str_message = AUDIT_NONE_STRING) Then
        If (chk_highroller_include_personal.Checked) Then
            _str_message = _str_message + ". " + GLB_NLS_GUI_PLAYER_TRACKING.GetString(6896) ' Include Personal Accounts
        Else
            _str_message = _str_message + "."
        End If
      End If
    Else
      _str_message = AUDIT_NONE_STRING
    End If

    Me.lbl_highroller_example.Text = _str_message

  End Sub

  ' PURPOSE: Get correct string to days or hours (irrelevant in english)
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Function GetLiteralFromUnity(ByVal Id As Integer) As String
    Select Case Id
      Case 1
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4887)
      Case 2, 3
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4886)
      Case Else
        Return String.Empty
    End Select
  End Function

  ' PURPOSE: Return if time selected is correct
  '
  '  PARAMS:
  '     - INPUT:
  '
  ' RETURNS:
  Private Function CheckPUS(ByVal Id As CLS_CUSTOM_ALARMS.ENUM_PUS, ByVal Value As Integer) As Boolean
    Dim _pus_value As Integer

    If Id = CLS_CUSTOM_ALARMS.ENUM_PUS.DAYS Then
      _pus_value = Value * DAY_HOURS
    Else
      _pus_value = Value
    End If

    If _pus_value < MIN_HOURS Or _pus_value > MAX_HOURS Then

      Return False
    End If

    Return True
  End Function

#End Region

#Region " Events "
  Private Sub chk_played_enabled_CheckStateChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_played_enabled.CheckedChanged
    Me.gb_played.Enabled = sender.checked

    If Not sender.checked And Not Me.chk_played_enabled.Checked Then
      Me.ef_played_pus_value.Enabled = False
      Me.cb_played_pus_unit.Enabled = False
    Else
      Me.ef_played_pus_value.Enabled = True
      Me.cb_played_pus_unit.Enabled = True
    End If

  End Sub

  Private Sub chk_won_enabled_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_won_enabled.CheckedChanged
    Me.gb_won.Enabled = sender.checked

    If Not sender.checked And Not Me.chk_won_enabled.Checked Then
      Me.ef_won_pus_value.Enabled = False
      Me.cb_won_pus_unit.Enabled = False
    Else
      Me.ef_won_pus_value.Enabled = True
      Me.cb_won_pus_unit.Enabled = True
    End If

  End Sub

  Private Sub chk_jackpot_enabled_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_jackpot_enabled.CheckStateChanged, chk_jackpot_enabled.CheckedChanged
    Me.gb_jackpot.Enabled = sender.checked

    If Not sender.checked And Not Me.chk_jackpot_enabled.Checked Then
      Me.ef_jackpot_pus_value.Enabled = False
      Me.cb_jackpot_pus_unit.Enabled = False
    Else
      Me.ef_jackpot_pus_value.Enabled = True
      Me.cb_jackpot_pus_unit.Enabled = True
    End If

  End Sub

  Private Sub chk_canceled_credits_enabled_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_canceled_credits_enabled.CheckStateChanged, chk_canceled_credits_enabled.CheckedChanged
    Me.gb_canceled_credits.Enabled = sender.checked

    If Not sender.checked And Not Me.chk_canceled_credits_enabled.Checked Then
      Me.ef_canceled_credits_pus_value.Enabled = False
      Me.cb_canceled_credits_pus_unit.Enabled = False
    Else
      Me.ef_canceled_credits_pus_value.Enabled = True
      Me.cb_canceled_credits_pus_unit.Enabled = True
    End If

  End Sub

  Private Sub chk_payout_enabled_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_payout_enabled.CheckStateChanged, chk_payout_enabled.CheckedChanged
    Me.gb_payout.Enabled = sender.checked
  End Sub

  Private Sub chk_counterfeits_enabled_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_counterfeits_enabled.CheckStateChanged, chk_counterfeits_enabled.CheckedChanged
    Me.gb_counterfeits.Enabled = sender.checked
  End Sub

  Private Sub chk_played_quantity_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_played_quantity.CheckedChanged
    Me.ef_played_quantity_value.Enabled = sender.checked
  End Sub

  Private Sub chk_played_amount_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_played_amount.CheckedChanged
    Me.ef_played_amount_value.Enabled = sender.checked
  End Sub

  Private Sub chk_won_quantity_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_won_quantity.CheckedChanged
    Me.ef_won_quantity_value.Enabled = sender.checked
  End Sub

  Private Sub chk_won_amount_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_won_amount.CheckedChanged
    Me.ef_won_amount_value.Enabled = sender.checked
  End Sub

  Private Sub chk_jackpot_quantity_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_jackpot_quantity.CheckedChanged
    Me.ef_jackpot_quantity_value.Enabled = sender.checked
  End Sub

  Private Sub chk_jackpot_amount_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_jackpot_amount.CheckedChanged
    Me.ef_jackpot_amount_value.Enabled = sender.checked
  End Sub

  Private Sub chk_canceled_credits_quantity_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_canceled_credits_quantity.CheckedChanged
    Me.ef_canceled_credits_quantity_value.Enabled = sender.checked
  End Sub

  Private Sub chk_canceled_credits_amount_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_canceled_credits_amount.CheckedChanged
    Me.ef_canceled_credits_amount_value.Enabled = sender.checked
  End Sub

  Private Sub chk_terminal_activity_enabled_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminal_activity_enabled.CheckStateChanged, chk_terminal_activity_enabled.CheckedChanged
    Me.gb_terminal_activity.Enabled = sender.checked
  End Sub

  ' PURPOSE: Timer operation to refresh the test area.
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Sub m_timer_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles m_timer.Tick
    Call RefreshExampleLabels()
  End Sub

#End Region


Private Sub chk_highroller_enabled_CheckedChanged(sender As Object, e As EventArgs) Handles chk_highroller_enabled.CheckedChanged, chk_highroller_enabled.CheckStateChanged

    Me.gb_highroller.Enabled = sender.checked

    If Not sender.checked And Not Me.chk_highroller_enabled.Checked Then
      Me.ef_highroller_pus_value.Enabled = False
      Me.cb_highroller_pus_unit.Enabled = False
    Else
      Me.ef_highroller_pus_value.Enabled = True
      Me.cb_highroller_pus_unit.Enabled = True
    End If


End Sub


Private Sub chk_highroller_betavg_CheckedChanged(sender As Object, e As EventArgs) Handles chk_highroller_betavg.CheckedChanged
  ef_highroller_bet_average.Enabled = Me.chk_highroller_enabled.Checked And sender.checked
End Sub

Private Sub chk_highroller_coinin_CheckedChanged(sender As Object, e As EventArgs) Handles chk_highroller_coinin.CheckedChanged
  ef_highroller_coinin.Enabled = Me.chk_highroller_enabled.Checked And sender.checked
End Sub


End Class