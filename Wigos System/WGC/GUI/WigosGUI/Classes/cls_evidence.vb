'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_evidence.vb
' DESCRIPTION:   Evidence Parameters class
' AUTHOR:        Joaquim Cid
' CREATION DATE: 20-MAR-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 20-MAR-2012  JCM    Initial version
' 02-APR-2012  JCM    Control column length NAME, ID1, ID2 from fields business/ representative/ holder
' 03-APR-2012  JCM    Corrected Auditor Log Strings
' 02-MAY-2012  MPO    Generate the evidence document
' 10-AUG-2012  DDM    Added fields Name, last name 1 and last name 2. Deleted full name.
' 12-FEB-2014  FBA    Added account holder id type control via CardData class IdentificationTypes
' 30-SEP-2015  FOS    Generated documents external retention
' 20-JUN-2016  LTC    Product Backlog Item 13612,13613,13614: Witholding Evidence.
' 31-OCT-2016  FGB    Bug 19793: Gui -> Witholding: error opening the report on prizes' retention
' 16-MAR-2017  ATB    PBI 25737:Third TAX - Report columns
'--------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports WSI.Common.DocumentList
Imports WSI.Common

Public Class CLASS_EVIDENCE
  Inherits CLASS_BASE

#Region " Constants "
  ' Fields length
  Public Const MAX_NAME_LEN As Integer = 150
#End Region

#Region " Enums "
  Public Enum EVIDENCE_DOCUMENT_STATUS
    EDS_ERROR = 0
    EDS_NOT_CREATED_YET = 1
    EDT_OK = 2
  End Enum
#End Region

#Region " Structure "


  Public Class TYPE_EVIDENCE
    Public operation_id As Int64
    Public account_id As Int64
    Public player_id1 As String
    Public player_id2 As String
    Public datetime As Date
    Public witholding_tax1 As Decimal
    Public witholding_tax2 As Decimal
    Public witholding_tax3 As Decimal
    Public prize As Decimal
    Public representative_name As String
    Public representative_id1 As String
    Public representative_id2 As String
    Public business_name As String
    Public business_id1 As String
    Public business_id2 As String
    Public document_id As Int64
    Public doc_list As WSI.Common.DocumentList
    Public player_name1 As String
    Public player_name2 As String
    Public player_name3 As String
    Public isGeneratedDocument As Boolean
  End Class

#End Region

#Region " Constructor / Destructor "

  Public Sub New()
    Call MyBase.New()
  End Sub

  Protected Overrides Sub Finalize()
    MyBase.Finalize()
  End Sub

#End Region

#Region " Members "

  Protected m_evidence As New TYPE_EVIDENCE
  Protected m_temp_files() As String
  Protected m_temp_folder As String

#End Region 'Members

#Region " Properties "

  Public Property Account_Id() As Int64
    Get
      Return m_evidence.account_id
    End Get
    Set(ByVal value As Int64)
      m_evidence.account_id = value
    End Set
  End Property

  Public Property Operation_Id() As Int64
    Get
      Return m_evidence.operation_id
    End Get
    Set(ByVal value As Int64)

    End Set
  End Property

  Public Property Player_Id1() As String
    Get
      Return m_evidence.player_id1
    End Get
    Set(ByVal value As String)
      m_evidence.player_id1 = value
    End Set
  End Property

  Public Property Player_Id2() As String
    Get
      Return m_evidence.player_id2
    End Get
    Set(ByVal value As String)
      m_evidence.player_id2 = value
    End Set
  End Property

  Public ReadOnly Property Date_Time() As Date
    Get
      Return m_evidence.datetime
    End Get
  End Property

  Public ReadOnly Property Witholding_tax2() As Decimal
    Get
      Return m_evidence.witholding_tax2
    End Get
  End Property

  Public ReadOnly Property Witholding_tax1() As Decimal
    Get
      Return m_evidence.witholding_tax1
    End Get
  End Property

  Public ReadOnly Property Witholding_tax3() As Decimal
    Get
      Return m_evidence.witholding_tax3
    End Get
  End Property

  Public ReadOnly Property Prize() As Decimal
    Get
      Return m_evidence.prize
    End Get
  End Property

  Public Property Representative_Name() As String
    Get
      Return m_evidence.representative_name
    End Get
    Set(ByVal value As String)
      m_evidence.representative_name = value
    End Set
  End Property

  Public Property Representative_Id1() As String
    Get
      Return m_evidence.representative_id1
    End Get
    Set(ByVal value As String)
      m_evidence.representative_id1 = value
    End Set
  End Property

  Public Property Representative_Id2() As String
    Get
      Return m_evidence.representative_id2
    End Get
    Set(ByVal value As String)
      m_evidence.representative_id2 = value
    End Set
  End Property

  Public Property Business_Name() As String
    Get
      Return m_evidence.business_name
    End Get
    Set(ByVal value As String)
      m_evidence.business_name = value
    End Set
  End Property

  Public Property Business_Id1() As String
    Get
      Return m_evidence.business_id1
    End Get
    Set(ByVal value As String)
      m_evidence.business_id1 = value
    End Set
  End Property

  Public Property Business_Id2() As String
    Get
      Return m_evidence.business_id2
    End Get
    Set(ByVal value As String)
      m_evidence.business_id2 = value
    End Set
  End Property

  Public ReadOnly Property Total_Payment() As String
    Get
      Return m_evidence.prize - m_evidence.witholding_tax1 - m_evidence.witholding_tax2 - m_evidence.witholding_tax3
    End Get
  End Property

  Public Property Doc_List() As WSI.Common.DocumentList
    Get
      Return m_evidence.doc_list
    End Get
    Set(ByVal value As WSI.Common.DocumentList)
      m_evidence.doc_list = value
    End Set
  End Property

  Public Property Temp_Files() As String()
    Get
      Return m_temp_files
    End Get
    Set(ByVal value As String())
      m_temp_files = value
    End Set
  End Property

  Public Property Temp_Folder() As String
    Get
      Return m_temp_folder
    End Get
    Set(ByVal value As String)
      m_temp_folder = value
    End Set
  End Property

  Public Property Document_Id() As Int64
    Get
      Return Me.m_evidence.document_id
    End Get
    Set(ByVal value As Int64)
      Me.m_evidence.document_id = value
    End Set
  End Property


  Public Property Player_Name1() As String
    Get
      Return Me.m_evidence.player_name1
    End Get
    Set(ByVal value As String)
      Me.m_evidence.player_name1 = value
    End Set
  End Property

  Public Property Player_Name2() As String
    Get
      Return Me.m_evidence.player_name2
    End Get
    Set(ByVal value As String)
      Me.m_evidence.player_name2 = value
    End Set
  End Property

  Public Property Player_Name3() As String
    Get
      Return Me.m_evidence.player_name3
    End Get
    Set(ByVal value As String)
      Me.m_evidence.player_name3 = value
    End Set
  End Property

  Public Property IsGeneratedDocument() As Boolean
    Get
      Return Me.m_evidence.isGeneratedDocument
    End Get
    Set(ByVal value As Boolean)
      Me.m_evidence.isGeneratedDocument = value
    End Set
  End Property

#End Region 'Properties

#Region " Overrides functions "

  Public Function LoadDocuments() As EVIDENCE_DOCUMENT_STATUS
    If (m_evidence.document_id = 0) Then
      Return EVIDENCE_DOCUMENT_STATUS.EDS_NOT_CREATED_YET
    End If

    If m_evidence.document_id > 0 Then
      Using _db_trx As New WSI.Common.DB_TRX()
        m_evidence.doc_list = New WSI.Common.DocumentList()

        If (WSI.Common.TableDocuments.Load(m_evidence.document_id, WSI.Common.DOCUMENT_TYPE.WITHOLDING, m_evidence.doc_list, _db_trx.SqlTransaction)) Then
          Return EVIDENCE_DOCUMENT_STATUS.EDT_OK
        End If

      End Using
    End If

    Return EVIDENCE_DOCUMENT_STATUS.EDS_ERROR
  End Function 'LoadDocuments

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As ENUM_STATUS

    Dim _rc As Integer

    m_evidence.operation_id = ObjectId

    ' Read Evidence parameters information
    _rc = EvidenceConf_DbRead(m_evidence, SqlCtx)

    Select Case _rc
      Case ENUM_DB_RC.DB_RC_OK

        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND

        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else

        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function   'DB_Read

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    ' Read Evidence parameters information
    _rc = EvidenceConf_DbUpdate(m_evidence, SqlCtx)

    Select Case _rc
      Case ENUM_DB_RC.DB_RC_OK

        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND

        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else

        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function 'DB_Update

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As ENUM_STATUS

    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR

  End Function 'DB_Insert

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As ENUM_STATUS

    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR

  End Function 'DB_Delete

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _rfc As String
    Dim _curp As String

    _rfc = IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.RFC)
    _curp = IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.CURP)

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_ACCOUNT)

    Call _auditor_data.SetName(GLB_NLS_GUI_INVOICING.Id(437), _
                               GUI_FormatDate(Me.m_evidence.datetime, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS))

    Call _auditor_data.SetField(0, Me.m_evidence.player_id1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(631) & "." & _rfc)                  '631.430 Jugador.RFC
    Call _auditor_data.SetField(0, Me.m_evidence.player_id2, GLB_NLS_GUI_PLAYER_TRACKING.GetString(631) & "." & _curp)                  '631.431 Jugador.CURP
    'Call _auditor_data.SetField(0, Me.m_evidence.player_full_name, GLB_NLS_GUI_PLAYER_TRACKING.GetString(631) & "." & GLB_NLS_GUI_INVOICING.GetString(235))                 '631.235 Jugador.Titular
    Call _auditor_data.SetField(0, Me.m_evidence.player_name3, GLB_NLS_GUI_PLAYER_TRACKING.GetString(631) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1186))                 '631.235 Jugador.Nombre
    Call _auditor_data.SetField(0, Me.m_evidence.player_name1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(631) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1188))                 '631.235 Jugador.Ap1
    Call _auditor_data.SetField(0, Me.m_evidence.player_name2, GLB_NLS_GUI_PLAYER_TRACKING.GetString(631) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1189))                 '631.235 Jugador.Ap2

    Call _auditor_data.SetField(0, Me.m_evidence.business_id1, GLB_NLS_GUI_INVOICING.GetString(150) & "." & _rfc)                      '150.430 Empresa.RFC
    Call _auditor_data.SetField(0, Me.m_evidence.business_id2, GLB_NLS_GUI_INVOICING.GetString(150) & "." & _curp)                      '150.431 Empresa.CURP
    Call _auditor_data.SetField(0, Me.m_evidence.business_name, GLB_NLS_GUI_INVOICING.GetString(150) & "." & GLB_NLS_GUI_INVOICING.GetString(235))                     '150.235 Empresa.Titular

    Call _auditor_data.SetField(0, Me.m_evidence.representative_id1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(578) & "." & _rfc)          '578.430 Representante.RFC
    Call _auditor_data.SetField(0, Me.m_evidence.representative_id2, GLB_NLS_GUI_PLAYER_TRACKING.GetString(578) & "." & _curp)          '578.431 Representante.CURP
    Call _auditor_data.SetField(0, Me.m_evidence.representative_name, GLB_NLS_GUI_PLAYER_TRACKING.GetString(578) & "." & GLB_NLS_GUI_INVOICING.GetString(235))         '578.235 Representante.Titular

    Return _auditor_data

  End Function 'AuditorData

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _temp_evidence As CLASS_EVIDENCE
    _temp_evidence = New CLASS_EVIDENCE

    _temp_evidence.m_evidence.operation_id = Me.m_evidence.operation_id
    '_temp_evidence.m_evidence.player_full_name = Me.m_evidence.player_full_name
    _temp_evidence.m_evidence.player_name1 = Me.m_evidence.player_name1
    _temp_evidence.m_evidence.player_name2 = Me.m_evidence.player_name2
    _temp_evidence.m_evidence.player_name3 = Me.m_evidence.player_name3
    _temp_evidence.m_evidence.player_id1 = Me.m_evidence.player_id1
    _temp_evidence.m_evidence.player_id2 = Me.m_evidence.player_id2
    _temp_evidence.m_evidence.datetime = Me.m_evidence.datetime
    _temp_evidence.m_evidence.witholding_tax1 = Me.m_evidence.witholding_tax1
    _temp_evidence.m_evidence.witholding_tax2 = Me.m_evidence.witholding_tax2
    _temp_evidence.m_evidence.witholding_tax3 = Me.m_evidence.witholding_tax3
    _temp_evidence.m_evidence.prize = Me.m_evidence.prize
    _temp_evidence.m_evidence.representative_name = Me.m_evidence.representative_name
    _temp_evidence.m_evidence.representative_id1 = Me.m_evidence.representative_id1
    _temp_evidence.m_evidence.representative_id2 = Me.m_evidence.representative_id2
    _temp_evidence.m_evidence.business_name = Me.m_evidence.business_name
    _temp_evidence.m_evidence.business_id1 = Me.m_evidence.business_id1
    _temp_evidence.m_evidence.business_id2 = Me.m_evidence.business_id2
    _temp_evidence.m_evidence.document_id = Me.m_evidence.document_id

    Return _temp_evidence

  End Function 'Duplicate 

#End Region 'Overrides functions

#Region " DB Functions "

  Private Function EvidenceConf_DbRead(ByVal EvidenceParams As TYPE_EVIDENCE, _
                                      ByVal Context As Integer) As Integer

    Dim _str_sql As String
    Dim _data_table_parameters As DataTable


    _str_sql = "SELECT   AMP_OPERATION_ID " _
                    & ", AMP_PLAYER_NAME " _
                    & ", AMP_PLAYER_ID1 " _
                    & ", AMP_PLAYER_ID2 " _
                    & ", AMP_DATETIME " _
                    & ", AMP_WITHOLDING_TAX1 " _
                    & ", AMP_WITHOLDING_TAX2 " _
                    & ", AMP_WITHOLDING_TAX3 " _
                    & ", AMP_PRIZE " _
                    & ", AMP_REPRESENTATIVE_NAME " _
                    & ", AMP_REPRESENTATIVE_ID1 " _
                    & ", AMP_REPRESENTATIVE_ID2 " _
                    & ", AMP_BUSINESS_NAME " _
                    & ", AMP_BUSINESS_ID1 " _
                    & ", AMP_BUSINESS_ID2 " _
                    & ", AMP_DOCUMENT_ID1 " _
                    & ", AMP_PLAYER_NAME1 " _
                    & ", AMP_PLAYER_NAME2 " _
                    & ", AMP_PLAYER_NAME3 " _
                    & ", AMP_GENERATED_DOCUMENT  " _
              & " FROM   ACCOUNT_MAJOR_PRIZES " _
              & "WHERE   AMP_OPERATION_ID = " & EvidenceParams.operation_id

    _data_table_parameters = GUI_GetTableUsingSQL(_str_sql, 5000)

    If _data_table_parameters.Rows.Count() < 1 Then

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

    End If

    If IsNothing(_data_table_parameters) Then

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

    End If

    EvidenceParams.operation_id = _data_table_parameters.Rows(0).Item("AMP_OPERATION_ID")
    'EvidenceParams.player_full_name = _data_table_parameters.Rows(0).Item("AMP_PLAYER_NAME")
    EvidenceParams.player_id1 = _data_table_parameters.Rows(0).Item("AMP_PLAYER_ID1")
    EvidenceParams.player_id2 = _data_table_parameters.Rows(0).Item("AMP_PLAYER_ID2")
    EvidenceParams.datetime = _data_table_parameters.Rows(0).Item("AMP_DATETIME")

    If _data_table_parameters.Rows(0).IsNull("AMP_WITHOLDING_TAX1") Then
      EvidenceParams.witholding_tax1 = 0
    Else
      EvidenceParams.witholding_tax1 = _data_table_parameters.Rows(0).Item("AMP_WITHOLDING_TAX1")
    End If

    If _data_table_parameters.Rows(0).IsNull("AMP_WITHOLDING_TAX2") Then
      EvidenceParams.witholding_tax2 = 0
    Else
      EvidenceParams.witholding_tax2 = _data_table_parameters.Rows(0).Item("AMP_WITHOLDING_TAX2")
    End If

    If _data_table_parameters.Rows(0).IsNull("AMP_WITHOLDING_TAX3") Then
      EvidenceParams.witholding_tax3 = 0
    Else
      EvidenceParams.witholding_tax3 = _data_table_parameters.Rows(0).Item("AMP_WITHOLDING_TAX3")
    End If

    EvidenceParams.prize = _data_table_parameters.Rows(0).Item("AMP_PRIZE")

    If _data_table_parameters.Rows(0).IsNull("AMP_REPRESENTATIVE_NAME") Then
      EvidenceParams.representative_name = ""
    Else
      EvidenceParams.representative_name = _data_table_parameters.Rows(0).Item("AMP_REPRESENTATIVE_NAME")
    End If

    If _data_table_parameters.Rows(0).IsNull("AMP_REPRESENTATIVE_ID1") Then
      EvidenceParams.representative_id1 = ""
    Else
      EvidenceParams.representative_id1 = _data_table_parameters.Rows(0).Item("AMP_REPRESENTATIVE_ID1")
    End If

    If _data_table_parameters.Rows(0).IsNull("AMP_REPRESENTATIVE_ID2") Then
      EvidenceParams.representative_id2 = ""
    Else
      EvidenceParams.representative_id2 = _data_table_parameters.Rows(0).Item("AMP_REPRESENTATIVE_ID2")
    End If

    If _data_table_parameters.Rows(0).IsNull("AMP_BUSINESS_NAME") Then
      EvidenceParams.business_name = ""
    Else
      EvidenceParams.business_name = _data_table_parameters.Rows(0).Item("AMP_BUSINESS_NAME")
    End If

    If _data_table_parameters.Rows(0).IsNull("AMP_BUSINESS_ID1") Then
      EvidenceParams.business_id1 = ""
    Else
      EvidenceParams.business_id1 = _data_table_parameters.Rows(0).Item("AMP_BUSINESS_ID1")
    End If

    If _data_table_parameters.Rows(0).IsNull("AMP_BUSINESS_ID2") Then
      EvidenceParams.business_id2 = ""
    Else
      EvidenceParams.business_id2 = _data_table_parameters.Rows(0).Item("AMP_BUSINESS_ID2")
    End If

    If _data_table_parameters.Rows(0).IsNull("AMP_DOCUMENT_ID1") Then
      EvidenceParams.document_id = 0
    Else
      EvidenceParams.document_id = _data_table_parameters.Rows(0).Item("AMP_DOCUMENT_ID1")
    End If

    ' Name
    If _data_table_parameters.Rows(0).IsNull("AMP_PLAYER_NAME3") Then
      EvidenceParams.player_name3 = ""
    Else
      EvidenceParams.player_name3 = _data_table_parameters.Rows(0).Item("AMP_PLAYER_NAME3")
    End If

    ' Last name1
    If _data_table_parameters.Rows(0).IsNull("AMP_PLAYER_NAME1") Then
      EvidenceParams.player_name1 = ""
    Else
      EvidenceParams.player_name1 = _data_table_parameters.Rows(0).Item("AMP_PLAYER_NAME1")
    End If

    ' Last name2
    If _data_table_parameters.Rows(0).IsNull("AMP_PLAYER_NAME2") Then
      EvidenceParams.player_name2 = ""
    Else
      EvidenceParams.player_name2 = _data_table_parameters.Rows(0).Item("AMP_PLAYER_NAME2")
    End If

    'IsGeneratedDocument
    If _data_table_parameters.Rows(0).IsNull("AMP_GENERATED_DOCUMENT") Then
      EvidenceParams.isGeneratedDocument = True
    Else
      EvidenceParams.isGeneratedDocument = _data_table_parameters.Rows(0).Item("AMP_GENERATED_DOCUMENT")
    End If

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function

  Private Function EvidenceConf_DbUpdate(ByVal EvidenceParams As TYPE_EVIDENCE, _
                                        ByVal Context As Integer) As Integer

    Dim _str_sql As String

    Dim _sqlTrans As System.Data.SqlClient.SqlTransaction = Nothing
    Dim _commit_trx As Boolean

    Dim _sql_command As SqlCommand
    Dim _rc As Integer
    Dim _result As Integer

    Try

      If Not GUI_BeginSQLTransaction(_sqlTrans) Then
        Exit Function
      End If

      _commit_trx = False

      ' UPDATE EVIDENCE PARAMETERS

      ' DDM 09-AUG-2012: Already is not update amp_player_name. Now, is updated amp_player_name3, amp_player_name2, amp_player_name1
      _str_sql = "UPDATE  ACCOUNT_MAJOR_PRIZES " & _
                   "SET   AMP_PLAYER_NAME3        = @pPlayerName3" & _
                   "    , AMP_PLAYER_NAME1        = @pPlayerName1" & _
                   "    , AMP_PLAYER_NAME2        = @pPlayerName2" & _
                   " ,    AMP_PLAYER_ID1          = @pPlayerId1" & _
                   " ,    AMP_PLAYER_ID2          = @pPlayerId2" & _
                   " ,    AMP_REPRESENTATIVE_NAME = @pRepresentativeName" & _
                   " ,    AMP_REPRESENTATIVE_ID1  = @pRepresentativeId1" & _
                   " ,    AMP_REPRESENTATIVE_ID2  = @pRepresentativeId2" & _
                   " ,    AMP_BUSINESS_NAME       = @pBusinessName" & _
                   " ,    AMP_BUSINESS_ID1        = @pBusinessId1" & _
                   " ,    AMP_BUSINESS_ID2        = @pBusinessId2" & _
                  " WHERE  AMP_OPERATION_ID        = @pOperationId "
      '--> These fields are not updated from frm_Evidence_edit so are not necessary 
      '            " ,    AMP_DATETIME            = @pOperationDate" & _
      '            " ,    AMP_WITHOLDING_TAX1     = @pWitholdingTax1" & _
      '            " ,    AMP_WITHOLDING_TAX2     = @pWitholdingTax2" & _
      '            " ,    AMP_PRIZE               = @pPrize" & _

      _sql_command = New SqlCommand(_str_sql)
      _sql_command.Connection = _sqlTrans.Connection
      _sql_command.Transaction = _sqlTrans

      '_sql_command.Parameters.Add("@pPlayerName", SqlDbType.VarChar, 150).Value = EvidenceParams.player_full_name
      _sql_command.Parameters.Add("@pPlayerName3", SqlDbType.VarChar, 150).Value = EvidenceParams.player_name3
      _sql_command.Parameters.Add("@pPlayerName1", SqlDbType.VarChar, 150).Value = EvidenceParams.player_name1
      _sql_command.Parameters.Add("@pPlayerName2", SqlDbType.VarChar, 150).Value = EvidenceParams.player_name2
      _sql_command.Parameters.Add("@pPlayerId1", SqlDbType.VarChar, 20).Value = EvidenceParams.player_id1
      _sql_command.Parameters.Add("@pPlayerId2", SqlDbType.VarChar, 20).Value = EvidenceParams.player_id2
      '_sql_command.Parameters.Add("@pOperationDate", SqlDbType.DateTime).Value = EvidenceParams.datetime
      '_sql_command.Parameters.Add("@pWitholdingTax1", SqlDbType.Money).Value = EvidenceParams.witholding_tax1
      '_sql_command.Parameters.Add("@pWitholdingTax2", SqlDbType.Money).Value = EvidenceParams.witholding_tax2
      '_sql_command.Parameters.Add("@pPrize", SqlDbType.Money).Value = EvidenceParams.prize
      _sql_command.Parameters.Add("@pRepresentativeName", SqlDbType.VarChar, 150).Value = EvidenceParams.representative_name
      _sql_command.Parameters.Add("@pRepresentativeId1", SqlDbType.VarChar, 20).Value = EvidenceParams.representative_id1
      _sql_command.Parameters.Add("@pRepresentativeId2", SqlDbType.VarChar, 20).Value = EvidenceParams.representative_id2
      _sql_command.Parameters.Add("@pBusinessName", SqlDbType.VarChar, 150).Value = EvidenceParams.business_name
      _sql_command.Parameters.Add("@pBusinessId1", SqlDbType.VarChar, 20).Value = EvidenceParams.business_id1
      _sql_command.Parameters.Add("@pBusinessId2", SqlDbType.VarChar, 20).Value = EvidenceParams.business_id2
      _sql_command.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = EvidenceParams.operation_id

      _rc = _sql_command.ExecuteNonQuery()

      If _rc <> 1 Then

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

      End If

      If _rc = 1 Then
        _commit_trx = True
      End If

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)

    Finally
      ' Either commit or rollback
      If Not (_sqlTrans.Connection Is Nothing) Then
        If _commit_trx Then
          _sqlTrans.Commit()
          _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Else
          _sqlTrans.Rollback()
          _result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If
      End If

      _sqlTrans.Dispose()
      _sqlTrans = Nothing

    End Try

    Return _result

  End Function ' EvidenceConf_DbUpdate

#End Region

#Region " Public function "

  Public Function DB_UpdateDocument() As Boolean

    Dim _sqlTrans As System.Data.SqlClient.SqlTransaction = Nothing
    Dim _commit_trx As Boolean

    _commit_trx = False

    Try

      If Not GUI_BeginSQLTransaction(_sqlTrans) Then
        Exit Function
      End If

      ' LTC 20-JUN-2016 
      If WSI.Common.Witholding.DB_CreateWitholdingDoc(Me.Operation_Id, _sqlTrans, Me.Doc_List) Then

        _commit_trx = True
      End If

      Return False

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)

    Finally
      ' Either commit or rollback
      If Not (_sqlTrans.Connection Is Nothing) Then
        If _commit_trx Then
          _sqlTrans.Commit()
        Else
          _sqlTrans.Rollback()
        End If
      End If

      _sqlTrans.Dispose()
      _sqlTrans = Nothing

    End Try

    Return True

  End Function

#End Region

End Class