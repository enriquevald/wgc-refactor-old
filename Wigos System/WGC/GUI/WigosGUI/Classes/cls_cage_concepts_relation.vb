'-------------------------------------------------------------------
' Copyright � 2014 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_cage_concepts_relation.vb
' DESCRIPTION:   Cage Source Target Concepts class for Cage Source Target Concepts Selection
' AUTHOR:        Alberto Marcos
' CREATION DATE: 19-SEP-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 19-SEP-2014  AMF    Initial version.
'-------------------------------------------------------------------

#Region "Imports"

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Text
Imports WSI.Common
Imports System.Data.SqlClient

#End Region ' Imports

Public Class cls_cage_concepts_relation
  Inherits CLASS_BASE

#Region "Constants"

  Public Const SQL_COLUMN_CONCEPT_ID = 0
  Public Const SQL_COLUMN_CONCEPT_DESCRIPTION = 1
  Public Const SQL_COLUMN_SOURCE_TARGET_ID = 2
  Public Const SQL_COLUMN_SOURCE_TARGET_NAME = 3
  Public Const SQL_COLUMN_TYPE As Integer = 4
  Public Const SQL_COLUMN_CASHIER_ACTION As Integer = 5
  Public Const SQL_COLUMN_ONLY_NATIONAL_CURRENCY As Integer = 6
  Public Const SQL_COLUMN_ENABLED As Integer = 7
  Public Const SQL_COLUMN_PRICE_FACTOR As Integer = 8

  Public Const FACTOR_VALUE As Integer = 1

#End Region ' Constants

#Region "Enum"

  Private Enum ROWACTION
    NORMAL = 0
    MODIFIED = 1
    DELETED = 2
    ADDED = 3
  End Enum ' ROWACTION

  Private Enum ROWORDER
    NO = 0
    YES = 1
  End Enum ' ROWORDER

#End Region ' Enum

#Region "Class"

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_DATA_CAGE_CONCEPT_RELATION
    Public data_table_cstc As DataTable

    ' Init structure
    Public Sub New()
      data_table_cstc = New DataTable()
    End Sub

  End Class ' TYPE_DATA_CAGE_CONCEPT_RELATION

#End Region ' Class

#Region "Members"

  Protected m_data_cage_concept_relation As New TYPE_DATA_CAGE_CONCEPT_RELATION
  Protected m_mode As frm_cage_concepts_relation.FORM_MODE

#End Region ' Members

#Region "Properties"

  Public Overloads Property DataCageConceptRelation() As DataTable
    Get
      Return m_data_cage_concept_relation.data_table_cstc
    End Get

    Set(ByVal Value As DataTable)
      m_data_cage_concept_relation.data_table_cstc = Value
    End Set

  End Property ' DataCageConceptRelation

  Public Property Mode() As frm_cage_concepts_relation.FORM_MODE
    Get
      Return m_mode
    End Get
    Set(ByVal Value As frm_cage_concepts_relation.FORM_MODE)
      m_mode = Value
    End Set
  End Property ' Mode

#End Region ' Properties

#Region "Overrides functions"

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads from the database into the given object
  '
  ' PARAMS :
  '     - INPUT :
  '         - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '
  ' NOTES :
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As Integer

    ' Read currency data
    _rc = DataCageConceptRelation_DbRead(ObjectId, SqlCtx)

    Select Case _rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Read

  '----------------------------------------------------------------------------
  ' PURPOSE : Inserts the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function ' DB_Insert

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes the given object from the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DEPENDENCIES
  '
  ' NOTES :
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function ' DB_Delete

  '----------------------------------------------------------------------------
  ' PURPOSE: Updates the given object into the database.
  '
  ' PARAMS:
  '   - INPUT:  None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES:
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim rc As Integer

    ' Update currency data
    rc = DataCageConceptsRelation_DbUpdate(SqlCtx)

    Select Case rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Update

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns the related auditor data for the current object.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created audit object
  '
  ' NOTES :
  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA
    Dim auditor_data As CLASS_AUDITOR_DATA
    Dim _dt As DataTable
    Dim _row As DataRow
    Dim _idx_row As Int64
    Dim _literal As String

    auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)
    _dt = New DataTable
    auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(5522), "")

    Try

      If Not Me.DataCageConceptRelation Is Nothing Then
        _dt = Me.DataCageConceptRelation.Copy
      End If

      For Each _row In _dt.Rows
        'TODO DELETED

        ' MODIFICADO y A�ADIDO
        _literal = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5523) & ":" & _row(SQL_COLUMN_SOURCE_TARGET_NAME) & "; " & _
                   GLB_NLS_GUI_PLAYER_TRACKING.GetString(5480) & ":" & _row(SQL_COLUMN_CONCEPT_DESCRIPTION) & "; " & _
                   GLB_NLS_GUI_PLAYER_TRACKING.GetString(5104) & ":" & frm_cage_concepts_relation.GetTypeDescription(_row(SQL_COLUMN_TYPE)) & "; " & _
                   GLB_NLS_GUI_PLAYER_TRACKING.GetString(5524) & ":" & frm_cage_concepts_relation.GetCashierActionDescription(IIf(IsDBNull(_row(SQL_COLUMN_CASHIER_ACTION)), -1, _row(SQL_COLUMN_CASHIER_ACTION)), frm_cage_concepts_relation.CASHIER_ACTION_DESCRIPTION_PURPOSE.SHOW_IN_GUI) & "; " & _
                   GLB_NLS_GUI_PLAYER_TRACKING.GetString(5525) & ":" & IIf(_row(SQL_COLUMN_ONLY_NATIONAL_CURRENCY), GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)) & "; " & _
                   GLB_NLS_GUI_PLAYER_TRACKING.GetString(5218) & ":" & IIf(_row(SQL_COLUMN_ENABLED), GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)) & "; " & _
                   GLB_NLS_GUI_PLAYER_TRACKING.GetString(5526) & ":" & GUI_FormatCurrency(IIf(_row(SQL_COLUMN_PRICE_FACTOR) Is DBNull.Value, FACTOR_VALUE, _row(SQL_COLUMN_PRICE_FACTOR)), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & " "

        If _row.RowState = DataRowState.Added Then
          _literal += GLB_NLS_GUI_COMMONMISC.GetString(10)
        End If

        Call auditor_data.SetField(0, _literal, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5527))

        _idx_row = _idx_row + 1

      Next

    Catch ex As Exception

      Debug.Print(ex.Message)
    End Try

    Return auditor_data

  End Function ' AuditorData

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns a duplicate of the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _temp_concept_relation As cls_cage_concepts_relation

    _temp_concept_relation = New cls_cage_concepts_relation
    _temp_concept_relation.m_data_cage_concept_relation.data_table_cstc = Me.m_data_cage_concept_relation.data_table_cstc.Copy()

    Return _temp_concept_relation

  End Function ' Duplicate

#End Region

#Region "Private Functions / Methods"

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads an object from the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '         - ObjectId: Integer
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES:
  Private Function DataCageConceptRelation_DbRead(ByVal ObjectId As Integer, ByVal Context As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Try
      Using _db_trx As New DB_TRX()

        m_data_cage_concept_relation.data_table_cstc = GetCageConceptRelation(ObjectId, _db_trx)

      End Using

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Catch ex As Exception

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR
    End Try

  End Function ' DataCageConceptRelation_DbRead

  '----------------------------------------------------------------------------
  ' PURPOSE : Read object from the DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - ObjectId: Integer
  '         - DbTrx: Transaction
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - Datatable:
  '
  ' NOTES :
  Private Function GetCageConceptRelation(ByVal ObjectId As Integer, ByVal DbTrx As DB_TRX) As DataTable
    Dim _table As DataTable
    Dim _sb As StringBuilder

    _table = New DataTable()
    _sb = New StringBuilder()

    Try

      _sb.AppendLine("    SELECT   CSTC_CONCEPT_ID             ")
      _sb.AppendLine("           , CC_NAME                    ")
      _sb.AppendLine("           , CSTC_SOURCE_TARGET_ID       ")
      _sb.AppendLine("           , CST_SOURCE_TARGET_NAME      ")
      _sb.AppendLine("           , CSTC_TYPE                   ")
      _sb.AppendLine("           , CSTC_CASHIER_ACTION         ")
      _sb.AppendLine("           , CSTC_ONLY_NATIONAL_CURRENCY ")
      _sb.AppendLine("           , CSTC_ENABLED                ")
      _sb.AppendLine("           , CASE WHEN ISNULL(CC_UNIT_PRICE,0)<>0 THEN CSTC_PRICE_FACTOR ELSE NULL END AS CSTC_PRICE_FACTOR")
      _sb.AppendLine("           , CC_ENABLED")
      _sb.AppendLine("      FROM   CAGE_SOURCE_TARGET_CONCEPTS ")
      _sb.AppendLine("INNER JOIN   CAGE_SOURCE_TARGET ON CST_SOURCE_TARGET_ID = CSTC_SOURCE_TARGET_ID AND CST_SOURCE_TARGET_ID > 0 ")
      _sb.AppendLine("INNER JOIN   CAGE_CONCEPTS ON CC_CONCEPT_ID = CSTC_CONCEPT_ID AND CC_CONCEPT_ID > 0 AND CC_TYPE = " & CageMeters.CageConceptType.UserCalculated)

      Select Case (Me.m_mode)

        Case frm_cage_concepts_relation.FORM_MODE.SOURCE_TARGET
          _sb.AppendLine(" WHERE   CSTC_SOURCE_TARGET_ID = @pObjectId ")

        Case frm_cage_concepts_relation.FORM_MODE.CONCEPT
          _sb.AppendLine(" WHERE   CSTC_CONCEPT_ID = @pObjectId ")

      End Select

      Using _sql_cmd As New SqlCommand(_sb.ToString)

        _sql_cmd.Parameters.Add("pObjectId", SqlDbType.Int).Value = ObjectId

        Using _sql_da As New SqlDataAdapter(_sql_cmd)

          DbTrx.Fill(_sql_da, _table)

        End Using
      End Using

    Catch ex As Exception

      Log.Exception(ex)
    End Try

    Return _table

  End Function ' GetCageConceptRelation

  '----------------------------------------------------------------------------
  ' PURPOSE: Updates the given object into the database.
  '
  ' PARAMS:
  '   - INPUT:  None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES:
  Private Function DataCageConceptsRelation_DbUpdate(ByVal Context As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _sql_trx As System.Data.SqlClient.SqlTransaction
    Dim _num_of_rows As Integer
    Dim _sb As StringBuilder

    Try
      _sql_trx = Nothing

      Using _db_trx As New DB_TRX()

        _sql_trx = _db_trx.SqlTransaction
        _sb = New StringBuilder()

        'UPDATE
        _num_of_rows = m_data_cage_concept_relation.data_table_cstc.Select("", "", DataViewRowState.ModifiedCurrent).Length
        For _idx_row_to As Integer = 0 To _num_of_rows - 1

          _sb.Length = 0
          _sb.AppendLine(" UPDATE   CAGE_SOURCE_TARGET_CONCEPTS                          ")
          _sb.AppendLine("    SET   CSTC_TYPE                    = @pType                ")
          _sb.AppendLine("        , CSTC_ONLY_NATIONAL_CURRENCY  = @OnlyNationalCurrency ")
          _sb.AppendLine("        , CSTC_CASHIER_ACTION          = @CashierAction        ")
          _sb.AppendLine("        , CSTC_CASHIER_CONTAINER       = @CashierContainer     ")
          _sb.AppendLine("        , CSTC_CASHIER_BTN_GROUP       = @pCashierBtnGroup     ")
          _sb.AppendLine("        , CSTC_ENABLED                 = @pEnabled             ")
          _sb.AppendLine("        , CSTC_PRICE_FACTOR            = @pPriceFactor         ")
          _sb.AppendLine("  WHERE   CSTC_SOURCE_TARGET_ID        = @pSourceTargetId      ")
          _sb.AppendLine("    AND   CSTC_CONCEPT_ID              = @pConceptId           ")

          Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), _sql_trx.Connection, _sql_trx)
            _cmd.Parameters.Add("@pConceptId", SqlDbType.BigInt).Value = m_data_cage_concept_relation.data_table_cstc.Select("", "", DataViewRowState.ModifiedCurrent)(_idx_row_to).Item(SQL_COLUMN_CONCEPT_ID)
            _cmd.Parameters.Add("@pSourceTargetId", SqlDbType.BigInt).Value = m_data_cage_concept_relation.data_table_cstc.Select("", "", DataViewRowState.ModifiedCurrent)(_idx_row_to).Item(SQL_COLUMN_SOURCE_TARGET_ID)
            _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = m_data_cage_concept_relation.data_table_cstc.Select("", "", DataViewRowState.ModifiedCurrent)(_idx_row_to).Item(SQL_COLUMN_TYPE)
            _cmd.Parameters.Add("@OnlyNationalCurrency", SqlDbType.Bit).Value = m_data_cage_concept_relation.data_table_cstc.Select("", "", DataViewRowState.ModifiedCurrent)(_idx_row_to).Item(SQL_COLUMN_ONLY_NATIONAL_CURRENCY)
            If IsDBNull(m_data_cage_concept_relation.data_table_cstc.Select("", "", DataViewRowState.ModifiedCurrent)(_idx_row_to).Item(SQL_COLUMN_CASHIER_ACTION)) OrElse _
             m_data_cage_concept_relation.data_table_cstc.Select("", "", DataViewRowState.ModifiedCurrent)(_idx_row_to).Item(SQL_COLUMN_CASHIER_ACTION) = -1 Then
              _cmd.Parameters.Add("@CashierAction", SqlDbType.Int).Value = DBNull.Value
              _cmd.Parameters.Add("@CashierContainer", SqlDbType.Int).Value = DBNull.Value
              _cmd.Parameters.Add("@pCashierBtnGroup", SqlDbType.NVarChar).Value = frm_cage_concepts_relation.GetCashierActionDescription(CageMeters.CageSourceTargetCashierAction.None, frm_cage_concepts_relation.CASHIER_ACTION_DESCRIPTION_PURPOSE.SAVE_IN_DATABASE)
            Else
              _cmd.Parameters.Add("@CashierAction", SqlDbType.Int).Value = m_data_cage_concept_relation.data_table_cstc.Select("", "", DataViewRowState.ModifiedCurrent)(_idx_row_to).Item(SQL_COLUMN_CASHIER_ACTION)
              _cmd.Parameters.Add("@CashierContainer", SqlDbType.Int).Value = m_data_cage_concept_relation.data_table_cstc.Select("", "", DataViewRowState.ModifiedCurrent)(_idx_row_to).Item(SQL_COLUMN_CASHIER_ACTION)
              _cmd.Parameters.Add("@pCashierBtnGroup", SqlDbType.NVarChar).Value = frm_cage_concepts_relation.GetCashierActionDescription(m_data_cage_concept_relation.data_table_cstc.Select("", "", DataViewRowState.ModifiedCurrent)(_idx_row_to).Item(SQL_COLUMN_CASHIER_ACTION), frm_cage_concepts_relation.CASHIER_ACTION_DESCRIPTION_PURPOSE.SAVE_IN_DATABASE)
            End If
            _cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).Value = m_data_cage_concept_relation.data_table_cstc.Select("", "", DataViewRowState.ModifiedCurrent)(_idx_row_to).Item(SQL_COLUMN_ENABLED)
            _cmd.Parameters.Add("@pPriceFactor", SqlDbType.Decimal).Value = m_data_cage_concept_relation.data_table_cstc.Select("", "", DataViewRowState.ModifiedCurrent)(_idx_row_to).Item(SQL_COLUMN_PRICE_FACTOR)

            _cmd.ExecuteNonQuery()
          End Using

        Next

        'TODO Delete like cls_cage_source_target
        '_num_of_rows = m_data_cage_concept_relation.data_table_cstc.Select("", "", DataViewRowState.Deleted).Length
        'For _idx_row_to As Integer = 0 To _num_of_rows - 1
        '
        'Next

        ' --- Currency denomination
        'INSERT
        _num_of_rows = m_data_cage_concept_relation.data_table_cstc.Select("", "", DataViewRowState.Added).Length
        For _idx_row_to As Integer = 0 To _num_of_rows - 1

          _sb.Length = 0
          _sb.AppendLine(" INSERT INTO   CAGE_SOURCE_TARGET_CONCEPTS ")
          _sb.AppendLine("             ( CSTC_CONCEPT_ID             ")
          _sb.AppendLine("             , CSTC_SOURCE_TARGET_ID       ")
          _sb.AppendLine("             , CSTC_TYPE                   ")
          _sb.AppendLine("             , CSTC_ONLY_NATIONAL_CURRENCY ")
          _sb.AppendLine("             , CSTC_CASHIER_ACTION         ")
          _sb.AppendLine("             , CSTC_CASHIER_CONTAINER      ")
          _sb.AppendLine("             , CSTC_CASHIER_BTN_GROUP      ")
          _sb.AppendLine("             , CSTC_ENABLED                ")
          _sb.AppendLine("             , CSTC_PRICE_FACTOR)          ")
          _sb.AppendLine("      VALUES ( @pConceptId                 ")
          _sb.AppendLine("             , @pSourceTargetId            ")
          _sb.AppendLine("             , @pType                      ")
          _sb.AppendLine("             , @OnlyNationalCurrency       ")
          _sb.AppendLine("             , @CashierAction              ")
          _sb.AppendLine("             , @CashierContainer           ")
          _sb.AppendLine("             , @pCashierBtnGroup           ")
          _sb.AppendLine("             , @pEnabled                   ")
          _sb.AppendLine("             , @pPriceFactor)              ")

          Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), _sql_trx.Connection, _sql_trx)
            _cmd.Parameters.Add("@pConceptId", SqlDbType.BigInt).Value = m_data_cage_concept_relation.data_table_cstc.Select("", "", DataViewRowState.Added)(_idx_row_to).Item(SQL_COLUMN_CONCEPT_ID)
            _cmd.Parameters.Add("@pSourceTargetId", SqlDbType.BigInt).Value = m_data_cage_concept_relation.data_table_cstc.Select("", "", DataViewRowState.Added)(_idx_row_to).Item(SQL_COLUMN_SOURCE_TARGET_ID)
            _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = m_data_cage_concept_relation.data_table_cstc.Select("", "", DataViewRowState.Added)(_idx_row_to).Item(SQL_COLUMN_TYPE)
            _cmd.Parameters.Add("@OnlyNationalCurrency", SqlDbType.Bit).Value = m_data_cage_concept_relation.data_table_cstc.Select("", "", DataViewRowState.Added)(_idx_row_to).Item(SQL_COLUMN_ONLY_NATIONAL_CURRENCY)
            If IsDBNull(m_data_cage_concept_relation.data_table_cstc.Select("", "", DataViewRowState.Added)(_idx_row_to).Item(SQL_COLUMN_CASHIER_ACTION)) OrElse _
             m_data_cage_concept_relation.data_table_cstc.Select("", "", DataViewRowState.Added)(_idx_row_to).Item(SQL_COLUMN_CASHIER_ACTION) = -1 Then
              _cmd.Parameters.Add("@CashierAction", SqlDbType.Int).Value = DBNull.Value
              _cmd.Parameters.Add("@CashierContainer", SqlDbType.Int).Value = DBNull.Value
              _cmd.Parameters.Add("@pCashierBtnGroup", SqlDbType.NVarChar).Value = frm_cage_concepts_relation.GetCashierActionDescription(CageMeters.CageSourceTargetCashierAction.None, frm_cage_concepts_relation.CASHIER_ACTION_DESCRIPTION_PURPOSE.SAVE_IN_DATABASE)
            Else
              _cmd.Parameters.Add("@CashierAction", SqlDbType.Int).Value = m_data_cage_concept_relation.data_table_cstc.Select("", "", DataViewRowState.Added)(_idx_row_to).Item(SQL_COLUMN_CASHIER_ACTION)
              _cmd.Parameters.Add("@CashierContainer", SqlDbType.Int).Value = m_data_cage_concept_relation.data_table_cstc.Select("", "", DataViewRowState.Added)(_idx_row_to).Item(SQL_COLUMN_CASHIER_ACTION)
              _cmd.Parameters.Add("@pCashierBtnGroup", SqlDbType.NVarChar).Value = frm_cage_concepts_relation.GetCashierActionDescription(m_data_cage_concept_relation.data_table_cstc.Select("", "", DataViewRowState.Added)(_idx_row_to).Item(SQL_COLUMN_CASHIER_ACTION), frm_cage_concepts_relation.CASHIER_ACTION_DESCRIPTION_PURPOSE.SAVE_IN_DATABASE)
            End If
            _cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).Value = m_data_cage_concept_relation.data_table_cstc.Select("", "", DataViewRowState.Added)(_idx_row_to).Item(SQL_COLUMN_ENABLED)
            _cmd.Parameters.Add("@pPriceFactor", SqlDbType.Decimal).Value = m_data_cage_concept_relation.data_table_cstc.Select("", "", DataViewRowState.Added)(_idx_row_to).Item(SQL_COLUMN_PRICE_FACTOR)

            _cmd.ExecuteNonQuery()
          End Using

          Call CageMeters.CreateCageMeters(m_data_cage_concept_relation.data_table_cstc.Select("", "", DataViewRowState.Added)(_idx_row_to).Item(SQL_COLUMN_SOURCE_TARGET_ID), _
                                           m_data_cage_concept_relation.data_table_cstc.Select("", "", DataViewRowState.Added)(_idx_row_to).Item(SQL_COLUMN_CONCEPT_ID), _
                                           0, _
                                           CageMeters.CreateCageMetersOption.CageMeters_CageAllOpenSessionMeters, _
                                           _sql_trx)

        Next

        ' --- Ok
        _db_trx.Commit()

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
      End Using

    Catch ex As Exception

      Log.Exception(ex)
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

  End Function ' DataCageConceptsRelation_DbUpdate

#End Region

End Class ' cls_cage_concepts_relation