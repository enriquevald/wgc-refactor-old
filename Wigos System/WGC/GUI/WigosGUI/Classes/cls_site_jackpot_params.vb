'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_site_jackpot_params.vb
'
' DESCRIPTION:   Site Jackpot Parameters class
'
' AUTHOR:        Ra�l Cervera
'
' CREATION DATE: 24-NOV-2010
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 24-NOV-2010  RCI    Initial version.
' 10-APR-2012  MPO    Added show_winner_name and show_terminal_name
' 31-MAY-2013  XCD    Don't show hide providers
' 15-JUL-2013  LEM    Added PaymentMode member
' 12-MAR-2014  MPO    Fixed Bug WIG-722: use ToXmlWithAll
' 15-APR-2015  ANM    Add week days contribution if user wants
' 21-NOV-2016  GMV    Product Backlog Item 19930:Jackpots - GUI - Show on WinUp setting
'--------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports WSI.Common
Imports System.Globalization

Public Class CLASS_SITE_JACKPOT_PARAMS
  Inherits CLASS_BASE

#Region " Constants "

  Public Const SQL_COLUMN_PROVIDER_ID As Integer = 0
  Public Const SQL_COLUMN_PROVIDER_NAME As Integer = 1
  Public Const SQL_COLUMN_CHECK As Integer = 2

  Public Const EMPTY_STRING_VALUE As String = "---"

#End Region ' Constants

#Region "Structure"

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_SITE_JACKPOT_PARAMS
    Public enabled As Integer
    Public contribution_pct As New List(Of Decimal)
    Public awarding_start As Integer
    Public awarding_end As Integer
    Public awarding_days As Integer
    Public awarding_min_occupation_pct As Double
    Public awarding_exclude_promotions As Integer
    Public awarding_exclude_anonymous As Integer
    Public jackpot_instances As New List(Of TYPE_SITE_JACKPOT_INSTANCE)
    Public animation_interval_01 As Integer
    Public animation_interval_02 As Integer
    Public recent_jackpot_interval As Integer
    Public promo_message As New List(Of String)
    Public to_compensate As Double
    Public only_redeemable As Integer
    Public terminal_list As New TerminalList
    Public show_winner_name As Boolean
    Public show_terminal_name As Boolean
    Public payment_mode As HANDPAY_PAYMENT_MODE
    Public allow_exceed_maximum As Boolean
    Public current_compensation_pct As Double
    Public comments As String
    Public contribution_fix As Boolean
  End Class

#Region "Jackpot Instance"

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_SITE_JACKPOT_INSTANCE
    Public index As Integer
    Public name As String
    Public contribution_pct As Double
    Public min_bet_amount As Double
    Public min_amount As Double
    Public max_amount As Double
    Public average As Double
    Public accumulated As Double
    Public show_on_winup As Boolean 'GMV 2016-11-21 Show on mobile app
  End Class

#End Region ' Jackpot Instance

#End Region ' Structure

#Region "Constructor / Destructor"

  Public Sub New()
    Call MyBase.New()
  End Sub

  Protected Overrides Sub Finalize()
    MyBase.Finalize()

  End Sub

#End Region ' Constructor / Destructor

#Region " Members "

  Protected m_jackpot_params As New TYPE_SITE_JACKPOT_PARAMS

#End Region ' Members

#Region "Properties"

  Public Property Enabled() As Boolean
    Get
      Return m_jackpot_params.enabled
    End Get

    Set(ByVal Value As Boolean)
      m_jackpot_params.enabled = Value
    End Set

  End Property

  Public Property WrkStart() As Integer
    Get
      Return m_jackpot_params.awarding_start
    End Get

    Set(ByVal value As Integer)
      m_jackpot_params.awarding_start = value
    End Set

  End Property

  Public Property WrkEnd() As Integer
    Get
      Return m_jackpot_params.awarding_end
    End Get

    Set(ByVal value As Integer)
      m_jackpot_params.awarding_end = value
    End Set

  End Property

  Public Property ContributionPct() As List(Of Decimal)
    Get
      Return m_jackpot_params.contribution_pct
    End Get

    Set(ByVal value As List(Of Decimal))
      m_jackpot_params.contribution_pct = value
    End Set

  End Property

  Public Property JackpotInstances() As List(Of TYPE_SITE_JACKPOT_INSTANCE)
    Get
      Return m_jackpot_params.jackpot_instances
    End Get

    Set(ByVal value As List(Of TYPE_SITE_JACKPOT_INSTANCE))
      m_jackpot_params.jackpot_instances = value
    End Set

  End Property

  Public Property WorkingDays() As Integer
    Get
      Return m_jackpot_params.awarding_days
    End Get
    Set(ByVal value As Integer)
      m_jackpot_params.awarding_days = value
    End Set
  End Property

  Public ReadOnly Property WorkingDaysText() As String
    Get
      Dim str_binary As String
      Dim bit_array As Char()
      Dim str_days As String

      str_binary = Convert.ToString(m_jackpot_params.awarding_days, 2)
      str_binary = New String("0", 7 - str_binary.Length) + str_binary

      bit_array = str_binary.ToCharArray()

      str_days = ""
      If (bit_array(5) = "1") Then str_days = str_days & " " & GLB_NLS_GUI_JACKPOT_MGR.GetString(298) '"Monday "
      If (bit_array(4) = "1") Then str_days = str_days & " " & GLB_NLS_GUI_JACKPOT_MGR.GetString(299) '"Tuesday "
      If (bit_array(3) = "1") Then str_days = str_days & " " & GLB_NLS_GUI_JACKPOT_MGR.GetString(300) '"Wednesday "
      If (bit_array(2) = "1") Then str_days = str_days & " " & GLB_NLS_GUI_JACKPOT_MGR.GetString(301) '"Thursday "
      If (bit_array(1) = "1") Then str_days = str_days & " " & GLB_NLS_GUI_JACKPOT_MGR.GetString(302) '"Friday "
      If (bit_array(0) = "1") Then str_days = str_days & " " & GLB_NLS_GUI_JACKPOT_MGR.GetString(303) '"Saturday "
      If (bit_array(6) = "1") Then str_days = str_days & " " & GLB_NLS_GUI_JACKPOT_MGR.GetString(304) '"Sunday "

      Return str_days.Trim
    End Get
  End Property

  Public Property MinOccupationPct() As Double
    Get
      Return m_jackpot_params.awarding_min_occupation_pct
    End Get

    Set(ByVal value As Double)
      m_jackpot_params.awarding_min_occupation_pct = value
    End Set

  End Property

  Public Property ExcludePromotions() As Boolean
    Get
      Return m_jackpot_params.awarding_exclude_promotions
    End Get

    Set(ByVal Value As Boolean)
      m_jackpot_params.awarding_exclude_promotions = Value
    End Set

  End Property

  Public Property ExcludeAnonymous() As Boolean
    Get
      Return m_jackpot_params.awarding_exclude_anonymous
    End Get

    Set(ByVal Value As Boolean)
      m_jackpot_params.awarding_exclude_anonymous = Value
    End Set

  End Property

  Public Property AnimInterval_01() As Integer
    Get
      Return m_jackpot_params.animation_interval_01
    End Get

    Set(ByVal value As Integer)
      m_jackpot_params.animation_interval_01 = value
    End Set

  End Property

  Public Property AnimInterval_02() As Integer
    Get
      Return m_jackpot_params.animation_interval_02
    End Get

    Set(ByVal value As Integer)
      m_jackpot_params.animation_interval_02 = value
    End Set

  End Property

  Public Property RecentInterval() As Integer
    Get
      Return m_jackpot_params.recent_jackpot_interval

    End Get

    Set(ByVal value As Integer)
      m_jackpot_params.recent_jackpot_interval = value
    End Set

  End Property

  Public Property PromoMess(ByVal idx As Integer) As String
    Get
      Return m_jackpot_params.promo_message(idx)

    End Get

    Set(ByVal value As String)
      m_jackpot_params.promo_message(idx) = value
    End Set

  End Property

  Public Property ToCompensate() As Double
    Get
      Return m_jackpot_params.to_compensate
    End Get

    Set(ByVal value As Double)
      m_jackpot_params.to_compensate = value
    End Set

  End Property

  Public Property OnlyRedeemable() As Boolean
    Get
      Return m_jackpot_params.only_redeemable
    End Get

    Set(ByVal Value As Boolean)
      m_jackpot_params.only_redeemable = Value
    End Set

  End Property
  Public ReadOnly Property TerminalList() As TerminalList
    Get
      Return m_jackpot_params.terminal_list
    End Get
  End Property

  Public Property ShowTerminalName() As Boolean
    Get
      Return m_jackpot_params.show_terminal_name
    End Get
    Set(ByVal value As Boolean)
      m_jackpot_params.show_terminal_name = value
    End Set
  End Property

  Public Property ShowWinnerName() As Boolean
    Get
      Return m_jackpot_params.show_winner_name
    End Get
    Set(ByVal value As Boolean)
      m_jackpot_params.show_winner_name = value
    End Set
  End Property

  Public Property PaymentMode() As HANDPAY_PAYMENT_MODE
    Get
      Return m_jackpot_params.payment_mode
    End Get
    Set(ByVal value As HANDPAY_PAYMENT_MODE)
      m_jackpot_params.payment_mode = value
    End Set
  End Property

  Public Property AllowExceedMaximum() As Boolean
    Get
      Return m_jackpot_params.allow_exceed_maximum
    End Get
    Set(ByVal value As Boolean)
      m_jackpot_params.allow_exceed_maximum = value
    End Set
  End Property

  Public Property CurrentCompensationPct() As Double
    Get
      Return m_jackpot_params.current_compensation_pct
    End Get
    Set(ByVal value As Double)
      m_jackpot_params.current_compensation_pct = value
    End Set
  End Property

  Public Property Comments() As String
    Get
      Return m_jackpot_params.comments
    End Get
    Set(ByVal value As String)
      m_jackpot_params.comments = value
    End Set
  End Property

  Public Property ContributionFix() As Boolean
    Get
      Return m_jackpot_params.contribution_fix
    End Get

    Set(ByVal Value As Boolean)
      m_jackpot_params.contribution_fix = Value
    End Set

  End Property

#End Region ' Properties

#Region " Overrides functions "

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads from the database into the given object
  '
  ' PARAMS:
  '   - INPUT:
  '     - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As ENUM_STATUS

    Dim _rc As Integer

    ' Read Jackpot parameters information
    _rc = JackpotConf_DbRead(m_jackpot_params, SqlCtx)

    Select Case _rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function   'DB_Read

  '----------------------------------------------------------------------------
  ' PURPOSE: Updates the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    ' Read Jackpot parameters information
    _rc = JackpotConf_DbUpdate(m_jackpot_params, SqlCtx)

    Select Case _rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select
  End Function 'DB_Update

  '----------------------------------------------------------------------------
  ' PURPOSE: Can't insert the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function 'DB_Insert

  '----------------------------------------------------------------------------
  ' PURPOSE: Can't delete the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function 'DB_Delete

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns a duplicate of the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _temp_jackpot As CLASS_SITE_JACKPOT_PARAMS
    Dim _idx As Integer
    Dim _item As TYPE_SITE_JACKPOT_INSTANCE
    Dim _contrib As Decimal

    _temp_jackpot = New CLASS_SITE_JACKPOT_PARAMS

    _temp_jackpot.m_jackpot_params.contribution_fix = Me.m_jackpot_params.contribution_fix
    _temp_jackpot.m_jackpot_params.contribution_pct = New List(Of Decimal)
    For Each _contrib In Me.m_jackpot_params.contribution_pct
      _temp_jackpot.m_jackpot_params.contribution_pct.Add(_contrib)
    Next
    _temp_jackpot.m_jackpot_params.enabled = Me.m_jackpot_params.enabled
    _temp_jackpot.m_jackpot_params.awarding_end = Me.m_jackpot_params.awarding_end
    _temp_jackpot.m_jackpot_params.awarding_start = Me.m_jackpot_params.awarding_start
    _temp_jackpot.m_jackpot_params.awarding_days = Me.m_jackpot_params.awarding_days
    _temp_jackpot.m_jackpot_params.awarding_min_occupation_pct = Me.m_jackpot_params.awarding_min_occupation_pct
    _temp_jackpot.m_jackpot_params.awarding_exclude_promotions = Me.m_jackpot_params.awarding_exclude_promotions
    _temp_jackpot.m_jackpot_params.awarding_exclude_anonymous = Me.m_jackpot_params.awarding_exclude_anonymous
    _temp_jackpot.m_jackpot_params.payment_mode = Me.m_jackpot_params.payment_mode


    _temp_jackpot.m_jackpot_params.jackpot_instances = New List(Of TYPE_SITE_JACKPOT_INSTANCE)

    For _idx = 0 To Me.m_jackpot_params.jackpot_instances.Count - 1

      _item = New TYPE_SITE_JACKPOT_INSTANCE
      _item.contribution_pct = Me.m_jackpot_params.jackpot_instances(_idx).contribution_pct
      _item.average = Me.m_jackpot_params.jackpot_instances(_idx).average
      _item.index = Me.m_jackpot_params.jackpot_instances(_idx).index
      _item.max_amount = Me.m_jackpot_params.jackpot_instances(_idx).max_amount
      _item.min_amount = Me.m_jackpot_params.jackpot_instances(_idx).min_amount
      _item.min_bet_amount = Me.m_jackpot_params.jackpot_instances(_idx).min_bet_amount
      _item.name = Me.m_jackpot_params.jackpot_instances(_idx).name
      _item.accumulated = Me.m_jackpot_params.jackpot_instances(_idx).accumulated
      _item.show_on_winup = Me.m_jackpot_params.jackpot_instances(_idx).show_on_winup


      _temp_jackpot.m_jackpot_params.jackpot_instances.Add(_item)
    Next

    _temp_jackpot.m_jackpot_params.animation_interval_01 = Me.m_jackpot_params.animation_interval_01
    _temp_jackpot.m_jackpot_params.animation_interval_02 = Me.m_jackpot_params.animation_interval_02
    _temp_jackpot.m_jackpot_params.recent_jackpot_interval = Me.m_jackpot_params.recent_jackpot_interval

    _temp_jackpot.m_jackpot_params.promo_message = New List(Of String)

    _temp_jackpot.m_jackpot_params.promo_message.Add(Me.m_jackpot_params.promo_message(0))
    _temp_jackpot.m_jackpot_params.promo_message.Add(Me.m_jackpot_params.promo_message(1))
    _temp_jackpot.m_jackpot_params.promo_message.Add(Me.m_jackpot_params.promo_message(2))

    _temp_jackpot.m_jackpot_params.to_compensate = Me.m_jackpot_params.to_compensate
    _temp_jackpot.m_jackpot_params.only_redeemable = Me.m_jackpot_params.only_redeemable

    _temp_jackpot.m_jackpot_params.show_terminal_name = Me.m_jackpot_params.show_terminal_name
    _temp_jackpot.m_jackpot_params.show_winner_name = Me.m_jackpot_params.show_winner_name

    _temp_jackpot.m_jackpot_params.terminal_list = New TerminalList()
    _temp_jackpot.m_jackpot_params.terminal_list.FromXml(Me.m_jackpot_params.terminal_list.ToXmlWithAll())

    _temp_jackpot.m_jackpot_params.allow_exceed_maximum = Me.m_jackpot_params.allow_exceed_maximum

    _temp_jackpot.m_jackpot_params.current_compensation_pct = Me.m_jackpot_params.current_compensation_pct

    _temp_jackpot.m_jackpot_params.comments = Me.m_jackpot_params.comments

    Return _temp_jackpot

  End Function 'Duplicate 

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns the related auditor data for the current object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _str_aux As String
    Dim _str_aux2 As String
    Dim _seconds As Integer
    Dim _hours As Integer
    Dim _minutes As Integer
    Dim _days As Integer
    Dim _date_time As Date
    Dim _idx As Integer
    Dim _base_field_name As String
    Dim _aux_field_name As String
    Dim _only_reedimable As Integer
    Dim _first_day_of_week As Integer
    Dim _contribution_pct As String


    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_SITE_JACKPOT_PARAMETERS)

    _auditor_data.SetName(GLB_NLS_GUI_JACKPOT_MGR.Id(331), "")

    If Me.Enabled Then
      _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(218), GLB_NLS_GUI_JACKPOT_MGR.GetString(218))
    Else
      _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(218), GLB_NLS_GUI_JACKPOT_MGR.GetString(250))
    End If

    _date_time = GUI_GetDateTime()
    Call Format_SecondsToDdHhMmSs(Me.WrkStart, _days, _hours, _minutes, _seconds)
    _str_aux2 = GUI_FormatTime(New DateTime(_date_time.Year, _date_time.Month, _date_time.Day, _hours, _minutes, _seconds), ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    _str_aux = GLB_NLS_GUI_JACKPOT_MGR.GetString(230) & "." & GLB_NLS_GUI_JACKPOT_MGR.GetString(204)
    _auditor_data.SetField(0, _str_aux2, _str_aux)

    _date_time = GUI_GetDateTime()
    Call Format_SecondsToDdHhMmSs(Me.WrkEnd, _days, _hours, _minutes, _seconds)
    _str_aux2 = GUI_FormatTime(New DateTime(_date_time.Year, _date_time.Month, _date_time.Day, _hours, _minutes, _seconds), ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    _str_aux = GLB_NLS_GUI_JACKPOT_MGR.GetString(230) & "." & GLB_NLS_GUI_JACKPOT_MGR.GetString(205)
    _auditor_data.SetField(0, _str_aux2, _str_aux)

    ' Contribution
    _only_reedimable = IIf(OnlyRedeemable, 333, 334)
    If Me.ContributionFix Then
      _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(_only_reedimable), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6216))
    Else
      _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(_only_reedimable), GLB_NLS_GUI_PLAYER_TRACKING.GetString(476))
    End If

    If ContributionPct(0) = -1 Then
      _contribution_pct = EMPTY_STRING_VALUE
    Else
      _contribution_pct = GUI_FormatNumber(ContributionPct(0), 4)
    End If
    _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2450), _contribution_pct)
    ' Store the output week days array starting on the proper day

    _first_day_of_week = CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek()

    For _idx = _first_day_of_week To ContributionPct().Count - 2 + _first_day_of_week
      Select Case (_idx Mod 7)
        Case 0
          If ContributionPct(1) = -1 Then
            _contribution_pct = EMPTY_STRING_VALUE
          Else
            _contribution_pct = GUI_FormatNumber(ContributionPct(1), 4)
          End If
          _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(295), _contribution_pct)
        Case 1
          If ContributionPct(2) = -1 Then
            _contribution_pct = EMPTY_STRING_VALUE
          Else
            _contribution_pct = GUI_FormatNumber(ContributionPct(2), 4)
          End If
          _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(289), _contribution_pct)
        Case 2
          If ContributionPct(3) = -1 Then
            _contribution_pct = EMPTY_STRING_VALUE
          Else
            _contribution_pct = GUI_FormatNumber(ContributionPct(3), 4)
          End If
          _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(290), _contribution_pct)
        Case 3
          If ContributionPct(4) = -1 Then
            _contribution_pct = EMPTY_STRING_VALUE
          Else
            _contribution_pct = GUI_FormatNumber(ContributionPct(4), 4)
          End If
          _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(291), _contribution_pct)
        Case 4
          If ContributionPct(5) = -1 Then
            _contribution_pct = EMPTY_STRING_VALUE
          Else
            _contribution_pct = GUI_FormatNumber(ContributionPct(5), 4)
          End If
          _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(292), _contribution_pct)
        Case 5
          If ContributionPct(6) = -1 Then
            _contribution_pct = EMPTY_STRING_VALUE
          Else
            _contribution_pct = GUI_FormatNumber(ContributionPct(6), 4)
          End If
          _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(293), _contribution_pct)
        Case 6
          If ContributionPct(7) = -1 Then
            _contribution_pct = EMPTY_STRING_VALUE
          Else
            _contribution_pct = GUI_FormatNumber(ContributionPct(7), 4)
          End If
          _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(294), _contribution_pct)
      End Select
    Next

    ' Working Days
    _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(296), WorkingDaysText())

    ' PaymentMode
    _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(490), PaymentModeString(Me.m_jackpot_params.payment_mode))

    ' Minimum Occupation Pct
    _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(346), GUI_FormatNumber(Me.MinOccupationPct, 2))

    ' Exclude Promotions
    If Me.ExcludePromotions Then
      _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(340), GLB_NLS_GUI_CONTROLS.GetString(283))
    Else
      _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(340), GLB_NLS_GUI_CONTROLS.GetString(284))
    End If

    ' Exclude Anonymous Accounts
    If Me.ExcludeAnonymous Then
      _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(347), GLB_NLS_GUI_CONTROLS.GetString(283))
    Else
      _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(347), GLB_NLS_GUI_CONTROLS.GetString(284))
    End If

    For _idx = 0 To Me.JackpotInstances.Count - 1

      _base_field_name = GLB_NLS_GUI_JACKPOT_MGR.GetString(305, CStr(Me.JackpotInstances(_idx).index))

      ' Instance Name
      _aux_field_name = _base_field_name & "." & GLB_NLS_GUI_JACKPOT_MGR.GetString(282)
      Call _auditor_data.SetField(0, Me.JackpotInstances(_idx).name, _aux_field_name)

      ' Instance Contribution
      _aux_field_name = _base_field_name & "." & GLB_NLS_GUI_JACKPOT_MGR.GetString(283)
      Call _auditor_data.SetField(0, Me.JackpotInstances(_idx).contribution_pct, _aux_field_name)

      ' Instance Minimum Bet
      _aux_field_name = _base_field_name & "." & GLB_NLS_GUI_JACKPOT_MGR.GetString(284)
      Call _auditor_data.SetField(0, Me.JackpotInstances(_idx).min_bet_amount, _aux_field_name)

      ' Instance Minimum
      _aux_field_name = _base_field_name & "." & GLB_NLS_GUI_JACKPOT_MGR.GetString(285)
      Call _auditor_data.SetField(0, Me.JackpotInstances(_idx).min_amount, _aux_field_name)

      ' Instance Maximum
      _aux_field_name = _base_field_name & "." & GLB_NLS_GUI_JACKPOT_MGR.GetString(286)
      Call _auditor_data.SetField(0, Me.JackpotInstances(_idx).max_amount, _aux_field_name)

      ' Instance Average
      _aux_field_name = _base_field_name & "." & GLB_NLS_GUI_JACKPOT_MGR.GetString(287)
      Call _auditor_data.SetField(0, Me.JackpotInstances(_idx).average, _aux_field_name)

      ' Show on Winup
      _aux_field_name = _base_field_name & "." & GLB_NLS_GUI_JACKPOT_MGR.GetString(499)
      Call _auditor_data.SetField(0, Me.JackpotInstances(_idx).show_on_winup, _aux_field_name)


    Next

    ' Jackpot award parameters
    ' 355 "Duraci�n de la animaci�n inicial del Jackpot"
    ' 348 "Duraci�n de la animaci�n final del Jackpot"
    ' 356 "Se anuncia el �ltimo jackpot durante "
    ' 327 "Mensaje 1"
    ' 328 "Mensaje 2"
    ' 329 "Mensaje 3"
    _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(355), AnimInterval_01())
    _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(348), AnimInterval_02())
    _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(356), RecentInterval())
    _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(327), PromoMess(0))
    _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(328), PromoMess(1))
    _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(329), PromoMess(2))

    ' Only Redeemable
    _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(331), GLB_NLS_GUI_JACKPOT_MGR.GetString(_only_reedimable))

    'Groups
    ' Type
    Call _auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(484), GLB_NLS_GUI_CONTROLS.GetString(486) + ": " + _
                                TerminalList.AuditStringType(GLB_NLS_GUI_CONFIGURATION.GetString(497), _
                                                             GLB_NLS_GUI_CONFIGURATION.GetString(499), _
                                                             GLB_NLS_GUI_CONFIGURATION.GetString(498)))


    ' Group list
    Call _auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(477), GLB_NLS_GUI_CONTROLS.GetString(482) + ": " + _
                               TerminalList.AuditStringList(GROUP_ELEMENT_TYPE.GROUP, _
                                                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Prviders list
    Call _auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(477), GLB_NLS_GUI_CONTROLS.GetString(478) + ": " + _
                               TerminalList.AuditStringList(GROUP_ELEMENT_TYPE.PROV, _
                                                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Zones list
    Call _auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(477), GLB_NLS_GUI_CONTROLS.GetString(479) + ": " + _
                               TerminalList.AuditStringList(GROUP_ELEMENT_TYPE.ZONE, _
                                                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Areas list
    Call _auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(477), GLB_NLS_GUI_CONTROLS.GetString(480) + ": " + _
                               TerminalList.AuditStringList(GROUP_ELEMENT_TYPE.AREA, _
                                                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Banks list
    Call _auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(477), GLB_NLS_GUI_CONTROLS.GetString(481) + ": " + _
                               TerminalList.AuditStringList(GROUP_ELEMENT_TYPE.BANK, _
                                                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Terminals list
    Call _auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(477), GLB_NLS_GUI_CONTROLS.GetString(483) + ": " + _
                               TerminalList.AuditStringList(GROUP_ELEMENT_TYPE.TERM, _
                                                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    '358 "Mostrar nombre del ganador"
    '359 "Mostrar nombre del terminal"

    If Me.ShowTerminalName Then
      _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(359), GLB_NLS_GUI_CONTROLS.GetString(283))
    Else
      _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(359), GLB_NLS_GUI_CONTROLS.GetString(284))
    End If

    If Me.ShowWinnerName Then
      _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(358), GLB_NLS_GUI_CONTROLS.GetString(283))
    Else
      _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(358), GLB_NLS_GUI_CONTROLS.GetString(284))
    End If

    '493 Superar m�ximo
    If Me.AllowExceedMaximum Then
      _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(493), GLB_NLS_GUI_CONTROLS.GetString(283))
    Else
      _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(493), GLB_NLS_GUI_CONTROLS.GetString(284))
    End If

    ' 497 Motivo del cambio
    _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(497), Me.Comments)

    Return _auditor_data

  End Function 'AuditorData

#End Region 'Overrides functions

#Region "DB Functions"

  Private Function JackpotConf_DbRead(ByVal JackpotParams As TYPE_SITE_JACKPOT_PARAMS, _
                                      ByVal Context As Integer) As Integer

    Dim _str_sql As String
    Dim _data_table_parameters As DataTable
    Dim _data_table_instances As DataTable
    Dim _xml As String
    Dim _idx As Integer

    If PaymentMode_DbRead() <> ENUM_DB_RC.DB_RC_OK Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    _str_sql = "SELECT SJP_ENABLED" _
                 & ", SJP_CONTRIBUTION_FIX" _
                 & ", SJP_CONTRIBUTION_PCT" _
                 & ", SJP_CONTRIBUTION_PCT0" _
                 & ", SJP_CONTRIBUTION_PCT1" _
                 & ", SJP_CONTRIBUTION_PCT2" _
                 & ", SJP_CONTRIBUTION_PCT3" _
                 & ", SJP_CONTRIBUTION_PCT4" _
                 & ", SJP_CONTRIBUTION_PCT5" _
                 & ", SJP_CONTRIBUTION_PCT6" _
                 & ", SJP_AWARDING_START" _
                 & ", SJP_AWARDING_END" _
                 & ", SJP_AWARDING_DAYS" _
                 & ", SJP_AWARDING_MIN_OCCUPATION_PCT" _
                 & ", SJP_AWARDING_EXCLUDE_PROMOTIONS" _
                 & ", SJP_AWARDING_EXCLUDE_ANONYMOUS" _
                 & ", SJP_ANIMATION_INTERVAL" _
                 & ", SJP_ANIMATION_INTERVAL2" _
                 & ", SJP_RECENT_INTERVAL" _
                 & ", SJP_PROMO_MESSAGE1" _
                 & ", SJP_PROMO_MESSAGE2" _
                 & ", SJP_PROMO_MESSAGE3" _
                 & ", SJP_TO_COMPENSATE" _
                 & ", SJP_ONLY_REDEEMABLE" _
                 & ", SJP_SHOW_WINNER_NAME" _
                 & ", SJP_SHOW_TERMINAL_NAME" _
                 & ", SJP_TERMINAL_LIST" _
                 & ", SJP_EXCEED_MAXIMUM_ALLOWED" _
                 & ", SJP_CURRENT_COMPENSATION_PCT" _
                 & " FROM SITE_JACKPOT_PARAMETERS"

    _data_table_parameters = GUI_GetTableUsingSQL(_str_sql, 5000)

    _str_sql = "SELECT SJI_INDEX" _
                 & ", SJI_NAME" _
                 & ", SJI_CONTRIBUTION_PCT" _
                 & ", SJI_MINIMUM_BET" _
                 & ", SJI_MINIMUM" _
                 & ", SJI_MAXIMUM" _
                 & ", SJI_AVERAGE" _
                 & ", ROUND (SJI_ACCUMULATED, 2, 1) AS SJI_ACCUMULATED"

    If WSI.Common.GeneralParam.GetBoolean("Features", "WinUP", False) Then
      _str_sql = _str_sql & ", SJI_SHOW_ON_WINUP"
    End If

    _str_sql = _str_sql & " FROM SITE_JACKPOT_INSTANCES"

      _data_table_instances = GUI_GetTableUsingSQL(_str_sql, 5000)

      If _data_table_parameters.Rows.Count() < 1 Or _data_table_instances.Rows.Count() < 1 Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      If IsNothing(_data_table_parameters) Or IsNothing(_data_table_instances) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      JackpotParams.enabled = _data_table_parameters.Rows(0).Item("SJP_ENABLED")
      JackpotParams.contribution_fix = _data_table_parameters.Rows(0).Item("SJP_CONTRIBUTION_FIX")
      If JackpotParams.contribution_fix Then
        JackpotParams.contribution_pct.Add(_data_table_parameters.Rows(0).Item("SJP_CONTRIBUTION_PCT"))
        ' Fill with -1 for process in audition
        JackpotParams.contribution_pct.Add(-1)
        JackpotParams.contribution_pct.Add(-1)
        JackpotParams.contribution_pct.Add(-1)
        JackpotParams.contribution_pct.Add(-1)
        JackpotParams.contribution_pct.Add(-1)
        JackpotParams.contribution_pct.Add(-1)
        JackpotParams.contribution_pct.Add(-1)
      Else
        ' Fill with -1 for process in audition
        JackpotParams.contribution_pct.Add(-1)
        JackpotParams.contribution_pct.Add(_data_table_parameters.Rows(0).Item("SJP_CONTRIBUTION_PCT0"))
        JackpotParams.contribution_pct.Add(_data_table_parameters.Rows(0).Item("SJP_CONTRIBUTION_PCT1"))
        JackpotParams.contribution_pct.Add(_data_table_parameters.Rows(0).Item("SJP_CONTRIBUTION_PCT2"))
        JackpotParams.contribution_pct.Add(_data_table_parameters.Rows(0).Item("SJP_CONTRIBUTION_PCT3"))
        JackpotParams.contribution_pct.Add(_data_table_parameters.Rows(0).Item("SJP_CONTRIBUTION_PCT4"))
        JackpotParams.contribution_pct.Add(_data_table_parameters.Rows(0).Item("SJP_CONTRIBUTION_PCT5"))
        JackpotParams.contribution_pct.Add(_data_table_parameters.Rows(0).Item("SJP_CONTRIBUTION_PCT6"))
      End If

      JackpotParams.awarding_start = _data_table_parameters.Rows(0).Item("SJP_AWARDING_START")
      JackpotParams.awarding_end = _data_table_parameters.Rows(0).Item("SJP_AWARDING_END")
      JackpotParams.awarding_days = _data_table_parameters.Rows(0).Item("SJP_AWARDING_DAYS")
      JackpotParams.awarding_min_occupation_pct = _data_table_parameters.Rows(0).Item("SJP_AWARDING_MIN_OCCUPATION_PCT")
      JackpotParams.awarding_exclude_promotions = _data_table_parameters.Rows(0).Item("SJP_AWARDING_EXCLUDE_PROMOTIONS")
      JackpotParams.awarding_exclude_anonymous = _data_table_parameters.Rows(0).Item("SJP_AWARDING_EXCLUDE_ANONYMOUS")
      JackpotParams.animation_interval_01 = _data_table_parameters.Rows(0).Item("SJP_ANIMATION_INTERVAL")
      JackpotParams.animation_interval_02 = _data_table_parameters.Rows(0).Item("SJP_ANIMATION_INTERVAL2")
      JackpotParams.recent_jackpot_interval = _data_table_parameters.Rows(0).Item("SJP_RECENT_INTERVAL")

      If Not IsDBNull(_data_table_parameters.Rows(0).Item("SJP_PROMO_MESSAGE1")) Then
        JackpotParams.promo_message.Add(_data_table_parameters.Rows(0).Item("SJP_PROMO_MESSAGE1"))
      Else
        JackpotParams.promo_message.Add("")
      End If
      If Not IsDBNull(_data_table_parameters.Rows(0).Item("SJP_PROMO_MESSAGE2")) Then
        JackpotParams.promo_message.Add(_data_table_parameters.Rows(0).Item("SJP_PROMO_MESSAGE2"))
      Else
        JackpotParams.promo_message.Add("")
      End If
      If Not IsDBNull(_data_table_parameters.Rows(0).Item("SJP_PROMO_MESSAGE3")) Then
        JackpotParams.promo_message.Add(_data_table_parameters.Rows(0).Item("SJP_PROMO_MESSAGE3"))
      Else
        JackpotParams.promo_message.Add("")
      End If

      JackpotParams.to_compensate = _data_table_parameters.Rows(0).Item("SJP_TO_COMPENSATE")
      JackpotParams.only_redeemable = _data_table_parameters.Rows(0).Item("SJP_ONLY_REDEEMABLE")
      JackpotParams.current_compensation_pct = _data_table_parameters.Rows(0).Item("SJP_CURRENT_COMPENSATION_PCT")

      For _idx = 0 To _data_table_instances.Rows.Count() - 1
        Dim jackpot_inst As New TYPE_SITE_JACKPOT_INSTANCE

        jackpot_inst.index = _data_table_instances.Rows(_idx).Item("SJI_INDEX")
        jackpot_inst.name = _data_table_instances.Rows(_idx).Item("SJI_NAME")
        jackpot_inst.contribution_pct = _data_table_instances.Rows(_idx).Item("SJI_CONTRIBUTION_PCT")
        jackpot_inst.min_bet_amount = _data_table_instances.Rows(_idx).Item("SJI_MINIMUM_BET")
        jackpot_inst.min_amount = _data_table_instances.Rows(_idx).Item("SJI_MINIMUM")
        jackpot_inst.max_amount = _data_table_instances.Rows(_idx).Item("SJI_MAXIMUM")
        jackpot_inst.average = _data_table_instances.Rows(_idx).Item("SJI_AVERAGE")
      jackpot_inst.accumulated = _data_table_instances.Rows(_idx).Item("SJI_ACCUMULATED")

      If WSI.Common.GeneralParam.GetBoolean("Features", "WinUP", False) Then
        If _data_table_instances.Rows(_idx).Item("SJI_SHOW_ON_WINUP") IsNot DBNull.Value Then
          jackpot_inst.show_on_winup = _data_table_instances.Rows(_idx).Item("SJI_SHOW_ON_WINUP")
        End If
      End If
        
        JackpotParams.jackpot_instances.Add(jackpot_inst)

      Next

      JackpotParams.show_winner_name = _data_table_parameters.Rows(0).Item("SJP_SHOW_WINNER_NAME")
      JackpotParams.show_terminal_name = _data_table_parameters.Rows(0).Item("SJP_SHOW_TERMINAL_NAME")
      JackpotParams.allow_exceed_maximum = _data_table_parameters.Rows(0).Item("SJP_EXCEED_MAXIMUM_ALLOWED")

      'SJP_TERMINAL_LIST
      _xml = Nothing
      If _data_table_parameters.Rows(0).Item("SJP_TERMINAL_LIST") IsNot DBNull.Value Then
        _xml = _data_table_parameters.Rows(0).Item("SJP_TERMINAL_LIST")
      End If

      JackpotParams.comments = ""

      JackpotParams.terminal_list.FromXml(_xml)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function

  Private Function JackpotConf_DbUpdate(ByVal JackpotParams As TYPE_SITE_JACKPOT_PARAMS, _
                                        ByVal Context As Integer) As Integer

    Dim _str_sql As String
    Dim _idx As Integer

    Dim _sql_trx As System.Data.SqlClient.SqlTransaction = Nothing
    Dim _commit_trx As Boolean

    Dim _sql_command As SqlCommand
    Dim _rc As Integer
    Dim _result As Integer

    Dim _xml As String

    Try

      'JBC  7-03-2014
      _xml = JackpotParams.terminal_list.ToXmlWithAll()

      If Not GUI_BeginSQLTransaction(_sql_trx) Then
        Exit Function
      End If

      _commit_trx = False

      ' UPDATE JACKPOT PARAMETERS
      _str_sql = "UPDATE   SITE_JACKPOT_PARAMETERS " & _
                   "SET   SJP_CONTRIBUTION_PCT            = CASE WHEN @p1 <> -1 THEN @p1 ELSE SJP_CONTRIBUTION_PCT END" & _
                   "    , SJP_AWARDING_START              = @p2" & _
                   "    , SJP_AWARDING_END                = @p3" & _
                   "    , SJP_ENABLED                     = @p4" & _
                   "    , SJP_AWARDING_DAYS               = @p5" & _
                   "    , SJP_AWARDING_MIN_OCCUPATION_PCT = @p6" & _
                   "    , SJP_AWARDING_EXCLUDE_PROMOTIONS = @p7" & _
                   "    , SJP_AWARDING_EXCLUDE_ANONYMOUS  = @p8" & _
                   "    , SJP_ANIMATION_INTERVAL          = @p9" & _
                   "    , SJP_ANIMATION_INTERVAL2         = @p10" & _
                   "    , SJP_RECENT_INTERVAL             = @p11" & _
                   "    , SJP_PROMO_MESSAGE1              = @p12" & _
                   "    , SJP_PROMO_MESSAGE2              = @p13" & _
                   "    , SJP_PROMO_MESSAGE3              = @p14" & _
                   "    , SJP_ONLY_REDEEMABLE             = @p15" & _
                   "    , SJP_SHOW_TERMINAL_NAME          = @p16" & _
                   "    , SJP_SHOW_WINNER_NAME            = @p17" & _
                   "    , SJP_TERMINAL_LIST               = @p18" & _
                   "    , SJP_EXCEED_MAXIMUM_ALLOWED      = @p19" & _
                   "    , SJP_CONTRIBUTION_FIX            = @p20" & _
                   "    , SJP_CONTRIBUTION_PCT0           = CASE WHEN @p21 <> -1 THEN @p21 ELSE SJP_CONTRIBUTION_PCT0 END" & _
                   "    , SJP_CONTRIBUTION_PCT1           = CASE WHEN @p22 <> -1 THEN @p22 ELSE SJP_CONTRIBUTION_PCT1 END" & _
                   "    , SJP_CONTRIBUTION_PCT2           = CASE WHEN @p23 <> -1 THEN @p23 ELSE SJP_CONTRIBUTION_PCT2 END" & _
                   "    , SJP_CONTRIBUTION_PCT3           = CASE WHEN @p24 <> -1 THEN @p24 ELSE SJP_CONTRIBUTION_PCT3 END" & _
                   "    , SJP_CONTRIBUTION_PCT4           = CASE WHEN @p25 <> -1 THEN @p25 ELSE SJP_CONTRIBUTION_PCT4 END" & _
                   "    , SJP_CONTRIBUTION_PCT5           = CASE WHEN @p26 <> -1 THEN @p26 ELSE SJP_CONTRIBUTION_PCT5 END" & _
                   "    , SJP_CONTRIBUTION_PCT6           = CASE WHEN @p27 <> -1 THEN @p27 ELSE SJP_CONTRIBUTION_PCT6 END"

      _sql_command = New SqlCommand(_str_sql)
      _sql_command.Connection = _sql_trx.Connection
      _sql_command.Transaction = _sql_trx

      _sql_command.Parameters.Add("@p1", SqlDbType.Float).Value = JackpotParams.contribution_pct(0)
      _sql_command.Parameters.Add("@p2", SqlDbType.Int).Value = JackpotParams.awarding_start
      _sql_command.Parameters.Add("@p3", SqlDbType.Int).Value = JackpotParams.awarding_end
      _sql_command.Parameters.Add("@p4", SqlDbType.Int).Value = JackpotParams.enabled
      _sql_command.Parameters.Add("@p5", SqlDbType.Int).Value = JackpotParams.awarding_days
      _sql_command.Parameters.Add("@p6", SqlDbType.Float).Value = JackpotParams.awarding_min_occupation_pct
      _sql_command.Parameters.Add("@p7", SqlDbType.Int).Value = JackpotParams.awarding_exclude_promotions
      _sql_command.Parameters.Add("@p8", SqlDbType.Int).Value = JackpotParams.awarding_exclude_anonymous
      _sql_command.Parameters.Add("@p9", SqlDbType.Int).Value = JackpotParams.animation_interval_01
      _sql_command.Parameters.Add("@p10", SqlDbType.Int).Value = JackpotParams.animation_interval_02
      _sql_command.Parameters.Add("@p11", SqlDbType.Int).Value = JackpotParams.recent_jackpot_interval
      _sql_command.Parameters.Add("@p12", SqlDbType.VarChar).Value = JackpotParams.promo_message(0)
      _sql_command.Parameters.Add("@p13", SqlDbType.VarChar).Value = JackpotParams.promo_message(1)
      _sql_command.Parameters.Add("@p14", SqlDbType.VarChar).Value = JackpotParams.promo_message(2)
      _sql_command.Parameters.Add("@p15", SqlDbType.Int).Value = JackpotParams.only_redeemable
      _sql_command.Parameters.Add("@p16", SqlDbType.Bit).Value = JackpotParams.show_terminal_name
      _sql_command.Parameters.Add("@p17", SqlDbType.Bit).Value = JackpotParams.show_winner_name
      If Not String.IsNullOrEmpty(_xml) Then
        _sql_command.Parameters.Add("@p18", SqlDbType.Xml).Value = _xml
      Else
        _sql_command.Parameters.Add("@p18", SqlDbType.Xml).Value = DBNull.Value
      End If
      _sql_command.Parameters.Add("@p19", SqlDbType.Bit).Value = JackpotParams.allow_exceed_maximum
      _sql_command.Parameters.Add("@p20", SqlDbType.Bit).Value = JackpotParams.contribution_fix
      _sql_command.Parameters.Add("@p21", SqlDbType.Float).Value = JackpotParams.contribution_pct(1)
      _sql_command.Parameters.Add("@p22", SqlDbType.Float).Value = JackpotParams.contribution_pct(2)
      _sql_command.Parameters.Add("@p23", SqlDbType.Float).Value = JackpotParams.contribution_pct(3)
      _sql_command.Parameters.Add("@p24", SqlDbType.Float).Value = JackpotParams.contribution_pct(4)
      _sql_command.Parameters.Add("@p25", SqlDbType.Float).Value = JackpotParams.contribution_pct(5)
      _sql_command.Parameters.Add("@p26", SqlDbType.Float).Value = JackpotParams.contribution_pct(6)
      _sql_command.Parameters.Add("@p27", SqlDbType.Float).Value = JackpotParams.contribution_pct(7)

      _rc = _sql_command.ExecuteNonQuery()

      If _rc <> 1 Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      ' UPDATE JACKPOT INSTANCES
      _str_sql = "UPDATE   SITE_JACKPOT_INSTANCES " & _
                "   SET   SJI_NAME             = @p1" & _
                "       , SJI_CONTRIBUTION_PCT = @p2" & _
                "       , SJI_MINIMUM          = @p3" & _
                "       , SJI_MAXIMUM          = @p4" & _
                "       , SJI_AVERAGE          = @p5" & _
                "       , SJI_MINIMUM_BET      = @p7"
      If WSI.Common.GeneralParam.GetBoolean("Features", "WinUP", False) Then
        _str_sql = _str_sql & "       , SJI_SHOW_ON_WINUP    = @p8"
      End If

      _str_sql = _str_sql & " WHERE   SJI_INDEX            = @p6"

      For _idx = 0 To JackpotParams.jackpot_instances.Count - 1
        _sql_command = New SqlCommand(_str_sql)
        _sql_command.Connection = _sql_trx.Connection
        _sql_command.Transaction = _sql_trx

        _sql_command.Parameters.Add("@p1", SqlDbType.NVarChar).Value = JackpotParams.jackpot_instances(_idx).name
        _sql_command.Parameters.Add("@p2", SqlDbType.Float).Value = JackpotParams.jackpot_instances(_idx).contribution_pct
        _sql_command.Parameters.Add("@p3", SqlDbType.Float).Value = JackpotParams.jackpot_instances(_idx).min_amount
        _sql_command.Parameters.Add("@p4", SqlDbType.Float).Value = JackpotParams.jackpot_instances(_idx).max_amount
        _sql_command.Parameters.Add("@p5", SqlDbType.Float).Value = JackpotParams.jackpot_instances(_idx).average
        _sql_command.Parameters.Add("@p6", SqlDbType.Int).Value = JackpotParams.jackpot_instances(_idx).index
        _sql_command.Parameters.Add("@p7", SqlDbType.Float, 1).Value = JackpotParams.jackpot_instances(_idx).min_bet_amount
        If WSI.Common.GeneralParam.GetBoolean("Features", "WinUP", False) Then
          _sql_command.Parameters.Add("@p8", SqlDbType.Bit).Value = JackpotParams.jackpot_instances(_idx).show_on_winup
        End If

        _rc = _sql_command.ExecuteNonQuery()

        If _rc <> 1 Then
          Exit For
        End If

      Next

      If Not DB_GeneralParam_Update("SiteJackpot", "PaymentMode", IIf(Me.PaymentMode = HANDPAY_PAYMENT_MODE.AUTOMATIC, "1", "0"), _sql_trx) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      ''''''''''''

      If _rc = 1 Then
        _commit_trx = True
      End If

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)

    Finally
      ' Either commit or rollback
      If Not (_sql_trx.Connection Is Nothing) Then
        If _commit_trx Then
          _sql_trx.Commit()
          _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Else
          _sql_trx.Rollback()
          _result = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If
      End If

      _sql_trx.Dispose()
      _sql_trx = Nothing

    End Try

    Return _result

  End Function ' JackpotConf_DbUpdate

  Private Function PaymentMode_DbRead() As ENUM_DB_RC
    Dim _str_sql As String
    Dim _mode As Object

    Try
      _str_sql = " SELECT   GP_KEY_VALUE                   " & _
                 "   FROM   GENERAL_PARAMS                 " & _
                 "  WHERE   GP_GROUP_KEY   = 'SiteJackpot' " & _
                 "    AND   GP_SUBJECT_KEY = 'PaymentMode' "

      Using _db_trx As New DB_TRX()
        Using _cmd As New SqlCommand(_str_sql)
          _mode = _db_trx.ExecuteScalar(_cmd)

          If _mode IsNot Nothing And _mode IsNot DBNull.Value Then
            If _mode = "1" Then
              Me.PaymentMode = HANDPAY_PAYMENT_MODE.AUTOMATIC
            Else
              Me.PaymentMode = HANDPAY_PAYMENT_MODE.MANUAL
            End If
          End If

        End Using
      End Using

      Return ENUM_DB_RC.DB_RC_OK
    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return ENUM_DB_RC.DB_RC_ERROR
  End Function ' PaymentMode_DbRead

#End Region ' DB Functions

  Public Shared Function PaymentModeString(ByVal Mode As HANDPAY_PAYMENT_MODE) As String
    Dim _str_mode As String

    _str_mode = ""

    Select Case (Mode)
      Case HANDPAY_PAYMENT_MODE.MANUAL
        _str_mode = GLB_NLS_GUI_JACKPOT_MGR.GetString(491)
      Case HANDPAY_PAYMENT_MODE.AUTOMATIC
        _str_mode = GLB_NLS_GUI_JACKPOT_MGR.GetString(492)
    End Select

    Return _str_mode
  End Function

End Class ' CLASS_SITE_JACKPOT_PARAMS
