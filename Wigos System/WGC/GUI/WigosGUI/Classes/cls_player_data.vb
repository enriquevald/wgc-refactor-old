'-------------------------------------------------------------------
' Copyright � 2007-2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   CLASS_PLAYER_DATA
' DESCRIPTION:   
' AUTHOR:        Luis Ernesto Mesa
' CREATION DATE: 28-FEB-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 28-FEB-2013  LEM    Initial version
' 28-MAR-2013  LEM    Added new field Twitter Account
' 26-APR-2013  LEM    Fixed Bug #753: Account updating don't update HolderId1 and HolderId2 when RFC or CURP are selected
' 13-JUN-2013  ACM    Added new player data(documents types, nationality, birthPlace, externalNumber, occupation, federalState) and beneficiary data.
' 20-JAN-2014  LEM    Fixed Bug WIGOSTITO-916: Error on saving modified player data.
' 12-FEB-2014  FBA    Added account holder id type control via CardData class IdentificationTypes
' 07-APR-2014  JBP    Added Holder is VIP.
' 03-AGO-2016  FJC    Bug 16072:Recepci�n: Errores Varios
' 03-OCT-2017  AMF    WIGOS-5478 Accounts - Change literals
' 13-FEB-2018  EOR    Bug 31515: WIGOS-8032 [Ticket #12229] Formulario de inscripci�n de jugadores en registro por l�mite de pago 
'--------------------------------------------------------------------

Imports GUI_Controls
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports WSI.Common
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common.Entrances
Imports WSI.Common.Entrances.Entities

Public Class CLASS_PLAYER_DATA
  Inherits CLASS_BASE

#Region " Members "

  Dim m_card_data As CardData

#End Region ' Members

#Region " Properties "

  Public Property CardData() As CardData
    Get
      Return Me.m_card_data
    End Get
    Set(ByVal value As CardData)
      Me.m_card_data = value
    End Set
  End Property
  Dim m_doctype As Dictionary(Of Integer, String) = New Dictionary(Of Integer, String)

  Public Property Doctypes As Dictionary(Of Integer, String)
    Get
      Return m_doctype
    End Get
    Set(value As Dictionary(Of Integer, String))
      m_doctype = value
    End Set
  End Property




#End Region ' Properties

#Region " Publics "


  Public Sub New()
    m_card_data = New CardData()
  End Sub

  ' PURPOSE: Storing new PIN account into database
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: True if successful storing
  Public Function SavePin(ByVal NewPin As String) As Boolean

    Dim _voucher_list As ArrayList
    Dim _session_info As CashierSessionInfo
    Dim _auditor As CLASS_AUDITOR_DATA
    Dim _holder_name As String

    ' It's not a real cashier session. Don't open a cashier session for this movement.
    _session_info = New CashierSessionInfo()
    _session_info.AuthorizedByUserName = CurrentUser.Name
    _session_info.TerminalId = 0
    _session_info.TerminalName = Environment.MachineName
    _session_info.CashierSessionId = 0

    _voucher_list = New ArrayList()

    Using _db_trx As New DB_TRX()

      If Not WSI.Common.CardData.DB_UpdatePinCard(m_card_data, _session_info, NewPin, _
                                                  m_card_data.PlayerTracking.Pin, False, _voucher_list, 0, _db_trx.SqlTransaction) Then
        Return False
      End If

      _db_trx.Commit()

    End Using

    _auditor = New CLASS_AUDITOR_DATA(AUDIT_NAME_ACCOUNT)

    If m_card_data.PlayerTracking.CardLevel = 0 Then
      _holder_name = WSI.Common.Resource.String("STR_FRM_ACCOUNT_USER_EDIT_ACCOUNT_MODE_ANONYMOUS")
    Else
      _holder_name = m_card_data.PlayerTracking.HolderName
    End If

    Call _auditor.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(1745), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1745, m_card_data.AccountId & " - " & _holder_name))

    _auditor.Notify(GLB_CurrentUser.GuiId, GLB_CurrentUser.Id, GLB_CurrentUser.Name, _
                    CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, 0)

    Return True

  End Function ' SavePin

#End Region ' Publics

#Region " Overrides "

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns the related auditor data for the current object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function AuditorData() As CLASS_AUDITOR_DATA

    Dim _auditor As CLASS_AUDITOR_DATA
    Dim _yes_text As String
    Dim _no_text As String

    _yes_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
    _no_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)

    _auditor = New CLASS_AUDITOR_DATA(AUDIT_NAME_ACCOUNT)

    If m_card_data.PlayerTracking.CardLevel = 0 Then
      _auditor.SetName(GLB_NLS_GUI_STATISTICS.Id(207), _
                               m_card_data.AccountId & " - " & WSI.Common.Resource.String("STR_FRM_ACCOUNT_USER_EDIT_ACCOUNT_MODE_ANONYMOUS"))
    Else
      _auditor.SetName(GLB_NLS_GUI_STATISTICS.Id(207), _
                               m_card_data.AccountId & " - " & m_card_data.PlayerTracking.HolderName)
    End If

    ' MAIN:
    ' Name
    _auditor.SetField(0, IIf(String.IsNullOrEmpty(m_card_data.PlayerTracking.HolderName3), AUDIT_NONE_STRING, m_card_data.PlayerTracking.HolderName3), AccountFields.GetName())

    ' Middle name
    _auditor.SetField(0, IIf(String.IsNullOrEmpty(m_card_data.PlayerTracking.HolderName4), AUDIT_NONE_STRING, m_card_data.PlayerTracking.HolderName4), AccountFields.GetMiddleName())
    ' Surname 1
    _auditor.SetField(0, IIf(String.IsNullOrEmpty(m_card_data.PlayerTracking.HolderName1), AUDIT_NONE_STRING, m_card_data.PlayerTracking.HolderName1), AccountFields.GetSurname1())
    ' Surname 2
    _auditor.SetField(0, IIf(String.IsNullOrEmpty(m_card_data.PlayerTracking.HolderName2), AUDIT_NONE_STRING, m_card_data.PlayerTracking.HolderName2), AccountFields.GetSurname2())
    ' VIP
    _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4802), IIf(m_card_data.PlayerTracking.HolderIsVIP, _yes_text, _no_text))
    ' Doc. Type
    _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1721), IdentificationTypes.DocIdTypeString(m_card_data.PlayerTracking.HolderIdType))
    ' Document
    _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1722), IIf(String.IsNullOrEmpty(m_card_data.PlayerTracking.HolderId), AUDIT_NONE_STRING, m_card_data.PlayerTracking.HolderId))
    'RFC
    _auditor.SetField(0, IIf(String.IsNullOrEmpty(m_card_data.PlayerTracking.HolderId1), AUDIT_NONE_STRING, m_card_data.PlayerTracking.HolderId1), IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.RFC))
    'CURP
    _auditor.SetField(0, IIf(String.IsNullOrEmpty(m_card_data.PlayerTracking.HolderId2), AUDIT_NONE_STRING, m_card_data.PlayerTracking.HolderId2), IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.CURP))
    ' Gender
    _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1723), IIf(String.IsNullOrEmpty(WSI.Common.CardData.GenderString(m_card_data.PlayerTracking.HolderGender)), AUDIT_NONE_STRING, WSI.Common.CardData.GenderString(m_card_data.PlayerTracking.HolderGender)))
    ' Marital Status
    _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1724), IIf(String.IsNullOrEmpty(WSI.Common.CardData.MaritalStatusString(m_card_data.PlayerTracking.HolderMaritalStatus - 1)), AUDIT_NONE_STRING, WSI.Common.CardData.MaritalStatusString(m_card_data.PlayerTracking.HolderMaritalStatus - 1)))
    ' Birth Date
    If m_card_data.PlayerTracking.HolderBirthDate <> DateTime.MinValue Then
      _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1725), GUI_FormatDate(m_card_data.PlayerTracking.HolderBirthDate, _
                                                                            ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                            ENUM_FORMAT_TIME.FORMAT_TIME_NONE))
    Else
      _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1725), AUDIT_NONE_STRING)
    End If
    ' WeddingDate
    If m_card_data.PlayerTracking.HolderWeddingDate <> DateTime.MinValue Then
      _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1726), GUI_FormatDate(m_card_data.PlayerTracking.HolderWeddingDate, _
                                                                            ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                            ENUM_FORMAT_TIME.FORMAT_TIME_NONE))
    Else
      _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1726), AUDIT_NONE_STRING)
    End If
    ' Occupation
    _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2058), IIf(String.IsNullOrEmpty(WSI.Common.CardData.OccupationsDescriptionString(m_card_data.PlayerTracking.HolderOccupationId)), AUDIT_NONE_STRING, WSI.Common.CardData.OccupationsDescriptionString(m_card_data.PlayerTracking.HolderOccupationId)))
    ' Birth  Country
    _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2088), IIf(String.IsNullOrEmpty(WSI.Common.CardData.CountriesString(m_card_data.PlayerTracking.HolderBirthCountry)), AUDIT_NONE_STRING, WSI.Common.CardData.CountriesString(m_card_data.PlayerTracking.HolderBirthCountry)))
    ' Nationality
    _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2089), IIf(String.IsNullOrEmpty(WSI.Common.CardData.NationalitiesString(m_card_data.PlayerTracking.HolderNationality)), AUDIT_NONE_STRING, WSI.Common.CardData.NationalitiesString(m_card_data.PlayerTracking.HolderNationality)))

    ' CONTACT:
    ' Phone 1
    _auditor.SetField(0, IIf(String.IsNullOrEmpty(m_card_data.PlayerTracking.HolderPhone01), AUDIT_NONE_STRING, m_card_data.PlayerTracking.HolderPhone01), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1727, "1"))
    ' Phone 2
    _auditor.SetField(0, IIf(String.IsNullOrEmpty(m_card_data.PlayerTracking.HolderPhone02), AUDIT_NONE_STRING, m_card_data.PlayerTracking.HolderPhone02), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1727, "2"))
    ' Email 1
    _auditor.SetField(0, IIf(String.IsNullOrEmpty(m_card_data.PlayerTracking.HolderEmail01), AUDIT_NONE_STRING, m_card_data.PlayerTracking.HolderEmail01), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1729, "1"))
    ' Email 2
    _auditor.SetField(0, IIf(String.IsNullOrEmpty(m_card_data.PlayerTracking.HolderEmail02), AUDIT_NONE_STRING, m_card_data.PlayerTracking.HolderEmail02), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1729, "2"))
    ' Twitter
    _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1730), IIf(String.IsNullOrEmpty(m_card_data.PlayerTracking.HolderTwitter), AUDIT_NONE_STRING, m_card_data.PlayerTracking.HolderTwitter))

    ' ADDRESS:
    ' Street
    _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1731), IIf(String.IsNullOrEmpty(m_card_data.PlayerTracking.HolderAddress01), AUDIT_NONE_STRING, m_card_data.PlayerTracking.HolderAddress01), _
                      AccountFields.GetAddressName())
    ' Colony
    _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1732), IIf(String.IsNullOrEmpty(m_card_data.PlayerTracking.HolderAddress02), AUDIT_NONE_STRING, m_card_data.PlayerTracking.HolderAddress02), _
                      AccountFields.GetColonyName())
    ' Delegation
    _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1733), IIf(String.IsNullOrEmpty(m_card_data.PlayerTracking.HolderAddress03), AUDIT_NONE_STRING, m_card_data.PlayerTracking.HolderAddress03), _
                      AccountFields.GetDelegationName())
    ' City
    _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1734), IIf(String.IsNullOrEmpty(m_card_data.PlayerTracking.HolderCity), AUDIT_NONE_STRING, m_card_data.PlayerTracking.HolderCity), _
                      AccountFields.GetTownshipName())
    ' ZIP
    _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1735), IIf(String.IsNullOrEmpty(m_card_data.PlayerTracking.HolderZip), AUDIT_NONE_STRING, m_card_data.PlayerTracking.HolderZip), _
                      AccountFields.GetZipName())
    ' Federal State
    _auditor.SetField(0, IIf(String.IsNullOrEmpty(WSI.Common.CardData.FedEntitiesString(m_card_data.PlayerTracking.HolderFedEntity)), AUDIT_NONE_STRING, _
                      WSI.Common.CardData.FedEntitiesString(m_card_data.PlayerTracking.HolderFedEntity)), AccountFields.GetStateName())

    ' External Number
    _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2091), IIf(String.IsNullOrEmpty(m_card_data.PlayerTracking.HolderExtNumber), AUDIT_NONE_STRING, m_card_data.PlayerTracking.HolderExtNumber), _
                      AccountFields.GetNumExtName())

    ' Validation address
    _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6705), CardData.GetHolderAddressValidationString(m_card_data.PlayerTracking.HolderAddressValidation))

    ' COMMENTS:
    _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1716), IIf(String.IsNullOrEmpty(m_card_data.PlayerTracking.HolderComments), AUDIT_NONE_STRING, m_card_data.PlayerTracking.HolderComments.Trim()))
    _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5081), IIf(m_card_data.PlayerTracking.ShowCommentsOnCashier, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337)))


    ' BENEFICIARY:
    ' Holder as Beneficiary
    _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2217), IIf(m_card_data.PlayerTracking.HolderHasBeneficiary, GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337)))

    If m_card_data.PlayerTracking.HolderHasBeneficiary = True Then
      ' Name 
      _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2061), IIf(String.IsNullOrEmpty(m_card_data.PlayerTracking.BeneficiaryName3), AUDIT_NONE_STRING, m_card_data.PlayerTracking.BeneficiaryName3))
      ' Surname 1
      _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2062), IIf(String.IsNullOrEmpty(m_card_data.PlayerTracking.BeneficiaryName1), AUDIT_NONE_STRING, m_card_data.PlayerTracking.BeneficiaryName1))
      ' Surname 2
      _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2063), IIf(String.IsNullOrEmpty(m_card_data.PlayerTracking.BeneficiaryName2), AUDIT_NONE_STRING, m_card_data.PlayerTracking.BeneficiaryName2))
      ' Gender
      _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2215), IIf(String.IsNullOrEmpty(WSI.Common.CardData.GenderString(m_card_data.PlayerTracking.BeneficiaryGender)), AUDIT_NONE_STRING, WSI.Common.CardData.GenderString(m_card_data.PlayerTracking.BeneficiaryGender)))
      ' Occupation
      _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2216), IIf(String.IsNullOrEmpty(WSI.Common.CardData.OccupationsDescriptionString(m_card_data.PlayerTracking.BeneficiaryOccupationId)), AUDIT_NONE_STRING, WSI.Common.CardData.OccupationsDescriptionString(m_card_data.PlayerTracking.BeneficiaryOccupationId)))
      ' RFC
      _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2308), IIf(String.IsNullOrEmpty(m_card_data.PlayerTracking.BeneficiaryId1), AUDIT_NONE_STRING, m_card_data.PlayerTracking.BeneficiaryId1))
      ' CURP
      _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2309), IIf(String.IsNullOrEmpty(m_card_data.PlayerTracking.BeneficiaryId2), AUDIT_NONE_STRING, m_card_data.PlayerTracking.BeneficiaryId2))
      ' Birth Date
      If m_card_data.PlayerTracking.BeneficiaryBirthDate <> DateTime.MinValue Then
        _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2092), GUI_FormatDate(m_card_data.PlayerTracking.BeneficiaryBirthDate, _
                                                                              ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                              ENUM_FORMAT_TIME.FORMAT_TIME_NONE))
      Else
        _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2092), AUDIT_NONE_STRING)
      End If
    End If

    Return _auditor

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Deletes the given object from the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return ENUM_STATUS.STATUS_ERROR
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Inserts the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return ENUM_STATUS.STATUS_ERROR
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads from the database into the given object
  '
  ' PARAMS:
  '   - INPUT:
  '     - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As ENUM_STATUS
    Dim ar As Entrances.Entities.AccountsReception
    Dim doctypeid As Integer
    If Doctypes Is Nothing Then
      Doctypes = New Dictionary(Of Integer, String)
    End If

    If CardData.DB_CardGetAllData(ObjectId, m_card_data) Then
      ar = Entrances.Customers.GetCustomerData(CardData.AccountId)
      If Not ar Is Nothing Then
        Doctypes.Clear()
        For Each documentType As KeyValuePair(Of Integer, String) In ar.DocumentTypes
          Doctypes.Add(documentType.Key, documentType.Value)
        Next
      End If

      doctypeid = CardData.PlayerTracking.HolderIdType
      If Doctypes.ContainsKey(doctypeid) Then
        Doctypes(doctypeid) = CardData.PlayerTracking.HolderId
      Else
        Doctypes.Add(doctypeid, CardData.PlayerTracking.HolderId)
      End If

    Else
      Return ENUM_STATUS.STATUS_ERROR
    End If

    Return ENUM_STATUS.STATUS_OK

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Updates the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _session_info As CashierSessionInfo
    Dim _operation_id As Int64
    Dim _ari As WSI.Common.Entrances.Entities.AccountsReception
    ' It's not a real cashier session. Don't open a cashier session for this movement.
    _session_info = New CashierSessionInfo()
    _session_info.AuthorizedByUserName = CurrentUser.Name
    _session_info.TerminalId = 0
    _session_info.TerminalName = Environment.MachineName
    _session_info.CashierSessionId = 0

    Using _trx As New DB_TRX()

      If CardData.DB_UpdateDataCard(m_card_data, _session_info, False, _operation_id, _trx.SqlTransaction) Then

        If Customers.ExistCustomer(m_card_data.AccountId) Then
          _ari = Customers.GetCustomerData(m_card_data.AccountId)
          _ari = UpdateAccountRecord(_ari, m_card_data, m_doctype)
          Customers.SaveCustomer(_ari, _trx.SqlTransaction)
        Else
          _ari = New AccountsReception()
          _ari.AccountId = m_card_data.AccountId
          _ari = UpdateAccountRecord(_ari, m_card_data, m_doctype)
          Customers.UpdateCustomer(_ari, _trx.SqlTransaction)
        End If
      Else
        Return ENUM_STATUS.STATUS_ERROR
      End If

      _trx.Commit()

    End Using

    Return ENUM_STATUS.STATUS_OK

  End Function

  ''' <summary>
  ''' Update CardData data to Account Reception
  ''' </summary>
  ''' <param name="accountsReception"></param>
  ''' <param name="mCardData"></param>
  ''' <param name="docTypes"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function UpdateAccountRecord(accountsReception As AccountsReception, mCardData As CardData, docTypes As Dictionary(Of Integer, String)) As AccountsReception
    'Nota: if we made cchanges in this function, we must check function: PlayerEditDetailsBaseController.cs ==> PlayerDataToModel() for apply same changes if needed
    With accountsReception
      .DocumentTypes = docTypes
      .FirstName = m_card_data.PlayerTracking.HolderName3
      .SecondName = m_card_data.PlayerTracking.HolderName4
      .FirstSurname = m_card_data.PlayerTracking.HolderName1
      .SecondSurname = m_card_data.PlayerTracking.HolderName2
      .Nacionality = m_card_data.PlayerTracking.HolderNationality

      .Contact.Email = m_card_data.PlayerTracking.HolderEmail01
      .Contact.FirstTelephone = m_card_data.PlayerTracking.HolderPhone01
      .Contact.SecondTelephone = m_card_data.PlayerTracking.HolderPhone02
      .Contact.Twitter = m_card_data.PlayerTracking.HolderTwitter

      ' Adress
      For Each _customer_adress As CustomerAddress In .Addresses
        ' Main Adress
        If _customer_adress.AddressType = WSI.Common.Entrances.Enums.ENUM_ADDRESS.HOME_ADRESS Then

          _customer_adress.Street = m_card_data.PlayerTracking.HolderAddress01
          _customer_adress.Colony = m_card_data.PlayerTracking.HolderAddress02
          _customer_adress.Delegation = m_card_data.PlayerTracking.HolderAddress03
          _customer_adress.Number = m_card_data.PlayerTracking.HolderExtNumber
          _customer_adress.Cp = m_card_data.PlayerTracking.HolderZip
          _customer_adress.FederalEntity = m_card_data.PlayerTracking.HolderFedEntity
          _customer_adress.CountryAddress = m_card_data.PlayerTracking.HolderAddressCountry
        End If
      Next

      .Comments = m_card_data.PlayerTracking.HolderComments
      .BirthDate = m_card_data.PlayerTracking.HolderBirthDate
      .Gender = m_card_data.PlayerTracking.HolderGender
    End With

    Return accountsReception
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns a duplicate of the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _clone As CLASS_PLAYER_DATA
    Dim _data As CardData

    _clone = New CLASS_PLAYER_DATA()
    _data = New CardData()

    _data.AccountId = m_card_data.AccountId

    _data.PlayerTracking.HolderName3 = m_card_data.PlayerTracking.HolderName3
    _data.PlayerTracking.HolderName4 = m_card_data.PlayerTracking.HolderName4
    _data.PlayerTracking.HolderName1 = m_card_data.PlayerTracking.HolderName1
    _data.PlayerTracking.HolderName2 = m_card_data.PlayerTracking.HolderName2
    _data.PlayerTracking.HolderName = m_card_data.PlayerTracking.HolderName
    _data.PlayerTracking.HolderIsVIP = m_card_data.PlayerTracking.HolderIsVIP
    _data.PlayerTracking.HolderIdType = m_card_data.PlayerTracking.HolderIdType
    _data.PlayerTracking.HolderId = m_card_data.PlayerTracking.HolderId
    _data.PlayerTracking.HolderId1 = m_card_data.PlayerTracking.HolderId1
    _data.PlayerTracking.HolderId2 = m_card_data.PlayerTracking.HolderId2
    _data.PlayerTracking.HolderGender = m_card_data.PlayerTracking.HolderGender
    _data.PlayerTracking.HolderMaritalStatus = m_card_data.PlayerTracking.HolderMaritalStatus
    _data.PlayerTracking.HolderBirthDate = m_card_data.PlayerTracking.HolderBirthDate
    _data.PlayerTracking.HolderWeddingDate = m_card_data.PlayerTracking.HolderWeddingDate
    _data.PlayerTracking.HolderPhone01 = RTrim(m_card_data.PlayerTracking.HolderPhone01)
    _data.PlayerTracking.HolderPhone02 = RTrim(m_card_data.PlayerTracking.HolderPhone02)
    _data.PlayerTracking.HolderEmail01 = m_card_data.PlayerTracking.HolderEmail01
    _data.PlayerTracking.HolderEmail02 = m_card_data.PlayerTracking.HolderEmail02
    _data.PlayerTracking.HolderTwitter = m_card_data.PlayerTracking.HolderTwitter
    _data.PlayerTracking.HolderAddress01 = RTrim(m_card_data.PlayerTracking.HolderAddress01)
    _data.PlayerTracking.HolderAddress02 = RTrim(m_card_data.PlayerTracking.HolderAddress02)
    _data.PlayerTracking.HolderAddress03 = RTrim(m_card_data.PlayerTracking.HolderAddress03)
    _data.PlayerTracking.HolderCity = RTrim(m_card_data.PlayerTracking.HolderCity)
    _data.PlayerTracking.HolderZip = m_card_data.PlayerTracking.HolderZip
    _data.PlayerTracking.HolderAddressValidation = m_card_data.PlayerTracking.HolderAddressValidation
    _data.PlayerTracking.HolderComments = m_card_data.PlayerTracking.HolderComments
    _data.PlayerTracking.ShowCommentsOnCashier = m_card_data.PlayerTracking.ShowCommentsOnCashier
    _data.PlayerTracking.HolderOccupationId = m_card_data.PlayerTracking.HolderOccupationId
    _data.PlayerTracking.HolderBirthCountry = m_card_data.PlayerTracking.HolderBirthCountry
    _data.PlayerTracking.HolderExtNumber = m_card_data.PlayerTracking.HolderExtNumber
    _data.PlayerTracking.HolderNationality = m_card_data.PlayerTracking.HolderNationality
    _data.PlayerTracking.HolderFedEntity = m_card_data.PlayerTracking.HolderFedEntity
    _data.PlayerTracking.Pin = m_card_data.PlayerTracking.Pin
    _data.CurrentBalance = m_card_data.CurrentBalance
    _data.PlayerTracking.HolderHasBeneficiary = m_card_data.PlayerTracking.HolderHasBeneficiary
    _data.PlayerTracking.BeneficiaryName1 = m_card_data.PlayerTracking.BeneficiaryName1
    _data.PlayerTracking.BeneficiaryName2 = m_card_data.PlayerTracking.BeneficiaryName2
    _data.PlayerTracking.BeneficiaryName3 = m_card_data.PlayerTracking.BeneficiaryName3
    _data.PlayerTracking.BeneficiaryId1 = m_card_data.PlayerTracking.BeneficiaryId1
    _data.PlayerTracking.BeneficiaryId2 = m_card_data.PlayerTracking.BeneficiaryId2
    _data.PlayerTracking.BeneficiaryBirthDate = m_card_data.PlayerTracking.BeneficiaryBirthDate
    _data.PlayerTracking.BeneficiaryOccupationId = m_card_data.PlayerTracking.BeneficiaryOccupationId
    _data.PlayerTracking.BeneficiaryGender = m_card_data.PlayerTracking.BeneficiaryGender

    _clone.CardData = _data


    Return _clone

  End Function

#End Region ' Overrides


End Class
