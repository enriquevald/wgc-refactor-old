﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_junket.vb
' DESCRIPTION:   Junket Class
' AUTHOR:        David Perelló
' CREATION DATE: 03-APR-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 03-APR-2017  DPC    Initial Version
' 31-JUL-2017  DPC    WIGOS-4050: Junkets: commission amount is not showing the real value
'--------------------------------------------------------------------
Imports GUI_CommonOperations
Imports System.Runtime.InteropServices
Imports WSI.Common
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Text
Imports System.Data.SqlClient
Imports WSI.Common.Junkets

Public Class CLASS_JUNKETS
  Inherits CLASS_BASE

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_JUNKET
    Public id As Int64
    Public code As String
    Public name As String
    Public junket_representative As TYPE_JUNKET_REPRESENTATIVE
    Public date_from As DateTime
    Public date_to As DateTime
    Public commissions As List(Of Commission)
    Public comment As String
    Public creation As DateTime
    Public update As DateTime
    Public flyers As List(Of CLASS_JUNKETS_FLYER.TYPE_FLYER)

    Public Sub New()
      Me.creation = GUI_FormatDate(WGDB.Now, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      Me.update = GUI_FormatDate(WGDB.Now, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      Me.date_from = GUI_FormatDate(GUI_GetDate.AddHours(GetDefaultClosingTime()), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      Me.date_to = GUI_FormatDate(GUI_GetDate.AddMonths(1).AddHours(GetDefaultClosingTime()), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      Me.junket_representative = New TYPE_JUNKET_REPRESENTATIVE
      Me.commissions = New List(Of Commission)
      Me.flyers = New List(Of CLASS_JUNKETS_FLYER.TYPE_FLYER)

      For Each _type As CommissionType In System.Enum.GetValues(GetType(CommissionType))
        Me.commissions.Add(New Commission(_type))
      Next

    End Sub
  End Class

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_JUNKET_REPRESENTATIVE
    Public id As Int64
    Public code As String
    Public name As String
  End Class

#End Region ' Structures

#Region " Enums "

  Public Enum ReportType
    Active
    StartDate
  End Enum ' ReportType

#End Region ' Enums

#Region " Members "

  Protected m_junket As New TYPE_JUNKET

#End Region ' Members

#Region " Properties "

  Public Property Id() As String
    Get
      Return m_junket.id
    End Get
    Set(ByVal value As String)
      m_junket.id = value
    End Set
  End Property

  Public Property Code() As String
    Get
      Return m_junket.code
    End Get
    Set(ByVal value As String)
      m_junket.code = value
    End Set
  End Property

  Public Property Name() As String
    Get
      Return m_junket.name
    End Get
    Set(ByVal value As String)
      m_junket.name = value
    End Set
  End Property

  Public Property JunketRepresentative() As TYPE_JUNKET_REPRESENTATIVE
    Get
      Return m_junket.junket_representative
    End Get
    Set(ByVal value As TYPE_JUNKET_REPRESENTATIVE)
      m_junket.junket_representative = value
    End Set
  End Property

  Public Property DateFrom() As DateTime
    Get
      Return m_junket.date_from
    End Get
    Set(ByVal value As DateTime)
      m_junket.date_from = value
    End Set
  End Property

  Public Property DateTo() As DateTime
    Get
      Return m_junket.date_to
    End Get
    Set(ByVal value As DateTime)
      m_junket.date_to = value
    End Set
  End Property

  Public Property Commissions() As List(Of Commission)
    Get
      Return m_junket.commissions
    End Get
    Set(ByVal value As List(Of Commission))
      m_junket.commissions = value
    End Set
  End Property

  Public Property Comment() As String
    Get
      Return m_junket.comment
    End Get
    Set(ByVal value As String)
      m_junket.comment = value
    End Set
  End Property

  Public Property Creation() As DateTime
    Get
      Return m_junket.creation
    End Get
    Set(ByVal value As DateTime)
      m_junket.creation = value
    End Set
  End Property

  Public Property Update() As DateTime
    Get
      Return m_junket.update
    End Get
    Set(ByVal value As DateTime)
      m_junket.update = value
    End Set
  End Property

  Public Property Flyers() As List(Of CLASS_JUNKETS_FLYER.TYPE_FLYER)
    Get
      Return m_junket.flyers
    End Get
    Set(ByVal value As List(Of CLASS_JUNKETS_FLYER.TYPE_FLYER))
      m_junket.flyers = value
    End Set
  End Property

#End Region ' Properties

#Region " Overrides "

  ''' <summary>
  ''' AuditData
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function AuditorData() As CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_JUNKETS)

    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(7997), Me.Code)

    Call _auditor_data.SetField(0, IIf(String.IsNullOrEmpty(Code), AUDIT_NONE_STRING, Code), _
                                String.Format("{0} {1}", GLB_NLS_GUI_PLAYER_TRACKING.GetString(5710), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8014)))

    Call _auditor_data.SetField(0, IIf(String.IsNullOrEmpty(Name), AUDIT_NONE_STRING, Name), _
                                String.Format("{0} {1}", GLB_NLS_GUI_PLAYER_TRACKING.GetString(4399), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8014)))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6506), GUI_FormatDate(Creation, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(297), GUI_FormatDate(DateFrom, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(298), GUI_FormatDate(DateTo, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS))

    Call _auditor_data.SetField(0, IIf(String.IsNullOrEmpty(JunketRepresentative.code), AUDIT_NONE_STRING, JunketRepresentative.code), _
                                String.Format("{0} {1}", GLB_NLS_GUI_PLAYER_TRACKING.GetString(5710), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8015)))

    Call _auditor_data.SetField(0, IIf(String.IsNullOrEmpty(JunketRepresentative.name), AUDIT_NONE_STRING, JunketRepresentative.name), _
                                String.Format("{0} {1}", GLB_NLS_GUI_PLAYER_TRACKING.GetString(4399), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8015)))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8007), GUI_FormatCurrency(getValueComission(CommissionType.ByCostumer), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8008), GUI_FormatCurrency(getValueComission(CommissionType.ByVisit), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8009), GUI_FormatCurrency(getValueComission(CommissionType.ByEnrolled), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8013), GUI_FormatCurrency(getValueComission(CommissionType.ByPoints), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8010), String.Format("{0}%", GUI_ParseNumber(getValueComission(CommissionType.ByCoinIn))))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8012), String.Format("{0}%", GUI_ParseNumber(getValueComission(CommissionType.ByBuyIn))))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8011), String.Format("{0}%", GUI_ParseNumber(getValueComission(CommissionType.ByTheoricalNetwin))))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1716), IIf(String.IsNullOrEmpty(Comment), AUDIT_NONE_STRING, Comment))

    Return _auditor_data

  End Function ' AuditorData

  ''' <summary>
  ''' DB_Delete
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS

  End Function ' DB_Delete

  ''' <summary>
  ''' DB_Insert
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS
    Return InsertJunket()
  End Function ' DB_Insert

  ''' <summary>
  ''' DB_Read
  ''' </summary>
  ''' <param name="ObjectId"></param>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Read(ObjectId As Object, ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS
    Return GetJunket(ObjectId)
  End Function ' DB_Read

  ''' <summary>
  ''' DB_Update
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS
    Return UpdateJunket()
  End Function ' DB_Update

  ''' <summary>
  ''' Duplicate
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function Duplicate() As CLASS_BASE

    Dim _temp_object As CLASS_JUNKETS

    _temp_object = Me.Clone

    Return _temp_object

  End Function ' Duplicate

#End Region ' Overrides

#Region " DB Actions "

  ''' <summary>
  ''' Insert a new junket
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function InsertJunket() As Integer

    Dim _sb As StringBuilder
    Dim _exist_code As Boolean
    Dim _sql_trx As SqlTransaction = Nothing
    Dim _param_id_junket As SqlParameter
    Dim _dt_commissions As DataTable
    Dim _num_inserted As Integer

    Try

      If Not GUI_BeginSQLTransaction(_sql_trx) Then

        Return ENUM_STATUS.STATUS_ERROR
      End If

      _sb = New StringBuilder
      _sb.AppendLine("   SELECT   COUNT(1)           ")
      _sb.AppendLine("     FROM   JUNKETS            ")
      _sb.AppendLine("    WHERE   JU_CODE = @pCode   ")

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), _sql_trx.Connection, _sql_trx)

        _cmd.Parameters.Add("@pCode", SqlDbType.NVarChar).Value = m_junket.code
        _exist_code = (_cmd.ExecuteScalar() > 0)

        If _exist_code Then
          Return ENUM_STATUS.STATUS_DUPLICATE_KEY
        End If

        _sb = New StringBuilder
        _sb.AppendLine("   INSERT INTO   JUNKETS                 ")
        _sb.AppendLine("               ( JU_REPRESENTATIVE_ID    ")
        _sb.AppendLine("               , JU_CODE                 ")
        _sb.AppendLine("               , JU_NAME                 ")
        _sb.AppendLine("               , JU_COMMENT              ")
        _sb.AppendLine("               , JU_DATE_FROM            ")
        _sb.AppendLine("               , JU_DATE_TO              ")
        _sb.AppendLine("               , JU_CREATION             ")
        _sb.AppendLine("               , JU_UPDATE )             ")
        _sb.AppendLine("        VALUES                           ")
        _sb.AppendLine("               ( @pRepresentativeId      ")
        _sb.AppendLine("               , @pCode                  ")
        _sb.AppendLine("               , @pName                  ")
        _sb.AppendLine("               , @pComment               ")
        _sb.AppendLine("               , @pFrom                  ")
        _sb.AppendLine("               , @pTo                    ")
        _sb.AppendLine("               , @pCreation              ")
        _sb.AppendLine("               , @pUpdate )              ")
        _sb.AppendLine("   SET @pID = SCOPE_IDENTITY();         ")

        _cmd.CommandText = _sb.ToString()
        _cmd.Parameters.Clear()

        _cmd.Parameters.Add("@pRepresentativeId", SqlDbType.BigInt).Value = m_junket.junket_representative.id
        _cmd.Parameters.Add("@pCode", SqlDbType.NVarChar).Value = m_junket.code
        _cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = m_junket.name
        _cmd.Parameters.Add("@pComment", SqlDbType.NVarChar).Value = m_junket.comment
        _cmd.Parameters.Add("@pFrom", SqlDbType.DateTime).Value = m_junket.date_from
        _cmd.Parameters.Add("@pTo", SqlDbType.DateTime).Value = m_junket.date_to
        _cmd.Parameters.Add("@pCreation", SqlDbType.DateTime).Value = m_junket.creation
        _cmd.Parameters.Add("@pUpdate", SqlDbType.DateTime).Value = m_junket.update

        _param_id_junket = _cmd.Parameters.Add("@pID", SqlDbType.BigInt)
        _param_id_junket.Direction = ParameterDirection.Output

        If _cmd.ExecuteNonQuery() <> 1 Then
          GUI_EndSQLTransaction(_sql_trx, False)

          Return ENUM_STATUS.STATUS_ERROR
        End If

        m_junket.id = _param_id_junket.Value

        _dt_commissions = CommissionsToDatatble(DataRowState.Added)

        Using _da As New SqlDataAdapter()
          _da.InsertCommand = CreateInsertCommandCommissions(_sql_trx)
          _num_inserted = _da.Update(_dt_commissions)
        End Using

        If _num_inserted <> _dt_commissions.Rows.Count Then
          Return ENUM_STATUS.STATUS_ERROR
        End If

        If Not GUI_EndSQLTransaction(_sql_trx, True) Then
          Return ENUM_STATUS.STATUS_ERROR
        End If

      End Using

    Catch _ex As Exception
      '  ' Rollback  
      GUI_EndSQLTransaction(_sql_trx, False)

      Return ENUM_STATUS.STATUS_ERROR
    End Try

    Return ENUM_STATUS.STATUS_OK

  End Function ' InsertJunket

  ''' <summary>
  ''' Get a representative junket
  ''' </summary>
  ''' <param name="ObjectId"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetJunket(Code As String) As Integer

    Dim _sb As StringBuilder
    Dim _ds As DataSet
    Dim _row_junket As DataRow
    Dim _row_junket_representative As DataRow

    Try

      _ds = New DataSet
      _row_junket = Nothing
      _row_junket_representative = Nothing

      _sb = New StringBuilder
      _sb.AppendLine("      SELECT   JU_ID                                                               ")
      _sb.AppendLine("             , JU_REPRESENTATIVE_ID                                                ")
      _sb.AppendLine("             , JU_CODE                                                             ")
      _sb.AppendLine("             , JU_NAME                                                             ")
      _sb.AppendLine("             , JU_COMMENT                                                          ")
      _sb.AppendLine("             , JU_DATE_FROM                                                        ")
      _sb.AppendLine("             , JU_DATE_TO                                                          ")
      _sb.AppendLine("             , JU_CREATION                                                         ")
      _sb.AppendLine("             , JU_UPDATE                                                           ")
      _sb.AppendLine("        FROM   JUNKETS                                                             ")
      _sb.AppendLine("       WHERE   JU_ID = @pJunketId;                                                 ")

      _sb.AppendLine("      SELECT   JC_ID                                                               ")
      _sb.AppendLine("             , JC_TYPE                                                             ")
      _sb.AppendLine("             , JC_FIXED_VALUE                                                      ")
      _sb.AppendLine("             , JC_VARIABLE_VALUE                                                   ")
      _sb.AppendLine("             , JC_DATE_FROM                                                        ")
      _sb.AppendLine("             , JC_DATE_TO                                                          ")
      _sb.AppendLine("             , JC_CREATION                                                         ")
      _sb.AppendLine("             , JC_UPDATE                                                           ")
      _sb.AppendLine("        FROM   JUNKETS_COMMISSIONS                                                 ")
      _sb.AppendLine("       WHERE   JC_JUNKET_ID = @pJunketId;                                          ")

      _sb.AppendLine("      SELECT   JR_ID                                                               ")
      _sb.AppendLine("             , JR_CODE                                                             ")
      _sb.AppendLine("             , JR_NAME                                                             ")
      _sb.AppendLine("        FROM   JUNKETS_REPRESENTATIVES JR                                          ")
      _sb.AppendLine("  INNER JOIN   JUNKETS JU ON JU.JU_REPRESENTATIVE_ID = JR.JR_ID                    ")
      _sb.AppendLine("       WHERE   JU_ID = @pJunketId;                                                 ")

      Using _db_trx As New WSI.Common.DB_TRX()
        Using _cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _cmd.Parameters.Add("@pJunketId", SqlDbType.NVarChar).Value = Code

          Using _reader As SqlDataAdapter = New SqlDataAdapter(_cmd)

            _reader.Fill(_ds)

            _ds.Tables(0).TableName = "JUNKETS"
            _ds.Tables(1).TableName = "JUNKETS_COMMISSIONS"
            _ds.Tables(2).TableName = "JUNKETS_REPRESENTATIVES"

            If _ds.Tables("JUNKETS").Rows.Count > 0 Then
              _row_junket = _ds.Tables("JUNKETS").Rows(0)
            End If

            If _ds.Tables("JUNKETS_REPRESENTATIVES").Rows.Count > 0 Then
              _row_junket_representative = _ds.Tables("JUNKETS_REPRESENTATIVES").Rows(0)
            End If

            m_junket.commissions.Clear()

            m_junket.id = _row_junket("JU_ID")
            m_junket.code = _row_junket("JU_CODE")
            m_junket.name = _row_junket("JU_NAME")
            m_junket.comment = _row_junket("JU_COMMENT").ToString()
            m_junket.date_from = _row_junket("JU_DATE_FROM")
            m_junket.date_to = _row_junket("JU_DATE_TO")
            m_junket.creation = _row_junket("JU_CREATION")
            m_junket.update = _row_junket("JU_UPDATE")

            m_junket.junket_representative.id = _row_junket_representative("JR_ID")
            m_junket.junket_representative.code = _row_junket_representative("JR_CODE")
            m_junket.junket_representative.name = _row_junket_representative("JR_NAME")

            Dim _comission As Commission

            For Each _row As DataRow In _ds.Tables("JUNKETS_COMMISSIONS").Rows

              _comission = New Commission

              _comission.Id = _row("JC_ID")
              _comission.Type = _row("JC_TYPE")
              _comission.FixedValue = _row("JC_FIXED_VALUE")
              _comission.VariableValue = _row("JC_VARIABLE_VALUE")
              _comission.DateFrom = _row("JC_DATE_FROM")
              _comission.DateTo = _row("JC_DATE_TO")
              _comission.Creation = _row("JC_CREATION")
              _comission.Update = _row("JC_UPDATE")

              m_junket.commissions.Add(_comission)

            Next

            If Not SetFlyersToJunket(_db_trx.SqlTransaction) Then

              Return ENUM_STATUS.STATUS_ERROR
            End If

          End Using
        End Using
      End Using

    Catch _ex As Exception

      Return ENUM_STATUS.STATUS_ERROR
    End Try

    Return ENUM_STATUS.STATUS_OK

  End Function ' GetJunket

  ''' <summary>
  ''' Edit a junket
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function UpdateJunket() As Integer

    Dim _sb As StringBuilder
    Dim _sql_trx As SqlTransaction = Nothing
    Dim _dt_commissions As DataTable
    Dim _num_updated As Integer

    Try

      If Not GUI_BeginSQLTransaction(_sql_trx) Then

        Return ENUM_STATUS.STATUS_ERROR
      End If

      _sb = New StringBuilder
      _sb.AppendLine("   UPDATE   JUNKETS                    				           ")
      _sb.AppendLine("      SET   JU_REPRESENTATIVE_ID = @pRepresentativeId    ")
      _sb.AppendLine("          , JU_CODE = @pCode                             ")
      _sb.AppendLine("          , JU_NAME = @pName                             ")
      _sb.AppendLine("          , JU_COMMENT = @pComment                       ")
      _sb.AppendLine("          , JU_DATE_FROM = @pFrom                        ")
      _sb.AppendLine("          , JU_DATE_TO = @pTo                            ")
      _sb.AppendLine("          , JU_UPDATE = @pUpdate                         ")
      _sb.AppendLine("    WHERE   JU_ID = @pJunketId                           ")

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), _sql_trx.Connection, _sql_trx)

        _cmd.Parameters.Add("@pJunketId", SqlDbType.BigInt).Value = m_junket.id
        _cmd.Parameters.Add("@pRepresentativeId", SqlDbType.BigInt).Value = m_junket.junket_representative.id
        _cmd.Parameters.Add("@pCode", SqlDbType.NVarChar).Value = m_junket.code
        _cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = m_junket.name
        _cmd.Parameters.Add("@pComment", SqlDbType.NVarChar).Value = m_junket.comment
        _cmd.Parameters.Add("@pFrom", SqlDbType.DateTime).Value = m_junket.date_from
        _cmd.Parameters.Add("@pTo", SqlDbType.DateTime).Value = m_junket.date_to
        _cmd.Parameters.Add("@pUpdate", SqlDbType.DateTime).Value = GUI_GetDateTime()

        If _cmd.ExecuteNonQuery() <> 1 Then
          GUI_EndSQLTransaction(_sql_trx, False)

          Return ENUM_STATUS.STATUS_ERROR
        End If

        _dt_commissions = CommissionsToDatatble(DataRowState.Modified)

        Using _da As New SqlDataAdapter()
          _da.UpdateCommand = CreateUpdateCommandCommissions(_sql_trx)
          _num_updated = _da.Update(_dt_commissions)
        End Using

        If _num_updated <> _dt_commissions.Rows.Count Then
          Return ENUM_STATUS.STATUS_ERROR
        End If

        If Not GUI_EndSQLTransaction(_sql_trx, True) Then
          Return ENUM_STATUS.STATUS_ERROR
        End If

      End Using

    Catch _ex As Exception
      '  ' Rollback  
      GUI_EndSQLTransaction(_sql_trx, False)

      Return ENUM_STATUS.STATUS_ERROR
    End Try

    Return ENUM_STATUS.STATUS_OK

  End Function ' UpdateJunket

  ''' <summary>
  ''' Create update command for comissions
  ''' </summary>
  ''' <param name="SqlTrx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CreateUpdateCommandCommissions(ByVal SqlTrx As SqlTransaction) As SqlCommand

    Dim _sb As StringBuilder
    Dim _sql_command As SqlCommand

    Try

      _sb = New StringBuilder
      _sb.AppendLine("   UPDATE   JUNKETS_COMMISSIONS                   ")
      _sb.AppendLine("      SET   JC_TYPE           = @pType            ")
      _sb.AppendLine("          , JC_FIXED_VALUE    = @pFixedValue      ")
      _sb.AppendLine("          , JC_VARIABLE_VALUE = @pVariableValue   ")
      _sb.AppendLine("          , JC_DATE_FROM      = @pFrom            ")
      _sb.AppendLine("          , JC_DATE_TO        = @pTo              ")
      _sb.AppendLine("          , JC_UPDATE         = @pUpdate          ")
      _sb.AppendLine("    WHERE   JC_ID             = @pComissionId     ")

      _sql_command = New SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx)
      _sql_command.Parameters.Add("@pComissionId", SqlDbType.BigInt).SourceColumn = "ID"
      _sql_command.Parameters.Add("@pType", SqlDbType.Int).SourceColumn = "TYPE"
      _sql_command.Parameters.Add("@pFixedValue", SqlDbType.Money).SourceColumn = "FIXEDVALUE"
      _sql_command.Parameters.Add("@pVariableValue", SqlDbType.Decimal).SourceColumn = "VARIABLEVALUE"
      _sql_command.Parameters.Add("@pFrom", SqlDbType.DateTime).SourceColumn = "DATEFROM"
      _sql_command.Parameters.Add("@pTo", SqlDbType.DateTime).SourceColumn = "DATETO"
      _sql_command.Parameters.Add("@pUpdate", SqlDbType.DateTime).SourceColumn = "UPDATE"

      Return _sql_command

    Catch _ex As Exception
      Log.Error(_ex.Message())

      Throw
    End Try

  End Function ' CreateUpdateCommandCommissions

  ''' <summary>
  ''' Create insert command for comissions
  ''' </summary>
  ''' <param name="SqlTrx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CreateInsertCommandCommissions(ByVal SqlTrx As SqlTransaction) As SqlCommand

    Dim _sb As StringBuilder
    Dim _sql_command As SqlCommand

    Try

      _sb = New StringBuilder
      _sb.AppendLine("   INSERT INTO   JUNKETS_COMMISSIONS   ")
      _sb.AppendLine("               ( JC_JUNKET_ID          ")
      _sb.AppendLine("               , JC_TYPE               ")
      _sb.AppendLine("               , JC_FIXED_VALUE        ")
      _sb.AppendLine("               , JC_VARIABLE_VALUE     ")
      _sb.AppendLine("               , JC_DATE_FROM          ")
      _sb.AppendLine("               , JC_DATE_TO            ")
      _sb.AppendLine("               , JC_CREATION           ")
      _sb.AppendLine("               , JC_UPDATE )           ")
      _sb.AppendLine("        VALUES                         ")
      _sb.AppendLine("               ( @pJunketId            ")
      _sb.AppendLine("               , @pType                ")
      _sb.AppendLine("               , @pFixedValue          ")
      _sb.AppendLine("               , @pVariableValue       ")
      _sb.AppendLine("               , @pFrom                ")
      _sb.AppendLine("               , @pTo                  ")
      _sb.AppendLine("               , @pCreation            ")
      _sb.AppendLine("               , @pUpdate )            ")

      _sql_command = New SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx)
      _sql_command.Parameters.Add("@pJunketId", SqlDbType.BigInt).Value = m_junket.id
      _sql_command.Parameters.Add("@pType", SqlDbType.Int).SourceColumn = "TYPE"
      _sql_command.Parameters.Add("@pFixedValue", SqlDbType.Money).SourceColumn = "FIXEDVALUE"
      _sql_command.Parameters.Add("@pVariableValue", SqlDbType.Decimal).SourceColumn = "VARIABLEVALUE"
      _sql_command.Parameters.Add("@pFrom", SqlDbType.DateTime).SourceColumn = "DATEFROM"
      _sql_command.Parameters.Add("@pTo", SqlDbType.DateTime).SourceColumn = "DATETO"
      _sql_command.Parameters.Add("@pCreation", SqlDbType.DateTime).SourceColumn = "CREATION"
      _sql_command.Parameters.Add("@pUpdate", SqlDbType.DateTime).SourceColumn = "UPDATE"

      Return _sql_command

    Catch _ex As Exception
      Log.Error(_ex.Message())

      Throw
    End Try

  End Function ' CreateInsertCommandCommissions

  ''' <summary>
  ''' Get Flyers linked to junket
  ''' </summary>
  ''' <param name="Trx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function SetFlyersToJunket(ByRef Trx As SqlTransaction)

    Dim _sb As StringBuilder
    'Dim _dt_flyers As DataTable
    Dim _flyer As CLASS_JUNKETS_FLYER.TYPE_FLYER

    Try

      '_dt_flyers = New DataTable

      _sb = New StringBuilder
      _sb.AppendLine("       SELECT   JF_ID                                          ")
      _sb.AppendLine("              , JF_CODE                                        ")
      _sb.AppendLine("              , JF_COMMENT                                     ")
      _sb.AppendLine("         FROM   JUNKETS_FLYERS JF                              ")
      _sb.AppendLine("   INNER JOIN   JUNKETS JU ON JU.JU_ID = JF.JF_JUNKET_ID       ")
      _sb.AppendLine("        WHERE   JU_ID = @pJunketId                             ")

      Using _cmd As New SqlCommand(_sb.ToString(), Trx.Connection, Trx)

        _cmd.Parameters.Add("@pJunketId", SqlDbType.NVarChar).Value = m_junket.id

        Using _reader As SqlDataReader = _cmd.ExecuteReader()

          While (_reader.Read())

            _flyer = New CLASS_JUNKETS_FLYER.TYPE_FLYER(False)
            _flyer.id = _reader("JF_ID")
            _flyer.code = _reader("JF_CODE")
            _flyer.comment = _reader("JF_COMMENT")

            m_junket.flyers.Add(_flyer)

          End While

        End Using
      End Using

      Return True

    Catch _ex As Exception
      Log.Error(_ex.Message())
    End Try

    Return False

  End Function ' SetFlyersToJunket

  ''' <summary>
  ''' Get flyers to junket, create db trx
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function SetFlyersToJunket()

    Try

      Flyers.Clear()

      Using _db_trx As New WSI.Common.DB_TRX()
        SetFlyersToJunket(_db_trx.SqlTransaction)
      End Using

      Return True

    Catch _ex As Exception
      Log.Error(_ex.Message())
    End Try

    Return False

  End Function ' SetFlyersToJunket

#End Region ' DB Actions

#Region " Public Methods "

  ''' <summary>
  ''' String Query for get the junkets
  ''' </summary>
  ''' <param name="CodeJunket"></param>
  ''' <param name="NameJunket"></param>
  ''' <param name="CodeRepresentative"></param>
  ''' <param name="NameRepresentative"></param>
  ''' <param name="Type"></param>
  ''' <param name="InitFrom"></param>
  ''' <param name="InitTo"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function GetJunkets(CodeJunket As String, NameJunket As String,
                             CodeRepresentative As String, NameRepresentative As String,
                             Type As ReportType, InitFrom As String, InitTo As String) As String

    Dim _sql_str As StringBuilder
    Dim _now As String

    _now = GUI_FormatDateDB(WGDB.Now)

    _sql_str = New StringBuilder()
    _sql_str.AppendLine("     SELECT   JU_CODE                                                 ")
    _sql_str.AppendLine("            , JU_NAME                                                 ")
    _sql_str.AppendLine("            , JR_CODE                                                 ")
    _sql_str.AppendLine("            , JR_NAME                                                 ")
    _sql_str.AppendLine("            , JU_DATE_FROM                                            ")
    _sql_str.AppendLine("            , JU_DATE_TO                                              ")
    _sql_str.AppendLine("            , JU_ID                                                   ")
    _sql_str.AppendLine("       FROM   JUNKETS                                                 ")
    _sql_str.AppendLine(" INNER JOIN   JUNKETS_REPRESENTATIVES ON JU_REPRESENTATIVE_ID = JR_ID ")

    If Not String.IsNullOrEmpty(CodeRepresentative) Then
      _sql_str.AppendLine(String.Format("    AND   {0}", GUI_FilterField("JR_CODE", CodeRepresentative, False, False, True)))
    End If

    If Not String.IsNullOrEmpty(NameRepresentative) Then
      _sql_str.AppendLine(String.Format("    AND   {0}", GUI_FilterField("JR_NAME", NameRepresentative, False, False, True)))
    End If

    _sql_str.AppendLine("      WHERE   1 = 1                                                   ")

    If Not String.IsNullOrEmpty(CodeJunket) Then
      _sql_str.AppendLine(String.Format("    AND   {0}", GUI_FilterField("JU_CODE", CodeJunket, False, False, True)))
    End If

    If Not String.IsNullOrEmpty(NameJunket) Then
      _sql_str.AppendLine(String.Format("    AND   {0}", GUI_FilterField("JU_NAME", NameJunket, False, False, True)))
    End If

    Select Case Type
      Case ReportType.Active
        _sql_str.AppendLine(String.Format("    AND   JU_DATE_TO > {0}", InitFrom))

        If Not String.IsNullOrEmpty(InitTo) Then
          _sql_str.AppendLine(String.Format("    AND   JU_DATE_FROM < {0}", InitTo))
        End If

      Case ReportType.StartDate
        _sql_str.AppendLine(String.Format("    AND   JU_DATE_FROM >= {0}", InitFrom))

        If Not String.IsNullOrEmpty(InitTo) Then
          _sql_str.AppendLine(String.Format("    AND   JU_DATE_FROM < {0}", InitTo))
        End If

    End Select

    _sql_str.AppendLine("   ORDER   BY JU_DATE_FROM DESC, JU_CODE, JR_CODE ")

    Return _sql_str.ToString

  End Function ' GetJunkets

  ''' <summary>
  ''' Set a comission or set and add comission in list
  ''' </summary>
  ''' <param name="Type"></param>
  ''' <param name="Value"></param>
  ''' <remarks></remarks>
  Public Sub SetComission(ByVal Type As CommissionType, ByVal Value As String)

    Dim _comission As Commission
    Dim _is_new_comission As Boolean

    _comission = Me.Commissions.Find(Function(a) a.Type = Type)

    _is_new_comission = (_comission Is Nothing)

    If _is_new_comission Then
      _comission = New Commission
    End If

    _comission.Type = Type
    _comission.DateFrom = Me.DateFrom
    _comission.DateTo = Me.DateTo

    Select Case Type
      Case CommissionType.ByCostumer, _
           CommissionType.ByVisit, _
           CommissionType.ByEnrolled, _
           CommissionType.ByPoints

        _comission.FixedValue = GUI_ParseCurrency(Value)

      Case CommissionType.ByCoinIn, _
           CommissionType.ByTheoricalNetwin, _
           CommissionType.ByBuyIn

        _comission.VariableValue = GUI_ParseNumber(Value)

    End Select

    If _is_new_comission Then
      Commissions.Add(_comission)
    End If

  End Sub ' SetComission

  ''' <summary>
  ''' Get value comission for type
  ''' </summary>
  ''' <param name="Type"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function getValueComission(ByVal Type As CommissionType) As Decimal

    Try

      Select Case Type
        Case CommissionType.ByCostumer, _
             CommissionType.ByVisit, _
             CommissionType.ByEnrolled, _
             CommissionType.ByPoints

          Return Me.Commissions.Find(Function(_comission) _comission.Type = Type).FixedValue

        Case CommissionType.ByCoinIn, _
             CommissionType.ByTheoricalNetwin, _
             CommissionType.ByBuyIn

          Return Me.Commissions.Find(Function(_comission) _comission.Type = Type).VariableValue

      End Select

    Catch _ex As Exception
      Return 0
    End Try

  End Function ' getValueComission

  ''' <summary>
  ''' Check if junket exist by code
  ''' </summary>
  ''' <param name="Code"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ExistJunket(Code As String) As Integer

    Dim _sb As StringBuilder
    Dim _exist_code As Boolean
    Dim _sql_trx As SqlTransaction = Nothing

    Try

      _sb = New StringBuilder
      _sb.AppendLine(" SELECT   COUNT(1)             ")
      _sb.AppendLine("   FROM   JUNKETS              ")
      _sb.AppendLine("  WHERE   JU_CODE = @pCode     ")

      Using _db_trx As New WSI.Common.DB_TRX()
        Using _cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _cmd.Parameters.Add("@pCode", SqlDbType.NVarChar).Value = Code
          _exist_code = (_cmd.ExecuteScalar() > 0)

          If _exist_code Then
            Return ENUM_STATUS.STATUS_DUPLICATE_KEY
          End If

        End Using
      End Using

    Catch _ex As Exception

      Return ENUM_STATUS.STATUS_ERROR
    End Try

    Return ENUM_STATUS.STATUS_OK

  End Function ' ExistJunket

  ''' <summary>
  ''' Get sql for working day
  ''' </summary>
  ''' <param name="DateFrom"></param>
  ''' <param name="DateTo"></param>
  ''' <param name="AccountDetails"></param>
  ''' <param name="ListRepresentatives"></param>
  ''' <param name="ListJunkets"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function WorkingDayQuery(ByVal DateFrom As uc_date_picker, ByVal DateTo As uc_date_picker, ByVal AccountDetails As uc_account_sel, _
                                  ByRef ListRepresentatives As String, ByRef ListJunkets As String, ByVal ExcludeWithoutActivity As Boolean) As String
    Dim _sb As StringBuilder
    Dim _sql_where As String = String.Empty
    Dim _sql_where_exclude As String = String.Empty

    _sb = New StringBuilder()

    _sb.AppendLine("    SELECT   JU_CODE                                                                                       ")
    _sb.AppendLine("           , JU_NAME                                                                                       ")
    _sb.AppendLine("           , JU_DATE_FROM                                                                                  ")
    _sb.AppendLine("           , JU_DATE_TO                                                                                    ")
    _sb.AppendLine("           , JR_CODE                                                                                       ")
    _sb.AppendLine("           , JR_NAME                                                                                       ")
    _sb.AppendLine("           , [dbo].[Opening](0,JCM_CREATION) AS GAMING_DAY                                                 ")
    _sb.AppendLine("           , SUM(CASE WHEN JCM_TYPE = 0 THEN JCM_AMOUNT ELSE 0 END) AS CUSTOMERCOMMISSION                  ")
    _sb.AppendLine("           , SUM(CASE WHEN JCM_TYPE = 1 THEN JCM_AMOUNT ELSE 0 END) AS VISITCOMMISSION                     ")
    _sb.AppendLine("           , SUM(CASE WHEN JCM_TYPE = 2 THEN JCM_AMOUNT ELSE 0 END) AS NEWCUSTOMERSCOMMISSION              ")
    _sb.AppendLine("           , SUM(CASE WHEN JCM_TYPE = 6 THEN JCM_AMOUNT ELSE 0 END) AS POINTSCOMMISSION                    ")
    _sb.AppendLine("           , SUM(CASE WHEN JCM_TYPE = 3 THEN JCM_AMOUNT ELSE 0 END) AS COININCOMMISSION                    ")
    _sb.AppendLine("           , SUM(CASE WHEN JCM_TYPE = 5 THEN JCM_AMOUNT ELSE 0 END) AS BUYINCOMMISSION                     ")
    _sb.AppendLine("           , SUM(CASE WHEN JCM_TYPE = 4 THEN JCM_AMOUNT ELSE 0 END) AS THEORETICALNETWINCOMMISSION         ")
    _sb.AppendLine("           , SUM(JCM_AMOUNT) AS TOTALCOMMISION                                                             ")
    _sb.AppendLine("      FROM   JUNKETS JU                                                                                    ")
    _sb.AppendLine(" LEFT JOIN   JUNKETS_FLYERS JF ON JF_JUNKET_ID = JU_ID                                                     ")
    _sb.AppendLine(" LEFT JOIN   JUNKETS_COMMISSIONS_MOVEMENTS JCM ON JCM_FLYER_ID = JF_ID                                     ")
    _sb.AppendLine(" LEFT JOIN   JUNKETS_REPRESENTATIVES JR ON JU_REPRESENTATIVE_ID = JR_ID                                    ")
    _sb.AppendLine(" LEFT JOIN   ACCOUNTS AC ON JCM_ACCOUNT_ID = AC_ACCOUNT_ID                                                 ")

    _sql_where = BuildWhere(DateFrom, DateTo, AccountDetails, ListRepresentatives, ListJunkets, ExcludeWithoutActivity)

    If Not ExcludeWithoutActivity Then
      _sql_where_exclude = BuildWhereExclude(AccountDetails, ListRepresentatives, ListJunkets)
    End If

    If Len(_sql_where) > 0 Then
      _sb.Append("     WHERE   ")
      _sb.Append(_sql_where)

      If _sql_where_exclude <> String.Empty Then
        _sb.Append("        OR   ")
        _sb.AppendLine(String.Format("({0})", _sql_where_exclude))
      End If

    End If

    _sb.AppendLine("  GROUP BY   JU_CODE, JU_NAME, [dbo].[Opening](0,JCM_CREATION), JR_CODE, JR_NAME, JU_DATE_FROM, JU_DATE_TO ")
    _sb.AppendLine("  ORDER BY   JU_CODE, GAMING_DAY                                                                           ")

    Return _sb.ToString()

  End Function ' WorkingDayQuery

  ''' <summary>
  ''' Get sql for representative
  ''' </summary>
  ''' <param name="DateFrom"></param>
  ''' <param name="DateTo"></param>
  ''' <param name="AccountDetails"></param>
  ''' <param name="ListRepresentatives"></param>
  ''' <param name="ListJunkets"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function RepresentativeQuery(ByVal DateFrom As uc_date_picker, ByVal DateTo As uc_date_picker, ByVal AccountDetails As uc_account_sel, _
                                      ByRef ListRepresentatives As String, ByRef ListJunkets As String, ByVal ExcludeWithoutActivity As Boolean) As String
    Dim _sb As StringBuilder
    Dim _sql_where As String = String.Empty
    Dim _sql_where_exclude As String = String.Empty

    _sb = New StringBuilder()

    _sb.AppendLine("    SELECT   JR_CODE                                                                               ")
    _sb.AppendLine("           , JR_NAME                                                                               ")
    _sb.AppendLine("           , SUM(CASE WHEN JCM_TYPE = 0 THEN JCM_AMOUNT ELSE 0 END) AS CUSTOMERCOMMISSION          ")
    _sb.AppendLine("           , SUM(CASE WHEN JCM_TYPE = 1 THEN JCM_AMOUNT ELSE 0 END) AS VISITCOMMISSION             ")
    _sb.AppendLine("           , SUM(CASE WHEN JCM_TYPE = 2 THEN JCM_AMOUNT ELSE 0 END) AS NEWCUSTOMERSCOMMISSION      ")
    _sb.AppendLine("           , SUM(CASE WHEN JCM_TYPE = 6 THEN JCM_AMOUNT ELSE 0 END) AS POINTSCOMMISSION            ")
    _sb.AppendLine("           , SUM(CASE WHEN JCM_TYPE = 3 THEN JCM_AMOUNT ELSE 0 END) AS COININCOMMISSION            ")
    _sb.AppendLine("           , SUM(CASE WHEN JCM_TYPE = 5 THEN JCM_AMOUNT ELSE 0 END) AS BUYINCOMMISSION             ")
    _sb.AppendLine("           , SUM(CASE WHEN JCM_TYPE = 4 THEN JCM_AMOUNT ELSE 0 END) AS THEORETICALNETWINCOMMISSION ")
    _sb.AppendLine("           , SUM(JCM_AMOUNT) AS TOTALCOMMISION                                                     ")
    _sb.AppendLine("      FROM   JUNKETS JU                                                                            ")
    _sb.AppendLine(" LEFT JOIN   JUNKETS_FLYERS JF ON JF_JUNKET_ID = JU_ID                                             ")
    _sb.AppendLine(" LEFT JOIN   JUNKETS_COMMISSIONS_MOVEMENTS JCM ON JCM_FLYER_ID = JF_ID                             ")
    _sb.AppendLine(" LEFT JOIN   JUNKETS_REPRESENTATIVES JR ON JU_REPRESENTATIVE_ID = JR_ID                            ")
    _sb.AppendLine(" LEFT JOIN   ACCOUNTS AC ON JCM_ACCOUNT_ID = AC_ACCOUNT_ID                                         ")

    _sql_where = BuildWhere(DateFrom, DateTo, AccountDetails, ListRepresentatives, ListJunkets, ExcludeWithoutActivity)

    If Not ExcludeWithoutActivity Then
      _sql_where_exclude = BuildWhereExclude(AccountDetails, ListRepresentatives, ListJunkets)
    End If

    If Len(_sql_where) > 0 Then
      _sb.Append("     WHERE   ")
      _sb.Append(_sql_where)

      If _sql_where_exclude <> String.Empty Then
        _sb.Append("        OR   ")
        _sb.AppendLine(String.Format("({0})", _sql_where_exclude))
      End If

    End If

    _sb.AppendLine("  GROUP BY   JR_CODE, JR_NAME                                                                      ")
    _sb.AppendLine("  ORDER BY   JR_CODE, JR_NAME                                                                      ")

    Return _sb.ToString()

  End Function ' RepresentativeQuery

  ''' <summary>
  ''' Get sql for customer
  ''' </summary>
  ''' <param name="DateFrom"></param>
  ''' <param name="DateTo"></param>
  ''' <param name="AccountDetails"></param>
  ''' <param name="ListRepresentatives"></param>
  ''' <param name="ListJunkets"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function CustomerQuery(ByVal DateFrom As uc_date_picker, ByVal DateTo As uc_date_picker, ByVal AccountDetails As uc_account_sel, _
                                ByRef ListRepresentatives As String, ByRef ListJunkets As String, ByVal ExcludeWithoutActivity As Boolean) As String
    Dim _sb As StringBuilder
    Dim _sql_where As String = String.Empty
    Dim _sql_where_exclude As String = String.Empty

    _sb = New StringBuilder()

    _sb.AppendLine("    SELECT   JU_CODE                                                                                      ")
    _sb.AppendLine("           , JU_NAME                                                                                      ")
    _sb.AppendLine("           , JU_DATE_FROM                                                                                 ")
    _sb.AppendLine("           , JU_DATE_TO                                                                                   ")
    _sb.AppendLine("           , JR_CODE                                                                                      ")
    _sb.AppendLine("           , JR_NAME                                                                                      ")
    _sb.AppendLine("           , JCM_ACCOUNT_ID                                                                               ")
    _sb.AppendLine("           , AC_HOLDER_NAME                                                                               ")
    _sb.AppendLine("           , SUM(CASE WHEN JCM_TYPE = 0 THEN JCM_AMOUNT ELSE 0 END) AS CUSTOMERCOMMISSION                 ")
    _sb.AppendLine("           , SUM(CASE WHEN JCM_TYPE = 1 THEN JCM_AMOUNT ELSE 0 END) AS VISITCOMMISSION                    ")
    _sb.AppendLine("           , SUM(CASE WHEN JCM_TYPE = 2 THEN JCM_AMOUNT ELSE 0 END) AS NEWCUSTOMERSCOMMISSION             ")
    _sb.AppendLine("           , SUM(CASE WHEN JCM_TYPE = 6 THEN JCM_AMOUNT ELSE 0 END) AS POINTSCOMMISSION                   ")
    _sb.AppendLine("           , SUM(CASE WHEN JCM_TYPE = 3 THEN JCM_AMOUNT ELSE 0 END) AS COININCOMMISSION                   ")
    _sb.AppendLine("           , SUM(CASE WHEN JCM_TYPE = 5 THEN JCM_AMOUNT ELSE 0 END) AS BUYINCOMMISSION                    ")
    _sb.AppendLine("           , SUM(CASE WHEN JCM_TYPE = 4 THEN JCM_AMOUNT ELSE 0 END) AS THEORETICALNETWINCOMMISSION        ")
    _sb.AppendLine("           , SUM(JCM_AMOUNT) AS TOTALCOMMISION                                                            ")
    _sb.AppendLine("      FROM   JUNKETS JU                                                                                   ")
    _sb.AppendLine(" LEFT JOIN   JUNKETS_FLYERS JF ON JF_JUNKET_ID = JU_ID                                                    ")
    _sb.AppendLine(" LEFT JOIN   JUNKETS_COMMISSIONS_MOVEMENTS JCM ON JCM_FLYER_ID = JF_ID                                    ")
    _sb.AppendLine(" LEFT JOIN   JUNKETS_REPRESENTATIVES JR ON JU_REPRESENTATIVE_ID = JR_ID                                   ")
    _sb.AppendLine(" LEFT JOIN   ACCOUNTS AC ON JCM_ACCOUNT_ID = AC_ACCOUNT_ID                                                ")

    _sql_where = BuildWhere(DateFrom, DateTo, AccountDetails, ListRepresentatives, ListJunkets, ExcludeWithoutActivity)

    If Not ExcludeWithoutActivity Then
      _sql_where_exclude = BuildWhereExclude(AccountDetails, ListRepresentatives, ListJunkets)
    End If

    If Len(_sql_where) > 0 Then
      _sb.Append("     WHERE   ")
      _sb.Append(_sql_where)

      If _sql_where_exclude <> String.Empty Then
        _sb.Append("        OR   ")
        _sb.AppendLine(String.Format("({0})", _sql_where_exclude))
      End If

    End If

    _sb.AppendLine("  GROUP BY   JU_CODE, JU_NAME, JCM_ACCOUNT_ID, AC_Holder_name, JR_CODE, JR_NAME, JU_DATE_FROM, JU_DATE_TO ")
    _sb.AppendLine("  ORDER BY   JU_CODE, JCM_ACCOUNT_ID                                                                      ")

    Return _sb.ToString()

  End Function ' CustomerQuery

#End Region ' Public Methods

#Region " Private Methods "

  ''' <summary>
  ''' Clone a object
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function Clone() As CLASS_JUNKETS

    Dim _junket As CLASS_JUNKETS
    Dim _comission As Commission
    Dim _flyer As CLASS_JUNKETS_FLYER.TYPE_FLYER

    _junket = New CLASS_JUNKETS

    _junket.Id = Me.m_junket.id
    _junket.Code = Me.m_junket.code
    _junket.Name = Me.m_junket.name
    _junket.Creation = Me.m_junket.creation
    _junket.DateFrom = Me.m_junket.date_from
    _junket.DateTo = Me.m_junket.date_to
    _junket.JunketRepresentative.id = Me.m_junket.junket_representative.id
    _junket.JunketRepresentative.code = Me.m_junket.junket_representative.code
    _junket.JunketRepresentative.name = Me.m_junket.junket_representative.name
    _junket.Comment = Me.m_junket.comment
    _junket.Update = Me.m_junket.update

    _junket.Commissions.Clear()

    For Each _comission_read As Commission In Me.m_junket.commissions

      _comission = New Commission

      _comission.Creation = _comission_read.Creation
      _comission.DateFrom = _comission_read.DateFrom
      _comission.DateTo = _comission_read.DateTo
      _comission.FixedValue = _comission_read.FixedValue
      _comission.Id = _comission_read.Id
      _comission.Type = _comission_read.Type
      _comission.Update = _comission_read.Update
      _comission.VariableValue = _comission_read.VariableValue

      _junket.Commissions.Add(_comission)

    Next

    For Each _flyer_read As CLASS_JUNKETS_FLYER.TYPE_FLYER In Me.m_junket.flyers

      _flyer = New CLASS_JUNKETS_FLYER.TYPE_FLYER(False)
      _flyer.id = _flyer_read.id
      _flyer.junket_id = _flyer_read.junket_id
      _flyer.code = _flyer_read.code
      _flyer.comment = _flyer_read.comment
      _junket.Flyers.Add(_flyer)

    Next

    Return _junket

  End Function ' Clone

  ''' <summary>
  ''' Update expiration date to all flyers linked to junket
  ''' </summary>
  ''' <param name="Trx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function UpdateExpirationDateFlyers(ByRef Trx As SqlTransaction) As Boolean

    Dim _sb As StringBuilder

    Try

      _sb = New StringBuilder
      _sb.AppendLine("   UPDATE   FLAGS                                                ")
      _sb.AppendLine("      SET   FL_EXPIRATION_DATE = @pExpirationDate                ")
      _sb.AppendLine("    WHERE   FL_RELATED_ID IN ( SELECT   JF_ID                    ")
      _sb.AppendLine("                                 FROM   JUNKETS_FLYERS           ")
      _sb.AppendLine("                                WHERE   JF_JUNKET_ID = @pID )    ")

      Using _cmd As New SqlCommand(_sb.ToString(), Trx.Connection, Trx)

        _cmd.Parameters.Add("@pID", SqlDbType.BigInt).Value = m_junket.id
        _cmd.Parameters.Add("@pExpirationDate", SqlDbType.DateTime).Value = m_junket.date_to

        _cmd.ExecuteNonQuery()

      End Using

      Return True

    Catch _ex As Exception
      Log.Error(_ex.Message())
    End Try

    Return False

  End Function ' UpdateExpirationDateFlyers

  ''' <summary>
  ''' Convert commission list to datatable
  ''' </summary>
  ''' <param name="RowState"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CommissionsToDatatble(RowState As DataRowState) As DataTable

    Dim _dt_commissions As DataTable

    Try

      _dt_commissions = WSI.Common.Misc.ListToDataTable(Me.Commissions)

      _dt_commissions.AcceptChanges()

      If RowState = DataRowState.Modified Then
        For Each _row As DataRow In _dt_commissions.Rows
          _row.SetModified()
        Next
      Else
        For Each _row As DataRow In _dt_commissions.Rows
          _row.SetAdded()
        Next
      End If

      Return _dt_commissions

    Catch _ex As Exception
      Log.Error(_ex.Message())
    End Try

    Return Nothing

  End Function ' CommissionsToDatatble

  Private Function BuildWhere(DateFrom As uc_date_picker, DateTo As uc_date_picker, Account As uc_account_sel, _
                              ListRepresentatives As String, ListJunkets As String, ByVal ExcludeWithoutActivity As Boolean) As String

    Dim _where As StringBuilder
    Dim _clause_added As Boolean
    Dim _account_sql As String

    _where = New StringBuilder

    ' Dates
    If DateFrom.Checked Then
      _where.AppendLine("JCM_CREATION >= " & GUI_FormatDateDB(DateFrom.Value))
      _clause_added = True
    End If

    If DateTo.Checked Then
      If _clause_added Then
        _where.Append("       AND   ")
      End If
      _where.AppendLine("JCM_CREATION <= " & GUI_FormatDateDB(DateTo.Value))
      _clause_added = True
    End If

    ' Account
    _account_sql = Account.GetFilterSQL
    If Len(_account_sql) > 0 Then
      If _clause_added Then
        _where.Append("       AND   ")
      End If
      _where.AppendLine(_account_sql)
      _clause_added = True
    End If

    ' Representatives
    If Not String.IsNullOrEmpty(ListRepresentatives) Then
      If _clause_added Then
        _where.Append("       AND   ")
      End If
      _where.AppendLine(String.Format("JU_REPRESENTATIVE_ID IN ({0})      ", ListRepresentatives))
      _clause_added = True
    End If

    ' Junkets
    If Not String.IsNullOrEmpty(ListJunkets) Then
      If _clause_added Then
        _where.Append("       AND   ")
      End If
      _where.AppendLine(String.Format("JU_ID IN ({0})      ", ListJunkets))
      _clause_added = True
    End If

    'ExcludeWithoutActivity
    If ExcludeWithoutActivity Then
      If _clause_added Then
        _where.Append("       AND   ")
      End If
      _where.AppendLine("[dbo].[Opening](0,JCM_CREATION) IS NOT NULL")
      _clause_added = True
    End If

    Return _where.ToString

  End Function

  Private Function BuildWhereExclude(Account As uc_account_sel, ListRepresentatives As String, ListJunkets As String) As String

    Dim _where As StringBuilder
    Dim _clause_added As Boolean
    Dim _account_sql As String

    _where = New StringBuilder

    ' Account
    _account_sql = Account.GetFilterSQL
    If Len(_account_sql) > 0 Then
      If _clause_added Then
        _where.Append("       AND   ")
      End If
      _where.AppendLine(_account_sql)
      _clause_added = True
    End If

    ' Representatives
    If Not String.IsNullOrEmpty(ListRepresentatives) Then
      If _clause_added Then
        _where.Append("       AND   ")
      End If
      _where.AppendLine(String.Format("JU_REPRESENTATIVE_ID IN ({0})      ", ListRepresentatives))
      _clause_added = True
    End If

    ' Junkets
    If Not String.IsNullOrEmpty(ListJunkets) Then
      If _clause_added Then
        _where.Append("       AND   ")
      End If
      _where.AppendLine(String.Format("JU_ID IN ({0})      ", ListJunkets))
      _clause_added = True
    End If

    'ExcludeWithoutActivity
    If _clause_added Then
      _where.Append("       AND   ")
    End If
    _where.Append("[dbo].[Opening](0,JCM_CREATION) IS NULL")
    _clause_added = True

    Return _where.ToString

  End Function

#End Region ' Private Methods

End Class
