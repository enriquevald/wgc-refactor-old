'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_gaming_table_type.vb
'
' DESCRIPTION : gaming table type class for user edition
'              
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 17-DEC-2013  JBP    Initial version
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common


Public Class CLASS_GAMING_TABLE_TYPE
  Inherits CLASS_BASE

#Region " Constants "

#End Region

#Region " Enums "

  Public Enum AFIP_TYPES
    Ruleta = 1
    Naipes = 2
    Dados = 3
    Torneo = 4
    Otros = 99
  End Enum

#End Region ' Enums 

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_GAMING_TABLE_TYPE
    Public gaming_table_type_id As Integer
    Public name As String
    Public enabled As Boolean = True
    Public AFIPType As Integer
  End Class

#End Region

#Region " Members "

  Protected m_gaming_table_type As New TYPE_GAMING_TABLE_TYPE


#End Region

#Region " Properties "

  Public Property GamingTableTypeID() As Integer
    Get
      Return m_gaming_table_type.gaming_table_type_id
    End Get

    Set(ByVal Value As Integer)
      m_gaming_table_type.gaming_table_type_id = Value
    End Set
  End Property

  Public Property Name() As String
    Get
      Return m_gaming_table_type.name
    End Get

    Set(ByVal Value As String)
      m_gaming_table_type.name = Value
    End Set
  End Property

  Public Property Enabled() As Boolean
    Get
      Return m_gaming_table_type.enabled
    End Get

    Set(ByVal Value As Boolean)
      m_gaming_table_type.enabled = Value
    End Set
  End Property

  Public Property AFIPType() As Integer
    Get
      Return m_gaming_table_type.AFIPType
    End Get

    Set(ByVal Value As Integer)
      m_gaming_table_type.AFIPType = Value
    End Set
  End Property


#End Region ' Structures

#Region " Public Functions "

#End Region ' Public Functions

#Region " Overrides functions "

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads from the database into the given object
  '
  ' PARAMS :
  '     - INPUT :
  '         - ObjectId: An object, it must be 'casted' to the desired KEY.
  '         - SqlCtx: SqlTransaction.
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '
  ' NOTES :

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As ENUM_STATUS

    Me.GamingTableTypeID = ObjectId
    _rc = ENUM_STATUS.STATUS_ERROR

    Using _db_trx As New DB_TRX()
      ' Read terminal data
      _rc = ReadGamingTableType(_db_trx.SqlTransaction)
    End Using

    Return _rc

  End Function ' DB_Read

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '         - SqlCtx: SqlTransaction.
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '
  ' NOTES :

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As ENUM_STATUS

    _rc = ENUM_STATUS.STATUS_ERROR

    Using _db_trx As New DB_TRX()
      ' Update terminal data
      _rc = UpdateGamingTableType(_db_trx.SqlTransaction)
    End Using

    Return _rc

  End Function ' DB_Update

  '----------------------------------------------------------------------------
  ' PURPOSE : Inserts the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '         - SqlCtx: SqlTransaction.
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '
  ' NOTES :
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As ENUM_STATUS

    _rc = ENUM_STATUS.STATUS_ERROR

    Using _db_trx As New DB_TRX()
      ' Insert terminal data
      _rc = InsertGamingTableType(_db_trx.SqlTransaction)
    End Using

    Return _rc

  End Function ' DB_Insert

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes the given object from the database.
  '
  ' PARAMS :
  '     - INPUT :
  '         - SqlCtx: SqlTransaction.
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_DEPENDENCIES
  '     - STATUS_ERROR
  '
  ' NOTES :

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As ENUM_STATUS

    _rc = ENUM_STATUS.STATUS_ERROR

    Using _db_trx As New DB_TRX()
      ' Delete terminal data
      _rc = DeleteGamingTableType(_db_trx.SqlTransaction)
    End Using

    Return _rc

  End Function ' DB_Delete

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns a duplicate of the given object.
  '
  ' PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created object
  '
  ' NOTES :

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim temp_object As CLASS_GAMING_TABLE_TYPE

    temp_object = New CLASS_GAMING_TABLE_TYPE
    temp_object.m_gaming_table_type.gaming_table_type_id = Me.m_gaming_table_type.gaming_table_type_id
    temp_object.m_gaming_table_type.name = Me.m_gaming_table_type.name
    temp_object.m_gaming_table_type.enabled = Me.m_gaming_table_type.enabled
    temp_object.m_gaming_table_type.AFIPType = Me.m_gaming_table_type.AFIPType

    Return temp_object

  End Function ' Duplicate

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns the related auditor data for the current object.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created audit object
  '
  ' NOTES :

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _is_enabled As String

    If Me.Enabled Then
      _is_enabled = GLB_NLS_GUI_CONFIGURATION.GetString(264)
    Else
      _is_enabled = GLB_NLS_GUI_CONFIGURATION.GetString(265)
    End If

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)
    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(3420), Me.Name)               ' 3420 "Tipo de Mesa"

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(3419), Me.Name)              ' 3419 "Nombre"
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(3422), _is_enabled)          ' 3421 "Habilitada"
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7332), Me.AFIPType)          ' AFIP Type"


    Return _auditor_data

  End Function ' AuditorData

#End Region

#Region " Private Functions "

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads a Gaming Table type object from DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - SqlTrans
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '
  ' NOTES :

  Private Function ReadGamingTableType(ByVal SqlTrans As SqlTransaction) As Integer

    Dim _sb As StringBuilder
    Dim _table As DataTable

    Try

      _sb = New StringBuilder()
      _sb.AppendLine("SELECT   GTT_GAMING_TABLE_TYPE_ID ")
      _sb.AppendLine("	     , GTT_NAME					        ")
      _sb.AppendLine("	     , GTT_ENABLED				      ")
      If (WSI.Common.AFIP_Common.AFIP_Common.IsEnabled) Then
        _sb.AppendLine("     , GTT_REPORT_TYPE_ID")
      End If
      _sb.AppendLine("  FROM   GAMING_TABLES_TYPES      ")

      _sb.AppendLine(" WHERE GTT_GAMING_TABLE_TYPE_ID = " & Me.GamingTableTypeID.ToString())

      _table = GUI_GetTableUsingSQL(_sb.ToString(), 5000)

      If IsNothing(_table) Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

      If _table.Rows.Count() = 0 Then
        Return ENUM_STATUS.STATUS_NOT_FOUND
      End If

      If _table.Rows.Count() <> 1 Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

      Me.Name = IIf(IsDBNull(_table.Rows(0).Item("GTT_NAME")), "", _table.Rows(0).Item("GTT_NAME"))
      Me.Enabled = IIf(IsDBNull(_table.Rows(0).Item("GTT_ENABLED")), False, _table.Rows(0).Item("GTT_ENABLED"))
      If (WSI.Common.AFIP_Common.AFIP_Common.IsEnabled) Then
        Me.AFIPType = IIf(IsDBNull(_table.Rows(0).Item("GTT_REPORT_TYPE_ID")), 0, _table.Rows(0).Item("GTT_REPORT_TYPE_ID"))
      End If

      Return ENUM_STATUS.STATUS_OK

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return ENUM_STATUS.STATUS_ERROR

  End Function ' ReadGamingTableType

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes a Gaming Table object from DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - SqlTrans
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_DEPENDENCIES
  '     - STATUS_ERROR
  '
  ' NOTES :

  Private Function DeleteGamingTableType(ByVal SqlTrans As SqlTransaction) As Integer

    Dim _sb As StringBuilder
    Dim _rc As ENUM_STATUS

    Try

      _rc = IsAssigned(SqlTrans)

      If _rc <> ENUM_STATUS.STATUS_OK Then
        Return _rc
      End If

      _sb = New StringBuilder()
      _sb.AppendLine("DELETE FROM GAMING_TABLES_TYPES ")
      _sb.AppendLine(" WHERE GTT_GAMING_TABLE_TYPE_ID = " & Me.GamingTableTypeID.ToString())

      If Not GUI_SQLExecuteNonQuery(_sb.ToString(), SqlTrans) Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

      SqlTrans.Commit()
      Return ENUM_STATUS.STATUS_OK

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return ENUM_STATUS.STATUS_ERROR

  End Function ' DeleteGamingTable

  '----------------------------------------------------------------------------
  ' PURPOSE : Adds a Gaming Table object into DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - SqlTrans
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_DUPLICATE_KEY
  '     - STATUS_ERROR
  '
  ' NOTES :

  Private Function InsertGamingTableType(ByVal SqlTrans As SqlTransaction) As Integer

    Dim _sb As StringBuilder
    Dim _param_id As SqlParameter
    Dim _row_affected As Integer
    Dim _rc As ENUM_STATUS

    Try

      _rc = ExistName(SqlTrans)

      If _rc <> ENUM_STATUS.STATUS_OK Then
        Return _rc
      End If

      _sb = New StringBuilder()
      _sb.AppendLine("INSERT INTO GAMING_TABLES_TYPES		      ")
      _sb.AppendLine("           (  GTT_NAME					        ")
      If (WSI.Common.AFIP_Common.AFIP_Common.IsEnabled) Then
        _sb.AppendLine("          , GTT_REPORT_TYPE_ID")
      End If
      _sb.AppendLine("           	, GTT_ENABLED	)		          ")
      _sb.AppendLine("     VALUES									            ")
      _sb.AppendLine("           (  @pName					          ")
      If (WSI.Common.AFIP_Common.AFIP_Common.IsEnabled) Then
        _sb.AppendLine("          , @pAFIPType	")
      End If
      _sb.AppendLine("           	, @pEnabled	  )		          ")
      _sb.AppendLine("        SET @pId    = SCOPE_IDENTITY(); ")

      Using _cmd As New SqlCommand(_sb.ToString(), SqlTrans.Connection, SqlTrans)

        _cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = Me.Name
        _cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).Value = Me.Enabled
        If (WSI.Common.AFIP_Common.AFIP_Common.IsEnabled) Then
          _cmd.Parameters.Add("@pAFIPType", SqlDbType.Int).Value = Me.AFIPType
        End If

        _param_id = _cmd.Parameters.Add("@pId", SqlDbType.Int)
        _param_id.Direction = ParameterDirection.Output

        _row_affected = _cmd.ExecuteNonQuery()

        If _row_affected <> 1 Or IsDBNull(_param_id.Value) Then
          Return ENUM_STATUS.STATUS_ERROR
        End If

        Me.GamingTableTypeID = _param_id.Value

        SqlTrans.Commit()
        Return ENUM_STATUS.STATUS_OK

      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return ENUM_STATUS.STATUS_ERROR

  End Function ' InsertGamingTable

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given Gaming Table object into the DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - SqlTrans
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_DEPENDENCIES
  '     - STATUS_DUPLICATE_KEY
  '     - STATUS_NOT_FOUND
  '     - STATUS_ERROR
  '
  ' NOTES :

  Private Function UpdateGamingTableType(ByVal SqlTrans As SqlTransaction) As Integer

    Dim _sb As StringBuilder
    Dim _row_affected As Integer
    Dim _rc As ENUM_STATUS

    Try

      If Not Me.Enabled Then
        _rc = IsAssigned(SqlTrans)

        If _rc <> ENUM_STATUS.STATUS_OK Then
          Return _rc
        End If
      End If

      _rc = ExistName(SqlTrans)

      If _rc <> ENUM_STATUS.STATUS_OK Then
        Return _rc
      End If

      _sb = New StringBuilder()

      ' UPDATE GAMING_TABLES
      _sb.AppendLine("  UPDATE   GAMING_TABLES_TYPES      		          ")
      _sb.AppendLine("     SET   GTT_NAME                 = @pName      ")
      _sb.AppendLine("         , GTT_ENABLED              = @pEnabled		")
      If (WSI.Common.AFIP_Common.AFIP_Common.IsEnabled) Then
        _sb.AppendLine("       , GTT_REPORT_TYPE_ID       = @pAFIPType	")
      End If

      _sb.AppendLine("   WHERE   GTT_GAMING_TABLE_TYPE_ID = @pId				")

      Using _cmd As New SqlCommand(_sb.ToString(), SqlTrans.Connection, SqlTrans)
        _cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = Me.Name
        _cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).Value = Me.Enabled
        _cmd.Parameters.Add("@pId", SqlDbType.Int).Value = Me.GamingTableTypeID
        If (WSI.Common.AFIP_Common.AFIP_Common.IsEnabled) Then
          _cmd.Parameters.Add("@pAFIPType", SqlDbType.Int).Value = Me.AFIPType
        End If

        _row_affected = _cmd.ExecuteNonQuery()

        If _row_affected = 0 Then
          Return ENUM_STATUS.STATUS_NOT_FOUND
        ElseIf _row_affected = 1 Then
          SqlTrans.Commit()
          Return ENUM_STATUS.STATUS_OK
        End If

      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return ENUM_STATUS.STATUS_ERROR

  End Function ' UpdateGamingTable


  '----------------------------------------------------------------------------
  ' PURPOSE : Check if exists Game table name or cashier terminal name.
  '
  ' PARAMS :
  '     - INPUT :
  '         - SqlTrans
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_DUPLICATE_KEY
  '     - STATUS_ERROR
  '
  ' NOTES :

  Private Function ExistName(ByVal SqlTrans As SqlTransaction) As ENUM_STATUS
    Dim _sb As StringBuilder
    Dim _data As Object

    Try

      ' Check Gaming table names
      _sb = New StringBuilder()

      _sb.AppendLine("  SELECT  GTT_NAME                          ")
      _sb.AppendLine("    FROM  GAMING_TABLES_TYPES               ")
      _sb.AppendLine("   WHERE  GTT_NAME = @pName	                ")
      _sb.AppendLine("     AND  GTT_GAMING_TABLE_TYPE_ID != @pId  ")

      Using _cmd As New SqlCommand(_sb.ToString(), SqlTrans.Connection, SqlTrans)
        _cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = Me.Name
        _cmd.Parameters.Add("@pId", SqlDbType.Int).Value = Me.GamingTableTypeID

        _data = _cmd.ExecuteScalar()

        If Not _data Is Nothing Then
          Return ENUM_STATUS.STATUS_DUPLICATE_KEY
        End If

        Return ENUM_STATUS.STATUS_OK

      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return ENUM_STATUS.STATUS_ERROR

  End Function ' ExistName

  '----------------------------------------------------------------------------
  ' PURPOSE : Check if exists Game table name or cashier terminal name.
  '
  ' PARAMS :
  '     - INPUT :
  '         - SqlTrans
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_DEPENDENCIES
  '     - STATUS_ERROR
  '
  ' NOTES :

  Private Function IsAssigned(ByVal SqlTrans As SqlTransaction) As ENUM_STATUS

    Dim _sb As StringBuilder
    Dim _data As Object

    Try

      ' Check Gaming table type assigned
      _sb = New StringBuilder()

      _sb.AppendLine("  SELECT   GT_NAME			      ")
      _sb.AppendLine("    FROM   GAMING_TABLES      ")
      _sb.AppendLine("   WHERE   GT_TYPE_ID = @pId  ")

      ' Check Gaming table names

      Using _cmd As New SqlCommand(_sb.ToString(), SqlTrans.Connection, SqlTrans)
        _cmd.Parameters.Add("@pId", SqlDbType.Int).Value = Me.GamingTableTypeID

        _data = _cmd.ExecuteScalar()

        If Not _data Is Nothing Then
          Return ENUM_STATUS.STATUS_DEPENDENCIES
        End If

        Return ENUM_STATUS.STATUS_OK

      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return ENUM_STATUS.STATUS_ERROR

  End Function ' IsAssigned

#End Region ' Private Functions


End Class
