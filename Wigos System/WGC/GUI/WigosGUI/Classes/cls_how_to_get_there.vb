﻿'-------------------------------------------------------------------
' Copyright © 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_catalog.vb
' DESCRIPTION:   Draw class for user edition
' AUTHOR:        Sergio Daniel Soria
' CREATION DATE: 31-AUG-2015
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 31-AUG-2015  SDS    Initial version.
' 11-DEC-2015  GMV    Product Backlog Item 6857:Review Iteración 15: Catalogos
' 21-DEC-2015  DLL    Fixed Bug TFS-7814: Catalogs always create disable
' -----------  ------ -----------------------------------------------


Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common
Imports System.Text
Imports System.Type
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports Newtonsoft.Json.Linq
Imports Newtonsoft.Json

Public Class CLS_HOW_TO_GET_THERE
  Inherits CLASS_BASE


#Region " Enum "

  Private Enum ROWACTION
    NORMAL = 0
    MODIFIED = 1
    DELETED = 2
  End Enum

#End Region 'Enum

#Region " Constants "

  Public Const SQL_COLUMN_CAT_ID As Integer = 0
  Public Const SQL_COLUMN_TYPE As Integer = 1
  Public Const SQL_COLUMN_NAME As Integer = 2
  Public Const SQL_COLUMN_DESCRIPTION As Integer = 3
  Public Const SQL_COLUMN_ENABLED As Integer = 4

  Public Const COLUMN_HOW_TO_ITEM_LANG As Integer = 0
  Public Const COLUMN_HOW_TO_ITEM_ID As Integer = 1
  Public Const COLUMN_HOW_TO_ITEM_TITLE As Integer = 2
  Public Const COLUMN_HOW_TO_ITEM_VALUE As Integer = 3
  Public Const COLUMN_HOW_TO_ITEM_TYPE As Integer = 4

  Private Const HOW_TO_TYPE_TEL As Integer = 1
  Private Const HOW_TO_TYPE_EMAIL As Integer = 2

  Private Const SPANISH_LANG As Integer = 1

  Private Const AUDIT_STATE_COLUMN_NAME As String = "ACTION"
#End Region ' Constants 

#Region " Structures "

  Public Class HOW_TO
    Public how_to_id As Int32
    Public title As String
    Public address As String
    Public how_to_items As DataTable

    Sub New()
      how_to_id = 0
      title = String.Empty
      address = String.Empty
      how_to_items = New DataTable()
    End Sub
  End Class

#End Region ' Structures

#Region " Members "
  Protected m_how_to As New HOW_TO
#End Region ' Members

#Region "Properties"
  Public Property HowToGetThereId() As Int32
    Get
      Return m_how_to.how_to_id
    End Get
    Set(ByVal value As Int32)
      m_how_to.how_to_id = value
    End Set
  End Property

  Public Property Title() As String
    Get
      Return m_how_to.title
    End Get
    Set(ByVal value As String)
      m_how_to.title = value
    End Set
  End Property

  Public Property Address() As String
    Get
      Return m_how_to.address
    End Get
    Set(ByVal value As String)
      m_how_to.address = value
    End Set
  End Property

  Public Property HowToGetThereItems() As DataTable
    Get
      Return m_how_to.how_to_items
    End Get
    Set(ByVal value As DataTable)
      m_how_to.how_to_items = value
    End Set
  End Property

#End Region ' Properties

#Region "Overrides"

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA
    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _dt As DataTable
    Dim _str_sort As String
    Dim _dv As DataView
    Dim _action_value As ROWACTION
    Dim _yes_text As String
    Dim _no_text As String
    Dim _str_name As String
    Dim _str_description As String


    _yes_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6692)
    _no_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6693)
    _str_sort = String.Empty
    _action_value = ROWACTION.NORMAL
    _dt = New DataTable()
    _str_name = String.Empty
    _str_description = String.Empty

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)

    Call _auditor_data.SetName(GLB_NLS_GUI_AUDITOR.Id(484), " - " & Me.m_how_to.title)
    Call _auditor_data.SetField(0, m_how_to.title, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6679)) 'titulo
    Call _auditor_data.SetField(0, m_how_to.address, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6682)) 'direccion

    If Not Me.HowToGetThereItems Is Nothing And Me.HowToGetThereItems.Rows.Count > 0 Then

      _dt = Me.HowToGetThereItems.Copy
      _dt.Columns.Add(AUDIT_STATE_COLUMN_NAME, GetType(System.Int16))

      ' ordenamos por ID
      _str_sort = "howToGetThereItemId DESC"

      ' marcamos cada row segun su estado
      For Each _dr As DataRow In _dt.Rows
        Select Case _dr.RowState
          Case DataRowState.Added, DataRowState.Modified
            _dr(AUDIT_STATE_COLUMN_NAME) = ROWACTION.MODIFIED
          Case DataRowState.Deleted
            _dr.RejectChanges()
            _dr(AUDIT_STATE_COLUMN_NAME) = ROWACTION.DELETED
          Case Else
            _dr(AUDIT_STATE_COLUMN_NAME) = ROWACTION.NORMAL
        End Select
      Next

      ' ordenamos el nuevo DT
      _dv = _dt.DefaultView
      _dv.Sort = _str_sort
      _dt = _dv.ToTable()

      For Each _dr As DataRow In _dt.Rows

        _action_value = _dr(AUDIT_STATE_COLUMN_NAME)

        _str_name = _dr(COLUMN_HOW_TO_ITEM_TITLE)

        Select Case _action_value
          Case ROWACTION.MODIFIED, ROWACTION.NORMAL

            If IsDBNull(_dr(COLUMN_HOW_TO_ITEM_TITLE)) Then
              _auditor_data.SetField(0, AUDIT_NONE_STRING, _str_name & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6679))
            Else
              _auditor_data.SetField(0, _dr(COLUMN_HOW_TO_ITEM_TITLE), _str_name & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6679))
            End If

            If IsDBNull(_dr(COLUMN_HOW_TO_ITEM_VALUE)) Then
              _auditor_data.SetField(0, AUDIT_NONE_STRING, _str_name & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6682))
            Else
              _auditor_data.SetField(0, _dr(COLUMN_HOW_TO_ITEM_VALUE), _str_name & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6682))
            End If

            If IsDBNull(_dr(COLUMN_HOW_TO_ITEM_TYPE)) Then
              _auditor_data.SetField(0, AUDIT_NONE_STRING, _str_name & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6682))
            Else
              _auditor_data.SetField(0, _dr(COLUMN_HOW_TO_ITEM_TYPE), _str_name & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6682))
            End If

          Case Else
        End Select
      Next
    End If

    Return _auditor_data

  End Function ' AuditorData

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes the given object from the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DEPENDENCIES
  '
  ' NOTES :
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR

  End Function ' DB_Delete

  '----------------------------------------------------------------------------
  ' PURPOSE : Inserts the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Return InsertHowToGetThere(SqlCtx)

  End Function ' DB_Insert

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads from the database into the given object
  '
  ' PARAMS :
  '     - INPUT :
  '         - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '
  ' NOTES :
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Return ReadHowToGetThere(ObjectId)

  End Function ' DB_Read

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Return UpdateHowToGetThere(SqlCtx)

  End Function ' DB_Update

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns a duplicate of the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _target As CLS_HOW_TO_GET_THERE

    _target = New CLS_HOW_TO_GET_THERE

    _target.Title = Me.Title
    _target.m_how_to.how_to_items = Me.m_how_to.how_to_items.Copy
    _target.Address = Me.Address
    Return _target

  End Function ' Duplicate

#End Region ' Overrides

#Region " Private Functions "

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads an root object  from the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES:
  Private Function ReadHowToGetThere(jObj As JObject) As Integer

    Try
      If IsNothing(jObj) Then
        Dim tbl As New DataTable

        tbl.Columns.Add("languageid", GetType(Integer))
        tbl.Columns.Add("howToGetThereItemId", GetType(Integer))
        tbl.Columns.Add("title", GetType(String))
        tbl.Columns.Add("value", GetType(String))
        tbl.Columns.Add("type", GetType(Integer))

        Me.Title = ""
        Me.Address = ""
        Me.HowToGetThereId = 0
        Me.HowToGetThereItems = tbl
      Else
        Me.Title = jObj.Item("title")
        Me.Address = jObj.Item("address")
        Me.HowToGetThereId = jObj.Item("howToGetThereId")

        Call ReadHowToGetThereItems(jObj.Item("items"))
      End If
      Return ENUM_STATUS.STATUS_OK

    Catch ex As Exception
      Return ENUM_STATUS.STATUS_ERROR
    End Try
  End Function ' ReadCatalogs

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads an child object  from the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  '
  ' NOTES:
  Public Sub ReadHowToGetThereItems(lItems As JArray)

    m_how_to.how_to_items = JsonConvert.DeserializeObject(Of DataTable)(lItems.ToString())

  End Sub

  '----------------------------------------------------------------------------
  ' PURPOSE: Insert the given object into the database.
  '
  ' PARAMS:
  '   - INPUT:  None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES:
  Private Function InsertHowToGetThere(ByVal Context As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _sql_transaction As System.Data.SqlClient.SqlTransaction
    Dim _row_affected As Integer
    Dim _do_commit As Boolean
    Dim _rc As Integer
    Dim _url As String
    Dim _api As CLS_API
    Dim response As String

    _sql_transaction = Nothing
    _row_affected = 0
    _do_commit = False
    _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

    Try

      If Not GUI_BeginSQLTransaction(_sql_transaction) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If


      _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/howtogetthere"
      _api = New CLS_API(_url)

      response = _api.postData(Encoding.UTF8.GetBytes(Me.ToJSON()))

      _row_affected = 1

      If _row_affected <> 1 Then
        _do_commit = False
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        Return _rc
      Else
        _do_commit = True
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK
      End If

    Catch ex As Exception
      _do_commit = False
      _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

    Finally

      GUI_EndSQLTransaction(_sql_transaction, _do_commit)

    End Try

    Return _rc

  End Function ' Inserthowtogetthere


  '----------------------------------------------------------------------------
  ' PURPOSE: Update the given object into the database.
  '
  ' PARAMS:
  '   - INPUT:  None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES:
  Private Function UpdateHowToGetThere(ByVal Context As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _sql_transaction As System.Data.SqlClient.SqlTransaction
    Dim _row_affected As Integer
    Dim _do_commit As Boolean
    Dim _rc As Integer
    Dim _url As String
    Dim _api As CLS_API
    Dim response As String

    _sql_transaction = Nothing
    _row_affected = 0
    _do_commit = False

    Try

      If Not GUI_BeginSQLTransaction(_sql_transaction) Then
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

        Return _rc
      End If

      _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/howtogetthere"
      _api = New CLS_API(_url)

      response = _api.putData(Encoding.UTF8.GetBytes(Me.ToJSON()))

      _row_affected = 1

      If _row_affected <> 1 Then
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        Return _rc
      Else
        _do_commit = True
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK
      End If

    Catch ex As Exception
      _do_commit = False

    Finally

      GUI_EndSQLTransaction(_sql_transaction, _do_commit)

    End Try

    Return _rc

  End Function ' UpdateHowToGetThere

  '----------------------------------------------------------------------------
  ' PURPOSE : Return the properties of the class in JSON format
  '
  ' PARAMS :
  '     - INPUT :
  '         None
  '
  '     - OUTPUT :
  '         None
  '
  ' RETURNS :
  '     - JSON string
  '
  ' NOTES :

  Private Function ToJSON() As String
    Dim _howToGetThereSchemaId As New Integer
    Dim _jObj As New JObject
    _jObj.Add("languageid", SPANISH_LANG)
    _jObj.Add("howToGetThereId", HowToGetThereId)
    _jObj.Add("title", Title)
    _jObj.Add("address", Address)
    _howToGetThereSchemaId = GetHowToGetThereId()
    _jObj.Add("SectionSchemaId", _howToGetThereSchemaId)
    _jObj.Add("items", ItemsToJSON())
    Return _jObj.ToString()
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Return the properties of the class in JSON format
  '
  ' PARAMS :
  '     - INPUT :
  '         None
  '
  '     - OUTPUT :
  '         None
  '
  ' RETURNS :
  '     - JSON string
  '
  ' NOTES :

  Private Function ItemsToJSON() As JArray
    Dim _jArr As New JArray
    Dim _jobj As JObject
    For Each item As DataRow In Me.HowToGetThereItems.Rows
      _jobj = New JObject
      _jobj.Add("languageId", SPANISH_LANG)
      _jobj.Add("howToGetThereItemId", Convert.ToInt32(item("howToGetThereItemId")))
      _jobj.Add("title", item("title").ToString)
      _jobj.Add("value", item("value").ToString)
      _jobj.Add("type", Convert.ToInt16(item("type")))
      _jArr.Add(_jobj)
    Next
    Return _jArr
  End Function

  Private Function GetHowToGetThereId() As Integer

    Dim _datatable As DataTable
    Dim _rc As Integer
    Dim _url As String
    Dim _api As CLS_API


    Try
      _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/sectionsschema/HowToGetThere"
      _api = New CLS_API(_url)

      _datatable = JsonConvert.DeserializeObject(Of DataTable)(_api.responseToArray(_api.sanitizeData(_api.getData())))

      If Not IsNothing(_datatable) AndAlso _datatable.Rows.Count = 1 Then

        _rc = _datatable.Rows(0).Item("sectionSchemaId")

      End If

    Catch ex As Exception

    End Try

    Return _rc

  End Function
#End Region ' Private Functions

End Class
