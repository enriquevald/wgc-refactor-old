﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_intellia_filters.vb
'
' DESCRIPTION : intellia filters class
'              
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 23-JUN-2017  PDM    Initial version
' 22-AUG-2017  EOR    Bug 29431: WIGOS-4284 GUI - 'Pérdidas' filter shouldn't be displayed
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common

Public Class CLASS_INTELLIA_FILTERS
  Inherits CLASS_BASE

#Region "Constructor"

  Public Sub New()
    m_list = ReadFilters()
    ReadRanges(m_list)
  End Sub

#End Region

#Region "Members"
  Protected m_list As New List(Of INTELLIA_FILTERS)
#End Region

#Region "Properties"

  Public Property Filters() As List(Of INTELLIA_FILTERS)
    Get
      Return m_list
    End Get
    Set(ByVal value As List(Of INTELLIA_FILTERS))
      m_list = value
    End Set
  End Property

#End Region

#Region "Structure"

  Public Class INTELLIA_FILTERS

    Private _id As Integer
    Private _title As String
    Private _visible As Boolean
    Private _editable As Boolean
    Private _hasUnknown As Boolean
    Private _nls As Integer
    Private _type As Integer
    Private _ranges As List(Of INTELLIA_FILTERS_RANGES)

    Public Property Id() As Integer
      Get
        Return _id
      End Get
      Set(ByVal value As Integer)
        _id = value
      End Set
    End Property

    Public Property Title() As String
      Get
        Return _title
      End Get
      Set(ByVal value As String)
        _title = value
      End Set
    End Property

    Public Property Visible() As Boolean
      Get
        Return _visible
      End Get
      Set(ByVal value As Boolean)
        _visible = value
      End Set
    End Property

    Public Property Editable() As Boolean
      Get
        Return _editable
      End Get
      Set(ByVal value As Boolean)
        _editable = value
      End Set
    End Property

    Public Property HasUnknown() As Boolean
      Get
        Return _hasUnknown
      End Get
      Set(ByVal value As Boolean)
        _hasUnknown = value
      End Set
    End Property

    Public Property Nls() As Integer
      Get
        Return _nls
      End Get
      Set(ByVal value As Integer)
        _nls = value
      End Set
    End Property

    Public Property Type() As Integer
      Get
        Return _type
      End Get
      Set(ByVal value As Integer)
        _type = value
      End Set
    End Property

    Public Property Ranges() As List(Of INTELLIA_FILTERS_RANGES)
      Get
        If _ranges Is Nothing Then
          _ranges = New List(Of INTELLIA_FILTERS_RANGES)
        End If
        Return _ranges
      End Get
      Set(ByVal value As List(Of INTELLIA_FILTERS_RANGES))
        _ranges = value
      End Set
    End Property

  End Class

  Public Class INTELLIA_FILTERS_RANGES
    Private _id As Integer
    Private _title As String
    Private _operator As String
    Private _field1 As Decimal
    Private _field2 As Decimal
    Private _color As String
    Private _operatorValue As Integer
    Private _id_temp As Integer
    Private _deleted As Boolean

    Public Property Id() As Integer
      Get
        Return _id
      End Get
      Set(ByVal value As Integer)
        _id = value
      End Set
    End Property

    Public Property IdTemp() As Integer
      Get
        Return _id_temp
      End Get
      Set(ByVal value As Integer)
        _id_temp = value
      End Set
    End Property

    Public Property Title() As String
      Get
        Return _title
      End Get
      Set(ByVal value As String)
        _title = value
      End Set
    End Property


    Public Property OperatorField() As String
      Get
        Return _operator
      End Get
      Set(ByVal value As String)
        _operator = value
      End Set
    End Property


    Public Property OperatorValue() As Integer
      Get
        Return _operatorValue
      End Get
      Set(ByVal value As Integer)
        _operatorValue = value
      End Set
    End Property


    Public Property Field1() As Decimal
      Get
        Return _field1
      End Get
      Set(ByVal value As Decimal)
        _field1 = value
      End Set
    End Property

    Public Property Field2() As Decimal
      Get
        Return _field2
      End Get
      Set(ByVal value As Decimal)
        _field2 = value
      End Set
    End Property


    Public Property Color() As String
      Get
        Return _color
      End Get
      Set(ByVal value As String)
        _color = value
      End Set
    End Property

    Public Property Deleted() As Boolean
      Get
        Return _deleted
      End Get
      Set(ByVal value As Boolean)
        _deleted = value
      End Set
    End Property

  End Class

#End Region

#Region "Overrides"

  Public Overrides Function AuditorData() As CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)

    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(8427), "")

    For Each _filters As INTELLIA_FILTERS In m_list

      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8429), _filters.Visible, "")

      For Each _range As INTELLIA_FILTERS_RANGES In _filters.Ranges.FindAll(Function(x) x.Id > 0)

        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8428), _range.Title, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8428))
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8442), _range.OperatorField, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8442))
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8443), _range.Field1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8443))
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8444), _range.Field2, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8444))
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8445), _range.Color, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8445))
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4567), _range.Deleted, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4567))

      Next

    Next

    For Each _filters As INTELLIA_FILTERS In m_list

      For Each _range As INTELLIA_FILTERS_RANGES In _filters.Ranges.FindAll(Function(x) x.Id = 0)
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8428), _range.Title, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8428))
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8442), _range.OperatorField, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8442))
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8443), _range.Field1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8443))
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8444), _range.Field2, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8444))
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8445), _range.Color, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8445))
      Next

    Next


    Return _auditor_data
  End Function

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS

  End Function

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS

  End Function

  Public Overrides Function DB_Read(ObjectId As Object, ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS

  End Function

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS
    Return Filters_Update(SqlCtx)
  End Function

  Public Overrides Function Duplicate() As CLASS_BASE

    Dim _target As CLASS_INTELLIA_FILTERS

    _target = New CLASS_INTELLIA_FILTERS

    _target.Filters = Me.Filters

    Return _target

  End Function

#End Region


#Region "Private Functions"

  Private Function ReadFilters() As List(Of INTELLIA_FILTERS)

    Dim _list As List(Of INTELLIA_FILTERS)
    Dim _str_sql As String
    Dim _item As INTELLIA_FILTERS
    Dim _reader As SqlDataReader

    _list = New List(Of INTELLIA_FILTERS)

    Using _db_trx As New DB_TRX()

      'EOR 22-AUG-2017 Not Show Losses
      _str_sql = "SELECT  lr_id,lr_name,lr_nls,lr_editable, lr_visible,lr_has_unknown_values, lr_section_id" & _
                " FROM   layout_ranges " & _
              "  WHERE   lr_section_id IN (60,70) " & _
              " AND lr_id <> 15 " & _
              " order by lr_section_id "

      Using _cmd As New SqlClient.SqlCommand(_str_sql, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

        _reader = _cmd.ExecuteReader()

        If _reader.HasRows Then
          Do While _reader.Read()

            _item = New INTELLIA_FILTERS

            _item.Id = _reader.GetInt32(0)
            _item.Title = _reader.GetString(1)
            _item.Nls = IIf(_reader.IsDBNull(2), 0, _reader.GetInt32(2))
            _item.Editable = IIf(_reader.IsDBNull(3), False, _reader.GetBoolean(3))
            _item.Visible = IIf(_reader.IsDBNull(4), False, _reader.GetBoolean(4))
            _item.HasUnknown = IIf(_reader.IsDBNull(5), False, _reader.GetBoolean(5))
            _item.Type = _reader.GetInt32(6)
            _list.Add(_item)

          Loop
        End If

        _reader.Close()

      End Using

    End Using

    Return _list

  End Function

  Private Sub ReadRanges(ByVal Filters As List(Of INTELLIA_FILTERS))

    Dim _list As List(Of INTELLIA_FILTERS_RANGES)
    Dim _str_sql As String
    Dim _str_sql_final As String
    Dim _item As INTELLIA_FILTERS_RANGES
    Dim _reader As SqlDataReader

    Using _db_trx As New DB_TRX()

      _str_sql = "SELECT  lrl_id,lrl_label,lrl_value1,lrl_value2, lrl_operator,lrl_color, lrl_editable" & _
                " FROM   layout_ranges_legends WHERE lrl_range_id="

      For Each _filter As INTELLIA_FILTERS In Filters

        _str_sql_final = _str_sql + _filter.Id.ToString

        _list = New List(Of INTELLIA_FILTERS_RANGES)

        Using _cmd As New SqlClient.SqlCommand(_str_sql_final, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _reader = _cmd.ExecuteReader()

          If _reader.HasRows Then
            Do While _reader.Read()

              _item = New INTELLIA_FILTERS_RANGES

              _item.Id = _reader.GetInt32(0)
              _item.IdTemp = 0
              _item.Title = _reader.GetString(1)
              _item.Field1 = IIf(_reader.IsDBNull(2), 0, _reader.GetDecimal(2))
              _item.Field2 = IIf(_reader.IsDBNull(3), 0, _reader.GetDecimal(3))
              _item.OperatorField = IIf(_reader.IsDBNull(4), "", _reader.GetString(4).Trim())
              _item.OperatorValue = OperatorToInt(_item.OperatorField.Trim())
              _item.Color = IIf(_reader.IsDBNull(5), "", _reader.GetString(5))
              _item.Deleted = False
              _list.Add(_item)

            Loop
          End If

          _reader.Close()

        End Using

        _filter.Ranges = _list

      Next

      VerifyUnknown(Filters)

    End Using

  End Sub


  Private Sub VerifyUnknown(ByRef Filters As List(Of INTELLIA_FILTERS))

    Dim range As INTELLIA_FILTERS_RANGES
    Dim rand As System.Random = New System.Random()

    For Each _filter As INTELLIA_FILTERS In Filters.FindAll(Function(x) x.HasUnknown = True)

        range = _filter.Ranges.Find(Function(x) x.OperatorValue = 0)

        If range Is Nothing Then

          range = New INTELLIA_FILTERS_RANGES
          range.Id = 0
          range.IdTemp = rand.Next(100, 200)
          range.Title = "Unknown"
          range.Field1 = 0
          range.Field2 = 0
          range.OperatorField = "Unknown"
          range.OperatorValue = 0
          range.Color = RandomColor()
          range.Deleted = False

          _filter.Ranges.Add(range)

      End If

    Next

  End Sub

#End Region

#Region "Public Function"

  '----------------------------------------------------------------------------
  ' PURPOSE: Update the given object into the database.
  '
  ' PARAMS:
  '   - INPUT:  None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES:
  Private Function Filters_Update(ByVal Context As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _str_sql As String
    Dim _error As Boolean
    Try

      Using _db_trx As New DB_TRX()

        ' UPDATE Beacon 
        _str_sql = "UPDATE [dbo].[layout_ranges] " & _
                    "   SET [lr_visible] = @visible " & _
                    "  WHERE lr_id   = @id "

        For Each _filters As INTELLIA_FILTERS In m_list

          Using _cmd As New SqlClient.SqlCommand(_str_sql, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

            _cmd.Parameters.Add("@id", SqlDbType.BigInt).Value = _filters.Id
            _cmd.Parameters.Add("@visible", SqlDbType.Bit).Value = _filters.Visible
            _cmd.ExecuteNonQuery()

          End Using

          _error = UpdateRanges(_filters.Id, _filters.Ranges, _db_trx)

          If _error Then
            Exit For
          End If
        Next

        If _error Then
          _db_trx.Rollback()
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If
        _db_trx.Commit()

      End Using '_db_trx

    Catch ex As Exception
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function ' Beacon_Insert


  Private Function UpdateRanges(ByVal IdFilter As Integer, ByVal Ranges As List(Of INTELLIA_FILTERS_RANGES), ByRef DB_TRX As DB_TRX) As Boolean


    Dim _str_sql_insert As String
    Dim _str_sql_update As String
    Dim _str_sql_delete As String

    _str_sql_delete = "DELETE FROM layout_ranges_legends WHERE lrl_id = @lrl_id"
    _str_sql_insert = "INSERT INTO layout_ranges_legends ([lrl_range_id],[lrl_label],[lrl_value1],[lrl_value2],[lrl_operator],[lrl_color]) VALUES (@lrl_range_id,@lrl_label,@lrl_value1,@lrl_value2,@lrl_operator,@lrl_color)"
    _str_sql_update = "UPDATE layout_ranges_legends SET lrl_label=@lrl_label,lrl_value1=@lrl_value1,lrl_value2=@lrl_value2,lrl_operator=@lrl_operator,lrl_color=@lrl_color WHERE lrl_id = @lrl_id"

    Try

      For Each _range As INTELLIA_FILTERS_RANGES In Ranges

        If _range.Deleted Then
          Using _cmd As New SqlClient.SqlCommand(_str_sql_delete, DB_TRX.SqlTransaction.Connection, DB_TRX.SqlTransaction)

            _cmd.Parameters.Add("@lrl_id", SqlDbType.BigInt).Value = _range.Id
            _cmd.ExecuteNonQuery()

          End Using
        ElseIf _range.Id = 0 Then
          Using _cmd As New SqlClient.SqlCommand(_str_sql_insert, DB_TRX.SqlTransaction.Connection, DB_TRX.SqlTransaction)

            _cmd.Parameters.Add("@lrl_range_id", SqlDbType.BigInt).Value = IdFilter
            _cmd.Parameters.Add("@lrl_label", SqlDbType.NVarChar, 30).Value = _range.Title
            _cmd.Parameters.Add("@lrl_operator", SqlDbType.NChar, 10).Value = _range.OperatorField
            _cmd.Parameters.Add("@lrl_value1", SqlDbType.Decimal).Value = _range.Field1
            _cmd.Parameters.Add("@lrl_value2", SqlDbType.Decimal).Value = _range.Field2
            _cmd.Parameters.Add("@lrl_color", SqlDbType.NVarChar, 10).Value = _range.Color
            _cmd.ExecuteNonQuery()

          End Using
        Else
          Using _cmd As New SqlClient.SqlCommand(_str_sql_update, DB_TRX.SqlTransaction.Connection, DB_TRX.SqlTransaction)

            _cmd.Parameters.Add("@lrl_id", SqlDbType.BigInt).Value = _range.Id
            _cmd.Parameters.Add("@lrl_label", SqlDbType.NVarChar, 30).Value = _range.Title
            _cmd.Parameters.Add("@lrl_operator", SqlDbType.NChar, 10).Value = _range.OperatorField
            _cmd.Parameters.Add("@lrl_value1", SqlDbType.Decimal).Value = _range.Field1
            _cmd.Parameters.Add("@lrl_value2", SqlDbType.Decimal).Value = _range.Field2
            _cmd.Parameters.Add("@lrl_color", SqlDbType.NVarChar, 10).Value = _range.Color
            _cmd.ExecuteNonQuery()

          End Using
        End If

      Next

    Catch ex As Exception
      Return True
    End Try
    Return False
  End Function

  Public Shared Function OperatorToInt(ByVal OperatorField As String) As Integer
    Select Case OperatorField.Trim()
      Case "Unknown"
        Return 0
      Case "<"
        Return 1
      Case "Between"
        Return 2
      Case ">"
        Return 3
      Case "="
        Return 4
    End Select
  End Function

  Public Shared Function IntToOperator(ByVal Value As Integer) As String
    Select Case Value
      Case 0
        Return "Unknown"
      Case 1
        Return "<"
      Case 2
        Return "Between"
      Case 3
        Return ">"
      Case 4
        Return "="
      Case Else
        Return ""
    End Select
  End Function

  Public Shared Function RandomColor() As String

    Dim _red As Integer
    Dim _green As Integer
    Dim _blue As Integer
    Dim _colorString As String

    ' Initialize the random-number generator
    Randomize()

    ' Generate random value between 1 and 6.
    _red = CInt(Int((254 * Rnd()) + 0))

    ' Initialize the random-number generator.
    Randomize()

    ' Generate random value between 1 and 6.
    _green = CInt(Int((254 * Rnd()) + 0))

    ' Initialize the random-number generator.
    Randomize()

    ' Generate random value between 1 and 6.
    _blue = CInt(Int((254 * Rnd()) + 0))

    _colorString = String.Format("#{0:X2}{1:X2}{2:X2}", _red, _green, _blue)

    Return _colorString

  End Function

#End Region

End Class
