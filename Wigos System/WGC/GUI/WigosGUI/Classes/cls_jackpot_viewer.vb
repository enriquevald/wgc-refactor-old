﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME :       cls_jackpot_viewer.vb
' DESCRIPTION :       jackpot viewer class for user edition
' AUTHOR:             Carlos Cáceres González              
' REVISION HISTORY :
'
' Date         Author  Description
' -----------  ------  -----------------------------------------------
' 16-MAY-2017  CCG     Initial version
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common
Imports WSI.Common.Jackpot

Public Class CLASS_JACKPOT_VIEWER
  Inherits CLASS_BASE

#Region " Constants "

#End Region ' Constants

#Region " Enums "

#End Region ' Enums 

#Region " Structures "

#End Region ' Structures

#Region " Members "

  Protected m_list As New List(Of JackpotViewer)
  Protected m_selected As New JackpotViewer

#End Region ' Members

#Region " Properties "

  Public Property Selected As JackpotViewer
    Get
      Return Me.m_selected
    End Get
    Set(value As JackpotViewer)
      Me.m_selected = value
    End Set
  End Property

  Public Property List As List(Of JackpotViewer)
    Get
      Return Me.m_list
    End Get
    Set(value As List(Of JackpotViewer))
      Me.m_list = value
    End Set

  End Property

#End Region ' Properties

#Region " Public Functions "

  Public Sub New()

    Me.List = New JackpotViewer().GetAllJackpotsViewers()

  End Sub

  ''' <summary>
  ''' Returns the related auditor data for the current object.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function AuditorData() As CLASS_AUDITOR_DATA
    Dim _auditor_data As CLASS_AUDITOR_DATA

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_JACKPOT_VIEWER_CONFIG)

    _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(8295), " - " + Me.Selected.Name)

    Call _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(282), Me.Selected.Name) ' Name
    Call _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(586), Me.Selected.Code) ' Code

    If Me.Selected.Enabled Then
      _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(218), GLB_NLS_GUI_JACKPOT_MGR.GetString(218)) ' Enabled
    Else
      _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(218), GLB_NLS_GUI_JACKPOT_MGR.GetString(250)) ' Disabled
    End If

    If Not Me.Selected.Jackpots Is Nothing Then

      For Each _jackpot_viewer_item As JackpotViewerItem In Me.Selected.Jackpots.Items

        Call _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(592), _jackpot_viewer_item.Order) ' Order
        Call _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(8296), _jackpot_viewer_item.Name) ' Jackpot name
        Call _auditor_data.SetField(GLB_NLS_GUI_JACKPOT_MGR.Id(215), _jackpot_viewer_item.Status) ' Status

      Next

    End If

    If Not Me.Selected.Terminals Is Nothing Then

      For Each _jackpot_viewer_terminal_item As JackpotViewerTerminalItem In Me.Selected.Terminals.Items

        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(492), _jackpot_viewer_terminal_item.TerminalId) ' Terminals 

      Next

    End If

    Return _auditor_data

  End Function ' AuditorData

  ''' <summary>
  ''' Deletes the given object from the database.
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS

    Try

      Return ENUM_STATUS.STATUS_ERROR

    Catch ex As Exception
      Log.Error(String.Format("Jackpot Viewer DB_Delete: There was an error deleting JackpotId:{0}", Me.Selected.Id))
      Log.Exception(ex)

      Return ENUM_STATUS.STATUS_ERROR
    End Try

    Return ENUM_STATUS.STATUS_OK

  End Function

  ''' <summary>
  ''' Reads from the database into the given object
  ''' </summary>
  ''' <param name="ObjectId"></param>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Read(ObjectId As Object, ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS
    Dim _jackpot_viewer As JackpotViewer

    Try

      Using _db_trx As New DB_TRX()
        _jackpot_viewer = New JackpotViewer(ObjectId, _db_trx.SqlTransaction)

        If _jackpot_viewer Is Nothing Then
          Return ENUM_STATUS.STATUS_ERROR
        End If

        Me.m_list.SetJackpotViewerById(ObjectId, _jackpot_viewer)
        Me.m_selected = _jackpot_viewer

      End Using

    Catch ex As Exception
      Log.Error(String.Format("Jackpot Viewer DB_Read: There was an error getting JackpotId:{0}", Me.Selected.Id))
      Log.Exception(ex)

      Return ENUM_STATUS.STATUS_ERROR

    End Try

    Return ENUM_STATUS.STATUS_OK

  End Function ' DB_Read

  ''' <summary>
  ''' Inserts the given object to the database.
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS

    Try

      Using _db_trx As New DB_TRX()

        ' Save Jackpot Viewer
        If Not Me.Selected.Save(_db_trx.SqlTransaction) Then
          Return ENUM_STATUS.STATUS_ERROR
        End If

        Me.m_list.Add(Me.Selected.Clone())

        _db_trx.Commit()

      End Using

    Catch ex As Exception

      Log.Error(String.Format("Jackpot Viewer DB_Insert: There was an error getting JackpotId:{0}", Me.Selected.Id))
      Log.Exception(ex)

      Return ENUM_STATUS.STATUS_ERROR
    End Try

    Return ENUM_STATUS.STATUS_OK

  End Function ' DB_Insert

  ''' <summary>
  ''' Updates the given object to the database.
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS

    Try

      ' Save Jackpot
      If Not Me.Selected.Save() Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

    Catch ex As Exception

      Log.Error(String.Format("Jackpot Viewer DB_Update: There was an error getting JackpotId:{0}", Me.Selected.Id))
      Log.Exception(ex)

      Return ENUM_STATUS.STATUS_ERROR
    End Try

    Return ENUM_STATUS.STATUS_OK

  End Function ' DB_Update

  ''' <summary>
  ''' Returns a duplicate of the given object.
  ''' Not includes ID
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function Duplicate() As CLASS_BASE

    Dim _temp_object As CLASS_JACKPOT_VIEWER

    _temp_object = New CLASS_JACKPOT_VIEWER

    _temp_object.List = Me.List.Clone()

    If Not Me.Selected Is Nothing Then
      _temp_object.Selected = Me.Selected.Clone()
    End If

    Return _temp_object

  End Function ' Duplicate

#Region " Jackpot "

#End Region

#End Region ' Public Functions

#Region " Private Functions "

#End Region ' Private Functions

End Class
