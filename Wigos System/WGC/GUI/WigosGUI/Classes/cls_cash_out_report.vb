﻿Imports GUI_CommonOperations
Imports System.Text
Imports GUI_Controls

Public Class cls_cash_out_report

#Region "Public Methods"

  ''' <summary>
  ''' Get data for show in the report
  ''' </summary>
  ''' <param name="DateFrom"></param>
  ''' <param name="DateTo"></param>
  ''' <param name="Providers"></param>
  ''' <param name="Terminals"></param>
  ''' <param name="Accounts"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function GetDataReport(ByVal DateFrom As uc_date_picker, ByVal DateTo As uc_date_picker, ByVal Providers As uc_provider, ByVal Terminals As uc_provider, ByVal Accounts As uc_account_sel, ByVal IsAnonymous As Boolean) As String

    Dim _sql_str As New StringBuilder

    _sql_str.AppendLine("     SELECT   APCURR_PROV_ID                                                  ")
    _sql_str.AppendLine("	           , APCURR_DATETIME                                                 ")
    _sql_str.AppendLine("	           , APCURR_TERMINAL_ID                                              ")
    _sql_str.AppendLine("            , APCURR_VOUCHER_ID                                               ")
    _sql_str.AppendLine("            , APCURR_PLAY_SESSION_ID                                          ")
    _sql_str.AppendLine("	           , APCURR_STATUS                                                   ")
    _sql_str.AppendLine("	           , APCURR_PROVIDER                                                 ")
    _sql_str.AppendLine("	           , APCURR_EGM                                                      ")
    _sql_str.AppendLine("	           , APCURR_TRANSACTION_NO                                           ")
    _sql_str.AppendLine("	           , APCURR_RECEIPT_NO                                               ")
    _sql_str.AppendLine("	           , APCURR_AMOUNT                                                   ")
    _sql_str.AppendLine("	           , APCURR_ACCOUNT_ID                                               ")
    _sql_str.AppendLine("	           , APCURR_ACCOUNT_HOLDER                                           ")
    _sql_str.AppendLine("       FROM   AUTO_PRINT_CASH_OUT_RECEIPT_REPORT                              ")
    _sql_str.AppendLine(" INNER JOIN   ACCOUNTS AC ON AC.AC_ACCOUNT_ID = APCURR_ACCOUNT_ID             ")
    _sql_str.AppendLine("      WHERE   1 = 1                                                           ")

    If DateFrom.Checked Then
      _sql_str.AppendLine(String.Format("        AND   APCURR_DATETIME     > {0}                                  ", GUI_FormatDateDB(DateFrom.Value)))
    End If

    If DateTo.Checked Then
      _sql_str.AppendLine(String.Format("        AND   APCURR_DATETIME    <= {0}                                  ", GUI_FormatDateDB(DateTo.Value)))
    End If

    If Providers.GetOnlyProviderIdListSelected().Length > 0 Then
      _sql_str.AppendLine(String.Format("        AND   APCURR_PROVIDER     IN ('{0}')                                  ", String.Join("','", Providers.GetOnlyProviderIdListSelected())))
    End If

    If Not String.IsNullOrEmpty(Terminals.GetProviderIdListSelected()) Then
      _sql_str.AppendLine(String.Format("        AND   APCURR_TERMINAL_ID IN {0}                                  ", Terminals.GetProviderIdListSelected()))
    End If

    If Not String.IsNullOrEmpty(Accounts.GetFilterSQL()) Then
      _sql_str.AppendLine(String.Format("        AND   {0}                                                        ", Accounts.GetFilterSQL()))
    End If

    If IsAnonymous Then
      _sql_str.AppendLine("        AND   AC.AC_HOLDER_LEVEL = 0                                                   ")
    End If

    _sql_str.AppendLine("   ORDER BY   APCURR_PROV_ID ASC, APCURR_DATETIME ASC, APCURR_TERMINAL_ID ASC            ")

    Return _sql_str.ToString

  End Function ' GetDataReport

#End Region

End Class
