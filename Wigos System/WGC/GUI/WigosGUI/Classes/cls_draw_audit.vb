'-------------------------------------------------------------------
' Copyright � 2008 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_draw_audit
' DESCRIPTION : Class for draw audit data
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 30-JUL-2008  TJG    Initial version
'--------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Xml

Public Class CLASS_DRAW_AUDIT
  Inherits CLASS_BASE

#Region " Constants "

  ' Fields length
  Public Const MAX_DRAW_PLAYS As Integer = 10
  Public Const MAX_LEN_GAME_NAME As Integer = 20
  Public Const MAX_LEN_TERMINAL_NAME As Integer = 50
  Public Const NUMBERS_PER_CARD As Integer = 25
  Public Const NUMBERS_PER_DRAW As Integer = 75

#End Region

#Region " GUI_Configuration.dll "

#Region " Structures "

  ' Jackpot prize (if any)
  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_DRAW_AUDIT_PLAY_JACKPOT
    Public awarded As Boolean
    Public amount As Double
    Public index As Integer
    Public name As String
  End Class
  ' Item of draw audit prizes array
  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_DRAW_AUDIT_CARD_PRIZE
    Public prize_credits As Double
    Public num_positions As Integer
    Public position_list() As Integer
  End Class

  ' Item of draw audit plays array
  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_DRAW_AUDIT_PLAY_CARD
    Public id As String
    Public type As String

    Public num_numbers As Integer
    Public numbers() As String

    Public num_card_prizes As Integer
    Public prizes() As TYPE_DRAW_AUDIT_CARD_PRIZE
  End Class

  ' Item of draw audit plays array
  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_DRAW_AUDIT_PLAY
    Public play_id As Int64
    Public play_date As New TYPE_DATE_TIME
    Public game_id As Integer

    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_LEN_GAME_NAME + 1)> _
    Public game_name As String
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=3)> _
    Public filler_0 As String                                 ' MAX_LEN_GAME_NAME + 1 + 3 = 24

    Public terminal_id As Integer

    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_LEN_TERMINAL_NAME + 1)> _
    Public terminal_name As String
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=1)> _
    Public filler_1 As String                                 ' MAX_LEN_GAME_NAME + 1 + 1 = 52

    Public denomination As Double
    Public played_credits As Double
    Public won_credits As Double
    Public jackpot_bound_credits As Double
    Public num_prizes As Integer

    Public jackpot As New TYPE_DRAW_AUDIT_PLAY_JACKPOT

    Public num_cards As Integer
    Public cards() As TYPE_DRAW_AUDIT_PLAY_CARD
  End Class

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_DRAW_AUDIT
    Public control_block As Integer
    Public draw_id As Int64
    Public draw_date As New TYPE_DATE_TIME
    Public num_winning_numbers As Integer
    Public winning_numbers() As String

    Public num_plays As Integer
    Public plays() As TYPE_DRAW_AUDIT_PLAY
  End Class

#End Region

#End Region

#Region " Members "

  Protected m_draw_audit As New TYPE_DRAW_AUDIT

#End Region

#Region " Properties "

  Public Property DrawId() As Int64
    Get
      Return m_draw_audit.draw_id
    End Get

    Set(ByVal Value As Int64)
      m_draw_audit.draw_id = Value
    End Set
  End Property

  Public Property DrawDate() As Date
    Get
      Return m_draw_audit.draw_date.Value
    End Get

    Set(ByVal Value As Date)
      m_draw_audit.draw_date.Value = Value
    End Set
  End Property

  Public Property NumWinningNumbers() As Integer
    Get
      Return m_draw_audit.num_winning_numbers
    End Get

    Set(ByVal Value As Integer)
      m_draw_audit.num_winning_numbers = Value
    End Set
  End Property

  Public Property WinningNumber(ByVal IdxNumber As Integer) As String
    Get
      Return m_draw_audit.winning_numbers(IdxNumber)
    End Get

    Set(ByVal Value As String)
      m_draw_audit.winning_numbers(IdxNumber) = Value
    End Set
  End Property

  Public Property NumPlays() As Integer
    Get
      Return m_draw_audit.num_plays
    End Get

    Set(ByVal Value As Integer)
      m_draw_audit.num_plays = Value
    End Set
  End Property

  Public Property Play(ByVal IdxPlay As Integer) As TYPE_DRAW_AUDIT_PLAY
    Get
      Return m_draw_audit.plays(IdxPlay)
    End Get

    Set(ByVal Value As TYPE_DRAW_AUDIT_PLAY)
      m_draw_audit.plays(IdxPlay) = Value
    End Set
  End Property

#End Region

#Region " Overrides functions "

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer

    Me.DrawId = ObjectId

    m_draw_audit.control_block = Marshal.SizeOf(m_draw_audit)

    ' Read user data (except password)
    rc = ReadDrawAudit(m_draw_audit, SqlCtx)

    Select Case rc

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer

    m_draw_audit.control_block = Marshal.SizeOf(m_draw_audit)

    ' Update user data
    rc = UpdateDrawAudit(m_draw_audit, SqlCtx)

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_PASSWORD
        Return CLASS_BASE.ENUM_STATUS.STATUS_PASSWORD

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer

    m_draw_audit.control_block = Marshal.SizeOf(m_draw_audit)

    ' Insert user data
    rc = InsertDrawAudit(m_draw_audit, SqlCtx)

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_PASSWORD
        Return CLASS_BASE.ENUM_STATUS.STATUS_PASSWORD

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer

    m_draw_audit.control_block = Marshal.SizeOf(m_draw_audit)

    ' Delete user data
    rc = DeleteDrawAudit(m_draw_audit, SqlCtx)

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DEPENDENCIES
        Return ENUM_STATUS.STATUS_DEPENDENCIES

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim auditor_data As New CLASS_AUDITOR_DATA(AUDIT_NAME_GENERIC)

    ' This function is defined because of the base class, but there is nothing to audit
    auditor_data.IsAuditable = False

    Return auditor_data

  End Function

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim temp_user As CLASS_DRAW_AUDIT

    temp_user = New CLASS_DRAW_AUDIT
    temp_user.m_draw_audit = m_draw_audit

    Return temp_user

  End Function

#End Region

#Region " Private Functions "

  Private Function ReadDrawAudit(ByVal DrawAudit As TYPE_DRAW_AUDIT, _
                                 ByVal Context As Integer) As Integer
    Dim str_sql As String
    Dim data_table As DataTable
    Dim jackpot_history_table As DataTable
    Dim card_prize As TYPE_DRAW_AUDIT_CARD_PRIZE
    Dim array_length As Integer
    Dim idx_play As Integer
    Dim idx_card As Integer
    Dim aux_text As String
    Dim card_id As String
    Dim idx_pos As Integer
    Dim idx_row As Integer
    Dim position_list() As String
    Dim idx_card_prize As Integer
    Dim str_reader As System.IO.StringReader
    Dim xml_reader As XmlReader
    
    If DrawAudit.draw_id < 1 Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
    End If

    str_sql = "select DA_DRAW_ID" _
                 & ", DA_DRAW_DATETIME" _
                 & ", DA_WINNING_NUMBERS" _
                 & ", DAP_PLAY_ID" _
                 & ", DAP_PLAY_DATETIME" _
                 & ", DAP_GAME_ID" _
                 & ", DAP_GAME_NAME" _
                 & ", DAP_TERMINAL_ID" _
                 & ", DAP_TERMINAL_NAME" _
                 & ", DAP_DENOMINATION" _
                 & ", DAP_PLAYED_CREDITS" _
                 & ", DAP_WON_CREDITS" _
                 & ", DAP_JACKPOT_BOUND_CREDITS" _
                 & ", DAP_CARDS" _
                 & ", DAP_PRIZE_LIST" _
             & " from C2_DRAW_AUDIT" _
                 & ", C2_DRAW_AUDIT_PLAYS" _
            & " where DA_DRAW_ID = " & DrawAudit.draw_id _
              & " and DA_DRAW_ID = DAP_DRAW_ID"

    data_table = GUI_GetTableUsingSQL(str_sql, 5000)
    If IsNothing(data_table) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If data_table.Rows.Count() < 1 Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    ' Parse the read data
    '     - Draw Datetime
    DrawAudit.draw_date.Value = data_table.Rows(0).Item("DA_DRAW_DATETIME")

    '     - Draw Winning numbers
    '           <NumBalls> </NumBalls> 
    '           <Balls> </Balls> 
    aux_text = data_table.Rows(0).Item("DA_WINNING_NUMBERS")
    str_reader = New System.IO.StringReader(aux_text)
    xml_reader = XmlReader.Create(str_reader)

    DrawAudit.num_winning_numbers = WSI.Common.XML.GetValue(xml_reader, "NumBalls")

    If DrawAudit.num_winning_numbers > 0 Then
      ' Get the numbers as a string 
      aux_text = WSI.Common.XML.GetValue(xml_reader, "Balls")
      If aux_text = "" Then
        DrawAudit.num_winning_numbers = 0
      Else
        DrawAudit.winning_numbers = aux_text.Split(" ")
        DrawAudit.num_winning_numbers = Math.Min(DrawAudit.num_winning_numbers, DrawAudit.winning_numbers.Length)
      End If
    End If

    xml_reader.Close()
    str_reader.Close()

    '     - Number of Plays
    DrawAudit.num_plays = data_table.Rows.Count()

    '     - For every Play related to the Draw
    If DrawAudit.num_plays < 1 Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
    End If

    ' Allocate the array of plays
    array_length = IIf(DrawAudit.num_plays = 1, 1, DrawAudit.num_plays - 1)
    ReDim DrawAudit.plays(array_length)

    For idx_play = 0 To DrawAudit.num_plays - 1
      DrawAudit.plays(idx_play) = New TYPE_DRAW_AUDIT_PLAY

      ' Play Details
      DrawAudit.plays(idx_play).play_id = data_table.Rows(idx_play).Item("DAP_PLAY_ID")
      DrawAudit.plays(idx_play).play_date.Value = data_table.Rows(idx_play).Item("DAP_PLAY_DATETIME")
      DrawAudit.plays(idx_play).game_id = data_table.Rows(idx_play).Item("DAP_GAME_ID")
      DrawAudit.plays(idx_play).game_name = NullTrim(data_table.Rows(idx_play).Item("DAP_GAME_NAME"))
      DrawAudit.plays(idx_play).terminal_id = data_table.Rows(idx_play).Item("DAP_TERMINAL_ID")
      DrawAudit.plays(idx_play).terminal_name = NullTrim(data_table.Rows(idx_play).Item("DAP_TERMINAL_NAME"))
      DrawAudit.plays(idx_play).denomination = data_table.Rows(idx_play).Item("DAP_DENOMINATION")
      DrawAudit.plays(idx_play).played_credits = data_table.Rows(idx_play).Item("DAP_PLAYED_CREDITS")
      DrawAudit.plays(idx_play).won_credits = data_table.Rows(idx_play).Item("DAP_WON_CREDITS")
      DrawAudit.plays(idx_play).jackpot_bound_credits = data_table.Rows(idx_play).Item("DAP_JACKPOT_BOUND_CREDITS")

      ' Cards related to the Play
      '   <CardList>
      '       <NumCards> </NumCards> 
      '       <Card>
      '           <CardId> </CardId> 
      '           <CardType> </CardType>
      '           <CardNumbers> </CardNumbers>
      '       </Card> 
      '       ...
      '   </CardList>

      aux_text = data_table.Rows(idx_play).Item("DAP_CARDS")
      str_reader = New System.IO.StringReader(aux_text)
      xml_reader = XmlReader.Create(str_reader)

      ' Number of Cards
      DrawAudit.plays(idx_play).num_cards = WSI.Common.XML.GetValue(xml_reader, "NumCards")

      If DrawAudit.plays(idx_play).num_cards > 0 Then
        array_length = IIf(DrawAudit.plays(idx_play).num_cards = 1, 1, DrawAudit.plays(idx_play).num_cards - 1)
        ReDim DrawAudit.plays(idx_play).cards(array_length)

        For idx_card = 0 To DrawAudit.plays(idx_play).num_cards - 1
          DrawAudit.plays(idx_play).cards(idx_card) = New TYPE_DRAW_AUDIT_PLAY_CARD

          DrawAudit.plays(idx_play).cards(idx_card).id = WSI.Common.XML.GetValue(xml_reader, "CardId").Trim
          DrawAudit.plays(idx_play).cards(idx_card).type = WSI.Common.XML.GetValue(xml_reader, "CardType")
          DrawAudit.plays(idx_play).cards(idx_card).num_card_prizes = 0
          DrawAudit.plays(idx_play).cards(idx_card).num_numbers = 0

          ' Get the card numbers as a string 
          aux_text = WSI.Common.XML.GetValue(xml_reader, "CardNumbers")
          If aux_text <> "" Then
            DrawAudit.plays(idx_play).cards(idx_card).numbers = aux_text.Split(" ")
            DrawAudit.plays(idx_play).cards(idx_card).num_numbers = DrawAudit.plays(idx_play).cards(idx_card).numbers.Length
          End If

        Next
      End If

      xml_reader.Close()
      str_reader.Close()

      ' Prizes related to the Cards
      ' Card Prizes are browsed later to allow relate them to the card

      '   <PrizeList>
      '       <NumPrizes></NumPrizes>
      '       <Prize>
      '           <CardId> </CardId>
      '           <CardType> </CardType>
      '           <CardPrizeList>
      '               <NumCardPrizes> </NumCardPrizes>
      '               <CardPrize>
      '                   <ResultIndex> </ResultIndex>
      '                   <PrizeCredits> </PrizeCredits>
      '                   <PrizePattern> 
      '                       <NumPositions> </NumPositions>
      '                       <PositionList> </PositionList>
      '                   </PrizePattern>	
      '               </CardPrize>
      '               ...
      '           </CardPrizeList>
      '       </Prize>
      '       ...
      '   </PrizeList>

      aux_text = data_table.Rows(idx_play).Item("DAP_PRIZE_LIST")
      str_reader = New System.IO.StringReader(aux_text)
      xml_reader = XmlReader.Create(str_reader)

      ' Number of Card Prizes
      DrawAudit.plays(idx_play).num_prizes = WSI.Common.XML.GetValue(xml_reader, "NumPrizes")

      If DrawAudit.plays(idx_play).num_prizes > 0 Then
        card_id = WSI.Common.XML.GetValue(xml_reader, "CardId").Trim

        ' Search for the card in the card list populated above 
        For idx_card = 0 To DrawAudit.plays(idx_play).num_cards - 1
          ' Seek the card id
          If card_id <> DrawAudit.plays(idx_play).cards(idx_card).id Then
            ' Card id does not match
            Continue For
          End If

          ' Card found
          DrawAudit.plays(idx_play).cards(idx_card).num_card_prizes = WSI.Common.XML.GetValue(xml_reader, "NumCardPrizes")

          ' Check out the card's prize list  
          If DrawAudit.plays(idx_play).cards(idx_card).num_card_prizes <= 0 Then
            ' No card prizes
            Continue For
          End If

          ' Populate the prize list  
          array_length = IIf(DrawAudit.plays(idx_play).cards(idx_card).num_card_prizes = 1, 1, DrawAudit.plays(idx_play).cards(idx_card).num_card_prizes - 1)
          ReDim DrawAudit.plays(idx_play).cards(idx_card).prizes(array_length)

          For idx_card_prize = 0 To DrawAudit.plays(idx_play).cards(idx_card).num_card_prizes - 1
            DrawAudit.plays(idx_play).cards(idx_card).prizes(idx_card_prize) = New TYPE_DRAW_AUDIT_CARD_PRIZE()

            ' To easy read the code
            card_prize = DrawAudit.plays(idx_play).cards(idx_card).prizes(idx_card_prize)

            card_prize.prize_credits = WSI.Common.XML.GetValue(xml_reader, "PrizeCredits")
            card_prize.num_positions = WSI.Common.XML.GetValue(xml_reader, "NumPositions")

            If card_prize.num_positions <= 0 Then
              Continue For
            End If

            ' Get the position number as a string 
            aux_text = WSI.Common.XML.GetValue(xml_reader, "PositionList")
            If aux_text = "" Then
              card_prize.num_positions = 0
            Else
              position_list = aux_text.Split(" ")

              card_prize.num_positions = Math.Min(card_prize.num_positions, position_list.Length)

              array_length = IIf(card_prize.num_positions = 1, 1, card_prize.num_positions - 1)
              ReDim card_prize.position_list(array_length)

              For idx_pos = 0 To card_prize.num_positions - 1
                ' card_prize.position_list(idx_pos) = New Integer
                card_prize.position_list(idx_pos) = Convert.ToInt32(position_list(idx_pos))
              Next
            End If
          Next  ' For idx_card_prize

          Exit For
        Next    ' For idx_card
      End If

      xml_reader.Close()
      str_reader.Close()
    Next        ' For idx_play 

    ' Check out the jackpot for any of the plays
    str_sql = "select C2JH_PLAY_ID" _
                 & ", C2JH_INDEX" _
                 & ", C2JH_NAME" _
                 & ", C2JH_AMOUNT" _
             & " from C2_JACKPOT_HISTORY"

    For idx_play = 0 To DrawAudit.num_plays - 1
      If idx_play = 0 Then
        str_sql = str_sql & " where C2JH_PLAY_ID = " & DrawAudit.plays(idx_play).play_id.ToString
      Else
        str_sql = str_sql & " or C2JH_PLAY_ID = " & DrawAudit.plays(idx_play).play_id.ToString
      End If

      DrawAudit.plays(idx_play).jackpot.awarded = False
    Next        ' For idx_play 

    jackpot_history_table = GUI_GetTableUsingSQL(str_sql, 5000)
    If IsNothing(jackpot_history_table) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If jackpot_history_table.Rows.Count() < 1 Then
      ' No jackpot was awarded in any play
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
    End If

    ' Browse the jackpots related to the draw's plays
    For idx_row = 0 To jackpot_history_table.Rows.Count() - 1
      ' Search the play 
      For idx_play = 0 To DrawAudit.num_plays - 1
        If DrawAudit.plays(idx_play).play_id = jackpot_history_table.Rows(idx_row).Item("C2JH_PLAY_ID") Then
          DrawAudit.plays(idx_play).jackpot.awarded = True
          DrawAudit.plays(idx_play).jackpot.amount = jackpot_history_table.Rows(idx_row).Item("C2JH_AMOUNT")
          DrawAudit.plays(idx_play).jackpot.index = jackpot_history_table.Rows(idx_row).Item("C2JH_INDEX")
          DrawAudit.plays(idx_play).jackpot.name = NullTrim(jackpot_history_table.Rows(idx_row).Item("C2JH_NAME"))
        End If
      Next        ' For idx_play 
    Next          ' For idx_row 

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function

  Private Function UpdateDrawAudit(ByVal DrawAudit As TYPE_DRAW_AUDIT, _
                                   ByVal Context As Integer) As Integer
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function

  Private Function InsertDrawAudit(ByVal DrawAudit As TYPE_DRAW_AUDIT, _
                                   ByVal Context As Integer) As Integer
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function

  Private Function DeleteDrawAudit(ByVal DrawAudit As TYPE_DRAW_AUDIT, _
                                   ByVal Context As Integer) As Integer
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function

#End Region

End Class
