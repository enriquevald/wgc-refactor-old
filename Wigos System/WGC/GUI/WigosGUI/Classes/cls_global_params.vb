﻿Imports Unity

Public NotInheritable Class cls_global_params
  Private Shared ReadOnly _instance As New Lazy(Of cls_global_params)(Function() New cls_global_params(), System.Threading.LazyThreadSafetyMode.ExecutionAndPublication)
  Private container As IUnityContainer

  Private Sub New()

  End Sub

  'Public Shared Sub SetContainer(container As IUnityContainer)
  '  container = container
  'End Sub
  Public Property GUIContainer() As IUnityContainer
    Get
      Return Container
    End Get
    Set(ByVal Value As IUnityContainer)
      container = Value
    End Set

  End Property

  Public Shared ReadOnly Property Instance() As cls_global_params
    Get
      Return _instance.Value
    End Get
  End Property
End Class
