'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_employee_shift.vb
' DESCRIPTION:   Employee Shift class
' AUTHOR:        Quim Morales
' CREATION DATE: 01-JUL-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 01-JUL-2013  QMP    Initial version
'--------------------------------------------------------------------
#Region " Imports "

Imports GUI_CommonOperations

#End Region ' Imports

Public Class CLASS_EMPLOYEE_SHIFT
  Inherits CLASS_BASE

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA
    Throw New NotImplementedException()
  End Function

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Throw New NotImplementedException()
  End Function

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Throw New NotImplementedException()
  End Function

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Throw New NotImplementedException()
  End Function

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Throw New NotImplementedException()
  End Function

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE
    Throw New NotImplementedException()
  End Function
End Class ' CLASS_EMPLOYEE_SHIFT
