﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_creditline_movements.vb
' DESCRIPTION:   Creditline Class
' AUTHOR:        Enric Tomas
' CREATION DATE: 28-MAR-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 28-MAR-2017  ETP    Initial Version
' 03-APR-2017  ETP    WIGOS - 116: CreditLines - Multisite.
'--------------------------------------------------------------------
Imports GUI_CommonOperations
Imports System.Runtime.InteropServices
Imports System.Xml
Imports WSI.Common
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Text
Imports System.Data.SqlClient
Imports WSI.Common.CreditLines
Imports GUI_Controls.frm_base_sel

Public Class CLASS_CREDITLINE_MOVEMENTS

#Region " Structs "
  Public Structure TYPE_DG_FILTER_ELEMENT
    Dim code As Integer
    Dim description As String
    Dim elem_type As Integer
  End Structure

  Public Structure NEW_ROW_GRID
    Dim type As Integer
    Dim type_description As String
    Dim value As String
  End Structure

#End Region ' Structs

#Region " Const "
  Private Const GRID_2_ROW_TYPE_HEADER = 0
  Private Const GRID_2_ROW_TYPE_DATA = 1
#End Region ' Const

#Region " Members "
  Private m_is_center As Boolean

#End Region ' Members

#Region "Constructor"
  Public Sub New()

    m_is_center = GeneralParam.GetBoolean("MultiSite", "IsCenter", False)

  End Sub ' New
#End Region 'Constructor


#Region " Public Methods "

  ''' <summary>
  ''' Get Filter Grid Data
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function GetMovTypesFilterGridData() As List(Of TYPE_DG_FILTER_ELEMENT)
    Dim _dg_filter_rows_list As List(Of TYPE_DG_FILTER_ELEMENT)
    _dg_filter_rows_list = New List(Of TYPE_DG_FILTER_ELEMENT)

    Me.AddHeader(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7947), _dg_filter_rows_list)

    Me.AddRow(CreditLineMovements.CreditLineMovementType.Create, _dg_filter_rows_list)
    Me.AddRow(CreditLineMovements.CreditLineMovementType.UpdateLimit, _dg_filter_rows_list)
    Me.AddRow(CreditLineMovements.CreditLineMovementType.UpdateTTO, _dg_filter_rows_list)
    Me.AddRow(CreditLineMovements.CreditLineMovementType.UpdateExpiration, _dg_filter_rows_list)
    Me.AddRow(CreditLineMovements.CreditLineMovementType.UpdateStatus, _dg_filter_rows_list)
    Me.AddRow(CreditLineMovements.CreditLineMovementType.UpdateSigners, _dg_filter_rows_list)
    Me.AddRow(CreditLineMovements.CreditLineMovementType.UpdateIBAN, _dg_filter_rows_list)


    Me.AddHeader(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7620), _dg_filter_rows_list)

    Me.AddRow(CreditLineMovements.CreditLineMovementType.GetMarker, _dg_filter_rows_list)
    Me.AddRow(CreditLineMovements.CreditLineMovementType.GetMarkerChips, _dg_filter_rows_list)
    Me.AddRow(CreditLineMovements.CreditLineMovementType.GetMarkerCardReplacement, _dg_filter_rows_list)
    Me.AddRow(CreditLineMovements.CreditLineMovementType.Payback, _dg_filter_rows_list)

    Return _dg_filter_rows_list
  End Function ' GetMovTypesFilterGridData

  ''' <summary>
  ''' Add new row
  ''' </summary>
  ''' <param name="Movement"></param>
  ''' <param name="FilterRowList"></param>
  ''' <remarks></remarks>
  Private Sub AddRow(ByVal Movement As CreditLineMovements.CreditLineMovementType, ByRef FilterRowList As List(Of TYPE_DG_FILTER_ELEMENT))
    Dim _row As TYPE_DG_FILTER_ELEMENT
    _row.code = Movement
    _row.description = CreditLineMovements.MovementTypeToString(Movement)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    FilterRowList.Add(_row)
  End Sub ' AddRow

  ''' <summary>
  ''' Add new Header
  ''' </summary>
  ''' <param name="Header"></param>
  ''' <param name="FilterRowList"></param>
  ''' <remarks></remarks>
  Private Sub AddHeader(ByVal Header As String, ByRef FilterRowList As List(Of TYPE_DG_FILTER_ELEMENT))
    Dim _row As TYPE_DG_FILTER_ELEMENT
    _row.code = -1
    _row.description = Header
    _row.elem_type = GRID_2_ROW_TYPE_HEADER
    FilterRowList.Add(_row)
  End Sub ' AddHeader


  ''' <summary>
  ''' Get credit lines movements
  ''' </summary>
  ''' <param name="DateFrom"></param>
  ''' <param name="DateTo"></param>
  ''' <param name="Movements"></param>
  ''' <param name="Account"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function getCreditLinesMovements(ByVal DateFrom As uc_date_picker, ByVal DateTo As uc_date_picker, ByVal Movements As String, ByVal Account As uc_account_sel, ByVal SiteSel As uc_sites_sel) As String

    Dim _sql_str As New StringBuilder
    Dim _account_sql As String = ""

    _sql_str.AppendLine(" SELECT   CL_ACCOUNT_ID                              ")
    _sql_str.AppendLine("        , ACC.AC_HOLDER_NAME                         ")
    _sql_str.AppendLine("        , CLM_ID                                     ")
    _sql_str.AppendLine("        , CLM_OPERATION_ID                           ")
    _sql_str.AppendLine("        , CLM_TYPE                                   ")
    _sql_str.AppendLine("        , CLM_OLD_VALUE                              ")
    _sql_str.AppendLine("        , CLM_NEW_VALUE                              ")
    _sql_str.AppendLine("        , CLM_CREATION                               ")
    _sql_str.AppendLine("        , CLM_CREATION_USER                          ")

    If (m_is_center) Then
      _sql_str.AppendLine("      , CLM_SITE_ID                                ")
    Else
      _sql_str.AppendLine("      , NULL AS CLM_SITE_ID                        ")
    End If

    _sql_str.AppendLine("       FROM   CREDIT_LINE_MOVEMENTS CLM              ")
    _sql_str.AppendLine(" INNER JOIN   CREDIT_LINES CL                        ")
    _sql_str.AppendLine("         ON   CL.CL_ID = CLM.CLM_CREDIT_LINE_ID      ")

    If (m_is_center) Then
      _sql_str.AppendLine("AND CL_SITE_ID = CLM_SITE_ID")
    End If

    _sql_str.AppendLine(" INNER JOIN   ACCOUNTS ACC                           ")
    _sql_str.AppendLine("ON CL.CL_ACCOUNT_ID = ACC.AC_ACCOUNT_ID              ")


    _account_sql = Account.GetFilterSQL

    If m_is_center OrElse (DateFrom.Checked) OrElse _
      (DateTo.Checked) OrElse _
      (Not String.IsNullOrEmpty(Movements)) OrElse _
      _account_sql <> "" Then
      _sql_str.AppendLine("WHERE")
      _sql_str.AppendLine(BuildWhere(DateFrom, DateTo, Movements, _account_sql, SiteSel))
    End If

    _sql_str.AppendLine("ORDER BY CLM_CREATION DESC, ACC.AC_HOLDER_NAME DESC")

    Return _sql_str.ToString

  End Function ' getCreditLinesMovements

#End Region ' Public Methods

#Region " Private Functions "


#Region " Manage xml "

  ''' <summary>
  ''' Xml values to data.
  ''' </summary>
  ''' <param name="Xml"></param>
  ''' <param name="Type"></param>
  ''' <param name="Report"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function XmlToData(Xml As String, Type As CreditLineMovements.CreditLineMovementType, Optional Report As Boolean = True) As String
    Dim _xml_data As New XmlDocument
    Dim _sb As StringBuilder
    Dim _value As Object
    Dim _str_value As String
    Dim _appends As List(Of String)

    _appends = New List(Of String)
    _sb = New StringBuilder()

    Try

      If (String.IsNullOrEmpty(Xml)) Then
        Return String.Empty
      End If

      _xml_data.LoadXml(Xml)

      _str_value = String.Empty
      _value = Nothing

      If (Report) Then
        For Each _xml_node As XmlNode In _xml_data.FirstChild.ChildNodes

          If (Type = CreditLineMovements.CreditLineMovementType.Create AndAlso _xml_node.Name <> CLASS_CREDITLINE.MovementsNodeXML.creditLimit.ToString()) Then
            Continue For
          End If

          Select Case _xml_node.Name

            Case CLASS_CREDITLINE.MovementsNodeXML.creditLimit.ToString(),
                 CLASS_CREDITLINE.MovementsNodeXML.TTO.ToString(),
                 CLASS_CREDITLINE.MovementsNodeXML.spent.ToString(),
                 CLASS_CREDITLINE.MovementsNodeXML.getMarker.ToString(),
                 CLASS_CREDITLINE.MovementsNodeXML.payback.ToString()

              If Not (_xml_node Is Nothing Or String.IsNullOrEmpty(_xml_node.InnerText)) Then
                Decimal.TryParse(_xml_node.InnerText, _value)
                _str_value = GUI_FormatCurrency(_value)
              End If

            Case CLASS_CREDITLINE.MovementsNodeXML.expirationDate.ToString(),
                 CLASS_CREDITLINE.MovementsNodeXML.createDate.ToString()

              If Not (_xml_node Is Nothing Or String.IsNullOrEmpty(_xml_node.InnerText)) Then
                _str_value = GUI_FormatDate(_xml_node.InnerText, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
              End If

              _str_value = _xml_node.InnerText

            Case CLASS_CREDITLINE.MovementsNodeXML.status.ToString()

              If Not (_xml_node Is Nothing Or String.IsNullOrEmpty(_xml_node.InnerText)) Then
                Int32.TryParse(_xml_node.InnerText, _value)
                _str_value = CreditLine.StatusToString(_value)
              End If

            Case CLASS_CREDITLINE.MovementsNodeXML.iban.ToString()

              If Not (_xml_node Is Nothing) Then
                _str_value = _xml_node.InnerText
              End If

            Case CLASS_CREDITLINE.MovementsNodeXML.signers.ToString()

              For Each _xml_signer As XmlNode In _xml_node.SelectNodes("//signers/signer/Name")
                If Not (_xml_signer Is Nothing) Then
                  _appends.Add(_xml_signer.InnerText)
                End If
              Next
              If (_appends.Count > 0) Then
                _str_value = String.Join(", ", _appends.ToArray)
              End If

          End Select

          _sb.Append(_str_value)

        Next
      End If

    Catch _ex As Exception
      Log.Error(_ex.Message())
    End Try

    Return _sb.ToString()

  End Function ' XmlToData


  ''' <summary>
  ''' Parse Xml to add creation rows.
  ''' </summary>
  ''' <param name="Xml"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function XmlToRow(Xml As String) As List(Of NEW_ROW_GRID)
    Dim _xml_data As New XmlDocument
    Dim _value As Object
    Dim _str_value As String
    Dim _list As List(Of NEW_ROW_GRID)
    Dim _new_item As NEW_ROW_GRID

    Dim _appends As List(Of String)
    _appends = New List(Of String)

    _list = New List(Of NEW_ROW_GRID)()


    If (String.IsNullOrEmpty(Xml)) Then
      Return _list
    End If

    Try
      _xml_data.LoadXml(Xml)
      _str_value = String.Empty
      _value = Nothing

      For Each _xml_node As XmlNode In _xml_data.FirstChild.ChildNodes
        _new_item = New NEW_ROW_GRID()
        _new_item.type = NodeXMLToMovementType(_xml_node.Name)
        _new_item.type_description = CreditLineMovements.MovementTypeToString(_new_item.type)

        Select Case _xml_node.Name

          Case CLASS_CREDITLINE.MovementsNodeXML.creditLimit.ToString(),
               CLASS_CREDITLINE.MovementsNodeXML.TTO.ToString(),
               CLASS_CREDITLINE.MovementsNodeXML.getMarker.ToString(),
               CLASS_CREDITLINE.MovementsNodeXML.payback.ToString()

            If Not (_xml_node Is Nothing Or String.IsNullOrEmpty(_xml_node.InnerText)) Then
              Decimal.TryParse(_xml_node.InnerText, _value)

              _new_item.value = GUI_FormatCurrency(_value)
              _list.Add(_new_item)
            End If

          Case CLASS_CREDITLINE.MovementsNodeXML.expirationDate.ToString()
            If Not (_xml_node Is Nothing Or String.IsNullOrEmpty(_xml_node.InnerText)) Then
              _new_item.value = _xml_node.InnerText
              _list.Add(_new_item)
            End If
          Case CLASS_CREDITLINE.MovementsNodeXML.createDate.ToString(),
            CLASS_CREDITLINE.MovementsNodeXML.spent.ToString()
            'Not Inserted
          Case CLASS_CREDITLINE.MovementsNodeXML.status.ToString()

            If Not (_xml_node Is Nothing Or String.IsNullOrEmpty(_xml_node.InnerText)) Then
              Int32.TryParse(_xml_node.InnerText, _value)
              _new_item.value = CreditLine.StatusToString(_value)
              _list.Add(_new_item)

            End If

          Case CLASS_CREDITLINE.MovementsNodeXML.iban.ToString()

            If Not (_xml_node Is Nothing) Then
              _new_item.value = _xml_node.InnerText
              _list.Add(_new_item)
            End If

          Case CLASS_CREDITLINE.MovementsNodeXML.signers.ToString()

            For Each _xml_signer As XmlNode In _xml_node.SelectNodes("//signers/signer/Name")
              If Not (_xml_signer Is Nothing) Then
                _appends.Add(_xml_signer.InnerText)
              End If
            Next
            If (_appends.Count > 0) Then
              _new_item.value = String.Join(", ", _appends.ToArray)
              _list.Add(_new_item)
            End If

        End Select

      Next

    Catch ex As Exception

    End Try

    Return _list
  End Function ' XmlToRow

  ''' <summary>
  ''' XML node to movementType.
  ''' </summary>
  ''' <param name="XmlNode"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function NodeXMLToMovementType(ByVal XmlNode As String) As CreditLineMovements.CreditLineMovementType

    Dim _movement_type As CreditLineMovements.CreditLineMovementType

    Select Case XmlNode
      Case CLASS_CREDITLINE.MovementsNodeXML.creditLimit.ToString()
        _movement_type = CreditLineMovements.CreditLineMovementType.UpdateLimit

      Case CLASS_CREDITLINE.MovementsNodeXML.TTO.ToString()
        _movement_type = CreditLineMovements.CreditLineMovementType.UpdateTTO
      Case CLASS_CREDITLINE.MovementsNodeXML.spent.ToString()
        _movement_type = CreditLineMovements.CreditLineMovementType.UpdateTTO
      Case CLASS_CREDITLINE.MovementsNodeXML.getMarker.ToString()
        _movement_type = CreditLineMovements.CreditLineMovementType.GetMarker

      Case CLASS_CREDITLINE.MovementsNodeXML.payback.ToString()
        _movement_type = CreditLineMovements.CreditLineMovementType.Payback

      Case CLASS_CREDITLINE.MovementsNodeXML.expirationDate.ToString()
        _movement_type = CreditLineMovements.CreditLineMovementType.UpdateExpiration

      Case CLASS_CREDITLINE.MovementsNodeXML.status.ToString()
        _movement_type = CreditLineMovements.CreditLineMovementType.UpdateStatus
      Case CLASS_CREDITLINE.MovementsNodeXML.iban.ToString()
        _movement_type = CreditLineMovements.CreditLineMovementType.UpdateIBAN
      Case CLASS_CREDITLINE.MovementsNodeXML.signers.ToString()
        _movement_type = CreditLineMovements.CreditLineMovementType.UpdateSigners
    End Select

    Return _movement_type
  End Function ' NodeXMLToMovementType

#End Region ' Manage xml


  ''' <summary>
  ''' Add and set credit line action
  ''' </summary>
  ''' <param name="CreditLineActions"></param>
  ''' <param name="CreationUser"></param>
  ''' <param name="MovementType"></param>
  ''' <param name="XmlOldValue"></param>
  ''' <param name="XmlNewValue"></param>
  ''' <remarks></remarks>
  Private Sub SetCreditLineAction(ByRef CreditLineActions As CreditLineActions, ByVal CreationUser As String, _
                                  ByVal MovementType As CreditLineMovements.CreditLineMovementType, ByVal XmlOldValue As String, _
                                  ByVal XmlNewValue As String)

    Dim _credit_line_action As CreditLineAction

    _credit_line_action = New CreditLineAction
    _credit_line_action.CreationUser = CreationUser
    _credit_line_action.MovementType = MovementType
    _credit_line_action.OldValue = XmlOldValue
    _credit_line_action.NewValue = XmlNewValue

    CreditLineActions.Actions.Add(_credit_line_action)

  End Sub ' SetCreditLineAction

  ''' <summary>
  ''' BuildWhere for filters.
  ''' </summary>
  ''' <param name="DateFrom"></param>
  ''' <param name="DateTo"></param>
  ''' <param name="Movements"></param>
  ''' <param name="AccountSql"></param>
  ''' <param name="SitesSel"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function BuildWhere(DateFrom As uc_date_picker, DateTo As uc_date_picker, Movements As String, AccountSql As String, SitesSel As uc_sites_sel) As String

    Dim _where As StringBuilder
    Dim _index As Integer
    Dim _clause_added As Boolean

    _where = New StringBuilder
    _index = 0
    _clause_added = False

    If DateFrom.Checked Then
      _where.AppendLine("CLM_CREATION >= " & GUI_FormatDateDB(DateFrom.Value))
      _clause_added = True
    End If

    If DateTo.Checked Then
      If _clause_added Then
        _where.AppendLine("AND")
      End If
      _where.AppendLine("CLM_CREATION < " & GUI_FormatDateDB(DateTo.Value))
      _clause_added = True
    End If

    If (Not String.IsNullOrEmpty(Movements)) Then
      If _clause_added Then
        _where.AppendLine("AND")
      End If
      _where.AppendLine("CLM_TYPE IN (" & Movements & ")")
      _clause_added = True
    End If

    If (AccountSql <> "") Then
      If _clause_added Then
        _where.AppendLine("AND")
      End If
      _where.AppendLine(AccountSql)
      _clause_added = True
    End If

    If (m_is_center AndAlso Not String.IsNullOrEmpty(SitesSel.GetSitesIdListSelectedAll())) Then
      If _clause_added Then
        _where.AppendLine("AND")
      End If
      _where.AppendLine("CLM_SITE_ID IN (" & SitesSel.GetSitesIdListSelectedAll() & ")")
    End If

    Return _where.ToString

  End Function ' BuildWhere

#End Region ' Private Functions

End Class