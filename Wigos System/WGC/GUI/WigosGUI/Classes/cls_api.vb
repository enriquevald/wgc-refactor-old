'-------------------------------------------------------------------
' Copyright © 2016 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_api.vb
' DESCRIPTION:   Site class for site edition screen
' AUTHOR:        Gustavo ALi / Sergio Soria
' CREATION DATE: 09-SEPT-2016
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 09-SEPT-2016  GA SDS    Initial version.
'
'-------------------------------------------------------------------
Imports System.Net
Imports System.IO
Imports Newtonsoft.Json.Linq
Imports Newtonsoft.Json

Public Class CLS_API
  Public _url As String
  Public _tbl As DataTable

#Region " Constants "

  ' ACTIONS
  Public Const API_GET As String = "GET"
  Public Const API_POST As String = "POST"
  Public Const API_PUT As String = "PUT"
  Public Const API_DELETE As String = "DELETE"
  Public Const API_CONTENT_TYPE As String = "application/json"

#End Region ' Constants
  Public Sub New(ByVal url As String, ByRef tbl As DataTable)
    _url = url
    _tbl = tbl
  End Sub
  Public Sub New(ByVal url As String)
    _url = url
    _tbl = New DataTable
  End Sub
  '----------------------------------------------------------------------------
  ' PURPOSE: Reads from the API
  '
  ' PARAMS:
  '   - INPUT:
  '     - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '   - OUTPUT: None
  '     -_tbl: charge Datatable
  '
  ' RETURNS:
  '     - responseServer

  '
  ' NOTES:
  Public Function getData() As String
    Dim _responseFromServer As String
    Try
      Dim _request As WebRequest = WebRequest.Create(_url)
      Dim _response As WebResponse
      Dim _datastream As Stream
      Dim _reader As StreamReader

      _request.Method = API_GET
      _response = _request.GetResponse()
      _datastream = _response.GetResponseStream()
      _reader = New StreamReader(_datastream)
      _responseFromServer = _reader.ReadToEnd()
      _reader.Close()
      _datastream.Close()
      _response.Close()
    Catch ex As Exception
      'SDS 2017-12-27  Winstats Aplicattions: Status & Version
      If ex.Message.Contains("500") Then
        Return "not accesible"
      Else
        Return "error"
      End If
    End Try
    Return _responseFromServer

  End Function
  '----------------------------------------------------------------------------
  ' PURPOSE: insert to API controller 
  '
  ' PARAMS:
  '   - INPUT:
  '     - head: byteArray with fields.
  '
  '   - OUTPUT: None
  '     -_tbl: charge Datatable
  '
  ' RETURNS:
  '     - responseServer

  '
  ' NOTES:
  Public Function postData(head As Byte()) As String
    Dim _request As WebRequest = WebRequest.Create(_url)
    Dim _response As WebResponse
    Dim _datastream As Stream
    Dim _reader As StreamReader
    Dim _responseFromServer As String

    _request = WebRequest.Create(_url)
    _request.Method = API_POST
    _request.ContentType = API_CONTENT_TYPE
    _request.ContentLength = head.Length
    _datastream = _request.GetRequestStream()
    _datastream.Write(head, 0, head.Length)
    _datastream.Close()
    _response = _request.GetResponse()
    _datastream = _response.GetResponseStream()
    _reader = New StreamReader(_datastream)
    _responseFromServer = _reader.ReadToEnd()
    _reader.Close()
    _datastream.Close()
    _response.Close()
    Return _responseFromServer
  End Function
  '----------------------------------------------------------------------------
  ' PURPOSE: update to API controller 
  '
  ' PARAMS:
  '   - INPUT:
  '     - head: byteArray with fields.
  '
  '   - OUTPUT: None
  '     -_tbl: charge Datatable 
  '
  ' RETURNS:
  '     - responseServer

  '
  ' NOTES:
  Public Function putData(head As Byte()) As String
    Dim _request As WebRequest = WebRequest.Create(_url)
    Dim _response As WebResponse
    Dim _datastream As Stream
    Dim _reader As StreamReader
    Dim _responseFromServer As String


    _request.Method = API_PUT
    _request.ContentType = API_CONTENT_TYPE
    _request.ContentLength = head.Length
    _datastream = _request.GetRequestStream()
    _datastream.Write(head, 0, head.Length)
    _datastream.Close()
    _response = _request.GetResponse()
    _datastream = _response.GetResponseStream()
    _reader = New StreamReader(_datastream)
    _responseFromServer = _reader.ReadToEnd()
    _reader.Close()
    _datastream.Close()
    _response.Close()
    Return _responseFromServer
  End Function
  '----------------------------------------------------------------------------
  ' PURPOSE: delete to API controller 
  '
  ' PARAMS:
  '   - INPUT:
  '
  '   - OUTPUT: None
  '     -_tbl: charge Datatable 
  '
  ' RETURNS:
  '     - responseServer

  '
  ' NOTES:
  Public Function deleteData() As String
    Dim _request As WebRequest = WebRequest.Create(_url)
    Dim _response As WebResponse
    Dim _datastream As Stream
    Dim _reader As StreamReader
    Dim _responseFromServer As String


    _request.Method = API_DELETE
    _response = _request.GetResponse()
    _datastream = _response.GetResponseStream()
    _reader = New StreamReader(_datastream)
    _responseFromServer = _reader.ReadToEnd()
    _reader.Close()
    _datastream.Close()
    _response.Close()
    Return _responseFromServer
  End Function
  '----------------------------------------------------------------------------
  ' PURPOSE: convert string to ByteArray
  '
  ' PARAMS:
  '   - INPUT:
  '     - texto: string JSON
  '
  '   - OUTPUT: None

  '
  ' RETURNS:
  '     - ByteArray

  '
  ' NOTES:
  Public Function JsonToByte(texto As String) As Byte()
    Return System.Text.Encoding.UTF8.GetBytes(texto)
  End Function
  '----------------------------------------------------------------------------
  ' PURPOSE: select an itemArray from JSON schema 
  '
  ' PARAMS:
  '   - INPUT:
  '     - response: string JSON
  '     - item: head of schema
  '
  '   - OUTPUT: None
  '     -_tbl: charge Datatable result
  '
  ' RETURNS:
  '     - responseServer

  '
  ' NOTES:
  Public Function parseData(response As String, item As String) As DataTable
    Dim _jObj As JObject
    Dim _response As String

    _jObj = JObject.Parse(response)
    Dim jChilds = _jObj.Item(item)
    _response = jChilds.ToString()
    _tbl = JsonConvert.DeserializeObject(Of DataTable)(_response)
    Return _tbl
  End Function

  Public Function responseToArray(resp As String) As String
    Dim _jArr As New JArray
    Dim _jObj As JObject

    _jObj = JObject.Parse(resp)
    _jArr.Add(_jObj)
    Return _jArr.ToString
  End Function

  Public Function clearChilds(_jObj As JObject, item As String) As JObject

    Dim _childs As JArray
    _childs = _jObj.Item(item)
    If _childs.Count = 0 Then
      _jObj.Remove(item)
    Else
      For Each elem As JObject In _childs
        elem = clearChilds(elem, item)
      Next
      _jObj.Remove(item)
      _jObj.Add(item, _childs)
    End If
    Return _jObj
  End Function

  Public Function sanitizeData(response As String) As String
    Dim result As String
    Dim fc As String = response.Substring(0, 1)

    If fc = "{" Then
      result = sanitizeData(JObject.Parse(response)).ToString
    ElseIf fc = "[" Then
      result = sanitizeData(JArray.Parse(response)).ToString
    Else
      result = "{error:'No es JSON'}"
    End If
    Return result
  End Function

  Public Function sanitizeData(response As JObject) As JObject
    Dim _jArr As JArray
    Dim _jObjClone As New JObject()
    Dim _jArrClone As JArray
    For Each item As JProperty In response.Children()
      If item.Value.GetType.Name = "JArray" Then
        _jArr = DirectCast(item.Value, JArray)
        If _jArr.Count <> 0 Then
          _jArrClone = sanitizeData(_jArr)
          _jObjClone.Add(item.Name, _jArrClone)
        End If
      Else
        _jObjClone.Add(item.Name, item.Value)
      End If
    Next
    Return _jObjClone
  End Function

  Public Function sanitizeData(response As JArray) As JArray
    Dim _jArrClone As New JArray
    _jArrClone = New JArray()
    For Each elem As JObject In response.Children()
      _jArrClone.Add(sanitizeData(elem))
    Next
    Return _jArrClone
  End Function

  Public Function removeItems(response As JArray, items As String()) As JArray
    Dim _jarray As New JArray

    For Each elem As JObject In response.Children()
      For Each item As String In items
        elem.Remove(item)
      Next
      _jarray.Add(elem)
    Next
    Return _jarray
  End Function

  Public Function cloneItems(response As JArray, items As String()) As JArray
    Dim _jarray As New JArray
    Dim _jObj As New JObject

    For Each elem As JObject In response.Children()
      For Each item As String In items
        _jObj.Add(elem.Item(item))
      Next
      _jarray.Add(_jObj)
    Next
    Return _jarray
  End Function
End Class
