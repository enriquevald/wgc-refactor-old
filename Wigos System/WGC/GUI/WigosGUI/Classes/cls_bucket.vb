﻿'-------------------------------------------------------------------
' Copyright © 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_bucket.vb
'
' DESCRIPTION : Bucket class for user edition
'              
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 11-DIC-2015  SGB    Initial version
' 17-MAR-2016  JRC    Bug 10734. If Not enabled then avoid validations
' 05-ABR-2016  JRC    Bug 10764:Buckets: no se audita la configuración
' 22-SEP-2016  FJC    Fixed Bug 18019:PromoBOX: no se muestran los puntos negativos de la cuenta de un cliente
' 04-NOV-2016  ATB    Bug 16319:Buckets: error en el nivel que aparece en la auditoría
'-------------------------------------------------------------------

Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_CommonMisc
Imports WSI.Common
Imports WSI.Common.Promotion
Imports WSI.Common.Buckets
Imports System.Text

Public Class CLASS_BUCKET
  Inherits CLASS_BASE

#Region " Constants "

  Public Const DATE_NULL As String = "1900/01/01 00:00:00"
  Public Const MAX_LEVELS As Integer = 4

#End Region

#Region " Structures "

  Public Class TYPE_BUCKET_LEVEL_PARAMS
    Public Param_A As Decimal
    Public Param_B As Decimal
  End Class



  Public Class TYPE_BUCKET

    Public bucket_id As Int64
    Public bucket_name As String
    Public enabled As Boolean
    Public system_type As Buckets.BucketSystemType
    Public bucket_type As Buckets.BucketType
    Public order_on_report As Int32
    Public expiration_days As Int32
    Public expiration_date As String
    Public lcd As Boolean
    Public cashier As Boolean
    Public promobox As Boolean
    Public level_flags As Boolean
    Public params_by_level As Dictionary(Of Integer, TYPE_BUCKET_LEVEL_PARAMS)
    Public param_K As Boolean
  End Class



#End Region 'Structures

#Region " Members "

  Protected m_bucket As New TYPE_BUCKET

#End Region

#Region " Properties "

  Public Property BucketId() As Int64
    Get
      Return m_bucket.bucket_id
    End Get
    Set(value As Int64)
      m_bucket.bucket_id = value
    End Set
  End Property

  Public Property BucketName() As String
    Get
      Return m_bucket.bucket_name
    End Get
    Set(value As String)
      m_bucket.bucket_name = value
    End Set
  End Property

  Public Property Enabled() As Boolean
    Get
      Return m_bucket.enabled
    End Get
    Set(value As Boolean)
      m_bucket.enabled = value
    End Set
  End Property

  Public Property SystemType() As Buckets.BucketSystemType
    Get
      Return m_bucket.system_type
    End Get
    Set(value As Buckets.BucketSystemType)
      m_bucket.system_type = value
    End Set
  End Property

  Public Property BucketType() As Buckets.BucketType
    Get
      Return m_bucket.bucket_type
    End Get
    Set(value As Buckets.BucketType)
      m_bucket.bucket_type = value
    End Set
  End Property

  Public Property OrderOnReport() As Int32
    Get
      Return m_bucket.order_on_report
    End Get
    Set(value As Int32)
      m_bucket.order_on_report = value
    End Set
  End Property

  Public Property ExpirationDays() As Int32
    Get
      Return m_bucket.expiration_days
    End Get
    Set(value As Int32)
      m_bucket.expiration_days = value
    End Set
  End Property

  Public Property ExpirationDate() As String
    Get
      Return m_bucket.expiration_date
    End Get
    Set(value As String)
      m_bucket.expiration_date = value
    End Set
  End Property


  Public Property LCD() As Boolean
    Get
      Return m_bucket.lcd
    End Get
    Set(value As Boolean)
      m_bucket.lcd = value
    End Set
  End Property

  Public Property PromoBox() As Boolean
    Get
      Return m_bucket.promobox
    End Get
    Set(value As Boolean)
      m_bucket.promobox = value
    End Set
  End Property

  Public Property Cashier() As Boolean
    Get
      Return m_bucket.cashier
    End Get
    Set(value As Boolean)
      m_bucket.cashier = value
    End Set
  End Property

  Public Property Level_flags() As Boolean
    Get
      Return m_bucket.level_flags
    End Get
    Set(value As Boolean)
      m_bucket.level_flags = value
    End Set
  End Property

  Public ReadOnly Property Params_By_Level As Dictionary(Of Integer, TYPE_BUCKET_LEVEL_PARAMS)
    Get
      Return m_bucket.params_by_level
    End Get
  End Property

  Public Property Param_K() As Boolean
    Get
      Return m_bucket.param_K
    End Get
    Set(value As Boolean)
      m_bucket.param_K = value
    End Set
  End Property




#End Region

#Region " Overrides functions "

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As ENUM_STATUS

    Return ReadBucket(ObjectId, SqlCtx)

  End Function ' DB_Read

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As ENUM_STATUS

    Return UpdateBucket(SqlCtx)

  End Function ' DB_Update

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As ENUM_STATUS
    Dim _rc As Integer

    ' Make sure that stacker with same id does not already exist
    'If ExistsDuplicateStakers(1) Then

    '  Return ENUM_STATUS.STATUS_DUPLICATE_KEY
    'End If

    '' Insert stacker data
    '_rc = InsertStacker(SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_PASSWORD
        Return CLASS_BASE.ENUM_STATUS.STATUS_PASSWORD

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Insert

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

  End Function ' DB_Delete

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE
    Dim _tmp_bucket As CLASS_BUCKET
    Dim _level_param As TYPE_BUCKET_LEVEL_PARAMS

    _tmp_bucket = New CLASS_BUCKET

    Try
      _tmp_bucket.m_bucket.bucket_id = m_bucket.bucket_id
      _tmp_bucket.m_bucket.bucket_name = m_bucket.bucket_name
      _tmp_bucket.m_bucket.enabled = m_bucket.enabled
      _tmp_bucket.m_bucket.system_type = m_bucket.system_type
      _tmp_bucket.m_bucket.bucket_type = m_bucket.bucket_type
      _tmp_bucket.m_bucket.order_on_report = m_bucket.order_on_report
      _tmp_bucket.m_bucket.expiration_days = m_bucket.expiration_days
      _tmp_bucket.m_bucket.expiration_date = m_bucket.expiration_date
      _tmp_bucket.m_bucket.lcd = m_bucket.lcd
      _tmp_bucket.m_bucket.promobox = m_bucket.promobox
      _tmp_bucket.m_bucket.cashier = m_bucket.cashier
      _tmp_bucket.m_bucket.level_flags = m_bucket.level_flags
      _tmp_bucket.m_bucket.params_by_level = New Dictionary(Of Integer, TYPE_BUCKET_LEVEL_PARAMS)

      For _idx As Integer = 1 To m_bucket.params_by_level.Count
        _level_param = New TYPE_BUCKET_LEVEL_PARAMS
        _level_param.Param_A = m_bucket.params_by_level(_idx).Param_A
        _level_param.Param_B = m_bucket.params_by_level(_idx).Param_B
        _tmp_bucket.m_bucket.params_by_level.Add(_idx, _level_param)
      Next
      _tmp_bucket.m_bucket.param_K = m_bucket.param_K
    Catch _ex As Exception

    End Try

    Return _tmp_bucket

  End Function ' Duplicate

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _yes_text As String
    Dim _no_text As String

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)


    _yes_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
    _no_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)

    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(6968), Me.BucketId)       '"BucketId"

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(253), Me.BucketName)       '"BucketName"

    If Me.Enabled Then
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6973), _yes_text)      '"Habilitado"
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6973), _no_text)       '"Habilitado"
    End If

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6972), Me.BucketType)    '"Tipo"

    Select Case Me.BucketType '"Tipo bucket"
      Case BucketType.RedemptionPoints
        Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(7000), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7028))

      Case BucketType.RankingLevelPoints
        Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(7000), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7029))

      Case BucketType.Credit_NR
        Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(7000), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7030))

      Case BucketType.Credit_RE
        Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(7000), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7031))

      Case BucketType.Comp1_RedemptionPoints
        Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(7000), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7032))

      Case BucketType.Comp2_Credit_NR
        Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(7000), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7033))

      Case BucketType.Comp3_Credit_RE
        Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(7000), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7034))

      Case Else
        Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(7000), AUDIT_NONE_STRING)
    End Select

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7001), Me.OrderOnReport) '"Posición en reporte"

    'visible

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7003), Me.ExpirationDays & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7006))       '"Caducidad de buckets del cliente": XX "días sin actividad"
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7003), Me.ExpirationDate & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7009))       '"Caducidad de buckets del cliente": "a fecha" XXX

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7011), IIf(Me.LCD, _yes_text, _no_text)) '"LCD:"
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7012), IIf(Me.Cashier, _yes_text, _no_text)) '"Cajero:"
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7013), IIf(Me.PromoBox, _yes_text, _no_text)) '"Promobox:"

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7023), IIf(Me.Param_K, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7020), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7017))) '"K:"

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7014), IIf(Me.Level_flags, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7015), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7016)))  ' Por Niveles / Globales

    If Me.Level_flags Then
      'Dim AValue(3) As String
      'Dim BValue(3) As String

      For _idx As Integer = 1 To Me.Params_By_Level.Count
        ' ATB 04-NOV-2016
        Call _auditor_data.SetField(0, Me.Params_By_Level(_idx).Param_A, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7008) & " " & (_idx).ToString & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7007) & " : ")
        Call _auditor_data.SetField(0, Me.Params_By_Level(_idx).Param_B, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7008) & " " & (_idx).ToString & " " & IIf(Me.Param_K, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7020), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7017)) & " : ")

        'AValue(_idx) = Me.Params_By_Level(_idx).Param_A
        'BValue(_idx) = Me.Params_By_Level(_idx).Param_B
      Next
      'Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7021), AValue)
      'Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7022), BValue)


    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7021), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7016) & ": " & Me.Params_By_Level(1).Param_A)
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7022), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7016) & ": " & Me.Params_By_Level(1).Param_B)
    End If




    Return _auditor_data

  End Function ' AuditorData

#End Region ' Overrides functions

#Region " Private Functions and Methods "

  Private Function ReadBucket(ByVal ObjectId As Int64, _
                               ByVal Context As Int32) As ENUM_STATUS
    Dim _sb As StringBuilder

    Try

      _sb = New System.Text.StringBuilder()


      _sb.AppendLine(" SELECT   BU_BUCKET_ID               ")
      _sb.AppendLine("        , BU_NAME                    ")
      _sb.AppendLine("        , BU_SYSTEM_TYPE             ")
      _sb.AppendLine("        , BU_BUCKET_TYPE             ")
      _sb.AppendLine("        , BU_ENABLED                 ")
      _sb.AppendLine("        , BU_EXPIRATION_DAYS         ")
      _sb.AppendLine("        , BU_EXPIRATION_DATE         ")
      _sb.AppendLine("        , BU_VISIBLE_FLAGS           ")
      _sb.AppendLine("        , BU_ORDER_ON_REPORTS        ")
      _sb.AppendLine("        , BU_LEVEL_FLAGS             ")
      _sb.AppendLine("        , BU_K_FACTOR                ")
      _sb.AppendLine("   FROM   BUCKETS                    ")
      _sb.AppendLine("  WHERE   BU_BUCKET_ID = @pBucketId  ")


      Using _db_trx As New WSI.Common.DB_TRX()
        Using _sql_cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _sql_cmd.Parameters.Add("@pBucketId", SqlDbType.BigInt).Value = ObjectId
          Using _reader As SqlDataReader = _sql_cmd.ExecuteReader()
            If (Not _reader.Read) Then

              Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
            Else

              m_bucket.bucket_id = ObjectId
              m_bucket.bucket_name = _reader("BU_NAME")
              m_bucket.enabled = _reader("BU_ENABLED")
              m_bucket.system_type = _reader("BU_SYSTEM_TYPE")
              m_bucket.bucket_type = _reader("BU_BUCKET_TYPE")
              m_bucket.expiration_days = _reader("BU_EXPIRATION_DAYS")
              m_bucket.expiration_date = IIf(Not IsDBNull(_reader("BU_EXPIRATION_DATE")) AndAlso _reader("BU_EXPIRATION_DATE") <> "", _reader("BU_EXPIRATION_DATE"), Nothing)
              m_bucket.param_K = _reader("BU_K_FACTOR")
              m_bucket.order_on_report = IIf(IsDBNull(_reader("BU_ORDER_ON_REPORTS")), 0, _reader("BU_ORDER_ON_REPORTS"))
              m_bucket.level_flags = _reader("BU_LEVEL_FLAGS")
              Dim visible As Long
              If (Not IsDBNull(_reader("BU_VISIBLE_FLAGS"))) Then
                visible = _reader("BU_VISIBLE_FLAGS")
              End If

              m_bucket.lcd = IIf(IsDBNull(_reader("BU_VISIBLE_FLAGS")), False, Buckets.ReadDeviceVisibility(visible, BucketVisibility.LCD))
              m_bucket.promobox = IIf(IsDBNull(_reader("BU_VISIBLE_FLAGS")), False, Buckets.ReadDeviceVisibility(visible, BucketVisibility.Promobox))
              m_bucket.cashier = IIf(IsDBNull(_reader("BU_VISIBLE_FLAGS")), False, Buckets.ReadDeviceVisibility(visible, BucketVisibility.Cashier))

              Fill_bucket_level_list(m_bucket)

            End If
          End Using
        End Using
      End Using

    Catch _ex As Exception

      Return ENUM_STATUS.STATUS_ERROR
    End Try

    Return ENUM_STATUS.STATUS_OK
  End Function ' ReadStacker

  Private Function UpdateBucket(ByVal Context As Int32) As ENUM_STATUS
    Dim _sql_trx As SqlTransaction = Nothing
    Dim _sb As StringBuilder

    If Not GUI_BeginSQLTransaction(_sql_trx) Then

      Return ENUM_STATUS.STATUS_ERROR
    End If
    Try

      Dim baseValue As ULong = 0
      baseValue = Buckets.SetDeviceVisibility(baseValue, Buckets.BucketVisibility.LCD, Me.LCD)
      baseValue = Buckets.SetDeviceVisibility(baseValue, Buckets.BucketVisibility.Cashier, Me.Cashier)
      baseValue = Buckets.SetDeviceVisibility(baseValue, Buckets.BucketVisibility.Promobox, Me.PromoBox)

      _sb = New StringBuilder()

      _sb.AppendLine("UPDATE   buckets                                 ")
      _sb.AppendLine("   SET   bu_enabled          = @pEnabled         ")
      _sb.AppendLine("       , bu_name             = @pBucketName      ")
      _sb.AppendLine("       , bu_visible_flags    = @pVisibleFlags    ")
      _sb.AppendLine("       , bu_order_on_reports = @pOrderOnReport   ")
      _sb.AppendLine("       , bu_expiration_days  = @pExporationDays  ")
      _sb.AppendLine("       , bu_expiration_date =  @pExpirationDate  ")
      _sb.AppendLine("       , bu_level_flags      = @pLevelFlags      ")
      _sb.AppendLine("       , bu_k_factor         = @pKFactor         ")
      _sb.AppendLine(" WHERE   bu_bucket_id        = @pBucketId        ")

      _sb.AppendLine(" IF @pBucketId = 2                               ")
      _sb.AppendLine(" BEGIN                                           ")
      _sb.AppendLine("    UPDATE  buckets                              ")
      _sb.AppendLine("       SET  bu_enabled       = @pEnabled         ")
      _sb.AppendLine("     WHERE  bu_bucket_id IN (8, 9)               ")
      _sb.AppendLine(" END                                             ")

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), _sql_trx.Connection, _sql_trx)

        _cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).Value = m_bucket.enabled
        _cmd.Parameters.Add("@pBucketName", SqlDbType.NVarChar).Value = m_bucket.bucket_name
        _cmd.Parameters.Add("@pVisibleFlags", SqlDbType.BigInt).Value = baseValue
        _cmd.Parameters.Add("@pOrderOnReport", SqlDbType.Int).Value = m_bucket.order_on_report
        _cmd.Parameters.Add("@pExporationDays", SqlDbType.Int).Value = m_bucket.expiration_days
        _cmd.Parameters.Add("@pExpirationDate", SqlDbType.NVarChar).Value = IIf(m_bucket.expiration_date = Nothing, "", m_bucket.expiration_date)
        _cmd.Parameters.Add("@pLevelFlags", SqlDbType.Bit).Value = m_bucket.level_flags
        _cmd.Parameters.Add("@pKFactor", SqlDbType.Bit).Value = m_bucket.param_K
        _cmd.Parameters.Add("@pBucketId", SqlDbType.Int).Value = m_bucket.bucket_id

        If _cmd.ExecuteNonQuery() = 0 Then

          GUI_EndSQLTransaction(_sql_trx, False)

          Return ENUM_STATUS.STATUS_NOT_FOUND
        End If

        If UpdateBucketLevels(_sql_trx) = ENUM_STATUS.STATUS_ERROR Then
          Return ENUM_STATUS.STATUS_ERROR
        End If

      End Using

      If Not GUI_EndSQLTransaction(_sql_trx, True) Then

        Return ENUM_STATUS.STATUS_ERROR
      End If

    Catch _ex As Exception
      If Not GUI_EndSQLTransaction(_sql_trx, False) Then

        Return ENUM_STATUS.STATUS_ERROR
      End If
    End Try

    Return ENUM_STATUS.STATUS_OK
  End Function ' UpdateBucket

  Private Function Fill_bucket_level_list(ByRef m_bucket As TYPE_BUCKET)
    Dim _sb As StringBuilder
    Dim _level_param As TYPE_BUCKET_LEVEL_PARAMS
    Dim _level As Integer

    Try
      m_bucket.params_by_level = New Dictionary(Of Integer, TYPE_BUCKET_LEVEL_PARAMS)
      _sb = New System.Text.StringBuilder()

      _sb.AppendLine(" SELECT   BUL_LEVEL_ID               ")
      _sb.AppendLine("        , BUL_A_FACTOR               ")
      _sb.AppendLine("        , BUL_B_FACTOR               ")
      _sb.AppendLine("   FROM   BUCKET_LEVELS              ")
      _sb.AppendLine("  WHERE   BUL_BUCKET_ID = @pBucketId  ")


      Using _db_trx As New WSI.Common.DB_TRX()
        Using _sql_cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _sql_cmd.Parameters.Add("@pBucketId", SqlDbType.BigInt).Value = m_bucket.bucket_id
          Using _reader As SqlDataReader = _sql_cmd.ExecuteReader()

            If _reader.HasRows Then
              Do While _reader.Read()
                _level_param = New TYPE_BUCKET_LEVEL_PARAMS
                _level = _reader("BUL_LEVEL_ID")
                _level_param.Param_A = _reader("BUL_A_FACTOR")
                _level_param.Param_B = _reader("BUL_B_FACTOR")

                If (m_bucket.params_by_level.ContainsKey(_level)) Then
                  m_bucket.params_by_level.Remove(_level)
                  m_bucket.params_by_level.Add(_level, _level_param)
                Else
                  m_bucket.params_by_level.Add(_level, _level_param)
                End If
              Loop
            Else
              Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
            End If
          End Using
        End Using
      End Using

    Catch _ex As Exception

      Return ENUM_STATUS.STATUS_ERROR
    End Try
    Return ENUM_STATUS.STATUS_OK
  End Function



#End Region ' Private Functions and Methods

  Private Function UpdateBucketLevels(sql_trx As SqlTransaction)
    Dim _sb As StringBuilder
    Dim _sb2 As StringBuilder


    Try

      Dim baseValue As ULong = 0

      _sb = New StringBuilder()

      _sb.AppendLine("DELETE  FROM BUCKET_LEVELS                             ")
      _sb.AppendLine(" WHERE bul_bucket_id = @pBucketid                      ")


      'this update "marks" the record for multisite synch purposes

      _sb.AppendLine(" UPDATE  BUCKETS                                       ")
      _sb.AppendLine("    SET                                                ")
      _sb.AppendLine("          BU_NAME               = BU_NAME              ")
      _sb.AppendLine("         ,BU_ENABLED            = BU_ENABLED           ")
      _sb.AppendLine("         ,BU_SYSTEM_TYPE        = BU_SYSTEM_TYPE       ")
      _sb.AppendLine("         ,BU_BUCKET_TYPE        = BU_BUCKET_TYPE       ")
      _sb.AppendLine("         ,BU_VISIBLE_FLAGS      = BU_VISIBLE_FLAGS     ")
      _sb.AppendLine("         ,BU_ORDER_ON_REPORTS   = BU_ORDER_ON_REPORTS  ")
      _sb.AppendLine("         ,BU_EXPIRATION_DAYS    = BU_EXPIRATION_DAYS   ")
      _sb.AppendLine("         ,BU_EXPIRATION_DATE    = BU_EXPIRATION_DATE   ")
      _sb.AppendLine("         ,BU_LEVEL_FLAGS        = BU_LEVEL_FLAGS       ")
      _sb.AppendLine("         ,BU_K_FACTOR	       = BU_K_FACTOR	           ")
      _sb.AppendLine(" WHERE   BU_BUCKET_ID = @pBucketid                     ")






      'this update "marks" the record for multisite synch purposes

      _sb.AppendLine(" UPDATE  BUCKETS                                       ")
      _sb.AppendLine("    SET                                                ")
      _sb.AppendLine("          BU_NAME               = BU_NAME              ")
      _sb.AppendLine("         ,BU_ENABLED            = BU_ENABLED           ")
      _sb.AppendLine("         ,BU_SYSTEM_TYPE        = BU_SYSTEM_TYPE       ")
      _sb.AppendLine("         ,BU_BUCKET_TYPE        = BU_BUCKET_TYPE       ")
      _sb.AppendLine("         ,BU_VISIBLE_FLAGS      = BU_VISIBLE_FLAGS     ")
      _sb.AppendLine("         ,BU_ORDER_ON_REPORTS   = BU_ORDER_ON_REPORTS  ")
      _sb.AppendLine("         ,BU_EXPIRATION_DAYS    = BU_EXPIRATION_DAYS   ")
      _sb.AppendLine("         ,BU_EXPIRATION_DATE    = BU_EXPIRATION_DATE   ")
      _sb.AppendLine("         ,BU_LEVEL_FLAGS        = BU_LEVEL_FLAGS       ")
      _sb.AppendLine("         ,BU_K_FACTOR	       = BU_K_FACTOR	           ")
      _sb.AppendLine(" WHERE   BU_BUCKET_ID = @pBucketid                     ")





      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), sql_trx.Connection, sql_trx)
        _cmd.Parameters.Add("@pBucketid", SqlDbType.BigInt).Value = m_bucket.bucket_id
        Dim result As Integer = _cmd.ExecuteNonQuery()
      End Using

      _sb2 = New StringBuilder()

      _sb2.AppendLine(" INSERT INTO [bucket_levels ]                     ")
      _sb2.AppendLine("            (bul_bucket_id,                       ")
      _sb2.AppendLine("            bul_level_id,                         ")
      _sb2.AppendLine("            bul_a_factor,                         ")
      _sb2.AppendLine("            bul_b_factor)                         ")
      _sb2.AppendLine("            VALUES                                ")
      _sb2.AppendLine("            (@pBul_bucket_id,                     ")
      _sb2.AppendLine("            @pBul_level_id,                       ")
      _sb2.AppendLine("            @pBul_a_factor,                       ")
      _sb2.AppendLine("            @pBul_b_factor)                       ")
      _sb2.AppendLine("                                                  ")




      Using _cmd As New SqlClient.SqlCommand(_sb2.ToString(), sql_trx.Connection, sql_trx)
        _cmd.Parameters.Add("@pBul_bucket_id", SqlDbType.BigInt)
        _cmd.Parameters.Add("@pBul_level_id", SqlDbType.BigInt)
        _cmd.Parameters.Add("@pBul_a_factor", SqlDbType.Decimal)
        _cmd.Parameters.Add("@pBul_b_factor", SqlDbType.Decimal)





        For _idx As Integer = 1 To m_bucket.params_by_level.Count
          _cmd.Parameters("@pBul_bucket_id").Value = m_bucket.bucket_id
          _cmd.Parameters("@pBul_level_id").Value = _idx
          _cmd.Parameters("@pBul_a_factor").Value = m_bucket.params_by_level(_idx).Param_A
          _cmd.Parameters("@pBul_b_factor").Value = m_bucket.params_by_level(_idx).Param_B
          _cmd.ExecuteNonQuery()
        Next

      End Using

    Catch _ex As Exception
      If Not GUI_EndSQLTransaction(sql_trx, False) Then

        Return ENUM_STATUS.STATUS_ERROR
      End If
    End Try

    Return ENUM_STATUS.STATUS_OK
  End Function

End Class
