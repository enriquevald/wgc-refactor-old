'-------------------------------------------------------------------
' Copyright � 2009 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_licence
'
' DESCRIPTION : Class to manage licences
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 29-APR-2008  APB    Initial version
'--------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports WSI.Common

Public Class CLASS_LICENCE
  Inherits CLASS_BASE


#Region " Constants "

  Public Const MAX_PATH_LEN = 260

  Public Const MAX_LICENCE_LENGTH As Integer = 4096

#End Region ' Constants

#Region " GUI_Configuration.dll "

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_LICENCE
    Public control_block As Integer
    Public identifier As Integer
    Public insertion_date As DateTime
    Public expiration_date As DateTime
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_LICENCE_LENGTH)> _
    Public licence As String
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_PATH_LEN)> _
    Public filename As String

    Public Sub New()
      MyBase.New()
      control_block = Marshal.SizeOf(Me)
    End Sub
  End Class

#End Region 'Structures

#Region " Function Prototypes "

  'Public Declare Function CheckLicenseFromFile Lib "WSI.Common" (ByVal LicenceFile As String, _
  '                                                               ByRef LicenseNotValidAfter As Date) As Boolean

  'Public Declare Function Common_GetPackageFileDetails Lib "CommonBase" (ByVal pFileName As String, _
  '                                                                   <[In](), [Out]()> _
  '                                                                   ByVal SwPackageFile As CLASS_SW_PACKAGE.TYPE_SW_PACKAGE_FILE) As Integer

  'Public Declare Function Common_GetSwPackageFile Lib "CommonBase" (ByVal Index As Integer, _
  '                                                                  ByVal pFileName As String, _
  '                                                                  ByVal pVersion As String, _
  '                                                                  ByRef pFileSize As Integer) As Boolean

  'Public Declare Function Common_ExpandPackageFile Lib "CommonBase" (ByVal PackageFileName As String, _
  '                                                                  <[In](), [Out]()> _
  '                                                                   ByVal TemporaryPath As String) As Integer

  'Public Declare Function Common_CopyFileList Lib "CommonBase" (ByVal SourcePath As String, _
  '                                                              ByVal TargetPath As String) As Boolean

  'Public Declare Function Common_CleanPath Lib "CommonBase" (ByVal TargetPath As String, _
  '                                                           ByVal DeletePath As Boolean) As Boolean

  'Public Declare Function Common_FtpConnect Lib "CommonBase" (ByVal FtpHost As String, _
  '                                                            ByVal FtpUser As String, _
  '                                                            ByVal FtpPassword As String, _
  '                                                            ByRef SessionHandle As Integer, _
  '                                                            ByRef Context As Integer) As Boolean

  'Public Declare Function Common_FtpSetPath Lib "CommonBase" (ByVal SessionHandle As Integer, _
  '                                                            ByVal TargetPath As String) As Boolean

  'Public Declare Function Common_FtpPutFile Lib "CommonBase" (ByVal SessionHandle As Integer, _
  '                                                            ByRef Context As Integer, _
  '                                                            ByVal SourceFile As String) As Boolean

  'Public Declare Function Common_CleanFtpPath Lib "CommonBase" (ByVal SessionHandle As Integer, _
  '                                                              ByVal Path As String, _
  '                                                              ByVal DeletePath As Boolean) As Boolean

  'Public Declare Function Common_FtpRenamePath Lib "CommonBase" (ByVal SessionHandle As Integer, _
  '                                                               ByVal SourcePath As String, _
  '                                                               ByVal TargetPath As String) As Boolean

  'Public Declare Function Common_FtpDisconnect Lib "CommonBase" (ByVal SessionHandle As Integer) As Boolean

  'Public Declare Function Common_FtpGetFileDetails Lib "CommonBase" (ByVal SessionHandle As Integer, _
  '                                                                   ByRef SearchHandle As Integer, _
  '                                                                   ByVal Path As String, _
  '                                                                    <[In](), [Out]()> _
  '                                                                   ByVal ModuleInfo As CLASS_SW_PACKAGE.TYPE_SW_PACKAGE_MODULE) As Boolean

#End Region

#End Region ' GUI_Configuration.dll

#Region " Members "

  Protected m_licence As New TYPE_LICENCE

#End Region 'Members

#Region " Properties "

  Public Property Identifier() As Integer
    Get
      Return m_licence.identifier
    End Get

    Set(ByVal Value As Integer)
      m_licence.identifier = Value
    End Set
  End Property

  Public Property InsertionDate() As Date
    Get
      Return m_licence.insertion_date
    End Get

    Set(ByVal Value As Date)
      m_licence.insertion_date = Value
    End Set
  End Property

  Public Property ExpirationDate() As Date
    Get
      Return m_licence.expiration_date
    End Get

    Set(ByVal Value As Date)
      m_licence.expiration_date = Value
    End Set
  End Property

  Public Property Licence() As String
    Get
      Return m_licence.licence
    End Get

    Set(ByVal Value As String)
      m_licence.licence = Value
    End Set
  End Property

  Public Property FileName() As String
    Get
      Return m_licence.filename
    End Get

    Set(ByVal Value As String)
      m_licence.filename = Value
    End Set
  End Property

#End Region ' Properties

#Region " Overrides functions "

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer

    rc = ReadLicence(m_licence, SqlCtx)

    Select Case rc

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Read

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Return ENUM_STATUS.STATUS_NOT_SUPPORTED

  End Function ' DB_Update

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer

    m_licence.control_block = Marshal.SizeOf(m_licence)

    ' Insert user data
    rc = InsertLicence(m_licence, SqlCtx)

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Insert

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer

    m_licence.control_block = Marshal.SizeOf(m_licence)

    ' Delete user data
    rc = DeleteLicence(m_licence, SqlCtx)

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Delete

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim auditor_data As CLASS_AUDITOR_DATA

    ' Create a new auditor_code for Software Download
    auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_DOWNLOAD)
    Call auditor_data.SetName(GLB_NLS_GUI_SW_DOWNLOAD.Id(252), GUI_FormatDate(Me.m_licence.expiration_date, _
                                                                              ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                              ENUM_FORMAT_TIME.FORMAT_HHMM))

    Return auditor_data

  End Function ' AuditorData

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim temp_licence As CLASS_LICENCE

    temp_licence = New CLASS_LICENCE
    temp_licence.m_licence = m_licence

    Return temp_licence

  End Function 'Duplicate

#End Region 'Overrides functions

#Region " Private Functions "

  Private Function ReadLicence(ByRef Licence As TYPE_LICENCE, _
                               ByVal Context As Integer) As Integer
    Dim str_sql As String
    Dim data_table As DataTable

    str_sql = "select WL_INSERTION_DATE " _
                & " , WL_EXPIRATION_DATE " _
                & " , WL_LICENCE " _
             & " from LICENCES" _
            & " where WL_ID = " & Licence.identifier.ToString

    data_table = GUI_GetTableUsingSQL(str_sql, 5000)
    If IsNothing(data_table) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If data_table.Rows.Count() <> 1 Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    ' Parse the read data
    '     - Insertion Datetime
    Licence.insertion_date = data_table.Rows(0).Item("WL_INSERTION_DATE")
    Licence.expiration_date = data_table.Rows(0).Item("WL_EXPIRATION_DATE")
    Licence.licence = data_table.Rows(0).Item("WL_LICENCE")

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function ' ReadLicence

  Private Function UpdateLicence(ByVal SwPackage As TYPE_LICENCE, _
                                 ByVal Context As Integer) As Integer

    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function ' UpdateLicence

  Private Function InsertLicence(ByVal Licence As TYPE_LICENCE, _
                                   ByVal Context As Integer) As Integer

    Dim str_sql As String
    Dim SqlTrans As System.Data.SqlClient.SqlTransaction = Nothing

    If Not GUI_BeginSQLTransaction(SqlTrans) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    str_sql = "INSERT INTO   LICENCES " & _
                         " ( WL_EXPIRATION_DATE " & _
                         " , WL_LICENCE " & _
                         " ) " & _
                  " VALUES " & _
                         " ( " & GUI_FormatDateDB(Licence.expiration_date) & _
                         " , '" & Licence.licence & "'" & _
                         " )"

    If Not GUI_SQLExecuteNonQuery(str_sql, SqlTrans) Then
      GUI_EndSQLTransaction(SqlTrans, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If Not GUI_EndSQLTransaction(SqlTrans, True) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function ' InsertSwPackage

  Private Function DeleteLicence(ByVal Licence As TYPE_LICENCE, _
                                   ByVal Context As Integer) As Integer

    Dim str_sql As String
    Dim SqlTrans As System.Data.SqlClient.SqlTransaction = Nothing

    If Not GUI_BeginSQLTransaction(SqlTrans) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    str_sql = "DELETE FROM LICENCES " & _
                   " WHERE WL_ID = " & Licence.identifier

    If Not GUI_SQLExecuteNonQuery(str_sql, SqlTrans) Then
      GUI_EndSQLTransaction(SqlTrans, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If Not GUI_EndSQLTransaction(SqlTrans, True) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function ' DeleteSwPackage

#End Region 'Private Functions

#Region " Public Functions "

  ' PURPOSE : 
  '
  '  PARAMS:
  '     - INPUT:
  '         - FileName
  '         - SwPackage
  '
  '     - OUTPUT:
  '
  ' RETURNS : Version 

  Public Shared Function ReadLicenceFile(ByVal FileName As String, _
                                         ByRef LicenceText As String, _
                                         ByRef ExpirationDate As Date) As Boolean

    Dim expiration_date As Date
    Dim licence_text As String = ""

    If WSI.Common.Misc.CheckLicenseFromFile(FileName, licence_text, expiration_date) = False Then
      Return False
    Else
      LicenceText = licence_text
      ExpirationDate = expiration_date

      Return True
    End If

  End Function ' ReadLicenceFile

#End Region ' Public Functions

End Class ' CLASS_LICENCE
