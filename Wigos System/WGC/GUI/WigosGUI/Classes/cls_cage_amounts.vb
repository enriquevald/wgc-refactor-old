'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_cage_amounts.vb
'
' DESCRIPTION : Cage Amounts class
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 06-NOV-2013  XMS    Initial version
' 18-NOV-2013  JPJ    Restricted amounts
' 18-JUL-2014  JBC & JPJ Fixed Bug WIG-1099: When creating denominations. 
' 25-JUL-2014  DLL    Chips are moved to a separate table
' 09-OCT-2014  DRV    Fixed Bug WIG-1441: Form allows to add chips with the same denomination.
' 13-NOV-2014  LRS    Add currency type (bill, coins) and make the update functionality
' 29-OCT-2015  ECP    Backlog Item 4950 Tables: MultiDenominations - Chip Sets (Eliminate funcionality completely)
' 08-APR-2016  RGR    Product Backlog Item 10936: Tables (Phase 1): Settings chips on different screens
' 23-MAR-2016  JML    Product Backlog Item 9755:Gaming Tables (Fase 1): Cage
'--------------------------------------------------------------------

Imports WSI.Common
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text

Public Class cls_cage_amounts
  Inherits CLASS_BASE

#Region " Constants "

  Public Const SQL_COLUMN_CUR_CURRENCY_ISO_CODE As Integer = 0
  Public Const SQL_COLUMN_CUR_CURRENCY_ALLOWED As Integer = 1
  Public Const SQL_COLUMN_CUR_CURRENCY_NAME As Integer = 2

  Public Const SQL_COLUMN_BILL_CURRENCY_ISO_CODE As Integer = 0
  Public Const SQL_COLUMN_BILL_CURRENCY_DENOMINATION As Integer = 1
  Public Const SQL_COLUMN_BILL_CURRENCY_REJECTED As Integer = 2
  Public Const SQL_COLUMN_BILL_CURRENCY_EDITABLE As Integer = 3
  Public Const SQL_COLUMN_BILL_CURRENCY_TYPE_COIN As Integer = 4
  Public Const SQL_COLUMN_BILL_CURRENCY_TYPE_BILL As Integer = 5
  Private Const MARK_DELETED As Integer = 8

  Private Const DUMMY_ROWS As Int16 = 60
  Private Const CAGE_COLUMN_ACTION_NAME As String = "ACTION"
  Private Const CAGE_COLUMN_ISO_CODE_NAME As String = "CUR_ISO_CODE"
  Private Const CAGE_COLUMN_EDITABLE_NAME As String = "EDITABLE_ROW"

#End Region       ' Constants 

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_DATA_CAGE_AMOUNTS
    Public data_table_vaa As DataTable

    ' Init structure
    Public Sub New()
      data_table_vaa = New DataTable()
    End Sub

  End Class

#End Region      ' Structures

#Region " Members "

  Protected m_data_cage_amounts As New TYPE_DATA_CAGE_AMOUNTS
  Protected m_data_cage_amounts_denomination As New TYPE_DATA_CAGE_AMOUNTS
  Private m_restricted_table_vaa As New DataTable 'Contains the restricted iso codes and amounts

#End Region         ' Members

#Region " Enum "

  Private Enum ROWACTION
    NORMAL = 0
    MODIFIED = 1
    DELETED = 2
    ADDED = 3
  End Enum

#End Region            ' Enum

#Region "Properties"

  Public Overloads Property RestrictedCageAmounts() As DataTable
    Get
      Return m_restricted_table_vaa
    End Get

    Set(ByVal Value As DataTable)
      m_restricted_table_vaa = Value
    End Set

  End Property       ' RestrictedCageAmounts

  Public Overloads Property DataCageAmounts() As DataTable
    Get
      Return m_data_cage_amounts.data_table_vaa
    End Get

    Set(ByVal Value As DataTable)
      m_data_cage_amounts.data_table_vaa = Value
    End Set

  End Property             ' DataCageAmounts

  Public Overloads Property DataCageAmountsDenomination() As DataTable
    Get
      Return m_data_cage_amounts_denomination.data_table_vaa
    End Get

    Set(ByVal Value As DataTable)
      m_data_cage_amounts_denomination.data_table_vaa = Value
    End Set

  End Property ' DataCageAmountsDenomination

#End Region        ' Properties

#Region "Overrides"

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns the related auditor data for the current object.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created audit object
  '
  ' NOTES :
  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim auditor_data As CLASS_AUDITOR_DATA
    Dim _dt As DataTable
    Dim _dv As DataView
    Dim _row As DataRow
    Dim _row2 As DataRow
    Dim _idx_row As Int64
    Dim _idx_blanks As Int64
    Dim _rows() As DataRow


    Using DbTrx As New DB_TRX()


      auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)

      _dt = New DataTable
      auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(3000), "")

      ' Currencies
      For Each _currency_enable As DataRow In Me.DataCageAmounts.Rows
        Call auditor_data.SetField(0, GetRepresentativeValue(_currency_enable(SQL_COLUMN_CUR_CURRENCY_ALLOWED)) & ", " & _
                                      _currency_enable(SQL_COLUMN_CUR_CURRENCY_NAME) & ", " & _
                                      _currency_enable(SQL_COLUMN_BILL_CURRENCY_ISO_CODE))
      Next

      Try

        ' m�ximo
        If Not Me.DataCageAmountsDenomination Is Nothing Then
          _dt = Me.DataCageAmountsDenomination.Copy
          _dt.Columns.Add(CAGE_COLUMN_ACTION_NAME, Type.GetType("System.Int16"))

          For Each _row In _dt.Rows
            Select Case _row.RowState
              Case DataRowState.Added
                _row(CAGE_COLUMN_ACTION_NAME) = ROWACTION.ADDED
                _row(CAGE_COLUMN_EDITABLE_NAME) = ROWACTION.NORMAL
              Case DataRowState.Deleted
                _row.RejectChanges()
                _row(CAGE_COLUMN_ACTION_NAME) = ROWACTION.NORMAL
                _row(CAGE_COLUMN_EDITABLE_NAME) = ROWACTION.DELETED
              Case DataRowState.Modified

                _row(CAGE_COLUMN_ACTION_NAME) = ROWACTION.MODIFIED
                _row(CAGE_COLUMN_EDITABLE_NAME) = ROWACTION.MODIFIED
              Case Else
                _row(CAGE_COLUMN_ACTION_NAME) = ROWACTION.NORMAL
                _row(CAGE_COLUMN_EDITABLE_NAME) = ROWACTION.NORMAL
            End Select

            If _row.RowState = DataRowState.Deleted Then

            ElseIf _row.RowState = DataRowState.Added Then
            End If
          Next

          _dv = _dt.DefaultView
          _dv.Sort = "ISO_CODE"
          _dt = _dv.ToTable()
        End If

        For Each _row In Me.m_data_cage_amounts.data_table_vaa.Rows
          _idx_row = 0
          _rows = _dt.Select("ISO_CODE= '" & _row.Item(CAGE_COLUMN_ISO_CODE_NAME).ToString & "'")
          For Each _row2 In _rows

            If _row2(CAGE_COLUMN_EDITABLE_NAME) = ROWACTION.MODIFIED Then
              If _row2(CAGE_COLUMN_EDITABLE_NAME) <> ROWACTION.DELETED Then

                AuditByCurrency(DbTrx, auditor_data, _row2, CageCurrencyType.Bill)
                AuditByCurrency(DbTrx, auditor_data, _row2, CageCurrencyType.Coin)
              Else

                AuditByCurrency(DbTrx, auditor_data, _row2)
              End If
              _idx_row = _idx_row + 1

              Continue For
            End If

            If _row2(CAGE_COLUMN_EDITABLE_NAME) <> ROWACTION.DELETED Then

              AuditByCurrency(DbTrx, auditor_data, _row2, CageCurrencyType.Bill)
              AuditByCurrency(DbTrx, auditor_data, _row2, CageCurrencyType.Coin)
            Else

              AuditByCurrency(DbTrx, auditor_data, _row2)
            End If
            _idx_row = _idx_row + 1


          Next

          For _idx_blanks = _idx_row To DUMMY_ROWS
            Call auditor_data.SetField(0, AUDIT_NONE_STRING, _row.Item(CAGE_COLUMN_ISO_CODE_NAME).ToString)
            Call auditor_data.SetField(0, AUDIT_NONE_STRING, _row.Item(CAGE_COLUMN_ISO_CODE_NAME).ToString)
          Next
        Next

      Catch ex As Exception

        Debug.Print(ex.Message)
      End Try
    End Using



    Return auditor_data

  End Function   ' AuditorData

  '----------------------------------------------------------------------------
  ' PURPOSE : Make audit field evaluating the currency type
  '
  ' PARAMS :
  '     - INPUT :
  '
  ' RETURNS :

  '
  ' NOTES :
  Private Sub AuditByCurrency(ByVal DbTrx As DB_TRX, ByRef AuditorData As CLASS_AUDITOR_DATA, ByRef Row As DataRow, Optional ByVal Type As CageCurrencyType = -1)
    Dim _col As Integer
    Dim _exist As Boolean
    Dim _text_aux As String

    If Type <> -1 Then
      _text_aux = ""

      Select Case Type
        Case CageCurrencyType.Bill
          _col = SQL_COLUMN_BILL_CURRENCY_TYPE_BILL
        Case CageCurrencyType.Coin
          _col = SQL_COLUMN_BILL_CURRENCY_TYPE_COIN
        Case Else

      End Select

      If Row(_col) = ROWACTION.ADDED Then  ' has been added
        _text_aux = " - " & GLB_NLS_GUI_COMMONMISC.GetString(10)

      ElseIf Row(_col) = ROWACTION.DELETED Then ' has been deleted

        _text_aux = " - " & GLB_NLS_GUI_COMMONMISC.GetString(9)
      Else ' can be added or deleted

        _exist = GetCageAmountsExist(DbTrx, Row(SQL_COLUMN_BILL_CURRENCY_ISO_CODE), Row(SQL_COLUMN_BILL_CURRENCY_DENOMINATION), Type)
        If Row(_col) = 1 And Not _exist Then
          _text_aux = " - " & GLB_NLS_GUI_COMMONMISC.GetString(10)
        ElseIf Row(_col) = 0 And _exist Then
          _text_aux = " - " & GLB_NLS_GUI_COMMONMISC.GetString(9)

        End If

      End If
      Call AuditorData.SetField(0, _
                                    CompleteDenominationString(Row, Type) & _text_aux, CompleteDenominationString(Row, Type))

    Else
      _text_aux = ""
      If Row(CAGE_COLUMN_EDITABLE_NAME) = ROWACTION.DELETED Then
        _text_aux = " - " & GLB_NLS_GUI_COMMONMISC.GetString(9)
      End If
      Call AuditorData.SetField(0, _
                                     CompleteDenominationString(Row, CageCurrencyType.Bill) & _text_aux, CompleteDenominationString(Row, Type))
      Call AuditorData.SetField(0, _
                                     CompleteDenominationString(Row, CageCurrencyType.Coin) & _text_aux, CompleteDenominationString(Row, Type))

    End If

  End Sub

  '----------------------------------------------------------------------------
  ' PURPOSE : Set the string definition for field audit
  '
  ' PARAMS :
  '     - INPUT :
  '
  ' RETURNS :
  '
  ' NOTES :
  Private Function CompleteDenominationString(ByVal Row As DataRow, ByVal Type As CageCurrencyType) As String

    Return Row(SQL_COLUMN_BILL_CURRENCY_ISO_CODE) & " - " & GUI_FormatCurrency(Row(SQL_COLUMN_BILL_CURRENCY_DENOMINATION), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & " " & CLASS_CAGE_CONTROL.GetCurrencyTypeDescription(Type)
  End Function
  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes the given object from the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DEPENDENCIES
  '
  ' NOTES :
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function     ' DB_Delete

  '----------------------------------------------------------------------------
  ' PURPOSE : Inserts the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function     ' DB_Insert

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads from the database into the given object
  '
  ' PARAMS :
  '     - INPUT :
  '         - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '
  ' NOTES :
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim rc As Integer

    ' Read currency data
    rc = DataCageAmounts_DbRead(SqlCtx)

    Select Case rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function       ' DB_Read

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim rc As Integer

    ' Update currency data
    rc = DataCageAmounts_DbUpdate(SqlCtx)

    Select Case rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function     ' DB_Update

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns a duplicate of the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE
    Dim temp_currencies_data As cls_cage_amounts

    temp_currencies_data = New cls_cage_amounts

    temp_currencies_data.m_data_cage_amounts.data_table_vaa = Me.m_data_cage_amounts.data_table_vaa.Copy()
    temp_currencies_data.m_data_cage_amounts_denomination.data_table_vaa = Me.m_data_cage_amounts_denomination.data_table_vaa.Copy()
    temp_currencies_data.m_restricted_table_vaa = Me.m_restricted_table_vaa.Copy()

    Return temp_currencies_data
  End Function     ' Duplicate

#End Region         ' Overrides

#Region "Private Functions"

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads an object from the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES:
  Private Function DataCageAmounts_DbRead(ByVal Context As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _denominations As String
    Try
      Using _db_trx As New DB_TRX()

        _denominations = GeneralParam.GetString("RegionalOptions", "CurrenciesAccepted", "MXN")

        'Amounts table
        m_data_cage_amounts.data_table_vaa = GetCageAmounts(_db_trx)

        'Amounts denomination table
        m_data_cage_amounts_denomination.data_table_vaa = GetCageAmountsDenomination(_db_trx)

      End Using

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Catch ex As Exception

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR
    End Try

  End Function      ' DataCageAmounts_DbRead

  '----------------------------------------------------------------------------
  ' PURPOSE: Updates the given object into the database.
  '
  ' PARAMS:
  '   - INPUT:  None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES:
  Private Function DataCageAmounts_DbUpdate(ByVal Context As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _sql_trx As System.Data.SqlClient.SqlTransaction
    Dim _num_of_rows As Integer
    Dim _cur_denomi_to_del() As DataRow
    Dim _iso_code As String
    Dim _sb As StringBuilder
    Dim _denomination As Decimal
    Dim _currencies_accepted_GP As String
    Dim _rows() As DataRow
    _currencies_accepted_GP = String.Empty

    Try
      _sql_trx = Nothing

      Using _db_trx As New DB_TRX()

        _sql_trx = _db_trx.SqlTransaction
        _sb = New StringBuilder()

        ' Now is possible delete the row with the mark for more simple update of DB.
        _cur_denomi_to_del = m_data_cage_amounts_denomination.data_table_vaa.Select("EDITABLE_ROW = 8", "")
        For _idx_row_to_del As Integer = 0 To _cur_denomi_to_del.Length - 1
          _cur_denomi_to_del(_idx_row_to_del).Delete()
        Next

        'DELETE
        _rows = m_data_cage_amounts_denomination.data_table_vaa.Select("", "", DataViewRowState.Deleted)
        _num_of_rows = _rows.Length
        For _idx_row_to As Integer = 0 To _num_of_rows - 1

          DeleteAmount(_sql_trx, _rows(_idx_row_to))

        Next


        ' --- Currency denomination
        'INSERT

        _rows = m_data_cage_amounts_denomination.data_table_vaa.Select("ISO_CODE <>'" & WSI.Common.Cage.CHIPS_ISO_CODE & "'", "", DataViewRowState.Added)
        _num_of_rows = _rows.Length
        For _idx_row_to As Integer = 0 To _num_of_rows - 1
          'evaluate coin
          If Not _rows(_idx_row_to).Item(SQL_COLUMN_BILL_CURRENCY_TYPE_COIN) Is System.DBNull.Value Then
            If _rows(_idx_row_to).Item(SQL_COLUMN_BILL_CURRENCY_TYPE_COIN) Then
              InsertAmount(_db_trx, _sql_trx, _rows(_idx_row_to), CageCurrencyType.Coin)
            End If
          End If
          'evaluate bill
          If Not _rows(_idx_row_to).Item(SQL_COLUMN_BILL_CURRENCY_TYPE_BILL) Is System.DBNull.Value Then
            If _rows(_idx_row_to).Item(SQL_COLUMN_BILL_CURRENCY_TYPE_BILL) Then
              InsertAmount(_db_trx, _sql_trx, _rows(_idx_row_to), CageCurrencyType.Bill)
            End If
          End If
        Next

        ' --- Currency denomination
        'Modified, can be delete or insert

        _rows = m_data_cage_amounts_denomination.data_table_vaa.Select("ISO_CODE <>'" & WSI.Common.Cage.CHIPS_ISO_CODE & "'", "", DataViewRowState.ModifiedOriginal)
        _num_of_rows = _rows.Length

        For _idx_row_to As Integer = 0 To _num_of_rows - 1
          _denomination = _rows(_idx_row_to).Item(SQL_COLUMN_BILL_CURRENCY_DENOMINATION)
          _iso_code = _rows(_idx_row_to).Item(SQL_COLUMN_BILL_CURRENCY_ISO_CODE, DataRowVersion.Original)
          'evaluate coin
          If Not _rows(_idx_row_to).Item(SQL_COLUMN_BILL_CURRENCY_TYPE_COIN) Is System.DBNull.Value AndAlso _rows(_idx_row_to).Item(SQL_COLUMN_BILL_CURRENCY_TYPE_COIN) Then ' insert denomination coin
            If Not GetCageAmountsExist(_db_trx, _iso_code, _denomination, CageCurrencyType.Coin) Then
              InsertAmount(_db_trx, _sql_trx, _rows(_idx_row_to), CageCurrencyType.Coin)
            End If
          Else ' delete denomination coin
            DeleteAmount(_sql_trx, _rows(_idx_row_to), CageCurrencyType.Coin)
          End If
          'evaluate bill
          If Not _rows(_idx_row_to).Item(SQL_COLUMN_BILL_CURRENCY_TYPE_BILL) Is System.DBNull.Value AndAlso _rows(_idx_row_to).Item(SQL_COLUMN_BILL_CURRENCY_TYPE_BILL) Then ' insert denomination bill
            If Not GetCageAmountsExist(_db_trx, _iso_code, _denomination, CageCurrencyType.Bill) Then
              InsertAmount(_db_trx, _sql_trx, _rows(_idx_row_to), CageCurrencyType.Bill)
            End If
          Else ' delete denomination coin bill
            DeleteAmount(_sql_trx, _rows(_idx_row_to), CageCurrencyType.Bill)
          End If

        Next


        ' --- Ok
        _db_trx.Commit()

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
      End Using

    Catch ex As Exception

      Log.Exception(ex)
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

  End Function    ' CurrencyData_DbUpdate


  Private Sub InsertAmount(ByVal DbTRx As DB_TRX, ByVal SqlTrx As System.Data.SqlClient.SqlTransaction, ByRef Row As DataRow, ByVal Type As CageCurrencyType)

    Dim _iso_code As String
    Dim _allowed As Boolean
    Dim _sb As StringBuilder
    Dim _denomination As Decimal


    _iso_code = Row.Item(SQL_COLUMN_BILL_CURRENCY_ISO_CODE)
    _denomination = Row.Item(SQL_COLUMN_BILL_CURRENCY_DENOMINATION)

    _allowed = Row.Item(SQL_COLUMN_BILL_CURRENCY_REJECTED)

    If Not GetCageAmountsExist(DbTRx, _iso_code, _denomination, Type) Then
      _sb = New StringBuilder
      _sb.Length = 0
      _sb.AppendLine("INSERT INTO  CAGE_CURRENCIES")
      _sb.AppendLine("            (CGC_ISO_CODE, CGC_DENOMINATION, CGC_ALLOWED,cgc_cage_currency_type)")
      _sb.AppendLine(" VALUES     (@pVaaIsoCode, @pVaaDenomination, @pVaaAllowed, @pVaaType)")

      Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx)
        _cmd.Parameters.Add("@pVaaIsoCode", SqlDbType.NVarChar, 3).Value = _iso_code
        _cmd.Parameters.Add("@pVaaDenomination", SqlDbType.Money).Value = _denomination
        _cmd.Parameters.Add("@pVaaAllowed", SqlDbType.Bit).Value = _allowed
        _cmd.Parameters.Add("@pVaaType", SqlDbType.Bit).Value = Type

        _cmd.ExecuteNonQuery()
      End Using

      Select Case Type
        Case CageCurrencyType.Bill
          Row.Item(SQL_COLUMN_BILL_CURRENCY_TYPE_BILL) = ROWACTION.ADDED ' changed for audit
        Case CageCurrencyType.Coin
          Row.Item(SQL_COLUMN_BILL_CURRENCY_TYPE_COIN) = ROWACTION.ADDED ' changed for audit
      End Select

    End If
  End Sub

  Private Sub DeleteAmount(ByVal SqlTrx As System.Data.SqlClient.SqlTransaction, ByRef Row As DataRow, Optional ByVal Type As CageCurrencyType = -1)
    Dim _iso_code As String

    Dim _sb As StringBuilder
    Dim _denomination As Decimal

    _sb = New StringBuilder
    _iso_code = Row.Item(SQL_COLUMN_BILL_CURRENCY_ISO_CODE, DataRowVersion.Original)
    _denomination = Row.Item(SQL_COLUMN_BILL_CURRENCY_DENOMINATION, DataRowVersion.Original)
    _sb.Length = 0
    _sb.AppendLine("DELETE FROM  CAGE_CURRENCIES")
    _sb.AppendLine(" WHERE       CGC_ISO_CODE = @pVaaIsoCode ")
    _sb.AppendLine("             AND CGC_DENOMINATION = @pVaaDenomination ")
    If Type > -1 Then
      _sb.AppendLine("             AND CGC_cage_currency_type = @pVaaCurrencyType ")
    End If
    Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx)
      _cmd.Parameters.Add("@pVaaIsoCode", SqlDbType.NVarChar, 3).Value = _iso_code
      _cmd.Parameters.Add("@pVaaDenomination", SqlDbType.Money).Value = _denomination
      If Type > -1 Then
        _cmd.Parameters.Add("@pVaaCurrencyType", SqlDbType.Money).Value = Type
      End If
      _cmd.ExecuteNonQuery()

      Select Case Type
        Case CageCurrencyType.Bill
          Row.Item(SQL_COLUMN_BILL_CURRENCY_TYPE_BILL) = ROWACTION.DELETED ' changed for audit
        Case CageCurrencyType.Coin
          Row.Item(SQL_COLUMN_BILL_CURRENCY_TYPE_COIN) = ROWACTION.DELETED ' changed for audit
      End Select
    End Using
  End Sub

  ' PURPOSE: Gets the representive value
  '
  '    - INPUT:
  '      Value: true or false
  '    - OUTPUT:
  '
  ' RETURNS:
  '    - Sufix : Contains the sufix for the audit
  Private Function GetRepresentativeValue(ByVal Value As String) As String

    If Value Then
      Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(698) ' "Si"
    Else
      Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(699) ' "No"
    End If

  End Function       ' GetRepresentativeValue

  '----------------------------------------------------------------------------
  ' PURPOSE : Read object (amount movements) from the DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - DbTrx: Transaction
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - Datatable:
  '
  ' NOTES :
  Public Function GetRestrictedAmountMovements(ByVal DbTrx As DB_TRX) As DataTable
    Dim _table As DataTable
    Dim _sb As StringBuilder

    _table = New DataTable()
    _sb = New StringBuilder()

    Try

      _sb.AppendLine("   SELECT *")
      _sb.AppendLine("   FROM")
      _sb.AppendLine("      (  SELECT    CMD.CMD_ISO_CODE    AS CMD_ISO_CODE ")
      _sb.AppendLine("                  , CMD.CMD_DENOMINATION      AS CMD_DENOMINATION ")
      _sb.AppendLine("                  , CASE WHEN CMD.CMD_CAGE_CURRENCY_TYPE IS NULL THEN 0 ELSE CMD.CMD_CAGE_CURRENCY_TYPE END AS CMD_TYPE")
      _sb.AppendLine("            FROM    CAGE_MOVEMENT_DETAILS CMD ")
      _sb.AppendLine("                       INNER JOIN CAGE_PENDING_MOVEMENTS CGM ON CMD.CMD_MOVEMENT_ID = CGM.CPM_MOVEMENT_ID")
      _sb.AppendLine("       UNION ALL ")
      _sb.AppendLine("          SELECT    CM.CM_CURRENCY_ISO_CODE       AS CMD_ISO_CODE")
      _sb.AppendLine("                  , CM.CM_CURRENCY_DENOMINATION   AS CMD_DENOMINATION")
      _sb.AppendLine("                  , CASE WHEN CM.CM_CAGE_CURRENCY_TYPE IS NULL THEN 0 ELSE CM.CM_CAGE_CURRENCY_TYPE END AS CMD_TYPE")
      _sb.AppendLine("            FROM    CAGE_MOVEMENTS  CGM ")
      _sb.AppendLine("                       INNER JOIN CAGE_PENDING_MOVEMENTS CPM ON CGM.CGM_MOVEMENT_ID = CPM.CPM_MOVEMENT_ID  ")
      _sb.AppendLine("                       INNER JOIN CASHIER_MOVEMENTS CM ON CGM.CGM_cashier_session_id = CM.cm_session_id  ")
      _sb.AppendLine("           WHERE    CPM.CPM_USER_ID = -1 ")
      _sb.AppendLine("                    AND CGM.CGM_CASHIER_SESSION_ID <> NULL")
      _sb.AppendLine("                    AND cm.cm_type IN (@pCloseSession, @pFillerOut)")
      _sb.AppendLine("       UNION ALL ")
      _sb.AppendLine("          SELECT	CMD.CMD_ISO_CODE    AS CMD_ISO_CODE ")
      _sb.AppendLine("          		, CMD.CMD_DENOMINATION      AS CMD_DENOMINATION ")
      _sb.AppendLine("          		, CASE WHEN CMD.CMD_CAGE_CURRENCY_TYPE IS NULL THEN 0 ELSE CMD.CMD_CAGE_CURRENCY_TYPE END AS CMD_TYPE ")
      _sb.AppendLine("          FROM  CAGE_MOVEMENTS AS CM")
      _sb.AppendLine("          INNER JOIN  CAGE_MOVEMENT_DETAILS AS CMD ON  CM.CGM_MOVEMENT_ID=CMD.CMD_MOVEMENT_ID")
      _sb.AppendLine("                WHERE(CM.CGM_TYPE = @pRequestType And CM.CGM_STATUS = @pRequestStatus)")
      _sb.AppendLine("      ) Temp")
      _sb.AppendLine("   GROUP BY     Temp.CMD_ISO_CODE, Temp.CMD_DENOMINATION,CMD_TYPE")

      Using _sql_cmd As New SqlCommand(_sb.ToString)
        _sql_cmd.Parameters.Add("@pCloseSession", SqlDbType.Int).Value = CASHIER_MOVEMENT.CAGE_CLOSE_SESSION
        _sql_cmd.Parameters.Add("@pFillerOut", SqlDbType.Int).Value = CASHIER_MOVEMENT.CAGE_FILLER_OUT
        _sql_cmd.Parameters.Add("@pRequestType", SqlDbType.Int).Value = Cage.CageOperationType.RequestOperation
        _sql_cmd.Parameters.Add("@pRequestStatus", SqlDbType.Int).Value = Cage.CageStatus.Sent

        Using _sql_da As New SqlDataAdapter(_sql_cmd)

          DbTrx.Fill(_sql_da, _table)

        End Using

      End Using

    Catch ex As Exception

      Log.Exception(ex)
    End Try

    Return _table
  End Function ' GetRestrictedAmountMovements

  '----------------------------------------------------------------------------
  ' PURPOSE : Read object (amount movements) from the DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - DbTrx: Transaction
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - Datatable:
  '
  ' NOTES :
  Private Function GetCageAmounts(ByVal DbTrx As DB_TRX) As DataTable
    Dim _table As DataTable
    Dim _sb As StringBuilder

    _table = New DataTable()
    _sb = New StringBuilder()

    Try

      ' DLL 07-JUL-2014: Only currencies. Chips goes in other form
      _sb.AppendLine(" SELECT   DISTINCT CE_CURRENCY_ISO_CODE AS CUR_ISO_CODE ")
      _sb.AppendLine("        , ISNULL(CGC_ALLOWED, 0) AS  CGC_ALLOWED  ")
      _sb.AppendLine("        , ISNULL(CE_DESCRIPTION, '')   AS CUR_NAME ")
      _sb.AppendLine("        , CE_TYPE ")
      _sb.AppendLine("   FROM   CURRENCY_EXCHANGE ")
      _sb.AppendLine("          LEFT JOIN CAGE_CURRENCIES ON CE_CURRENCY_ISO_CODE = CGC_ISO_CODE ")
      _sb.AppendLine("  WHERE   CE_TYPE = @pCurrencyType")
      _sb.AppendLine("ORDER BY  CE_CURRENCY_ISO_CODE ")

      Using _sql_cmd As New SqlCommand(_sb.ToString)

        _sql_cmd.Parameters.Add("@pCurrencyType", SqlDbType.Int).Value = CurrencyExchangeType.CURRENCY

        Using _sql_da As New SqlDataAdapter(_sql_cmd)

          DbTrx.Fill(_sql_da, _table)
          CurrencyExchange.GetTableDescription(_table, True)
        End Using

      End Using

    Catch ex As Exception

      Log.Exception(ex)
    End Try

    Return _table
  End Function              ' GetCageAmounts

  '----------------------------------------------------------------------------
  ' PURPOSE : Read object (amount movements) from the DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - DbTrx: Transaction
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - Boolean:
  '
  ' NOTES :
  Private Function GetCageAmountsExist(ByVal DbTrx As DB_TRX, ByVal IsoCode As String, ByVal Deno As String, ByVal Type As CageCurrencyType) As Boolean
    Dim _table As DataTable
    Dim _sb As StringBuilder
    Dim _return As Boolean
    Dim _data As Object

    _table = New DataTable()
    _sb = New StringBuilder()
    _return = False

    Try

      _sb.AppendLine("    SELECT   TOP 1 *                            ")
      _sb.AppendLine("      FROM   CAGE_CURRENCIES                       ")
      _sb.AppendLine("     WHERE   CGC_ISO_CODE = '" & IsoCode & "'   ")
      _sb.AppendLine("       AND   CGC_DENOMINATION = " & GUI_LocalNumberToDBNumber(Deno)) ' lmrs 22/10/2014 replace string for numer and format
      _sb.AppendLine("       AND   CGC_CAGE_CURRENCY_TYPE = " & Type & "  ")

      Using _sql_cmd As New SqlCommand(_sb.ToString, DbTrx.SqlTransaction.Connection, DbTrx.SqlTransaction)
        _data = _sql_cmd.ExecuteScalar()
        If Not IsNothing(_data) AndAlso _data IsNot DBNull.Value Then
          _return = True
        End If

      End Using

    Catch ex As Exception

      Log.Exception(ex)
    End Try

    Return _return

  End Function              ' GetCageAmounts

  '----------------------------------------------------------------------------
  ' PURPOSE : Read object (amounts denomination) from the DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - DbTrx: Transaction
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - Datatable:
  '
  ' NOTES :
  Public Function GetCageAmountsDenomination(ByVal DbTrx As DB_TRX, Optional ByVal ExternalForm As Boolean = False) As DataTable
    Dim _table As DataTable
    Dim _sb As StringBuilder

    _table = New DataTable()
    _sb = New StringBuilder()

    Try

      _sb.AppendLine("   SELECT    CGC_ISO_CODE AS ISO_CODE")
      _sb.AppendLine("          ,  CGC_DENOMINATION AS DENOMINATION ")
      _sb.AppendLine("          ,  CGC_ALLOWED AS ALLOWED ")
      _sb.AppendLine("          ,  -1 as EDITABLE_ROW")
      If Not ExternalForm Then
        _sb.AppendLine("          , ISNULL((SELECT 1 FROM CAGE_CURRENCIES AS CC2 WHERE CC2.CGC_ISO_CODE = CAGE_CURRENCIES.CGC_ISO_CODE AND  CC2.CGC_DENOMINATION = CAGE_CURRENCIES.CGC_DENOMINATION AND CC2.CGC_CAGE_CURRENCY_TYPE=1),0) AS COIN")
        _sb.AppendLine("          , ISNULL((SELECT 1 FROM CAGE_CURRENCIES AS CC2 WHERE CC2.CGC_ISO_CODE = CAGE_CURRENCIES.CGC_ISO_CODE AND CC2.CGC_DENOMINATION=CAGE_CURRENCIES.CGC_DENOMINATION AND CC2.CGC_CAGE_CURRENCY_TYPE=0),0) AS BILL")
      Else
        _sb.AppendLine("            , CASE WHEN CGC_DENOMINATION > 0 THEN ISNULL(CGC_CAGE_CURRENCY_TYPE,0) ELSE 99 END AS TYPE ")
      End If
      _sb.AppendLine("              FROM   CAGE_CURRENCIES")
      _sb.AppendLine("              WHERE   CGC_DENOMINATION  >= 0")
      If Not ExternalForm Then
        _sb.AppendLine("   GROUP BY CGC_DENOMINATION, CGC_ISO_CODE, CGC_ALLOWED ")
      End If

      _sb.AppendLine("    ORDER BY   ISO_CODE ")
      _sb.AppendLine("             , DENOMINATION ")

      Using _sql_cmd As New SqlCommand(_sb.ToString)

        Using _sql_da As New SqlDataAdapter(_sql_cmd)

          DbTrx.Fill(_sql_da, _table)

        End Using
      End Using

    Catch ex As Exception

      Log.Exception(ex)
    End Try

    Return _table
  End Function  ' GetCageAmountsDenomination

  '----------------------------------------------------------------------------
  ' PURPOSE : Read object (amounts denomination for currencies and chips) from the DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - DbTrx: Transaction
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - Datatable:
  '
  ' NOTES :
  Public Function GetCageDenominations_CurrenciesAndChips(ByVal DbTrx As DB_TRX, Optional ByVal ExternalForm As Boolean = False) As DataTable
    Dim _table As DataTable
    Dim _sb As StringBuilder

    _table = New DataTable()
    _sb = New StringBuilder()

    Try

      _sb.AppendLine("   SELECT    CGC_ISO_CODE AS ISO_CODE")
      _sb.AppendLine("          ,  CGC_DENOMINATION AS DENOMINATION ")
      _sb.AppendLine("          ,  CGC_ALLOWED AS ALLOWED ")
      _sb.AppendLine("          ,  -1 as EDITABLE_ROW")
      If Not ExternalForm Then
        _sb.AppendLine("          , ISNULL((SELECT 1 FROM CAGE_CURRENCIES AS CC2 WHERE CC2.CGC_ISO_CODE = CAGE_CURRENCIES.CGC_ISO_CODE AND  CC2.CGC_DENOMINATION = CAGE_CURRENCIES.CGC_DENOMINATION AND CC2.CGC_CAGE_CURRENCY_TYPE=1),0) AS COIN")
        _sb.AppendLine("          , ISNULL((SELECT 1 FROM CAGE_CURRENCIES AS CC2 WHERE CC2.CGC_ISO_CODE = CAGE_CURRENCIES.CGC_ISO_CODE AND CC2.CGC_DENOMINATION=CAGE_CURRENCIES.CGC_DENOMINATION AND CC2.CGC_CAGE_CURRENCY_TYPE=0),0) AS BILL")
      Else
        _sb.AppendLine("            , CASE WHEN CGC_DENOMINATION > 0 THEN ISNULL(CGC_CAGE_CURRENCY_TYPE,0) ELSE 99 END AS TYPE ")
      End If
      _sb.AppendLine("          ,  CGC_CAGE_CURRENCY_TYPE AS CAGE_CURRENCY_TYPE")
      _sb.AppendLine("          ,  -1 AS CH_CHIP_ID")
      _sb.AppendLine("              FROM   CAGE_CURRENCIES")
      _sb.AppendLine("              WHERE   CGC_DENOMINATION  >= 0")
      If Not ExternalForm Then
        _sb.AppendLine("   GROUP BY CGC_DENOMINATION, CGC_ISO_CODE, CGC_ALLOWED, CGC_CAGE_CURRENCY_TYPE ")
      End If
      _sb.AppendLine(" UNION ALL ")
      _sb.AppendLine("   SELECT    CH_ISO_CODE AS ISO_CODE")
      _sb.AppendLine("          ,  CH_DENOMINATION AS DENOMINATION ")
      _sb.AppendLine("          ,  CH_ALLOWED AS ALLOWED ")
      _sb.AppendLine("          ,  -1 AS EDITABLE_ROW")
      If Not ExternalForm Then
        _sb.AppendLine("          , 0 AS COIN")
        _sb.AppendLine("          , 0 AS BILL")
      Else
        _sb.AppendLine("            , CH_CHIP_TYPE AS TYPE ")
      End If
      _sb.AppendLine("          ,  CH_CHIP_TYPE AS CAGE_CURRENCY_TYPE")
      _sb.AppendLine("          ,  CH_CHIP_ID")
      _sb.AppendLine("              FROM   CHIPS")
      _sb.AppendLine("              WHERE   CH_ALLOWED = 1")
      If Not ExternalForm Then
        _sb.AppendLine("  GROUP BY CH_DENOMINATION, CH_ISO_CODE, CH_ALLOWED, CH_CHIP_TYPE, CH_CHIP_ID ")
      End If
      _sb.AppendLine("    ORDER BY   CAGE_CURRENCY_TYPE ")
      _sb.AppendLine("             , ISO_CODE ")
      _sb.AppendLine("             , DENOMINATION ")

      Using _sql_cmd As New SqlCommand(_sb.ToString)

        Using _sql_da As New SqlDataAdapter(_sql_cmd)

          DbTrx.Fill(_sql_da, _table)

        End Using
      End Using

    Catch ex As Exception

      Log.Exception(ex)
    End Try

    Return _table
  End Function  ' GetRestrictedAmountMovements


#End Region ' Private Functions

End Class
