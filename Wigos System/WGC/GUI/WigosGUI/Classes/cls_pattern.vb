'-------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_pattern.vb
' AUTHOR:        Javier Molina
' CREATION DATE: 08-MAY-2014
' DESCRIPTION : Pattern class for user edition
'              
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 08-MAY-2014  JMM    Initial version
' 05-SEP-2014  JPJ    Mailing alarm functionality
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports WSI.Common
Imports System.Text

Public Class CLASS_PATTERN
  Inherits CLASS_BASE

#Region " Constants "

  ' Fields length
  Public Const MAX_NAME_LEN As Integer = 50
  Public Const MAX_DESCRIPTION_LEN As Integer = 350
  Public Const MAX_MAILING_ADDRESS_LEN As Integer = 500
  Public Const MAX_MAILING_SUBJECT_LEN As Integer = 200

#End Region

#Region " Enums "

#End Region ' Enums 

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_PATTERN
    Public pattern_id As Int64
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_NAME_LEN + 1)> _
    Public name As String
    Public description As String
    Public active As Boolean
    Public pattern As New PatternList
    Public alarm_code As Integer
    Public alarm_name As String
    Public alarm_description As String
    Public alarm_severity As AlarmSeverity
    Public type As PATTERN_TYPE
    Public source As Integer
    Public last_occurrence As Date
    Public num_occurrences As Integer
    Public terminal_list As New TerminalList
    Public scheduled_time_from As Integer
    Public scheduled_time_to As Integer
    Public life_time As Integer
    Public mailing_active As Boolean
    Public mailing_address_list As String
    Public mailing_subject As String
  End Class

#End Region

#Region " Members "

  Protected m_pattern As New TYPE_PATTERN

#End Region

#Region " Properties "

  Public Property PatternID() As Int64
    Get
      Return m_pattern.pattern_id
    End Get

    Set(ByVal Value As Int64)
      m_pattern.pattern_id = Value
    End Set
  End Property

  Public Property Name() As String
    Get
      Return m_pattern.name
    End Get

    Set(ByVal Value As String)
      m_pattern.name = Value
    End Set
  End Property

  Public Property Description() As String
    Get
      Return Me.m_pattern.description
    End Get
    Set(ByVal value As String)
      Me.m_pattern.description = value
    End Set
  End Property

  Public Property Active() As Boolean
    Get
      Return m_pattern.active
    End Get

    Set(ByVal Value As Boolean)
      m_pattern.active = Value
    End Set
  End Property

  Public ReadOnly Property Pattern() As PatternList
    Get
      Return m_pattern.pattern
    End Get
  End Property

  Public Property AlarmCode() As Integer
    Get
      Return m_pattern.alarm_code
    End Get
    Set(ByVal Value As Integer)
      m_pattern.alarm_code = Value
    End Set
  End Property

  Public Property AlarmName() As String
    Get
      Return m_pattern.alarm_name
    End Get
    Set(ByVal Value As String)
      m_pattern.alarm_name = Value
    End Set
  End Property

  Public Property AlarmDescription() As String
    Get
      Return m_pattern.alarm_description
    End Get
    Set(ByVal Value As String)
      m_pattern.alarm_description = Value
    End Set
  End Property

  Public Property AlarmSeverity() As AlarmSeverity
    Get
      Return m_pattern.alarm_severity
    End Get
    Set(ByVal Value As AlarmSeverity)
      m_pattern.alarm_severity = Value
    End Set
  End Property

  Public Property Type() As PATTERN_TYPE
    Get
      Return m_pattern.type
    End Get
    Set(ByVal Value As PATTERN_TYPE)
      m_pattern.type = Value
    End Set
  End Property

  Public Property Source() As Integer
    Get
      Return m_pattern.source
    End Get
    Set(ByVal Value As Integer)
      m_pattern.source = Value
    End Set
  End Property

  Public Property LastOcurrence() As Date
    Get
      Return m_pattern.last_occurrence
    End Get
    Set(ByVal Value As Date)
      m_pattern.last_occurrence = Value
    End Set
  End Property

  Public Property NumOccurrences() As Integer
    Get
      Return m_pattern.num_occurrences
    End Get
    Set(ByVal Value As Integer)
      m_pattern.num_occurrences = Value
    End Set
  End Property

  Public ReadOnly Property TerminalList() As TerminalList
    Get
      Return m_pattern.terminal_list
    End Get
  End Property

  Public Property ScheduledTimeFrom() As Integer
    Get
      Return m_pattern.scheduled_time_from
    End Get
    Set(ByVal Value As Integer)
      m_pattern.scheduled_time_from = Value
    End Set
  End Property

  Public Property ScheduledTimeTo() As Integer
    Get
      Return m_pattern.scheduled_time_to
    End Get
    Set(ByVal Value As Integer)
      m_pattern.scheduled_time_to = Value
    End Set
  End Property

  Public Property LifeTime() As Integer
    Get
      Return m_pattern.life_time
    End Get
    Set(ByVal Value As Integer)
      m_pattern.life_time = Value
    End Set
  End Property

  Public Property MailingActive() As Boolean
    Get
      Return m_pattern.mailing_active
    End Get
    Set(ByVal Value As Boolean)
      m_pattern.mailing_active = Value
    End Set
  End Property

  Public Property MailingAddressList() As String
    Get
      Return m_pattern.mailing_address_list
    End Get
    Set(ByVal Value As String)
      m_pattern.mailing_address_list = Value
    End Set
  End Property

  Public Property MailingSubject() As String
    Get
      Return m_pattern.mailing_subject
    End Get
    Set(ByVal Value As String)
      m_pattern.mailing_subject = Value
    End Set
  End Property
#End Region ' Structures

#Region " Overrides functions "

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads from the database into the given object
  '
  ' PARAMS :
  '     - INPUT :
  '         - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '
  ' NOTES :

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    Me.PatternID = ObjectId

    ' Read pattern data
    _rc = ReadPattern(m_pattern, SqlCtx)

    Select Case _rc

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Read

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    ' Update pattern data
    _rc = UpdatePattern(m_pattern, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Update

  '----------------------------------------------------------------------------
  ' PURPOSE : Inserts the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    ' Insert pattern data
    _rc = InsertPattern(m_pattern, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Insert

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes the given object from the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DEPENDENCIES
  '
  ' NOTES :

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    ' Delete pattern data
    _rc = DeletePattern(Me.PatternID, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DEPENDENCIES
        Return ENUM_STATUS.STATUS_DEPENDENCIES

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Delete

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns a duplicate of the given object.
  '
  ' PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created object
  '
  ' NOTES :

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim temp_pattern As CLASS_PATTERN

    temp_pattern = New CLASS_PATTERN
    temp_pattern.m_pattern.pattern_id = Me.m_pattern.pattern_id
    temp_pattern.m_pattern.name = Me.m_pattern.name
    temp_pattern.m_pattern.description = Me.m_pattern.description
    temp_pattern.m_pattern.active = Me.m_pattern.active

    temp_pattern.m_pattern.pattern = New PatternList()
    temp_pattern.m_pattern.pattern.FromXml(Me.m_pattern.pattern.ToXml())

    temp_pattern.m_pattern.alarm_code = Me.m_pattern.alarm_code
    temp_pattern.m_pattern.alarm_name = Me.m_pattern.alarm_name
    temp_pattern.m_pattern.alarm_description = Me.m_pattern.alarm_description
    temp_pattern.m_pattern.alarm_severity = Me.m_pattern.alarm_severity
    temp_pattern.m_pattern.type = Me.m_pattern.type
    temp_pattern.m_pattern.source = Me.m_pattern.source
    temp_pattern.m_pattern.last_occurrence = Me.m_pattern.last_occurrence
    temp_pattern.m_pattern.num_occurrences = Me.m_pattern.num_occurrences

    temp_pattern.m_pattern.terminal_list = New TerminalList()
    temp_pattern.m_pattern.terminal_list.FromXml(Me.m_pattern.terminal_list.ToXml())

    temp_pattern.m_pattern.scheduled_time_from = Me.m_pattern.scheduled_time_from
    temp_pattern.m_pattern.scheduled_time_to = Me.m_pattern.scheduled_time_to

    temp_pattern.m_pattern.life_time = Me.m_pattern.life_time
    temp_pattern.m_pattern.mailing_active = Me.m_pattern.mailing_active
    temp_pattern.m_pattern.mailing_address_list = Me.m_pattern.mailing_address_list
    temp_pattern.m_pattern.mailing_subject = Me.m_pattern.mailing_subject

    Return temp_pattern

  End Function ' Duplicate

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns the related auditor data for the current object.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created audit object
  '
  ' NOTES :

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _yes_text As String
    Dim _no_text As String

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_PATTERNS)

    _yes_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
    _no_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)

    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(4947), Me.Name)     ' 4947 "Pattern"
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(353), Me.Name)     ' 353 "Nombre"

    ' Active
    If Me.Active Then
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(397), _yes_text) ' 397 "Active"
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(397), _no_text)  ' 397 "Active"
    End If

    ' Scheduled time
    If Me.ScheduledTimeFrom = -1 Then
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(311), "---")                                                 ' 311 "Start date"
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(311), TimeSpan.FromSeconds(Me.ScheduledTimeFrom).ToString)   ' 311 "Start date"
    End If

    If Me.ScheduledTimeTo = -1 Then
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(312), "---")                                                 ' 312 "End date"
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(312), TimeSpan.FromSeconds(Me.ScheduledTimeTo).ToString)     ' 312 "End date"
    End If

    'Life time
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4972), Me.LifeTime)                     ' 4972 "Life time"

    'Type
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1087), PatternTypeDescription(Me.Type)) ' 1087 "Type"

    'Description
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(688), Me.Description)                   ' 688 "Description"

    'Alarm Name
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4957), Me.AlarmName)                    ' 4957 "Alarm name"

    'Alarm Description
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4958), Me.AlarmDescription)             ' 4958 "Alarm description"

    'Alarm Severity
    Select Case Me.AlarmSeverity
      Case AlarmSeverity.Error
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4959), GLB_NLS_GUI_ALARMS.GetString(357))    ' 4959 "Alarm severity"
      Case AlarmSeverity.Info
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4959), GLB_NLS_GUI_ALARMS.GetString(206))    ' 4959 "Alarm severity"
      Case AlarmSeverity.Warning
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4959), GLB_NLS_GUI_ALARMS.GetString(358))    ' 4959 "Alarm severity"
      Case Else
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4959), GLB_NLS_GUI_ALARMS.GetString(786))    ' 4959 "Alarm severity"
    End Select

    'Alarm mailing active
    Call _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(396), Me.MailingActive)          ' "Mailing active"
    'Alarm mailing subject
    Call _auditor_data.SetField(GLB_NLS_GUI_STATISTICS.Id(436), Me.MailingSubject)            ' "Alarm mailing subject"
    'Alarm mailing address list
    Call _auditor_data.SetField(GLB_NLS_GUI_STATISTICS.Id(429), Me.MailingAddressList)        ' "Alarm mailing address list"

    'TerminalList
    ' Type
    Call _auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(486), Me.TerminalList.AuditStringType(GLB_NLS_GUI_CONFIGURATION.GetString(497), _
                                                                                              GLB_NLS_GUI_CONFIGURATION.GetString(499), _
                                                                                              GLB_NLS_GUI_CONFIGURATION.GetString(498)))

    ' Group list
    Call _auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(477), GLB_NLS_GUI_CONTROLS.GetString(482) + ": " + _
                               Me.TerminalList.AuditStringList(GROUP_ELEMENT_TYPE.GROUP, _
                                                               GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Prviders list
    Call _auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(477), GLB_NLS_GUI_CONTROLS.GetString(478) + ": " + _
                               Me.TerminalList.AuditStringList(GROUP_ELEMENT_TYPE.PROV, _
                                                               GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Zones list
    Call _auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(477), GLB_NLS_GUI_CONTROLS.GetString(479) + ": " + _
                               Me.TerminalList.AuditStringList(GROUP_ELEMENT_TYPE.ZONE, _
                                                               GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Areas list
    Call _auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(477), GLB_NLS_GUI_CONTROLS.GetString(480) + ": " + _
                               Me.TerminalList.AuditStringList(GROUP_ELEMENT_TYPE.AREA, _
                                                               GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Banks list
    Call _auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(477), GLB_NLS_GUI_CONTROLS.GetString(481) + ": " + _
                               Me.TerminalList.AuditStringList(GROUP_ELEMENT_TYPE.BANK, _
                                                               GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Terminals list
    Call _auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(477), GLB_NLS_GUI_CONTROLS.GetString(483) + ": " + _
                                Me.TerminalList.AuditStringList(GROUP_ELEMENT_TYPE.TERM, _
                                                                GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Pattern list
    'Debug.Print(String.Format("Pattern {0}: {1}", Me.Name, Me.Pattern.AuditString()))
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4947), Me.Pattern.AuditString())

    Return _auditor_data

  End Function ' AuditorData

#End Region

#Region " Private Functions "

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads a gift object from DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - Item.gift_id
  '         - Context
  '
  '     - OUTPUT :
  '         - Item
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function ReadPattern(ByVal Item As TYPE_PATTERN, ByVal Context As Integer) As Integer

    Dim _sql_query As StringBuilder
    Dim _data_table As DataTable
    Dim _sql_tx As System.Data.SqlClient.SqlTransaction = Nothing
    Dim _xml As String

    Try
      _sql_query = New StringBuilder

      _sql_query.AppendLine("SELECT   PT_ID                         ")
      _sql_query.AppendLine("       , PT_NAME                       ")
      _sql_query.AppendLine("       , PT_DESCRIPTION                ")
      _sql_query.AppendLine("       , PT_ACTIVE                     ")
      _sql_query.AppendLine("       , PT_PATTERN                    ")
      _sql_query.AppendLine("       , PT_AL_CODE                    ")
      _sql_query.AppendLine("       , PT_AL_NAME                    ")
      _sql_query.AppendLine("       , PT_AL_DESCRIPTION             ")
      _sql_query.AppendLine("       , PT_AL_SEVERITY                ")
      _sql_query.AppendLine("       , PT_TYPE                       ")
      _sql_query.AppendLine("       , PT_SOURCE                     ")
      _sql_query.AppendLine("       , PT_LAST_FIND                  ")
      _sql_query.AppendLine("       , PT_DETECTIONS                 ")
      _sql_query.AppendLine("       , PT_RESTRICTED_TO_TERMINAL_LIST")
      _sql_query.AppendLine("       , PT_SCHEDULE_TIME_FROM         ")
      _sql_query.AppendLine("       , PT_SCHEDULE_TIME_TO           ")
      _sql_query.AppendLine("       , PT_LIFE_TIME                  ")
      _sql_query.AppendLine("       , PT_ACTIVE_MAILING             ")
      _sql_query.AppendLine("       , PT_ADDRESS_LIST               ")
      _sql_query.AppendLine("       , PT_SUBJECT                    ")

      _sql_query.AppendLine("  FROM   PATTERNS                      ")
      _sql_query.AppendLine(" WHERE   PT_ID = " + Item.pattern_id.ToString())

      _data_table = GUI_GetTableUsingSQL(_sql_query.ToString(), 50)
      If IsNothing(_data_table) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      If _data_table.Rows.Count() <> 1 Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      Item.pattern_id = NullTrim(_data_table.Rows(0).Item("PT_ID"))
      Item.name = _data_table.Rows(0).Item("PT_NAME")
      Item.description = _data_table.Rows(0).Item("PT_DESCRIPTION")
      Item.active = _data_table.Rows(0).Item("PT_ACTIVE")

      'PT_PATTERN
      _xml = Nothing
      If _data_table.Rows(0).Item("PT_PATTERN") IsNot DBNull.Value Then
        _xml = _data_table.Rows(0).Item("PT_PATTERN")
      End If
      Item.pattern.FromXml(_xml)

      Item.alarm_code = _data_table.Rows(0).Item("PT_AL_CODE")
      Item.alarm_name = _data_table.Rows(0).Item("PT_AL_NAME")
      Item.alarm_description = _data_table.Rows(0).Item("PT_AL_DESCRIPTION")
      Item.alarm_severity = _data_table.Rows(0).Item("PT_AL_SEVERITY")
      Item.type = _data_table.Rows(0).Item("PT_TYPE")
      Item.source = _data_table.Rows(0).Item("PT_SOURCE")

      If _data_table.Rows(0).Item("PT_LAST_FIND") IsNot DBNull.Value Then
        Item.last_occurrence = _data_table.Rows(0).Item("PT_LAST_FIND")
      End If

      Item.num_occurrences = _data_table.Rows(0).Item("PT_DETECTIONS")

      'PT_RESTRICTED_TO_TERMINAL_LIST
      _xml = Nothing
      If _data_table.Rows(0).Item("PT_RESTRICTED_TO_TERMINAL_LIST") IsNot DBNull.Value Then
        _xml = _data_table.Rows(0).Item("PT_RESTRICTED_TO_TERMINAL_LIST")
      End If
      Item.terminal_list.FromXml(_xml)

      Item.scheduled_time_from = -1
      Item.scheduled_time_to = -1
      If _data_table.Rows(0).Item("PT_SCHEDULE_TIME_FROM") IsNot DBNull.Value Then
        Item.scheduled_time_from = _data_table.Rows(0).Item("PT_SCHEDULE_TIME_FROM")
      End If

      If _data_table.Rows(0).Item("PT_SCHEDULE_TIME_TO") IsNot DBNull.Value Then
        Item.scheduled_time_to = _data_table.Rows(0).Item("PT_SCHEDULE_TIME_TO")
      End If

      Item.life_time = _data_table.Rows(0).Item("PT_LIFE_TIME")

      Item.mailing_active = _data_table.Rows(0).Item("PT_ACTIVE_MAILING")
      If _data_table.Rows(0).Item("PT_ADDRESS_LIST") IsNot DBNull.Value Then
        Item.mailing_address_list = _data_table.Rows(0).Item("PT_ADDRESS_LIST")
      End If
      If _data_table.Rows(0).Item("PT_SUBJECT") IsNot DBNull.Value Then
        Item.mailing_subject = _data_table.Rows(0).Item("PT_SUBJECT")
      End If

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

  End Function ' ReadPattern

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes a gift object from DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - GiftId
  '         - Context
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function DeletePattern(ByVal PatternId As Integer, ByVal Context As Integer) As Integer
    Dim _sql_query As StringBuilder
    Dim _sql_tx As System.Data.SqlClient.SqlTransaction = Nothing

    '     - Remove the gift item from the PATTERNS table
    Try
      If Not GUI_BeginSQLTransaction(_sql_tx) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      _sql_query = New StringBuilder

      _sql_query.AppendLine("DELETE   PATTERNS")
      _sql_query.AppendLine(" WHERE   PT_ID = " + PatternId.ToString())

      If Not GUI_SQLExecuteNonQuery(_sql_query.ToString(), _sql_tx) Then
        ' Rollback  
        GUI_EndSQLTransaction(_sql_tx, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      ' Commit
      If Not GUI_EndSQLTransaction(_sql_tx, True) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

    Catch ex As Exception
      ' Rollback  
      GUI_EndSQLTransaction(_sql_tx, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function ' DeletePattern

  '----------------------------------------------------------------------------
  ' PURPOSE : Adds a gift object into DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - Item
  '         - Context
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function InsertPattern(ByVal Item As TYPE_PATTERN, ByVal Context As Integer) As Integer
    Dim _sql_query As StringBuilder
    Dim _sql_tx As System.Data.SqlClient.SqlTransaction = Nothing
    Dim _data_table As DataTable
    Dim _sql_command As SqlCommand
    Dim _rc As Integer
    Dim _xml As String
    Dim _output_param As SqlParameter
    Dim PatternId As Int64

    ' Steps
    '     - Check whether there is no other pattern with the same name 
    '     - Insert the pattern item to the PATTERNS table

    _rc = 0

    Try
      '     - Check whether there is no other pattern with the same name 
      _sql_query = New StringBuilder

      _sql_query.AppendLine("SELECT   COUNT (*)         ")
      _sql_query.AppendLine(" FROM   PATTERNS           ")
      _sql_query.AppendLine(" WHERE   PT_NAME = @pName  ")

      _sql_command = New SqlCommand(_sql_query.ToString())
      _sql_command.Parameters.Add("@pName", SqlDbType.NVarChar).Value = Item.name
      _data_table = GUI_GetTableUsingCommand(_sql_command, 10)

      If Not IsNothing(_data_table) Then
        If _data_table.Rows.Count() > 0 Then
          If _data_table.Rows(0).Item(0) > 0 Then
            Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
          End If
        End If
      End If

      If Not GUI_BeginSQLTransaction(_sql_tx) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      _sql_query = New StringBuilder

      _sql_query.AppendLine("INSERT INTO PATTERNS ( PT_NAME                       ")
      _sql_query.AppendLine("                     , PT_DESCRIPTION                ")
      _sql_query.AppendLine("                     , PT_ACTIVE                     ")
      _sql_query.AppendLine("                     , PT_PATTERN                    ")
      _sql_query.AppendLine("                     , PT_AL_CODE                    ")
      _sql_query.AppendLine("                     , PT_AL_NAME                    ")
      _sql_query.AppendLine("                     , PT_AL_DESCRIPTION             ")
      _sql_query.AppendLine("                     , PT_AL_SEVERITY                ")
      _sql_query.AppendLine("                     , PT_TYPE                       ")
      _sql_query.AppendLine("                     , PT_SOURCE                     ")
      _sql_query.AppendLine("                     , PT_DETECTIONS                 ")
      _sql_query.AppendLine("                     , PT_RESTRICTED_TO_TERMINAL_LIST")
      _sql_query.AppendLine("                     , PT_SCHEDULE_TIME_FROM         ")
      _sql_query.AppendLine("                     , PT_SCHEDULE_TIME_TO           ")
      _sql_query.AppendLine("                     , PT_LIFE_TIME                  ")
      _sql_query.AppendLine("                     , PT_ACTIVE_MAILING             ")
      _sql_query.AppendLine("                     , PT_ADDRESS_LIST               ")
      _sql_query.AppendLine("                     , PT_SUBJECT                    ")
      _sql_query.AppendLine("                     )                               ")
      _sql_query.AppendLine("             VALUES ( @pName                         ")
      _sql_query.AppendLine("                    , @pDescription                  ")
      _sql_query.AppendLine("                    , @pActive                       ")
      _sql_query.AppendLine("                    , @pPattern                      ")
      _sql_query.AppendLine("                    , @pAlarmCode                    ")
      _sql_query.AppendLine("                    , @pAlarmName                    ")
      _sql_query.AppendLine("                    , @pAlarmDescription             ")
      _sql_query.AppendLine("                    , @pAlarmSeverity                ")
      _sql_query.AppendLine("                    , @pType                         ")
      _sql_query.AppendLine("                    , @pSource                       ")
      _sql_query.AppendLine("                    , 0                              ")
      _sql_query.AppendLine("                    , @pTerminalList                 ")
      _sql_query.AppendLine("                    , @pScheduledTimeFrom            ")
      _sql_query.AppendLine("                    , @pScheduledTimeTo              ")
      _sql_query.AppendLine("                    , @pLifeTime                     ")
      _sql_query.AppendLine("                    , @pMailingActive                ")
      _sql_query.AppendLine("                    , @pMailingAddress               ")
      _sql_query.AppendLine("                    , @pMailingSubject               ")
      _sql_query.AppendLine("                    )                                ")
      _sql_query.AppendLine(" SET @pPatternId = SCOPE_IDENTITY()                  ")

      _sql_command = New SqlCommand(_sql_query.ToString(), _sql_tx.Connection, _sql_tx)
      _sql_command.Parameters.Add("@pName", SqlDbType.NVarChar, CLASS_PATTERN.MAX_NAME_LEN).Value = Item.name
      _sql_command.Parameters.Add("@pDescription", SqlDbType.NVarChar, CLASS_PATTERN.MAX_DESCRIPTION_LEN).Value = Item.description
      _sql_command.Parameters.Add("@pActive", SqlDbType.Bit).Value = Item.active
      _sql_command.Parameters.Add("@pPattern", SqlDbType.Xml).Value = Item.pattern.ToXml
      _sql_command.Parameters.Add("@pAlarmCode", SqlDbType.Int).Value = 0
      _sql_command.Parameters.Add("@pAlarmName", SqlDbType.NVarChar, CLASS_PATTERN.MAX_NAME_LEN).Value = Item.alarm_name
      _sql_command.Parameters.Add("@pAlarmDescription", SqlDbType.NVarChar, CLASS_PATTERN.MAX_DESCRIPTION_LEN).Value = Item.alarm_description
      _sql_command.Parameters.Add("@pAlarmSeverity", SqlDbType.Int).Value = Item.alarm_severity
      _sql_command.Parameters.Add("@pType", SqlDbType.Int).Value = Item.type
      _sql_command.Parameters.Add("@pSource", SqlDbType.Int).Value = Item.source

      _xml = Item.terminal_list.ToXml()
      If Not String.IsNullOrEmpty(_xml) Then
        _sql_command.Parameters.Add("@pTerminalList", SqlDbType.Xml).Value = _xml
      Else
        _sql_command.Parameters.Add("@pTerminalList", SqlDbType.Xml).Value = DBNull.Value
      End If

      If Item.scheduled_time_from = -1 Then
        _sql_command.Parameters.Add("@pScheduledTimeFrom", SqlDbType.Int).Value = DBNull.Value
      Else
        _sql_command.Parameters.Add("@pScheduledTimeFrom", SqlDbType.Int).Value = Item.scheduled_time_from
      End If

      If Item.scheduled_time_to = -1 Then
        _sql_command.Parameters.Add("@pScheduledTimeTo", SqlDbType.Int).Value = DBNull.Value
      Else
        _sql_command.Parameters.Add("@pScheduledTimeTo", SqlDbType.Int).Value = Item.scheduled_time_to
      End If

      _sql_command.Parameters.Add("@pLifeTime", SqlDbType.Int).Value = Item.life_time

      _sql_command.Parameters.Add("@pMailingActive", SqlDbType.Bit).Value = Item.mailing_active
      _sql_command.Parameters.Add("@pMailingAddress", SqlDbType.NVarChar, CLASS_PATTERN.MAX_MAILING_ADDRESS_LEN).Value = Item.mailing_address_list
      _sql_command.Parameters.Add("@pMailingSubject", SqlDbType.NVarChar, CLASS_PATTERN.MAX_MAILING_SUBJECT_LEN).Value = Item.mailing_subject

      _output_param = New SqlParameter("@pPatternId", SqlDbType.BigInt)
      _output_param.Direction = ParameterDirection.Output

      _sql_command.Parameters.Add(_output_param)

      _rc = _sql_command.ExecuteNonQuery()

      If _output_param.Value IsNot DBNull.Value Then
        PatternId = _output_param.Value
      End If

      If _rc <> 1 Then
        ' Rollback  
        GUI_EndSQLTransaction(_sql_tx, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      ' Insert the related record on ALARM_CATALOG
      If Not Alarm.InsertAlarmCatalog(PatternId + Alarm.PatternIDOffset, Item.alarm_name, Item.alarm_description, _sql_tx) Then
        ' Rollback  
        GUI_EndSQLTransaction(_sql_tx, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      ' Update the pattern with the proper alarm code value
      _sql_query = New StringBuilder

      _sql_query.AppendLine("UPDATE   PATTERNS                 ")
      _sql_query.AppendLine("   SET   PT_AL_CODE  = @pAlarmCode")
      _sql_query.AppendLine(" WHERE   PT_ID       = @pPatternId")

      _sql_command = New SqlCommand(_sql_query.ToString(), _sql_tx.Connection, _sql_tx)

      _sql_command.Parameters.Add("@pAlarmCode", SqlDbType.Int).Value = PatternId + Alarm.PatternIDOffset
      _sql_command.Parameters.Add("@pPatternId", SqlDbType.BigInt).Value = PatternId

      _rc = _sql_command.ExecuteNonQuery()

      If _rc <> 1 Then
        ' Rollback  
        GUI_EndSQLTransaction(_sql_tx, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      ' Commit
      If Not GUI_EndSQLTransaction(_sql_tx, True) Then
        ' Rollback  
        GUI_EndSQLTransaction(_sql_tx, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

    Catch ex As Exception
      ' Rollback  
      GUI_EndSQLTransaction(_sql_tx, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function ' InsertGift

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given gift object into the DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - Item
  '         - Context
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function UpdatePattern(ByVal Item As TYPE_PATTERN, ByVal Context As Integer) As Integer
    Dim _sql_query As StringBuilder
    Dim _sql_tx As System.Data.SqlClient.SqlTransaction = Nothing
    Dim _data_table As DataTable
    Dim _sql_command As SqlCommand
    Dim _xml As String
    Dim _rc As Integer

    ' Steps
    '     - Check whether there is no other pattern with the same name 
    '     - Update the pattern's data 

    _rc = 0

    Try
      '   - Check whether there is no other pattern with the same name 

      _sql_query = New StringBuilder

      _sql_query.AppendLine("SELECT   COUNT (*)             ")
      _sql_query.AppendLine("  FROM   PATTERNS              ")
      _sql_query.AppendLine(" WHERE   PT_NAME =  @pName     ")
      _sql_query.AppendLine("   AND   PT_ID <> @pPatternId  ")

      _sql_command = New SqlCommand(_sql_query.ToString())
      _sql_command.Parameters.Add("@pName", SqlDbType.NVarChar).Value = Item.name
      _sql_command.Parameters.Add("@pPatternId", SqlDbType.BigInt).Value = Item.pattern_id
      _data_table = GUI_GetTableUsingCommand(_sql_command, 10)

      If Not IsNothing(_data_table) Then
        If _data_table.Rows.Count() > 0 Then
          If _data_table.Rows(0).Item(0) > 0 Then
            Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
          End If
        End If
      End If

      '   - Update the patterns's data 
      If Not GUI_BeginSQLTransaction(_sql_tx) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      _sql_query = New StringBuilder

      _sql_query.AppendLine("UPDATE   PATTERNS                                              ")
      _sql_query.AppendLine("   SET   PT_NAME                          = @pName             ")
      _sql_query.AppendLine("       , PT_DESCRIPTION                   = @pDescription      ")
      _sql_query.AppendLine("       , PT_ACTIVE                        = @pActive           ")
      _sql_query.AppendLine("       , PT_PATTERN                       = @pPattern          ")
      _sql_query.AppendLine("       , PT_AL_NAME                       = @pAlarmName        ")
      _sql_query.AppendLine("       , PT_AL_DESCRIPTION                = @pAlarmDescription ")
      _sql_query.AppendLine("       , PT_AL_SEVERITY                   = @pAlarmSeverity    ")
      _sql_query.AppendLine("       , PT_TYPE                          = @pType             ")
      _sql_query.AppendLine("       , PT_SOURCE                        = @pSource           ")
      _sql_query.AppendLine("       , PT_RESTRICTED_TO_TERMINAL_LIST   = @pTerminalList     ")
      _sql_query.AppendLine("       , PT_SCHEDULE_TIME_FROM            = @pScheduledTimeFrom")
      _sql_query.AppendLine("       , PT_SCHEDULE_TIME_TO              = @pScheduledTimeTo  ")
      _sql_query.AppendLine("       , PT_LIFE_TIME                     = @pLifeTime         ")
      _sql_query.AppendLine("       , PT_ACTIVE_MAILING                = @pMailingActive    ")
      _sql_query.AppendLine("       , PT_ADDRESS_LIST                  = @pMailingAddress   ")
      _sql_query.AppendLine("       , PT_SUBJECT                       = @pMailingSubject   ")

      _sql_query.AppendLine(" WHERE   PT_ID                            = @pPatternId        ")

      _sql_command = New SqlCommand(_sql_query.ToString(), _sql_tx.Connection, _sql_tx)
      _sql_command.Parameters.Add("@pName", SqlDbType.NVarChar, CLASS_PATTERN.MAX_NAME_LEN).Value = Item.name
      _sql_command.Parameters.Add("@pDescription", SqlDbType.NVarChar, CLASS_PATTERN.MAX_DESCRIPTION_LEN).Value = Item.description
      _sql_command.Parameters.Add("@pActive", SqlDbType.Bit).Value = Item.active

      _xml = Item.pattern.ToXml()
      If Not String.IsNullOrEmpty(_xml) Then
        _sql_command.Parameters.Add("@pPattern", SqlDbType.Xml).Value = _xml
      Else
        _sql_command.Parameters.Add("@pPattern", SqlDbType.Xml).Value = DBNull.Value
      End If

      _sql_command.Parameters.Add("@pAlarmName", SqlDbType.NVarChar, CLASS_PATTERN.MAX_NAME_LEN).Value = Item.alarm_name
      _sql_command.Parameters.Add("@pAlarmDescription", SqlDbType.NVarChar, CLASS_PATTERN.MAX_DESCRIPTION_LEN).Value = Item.alarm_description
      _sql_command.Parameters.Add("@pAlarmSeverity", SqlDbType.Int).Value = Item.alarm_severity
      _sql_command.Parameters.Add("@pType", SqlDbType.Int).Value = Item.type
      _sql_command.Parameters.Add("@pSource", SqlDbType.Int).Value = Item.source

      _xml = Item.terminal_list.ToXml()
      If Not String.IsNullOrEmpty(_xml) Then
        _sql_command.Parameters.Add("@pTerminalList", SqlDbType.Xml).Value = _xml
      Else
        _sql_command.Parameters.Add("@pTerminalList", SqlDbType.Xml).Value = DBNull.Value
      End If

      If Item.scheduled_time_from = -1 Then
        _sql_command.Parameters.Add("@pScheduledTimeFrom", SqlDbType.Int).Value = DBNull.Value
      Else
        _sql_command.Parameters.Add("@pScheduledTimeFrom", SqlDbType.Int).Value = Item.scheduled_time_from
      End If

      If Item.scheduled_time_to = -1 Then
        _sql_command.Parameters.Add("@pScheduledTimeTo", SqlDbType.Int).Value = DBNull.Value
      Else
        _sql_command.Parameters.Add("@pScheduledTimeTo", SqlDbType.Int).Value = Item.scheduled_time_to
      End If

      _sql_command.Parameters.Add("@pLifeTime", SqlDbType.Int).Value = Item.life_time

      _sql_command.Parameters.Add("@pMailingActive", SqlDbType.Bit).Value = Item.mailing_active
      If String.IsNullOrEmpty(Item.mailing_address_list) Then
        _sql_command.Parameters.Add("@pMailingAddress", SqlDbType.NVarChar, CLASS_PATTERN.MAX_MAILING_ADDRESS_LEN).Value = DBNull.Value
      Else
        _sql_command.Parameters.Add("@pMailingAddress", SqlDbType.NVarChar, CLASS_PATTERN.MAX_MAILING_ADDRESS_LEN).Value = Item.mailing_address_list
      End If
      If String.IsNullOrEmpty(Item.mailing_subject) Then
        _sql_command.Parameters.Add("@pMailingSubject", SqlDbType.NVarChar, CLASS_PATTERN.MAX_MAILING_SUBJECT_LEN).Value = DBNull.Value
      Else
        _sql_command.Parameters.Add("@pMailingSubject", SqlDbType.NVarChar, CLASS_PATTERN.MAX_MAILING_SUBJECT_LEN).Value = Item.mailing_subject
      End If

      _sql_command.Parameters.Add("@pPatternId", SqlDbType.BigInt).Value = Item.pattern_id
      _rc = _sql_command.ExecuteNonQuery()

      If _rc <> 1 Then
        ' Rollback  
        GUI_EndSQLTransaction(_sql_tx, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      If Not Alarm.UpdateAlarmCatalog(Item.pattern_id + Alarm.PatternIDOffset, Item.alarm_name, Item.alarm_description, _sql_tx) Then
        ' Rollback  
        GUI_EndSQLTransaction(_sql_tx, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      ' Commit
      If Not GUI_EndSQLTransaction(_sql_tx, True) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

    Catch ex As Exception
      ' Rollback  
      GUI_EndSQLTransaction(_sql_tx, False)
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function ' UpdateGift

#End Region ' Private Functions

#Region " Public Functions "

  Public Shared Function PatternTypeDescription(ByVal Type As PATTERN_TYPE) As String
    Select Case Type
      Case PATTERN_TYPE.CONSECUTIVE
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4948) ' 4948 "Consecutive"
      Case PATTERN_TYPE.NO_CONSECUTIVE_WITH_ORDER
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4949) ' 4949 "No consecutive with order"
      Case PATTERN_TYPE.NO_ORDER
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4950) ' 4950 "With no order"
      Case PATTERN_TYPE.NO_ORDER_WITH_REPETITION
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4951) ' 4951 "With no order and repetition"
      Case Else
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(786)  '  786 "Unknown"
    End Select
  End Function ' PatternTypeDescription

#End Region

End Class
