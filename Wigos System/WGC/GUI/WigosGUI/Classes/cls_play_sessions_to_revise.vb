'-------------------------------------------------------------------
' Copyright � 2007-2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   CLASS_PLAY_SESSIONS_TO_REVISE
' DESCRIPTION:   
' AUTHOR:        Alberto Marcos
' CREATION DATE: 07-OCT-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 07-OCT-2013  AMF    Initial version
' 05-FEB-2015 JRC    PBI 7909: Multiple Bucets
'--------------------------------------------------------------------
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common.MultiPromos
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common


Public Class CLASS_PLAY_SESSIONS_TO_REVISE
  Inherits CLASS_BASE

  Public Class TYPE_PLAY_SESSION_TO_REVISE
    Public dt_play_sessions As DataTable
    Public dt_accounts As DataTable
  End Class

#Region " Constants "

  Private Const COLUMN_PLAY_SESSION_ID As Integer = 0
  Private Const COLUMN_ACCOUNT_ID As Integer = 1
  Private Const COLUMN_TERMINAL_NAME As Integer = 3
  Private Const COLUMN_COMPUTED_POINTS As Integer = 4
  Private Const COLUMN_AWARDED_POINTS As Integer = 5
  Private Const COLUMN_AWARDED_POINTS_STATUS As Integer = 6
  Private Const COLUMN_TERMINAL_ID As Integer = 7
  Private Const COLUMN_HOLDER_NAME As Integer = 8

#End Region ' Constants

#Region " Members "

  Protected m_account_id_list As New List(Of String)
  Protected m_revise As New TYPE_PLAY_SESSION_TO_REVISE

#End Region 'Members

#Region "Properties"

  Public Property AccountIdList() As List(Of String)
    Get
      Return m_account_id_list
    End Get

    Set(ByVal Value As List(Of String))
      m_account_id_list = Value
    End Set

  End Property

  Public Property DtPlaySessions() As DataTable
    Get
      Return m_revise.dt_play_sessions
    End Get

    Set(ByVal Value As DataTable)
      m_revise.dt_play_sessions = Value
    End Set

  End Property

  Public Property DtAccounts() As DataTable
    Get
      Return m_revise.dt_accounts
    End Get

    Set(ByVal Value As DataTable)
      m_revise.dt_accounts = Value
    End Set

  End Property

#End Region ' Properties

#Region "Overrides"

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _str_status As String = ""

    ' Create a new auditor_code
    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_ACCOUNT)

    For Each _row As DataRow In DtPlaySessions.Rows

      Call _auditor_data.SetName(GLB_NLS_GUI_INVOICING.Id(115), _row.Item(COLUMN_ACCOUNT_ID).ToString())

      ' Awarded Points      
      Call _auditor_data.SetField(0, GUI_FormatNumber(_row.Item(COLUMN_AWARDED_POINTS)), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2578, _row.Item(COLUMN_PLAY_SESSION_ID).ToString()))

      ' Status
      Select Case _row.Item(COLUMN_AWARDED_POINTS_STATUS)
        Case AwardPointsStatus.Default
          _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2753)
        Case AwardPointsStatus.CalculatedAndReward
          _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2754)
        Case AwardPointsStatus.Pending
          _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2755)
        Case AwardPointsStatus.Manual
          _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2756)
        Case AwardPointsStatus.ManualCalculated
          _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2757)
      End Select
      Call _auditor_data.SetField(0, _str_status, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2759, _row.Item(COLUMN_PLAY_SESSION_ID).ToString()))
    Next

    Return _auditor_data
  End Function ' AuditorData

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function ' DB_Delete

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function ' DB_Insert

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    Me.AccountIdList = ObjectId

    _rc = PlaySessionsToRevise_DbRead(m_revise)

    Select Case _rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Read

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _am_table As AccountMovementsTable
    Dim _fin_points As Decimal

    Using _db_trx As New DB_TRX()
      For Each _row As DataRow In DtPlaySessions.Rows
        If _row(COLUMN_AWARDED_POINTS_STATUS) = AwardPointsStatus.Manual Or _
           _row(COLUMN_AWARDED_POINTS_STATUS) = AwardPointsStatus.ManualCalculated Then
          ' Update account
          _am_table = New AccountMovementsTable(_row(COLUMN_TERMINAL_ID), _row(COLUMN_TERMINAL_NAME).ToString(), _row(COLUMN_PLAY_SESSION_ID))
          If Not Trx_UpdateAccountPoints(_row(COLUMN_ACCOUNT_ID), _row(COLUMN_AWARDED_POINTS), _fin_points, _db_trx.SqlTransaction) Then

            Return ENUM_STATUS.STATUS_ERROR
          End If
          ' Add movement
          'If Not _am_table.Add(0, _row(COLUMN_ACCOUNT_ID), MovementType.PointsAwarded, _fin_points - _row(COLUMN_AWARDED_POINTS), 0, _row(COLUMN_AWARDED_POINTS), _fin_points) Then
          ' JRC:MovementType.PointsAwarded

          If Not _am_table.Add(0, _row(COLUMN_ACCOUNT_ID), MovementType.MULTIPLE_BUCKETS_EndSession + Buckets.BucketId.RankingLevelPoints, _fin_points - _row(COLUMN_AWARDED_POINTS), 0, _row(COLUMN_AWARDED_POINTS), _fin_points) Then
            Return ENUM_STATUS.STATUS_ERROR
          End If
          If Not _am_table.Save(_db_trx.SqlTransaction) Then

            Return ENUM_STATUS.STATUS_ERROR
          End If
          ' Update play session
          If (PlaySessionsToRevise_DbUpdate(_row(COLUMN_AWARDED_POINTS), _row(COLUMN_AWARDED_POINTS_STATUS), _
                                               _row(COLUMN_PLAY_SESSION_ID), _db_trx.SqlTransaction, SqlCtx) <> 0) Then

            Return ENUM_STATUS.STATUS_ERROR
          End If
          ' Commit
          _db_trx.Commit()
        End If
      Next
    End Using

  End Function ' DB_Update

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _temp_play_session As CLASS_PLAY_SESSIONS_TO_REVISE

    _temp_play_session = New CLASS_PLAY_SESSIONS_TO_REVISE

    _temp_play_session.DtPlaySessions = Me.DtPlaySessions.Copy
    _temp_play_session.DtAccounts = Me.DtAccounts.Copy

    Return _temp_play_session

  End Function ' Duplicate

#End Region ' Overrides

#Region " Private Functions "

  Private Function PlaySessionsToRevise_DbRead(ByVal pRevise As TYPE_PLAY_SESSION_TO_REVISE) As Integer
    Dim _sb As StringBuilder
    Dim _adapter As SqlDataAdapter
    Dim _row_insert As DataRow
    Dim _account_id_insert As Long
    Dim _computed_points_insert As Double
    Dim _awarded_points_insert As Double
    Dim _total_play_sessions_insert As Integer
    Dim _holder_name_insert As String

    Try
      _sb = New StringBuilder()

      _sb.AppendLine("  SELECT   PS_PLAY_SESSION_ID ")
      _sb.AppendLine("         , PS_ACCOUNT_ID ")
      _sb.AppendLine("         , TE_PROVIDER_ID ")
      _sb.AppendLine("         , TE_NAME ")
      _sb.AppendLine("         , PS_COMPUTED_POINTS ")
      _sb.AppendLine("         , PS_AWARDED_POINTS ")
      _sb.AppendLine("         , PS_AWARDED_POINTS_STATUS ")
      _sb.AppendLine("         , TE_TERMINAL_ID ")
      _sb.AppendLine("         , AC_HOLDER_NAME ")
      _sb.AppendLine("    FROM   PLAY_SESSIONS ")
      _sb.AppendLine("   INNER   JOIN ACCOUNTS ON PS_ACCOUNT_ID = AC_ACCOUNT_ID ")
      _sb.AppendLine("    LEFT   OUTER JOIN TERMINALS ON TE_TERMINAL_ID = PS_TERMINAL_ID ")
      _sb.AppendLine("   WHERE   PS_AWARDED_POINTS_STATUS = @pAwardPointsStatus")
      _sb.AppendLine("     AND   PS_STATUS <> @pPlaySessionStatus")
      _sb.AppendLine("     AND   PS_ACCOUNT_ID IN ( " & String.Join(",", AccountIdList.ToArray()) & " ) ")
      _sb.AppendLine("ORDER BY   PS_ACCOUNT_ID, PS_PLAY_SESSION_ID ")

      Using _db_trx As New DB_TRX()
        Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _cmd.Parameters.Add("@pAwardPointsStatus", SqlDbType.Int).Value = AwardPointsStatus.Pending
          _cmd.Parameters.Add("@pPlaySessionStatus", SqlDbType.Int).Value = PlaySessionStatus.Opened

          _adapter = New SqlDataAdapter(_cmd)
          pRevise.dt_play_sessions = New DataTable()
          _adapter.Fill(pRevise.dt_play_sessions)
        End Using
      End Using

      If IsNothing(pRevise.dt_play_sessions) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      If pRevise.dt_play_sessions.Rows.Count() < 1 Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      'DtAccounts
      pRevise.dt_accounts = New DataTable()
      With pRevise.dt_accounts
        .Columns.Add("AC_ACCOUNT_ID")
        .Columns.Add("AC_HOLDER_NAME")
        .Columns.Add("PS_COMPUTED_POINTS")
        .Columns.Add("PS_AWARDED_POINTS")
        .Columns.Add("TOTAL_PLAY_SESSIONS")

        .PrimaryKey = New DataColumn() {.Columns(0)}
      End With

      _account_id_insert = 0
      _computed_points_insert = 0
      _awarded_points_insert = 0
      _total_play_sessions_insert = 0
      _holder_name_insert = ""

      For Each _row As DataRow In DtPlaySessions.Rows

        If (_account_id_insert <> _row(COLUMN_ACCOUNT_ID) And (_account_id_insert <> 0)) Then
          _row_insert = pRevise.dt_accounts.NewRow()
          _row_insert("AC_ACCOUNT_ID") = _account_id_insert
          _row_insert("AC_HOLDER_NAME") = _holder_name_insert
          _row_insert("PS_COMPUTED_POINTS") = _computed_points_insert
          _row_insert("PS_AWARDED_POINTS") = _awarded_points_insert
          _row_insert("TOTAL_PLAY_SESSIONS") = _total_play_sessions_insert

          pRevise.dt_accounts.Rows.Add(_row_insert)

          _computed_points_insert = 0
          _awarded_points_insert = 0
          _total_play_sessions_insert = 0
          _holder_name_insert = ""
        End If

        _account_id_insert = _row(COLUMN_ACCOUNT_ID)
        _computed_points_insert += _row(COLUMN_COMPUTED_POINTS)
        _awarded_points_insert += _row(COLUMN_AWARDED_POINTS)
        _total_play_sessions_insert += 1
        _holder_name_insert = _row(COLUMN_HOLDER_NAME)
      Next

      _row_insert = pRevise.dt_accounts.NewRow()
      _row_insert("AC_ACCOUNT_ID") = _account_id_insert
      _row_insert("AC_HOLDER_NAME") = _holder_name_insert
      _row_insert("PS_COMPUTED_POINTS") = _computed_points_insert
      _row_insert("PS_AWARDED_POINTS") = _awarded_points_insert
      _row_insert("TOTAL_PLAY_SESSIONS") = _total_play_sessions_insert

      pRevise.dt_accounts.Rows.Add(_row_insert)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Catch _ex As Exception
      Log.Exception(_ex)
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

  End Function ' PlaySessionsToRevise_DbRead

  Private Function PlaySessionsToRevise_DbUpdate(ByVal AwardedPoints As Double, ByVal AwardedPointsStatus As Integer, _
                                                 ByVal PlaySessionId As Long, ByVal SqlTrans As SqlTransaction, ByVal Context As Integer) As Integer
    Dim _sb As StringBuilder

    Try

      _sb = New StringBuilder()

      Using _da As New SqlDataAdapter()

        _sb.AppendLine(" UPDATE   PLAY_SESSIONS ")
        _sb.AppendLine("    SET   PS_AWARDED_POINTS = @pAwardedPoints ")
        _sb.AppendLine("        , PS_AWARDED_POINTS_STATUS = @pAwardedPointsStatus ")
        _sb.AppendLine("  WHERE   PS_PLAY_SESSION_ID = @pPlaySessionId")

        Using _cmd As New SqlCommand(_sb.ToString, SqlTrans.Connection, SqlTrans)

          _cmd.Parameters.Add("@pAwardedPoints", SqlDbType.Money).Value = AwardedPoints
          _cmd.Parameters.Add("@pAwardedPointsStatus", SqlDbType.Int).Value = AwardedPointsStatus
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId

          If _cmd.ExecuteNonQuery() <> 1 Then
            Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
          End If

        End Using

      End Using

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Catch ex As Exception

    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

  End Function ' PlaySessionsToRevise_DbUpdate

#End Region ' Private Functions

End Class