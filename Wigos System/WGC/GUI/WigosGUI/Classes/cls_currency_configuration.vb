'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_currency_configuration.vb
'
' DESCRIPTION : Currency Configuration Data class
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------- ------ -----------------------------------------------
' 30-JUL-2013 AMF    Initial version   
' 10-SEP-2013 ICS    Added suport for Check recharges
' 28-NOV-2013 JCA    Delete 'X01' from datatable (Cage.CHIPS_ISO_CODE)
' 02-DEC-2013 CCG    Fixed bug WIG-451 Check foreign currency before initialice the values
' 12-DEC-2013 FBA    Fixed bug WIG-481
' 10-APR-2014 DLL    Added two columns for old and new Decimals
' 25-JUL-2014 DLL    Chips are moved to a separate table
' 18-SEP-2014 DLL    Added new movement Cash Advance
' 03-OCT-2014 DLL    Fixed bug WIG-1404 when chips enabled all denominations enabled
' 05-MAY-2016 ETP    PBI 12691: Added Multidivisa config.
' 29-JUN-2016 ETP    Fixed bug 14938: Countr: Excepci�n no controlada al acceder a "operaciones con tarjeta/cheque/divisa"
'--------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports WSI.Common
Imports System.Text
Imports WSI.Common.PinPad

Public Class CLS_CURRENCY_CONFIGURATION
  Inherits CLASS_BASE

#Region " Constants "

#End Region  ' Constants 

#Region " Structures "
  Public Class TYPE_CURRENCY_CONFIGURATION
    Public currencies As DataTable
    Public foreigncurrencies As DataTable
    Public currency_ISO_Code_GP As String
    Public currencies_accepted_GP As String
    Public bank_card_currency_exchange_list As Dictionary(Of bank_card, CURRENCIES_ENABLED)
    Public bank_card_enabled As Boolean
    Public foreign_currency_ISO_Code_GP As String
    Public foreign_currency_exchange_list As Dictionary(Of String, CURRENCIES_ENABLED)
    Public foreign_currency_voucher_title_GP As String
    Public bank_card_voucher_title_GP As String
    Public hide_bank_card_voucher_GP As Boolean
    Public hide_foreign_currency_voucher_GP As Boolean
    Public check_currency_exchange As CurrencyExchange
    Public check_voucher_title_GP As String
    Public hide_check_voucher_GP As Boolean

    Public specific_bank_card_types_GP As Boolean
    Public edit_bank_card_transaction_data_GP As Boolean
    Public edit_bank_check_transaction_data_GP As Boolean
    Public hide_bank_card_voucher_cash_advance_GP As Boolean
    Public bank_card_voucher_cash_advance_title_GP As String
    Public hide_check_voucher_cash_advance_GP As Boolean
    Public check_voucher_cash_advance_title_GP As String

    Public commission_to_company_b_enabled As Boolean
    Public bank_card_voucher_commission As String
    Public check_voucher_commission As String
    Public currency_voucher_commission As String

    Sub New()
      currencies = New DataTable()
      foreigncurrencies = New DataTable()
      currency_ISO_Code_GP = String.Empty
      currencies_accepted_GP = String.Empty
      foreign_currency_ISO_Code_GP = String.Empty
      bank_card_enabled = False
      bank_card_currency_exchange_list = New Dictionary(Of bank_card, CURRENCIES_ENABLED)
      foreign_currency_exchange_list = New Dictionary(Of String, CURRENCIES_ENABLED)
      foreign_currency_voucher_title_GP = String.Empty
      bank_card_voucher_title_GP = String.Empty
      hide_bank_card_voucher_GP = False
      hide_foreign_currency_voucher_GP = False
      check_currency_exchange = New CurrencyExchange
      check_voucher_title_GP = False
      hide_check_voucher_GP = False
      specific_bank_card_types_GP = False
      edit_bank_card_transaction_data_GP = False
      edit_bank_check_transaction_data_GP = False
      hide_bank_card_voucher_cash_advance_GP = False
      bank_card_voucher_cash_advance_title_GP = String.Empty
      hide_check_voucher_cash_advance_GP = False
      check_voucher_cash_advance_title_GP = String.Empty
      commission_to_company_b_enabled = False
      bank_card_voucher_commission = String.Empty
      check_voucher_commission = String.Empty
      currency_voucher_commission = String.Empty

    End Sub
  End Class

#End Region ' Structures

#Region " Members "
  Protected m_currency_configuration As New TYPE_CURRENCY_CONFIGURATION

#End Region    ' Members

#Region "Properties"

  Public Property Currencies() As DataTable
    Get
      Return m_currency_configuration.currencies
    End Get
    Set(ByVal value As DataTable)
      m_currency_configuration.currencies = value
    End Set
  End Property

  Public Property ForeignCurrencies() As DataTable
    Get
      Return m_currency_configuration.foreigncurrencies
    End Get
    Set(ByVal value As DataTable)
      m_currency_configuration.foreigncurrencies = value
    End Set
  End Property
  Public Property ForeignCurrency_Title() As String
    Get
      Return m_currency_configuration.foreign_currency_voucher_title_GP
    End Get
    Set(ByVal Value As String)
      m_currency_configuration.foreign_currency_voucher_title_GP = Value
    End Set
  End Property
  Public Property BankCard_Title() As String
    Get
      Return m_currency_configuration.bank_card_voucher_title_GP
    End Get
    Set(ByVal Value As String)
      m_currency_configuration.bank_card_voucher_title_GP = Value
    End Set
  End Property
  Public Property Check_Title() As String
    Get
      Return m_currency_configuration.check_voucher_title_GP
    End Get
    Set(ByVal Value As String)
      m_currency_configuration.check_voucher_title_GP = Value
    End Set
  End Property
  Public Property Hide_BankCard_Voucher() As Boolean
    Get
      Return m_currency_configuration.hide_bank_card_voucher_GP
    End Get
    Set(ByVal Value As Boolean)
      m_currency_configuration.hide_bank_card_voucher_GP = Value
    End Set
  End Property
  Public Property Hide_Check_Voucher() As Boolean
    Get
      Return m_currency_configuration.hide_check_voucher_GP
    End Get
    Set(ByVal Value As Boolean)
      m_currency_configuration.hide_check_voucher_GP = Value
    End Set
  End Property
  Public Property Hide_ForeignCurrency_Voucher() As Boolean
    Get
      Return m_currency_configuration.hide_foreign_currency_voucher_GP
    End Get
    Set(ByVal Value As Boolean)
      m_currency_configuration.hide_foreign_currency_voucher_GP = Value
    End Set
  End Property

  Public Property Currencies_Accepted_GP() As String
    Get
      Return m_currency_configuration.currencies_accepted_GP
    End Get
    Set(ByVal Value As String)
      m_currency_configuration.currencies_accepted_GP = Value
    End Set
  End Property

  Public Property Currency_ISO_Code_GP() As String
    Get
      Return m_currency_configuration.currency_ISO_Code_GP
    End Get
    Set(ByVal Value As String)
      m_currency_configuration.currency_ISO_Code_GP = Value
    End Set
  End Property

  Public Property Foreign_Currency_ISO_Code_GP() As String
    Get
      Return m_currency_configuration.foreign_currency_ISO_Code_GP
    End Get
    Set(ByVal Value As String)
      m_currency_configuration.foreign_currency_ISO_Code_GP = Value
    End Set
  End Property

  Public Property BankCardEnabled() As Boolean
    Get
      Return m_currency_configuration.bank_card_enabled
    End Get
    Set(ByVal Value As Boolean)
      m_currency_configuration.bank_card_enabled = Value
    End Set
  End Property

  Public Property BankCardCurrencyExchangeList() As Dictionary(Of bank_card, CURRENCIES_ENABLED)
    Get
      Return m_currency_configuration.bank_card_currency_exchange_list
    End Get
    Set(ByVal Value As Dictionary(Of bank_card, CURRENCIES_ENABLED))
      m_currency_configuration.bank_card_currency_exchange_list = Value
    End Set
  End Property
  Public Property CheckCurrencyExchange() As CurrencyExchange
    Get
      Return m_currency_configuration.check_currency_exchange
    End Get
    Set(ByVal Value As CurrencyExchange)
      m_currency_configuration.check_currency_exchange = Value
    End Set
  End Property

  Public Property ForeignCurrencyExchangeList() As Dictionary(Of String, CURRENCIES_ENABLED)
    Get
      Return m_currency_configuration.foreign_currency_exchange_list
    End Get
    Set(ByVal Value As Dictionary(Of String, CURRENCIES_ENABLED))
      m_currency_configuration.foreign_currency_exchange_list = Value
    End Set
  End Property



  Public Property SpecificBankCardTypes() As Boolean
    Get
      Return m_currency_configuration.specific_bank_card_types_GP
    End Get
    Set(ByVal Value As Boolean)
      m_currency_configuration.specific_bank_card_types_GP = Value
    End Set
  End Property

  Public Property EditBankCardTransactionData() As Boolean
    Get
      Return m_currency_configuration.edit_bank_card_transaction_data_GP
    End Get
    Set(ByVal Value As Boolean)
      m_currency_configuration.edit_bank_card_transaction_data_GP = Value
    End Set
  End Property

  Public Property EditBankCheckTransactionData() As Boolean
    Get
      Return m_currency_configuration.edit_bank_check_transaction_data_GP
    End Get
    Set(ByVal Value As Boolean)
      m_currency_configuration.edit_bank_check_transaction_data_GP = Value
    End Set
  End Property

  Public Property HideBankCardVoucherCashAdvance() As Boolean
    Get
      Return m_currency_configuration.hide_bank_card_voucher_cash_advance_GP
    End Get
    Set(ByVal Value As Boolean)
      m_currency_configuration.hide_bank_card_voucher_cash_advance_GP = Value
    End Set
  End Property

  Public Property BankCardVoucherCashAdvanceTitle() As String
    Get
      Return m_currency_configuration.bank_card_voucher_cash_advance_title_GP
    End Get
    Set(ByVal Value As String)
      m_currency_configuration.bank_card_voucher_cash_advance_title_GP = Value
    End Set
  End Property

  Public Property HideCheckVoucherCashAdvance() As Boolean
    Get
      Return m_currency_configuration.hide_check_voucher_cash_advance_GP
    End Get
    Set(ByVal Value As Boolean)
      m_currency_configuration.hide_check_voucher_cash_advance_GP = Value
    End Set
  End Property

  Public Property CheckVoucherCashAdvanceTitle() As String
    Get
      Return m_currency_configuration.check_voucher_cash_advance_title_GP
    End Get
    Set(ByVal Value As String)
      m_currency_configuration.check_voucher_cash_advance_title_GP = Value
    End Set
  End Property

  Public Property ComissionToCompanyBEnabled() As Boolean
    Get
      Return m_currency_configuration.commission_to_company_b_enabled
    End Get
    Set(ByVal Value As Boolean)
      m_currency_configuration.commission_to_company_b_enabled = Value
    End Set
  End Property

  Public Property BankCardVoucherCommission() As String
    Get
      Return m_currency_configuration.bank_card_voucher_commission
    End Get
    Set(ByVal Value As String)
      m_currency_configuration.bank_card_voucher_commission = Value
    End Set
  End Property

  Public Property CheckVoucherCommission() As String
    Get
      Return m_currency_configuration.check_voucher_commission
    End Get
    Set(ByVal Value As String)
      m_currency_configuration.check_voucher_commission = Value
    End Set
  End Property

  Public Property CurrencyVoucherCommission() As String
    Get
      Return m_currency_configuration.currency_voucher_commission
    End Get
    Set(ByVal Value As String)
      m_currency_configuration.currency_voucher_commission = Value
    End Set
  End Property

#End Region   ' Properties

#Region "Overrides"

  Public Sub Get_NLS_ID(ByVal Bank_Card_Type As bank_card, ByRef NLS_ID As Integer)

    If (Bank_Card_Type.card_type = CARD_TYPE.NONE And Bank_Card_Type.card_scope = CARD_SCOPE.NONE) Then
      NLS_ID = 2474
      Return
    End If

    If (Bank_Card_Type.card_type = CARD_TYPE.CREDIT And Bank_Card_Type.card_scope = CARD_SCOPE.NONE) Then
      NLS_ID = 7373
      Return
    End If

    If (Bank_Card_Type.card_type = CARD_TYPE.DEBIT And Bank_Card_Type.card_scope = CARD_SCOPE.NONE) Then
      NLS_ID = 7376
      Return
    End If

    If (Bank_Card_Type.card_type = CARD_TYPE.NONE And Bank_Card_Type.card_scope = CARD_SCOPE.NATIONAL) Then
      NLS_ID = 7393
      Return
    End If

    If (Bank_Card_Type.card_type = CARD_TYPE.NONE And Bank_Card_Type.card_scope = CARD_SCOPE.INTERNATIONAL) Then
      NLS_ID = 7394
      Return
    End If

    If (Bank_Card_Type.card_type = CARD_TYPE.DEBIT And Bank_Card_Type.card_scope = CARD_SCOPE.NATIONAL) Then
      NLS_ID = 7377
      Return
    End If

    If (Bank_Card_Type.card_type = CARD_TYPE.DEBIT And Bank_Card_Type.card_scope = CARD_SCOPE.INTERNATIONAL) Then
      NLS_ID = 7378
      Return
    End If

    If (Bank_Card_Type.card_type = CARD_TYPE.CREDIT And Bank_Card_Type.card_scope = CARD_SCOPE.NATIONAL) Then
      NLS_ID = 7374
      Return
    End If

    If (Bank_Card_Type.card_type = CARD_TYPE.CREDIT And Bank_Card_Type.card_scope = CARD_SCOPE.INTERNATIONAL) Then
      NLS_ID = 8375
      Return
    End If

  End Sub


  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _tmp_str_iso_code As String = ""
    Dim _nls_id As Int32
    Dim _index As Int32
    Dim _bank_card As bank_card

    _nls_id = 0
    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)
    _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(2429), "")

    Call _auditor_data.SetField(0, Me.Currency_ISO_Code_GP, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2072))
    Call _auditor_data.SetField(0, IIf(Me.Foreign_Currency_ISO_Code_GP = "", AUDIT_NONE_STRING, Me.Foreign_Currency_ISO_Code_GP), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2430))
    Call _auditor_data.SetField(0, Me.Currencies_Accepted_GP, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2075))

    ' Bank Card

    Call _auditor_data.SetField(0, IIf(BankCardEnabled, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)), _
                                                  GLB_NLS_GUI_PLAYER_TRACKING.GetString(2474, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2143)))

    For Each _keyvalue As KeyValuePair(Of bank_card, CURRENCIES_ENABLED) In Me.BankCardCurrencyExchangeList
      Dim _cur As CURRENCIES_ENABLED = _keyvalue.Value
      Call _auditor_data.SetField(0, IIf(_cur.foreign_currency_exchange.Status, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)), _
                                         GLB_NLS_GUI_PLAYER_TRACKING.GetString(2474, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2143)))

      If _cur.foreign_currency_exchange.Status Then
        _tmp_str_iso_code = _cur.foreign_currency_exchange.CurrencyCode
        Get_NLS_ID(_cur.BankCard, _nls_id)
        Call _auditor_data.SetField(0, GUI_FormatNumber(_cur.foreign_currency_exchange.ChangeRate.Value, 8), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2475, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2144), _tmp_str_iso_code))
        Call _auditor_data.SetField(0, GUI_FormatCurrency(_cur.foreign_currency_exchange.FixedComission.Value), GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_id, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2158)))
        Call _auditor_data.SetField(0, GUI_FormatNumber(_cur.foreign_currency_exchange.VariableComission.Value), GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_id, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2159)))
        Call _auditor_data.SetField(0, GUI_FormatCurrency(_cur.foreign_currency_exchange.FixedNR2.Value), GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_id, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2160)))
        Call _auditor_data.SetField(0, GUI_FormatNumber(_cur.foreign_currency_exchange.VariableNR2.Value), GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_id, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2161)))
      Else
        Call _auditor_data.SetField(0, GUI_FormatNumber(0, 8), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2475, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2144), _tmp_str_iso_code))
        Call _auditor_data.SetField(0, GUI_FormatCurrency(0), GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_id, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2158)))
        Call _auditor_data.SetField(0, GUI_FormatNumber(0), GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_id, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2159)))
        Call _auditor_data.SetField(0, GUI_FormatCurrency(0), GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_id, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2160)))
        Call _auditor_data.SetField(0, GUI_FormatNumber(0), GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_id, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2161)))
      End If
    Next

    _bank_card.Initialize(CARD_TYPE.NONE, CARD_SCOPE.NONE)
    Get_NLS_ID(_bank_card, _nls_id)
    For _index = Me.BankCardCurrencyExchangeList.Count + 1 To 4
      Call _auditor_data.SetField(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(279), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2474, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2143)))
      Call _auditor_data.SetField(0, GUI_FormatNumber(0, 8), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2475, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2144), _tmp_str_iso_code))
      Call _auditor_data.SetField(0, GUI_FormatCurrency(0), GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_id, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2158)))
      Call _auditor_data.SetField(0, GUI_FormatNumber(0), GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_id, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2159)))
      Call _auditor_data.SetField(0, GUI_FormatCurrency(0), GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_id, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2160)))
      Call _auditor_data.SetField(0, GUI_FormatNumber(0), GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_id, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2161)))
    Next



    Call _auditor_data.SetField(0, Me.BankCard_Title, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2474, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2536)))
    Call _auditor_data.SetField(0, IIf(Me.Hide_BankCard_Voucher, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)), _
                                                  GLB_NLS_GUI_PLAYER_TRACKING.GetString(2474, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2537)))
    Call _auditor_data.SetField(0, IIf(Me.SpecificBankCardTypes, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)), _
                                                      GLB_NLS_GUI_PLAYER_TRACKING.GetString(5464))
    Call _auditor_data.SetField(0, IIf(Me.EditBankCardTransactionData, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)), _
                                               GLB_NLS_GUI_PLAYER_TRACKING.GetString(2474, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5465)))

    Call _auditor_data.SetField(0, IIf(Me.HideBankCardVoucherCashAdvance, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)), _
                                              GLB_NLS_GUI_PLAYER_TRACKING.GetString(2474, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5471)))
    Call _auditor_data.SetField(0, Me.BankCardVoucherCashAdvanceTitle, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2474, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5470)))
    Call _auditor_data.SetField(0, Me.BankCardVoucherCommission, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2558) & "." & _
                                                                 GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2536))

    ' Check
    Call _auditor_data.SetField(0, IIf(Me.CheckCurrencyExchange.Status, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)), _
                                                  GLB_NLS_GUI_PLAYER_TRACKING.GetString(2592, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2143)))

    Call _auditor_data.SetField(0, IIf(Me.EditBankCheckTransactionData, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)), _
                                              GLB_NLS_GUI_PLAYER_TRACKING.GetString(2592, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5465)))

    Call _auditor_data.SetField(0, IIf(Me.HideCheckVoucherCashAdvance, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)), _
                                              GLB_NLS_GUI_PLAYER_TRACKING.GetString(2592, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5471)))
    Call _auditor_data.SetField(0, Me.CheckVoucherCashAdvanceTitle, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2592, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5470)))

    Call _auditor_data.SetField(0, GUI_FormatCurrency(Me.CheckCurrencyExchange.FixedComission.Value), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2592, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2158)))
    Call _auditor_data.SetField(0, GUI_FormatNumber(Me.CheckCurrencyExchange.VariableComission.Value), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2592, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2159)))
    Call _auditor_data.SetField(0, GUI_FormatCurrency(Me.CheckCurrencyExchange.FixedNR2.Value), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2592, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2160)))
    Call _auditor_data.SetField(0, GUI_FormatNumber(Me.CheckCurrencyExchange.VariableNR2.Value), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2592, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2161)))
    Call _auditor_data.SetField(0, Me.Check_Title, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2592, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2536)))
    Call _auditor_data.SetField(0, IIf(Me.Hide_Check_Voucher, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)), _
                                                  GLB_NLS_GUI_PLAYER_TRACKING.GetString(2592, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2537)))
    Call _auditor_data.SetField(0, Me.CheckVoucherCommission, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2594) & "." & _
                                                             GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2536))
    'Foreign Currency
    For Each _keyvalue As KeyValuePair(Of String, CURRENCIES_ENABLED) In Me.ForeignCurrencyExchangeList
      Dim _cur As CURRENCIES_ENABLED = _keyvalue.Value
      If _cur.foreign_currency_exchange.CurrencyCode <> "" Then
        _tmp_str_iso_code = _cur.foreign_currency_exchange.CurrencyCode
        Call _auditor_data.SetField(0, GUI_FormatNumber(_cur.foreign_currency_exchange.ChangeRate.Value, 8), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2475, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2144), _tmp_str_iso_code))
        Call _auditor_data.SetField(0, GUI_FormatNumber(_cur.foreign_currency_exchange.Decimals.Value), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2475, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2145), _tmp_str_iso_code))
        Call _auditor_data.SetField(0, GUI_FormatCurrency(_cur.foreign_currency_exchange.FixedComission.Value), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2475, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2158), _tmp_str_iso_code))
        Call _auditor_data.SetField(0, GUI_FormatNumber(_cur.foreign_currency_exchange.VariableComission.Value), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2475, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2159), _tmp_str_iso_code))
        Call _auditor_data.SetField(0, GUI_FormatCurrency(_cur.foreign_currency_exchange.FixedNR2.Value), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2475, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2160), _tmp_str_iso_code))
        Call _auditor_data.SetField(0, GUI_FormatNumber(_cur.foreign_currency_exchange.VariableNR2.Value), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2475, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2161), _tmp_str_iso_code))
      End If

    Next

    Call _auditor_data.SetField(0, Me.ForeignCurrency_Title, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2475, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2536), ""))
    Call _auditor_data.SetField(0, IIf(Me.Hide_ForeignCurrency_Voucher, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)), _
                                                GLB_NLS_GUI_PLAYER_TRACKING.GetString(2475, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2537), ""))
    Call _auditor_data.SetField(0, Me.CurrencyVoucherCommission, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2430) & "." & _
                                                      GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2536))

    Return _auditor_data

  End Function ' AuditorData

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR

  End Function ' DB_Delete

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR

  End Function ' DB_Insert

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim rc As Integer

    ' Read currency exchange configuration data
    rc = CurrencyExchangeConfigurationData_DbRead(SqlCtx)

    Select Case rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Read

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim rc As Integer

    ' Update currency exchange configuration data
    rc = CurrencyExchangeConfigurationData_DbUpdate(SqlCtx)

    Select Case rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Update

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _target As CLS_CURRENCY_CONFIGURATION

    _target = New CLS_CURRENCY_CONFIGURATION

    _target.m_currency_configuration.currencies = Me.m_currency_configuration.currencies.Copy()
    _target.m_currency_configuration.foreigncurrencies = Me.m_currency_configuration.foreigncurrencies.Copy()
    _target.m_currency_configuration.currency_ISO_Code_GP = Me.m_currency_configuration.currency_ISO_Code_GP
    _target.m_currency_configuration.foreign_currency_ISO_Code_GP = Me.m_currency_configuration.foreign_currency_ISO_Code_GP
    _target.m_currency_configuration.currencies_accepted_GP = Me.m_currency_configuration.currencies_accepted_GP
    _target.m_currency_configuration.bank_card_enabled = Me.m_currency_configuration.bank_card_enabled

    _target.m_currency_configuration.check_currency_exchange = Me.m_currency_configuration.check_currency_exchange.Copy()
    _target.m_currency_configuration.foreign_currency_voucher_title_GP = Me.m_currency_configuration.foreign_currency_voucher_title_GP
    _target.m_currency_configuration.bank_card_voucher_title_GP = Me.m_currency_configuration.bank_card_voucher_title_GP
    _target.m_currency_configuration.check_voucher_title_GP = Me.m_currency_configuration.check_voucher_title_GP
    _target.m_currency_configuration.hide_bank_card_voucher_GP = Me.m_currency_configuration.hide_bank_card_voucher_GP
    _target.m_currency_configuration.hide_check_voucher_GP = Me.m_currency_configuration.hide_check_voucher_GP
    _target.m_currency_configuration.hide_foreign_currency_voucher_GP = Me.m_currency_configuration.hide_foreign_currency_voucher_GP
    _target.m_currency_configuration.specific_bank_card_types_GP = Me.m_currency_configuration.specific_bank_card_types_GP
    _target.m_currency_configuration.edit_bank_card_transaction_data_GP = Me.m_currency_configuration.edit_bank_card_transaction_data_GP
    _target.m_currency_configuration.edit_bank_check_transaction_data_GP = Me.m_currency_configuration.edit_bank_check_transaction_data_GP
    _target.m_currency_configuration.hide_bank_card_voucher_cash_advance_GP = Me.m_currency_configuration.hide_bank_card_voucher_cash_advance_GP
    _target.m_currency_configuration.bank_card_voucher_cash_advance_title_GP = Me.m_currency_configuration.bank_card_voucher_cash_advance_title_GP
    _target.m_currency_configuration.hide_check_voucher_cash_advance_GP = Me.m_currency_configuration.hide_check_voucher_cash_advance_GP
    _target.m_currency_configuration.check_voucher_cash_advance_title_GP = Me.m_currency_configuration.check_voucher_cash_advance_title_GP
    _target.m_currency_configuration.bank_card_voucher_commission = Me.m_currency_configuration.bank_card_voucher_commission
    _target.m_currency_configuration.check_voucher_commission = Me.m_currency_configuration.check_voucher_commission
    _target.m_currency_configuration.currency_voucher_commission = Me.m_currency_configuration.currency_voucher_commission

    For Each _keyvalue As KeyValuePair(Of bank_card, CURRENCIES_ENABLED) In Me.BankCardCurrencyExchangeList
      Dim _cur As CURRENCIES_ENABLED = _keyvalue.Value

      _target.m_currency_configuration.bank_card_currency_exchange_list.Add(_keyvalue.Key, New CURRENCIES_ENABLED(_cur))

    Next

    For Each _keyvalue As KeyValuePair(Of String, CURRENCIES_ENABLED) In Me.ForeignCurrencyExchangeList
      Dim _cur As CURRENCIES_ENABLED = _keyvalue.Value
      If Not (_cur.foreign_currency_exchange.CurrencyCode Is Nothing) Then
        _target.m_currency_configuration.foreign_currency_exchange_list.Add(_cur.foreign_currency_exchange.CurrencyCode, New CURRENCIES_ENABLED(_cur))
      End If
    Next

    Return _target

  End Function ' Duplicate

#End Region ' Overrides

#Region "Private Functions"

  Private Function CurrencyExchangeConfigurationData_DbRead(ByVal Context As Integer) As Integer

    Dim _str_bld As StringBuilder = New StringBuilder()
    Dim _rc As Integer
    Dim _data_row As DataRow()

    If Not CurrencyExchange.ReadCurrencyExchangeFilter(False, m_currency_configuration.currencies) Then
      Return ENUM_STATUS.STATUS_ERROR
    End If

    If Not CurrencyExchange.ReadCurrencyExchangeFilter(False, m_currency_configuration.foreigncurrencies) Then
      Return ENUM_STATUS.STATUS_ERROR
    End If

    _data_row = m_currency_configuration.currencies.Select("ISO_CODE ='" & Cage.CHIPS_ISO_CODE & "'")
    If _data_row.Length > 0 Then
      m_currency_configuration.currencies.Rows.Remove(_data_row(0))
    End If

    _data_row = m_currency_configuration.foreigncurrencies.Select("ISO_CODE ='" & Cage.CHIPS_ISO_CODE & "'")
    If _data_row.Length > 0 Then
      m_currency_configuration.foreigncurrencies.Rows.Remove(_data_row(0))
    End If

    _rc = CurrencyExchangeConf_DbRead(Context)

    If _rc <> ENUM_DB_RC.DB_RC_OK Then
      Return _rc
    End If

    Return ENUM_STATUS.STATUS_OK

  End Function ' CurrencyExchangeConfigurationData_DbRead


  Private Function CurrencyExchangeConfigurationData_DbUpdate(ByVal Context As Integer) As Integer

    Try
      Using _db_trx As New DB_TRX()

        If Not Currency_Exchange_Configuration_DbUpdate(_db_trx.SqlTransaction) Then

          Return ENUM_DB_RC.DB_RC_ERROR_DB
        End If

        If Not CageMeters.UpdateAndCreateCageSessionMeters(_db_trx.SqlTransaction) Then

          Return ENUM_DB_RC.DB_RC_ERROR_DB
        End If

        _db_trx.Commit()
      End Using

#If DEBUG Then
      Call WSI.Common.GeneralParam.ReloadGP()
#End If
      Return ENUM_DB_RC.DB_RC_OK
    Catch
    End Try

    Return ENUM_DB_RC.DB_RC_ERROR_DB

  End Function 'CurrencyExchangeConfigurationData_DbUpdate

  '----------------------------------------------------------------------------
  ' PURPOSE: Update the database General_Params. If it goes ok the method returns true 
  '                                              if it doesn't go ok return false
  ' PARAMS:
  '   - INPUT: Transaction
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - It returns True or false, depends if the transaction was fine or not
  '
  ' NOTES:
  Private Function Currency_Exchange_Configuration_DbUpdate(ByVal Trx As SqlTransaction) As Boolean

    Dim _sb As StringBuilder
    Dim _status As Boolean
    Dim _str_tmp As String
    Dim _chips_enabled As Boolean
    Dim _is_multi_currency_exchange As Boolean
    Dim _general_params As WSI.Common.GeneralParam.Dictionary

    Dim _currrency_list As List(Of CurrencyExchange)
    Dim _xml As String

    _chips_enabled = False

    _general_params = WSI.Common.GeneralParam.Dictionary.Create()

    Try

      'General Params Currency ISO Code
      If Not DB_GeneralParam_Update("RegionalOptions", "CurrencyISOCode", Me.m_currency_configuration.currency_ISO_Code_GP, Trx) Then
        Return False
      End If

      'National Currency: Update Status
      _sb = New StringBuilder()
      _sb.AppendLine("UPDATE   CURRENCY_EXCHANGE")
      _sb.AppendLine("   SET   CE_STATUS            = @pCurStatus")
      _sb.AppendLine(" WHERE   CE_CURRENCY_ISO_CODE = @pCurIsoCode")
      _sb.AppendLine("   AND   CE_TYPE              = @pCurType")

      Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), Trx.Connection, Trx)
        _cmd.Parameters.Add("@pCurStatus", SqlDbType.Bit).Value = True
        _cmd.Parameters.Add("@pCurIsoCode", SqlDbType.NVarChar).Value = Me.m_currency_configuration.currency_ISO_Code_GP
        _cmd.Parameters.Add("@pCurType", SqlDbType.Int).Value = CurrencyExchangeType.CURRENCY

        _cmd.ExecuteNonQuery()
      End Using

      If _general_params.GetBoolean("MultiCurrencyExchange", "Enabled") Then
        _is_multi_currency_exchange = True
      End If

      If _general_params.GetString("RegionalOptions", "CurrenciesAccepted").Contains(Cage.CHIPS_ISO_CODE) Then
        _chips_enabled = True
      End If
      If Not DB_GeneralParam_Update("RegionalOptions", "CurrenciesAccepted", Me.m_currency_configuration.currencies_accepted_GP, Trx) Then
        Return False
      End If

      ' Voucher titles and hide voucher
      If Not DB_GeneralParam_Update("Cashier.Voucher", "PaymentMethod.Currency.Title", Me.m_currency_configuration.foreign_currency_voucher_title_GP, Trx) Then
        Return False
      End If
      If Not DB_GeneralParam_Update("Cashier.Voucher", "PaymentMethod.BankCard.Title", Me.m_currency_configuration.bank_card_voucher_title_GP, Trx) Then
        Return False
      End If
      If Not DB_GeneralParam_Update("Cashier.Voucher", "PaymentMethod.Check.Title", Me.m_currency_configuration.check_voucher_title_GP, Trx) Then
        Return False
      End If
      If Not DB_GeneralParam_Update("Cashier.Voucher", "PaymentMethod.Currency.HideVoucher", IIf(Me.m_currency_configuration.hide_foreign_currency_voucher_GP, 1, 0), Trx) Then
        Return False
      End If
      If Not DB_GeneralParam_Update("Cashier.Voucher", "PaymentMethod.BankCard.HideVoucher", IIf(Me.m_currency_configuration.hide_bank_card_voucher_GP, 1, 0), Trx) Then
        Return False
      End If
      If Not DB_GeneralParam_Update("Cashier.Voucher", "PaymentMethod.Check.HideVoucher", IIf(Me.m_currency_configuration.hide_check_voucher_GP, 1, 0), Trx) Then
        Return False
      End If
      If Not DB_GeneralParam_Update("Cashier.PaymentMethod", "BankCard.DifferentiateType", IIf(Me.m_currency_configuration.specific_bank_card_types_GP, 1, 0), Trx) Then
        Return False
      End If
      If Not DB_GeneralParam_Update("Cashier.PaymentMethod", "BankCard.EnterTransactionDetails", IIf(Me.m_currency_configuration.edit_bank_card_transaction_data_GP, 1, 0), Trx) Then
        Return False
      End If
      If Not DB_GeneralParam_Update("Cashier.PaymentMethod", "Check.EnterTransactionDetails", IIf(Me.m_currency_configuration.edit_bank_check_transaction_data_GP, 1, 0), Trx) Then
        Return False
      End If

      If Not DB_GeneralParam_Update("Cashier.Voucher", "PaymentMethod.CashAdvance.BankCard.HideVoucher", IIf(Me.m_currency_configuration.hide_bank_card_voucher_cash_advance_GP, 1, 0), Trx) Then
        Return False
      End If
      If Not DB_GeneralParam_Update("Cashier.Voucher", "PaymentMethod.CashAdvance.BankCard.Title", Me.m_currency_configuration.bank_card_voucher_cash_advance_title_GP, Trx) Then
        Return False
      End If
      If Not DB_GeneralParam_Update("Cashier.Voucher", "PaymentMethod.CashAdvance.Check.HideVoucher", IIf(Me.m_currency_configuration.hide_check_voucher_cash_advance_GP, 1, 0), Trx) Then
        Return False
      End If
      If Not DB_GeneralParam_Update("Cashier.Voucher", "PaymentMethod.CashAdvance.Check.Title", Me.m_currency_configuration.check_voucher_cash_advance_title_GP, Trx) Then
        Return False
      End If
      If Not DB_GeneralParam_Update("Cashier.PaymentMethod", "CommissionToCompanyB.BankCard.VoucherTitle", Me.m_currency_configuration.bank_card_voucher_commission, Trx) Then
        Return False
      End If
      If Not DB_GeneralParam_Update("Cashier.PaymentMethod", "CommissionToCompanyB.Check.VoucherTitle", Me.m_currency_configuration.check_voucher_commission, Trx) Then
        Return False
      End If
      If Not DB_GeneralParam_Update("Cashier.PaymentMethod", "CommissionToCompanyB.Currency.VoucherTitle", Me.m_currency_configuration.currency_voucher_commission, Trx) Then
        Return False
      End If

      'ETP gets al cart configuration XML.
      _currrency_list = New List(Of CurrencyExchange)
      For Each _keyvalue As KeyValuePair(Of bank_card, CURRENCIES_ENABLED) In Me.BankCardCurrencyExchangeList
        Dim _cur As CURRENCIES_ENABLED = _keyvalue.Value
        If (_cur.Enabled) Then
          _currrency_list.Add(_cur.foreign_currency_exchange)
        End If
      Next
      _xml = CurrencyExchange.CurrencyConfigurationToXML(_currrency_list)

      ' ICS 13-SEP-2013: If record does not exist it's inserted
      _sb = New StringBuilder()
      _sb.AppendLine("IF NOT EXISTS ( SELECT 1 FROM CURRENCY_EXCHANGE           ")
      _sb.AppendLine("        WHERE   CE_TYPE = @pCurType                       ")
      _sb.AppendLine("          AND   CE_CURRENCY_ISO_CODE = @pCurIsoCode )     ")
      _sb.AppendLine("  INSERT INTO   CURRENCY_EXCHANGE ( CE_TYPE               ")
      _sb.AppendLine("              , CE_CURRENCY_ISO_CODE                      ")
      _sb.AppendLine("              , CE_DESCRIPTION                            ")
      _sb.AppendLine("              , CE_CHANGE                                 ")
      _sb.AppendLine("              , CE_NUM_DECIMALS                           ")
      _sb.AppendLine("              , CE_STATUS                                 ")
      _sb.AppendLine("              , CE_CONFIGURATION            )             ")

      _sb.AppendLine("       VALUES ( @pCurType                                 ")
      _sb.AppendLine("              , @pCurIsoCode                              ")
      _sb.AppendLine("              , @pCurDescription                          ")
      _sb.AppendLine("              , 1                                         ")
      _sb.AppendLine("              , 2                                         ")
      _sb.AppendLine("              , @pCurStatus                               ")
      _sb.AppendLine("              , @pCurCardConfiguration  )                 ")

      _sb.AppendLine("ELSE                                                      ")
      _sb.AppendLine("UPDATE   CURRENCY_EXCHANGE                                ")
      _sb.AppendLine("  SET       CE_STATUS              = @pCurStatus          ")
      _sb.AppendLine("       , CE_CONFIGURATION       = @pCurCardConfiguration  ")
      _sb.AppendLine(" WHERE   CE_CURRENCY_ISO_CODE   = @pCurIsoCode            ")
      _sb.AppendLine("         AND CE_TYPE            = @pCurType               ")


      'Bank Card
      Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), Trx.Connection, Trx)
        _cmd.Parameters.Add("@pCurStatus", SqlDbType.Bit).Value = m_currency_configuration.bank_card_enabled
        _cmd.Parameters.Add("@pCurIsoCode", SqlDbType.NVarChar).Value = Me.m_currency_configuration.currency_ISO_Code_GP
        _cmd.Parameters.Add("@pCurType", SqlDbType.Int).Value = CurrencyExchangeType.CARD
        _cmd.Parameters.Add("@pCurDescription", SqlDbType.NVarChar).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2558)
        _cmd.Parameters.Add("@pCurCardConfiguration", SqlDbType.Xml).Value = _xml

        _cmd.ExecuteNonQuery()
      End Using

      'Check

      _currrency_list = New List(Of CurrencyExchange)
      _currrency_list.Add(Me.CheckCurrencyExchange)
      _xml = CurrencyExchange.CurrencyConfigurationToXML(_currrency_list)

      ' ICS 13-SEP-2013: If record does not exist it's inserted
      _sb = New StringBuilder()
      _sb.AppendLine("IF NOT EXISTS ( SELECT 1 FROM CURRENCY_EXCHANGE           ")
      _sb.AppendLine("        WHERE   CE_TYPE = @pCurType                       ")
      _sb.AppendLine("          AND   CE_CURRENCY_ISO_CODE = @pCurIsoCode )     ")
      _sb.AppendLine("  INSERT INTO   CURRENCY_EXCHANGE ( CE_TYPE               ")
      _sb.AppendLine("              , CE_CURRENCY_ISO_CODE                      ")
      _sb.AppendLine("              , CE_DESCRIPTION                            ")
      _sb.AppendLine("              , CE_CHANGE                                 ")
      _sb.AppendLine("              , CE_NUM_DECIMALS                           ")
      _sb.AppendLine("              , CE_VARIABLE_COMMISSION                    ")
      _sb.AppendLine("              , CE_FIXED_COMMISSION                       ")
      _sb.AppendLine("              , CE_VARIABLE_NR2                           ")
      _sb.AppendLine("              , CE_FIXED_NR2                              ")
      _sb.AppendLine("              , CE_STATUS                                 ")
      _sb.AppendLine("              , CE_CONFIGURATION            )             ")

      _sb.AppendLine("       VALUES ( @pCurType                                 ")
      _sb.AppendLine("              , @pCurIsoCode                              ")
      _sb.AppendLine("              , @pCurDescription                          ")
      _sb.AppendLine("              , 1                                         ")
      _sb.AppendLine("              , 2                                         ")
      _sb.AppendLine("              , @pCurVariableCommission                   ")
      _sb.AppendLine("              , @pCurFixedCommission                      ")
      _sb.AppendLine("              , @pCurVariableNR2                          ")
      _sb.AppendLine("              , @pCurFixedNR2                             ")
      _sb.AppendLine("              , @pCurStatus                               ")
      _sb.AppendLine("              , @pCurCardConfiguration  )                 ")

      _sb.AppendLine("ELSE                                                      ")
      _sb.AppendLine("UPDATE   CURRENCY_EXCHANGE                                ")
      _sb.AppendLine("   SET   CE_VARIABLE_COMMISSION = @pCurVariableCommission ")
      _sb.AppendLine("       , CE_FIXED_COMMISSION    = @pCurFixedCommission    ")
      _sb.AppendLine("       , CE_VARIABLE_NR2        = @pCurVariableNR2        ")
      _sb.AppendLine("       , CE_FIXED_NR2           = @pCurFixedNR2           ")
      _sb.AppendLine("       , CE_STATUS              = @pCurStatus             ")
      _sb.AppendLine("       , CE_CONFIGURATION       = @pCurCardConfiguration  ")
      _sb.AppendLine(" WHERE   CE_CURRENCY_ISO_CODE   = @pCurIsoCode            ")
      _sb.AppendLine("         AND CE_TYPE            = @pCurType               ")

      Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), Trx.Connection, Trx)
        _cmd.Parameters.Add("@pCurVariableCommission", SqlDbType.Money).Value = Me.m_currency_configuration.check_currency_exchange.VariableComission.Value
        _cmd.Parameters.Add("@pCurFixedCommission", SqlDbType.Money).Value = Me.m_currency_configuration.check_currency_exchange.FixedComission.Value
        _cmd.Parameters.Add("@pCurVariableNR2", SqlDbType.Money).Value = Me.m_currency_configuration.check_currency_exchange.VariableNR2.Value
        _cmd.Parameters.Add("@pCurFixedNR2", SqlDbType.Money).Value = Me.m_currency_configuration.check_currency_exchange.FixedNR2.Value
        _cmd.Parameters.Add("@pCurStatus", SqlDbType.Bit).Value = Me.m_currency_configuration.check_currency_exchange.Status
        _cmd.Parameters.Add("@pCurIsoCode", SqlDbType.NVarChar).Value = Me.m_currency_configuration.currency_ISO_Code_GP
        _cmd.Parameters.Add("@pCurType", SqlDbType.Int).Value = CurrencyExchangeType.CHECK
        _cmd.Parameters.Add("@pCurDescription", SqlDbType.NVarChar).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2594)
        _cmd.Parameters.Add("@pCurCardConfiguration", SqlDbType.Xml).Value = _xml

        _cmd.ExecuteNonQuery()
      End Using

      'Update Status
      _sb = New StringBuilder()
      _sb.AppendLine("UPDATE   CURRENCY_EXCHANGE")
      _sb.AppendLine("   SET   CE_STATUS              = @pCurStatus")
      _sb.AppendLine(" WHERE   CE_CURRENCY_ISO_CODE  <> @pCurIsoCode")
      _sb.AppendLine("   AND   (CE_TYPE               = @pCardType")
      _sb.AppendLine("    OR   CE_TYPE                = @pCheckType )")

      Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), Trx.Connection, Trx)
        _cmd.Parameters.Add("@pCurStatus", SqlDbType.Bit).Value = False
        _cmd.Parameters.Add("@pCurIsoCode", SqlDbType.NVarChar).Value = Me.m_currency_configuration.currency_ISO_Code_GP
        _cmd.Parameters.Add("@pCardType", SqlDbType.Int).Value = CurrencyExchangeType.CARD
        _cmd.Parameters.Add("@pCheckType", SqlDbType.Int).Value = CurrencyExchangeType.CHECK

        _cmd.ExecuteNonQuery()
      End Using

      'Update table Cage Amount (For Cage mode)
      If Me.m_currency_configuration.foreign_currency_ISO_Code_GP <> "" Then
        _status = True
      Else
        _status = False
      End If

      _str_tmp = "'" & m_currency_configuration.currency_ISO_Code_GP & "'"

      _sb = New StringBuilder()

      _sb.AppendLine("UPDATE   CAGE_CURRENCIES ")
      _sb.AppendLine("   SET   CGC_ALLOWED = @pCurStatus ")
      _sb.AppendLine(" WHERE   CGC_ISO_CODE NOT IN (")
      _sb.AppendLine(_str_tmp)
      _sb.AppendLine(")")

      If _chips_enabled Then
        _sb.AppendLine("UPDATE   CHIPS ")
        _sb.AppendLine("   SET   CH_ALLOWED = @pCurStatus ")
      End If

      Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), Trx.Connection, Trx)
        _cmd.Parameters.Add("@pCurStatus", SqlDbType.Bit).Value = False

        _cmd.ExecuteNonQuery()
      End Using

      _sb = New StringBuilder()
      _sb.AppendLine("UPDATE   CAGE_CURRENCIES ")
      _sb.AppendLine("   SET   CGC_ALLOWED = @pCurStatus ")
      _sb.AppendLine(" WHERE   CGC_ISO_CODE IN (")
      _sb.AppendLine(_str_tmp)
      _sb.AppendLine(")")

      If _chips_enabled Then
        _sb.AppendLine("UPDATE   CHIPS ")
        _sb.AppendLine("   SET   CH_ALLOWED = @pCurStatus ")
      End If

      Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), Trx.Connection, Trx)
        _cmd.Parameters.Add("@pCurStatus", SqlDbType.Bit).Value = True

        _cmd.ExecuteNonQuery()
      End Using


      Dim _tmp_in_isocodes As String = m_currency_configuration.foreign_currency_ISO_Code_GP

      If Not String.IsNullOrEmpty(_tmp_in_isocodes) Then

        _tmp_in_isocodes = "'" + _tmp_in_isocodes.Replace(",", "','") + "'"

        _sb = New StringBuilder()
        _sb.AppendLine("UPDATE   CAGE_CURRENCIES ")
        _sb.AppendLine("   SET   CGC_ALLOWED = @pCurStatus")
        _sb.AppendFormat(" WHERE   CGC_ISO_CODE IN ({0})", _tmp_in_isocodes)

        Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), Trx.Connection, Trx)
          _cmd.Parameters.Add("@pCurStatus", SqlDbType.Bit).Value = _status

          _cmd.ExecuteNonQuery()
        End Using
      End If


      For Each _keyvalue As KeyValuePair(Of String, CURRENCIES_ENABLED) In Me.ForeignCurrencyExchangeList
        Dim _cur As CURRENCIES_ENABLED = _keyvalue.Value
        ' Currency Exchange Audit
        If _cur.foreign_old_change_rate <> _cur.foreign_currency_exchange.ChangeRate.Value OrElse _
           _cur.old_num_decimals <> _cur.foreign_currency_exchange.Decimals.Value Then
          _sb = New StringBuilder()
          _sb.AppendLine("INSERT INTO   CURRENCY_EXCHANGE_AUDIT")
          _sb.AppendLine("            ( CEA_TYPE ")
          _sb.AppendLine("            , CEA_CURRENCY_ISO_CODE ")
          _sb.AppendLine("            , CEA_DATETIME ")
          _sb.AppendLine("            , CEA_OLD_CHANGE ")
          _sb.AppendLine("            , CEA_NEW_CHANGE")
          _sb.AppendLine("            , CEA_OLD_NUM_DECIMALS ")
          _sb.AppendLine("            , CEA_NEW_NUM_DECIMALS  ) ")
          _sb.AppendLine("     VALUES ( @pCeaType ")
          _sb.AppendLine("            , @pCeaIsoCode ")
          _sb.AppendLine("            , GetDate() ")
          _sb.AppendLine("            , @pCeaOldChange ")
          _sb.AppendLine("            , @pCeaNewChange ")
          _sb.AppendLine("            , @pCeaOldDecimals ")
          _sb.AppendLine("            , @pCeaNewDecimals) ")

          Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), Trx.Connection, Trx)
            _cmd.Parameters.Add("@pCeaType", SqlDbType.Int).Value = CurrencyExchangeType.CURRENCY
            _cmd.Parameters.Add("@pCeaIsoCode", SqlDbType.NVarChar).Value = _cur.foreign_currency_exchange.CurrencyCode
            _cmd.Parameters.Add("@pCeaOldChange", SqlDbType.Decimal).Value = _cur.foreign_old_change_rate
            _cmd.Parameters.Add("@pCeaNewChange", SqlDbType.Decimal).Value = _cur.foreign_currency_exchange.ChangeRate.Value
            _cmd.Parameters.Add("@pCeaOldDecimals", SqlDbType.Decimal).Value = _cur.old_num_decimals
            _cmd.Parameters.Add("@pCeaNewDecimals", SqlDbType.Decimal).Value = _cur.foreign_currency_exchange.Decimals.Value

            If _cmd.ExecuteNonQuery() <> 1 Then
              Return False
            End If
          End Using
        End If
      Next

      If (Not _is_multi_currency_exchange) Then
        'Update Status for all currencies (except national). All disabled
        _sb = New StringBuilder()
        _sb.AppendLine("UPDATE   CURRENCY_EXCHANGE")
        _sb.AppendLine("   SET   CE_STATUS              = @pCurStatus")
        _sb.AppendLine(" WHERE   CE_CURRENCY_ISO_CODE  <> @pCurIsoCode")
        _sb.AppendLine("   AND   CE_TYPE                = @pCurType")

        Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), Trx.Connection, Trx)
          _cmd.Parameters.Add("@pCurStatus", SqlDbType.Bit).Value = False
          _cmd.Parameters.Add("@pCurIsoCode", SqlDbType.NVarChar).Value = Me.m_currency_configuration.currency_ISO_Code_GP
          _cmd.Parameters.Add("@pCurType", SqlDbType.Int).Value = CurrencyExchangeType.CURRENCY

          _cmd.ExecuteNonQuery()
        End Using

        'If GeneralParam.GetString("RegionalOptions", "CurrenciesAccepted").Contains(Cage.CHIPS_ISO_CODE) Then
        If _chips_enabled Then
          'Update Status for X01
          _sb = New StringBuilder()
          _sb.AppendLine("UPDATE   CURRENCY_EXCHANGE")
          _sb.AppendLine("   SET   CE_STATUS              = @pCurStatus")
          _sb.AppendLine(" WHERE   CE_CURRENCY_ISO_CODE   = @pCurIsoCode")
          _sb.AppendLine("   AND   CE_TYPE                = @pCurType")

          Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), Trx.Connection, Trx)
            _cmd.Parameters.Add("@pCurStatus", SqlDbType.Bit).Value = True
            _cmd.Parameters.Add("@pCurIsoCode", SqlDbType.NVarChar).Value = Cage.CHIPS_ISO_CODE
            _cmd.Parameters.Add("@pCurType", SqlDbType.Int).Value = CurrencyExchangeType.CURRENCY

            _cmd.ExecuteNonQuery()
          End Using
        End If

      End If


      Dim _result As Boolean
      _result = True

      'Foreign Currency
      For Each _keyvalue As KeyValuePair(Of String, CURRENCIES_ENABLED) In Me.ForeignCurrencyExchangeList
        Dim _cur As CURRENCIES_ENABLED = _keyvalue.Value
        If Not _result Then
          Continue For
        End If

        _currrency_list = New List(Of CurrencyExchange)
        _currrency_list.Add(_cur.foreign_currency_exchange)
        _xml = CurrencyExchange.CurrencyConfigurationToXML(_currrency_list)


        _sb = New StringBuilder()
        _sb.AppendLine("UPDATE   CURRENCY_EXCHANGE")
        _sb.AppendLine("   SET   CE_CHANGE              = @pCurChange")
        _sb.AppendLine("       , CE_NUM_DECIMALS        = @pCurNumDecimals")
        _sb.AppendLine("       , CE_VARIABLE_COMMISSION = @pCurVariableCommission")
        _sb.AppendLine("       , CE_FIXED_COMMISSION    = @pCurFixedCommission")
        _sb.AppendLine("       , CE_VARIABLE_NR2        = @pCurVariableNR2")
        _sb.AppendLine("       , CE_FIXED_NR2           = @pCurFixedNR2")
        _sb.AppendLine("       , CE_STATUS              = @pCurStatus")
        _sb.AppendLine("       , CE_CONFIGURATION       = @pCurConfiguration")
        _sb.AppendLine(" WHERE   CE_CURRENCY_ISO_CODE   = @pCurIsoCode")
        _sb.AppendLine("         AND CE_TYPE            = @pCurType")

        Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), Trx.Connection, Trx)
          _cmd.Parameters.Add("@pCurChange", SqlDbType.Decimal).Value = _cur.foreign_currency_exchange.ChangeRate.Value
          _cmd.Parameters.Add("@pCurNumDecimals", SqlDbType.Int).Value = _cur.foreign_currency_exchange.Decimals.Value
          _cmd.Parameters.Add("@pCurVariableCommission", SqlDbType.Money).Value = _cur.foreign_currency_exchange.VariableComission.Value
          _cmd.Parameters.Add("@pCurFixedCommission", SqlDbType.Money).Value = _cur.foreign_currency_exchange.FixedComission.Value
          _cmd.Parameters.Add("@pCurVariableNR2", SqlDbType.Money).Value = _cur.foreign_currency_exchange.VariableNR2.Value
          _cmd.Parameters.Add("@pCurFixedNR2", SqlDbType.Money).Value = _cur.foreign_currency_exchange.FixedNR2.Value
          _cmd.Parameters.Add("@pCurStatus", SqlDbType.Bit).Value = _cur.Enabled
          _cmd.Parameters.Add("@pCurIsoCode", SqlDbType.NVarChar).Value = _cur.foreign_currency_exchange.CurrencyCode
          _cmd.Parameters.Add("@pCurType", SqlDbType.Int).Value = CurrencyExchangeType.CURRENCY
          _cmd.Parameters.Add("@pCurConfiguration", SqlDbType.Xml).Value = _xml

          _result = _result And (_cmd.ExecuteNonQuery() = 1)

        End Using
      Next
      Return _result
    Catch ex As SqlException
      Debug.Print(ex.Message)
    End Try

    Return False

  End Function ' Currency_Exchange_Configuration_DbUpdate




  Private Sub AddBankTypeToList(ByRef List As List(Of bank_card), Card_Type As CARD_TYPE, Card_Scope As CARD_SCOPE)

    Dim _bank_card As bank_card
    _bank_card = New bank_card()
    _bank_card.Initialize(Card_Type, Card_Scope)

    List.Add(_bank_card)

  End Sub

  '----------------------------------------------------------------------------
  ' PURPOSE: It calls the Password_Configuration_DbRead and checks if the database result is ok
  '                                              
  ' PARAMS:
  '   - INPUT: 
  '   - OUTPUT:
  '
  ' RETURNS:
  '
  ' NOTES:
  Private Function CurrencyExchangeConf_DbRead(ByVal Context As Integer) As Integer

    Dim _final_db_result
    Dim _value As String
    Dim _general_params As WSI.Common.GeneralParam.Dictionary
    Dim _currenciesAccepted() As String = Nothing
    Dim _foreign_currency_Ok As Boolean
    Dim _split_delimiters() As Char
    Dim _tmp_string As String
    Dim _selected_rows As DataRow()
    Dim _list As List(Of bank_card)
    Dim _cur As CURRENCIES_ENABLED
    Dim _differentiate As Int32

    _value = ""
    _final_db_result = ENUM_DB_RC.DB_RC_OK
    _list = New List(Of bank_card)

    Try
      _general_params = WSI.Common.GeneralParam.Dictionary.Create()

      m_currency_configuration.currencies_accepted_GP = _general_params.GetString("RegionalOptions", "CurrenciesAccepted")
      m_currency_configuration.currency_ISO_Code_GP = _general_params.GetString("RegionalOptions", "CurrencyISOCode")

      m_currency_configuration.bank_card_voucher_title_GP = _general_params.GetString("Cashier.Voucher", "PaymentMethod.BankCard.Title")
      m_currency_configuration.foreign_currency_voucher_title_GP = _general_params.GetString("Cashier.Voucher", "PaymentMethod.Currency.Title")
      m_currency_configuration.check_voucher_title_GP = _general_params.GetString("Cashier.Voucher", "PaymentMethod.Check.Title")
      m_currency_configuration.hide_bank_card_voucher_GP = _general_params.GetBoolean("Cashier.Voucher", "PaymentMethod.BankCard.HideVoucher")
      m_currency_configuration.hide_foreign_currency_voucher_GP = _general_params.GetBoolean("Cashier.Voucher", "PaymentMethod.Currency.HideVoucher")
      m_currency_configuration.hide_check_voucher_GP = _general_params.GetBoolean("Cashier.Voucher", "PaymentMethod.Check.HideVoucher")
      m_currency_configuration.specific_bank_card_types_GP = _general_params.GetBoolean("Cashier.PaymentMethod", "BankCard.DifferentiateType")
      m_currency_configuration.edit_bank_card_transaction_data_GP = _general_params.GetBoolean("Cashier.PaymentMethod", "BankCard.EnterTransactionDetails")
      m_currency_configuration.edit_bank_check_transaction_data_GP = _general_params.GetBoolean("Cashier.PaymentMethod", "Check.EnterTransactionDetails")
      m_currency_configuration.hide_bank_card_voucher_cash_advance_GP = _general_params.GetBoolean("Cashier.Voucher", "PaymentMethod.CashAdvance.BankCard.HideVoucher")
      m_currency_configuration.bank_card_voucher_cash_advance_title_GP = _general_params.GetString("Cashier.Voucher", "PaymentMethod.CashAdvance.BankCard.Title")
      m_currency_configuration.hide_check_voucher_cash_advance_GP = _general_params.GetBoolean("Cashier.Voucher", "PaymentMethod.CashAdvance.Check.HideVoucher")
      m_currency_configuration.check_voucher_cash_advance_title_GP = _general_params.GetString("Cashier.Voucher", "PaymentMethod.CashAdvance.Check.Title")

      m_currency_configuration.commission_to_company_b_enabled = _general_params.GetBoolean("Cashier.PaymentMethod", "CommissionToCompanyB.Enabled")
      m_currency_configuration.bank_card_voucher_commission = _general_params.GetString("Cashier.PaymentMethod", "CommissionToCompanyB.BankCard.VoucherTitle")
      m_currency_configuration.check_voucher_commission = _general_params.GetString("Cashier.PaymentMethod", "CommissionToCompanyB.Check.VoucherTitle")
      m_currency_configuration.currency_voucher_commission = _general_params.GetString("Cashier.PaymentMethod", "CommissionToCompanyB.Currency.VoucherTitle")

      _differentiate = 0

      If (WSI.Common.Misc.IsPinPadEnabled()) Then
        _differentiate = _general_params.GetInt32("Cashier.PaymentMethod", "DifferentiateComission", 0)
      End If

      ' Credit Card
      Select Case _differentiate
        Case 0
          AddBankTypeToList(_list, CARD_TYPE.NONE, CARD_SCOPE.NONE)
        Case 1
          AddBankTypeToList(_list, CARD_TYPE.DEBIT, CARD_SCOPE.NONE)
          AddBankTypeToList(_list, CARD_TYPE.CREDIT, CARD_SCOPE.NONE)
        Case 2
          AddBankTypeToList(_list, CARD_TYPE.NONE, CARD_SCOPE.NATIONAL)
          AddBankTypeToList(_list, CARD_TYPE.NONE, CARD_SCOPE.INTERNATIONAL)
        Case 3
          AddBankTypeToList(_list, CARD_TYPE.DEBIT, CARD_SCOPE.NATIONAL)
          AddBankTypeToList(_list, CARD_TYPE.CREDIT, CARD_SCOPE.NATIONAL)
          AddBankTypeToList(_list, CARD_TYPE.DEBIT, CARD_SCOPE.INTERNATIONAL)
          AddBankTypeToList(_list, CARD_TYPE.CREDIT, CARD_SCOPE.INTERNATIONAL)
        Case Else
          AddBankTypeToList(_list, CARD_TYPE.NONE, CARD_SCOPE.NONE)
          Log.Warning("GP Cashier.PaymentMethod.DifferentiateComission used 0 as default.")
      End Select

      For Each item As bank_card In _list
        _cur = New CURRENCIES_ENABLED()
        If Not CurrencyExchange.ReadCurrencyExchange(CurrencyExchangeType.CARD, m_currency_configuration.currency_ISO_Code_GP, item, _cur.foreign_currency_exchange) Then
          _cur.foreign_currency_exchange.ChangeRate = 1
          _cur.foreign_currency_exchange.Decimals = 0
          _cur.foreign_currency_exchange.FixedComission = 0
          _cur.foreign_currency_exchange.VariableComission = 0
          _cur.foreign_currency_exchange.FixedNR2 = 0
          _cur.foreign_currency_exchange.VariableNR2 = 0
          _cur.foreign_currency_exchange.BankCard = item
        Else
          BankCardEnabled = BankCardEnabled Or _cur.foreign_currency_exchange.Status
        End If
        BankCardCurrencyExchangeList.Add(item, _cur)

      Next

      ' Check
      If Not CurrencyExchange.ReadCurrencyExchange(CurrencyExchangeType.CHECK, m_currency_configuration.currency_ISO_Code_GP, m_currency_configuration.check_currency_exchange) Then
        m_currency_configuration.check_currency_exchange.ChangeRate = 1
        m_currency_configuration.check_currency_exchange.Decimals = 0
        m_currency_configuration.check_currency_exchange.FixedComission = 0
        m_currency_configuration.check_currency_exchange.VariableComission = 0
        m_currency_configuration.check_currency_exchange.FixedNR2 = 0
        m_currency_configuration.check_currency_exchange.VariableNR2 = 0
      End If

      ' Foreign Currency (Chips doesn't count as foreign currency) 
      _split_delimiters = New Char() {";"}
      _tmp_string = m_currency_configuration.currencies_accepted_GP.Replace(Cage.CHIPS_ISO_CODE, "")
      _tmp_string = _tmp_string.Replace(m_currency_configuration.currency_ISO_Code_GP, "")
      _currenciesAccepted = _tmp_string.Split(_split_delimiters, StringSplitOptions.RemoveEmptyEntries)

      If _currenciesAccepted.Length >= 1 Then
        m_currency_configuration.foreign_currency_ISO_Code_GP = String.Join(",", _currenciesAccepted)
      End If


      _selected_rows = m_currency_configuration.foreigncurrencies.Select("ISO_CODE = '" & m_currency_configuration.currency_ISO_Code_GP & "'")

      If Not _selected_rows.Length = 0 Then
        m_currency_configuration.foreigncurrencies.Rows.Remove(_selected_rows(0))
      End If

      For Each _row As DataRow In m_currency_configuration.foreigncurrencies.Rows
        _cur = New CURRENCIES_ENABLED()
        _foreign_currency_Ok = CurrencyExchange.ReadCurrencyExchange(CurrencyExchangeType.CURRENCY, _row("ISO_CODE"), _cur.foreign_currency_exchange)
        If _foreign_currency_Ok Then
          _cur.foreign_old_change_rate = _cur.foreign_currency_exchange.ChangeRate
          _cur.old_num_decimals = _cur.foreign_currency_exchange.Decimals
        Else
          ' If the currency is no in the database, initialice with default values
          _cur.foreign_currency_exchange.ChangeRate = 1
          _cur.foreign_old_change_rate = 1
          _cur.foreign_currency_exchange.Decimals = 0
          _cur.foreign_currency_exchange.FixedComission = 0
          _cur.foreign_currency_exchange.VariableComission = 0
          _cur.foreign_currency_exchange.FixedNR2 = 0
          _cur.foreign_currency_exchange.VariableNR2 = 0
          _cur.foreign_currency_exchange.CurrencyCode = _row("ISO_CODE")
        End If
        ForeignCurrencyExchangeList.Add(_row("ISO_CODE"), _cur)
      Next
      Return _final_db_result

    Catch

    End Try

    Return ENUM_DB_RC.DB_RC_ERROR_DB

  End Function ' CurrencyExchangeConf_DbRead

  '----------------------------------------------------------------------------
  ' PURPOSE : Read object (amount movements) from the DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - DbTrx: Transaction
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - Datatable:
  '
  ' NOTES :
  Public Function GetRestrictedAmountMovements(ByVal DbTrx As DB_TRX) As DataTable
    Dim _table As DataTable
    Dim _sb As StringBuilder

    _table = New DataTable()
    _sb = New StringBuilder()

    Try

      _sb.AppendLine("   SELECT *")
      _sb.AppendLine("   FROM")
      _sb.AppendLine("      (  SELECT    CMD.CMD_ISO_CODE    AS CMD_ISO_CODE ")
      _sb.AppendLine("                  , CMD.CMD_DENOMINATION      AS CMD_DENOMINATION ")
      _sb.AppendLine("            FROM    CAGE_MOVEMENT_DETAILS CMD ")
      _sb.AppendLine("                       INNER JOIN CAGE_PENDING_MOVEMENTS CGM ON CMD.CMD_MOVEMENT_ID = CGM.CPM_MOVEMENT_ID")
      _sb.AppendLine("       UNION ALL ")
      _sb.AppendLine("          SELECT    CM.CM_CURRENCY_ISO_CODE       AS CMD_ISO_CODE")
      _sb.AppendLine("                  , CM.CM_CURRENCY_DENOMINATION   AS CMD_DENOMINATION")
      _sb.AppendLine("            FROM    CAGE_MOVEMENTS  CGM ")
      _sb.AppendLine("                       INNER JOIN CAGE_PENDING_MOVEMENTS CPM ON CGM.CGM_MOVEMENT_ID = CPM.CPM_MOVEMENT_ID  ")
      _sb.AppendLine("                       INNER JOIN CASHIER_MOVEMENTS CM ON CGM.CGM_cashier_session_id = CM.cm_session_id  ")
      _sb.AppendLine("           WHERE    CPM.CPM_USER_ID = -1 ")
      _sb.AppendLine("                    AND CGM.CGM_CASHIER_SESSION_ID <> NULL")
      _sb.AppendLine("                    AND CM.cm_type IN (@pCloseSession, @pFillerOut)")
      _sb.AppendLine("      ) Temp")
      _sb.AppendLine("   GROUP BY     Temp.CMD_ISO_CODE, Temp.CMD_DENOMINATION")

      Using _sql_cmd As New SqlCommand(_sb.ToString)
        _sql_cmd.Parameters.Add("@pCloseSession", SqlDbType.Int).Value = CASHIER_MOVEMENT.CAGE_CLOSE_SESSION
        _sql_cmd.Parameters.Add("@pFillerOut", SqlDbType.Int).Value = CASHIER_MOVEMENT.CAGE_FILLER_OUT

        Using _sql_da As New SqlDataAdapter(_sql_cmd)

          DbTrx.Fill(_sql_da, _table)

        End Using

      End Using

    Catch ex As Exception

      Log.Exception(ex)
    End Try

    Return _table
  End Function ' GetRestrictedAmountMovements

#End Region

End Class
