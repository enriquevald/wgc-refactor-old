'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_buckets_multiplier.vb
' DESCRIPTION:   Configuration buckets multiplier
' AUTHOR:        Xavi Guirado Jornet
' CREATION DATE: 16-AUG-2016
'
' REVISION HISTORY:
'
' Date         Author      Description
' -----------  ---------   ------------------------------------------
' 16-AGU-2016  XGJ         Initial version.
' 03-FEB-2017  ETP         Bug 23981: Buckets: Not controlet exception when creating new multipliyer.
' -------------------------------------------------------------------

Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text
Imports System.IO

Public Class CLASS_BUCKETS_MULTIPLIER
  Inherits CLASS_BASE

#Region " Enums "

  Public Enum ENUM_TYPE_MESSAGE
    WITHOUT_CARD = 0
    ANONYMOUS_ACCOUNT = 1
    CANCELED_ACCOUNT = 2
  End Enum

#End Region

#Region " Consts "
  Private Const MSG_LONG_LINE_LCD_1 As Integer = 20
#End Region

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_BUCKET
    Public bucket_id As Integer
    Public bucket_site_list As String
    Public bucket_terminal_list As New TerminalList
    Public bucket_enabled As Boolean
    Public bucket_order As Integer
    Public bucket_schedule_start As DateTime
    Public bucket_schedule_end As DateTime
    Public bucket_schedule_weekday As Integer
    Public bucket_schedule1_time_from As Integer
    Public bucket_schedule1_time_to As Integer
    Public bucket_schedule2_enabled As Boolean
    Public bucket_schedule2_time_from As Integer
    Public bucket_schedule2_time_to As Integer
    Public bucket_description As String
    Public bucket_multiplier As Decimal
    Public bucket_afected_id As Integer
    Public bucket_name As String
    Public bucket_level As String
  End Class

#End Region

#Region " Members "

  Protected m_bucket As New TYPE_BUCKET
  Private m_is_center As Boolean = GeneralParam.GetBoolean("MultiSite", "IsCenter", False)

#End Region

#Region " Properties "
  Dim ResourceId As Integer

  Public Property MsgId() As Integer
    Get
      Return m_bucket.bucket_id
    End Get
    Set(ByVal Value As Integer)
      m_bucket.bucket_id = Value
    End Set
  End Property

  Public Property SiteList() As String
    Get
      Return m_bucket.bucket_site_list
    End Get
    Set(ByVal Value As String)
      m_bucket.bucket_site_list = Value
    End Set
  End Property

  Public Property TerminalList() As TerminalList
    Get
      Return m_bucket.bucket_terminal_list
    End Get
    Set(ByVal Value As TerminalList)
      m_bucket.bucket_terminal_list = Value
    End Set
  End Property

  Public Property Enabled() As Boolean
    Get
      Return m_bucket.bucket_enabled
    End Get
    Set(ByVal Value As Boolean)
      m_bucket.bucket_enabled = Value
    End Set
  End Property

  Public Property Order() As Integer
    Get
      Return m_bucket.bucket_order
    End Get
    Set(ByVal Value As Integer)
      m_bucket.bucket_order = Value
    End Set
  End Property

  Public Property ScheduleStart() As DateTime
    Get
      Return m_bucket.bucket_schedule_start
    End Get
    Set(ByVal Value As DateTime)
      m_bucket.bucket_schedule_start = Value
    End Set
  End Property

  Public Property ScheduleEnd() As DateTime
    Get
      Return m_bucket.bucket_schedule_end
    End Get
    Set(ByVal Value As DateTime)
      m_bucket.bucket_schedule_end = Value
    End Set
  End Property

  Public Property ScheduleWeekday() As Integer
    Get
      Return m_bucket.bucket_schedule_weekday
    End Get
    Set(ByVal Value As Integer)
      m_bucket.bucket_schedule_weekday = Value
    End Set
  End Property

  Public Property Schedule1TimeFrom() As Integer
    Get
      Return m_bucket.bucket_schedule1_time_from
    End Get
    Set(ByVal Value As Integer)
      m_bucket.bucket_schedule1_time_from = Value
    End Set
  End Property

  Public Property Schedule1TimeTo() As Integer
    Get
      Return m_bucket.bucket_schedule1_time_to
    End Get
    Set(ByVal Value As Integer)
      m_bucket.bucket_schedule1_time_to = Value
    End Set
  End Property

  Public Property Schedule2Enabled() As Boolean
    Get
      Return m_bucket.bucket_schedule2_enabled
    End Get
    Set(ByVal Value As Boolean)
      m_bucket.bucket_schedule2_enabled = Value
    End Set
  End Property

  Public Property Schedule2TimeFrom() As Integer
    Get
      Return m_bucket.bucket_schedule2_time_from
    End Get
    Set(ByVal Value As Integer)
      m_bucket.bucket_schedule2_time_from = Value
    End Set
  End Property

  Public Property Schedule2TimeTo() As Integer
    Get
      Return m_bucket.bucket_schedule2_time_to
    End Get
    Set(ByVal Value As Integer)
      m_bucket.bucket_schedule2_time_to = Value
    End Set
  End Property

  Public Property Description() As String
    Get
      Return m_bucket.bucket_description
    End Get
    Set(ByVal Value As String)
      m_bucket.bucket_description = Value
    End Set
  End Property

  Public Property Multiplier() As Decimal
    Get
      Return m_bucket.bucket_multiplier
    End Get
    Set(ByVal Value As Decimal)
      m_bucket.bucket_multiplier = Value
    End Set
  End Property

  Public Property BucketAfectedId() As Integer
    Get
      Return m_bucket.bucket_afected_id
    End Get
    Set(ByVal Value As Integer)
      m_bucket.bucket_afected_id = Value
    End Set
  End Property

  Public Property BucketName() As String
    Get
      Return m_bucket.bucket_name
    End Get
    Set(ByVal Value As String)
      m_bucket.bucket_name = Value
    End Set
  End Property

  Public Property BucketLevels() As String
    Get
      Return m_bucket.bucket_level
    End Get
    Set(value As String)
      m_bucket.bucket_level = value
    End Set
  End Property
#End Region

#Region "Overrides"

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _field_value As String
    Dim _empty_value As String
    Dim elapsed As TimeSpan
    Dim _levels_apply As String
    Dim _level_description As String

    _levels_apply = String.Empty
    _level_description = String.Empty
    _empty_value = "---"

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)

    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(7513), "'" & Me.Description & "'")
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6026), Me.Description) ' Descripcion
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6969), Me.BucketName) ' Bucket
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6022), Me.Multiplier) ' Multiplicador

    If Me.Enabled Then
      _field_value = GLB_NLS_GUI_CONFIGURATION.GetString(264)
    Else
      _field_value = GLB_NLS_GUI_CONFIGURATION.GetString(265)
    End If

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(3422), _field_value)         ' 3421 "Habilitada"
    Call _auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(483), GLB_NLS_GUI_CONTROLS.GetString(486) + ": " + _
                                TerminalList.AuditStringType(GLB_NLS_GUI_CONFIGURATION.GetString(497), _
                                                             GLB_NLS_GUI_CONFIGURATION.GetString(499), _
                                                             GLB_NLS_GUI_CONFIGURATION.GetString(498)))


    ' Group list
    Call _auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(483), GLB_NLS_GUI_CONTROLS.GetString(482) + ": " + _
                               TerminalList.AuditStringList(GROUP_ELEMENT_TYPE.GROUP, _
                                                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Prviders list
    Call _auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(483), GLB_NLS_GUI_CONTROLS.GetString(478) + ": " + _
                               TerminalList.AuditStringList(GROUP_ELEMENT_TYPE.PROV, _
                                                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Zones list
    Call _auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(483), GLB_NLS_GUI_CONTROLS.GetString(479) + ": " + _
                               TerminalList.AuditStringList(GROUP_ELEMENT_TYPE.ZONE, _
                                                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Areas list
    Call _auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(483), GLB_NLS_GUI_CONTROLS.GetString(480) + ": " + _
                               TerminalList.AuditStringList(GROUP_ELEMENT_TYPE.AREA, _
                                                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Banks list
    Call _auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(483), GLB_NLS_GUI_CONTROLS.GetString(481) + ": " + _
                               TerminalList.AuditStringList(GROUP_ELEMENT_TYPE.BANK, _
                                                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Terminals list
    Call _auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(483), GLB_NLS_GUI_CONTROLS.GetString(483) + ": " + _
                               TerminalList.AuditStringList(GROUP_ELEMENT_TYPE.TERM, _
                                                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Date Start
    _field_value = GUI_FormatDate(Me.ScheduleStart, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(289), _field_value)

    ' Date Finish
    If Me.ScheduleEnd = Nothing Then
      _field_value = _empty_value
    Else
      _field_value = GUI_FormatDate(Me.ScheduleEnd, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(290), _field_value)

    ' Schedule data
    ' Schedule weekdays
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(299), WorkingDaysText(Me.ScheduleWeekday))

    ' Schedule timetable 
    elapsed = TimeSpan.FromSeconds(Me.Schedule1TimeFrom)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(292), GLB_NLS_GUI_PLAYER_TRACKING.GetString(297) & " " + elapsed.ToString())
    elapsed = TimeSpan.FromSeconds(Me.Schedule1TimeTo)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(292), GLB_NLS_GUI_PLAYER_TRACKING.GetString(298) & " " + elapsed.ToString())

    If Me.Schedule2Enabled Then
      elapsed = TimeSpan.FromSeconds(Me.Schedule2TimeFrom)
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(293), GLB_NLS_GUI_PLAYER_TRACKING.GetString(297) & " " + elapsed.ToString())
      elapsed = TimeSpan.FromSeconds(Me.Schedule2TimeTo)
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(293), GLB_NLS_GUI_PLAYER_TRACKING.GetString(298) & " " + elapsed.ToString())
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(293), GLB_NLS_GUI_PLAYER_TRACKING.GetString(297) & AUDIT_NONE_STRING)
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(293), GLB_NLS_GUI_PLAYER_TRACKING.GetString(298) & AUDIT_NONE_STRING)
    End If

    'If m_is_center Then
    '  Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1771), Me.SiteList) ' Sites
    'End If

    ' Levels
    If (m_bucket.bucket_level <> Nothing) Then
      For _index As Integer = 0 To m_bucket.bucket_level.Length - 1
        If (m_bucket.bucket_level.Substring(_index, 1) = "1") Then
          _level_description = "Level" + String.Format("{0:00}", (_index + 1)) + ".Name"
          If (_levels_apply <> String.Empty) Then
            _levels_apply = _levels_apply + "/"
          End If
          _levels_apply = _levels_apply + GeneralParam.GetString("PlayerTracking", _level_description)
        End If
      Next

    End If

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7821), _levels_apply)

    Return _auditor_data

  End Function

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As ENUM_STATUS

    _rc = ENUM_STATUS.STATUS_ERROR

    Using _db_trx As New DB_TRX()
      ' Insert bucket multiplier data
      _rc = InsertBucket(_db_trx.SqlTransaction)

    End Using

    Return _rc

  End Function

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As ENUM_STATUS

    Me.MsgId = ObjectId
    _rc = ENUM_STATUS.STATUS_ERROR

    Using _db_trx As New DB_TRX()
      ' Read bucket multiplier data
      _rc = ReadBucketMultiplier(_db_trx.SqlTransaction)
    End Using

    Return _rc

  End Function

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As Integer

    _rc = ENUM_STATUS.STATUS_ERROR

    Using _db_trx As New DB_TRX()
      ' Update bucket data
      _rc = UpdateBucketMultiplier(_db_trx.SqlTransaction)

    End Using

    Return _rc
  End Function

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim temp_object As CLASS_BUCKETS_MULTIPLIER

    temp_object = New CLASS_BUCKETS_MULTIPLIER
    temp_object.m_bucket.bucket_id = Me.m_bucket.bucket_id
    temp_object.m_bucket.bucket_site_list = Me.m_bucket.bucket_site_list
    temp_object.m_bucket.bucket_terminal_list = New TerminalList()
    temp_object.m_bucket.bucket_terminal_list.FromXml(Me.m_bucket.bucket_terminal_list.ToXml())
    temp_object.m_bucket.bucket_enabled = Me.m_bucket.bucket_enabled
    temp_object.m_bucket.bucket_schedule_start = Me.m_bucket.bucket_schedule_start
    temp_object.m_bucket.bucket_schedule_end = Me.m_bucket.bucket_schedule_end
    temp_object.m_bucket.bucket_schedule_weekday = Me.m_bucket.bucket_schedule_weekday
    temp_object.m_bucket.bucket_schedule1_time_from = Me.m_bucket.bucket_schedule1_time_from
    temp_object.m_bucket.bucket_schedule1_time_to = Me.m_bucket.bucket_schedule1_time_to
    temp_object.m_bucket.bucket_schedule2_enabled = Me.m_bucket.bucket_schedule2_enabled
    temp_object.m_bucket.bucket_schedule2_time_from = Me.m_bucket.bucket_schedule2_time_from
    temp_object.m_bucket.bucket_schedule2_time_to = Me.m_bucket.bucket_schedule2_time_to
    temp_object.m_bucket.bucket_description = Me.m_bucket.bucket_description
    temp_object.m_bucket.bucket_multiplier = Me.m_bucket.bucket_multiplier
    temp_object.m_bucket.bucket_afected_id = Me.m_bucket.bucket_afected_id
    temp_object.m_bucket.bucket_name = Me.m_bucket.bucket_name
    temp_object.m_bucket.bucket_level = Me.m_bucket.bucket_level

    Return temp_object

  End Function

#End Region

#Region "Private"
  Private Function ReadBucketMultiplier(ByVal SqlTrans As SqlTransaction) As Integer

    Dim _sb As StringBuilder
    Dim _data_table As DataTable
    Dim _xml As String

    Try

      _sb = New StringBuilder()

      _sb.AppendLine(" SELECT   a.BM_BUCKET_ID ")
      _sb.AppendLine("        , a.BM_BUCKET_DESCRIPTION ")
      _sb.AppendLine("        , a.BM_BUCKET_AFECTED_ID ")
      _sb.AppendLine("        , a.BM_BUCKET_MULTIPLIER ")
      _sb.AppendLine("        , a.BM_BUCKET_TERMINALS_LIST ")
      _sb.AppendLine("        , a.BM_BUCKET_ENABLED ")
      _sb.AppendLine("        , a.BM_BUCKET_ORDER ")
      _sb.AppendLine("        , a.BM_BUCKET_SCHEDULE_START ")
      _sb.AppendLine("        , a.BM_BUCKET_SCHEDULE_END ")
      _sb.AppendLine("        , a.BM_BUCKET_SCHEDULE_WEEKDAY ")
      _sb.AppendLine("        , a.BM_BUCKET_SCHEDULE1_TIME_FROM ")
      _sb.AppendLine("        , a.BM_BUCKET_SCHEDULE1_TIME_TO ")
      _sb.AppendLine("        , a.BM_BUCKET_SCHEDULE2_ENABLED ")
      _sb.AppendLine("        , a.BM_BUCKET_SCHEDULE2_TIME_FROM ")
      _sb.AppendLine("        , a.BM_BUCKET_SCHEDULE2_TIME_TO ")
      _sb.AppendLine("        , b.BU_NAME ")
      _sb.AppendLine("        , a.BM_LEVEL ")
      _sb.AppendLine("   FROM   BUCKETS_MULTIPLIER_SCHEDULE a, BUCKETS b")
      _sb.AppendLine(" WHERE BM_BUCKET_ID = " + Me.MsgId.ToString() + " AND a.BM_BUCKET_AFECTED_ID = b.BU_BUCKET_ID")

      _data_table = GUI_GetTableUsingSQL(_sb.ToString(), 5000)

      If IsNothing(_data_table) Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

      If _data_table.Rows.Count() = 0 Then
        Return ENUM_STATUS.STATUS_NOT_FOUND
      End If

      If _data_table.Rows.Count() <> 1 Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

      Me.MsgId = IIf(IsDBNull(_data_table.Rows(0).Item("BM_BUCKET_ID")), 0, _data_table.Rows(0).Item("BM_BUCKET_ID"))

      Me.SiteList = Nothing

      ' PM_PROVIDER_LIST
      _xml = Nothing
      If _data_table.Rows(0).Item("BM_BUCKET_TERMINALS_LIST") IsNot DBNull.Value Then
        _xml = _data_table.Rows(0).Item("BM_BUCKET_TERMINALS_LIST")
      End If

      Me.TerminalList.FromXml(_xml)
      Me.Enabled = IIf(IsDBNull(_data_table.Rows(0).Item("BM_BUCKET_ENABLED")), Nothing, _data_table.Rows(0).Item("BM_BUCKET_ENABLED"))
      Me.Order = IIf(IsDBNull(_data_table.Rows(0).Item("BM_BUCKET_ORDER")), Nothing, _data_table.Rows(0).Item("BM_BUCKET_ORDER"))
      Me.ScheduleStart = IIf(IsDBNull(_data_table.Rows(0).Item("BM_BUCKET_SCHEDULE_START")), Nothing, _data_table.Rows(0).Item("BM_BUCKET_SCHEDULE_START"))
      Me.ScheduleEnd = IIf(IsDBNull(_data_table.Rows(0).Item("BM_BUCKET_SCHEDULE_END")), Nothing, _data_table.Rows(0).Item("BM_BUCKET_SCHEDULE_END"))
      Me.ScheduleWeekday = IIf(IsDBNull(_data_table.Rows(0).Item("BM_BUCKET_SCHEDULE_WEEKDAY")), Nothing, _data_table.Rows(0).Item("BM_BUCKET_SCHEDULE_WEEKDAY"))
      Me.Schedule1TimeFrom = IIf(IsDBNull(_data_table.Rows(0).Item("BM_BUCKET_SCHEDULE1_TIME_FROM")), Nothing, _data_table.Rows(0).Item("BM_BUCKET_SCHEDULE1_TIME_FROM"))
      Me.Schedule1TimeTo = IIf(IsDBNull(_data_table.Rows(0).Item("BM_BUCKET_SCHEDULE1_TIME_TO")), Nothing, _data_table.Rows(0).Item("BM_BUCKET_SCHEDULE1_TIME_TO"))
      Me.Schedule2Enabled = IIf(IsDBNull(_data_table.Rows(0).Item("BM_BUCKET_SCHEDULE2_ENABLED")), Nothing, _data_table.Rows(0).Item("BM_BUCKET_SCHEDULE2_ENABLED"))
      Me.Schedule2TimeFrom = IIf(IsDBNull(_data_table.Rows(0).Item("BM_BUCKET_SCHEDULE2_TIME_FROM")), Nothing, _data_table.Rows(0).Item("BM_BUCKET_SCHEDULE2_TIME_FROM"))
      Me.Schedule2TimeTo = IIf(IsDBNull(_data_table.Rows(0).Item("BM_BUCKET_SCHEDULE2_TIME_TO")), Nothing, _data_table.Rows(0).Item("BM_BUCKET_SCHEDULE2_TIME_TO"))
      Me.Description = IIf(IsDBNull(_data_table.Rows(0).Item("BM_BUCKET_DESCRIPTION")), Nothing, _data_table.Rows(0).Item("BM_BUCKET_DESCRIPTION"))
      Me.Multiplier = IIf(IsDBNull(_data_table.Rows(0).Item("BM_BUCKET_MULTIPLIER")), Nothing, _data_table.Rows(0).Item("BM_BUCKET_MULTIPLIER"))
      Me.BucketName = IIf(IsDBNull(_data_table.Rows(0).Item("BU_NAME")), Nothing, _data_table.Rows(0).Item("BU_NAME"))
      Me.BucketLevels = IIf(IsDBNull(_data_table.Rows(0).Item("BM_LEVEL")), "0", _data_table.Rows(0).Item("BM_LEVEL"))

      Return ENUM_STATUS.STATUS_OK

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return ENUM_STATUS.STATUS_ERROR

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Adds a LCD Message object into DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - SqlTrans
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_DEPENDENCIES
  '     - STATUS_DUPLICATE_KEY
  '     - STATUS_ERROR
  '
  ' NOTES :

  Private Function InsertBucket(ByVal SqlTrans As SqlTransaction) As Integer

    Dim _sb As StringBuilder
    Dim _row_affected As Integer
    Dim _terminals_list_xml As String
    Dim _message_xml As String
    Dim _unique_id As Int64
    Dim _sequence_value As Int64
    Dim _site_id As Int32

    Try
      _terminals_list_xml = Me.TerminalList.ToXml()
      _message_xml = String.Empty

      If Not Sequences.GetValue(SqlTrans, WSI.Common.SequenceId.LCDMessage, _sequence_value) Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

      _site_id = GeneralParam.GetInt32("Site", "Identifier", 0)
      If _site_id > 999 Then
        Log.Error("SiteId is greater than 999")
        Return ENUM_STATUS.STATUS_ERROR

      End If
      If _site_id < 0 Then
        Log.Error("SiteId not configured!")
        Return ENUM_STATUS.STATUS_ERROR

      End If

      _unique_id = _sequence_value * 1000 + _site_id Mod 1000

      _sb = New StringBuilder()
      _sb.AppendLine("DECLARE   @NewOrder          as INT    ")
      _sb.AppendLine(" SELECT   @NewOrder = ISNULL(MAX(BM_BUCKET_ORDER), 0) + 1")
      _sb.AppendLine("   FROM   BUCKETS_MULTIPLIER_SCHEDULE ")
      _sb.AppendLine("  WHERE   BM_BUCKET_ID % 1000  = @pSiteId ")

      _sb.AppendLine("INSERT INTO BUCKETS_MULTIPLIER_SCHEDULE")
      _sb.AppendLine("           (  BM_BUCKET_ID						     ")
      _sb.AppendLine("           	, BM_BUCKET_DESCRIPTION				 ")
      _sb.AppendLine("           	, BM_BUCKET_AFECTED_ID			   ")
      _sb.AppendLine("           	, BM_BUCKET_MULTIPLIER			   ")
      _sb.AppendLine("           	, BM_BUCKET_TERMINALS_LIST		     ")
      _sb.AppendLine("           	, BM_BUCKET_ENABLED					        ")
      _sb.AppendLine("           	, BM_BUCKET_SCHEDULE_START				  ")
      _sb.AppendLine("           	, BM_BUCKET_SCHEDULE_END					  ")
      _sb.AppendLine("           	, BM_BUCKET_SCHEDULE_WEEKDAY 	      ")
      _sb.AppendLine("           	, BM_BUCKET_SCHEDULE1_TIME_FROM     ")
      _sb.AppendLine("           	, BM_BUCKET_SCHEDULE1_TIME_TO       ")
      _sb.AppendLine("           	, BM_BUCKET_SCHEDULE2_ENABLED       ")
      _sb.AppendLine("           	, BM_BUCKET_SCHEDULE2_TIME_FROM     ")
      _sb.AppendLine("           	, BM_BUCKET_SCHEDULE2_TIME_TO       ")
      _sb.AppendLine("           	, BM_LEVEL                          ")
      _sb.AppendLine("           	, BM_BUCKET_ORDER )    ")
      _sb.AppendLine("     VALUES									              ")
      _sb.AppendLine("           (  @pUniqueId			          ")
      _sb.AppendLine("           	, @pDescription			            ")
      _sb.AppendLine("           	, @pAfectedId			            ")
      _sb.AppendLine("           	, @pMultiplier			            ")
      _sb.AppendLine("           	, @pTerminalList 				      ")
      _sb.AppendLine("           	, @pEnabled						        ")
      _sb.AppendLine("          	, @pScheduleStart			        ")
      _sb.AppendLine("          	, @pScheduleEnd 			        ")
      _sb.AppendLine("          	, @pScheduleWeekday           ")
      _sb.AppendLine("           	, @pSchedule1TimeFrom  		    ")
      _sb.AppendLine("           	, @pSchedule1TimeTo           ")
      _sb.AppendLine("           	, @pSchedule2Enabled           ")
      _sb.AppendLine("           	, @pSchedule2TimeFrom  		    ")
      _sb.AppendLine("           	, @pSchedule2TimeTo           ")
      _sb.AppendLine("           	, @pLevel           ")
      _sb.AppendLine("           	, @NewOrder )        ")

      Using _cmd As New SqlCommand(_sb.ToString(), SqlTrans.Connection, SqlTrans)

        _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = _site_id
        _cmd.Parameters.Add("@pUniqueId", SqlDbType.BigInt).Value = _unique_id

        If m_is_center And Me.SiteList <> Nothing Then
          _cmd.Parameters.Add("@pSiteList", SqlDbType.Xml).Value = SitesFromStringToXML(Me.SiteList)
        Else
          _cmd.Parameters.Add("@pSiteList", SqlDbType.Xml).Value = DBNull.Value
        End If

        _cmd.Parameters.Add("@pTerminalList", SqlDbType.NVarChar).Value = IIf(IsNothing(_terminals_list_xml), DBNull.Value, _terminals_list_xml)

        _cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).Value = Me.Enabled
        _cmd.Parameters.Add("@pScheduleStart", SqlDbType.DateTime).Value = Me.ScheduleStart
        _cmd.Parameters.Add("@pScheduleEnd", SqlDbType.DateTime).Value = IIf(Me.ScheduleEnd = Nothing, DBNull.Value, Me.ScheduleEnd)
        _cmd.Parameters.Add("@pScheduleWeekday", SqlDbType.Int).Value = Me.ScheduleWeekday
        _cmd.Parameters.Add("@pSchedule1TimeFrom", SqlDbType.Int).Value = Me.Schedule1TimeFrom
        _cmd.Parameters.Add("@pSchedule1TimeTo", SqlDbType.Int).Value = Me.Schedule1TimeTo
        _cmd.Parameters.Add("@pSchedule2Enabled", SqlDbType.Bit).Value = Me.Schedule2Enabled
        _cmd.Parameters.Add("@pSchedule2TimeFrom", SqlDbType.Int).Value = Me.Schedule2TimeFrom
        _cmd.Parameters.Add("@pSchedule2TimeTo", SqlDbType.Int).Value = Me.Schedule2TimeTo
        _cmd.Parameters.Add("@pMessage", SqlDbType.NVarChar).Value = _message_xml
        _cmd.Parameters.Add("@pDescription", SqlDbType.NVarChar).Value = Me.Description
        _cmd.Parameters.Add("@pMultiplier", SqlDbType.Decimal).Value = Me.Multiplier
        _cmd.Parameters.Add("@pAfectedId", SqlDbType.Int).Value = Me.BucketAfectedId
        _cmd.Parameters.Add("@pLevel", SqlDbType.NVarChar).Value = Me.BucketLevels

        _row_affected = _cmd.ExecuteNonQuery()

        If _row_affected < 1 Then
          Return ENUM_STATUS.STATUS_ERROR
        End If

        Me.MsgId = _unique_id

        SqlTrans.Commit()

        Return ENUM_STATUS.STATUS_OK

      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return ENUM_STATUS.STATUS_ERROR

  End Function ' InsertGamingTable

  '----------------------------------------------------------------------------
  ' PURPOSE : Update a bucket multiplier object into DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - SqlTrans
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_DEPENDENCIES
  '     - STATUS_DUPLICATE_KEY
  '     - STATUS_ERROR
  '
  ' NOTES :

  Private Function UpdateBucketMultiplier(ByVal SqlTrans As SqlTransaction) As Integer

    Dim _sb As StringBuilder
    Dim _row_affected As Integer
    Dim _terminals_list_xml As String

    _terminals_list_xml = Me.TerminalList.ToXml()

    ' If the ID to be modified is the multisite message and we are on site -> error
    If ((Me.MsgId Mod 1000) = 0) And Not GeneralParam.GetBoolean("MultiSite", "IsCenter", False) Then
      Return ENUM_STATUS.STATUS_ERROR
    End If

    Try

      _sb = New StringBuilder()

      _sb.AppendLine("UPDATE   BUCKETS_MULTIPLIER_SCHEDULE ")
      _sb.AppendLine("   SET   BM_BUCKET_AFECTED_ID = @pAfectedId ")
      _sb.AppendLine("       , BM_BUCKET_TERMINALS_LIST = @pTerminalList ")
      _sb.AppendLine("       , BM_BUCKET_ENABLED  = @pEnabled ")
      _sb.AppendLine("       , BM_BUCKET_ORDER = @pOrder ")
      _sb.AppendLine("       , BM_BUCKET_SCHEDULE_START = @pScheduleStart ")
      _sb.AppendLine("       , BM_BUCKET_SCHEDULE_END = @pScheduleEnd ")
      _sb.AppendLine("       , BM_BUCKET_SCHEDULE_WEEKDAY = @pScheduleWeekday ")
      _sb.AppendLine("       , BM_BUCKET_SCHEDULE1_TIME_FROM = @pSchedule1TimeFrom ")
      _sb.AppendLine("       , BM_BUCKET_SCHEDULE1_TIME_TO = @pSchedule1TimeTo ")
      _sb.AppendLine("       , BM_BUCKET_SCHEDULE2_ENABLED = @pSchedule2Enabled ")
      _sb.AppendLine("       , BM_BUCKET_SCHEDULE2_TIME_FROM = @pSchedule2TimeFrom ")
      _sb.AppendLine("       , BM_BUCKET_SCHEDULE2_TIME_TO = @pSchedule2TimeTo ")
      _sb.AppendLine("       , BM_BUCKET_DESCRIPTION = @pDescription ")
      '_sb.AppendLine("       , BM_BUCKET_MASTER_SEQUENCE_ID = @pMasterSequenceId ")
      _sb.AppendLine("       , BM_BUCKET_MULTIPLIER = @pMultiplier ")
      _sb.AppendLine("       , BM_LEVEL = @pLevel ")
      _sb.AppendLine(" WHERE   BM_BUCKET_ID = @pID ")

      Using _cmd As New SqlCommand(_sb.ToString(), SqlTrans.Connection, SqlTrans)

        _cmd.Parameters.Add("@pAfectedId", SqlDbType.Int).Value = Me.BucketAfectedId
        _cmd.Parameters.Add("@pTerminalList", SqlDbType.NVarChar).Value = IIf(IsNothing(_terminals_list_xml), DBNull.Value, _terminals_list_xml)
        _cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).Value = Me.Enabled
        _cmd.Parameters.Add("@pOrder", SqlDbType.Int).Value = Me.Order
        _cmd.Parameters.Add("@pScheduleStart", SqlDbType.DateTime).Value = Me.ScheduleStart
        _cmd.Parameters.Add("@pScheduleEnd", SqlDbType.DateTime).Value = IIf(Me.ScheduleEnd = Nothing, DBNull.Value, Me.ScheduleEnd)
        _cmd.Parameters.Add("@pScheduleWeekday", SqlDbType.Int).Value = Me.ScheduleWeekday
        _cmd.Parameters.Add("@pSchedule1TimeFrom", SqlDbType.Int).Value = Me.Schedule1TimeFrom
        _cmd.Parameters.Add("@pSchedule1TimeTo", SqlDbType.Int).Value = Me.Schedule1TimeTo
        _cmd.Parameters.Add("@pSchedule2Enabled", SqlDbType.Bit).Value = Me.Schedule2Enabled
        _cmd.Parameters.Add("@pSchedule2TimeFrom", SqlDbType.Int).Value = Me.Schedule2TimeFrom
        _cmd.Parameters.Add("@pSchedule2TimeTo", SqlDbType.Int).Value = Me.Schedule2TimeTo
        _cmd.Parameters.Add("@pDescription", SqlDbType.NVarChar).Value = Me.Description
        '_cmd.Parameters.Add("@pMasterSequenceId", SqlDbType.Int).Value = Me.MasterSequenceId
        _cmd.Parameters.Add("@pMultiplier", SqlDbType.Decimal).Value = Me.Multiplier
        _cmd.Parameters.Add("@pID", SqlDbType.Int).Value = Me.MsgId
        _cmd.Parameters.Add("@pLevel", SqlDbType.NVarChar).Value = Me.m_bucket.bucket_level

        _row_affected = _cmd.ExecuteNonQuery()

        If _row_affected <> 1 Then
          Return ENUM_STATUS.STATUS_ERROR
        End If

        SqlTrans.Commit()
        Return ENUM_STATUS.STATUS_OK

      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return ENUM_STATUS.STATUS_ERROR

  End Function ' InsertGamingTable

#End Region

#Region "Public"
  Public Sub ResetIdValues()

    Me.MsgId = 0

    If Not m_is_center Then
      Me.SiteList = Nothing
    End If

    Me.Order = 0
    Me.ResourceId = 0
    'Me.Timestamp = 0
    'Me.MasterSequenceId = 0
    'Me.ComputedOrder = 0

  End Sub

  Public Function SitesFromStringToXML(ByVal Sites As String) As String
    Dim _xml_sites As System.Xml.XmlDocument
    Dim _xml_element As System.Xml.XmlElement
    Dim _xml_node As System.Xml.XmlElement
    Dim _xml_attribute As System.Xml.XmlAttribute
    Dim _sites_list As String()

    _xml_sites = New System.Xml.XmlDocument

    If Not String.IsNullOrEmpty(Sites) Then
      _sites_list = Sites.Split(",")
      _xml_element = _xml_sites.AppendChild(_xml_sites.CreateElement("Sites"))
      _xml_sites.AppendChild(_xml_element)
      For Each _item As String In _sites_list
        _xml_node = _xml_sites.CreateElement("Site")
        _xml_attribute = _xml_sites.CreateAttribute("Id")
        _xml_attribute.Value = Int64.Parse(_item.Trim)
        _xml_node.Attributes.Append(_xml_attribute)
        _xml_element.AppendChild(_xml_node)
      Next

      Return _xml_sites.InnerXml
    End If

    Return ""
  End Function

  Public Function SitesFromXMLToString(ByVal Sites As String) As String
    Dim _xml_message As System.Xml.XmlDocument
    Dim _xml_node_list As System.Xml.XmlNodeList
    Dim _site_list As String

    _xml_message = New System.Xml.XmlDocument
    _site_list = ""

    If Not String.IsNullOrEmpty(Sites) Then
      _xml_message.LoadXml(Sites)
      _xml_node_list = _xml_message.GetElementsByTagName("Site")
      If _xml_node_list.Count > 0 Then
        For Each _item As System.Xml.XmlElement In _xml_node_list
          If Not String.IsNullOrEmpty(_site_list) Then
            _site_list += ", "
          End If
          _site_list += _item.Attributes("Id").Value
        Next
        Return _site_list
      End If
    End If
    Return ""

  End Function

  Public ReadOnly Property WorkingDaysText(ByVal Days As Integer) As String
    Get
      Dim _str_binary As String
      Dim _bit_array As Char()
      Dim _str_days As String

      _str_days = ""

      _str_binary = Convert.ToString(Days, 2)
      _str_binary = New String("0", 7 - _str_binary.Length) + _str_binary

      _bit_array = _str_binary.ToCharArray()

      If (_bit_array(6) = "1") Then _str_days = _str_days & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(325) '"Monday "
      If (_bit_array(5) = "1") Then _str_days = _str_days & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(326) '"Tuesday "
      If (_bit_array(4) = "1") Then _str_days = _str_days & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(327) '"Wednesday "
      If (_bit_array(3) = "1") Then _str_days = _str_days & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(328) '"Thursday "
      If (_bit_array(2) = "1") Then _str_days = _str_days & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(329) '"Friday "
      If (_bit_array(1) = "1") Then _str_days = _str_days & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(330) '"Saturday "
      If (_bit_array(0) = "1") Then _str_days = _str_days & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(331) '"Sunday "

      Return _str_days.TrimStart

    End Get
  End Property
#End Region

End Class
