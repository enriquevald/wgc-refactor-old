﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_creditline.vb
' DESCRIPTION:   Creditline Class
' AUTHOR:        David Perelló
' CREATION DATE: 13-MAR-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 13-MAR-2017  DPC    Initial Version
'--------------------------------------------------------------------
Imports GUI_CommonOperations
Imports System.Runtime.InteropServices
Imports System.Xml
Imports WSI.Common
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Text
Imports System.Data.SqlClient
Imports WSI.Common.CreditLines

Public Class CLASS_CREDITLINE
  Inherits CLASS_BASE

#Region " Enums "

  Public Enum MovementsNodeXML
    creditLimit
    TTO
    spent
    getMarker
    payback
    expirationDate
    createDate
    status
    iban
    signers
    creditLine
  End Enum

#End Region

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_CREDIT_LINE
    Public account_id As Int64
    Public holder_name As String
    Public limit_amount As Decimal
    Public tto_amount As Decimal
    Public spent_amount As Decimal
    Public iso_code As String
    Public status As Int16
    Public iban As String
    Public signers As String
    Public creation As DateTime
    Public update As DateTime
    Public expired As DateTime
    Public has_expired_date As Boolean
    Public last_payback As DateTime

    Public Sub New()
      Me.spent_amount = 0
      Me.iso_code = CurrencyExchange.GetNationalCurrency()
      Me.status = CreditLines.StatusDefinition.PendingApproval
      Me.signers = "<signers />"
    End Sub
  End Class

#End Region ' Structures

#Region " Members "

  Protected m_credit_line As New TYPE_CREDIT_LINE
  Private m_is_center As Boolean


#End Region ' Members

#Region "Constructor"
  Public Sub New()

    m_is_center = GeneralParam.GetBoolean("MultiSite", "IsCenter", False)

  End Sub

#End Region 'Constructor

#Region " Properties "

  Public Property AccountId() As Int64
    Get
      Return m_credit_line.account_id
    End Get
    Set(ByVal value As Int64)
      m_credit_line.account_id = value
    End Set
  End Property

  Public Property HolderName() As String
    Get
      Return m_credit_line.holder_name
    End Get
    Set(ByVal value As String)
      m_credit_line.holder_name = value
    End Set
  End Property

  Public Property LimitAmount() As Decimal
    Get
      Return m_credit_line.limit_amount
    End Get
    Set(ByVal value As Decimal)
      m_credit_line.limit_amount = value
    End Set
  End Property

  Public Property TTOAmount() As Decimal
    Get
      Return m_credit_line.tto_amount
    End Get
    Set(ByVal value As Decimal)
      m_credit_line.tto_amount = value
    End Set
  End Property

  Public Property SpentAmount() As Decimal
    Get
      Return m_credit_line.spent_amount
    End Get
    Set(ByVal value As Decimal)
      m_credit_line.spent_amount = value
    End Set
  End Property

  Public Property IsoCode() As String
    Get
      Return m_credit_line.iso_code
    End Get
    Set(ByVal value As String)
      m_credit_line.iso_code = value
    End Set
  End Property

  Public Property Status() As CreditLines.StatusDefinition
    Get
      Return m_credit_line.status
    End Get
    Set(ByVal value As CreditLines.StatusDefinition)
      m_credit_line.status = value
    End Set
  End Property

  Public Property Iban() As String
    Get
      Return m_credit_line.iban
    End Get
    Set(ByVal value As String)
      m_credit_line.iban = value
    End Set
  End Property

  Public Property Signers() As String
    Get
      Return m_credit_line.signers
    End Get
    Set(ByVal value As String)
      m_credit_line.signers = value
    End Set
  End Property

  Public Property Creation() As DateTime
    Get
      Return m_credit_line.creation
    End Get
    Set(ByVal value As DateTime)
      m_credit_line.creation = value
    End Set
  End Property

  Public Property Update() As DateTime
    Get
      Return m_credit_line.update
    End Get
    Set(ByVal value As DateTime)
      m_credit_line.update = value
    End Set
  End Property

  Public Property Expired() As DateTime
    Get
      Return m_credit_line.expired
    End Get
    Set(ByVal value As DateTime)
      m_credit_line.expired = value
    End Set
  End Property

  Public Property HasExpired() As Boolean
    Get
      Return m_credit_line.has_expired_date
    End Get
    Set(ByVal value As Boolean)
      m_credit_line.has_expired_date = value
    End Set
  End Property

  Public Property LastPayback() As DateTime
    Get
      Return m_credit_line.last_payback
    End Get
    Set(ByVal value As DateTime)
      m_credit_line.last_payback = value
    End Set
  End Property

  Private m_credit_line_read As CLASS_CREDITLINE
  Public Property CreditLineRead() As CLASS_CREDITLINE
    Get
      Return m_credit_line_read
    End Get
    Set(ByVal value As CLASS_CREDITLINE)
      m_credit_line_read = value
    End Set
  End Property

#End Region ' Properties

#Region " Overrides "

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function AuditorData() As CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _xml_signers As New XmlDocument
    Dim _signers As List(Of String)

    _signers = New List(Of String)
    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_CREDIT_LINES)

    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(7947), String.Format("{0} - {1}", Me.AccountId, Me.HolderName))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(496), AccountId)

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7948), GUI_FormatCurrency(LimitAmount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1997), IIf(HasExpired, Expired, AUDIT_NONE_STRING))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7949), GUI_FormatCurrency(TTOAmount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2555), GUI_FormatCurrency(SpentAmount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT))

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7952), IIf(String.IsNullOrEmpty(Iban), AUDIT_NONE_STRING, Iban))

    Select Case Me.Status
      Case CreditLines.StatusDefinition.PendingApproval
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7955), CreditLines.CreditLine.StatusToString(CreditLines.StatusDefinition.PendingApproval))
      Case CreditLines.StatusDefinition.NotApproved
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7955), CreditLines.CreditLine.StatusToString(CreditLines.StatusDefinition.NotApproved))
      Case CreditLines.StatusDefinition.Approved
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7955), CreditLines.CreditLine.StatusToString(CreditLines.StatusDefinition.Approved))
      Case CreditLines.StatusDefinition.Expired
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7955), CreditLines.CreditLine.StatusToString(CreditLines.StatusDefinition.Expired))
      Case CreditLines.StatusDefinition.Suspended
        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7955), CreditLines.CreditLine.StatusToString(CreditLines.StatusDefinition.Suspended))
    End Select

    _xml_signers.LoadXml(Me.Signers)

    For Each _signer As XmlNode In _xml_signers.SelectNodes("//signers/signer/Name")
      _signers.Add(_signer.InnerText)
    Next

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7953), IIf(_signers.Count > 0, String.Join(", ", _signers.ToArray), AUDIT_NONE_STRING))

    Return _auditor_data

  End Function

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS
    Return Nothing
  End Function

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS
    Return InsertCreditLine()
  End Function

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <param name="ObjectId"></param>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Read(ObjectId As Object, ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS
    Return GetCreditLine(ObjectId)
  End Function

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS
    Return UpdateCreditLine()
  End Function

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function Duplicate() As CLASS_BASE

    Dim _tmp_credit_line As CLASS_CREDITLINE

    _tmp_credit_line = New CLASS_CREDITLINE
    _tmp_credit_line.m_credit_line.account_id = m_credit_line.account_id
    _tmp_credit_line.m_credit_line.holder_name = m_credit_line.holder_name
    _tmp_credit_line.m_credit_line.limit_amount = m_credit_line.limit_amount
    _tmp_credit_line.m_credit_line.tto_amount = m_credit_line.tto_amount
    _tmp_credit_line.m_credit_line.spent_amount = m_credit_line.spent_amount
    _tmp_credit_line.m_credit_line.iso_code = m_credit_line.iso_code
    _tmp_credit_line.m_credit_line.status = m_credit_line.status
    _tmp_credit_line.m_credit_line.iban = m_credit_line.iban
    _tmp_credit_line.m_credit_line.signers = m_credit_line.signers
    _tmp_credit_line.m_credit_line.creation = m_credit_line.creation
    _tmp_credit_line.m_credit_line.update = m_credit_line.update
    _tmp_credit_line.m_credit_line.expired = m_credit_line.expired
    _tmp_credit_line.m_credit_line.has_expired_date = m_credit_line.has_expired_date
    _tmp_credit_line.m_credit_line.last_payback = m_credit_line.last_payback

    Return _tmp_credit_line

  End Function

#End Region ' Overrides

#Region " Public Methods "

  ''' <summary>
  ''' Resturns the status list
  ''' </summary>
  ''' <param name="Status"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function getListStatus(ByVal Status As StatusDefinition) As Dictionary(Of Integer, String)

    Dim _dic_status As Dictionary(Of Integer, String)

    _dic_status = New Dictionary(Of Integer, String)
    SetStatus(_dic_status, StatusDefinition.PendingApproval)

    Select Case Status
      Case StatusDefinition.None
      Case StatusDefinition.PendingApproval, StatusDefinition.NotApproved
        SetStatus(_dic_status, StatusDefinition.NotApproved)
      Case StatusDefinition.Approved
        SetStatus(_dic_status, StatusDefinition.Approved)
        SetStatus(_dic_status, StatusDefinition.Suspended)
      Case StatusDefinition.Suspended
        SetStatus(_dic_status, StatusDefinition.Suspended)
      Case StatusDefinition.Expired
        SetStatus(_dic_status, StatusDefinition.Expired)
      Case StatusDefinition.All
        SetStatus(_dic_status, StatusDefinition.Approved)
        SetStatus(_dic_status, StatusDefinition.Expired)
        SetStatus(_dic_status, StatusDefinition.NotApproved)
        SetStatus(_dic_status, StatusDefinition.Suspended)
    End Select

    Return _dic_status

  End Function ' getListStatus

  ''' <summary>
  ''' Get credit lines
  ''' </summary>
  ''' <param name="DateFrom"></param>
  ''' <param name="Statuses"></param>
  ''' <param name="Account"></param>
  ''' <param name="WithDebt"></param>
  ''' <param name="WithoutDebt"></param>
  ''' <param name="AmountDebt"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function getCreditLines(ByVal DateFrom As uc_date_picker, ByVal Statuses As uc_checked_list, ByVal Account As uc_account_sel, WithDebt As Boolean, WithoutDebt As Boolean, AmountDebt As Double, ByVal SiteSel As uc_sites_sel) As String

    Dim _sql_str As New StringBuilder
    Dim _account_sql As String = ""

    _sql_str.AppendLine("     SELECT   CL_ACCOUNT_ID               ")
    _sql_str.AppendLine("            , ACC.AC_HOLDER_NAME          ")
    _sql_str.AppendLine("            , CL_LIMIT_AMOUNT             ")
    _sql_str.AppendLine("            , CL_TTO_AMOUNT               ")
    _sql_str.AppendLine("            , CL_SPENT_AMOUNT             ")
    _sql_str.AppendLine("            , CL_STATUS                   ")
    _sql_str.AppendLine("            , CL_CREATION                 ")
    _sql_str.AppendLine("            , CL_UPDATE                   ")
    _sql_str.AppendLine("            , CL_EXPIRED                  ")
    _sql_str.AppendLine("            , CL_LAST_PAYBACK             ")
    If (m_is_center) Then
      _sql_str.AppendLine("          , CL_SITE_ID                  ")
    Else
      _sql_str.AppendLine("      , NULL AS CL_SITE_ID              ")
    End If
    _sql_str.AppendLine("       FROM   CREDIT_LINES CRL            ")
    _sql_str.AppendLine(" INNER JOIN   ACCOUNTS ACC                ")
    _sql_str.AppendLine(" ON CRL.CL_ACCOUNT_ID = ACC.AC_ACCOUNT_ID ")

    _account_sql = Account.GetFilterSQL

    If m_is_center OrElse (DateFrom.Checked) OrElse _
      (Statuses.SelectedIndexes.Length <> 0 AndAlso Statuses.SelectedIndexes.Length <> Statuses.Count) OrElse _
      _account_sql <> "" OrElse WithDebt OrElse WithoutDebt Then
      _sql_str.AppendLine("WHERE")
      _sql_str.AppendLine(BuildWhere(DateFrom, Statuses, _account_sql, WithDebt, WithoutDebt, AmountDebt, SiteSel))
    End If

    _sql_str.AppendLine("ORDER BY CL_ACCOUNT_ID ASC                ")

    Return _sql_str.ToString

  End Function ' getCreditLines

#End Region ' Public Methods

#Region " Private Functions "

#Region " DB Actions "
  ''' <summary>
  ''' Update credit line
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function UpdateCreditLine() As Integer

    Dim _sb As StringBuilder
    Dim _sql_trx As System.Data.SqlClient.SqlTransaction = Nothing
    Dim _id_credit_line As Int64

    Try

      If Not GUI_BeginSQLTransaction(_sql_trx) Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

      _sb = New StringBuilder
      _sb.AppendLine(" UPDATE   CREDIT_LINES                       ")
      _sb.AppendLine("    SET   CL_LIMIT_AMOUNT = @pLimitAmount    ")
      _sb.AppendLine("        , CL_TTO_AMOUNT   = @pTtoAmount      ")
      _sb.AppendLine("        , CL_ISO_CODE     = @pIsoCode        ")
      _sb.AppendLine("        , CL_STATUS       = @pStatus         ")
      _sb.AppendLine("        , CL_IBAN         = @pIban           ")
      _sb.AppendLine("        , CL_XML_SIGNERS  = @pXmlSigners     ")
      _sb.AppendLine("        , CL_UPDATE       = @pUpdate         ")
      _sb.AppendLine("        , CL_EXPIRED      = @pExpired        ")
      _sb.AppendLine("  WHERE   CL_ACCOUNT_ID   = @pAccountId      ")

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), _sql_trx.Connection, _sql_trx)

        _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = m_credit_line.account_id
        _cmd.Parameters.Add("@pLimitAmount", SqlDbType.Decimal).Value = m_credit_line.limit_amount
        _cmd.Parameters.Add("@pTtoAmount", SqlDbType.Decimal).Value = m_credit_line.tto_amount
        _cmd.Parameters.Add("@pIsoCode", SqlDbType.VarChar).Value = m_credit_line.iso_code
        _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = m_credit_line.status
        _cmd.Parameters.Add("@pIban", SqlDbType.VarChar).Value = m_credit_line.iban
        _cmd.Parameters.Add("@pXmlSigners", SqlDbType.Xml).Value = m_credit_line.signers
        _cmd.Parameters.Add("@pUpdate", SqlDbType.DateTime).Value = WGDB.Now 'm_credit_line.update
        _cmd.Parameters.Add("@pExpired", SqlDbType.DateTime).Value = IIf(HasExpired, m_credit_line.expired, DBNull.Value)

        If _cmd.ExecuteNonQuery() <> 1 Then
          GUI_EndSQLTransaction(_sql_trx, False)

          Return ENUM_STATUS.STATUS_ERROR
        End If

        _sb = New StringBuilder

        _sb.AppendLine(" SELECT   CL_ID                       ")
        _sb.AppendLine("   FROM   CREDIT_LINES                ")
        _sb.AppendLine("  WHERE   CL_ACCOUNT_ID = @pAccountId ")

        _cmd.CommandText = _sb.ToString()

        _id_credit_line = _cmd.ExecuteScalar()

        If Not CreditLinesBusinessLogic.CreateMovements(GetMovementUpdate(_id_credit_line), _sql_trx) Then
          Return ENUM_STATUS.STATUS_ERROR
        End If

        If Not GUI_EndSQLTransaction(_sql_trx, True) Then
          Return ENUM_STATUS.STATUS_ERROR
        End If

      End Using

    Catch ex As Exception
      '  ' Rollback  
      GUI_EndSQLTransaction(_sql_trx, False)

      Return ENUM_STATUS.STATUS_ERROR
    End Try

    Return ENUM_STATUS.STATUS_OK

  End Function ' UpdateCreditLine

  ''' <summary>
  ''' Insert credit line
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function InsertCreditLine() As Integer

    Dim _sb As StringBuilder
    Dim _exist_account As Boolean
    Dim _sql_trx As System.Data.SqlClient.SqlTransaction = Nothing
    Dim _para_id_credit_line As SqlParameter

    Try

      If Not GUI_BeginSQLTransaction(_sql_trx) Then

        Return ENUM_STATUS.STATUS_ERROR
      End If

      _sb = New StringBuilder
      _sb.AppendLine(" SELECT   COUNT(1)                    ")
      _sb.AppendLine("   FROM   CREDIT_LINES                ")
      _sb.AppendLine("  WHERE   CL_ACCOUNT_ID = @pAccountId ")

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), _sql_trx.Connection, _sql_trx)

        _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = m_credit_line.account_id
        _exist_account = (_cmd.ExecuteScalar() > 0)

        If _exist_account Then
          Return ENUM_STATUS.STATUS_DUPLICATE_KEY
        End If

        _sb = New StringBuilder
        _sb.AppendLine(" INSERT INTO   CREDIT_LINES            ")
        _sb.AppendLine("             ( CL_ACCOUNT_ID           ")
        _sb.AppendLine("             , CL_LIMIT_AMOUNT         ")
        _sb.AppendLine("             , CL_TTO_AMOUNT           ")
        _sb.AppendLine("             , CL_SPENT_AMOUNT         ")
        _sb.AppendLine("             , CL_ISO_CODE             ")
        _sb.AppendLine("             , CL_STATUS               ")
        _sb.AppendLine("             , CL_IBAN                 ")
        _sb.AppendLine("             , CL_XML_SIGNERS          ")
        _sb.AppendLine("             , CL_CREATION             ")
        _sb.AppendLine("             , CL_UPDATE               ")
        _sb.AppendLine("             , CL_EXPIRED              ")
        _sb.AppendLine("             , CL_LAST_PAYBACK)        ")
        _sb.AppendLine("      VALUES                           ")
        _sb.AppendLine("             ( @pAccountId             ")
        _sb.AppendLine("             , @pLimitAmount           ")
        _sb.AppendLine("             , @pTtoAmount             ")
        _sb.AppendLine("             , @pSpentAmount           ")
        _sb.AppendLine("             , @pIsoCode               ")
        _sb.AppendLine("             , @pStatus                ")
        _sb.AppendLine("             , @pIban                  ")
        _sb.AppendLine("             , @pXmlSigners            ")
        _sb.AppendLine("             , @pCreation              ")
        _sb.AppendLine("             , @pUpdate                ")
        _sb.AppendLine("             , @pExpired               ")
        _sb.AppendLine("             , @pLastPayback)          ")
        _sb.AppendLine("SET @pID = SCOPE_IDENTITY();           ")

        _cmd.CommandText = _sb.ToString()

        _cmd.Parameters.Add("@pLimitAmount", SqlDbType.Decimal).Value = m_credit_line.limit_amount
        _cmd.Parameters.Add("@pTtoAmount", SqlDbType.Decimal).Value = m_credit_line.tto_amount
        _cmd.Parameters.Add("@pSpentAmount", SqlDbType.Decimal).Value = m_credit_line.spent_amount
        _cmd.Parameters.Add("@pIsoCode", SqlDbType.VarChar).Value = m_credit_line.iso_code
        _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = m_credit_line.status
        _cmd.Parameters.Add("@pIban", SqlDbType.VarChar).Value = m_credit_line.iban
        _cmd.Parameters.Add("@pXmlSigners", SqlDbType.Xml).Value = m_credit_line.signers
        _cmd.Parameters.Add("@pCreation", SqlDbType.DateTime).Value = WGDB.Now  'm_credit_line.creation
        _cmd.Parameters.Add("@pUpdate", SqlDbType.DateTime).Value = WGDB.Now 'm_credit_line.update
        _cmd.Parameters.Add("@pExpired", SqlDbType.DateTime).Value = IIf(HasExpired, m_credit_line.expired, DBNull.Value)
        _cmd.Parameters.Add("@pLastPayback", SqlDbType.DateTime).Value = DBNull.Value 'm_credit_line.last_payback

        _para_id_credit_line = _cmd.Parameters.Add("@pID", SqlDbType.BigInt)
        _para_id_credit_line.Direction = ParameterDirection.Output

        If _cmd.ExecuteNonQuery() <> 1 Then
          GUI_EndSQLTransaction(_sql_trx, False)

          Return ENUM_STATUS.STATUS_ERROR
        End If

        If Not CreditLinesBusinessLogic.CreateMovements(GetMovementCreate(_para_id_credit_line.Value), _sql_trx) Then
          Return ENUM_STATUS.STATUS_ERROR
        End If

        If Not GUI_EndSQLTransaction(_sql_trx, True) Then
          Return ENUM_STATUS.STATUS_ERROR
        End If

      End Using

    Catch ex As Exception
      '  ' Rollback  
      GUI_EndSQLTransaction(_sql_trx, False)

      Return ENUM_STATUS.STATUS_ERROR
    End Try

    Return ENUM_STATUS.STATUS_OK

  End Function ' InsertCreditLine

  ''' <summary>
  ''' Get credit line
  ''' </summary>
  ''' <param name="ObjectId"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function GetCreditLine(ObjectId As cls_account) As Integer

    Dim _sb As StringBuilder

    Try

      _sb = New StringBuilder
      _sb.AppendLine(" SELECT   CL_ID                       ")
      _sb.AppendLine("        , CL_ACCOUNT_ID               ")
      _sb.AppendLine("        , CL_LIMIT_AMOUNT             ")
      _sb.AppendLine("        , CL_TTO_AMOUNT               ")
      _sb.AppendLine("        , CL_SPENT_AMOUNT             ")
      _sb.AppendLine("        , CL_ISO_CODE                 ")
      _sb.AppendLine("        , CL_STATUS                   ")
      _sb.AppendLine("        , CL_IBAN                     ")
      _sb.AppendLine("        , CL_XML_SIGNERS              ")
      _sb.AppendLine("        , CL_CREATION                 ")
      _sb.AppendLine("        , CL_UPDATE                   ")
      _sb.AppendLine("        , CL_EXPIRED                  ")
      _sb.AppendLine("        , CL_LAST_PAYBACK             ")
      _sb.AppendLine("   FROM   CREDIT_LINES                ")
      _sb.AppendLine("  WHERE   CL_ACCOUNT_ID = @pAccountId ")

      Using _db_trx As New WSI.Common.DB_TRX()
        Using _sql_cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = ObjectId.id_account

          Using _reader As SqlDataReader = _sql_cmd.ExecuteReader()
            If (Not _reader.Read) Then

              m_credit_line.account_id = ObjectId.id_account
              m_credit_line.holder_name = ObjectId.holder_name

              Return ENUM_STATUS.STATUS_NOT_FOUND
            Else

              m_credit_line.account_id = _reader("CL_ACCOUNT_ID")
              m_credit_line.holder_name = ObjectId.holder_name
              m_credit_line.limit_amount = _reader("CL_LIMIT_AMOUNT")
              m_credit_line.tto_amount = _reader("CL_TTO_AMOUNT")
              m_credit_line.spent_amount = _reader("CL_SPENT_AMOUNT")
              m_credit_line.iso_code = _reader("CL_ISO_CODE")
              m_credit_line.status = _reader("CL_STATUS")
              m_credit_line.iban = _reader("CL_IBAN")
              m_credit_line.signers = _reader("CL_XML_SIGNERS")
              m_credit_line.creation = _reader("CL_CREATION")
              m_credit_line.update = _reader("CL_UPDATE")

              If Not IsDBNull(_reader("CL_EXPIRED")) Then
                m_credit_line.expired = _reader("CL_EXPIRED")
                m_credit_line.has_expired_date = True
              Else
                m_credit_line.expired = GUI_GetDate().AddDays(1).AddHours(GetDefaultClosingTime())
                m_credit_line.has_expired_date = False
              End If

              If Not IsDBNull(_reader("CL_LAST_PAYBACK")) Then
                m_credit_line.last_payback = _reader("CL_LAST_PAYBACK")
              End If

            End If
          End Using
        End Using
      End Using

    Catch _ex As Exception

      Return ENUM_STATUS.STATUS_ERROR
    End Try

    Return ENUM_STATUS.STATUS_OK

  End Function ' GetCreditLine

#End Region ' DB Actions

#Region " Manage xml "

  ''' <summary>
  ''' Get Movement from create credit line
  ''' </summary>
  ''' <param name="operationID"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetMovementCreate(ByVal OperationId As Int64) As CreditLineActions

    Dim _credit_line_actions As New CreditLineActions

    Try

      _credit_line_actions.Actions = New List(Of CreditLineAction)

      _credit_line_actions.Action = CreditLinesBusinessLogic.ActionType.CreateCreditLine
      _credit_line_actions.CreditLineId = OperationId
      _credit_line_actions.AccountId = m_credit_line.account_id
      _credit_line_actions.AddSubAmount = m_credit_line.limit_amount

      SetCreditLineAction(_credit_line_actions, GLB_CurrentUser.Name, CreditLineMovements.CreditLineMovementType.Create, String.Empty, GetXMLChangeCreditLine(CreditLineMovements.CreditLineMovementType.Create, True))

      Return _credit_line_actions

    Catch _ex As Exception
      Log.Error(_ex.Message())
    End Try

    Return Nothing

  End Function ' GetMovementCreate

  ''' <summary>
  ''' Get movement from update credit line
  ''' </summary>
  ''' <param name="operationID"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetMovementUpdate(ByVal OperationId As Int64) As CreditLineActions

    Dim _credit_line_actions As CreditLineActions
    Dim _is_update As Boolean

    Try

      _credit_line_actions = New CreditLineActions
      _credit_line_actions.Actions = New List(Of CreditLineAction)


      _credit_line_actions.CreditLineId = OperationId
      _credit_line_actions.AccountId = m_credit_line.account_id
      _credit_line_actions.AddSubAmount = m_credit_line.limit_amount

      If CreditLineRead.LimitAmount <> LimitAmount Then
        SetCreditLineAction(_credit_line_actions, GLB_CurrentUser.Name, CreditLineMovements.CreditLineMovementType.UpdateLimit, _
                            GetXMLChangeCreditLine(CreditLineMovements.CreditLineMovementType.UpdateLimit, False), _
                            GetXMLChangeCreditLine(CreditLineMovements.CreditLineMovementType.UpdateLimit, True))
        _is_update = True
      End If

      If CreditLineRead.Expired <> Expired Or CreditLineRead.HasExpired <> HasExpired Then
        SetCreditLineAction(_credit_line_actions, GLB_CurrentUser.Name, CreditLineMovements.CreditLineMovementType.UpdateExpiration, _
                            GetXMLChangeCreditLine(CreditLineMovements.CreditLineMovementType.UpdateExpiration, False), _
                            GetXMLChangeCreditLine(CreditLineMovements.CreditLineMovementType.UpdateExpiration, True))
        _is_update = True
      End If

      If CreditLineRead.Iban <> Iban Then
        SetCreditLineAction(_credit_line_actions, GLB_CurrentUser.Name, CreditLineMovements.CreditLineMovementType.UpdateIBAN, _
                            GetXMLChangeCreditLine(CreditLineMovements.CreditLineMovementType.UpdateIBAN, False), _
                            GetXMLChangeCreditLine(CreditLineMovements.CreditLineMovementType.UpdateIBAN, True))
        _is_update = True
      End If

      If CreditLineRead.Signers <> Signers Then
        SetCreditLineAction(_credit_line_actions, GLB_CurrentUser.Name, CreditLineMovements.CreditLineMovementType.UpdateSigners, _
                            GetXMLChangeCreditLine(CreditLineMovements.CreditLineMovementType.UpdateSigners, False), _
                            GetXMLChangeCreditLine(CreditLineMovements.CreditLineMovementType.UpdateSigners, True))
        _is_update = True
      End If

      If CreditLineRead.Status <> Status Then
        SetCreditLineAction(_credit_line_actions, GLB_CurrentUser.Name, CreditLineMovements.CreditLineMovementType.UpdateStatus, _
                            GetXMLChangeCreditLine(CreditLineMovements.CreditLineMovementType.UpdateStatus, False), _
                            GetXMLChangeCreditLine(CreditLineMovements.CreditLineMovementType.UpdateStatus, True))
        _is_update = True
      End If

      If CreditLineRead.TTOAmount <> TTOAmount Then
        SetCreditLineAction(_credit_line_actions, GLB_CurrentUser.Name, CreditLineMovements.CreditLineMovementType.UpdateTTO, _
                            GetXMLChangeCreditLine(CreditLineMovements.CreditLineMovementType.UpdateTTO, False), _
                            GetXMLChangeCreditLine(CreditLineMovements.CreditLineMovementType.UpdateTTO, True))
        _is_update = True
      End If

      If _is_update Then
        _credit_line_actions.Action = CreditLinesBusinessLogic.ActionType.UpdateCreditLine
      Else

        Select Case Status
          Case CreditLines.StatusDefinition.Approved
            _credit_line_actions.Action = CreditLinesBusinessLogic.ActionType.ApprovedCreditLine
          Case CreditLines.StatusDefinition.Expired
            _credit_line_actions.Action = CreditLinesBusinessLogic.ActionType.ExpiredCreditLine
          Case CreditLines.StatusDefinition.NotApproved
            _credit_line_actions.Action = CreditLinesBusinessLogic.ActionType.NotApprovedCreditLine
          Case CreditLines.StatusDefinition.Suspended
            _credit_line_actions.Action = CreditLinesBusinessLogic.ActionType.SuspendedCreditLine
        End Select

      End If

      Return _credit_line_actions

    Catch _ex As Exception
      Log.Error(_ex.Message())
    End Try

    Return Nothing

  End Function ' GetMovementUpdate

  ''' <summary>
  ''' Get XML movement from change credit line
  ''' </summary>
  ''' <param name="typeMovement"></param>
  ''' <param name="newValue"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetXMLChangeCreditLine(ByVal TypeMovement As CreditLineMovements.CreditLineMovementType, ByVal NewValue As Boolean) As String

    Dim _xml_doc As XmlDocument
    Dim _root_node As XmlElement

    Try

      _xml_doc = New XmlDocument

      _root_node = _xml_doc.CreateElement(MovementsNodeXML.creditLine.ToString())
      _xml_doc.AppendChild(_root_node)

      Select Case TypeMovement

        Case CreditLineMovements.CreditLineMovementType.Create
          AddChildNode(_xml_doc, MovementsNodeXML.creditLimit.ToString(), GUI_ParseCurrency(Me.LimitAmount))
          AddChildNode(_xml_doc, MovementsNodeXML.expirationDate.ToString(), IIf(HasExpired, GUI_FormatDate(Me.Expired, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM), ""))
          AddChildNode(_xml_doc, MovementsNodeXML.TTO.ToString(), GUI_ParseCurrency(Me.TTOAmount))
          AddChildNode(_xml_doc, MovementsNodeXML.createDate.ToString(), GUI_FormatDate(Me.Creation, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM))
          AddChildNode(_xml_doc, MovementsNodeXML.spent.ToString(), GUI_ParseCurrency(Me.SpentAmount))
          AddChildNode(_xml_doc, MovementsNodeXML.status.ToString(), Me.Status)
          AddChildNode(_xml_doc, MovementsNodeXML.iban.ToString(), Me.Iban)
          AddChilSigners(_xml_doc, NewValue)

        Case CreditLineMovements.CreditLineMovementType.UpdateLimit
          AddChildNode(_xml_doc, MovementsNodeXML.creditLimit.ToString(), IIf(NewValue, GUI_ParseCurrency(Me.LimitAmount), GUI_ParseCurrency(CreditLineRead.LimitAmount)))

        Case CreditLineMovements.CreditLineMovementType.UpdateExpiration

          If ((NewValue AndAlso HasExpired) OrElse (Not NewValue AndAlso CreditLineRead.HasExpired)) Then
            AddChildNode(_xml_doc, MovementsNodeXML.expirationDate.ToString(), IIf(NewValue, _
                                                         GUI_FormatDate(Me.Expired, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM), _
                                                         GUI_FormatDate(CreditLineRead.Expired, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)))
          Else
            AddChildNode(_xml_doc, MovementsNodeXML.expirationDate.ToString(), String.Empty)
          End If

        Case CreditLineMovements.CreditLineMovementType.UpdateIBAN
          AddChildNode(_xml_doc, MovementsNodeXML.iban.ToString(), IIf(NewValue, Me.Iban, CreditLineRead.Iban))

        Case CreditLineMovements.CreditLineMovementType.UpdateSigners
          AddChilSigners(_xml_doc, NewValue)

        Case CreditLineMovements.CreditLineMovementType.UpdateTTO
          AddChildNode(_xml_doc, MovementsNodeXML.TTO.ToString(), IIf(NewValue, GUI_ParseCurrency(Me.TTOAmount), GUI_ParseCurrency(CreditLineRead.TTOAmount)))

        Case CreditLineMovements.CreditLineMovementType.UpdateStatus
          AddChildNode(_xml_doc, MovementsNodeXML.status.ToString(), IIf(NewValue, Me.Status, CreditLineRead.Status))

      End Select

      Return _xml_doc.OuterXml

    Catch _ex As Exception
      Log.Error(_ex.Message())
    End Try

    Return String.Empty

  End Function ' GetXMLChangeCreditLine

  ''' <summary>
  ''' Add Child in XML signers
  ''' </summary>
  ''' <param name="xml"></param>
  ''' <param name="newValue"></param>
  ''' <remarks></remarks>
  Private Sub AddChilSigners(ByRef Xml As XmlDocument, ByVal NewValue As Boolean)

    Dim _xml_signers As XmlDataDocument

    Try

      _xml_signers = New XmlDataDocument

      If NewValue Then
        _xml_signers.LoadXml(Me.Signers)
      Else
        _xml_signers.LoadXml(Me.CreditLineRead.Signers)
      End If

      Xml.DocumentElement.AppendChild(Xml.ImportNode(_xml_signers.FirstChild, True))

    Catch _ex As Exception
      Log.Error(_ex.Message())
    End Try

  End Sub ' AddChilSigners

  ''' <summary>
  ''' Add chil in XML from XML movement credit line
  ''' </summary>
  ''' <param name="xml"></param>
  ''' <param name="nodeName"></param>
  ''' <param name="value"></param>
  ''' <remarks></remarks>
  Private Sub AddChildNode(ByRef xml As XmlDocument, ByVal NodeName As String, ByVal Value As String)

    Dim _root_node As XmlElement
    Dim _element As XmlElement

    Try

      _root_node = xml.SelectSingleNode("//creditLine")
      _element = xml.CreateElement(NodeName)
      _root_node.AppendChild(_element)
      _element.AppendChild(xml.CreateTextNode(Value))

    Catch _ex As Exception
      Log.Error(_ex.Message())
    End Try

  End Sub ' AddChildNode

#End Region ' Manage xml

  ''' <summary>
  ''' Add status to dictionary
  ''' </summary>
  ''' <param name="DicStatus"></param>
  ''' <param name="Status"></param>
  ''' <remarks></remarks>
  Private Sub SetStatus(ByRef DicStatus As Dictionary(Of Integer, String), ByVal Status As StatusDefinition)
    DicStatus.Add(Status, CreditLine.StatusToString(Status))
  End Sub ' SetStatus

  ''' <summary>
  ''' Add and set credit line action
  ''' </summary>
  ''' <param name="CreditLineActions"></param>
  ''' <param name="CreationUser"></param>
  ''' <param name="MovementType"></param>
  ''' <param name="XmlOldValue"></param>
  ''' <param name="XmlNewValue"></param>
  ''' <remarks></remarks>
  Private Sub SetCreditLineAction(ByRef CreditLineActions As CreditLineActions, ByVal CreationUser As String, _
                                  ByVal MovementType As CreditLineMovements.CreditLineMovementType, ByVal XmlOldValue As String, _
                                  ByVal XmlNewValue As String)

    Dim _credit_line_action As CreditLineAction

    _credit_line_action = New CreditLineAction
    _credit_line_action.CreationUser = CreationUser
    _credit_line_action.MovementType = MovementType
    _credit_line_action.OldValue = XmlOldValue
    _credit_line_action.NewValue = XmlNewValue

    CreditLineActions.Actions.Add(_credit_line_action)

  End Sub ' SetCreditLineAction

  Private Function BuildWhere(DateFrom As uc_date_picker, Statuses As uc_checked_list, AccountSql As String, WithDebt As Boolean, WithoutDebt As Boolean, AmountDebt As Double, SitesSel As uc_sites_sel) As String

    Dim _where As StringBuilder
    Dim _index As Integer
    Dim _clause_added As Boolean

    _where = New StringBuilder
    _index = 0
    _clause_added = False

    If DateFrom.Checked Then
      _where.AppendLine("CL_EXPIRED >= " & GUI_FormatDateDB(DateFrom.Value))
      _clause_added = True
    End If

    If (Statuses.SelectedIndexes.Length <> 0 And Statuses.SelectedIndexes.Length <> Statuses.Count) Then
      If _clause_added Then
        _where.AppendLine("AND")
      End If
      _where.AppendLine("CL_STATUS IN (" & Statuses.SelectedIndexesList & ")")
      _clause_added = True
    End If

    If (AccountSql <> "") Then
      If _clause_added Then
        _where.AppendLine("AND")
      End If
      _where.AppendLine(AccountSql)
      _clause_added = True
    End If

    If WithDebt And Not WithoutDebt Then
      If _clause_added Then
        _where.AppendLine("AND")
      End If
      _where.AppendLine(String.Format("CL_SPENT_AMOUNT > {0}", AmountDebt))
      _clause_added = True
    ElseIf WithoutDebt And Not WithDebt Then
      If _clause_added Then
        _where.AppendLine("AND")
      End If
      _where.AppendLine("CL_SPENT_AMOUNT = 0")
      _clause_added = True
    Else
      If _clause_added Then
        _where.AppendLine("AND")
      End If
      _where.AppendLine(String.Format("(CL_SPENT_AMOUNT > {0} OR CL_SPENT_AMOUNT = 0)", AmountDebt))
      _clause_added = True
    End If

    If (m_is_center AndAlso Not String.IsNullOrEmpty(SitesSel.GetSitesIdListSelectedAll())) Then
      If _clause_added Then
        _where.AppendLine("AND")
      End If
      _where.AppendLine("CL_SITE_ID IN (" & SitesSel.GetSitesIdListSelectedAll() & ")")
      _clause_added = True
    End If

    Return _where.ToString

  End Function ' BuildWhere

#End Region ' Private Functions

End Class

Public Class cls_account
  Public Property id_account() As String
  Public Property holder_name() As String
End Class