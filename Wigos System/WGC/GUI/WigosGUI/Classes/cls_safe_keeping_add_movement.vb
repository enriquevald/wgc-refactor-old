'-------------------------------------------------------------------
' Copyright � 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_safe_keeping_add_movemen.vb
'
' DESCRIPTION : safe keeping movement class for data reading
'              
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 15-APR-2015  DRV    Initial version
'-------------------------------------------------------------------
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.text

Public Class CLASS_SAFE_KEEPING_ADD_MOVEMENT
  Inherits CLASS_BASE

#Region " Members "
  Private m_holder_id As Int64
  Private m_movements As DataTable
  Private m_holder_name As String
  Private m_balance As Currency
  Private m_max_balance As Currency
  Private m_blocked As Boolean
#End Region

#Region " Properties "
  Public Property HolderId() As Int64
    Get
      Return m_holder_id
    End Get
    Set(ByVal value As Int64)
      m_holder_id = value
    End Set
  End Property

  Public Property Movements() As DataTable
    Get
      Return m_movements
    End Get
    Set(ByVal value As DataTable)
      m_movements = value
    End Set
  End Property

  Public Property HolderName() As String
    Get
      Return m_holder_name
    End Get
    Set(ByVal value As String)
      m_holder_name = value
    End Set
  End Property

  Public Property Balance() As Currency
    Get
      Return m_balance
    End Get
    Set(ByVal value As Currency)
      m_balance = value
    End Set
  End Property

  Public Property MaxBalance() As Currency
    Get
      Return m_max_balance
    End Get
    Set(ByVal value As Currency)
      m_max_balance = value
    End Set
  End Property

  Public Property Blocked() As Boolean
    Get
      Return m_blocked
    End Get
    Set(ByVal value As Boolean)
      m_blocked = value
    End Set
  End Property


#End Region

#Region " Overrides functions "

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads from the database into the given object
  '
  ' PARAMS :
  '     - INPUT :
  '         - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '
  ' NOTES :

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    Me.m_holder_id = ObjectId

    ' Read data
    _rc = ReadHolderData(SqlCtx)

    Select Case _rc

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Read

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    ' Update gift data
    '_rc = Updategift(m_gift, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Update

  '----------------------------------------------------------------------------
  ' PURPOSE : Inserts the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Return ENUM_STATUS.STATUS_ERROR

  End Function ' DB_Insert

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes the given object from the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DEPENDENCIES
  '
  ' NOTES :

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Return ENUM_STATUS.STATUS_ERROR

  End Function ' DB_Delete

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns a duplicate of the given object.
  '
  ' PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created object
  '
  ' NOTES :

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim temp_movement As CLASS_SAFE_KEEPING_ADD_MOVEMENT

    temp_movement = New CLASS_SAFE_KEEPING_ADD_MOVEMENT
    temp_movement.Blocked = Me.Blocked
    temp_movement.Balance = Me.Balance
    temp_movement.HolderId = Me.HolderId
    temp_movement.HolderName = Me.HolderName
    temp_movement.MaxBalance = Me.MaxBalance
    temp_movement.Movements = Me.Movements.Copy()

    Return temp_movement

  End Function ' Duplicate

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns the related auditor data for the current object.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created audit object
  '
  ' NOTES :

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_CAGE)

    Return _auditor_data

  End Function ' AuditorData

#End Region

#Region "Private Functions"
  '----------------------------------------------------------------------------
  ' PURPOSE : Reads holder data and las 10 account safe keeping movements.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created audit object
  '
  ' NOTES :
  Private Function ReadHolderData(ByVal SqlCtx As Int64) As ENUM_CONFIGURATION_RC
    Dim _sql_query As StringBuilder
    Dim _ds_data As DataSet
    Dim _sql_tx As System.Data.SqlClient.SqlTransaction = Nothing

    _sql_query = New StringBuilder()
    _sql_query.AppendLine(" SELECT   SKA_OWNER_NAME ")
    _sql_query.AppendLine("        , SKA_BALANCE ")
    _sql_query.AppendLine("        , SKA_MAX_BALANCE ")
    _sql_query.AppendLine("        , SKA_BLOCK_REASON ")
    _sql_query.AppendLine("   FROM   SAFE_KEEPING_ACCOUNTS ")
    _sql_query.AppendLine("  WHERE   SKA_SAFE_KEEPING_ID = @pSafeKeepingAccountId ")

    _sql_query.AppendLine("   SELECT   TOP 11 SKO_DATETIME ")
    _sql_query.AppendLine("          , SKO_AMOUNT ")
    _sql_query.AppendLine("          , SKO_FINAL_BALANCE ")
    _sql_query.AppendLine("          , SKO_TYPE ")
    _sql_query.AppendLine("     FROM   SAFE_KEEPING_OPERATIONS")
    _sql_query.AppendLine("    WHERE   SKO_SAFE_KEEPING_ID = @pSafeKeepingAccountId ")
    _sql_query.AppendLine(" ORDER BY   SKO_DATETIME DESC ")

    Try
      Using _db_trx As New DB_TRX
        Using _sql_cmd As New SqlCommand(_sql_query.ToString, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          Using _da_data As New SqlClient.SqlDataAdapter(_sql_cmd)
            _ds_data = New DataSet()
            _sql_cmd.Parameters.Add("@pSafeKeepingAccountId", SqlDbType.BigInt).Value = Me.m_holder_id
            _db_trx.Fill(_da_data, _ds_data)
          End Using
        End Using
      End Using

      If IsNothing(_ds_data.Tables(0)) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
      End If

      If _ds_data.Tables(0).Rows.Count() <> 1 Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      If Not IsDBNull(_ds_data.Tables(0).Rows(0).Item("SKA_OWNER_NAME")) Then
        m_holder_name = _ds_data.Tables(0).Rows(0).Item("SKA_OWNER_NAME")
      Else
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR
      End If

      If Not IsDBNull(_ds_data.Tables(0).Rows(0).Item("SKA_BALANCE")) Then
        m_balance = CType(_ds_data.Tables(0).Rows(0).Item("SKA_BALANCE").ToString(), Currency)
      Else
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR
      End If

      If Not IsDBNull(_ds_data.Tables(0).Rows(0).Item("SKA_MAX_BALANCE")) Then
        m_max_balance = CType(_ds_data.Tables(0).Rows(0).Item("SKA_MAX_BALANCE").ToString(), Currency)
      Else
        m_max_balance = -1
      End If

      If Not IsDBNull(_ds_data.Tables(0).Rows(0).Item("SKA_BLOCK_REASON")) Then
        m_blocked = CType(_ds_data.Tables(0).Rows(0).Item("SKA_BLOCK_REASON"), Boolean)
      End If

      If Not IsNothing(_ds_data.Tables(1)) Then
        m_movements = _ds_data.Tables(1)
      End If

    Catch _ex As Exception
      Log.Exception(_ex)
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function

#End Region ' Private Functions
End Class
