'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_gaming_table_session.vb
'
' DESCRIPTION : gaming table session class
'              
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 12-JAN-2015  DHA    Initial version
' 11-JUL-2016  JML    Product Backlog Item 15065: Gaming tables (Fase 4): Player tracking. Multicurrency
' 26-OCT-2017  RAB    Bug 30435:WIGOS-6138 Gaming tables point assignment: player session manual entry allows point assignment to anonymous user
' 26-OCT-2017  RAB    Bug 30440:WIGOS-6154 Gaming tables point assignment: player session manual entry point assignment is not calculated like buckets
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common

Public Class CLASS_GAMING_TABLE_SESSION
  Inherits CLASS_BASE

#Region "Structures"

  <StructLayout(LayoutKind.Sequential)> _
Public Class TYPE_GAMING_TABLE_SESSION
    Public gt_table_id As Integer
    Public gt_table_name As String
    Public gt_table_session_name As String
    Public gt_table_session_id As Int64
    Public gt_is_unknown As Boolean
    Public gt_terminal_id As Int32
    Public gt_terminal_name As String
    Public gt_account_id As Int64
    Public gt_start_session As DateTime
    Public gt_end_session As DateTime
    Public gt_game_time As Int64
    Public gt_walk_time As Int64
    Public gt_speed As Integer
    Public gt_skill As Integer
    Public gt_played_amount As Decimal
    Public gt_current_bet As Decimal
    Public gt_plays As Integer
    Public gt_min_bet As Decimal
    Public gt_max_bet As Decimal
    Public gt_chips_in As Decimal
    Public gt_chips_out As Decimal
    Public gt_buy_in As Decimal
    Public gt_points As Decimal
    Public gt_computed_points As Decimal
    Public gt_account_name As String
    Public gt_iso_code As String
  End Class

#End Region ' Structures

#Region "Constants "

  Public Const DT_COLUMN_TYPE_ID As Integer = 0
  Public Const DT_COLUMN_TYPE_NAME As Integer = 1
  Public Const DT_COLUMN_TABLE_ID As Integer = 2
  Public Const DT_COLUMN_TABLE_NAME As Integer = 3
  Public Const DT_COLUMN_CASHIER_SESSION_ID As Integer = 4
  Public Const DT_COLUMN_GT_SESSION_ID As Integer = 5
  Public Const DT_COLUMN_CASHIER_SESSION_NAME As Integer = 6
  Public Const DT_COLUMN_CASHIER_SESSION_OPENING As Integer = 7
  Public Const DT_COLUMN_CASHIER_SESSION_CLOSING As Integer = 8
  Public Const DT_COLUMN_CASHIER_SESSION_STATUS As Integer = 9
  Public Const DT_COLUMN_VIRTUAL_TERMINAL As Integer = 10
  Public Const DT_COLUMN_VIRTUAL_ACCOUNT As Integer = 11
  Public Const DT_COLUMN_VIRTUAL_TERMINAL_NAME As Integer = 12
  Public Const DT_COLUMN_THEORICAL_HOLD As Integer = 13

#End Region ' Constants

#Region "Members"

  Protected m_gt_session As New TYPE_GAMING_TABLE_SESSION

#End Region ' Members

#Region "Properties"

  Public Property TableId() As Integer
    Get
      Return m_gt_session.gt_table_id
    End Get
    Set(ByVal Value As Integer)
      m_gt_session.gt_table_id = Value
    End Set
  End Property

  Public Property TableName() As String
    Get
      Return m_gt_session.gt_table_name
    End Get
    Set(ByVal Value As String)
      m_gt_session.gt_table_name = Value
    End Set
  End Property

  Public Property TableSessionName() As String
    Get
      Return m_gt_session.gt_table_session_name
    End Get
    Set(ByVal Value As String)
      m_gt_session.gt_table_session_name = Value
    End Set
  End Property

  Public Property TableSessionId() As Int64
    Get
      Return m_gt_session.gt_table_session_id
    End Get
    Set(ByVal Value As Int64)
      m_gt_session.gt_table_session_id = Value
    End Set
  End Property

  Public Property IsUnknown() As Boolean
    Get
      Return m_gt_session.gt_is_unknown
    End Get
    Set(ByVal Value As Boolean)
      m_gt_session.gt_is_unknown = Value
    End Set
  End Property

  Public Property TerminalId() As Int32
    Get
      Return m_gt_session.gt_terminal_id
    End Get
    Set(ByVal Value As Int32)
      m_gt_session.gt_terminal_id = Value
    End Set
  End Property

  Public Property TerminalName() As String
    Get
      Return m_gt_session.gt_terminal_name
    End Get
    Set(ByVal Value As String)
      m_gt_session.gt_terminal_name = Value
    End Set
  End Property

  Public Property AccountId() As Int64
    Get
      Return m_gt_session.gt_account_id
    End Get
    Set(ByVal Value As Int64)
      m_gt_session.gt_account_id = Value
    End Set
  End Property

  Public Property AccountName() As String
    Get
      Return m_gt_session.gt_account_name
    End Get
    Set(ByVal Value As String)
      m_gt_session.gt_account_name = Value
    End Set
  End Property

  Public Property StartSession() As DateTime
    Get
      Return m_gt_session.gt_start_session
    End Get
    Set(ByVal Value As DateTime)
      m_gt_session.gt_start_session = Value
    End Set
  End Property

  Public Property EndSession() As DateTime
    Get
      Return m_gt_session.gt_end_session
    End Get
    Set(ByVal Value As DateTime)
      m_gt_session.gt_end_session = Value
    End Set
  End Property

  Public Property GameTime() As Int64
    Get
      Return m_gt_session.gt_game_time
    End Get
    Set(ByVal Value As Int64)
      m_gt_session.gt_game_time = Value
    End Set
  End Property

  Public Property WalkTime() As Int64
    Get
      Return m_gt_session.gt_walk_time
    End Get
    Set(ByVal Value As Int64)
      m_gt_session.gt_walk_time = Value
    End Set
  End Property

  Public Property Speed() As Integer
    Get
      Return m_gt_session.gt_speed
    End Get
    Set(ByVal Value As Integer)
      m_gt_session.gt_speed = Value
    End Set
  End Property

  Public Property Skill() As Integer
    Get
      Return m_gt_session.gt_skill
    End Get
    Set(ByVal Value As Integer)
      m_gt_session.gt_skill = Value
    End Set
  End Property

  Public Property PlayedAmount() As Decimal
    Get
      Return m_gt_session.gt_played_amount
    End Get
    Set(ByVal Value As Decimal)
      m_gt_session.gt_played_amount = Value
    End Set
  End Property

  Public Property CurrentBet() As Decimal
    Get
      Return m_gt_session.gt_current_bet
    End Get
    Set(ByVal Value As Decimal)
      m_gt_session.gt_current_bet = Value
    End Set
  End Property

  Public Property Plays() As Integer
    Get
      Return m_gt_session.gt_plays
    End Get
    Set(ByVal Value As Integer)
      m_gt_session.gt_plays = Value
    End Set
  End Property

  Public Property MinBet() As Decimal
    Get
      Return m_gt_session.gt_min_bet
    End Get
    Set(ByVal Value As Decimal)
      m_gt_session.gt_min_bet = Value
    End Set
  End Property

  Public Property MaxBet() As Decimal
    Get
      Return m_gt_session.gt_max_bet
    End Get
    Set(ByVal Value As Decimal)
      m_gt_session.gt_max_bet = Value
    End Set
  End Property

  Public Property ChipsIn() As Decimal
    Get
      Return m_gt_session.gt_chips_in
    End Get
    Set(ByVal Value As Decimal)
      m_gt_session.gt_chips_in = Value
    End Set
  End Property

  Public Property ChipsOut() As Decimal
    Get
      Return m_gt_session.gt_chips_out
    End Get
    Set(ByVal Value As Decimal)
      m_gt_session.gt_chips_out = Value
    End Set
  End Property

  Public Property BuyIn() As Decimal
    Get
      Return m_gt_session.gt_buy_in
    End Get
    Set(ByVal Value As Decimal)
      m_gt_session.gt_buy_in = Value
    End Set
  End Property

  Public Property Points() As Decimal
    Get
      Return m_gt_session.gt_points
    End Get
    Set(ByVal Value As Decimal)
      m_gt_session.gt_points = Value
    End Set
  End Property

  Public Property ComputedPoints() As Decimal
    Get
      Return m_gt_session.gt_computed_points
    End Get
    Set(ByVal Value As Decimal)
      m_gt_session.gt_computed_points = Value
    End Set
  End Property

  Public Property IsoCode() As String
    Get
      Return m_gt_session.gt_iso_code
    End Get
    Set(ByVal Value As String)
      m_gt_session.gt_iso_code = Value
    End Set
  End Property

#End Region ' Properties

#Region "Overrides"

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA
    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _field_value As String
    Dim _datetime_value As DateTime

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_GAMING_TABLES)

    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(5856), "'" & Me.TableName & "' " & GLB_NLS_GUI_CONTROLS.GetString(14) & " '" & Me.TableSessionName & "'")

    ' Account
    If Me.IsUnknown Then
      Call _auditor_data.SetField(GLB_NLS_GUI_INVOICING.Id(230), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5020))
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_INVOICING.Id(230), Me.AccountId)
    End If

    ' Start
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5285), GUI_FormatDate(Me.StartSession, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS))

    ' End
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5286), GUI_FormatDate(Me.EndSession, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS))

    ' Walk Time
    _datetime_value = DateTime.Now.Date.AddSeconds(Me.WalkTime)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4984), GUI_FormatTime(_datetime_value, ENUM_FORMAT_TIME.FORMAT_HHMMSS))


    ' Speed
    Select Case Me.Speed
      Case GTPlayerTrackingSpeed.Slow
        _field_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5021)
      Case GTPlayerTrackingSpeed.Medium
        _field_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5022)
      Case GTPlayerTrackingSpeed.Fast
        _field_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5023)
      Case Else
        _field_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5020)
    End Select
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5019), _field_value)

    ' Skill
    Select Case Me.Skill
      Case GTPlaySessionPlayerSkill.Soft
        _field_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5025)
      Case GTPlaySessionPlayerSkill.Average
        _field_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5026)
      Case GTPlaySessionPlayerSkill.Hard
        _field_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5027)
      Case Else
        _field_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5020)
    End Select
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5024), _field_value)

    ' Plays
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4983), Me.Plays)

    ' Played Amount
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(3372), GUI_FormatCurrency(Me.PlayedAmount, 2))

    ' Current Bet
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4486), GUI_FormatCurrency(Me.CurrentBet, 2))

    ' Min Bet
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(420), GUI_FormatCurrency(Me.MinBet, 2))

    ' Max Bet
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(845), GUI_FormatCurrency(Me.MaxBet, 2))

    ' Chips In
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5287), GUI_FormatCurrency(Me.ChipsIn, 2))

    ' Chips Out
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5288), GUI_FormatCurrency(Me.ChipsOut, 2))

    ' Buy In
    'If (GeneralParam.GetString("GamingTable.PlayerTracking", "PlayerTracking.SellChips.Text", String.Empty) = String.Empty) Then
    '  _field_value = GLB_NLS_GUI_PLAYER_TRACKING.Id(5289)
    'Else
    '  _field_value = GLB_NLS_GUI_PLAYER_TRACKING.Id(5073, GeneralParam.GetString("GamingTable.PlayerTracking", "PlayerTracking.SellChips.Text"))
    'End If
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5289), GUI_FormatCurrency(Me.BuyIn, 2))

    ' Awared Points
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2684), Me.Points)

    ' Computed Points
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2683), Me.ComputedPoints)

    ' Currency
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2430), Me.IsoCode)

    Return _auditor_data
  End Function

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As ENUM_STATUS

    _rc = ENUM_STATUS.STATUS_ERROR

    Using _db_trx As New DB_TRX()

      _rc = InsertGamingTableSession(_db_trx.SqlTransaction)

    End Using

    Return _rc
  End Function

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE
    Dim temp_object As CLASS_GAMING_TABLE_SESSION

    temp_object = New CLASS_GAMING_TABLE_SESSION
    temp_object.m_gt_session.gt_table_id = Me.m_gt_session.gt_table_id
    temp_object.m_gt_session.gt_table_name = Me.m_gt_session.gt_table_name
    temp_object.m_gt_session.gt_table_session_id = Me.m_gt_session.gt_table_session_id
    temp_object.m_gt_session.gt_table_session_name = Me.m_gt_session.gt_table_session_name
    temp_object.m_gt_session.gt_account_id = Me.m_gt_session.gt_account_id
    temp_object.m_gt_session.gt_start_session = Me.m_gt_session.gt_start_session
    temp_object.m_gt_session.gt_end_session = Me.m_gt_session.gt_end_session
    temp_object.m_gt_session.gt_game_time = Me.m_gt_session.gt_game_time
    temp_object.m_gt_session.gt_walk_time = Me.m_gt_session.gt_walk_time
    temp_object.m_gt_session.gt_speed = Me.m_gt_session.gt_speed
    temp_object.m_gt_session.gt_skill = Me.m_gt_session.gt_skill
    temp_object.m_gt_session.gt_played_amount = Me.m_gt_session.gt_played_amount
    temp_object.m_gt_session.gt_current_bet = Me.m_gt_session.gt_current_bet
    temp_object.m_gt_session.gt_plays = Me.m_gt_session.gt_plays
    temp_object.m_gt_session.gt_min_bet = Me.m_gt_session.gt_min_bet
    temp_object.m_gt_session.gt_max_bet = Me.m_gt_session.gt_max_bet
    temp_object.m_gt_session.gt_chips_in = Me.m_gt_session.gt_chips_in
    temp_object.m_gt_session.gt_chips_out = Me.m_gt_session.gt_chips_out
    temp_object.m_gt_session.gt_buy_in = Me.m_gt_session.gt_buy_in
    temp_object.m_gt_session.gt_points = Me.m_gt_session.gt_points
    temp_object.m_gt_session.gt_computed_points = Me.m_gt_session.gt_computed_points

    Return temp_object

  End Function

#End Region ' Overrides

#Region "Private"
  '----------------------------------------------------------------------------
  ' PURPOSE : Adds a LCD Message object into DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - SqlTrans
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_DEPENDENCIES
  '     - STATUS_DUPLICATE_KEY
  '     - STATUS_ERROR
  '
  ' NOTES :

  Private Function InsertGamingTableSession(ByVal SqlTrans As SqlTransaction) As Integer

    Dim _sb As StringBuilder
    Dim _row_affected As Integer

    Try

      _sb = New StringBuilder()
      _sb.AppendLine("INSERT INTO   GT_PLAY_SESSIONS ")
      _sb.AppendLine("            ( GTPS_GAMING_TABLE_ID ")
      _sb.AppendLine("            , GTPS_GAMING_TABLE_SESSION_ID ")
      _sb.AppendLine("            , GTPS_STARTED ")
      _sb.AppendLine("            , GTPS_FINISHED ")
      _sb.AppendLine("            , GTPS_ACCOUNT_ID ")
      _sb.AppendLine("            , GTPS_SEAT_ID ")
      _sb.AppendLine("            , GTPS_STATUS ")
      _sb.AppendLine("            , GTPS_PLAYS ")
      _sb.AppendLine("            , GTPS_CURRENT_BET ")
      _sb.AppendLine("            , GTPS_BET_MIN ")
      _sb.AppendLine("            , GTPS_BET_MAX ")
      _sb.AppendLine("            , GTPS_PLAYED_AMOUNT ")
      _sb.AppendLine("            , GTPS_PLAYED_AVERAGE ")
      _sb.AppendLine("            , GTPS_POINTS ")
      _sb.AppendLine("            , GTPS_COMPUTED_POINTS ")
      _sb.AppendLine("            , GTPS_GAME_TIME ")
      _sb.AppendLine("            , GTPS_START_WALK ")
      _sb.AppendLine("            , GTPS_WALK ")
      _sb.AppendLine("            , GTPS_PLAYER_SKILL ")
      _sb.AppendLine("            , GTPS_PLAYER_SPEED ")
      _sb.AppendLine("            , GTPS_FINISHED_USER_ID ")
      _sb.AppendLine("            , GTPS_UPDATED_USER_ID ")
      _sb.AppendLine("            , GTPS_TOTAL_SELL_CHIPS ")
      _sb.AppendLine("            , GTPS_CHIPS_OUT ")
      _sb.AppendLine("            , GTPS_CHIPS_IN ")
      _sb.AppendLine("            , GTPS_ISO_CODE ")
      _sb.AppendLine("            , GTPS_NETWIN ) ")
      _sb.AppendLine("     VALUES ")
      _sb.AppendLine("            ( @pGamingTableId ")
      _sb.AppendLine("            , @pGamingTableSessionId ")
      _sb.AppendLine("            , @pStarted ")
      _sb.AppendLine("            , @pFinished ")
      _sb.AppendLine("            , @pAccountId ")
      _sb.AppendLine("            , @pSeat ")
      _sb.AppendLine("            , @Status ")
      _sb.AppendLine("            , @pPlays ")
      _sb.AppendLine("            , @pCurrentBet ")
      _sb.AppendLine("            , @pBetMin ")
      _sb.AppendLine("            , @pBetMax ")
      _sb.AppendLine("            , @pPlayedAmount ")
      _sb.AppendLine("            , @pPlayedAverage ")
      _sb.AppendLine("            , @pPoints ")
      _sb.AppendLine("            , @pComputedPoints ")
      _sb.AppendLine("            , @pGameTime ")
      _sb.AppendLine("            , @pStartWalk ")
      _sb.AppendLine("            , @pWalk ")
      _sb.AppendLine("            , @pPlayerSkill ")
      _sb.AppendLine("            , @pPlayerSpeed ")
      _sb.AppendLine("            , @pFinishedUserId ")
      _sb.AppendLine("            , @pUpdatedUserId ")
      _sb.AppendLine("            , @pTotalSellChips ")
      _sb.AppendLine("            , @pChipsOut ")
      _sb.AppendLine("            , @pChipsIn ")
      _sb.AppendLine("            , @pIsoCode ")
      _sb.AppendLine("            , @pNetwin ) ")

      Using _cmd As New SqlCommand(_sb.ToString(), SqlTrans.Connection, SqlTrans)

        _cmd.Parameters.Add("@pGamingTableId", SqlDbType.Int).Value = Me.TableId
        _cmd.Parameters.Add("@pGamingTableSessionId", SqlDbType.BigInt).Value = Me.TableSessionId
        _cmd.Parameters.Add("@pStarted", SqlDbType.DateTime).Value = Me.StartSession
        _cmd.Parameters.Add("@pFinished", SqlDbType.DateTime).Value = Me.EndSession
        _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = Me.AccountId
        _cmd.Parameters.Add("@pSeat", SqlDbType.BigInt).Value = -1
        _cmd.Parameters.Add("@Status", SqlDbType.Int).Value = GTPlaySessionStatus.Closed
        _cmd.Parameters.Add("@pPlays", SqlDbType.Int).Value = Me.Plays
        _cmd.Parameters.Add("@pCurrentBet", SqlDbType.Money).Value = Me.CurrentBet
        _cmd.Parameters.Add("@pBetMin", SqlDbType.Money).Value = Me.MinBet
        _cmd.Parameters.Add("@pBetMax", SqlDbType.Money).Value = Me.MaxBet
        _cmd.Parameters.Add("@pPlayedAmount", SqlDbType.Money).Value = Me.PlayedAmount
        _cmd.Parameters.Add("@pPlayedAverage", SqlDbType.Money).Value = Me.CurrentBet
        _cmd.Parameters.Add("@pPoints", SqlDbType.Money).Value = Me.Points
        _cmd.Parameters.Add("@pComputedPoints", SqlDbType.Money).Value = Me.ComputedPoints
        _cmd.Parameters.Add("@pGameTime", SqlDbType.Int).Value = Me.GameTime
        _cmd.Parameters.Add("@pStartWalk", SqlDbType.DateTime).Value = DBNull.Value
        _cmd.Parameters.Add("@pWalk", SqlDbType.BigInt).Value = Me.WalkTime
        _cmd.Parameters.Add("@pPlayerSkill", SqlDbType.Int).Value = Me.Skill
        _cmd.Parameters.Add("@pPlayerSpeed", SqlDbType.Int).Value = Me.Speed
        _cmd.Parameters.Add("@pFinishedUserId", SqlDbType.Int).Value = GLB_CurrentUser.Id
        _cmd.Parameters.Add("@pUpdatedUserId", SqlDbType.Int).Value = GLB_CurrentUser.Id
        _cmd.Parameters.Add("@pTotalSellChips", SqlDbType.Money).Value = Me.BuyIn
        _cmd.Parameters.Add("@pChipsOut", SqlDbType.Money).Value = Me.ChipsOut
        _cmd.Parameters.Add("@pChipsIn", SqlDbType.Money).Value = Me.ChipsIn
        _cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).Value = Me.IsoCode
        _cmd.Parameters.Add("@pNetwin", SqlDbType.Money).Value = Me.ChipsIn + Me.BuyIn - Me.ChipsOut

        _row_affected = _cmd.ExecuteNonQuery()

        If _row_affected < 1 Then
          Return ENUM_STATUS.STATUS_ERROR
        End If


        If Me.TerminalId <> 0 And Me.Points > 0 Then
          Dim _won_bucket_list As New WonBucketDict

          If (GamingTableBusinessLogic.Get_CalculateBuckets(Me.TerminalId, Me.AccountId, 0, 0, 0, _won_bucket_list, SqlTrans) = GamingTableBusinessLogic.GT_RESPONSE_CODE.ERROR_GENERIC) Then
            Log.Message("WSI.Common.GamingTableBusinessLogic.Get_CalculateBuckets()")
            Return ENUM_STATUS.STATUS_ERROR
          End If

          For Each _key_value As KeyValuePair(Of Buckets.BucketId, WonBucket) In _won_bucket_list.Dict
            _key_value.Value.AccountId = Me.AccountId
            _key_value.Value.TerminalId = Me.TerminalId
            _key_value.Value.WonValue = Me.Points
          Next

          If Not GamingTableBusinessLogic.UpdateAccountBuckets(Me.AccountId, Me.TerminalName, _won_bucket_list, SqlTrans) Then
            Log.Message("WSI.Common.GamingTableBusinessLogic.UpdateAccountBuckets()")
            Return ENUM_STATUS.STATUS_ERROR
          End If
        End If

        SqlTrans.Commit()

        Return ENUM_STATUS.STATUS_OK

      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return ENUM_STATUS.STATUS_ERROR

  End Function ' InsertGamingTable


#End Region ' Private

#Region "Public"

  Public Function ReadGamingTablesInfo(ByRef GamingTables As DataTable) As Boolean

    Dim _sb As StringBuilder
    Dim _max_gt_sessions As Int32

    Using DB_TRX As New DB_TRX

      Try

        _max_gt_sessions = GeneralParam.GetInt32("GamingTable.PlayerTracking", "ShowSessionsOnManualEntry", 20)

        _sb = New StringBuilder()

        ' Get all the data asociated for each gaming table: TOP XX sessions, virtual terminal, virtual account...
        _sb.AppendLine("WITH Gam_Tab_Sesions AS ( ")
        _sb.AppendLine("    SELECT GTS_GAMING_TABLE_ID, GTS_GAMING_TABLE_SESSION_ID, GTS_CASHIER_SESSION_ID, ROW_NUMBER() ")
        _sb.AppendLine("                                                                                      OVER ( ")
        _sb.AppendLine("                                                                                          PARTITION BY GTS_GAMING_TABLE_ID ")
        _sb.AppendLine("                                                                                          ORDER BY GTS_GAMING_TABLE_SESSION_ID DESC")
        _sb.AppendLine("                                                                                      ) AS SESSION_NO ")
        _sb.AppendLine("    FROM GAMING_TABLES_SESSIONS ")
        _sb.AppendLine("), ")
        _sb.AppendLine("     Gam_Tab_Seats AS ( ")
        _sb.AppendLine("    SELECT GTS_GAMING_TABLE_ID, GTS_TERMINAL_ID, GTS_ACCOUNT_ID, ROW_NUMBER() ")
        _sb.AppendLine("                                                                  OVER ( ")
        _sb.AppendLine("                                                                      PARTITION BY GTS_GAMING_TABLE_ID ")
        _sb.AppendLine("                                                                      ORDER BY GTS_TERMINAL_ID, GTS_ACCOUNT_ID ")
        _sb.AppendLine("                                                                  ) AS SEAT_NO ")
        _sb.AppendLine("    FROM GT_SEATS ")
        _sb.AppendLine(") ")

        _sb.AppendLine("SELECT   GTT_GAMING_TABLE_TYPE_ID ")
        _sb.AppendLine("       , GTT_NAME ")
        _sb.AppendLine("       , GT_GAMING_TABLE_ID ")
        _sb.AppendLine("       , GT_NAME ")
        _sb.AppendLine("       , CS_SESSION_ID ")
        _sb.AppendLine("       , GTS_GAMING_TABLE_SESSION_ID ")
        _sb.AppendLine("       , CS_NAME ")
        _sb.AppendLine("       , CS_OPENING_DATE ")
        _sb.AppendLine("       , CS_CLOSING_DATE ")
        _sb.AppendLine("       , CS_STATUS ")
        _sb.AppendLine("       , GTS_TERMINAL_ID ")
        _sb.AppendLine("       , GTS_ACCOUNT_ID ")
        _sb.AppendLine("       , TE_NAME ")
        _sb.AppendLine("       , ISNULL(GT_THEORIC_HOLD, 0) AS GT_THEORIC_HOLD")
        _sb.AppendLine("  FROM   GAMING_TABLES_TYPES ")
        _sb.AppendLine(" INNER   JOIN GAMING_TABLES    ON GT_TYPE_ID         = GTT_GAMING_TABLE_TYPE_ID ")
        _sb.AppendLine(" INNER   JOIN Gam_Tab_Sesions  ON GT_GAMING_TABLE_ID = Gam_Tab_Sesions.GTS_GAMING_TABLE_ID AND SESSION_NO <= " & _max_gt_sessions)
        _sb.AppendLine(" INNER   JOIN CASHIER_SESSIONS ON CS_SESSION_ID      = GTS_CASHIER_SESSION_ID ")
        _sb.AppendLine(" INNER   JOIN Gam_Tab_Seats    ON GT_GAMING_TABLE_ID = Gam_Tab_Seats.GTS_GAMING_TABLE_ID   AND SEAT_NO    <= 1 ")
        _sb.AppendLine(" INNER   JOIN TERMINALS        ON TE_TERMINAL_ID     = GTS_TERMINAL_ID ")
        _sb.AppendLine(" ORDER   BY GTT_NAME, GT_NAME, CS_SESSION_ID DESC ")

        GamingTables = GUI_GetTableUsingSQL(_sb.ToString(), 5000)

        If IsNothing(GamingTables) Then
          Return False
        End If

        If GamingTables.Rows.Count() = 0 Then
          Return False
        End If

        Return True

      Catch _ex As Exception
        Log.Exception(_ex)
      End Try

    End Using

    Return False

  End Function

  Public Function ReadAccountInfo(ByVal FilterSQL As String, ByRef AccountId As Int64, ByRef AccountName As String, ByRef AnonymAccount As Boolean) As Boolean

    Dim _data_table As DataTable
    Dim _sb As StringBuilder

    Using DB_TRX As New DB_TRX

      Try

        _sb = New StringBuilder()

        _sb.AppendLine("SELECT   TOP 1 ")
        _sb.AppendLine("	       AC_ACCOUNT_ID ")
        _sb.AppendLine("	     , AC_HOLDER_NAME ")
        _sb.AppendLine("	     , AC_HOLDER_LEVEL ")
        _sb.AppendLine("  FROM   ACCOUNTS ")
        _sb.AppendLine(" WHERE " & FilterSQL)

        _data_table = GUI_GetTableUsingSQL(_sb.ToString(), 5000)

        If IsNothing(_data_table) Then
          Return False
        End If

        If _data_table.Rows.Count() = 0 Then
          Return False
        End If

        If _data_table.Rows.Count() <> 1 Then
          Return False
        End If

        AccountId = IIf(IsDBNull(_data_table.Rows(0).Item("AC_ACCOUNT_ID")), "", _data_table.Rows(0).Item("AC_ACCOUNT_ID"))
        AccountName = IIf(IsDBNull(_data_table.Rows(0).Item("AC_HOLDER_NAME")), "", _data_table.Rows(0).Item("AC_HOLDER_NAME"))
        AnonymAccount = IIf(_data_table.Rows(0).Item("AC_HOLDER_LEVEL") = 0, True, False)

        Return True

      Catch _ex As Exception
        Log.Exception(_ex)
      End Try

    End Using

    Return False

  End Function

#End Region ' Public

End Class
