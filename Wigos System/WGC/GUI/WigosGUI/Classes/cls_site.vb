'-------------------------------------------------------------------
' Copyright � 2011 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_site.vb
' DESCRIPTION:   Site class for site edition screen
' AUTHOR:        Agust� Poch
' CREATION DATE: 07-FEB-2011
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 07-FEB-2011  APB    Initial version.
'
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices

Public Class CLASS_SITE
  Inherits CLASS_BASE

#Region " Constants "

  ' Fields length
  Public Const MAX_SITE_NAME_LEN As Integer = 30

#End Region ' Constants

#Region " GUI_Configuration.dll "

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
   Public Class TYPE_SITE
    Public control_block As Integer
    Public site_id As Integer
    Public operator_id As Integer
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_SITE_NAME_LEN + 1)> _
    Public site_name As String
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=3)> _
    Public filler_0 As String

  End Class ' TYPE_SITE

#End Region ' Structures

#End Region ' GUI_Configuration.dll

#Region " Members "

  Protected m_site As New TYPE_SITE
  Protected m_operator_name As String

#End Region

#Region " Properties "

  Public Property SiteId() As Integer
    Get
      Return m_site.site_id
    End Get
    Set(ByVal Value As Integer)
      m_site.site_id = Value
    End Set
  End Property

  Public Property OperatorName() As String
    Get
      Return m_operator_name
    End Get
    Set(ByVal Value As String)
      m_operator_name = Value
    End Set
  End Property

  Public Property SiteName() As String
    Get
      Return m_site.site_name
    End Get
    Set(ByVal Value As String)
      m_site.site_name = Value
    End Set
  End Property

  Public Property OperatorId() As Integer
    Get
      Return m_site.operator_id
    End Get
    Set(ByVal Value As Integer)
      m_site.operator_id = Value
    End Set
  End Property

#End Region ' Properties

#Region " Overrides functions "

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer

    Me.SiteId = ObjectId
    m_site.control_block = Marshal.SizeOf(m_site)

    ' Read site data 
    rc = ReadSite(m_site, SqlCtx)

    Select Case rc

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        ' Operator name
        Me.OperatorName = CLASS_SITE_OPERATOR.GetOperatorNameForId(Me.OperatorId, SqlCtx)
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Read

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer

    m_site.control_block = Marshal.SizeOf(m_site)

    ' Update site data
    rc = UpdateSite(m_site, SqlCtx)

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        ' Operator name
        Me.OperatorName = CLASS_SITE_OPERATOR.GetOperatorNameForId(Me.OperatorId, SqlCtx)
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Update

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer

    m_site.control_block = Marshal.SizeOf(m_site)

    ' Insert site data
    rc = InsertSite(m_site, SqlCtx)

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        ' Operator name
        Me.OperatorName = CLASS_SITE_OPERATOR.GetOperatorNameForId(Me.OperatorId, SqlCtx)
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Insert

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer

    ' Delete site data
    rc = DeleteSite(Me.SiteId, SqlCtx)

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        ' Operator name
        Me.OperatorName = CLASS_SITE_OPERATOR.GetOperatorNameForId(Me.OperatorId, SqlCtx)
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Delete

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim auditor_data As CLASS_AUDITOR_DATA

    auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_SITES_OPERATORS)
    Call auditor_data.SetName(GLB_NLS_GUI_CONFIGURATION.Id(380), Me.SiteId)

    ' Site Id
    Call auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(209), Me.SiteId)

    ' Site name
    Call auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(210), IIf(Me.SiteName = "", AUDIT_NONE_STRING, Me.SiteName))

    ' Operator name
    Call auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(381), IIf(Me.OperatorName = "", AUDIT_NONE_STRING, Me.OperatorName))

    Return auditor_data

  End Function ' AuditorData

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim temp_site As CLASS_SITE

    temp_site = New CLASS_SITE
    temp_site.m_site.control_block = Me.m_site.control_block
    temp_site.m_site.site_id = Me.m_site.site_id
    temp_site.m_site.operator_id = Me.m_site.operator_id
    temp_site.m_site.site_name = Me.m_site.site_name
    temp_site.m_site.filler_0 = Me.m_site.filler_0
    temp_site.m_operator_name = Me.m_operator_name

    Return temp_site

  End Function ' Duplicate

#End Region ' Overrides functions

#Region " Private Functions "

  Private Function ReadSite(ByVal Site As TYPE_SITE, _
                            ByVal Context As Integer) As Integer
    Dim str_sql As String
    Dim data_table As DataTable

    str_sql = "SELECT ST_SITE_ID " & _
                  " , ST_NAME " & _
                  " , ST_OPERATOR_ID " & _
               " FROM SITES " & _
              " WHERE ST_SITE_ID = " & Site.site_id

    data_table = GUI_GetTableUsingSQL(str_sql, 5000)
    If IsNothing(data_table) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If data_table.Rows.Count() <> 1 Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    Site.site_name = NullTrim(data_table.Rows(0).Item("ST_NAME"))
    Site.operator_id = data_table.Rows(0).Item("ST_OPERATOR_ID")

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function ' ReadSite

  Private Function UpdateSite(ByVal Site As TYPE_SITE, _
                              ByVal Context As Integer) As Integer

    Dim str_sql As String
    Dim SqlTrans As System.Data.SqlClient.SqlTransaction = Nothing

    If Not GUI_BeginSQLTransaction(SqlTrans) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    str_sql = "UPDATE   SITES " & _
                " SET   ST_OPERATOR_ID = " & Site.operator_id & _
                    " , ST_NAME = '" & Site.site_name & "' " & _
              " WHERE   ST_SITE_ID = " & Site.site_id

    If Not GUI_SQLExecuteNonQuery(str_sql, SqlTrans) Then
      GUI_EndSQLTransaction(SqlTrans, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If Not GUI_EndSQLTransaction(SqlTrans, True) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function ' UpdateSite

  Private Function InsertSite(ByVal Site As TYPE_SITE, _
                              ByVal Context As Integer) As Integer

    Dim str_sql As String
    Dim db_count As Integer = 0
    Dim SqlTrans As System.Data.SqlClient.SqlTransaction = Nothing
    Dim data_table As DataTable

    If Not GUI_BeginSQLTransaction(SqlTrans) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    str_sql = "SELECT  COUNT(*) " & _
               " FROM  SITES " & _
              " WHERE  ST_SITE_ID = '" & Site.site_id & "'"

    data_table = GUI_GetTableUsingSQL(str_sql, 5000)
    If IsNothing(data_table) Then
      GUI_EndSQLTransaction(SqlTrans, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    db_count = data_table.Rows(0).Item(0)

    If db_count > 0 Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
    End If

    str_sql = "INSERT INTO   SITES " & _
                         " ( ST_SITE_ID " & _
                         " , ST_NAME " & _
                         " , ST_OPERATOR_ID " & _
                         " ) " & _
                  " VALUES " & _
                         " ( " & Site.site_id & _
                         " , '" & Site.site_name & "' " & _
                         " , " & Site.operator_id & _
                         " )"

    If Not GUI_SQLExecuteNonQuery(str_sql, SqlTrans) Then
      GUI_EndSQLTransaction(SqlTrans, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If Not GUI_EndSQLTransaction(SqlTrans, True) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function ' InsertSite

  Private Function DeleteSite(ByVal SiteId As Integer, _
                              ByVal Context As Integer) As Integer

    Dim str_sql As String
    Dim SqlTrans As System.Data.SqlClient.SqlTransaction = Nothing

    If Not GUI_BeginSQLTransaction(SqlTrans) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    str_sql = "DELETE  SITES " & _
              " WHERE  ST_SITE_ID = " & SiteId

    If Not GUI_SQLExecuteNonQuery(str_sql, SqlTrans) Then
      GUI_EndSQLTransaction(SqlTrans, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If Not GUI_EndSQLTransaction(SqlTrans, True) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function ' DeleteSite

#End Region ' Private Functions

End Class
