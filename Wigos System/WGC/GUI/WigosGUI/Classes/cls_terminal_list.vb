'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_terminal_retirement.vb
'
' DESCRIPTION : terminal class for user edition
'              
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 08-MAR-2011  TJG    Initial version
' 09-JAN-2014  ICS    Close open play sessions when terminal is retired
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient

Public Class CLASS_TERMINAL_LIST
  Inherits CLASS_BASE

#Region " Constants "
#End Region

#Region " Enums "
#End Region

#Region " Structures "
#End Region

#Region " Members "

  Private m_list_ids As ArrayList
  Private m_list_terminals As ArrayList

#End Region

#Region " Properties "

  Public Sub New()

    MyBase.New()

    Me.m_list_ids = New ArrayList
    Me.m_list_terminals = New ArrayList

  End Sub

  Public Property Count() As Integer
    Get
      Return m_list_ids.Count
    End Get

    Set(ByVal Value As Integer)
    End Set
  End Property

  Public Property Item(ByVal Index As Integer) As CLASS_TERMINAL
    Get
      Return m_list_terminals(Index)
    End Get

    Set(ByVal Value As CLASS_TERMINAL)
      m_list_terminals(Index) = Value
    End Set
  End Property

#End Region ' Structures

#Region " Overrides functions "

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads from the database into the given object
  '
  ' PARAMS :
  '     - INPUT :
  '         - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '
  ' NOTES :

  Public Overrides Function DB_Read(ByVal ObjectId As Object, _
                                    ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As Integer
    Dim _terminal_id As Integer
    Dim _aux_terminal As CLASS_TERMINAL

    ' Read terminals' data
    For Each _terminal_id In Me.m_list_ids
      _aux_terminal = New CLASS_TERMINAL
      _rc = _aux_terminal.DB_Read(_terminal_id, SqlCtx)

      Select Case _rc
        Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
          Me.AddTerminal(_aux_terminal)

        Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
          Return ENUM_STATUS.STATUS_NOT_FOUND

        Case Else
          Return ENUM_STATUS.STATUS_ERROR

      End Select
    Next

    Return CLASS_BASE.ENUM_STATUS.STATUS_OK

  End Function ' DB_Read

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer
    Dim _idx_terminal As Integer
    Dim _aux_terminal As CLASS_TERMINAL

    ' Update terminals' data
    For _idx_terminal = 0 To m_list_terminals.Count - 1
      _aux_terminal = m_list_terminals.Item(_idx_terminal)

      _rc = _aux_terminal.DB_Update(SqlCtx)

      Select Case _rc
        Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK

        Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
          Return ENUM_STATUS.STATUS_NOT_FOUND

        Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
          Return ENUM_STATUS.STATUS_DUPLICATE_KEY

        Case Else
          Return ENUM_STATUS.STATUS_ERROR

      End Select

    Next

    Return CLASS_BASE.ENUM_STATUS.STATUS_OK

  End Function ' DB_Update

  '----------------------------------------------------------------------------
  ' PURPOSE : Inserts the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Return ENUM_STATUS.STATUS_ERROR

  End Function ' DB_Insert

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes the given object from the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DEPENDENCIES
  '
  ' NOTES :

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Return ENUM_STATUS.STATUS_ERROR

  End Function ' DB_Delete

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns a duplicate of the given object.
  '
  ' PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created object
  '
  ' NOTES :

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _temp_object As CLASS_TERMINAL_LIST
    Dim _idx_terminal As Integer

    _temp_object = New CLASS_TERMINAL_LIST

    For _idx_terminal = 0 To m_list_ids.Count - 1
      _temp_object.AddId(Me.m_list_ids.Item(_idx_terminal))
    Next

    For _idx_terminal = 0 To m_list_terminals.Count - 1
      _temp_object.AddTerminal(Me.m_list_terminals.Item(_idx_terminal))
    Next

    Return _temp_object

  End Function ' Duplicate

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns the related auditor data for the current object.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created audit object
  '
  ' NOTES :

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _idx_terminal As Integer
    Dim _aux_terminal As CLASS_TERMINAL

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_TERMINAL_RETIREMENT)

    ' Update terminals' data
    For _idx_terminal = 0 To Me.m_list_terminals.Count - 1

      _aux_terminal = Me.m_list_terminals.Item(_idx_terminal)

      ' 454 "Retirada de Terminales"
      Call _auditor_data.SetName(GLB_NLS_GUI_CONFIGURATION.Id(454), "(#" + Me.m_list_terminals.Count.ToString() + ")")

      ' 259 "Estado"
      Call _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(259), CLASS_TERMINAL.StatusName(_aux_terminal.Status) + " # " + _aux_terminal.Name)

      ' 259 "ExternalId"
      Call _auditor_data.SetField(GLB_NLS_GUI_AUDITOR.Id(331), _aux_terminal.ExternalId + " # " + _aux_terminal.Name)

      ' 455 "Fecha de Retirada"
      If _aux_terminal.Status = WSI.Common.TerminalStatus.RETIRED _
     And _aux_terminal.RetirementDate <> Nothing Then
        Call _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(455), _aux_terminal.RetirementDate + " # " + _aux_terminal.Name)
      Else
        Call _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(455), "---" + " # " + _aux_terminal.Name)
      End If

      ' Sesiones de juego: Cierre manual
      Select Case _aux_terminal.RetirementPlaySessions
        Case CLASS_TERMINAL.ENUM_RETIREMENT_PLAY_SESSIONS_STATUS.REMAIN_OPEN
          Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4442), GLB_NLS_GUI_AUDITOR.GetString(337))
        Case CLASS_TERMINAL.ENUM_RETIREMENT_PLAY_SESSIONS_STATUS.CLOSE
          Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4442), GLB_NLS_GUI_AUDITOR.GetString(336))
        Case Else
          Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4442), "---")
      End Select
    Next

    Return _auditor_data

  End Function ' AuditorData

#End Region

#Region " Public Functions "

  Public Function AddId(ByVal TerminalId As Int64) As Integer

    m_list_ids.Add(TerminalId)

  End Function

  Public Function AddTerminal(ByRef TerminalItem As CLASS_TERMINAL) As Integer

    m_list_terminals.Add(TerminalItem.Duplicate)

  End Function

#End Region ' Public Functions

#Region " Private Functions "
#End Region ' Private Functions

End Class