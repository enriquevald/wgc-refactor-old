'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_points_to_credit.vb
' DESCRIPTION:   Points to credit class.
' AUTHOR:        Joan Marc Pepi� Jamison
' CREATION DATE: 07-OCT-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 07-OCT-2013  JPJ    Initial version
' 28-OCT-2013  JPJ    Fixed Bug WIG-327: String not found
' 29-OCT-2013  JPJ    Fixed Bug WIG-338: Points don't have to contain decimals.
' 31-OCT-2013  JPJ    Fixed Bug WIG-349: Name swap.
' 05-DEC-2013  JML,LJM     Fixed Bug WIG-458: Error on gift's limits
' 28-APR-2014  JBP    Added VIP column for set GIFT property. 
' 02-MAY-2014  RRR    Fixed Bug WIG-882: Exeception on adding new register.
' 30-MAY-2014  AMF    Fixed Bug WIG-973: Auditor check Only VIP
' 27-JUN-2014  LEM    Fixed Bug WIG-1055: Confirmation message when closing without changes.
'--------------------------------------------------------------------

Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common
Imports System.Data.SqlClient
Imports System.Text

Public Class CLASS_POINTS_TO_CREDITS
  Inherits CLASS_BASE

#Region " Structures "

  Public Class TYPE_POINTS_TO_CREDITS

#Region " Enum "

    Private Enum ROWACTION
      NORMAL = 0
      DELETED = 1
    End Enum

#End Region 'Enum

#Region " Members "

    Private m_points_id As Int64
    Private m_points_name As String
    Private m_points_description As String
    Private m_points_enabled As Boolean
    Private m_points_credit_type As ACCOUNT_PROMO_CREDIT_TYPE
    Private m_points_updated As Date
    Private m_points_schedule_weeday As Int32
    Private m_points_schedule_time_From1 As Int32
    Private m_points_schedule_time_To1 As Int32
    Private m_points_schedule2_enabled As Boolean
    Private m_points_schedule_time_From2 As Int32
    Private m_points_schedule_time_To2 As Int32
    Private m_restricted_to_terminal0 As New TerminalList
    Private m_restricted_to_terminal1 As New TerminalList
    Private m_restricted_to_terminal2 As New TerminalList
    Private m_restricted_to_terminal3 As New TerminalList
    Private m_restricted_to_terminal4 As New TerminalList
    Private m_restricted_to_terminal5 As New TerminalList
    Private m_restricted_to_terminal6 As New TerminalList
    Private m_gift_table As DataTable
    Private m_promotion_id As Int64
    Private m_points_sufix As String
    Private m_gift_table_source As DataTable = Nothing
    Private m_award_game As Long

#End Region 'Members

#Region " Properties "

    Public Property Points_Id() As Int64
      Get
        Return Me.m_points_id
      End Get

      Set(ByVal Value As Int64)
        Me.m_points_id = Value
      End Set
    End Property

    Public Property Points_Name() As String
      Get
        Return Me.m_points_name
      End Get
      Set(ByVal value As String)
        Me.m_points_name = value
      End Set
    End Property

    Public Property Points_Description() As String
      Get
        Return Me.m_points_description
      End Get
      Set(ByVal value As String)
        Me.m_points_description = value
      End Set
    End Property

    Public Property Points_Enabled() As Boolean
      Get
        Return Me.m_points_enabled
      End Get
      Set(ByVal value As Boolean)
        Me.m_points_enabled = value
      End Set
    End Property

    Public Property Points_Credit_Type() As ACCOUNT_PROMO_CREDIT_TYPE
      Get
        Return Me.m_points_credit_type
      End Get
      Set(ByVal value As ACCOUNT_PROMO_CREDIT_TYPE)
        Me.m_points_credit_type = value
      End Set
    End Property

    Public Property Points_Update() As Date
      Get
        Return Me.m_points_updated
      End Get
      Set(ByVal value As Date)
        Me.m_points_updated = value
      End Set
    End Property

    Public Property Points_Schedule_Weekday() As Int32
      Get
        Return Me.m_points_schedule_weeday
      End Get
      Set(ByVal value As Int32)
        Me.m_points_schedule_weeday = value
      End Set
    End Property

    Public Property Points_Schedule_Time_From1() As Int32
      Get
        Return Me.m_points_schedule_time_From1
      End Get
      Set(ByVal value As Int32)
        Me.m_points_schedule_time_From1 = value
      End Set
    End Property

    Public Property Points_Schedule_Time_To1() As Int32
      Get
        Return Me.m_points_schedule_time_To1
      End Get
      Set(ByVal value As Int32)
        Me.m_points_schedule_time_To1 = value
      End Set
    End Property

    Public Property Points_Schedule2_Enabled() As Boolean
      Get
        Return Me.m_points_schedule2_enabled
      End Get
      Set(ByVal value As Boolean)
        Me.m_points_schedule2_enabled = value
      End Set
    End Property

    Public Property Points_Schedule_Time_From2() As Int32
      Get
        Return Me.m_points_schedule_time_From2
      End Get
      Set(ByVal value As Int32)
        Me.m_points_schedule_time_From2 = value
      End Set
    End Property

    Public Property Points_Schedule_Time_To2() As Int32
      Get
        Return Me.m_points_schedule_time_To2
      End Get
      Set(ByVal value As Int32)
        Me.m_points_schedule_time_To2 = value
      End Set
    End Property

    Public Property Restricted_To_Terminal0() As TerminalList
      Get
        Return Me.m_restricted_to_terminal0
      End Get
      Set(ByVal value As TerminalList)
        Me.m_restricted_to_terminal0 = value
      End Set
    End Property

    Public Property Restricted_To_Terminal1() As TerminalList
      Get
        Return Me.m_restricted_to_terminal1
      End Get
      Set(ByVal value As TerminalList)
        Me.m_restricted_to_terminal1 = value
      End Set
    End Property

    Public Property Restricted_To_Terminal2() As TerminalList
      Get
        Return Me.m_restricted_to_terminal2
      End Get
      Set(ByVal value As TerminalList)
        Me.m_restricted_to_terminal2 = value
      End Set
    End Property

    Public Property Restricted_To_Terminal3() As TerminalList
      Get
        Return Me.m_restricted_to_terminal3
      End Get
      Set(ByVal value As TerminalList)
        Me.m_restricted_to_terminal3 = value
      End Set
    End Property

    Public Property Restricted_To_Terminal4() As TerminalList
      Get
        Return Me.m_restricted_to_terminal4
      End Get
      Set(ByVal value As TerminalList)
        Me.m_restricted_to_terminal4 = value
      End Set
    End Property

    Public Property Restricted_To_Terminal5() As TerminalList
      Get
        Return Me.m_restricted_to_terminal5
      End Get
      Set(ByVal value As TerminalList)
        Me.m_restricted_to_terminal5 = value
      End Set
    End Property

    Public Property Restricted_To_Terminal6() As TerminalList
      Get
        Return Me.m_restricted_to_terminal6
      End Get
      Set(ByVal value As TerminalList)
        Me.m_restricted_to_terminal6 = value
      End Set
    End Property

    Public Property Gift_Table() As DataTable
      Get
        Return Me.m_gift_table
      End Get
      Set(ByVal value As DataTable)
        Me.m_gift_table = value
      End Set
    End Property

    Public Property Promotion_Id() As Int64
      Get
        Return Me.m_promotion_id
      End Get

      Set(ByVal Value As Int64)
        Me.m_promotion_id = Value
      End Set
    End Property

    Public Property Points_Sufix() As String
      Get
        Return Me.m_points_sufix
      End Get
      Set(ByVal value As String)
        Me.m_points_sufix = value
      End Set
    End Property

    Public ReadOnly Property WorkingDaysText(ByVal Days As Integer) As String
      Get
        Dim _str_binary As String
        Dim _bit_array As Char()
        Dim _str_days As String

        _str_days = ""

        _str_binary = Convert.ToString(Days, 2)
        _str_binary = New String("0", 7 - _str_binary.Length) + _str_binary

        _bit_array = _str_binary.ToCharArray()

        If (_bit_array(6) = "1") Then _str_days = _str_days & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(325) '"Monday "
        If (_bit_array(5) = "1") Then _str_days = _str_days & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(326) '"Tuesday "
        If (_bit_array(4) = "1") Then _str_days = _str_days & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(327) '"Wednesday "
        If (_bit_array(3) = "1") Then _str_days = _str_days & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(328) '"Thursday "
        If (_bit_array(2) = "1") Then _str_days = _str_days & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(329) '"Friday "
        If (_bit_array(1) = "1") Then _str_days = _str_days & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(330) '"Saturday "
        If (_bit_array(0) = "1") Then _str_days = _str_days & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(331) '"Sunday "


        Return _str_days.TrimStart

      End Get
    End Property

    Public Property AwardGame() As Long
      Get
        Return m_award_game
      End Get
      Set(ByVal value As Long)
        m_award_game = value
      End Set
    End Property

#End Region 'Properties

#Region " Constants "

    ' Datatable Columns by Id
    Private Const GIFT_COLUMN_LEVEL1 As Integer = 0  'LEVEL1
    Private Const GIFT_COLUMN_LEVEL2 As Integer = 1  'LEVEL2
    Private Const GIFT_COLUMN_LEVEL3 As Integer = 2  'LEVEL3
    Private Const GIFT_COLUMN_LEVEL4 As Integer = 3  'LEVEL4
    Private Const GIFT_COLUMN_IS_VIP As Integer = 4  'IS VIP
    Private Const GIFT_COLUMN_CREDITS As Integer = 5 'CREDITS
    Private Const GIFT_COLUMN_GIFT_ID As Integer = 6 'GIFT_ID

    Private Const GIFT_COLUMN_ACTION_NAME As String = "ACTION"

    Private Const GIFT_NUM_ROWS As Integer = 100 ' Number of gifts elements

#End Region 'Constants

#Region " Public Function "

    ' PURPOSE: Get new empty datatable
    '
    '    - INPUT:
    '
    '    - OUTPUT:
    '
    ' RETURNS:
    '    - DataTable : Gift datatable
    Public Function CreateGiftTable() As DataTable
      Dim _gift_data As DataTable = New DataTable()

      _gift_data.Columns.Add(GIFT_COLUMN_LEVEL1, Type.GetType("System.Decimal"))
      _gift_data.Columns.Add(GIFT_COLUMN_LEVEL2, Type.GetType("System.Decimal"))
      _gift_data.Columns.Add(GIFT_COLUMN_LEVEL3, Type.GetType("System.Decimal"))
      _gift_data.Columns.Add(GIFT_COLUMN_LEVEL4, Type.GetType("System.Decimal"))
      _gift_data.Columns.Add(GIFT_COLUMN_IS_VIP, Type.GetType("System.Boolean"))
      _gift_data.Columns.Add(GIFT_COLUMN_CREDITS, Type.GetType("System.Decimal"))
      _gift_data.Columns.Add(GIFT_COLUMN_GIFT_ID, Type.GetType("System.Int64"))

      _gift_data.PrimaryKey = New DataColumn() {_gift_data.Columns(GIFT_COLUMN_GIFT_ID)}

      Return _gift_data

    End Function 'CreateGiftTable

    ' PURPOSE: Clone/duplicate the object
    '
    '    - INPUT:
    '
    '    - OUTPUT:
    '
    ' RETURNS:
    '    - TYPE_POINTS_TO_CREDITS : New instance copy of the actual
    Public Function Clone() As TYPE_POINTS_TO_CREDITS
      Dim _clone As TYPE_POINTS_TO_CREDITS

      _clone = New TYPE_POINTS_TO_CREDITS()
      _clone.m_points_id = Me.m_points_id
      _clone.m_points_name = Me.m_points_name
      _clone.m_points_description = Me.m_points_description
      _clone.m_points_enabled = Me.m_points_enabled
      _clone.m_points_credit_type = Me.m_points_credit_type
      _clone.m_points_updated = Me.m_points_updated
      _clone.m_points_schedule_weeday = Me.m_points_schedule_weeday
      _clone.m_points_schedule_time_From1 = Me.m_points_schedule_time_From1
      _clone.m_points_schedule_time_To1 = Me.m_points_schedule_time_To1
      _clone.m_points_schedule2_enabled = Me.m_points_schedule2_enabled
      _clone.m_points_schedule_time_From2 = Me.m_points_schedule_time_From2
      _clone.m_points_schedule_time_To2 = Me.m_points_schedule_time_To2

      _clone.m_restricted_to_terminal0 = New TerminalList()
      _clone.m_restricted_to_terminal0.FromXml(Me.m_restricted_to_terminal0.ToXml())

      _clone.m_restricted_to_terminal1 = New TerminalList()
      _clone.m_restricted_to_terminal1.FromXml(Me.m_restricted_to_terminal1.ToXml())

      _clone.m_restricted_to_terminal2 = New TerminalList()
      _clone.m_restricted_to_terminal2.FromXml(Me.m_restricted_to_terminal2.ToXml())

      _clone.m_restricted_to_terminal3 = New TerminalList()
      _clone.m_restricted_to_terminal3.FromXml(Me.m_restricted_to_terminal3.ToXml())

      _clone.m_restricted_to_terminal4 = New TerminalList()
      _clone.m_restricted_to_terminal4.FromXml(Me.m_restricted_to_terminal4.ToXml())

      _clone.m_restricted_to_terminal5 = New TerminalList()
      _clone.m_restricted_to_terminal5.FromXml(Me.m_restricted_to_terminal5.ToXml())

      _clone.m_restricted_to_terminal6 = New TerminalList()
      _clone.m_restricted_to_terminal6.FromXml(Me.m_restricted_to_terminal6.ToXml())

      If Not IsNothing(Me.m_gift_table) Then
        _clone.m_gift_table = Me.m_gift_table.Copy()
        _clone.m_gift_table_source = Me.m_gift_table.Copy()
        m_gift_table_source = Me.m_gift_table.Copy()
      End If

      _clone.m_promotion_id = Me.m_promotion_id
      _clone.m_points_sufix = Me.m_points_sufix
      _clone.m_award_game = Me.m_award_game

      Return _clone

    End Function 'Clone

#End Region 'Public Function

#Region " Public Sub "

    ' PURPOSE: Clone the class
    '
    '    - INPUT:
    '
    '    - OUTPUT:
    '
    ' RETURNS:
    '    - Sufix : Contains the sufix for the audit
    Private Function GetSufix(ByVal TableNumber As String, ByVal Element As String) As String

      Return "TC." & TableNumber & "." & Element
    End Function 'GetSufix

    ' PURPOSE: Audits the class
    '
    '    - INPUT:
    '       - TableNumber: Number of the tabpage that is being audited
    '       - Auditor: Auditor object.
    '
    '    - OUTPUT:
    '
    ' RETURNS:
    '    - 
    Public Sub AuditPointsToCredit(ByVal TableNumber As String, ByRef Auditor As CLASS_AUDITOR_DATA)
      Dim _elapsed As TimeSpan
      Dim _promogame_string As String

      _promogame_string = String.Empty

      Call Auditor.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(2714), "")                                                         ' Redemption table
      Call Auditor.SetField(0, Me.Points_Name, GetSufix(TableNumber, GLB_NLS_GUI_CONTROLS.GetString(461)))                   ' Name
      'JPJ 28-OCT-2013: Defect WIG-327 String not found
      Call Auditor.SetField(0, Me.Points_Description, GetSufix(TableNumber, GLB_NLS_GUI_PLAYER_TRACKING.GetString(688)))     ' Description
      Call Auditor.SetField(0, Me.Points_Sufix, GetSufix(TableNumber, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2851)))          ' Sufix
      ' Credit type
      If Me.Points_Credit_Type = ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE Then
        Call Auditor.SetField(0, GLB_NLS_GUI_INVOICING.GetString(337), GetSufix(TableNumber, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1163)))
      Else
        Call Auditor.SetField(0, GLB_NLS_GUI_INVOICING.GetString(338), GetSufix(TableNumber, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1163)))
      End If

      If Me.Points_Enabled Then
        Call Auditor.SetField(0, GLB_NLS_GUI_CONFIGURATION.GetString(311), GetSufix(TableNumber, GLB_NLS_GUI_CONTROLS.GetString(261)))
      Else
        Call Auditor.SetField(0, GLB_NLS_GUI_CONFIGURATION.GetString(328), GetSufix(TableNumber, GLB_NLS_GUI_CONTROLS.GetString(261)))
      End If

      ' Schedule data
      ' Schedule weekdays
      Call Auditor.SetField(0, WorkingDaysText(Me.Points_Schedule_Weekday), GetSufix(TableNumber, GLB_NLS_GUI_PLAYER_TRACKING.GetString(299)))

      ' Schedule timetable 
      _elapsed = TimeSpan.FromSeconds(Me.Points_Schedule_Time_From1)
      Call Auditor.SetField(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(297) & " " & _elapsed.ToString(), GetSufix(TableNumber, GLB_NLS_GUI_PLAYER_TRACKING.GetString(292)))

      _elapsed = TimeSpan.FromSeconds(Me.Points_Schedule_Time_To1)
      Call Auditor.SetField(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(298) & " " & _elapsed.ToString(), GetSufix(TableNumber, GLB_NLS_GUI_PLAYER_TRACKING.GetString(292)))

      If Me.Points_Schedule2_Enabled Then
        _elapsed = TimeSpan.FromSeconds(Me.Points_Schedule_Time_From2)
        Call Auditor.SetField(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(297) & " " & _elapsed.ToString(), GetSufix(TableNumber, GLB_NLS_GUI_PLAYER_TRACKING.GetString(293)))
        _elapsed = TimeSpan.FromSeconds(Me.Points_Schedule_Time_To2)
        Call Auditor.SetField(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(298) & " " & _elapsed.ToString(), GetSufix(TableNumber, GLB_NLS_GUI_PLAYER_TRACKING.GetString(293)))
      Else
        Call Auditor.SetField(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(297) & AUDIT_NONE_STRING, GetSufix(TableNumber, GLB_NLS_GUI_PLAYER_TRACKING.GetString(293)))
        Call Auditor.SetField(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(298) & AUDIT_NONE_STRING, GetSufix(TableNumber, GLB_NLS_GUI_PLAYER_TRACKING.GetString(293)))
      End If

      Call AuditorTerminalList(TableNumber, Auditor)
      Call AuditorGiftList(TableNumber, Auditor)

      ' Promogame
      If Me.AwardGame = -1 Then
        _promogame_string = GLB_NLS_GUI_PLAYER_TRACKING.GetString(248)
      Else
        _promogame_string = New PromoGame(Me.AwardGame).Name
      End If
      Call Auditor.SetField(0, _promogame_string, GetSufix(TableNumber, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8541)))

    End Sub ' AuditPointsToCredit

    ' PURPOSE: Audits the class Terminal lists
    '
    '    - INPUT:
    '       - TableNumber: Number of the tabpage that is being audited
    '       - Auditor: Auditor object.
    '
    '    - OUTPUT:
    '
    ' RETURNS:
    '    - 
    Private Sub AuditorTerminalList(ByVal TableNumber As String, ByRef Auditor As CLASS_AUDITOR_DATA)
      Dim _idx_day As Int16
      Dim _terminal_list As New TerminalList
      Dim _day As String = ""

      For _idx_day = 0 To 6

        Select Case _idx_day
          Case 0
            _terminal_list = Me.Restricted_To_Terminal0
            _day = GLB_NLS_GUI_PLAYER_TRACKING.GetString(331) '"Sunday "
          Case 1
            _terminal_list = Me.Restricted_To_Terminal1
            _day = GLB_NLS_GUI_PLAYER_TRACKING.GetString(325) '"Monday "
          Case 2
            _terminal_list = Me.Restricted_To_Terminal2
            _day = GLB_NLS_GUI_PLAYER_TRACKING.GetString(326) '"Tuesday "
          Case 3
            _terminal_list = Me.Restricted_To_Terminal3
            _day = GLB_NLS_GUI_PLAYER_TRACKING.GetString(327) '"Wednesday "
          Case 4
            _terminal_list = Me.Restricted_To_Terminal4
            _day = GLB_NLS_GUI_PLAYER_TRACKING.GetString(328) '"Thursday "
          Case 5
            _terminal_list = Me.Restricted_To_Terminal5
            _day = GLB_NLS_GUI_PLAYER_TRACKING.GetString(329) '"Friday "
          Case 6
            _terminal_list = Me.Restricted_To_Terminal6
            _day = GLB_NLS_GUI_PLAYER_TRACKING.GetString(330) '"Saturday "
        End Select

        Call Auditor.SetField(0, GLB_NLS_GUI_CONTROLS.GetString(486) & ": " & _
                              _terminal_list.AuditStringType(GLB_NLS_GUI_CONFIGURATION.GetString(497), _
                                                             GLB_NLS_GUI_CONFIGURATION.GetString(499), _
                                                             GLB_NLS_GUI_CONFIGURATION.GetString(498)) _
                              , GetSufix(TableNumber & "." & _day.ToString, GLB_NLS_GUI_CONTROLS.GetString(435)))
        ' Group list
        Call Auditor.SetField(0, GLB_NLS_GUI_CONTROLS.GetString(482) & ": " & _
                              _terminal_list.AuditStringList(GROUP_ELEMENT_TYPE.GROUP, _
                                                             GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)) _
                              , GetSufix(TableNumber & "." & _day.ToString, GLB_NLS_GUI_CONTROLS.GetString(465)))

        ' Providers list
        Call Auditor.SetField(0, GLB_NLS_GUI_CONTROLS.GetString(478) & ": " & _
                              _terminal_list.AuditStringList(GROUP_ELEMENT_TYPE.PROV, _
                                                             GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)) _
                              , GetSufix(TableNumber & "." & _day.ToString, GLB_NLS_GUI_CONTROLS.GetString(467)))
        ' Zones list
        Call Auditor.SetField(0, GLB_NLS_GUI_CONTROLS.GetString(479) & ": " & _
                              _terminal_list.AuditStringList(GROUP_ELEMENT_TYPE.ZONE, _
                                                             GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)) _
                              , GetSufix(TableNumber & "." & _day.ToString, GLB_NLS_GUI_CONTROLS.GetString(468)))
        ' Areas list
        Call Auditor.SetField(0, GLB_NLS_GUI_CONTROLS.GetString(480) & ": " & _
                              _terminal_list.AuditStringList(GROUP_ELEMENT_TYPE.AREA, _
                                                             GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)) _
                              , GetSufix(TableNumber & "." & _day.ToString, GLB_NLS_GUI_CONTROLS.GetString(469)))
        ' Banks list
        Call Auditor.SetField(0, GLB_NLS_GUI_CONTROLS.GetString(481) & ": " & _
                              _terminal_list.AuditStringList(GROUP_ELEMENT_TYPE.BANK, _
                                                             GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)) _
                              , GetSufix(TableNumber & "." & _day.ToString, GLB_NLS_GUI_CONTROLS.GetString(470)))
        ' Terminals list
        Call Auditor.SetField(0, GLB_NLS_GUI_CONTROLS.GetString(483) & ": " & _
                                   _terminal_list.AuditStringList(GROUP_ELEMENT_TYPE.TERM, _
                                                                GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)) _
                              , GetSufix(TableNumber & "." & _day.ToString, GLB_NLS_GUI_CONTROLS.GetString(464)))
      Next

    End Sub ' AuditorTerminalList

    ' PURPOSE: Audits the class Gifts datatable
    '
    '    - INPUT:
    '       - TableNumber: Number of the tabpage that is being audited
    '       - Auditor: Auditor object.
    '
    '    - OUTPUT:
    '
    ' RETURNS:
    '    - 
    Private Sub AuditorGiftList(ByVal TableNumber As String, ByRef Auditor As CLASS_AUDITOR_DATA)
      Dim _idx_row As Int64
      Dim _idx_blanks As Int64
      Dim _sb As StringBuilder
      Dim _separator As String = "/"
      Dim _row As DataRow
      Dim _dv As DataView
      Dim _dt As DataTable = New DataTable
      Dim _yes_text As String
      Dim _no_text As String

      _yes_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
      _no_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)

      If Not Me.Gift_Table Is Nothing Then
        _dt = Me.Gift_Table.Copy
        _dt.Columns.Add(GIFT_COLUMN_ACTION_NAME, Type.GetType("System.Int16"))

        For Each _row In _dt.Rows
          If _row.RowState = DataRowState.Deleted Then
            _row.RejectChanges()
            _row(GIFT_COLUMN_ACTION_NAME) = ROWACTION.DELETED
          Else
            _row(GIFT_COLUMN_ACTION_NAME) = ROWACTION.NORMAL
          End If
        Next

        _dv = _dt.DefaultView
        _dv.Sort = "GIFT_ID" & " DESC"
        _dt = _dv.ToTable()
      End If

      _sb = New StringBuilder
      _idx_row = 0
      For Each _row In _dt.Rows
        _sb.Length = 0
        If _row(GIFT_COLUMN_ACTION_NAME) = 1 Then
          Call Auditor.SetField(0, AUDIT_NONE_STRING, GetSufix(TableNumber, _idx_row.ToString))
          _idx_row = _idx_row + 1
          Continue For
        End If
        ' 29-OCT-2013  JPJ    Defect WIG-338 Points don't have to contain decimals.
        If Not _row.Item(GIFT_COLUMN_CREDITS) Is DBNull.Value Then
          _sb.Append(GUI_FormatCurrency(_row.Item(GIFT_COLUMN_CREDITS)))
        End If
        _sb.Append(" ")

        If Not _row.Item(GIFT_COLUMN_LEVEL1) Is DBNull.Value Then
          _sb.Append(GUI_FormatNumber(_row.Item(GIFT_COLUMN_LEVEL1), 0))
        Else
          _sb.Append(AUDIT_NONE_STRING)
        End If
        _sb.Append(_separator)

        If Not _row.Item(GIFT_COLUMN_LEVEL2) Is DBNull.Value Then
          _sb.Append(GUI_FormatNumber(_row.Item(GIFT_COLUMN_LEVEL2), 0))
        Else
          _sb.Append(AUDIT_NONE_STRING)
        End If
        _sb.Append(_separator)

        If Not _row.Item(GIFT_COLUMN_LEVEL3) Is DBNull.Value Then
          _sb.Append(GUI_FormatNumber(_row.Item(GIFT_COLUMN_LEVEL3), 0))
        Else
          _sb.Append(AUDIT_NONE_STRING)
        End If
        _sb.Append(_separator)

        If Not _row.Item(GIFT_COLUMN_LEVEL4) Is DBNull.Value Then
          _sb.Append(GUI_FormatNumber(_row.Item(GIFT_COLUMN_LEVEL4), 0))
        Else
          _sb.Append(AUDIT_NONE_STRING)
        End If
        _sb.Append(_separator)

        If Not _row.Item(GIFT_COLUMN_IS_VIP) Is DBNull.Value AndAlso CBool(_row.Item(GIFT_COLUMN_IS_VIP)) Then
          _sb.Append(_yes_text)
        Else
          _sb.Append(_no_text)
        End If

        Call Auditor.SetField(0, _sb.ToString, GetSufix(TableNumber, _idx_row.ToString))

        _idx_row = _idx_row + 1
      Next

      For _idx_blanks = _idx_row To GIFT_NUM_ROWS
        Call Auditor.SetField(0, AUDIT_NONE_STRING, GetSufix(TableNumber, _idx_row.ToString))
      Next

    End Sub ' AuditorGiftList

#End Region 'Public Sub

    Public Sub New()

    End Sub
  End Class ' TYPE_POINTS_TO_CREDITS

#End Region 'Structures

#Region " Constants "

  ' Grid Columns by name
  Private Const GIFT_COLUMN_LEVEL1 As String = "LEVEL1"
  Private Const GIFT_COLUMN_LEVEL2 As String = "LEVEL2"
  Private Const GIFT_COLUMN_LEVEL3 As String = "LEVEL3"
  Private Const GIFT_COLUMN_LEVEL4 As String = "LEVEL4"
  Private Const GIFT_COLUMN_IS_VIP As String = "IS_VIP"
  Private Const GIFT_COLUMN_CREDITS As String = "CREDITS"
  Private Const GIFT_COLUMN_GIFT_ID As String = "GIFT_ID"

#End Region 'Constants

#Region " Enum "

  Enum POINT_MEMBERS
    PTC_POINTS_TO_CREDITS_ID = 0
    PTC_NAME = 1
    PTC_DESCRIPTION = 2
    PTC_ENABLED = 3
    PTC_CREDIT_TYPE = 4
    PTC_UPDATED = 5
    PTC_SCHEDULE_WEEKDAY = 6
    PTC_SCHEDULE1_TIME_FROM = 7
    PTC_SCHEDULE1_TIME_TO = 8
    PTC_SCHEDULE2_ENABLED = 9
    PTC_SCHEDULE2_TIME_FROM = 10
    PTC_SCHEDULE2_TIME_TO = 11
    PTC_RESTRICTED_TO_TERMINAL_LIST_0 = 12
    PTC_RESTRICTED_TO_TERMINAL_LIST_1 = 13
    PTC_RESTRICTED_TO_TERMINAL_LIST_2 = 14
    PTC_RESTRICTED_TO_TERMINAL_LIST_3 = 15
    PTC_RESTRICTED_TO_TERMINAL_LIST_4 = 16
    PTC_RESTRICTED_TO_TERMINAL_LIST_5 = 17
    PTC_RESTRICTED_TO_TERMINAL_LIST_6 = 18
    PTC_NAME_PREFIX = 19
    PM_PROMOTION_ID = 20
    PM_PROMOGAME_ID = 21
  End Enum

#End Region 'Enum

#Region " Members "

  Private m_points_list As List(Of TYPE_POINTS_TO_CREDITS) = New List(Of TYPE_POINTS_TO_CREDITS)

#End Region 'Members

#Region " Properties "

  Public Property PointsToCreaditsList() As List(Of TYPE_POINTS_TO_CREDITS)
    Set(ByVal value As List(Of TYPE_POINTS_TO_CREDITS))
      m_points_list = value
    End Set
    Get
      Return m_points_list
    End Get
  End Property

#End Region 'Properties

#Region " Overrides "

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns the related auditor data for the current object.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created audit object
  '
  ' NOTES :
  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA
    Dim _auditor As CLASS_AUDITOR_DATA
    Dim _idx_table As Integer

    _auditor = New CLASS_AUDITOR_DATA(AUDIT_CODE_GIFTS)

    For _idx_table = 0 To Me.PointsToCreaditsList.Count - 1
      Call Me.PointsToCreaditsList(_idx_table).AuditPointsToCredit(_idx_table.ToString, _auditor)
    Next

    Return _auditor

  End Function 'AuditorData

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes the given object from the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DEPENDENCIES
  '
  ' NOTES :
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As Integer

    ' Delete points to credit data
    _rc = DeletePointsToCredit(Me.PointsToCreaditsList, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DEPENDENCIES
        Return ENUM_STATUS.STATUS_DEPENDENCIES

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function 'DB_Delete

  '----------------------------------------------------------------------------
  ' PURPOSE : Inserts the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As Integer

    ' Insert points to credit
    _rc = InsertPointsToCredits(Me.PointsToCreaditsList, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function 'DB_Insert

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads from the database into the given object
  '
  ' PARAMS :
  '     - INPUT :
  '         - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '
  ' NOTES :
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As Integer

    ' Read gift data
    _rc = ReadPointsToCredit()

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function 'DB_Read

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As Integer

    ' Update points to credit
    _rc = UpdatePointsToCredits(Me.PointsToCreaditsList, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function 'DB_Update

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns a duplicate of the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function Duplicate() As CLASS_BASE
    Dim _clone As CLASS_POINTS_TO_CREDITS
    Dim _item_cloned As TYPE_POINTS_TO_CREDITS

    _clone = New CLASS_POINTS_TO_CREDITS()

    _clone.PointsToCreaditsList = New List(Of TYPE_POINTS_TO_CREDITS)

    For Each _item As TYPE_POINTS_TO_CREDITS In Me.PointsToCreaditsList
      _item_cloned = _item.Clone()
      _clone.PointsToCreaditsList.Add(_item_cloned)
    Next

    Return _clone

  End Function ' Duplicate

#End Region 'Overrides

#Region " Private Functions "

  '----------------------------------------------------------------------------
  ' PURPOSE: Inserts the given object list to the database.
  '
  ' PARAMS:
  '   - INPUT: 
  '       - Items: List of objects that have to be treated
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES:
  Private Function InsertPointsToCredits(ByVal Items As List(Of TYPE_POINTS_TO_CREDITS), _
                                         ByVal Context As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Inserts the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: 
  '       - Items: List of objects that have to be treated
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES:
  Private Function InsertPointToCredit(ByVal Item As TYPE_POINTS_TO_CREDITS, _
                                       ByVal Trx As SqlTransaction, _
                                       ByRef PointsToCreditId As Int64) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _obj As Object
    Dim _sb As StringBuilder
    Dim _xml As String = Nothing
    Dim _new_id As SqlParameter

    Try

      _sb = New StringBuilder()
      _sb.AppendLine("SELECT PTC_POINTS_TO_CREDITS_ID FROM POINTS_TO_CREDITS WHERE PTC_POINTS_TO_CREDITS_ID = @pPointsId")
      Using _sql_cmd As New SqlCommand(_sb.ToString, Trx.Connection, Trx)

        _sql_cmd.Parameters.Add("@pPointsId", SqlDbType.BigInt).Value = Item.Points_Id
        _obj = _sql_cmd.ExecuteScalar()

        If Not _obj Is Nothing Then

          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        End If

      End Using

      _sb.Length = 0
      _sb.AppendLine("INSERT INTO POINTS_TO_CREDITS")
      _sb.AppendLine("            ( PTC_NAME")
      _sb.AppendLine("            , PTC_DESCRIPTION")
      _sb.AppendLine("            , PTC_ENABLED")
      _sb.AppendLine("            , PTC_CREDIT_TYPE")
      _sb.AppendLine("            , PTC_UPDATED")
      _sb.AppendLine("            , PTC_SCHEDULE_WEEKDAY")
      _sb.AppendLine("            , PTC_SCHEDULE1_TIME_FROM")
      _sb.AppendLine("            , PTC_SCHEDULE1_TIME_TO")
      _sb.AppendLine("            , PTC_SCHEDULE2_ENABLED")
      _sb.AppendLine("            , PTC_SCHEDULE2_TIME_FROM")
      _sb.AppendLine("            , PTC_SCHEDULE2_TIME_TO")
      _sb.AppendLine("            , PTC_RESTRICTED_TO_TERMINAL_LIST_0")
      _sb.AppendLine("            , PTC_RESTRICTED_TO_TERMINAL_LIST_1")
      _sb.AppendLine("            , PTC_RESTRICTED_TO_TERMINAL_LIST_2")
      _sb.AppendLine("            , PTC_RESTRICTED_TO_TERMINAL_LIST_3")
      _sb.AppendLine("            , PTC_RESTRICTED_TO_TERMINAL_LIST_4")
      _sb.AppendLine("            , PTC_RESTRICTED_TO_TERMINAL_LIST_5")
      _sb.AppendLine("            , PTC_RESTRICTED_TO_TERMINAL_LIST_6")
      _sb.AppendLine("            , PTC_NAME_PREFIX)")
      _sb.AppendLine("    VALUES (  @pName")
      _sb.AppendLine("            , @pDescription")
      _sb.AppendLine("            , @pEnabled")
      _sb.AppendLine("            , @pType")
      _sb.AppendLine("            , @pUpdate")
      _sb.AppendLine("            , @pScheduleWeekday")
      _sb.AppendLine("            , @pScheduleTime1From")
      _sb.AppendLine("            , @pScheduleTime1To")
      _sb.AppendLine("            , @pScheduleTime2Enabled")
      _sb.AppendLine("            , @pScheduleTime2From")
      _sb.AppendLine("            , @pScheduleTime2To")
      _sb.AppendLine("            , @pTerminalList0")
      _sb.AppendLine("            , @pTerminalList1")
      _sb.AppendLine("            , @pTerminalList2")
      _sb.AppendLine("            , @pTerminalList3")
      _sb.AppendLine("            , @pTerminalList4")
      _sb.AppendLine("            , @pTerminalList5")
      _sb.AppendLine("            , @pTerminalList6")
      _sb.AppendLine("            , @pSufix);")
      _sb.AppendLine("SET @pAdvId = SCOPE_IDENTITY();")

      Using _sql_cmd As New SqlCommand(_sb.ToString, Trx.Connection, Trx)

        _sql_cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = Item.Points_Name
        _sql_cmd.Parameters.Add("@pDescription", SqlDbType.NVarChar).Value = Item.Points_Description
        _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).Value = Item.Points_Credit_Type
        _sql_cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).Value = Item.Points_Enabled
        _sql_cmd.Parameters.Add("@pUpdate", SqlDbType.DateTime).Value = WSI.Common.WGDB.Now '_item.Points_Update
        _sql_cmd.Parameters.Add("@pScheduleWeekday", SqlDbType.Int).Value = Item.Points_Schedule_Weekday
        _sql_cmd.Parameters.Add("@pScheduleTime1From", SqlDbType.Int).Value = Item.Points_Schedule_Time_From1
        _sql_cmd.Parameters.Add("@pScheduleTime1To", SqlDbType.Int).Value = Item.Points_Schedule_Time_To1
        _sql_cmd.Parameters.Add("@pScheduleTime2Enabled", SqlDbType.Bit).Value = Item.Points_Schedule2_Enabled
        _sql_cmd.Parameters.Add("@pScheduleTime2From", SqlDbType.Int).Value = Item.Points_Schedule_Time_From2
        _sql_cmd.Parameters.Add("@pScheduleTime2To", SqlDbType.Int).Value = Item.Points_Schedule_Time_To2

        _xml = Item.Restricted_To_Terminal0.ToXml
        If Not _xml Is Nothing Then
          _sql_cmd.Parameters.Add("@pTerminalList0", SqlDbType.Xml).Value = _xml
        Else
          _sql_cmd.Parameters.Add("@pTerminalList0", SqlDbType.Xml).Value = DBNull.Value
        End If

        _xml = Item.Restricted_To_Terminal1.ToXml
        If Not _xml Is Nothing Then
          _sql_cmd.Parameters.Add("@pTerminalList1", SqlDbType.Xml).Value = _xml
        Else
          _sql_cmd.Parameters.Add("@pTerminalList1", SqlDbType.Xml).Value = DBNull.Value
        End If

        _xml = Item.Restricted_To_Terminal2.ToXml
        If Not _xml Is Nothing Then
          _sql_cmd.Parameters.Add("@pTerminalList2", SqlDbType.Xml).Value = _xml
        Else
          _sql_cmd.Parameters.Add("@pTerminalList2", SqlDbType.Xml).Value = DBNull.Value
        End If

        _xml = Item.Restricted_To_Terminal3.ToXml
        If Not _xml Is Nothing Then
          _sql_cmd.Parameters.Add("@pTerminalList3", SqlDbType.Xml).Value = _xml
        Else
          _sql_cmd.Parameters.Add("@pTerminalList3", SqlDbType.Xml).Value = DBNull.Value
        End If

        _xml = Item.Restricted_To_Terminal4.ToXml
        If Not _xml Is Nothing Then
          _sql_cmd.Parameters.Add("@pTerminalList4", SqlDbType.Xml).Value = _xml
        Else
          _sql_cmd.Parameters.Add("@pTerminalList4", SqlDbType.Xml).Value = DBNull.Value
        End If

        _xml = Item.Restricted_To_Terminal5.ToXml
        If Not _xml Is Nothing Then
          _sql_cmd.Parameters.Add("@pTerminalList5", SqlDbType.Xml).Value = _xml
        Else
          _sql_cmd.Parameters.Add("@pTerminalList5", SqlDbType.Xml).Value = DBNull.Value
        End If

        _xml = Item.Restricted_To_Terminal6.ToXml
        If Not _xml Is Nothing Then
          _sql_cmd.Parameters.Add("@pTerminalList6", SqlDbType.Xml).Value = _xml
        Else
          _sql_cmd.Parameters.Add("@pTerminalList6", SqlDbType.Xml).Value = DBNull.Value
        End If

        If Not String.IsNullOrEmpty(Item.Points_Sufix) Then
          _sql_cmd.Parameters.Add("@pSufix", SqlDbType.NVarChar).Value = Item.Points_Sufix
        Else
          _sql_cmd.Parameters.Add("@pSufix", SqlDbType.NVarChar).Value = DBNull.Value
        End If

        _new_id = _sql_cmd.Parameters.Add("@pAdvId", SqlDbType.BigInt)
        _new_id.Direction = ParameterDirection.Output

        If _sql_cmd.ExecuteNonQuery() <> 1 Or IsDBNull(_new_id.Value) Then

          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        Else

          PointsToCreditId = _new_id.Value
        End If

      End Using

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Catch ex As Exception

      Log.Exception(ex)
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

  End Function ' InsertPointsToCredit

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads an object from the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES:
  Private Function ReadPointsToCredit() As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _sb As New StringBuilder
    Dim _sql_trx As SqlClient.SqlTransaction
    Dim _item As TYPE_POINTS_TO_CREDITS

    _sb.AppendLine("SELECT         PTC_POINTS_TO_CREDITS_ID                                                  ")
    _sb.AppendLine("             , PTC_NAME                                                                  ")
    _sb.AppendLine("             , PTC_DESCRIPTION                                                           ")
    _sb.AppendLine("             , PTC_ENABLED                                                               ")
    _sb.AppendLine("             , PTC_CREDIT_TYPE                                                           ")
    _sb.AppendLine("             , PTC_UPDATED                                                               ")
    _sb.AppendLine("             , PTC_SCHEDULE_WEEKDAY                                                      ")
    _sb.AppendLine("             , PTC_SCHEDULE1_TIME_FROM                                                   ")
    _sb.AppendLine("             , PTC_SCHEDULE1_TIME_TO                                                     ")
    _sb.AppendLine("             , PTC_SCHEDULE2_ENABLED                                                     ")
    _sb.AppendLine("             , PTC_SCHEDULE2_TIME_FROM                                                   ")
    _sb.AppendLine("             , PTC_SCHEDULE2_TIME_TO                                                     ")
    _sb.AppendLine("             , PTC_RESTRICTED_TO_TERMINAL_LIST_0                                         ")
    _sb.AppendLine("             , PTC_RESTRICTED_TO_TERMINAL_LIST_1                                         ")
    _sb.AppendLine("             , PTC_RESTRICTED_TO_TERMINAL_LIST_2                                         ")
    _sb.AppendLine("             , PTC_RESTRICTED_TO_TERMINAL_LIST_3                                         ")
    _sb.AppendLine("             , PTC_RESTRICTED_TO_TERMINAL_LIST_4                                         ")
    _sb.AppendLine("             , PTC_RESTRICTED_TO_TERMINAL_LIST_5                                         ")
    _sb.AppendLine("             , PTC_RESTRICTED_TO_TERMINAL_LIST_6                                         ")
    _sb.AppendLine("             , PTC_NAME_PREFIX                                                           ")
    _sb.AppendLine("             , PM.PM_PROMOTION_ID                                                        ")
    _sb.AppendLine("             , PM.PM_PROMOGAME_ID                                                        ")
    _sb.AppendLine("       FROM    POINTS_TO_CREDITS                                                         ")
    _sb.AppendLine(" INNER JOIN    PROMOTIONS AS PM ON PM.PM_POINTS_TO_CREDITS_ID = PTC_POINTS_TO_CREDITS_ID ")
    _sb.AppendLine("   ORDER BY    PTC_ENABLED DESC                                                          ")
    _sb.AppendLine("             , PTC_NAME                                                                  ")

    Try

      Using _db_trx As New DB_TRX()

        _sql_trx = _db_trx.SqlTransaction

        Using _sql_cmd As New SqlCommand(_sb.ToString, _sql_trx.Connection, _sql_trx)

          Using _reader As SqlDataReader = _sql_cmd.ExecuteReader()

            While _reader.Read()
              _item = New TYPE_POINTS_TO_CREDITS()
              _item.Points_Id = _reader.GetInt64(POINT_MEMBERS.PTC_POINTS_TO_CREDITS_ID)
              _item.Points_Name = _reader(POINT_MEMBERS.PTC_NAME)
              If Not _reader.IsDBNull(POINT_MEMBERS.PTC_DESCRIPTION) Then
                _item.Points_Description = _reader(POINT_MEMBERS.PTC_DESCRIPTION)
              End If
              _item.Points_Enabled = _reader(POINT_MEMBERS.PTC_ENABLED)
              _item.Points_Credit_Type = _reader(POINT_MEMBERS.PTC_CREDIT_TYPE)
              _item.Points_Update = _reader(POINT_MEMBERS.PTC_UPDATED)
              _item.Points_Schedule_Weekday = _reader(POINT_MEMBERS.PTC_SCHEDULE_WEEKDAY)
              _item.Points_Schedule_Time_From1 = _reader(POINT_MEMBERS.PTC_SCHEDULE1_TIME_FROM)
              _item.Points_Schedule_Time_To1 = _reader(POINT_MEMBERS.PTC_SCHEDULE1_TIME_TO)
              _item.Points_Schedule2_Enabled = _reader(POINT_MEMBERS.PTC_SCHEDULE2_ENABLED)

              If Not _reader.IsDBNull(POINT_MEMBERS.PTC_SCHEDULE2_TIME_FROM) Then
                _item.Points_Schedule_Time_From2 = _reader(POINT_MEMBERS.PTC_SCHEDULE2_TIME_FROM)
              End If

              If Not _reader.IsDBNull(POINT_MEMBERS.PTC_SCHEDULE2_TIME_TO) Then
                _item.Points_Schedule_Time_To2 = _reader(POINT_MEMBERS.PTC_SCHEDULE2_TIME_TO)
              End If

              _item.Restricted_To_Terminal0 = New TerminalList()
              If Not _reader.IsDBNull(POINT_MEMBERS.PTC_RESTRICTED_TO_TERMINAL_LIST_0) Then
                _item.Restricted_To_Terminal0.FromXml(_reader(POINT_MEMBERS.PTC_RESTRICTED_TO_TERMINAL_LIST_0))
              End If
              _item.Restricted_To_Terminal1 = New TerminalList()
              If Not _reader.IsDBNull(POINT_MEMBERS.PTC_RESTRICTED_TO_TERMINAL_LIST_1) Then
                _item.Restricted_To_Terminal1.FromXml(_reader(POINT_MEMBERS.PTC_RESTRICTED_TO_TERMINAL_LIST_1))
              End If
              _item.Restricted_To_Terminal2 = New TerminalList()
              If Not _reader.IsDBNull(POINT_MEMBERS.PTC_RESTRICTED_TO_TERMINAL_LIST_2) Then
                _item.Restricted_To_Terminal2.FromXml(_reader(POINT_MEMBERS.PTC_RESTRICTED_TO_TERMINAL_LIST_2))
              End If
              _item.Restricted_To_Terminal3 = New TerminalList()
              If Not _reader.IsDBNull(POINT_MEMBERS.PTC_RESTRICTED_TO_TERMINAL_LIST_3) Then
                _item.Restricted_To_Terminal3.FromXml(_reader(POINT_MEMBERS.PTC_RESTRICTED_TO_TERMINAL_LIST_3))
              End If
              _item.Restricted_To_Terminal4 = New TerminalList()
              If Not _reader.IsDBNull(POINT_MEMBERS.PTC_RESTRICTED_TO_TERMINAL_LIST_4) Then
                _item.Restricted_To_Terminal4.FromXml(_reader(POINT_MEMBERS.PTC_RESTRICTED_TO_TERMINAL_LIST_4))
              End If
              _item.Restricted_To_Terminal5 = New TerminalList()
              If Not _reader.IsDBNull(POINT_MEMBERS.PTC_RESTRICTED_TO_TERMINAL_LIST_5) Then
                _item.Restricted_To_Terminal5.FromXml(_reader(POINT_MEMBERS.PTC_RESTRICTED_TO_TERMINAL_LIST_5))
              End If
              _item.Restricted_To_Terminal6 = New TerminalList()
              If Not _reader.IsDBNull(POINT_MEMBERS.PTC_RESTRICTED_TO_TERMINAL_LIST_6) Then
                _item.Restricted_To_Terminal6.FromXml(_reader(POINT_MEMBERS.PTC_RESTRICTED_TO_TERMINAL_LIST_6))
              End If
              If Not _reader.IsDBNull(POINT_MEMBERS.PTC_NAME_PREFIX) Then
                _item.Points_Sufix = _reader(POINT_MEMBERS.PTC_NAME_PREFIX)
              End If
              If Not _reader.IsDBNull(POINT_MEMBERS.PM_PROMOTION_ID) Then
                _item.Promotion_Id = _reader.GetInt64(POINT_MEMBERS.PM_PROMOTION_ID)
              End If
              If Not _reader.IsDBNull(POINT_MEMBERS.PM_PROMOGAME_ID) Then
                _item.AwardGame = _reader.GetInt64(POINT_MEMBERS.PM_PROMOGAME_ID)
              End If

              Using _db_trx2 As New DB_TRX()
                Dim _data As DataTable = New DataTable

                _data = GetPointsToCreditGifts(_item.Points_Id, _db_trx2)
                _item.Gift_Table = _data
              End Using

              Me.PointsToCreaditsList.Add(_item)

            End While

            _reader.Close()

          End Using

        End Using

      End Using

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Catch ex As Exception

      Log.Exception(ex)
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

  End Function ' ReadPointsToCredit

  '----------------------------------------------------------------------------
  ' PURPOSE: Delete an object from the database.
  '
  ' PARAMS:
  '   - INPUT: 
  '     - List of objects that have to be treatedNone
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES:
  Private Function DeletePointsToCredit(ByVal Items As List(Of TYPE_POINTS_TO_CREDITS), _
                                        ByVal Context As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _sql_query As String
    Dim _item As TYPE_POINTS_TO_CREDITS
    Dim _rc As Integer

    Try

      Using _db_trx As New WSI.Common.DB_TRX()

        For Each _item In Items

          _sql_query = "DELETE POINTS_TO_CREDITS WHERE GR_ID = @pPointsId"

          Using _sql_cmd As New SqlCommand(_sql_query, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

            _sql_cmd.Parameters.Add("@pPointsId", SqlDbType.BigInt).Value = _item.Points_Id

            _rc = _sql_cmd.ExecuteNonQuery()
            If _rc = 0 Then

              Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND

            ElseIf _rc > 1 Then

              Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

            End If

          End Using

        Next

        _db_trx.Commit()
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

      End Using

    Catch ex As Exception

      Log.Exception(ex)
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

  End Function ' DeletePointsToCredit

  '----------------------------------------------------------------------------
  ' PURPOSE: Updates the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: 
  '       - Items: List of objects that have to be treated
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES:
  Private Function UpdatePointsToCredits(ByVal Items As List(Of TYPE_POINTS_TO_CREDITS), _
                                         ByVal Context As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _item As TYPE_POINTS_TO_CREDITS
    Dim _gift_item As CLASS_GIFT.TYPE_GIFT
    Dim _promo_item As CLASS_PROMOTION.TYPE_PROMOTION
    Dim _points_id As Int64

    Try

      Using _db_trx As New WSI.Common.DB_TRX()

        For Each _item In Items

          If _item.Points_Id = 0 Then
            If InsertPointToCredit(_item, _db_trx.SqlTransaction, _points_id) <> ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then

              Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
            End If
          Else
            _points_id = _item.Points_Id
            If UpdatePointToCredit(_item, _db_trx.SqlTransaction) <> ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then

              Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
            End If
          End If

          'Gifts
          For Each _row As DataRow In _item.Gift_Table.Rows
            If _row.RowState = DataRowState.Deleted Then
              'We delete the gift
              If DeleteGift(_row(GIFT_COLUMN_GIFT_ID, DataRowVersion.Original), _db_trx.SqlTransaction) <> ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then

                Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
              End If
            Else
              'We prepare the gift
              _gift_item = PrepareGiftInformation(_item, _row)
              If _gift_item.gift_id = 0 Then
                'We Add a new gift
                If InsertGift(_gift_item, _points_id, _db_trx.SqlTransaction) <> ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then

                  Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
                End If
              Else
                'We update the gift
                If UpdateGift(_gift_item, _points_id, _db_trx.SqlTransaction) <> ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then

                  Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
                End If
              End If
            End If
          Next
          _gift_item = Nothing

          'Promotion
          _promo_item = PreparePromotionInformation(_item)
          If _promo_item.promotion_id = 0 Then
            If InsertPromotion(_promo_item, _points_id, _db_trx.SqlTransaction) <> ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then

              Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
            End If
          Else
            If UpdatePromotion(_promo_item, _points_id, _db_trx.SqlTransaction) <> ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then

              Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
            End If
          End If
        Next

        _db_trx.Commit()
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

      End Using

    Catch ex As Exception

      Log.Exception(ex)
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

  End Function ' UpdatePointsToCredits

  '----------------------------------------------------------------------------
  ' PURPOSE: Updates the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: 
  '       - Item: Object that has to be treated
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES:
  Private Function UpdatePointToCredit(ByVal Item As TYPE_POINTS_TO_CREDITS, _
                                       ByVal Trx As SqlTransaction) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _sb As StringBuilder
    Dim _xml As String = Nothing
    Dim _rc As Integer

    Try

      _sb = New StringBuilder
      _sb.AppendLine("UPDATE   POINTS_TO_CREDITS")
      _sb.AppendLine("   SET   PTC_NAME                            = @pName")
      _sb.AppendLine("       , PTC_DESCRIPTION                     = @pDescription")
      _sb.AppendLine("       , PTC_ENABLED                         = @pEnabled")
      _sb.AppendLine("       , PTC_CREDIT_TYPE                     = @pType")
      _sb.AppendLine("       , PTC_UPDATED                         = @pUpdate")
      _sb.AppendLine("       , PTC_SCHEDULE_WEEKDAY                = @pScheduleWeekday")
      _sb.AppendLine("       , PTC_SCHEDULE1_TIME_FROM             = @pScheduleTime1From")
      _sb.AppendLine("       , PTC_SCHEDULE1_TIME_TO               = @pScheduleTime1To")
      _sb.AppendLine("       , PTC_SCHEDULE2_ENABLED               = @pScheduleTime2Enabled")
      _sb.AppendLine("       , PTC_SCHEDULE2_TIME_FROM             = @pScheduleTime2From")
      _sb.AppendLine("       , PTC_SCHEDULE2_TIME_TO               = @pScheduleTime2To")
      _sb.AppendLine("       , PTC_RESTRICTED_TO_TERMINAL_LIST_0   = @pTerminalList0")
      _sb.AppendLine("       , PTC_RESTRICTED_TO_TERMINAL_LIST_1   = @pTerminalList1")
      _sb.AppendLine("       , PTC_RESTRICTED_TO_TERMINAL_LIST_2   = @pTerminalList2")
      _sb.AppendLine("       , PTC_RESTRICTED_TO_TERMINAL_LIST_3   = @pTerminalList3")
      _sb.AppendLine("       , PTC_RESTRICTED_TO_TERMINAL_LIST_4   = @pTerminalList4")
      _sb.AppendLine("       , PTC_RESTRICTED_TO_TERMINAL_LIST_5   = @pTerminalList5")
      _sb.AppendLine("       , PTC_RESTRICTED_TO_TERMINAL_LIST_6   = @pTerminalList6")
      _sb.AppendLine("       , PTC_NAME_PREFIX                     = @pSufix")
      _sb.AppendLine(" WHERE   PTC_POINTS_TO_CREDITS_ID            = @pPointsId ")
      ' Update the points to credits
      Using _sql_cmd As New SqlCommand(_sb.ToString, Trx.Connection, Trx)

        _sql_cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = Item.Points_Name

        If Not String.IsNullOrEmpty(Item.Points_Description) Then
          _sql_cmd.Parameters.Add("@pDescription", SqlDbType.NVarChar).Value = Item.Points_Description
        Else
          _sql_cmd.Parameters.Add("@pDescription", SqlDbType.NVarChar).Value = DBNull.Value
        End If
        _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).Value = Item.Points_Credit_Type
        _sql_cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).Value = Item.Points_Enabled
        _sql_cmd.Parameters.Add("@pUpdate", SqlDbType.DateTime).Value = WSI.Common.WGDB.Now 'Item.Points_Update
        _sql_cmd.Parameters.Add("@pScheduleWeekday", SqlDbType.Int).Value = Item.Points_Schedule_Weekday
        _sql_cmd.Parameters.Add("@pScheduleTime1From", SqlDbType.Int).Value = Item.Points_Schedule_Time_From1
        _sql_cmd.Parameters.Add("@pScheduleTime1To", SqlDbType.Int).Value = Item.Points_Schedule_Time_To1
        _sql_cmd.Parameters.Add("@pScheduleTime2Enabled", SqlDbType.Bit).Value = Item.Points_Schedule2_Enabled
        _sql_cmd.Parameters.Add("@pScheduleTime2From", SqlDbType.Int).Value = Item.Points_Schedule_Time_From2
        _sql_cmd.Parameters.Add("@pScheduleTime2To", SqlDbType.Int).Value = Item.Points_Schedule_Time_To2

        _xml = Item.Restricted_To_Terminal0.ToXml
        If Not _xml Is Nothing Then
          _sql_cmd.Parameters.Add("@pTerminalList0", SqlDbType.Xml).Value = _xml
        Else
          _sql_cmd.Parameters.Add("@pTerminalList0", SqlDbType.Xml).Value = DBNull.Value
        End If

        _xml = Item.Restricted_To_Terminal1.ToXml
        If Not _xml Is Nothing Then
          _sql_cmd.Parameters.Add("@pTerminalList1", SqlDbType.Xml).Value = _xml
        Else
          _sql_cmd.Parameters.Add("@pTerminalList1", SqlDbType.Xml).Value = DBNull.Value
        End If

        _xml = Item.Restricted_To_Terminal2.ToXml
        If Not _xml Is Nothing Then
          _sql_cmd.Parameters.Add("@pTerminalList2", SqlDbType.Xml).Value = _xml
        Else
          _sql_cmd.Parameters.Add("@pTerminalList2", SqlDbType.Xml).Value = DBNull.Value
        End If

        _xml = Item.Restricted_To_Terminal3.ToXml
        If Not _xml Is Nothing Then
          _sql_cmd.Parameters.Add("@pTerminalList3", SqlDbType.Xml).Value = _xml
        Else
          _sql_cmd.Parameters.Add("@pTerminalList3", SqlDbType.Xml).Value = DBNull.Value
        End If

        _xml = Item.Restricted_To_Terminal4.ToXml
        If Not _xml Is Nothing Then
          _sql_cmd.Parameters.Add("@pTerminalList4", SqlDbType.Xml).Value = _xml
        Else
          _sql_cmd.Parameters.Add("@pTerminalList4", SqlDbType.Xml).Value = DBNull.Value
        End If

        _xml = Item.Restricted_To_Terminal5.ToXml
        If Not _xml Is Nothing Then
          _sql_cmd.Parameters.Add("@pTerminalList5", SqlDbType.Xml).Value = _xml
        Else
          _sql_cmd.Parameters.Add("@pTerminalList5", SqlDbType.Xml).Value = DBNull.Value
        End If

        _xml = Item.Restricted_To_Terminal6.ToXml
        If Not _xml Is Nothing Then
          _sql_cmd.Parameters.Add("@pTerminalList6", SqlDbType.Xml).Value = _xml
        Else
          _sql_cmd.Parameters.Add("@pTerminalList6", SqlDbType.Xml).Value = DBNull.Value
        End If

        If Not String.IsNullOrEmpty(Item.Points_Sufix) Then
          _sql_cmd.Parameters.Add("@pSufix", SqlDbType.NVarChar).Value = Item.Points_Sufix
        Else
          _sql_cmd.Parameters.Add("@pSufix", SqlDbType.NVarChar).Value = DBNull.Value
        End If

        _sql_cmd.Parameters.Add("@pPointsId", SqlDbType.BigInt).Value = Item.Points_Id

        _rc = _sql_cmd.ExecuteNonQuery()
        If _rc = 0 Then

          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND

        ElseIf _rc > 1 Then

          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

        End If

      End Using

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Catch ex As Exception

      Log.Exception(ex)
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

  End Function ' UpdatePointsToCredits

#Region " Gifts "

  '----------------------------------------------------------------------------
  ' PURPOSE: We generate the gift information.
  '
  ' PARAMS:
  '   - INPUT: 
  '       - Item: Object (Redemption table) that have to be treated
  '       - Row: Gift element.
  '   - OUTPUT: None
  '
  ' RETURNS:
  '   - New Gift object item
  '
  ' NOTES:
  Private Function PrepareGiftInformation(ByVal Item As TYPE_POINTS_TO_CREDITS, ByVal Row As DataRow) As CLASS_GIFT.TYPE_GIFT
    Dim _gift_item As CLASS_GIFT.TYPE_GIFT
    Dim _prefix As String

    _gift_item = New CLASS_GIFT.TYPE_GIFT

    If Not Row(GIFT_COLUMN_LEVEL1) Is DBNull.Value Then
      _gift_item.points_level1 = Row(GIFT_COLUMN_LEVEL1)
    Else
      _gift_item.points_level1 = -1
    End If
    If Not Row(GIFT_COLUMN_LEVEL2) Is DBNull.Value Then
      _gift_item.points_level2 = Row(GIFT_COLUMN_LEVEL2)
    Else
      _gift_item.points_level2 = -1
    End If
    If Not Row(GIFT_COLUMN_LEVEL3) Is DBNull.Value Then
      _gift_item.points_level3 = Row(GIFT_COLUMN_LEVEL3)
    Else
      _gift_item.points_level3 = -1
    End If
    If Not Row(GIFT_COLUMN_LEVEL4) Is DBNull.Value Then
      _gift_item.points_level4 = Row(GIFT_COLUMN_LEVEL4)
    Else
      _gift_item.points_level4 = -1
    End If
    If Not Row(GIFT_COLUMN_IS_VIP) Is DBNull.Value Then
      _gift_item.is_vip = Row(GIFT_COLUMN_IS_VIP)
    Else
      _gift_item.is_vip = False
    End If

    _gift_item.conversion_to_nrc = Row(GIFT_COLUMN_CREDITS)
    _gift_item.gift_id = Row(GIFT_COLUMN_GIFT_ID)

    Select Case Item.Points_Credit_Type
      Case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE
        _gift_item.type = CLASS_GIFT.ENUM_GIFT_TYPE.REDEEMABLE_CREDIT
        _prefix = GLB_NLS_GUI_PLAYER_TRACKING.GetString(310, GUI_FormatCurrency(Row(GIFT_COLUMN_CREDITS).ToString)) ' %1 Cr�dito Redimible
      Case Else
        _gift_item.type = CLASS_GIFT.ENUM_GIFT_TYPE.NOT_REDEEMABLE_CREDIT
        _prefix = GLB_NLS_GUI_PLAYER_TRACKING.GetString(345, GUI_FormatCurrency(Row(GIFT_COLUMN_CREDITS).ToString)) ' %1 Cr�dito No Redimible
    End Select

    If String.IsNullOrEmpty(Item.Points_Sufix) Then
      _gift_item.name = Item.Points_Name & " " & _prefix
    Else
      _gift_item.name = Item.Points_Sufix & " " & _prefix
    End If
    _gift_item.available = CLASS_GIFT.ENUM_GIFT_STATUS.NOT_AVAILABLE
    _gift_item.current_stock = 0
    _gift_item.request_counter = 0
    _gift_item.description = ""
    _gift_item.account_daily_limit = 0
    _gift_item.account_monthly_limit = 0
    _gift_item.global_daily_limit = 0
    _gift_item.global_monthly_limit = 0

    Return _gift_item

  End Function 'PrepareGiftInformation

  '----------------------------------------------------------------------------
  ' PURPOSE: Inserts the given object (Gift) to the database.
  '
  ' PARAMS:
  '   - INPUT: 
  '       - Item: Object (Gift) that has to be treated
  '   - OUTPUT: None
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES:
  Private Function InsertGift(ByVal Item As CLASS_GIFT.TYPE_GIFT, _
                              ByVal PointsToCreditId As Int64, _
                              ByVal Trx As SqlTransaction) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _sb As StringBuilder

    ' Steps
    '     - Check whether there is no other gift with the same name 
    '     - Insert the gift item to the GIFTS table
    Try
      _sb = New StringBuilder()

      _sb.AppendLine("INSERT INTO GIFTS  ( GI_NAME")
      _sb.AppendLine("                   , GI_TYPE")
      _sb.AppendLine("                   , GI_AVAILABLE")
      _sb.AppendLine("                   , GI_CONVERSION_TO_NRC")
      _sb.AppendLine("                   , GI_CURRENT_STOCK")
      _sb.AppendLine("                   , GI_REQUEST_COUNTER")
      _sb.AppendLine("                   , GI_DELIVERY_COUNTER")
      _sb.AppendLine("                   , GI_SMALL_RESOURCE_ID ")
      _sb.AppendLine("                   , GI_LARGE_RESOURCE_ID ")
      _sb.AppendLine("                   , GI_DESCRIPTION ")
      _sb.AppendLine("                   , GI_ACCOUNT_DAILY_LIMIT ")
      _sb.AppendLine("                   , GI_ACCOUNT_MONTHLY_LIMIT ")
      _sb.AppendLine("                   , GI_GLOBAL_DAILY_LIMIT ")
      _sb.AppendLine("                   , GI_GLOBAL_MONTHLY_LIMIT ")
      _sb.AppendLine("                   , GI_POINTS_LEVEL1 ")
      _sb.AppendLine("                   , GI_POINTS_LEVEL2 ")
      _sb.AppendLine("                   , GI_POINTS_LEVEL3 ")
      _sb.AppendLine("                   , GI_POINTS_LEVEL4 ")
      _sb.AppendLine("                   , GI_VIP ")
      _sb.AppendLine("                   , GI_POINTS_TO_CREDITS_ID )")
      _sb.AppendLine("                   VALUES ( @pName")
      _sb.AppendLine("                   , @pType")
      _sb.AppendLine("                   , @pAviable")
      _sb.AppendLine("                   , @pConversionToNrc")
      _sb.AppendLine("                   , @pCurrentStock ")
      _sb.AppendLine("                   , 0 ")
      _sb.AppendLine("                   , 0 ")
      _sb.AppendLine("                   , @pSmallResourceId ")
      _sb.AppendLine("                   , @pLargeResourceId ")
      _sb.AppendLine("                   , @pDescription ")
      _sb.AppendLine("                   , @pAccountDailyLimit ")
      _sb.AppendLine("                   , @pAccountMonthlyLimit ")
      _sb.AppendLine("                   , @pGlobalDailyLimit ")
      _sb.AppendLine("                   , @pGlobalMonthlyLimit ")
      _sb.AppendLine("                   , @pPointsLevel1 ")
      _sb.AppendLine("                   , @pPointsLevel2 ")
      _sb.AppendLine("                   , @pPointsLevel3 ")
      _sb.AppendLine("                   , @pPointsLevel4 ")
      _sb.AppendLine("                   , @pVip ")
      _sb.AppendLine("                   , @pPointsId ) ")

      Using _sql_cmd As New SqlCommand(_sb.ToString, Trx.Connection, Trx)
        _sql_cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = Item.name
        _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).Value = Item.type
        _sql_cmd.Parameters.Add("@pAviable", SqlDbType.Bit).Value = Item.available
        _sql_cmd.Parameters.Add("@pConversionToNrc", SqlDbType.Decimal).Value = Item.conversion_to_nrc
        _sql_cmd.Parameters.Add("@pCurrentStock", SqlDbType.Int).Value = Item.current_stock

        If Item.small_resource_id.HasValue Then
          _sql_cmd.Parameters.Add("@pSmallResourceId", SqlDbType.BigInt).Value = Item.small_resource_id
        Else
          _sql_cmd.Parameters.Add("@pSmallResourceId", SqlDbType.BigInt).Value = DBNull.Value
        End If

        If Item.large_resource_id.HasValue Then
          _sql_cmd.Parameters.Add("@pLargeResourceId", SqlDbType.BigInt).Value = Item.large_resource_id
        Else
          _sql_cmd.Parameters.Add("@pLargeResourceId", SqlDbType.BigInt).Value = DBNull.Value
        End If

        _sql_cmd.Parameters.Add("@pDescription", SqlDbType.NVarChar).Value = Item.description
        _sql_cmd.Parameters.Add("@pAccountDailyLimit", SqlDbType.Int).Value = Item.account_daily_limit
        _sql_cmd.Parameters.Add("@pAccountMonthlyLimit", SqlDbType.Int).Value = Item.account_monthly_limit
        _sql_cmd.Parameters.Add("@pGlobalDailyLimit", SqlDbType.Int).Value = Item.global_daily_limit
        _sql_cmd.Parameters.Add("@pGlobalMonthlyLimit", SqlDbType.Int).Value = Item.global_monthly_limit

        '''if points for level = -1 it means that the gift it's not available for that level in the DB it's saved as NULL
        If Item.points_level1 <> -1 Then
          _sql_cmd.Parameters.Add("@pPointsLevel1", SqlDbType.Money).Value = Item.points_level1
        Else
          _sql_cmd.Parameters.Add("@pPointsLevel1", SqlDbType.Money).Value = DBNull.Value
        End If

        If Item.points_level2 <> -1 Then
          _sql_cmd.Parameters.Add("@pPointsLevel2", SqlDbType.Money).Value = Item.points_level2
        Else
          _sql_cmd.Parameters.Add("@pPointsLevel2", SqlDbType.Money).Value = DBNull.Value
        End If

        If Item.points_level3 <> -1 Then
          _sql_cmd.Parameters.Add("@pPointsLevel3", SqlDbType.Money).Value = Item.points_level3
        Else
          _sql_cmd.Parameters.Add("@pPointsLevel3", SqlDbType.Money).Value = DBNull.Value
        End If

        If Item.points_level4 <> -1 Then
          _sql_cmd.Parameters.Add("@pPointsLevel4", SqlDbType.Money).Value = Item.points_level4
        Else
          _sql_cmd.Parameters.Add("@pPointsLevel4", SqlDbType.Money).Value = DBNull.Value
        End If

        _sql_cmd.Parameters.Add("@pVip", SqlDbType.BigInt).Value = Item.is_vip
        _sql_cmd.Parameters.Add("@pPointsId", SqlDbType.BigInt).Value = PointsToCreditId

        If _sql_cmd.ExecuteNonQuery() <> 1 Then

          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

      End Using

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Catch ex As Exception

      Log.Exception(ex)
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function ' InsertGift

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given gift object into the DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - Item
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :
  Private Function UpdateGift(ByVal Item As CLASS_GIFT.TYPE_GIFT, _
                              ByVal PointsToCreditId As Int64, _
                              ByVal Trx As SqlTransaction) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _sb As StringBuilder

    Try
      _sb = New StringBuilder()

      _sb.AppendLine("UPDATE   GIFTS")
      _sb.AppendLine("   SET   GI_NAME                   = @pName")
      _sb.AppendLine("       , GI_TYPE                   = @pType")
      _sb.AppendLine("       , GI_CONVERSION_TO_NRC      = @pConversionToNrc")
      _sb.AppendLine("       , GI_POINTS_LEVEL1          = @pPointsLevel1")
      _sb.AppendLine("       , GI_POINTS_LEVEL2          = @pPointsLevel2")
      _sb.AppendLine("       , GI_POINTS_LEVEL3          = @pPointsLevel3")
      _sb.AppendLine("       , GI_POINTS_LEVEL4          = @pPointsLevel4")
      _sb.AppendLine("       , GI_VIP                    = @pVip")
      _sb.AppendLine("       , GI_POINTS_TO_CREDITS_ID   = @pPointsId")
      _sb.AppendLine(" WHERE   GI_GIFT_ID                = @pGiftId")

      Using _sql_cmd As New SqlCommand(_sb.ToString, Trx.Connection, Trx)
        _sql_cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = Item.name
        _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).Value = Item.type
        _sql_cmd.Parameters.Add("@pConversionToNrc", SqlDbType.Decimal).Value = Item.conversion_to_nrc

        'if points for level = -1 it means that the gift it's not available for that level in the DB it's saved as NULL
        If Item.points_level1 <> -1 Then
          _sql_cmd.Parameters.Add("@pPointsLevel1", SqlDbType.Money).Value = Item.points_level1
        Else
          _sql_cmd.Parameters.Add("@pPointsLevel1", SqlDbType.Money).Value = DBNull.Value
        End If

        If Item.points_level2 <> -1 Then
          _sql_cmd.Parameters.Add("@pPointsLevel2", SqlDbType.Money).Value = Item.points_level2
        Else
          _sql_cmd.Parameters.Add("@pPointsLevel2", SqlDbType.Money).Value = DBNull.Value
        End If

        If Item.points_level3 <> -1 Then
          _sql_cmd.Parameters.Add("@pPointsLevel3", SqlDbType.Money).Value = Item.points_level3
        Else
          _sql_cmd.Parameters.Add("@pPointsLevel3", SqlDbType.Money).Value = DBNull.Value
        End If

        If Item.points_level4 <> -1 Then
          _sql_cmd.Parameters.Add("@pPointsLevel4", SqlDbType.Money).Value = Item.points_level4
        Else
          _sql_cmd.Parameters.Add("@pPointsLevel4", SqlDbType.Money).Value = DBNull.Value
        End If

        _sql_cmd.Parameters.Add("@pVip", SqlDbType.Bit).Value = Item.is_vip
        _sql_cmd.Parameters.Add("@pPointsId", SqlDbType.BigInt).Value = PointsToCreditId
        _sql_cmd.Parameters.Add("@pGiftId", SqlDbType.BigInt).Value = Item.gift_id

        If _sql_cmd.ExecuteNonQuery() <> 1 Then

          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

      End Using

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Catch ex As Exception

      Log.Exception(ex)
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function ' UpdateGift

  '----------------------------------------------------------------------------
  ' PURPOSE : Read object (points to credit) from the DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - PointsToCreditId: Points to credit primary key
  '         - DbTrx: Transaction
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - Datatable:
  '
  ' NOTES :
  Private Function GetPointsToCreditGifts(ByVal PointsToCreditId As Int64, _
                                          ByVal DbTrx As DB_TRX) As DataTable
    Dim _table As DataTable
    Dim _sb As StringBuilder

    _table = New DataTable()
    _sb = New StringBuilder()

    Try
      _sb.AppendLine("   SELECT    GI_POINTS_LEVEL1 AS " & GIFT_COLUMN_LEVEL1)
      _sb.AppendLine("           , GI_POINTS_LEVEL2 AS " & GIFT_COLUMN_LEVEL2)
      _sb.AppendLine("           , GI_POINTS_LEVEL3 AS " & GIFT_COLUMN_LEVEL3)
      _sb.AppendLine("           , GI_POINTS_LEVEL4 AS " & GIFT_COLUMN_LEVEL4)
      _sb.AppendLine("           , GI_VIP AS " & GIFT_COLUMN_IS_VIP)
      _sb.AppendLine("           , GI_CONVERSION_TO_NRC AS " & GIFT_COLUMN_CREDITS)
      _sb.AppendLine("           , GI_GIFT_ID AS " & GIFT_COLUMN_GIFT_ID)
      _sb.AppendLine("     FROM    GIFTS")
      _sb.AppendLine("    WHERE    GI_POINTS_TO_CREDITS_ID = @pPointsToCreditId")
      _sb.AppendLine(" ORDER BY    GI_CONVERSION_TO_NRC")

      Using _sql_cmd As New SqlCommand(_sb.ToString)

        _sql_cmd.Parameters.Add("@pPointsToCreditId", SqlDbType.BigInt).Value = PointsToCreditId

        Using _sql_da As New SqlDataAdapter(_sql_cmd)

          DbTrx.Fill(_sql_da, _table)

        End Using

      End Using

    Catch ex As Exception

      Log.Exception(ex)
    End Try

    Return _table
  End Function ' GetPointsToCreditGifts

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes a gift object from DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - GiftId
  '         - Context
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :
  Private Function DeleteGift(ByVal GiftId As Int64, _
                              ByVal Trx As SqlTransaction) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _sb As StringBuilder
    Dim _small_resource_id As Nullable(Of Long)
    Dim _large_resource_id As Nullable(Of Long)

    ' Remove the gift item from the GIFTS table
    Try

      _sb = New StringBuilder
      _sb.AppendLine("SELECT   GI_SMALL_RESOURCE_ID ")
      _sb.AppendLine("        ,GI_LARGE_RESOURCE_ID ")
      _sb.AppendLine("  FROM   GIFTS ")
      _sb.AppendLine(" WHERE   GI_GIFT_ID = @pGiftId")

      Using _sql_cmd As New SqlCommand(_sb.ToString, Trx.Connection, Trx)

        _sql_cmd.Parameters.Add("@pGiftId", SqlDbType.BigInt).Value = GiftId
        Using _reader As SqlDataReader = _sql_cmd.ExecuteReader()

          While _reader.Read()
            If IsDBNull(_reader(0)) Then
              _small_resource_id = Nothing
            Else
              _small_resource_id = _reader(0)
            End If

            If IsDBNull(_reader(1)) Then
              _large_resource_id = Nothing
            Else
              _large_resource_id = _reader(1)
            End If
          End While

          _reader.Close()
        End Using
      End Using

      _sb.Length = 0
      _sb.AppendLine("DELETE   GIFTS")
      _sb.AppendLine(" WHERE   GI_GIFT_ID = @pGiftId")
      Using _sql_cmd As New SqlCommand(_sb.ToString, Trx.Connection, Trx)

        _sql_cmd.Parameters.Add("@pGiftId", SqlDbType.BigInt).Value = GiftId

        If _sql_cmd.ExecuteNonQuery() <> 1 Then

          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

      End Using

      If _small_resource_id.HasValue Then
        If Not WSI.Common.WKTResources.Delete(_small_resource_id, Trx) Then

          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If
      End If

      If _large_resource_id.HasValue Then
        If Not WSI.Common.WKTResources.Delete(_large_resource_id, Trx) Then

          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If
      End If

    Catch ex As Exception

      Log.Exception(ex)
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function ' DeleteGift

#End Region ' Gifts

#Region " Promotions "

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given promotion object into the DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - Promo: Promotion object
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :
  Private Function UpdatePromotion(ByVal Promo As CLASS_PROMOTION.TYPE_PROMOTION, _
                                   ByVal PointsToCreditId As Int64, _
                                   ByVal Trx As SqlTransaction) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _sb As StringBuilder

    Try

      _sb = New StringBuilder
      _sb.AppendLine("UPDATE   PROMOTIONS ")
      _sb.AppendLine("   SET   PM_NAME = '" & Promo.name.Replace("'", "''") & "' ")
      _sb.AppendLine("       , PM_ENABLED = " & Promo.enabled)
      _sb.AppendLine("       , PM_TYPE = " & Promo.type)
      _sb.AppendLine("       , PM_SCHEDULE_WEEKDAY = " & Promo.schedule_weekday)
      _sb.AppendLine("       , PM_SCHEDULE1_TIME_FROM = " & Promo.schedule1_time_from)
      _sb.AppendLine("       , PM_SCHEDULE1_TIME_TO = " & Promo.schedule1_time_to)
      _sb.AppendLine("       , PM_SCHEDULE2_ENABLED = " & Promo.schedule2_enabled)
      _sb.AppendLine("       , PM_SCHEDULE2_TIME_FROM = " & Promo.schedule2_time_from)
      _sb.AppendLine("       , PM_SCHEDULE2_TIME_TO = " & Promo.schedule2_time_to)
      _sb.AppendLine("       , PM_CREDIT_TYPE = " & Promo.credit_type)
      _sb.AppendLine("       , PM_POINTS_TO_CREDITS_ID = " & PointsToCreditId)
      _sb.AppendLine("       , PM_PROMOGAME_ID = " & Promo.promogame)
      _sb.AppendLine(" WHERE   PM_PROMOTION_ID = " & Promo.promotion_id)

      Using _sql_cmd As New SqlCommand(_sb.ToString, Trx.Connection, Trx)
        If _sql_cmd.ExecuteNonQuery() = 1 Then

          Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        End If

      End Using

    Catch ex As Exception

      Log.Exception(ex)

    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

  End Function ' UpdatePromotion

  '----------------------------------------------------------------------------
  ' PURPOSE: Inserts the given object (Promotion) to the database.
  '
  ' PARAMS:
  '   - INPUT: 
  '       - Item: Object (Gift) that has to be treated
  '   - OUTPUT: None
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES:
  Private Function InsertPromotion(ByVal pPromo As CLASS_PROMOTION.TYPE_PROMOTION, _
                                   ByVal PointsToCreditId As Int64, _
                                   ByVal Trx As SqlTransaction) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _list_xml As String
    Dim _list_restricted_xml As String
    Dim _sb As StringBuilder
    Dim _param As SqlClient.SqlParameter
    Dim _promotion_id As Int64

    _list_xml = pPromo.terminal_list.ToXml()
    ' AMF 29-AUG-2013
    _list_restricted_xml = pPromo.terminal_list_restricted.ToXml()

    Try

      _sb = New StringBuilder()
      _sb.AppendLine(" INSERT INTO   PROMOTIONS ")
      _sb.AppendLine("             ( PM_NAME ")
      _sb.AppendLine("             , PM_ENABLED ")
      _sb.AppendLine("             , PM_TYPE ")
      _sb.AppendLine("             , PM_DATE_START ")
      _sb.AppendLine("             , PM_DATE_FINISH ")
      _sb.AppendLine("             , PM_SCHEDULE_WEEKDAY ")
      _sb.AppendLine("             , PM_SCHEDULE1_TIME_FROM ")
      _sb.AppendLine("             , PM_SCHEDULE1_TIME_TO ")
      _sb.AppendLine("             , PM_SCHEDULE2_ENABLED ")
      _sb.AppendLine("             , PM_SCHEDULE2_TIME_FROM ")
      _sb.AppendLine("             , PM_SCHEDULE2_TIME_TO ")
      _sb.AppendLine("             , PM_GENDER_FILTER ")
      _sb.AppendLine("             , PM_BIRTHDAY_FILTER ")
      _sb.AppendLine("             , PM_EXPIRATION_TYPE ")
      _sb.AppendLine("             , PM_EXPIRATION_VALUE ")
      _sb.AppendLine("             , PM_MIN_CASH_IN ")
      _sb.AppendLine("             , PM_MIN_CASH_IN_REWARD ")
      _sb.AppendLine("             , PM_CASH_IN ")
      _sb.AppendLine("             , PM_CASH_IN_REWARD ")
      _sb.AppendLine("             , PM_WON_LOCK ")
      _sb.AppendLine("             , PM_NUM_TOKENS ")
      _sb.AppendLine("             , PM_TOKEN_NAME ")
      _sb.AppendLine("             , PM_TOKEN_REWARD ")
      _sb.AppendLine("             , PM_DAILY_LIMIT ")
      _sb.AppendLine("             , PM_MONTHLY_LIMIT ")
      _sb.AppendLine("             , PM_GLOBAL_DAILY_LIMIT ")
      _sb.AppendLine("             , PM_GLOBAL_MONTHLY_LIMIT ")
      _sb.AppendLine("             , PM_GLOBAL_LIMIT ")
      _sb.AppendLine("             , PM_LEVEL_FILTER ")
      _sb.AppendLine("             , PM_PERMISSION ")
      _sb.AppendLine("             , PM_FREQ_FILTER_LAST_DAYS ")
      _sb.AppendLine("             , PM_FREQ_FILTER_MIN_DAYS ")
      _sb.AppendLine("             , PM_FREQ_FILTER_MIN_CASH_IN ")
      _sb.AppendLine("             , PM_MIN_SPENT ")
      _sb.AppendLine("             , PM_MIN_SPENT_REWARD ")
      _sb.AppendLine("             , PM_SPENT ")
      _sb.AppendLine("             , PM_SPENT_REWARD ")
      _sb.AppendLine("             , PM_MIN_PLAYED ")
      _sb.AppendLine("             , PM_MIN_PLAYED_REWARD ")
      _sb.AppendLine("             , PM_PLAYED ")
      _sb.AppendLine("             , PM_PLAYED_REWARD ")
      _sb.AppendLine("             , PM_PROVIDER_LIST ")
      _sb.AppendLine("             , PM_SMALL_RESOURCE_ID ")
      _sb.AppendLine("             , PM_LARGE_RESOURCE_ID ")
      _sb.AppendLine("             , PM_CREDIT_TYPE ")
      _sb.AppendLine("             , PM_TICKET_FOOTER ")
      _sb.AppendLine("             , PM_CATEGORY_ID ")
      _sb.AppendLine("             , PM_VISIBLE_ON_PROMOBOX ")
      _sb.AppendLine("             , PM_EXPIRATION_LIMIT ")
      _sb.AppendLine("             , PM_RESTRICTED_TO_TERMINAL_LIST ")
      _sb.AppendLine("             , PM_PLAY_RESTRICTED_TO_PROVIDER_LIST ")
      _sb.AppendLine("             , PM_POINTS_TO_CREDITS_ID ")
      _sb.AppendLine("             , PM_PROMOGAME_ID ")
      _sb.AppendLine("             ) ")
      _sb.AppendLine("               VALUES ")
      _sb.AppendLine("             ( @pName ")
      _sb.AppendLine("             , @pEnabled ")
      _sb.AppendLine("             , @pType ")
      _sb.AppendLine("             , @pDateStart ")
      _sb.AppendLine("             , @pDateFinish ")
      _sb.AppendLine("             , @pScheduleWeekday ")
      _sb.AppendLine("             , @pSchedule1TimeFrom ")
      _sb.AppendLine("             , @pSchedule1TimeTo ")
      _sb.AppendLine("             , @pSchedule2Enabled ")
      _sb.AppendLine("             , @pSchedule2TimeFrom ")
      _sb.AppendLine("             , @pSchedule2TimeTo ")
      _sb.AppendLine("             , @pGenderFilter ")
      _sb.AppendLine("             , @pBirthdayFilter ")
      _sb.AppendLine("             , @pExpirationType ")
      _sb.AppendLine("             , @pExpirationValue ")
      _sb.AppendLine("             , @pMinCashIn ")
      _sb.AppendLine("             , @pMinCashInReward ")
      _sb.AppendLine("             , @pCashIn ")
      _sb.AppendLine("             , @pCashInReward ")
      _sb.AppendLine("             , @pWonLock ")
      _sb.AppendLine("             , @pNumTokens ")
      _sb.AppendLine("             , @pTokenName ")
      _sb.AppendLine("             , @pTokenReward ")
      _sb.AppendLine("             , @pDailyLimit ")
      _sb.AppendLine("             , @pMonthlyLimit ")
      _sb.AppendLine("             , @pGlobalDailyLimit ")
      _sb.AppendLine("             , @pGlobalMonthlyLimit ")
      _sb.AppendLine("             , @pGlobalLimit ")
      _sb.AppendLine("             , @pLevelFilter ")
      _sb.AppendLine("             , @pSpecialPermission ")
      _sb.AppendLine("             , @pFreqFilterLastDays ")
      _sb.AppendLine("             , @pFreqFilterMinDays ")
      _sb.AppendLine("             , @pFreqFilterMinCashIn ")
      _sb.AppendLine("             , @pMinSpent ")
      _sb.AppendLine("             , @pMinSpentReward ")
      _sb.AppendLine("             , @pSpent ")
      _sb.AppendLine("             , @pSpentReward ")
      _sb.AppendLine("             , @pMinPlayed ")
      _sb.AppendLine("             , @pMinPlayedReward ")
      _sb.AppendLine("             , @pPlayed ")
      _sb.AppendLine("             , @pPlayedReward ")
      _sb.AppendLine("             , @pProviderListValue ")
      _sb.AppendLine("             , @pSmallImage ")
      _sb.AppendLine("             , @pLargeImage ")
      _sb.AppendLine("             , @pCreditType ")
      _sb.AppendLine("             , @pTicketFooter ")
      _sb.AppendLine("             , @pCategory ")
      _sb.AppendLine("             , @pVisibleOnPromoBOX")
      _sb.AppendLine("             , @pExpirationLimit")
      _sb.AppendLine("             , @pProviderListRestrictedValue ")
      _sb.AppendLine("             , @pRestrictedToProviderList")
      _sb.AppendLine("             , @pPointsId")
      _sb.AppendLine("             , @pPromoGame")
      _sb.AppendLine("             ) ")

      _sb.AppendLine("SET @pPromotionId = SCOPE_IDENTITY()")

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString(), Trx.Connection, Trx)

        _cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = pPromo.name
        _cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).Value = pPromo.enabled
        _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = pPromo.type
        _cmd.Parameters.Add("@pDateStart", SqlDbType.DateTime).Value = WSI.Common.WGDB.Now 'pPromo.date_start.Value
        _cmd.Parameters.Add("@pDateFinish", SqlDbType.DateTime).Value = New Date(2100, 12, 1) 'pPromo.date_finish.Value
        _cmd.Parameters.Add("@pScheduleWeekday", SqlDbType.Int).Value = pPromo.schedule_weekday
        _cmd.Parameters.Add("@pSchedule1TimeFrom", SqlDbType.Int).Value = pPromo.schedule1_time_from
        _cmd.Parameters.Add("@pSchedule1TimeTo", SqlDbType.Int).Value = pPromo.schedule1_time_to
        _cmd.Parameters.Add("@pSchedule2Enabled", SqlDbType.Bit).Value = pPromo.schedule2_enabled
        _cmd.Parameters.Add("@pSchedule2TimeFrom", SqlDbType.Int).Value = pPromo.schedule2_time_from
        _cmd.Parameters.Add("@pSchedule2TimeTo", SqlDbType.Int).Value = pPromo.schedule2_time_to
        _cmd.Parameters.Add("@pGenderFilter", SqlDbType.Int).Value = pPromo.gender_filter
        _cmd.Parameters.Add("@pBirthdayFilter", SqlDbType.Int).Value = pPromo.birthday_filter
        _cmd.Parameters.Add("@pExpirationType", SqlDbType.Int).Value = pPromo.expiration_type
        _cmd.Parameters.Add("@pExpirationValue", SqlDbType.Int).Value = pPromo.expiration_value
        _cmd.Parameters.Add("@pMinCashIn", SqlDbType.Money).Value = pPromo.min_cash_in
        _cmd.Parameters.Add("@pMinCashInReward", SqlDbType.Money).Value = pPromo.min_cash_in_reward
        _cmd.Parameters.Add("@pCashIn", SqlDbType.Money).Value = pPromo.cash_in
        _cmd.Parameters.Add("@pCashInReward", SqlDbType.Money).Value = pPromo.cash_in_reward
        _cmd.Parameters.Add("@pWonLock", SqlDbType.Money).Value = IIf(pPromo.won_lock_enabled, pPromo.won_lock, DBNull.Value)
        _cmd.Parameters.Add("@pNumTokens", SqlDbType.Int).Value = pPromo.num_tokens
        _cmd.Parameters.Add("@pTokenName", SqlDbType.VarChar).Value = IIf(IsNothing(pPromo.token_name), DBNull.Value, pPromo.token_name)
        _cmd.Parameters.Add("@pTokenReward", SqlDbType.Money).Value = pPromo.token_reward
        _cmd.Parameters.Add("@pDailyLimit", SqlDbType.Money).Value = IIf(pPromo.daily_limit > 0, pPromo.daily_limit, DBNull.Value)
        _cmd.Parameters.Add("@pMonthlyLimit", SqlDbType.Money).Value = IIf(pPromo.monthly_limit > 0, pPromo.monthly_limit, DBNull.Value)
        _cmd.Parameters.Add("@pGlobalDailyLimit", SqlDbType.Money).Value = IIf(pPromo.global_daily_limit > 0, pPromo.global_daily_limit, DBNull.Value)
        _cmd.Parameters.Add("@pGlobalMonthlyLimit", SqlDbType.Money).Value = IIf(pPromo.global_monthly_limit > 0, pPromo.global_monthly_limit, DBNull.Value)
        _cmd.Parameters.Add("@pGlobalLimit", SqlDbType.Money).Value = IIf(pPromo.global_limit > 0, pPromo.global_limit, DBNull.Value)
        _cmd.Parameters.Add("@pLevelFilter", SqlDbType.Int).Value = pPromo.level_filter
        _cmd.Parameters.Add("@pSpecialPermission", SqlDbType.Int).Value = pPromo.special_permission
        _cmd.Parameters.Add("@pFreqFilterLastDays", SqlDbType.Int).Value = pPromo.freq_filter_last_days
        _cmd.Parameters.Add("@pFreqFilterMinDays", SqlDbType.Int).Value = pPromo.freq_filter_min_days
        _cmd.Parameters.Add("@pFreqFilterMinCashIn", SqlDbType.Money).Value = pPromo.freq_filter_min_cash_in
        _cmd.Parameters.Add("@pMinSpent", SqlDbType.Money).Value = pPromo.min_spent
        _cmd.Parameters.Add("@pMinSpentReward", SqlDbType.Money).Value = pPromo.min_spent_reward
        _cmd.Parameters.Add("@pSpent", SqlDbType.Money).Value = pPromo.spent
        _cmd.Parameters.Add("@pSpentReward", SqlDbType.Money).Value = pPromo.spent_reward
        _cmd.Parameters.Add("@pMinPlayed", SqlDbType.Money).Value = pPromo.min_played
        _cmd.Parameters.Add("@pMinPlayedReward", SqlDbType.Money).Value = pPromo.min_played_reward
        _cmd.Parameters.Add("@pPlayed", SqlDbType.Money).Value = pPromo.played
        _cmd.Parameters.Add("@pPlayedReward", SqlDbType.Money).Value = pPromo.played_reward
        _cmd.Parameters.Add("@pProviderListValue", SqlDbType.NVarChar).Value = IIf(IsNothing(_list_xml), DBNull.Value, _list_xml)
        _cmd.Parameters.Add("@pSmallImage", SqlDbType.BigInt).Value = IIf(pPromo.small_resource_id.HasValue, pPromo.small_resource_id, DBNull.Value)
        _cmd.Parameters.Add("@pLargeImage", SqlDbType.BigInt).Value = IIf(pPromo.large_resource_id.HasValue, pPromo.large_resource_id, DBNull.Value)
        _cmd.Parameters.Add("@pCreditType", SqlDbType.Int).Value = pPromo.credit_type
        _cmd.Parameters.Add("@pTicketFooter", SqlDbType.NVarChar).Value = "" 'pPromo.ticket_footer
        _cmd.Parameters.Add("@pCategory", SqlDbType.Int).Value = pPromo.category_id
        _cmd.Parameters.Add("@pVisibleOnPromoBOX", SqlDbType.Bit).Value = pPromo.visible_on_PromoBOX
        _cmd.Parameters.Add("@pExpirationLimit", SqlDbType.DateTime).Value = IIf(pPromo.expiration_limit.Value = DateTime.MinValue, DBNull.Value, pPromo.expiration_limit.Value)
        _cmd.Parameters.Add("@pProviderListRestrictedValue", SqlDbType.NVarChar).Value = IIf(IsNothing(_list_restricted_xml), DBNull.Value, _list_restricted_xml)
        _cmd.Parameters.Add("@pRestrictedToProviderList", SqlDbType.Int).Value = IIf(IsNothing(_list_xml), "0", "1")
        _cmd.Parameters.Add("@pPointsId", SqlDbType.BigInt).Value = PointsToCreditId
        _cmd.Parameters.Add("@pPromoGame", SqlDbType.BigInt).Value = pPromo.promogame

        _param = _cmd.Parameters.Add("@pPromotionId", SqlDbType.BigInt)
        _param.Direction = ParameterDirection.Output

        If _cmd.ExecuteNonQuery() <> 1 Or IsDBNull(_param.Value) Then

          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        Else
          _promotion_id = _param.Value
        End If

        If pPromo.type = Promotion.PROMOTION_TYPE.PREASSIGNED Then

          If Not WSI.Common.AccountsImport.InsertForPreassignedPromotion(pPromo.accounts_preassigned, _promotion_id, Trx) Then
            Throw New Exception("")
          End If

        End If

      End Using

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Catch ex As Exception

      Log.Exception(ex)
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

  End Function ' InsertPromotion

  '----------------------------------------------------------------------------
  ' PURPOSE: We generate the promotion information.
  '
  ' PARAMS:
  '   - INPUT: 
  '       - Item: Object (Redemption table) that have to be treated
  '   - OUTPUT: None
  '
  ' RETURNS:
  '   - New promotion object item
  '
  ' NOTES:
  Private Function PreparePromotionInformation(ByVal Item As TYPE_POINTS_TO_CREDITS) As CLASS_PROMOTION.TYPE_PROMOTION
    Dim _promo As CLASS_PROMOTION.TYPE_PROMOTION

    _promo = New CLASS_PROMOTION.TYPE_PROMOTION

    _promo.promotion_id = Item.Promotion_Id
    _promo.name = Item.Points_Name
    _promo.enabled = Item.Points_Enabled
    _promo.special_permission = CLASS_PROMOTION.ENUM_SPECIAL_PERMISSION.NOT_SET
    If Item.Points_Credit_Type = ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE Then
      _promo.type = Promotion.PROMOTION_TYPE.PER_POINTS_REDEEMABLE
    Else
      _promo.type = Promotion.PROMOTION_TYPE.PER_POINTS_NOT_REDEEMABLE
    End If

    _promo.credit_type = Item.Points_Credit_Type

    _promo.date_start.IsNull = True
    _promo.date_finish.IsNull = True
    _promo.schedule_weekday = Item.Points_Schedule_Weekday
    _promo.schedule1_time_from = Item.Points_Schedule_Time_From1
    _promo.schedule1_time_to = Item.Points_Schedule_Time_To1
    _promo.schedule2_enabled = Item.Points_Schedule2_Enabled
    _promo.schedule2_time_from = Item.Points_Schedule_Time_From2
    _promo.schedule2_time_to = Item.Points_Schedule_Time_To2
    _promo.gender_filter = CLASS_PROMOTION.ENUM_GENDER_FILTER.NOT_SET
    _promo.birthday_filter = CLASS_PROMOTION.ENUM_BIRTHDAY_FILTER.NOT_SET
    _promo.level_filter = 0
    _promo.freq_filter_last_days = 0
    _promo.freq_filter_min_days = 0
    _promo.freq_filter_min_cash_in = 0
    _promo.expiration_type = CLASS_PROMOTION.ENUM_EXPIRATION_TYPE.NOT_SET
    _promo.expiration_value = 0
    _promo.min_cash_in = 0
    _promo.min_cash_in_reward = 0
    _promo.cash_in = 0
    _promo.cash_in_reward = 0
    _promo.won_lock_enabled = False
    _promo.won_lock = 0
    _promo.num_tokens = 0
    _promo.token_name = ""
    _promo.token_reward = 0
    _promo.daily_limit = 0
    _promo.monthly_limit = 0
    _promo.global_daily_limit = 0
    _promo.global_monthly_limit = 0
    _promo.min_spent = 0
    _promo.min_spent_reward = 0
    _promo.spent = 0
    _promo.spent_reward = 0
    _promo.min_played = 0
    _promo.min_played_reward = 0
    _promo.played = 0
    _promo.played_reward = 0

    _promo.promo_screen_mode = CLASS_PROMOTION.ENUM_PROMO_SCREEN_MODE.MODE_EDIT

    _promo.visible_on_PromoBOX = False

    _promo.expiration_limit.IsNull = True
    _promo.expiration_limit.Value = Date.MinValue

    _promo.promogame = Item.AwardGame

    Return _promo

  End Function ' PreparePromotionInformation

#End Region ' Promotions

#End Region 'Private Functions

End Class ' CLASS_POINTS_TO_CREDITS
