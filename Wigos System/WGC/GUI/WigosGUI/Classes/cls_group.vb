'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_group.vb
'
' DESCRIPTION : Group class for user edition
'              
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 01-JUL-2013  RBG    Initial version
' 27-MAR-2014  MPO    Performance improved for terminal generations
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls

Imports WSI.Common.Groups

Imports System.Runtime.InteropServices
Imports System.Data.SqlClient

Public Class CLASS_GROUP
  Inherits CLASS_BASE

#Region " Constants "

  ' Fields length
  Public Const MAX_NAME_LEN As Integer = 50

#End Region

#Region " Enums "

#End Region ' Enums 

#Region " Structures "

  Public Class TYPE_GROUP
    Public group_id As Int64
    Public content_type As Int32
    Public type As Int32
    Public name As String
    Public description As String
    Public updated As Date
    Public content As DataTable
    Public enabled As Boolean
  End Class

#End Region

#Region " Members "

  Protected m_group As New TYPE_GROUP

#End Region

#Region " Properties "

  Sub New()

    Me.m_group.enabled = True

  End Sub


  Public Property GroupId() As Int64
    Get
      Return Me.m_group.group_id
    End Get

    Set(ByVal Value As Int64)
      Me.m_group.group_id = Value
    End Set
  End Property

  Public Property ContentType() As Int32
    Get
      Return Me.m_group.content_type
    End Get

    Set(ByVal Value As Int32)
      Me.m_group.content_type = Value
    End Set
  End Property

  Public Property Type() As Int32
    Get
      Return Me.m_group.type
    End Get

    Set(ByVal Value As Int32)
      Me.m_group.type = Value
    End Set
  End Property

  Public Property Name() As String
    Get
      Return Me.m_group.name
    End Get

    Set(ByVal Value As String)
      Me.m_group.name = Value
    End Set
  End Property

  Public Property Description() As String
    Get
      Return Me.m_group.description
    End Get

    Set(ByVal Value As String)
      Me.m_group.description = Value
    End Set
  End Property


  Public Property Updated() As Date
    Get
      Return Me.m_group.updated
    End Get

    Set(ByVal Value As Date)
      Me.m_group.updated = Value
    End Set
  End Property

  Public Property Content() As DataTable
    Get
      Return Me.m_group.content
    End Get

    Set(ByVal Value As DataTable)
      Me.m_group.content = Value
    End Set
  End Property

  Public Property Enabled() As Boolean
    Get
      Return Me.m_group.enabled
    End Get

    Set(ByVal Value As Boolean)
      Me.m_group.enabled = Value
    End Set
  End Property

#End Region ' Properties

#Region " Overrides functions "

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads from the database into the given object
  '
  ' PARAMS :
  '     - INPUT :
  '         - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '
  ' NOTES :

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    Me.GroupId = ObjectId

    ' Read gift data
    _rc = ReadGroup(m_group, SqlCtx)

    Select Case _rc

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Read

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    ' Update group data
    _rc = UpdateGroup(m_group, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Update

  '----------------------------------------------------------------------------
  ' PURPOSE : Inserts the given object to the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES :

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    ' Insert group data
    _rc = InsertGroup(m_group, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Insert

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes the given object from the database.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DEPENDENCIES
  '
  ' NOTES :

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    ' Delete gift data
    _rc = DeleteGroup(Me.GroupId, SqlCtx)

    Select Case _rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DEPENDENCIES
        Return ENUM_STATUS.STATUS_DEPENDENCIES

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Delete

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns a duplicate of the given object.
  '
  ' PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created object
  '
  ' NOTES :

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _temp_group As CLASS_GROUP

    _temp_group = New CLASS_GROUP
    _temp_group.m_group.group_id = Me.m_group.group_id
    _temp_group.m_group.content_type = Me.m_group.content_type
    _temp_group.m_group.type = Me.m_group.type
    _temp_group.m_group.name = Me.m_group.name
    _temp_group.m_group.updated = Me.m_group.updated
    _temp_group.m_group.description = Me.m_group.description
    _temp_group.m_group.enabled = Me.m_group.enabled
    _temp_group.m_group.content = Me.m_group.content.Copy

    Return _temp_group

  End Function ' Duplicate

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns the related auditor data for the current object.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created audit object
  '
  ' NOTES :
  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _added_list As List(Of String)
    Dim _type As String
    Dim _dt_aux As DataTable
    Dim _idx As Int32
    Dim _sorted_rows As DataRow()
    Dim _state As DataRowState

    ' Add main line
    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)

    Call _auditor_data.SetName(GLB_NLS_GUI_CONTROLS.Id(465), Me.Name)           ' "Group"
    Call _auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(461), Me.Name)          ' "Name"
    Call _auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(473), Me.Description)   ' "Description"
    Call _auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(281), Me.Enabled)       ' "Enabled"

    ' Copy de element's list for not modify the original rows
    _dt_aux = Me.Content.Copy

    ' Sort de rows to ensure the same order for read and edit objects
    _sorted_rows = _dt_aux.Select("", "ID,TYPE,NAME ASC", DataViewRowState.Unchanged Or DataViewRowState.Deleted Or DataViewRowState.Added)
    _added_list = New List(Of String)
    _idx = 0

    ' Process each elemen's row
    For Each _row As DataRow In _sorted_rows

      _state = _row.RowState

      ' Reject changes to acces to the row's infomation 
      If (_state = DataRowState.Deleted) Then
        _row.RejectChanges()
      End If

      Select Case (CType(_row(ELEMENTS_COLUMN_NODE_TYPE), GROUP_NODE_ELEMENT))

        Case GROUP_NODE_ELEMENT.GROUP
          _type = GLB_NLS_GUI_CONTROLS.GetString(471)
        Case GROUP_NODE_ELEMENT.GROUPS
          _type = GLB_NLS_GUI_CONTROLS.GetString(471)
        Case GROUP_NODE_ELEMENT.PROVIDER
          _type = GLB_NLS_GUI_CONTROLS.GetString(467)
        Case GROUP_NODE_ELEMENT.PROVIDERS
          _type = GLB_NLS_GUI_CONTROLS.GetString(467)
        Case GROUP_NODE_ELEMENT.ZONE
          _type = GLB_NLS_GUI_CONTROLS.GetString(468)
        Case GROUP_NODE_ELEMENT.ZONES
          _type = GLB_NLS_GUI_CONTROLS.GetString(468)
        Case GROUP_NODE_ELEMENT.AREAS
          _type = GLB_NLS_GUI_CONTROLS.GetString(469)
        Case GROUP_NODE_ELEMENT.BANKS
          _type = GLB_NLS_GUI_CONTROLS.GetString(470)
        Case GROUP_NODE_ELEMENT.TERMINALS
          _type = GLB_NLS_GUI_CONTROLS.GetString(472)
        Case GROUP_NODE_ELEMENT.ALL
          _type = ""
        Case Else
          _type = ""
      End Select

      Select Case (_state)

        Case DataRowState.Added
          _added_list.Add(_row(ELEMENTS_COLUMN_NAME).ToString)
        Case DataRowState.Deleted
          Call _auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(466), _type + ": ---")
        Case Else
          Call _auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(466), _row(ELEMENTS_COLUMN_NAME).ToString)

      End Select

      _idx += 1

    Next

    ' Process the added rows
    For Each _item As String In _added_list
      Call _auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(466), _item) ' "Nuevo elemento"
    Next

    Return _auditor_data

  End Function ' AuditorData

#End Region

#Region " Private Functions "

  '----------------------------------------------------------------------------
  ' PURPOSE : Reads a group object from DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - GroupId
  '         - Context
  '
  '     - OUTPUT :
  '         - Item
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_NOT_FOUND
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function ReadGroup(ByVal Item As TYPE_GROUP, _
                            ByVal Context As Integer) As Integer

    Dim _sql_query As String

    _sql_query = " SELECT GR_ID AS ID" & _
                 "      , GR_CONTENT_TYPE AS CONTENT_TYPE" & _
                 "      , GR_TYPE AS TYPE" & _
                 "      , GR_NAME AS NAME" & _
                 "      , GR_UPDATED AS UPDATED" & _
                 "      , GR_DESCRIPTION AS DESCRIPTION" & _
                 "      , GR_ENABLED AS ENABLED" & _
                 " FROM GROUPS" & _
                 " WHERE GR_ID = @pGroupId"

    Try

      Using _db_trx As New WSI.Common.DB_TRX()

        Using _sql_cmd As New SqlCommand(_sql_query, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _sql_cmd.Parameters.Add("@pGroupId", SqlDbType.BigInt).Value = Me.GroupId

          Using _reader As SqlDataReader = _sql_cmd.ExecuteReader()

            If (Not _reader.Read) Then

              Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND

            Else

              ContentType = _reader("CONTENT_TYPE")
              Type = _reader("TYPE")
              Name = _reader("NAME")
              Updated = _reader("UPDATED")
              Enabled = _reader("ENABLED")

              If Not IsDBNull(_reader("DESCRIPTION")) Then
                Description = _reader("DESCRIPTION")
              End If

              If (GetElements(GroupConvert(Type, GroupId), GroupId, Content)) Then

                Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

              End If

            End If

          End Using

        End Using

      End Using

    Catch ex As Exception

    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

  End Function ' ReadGroup

  '----------------------------------------------------------------------------
  ' PURPOSE : Deletes a group object from DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - GroupId
  '         - Context
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_NOT_FOUND
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function DeleteGroup(ByVal Id As Int64, _
                              ByVal Context As Integer) As Integer
    Dim _sql_query As String

    Try

      Using _db_trx As New WSI.Common.DB_TRX()

        ' Delete elements of group to delete or instances of the group o delete as element
        _sql_query = "DELETE GROUP_ELEMENTS WHERE GE_GROUP_ID = @pGroupId OR (GE_ELEMENT_ID = @pGroupId AND GE_ELEMENT_TYPE = @pType)"

        Using _sql_cmd As New SqlCommand(_sql_query, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _sql_cmd.Parameters.Add("@pGroupId", SqlDbType.BigInt).Value = Id
          _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).Value = WSI.Common.GROUP_ELEMENT_TYPE.GROUP
          _sql_cmd.ExecuteNonQuery()

        End Using

        _sql_query = "DELETE GROUPS WHERE GR_ID = @pGroupId"

        Using _sql_cmd As New SqlCommand(_sql_query, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _sql_cmd.Parameters.Add("@pGroupId", SqlDbType.BigInt).Value = Id

          If _sql_cmd.ExecuteNonQuery() = 0 Then

            Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND

          ElseIf _sql_cmd.ExecuteNonQuery() > 1 Then

            Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

          End If

        End Using

        _db_trx.Commit()
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

      End Using

    Catch ex As Exception

    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

  End Function ' DeleteGroup

  '----------------------------------------------------------------------------
  ' PURPOSE : Adds a group object into DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - Item
  '         - Context
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_DUPLICATE_KEY
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function InsertGroup(ByVal Item As TYPE_GROUP, _
                              ByVal Context As Integer) As Integer
    Dim _obj As Object
    Dim _new_id As SqlParameter
    Dim _sb As System.Text.StringBuilder

    _sb = New System.Text.StringBuilder()

    Try

      Using _db_trx As New WSI.Common.DB_TRX()

        _sb.AppendLine(" SELECT GR_ID FROM GROUPS WHERE GR_NAME = @pGroupName")

        Using _sql_cmd As New SqlCommand(_sb.ToString, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _sql_cmd.Parameters.Add("@pGroupName", SqlDbType.VarChar).Value = Me.Name
          _obj = _sql_cmd.ExecuteScalar()

          If Not _obj Is Nothing Then

            Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY

          End If

        End Using

        _sb.Length = 0
        _sb.AppendLine("INSERT INTO GROUPS ( GR_CONTENT_TYPE")
        _sb.AppendLine("                   , GR_TYPE ")
        _sb.AppendLine("                   , GR_NAME ")
        _sb.AppendLine("                   , GR_UPDATED ")
        _sb.AppendLine("                   , GR_DESCRIPTION ")
        _sb.AppendLine("                   , GR_ENABLED) ")
        _sb.AppendLine("            VALUES ( @pContentType")
        _sb.AppendLine("                   , @pType")
        _sb.AppendLine("                   , @pName")
        _sb.AppendLine("                   , GETDATE()")
        _sb.AppendLine("                   , @pDescription")
        _sb.AppendLine("                   , @pEnabled);")
        _sb.AppendLine("SET @pAdvId = SCOPE_IDENTITY();")

        Using _sql_cmd As New SqlCommand(_sb.ToString, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _sql_cmd.Parameters.Add("@pContentType", SqlDbType.Int).Value = Item.content_type
          _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).Value = WSI.Common.GROUP_ELEMENT_TYPE.GROUP
          _sql_cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = Item.name
          _sql_cmd.Parameters.Add("@pDescription", SqlDbType.NVarChar).Value = Item.description
          _sql_cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).Value = Item.enabled
          _new_id = _sql_cmd.Parameters.Add("@pAdvId", SqlDbType.BigInt)
          _new_id.Direction = ParameterDirection.Output

          If _sql_cmd.ExecuteNonQuery() <> 1 Or IsDBNull(_new_id.Value) Then

            Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

          End If

        End Using

        Using _da As New SqlDataAdapter()

          _sb.Length = 0
          _sb.AppendLine("INSERT INTO GROUP_ELEMENTS ( GE_ELEMENT_TYPE")
          _sb.AppendLine("                           , GE_ELEMENT_ID")
          _sb.AppendLine("                           , GE_GROUP_ID")
          _sb.AppendLine("                           , GE_QUERY)")
          _sb.AppendLine("                    VALUES ( @pType")
          _sb.AppendLine("                           , @pId")
          _sb.AppendLine("                           , @pGroupId")
          _sb.AppendLine("                           , @pQuery)")

          _da.InsertCommand = New SqlCommand(_sb.ToString)

          _da.InsertCommand.Parameters.Add("@pType", SqlDbType.Int).SourceColumn = WSI.Common.Groups.ELEMENTS_COLUMN_TYPE
          _da.InsertCommand.Parameters.Add("@pId", SqlDbType.BigInt).SourceColumn = WSI.Common.Groups.ELEMENTS_COLUMN_ID
          _da.InsertCommand.Parameters.Add("@pGroupId", SqlDbType.BigInt).Value = _new_id.Value
          _da.InsertCommand.Parameters.Add("@pQuery", SqlDbType.VarChar, 50).Value = ""

          _da.InsertCommand.Connection = _db_trx.SqlTransaction.Connection
          _da.InsertCommand.Transaction = _db_trx.SqlTransaction
          _da.InsertCommand.UpdatedRowSource = UpdateRowSource.None

          _da.Update(Item.content)

        End Using

        _db_trx.Commit()
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

      End Using

    Catch ex As Exception

    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

  End Function ' InsertGroup

  '----------------------------------------------------------------------------
  ' PURPOSE : Updates the given group object into the DB
  '
  ' PARAMS :
  '     - INPUT :
  '         - Item
  '         - Context
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - CONFIGURATION_OK
  '     - CONFIGURATION_ERROR_NOT_FOUND
  '     - CONFIGURATION_ERROR_DB
  '
  ' NOTES :

  Private Function UpdateGroup(ByVal Item As TYPE_GROUP, _
                              ByVal Context As Integer) As Integer
    Dim _sql_query As String
    Dim _dt_aux As DataTable
    Dim _data As Object
    Dim _num_rows As Integer

    Try

      ' Make a copy to prevent the delete of rows in update command. The audit proces requires all the rows.
      _dt_aux = Item.content.Copy

      Using _db_trx As New WSI.Common.DB_TRX()

        ' Check whether there is no other group with the same name 
        _sql_query = " SELECT   COUNT (*)" & _
                     " FROM   GROUPS" & _
                     " WHERE   GR_NAME = @pName " & _
                     " AND   GR_ID <> @pGroupId "

        Using _sql_cmd As New SqlCommand(_sql_query, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _sql_cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = Item.name
          _sql_cmd.Parameters.Add("@pGroupId", SqlDbType.BigInt).Value = Item.group_id

          _data = _sql_cmd.ExecuteScalar

          If Not IsNothing(_data) AndAlso IsNumeric(_data) Then

            _num_rows = CType(_data, Integer)
            If _num_rows > 0 Then

              Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY

            End If

          Else

            Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

          End If

        End Using

        ' Update the group's name
        _sql_query = " UPDATE GROUPS" & _
                     " SET GR_NAME = @pName" & _
                     "      , GR_UPDATED = GETDATE()" & _
                     "      , GR_DESCRIPTION = @pDescription" & _
                     "      , GR_ENABLED = @pEnabled" & _
                     " WHERE GR_ID = @pGroupId"

        Using _sql_cmd As New SqlCommand(_sql_query, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _sql_cmd.Parameters.Add("@pName", SqlDbType.VarChar).Value = Item.name
          _sql_cmd.Parameters.Add("@pDescription", SqlDbType.VarChar, 255).Value = Item.description
          _sql_cmd.Parameters.Add("@pGroupId", SqlDbType.BigInt).Value = Item.group_id
          _sql_cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).Value = Item.enabled
          If _sql_cmd.ExecuteNonQuery() = 0 Then

            Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND

          ElseIf _sql_cmd.ExecuteNonQuery() > 1 Then

            Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

          End If

        End Using

        ' Update the group's elements
        Using _da As New SqlDataAdapter()

          _sql_query = "INSERT INTO GROUP_ELEMENTS (GE_ELEMENT_TYPE,GE_ELEMENT_ID,GE_GROUP_ID,GE_QUERY) VALUES (@pType,@pId,@pGroupId,@pQuery)"

          _da.InsertCommand = New SqlCommand(_sql_query)

          _da.InsertCommand.Parameters.Add("@pType", SqlDbType.Int).SourceColumn = ELEMENTS_COLUMN_TYPE
          _da.InsertCommand.Parameters.Add("@pId", SqlDbType.BigInt).SourceColumn = ELEMENTS_COLUMN_ID
          _da.InsertCommand.Parameters.Add("@pGroupId", SqlDbType.BigInt).Value = Item.group_id
          _da.InsertCommand.Parameters.Add("@pQuery", SqlDbType.VarChar, 50).Value = ""

          _da.InsertCommand.Connection = _db_trx.SqlTransaction.Connection
          _da.InsertCommand.Transaction = _db_trx.SqlTransaction
          _da.InsertCommand.UpdatedRowSource = UpdateRowSource.None

          _sql_query = "DELETE GROUP_ELEMENTS WHERE GE_ELEMENT_ID = @pId AND GE_ELEMENT_TYPE = @pType AND GE_GROUP_ID = @pGroupId"

          _da.DeleteCommand = New SqlCommand(_sql_query)

          _da.DeleteCommand.Parameters.Add("@pType", SqlDbType.Int).SourceColumn = ELEMENTS_COLUMN_TYPE
          _da.DeleteCommand.Parameters.Add("@pId", SqlDbType.BigInt).SourceColumn = ELEMENTS_COLUMN_ID
          _da.DeleteCommand.Parameters.Add("@pGroupId", SqlDbType.BigInt).Value = Item.group_id

          _da.DeleteCommand.Connection = _db_trx.SqlTransaction.Connection
          _da.DeleteCommand.Transaction = _db_trx.SqlTransaction
          _da.DeleteCommand.UpdatedRowSource = UpdateRowSource.None

          _da.Update(_dt_aux)

        End Using

        _db_trx.Commit()
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

      End Using

    Catch ex As Exception

    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

  End Function ' UpdateGroup

#End Region ' Private Functions

End Class
