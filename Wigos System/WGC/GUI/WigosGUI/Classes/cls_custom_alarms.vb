'------------------------------------------------------------------------------------------------
' Copyright © 2013 Win Systems Ltd.
'------------------------------------------------------------------------------------------------
'
' MODULE NAME : cls_custom_alarms.vb
'
' DESCRIPTION : Jaume Barnés
'
' REVISION HISTORY:
' 
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 10-APR-2014  JBC    Initial version.
' 06-MAY-2014  JBC    Fixed Bug WIG-898: NLS changed.
' 08-MAY-2014  JBC    Fixed Bug WIG-904: NLS changed (hours/days).
' 20-OCT-2015  GMV    TFS 5275 : HighRollers GUI Configuración
' 11-NOV-2015  GMV    TFS 5275 : HighRollers Extended definition
' 03-DEC-2015  DLL    Fixed Bug TFS-6662: Highrollers configuration doesn't work
'--------------------------------------------------------------------
#Region " Imports "

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports WSI.Common
Imports WSI.Common.OperationsSchedule
Imports System.Text
Imports System.Data.SqlClient
Imports GUI_Controls.Misc

#End Region ' Imports

Public Class CLS_CUSTOM_ALARMS
  Inherits CLASS_BASE

#Region " Enums "
  Public Enum ENUM_PUS
    HOURS = 1
    DAYS = 2
  End Enum
#End Region

#Region " Members "
  'PAYOUT
  Private m_payout_enabled As Boolean
  Private m_payout_pus_value As Int64
  Private m_payout_pus_unit As Int32
  Private m_payout_limit_lower As Decimal
  Private m_payout_limit_upper As Decimal

  'PLAYED
  Private m_played_enabled As Boolean
  Private m_played_pus_value As Int64
  Private m_played_pus_unit As Int32
  Private m_played_limit_amount As Decimal
  Private m_played_limit_quantity As Int32

  'WON
  Private m_won_enabled As Boolean
  Private m_won_pus_value As Int64
  Private m_won_pus_unit As Int32
  Private m_won_limit_amount As Decimal
  Private m_won_limit_quantity As Int32

  'COUNTERFEIT
  Private m_counterfeits_enabled As Boolean
  Private m_counterfeits_pus_value As Int64
  Private m_counterfeits_pus_unit As Int32
  Private m_counterfeits_limit_quantity As Int32

  'JACKPOT
  Private m_jackpot_enabled As Boolean
  Private m_jackpot_pus_value As Int64
  Private m_jackpot_pus_unit As Int32
  Private m_jackpot_limit_amount As Decimal
  Private m_jackpot_limit_quantity As Int32

  'CANCELCREDIT
  Private m_canceled_credits_enabled As Boolean
  Private m_canceled_credits_pus_value As Int64
  Private m_canceled_credits_pus_unit As Int32
  Private m_canceled_credits_limit_amount As Decimal
  Private m_canceled_credits_limit_quantity As Int32

  'TERMINAL ACTIVITY
  Private m_terminal_activity_enabled As Boolean
  Private m_terminal_activity_pus_value As Int64
  Private m_terminal_activity_pus_unit As Int32

  'HIGHROLLER
  Private m_highroller_enabled As Boolean
  Private m_highroller_pus_value As Int64
  Private m_highroller_pus_unit As Int32
  Private m_highroller_bet_average_amount As Decimal
  Private m_highroller_coin_in_amount As Decimal
  Private m_highroller_process_anonymous As Boolean

#End Region

#Region "Properties "

  Public Property Payout() As Boolean
    Get

      Return Me.m_payout_enabled
    End Get
    Set(ByVal value As Boolean)
      Me.m_payout_enabled = value
    End Set
  End Property

  Public Property Payout_PUS_Value() As Int64
    Get
      Return m_payout_pus_value
    End Get
    Set(ByVal value As Int64)
      m_payout_pus_value = value
    End Set
  End Property

  Public Property Payout_PUS_Unit() As Int32
    Get
      Return m_payout_pus_unit
    End Get
    Set(ByVal value As Int32)
      m_payout_pus_unit = value
    End Set
  End Property

  Public Property Payout_Limit_Lower() As Decimal
    Get
      Return m_payout_limit_lower
    End Get
    Set(ByVal value As Decimal)
      m_payout_limit_lower = value
    End Set
  End Property

  Public Property Payout_Limit_Upper() As Decimal
    Get
      Return m_payout_limit_upper
    End Get
    Set(ByVal value As Decimal)
      m_payout_limit_upper = value
    End Set
  End Property

  Public Property Played_Enabled() As Boolean
    Get
      Return m_played_enabled
    End Get
    Set(ByVal value As Boolean)
      m_played_enabled = value
    End Set
  End Property

  Public Property Played_Limit_Amount() As Decimal
    Get

      Return m_played_limit_amount
    End Get
    Set(ByVal value As Decimal)
      m_played_limit_amount = value
    End Set
  End Property

  Public Property Played_PUS_Value() As Int64
    Get
      Return m_played_pus_value
    End Get
    Set(ByVal value As Int64)
      m_played_pus_value = value
    End Set
  End Property

  Public Property Played_PUS_Unit() As Int32
    Get
      Return m_played_pus_unit
    End Get
    Set(ByVal value As Int32)
      m_played_pus_unit = value
    End Set
  End Property

  Public Property Played_Limit_Quantity() As Int32
    Get

      Return m_played_limit_quantity
    End Get
    Set(ByVal value As Int32)
      m_played_limit_quantity = value
    End Set
  End Property

  Public Property Won_Enabled() As Boolean
    Get

      Return m_won_enabled
    End Get
    Set(ByVal value As Boolean)
      m_won_enabled = value
    End Set
  End Property

  Public Property Won_PUS_Value() As Int64
    Get
      Return m_won_pus_value
    End Get
    Set(ByVal value As Int64)
      m_won_pus_value = value
    End Set
  End Property

  Public Property Won_PUS_Unit() As Int32
    Get
      Return m_won_pus_unit
    End Get
    Set(ByVal value As Int32)
      m_won_pus_unit = value
    End Set
  End Property

  Public Property Won_Limit_Amount() As Decimal
    Get

      Return m_won_limit_amount
    End Get
    Set(ByVal value As Decimal)
      m_won_limit_amount = value
    End Set
  End Property

  Public Property Won_Limit_Quantity() As Int32
    Get

      Return m_won_limit_quantity
    End Get
    Set(ByVal value As Int32)
      m_won_limit_quantity = value
    End Set
  End Property

  Public Property Counterfeit_Enabled() As Boolean
    Get

      Return m_counterfeits_enabled
    End Get
    Set(ByVal value As Boolean)
      m_counterfeits_enabled = value
    End Set
  End Property

  Public Property Counterfeit_PUS_Value() As Int64
    Get

      Return m_counterfeits_pus_value
    End Get
    Set(ByVal value As Int64)
      m_counterfeits_pus_value = value
    End Set
  End Property

  Public Property Counterfeit_PUS_Unit() As Int32
    Get

      Return m_counterfeits_pus_unit
    End Get
    Set(ByVal value As Int32)
      m_counterfeits_pus_unit = value
    End Set
  End Property

  Public Property Counterfeit_Limit_Quantity() As Int32
    Get

      Return m_counterfeits_limit_quantity
    End Get
    Set(ByVal value As Int32)
      m_counterfeits_limit_quantity = value
    End Set
  End Property

  Public Property Jackpot_Enabled() As Boolean
    Get

      Return m_jackpot_enabled
    End Get
    Set(ByVal value As Boolean)
      m_jackpot_enabled = value
    End Set
  End Property

  Public Property Jackpot_PUS_Value() As Int64
    Get

      Return m_jackpot_pus_value
    End Get
    Set(ByVal value As Int64)
      m_jackpot_pus_value = value
    End Set
  End Property

  Public Property Jackpot_PUS_Unit() As Int32
    Get

      Return m_jackpot_pus_unit
    End Get
    Set(ByVal value As Int32)
      m_jackpot_pus_unit = value
    End Set
  End Property

  Public Property Jackpot_Limit_Quantity() As Int32
    Get

      Return m_jackpot_limit_quantity
    End Get
    Set(ByVal value As Int32)
      m_jackpot_limit_quantity = value
    End Set
  End Property

  Public Property Jackpot_Limit_Amount() As Decimal
    Get

      Return m_jackpot_limit_amount
    End Get
    Set(ByVal value As Decimal)
      m_jackpot_limit_amount = value
    End Set
  End Property

  Public Property Canceled_Credit_Enabled() As Boolean
    Get

      Return m_canceled_credits_enabled
    End Get
    Set(ByVal value As Boolean)
      m_canceled_credits_enabled = value
    End Set
  End Property

  Public Property Canceled_Credit_PUS_Value() As Int64
    Get

      Return m_canceled_credits_pus_value
    End Get
    Set(ByVal value As Int64)
      m_canceled_credits_pus_value = value
    End Set
  End Property

  Public Property Canceled_Credit_PUS_Unit() As Int32
    Get

      Return m_canceled_credits_pus_unit
    End Get
    Set(ByVal value As Int32)
      m_canceled_credits_pus_unit = value
    End Set
  End Property

  Public Property Canceled_Credit_Limit_Quantity() As Int32
    Get

      Return m_canceled_credits_limit_quantity
    End Get
    Set(ByVal value As Int32)
      m_canceled_credits_limit_quantity = value
    End Set
  End Property

  Public Property Canceled_Credit_Limit_Amount() As Decimal
    Get

      Return m_canceled_credits_limit_amount
    End Get
    Set(ByVal value As Decimal)
      m_canceled_credits_limit_amount = value
    End Set
  End Property

  Public Property Terminal_Activity_Enabled() As Boolean
    Get
      Return m_terminal_activity_enabled
    End Get
    Set(ByVal value As Boolean)
      m_terminal_activity_enabled = value
    End Set
  End Property

  Public Property Terminal_Activity_PUS_Value() As Int64
    Get
      Return m_terminal_activity_pus_value
    End Get
    Set(ByVal value As Int64)
      m_terminal_activity_pus_value = value
    End Set
  End Property

  Public Property Terminal_Activity_PUS_Unit() As Int32
    Get
      Return m_terminal_activity_pus_unit
    End Get
    Set(ByVal value As Int32)
      m_terminal_activity_pus_unit = value
    End Set
  End Property
  Public Property Highroller_Enabled() As Boolean
    Get
      Return m_highroller_enabled
    End Get
    Set(ByVal value As Boolean)
      m_highroller_enabled = value
    End Set
  End Property

  Public Property Highroller_PUS_Value() As Int64
    Get
      Return m_highroller_pus_value
    End Get
    Set(ByVal value As Int64)
      m_highroller_pus_value = value
    End Set
  End Property

  Public Property Highroller_PUS_Unit() As Int32
    Get
      Return m_highroller_pus_unit
    End Get
    Set(ByVal value As Int32)
      m_highroller_pus_unit = value
    End Set
  End Property

  Public Property Highroller_Bet_Average_Amount() As Decimal
    Get

      Return m_highroller_bet_average_amount
    End Get
    Set(ByVal value As Decimal)
      m_highroller_bet_average_amount = value
    End Set
  End Property

  Public Property Highroller_Coin_In_Amount() As Decimal
    Get
      Return m_highroller_coin_in_amount
    End Get
    Set(ByVal value As Decimal)
      m_highroller_coin_in_amount = value
    End Set
  End Property

  Public Property Highroller_ProcessAnonymous() As Boolean
    Get
      Return m_highroller_process_anonymous
    End Get
    Set(ByVal value As Boolean)
      m_highroller_process_anonymous = value
    End Set
  End Property

#End Region

#Region " Overrides "

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _yes_text As String
    Dim _no_text As String
    Dim _array_time() As String
    Dim _str_text As String

    ReDim _array_time(4)
    _str_text = String.Empty

    _array_time(0) = AUDIT_NONE_STRING
    _array_time(1) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2582).ToLower ' hours
    _array_time(2) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(299).ToLower ' days
    _array_time(3) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6895).ToLower ' minutes

    _yes_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
    _no_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)

    Call _auditor_data.SetName(0, "", GLB_NLS_GUI_PLAYER_TRACKING.GetString(4859)) '"Alarmas"

    'JUGADO
    Call _auditor_data.SetField(0, IIf(Me.m_played_enabled, _yes_text, _no_text), GLB_NLS_GUI_PLAYER_TRACKING.GetString(537))

    Call _auditor_data.SetField(0, Me.m_played_pus_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(537) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4854))
    Call _auditor_data.SetField(0, _array_time(Me.m_played_pus_unit), GLB_NLS_GUI_PLAYER_TRACKING.GetString(537) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4854))

    If (Me.m_played_limit_quantity >= 0) Then
      Call _auditor_data.SetField(0, Me.m_played_limit_quantity, GLB_NLS_GUI_PLAYER_TRACKING.GetString(537) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4844))
    Else
      Call _auditor_data.SetField(0, AUDIT_NONE_STRING, GLB_NLS_GUI_PLAYER_TRACKING.GetString(537) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4844))
    End If

    If (Me.m_played_limit_amount >= 0) Then
      Call _auditor_data.SetField(0, Me.m_played_limit_amount, GLB_NLS_GUI_PLAYER_TRACKING.GetString(537) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2916))
    Else
      Call _auditor_data.SetField(0, AUDIT_NONE_STRING, GLB_NLS_GUI_PLAYER_TRACKING.GetString(537) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2916))
    End If

    'WON
    Call _auditor_data.SetField(0, IIf(Me.m_won_enabled, _yes_text, _no_text), GLB_NLS_GUI_PLAYER_TRACKING.GetString(798))

    Call _auditor_data.SetField(0, Me.m_won_pus_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(798) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4854))
    Call _auditor_data.SetField(0, _array_time(Me.m_won_pus_unit), GLB_NLS_GUI_PLAYER_TRACKING.GetString(798) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4854))

    If (Me.m_won_limit_quantity >= 0) Then
      Call _auditor_data.SetField(0, Me.m_won_limit_quantity, GLB_NLS_GUI_PLAYER_TRACKING.GetString(798) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4844))
    Else
      Call _auditor_data.SetField(0, AUDIT_NONE_STRING, GLB_NLS_GUI_PLAYER_TRACKING.GetString(798) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4844))
    End If

    If (Me.m_won_limit_amount >= 0) Then
      Call _auditor_data.SetField(0, Me.m_won_limit_amount, GLB_NLS_GUI_PLAYER_TRACKING.GetString(798) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2916))
    Else
      Call _auditor_data.SetField(0, AUDIT_NONE_STRING, GLB_NLS_GUI_PLAYER_TRACKING.GetString(798) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2916))
    End If

    'JACKPOT
    Call _auditor_data.SetField(0, IIf(Me.m_jackpot_enabled, _yes_text, _no_text), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2686))

    Call _auditor_data.SetField(0, _array_time(Me.m_jackpot_pus_unit), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2686) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4854))
    Call _auditor_data.SetField(0, Me.m_jackpot_pus_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2686) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4854))

    If (Me.m_jackpot_limit_quantity >= 0) Then
      Call _auditor_data.SetField(0, Me.m_jackpot_limit_quantity, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2686) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4844))
    Else
      Call _auditor_data.SetField(0, AUDIT_NONE_STRING, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2686) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4844))
    End If

    If (Me.m_jackpot_limit_amount >= 0) Then
      Call _auditor_data.SetField(0, Me.m_jackpot_limit_amount, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2686) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2916))
    Else
      Call _auditor_data.SetField(0, AUDIT_NONE_STRING, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2686) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2916))
    End If

    'CANCELED CREDITS
    Call _auditor_data.SetField(0, IIf(Me.m_canceled_credits_enabled, _yes_text, _no_text), GLB_NLS_GUI_PLAYER_TRACKING.GetString(4857))

    Call _auditor_data.SetField(0, _array_time(Me.m_canceled_credits_pus_unit), GLB_NLS_GUI_PLAYER_TRACKING.GetString(4857) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4854))
    Call _auditor_data.SetField(0, Me.m_canceled_credits_pus_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4857) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4854))

    If (Me.m_canceled_credits_limit_quantity >= 0) Then
      Call _auditor_data.SetField(0, Me.m_canceled_credits_limit_quantity, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4857) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4844))
    Else
      Call _auditor_data.SetField(0, AUDIT_NONE_STRING, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4857) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4844))
    End If

    If (Me.m_canceled_credits_limit_amount >= 0) Then
      Call _auditor_data.SetField(0, Me.m_canceled_credits_limit_amount, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4857) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2916))
    Else
      Call _auditor_data.SetField(0, AUDIT_NONE_STRING, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4857) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2916))
    End If

    'PAYOUT
    Call _auditor_data.SetField(0, IIf(Me.m_payout_enabled, _yes_text, _no_text), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1924))

    Call _auditor_data.SetField(0, _array_time(Me.m_payout_pus_unit), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1924) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4854))
    Call _auditor_data.SetField(0, Me.m_payout_pus_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1924) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4854))
    Call _auditor_data.SetField(0, Me.m_payout_limit_lower, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1924) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4841))
    Call _auditor_data.SetField(0, Me.m_payout_limit_upper, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1924) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4842))

    'COUNTERFEITS
    Call _auditor_data.SetField(0, IIf(Me.m_counterfeits_enabled, _yes_text, _no_text), GLB_NLS_GUI_PLAYER_TRACKING.GetString(4843))

    Call _auditor_data.SetField(0, _array_time(Me.m_counterfeits_pus_unit), GLB_NLS_GUI_PLAYER_TRACKING.GetString(4843) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4854))
    Call _auditor_data.SetField(0, Me.m_counterfeits_pus_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4843) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4854))

    If (Me.m_counterfeits_limit_quantity >= 0) Then
      Call _auditor_data.SetField(0, Me.m_counterfeits_limit_quantity, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4843) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4844))
    Else
      Call _auditor_data.SetField(0, AUDIT_NONE_STRING, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4843) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4844))
    End If

    'TERMINAL ACTIVITY  
    Call _auditor_data.SetField(0, IIf(Me.m_terminal_activity_enabled, _yes_text, _no_text), GLB_NLS_GUI_PLAYER_TRACKING.GetString(4852))

    Call _auditor_data.SetField(0, _array_time(Me.m_terminal_activity_pus_unit), GLB_NLS_GUI_PLAYER_TRACKING.GetString(4852) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4854))
    Call _auditor_data.SetField(0, Me.m_terminal_activity_pus_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4852) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4854))


    'HIGHROLLER
    Call _auditor_data.SetField(0, IIf(Me.m_highroller_enabled, _yes_text, _no_text), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6892))

    Call _auditor_data.SetField(0, Me.m_highroller_pus_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6892) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4854))
    Call _auditor_data.SetField(0, _array_time(Me.m_highroller_pus_unit), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6892) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4854))

    If (Me.m_highroller_bet_average_amount >= 0) Then
      Call _auditor_data.SetField(0, Me.m_highroller_bet_average_amount, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6892) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6893))
    Else
      Call _auditor_data.SetField(0, AUDIT_NONE_STRING, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6892) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6893))
    End If

    If (Me.m_highroller_coin_in_amount >= 0) Then
      Call _auditor_data.SetField(0, Me.m_highroller_coin_in_amount, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6892) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1387))
    Else
      Call _auditor_data.SetField(0, AUDIT_NONE_STRING, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6892) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1387))
    End If

    Call _auditor_data.SetField(0, IIf(Me.m_highroller_process_anonymous, _yes_text, _no_text), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6892))


    Return _auditor_data

  End Function

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Return ENUM_STATUS.STATUS_NOT_SUPPORTED

  End Function

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Return ENUM_STATUS.STATUS_NOT_SUPPORTED

  End Function

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As ENUM_STATUS

    Using _db_trx As New DB_TRX()

      _rc = Me.Read(_db_trx.SqlTransaction)

    End Using ' _db_trx

    Return _rc

  End Function

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As ENUM_STATUS

    Using _db_trx As New DB_TRX()

      _rc = Me.Update(_db_trx.SqlTransaction)
      _db_trx.Commit()

    End Using ' _db_trx

    Return _rc
  End Function

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE
    Dim _clone As CLS_CUSTOM_ALARMS

    _clone = New CLS_CUSTOM_ALARMS()

    'PAYOUT
    _clone.m_payout_enabled = Me.m_payout_enabled
    _clone.m_payout_pus_value = Me.m_payout_pus_value
    _clone.m_payout_pus_unit = Me.m_payout_pus_unit
    _clone.m_payout_limit_lower = Me.m_payout_limit_lower
    _clone.m_payout_limit_upper = Me.m_payout_limit_upper

    'PLAYED
    _clone.m_played_enabled = Me.m_played_enabled
    _clone.m_played_pus_value = Me.m_played_pus_value
    _clone.m_played_pus_unit = Me.m_played_pus_unit
    _clone.m_played_limit_amount = Me.m_played_limit_amount
    _clone.m_played_limit_quantity = Me.m_played_limit_quantity

    'WON
    _clone.m_won_enabled = Me.m_won_enabled
    _clone.m_won_pus_value = Me.m_won_pus_value
    _clone.m_won_pus_unit = Me.m_won_pus_unit
    _clone.m_won_limit_amount = Me.m_won_limit_amount
    _clone.m_won_limit_quantity = Me.m_won_limit_quantity

    'COUNTERFEIT
    _clone.m_counterfeits_enabled = Me.m_counterfeits_enabled
    _clone.m_counterfeits_pus_value = Me.m_counterfeits_pus_value
    _clone.m_counterfeits_pus_unit = Me.m_counterfeits_pus_unit
    _clone.m_counterfeits_limit_quantity = Me.m_counterfeits_limit_quantity

    'JACKPOT
    _clone.m_jackpot_enabled = Me.m_jackpot_enabled
    _clone.m_jackpot_pus_value = Me.m_jackpot_pus_value
    _clone.m_jackpot_pus_unit = Me.m_jackpot_pus_unit
    _clone.m_jackpot_limit_amount = Me.m_jackpot_limit_amount
    _clone.m_jackpot_limit_quantity = Me.m_jackpot_limit_quantity

    'CANCELCREDIT
    _clone.m_canceled_credits_enabled = Me.m_canceled_credits_enabled
    _clone.m_canceled_credits_pus_value = Me.m_canceled_credits_pus_value
    _clone.m_canceled_credits_pus_unit = Me.m_canceled_credits_pus_unit
    _clone.m_canceled_credits_limit_amount = Me.m_canceled_credits_limit_amount
    _clone.m_canceled_credits_limit_quantity = Me.m_canceled_credits_limit_quantity

    'TERMINAL ACTIVITY
    _clone.m_terminal_activity_enabled = Me.m_terminal_activity_enabled
    _clone.m_terminal_activity_pus_value = Me.m_terminal_activity_pus_value
    _clone.m_terminal_activity_pus_unit = Me.m_terminal_activity_pus_unit

    'HIGHROLLER
    _clone.m_highroller_enabled = Me.m_highroller_enabled
    _clone.m_highroller_pus_value = Me.m_highroller_pus_value
    _clone.m_highroller_pus_unit = Me.m_highroller_pus_unit
    _clone.m_highroller_bet_average_amount = Me.m_highroller_bet_average_amount
    _clone.m_highroller_coin_in_amount = Me.m_highroller_coin_in_amount
    _clone.m_highroller_process_anonymous = Me.m_highroller_process_anonymous

    Return _clone
  End Function

#End Region

#Region "Private functions"

  Private Function Read(ByVal Trx As SqlTransaction)

    Dim _gp As GeneralParam.Dictionary

    'Populate General Params dictionary with the current database values
    _gp = GeneralParam.Dictionary.Create(Trx)

    Try
      'PAYOUT
      m_payout_enabled = _gp.GetBoolean("Alarms", "Payout.Enabled")
      m_payout_pus_value = _gp.GetInt64("Alarms", "Payout.PeriodUnderStudy.Value")
      m_payout_pus_unit = _gp.GetInt64("Alarms", "Payout.PeriodUnderStudy.Unit")
      m_payout_limit_lower = _gp.GetDecimal("Alarms", "Payout.Limit.Lower")
      m_payout_limit_upper = _gp.GetDecimal("Alarms", "Payout.Limit.Upper")

      m_played_enabled = _gp.GetBoolean("Alarms", "Played.Enabled")
      m_played_pus_value = _gp.GetInt64("Alarms", "Played.PeriodUnderStudy.Value")
      m_played_pus_unit = _gp.GetInt64("Alarms", "Played.PeriodUnderStudy.Unit")
      m_played_limit_amount = _gp.GetDecimal("Alarms", "Played.Limit.Amount")
      m_played_limit_quantity = _gp.GetInt64("Alarms", "Played.Limit.Quantity")

      m_won_enabled = _gp.GetBoolean("Alarms", "Won.Enabled")
      m_won_pus_value = _gp.GetInt64("Alarms", "Won.PeriodUnderStudy.Value")
      m_won_pus_unit = _gp.GetInt64("Alarms", "Won.PeriodUnderStudy.Unit")
      m_won_limit_amount = _gp.GetDecimal("Alarms", "Won.Limit.Amount")
      m_won_limit_quantity = _gp.GetInt64("Alarms", "Won.Limit.Quantity")

      m_counterfeits_enabled = _gp.GetBoolean("Alarms", "Counterfeit.Enabled")
      m_counterfeits_pus_value = _gp.GetInt64("Alarms", "Counterfeit.PeriodUnderStudy.Value")
      m_counterfeits_pus_unit = _gp.GetInt64("Alarms", "Counterfeit.PeriodUnderStudy.Unit")
      m_counterfeits_limit_quantity = _gp.GetInt64("Alarms", "Counterfeit.Limit.Quantity")

      m_jackpot_enabled = _gp.GetBoolean("Alarms", "Jackpot.Enabled")
      m_jackpot_pus_value = _gp.GetInt64("Alarms", "Jackpot.PeriodUnderStudy.Value")
      m_jackpot_pus_unit = _gp.GetInt64("Alarms", "Jackpot.PeriodUnderStudy.Unit")
      m_jackpot_limit_amount = _gp.GetDecimal("Alarms", "Jackpot.Limit.Amount")
      m_jackpot_limit_quantity = _gp.GetInt64("Alarms", "Jackpot.Limit.Quantity")

      m_canceled_credits_enabled = _gp.GetBoolean("Alarms", "CanceledCredit.Enabled")
      m_canceled_credits_pus_value = _gp.GetInt64("Alarms", "CanceledCredit.PeriodUnderStudy.Value")
      m_canceled_credits_pus_unit = _gp.GetInt64("Alarms", "CanceledCredit.PeriodUnderStudy.Unit")
      m_canceled_credits_limit_amount = _gp.GetDecimal("Alarms", "CanceledCredit.Limit.Amount")
      m_canceled_credits_limit_quantity = _gp.GetInt64("Alarms", "CanceledCredit.Limit.Quantity")

      m_terminal_activity_enabled = _gp.GetBoolean("Alarms", "MachineWithoutPlays.Enabled")
      m_terminal_activity_pus_value = _gp.GetInt64("Alarms", "MachineWithoutPlays.PeriodUnderStudy.Value")
      m_terminal_activity_pus_unit = _gp.GetInt64("Alarms", "MachineWithoutPlays.PeriodUnderStudy.Unit")

      m_highroller_enabled = _gp.GetBoolean("Alarms", "HighRoller.Enabled")
      m_highroller_pus_value = _gp.GetInt64("Alarms", "HighRoller.PeriodUnderStudy.Value")
      m_highroller_pus_unit = _gp.GetInt64("Alarms", "HighRoller.PeriodUnderStudy.Unit")
      m_highroller_bet_average_amount = _gp.GetDecimal("Alarms", "HighRoller.Limit.BetAverage")
      m_highroller_coin_in_amount = _gp.GetDecimal("Alarms", "HighRoller.Limit.CoinIn")
      m_highroller_process_anonymous = _gp.GetBoolean("Alarms", "HighRoller.ProcessAnonymous")

      Return ENUM_STATUS.STATUS_OK

    Catch ex As Exception
      Log.Exception(ex)
    End Try

    Return ENUM_STATUS.STATUS_ERROR

  End Function

  Private Function Update(ByVal Trx As SqlTransaction) As ENUM_STATUS
    Dim _dic_key As Dictionary(Of String, String)

    Try
      _dic_key = New Dictionary(Of String, String)

      'PAYOUT
      _dic_key.Add("Payout.Enabled", IIf(m_payout_enabled, 1, 0))
      _dic_key.Add("Payout.PeriodUnderStudy.Value", m_payout_pus_value.ToString())
      _dic_key.Add("Payout.PeriodUnderStudy.Unit", m_payout_pus_unit.ToString())
      _dic_key.Add("Payout.Limit.Lower", m_payout_limit_lower.ToString())
      _dic_key.Add("Payout.Limit.Upper", m_payout_limit_upper.ToString())

      _dic_key.Add("Played.Enabled", IIf(m_played_enabled, 1, 0))
      _dic_key.Add("Played.PeriodUnderStudy.Value", m_played_pus_value.ToString())
      _dic_key.Add("Played.PeriodUnderStudy.Unit", m_played_pus_unit.ToString())
      _dic_key.Add("Played.Limit.Amount", m_played_limit_amount.ToString())
      _dic_key.Add("Played.Limit.Quantity", m_played_limit_quantity.ToString())

      _dic_key.Add("Won.Enabled", IIf(m_won_enabled, 1, 0))
      _dic_key.Add("Won.PeriodUnderStudy.Value", m_won_pus_value.ToString())
      _dic_key.Add("Won.PeriodUnderStudy.Unit", m_won_pus_unit.ToString())
      _dic_key.Add("Won.Limit.Amount", m_won_limit_amount.ToString())
      _dic_key.Add("Won.Limit.Quantity", m_won_limit_quantity.ToString())

      _dic_key.Add("Counterfeit.Enabled", IIf(m_counterfeits_enabled, 1, 0))
      _dic_key.Add("Counterfeit.PeriodUnderStudy.Value", m_counterfeits_pus_value.ToString())
      _dic_key.Add("Counterfeit.PeriodUnderStudy.Unit", m_counterfeits_pus_unit.ToString())
      _dic_key.Add("Counterfeit.Limit.Quantity", m_counterfeits_limit_quantity)

      _dic_key.Add("Jackpot.Enabled", IIf(m_jackpot_enabled, 1, 0))
      _dic_key.Add("Jackpot.PeriodUnderStudy.Value", m_jackpot_pus_value.ToString())
      _dic_key.Add("Jackpot.PeriodUnderStudy.Unit", m_jackpot_pus_unit.ToString())
      _dic_key.Add("Jackpot.Limit.Amount", m_jackpot_limit_amount.ToString())
      _dic_key.Add("Jackpot.Limit.Quantity", m_jackpot_limit_quantity.ToString())

      _dic_key.Add("CanceledCredit.Enabled", IIf(m_canceled_credits_enabled, 1, 0))
      _dic_key.Add("CanceledCredit.PeriodUnderStudy.Value", m_canceled_credits_pus_value.ToString())
      _dic_key.Add("CanceledCredit.PeriodUnderStudy.Unit", m_canceled_credits_pus_unit.ToString())
      _dic_key.Add("CanceledCredit.Limit.Amount", m_canceled_credits_limit_amount.ToString())
      _dic_key.Add("CanceledCredit.Limit.Quantity", m_canceled_credits_limit_quantity.ToString())

      _dic_key.Add("MachineWithoutPlays.Enabled", IIf(m_terminal_activity_enabled, 1, 0))
      _dic_key.Add("MachineWithoutPlays.PeriodUnderStudy.Value", m_terminal_activity_pus_value.ToString())
      _dic_key.Add("MachineWithoutPlays.PeriodUnderStudy.Unit", m_terminal_activity_pus_unit.ToString())

      _dic_key.Add("HighRoller.Enabled", IIf(m_highroller_enabled, 1, 0))
      _dic_key.Add("HighRoller.PeriodUnderStudy.Value", m_highroller_pus_value.ToString())
      _dic_key.Add("HighRoller.PeriodUnderStudy.Unit", m_highroller_pus_unit.ToString())
      _dic_key.Add("HighRoller.Limit.BetAverage", m_highroller_bet_average_amount.ToString())
      _dic_key.Add("HighRoller.Limit.CoinIn", m_highroller_coin_in_amount.ToString())
      _dic_key.Add("HighRoller.ProcessAnonymous", IIf(m_highroller_process_anonymous, 1, 0))

      For Each _dic As KeyValuePair(Of String, String) In _dic_key

        If Not DB_GeneralParam_Update("Alarms", _dic.Key, _dic.Value, Trx) Then

          Return ENUM_STATUS.STATUS_ERROR
        End If

      Next

      Return ENUM_STATUS.STATUS_OK

    Catch ex As Exception

      Log.Exception(ex)
    End Try

    Return ENUM_STATUS.STATUS_ERROR

  End Function

#End Region

End Class