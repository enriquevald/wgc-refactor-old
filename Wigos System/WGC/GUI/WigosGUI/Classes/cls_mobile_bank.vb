'-------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME :       cls_mobile_bank.vb
'
' DESCRIPTION :       Mobile bank class
'
' AUTHOR:             Didac Campanals
'
' CREATION DATE:      17-NOV-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 17-NOV-2014  DCS    Initial version   
'--------------------------------------------------------------------

#Region "Imports"
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common
#End Region ' Imports

Public Class cls_mobile_bank
  Inherits CLASS_BASE

#Region " Members "
  Protected m_mobile_bank As New WSI.Common.MBCardData
#End Region ' Members

#Region "Properties"

  Public Property ID() As Int32
    Get
      Return m_mobile_bank.CardId
    End Get
    Set(ByVal value As Int32)
      m_mobile_bank.CardId = value
    End Set
  End Property

  Public Property HolderName() As String
    Get
      Return m_mobile_bank.HolderName
    End Get
    Set(ByVal value As String)
      m_mobile_bank.HolderName = value
    End Set
  End Property

  Public Property EmployeeCode() As String
    Get
      Return m_mobile_bank.EmployeeCode
    End Get
    Set(ByVal value As String)
      m_mobile_bank.EmployeeCode = value
    End Set
  End Property

  Public Property Blocked() As Boolean
    Get
      Return m_mobile_bank.Blocked
    End Get
    Set(ByVal value As Boolean)
      m_mobile_bank.Blocked = value
    End Set
  End Property

  Public Property Pin() As String
    Get
      Return m_mobile_bank.Pin
    End Get
    Set(ByVal value As String)
      m_mobile_bank.Pin = value
    End Set
  End Property

  Public Property RechargeLimit() As Currency
    Get
      Return m_mobile_bank.RechargeLimit
    End Get
    Set(ByVal value As Currency)
      m_mobile_bank.RechargeLimit = value
    End Set
  End Property

  Public Property TotalLimit() As Currency
    Get
      Return m_mobile_bank.TotalLimit
    End Get
    Set(ByVal value As Currency)
      m_mobile_bank.TotalLimit = value
    End Set
  End Property

  Public Property NumberOfRechargesLimit() As Integer
    Get
      Return m_mobile_bank.NumberOfRechargesLimit
    End Get
    Set(ByVal value As Integer)
      m_mobile_bank.NumberOfRechargesLimit = value
    End Set
  End Property

  Public Property TrackData() As String
    Get
      Return m_mobile_bank.TrackData
    End Get
    Set(value As String)
      m_mobile_bank.TrackData = value
    End Set
  End Property

  Public Property UserId() As Integer
    Get
      Return m_mobile_bank.UserId
    End Get
    Set(value As Integer)
      m_mobile_bank.UserId = value
    End Set
  End Property

#End Region ' Properties

#Region "Overrides"

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA
    Dim _auditor_data As CLASS_AUDITOR_DATA

    Dim _null_code As String = "---"
    Dim _text_yes As String = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)  ' S�
    Dim _text_no As String = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)   ' No

    ' Initialize variables
    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)

    ' Set Name and Identifier
    ' Mobile bank  - 1182 - Original Name
    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(1182), Me.m_mobile_bank.CardId & " - " & Me.m_mobile_bank.HolderName)

    ' Name
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(253), Me.m_mobile_bank.HolderName)

    ' Employee code
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4704), Me.m_mobile_bank.EmployeeCode)

    ' PIN
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(620), Me.m_mobile_bank.Pin, , CLASS_AUDITOR_DATA.ENUM_FIELD_TYPE.FIELD_NO_DATA)

    ' Blocked
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(558), IIf((Me.m_mobile_bank.Blocked), _text_yes, _text_no))

    ' Limit by recharge
    If (Me.m_mobile_bank.RechargeLimit.SqlMoney > 0) Then
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5690), GUI_FormatCurrency(Me.m_mobile_bank.RechargeLimit, 2))
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5690), _null_code)
    End If

    ' Num of recharges limit
    If (Me.m_mobile_bank.NumberOfRechargesLimit > 0) Then
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5691), GUI_FormatNumber(Me.m_mobile_bank.NumberOfRechargesLimit, 0))
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5691), _null_code)
    End If

    ' Acumulated recharges limit
    If (Me.m_mobile_bank.TotalLimit.SqlMoney > 0) Then
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5698), GUI_FormatCurrency(Me.m_mobile_bank.TotalLimit, 2))
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(5698), _null_code)
    End If

    ' Trackdata
    Call _auditor_data.SetField(0, IIf(String.IsNullOrEmpty(Me.TrackData), AUDIT_NONE_STRING, Me.TrackData) _
                               , GLB_NLS_GUI_PLAYER_TRACKING.GetString(5467) & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5468))

    ' UserId
    If (Me.m_mobile_bank.UserId > 0) Then
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2253), GUI_FormatNumber(Me.m_mobile_bank.UserId))
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(2253), _null_code)
    End If

    Return _auditor_data
  End Function

  ''' <summary>
  ''' Delete mobile bank
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    If WSI.Common.MobileBank.DB_DeleteMobileBank(Me.m_mobile_bank) Then
      Return ENUM_STATUS.STATUS_OK
    Else
      Return ENUM_STATUS.STATUS_ERROR
    End If
  End Function

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    If WSI.Common.MobileBank.DB_InsertMobileBank(Me.m_mobile_bank) Then
      Return ENUM_STATUS.STATUS_OK
    Else
      Return ENUM_STATUS.STATUS_ERROR
    End If
  End Function

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Me.m_mobile_bank.CardId = ObjectId

    If WSI.Common.MobileBank.DB_MBCardGetAllData(Me.m_mobile_bank.CardId, Me.m_mobile_bank, Nothing) Then
      Return ENUM_STATUS.STATUS_OK
    Else
      Return ENUM_STATUS.STATUS_ERROR
    End If
  End Function

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    If WSI.Common.MobileBank.DB_UpdateMobileBank(Me.m_mobile_bank) Then
      Return ENUM_STATUS.STATUS_OK
    Else
      Return ENUM_STATUS.STATUS_ERROR
    End If
  End Function

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE
    Dim _target As cls_mobile_bank

    _target = New cls_mobile_bank

    _target.ID = Me.ID
    _target.HolderName = Me.HolderName
    _target.EmployeeCode = Me.EmployeeCode
    _target.Blocked = Me.Blocked
    _target.Pin = Me.Pin
    _target.RechargeLimit = Me.RechargeLimit
    _target.TotalLimit = Me.TotalLimit
    _target.NumberOfRechargesLimit = Me.NumberOfRechargesLimit
    _target.TrackData = Me.TrackData
    _target.UserId = Me.UserId

    Return _target
  End Function

  Public Function IsTrackDataValid(ByVal TrackData As String) As Boolean

    Dim _rc As Boolean
    Dim _internal_card_id As String = String.Empty
    Dim _card_type As Integer

    'Obtain internal card identifier from track data
    _rc = CardNumber.TrackDataToInternal(TrackData, _internal_card_id, _card_type)

    Return _rc
  End Function

#End Region ' Overrides

End Class
