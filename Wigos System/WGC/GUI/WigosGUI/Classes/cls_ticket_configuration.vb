'-------------------------------------------------------------------
' Copyright � 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_ticket_configuration.vb
'
' DESCRIPTION:   ticket print configuration
'
' AUTHOR:        Ferran Ortner
'
' CREATION DATE: 23-APR-2015
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 22-JUL-2015  FOS    Initial version.
' 07-AUG-2015  DCS    Refactoring and update audit code
' 20-AGO-2015  FOS    Add funcionality reprint.
' 05-OCT-2015  ETP    Fixed Bug 4373: Avoid to load HandPay validacion Param. Application won't crash if an error is returned.
' 19-JAN-2017  FOS    Fixed Bug 22330: Print ticket 2 times, when modify the user data.
'--------------------------------------------------------------------

Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Data.SqlClient
Imports WSI.Common
Imports System.Text

Public Class CLASS_TICKET_PRINT_CONFIGURATION
  Inherits CLASS_BASE


#Region "Constructor / Destructor"

  Public Sub New()

    Call MyBase.New()

  End Sub

  Protected Overrides Sub Finalize()
    MyBase.Finalize()
  End Sub

#End Region ' Constructor / Destructor

#Region " Members "

  Private m_dic_ticket_print_configurations As Dictionary(Of WSI.Common.OperationCode, WSI.Common.OperationVoucherParams.VoucherParameters)
  Private m_voucher_reprint_text As String
  Private m_voucher_print As String
  Private m_voucher_generate As String
  Private m_voucher_reprint As String

#End Region ' Members

#Region "Properties"

  Public Property TicketDicPrintConfiguration() As Dictionary(Of WSI.Common.OperationCode, WSI.Common.OperationVoucherParams.VoucherParameters)
    Get
      Return m_dic_ticket_print_configurations
    End Get
    Set(ByVal value As Dictionary(Of WSI.Common.OperationCode, WSI.Common.OperationVoucherParams.VoucherParameters))
      m_dic_ticket_print_configurations = value
    End Set
  End Property

  Public Property VoucherReprintText() As String
    Get
      Return m_voucher_reprint_text
    End Get
    Set(ByVal value As String)
      m_voucher_reprint_text = value
    End Set
  End Property

  Public Property VoucherPrint() As Boolean
    Get
      Return m_voucher_print
    End Get
    Set(ByVal value As Boolean)
      m_voucher_print = value
    End Set
  End Property

  Public Property VoucherGenerate() As Boolean
    Get
      Return m_voucher_generate
    End Get
    Set(ByVal value As Boolean)
      m_voucher_generate = value
    End Set
  End Property

  Public Property VoucherReprint() As Boolean
    Get
      Return m_voucher_reprint
    End Get
    Set(ByVal value As Boolean)
      m_voucher_reprint = value
    End Set
  End Property

#End Region ' Properties

#Region " Overrides functions "

  ' PURPOSE: Reads from the database into the given object
  '
  ' PARAMS:
  '   - INPUT:
  '         - ObjectId: An object, it must be 'casted' to the desired KEY.
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  ' NOTES:
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As ENUM_STATUS

    Dim _rc As Integer

    _rc = ReadTicketPrintConfiguration()

    Select Case _rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Read

  ' PURPOSE: Updates the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As ENUM_CONFIGURATION_RC

    _rc = UpdateOpertationVouchersParameters()

    Select Case _rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select
  End Function ' DB_Update

  ' PURPOSE: Can't insert the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function ' DB_Insert

  ' PURPOSE: Can't delete the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function ' DB_Delete

  ' PURPOSE: Returns a duplicate of the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE
    Dim _temp_configuration As CLASS_TICKET_PRINT_CONFIGURATION
    Dim _voucher As OperationVoucherParams.VoucherParameters

    _temp_configuration = New CLASS_TICKET_PRINT_CONFIGURATION()

    _temp_configuration.m_dic_ticket_print_configurations = New Dictionary(Of WSI.Common.OperationCode, WSI.Common.OperationVoucherParams.VoucherParameters)
    For Each _ticket_configuration As KeyValuePair(Of WSI.Common.OperationCode, WSI.Common.OperationVoucherParams.VoucherParameters) In Me.m_dic_ticket_print_configurations
      _voucher = New OperationVoucherParams.VoucherParameters(_ticket_configuration.Value.Generate, _ticket_configuration.Value.Reprint, _ticket_configuration.Value.PrintCopy)

      _temp_configuration.m_dic_ticket_print_configurations.Add(_ticket_configuration.Key, _voucher)
    Next

    _temp_configuration.VoucherPrint = m_voucher_print
    _temp_configuration.VoucherReprintText = m_voucher_reprint_text
    _temp_configuration.VoucherReprint = m_voucher_reprint
    _temp_configuration.VoucherGenerate = m_voucher_generate

    Return _temp_configuration
  End Function ' Duplicate 

  ' PURPOSE: Returns the related auditor data for the current object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA
    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _operation_code_str As String

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)

    Call _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(450), VoucherReprintText)
    'general params
    If VoucherPrint Then
      Call _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(230), GLB_NLS_GUI_CONFIGURATION.GetString(311))
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(230), GLB_NLS_GUI_CONFIGURATION.GetString(328))
    End If

    If VoucherReprint Then
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6277), GLB_NLS_GUI_CONFIGURATION.GetString(311))
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6277), GLB_NLS_GUI_CONFIGURATION.GetString(328))
    End If

    If VoucherGenerate Then
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6619), GLB_NLS_GUI_CONFIGURATION.GetString(311))
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6619), GLB_NLS_GUI_CONFIGURATION.GetString(328))
    End If

    ' grid 
    For Each ticket_configuration As KeyValuePair(Of WSI.Common.OperationCode, WSI.Common.OperationVoucherParams.VoucherParameters) In m_dic_ticket_print_configurations

      _operation_code_str = TranslateOperationType(ticket_configuration.Key)

      Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(6617), "")                                                         ' Redemption table

      If ticket_configuration.Value.Generate Then
        Call _auditor_data.SetField(0, GLB_NLS_GUI_CONFIGURATION.GetString(311), _operation_code_str & ": " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6619))
      Else
        Call _auditor_data.SetField(0, GLB_NLS_GUI_CONFIGURATION.GetString(328), _operation_code_str & ": " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6619))
      End If

      If ticket_configuration.Value.Reprint Then
        Call _auditor_data.SetField(0, GLB_NLS_GUI_CONFIGURATION.GetString(311), _operation_code_str & ": " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6277))
      Else
        Call _auditor_data.SetField(0, GLB_NLS_GUI_CONFIGURATION.GetString(328), _operation_code_str & ": " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6277))
      End If

      Call _auditor_data.SetField(0, IIf(ticket_configuration.Value.PrintCopy = 0, String.Empty, ticket_configuration.Value.PrintCopy), _operation_code_str & ": " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6623))

      If ticket_configuration.Value.Print Then
        Call _auditor_data.SetField(0, GLB_NLS_GUI_CONFIGURATION.GetString(311), _operation_code_str & ": " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6620))
      Else
        Call _auditor_data.SetField(0, GLB_NLS_GUI_CONFIGURATION.GetString(328), _operation_code_str & ": " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6620))
      End If
    Next

    Return _auditor_data

  End Function ' AuditorData

#End Region 'Overrides functions

#Region " Private Functions "

  ' PURPOSE: Read ticket print configuration
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function ReadTicketPrintConfiguration() As Integer

    m_dic_ticket_print_configurations = WSI.Common.OperationVoucherParams.OperationVoucherDictionary.Create()

    If (m_dic_ticket_print_configurations Is Nothing) Then
      Log.Error("VoucherOperations not initialized.")
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR
    End If

    VoucherReprintText = GetCashierVoucherData("ReprintText")
    VoucherPrint = GetCashierVoucherData("Print")
    VoucherGenerate = GetCashierVoucherData("Generate")
    VoucherReprint = GetCashierVoucherData("Reprint")

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function ' ReadTicketPrintConfiguration

  ' PURPOSE: Update DB Voucher parameters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function UpdateOpertationVouchersParameters() As ENUM_CONFIGURATION_RC
    Dim _str_sql As StringBuilder
    Dim _sql_trx As System.Data.SqlClient.SqlTransaction = Nothing
    Dim _update_result As ENUM_CONFIGURATION_RC
    Dim _voucher_parameters As WSI.Common.OperationVoucherParams.VoucherParameters
    Dim _rc As Integer

    Try

      Using _db_trx As New WSI.Common.DB_TRX()

        _update_result = UpdateGeneralParameters(_db_trx.SqlTransaction)
        If _update_result <> ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then

          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        For Each _ticket_print_configuration As KeyValuePair(Of WSI.Common.OperationCode, WSI.Common.OperationVoucherParams.VoucherParameters) In m_dic_ticket_print_configurations

          _str_sql = New StringBuilder

          _str_sql.AppendLine("   UPDATE OPERATION_VOUCHER_PARAMETERS    ")
          _str_sql.AppendLine("     SET  OVP_GENERATE       = @pGenerate      ")
          _str_sql.AppendLine("        , OVP_REPRINT        = @pReprint         ")
          _str_sql.AppendLine("        , OVP_PRINT_COPY     = @pNumPrintCopy  ")
          _str_sql.AppendLine("   WHERE  OVP_OPERATION_CODE = @pOperationCode ")

          Using _sql_command As New SqlCommand(_str_sql.ToString, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
            _voucher_parameters = _ticket_print_configuration.Value
            _sql_command.Parameters.Add("@pGenerate     ", SqlDbType.Bit).Value = _voucher_parameters.Generate
            _sql_command.Parameters.Add("@pReprint      ", SqlDbType.Bit).Value = _voucher_parameters.Reprint
            _sql_command.Parameters.Add("@pNumPrintCopy ", SqlDbType.Int).Value = _voucher_parameters.PrintCopy
            _sql_command.Parameters.Add("@pOperationCode", SqlDbType.Int).Value = _ticket_print_configuration.Key
            _rc = _sql_command.ExecuteNonQuery()

            If _rc <> 1 Then
              Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
            End If
          End Using
        Next

        _db_trx.Commit()
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
      End Using

    Catch ex As Exception
      Log.Exception(ex)
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
  End Function ' UpdateOpertationVouchersParameters

  ' PURPOSE: Update general params 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function UpdateGeneralParameters(_sql_trx As System.Data.SqlClient.SqlTransaction) As ENUM_CONFIGURATION_RC
    Dim _str_sql As StringBuilder

    _str_sql = New StringBuilder

    Try

      Using _sql_command As New SqlCommand(_str_sql.ToString, _sql_trx.Connection, _sql_trx)

        If Not DB_GeneralParam_Update("Cashier.Voucher", "ReprintText", m_voucher_reprint_text, _sql_trx) Then
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        If Not DB_GeneralParam_Update("Cashier.Voucher", "Print", IIf(m_voucher_print.Equals("True"), "1", "0"), _sql_trx) Then
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        If Not DB_GeneralParam_Update("Cashier.Voucher", "Reprint", IIf(m_voucher_reprint.Equals("True"), "1", "0"), _sql_trx) Then
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If

        If Not DB_GeneralParam_Update("Cashier.Voucher", "Generate", IIf(m_voucher_generate.Equals("True"), "1", "0"), _sql_trx) Then
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If
      End Using

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
    Catch ex As Exception
      Log.Exception(ex)
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

  End Function ' UpdateGeneralParameters

#End Region ' Private Functions

#Region " Public Functions "

  ' PURPOSE: Gets the movement name and options of each kind of movement type
  '
  '  PARAMS:
  '     - INPUT:
  '           - operation_type: the Code of the operation
  '     - OUTPUT:
  '           - unrecognized_movement: true if the movement doesn't have a personalized title
  '
  ' RETURNS:
  '     - A string with the name of the operation type
  Public Function TranslateOperationType(ByVal operation_type As Integer) As String
    Dim _str_type As String

    _str_type = ""

    Select Case operation_type

      Case WSI.Common.OperationCode.NOT_SET
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(670)

      Case WSI.Common.OperationCode.CHIPS_SALE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6600)

      Case WSI.Common.OperationCode.CHIPS_SALE_WITH_RECHARGE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6607)

      Case WSI.Common.OperationCode.CHIPS_PURCHASE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6610)

      Case WSI.Common.OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6611)

      Case WSI.Common.OperationCode.CASH_IN
        If TITO.Utils.IsTitoMode() Then
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2687)
        Else
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(607)
        End If

      Case WSI.Common.OperationCode.CASH_OUT
        If TITO.Utils.IsTitoMode() Then
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4607)
        Else
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5865)
        End If

      Case WSI.Common.OperationCode.PROMOTION
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(609)

      Case WSI.Common.OperationCode.MB_CASH_IN
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(610)

      Case WSI.Common.OperationCode.MB_PROMOTION
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(611)

      Case WSI.Common.OperationCode.HANDPAY
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1834)

      Case WSI.Common.OperationCode.GIFT_REQUEST
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(612)

      Case WSI.Common.OperationCode.GIFT_DELIVERY
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(613)

      Case WSI.Common.OperationCode.CANCEL_GIFT_INSTANCE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1788)

      Case WSI.Common.OperationCode.DRAW_TICKET
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(614)

      Case WSI.Common.OperationCode.GIFT_DRAW_TICKET
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(625)

      Case WSI.Common.OperationCode.GIFT_REDEEMABLE_AS_CASHIN
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2510)

      Case WSI.Common.OperationCode.ACCOUNT_PIN_RANDOM
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(621)

      Case WSI.Common.OperationCode.NA_CASH_IN
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(623)

      Case WSI.Common.OperationCode.NA_PROMOTION
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(624)

      Case WSI.Common.OperationCode.PROMOBOX_TOTAL_RECHARGES_PRINT
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1433)

      Case WSI.Common.OperationCode.CHIPS_PURCHASE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4310)

      Case WSI.Common.OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4313)

      Case WSI.Common.OperationCode.CHIPS_SALE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4311)

      Case WSI.Common.OperationCode.CHIPS_SALE_WITH_RECHARGE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4312)

      Case WSI.Common.OperationCode.TRANSFER_CREDIT_OUT
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4604)

      Case WSI.Common.OperationCode.CASH_ADVANCE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5461)

      Case WSI.Common.OperationCode.CASH_WITHDRAWAL
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5855)

      Case WSI.Common.OperationCode.CASH_DEPOSIT
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5859)

      Case WSI.Common.OperationCode.TITO_OFFLINE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3366)

      Case WSI.Common.OperationCode.TITO_TICKET_VALIDATION
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4716)

      Case WSI.Common.OperationCode.MB_DEPOSIT
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5862)

      Case WSI.Common.OperationCode.HANDPAY_CANCELLATION
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1835)

      Case WSI.Common.OperationCode.HANDPAY_VALIDATION
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6687)

        ' LA 12-04-2016
      Case WSI.Common.OperationCode.PROMOTION_WITH_TAXES
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7272)

      Case WSI.Common.OperationCode.PRIZE_PAYOUT
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7273)

      Case WSI.Common.OperationCode.PRIZE_PAYOUT_AND_RECHARGE
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7274)

      Case WSI.Common.OperationCode.ACCOUNT_PERSONALIZATION
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1711)

      Case Else
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(670)

    End Select
    Return _str_type
  End Function 'TranslateOperationType

#End Region ' Public Functions

End Class
