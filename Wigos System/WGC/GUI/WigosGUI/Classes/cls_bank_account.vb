'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   CLASS_BANK_ACCOUNT
' DESCRIPTION:   Bank accounts class
' AUTHOR:        Luis Ernesto Mesa
' CREATION DATE: 14-JAN-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 14-JAN-2013  LEM    Initial version
'--------------------------------------------------------------------

#Region " Imports "

Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Data.SqlClient
Imports WSI.Common
Imports System.Text

#End Region

Public Class CLASS_BANK_ACCOUNT
  Inherits CLASS_BASE

#Region " Enums "

  Enum BA_MEMBERS

    ID = 0
    BANK_NAME = 1
    DESCRIPTION = 2
    EFECTIVE_DAYS = 3
    CUSTOMER_NUMBER = 4
    ACCOUNT_NUMBER = 5
    PAYMENT_TYPE = 6
    ENABLED = 7

  End Enum

#End Region

#Region " Members "

  Dim m_id As Integer
  Dim m_bank_name As String
  Dim m_description As String
  Dim m_efective_days As String
  Dim m_customer_number As String
  Dim m_account_number As String
  Dim m_payment_type As String
  Dim m_enabled As Boolean
  Dim m_first_change As Boolean = True
  Dim m_bank_accounts_data As DataTable

#End Region

#Region " Properties "

  Public Property BankName() As String
    Get
      Return m_bank_name
    End Get
    Set(ByVal value As String)
      m_bank_name = value
    End Set
  End Property

  Public Property Description() As String
    Get
      Return m_description
    End Get
    Set(ByVal value As String)
      m_description = value
    End Set
  End Property

  Public Property EfectiveDays() As String
    Get
      Return m_efective_days
    End Get
    Set(ByVal value As String)
      m_efective_days = value
    End Set
  End Property

  Public Property CustomerNumber() As String
    Get
      Return m_customer_number
    End Get
    Set(ByVal value As String)
      m_customer_number = value
    End Set
  End Property

  Public Property AccountNumber() As String
    Get
      Return m_account_number
    End Get
    Set(ByVal value As String)
      m_account_number = value
    End Set
  End Property

  Public Property PaymentType() As String
    Get
      Return m_payment_type
    End Get
    Set(ByVal value As String)
      m_payment_type = value
    End Set
  End Property

  Public Property Enabled() As Boolean
    Get
      Return m_enabled
    End Get
    Set(ByVal value As Boolean)
      m_enabled = value
    End Set
  End Property

  Public Property Id() As Integer
    Get
      Return m_id
    End Get
    Set(ByVal value As Integer)
      If m_id = 0 Then
        m_id = value
      End If
    End Set
  End Property

#End Region

#Region " Overrides "

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns the related auditor data for the current object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)

    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(1562), Me.m_description)

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1560), GLB_NLS_GUI_AUDITOR.GetString(IIf(Me.m_enabled, 336, 337)))
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1554), Me.m_bank_name)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1555), Me.m_description)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1556), Me.m_efective_days)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1557), Me.m_customer_number)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1558), Me.m_account_number)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1595), Me.m_payment_type)

    Return _auditor_data

  End Function ' AuditorData

  '----------------------------------------------------------------------------
  ' PURPOSE: Deletes the given object from the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As ENUM_STATUS

    Dim _rc As ENUM_STATUS

    Using _db_trx As New DB_TRX
      _rc = Me.Delete(_db_trx.SqlTransaction)

      If _rc <> ENUM_STATUS.STATUS_OK Then
        Call _db_trx.Rollback()
      Else
        _rc = IIf(_db_trx.Commit() = True, ENUM_STATUS.STATUS_OK, ENUM_STATUS.STATUS_ERROR)
      End If

    End Using

    Return _rc

  End Function ' DB_Delete

  '----------------------------------------------------------------------------
  ' PURPOSE: Inserts the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As ENUM_STATUS

    Dim _rc As ENUM_STATUS

    Using _db_trx As New DB_TRX()

      _rc = Me.Save(_db_trx.SqlTransaction)

      If _rc <> ENUM_STATUS.STATUS_OK Then
        Call _db_trx.Rollback()
      Else
        _rc = IIf(_db_trx.Commit() = True, ENUM_STATUS.STATUS_OK, ENUM_STATUS.STATUS_ERROR)
      End If

    End Using

    Return _rc

  End Function ' DB_Insert

  '----------------------------------------------------------------------------
  ' PURPOSE: Updates the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As ENUM_STATUS

    Dim _rc As ENUM_STATUS

    Using _db_trx As New DB_TRX()

      _rc = Me.Save(_db_trx.SqlTransaction)

      If _rc <> ENUM_STATUS.STATUS_OK Then
        Call _db_trx.Rollback()
      Else
        _rc = IIf(_db_trx.Commit() = True, ENUM_STATUS.STATUS_OK, ENUM_STATUS.STATUS_ERROR)
      End If

    End Using

    Return _rc

  End Function ' DB_Update

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads from the database into the given object
  '
  ' PARAMS:
  '   - INPUT:
  '     - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As ENUM_STATUS

    Dim _rc As ENUM_STATUS

    Using _db_trx As New DB_TRX()

      Me.m_id = ObjectId
      _rc = Me.Read(_db_trx.SqlTransaction)

      If _rc <> ENUM_STATUS.STATUS_OK Then
        Call _db_trx.Rollback()
      Else
        _rc = IIf(_db_trx.Commit() = True, ENUM_STATUS.STATUS_OK, ENUM_STATUS.STATUS_ERROR)
      End If

    End Using ' _db_trx

    Return _rc

  End Function ' DB_Read

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns a duplicate of the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function Duplicate() As CLASS_BASE

    Dim _clone As CLASS_BANK_ACCOUNT

    _clone = New CLASS_BANK_ACCOUNT()

    _clone.m_id = Me.m_id
    _clone.m_bank_name = Me.m_bank_name
    _clone.m_description = Me.m_description
    _clone.m_efective_days = Me.m_efective_days
    _clone.m_customer_number = Me.m_customer_number
    _clone.m_account_number = Me.m_account_number
    _clone.m_payment_type = Me.m_payment_type
    _clone.m_enabled = Me.m_enabled

    Return _clone

  End Function ' Duplicate

#End Region

#Region " Privates "

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads an bank_account object from the database, identified
  '          by the property Id of this same instance.
  '
  ' PARAMS:
  '   - INPUT: Trx As SqlTransaction
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Private Function Read(ByVal Trx As SqlTransaction) As ENUM_STATUS

    Dim _idx_bank_account As Integer

    If m_first_change Then
      If Not WSI.Common.PaymentOrder.BanksAccounts(True, m_bank_accounts_data, Trx) Then
        Return ENUM_STATUS.STATUS_ERROR
      End If
      m_first_change = False
    End If

    Try
      For _idx_bank_account = 0 To m_bank_accounts_data.Rows.Count - 1
        If (m_bank_accounts_data.Rows(_idx_bank_account).Item(0) = Me.Id) Then

          Me.m_bank_name = m_bank_accounts_data.Rows(_idx_bank_account).Item(BA_MEMBERS.BANK_NAME) '_reader.GetString(BA_MEMBERS.BANK_NAME)
          Me.m_description = m_bank_accounts_data.Rows(_idx_bank_account).Item(BA_MEMBERS.DESCRIPTION) '_reader.GetString(BA_MEMBERS.DESCRIPTION)
          Me.m_efective_days = m_bank_accounts_data.Rows(_idx_bank_account).Item(BA_MEMBERS.EFECTIVE_DAYS) 'Me.m_efective_days = _reader.GetString(BA_MEMBERS.EFECTIVE_DAYS)
          Me.m_customer_number = m_bank_accounts_data.Rows(_idx_bank_account).Item(BA_MEMBERS.CUSTOMER_NUMBER) 'Me.m_customer_number = _reader.GetString(BA_MEMBERS.CUSTOMER_NUMBER)
          Me.m_account_number = m_bank_accounts_data.Rows(_idx_bank_account).Item(BA_MEMBERS.ACCOUNT_NUMBER) 'Me.m_account_number = _reader.GetString(BA_MEMBERS.ACCOUNT_NUMBER)
          Me.m_payment_type = m_bank_accounts_data.Rows(_idx_bank_account).Item(BA_MEMBERS.PAYMENT_TYPE) 'Me.m_payment_type = _reader.GetString(BA_MEMBERS.PAYMENT_TYPE)
          Me.m_enabled = True

        End If
      Next
    Catch _ex As Exception

      Return ENUM_STATUS.STATUS_ERROR

    End Try

    ''''Dim _sb As StringBuilder

    ''''Try
    ''''  _sb = New StringBuilder()


    ''''  _sb.AppendLine(" SELECT   BA_ACCOUNT_ID         ")
    ''''  _sb.AppendLine("        , BA_BANK_NAME          ")
    ''''  _sb.AppendLine("        , BA_DESCRIPTION        ")
    ''''  _sb.AppendLine("        , BA_EFFECTIVE_DAYS     ")
    ''''  _sb.AppendLine("        , BA_CUSTOMER_NUMBER    ")
    ''''  _sb.AppendLine("        , BA_ACCOUNT_NUMBER     ")
    ''''  _sb.AppendLine("        , BA_METHOD_PAYMENT     ")
    ''''  _sb.AppendLine("        , BA_ENABLED            ")
    ''''  _sb.AppendLine("   FROM   BANK_ACCOUNTS         ")
    ''''  _sb.AppendLine("  WHERE   BA_ACCOUNT_ID = @pId  ")

    ''''  Using _cmd As New SqlCommand(_sb.ToString(), Trx.Connection, Trx)

    ''''    _cmd.Parameters.Add("@pId", SqlDbType.Int).Value = Me.m_id

    ''''    Using _reader As SqlDataReader = _cmd.ExecuteReader()

    ''''      If Not _reader.HasRows() Then
    ''''        Return ENUM_STATUS.STATUS_NOT_FOUND
    ''''      End If

    ''''      While _reader.Read()

    ''''        Me.m_bank_name = _reader.GetString(BA_MEMBERS.BANK_NAME)
    ''''        Me.m_description = _reader.GetString(BA_MEMBERS.DESCRIPTION)
    ''''        Me.m_efective_days = _reader.GetString(BA_MEMBERS.EFECTIVE_DAYS)
    ''''        Me.m_customer_number = _reader.GetString(BA_MEMBERS.CUSTOMER_NUMBER)
    ''''        Me.m_account_number = _reader.GetString(BA_MEMBERS.ACCOUNT_NUMBER)
    ''''        Me.m_payment_type = _reader.GetString(BA_MEMBERS.PAYMENT_TYPE)
    ''''        Me.m_enabled = _reader.GetBoolean(BA_MEMBERS.ENABLED)

    ''''      End While

    ''''      _reader.Close()

    ''''    End Using '_reader

    ''''  End Using '_cmd

    ''''  Return ENUM_STATUS.STATUS_OK

    ''''Catch _ex As Exception

    ''''  Return ENUM_STATUS.STATUS_ERROR

    ''''End Try

  End Function ' Read

  '----------------------------------------------------------------------------
  ' PURPOSE: Inserts or updates an bank_account object from the database, 
  '          depending on the property Id of this same instance. 
  '
  ' PARAMS:
  '   - INPUT: Trx As SqlTransaction
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Private Function Save(ByVal Trx As SqlTransaction) As ENUM_STATUS

    Dim _sb As System.Text.StringBuilder
    Dim _id As SqlParameter

    Try

      _sb = New StringBuilder()

      If Me.m_id = 0 Then
        _sb.AppendLine(" INSERT INTO   BANK_ACCOUNTS              ")
        _sb.AppendLine("             ( BA_BANK_NAME               ")
        _sb.AppendLine("             , BA_DESCRIPTION             ")
        _sb.AppendLine("             , BA_EFFECTIVE_DAYS          ")
        _sb.AppendLine("             , BA_CUSTOMER_NUMBER         ")
        _sb.AppendLine("             , BA_ACCOUNT_NUMBER          ")
        _sb.AppendLine("             , BA_METHOD_PAYMENT          ")
        _sb.AppendLine("             , BA_ENABLED                 ")
        _sb.AppendLine("             )                            ")
        _sb.AppendLine("        VALUES                            ")
        _sb.AppendLine("             ( @pBankName                 ")
        _sb.AppendLine("             , @pDescription              ")
        _sb.AppendLine("             , @pEfectiveDays             ")
        _sb.AppendLine("             , @pCustomerNumber           ")
        _sb.AppendLine("             , @pAccountNumber            ")
        _sb.AppendLine("             , @pPaymentType              ")
        _sb.AppendLine("             , @pEnabled                  ")
        _sb.AppendLine("             )                            ")
        _sb.AppendLine("         SET   @pNewId = SCOPE_IDENTITY() ")
      Else
        _sb.AppendLine(" UPDATE   BANK_ACCOUNTS                         ")
        _sb.AppendLine("    SET   BA_BANK_NAME       = @pBankName       ")
        _sb.AppendLine("        , BA_DESCRIPTION     = @pDescription    ")
        _sb.AppendLine("        , BA_EFFECTIVE_DAYS  = @pEfectiveDays   ")
        _sb.AppendLine("        , BA_CUSTOMER_NUMBER = @pCustomerNumber ")
        _sb.AppendLine("        , BA_ACCOUNT_NUMBER  = @pAccountNumber  ")
        _sb.AppendLine("        , BA_METHOD_PAYMENT  = @pPaymentType    ")
        _sb.AppendLine("        , BA_ENABLED         = @pEnabled        ")
        _sb.AppendLine("  WHERE   BA_ACCOUNT_ID      = @pId             ")
      End If


      Using _cmd As New SqlCommand(_sb.ToString, Trx.Connection, Trx)

        _cmd.Parameters.Add("@pBankName", SqlDbType.NVarChar).Value = Me.m_bank_name
        _cmd.Parameters.Add("@pDescription", SqlDbType.NVarChar).Value = Me.m_description
        _cmd.Parameters.Add("@pEfectiveDays", SqlDbType.NVarChar).Value = Me.m_efective_days
        _cmd.Parameters.Add("@pCustomerNumber", SqlDbType.NVarChar).Value = Me.m_customer_number
        _cmd.Parameters.Add("@pAccountNumber", SqlDbType.NVarChar).Value = Me.m_account_number
        _cmd.Parameters.Add("@pPaymentType", SqlDbType.NVarChar).Value = Me.m_payment_type
        _cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).Value = Me.m_enabled

        _cmd.Parameters.Add("@pId", SqlDbType.Int).Value = Me.m_id

        _id = _cmd.Parameters.Add("@pNewId", SqlDbType.Int)
        _id.Direction = ParameterDirection.Output

        If _cmd.ExecuteNonQuery() <> 1 Then
          Return ENUM_STATUS.STATUS_ERROR
        End If

        If Me.m_id = 0 Then
          Me.m_id = _id.Value
        End If

      End Using

      Return ENUM_STATUS.STATUS_OK

    Catch _ex As Exception

      Return ENUM_STATUS.STATUS_ERROR

    End Try

  End Function ' Save

  '----------------------------------------------------------------------------
  ' PURPOSE: Deletes an bank_account object from the database, identified
  '          by the property Id of this same instance.
  '
  ' PARAMS:
  '   - INPUT: Trx As SqlTransaction
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Private Function Delete(ByVal Trx As SqlTransaction) As ENUM_STATUS

    Return ENUM_STATUS.STATUS_ERROR

  End Function ' Delete

#End Region

#Region " Publics "

  '----------------------------------------------------------------------------
  ' PURPOSE: Initializes the Bank_account object.
  '
  ' PARAMS:
  '   - INPUT: None
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '
  ' NOTES:
  Public Sub New()

    ' New objects have a default Id = 0
    Me.m_id = 0

  End Sub ' New

  '----------------------------------------------------------------------------
  ' PURPOSE: Read data to add into a selection combo
  '
  ' PARAMS:
  '   - INPUT: None
  '
  '   - OUTPUT: None
  '
  ' RETURNS: DataTable
  '
  ' NOTES:
  Public Shared Function FillComboAccounts() As DataTable

    Dim _table As DataTable
    Dim _sql As String

    _sql = " SELECT   BA_ACCOUNT_ID	      " & _
           "        , BA_DESCRIPTION      " & _
           "   FROM   BANK_ACCOUNTS       " & _
           "  WHERE   BA_ENABLED = 'TRUE' "

    _table = GUI_GetTableUsingSQL(_sql, 5000)

    Return _table

  End Function ' FillComboAccounts

  '--------------------------------------------------------------------------------------------------
  ' PURPOSE: Read all data from Bank_accounts table from database acording to BA_MEMBERS indexes
  '
  ' PARAMS:
  '   - INPUT: None
  '
  '   - OUTPUT: None
  '
  ' RETURNS: DataTable
  '
  ' NOTES:
  Public Shared Function GetAllAccounts() As DataTable

    Dim _table As DataTable
    Dim _sql As String

    _sql = " SELECT   BA_ACCOUNT_ID	     " & _
           "        , BA_BANK_NAME       " & _
           "        , BA_DESCRIPTION     " & _
           "        , BA_EFFECTIVE_DAYS  " & _
           "        , BA_CUSTOMER_NUMBER " & _
           "        , BA_ACCOUNT_NUMBER  " & _
           "        , BA_METHOD_PAYMENT  " & _
           "        , BA_ENABLED         " & _
           "   FROM   BANK_ACCOUNTS      "

    _table = GUI_GetTableUsingSQL(_sql, 5000)

    Return _table

  End Function ' GetAllAccounts

#End Region

End Class
