'-------------------------------------------------------------------
' Copyright � 2007-2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   CLASS_BONUS
' DESCRIPTION:   
' AUTHOR:        Javier Molina
' CREATION DATE: 19-FEB-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 19-FEB-2014  JMM    Initial version
'--------------------------------------------------------------------

Imports GUI_Controls
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports WSI.Common
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text

Public Class CLASS_BONUS
  Inherits CLASS_BASE

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
   Public Class TYPE_BONUS
    Public bonus_id As Long
    Public bonus_date As DateTime
    Public provider_name As String
    Public terminal_name As String
    Public transferred As Decimal
    Public current_wcp_status As BonusTransferStatus
    Public current_sas_host_status As frm_bonuses.SAS_HOST_STATUS
    Public to_transfer As Decimal
  End Class

#End Region ' Structures

#Region " Members "

  Protected m_bonus As New TYPE_BONUS

#End Region ' Members

#Region " Properties "

  Public Property BonusId() As Long
    Get
      Return Me.m_bonus.bonus_id
    End Get
    Set(ByVal value As Long)
      Me.m_bonus.bonus_id = value
    End Set
  End Property

  Public Property BonusDate() As DateTime
    Get
      Return Me.m_bonus.bonus_date
    End Get
    Set(ByVal value As DateTime)
      Me.m_bonus.bonus_date = value
    End Set
  End Property

  Public Property ProviderName() As String
    Get
      Return Me.m_bonus.provider_name
    End Get
    Set(ByVal value As String)
      Me.m_bonus.provider_name = value
    End Set
  End Property

  Public Property TerminalName() As String
    Get
      Return Me.m_bonus.terminal_name
    End Get
    Set(ByVal value As String)
      Me.m_bonus.terminal_name = value
    End Set
  End Property

  Public Property Transferred() As Decimal
    Get
      Return Me.m_bonus.transferred
    End Get
    Set(ByVal value As Decimal)
      Me.m_bonus.transferred = value
    End Set
  End Property

  Public Property CurrentWCPStatus() As BonusTransferStatus
    Get
      Return Me.m_bonus.current_wcp_status
    End Get
    Set(ByVal value As BonusTransferStatus)
      Me.m_bonus.current_wcp_status = value
    End Set
  End Property

  Public Property CurrentSASHostStatus() As frm_bonuses.BONUS_STATUS
    Get
      Return Me.m_bonus.current_sas_host_status
    End Get
    Set(ByVal value As frm_bonuses.BONUS_STATUS)
      Me.m_bonus.current_sas_host_status = value
    End Set
  End Property

  Public Property ToTransfer() As Decimal
    Get
      Return Me.m_bonus.to_transfer
    End Get
    Set(ByVal value As Decimal)
      Me.m_bonus.to_transfer = value
    End Set
  End Property

#End Region ' Properties

#Region " Publics "

  Public Sub New()
    m_bonus.bonus_id = 0
    m_bonus.bonus_date = DateTime.MinValue
    m_bonus.provider_name = ""
    m_bonus.terminal_name = ""
    m_bonus.transferred = 0
    m_bonus.current_wcp_status = BonusTransferStatus.Error
    m_bonus.current_sas_host_status = frm_bonuses.SAS_HOST_STATUS.BONUS_STATUS_UNKNOWN
    m_bonus.to_transfer = 0
  End Sub

#End Region ' Publics

#Region " Overrides "

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns the related auditor data for the current object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function AuditorData() As CLASS_AUDITOR_DATA

    Dim _auditor As CLASS_AUDITOR_DATA
    Dim _current_status As frm_bonuses.BONUS_STATUS

    _auditor = New CLASS_AUDITOR_DATA(AUDIT_CODE_BONUSES)

    Call _auditor.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(3389), GUI_FormatCurrency(Me.ToTransfer) & " - " & Me.BonusDate)

    'Current Status
    _current_status = frm_bonuses.TranslateWCPStatus(m_bonus.current_wcp_status)

    Call _auditor.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4676), GLB_NLS_GUI_PLAYER_TRACKING.GetString(frm_bonuses.GetStatusNLS(_current_status)))

    Return _auditor

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Deletes the given object from the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return ENUM_STATUS.STATUS_ERROR
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Inserts the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return ENUM_STATUS.STATUS_ERROR
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads from the database into the given object
  '
  ' PARAMS:
  '   - INPUT:
  '     - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As ENUM_STATUS

    Dim rc As Integer

    Me.BonusId = ObjectId

    rc = ReadBonus(m_bonus, SqlCtx)

    Select Case rc

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Updates the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim rc As Integer

    ' Update draw data
    rc = UpdateBonus(m_bonus, SqlCtx)

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Returns a duplicate of the given object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _data As CLASS_BONUS

    _data = New CLASS_BONUS()

    _data.BonusDate = Me.BonusDate
    _data.ProviderName = Me.ProviderName
    _data.TerminalName = Me.TerminalName
    _data.Transferred = Me.Transferred
    _data.CurrentWCPStatus = Me.CurrentWCPStatus
    _data.CurrentSASHostStatus = Me.CurrentSASHostStatus
    _data.ToTransfer = Me.ToTransfer

    Return _data

  End Function

#End Region ' Overrides

#Region " Private Functions "

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads the bonus indicated by Bonus.bonus_id from the database into the Bonus object
  '
  ' PARAMS:
  '   - INPUT:
  '     - Bonus As TYPE_BONUS (only field draw_id)
  '
  '   - OUTPUT:
  '     - Bonus As TYPE_BONUS (the rest of fields)
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Private Function ReadBonus(ByVal Bonus As TYPE_BONUS, ByVal Context As Integer) As Integer
    Dim _sql_connection As SqlConnection
    Dim _sql_command As SqlCommand
    Dim _str_sql As StringBuilder
    Dim _sql_reader As SqlDataReader
    Dim _num_rows As Integer

    Bonus.bonus_date = DateTime.MinValue
    Bonus.provider_name = ""
    Bonus.terminal_name = ""
    Bonus.transferred = 0
    Bonus.current_wcp_status = BonusTransferStatus.Error
    Bonus.current_sas_host_status = frm_bonuses.SAS_HOST_STATUS.BONUS_STATUS_UNKNOWN
    Bonus.to_transfer = 0

    Try
      _sql_connection = NewSQLConnection()

      If Not (_sql_connection Is Nothing) Then
        ' Connected
        If _sql_connection.State <> ConnectionState.Open Then
          ' Connecting ..
          _sql_connection.Open()
        End If

        If Not _sql_connection.State = ConnectionState.Open Then
          Exit Try
        End If
      End If

      _str_sql = New StringBuilder()

      _str_sql.AppendLine("SELECT   BNS_INSERTED ")
      _str_sql.AppendLine("       , TE_PROVIDER_ID ")
      _str_sql.AppendLine("       , TE_NAME ")
      _str_sql.AppendLine("       , BNS_TRANSFERRED_RE + BNS_TRANSFERRED_PROMO_RE + BNS_TRANSFERRED_PROMO_NR as BNS_TOTAL_TRANSFERRED ")
      _str_sql.AppendLine("       , BNS_TRANSFER_STATUS ")
      _str_sql.AppendLine("       , BNS_SAS_HOST_STATUS ")
      _str_sql.AppendLine("       , BNS_SAS_HOST_STATUS ")
      _str_sql.AppendLine("       , BNS_TO_TRANSFER_RE + BNS_TO_TRANSFER_PROMO_RE + BNS_TO_TRANSFER_PROMO_NR as BNS_TOTAL_TO_TRANSFER ")
      _str_sql.AppendLine("  FROM   BONUSES ")
      _str_sql.AppendLine(" INNER   JOIN TERMINALS ON TE_TERMINAL_ID = BNS_TARGET_TERMINAL_ID ")
      _str_sql.AppendLine(" WHERE   BNS_BONUS_ID = @pBonusId")

      _sql_command = New SqlCommand(_str_sql.ToString())
      _sql_command.Connection = _sql_connection

      _sql_command.Parameters.Add("@pBonusId", SqlDbType.BigInt).Value = Bonus.bonus_id

      _sql_reader = _sql_command.ExecuteReader()

      _num_rows = 0
      While _sql_reader.Read()

        Bonus.bonus_date = _sql_reader("BNS_INSERTED")
        Bonus.provider_name = _sql_reader("TE_PROVIDER_ID")
        Bonus.terminal_name = _sql_reader("TE_NAME")

        If Not IsDBNull(_sql_reader("BNS_TOTAL_TRANSFERRED")) Then
          Bonus.transferred = _sql_reader("BNS_TOTAL_TRANSFERRED")
        End If

        Bonus.current_wcp_status = _sql_reader("BNS_TRANSFER_STATUS")
        Bonus.current_sas_host_status = _sql_reader("BNS_SAS_HOST_STATUS")

        If Not IsDBNull(_sql_reader("BNS_TOTAL_TO_TRANSFER")) Then
          Bonus.to_transfer = _sql_reader("BNS_TOTAL_TO_TRANSFER")
        End If

        _num_rows = _num_rows + 1
      End While

      _sql_reader.Close()

      If _num_rows = 0 Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
      End If
      If _num_rows <> 1 Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

    Catch ex As Exception
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function ' ReadDraw

  '----------------------------------------------------------------------------
  ' PURPOSE: Updates the given Bonus to the database
  '
  ' PARAMS:
  '   - INPUT:
  '     - Bonus As TYPE_BONUS
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Private Function UpdateBonus(ByVal Bonus As TYPE_BONUS, ByVal Context As Integer) As Integer
    Dim _sql_command As SqlCommand
    Dim _str_sql As StringBuilder
    Dim _db_count As Integer = 0
    Dim _num_rows_updated As Integer

    'XVV 16/04/2007
    'Assign Nothing to variable because compiler generate a warning
    Dim SqlTrans As SqlTransaction = Nothing

    Try
      If Not GUI_BeginSQLTransaction(SqlTrans) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      _str_sql = New StringBuilder

      _str_sql.AppendLine("UPDATE   BONUSES ")
      _str_sql.AppendLine("   SET   BNS_TRANSFER_STATUS = @pTransferStatus")
      _str_sql.AppendLine("       , BNS_TRANSFERRED_RE  = @pTransferred")
      _str_sql.AppendLine(" WHERE   BNS_BONUS_ID = @pBonusId")

      _sql_command = New SqlCommand(_str_sql.ToString())
      _sql_command.Transaction = SqlTrans
      _sql_command.Connection = SqlTrans.Connection

      _sql_command.Parameters.Add("@pBonusId", SqlDbType.BigInt).Value = Bonus.bonus_id
      _sql_command.Parameters.Add("@pTransferStatus", SqlDbType.Int).Value = Bonus.current_wcp_status
      _sql_command.Parameters.Add("@pTransferred", SqlDbType.Money).Value = Bonus.transferred

      _num_rows_updated = _sql_command.ExecuteNonQuery()

      If _num_rows_updated <> 1 Then
        GUI_EndSQLTransaction(SqlTrans, False)

        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      If Not GUI_EndSQLTransaction(SqlTrans, True) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

    Catch ex As Exception
      GUI_EndSQLTransaction(SqlTrans, False)
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function ' UpdateBonus

#End Region ' Private Functions

End Class
