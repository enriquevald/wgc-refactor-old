﻿'-------------------------------------------------------------------
' Copyright © 2016 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_countr.vb
'
' DESCRIPTION : countr class for user edition
'              
' REVISION HISTORY :e
'
' Date         Author  Description
' -----------  ------  -----------------------------------------------
' 23-MAR-2016  YNM     Initial version
' 03-AUG-2016  JBP     Refactor
' 03-AUG-2016  JBP     Bug 17105: CountR: No se actualiza la columna "entregado" de la sesión de CountR cuando se cancela su recaudación asociada.
'                      Bug 17167:CountR: Duplicado de sesiones y otras inconsistencias
' 09-FEN-2017 JML      PBI 24378: Reabrir sesiones de caja y mesas de juego: rehacer reapertura y corrección de Bugs
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common
Imports WSI.Common.CountR

Public Class CLASS_COUNTR
  Inherits CLASS_BASE

#Region " Constants "

#End Region ' Constants

#Region " Enums "

#End Region ' Enums 

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_COUNTR
    Inherits CountR
  End Class

#End Region ' Structures

#Region " Members "

  Protected m_countr As New CountR
  Protected m_cashier_session_info As CashierSessionInfo
  Protected m_countr_session As CountRSession
  Private m_operation_code As OperationCode
  'Protected m_is_closing As Boolean
  'Protected m_movement_id As Integer
  'Protected m_operation_type As Cage.CageOperationType
  'Protected m_currencies As DataTable
  Private m_cashier_movement_detail As CASHIER_MOVEMENT
  Private m_cashier_movement_total As CASHIER_MOVEMENT
  Private m_cage_control As CLASS_CAGE_CONTROL

  Public m_retired As Boolean

#End Region ' Members

#Region " Properties "

  Public Property CashierSessionInfo As CashierSessionInfo
    Get
      Return m_cashier_session_info
    End Get
    Set(Value As CashierSessionInfo)
      m_cashier_session_info = Value
    End Set
  End Property

  Public Property Session As CountRSession
    Get
      Return m_countr_session
    End Get
    Set(Value As CountRSession)
      m_countr_session = Value
    End Set
  End Property

  Public Property Id() As Int32
    Get
      Return m_countr.Id
    End Get
    Set(Value As Int32)
      m_countr.Id = Value
    End Set
  End Property

  Public Property Code() As Int32
    Get
      Return m_countr.Code
    End Get
    Set(Value As Int32)
      m_countr.Code = Value
    End Set
  End Property

  Public Property Name() As String
    Get
      Return m_countr.Name
    End Get
    Set(Value As String)
      m_countr.Name = Value
    End Set
  End Property

  Public Property Description() As String
    Get
      Return m_countr.Description
    End Get
    Set(Value As String)
      m_countr.Description = Value
    End Set
  End Property

  Public Property Provider() As String
    Get
      Return m_countr.Provider
    End Get
    Set(Value As String)
      m_countr.Provider = Value
    End Set
  End Property

  Public Property AreaId() As Int32
    Get
      Return m_countr.AreaId
    End Get
    Set(Value As Int32)
      m_countr.AreaId = Value
    End Set
  End Property

  Public Property AreaName() As String
    Get
      Return m_countr.AreaName
    End Get
    Set(Value As String)
      m_countr.AreaName = Value
    End Set
  End Property

  Public Property Zone() As Boolean
    Get
      Return m_countr.Zone
    End Get
    Set(Value As Boolean)
      m_countr.Zone = Value
    End Set
  End Property

  Public Property BankId() As Int32
    Get
      Return m_countr.BankId
    End Get
    Set(Value As Int32)
      m_countr.BankId = Value
    End Set
  End Property

  Public Property BankName() As String
    Get
      Return m_countr.BankName
    End Get
    Set(Value As String)
      m_countr.BankName = Value
    End Set
  End Property

  Public Property FloorId() As String
    Get
      Return m_countr.FloorId
    End Get
    Set(Value As String)
      m_countr.FloorId = Value
    End Set
  End Property

  Public Property MaxPayment As Currency
    Get
      Return m_countr.MaxPayment
    End Get
    Set(Value As Currency)
      m_countr.MaxPayment = Value
    End Set
  End Property

  Public Property MinPayment As Currency
    Get
      Return m_countr.MinPayment
    End Get
    Set(Value As Currency)
      m_countr.MinPayment = Value
    End Set
  End Property

  Public Property IsoCode As String
    Get
      Return m_countr.IsoCode
    End Get
    Set(Value As String)
      m_countr.IsoCode = Value
    End Set
  End Property

  Public Property ShowLog As Boolean
    Get
      Return m_countr.ShowLog
    End Get
    Set(Value As Boolean)
      m_countr.ShowLog = Value
    End Set
  End Property

  Public Property CreateTicket As Boolean
    Get
      Return m_countr.CreateTicket
    End Get
    Set(Value As Boolean)
      m_countr.CreateTicket = Value
    End Set
  End Property

  Public Property Enabled As Boolean
    Get
      Return m_countr.Enabled
    End Get
    Set(Value As Boolean)
      m_countr.Enabled = Value
    End Set
  End Property

  Public Property Created As DateTime
    Get
      Return m_countr.Created
    End Get
    Set(Value As DateTime)
      m_countr.Created = Value
    End Set
  End Property

  Public Property LastModified As DateTime
    Get
      Return m_countr.LastModified
    End Get
    Set(Value As DateTime)
      m_countr.LastModified = Value
    End Set
  End Property

  Public Property RetirementDate As DateTime
    Get
      Return m_countr.RetirementDate
    End Get
    Set(Value As DateTime)
      m_countr.RetirementDate = Value
    End Set
  End Property

  Public Property RetirementUserId As Int64
    Get
      Return m_countr.RetirementUserId
    End Get
    Set(Value As Int64)
      m_countr.RetirementUserId = Value
    End Set
  End Property

  Public Property Retired As Boolean
    Get
      Return m_retired
    End Get
    Set(Value As Boolean)
      m_retired = Value
    End Set

  End Property

  Public Property RetirementUserName As String
    Get
      Return m_countr.RetirementUserName
    End Get
    Set(Value As String)
      m_countr.RetirementUserName = Value
    End Set
  End Property

  Public Property LastConnection As DateTime
    Get
      Return m_countr.LastConnection
    End Get
    Set(value As DateTime)
      m_countr.LastConnection = value
    End Set
  End Property

  Public Property Status As Integer
    Get
      Return m_countr.Status
    End Get
    Set(value As Integer)
      m_countr.Status = value
    End Set
  End Property

  Public Property IpAddress As String
    Get
      Return m_countr.IpAddress
    End Get
    Set(value As String)
      m_countr.IpAddress = value
    End Set
  End Property

  Public Property OptCode() As OperationCode
    Get
      Return m_operation_code
    End Get

    Set(ByVal Value As OperationCode)
      m_operation_code = Value
    End Set
  End Property ' CountROperationCode

  Public Property CashierMovementDetail() As CASHIER_MOVEMENT
    Get
      Return m_cashier_movement_detail
    End Get

    Set(ByVal Value As CASHIER_MOVEMENT)
      m_cashier_movement_detail = Value
    End Set
  End Property ' CountRCashierMovementDetail

  Public Property CashierMovementTotal() As CASHIER_MOVEMENT
    Get
      Return m_cashier_movement_total
    End Get

    Set(ByVal Value As CASHIER_MOVEMENT)
      m_cashier_movement_total = Value
    End Set
  End Property ' CountRCashierMovementTotal

  'Public Property IsClosing As Boolean
  '  Get
  '    Return m_is_closing
  '  End Get
  '  Set(Value As Boolean)
  '    m_is_closing = Value
  '  End Set
  'End Property ' IsClosing

  'Public Property MovementId As Integer
  '  Get
  '    Return m_movement_id
  '  End Get
  '  Set(Value As Integer)
  '    m_movement_id = Value
  '  End Set
  'End Property ' MovementId

  'Public Property OperationType As Cage.CageOperationType
  '  Get
  '    Return m_operation_type
  '  End Get
  '  Set(Value As Cage.CageOperationType)
  '    m_operation_type = Value
  '  End Set
  'End Property ' OperationType

  'Public Property Currencies As DataTable
  '  Get
  '    Return m_currencies
  '  End Get
  '  Set(Value As DataTable)
  '    m_currencies = Value
  '  End Set
  'End Property ' Currencies

  Public Property CageControl As CLASS_CAGE_CONTROL
    Get
      Return m_cage_control
    End Get
    Set(Value As CLASS_CAGE_CONTROL)
      m_cage_control = Value
    End Set
  End Property ' CageControl

#End Region ' Properties

#Region " Public Functions "

  ''' <summary>
  ''' Returns the related auditor data for the current object.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function AuditorData() As CLASS_AUDITOR_DATA
    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _field_value As String

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)

    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(7226), Me.Name)                             ' "CountR"

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7227), Me.Code.ToString())                 ' "Código"

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7228), Me.Name)                            ' "Nombre"  

    _field_value = IIf(String.IsNullOrEmpty(Me.Description), AUDIT_NONE_STRING, Me.Description)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7235), _field_value)                       ' "Descripción"

    _field_value = IIf(String.IsNullOrEmpty(Me.Provider), AUDIT_NONE_STRING, Me.Provider)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7236), _field_value)                       ' "Proveedor"

    _field_value = IIf(Me.BankName = String.Empty, AUDIT_NONE_STRING, Me.BankName + " (" + Me.AreaName + ")")
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7232), _field_value)                       ' "Isla" (435 "Area")

    _field_value = IIf(String.IsNullOrEmpty(Me.FloorId), AUDIT_NONE_STRING, Me.FloorId)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7233), _field_value)                       ' " floor Id

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7237), Me.IsoCode)                         ' "IsoCode" (435 "Area")

    _field_value = IIf(Me.ShowLog, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6091), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6092))
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7325), _field_value)                       ' "ShowLog"

    _field_value = IIf(Me.CreateTicket, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6091), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6092))
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7331), _field_value)                       ' "CreateTicket"

    _field_value = IIf(Me.Enabled, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6091), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6092))
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7234), _field_value)                       ' "Habilitada"

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7239), Me.MaxPayment.ToString())           ' "Max Payment"

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7240), Me.MinPayment.ToString())           ' "Min Payment"

    _field_value = IIf(Me.RetirementDate = DateTime.MinValue, AUDIT_NONE_STRING, Me.RetirementDate.ToString())
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7242), _field_value)                       ' "Fecha de retiro"

    _field_value = IIf(Me.RetirementDate = DateTime.MinValue, AUDIT_NONE_STRING, Me.RetirementUserName)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7243), _field_value)                       ' "Usuario de Retiro"

    _field_value = IIf(String.IsNullOrEmpty(Me.IpAddress), AUDIT_NONE_STRING, Me.IpAddress)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1897), _field_value)                       'IP Address

    Return _auditor_data

  End Function ' AuditorData

  ''' <summary>
  ''' Deletes the given object from the database.
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS
    Dim _sql_tx As System.Data.SqlClient.SqlTransaction = Nothing

    Try

      If Not GUI_BeginSQLTransaction(_sql_tx) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      If Not m_countr.DB_Delete(_sql_tx) Then
        GUI_EndSQLTransaction(_sql_tx, False)
        Return ENUM_STATUS.STATUS_ERROR

      Else
        ' Commit
        If Not GUI_EndSQLTransaction(_sql_tx, True) Then
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If
      End If

    Catch ex As Exception
      ' Rollback  
      GUI_EndSQLTransaction(_sql_tx, False)
      Log.Error(String.Format("CountR DB_Delete: There was an error deleting CountR NAME: {0}, CODE:{1}", Name, Code.ToString()))
      Log.Exception(ex)
      Return ENUM_STATUS.STATUS_ERROR
    End Try

    Return ENUM_STATUS.STATUS_OK

  End Function

  ''' <summary>
  ''' Reads from the database into the given object
  ''' </summary>
  ''' <param name="ObjectId"></param>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Read(ObjectId As Object, ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS
    Dim _sql_tx As System.Data.SqlClient.SqlTransaction = Nothing

    Try

      If Not GUI_BeginSQLTransaction(_sql_tx) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      Me.Id = ObjectId

      If Not CountR.DB_GetCountR(Me.Id, m_countr, _sql_tx) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      If Not GUI_EndSQLTransaction(_sql_tx, True) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

    Catch ex As Exception
      ' Rollback  
      GUI_EndSQLTransaction(_sql_tx, False)
      Log.Error(String.Format("CountR DB_GetCountR: There was an error getting CountR NAME: {0}, CODE:{1}", Name, Code.ToString()))
      Log.Exception(ex)
      Return ENUM_STATUS.STATUS_ERROR
    End Try

    Return ENUM_STATUS.STATUS_OK

  End Function ' DB_Read

  ''' <summary>
  ''' Inserts the given object to the database.
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS
    Dim _sql_tx As System.Data.SqlClient.SqlTransaction = Nothing

    Try

      If Not GUI_BeginSQLTransaction(_sql_tx) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      If m_countr.CheckDuplicades(_sql_tx) Then
        GUI_EndSQLTransaction(_sql_tx, False)
        Return ENUM_STATUS.STATUS_ALREADY_EXISTS
      End If

      If Not m_countr.DB_Insert(_sql_tx) Then
        GUI_EndSQLTransaction(_sql_tx, False)
        Return ENUM_STATUS.STATUS_ERROR

      Else
        ' Commit
        If Not GUI_EndSQLTransaction(_sql_tx, True) Then
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If
      End If

    Catch ex As Exception
      ' Rollback  
      GUI_EndSQLTransaction(_sql_tx, False)
      Log.Error(String.Format("CountR DB_Insert: There was an error creating new CountR NAME: {0}, CODE:{1}", Name, Code.ToString()))
      Log.Exception(ex)
      Return ENUM_STATUS.STATUS_ERROR
    End Try

    Return ENUM_STATUS.STATUS_OK

  End Function ' DB_Insert

  ''' <summary>
  ''' Updates the given object to the database.
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS

    Dim _sql_tx As System.Data.SqlClient.SqlTransaction = Nothing

    Try

      If Not GUI_BeginSQLTransaction(_sql_tx) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

      If m_countr.CheckDuplicades(_sql_tx) Then
        GUI_EndSQLTransaction(_sql_tx, False)
        Return ENUM_STATUS.STATUS_ALREADY_EXISTS
      End If

      If Not m_countr.DB_Update(_sql_tx) Then
        GUI_EndSQLTransaction(_sql_tx, False)
        Return ENUM_STATUS.STATUS_ERROR

      Else
        ' Commit
        If Not GUI_EndSQLTransaction(_sql_tx, True) Then
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If
      End If

    Catch ex As Exception
      ' Rollback  
      GUI_EndSQLTransaction(_sql_tx, False)

      Log.Error(String.Format("CountR DB_Update: There was an error updating CountR NAME: {0}, CODE:{1}", Name, Code.ToString()))
      Log.Exception(ex)
      Return ENUM_STATUS.STATUS_ERROR
    End Try

    Return ENUM_STATUS.STATUS_OK

  End Function ' DB_Update

  ''' <summary>
  ''' Returns a duplicate of the given object.
  ''' Not includes ID
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function Duplicate() As CLASS_BASE

    Dim _temp_object As CLASS_COUNTR

    _temp_object = New CLASS_COUNTR

    _temp_object.Id = Me.Id
    _temp_object.Code = Me.Code
    _temp_object.Name = Me.Name
    _temp_object.Description = Me.Description
    _temp_object.Provider = Me.Provider
    _temp_object.AreaId = Me.AreaId
    _temp_object.AreaName = Me.AreaName
    _temp_object.BankId = Me.BankId
    _temp_object.BankName = Me.BankName
    _temp_object.FloorId = Me.FloorId
    _temp_object.MaxPayment = Me.MaxPayment
    _temp_object.MinPayment = Me.MinPayment
    _temp_object.IsoCode = Me.IsoCode
    _temp_object.ShowLog = Me.ShowLog
    _temp_object.CreateTicket = Me.CreateTicket
    _temp_object.Enabled = Me.Enabled
    _temp_object.Created = Me.Created
    _temp_object.LastModified = Me.LastModified
    _temp_object.RetirementDate = Me.RetirementDate
    _temp_object.RetirementUserId = Me.RetirementUserId
    _temp_object.RetirementUserName = Me.RetirementUserName
    _temp_object.LastConnection = Me.LastConnection
    _temp_object.Status = Me.Status
    _temp_object.IpAddress = Me.IpAddress
    _temp_object.Zone = Me.Zone
    Return _temp_object

  End Function ' Duplicate

  ''' <summary>
  ''' Reset values new CountR copy
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub ResetIdValues()

    Me.Id = 0
    Me.Code = 0
    Me.Name = String.Empty
    Me.IpAddress = String.Empty

  End Sub ' ResetIdValues


#Region " CountR "


  ''' <summary>
  ''' Insert CountR movement and details into database
  ''' </summary>
  ''' <param name="SqlTrx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  ' JBP: TODO REFACTOR AND MOVE ALL CAGE_CONTROL COMMON FUNCTIONS TO SHARED CLASS
  Public Function AddMovementAndDetails(ByRef CageControl As CLASS_CAGE_CONTROL, ByVal SqlTrx As SqlTransaction) As ENUM_CONFIGURATION_RC

    Dim _result As ENUM_CONFIGURATION_RC

    _result = ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    CageControl.MovementDatetime = WGDB.Now

    Me.CageControl = CageControl
    CageControl.Status = Cage.CageStatus.OK

    ' Get CountR info
    If Not CountR.DB_GetCountR(Me.CageControl.CountR.Id, m_countr, SqlTrx) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    ' Check if selected CountR is enabled
    If Not Me.Enabled Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
    End If

    ' Get or open CashierSessionInfo and CountRSessionInfo. 
    ' FJC 08-08-2016
    If CageControl.CashierSessionId <= 0 Then
      CountRBusinessLogic.GetOrOpenCashierSessionInfoWithGUIUser(m_countr, GLB_CurrentUser.Id, GLB_CurrentUser.Name, SqlTrx, Me.CashierSessionInfo, Me.Session)
      CageControl.CashierSessionId = Me.CashierSessionInfo.CashierSessionId
    End If

    If CageControl.CountRCloseSession Then
      Return CreateCloseSessionMovements(SqlTrx)
    End If

    Return Me.CreateMovements(SqlTrx)

  End Function

  ''' <summary>
  ''' Create close session movements for CountR deposits
  ''' </summary>
  ''' <param name="TerminalCashierSessionInfo"></param>
  ''' <param name="CountR"></param>
  ''' <param name="CountRS"></param>
  ''' <param name="SqlTrx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function CreateCloseSessionMovements(ByVal SqlTrx As SqlTransaction) As ENUM_CONFIGURATION_RC

    Dim _total_cash_collected As Decimal

    ' Close session
    Me.CashierMovementDetail = CASHIER_MOVEMENT.CAGE_CLOSE_SESSION
    Me.CashierMovementTotal = CASHIER_MOVEMENT.CLOSE_SESSION

    ' Get CountRSession
    If Not CountRBusinessLogic.GetCountRSessionsById(CageControl.CashierSessionId, Me.Session, Me.CashierSessionInfo, SqlTrx) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    _total_cash_collected = IIf(IsDBNull(CageControl.TableCurrencies.Compute("Sum(TOTAL)", "ISO_CODE = '" & GeneralParam.GetString("RegionalOptions", "CurrencyISOCode") & "' AND (DENOMINATION =" & Cage.COINS_CODE & " OR DENOMINATION > 0)" & " AND TYPE <> 1")), _
                                                                              0, _
                                                                              CageControl.TableCurrencies.Compute("Sum(TOTAL)", "ISO_CODE = '" & GeneralParam.GetString("RegionalOptions", "CurrencyISOCode") & "' AND (DENOMINATION =" & Cage.COINS_CODE & " OR DENOMINATION > 0)" & " AND TOTAL IS NOT NULL"))

    Try

      ' JBP: Update CountR Money Collection (IMPORTANT: ONLY ON CLOSE SESSION!!)
      If Not Me.UpdateCountRTicketCollection(SqlTrx) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR
      End If

      ' JBP: Create Cashier movements
      If Not Me.CreateCashierMovements(_total_cash_collected, True, SqlTrx) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR
      End If

      ' JBP: Update collected amount
      If Not Me.Session.UpdateSession_Collected(_total_cash_collected, SqlTrx) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR
      End If

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Catch _ex As Exception
      Log.Error(_ex.Message())
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR

  End Function

  ''' <summary>
  ''' Create close session movements for CountR deposits
  ''' </summary>
  ''' <param name="TerminalCashierSessionInfo"></param>
  ''' <param name="CountR"></param>
  ''' <param name="CountRS"></param>
  ''' <param name="SqlTrx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function UndoCollectedAmount(ByVal CashierSessionId As Int32, ByVal SqlTrx As SqlTransaction) As ENUM_CONFIGURATION_RC

    ' Get CountRSession
    If Not CountRBusinessLogic.GetCountRSessionsById(CashierSessionId, Me.Session, Me.CashierSessionInfo, SqlTrx) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    Try

      ' JBP: Undo collected amount
      If Not Me.Session.UpdateSession_UndoCollected(SqlTrx) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR
      End If

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Catch _ex As Exception
      Log.Error(_ex.Message())
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR

  End Function

  ''' <summary>
  ''' Create movements for CountR deposits
  ''' </summary>
  ''' <param name="TerminalCashierSessionInfo"></param>
  ''' <param name="CountR"></param>
  ''' <param name="CountRS"></param>
  ''' <param name="SqlTrx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CreateMovements(ByVal SqlTrx As SqlTransaction) As ENUM_CONFIGURATION_RC

    Dim _total_amount As Decimal
    Dim _withdrawal_increment As Decimal
    Dim _deposit_increment As Decimal

    ' Initialize amounts
    _withdrawal_increment = 0
    _deposit_increment = 0

    Try

      If Me.CageControl.OperationType = Cage.CageOperationType.FromCashier Or Me.CageControl.OperationType = Cage.CageOperationType.FromCashierClosing Then
        ' Reception
        Me.OptCode = OperationCode.CASH_WITHDRAWAL
        Me.CashierMovementDetail = CASHIER_MOVEMENT.CAGE_FILLER_OUT
        Me.CashierMovementTotal = CASHIER_MOVEMENT.FILLER_OUT
      Else
        ' Sent
        Me.OptCode = OperationCode.CASH_DEPOSIT
        Me.CashierMovementDetail = CASHIER_MOVEMENT.CAGE_FILLER_IN
        Me.CashierMovementTotal = CASHIER_MOVEMENT.FILLER_IN
      End If

      If Not Me.CreateCashierMovements(_total_amount, False, SqlTrx) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR
      End If

      ' Update CountR session   
      If Me.CageControl.OperationType = Cage.CageOperationType.FromCashier Or Me.CageControl.OperationType = Cage.CageOperationType.FromCashierClosing Then
        _withdrawal_increment = _total_amount ' Reception
      Else
        _deposit_increment = _total_amount    ' Sent
      End If

      If Not Me.Session.UpdateSession_Movement(_withdrawal_increment, _deposit_increment, SqlTrx) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR
      End If

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Catch _ex As Exception
      Log.Error(_ex.Message())
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR

  End Function ' CreateCountRMovements

  ''' <summary>
  ''' Create CountR cashier movements
  ''' </summary>
  ''' <param name="SqlTrx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CreateCashierMovements(ByRef TotalAmount As Decimal, ByVal IsClosing As Boolean, ByVal SqlTrx As SqlTransaction) As Boolean

    Dim _operation_id As Int64
    Dim _row As DataRow
    Dim _cm_mov_table As CashierMovementsTable
    Dim _rv_total As Object
    Dim _rv_iso_code As Object
    Dim _rv_denomination As Object

    _operation_id = 0
    _cm_mov_table = New CashierMovementsTable(Me.CashierSessionInfo)

    ' Create cashier movement - Details
    For Each _row In Me.CageControl.TableCurrencies.Select("QUANTITY <> 0 OR (TOTAL <> 0 AND DENOMINATION = -1)", "ISO_CODE ASC, TYPE ASC, DENOMINATION DESC")

      _rv_total = Me.CageControl.GetRowValue(_row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.TOTAL))
      _rv_iso_code = _row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.ISO_CODE)
      _rv_denomination = _row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.DENOMINATION)

      ' Add cashier movement detail
      If IsClosing And CashierMovementDetail = CASHIER_MOVEMENT.CAGE_CLOSE_SESSION Then
        Continue For
      End If

      If Not _cm_mov_table.Add(_operation_id, Me.CashierMovementDetail, _rv_total, 0, String.Empty, String.Empty, _rv_iso_code, _rv_denomination, Me.CageControl.MovementID, 0, -1, CurrencyExchangeType.CURRENCY, CageCurrencyType.Bill, Nothing) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR
      End If

      TotalAmount += Me.CageControl.GetRowValue(_row.Item(CLASS_CAGE_CONTROL.DT_COLUMN_CURRENCY.TOTAL))
    Next

    If Not IsClosing Then
      ' Create cashier movement - Total: If close session, the movement added in CloseCountRSession
      If Not _cm_mov_table.Add(_operation_id, Me.CashierMovementTotal, TotalAmount, 0, String.Empty, String.Empty, String.Empty, 0, Me.CageControl.MovementID, 0) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR
      End If

      ' Insert deposit operation: If close session, dont create operation
      If Not Operations.DB_InsertOperation(Me.OptCode, 0, Me.CashierSessionInfo.CashierSessionId, 0, 0, 0, 0, 0, 0, String.Empty, _operation_id, SqlTrx) Then
        Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR
      End If
    End If

    ' Save cashier movements
    If Not _cm_mov_table.Save(SqlTrx) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR
    End If

    Return True
  End Function

  ''' <summary>
  ''' Update 
  ''' </summary>
  ''' <param name="SqlTrx"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function UpdateCountRTicketCollection(ByVal SqlTrx As SqlTransaction) As Boolean

    Me.CageControl.ReadTheoreticalCollectionNewMode(0, Me.CashierSessionInfo.CashierSessionId)

    If Not Me.CageControl.InsertStackerCollection(True, SqlTrx) Then
      Return False
    End If

    Return True
  End Function

#End Region

#End Region ' Public Functions

#Region " Private Functions "


#End Region ' Private Functions

End Class
