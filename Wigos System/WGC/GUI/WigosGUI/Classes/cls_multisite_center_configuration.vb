'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_multisite_center_configuration.vb
' DESCRIPTION:   
' AUTHOR:        Artur Nebot Garrig�
' CREATION DATE: 15-03-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 15-MAR-2013  ANG    Initial version
' 06-MAY-2013  ANG    Fix bug #765
' 21-MAY-2013  AMF    New Column Name in Sites
' 14-JUN-2013  AMF    Fixed Bug #852: Don't truncate audit text
' 23-APR-2015  ANM    Get currency's ISO_CODE in GetSites
' 14-OCT-2016  RAB    PBI 18098: General params: Automatically add the GP to the system
' -------------------------------------------------------------------

Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text
Imports System.Xml
Imports System.IO

Public Class CLS_MULTISITE_CENTER_CONFIGURATION
  Inherits CLASS_BASE

#Region " Constants "

  Public Const MULTISITE_ID As Int32 = 0

  Public Const DEFAULT_DB_ID As Byte = 0
#End Region

#Region " Structures "
  Public Class TYPE_MULTISITE_CENTER_CONFIG
    Public site_id As String
    Public site_name As String
    Public members_card_id As String
    Public center_address_1 As String
    Public center_address_2 As String
    Public tasks As DataTable
    Public sites As DataTable
    Public newsites As List(Of Int32)

    Sub New()
      site_id = String.Empty
      site_name = String.Empty
      tasks = New DataTable()
      sites = New DataTable()
      newsites = New List(Of Int32)
    End Sub
  End Class

#End Region

#Region " Members"
  Protected m_multisite_center_config As TYPE_MULTISITE_CENTER_CONFIG
  Protected m_detect_new_sites As List(Of Int32)
#End Region

#Region "Properties"
  Public ReadOnly Property SiteID() As String
    Get

      Return m_multisite_center_config.site_id
    End Get
  End Property

  Public ReadOnly Property SiteName() As String
    Get

      Return m_multisite_center_config.site_name
    End Get
  End Property

  Public Property MembersCardID() As String
    Get

      Return m_multisite_center_config.members_card_id
    End Get
    Set(ByVal Value As String)
      m_multisite_center_config.members_card_id = Value
    End Set
  End Property

  Public Property CenterAddress1() As String
    Get

      Return m_multisite_center_config.center_address_1
    End Get
    Set(ByVal Value As String)
      m_multisite_center_config.center_address_1 = Value
    End Set
  End Property

  Public Property CenterAddress2() As String
    Get

      Return m_multisite_center_config.center_address_2
    End Get
    Set(ByVal Value As String)
      m_multisite_center_config.center_address_2 = Value
    End Set
  End Property

  Public Property Tasks() As DataTable
    Get

      Return m_multisite_center_config.tasks
    End Get
    Set(ByVal value As DataTable)
      m_multisite_center_config.tasks = value
    End Set
  End Property

  Public Property Sites() As DataTable
    Get
      Return m_multisite_center_config.sites
    End Get
    Set(ByVal value As DataTable)
      m_multisite_center_config.sites = value
    End Set
  End Property

  Public Property NewSites() As List(Of Int32)
    Get
      Return m_multisite_center_config.newsites
    End Get
    Set(ByVal value As List(Of Int32))
      m_multisite_center_config.newsites = value
    End Set
  End Property
#End Region

#Region "Overrides functions"

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _task_id As TYPE_MULTISITE_SITE_TASKS
    Dim _str_section As String
    Dim _title As String
    Dim _nls_id As Integer
    Dim _task_name As String
    Dim _label_enabled As String
    Dim _label_yes As String
    Dim _label_no As String
    Dim _str_value As String
    Dim _label_interval As String
    Dim _label_starting_hour As String
    Dim _label_max_items_to_upload As String
    Dim _site_id As Integer
    Dim _is_site_enabled As Boolean
    Dim _is_elp_enabled As Boolean
    Dim _site_name As String
    Dim _db_principal As String
    Dim _db_mirror As String

    'Dim _site_config As New SiteConfig

    _label_enabled = GLB_NLS_GUI_PLAYER_TRACKING.GetString(757) ' Enabled
    _label_yes = GLB_NLS_GUI_PLAYER_TRACKING.GetString(698)
    _label_no = GLB_NLS_GUI_PLAYER_TRACKING.GetString(699)
    _label_interval = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1800) ' Interval
    _label_max_items_to_upload = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1820) ' Max items to upload
    _label_starting_hour = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1821) ' Starting hour
    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_MULTISITE)

    ' Trick to shorten label to fit in Auditor NLS... 
    If (_label_interval.Contains(" ")) Then
      _label_interval = CType(_label_interval.Split(" "), String())(0)
    End If

    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(1697), "") '' Multisite

    _str_value = IIf(Me.MembersCardID = String.Empty, AUDIT_NONE_STRING, Me.MembersCardID)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1908), _str_value)

    _str_value = IIf(Me.CenterAddress1 = String.Empty, AUDIT_NONE_STRING, Me.CenterAddress1)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1690), _str_value)

    _str_value = IIf(Me.CenterAddress2 = String.Empty, AUDIT_NONE_STRING, Me.CenterAddress2)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1691), _str_value)

    '' TASKS!
    For Each _task_row As DataRow In Me.Tasks.Rows
      _task_id = _task_row("ST_TASK_ID")
      _task_name = mdl_multisite.TranslateMultiSiteProcessID(_task_id)
      _nls_id = mdl_multisite.TranslateMultiSiteProcessIDToNlsId(_task_id)

      '' Enabled
      _str_section = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1798) ' Tareas
      _title = GetAuditorHeader(_str_section, _task_name, _label_enabled)   'Tasks.'Task Name' Enabled = ?

      _str_value = IIf(_task_row("ST_ENABLED"), _label_yes, _label_no)
      Call _auditor_data.SetField(0, _str_value, _title)

      '' Interval
      _str_value = IIf(_task_row.IsNull("ST_INTERVAL_SECONDS"), AUDIT_NONE_STRING, _task_row("ST_INTERVAL_SECONDS"))
      _title = GetAuditorHeader(_str_section, _task_name, _label_interval)

      Call _auditor_data.SetField(0, _str_value, _title)

      '' Max items
      _str_value = IIf(_task_row.IsNull("ST_MAX_ROWS_TO_UPLOAD"), 0, _task_row("ST_MAX_ROWS_TO_UPLOAD"))
      _title = GetAuditorHeader(_str_section, _task_name, _label_max_items_to_upload)

      Call _auditor_data.SetField(0, _str_value, _title)

      '' Start hour
      _str_value = IIf(_task_row.IsNull("ST_START_TIME"), 0, _task_row("ST_START_TIME"))
      _title = GetAuditorHeader(_str_section, _task_name, _label_starting_hour)

      Call _auditor_data.SetField(0, _str_value, _title)

    Next



    '' Sites!
    For Each _site_row As DataRow In Me.Sites.Rows

      _db_principal = String.Empty
      _db_mirror = String.Empty

      _site_id = _site_row("ST_SITE_ID")
      _is_site_enabled = _site_row("ST_STATE")
      _is_elp_enabled = _site_row("ST_ELP")
      _site_name = _site_row("ST_NAME")

      _db_principal = IIf(Not _site_row.IsNull("DbPrincipal"), _site_row("DbPrincipal"), AUDIT_NONE_STRING)
      _db_mirror = IIf(Not _site_row.IsNull("DbMirror"), _site_row("DbMirror"), AUDIT_NONE_STRING)
      
      If Me.NewSites.Contains(_site_id) Then
        '' Site %1 Added. Enabled 
        Call _auditor_data.SetField(0, _is_site_enabled, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1979, _site_id.ToString()))
        Call _auditor_data.SetField(0, IIf(_site_name.Trim.Length > 0, _site_name.Trim(), AUDIT_NONE_STRING), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1994, _site_id.ToString()))
        Call _auditor_data.SetField(0, IIf(_db_principal.Length > 0, _db_principal, AUDIT_NONE_STRING), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2047, _site_id.ToString(), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2045))) '"Site %1 Added.Server 1
        Call _auditor_data.SetField(0, IIf(_db_mirror.Length > 0, _db_mirror, AUDIT_NONE_STRING), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2047, _site_id.ToString(), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2046)))         ' "Site %1 Added.Server 2
        Call _auditor_data.SetField(0, _is_elp_enabled, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2401, _site_id.ToString(), GeneralParam.GetString("ExternalLoyaltyProgram.Mode01", "Name")))

        Continue For
      End If

      ''  Site %1. Enabled 
      Call _auditor_data.SetField(0, _is_site_enabled, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1977, _site_id.ToString()))
      Call _auditor_data.SetField(0, IIf(_site_name.Trim.Length > 0, _site_name.Trim(), AUDIT_NONE_STRING), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1993, _site_id.ToString()))
      Call _auditor_data.SetField(0, IIf(_db_principal.Length > 0, _db_principal, AUDIT_NONE_STRING), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1773) & " " & _site_id & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2045)) ' Servidor 1
      Call _auditor_data.SetField(0, IIf(_db_mirror.Length > 0, _db_mirror, AUDIT_NONE_STRING), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1773) & " " & _site_id & "." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2046)) ' Servidor 2
      Call _auditor_data.SetField(0, _is_elp_enabled, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2402, _site_id.ToString(), GeneralParam.GetString("ExternalLoyaltyProgram.Mode01", "Name")))

    Next



    Return _auditor_data
  End Function

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

  End Function

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

  End Function

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As ENUM_STATUS

    Using _db_trx As New DB_TRX()

      _rc = GetConfigurationData(Me.m_multisite_center_config, _db_trx.SqlTransaction)

      Select Case _rc
        Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
          _db_trx.Commit()
          Return CLASS_BASE.ENUM_STATUS.STATUS_OK

        Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
          _db_trx.Rollback()
          Return ENUM_STATUS.STATUS_NOT_FOUND

        Case Else
          _db_trx.Rollback()
          Return ENUM_STATUS.STATUS_ERROR

      End Select

    End Using
  End Function

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Dim _rc As Integer


    Using _db_trx As New DB_TRX
      _rc = Me.UpdateMultisiteConfig(Me.m_multisite_center_config, _db_trx.SqlTransaction)

      Select Case _rc
        Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
          _db_trx.Commit()
          Return CLASS_BASE.ENUM_STATUS.STATUS_OK

        Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
          _db_trx.Rollback()
          Return ENUM_STATUS.STATUS_NOT_FOUND

        Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
          _db_trx.Rollback()
          Return ENUM_STATUS.STATUS_DUPLICATE_KEY

        Case Else
          _db_trx.Rollback()
          Return ENUM_STATUS.STATUS_ERROR

      End Select
    End Using

  End Function

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim _target As CLS_MULTISITE_CENTER_CONFIGURATION

    _target = New CLS_MULTISITE_CENTER_CONFIGURATION

    _target.m_multisite_center_config.site_id = Me.m_multisite_center_config.site_id
    _target.m_multisite_center_config.site_name = Me.m_multisite_center_config.site_name
    _target.m_multisite_center_config.center_address_1 = Me.m_multisite_center_config.center_address_1
    _target.m_multisite_center_config.center_address_2 = Me.m_multisite_center_config.center_address_2
    _target.m_multisite_center_config.members_card_id = Me.m_multisite_center_config.members_card_id
    _target.m_multisite_center_config.tasks = Me.m_multisite_center_config.tasks.Copy()
    _target.m_multisite_center_config.sites = Me.m_multisite_center_config.sites.Copy()

    For Each _new_site_id As Int32 In Me.m_multisite_center_config.newsites
      _target.m_multisite_center_config.newsites.Add(_new_site_id)
    Next

    Return _target


  End Function
#End Region

#Region "Private Functions"


  Private Function GetConfigurationData(ByRef CenterConfiguration As TYPE_MULTISITE_CENTER_CONFIG, ByRef Trx As SqlTransaction) As Integer

    Dim _rc As Integer
    Dim _dt_gp As DataTable

    _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Try

      CenterConfiguration.site_id = GetSiteId()
      CenterConfiguration.site_name = GetSiteName()

      _dt_gp = GetMultisiteGeneralParams(Trx, New String() {"CenterAddress1", "CenterAddress2", "MembersCardId"})

      CenterConfiguration.center_address_1 = _dt_gp.Rows.Find(New Object() {"MultiSite", "CenterAddress1"})("GP_KEY_VALUE")
      CenterConfiguration.center_address_2 = _dt_gp.Rows.Find(New String() {"MultiSite", "CenterAddress2"})("GP_KEY_VALUE")
      CenterConfiguration.members_card_id = _dt_gp.Rows.Find(New String() {"MultiSite", "MembersCardId"})("GP_KEY_VALUE")

      CenterConfiguration.tasks = GetTasks(Trx)
      CenterConfiguration.sites = GetSites(Trx)
      If CenterConfiguration.tasks Is Nothing Then
        _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
      End If

    Catch _ex As Exception
      _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, "cls_multisite_configuration", "GetConfigurationData", _ex.Message)
    End Try

    Return _rc

  End Function

  Private Function GetSites(ByRef Trx As SqlTransaction) As DataTable
    Dim _bd As StringBuilder
    Dim _dt_sites As DataTable
    Dim _str_xml As String
    'Dim _column As DataColumn

    _bd = New StringBuilder()
    _dt_sites = New DataTable()

    _bd.AppendLine("  SELECT   ST_SITE_ID ")
    _bd.AppendLine("         , ST_STATE   ")
    _bd.AppendLine("         , ST_NAME    ")
    _bd.AppendLine("         , ST_CONNECTION_STRING ")
    _bd.AppendLine("         , ISNULL(ST_ELP,0) AS ST_ELP ")
    _bd.AppendLine("         , SC_ISO_CODE")
    _bd.AppendLine("    FROM  SITES ")
    _bd.AppendLine("    LEFT  JOIN SITE_CURRENCIES ON SITES.ST_SITE_ID = SITE_CURRENCIES.SC_SITE_ID")
    _bd.AppendLine("          AND  SC_TYPE = 1")
    _bd.AppendLine("    ORDER BY   ST_SITE_ID ")

    _dt_sites = GUI_GetTableUsingSQL(_bd.ToString(), 5000, "SITES", Trx)

    _dt_sites.Columns.Add("DBPrincipal", Type.GetType("System.String"))
    _dt_sites.Columns("DBPrincipal").DefaultValue = String.Empty

    _dt_sites.Columns.Add("DBMirror", Type.GetType("System.String"))
    _dt_sites.Columns("DBMirror").DefaultValue = String.Empty

    For Each _row As DataRow In _dt_sites.Rows

      _str_xml = _row("ST_CONNECTION_STRING").ToString()

      If _str_xml <> String.Empty Then
        Using _xml_reader = XmlReader.Create(New StringReader(_str_xml))
          _row("DBPrincipal") = XML.ReadTagValue(_xml_reader, "DBPrincipal")
          _row("DBMirror") = XML.ReadTagValue(_xml_reader, "DBMirror")
        End Using
      End If
    Next

      _dt_sites.Columns.Remove("ST_CONNECTION_STRING")
      _dt_sites.PrimaryKey = New DataColumn() {_dt_sites.Columns("ST_SITE_ID")}

      Return _dt_sites

  End Function
  Private Function GetTasks(ByRef Trx As SqlTransaction) As DataTable

    Dim _bd As StringBuilder
    Dim _dt_tasks As DataTable

    _bd = New StringBuilder()
    _dt_tasks = New DataTable()


    _bd.AppendLine("SELECT   ST_TASK_ID                              ")
    _bd.AppendLine("       , CAST(ST_ENABLED as Bit)  as ST_ENABLED  ")
    _bd.AppendLine("       , CAST(ST_INTERVAL_SECONDS    as Integer) ST_INTERVAL_SECONDS")
    _bd.AppendLine("       , ST_MAX_ROWS_TO_UPLOAD                   ")
    _bd.AppendLine("       , ST_START_TIME                           ")

    _bd.AppendLine(" FROM    MS_SITE_TASKS                           ")
    _bd.AppendFormat("WHERE    ST_SITE_ID = {0}                      ", MULTISITE_ID)

    _dt_tasks = GUI_GetTableUsingSQL(_bd.ToString(), 5000, "MS_SITE_TASKS", Trx)

    Return _dt_tasks

  End Function



  Public Shared Function GenerateWigosGUIConfigFile(ByRef DbPrincipal As String, ByRef DbMirror As String, ByVal DbId As Int32) As String

    Dim _str_bd_xml As New StringBuilder

    _str_bd_xml.AppendFormat("<SiteConfig>										").AppendLine()
    _str_bd_xml.AppendFormat("  <DBPrincipal>{0}</DBPrincipal>", DbPrincipal).AppendLine()
    _str_bd_xml.AppendFormat("  <DBMirror>{0}</DBMirror>			", DbMirror).AppendLine()
    _str_bd_xml.AppendFormat("  <DBId>{0}</DBId>							", DbId).AppendLine()
    _str_bd_xml.AppendFormat("</SiteConfig>										").AppendLine()

    Return _str_bd_xml.ToString()

  End Function
  Private Function UpdateMultisiteConfig(ByRef CenterConfiguration As TYPE_MULTISITE_CENTER_CONFIG, ByRef Trx As SqlTransaction) As Integer

    Dim _bd As StringBuilder
    Dim _rows_affected As Integer
    Dim _db_principal As String
    Dim _db_mirror As String
    Dim _xml_site_config As String
    Dim _db_id As UInt16

    Try

      ' UPDATE GENERAL PARAMS
      Dim _gp_update_list As New List(Of String())

      _gp_update_list.Add(GeneralParam.AddToList("MultiSite", "MembersCardId", Me.m_multisite_center_config.members_card_id))
      _gp_update_list.Add(GeneralParam.AddToList("MultiSite", "CenterAddress1", Me.m_multisite_center_config.center_address_1))
      _gp_update_list.Add(GeneralParam.AddToList("MultiSite", "CenterAddress2", Me.m_multisite_center_config.center_address_2))

      For Each _general_param As String() In _gp_update_list
        If (Not GUI_Controls.DB_GeneralParam_Update(_general_param(GeneralParam.COLUMN_NAME.Group),
                                                    _general_param(GeneralParam.COLUMN_NAME.Subject),
                                                    _general_param(GeneralParam.COLUMN_NAME.Value),
                                                    Trx)) Then

          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB          
        End If
      Next

      ' UPDATE TASKS
      _bd = New StringBuilder()
      _bd.AppendLine("UPDATE   MS_SITE_TASKS                              ")
      _bd.AppendLine("   SET   ST_ENABLED = @pEnabled                     ")
      _bd.AppendLine("       , ST_INTERVAl_SECONDS   = @pIntervalSeconds  ")
      _bd.AppendLine("       , ST_MAX_ROWS_TO_UPLOAD = @pMaxRow2Upload    ")
      _bd.AppendLine("       , ST_START_TIME         = @pStartTime        ")
      _bd.AppendLine(" WHERE   ST_TASK_ID = @pTaskId                      ")

      Using _cmd As New SqlCommand(_bd.ToString(), Trx.Connection, Trx)
        _cmd.Parameters.Add("@pEnabled", SqlDbType.Int).SourceColumn = "ST_ENABLED"
        _cmd.Parameters.Add("@pIntervalSeconds", SqlDbType.Int).SourceColumn = "ST_INTERVAl_SECONDS"
        _cmd.Parameters.Add("@pTaskId", SqlDbType.Int).SourceColumn = "ST_TASK_ID"
        _cmd.Parameters.Add("@pMaxRow2Upload", SqlDbType.Int).SourceColumn = "ST_MAX_ROWS_TO_UPLOAD"
        _cmd.Parameters.Add("@pStartTime", SqlDbType.Int).SourceColumn = "ST_START_TIME"

        _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = MULTISITE_ID

        Using _da As New SqlDataAdapter(_cmd)

          _da.InsertCommand = _cmd
          _da.UpdateBatchSize = 500
          _da.InsertCommand.UpdatedRowSource = UpdateRowSource.None
          _rows_affected = _da.Update(CenterConfiguration.tasks)

        End Using
      End Using

      '' UPDATE SITES.


      CenterConfiguration.sites.Columns.Add("ST_CONNECTION_STRING", Type.GetType("System.String"))

      For Each _site_row As DataRow In CenterConfiguration.sites.Rows

        _db_principal = _site_row("DbPrincipal")
        _db_mirror = _site_row("DbMirror")

        If Not String.IsNullOrEmpty(_db_principal) _
                Or Not String.IsNullOrEmpty(_db_mirror) Then

          _db_id = DEFAULT_DB_ID
          If Environment.GetEnvironmentVariable("LKS_VC_DEV") <> vbNullString Then
            _db_id = _site_row("ST_SITE_ID")
          End If

          _xml_site_config = GenerateWigosGUIConfigFile(_db_principal, _db_mirror, _db_id)
          _site_row("ST_CONNECTION_STRING") = _xml_site_config

        Else
          _site_row("ST_CONNECTION_STRING") = DBNull.Value
        End If

      Next

      Using _da As New SqlDataAdapter()

        _bd.Length = 0

        _bd.AppendLine("IF NOT EXISTS ( SELECT TOP 1 ST_SITE_ID FROM SITES WHERE ST_SITE_ID = @pSiteId )")
        _bd.AppendLine("   INSERT INTO   SITES ( ST_SITE_ID       ")
        _bd.AppendLine("               , ST_NAME                  ")
        _bd.AppendLine("               , ST_STATE                 ")
        _bd.AppendLine("               , ST_OPERATOR_ID           ")
        _bd.AppendLine("               , ST_CONNECTION_STRING     ")
        _bd.AppendLine("               , ST_ELP               )   ")
        _bd.AppendLine("        VALUES ( @pSiteId                 ")
        _bd.AppendLine("               , @PSiteName               ")
        _bd.AppendLine("               , @pState                  ")
        _bd.AppendLine("               , @pOperatorId             ")
        _bd.AppendLine("               , @pConnectionString       ")
        _bd.AppendLine("               , @pElp                )   ")
        _bd.AppendLine("ELSE                                                 ")
        _bd.AppendLine("   UPDATE   SITES                                    ")
        _bd.AppendLine("      SET   ST_STATE             = @pState           ")
        _bd.AppendLine("          , ST_NAME              = @pSiteName        ")
        _bd.AppendLine("          , ST_CONNECTION_STRING = @pConnectionString")
        _bd.AppendLine("          , ST_ELP               = @pElp             ")
        _bd.AppendLine("    WHERE   ST_SITE_ID           = @pSiteId          ")

        _da.InsertCommand = New SqlCommand(_bd.ToString())
        _da.InsertCommand.Parameters.Add("@pSiteId", SqlDbType.Int).SourceColumn = "ST_SITE_ID"
        _da.InsertCommand.Parameters.Add("@pSiteName", SqlDbType.NVarChar, 50).SourceColumn = "ST_NAME"
        _da.InsertCommand.Parameters.Add("@pState", SqlDbType.Bit).SourceColumn = "ST_STATE"
        _da.InsertCommand.Parameters.Add("@pOperatorId", SqlDbType.Int).Value = 1 '' TODO!
        _da.InsertCommand.Parameters.Add("@pConnectionString", SqlDbType.Xml).SourceColumn = "ST_CONNECTION_STRING"
        _da.InsertCommand.Parameters.Add("@pElp", SqlDbType.Int).SourceColumn = "ST_ELP"

        _da.InsertCommand.Connection = Trx.Connection
        _da.InsertCommand.Transaction = Trx
        _da.InsertCommand.UpdatedRowSource = UpdateRowSource.None

        _rows_affected = _da.Update(CenterConfiguration.sites)

      End Using

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

    Catch _ex As Exception
      Log.Exception(_ex)

    End Try


    Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

  End Function

  ' PURPOSE: Get a DataTable containing GENERAL_PARAMS table structure
  '                  
  '  PARAMS:         
  '     - INPUT:     
  '           - 
  '     - OUTPUT:    
  '           -  Trax As SqlTransaction, 
  '                  
  ' RETURNS:         
  '     - DataTable
  '                  
  Private Function GetMultisiteGeneralParams(ByRef Trax As SqlTransaction, Optional ByVal Names As String() = Nothing) As DataTable
    Dim _str_bld As StringBuilder
    Dim _dtable As DataTable

    _dtable = New DataTable()
    _str_bld = New StringBuilder

    _str_bld.AppendLine("SELECT   GP_GROUP_KEY  ")
    _str_bld.AppendLine("       , GP_SUBJECT_KEY")
    _str_bld.AppendLine("       , GP_KEY_VALUE  ")
    _str_bld.AppendLine("  FROM   GENERAL_PARAMS")

    If Not Names Is Nothing AndAlso Names.Length > 0 Then
      _str_bld.AppendLine("WHERE   GP_GROUP_KEY = 'MultiSite' ")
      _str_bld.AppendFormat("  AND   GP_SUBJECT_KEY IN('{0}')", String.Join("','", Names))

    End If


    Using _sql_cmd As New SqlCommand(_str_bld.ToString(), Trax.Connection, Trax)

      Using _sql_da As New SqlDataAdapter(_sql_cmd)
        If (Names Is Nothing) Then
          _sql_da.FillSchema(_dtable, SchemaType.Source)
        Else
          _sql_da.Fill(_dtable)
          _dtable.PrimaryKey = New DataColumn() {_dtable.Columns("GP_GROUP_KEY"), _dtable.Columns("GP_SUBJECT_KEY")}
        End If

      End Using

    End Using

    Return _dtable

  End Function 'GetGeneralParamsSchemma

  ' PURPOSE : Returns Auditor header
  '
  '
  ' Example : Retur String like "User Fields . Surname Editable "

  Private Function GetAuditorHeader(ByVal FieldGroup As String, ByVal FieldName As String, ByVal FieldProperty As String) As String

    Dim _double_quotes As String
    _double_quotes = String.Empty ' Chr(34)
    Return String.Format("{0}{1}{0}.{0}{2}{0} {3}", _double_quotes, FieldGroup, FieldName, FieldProperty)
  End Function

#End Region

#Region "Public Functions"


  ' PURPOSE: New
  '                  
  '  PARAMS:         
  '     - INPUT:     
  '           - None
  '     - OUTPUT:    
  '           - None
  '                  
  ' RETURNS:         
  '     -None
  '                  
  Public Sub New()
    Me.m_multisite_center_config = New TYPE_MULTISITE_CENTER_CONFIG()
    m_detect_new_sites = New List(Of Int32)
  End Sub 'New



#End Region

End Class