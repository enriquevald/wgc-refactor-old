'-------------------------------------------------------------------
' Copyright � 2009 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_account.vb
' DESCRIPTION:   Player accounts class
' AUTHOR:        Miquel Beltran Febrer
' CREATION DATE: 16-DIC-2009
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 16-DIC-2009  MBF    First
'--------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices

Public Class CLASS_ACCOUNT
  Inherits CLASS_BASE

#Region " Constants "

  Public Const MAX_STRING_LEN = 50
  Public Const MAX_ZIP_LEN = 10
  Public Const MAX_PHONE_NUMBER_LEN = 20
  Public Const MAX_COMMENTS_LEN = 100

#End Region ' Constants

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_ACCOUNT
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_STRING_LEN)> _
    Public holder_name As String
    Public points As Double
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_STRING_LEN)> _
    Public holder_address_01 As String
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_STRING_LEN)> _
    Public holder_address_02 As String
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_STRING_LEN)> _
    Public holder_address_03 As String
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_STRING_LEN)> _
    Public holder_city As String
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_ZIP_LEN)> _
    Public holder_zip As String
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_STRING_LEN)> _
    Public holder_email_01 As String
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_STRING_LEN)> _
    Public holder_email_02 As String
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_PHONE_NUMBER_LEN)> _
    Public holder_phone_number_01 As String
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_PHONE_NUMBER_LEN)> _
    Public holder_phone_number_02 As String
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_COMMENTS_LEN)> _
    Public holder_comments As String
    Public holder_gender As Integer
    Public holder_marital_status As Integer
    Public holder_birth_date As DateTime
    Public card_level As Integer
  End Class

#End Region

#Region " Members "

  Protected m_account As New TYPE_ACCOUNT

#End Region 'Members

#Region "Properties"

  Public Property HolderName() As String
    Get
      Return m_account.holder_name
    End Get

    Set(ByVal Value As String)
      m_account.holder_name = Value
    End Set

  End Property

  Public Property Points() As Double
    Get
      Return m_account.points
    End Get

    Set(ByVal Value As Double)
      m_account.points = Value
    End Set

  End Property

  Public Property HolderAddress01() As String
    Get
      Return m_account.holder_address_01
    End Get

    Set(ByVal Value As String)
      m_account.holder_address_01 = Value
    End Set

  End Property

  Public Property HolderAddress02() As String
    Get
      Return m_account.holder_address_02
    End Get

    Set(ByVal Value As String)
      m_account.holder_address_02 = Value
    End Set

  End Property

  Public Property HolderAddress03() As String
    Get
      Return m_account.holder_address_03
    End Get

    Set(ByVal Value As String)
      m_account.holder_address_03 = Value
    End Set

  End Property

  Public Property HolderCity() As String
    Get
      Return m_account.holder_city
    End Get

    Set(ByVal Value As String)
      m_account.holder_city = Value
    End Set

  End Property

  Public Property HolderZip() As String
    Get
      Return m_account.holder_zip
    End Get

    Set(ByVal Value As String)
      m_account.holder_zip = Value
    End Set

  End Property

  Public Property HolderEmail01() As String
    Get
      Return m_account.holder_email_01
    End Get

    Set(ByVal Value As String)
      m_account.holder_email_01 = Value
    End Set

  End Property

  Public Property HolderEmail02() As String
    Get
      Return m_account.holder_email_02
    End Get

    Set(ByVal Value As String)
      m_account.holder_email_02 = Value
    End Set

  End Property

  Public Property HolderPhoneNumber01() As String
    Get
      Return m_account.holder_phone_number_01
    End Get

    Set(ByVal Value As String)
      m_account.holder_phone_number_01 = Value
    End Set

  End Property

  Public Property HolderPhoneNumber02() As String
    Get
      Return m_account.holder_phone_number_02
    End Get

    Set(ByVal Value As String)
      m_account.holder_phone_number_02 = Value
    End Set

  End Property

  Public Property HolderComments() As String
    Get
      Return m_account.holder_comments
    End Get

    Set(ByVal Value As String)
      m_account.holder_comments = Value
    End Set

  End Property

  Public Property HolderGender() As Integer
    Get
      Return m_account.holder_gender
    End Get

    Set(ByVal Value As Integer)
      m_account.holder_gender = Value
    End Set

  End Property

  Public Property HolderMaritalStatus() As Integer
    Get
      Return m_account.holder_marital_status
    End Get

    Set(ByVal Value As Integer)
      m_account.holder_marital_status = Value
    End Set

  End Property

  Public Property HolderBirthDate() As DateTime
    Get
      Return m_account.holder_birth_date
    End Get

    Set(ByVal Value As DateTime)
      m_account.holder_birth_date = Value
    End Set

  End Property

  Public Property HolderCardLevel() As Integer
    Get
      Return m_account.card_level
    End Get

    Set(ByVal Value As Integer)
      m_account.card_level = Value
    End Set

  End Property


#End Region

#Region "Overrides"

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA
    Dim auditor_data As CLASS_AUDITOR_DATA

    auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_ACCOUNT)

    Return auditor_data
  End Function

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE
    Dim temp_account As CLASS_ACCOUNT

    temp_account = New CLASS_ACCOUNT

    Return temp_account
  End Function

#End Region




End Class
