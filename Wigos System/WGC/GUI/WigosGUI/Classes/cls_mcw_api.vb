'-------------------------------------------------------------------
' Copyright © 2008-2009 Win Systems International.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_mcw_api.vb
' 
' DESCRIPTION:   Class to encapsulate the MCW API communication.
'
' AUTHOR:        Ronald Rodríguez
'
' CREATION DATE: 25-SEP-2008
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ ----------------------------------------------
' 25-SEP-2008  RRT    Initial version
' 01-DEC-2016  ETP    PBI 21172 Export two track data to player cards.
'-------------------------------------------------------------------

#Region " Imports "

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.IO
Imports System.Data.SqlClient

#End Region

Public Class CLASS_MCW_API

#Region " Constants "

  ' Module function codes
  Private Const MCW_CODE_MODULE_INIT As Short = 1
  Private Const MCW_CODE_DEVICE_INIT As Short = 2
  Private Const MCW_CODE_DEVICE_STATUS_QUERY As Short = 3
  Private Const MCW_CODE_WRITE As Short = 4
  Private Const MCW_CODE_DEVICE_CLOSE As Short = 5

  ' Events notifications
  Private Const HANDLE_IDX_STOP As Integer = 0
  Private Const HANDLE_IDX_START_WRITE As Integer = 1
  Private Const HANDLE_IDX_EXIT_THREAD As Integer = 2

  Private Const MCW_MAX_MODELS As Short = 10
  Private Const MCW_MODEL_NAME_SIZE As Short = 20

  Private Const MCW_MAX_DATA_LENGTH As Short = 256

  ' Module external procedures return codes
  Public Const MCW_STATUS_OK As Short = 0    ' Ok
  Public Const MCW_STATUS_ERROR As Short = 1 ' Error

  ' Card Coercitivity
  Public Const MCW_CARD_HI_CO As Short = 1
  Public Const MCW_CARD_LO_CO As Short = 2


#End Region

#Region " Structures "

  Public Structure TYPE_SITE_DATA
    Dim id As Integer
    Dim name As String
    Dim internal_last_sequence As Int64
  End Structure

  Public Structure TYPE_MCW_MODEL_DATA
    Dim id As Integer
    Dim name As String
  End Structure

  Public Structure TYPE_MCW_WRITE_INPUT
    Dim data_length As Short
    Dim data() As Byte
  End Structure

  Public Structure TYPE_MCW_WRITE_RESULT
    Dim write_in_process As Boolean
    Dim write_processed As Boolean
    Dim write_status As Integer
    Dim sequence_updated As Boolean
    Dim session_last_sequence As Int64
    Dim total_sequence As Int64
  End Structure

#End Region

#Region " Internal Classes "

  <StructLayout(LayoutKind.Sequential)> _
   Public Class CLASS_MCW_MODEL
    Private id As Integer
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MCW_MODEL_NAME_SIZE + 1 + 3)> _
    Private name As String

    Public ReadOnly Property ModelId() As Integer
      Get
        Return id
      End Get
    End Property

    Public ReadOnly Property ModelName() As String
      Get
        Return name
      End Get
    End Property

    Public Sub New()
      id = 0
      name = ""
    End Sub
  End Class

  <StructLayout(LayoutKind.Sequential)> _
  Public Class CLASS_MCW_MODELS_LIST
    Private control_block As Integer
    Private num_models As Integer
    Private models As New CLASS_VECTOR()

    Public ReadOnly Property NumModels() As Integer
      Get
        Return num_models
      End Get
    End Property

    Public ReadOnly Property ModelsList() As CLASS_VECTOR
      Get
        Return models
      End Get
    End Property

    Public Sub New()
      MyBase.New()
      control_block = Marshal.SizeOf(Me)

      Call ModelsList.Init(GetType(CLASS_MCW_MODEL), MCW_MAX_MODELS)

      num_models = 0

    End Sub
  End Class

  <StructLayout(LayoutKind.Sequential)> _
  Public Class CLASS_MCW_WRITE
    Private control_block As Integer
    Private coercivity_type As Short
    Private data1_length As Short
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MCW_MAX_DATA_LENGTH)> _
    Private data1 As String
    Private data2_length As Short
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MCW_MAX_DATA_LENGTH)> _
    Private data2 As String
    Private data3_length As Short
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MCW_MAX_DATA_LENGTH)> _
    Private data3 As String

    Public Property Coercivity() As Short
      Get
        Return coercivity_type
      End Get

      Set(ByVal Value As Short)
        coercivity_type = Value
      End Set
    End Property

    Public WriteOnly Property Track1_Length() As Short
      Set(ByVal Value As Short)
        data1_length = Value
      End Set
    End Property


    Public WriteOnly Property Track1_Data() As String
      Set(ByVal Value As String)
        data1 = Value
      End Set
    End Property

    'Public WriteOnly Property Track1_Data() As Byte()
    '  Set(ByVal Value As Byte())
    '    Dim idx_byte As Short
    '    For idx_byte = 0 To Math.Min(Value.Length - 1, MCW_MAX_DATA_LENGTH)
    '      data1(idx_byte) = Value(idx_byte)
    '    Next
    '  End Set
    'End Property

    Public WriteOnly Property Track2_Length() As Short
      Set(ByVal Value As Short)
        data2_length = Value
      End Set
    End Property

    Public WriteOnly Property Track2_Data() As String
      Set(ByVal Value As String)
        data2 = Value
      End Set
    End Property

    'Public WriteOnly Property Track2_Data() As Byte()
    '  Set(ByVal Value As Byte())
    '    Dim idx_byte As Short
    '    For idx_byte = 0 To Math.Min(Value.Length - 1, MCW_MAX_DATA_LENGTH)
    '      data1(idx_byte) = Value(idx_byte)
    '    Next
    '  End Set
    'End Property

    Public WriteOnly Property Track3_Length() As Short
      Set(ByVal Value As Short)
        data3_length = Value
      End Set
    End Property

    Public WriteOnly Property Track3_Data() As String
      Set(ByVal Value As String)
        data3 = Value
      End Set
    End Property

    'Public WriteOnly Property Track3_Data() As Byte()
    '  Set(ByVal Value As Byte())
    '    Dim idx_byte As Short
    '    For idx_byte = 0 To Math.Min(Value.Length - 1, MCW_MAX_DATA_LENGTH)
    '      data1(idx_byte) = Value(idx_byte)
    '    Next
    '  End Set
    'End Property

    Public Sub New()
      MyBase.New()
      control_block = Marshal.SizeOf(Me)

      data1_length = 0
      data2_length = 0
      data3_length = 0
    End Sub

  End Class

#End Region

#Region " Public Events "

  ' The Message Received Event
  Public Event EventCardWriteResult(ByVal CardWriteResult As TYPE_MCW_WRITE_RESULT)

#End Region

#Region " Members "

  Private m_thread As System.Threading.Thread
  Private m_model_files As New ArrayList()
  Private m_models_data As New ArrayList()
  Private m_initialized As Boolean
  Private m_device_initialized As Boolean
  Private m_card_coercivity As Short

  Private m_write_result As TYPE_MCW_WRITE_RESULT
  Private m_site_data As TYPE_SITE_DATA

  Private m_ev_stop As New System.Threading.AutoResetEvent(False)
  Private m_ev_start_write As New System.Threading.AutoResetEvent(False)
  Private m_ev_exit_thread As New System.Threading.AutoResetEvent(False)

  Private m_accept_requests As Boolean
  Private m_exit_thread As Boolean
  Private m_write_internal_track_data_to_track2 As Boolean

#End Region

#Region " External Prototypes "

  Private Declare Function Common_MCW_ModuleInit Lib "CommonMisc" (<[In](), [Out]()> _
                                                                ByVal Models As CLASS_MCW_MODELS_LIST) As Short

  Private Declare Function Common_MCW_DeviceWrite Lib "CommonMisc" (<[In](), [Out]()> _
                                                                 ByVal WriteData As CLASS_MCW_WRITE) As Short

  Private Declare Function Common_MCW_DeviceCancelWrite Lib "CommonMisc" () As Short

  Private Declare Function Common_MCW_DeviceClose Lib "CommonMisc" () As Short

#End Region

#Region " Properties "

  Public ReadOnly Property IsInitialized() As Boolean
    Get
      Return m_initialized
    End Get

  End Property

  Public Property DeviceInitialized() As Boolean
    Get
      Return m_device_initialized
    End Get
    Set(ByVal Value As Boolean)
      m_device_initialized = Value
    End Set
  End Property

  Public Property CardCoercivity() As Short
    Get
      Return m_card_coercivity
    End Get
    Set(ByVal Value As Short)
      m_card_coercivity = Value
    End Set
  End Property

  Public Property WriteInternalTrackDataToTrack2() As Boolean
    Get
      Return m_write_internal_track_data_to_track2
    End Get
    Set(ByVal Value As Boolean)
      m_write_internal_track_data_to_track2 = Value
    End Set
  End Property

  Public ReadOnly Property ModelsData() As ArrayList
    Get
      Return m_models_data
    End Get
  End Property

  Public Property SiteId() As Integer
    Get
      Return m_site_data.id
    End Get
    Set(ByVal Value As Integer)
      m_site_data.id = Value
    End Set
  End Property

  Public Property SiteName() As String
    Get
      Return m_site_data.name
    End Get
    Set(ByVal Value As String)
      m_site_data.name = Value
    End Set
  End Property

  Public WriteOnly Property ResetSessionSequence()
    Set(ByVal value)
      m_write_result.session_last_sequence = 0
    End Set
  End Property

  Public Property SiteLastSequence() As Int64
    Get
      Return m_site_data.internal_last_sequence
    End Get
    Set(ByVal Value As Int64)
      m_site_data.internal_last_sequence = Value
    End Set
  End Property

#End Region

#Region " Private Functions "

  ' PURPOSE: Module initialization: Call MCW Api module init to obtain the list of devices.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Function ModuleInit() As Integer
    Dim idx_model As Integer
    Dim models_list As New CLASS_MCW_MODELS_LIST()
    Dim model_item As New CLASS_MCW_MODEL
    Dim model_data As TYPE_MCW_MODEL_DATA

    If Common_MCW_ModuleInit(models_list) = False Then
      Return MCW_STATUS_ERROR
    End If

    ' Fill models array
    For idx_model = 0 To models_list.ModelsList.Count - 1
      Call models_list.ModelsList.GetItem(idx_model, model_item)

      model_data = New TYPE_MCW_MODEL_DATA
      model_data.id = model_item.ModelId
      model_data.name = model_item.ModelName
      m_models_data.Add(model_data)
    Next

    Return MCW_STATUS_OK

  End Function ' ModuleInit

  ' PURPOSE : Gets the last recorded id. of the site.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '         - LastSequence: Last recorded card id.
  '
  ' RETURNS:
  '     - TRUE: Success
  '     - FALSE: Error

  Public Function Site_GetLastSequence(ByVal SiteId As Integer, ByRef LastSequence As Int64) As Boolean
    Dim card_generation As DataTable
    Dim db_site_id As Integer
    Dim sql_text As String
    Dim SqlTrans As System.Data.SqlClient.SqlTransaction = Nothing

    db_site_id = SiteId

    If Not GUI_BeginSQLTransaction(SqlTrans) Then
      Return False
    End If

    ' Build query
    sql_text = "select CG_LAST_SEQUENCE" _
              & " from CARD_GENERATION" _
              & " where CG_SITE_ID = " & db_site_id

    ' Check query results
    card_generation = GUI_GetTableUsingSQL(sql_text, 1)
    If IsNothing(card_generation) Then
      GUI_EndSQLTransaction(SqlTrans, False)

      Return False
    End If

    ' Check for number of rows
    If card_generation.Rows.Count() = 1 Then
      LastSequence = card_generation.Rows.Item(0).Item("CG_LAST_SEQUENCE")

      Return True
    Else
      ' Insert row (initial sequence id.)
      LastSequence = 0

      sql_text = "INSERT INTO CARD_GENERATION " & _
                           " ( CG_SITE_ID " & _
                           " , CG_LAST_SEQUENCE " & _
                           " )" & _
                    " VALUES " & _
                           " ( " & SiteId & _
                           " , " & LastSequence & _
                           " )"

      If Not GUI_SQLExecuteNonQuery(sql_text, SqlTrans) Then
        GUI_EndSQLTransaction(SqlTrans, False)

        Return False
      End If
    End If

    ' End transaction
    If Not GUI_EndSQLTransaction(SqlTrans, True) Then
      Return False
    End If

    Return True

  End Function ' Site_GetLastSequence


  ' PURPOSE : Set the last recorded id. on a specific site.
  '
  '  PARAMS:
  '     - INPUT:
  '         - LastSequence: Last recorded card id.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: Success
  '     - FALSE: Error

  Public Function Site_SetLastSequence(ByVal SiteId As Integer, ByVal LastSequence As Int64) As Integer
    Dim SqlTrans As System.Data.SqlClient.SqlTransaction = Nothing
    Dim sql_command As SqlCommand
    Dim str_sql As String
    Dim num_rows_updated As Integer
    Dim commit_trx As Boolean
    Dim rc As Integer

    rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB

    Try

      If Not GUI_BeginSQLTransaction(SqlTrans) Then
        Exit Function
      End If

      commit_trx = True

      str_sql = "UPDATE CARD_GENERATION " & _
                  " SET CG_LAST_SEQUENCE = " & LastSequence & _
                " WHERE CG_SITE_ID = " & SiteId

      sql_command = New SqlCommand(str_sql)
      sql_command.Connection = SqlTrans.Connection
      sql_command.Transaction = SqlTrans

      If Not GUI_SQLExecuteNonQuery(str_sql, SqlTrans, num_rows_updated) Then
        commit_trx = False
      End If

      If num_rows_updated = 0 Then
        commit_trx = False
      End If

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)

    Finally
      ' Either commit or rollback
      If Not (SqlTrans.Connection Is Nothing) Then
        If commit_trx Then
          SqlTrans.Commit()
          rc = ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Else
          SqlTrans.Rollback()
          rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If
      End If

      SqlTrans.Dispose()
      SqlTrans = Nothing

    End Try

    Return rc

  End Function ' Site_SetLastSequence

  ' PURPOSE: Record data on a card.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '   MCW_STATUS_OK: Record sucessfull.
  '   MCW_STATUS_ERROR: Error recording card.
  '
  Private Function RecordCard() As Integer
    Dim write_data As New CLASS_MCW_WRITE()
    Dim card_string As String
    Dim internal_card_id As String
    Dim record_sequence As Int64
    Dim bool_rc As Boolean
    Dim rc As Integer

    card_string = ""
    m_write_result.write_in_process = True

    record_sequence = m_site_data.internal_last_sequence + 1

    internal_card_id = WSI.Common.CardNumber.MakeInternalTrackData(m_site_data.id, record_sequence)

    bool_rc = WSI.Common.CardNumber.TrackDataToExternal(card_string, internal_card_id, MAGNETIC_CARD_TYPES.CARD_TYPE_PLAYER)
    If Not bool_rc Then
      Return MCW_STATUS_ERROR
    End If

    ' Prepare API dll data
    write_data.Coercivity = CardCoercivity
    write_data.Track1_Length = card_string.Length
    write_data.Track1_Data = card_string
    '' APB (09-MAR-2009)
    '' Copied same data to other two tracks to try to improve
    ''        card readability.
    ''ETP (01-DIC-2016) 
    ''Save track 2 data.
    If (WriteInternalTrackDataToTrack2) Then
      write_data.Track2_Length = internal_card_id.Length
      write_data.Track2_Data = internal_card_id
    End If
    'write_data.Track3_Length = card_string.Length
    'write_data.Track3_Data = card_string

    If Common_MCW_DeviceWrite(write_data) = False Then
      m_write_result.write_status = MCW_STATUS_ERROR
    Else
      m_write_result.write_status = MCW_STATUS_OK

      ' Increment last recorded sequence number
      m_site_data.internal_last_sequence = m_site_data.internal_last_sequence + 1

      rc = Site_SetLastSequence(m_site_data.id, m_site_data.internal_last_sequence)
      If rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB Then
        m_write_result.sequence_updated = False
        Call NLS_MsgBox(GLB_NLS_GUI_CLASS_II.Id(188), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Else
        m_write_result.sequence_updated = True
        m_write_result.session_last_sequence = m_write_result.session_last_sequence + 1
        m_write_result.total_sequence = m_site_data.internal_last_sequence
      End If
    End If

    ' Notify the write result with and event.
    RaiseEvent EventCardWriteResult(m_write_result)

    System.Threading.Thread.Sleep(800)
    Thread_EnableWrite()

    If m_write_result.write_status = MCW_STATUS_ERROR Then
      Return MCW_STATUS_ERROR
    End If

    Return MCW_STATUS_OK

  End Function ' RecordCard


  ' PURPOSE: Requests to Mcw Api
  '           - Module initialization.
  '           - Devices initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub Run()
    Dim wait_handles() As System.Threading.WaitHandle
    Dim idx_handle As Integer

    ' Process requests actions:
    '   - Initialize Mcw Module.
    '   - Initialize Mcw Devices.
    '   - Prepare notification events.
    '   - Loop waiting for device requests.

    '   - Initialize Mcw Module
    If ModuleInit() = MCW_STATUS_ERROR Then
      Exit Sub
    End If

    m_initialized = True

    ' - Prepare notification events.
    '   - Stop before starting to write.
    '   - Start recording process.
    ReDim wait_handles(2)
    wait_handles(HANDLE_IDX_STOP) = m_ev_stop
    wait_handles(HANDLE_IDX_START_WRITE) = m_ev_start_write
    wait_handles(HANDLE_IDX_EXIT_THREAD) = m_ev_exit_thread

    'Loop waiting for requests
    While Not m_exit_thread
      idx_handle = System.Threading.WaitHandle.WaitAny(wait_handles)
      Select Case idx_handle

        Case HANDLE_IDX_STOP
          m_accept_requests = False

        Case HANDLE_IDX_START_WRITE
          If m_accept_requests Then
            RecordCard()
          End If

        Case HANDLE_IDX_EXIT_THREAD
          m_exit_thread = True

        Case Else
          m_accept_requests = False
          m_exit_thread = True
      End Select
    End While

  End Sub ' Run

  ' PURPOSE: Stop requests thread.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub Finalize()

    MyBase.Finalize()

  End Sub ' Finalize

#End Region

#Region " Public Functions "

  ' PURPOSE: Mcw Api class constructor method.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Sub New()

    m_initialized = False
    m_device_initialized = False
    m_accept_requests = True
    m_exit_thread = False

    m_site_data.id = 0
    m_site_data.name = ""
    m_site_data.internal_last_sequence = 0

    m_write_result.write_in_process = False
    m_write_result.write_processed = False
    m_write_result.write_status = 0
    m_write_result.session_last_sequence = 0
    m_write_result.total_sequence = 0

    ' Create a Mcw Api request thread
    m_thread = New Threading.Thread(AddressOf Me.Run)
    m_thread.Name = "MCW API Thread"
    m_thread.Start()

  End Sub ' New

  ' PURPOSE: Called to request a card write. Data must be already available.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Sub Thread_EnableWrite()
    Call m_ev_start_write.Set()
    m_accept_requests = True

  End Sub ' Thread_EnableWrite


  ' PURPOSE: Called to stop request thread.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Sub Thread_StopRequests()

    ' Close device
    If DeviceInitialized Then
      If Common_MCW_DeviceCancelWrite() = False Then
        Call NLS_MsgBox(GLB_NLS_GUI_CLASS_II.Id(192), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      End If

      If Common_MCW_DeviceClose() = False Then
        Call NLS_MsgBox(GLB_NLS_GUI_CLASS_II.Id(192), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      End If

      DeviceInitialized = False
    End If

    Call m_ev_stop.Set()

  End Sub ' Thread_StopRequests


  ' PURPOSE: Called to stop request thread.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Sub Thread_Exit()
    Call m_ev_exit_thread.Set()
  End Sub ' Thread_Exit

#End Region

End Class

