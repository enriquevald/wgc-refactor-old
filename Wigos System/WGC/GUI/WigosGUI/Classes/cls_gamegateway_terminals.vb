﻿'-------------------------------------------------------------------
' Copyright © 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_gamegateway_terminals.vb
'
' DESCRIPTION : GameGateway terminals configuration class
' 
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 14-OCT-2015  JRC    Initial Version
' 22-OCT-2015  AVZ    Change InTouchGames to GameGateway
'--------------------------------------------------------------------

#Region "Imports"
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common
Imports GUI_CommonMisc
Imports System.Runtime.InteropServices
Imports System.Globalization
#End Region ' Imports

Public Class cls_gamegateway_terminals
  Inherits CLASS_BASE

#Region " Enum "

#End Region 'Enum

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_GAMEGATEWAY_TERMINALS
    Public gamegateway_enabledGP As Boolean
    Public terminal_list As New TerminalList
  End Class

#End Region ' Structures

#Region " Members "

  Protected m_GameGateway_cfg As New TYPE_GAMEGATEWAY_TERMINALS

#End Region ' Members

#Region " Properties "

  Public Property GameGateway_EnabledGP() As Boolean
    Get
      Return m_GameGateway_cfg.gamegateway_enabledGP
    End Get
    Set(ByVal value As Boolean)
      m_GameGateway_cfg.gamegateway_enabledGP = value
    End Set
  End Property


  Public ReadOnly Property TerminalList() As TerminalList
    Get
      Return m_GameGateway_cfg.terminal_list
    End Get
  End Property

#End Region ' Properties 

#Region " Publics "
#End Region ' Publics

#Region "Constructor / Destructor"

  Public Sub New()
    Call MyBase.New()
  End Sub

  Protected Overrides Sub Finalize()
    MyBase.Finalize()

  End Sub

#End Region ' Constructor / Destructor

#Region " Overrides "
  '----------------------------------------------------------------------------
  ' PURPOSE: Returns the related auditor data for the current object.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - The newly created object
  '
  ' NOTES:
  Public Overrides Function AuditorData() As CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA
    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)

    _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(6835), "")

    If Me.GameGateway_EnabledGP Then
      _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6841), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6839))
    Else
      _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6841), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6840))
    End If

    'Taken from same TerminalList control in frm_site_jackpot_configuration
    'Groups
    ' Type
    Call _auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(567), GLB_NLS_GUI_CONTROLS.GetString(486) + ": " + _
                                TerminalList.AuditStringType(GLB_NLS_GUI_CONFIGURATION.GetString(497), _
                                                             GLB_NLS_GUI_CONFIGURATION.GetString(499), _
                                                             GLB_NLS_GUI_CONFIGURATION.GetString(498)))


    ' Group list
    Call _auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(477), GLB_NLS_GUI_CONTROLS.GetString(482) + ": " + _
                               TerminalList.AuditStringList(GROUP_ELEMENT_TYPE.GROUP, _
                                                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Prviders list
    Call _auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(477), GLB_NLS_GUI_CONTROLS.GetString(478) + ": " + _
                               TerminalList.AuditStringList(GROUP_ELEMENT_TYPE.PROV, _
                                                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Zones list
    Call _auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(477), GLB_NLS_GUI_CONTROLS.GetString(479) + ": " + _
                               TerminalList.AuditStringList(GROUP_ELEMENT_TYPE.ZONE, _
                                                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Areas list
    Call _auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(477), GLB_NLS_GUI_CONTROLS.GetString(480) + ": " + _
                               TerminalList.AuditStringList(GROUP_ELEMENT_TYPE.AREA, _
                                                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Banks list
    Call _auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(477), GLB_NLS_GUI_CONTROLS.GetString(481) + ": " + _
                               TerminalList.AuditStringList(GROUP_ELEMENT_TYPE.BANK, _
                                                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    ' Terminals list
    Call _auditor_data.SetField(GLB_NLS_GUI_CONTROLS.Id(477), GLB_NLS_GUI_CONTROLS.GetString(483) + ": " + _
                               TerminalList.AuditStringList(GROUP_ELEMENT_TYPE.TERM, _
                                                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)))

    Return _auditor_data

  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Deletes the given object from the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return ENUM_STATUS.STATUS_ERROR
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Inserts the given object to the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return ENUM_STATUS.STATUS_ERROR
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads from the database into the given object
  '
  ' PARAMS:
  '   - INPUT:
  '     - ObjectId: An object, it must be 'casted' to the desired KEY.
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As ENUM_STATUS

    Dim _rc As Integer

    ' Read GameGateway information
    _rc = Read_GameGateway_cfg(m_GameGateway_cfg, SqlCtx)

    Select Case _rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select






  End Function ' DB_Read

  '----------------------------------------------------------------------------
  ' PURPOSE: Updates the given Bonus to the database
  '
  ' PARAMS:
  '   - INPUT:
  '     - Bonus As TYPE_BONUS
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim _rc As Integer

    ' Read parameters information
    _rc = Update_GameGateway_cfg(m_GameGateway_cfg, SqlCtx)

    Select Case _rc
      Case ENUM_DB_RC.DB_RC_OK
        Return ENUM_STATUS.STATUS_OK

      Case ENUM_DB_RC.DB_RC_ERROR_NOT_FOUND
        Return CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Update

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE
    Dim _target As cls_gamegateway_terminals
    _target = New cls_gamegateway_terminals


    _target.GameGateway_EnabledGP = Me.GameGateway_EnabledGP
    _target.TerminalList.FromXml(Me.TerminalList.ToXmlWithAll())


    Return _target

  End Function ' Duplicate


#End Region ' Overrides 


#Region " Private Functions "

  '----------------------------------------------------------------------------
  ' PURPOSE: Reads an object from the database.
  '
  ' PARAMS:
  '   - INPUT: None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - STATUS_OK
  '     - STATUS_ERROR
  '     - STATUS_NOT_FOUND
  '     - STATUS_DUPLICATE_KEY
  '
  ' NOTES:
  Private Function Read_GameGateway_cfg(ByVal GameGateway_cfg As TYPE_GAMEGATEWAY_TERMINALS, _
                                      ByVal Context As Integer) As Integer

    Dim _sb As StringBuilder
    Dim _table As DataTable
    Dim _xml As String
    Dim _general_params As WSI.Common.GeneralParam.Dictionary


    Try
      _general_params = WSI.Common.GeneralParam.Dictionary.Create()
      GameGateway_cfg.gamegateway_enabledGP = _general_params.GetBoolean("GameGateway", "Enabled", False)

      _sb = New StringBuilder()

      _sb.AppendLine("SELECT gtl_terminal_list ")
      _sb.AppendLine("  FROM gamegateway_terminal_list  ")

      _table = GUI_GetTableUsingSQL(_sb.ToString(), 5000)

      If IsNothing(_table) Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

      If _table.Rows.Count() > 1 Then
        Return ENUM_STATUS.STATUS_ERROR
      End If

      If _table.Rows.Count() = 1 Then
        'TERMINAL_LIST
        _xml = Nothing
        If _table.Rows(0).Item("gtl_terminal_list") IsNot DBNull.Value Then
          _xml = _table.Rows(0).Item("gtl_terminal_list")
        End If
        GameGateway_cfg.terminal_list.FromXml(_xml)

      End If


      Return ENUM_STATUS.STATUS_OK

    Catch ex As Exception
      Return ENUM_STATUS.STATUS_ERROR
    End Try
  End Function ' ReadChipsSets

  '----------------------------------------------------------------------------
  ' PURPOSE: Update the given object into the database.
  '
  ' PARAMS:
  '   - INPUT:  None
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_CONFIGURATION_RC
  '
  ' NOTES:
  Private Function Update_GameGateway_cfg(ByVal _GameGateway_cfg As TYPE_GAMEGATEWAY_TERMINALS, _
                                        ByVal Context As Integer) As Integer
    Dim _str_sql As String
    Dim _num_rows_updated As Integer
    Dim _xml As String

    Try
      _xml = _GameGateway_cfg.terminal_list.ToXmlWithAll()

      Using _db_trx As New DB_TRX()

        If Not DB_GeneralParam_Update("GameGateway", "Enabled", IIf(_GameGateway_cfg.gamegateway_enabledGP, "1", "0"), _db_trx.SqlTransaction) Then
          Return ENUM_DB_RC.DB_RC_ERROR_DB

        End If


        ' UPDATE GameGateway Terminal List
        _str_sql = "UPDATE  gamegateway_terminal_list " & _
                    "   SET  gtl_terminal_list = @p1 "

        Using _cmd As New SqlClient.SqlCommand(_str_sql, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          If Not String.IsNullOrEmpty(_xml) Then
            _cmd.Parameters.Add("@p1", SqlDbType.Xml).Value = _xml
          Else
            _cmd.Parameters.Add("@p1", SqlDbType.Xml).Value = DBNull.Value
          End If
          _num_rows_updated = _cmd.ExecuteNonQuery()

        End Using

        If _num_rows_updated > 0 Then
          ' Min one row
          _db_trx.Commit()
        Else
          _db_trx.Rollback()
          Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
        End If


      End Using '_db_trx

    Catch ex As Exception
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End Try

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function ' ChipsSets_Insert

#End Region ' Private Functions 


End Class
